/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DataServicesUtil.java 1.40     08/12/03 SMI"
 */

package com.sun.cluster.agent.dataservices.utils;

// J2SE
import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.logging.Logger;

// JMX
import javax.management.AttributeValueExp;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.StringValueExp;

// CACAO
import com.sun.cacao.ObjectNameFactory;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.cluster.ClusterMBean;
import com.sun.cluster.agent.ipmpgroup.IpmpGroupMBean;
import com.sun.cluster.agent.node.NodeMBean;
import com.sun.cluster.agent.node.ZoneMBean;
import com.sun.cluster.agent.rgm.ResourceGroupMBean;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.agent.rgm.ResourceTypeMBean;
import com.sun.cluster.agent.rgm.RgmManagerMBean;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyEnum;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.agent.transport.AdapterData;
import com.sun.cluster.agent.transport.NodeAdapterMBean;
import com.sun.cluster.agent.transport.ZoneAdapterMBean;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.common.TrustedMBeanModel;


/**
 * This class provides a set of utility methods for the DataService Module
 */
public class DataServicesUtil {

    public static final int IPv4LENGTH = 4;
    public static final int IPv6LENGTH = 6;

    private static int ZONEDETAIL_LEN = 4;
    private static int ZONEID_INDEX = 0;
    private static int ZONENAME_INDEX = 1;
    private static int ZONESTATUS_INDEX = 2;
    private static int ZONEPATH_INDEX = 3;

    private static Logger logger = Logger.getLogger(Util.DOMAIN);

    /**
     * Concatenates two String Arrays into a new Array
     *
     * @param  array1  String Array 1
     * @param  array2  String Array 2
     *
     * @return  the concatenated String Array
     */
    public static String[] concatenateArrays(String array1[], String array2[]) {

        int length1 = array1.length;
        int length2 = array2.length;
        int length3 = length1 + length2;
        String finalArray[] = new String[length3];

        for (int i = 0; i < length1; i++)
            finalArray[i] = array1[i];

        for (int i = length1; i < length3; i++)
            finalArray[i] = array2[i - length1];

        return finalArray;
    }

    /**
     * Converts String Array to String
     *
     * @param  array  StringArray
     *
     * @return  the String from the Array
     */
    public static String convertArrayToString(String array[]) {

        StringBuffer buffer = new StringBuffer();

        for (int i = 0; i < array.length; i++)
            buffer.append(array[i] + " ");

        return buffer.toString();
    }

    public static ResourceTypeMBean getResourceTypeMBean(MBeanServer mbs,
        String rType, boolean doRegister) {
        ResourceTypeMBean rtMbean = null;
        ExitStatus status[] = null;

        try {

            // Get the ObjectName Factory for RGM CMASS Module
            ObjectNameFactory rgmOnf = new ObjectNameFactory(ResourceMBean.class
                    .getPackage().getName());

            // Check whether the ResourceType is Registered
            ObjectName rtObjectName = rgmOnf.getObjectNamePattern(
                    ResourceTypeMBean.class);
            QueryExp query = Query.or(Query.initialSubString(Query.attr("Name"),
                        Query.value(rType + ":")),
                    Query.eq(Query.attr("Name"), Query.value(rType)));

            List rtMBeanList = TrustedMBeanModel.getMBeanProxies(Util
                    .getClusterEndpoint(), ResourceTypeMBean.class, false,
                    query);
            int numRts = rtMBeanList.size();

            // When multiple RTs of different versions are present
            // always work with the latest version
            if (numRts > 1) {
                String rtName = getLatestRTVersion(rtMBeanList);

                if (rtName != null) {
                    rType = rtName;
                }
            }

            // This step is necessary as rtName is usually appended with version
            // We execute the query again and get the first entry.
            // This entry should give us the ObjectName.
            query = Query.or(Query.initialSubString(Query.attr("Name"),
                        Query.value(rType + ":")),
                    Query.eq(Query.attr("Name"), Query.value(rType)));

            Set set = mbs.queryNames(rtObjectName, query);

            // If there are still more than one MBean for this
            // resource type, something is wrong, get out.
            if (set.size() > 1) {
                return null;
            }

            Iterator setIterator = set.iterator();
            ObjectName rTypeObjectName = (ObjectName) setIterator.next();

            // Get Handler to the ResourceType MBean
            rtMbean = (ResourceTypeMBean) MBeanServerInvocationHandler
                .newProxyInstance(mbs, rTypeObjectName, ResourceTypeMBean.class,
                    false);

            // If the MBean is not present for this specific RT,
            // there is nothing the code below can do.
            if (rtMbean == null) {
                return null;
            }

            if (!rtMbean.isRegistered() && doRegister) {

                // Get Handler to the RGM Mbean
                RgmManagerMBean rgmMbean = (RgmManagerMBean)
                    MBeanServerInvocationHandler.newProxyInstance(mbs,
                        rgmOnf.getObjectName(RgmManagerMBean.class, null),
                        RgmManagerMBean.class, false);

                // Register the Resource.
                status = rgmMbean.registerResourceType(rType);
            }

            return rtMbean;
        } catch (CommandExecutionException e) {
            status = e.getExitStatusArray();
            System.out.println("Command Tried to Execute : \n" +
                DataServicesUtil.convertArrayToString(
                    status[0].getCmdExecuted()));

            System.out.println(status[0].getErrStrings());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    /**
     * This method returns the System Properties for a particular Resource of
     * ResourceType rType. This method Registers the ResourceType if the
     * ResourceType is not Registered.
     *
     * @param  mbs  The MBeanServer reference
     * @param  rType  ResourceType of the Resource
     *
     * @return  String Array of Resource's extension properties
     */
    public static String[] getSystemProperties(MBeanServer mbs, String rType) {

        ExitStatus status[] = null;

        try {
            ResourceTypeMBean rtMbean = getResourceTypeMBean(mbs, rType, true);

            if (rtMbean == null) {
                return null;
            }

            List sysPropertiesList = rtMbean.getSystemProperties();
            int numProperties = sysPropertiesList.size();
            String sysProperties[] = new String[numProperties];

            Iterator i = sysPropertiesList.iterator();
            int j = 0;

            while (i.hasNext()) {

                // The element in the List is of the Form
                // <NAME=START_TIMEOUT  EXT=false REQ=false DESC='null' ...
                // Store only the value for Name.
                // Take SubString from 6 ie len(<NAME=) to The First Space.
                String entry = i.next().toString().trim();
                entry = entry.substring(6, entry.indexOf(" "));
                sysProperties[j++] = entry;
            }

            return sysProperties;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;
    }


    /**
     * This method returns the Extension Properties for a particular Resource of
     * ResourceType rType. This method Registers the ResourceType if the
     * ResourceType is not Registered.
     *
     * @param  mbs  The MBeanServer reference
     * @param  rType  ResourceType of the Resource
     *
     * @return  String Array of Resource's extension properties
     */
    public static String[] getExtensionProperties(MBeanServer mbs,
        String rType) {

        ExitStatus status[] = null;

        try {
            ResourceTypeMBean rtMbean = getResourceTypeMBean(mbs, rType, true);

            if (rtMbean == null) {
                return null;
            }

            List extPropertiesList = rtMbean.getExtProperties();
            int numProperties = extPropertiesList.size();
            String extProperties[] = new String[numProperties];
            Iterator i = extPropertiesList.iterator();
            int j = 0;

            while (i.hasNext()) {

                // The element in the List is of the Form
                // <NAME=NetIfList EXT=true REQ=false DESC='null' ...
                // Store only the value for "NAME".
                // Take SubString from 6 ie len(<NAME=) to Space.
                // Also Remove the trailing spaces.
                String entry = i.next().toString().trim();
                entry = entry.substring(6, entry.indexOf(" "));
                extProperties[j++] = entry;
            }

            return extProperties;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    /**
     * Returns the System and Extension Properties for a Resource of
     * ResourceType rType.
     *
     * @param  mbs  MBeanServer
     * @param  rType  The ResourceType of the Resource
     *
     * @return  String[] Array of Extension and System Properties
     */
    public static String[] getRTRProperties(MBeanServer mbs, String rType) {
        return getExtensionProperties(mbs, rType);
    }

    /**
     * Returns the Nodelist of the Cluster including the zones
     *
     * @param  mbs  MBeanServer
     *
     * @return  String[] Nodelist Array
     */
    public static String[] getNodeList(MBeanServer mbs) {
        String nodeList[] = null;

        // Get the ObjectName Factory for Cluster CMASS Module
        ObjectNameFactory clusterOnf = new ObjectNameFactory(ClusterMBean.class
                .getPackage().getName());

        // Get the Objectname Pattern For ClusterMBean Instances
        ObjectName clusterObjectName = clusterOnf.getObjectNamePattern(
                ClusterMBean.class);

        Set clusterMBeanObjNames = mbs.queryNames(clusterObjectName, null);

        // Iterate through the list and get the Nodenames
        Iterator i = clusterMBeanObjNames.iterator();

        if (i.hasNext()) {
            ObjectName clusterMBeanObjectName = (ObjectName) i.next();

            // Get Handler to the Cluster MBean
            ClusterMBean clusterMbean = (ClusterMBean)
                MBeanServerInvocationHandler.newProxyInstance(mbs,
                    clusterMBeanObjectName, ClusterMBean.class, false);
            nodeList = clusterMbean.getAllZones();
        }

        // Get offline nodes
        ObjectNameFactory nodeOnf = new ObjectNameFactory(NodeMBean.class
                .getPackage().getName());
        ObjectName nodeNamePattern = nodeOnf.getObjectNamePattern(
                NodeMBean.class);
        List offlineNodes = new ArrayList();

        for (int j = 0; j < nodeList.length; j++) {

            if (nodeList[j].indexOf(Util.COLON) == -1) {

                try {
                    QueryExp query = Query.match(new AttributeValueExp("Name"),
                            new StringValueExp(nodeList[j]));
                    Set nodeMBeans = mbs.queryNames(nodeNamePattern, query);

                    if (nodeMBeans.size() > 0) {
                        Iterator setIterator = nodeMBeans.iterator();
                        ObjectName nodeObjectName = (ObjectName) setIterator
                            .next();
                        NodeMBean nodeMbean = (NodeMBean)
                            MBeanServerInvocationHandler.newProxyInstance(mbs,
                                nodeObjectName, NodeMBean.class, false);

                        if (!nodeMbean.isOnline()) {
                            offlineNodes.add(nodeList[j]);
                        }
                    }
                } catch (Exception e) {
                    offlineNodes.add(nodeList[j]);
                }
            }
        }

        // Prune the offline nodes and their Zones
        List finalNodes = new ArrayList();

        for (int j = 0; j < nodeList.length; j++) {
            String split_str[] = nodeList[j].split(Util.COLON);

            if (!offlineNodes.contains(split_str[0])) {
                finalNodes.add(nodeList[j]);
            }
        }

        return (String[]) finalNodes.toArray(new String[finalNodes.size()]);
    }


    /**
     * Return the Nodelist of the cluster including zones
     *
     * @return  String[] Nodelist Array
     *
     * @deprecated
     */
    public static String[] getNodeList() {
        ClusterMBean mbean = null;
        String nodeList[];

        try {
            mbean = (ClusterMBean) TrustedMBeanModel.getMBeanProxy(Util
                    .getClusterEndpoint(), ClusterMBean.class, null);
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());

            return null;
        } catch (SecurityException see) {
            System.out.println(see.getMessage());

            return null;
        }

        nodeList = mbean.getAllZones();

        return nodeList;
    }

    /**
     * Native method to check whether the given hostname is valid or not.
     *
     * @param  hostname  Hostname to check
     *
     * @return  "true" if it is a valid IPv4 or IPv6 address "false" if it is an
     * invalid hostname
     */
    public static native boolean isHostNameValid(String hostname);


    /**
     * Native method to check whether the given hostname is IPv6 or not.
     *
     * @param  hostname  Hostname to check
     *
     * @return  "true" if it is an IPv6 hostname "false" if it is an invalid
     * hostname
     */
    public static native boolean isHostNameIPv6(String hostname);


    /**
     * This method retrieves the IPMP Groups Configured on the Node
     *
     * @param  mbs  MBeanServer
     *
     * @return  String[] Array of IPMPGroup Names
     */
    public static String[] getIPMPGroups(MBeanServer mbs) {

        String ipmpNameList[];

        // Get the ObjectName Factory for IPMP CMASS Module
        ObjectNameFactory ipmpOnf = new ObjectNameFactory(IpmpGroupMBean.class
                .getPackage().getName());

        // Get the Objectname Pattern For IpmpGroupMBean Instances
        ObjectName ipmpObjectNamePat = ipmpOnf.getObjectNamePattern(
                IpmpGroupMBean.class);

        Set ipmpMBeanObjNames = mbs.queryNames(ipmpObjectNamePat, null);
        ipmpNameList = new String[ipmpMBeanObjNames.size()];

        // Iterate through the list and get the Nodenames
        Iterator i = ipmpMBeanObjNames.iterator();
        int j = 0;

        while (i.hasNext()) {
            ObjectName ipmpObjectName = (ObjectName) i.next();

            // Replace ":" with @
            ipmpNameList[j++] = ipmpOnf.getInstanceName(ipmpObjectName).replace(
                    ':', '@');
        }

        return ipmpNameList;
    }

    /**
     * This method retrievs all RGs of type rtName hosted on nodes specified by
     * nodeList
     *
     * @param  nodeList  list of nodes
     * @param  rtName  Resource type
     *
     * @return  list of RGs that match the search criteria
     */
    public static List getRGList(MBeanServer mbs, String nodeList[],
        String rtName, String extPropNames[]) {
        Set resources = null;
        List rgList = null;
        Set rgSet = null;
        HashMap map = null;

        // query for all RGs
        try {

            // Get the ObjectName Factory for RGM CMASS Module
            ObjectNameFactory rgmOnf = new ObjectNameFactory(ResourceMBean.class
                    .getPackage().getName());

            ObjectName rgObjectName = rgmOnf.getObjectNamePattern(
                    ResourceGroupMBean.class);
            ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                    ResourceMBean.class);
            rgSet = mbs.queryNames(rgObjectName, null);

            Iterator rgIterator = rgSet.iterator();

            while (rgIterator.hasNext()) {
                rgObjectName = (ObjectName) rgIterator.next();

                // Get Handler to the ResourceGroup MBean
                ResourceGroupMBean rg = (ResourceGroupMBean)
                    MBeanServerInvocationHandler.newProxyInstance(mbs,
                        rgObjectName, ResourceGroupMBean.class, false);

                List connectedNodes = Arrays.asList(rg.getNodeNames());
                List nodes = Arrays.asList(nodeList);

                String rgName = rg.getName();

                if (connectedNodes.containsAll(nodes) &&
                        nodes.containsAll(connectedNodes)) {

                    // query for all resources in resource rg of type
                    // HAStoragePlus
                    QueryExp query = Query.and(Query.eq(
                                new AttributeValueExp("ResourceGroupName"),
                                Query.value(rgName)),
                            Query.or(
                                Query.initialSubString(Query.attr("Type"),
                                    Query.value(rtName + ":")),
                                Query.eq(Query.attr("Type"),
                                    Query.value(rtName))));

                    resources = mbs.queryNames(rObjectName, query);

                    if (rgList == null) {
                        rgList = new ArrayList();
                    }

                    Iterator iterator = resources.iterator();

                    while (iterator.hasNext()) {
                        ObjectName rsObjectName = (ObjectName) iterator.next();
                        String rsName = rgmOnf.getInstanceName(rsObjectName);
                        map = new HashMap();
                        map.put(Util.RG_NAME, rgName);
                        map.put(Util.RS_NAME, rsName);

                        if (extPropNames != null) {

                            for (int i = 0; i < extPropNames.length; i++) {
                                Object obj = getResourcePropertyValue(mbs,
                                        rsName, extPropNames[i]);
                                map.put(extPropNames[i], obj);
                            }
                        }

                        rgList.add(map);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return rgList;
    }

    /**
     * This method retrievs all resources of type rtName hosted on nodes
     * specified by nodeList
     *
     * @param  mbs Mbean server
     * @param  nodeList  list of nodes
     * @param  rtName  Resource type
     * @param  extPropsArg  list of extension properties to return
     *
     * @return  List of resources that match the search criteria
     */
    public static List getGDDResources(MBeanServer mbs, String nodeList[],
        String rtName, String extPropsArg[]) {
        Set resources = null;
        HashMap resourceMap = null;
        List resourceList = null;
        Set rgSet = null;

        // query for all RGs
        try {

            // Get the ObjectName Factory for RGM CMASS Module
            ObjectNameFactory rgmOnf = new ObjectNameFactory(ResourceMBean.class
                    .getPackage().getName());

            ObjectName rgObjectName = rgmOnf.getObjectNamePattern(
                    ResourceGroupMBean.class);
            ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                    ResourceMBean.class);
            rgSet = mbs.queryNames(rgObjectName, null);

            Iterator rgIterator = rgSet.iterator();

            while (rgIterator.hasNext()) {
                rgObjectName = (ObjectName) rgIterator.next();

                // Get Handler to the ResourceGroup MBean
                ResourceGroupMBean rg = (ResourceGroupMBean)
                    MBeanServerInvocationHandler.newProxyInstance(mbs,
                        rgObjectName, ResourceGroupMBean.class, false);


                List connectedNodes = Arrays.asList(rg.getNodeNames());
                List nodes = Arrays.asList(nodeList);

                String rgName = rg.getName();

                if (connectedNodes.containsAll(nodes) &&
                        nodes.containsAll(connectedNodes)) {

                    // query for all resources in resource rg of resource type
                    // passed as argument
                    QueryExp query = Query.and(Query.eq(
                                new AttributeValueExp("ResourceGroupName"),
                                Query.value(rgName)),
                            Query.or(
                                Query.initialSubString(Query.attr("Type"),
                                    Query.value(rtName + ":")),
                                Query.eq(Query.attr("Type"),
                                    Query.value(rtName))));

                    resources = mbs.queryNames(rObjectName, query);

                    if (resourceList == null) {
                        resourceList = new ArrayList();
                    }

                    Iterator iterator = resources.iterator();

                    while (iterator.hasNext()) {
                        ObjectName rsObjectName = (ObjectName) iterator.next();

                        // Get Handler to the resource mbean
                        ResourceMBean resource = (ResourceMBean)
                            MBeanServerInvocationHandler.newProxyInstance(mbs,
                                rsObjectName, ResourceMBean.class, false);

                        String rsName = rgmOnf.getInstanceName(rsObjectName);
                        resourceMap = new HashMap();
                        resourceMap.put(Util.RS_NAME, rsName);
                        resourceMap.put(Util.RG_NAME, rgName);

                        // Find the property name in Extension Properties
                        List extProps = resource.getExtProperties();
                        Iterator extPropIter = extProps.iterator();

                        if (extPropsArg != null) {

                            for (int i = 0; i < extPropsArg.length; i++) {
                                boolean found = false;

                                while (extPropIter.hasNext() && !found) {
                                    ResourceProperty rp = (ResourceProperty)
                                        extPropIter.next();
                                    String entry = rp.getName().trim();

                                    if (entry.equals(extPropsArg[i])) {
                                        found = true;

                                        String value = null;

                                        if (rp instanceof
                                                ResourcePropertyString) {
                                            ResourcePropertyString rpStr =
                                                (ResourcePropertyString) rp;
                                            value = rpStr.getValue();
                                        }

                                        resourceMap.put(extPropsArg[i], value);
                                    }
                                }
                            }
                        }

                        resourceList.add(resourceMap);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return resourceList;
    }

    /**
     * This method retrievs all resources of type rtName hosted on nodes
     * specified by nodeList
     *
     * @param  nodeList  list of nodes
     * @param  rtName  Resource type
     *
     * @return  list of resources that match the search criteria
     */
    public static List getResourceList(MBeanServer mbs, String nodeList[],
        String rtName) {
        Set resources = null;
        List resourceList = null;
        Set rgSet = null;

        // query for all RGs
        try {

            // Get the ObjectName Factory for RGM CMASS Module
            ObjectNameFactory rgmOnf = new ObjectNameFactory(ResourceMBean.class
                    .getPackage().getName());

            ObjectName rgObjectName = rgmOnf.getObjectNamePattern(
                    ResourceGroupMBean.class);
            ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                    ResourceMBean.class);
            rgSet = mbs.queryNames(rgObjectName, null);

            Iterator rgIterator = rgSet.iterator();

            while (rgIterator.hasNext()) {
                rgObjectName = (ObjectName) rgIterator.next();

                // Get Handler to the ResourceGroup MBean
                ResourceGroupMBean rg = (ResourceGroupMBean)
                    MBeanServerInvocationHandler.newProxyInstance(mbs,
                        rgObjectName, ResourceGroupMBean.class, false);


                List connectedNodes = Arrays.asList(rg.getNodeNames());
                List nodes = Arrays.asList(nodeList);

                String rgName = rg.getName();

                if (connectedNodes.containsAll(nodes) &&
                        nodes.containsAll(connectedNodes)) {

                    // query for all resources in resource rg of type
                    // HAStoragePlus
                    QueryExp query = Query.and(Query.eq(
                                new AttributeValueExp("ResourceGroupName"),
                                Query.value(rgName)),
                            Query.or(
                                Query.initialSubString(Query.attr("Type"),
                                    Query.value(rtName + ":")),
                                Query.eq(Query.attr("Type"),
                                    Query.value(rtName))));

                    resources = mbs.queryNames(rObjectName, query);

                    if (resourceList == null) {
                        resourceList = new ArrayList();
                    }

                    Iterator iterator = resources.iterator();

                    while (iterator.hasNext()) {
                        ObjectName rsObjectName = (ObjectName) iterator.next();
                        String rsName = rgmOnf.getInstanceName(rsObjectName);
                        String entryToAdd = rsName + Util.COLON + rgName;

                        if (!resourceList.contains(entryToAdd)) {
                            resourceList.add(entryToAdd);
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return resourceList;
    }

    /**
     * This method retrieves the names of all Resources with a particular
     * Extension or System propertyName set to a propertyValue.
     *
     * @param  rtName  ResourceType of the Resource
     * @param  propertyName  PropertyName
     * @param  propertyValue  PropertyValue
     *
     * @return  List of Matching Resources
     */
    public static List getResourceList(MBeanServer mbs, String rtName,
        String propertyName, Object propertyValue) {

        // Search Result
        List resourceList = null;

        // Get the ObjectName Factory for RGM CMASS Module
        ObjectNameFactory rgmOnf = new ObjectNameFactory(ResourceMBean.class
                .getPackage().getName());

        // Get the ResourceType with the version
        ObjectName rtObjectName = rgmOnf.getObjectNamePattern(
                ResourceTypeMBean.class);
        QueryExp query = Query.match(new AttributeValueExp("Name"),
                Query.value(rtName + "*"));
        Set rtset = mbs.queryNames(rtObjectName, query);

        Iterator i = rtset.iterator();
        boolean found = false;

        if (i.hasNext()) {
            rtObjectName = (ObjectName) i.next();

            String rtNameWithVersion = rgmOnf.getInstanceName(rtObjectName);

            // Get the ResourceNamesList for the rtName
            ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                    ResourceMBean.class);
            query = Query.match(new AttributeValueExp("Type"),
                    Query.value(rtNameWithVersion));

            Set rset = mbs.queryNames(rObjectName, query);

            Iterator rsetIter = rset.iterator();

            while (rsetIter.hasNext()) {
                found = false;
                rObjectName = (ObjectName) rsetIter.next();

                // Get Handler to the Resource MBean
                ResourceMBean rMbean = (ResourceMBean)
                    MBeanServerInvocationHandler.newProxyInstance(mbs,
                        rObjectName, ResourceMBean.class, false);


                // Find the property name in Extension Properties
                List extProps = rMbean.getExtProperties();
                Iterator extPropIter = extProps.iterator();

                while (extPropIter.hasNext() && !found) {
                    ResourceProperty rp = (ResourceProperty) extPropIter.next();

                    if (isPropertyValueEqual(rp, propertyName, propertyValue)) {
                        found = true;

                        if (resourceList == null) {
                            resourceList = new ArrayList();
                        }

                        resourceList.add(rMbean.getName());
                    }
                }

                // Find the property name in System Properties
                if (!found) {
                    List sysProps = rMbean.getSystemProperties();
                    Iterator sysPropIter = sysProps.iterator();

                    while (sysPropIter.hasNext() && !found) {
                        ResourceProperty rp = (ResourceProperty) sysPropIter
                            .next();

                        if (isPropertyValueEqual(rp, propertyName,
                                    propertyValue)) {
                            found = true;

                            if (resourceList == null) {
                                resourceList = new ArrayList();
                            }

                            resourceList.add(rMbean.getName());
                        }
                    }
                }
            }
        }

        return resourceList;
    }


    /**
     * This method retrieves the names of all HASP Resources that have
     * FileSystemMountPoint containing the propertyValue
     *
     * @param  mbs MBeanServer
     * @param  query Query to run
     * @param  dir
     *
     * @return  List of Matching Resources
     */
    public static List getHASPResListContainingDir(MBeanServer mbs,
        QueryExp query, String dir) {

        // Get the ObjectName Factory for RGM CMASS Module
        List resourceList = null;

        ObjectNameFactory rgmOnf = new ObjectNameFactory(ResourceMBean.class
                .getPackage().getName());

        ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                ResourceMBean.class);

        Set resources = mbs.queryNames(rObjectName, query);

        boolean found = false;

        ResourceMBean resource = null;
        Iterator iterator = resources.iterator();

        while (iterator.hasNext()) {
            found = false;
            rObjectName = (ObjectName) iterator.next();

            // Get Handler to the Resource MBean
            resource = (ResourceMBean) MBeanServerInvocationHandler
                .newProxyInstance(mbs, rObjectName, ResourceMBean.class, false);

            // Find the property name in Extension Properties
            List extProps = resource.getExtProperties();
            Iterator extPropIter = extProps.iterator();

            while (extPropIter.hasNext() && !found) {
                ResourceProperty rp = (ResourceProperty) extPropIter.next();
                String entry = rp.getName().trim();

                if (entry.equals(Util.HASP_ALL_FILESYSTEMS)) {
                    found = true;

                    if (rp instanceof ResourcePropertyStringArray) {
                        ResourcePropertyStringArray rpStrArr =
                            (ResourcePropertyStringArray) rp;
                        String rpArr[] = rpStrArr.getValue();

                        for (int i = 0; i < rpArr.length; i++) {

                            if (dir.startsWith(rpArr[i])) {

                                if (resourceList == null) {
                                    resourceList = new ArrayList();
                                }

                                resourceList.add(resource.getName());

                                break;
                            }
                        }
                    }
                }
            }
        }

        return resourceList;
    }


    /**
     * This method retrieves the names of all Resources with a particular
     * Extension or System propertyName set to a propertyValue.
     *
     * @param  mbs MBeanServer
     * @param  query Query to run
     * @param  propertyName  PropertyName
     * @param  propertyValue  PropertyValue
     *
     * @return  List of Matching Resources
     */
    public static List getResourceList(MBeanServer mbs, QueryExp query,
        String propertyName, Object propertyValue) {
        List resourceList = null;

        // Get the ObjectName Factory for RGM CMASS Module
        ObjectNameFactory rgmOnf = new ObjectNameFactory(ResourceMBean.class
                .getPackage().getName());

        ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                ResourceMBean.class);

        Set resources = mbs.queryNames(rObjectName, query);

        boolean found = false;


        ResourceMBean resource = null;
        Iterator i = resources.iterator();

        while (i.hasNext()) {
            found = false;
            rObjectName = (ObjectName) i.next();

            // Get Handler to the Resource MBean
            resource = (ResourceMBean) MBeanServerInvocationHandler
                .newProxyInstance(mbs, rObjectName, ResourceMBean.class, false);

            // Find the property name in Extension Properties
            List extProps = resource.getExtProperties();
            Iterator extPropIter = extProps.iterator();

            while (extPropIter.hasNext() && !found) {
                ResourceProperty rp = (ResourceProperty) extPropIter.next();

                if (isPropertyValueEqual(rp, propertyName, propertyValue)) {

                    if (resourceList == null) {
                        resourceList = new ArrayList();
                    }

                    found = true;
                    resourceList.add(resource.getName());
                }
            }

            // Find the property name in System Properties
            if (!found) {
                List sysProps = resource.getSystemProperties();
                Iterator sysPropIter = sysProps.iterator();

                while (sysPropIter.hasNext() && !found) {
                    ResourceProperty rp = (ResourceProperty) sysPropIter.next();

                    if (isPropertyValueEqual(rp, propertyName, propertyValue)) {
                        found = true;

                        if (resourceList == null) {
                            resourceList = new ArrayList();
                        }

                        resourceList.add(resource.getName());
                    }
                }
            }
        } // while

        return resourceList;
    }

    /**
     * Checks for a Property Name and Value against a ResourceProperty Object
     *
     * @param  rp  ResourcePropertyObject
     * @param  propertyName  PropertyName to check
     * @param  propertyValue  PropertyValue to check
     */
    public static boolean isPropertyValueEqual(ResourceProperty rp,
        String propertyName, Object propertyValue) {


        String entry = rp.getName().trim();

        if (entry.equals(propertyName)) {

            // If Boolean Property
            if (rp instanceof ResourcePropertyBoolean) {
                ResourcePropertyBoolean rpBool = (ResourcePropertyBoolean) rp;
                Boolean value = (Boolean) propertyValue;

                if (value.equals(rpBool.getValue())) {
                    return true;
                }
            }

            // If Integer Property
            if (rp instanceof ResourcePropertyInteger) {
                ResourcePropertyInteger rpInt = (ResourcePropertyInteger) rp;
                Integer value = (Integer) propertyValue;

                if (value.equals(rpInt.getValue())) {
                    return true;
                }
            }

            // If Enum Property
            if (rp instanceof ResourcePropertyEnum) {
                ResourcePropertyEnum rpEnum = (ResourcePropertyEnum) rp;

                if (rpEnum.getEnumList().containsAll((List) propertyValue)) {
                    return true;
                }
            }

            // If String Property
            if (rp instanceof ResourcePropertyString) {
                ResourcePropertyString rpStr = (ResourcePropertyString) rp;
                String value = (String) propertyValue;

                if (value.equals(rpStr.getValue())) {
                    return true;
                }
            }

            // If String[] Property
            if (rp instanceof ResourcePropertyStringArray) {
                ResourcePropertyStringArray rpStrArr =
                    (ResourcePropertyStringArray) rp;
                String valueArr[] = (String[]) propertyValue;
                String matchArr[] = rpStrArr.getValue();

                for (int i = 0; i < matchArr.length; i++) {
                    String match = matchArr[i];

                    for (int j = 0; j < valueArr.length; j++) {
                        String value = valueArr[j];

                        if (value.equals(match)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }


    /**
     * Get the property value corresponding to given resource name and extension
     * property name.
     *
     * @param  mbs  MBean Server
     * @param  rsName  Resource Name
     * @param  extPropName Extension Property Name
     */
    public static Object getResourcePropertyValue(MBeanServer mbs,
        String rsName, String extPropName) {
        ObjectNameFactory rgmOnf = null;
        List rExtProps = null;
        Object retObj = null;

        try {
            rgmOnf = new ObjectNameFactory(ResourceMBean.class.getPackage()
                    .getName());

            ObjectName rObjectName = rgmOnf.getObjectName(ResourceMBean.class,
                    rsName);
            ResourceMBean resMBean = (ResourceMBean)
                MBeanServerInvocationHandler.newProxyInstance(mbs, rObjectName,
                    ResourceMBean.class, false);

            rExtProps = resMBean.getExtProperties();

            Iterator iterExtProps = rExtProps.iterator();

            while (iterExtProps.hasNext()) {
                ResourceProperty property = (ResourceProperty) iterExtProps
                    .next();

                if (property.getName().equals(extPropName)) {
                    retObj = getResourcePropertyValue(property, false);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            return retObj;
        }

    }

    /**
     * Retrieve the value from a ResourceProperty Object
     *
     * @param  rp  ResourceProperty whose value is needed
     * @param  def  Whether the default value is needed
     *
     * @return  value of the ResourceProperty, default value if default is set
     * to true.
     */
    public static Object getResourcePropertyValue(ResourceProperty rp,
        boolean def) {

        // If Boolean Property
        if (rp instanceof ResourcePropertyBoolean) {
            ResourcePropertyBoolean rpBool = (ResourcePropertyBoolean) rp;

            if (def) {
                return rpBool.getDefaultValue();
            } else {
                return rpBool.getValue();
            }
        }

        // If Integer Property
        if (rp instanceof ResourcePropertyInteger) {
            ResourcePropertyInteger rpInt = (ResourcePropertyInteger) rp;

            if (def) {
                return rpInt.getDefaultValue();
            } else {
                return rpInt.getValue();
            }
        }

        // If Enum Property
        if (rp instanceof ResourcePropertyEnum) {
            ResourcePropertyEnum rpEnum = (ResourcePropertyEnum) rp;

            if (def) {
                return rpEnum.getDefaultValue();
            } else {
                return rpEnum.getValue();
            }
        }

        // If String Property
        if (rp instanceof ResourcePropertyString) {
            ResourcePropertyString rpStr = (ResourcePropertyString) rp;

            if (def) {
                return rpStr.getDefaultValue();
            } else {
                return rpStr.getValue();
            }
        }

        // If String[] Property
        if (rp instanceof ResourcePropertyStringArray) {
            ResourcePropertyStringArray rpStrArr = (ResourcePropertyStringArray)
                rp;

            if (def) {
                return rpStrArr.getDefaultValue();
            } else {
                return rpStrArr.getValue();
            }
        }

        return null;
    }


    /**
     * Get the property value corresponding to given resource name and extension
     * property name.
     *
     * @param  mbs  MBean Server
     * @param  rsName  Resource Name
     * @param  extPropName Extension Property Name
     */
    public static Object getResourceExtPropertyValue(MBeanServer mbs,
        String rsName, String extPropName) {
        ObjectNameFactory rgmOnf = null;
        List rExtProps = null;
        Object retObj = null;

        try {
            rgmOnf = new ObjectNameFactory(ResourceMBean.class.getPackage()
                    .getName());

            ObjectName rObjectName = rgmOnf.getObjectName(ResourceMBean.class,
                    rsName);
            ResourceMBean resMBean = (ResourceMBean)
                MBeanServerInvocationHandler.newProxyInstance(mbs, rObjectName,
                    ResourceMBean.class, false);

            rExtProps = resMBean.getExtProperties();

            Iterator iterExtProps = rExtProps.iterator();

            while (iterExtProps.hasNext()) {
                ResourceProperty property = (ResourceProperty) iterExtProps
                    .next();

                if (property.getName().equals(extPropName)) {
                    retObj = getResourcePropertyValue(property, false);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            return retObj;
        }

    }


    /**
     * Gets the ResourceProperty for an Extension or System Property From the
     * RTR file for a give resource type
     *
     * @param  rType  Type of the Resource
     * @param  propertyName  Name of the Property
     *
     * @return  ResourceProperty for the property
     */
    public static ResourceProperty getRTRProperty(MBeanServer mbs, String rType,
        String propertyName) {


        ResourceTypeMBean rtMbean = getResourceTypeMBean(mbs, rType, false);

        if (rtMbean == null) {
            return null;
        }

        // Get the System and Extension Properties
        List sysPropertiesList = rtMbean.getSystemProperties();
        List extPropertiesList = rtMbean.getExtProperties();

        Iterator sysIter = sysPropertiesList.iterator();
        Iterator extIter = extPropertiesList.iterator();

        // Search for our property in System Properties
        while (sysIter.hasNext()) {
            ResourceProperty rp = (ResourceProperty) sysIter.next();

            if (rp.getName().equals(propertyName)) {
                return rp;
            }
        }

        // Search for our property in Extension Properties
        while (extIter.hasNext()) {
            ResourceProperty rp = (ResourceProperty) extIter.next();

            if (rp.getName().equals(propertyName)) {
                return rp;
            }
        }

        return null;
    }

    /**
     * Gets the Default Value for an Extension or System Property From the RTR
     * file for a give resource type
     *
     * @param  mbs  MBeanServer
     * @param  rType  Type of the Resource
     * @param  propertyName  Name of the Property
     *
     * @return  defaultValue for the property
     */
    public static Object getDefaultRTRValue(MBeanServer mbs, String rType,
        String propertyName) {

        ResourceTypeMBean rtMbean = getResourceTypeMBean(mbs, rType, false);

        if (rtMbean == null) {
            return null;
        }

        // Get the System and Extension Properties
        List sysPropertiesList = rtMbean.getSystemProperties();
        List extPropertiesList = rtMbean.getExtProperties();

        Iterator sysIter = sysPropertiesList.iterator();
        Iterator extIter = extPropertiesList.iterator();

        // Search for our property in System Properties
        while (sysIter.hasNext()) {
            ResourceProperty rp = (ResourceProperty) sysIter.next();

            if (rp.getName().equals(propertyName)) {
                return getResourcePropertyValue(rp, true);
            }
        }

        // Search for our property in Extension Properties
        while (extIter.hasNext()) {
            ResourceProperty rp = (ResourceProperty) extIter.next();

            if (rp.getName().equals(propertyName)) {
                return getResourcePropertyValue(rp, true);
            }
        }

        return null;
    }


    /**
     * Gets the resource type from a given resource name
     *
     * @param  mbs  MBeanServer
     * @param  rsName  Name of the Cluster Resource 
     *
     * @return  String of Cluster Resource Type 
     */
    public static String getResourceType(MBeanServer mbs, String rsName) {


        try {
            ObjectNameFactory rgmOnf = new ObjectNameFactory(
                ResourceMBean.class.getPackage().getName());
            ObjectName rObjectName = rgmOnf.getObjectName(
                ResourceMBean.class, rsName);
            ResourceMBean resMBean = (ResourceMBean)
                MBeanServerInvocationHandler.newProxyInstance(
                    mbs, 
                    rObjectName,
                    ResourceMBean.class, 
                    false);

            if (resMBean != null) {
                return resMBean.getType();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        return null;
    }

    /**
     * Returns a list of IPMP Groups in exclusive IP zones
     *
     * @param  hostnames  Hostnames for whom IPMP Groups should be discovered.
     * @param  nodeKey zone name in the form "node:zone",
     *			    for whom IPMP Groups should be discovered.
     * @return  List of IPMPGroups found in the form "group@node:zone".
     *		Returns NULL if none.
     */
    public static List getIPMPGroupsForHostsinZone(
	String hostnames[], String nodeKey, ZoneAdapterMBean mbean) {

        // IPMPGroup List
        List ipmpListonHost = null;
        List ipmpList = null;
        HashSet ipmpSet = null;

        for (int i = 0; i < hostnames.length; i++) {
	    String hostname = hostnames[i];

	    boolean foundFlag = true;

	    AdapterData[] adapters = null;
	    try {
		adapters = mbean.readData(nodeKey, true);
	    } catch (Exception e) {
		return null;
	    }

	    for (int k = 0; k < adapters.length; k++) {

		// The mbean returns only plumbed (configured) adapters
		// so there is no need to check to see if it's
		// plumbed

		AdapterData adapter = adapters[k];
		String ipmpGroup = adapter.group;

		if (ipmpGroup == null) {
		    continue;
		}

		// The adapter may have more than one subnet
		// configured.  Check each subnet to see if any of
		// them qualify.
		for (int l = 0; l < adapter.ipMasks.length; l++) {

		    // Get the NetMask and BroadCast Addresses
		    String ipMask = adapter.ipMasks[l];
		    String ipNet = adapter.ipNets[l];

		// Check whether the adapter can host the hostname
		try {

		    // Get the IPVersion of the hostname
		    InetAddress hostAddress = InetAddress.getByName(
			hostname);

		    InetAddress maskAddr = InetAddress.getByName(
			ipMask);
		    InetAddress netAddr = InetAddress.getByName(
			ipNet);

		    // If Adapter Instance is IPv4 and Hostname is also IPv4
		    if ((hostAddress instanceof Inet4Address) &&
			(!adapter.isIPv6)) {
			int net = convertByteToInt(netAddr.getAddress());
			int mask = convertByteToInt(maskAddr.getAddress());
			int br = (net | (~mask));

			if (!subnet_match_v4(mask, br, hostname)) {
			    foundFlag = false;
			} else {
			    foundFlag = true;
			}
		    } else if ((hostAddress instanceof Inet6Address) &&
			(adapter.isIPv6)) {

			// If Adapter Instance is IPv6 and Hostname
			// is also Ipv6. Need to check this case
			// thoroughly
			long net = convertByteToLong(netAddr.getAddress());
			long mask = convertByteToLong(maskAddr.getAddress());
			long br = (net | (~mask));

			if (!subnet_match_v6(mask, br, hostname)) {
			    foundFlag = false;
			} else {
			    foundFlag = true;
			}
		    }
		    // Could not find an Adapter with the suitable IPVersion
		    else {
			foundFlag = false;
		    }
		} catch (UnknownHostException uhe) {

			// Shouldn't really happen
			foundFlag = false;
			System.out.println(uhe.getMessage());
		    }

		    if (foundFlag) {

			if (ipmpListonHost == null) {
			    ipmpListonHost = new ArrayList();
			}

			ipmpGroup = ipmpGroup + "@" + nodeKey;

			if (!ipmpListonHost.contains(ipmpGroup)) {
			    ipmpListonHost.add(ipmpGroup);
			}
		    }
		}
	    }

	    if (ipmpListonHost != null) {

		if (ipmpSet == null) {
		    ipmpSet = new HashSet();
		}

		ipmpSet.addAll(ipmpListonHost);
	    } else {

		// If no adapter was found suitable for a hostname
		// then this is a problem. null must be handled
		// by the caller
		return null;
	    }
	}

        if (ipmpSet != null) {
            ipmpList = new ArrayList(ipmpSet);
        }

        return ipmpList;
    }

    /**
     * Returns a list of IPMP Groups that can host the given hostname list
     * on the cluster.
     *
     * @param  hostnames  Hostnames for whom IPMP Groups should be discovered.
     * @param  nodeAdapterMBeans  MBeans for adapter ObjectNames from a node.
     *
     * @return  List of IPMPGroups that can host this hostname. Returns NULL if
     * none.
     */
    public static List getIPMPGroupForHost(String hostnames[],
        List nodeAdapterMBeans) {

        // IPMPGroup List
        List ipmpListonHost = null;
        List ipmpList = null;
        HashSet ipmpSet = null;

        for (int i = 0; i < hostnames.length; i++) {
            boolean foundFlag = true;
            Iterator nodeAdapterIter = nodeAdapterMBeans.iterator();
            ipmpListonHost = null;

            while (nodeAdapterIter.hasNext()) {

                // Get Handler to the NodeAdapter Mbean
                NodeAdapterMBean nodeAdapterMbean = (NodeAdapterMBean)
                    nodeAdapterIter.next();

                if (!nodeAdapterMbean.isPlumbed()) {
                    continue;
                }

                String ipmpGroup = nodeAdapterMbean.getIpmpGroup();
                String nodeName = nodeAdapterMbean.getNodeName();

                if (ipmpGroup == null) {
                    continue;
                }

                // Get the NetMask and BroadCast Addresses
                String broadCastAddr = nodeAdapterMbean.getBroadcast();
                String netMask = nodeAdapterMbean.getNetmask();

                // Check whether the adapter can host all the hostnames
                try {

                    // Get the IPVersion of the hostname
                    InetAddress hostAddress = InetAddress.getByName(
                            hostnames[i]);


                    // If Adapter Instance is IPv4 and Hostname is also IPv4
                    if ((hostAddress instanceof Inet4Address) &&
                            nodeAdapterMbean.isFlagIPV4()) {

                        if (!subnet_match_v4(netMask, broadCastAddr,
			    hostnames[i])) {

                            foundFlag = false;
                        } else {
                            foundFlag = true;
                        }
                    } else if ((hostAddress instanceof Inet6Address) &&
			nodeAdapterMbean.isFlagIPV6()) {

			// If Adapter Instance is IPv6 and Hostname is also Ipv6
			// Need to check this case thoroughly
                        if (!subnet_match_v6(netMask, broadCastAddr,
			    hostnames[i])) {

                            foundFlag = false;
                        } else {
                            foundFlag = true;
                        }
                    }
                    // Could not find an Adapter with the suitable IPVersion
                    else {
                        foundFlag = false;
                    }
                } catch (UnknownHostException uhe) {

                    // Should'nt really happen
                    foundFlag = false;
                    System.out.println(uhe.getMessage());
                }

                if (foundFlag) {

                    if (ipmpListonHost == null) {
                        ipmpListonHost = new ArrayList();
                    }

                    ipmpGroup = ipmpGroup + "@" + nodeName;

                    if (!ipmpListonHost.contains(ipmpGroup)) {
                        ipmpListonHost.add(ipmpGroup);
                    }
                }
            }

            if (ipmpListonHost != null) {

                if (ipmpSet == null) {
                    ipmpSet = new HashSet();
                }

                ipmpSet.addAll(ipmpListonHost);
            } else {

                // If no adapter was found suitable for a hostname
                // then this is a problem. null must be handled
                // by the caller
                return null;
            }
        }

        if (ipmpSet != null) {
            ipmpList = new ArrayList(ipmpSet);
        }

        return ipmpList;
    }

    /**
     * Can the v4 Adapter instance host the hostname
     *
     * @param  maskAddr  Netmask of the Adapter
     * @param  broadCastAddr  BroadCast IpAddress of the Adapter
     * @param  hostname  Hostname to validate
     *
     * @returns  True if the adapter can host else return false
     */
    private static boolean subnet_match_v4(int maskAddr, int broadCastAddr,
        String hostname) {

        try {
            InetAddress hostAddress = InetAddress.getByName(hostname);

            // Expect host to be v4 Address
            if (hostAddress instanceof Inet4Address) {
                Inet4Address hostIPv4 = (Inet4Address) hostAddress;

                // Get the integer values for the Addresses
                int hostAddr = convertByteToInt(hostIPv4.getAddress());

                // Check for the netmask
                if ((hostAddr & maskAddr) == (broadCastAddr & maskAddr)) {
                    return true;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return false;
    }

    /**
     * Can the v6 Adapter instance host the hostname
     *
     * @param  ipMask  Netmask of the Adapter
     * @param  ipNet  IpAddress of the Adapter
     * @param  hostname  Hostname to validate
     *
     * @returns  True if the adapter can host else return false
     */
    private static boolean subnet_match_v6(long maskAddr, long broadCastAddr,
        String hostname) {

        try {
            InetAddress hostAddress = InetAddress.getByName(hostname);

            // Expect host to be v6 Address
            if (hostAddress instanceof Inet6Address) {
                Inet6Address hostIPv6 = (Inet6Address) hostAddress;

                // Get the integer values for the Addresses
                long hostAddr = convertByteToLong(hostIPv6.getAddress());

                // Check for the netmask
                if ((hostAddr & maskAddr) == (broadCastAddr & maskAddr)) {
                    return true;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return false;
    }

    /**
     * Can the v4 Adapter instance host the hostname
     *
     * @param  ipMask  Netmask of the Adapter
     * @param  ipBroadCast  BroadCast IpAddress of the Adapter
     * @param  hostname  Hostname to validate
     *
     * @returns  True if the adapter can host else return false
     */
    private static boolean subnet_match_v4(String ipMask, String ipBroadCast,
        String hostname) {

        try {
            InetAddress hostAddress = InetAddress.getByName(hostname);
            InetAddress broadCastAddress = InetAddress.getByName(ipBroadCast);

            // Expect host to be v4 Address
            if (hostAddress instanceof Inet4Address) {
                Inet4Address hostIPv4 = (Inet4Address) hostAddress;
                Inet4Address broadCastv4 = (Inet4Address) broadCastAddress;

                // Get the integer values for the Addresses
                int hostAddr = convertByteToInt(hostIPv4.getAddress());
                int broadCastAddr = convertByteToInt(broadCastv4.getAddress());
                int maskAddr = convertByteToInt(getAddress(ipMask));

                // Check for the netmask
                if ((hostAddr & maskAddr) == (broadCastAddr & maskAddr)) {
                    return true;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return false;
    }

    /**
     * Can the v6 Adapter instance host the hostname
     *
     * @param  ipMask  Netmask of the Adapter
     * @param  ipNet  IpAddress of the Adapter
     * @param  hostname  Hostname to validate
     *
     * @returns  True if the adapter can host else return false
     */
    private static boolean subnet_match_v6(String ipMask, String ipBroadCast,
        String hostname) {

        try {
            InetAddress hostAddress = InetAddress.getByName(hostname);
            InetAddress broadCastAddress = InetAddress.getByName(ipBroadCast);

            // Expect host to be v6 Address
            if (hostAddress instanceof Inet6Address) {
                Inet6Address hostIPv6 = (Inet6Address) hostAddress;
                Inet6Address broadCastv6 = (Inet6Address) broadCastAddress;

                // Get the integer values for the Addresses
                long hostAddr = convertByteToLong(hostIPv6.getAddress());
                long broadCastAddr = convertByteToLong(broadCastv6
                        .getAddress());
                long maskAddr = convertByteToLong(getAddress(ipMask));

                // Check for the netmask
                if ((hostAddr & maskAddr) == (broadCastAddr & maskAddr)) {
                    return true;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return false;
    }

    /**
     * Returns the raw IP address of this hexadecimal object. The result is in
     * network byte order: the highest order byte of the address is in <code>
     * getAddress()[0]</code>.
     *
     * @return  the raw IP address of this object.
     */
    public static byte[] getAddress(String hexaAddress) {

        try {
            byte addr[] = new byte[IPv4LENGTH];
            int address = (int) Long.parseLong(hexaAddress.toUpperCase(), 16);
            addr[0] = (byte) ((address >>> 24) & 0xFF);
            addr[1] = (byte) ((address >>> 16) & 0xFF);
            addr[2] = (byte) ((address >>> 8) & 0xFF);
            addr[3] = (byte) (address & 0xFF);

            return addr;
        } catch (NumberFormatException nfe) {
            System.out.println(nfe.getMessage());

            return null;
        }
    }

    /**
     * Converts the byte array into 32 bit int
     *
     * @param  addr  ByteArray to be converted
     *
     * @return  converted int
     */
    public static int convertByteToInt(byte addr[]) {
        int address;

        if (addr.length == IPv4LENGTH) {
            address = addr[3] & 0xFF;
            address |= ((addr[2] << 8) & 0xFF00);
            address |= ((addr[1] << 16) & 0xFF0000);
            address |= ((addr[0] << 24) & 0xFF000000);

            return address;
        } else {
            return 0;
        }
    }

    /**
     * Converts the byte array into long
     *
     * @param  addr  ByteArray to be converted
     *
     * @return  converted long
     */
    public static long convertByteToLong(byte addr[]) {
        long address;

        if (addr.length == IPv6LENGTH) {

            address = addr[5] & 0xFF;
            address |= ((addr[4] << 8) & 0xFF00L);
            address |= ((addr[3] << 16) & 0xFF0000L);
            address |= ((addr[2] << 24) & 0xFF000000L);
            address |= ((addr[1] << 32) & 0xFF00000000L);
            address |= ((addr[0] << 48) & 0xFF0000000000L);

            return address;
        } else {
            return 0;
        }
    }

    /**
     * Checks whether the given ResourceGroup name is already used in the
     * cluster
     *
     * @param  rgName  Name that has to be checked
     *
     * @return  "true" if the name is being used "false" if the name is
     * available
     */
    public static boolean isRGNameUsed(MBeanServer mbs, String rgName) {

        // Get the ObjectName Factory for RGM CMASS Module
        ObjectNameFactory rgmOnf = new ObjectNameFactory(ResourceMBean.class
                .getPackage().getName());

        ObjectName rgObjectName = rgmOnf.getObjectNamePattern(
                ResourceGroupMBean.class);
        QueryExp query = Query.match(new AttributeValueExp("Name"),
                Query.value(rgName));
        Set rgNames;

        try {
            rgNames = mbs.queryNames(rgObjectName, query);
        } catch (Exception e) {
            System.out.println(e.getMessage());

            return false;
        }

        return (rgNames.size() > 0);
    }

    /**
     * Checks whether the given ResourceGroup name is already used in the
     * cluster
     *
     * @param  rgName  Name that has to be checked
     *
     * @return  "true" if the name is being used "false" if the name is
     * available
     */
    public static boolean isRGNameUsed(String rgName) {

        String agentLocation = Util.getClusterEndpoint();
        QueryExp query = Query.match(new AttributeValueExp("Name"),
                Query.value(rgName));
        Set rgNames;

        try {
            rgNames = TrustedMBeanModel.getInstanceNames(agentLocation,
                    ResourceGroupMBean.class, query);
        } catch (Exception e) {
            System.out.println(e.getMessage());

            return false;
        }

        return (rgNames.size() > 0);
    }

    /**
     * Checks whether the given Resource name is already used in the cluster
     *
     * @param  rName  Name that has to be checked
     *
     * @return  "true" if the name is being used "false" if the name is
     * available
     */
    public static boolean isResourceNameUsed(MBeanServer mbs, String rName) {

        // Get the ObjectName Factory for RGM CMASS Module
        ObjectNameFactory rgmOnf = new ObjectNameFactory(ResourceMBean.class
                .getPackage().getName());

        ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                ResourceMBean.class);

        QueryExp query = Query.match(new AttributeValueExp("Name"),
                Query.value(rName));
        Set rNames;

        try {
            rNames = mbs.queryNames(rObjectName, query);
        } catch (Exception e) {
            System.out.println(e.getMessage());

            return false;
        }

        return (rNames.size() > 0);
    }

    /**
     * Checks whether the given Resource name is already used in the cluster
     *
     * @param  rName  Name that has to be checked
     *
     * @return  "true" if the name is being used "false" if the name is
     * available
     */
    public static boolean isResourceNameUsed(String rName) {

        String agentLocation = Util.getClusterEndpoint();
        QueryExp query = Query.match(new AttributeValueExp("Name"),
                Query.value(rName));
        Set rNames;

        try {
            rNames = TrustedMBeanModel.getInstanceNames(agentLocation,
                    ResourceMBean.class, query);
        } catch (Exception e) {
            System.out.println(e.getMessage());

            return false;
        }

        return (rNames.size() > 0);
    }

    /**
     * Returns the zone export path given the nodename.
     *
     * @param  mbs  MBean Server
     * @param  nodeName  node name of the form nodename:zonename
     *
     * @return  zone export path
     */
    public static String getZoneExportPath(MBeanServer mbs, String nodeName) {
        String zone = null;
        String zoneExportPath = null;

        // Get the zone path
        try {

            if (nodeName != null) {
                ObjectNameFactory zoneOnf = new ObjectNameFactory(
                        ZoneMBean.class.getPackage().getName());
                ObjectName zoneNamePattern = zoneOnf.getObjectNamePattern(
                        ZoneMBean.class);
                QueryExp query = Query.match(new AttributeValueExp("Name"),
                        new StringValueExp(nodeName));
                Set zoneMBeans = mbs.queryNames(zoneNamePattern, query);

                if ((zoneMBeans != null) && (zoneMBeans.size() > 0)) {
                    Iterator setIterator = zoneMBeans.iterator();
                    ObjectName zoneObjectName = (ObjectName) setIterator.next();
                    ZoneMBean zoneMbean = (ZoneMBean)
                        MBeanServerInvocationHandler.newProxyInstance(mbs,
                            zoneObjectName, ZoneMBean.class, false);

                    if (nodeName.split(Util.COLON).length == 2) {
                        zone = (nodeName.split(Util.COLON))[1];

                        ExitStatus status[] = zoneMbean.getZoneDetails(zone);

                        if ((status.length > 0) &&
                                (status[0].getReturnCode().intValue() ==
                                    status[0].SUCCESS)) {
                            List output = status[0].getOutStrings();

                            // Value returned is of the form
                            // zoneid:zonename:status:path
                            if ((output != null) && (output.size() > 0)) {
                                String outLine = (String) output.get(0);
                                String zoneDetails[] = outLine.split(
                                        Util.COLON);

                                // The solaris command output is parsed
                                // for the zoneExportPath
                                if ((zoneDetails != null) &&
                                        (zoneDetails.length ==
                                            ZONEDETAIL_LEN)) {
                                    zoneExportPath =
                                        zoneDetails[ZONEPATH_INDEX];
                                }
                            }
                        }
                    }
                }
            }
        } catch (CommandExecutionException cee) {
        }

        return zoneExportPath;
    }

    /**
     * Returns a list of common elements from two given lists.
     *
     * @param  list1  First List
     * @param  list2  Second List
     *
     * @return  list of common elements
     */
    public static List getCommonElements(List list1, List list2) {
        List resultList = new ArrayList();
        Iterator iterList1 = list1.iterator();
        String item = null;

        while (iterList1.hasNext()) {
            item = (String) iterList1.next();

            if (list2.contains(item))
                resultList.add(item);
        }

        return resultList;
    }

    /*
     * This code will return the RT name of the latest RT, when
     * multiple RTs of the same type are present with different
     * versions. This code assumes that the list of MBeans
     * that are being passed are the same RT but just the versions
     * are different.
     */
    public static String getLatestRTVersion(List resourceTypeMBeans) {

        String latestVersion = null;
        String curVersion = null;
        String zRtName = null;
        String newRtName = null;

        if (resourceTypeMBeans == null)
            return null;

        for (int i = 0; i < resourceTypeMBeans.size(); i++) {
            ResourceTypeMBean rtMBean = (ResourceTypeMBean) resourceTypeMBeans
                .get(i);
            zRtName = rtMBean.getName();
            curVersion = rtMBean.getVersion();

            if (curVersion == null) {
                continue;
            }

            if (latestVersion == null) {
                latestVersion = rtMBean.getVersion();

                continue;
            }

            latestVersion = greaterVersion(latestVersion, curVersion);
        }

        if ((zRtName != null) && (latestVersion != null)) {
            String splitRTName[] = zRtName.split(Util.COLON);
            StringBuffer newRT = new StringBuffer(splitRTName[0]);
            newRT.append(Util.COLON);
            newRT.append(latestVersion);
            newRtName = newRT.toString();
        }

        return newRtName;
    }

    /*
     * Compare two version strings of format a.b.c and d.e.f
     * b, c and e, f are optional. There could be versions
     * of the form 1 and 1.1
     */
    private static String greaterVersion(String currentVer, String challenger) {
        String greaterVer = null;
        String biggerVersionString = null;

        if ((currentVer == null) || (challenger == null)) {
            return null;
        }

        String curVerArr[] = currentVer.split(Util.DOT);
        String newVerArr[] = challenger.split(Util.DOT);
        int iterLen = 0;

        // Iterate only till the smallest number of DOTs
        // Store which version string contains the largest
        // number of positions
        if (curVerArr.length <= newVerArr.length) {
            iterLen = curVerArr.length;
            biggerVersionString = challenger;
        } else {
            iterLen = newVerArr.length;
            biggerVersionString = currentVer;
        }

        // The loop will continue only when the version
        // number in the same position are equal
        try {

            for (int y = 0; y < iterLen; y++) {

                if (Integer.parseInt(curVerArr[y]) >
                        Integer.parseInt(newVerArr[y])) {
                    greaterVer = currentVer;

                    break;
                } else if (Integer.parseInt(newVerArr[y]) >
                        Integer.parseInt(curVerArr[y])) {
                    greaterVer = challenger;

                    break;
                }
            }
        } catch (NumberFormatException nume) {
            return null;
        }

        // If all the positions till now are equal, then
        // the version with the largest number of positions
        // is the greatest. Example 1.1 and 1.1.3
        if (greaterVer == null) {
            greaterVer = biggerVersionString;
        }

        return greaterVer;
    }

    /**
     * Executes the Clear Command to clear the screen
     */
    public static native void clearScreen();

    /**
     * Checks if there are any unacceptable character in the mountpoint
     *
     * @param  mountPoint  that has to be checked
     *
     * @return  "true" if the mountPoint doesnt have any commas "false" if there
     * are any commas
     */
    public static boolean isValidMountPoint(String mountPoint) {

        boolean retVal = true;

        // check for comma
        retVal = (mountPoint.indexOf(Util.COMMA) == -1);

        // check for proper use of space
        String split_str[] = mountPoint.split(Util.SPACE);

        for (int i = 0; i < (split_str.length - 1); i++) {

            if (split_str[i].equals("") ||
                    !split_str[i].endsWith(Util.BACKSLASH)) {
                retVal = false;

                break;
            }
        }

        return retVal;

    }

    /**
     * Checks if there are any unacceptable character in the path and remove
     * them
     *
     * @param  path  that has to be checked
     *
     * @return  String with all the special characters removed.
     */
    public static String removeSpecialChars(String path) {

        // Remove all special characters since RG/RS Name cannot have special
        // have special characters other than '-' and '_'.
        if (!Pattern.compile("[\\W&&[^-_]]*").matcher(path).matches()) {
            path = path.replaceAll("[\\W&&[^-_]]", "");
        }

        return (path);
    }

   
    public static HashMap getFsMountMap(MBeanServer mbs, String[] nodeList) {

        // get hashmap of available filesystem mountpoints in /etc/vfstab
        ObjectNameFactory dsOnf = new ObjectNameFactory(Util.DOMAIN);
        HAStoragePlusMBean haspMBean = (HAStoragePlusMBean)
            MBeanServerInvocationHandler.newProxyInstance(mbs,
                dsOnf.getObjectName(HAStoragePlusMBean.class, null),
                HAStoragePlusMBean.class, false);
        HashMap fsMntMap = haspMBean.obtainMountpointFSTypes(nodeList);

        return fsMntMap;
    }
}
