/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)Infrastructure.java	1.24	08/12/03 SMI"
 */
package com.sun.cluster.agent.dataservices.utils;

// J2SE
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// JMX
import javax.management.AttributeValueExp;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;

// CACAO
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// CMAS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.*;
import com.sun.cluster.agent.rgm.ResourceGroupMBean;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.common.TrustedMBeanModel;

/**
 * Implementation of the Infrastructure MBean
 */
public class Infrastructure implements InfrastructureMBean {

    private MBeanServer mBeanServer = null;
    private ObjectNameFactory onf = null;

    private static int CZ_INDEX = 0;
    private static int R_INDEX = 1;

    /**
     * Creates a new instance of Infrastructure for the mBeanServer
     */
    public Infrastructure(MBeanServer mbs, ObjectNameFactory onf) {
        this.mBeanServer = mbs;
        this.onf = onf;
    }

    /**
     * Executes the command specified by the array
     *
     * @param  command  Command to be executed eg : command[0] = "ls" command[1]
     * = "-a" executes ls -a
     */
    public ExitStatus[] executeCommand(String command[])
        throws CommandExecutionException {
        InvocationStatus result[] = null;
        ExitStatus resultWrapper[] = null;

        try {
            result = InvokeCommand.execute(new String[][] { command }, null);
            resultWrapper = ExitStatus.createArray(result);
        } catch (InvocationException ie) {
            result = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(result));
        }

        return resultWrapper;
    }

    /**
     * Execute an operation on the specified MBean on a domain with the given
     * parameters. The method will search only in the Dataservices Domain.
     *
     * @param  mbeanInterface  Name of the MBean
     * @param  opName  OperationName
     * @param  paramTypes  String[] of ParamTypes
     * @param  paramValues  Object[] of ParamValues
     */
    public Object executeOperation(Class mbeanInterface, String opName,
        String paramTypes[], Object paramValues[]) throws Exception {

        try {
            ObjectName objName = onf.getObjectName(mbeanInterface, null);

            return mBeanServer.invoke(objName, opName, paramValues, paramTypes);
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Serialize objecr to a file in the cluster node
     *
     * @param  fileName  Name of the file
     * @param  object  Object to be written
     */
    public void writeObject(String fileName, Object object) throws IOException {

        // Serialize the object
        ObjectOutputStream objectOutput = null;

        try {
            objectOutput = new ObjectOutputStream(new FileOutputStream(
                        fileName));
            objectOutput.writeObject(object);
        } catch (IOException e) {
            throw e;
        } finally {

            try {
                objectOutput.close();
            } catch (Exception e) {
                throw new IOException(e.toString());
            }
        }
    }

    /**
     * Checks whether a particular package has been installed on all the cluster
     * nodes. The array of nodes without the package is returned.
     *
     * @param  nodeList  Array of Nodes on which the package has to be checked
     * @param  servicePkg  Name of the Package to be checked
     *
     * @return  Array of nodes that do not have the required Package
     */
    public String[] checkPackages(String nodeList[], String servicePkg) {
        List commandList = new ArrayList();
        ExitStatus exitStatusArr[] = null;
        List errorNodes = new ArrayList();

        try {

            // Construct the PackageInfo Command
            commandList.add("/usr/bin/pkginfo");
            commandList.add("-i");
            commandList.add(servicePkg);

            String commands[] = new String[commandList.size()];
            commands = (String[]) commandList.toArray(commands);

            // We connect to every node's InfrastructureMBean and
            // Check whether the package is installed
            Map env = new HashMap();
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                InfrastructureMBean.class.getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());

            JMXConnector localConnection = TrustedMBeanModel
                .getWellKnownConnector(Util.getClusterEndpoint(), env);

            for (int i = 0; i < nodeList.length; i++) {

                // Check package on ith Node
                try {
                    JMXConnector connector = TrustedMBeanModel
                        .getWellKnownConnector(Util.getNodeEndpoint(
                                localConnection, nodeList[i]), env);
                    InfrastructureMBean infrastructureMBeanOnNode =
                        (InfrastructureMBean) TrustedMBeanModel.getMBeanProxy(
                            Util.DOMAIN, connector, InfrastructureMBean.class,
                            null, false);
                    exitStatusArr = infrastructureMBeanOnNode.executeCommand(
                            commands);

                    ExitStatus exitStatus = exitStatusArr[0];

                    if (exitStatus.getReturnCode().intValue() != 0) {

                        // NON-Zero Error Status, add this node
                        // as a node with improper package
                        errorNodes.add(nodeList[i]);
                    }
                } catch (Exception e) {

                    // NON-Zero Error Status or some exception, add this node
                    // as a node with improper package
                    errorNodes.add(nodeList[i]);
                }
            }
        } catch (Exception e) {
            // Shouldnt Happen
        }

        String errorNodesArr[] = new String[errorNodes.size()];
        errorNodesArr = (String[]) errorNodes.toArray(errorNodesArr);

        return errorNodesArr;
    }

    public String[] checkMBeans(String nodeList[], Class mBean)
        throws Exception {

        JMXConnector connector = null;
        JMXConnector localConnector = null;
        List errorNodes = null;

        // Get connector for the node on which this mbean is instantiated.
        Map env = new HashMap();
        env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
            this.getClass().getClassLoader());
        env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
            this.getClass().getClassLoader());
        localConnector = TrustedMBeanModel.getWellKnownConnector(Util
                .getClusterEndpoint(), env);

        for (int i = 0; i < nodeList.length; i++) {

            String nodeToCheck = (nodeList[i].split(Util.COLON))[0];

            try {
                Object mBeanRef = null;
                connector = TrustedMBeanModel.getWellKnownConnector(Util
                        .getNodeEndpoint(localConnector, nodeToCheck), env);
                mBeanRef = TrustedMBeanModel.getMBeanProxy(Util.DOMAIN,
                        connector, mBean, null, false);

                if (mBeanRef == null)
                    throw new Exception();
            } catch (Exception e) {
                if (errorNodes == null) {
                    errorNodes = new ArrayList();
                }

                if (!errorNodes.contains(nodeToCheck)) {
                    errorNodes.add(nodeToCheck);
                }
            }
        }

        if (errorNodes == null) {
            return null;
        } else {
            return (String[]) errorNodes.toArray(new String[errorNodes.size()]);
        }
    }

    public String[] checkCacaoagent(String nodeList[]) throws Exception {
        return checkMBeans(nodeList, InfrastructureMBean.class);
    }


    // Implementation For the ServiceConfig Interface Methods

    public String[] getDiscoverableProperties() {
        return null;
    }

    public String[] getAllProperties() {
        return null;
    }

    public HashMap discoverPossibilities(String nodeList[],
        String propertyNames[], Object helperData) {
        return null;
    }

    public ErrorValue validateInput(String propNames[], HashMap userInputs,
        Object helperData) {
        return null;
    }

    public ErrorValue applicationConfiguration(Object helperData) {
        return null;

    }

    public String[] aggregateValues(String propertyName, Map unfilteredValues,
        Object helperData) {
        return null;
    }

    public HashMap discoverPossibilities(String propertyNames[],
        Object helperData) {

        HashMap resultMap = new HashMap();
        List propList = Arrays.asList(propertyNames);

        if (propList.contains(Util.LH_RSLIST)) {
            String nodeList[] = (String[]) ((HashMap) helperData).get(
                    Util.RG_NODELIST);

            // get a list of Logical Host resources
            // with nodelist set to nodeList
            List resources = null;

            if (nodeList == null) {
                String rgName = (String) ((HashMap) helperData).get(
                        Util.RG_NAME);

                // get all logical host resources in resource group rgName
                QueryExp query = Query.and(Query.match(
                            new AttributeValueExp("Type"),
                            Query.value(Util.LH_RTNAME + "*")),
                        Query.eq(new AttributeValueExp("ResourceGroupName"),
                            Query.value(rgName)));

                // Get the ObjectName Factory for RGM CMASS Module
                ObjectNameFactory rgmOnf = new ObjectNameFactory(
                        ResourceMBean.class.getPackage().getName());

                ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                        ResourceMBean.class);

                Set rset = mBeanServer.queryNames(rObjectName, query);

                if (rset != null) {

                    if (resources == null) {
                        resources = new ArrayList();

                        Iterator i = rset.iterator();

                        while (i.hasNext()) {
                            ObjectName rsObjName = (ObjectName) i.next();
                            String rsName = rgmOnf.getInstanceName(rsObjName);
                            resources.add(rsName + Util.COLON + rgName);
                        }
                    }
                }
            } else {
                resources = DataServicesUtil.getResourceList(mBeanServer,
                        nodeList, Util.LH_RTNAME);
            }

            resultMap.put(Util.LH_RSLIST, resources);
        }

        if (propList.contains(Util.STORAGE_RSLIST)) {
            String nodeList[] = (String[]) ((HashMap) helperData).get(
                    Util.RG_NODELIST);

            // get a list of storage resources
            // with nodelist set to nodeList
            List resources = DataServicesUtil.getResourceList(mBeanServer,
                    nodeList, Util.HASP_RTNAME);
            resources.addAll(DataServicesUtil.getResourceList(mBeanServer,
                    nodeList, Util.QFS_RTNAME));
            resultMap.put(Util.STORAGE_RSLIST, resources);
        }

        if (propList.contains(Util.NODELIST)) {
            resultMap.put(Util.NODELIST,
                DataServicesUtil.getNodeList(mBeanServer));
        }

        if (propList.contains(Util.STORAGE_RGLIST)) {
            String nodeList[] = (String[]) ((HashMap) helperData).get(
                    Util.RG_NODELIST);
            String props[] = new String[] {
                    Util.HASP_ALL_FILESYSTEMS, 
                    Util.HASP_ALL_DEVICES, 
                    Util.QFS_FILESYSTEM
                };
            List rgs = null;

            if (nodeList == null) {
                String rgName = (String) ((HashMap) helperData).get(
                        Util.RG_NAME);

                // get all logical host resources in resource group rgName
                QueryExp query = Query.and(
                       Query.or(Query.match(new AttributeValueExp("Type"),
                            Query.value(Util.HASP_RTNAME + "*")),
                            Query.match(new AttributeValueExp("Type"),
                            Query.value(Util.QFS_RTNAME + "*"))),
                        Query.eq(new AttributeValueExp("ResourceGroupName"),
                            Query.value(rgName)));

                // Get the ObjectName Factory for RGM CMASS Module
                ObjectNameFactory rgmOnf = new ObjectNameFactory(
                        ResourceMBean.class.getPackage().getName());

                ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                        ResourceMBean.class);

                Set rset = mBeanServer.queryNames(rObjectName, query);

                if (rset != null) {

                    if (rgs == null) {
                        rgs = new ArrayList();
                    }

                    Iterator i = rset.iterator();

                    while (i.hasNext()) {
                        ObjectName rsObjName = (ObjectName) i.next();
                        String rsName = rgmOnf.getInstanceName(rsObjName);
                        HashMap map = new HashMap();
                        map.put(Util.RG_NAME, rgName);
                        map.put(Util.RS_NAME, rsName);
                        for (int j = 0; j < props.length; j++) {
                            Object obj = DataServicesUtil
                                .getResourcePropertyValue(mBeanServer, rsName,
                                    props[j]);
                            map.put(props[j], obj);
                        }

                        rgs.add(map);
                    }
                }
            } else {
                List haspRgs = DataServicesUtil.getRGList(mBeanServer, nodeList,
                        Util.HASP_RTNAME, props);
                List qfsRgs = DataServicesUtil.getRGList(mBeanServer, nodeList,
                        Util.QFS_RTNAME, props);
                if (haspRgs != null || qfsRgs != null) {
                    rgs = new ArrayList();
                    if (haspRgs != null) {
                        rgs.addAll(haspRgs);
                    } 
                    if (qfsRgs != null) {
                        rgs.addAll(qfsRgs);
                    } 
                }
            }

            resultMap.put(Util.STORAGE_RGLIST, rgs);
        }

        if (propList.contains(Util.LH_RGLIST)) {
            String nodeList[] = (String[]) ((HashMap) helperData).get(
                    Util.RG_NODELIST);
            String props[] = new String[] { Util.LH_HOSTLIST_EXT_PROP };
            List rgList = null;

            if (nodeList == null) {
                String rgName = (String) ((HashMap) helperData).get(
                        Util.RG_NAME);

                // get all logical host resources in resource group rgName
                QueryExp query = Query.and(Query.match(
                            new AttributeValueExp("Type"),
                            Query.value(Util.LH_RTNAME + "*")),
                        Query.eq(new AttributeValueExp("ResourceGroupName"),
                            Query.value(rgName)));

                // Get the ObjectName Factory for RGM CMASS Module
                ObjectNameFactory rgmOnf = new ObjectNameFactory(
                        ResourceMBean.class.getPackage().getName());

                ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                        ResourceMBean.class);

                Set rset = mBeanServer.queryNames(rObjectName, query);

                if (rset != null) {

                    if (rgList == null) {
                        rgList = new ArrayList();
                    }

                    Iterator i = rset.iterator();

                    while (i.hasNext()) {
                        ObjectName rsObjName = (ObjectName) i.next();
                        String rsName = rgmOnf.getInstanceName(rsObjName);
                        HashMap map = new HashMap();
                        map.put(Util.RG_NAME, rgName);
                        map.put(Util.RS_NAME, rsName);

                        for (int j = 0; j < props.length; j++) {
                            Object obj = DataServicesUtil
                                .getResourcePropertyValue(mBeanServer, rsName,
                                    props[j]);
                            map.put(props[j], obj);
                        }

                        rgList.add(map);
                    }
                }
            } else {
                rgList = DataServicesUtil.getRGList(mBeanServer, nodeList,
                        Util.LH_RTNAME, props);
            }

            resultMap.put(Util.LH_RGLIST, rgList);
        }

        return resultMap;
    }
}
