/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAOracle.java 1.19     08/07/23 SMI"
 */


package com.sun.cluster.agent.dataservices.haoracle;

// J2SE

// CACAO
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.*;
import com.sun.cluster.agent.dataservices.utils.*;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.devicegroup.DeviceGroupMBean;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.agent.rgm.ResourceTypeMBean;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.common.TrustedMBeanModel;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.management.AttributeValueExp;

// JMX
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;


/**
 * This interface defines the management operations available to configure a
 * HAOracle Resource
 */
public class HAOracle implements HAOracleMBean {

    /* The MBeanServer in which this MBean is inserted. */
    private MBeanServer mBeanServer;

    /* The object name factory used to register this MBean */
    private ObjectNameFactory onf = null;

    /* Command Generation Structure for this Dataservice */
    private DataServiceInfo serviceInfo = null;

    /* Command Generation Structure for this Dataservice */
    private ServiceConfig oracleConfiguration = null;

    private String PURPOSE = "oracle";

    private MBeanServerConnection mbsConnections[];
    private JMXConnector connectors[] = null;
    private JMXConnector localConnector = null;

    /* List of commands executed */
    private List commandList = null;

    /**
     * Default constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this MBean.
     */
    public HAOracle(MBeanServer mBeanServer, ObjectNameFactory onf,
        DataServiceInfo serviceInfo) {

        this.mBeanServer = mBeanServer;
        this.onf = onf;
        this.serviceInfo = serviceInfo;

        if (serviceInfo != null) {
            this.oracleConfiguration = serviceInfo.getConfigurationClass(this
                    .getClass().getClassLoader());
        }
    }


    /**
     * Returns the ServiceInfo Structure for this DataserviceMBean
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public DataServiceInfo getServiceInfo() {
        return this.serviceInfo;
    }

    /**
     * Sets the ServiceInfo Structure for this DataserviceMBean
     *
     * @param  serviceInfo  Command Generation Structure
     */
    public void setServiceInfo(DataServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;

        if (serviceInfo != null) {
            this.oracleConfiguration = serviceInfo.getConfigurationClass(this
                    .getClass().getClassLoader());
        }
    }

    /**
     * Returns list of executed command list
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public List getCommandList() {
        return this.commandList;
    }

    /**
     * Generates the set of commands for configuring this service, given the
     * configuration information. This command uses the ServiceInfo structure,
     * fills the same and generates the command list.
     *
     * @param  configurationInformation  ConfigurationInformation for
     * configuring an instance of the Dataservice
     *
     * @return  Commands Generated for the given ConfigurationInformation
     */
    public List generateCommands(Map configurationInformation)
        throws CommandExecutionException {

        ResourceInfo haspRsrc = null;
        ResourceInfo qfsRsrc = null;
        ResourceInfo lhRs = null;
        ResourceInfo oracleServerRs = null;
        ResourceInfo oracleListenerRs = null;
        List dependencyList = null;

        // define a array list to keep track newly created RS object(s)
        ArrayList newRsList = new ArrayList();

        try {
            /*
             * We need to fill HASP and QFS Resources separately - as we could have
             * multiple HASP/QFS resources and command generator handles this in
             * a separate fashion.
             *
             * HASP resources command generation
             * 1. Get ResourceInfo Object from serviceInfo
             * 2. Get copy(s) of the resourceinfo object
             * 3. Fill resource info object(s).
             */

            // Fill resource info object(s)

            Iterator iterkeys = null;
            List storageRSNameList = null;
            String rsName = null;
            List resExtProp = new ArrayList();
            List resSysProp = new ArrayList();

            // HASP/QFS Resources already present on the cluster to be used by
            // HAOracle res

            String nodeList[] = (String[]) configurationInformation.remove(
                    Util.RG_NODELIST);

            storageRSNameList = (ArrayList) configurationInformation.remove(
                    Util.HASP_RES_ID + Util.NAME_TAG);

            HashMap newHaspRS = (HashMap) configurationInformation.remove(
                    Util.NEW_HASP_RS);
            List lhRSNameList = (ArrayList) configurationInformation.remove(
                    Util.LH_RES_ID + Util.NAME_TAG);
            HashMap newLhRS = (HashMap) configurationInformation.remove(
                    Util.NEW_LH_RS);

            serviceInfo.fillServiceInfoStructure(configurationInformation);
            haspRsrc = serviceInfo.getResource(Util.HASP_RES_ID);
            qfsRsrc = serviceInfo.getResource(Util.QFS_RES_ID);

            oracleServerRs = serviceInfo.getResource(Util.ORA_SERVER_RES_ID);
            dependencyList = oracleServerRs.getDependency();
            if (oracleServerRs != null) {
                oracleServerRs.setBringOnline(true);
            }

            oracleListenerRs = serviceInfo.getResource(
                Util.ORA_LISTENER_RES_ID);
            if (oracleListenerRs != null) {
                oracleListenerRs.setBringOnline(true);
            }

            if (storageRSNameList != null) {
                iterkeys = storageRSNameList.iterator();

                while (iterkeys.hasNext()) {

                    ResourceInfo resInfo; // tmp resourceInfo reference
                    rsName = (String) iterkeys.next();
                    String rsType = DataServicesUtil.getResourceType(mBeanServer,
                        rsName);

                    if (rsType != null && rsType.indexOf(Util.QFS_RTNAME) != -1) {
                        // it's a QFS resource
                        if (qfsRsrc.getResourceName() != null) {
                            // the previous one is used already
                            // create new one and add it to the NFS dependency list
                            qfsRsrc = qfsRsrc.copyResource();
                            newRsList.add(qfsRsrc);
                            dependencyList.add(qfsRsrc);
                        } 
                        resInfo = qfsRsrc;
                    } else {
                        if (haspRsrc.getResourceName() != null) {
                            // the previous one is used already
                            // create new one and add it to the NFS dependency list
                            haspRsrc = haspRsrc.copyResource();
                            newRsList.add(haspRsrc);
                            dependencyList.add(haspRsrc);
                        }
                        resInfo = haspRsrc;
                    }
                    resInfo.setResourceName(rsName);
                    resInfo.setExtProperties(resExtProp);
                    resInfo.setSysProperties(resSysProp);
                    resInfo.setBringOnline(true);
                }
            }

            // HASP/QFS Resources created through HAOracle wizard
            if (newHaspRS != null) {

                // There is a new hasp/qfs resource to be created
/*
                ObjectNameFactory dsOnf = new ObjectNameFactory(Util.DOMAIN);
                HAStoragePlusMBean haspMBean = (HAStoragePlusMBean)
                    MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
                        dsOnf.getObjectName(HAStoragePlusMBean.class, null),
                        HAStoragePlusMBean.class, false);
                HashMap fsMntMap = haspMBean.obtainMountpointFSTypes(nodeList);
*/
                HashMap fsMntMap = DataServicesUtil.getFsMountMap(
                        mBeanServer, nodeList);

                String fsMountPoints[] = (String[]) newHaspRS.get(
                        Util.NEW_FSMOUNTPOINTS);
                String fstype = null; // if it's null, hasp will be created
                if (fsMountPoints != null) {
                    String fsEntry = fsMountPoints[0];
                    int commaInd = 0;
                    if ((commaInd = fsMountPoints[0].indexOf(Util.COMMA)) != -1) {
                        // contain multiple filesystem mount directories
                        fsEntry = fsMountPoints[0].substring(0, commaInd);
                    }
                    fstype = (String)fsMntMap.get(fsEntry);
                }
                ResourceInfo resInfo; // tmp resourceInfo reference

                if (fstype != null && fstype.equals(Util.FSTYPE_SAMFS)) {
                    // it's a QFS resource
                    if (qfsRsrc.getResourceName() != null) {
                        // the previous one is used already
                        // create new one and add it to the NFS dependency list
                        qfsRsrc = qfsRsrc.copyResource();
                        newRsList.add(qfsRsrc);
                        dependencyList.add(qfsRsrc);
                    }
                    resInfo = qfsRsrc;
                } else { // all other will be used to create HASP resource
                    if (haspRsrc.getResourceName() != null) {
                        // the previous one is used already
                        // create new one and add it to the NFS dependency list
                        haspRsrc = haspRsrc.copyResource();
                        newRsList.add(haspRsrc);
                        dependencyList.add(haspRsrc);
                    }
                    resInfo = haspRsrc;
                }
                rsName = (String) newHaspRS.get(Util.NEW_HASP_RSNAME);
                resInfo.setResourceName(rsName);

                // Resource Properties
                resExtProp = new ArrayList();
                resSysProp = new ArrayList();

                if ((fsMountPoints != null) && (fsMountPoints.length > 0)) {
                    ResourceProperty rp;

                    if (fstype != null && fstype.equals(Util.FSTYPE_SAMFS)) {
                        // overwrite the rp with QFS_FILESYSTEM
                        rp = new ResourcePropertyStringArray(
                            Util.QFS_FILESYSTEM, fsMountPoints);
                    } else {
                        rp = new ResourcePropertyStringArray(
                            Util.HASP_ALL_FILESYSTEMS, fsMountPoints);

                    }
                    resExtProp.add(rp);
                }

                // Get the map to add for /etc/vfstab and update all nodes
                HashMap fsHashMap = (HashMap) newHaspRS.get(
                    Util.HASP_ALL_FILESYSTEMS);

                if (fsHashMap != null) {
                    HashMap map_to_add = (HashMap) fsHashMap.get(
                            Util.VFSTAB_MAP_TO_ADD);

                    if (map_to_add != null) {
                        createMBeanServerConnections(nodeList);

                        FileAccessorWrapper fileAccessor =
                            new FileAccessorWrapper(Util.VFSTAB_FILE,
                                this.mbsConnections);

                        for (int i = 0; i < nodeList.length; i++) {
                            String split_str[] = nodeList[i].split(Util.COLON);
                            ArrayList nodeEntry = (ArrayList) map_to_add.get(
                                    split_str[0]);

                            if (nodeEntry != null) {

                                // each element of arraylist is a vfstab entry
                                fileAccessor.appendLine(nodeEntry,
                                    this.mbsConnections[i]);
                            }
                        }

                        closeConnections(nodeList);
                    }
                }

                String globalDevicePaths[] = (String[]) newHaspRS.get(
                        Util.HASP_ALL_DEVICES);
                if ((globalDevicePaths != null) &&
                        (globalDevicePaths.length > 0)) {
                    ResourceProperty rp = new ResourcePropertyStringArray(
                            Util.HASP_ALL_DEVICES, globalDevicePaths);

                    resExtProp.add(rp);
                }

                // Resource Extension Properties
                resInfo.setExtProperties(resExtProp);
                resInfo.setSysProperties(resSysProp);
                resInfo.setBringOnline(true);
            }
            // set resource dependency on this newly created hasp
            // resourceInfo structure
            if (oracleServerRs != null) {
                oracleServerRs.setDependency(dependencyList);
            }

            if (oracleListenerRs != null) {
                oracleListenerRs.setDependency(dependencyList);
            }

            /*
             * We need to fill LH Resources separately - as we could have
             * multiple LH resources and command generator handles this in
             * a separate fashion.
             *
             * LH resources command generation
             * 1. Get ResourceInfo Object from serviceInfo
             * 2. Get copy(s) of the resourceinfo object
             * 3. Fill resource info object(s).
             */

            lhRs = serviceInfo.getResource(Util.LH_RES_ID);

            // Fill resource info object(s)

            iterkeys = null;

            List LHExtProp = new ArrayList();
            List LHSysProp = new ArrayList();

            // LH Resources already present on the cluster to be used by
            // HAOracle res

            if (lhRSNameList != null) {
                iterkeys = lhRSNameList.iterator();

                while (iterkeys.hasNext()) {
                    rsName = (String) iterkeys.next();

                    // Resource Name
                    lhRs.setResourceName(rsName);

                    // Resource Extension Properties - must copy empty list as
                    // per
                    // command generator requirements.
                    lhRs.setExtProperties(LHExtProp);
                    lhRs.setSysProperties(LHSysProp);
                    lhRs.setBringOnline(true);
                    lhRs = lhRs.copyResource();
                }
            }

            // LH Resources created through HAOracle wizard
            if (newLhRS != null) {

                // There is atleast one new LH resource to be created
                rsName = (String) newLhRS.get(Util.NEW_LH_RSNAME);
                lhRs.setResourceName(rsName);

                // Resource Properties
                LHExtProp = new ArrayList();
                LHSysProp = new ArrayList();

                String hostNameList[] = (String[]) newLhRS.get(
                        Util.HOSTNAMELIST);

                if ((hostNameList != null) && (hostNameList.length > 0)) {
                    ResourceProperty rp = new ResourcePropertyStringArray(
                            Util.HOSTNAMELIST, hostNameList);

                    LHExtProp.add(rp);
                }

                String netifList[] = (String[]) newLhRS.get(Util.NETIFLIST);

                if ((netifList != null) && (netifList.length > 0)) {
                    ResourceProperty rp = new ResourcePropertyStringArray(
                            Util.NETIFLIST, netifList);

                    LHExtProp.add(rp);
                }

                // Resource Extension Properties
                lhRs.setExtProperties(LHExtProp);
                lhRs.setSysProperties(LHSysProp);
                lhRs.setBringOnline(true);
            } else {

                // Removing the resource with null values.
                lhRs.removeResource();
            }

            // Generate CommandSet for this Service
            serviceInfo.generateCommandSet(mBeanServer, onf);
            commandList = serviceInfo.getCommandSet();

        } catch (Exception e) {
e.printStackTrace();
            // Unconfigure the Groups and Resources
            try {
                commandList = serviceInfo.getCommandSet();

                // add Rollback Success string to the command list - this
                // would be used as a marker to demarcate configuration and
                // rollback commands
                commandList.add(Util.ROLLBACK_SUCCESS);
                serviceInfo.rollBack(mBeanServer, onf);
                commandList.addAll(serviceInfo.getCommandSet());
            } catch (Exception re) {

                // Rollback Failed
                // add Rollback Fail string in place of Rollback Success string
                // to the command list - this would be used as a marker to
                // demarcate configuration and rollback commands
                commandList.remove(Util.ROLLBACK_SUCCESS);
                commandList.add(Util.ROLLBACK_FAIL);
                commandList.addAll(serviceInfo.getCommandSet());
            }

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException) e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        } finally {

            // Always reset the structure to the original form
            // Clear hasp/qfs rsrcs

           if (newRsList.size() > 0) {
                List resourceList = serviceInfo.getResourceGroup(
                    Util.SERVER_GROUP).getResources();
 
                Iterator i = newRsList.iterator();
                while (i.hasNext()) {
                    ResourceInfo resource = (ResourceInfo)i.next();
                    resourceList.remove(resource);
                    dependencyList.remove(resource);
                }
                if (oracleServerRs != null) {
                    oracleServerRs.setDependency(dependencyList);
                }

                if (oracleListenerRs != null) {
                    oracleListenerRs.setDependency(dependencyList);
                }
            }

            serviceInfo.reset();
        }

        return commandList;
    }

    private void createMBeanServerConnections(String nodeList[]) {

        // first get the local connection
        try {
            HashMap env = new HashMap();
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                this.getClass().getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());
            this.localConnector = TrustedMBeanModel.getWellKnownConnector(Util
                    .getClusterEndpoint(), env);
            this.mbsConnections = new MBeanServerConnection[nodeList.length];
            this.connectors = new JMXConnector[nodeList.length];

            for (int i = 0; i < nodeList.length; i++) {
                String split_str[] = nodeList[i].split(Util.COLON);
                String nodeEndPoint = Util.getNodeEndpoint(localConnector,
                        split_str[0]);

                connectors[i] = TrustedMBeanModel.getWellKnownConnector(
                        nodeEndPoint, env);

                // Now get a reference to the mbean server connection
                this.mbsConnections[i] = connectors[i]
                    .getMBeanServerConnection();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SecurityException se) {
            se.printStackTrace();
            throw se;
        }
    }

    public void closeConnections(String nodeList[]) {

        if (nodeList != null) {

            for (int i = 0; i < nodeList.length; i++) {

                try {

                    if (connectors[i] != null) {
                        connectors[i].close();
                    }
                } catch (IOException ioe) {
                }
            }
        }

        if (localConnector != null) {

            try {
                localConnector.close();
            } catch (IOException ioe) {
            }
        }
    }

    // Implementation For the ServiceConfig Interface Methods

    public String[] getDiscoverableProperties() {

        // FileSystems and Device groups can be discovered at this point
        // we should perhaps get this list from the data services layer.
        // NODELIST discovery is called as required from the wizards
        // directly to the discoverPossibilties() Performing discovery
        // here will overwrite the values used by the wizards.
        // String discoverableProps[] = { Util.NODELIST };
        String discoverableProps[] = {};

        return discoverableProps;
    }


    public String[] getAllProperties() {
        return null;
    }

    public String[] getAllProperties(String rtName) {

        // Retrieve Properties From the RTR File.
        String properties[] = DataServicesUtil.getRTRProperties(mBeanServer,
                rtName);

        return properties;
    }

    public HashMap discoverPossibilities(String propertyNames[],
        Object helperData) {

        HashMap resultMap = new HashMap();
        List propList = Arrays.asList(propertyNames);

        String propertyName = null;
        String server_propertyName = null;
        String listener_propertyName = null;
        ResourceProperty property = null;
        String propNames[] = null;
        String allProps[] = null;
        String discoverableProps[] = null;

        // Discover PropertyNames : This is called while Initializing the
        // WizardContext Model
        if (propList.contains(Util.PROPERTY_NAMES)) {

            // Get the discoverableProperty Values
            discoverableProps = getDiscoverableProperties();

            for (int i = 0; i < discoverableProps.length; i++) {
                propNames = new String[] { discoverableProps[i] };
                resultMap.putAll(discoverPossibilities(propNames, null));
            }

            // Add other propertyNames to the resultMap
            // Get the server properties
            allProps = getAllProperties(Util.ORA_SERVER_RTNAME);

            for (int i = 0; i < allProps.length; i++) {
                propertyName = allProps[i];
                server_propertyName = Util.SERVER_PREFIX + propertyName;

                // Omit the Discovered Properties
                if (!resultMap.containsKey(server_propertyName)) {

                    // Get the default values for these properties if any
                    property = DataServicesUtil.getRTRProperty(mBeanServer,
                            Util.ORA_SERVER_RTNAME, propertyName);
                    resultMap.put(server_propertyName, property);
                }
            }

            // Add other propertyNames to the resultMap
            // Get the listener properties
            allProps = getAllProperties(Util.ORA_LISTENER_RTNAME);

            for (int i = 0; i < allProps.length; i++) {
                propertyName = allProps[i];
                listener_propertyName = Util.LISTENER_PREFIX + propertyName;

                // Omit the Discovered Properties
                if (!resultMap.containsKey(listener_propertyName)) {

                    // Get the default values for these properties if any
                    property = DataServicesUtil.getRTRProperty(mBeanServer,
                            Util.ORA_LISTENER_RTNAME, propertyName);
                    resultMap.put(listener_propertyName, property);
                }
            }
        }

        if (propList.contains(Util.ORA_SERVER_RTNAME)) {

            // Get the discoverableProperty Values
            discoverableProps = getDiscoverableProperties();

            for (int i = 0; i < discoverableProps.length; i++) {
                propNames = new String[] { discoverableProps[i] };
                resultMap.putAll(discoverPossibilities(propNames, null));
            }

            // Add other propertyNames to the resultMap
            // Get the server properties
            allProps = getAllProperties(Util.ORA_SERVER_RTNAME);

            for (int i = 0; i < allProps.length; i++) {
                propertyName = allProps[i];
                server_propertyName = Util.SERVER_PREFIX + propertyName;

                // Omit the Discovered Properties
                if (!resultMap.containsKey(server_propertyName)) {

                    // Get the default values for these properties if any
                    property = DataServicesUtil.getRTRProperty(mBeanServer,
                            Util.ORA_SERVER_RTNAME, propertyName);
                    resultMap.put(server_propertyName, property);
                }
            }
        }

        if (propList.contains(Util.ORA_LISTENER_RTNAME)) {

            // Get the discoverableProperty Values
            discoverableProps = getDiscoverableProperties();

            for (int i = 0; i < discoverableProps.length; i++) {
                propNames = new String[] { discoverableProps[i] };
                resultMap.putAll(discoverPossibilities(propNames, null));
            }

            // Add other propertyNames to the resultMap
            // Get the listener properties
            allProps = getAllProperties(Util.ORA_LISTENER_RTNAME);

            for (int i = 0; i < allProps.length; i++) {
                propertyName = allProps[i];
                listener_propertyName = Util.LISTENER_PREFIX + propertyName;

                // Omit the Discovered Properties
                if (!resultMap.containsKey(listener_propertyName)) {

                    // Get the default values for these properties if any
                    property = DataServicesUtil.getRTRProperty(mBeanServer,
                            Util.ORA_LISTENER_RTNAME, propertyName);
                    resultMap.put(listener_propertyName, property);
                }
            }
        }

        if (propList.contains(Util.NODELIST)) {
            resultMap.put(Util.NODELIST,
                DataServicesUtil.getNodeList(mBeanServer));
        }

        if (propList.contains(Util.RG_NAME)) {

            // Check if the rgName already exists
            String rgnamestr;
            int count = 0;

            do {
                StringBuffer rg_prefix = new StringBuffer(PURPOSE);
                StringBuffer rgname = rg_prefix;

                if (count > 0) {
                    rgname.append(Util.UNDERSCORE);
                    rgname.append(count);
                }

                rgname.append(Util.RG_POSTFIX);
                count++;
                rgnamestr = new String(rgname.toString());
            } while (DataServicesUtil.isRGNameUsed(mBeanServer, rgnamestr));

            resultMap.put(Util.RG_NAME, rgnamestr);
        }

        if (propList.contains(Util.RS_NAME)) {
            String rtName = (String) ((HashMap) helperData).get(Util.RTNAME);
            String sid = (String) ((HashMap) helperData).get(Util.ORACLE_SID);
            StringBuffer rs_prefix = new StringBuffer(getName(
                        new String[] { rtName, sid }));
            StringBuffer rsname;
            String rsnamestr;
            int count = 0;

            do {
                rsname = rs_prefix;

                if (count > 0) {
                    rsname.append(Util.UNDERSCORE);
                    rsname.append(count);
                }

                rsname.append(Util.RS_POSTFIX);
                count++;
                rsnamestr = rsname.toString();
            } while (DataServicesUtil.isResourceNameUsed(mBeanServer,
                    rsnamestr));

            resultMap.put(Util.RS_NAME, rsnamestr);
        }

        if (propList.contains(Util.HASP_RSLIST)) {
            String nodeList[] = (String[]) ((HashMap) helperData).get(
                    Util.RG_NODELIST);

            // get a list of HAStoragePlus resources
            // with nodelist set to nodeList
            List resources = DataServicesUtil.getResourceList(mBeanServer,
                    nodeList, Util.HASP_RTNAME);
            resultMap.put(Util.HASP_RSLIST, resources);
        }

        if (propList.contains(Util.ORAHOME_HASP_RSLIST)) {
            List resources = (List) ((HashMap) helperData).get(
                    Util.HASP_RSLIST);
            String oraHome = (String) ((HashMap) helperData).get(
                    Util.ORACLE_HOME);

            int size = resources.size();
            List oraHomeResources = null;
            List oraResources = null;

            for (int i = 0; i < size; i++) {
                String resource = (String) resources.get(i);

                // split on colon to get the rsName and rgName
                String splitStr[] = resource.split(Util.COLON);


                QueryExp query = Query.and(Query.eq(
                            new AttributeValueExp("Name"),
                            Query.value(splitStr[0])),
                        Query.eq(new AttributeValueExp("ResourceGroupName"),
                            Query.value(splitStr[1])));

                oraHomeResources = DataServicesUtil.getHASPResListContainingDir(
                        mBeanServer, query, oraHome);

                if (oraResources == null) {
                    oraResources = new ArrayList();
                }

                if (oraHomeResources != null) {
                    Iterator iterator = oraHomeResources.iterator();

                    while (iterator.hasNext()) {
                        String rsName = (String) iterator.next();
                        String entryToAdd = rsName + Util.COLON + splitStr[1];

                        if (!oraResources.contains(entryToAdd)) {
                            oraResources.add(entryToAdd);
                        }
                    }
                }
            }

            resultMap.put(Util.ORAHOME_HASP_RSLIST, oraResources);
        }

        if (propList.contains(Util.LH_RSLIST)) {
            String nodeList[] = (String[]) ((HashMap) helperData).get(
                    Util.RG_NODELIST);

            // get a list of Logical Host resources
            // with nodelist set to nodeList
            List resources = null;

            if (nodeList == null) {
                String rgName = (String) ((HashMap) helperData).get(
                        Util.RG_NAME);

                // get all logical host resources in resource group rgName
                QueryExp query = Query.and(Query.match(
                            new AttributeValueExp("Type"),
                            Query.value(Util.LH_RTNAME + "*")),
                        Query.eq(new AttributeValueExp("ResourceGroupName"),
                            Query.value(rgName)));

                // Get the ObjectName Factory for RGM CMASS Module
                ObjectNameFactory rgmOnf = new ObjectNameFactory(
                        ResourceMBean.class.getPackage().getName());

                ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                        ResourceMBean.class);

                Set rset = mBeanServer.queryNames(rObjectName, query);

                if (rset != null) {

                    if (resources == null) {
                        resources = new ArrayList();

                        Iterator i = rset.iterator();

                        while (i.hasNext()) {
                            ObjectName rsObjName = (ObjectName) i.next();
                            String rsName = rgmOnf.getInstanceName(rsObjName);
                            resources.add(rsName + Util.COLON + rgName);
                        }
                    }
                }
            } else {
                resources = DataServicesUtil.getResourceList(mBeanServer,
                        nodeList, Util.LH_RTNAME);
            }

            resultMap.put(Util.LH_RSLIST, resources);
        }

        if (propList.contains(Util.ZONE_ROOTPATH)) {
            String nodeName = (String) helperData;
            String zoneExportPath = DataServicesUtil.getZoneExportPath(
                    mBeanServer, nodeName);

            StringBuffer zoneRootPath = new StringBuffer();

            if ((zoneExportPath != null) &&
                    (zoneExportPath.trim().length() > 0)) {
                zoneRootPath.append(zoneExportPath);
                zoneRootPath.append(Util.ROOT_PATH);
            } else {
                zoneRootPath.append("/");
            }

            resultMap.put(Util.ZONE_ROOTPATH, zoneRootPath.toString());
        }

        if (propList.contains(Util.ORACLE_HOME) ||
                propList.contains(Util.ORACLE_SID) ||
                propList.contains(Util.ALERT_LOG_FILE) ||
                propList.contains(Util.PARAMETER_FILE) ||
                propList.contains(Util.USER_ENV)) {

            // Discover possiblities for DataServices Layer
            try {
                ServiceConfig oracleDS = getConfigObject();
                HashMap retMap = oracleDS.discoverPossibilities(propertyNames,
                        helperData);

                if (retMap != null) {
                    resultMap.putAll(retMap);
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }

        // Discover Zone Enable
        if (propList.contains(Util.ZONE_ENABLE)) {

            try {
                ServiceConfig oracleDS = getConfigObject();
                HashMap retMap = oracleDS.discoverPossibilities(
                        new String[] { Util.ORACLE_ZONE_ENABLE }, null);

                if (retMap != null) {
                    resultMap.put(Util.ZONE_ENABLE,
                        retMap.get(Util.ORACLE_ZONE_ENABLE));
                }
            } catch (Exception ex) {
                resultMap.put(Util.ZONE_ENABLE, null);
            }
        }

        return resultMap;
    }

    private String getName(String props[]) {
        String rtName = props[0];
        String namestr = rtName.substring(rtName.indexOf(Util.DOT) + 1);
        StringBuffer name = new StringBuffer(namestr);

        for (int i = 1; i < props.length; i++) {

            if (props[i] != null) {
                name.append(props[i]);
            }
        }

        return name.toString();
    }

    public HashMap discoverPossibilities(String nodeList[],
        String propertyNames[], Object helperData) {
        return null;
    }

    public ErrorValue validateInput(String propNames[], HashMap userInputs,
        Object helperData) {
        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        List propList = Arrays.asList(propNames);
        boolean ret;
        String value;

        if (propList.contains(Util.RS_NAME)) {

            // validate user input
            value = (String) userInputs.get(Util.RS_NAME);

            if (DataServicesUtil.isResourceNameUsed(mBeanServer, value)) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append("");
            }
        }

        if (propList.contains(Util.RG_NAME)) {
            value = (String) userInputs.get(Util.RG_NAME);

            if (DataServicesUtil.isRGNameUsed(mBeanServer, value)) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append("");
            }
        }

        if (!propList.contains(Util.RS_NAME) &&
                !propList.contains(Util.RG_NAME)) {

            // validateInput of DataServices Layer
            // do not run validation for now.
            try {
                ServiceConfig oracleDS = getConfigObject();
                ErrorValue errfromDS = oracleDS.validateInput(propNames,
                        userInputs, helperData);

                if (!errfromDS.getReturnValue().booleanValue()) {
                    err.setReturnVal(Boolean.FALSE);
                    errStr.append(errfromDS.getErrorString());
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                err.setReturnVal(Boolean.FALSE);
                errStr.append(ex.getMessage());
            }
        }

        err.setErrorString(errStr.toString());

        return err;
    }

    public ErrorValue applicationConfiguration(Object helperData) {
        return null;

    }

    public String[] aggregateValues(String propertyName, Map unfilteredValues,
        Object helperData) {
        return null;
    }

    private ServiceConfig getConfigObject() {

        if (oracleConfiguration == null) {
            oracleConfiguration = serviceInfo.getConfigurationClass(this
                    .getClass().getClassLoader());
        }

        return oracleConfiguration;
    }
}
