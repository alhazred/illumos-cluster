/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)VfstabFileAccessorMBean.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.dataservices.utils;

// Java
import java.util.ArrayList;


/**
 * Interface VfstabFileAccessorMBean The methods defined in this interface to
 * access vfstab file are wrapper functions over getvfsent(), getvfsspec(),
 * getvfsfile(), getvfsany(), putvfsent() functions.
 */
public interface VfstabFileAccessorMBean {


    /**
     * Read the entire contents of the vfstab file. This function internally
     * uses getvfsent() API to read through vfstab file.
     *
     * @return  Returns a list of VfsStruct objects.
     */
    public ArrayList readFully();


    /**
     * Writes the given list of VfsStruct objects to vfstab file. This function
     * checks for the presence of given VfsStruct in the vfstab file, through
     * getvfsany() API. If the VfsStruct is not present the function uses
     * putvfsent() API to write to vfstab file.
     *
     * @param  list  of VfsStruct objects.
     */
    public void appendLine(ArrayList list);


    /**
     * This function searches the vfstab file until a mount point matching data
     * is found and prepares a corresponding VfsStruct with the fields from the
     * line in the vfstab file. This function internally uses getvfsfile() API.
     *
     * @return  Returns a list of thus found VfsStruct objects.
     */
    public ArrayList getvfsfile(String data);


    /**
     * This function searches the vfstab file until a special device matching
     * data is found and prepares a corresponding VfsStruct with the fields from
     * the line in the vfstab file.  The data argument will try to match on
     * device type (block or character special) and major and minor device
     * numbers.  If  it  cannot match in this manner, then it compares the
     * strings. This function internally uses getvfsspec() API.
     *
     * @return  Returns a list of thus found VfsStruct objects.
     */
    public ArrayList getvfsspec(String data);


    /**
     * The getvfsany() function searches the vfstab file until a match is found
     * between a line in the file and given VfsStruct object. A match occurrs if
     * all non-null entries in the given VfsStruct object match the
     * corresponding fields in the file. This function internally uses
     * getvfsany() API.
     *
     * @return  Returns a list of thus found VfsStruct objects.
     */
    public ArrayList getvfsany(VfsStruct vfsStruct);

}
