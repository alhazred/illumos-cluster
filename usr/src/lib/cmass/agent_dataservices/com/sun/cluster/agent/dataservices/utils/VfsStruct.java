/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)VfsStruct.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.agent.dataservices.utils;

import java.io.Serializable;


/**
 * Class VfsStruct The objects of this class are corresponding to vfs Struct as
 * returned by getvfsent API. If value for any field is empty string, populate
 * it as null for proper functioning of the C APIs.
 */

public class VfsStruct implements Serializable {

    public String vfs_special = null;
    public String vfs_fsckdev = null;
    public String vfs_mountp = null;
    public String vfs_fstype = null;
    public String vfs_fsckpass = null;
    public String vfs_automnt = null;
    public String vfs_mntopts = null;

    public VfsStruct() {
    }

    public String toString() {

        StringBuffer buffer = new StringBuffer();
        buffer.append("\n\t\t");
        buffer.append("vfs_special : " + vfs_special + "\n\t\t");
        buffer.append("vfs_fsckdev : " + vfs_fsckdev + "\n\t\t");
        buffer.append("vfs_mountp : " + vfs_mountp + "\n\t\t");
        buffer.append("vfs_fstype : " + vfs_fstype + "\n\t\t");
        buffer.append("vfs_fsckpass : " + vfs_fsckpass + "\n\t\t");
        buffer.append("vfs_automnt : " + vfs_automnt + "\n\t\t");
        buffer.append("vfs_mntopts : " + vfs_mntopts + "\n\n");

        return buffer.toString();
    }

    public boolean equals(VfsStruct that) {

        boolean flag = false;

        if (compareStrings(this.vfs_special, that.vfs_special) &&
                compareStrings(this.vfs_fsckdev, that.vfs_fsckdev) &&
                compareStrings(this.vfs_mountp, that.vfs_mountp) &&
                compareStrings(this.vfs_fstype, that.vfs_fstype) &&
                compareStrings(this.vfs_fsckpass, that.vfs_fsckpass) &&
                compareStrings(this.vfs_automnt, that.vfs_automnt) &&
                compareStrings(this.vfs_mntopts, that.vfs_mntopts))
            flag = true;

        return flag;
    }


    public void setVfs_special(String value) {

        if (value == "")
            value = null;

        this.vfs_special = value;
    }

    public String getVfs_special() {
        String value = this.vfs_special;

        if (value == null)
            value = "";

        return value;
    }

    public void setVfs_fsckdev(String value) {

        if (value == "")
            value = null;

        this.vfs_fsckdev = value;
    }

    public String getVfs_fsckdev() {
        String value = this.vfs_fsckdev;

        if (value == null)
            value = "";

        return value;
    }

    public void setVfs_mountp(String value) {

        if (value == "")
            value = null;

        this.vfs_mountp = value;
    }

    public String getVfs_mountp() {
        String value = this.vfs_mountp;

        if (value == null)
            value = "";

        return value;
    }

    public void setVfs_fstype(String value) {

        if (value == "")
            value = null;

        this.vfs_fstype = value;
    }

    public String getVfs_fstype() {
        String value = this.vfs_fstype;

        if (value == null)
            value = "";

        return value;
    }


    public void setVfs_fsckpass(String value) {

        if (value == "")
            value = null;

        this.vfs_fsckpass = value;
    }

    public String getVfs_fsckpass() {
        String value = this.vfs_fsckpass;

        if (value == null)
            value = "";

        return value;
    }

    public void setVfs_automnt(String value) {

        if (value == "")
            value = null;

        this.vfs_automnt = value;
    }

    public String getVfs_automnt() {
        String value = this.vfs_automnt;

        if (value == null)
            value = "";

        return value;
    }

    public void setVfs_mntopts(String value) {

        if (value == "")
            value = null;

        this.vfs_mntopts = value;
    }

    public String getVfs_mntopts() {
        String value = this.vfs_mntopts;

        if (value == null)
            value = "";

        return value;
    }


    private boolean compareStrings(String s1, String s2) {

        if (s1 == null)
            s1 = "";

        if (s2 == null)
            s2 = "";

        return s1.equals(s2);
    }


}
