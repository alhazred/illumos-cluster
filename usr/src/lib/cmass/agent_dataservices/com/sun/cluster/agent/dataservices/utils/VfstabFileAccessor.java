/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)VfstabFileAccessor.java 1.11     08/05/20 SMI"
 */

package com.sun.cluster.agent.dataservices.utils;

// J2SE
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * Class VfstabFileAccessor The methods defined here, to access vfstab file are
 * wrapper functions over getvfsent(), getvfsspec(), getvfsfile(), getvfsany()
 * and putvfsent() functions.
 */
public class VfstabFileAccessor implements VfstabFileAccessorMBean {

    private ArrayList vfsStructList = null;

    /**
     * Read the entire contents of the vfstab file. This function internally
     * uses getvfsent() API to read through vfstab file.
     *
     * @return  Returns a list of VfsStruct objects.
     */
    public ArrayList readFully() {
        vfsStructList = new ArrayList();
        this.executeOperation(Util.vfstab_op_readFully, null, null);

        return vfsStructList;
    }

    /**
     * Writes the given list of VfsStruct objects to vfstab file. This function
     * checks for the presence of given VfsStruct in the vfstab file, through
     * getvfsany() API. If the VfsStruct is not present the function uses
     * putvfsent() API to write to vfstab file.
     *
     * @param  list  of VfsStruct objects.
     */
    public void appendLine(ArrayList list) {

        Iterator iter = list.iterator();

        while (iter.hasNext()) {
            VfsStruct vfsStruct = (VfsStruct) iter.next();
            ArrayList tmpList = this.getvfsany(vfsStruct);

            // write to vfstab file only if the given vfstruct is not present
            if (tmpList.isEmpty()) {
                this.executeOperation(Util.vfstab_op_appendLine, null,
                    vfsStruct);
            }
        }
    }


    /**
     * This function searches the vfstab file until a mount point matching data
     * is found and prepares a corresponding VfsStruct with the fields from the
     * line in the vfstab file. This function internally uses getvfsfile() API.
     *
     * @return  Returns a list of thus found VfsStruct objects.
     */
    public ArrayList getvfsfile(String data) {
        vfsStructList = new ArrayList();
        this.executeOperation(Util.vfstab_op_getvfsfile, data, null);

        return vfsStructList;
    }


    /**
     * This function searches the vfstab file until a special device matching
     * data is found and prepares a corresponding VfsStruct with the fields from
     * the line in the vfstab file.  The data argument will try to match on
     * device type (block or character special) and major and minor device
     * numbers.  If  it  cannot match in this manner, then it compares the
     * strings. This function internally uses getvfsspec() API.
     *
     * @return  Returns a list of thus found VfsStruct objects.
     */
    public ArrayList getvfsspec(String data) {
        vfsStructList = new ArrayList();
        this.executeOperation(Util.vfstab_op_getvfsspec, data, null);

        return vfsStructList;
    }


    /**
     * The getvfsany() function searches the vfstab file until a match is found
     * between a line in the file and given VfsStruct object. A match occurrs if
     * all non-null entries in the given VfsStruct object match the
     * corresponding fields in the file. This function internally uses
     * getvfsany() API.
     *
     * @return  Returns a list of thus found VfsStruct objects.
     */
    public ArrayList getvfsany(VfsStruct vfsStruct) {
        vfsStructList = new ArrayList();

        // getvfsany is not called if all of the fields are null
        VfsStruct tmpVfsStruct = new VfsStruct();

        if (!vfsStruct.equals(tmpVfsStruct)) {
            this.executeOperation(Util.vfstab_op_getvfsany, null, vfsStruct);
        }

        return vfsStructList;
    }


    /**
     * Add the given vfsStruct object to vfsStructList. This function is called
     * by the native fillVfsTabStructList() repeatedly to fill vfsStructList.
     *
     * @param  VfsStruct  vfsStruct object
     *
     * @return  void
     */
    private void addToList(VfsStruct vfsStruct) {
        vfsStructList.add(vfsStruct);
    }

    private native void executeOperation(String operation, String data,
        VfsStruct vfsStruct);

    private native void runCommand(String cmd);
}
