/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NFS.java 1.30     08/12/16 SMI"
 */


package com.sun.cluster.agent.dataservices.nfs;

// J2SE

// CACAO
import com.sun.cacao.ObjectNameFactory;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.hastorageplus.HAStoragePlusMBean;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.common.ServiceConfig;
import com.sun.cluster.agent.dataservices.utils.DataServiceInfo;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.FileAccessorMBean;
import com.sun.cluster.agent.dataservices.utils.FileAccessorWrapper;
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;
import com.sun.cluster.agent.dataservices.utils.ResourceGroupInfo;
import com.sun.cluster.agent.dataservices.utils.ResourceInfo;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.rgm.ResourceGroupMBean;
import com.sun.cluster.agent.rgm.ResourceGroupModeEnum;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.agent.transport.NodeAdapterMBean;
import com.sun.cluster.common.TrustedMBeanModel;

import java.io.File;
import java.io.IOException;

import java.lang.reflect.Method;

import java.net.URL;
import java.net.URLClassLoader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.management.AttributeValueExp;

// JMX
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;


/**
 * NFS MBean interface implementation.
 */
public class NFS implements NFSMBean {

    /* the default value for the NFS share security option */
    private static final String DEFAULT = "default";

    /* RTName of NFS MBean - should get from serviceInfo object */
    private final String RTNAME = "SUNW.nfs";

    /* The MBeanServer in which this MBean is inserted. */
    private MBeanServer mBeanServer;

    /* The object name factory used to register this MBean */
    private ObjectNameFactory onf = null;

    /* Command Generation Structure for this Dataservice */
    private DataServiceInfo serviceInfo = null;

    /* Object of DataService Class */
    private Object objDataServiceCl = null;

    /* List of commands executed */
    private List commandList = null;

    private MBeanServerConnection mbsConnections[];
    private JMXConnector connectors[] = null;
    private JMXConnector localConnector = null;

    /**
     * Default constructor.
     */
    public NFS(MBeanServer mBeanServer, ObjectNameFactory onf,
        DataServiceInfo serviceInfo) {
        this.mBeanServer = mBeanServer;
        this.onf = onf;
        this.serviceInfo = serviceInfo;

    }


    /**
     * Returns the ServiceInfo Structure for this DataserviceMBean
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public DataServiceInfo getServiceInfo() {
        return this.serviceInfo;
    }


    /**
     * Returns list of executed command list
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public List getCommandList() {
        return this.commandList;
    }

    /**
     * Sets the ServiceInfo Structure for this DataserviceMBean
     *
     * @param  serviceInfo  Command Generation Structure
     */
    public void setServiceInfo(DataServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    /**
     * Generates the set of commands for configuring this service, given the
     * configuration information. This command uses the ServiceInfo structure,
     * fills the same and generates the command list. This method needs to be
     * synchronized because this method operates on Wizard instance specific
     * information.
     *
     * @param  configurationInformation  ConfigurationInformation for
     * configuring an instance of the Dataservice
     *
     * @return  Commands Generated for the given ConfigurationInformation
     */
    public synchronized List generateCommands(Map configurationInformation)
        throws CommandExecutionException {

        ResourceInfo haspRsrc = null;
        ResourceInfo qfsRsrc = null;
        ResourceInfo nfsRsrc = null;
        List dependencyList = null;

        commandList = new ArrayList();

        // define a array list to keep track newly created RS object(s)
        ArrayList newRsList = new ArrayList();

        // Application Configuration Data
        boolean appConfSet = false;
        Map appConfMap = (HashMap) configurationInformation.remove(
                Util.NFS_APPCONFDATA);
        String nodeList[] = null;

        Map methodInvokeMap = new HashMap();
        methodInvokeMap.put("MBeanName", NFSMBean.class);
        methodInvokeMap.put("OperationName", "applicationConfiguration");
        methodInvokeMap.put("ParamType", new String[] { "java.lang.Object" });
        methodInvokeMap.put("ParamValue", new Object[] { appConfMap });
        methodInvokeMap.put("OperationMessage",
            (String) configurationInformation.remove(Util.NFS_OP_MSG));

        Map customConfigureMap = new HashMap();
        customConfigureMap.put("operation", methodInvokeMap);

        try {

            /*
             * We need to fill HASP and QFS Resources separately - as we could have
             * multiple HASP/QFS resources and command generator handles this in
             * a separate fashion.
             *
             * HASP/QFS resources command generation
             * 1. Get both HASP/QFS ResourceInfo Object from serviceInfo
             * 2. Get copy(s) of the resourceinfo object
             * 3. Fill resource info object(s).
             */

            haspRsrc = serviceInfo.getResource(Util.NFS_HASP_RID);
            qfsRsrc = serviceInfo.getResource(Util.QFS_RES_ID);

            // Fill resource info object(s)
            Iterator iterkeys = null;
            Set haspWizSelKeys = null;
            List storageResNameList = null;
            String rsName = null;
            List resExtProp = new ArrayList();
            List resSysProp = new ArrayList();
            FileAccessorWrapper fileAccessor = null;
            List mtptList = null;

            // PathPrefix
            HashMap rgProps = (HashMap) configurationInformation.get(
                    Util.NFS_RESOURCE_GROUP + Util.RGPROP_TAG);
            String pathPrefixMtpt = (String) rgProps.get(Util.PATHPREFIX);
            Object tmpObj = rgProps.remove(Util.NFS_DIR_ADMIN);

            if (tmpObj != null) {
                rgProps.put(Util.PATHPREFIX,
                    appendDir(pathPrefixMtpt, (String) tmpObj));
            }

            configurationInformation.put(Util.NFS_RESOURCE_GROUP +
                Util.RGPROP_TAG, rgProps);

            nodeList = (String[]) configurationInformation.remove(
                    Util.RG_NODELIST);
            createMBeanServerConnections(nodeList);

            // HASP/QFS Resources already present on the cluster to be used by NFS
            // resource
            storageResNameList = (ArrayList) configurationInformation.remove(
                    Util.NFS_HASP_RID + Util.NAME_TAG);

            // Hasp Resources created through NFS wizard
            HashMap haspWizSelection1 = (HashMap) configurationInformation
                .remove(Util.NFS_HASPWIZSEL1);
            HashMap haspWizSelection2 = (HashMap) configurationInformation
                .remove(Util.NFS_HASPWIZSEL2);

            // Fill the ServiceInfo object with the configurationInformation
            // having information other than hasp resources.
            serviceInfo.fillServiceInfoStructure(configurationInformation);

            // NFS Resource handler
            nfsRsrc = serviceInfo.getResource(Util.NFS_RID);
            dependencyList = nfsRsrc.getDependency();
 
            // set NFS Resource bringonline flag to true. This would bring up
            // the newly created nfs service
            nfsRsrc.setBringOnline(true);

            if (storageResNameList != null) {

                // The existing HASP/QFS resource(s) to be used by nfs.
                iterkeys = storageResNameList.iterator();

                while (iterkeys.hasNext()) {

                    ResourceInfo resInfo; // tmp resourceInfo reference
                    // resource name and type
                    rsName = (String) iterkeys.next();
                    String rsType = DataServicesUtil.getResourceType(mBeanServer,
                        rsName);

                    if (rsType != null && rsType.indexOf(Util.QFS_RTNAME) != -1) {
                        // it's a QFS resource
                        if (qfsRsrc.getResourceName() != null) {
                            // the previous one is used already
                            // create new one and add it to the NFS dependency list
                            qfsRsrc = qfsRsrc.copyResource();
                            newRsList.add(qfsRsrc);
                            dependencyList.add(qfsRsrc);
                        } 
                        resInfo = qfsRsrc;
                        mtptList = Arrays.asList((String[]) DataServicesUtil
                            .getResourceExtPropertyValue(mBeanServer, rsName,
                                Util.QFS_FILESYSTEM));
                    } else {
                        if (haspRsrc.getResourceName() != null) {
                            // the previous one is used already
                            // create new one and add it to the NFS dependency list
                            haspRsrc = haspRsrc.copyResource();
                            newRsList.add(haspRsrc);
                            dependencyList.add(haspRsrc);
                        }
                        resInfo = haspRsrc;
                        mtptList = Arrays.asList((String[]) DataServicesUtil
                            .getResourceExtPropertyValue(mBeanServer, rsName,
                                Util.HASP_ALL_FILESYSTEMS));
                    }
                    resInfo.setResourceName(rsName);
                    resInfo.setExtProperties(resExtProp);
                    resInfo.setSysProperties(resSysProp);
                    
                    // If this resource is monitoring the path prefix mount
                    // point, set application configuration data for this res.
                    if (mtptList.contains(pathPrefixMtpt)) {
                        resInfo.setBringOnline(true);
                        resInfo.setCustomConfigureMap(customConfigureMap);
                        appConfSet = true;
                    }
                }
            }

            if (haspWizSelection1 != null) {

                // There is at least one new hasp/qfs resource to be created
                // find all possible entry in vfstab for both ufs and qfs
                HashMap fsMntMap = fsMntMap =  DataServicesUtil.getFsMountMap(
                    mBeanServer, nodeList);
                haspWizSelKeys = haspWizSelection1.keySet();
                iterkeys = haspWizSelKeys.iterator();

                while (iterkeys.hasNext()) {
                    
                    ResourceInfo resInfo; // tmp resourceInfo reference
                    rsName = (String) iterkeys.next();
                    String fsMountPoints[] = (String[])haspWizSelection1.get(
                        rsName);

                    String fstype = null; // if it's null, hasp will be created
                    if (fsMountPoints != null) {
                        // use the first mount point to find fstype
                        String fsEntry = fsMountPoints[0];
                        int commaInd = 0;
                        if ((commaInd = fsMountPoints[0].indexOf(Util.COMMA)) != -1) {
                            // contain multiple filesystem mount directories
                            fsEntry = fsMountPoints[0].substring(0, commaInd);
                        }
                        fstype = (String)fsMntMap.get(fsEntry);
                    }

                    if (fstype != null && fstype.equals(Util.FSTYPE_SAMFS)) {
                        // it's a QFS resource
                        if (qfsRsrc.getResourceName() != null) {
                            // the previous one is used already
                            // create new one and add it to the NFS dependency list
                            qfsRsrc = qfsRsrc.copyResource();
                            newRsList.add(qfsRsrc);
                            dependencyList.add(qfsRsrc);
                        }
                        resInfo = qfsRsrc;
                    } else { // all other will be used to create HASP
                        if (haspRsrc.getResourceName() != null) {
                            // the previous one is used already
                            // create new one and add it to the NFS dependency list
                            haspRsrc = haspRsrc.copyResource();
                            newRsList.add(haspRsrc);
                            dependencyList.add(haspRsrc);
                        }
                        resInfo = haspRsrc;
                    }
                    resInfo.setResourceName(rsName);

                    // Resource Properties
                    resExtProp = new ArrayList();
                    resSysProp = new ArrayList();

                    if ((fsMountPoints != null) && (fsMountPoints.length > 0)) {

                        // If this hasp resource is monitoring the path prefix
                        // mount point, set application configuration data for
                        // this resource.
                        mtptList = Arrays.asList(fsMountPoints);

                        if (mtptList.contains(pathPrefixMtpt)) {
                            resInfo.setBringOnline(true);
                            resInfo.setCustomConfigureMap(customConfigureMap);
                            appConfSet = true;
                        }

                        ResourceProperty rp;

                        if (fstype != null && fstype.equals(Util.FSTYPE_SAMFS)) {
                            // overwrite the rp with QFS_FILESYSTEM
                            rp = new ResourcePropertyStringArray(
                                Util.QFS_FILESYSTEM, fsMountPoints);
                        } else {
                            rp = new ResourcePropertyStringArray(
                                Util.HASP_ALL_FILESYSTEMS, fsMountPoints);

                            // Get the map to add for /etc/vfstab and update all nodes
                            HashMap fsHashMap = (HashMap) haspWizSelection2.get(
                                rsName);
                            HashMap map_to_add = (HashMap) fsHashMap.get(
                                Util.VFSTAB_MAP_TO_ADD);
                            fileAccessor = new FileAccessorWrapper(Util.VFSTAB_FILE,
                                this.mbsConnections);

                            for (int i = 0; i < nodeList.length; i++) {
                                String split_str[] = nodeList[i].split(Util.COLON);
                                ArrayList nodeEntry = (ArrayList) map_to_add.get(
                                    nodeList[i]);

                                if (nodeEntry != null) {
                                    // each element of ArrayList is a vfstab entry
                                    fileAccessor.appendLine(nodeEntry,
                                        this.mbsConnections[i]);
                                }
                            }
                        }
                        resExtProp.add(rp);
                    }

                    // Resource Extension Properties
                    resInfo.setExtProperties(resExtProp);
                    resInfo.setSysProperties(resSysProp);
                }
            }

            // done with adding all storage resources, update dependency list
            nfsRsrc.setDependency(dependencyList);

            if (!appConfSet) {

                // The PathPrefix mountpoint is neither under existing hasp
                // reosurces nor under new hasp resources.
                // Would try to excess it from this node only.
                ErrorValue err = dumpShareCmds(appConfMap);

                if (!err.getReturnValue().booleanValue()) {
                    throw new CommandExecutionException(err.getErrorString());
                }
            }

            // Generate CommandSet for this Service
            serviceInfo.generateCommandSet(mBeanServer, onf);
            commandList = serviceInfo.getCommandSet();

        } catch (Exception e) {
            // Unconfigure the Groups and Resources
            try {
                commandList = serviceInfo.getCommandSet();

                // add Rollback Success string to the command list - this
                // would be used as a marker to demarcate configuration and
                // rollback commands
                commandList.add(Util.ROLLBACK_SUCCESS);
                serviceInfo.rollBack(mBeanServer, onf);
                commandList.addAll(serviceInfo.getCommandSet());
            } catch (Exception re) {

                // Rollback Failed
                // add Rollback Fail string in place of Rollback Success
                // string to the command list - this would be used as a
                // marker to demarcate configuration and rollback commands
                commandList.remove(Util.ROLLBACK_SUCCESS);
                commandList.add(Util.ROLLBACK_FAIL);
                commandList.addAll(serviceInfo.getCommandSet());
            }

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException) e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        } finally {

            // Always reset the structure to the original form
            closeConnections(nodeList);

            if (newRsList.size() > 0) {
                List resourceList = serviceInfo.getResourceGroup(
                    Util.NFS_RESOURCE_GROUP).getResources();
 
                Iterator i = newRsList.iterator();
                while (i.hasNext()) {
                    ResourceInfo resource = (ResourceInfo)i.next();
                    resourceList.remove(resource);
                    dependencyList.remove(resource);
                }
                nfsRsrc.setDependency(dependencyList);
            }
            serviceInfo.reset();
            newRsList.clear();
        }

        return commandList;
    }

    private void createMBeanServerConnections(String nodeList[]) {

        // first get the local connection
        try {
            HashMap env = new HashMap();
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                this.getClass().getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());
            this.localConnector = TrustedMBeanModel.getWellKnownConnector(Util
                    .getClusterEndpoint(), env);
            this.mbsConnections = new MBeanServerConnection[nodeList.length];
            this.connectors = new JMXConnector[nodeList.length];

            for (int i = 0; i < nodeList.length; i++) {
                String split_str[] = nodeList[i].split(Util.COLON);
                String nodeEndPoint = Util.getNodeEndpoint(localConnector,
                        split_str[0]);

                connectors[i] = TrustedMBeanModel.getWellKnownConnector(
                        nodeEndPoint, env);

                // Now get a reference to the mbean server connection
                this.mbsConnections[i] = connectors[i]
                    .getMBeanServerConnection();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SecurityException se) {
            se.printStackTrace();
            throw se;
        }
    }

    public void closeConnections(String nodeList[]) {

        if (nodeList != null) {

            for (int i = 0; i < nodeList.length; i++) {

                try {

                    if (connectors[i] != null) {
                        connectors[i].close();
                    }
                } catch (IOException ioe) {
                }
            }
        }

        if (localConnector != null) {

            try {
                localConnector.close();
            } catch (IOException ioe) {
            }
        }
    }

    // Implementation For the ServiceConfig Interface Methods
    public String[] getDiscoverableProperties() {

        // Need to discover only nodelist at the start of the wizard.
        String discoverableProps[] = {};

        return discoverableProps;
    }

    public String[] getAllProperties() {

        // No other propertes (ex: default properties from RTR file) are needed
        // for NFS.
        String properties[] = new String[] {};

        return properties;
    }

    public HashMap discoverPossibilities(String propertyNames[],
        Object helperData) {


        HashMap resultMap = null;

        // Initializing the DataServiceConfig Class
        ServiceConfig dataServiceCl = serviceInfo.getConfigurationClass(this
                .getClass().getClassLoader());

        // Discover PropertyNames : This is called while Initializing the
        // WizardContext Model
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.PROPERTY_NAMES)) {

            // Get the discoverableProperty Values
            String discoverableProps[] = getDiscoverableProperties();

            for (int i = 0; i < discoverableProps.length; i++) {
                String propertyName[] = { discoverableProps[i] };

                if (resultMap == null) {
                    resultMap = new HashMap();
                    resultMap = discoverPossibilities(propertyName, null);
                } else {
                    resultMap.putAll(discoverPossibilities(propertyName, null));
                }
            }

            // Add other propertyNames to the resultMap
            String allProps[] = getAllProperties();

            for (int i = 0; i < allProps.length; i++) {

                String propertyName = allProps[i];

                if (resultMap == null) {
                    resultMap = new HashMap();
                }

                // Omit the Discovered Properties
                if (!resultMap.containsKey(propertyName)) {

                    // Get the default values for these properties if any
                    Object propertyValue = DataServicesUtil.getDefaultRTRValue(
                            mBeanServer, RTNAME, propertyName);
                    resultMap.put(propertyName, propertyValue);
                }
            }

            return resultMap;
        }

        // Discover nodelist
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.NODELIST)) {
            resultMap = new HashMap();
            resultMap.put(Util.NODELIST,
                DataServicesUtil.getNodeList(mBeanServer));

            return resultMap;
        }
        // Discover unique name for ResourceGroup
        else if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.RG_NAME)) {
            resultMap = new HashMap();

            String rgName = (String) helperData;

            // Check if the rgName already exists
            if (DataServicesUtil.isRGNameUsed(mBeanServer,
                        rgName + Util.RG_POSTFIX)) {
                int count = 1;

                while (DataServicesUtil.isRGNameUsed(mBeanServer,
                            rgName + "-" + count + Util.RG_POSTFIX)) {
                    count++;
                }

                rgName = rgName + "-" + count + Util.RG_POSTFIX;
            } else {
                rgName = rgName + Util.RG_POSTFIX;
            }

            resultMap.put(Util.RG_NAME, rgName);

            return resultMap;
        }
        // Discover unique name for Resource
        else if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.RS_NAME)) {
            resultMap = new HashMap();

            String rName = (String) helperData;

            // Check if the resName already exists
            if (DataServicesUtil.isResourceNameUsed(mBeanServer,
                        rName + Util.RS_POSTFIX)) {
                int count = 1;

                while (DataServicesUtil.isResourceNameUsed(mBeanServer,
                            rName + "-" + count + Util.RS_POSTFIX)) {
                    count++;
                }

                rName = rName + "-" + count + Util.RS_POSTFIX;
            } else {
                rName = rName + Util.RS_POSTFIX;
            }

            resultMap.put(Util.RS_NAME, rName);

            return resultMap;
        }

        // Discover unique name for NFS Resource
        else if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.NFS_RID)) {
            resultMap = new HashMap();

            HashMap userInputs = (HashMap) helperData;
            int count = 1;

            // NFS Resource Name
            String strToUse = (String) userInputs.get(Util.RS_NAME);
            String rName = strToUse + Util.RS_POSTFIX;

            String nodeList[] = (String[]) userInputs.get(Util.RG_NODELIST);

            // Direcotry Path
            String dirPath = (String) userInputs.get(Util.PATHPREFIX);

            if (dirPath.charAt(dirPath.length() - 1) != Util.SLASH.charAt(0)) {
                dirPath = dirPath + Util.SLASH;
            }

            dirPath = dirPath + Util.NFS_DIRPOSTFIX;

            String filePathName = dirPath + Util.SLASH + Util.NFS_FILEPREFIX +
                rName;

            // Check for resource name usage and presence of dfstab file with
            // using that resource name
            while (DataServicesUtil.isResourceNameUsed(mBeanServer, rName) ||
                    isDfstabFilePresent(nodeList, filePathName)) {
                rName = strToUse + "-" + count + Util.RS_POSTFIX;
                filePathName = dirPath + Util.SLASH + Util.NFS_FILEPREFIX +
                    rName;
                count++;
            }

            resultMap.put(Util.RS_NAME, rName);

            return resultMap;
        }

        // Discover Share Options
        else if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.NFS_SHARE_OPT)) {

            // Discover possiblities for DataServices Layer
            try {
                resultMap = (HashMap) dataServiceCl.discoverPossibilities(
                        new String[] { Util.NFS_SHARE_OPT }, null);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        // Discover PathPrefix for a given RG Name
        else if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.PATHPREFIX)) {
            resultMap = new HashMap();

            String rgName = (String) helperData;
            String pathPrefix = "";
            ObjectNameFactory rgmOnf = null;

            if ((rgName != null) && (rgName != "")) {

                try {
                    rgmOnf = new ObjectNameFactory(ResourceGroupMBean.class
                            .getPackage().getName());

                    ObjectName rObjectName = rgmOnf.getObjectName(
                            ResourceGroupMBean.class, rgName);
                    ResourceGroupMBean rgMBean = (ResourceGroupMBean)
                        MBeanServerInvocationHandler.newProxyInstance(
                            mBeanServer, rObjectName, ResourceGroupMBean.class,
                            false);

                    if (rgMBean != null) {
                        pathPrefix = rgMBean.getDirectoryPath();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            resultMap.put(Util.PATHPREFIX, pathPrefix);
        }

        // Discover Zone Enable
        else if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.ZONE_ENABLE)) {

            // Discover possiblities for DataServices Layer
            try {
                resultMap = (HashMap) dataServiceCl.discoverPossibilities(
                        new String[] { Util.ZONE_ENABLE }, null);
            } catch (Exception ex) {
                resultMap = new HashMap();
                resultMap.put(Util.ZONE_ENABLE, null);
            }
        }

        return resultMap;
    }

    public HashMap discoverPossibilities(String nodeList[],
        String propertyNames[], Object helperData) {
        HashMap resultMap = new HashMap();
        List propList = Arrays.asList(propertyNames);
        Map env = new HashMap();
        JMXConnector connector = null;
        JMXConnector localConnector = null;

        try {

            // Get connector for the node on which this mbean is instantiated.
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                this.getClass().getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());

            localConnector = TrustedMBeanModel.getWellKnownConnector(Util
                    .getClusterEndpoint(), env);

            // Discover share options
            if (propList.contains(Util.NFS_SHARE_OPT)) {
                HashMap tmpMap = null;
                NFSMBean mBean = null;
                HashMap shareOptions = new HashMap();
                List shareOptSecurity = new ArrayList();
                String propertyName[] = null;

                for (int i = 0; i < nodeList.length; i++) {

                    // Get connector for the node
                    connector = TrustedMBeanModel.getWellKnownConnector(Util
                            .getNodeEndpoint(localConnector,
                                (nodeList[i].split(Util.COLON))[0]), env);

                    // Get NFSMBean for the node
                    mBean = (NFSMBean) TrustedMBeanModel.getMBeanProxy(
                            Util.DOMAIN, connector, NFSMBean.class, null,
                            false);

                    // Discover possibilities for share options on the node.
                    propertyName = new String[] { Util.NFS_SHARE_OPT };
                    tmpMap = (HashMap) mBean.discoverPossibilities(propertyName,
                            null);

                    // Need to get common elements from security options
                    // returned from share options map from all the nodes
                    if (i == 0) {
                        shareOptions = (HashMap) tmpMap.get(Util.NFS_SHARE_OPT);
                        shareOptSecurity = Arrays.asList((String[]) shareOptions
                                .get(Util.SHARE_OPT_SECURITY));
                    } else {
                        shareOptions = (HashMap) tmpMap.get(Util.NFS_SHARE_OPT);
                        shareOptSecurity = DataServicesUtil.getCommonElements(
                                Arrays.asList(
                                    (String[]) shareOptions.get(
                                        Util.SHARE_OPT_SECURITY)),
                                shareOptSecurity);
                    }

                    // Close connection
                    connector.close();
                }

                // fill common security options in final result hashmap for
                // share options

                String[] shareOptSecurityArray =
                    getShareOptionSecurityArray(shareOptSecurity);

                
                shareOptions.put(Util.SHARE_OPT_SECURITY,
                    shareOptSecurityArray);
                resultMap.put(Util.NFS_SHARE_OPT, shareOptions);
            }

            // Discover NFS dfstab file
            if (propList.contains(Util.NFS_DFSTAB_FILE)) {
                FileAccessorMBean mBean = null;
                String filePathName = (String) helperData;
                boolean flag = false;
                int i;
                String node = null;

                for (i = 0; (i < nodeList.length) && !flag; i++) {

                    // Get connector for the node
                    connector = TrustedMBeanModel.getWellKnownConnector(Util
                            .getNodeEndpoint(localConnector,
                                (nodeList[i].split(Util.COLON))[0]), env);

                    // Get NFSMBean for the node
                    mBean = (FileAccessorMBean) TrustedMBeanModel.getMBeanProxy(
                            Util.DOMAIN, connector, FileAccessorMBean.class,
                            null, false);

                    // Discover possibilities for nfs dfstab file on the node
                    if (mBean.ifFileExists(filePathName)) {
                        flag = true;
                    }

                    // Close connection
                    connector.close();
                }

                if (flag) {
                    node = nodeList[i - 1];
                }

                resultMap.put(Util.NFS_DFSTAB_FILE, node);
            }


            // Close local connector after discovery of all properties
            localConnector.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SecurityException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return resultMap;
        }
    }

    public ErrorValue validateInput(String propertyNames[], HashMap userInputs,
        Object helperData) {

        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        List propList = Arrays.asList(propertyNames);

        // Validate ResourceGroup Name
        if (propList.contains(Util.RESOURCEGROUPNAME)) {
            String rgName = (String) userInputs.get(Util.RESOURCEGROUPNAME);

            if (DataServicesUtil.isRGNameUsed(mBeanServer, rgName)) {
                err.setReturnVal(Boolean.FALSE);
            }
        }
        // Validate Resource Name
        else if (propList.contains(Util.RESOURCENAME)) {
            String rName = (String) userInputs.get(Util.RESOURCENAME);
            Object tmpObj = userInputs.get(Util.PATHPREFIX);

            if (DataServicesUtil.isResourceNameUsed(mBeanServer, rName)) {
                err.setReturnVal(Boolean.FALSE);
            } else if (tmpObj != null) {

                // Check for dfstab file existence
                String dirPath = (String) tmpObj;
                String nodeList[] = (String[]) userInputs.get(Util.RG_NODELIST);

                // Directory Path
                if (dirPath.charAt(dirPath.length() - 1) !=
                        Util.SLASH.charAt(0)) {
                    dirPath = dirPath + Util.SLASH;
                }

                dirPath = dirPath + Util.NFS_DIRPOSTFIX;

                // File Name
                String filePathName = dirPath + Util.SLASH +
                    Util.NFS_FILEPREFIX + rName;
                HashMap tmpMap = discoverPossibilities(nodeList,
                        new String[] { Util.NFS_DFSTAB_FILE }, filePathName);
                tmpObj = tmpMap.get(Util.NFS_DFSTAB_FILE);

                if (tmpObj != null) {
                    String node = (String) tmpMap.get(Util.NFS_DFSTAB_FILE);
                    err.setReturnVal(Boolean.FALSE);
                    err.setErrorString(filePathName + Util.SEPARATOR + node);
                }
            }
        }
        // Validate share options
        else if (propList.contains(Util.NFS_SHARE_OPT)) {

            // validate share options using dataservice config class
            try {

                // Initializing the DataServiceConfig Class
                ServiceConfig dataServiceCl = serviceInfo.getConfigurationClass(
                        this.getClass().getClassLoader());
                ErrorValue retErr = dataServiceCl.validateInput(
                        new String[] { Util.NFS_SHARE_OPT }, userInputs, null);

                if (!retErr.getReturnValue().booleanValue()) {
                    err.setReturnVal(Boolean.FALSE);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return err;
    }

    public ErrorValue applicationConfiguration(Object helperData) {
        return dumpShareCmds(helperData);
    }


    public String[] aggregateValues(String propertyName, Map unfilteredValues,
        Object helperData) {
        return null;
    }

    private ErrorValue dumpShareCmds(Object helperData) {

        /** Dump NFS administrative files  */
        /*
         * mkdir -p /<PathPrefix>/SUNW.nfs
         * touch /<PathPrefix>/SUNW.nfs/dfstab.<NFS Res name>
         * echo <share commands list> >>
         * /<PathPrefix>/SUNW.nfs/dfstab.<NFS Res name>
         */

        ExitStatus status[] = null;
        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        FileAccessorWrapper fileAccessor = null;
        HashMap helperDataMap = (HashMap) helperData;

        String shareCmdsList[] = (String[]) helperDataMap.get(
                Util.NFS_SHARE_CMDS);
        String dirPath = (String) helperDataMap.get(Util.PATHPREFIX);
        String filePathName = (String) helperDataMap.get(Util.NFS_RID +
                Util.NAME_TAG);

        // Directory Path
        if (dirPath.charAt(dirPath.length() - 1) != Util.SLASH.charAt(0)) {
            dirPath = dirPath + Util.SLASH;
        }

        dirPath = dirPath + Util.NFS_DIRPOSTFIX;

        // File Name
        filePathName = dirPath + Util.SLASH + Util.NFS_FILEPREFIX +
            filePathName;

        MBeanServerConnection mbsConnections[] = new MBeanServerConnection[] {
                mBeanServer
            };
        fileAccessor = new FileAccessorWrapper(filePathName, mbsConnections);

        if (!(fileAccessor.createDir(dirPath, null) &&
                    fileAccessor.createAndOverwriteFile(filePathName, null) &&
                    fileAccessor.appendLine(shareCmdsList, null, false))) {
            err.setReturnVal(Boolean.FALSE);
            err.setErrorString("nfs.resultspanel.dumpAdmin.error");

            return err;
        }


        // Create directories corresponding to share path
        String split_str[] = null;

        for (int j = 0; j < shareCmdsList.length; j++) {
            split_str = shareCmdsList[j].split(Util.SPACE);

            if (!(fileAccessor.createDir(split_str[split_str.length - 1],
                            null) &&
                        changePermissions(split_str[split_str.length - 1],
                            Util.NFS_SHAREDIR_PERMISSION).getReturnValue()
                        .booleanValue())) {
                err.setReturnVal(Boolean.FALSE);
                err.setErrorString("nfs.resultspanel.createShareDir.error");

                return err;
            }
        }

        // Need to restrict access to dfstab file created for nfs resource
        changePermissions(filePathName, Util.NFS_DFSTABFILE_PERMISSION);

        // Need to give execute permission to the PathPrefix directory
        changePermissions((String)helperDataMap.get(Util.PATHPREFIX),
            Util.NFS_PATHPREFIXDIR_PERMISSION);

        // Need to give execute permission to the PathPrefix directory
        return changePermissions(dirPath, Util.NFS_PATHPREFIXDIR_PERMISSION);
    }

    private String appendDir(String mtpt, String dir) {
        mtpt = mtpt.trim();
        dir = dir.trim();

        if (!dir.equals("") &&
                (mtpt.charAt(mtpt.length() - 1) != Util.SLASH.charAt(0)) &&
                (dir.charAt(0) != Util.SLASH.charAt(0))) {
            mtpt = mtpt + Util.SLASH;
        }

        mtpt = mtpt + dir;

        return mtpt;
    }

    private boolean isDfstabFilePresent(String nodeList[],
        String filePathName) {

        boolean retVal = false;
        HashMap tmpMap = discoverPossibilities(nodeList,
                new String[] { Util.NFS_DFSTAB_FILE }, filePathName);

        Object tmpObj = tmpMap.get(Util.NFS_DFSTAB_FILE);

        if (tmpObj != null) {
            retVal = true;
        }

        return retVal;
    }


    private ErrorValue changePermissions(String dirPath, String permissions) {
        ExitStatus status[] = null;
        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        try {
            List commands = new ArrayList();
            commands.add("/usr/bin/chmod");
            commands.add(permissions);
            commands.add(dirPath);

            ObjectNameFactory infraOnf = new ObjectNameFactory(Util.DOMAIN);
            ObjectName infraMBeanObjName = infraOnf.getObjectName(
                    InfrastructureMBean.class, null);
            InfrastructureMBean infrastructureMBean = (InfrastructureMBean)
                MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
                    infraMBeanObjName, InfrastructureMBean.class, false);
            String commandArr[] = new String[commands.size()];
            commandArr = (String[]) commands.toArray(commandArr);
            status = infrastructureMBean.executeCommand(commandArr);
        } catch (Exception e) {
            e.printStackTrace();

            if (e instanceof CommandExecutionException) {
                CommandExecutionException cee = (CommandExecutionException) e;
                status = cee.getExitStatusArray();

                return new ErrorValue(Boolean.FALSE,
                        "Command Tried to Execute : " +
                        DataServicesUtil.convertArrayToString(
                            status[0].getCmdExecuted()));
            }

            return new ErrorValue(Boolean.FALSE, e.getMessage());
        }

        return err;
    }

    /** 
     * convert NFS security option list to a String array and 
     * the first element of the array is "default"
     **/
    private String [] getShareOptionSecurityArray(List shareOptSecurity) {

        if (shareOptSecurity == null) return null;

        String [] securityOptions = 
            (String[])shareOptSecurity.toArray(new String[] {});
        String orginalFirstOption = securityOptions[0];

        if (!orginalFirstOption.equals(DEFAULT)) {
            // the first one is not "default", go through the array
            for (int i = 1; i < securityOptions.length; i++) {
                if (securityOptions[i].equals(DEFAULT)) {
                    // find the "default" one, exchange it with the first.
                    securityOptions[i] = orginalFirstOption;
                    securityOptions[0] = DEFAULT;
                    break;
                }
            }
        }

        return securityOptions;
    }
}
