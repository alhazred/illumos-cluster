/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)FileAccessor.java 1.13     08/05/20 SMI"
 */


package com.sun.cluster.agent.dataservices.utils;

// J2SE
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


/**
 * File Accessor MBean interface implementation.
 */
public class FileAccessor implements FileAccessorMBean {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.dataservices");
    private final static String logTag = "FileAccessor";

    /**
     * FileAccessor would not read the file if the size of given file exceeds
     * FILE_SIZE_READ_LIMIT. This parameter could be specified in Deployment
     * descriptor of this module.
     */
    private long FILE_SIZE_READ_LIMIT;


    /**
     * Constructor Function to initialize the FileAccessor object.
     *
     * @param  fileSizeReadLimit  File size exceeding the value of this
     * parameter would not be read.
     */
    public FileAccessor(long fileSizeReadLimit) {
        FILE_SIZE_READ_LIMIT = fileSizeReadLimit;
    }

    /**
     * Read the entire contents of the file. This function would return an
     * exception if the size of the file specified exceeds FILE_SIZE_READ_LIMIT.
     * This parameter could be specified in Deployment descriptor of this
     * module.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     *
     * @return  Returns an array of strings containing the file contents. Each
     * element of the array represents each line of the file. The tab in each
     * line of the file would be replaced by ":".
     */
    public String[] readFully(String filePathName) {

        String inBuffer;
        List listBuffer = new ArrayList();

        try {

            // Check for file size limitation for reading files.
            if (getFileSize(filePathName) > FILE_SIZE_READ_LIMIT)
                return null;

            BufferedReader reader = new BufferedReader(new FileReader(
                        filePathName));

            while ((inBuffer = reader.readLine()) != null)
                listBuffer.add(replace(inBuffer, "\\t", ":"));

            reader.close();
        } catch (FileNotFoundException e) {
            logger.fine("File Not Found : " + e.getMessage());
        } catch (Exception e) {
            logger.fine("Unable to read the file : " + e.getMessage());
        }

        return (String[]) listBuffer.toArray(new String[listBuffer.size()]);
    }


    /**
     * Writes the given string array to the given file at the specified path
     * name. Each string in the array would be written as a new line in the end
     * of the file with each occurrence of ':' replaced by tab.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     * @param  objToWrite  String array to be written to the file.
     */
    public void appendLine(String filePathName, String objToWrite[]) {

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(filePathName,
                        true));

            for (int i = 0; i < objToWrite.length; i++) {
                out.write(replace(objToWrite[i], ":", "\t") + "\n");
            }

            out.close();
        } catch (Exception e) {
            logger.fine("Unable to write to the file : " + e.getMessage());
        }
    }

    /**
     * Writes the given string array to the given file at the specified path
     * name. Each string in the array would be written as a new line in the end
     * of the file with occurrences of ':' retained if replaceColon is "false".
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     * @param  objToWrite  String array to be written to the file.
     * @param  replaceColon  boolean whether or not to replace the ":" with
     * "\t".
     */
    public boolean appendLine(String filePathName, String objToWrite[],
        boolean replaceColon) {

        boolean retVal = true;

        if (replaceColon == true) {
            appendLine(filePathName, objToWrite);
        } else {

            try {
                BufferedWriter out = new BufferedWriter(new FileWriter(
                            filePathName, true));

                for (int i = 0; i < objToWrite.length; i++) {
                    out.write(objToWrite[i] + "\n");
                }

                out.close();
            } catch (Exception e) {
                logger.fine("Unable to write to the file : " + e.getMessage());
                retVal = false;
            }
        }

        return retVal;
    }

    /**
     * Returns size of the file specified in bytes.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     *
     * @return  Size of the specified file in bytes.
     */
    public long getFileSize(String filePathName) {

        File file = new File(filePathName);

        return file.length();

    }


    /**
     * Checks for the existence of the file.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     *
     * @return  Boolean specifying the existence of the file.
     */
    public boolean ifFileExists(String filePathName) {

        File file = new File(filePathName);

        return file.exists();

    }


    /**
     * Creates the directory named by this abstract pathname, including any
     * necessary but nonexistent parent directories.
     *
     * @param  pathName  The pathname of the directory to be created
     *
     * @return  true if the command succeeds.
     */
    public boolean createDir(String pathName) {
        File file = new File(pathName);

        if (file.exists())
            return true;
        else
            return (file.mkdirs());
    }

    /**
     * Creates the file named by this abstract pathname even if the file exists.
     *
     * @param  pathName  The pathname of the directory to be created
     *
     * @return  true if the command succeeds.
     */
    public boolean createAndOverwriteFile(String pathName) {

        try {
            FileWriter out = new FileWriter(pathName);

            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    /**
     * Returns mode of the specified file.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     *
     * @return  Mode of the specified file as long.
     */
    public native long getFileMode(String filePathName);


    /**
     * Returns owner of the specified file.
     *
     * @param  filePathName  The name of the file prefixed with its absolute
     * path.
     *
     * @return  Owner of the file specified as long.
     */
    public native long getFileOwner(String filePathName);


    /**
     * Private Utility function, to replace occurrence of string 'pattern' in
     * string 'str' with string 'replace'.
     *
     * @param  str  The complete string to be updated.
     * @param  pattern  The string to be replaced.
     * @param  replace  The string to be replaced with.
     *
     * @return  The updated string after replacing all occurrences of 'pattern'
     * with 'replace'.
     */
    private String replace(String str, String pattern, String replace) {

        StringBuffer result = new StringBuffer();

        String buf[] = str.split(pattern);

        for (int i = 0; i < buf.length; i++) {
            result.append(buf[i]);

            if (i != (buf.length - 1))
                result.append(replace);
        }

        return result.toString();
    }

    /**
     * @deprecated
     */
    public void appendLine(String filePathName, String stringToWrite) {

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(filePathName,
                        true));

            out.write(replace(stringToWrite, ":", "\t") + "\n");
            out.close();
        } catch (Exception e) {
            logger.fine("Unable to write to the file : " + e.getMessage());
        }
    }
}
