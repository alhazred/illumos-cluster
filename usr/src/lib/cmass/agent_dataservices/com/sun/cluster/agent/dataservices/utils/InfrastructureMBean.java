/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)InfrastructureMBean.java 1.10     08/12/03 SMI"
 */

package com.sun.cluster.agent.dataservices.utils;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.ServiceConfig;

import java.io.IOException;


/**
 * This MBean interface defines general operations for configuring Dataservices
 */
public interface InfrastructureMBean extends ServiceConfig {

    /**
     * Executes the command specified by the array
     *
     * @param  command  Command to be executed eg : command[0] = "ls" command[1]
     * = "-a" executes ls -a
     */
    public ExitStatus[] executeCommand(String command[])
        throws CommandExecutionException;

    /**
     * Executes an operation on the specified MBean with the given parameters
     *
     * @param  mBeanName  Name of the MBean
     * @param  opName  OperationName
     * @param  paramTypes  String[] of ParamTypes
     * @param  paramValues  Object[] of ParamValues
     */
    public Object executeOperation(Class mBeanName, String opName,
        String paramTypes[], Object paramValues[]) throws Exception;

    /**
     * Serialize object to a specified file in the cluster node
     */
    public void writeObject(String fileName, Object object) throws IOException;

    /**
     * Checks whether a particular package has been installed on the cluster
     * node
     */
    public String[] checkPackages(String nodeList[], String servicePkg)
        throws Exception;

    /**
     * Check whether a particular MBean is accessible on the nodes. It returns
     * the list of error nodes
     */
    public String[] checkMBeans(String nodeList[], Class mBean)
        throws Exception;

    /**
     * Check whether the Cacao agent is accessible on the nodes. It returns the
     * list of error nodes
     */
    public String[] checkCacaoagent(String nodeList[]) throws Exception;
}
