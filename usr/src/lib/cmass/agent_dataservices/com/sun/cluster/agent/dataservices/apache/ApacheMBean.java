/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ApacheMBean.java 1.10     08/05/20 SMI"
 */


package com.sun.cluster.agent.dataservices.apache;

// J2SE
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.dataservices.common.*;
import com.sun.cluster.agent.dataservices.utils.*;

import java.util.List;
import java.util.Map;


/**
 * This interface defines the management operation available to configure
 * Apache.
 */
public interface ApacheMBean extends ServiceConfig {

    // Get the Handler to the ServiceInfo Structure .
    public DataServiceInfo getServiceInfo();

    // Set the ServiceInfo Structure on the MBean.
    public void setServiceInfo(DataServiceInfo serviceInfo);

    // Generate the List of Commands Based on the Configuration Information
    public List generateCommands(Map configurationInformation)
        throws CommandExecutionException;

    // Get the list of executed command list from serviceinfo object
    public List getCommandList();
}
