/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)Apache.java 1.29     08/12/19 SMI"
 */


package com.sun.cluster.agent.dataservices.apache;

// J2SE

// CACAO
import com.sun.cacao.ObjectNameFactory;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.*;
import com.sun.cluster.agent.dataservices.logicalhost.LogicalHost;
import com.sun.cluster.agent.dataservices.sharedaddress.SharedAddress;
import com.sun.cluster.agent.dataservices.utils.*;
import com.sun.cluster.agent.node.ZoneMBean;
import com.sun.cluster.agent.rgm.ResourceGroupMBean;
import com.sun.cluster.agent.rgm.ResourceGroupModeEnum;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.net.Inet4Address;
import java.net.InetAddress;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.AttributeValueExp;

// JMX
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.StringValueExp;


/**
 * This interface defines the management operations available to configure a
 * Apache Resource
 */
public class Apache implements ApacheMBean {

    /* RTName of Apache MBean */
    public static final String RTNAME = "SUNW.apache";

    public static final String FAILOVER = "Failover";
    public static final String SCALABLE = "Scalable";

    private static final String TCP = "tcp";
    private static final String TCP6 = "tcp6";

    // Path for Scalable Apache XML
    private static final String SCALABLE_APACHE_XML =
        "/opt/cluster/lib/ds/registry/ScalableApacheServiceConfig.xml";

    private static final String ROOT = "/root";

    // PATH for apache configuration script
    private static final String APACHE_CONFIG_SCRIPT =
        "/usr/cluster/lib/ds/apache/configureApache.ksh";

    // Default Name for apache rg
    private static final String APACHE_RG_PREFIX = "apache-server";

    // Default Name for apache resource
    private static final String APACHE_RES_PREFIX = "apache-";

    /* The MBeanServer in which this MBean is inserted. */
    private MBeanServer mBeanServer;

    private ObjectNameFactory onf = null;

    /* Command Generation Structure for this Dataservice */
    private DataServiceInfo serviceInfo = null;

    /* Object Name Factory for RGM Module */
    private ObjectNameFactory rgmOnf = null;

    /* Command Generation Structure for ScalableApache */
    private DataServiceInfo scalableServiceInfo = null;

    /* Reference to Configuration Class */
    private ServiceConfig apacheConfig = null;

    /* Reference to FileAccessorMbean */
    private FileAccessorMBean fileMBean;

    /* List of commands executed */
    private List commandList = null;


    /**
     * Default constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this MBean.
     */
    public Apache(MBeanServer mBeanServer, ObjectNameFactory onf,
        DataServiceInfo serviceInfo) {

        try {
            this.mBeanServer = mBeanServer;
            this.serviceInfo = serviceInfo;
            this.scalableServiceInfo = new DataServiceInfo(new File(
                        SCALABLE_APACHE_XML));
            this.rgmOnf = onf;

            // Get an instance of ConfigurationClass
            apacheConfig = serviceInfo.getConfigurationClass(this.getClass()
                    .getClassLoader());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Returns the ServiceInfo Structure for this DataserviceMBean
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public DataServiceInfo getServiceInfo() {
        return this.serviceInfo;
    }

    /**
     * Sets the ServiceInfo Structure for this DataserviceMBean
     *
     * @param  serviceInfo  Command Generation Structure
     */
    public void setServiceInfo(DataServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    /**
     * Returns list of executed command list
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public List getCommandList() {
        return this.commandList;
    }

    /**
     * Generates the set of commands for configuring this service, given the
     * configuration information. This command uses the ServiceInfo structure,
     * fills the same and generates the command list. This method needs to be
     * synchronized because this method operates on Wizard instance specific
     * information. The LogicalHost MBean is a singleton,if we can maitain an
     * unique instance of this MBean for every wizard instance we need not
     * syncronize.
     *
     * @param  configurationInformation  ConfigurationInformation for
     * configuring an instance of the Dataservice
     *
     * @return  Commands Generated for the given ConfigurationInformation
     */
    public synchronized List generateCommands(Map configurationInformation)
        throws CommandExecutionException {

        // Depending on the Apache Mode use the respective
        // ServiceInfo objects
        String apacheMode = (String) configurationInformation.get(
                Util.APACHE_MODE);

        try {

            // The Storage Resource should be brought online after command
            // Generation. We get the handler for this resource and
            // set bringOnline to true
            ResourceInfo storageResourceInfo;
            ResourceInfo apacheResourceInfo;

            if (configurationInformation.containsKey(
                    Util.QFS_RES_ID + Util.NAME_TAG)) {

                if (apacheMode.equals(FAILOVER)) {
                    storageResourceInfo = serviceInfo.getResource(
                        Util.QFS_RES_ID);
                } else {
                    storageResourceInfo = scalableServiceInfo.getResource(
                        Util.QFS_RES_ID);
                }

            } else {

                if (apacheMode.equals(FAILOVER)) {
                    storageResourceInfo = serviceInfo.getResource(
                        Util.STORAGERESOURCE);
                } else {
                    storageResourceInfo = scalableServiceInfo.getResource(
                        Util.STORAGERESOURCE);
                }
            }

            if (apacheMode.equals(FAILOVER)) {
                apacheResourceInfo = serviceInfo.getResource(
                        Util.APACHERESOURCE);
            } else {
                apacheResourceInfo = scalableServiceInfo.getResource(
                        Util.APACHERESOURCE);
                scalableServiceInfo.getResource(Util.SHAREDADDRESS)
                .setBringOnline(true);
            }

            storageResourceInfo.setBringOnline(true);
            apacheResourceInfo.setBringOnline(true);

            String apacheConfFilePath = (String) configurationInformation.get(
                    Util.APACHE_CONF_FILE);
            String apacheDoc = (String) configurationInformation.get(
                    Util.APACHE_DOC);
            String mountPoint = (String) configurationInformation.get(
                    Util.APACHE_MOUNT);
            String serverRoot = (String) configurationInformation.get(
                    Util.APACHE_HOME);
            String currentNode = (String) configurationInformation.get(
                    Util.NODE_TO_CONNECT);
            String apacheRes = (String) configurationInformation.get(
                    Util.APACHE_RESOURCE_NAME);

            configurationInformation.remove(Util.APACHE_MODE);
            configurationInformation.remove(Util.APACHE_CONF_FILE);
            configurationInformation.remove(Util.APACHE_DOC);
            configurationInformation.remove(Util.APACHE_MOUNT);
            configurationInformation.remove(Util.APACHE_HOME);
            configurationInformation.remove(Util.NODE_TO_CONNECT);
            configurationInformation.remove(Util.APACHE_RESOURCE_NAME);

            // Create a configurationMap which would hold the configuration
            // commands, operations to be made for Apache
            Map confMap = new HashMap();

            String confFileName = apacheConfFilePath.substring(
                    apacheConfFilePath.lastIndexOf('/') + 1,
                    apacheConfFilePath.length());

            // create command array
            List tmpCommandList = new ArrayList();
            tmpCommandList.add(APACHE_CONFIG_SCRIPT);
            tmpCommandList.add("copyConfiguration");
            tmpCommandList.add(currentNode);
            tmpCommandList.add(mountPoint);
            tmpCommandList.add(confFileName);
            apacheDoc = apacheDoc.replace('\"', ' ');
            apacheDoc = apacheDoc.trim();
            tmpCommandList.add(apacheDoc);

            if (apacheDoc.indexOf(mountPoint, 0) == 0) {

                // Doc Root was specified along with mounpoint path (or)
                // Doc Root and Mount Point are the same
                // Doc Root need not be copied at all
                tmpCommandList.add("/");
            } else {

                // Doc root has no relation with the mountpoint
                // Copy DocRoot at the <mount point>/<apacjhe-res-name>
                // directory
                tmpCommandList.add(mountPoint + Util.SLASH + apacheRes);
            }

            tmpCommandList.add(apacheRes);

            System.out.println(tmpCommandList);

            String commands[] = new String[tmpCommandList.size()];
            commands = (String[]) tmpCommandList.toArray(commands);

            confMap.put("command", commands);

            // set the map
            storageResourceInfo.setCustomConfigureMap(confMap);

            if (apacheMode.equals(FAILOVER)) {

                // Fill the ServiceInfo object with the configurationInformation
                serviceInfo.fillServiceInfoStructure(configurationInformation);

                // Generate CommandSet for this Service
                serviceInfo.generateCommandSet(mBeanServer, rgmOnf);
                commandList = serviceInfo.getCommandSet();

            } else {

                List rsExtProp = (List)configurationInformation.get(
                    Util.STORAGERESOURCE + Util.EXTPROP_TAG);

                if (rsExtProp != null) { 

                    // it's HASP, set AFFINITY_ON to false for scalable apache
                    ResourceProperty rp = new ResourcePropertyBoolean(
                        Util.AFFINITY_ON, Boolean.FALSE);
                    rsExtProp.add(rp);
                }

                // Fill the ServiceInfo object with the configurationInformation
                scalableServiceInfo.fillServiceInfoStructure(
                    configurationInformation);

                // Generate CommandSet for this Service
                scalableServiceInfo.generateCommandSet(mBeanServer, rgmOnf);
                commandList = scalableServiceInfo.getCommandSet();
            }

        } catch (Exception e) {

e.printStackTrace();
            // Unconfigure the Groups and Resources
            try {

                if (apacheMode.equals(FAILOVER)) {
                    commandList = serviceInfo.getCommandSet();

                    // add Rollback Success string to the command list - this
                    // would be used as a marker to demarcate configuration and
                    // rollback commands
                    commandList.add(Util.ROLLBACK_SUCCESS);
                    serviceInfo.rollBack(mBeanServer, rgmOnf);
                    commandList.addAll(serviceInfo.getCommandSet());
                } else {
                    commandList = scalableServiceInfo.getCommandSet();

                    // add Rollback Success string to the command list - this
                    // would be used as a marker to demarcate configuration and
                    // rollback commands
                    commandList.add(Util.ROLLBACK_SUCCESS);
                    scalableServiceInfo.rollBack(mBeanServer, rgmOnf);
                    commandList.addAll(scalableServiceInfo.getCommandSet());
                }
            } catch (Exception re) {

                // Rollback Failed
                // add Rollback Fail string in place of Rollback Success string
                // to the command list - this would be used as a marker to
                // demarcate configuration and rollback commands
                commandList.remove(Util.ROLLBACK_SUCCESS);
                commandList.add(Util.ROLLBACK_FAIL);
                commandList.addAll(serviceInfo.getCommandSet());
            }

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException) e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        } finally {

            // Always reset the structure to the original form
            serviceInfo.reset();
            scalableServiceInfo.reset();
        }

        return commandList;
    }


    // Implementation For the ServiceConfig Interface Methods
    public String[] getDiscoverableProperties() {

        try {

            // Call the GetDiscoverable Properties on the configuration class
            String discoverableProps[] = apacheConfig
                .getDiscoverableProperties();

            return discoverableProps;
        } catch (Exception e) {
            System.out.println("Exception in Get Discoverable Props : " +
                e.getMessage());
        }

        return null;
    }

    public String[] getAllProperties() {

        try {
            // Add the following properties for Apache and other resources
            String rgProperties[] = {
                    Util.APACHE_MODE, Util.APACHE_RESOURCE_NAME,
                    Util.APACHE_RESOURCE_GROUP, Util.APACHE_SHAREDADDRESS_GROUP,
                    Util.LH_RESOURCE_NAME, Util.SH_RESOURCE_NAME,
                    Util.STORAGE_RESOURCES
                };

            // Get the properties from the Configuration Class
            String dataServiceAllProps[] = apacheConfig.getAllProperties();

            return DataServicesUtil.concatenateArrays(rgProperties,
                    dataServiceAllProps);
        } catch (Exception e) {
            System.out.println("Exception in Get All Props : " +
                e.getMessage());
        }

        return null;
    }

    public HashMap discoverPossibilities(String propertyNames[],
        Object helperData) {

        HashMap resultMap = null;

        // Discover PropertyNames : This is called while Initializing the
        // WizardContext Model
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.PROPERTY_NAMES)) {

            // Get the discoverableProperty Values
            String discoverableProps[] = getDiscoverableProperties();

            for (int i = 0; i < discoverableProps.length; i++) {
                String propertyName[] = { discoverableProps[i] };

                if (resultMap == null) {
                    resultMap = new HashMap();
                    resultMap = discoverPossibilities(propertyName, null);
                } else {
                    resultMap.putAll(discoverPossibilities(propertyName, null));
                }
            }

            // Add other propertyNames to the resultMap
            String allProps[] = getAllProperties();

            for (int i = 0; i < allProps.length; i++) {
                String propertyName = allProps[i];

                if (resultMap == null) {
                    resultMap = new HashMap();
                }

                // Omit the Discovered Properties
                if (!resultMap.containsKey(propertyName)) {
                    resultMap.put(propertyName, null);
                }
            }

            return resultMap;
        }

        // Discover unique name for ApacheResourceGroup
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_RESOURCE_GROUP)) {

            // Generated Name <apache-server><count>-rg
            resultMap = new HashMap();

            String rgName;

            // apache-server-rg
            rgName = APACHE_RG_PREFIX + "-rg";

            // Check if the rgName already exists
            if (DataServicesUtil.isRGNameUsed(mBeanServer, rgName)) {
                rgName = APACHE_RG_PREFIX;

                int count = 1;

                while (DataServicesUtil.isRGNameUsed(mBeanServer,
                            rgName + count)) {
                    count++;
                }

                rgName = rgName + count;
                rgName = rgName + "-rg";
            }

            resultMap.put(Util.APACHE_RESOURCE_GROUP, rgName);

            return resultMap;
        }

        // Discover unique name for Apache Resource
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_RESOURCE_NAME)) {

            resultMap = new HashMap();

            String rName;
            String hostNames = (String) helperData;

            // Generated Name apache-<listen>-rs-<count>
            rName = APACHE_RES_PREFIX + hostNames.replace(',', '-') + "-rs";

            // Check if the name already exists
            if (DataServicesUtil.isResourceNameUsed(mBeanServer, rName)) {
                int count = 1;

                while (DataServicesUtil.isResourceNameUsed(mBeanServer,
                            rName + "-" + count)) {
                    count++;
                }

                rName = rName + "-" + count;
            }

            resultMap.put(Util.APACHE_RESOURCE_NAME, rName);

            return resultMap;
        }

        // Discover NodeList
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.NODELIST)) {
            resultMap = new HashMap();
            resultMap.put(Util.NODELIST,
                DataServicesUtil.getNodeList(mBeanServer));
        }

        // Discover the Zone Export Path of a Zone
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_ZONE)) {

            String zone = (String) helperData;
            resultMap = new HashMap();

            String zoneExportPath = null;

            try {

                if (zone != null) {

                    // Get the zone path for this zone
                    ObjectNameFactory zoneOnf = new ObjectNameFactory(
                            ZoneMBean.class.getPackage().getName());
                    ObjectName zoneNamePattern = zoneOnf.getObjectNamePattern(
                            ZoneMBean.class);
                    QueryExp query = Query.match(new AttributeValueExp("Name"),
                            new StringValueExp(zone));
                    Set zoneMBeans = mBeanServer.queryNames(zoneNamePattern,
                            query);

                    if (zoneMBeans.size() > 0) {
                        Iterator setIterator = zoneMBeans.iterator();
                        ObjectName zoneObjectName = (ObjectName) setIterator
                            .next();
                        ZoneMBean zoneMbean = (ZoneMBean)
                            MBeanServerInvocationHandler.newProxyInstance(
                                mBeanServer, zoneObjectName, ZoneMBean.class,
                                false);

                        if (zone.split(Util.COLON).length == 2) {
                            zone = (zone.split(Util.COLON))[1];

                            ExitStatus status[] = zoneMbean.getZoneDetails(
                                    zone);

                            if ((status.length > 0) &&
                                    (status[0].getReturnCode().intValue() ==
                                        status[0].SUCCESS)) {
                                List output = status[0].getOutStrings();

                                if ((output != null) && (output.size() > 0)) {
                                    String outLine = (String) output.get(0);
                                    String zoneDetails[] = outLine.split(
                                            Util.COLON);

                                    // The solaris command output is parsed
                                    // for the zoneExportPath
                                    if ((zoneDetails != null) &&
                                            (zoneDetails.length >= 4)) {

                                        // Add /root to the export Path
                                        zoneExportPath = zoneDetails[3] + ROOT;
                                    } else {
                                        zoneExportPath = "/";
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                zoneExportPath = "/";
            }

            resultMap.put(Util.APACHE_ZONE, zoneExportPath);

            return resultMap;
        }

        // Discover Apache Configuration File for eache Node/Zone in the
        // Nodelist.
        // If the helper data specifies a zone (in the format nodeName:zone),
        // the zoneExport path of the specified zone is passed to get
        // the list of apache configuration files.
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_CONF_FILE)) {

            Map helperMap = (HashMap) helperData;
            String zone = null;

            if (helperMap != null) {
                zone = (String) helperMap.get(Util.APACHE_ZONE);
            }

            String zoneExportPath = null;
            ArrayList apacheHomeList = null;
            String apacheHome = null;

            // Get the ZonePath for a zone
            try {

                if (zone != null) {

                    // Get the zone path for this zone
                    ObjectNameFactory zoneOnf = new ObjectNameFactory(
                            ZoneMBean.class.getPackage().getName());
                    ObjectName zoneNamePattern = zoneOnf.getObjectNamePattern(
                            ZoneMBean.class);
                    QueryExp query = Query.match(new AttributeValueExp("Name"),
                            new StringValueExp(zone));
                    Set zoneMBeans = mBeanServer.queryNames(zoneNamePattern,
                            query);

                    if (zoneMBeans.size() > 0) {
                        Iterator setIterator = zoneMBeans.iterator();
                        ObjectName zoneObjectName = (ObjectName) setIterator
                            .next();
                        ZoneMBean zoneMbean = (ZoneMBean)
                            MBeanServerInvocationHandler.newProxyInstance(
                                mBeanServer, zoneObjectName, ZoneMBean.class,
                                false);

                        if (zone.split(Util.COLON).length == 2) {
                            zone = (zone.split(Util.COLON))[1];

                            ExitStatus status[] = zoneMbean.getZoneDetails(
                                    zone);

                            if ((status.length > 0) &&
                                    (status[0].getReturnCode().intValue() ==
                                        status[0].SUCCESS)) {
                                List output = status[0].getOutStrings();

                                if ((output != null) && (output.size() > 0)) {
                                    String outLine = (String) output.get(0);
                                    String zoneDetails[] = outLine.split(
                                            Util.COLON);

                                    // The solaris command output is parsed
                                    // for the zoneExportPath
                                    if ((zoneDetails != null) &&
                                            (zoneDetails.length == 4)) {
                                        zoneExportPath = zoneDetails[3] + ROOT;
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (CommandExecutionException cee) {
                zoneExportPath = null;
            }

            return apacheConfig.discoverPossibilities(propertyNames,
                    (Object) zoneExportPath);
        }

        // Discover APACHE_PORT from the configuration file
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_PORT)) {
            HashMap discoveredValue = apacheConfig.discoverPossibilities(
                    propertyNames, helperData);
            String port[] = (String[]) discoveredValue.get(Util.APACHE_PORT);

            if ((port == null) || (port.length == 0)) {
                discoveredValue.put(Util.APACHE_PORT, new String[] { "80" });
            }

            return discoveredValue;
        }

        // Discover APACHE_HOME from the configuration file
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_HOME)) {
            return apacheConfig.discoverPossibilities(propertyNames,
                    helperData);
        }

        // Discover APACHE_DOC from the configuration file
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_DOC)) {
            return apacheConfig.discoverPossibilities(propertyNames,
                    helperData);
        }

        // Discover PIDFile Directory from the configuration file
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.PIDFILE_DIR)) {
            return apacheConfig.discoverPossibilities(propertyNames,
                    helperData);
        }

        // Discover if PORT_LIST needs to be tcp or tcp/6
        // Helper data is APACHE_LISTEN with the hostname arrays
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.PORT_LIST)) {
            String hostnames[] = (String[]) helperData;
            resultMap = new HashMap();

            for (int i = 0; i < hostnames.length; i++) {

                if (DataServicesUtil.isHostNameIPv6(hostnames[i])) {
                    resultMap.put(Util.PORT_LIST, TCP6);

                    return resultMap;
                }
            }

            resultMap.put(Util.PORT_LIST, TCP);

            return resultMap;
        }

        // Discover APACHE_LISTEN from the LH or SH resource
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_LISTEN)) {
            String listenResource = (String) helperData;

            String hostResource = null;
            String hostNames[] = null;

            // Search in Both the LH and SH Resource
            QueryExp query = Query.or(Query.match(new AttributeValueExp("Type"),
                        Query.value(LogicalHost.RTNAME + "*")),
                    Query.match(new AttributeValueExp("Type"),
                        Query.value(SharedAddress.RTNAME + "*")));

            // Get the Objectname Pattern For ResourceMBean Instances
            ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                    ResourceMBean.class);
            Set rset = mBeanServer.queryNames(rObjectName, query);

            // Return null if there are no Resources
            if (rset.size() == 0) {
                return null;
            }

            Iterator rsetIter = rset.iterator();

            while (rsetIter.hasNext()) {

                // Get Handler to the HostResource MBean
                ObjectName hostMBeanObjName = (ObjectName) rsetIter.next();
                ResourceMBean hostMBean = (ResourceMBean)
                    MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
                        hostMBeanObjName, ResourceMBean.class, false);

                // Get the Name of the HostResource
                hostResource = hostMBean.getName();

                if (hostResource.equals(listenResource)) {

                    // Get the HostName List
                    List extensionProperties = hostMBean.getExtProperties();
                    Iterator extIter = extensionProperties.iterator();

                    while (extIter.hasNext()) {
                        ResourceProperty hostExtProp = (ResourceProperty)
                            extIter.next();

                        if (hostExtProp.getName().equals(Util.HOSTNAMELIST)) {
                            hostNames = (String[]) DataServicesUtil
                                .getResourcePropertyValue(hostExtProp, false);

                            break;
                        }
                    }

                    resultMap = new HashMap();
                    resultMap.put(Util.APACHE_LISTEN, hostNames);

                    break;
                }
            }

            return resultMap;
        }

        // Discover Zone Enable
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.ZONE_ENABLE)) {
            resultMap = (HashMap) apacheConfig.discoverPossibilities(
                    new String[] { Util.ZONE_ENABLE }, null);

            return resultMap;
        }

        // Discover SharedAddress for Scalable Apache
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.SH_NAMES)) {

            resultMap = new HashMap();

            Map helperMap = (HashMap) helperData;
            String apacheNodeList[] = (String[]) helperMap.get(
                    Util.RG_NODELIST);
            String props[] = new String[] { Util.LH_HOSTLIST_EXT_PROP };
            List rgList = DataServicesUtil.getRGList(mBeanServer,
                    apacheNodeList, Util.SH_RTNAME, props);
            resultMap.put(Util.SH_RGLIST, rgList);

            return resultMap;
        }

        // Discover LogicalHost For Failover Apache
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.LH_NAMES)) {

            resultMap = new HashMap();

            Map helperMap = (HashMap) helperData;
            String apacheResourceGroup = (String) helperMap.get(
                    Util.APACHE_RESOURCE_GROUP);
            String apacheNodeList[] = (String[]) helperMap.get(
                    Util.RG_NODELIST);
            String props[] = new String[] { Util.LH_HOSTLIST_EXT_PROP };
            List rgList = null;

            // If apacheResourceGroup is known, discover logicalhost resources
            // in that group
            if (apacheResourceGroup != null) {
                QueryExp query = Query.and(Query.match(
                            new AttributeValueExp("Type"),
                            Query.value(Util.LH_RTNAME + "*")),
                        Query.eq(new AttributeValueExp("ResourceGroupName"),
                            Query.value(apacheResourceGroup)));

                // Get the ObjectName Factory for RGM CMASS Module
                ObjectNameFactory rgmOnf = new ObjectNameFactory(
                        ResourceMBean.class.getPackage().getName());

                ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                        ResourceMBean.class);

                Set rset = mBeanServer.queryNames(rObjectName, query);

                if (rset != null) {

                    if (rgList == null) {
                        rgList = new ArrayList();
                    }

                    Iterator i = rset.iterator();

                    while (i.hasNext()) {
                        ObjectName rsObjName = (ObjectName) i.next();
                        String rsName = rgmOnf.getInstanceName(rsObjName);
                        HashMap map = new HashMap();
                        map.put(Util.RG_NAME, apacheResourceGroup);
                        map.put(Util.RS_NAME, rsName);

                        for (int j = 0; j < props.length; j++) {
                            Object obj = DataServicesUtil
                                .getResourcePropertyValue(mBeanServer, rsName,
                                    props[j]);
                            map.put(props[j], obj);
                        }

                        rgList.add(map);
                    }
                }
            } else {
                rgList = DataServicesUtil.getRGList(mBeanServer, apacheNodeList,
                        Util.LH_RTNAME, props);
            }

            resultMap.put(Util.LH_RGLIST, rgList);

            return resultMap;
        }

        // Discover stroage Resources/MountPoints depending on Apache Mode
        // and node list
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.STORAGE_RESOURCES)) {

            Map helperMap = (HashMap) helperData;
            String apacheMode = (String) helperMap.get(Util.APACHE_MODE);
            String nodeList[] = (String[]) helperMap.get(Util.RG_NODELIST);

            /*
             * If Apache Mode is Scalable, discover only those HASP Resources
             * with pxfs(GLOBAL) mount points otherwise discover all HASP Res
             *
             * The ResultMap would be :
             * HASP ResourceName (Key) - MountPoints (Value) (PXFS if scalable)
             */
            String storageResource = null;
            String mountPoints[] = null;

            QueryExp query = Query.or(
                Query.match(new AttributeValueExp("Type"), 
                    Query.value(Util.HASP_RTNAME + "*")),
                Query.match(new AttributeValueExp("Type"), 
                    Query.value(Util.QFS_RTNAME + "*")));

            // Get the Objectname Pattern For ResourceMBean Instances
            ObjectName rObjectName = rgmOnf.getObjectNamePattern(
                    ResourceMBean.class);
            Set rset = mBeanServer.queryNames(rObjectName, query);

            // Return null if there are no storage Resource
            if (rset.size() == 0) {
                return null;
            }

            resultMap = new HashMap();

            Iterator rsetIter = rset.iterator();

            while (rsetIter.hasNext()) {

                // Get Handler to the Storage(either HASP or QFS) Resource MBean
                ObjectName storageMBeanObjName = (ObjectName) rsetIter.next();
                ResourceMBean storageMBean = (ResourceMBean)
                    MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
                        storageMBeanObjName, ResourceMBean.class, false);

                // Get Handler to the Resource Group MBean
                String storageResourceGroup = storageMBean.getResourceGroupName();

                ObjectName storageRGMBeanObjName = rgmOnf.getObjectName(
                        ResourceGroupMBean.class, storageResourceGroup);
                ResourceGroupMBean storageRGMBean = (ResourceGroupMBean)
                    MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
                        storageRGMBeanObjName, ResourceGroupMBean.class, false);
                ResourceGroupModeEnum storageRGMode = storageRGMBean.getMode();
                String storageRGNodeList[] = storageRGMBean.getNodeNames();

                // Check if user selected nodelist and RG's nodelist
                // are same
                List storageRGNodes = Arrays.asList(storageRGNodeList);
                List userNodes = Arrays.asList(nodeList);

                if ((storageRGNodeList.length == nodeList.length) &&
                        (userNodes.containsAll(storageRGNodes))) {

                    // If its scalable apache, then we look into
                    // HASP resources only in Scalable Group.
                    // If its failover apache, then we look into
                    // HASP resources only in Failover Group.
                    // QFS resources can be used for both
                    if ((apacheMode.equals(FAILOVER) &&
                                (storageRGMode.compareTo(
                                        ResourceGroupModeEnum.FAILOVER) ==
                                    0)) ||
                            (apacheMode.equals(SCALABLE) &&
                                (storageRGMode.compareTo(
                                        ResourceGroupModeEnum.SCALABLE) ==
                                    0)) ||
                            storageMBean.getType().indexOf(Util.QFS_RTNAME) != -1) {

                        // Get the Name of the storage Resource
                        storageResource = storageMBean.getName();

                        // Get the mount point
                        List extensionProperties = storageMBean.getExtProperties();

                        Iterator extIter = extensionProperties.iterator();

                        while (extIter.hasNext()) {
                            ResourceProperty storageExtProp = (ResourceProperty)
                                extIter.next();

                            if (storageExtProp.getName().equals(
                                        Util.HASP_ALL_FILESYSTEMS) ||
                                    storageExtProp.getName().equals(
                                        Util.QFS_FILESYSTEM)) { 
                                mountPoints = (String[]) DataServicesUtil
                                    .getResourcePropertyValue(storageExtProp,
                                        false);
                                break;
                            }
                        }

                        resultMap.put(storageResource, mountPoints);
                    }
                }
            }

            return resultMap;
        }

        // Find out the version of apache used for configuration
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_VERSION)) {

            HashMap userInputs = (HashMap) helperData;
            String confFilePath = (String) userInputs.get(Util.CONF_FILE);
            String zoneExportPath = (String) userInputs.get(Util.APACHE_ZONE);
            resultMap = new HashMap();

            // Add zoneExportPath if conf file is inside a zone
            if (zoneExportPath != null) {
                confFilePath = zoneExportPath + confFilePath;
            }

            // Check Conf File
            // The Conf File is Parsed for ServerRoot
            HashMap discoveredValue = discoverPossibilities(
                    new String[] { Util.APACHE_HOME }, confFilePath);
            String serverRoot[] = (String[]) discoveredValue.get(
                    Util.APACHE_HOME);

            if ((serverRoot != null) && (serverRoot.length > 0)) {

                // Validate using the discovered ServerRoot
                // The conf file is validated against any syntax errors
                // and whether the ServerRoot points to the supported
                // Apache version
                if (zoneExportPath != null) {
                    serverRoot[0] = zoneExportPath + serverRoot[0];
                }

                return (apacheConfig.discoverPossibilities(propertyNames,
                            serverRoot[0]));
            }

            return resultMap;
        }

        return resultMap;
    }

    public HashMap discoverPossibilities(String nodeList[],
        String propertyNames[], Object helperData) {
        return null;
    }

    public ErrorValue validateInput(String propertyNames[], HashMap userInputs,
        Object helperData) {

        // Validate ResourceGroup Name
        if (propertyNames[0].equals(Util.RESOURCEGROUPNAME)) {
            String rgName = (String) userInputs.get(Util.RESOURCEGROUPNAME);

            if (DataServicesUtil.isRGNameUsed(mBeanServer, rgName)) {
                return new ErrorValue(new Boolean(false), " ");
            }

            return new ErrorValue(new Boolean(true), " ");
        }
        // Validate Resource Name
        else if (propertyNames[0].equals(Util.RESOURCENAME)) {
            String rName = (String) userInputs.get(Util.RESOURCENAME);

            if (DataServicesUtil.isResourceNameUsed(mBeanServer, rName)) {
                return new ErrorValue(new Boolean(false), " ");
            }

            return new ErrorValue(new Boolean(true), " ");
        }

        // Validation of APACHE_PORT
        // UserInputs contains - Port_List and Host_Name
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_PORT)) {
            return apacheConfig.validateInput(propertyNames, userInputs,
                    helperData);
        }

        // Validate APACHE_DOC ROOT
        // UserInputs contains - DOC_ROOT location
        // The DOCROOT should exists and should be a directory
        // with read permissions
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_DOC)) {
            String docRootLocation = (String) userInputs.get(Util.APACHE_DOC);
            File docRoot = new File(docRootLocation);

            if (!docRoot.exists() || !docRoot.isDirectory()) {
                return new ErrorValue(new Boolean(false),
                        "apache.docRootPanel.docRootNotFoundError");
            } else if (!docRoot.canRead()) {
                return new ErrorValue(new Boolean(false),
                        "apache.docRootPanel.docRootNotReadable");
            } else {
                return new ErrorValue(new Boolean(true), " ");
            }
        }

        // Validation of Apache Configuration File :
        // -- Validate if the File exits in the current node
        // or someother node or device (if specified)
        // -- Check Syntax of the file
        // -- Server Root should be pointing to a valid apache version
        if ((propertyNames.length == 1) &&
                propertyNames[0].equals(Util.APACHE_CONF_FILE)) {

            String confFilePath = (String) userInputs.get(Util.CONF_FILE);
            String zoneExportPath = (String) userInputs.get(Util.APACHE_ZONE);

            // Add zoneExportPath if conf file is inside a zone
            if (zoneExportPath != null) {
                confFilePath = zoneExportPath + confFilePath;
            }

            // Check Existence
            File confFile = new File(confFilePath);

            if (!confFile.canRead() || confFile.isDirectory()) {
                return new ErrorValue(new Boolean(false),
                        "apache.confFilePanel.fileNotFoundError");
            } else {

                // Check Conf File
                // The Conf File is Parsed for ServerRoot
                HashMap discoveredValue = discoverPossibilities(
                        new String[] { Util.APACHE_HOME }, confFilePath);
                String serverRoot[] = (String[]) discoveredValue.get(
                        Util.APACHE_HOME);

                if ((serverRoot != null) && (serverRoot.length > 0)) {

                    // Validate using the discovered ServerRoot
                    // The conf file is validated against any syntax errors
                    // and whether the ServerRoot points to the supported
                    // Apache version
                    if (zoneExportPath != null) {
                        serverRoot[0] = zoneExportPath + serverRoot[0];
                    }

                    return apacheConfig.validateInput(propertyNames, userInputs,
                            serverRoot[0]);
                } else {

                    // If ServerRoot could not be located, we should error out
                    return new ErrorValue(new Boolean(false),
                            "apache.confFilePanel.rootNotFoundError");
                }
            }
        }

        return null;
    }

    public ErrorValue applicationConfiguration(Object helperData) {
        return apacheConfig.applicationConfiguration(helperData);
    }

    public String[] aggregateValues(String propertyName, Map unfilteredValues,
        Object helperData) {
        return null;
    }
}
