/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)ResourceGroupInfo.java	1.24	09/02/10 SMI"
 */


package com.sun.cluster.agent.dataservices.utils;

// J2SE
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

// JMX
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;

// CACAO
import com.sun.cacao.ObjectNameFactory;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.rgm.RgGroupMBean;
import com.sun.cluster.agent.rgm.ResourceGroupData;

/**
 * The ResourceGroupInfo class wraps the ResourceGroup information for a
 * ResourceGroup for a DataService
 *
 * <p>This class provides set of member variables and methods for configuring a
 * ResourceGroup.
 */
public class ResourceGroupInfo {

    private String groupName = null; // Logical Name of the ResourceGroup
    private String name = null; // Actual Name of the Group in Cluster
    private String type = null; // Type of the Group
    private boolean optional; // Is this Group optional

    private List dependency = null; // List of Group This Group Depends upon
    private List dependent = null; // List of groups that depend on this group
    private List resources = null; // List of Resources to be configured in this
                                   // Group
    private Map rgProperties = null; // RGProperties for this ResourceGroup.
    private boolean groupConfigured; // Specifies whether the group is
                                     // configured or not.
    private boolean newRG = true; // By Default create a New ResourceGroup
    private boolean groupRollbacked; // Specifies whether the group is
                                     // rollbacked or not
    // the name of the zone cluster or null for base cluster
    private String zoneClusterName = null;

    // set runCmd flag to true
    private boolean runCmd = true;

    /**
     * Creates a new instance of ResourceGroupInfo
     */
    public ResourceGroupInfo() {
    }

    /**
     * Getter method for the ResourceGroup's logical name as specified in the
     * ServiceConfig XML
     *
     * @return  Logical Name of the ResourceGroup as specified in the
     * ServiceConfig XML
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Setter method for the ResourceGroup's logical name.
     *
     * @param  groupName  The Logical Name of the ResourceGroup.
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * Getter method for the ResourceGroup name as it would be in SunCluster.
     *
     * @return  Name of the ResourceGroup as it is in SunCluster
     */
    public String getName() {
        return name;
    }


    /**
     * Creates a copy of this ResourceGroup including the Resource structures
     *
     * @return  Cloned ResourceGroup
     */
    public ResourceGroupInfo copyGroup() {
        ResourceGroupInfo group = new ResourceGroupInfo();
        group.setGroupName(groupName);
        group.setType(type);
        group.setOptional(optional);

        // Cloned group points to the same dependency Groups' references
        group.setDependency(dependency);
        group.setDependent(dependent);

        // Clone the ResourceInfo objects inside this group
        for (Iterator i = resources.iterator(); i.hasNext();) {

            // Clone a resource and attach it to this group
            ResourceInfo resource = (ResourceInfo) i.next();
            resource.copyResource(group);
        }

        return group;
    }

    /**
     * Setter method for the ResourceGroup name as it would be in SunCluster
     *
     * @param  name  Name of the ResourceGroup as it is in SunCluster
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter method for the ResourceGroup type (Failover or Scalable)
     *
     * @return  Type of the ResourceGroup
     */
    public String getType() {
        return type;
    }

    /**
     * Setter method for the ResourceGroup type (Failover or Scalable)
     *
     * @param  type  Type of the ResourceGroup
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Getter method for the ResourceGroup dependencies
     *
     * @return  List of Groups that this Group depends upon.
     */
    public List getDependency() {
        return dependency;
    }

    /**
     * Setter method for the ResourceGroup dependencies
     *
     * @param  dependency  the List of Groups that this Group depends upon.
     */
    public void setDependency(List dependency) {
        this.dependency = dependency;
    }

    /**
     * Getter method for the ResourceGroup dependent
     *
     * @return  List of Groups that depend on this group
     */
    public List getDependent() {
        return dependent;
    }

    /**
     * Setter method for the ResourceGroup dependent
     *
     * @param  dependent  the List of Groups that depend on this group.
     */
    public void setDependent(List dependent) {
        this.dependent = dependent;
    }

    /**
     * Check whether ResourceGroup is optional for the DataService
     *
     * @return  <code>"true"</code> if optional <code>"false"</code> if not
     * optional.
     */
    public boolean isOptional() {
        return optional;
    }

    /**
     * Specifies whether the ResourceGroup is optional or not for the
     * Dataservice.
     *
     * @param  optional  <code>"true"</code> if optional <code>"false"</code> if
     * not optional.
     */
    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    /**
     * Gets the List of Resources configured in the ResourceGroup
     *
     * @return  List of Resources to be configured in the ResourceGroup
     */
    public List getResources() {
        return resources;
    }

    /**
     * Sets the List of Resources to be configured in the ResourceGroup
     *
     * @param  resources  List of Resources to be configured in the
     * ResourceGroup
     */
    public void setResources(List resources) {
        this.resources = resources;
    }

    /**
     * Gets the ResourceGroup Properties
     *
     * @return  The RGProperties for this ResourceGroup
     */
    public Map getRgProperties() {
        return rgProperties;
    }

    /**
     * Returns the name of the zone cluster or null for base cluster
     *
     * @return zoneClusterName name of the zone cluster or null for base cluster.
     */
    public String getZoneClusterName() {
	return this.zoneClusterName;
    }

    /**
     * Sets the name of the zone cluster to the name specified.
     *
     * @param zoneClusterName name of the zone cluster or null for base cluster
     */
    public void setZoneClusterName(String zoneClusterName) {
	this.zoneClusterName = zoneClusterName;
    }    


    /**
     * Sets the List of Resources to be configured in the ResourceGroup
     *
     * @param  rgProperties  RGProperties for this ResourceGroup
     */
    public void setRgProperties(Map rgProperties) {
        this.rgProperties = rgProperties;
    }

    /**
     * Generate Commands needed to Configure the Group and the Resources inside
     * it. The generated commands are stored in the commandSet List.
     *
     * @param  commandSet  The List where the generated Commands should be
     * stored.
     *
     * @throws  Exception
     */
    public void generateCommandSet(MBeanServer mbs, ObjectNameFactory rgmOnf,
        List commandSet) throws Exception {

        ExitStatus status[] = null;
        StringBuffer buffer = null;
        String cmds[] = null;

        // If ResourceGroup is already configured do nothing
        if (groupConfigured) {
            return;
        }

        // Check if name is set, if not and if RG is mandatory throw Exception
        if ((name == null) && (optional == false))
            throw new Exception("Group Should be Configured");
        else if (name == null) // The Group is not configured, do not generate
                               // Commands
            return;

        // Configure the Dependencies of this RG and generate their Commands
        if (dependency != null) {
            Iterator i = dependency.iterator();

            while (i.hasNext()) {
                ResourceGroupInfo group = (ResourceGroupInfo) i.next();
                List dependentList = group.getDependent();

                if (dependentList == null) {
                    dependentList = new ArrayList();
                }

                if (!dependentList.contains(this)) {
                    dependentList.add(this);
                }

                group.setDependent(dependentList);

                try {
                    group.generateCommandSet(mbs, rgmOnf, commandSet);
                } catch (CommandExecutionException e) {
                    throw e;
                } catch (Exception e) {
                    throw e;
                }

                group.setGroupConfigured(true);
            }
        }

        // Generate the commands

        try {

            // MBean call: Check if RG with Same Name exists,accordingly set
            // newRG Flag

	    RgGroupMBean rgGroupMBean = (RgGroupMBean)
		MBeanServerInvocationHandler.newProxyInstance(mbs,
		    rgmOnf.getObjectName(RgGroupMBean.class, null),
		    RgGroupMBean.class, false);

            if (rgGroupMBean.isResourceGroupRegistered(name, zoneClusterName)) {
                newRG = false;
            }

            // MBean Call: If New RG create a New RG with the configuration.
            if (newRG) {

                // Add the Dependencies to the rgProperties
                if (dependency != null) {
                    String dependArray[] = getDependencyArray();
                    String dependencies = new String(dependArray[0]);

                    for (int i = 1; i < dependArray.length; i++) {
                        dependencies = dependencies.concat(",").concat(
                                dependArray[i]);
                    }

                    rgProperties.put("RG_Dependencies", dependencies);
                }

                // Create a New RG
                status = rgGroupMBean.addResourceGroup(
		    name, rgProperties, zoneClusterName, runCmd);
                commandSet.add(DataServicesUtil.convertArrayToString(
                        status[0].getCmdExecuted()));

            } else {

                if ((rgProperties != null) && (rgProperties.size() > 0)) {
                    status = rgGroupMBean.changeProperties(
			name, rgProperties, zoneClusterName, runCmd);
                    commandSet.add(DataServicesUtil.convertArrayToString(
                            status[0].getCmdExecuted()));
                }

                // Set the Dependencies of the RG with the  new Dependencies.
                if (this.dependency != null) {

		    if (rgGroupMBean.isResourceGroupRegistered(name, zoneClusterName)) {

                        // Get the ResourceGroup's Existing Dependencies
			ResourceGroupData rgData = rgGroupMBean.getResourceGroup(
			    name, zoneClusterName);

                        String existingDependencies[] = rgData.getDependencies();
                        String finalDependencies[] = DataServicesUtil
                            .concatenateArrays(existingDependencies,
                                getDependencyArray());

                        // Remove duplicates from the array
                        List finalList = Arrays.asList(finalDependencies);
                        SortedSet dependencySet = new TreeSet();

                        for (Iterator i = finalList.iterator(); i.hasNext();) {
                            dependencySet.add((String) i.next());
                        }

                        finalDependencies = new String[dependencySet.size()];
                        finalDependencies = (String[]) dependencySet.toArray(
                                finalDependencies);

                        // Change the Dependencies.
                        status = rgGroupMBean.changeDependencies(
			    name, finalDependencies, zoneClusterName, runCmd);
                        commandSet.add(DataServicesUtil.convertArrayToString(
                                status[0].getCmdExecuted()));
                    } else {
                        System.out.println(
                            "ResourceGroup Not Registered with CMASS : " +
                            name);
                    }
                }
            }
        } catch (CommandExecutionException e) {
            status = e.getExitStatusArray();
            System.out.println("Command Tried to Execute : \n" +
                DataServicesUtil.convertArrayToString(
                    status[0].getCmdExecuted()));
            System.out.println(status[0].getErrStrings());
            throw e;
        }

        // Iterate through the ResourceList of this group and Call
        // ConfigureResource
        Iterator j = resources.iterator();

        while (j.hasNext()) {
            ResourceInfo resource = (ResourceInfo) j.next();

            try {
                resource.generateCommand(mbs, rgmOnf, commandSet);
            } catch (CommandExecutionException e) {

                // Set Group Configured as true so that rollback
                // would happen for the group and the resources
                // that were created
                groupConfigured = true;
                throw e;
            } catch (Exception e) {
                throw e;
            }

            resource.setResourceConfigured(true);
        }
    }

    /**
     * toString method.
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n\t");

        buffer.append("group             :" + groupName + "\n\t");
        buffer.append("resourceGroupName :" + name + "\n\t");
        buffer.append("zoneClusterName   :" + zoneClusterName + "\n\t");
        buffer.append("dependency        :" + getDependencies() + "\n\t");
        buffer.append("dependent         :" + getDependents() + "\n\t");
        buffer.append("optional          :" + optional + "\n\t");
        buffer.append("type              :" + type + "\n\t");
        buffer.append("rgProperties      :" + rgProperties + "\n\n\t");
        buffer.append("Resources         :" + "\n\t\t");

        Iterator i = resources.iterator();

        while (i.hasNext()) {
            buffer.append(i.next() + "\n\t\t");
        }

        buffer.append("\n");

        return buffer.toString();
    }

    /**
     * Returns a space separated list of ResourceGroups this ResourceGroup
     * depends on.
     *
     * @return  space separated list of ResourceGroups this ResourceGroup
     * depends on (or) return
     *
     * <p>"No Dependency"</p>
     *
     * if there is no dependency
     */
    public String getDependencies() {

        StringBuffer buffer;

        if (dependency == null) {
            return "No Dependency";
        } else {
            buffer = new StringBuffer();

            Iterator i = dependency.iterator();

            while (i.hasNext()) {
                ResourceGroupInfo group = (ResourceGroupInfo) i.next();
                buffer.append(group.getGroupName() + " ");
            }

            return buffer.toString();
        }
    }

    /**
     * Returns an String array of Names of ResourceGroup this ResourceGroup
     * depends on
     *
     * @return  String array of names of ResourceGroup this ResourceGroup
     * depends on
     */
    public String[] getDependencyArray() {

        String buffer[];
        int j = 0;

        if (dependency == null) {
            return null;
        } else {
            buffer = new String[dependency.size()];

            Iterator i = dependency.iterator();

            while (i.hasNext()) {
                ResourceGroupInfo resourceGroup = (ResourceGroupInfo) i.next();
                buffer[j++] = resourceGroup.getName();
            }

            return buffer;
        }
    }


    /**
     * Returns a space separated list of ResourceGroups that depend on this RG.
     *
     * @return  space separated list of ResourceGroups that depend on this RG
     * (or) return
     *
     * <p>"No Dependent"</p>
     *
     * if there are no dependents
     */
    public String getDependents() {

        StringBuffer buffer;

        if (dependent == null) {
            return "No Dependent";
        } else {
            buffer = new StringBuffer();

            Iterator i = dependent.iterator();

            while (i.hasNext()) {
                ResourceGroupInfo group = (ResourceGroupInfo) i.next();
                buffer.append(group.getGroupName() + " ");
            }

            return buffer.toString();
        }
    }

    /**
     * Returns an String array of Names of ResourceGroups that depend on this
     * RG.
     *
     * @return  String array of names of ResourceGroup that depend on this RG.
     */
    public String[] getDependentArray() {

        String buffer[];
        int j = 0;

        if (dependent == null) {
            return null;
        } else {
            buffer = new String[dependent.size()];

            Iterator i = dependent.iterator();

            while (i.hasNext()) {
                ResourceGroupInfo resourceGroup = (ResourceGroupInfo) i.next();
                buffer[j++] = resourceGroup.getName();
            }

            return buffer;
        }
    }

    /**
     * Checks whether the Commands Have been generated for this group
     *
     * @return  whether Commands haveb been generated for this group
     */
    public boolean isGroupConfigured() {
        return groupConfigured;
    }

    /**
     * Sets whether the Commands Have been generated for this group
     *
     * @param  groupConfigured  true if Commands have been generated false if
     * commands have not been generated.
     */
    public void setGroupConfigured(boolean groupConfigured) {
        this.groupConfigured = groupConfigured;
    }

    /**
     * This method removes any session specific information that was used to
     * generate the commands
     */
    public void reset() {

        // Initialize all instance specific information
        this.name = null;
        this.rgProperties = null;
        this.groupConfigured = false;
        this.newRG = true;
        this.groupRollbacked = false;
	this.zoneClusterName = null;

        // Iterate through the resource List and reset the information
        Iterator i = resources.iterator();

        while (i.hasNext()) {
            ResourceInfo resource = (ResourceInfo) i.next();
            resource.reset();
        }
    }

    /**
     * Rollbacks the command generated for this group
     */
    public void rollBack(MBeanServer mbs, ObjectNameFactory rgmOnf,
        List commandSet) throws Exception {
        ExitStatus status[] = null;
        StringBuffer buffer = null;
        String cmds[] = null;

        if (groupRollbacked || !groupConfigured) {
            return;
        }

        if (name != null) {

            // Rollback the commands for the groups dependent on this group
            if (dependent != null) {
                Iterator i = dependent.iterator();

                while (i.hasNext()) {
                    ResourceGroupInfo group = (ResourceGroupInfo) i.next();

                    try {
                        group.rollBack(mbs, rgmOnf, commandSet);
                    } catch (Exception e) {
                        throw e;
                    }

                    group.setGroupRollbacked(true);
                }
            }

            try {
		RgGroupMBean rgGroupMBean = (RgGroupMBean)
		    MBeanServerInvocationHandler.newProxyInstance(mbs,
			rgmOnf.getObjectName(RgGroupMBean.class, null),
			RgGroupMBean.class, false);

		// Rollback Resources in the ResourceGroup
                // Rollback the commands for this group
                if (newRG) {

                    Iterator j = resources.iterator();

                    while (j.hasNext()) {
                        ResourceInfo resource = (ResourceInfo) j.next();

                        try {
                            resource.rollBack(mbs, rgmOnf, commandSet);
                        } catch (Exception e) {
                            throw e;
                        }

                        resource.setResourceRollbacked(true);
                    }

                    // Get Handler to the ResourceGroupData 
		    ResourceGroupData rgData = rgGroupMBean.getResourceGroup(
			name, zoneClusterName);

                    // Bring the ResourceGroup offline if required
                    if (rgData.isOnline()) {
                        status = rgGroupMBean.bringOffline(name, zoneClusterName);
                        commandSet.add(DataServicesUtil.convertArrayToString(
                                status[0].getCmdExecuted()));
                    }

                    // Unmanage the RG and delete it
                    status = rgGroupMBean.unmanage(name, zoneClusterName);
                    commandSet.add(DataServicesUtil.convertArrayToString(
                            status[0].getCmdExecuted()));
                    status = rgGroupMBean.remove(name, zoneClusterName);
                    commandSet.add(DataServicesUtil.convertArrayToString(
                            status[0].getCmdExecuted()));
                }
                // If this ResourceGroup was configured as  Existing
                // Resource Group
                else {
                    // Rollback the resources in the group
                    Iterator j = resources.iterator();

                    while (j.hasNext()) {
                        ResourceInfo resource = (ResourceInfo) j.next();

                        try {
                            resource.rollBack(mbs, rgmOnf, commandSet);
                        } catch (Exception e) {
                            throw e;
                        }
                    }

                    // Rollback any dependencies
                    if (dependency != null) {
			// Get Handler to the ResourceGroupData
			ResourceGroupData rgData = rgGroupMBean.getResourceGroup(
			    name, zoneClusterName);
                        String existingDependencies[] = rgData.getDependencies();
                        String addedDependencies[] = getDependencyArray();
                        List existing = Arrays.asList(existingDependencies);
                        List added = Arrays.asList(addedDependencies);
                        existing.removeAll(added);

                        String finalDependencies[] =
                            new String[existing.size()];
                        finalDependencies = (String[]) existing.toArray(
                                finalDependencies);
                        status = rgGroupMBean.changeDependencies(
			    name, finalDependencies, zoneClusterName, runCmd);
                        commandSet.add(DataServicesUtil.convertArrayToString(
                                status[0].getCmdExecuted()));
                    }
                }
            } catch (CommandExecutionException e) {
                status = e.getExitStatusArray();
                System.out.println("Command Tried to Execute : \n" +
                    DataServicesUtil.convertArrayToString(
                        status[0].getCmdExecuted()));
                System.out.println(status[0].getErrStrings());
                throw e;
            }
        }
    }

    /**
     * Returns true if the Group had been rollbacked
     */
    public boolean isGroupRollbacked() {
        return groupRollbacked;
    }

    /**
     * Sets the Rollback flag for the Flag
     */
    public void setGroupRollbacked(boolean groupRollbacked) {
        this.groupRollbacked = groupRollbacked;
    }
}
