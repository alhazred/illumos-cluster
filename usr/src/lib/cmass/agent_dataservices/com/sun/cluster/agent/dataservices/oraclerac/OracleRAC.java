/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)OracleRAC.java	1.58	09/03/11 SMI"
 */

package com.sun.cluster.agent.dataservices.oraclerac;

// CACAO
import com.sun.cacao.ObjectNameFactory;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.common.ServiceConfig;
import com.sun.cluster.agent.dataservices.utils.*;
import com.sun.cluster.agent.devicegroup.DeviceGroupMBean;
import com.sun.cluster.agent.devicegroup.NasDeviceMBean;
import com.sun.cluster.agent.rgm.RGroupMBean;
import com.sun.cluster.agent.rgm.RgGroupMBean;
import com.sun.cluster.agent.rgm.ResourceData;
import com.sun.cluster.agent.rgm.ResourceGroupData;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyEnum;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.agent.zonecluster.ZoneClusterFSTab;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.common.CMASUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// JMX
import javax.management.AttributeValueExp;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;


/**
 * OracleRAC MBean interface implementation.
 */
public class OracleRAC implements OracleRACMBean {

    /* RTName of Orac MBean */
    private static final String NAME = "NAME";

    private static Logger logger = Logger.getLogger(Util.DOMAIN);

    // These static strings are used in RG name generation

//    private static String DG_RG_PREFIX = "dg";
//    private static String FS_RG_PREFIX = "mnt";
//    private static String MDS_RG_PREFIX = "mds";
//    private static String QFS = "qfs";

    private static String METASTAT_CMD = "/usr/sbin/metastat";

    private static String MINUS_M = "-m";

    /* The MBeanServer in which this MBean is inserted. */
    private MBeanServer mBeanServer;

    /* The object name factory used to register this MBean */
    private ObjectNameFactory onf = null;

    /* Command Generation Structure for this Dataservice */
    private DataServiceInfo serviceInfo = null;

    /* Command Generation Structure for this Dataservice */
    private ServiceConfig oracleConfiguration = null;

    /* List of commands executed */
    private List commandList = null;

    /* Connection References */
    private MBeanServerConnection mbsConnections[];
    private JMXConnector connectors[] = null;
    private JMXConnector localConnector = null;
    private ArrayList lhResNameArrList = null;

    private List duplicateRGStructures = null;
    private String VXDG_COMMAND = "/usr/sbin/vxdg";
    private String VXDG_COMMAND_LIST = "list";
    private String VXDG_COMMAND_SHARED = "shared";

    private static String SVM_HDR = "/dev/md";
    private static String VX_HDR = "/dev/vx";

    private static int CZ_INDEX = 0;
    private static int R_INDEX = 1;

    /**
     * Default constructor.
     */
    public OracleRAC(MBeanServer mBeanServer, ObjectNameFactory onf,
        DataServiceInfo serviceInfo) {
        this.mBeanServer = mBeanServer;
        this.onf = onf;
        this.serviceInfo = serviceInfo;
    }

    /**
     * Returns the ServiceInfo Structure for this DataserviceMBean
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public DataServiceInfo getServiceInfo() {
        return this.serviceInfo;
    }

    /**
     * Returns list of executed command list
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public List getCommandList() {
        return this.commandList;
    }

    /**
     * Sets the ServiceInfo Structure for this DataserviceMBean
     *
     * @param  serviceInfo  Command Generation Structure
     */
    public void setServiceInfo(DataServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    /**
     * Generates the set of commands for configuring this service, given the
     * configuration information. This command uses the ServiceInfo structure,
     * fills the same and generates the command list. This method needs to be
     * synchronized because this method operates on Wizard instance specific
     * information.
     *
     * @param  configurationInformation  ConfigurationInformation for
     * configuring an instance of the Dataservice
     *
     * @return  Commands Generated for the given ConfigurationInformation
     */
    public synchronized List generateCommands(Map configurationInformation)
        throws CommandExecutionException {

        /*
         * The same MBean is used for generating commands for RAC Database
         * and for RAC Framework, Storage
         */

        commandList = new ArrayList();

        boolean storageCG = false;

        try {

            boolean frameworkCG = false;

            // Configure Storage Resources if any
            Map storageMap = (HashMap)configurationInformation.get(
                    Util.STORAGERESOURCE);

            if (storageMap != null) {
                storageCG = true;

                String frameworkRG = (String)configurationInformation.get(
                        Util.ORF_RESOURCE_GROUP + Util.NAME_TAG);

		String frameworkRGInBC = (String)configurationInformation.get(
		    Util.ORF_RESOURCE_GROUP + Util.NAME_TAG + Util.BC_TAG);

		String selZoneCluster = (String)configurationInformation.
			get(Util.SEL_ZONE_CLUSTER);

		String zonePath = (String)configurationInformation.
		    get(Util.ZONE_PATH);

		String waitZCBootRSName = (String)configurationInformation.
		    get(Util.WAIT_ZC_BOOT_RESOURCE);

		String waitZCBootRGName = (String)configurationInformation.
		    get(Util.WAIT_ZC_BOOT_GROUP);

		configurationInformation.remove(Util.ZONE_PATH);
		configurationInformation.remove(Util.WAIT_ZC_BOOT_GROUP);
		configurationInformation.remove(Util.WAIT_ZC_BOOT_RESOURCE);

                duplicateRGStructures = new ArrayList();

                fillServiceInfoForStorage(storageMap,
		    waitZCBootRSName, waitZCBootRGName,
		    frameworkRG, frameworkRGInBC,
		    selZoneCluster, zonePath);
            } else {
                frameworkCG = true;
            }

            configurationInformation.remove(Util.STORAGERESOURCE);
	    configurationInformation.remove(Util.ORF_RESOURCE_GROUP +
		Util.NAME_TAG + Util.BC_TAG);

            // Generate commands for RAC Database
            String configType = (String)configurationInformation.get(
                    Util.CONFIGURATION_TYPE);

            if ((configType != null) &&
		(configType.equals(Util.RAC_10G) ||
		configType.equals(Util.RAC_9I))) {
                commandList = generateCommandsForDatabase(
                        configurationInformation, configType);

                return commandList;
            }

            configurationInformation.remove(Util.CONFIGURATION_TYPE);
	    configurationInformation.remove(Util.SEL_ZONE_CLUSTER);

            // Generate commands for RAC Framework and Storage
            // Fill the ServiceInfo object with the configurationInformation
            serviceInfo.fillServiceInfoStructure(configurationInformation);

            ResourceInfo frameworkResource;
            String frameworkRSName = null;
            String frameworkRGName = null;

            if (frameworkCG) {
                frameworkResource = serviceInfo.getResource(Util.ORF_RID);
                frameworkRSName = frameworkResource.getResourceName();
            }

            if (storageCG) {
                ResourceGroupInfo frameworkRG = serviceInfo.getResourceGroup(
                        Util.ORF_RESOURCE_GROUP);
                frameworkRG.setGroupConfigured(true);

                List resources = frameworkRG.getResources();

                if (resources != null) {
                    Iterator j = resources.iterator();

                    while (j.hasNext()) {
                        ResourceInfo resource = (ResourceInfo)j.next();
                        resource.setResourceConfigured(true);
                    }
                }
            }

            // Generate CommandSet for this Service
            serviceInfo.generateCommandSet(mBeanServer, onf);
            commandList = serviceInfo.getCommandSet();

            // The framework RG must be brought online only after all the
            // resources in the RG have been created.
            if (frameworkCG) {
                HashMap confMap = new HashMap();
                serviceInfo.reset();
                frameworkRGName = (String)configurationInformation.get(
                        Util.ORF_RESOURCE_GROUP + Util.NAME_TAG);

		String zcNameForFramework = (String)configurationInformation.get(
			Util.ORF_RESOURCE_GROUP + Util.ZC_TAG);

                confMap.put(Util.ORF_RESOURCE_GROUP + Util.NAME_TAG,
                    frameworkRGName);
		confMap.put(Util.ORF_RESOURCE_GROUP + Util.ZC_TAG, zcNameForFramework);

                frameworkResource = serviceInfo.getResource(Util.ORF_RID);
                frameworkResource.setResourceName(frameworkRSName);
		frameworkResource.setZoneClusterName(zcNameForFramework);

                frameworkResource.setBringOnline(true);
                serviceInfo.fillServiceInfoStructure(confMap);
                serviceInfo.generateCommandSet(mBeanServer, onf);

                List onlineCmdList = serviceInfo.getCommandSet();

                if (commandList != null) {
                    commandList.addAll(onlineCmdList);
                } else {
                    commandList = onlineCmdList;
                }
            }

        } catch (Exception e) {

            // Unconfigure the Groups and Resources
            try {

                if (storageCG) {
                    ResourceGroupInfo frameworkRG = serviceInfo
                        .getResourceGroup(Util.ORF_RESOURCE_GROUP);
                    frameworkRG.setGroupConfigured(false);
                }

                if (!commandList.contains(Util.ROLLBACK_SUCCESS) &&
                        !commandList.contains(Util.ROLLBACK_FAIL)) {
                    // rollback success and failure hasn't already been added

                    commandList = serviceInfo.getCommandSet();

                    // add Rollback Success string to the command list - this
                    // would be used as a marker to demarcate configuration and
                    // rollback commands
                    commandList.add(Util.ROLLBACK_SUCCESS);
                    serviceInfo.rollBack(mBeanServer, onf);
                    commandList.addAll(serviceInfo.getCommandSet());
                }
            } catch (Exception re) {
		re.printStackTrace();
                // Rollback Failed
                // add Rollback Fail string in place of Rollback Success string
                // to the command list - this would be used as a marker to
                // demarcate configuration and rollback commands
                commandList.remove(Util.ROLLBACK_SUCCESS);
                commandList.add(Util.ROLLBACK_FAIL);
                commandList.addAll(serviceInfo.getCommandSet());
            }

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException)e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        } finally {

            // Remove the duplicate RGs
            if (duplicateRGStructures != null) {
                serviceInfo.getResourceGroupList().removeAll(
                    duplicateRGStructures);
            }

            serviceInfo.reset();
        }

        return commandList;
    }

    private List createCRSFrameworkResource(String crsFrameworkRSName,
        String racFrameworkRSName, String racFrameworkRGName,
        ArrayList selectedStorage, ArrayList gddLocalNodeRSList,
	String selZoneCluster)
        throws CommandExecutionException {
        List crsCommandList = new ArrayList();
        HashMap crsMap = new HashMap();
        List emptyProps = new ArrayList();

        List extProps = new ArrayList();
        List sysProps = new ArrayList();
        ResourceInfo crsFrameworkRS = null;
        ResourceInfo racFrameworkRS = null;

        serviceInfo.reset();

        // existing rac framework resource
        racFrameworkRS = serviceInfo.getResource(Util.ORF_RID);
        racFrameworkRS.setResourceName(racFrameworkRSName);
        racFrameworkRS.setExtProperties(emptyProps);
        racFrameworkRS.setSysProperties(emptyProps);
	racFrameworkRS.setZoneClusterName(selZoneCluster);
        racFrameworkRS.setBringOnline(true);

        // RG information
        crsMap.put(Util.ORF_RESOURCE_GROUP + Util.NAME_TAG, racFrameworkRGName);
        crsMap.put(Util.ORF_RESOURCE_GROUP + Util.ZC_TAG, selZoneCluster);

        // Resource information
        crsMap.put(Util.CRS_FRAMEWORK_RSID + Util.NAME_TAG, crsFrameworkRSName);
        crsMap.put(Util.CRS_FRAMEWORK_RSID + Util.ZC_TAG, selZoneCluster);

        // no extension props to be set
        crsMap.put(Util.CRS_FRAMEWORK_RSID + Util.EXTPROP_TAG, extProps);

        if ((selectedStorage != null) && (selectedStorage.size() > 0)) {

            // Set system property for resource_dependencies_offline_restart
            // this is only till the command generator does not support this
            ResourcePropertyStringArray rpArr = new ResourcePropertyStringArray(
                    Util.RS_DEPEND_OFFLINE_RESTART,
                    (String[])gddLocalNodeRSList.toArray(new String[0]));
            sysProps.add(rpArr);
        }

        crsMap.put(Util.CRS_FRAMEWORK_RSID + Util.SYSPROP_TAG, sysProps);

        crsFrameworkRS = serviceInfo.getResource(Util.CRS_FRAMEWORK_RSID);

        if (crsFrameworkRS != null) {
            crsFrameworkRS.setBringOnline(true);
        }

        try {

            // Fill the ServiceInfo object with the configurationInformation
            serviceInfo.fillServiceInfoStructure(crsMap);

            // Generate CommandSet for this Service
            serviceInfo.generateCommandSet(mBeanServer, onf);
        } catch (Exception e) {

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException)e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        } finally {
            crsCommandList.addAll(serviceInfo.getCommandSet());
        }

        return crsCommandList;
    }

    /**
     * Generate commands for the RAC Database wizard
     *
     * @param  configurationInformation  Has the configuration information
     * @param  configType  RAC Database type (10G or 9i)
     *
     * @return  CommandList
     */
    private List generateCommandsForDatabase(Map configurationInformation,
        String configType) throws CommandExecutionException {

        Map confMap = new HashMap();
        ResourceInfo scalableMountPointRS = null;
        ResourceInfo scalableDevGroupRS = null;

	String selZoneCluster = (String)configurationInformation.get(
	    Util.SEL_ZONE_CLUSTER);

        if (configType.equals(Util.RAC_9I)) {
            return generateCommandsFor9IDatabase(configurationInformation);
        }

        if (configType.equals(Util.RAC_10G)) {

            ResourceInfo racFrameworkRS = null;
            ResourceInfo proxyRS = null;

            // CRS Framework Resource Name
            String crsFrameworkRSName = (String)configurationInformation.get(
		Util.CRS_FRAMEWORK_RSNAME);
            String racFrameworkRGName = (String)configurationInformation.get(
		Util.RAC_FRAMEWORK_RGNAME);

            String racFrameworkRSName = (String)configurationInformation.get(
		Util.RAC_FRAMEWORK_RSNAME);

            boolean crsFrameworkPresent = ((Boolean)
		    configurationInformation.get(
		Util.CRS_FRAMEWORK_PRESENT)).booleanValue();

            // selected GDD RSList
            String selectedStorage[] = (String[])configurationInformation.get(
		Util.SEL_STORAGE);

            ArrayList selectedStorageForCRS = (ArrayList)
                configurationInformation.get(Util.SEL_SCAL_RS);

            ArrayList gddRSList = new ArrayList();
            ArrayList gddLocalNodeRSList = new ArrayList();

            if (selectedStorageForCRS != null) {
                StringBuffer stBuffer;
                int size = selectedStorageForCRS.size();

                for (int i = 0; i < size; i++) {
                    stBuffer = new StringBuffer((String)selectedStorageForCRS
                            .get(i));
                    stBuffer.append(Util.LOCAL_NODE);
                    gddLocalNodeRSList.add(stBuffer.toString());
                }
            }

            if (selectedStorage != null) {

                for (int i = 0; i < selectedStorage.length; i++) {
                    gddRSList.add(selectedStorage[i]);
                }
            }

            if (!crsFrameworkPresent) {

                // create CRS framework resources
                commandList = createCRSFrameworkResource(
		    crsFrameworkRSName, racFrameworkRSName, racFrameworkRGName,
		    selectedStorageForCRS, gddLocalNodeRSList,
		    selZoneCluster);
            }

            List emptyProps = new ArrayList();
            List extProps = new ArrayList();
            List sysProps = new ArrayList();

            serviceInfo.reset();

            // existing rac framework resource
            racFrameworkRS = serviceInfo.getResource(Util.ORF_RID);
            racFrameworkRS.setResourceName(racFrameworkRSName);
            racFrameworkRS.setExtProperties(emptyProps);
            racFrameworkRS.setSysProperties(emptyProps);
	    racFrameworkRS.setZoneClusterName(selZoneCluster);

            // RAC Proxy Resource Name
            String nodeList[] = (String[])configurationInformation.get(
		Util.RG_NODELIST);
            String racProxyRGName = (String)configurationInformation.get(
		Util.RAC_PROXY_RGNAME);
            String racProxyRSName = (String)configurationInformation.get(
		Util.RAC_PROXY_RSNAME);

            // RG Name
            confMap.put(Util.RAC_10G_RGID + Util.NAME_TAG, racProxyRGName);
            confMap.put(Util.RAC_10G_RGID + Util.ZC_TAG, selZoneCluster);

            // get a comma separated nodelist
            String nodeNames = Util.convertArraytoString(nodeList);

            // Set RG affinities
            HashMap rgProps = new HashMap();
            StringBuffer buffer = new StringBuffer();
            buffer.append(Util.POSITIVE);
            buffer.append(Util.POSITIVE);
            buffer.append(racFrameworkRGName);

            // get all GDD RGs and set RG affinities
            String storage_rgs[] = (String[])configurationInformation.get(
		Util.SEL_STORAGE_RG);

            if (storage_rgs != null) {

                for (int k = 0; k < storage_rgs.length; k++) {
                    buffer.append(Util.COMMA);
                    buffer.append(Util.POSITIVE);
                    buffer.append(Util.POSITIVE);
                    buffer.append(storage_rgs[k]);
                }
            }

            rgProps.put(Util.RG_AFFINITY, buffer.toString());
            rgProps.put(Util.NODELIST, nodeNames);
            rgProps.put(Util.MAX_PRIMARIES, String.valueOf(nodeList.length));
            rgProps.put(Util.DESIRED_PRIMARIES,
                String.valueOf(nodeList.length));
            rgProps.put(Util.RG_MODE, Util.SCALABLE_RG_MODE);
            confMap.put(Util.RAC_10G_RGID + Util.RGPROP_TAG, rgProps);
            confMap.put(Util.RAC_10G_RGID + Util.ZC_TAG, selZoneCluster);

            // Resource Name
            confMap.put(Util.RAC_10G_PROXY_RSID + Util.NAME_TAG,
                racProxyRSName);

            ResourcePropertyString rp = null;
            extProps = new ArrayList();

            // CRS HOME
            rp = new ResourcePropertyString(Util.CRS_HOME,
                    (String)configurationInformation.get(Util.SEL_CRS_HOME));
            extProps.add(rp);

            // DB NAME
            rp = new ResourcePropertyString(Util.DB_NAME,
                    (String)configurationInformation.get(Util.SEL_DB_NAME));
            extProps.add(rp);

            // ORACLE HOME
            rp = new ResourcePropertyString(Util.ORACLE_HOME,
                    (String)configurationInformation.get(
                        Util.SEL_ORACLE_HOME));
            extProps.add(rp);

            // ORACLE SID
            HashMap map = (HashMap)configurationInformation.get(
                    Util.SEL_ORACLE_SID);
            rp = new ResourcePropertyString();
            rp.setName(Util.ORACLE_SID);
            rp.setPernode(true);
            rp.setPernodeValue(map);
            extProps.add(rp);

            sysProps = new ArrayList();

            if (gddRSList != null) {
                gddRSList.add(crsFrameworkRSName);

                // Set system property for resource_dependencies_offline_restart
                // this is only till the command generator does not support this
                ResourcePropertyStringArray rpArr =
                    new ResourcePropertyStringArray(
                        Util.RS_DEPEND_OFFLINE_RESTART,
                        (String[])gddRSList.toArray(new String[0]));
                sysProps.add(rpArr);
            }

            confMap.put(Util.RAC_10G_PROXY_RSID + Util.SYSPROP_TAG, sysProps);
            confMap.put(Util.RAC_10G_PROXY_RSID + Util.EXTPROP_TAG, extProps);
            confMap.put(Util.RAC_10G_PROXY_RSID + Util.ZC_TAG,
                selZoneCluster);

            proxyRS = serviceInfo.getResource(Util.RAC_10G_PROXY_RSID);

            if (proxyRS != null) {
                proxyRS.setBringOnline(true);
            }
        }

        ErrorValue errVal = null;
        ErrorValue errVal1 = null;

        try {

            // Fill the ServiceInfo object with the configurationInformation
            serviceInfo.fillServiceInfoStructure(confMap);
            // Generate CommandSet for this Service

            serviceInfo.generateCommandSet(mBeanServer, onf);

            if (configType.equals(Util.RAC_10G)) {

                // selected GDD RSList
                String selectedStorage[] = (String[])configurationInformation
                    .get(Util.SEL_STORAGE);

                if ((selectedStorage != null) && (selectedStorage.length > 0)) {
                    HashMap helperData = new HashMap();
                    helperData.put(Util.UNDO, Util.NO_OP);
                    helperData.put(Util.VALUES, selectedStorage);
		    // pass the node host map

		    helperData.put(Util.SEL_ZONE_CLUSTER, selZoneCluster);

                    String selectedCrsHome = (String)configurationInformation
                        .get(Util.SEL_CRS_HOME);
                    helperData.put(Util.CRS_HOME, selectedCrsHome);

                    String selectedDbName = (String)configurationInformation
                        .get(Util.SEL_DB_NAME);
                    helperData.put(Util.DB_NAME, selectedDbName);

                    // call dataservice to create CRS resources
                    ServiceConfig oracleDS = getConfigObject();
                    errVal = oracleDS.applicationConfiguration(helperData);

                    if (!errVal.getReturnValue().booleanValue()) {

                        // crs resource creation failed. Undo all CRS and
                        // scrgadm commands
                        helperData.put(Util.UNDO, Util.YES_OP);
                        errVal1 = oracleDS.applicationConfiguration(helperData);
                        // throw CommandExecutionException

                        String errStr = errVal.getErrorString();
                        List errStrings = new ArrayList();
                        errStrings.add(errStr);

                        ExitStatus exitStatusArray[] = new ExitStatus[1];
                        exitStatusArray[0] = new ExitStatus();
                        exitStatusArray[0].setErrStrings(errStrings);

                        CommandExecutionException cee =
                            new CommandExecutionException(null,
                                exitStatusArray);
                        throw cee;
                    }
                }
            }
        } catch (Exception e) {
            // Unconfigure the Groups and Resources
            try {
                commandList.addAll(serviceInfo.getCommandSet());

                if (errVal1 != null) {

                    if (!errVal1.getReturnValue().booleanValue()) {

                        // rollback of CRS proxy resources failed
                        if (!commandList.contains(Util.ROLLBACK_FAIL)) {
                            commandList.add(Util.ROLLBACK_FAIL);
                        }

                        commandList.add(errVal1.getErrorString());
                    }
                }

                // add Rollback Success string to the command list - this
                // would be used as a marker to demarcate configuration and
                // rollback commands
                if (!commandList.contains(Util.ROLLBACK_FAIL)) {
                    commandList.add(Util.ROLLBACK_SUCCESS);
                }

                serviceInfo.rollBack(mBeanServer, onf);
                commandList.addAll(serviceInfo.getCommandSet());
            } catch (Exception re) {

                // Rollback Failed
                // add Rollback Fail string in place of Rollback Success string
                // to the command list - this would be used as a marker to
                // demarcate configuration and rollback commands
                commandList.remove(Util.ROLLBACK_SUCCESS);

                if (!commandList.contains(Util.ROLLBACK_FAIL)) {
                    commandList.add(Util.ROLLBACK_FAIL);
                }

                commandList.addAll(serviceInfo.getCommandSet());
            }

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException)e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        } finally {

            // Always reset the structure to the original form
            serviceInfo.reset();
        }

        // call Data services code to create the CRS resources
        commandList.addAll(serviceInfo.getCommandSet());

        if (configType.equals(Util.RAC_10G)) {

            // add the error string returned from errVal to the list.
            if ((commandList != null) && (errVal != null)) {
                commandList.add(errVal.getErrorString());
            }
        }

        return commandList;
    }


    /**
     * Generate commands for the RAC Database 9i wizard
     *
     * @param  configurationInformation  Has the configuration information
     *
     * @return  CommandList
     */
    private List generateCommandsFor9IDatabase(Map configurationInformation)
        throws CommandExecutionException {

        HashMap confMap = fillCmdGeneratorMap(configurationInformation, false);

        commandList = new ArrayList();

        ErrorValue errVal = null;

        try {
            serviceInfo.fillServiceInfoStructure(confMap);
            serviceInfo.generateCommandSet(mBeanServer, onf);

        } catch (Exception e) {
            commandList.addAll(serviceInfo.getCommandSet());

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException)e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        }

        commandList = serviceInfo.getCommandSet();

        String listenerRsName = (String)confMap.get(
                Util.RAC9I_SCAL_LISTENER_ID + Util.NAME_TAG);
        String racRgName = (String)confMap.get(Util.RAC9I_DBGROUP_ID +
                Util.NAME_TAG);

        if (listenerRsName != null) {
            HashMap secondMap = new HashMap();
            secondMap.put(Util.RAC9I_DBGROUP_ID + Util.NAME_TAG, racRgName);
            secondMap.put(Util.RAC9I_SCAL_LISTENER_ID + Util.NAME_TAG,
                listenerRsName);

            // Resource properties
            List systemProps = new ArrayList();

            if (lhResNameArrList != null) {
                ResourcePropertyStringArray rpArr =
                    new ResourcePropertyStringArray(Util.RS_WEAK_DEPENDENCIES,
                        (String[])lhResNameArrList.toArray(new String[0]));
                systemProps.add(rpArr);
            }

            secondMap.put(Util.RAC9I_SCAL_LISTENER_ID + Util.SYSPROP_TAG,
                systemProps);

            try {
                serviceInfo.reset();
                serviceInfo.fillServiceInfoStructure(secondMap);
                serviceInfo.generateCommandSet(mBeanServer, onf);
            } catch (Exception e) {
                commandList.addAll(serviceInfo.getCommandSet());

                if (e instanceof CommandExecutionException) {
                    throw (CommandExecutionException)e;
                } else if (e instanceof IOException) {
                    throw new CommandExecutionException(Util.IO_ERROR);
                } else {
                    throw new CommandExecutionException(e.getMessage());
                }
            }

            List secondList = serviceInfo.getCommandSet();
            commandList.addAll(secondList);
        }

        return commandList;
    }


    private HashMap fillCmdGeneratorMap(Map configurationInformation,
        boolean xIsConfigured) {

        HashMap confMap = new HashMap();
        String listenerRsName = null;
        ResourceInfo caLhRes = null;
        ResourceInfo racFrameworkRS = null;
        boolean bConfigServer = false;
        boolean bConfigListener = false;

        Integer preference = (Integer)configurationInformation.get(
	    Util.DB_COMPONENTS);
        int prefInt = preference.intValue();

        if ((prefInt == Util.SERVER) || (prefInt == Util.BOTH_S_L)) {
            bConfigServer = true;
        }

        if ((prefInt == Util.LISTENER) || (prefInt == Util.BOTH_S_L)) {
            bConfigListener = true;
        }

        String racRgName = (String)configurationInformation.get(
	    Util.RAC_RG_NAME);
        String nodeList[] = (String[])configurationInformation.get(
	    Util.RG_NODELIST);
        String nodeNames = Util.convertArraytoString(nodeList);

	String selZoneCluster = (String)configurationInformation.get(
	    Util.SEL_ZONE_CLUSTER);

        // ResourceGroup Name
        confMap.put(Util.RAC9I_DBGROUP_ID + Util.NAME_TAG, racRgName);

        // ResourceGroup Properties
        Map rgPropMap = new HashMap();
        rgPropMap.put(Util.NODELIST, nodeNames);

        String racFrameworkRGName = (String)configurationInformation.get(
	    Util.RAC_FRAMEWORK_RGNAME);
        String racFrameworkRSName = (String)configurationInformation.get(
	    Util.RAC_FRAMEWORK_RSNAME);

        // RG Name
        confMap.put(Util.ORF_RESOURCE_GROUP + Util.NAME_TAG,
            racFrameworkRGName);
	confMap.put(Util.ORF_RESOURCE_GROUP + Util.ZC_TAG,
	    selZoneCluster);

        // existing rac framework resource
        ArrayList emptyProps = new ArrayList();
        racFrameworkRS = serviceInfo.getResource(Util.ORF_RID);
        racFrameworkRS.setResourceName(racFrameworkRSName);
        racFrameworkRS.setExtProperties(emptyProps);
        racFrameworkRS.setSysProperties(emptyProps);
	racFrameworkRS.setZoneClusterName(selZoneCluster);

        // Set RG affinities
        StringBuffer buffer = new StringBuffer();
        buffer.append(Util.POSITIVE);
        buffer.append(Util.POSITIVE);
        buffer.append(racFrameworkRGName);
        rgPropMap.put(Util.RG_AFFINITY, buffer.toString());
        rgPropMap.put(Util.MAX_PRIMARIES, String.valueOf(nodeList.length));
        rgPropMap.put(Util.DESIRED_PRIMARIES, String.valueOf(nodeList.length));
        confMap.put(Util.RAC9I_DBGROUP_ID + Util.RGPROP_TAG, rgPropMap);
	confMap.put(Util.RAC9I_DBGROUP_ID + Util.ZC_TAG, selZoneCluster);

        String oraHome = (String)configurationInformation.get(
                Util.SEL_ORACLE_HOME);

        if (bConfigListener) {
            listenerRsName = (String)configurationInformation.get(
                    Util.RAC_LISTENER_RS_NAME);

            confMap.put(Util.RAC9I_SCAL_LISTENER_ID + Util.NAME_TAG,
                listenerRsName);

            // Resource properties
            List OracleExtProp = new ArrayList();
            List OracleSysProp = new ArrayList();

            ResourceProperty rp = new ResourcePropertyString(Util.ORACLE_HOME,
		oraHome);
            OracleExtProp.add(rp);

            // get all other extension properties
            String propertyNames[] = (String[])configurationInformation.get(
		Util.SCALABLE_LISTENER_PROPS);

            // All extension properties
            for (int i = 0; i < propertyNames.length; i++) {

                // Filter already set properties
                if (!propertyNames[i].equals(Util.RG_NAME) &&
		    !propertyNames[i].equals(Util.RS_NAME) &&
		    !propertyNames[i].equals(Util.NODELIST) &&
		    !propertyNames[i].equals(Util.LISTENER_ORACLE_HOME) &&
		    propertyNames[i].startsWith(Util.LISTENER_PREFIX)) {
                    ResourceProperty prop = getNodeSpecificPropValues(
                            configurationInformation, nodeList,
                            propertyNames[i]);

                    if (prop != null) {
                        OracleExtProp.add(prop);
                    }
		}
            }

            confMap.put(Util.RAC9I_SCAL_LISTENER_ID + Util.EXTPROP_TAG,
                OracleExtProp);

            ArrayList extProps = null;
            ArrayList stdList = null;
            caLhRes = serviceInfo.getResource(Util.RAC9I_LH_ID);

            ResourceGroupInfo zLhRGInfo = serviceInfo.getResourceGroup(
		Util.RAC9I_LHGROUP_ID);
            lhResNameArrList = null;

            for (int i = 0; i < nodeList.length; i++) {

                if (lhResNameArrList == null) {
                    lhResNameArrList = new ArrayList();
                }

                String curNode = nodeList[i];
                String tmpArry[] = curNode.split(Util.COLON);
                curNode = tmpArry[0];

                String lhRgName = (String)configurationInformation.get(
		    Util.SEL_LH_RG + curNode);
                ResourceGroupInfo curRGInfo = null;

                if (i != 0) {
                    curRGInfo = zLhRGInfo.copyGroup();
                } else {
                    curRGInfo = zLhRGInfo;
                }

                curRGInfo.setName(lhRgName);

                List tmpList = Arrays.asList(nodeList);
                ArrayList nodeAsList = new ArrayList(tmpList);

                for (int j = 0; j < i; j++) {
                    Object tmpObj = nodeAsList.remove(0);
                    nodeAsList.add(tmpObj);
                }

                String nodeAsArr[] = (String[])nodeAsList.toArray(
                        new String[0]);
                String nodeAsStr = Util.convertArraytoString(nodeAsArr);
                Map lhrgPropMap = new HashMap();
                lhrgPropMap.put(Util.NODELIST, nodeAsStr);
		lhrgPropMap.put(Util.FAILBACK_PROP, new Boolean(true));
                curRGInfo.setRgProperties(lhrgPropMap);
                curRGInfo.setGroupConfigured(xIsConfigured);
		curRGInfo.setZoneClusterName(selZoneCluster);

                ResourceInfo lhResourceInfo = serviceInfo.getResource(curRGInfo,
                        Util.RAC9I_LH_ID);

                // Existing LH Resources
                stdList = null;

                Vector selLH = (Vector)configurationInformation.get(
                        Util.SEL_LH_RS + curNode);

                if (selLH != null) {
                    String selLHArr[] = (String[])selLH.toArray(new String[0]);

                    for (int x = 0; x < selLHArr.length; x++) {

                        if ((selLHArr[x] != null) &&
                                (selLHArr[x].trim().length() > 0)) {
                            lhResourceInfo.setResourceName(selLHArr[x]);
                            lhResNameArrList.add(selLHArr[x]);
                            extProps = new ArrayList();
                            stdList = new ArrayList();
                            lhResourceInfo.setExtProperties(extProps);
                            lhResourceInfo.setSysProperties(stdList);
                            lhResourceInfo.setResourceConfigured(xIsConfigured);
			    lhResourceInfo.setZoneClusterName(selZoneCluster);
                            lhResourceInfo = lhResourceInfo.copyResource();
                        }
                    }
                }

                String hostNameList[] = (String[])configurationInformation.get(
		    Util.HOSTNAMELIST + curNode);
                String rsName;
                String netifList[];

                if (hostNameList != null) {

                    // There is atleast one new LH resource to be created
                    rsName = (String)configurationInformation.get(
			Util.SEL_LH_RSNAMES + curNode);
                    lhResourceInfo.setResourceName(rsName);
                    lhResNameArrList.add(rsName);

                    // Resource Properties
                    extProps = new ArrayList();
                    stdList = new ArrayList();

                    if ((hostNameList != null) && (hostNameList.length > 0)) {
                        rp = new ResourcePropertyStringArray(Util.HOSTNAMELIST,
                                hostNameList);
                        extProps.add(rp);
                    }

                    netifList = (String[])configurationInformation.get(
			Util.NETIFLIST + curNode);

                    if ((netifList != null) && (netifList.length > 0)) {
                        rp = new ResourcePropertyStringArray(Util.NETIFLIST,
                                netifList);
                        extProps.add(rp);
                    }

                    // Resource Extension Properties
                    lhResourceInfo.setExtProperties(extProps);
                    lhResourceInfo.setSysProperties(stdList);
                    lhResourceInfo.setResourceConfigured(xIsConfigured);
		    lhResourceInfo.setZoneClusterName(selZoneCluster);
                } else {
                    lhResourceInfo.removeResource();
                }

                if (i != 0) {
                    serviceInfo.getResourceGroupList().add(curRGInfo);
                }
            }

            confMap.put(Util.RAC9I_SCAL_LISTENER_ID + Util.SYSPROP_TAG,
                OracleSysProp);

	    confMap.put(Util.RAC9I_SCAL_LISTENER_ID + Util.ZC_TAG,
			selZoneCluster);

            ResourceInfo listenerRes = serviceInfo.getResource(
                    Util.RAC9I_SCAL_LISTENER_ID);
            listenerRes.setResourceConfigured(xIsConfigured);
        }

        // Resource Structure
        if (bConfigServer) {

            // Oracle Server Resource
            String serverRsName = (String)configurationInformation.get(
                    Util.RAC_SERVER_RS_NAME);
            confMap.put(Util.RAC9I_SCAL_SERVER_ID + Util.NAME_TAG,
                serverRsName);

            // Resource Properties
            List OracleExtProp = new ArrayList();
            List OracleSysProp = new ArrayList();

            ResourceProperty rp = new ResourcePropertyString(Util.ORACLE_HOME,
                    oraHome);
            OracleExtProp.add(rp);

            HashMap sidMap = getNodeSpecificStringValues(
                    configurationInformation, nodeList, Util.SEL_ORACLE_SID);
            ResourcePropertyString rpStr = new ResourcePropertyString();
            rpStr.setName(Util.ORACLE_SID);
            rpStr.setPernode(true);
            rpStr.setPernodeValue(sidMap);
            OracleExtProp.add(rpStr);

            // get all other extension properties
            String propertyNames[] = (String[])configurationInformation.get(
                    Util.SCALABLE_SERVER_PROPS);

            // All extension properties
            for (int i = 0; i < propertyNames.length; i++) {

                // Filter already set properties
                if (!propertyNames[i].equals(Util.RG_NAME) &&
		    !propertyNames[i].equals(Util.RS_NAME) &&
		    !propertyNames[i].equals(Util.NODELIST) &&
		    !propertyNames[i].equals(Util.SERVER_ORACLE_HOME) &&
		    !propertyNames[i].equals(
			Util.SERVER_PREFIX + Util.ORACLE_SID) &&
		    propertyNames[i].startsWith(Util.SERVER_PREFIX)) {
                    ResourceProperty prop = getNodeSpecificPropValues(
                            configurationInformation, nodeList,
                            propertyNames[i]);

                    if (prop != null) {
                        OracleExtProp.add(prop);
                    }
                }
            }

            confMap.put(Util.RAC9I_SCAL_SERVER_ID + Util.EXTPROP_TAG,
                OracleExtProp);

            // selected GDD RSList
            String selectedStorage[] = (String[])configurationInformation.get(
                    Util.SEL_STORAGE);
            ArrayList gddRSList = null;

            if (selectedStorage != null) {
                ArrayList extProps = new ArrayList();
                ArrayList sysProps = new ArrayList();

                for (int i = 0; i < selectedStorage.length; i++) {

                    if (gddRSList == null) {
                        gddRSList = new ArrayList();
                    }

                    StringBuffer stBuffer = new StringBuffer(
                            selectedStorage[i]);
                    stBuffer.append(Util.LOCAL_NODE);
                    gddRSList.add(stBuffer.toString());
                }

                if (gddRSList != null) {

                    // Set system property for
                    // resource_dependencies_offline_restart
                    // this is only till the command generator does not
                    // support this
                    ResourcePropertyStringArray rpArr =
                        new ResourcePropertyStringArray(
                            Util.RS_DEPEND_OFFLINE_RESTART,
                            (String[])gddRSList.toArray(new String[0]));
                    OracleSysProp.add(rpArr);
                }
            }

            if (listenerRsName != null) {
                ResourcePropertyStringArray rpArr =
                    new ResourcePropertyStringArray(Util.RS_WEAK_DEPENDENCIES,
                        new String[] { listenerRsName });
                OracleSysProp.add(rpArr);
            }

            confMap.put(Util.RAC9I_SCAL_SERVER_ID + Util.SYSPROP_TAG,
                OracleSysProp);

	    confMap.put(
		    Util.RAC9I_SCAL_SERVER_ID + Util.ZC_TAG, selZoneCluster);

            ResourceInfo serverRes = serviceInfo.getResource(
                    Util.RAC9I_SCAL_SERVER_ID);
            serverRes.setResourceConfigured(xIsConfigured);
        }

        ResourceGroupInfo dbRGInfo = serviceInfo.getResourceGroup(
                Util.RAC9I_DBGROUP_ID);
        dbRGInfo.setGroupConfigured(xIsConfigured);

        return confMap;
    }

    /**
     * Bring Resources Online for the RAC Database 9i wizard
     *
     * @param  configurationInformation  Has the configuration information
     *
     * @return  CommandList
     */
    public List bringOnline9IDatabase(Map configurationInformation)
        throws CommandExecutionException {

        Map confMap = new HashMap();
        String listenerRsName = null;
        ResourceInfo caLhRes = null;
        ResourceInfo racFrameworkRS = null;
        boolean bConfigServer = false;
        boolean bConfigListener = false;

        configurationInformation.remove(Util.CONFIGURATION_TYPE);
        configurationInformation.remove(Util.STORAGERESOURCE);
        serviceInfo.reset();

        Integer preference = (Integer)configurationInformation.get(
	    Util.DB_COMPONENTS);
        int prefInt = preference.intValue();

        if ((prefInt == Util.SERVER) || (prefInt == Util.BOTH_S_L)) {
            bConfigServer = true;
        }

        if ((prefInt == Util.LISTENER) || (prefInt == Util.BOTH_S_L)) {
            bConfigListener = true;
        }

        String nodeList[] = (String[])configurationInformation.get(
	    Util.RG_NODELIST);

	String selZoneCluster = (String)configurationInformation.get(
	    Util.SEL_ZONE_CLUSTER);

        if (bConfigListener) {

            ResourceGroupInfo zLhRGInfo = serviceInfo.getResourceGroup(
		Util.RAC9I_LHGROUP_ID);

            for (int i = 0; i < nodeList.length; i++) {
                String curNode = nodeList[i];
                String tmpArry[] = curNode.split(Util.COLON);
                curNode = tmpArry[0];

                String lhRgName = (String)configurationInformation.get(
		    Util.SEL_LH_RG + curNode);
                ResourceGroupInfo curRGInfo = null;

                if (i != 0) {
                    curRGInfo = zLhRGInfo.copyGroup();
                } else {
                    curRGInfo = zLhRGInfo;
                }

                curRGInfo.setName(lhRgName);

                ResourceInfo lhResourceInfo = serviceInfo.getResource(curRGInfo,
		    Util.RAC9I_LH_ID);

                // Existing LH Resources
                Vector selLH = (Vector)configurationInformation.get(
		    Util.SEL_LH_RS + curNode);

                if (selLH != null) {
                    String selLHArr[] = (String[])selLH.toArray(new String[0]);

                    for (int x = 0; x < selLHArr.length; x++) {

                        if ((selLHArr[x] != null) &&
                                (selLHArr[x].trim().length() > 0)) {
                            lhResourceInfo.setResourceName(selLHArr[x]);
                            lhResourceInfo.setBringOnline(true);
			    lhResourceInfo.setZoneClusterName(selZoneCluster);
                            lhResourceInfo = lhResourceInfo.copyResource();
                        }
                    }
                }

                String rsName;
                String hostNameList[] = (String[])configurationInformation.get(
                        Util.HOSTNAMELIST + curNode);

                if (hostNameList != null) {

                    // There is atleast one new LH resource to be created
                    rsName = (String)configurationInformation.get(
                            Util.SEL_LH_RSNAMES + curNode);
                    lhResourceInfo.setResourceName(rsName);
		    lhResourceInfo.setZoneClusterName(selZoneCluster);
                    lhResourceInfo.setBringOnline(true);
                } else {
                    lhResourceInfo.removeResource();
                }

                if (i != 0) {
                    serviceInfo.getResourceGroupList().add(curRGInfo);
                }
            }
        }

        // Bring the LH Hostnames Online First
        commandList = new ArrayList();

        ErrorValue errVal = null;

        try {
            serviceInfo.fillServiceInfoStructure(confMap);
            serviceInfo.generateCommandSet(mBeanServer, onf);
        } catch (Exception e) {
            commandList.addAll(serviceInfo.getCommandSet());

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException)e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        } finally {
            serviceInfo.reset();
        }

        commandList.addAll(serviceInfo.getCommandSet());

        String racRgName = (String)configurationInformation.get(
                Util.RAC_RG_NAME);

        confMap.put(Util.RAC9I_DBGROUP_ID + Util.NAME_TAG, racRgName);

        String racFrameworkRGName = (String)configurationInformation.get(
                Util.RAC_FRAMEWORK_RGNAME);
        String racFrameworkRSName = (String)configurationInformation.get(
                Util.RAC_FRAMEWORK_RSNAME);
        confMap.put(Util.ORF_RESOURCE_GROUP + Util.NAME_TAG,
            racFrameworkRGName);

        racFrameworkRS = serviceInfo.getResource(Util.ORF_RID);
        racFrameworkRS.setResourceName(racFrameworkRSName);
	racFrameworkRS.setZoneClusterName(selZoneCluster);
        racFrameworkRS.setBringOnline(true);

        if (bConfigListener) {
            listenerRsName = (String)configurationInformation.get(
                    Util.RAC_LISTENER_RS_NAME);

            ResourceInfo listenerRes = serviceInfo.getResource(
                    Util.RAC9I_SCAL_LISTENER_ID);
            listenerRes.setResourceName(listenerRsName);
	    listenerRes.setZoneClusterName(selZoneCluster);
            listenerRes.setBringOnline(true);
        }

        // Resource Structure
        if (bConfigServer) {
            String serverRsName = (String)configurationInformation.get(
                    Util.RAC_SERVER_RS_NAME);
            ResourceInfo serverRes = serviceInfo.getResource(
                    Util.RAC9I_SCAL_SERVER_ID);
            serverRes.setResourceName(serverRsName);
	    serverRes.setZoneClusterName(selZoneCluster);
            serverRes.setBringOnline(true);
        }

        // Bring the Oracle Resources Online
        try {
            serviceInfo.fillServiceInfoStructure(confMap);
            serviceInfo.generateCommandSet(mBeanServer, onf);

        } catch (Exception e) {
            commandList.addAll(serviceInfo.getCommandSet());

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException)e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        } finally {
            serviceInfo.reset();
        }

        List nextList = serviceInfo.getCommandSet();
        commandList.addAll(nextList);

        return commandList;
    }

    /**
     * Bring Resources Online for the RAC Database 9i wizard
     *
     * @param  configurationInformation  Has the configuration information
     *
     * @return  CommandList
     */
    public List rollBack9ICreation(Map configurationInformation)
        throws CommandExecutionException {

        configurationInformation.remove(Util.CONFIGURATION_TYPE);
        configurationInformation.remove(Util.STORAGERESOURCE);
        serviceInfo.reset();

        HashMap confMap = fillCmdGeneratorMap(configurationInformation, true);
        commandList = new ArrayList();

        ErrorValue errVal = null;

        try {
            serviceInfo.fillServiceInfoStructure(confMap);
            serviceInfo.rollBack(mBeanServer, onf);
        } catch (Exception e) {
            commandList.addAll(serviceInfo.getCommandSet());

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException)e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        } finally {
            serviceInfo.reset();
        }

        commandList = serviceInfo.getCommandSet();

        return commandList;
    }

    public void closeConnections(String nodeList[]) {

        if (nodeList != null) {

            for (int i = 0; i < nodeList.length; i++) {

                try {

                    if (connectors[i] != null) {
                        connectors[i].close();
                    }
                } catch (IOException ioe) {
                }
            }
        }

        if (localConnector != null) {

            try {
                localConnector.close();
            } catch (IOException ioe) {
            }
        }
    }

    private String getRGName(List list, String rsName) {
        String rgName = null;

        if (list != null) {
            int size = list.size();
            int i = 0;
            boolean found = false;

            while (!found && (i < size)) {
                String listElem = (String)list.get(i);

                // value is of type rsname:rgname
                String split_str[] = listElem.split(Util.COLON);

                if (split_str[0].equals(rsName)) {
                    found = true;
                    rgName = split_str[1];
                } else {
                    i++;
                }
            }
        }

        return rgName;
    }


    // Implementation For the ServiceConfig Interface Methods
    public String[] getDiscoverableProperties() {

        // Need to discover nodelist at the start of the wizard.
        String discoverableProps[] = {};

        return discoverableProps;
    }

    public String[] getAllProperties() {

        String properties[] = new String[] {};

        return properties;
    }

    public String[] getRTRProps(String rtName) {

        // Retrieve Properties From the RTR File.
        String properties[] = DataServicesUtil.getRTRProperties(mBeanServer,
                rtName);

        return properties;
    }

    public HashMap discoverPossibilities(String propertyNames[],
        Object helperData) {

        HashMap resultMap = new HashMap();
        List propList = Arrays.asList(propertyNames);
        String propertyName = null;
        String server_propertyName = null;
        String listener_propertyName = null;
        ResourceProperty property = null;
        String allProps[] = null;
        String discoverableProps[] = null;

        /*
         * Common discovery for Framework and Storage
         */

        // Discover PropertyNames : This is called while Initializing the
        // WizardContext Model
        if (propList.contains(Util.PROPERTY_NAMES)) {

            // Get the discoverableProperty Values
            discoverableProps = getDiscoverableProperties();

            for (int i = 0; i < discoverableProps.length; i++) {
                propertyNames = new String[] { discoverableProps[i] };
                resultMap.putAll(discoverPossibilities(propertyNames, null));
            }

            // Add other propertyNames to the resultMap
            allProps = getAllProperties();

            for (int i = 0; i < allProps.length; i++) {
                propertyName = allProps[i];

                // Omit the Discovered Properties
                if (!resultMap.containsKey(propertyName)) {

                    // Get the default values for these properties if any
                    Object propertyValue = DataServicesUtil.getDefaultRTRValue(
                            mBeanServer, NAME, propertyName);
                    resultMap.put(propertyName, propertyValue);
                }
            }
        }

        // Discover nodelist
        if (propList.contains(Util.NODELIST)) {
            resultMap.put(Util.NODELIST,
                DataServicesUtil.getNodeList(mBeanServer));
        }

        /*
         * Discovery for RAC Storage
         */

        /**
         * Discover the Metaserver Nodes for a QFS Filesystem
         * The resultMap would have a list of Nodes for the
         * Metaserver resource
         */
        if (propList.contains(Util.METASERVER_NODES)) {
            String qfsFilesystem = (String)helperData;
            resultMap.put(Util.METASERVER_NODES,
                getMetaserverNodeList(qfsFilesystem));
        }

        /**
         * Discover the Oban Diskgroups for the QFS Filesystem
         * returns the Oban diskgroup name for a QFS Filesystem
         * or "NULL"
         */
        if (propList.contains(Util.OBAN_UNDER_QFS)) {
            String qfsFilesystem = (String)helperData;
            resultMap.put(Util.OBAN_UNDER_QFS,
                getObanDG_forQFS(qfsFilesystem));
        }

        /*
         * Discover the shared-qfs filesystems on the node
         * helperData is null
         *
         * The resultMap would have a list of s-qfs filesystems on the node
         * If there are no shared qfs filesystems or the mcf file is
         * missing on the node, we get an empty list
         */
        if (propList.contains(Util.SQFS)) {

            // Call the RAC Serviceinfo class to get list of shared QFS
            // filesystems on this node
            List allQFSFilesystems = getSharedQFSFileSystems();
            resultMap.put(Util.ALL_SQFS, allQFSFilesystems);
        }

        /*
         * Discover VxVM DeviceGroups in the cluster that are under resources of
         * type SUNW.ScalDeviceGroup and those that arent.
         *
         * The helperData would specify the nodelist of the RAC Framework RG
         *
         * The returned HashMap structure would be as follows :
         * Key                        Value
         * RGM_VxVMDEVICEGROUP        Map of RGM managed VxVM DeviceGroups
         * NONRGM_VxVMDEVICEGROUP     Map of Non RGM Managed VxVM DeviceGroups
         */
        if (propList.contains(Util.VxVMDEVICEGROUP)) {
	    // It is safely assumed that the CVM DeviceGroup's connected nodes
	    // would be the same as that of the SUNW.cvm resource's resource
	    // group (RAC Framework Group)

	    // So the below discovery should be performed
	    // only on one of the RAC Framework RGs NodeList

	    List cvmDGs = new ArrayList();

	    ObjectNameFactory inonf = new ObjectNameFactory(Util.DOMAIN);
	    ObjectName infraMBeanObjName = inonf.getObjectName(
		InfrastructureMBean.class, null);
	    InfrastructureMBean infrastructureMBean = (InfrastructureMBean)
		MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
		    infraMBeanObjName, InfrastructureMBean.class, false);
	    List commands = new ArrayList();
	    List output = null;

	    // Assumption that CVM is configured properly
	    // on the RAC Nodes
	    // Execute vxdg list

	    commands.add(VXDG_COMMAND);
	    commands.add(VXDG_COMMAND_LIST);

	    String commandArr[] = new String[commands.size()];
	    commandArr = (String[])commands.toArray(commandArr);

	    try {
		ExitStatus status[] = infrastructureMBean.executeCommand(
		    commandArr);

		if ((status.length > 0) &&
		    (status[0].getReturnCode().intValue() ==
			status[0].SUCCESS)) {
		    output = status[0].getOutStrings();

		    for (Iterator o = output.iterator(); o.hasNext();) {
			String outLine = (String)o.next();

			String dgInfo[] = outLine.trim().split("[ \t]+");

			// grep for shared
			if ((dgInfo.length == 3) &&
			    (dgInfo[1].indexOf(VXDG_COMMAND_SHARED) !=
				-1)) {
			    cvmDGs.add(dgInfo[0]);
			}
		    }
		}
	    } catch (Exception e) {
		// Nothing to do
		e.printStackTrace();
	    }

	    resultMap.put(Util.VxVMDEVICEGROUP, cvmDGs);
        }


        /*
         * Discovery for RAC Database
         */

        if (propList.contains(Util.SCALABLE_SERVER_PROPS)) {

            // Add other propertyNames to the resultMap
            // Get the server properties
            HashMap propMap = new HashMap();
            allProps = getRTRProps(Util.RAC_SERVER_RTNAME);

            if (allProps != null) {

                for (int i = 0; i < allProps.length; i++) {
                    propertyName = allProps[i];
                    server_propertyName = Util.SERVER_PREFIX + propertyName;

                    // Get the default values for these properties if any
                    property = DataServicesUtil.getRTRProperty(mBeanServer,
                            Util.RAC_SERVER_RTNAME, propertyName);
                    propMap.put(server_propertyName, property);
                }

                resultMap.put(Util.SCALABLE_SERVER_PROPS, propMap);
            } else {
                resultMap.put(Util.SCALABLE_SERVER_PROPS, null);
            }
        }

        if (propList.contains(Util.SCALABLE_LISTENER_PROPS)) {

            // Add other propertyNames to the resultMap
            // Get the listener properties
            HashMap propMap = new HashMap();
            allProps = getRTRProps(Util.RAC_LISTENER_RTNAME);

            if (allProps != null) {

                for (int i = 0; i < allProps.length; i++) {
                    propertyName = allProps[i];
                    listener_propertyName = Util.LISTENER_PREFIX + propertyName;

                    // Get the default values for these properties if any
                    property = DataServicesUtil.getRTRProperty(mBeanServer,
                            Util.RAC_LISTENER_RTNAME, propertyName);
                    propMap.put(listener_propertyName, property);
                }

                resultMap.put(Util.SCALABLE_LISTENER_PROPS, propMap);
            } else {
                resultMap.put(Util.SCALABLE_LISTENER_PROPS, null);
            }
        }

	ServiceConfig oracleDS = null;
	HashMap dataServicesMap = null;
	HashMap newDataServicesMap = null;
        try {
            oracleDS = getConfigObject();

            dataServicesMap = oracleDS.discoverPossibilities(propertyNames,
		helperData);

            if (propList.contains(Util.CRS_OCR_VOTING_FILES)) {

                // need to do some special handling for
                // Util.CRS_OCR_VOTING_FILES
                dataServicesMap = processCrsOcrVotingDiskOutput(helperData,
                        dataServicesMap);
            }

	    /**
	     * Discover Storage Management schemes
	     * Modify this code to work with zone clusters.
	     * For zone clusters, whats installed in the global zone
	     * and whats configured in a zone cluster, decides the
	     * storage management scheme available.
	     */
	    if (propList.contains(Util.ORF_STORAGE_MGMT_SCHEMES)) {
		// the management scheme is based on what packages are installed
		// and how storage is configured in a zone cluster
		if (helperData != null && helperData instanceof HashMap) {
		    dataServicesMap = fetchSchemesForZoneCluster(
			dataServicesMap, (HashMap)helperData);
		}
	    }

	    // Discover Zone Enable
	    if (propList.contains(Util.ZONE_ENABLE)) {
		dataServicesMap.put(Util.ZONE_ENABLE,
			dataServicesMap.get(Util.RAC_ZONE_ENABLE));
	    }
        } catch (Exception ex) {
	    ex.printStackTrace();
        }

	if (dataServicesMap != null) {
	    resultMap.putAll(dataServicesMap);
        }

        return resultMap;
    }

    private ArrayList getVfsSpecialEntriesForZC(
	String zcFSPath, String[] nodeList) {
	// Just select the first node since the entries are on all the nodes
	String[] firstNode = new String[] { nodeList[0] };
	FileAccessorWrapper fileWrapper = null;
	createMBeanServerConnections(firstNode);

	ArrayList vfsSpecialEntries = new ArrayList();

	try {
	    fileWrapper = new FileAccessorWrapper(Util.VFSTAB_FILE,
		mbsConnections);

	    for (int i = 0; i < firstNode.length; i++) {
		List contents = (ArrayList)fileWrapper.readFully(
		    mbsConnections[i]);

		for (Iterator iter = contents.iterator(); iter.hasNext();) {
		    VfsStruct vfsEntry = (VfsStruct)iter.next();

		    String vfsMountPoint = vfsEntry.getVfs_mountp();
		    File file = new File(vfsMountPoint);
		    String absPath = file.getAbsolutePath();

		    if (zcFSPath.startsWith(absPath)) {
			// matching path found
			// return vfs special
			vfsSpecialEntries.add(vfsEntry.getVfs_special());
		    }
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    closeConnections(firstNode);
	    return vfsSpecialEntries;
	}
    }

    private HashMap fetchSchemesForZoneCluster(HashMap dataServicesMap,
	HashMap helperData) {
	HashMap newMap = null;
	List disSchemes = null;

	if (dataServicesMap != null) {
	    newMap = new HashMap(dataServicesMap);
	    disSchemes = (ArrayList)
		dataServicesMap.get(Util.ORF_STORAGE_MGMT_SCHEMES);
	} else {
	    newMap = new HashMap();
	}

	List storageSchemes = new ArrayList();
	ArrayList fsTabList = (ArrayList)
	    ((HashMap)helperData).get(Util.ZONE_CLUSTER_FSTAB_LIST);
	ArrayList devTabList = (ArrayList)
	    ((HashMap)helperData).get(Util.ZONE_CLUSTER_DEVTAB_LIST);
	String[] nodeList = (String [])
	    ((HashMap)helperData).get(Util.BASE_CLUSTER_NODELIST);

	String device = null;
	if (devTabList != null) {
	    int size = devTabList.size();
	    for (int i = 0; i < size; i++) {
		device = (String)devTabList.get(i);
		if (device.startsWith(SVM_HDR) &&
		    disSchemes.contains(Util.SVM)) {
		    if (!storageSchemes.contains(Util.SVM)) {
			storageSchemes.add(Util.SVM);
		    }
		    break;
		} else if (device.startsWith(VX_HDR) &&
		    disSchemes.contains(Util.VXVM)) {
		    if (!storageSchemes.contains(Util.VXVM)) {
			storageSchemes.add(Util.VXVM);
		    }
		    break;
		}
	    }
	}

	if (disSchemes.contains(Util.QFS_SVM)) {
	    // the SVM/Oban devices underlying shared QFS
	    // will probably not be exported to the zone cluster
	    // the shared QFS file system must have been exported.

	    ZoneClusterFSTab fsTabEntry = null;

	    if (fsTabList != null) {
		int size = fsTabList.size();
		for (int i = 0; i < size; i++) {
		    if (!storageSchemes.contains(Util.QFS_SVM) ||
			!storageSchemes.contains(Util.QFS_HWRAID)) {
			fsTabEntry = (ZoneClusterFSTab) fsTabList.get(i);
//
//		For loop back mounts
//	    if (fsTabEntry != null) {
//		String specialFile = fsTabEntry.getFS();
//		// Check in the /etc/vfstab file of the global zone
//		// and get the device to mount
//		ArrayList vfsSpecialEntries = getVfsSpecialEntriesForZC(
//		    zcFilePath, nodeList);
//		if (vfsSpecialEntries != null) {
//		    int vfsSize = vfsSpecialEntries.size();
//		    for (int j = 0; j < vfsSize; j++) {
//			if (!storageSchemes.contains(Util.QFS_SVM) ||
//			    !storageSchemes.contains(Util.QFS_HWRAID)) {
//			    String obanDG = getObanDG_forQFS(
//				(String)vfsSpecialEntries.get(j));
//
//			    if (obanDG != null) {
//				if (!storageSchemes.contains(Util.QFS_SVM)) {
//				    storageSchemes.add(Util.QFS_SVM);
//				}
//			    } else {
//				if (!storageSchemes.contains(Util.QFS_HWRAID)) {
//				    storageSchemes.add(Util.QFS_HWRAID);
//				}
//			    }
//			}
//		    }
//		}
//	    }
			// For direct mounts
			if (fsTabEntry != null) {
			    String specialEntry = fsTabEntry.getFS();
			    // for direct mounts this is the name of the QFS
			    // file system as it appears in the mcf file
			    if (!storageSchemes.contains(Util.QFS_SVM) ||
				!storageSchemes.contains(Util.QFS_HWRAID)) {
				String obanDG = getObanDG_forQFS(specialEntry);
				if (obanDG != null) {
				    if (!storageSchemes.contains(
					    Util.QFS_SVM)) {
					storageSchemes.add(Util.QFS_SVM);
				    }
				}
				if (!storageSchemes.contains(Util.QFS_HWRAID)) {
				    storageSchemes.add(Util.QFS_HWRAID);
				}
			    }
			}
		    }
		}
	    }
	}


	if (disSchemes.contains(Util.NAS)) {
	    if (!storageSchemes.contains(Util.NAS)) {
		storageSchemes.add(Util.NAS);
	    }
	}

	if (disSchemes.contains(Util.HWRAID)) {
	    if (!storageSchemes.contains(Util.HWRAID)) {
		storageSchemes.add(Util.HWRAID);
	    }
	}

	newMap.put(Util.ORF_STORAGE_MGMT_SCHEMES, storageSchemes);
	return newMap;
    }

    private HashMap processCrsOcrVotingDiskOutput(Object helperData,
        HashMap oldMap) {
        HashMap newMap = new HashMap(oldMap);

        List newMountPointList = null;
        List newDiskGroupList = null;

        // get the complete list of mount point dirs and
        // disk groups from the helperData
        List scMountPointDirs = null;
        List scDiskGroupNames = null;

        if (helperData != null) {
            Object obj = ((HashMap)helperData).get(Util.SC_SCALMOUNTPOINTS);

            if ((obj != null) && (obj instanceof ArrayList)) {
                scMountPointDirs = (ArrayList)obj;
            }

            obj = ((HashMap)helperData).get(Util.SC_SCALDEVGROUPS);

            if ((obj != null) && (obj instanceof ArrayList)) {
                scDiskGroupNames = (ArrayList)obj;
            }

        }

        // get the discovered values
        List orcVotingDiskPaths = (ArrayList)oldMap.get(
                Util.CRS_OCR_VOTING_FILES);

        // need to figure out if the path is a filesystem path or a disk group
        // path
        // map absolute/canonical paths to mount point dirs
        if (orcVotingDiskPaths != null) {
            int size1 = orcVotingDiskPaths.size();

            for (int i1 = 0; i1 < size1; i1++) {
                String absPath = (String)orcVotingDiskPaths.get(i1);

                try {

                    if (isDisk(absPath)) {

                        // get the disk group name corresponding to the path
                        String dgName = getDCSServiceName(absPath);

                        if ((scDiskGroupNames != null) &&
			    scDiskGroupNames.contains(dgName)) {

                            // add this dgname to the discovered list
                            if (newDiskGroupList == null) {
                                newDiskGroupList = new ArrayList();
                            }

                            if (!newDiskGroupList.contains(dgName)) {
                                newDiskGroupList.add(dgName);
                            }
                        }
                    } else {
                        String canonicalPath = new File(absPath)
                            .getCanonicalPath();

                        // is a filesystem
                        if (scMountPointDirs != null) {
                            int size2 = scMountPointDirs.size();
                            int i2 = 0;
                            boolean found = false;

                            while ((i2 < size2) && !found) {
                                String scAbsPath = (String)scMountPointDirs
                                    .get(i2);

                                try {
                                    File scFile = new File(scAbsPath);
                                    String scCanonicalPath = scFile
                                        .getCanonicalPath();

                                    if (canonicalPath.startsWith(
                                                scCanonicalPath)) {
                                        found = true;

                                        if (newMountPointList == null) {
                                            newMountPointList = new ArrayList();
                                        }

                                        if (
                                            !newMountPointList.contains(
                                                    scCanonicalPath)) {
                                            newMountPointList.add(
                                                scCanonicalPath);
                                        }
                                    }
                                } catch (Exception exception) {
				    exception.printStackTrace();
                                }

                                i2++;
                            }
                        }
                    } // else
                } catch (Exception e) {
		    e.printStackTrace();
                }
            }
        }

        newMap.put(Util.DISCOVERED_MOUNTPOINTS, newMountPointList);
        newMap.put(Util.DISCOVERED_DEVGROUPS, newDiskGroupList);

        return newMap;
    }

    public HashMap discoverPossibilities(String nodeList[],
        String propertyNames[], Object helperData) {

        HashMap resultMap = new HashMap();
        List propList = Arrays.asList(propertyNames);
        Map env = new HashMap();
        JMXConnector connector = null;
        JMXConnector localConnector = null;

        try {

            // Get connector for the node on which this mbean is instantiated.
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                this.getClass().getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());

            localConnector = TrustedMBeanModel.getWellKnownConnector(Util
                    .getClusterEndpoint(), env);

            /*
             * Discovery for RAC Framework
             */

            // Discover storage management schemes
	    if (propList.contains(Util.ORF_STORAGE_MGMT_SCHEMES)) {
		HashMap tmpMap = null;
		OracleRACMBean mBean = null;
		List storageMgmtSchemes = null;
		String propertyName[] = null;

		for (int i = 0; i < nodeList.length; i++) {
		    // Get connector for the node
		    connector = TrustedMBeanModel.getWellKnownConnector(
			Util.getNodeEndpoint(localConnector,
			(nodeList[i].split(Util.COLON))[0]),
			env);
		    // Get OracleRACMBean for the node
		    mBean = (OracleRACMBean)TrustedMBeanModel.getMBeanProxy(
				Util.DOMAIN, connector, OracleRACMBean.class,
				null, false);
		    // Discover possibilities for storage management schemes
		    // on the node.
		    propertyName = new String[] {Util.ORF_STORAGE_MGMT_SCHEMES};
		    tmpMap = (HashMap)mBean.discoverPossibilities(
			propertyName, helperData);

		    // Need to get common elements from returned lists from
		    // all nodes.
		    if (i == 0) {
			storageMgmtSchemes = (List)tmpMap.get(
			Util.ORF_STORAGE_MGMT_SCHEMES);
		    } else {
			storageMgmtSchemes =
				DataServicesUtil.getCommonElements(
			    (List)tmpMap.get(Util.ORF_STORAGE_MGMT_SCHEMES),
			    storageMgmtSchemes);
		    }
		    // Close connection
		    connector.close();
		}
		resultMap.put(
			Util.ORF_STORAGE_MGMT_SCHEMES, storageMgmtSchemes);
	    }

	    // Discover Framework RT
	    if (propList.contains(Util.FRMWRK_RT)) {
		HashMap tmpMap = null;
		OracleRACMBean mBean = null;
		List frmwrkRT = null;
		String propertyName[] = null;
		for (int i = 0; i < nodeList.length; i++) {
		    // Get connector for the node
		    connector = TrustedMBeanModel.getWellKnownConnector(
			Util.getNodeEndpoint(localConnector,
			(nodeList[i].split(Util.COLON))[0]), env);
		    // Get OracleRACMBean for the node
		    mBean = (OracleRACMBean)TrustedMBeanModel.getMBeanProxy(
			Util.DOMAIN, connector, OracleRACMBean.class,
			null, false);
		    // Discover possibilities for Framework RT
		    propertyName = new String[] {Util.FRMWRK_RT};
		    tmpMap = (HashMap)mBean.discoverPossibilities(
			propertyName, helperData);
		    if (i == 0) {
			frmwrkRT = (List)tmpMap.get(Util.FRMWRK_RT);
		    } else {
			frmwrkRT = DataServicesUtil.getCommonElements(
			    (List)tmpMap.get(Util.FRMWRK_RT), frmwrkRT);
		    }
		    // Close connection
		    connector.close();
		}
		resultMap.put(Util.FRMWRK_RT, frmwrkRT);
	    }

	    /*
	     * Discovery for RAC Storage
	     */

	    /*
	     * Discover NAS filesystems for the RAC nodelist
	     * that are under resources of type SUNW.ScalMountPoint
	     * and those that arent.
	     *
	     * The returned HashMap structure would be as follows :
	     * Key                     Value
	     * RGM_NAS                 Map of RGM managed NAS filesystems
	     * NONRGM_NAS              Map of Non RGM managed NAS filesystems
	     */
	    if (propList.contains(Util.NAS)) {
		ArrayList nasMountPoints = null;

		// Get the NAS Exported Filesystems from the NAS MBean
		QueryExp query = Query.match(new AttributeValueExp("Type"),
			Query.value(Util.NAS));
		ObjectNameFactory dgOnf = new ObjectNameFactory(
			NasDeviceMBean.class);
		ObjectName nasNamePattern = dgOnf.getObjectNamePattern(
			NasDeviceMBean.class);
		mBeanServer.queryNames(nasNamePattern, null);

		Set nasMBeansObjNames = mBeanServer.queryNames(nasNamePattern,
			query);

		for (Iterator i = nasMBeansObjNames.iterator(); i.hasNext();) {
		    ObjectName nasMBeanObjName = (ObjectName)i.next();
		    NasDeviceMBean nasMBean = (NasDeviceMBean)
			MBeanServerInvocationHandler.newProxyInstance(
			    mBeanServer, nasMBeanObjName, NasDeviceMBean.class,
			    false);
		    String nasName = (String)nasMBean.getName();
		    String exportedFS[] = (String[])nasMBean
			.getExportedFilesystems();

		    if (exportedFS != null) {
			for (int j = 0; j < exportedFS.length; j++) {
			    String filesystem = nasName + Util.COLON +
				exportedFS[j];
			    String nasMountPoint = getMountPointForFS(
					filesystem,
					nodeList);

			    if (nasMountPoint != null) {
				if (nasMountPoints == null) {
				    nasMountPoints = new ArrayList();
				}
				nasMountPoints.add(
				    nasMountPoint +
				    Util.COMMA + filesystem);
			    }
			}
		    }
		}
		resultMap.put(Util.NAS, nasMountPoints);
	    }

	    /*
	     * Discover S-QFS filesystems.
	     */
	    if (propList.contains(Util.SQFS)) {
		List allQFSFilesystems = null;
		OracleRACMBean mBean = null;
		String propertyName[] = null;
		HashMap tmpMap = null;
		List zcQFSFilesystems = null;
		List pcQFSFilesystems = null;
		List selQFSFilesystems = null;

		/*
		 * Get all the S-QFS filesystems from the mcf file
		 * for the RACNodelist
		 *
		 * Connect to every RAC Node's RACMBean and discover
		 * the QFS filesystems. Get those filesystems' names
		 * which are available from all the RAC Nodes
		 */
		for (int i = 0; i < nodeList.length; i++) {
		    connector = TrustedMBeanModel.getWellKnownConnector(Util
			    .getNodeEndpoint(localConnector, nodeList[i]), env);
		    mBean = (OracleRACMBean)TrustedMBeanModel.getMBeanProxy(
			    Util.DOMAIN, connector, OracleRACMBean.class, null,
			    false);
		    propertyName = new String[] { Util.SQFS };
		    tmpMap = (HashMap)mBean.discoverPossibilities(propertyName,
			    null);

		    // Need to get common elements from returned lists from
		    // all nodes.
		    if (i == 0) {
			allQFSFilesystems = (List)tmpMap.get(Util.ALL_SQFS);
		    } else {
			allQFSFilesystems = DataServicesUtil.getCommonElements(
				(List)tmpMap.get(Util.ALL_SQFS),
				allQFSFilesystems);
		    }

		    // Close connection
		    connector.close();
		}

		ArrayList zoneClusterPaths = null;
		String zoneClusterPath = null;
		StringBuffer zoneRootPath;
		String absZoneRootPath = null;
		String selectedZoneCluster = null;
		HashMap fsTabMap = null;
		ArrayList fsTabList = null;
		ZoneClusterFSTab fsTabEntry = null;
		String[] zoneClusterList = null;

		if (helperData != null && helperData instanceof HashMap) {
		    HashMap map = (HashMap)helperData;
		    selectedZoneCluster = (String)
			    map.get(Util.SEL_ZONE_CLUSTER);
		    zoneClusterPaths = (ArrayList)map.get(Util.ZONE_PATH);
		    fsTabMap = (HashMap)map.get(Util.ZONE_CLUSTER_FSTAB_LIST);
		    zoneClusterList = (String[])map.get(Util.ZONE_CLUSTER_LIST);
		}

		for (Iterator i = allQFSFilesystems.iterator(); i.hasNext();) {
		    String qfsName = (String)i.next();

		    // Read the vfstab file for the MountpointDir for this QFS
		    // filesystem

		    // Even if any one node's vfstab has an entry for the qfs
		    // filesystem we take it.
		    // Later when commands are generated the entries are made
		    // consistent on all the nodes

		    String qfsMountPoint = getMountPointForFS(qfsName,
			    nodeList);

		    String relMountPoint = null;

		    // For zone clusters the mount point is the zone root path
		    // appended with the mount point in the zone cluster


		    if (qfsMountPoint != null) {
			File file1 = new File(qfsMountPoint);
			String absQfsMountPoint = file1.getAbsolutePath();

			if (selectedZoneCluster != null &&
			    selectedZoneCluster.trim().length() > 0) {

//		    for loop back mounts

//		    fsTabList = (ArrayList)fsTabMap.get(selectedZoneCluster);
//		    if (fsTabList != null) {
//			int fsTabListSize = fsTabList.size();
//			// If user selected zone cluster, get the fstab entry
//			// for the selected zone cluster and match the dir
//			// against the entry in the vfstab file
//			for (int k = 0; k < fsTabListSize; k++) {
//			    fsTabEntry = (ZoneClusterFSTab)fsTabList.get(k);
//
//			    String zcMountPoint = fsTabEntry.getMountPoint();
//			    File file2 = new File(zcMountPoint);
//			    String absZCMountPoint = file2.getAbsolutePath();
//
//			    if (absQfsMountPoint.startsWith(absZCMountPoint)) {
//				if (zcQFSFilesystems == null) {
//				    zcQFSFilesystems = new ArrayList();
//				}
//
//				zcQFSFilesystems.add(qfsName + Util.COMMA +
//				    absZCMountPoint);
//			    }
//			}
//		    }

			    /**
			     * For direct mounts
			     */
			    // If user selected zone cluster, get the ones that
			    // match the zone path
			    if (zoneClusterPaths != null &&
				    zoneClusterPaths.size() == 1) {
				zoneClusterPath =
					(String)zoneClusterPaths.get(0);
				zoneRootPath =
					new StringBuffer(zoneClusterPath);
				zoneRootPath.append(Util.ROOT_PATH);

				File file2 = new File(zoneRootPath.toString());
				absZoneRootPath = file2.getAbsolutePath();

				if (absZoneRootPath != null &&
				    absQfsMountPoint.startsWith(
						    absZoneRootPath)) {
				    // this is a mount point in a zone cluster
				    // strip off the zone root path to get the
				    // relative path in the zone cluster

				    int length = absZoneRootPath.length();
				    relMountPoint =
					    absQfsMountPoint.substring(length);

				    if (zcQFSFilesystems == null) {
					zcQFSFilesystems = new ArrayList();
				    }
				    zcQFSFilesystems.add(
					  qfsName + Util.COMMA + relMountPoint);
				}
			    }
			} else {

			    // filter out the ones that are used by ZCs

			    if (zoneClusterList != null) {
				boolean found = false;
				int j = 0;
				while (j < zoneClusterList.length && !found) {
				    if (fsTabMap != null &&
					    !fsTabMap.isEmpty()) {
					fsTabList = (ArrayList)fsTabMap.get(
					    zoneClusterList[j]);

					if (fsTabList != null) {
					    int fsTabListSize =
						    fsTabList.size();
					    int k = 0;
					    while (k < fsTabListSize &&
								    !found) {
						fsTabEntry = (ZoneClusterFSTab)
						    fsTabList.get(k);
						String zcMountPoint =
						    fsTabEntry.getMountPoint();

						File file2 =
							new File(zcMountPoint);
						String absZCMountPoint = file2.
						    getAbsolutePath();

						if (absQfsMountPoint.startsWith(
						    absZCMountPoint))  {
						    found = true;
						} else {
						    j++;
						    k++;
						}
					    }
					}
				    }
				}

				if (!found) {
				    // mount point not exported to a ZC
				    // can be used by the base cluster
				    if (pcQFSFilesystems == null) {
					pcQFSFilesystems = new ArrayList();
				    }
				    pcQFSFilesystems.add(qfsName + Util.COMMA +
					absQfsMountPoint);
				}
			    } else {
				// can be used by the base cluster
				if (pcQFSFilesystems == null) {
				    pcQFSFilesystems = new ArrayList();
				}
				pcQFSFilesystems.add(qfsName + Util.COMMA +
				    absQfsMountPoint);
			    }
				

//		    // Filter out the ones that are zone Cluster paths.
//		    boolean found = false;
//		    if (zoneClusterPaths != null) {
//			int size = zoneClusterPaths.size();
//			int j = 0;
//			while (j < size && !found) {
//			    zoneClusterPath = (String)zoneClusterPaths.get(j);
//			    zoneRootPath =  new StringBuffer(zoneClusterPath);
//			    zoneRootPath.append(Util.ROOT_PATH);
//
//			    File file2 = new File(zoneRootPath.toString());
//			    absZoneRootPath = file2.getAbsolutePath();
//
//			    if (absQfsMountPoint.startsWith(absZoneRootPath)) {
//				found = true;
//			    } else {
//				j++;
//			    }
//			}
//
//			if (!found) {
//			    if (pcQFSFilesystems == null) {
//				pcQFSFilesystems = new ArrayList();
//			    }
//			    pcQFSFilesystems.add(
//				* qfsName + Util.COMMA + absQfsMountPoint);
//			}
//		    } else {
//			// no zone clusters configured. Return everything you
//			// discovered
//			if (pcQFSFilesystems == null) {
//			    pcQFSFilesystems = new ArrayList();
//			}
//			pcQFSFilesystems.add(
//				qfsName + Util.COMMA + absQfsMountPoint);
//			}

			}
		    }
		}

		if (selectedZoneCluster != null &&
		    selectedZoneCluster.trim().length() > 0) {
		    selQFSFilesystems = zcQFSFilesystems;
		} else {
		    selQFSFilesystems = pcQFSFilesystems;
		}

		resultMap.put(Util.SQFS, selQFSFilesystems);
	    }

	    /*
	     * Discover SVM DeviceGroups in the cluster.
	     *
	     * The helperData would specify properties for a zone cluster
	     *
	     * The returned HashMap structure would be as follows :
	     * Key                       Value
	     * SVMDEVICEGROUP        Map of SVM DeviceGroups
	     */
	    if (propList.contains(Util.SVMDEVICEGROUP)) {
		Map scalDGMap = null;
		HashMap userInputs = null;
		String selZoneCluster = null;
		ArrayList devTabList = null;
		HashMap devTabMap = null;
		ArrayList fsTabList = null;
		HashMap fsTabMap = null;

		String[] zoneClusterList = null;
		ClusterDiskGroupInfo zcDiskGroup = null;
		ClusterDiskGroupInfo pcDiskGroup = null;

		ArrayList zcDiskGroupInfoList = null;
		ArrayList pcDiskGroupInfoList = null;

		List racNodeList = Arrays.asList(nodeList);

		if (helperData != null && helperData instanceof HashMap) {
		    userInputs = (HashMap)helperData;
		    selZoneCluster = (String)
			    userInputs.get(Util.SEL_ZONE_CLUSTER);
		    devTabMap = (HashMap)userInputs.get(
			Util.ZONE_CLUSTER_DEVTAB_LIST);
		    fsTabMap = (HashMap)userInputs.get(
			Util.ZONE_CLUSTER_FSTAB_LIST);
		    zoneClusterList = (String [])userInputs.get(
			Util.ZONE_CLUSTER_LIST);
		}

		/**
		 * FOR DEBUGGING ONLY
		 */
//		selectedCluster = Util.BASE_CLUSTER;
//		// selectedCluster = Util.ZONE_CLUSTER;
//		selZoneCluster = null;
//		// selZoneCluster = "cz1";
//		devTabList = new ArrayList();
//		devTabList.add("/dev/md/crsdg/dsk/d1*");
//
//		devTabMap = new HashMap();
//		devTabMap.put("cz1", devTabList);
//
//		ArrayList devTabList2 = new ArrayList();
//		devTabList2.add("/dev/md/crsdg/dsk/d2*");
//		devTabMap.put("cz2", devTabList2);
//
//		zoneClusterList = new String[] { "cz1", "cz2" };

		/* END DEBUGGING */

		try {

		    // Get only the OBAN Disksets
		    // Here,SVMDEVICEGROUP = "Multi-owner_SVM"
		    // If this representation of OBAN in the DeviceGroupMBean is
		    // changed the value of SVMDEVICEGROUP should be changed as
		    // well.
		    QueryExp query = Query.match(new AttributeValueExp("Type"),
			    Query.value(Util.SVMDEVICEGROUP));

		    ObjectNameFactory dgOnf = new ObjectNameFactory(
			    DeviceGroupMBean.class);
		    ObjectName dgNamePattern = dgOnf.getObjectNamePattern(
			    DeviceGroupMBean.class);
		    mBeanServer.queryNames(dgNamePattern, null);

		    DiskGroupInfo pcDiskGroupInfo = null;
		    DiskGroupInfo zcDiskGroupInfo = null;
		    HashMap pcAvailSVMVolMap = null;
		    HashMap pcAvailQfsSVMVolMap = null;
		    HashMap pcNotAvailVolMap = null;

		    Set dgMBeansObjNames = mBeanServer.queryNames(dgNamePattern,
			    query);

		    for (Iterator iterator = dgMBeansObjNames.iterator();
							iterator.hasNext();) {
			ObjectName dgMBeanObjName =
				(ObjectName)iterator.next();
			DeviceGroupMBean dgMBean = (DeviceGroupMBean)
			    MBeanServerInvocationHandler.newProxyInstance(
				mBeanServer, dgMBeanObjName,
				DeviceGroupMBean.class,
				false);

			// Check whether the RAC RG Nodelist is contained
			// in the DeviceGroup's Nodelist
			String dgNodes[] = dgMBean.getConnectedNodeOrder();
			List dgNodeList = Arrays.asList(dgNodes);

			if (dgNodeList.containsAll(racNodeList)) {
			    String diskGroupName = dgMBean.getName();
			    String primaryNodeName =
				    dgMBean.getPrimaryNodeName();

			    // Need a function that returns us the volumes given
			    // the disk group name

			    HashMap volumes = getObanVolumes(diskGroupName);
			    if (volumes != null) {
				pcAvailSVMVolMap = new HashMap(volumes);
			    }

			    pcNotAvailVolMap = new HashMap();

			    if (selZoneCluster != null) {
				// If configuring on a zone cluster, get the
				// device group entries and find the ones that
				// match the device group

				if (devTabMap != null && !devTabMap.isEmpty()) {
				    devTabList = (ArrayList)devTabMap.get(
					selZoneCluster);
				}

				// Get the FSTab entry and fetch the disk group
				// name the volume name from it.
				if (fsTabMap != null && !fsTabMap.isEmpty()) {
				    fsTabList = (ArrayList)fsTabMap.get(
					selZoneCluster);
				}

				zcDiskGroupInfo = getZCDiskGroupInfo(
				    devTabList, fsTabList, diskGroupName,
				    volumes, primaryNodeName, nodeList);

				if (zcDiskGroupInfoList == null) {
				    zcDiskGroupInfoList = new ArrayList();
				}

				zcDiskGroupInfoList.add(zcDiskGroupInfo);

			    } else {
				// physical cluster. Get the volumes for each of
				// the zone clusters to do the appropriate
				// filtering.
				if (zoneClusterList != null) {
				    for (int i = 0;
				    i < zoneClusterList.length; i++) {
					if (devTabMap != null &&
						!devTabMap.isEmpty()) {
					    devTabList = (ArrayList)
						    devTabMap.get(
							    zoneClusterList[i]);
					}


					if (fsTabMap != null &&
						!fsTabMap.isEmpty()) {
					    fsTabList = (ArrayList)fsTabMap.get(
						    zoneClusterList[i]);
					}

					zcDiskGroupInfo = getZCDiskGroupInfo(
					    devTabList, fsTabList,
					    diskGroupName,
					    volumes, primaryNodeName, nodeList);

					// see what SVM volumes are available in
					// ZC and remove them from the physical
					// cluster.
					HashMap zcAvailSVMVolMap = (HashMap)
					    zcDiskGroupInfo.getAvailSVMVolMap();

					if (pcAvailSVMVolMap != null) {

					    // filter out the SVM and shared QFS
					    //  with SVM disk groups

					    if (zcAvailSVMVolMap != null) {

						Set zcEntrySet =
						    zcAvailSVMVolMap.entrySet();
						Set pcEntrySet =
						    pcAvailSVMVolMap.entrySet();
						pcEntrySet.removeAll(
							zcEntrySet);
					    }

					}

					if (pcNotAvailVolMap != null) {
					    if (zcAvailSVMVolMap != null) {
						pcNotAvailVolMap.putAll(
							zcAvailSVMVolMap);
					    }
					}
				    }
                                }

				pcDiskGroupInfo = new DiskGroupInfo();
				pcDiskGroupInfo.setDiskGroupName(diskGroupName);
				pcDiskGroupInfo.setAvailSVMVolMap(
							pcAvailSVMVolMap);
				pcDiskGroupInfo.setAvailQfsSVMVolMap(null);
				pcDiskGroupInfo.setNotAvailVolMap(
							pcNotAvailVolMap);
				pcDiskGroupInfo.setPrimaryNodeName(
							primaryNodeName);

				if (pcDiskGroupInfoList == null) {
				    pcDiskGroupInfoList = new ArrayList();
				}
				pcDiskGroupInfoList.add(pcDiskGroupInfo);
			    }
			}
		    }

		    if (selZoneCluster != null) {
			// Look for a resource that matches the entire disk set
			// or volumes in the disk set.
			// Check whether the deviceGroup and its volumes are
			// under any resource of type SUNW.ScalDeviceGroup

			// fill resource information for the available volumes.
			zcDiskGroup = new ClusterDiskGroupInfo(
			    selZoneCluster, zcDiskGroupInfoList);

			resultMap.put(Util.SVMDEVICEGROUP, zcDiskGroup);
		    } else {
			pcDiskGroup = new ClusterDiskGroupInfo(
			    Util.BASE_CLUSTER_KEY, pcDiskGroupInfoList);
			resultMap.put(Util.SVMDEVICEGROUP, pcDiskGroup);
		    }

		} catch (Exception e) {
		    e.printStackTrace();
		}

	    }
	    // Close local connector after discovery of all properties
	    localConnector.close();
	} catch (IOException ioe) {
	    ioe.printStackTrace();
	} catch (SecurityException se) {
	    se.printStackTrace();
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    return resultMap;
	}
    }

    /**
     * Parse the metastat -s <diskGroupName> -p output to get the volumes.e.g.
     * metastat -s racdg -p
     *	racdg/d3 -m racdg/d30 racdg/d31 1
     *	racdg/d30 1 1 /dev/did/rdsk/d4s0
     *	racdg/d31 1 1 /dev/did/rdsk/d5s0
     *	racdg/d4 -m racdg/d40 racdg/d41 1
     *	racdg/d40 1 1 /dev/did/rdsk/d4s1
     * 	racdg/d41 1 1 /dev/did/rdsk/d5s1
     * @param String diskGroupName
     * @return map containing the volumes in the disk group and a RS value
     *	       initialized to null
     */
    private HashMap getObanVolumes(String diskGroupName) {
	HashMap volumesMap = null;
	ArrayList volsToSkip = null;

	try {
	    ObjectNameFactory inonf = new ObjectNameFactory(Util.DOMAIN);
	    ObjectName infraMBeanObjName = inonf.getObjectName(
		InfrastructureMBean.class, null);
	    InfrastructureMBean infrastructureMBean = (InfrastructureMBean)
		MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
			infraMBeanObjName, InfrastructureMBean.class, false);

	    List commands = new ArrayList();
	    List output = null;

	    // Execute metaset -s <diskset> -p command
	    commands.add(METASTAT_CMD);
	    commands.add("-s");
	    commands.add(diskGroupName);
	    commands.add("-p");

	    String commandArr[] = new String[commands.size()];
	    commandArr = (String[])commands.toArray(commandArr);

	    try {
		ExitStatus status[] = infrastructureMBean.executeCommand(
			commandArr);

		if ((status.length > 0) &&
			(status[0].getReturnCode().intValue() ==
			status[0].SUCCESS)) {
		    output = status[0].getOutStrings();

		    for (Iterator o = output.iterator(); o.hasNext();) {
			String outLine = (String)o.next();

			String metastatOut[] = outLine.trim().split("[ \t]+");

			if (metastatOut != null && metastatOut.length > 1) {

			    if (volsToSkip == null ||
				!volsToSkip.contains(metastatOut[0])) {
				String dgInfo[] =
					metastatOut[0].trim().split(Util.SLASH);


				String secEntry = metastatOut[1];

				if (secEntry.equals(MINUS_M)) {
				    if (volsToSkip == null) {
					volsToSkip = new ArrayList();
				    }

				    for (int i = 2; i < metastatOut.length - 2; i++) {
					if (!volsToSkip.contains(metastatOut[i])){
					    volsToSkip.add(metastatOut[i]);
					}
				    }
				}

				// add the device
				if (dgInfo.length >= 2) {
				    String volName = dgInfo[1];
				    if (volumesMap == null) {
					volumesMap = new HashMap();
				    }

				    volumesMap.put(volName, null);
				}
			    }
			}
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    return volumesMap;
	}
    }

    /**
     * get path name of the form /dev/md/<diskGroup>/dsk/volume.
     * @param diskGroupName name of the disk group
     * @param volume
     * @return Complete Oban Path
     */
    private String[] getCompleteObanPath(String diskGroupName, String volume) {

	String[] obanPaths = new String[2];
	StringBuffer volBuffer = new StringBuffer();
	volBuffer.append(SVM_HDR);
	volBuffer.append(Util.SLASH);
	volBuffer.append(diskGroupName);
	volBuffer.append(Util.DSK);
	volBuffer.append(volume);

	obanPaths[0] = volBuffer.toString();

	StringBuffer volBuffer2 = new StringBuffer();
	volBuffer2.append(SVM_HDR);
	volBuffer2.append(Util.SLASH);
	volBuffer2.append(diskGroupName);
	volBuffer2.append(Util.RDSK);
	volBuffer2.append(volume);

	obanPaths[1] = volBuffer2.toString();

	return obanPaths;
    }

    /**
     * get a Map for a zone cluster containing the diskgroup name, list of
     * available volumes and a list of volumes not available for zone cluster.
     * @param devTabList list containing the devices exported to a zone cluster.
     * @param fsTabList list containing the filesystems exported to a zc.
     * @param list list containing the device entries or fstab entries depending
     *        on the storage scheme.
     * @param diskGroupName name of the disk group
     * @param volumes list of volumes contained in the disk group
     * @param primaryNodeName name of the primary node for the disk group
     * @param nodeList the list of nodes
     * @return diskGroupInfo containing the disk group information
     */
    private DiskGroupInfo getZCDiskGroupInfo(
	ArrayList devTabList, ArrayList fsTabList,
	String diskGroupName, HashMap volumes,
	String primaryNodeName, String[] nodeList) {

	DiskGroupInfo diskGroupInfo = new DiskGroupInfo();

	HashMap notAvailVolMap = new HashMap();
	HashMap availSVMVolMap = null;
	HashMap availQfsSVMVolMap = null;

	if (volumes != null) {
	    notAvailVolMap = new HashMap(volumes);
	}

	// For SVM use the devtab list to get the list of devices
	if (devTabList != null) {
	    int size = devTabList.size();
	    for (int k = 0; k < size; k++) {
		String devTabEntry = (String)devTabList.get(k);

		// Pattern for checking disk pathname
		String regEx = CMASUtil.wildCardToRegix(devTabEntry);
		Pattern p = Pattern.compile(regEx);

		if (volumes != null) {
		    Set keySet = volumes.keySet();
		    Iterator iterator = keySet.iterator();
		    Matcher m = null;
		    while (iterator.hasNext()) {
			String volume = (String)iterator.next();

			String[] obanPaths = getCompleteObanPath(
			    diskGroupName,
			    volume);

			boolean found = false;
			int ctr = 0;
			while (!found && ctr < obanPaths.length) {

			    m = p.matcher(obanPaths[ctr]);
			    if (m.matches()) {
				// this entry has been exported to the ZC
				// Display this entry
				if (availSVMVolMap == null) {
				    availSVMVolMap = new HashMap();
				}
				availSVMVolMap.put(volume, null);
				try {
				    notAvailVolMap.remove(volume);
				} catch (Exception e) {
				    e.printStackTrace();
				}
				found = true;
			    }
			    ctr++;
			}
		    }
		}
	    }
	}

	ZoneClusterFSTab fsTabEntry = null;
	if (fsTabList != null) {
	    int size = fsTabList.size();

	    for (int i = 0; i < size; i++) {
		fsTabEntry = (ZoneClusterFSTab)fsTabList.get(i);
		if (fsTabEntry != null) {
		    String specialFile = fsTabEntry.getFS();

		    // for direct mounts this is the name of the QFS file system
		    // as it appears in the mcf file

//		    FOR LOOPBACK MOUNTS
//
//		    // Check in the /etc/vfstab file of the global zone
//		    // and get the device to mount
//		    File zcFile = new File(specialFile);
//		    String zcFilePath = zcFile.getAbsolutePath();
//
//		    // Get all entries matching the vfstab entry in ZC
//		    ArrayList vfsSpecialEntries = getVfsSpecialEntriesForZC(
//			zcFilePath, nodeList);
//		    if (vfsSpecialEntries != null) {
//			int vfsSize = vfsSpecialEntries.size();
//			for (int j = 0; j < vfsSize; j++) {
//			    // get the OBAN DG for this filesystem
//			    String diskGroup = getDiskGroupEntryForQFS(
//				(String)vfsSpecialEntries.get(j));
//			}
//		    }


		    String diskGroup = getDiskGroupEntryForQFS(specialFile);

		    if (diskGroup != null) {

			if (volumes != null) {
			    Set keySet = volumes.keySet();
			    Iterator iterator = keySet.iterator();
			    while (iterator.hasNext()) {
				String volume = (String)iterator.next();

				// volume of the disk group. Check if this
				// is contained in the SVM volMap
				Set keySet2 = null;
				if (availSVMVolMap != null) {
				    keySet2 = availSVMVolMap.keySet();
				}
				if (availSVMVolMap == null ||
				    !keySet2.contains(volume)) {
				    // add to QfsSVMVolMap

				    if (availQfsSVMVolMap == null) {
					availQfsSVMVolMap = new HashMap();
				    }
				    availQfsSVMVolMap.put(volume, null);
				    try {
					notAvailVolMap.remove(volume);
				    } catch (Exception e) {
					e.printStackTrace();
				    }
				}
			    }
			}
		    }
		}
	    }
	}

	diskGroupInfo.setDiskGroupName(diskGroupName);
	diskGroupInfo.setAvailSVMVolMap(availSVMVolMap);
	diskGroupInfo.setAvailQfsSVMVolMap(availQfsSVMVolMap);
	diskGroupInfo.setNotAvailVolMap(notAvailVolMap);
	diskGroupInfo.setPrimaryNodeName(primaryNodeName);

	return diskGroupInfo;
    }

    public ErrorValue validateInput(String propertyNames[], HashMap userInputs,
	    Object helperData) {

	ErrorValue err = new ErrorValue();
	err.setReturnVal(Boolean.TRUE);

	try {
	    ServiceConfig oracleDS = getConfigObject();
	    err = oracleDS.validateInput(propertyNames, userInputs,
		    helperData);
	} catch (Exception ex) {
	    ex.printStackTrace();
	}

        return err;
    }

    public ErrorValue applicationConfiguration(Object helperData) {

        FileAccessorWrapper fileWrapper = null;

        // For NAS and QFS-Metaserver Resource Creation, we need to add the
        // mountPoint to the node's vfstab file and mount it before creating
        // the resource

        Map appConf = (HashMap)helperData;
        String mountPoint = (String)appConf.get(Util.MOUNT_POINT);
        String fsName = (String)appConf.get(Util.FILE_SYSTEM_NAME);
        String fsType = (String)appConf.get(Util.FILE_SYSTEM_TYPE);
        List nodeList = (ArrayList)appConf.get(Util.NODELIST);
        String nodes[] = new String[nodeList.size()];
        nodes = (String[])nodeList.toArray(nodes);

        // Read the vfstab file on the node
        createMBeanServerConnections(nodes);

        try {
            fileWrapper = new FileAccessorWrapper(Util.VFSTAB_FILE,
                    mbsConnections);

            for (int i = 0; i < nodes.length; i++) {
                boolean entryFound = false;
                List contents = (ArrayList)fileWrapper.readFully(
                        mbsConnections[i]);

                for (Iterator iter = contents.iterator(); iter.hasNext();) {
                    VfsStruct vfsEntry = (VfsStruct)iter.next();

                    if (vfsEntry.getVfs_special().equals(fsName)) {
                        entryFound = true;

                        break;
                    }
                }

                if (!entryFound) {
                    VfsStruct newVfsEntry = new VfsStruct();
                    List structList = new ArrayList();
                    newVfsEntry.setVfs_special(fsName);
                    newVfsEntry.setVfs_mountp(mountPoint);

                    if (fsType.equals(Util.SQFS)) {
                        newVfsEntry.setVfs_fstype("samfs");
                        newVfsEntry.setVfs_automnt("no");
                        newVfsEntry.setVfs_mntopts("shared");
                    } else {
                        newVfsEntry.setVfs_fstype("NAS");
                        newVfsEntry.setVfs_automnt("no");
                    }

                    structList.add(newVfsEntry);
                    fileWrapper.appendLine(structList, mbsConnections[i]);
                }

                // Create the mountpoint dir if not present and mount the
                // filesystem
                fileWrapper.createDir(mountPoint, mbsConnections[i]);

                List commands = new ArrayList();
                commands.add("/usr/sbin/mount");
                commands.add(mountPoint);

                ObjectNameFactory infonf = new ObjectNameFactory(Util.DOMAIN);
                ObjectName infraMBeanObjName = infonf.getObjectName(
                        InfrastructureMBean.class, null);
                InfrastructureMBean infrastructureMBean = (InfrastructureMBean)
                    MBeanServerInvocationHandler.newProxyInstance(
                        mbsConnections[i], infraMBeanObjName,
                        InfrastructureMBean.class, false);
                String commandArr[] = new String[commands.size()];
                commandArr = (String[])commands.toArray(commandArr);

                try {
                    infrastructureMBean.executeCommand(commandArr);
                } catch (CommandExecutionException cee) {
                    // Happens when the mount point is already mounted
		    cee.printStackTrace();
                }
            }

            return null;
        } catch (Exception e) {
	    e.printStackTrace();
            return null;
        } finally {
            closeConnections(nodes);
        }
    }

    public String[] aggregateValues(String propertyName, Map unfilteredValues,
        Object helperData) {
        return null;
    }

    private ServiceConfig getConfigObject() {

        if (oracleConfiguration == null) {
            oracleConfiguration = serviceInfo.getConfigurationClass(this
                    .getClass().getClassLoader());
        }

        return oracleConfiguration;
    }

    private HashMap getNodeSpecificStringValues(Map xConfMap,
        String xNodeList[], String xKey) {
        HashMap zValueMap = new HashMap();

        for (int i = 0; i < xNodeList.length; i++) {
            String curNode = xNodeList[i];
            String tmpArry[] = curNode.split(Util.COLON);
            curNode = tmpArry[0];
            zValueMap.put(curNode, (String)xConfMap.get(xKey + curNode));
        }

        return zValueMap;
    }

    private ResourceProperty getNodeSpecificPropValues(Map xConfMap,
        String xNodeList[], String xKey) {
        HashMap zValueMap = new HashMap();

        for (int i = 0; i < xNodeList.length; i++) {
            String curNode = xNodeList[i];
            String tmpArry[] = curNode.split(Util.COLON);
            curNode = tmpArry[0];

            ResourceProperty prop = (ResourceProperty)xConfMap.get(xKey +
                    curNode);

            if (prop == null) {
                continue;
            }

            String propName = prop.getName();

            if (prop instanceof ResourcePropertyString) {
                String val = ((ResourcePropertyString)prop).getValue();

                if ((val != null) && (val.trim().length() > 0)) {
                    zValueMap.put(curNode, val);
                }

                if (i == (xNodeList.length - 1)) {
                    ResourcePropertyString propStr =
                        new ResourcePropertyString();
                    propStr.setName(propName);
                    propStr.setPernode(true);
                    propStr.setPernodeValue(zValueMap);

                    return (ResourceProperty)propStr;
                }
            } else if (prop instanceof ResourcePropertyInteger) {
                Integer val = ((ResourcePropertyInteger)prop).getValue();

                if (val != null) {
                    zValueMap.put(curNode, val);
                }

                if (i == (xNodeList.length - 1)) {
                    ResourcePropertyInteger propInt =
                        new ResourcePropertyInteger();
                    propInt.setName(propName);
                    propInt.setPernode(true);
                    propInt.setPernodeValue(zValueMap);

                    return (ResourceProperty)propInt;
                }
            } else if (prop instanceof ResourcePropertyBoolean) {
                Boolean val = ((ResourcePropertyBoolean)prop).getValue();

                if (val != null) {
                    zValueMap.put(curNode, val);
                }

                if (i == (xNodeList.length - 1)) {
                    ResourcePropertyBoolean propBoo =
                        new ResourcePropertyBoolean();
                    propBoo.setName(propName);
                    propBoo.setPernode(true);
                    propBoo.setPernodeValue(zValueMap);

                    return (ResourceProperty)propBoo;
                }
            } else if (prop instanceof ResourcePropertyEnum) {
                String val = ((ResourcePropertyEnum)prop).getValue();

                if ((val != null) && (val.trim().length() > 0)) {
                    zValueMap.put(curNode, val);
                }

                if (i == (xNodeList.length - 1)) {
                    ResourcePropertyEnum propEnum = new ResourcePropertyEnum();
                    propEnum.setName(propName);
                    propEnum.setPernode(true);
                    propEnum.setPernodeValue(zValueMap);

                    return (ResourceProperty)propEnum;
                }
            } else if (prop instanceof ResourcePropertyStringArray) {
                String val[] = ((ResourcePropertyStringArray)prop).getValue();

                if ((val != null) && (val.length > 0)) {
                    zValueMap.put(curNode, val);
                }

                if (i == (xNodeList.length - 1)) {
                    ResourcePropertyStringArray propArr =
                        new ResourcePropertyStringArray();
                    propArr.setName(propName);
                    propArr.setPernode(true);

// ResourcePropertyStringArray does seem to have a way to set per-node
// values
// propArr.setPernodeValue(zValueMap);
                    propArr.setValue(val);

                    return (ResourceProperty)propArr;
                }
            }
        }

        return null;
    }


    /**
     * Methods for RAC Storage Wizard
     */

    /**
     * Fills the serviceInfo structure for Storage Resources
     *
     * @param Map storageMap containing the StorageResources Information
     * @param String waitZCBootRSName name of the wait for ZC Boot resource
     * @param String waitZCBootRGName name of the wait for ZC Boot Group
     * @param String frameworkRG name of the Framework resource group
     * @param String frameworkRGInBC name of the framework RG in the base
     *	      cluster
     * @param String selZoneCluster the name of the ZC cluster or null
     * @param String zonePath the zone cluster path if configuring for a ZC else
     *	      null
     */
    private void fillServiceInfoForStorage(Map storageMap,
	String waitZCBootRSName, String waitZCBootRGName,
	String frameworkRG, String frameworkRGInBC,
	String selZoneCluster, String zonePath) {

	ResourceGroupInfo waitZCBootGroupInfo =
	    serviceInfo.getResourceGroup(Util.WAIT_ZC_BOOT_GROUP);

	String[] baseClusterNodeList = (String[])storageMap.get(
	    Util.BASE_CLUSTER_NODELIST);

	if (waitZCBootRSName != null) {
	    fillServiceInfoForWaitZCBoot(
		waitZCBootGroupInfo, 
		waitZCBootRSName, waitZCBootRGName,
		baseClusterNodeList, selZoneCluster);
	}

        // We would use the STORAGEGROUP structure as a
        // template only
        ResourceGroupInfo storageGroup_for_dgs = serviceInfo.getResourceGroup(
                Util.DG_STORAGEGROUP);
        ResourceGroupInfo storageGroup_for_fs = serviceInfo.getResourceGroup(
                Util.FS_STORAGEGROUP);

        // Get the DiskResources
        String diskResources[] = (String[])storageMap.get(
                Util.SELECTED_DG_RESOURCES);

        if (diskResources != null) {

            // Fill ServiceInfo for DiskResources
            fillServiceInfoForDisk(
		storageMap, diskResources, storageGroup_for_dgs,
		frameworkRG, frameworkRGInBC, selZoneCluster);
        }

        // Get the Filesystems Resources
        String fsResources[] = (String[])storageMap.get(
                Util.SELECTED_FS_RESOURCES);

        if (fsResources != null) {

            // Fill ServiceInfo for FileResources
            fillServiceInfoForFilesystems(storageMap, fsResources,
                storageGroup_for_fs,
		waitZCBootRSName, waitZCBootRGName,
		frameworkRG,
		selZoneCluster, zonePath);
        }
    }

    /**
     * Fills the serviceInfo structure for waitZCBoot resources
     *
     * @param waitZCBootRSName name of the resource for waitZCBoot
     * @param waitZCBootRGName name of the group for waitZCBoot
     * @param baseClusterNodeList array containing the selected base cluster nodes.
     * @param String selZoneCluster the name of the ZC
     */
    private void fillServiceInfoForWaitZCBoot(
	ResourceGroupInfo argWaitZCBootGroupInfo,
	String waitZCBootRSName, String waitZCBootRGName,
	String[] baseClusterNodeList, String selZoneCluster) {

	ResourceInfo waitZCBootResourceInfo = null;
	ResourceGroupInfo waitZCBootGroupInfo = null;

	if (selZoneCluster != null &&
	    selZoneCluster.trim().length() > 0) {

	    /**
	     * Fill Group Structure for WaitZCBootGroup
	     */
	    waitZCBootGroupInfo =
		    serviceInfo.getResourceGroup(
			Util.WAIT_ZC_BOOT_GROUP, waitZCBootRGName);

	    RGroupMBean rGroupMBean = (RGroupMBean)
		MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
		    onf.getObjectName(RGroupMBean.class, null),
		    RGroupMBean.class, false);

	    RgGroupMBean rgGroupMBean = (RgGroupMBean)
		MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
		    onf.getObjectName(RgGroupMBean.class, null),
		    RgGroupMBean.class, false);

	    ResourceData rData = null;
	    ResourceGroupData rgData = null;

	    if (waitZCBootGroupInfo == null) {
		waitZCBootGroupInfo = argWaitZCBootGroupInfo.copyGroup();

		StringBuffer buffer = new StringBuffer();
		waitZCBootGroupInfo.setName(waitZCBootRGName);
		// waitZCBoot RG always runs in base cluster
		waitZCBootGroupInfo.setZoneClusterName(null);

		rgData = rgGroupMBean.getResourceGroup(waitZCBootRGName, null);

		if (rgData == null) {

		    Map rgProperties = new HashMap();

		    for (int i = 0; i < baseClusterNodeList.length; i++) {
			buffer.append(baseClusterNodeList[i]);

			if (i < (baseClusterNodeList.length - 1)) {
			    buffer.append(",");
			}
		    }

		    rgProperties.put(Util.NODELIST, buffer.toString());
		    rgProperties.put(Util.RG_MODE, Util.SCALABLE_RG_MODE);
		    rgProperties.put(Util.MAX_PRIMARIES,
			String.valueOf(baseClusterNodeList.length));
		    rgProperties.put(Util.DESIRED_PRIMARIES,
			String.valueOf(baseClusterNodeList.length));
		    waitZCBootGroupInfo.setRgProperties(rgProperties);
		}
		serviceInfo.getResourceGroupList().add(waitZCBootGroupInfo);
		duplicateRGStructures.add(waitZCBootGroupInfo);
	    }

	    /*
	     * Fill WaitZCBoot Resource Structure
	     */
	    waitZCBootResourceInfo = serviceInfo.getResource(
		    waitZCBootGroupInfo,
		    Util.WAIT_ZC_BOOT_RESOURCE);

	    if (waitZCBootResourceInfo.getResourceName() != null) {

		// This means there is already a Resource configured
		// Create another resource in the same group

		ResourceInfo newWaitZCBootResourceInfo =
		    waitZCBootResourceInfo.copyResource(waitZCBootGroupInfo);
		waitZCBootResourceInfo = newWaitZCBootResourceInfo;
	    }

	    waitZCBootResourceInfo.setResourceName(waitZCBootRSName);
	    waitZCBootResourceInfo.setZoneClusterName(null);
	    rData = rGroupMBean.getResourceData(waitZCBootRSName, null);
	    if (rData == null) {
		// No resource found
		List extProperties = new ArrayList();
		ResourceProperty rp = new ResourcePropertyString(
		    Util.ZCNAME_PROP, selZoneCluster);
		extProperties.add(rp);
		waitZCBootResourceInfo.setExtProperties(extProperties);
	    }
	    waitZCBootResourceInfo.setBringOnline(true);
        }
    }

    /**
     * Fills the serviceInfo structure for File System Resources
     *
     * @param Map  containing Storage Resources Information
     * @param String  Array containing Filesystem Resources
     * @param ResourceGroupInfo  group to be used as template,
     * @param waitZCBootRSName name of the resource for waitZCBoot
     * @param waitZCBootRGName name of the group for waitZCBoot
     * @param String selZoneCluster the name of the ZC or null
     * @param String zonePath the path of the zone or null if
     * if configuring on a base cluster
     */
    private void fillServiceInfoForFilesystems(Map storageMap,
        String fsResources[], ResourceGroupInfo storageGroup,
	String waitZCBootRSName, String waitZCBootRGName,
        String frameworkRG, String selZoneCluster, String zonePath) {

        /*
         * Command Generation Steps for Filesystem Resource
         *
         * 1. Get the Filesystem Resources List
         * 2. For every Filesystem Resource
         *  2(a) Get the resourceInformation (Group,Type)
         *  2(b) If it is a QFS Filesystem
         *       If s-qfs is on OBAN
         *              Get the OBAN Diskgroup under the QFS Filesystem.
         *              Find the ResourceInfo Object for the OBAN Diskgroup
         *              Create the Resource for s-qfs on OBAN on that
         *              Resource's ResourceGroup
         *       If s-qfs is on RAW
         *              Check if a ResourceGroup Object with that name exists
         *              in the serviceInfo object
         *              If no create a new Group Object with that name
         *              Add the s-qfs on RAW resource on that ResourceGroup
         */

        for (int i = 0; i < fsResources.length; i++) {

            Map fsResourceMap = (HashMap)storageMap.get(fsResources[i]);

            String fsType = (String)fsResourceMap.get(Util.FILE_SYSTEM_TYPE);
            String fsName = (String)fsResourceMap.get(Util.FILE_SYSTEM_NAME);

            if (fsType.equals(Util.NAS)) {
                String nodeList[] = (String[])storageMap.get(Util.NODELIST);
                String resourceGroup = (String)fsResourceMap.get(
                        Util.STORAGEGROUP);
                String mountPoint = (String)fsResourceMap.get(
                        Util.MOUNT_POINT);
                fillServiceInfoForNAS(storageGroup, mountPoint, fsName,
                    fsResources[i], resourceGroup, nodeList, frameworkRG,
			selZoneCluster);
            } else if (fsType.equals(Util.SQFS)) {

                // Get s-qfs information
                String msResource = (String)fsResourceMap.get(
                        Util.METASERVER_RESOURCE);
                String msGroup = (String)fsResourceMap.get(
                        Util.METASERVER_GROUP);
                List msNodes = (ArrayList)fsResourceMap.get(
                        Util.METASERVER_NODES);
                String obanDG = (String)fsResourceMap.get(Util.OBAN_UNDER_QFS);
                String mountPoint = (String)fsResourceMap.get(
                        Util.MOUNT_POINT);
                String resourceGroup = (String)fsResourceMap.get(
                        Util.STORAGEGROUP);
                ResourceGroupInfo metaGroup = serviceInfo.getResourceGroup(
                        Util.METASERVER_GROUP);
                String nodeList[] = (String[])storageMap.get(Util.NODELIST);
		String baseClusterNodeList[] = (String[])
				storageMap.get(Util.BASE_CLUSTER_NODELIST);

                // s-qfs is created on RAW
                if (obanDG == null) {
                    fillServiceInfoForQFS_RAW(storageGroup, metaGroup,
                        msResource, msGroup, msNodes,
			mountPoint, fsName,
                        resourceGroup, fsResources[i],
			waitZCBootRSName, waitZCBootRGName,
			nodeList, baseClusterNodeList, frameworkRG,
			selZoneCluster, zonePath);
                } else {

                    // s-qfs is created on SVM Volumes
                    fillServiceInfoForQFS_OBAN(storageMap, storageGroup,
                        metaGroup, msResource, msGroup, msNodes,
			mountPoint, fsName, resourceGroup, fsResources[i],
			waitZCBootRSName, waitZCBootRGName,
			nodeList, baseClusterNodeList,
			obanDG, frameworkRG,
			selZoneCluster, zonePath);
                }
            }
        }
    }


    /**
     * Fill the serviceInfo structure for NAS
     *
     * @param  storageGroup  StorageGroup Template
     * @param  mountPoint  NAS Mountpoint
     * @param  resourceName  NAS Resource Name
     * @param  resourceGroup  Storage Group Name
     * @param  nodeList  StorageGroup Template
     * @param String selZoneCluster the name of the ZC or null
     */
    private void fillServiceInfoForNAS(ResourceGroupInfo storageGroup,
        String mountPoint, String exportedFS, String resourceName,
        String resourceGroup, String nodeList[], String frameworkRG,
	String selZoneCluster) {

        /*
         *  Fill Group Structure
         */
        ResourceGroupInfo fsGroupInfo = serviceInfo.getResourceGroup(
                Util.FS_STORAGEGROUP, resourceGroup);


        if (fsGroupInfo == null) {
            fsGroupInfo = storageGroup.copyGroup();
            fsGroupInfo.setName(resourceGroup);

	    RgGroupMBean rgGroupMBean = (RgGroupMBean)
		MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
		    onf.getObjectName(RgGroupMBean.class, null),
		    RgGroupMBean.class, false);

	    ResourceGroupData rgData = rgGroupMBean.getResourceGroup(
		resourceGroup, selZoneCluster);

	    if (rgData == null) {

                // Set the properties for the new Scalable RG
                Map rgProperties = new HashMap();
                StringBuffer buffer = new StringBuffer();

                for (int i = 0; i < nodeList.length; i++) {
                    buffer.append((String)nodeList[i]);

                    if (i < (nodeList.length - 1)) {
                        buffer.append(",");
                    }
                }

                rgProperties.put(Util.NODELIST, buffer.toString());
                rgProperties.put(Util.MAX_PRIMARIES,
                    String.valueOf(nodeList.length));
                rgProperties.put(Util.DESIRED_PRIMARIES,
                    String.valueOf(nodeList.length));
		rgProperties.put(Util.RG_MODE, Util.SCALABLE_RG_MODE);
                fsGroupInfo.setRgProperties(rgProperties);
		fsGroupInfo.setZoneClusterName(selZoneCluster);
            }

            // Add this group to the serviceInfo
            serviceInfo.getResourceGroupList().add(fsGroupInfo);
            duplicateRGStructures.add(fsGroupInfo);
        }

        /*
         *  Fill Resource Structure
         */
        ResourceInfo fsResourceInfo = serviceInfo.getResource(fsGroupInfo,
                Util.SCALABLE_NAS_RES);

        // If there are multiple mount points,
        // we need to create ResourceInfo objects for each of
        // the mountpoints.
        if (fsResourceInfo.getResourceName() != null) {

            // This means there is already a Resource configured for a
            // S-QFS mountPoint
            ResourceInfo newFSResourceInfo = fsResourceInfo.copyResource(
                    fsGroupInfo);
            fsResourceInfo = newFSResourceInfo;
        }

        fsResourceInfo.setResourceName(resourceName);
	fsResourceInfo.setZoneClusterName(selZoneCluster);

	RGroupMBean rGroupMBean = (RGroupMBean)
	    MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
		onf.getObjectName(RGroupMBean.class, null),
		RGroupMBean.class, false);

	ResourceData rData = rGroupMBean.getResourceData(
	    resourceName, selZoneCluster);

        // If its a new resource set the Extension Properties
        if (rData == null) {
            List extProperties = new ArrayList();
            ResourceProperty rp = new ResourcePropertyString(
                    Util.MOUNTPOINT_DIR, mountPoint);
            extProperties.add(rp);
            rp = new ResourcePropertyString(Util.FILE_SYSTEM_TYPE,
                    Util.NAS.toLowerCase());
            extProperties.add(rp);
            rp = new ResourcePropertyString(Util.TARGET_FILE_SYSTEM,
                    exportedFS);
            extProperties.add(rp);
            fsResourceInfo.setExtProperties(extProperties);
        }

        fsResourceInfo.setBringOnline(true);
    }

    /**
     * Fill the serviceInfo structure for s-qfs on OBAN
     */
    private void fillServiceInfoForQFS_OBAN(Map storageMap,
        ResourceGroupInfo storageGroup, ResourceGroupInfo metaGroup,
        String metaserverResource, String metaserverGroup, List metaserverNodes,
	String mountPoint, String fsName,
	String resourceGroup, String resourceName,
	String waitZCBootRSName, String waitZCBootRGName,
	String nodeList[], String baseClusterNodeList[],
	String obanDG, String frameworkRG, String selZoneCluster,
	String zonePath) {

	RGroupMBean rGroupMBean = (RGroupMBean)
	    MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
		onf.getObjectName(RGroupMBean.class, null),
		RGroupMBean.class, false);

	RgGroupMBean rgGroupMBean = (RgGroupMBean)
	    MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
		onf.getObjectName(RgGroupMBean.class, null),
		RgGroupMBean.class, false);

	ResourceData rData = null;
	ResourceGroupData rgData = null;

        /**
         * Fill Group Structure for Metaserver Group
         */
        ResourceGroupInfo metaGroupInfo = serviceInfo.getResourceGroup(
                Util.METASERVER_GROUP, metaserverGroup);

        if (metaGroupInfo == null) {
            metaGroupInfo = metaGroup.copyGroup();

            StringBuffer buffer = new StringBuffer();
            metaGroupInfo.setName(metaserverGroup);
	    // meta server RG always runs in the base cluster
	    metaGroupInfo.setZoneClusterName(null);

            Map rgProperties = new HashMap();

            for (int i = 0; i < baseClusterNodeList.length; i++) {
                buffer.append(baseClusterNodeList[i]);

                if (i < (baseClusterNodeList.length - 1)) {
                    buffer.append(",");
                }
            }

            rgProperties.put(Util.NODELIST, buffer.toString());
            metaGroupInfo.setRgProperties(rgProperties);

            serviceInfo.getResourceGroupList().add(metaGroupInfo);
            duplicateRGStructures.add(metaGroupInfo);
        }

        /*
         * Fill Metaserver Resource Structure
         */
        ResourceInfo metaResourceInfo = serviceInfo.getResource(metaGroupInfo,
                Util.METASERVER_RESOURCE);

        if (metaResourceInfo.getResourceName() != null) {

            // This means there is already a Resource configured for a
            // S-QFS mountPoint. Create another resource in the same group
            ResourceInfo newMetaResourceInfo = metaResourceInfo.copyResource(
                    metaGroupInfo);
            metaResourceInfo = newMetaResourceInfo;
        }

	String newMountPoint = mountPoint;

	if (selZoneCluster != null &&
	    selZoneCluster.trim().length() > 0) {
	    // configuring on a zone cluster
	    newMountPoint = zonePath + "/root" + mountPoint;
	    metaResourceInfo.setResourceName(
		    Util.GLOBAL_PREFIX + metaserverResource);
	} else {
	    metaResourceInfo.setResourceName(metaserverResource);
	}

	// The meta server resources are always created in the base cluster
	metaResourceInfo.setZoneClusterName(null);

	rData = rGroupMBean.getResourceData(
	    metaserverResource, null);

        if (rData == null) {
            List extProperties = new ArrayList();
            ResourceProperty rp = new ResourcePropertyStringArray(
                    Util.QFS_FILE_SYSTEM, new String[] { newMountPoint });
            extProperties.add(rp);
            metaResourceInfo.setExtProperties(extProperties);
        }

	// Make sure the mountpoint is added to all the node's vfstab entries
	// and mounted on all the nodes
	Map appConfMap = new HashMap();
        appConfMap.put(Util.NODELIST, metaserverNodes);
        appConfMap.put(Util.MOUNT_POINT, newMountPoint);
        appConfMap.put(Util.FILE_SYSTEM_NAME, fsName);
        appConfMap.put(Util.FILE_SYSTEM_TYPE, Util.SQFS);
        applicationConfiguration(appConfMap);

        // Discover the Scalable GDD Resource for the ObanDG
        // Create the s-qfs Resource in that ResourceGroup

        // Try to find a GDD Resource for the obanDG in the cluster
        // if not found it must have been configured by the wizard itself
	// For s-QFS with SVM, the Scalable Device group runs in the base
	// cluster

        ResourceGroupInfo obanDGResourceGroup = null;

        List dgResources = rGroupMBean.getResourceList(null,
	    "SUNW.ScalDeviceGroup", Util.DISKGROUP_NAME, obanDG);

        // Store the dg resource name for the FS to set
        // offline restart dependency later
        String dgResourceName = null;
	ResourceInfo diskResourceInfo = null;

        // If there is no existing resource for the diskgroup
        // The diskResource might have been configured by the wizard

        if ((dgResources == null) || (dgResources.size() == 0)) {

            // Search the storageMap for the diskResource
            boolean diskGroupInfoFound = false;
            String diskResources[] = (String[])storageMap.get(
                    Util.SELECTED_DG_RESOURCES);

            if (diskResources != null) {

                for (int i = 0; i < diskResources.length; i++) {

                    // Diskgroup Resource Information
                    String diskResource = diskResources[i];

                    Map diskResourceMap = (HashMap)storageMap.get(
                            diskResource);
                    String resourceGroupName = (String)diskResourceMap.get(
                            Util.STORAGEGROUP);
                    String diskGroupType = (String)diskResourceMap.get(
                            Util.DISKGROUP_TYPE);
                    String diskGroupName = (String)diskResourceMap.get(
                            Util.DISKGROUP_NAME);

                    if (diskGroupType.equals(Util.SVMDEVICEGROUP) &&
			diskGroupName.equals(obanDG)) {
                        obanDGResourceGroup = serviceInfo.getResourceGroup(
                                Util.DG_STORAGEGROUP, resourceGroupName);

                        // Store the dg resource name for the FS to set
                        // offline restart dependency later
                        dgResourceName = diskResource;
                        diskGroupInfoFound = true;

                        break;
                    }
                }
            }

            // Configure the oban dg for the resource

            if (!diskGroupInfoFound) {

                // generate the ResourceGroup and the ResourceNames
                // from the oban dg

                String propertyName[] = null;

                propertyName = new String[] { Util.STORAGERESOURCE };

		// TODO : generate Resource in RAC Model

                Map discoveredMap = discoverPossibilities(propertyName, obanDG);
                String diskResourceName = (String)discoveredMap.get(
                        Util.STORAGERESOURCE);

                // Store the dg resource name for the FS to set
                // offline restart dependency later
                dgResourceName = diskResourceName;

                propertyName = new String[] { Util.STORAGEGROUP };
                discoveredMap = discoverPossibilities(propertyName,
                        diskResourceName);

                String diskResourceGroupName = (String)discoveredMap.get(
                        Util.STORAGEGROUP);

                obanDGResourceGroup =
                    (serviceInfo.getResourceGroup(Util.DG_STORAGEGROUP))
                    .copyGroup();
                obanDGResourceGroup.setName(diskResourceGroupName);
		obanDGResourceGroup.setZoneClusterName(null);

                // Set the properties for the new Scalable RG
                Map rgProperties = new HashMap();
                StringBuffer buffer = new StringBuffer();

                for (int j = 0; j < baseClusterNodeList.length; j++) {
                    buffer.append((String)baseClusterNodeList[j]);

                    if (j < (baseClusterNodeList.length - 1)) {
                        buffer.append(",");
                    }
                }

                rgProperties.put(Util.NODELIST, buffer.toString());
                rgProperties.put(Util.MAX_PRIMARIES,
                    String.valueOf(nodeList.length));
                rgProperties.put(Util.DESIRED_PRIMARIES,
                    String.valueOf(nodeList.length));
                rgProperties.put(Util.RG_AFFINITY,
                    Util.PLUS_PLUS + frameworkRG);
		rgProperties.put(Util.RG_MODE, Util.SCALABLE_RG_MODE);
                obanDGResourceGroup.setRgProperties(rgProperties);

                diskResourceInfo = serviceInfo.getResource(
                        obanDGResourceGroup, Util.SCALABLE_SVM_RES);
                diskResourceInfo.setResourceName(diskResourceName);
		diskResourceInfo.setZoneClusterName(null);
                diskResourceInfo.setBringOnline(true);

                // Set the extension properties
                List extProperties = new ArrayList();
                ResourceProperty rp = new ResourcePropertyString(
                        Util.DISKGROUP_NAME, obanDG);
                extProperties.add(rp);
                diskResourceInfo.setExtProperties(extProperties);

                serviceInfo.getResourceGroupList().add(obanDGResourceGroup);
                duplicateRGStructures.add(obanDGResourceGroup);
            }

        } else {
	    // There is an existing resource for the diskgroup
	    // in the cluster
            String dgresourceName = (String)dgResources.get(0);

	    rData = rGroupMBean.getResourceData(dgresourceName, null);

            // Store the dg resource name for the FS to set
            // offline restart dependency later
            dgResourceName = dgresourceName;

	    String resourceGroupName = rData.getResourceGroupName();

	    // There might be some other resource in the resource group
	    obanDGResourceGroup = serviceInfo.getResourceGroup(
		Util.DG_STORAGEGROUP, resourceGroupName);

	    if (obanDGResourceGroup == null) {
		// Create new ResourceGroupInfo structure for the
		// resourceGroup and resource
		obanDGResourceGroup =
		    (serviceInfo.getResourceGroup(
				Util.DG_STORAGEGROUP)).copyGroup();

		obanDGResourceGroup.setName(resourceGroupName);
		obanDGResourceGroup.setZoneClusterName(null);
		serviceInfo.getResourceGroupList().add(obanDGResourceGroup);
		duplicateRGStructures.add(obanDGResourceGroup);
	    }

	    diskResourceInfo = serviceInfo.getResource(
		obanDGResourceGroup, Util.SCALABLE_SVM_RES);
	    diskResourceInfo.setResourceName(dgresourceName);
	    diskResourceInfo.setZoneClusterName(null);
	    diskResourceInfo.setBringOnline(true);
        }

	if (dgResourceName != null) {

	    ArrayList metaserverSysProps = new ArrayList();

	    String[] rsDependencies = null;

	    if (selZoneCluster != null &&
		selZoneCluster.trim().length() > 0) {

		// configuring on a zone cluster
		if (waitZCBootRSName != null) {
		    rsDependencies = new String[] { dgResourceName,
						    waitZCBootRSName };
		} else {
		    rsDependencies = new String[] { dgResourceName };
		}
	    } else {
		rsDependencies = new String [] { dgResourceName };
	    }
	    // set dependency of QFS metadata server resource on the scalable
	    // device group resource and waitZCBootResource if applicable
	    ResourceProperty rp = new ResourcePropertyStringArray(
		Util.RESOURCE_DEPENDENCIES,
		rsDependencies);

	    metaserverSysProps.add(rp);

	    metaResourceInfo.setSysProperties(metaserverSysProps);
	}

	metaResourceInfo.setBringOnline(true);

        // The MetaserverGroup should have a ++affinity with the OBAN diskgroup
        Map metaGroupRGProperties = metaGroupInfo.getRgProperties();
        String currentAffinity = (String)metaGroupRGProperties.get(
                Util.RG_AFFINITY);

	String toAddAffinity = null;
	if (obanDGResourceGroup.getName() != null) {
	    toAddAffinity = Util.PLUS_PLUS + obanDGResourceGroup.getName();
	}

	String toAddSecondAffinity = null;
	if (selZoneCluster != null &&
	    selZoneCluster.trim().length() > 0) {
	    if (waitZCBootRGName != null) {
		toAddSecondAffinity = Util.PLUS_PLUS + waitZCBootRGName;
	    }
	}
	     
        String newAffinity = null;

        if (currentAffinity != null) {
            String splitAffinity[] = currentAffinity.split(Util.COMMA);
            ArrayList splitAffinityList = new ArrayList(Arrays.asList(
                        splitAffinity));

	    if (toAddAffinity != null &&
		!splitAffinityList.contains(toAddAffinity)) {
                splitAffinityList.add(toAddAffinity);
            }

	    if (toAddSecondAffinity != null &&
		!splitAffinityList.contains(toAddSecondAffinity)) {
		splitAffinityList.add(toAddSecondAffinity);
	    }

            String newAffinityArr[] = (String[])splitAffinityList.toArray(
                    new String[0]);
            newAffinity = Util.convertArraytoString(newAffinityArr);
        } else {
	    ArrayList splitAffinityList = new ArrayList();
	    if (toAddAffinity != null) {
		splitAffinityList.add(toAddAffinity);
	    }
	    if (toAddSecondAffinity != null) {
		splitAffinityList.add(toAddSecondAffinity);
	    }
            String newAffinityArr[] = (String[])splitAffinityList.toArray(
                    new String[0]);
            newAffinity = Util.convertArraytoString(newAffinityArr);
        }

	if (newAffinity != null) {
	    metaGroupRGProperties.put(Util.RG_AFFINITY, newAffinity);
	}
        metaGroupInfo.setRgProperties(metaGroupRGProperties);

        // Fill the resource structure
        if (obanDGResourceGroup != null) {

            /*
             * Fill Resource Structure
             */
            ResourceGroupInfo fsGroupInfo = serviceInfo.getResourceGroup(
                    Util.FS_STORAGEGROUP, resourceGroup);

            if (fsGroupInfo == null) {
                fsGroupInfo = storageGroup.copyGroup();
                fsGroupInfo.setName(resourceGroup);
		fsGroupInfo.setZoneClusterName(selZoneCluster);

		// Scalable mount point RG/R runs in the ZC
		rgData = rgGroupMBean.getResourceGroup(
		    resourceGroup, selZoneCluster);

		if (rgData == null) {
                    // Set the properties for the new Scalable RG
                    Map rgProperties = new HashMap();
                    StringBuffer buffer = new StringBuffer();

                    for (int i = 0; i < nodeList.length; i++) {
                        buffer.append((String)nodeList[i]);

                        if (i < (nodeList.length - 1)) {
                            buffer.append(",");
                        }
                    }

                    rgProperties.put(Util.NODELIST, buffer.toString());
                    rgProperties.put(Util.MAX_PRIMARIES,
                        String.valueOf(nodeList.length));
                    rgProperties.put(Util.DESIRED_PRIMARIES,
                        String.valueOf(nodeList.length));
		    rgProperties.put(Util.RG_MODE, Util.SCALABLE_RG_MODE);
                    fsGroupInfo.setRgProperties(rgProperties);
                }

                // Add this group to the serviceInfo
                serviceInfo.getResourceGroupList().add(fsGroupInfo);
                duplicateRGStructures.add(fsGroupInfo);
            }

            Map fsGroupProperties = fsGroupInfo.getRgProperties();
            currentAffinity = (String)fsGroupProperties.get(Util.RG_AFFINITY);
	    if (obanDGResourceGroup.getName() != null) {
		if (selZoneCluster  != null &&
		    selZoneCluster.trim().length() > 0) {
		    // configuring in a ZC
		    // the Scalable mount point RG has a ++RG_AFFINITY on
		    // Scalable device group RG in the base cluster. Append
		    // global: to the scalable device group RG
		    toAddAffinity = Util.PLUS_PLUS + Util.GLOBAL_PREFIX +
			obanDGResourceGroup.getName();
		} else {
		    // if configuring in a base cluster no global: needs to be
		    // appended.
		    toAddAffinity = Util.PLUS_PLUS + obanDGResourceGroup.getName();
		}
	    }

            newAffinity = null;

            if (currentAffinity != null) {
                String splitAffinity[] = currentAffinity.split(Util.COMMA);
                ArrayList splitAffinityList = new ArrayList(Arrays.asList(
                            splitAffinity));

		if (toAddAffinity != null) {
		    if (!splitAffinityList.contains(toAddAffinity)) {
			splitAffinityList.add(toAddAffinity);
		    }
		}

                String newAffinityArr[] = (String[])splitAffinityList.toArray(
                        new String[0]);
                newAffinity = Util.convertArraytoString(newAffinityArr);
            } else {
                newAffinity = toAddAffinity;
            }

	    if (newAffinity != null) {
		fsGroupProperties.put(Util.RG_AFFINITY, newAffinity);
	    }
            fsGroupInfo.setRgProperties(fsGroupProperties);

            ResourceInfo fsResourceInfo = serviceInfo.getResource(fsGroupInfo,
                    Util.SCALABLE_QFS_SVM_RES);

            // If there are multiple S-QFS mount points on a single OBAN
            // Diskgroup, we need to create ResourceInfo objects for each of
            // the S-QFS mountpoints. This case is possible only for S-QFS
            // mountpoints on OBAN. For S-QFS Raw and NAS, resources for
            // each mountpoint is created in a separate ResourceGroup
            if (fsResourceInfo.getResourceName() != null) {

                // This means there is already a Resource configured for a
                // S-QFS mountPoint
                ResourceInfo newFSResourceInfo = fsResourceInfo.copyResource(
                        fsGroupInfo);
                fsResourceInfo = newFSResourceInfo;
            }

            fsResourceInfo.setResourceName(resourceName);
	    fsResourceInfo.setZoneClusterName(selZoneCluster);

	    rData = rGroupMBean.getResourceData(
		resourceName, selZoneCluster);

            // If its a new resource set the Extension Properties
	    if (rData == null) {

                List sysProperties = new ArrayList();
                ResourceProperty rp;

                if (dgResourceName != null) {
		    if (selZoneCluster != null &&
			selZoneCluster.trim().length() > 0) {
			// when configuring in a ZC add the global:
			// to the disk group resource name
			rp = new ResourcePropertyStringArray(
                            Util.RS_DEPEND_OFFLINE_RESTART,
                            new String[] { Util.GLOBAL_PREFIX +
							dgResourceName });
		    } else {
			rp = new ResourcePropertyStringArray(
                            Util.RS_DEPEND_OFFLINE_RESTART,
                            new String[] { dgResourceName });
		    }
                    sysProperties.add(rp);
                }

                fsResourceInfo.setSysProperties(sysProperties);

                List extProperties = new ArrayList();
                rp = new ResourcePropertyString(Util.MOUNTPOINT_DIR,
                        mountPoint);
                extProperties.add(rp);
                rp = new ResourcePropertyString(Util.FILE_SYSTEM_TYPE,
                        Util.SQFS);
                extProperties.add(rp);
                rp = new ResourcePropertyString(Util.TARGET_FILE_SYSTEM,
                        fsName);
                extProperties.add(rp);
                fsResourceInfo.setExtProperties(extProperties);
            }

	    // Set the dependency to the Created Metaserver Resource
	    List dependency = new ArrayList();
	    dependency.add(metaResourceInfo);
	    fsResourceInfo.setDependency(dependency);
	    fsResourceInfo.setBringOnline(true);
        }
    }


    /**
     * Fill the serviceInfo structure for s-qfs on RAW device
     */
    private void fillServiceInfoForQFS_RAW(ResourceGroupInfo storageGroup,
	ResourceGroupInfo metaGroup, String metaserverResource,
        String metaserverGroup, List metaserverNodes,
	String mountPoint, String fsName,
	String resourceGroup, String resourceName,
	String waitZCBootRSName, String waitZCBootRGName,
        String nodeList[], String baseClusterNodeList[], String frameworkRG,
	String selZoneCluster, String zonePath) {

	RGroupMBean rGroupMBean = (RGroupMBean)
	    MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
		onf.getObjectName(RGroupMBean.class, null),
		RGroupMBean.class, false);

	RgGroupMBean rgGroupMBean = (RgGroupMBean)
	    MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
		onf.getObjectName(RgGroupMBean.class, null),
		RgGroupMBean.class, false);

	ResourceData rData = null;
	ResourceGroupData rgData = null;

        /**
         * Fill Group Structure for Metaserver Group
         */
        ResourceGroupInfo metaGroupInfo = serviceInfo.getResourceGroup(
                Util.METASERVER_GROUP, metaserverGroup);

        if (metaGroupInfo == null) {
            metaGroupInfo = metaGroup.copyGroup();

            StringBuffer buffer = new StringBuffer();
            metaGroupInfo.setName(metaserverGroup);

            Map rgProperties = new HashMap();

            for (int i = 0; i < baseClusterNodeList.length; i++) {
                buffer.append(baseClusterNodeList[i]);

                if (i < (baseClusterNodeList.length - 1)) {
                    buffer.append(",");
                }
            }

	    String toAddAffinity = null;
	    if (selZoneCluster != null &&
		selZoneCluster.trim().length() > 0) {
		if (waitZCBootRGName != null) {
		    toAddAffinity = Util.PLUS_PLUS + waitZCBootRGName;
		}
	    }

	    if (toAddAffinity != null) {
		rgProperties.put(Util.RG_AFFINITY, toAddAffinity);
	    }

            rgProperties.put(Util.NODELIST, buffer.toString());
            metaGroupInfo.setRgProperties(rgProperties);
	    // Meta server RGs and RSs are always created in the base cluster
	    metaGroupInfo.setZoneClusterName(null);
            serviceInfo.getResourceGroupList().add(metaGroupInfo);
            duplicateRGStructures.add(metaGroupInfo);
        }

        /*
         * Fill Metaserver Resource Structure
         */
        ResourceInfo metaResourceInfo = serviceInfo.getResource(metaGroupInfo,
	    Util.METASERVER_RESOURCE);

        if (metaResourceInfo.getResourceName() != null) {

            // This means there is already a Resource configured for a
            // S-QFS mountPoint. Create another resource in the same group
            ResourceInfo newMetaResourceInfo = metaResourceInfo.copyResource(
                    metaGroupInfo);
            metaResourceInfo = newMetaResourceInfo;
        }

	if (selZoneCluster != null &&
	    selZoneCluster.trim().length() > 0) {
	    metaResourceInfo.setResourceName(Util.GLOBAL_PREFIX +
						    metaserverResource);
	} else {
	    metaResourceInfo.setResourceName(metaserverResource);
	}

	rData = rGroupMBean.getResourceData(metaserverResource, null);
	metaResourceInfo.setZoneClusterName(null);

	String newMountPoint = mountPoint;
        if (rData == null) {
            List extProperties = new ArrayList();
            ResourceProperty rp = null;
	    if (selZoneCluster != null &&
		selZoneCluster.trim().length() > 0) {
		newMountPoint = zonePath + "/root" + mountPoint;
	    }
	    rp = new ResourcePropertyStringArray(
		Util.QFS_FILE_SYSTEM, new String[] { newMountPoint });

            extProperties.add(rp);
            metaResourceInfo.setExtProperties(extProperties);
        }


	String[] rsDependencies = null;
	if (waitZCBootRSName != null) {
	    rsDependencies = new String[] { waitZCBootRSName };
	}
	// set dependency of QFS metadata server resource
	// waitZCBootRSName if applicable

	ResourceProperty rp1 = new ResourcePropertyStringArray(
	    Util.RESOURCE_DEPENDENCIES,
	    rsDependencies);

	List metaserverSysProps = new ArrayList();
	metaserverSysProps.add(rp1);
	metaResourceInfo.setSysProperties(metaserverSysProps);
        metaResourceInfo.setBringOnline(true);

        // Make sure the mountpoint is added to all the node's vfstab entries
        // and mounted on all the nodes
        Map appConfMap = new HashMap();
        appConfMap.put(Util.NODELIST, metaserverNodes);
        appConfMap.put(Util.MOUNT_POINT, newMountPoint);
        appConfMap.put(Util.FILE_SYSTEM_NAME, fsName);
        appConfMap.put(Util.FILE_SYSTEM_TYPE, Util.SQFS);
        applicationConfiguration(appConfMap);


        /*
         * Fill Group Structure
         */
        ResourceGroupInfo fsGroupInfo = serviceInfo.getResourceGroup(
                Util.FS_STORAGEGROUP, resourceGroup);

        if (fsGroupInfo == null) {
            fsGroupInfo = storageGroup.copyGroup();
            fsGroupInfo.setName(resourceGroup);

	    // the scalable mount point RG/RS runs in the selected zone cluster
	    // or null for base cluster
	    fsGroupInfo.setZoneClusterName(selZoneCluster);

	    rgData = rgGroupMBean.getResourceGroup(
		resourceGroup, selZoneCluster);

	    if (rgData == null) {

                // Set the properties for the new Scalable RG
                Map rgProperties = new HashMap();
                StringBuffer buffer = new StringBuffer();

                for (int i = 0; i < nodeList.length; i++) {
                    buffer.append((String) nodeList[i]);

                    if (i < (nodeList.length - 1)) {
                        buffer.append(",");
                    }
                }

                rgProperties.put(Util.NODELIST, buffer.toString());
                rgProperties.put(Util.MAX_PRIMARIES,
                    String.valueOf(nodeList.length));
                rgProperties.put(Util.DESIRED_PRIMARIES,
                    String.valueOf(nodeList.length));
		rgProperties.put(Util.RG_MODE, Util.SCALABLE_RG_MODE);
                fsGroupInfo.setRgProperties(rgProperties);
            }

            // Add this group to the serviceInfo
            serviceInfo.getResourceGroupList().add(fsGroupInfo);
            duplicateRGStructures.add(fsGroupInfo);
        }

        /*
         * Fill Resource Structure
         */
        ResourceInfo fsResourceInfo = serviceInfo.getResource(fsGroupInfo,
                Util.SCALABLE_QFS_RES);

        // If there are multiple S-QFS mount points,
        // we need to create ResourceInfo objects for each of
        // the S-QFS mountpoints.
        if (fsResourceInfo.getResourceName() != null) {

            // This means there is already a Resource configured for a
            // S-QFS mountPoint
            ResourceInfo newFSResourceInfo = fsResourceInfo.copyResource(
                    fsGroupInfo);
            fsResourceInfo = newFSResourceInfo;
        }

        fsResourceInfo.setResourceName(resourceName);
	// For s-QFS mount points, RS is in the ZC
	fsResourceInfo.setZoneClusterName(selZoneCluster);
	rData = rGroupMBean.getResourceData(resourceName, selZoneCluster);

        // If its a new resource set the Extension Properties
	if (rData == null) {
            List extProperties = new ArrayList();
            ResourceProperty rp = new ResourcePropertyString(
		Util.MOUNTPOINT_DIR, mountPoint);
            extProperties.add(rp);
            rp = new ResourcePropertyString(Util.FILE_SYSTEM_TYPE, Util.SQFS);
            extProperties.add(rp);
            rp = new ResourcePropertyString(Util.TARGET_FILE_SYSTEM, fsName);
            extProperties.add(rp);
            fsResourceInfo.setExtProperties(extProperties);
        }

        // Set the dependency to the Created Metaserver Resource
	List dependency1 = new ArrayList();
	dependency1.add(metaResourceInfo);
	fsResourceInfo.setDependency(dependency1);
        fsResourceInfo.setBringOnline(true);
    }


    /**
     * Fills the serviceInfo structure for Disk Storage Resources
     *
     * @param  Map  containing Storage Resource Informatio
     * @param  Set  containing DiskResources
     * @param  ResourceGroupInfo  group to be used as template
     * @param String frameworkRG framework RG in cluster
     * @param String frameworkRGInBC framework RG in base cluster if configuring
     *        on a ZC else null
     * @param  String selZoneCluster containing the zone cluster name or
     *		      null for the base cluster
     */
    private void fillServiceInfoForDisk(Map storageMap, String diskResources[],
        ResourceGroupInfo storageGroup, String frameworkRG,
	String frameworkRGInBC, String selZoneCluster) {

	RGroupMBean rGroupMBean = (RGroupMBean)
	    MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
		onf.getObjectName(RGroupMBean.class, null),
		RGroupMBean.class, false);

	RgGroupMBean rgGroupMBean = (RgGroupMBean)
	    MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
		onf.getObjectName(RgGroupMBean.class, null),
		RgGroupMBean.class, false);

	ResourceData rData = null;
	ResourceGroupData rgData = null;

        /*
         * Command Generation Steps for DiskResource
         *
         * 1. Get the diskResources List
         * 2. For every diskResource
         *  2(a) Get the resourceInformation (Group,Type)
         *  2(b) Check if a ResourceGroupInfo Object with that name
         *       exists in the serviceInfo object
         *  2(b) If no Create a new Group Object with the Group's Name
         *  2(c) If(DiskGroupType is SVM)
         *          Set the ScalableSVM Resource for the group
         *          if the resource is new
         *              Set the ResourceProperties for that Resource
         *  2(d) If(DiskGroupType is VxVM)
         *         Set the ScalableVxVM Resource for the group
         *         if the resource is new
         *             Set the ResourceProperties for that Resource
         */
        String nodeList[] = (String[]) storageMap.get(Util.NODELIST);

	String baseClusterNodeList[] = (String[])storageMap.get(
	    Util.BASE_CLUSTER_NODELIST);

        for (int i = 0; i < diskResources.length; i++) {

            // Diskgroup Resource Information
            String diskResource = diskResources[i];

	    // Check to see if RG should be created in the base cluster or the
	    // ZC split up the diskResource
	    String[] splitStr = diskResource.split(Util.COLON);
	    String zoneClusterName = null;
	    String rsName = null;
	    if (splitStr.length == 2) {
		zoneClusterName = splitStr[Util.ZC_INDEX];
		rsName = splitStr[Util.R_INDEX];
	    } else {
		rsName = diskResource;
	    }

            Map diskResourceMap = (HashMap)storageMap.get(diskResource);

            String resourceGroup = (String)diskResourceMap.get(
		Util.STORAGEGROUP);
            String diskGroupType = (String)diskResourceMap.get(
		Util.DISKGROUP_TYPE);
            String diskGroupName = (String)diskResourceMap.get(
		Util.DISKGROUP_NAME);
	    String logicalDevList = (String)diskResourceMap.get(
		Util.LOGICAL_DEVLIST);

            /*
             * Fill Group Structure
             */
            ResourceGroupInfo diskGroupInfo = serviceInfo.getResourceGroup(
                    Util.DG_STORAGEGROUP, resourceGroup);

            if (diskGroupInfo == null) {
                diskGroupInfo = storageGroup.copyGroup();
                diskGroupInfo.setName(resourceGroup);

		// if the zone cluster name is set disk group RG is created in
		// the ZC else in the base cluster.
		diskGroupInfo.setZoneClusterName(zoneClusterName);
		rgData = rgGroupMBean.getResourceGroup(resourceGroup,
		    zoneClusterName);

		if (rgData == null) {
                    // Set the properties for the new Scalable RG
                    Map rgProperties = new HashMap();
                    StringBuffer buffer = new StringBuffer();
		    String[] list = null;

		    if (zoneClusterName == null) {
			// use the base cluster node list
			list = baseClusterNodeList;
		    } else {
			list = nodeList;
		    }

                    for (int j = 0; j < list.length; j++) {
                        buffer.append((String)list[j]);

                        if (j < (list.length - 1)) {
                            buffer.append(",");
                        }
                    }

                    rgProperties.put(Util.NODELIST, buffer.toString());
                    rgProperties.put(Util.MAX_PRIMARIES,
                        String.valueOf(list.length));
                    rgProperties.put(Util.DESIRED_PRIMARIES,
                        String.valueOf(list.length));

		    // check to see which framework RG to have a dependency on
		    if (selZoneCluster != null &&
			selZoneCluster.trim().length() > 0) {
			// configuring in a ZC

			if (zoneClusterName != null &&
			    zoneClusterName.trim().length() > 0) {
			    // scalable device group being configured in a
			    // zone cluster. Either SVM or CVM being configured.
			    rgProperties.put(Util.RG_AFFINITY,
				Util.PLUS_PLUS + Util.GLOBAL_PREFIX + 
				frameworkRGInBC);
			} else {
			    // scalable device group being configured in a
			    //  base cluster. s-QFS with SVM.
			    rgProperties.put(Util.RG_AFFINITY,
				Util.PLUS_PLUS + frameworkRGInBC);
			}
		    } else {
			// configuring in a base cluster

			rgProperties.put(Util.RG_AFFINITY,
			    Util.PLUS_PLUS + frameworkRG);
		    }
		    rgProperties.put(Util.RG_MODE, Util.SCALABLE_RG_MODE);
		    diskGroupInfo.setRgProperties(rgProperties);
                }

                // Add this group to the serviceInfo
                serviceInfo.getResourceGroupList().add(diskGroupInfo);
                duplicateRGStructures.add(diskGroupInfo);
            }

            /*
             * Fill Resource Structure
             */
            // GDD Resource Managing Oban DiskGroup
            if (diskGroupType.equals(Util.SVMDEVICEGROUP)) {
                ResourceInfo diskResourceInfo = serviceInfo.getResource(
		    diskGroupInfo, Util.SCALABLE_SVM_RES);

                // If there are multiple
                // Diskgroup, we need to create ResourceInfo objects for each of
                // them.
                if (diskResourceInfo.getResourceName() != null) {

                    // This means there is already a Resource configured for a
                    // SVM disk group
                    ResourceInfo newDiskResourceInfo = diskResourceInfo
                        .copyResource(diskGroupInfo);
                    diskResourceInfo = newDiskResourceInfo;
                }

                diskResourceInfo.setResourceName(rsName);
		diskResourceInfo.setZoneClusterName(zoneClusterName);
		rData = rGroupMBean.getResourceData(diskResource,
			    zoneClusterName);

                // If its a new resource set the Extension Properties
                if (rData == null) {
                    List extProperties = new ArrayList();
                    ResourceProperty rp = new ResourcePropertyString(
                            Util.DISKGROUP_NAME, diskGroupName);
                    extProperties.add(rp);

		    if (logicalDevList != null &&
			!logicalDevList.equals(Util.ALL)) {
			// if set to ALL, no need to set the logical devlist.
			// implies all logical devices in the ZC.
			String[] logicalDevArray = (String[])
			    logicalDevList.split(Util.COMMA);
			ResourcePropertyStringArray rpArray =
			    new ResourcePropertyStringArray(
				Util.LOGICAL_DEVLIST,  logicalDevArray);
			extProperties.add(rpArray);
		    }
		    diskResourceInfo.setExtProperties(extProperties);
                }

                diskResourceInfo.setBringOnline(true);
            }

            // GDD Resource Managing VxVM DiskGroup
            if (diskGroupType.equals(Util.VxVMDEVICEGROUP)) {
                ResourceInfo diskResourceInfo = serviceInfo.getResource(
                        diskGroupInfo, Util.SCALABLE_CVM_RES);

                // If there are multiple
                // Diskgroup, we need to create ResourceInfo objects for each of
                // them.
                if (diskResourceInfo.getResourceName() != null) {

                    // This means there is already a Resource configured for a
                    // VxVM disk group
                    ResourceInfo newDiskResourceInfo = diskResourceInfo
                        .copyResource(diskGroupInfo);
                    diskResourceInfo = newDiskResourceInfo;
                }

                diskResourceInfo.setResourceName(diskResource);
		diskResourceInfo.setZoneClusterName(zoneClusterName);

		rData = rGroupMBean.getResourceData(diskResource,
						    zoneClusterName);

                // If its a new resource set the Extension Properties
                if (rData == null) {
                    List extProperties = new ArrayList();
                    ResourceProperty rp = new ResourcePropertyString(
                            Util.DISKGROUP_NAME, diskGroupName);
                    extProperties.add(rp);

		    if (logicalDevList != null &&
			!logicalDevList.equals(Util.ALL)) {
			String[] logicalDevArray = (String[])
			    logicalDevList.split(Util.COMMA);
			ResourcePropertyStringArray rpArray =
			    new ResourcePropertyStringArray(
				Util.LOGICAL_DEVLIST, logicalDevArray);
			extProperties.add(rpArray);
		    }
		    diskResourceInfo.setExtProperties(extProperties);
                }

                diskResourceInfo.setBringOnline(true);
            }
        }
    }

    /**
     * Set the MBeanServer Connections for this MBean instance depending on the
     * Nodelist
     */
    private void createMBeanServerConnections(String nodeList[]) {

        // first get the local connection
        try {
            HashMap env = new HashMap();
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                this.getClass().getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());
            this.localConnector = TrustedMBeanModel.getWellKnownConnector(Util
                    .getClusterEndpoint(), env);
            this.mbsConnections = new MBeanServerConnection[nodeList.length];
            this.connectors = new JMXConnector[nodeList.length];

            for (int i = 0; i < nodeList.length; i++) {
                String split_str[] = nodeList[i].split(Util.COLON);
                String nodeEndPoint = Util.getNodeEndpoint(localConnector,
                        split_str[0]);
                connectors[i] = TrustedMBeanModel.getWellKnownConnector(
                        nodeEndPoint, env);

                // Now get a reference to the mbean server connection
                this.mbsConnections[i] = connectors[i]
                    .getMBeanServerConnection();
            }
        } catch (IOException ioe) {
	    ioe.printStackTrace();
        } catch (SecurityException se) {
	    se.printStackTrace();
        }
    }

    /**
     * Get the mountpoint Dir for a QFS filesystem/ NAS Exported filesystem by
     * parsing the VFSTAB File. The VFSTAB file is parsed on all the nodes of
     * the nodelist. The method returns on finding an entry for the qfs or nas
     * filesystem on any node. If no entry is found on any node the method
     * returns null.
     *
     * @param  qfsFileSystem  (or) NAS Exported Filesystem
     */
    private String getMountPointForFS(String fileSystem, String nodeList[]) {
        FileAccessorWrapper fileWrapper = null;
        createMBeanServerConnections(nodeList);

        try {
            fileWrapper = new FileAccessorWrapper(Util.VFSTAB_FILE,
                    mbsConnections);

            for (int i = 0; i < nodeList.length; i++) {
                List contents = (ArrayList)fileWrapper.readFully(
                        mbsConnections[i]);

                for (Iterator iter = contents.iterator(); iter.hasNext();) {
                    VfsStruct vfsEntry = (VfsStruct) iter.next();

                    if (vfsEntry.getVfs_special().equals(fileSystem)) {
                        return vfsEntry.getVfs_mountp();
                    }
                }
            }

            return null;
        } catch (Exception e) {
	    e.printStackTrace();
            return null;
        } finally {
            closeConnections(nodeList);
        }
    }

    /**
     * Lists all the Shared QFS filesystems by parsing the mcf file in the node
     *
     * @return  List of s-qfs filesystems, if no mcf file is found returns an
     * empty list
     */
    private List getSharedQFSFileSystems() {

        List qfsFSNames = new ArrayList();

        // The mcf file is parsed for the qfs name
        // then we check if its "shared" and "on"
        File mcfFile = new File("/etc/opt/SUNWsamfs/mcf");

        if (!mcfFile.exists() || !mcfFile.canRead()) {

            // Return an empty list if the file doesnt exists on the node
            return qfsFSNames;
        }

        try {
            FileReader mcfFileReader = new FileReader("/etc/opt/SUNWsamfs/mcf");
            BufferedReader fileInput = new BufferedReader(mcfFileReader);
            String text;

            // The mcf File is delimited by whitespaces, so we split each line
            // into the corressponding fields and get the values
            while ((text = fileInput.readLine()) != null) {

                if ((text.indexOf("#") != 0) && (text.trim().length() > 1)) {
                    String fields[] = text.trim().split("[ \t]+");

                    // Filter only filesystem definitions
                    if (fields[0].indexOf("/") != 0) {
                        String qfsName = fields[0];

                        if (fields[5].equalsIgnoreCase("shared")) {
                            qfsFSNames.add(qfsName);
                        }
                    }
                }
            }
        } catch (Exception e) {

            // Unable to read mcf file in this node, return empty list
	    e.printStackTrace();
            return qfsFSNames;
        }

        return qfsFSNames;
    }

    /**
     * Gets the possible nodelists for metaserver resourcegroup for a qfs
     * filesystem
     *
     * @param  qfsName  Name of the QFS Filesystem
     *
     * @return  List of Metaservernodes
     */
    private List getMetaserverNodeList(String qfsName) {
        List hostNames = new ArrayList();

        // The qfs configuration file for the particular
        // qfs filesystem is parsed
        File hostsFile = new File("/etc/opt/SUNWsamfs/hosts." + qfsName);

        if (!hostsFile.exists() || !hostsFile.canRead()) {
            return hostNames;
        }

        try {
            FileReader hostFileReader = new FileReader(
                    "/etc/opt/SUNWsamfs/hosts." + qfsName);
            BufferedReader fileInput = new BufferedReader(hostFileReader);
            String text;

            while ((text = fileInput.readLine()) != null) {

                if ((text.indexOf("#") != 0) && (text.trim().length() > 1)) {
                    String fields[] = text.trim().split("[ \t]+");

                    if (fields[0].indexOf("/") != 0) {
                        hostNames.add(fields[0]);
                    }
                }
            }
        } catch (Exception e) {

            // Unable to find/read mcf file in this node, return empty list
	    e.printStackTrace();
        }

        return hostNames;
    }

    /**
     * Gets the disk group entry on which qfs filesystem is created.
     */
    private String getDiskGroupEntryForQFS(String qfsName) {

        String diskGroup = null;
        List obanDisks = new ArrayList();

        // The mcf file is parsed for the qfs name
        // then we check if its "shared" and "on"
        File mcfFile = new File("/etc/opt/SUNWsamfs/mcf");

        if (!mcfFile.exists() || !mcfFile.canRead()) {
            return null;
        }

        try {
            FileReader mcfFileReader = new FileReader("/etc/opt/SUNWsamfs/mcf");
            BufferedReader fileInput = new BufferedReader(mcfFileReader);
            String text;

            // The mcf File is delimited by whitespaces, so we split each line
            // into
            // the corressponding fields and get the values
            while ((text = fileInput.readLine()) != null) {

                if ((text.indexOf("#") != 0) && (text.trim().length() > 1)) {
                    String fields[] = text.trim().split("[ \t]+");

                    if (fields[0].indexOf(SVM_HDR) == 0) {
                        if (fields[3].equalsIgnoreCase(qfsName)) {
                            obanDisks.add(fields[0]);
                        }
                    }
                }
            }
        } catch (Exception e) {

            // Unable to find/read mcf file in this node, return empty list
	    e.printStackTrace();
        }

        if (obanDisks.size() > 0) {
            diskGroup = (String)obanDisks.get(0);
        }

        return diskGroup;
    }

    /**
     * Gets the OBAN Diskgroup on which qfs filesystem is created Returns "null"
     * if qfs is created on raw disk.
     */
    private String getObanDG_forQFS(String qfsName) {
        String obanDG = null;
        List obanDisks = new ArrayList();

        // The mcf file is parsed for the qfs name
        // then we check if its "shared" and "on"
        File mcfFile = new File("/etc/opt/SUNWsamfs/mcf");

        if (!mcfFile.exists() || !mcfFile.canRead()) {
            return null;
        }

        try {
            FileReader mcfFileReader = new FileReader("/etc/opt/SUNWsamfs/mcf");
            BufferedReader fileInput = new BufferedReader(mcfFileReader);
            String text;

            // The mcf File is delimited by whitespaces, so we split each line
            // into
            // the corressponding fields and get the values
            while ((text = fileInput.readLine()) != null) {
                if ((text.indexOf("#") != 0) && (text.trim().length() > 1)) {
                    String fields[] = text.trim().split("[ \t]+");

                    if (fields[0].indexOf(SVM_HDR) == 0) {

                        if (fields[3].equalsIgnoreCase(qfsName)) {
                            obanDisks.add(fields[0]);
                        }
                    }
                }
            }
        } catch (Exception e) {

            // Unable to find/read mcf file in this node, return empty list
	    e.printStackTrace();
        }

        // The code should be changed to handle QFS on multiple OBAN volumes
        // currently that is not supported
        if (obanDisks.size() > 0) {
            String disk = (String)obanDisks.get(0);
            obanDG = (disk.split("/"))[3];
        }

        return obanDG;
    }

    /**
     * isDisk
     *
     * @param  path  to the device or filesystem
     *
     * @return  Returns true if the given path is a device else false
     */
    public native boolean isDisk(String path);

    /**
     * getDCSServiceName
     *
     * @param  devPath
     *
     * @return  Returns string containing the DCS device group name
     */
    public native String getDCSServiceName(String devPath);
}
