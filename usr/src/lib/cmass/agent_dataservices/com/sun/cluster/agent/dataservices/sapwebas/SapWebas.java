/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SapWebas.java 1.22     08/12/03 SMI"
 */


package com.sun.cluster.agent.dataservices.sapwebas;

// J2SE
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

// JMX
import javax.management.AttributeValueExp;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;

// CACAO
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// CMAS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.*;
import com.sun.cluster.agent.dataservices.utils.*;
import com.sun.cluster.agent.devicegroup.DeviceGroupMBean;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.agent.rgm.ResourceTypeMBean;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.common.TrustedMBeanModel;


/**
 * This interface defines the management operations available to configure a
 * SapWebas Resource
 */
public class SapWebas implements SapWebasMBean {

    private static String CS_RG_PREFIX = "enq_scs_";
    private static String REP_PREFIX = "rep_";
    private static String SCS_PREFIX = "scs_";
    private static String ENQ_PREFIX = "enq_";

    /* The MBeanServer in which this MBean is inserted. */
    private MBeanServer mBeanServer;

    /* The object name factory used to register this MBean */
    private ObjectNameFactory onf = null;

    /* Command Generation Structure for this Dataservice */
    private DataServiceInfo serviceInfo = null;

    /* Command Generation Structure for this Dataservice */
    private ServiceConfig sapConfiguration = null;

    private MBeanServerConnection mbsConnections[];
    private JMXConnector connectors[] = null;
    private JMXConnector localConnector = null;

    /* Command Generation Structure for this Dataservice */
    private List commandList = null;
    private int configurationStatus = Util.NOTCONFIGURED;

    /**
     * Default constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this MBean.
     */
    public SapWebas(MBeanServer mBeanServer, ObjectNameFactory onf,
        DataServiceInfo xServiceInfo) {

        this.mBeanServer = mBeanServer;
        this.onf = onf;
        this.serviceInfo = xServiceInfo;

        if (serviceInfo != null) {
            this.sapConfiguration = serviceInfo.getConfigurationClass(this
                    .getClass().getClassLoader());
        }
    }

    /**
     * Returns list of executed command list
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public List getCommandList() {
        return this.commandList;
    }

    /*
     * Returns the current configuration status of the Application
     * -1 - Unconfigured
     * 0 - Successfully Configured
     *  1 - Failed Configuration but successfully rollbacked
     *  2 - Failed Configuration and failed to rollback
     */
    public int getConfigurationStatus() {
        return this.configurationStatus;
    }

    /**
     * Returns the ServiceInfo Structure for this DataserviceMBean
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public DataServiceInfo getServiceInfo() {
        return this.serviceInfo;
    }

    /**
     * Sets the ServiceInfo Structure for this DataserviceMBean
     *
     * @param xServiceInfo  Command Generation Structure
     */
    public void setServiceInfo(DataServiceInfo xServiceInfo) {
        this.serviceInfo = xServiceInfo;

        if (serviceInfo != null) {
            this.sapConfiguration = serviceInfo.getConfigurationClass(this
                    .getClass().getClassLoader());
        }
    }

    /**
     * Generates the set of commands for configuring this service, given the
     * configuration information. This command uses the ServiceInfo structure,
     * fills the same and generates the command list.
     *
     * @param  configurationInformation  ConfigurationInformation for
     * configuring an instance of the Dataservice
     *
     * @return  Commands Generated for the given ConfigurationInformation
     */
    public List generateCommands(Map configurationInformation)
        throws CommandExecutionException {
        ResourceInfo scsRes = null;
        ResourceInfo repRes = null;
        ResourceInfo enqRes = null;
        ResourceInfo csStorageRes = null;
        ResourceInfo caLhRes = null;
        ResourceInfo repStorageRes = null;
        ResourceInfo repLhRes = null;
        Map confMap = new HashMap();
        HashMap csVfsTabEntries = null;
        HashMap fsMntMap = null;

        serviceInfo.reset();

        // Central Services Resource Group
        String repRGName = (String) configurationInformation.get(
                Util.REP_RG_NAME);
        String csRGName = (String) configurationInformation.get(
                Util.CS_RG_NAME);
        confMap.put(Util.CS_GRP + Util.NAME_TAG, csRGName);

        HashMap rgProps = new HashMap();
        String nodeList[] = (String[]) configurationInformation.get(
                Util.RG_NODELIST);
        StringBuffer buffer;
        rgProps.put(Util.NODELIST, Util.convertArraytoString(nodeList));
        confMap.put(Util.CS_GRP + Util.RGPROP_TAG, rgProps);

        // Enq Resource
        confMap.put(Util.ENQ_RS + Util.NAME_TAG,
            configurationInformation.get(Util.ENQ_RES_NAME));

        List enqExtProps = new ArrayList();
        List stdList = new ArrayList();
        ResourceProperty rp;
        rp = new ResourcePropertyString(Util.SAP_USER,
                (String) configurationInformation.get(Util.SAP_USER));
        enqExtProps.add(rp);
        rp = new ResourcePropertyString(Util.ENQ_I_NO,
                (String) configurationInformation.get(Util.ENQ_INSTANCE_NUM));
        enqExtProps.add(rp);
        rp = new ResourcePropertyString(Util.ENQ_LOC,
                (String) configurationInformation.get(Util.ENQ_SERVER_LOC));
        enqExtProps.add(rp);
        rp = new ResourcePropertyString(Util.ENQ_PROF,
                (String) configurationInformation.get(
                    Util.ENQ_SERVER_PROFILE_LOC));
        enqExtProps.add(rp);
        confMap.put(Util.ENQ_RS + Util.EXTPROP_TAG, enqExtProps);
        enqRes = serviceInfo.getResource(Util.ENQ_RS);

        if (enqRes != null) {
            enqRes.setBringOnline(true);
        }

        // SCS Resource
        confMap.put(Util.SCS_RS + Util.NAME_TAG,
            configurationInformation.get(Util.SCS_RES_NAME));

        List extProps = new ArrayList();
        rp = new ResourcePropertyString(Util.SID,
                (String) configurationInformation.get(Util.SAP_SID));
        extProps.add(rp);
        rp = new ResourcePropertyString(Util.SAP_USER,
                (String) configurationInformation.get(Util.SAP_USER));
        extProps.add(rp);
        rp = new ResourcePropertyString(Util.SCS_I_NO,
                (String) configurationInformation.get(Util.SCS_INSTANCE_NUM));
        extProps.add(rp);
        rp = new ResourcePropertyString(Util.SCS_NAME,
                (String) configurationInformation.get(Util.SCS_INSTANCE_NAME));
        extProps.add(rp);
        confMap.put(Util.SCS_RS + Util.EXTPROP_TAG, extProps);
        scsRes = serviceInfo.getResource(Util.SCS_RS);

        if (scsRes != null) {
            scsRes.setBringOnline(true);
        }

        String selDbResName = (String) configurationInformation.get(
                Util.SELECTED_DB);

        // Existing storage Resources
        String selHasp = (String) configurationInformation.get(
                Util.CS_SELECTED_STORAGE);
        String selHaspArr[] = null;
        stdList = new ArrayList();

        ArrayList scsStdList = new ArrayList();
        if (selHasp != null) {
            selHaspArr = selHasp.split(Util.COMMA);

            StringBuffer dependentResources = null;

            for (int i = 0; i < selHaspArr.length; i++) {

                if ((selHaspArr[i] != null) &&
                        (selHaspArr[i].trim().length() > 0)) {

                    if (dependentResources == null) {
                        dependentResources = new StringBuffer();
                        dependentResources.append(selHaspArr[i]);
                    } else {
                        dependentResources.append(Util.COMMA);
                        dependentResources.append(selHaspArr[i]);
                    }
                }
            }

            if (dependentResources != null) {
                ResourcePropertyString dependProp = new ResourcePropertyString(
                        Util.RESOURCE_DEPENDENCIES,
                        dependentResources.toString());
                stdList.add(dependProp);
                scsStdList.add(dependProp);
            }
        }

        if (selDbResName != null) {
            ResourcePropertyString dbDependency = new ResourcePropertyString(
                    Util.RESOURCE_DEPENDENCIES, selDbResName);
            stdList.add(dbDependency);
            scsStdList.add(dbDependency);
        }

        confMap.put(Util.SCS_RS + Util.SYSPROP_TAG, scsStdList);
        confMap.put(Util.ENQ_RS + Util.SYSPROP_TAG, stdList);

        // New Hasp Resources
        String newFS[] = (String[]) configurationInformation.get(
                Util.CS_SELECTED_FS);

        String newGD[] = (String[]) configurationInformation.get(
                Util.CS_SELECTED_GD);

        if ((newFS != null) || (newGD != null)) {
            extProps = new ArrayList();
            stdList = new ArrayList();
            String fstype = null;

            if (newGD == null) {
                fsMntMap = DataServicesUtil.getFsMountMap(
                    mBeanServer, nodeList);
            }
            if (fsMntMap != null && newFS != null) {
                String fsEntry = newFS[0];
                int commaInd = 0;
                if ((commaInd = newFS[0].indexOf(Util.COMMA)) != -1) {
                    // contain multiple filesystem mount directories
                    fsEntry = newFS[0].substring(0, commaInd);
                }
                fstype = (String)fsMntMap.get(fsEntry);
            }

            if (fstype != null && fstype.equals(Util.FSTYPE_SAMFS)) {
                // it's a QFS resource
                csStorageRes = serviceInfo.getResource(Util.QFS_RES_ID);
                rp = new ResourcePropertyStringArray(
                    Util.QFS_FILESYSTEM, newFS);
                extProps.add(rp);
            } else { // HASP resource
                csStorageRes = serviceInfo.getResource(Util.CS_HASP_RS);
                if ((newFS != null) && (newFS.length > 0)) {
                    rp = new ResourcePropertyStringArray(
                        Util.HASP_ALL_FILESYSTEMS,
                        newFS);
                    extProps.add(rp);
                }
                if ((newGD != null) && (newGD.length > 0)) {
                    rp = new ResourcePropertyStringArray(
                        Util.HASP_ALL_DEVICES,
                        newGD);
                    extProps.add(rp);
                }
                rp = new ResourcePropertyBoolean(Util.AFFINITY_ON,
                    Boolean.FALSE);
                extProps.add(rp);
            }

            String resName = (String) configurationInformation.get(
                    Util.CS_STORAGE_NAME);
            csStorageRes.setResourceName(resName);
            csStorageRes.setExtProperties(extProps);
            csStorageRes.setSysProperties(stdList);
            csStorageRes.setBringOnline(true);

            // Get the map to add for /etc/vfstab and update all nodes
            HashMap fsHashMap = (HashMap) configurationInformation.get(
                    Util.CS_VFSTAB_ENTRIES);

            if (fsHashMap != null) {
                HashMap map_to_add = (HashMap) fsHashMap.get(
                        Util.VFSTAB_MAP_TO_ADD);
                csVfsTabEntries = map_to_add;

                if (map_to_add != null) {
                    createMBeanServerConnections(nodeList);

                    FileAccessorWrapper fileAccessor = new FileAccessorWrapper(
                            Util.VFSTAB_FILE, this.mbsConnections);

                    for (int i = 0; i < nodeList.length; i++) {
                        String split_str[] = nodeList[i].split(Util.COLON);
                        ArrayList nodeEntry = (ArrayList) map_to_add.get(
                                split_str[0]);

                        if (nodeEntry != null) {

                            // each element of arraylist is a vfstab entry
                            fileAccessor.appendLine(nodeEntry,
                                this.mbsConnections[i]);
                        }
                    }

                    closeConnections(nodeList);
                }
            }
        }

        // Existing LH Resources
        String selLH = (String) configurationInformation.get(Util.SELECTED_LH);
        String selLHArr[] = selLH.split(Util.COMMA);
        stdList = null;
        caLhRes = serviceInfo.getResource(Util.CS_LH_RS);

        for (int i = 0; i < selLHArr.length; i++) {

            if ((selLHArr[i] != null) && (selLHArr[i].trim().length() > 0)) {
                caLhRes.setResourceName(selLHArr[i]);
                extProps = new ArrayList();
                stdList = new ArrayList();
                caLhRes.setExtProperties(extProps);
                caLhRes.setSysProperties(stdList);
                caLhRes.setBringOnline(true);
                caLhRes = caLhRes.copyResource();
            }
        }

        String hostNameList[] = (String[]) configurationInformation.get(
                Util.CS_NEW_HOSTS);
        String rsName;
        String netifList[];

        if (hostNameList != null) {

            // There is atleast one new LH resource to be created
            rsName = (String) configurationInformation.get(Util.CS_LH_NAME);
            caLhRes.setResourceName(rsName);

            // Resource Properties
            extProps = new ArrayList();
            stdList = new ArrayList();

            if ((hostNameList != null) && (hostNameList.length > 0)) {
                rp = new ResourcePropertyStringArray(Util.HOSTNAMELIST,
                        hostNameList);
                extProps.add(rp);
            }

            netifList = (String[]) configurationInformation.get(
                    Util.CS_NETIFLIST);

            if ((netifList != null) && (netifList.length > 0)) {
                rp = new ResourcePropertyStringArray(Util.NETIFLIST, netifList);
                extProps.add(rp);
            }

            // Resource Extension Properties
            caLhRes.setExtProperties(extProps);
            caLhRes.setSysProperties(stdList);
            caLhRes.setBringOnline(true);
        } else {
            caLhRes.removeResource();
        }

        // Replica Server Resource group
        if (repRGName != null) {
            confMap.put(Util.REP_GRP + Util.NAME_TAG, repRGName);

            HashMap reprgProps = new HashMap();
            nodeList = (String[]) configurationInformation.get(
                    Util.RG_NODELIST);
            reprgProps.put(Util.NODELIST, Util.convertArraytoString(nodeList));
            buffer = new StringBuffer();
            buffer.append(Util.NEGATIVE);
            buffer.append(Util.NEGATIVE);
            buffer.append(csRGName);
            reprgProps.put(Util.RG_AFFINITY, buffer.toString());
            confMap.put(Util.REP_GRP + Util.RGPROP_TAG, reprgProps);

            // Replica Resource
            confMap.put(Util.REP_RS + Util.NAME_TAG,
                configurationInformation.get(Util.REP_RES_NAME));
            extProps = new ArrayList();
            rp = new ResourcePropertyString(Util.SAP_USER,
                    (String) configurationInformation.get(Util.SAP_USER));
            extProps.add(rp);
            rp = new ResourcePropertyString(Util.REP_LOC,
                    (String) configurationInformation.get(Util.REP_SERVER_LOC));
            extProps.add(rp);
            rp = new ResourcePropertyString(Util.REP_PROF,
                    (String) configurationInformation.get(
                        Util.REP_SERVER_PROFILE_LOC));
            extProps.add(rp);
            confMap.put(Util.REP_RS + Util.EXTPROP_TAG, extProps);
            repRes = serviceInfo.getResource(Util.REP_RS);

            if (repRes != null) {
                repRes.setBringOnline(true);
            }

            // Existing storagr Resources
            selHasp = (String) configurationInformation.get(
                    Util.REP_SELECTED_STORAGE);

            stdList = new ArrayList();
            if (selHasp != null) {
            
                selHaspArr = selHasp.split(Util.COMMA);
                StringBuffer dependentResources = null;

                for (int i = 0; i < selHaspArr.length; i++) {

                    if ((selHaspArr[i] != null) &&
                            (selHaspArr[i].trim().length() > 0)) {

                        if (dependentResources == null) {
                            dependentResources = new StringBuffer();
                            dependentResources.append(selHaspArr[i]);
                        } else {
                            dependentResources.append(Util.COMMA);
                            dependentResources.append(selHaspArr[i]);
                        }
                    }
                }

                if (dependentResources != null) {
                    ResourcePropertyString dependProp =
                        new ResourcePropertyString(Util.RESOURCE_DEPENDENCIES,
                            dependentResources.toString());
                    stdList.add(dependProp);
                }
            }
            confMap.put(Util.REP_RS + Util.SYSPROP_TAG, stdList);

            // New Hasp Resources
            newFS = (String[]) configurationInformation.get(
                    Util.REP_SELECTED_FS);
            newGD = (String[]) configurationInformation.get(
                    Util.REP_SELECTED_GD);

            if ((newFS != null) || (newGD != null)) {
                extProps = new ArrayList();
                stdList = new ArrayList();
                String fstype = null;

                if (newGD == null && fsMntMap == null) {
                    fsMntMap =  DataServicesUtil.getFsMountMap(
                        mBeanServer, nodeList);
                } 
                if (fsMntMap != null && newFS != null) {
                    String fsEntry = newFS[0];
                    int commaInd = 0;
                    if ((commaInd = newFS[0].indexOf(Util.COMMA)) != -1) {
                        // contain multiple filesystem mount directories
                        fsEntry = newFS[0].substring(0, commaInd);
                    }
                    fstype = (String)fsMntMap.get(fsEntry);
                }

                if (fstype != null && fstype.equals(Util.FSTYPE_SAMFS)) {
                    // it's a QFS resource
                    repStorageRes = serviceInfo.getResource(Util.REP_QFS_RS);
                    rp = new ResourcePropertyStringArray(
                        Util.QFS_FILESYSTEM, newFS);
                    extProps.add(rp);
                } else { // HASP resource
                    repStorageRes = serviceInfo.getResource(Util.REP_HASP_RS);
                    if ((newFS != null) && (newFS.length > 0)) {
                        rp = new ResourcePropertyStringArray(
                            Util.HASP_ALL_FILESYSTEMS,
                            newFS);
                        extProps.add(rp);
                    }
                    if ((newGD != null) && (newGD.length > 0)) {
                        rp = new ResourcePropertyStringArray(
                            Util.HASP_ALL_DEVICES,
                            newGD);
                        extProps.add(rp);
                    }
                    rp = new ResourcePropertyBoolean(Util.AFFINITY_ON,
                        Boolean.FALSE);
                    extProps.add(rp);
                }

                String resName = (String) configurationInformation.get(
                        Util.REP_STORAGE_NAME);
                repStorageRes.setResourceName(resName);
                repStorageRes.setExtProperties(extProps);
                repStorageRes.setSysProperties(stdList);
                repStorageRes.setBringOnline(true);

                // Get the map to add for /etc/vfstab and update all nodes
                HashMap fsHashMap = (HashMap) configurationInformation.get(
                        Util.REP_VFSTAB_ENTRIES);

                if (fsHashMap != null) {
                    HashMap map_to_add = (HashMap) fsHashMap.get(
                            Util.VFSTAB_MAP_TO_ADD);

                    if (map_to_add != null) {
                        createMBeanServerConnections(nodeList);

                        FileAccessorWrapper fileAccessor =
                            new FileAccessorWrapper(Util.VFSTAB_FILE,
                                this.mbsConnections);

                        for (int i = 0; i < nodeList.length; i++) {
                            String split_str[] = nodeList[i].split(Util.COLON);
                            ArrayList nodeEntry = (ArrayList) map_to_add.get(
                                    split_str[0]);

                            if (nodeEntry != null) {

                                // each element of arraylist is a vfstab entry
                                if (csVfsTabEntries != null) {
                                    ArrayList csnodeEntry = (ArrayList)
                                        csVfsTabEntries.get(split_str[0]);

                                    if ((csnodeEntry != null) &&
                                            csnodeEntry.equals(nodeEntry)) {
                                        continue;
                                    }
                                }

                                fileAccessor.appendLine(nodeEntry,
                                    this.mbsConnections[i]);
                            }
                        }

                        closeConnections(nodeList);
                    }
                }
            }


            // Existing LH Resources
            selLH = (String) configurationInformation.get(Util.REP_SELECTED_LH);
            selLHArr = selLH.split(Util.COMMA);
            stdList = null;
            caLhRes = serviceInfo.getResource(Util.REP_LH_RS);

            for (int i = 0; i < selLHArr.length; i++) {

                if ((selLHArr[i] != null) &&
                        (selLHArr[i].trim().length() > 0)) {
                    caLhRes.setResourceName(selLHArr[i]);
                    extProps = new ArrayList();
                    stdList = new ArrayList();
                    caLhRes.setExtProperties(extProps);
                    caLhRes.setSysProperties(stdList);
                    caLhRes.setBringOnline(true);
                    caLhRes = caLhRes.copyResource();
                }
            }

            hostNameList = (String[]) configurationInformation.get(
                    Util.REP_NEW_HOSTS);

            if (hostNameList != null) {

                // There is atleast one new LH resource to be created
                rsName = (String) configurationInformation.get(
                        Util.REP_LH_NAME);
                caLhRes.setResourceName(rsName);
                extProps = new ArrayList();
                stdList = new ArrayList();

                if ((hostNameList != null) && (hostNameList.length > 0)) {
                    rp = new ResourcePropertyStringArray(Util.HOSTNAMELIST,
                            hostNameList);
                    extProps.add(rp);
                }

                netifList = (String[]) configurationInformation.get(
                        Util.REP_NETIFLIST);

                if ((netifList != null) && (netifList.length > 0)) {
                    rp = new ResourcePropertyStringArray(Util.NETIFLIST,
                            netifList);
                    extProps.add(rp);
                }

                caLhRes.setExtProperties(extProps);
                caLhRes.setSysProperties(stdList);
                caLhRes.setBringOnline(true);
            } else {
                caLhRes.removeResource();
            }

        }

        boolean bSuccess = true;
        commandList = null;

        try {

            // Fill the ServiceInfo object with the configurationInformation
            serviceInfo.fillServiceInfoStructure(confMap);

            // Generate CommandSet for this Service
            serviceInfo.generateCommandSet(mBeanServer, onf);
            commandList = serviceInfo.getCommandSet();

        } catch (Exception e) {
            commandList = serviceInfo.getCommandSet();

            if (e instanceof CommandExecutionException) {
                bSuccess = false;

                ExitStatus status[] = ((CommandExecutionException) e)
                    .getExitStatusArray();
                String errStr = null;

                if ((status != null) && (status.length > 0)) {
                    List errList = status[0].getErrStrings();
                    errStr = "";

                    if (errList != null) {
                        Iterator iter = errList.iterator();

                        while (iter.hasNext()) {
                            errStr = errStr + iter.next() + "\n";
                        }
                    }
                }

                commandList.add(errStr);

                // Unconfigure the Groups and Resources
                try {

                    // add empty string to the command list - this would be used
                    // as a marker to demarcate configuration and rollback
                    // commands
                    commandList.add("");
                    serviceInfo.rollBack(mBeanServer, onf);
                    commandList.addAll(serviceInfo.getCommandSet());
                    configurationStatus = Util.FAILED_ROLLBACKED;
                    throw (CommandExecutionException) e;
                } catch (Exception re) {
                    configurationStatus = Util.ROLLBACK_FAILED;
                    re.printStackTrace();
                }

                throw (CommandExecutionException) e;
            } else {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }

        // Set RG Affinities
        confMap = new HashMap();
        confMap.put(Util.CS_GRP + Util.NAME_TAG, csRGName);
        rgProps = new HashMap();

        if ((repRGName != null) && bSuccess) {
            buffer = new StringBuffer();
            buffer.append(Util.POSITIVE);
            buffer.append(repRGName);
            rgProps.put(Util.RG_AFFINITY, buffer.toString());
            confMap.put(Util.CS_GRP + Util.RGPROP_TAG, rgProps);

            try {
                serviceInfo.reset();
                serviceInfo.fillServiceInfoStructure(confMap);

                serviceInfo.generateCommandSet(mBeanServer, onf);
                commandList.addAll(serviceInfo.getCommandSet());
                configurationStatus = Util.CONFIGURED;
            } catch (Exception e) {

                if (e instanceof CommandExecutionException) {
                    commandList.addAll(serviceInfo.getCommandSet());
                    configurationStatus = Util.FAILED_NOTROLLBACKED;

                    ExitStatus status[] = ((CommandExecutionException) e)
                        .getExitStatusArray();
                    String errStr = null;

                    if ((status != null) && (status.length > 0)) {

                        List errList = status[0].getErrStrings();
                        errStr = "";

                        if (errList != null) {
                            Iterator iter = errList.iterator();

                            while (iter.hasNext()) {
                                errStr = errStr + iter.next() + "\n";
                            }
                        }
                    }

                    commandList.add(errStr);
                    throw (CommandExecutionException) e;
                } else {
                    // Other exceptions in setting affinities can be ignored
                    System.out.println("Exception in Setting Affinities:" +
                        e.getMessage());
                }
            }
        }

        return commandList;
    }

    private void createMBeanServerConnections(String nodeList[]) {

        // first get the local connection
        try {
            HashMap env = new HashMap();
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                this.getClass().getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());
            this.localConnector = TrustedMBeanModel.getWellKnownConnector(Util
                    .getClusterEndpoint(), env);
            this.mbsConnections = new MBeanServerConnection[nodeList.length];
            this.connectors = new JMXConnector[nodeList.length];

            for (int i = 0; i < nodeList.length; i++) {
                String split_str[] = nodeList[i].split(Util.COLON);
                String nodeEndPoint = Util.getNodeEndpoint(localConnector,
                        split_str[0]);

                connectors[i] = TrustedMBeanModel.getWellKnownConnector(
                        nodeEndPoint, env);

                // Now get a reference to the mbean server connection
                this.mbsConnections[i] = connectors[i]
                    .getMBeanServerConnection();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SecurityException se) {
            se.printStackTrace();
            throw se;
        }
    }

    public void closeConnections(String nodeList[]) {

        if (nodeList != null) {

            for (int i = 0; i < nodeList.length; i++) {

                try {

                    if (connectors[i] != null) {
                        connectors[i].close();
                    }
                } catch (IOException ioe) {
                }
            }
        }

        if (localConnector != null) {

            try {
                localConnector.close();
            } catch (IOException ioe) {
            }
        }
    }

    // Implementation For the ServiceConfig Interface Methods

    public String[] getDiscoverableProperties() {
        String discoverableProps[];
        ServiceConfig sapDS = getConfigObject();
        discoverableProps = sapDS.getDiscoverableProperties();

        return discoverableProps;
    }

    public String[] getAllProperties() {
        String properties[];
        ServiceConfig sapDS = getConfigObject();
        properties = sapDS.getAllProperties();

        return properties;
    }

    public HashMap discoverPossibilities(String propertyNames[],
        Object helperData) {
        HashMap resultMap = new HashMap();
        List propList = Arrays.asList(propertyNames);

        if (propList.contains(Util.STORAGE_RGLIST)) {
            String extPropNames[] = new String[] {
                    Util.HASP_ALL_FILESYSTEMS, 
                    Util.HASP_ALL_DEVICES,
                    Util.QFS_FILESYSTEM
                };

            QueryExp query = Query.or(Query.match(new AttributeValueExp("Type"),
                Query.value(Util.HASP_RTNAME + "*")),
                Query.match(new AttributeValueExp("Type"),
                Query.value(Util.QFS_RTNAME + "*")));

            ArrayList resources = new ArrayList();
            List rset = null;

            try {
                rset = TrustedMBeanModel.getMBeanProxies(Util
                        .getClusterEndpoint(), ResourceMBean.class, false,
                        query);

                if (rset != null) {

                    for (int i = 0; i < rset.size(); i++) {
                        ResourceMBean resBean = (ResourceMBean) rset.get(i);
                        String rsName = resBean.getName();
                        HashMap map = new HashMap();
                        map.put(Util.RG_NAME, resBean.getResourceGroupName());
                        map.put(Util.RS_NAME, rsName);

                        for (int j = 0; j < extPropNames.length; j++) {
                            Object obj = DataServicesUtil
                                .getResourcePropertyValue(mBeanServer, rsName,
                                    extPropNames[j]);
                            map.put(extPropNames[j], obj);
                        }

                        resources.add(map);
                    }
                }
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            } catch (SecurityException see) {
                System.out.println(see.getMessage());
            }

            resultMap.put(Util.STORAGE_RGLIST, resources);
        }

        if (propList.contains(Util.NODELIST)) {
            resultMap.put(Util.NODELIST,
                DataServicesUtil.getNodeList(mBeanServer));
        }

        if (propList.contains(Util.DB_RSLIST)) {
            QueryExp query = Query.or(Query.match(new AttributeValueExp("Type"),
                        Query.value(Util.ORADB_RTNAME + "*")),
                    Query.match(new AttributeValueExp("Type"),
                        Query.value(Util.SAPDB_RTNAME + "*")));

            List resources = new ArrayList();
            List rset = null;

            try {
                rset = TrustedMBeanModel.getMBeanProxies(Util
                        .getClusterEndpoint(), ResourceMBean.class, false,
                        query);

                if (rset != null) {

                    for (int i = 0; i < rset.size(); i++) {
                        ResourceMBean resBean = (ResourceMBean) rset.get(i);
                        resources.add(resBean.getName() + Util.COLON +
                            resBean.getResourceGroupName());
                    }
                }
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            } catch (SecurityException see) {
                System.out.println(see.getMessage());
            }

            resultMap.put(Util.DB_RSLIST, resources);
        }

        if (propList.contains(Util.LH_RSLIST)) {
            String nodeList[] = (String[]) ((HashMap) helperData).get(
                    Util.RG_NODELIST);

            // get a list of Logical Host resources
            // with nodelist set to nodeList
            List resources = null;
            String rgName = (String) ((HashMap) helperData).get(Util.RG_NAME);

            if (rgName != null) {

                // get all logical host resources in resource group rgName
                QueryExp query = Query.and(Query.match(
                            new AttributeValueExp("Type"),
                            Query.value(Util.LH_RTNAME + "*")),
                        Query.eq(new AttributeValueExp("ResourceGroupName"),
                            Query.value(rgName)));

                Set rset = null;

                try {
                    rset = TrustedMBeanModel.getInstanceNames(Util
                            .getClusterEndpoint(), ResourceMBean.class, query);
                } catch (IOException ioe) {
                    System.out.println(ioe.getMessage());
                } catch (SecurityException see) {
                    System.out.println(see.getMessage());
                }

                if (rset != null) {

                    if (resources == null) {
                        resources = new ArrayList();

                        Iterator i = rset.iterator();

                        while (i.hasNext()) {
                            resources.add((String) i.next() + Util.COLON +
                                rgName);
                        }
                    }
                }
            } else {
                resources = DataServicesUtil.getResourceList(mBeanServer,
                        nodeList, Util.LH_RTNAME);
            }

            resultMap.put(Util.LH_RSLIST, resources);
        }

        if (propList.contains(Util.SAP_CS_RG_NAME)) {
            String enq_i_no = (String) ((HashMap) helperData).get(
                    Util.ENQ_I_NO);
            StringBuffer rg_prefix = new StringBuffer(CS_RG_PREFIX);
            rg_prefix.append(enq_i_no);
            resultMap.put(Util.SAP_CS_RG_NAME, getRGName(rg_prefix.toString()));
        }

        if (propList.contains(Util.SAP_REP_RG_NAME)) {
            String enq_i_no = (String) ((HashMap) helperData).get(
                    Util.ENQ_I_NO);
            StringBuffer rg_prefix = new StringBuffer(REP_PREFIX);
            rg_prefix.append(enq_i_no);
            resultMap.put(Util.SAP_REP_RG_NAME,
                getRGName(rg_prefix.toString()));
        }

        if (propList.contains(Util.ENQ_R_NAME)) {
            String enq_i_no = (String) ((HashMap) helperData).get(
                    Util.ENQ_I_NO);
            StringBuffer rg_prefix = new StringBuffer(ENQ_PREFIX);
            rg_prefix.append(enq_i_no);
            resultMap.put(Util.ENQ_R_NAME,
                getResourceName(rg_prefix.toString()));
        }

        if (propList.contains(Util.SCS_R_NAME)) {
            String enq_i_no = (String) ((HashMap) helperData).get(
                    Util.SCS_I_NO);
            StringBuffer rg_prefix = new StringBuffer(SCS_PREFIX);
            rg_prefix.append(enq_i_no);
            resultMap.put(Util.SCS_R_NAME,
                getResourceName(rg_prefix.toString()));
        }

        if (propList.contains(Util.REP_R_NAME)) {
            String enq_i_no = (String) ((HashMap) helperData).get(
                    Util.ENQ_I_NO);
            StringBuffer rg_prefix = new StringBuffer(REP_PREFIX);
            rg_prefix.append(enq_i_no);
            resultMap.put(Util.REP_R_NAME,
                getResourceName(rg_prefix.toString()));
        }

        if (propList.contains(Util.ZONE_ENABLE)) {
            ServiceConfig serviceConfigDS = getConfigObject();
            HashMap retMapDS = (HashMap) serviceConfigDS.discoverPossibilities(
                    new String[] { Util.ZONE_ENABLE }, null);
            resultMap.put(Util.ZONE_ENABLE, retMapDS.get(Util.ZONE_ENABLE));
        }

        ServiceConfig sapDS = getConfigObject();
        HashMap retMap = sapDS.discoverPossibilities(propertyNames, helperData);

        if (retMap != null) {
            resultMap.putAll(retMap);
        }

        return resultMap;
    }

    public HashMap discoverPossibilities(String nodeList[],
        String propertyNames[], Object helperData) {
        return discoverPossibilities(propertyNames, helperData);
    }

    public ErrorValue validateInput(String propNames[], HashMap userInputs,
        Object helperData) {
        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        List propList = Arrays.asList(propNames);
        boolean ret;
        String value;

        if (propList.contains(Util.SAP_CS_RG_NAME)) {
            value = (String) userInputs.get(Util.SAP_CS_RG_NAME);
            ret = DataServicesUtil.isRGNameUsed(value);

            if (ret) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append(value);
                errStr.append(" is already in use");
            }
        }

        if (propList.contains(Util.SAP_REP_RG_NAME)) {
            value = (String) userInputs.get(Util.SAP_REP_RG_NAME);
            ret = DataServicesUtil.isRGNameUsed(value);

            if (ret) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append(Util.NEWLINE);
                errStr.append(value);
                errStr.append(" is already in use");
            }
        }

        if (propList.contains(Util.ENQ_R_NAME)) {
            value = (String) userInputs.get(Util.ENQ_R_NAME);
            ret = DataServicesUtil.isResourceNameUsed(value);

            if (ret) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append(Util.NEWLINE);
                errStr.append(value);
                errStr.append(" is already in use");
            }
        }

        if (propList.contains(Util.SCS_R_NAME)) {
            value = (String) userInputs.get(Util.SCS_R_NAME);
            ret = DataServicesUtil.isResourceNameUsed(value);

            if (ret) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append(Util.NEWLINE);
                errStr.append(value);
                errStr.append(" is already in use");
            }
        }

        if (propList.contains(Util.REP_R_NAME)) {
            value = (String) userInputs.get(Util.REP_R_NAME);
            ret = DataServicesUtil.isResourceNameUsed(value);

            if (ret) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append(Util.NEWLINE);
                errStr.append(value);
                errStr.append(" is already in use");
            }
        }

        ServiceConfig sapDS = getConfigObject();
        ErrorValue errfromDS = sapDS.validateInput(propNames, userInputs,
                helperData);

        if (!errfromDS.getReturnValue().booleanValue()) {
            err.setReturnVal(Boolean.FALSE);
            errStr.append(Util.NEWLINE);
            errStr.append(errfromDS.getErrorString());
        }

        err.setErrorString(errStr.toString());

        return err;
    }

    public ErrorValue applicationConfiguration(Object helperData) {
        return null;

    }

    public String[] aggregateValues(String propertyName, Map unfilteredValues,
        Object helperData) {
        return null;
    }

    private ServiceConfig getConfigObject() {

        if (sapConfiguration == null) {
            sapConfiguration = serviceInfo.getConfigurationClass(this.getClass()
                    .getClassLoader());
        }

        return sapConfiguration;
    }

    private String getResourceName(String prefix) {
        StringBuffer rg_prefix = new StringBuffer(prefix);
        StringBuffer cs_rgname;
        String cs_rgnamestr;
        int count = 0;

        do {
            cs_rgname = rg_prefix;

            if (count > 0) {
                cs_rgname.append(Util.UNDERSCORE);
                cs_rgname.append(count);
            }

            cs_rgname.append(Util.RS_SUFFIX);
            count++;
            cs_rgnamestr = cs_rgname.toString();
        } while (DataServicesUtil.isResourceNameUsed(cs_rgnamestr));

        return cs_rgnamestr;
    }

    private String getRGName(String prefix) {
        StringBuffer rg_prefix = new StringBuffer(prefix);
        StringBuffer cs_rgname;
        String cs_rgnamestr;
        int count = 0;

        do {
            cs_rgname = rg_prefix;

            if (count > 0) {
                cs_rgname.append(Util.UNDERSCORE);
                cs_rgname.append(count);
            }

            cs_rgname.append(Util.RG_SUFFIX);
            count++;
            cs_rgnamestr = cs_rgname.toString();
        } while (DataServicesUtil.isRGNameUsed(cs_rgnamestr));

        return cs_rgnamestr;
    }
}
