/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)DiskGroupInfo.java	1.5	08/07/23 SMI"
 */


package com.sun.cluster.agent.dataservices.utils;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

/**
 * The DiskGroupInfo class wraps the disk group information.
 */
public class DiskGroupInfo implements java.io.Serializable {

    // Name of the disk group
    private String diskGroupName = null;

    // Map containing the volumes available to a cluster for SVM
    // Stored as volume name and resource name key value pair
    private HashMap availSVMVolMap = null; 

    // Map containing the volumes available to a cluster for QFS_SVM
    // Stored as volume name and resource name key value pair
    private HashMap availQfsSVMVolMap = null;
 
    // Map containing the volumes not available to a cluster
    private HashMap notAvailVolMap = null;

    // Name of the primary node for the disk group
    private String primaryNodeName = null;

    // Map containing the resource name and a list of volumes in the map
    // One map for the device groups in the zone cluster 
    private HashMap rsSVMMap = null;

    // One map for the device groups in the base cluster 
    private HashMap rsQfsSVMMap = null;

    private String dgType = null;

    private ClusterDiskGroupInfo clusterDiskGroupInfo = null;

    /**
     * Creates a new instance of DiskGroupInfo
     */
    public DiskGroupInfo() {
    }

    /**
     * Creates a new instance of DiskGroupInfo 
     */
    public DiskGroupInfo(ClusterDiskGroupInfo clusterDiskGroupInfo) {
	this.clusterDiskGroupInfo = clusterDiskGroupInfo;
    }

    /**
     * Creates a new instance of DiskGroupInfo based on the values passed
     */
    public DiskGroupInfo(String diskGroupName, HashMap availSVMVolMap,
	HashMap availQfsSVMVolMap, HashMap notAvailVolMap,
	String primaryNodeName, HashMap rsSVMMap, HashMap rsQfsSVMMap) {
	this.diskGroupName = diskGroupName;
	this.availSVMVolMap = availSVMVolMap;
	this.availQfsSVMVolMap = availQfsSVMVolMap;
	this.notAvailVolMap = notAvailVolMap;
	this.primaryNodeName = primaryNodeName;
	this.rsSVMMap = rsSVMMap;
	this.rsQfsSVMMap = rsQfsSVMMap;
	this.dgType = dgType;
    }

    /**
     * Getter methods
     */
    public String getDiskGroupName() {
	return this.diskGroupName;
    }

    public HashMap getAvailSVMVolMap() {
	return this.availSVMVolMap;
    }

    public HashMap getAvailQfsSVMVolMap() {
	return this.availQfsSVMVolMap;
    }

    public HashMap getNotAvailVolMap() {
	return this.notAvailVolMap;
    }

    public String getPrimaryNodeName() {
	return this.primaryNodeName;
    }

    public ClusterDiskGroupInfo getClusterDiskGroupInfo() {
	return this.clusterDiskGroupInfo;
    }

    public HashMap getSVMResourceMap() {
	return this.rsSVMMap;
    }
 
    public HashMap getQfsSVMResourceMap() {
	return this.rsQfsSVMMap;
    }
 
    public String getDiskGroupType() {
	return this.dgType;
    }

    public void setDiskGroupName(String diskGroupName) {
	this.diskGroupName = diskGroupName;
    }

    public void setAvailSVMVolMap(HashMap availSVMVolMap) {
	this.availSVMVolMap = availSVMVolMap;
    }

    public void setAvailQfsSVMVolMap(HashMap availQfsSVMVolMap) {
	this.availQfsSVMVolMap = availQfsSVMVolMap;
    }

    public void setNotAvailVolMap(HashMap notAvailVolMap) {
	this.notAvailVolMap = notAvailVolMap;
    }

    public void setPrimaryNodeName(String primaryNodeName) {
	this.primaryNodeName = primaryNodeName;
    }

    public void setClusterDiskGroupInfo(ClusterDiskGroupInfo clusterDiskGroupInfo) {
	this.clusterDiskGroupInfo = clusterDiskGroupInfo;
    }

    public void setSVMResourceMap(HashMap rsSVMMap) {
	this.rsSVMMap = rsSVMMap;
    }

    public void setQfsSVMResourceMap(HashMap rsQfsSVMMap) {
	this.rsQfsSVMMap = rsQfsSVMMap;
    }

    public void setDiskGroupType(String dgType) {
	this.dgType = dgType;
    }

    /**
     * toString()
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n\t\t");
        buffer.append("diskGroupName     :" + diskGroupName + "\n\t\t");
        buffer.append("availSVMVolMap    :" + availSVMVolMap + "\n\t\t");
        buffer.append("availQfsSVMVolMap :" + availQfsSVMVolMap + "\n\t\t");
        buffer.append("notAvailVolMap    :" + notAvailVolMap + "\n\t\t");
        buffer.append("primaryNodeName   :" + primaryNodeName + "\n\t\t");
        buffer.append("resourceSVMMap    :" + rsSVMMap + "\n\t\t");
        buffer.append("resourceQfsSVMMap :" + rsQfsSVMMap + "\n\t\t");
        buffer.append("dgType            :" + dgType + "\n\t\t");

        return buffer.toString();
    }

    /**
     * This method is to clear the DiskGroupInfo structure
     * generate the commands
     */
    public void clear() {

        // Reset all instance specific information
        this.diskGroupName = null;
        this.availSVMVolMap = null;
	this.availQfsSVMVolMap = null;
        this.notAvailVolMap = null;
        this.primaryNodeName = null;
	this.rsSVMMap = null;
	this.rsQfsSVMMap = null;
	this.dgType = null;
    }
}
