/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAStoragePlus.java 1.41     09/04/22 SMI"
 */


package com.sun.cluster.agent.dataservices.hastorageplus;

// J2SE
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;

// JMX
import javax.management.AttributeValueExp;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;

// CACAO
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// CMAS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.*;
import com.sun.cluster.agent.dataservices.utils.*;
import com.sun.cluster.agent.devicegroup.DeviceGroupMBean;
import com.sun.cluster.agent.rgm.ResourceMBean;
import com.sun.cluster.agent.rgm.ResourceTypeMBean;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.common.TrustedMBeanModel;

/**
 * This interface defines the management operations available to configure a
 * HAStoragePlus Resource
 */
public class HAStoragePlus implements HAStoragePlusMBean {
    private static final String FSTYP = "/usr/sbin/fstyp";
    private static final String FSTYP_ERROR = "Unknown_fstyp";

    private static String SVC_DEV_GLB_HDR = "/dev/global";
    private static String SVC_DEV_MD_HDR = "/dev/md";
    private static String SVC_DEV_VX_HDR = "/dev/vx";

    private static String SCSWITCH = "/usr/cluster/bin/scswitch";
    private static String STR_HASP = "hasp";

    /* RTName of LogicalHost MBean */
    private static final String HASP_RT = "SUNW.HAStoragePlus";
    private static final String QFS_RT = "SUNW.qfs";

    private static Logger logger = Logger.getLogger(Util.DOMAIN);

    /* The MBeanServer in which this MBean is inserted. */
    private MBeanServer mBeanServer;

    /* The object name factory used to register this MBean */
    private ObjectNameFactory onf = null;

    /* Command Generation Structure for this Dataservice */
    private DataServiceInfo serviceInfo = null;

    private MBeanServerConnection mbsConnections[];
    private JMXConnector connectors[] = null;
    private JMXConnector localConnector = null;
    private int DEV_PATH_INDEX = 3;

    /* List of commands executed */
    private List commandList = null;

    /**
     * Default constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this MBean.
     */
    public HAStoragePlus(MBeanServer mBeanServer, ObjectNameFactory onf,
        DataServiceInfo serviceInfo) {

        this.mBeanServer = mBeanServer;
        this.onf = onf;
        this.serviceInfo = serviceInfo;
    }


    /**
     * Returns the ServiceInfo Structure for this DataserviceMBean
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public DataServiceInfo getServiceInfo() {
        return this.serviceInfo;
    }

    /**
     * Sets the ServiceInfo Structure for this DataserviceMBean
     *
     * @param  serviceInfo  Command Generation Structure
     */
    public void setServiceInfo(DataServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    /**
     * Returns list of executed command list
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public List getCommandList() {
        return this.commandList;
    }

    /**
     * Generates the set of commands for configuring this service, given the
     * configuration information. This command uses the ServiceInfo structure,
     * fills the same and generates the command list.
     *
     * @param  configurationInformation  ConfigurationInformation for
     * configuring an instance of the Dataservice
     *
     * @return  Commands Generated for the given ConfigurationInformation
     */
    public List generateCommands(Map configurationInformation)
        throws CommandExecutionException {

        try {

            // Fill the ServiceInfo object with the configurationInformation
            serviceInfo.fillServiceInfoStructure(configurationInformation);

            // The Resource(s) should be brought online after command
            // Generation. We get the handler for this resource and
            // set bringOnline to true
            ResourceInfo resourceInfo;
            
            if (configurationInformation.containsKey(
                    Util.QFS_RES_ID + Util.NAME_TAG)) {
                resourceInfo = serviceInfo.getResource(Util.QFS_RES_ID);
            } else {
                resourceInfo = serviceInfo.getResource(Util.HASP_RES_ID);
            }
            resourceInfo.setBringOnline(true);

            // Generate CommandSet for this Service
            serviceInfo.generateCommandSet(mBeanServer, onf);
            commandList = serviceInfo.getCommandSet();

        } catch (Exception e) {

            // Unconfigure the Groups and Resources
            try {
                commandList = serviceInfo.getCommandSet();

                // add Rollback Success string to the command list - this
                // would be used as a marker to demarcate configuration and
                // rollback commands
                commandList.add(Util.ROLLBACK_SUCCESS);
                serviceInfo.rollBack(mBeanServer, onf);
                commandList.addAll(serviceInfo.getCommandSet());
            } catch (Exception re) {

                // Rollback Failed
                // add Rollback Fail string in place of Rollback Success string
                // to the command list - this would be used as a marker to
                // demarcate configuration and rollback commands
                commandList.remove(Util.ROLLBACK_SUCCESS);
                commandList.add(Util.ROLLBACK_FAIL);
                commandList.addAll(serviceInfo.getCommandSet());

            }

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException) e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        } finally {

            // Always reset the structure to the original form
            serviceInfo.reset();
        }

        return commandList;
    }


    // Implementation For the ServiceConfig Interface Methods

    public String[] getDiscoverableProperties() {

        // FileSystems and Device groups can be discovered at this point
        // we should perhaps get this list from the data services layer.
        return new String[] {};
    }

    public String[] getAllProperties() {

        // Retrieve Properties From the RTR File.
        String properties[] = DataServicesUtil.getExtensionProperties(
                mBeanServer, HASP_RT);

        return properties;
    }

    public HashMap discoverPossibilities(String propertyNames[],
        Object helperData) {

        HashMap resultMap = new HashMap();
        List propList = Arrays.asList(propertyNames);

        ResourceProperty property = null;

        // Discover PropertyNames : This is called while Initializing the
        // WizardContext Model
        if (propList.contains(Util.PROPERTY_NAMES)) {

            // Get the discoverableProperty Values
            String discoverableProps[] = getDiscoverableProperties();

            for (int i = 0; i < discoverableProps.length; i++) {
                String propertyName[] = { discoverableProps[i] };
                resultMap.putAll(discoverPossibilities(propertyName, null));
            }

            // Add other propertyNames to the resultMap
            String allProps[] = getAllProperties();

            if (allProps != null) {

                for (int i = 0; i < allProps.length; i++) {
                    String propertyName = allProps[i];

                    // Omit the Discovered Properties
                    if (!resultMap.containsKey(propertyName)) {

                        // Get the default values for these properties if any
                        property = DataServicesUtil.getRTRProperty(mBeanServer,
                                HASP_RT, propertyName);
                        resultMap.put(propertyName, property);
                    }
                }
            }
        }

        if (propList.contains(Util.NODELIST)) {
            resultMap.put(Util.NODELIST,
                DataServicesUtil.getNodeList(mBeanServer));
        }

        if (propList.contains(Util.HASP_ALL_DEVICES)) {
            resultMap.put(Util.HASP_ALL_DEVICES,
                getGlobalDevicePaths((HashMap) helperData));
        }

        if (propList.contains(Util.HASP_ALL_FILESYSTEMS)) {
            resultMap.put(Util.HASP_ALL_FILESYSTEMS,
                getFilesystemMountPoints((HashMap) helperData));
        }

        if (propList.contains(Util.HASP_FSTYPE)) {

            // get device path from the helper data
            resultMap.put(Util.HASP_FSTYPE, getFSType((String) helperData));
        }

        if (propList.contains(Util.ZONE_ENABLE)) {

            // Hasp support local zones.
            // Return false to disable local zones support.
            resultMap.put(Util.ZONE_ENABLE, new Boolean(true));
        }

        if (propList.contains(Util.RG_NAME)) {
            String fsSelections[] = (String[]) ((HashMap) helperData).get(
                    Util.HASP_ALL_FILESYSTEMS);
            String devSelections[] = (String[]) ((HashMap) helperData).get(
                    Util.HASP_ALL_DEVICES);
            String strToUse =
                ((fsSelections == null) || (fsSelections.length == 0))
                ? devSelections[0] : fsSelections[0];

            StringBuffer rg_prefix = new StringBuffer(getName(strToUse));
            StringBuffer rgname;
            String rgnamestr;
            int count = 0;

            do {
                rgname = rg_prefix;

                if (count > 0) {
                    rgname.append(Util.UNDERSCORE);
                    rgname.append(count);
                }

                rgname.append(Util.RG_POSTFIX);
                count++;
                rgnamestr = rgname.toString();
            } while (DataServicesUtil.isRGNameUsed(mBeanServer, rgnamestr));

            resultMap.put(Util.RG_NAME, rgnamestr);
        }

        if (propList.contains(Util.RS_NAME)) {
            String fsSelections[] = (String[]) ((HashMap) helperData).get(
                    Util.HASP_ALL_FILESYSTEMS);
            String devSelections[] = (String[]) ((HashMap) helperData).get(
                    Util.HASP_ALL_DEVICES);
            ArrayList resExceptions = (ArrayList) ((HashMap) helperData).get(
                    Util.EXEMPT);
            String strToUse =
                ((fsSelections == null) || (fsSelections.length == 0))
                ? devSelections[0] : fsSelections[0];

            StringBuffer rs_prefix = new StringBuffer(getName(strToUse));
            StringBuffer rsname;
            String rsnamestr;
            int count = 0;

            do {
                rsname = rs_prefix;

                if (count > 0) {
                    rsname.append(Util.UNDERSCORE);
                    rsname.append(count);
                }

                rsname.append(Util.RS_POSTFIX);
                count++;
                rsnamestr = rsname.toString();
            } while (
                DataServicesUtil.isResourceNameUsed(mBeanServer, rsnamestr) ||
                ((resExceptions != null) && resExceptions.contains(rsnamestr)));

            resultMap.put(Util.RS_NAME, rsnamestr);
        }

        return resultMap;
    }

    private String getName(String name) {

        // get rid of all starting slashes
        while (name.startsWith(Util.SLASH)) {
            name = name.substring(name.indexOf(Util.SLASH) + 1);
        }

        // get rid of all ending slashes
        while (name.endsWith(Util.SLASH)) {
            name = name.substring(0, name.lastIndexOf(Util.SLASH));
        }

        name = name.replaceAll(Util.SLASH, Util.UNDERSCORE);

        // Names cannot start with a number
        String firstLetter = name.substring(0, 1);

        if (firstLetter.matches("\\d")) {
            name = STR_HASP.concat(name);
        }

        name = DataServicesUtil.removeSpecialChars(name);

        return name;
    }

    public HashMap discoverPossibilities(String nodeList[],
        String propertyNames[], Object helperData) {
        return null;
    }

    public HashMap obtainMountpointFSTypes(String nodes[]) {

        /* 
         * The returned hash contains available filesystem
         * mountpoints(as key) and their fstype(as value)
         */ 
        HashMap mntTypeMap = new HashMap(); // return HashMap

        HashMap helpData = new HashMap();
        helpData.put(Util.RG_NODELIST, nodes);
        HashMap fsMntMap = getFilesystemMountPoints(helpData);

        VfsStruct vfstabEntries[] = (VfsStruct[]) fsMntMap.get(
            Util.VFSTAB_ENTRIES);
        if (vfstabEntries != null) {
            for (int i = 0; i < vfstabEntries.length; i++) {
                mntTypeMap.put(vfstabEntries[i].getVfs_mountp(),
                    vfstabEntries[i].getVfs_fstype());
            }
        }
        return mntTypeMap;
    }

    private String getFSType(String devPath) {
        String fstype = null;

        // Run  fstyp to figure out the fstype
        String cmd[][] = {
                { FSTYP, devPath }
            };

        // run the command
        InvocationStatus exits[] = null;

        try {
            exits = InvokeCommand.execute(cmd, null);
        } catch (InvocationException ie) {
            return null;
        }

        ExitStatus es[] = ExitStatus.createArray(exits);

        for (int i = 0; i < es.length; i++) {

            if (es[i] == null) {
                break;
            }

            List out = es[i].getOutStrings();

            if (out.size() > 0) {

                for (int j = 0; j < out.size(); j++) {
                    fstype = (String) out.get(j);

                    if (fstype.indexOf(FSTYP_ERROR) != -1) {

                        // the error string is present in fstype
                        fstype = null;
                    }

                }
            }
        }

        return fstype;
    }


    /*
     * Checks if the device nodelist is a sub-set of the RG nodelist
     * for failover configurations.
     * For scalable configurations, this checks if there is a devicegroup
     * mBean for device
     */
    private boolean canBePrimaried(String devGroup, String nodelist[],
        boolean isScalable) {

        if ((devGroup == null) || (nodelist == null)) {
            return false;
        }

        devGroup = devGroup.trim();

        ArrayList rgNodeList = null;

        for (int i = 0; i < nodelist.length; i++) {

            if (rgNodeList == null) {
                rgNodeList = new ArrayList();
            }

            String nodeSplit[] = nodelist[i].split(Util.COLON);

            if (!rgNodeList.contains(nodeSplit[0])) {
                rgNodeList.add(nodeSplit[0]);
            }
        }

        // query the DeviceGroupMBean for exact device group Mbean
        QueryExp queryExp = Query.eq(Query.attr("Name"), Query.value(devGroup));

        // Get the ObjectName Factory for DeviceGroupMBean CMASS Module
        ObjectNameFactory onf = new ObjectNameFactory(DeviceGroupMBean.class
                .getPackage().getName());

        ObjectName deviceGroupObject = onf.getObjectNamePattern(
                DeviceGroupMBean.class);

        Set devices = null;
        Iterator i = null;
        devices = mBeanServer.queryNames(deviceGroupObject, queryExp);
        i = devices.iterator();

        while (i.hasNext()) {
            ObjectName deviceObjName = (ObjectName) i.next();
            String deviceName = onf.getInstanceName(deviceObjName);

            // Get Handler to the DeviceGroupMBean
            DeviceGroupMBean deviceMBean = (DeviceGroupMBean)
                MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
                    deviceObjName, DeviceGroupMBean.class, false);

            if (deviceMBean == null) {
                continue;
            }

            if (isScalable) {
                return true;
            } else {
                String dgNodes[] = deviceMBean.getConnectedNodeOrder();
                List dgNodeList = Arrays.asList(dgNodes);

                if (dgNodeList.containsAll(rgNodeList)) {
                    return true;
                }
            }
        }

        return false;
    }

    public ErrorValue validateInput(String propNames[], HashMap userInputs,
        Object helperData) {
        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

        StringBuffer errStr = new StringBuffer();
        List propList = Arrays.asList(propNames);
        boolean ret;
        String value;

        if (propList.contains(Util.RS_NAME)) {

            // validate user input
            value = (String) userInputs.get(Util.RS_NAME);
            ret = DataServicesUtil.isResourceNameUsed(mBeanServer, value);

            if (ret) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append("");
            }
        }

        if (propList.contains(Util.RG_NAME)) {
            value = (String) userInputs.get(Util.RG_NAME);
            ret = DataServicesUtil.isRGNameUsed(mBeanServer, value);

            if (ret) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append("");
            }
        }

        if (propList.contains(Util.HASP_ALL_FILESYSTEMS) ||
                propList.contains(Util.HASP_ALL_DEVICES)) {

            // call getDCSServiceName
            String nodeList[] = (String[]) userInputs.get(Util.RG_NODELIST);
            String devPath = (String) userInputs.get(Util.DEVPATH);
            String rgMode = (String) userInputs.get(Util.RG_MODE);
            boolean bScalable = false;

            if ((rgMode != null) && rgMode.equals(Util.SCALABLE_RG_MODE)) {
                bScalable = true;
            }

            String devGroupName = getDCSServiceName(devPath);
            ret = canBePrimaried(devGroupName, nodeList, bScalable);

            if (!ret) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append("");
            }
        }

        if (propList.contains(Util.MOUNT_POINT)) {
            String mountpoint = (String) userInputs.get(Util.MOUNT_POINT);
            String zNodeList[] = (String[]) userInputs.get(Util.RG_NODELIST);
            File file = new File(mountpoint);

            if (file.exists() && file.isFile()) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append("");
            } else {
                boolean isUnique = isUniqueMountEntry(mountpoint, zNodeList);

                if (!isUnique) {
                    err.setReturnVal(Boolean.FALSE);
                    errStr.append("");
                }
            }
        }

        if (propList.contains(Util.DEVICE_PATH)) {
            String devpath = (String) userInputs.get(Util.DEVICE_PATH);
            String zNodeList[] = (String[]) userInputs.get(Util.RG_NODELIST);
            String presentNodes = findDeviceEntry(devpath, zNodeList);

            if ((presentNodes != null) && (presentNodes.length() > 0)) {
                err.setReturnVal(Boolean.FALSE);
                errStr.append(presentNodes);
            }
        }

        err.setErrorString(errStr.toString());

        return err;
    }

    public ErrorValue applicationConfiguration(Object helperData) {
        return null;

    }

    public String[] aggregateValues(String propertyName, Map unfilteredValues,
        Object helperData) {
        return null;
    }

    /**
     * This method retrieves the GlobalDevicePaths on the Cluster that are not
     * in any HAStoragePlus resource
     *
     * @param  hashMap containing nodes
     *
     * @return  HashMap of GlobalDevicePaths
     */
    public HashMap getGlobalDevicePaths(HashMap hashMap) {
        HashMap retMap = null;

        List list_dgs = new ArrayList();
        List list_rawdevices = new ArrayList();

        List dgs = new ArrayList();
        List rawdevs = new ArrayList();
        boolean doRgNodeListCheck = true;

        // Get the RG Node List
        String nodes[] = (String[]) hashMap.get(Util.RG_NODELIST);
        String rgMode = (String) hashMap.get(Util.RG_MODE);

        if ((rgMode != null) && rgMode.equals(Util.SCALABLE_RG_MODE)) {
            doRgNodeListCheck = false;
        }

        ArrayList rgNodeList = null;

        for (int i = 0; i < nodes.length; i++) {

            if (rgNodeList == null) {
                rgNodeList = new ArrayList();
            }

            String nodeSplit[] = nodes[i].split(Util.COLON);

            if (!rgNodeList.contains(nodeSplit[0])) {
                rgNodeList.add(nodeSplit[0]);
            }
        }

        // query the DeviceGroupMBean for SVM or VxVM device groups
        QueryExp queryExp = Query.not(Query.or(
                    Query.eq(Query.attr("Type"), Query.value(Util.GLOBAL_DISK)),
                    Query.eq(Query.attr("Type"),
                        Query.value(Util.LOCAL_DISK))));

        // Get the ObjectName Factory for DeviceGroupMBean CMASS Module
        ObjectNameFactory onf = new ObjectNameFactory(DeviceGroupMBean.class
                .getPackage().getName());

        ObjectName deviceGroupObject = onf.getObjectNamePattern(
                DeviceGroupMBean.class);

        Set devices = null;
        Iterator i = null;
        StringBuffer stBuffer = null;
        devices = mBeanServer.queryNames(deviceGroupObject, queryExp);
        i = devices.iterator();

        while (i.hasNext()) {
            ObjectName deviceObjName = (ObjectName) i.next();
            String deviceName = onf.getInstanceName(deviceObjName);

            // Get Handler to the DeviceGroupMBean
            DeviceGroupMBean deviceMBean = (DeviceGroupMBean)
                MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
                    deviceObjName, DeviceGroupMBean.class, false);

            stBuffer = new StringBuffer(deviceName);

            String dgNodes[] = deviceMBean.getConnectedNodeOrder();
            List dgNodeList = Arrays.asList(dgNodes);

            if (!haspResExists(Util.HASP_ALL_DEVICES, deviceName)) {

                if (doRgNodeListCheck && dgNodeList.containsAll(rgNodeList)) {

                    // add deviceName:type
                    stBuffer.append(Util.COLON);
                    stBuffer.append(deviceMBean.getType());
                    dgs.add(stBuffer.toString());
                    list_dgs.add(deviceName);
                }
            }
        }

        // query the DeviceGroupMBean for Local or global disks
        queryExp = Query.or(Query.eq(Query.attr("Type"),
                    Query.value(Util.GLOBAL_DISK)),
                Query.eq(Query.attr("Type"), Query.value(Util.LOCAL_DISK)));

        Set rawDisks = null;
        rawDisks = mBeanServer.queryNames(deviceGroupObject, queryExp);
        i = rawDisks.iterator();

        while (i.hasNext()) {
            ObjectName deviceObjName = (ObjectName) i.next();
            String deviceName = onf.getInstanceName(deviceObjName);

            // Get Handler to the DeviceGroupMBean
            DeviceGroupMBean deviceMBean = (DeviceGroupMBean)
                MBeanServerInvocationHandler.newProxyInstance(mBeanServer,
                    deviceObjName, DeviceGroupMBean.class, false);

            stBuffer = new StringBuffer(deviceName);

            String dgNodes[] = deviceMBean.getConnectedNodeOrder();
            List dgNodeList = Arrays.asList(dgNodes);

            if (!haspResExists(Util.HASP_ALL_DEVICES, deviceName)) {

                if (doRgNodeListCheck && dgNodeList.containsAll(rgNodeList)) {
                    stBuffer.append(Util.COLON);
                    stBuffer.append(deviceMBean.getType());
                    rawdevs.add(stBuffer.toString());
                    list_rawdevices.add(deviceName);
                }
            }
        }

        String hasp_devicegroups[] = (String[]) list_dgs.toArray(new String[0]);
        String hasp_rawdevices[] = (String[]) list_rawdevices.toArray(
                new String[0]);

        String devicegroups[] = (String[]) dgs.toArray(new String[0]);
        String rawdevices[] = (String[]) rawdevs.toArray(new String[0]);

        retMap = new HashMap();
        retMap.put(Util.HASP_DEVICE_GROUPS, hasp_devicegroups);
        retMap.put(Util.HASP_RAW_DEVICES, hasp_rawdevices);
        retMap.put(Util.DEVICE_GROUPS, devicegroups);
        retMap.put(Util.RAW_DEVICES, rawdevices);

        return retMap;

    }

    private boolean haspResExists(String propName, String deviceName) {
        QueryExp haspQuery = Query.match(new AttributeValueExp("Type"),
                Query.value(HASP_RT + "*"));
        if (
            DataServicesUtil.getResourceList(mBeanServer, haspQuery, propName,
                    new String[] { deviceName }) != null) {
            return true;
        }
        return false;
    }
    
    private boolean qfsResExists(String propName, String deviceName) {
        QueryExp qfsQuery = Query.match(new AttributeValueExp("Type"),
                Query.value(QFS_RT + "*"));
        if (
            DataServicesUtil.getResourceList(mBeanServer, qfsQuery, propName,
                    new String[] { deviceName }) != null) {
            return true;
        }

        return false;
    }

    /**
     * This method retrieves the FilesystemMountPoints on the cluster that are
     * not part of any HAStoragePlus Resource
     *
     * @param  hashMap of the list of nodes passed
     *
     * @return  HashMap of FilesystemMountPoints
     */
    public HashMap getFilesystemMountPoints(HashMap hashMap) {

        String filesystems[] = null;
        VfsStruct vfstabEntries[] = null;
        Vector vfilesystems = new Vector();
        Vector v_vfstabEntries = new Vector();
        HashMap map_nodes = new HashMap();
        HashMap map_to_add = new HashMap();
        ArrayList list = new ArrayList();
        Vector v_error_map = new Vector();
        MBeanServerConnection connections[] = null;
        JMXConnector connectors[] = null;
        JMXConnector localConnector = null;
        boolean bScalable = false;
        String global_dev = "/global/\\.devices/node@[0-9]+";


        // Get the RG Node List
        String nodes[] = (String[]) hashMap.get(Util.RG_NODELIST);
        String rgMode = (String) hashMap.get(Util.RG_MODE);

        if ((rgMode != null) && rgMode.equals(Util.SCALABLE_RG_MODE)) {
            bScalable = true;
        }

        // Use the FileAccessorWrapper class to get a handle
        // read the file fully

        // createMBeanServer connection objects
        createMBeanServerConnections(nodes);

        FileAccessorWrapper fileWrapper = new FileAccessorWrapper(
                Util.VFSTAB_FILE, this.mbsConnections);

        ArrayList vfstabList = null;
        VfsStruct vfstab_i = null;
        VfsStruct vfstab_j = null;
        ArrayList vfstabList_i = null;
        ArrayList vfstabList_j = null;
        String split_str[] = null;

        boolean found = false;

        boolean error = false;

        for (int i = 0; i < nodes.length; i++) {
            split_str = nodes[i].split(Util.COLON);
            vfstabList = (ArrayList) fileWrapper.readFully(
                    this.mbsConnections[i]);
            map_nodes.put(split_str[0], vfstabList);
        }

        for (int i = 0; i < nodes.length; i++) {
            split_str = nodes[i].split(Util.COLON);
            vfstabList_i = (ArrayList) map_nodes.get(split_str[0]);

            int size_i = vfstabList_i.size();

            // read the entries from nodes(i)
            for (int index_i = 0; index_i < size_i; index_i++) {
                vfstab_i = (VfsStruct) vfstabList_i.get(index_i);

                // logger.finest("\nline "+(index_i+1)+" on node "+ nodes[i]
                //  +"= "+ vfstab_i);
                // read the last entry here to see if it is set to global

                // add a COMMA in the beginning and end of mount option
                String mount_option = Util.COMMA +
                    vfstab_i.getVfs_mntopts() + Util.COMMA;
                if (!isDCSDevicePath(vfstab_i.getVfs_special()) && 
                        !isSharedQFSMountPoint(mount_option, 
                            vfstab_i.getVfs_fstype())) {
                    continue;
                }

		// skip entries with /global/.devices/node@
		String mount_point = vfstab_i.getVfs_mountp();
		if(mount_point.matches(global_dev)) {
			continue;
		};

                if (nodes.length == 1) {


                    if (isGlobalOrFailover(mount_option,
                                vfstab_i.getVfs_automnt()) || 
                            isSharedQFSMountPoint(mount_option, 
                                vfstab_i.getVfs_fstype())) {
                        if (!vfilesystems.contains(mount_point)) {
                            vfilesystems.add(mount_point);
                            v_vfstabEntries.add(vfstab_i);
                        }
                    }
                }

                // Go check the other nodes now
                for (int j = 0; j < nodes.length; j++) {

                    if (i == j) {
                        continue;
                    }

                    found = false;
                    error = false;
                    split_str = nodes[j].split(Util.COLON);
                    vfstabList_j = (ArrayList) map_nodes.get(split_str[0]);

                    int size_j = vfstabList_j.size();
                    int index_j = -1;

                    while ((index_j < (size_j - 1)) && (!found)) {
                        index_j++;
                        vfstab_j = (VfsStruct) vfstabList_j.get(index_j);

                        if (vfstab_j.equals(vfstab_i)) {

                            // A matching entry is found in the node.
                            // do nothing
                            found = true;
                        } else {

                            // check for typos
                            if (!isDCSDevicePath(vfstab_j.getVfs_special()) && 
                                    !isSharedQFSMountPoint(mount_option,
                                        vfstab_i.getVfs_fstype())) {
                                continue;
                            }

                            if ((vfstab_i.getVfs_special()).equals(
                                        vfstab_j.getVfs_special())) {

                                // the device_to_mount entry matches so
                                // possibly a typo
                                // Error condition. Notify user
                                // provide a function in VfstabFileAccessor
                                // that can do a replace line
                                error = true;

                                break;
                            }
                        }
                    } // while

                    if (error) {
                        v_error_map.add(vfstab_i);
                        v_error_map.add(vfstab_j);

                        continue;
                    } else {

                        if (!found) {

                            // Match is not found
                            split_str = nodes[j].split(Util.COLON);
                            list = (ArrayList) map_to_add.get(split_str[0]);

                            if (list == null) {
                                list = new ArrayList();
                            }

                            list.add(vfstab_i);
                            map_to_add.put(split_str[0], list);
                        }

                        // add a COMMA in the beginning and end of mount option
                        mount_option = Util.COMMA +
                            vfstab_i.getVfs_mntopts() + Util.COMMA;

                        if (isGlobalOrFailover(mount_option,
                                    vfstab_i.getVfs_automnt()) || 
                                isSharedQFSMountPoint(mount_option,
                                    vfstab_i.getVfs_fstype())) {

                            if (!vfilesystems.contains(mount_point)) {
                                vfilesystems.add(mount_point);
                                v_vfstabEntries.add(vfstab_i);
                            }
                        }
                    } // else
                } // for
            } // index_i
        } // for nodes

        int size = vfilesystems.size();

        String filesystem = null;

        // filter out the filesystem mount points that are already a part of a
        // HASP resource or QFS resource
        int i = 0;

        while (i < size) {
            filesystem = (String) vfilesystems.elementAt(i);

            if (haspResExists(Util.HASP_ALL_FILESYSTEMS, filesystem) || 
                    qfsResExists(Util.QFS_FILESYSTEM, filesystem)) {
                vfilesystems.removeElementAt(i);
                v_vfstabEntries.removeElementAt(i);
                size = vfilesystems.size();
            } else {
                i++;
            }
        }

        filesystem = null;

        // filter out the filesystem mount points on SVM/VxVM devicegroups
        // whose connected node list are not part of the RG Nodelist
        i = 0;

        while (i < size) {
            VfsStruct vfstab_entry = (VfsStruct) v_vfstabEntries.get(i);
	    boolean bRemoveEntry = false;

            if (!isSharedQFSMountPoint(vfstab_entry.getVfs_mntopts(),
                        vfstab_entry.getVfs_fstype())) {
		// Nor a shared QFS file system mount point
		if (!isValidFilesystemType(vfstab_entry.getVfs_special())) {
		   // the raw device path is not a valid path
		   bRemoveEntry = true;
		} else if (!checkDCSNodelist(nodes, vfstab_entry.getVfs_special(),
                        bScalable)) {
		    // device cannot be primaried on all nodes
		    bRemoveEntry = true;
		} else if (bScalable && isLocalFileSystem(vfstab_entry)) {
		    // is a local file system
		    bRemoveEntry = true;
		}
	    }

	    if (bRemoveEntry) {
		vfilesystems.removeElementAt(i);
		v_vfstabEntries.removeElementAt(i);
		size = vfilesystems.size();
	    } else {
		i++;
	    }
        }

        filesystems = (String[]) vfilesystems.toArray(new String[0]);
        vfstabEntries = (VfsStruct[]) v_vfstabEntries.toArray(new VfsStruct[0]);

        // Create a HashMap that has the FileSystems, errormap and the
        // map_to_add
        HashMap retMap = new HashMap();

        retMap.put(Util.FILE_SYSTEMS, filesystems);
        retMap.put(Util.VFSTAB_ENTRIES, vfstabEntries);
        retMap.put(Util.ERROR_VFSTAB_VECTOR, v_error_map);
        retMap.put(Util.VFSTAB_MAP_TO_ADD, map_to_add);

        closeConnections(nodes);

        return retMap;
    }

    /*
     * The function findDeviceEntry() takes the device path and
     * returns the list of nodes in which /etc/vfstab file contains
     * a entry for this device path
     */
    private String findDeviceEntry(String xDevPath, String xNodeList[]) {

        // createMBeanServer connection objects
        createMBeanServerConnections(xNodeList);

        FileAccessorWrapper fileWrapper = new FileAccessorWrapper(
                Util.VFSTAB_FILE, this.mbsConnections);
        String split_str[] = null;
        StringBuffer resultNodes = null;

        for (int i = 0; i < xNodeList.length; i++) {
            Object vfsEntryObj = fileWrapper.getvfsspec(xDevPath,
                    this.mbsConnections[i]);
            ArrayList vfsEntries = (ArrayList) vfsEntryObj;

            if (vfsEntries.size() > 0) {
                split_str = xNodeList[i].split(Util.COLON);

                if (resultNodes == null) {
                    resultNodes = new StringBuffer();
                    resultNodes.append(split_str[0]);
                } else {
                    resultNodes.append(Util.COMMA);
                    resultNodes.append(split_str[0]);
                }
            }
        }

        if (resultNodes == null) {
            return null;
        }

        return resultNodes.toString();
    }

    /*
     * The function isUniqueMountEntry() takes the mount path and
     * returns if that mount point exist in /etc/vfstab file
     */
    private boolean isUniqueMountEntry(String xPath, String xNodeList[]) {

        // createMBeanServer connection objects
        createMBeanServerConnections(xNodeList);

        FileAccessorWrapper fileWrapper = new FileAccessorWrapper(
                Util.VFSTAB_FILE, this.mbsConnections);
        ArrayList vfsEntries = null;

        try {
            Object vfsEntryObj = fileWrapper.getvfsfile(xPath,
                    this.localConnector.getMBeanServerConnection());
            vfsEntries = (ArrayList) vfsEntryObj;
        } catch (Exception e) {
            return false;
        }

        if (vfsEntries.size() > 0) {
            return false;
        }

        return true;
    }


    private void createMBeanServerConnections(String nodeList[]) {

        // first get the local connection
        try {
            HashMap env = new HashMap();
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                this.getClass().getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());
            this.localConnector = TrustedMBeanModel.getWellKnownConnector(Util
                    .getClusterEndpoint(), env);
            this.mbsConnections = new MBeanServerConnection[nodeList.length];
            this.connectors = new JMXConnector[nodeList.length];

            for (int i = 0; i < nodeList.length; i++) {
                String split_str[] = nodeList[i].split(Util.COLON);
                String nodeEndPoint = Util.getNodeEndpoint(localConnector,
                        split_str[0]);

                connectors[i] = TrustedMBeanModel.getWellKnownConnector(
                        nodeEndPoint, env);

                // Now get a reference to the mbean server connection
                this.mbsConnections[i] = connectors[i]
                    .getMBeanServerConnection();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SecurityException se) {
            se.printStackTrace();
            throw se;
        }
    }

    public void closeConnections(String nodeList[]) {

        if (nodeList != null) {

            for (int i = 0; i < nodeList.length; i++) {

                try {

                    if (connectors[i] != null) {
                        connectors[i].close();
                    }
                } catch (IOException ioe) {
                }
            }
        }

        if (localConnector != null) {

            try {
                localConnector.close();
            } catch (IOException ioe) {
            }
        }
    }

    /*
     * Valid device paths are /dev/global/dsk/xxx,
     * /dev/md/dsk/xxxx, and /dev/vx/xxx/dsk/xxx.
     */
    private boolean isDCSDevicePath(String devPath) {

        if ((devPath.startsWith(SVC_DEV_GLB_HDR)) ||
                (devPath.startsWith(SVC_DEV_MD_HDR)) ||
                (devPath.startsWith(SVC_DEV_VX_HDR))) {
            return true;
        }

        return false;
    }

    private boolean isSharedQFSMountPoint(String mount_option, String fstype) {
        if (Util.FSTYPE_SAMFS.equals(fstype) && 
                mount_option.indexOf(Util.SHARED) != -1) {
            return true;
        }
        return false;
    }


    private boolean isLocalFileSystem(VfsStruct xVfsEntry) {
        String mtOptions = xVfsEntry.getVfs_mntopts();
        String fstype = xVfsEntry.getVfs_fstype();
        StringBuffer paddedMtOptions = new StringBuffer(Util.COMMA);
        paddedMtOptions.append(mtOptions);
        paddedMtOptions.append(Util.COMMA);

        if ((paddedMtOptions.indexOf(Util.GLOBAL) != -1) && 
                !Util.FSTYPE_SAMFS.equals(fstype)) {
            return false;
        }

        return true;
    }

    private boolean isGlobalOrFailover(String mount_option,
        String mount_at_boot) {

        if (mount_option.indexOf(Util.M) == -1) {

            if ((mount_option.indexOf(Util.GLOBAL) != -1) ||

                    // => ,global, was found. global filesystem
                    ((mount_option.indexOf(Util.GLOBAL) == -1) &&
                        (mount_at_boot.equals(Util.NO) &&
                            (mount_at_boot.length() != 0)))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if ResourceGroup nodelist is a subset of DeviceGroup's connected
     * nodelist. This check is needed for SVM or VxVM DeviceGroups
     */
    private boolean checkDCSNodelist(String nodeList[], String devPath,
        boolean isScalable) {

        if (devPath.startsWith(SVC_DEV_GLB_HDR)) {
            return true;
        }

        String dgName = getDCSServiceName(devPath);

	return canBePrimaried(dgName, nodeList, isScalable);
    }

    /**
     * getDCSServiceName
     *
     * @param  devPath
     *
     * @return  Returns string containing the DCS device group name
     */
    public native String getDCSServiceName(String devPath);

    /**
     * isValidFilesystemType
     * @param path to the device
     *
     * @return checks if the raw device path is a valid file system type
     *	       or not.
     */
    private boolean isValidFilesystemType(String devPath) {
	String fsType = getFSType(devPath);

	return fsType != null;
    }
}
