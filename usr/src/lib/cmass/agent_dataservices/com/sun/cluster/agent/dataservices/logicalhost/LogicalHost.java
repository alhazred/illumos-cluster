/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)LogicalHost.java 1.36     08/09/22 SMI"
 */


package com.sun.cluster.agent.dataservices.logicalhost;

// J2SE
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;

// CACAO
import com.sun.cacao.ObjectNameFactory;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.dataservices.common.*;
import com.sun.cluster.agent.dataservices.sharedaddress.SharedAddress;
import com.sun.cluster.agent.dataservices.utils.*;
import com.sun.cluster.agent.transport.NodeAdapterMBean;
import com.sun.cluster.agent.transport.ZoneAdapterMBean;
import com.sun.cluster.common.TrustedMBeanModel;


/**
 * This interface defines the management operations available to configure a
 * LogicalHost Resource
 */
public class LogicalHost implements LogicalHostMBean {

    /* RTName of LogicalHost MBean */
    public static final String RTNAME = "SUNW.LogicalHostname";

    /* The MBeanServer in which this MBean is inserted. */
    private MBeanServer mBeanServer;

    /* The object name factory used to register this MBean */
    private ObjectNameFactory onf = null;

    /* Command Generation Structure for this Dataservice */
    private DataServiceInfo serviceInfo = null;

    private MBeanServerConnection mbsConnections[];
    private JMXConnector connectors[] = null;
    private JMXConnector localConnector = null;

    /* List of commands executed */
    private List commandList = null;

    private static Logger logger = Logger.getLogger(Util.DOMAIN);

    /**
     * Default constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this MBean.
     */
    public LogicalHost(MBeanServer mBeanServer, ObjectNameFactory onf,
        DataServiceInfo serviceInfo) {

        this.mBeanServer = mBeanServer;
        this.onf = onf;
        this.serviceInfo = serviceInfo;
    }


    /**
     * Returns the ServiceInfo Structure for this DataserviceMBean
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public DataServiceInfo getServiceInfo() {
        return this.serviceInfo;
    }

    /**
     * Sets the ServiceInfo Structure for this DataserviceMBean
     *
     * @param  serviceInfo  Command Generation Structure
     */
    public void setServiceInfo(DataServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    /**
     * Returns list of executed command list
     *
     * @return  serviceInfo Command Generation Structure for this Dataservice
     */
    public List getCommandList() {
        return this.commandList;
    }

    /**
     * Generates the set of commands for configuring this service, given the
     * configuration information. This command uses the ServiceInfo structure,
     * fills the same and generates the command list. This method needs to be
     * synchronized because this method operates on Wizard instance specific
     * information. The LogicalHost MBean is a singleton,if we can maitain an
     * unique instance of this MBean for every wizard instance we need not
     * syncronize.
     *
     * @param  configurationInformation  ConfigurationInformation for
     * configuring an instance of the Dataservice
     *
     * @return  Commands Generated for the given ConfigurationInformation
     */
    public synchronized List generateCommands(Map configurationInformation)
        throws CommandExecutionException {

        try {

            // Fill the ServiceInfo object with the configurationInformation
            serviceInfo.fillServiceInfoStructure(configurationInformation);
            serviceInfo.getResource(Util.LOGICALHOST).setBringOnline(true);

            // Generate CommandSet for this Service
            serviceInfo.generateCommandSet(mBeanServer, onf);
            commandList = serviceInfo.getCommandSet();

        } catch (Exception e) {

            // Unconfigure the Groups and Resources
            try {
                commandList = serviceInfo.getCommandSet();

                // add Rollback Success string to the command list - this
                // would be used as a marker to demarcate configuration and
                // rollback commands
                commandList.add(Util.ROLLBACK_SUCCESS);
                serviceInfo.rollBack(mBeanServer, onf);
                commandList.addAll(serviceInfo.getCommandSet());
            } catch (Exception re) {

                // Rollback Failed
                // add Rollback Fail string in place of Rollback Success string
                // to the command list - this would be used as a marker to
                // demarcate configuration and rollback commands
                commandList.remove(Util.ROLLBACK_SUCCESS);
                commandList.add(Util.ROLLBACK_FAIL);
                commandList.addAll(serviceInfo.getCommandSet());
            }

            if (e instanceof CommandExecutionException) {
                throw (CommandExecutionException) e;
            } else if (e instanceof IOException) {
                throw new CommandExecutionException(Util.IO_ERROR);
            } else {
                throw new CommandExecutionException(e.getMessage());
            }
        } finally {

            // Always reset the structure to the original form
            serviceInfo.reset();
        }

        return commandList;
    }


    // Implementation For the ServiceConfig Interface Methods
    public String[] getDiscoverableProperties() {

        // Only IPMP Groups and Nodelist can be truly discovered at start
        String discoverableProps[] = {};

        return discoverableProps;
    }

    public String[] getAllProperties() {

        // Add the following ResourceGroup properties
        String rgProperties[] = {
                Util.RESOURCENAME, Util.RESOURCEGROUPNAME, Util.NODELIST
            };

        return rgProperties;
    }

    public HashMap discoverPossibilities(String propertyNames[],
        Object helperData) {

        HashMap resultMap = new HashMap();

	for (int p = 0; p < propertyNames.length; p++) {

	    String prop = propertyNames[p];

	    // Discover PropertyNames : This is called while Initializing the
	    // WizardContext Model
	    if (prop.equals(Util.PROPERTY_NAMES)) {

		// Get the discoverableProperty Values
		String discoverableProps[] = getDiscoverableProperties();

		resultMap.putAll(discoverPossibilities(discoverableProps,
		    null));

		// Add other propertyNames to the resultMap
		String allProps[] = getAllProperties();

		for (int i = 0; i < allProps.length; i++) {

		    String propertyName = allProps[i];

		    // Omit the Discovered Properties
		    if (!resultMap.containsKey(propertyName)) {
			resultMap.put(propertyName, null);
		    }
		}

		return resultMap;
	    } else if (prop.equals(Util.RESOURCEGROUPNAME)) {

		//
		// Discover unique name for ResourceGroup
		//

		String rgName;
		String hostNames = (String)helperData;
		rgName = hostNames.replace(',', '-');
		rgName = hostNames.replace('.', '-');

		String xRgName = new String(rgName);
		StringBuffer rgnamBuf = new StringBuffer(xRgName);
		rgName = rgnamBuf.toString();

		// Resource Group name should always start with an alphabet
		// validate and append char sequence
		String firstChar = rgName.substring(0, 1);
		try {
		    Integer intr = Integer.valueOf(firstChar);
		    // No exception thrown, implies rgName starts with integer
		    rgnamBuf = new StringBuffer();
		    rgnamBuf.append(Util.LHNAME_PREFIX);
		    rgnamBuf.append(Util.DASH);
		    rgnamBuf.append(rgName);
		    xRgName = rgnamBuf.toString();
		} catch (NumberFormatException nfe) {
		    // rgName is OK, continue..
		}

		rgnamBuf.append(Util.RG_POSTFIX);
		rgName = rgnamBuf.toString();

		int count = 0;

		// Check if the rgName already exists
		while (DataServicesUtil.isRGNameUsed(mBeanServer, rgName)) {
		    count++;
		    rgnamBuf = new StringBuffer(xRgName);
		    rgnamBuf.append(Util.DASH);
		    rgnamBuf.append(count);
		    rgnamBuf.append(Util.RG_POSTFIX);
		    rgName = rgnamBuf.toString();
		}

		resultMap.put(Util.RESOURCEGROUPNAME, rgName);

		return resultMap;
	    } else if (prop.equals(Util.RESOURCENAME)) {

		//
		// Discover unique name for Resource
		//

		String rName;
		String hostNames = (String)helperData;
		rName = hostNames.replace(',', '-');
		rName = hostNames.replace('.', '-');

		String xRsName = new String(rName);
		StringBuffer rnamBuf = new StringBuffer(xRsName);
		rName = rnamBuf.toString();

		// Resource name should always start with an alphabet
		// validate and append char sequence
		String firstChar = rName.substring(0, 1);
		try {
		    Integer intr = Integer.valueOf(firstChar);
		    // No exception thrown, implies rName starts with integer
		    rnamBuf = new StringBuffer();
		    rnamBuf.append(Util.LHNAME_PREFIX);
		    rnamBuf.append(Util.DASH);
		    rnamBuf.append(rName);
		    xRsName = rnamBuf.toString();
		} catch (NumberFormatException nfe) {
		    // rName is OK, continue..
		}

		rnamBuf.append(Util.RS_POSTFIX);
		rName = rnamBuf.toString();

		int count = 0;

		// Check if the rsName already exists
		while (DataServicesUtil.isResourceNameUsed(mBeanServer, rName)) {
		    count++;
		    rnamBuf = new StringBuffer(xRsName);
		    rnamBuf.append(Util.DASH);
		    rnamBuf.append(count);
		    rnamBuf.append(Util.RS_POSTFIX);
		    rName = rnamBuf.toString();
		}

		resultMap.put(Util.RESOURCENAME, rName);

		return resultMap;
	    } else if (prop.equals(Util.NODELIST)) {

		//
		// Discover NodeList
		//

		resultMap.put(Util.NODELIST,
		    DataServicesUtil.getNodeList(mBeanServer));
	    } else if (prop.equals(Util.ZONE_ENABLE)) {

		//
		// Discover ZONE enable
		//

		// Logical Host support local zones.
		// Return false to disable local zones support.
		resultMap.put(Util.ZONE_ENABLE, new Boolean(true));
	    } else if (prop.equals(Util.NETIFLIST)) {

		// Discover IPMP Groups for the nodelist and hostnames.
		//
		// In case the nodelist contains Exclusive-IP zones,
		// the discovery should be restricted to just the IPMP
		// groups defined within those zone.
		//
		// The mixed IP-type check has already been done
		// previously, so by the time we get here, we know that
		// either all are Exclusive-IP zones, or none of them
		// are.

		List ipmpGroupList = null;
		List nodeAdapterMBeans = new ArrayList();

		// Get the Hostname List and Node List from the Helper Data
		Map helperMap = (Map)helperData;
		String hostNames[] = (String[])helperMap.get(Util.HOSTNAMELIST);
		String nodeList[] = (String[])helperMap.get(Util.NODELIST);
		Boolean b = (Boolean)helperMap.get(Util.EXCLUSIVEIP);
		boolean exclusiveIpZones = false;
		if (b != null) {
		    exclusiveIpZones = b.booleanValue();
		}

		List nodesToDiscover = new ArrayList();
		List tmpList = null;

		if (exclusiveIpZones) {
		    try {
			ObjectNameFactory onf = new ObjectNameFactory(
			    ZoneAdapterMBean.class.getPackage().getName());
			ObjectName zoneAdapterObjName = onf.getObjectName(
			    ZoneAdapterMBean.class, null);
			ZoneAdapterMBean mbean =
			    (ZoneAdapterMBean)MBeanServerInvocationHandler.
			    newProxyInstance(mBeanServer, zoneAdapterObjName,
			    ZoneAdapterMBean.class, false);
			for (int i = 0; i < nodeList.length; i++) {
			    tmpList = DataServicesUtil.
				getIPMPGroupsForHostsinZone(hostNames,
				nodeList[i], mbean);
			    if (tmpList == null) {
				// The loop breaks even if one node does not
				// return a appropriate IPMP group for the
				// user given hostnames
				ipmpGroupList = null;
				break;
			    }
			    if (ipmpGroupList == null) {
				ipmpGroupList = tmpList;
			    } else {
				ipmpGroupList.addAll(tmpList);
			    }
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		} else {
		    // If not Exclusive-IP zones, then make the nodelist
		    // contains just the unique list of plain nodes
		    // without the ":zone" suffixes

		    for (int i = 0; i < nodeList.length; i++) {
			String split_str[] = nodeList[i].split(Util.COLON);
			String node = split_str[0];
			if (!nodesToDiscover.contains(node)) {
			    nodesToDiscover.add(node);
			}
		    }

		    nodeList = (String[]) nodesToDiscover.toArray(
			new String[nodesToDiscover.size()]);

		    try {

			ObjectNameFactory onf = new ObjectNameFactory(
			    NodeAdapterMBean.class.getPackage().getName());
			ObjectName nodeAdapterObjName =
			    onf.getObjectNamePattern(NodeAdapterMBean.class);

			createMBeanServerConnections(nodeList);

			for (int i = 0; i < nodeList.length; i++) {
			    Set adapterInstanceNames =
				mbsConnections[i].queryNames(nodeAdapterObjName,
				null);
			    nodeAdapterMBeans.clear();

			    for (Iterator it = adapterInstanceNames.iterator();
				    it.hasNext(); /* */) {
				ObjectName adapterObjName =
				    (ObjectName)it.next();
				NodeAdapterMBean adapterMBean =
				    (NodeAdapterMBean)
				    MBeanServerInvocationHandler.
				    newProxyInstance(
					mbsConnections[i], adapterObjName,
					NodeAdapterMBean.class, false);
				nodeAdapterMBeans.add(adapterMBean);
			    }

			    tmpList = DataServicesUtil.getIPMPGroupForHost(
				    hostNames, nodeAdapterMBeans);

			    if (tmpList != null) {

				if (ipmpGroupList == null) {
				    ipmpGroupList = tmpList;
				} else {
				    ipmpGroupList.addAll(tmpList);
				}
			    } else {

				// The loop breaks even if one node does not
				// return a appropriate IPMP group for the
				// user given hostnames
				ipmpGroupList = null;

				break;
			    }
			}

			closeConnections(nodeList);

		    } catch (Exception e) {
			e.printStackTrace();
		    }
		}
		resultMap.put(Util.NETIFLIST, ipmpGroupList);
	    }
	}

	return resultMap;
}

public HashMap discoverPossibilities(String nodeList[],
    String propertyNames[], Object helperData) {

    return null;
}

public ErrorValue validateInput(String propertyNames[], HashMap userInputs,
    Object helperData) {


    // Validate ResourceGroup Name
    if (propertyNames[0].equals(Util.RESOURCEGROUPNAME)) {
	String rgName = (String) userInputs.get(Util.RESOURCEGROUPNAME);

            if (DataServicesUtil.isRGNameUsed(mBeanServer, rgName)) {
                return new ErrorValue(new Boolean(false), " ");
            }

	    // Resource Group name should always start with an alphabet
	    String firstChar = rgName.substring(0, 1);
	    try {
		Integer intr = Integer.valueOf(firstChar);
		// No exception thrown, implies rgName starts with integer
                return new ErrorValue(new Boolean(false), "Invalid ResourceGroup Name");
	    } catch (NumberFormatException nfe) {
		// rgName is OK, continue..
	    }

            return new ErrorValue(new Boolean(true), " ");
        }
        // Validate Resource Name
        else if (propertyNames[0].equals(Util.RESOURCENAME)) {
            String rName = (String) userInputs.get(Util.RESOURCENAME);

            if (DataServicesUtil.isResourceNameUsed(mBeanServer, rName)) {
                return new ErrorValue(new Boolean(false), " ");
            }

	    // Resource name should always start with an alphabet
	    String firstChar = rName.substring(0, 1);
	    try {
		Integer intr = Integer.valueOf(firstChar);
		// No exception thrown, implies rName starts with integer
                return new ErrorValue(new Boolean(false), "Invalid Resource Name");
	    } catch (NumberFormatException nfe) {
		// rName is OK, continue..
	    }

            return new ErrorValue(new Boolean(true), " ");
        }
        // Validate Hostname
        // Not Validating whether this interface is plumbed elsewhere
        // Other than the cluster nodes
        else if (propertyNames[0].equals(Util.HOSTNAMELIST)) {
            String hostNamesArr[] = (String[]) userInputs.get(
                    Util.HOSTNAMELIST);

            // Validate for Valid HostName
            for (int i = 0; i < hostNamesArr.length; i++) {

                if (!DataServicesUtil.isHostNameValid(hostNamesArr[i])) {
                    return new ErrorValue(new Boolean(false),
                            hostNamesArr[i] + " is an invalid hostname");
                }

                // Validate whether the HostName is already used by Another LH
                // Resource in the Cluster
                List resourceList = DataServicesUtil.getResourceList(
                        mBeanServer, RTNAME, Util.HOSTNAMELIST,
                        new String[] { hostNamesArr[i] });

                if ((resourceList != null) && (resourceList.size() > 0)) {
                    return new ErrorValue(new Boolean(false),
                            hostNamesArr[i] + " is being used by the another " +
                            "resource " +
                            resourceList.iterator().next().toString());
                }

                // Validate whether the HostName is already used by SH
                // Resource in the Cluster
                resourceList = DataServicesUtil.getResourceList(mBeanServer,
                        SharedAddress.RTNAME, Util.HOSTNAMELIST,
                        new String[] { hostNamesArr[i] });

                if ((resourceList != null) && (resourceList.size() > 0)) {
                    return new ErrorValue(new Boolean(false),
                            hostNamesArr[i] + " is being used by the another " +
                            "resource " +
                            resourceList.iterator().next().toString());
                }
            }
        }

        return new ErrorValue(new Boolean(true), " ");
    }

    public ErrorValue applicationConfiguration(Object helperData) {
        return null;

    }

    public String[] aggregateValues(String propertyName, Map unfilteredValues,
        Object helperData) {
        return null;
    }

    private void createMBeanServerConnections(String nodeList[]) {

        // first get the local connection
        try {
            HashMap env = new HashMap();
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                this.getClass().getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());
            this.localConnector = TrustedMBeanModel.getWellKnownConnector(Util
                    .getClusterEndpoint(), env);
            this.mbsConnections = new MBeanServerConnection[nodeList.length];
            this.connectors = new JMXConnector[nodeList.length];

            for (int i = 0; i < nodeList.length; i++) {
                String split_str[] = nodeList[i].split(Util.COLON);
                String nodeEndPoint = Util.getNodeEndpoint(localConnector,
                        split_str[0]);

                connectors[i] = TrustedMBeanModel.getWellKnownConnector(
                        nodeEndPoint, env);

                // Now get a reference to the mbean server connection
                this.mbsConnections[i] = connectors[i]
                    .getMBeanServerConnection();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SecurityException se) {
            se.printStackTrace();
            throw se;
        }
    }

    private void closeConnections(String nodeList[]) {

        if (nodeList != null) {

            for (int i = 0; i < nodeList.length; i++) {

                try {

                    if (connectors[i] != null) {
                        connectors[i].close();
                        connectors[i] = null;
                    }
                } catch (IOException ioe) {
                }
            }
        }

        if (localConnector != null) {

            try {
                localConnector.close();
                localConnector = null;
            } catch (IOException ioe) {
            }
        }
    }
}
