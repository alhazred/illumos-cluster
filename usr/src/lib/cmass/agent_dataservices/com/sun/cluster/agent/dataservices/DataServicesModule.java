/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DataServicesModule.java 1.20     08/05/20 SMI"
 */

package com.sun.cluster.agent.dataservices;

// J2SE

// Cacao
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

import com.sun.cluster.agent.dataservices.utils.DataServiceInfo;
import com.sun.cluster.agent.dataservices.utils.FileAccessor;
import com.sun.cluster.agent.dataservices.utils.FileAccessorMBean;
import com.sun.cluster.agent.dataservices.utils.Infrastructure;
import com.sun.cluster.agent.dataservices.utils.InfrastructureMBean;

// CMASS
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.utils.VfstabFileAccessor;
import com.sun.cluster.agent.dataservices.utils.VfstabFileAccessorMBean;
import com.sun.cluster.agent.rgm.ResourceMBean;

import java.io.File;

import java.lang.reflect.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServer;


/**
 * DataServices module class. This module class would register all the
 * DataServices MBeans and Utility MBeans.
 */
public class DataServicesModule extends Module {

    /* Directory where the XML files are present for each data service */
    private static String XML_DIR = "/opt/cluster/lib/ds/registry/";

    /* Java class path system property name */
    private static String CLASSPATH = "java.class.path";

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(Util.DOMAIN);
    private final static String logTag = "DataServicesModule";

    /**
     * FileAccessor would not read the file if the size of given file exceeds
     * FILE_SIZE_READ_LIMIT. This parameter could be specified in Deployment
     * descriptor of this module. The value of this parameter is specified in
     * bytes.
     */
    private long FILE_SIZE_READ_LIMIT;

    /** Internal reference to the MBeanServer */
    private MBeanServer mbs;

    /** Internal reference to the ObjectNameFactory */
    private ObjectNameFactory objectNameFactory;

    /** Reference to RGM Module ObjectNameFactory */
    private ObjectNameFactory rgmOnf;

    /** List of MBeans registered */
    private List mbeanList = null;

    /** Reference to the serviceInfo object */
    private DataServiceInfo serviceInfo;

    /**
     * Creates a new <code>DataServicesModule</code> instance.
     *
     * @param  descriptor  a <code>com.sun.cacao.DeploymentDescriptor</code>
     * value
     */
    public DataServicesModule(DeploymentDescriptor descriptor) {

        super(descriptor);
        logger.entering(logTag, "<init>", descriptor);


        Map parameters = descriptor.getParameters();

        try {

            // Load the agent JNI library
            String library = (String) parameters.get("library");
            Runtime.getRuntime().load(library);

            // Load other parameters
            FILE_SIZE_READ_LIMIT = Long.parseLong((String) parameters.get(
                        "FILE_SIZE_READ_LIMIT"));

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to load native runtime library" + ule);
            throw ule;
        } catch (NumberFormatException nfe) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Error reading parameters" + nfe);
            throw nfe;

        } finally {
            logger.exiting(logTag, "<init>");
        }
    }

    /**
     * Invoked by the container when unlocking the module.
     */
    protected void start() {

        logger.entering(logTag, "start");

        // Get the MBean Server
        mbs = getMbs();

        // Get the ObjectName Factory for this Module
        objectNameFactory = new ObjectNameFactory(getDomainName());

        // Get the ObjectName Factory for RGM CMASS Module
        rgmOnf = new ObjectNameFactory(ResourceMBean.class.getPackage()
                .getName());

        try {

            // Register VfstabFileAccessorMBean
            logger.finest("create VfstabFileAccessorMBean");
            mbs.registerMBean(new VfstabFileAccessor(),
                objectNameFactory.getObjectName(VfstabFileAccessorMBean.class,
                    null));

            // Registering FileAccessorMBean
            logger.finest("create FileAccessorMBean");
            mbs.registerMBean(new FileAccessor(FILE_SIZE_READ_LIMIT),
                objectNameFactory.getObjectName(FileAccessorMBean.class, null));

            // Registering InfrastructureMBean
            logger.finest("create InfrastructureMBean");
            mbs.registerMBean(new Infrastructure(mbs, objectNameFactory),
                objectNameFactory.getObjectName(InfrastructureMBean.class,
                    null));

            mbeanList = new ArrayList();

            /*
             * Get a list of all xml files under the
             * /opt/cluster/lib/ds/registry/
             */
            File xml_dir = new File(XML_DIR);

            File files[] = xml_dir.listFiles();

            for (int i = 0; i < files.length; i++) {

                if ((!(files[i].getName()).endsWith(".xml")) ||
                        (files[i] == null) || (files[i].isDirectory()) ||
                        !(files[i].exists()) || !(files[i].canRead())) {
                    continue;
                }

                DataServiceInfo serviceInfo = new DataServiceInfo(files[i]);

                try {

                    // get servicename from serviceInfo
                    String service = serviceInfo.getService();
                    String className = Util.DOMAIN + Util.DOT +
                        service.toLowerCase() + Util.DOT;
                    String serviceName = className + service;
                    String serviceNameMBean = serviceName + "MBean";

                    // Register all the MBeans
                    logger.finest("create " + serviceNameMBean);

                    Object child = createObject(serviceName,
                            new Class[] {
                                javax.management.MBeanServer.class,
                                com.sun.cacao.ObjectNameFactory.class,
                                com.sun.cluster.agent.dataservices.utils
                                .DataServiceInfo.class
                            }, new Object[] { mbs, rgmOnf, serviceInfo });

                    Class targetClass = Class.forName(serviceNameMBean);

                    if (child != null) {
                        mbs.registerMBean(child,
                            objectNameFactory.getObjectName(targetClass, null));
                        mbeanList.add(targetClass);
                    }
                }
                // If MBean Class is not found, do not register
                catch (ClassNotFoundException ce) {
                    logger.finest("Unable to find class : " + ce.getMessage());
                }
            } // for

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);

            logger.finest("Unable to start DataServices module : " +
                e.getMessage());
        }

        logger.exiting(logTag, "start");
    }

    /**
     * Invoked by the container when locking the module.
     */
    protected void stop() {

        logger.entering(logTag, "stop");

        try {

            // Unregistering VfstabFileAccessorMBean
            mbs.unregisterMBean(objectNameFactory.getObjectName(
                    VfstabFileAccessorMBean.class, null));

            // Unregistering FileAccessorMBean
            mbs.unregisterMBean(objectNameFactory.getObjectName(
                    FileAccessorMBean.class, null));

            // Unregistering InfrastructureMBean
            mbs.unregisterMBean(objectNameFactory.getObjectName(
                    InfrastructureMBean.class, null));

            // Unregistering all MBeans that are in mbeanList
            int size = mbeanList.size();

            for (int i = 0; i < size; i++) {
                mbs.unregisterMBean(objectNameFactory.getObjectName(
                        (Class) mbeanList.get(i), null));
            }

            mbeanList.clear();
            mbeanList = null;
        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            // Ignore - already logged
        }

        objectNameFactory = null;
        mbs = null;
        logger.exiting(logTag, "stop");
    }

    private Object createObject(String classname, Class argTypes[],
        Object args[]) {
        Class targetClass = null;

        try {
            targetClass = Class.forName(classname);

            Constructor constructor = targetClass.getConstructor(argTypes);

            return (constructor.newInstance(args));
        } catch (ClassNotFoundException e) {
            logger.finest("class " + classname + " not found.");
        } catch (NoSuchMethodException e) {
            logger.finest("Constructor " + classname + " not found in class " +
                targetClass.getName());
            logger.finest("Expecting constructor with argument list:");

            for (int i = 0; i < argTypes.length; i++) {
                logger.finest("  " + argTypes[i]);
            }

            e.printStackTrace();
        } catch (SecurityException e) {
            logger.finest("No access for constructor in class " +
                targetClass.getName());
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            logger.finest("No access for constructor in class " +
                targetClass.getName());
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            logger.finest("Bad argument passed to constructor in class " +
                targetClass.getName());
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger.finest("InvocationTargetException thrown in " +
                "constructor in class " + targetClass.getName());
            e.printStackTrace();
        } catch (InstantiationException e) {
            logger.finest("InstantiationException thrown in " +
                "constructor in class " + targetClass.getName());
            e.printStackTrace();
        }

        return (null);
    }
}
