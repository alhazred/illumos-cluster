/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)DataServiceInfo.java	1.19	08/12/03 SMI"
 */


package com.sun.cluster.agent.dataservices.utils;

// J2SE
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// JMX
import javax.management.MBeanServer;

// CACAO
import com.sun.cacao.ObjectNameFactory;

// CMAS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.dataservices.common.ServiceConfig;


/**
 * The DataServiceInfo class wraps the configuration information for a
 * Dataservice that is specified in the Serviceconfig XML file.
 */
public class DataServiceInfo {

    private String service; // Name of the Dataservice
    private String jarFileLocation; // Location of the ServiceConfiguration JAR
    private String configurationClassName; // Name of the ConfigurationClass
    private String verificationClassName; // Name of the VerificaionClass

    private ServiceConfig configurationClass = null; // Class of type
                                                     // configurationClassName
    private Class verificationClass = null; // Class of type
                                            // verificationClassName
    private List resourceGroupList; // List of Groups to be configured for this
                                    // Service.
    private List commandSet; // List of commands generated to configure this
                             // group.


    /**
     * Creates a new instance of DataServiceInfo from the serviceConfigXML file.
     *
     * @param  serviceConfigXML  the ServiceConfig XML file that is to be parsed
     */
    public DataServiceInfo(File serviceConfigXML) {

        try {

            // Check for File Existence
            if (!serviceConfigXML.exists()) {
                throw new Exception(serviceConfigXML + " : File Not Found");
            } // Check for Read permissions
            else if (!serviceConfigXML.canRead()) {
                throw new Exception(serviceConfigXML +
                    " : File does not Have Proper Read Permissions");
            } else {

                // Initialize the ResourceGroup List
                setResourceGroupList(new ArrayList());

                /*
                 * Create an instance of ServiceConfigXMLParser and populate
                 * current ServiceInfo
                 * object from the serviceConfigXML file.
                 */
                ServiceConfigXMLParser serviceConfigXMLParser =
                    new ServiceConfigXMLParser(this, serviceConfigXML);
            }
        } catch (Exception e) {
            System.out.println("Unable to parse XML File" + "\n" +
                e.toString());
            System.exit(1);
        }
    }

    /**
     * Generate the list of commands based on the service configuration
     * information.
     *
     * @param  mbs
     *
     * @param  onf
     *
     * @throws  Exception
     */
    public void generateCommandSet(MBeanServer mbs, ObjectNameFactory onf)
        throws Exception {

        if (resourceGroupList != null) {
            commandSet = new ArrayList();

            // Iterate through the groupList and generate commands for each
            // group
            Iterator i = resourceGroupList.iterator();

            while (i.hasNext()) {
                ResourceGroupInfo group = (ResourceGroupInfo) i.next();

                try {
                    group.generateCommandSet(mbs, onf, commandSet);
                } catch (CommandExecutionException e) {
                    addErrorCommandToList(e);
                    throw e;
                } catch (Exception e) {
                    throw e;
                }

                group.setGroupConfigured(true);
            }
        }
    }

    /**
     * Rollbacks all the commands generated for the configuration information
     * held by the datastructure
     */
    public void rollBack(MBeanServer mbs, ObjectNameFactory onf)
        throws Exception {

        if (resourceGroupList != null) {
            commandSet = new ArrayList();

            // Iterate through the groupList and rollback commands
            // for each group
            Iterator i = resourceGroupList.iterator();

            while (i.hasNext()) {
                ResourceGroupInfo group = (ResourceGroupInfo) i.next();

                try {
                    group.rollBack(mbs, onf, commandSet);
                } catch (Exception e) {

                    if (e instanceof CommandExecutionException) {
                        CommandExecutionException cex =
                            (CommandExecutionException) e;

                        addErrorCommandToList(cex);

                        // Add error message to the list of commands
                        ExitStatus rStatus[] = cex.getExitStatusArray();
                        StringBuffer errStr = new StringBuffer();

                        if ((rStatus != null) && (rStatus.length > 0)) {
                            List errList = rStatus[0].getErrStrings();

                            if (errList != null) {
                                Iterator iter = errList.iterator();

                                while (iter.hasNext()) {
                                    errStr.append(iter.next());
                                    errStr.append("\n");
                                }
                            }
                        }

                        commandSet.add(errStr.toString());
                    }

                    throw e;
                }

                group.setGroupRollbacked(true);
            }
        }
    }

    /**
     * This method removes any session specific information that was used to
     * generate the commands
     */
    public void reset() {

        // Iterate through the groupList and reset the information
        Iterator i = resourceGroupList.iterator();

        while (i.hasNext()) {
            ResourceGroupInfo group = (ResourceGroupInfo) i.next();
            group.reset();
        }
    }

    /**
     * Returns the Name of the Service this object represents
     *
     * @return  the Servicename
     */
    public String getService() {
        return service;
    }

    /**
     * Sets the Name of the Service this object represents
     *
     * @param  service  Name of the Service
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * Returns the jarFileLocation for this Service
     *
     * @return  the jarFileLocation for this Service
     */
    public String getJarFileLocation() {
        return jarFileLocation;
    }

    /**
     * Sets the jarFileLocation for this Service
     *
     * @param  jarFileLocation  the jarFileLocation for this Service
     */
    public void setJarFileLocation(String jarFileLocation) {
        this.jarFileLocation = jarFileLocation;
    }

    /**
     * Return Class object given the class name
     */
    public Class getClass(String className, ClassLoader parent) {
        Class myClass = null;

        try {
            URL urlArray[] = new URL[1];
            urlArray[0] = new URL("jar:file:" + getJarFileLocation() + "!/");

            URLClassLoader myClassLoader = URLClassLoader.newInstance(urlArray,
                    parent);
            myClass = myClassLoader.loadClass(className);
        } catch (Exception e) {
            System.out.println("Could not load class " + e.getMessage());
        }

        return myClass;
    }

    /**
     * Returns the configuration Class object of type ServiceConfig
     */
    public ServiceConfig getConfigurationClass(ClassLoader parent) {
        Class configClass = null;

        if (this.configurationClass == null) {
            configClass = getClass(getConfigurationClassName(), parent);

            if (configClass != null) {

                try {
                    this.configurationClass = (ServiceConfig) configClass
                        .newInstance();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }

        return this.configurationClass;
    }

    /**
     * Returns the verification Class
     */
    public Class getVerificationClass(ClassLoader parent) {

        if (this.verificationClass == null) {
            this.verificationClass = getClass(getVerificationClassName(),
                    parent);
        }

        return this.verificationClass;
    }

    /**
     * Returns the configurationClassName for this Service.
     *
     * @return  the configurationClassName for this Service.
     */
    public String getConfigurationClassName() {
        return configurationClassName;
    }

    /**
     * Sets the ConfigurationClass for this Service
     *
     * @param  configurationClassName  the ConfigurationClass for this Service
     */
    public void setConfigurationClassName(String configurationClassName) {
        this.configurationClassName = configurationClassName;
    }

    /**
     * Returns the VerificationClass for this service
     *
     * @return  VerificationClass for this Service
     */
    public String getVerificationClassName() {
        return verificationClassName;
    }

    /**
     * Sets the VerificationClass for this Service
     *
     * @param  verificationClassName for this Service
     */
    public void setVerificationClassName(String verificationClassName) {
        this.verificationClassName = verificationClassName;
    }

    /**
     * Returns the ResourceGroup List of this serviceInfo object
     *
     * @return  ResourceGroup List for this Service
     */
    public List getResourceGroupList() {
        return resourceGroupList;
    }

    /**
     * Sets the ResourceGroupList of this serviceInfo object
     *
     * @param  resourceGroupList  ResourceGroup List for this Service
     */
    public void setResourceGroupList(List resourceGroupList) {
        this.resourceGroupList = resourceGroupList;
    }

    /**
     * Returns a ResourceGroupInfo object given its logical name.
     *
     * @param  groupName  Logical name of the Group
     *
     * @return  ResourceGroupInfo object (or) null if no group is found with
     * that logical name.
     */
    public ResourceGroupInfo getResourceGroup(String groupName) {

        if (groupName == null)
            return null;

        Iterator i = resourceGroupList.iterator();

        while (i.hasNext()) {
            ResourceGroupInfo group = (ResourceGroupInfo) i.next();

            if ((group != null) && group.getGroupName().equals(groupName)) {
                return group;
            }
        }

        return null;
    }

    /**
     * Finds a ResourceGroupInfo object in this serviceInfo object for a
     * specified groupName and groupId
     *
     * @param  groupId  Logical ID of the group
     * @param  nameInCluster  of the group in the cluster
     *
     * @return  group object if found or NULL
     */
    public ResourceGroupInfo getResourceGroup(String groupId,
        String nameInCluster) {
        List groupList = resourceGroupList;

        if ((groupId == null) || (nameInCluster == null)) {
            return null;
        }

        for (Iterator i = groupList.iterator(); i.hasNext();) {
            ResourceGroupInfo group = (ResourceGroupInfo) i.next();

            if (group.getGroupName().equals(groupId)) {
                String name = group.getName();

                if ((name != null) && name.equals(nameInCluster)) {
                    return group;
                }
            }
        }

        return null;
    }

    /**
     * Returns a ResourceInfo object given its ResourceId
     *
     * @param  resourceId  ResourceId of the Resource
     *
     * @return  ResourceInfo object (or) null if no Resource is found with the
     * given ResourceId
     */
    public ResourceInfo getResource(String resourceId) {

        if (resourceId == null)
            return null;

        Iterator i = resourceGroupList.iterator();

        while (i.hasNext()) {
            ResourceGroupInfo group = (ResourceGroupInfo) i.next();

            if (group != null) {
                Iterator j = group.getResources().iterator();

                while (j.hasNext()) {
                    ResourceInfo resource = (ResourceInfo) j.next();

                    if ((resource != null) &&
                            resource.getResourceId().equals(resourceId)) {
                        return resource;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Find a ResourceInfo object within a ResourceGroup object This is useful
     * when we have multiple group structures of the same group ID in the
     * serviceInfo object
     *
     * @param  resourceGroup
     * @param  resourceId
     *
     * @return  ResourceInfo object if found or null
     */
    public ResourceInfo getResource(ResourceGroupInfo resourceGroup,
        String resourceId) {
        List resources = resourceGroup.getResources();

        for (Iterator i = resources.iterator(); i.hasNext();) {
            ResourceInfo resource = (ResourceInfo) i.next();

            if (resource.getResourceId().equals(resourceId)) {
                return resource;
            }
        }

        return null;
    }


    /**
     * toString() Method
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("Service Name            :" + service + "\n");
        buffer.append("jarFileLocation         :" + jarFileLocation + "\n");
        buffer.append("configurationClassName      :" + configurationClassName +
            "\n");
        buffer.append("verificationClassName       :" + verificationClassName +
            "\n");

        Iterator i = resourceGroupList.iterator();

        while (i.hasNext()) {
            buffer.append(i.next() + "\n\t");
        }

        return buffer.toString();
    }

    /**
     * Gets the CommandSet generated for this Service.
     *
     * @return  The CommandSet for this Service
     */
    public List getCommandSet() {
        return commandSet;
    }

    /**
     * Fills the ServiceInfo Structure with the Information Present in the
     * HashMap
     *
     * @param  configurationInformation  HashMap containing the configuration
     * Information. The HashMap is defined as follows : For Every Group or
     * ResourceType entry in the XML, there will be the following keys in the
     * HashMap, For Groups, <GroupName>_Name = Name of the Group as in the
     * Cluster <GroupName>_rgProperties = Map of rgProperties to be set for this
     * Group. For ResourceTypes, <ResourceType>_Name = Name of the Resource as
     * in the Cluster <ResourceType>_sysProperties = System Properties for the
     * Resource <ResourceType>_extnProperties = Extension Properties for the
     * Resource
     */
    public void fillServiceInfoStructure(Map configurationInformation)
        throws Exception {

        if (configurationInformation == null) {
            throw new Exception("Configuration Information Unavailable");
        }


        Set entrySet = configurationInformation.entrySet();
        Iterator i = entrySet.iterator();

        // Go through Each of the Key and set the Properties
        while (i.hasNext()) {
            Map.Entry entry = (Map.Entry) i.next();
            String key = (String) entry.getKey();
            Object value = entry.getValue();

            // Get the Handler to the Group or ResourceType object from the Key

            // Split the Key into ResourceID or GroupName, Property
            String keys[] = key.split("_");

            if (keys.length != 2) {
                throw new Exception("Illegal Format in HashMap : " + key);
            } else {
                String id = keys[0];
                String property = keys[1];

                ResourceInfo resource = getResource(id);

                if (resource == null) {
                    ResourceGroupInfo group = getResourceGroup(id);

                    if (group == null) {
                        throw new Exception(
                            "No Object Found For the ID String : " + id);
                    }
                    // Found a Matching Group
                    else {

                        if (property.equals("Name")) {
                            group.setName(value.toString());
                        } else if (property.equals("rgProperties")) {

                            if (value instanceof Map) {
                                group.setRgProperties((Map) value);
                            } else {
                                throw new Exception("Invalid Value for Key : " +
                                    key);
                            }
			} else if (property.equals("zoneCluster")) {
			    if (value != null) {
				group.setZoneClusterName(value.toString());
			    }
                        } else {
                            throw new Exception("Invalid Property Name : " +
                                property);
                        }
                    }
                    // Found a Matching ResourceInfo Object
                } else {

                    if (property.equals("Name")) {
                        resource.setResourceName(value.toString());
		    } else if (property.equals("zoneCluster")) {
			if (value != null) {
			    resource.setZoneClusterName(value.toString());
			}
                    } else if (property.equals("sysProperties")) {

                        if (value instanceof List) {
                            resource.setSysProperties((List) value);
                        } else {
                            throw new Exception("Invalid Value for Key : " +
                                key);
                        }
                    } else if (property.equals("extProperties")) {

                        if (value instanceof List) {
                            resource.setExtProperties((List) value);
                        } else {
                            throw new Exception("Invalid Value for Key : " +
                                key);
                        }
                    } else {
                        throw new Exception("Invalid Property Name : " +
                            property);
                    }
                }
            }
        }
    }


    private void addErrorCommandToList(CommandExecutionException e) {
        ExitStatus status[] = ((CommandExecutionException) e)
            .getExitStatusArray();
        StringBuffer cmdsStr = null;

        if ((status != null) && (status.length > 0)) {
            String cmds[] = status[0].getCmdExecuted();

            if (cmds != null) {

                for (int x = 0; x < cmds.length; x++) {

                    if (cmdsStr == null) {
                        cmdsStr = new StringBuffer();
                        cmdsStr.append(cmds[x]);
                    } else {
                        cmdsStr.append(" ");
                        cmdsStr.append(cmds[x]);
                    }
                }
            }
        }

        if (cmdsStr != null) {
            commandSet.add(cmdsStr.toString());
        }
    }
}
