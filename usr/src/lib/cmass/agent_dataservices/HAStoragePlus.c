/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */
#pragma ident	"@(#)HAStoragePlus.c	1.4	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libdcs.h>
#include <netdb.h>
#include <jniutil.h>
#include "com/sun/cluster/agent/dataservices/hastorageplus/HAStoragePlus.h"

/*
 * Class:     com_sun_cluster_agent_dataservices_hastorageplus_HAStoragePlus
 * Method:    getDCSServiceName
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jstring JNICALL
Java_com_sun_cluster_agent_dataservices_hastorageplus_\
HAStoragePlus_getDCSServiceName
	(JNIEnv *env, jclass this, jstring devPath) {

	dc_error_t dc;
	char *sername = NULL;
	struct stat s;
	dev_t dev;
	jstring result_string = NULL;

	const char *path = (*env)->GetStringUTFChars(env, devPath, NULL);

	if (path != NULL) {
	    if (stat(path, &s) == 0 &&
		(S_ISCHR(s.st_mode) ||
		S_ISBLK(s.st_mode))) {

		dev = s.st_rdev;
		dcs_initialize();
		dc = dcs_get_service_name_by_dev_t(
				getemajor(dev),
				geteminor(dev),
				&sername);

		if (dc != DCS_SUCCESS) {
		    sername = "ERROR";
		}
	    }
	} else {
		sername = "ERROR";
	}

	result_string = (jstring) newObjStringParam(env,
				"java/lang/String",
				sername);

	return (result_string);
}
