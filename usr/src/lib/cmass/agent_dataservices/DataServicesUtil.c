/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */
#pragma ident	"@(#)DataServicesUtil.c	1.11	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <jniutil.h>
#include "com/sun/cluster/agent/dataservices/utils/DataServicesUtil.h"


/*
 * Class:     com_sun_cluster_agent_dataservices_util_DataServicesUtil
 * Method:    isHostNameValid
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL
Java_com_sun_cluster_agent_dataservices_utils_DataServicesUtil_isHostNameValid
	(JNIEnv *env, jclass this, jstring hostName) {

	char abuf[INET6_ADDRSTRLEN];
	int error_num;
	struct hostent *hp;

	/* Get the Hostname to be tested */
	const char *hName = (*env)->GetStringUTFChars(env, hostName, NULL);

	/* return 1 if argv is null */
	if (hName == NULL) {
		return (JNI_FALSE);
	}

	/* argv can be a pointer to a hostname or literal IP address */
	hp = getipnodebyname(hName, AF_INET6, AI_ALL | AI_ADDRCONFIG |
		AI_V4MAPPED, &error_num);

	if (hp == NULL) {
		/*
		 * error_num might also be NO_DATA or TRY_AGAIN
		 * Not Handling them as of now.
		 */
		return (JNI_FALSE);
	}
	freehostent(hp);
	return (JNI_TRUE);
}

/*
 * Class:     com_sun_cluster_agent_dataservices_util_DataServicesUtil
 * Method:    isHostNameIPv6
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL
Java_com_sun_cluster_agent_dataservices_utils_DataServicesUtil_isHostNameIPv6
	(JNIEnv *env, jclass this, jstring hostName) {

	int check;
	char abuf[INET6_ADDRSTRLEN];
	int error_num;
	struct hostent *hp;

	/* Get the Hostname to be tested */
	const char *hName = (*env)->GetStringUTFChars(env, hostName, NULL);

	/* return 1 if argv is null */
	if (hName == NULL) {
		return (JNI_FALSE);
	}

	/*
	 * argv can be a pointer to a hostname or literal IP address
	 *
	 * Checking strictly for IPv6 Address
	 */
	hp = getipnodebyname(hName, AF_INET6, 0, &error_num);
	if (hp == NULL) {
		/*
		 * error_num might also be NO_DATA or TRY_AGAIN
		 * Not Handling them as of now.
		 */
		return (JNI_FALSE);
	}

	freehostent(hp);
	return (JNI_TRUE);
}

/*
 * Class:     com_sun_cluster_agent_dataservices_utils_DataServicesUtil
 * Method:    clearScreen
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_com_sun_cluster_agent_dataservices_utils_DataServicesUtil_clearScreen
(JNIEnv *env, jclass this) {
	system("clear");
}
