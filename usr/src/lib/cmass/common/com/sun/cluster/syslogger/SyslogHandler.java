/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SyslogHandler.java 1.6     08/05/20 SMI"
 *
 */

package com.sun.cluster.syslogger;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.text.MessageFormat;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.*;


/**
 * The <tt>SyslogHandler</tt> object takes log messages from a Logger and
 * publishes them.  The log messages are written to the system log subsystem.
 *
 * <p>The <tt>SyslogHandler</tt> can be disabled by doing a <tt>
 * setLevel(Level.OFF)</tt> and can  be re-enabled by doing a <tt>setLevel</tt>
 * with an appropriate level.
 *
 * <p>Code inspired from Lockhart's ConsoleLogHandler sources
 */

public class SyslogHandler extends Handler {

    protected static final int SYSLOG_DEBUG = 7;
    protected static final int SYSLOG_INFO = 6;
    protected static final int SYSLOG_NOTICE = 5;
    protected static final int SYSLOG_WARNING = 4;
    protected static final int SYSLOG_ERR = 3;
    protected static final int SYSLOG_CRIT = 2;

    // For completeness, even though we don't use them ...
    protected static final int SYSLOG_ALERT = 1;
    protected static final int SYSLOG_EMERG = 0;

    private LogManager manager = LogManager.getLogManager();
    private boolean open = false;

    /**
     * Creates a new <code>SyslogHandler</code> instance.
     */
    public SyslogHandler() {

        open = true;
    }

    private static native void doSyslog(String message, String identity,
        int severity);

    /**
     * Publish a <tt>LogRecord</tt>. Log a message, specifying source class,
     * method, and resource bundle name, with an array of object arguments.
     *
     * <p>The logging request was made initially to a <tt>Logger</tt> object,
     * which initialized the <tt>LogRecord</tt> and forwarded it here.
     *
     * <p>The <tt>Handler</tt> is responsible for formatting the message, when
     * and if necessary.  The formatting should include localization.
     *
     * @param  record  description of the log event
     */
    public void publish(LogRecord record) {

        if (!isLoggable(record)) {
            return;
        }

        int sys_severity = 0;
        Level loglevel;

        // Translate severity to syslog severity.  Note that the Java
        // log levels go in the opposite order of the way syslog does
        // it, and the user shouldn't care about that.

        loglevel = record.getLevel();

        if (loglevel == Level.SEVERE)
            sys_severity = SYSLOG_CRIT;
        else if (loglevel == Level.WARNING)
            sys_severity = SYSLOG_WARNING;
        else if (loglevel == Level.INFO)
            sys_severity = SYSLOG_INFO;
        else
            sys_severity = SYSLOG_DEBUG;


        // Here, we retrieve the localized message string from the
        // resource bundle, the name of which is passed in by this
        // method's caller.

        String detailedMsg = null; // Translation of `detailedKey'
        ResourceBundle clientResBundle = null;

        // Get the resource bundle from the name passed in
        clientResBundle = record.getResourceBundle();

        Object args[] = record.getParameters();
        String source = record.getMessage();
        detailedMsg = source;

        if (clientResBundle != null) {

            try {
                detailedMsg = MessageFormat.format(clientResBundle.getString(
                            source), args);
            } catch (Exception e) {
                // ignore
            }
        }

        detailedMsg = record.getSourceClassName() + "." +
            record.getSourceMethodName() + " : " + detailedMsg;

        /* Add a stack trace to the syslog record */

        /*
         * disabled, since this is too verbose for syslog. The trace
         * can be found in the debug log
         */


        doSyslog(detailedMsg, "cmasagent", sys_severity);
    }

    /**
     * flush the system : this method does nothing here
     */

    public void flush() {
    }

    /**
     * Close the <tt>Handler</tt> and free all associated resources.
     *
     * <p>The close method will perform a <tt>flush</tt> and then close the <tt>
     * Handler</tt>.   After close has been called this <tt>Handler</tt> should
     * no longer be used.  Method calls may either be silently ignored or may
     * throw runtime exceptions.
     *
     * @exception  SecurityException  if a security manager exists and if he
     * caller does not have <tt>LoggingPermission("control")</tt>.
     */
    public void close() throws SecurityException {

        open = false;
    }

    /**
     * Check if the <tt>SyslogHandler</tt> would actually log a given LogRecord.
     *
     * @param  record  a <tt>LogRecord</tt>
     *
     * @return  true if the <tt>LogRecord</tt> would be logged.
     */
    public boolean isLoggable(LogRecord record) {

        if (!open) {
            return false;
        }

        return super.isLoggable(record);
    }

}
