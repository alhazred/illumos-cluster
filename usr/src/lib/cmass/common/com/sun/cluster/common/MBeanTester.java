/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)MBeanTester.java 1.7     08/12/03 SMI"
 */

package com.sun.cluster.common;
import java.util.ArrayList;

public class MBeanTester {

    static final String LIB_DIR = "/usr/cluster/lib/cmass";
    static final String AGENT_PREFIX = "com.sun.cluster.agent.";

    // Each MBean should add its pacakge to this list
    static String mbeanPackages[] = {
	AGENT_PREFIX + "cluster",
	AGENT_PREFIX + "configaccess",
	AGENT_PREFIX + "dataservices.apache",
	AGENT_PREFIX + "dataservices.haoracle",
	AGENT_PREFIX + "dataservices.hastorageplus",
	AGENT_PREFIX + "dataservices.nfs",
	AGENT_PREFIX + "dataservices.oraclerac",
	AGENT_PREFIX + "dataservices.sapwebas",
	AGENT_PREFIX + "dataservices.sharedaddress",
	AGENT_PREFIX + "devicegroup",
	AGENT_PREFIX + "dpm",
	AGENT_PREFIX + "event",
	AGENT_PREFIX + "failovercontrol",
	AGENT_PREFIX + "ganymede",
	AGENT_PREFIX + "ipmpgroup",
	AGENT_PREFIX + "logquery",
	AGENT_PREFIX + "node",
	AGENT_PREFIX + "quorumdevice",
	AGENT_PREFIX + "rgm",
	AGENT_PREFIX + "event_mib",
	AGENT_PREFIX + "transport",
	AGENT_PREFIX + "utils"
    };

    static boolean debug = false;

    /**
     * Usage: MBeanTester [-n <n>][-q] <mbean-name> [mbean-args] ...
     *
     * @param args    -n <n>	repeat test <n> times
     *		    -q		quiet mode
     *		    -d		debug mode
     *
     *		    any unrecognized args are passed to the mbeans,
     *		    so that mbeans can have their own args in addition
     */
    public static void main(String args[]) {
	int iterations = 1;
	boolean quiet = false;

	dprintln("starting MBeanTester.main() ");
	for (int i = 0; i < args.length; i++)
	    dprintln("  arg["+i+"] = "+args[i]);

	int arg = 0;
	for (arg = 0; arg < args.length; arg++) {
	    if (args[arg].equals("-q")) {
		quiet = true;
	    } else if (args[arg].equals("-n")) {
		iterations = Integer.parseInt(args[++arg]);
	    } else if (args[arg].equals("-d")) {
		debug = true;
	    } else if (args[arg].startsWith("-")) {
		usage();
	    } else {
		break;
	    }
	}

	if (arg >= args.length) {
	    usage();
	}

	String mbean = args[arg++];
	ArrayList mbeanArgList = new ArrayList();
	while (arg < args.length) {
	    mbeanArgList.add(args[arg++]);
	}

	Class clazz = null;
	TestableMBean instance = null;
	boolean failed = false;
	long elapsed;
	long start = System.currentTimeMillis();

	try {
	    dprintln("trying " + mbean);
	    clazz = Class.forName(mbean);
	} catch (Exception e) {
	    dprintln("Exception: " + e.getMessage());
	    boolean found = false;
	    for (int j = 0; j < mbeanPackages.length; j++) {
		try {
		    dprintln("trying " + mbeanPackages[j] + "." + mbean);
		    clazz = null;
		    clazz = Class.forName(mbeanPackages[j] + "." + mbean);
		    if (clazz != null) {
			found = true;
			break;
		    }
		} catch (Exception e2) {
		    dprintln(e.getMessage());
		}
	    }

	    if (!found) {
		println("class " + mbean + " not found");
		System.exit(1);
	    }
	}

	try {
	    instance =  (TestableMBean)clazz.newInstance();
	} catch (Exception e) {
	    dprintln(e.getMessage());
	    println("Class.newInstance() failed!");
	    System.exit(1);
	}

	dprintln("created new instance of " + mbean);
	dprintln("class = " + instance.getClass().getName());

	dprintln("calling setQuiet() ");
	instance.setQuiet(quiet);
	instance.setDebug(debug);

	String mbargs[] = (String [])(mbeanArgList.toArray(new String[0]));
	for (int j = 0; j < iterations; j++) {
	    long istart = System.currentTimeMillis();
	    try {
		dprintln("calling " + mbean + ".test(): args: " + mbargs);
		instance.test(mbargs);
		dprintln("test() returned");
	    } catch (Exception e) {
		println(e.getMessage());
		println(mbean + " test failed");
		failed = true;
		break;
	    }
	    elapsed = System.currentTimeMillis() - istart;
	    println("iteration " + (j+1) + ": time="+elapsed+" ms\n");
	}

	elapsed = System.currentTimeMillis() - start;
	println("total time="+elapsed+" ms");

    }

    public static void println(String msg) {
	System.out.println(msg);
    }

    public static void dprintln(String msg) {
	if (debug)
	    System.out.println(msg);
    }

    private static void usage() {
	println("Usage: MBeanTester [-n <n>][-q] <mbean-name> [mbean-args]...");
	System.exit(1);
    }
}
