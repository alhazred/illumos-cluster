/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)TrustedRetryMBeanServerInvocationHandler.java 1.10     08/05/20 SMI"
 */

package com.sun.cluster.common;

// JDK
import com.sun.cacao.agent.JmxClient;

// Cacao
import com.sun.cacao.agent.MBeanServerInvocationHandler;

import java.io.IOException;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

// JMX
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;


/**
 * <p>{@link InvocationHandler} that forwards methods in an MBean's management
 * interface through the MBean server to the MBean.</p>
 *
 * <p>This {@link InvocationHandler} delegates to an
 * {@link MBeanServerInvocationHandler}, the difference being that this
 * invocation handler offers a single-shot retry if any operation on the proxy
 * fails with an IO exception.</p>
 *
 * <p>By wrapping the proxy's IO exception we are able to reconnect to the mbean
 * server if the connection has failed unexpectedly</p>
 */
public class TrustedRetryMBeanServerInvocationHandler
    implements InvocationHandler {

    // Instance variables
    private String agentLocation;
    private ObjectName objectName;
    private Class interfaceClass;
    private boolean isSnapshotProxy;
    private JMXConnector connector;
    private MBeanServerConnection mbsc;
    private MBeanServerInvocationHandler handler;

    /**
     * <p>Invocation handler that forwards methods through an MBean server. This
     * constructor may be called instead of relying on {@link #newProxyInstance}
     * , for instance if you need to supply a different {@link ClassLoader} to
     * {@link Proxy#newProxyInstance Proxy.newProxyInstance}.</p>
     *
     * @param  agentLocation  a <code>String</code> value
     * @param  objectName  the name of the MBean within the MBean server to
     * which methods will be forwarded.
     * @param  interfaceClass  the interface Class that we are reflecting
     * @param  isSnapshotProxy  true if this is a snapshotProxy invocation
     * handler
     */
    public TrustedRetryMBeanServerInvocationHandler(String agentLocation,
        ObjectName objectName, Class interfaceClass, boolean isSnapshotProxy)
        throws IOException {

        this.agentLocation = agentLocation;
        this.objectName = objectName;
        this.interfaceClass = interfaceClass;
        this.isSnapshotProxy = isSnapshotProxy;

        this.connector = TrustedMBeanModel.getWellKnownConnector(agentLocation);

        // Now get a reference to the mbean server connection
        this.mbsc = this.connector.getMBeanServerConnection();

        try {
            // XXX This could be removed for performance and replaced with
            // the fact that an UndeclaredThrowable is thrown when trying
            // to use the proxy if there is no underlying mbean - but the
            // current code everywhere expects an
            // IllegalArgumentException at proxy creation time when
            // the mbean does not exist

            if (!mbsc.isRegistered(objectName)) {
                throw new IllegalArgumentException(
                    "Cannot locate managed object " +
                    "named " + objectName);
            }
        } catch (IOException e) {
            // If we had a connection problem
            // try again

            this.connector.close();
            this.connector = TrustedMBeanModel.getWellKnownConnector(
                    agentLocation);

            // Now get a reference to the mbean server connection
            this.mbsc = this.connector.getMBeanServerConnection();


            // XXX This could be removed for performance and replaced with
            // the fact that an UndeclaredThrowable is thrown when trying
            // to use the proxy if there is no underlying mbean - but the
            // current code everywhere expects an
            // IllegalArgumentException at proxy creation time when
            // the mbean does not exist

            if (!mbsc.isRegistered(objectName)) {
                throw new IllegalArgumentException(
                    "Cannot locate managed object " +
                    "named " + objectName);
            }
        }

        this.handler = new MBeanServerInvocationHandler(mbsc, objectName,
                interfaceClass, isSnapshotProxy);
    }

    /**
     * <p>Invocation handler that forwards methods through an MBean server. This
     * constructor may be called instead of relying on {@link #newProxyInstance}
     * , for instance if you need to supply a different {@link ClassLoader} to
     * {@link Proxy#newProxyInstance Proxy.newProxyInstance}.</p>
     *
     * @param  connector  a <code>JMXConnector</code> value
     * @param  objectName  the name of the MBean within the MBean server to
     * which methods will be forwarded.
     * @param  interfaceClass  the interface Class that we are reflecting
     * @param  isSnapshotProxy  true if this is a snapshotProxy invocation
     * handler
     */
    public TrustedRetryMBeanServerInvocationHandler(JMXConnector connector,
        ObjectName objectName, Class interfaceClass, boolean isSnapshotProxy)
        throws IOException {

        this.connector = connector;
        this.objectName = objectName;
        this.interfaceClass = interfaceClass;
        this.isSnapshotProxy = isSnapshotProxy;

        // Now get a reference to the mbean server connection
        this.mbsc = this.connector.getMBeanServerConnection();

        try {
            // XXX This could be removed for performance and replaced with
            // the fact that an UndeclaredThrowable is thrown when trying
            // to use the proxy if there is no underlying mbean - but the
            // current code everywhere expects an
            // IllegalArgumentException at proxy creation time when
            // the mbean does not exist

            if (!mbsc.isRegistered(objectName)) {
                throw new IllegalArgumentException(
                    "Cannot locate managed object " +
                    "named " + objectName);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        this.handler = new MBeanServerInvocationHandler(mbsc, objectName,
                interfaceClass, isSnapshotProxy);
    }

    /**
     * <p>Return a proxy that implements the given interface by forwarding its
     * methods through the given MBean server to the named MBean, with or
     * without the 'snapshot' functionality</p>
     *
     * @param  agentLocation  a <code>String</code> value
     * @param  objectName  the name of the MBean within <code>connection</code>
     * to forward to.
     * @param  interfaceClass  the management interface that the MBean exports,
     * which will also be implemented by the returned proxy. via <code>
     * connection</code>.
     * @param  isSnapshotProxy  true if this is a snapshotProxy invocation
     * handler
     *
     * @return  the new proxy instance.
     */
    public static Object newProxyInstance(String agentLocation,
        ObjectName objectName, Class interfaceClass, boolean isSnapshotProxy)
        throws IOException {

        final InvocationHandler handler =
            new TrustedRetryMBeanServerInvocationHandler(agentLocation,
                objectName, interfaceClass, isSnapshotProxy);

        final Class interfaces[];

        interfaces = new Class[] { interfaceClass };

        return Proxy.newProxyInstance(interfaceClass.getClassLoader(),
                interfaces, handler);
    }

    /**
     * <p>Return a proxy that implements the given interface by forwarding its
     * methods through the given MBean server to the named MBean, with or
     * without the 'snapshot' functionality</p>
     *
     * @param  connector  a <code>JMXConnector</code> value
     * @param  objectName  the name of the MBean within <code>connection</code>
     * to forward to.
     * @param  interfaceClass  the management interface that the MBean exports,
     * which will also be implemented by the returned proxy. via <code>
     * connection</code>.
     * @param  isSnapshotProxy  true if this is a snapshotProxy invocation
     * handler
     *
     * @return  the new proxy instance.
     */
    public static Object newProxyInstance(JMXConnector connector,
        ObjectName objectName, Class interfaceClass, boolean isSnapshotProxy)
        throws IOException {

        final InvocationHandler handler =
            new TrustedRetryMBeanServerInvocationHandler(connector, objectName,
                interfaceClass, isSnapshotProxy);

        final Class interfaces[];

        interfaces = new Class[] { interfaceClass };

        return Proxy.newProxyInstance(interfaceClass.getClassLoader(),
                interfaces, handler);
    }


    /**
     * Called by the Proxy mechanism when invoking any method on the interface
     *
     * @param  proxy  the proxy object we are invoking the method on
     * @param  method  a <code>Method</code> value
     * @param  args  an <code>Object[]</code> value containing method args
     *
     * @return  an <code>Object</code> value
     *
     * @exception  Throwable  if an error occurs
     */
    public synchronized Object invoke(Object proxy, Method method,
        Object args[]) throws Throwable {

        try {
            return handler.invoke(proxy, method, args);
        } catch (IOException e) {

            // If we had a connection problem, create a new proxy
            // and try again

            this.connector.close();
            this.connector = TrustedMBeanModel.getWellKnownConnector(
                    agentLocation);

            // Now get a reference to the mbean server connection
            this.mbsc = this.connector.getMBeanServerConnection();

            this.handler = new MBeanServerInvocationHandler(mbsc, objectName,
                    interfaceClass, isSnapshotProxy);

            return handler.invoke(proxy, method, args);

            // If this second attempt throws an exception, bail out
        }
    }
}
