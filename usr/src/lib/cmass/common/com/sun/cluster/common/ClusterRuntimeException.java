/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ClusterRuntimeException.java 1.6     08/05/20 SMI"
 */

package com.sun.cluster.common;

/**
 * Superclass for all Cluster Runtime exceptions
 *
 * @author  nstephen
 */
public class ClusterRuntimeException extends RuntimeException
    implements java.io.Serializable {

    /**
     * Creates a new instance of <code>ClusterRuntimeException</code> without
     * detail message.
     */
    public ClusterRuntimeException() {
        super();
    }

    /**
     * Constructs an instance of <code>ClusterRuntimeException</code> with the
     * specified detail message.
     *
     * @param  msg  the detail message.
     */
    public ClusterRuntimeException(String msg) {
        super(msg);
    }
}
