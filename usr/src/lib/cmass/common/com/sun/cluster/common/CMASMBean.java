/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)CMASMBean.java 1.5     08/05/20 SMI"
 */
package com.sun.cluster.common;

//
// Base class for all CMASMbeans
//
public class CMASMBean implements TestableMBean {

    static protected final String LIB_DIR = "/usr/cluster/lib/cmass";
    private boolean quiet = false;
    private boolean debug = false;

    // Every MBean must override this method.
    public void test(String args[]) throws Error {
	println("mbean failed to implement test() method!");
    }

    public final void setQuiet(boolean on) {
	quiet = on;
    }

    public final void setDebug(boolean on) {
	debug = on;
    }

    // Load the agent JNI libraries
    protected void loadLib(String lib) throws UnsatisfiedLinkError {
	dprintln("attmpting to load "+ lib);
	lib = LIB_DIR + "/" + lib;
	try {
	    Runtime.getRuntime().load(lib);
	} catch (UnsatisfiedLinkError ule) {
	    println("load of "+ lib +" failed!");
	    ule.printStackTrace();
	    throw ule;
	}
	dprintln("load of "+ lib + " succeeded");
    }

    protected void print(String msg) {
	if (!quiet) {
	    System.out.print(msg);
	}
	// XXX: maybe write msg to a log file as well?
    }

    protected void println(String msg) {
	if (!quiet) {
	    System.out.println(msg);
	}
	// XXX: maybe write msg to a log file as well?
    }

    protected void dprint(String msg) {
	if (debug) {
	    System.out.print(msg);
	}
    }

    protected void dprintln(String msg) {
	if (debug) {
	    System.out.println(msg);
	}
    }
}
