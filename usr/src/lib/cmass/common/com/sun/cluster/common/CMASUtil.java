/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * pragma ident	"@(#)CMASUtil.java	1.2	08/06/27 SMI"
 */

package com.sun.cluster.common;

public class CMASUtil {
    public static final String COLON = ":";

    public static String wildCardToRegix(String wild) {
        if (wild == null) {
            return null;
        }

        StringBuffer buffer = new StringBuffer();

        char [] chars = wild.toCharArray();

        for (int i = 0; i < chars.length; ++i)
        {
            if (chars[i] == '*') {
                buffer.append(".*");
            } else if (chars[i] == '?') {
                buffer.append(".");
            } else if ("+()$.{}|\\".indexOf(chars[i]) != -1) {
                buffer.append('\\').append(chars[i]);
            } else {
                buffer.append(chars[i]);
            }
        }
        return buffer.toString();
    }
}
