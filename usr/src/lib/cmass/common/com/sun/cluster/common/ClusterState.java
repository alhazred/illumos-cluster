/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ClusterState.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.common;

/**
 * Common class to provide access to the cluster states
 *
 * @author  Bharathi Subramanian
 */

public class ClusterState {

    /**
     * This field is used to indicate the Online state of a cluster component.
     */
    public static final int CLSTATE_ONLINE = 0;

    /**
     * This field is used to indicate the Offline state of a cluster component.
     */
    public static final int CLSTATE_OFFLINE = 1;

    /**
     * This field is used to indicate the Faulted state of a cluster component.
     */
    public static final int CLSTATE_FAULTED = 2;

    /**
     * This field is used to indicate the Degraded state of a cluster component.
     */
    public static final int CLSTATE_DEGRADED = 3;

    /** This field is used to indicate the Wait state of a cluster component. */
    public static final int CLSTATE_WAIT = 4;

    /**
     * This field is used to indicate the Enabled state of a cluster component.
     */
    public static final int CLSTATE_ENABLED = 5;

    /**
     * This field is used to indicate the Disabled state of a cluster component.
     */
    public static final int CLSTATE_DISABLED = 6;

    /**
     * This field is used to indicate that the state of the cluster component is
     * unknown.
     */
    public static final int CLSTATE_UNKNOWN = 7;

    /**
     * This field is used to indicate that the cluster component is not
     * monitored.
     */
    public static final int CLSTATE_NOTMONITORED = 8;

    /**
     * This field is used to indicate that the cluster component is monitored.
     */
    public static final int CLSTATE_MONITORED = 9;

    /** This field is used to indicate that the cluster component is managed. */
    public static final int CLSTATE_MANAGED = 10;

    /**
     * This field is used to indicate that the cluster component is not managed.
     */
    public static final int CLSTATE_UNMANAGED = 11;

    /**
     * This field is used to indicate that the cluster component is in Stop
     * Failed state.
     */
    public static final int CLSTATE_STOPFAILED = 12;

    /**
     * This field is used to indicate that the cluster component is in Suspended
     * state.
     */
    public static final int CLSTATE_SUSPENDED = 13;

    /**
     * This field is used to indicate that the cluster component is not in
     * Suspended state.
     */
    public static final int CLSTATE_NOTSUSPENDED = 14;


}
