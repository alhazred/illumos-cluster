/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ClusterPaths.java	1.13	08/07/14 SMI"
 */

package com.sun.cluster.common;

/**
 * Common class to provide access to cluster paths.
 */

public class ClusterPaths {
    public static final String bin = "/usr/cluster/bin/";
    public static final String lib = "/usr/cluster/lib/";
    public static final String cmass = lib + "cmass/";

    /* Commands */
    public static final String CHMOD_CMD = "/usr/bin/chmod";
    public static final String CHOWN_CMD = "/usr/bin/chown";
    public static final String RM_CMD = "/usr/bin/rm";
    public static final String NEWTASK_CMD = "/usr/bin/newtask";
    public static final String ZONEADM_CMD = "/usr/sbin/zoneadm";
    public static final String ZLOGIN_CMD = "/usr/sbin/zlogin";

    /* Sun Cluster Commands */
    public static final String CL_DG_CMD = bin + "cldevicegroup";
    public static final String CL_NAS_CMD = bin + "clnasdevice";
    public static final String CL_DEVICE_CMD = bin + "cldevice";
    public static final String CL_NODE_CMD = bin + "clnode";
    public static final String CL_QUORUM_CMD = bin + "clquorum";
    public static final String CL_RG_CMD = bin + "clresourcegroup";
    public static final String CL_RES_CMD = bin + "clresource";
    public static final String CL_RT_CMD = bin + "clresourcetype";
    public static final String CL_RES_SA_CMD = bin + "clressharedaddress";
    public static final String CL_RES_LH_CMD = bin + "clreslogicalhostname";
    public static final String SCRGADM_RTR_DIR = lib + "rgm/rtreg";
    public static final String SCRGADM_OPT_RTR_DIR =
        "/opt/cluster/lib/rgm/rtreg";
    public static final String CL_SNMP_MIB_CMD = bin + "clsnmpmib";
    public static final String CL_SNMP_HOST_CMD = bin + "clsnmphost";
    public static final String CL_SNMP_USER_CMD = bin + "clsnmpuser";
    public static final String SCEVENTMIB_CMD = bin + "sceventmib";
    public static final String SNMP_MIB_PROP_FILE =
        "/etc/opt/SUNWcacao/modules/agent_snmp_events_mib.properties";
    public static final String CL_INTR_CMD = bin + "clinterconnect";
    public static final String CL_TELEMETRY_ATTR_CMD = bin +
        "cltelemetryattribute";

    public static final String ZC_OPTION = "-Z";

    /* private command */
    public static final String SCSLM_GETSCHED_CMD = lib + "sc/scslm_getsched";
    public static final String ZC_GETPROJECTNAMES_CMD = lib + "sc/zc_getProjectNames";

    /* Service Level Management Commands (currently old-style) */
    public static final String SC_SLM_THRESH_CMD = bin + "scslmthresh";
}
