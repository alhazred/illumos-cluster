/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)jniutil.h	1.7	08/05/21 SMI"
 */

#ifndef __JNIUTIL_H__
#define	__JNIUTIL_H__

#pragma ident	"@(#)jniutil.h	1.7	08/05/21 SMI"

#include <jni.h>
#include <scha.h>
#include <cl_query/cl_query_types.h>

/* helper function to throw an exception of a given type */
void throwByName(JNIEnv *env, const char *name, const char *msg);

/* helper function to delete a local reference if it is held */
void delRef(JNIEnv *env, void *ref);

/* Helper function for creating an object (constructor takes no params) */
jobject newObjNullParam(JNIEnv *env, const char *classname);


/* Helper function for creating a Boolean (non-zero param = TRUE) */
jobject newBoolean(JNIEnv *env, int value);

/* Helper function for creating an object (constructor takes string param) */
jobject newObjStringParam(JNIEnv *env,
			    const char *classname,
			    const char *param);

/* Helper function for creating an object (constructor takes int32_t param) */
jobject newObjInt32Param(JNIEnv *env,
			    const char *classname,
			    const int32_t param);

/* Helper function for creating an object (constructor takes int64_t param) */
jobject newObjInt64Param(JNIEnv *env,
			    const char *classname,
			    const int64_t param);

/* Helper function for creating a String array */
jobjectArray newStringArray(JNIEnv *env,
			    int array_len,
			    char **array_val);

/* Helper function to retrieve a string field value in a java object */
char* getFieldStringValue(JNIEnv *env, jobject this, const char* field_name);

/* Helper function to retrieve get a CMASS Enum */
jobject getCMASSEnum(JNIEnv *env,
		    const char *client_enum_class,
		    int enum_value);

/* free scha_str_array_t *list */
void strarr_free(scha_str_array_t *str_arr_list);

/*
 * helper functions to convert a cl_query_list_t
 * into a Java string array
 */
jobjectArray getClQueryList(JNIEnv *env,
			    cl_query_list_t *list);

void setObjectField(JNIEnv *env, jobject obj, jfieldID fId, jobject value);

void setStringField(JNIEnv *env, jobject obj, jfieldID fId, char *value);

void setIntField(JNIEnv *env, jobject obj, jfieldID fId, jint value);

void setLongField(JNIEnv *env, jobject obj, jfieldID fId, jlong value);

void setBooleanField(JNIEnv *env, jobject obj, jfieldID fId, jboolean value);

#endif /* __JNIUTIL_H__ */
