/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sysloghandler_jni.c	1.4	08/05/20 SMI"

#include <jni.h>
#include <syslog.h>

#include "com/sun/cluster/syslogger/SyslogHandler.h"

JNIEXPORT void
    JNICALL
    Java_com_sun_cluster_syslogger_SyslogHandler_doSyslog(
    JNIEnv * env,
    jclass class_object,
    jstring mesg,
    jstring identity,
    jint severity)
{
	const char *message_string;
	const char *identity_string;

	message_string = (*env)->GetStringUTFChars(env, mesg, NULL);
	identity_string = (*env)->GetStringUTFChars(env, identity, NULL);

	openlog(identity_string, LOG_PID, LOG_DAEMON);
	syslog(severity, message_string);
	closelog();

	(*env)->ReleaseStringUTFChars(env, mesg, message_string);
	(*env)->ReleaseStringUTFChars(env, identity, identity_string);



} /*lint !e715 */
