/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)jniutil.c 1.8     08/05/21 SMI"
 */

#pragma ident	"@(#)rgm_errmsg.cc	1.53	03/04/15 SMI"

#include <jniutil.h>
#include <stdlib.h>



/*
 * helper function to throw an exception of a given type
 */
void
    throwByName(JNIEnv * env, const char *name, const char *msg)
{

	jclass cls = (*env)->FindClass(env, name);
	/*
	 * if cls is NULL, and exception has already been thrown
	 */
	if (cls != NULL) {
		(*env)->ThrowNew(env, cls, msg);
	}
	delRef(env, cls);
}

/*
 * helper function to delete a local reference if it is held
 */
void
    delRef(JNIEnv * env, void *ref)
{



	if (ref != NULL) {

		(*env)->DeleteLocalRef(env, ref);
	}
}

/*
 * Helper function for creating an object (constructor takes no params)
 */
jobject
    newObjNullParam(JNIEnv * env, const char *classname)
{
	jclass objClass = NULL;
	jmethodID cid = NULL;
	jobject result = NULL;


	/*
	 * get local ref to class
	 */
	objClass = (*env)->FindClass(env, classname);
	if (objClass == NULL)
		return (NULL);

	/*
	 * get constructor
	 */
	cid = (*env)->GetMethodID(env, objClass, "<init>", "()V");

	if (cid != NULL) {
		result = (*env)->NewObject(env, objClass, cid);
	}
	/*
	 * delete local refs
	 */
	delRef(env, objClass);

	return (result);
}

/*
 * Helper function for creating a Boolean object
 */
jobject
    newBoolean(JNIEnv * env, int value)
{
	jclass objClass = NULL;
	jmethodID cid = NULL;
	jobject result = NULL;



	/*
	 * get local ref to class
	 */
	objClass = (*env)->FindClass(env, "java/lang/Boolean");
	if (objClass == NULL)
		return (NULL);

	/*
	 * get constructor
	 */
	cid = (*env)->GetMethodID(env, objClass, "<init>", "(Z)V");

	if (cid != NULL) {
		result = (*env)->NewObject(env,
		    objClass,
		    cid,
		    (value ? JNI_TRUE : JNI_FALSE));
	}
	/*
	 * delete local refs
	 */
	delRef(env, objClass);

	return (result);
}

/*
 * Helper function for creating an object (constructor takes string param)
 */
jobject
    newObjStringParam(JNIEnv * env,
    const char *classname,
    const char *param)
{
	jclass objClass = NULL;
	jmethodID cid = NULL;
	jstring strparam = NULL;
	jobject result = NULL;


	objClass = (*env)->FindClass(env, classname);
	if (objClass == NULL)
		return (NULL);

	/*
	 * get constructor
	 */
	cid = (*env)->GetMethodID(env, objClass, "<init>",
	    "(Ljava/lang/String;)V");

	if (cid != NULL) {
		/*
		 * Create our string parameter object
		 */
		strparam = (*env)->NewStringUTF(env, param);

		if (strparam != NULL) {

			/*
			 * Create the object
			 */
			result = (*env)->NewObject(env, objClass,
			    cid, strparam);
		}
	}
	/*
	 * Delete local refs
	 */
	delRef(env, objClass);
	delRef(env, strparam);

	return (result);
}

/*
 * Helper function for creating an object (constructor takes int32 param)
 */
jobject
    newObjInt32Param(JNIEnv * env,
    const char *classname,
    const int32_t param)
{
	jclass objClass = NULL;
	jmethodID cid = NULL;
	jobject result = NULL;



	objClass = (*env)->FindClass(env, classname);
	if (objClass == NULL)
		return (NULL);

	/*
	 * get constructor
	 */
	cid = (*env)->GetMethodID(env, objClass, "<init>", "(I)V");

	if (cid != NULL) {
		/*
		 * Create the object
		 */
		result = (*env)->NewObject(env, objClass, cid, param);
	}
	/*
	 * Delete local refs
	 */
	delRef(env, objClass);

	return (result);
}

/*
 * Helper function for creating an object (constructor takes int64 param)
 */
jobject
    newObjInt64Param(JNIEnv * env,
    const char *classname,
    const int64_t param)
{
	jclass objClass = NULL;
	jmethodID cid = NULL;
	jobject result = NULL;

	objClass = (*env)->FindClass(env, classname);
	if (objClass == NULL)
		return (NULL);

	/*
	 * get constructor
	 */
	cid = (*env)->GetMethodID(env, objClass, "<init>", "(J)V");

	if (cid != NULL) {
		/*
		 * Create the object
		 */
		result = (*env)->NewObject(env, objClass, cid, (jlong) param);
	}
	/*
	 * Delete local refs
	 */
	delRef(env, objClass);

	return (result);
}

/*
 * Helper function for creating a String array
 */
jobjectArray
    newStringArray(JNIEnv * env,
    int array_len,
    char **array_val)
{
	int i;
	jclass stringClass = NULL;
	jstring arrayElement = NULL;
	jobjectArray result = NULL;



	/*
	 * Load String class
	 */
	stringClass = (*env)->FindClass(env, "java/lang/String");
	if (stringClass == NULL)
		return (NULL);

	/*
	 * Construct array
	 */
	result = (*env)->NewObjectArray(env, array_len, stringClass, NULL);
	if (result == NULL)
		return (NULL);

	/*
	 * Fill array
	 */
	for (i = 0; i < array_len; i++) {
		arrayElement = (*env)->NewStringUTF(env, array_val[i]);
		if (arrayElement != NULL) {
			(*env)->SetObjectArrayElement(env,
			    result, i, arrayElement);
			(*env)->DeleteLocalRef(env, arrayElement);
		}
	}

	return (result);
}

void
setObjectField(JNIEnv *env, jobject obj, jfieldID fId, jobject value) {
	(*env)->SetObjectField(env, obj, fId, value);
}

void
setStringField(JNIEnv *env, jobject obj, jfieldID fId, char *value) {
	(*env)->SetObjectField(env, obj, fId,
		(*env)->NewStringUTF(env, (char *)value));
}

void
setIntField(JNIEnv *env, jobject obj, jfieldID fId, jint value) {
	(*env)->SetIntField(env, obj, fId, (jint)value);
}

void
setLongField(JNIEnv *env, jobject obj, jfieldID fId, jlong value) {
	(*env)->SetLongField(env, obj, fId, (jlong)value);
}

void
setBooleanField(JNIEnv *env, jobject obj, jfieldID fId, jboolean value) {
	(*env)->SetBooleanField(env, obj, fId, (jboolean)value);
}

#ifdef REALLY_USED

/*
 * Helper function to retrieve a string field value in a java object
 */

char *
    getFieldStringValue(JNIEnv * env, jobject this, const char *field_name)
{
	jfieldID fid;
	jstring jstr;
	char *instance_name;

	jclass module_class = (*env)->GetObjectClass(env, this);

	fid = (*env)->GetFieldID(env, module_class, field_name,
	    "Ljava/lang/String;");

	if (fid == NULL) {
		(*env)->DeleteLocalRef(env, module_class);
		return (NULL);
	}
	jstr = (*env)->GetObjectField(env, this, fid);
	instance_name = (char *)(*env)->GetStringUTFChars(env, jstr, NULL);

	(*env)->DeleteLocalRef(env, jstr);
	(*env)->DeleteLocalRef(env, module_class);

	return (instance_name);
}
#endif
/*
 * Helper function to retrieve get a CMASS Enum
 */
jobject
    getCMASSEnum(JNIEnv * env, const char *client_enum_class,
    int enum_value)
{

	/*
	 * Get the CMASS Enum class
	 */
	jclass enum_class;
	jmethodID enum_get_enum;
	jobject return_value;
	jclass cenum_class;

	/*
	 * Get the CMASS Enum class
	 */
	enum_class = (*env)->FindClass(env,
	    "com/sun/cacao/Enum");
	if (enum_class == NULL)
		return (NULL);
	/*
	 * get 'getEnum' static method
	 */
	enum_get_enum = (*env)->GetStaticMethodID(env, enum_class,
	    "getEnum",
	    "(Ljava/lang/Class;I)Lcom/sun/cacao/Enum;");
	if (enum_get_enum == NULL)
		return (NULL);

	/*
	 * Get the client's Enum class
	 */
	cenum_class = (*env)->FindClass(env, client_enum_class);
	if (cenum_class == NULL)
		return (NULL);

	/*
	 * Call the static method
	 */
	return_value =
	    (*env)->CallStaticObjectMethod(env, enum_class, enum_get_enum,
	    cenum_class, enum_value);
	return (return_value);
}

/*
 * free scha_str_array_t *list
 */
void
    strarr_free(scha_str_array_t *str_arr_list)
{
	int i = 0;
	if (str_arr_list == NULL)
		return;

	if (str_arr_list->str_array == NULL)
		return;

	for (i = 0; str_arr_list->str_array[i] != NULL; i++)
		free(str_arr_list->str_array[i]);
	free(str_arr_list->str_array);
	free(str_arr_list);
}

/*
 * helper functions to convert a cl_query_list_t
 * into a Java string array
 */
jobjectArray
    getClQueryList(JNIEnv * env,
    cl_query_list_t *list)
{

	return (newStringArray(env, (int)list->list_len, list->list_val));
}
