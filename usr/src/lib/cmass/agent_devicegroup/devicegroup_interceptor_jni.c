/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Devicegroup mbean interceptor
 */

#pragma ident	"@(#)devicegroup_interceptor_jni.c	1.9	08/05/20 SMI"

#include "com/sun/cluster/agent/devicegroup/DeviceGroupInterceptor.h"

#include <jniutil.h>
#include <stdio.h>
#include <string.h>
#include <cl_query/cl_query_types.h>


/*
 * A couple of helper macros for things we do a lot,
 * they contain references to local variables in the
 * function below
 */
#define	PUT_ATTR(MAP, NAME, VALUE)		\
	PUT(MAP, NAME, (*env)->NewObject(env,	\
	    attribute_class,			\
	    attribute_constructor,		\
	    (*env)->NewStringUTF(env, NAME),	\
	    VALUE))

#define	PUT(MAP, NAME, VALUE)						\
	(*env)->CallObjectMethod(env,					\
				    MAP,				\
				    map_put,				\
				    (*env)->NewStringUTF(env, NAME),	\
				    VALUE)


/*
 * Multi-owner_SVM is now supported.
 * As the list of possible device type is not freezed
 * we perform the check against well knowned not allowed type
 */
static int d_is_valid_type(unsigned char *type) {
	unsigned char hash_flag;

	if (type == NULL) {
		return (0);
	}

	hash_flag = type[0];

	switch (hash_flag) {
	case 'M':
	{
		if (strcmp((const char *)type, "Multi-owner_SVM") == 0)
			return (1);
		break;
	}
	default:
		return (1);
	}
	return (0);
}

/*
 * Class:     com_sun_cluster_agent_devicegroup_DeviceGroupInterceptor
 * Method:    fillCache
 * Signature: ()Ljava/util/Map;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_devicegroup_DeviceGroupInterceptor_fillCache
(JNIEnv * env, jobject this) {

	/*
	 * return a Map of Maps containing all instances
	 * key = instanceName
	 * value = Map
	 * key=attributeName
	 * value=corresponding Attribute object
	 */

	jobject cache_map = NULL;
	jobject instance_map = NULL;
	jclass map_class = NULL;
	jmethodID map_put;

	jclass attribute_class = NULL;
	jmethodID attribute_constructor = NULL;

	jobject mounted_filesystem_map = NULL;
	jobject mount_point_map = NULL;
	jclass string_class = NULL;

	jclass cl_state_class = NULL;
	jmethodID cl_init_method;
	jobject   dg_status   = NULL;
	jint dg_status_code;

	int i;

	cl_query_error_t clquery_retval = CL_QUERY_ECOMM;
	cl_query_result_t result;
	cl_query_device_info_t *devicegroup = NULL;

	/*
	 * get our references to 'attribute_class'
	 */

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * get constructor
	 */
	attribute_constructor =
	    (*env)->GetMethodID(env,
	    attribute_class,
	    "<init>", "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;


	/*
	 * Create the map we are going to return
	 */
	cache_map = newObjNullParam(env, "java/util/HashMap");
	if (cache_map == NULL)
		goto error;

	/*
	 * get our references to map_class and the put method
	 */
	map_class = (*env)->FindClass(env, "java/util/Map");
	if (map_class == NULL)
		goto error;
	map_put =
	    (*env)->GetMethodID(env,
	    map_class,
	    "put",
	    "(Ljava/lang/Object;Ljava/lang/Object;)" "Ljava/lang/Object;");
	if (map_put == NULL)
		goto error;

	/*
	 * Get a reference to string class needed for array creation later
	 */
	string_class = (*env)->FindClass(env, "java/lang/String");
	if (string_class == NULL)
		goto error;

	/*
	 * Call SCHA_API to get all info
	 */
	clquery_retval = cl_query_get_info(CL_QUERY_DEVICE_INFO, NULL, &result);
	if (clquery_retval != CL_QUERY_OK)
		goto error;

	for (devicegroup = (cl_query_device_info_t *)result.value_list;
	    devicegroup != NULL; devicegroup = devicegroup->next) {

		/*
		 * we are going to create a map for this instance and
		 * populate it with its attributes
		 */

		instance_map = newObjNullParam(env, "java/util/HashMap");
		if (instance_map == NULL)
			goto error;

		PUT_ATTR(instance_map,
		    "Name", (*env)->NewStringUTF(env, (char *)
			devicegroup->device_name));

		dg_status_code = devicegroup->status;

		cl_state_class = (*env)->FindClass(env, "java/lang/Integer");

		if (cl_state_class == NULL)
			goto error;

		cl_init_method = (*env)->GetMethodID(env, cl_state_class,
							"<init>", "(I)V");

		if (cl_init_method == NULL)
			goto error;

		dg_status = (*env)->NewObject(env,
						cl_state_class,
						cl_init_method,
						dg_status_code);

		PUT_ATTR(instance_map,
			"DeviceGroupStatus",
			dg_status);

		PUT_ATTR(instance_map,
			"Online",
			newBoolean(env, (
			(dg_status_code == CL_QUERY_ONLINE) ||
			(dg_status_code == CL_QUERY_DEGRADED))));

		if (d_is_valid_type((unsigned char*)devicegroup->device_type)) {
			PUT_ATTR(instance_map,
			    "Type",
			    (*env)->NewStringUTF(env,
				(char *)devicegroup->device_type));
		}

		if (devicegroup->primary_node_name == NULL) {
			PUT_ATTR(instance_map, "PrimaryNodeName",
				(*env)->NewStringUTF(env, ""));
		} else {
			PUT_ATTR(instance_map, "PrimaryNodeName",
				(*env)->NewStringUTF(env, (char *)
					devicegroup->primary_node_name));
		}

		if (devicegroup->replication_type == NULL) {
			PUT_ATTR(instance_map, "ReplicationType",
				(*env)->NewStringUTF(env, ""));
		} else {
			PUT_ATTR(instance_map, "ReplicationType",
				(*env)->NewStringUTF(env, (char *)
				devicegroup->replication_type));
		}

		PUT_ATTR(instance_map,
		    "SecondaryNodeNames",
		    getClQueryList(env, &devicegroup->alternate_nodes));
		PUT_ATTR(instance_map,
		    "SpareNodeNames",
		    getClQueryList(env, &devicegroup->spare_nodes));


		{
			/*
			 * Fill out an array with node names
			 */
			jobjectArray array = NULL;

			/*
			 * Construct array
			 */
			array = (*env)->NewObjectArray(env,
			    (int)devicegroup->file_systems_path.list_len,
			    string_class, NULL);
			if (array == NULL)
				goto error;

			/*
			 * populate the array
			 */
			for (i = 0;
			    i < (int)devicegroup->file_systems_path.list_len;
			    i++) {

				(*env)->SetObjectArrayElement(env,
				    array, i, (*env)->NewStringUTF(env, (char *)
					(devicegroup->file_systems_path).
					list_val[i].node_name));
			}
			PUT_ATTR(instance_map, "ConnectedNodeOrder", array);
		}

		PUT_ATTR(instance_map,
		    "FailbackEnabled",
		    newBoolean(env,
			(int)(devicegroup->failback == CL_QUERY_ENABLED)));

		PUT_ATTR(instance_map,
		    "ConnectedNodeOrderEnabled",
		    newBoolean(env,
			(devicegroup->OrderedNodesList == CL_QUERY_ENABLED)));
		PUT_ATTR(instance_map,
		    "SecondariesCount",
		    newObjInt32Param(env,
			"java/lang/Integer",
			(int)devicegroup->avail_alternate_nodes));

		{
			/*
			 * Remove a trailing 'sxxx' (slice) from the path
			 * if we find one after the last '/'
			 */
			char *ptr = strrchr(devicegroup->logical_disk_path,
			    '/');
			if (ptr != NULL) {
				ptr = strchr(ptr, 's');
				if (ptr != NULL) {
					*ptr = 0;
				}
			}
			PUT_ATTR(instance_map,
			    "LogicalDiskPath",
			    (*env)->NewStringUTF(env, (char *)
				devicegroup->logical_disk_path));
		}

		/*
		 * For MountedFilesystemPath we need a map of node -> path
		 */
		mounted_filesystem_map = newObjNullParam(env,
		    "java/util/HashMap");
		if (mounted_filesystem_map == NULL)
			goto error;
		for (i = 0;
		    i < (int)devicegroup->file_systems_path.list_len; i++) {
			/*
			 * Skip over entries with a null file_systems_path
			 */
			if ((devicegroup->file_systems_path).
			    list_val[i].phys_path_name != NULL) {
				PUT(mounted_filesystem_map,
				    (devicegroup->file_systems_path).
				    list_val[i].node_name,
				    (*env)->NewStringUTF(env, (char *)
					(devicegroup->file_systems_path).
					list_val[i].phys_path_name));
			}
		}
		PUT_ATTR(instance_map,
		    "MountedFilesystemPath", mounted_filesystem_map);

		/*
		 * For mount point path we need a map of device -> mnt point
		 */
		mount_point_map = newObjNullParam(env, "java/util/HashMap");
		if (mount_point_map == NULL)
			goto error;
		for (i = 0;
		    i < (int)devicegroup->file_systems_path.list_len; i++) {
			/*
			 * Skip over entries with a null file_systems_path
			 * and null mount points
			 */
			if (((devicegroup->file_systems_path).
				list_val[i].phys_path_name != NULL) &&
			    ((devicegroup->file_systems_path).
				list_val[i].mount_point != NULL)) {
				PUT(mount_point_map,
				    (devicegroup->file_systems_path).
				    list_val[i].phys_path_name,
				    (*env)->NewStringUTF(env, (char *)
					(devicegroup->file_systems_path).
					list_val[i].mount_point));
			}
		}



		PUT_ATTR(instance_map, "MountPointPath", mount_point_map);

		/*
		 * Now put map into the returned map under this instance
		 */

		PUT(cache_map, devicegroup->device_name, instance_map);
	}


    error:

	if (clquery_retval == CL_QUERY_OK)
		(void) cl_query_free_result(CL_QUERY_DEVICE_INFO, &result);

	return (cache_map);
} /*lint !e715 */
