/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Devicegroup mbean interceptor
 */

#pragma ident	"@(#)nasdevice_interceptor_jni.c	1.12	08/05/20 SMI"

#include "com/sun/cluster/agent/devicegroup/NasDeviceInterceptor.h"

#include <jniutil.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <cl_query/cl_query_types.h>
#include "scnas.h"

#define	NAS_TYPE "NAS"
#define	NAS_KEY_TABLE "NAS_Service_keys"
#define	NAS_TABLE_PREFIX "NAS_Service_"
#define	NAS_TABLE_PREFIX_LEN 12 /* Length of NAS_TABLE_PREFIX string */
#define	DEVICE_NAME "NAS_ServiceName"
#define	DEVICE_TYPE "NAS_ServiceType"
#define	LOGIN_NAME "NAS_UserName"
#define	PASSWD "NAS_Passwd"
#define	DIR_PREFIX "NAS_Directories_"


/*
 * A couple of helper macros for things we do a lot,
 * they contain references to local variables in the
 * function below
 */
#define	PUT_ATTR(MAP, NAME, VALUE)		\
	PUT(MAP, NAME, (*env)->NewObject(env,	\
	    attribute_class,			\
	    attribute_constructor,		\
	    (*env)->NewStringUTF(env, NAME),	\
	    VALUE))

#define	PUT(MAP, NAME, VALUE)						\
	(*env)->CallObjectMethod(env,					\
				    MAP,				\
				    map_put,				\
				    (*env)->NewStringUTF(env, NAME),	\
				    VALUE)
/*
 * Class:     com_sun_cluster_agent_devicegroup_DeviceGroupInterceptor
 * Method:    fillCache
 * Signature: ()Ljava/util/Map;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_devicegroup_NasDeviceInterceptor_fillCache
(JNIEnv * env, jobject this) {

	/*
	 * return a Map of Maps containing all instances
	 * key = instanceName
	 * value = Map
	 * key=attributeName
	 * value=corresponding Attribute object
	 */

	jobject cache_map = NULL;
	jobject instance_map = NULL;
	jclass map_class = NULL;
	jmethodID map_put;

	jclass attribute_class = NULL;
	jmethodID attribute_constructor = NULL;

	jclass string_class = NULL;

	cl_query_error_t clquery_retval = CL_QUERY_ECOMM;

	cl_query_name_t tableName;
	cl_query_table_t keytableContents;
	cl_query_table_t filertableContents;
	int i, j, k, count;
	jobjectArray array = NULL;

	/*
	 * get our references to 'attribute_class'
	 */

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * get constructor
	 */
	attribute_constructor =
	    (*env)->GetMethodID(env,
	    attribute_class,
	    "<init>", "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;


	/*
	 * Create the map we are going to return
	 */
	cache_map = newObjNullParam(env, "java/util/HashMap");
	if (cache_map == NULL)
		goto error;

	/*
	 * get our references to map_class and the put method
	 */
	map_class = (*env)->FindClass(env, "java/util/Map");
	if (map_class == NULL)
		goto error;
	map_put =
	    (*env)->GetMethodID(env,
	    map_class,
	    "put",
	    "(Ljava/lang/Object;Ljava/lang/Object;)" "Ljava/lang/Object;");
	if (map_put == NULL)
		goto error;

	/*
	 * Get a reference to string class needed for array creation later
	 */
	string_class = (*env)->FindClass(env, "java/lang/String");
	if (string_class == NULL)
		goto error;

	/*
	 * Call SCHA_API to get all info
	 */

	clquery_retval = cl_query_read_table(NAS_KEY_TABLE, &keytableContents);
	if (clquery_retval != CL_QUERY_OK)
		goto error;
	for (i = 0; i < (int)keytableContents.n_rows; i++) {
		char *key, *value;
		char *filerName = NULL;
		int mem_err;

		key = keytableContents.table_rows[i]->key;
		value = keytableContents.table_rows[i]->value;

		/* Get the NAS CCR table name */
		tableName = (char *)malloc(NAS_TABLE_PREFIX_LEN +
		    strlen(key) + 1);
		if (tableName == NULL)
			goto error;
		mem_err = sprintf(tableName, "%s%s", NAS_TABLE_PREFIX, key);
		if (mem_err < 0) {
			free(tableName);
			goto error;
		}
		clquery_retval = cl_query_read_table(tableName,
		    &filertableContents);
		if (clquery_retval != CL_QUERY_OK) {
			free(tableName);
			goto error;
		}

		/*
		 * we are going to create a map for this instance and
		 * populate it with its attributes
		 */
		instance_map = newObjNullParam(env, "java/util/HashMap");
		if (instance_map == NULL) {
			free(tableName);
			goto error;
		}

		/*
		 * Fill out an array with node names
		 */
		array = NULL;

		for (j = 0, k = 0, count = 0;
		    j < (int)filertableContents.n_rows; j++) {
		    char *filerKey, *filerValue;
		    filerKey = filertableContents.table_rows[j]->key;
		    filerValue = filertableContents.  table_rows[j]->value;
		    if (strcmp(filerKey, DEVICE_NAME) == 0) {
			filerName = strdup(filerValue);
			PUT_ATTR(instance_map,
			    "Name",
			    (*env)->NewStringUTF(env, filerValue));
			count++;
			continue;
		    }
		    if (strcmp(filerKey, DEVICE_TYPE) == 0) {
			PUT_ATTR(instance_map,
			    "Vendor",
			    (*env)->NewStringUTF(env, filerValue));
			PUT_ATTR(instance_map,
			    "Type",
			    (*env)->NewStringUTF(env, NAS_TYPE));
			count++;
			continue;
		    }
		    if (strcmp(filerKey, LOGIN_NAME) == 0) {
			PUT_ATTR(instance_map,
			    "LoginName",
			    (*env)->NewStringUTF(env, filerValue));
			count++;
			continue;
		    }
		    if (strcmp(filerKey, PASSWD) == 0) {
			char *zPassword = NULL;
			if (filerValue != NULL) {
				zPassword = strdup(filerValue);
				scnas_rot13_alg(&zPassword);
				PUT_ATTR(instance_map,
				    "Passwd",
				    (*env)->NewStringUTF(env,
				    zPassword));
				count++;
				free(zPassword);
			}
			continue;
		    }
		    if (strncmp(filerKey, DIR_PREFIX,
			strlen(DIR_PREFIX)) == 0) {
			if (array == NULL) {
				array = (*env)->NewObjectArray(env,
				    (int)(filertableContents.n_rows
				    - count),
				    string_class, NULL);
				if (array == NULL) {
					free(tableName);
					goto error;
				}
			}
			(*env)->SetObjectArrayElement(env,
			    array, k++,
			    (*env)->NewStringUTF(env, (char *)
			    filerValue));
		    }
		}
		PUT_ATTR(instance_map, "ExportedFilesystems", array);

		/*
		 * Now put map into the returned map under this instance
		 */
		PUT(cache_map, filerName, instance_map);
		clquery_retval = cl_query_free_table(&filertableContents);
		free(filerName);
		free(tableName);
		if (clquery_retval != CL_QUERY_OK)
			goto error;
	}
	error:
	if (clquery_retval == CL_QUERY_OK)
		(void) cl_query_free_table(&keytableContents);

	return (cache_map);
}
