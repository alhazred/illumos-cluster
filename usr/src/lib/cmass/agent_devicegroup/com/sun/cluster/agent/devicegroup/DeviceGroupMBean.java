/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DeviceGroupMBean.java 1.10     08/05/20 SMI"
 */
package com.sun.cluster.agent.devicegroup;

// J2SE
import java.util.Map;

// Agent
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.ClusterException;


/**
 * This interface defines the management operation available on a remote
 * Cluster's global device.
 */
public interface DeviceGroupMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "devicegroup";

    /**
     * Get the global device's key name.
     *
     * @return  global device's key name.
     */
    public String getName();

    /**
     * Give the "online" status of the global device.
     *
     * @return  <code>true</code> if the node is online and accessible, <code>
     * false</code> if not.
     */
    public boolean isOnline();

    /**
     * Get the device group status as String.
     *
     * @return  global device's status
     */
    public int getDeviceGroupStatus();

    /**
     * Get the type of the global device.
     *
     * @return  global device's type.
     */
    public String getType();

    /**
     * Get the type of the replication.
     *
     * @return  replication type of the global device.
     */
    public String getReplicationType();

    /**
     * Get the name of the primary node manipulating the global device.
     *
     * @return  global device's primary node name.
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String getPrimaryNodeName();

    /**
     * Get the array of available secondary nodes.
     *
     * @return  an array of node names, which may be of length zero
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String[] getSecondaryNodeNames();

    /**
     * Get the array of available spare nodes.
     *
     * @return  an array of node names, which may be of length zero
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String[] getSpareNodeNames();

    /**
     * Get the current order of the physically connected nodes. This list
     * identifies the order in which the secondaries shall switch with the
     * primary.
     *
     * @return  an ordered array of node names.
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String[] getConnectedNodeOrder();

    /**
     * Get failback flag.
     *
     * @return  <code>true</code> if failback is enabled, <code>false</code>
     * else.
     */
    public boolean isFailbackEnabled();

    /**
     * Get the flag indicating if the ordered node list given by
     * {@link #getConnectedNodeOrder} shall be used or not.
     *
     * @return  <code>true</code> if the order list should be used, <code>
     * false</code> else.
     */
    public boolean isConnectedNodeOrderEnabled();

    /**
     * Get the number of available secondary nodes.
     *
     * @return  the number of secondaries.
     */
    public int getSecondariesCount();

    /**
     * Get the full path to the global device's logical disk.
     *
     * @return  path to logical disk.
     */
    public String getLogicalDiskPath();

    /**
     * Get the full paths to the global device's mounted filesystem for each
     * node
     *
     * @return  a map of node to filesystem path for each connected node
     */
    public Map getMountedFilesystemPath();

    /**
     * Get the mount point for a device groups for each device
     *
     * @return  a map of device to mounted point
     */
    public Map getMountPointPath();

    /**
     * Switch the primary node.
     *
     * @param  newPrimary  name of the new primary node.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changePrimaryNode(String newPrimary)
        throws CommandExecutionException;

    /**
     * Change the number of available secondary nodes.
     *
     * @param  newSecondariesCount  new number of secondary nodes.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeSecondariesCount(int newSecondariesCount)
        throws CommandExecutionException;

    /**
     * Set failback flag to <code>true</code> or <code>false</code>.
     *
     * @param  newFailbackFlag  the new value of failback flag.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeFailbackFlag(boolean newFailbackFlag)
        throws CommandExecutionException;

    /**
     * Set the flag indicating if the ordered node list given by
     * {@link #getConnectedNodeOrder} shall be used or not.
     *
     * @param  newOrderedNodeFlag  the new value of the flag.
     * @param  nodeOrder  the ordered node list.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeConnectedNodeOrderFlag(boolean newOrderedNodeFlag,
        String nodeOrder[]) throws CommandExecutionException;

    /**
     * Change the order the selectable secondaries by giving a new ordered list
     * of node's name.
     *
     * @param  newOrder  the new ordered node list.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeConnectedNodeOrder(String newOrder[])
        throws CommandExecutionException;

    /**
     * Bring the global device online.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  ClusterException  if no connected nodes are online
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOnline() throws CommandExecutionException,
        ClusterException;

    /**
     * Take the global device offline.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] takeOffline() throws CommandExecutionException;
}
