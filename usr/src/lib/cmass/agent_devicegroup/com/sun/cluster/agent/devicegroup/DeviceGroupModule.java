/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DeviceGroupModule.java 1.10     08/05/20 SMI"
 */

package com.sun.cluster.agent.devicegroup;

// JDK
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServer;
import javax.management.ObjectName;

// Cacao
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// local
import com.sun.cluster.common.ClusterRuntimeException;


public class DeviceGroupModule extends Module {

    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.devicegroup");

    /* Our interceptor. */
    private DeviceGroupInterceptor interceptor;

    private NasDeviceInterceptor nasInterceptor;
    private NasDeviceManager nasdeviceMgr;
    private ObjectName objectName;

    /* Our dispatcher. */
    private VirtualMBeanDomainDispatcher dispatcher;

    public DeviceGroupModule(DeploymentDescriptor descriptor) {
        super(descriptor);

        Map parameters = descriptor.getParameters();

        try {

            // Load the agent JNI library
            String library = (String) parameters.get("library");
            Runtime.getRuntime().load(library);

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING,
                "Unable to load native runtime library" + ule);
            throw ule;
        }
    }

    protected void start() throws ClusterRuntimeException {
        logger.fine("Create Interceptor");

        // Create the virtual MBean interceptor and start it.
        ObjectNameFactory objectNameFactory = new ObjectNameFactory(
                getDomainName());
        dispatcher = new VirtualMBeanDomainDispatcher(getMbs(),
                objectNameFactory);

        try {
            dispatcher.unlock();

            MBeanServer mBeanServer = getMbs();
            interceptor = new DeviceGroupInterceptor(mBeanServer, dispatcher,
                    objectNameFactory);
            interceptor.unlock();


            nasInterceptor = new NasDeviceInterceptor(mBeanServer, dispatcher,
                    objectNameFactory);
            nasInterceptor.unlock();

            // Create Nas Device Managers
            nasdeviceMgr = new NasDeviceManager(mBeanServer);
            objectName = objectNameFactory.getObjectName(
                    NasDeviceManagerMBean.class, null);
            mBeanServer.registerMBean(nasdeviceMgr, objectName);
        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING, "Unable to start DeviceGroup module" + e);
            throw new ClusterRuntimeException(e.getMessage());
        }
    }

    protected void stop() {
        logger.fine("Stop Interceptor");

        try {
            dispatcher.lock();
            interceptor.lock();
            nasInterceptor.lock();
            getMbs().unregisterMBean(objectName);

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING, "Unable to stop DeviceGroup module" + e);
            throw new ClusterRuntimeException(e.getMessage());
        }

        dispatcher = null;
        interceptor = null;
        nasInterceptor = null;
    }
}
