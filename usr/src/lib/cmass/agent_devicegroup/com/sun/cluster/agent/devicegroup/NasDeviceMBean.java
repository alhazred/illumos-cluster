/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NasDeviceMBean.java 1.9     08/05/20 SMI"
 */
package com.sun.cluster.agent.devicegroup;

// J2SE
import java.util.Map;

// Agent
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.ClusterException;


/**
 * This interface defines the management operation available on a Cluster's NAS
 * device.
 */
public interface NasDeviceMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "nasdevice";

    /**
     * Get the global device's key name.
     *
     * @return  global device's key name.
     */
    public String getName();

    /**
     * Give the "online" status of the nas device.
     *
     * @return  <code>true</code> if the node is online and accessible, <code>
     * false</code> if not.
     */
    public boolean isOnline();

    /**
     * Get the type of the nas device.
     *
     * @return  nas device's type.
     */
    public String getType();

    public String getVendor();

    public String getLoginName();

    public String getPasswd();

    /**
     * Get the paths to the exported file systems from this NAS device
     *
     * @return  a string array of exported filesystem path from this NAS device
     */
    public String[] getExportedFilesystems();

    /**
     * Changes the username, password and file systems exported for the NAS
     * device
     *
     * @param  userName  the new value for the user name.
     * @param  passwd  the new value for the passwd.
     * @param  oldExports  is a string array of file systems presently exported
     * @param  newExports  is a string array of file systems targeted for export
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeProperties(String userName, String passwd,
        String oldExports[], String newExports[])
        throws CommandExecutionException;
}
