/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NasDeviceInterceptor.java 1.14     09/04/03 SMI"
 */

package com.sun.cluster.agent.devicegroup;

// JDK
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX / JDMK
import com.sun.jdmk.JdmkMBeanServer;
import com.sun.jdmk.ServiceName;
import com.sun.jdmk.interceptor.MBeanServerInterceptor;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerNotification;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.CachedVirtualMBeanInterceptor;
import com.sun.cacao.agent.JmxClient;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Locale
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.cluster.Cluster;
import com.sun.cluster.agent.event.ClEventDefs;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.event.SysEventNotifierMBean;
import com.sun.cluster.common.ClusterException;
import com.sun.cluster.common.ClusterPaths;


/**
 * CMASS interceptor for the device group module. All instance and attribute
 * values are fetched through JNI, and all operations are performed through an
 * invocation of a CLI. This interceptor also listens for the events triggered by
 * the cluster for dynamic handle of device group's creation and deletion.
 */
public class NasDeviceInterceptor extends CachedVirtualMBeanInterceptor
    implements NotificationListener {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.devicegroup");
    private final static String logTag = "NasDeviceInterceptor";

    /* Internal reference to the SysEvent Notifier. */
    private ObjectName sysEventNotifier;

    /* The MBeanServer in which this interceptor is inserted. */
    private MBeanServer mBeanServer;

    /**
     * Constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this interceptor.
     * @param  dispatcher VirtualMBeanDomain Dispatcher
     * @param  onf Object name factory
     */
    public NasDeviceInterceptor(MBeanServer mBeanServer,
        VirtualMBeanDomainDispatcher dispatcher, ObjectNameFactory onf) {
        super(mBeanServer, dispatcher, onf, NasDeviceMBean.class, null);
        this.mBeanServer = mBeanServer;
        this.sysEventNotifier =
            new ObjectNameFactory("com.sun.cluster.agent.event").getObjectName(
                SysEventNotifierMBean.class, null);
    }

    /**
     * Called when the module is unlocked
     */
    public void unlock() throws Exception {
        super.unlock();
        logger.entering(logTag, "unlock");

        try {

            // No need to use a proxy here, we can go directly to the
            // addNotificationListener method on the mbean server.
            mBeanServer.addNotificationListener(sysEventNotifier, this, null,
                null);
        } catch (InstanceNotFoundException e) {
            logger.log(Level.WARNING, "caught exception", e);
        }
    }

    /**
     * Called when the module is locked
     */
    public void lock() throws Exception {
        super.lock();
        logger.entering(logTag, "lock");

        // Unregister for event listening.
        try {

            // No need to use a proxy here, we can go directly to the
            // addNotificationListener method on the mbean server.
            mBeanServer.removeNotificationListener(sysEventNotifier, this, null,
                null);
        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);
        }
    }

    /**
     * The interceptor should monitor create/destroy/attribute-change events and
     * emit appropriate notifications from the mbean server (for create/destroy)
     * or from the virtual mbean (attribute change). This method implements the
     * NotificationListener interface.
     *
     * @param  notification  a <code>Notification</code> value
     * @param  handback  an <code>Object</code> value
     */
    public void handleNotification(Notification notification, Object handback) {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("received cluster event " + notification);
        }

        if (!(notification instanceof SysEventNotification))
            return;

        SysEventNotification clEvent = (SysEventNotification) notification;

        // an event might indicate that configuration has changed,
        // rather than check all event types, just empty our cache
        invalidateCache();

        if (clEvent.getSubclass().equals(
                    ClEventDefs.ESC_CLUSTER_DCS_CONFIG_CHANGE)) {

            Map clEventAttrs = clEvent.getAttrs();

            String name = (String) clEventAttrs.get(ClEventDefs.CL_DS_NAME);

            int configActionInt =
                ((Long) (clEventAttrs.get(ClEventDefs.CL_CONFIG_ACTION)))
                .intValue();

            // workaround for JDK1.5/1.6 to make sure the Enum is referenced.
            ClEventDefs.ClEventConfigActionEnum configAction =
                ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED;
            configAction = (ClEventDefs.ClEventConfigActionEnum)
                (ClEventDefs.ClEventConfigActionEnum.getEnum(
                        ClEventDefs.ClEventConfigActionEnum.class,
                        configActionInt));

            if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED) {

                // Send an MBean registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Register MBean " + name);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            NasDeviceMBean.class, name);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.REGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "can't send notification", e);
                }

            } else if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.
                    CL_EVENT_CONFIG_REMOVED) {

                // Send an MBean unregistration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Unregister MBean " + name);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            NasDeviceMBean.class, name);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.UNREGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "caught exception", e);
                }
            }
        }
    }

    /**
     * Called by the parent interceptor class
     *
     * @param  instanceName  a <code>String</code> value
     * @param  operationName  a <code>String</code> value
     * @param  params  an <code>Object[]</code> value
     * @param  signature  a <code>String[]</code> value
     *
     * @return  an <code>Object</code> value
     *
     * @exception  InstanceNotFoundException  if an error occurs
     * @exception  MBeanException  if an error occurs
     * @exception  ReflectionException  if an error occurs
     * @exception  IOException  if an error occurs
     */
    public Object invoke(String instanceName, String operationName,
        Object params[], String signature[]) throws InstanceNotFoundException,
        MBeanException, ReflectionException, IOException {

        logger.entering(logTag, "invoke : " + operationName, signature);

        if (!isRegistered(instanceName)) {
            throw new InstanceNotFoundException("cannot find the instance " +
                instanceName);
        }

        // Construct the appropriate comand string depending on
        // the operation name.
        String commands[][] = null;
        File tmpFile = null;
        String fileName = null;
        InvocationStatus CmdStatus[] = null;

        if (operationName.equals("changeProperties")) {

            /*
             * 0: login name
             * 1: password
             * 2: old list of filesystems
             * 3: new list of filesystems
             * arg type appears in signature[] & values appear in params[]
             */
            if ((signature != null) && (signature.length == 4) &&
                    (signature[0].equals("java.lang.String")) &&
                    (signature[1].equals("java.lang.String")) &&
                    (signature[2].equals("[Ljava.lang.String;")) &&
                    (signature[3].equals("[Ljava.lang.String;"))) {

                ExitStatus exits[] = null;
                fileName = "/tmp/" + instanceName;
                tmpFile = new File(fileName);

                try {

                    if (tmpFile.exists()) {
                        String remove[][] = new String[][] {
                                { ClusterPaths.RM_CMD, fileName }
                            };
                        CmdStatus = InvokeCommand.execute(remove, null);
                    }
                } catch (Exception e) {
                    throw new MBeanException(new CommandExecutionException(
                            "Unable to create temporary file for password"));
                }

                String loginName = ((String) params[0]).trim();

                if ((loginName == null) || (loginName.length() == 0)) {
                    throw new MBeanException(new CommandExecutionException(
                            "Invalid signature: loginName"));
                }

                try {
                    InvocationStatus permCmdStatus[] = null;

                    FileOutputStream tmpFileStream = new FileOutputStream(
                            tmpFile);
                    byte passBytes[] = ((String) params[1]).getBytes();
                    tmpFileStream.write(passBytes);
                    tmpFileStream.close();

                    String chOwn[][] = new String[][] {
                            { ClusterPaths.CHOWN_CMD, "root:other", fileName },
                            { ClusterPaths.CHMOD_CMD, "0600", fileName }
                        };
                    permCmdStatus = InvokeCommand.execute(chOwn, null);

                } catch (Exception e) {
                    throw new MBeanException(new CommandExecutionException(
                            "Unable to create temporary file for password"));
                }

                // clean up incoming filesystem lists
                String oldFilesystems[] = NasDeviceManager.sanitize((String[])
                        params[2]);
                String newFilesystems[] = NasDeviceManager.sanitize((String[])
                        params[3]);

                // possibility 1: newFilesystems list is empty
                // just use clnasdevice's remove all command
                if ((newFilesystems == null) || (newFilesystems.length == 0) ||
                        ((newFilesystems.length == 1) &&
                            (newFilesystems[0].length() == 0))) {

                    commands = new String[][] {
                            {
                                ClusterPaths.CL_NAS_CMD, "set", "-u",
                                ((String) params[0]).trim(), "-f", fileName,
                                instanceName
                            },
                            {
                                ClusterPaths.CL_NAS_CMD, "remove-dir", "-d",
                                "all", instanceName
                            }
                        };
                } else {

                    // possibility 2:
                    // there some filesystems to add or to keep

                    // calculate lists of filesystems for removing
                    // and for adding
                    // null will mean this action not needed
                    String fsToRemove[] = stringArrayDiffs(oldFilesystems,
                            newFilesystems);
                    String fsToAdd[] = stringArrayDiffs(newFilesystems,
                            oldFilesystems);

                    // package up multiple commands for delivery
                    // into InvokeCommand:
                    // set tmp password file
                    // run one or both of remove-dir and add-dir
                    String removeCmd[] = null;
                    String addCmd[] = null;

                    int numCmds = 1; // set pswd file

                    if (fsToRemove != null) {
                        numCmds++;
                        removeCmd = generateClnasdeviceCmd("remove-dir",
                                fsToRemove, instanceName);
                    }

                    if (fsToAdd != null) {
                        numCmds++;
                        addCmd = generateClnasdeviceCmd("add-dir", fsToAdd,
                                instanceName);
                    }

                    commands = new String[numCmds][];

                    int cmdIndex = 0;

                    // set password file
                    commands[cmdIndex++] = new String[] {
                            ClusterPaths.CL_NAS_CMD, "set", "-u",
                            ((String) params[0]).trim(), "-f", fileName,
                            instanceName
                        };

                    if (fsToRemove != null) {
                        commands[cmdIndex++] = removeCmd;
                    }

                    if (fsToAdd != null) {
                        commands[cmdIndex++] = addCmd;
                    }

                } // possibility 2: fs to remove &/or add
            } else {
                String msg = "Invalid signature: incorrect number " +
                    "or type of args sent";
                throw new IOException(msg);
            }

            // end operation changeProperties
        } else {
            throw new ReflectionException(new IllegalArgumentException(
                    operationName));
        }

        // The operation might change state data, invalidate our cache
        invalidateCache();

        // Run the command(s)
        ExitStatus exits[] = null;
        InvocationStatus invocStatus[] = null;

        try {
            invocStatus = InvokeCommand.execute(commands, null);
            exits = ExitStatus.createArray(invocStatus);

            if ((tmpFile != null) && (tmpFile.exists())) {
                String remove[][] = new String[][] {
                        { ClusterPaths.RM_CMD, fileName }
                    };
                CmdStatus = InvokeCommand.execute(remove, null);
            }
        } catch (InvocationException ie) {
            invocStatus = ie.getInvocationStatusArray();
            throw new MBeanException(new CommandExecutionException(
                    ie.getMessage(), ExitStatus.createArray(invocStatus)));
        }

        return exits;
    } // invoke

    /*
     * return a String array containing all elements in array1
     * that are not also in array2
     * (order doesn't matter, nor does efficiency particularly)
     * if array2 is null, array1 is returned
     * if array1 is null, null is returned
     */
    private String[] stringArrayDiffs(String array1[], String array2[]) {


        String diffs[] = null;
        ArrayList alist1 = new ArrayList();
        ArrayList alist2 = new ArrayList();

        if ((array1 == null) || (array2 == null)) {
            diffs = array1;

            return diffs; // early exit
        }

        // use ArayLists in order to use the Collections API
        for (int i = 0; i < array1.length; i++) {
            alist1.add(array1[i].trim());
        }

        for (int i = 0; i < array2.length; i++) {
            alist2.add(array2[i].trim());
        }

        // uniq to alist1
        alist1.removeAll(alist2);

        // assemble result into typed String array
        int siz = alist1.size();

        if (siz > 0) {
            diffs = new String[siz];

            for (int i = 0; i < siz; i++) {
                diffs[i] = (String) alist1.get(i);
            }
        }

        return diffs;
    } // stringArrayDiffs


    /*
     * given an operation name, a list of filesystems, and a device name
     * return a String[] holding the appropriate clnasdevice command
     * suitable for sending to InvokeCommand
     * return null if incoming filesystems arg is null
     */
    private String[] generateClnasdeviceCmd(String operation,
        String filesystems[], String deviceName) {

        if (filesystems == null) {
            return null; // early exit
        }

        StringBuffer nasDirArgs = new StringBuffer();
        nasDirArgs.append(ClusterPaths.CL_NAS_CMD);
        nasDirArgs.append(":");
        nasDirArgs.append(operation);
        nasDirArgs.append(":");
        nasDirArgs.append("-d");
        nasDirArgs.append(":");
        nasDirArgs.append(filesystems[0]);

        for (int i = 1; i < filesystems.length; i++) {
            nasDirArgs.append(",");
            nasDirArgs.append(filesystems[i]);
        }

        nasDirArgs.append(":");
        nasDirArgs.append(deviceName);

        return (String[]) nasDirArgs.toString().split(":");
    } // generateClnasdeviceCmd

    /**
     * Fill the cache
     *
     * @return  a <code>Map</code>, key is instance name, value is a map of
     * attribute name to attribute value.
     */
    protected native Map fillCache();
}
