/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NasDeviceManager.java 1.12     08/12/03 SMI"
 */

package com.sun.cluster.agent.devicegroup;

// JDK
import java.io.File;
import java.io.FileOutputStream;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.MBeanException;

// Cacao
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.ClusterPaths;


/**
 * This class implements the {@link NasDeviceManagerMBean} interface. This MBean
 * is created and register into the CMAS agent in the same way as the
 * QuorumDeviceModule MBean.
 */
public class NasDeviceManager implements NasDeviceManagerMBean {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.devicegroup");
    private final static String logTag = "NasDeviceManager";

    /* The MBeanServer in which this MBean is inserted. */
    private MBeanServer mBeanServer;

    /**
     * Default constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this MBean.
     */
    public NasDeviceManager(MBeanServer mBeanServer) {
        this.mBeanServer = mBeanServer;
    }

    /**
     * This method creates a new instance of a NetApp Nas Device.
     *
     * @param  filerName  the name of the device to use for creating the new Nas
     * @param  filerType  the vendor of the NAS device.
     * @param  user  User name used to access the NAS device.
     * @param  passwd  Password for the user to login to the NAS device.
     * @param  fs  List of directories on the NAS device.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addNasDevice(String filerName, String filerType,
        String user, String passwd, String fs[], boolean commit)
        throws CommandExecutionException {

        String filesystems[] = sanitize(fs);
        ExitStatus exits[] = null;

        if ((filerName == null) || (filerName.length() == 0) ||
                (filerType == null) || (filerType.length() == 0) ||
                (user == null) || (user.length() == 0) || (passwd == null)) {

            throw new CommandExecutionException("Invalid Call to addNasDevice");
        }

        String fileName = "/tmp/" + filerName;
        File tmpFile = new File(fileName);

        try {
            InvocationStatus permCmdStatus[] = null;

            if (tmpFile.exists()) {
                String remove[][] = new String[][] {
                        { ClusterPaths.RM_CMD, fileName }
                    };
                permCmdStatus = InvokeCommand.execute(remove, null);
            }

            FileOutputStream tmpFileStream = new FileOutputStream(tmpFile);
            byte passBytes[] = passwd.getBytes();
            tmpFileStream.write(passBytes);
            tmpFileStream.close();

            String chOwn[][] = new String[][] {
                    { ClusterPaths.CHOWN_CMD, "root:other", fileName },
                    { ClusterPaths.CHOWN_CMD, "0600", fileName }
                };
            permCmdStatus = InvokeCommand.execute(chOwn, null);

        } catch (Exception e) {
            logger.log(Level.WARNING, "Inside CommandExecutionException");
            throw new CommandExecutionException(
                "Unable to create temporary file for password");
        }

        StringBuffer nasDirArgs = new StringBuffer();
        String cmds[][] = null;

        if ((filesystems == null) || (filesystems.length == 0) ||
                ((filesystems.length == 1) && (filesystems[0].length() == 0))) {
            cmds = new String[][] {
                    {
                        ClusterPaths.CL_NAS_CMD, "add", "-t", filerType, "-u",
                        user, "-f", fileName, filerName
                    }
                };
            exits = new ExitStatus[] {
                    new ExitStatus(ExitStatus.escapeCommand((String[]) cmds[0]),
                        ExitStatus.SUCCESS, null, null)
                };

        } else {
            nasDirArgs.append(ClusterPaths.CL_NAS_CMD);
            nasDirArgs.append(":");
            nasDirArgs.append("add-dir");
            nasDirArgs.append(":");
            nasDirArgs.append("-d");
            nasDirArgs.append(":");
            nasDirArgs.append(filesystems[0]);

            for (int i = 1; i < filesystems.length; i++) {
                nasDirArgs.append(",");
                nasDirArgs.append(filesystems[i]);
            }

            nasDirArgs.append(":");
            nasDirArgs.append(filerName);

            cmds = new String[][] {
                    {
                        ClusterPaths.CL_NAS_CMD, "add", "-t", filerType, "-u",
                        user, "-f", fileName, filerName
                    },
                    (String[]) nasDirArgs.toString().split(":")
                };
            exits = new ExitStatus[] {
                    new ExitStatus(ExitStatus.escapeCommand((String[]) cmds[0]),
                        ExitStatus.SUCCESS, null, null),
                    new ExitStatus(ExitStatus.escapeCommand((String[]) cmds[1]),
                        ExitStatus.SUCCESS, null, null)
                };
        }

        InvocationStatus invocStatus[] = null;

        try {

            if (commit) {
                invocStatus = InvokeCommand.execute(cmds, null);
                exits = ExitStatus.createArray(invocStatus);
            }

            InvocationStatus CmdStatus[] = null;

            if (tmpFile.exists()) {
                String remove[][] = new String[][] {
                        { ClusterPaths.RM_CMD, fileName }
                    };
                CmdStatus = InvokeCommand.execute(remove, null);
            }
        } catch (InvocationException ie) {
            invocStatus = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(invocStatus));
        }

        return exits;
    }

    /**
     * This method creates a new instance of a Sun Nas Device.
     *
     * @param  filerName  the name of the device to use for creating the new Nas
     * @param  filerType  the vendor of the NAS device.
     * @param  fs  List of directories on the NAS device.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addNasDevice(String filerName, String filerType,
        String fs[], boolean commit) throws CommandExecutionException {

        String filesystems[] = sanitize(fs);
        ExitStatus exits[] = null;

        if ((filerName == null) || (filerName.length() == 0) ||
                (filerType == null) || (filerType.length() == 0)) {
            throw new CommandExecutionException("Invalid Call to addNasDevice");
        }

        StringBuffer nasDirArgs = new StringBuffer();
        String cmds[][] = null;

        if ((filesystems == null) || (filesystems.length == 0) ||
                ((filesystems.length == 1) && (filesystems[0].length() == 0))) {
            cmds = new String[][] {
                    {
                        ClusterPaths.CL_NAS_CMD, "add", "-t", filerType,
                        filerName
                    }
                };
            exits = new ExitStatus[] {
                    new ExitStatus(ExitStatus.escapeCommand((String[]) cmds[0]),
                        ExitStatus.SUCCESS, null, null)
                };

        } else {
            nasDirArgs.append(ClusterPaths.CL_NAS_CMD);
            nasDirArgs.append(":");
            nasDirArgs.append("add-dir");
            nasDirArgs.append(":");
            nasDirArgs.append("-d");
            nasDirArgs.append(":");
            nasDirArgs.append(filesystems[0]);

            for (int i = 1; i < filesystems.length; i++) {
                nasDirArgs.append(",");
                nasDirArgs.append(filesystems[i]);
            }

            nasDirArgs.append(":");
            nasDirArgs.append(filerName);

            cmds = new String[][] {
                    {
                        ClusterPaths.CL_NAS_CMD, "add", "-t", filerType,
                        filerName
                    },
                    (String[]) nasDirArgs.toString().split(":")
                };
            exits = new ExitStatus[] {
                    new ExitStatus(ExitStatus.escapeCommand((String[]) cmds[0]),
                        ExitStatus.SUCCESS, null, null),
                    new ExitStatus(ExitStatus.escapeCommand((String[]) cmds[1]),
                        ExitStatus.SUCCESS, null, null)
                };
        }

        InvocationStatus invocStatus[] = null;

        try {

            if (commit) {
                invocStatus = InvokeCommand.execute(cmds, null);
                exits = ExitStatus.createArray(invocStatus);
            }

        } catch (InvocationException ie) {
            invocStatus = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(invocStatus));
        }

        return exits;
    }

    /**
     * Remove definitely this nas device.
     *
     * @param  filerName  the name of the device related to the nas Device to be
     * removed.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeNasDevice(String filerName)
        throws CommandExecutionException {

        // Run the command(s)
        String cmds[][] = new String[][] {
                {
                    ClusterPaths.CL_NAS_CMD, "remove-dir", "-d", "all",
                    filerName
                },
                { ClusterPaths.CL_NAS_CMD, "remove", filerName }
            };
        ExitStatus exits[];
        InvocationStatus invocStatus[] = null;

        try {
            invocStatus = InvokeCommand.execute(cmds, null);
            exits = ExitStatus.createArray(invocStatus);
        } catch (InvocationException ie) {
            invocStatus = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(invocStatus));
        }

        return exits;
    }

    /*
     * given a String array, remove all dups and trim all strings
     * remove all empty strings
     * returns null if no array elements to return
     * does not return empty array
     */
    public static String[] sanitize(String arrayIn[]) {
        String arrayOut[] = null;
        ArrayList alist = new ArrayList();
        String str = null;

        for (int i = 0; i < arrayIn.length; i++) {
            str = arrayIn[i].trim();

            if ((str.length() > 0) && !alist.contains(str)) {
                alist.add(str);
            }
        }

        int siz = alist.size();

        if (siz > 0) {
            arrayOut = new String[siz];

            for (int i = 0; i < siz; i++) {
                arrayOut[i] = (String) alist.get(i);
            }
        }

        return arrayOut;
    } // sanitize
}
