/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DeviceGroupInterceptor.java 1.13     09/03/27 SMI"
 */

package com.sun.cluster.agent.devicegroup;

// JDK
import java.io.IOException;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerNotification;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;

// JDMK
import com.sun.jdmk.JdmkMBeanServer;
import com.sun.jdmk.ServiceName;
import com.sun.jdmk.interceptor.MBeanServerInterceptor;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.CachedVirtualMBeanInterceptor;
import com.sun.cacao.agent.JmxClient;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Locale
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.event.ClEventDefs;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.event.SysEventNotifierMBean;
import com.sun.cluster.agent.node.NodeMBean;
import com.sun.cluster.common.ClusterException;
import com.sun.cluster.common.ClusterPaths;


/**
 * CMASS interceptor for the device group module. All instance and attribute
 * values are fetched through JNI, and all operations are performed through an
 * invocation of a CLI. This interceptor also listen for the events triggered by
 * the cluster for dynamic handle of device group's creation and deletion.
 */
public class DeviceGroupInterceptor extends CachedVirtualMBeanInterceptor
    implements NotificationListener {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.devicegroup");
    private final static String logTag = "DeviceGroupInterceptor";

    /* Internal reference to the SysEvent Notifier. */
    private ObjectName sysEventNotifier;

    /* The MBeanServer in which this interceptor is inserted. */
    private MBeanServer mBeanServer;

    /**
     * Default Constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this interceptor.
     * @param  dispatcher  DomainDispatcher that this virtual mbean interceptor
     * should register into for dispatching.
     * @param  onf  ObjectNameFactory implementation to be used by this
     * VirtualMBeanInterceptor
     */
    public DeviceGroupInterceptor(MBeanServer mBeanServer,
        VirtualMBeanDomainDispatcher dispatcher, ObjectNameFactory onf) {
        super(mBeanServer, dispatcher, onf, DeviceGroupMBean.class, null);
        this.mBeanServer = mBeanServer;
        this.sysEventNotifier =
            new ObjectNameFactory("com.sun.cluster.agent.event").getObjectName(
                SysEventNotifierMBean.class, null);
    }

    /**
     * Called when the module is unlocked
     */
    public void unlock() throws Exception {
        super.unlock();
        logger.entering(logTag, "start");

        try {

            // No need to use a proxy here, we can go directly to the
            // addNotificationListener method on the mbean server.
            mBeanServer.addNotificationListener(sysEventNotifier, this, null,
                null);
        } catch (InstanceNotFoundException e) {
            logger.warning("unable to register for devicegroup config " +
                "change events");
        }
    }

    /**
     * Called when the module is locked
     */
    public void lock() throws Exception {
        super.lock();
        logger.entering(logTag, "stop");

        // Unregister for event listening.
        try {

            // No need to use a proxy here, we can go directly to the
            // removeNotificationListener method on the mbean server.
            mBeanServer.removeNotificationListener(sysEventNotifier, this, null,
                null);
        } catch (Exception e) {
            logger.warning("Unable to unregister device group config listener");
        }

        invalidateCache();
    }

    /**
     * The interceptor should monitor create/destroy/attribute-change events and
     * emit appropriate notifications from the mbean server (for create/destroy)
     * or from the virtual mbean (attribute change). This method implements the
     * NotificationListener interface.
     *
     * @param  notification  a <code>Notification</code> value
     * @param  handback  an <code>Object</code> value
     */
    public void handleNotification(Notification notification, Object handback) {

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("received cluster event " + notification);
        }

        if (!(notification instanceof SysEventNotification))
            return;

        SysEventNotification clEvent = (SysEventNotification) notification;

        // an event might indicate that configuration has changed,
        // rather than check all event types, just empty our cache
        invalidateCache();

        if (clEvent.getSubclass().equals(
                    ClEventDefs.ESC_CLUSTER_DCS_CONFIG_CHANGE)) {

            Map clEventAttrs = clEvent.getAttrs();

            String name = (String) clEventAttrs.get(ClEventDefs.CL_DS_NAME);

            int configActionInt =
                ((Long) (clEventAttrs.get(ClEventDefs.CL_CONFIG_ACTION)))
                .intValue();

            // workaround for JDK1.5/1.6 to make sure the Enum is referenced.
            ClEventDefs.ClEventConfigActionEnum configAction =
                ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED;
            configAction = (ClEventDefs.ClEventConfigActionEnum)
                (ClEventDefs.ClEventConfigActionEnum.getEnum(
                        ClEventDefs.ClEventConfigActionEnum.class,
                        configActionInt));

            if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED) {

                // Send an MBean registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Register MBean " + name);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            DeviceGroupMBean.class, name);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.REGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "can't send notification", e);
                }

            } else if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.
                    CL_EVENT_CONFIG_REMOVED) {

                // Send an MBean unregistration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Unregister MBean " + name);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            DeviceGroupMBean.class, name);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.UNREGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "can't send notification", e);
                }
            }
        }
    }

    /**
     * Called by the parent interceptor class
     *
     * @param  instanceName  a <code>String</code> value
     * @param  operationName  a <code>String</code> value
     * @param  params  an <code>Object[]</code> value
     * @param  signature  a <code>String[]</code> value
     *
     * @return  an <code>Object</code> value
     *
     * @exception  InstanceNotFoundException  if an error occurs
     * @exception  MBeanException  if an error occurs
     * @exception  ReflectionException  if an error occurs
     * @exception  IOException  if an error occurs
     */
    public Object invoke(String instanceName, String operationName,
        Object params[], String signature[]) throws InstanceNotFoundException,
        MBeanException, ReflectionException, IOException {

        logger.entering(logTag, "invoke : " + operationName, signature);

        if (!isRegistered(instanceName)) {
            throw new InstanceNotFoundException("cannot find the instance " +
                instanceName);
        }

        // Construct the appropriate comand string depending on
        // the operation name.
        String commands[][] = null;

        if (operationName.equals("changePrimaryNode")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_DG_CMD, "switch", "-n",
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("changeSecondariesCount")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("int"))) {
                int intValue = ((Integer) params[0]).intValue();

                commands = new String[][] {
                        {
                            ClusterPaths.CL_DG_CMD, "set", "-p",
                            "numsecondaries=" +
                            ((intValue == 0) ? "" : params[0].toString()),
                            instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("changeFailbackFlag")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("boolean"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_DG_CMD, "set", "-p",
                            "failback=" +
                            (((Boolean) params[0]).booleanValue() ? "true"
                                                                  : "false"),
                            instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }
        } else if (operationName.equals("changeConnectedNodeOrderFlag")) {

            if ((signature != null) && (signature.length == 2) &&
                    (signature[0].equals("boolean")) &&
                    (signature[1].equals("[Ljava.lang.String;"))) {
                String nodes[] = (String[]) params[1];
                StringBuffer nodelist = new StringBuffer();

                for (int i = 0; i < nodes.length; i++) {

                    if (i != 0)
                        nodelist.append(",");

                    nodelist.append(nodes[i]);
                }

                commands = new String[][] {
                        {
                            ClusterPaths.CL_DG_CMD, "set", "-n",
                            nodelist.toString(), "-p",
                            "preferenced=" + params[0], instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("changeConnectedNodeOrder")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("[Ljava.lang.String;"))) {

                String nodes[] = (String[]) params[0];
                StringBuffer nodelist = new StringBuffer();

                for (int i = 0; i < nodes.length; i++) {

                    if (i != 0)
                        nodelist.append(",");

                    nodelist.append(nodes[i]);
                }

                commands = new String[][] {
                        {
                            ClusterPaths.CL_DG_CMD, "set", "-n",
                            nodelist.toString(), instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("bringOnline")) {

            if ((signature != null) && (signature.length == 0)) {

                // Find the first online node to switch to
                AttributeList attributeList = getAttributes(instanceName,
                        new String[] { "ConnectedNodeOrder" });
                Attribute attribute = (Attribute) attributeList.get(0);
                String nodeList[] = (String[]) (attribute.getValue());

                String targetNode = null;

                for (int i = 0; i < nodeList.length; i++) {

                    try {
                        logger.fine("Status of node " + nodeList[i]);

                        Map value = JmxClient.getAttributeValues(mBeanServer,
                                new ObjectNameFactory(
                                    "com.sun.cluster.agent.node"),
                                NodeMBean.class, nodeList[i], "Online", null);

                        if (
                            ((Boolean) (value.get(nodeList[i])))
                                .booleanValue() == true) {
                            targetNode = nodeList[i];

                            break;
                        }
                    } catch (Exception e) {
                        logger.log(Level.WARNING, "caught exception", e);
                    }
                }

                if (targetNode == null) {

                    // XXX this should be a typed exception
                    throw new MBeanException(new ClusterException(
                            "No node available"));
                }

                commands = new String[][] {
                        {
                            ClusterPaths.CL_DG_CMD, "online", "-n", targetNode,
                            instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("takeOffline")) {

            if ((signature != null) && (signature.length == 0)) {
                commands = new String[][] {
                        { ClusterPaths.CL_DG_CMD, "offline", instanceName }
                    };
            } else {
                throw new IOException("Invalid signature");
            }
        } else {
            throw new ReflectionException(new IllegalArgumentException(
                    operationName));
        }

        // The operation might change state data, invalidate the cache
        invalidateCache();

        // Run the command(s)
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(commands, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new MBeanException(new CommandExecutionException(
                    ie.getMessage(), ExitStatus.createArray(exits)));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * Fill the cache
     *
     * @return  a <code>Map</code>, key is instance name, value is a map of
     * attribute name to attribute value.
     */
    protected native Map fillCache();
}
