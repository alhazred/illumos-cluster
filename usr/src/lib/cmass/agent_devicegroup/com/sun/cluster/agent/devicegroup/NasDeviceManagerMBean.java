/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NasDeviceManagerMBean.java 1.7     08/12/03 SMI"
 */

package com.sun.cluster.agent.devicegroup;

import com.sun.cacao.invocation.InvocationStatus;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available to handle Nas
 * Devices.
 */
public interface NasDeviceManagerMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public final static String TYPE = "nasdeviceManager";

    /**
     * This method creates a new instance of a NetApp Nas Device.
     *
     * @param  filerName  the name of the nas device to use for creating the new NAS
     * @param  filerType  the vendor of the nas device.
     * @param  user  the user ID for accessing the nas device.
     * @param  passwd  Password for the user to access the nas device.
     * @param  fileSystems Directories on the NAS device. 
     * @param  commit  Commit the opertation if set to <code>true</code>
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addNasDevice(String filerName, String filerType,
        String user, String passwd, String fileSystems[], boolean commit)
        throws CommandExecutionException;

    /**
     * This method creates a new instance of a Sun Nas Device.
     *
     * @param  filerName  the name of the nas device to use for creating the new NAS
     * Device.
     * @param  filerType  the vendor of the nas device.
     * @param  fileSystems Directories on the NAS device. 
     * @param  commit  Commit the opertation if set to <code>true</code>
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addNasDevice(String filerName, String filerType,
        String fileSystems[], boolean commit) throws CommandExecutionException;

    /**
     * Remove definitely this nas device.
     *
     * @param  name  the name of the nas device to delete
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeNasDevice(String filerName)
        throws CommandExecutionException;

}
