/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)QuorumDeviceManagerMBean.java 1.9     08/05/20 SMI"
 */

package com.sun.cluster.agent.quorumdevice;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available to handle Quorum
 * Devices.
 */
public interface QuorumDeviceManagerMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public final static String TYPE = "quorumdeviceManager";

    /**
     * This method creates a new instance of a Quorum Device.
     *
     * @param  name  the name of the device to use for creating the new Quorum
     * Device, like /dev/did/rdsk/d3.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addQuorumDevice(String name, boolean commit)
        throws CommandExecutionException;

    public ExitStatus[] addQuorumDevice(String qname, String qtype,
        String prop1, String prop2, boolean commit)
        throws CommandExecutionException;

    /**
     * Remove definitely this quorum device.
     *
     * @param  name  the name of the device related to the Quorum Device to be
     * removed, like /dev/did/rdsk/d3.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeQuorumDevice(String name)
        throws CommandExecutionException;

    /**
     * Reset all the quorum device and leave the install mode
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] resetQuorumDevices() throws CommandExecutionException;

}
