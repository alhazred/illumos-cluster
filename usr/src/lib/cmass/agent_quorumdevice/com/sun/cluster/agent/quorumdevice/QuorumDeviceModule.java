/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)QuorumDeviceModule.java 1.9     08/05/20 SMI"
 */
package com.sun.cluster.agent.quorumdevice;

// JDK
import java.util.Map;
import java.util.logging.Logger;

// JMX
import javax.management.ObjectName;

// Cacao
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// Local
import com.sun.cluster.common.ClusterRuntimeException;


/**
 * Core implementation of the QuorumDevice module. This module is using JDMK's
 * interceptor and JNI design pattern for subsequent calls to the SCHA API.
 */
public class QuorumDeviceModule extends Module {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.quorumdevice");
    private final static String logTag = "QuorumDeviceModule";

    // Our dispatcher.
    private VirtualMBeanDomainDispatcher dispatcher;

    /* Internal reference to the JDMK interceptors and MBean manager. */
    private QuorumDeviceInterceptor quorumDeviceInterceptor;
    private QuorumDeviceManager quorumDeviceManager;
    private ObjectName objectName;

    /**
     * Constructor called by the container
     *
     * @param  descriptor  the deployment descriptor used to load the module
     */
    public QuorumDeviceModule(DeploymentDescriptor descriptor) {
        super(descriptor);
        logger.entering(logTag, "<init>", descriptor);

        Map parameters = descriptor.getParameters();

        try {

            // Load the agent JNI library
            String library = (String) parameters.get("library");
            Runtime.getRuntime().load(library);

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to load native runtime library" + ule);
            throw ule;
        }

        logger.exiting(logTag, "<init>");
    }

    /**
     * Invoked by the container when starting up.
     *
     * @throws  ClusterRuntimeException  If unable to start.
     */
    protected void start() throws ClusterRuntimeException {
        logger.entering(logTag, "start");

        ObjectNameFactory objectNameFactory = new ObjectNameFactory(
                getDomainName());

        logger.finest("create VirtualMBeanDomainDispatcher");
        dispatcher = new VirtualMBeanDomainDispatcher(getMbs(),
                objectNameFactory);

        try {
            logger.fine("unlock dispatcher");
            dispatcher.unlock();

            logger.finest("create QuorumDeviceInterceptor");
            quorumDeviceInterceptor = new QuorumDeviceInterceptor(getMbs(),
                    dispatcher, objectNameFactory);
            logger.fine("unlock QuorumDeviceInterceptor");
            quorumDeviceInterceptor.unlock();

            logger.finest("create QuorumDeviceManager");
            quorumDeviceManager = new QuorumDeviceManager(getMbs());
            logger.fine("register QuorumDeviceManager");
            getMbs().registerMBean(quorumDeviceManager,
                objectNameFactory.getObjectName(QuorumDeviceManagerMBean.class,
                    null));

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to start Quorum Device module : " +
                e.getMessage());
            throw new ClusterRuntimeException(e.getMessage());

        } finally {
            logger.exiting(logTag, "start");
        }
    }

    /**
     * Invoked by the container when stopping, must release all resources.
     */
    protected void stop() {
        logger.entering(logTag, "stop");

        try {
            logger.fine("lock dispatcher");
            dispatcher.lock();
            logger.fine("lock QuorumDeviceInterceptor");
            quorumDeviceInterceptor.lock();
            logger.fine("unregister QuorumDeviceManager");

            ObjectNameFactory objectNameFactory = new ObjectNameFactory(
                    getDomainName());
            getMbs().unregisterMBean(objectNameFactory.getObjectName(
                    QuorumDeviceManagerMBean.class, null));

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to stop Quorum Device module : " +
                e.getMessage());
            throw new ClusterRuntimeException(e.getMessage());

        } finally {
            dispatcher = null;
            quorumDeviceInterceptor = null;
            logger.exiting(logTag, "stop");
        }
    }
}
