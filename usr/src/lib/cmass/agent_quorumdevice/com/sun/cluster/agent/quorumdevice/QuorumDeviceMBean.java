/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)QuorumDeviceMBean.java 1.10     08/05/20 SMI"
 */
package com.sun.cluster.agent.quorumdevice;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available on a remote Cluster
 * Quorum Device.
 */
public interface QuorumDeviceMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "quorumdevice";

    /**
     * Get the quorum device's key name.
     *
     * @return  quorum device's key name which is the MBean instance name, like
     * /dev/did/rdsk/d2 for /dev/did/rdsk/d2s2.
     */
    public String getName();

    /**
     * Get the quorum device's name.
     *
     * @return  quorum device's name, like d2 for /dev/did/rdsk/d2s2. This
     * function returns NULL for NAS type of device
     */
    public String getDevice();

    /**
     * Get the quorum device's type from the CCR. Values are scsi, netappnas
     * etc..
     */
    public String getDeviceType();

    /**
     * Get the quorum device's slice number. This function returns NULL for NAS
     * type of device
     *
     * @return  quorum device's slice number, like s2 for /dev/did/rdsk/d2s2.
     */
    public String getSlice();

    /**
     * Indicates if the quorum device is online or not (operational state).
     *
     * @return  <code>true</code> if the quorum device is online, <code>
     * false</code> if not.
     */
    public boolean isOnline();

    /**
     * Indicates if the quorum device is enabled or not (admin state).
     *
     * @return  <code>true</code> if the quorum device is online, <code>
     * false</code> if not.
     */
    public boolean isEnabled();

    /**
     * Indicates if the quorum device is in maintenance mode or not.
     *
     * @return  <code>true</code> if the quorum device is in maintenance, <code>
     * false</code> if not.
     */
    public boolean isInMaintenanceMode();

    /**
     * Get the number of current votes for this quorum device.
     *
     * @return  amount of current votes.
     */
    public int getCurrentVotes();

    /**
     * Get the number of possible votes for this quorum device.
     *
     * @return  amount of possible votes.
     */
    public int getPossibleVotes();

    /**
     * Get the number of needed votes for the quorum algorythm.
     *
     * @return  amount of needed votes.
     */
    public int getClusterNeededVotes();

    /**
     * Get the number of configured votes for the quorum algorythm.
     *
     * @return  amount of configured votes.
     */
    public int getClusterConfiguredVotes();

    /**
     * Get the number of Present votes for the quorum algorythm.
     *
     * @return  amount of Present votes.
     */
    public int getClusterPresentVotes();

    /**
     * Get the quorum device's access mode. This function returns NULL for NAS
     * type of device
     *
     * @return  quorum device's access mode.
     */
    public String getAccessMode();

    /**
     * Get the array of enabled nodes linked to this quorum device.
     *
     * @return  an array of node's name; <code>null</code> if no node is
     * enabled.
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String[] getEnabledNodeNames();

    /**
     * Get the array of disabled nodes linked to this quorum device.
     *
     * @return  an array of node's name; <code>null</code> if no node is
     * disabled.
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String[] getDisabledNodeNames();

    /**
     * Reset this quorum device.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] reset() throws CommandExecutionException;

    /**
     * Put this quorum device in maintenance mode. This will remove this device
     * from the Quorum and the Quorum will be re-calculated.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] putInMaintenanceMode() throws CommandExecutionException;


    /*
     * The following three functions are specific to NAS type of
     * quorum device
     */

    /**
     * Get the filer name corresponding to a NAS quorum device. The function
     * returns NULL for scsi quorum devices
     *
     * @return  filer name corresponding to a NAS quorum device.
     */
    public String getFilerName();

    /**
     * Get the name of the lun on the filer that is used for quorum. This is
     * valid only for NAS quorum devices. The function returns NULL for scsi
     * quorum devices
     *
     * @return  filer name corresponding to a NAS quorum device.
     */
    public String getLunName();

    /**
     * Get the Lun ID on the filer that is used for quorum. This is valid only
     * for NAS quorum devices. The function returns NULL for scsi quorum devices
     *
     * @return  filer name corresponding to a NAS quorum device.
     */
    public String getLunID();

    /**
     * The following two functions are specific to Quorum Server
     */

    /**
     * Get the quorum server host name or IP of the quorum. This is valid only
     * for Quorum server. It returns NULL for other types of quorum devices.
     *
     * @return  IP address corresponding to a Quorum Server.
     */
    public String getQServerHost();

    /**
     * Get the quorum server port number used for the quorum. This is valid only
     * for Quorum server. It returns NULL for other types of quorum devices.
     *
     * @return  port number corresponding to a Quorum Server.
     */
    public String getQServerPort();
}
