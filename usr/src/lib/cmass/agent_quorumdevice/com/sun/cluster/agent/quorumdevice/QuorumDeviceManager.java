/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)QuorumDeviceManager.java 1.17     08/12/03 SMI"
 */

package com.sun.cluster.agent.quorumdevice;

// JMX
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.MBeanException;

// Cacao
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.ClusterPaths;



/**
 * This class implements the {@link QuorumDeviceManagerMBean} interface. This
 * MBean is created and register into the CMAS agent in the same way as the
 * {@link QuorumDeviceModule} MBean.
 */
public class QuorumDeviceManager implements QuorumDeviceManagerMBean {

    private final static String NETAPP_NAS = "netapp_nas";
    private final static String QUORUM_SERVER = "quorum_server";

    /* The MBeanServer in which this MBean is inserted. */
    private MBeanServer mBeanServer;

    /**
     * Default constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this MBean.
     */
    public QuorumDeviceManager(MBeanServer mBeanServer) {
        this.mBeanServer = mBeanServer;
    }

    /**
     * This method creates a new instance of a Quorum Device.
     *
     * @param  name  the name of the device to use for creating the new Quorum
     * Device.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addQuorumDevice(String name, boolean commit)
        throws CommandExecutionException {

        // Run the command(s)
        String cmds[][] = new String[][] {
                { ClusterPaths.CL_QUORUM_CMD, "add", name }
            };
        ExitStatus exits[] = null;
        InvocationStatus invocStatus[] = null;

        try {

            if (commit) {
                invocStatus = InvokeCommand.execute(cmds, null);
                exits = ExitStatus.createArray(invocStatus);
            } else {
                exits = new ExitStatus[] {
                        new ExitStatus(ExitStatus.escapeCommand(
                                (String[]) cmds[0]), ExitStatus.SUCCESS, null,
                            null)
                    };
            }
        } catch (InvocationException ie) {
            invocStatus = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(invocStatus));
        }

        return exits;
    }

    /**
     * This method creates a new instance of a Quorum Device.
     *
     * @param  qname  the name of the device to use for creating the new Quorum
     * Device.
     * @param  qtype  the type of the quorum device.
     * @param  prop1  quorum device property. For netapp_nas qtype,
     * prop1 is filername; for quorum_server type, prop1 is hostname.
     * @param  prop2  quorum device property. For netapp_nas qtype,
     * prop2 is the lun id; for quorum_server type, prop2 is the port number.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addQuorumDevice(String qname, String qtype,
        String prop1, String prop2, boolean commit)
        throws CommandExecutionException {

        // Create the command(s)
        String cmds[][] = null;

        if ((qname == null) || (qname.length() == 0) || (qtype == null) ||
                (qtype.length() == 0)) {
            throw new CommandExecutionException(
                "Invalid Call to addQuorumDevice");
        }

        if (qtype.equals(NETAPP_NAS)) {
            cmds = new String[][] {
                    {
                        ClusterPaths.CL_QUORUM_CMD, "add", "-t", qtype,
                        "-p filer=" + prop1, "-p lun_id=" + prop2, qname
                    }
                };
        } else if (qtype.equals(QUORUM_SERVER)) {

            // Check required properties.
            if ((prop1 == null) || (prop1.length() == 0) || (prop2 == null) ||
                    (prop2.length() == 0)) {
                throw new CommandExecutionException(
                    "Invalid Call to addQuorumDevice");
            }

            cmds = new String[][] {
                    {
                        ClusterPaths.CL_QUORUM_CMD, "add", "-t", qtype,
                        "-p qshost=" + prop1, "-p port=" + prop2, qname
                    }
                };
        } else {
            throw new CommandExecutionException(
                "Invalid Call to addQuorumDevice");
        }

        // Run the command
        ExitStatus exits[] = null;
        InvocationStatus invocStatus[] = null;

        try {

            if (commit) {
                invocStatus = InvokeCommand.execute(cmds, null);
                exits = ExitStatus.createArray(invocStatus);
            } else {
                exits = new ExitStatus[] {
                        new ExitStatus(ExitStatus.escapeCommand(
                                (String[]) cmds[0]), ExitStatus.SUCCESS, null,
                            null)
                    };
            }
        } catch (InvocationException ie) {
            invocStatus = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(invocStatus));
        }

        return exits;
    }

    /**
     * Remove definitely this quorum device.
     *
     * @param  name  the name of the device related to the Quorum Device to be
     * removed.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeQuorumDevice(String name)
        throws CommandExecutionException {

        // Run the command(s)
        String cmds[][] = new String[][] {
                { ClusterPaths.CL_QUORUM_CMD, "remove", name }
            };
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * Reset all the quorum device and leave the install mode
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] resetQuorumDevices() throws CommandExecutionException {

        // Run the command(s)
        String cmds[][] = new String[][] {
                { ClusterPaths.CL_QUORUM_CMD, "reset" }
            };
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }
}
