/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Quorum Device mbean interceptor
 */

#pragma	ident	"@(#)quorumdevice_interceptor_jni.c 1.12	08/05/21 SMI"

#include "com/sun/cluster/agent/quorumdevice/QuorumDeviceInterceptor.h"

#include <jniutil.h>
#include <string.h>
#include <stdio.h>
#include <cl_query/cl_query_types.h>

/*
 * A couple of helper macros for things we do a lot,
 * they contain references to local variables in the
 * function below
 */
#define	PUT_ATTR(MAP, NAME, VALUE)		\
	PUT(MAP, NAME, (*env)->NewObject(env,	\
	    attribute_class,			\
	    attribute_constructor,		\
	    (*env)->NewStringUTF(env, NAME),	\
	    VALUE))

#define	PUT(MAP, NAME, VALUE)						\
	(*env)->CallObjectMethod(env,					\
				    MAP,				\
				    map_put,				\
				    (*env)->NewStringUTF(env, NAME),	\
				    VALUE)


/*
 * Class:     com_sun_cluster_agent_quorumdevice_QuorumDeviceInterceptor
 * Method:    fillCache
 * Signature: ()Ljava/util/Map;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_quorumdevice_QuorumDeviceInterceptor_fillCache
	(JNIEnv * env, jobject this) {

	/*
	 * return a Map of Maps containing all instances
	 * key = instanceName
	 * value = Map
	 * . key=attributeName
	 * . value=corresponding Attribute object
	 */

	jobject cache_map = NULL;
	jobject instance_map = NULL;
	jclass map_class = NULL;
	jmethodID map_put;

	jclass attribute_class = NULL;
	jmethodID attribute_constructor = NULL;

	cl_query_result_t result;
	cl_query_quorum_info_t *quorum = NULL;
	cl_query_error_t error = CL_QUERY_ERROR_MAX;

	/*
	 * get our references to 'attribute_class'
	 */

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * get constructor
	 */
	attribute_constructor =
	    (*env)->GetMethodID(env,
	    attribute_class,
	    "<init>", "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;


	/*
	 * Create the map we are going to return
	 */
	cache_map = newObjNullParam(env, "java/util/HashMap");
	if (cache_map == NULL)
		goto error;

	/*
	 * get our references to map_class and the put method
	 */
	map_class = (*env)->FindClass(env, "java/util/Map");
	if (map_class == NULL)
		goto error;
	map_put =
	    (*env)->GetMethodID(env,
	    map_class,
	    "put",
	    "(Ljava/lang/Object;Ljava/lang/Object;)" "Ljava/lang/Object;");
	if (map_put == NULL)
		goto error;

	/*
	 * Now get the data
	 */
	error = cl_query_get_info(CL_QUERY_QUORUM_INFO, NULL, &result);
	if (error != CL_QUERY_OK)
		goto error;

	for (quorum = (cl_query_quorum_info_t *)result.value_list;
	    quorum != NULL; quorum = quorum->next) {

		char *idx = NULL;

		/*
		 * we are going to create a map for this instance and
		 * populate it with its attributes
		 */

		instance_map = newObjNullParam(env, "java/util/HashMap");
		if (instance_map == NULL)
			goto error;

		PUT_ATTR(instance_map, "Device",
		    (*env)->NewStringUTF(env, (char *)
			quorum->device_name));

		PUT_ATTR(instance_map,
		    "DeviceType", (*env)->NewStringUTF(env, (char *)
		    quorum->device_type));

		if ((strncmp(quorum->device_type, SCSI_TYPE,
		    strlen(SCSI_TYPE)) == 0) ||
		    (strcmp(quorum->device_type, SD_TYPE) == 0)) {

			/*
			* separate out the slice name and name
			* name = /dev/did/rdsk/d2, device_name = d2
			* slice name = s2
			*/
			idx = strrchr(quorum->key_name, '/');
			if ((quorum->key_name != NULL) && (idx != NULL)) {
				/*
				* Find first character of the slice
				*/

				idx += strlen(quorum->device_name) + 1;

				PUT_ATTR(instance_map, "Slice",
				    (*env)->NewStringUTF(env, idx));

				/*
				* Now replace 's' of slice with NULL
				*/
				*idx = 0;
				PUT_ATTR(instance_map, "Name",
				    (*env)->NewStringUTF(env, (char *)
				    quorum->key_name));
			}
		} else {
			PUT_ATTR(instance_map, "Name",
			    (*env)->NewStringUTF(env, (char *)
				quorum->key_name));
			PUT_ATTR(instance_map, "Slice",
			    (*env)->NewStringUTF(env, NULL));
		}

		PUT_ATTR(instance_map, "Online",
		    newBoolean(env, (quorum->status == CL_QUERY_ONLINE)));
		PUT_ATTR(instance_map, "Enabled",
		    newBoolean(env, (quorum->state == CL_QUERY_ENABLED)));


		PUT_ATTR(instance_map, "InMaintenanceMode",
		    newBoolean(env, (quorum->status == CL_QUERY_DISABLED)));

		PUT_ATTR(instance_map,
		    "CurrentVotes",
		    newObjInt32Param(env,
			"java/lang/Integer", (int32_t)quorum->current_votes));

		PUT_ATTR(instance_map,
		    "PossibleVotes",
		    newObjInt32Param(env,
			"java/lang/Integer", (int32_t)quorum->possible_votes));

		PUT_ATTR(instance_map,
		    "ClusterNeededVotes",
		    newObjInt32Param(env,
			"java/lang/Integer", (int32_t)quorum->votes_needed));
		PUT_ATTR(instance_map,
		    "ClusterPresentVotes",
		    newObjInt32Param(env,
			"java/lang/Integer", (int32_t)quorum->votes_present));
		PUT_ATTR(instance_map,
		    "ClusterConfiguredVotes",
		    newObjInt32Param(env,
			"java/lang/Integer",
			(int32_t)quorum->votes_configured));

		PUT_ATTR(instance_map,
		    "AccessMode", (*env)->NewStringUTF(env, (char *)
			quorum->access_mode));

		PUT_ATTR(instance_map,
		    "EnabledNodeNames",
		    getClQueryList(env, &quorum->enabled_nodes));
		PUT_ATTR(instance_map,
		    "DisabledNodeNames",
		    getClQueryList(env, &quorum->disabled_nodes));

		PUT_ATTR(instance_map,
		    "FilerName", (*env)->NewStringUTF(env, (char *)
		    quorum->filer_name));
		PUT_ATTR(instance_map,
		    "LunName", (*env)->NewStringUTF(env, (char *)
		    quorum->lun_name));
		PUT_ATTR(instance_map,
		    "LunID", (*env)->NewStringUTF(env, (char *)
		    quorum->lunid));

		PUT_ATTR(instance_map,
		    "QServerHost", (*env)->NewStringUTF(env, (char *)
		    quorum->qs_host));
		PUT_ATTR(instance_map,
		    "QServerPort", (*env)->NewStringUTF(env, (char *)
		    quorum->qs_port));

		/*
		 * Now put this map into the returned map under this
		 * instance
		 */
		PUT(cache_map, quorum->key_name, instance_map);
	}

	error:

	if (error == CL_QUERY_OK)
		error = cl_query_free_result(CL_QUERY_QUORUM_INFO, &result);

	return (cache_map);

} /*lint !e715 */
