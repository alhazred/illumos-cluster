/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)FailoverControlMBean.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.agent.failovercontrol;

/**
 * This interface defines the management operations available for failover
 * modules. These module should a property that indicates that they are failover
 * modules and to which faiover group they belong to.
 */
public interface FailoverControlMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "failovercontrol";

    /*
     * Start (ie unlock) all the failover modules belonging
     * to the given failover group.
     *
     * @param name of the failover group to start
     * @return boolean value TRUE  means start success
     *                       FALSE means start failure
     *
     */
    public boolean startFailoverGroup(String failoverGroupName);

    /*
     * Stop (ie lock) all the failover modules belonging
     * to the given failover group.
     *
     * @param name of the failover group to stop
     */
    public void stopFailoverGroup(String failoverGroupName);

    /*
     * Check the health of all the failover modules belonging
     * to the given failover group.
     *
     * @param name of the failover group to stop
     *
     * @return boolean value TRUE  means health-check success
     *                       FALSE means health-check failure
     */
    public boolean isFailoverGroupHealthy(String failoverGroup);

}
