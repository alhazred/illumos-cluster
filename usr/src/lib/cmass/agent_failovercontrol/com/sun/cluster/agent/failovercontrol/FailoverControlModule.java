/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)FailoverControlModule.java 1.8     08/05/20 SMI"
 */
package com.sun.cluster.agent.failovercontrol;

// JDK
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

// Cacao
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// Agent
import com.sun.cluster.common.ClusterRuntimeException;


/**
 * Failover control module.
 */
public class FailoverControlModule extends Module {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.failovercontrol");
    private final static String logTag = "FailoverControlModule";

    /* Our objectname. */
    private ObjectName objectName;

    /**
     * Constructor called by the container
     *
     * @param  descriptor  the deployment descriptor used to load the module
     */
    public FailoverControlModule(DeploymentDescriptor descriptor) {
        super(descriptor);
    }

    /**
     * Invoked by the container when starting up.
     *
     * @throws  ClusterRuntimeException  If unable to start.
     */
    protected void start() throws ClusterRuntimeException {
        logger.entering(logTag, "start");

        try {
            logger.finest("create FailoverControlMBean");

            ObjectNameFactory objectNameFactory = new ObjectNameFactory(
                    getDomainName());

            FailoverControlMBean failoverControlMBean = new FailoverControl(
                    this, getMbs(), objectNameFactory);

            objectName = objectNameFactory.getObjectName(
                    FailoverControlMBean.class, null);
            logger.fine("register FailoverControlMBean");
            getMbs().registerMBean(failoverControlMBean, objectName);

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to start Failover Control module : " +
                e.getMessage());
            throw new ClusterRuntimeException(e.getMessage());

        } finally {
            logger.exiting(logTag, "start");
        }
    }

    /**
     * Invoked by the container when stopping, must release all resources.
     */
    protected void stop() {
        logger.entering(logTag, "stop");

        try {
            logger.fine("unregister FailoverControlMBean");
            getMbs().unregisterMBean(objectName);

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to stop Failover Control module : " +
                e.getMessage());
            throw new ClusterRuntimeException(e.getMessage());

        } finally {
            logger.exiting(logTag, "stop");
        }
    }

}
