/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ModuleComparator.java 1.9     08/05/20 SMI"
 */

package com.sun.cluster.agent.failovercontrol;

import com.sun.cacao.Dependency;

// JDK
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

// Cacao
import com.sun.cacao.ModuleMBean;


/**
 * An internal Comparator class that can do native sorts in standard or inverse
 * ording
 */

public class ModuleComparator implements Comparator {

    protected int sign = 1;

    public ModuleComparator(boolean inverted) {

        if (inverted)
            sign = -1;
        else
            sign = 1;
    }

    public int compare(Object o1, Object o2) {

        ModuleMBean mbeans[] = new ModuleMBean[2];
        List dependencies[] = new List[2];
        String className[] = new String[2];
        String moduleName[] = new String[2];

        mbeans[0] = (ModuleMBean) o1;
        mbeans[1] = (ModuleMBean) o2;

        for (int i = 0; i < 2; i++) {
            dependencies[i] = mbeans[i].getDeploymentDescriptor()
                .getDependencies();
            className[i] = mbeans[i].getDeploymentDescriptor().getModuleClass();
            moduleName[i] = mbeans[i].getDeploymentDescriptor().getName();
        }

        // No dependency for both modules, return alpha order
        if ((dependencies[0] == null) && (dependencies[1] == null)) {

            return sign * (className[0].compareTo(className[1]));
        }

        // No dependency for first module, load this one first
        if (dependencies[0] == null) {
            return sign;
        }

        // No dependency for second module, load this one first
        if (dependencies[1] == null) {
            return -sign;
        }

        // Same dependencies for both modules, return alpha order
        if (dependencies[0].equals(dependencies[1])) {

            return sign * (className[0].compareTo(className[1]));
        }

        for (int i = 0; i < dependencies[0].size(); i++) {
            Dependency dependency = (Dependency) dependencies[0].get(i);

            if (dependency.getModuleName().equals(moduleName[1])) {
                return -sign;
            }
        }

        for (int i = 0; i < dependencies[1].size(); i++) {
            Dependency dependency = (Dependency) dependencies[1].get(i);

            if (dependency.getModuleName().equals(moduleName[0])) {
                return sign;
            }
        }

        return sign;

    }

    public boolean equals(Object obj) {
        return ((obj instanceof ModuleComparator) &&
                (((ModuleComparator) obj).sign == sign));
    }
}
