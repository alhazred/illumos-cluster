/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)FailoverControl.java 1.10     08/05/20 SMI"
 *
 */
package com.sun.cluster.agent.failovercontrol;

// JDK
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

// cacao
import com.sun.cacao.ModuleMBean;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.JmxClient;
import com.sun.cacao.element.AdministrativeStateEnum;
import com.sun.cacao.element.OperationalStateEnum;

// Agent
import com.sun.cluster.common.ClusterRuntimeException;


/**
 * Failover control module.
 */
public class FailoverControl implements FailoverControlMBean {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.failovercontrol");

    /* internal IDs for operations we can perform on modules */
    private final String UNLOCK = "UNLOCK";
    private final String LOCK = "LOCK";
    private final String CHECK = "CHECK";

    private ModuleMBean myModule;
    private MBeanServer mbeanServer;
    private ObjectNameFactory myObjectNameFactory;

    /**
     * Constructor called by the container.
     */
    public FailoverControl(ModuleMBean module, MBeanServer mbs,
        ObjectNameFactory onf) {

        myModule = module;
        mbeanServer = mbs;
        myObjectNameFactory = onf;
    }

    /*
     * Start (ie unlock) all the failover modules belonging
     * to the given failover group.
     *
     * @param name of the failover group to start
     * @return boolean value TRUE  means start success
     *                       FALSE means start failure
     *
     */
    public boolean startFailoverGroup(String failoverGroupName) {

        if (failoverGroupName != null) {
            return doOperation(UNLOCK, failoverGroupName);
        } else {
            return false;
        }
    }

    /*
     * Stop (ie lock) all the failover modules belonging
     * to the given failover group.
     *
     * @param name of the failover group to stop
     */
    public void stopFailoverGroup(String failoverGroupName) {

        if (failoverGroupName != null) {
            doOperation(LOCK, failoverGroupName);
        }
    }

    /*
     * Check the health of all the failover modules belonging
     * to the given failover group.
     *
     * @param name of the failover group to stop
     *
     * @return boolean value TRUE  means health-check success
     *                       FALSE means health-check failure
     */
    public boolean isFailoverGroupHealthy(String failoverGroupName) {

        if (failoverGroupName != null) {
            return doOperation(CHECK, failoverGroupName);
        } else {
            return false;
        }
    }

    private boolean doOperation(String operation, String failoverGroupName) {

        ModuleComparator comparator = null;
        boolean res = true;

        /*
         * For LOCK operations we want to do everything in reverse order
         * since we are disabling modules (do last module first)
         */
        if (operation.equals(UNLOCK)) {
            comparator = new ModuleComparator(true);
        } else {
            comparator = new ModuleComparator(false);
        }

        ObjectNameFactory onf = new ObjectNameFactory("com.sun.cacao");

        try {
            List mbeanList = JmxClient.getMBeanProxies(mbeanServer, onf,
                    ModuleMBean.class, false, null);

            // Identify the failover modules, and sort them

            // Sort order specified by comparator - reverse sort when
            // we are going to shut the modules down.

            SortedSet sortedSet = new TreeSet(comparator);
            sortedSet.addAll(mbeanList);

            // Now we can iterate through the sorted list of modules
            // LOCKing or UNLOCKing or CHECKing them as appropriate

            Iterator i = sortedSet.iterator();

            while (i.hasNext()) {

                ModuleMBean mbean = (ModuleMBean) (i.next());

                try {

                    /*
                     * operations only happen on failover modules that belong
                     * to the failover group given in parameter
                     */
                    Map parameters = mbean.getDeploymentDescriptor()
                        .getParameters();

                    if (parameters == null)
                        continue;

                    String failoverGroupProperty = (String) parameters.get(
                            ModuleMBean.FAILOVER_MODULE_PROPERTY);

                    if ((failoverGroupProperty == null) ||
                            (!failoverGroupProperty.equals(failoverGroupName)))
                        continue;

                    if (operation.equals(CHECK)) {

                        if (!mbean.isHealthy()) {
                            res = false;
                            logger.log(Level.WARNING,
                                "HealthCheck failed for module : " +
                                mbean.getName());
                        }
                    } else if (operation.equals(UNLOCK)) {
                        logger.fine("UNLOCKing " + mbean.getName());
                        mbean.unlock();

                        if (mbean.getOperationalState() ==
                                OperationalStateEnum.DISABLED) {
                            res = false;
                            logger.log(Level.WARNING,
                                "Start failed for module : " + mbean.getName());
                        }
                    } else if (operation.equals(LOCK)) {
                        logger.fine("LOCKing " + mbean.getName());
                        mbean.lock();
                    }
                } catch (Exception e) {
                    logger.warning("Unable to perform " + operation + " on " +
                        mbean.getName() + " : " + e.toString());

                    e.printStackTrace();

                    res = false;
                    throw new ClusterRuntimeException("operation failed");
                }
            }
        } catch (Exception e) {
            logger.warning("Caught exception : " + e.toString());

            e.printStackTrace();

            res = false;
            throw new ClusterRuntimeException("operation failed");
        }

        return res;
    }
}
