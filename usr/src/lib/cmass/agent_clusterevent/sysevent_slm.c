/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)sysevent_slm.c	1.6	08/05/20 SMI"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <door.h>
#include <fcntl.h>
#include <syslog.h>
#include <libsysevent.h>
#include <pthread.h>
#include <libnvpair.h>
#include <sys/sysevent/eventdefs.h>
#include <pwd.h>
#include <sys/cl_eventdefs.h>

#include "sysevent_cmas.h"

#define	CMASA_SLM_DOOR_FAILED	gettext("CMASA SLM door create failed\n")

/*
 * syseventd event handler
 */
static	int	cmasa_slm_debug = 0;
static	int	cmasa_slm_deliver_event(sysevent_t *ev, int flag);
static	int	door_fd = -1;

typedef struct ev_queue {
	struct ev_queue *evq_next;
	sysevent_t	*evq_ev;
} ev_queue_t;

static mutex_t evq_lock;
static cond_t evq_cv;
static ev_queue_t *eventq_head;
static ev_queue_t *eventq_tail;

static int cleanup;
static thread_t cmasa_slm_deliver_thr_id;

static struct slm_mod_ops cmasa_slm_mod_ops = {
	SE_MAJOR_VERSION, SE_MINOR_VERSION, SE_MAX_RETRY_LIMIT,
	cmasa_slm_deliver_event
};

/*
 * deliver the event to the plugin if the door exists
 */
static void
post_cmasa_event(sysevent_t ev)
{
	door_arg_t darg;

	darg.data_ptr = ev;
	darg.data_size = sysevent_get_size(ev);
	darg.desc_ptr = NULL;
	darg.desc_num = 0;
	darg.rbuf = NULL;
	darg.rsize = 0;

	if (door_fd >= 0) {
		if (door_call(door_fd, &darg) < 0) {
			close(door_fd);
			door_fd = open(CMASA_EVENT_DOOR, O_RDONLY);
			(void) door_call(door_fd, &darg);
		}
	}
}

/*ARGSUSED*/
static void *
cmasa_slm_deliver_thr(void *args)
{
	ev_queue_t *ev_queue_p;

	for (;;) {
		(void) mutex_lock(&evq_lock);
		while (eventq_head == NULL && cleanup == 0) {
			(void) cond_wait(&evq_cv, &evq_lock);
		}
		ev_queue_p = eventq_head;
		if (eventq_head != NULL) {
			eventq_head = eventq_head->evq_next;
		}
		(void) mutex_unlock(&evq_lock);
		while (ev_queue_p) {
			post_cmasa_event(ev_queue_p->evq_ev);
			free(ev_queue_p->evq_ev);
			free(ev_queue_p);
			ev_queue_p = NULL;
			(void) mutex_lock(&evq_lock);
			if (eventq_head != NULL) {
				ev_queue_p = eventq_head;
				eventq_head = eventq_head->evq_next;
			}
			(void) mutex_unlock(&evq_lock);
		}
		if (cleanup)
			return (NULL);
	}
	/*NOTREACHED*/
}

/*
 * cmasa_slm_deliver_event - called by syseventd to deliver an event buffer.
 *                      The event buffer is subsequently delivered to
 *                      cmasa_d.  If cmasa_d, is not responding to the
 *                      delivery attempt, we will ignore it.
 */
/*ARGSUSED*/
static int
cmasa_slm_deliver_event(sysevent_t *ev, int flag)
{
	size_t ev_size;
	ev_queue_t *new_evq;

	/*
	 * Filter out uninteresting events
	 */
	if (strcmp(sysevent_get_class_name(ev), EC_CLUSTER) != 0) {
		return (0);
	}

	/* Queue event for delivery */
	new_evq = (ev_queue_t *)calloc(1, sizeof (ev_queue_t));
	if (new_evq == NULL) {
		return (EAGAIN);
	}

	ev_size = sysevent_get_size(ev);
	new_evq->evq_ev = (sysevent_t *)malloc(ev_size);
	if (new_evq->evq_ev == NULL) {
		free(new_evq);
		return (EAGAIN);
	}
	bcopy(ev, new_evq->evq_ev, ev_size);

	(void) mutex_lock(&evq_lock);
	if (eventq_head == NULL) {
		eventq_head = new_evq;
	} else {
		eventq_tail->evq_next = new_evq;
	}
	eventq_tail = new_evq;

	(void) cond_signal(&evq_cv);
	(void) mutex_unlock(&evq_lock);

	return (0);
}

struct slm_mod_ops *
slm_init(void)
{
	struct passwd pw;
	struct passwd *retpw;
	char buf[1024];		/* 1k big enough for any password entry */

	/* Make a file descriptor for the CMAS agent */
	retpw = getpwnam_r(CMASA_USERNAME, &pw, buf, 1024);
	if (retpw == NULL) {
		return (NULL);
	}

	door_fd = open(CMASA_EVENT_DOOR, O_CREAT, S_IRUSR | S_IWUSR);

	if (door_fd < 0)
		return (NULL);

	/*
	 * change the door file to be owned by the CMASA owner
	 * so that it can fattach to it to receive events
	 */
	if (fchown(door_fd, retpw->pw_uid, retpw->pw_gid) != 0) {
		return (NULL);
	}

	cleanup = 0;

	eventq_head = NULL;
	eventq_tail = NULL;

	(void) mutex_init(&evq_lock, USYNC_THREAD, NULL);
	(void) cond_init(&evq_cv, USYNC_THREAD, NULL);

	if (thr_create(NULL, NULL, cmasa_slm_deliver_thr,
		NULL, THR_BOUND, &cmasa_slm_deliver_thr_id) != 0) {
		(void) mutex_destroy(&evq_lock);
		(void) cond_destroy(&evq_cv);
		return (NULL);
	}
	return (&cmasa_slm_mod_ops);
}

void
slm_fini(void)
{
	/*
	 * Wait for all events to be sent
	 */
	(void) mutex_lock(&evq_lock);
	cleanup = 1;
	(void) cond_signal(&evq_cv);
	(void) mutex_unlock(&evq_lock);

	/* Wait for delivery thread to exit */
	(void) thr_join(cmasa_slm_deliver_thr_id, NULL, NULL);

	(void) mutex_destroy(&evq_lock);
	(void) cond_destroy(&evq_cv);

	if (door_fd >= 0)
		(void) close(door_fd);
	door_fd = -1;
}
