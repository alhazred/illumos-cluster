/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Adapter mbean interceptor
 */

#pragma ident	"@(#)syseventjni.c	1.8	08/05/20 SMI"

#include "com/sun/cluster/agent/event/SysEventNotifier.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stropts.h>
#include <fcntl.h>
#include <libnvpair.h>
#include <syslog.h>
#include <string.h>
#include <strings.h>
#include <libsysevent.h>
#include <door.h>
#include <stdio.h>
#include <errno.h>

#include "sysevent_cmas.h"

#include <jniutil.h>

#ifndef TRUE
#define	TRUE 1
#endif
#ifndef FALSE
#define	FALSE 0
#endif

#define	MAX_PRIM_TYPE_STR_BUF 100

/*
 * Union holding all types a nvpair can hold
 */
typedef union pairdata_t {
	uchar_t uchar;
	int16_t int16;
	uint16_t uint16;
	int32_t int32;

	uint32_t uint32;
	int64_t int64;
	uint64_t uint64;

	char *str;
} pairdata_t;

static JavaVM *jvm;
static jobject sysEventNotifierObject;
static jmethodID sysEventNotifyMethodID;

static int door_id = -1;

static char *eventClass;

/*
 * Event handler door callback. Invoked by syseventd to deliver an event.
 *
 * Receives events through a door registered with the syseventd SLM
 * mechanism - cannot use the solaris 9 and up proces-local APIs since
 * they require root privilege.
 */

static void event_handler(void *cookie, char *argp, size_t arg_size,
	door_desc_t *dp, uint_t n_desc)
{

	sysevent_t *ev;
	int retcode = 0;

	/*
	 * jvm stuff
	 */
	JNIEnv *env;

	/*
	 * event vars
	 */
	char *class;
	char *subclass;
	char *publisher;
	pid_t pid;
	uint64_t seqno;

	/*
	 * nvlist vars
	 */
	nvlist_t *nvlist;
	nvpair_t *nvpair;
	char *pairname;
	data_type_t pairtype;
	char *pairclassName = NULL;
	int nvlist_retcode;

	/*
	 * string repr of primitive type
	 */
	char pairval[MAX_PRIM_TYPE_STR_BUF] = "";

	char *pairstring;
	pairdata_t pairdata;

	/*
	 * hashmap data
	 */
	jobject hashmap = NULL;
	jclass hashclass;
	jmethodID mid;

	jstring key;
	jobject value;
	jstring classStringRef;
	jstring subclassStringRef;
	jstring publisherStringRef;

	jint retval;

	if (argp == NULL || arg_size == (size_t)0) {
		goto exit;
	}

	ev = (sysevent_t)argp;

	/*
	 * get event header info
	 */
	class = sysevent_get_class_name(ev);
	subclass = sysevent_get_subclass_name(ev);
	sysevent_get_pid(ev, &pid);
	publisher = sysevent_get_pub_name(ev);
	seqno = sysevent_get_seq(ev);

	/*
	 * Try and attach to VM
	 */

	retval = (*jvm)->AttachCurrentThread(jvm, (void **)&env, NULL);
	if (retval < 0) {
		retcode = 1;
		goto exit;
	}
	/*
	 * Translate attributes and add to hashmap
	 */
	if (sysevent_get_attr_list(ev, &nvlist) != -1) {

		/*
		 * Local ref - need to delete ref afterwards
		 */
		hashmap = newObjNullParam(env, "java/util/HashMap");

		if (hashmap == NULL) {
			retcode = 1;
			goto exit;
		}
		/*
		 * local ref - need to delete ref afterwards
		 */
		hashclass = (*env)->GetObjectClass(env, hashmap);

		mid = (*env)->GetMethodID(env,
		    hashclass,
		    "put",
		    "(Ljava/lang/Object;Ljava/lang/Object;)"
		    "Ljava/lang/Object;");

		/*
		 * Iterate through the attrs
		 */
		nvpair = nvlist_next_nvpair(nvlist, NULL);

		/*
		 * This loop converts attributes into strings and * calls
		 * the Java constructor on primitive types with * a String
		 * parameter. This could later be optimized * to use
		 * primitive types directly but the code was * marginally
		 * easier to develop/debug this way */
		while (nvpair != NULL) {

			/*
			 * initialize to point to primitive data
			 */
			pairstring = pairval;

			pairname = nvpair_name(nvpair);
			pairtype = nvpair_type(nvpair);

			switch (pairtype) {
			case DATA_TYPE_BOOLEAN:
				/*
				 * boolean seems to be an exception, no
				 * nvpair_ operation for it
				 */
				nvlist_retcode = nvlist_lookup_boolean(nvlist,
				    pairname);
				if (nvlist_retcode == TRUE) {
					pairstring = "true";
				} else if (nvlist_retcode == FALSE) {
					pairstring = "false";
				} else {
					break;
				}
				pairclassName = "java/lang/Boolean";
				break;
			case DATA_TYPE_BYTE:
				nvlist_retcode = nvpair_value_byte(nvpair,
				    &pairdata.uchar);

				(void) snprintf(pairval, MAX_PRIM_TYPE_STR_BUF,
				    "%c", pairdata.uchar);
				pairclassName = "java/lang/Byte";
				break;

			case DATA_TYPE_INT16:
				nvlist_retcode = nvpair_value_int16(nvpair,
				    &pairdata.int16);

				(void) snprintf(pairval, MAX_PRIM_TYPE_STR_BUF,
				    "%hd", pairdata.int16);
				pairclassName = "java/lang/Short";
				break;
			case DATA_TYPE_UINT16:
				nvlist_retcode = nvpair_value_uint16(nvpair,
				    &pairdata.uint16);

				(void) snprintf(pairval, MAX_PRIM_TYPE_STR_BUF,
				    "%hu", pairdata.uint16);
				pairclassName = "java/lang/Integer";
				break;
			case DATA_TYPE_INT32:
				nvlist_retcode = nvpair_value_int32(nvpair,
				    &pairdata.int32);

				(void) snprintf(pairval, MAX_PRIM_TYPE_STR_BUF,
				    "%d", pairdata.int32);
				pairclassName = "java/lang/Integer";
				break;

			case DATA_TYPE_UINT32:
				nvlist_retcode = nvpair_value_uint32(nvpair,
				    &pairdata.uint32);

				(void) snprintf(pairval, MAX_PRIM_TYPE_STR_BUF,
				    "%u", pairdata.uint32);
				pairclassName = "java/lang/Long";
				break;
			case DATA_TYPE_INT64:
				nvlist_retcode = nvpair_value_int64(nvpair,
				    &pairdata.int64);

				(void) snprintf(pairval, MAX_PRIM_TYPE_STR_BUF,
				    "%ld", pairdata.int64);
				pairclassName = "java/lang/Long";
				break;
			case DATA_TYPE_UINT64:
				nvlist_retcode = nvpair_value_uint64(nvpair,
				    &pairdata.uint64);

				(void) snprintf(pairval, MAX_PRIM_TYPE_STR_BUF,
				    "%lu", pairdata.uint64);
				pairclassName = "java/math/BigInteger";
				break;
			case DATA_TYPE_STRING:

				nvlist_retcode = nvpair_value_string(nvpair,
				    &pairdata.str);

				pairstring = pairdata.str;
				pairclassName = "java/lang/String";
				break;

			case DATA_TYPE_BYTE_ARRAY:
			case DATA_TYPE_INT16_ARRAY:
			case DATA_TYPE_UINT16_ARRAY:
			case DATA_TYPE_INT32_ARRAY:
			case DATA_TYPE_UINT32_ARRAY:
			case DATA_TYPE_INT64_ARRAY:
			case DATA_TYPE_UINT64_ARRAY:
			case DATA_TYPE_STRING_ARRAY:
			case DATA_TYPE_HRTIME:
			case DATA_TYPE_UNKNOWN:
			default:

				/*
				 * indicate error, skip this attr
				 */
				nvlist_retcode = -1;

				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_DEBUG,
					"sysevent_jni(): arg %s "
					"of type %d ignored", pairname,
						pairtype);

				break;
			}

			if (nvlist_retcode < 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_DEBUG,
				    "sysevent_jni(): arg %s return code %d",
				    pairname, nvlist_retcode);
				break;
			}
			/*
			 * New ref - need to delete afterwards
			 */
			key = (*env)->NewStringUTF(env, pairname);

			/*
			 * New ref - need to delete afterwards
			 */
			value = newObjStringParam(env,
			    pairclassName, pairstring);

			if (key == NULL || value == NULL) {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_DEBUG,
					"sysevent_jni(): arg %s unable "
					"to convert to java", pairname);
				break;
			}
			/*
			 * add this element to the hashmap
			 */

			(*env)->CallObjectMethod(env, hashmap,
			    mid, key, value);

			/*
			 * delete local refs
			 */
			(*env)->DeleteLocalRef(env, key);
			(*env)->DeleteLocalRef(env, value);

			/*
			 * move on to next item
			 */
			nvpair = nvlist_next_nvpair(nvlist, nvpair);
		}
		nvlist_free(nvlist);
		nvlist = NULL;
	}
	classStringRef = (*env)->NewStringUTF(env, class);
	subclassStringRef = (*env)->NewStringUTF(env, subclass);
	publisherStringRef = (*env)->NewStringUTF(env, publisher);

	(*env)->CallVoidMethod(env,
	    sysEventNotifierObject,
	    sysEventNotifyMethodID,
	    classStringRef,
	    subclassStringRef,
	    publisherStringRef,
	    (jlong) pid,
	    (jlong) seqno,
	    hashmap);

	/*
	 * delete local refs
	 */
	(*env)->DeleteLocalRef(env, hashmap);
	(*env)->DeleteLocalRef(env, classStringRef);
	(*env)->DeleteLocalRef(env, subclassStringRef);
	(*env)->DeleteLocalRef(env, publisherStringRef);

	/*
	 * Remove ourselves from the JVM
	 */
	(*jvm)->DetachCurrentThread(jvm);

exit:
	(void) door_return(NULL, 0, NULL, 0);
}

/*
 * Class:     SysEventNotifier
 * Method:    event_jni_init
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_com_sun_cluster_agent_event_SysEventNotifier_event_1jni_1init
	(JNIEnv * env, jobject obj, jstring eventClassParam) {

	const char *strPtr;
	jclass sysEventNotifierClass;

	/*
	 * initialize any static handles
	 */
	(*env)->GetJavaVM(env, &jvm);

	sysEventNotifierObject = (*env)->NewGlobalRef(env, obj);

	/*
	 * make local copy of the eventClass parameter
	 */
	strPtr = (*env)->GetStringUTFChars(env, eventClassParam, NULL);
	if (strPtr == NULL)
		return;
	eventClass = strdup(strPtr);
	if (eventClass == NULL)
		return;

	(*env)->ReleaseStringUTFChars(env, eventClassParam, strPtr);

	/*
	 * local ref - need to delete afterwards
	 */
	sysEventNotifierClass =
	    (*env)->FindClass(env,
	    "com/sun/cluster/agent/event/SysEventNotifier");

	sysEventNotifyMethodID =
	    (*env)->GetMethodID(env,
	    sysEventNotifierClass,
	    "sysEventNotify",
	    "("
	    "Ljava/lang/String;"
	    "Ljava/lang/String;"
	    "Ljava/lang/String;"
	    "JJLjava/util/Map;"
	    ")V");

	delRef(env, sysEventNotifierClass);
}

/*
 * Class:     SysEventNotifier
 * Method:    event_jni_start
 * Signature: ()V
 *
 * We receive events through a door from a dedicated syseventd SLM
 */
JNIEXPORT void JNICALL
Java_com_sun_cluster_agent_event_SysEventNotifier_event_1jni_1start
	(JNIEnv * env, jobject obj) {

	struct stat stbuf;

	/*
	 * Open the door to receive sysevent notifications
	 */

	door_id = door_create(event_handler, CMASA_EVENT_DOOR_COOKIE, 0);

	if (door_id < 0)
		return;

	if (stat(CMASA_EVENT_DOOR, &stbuf) < 0) {
		/* No file - can't do anything */
		(void) door_revoke(door_id);
		door_id = -1;
		return;
	}

	fdetach(CMASA_EVENT_DOOR);
	if (fattach(door_id, CMASA_EVENT_DOOR) < 0) {
		if (errno == EBUSY) {
			if (fattach(door_id, CMASA_EVENT_DOOR) >= 0) {
			    return;
			}
		}
		(void) door_revoke(door_id);
		door_id = -1;
	}
}

/*
 * Class:     SysEventNotifier
 * Method:    event_jni_stop
 * Signature: ()V
 *
 */
JNIEXPORT void JNICALL
Java_com_sun_cluster_agent_event_SysEventNotifier_event_1jni_1stop
(JNIEnv * env, jobject obj) {

	fdetach(CMASA_EVENT_DOOR);
	/* Close the door for notifications */
	if (door_id >= 0) {
		(void) door_revoke(door_id);
		door_id = -1;
	}
}
