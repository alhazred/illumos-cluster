//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)SysEventNotifierMBean.java 1.7     08/05/20 SMI"
//

package com.sun.cluster.agent.event;

import java.util.List;
import java.util.Set;


public interface SysEventNotifierMBean {

    /** the TYPE field we use in mbean name */
    public static final String TYPE = "syseventnotifier";

    /**
     * returns the sysevent class names that this notifier is registered to
     * receive
     *
     * @return  returns a Set of Strings containing event class names
     */
    public Set getEventClasses();

    /**
     * Set the time in milliseconds for which we maintain event history
     */
    public void setEventHistoryPeriod(long milliseconds);

    /**
     * Get the time in milliseconds for which we maintain event history
     */
    public long getEventHistoryPeriod();

    /**
     * Get the List of events received over the given history period
     */
    public List getEventHistory();

}
