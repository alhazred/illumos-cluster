//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ClEventDefs.java	1.11	08/07/10 SMI"
 */

package com.sun.cluster.agent.event;

// JDK
import java.io.Serializable;

// Cacao
import com.sun.cacao.Enum;


/**
 * This class defines constants used in cluster events sent by the sysevent API.
 * It has been generated manually with the help of some emacs macros from the
 * file usr/src/common/cl/sys/cl_eventdefs.h and should stay in synchronization
 * with this file
 */
/**
 * hand generated enum from definition in file common/cl/sys/cl_eventdefs.h
 */
public class ClEventDefs {

    /*
     * cl_eventdefs.h - declarations/definitions for generating cluster
     *              events using the sysevent API.
     *
     */

    /**
     * Event class reserved for use by Sun Cluster components. This definition
     * MUST BE in sync with the definition in
     * /usr/include/sys/sysevent/eventdefs.h (PSARC 2001/722).
     */
    public static final String EC_CLUSTER = "EC_Cluster";

    /*
     * Required cluster event attribute names.
     *  Each cluster event is required to provide the following
     *  set of attributes:
     *
     *  - name of the cluster where event took place
     *  - ID of the cluster where event took place
     *  - Nodename of the cluster where event took place
     *  - Event date
     *  - Event time
     *  - Event initiator
     *  - Event severity
     *
     */

    /** Required cluster event attribute names */
    public static final String CL_CLUSTER_NAME = "ev_cluster_name";

    /** Required cluster event attribute names */
    public static final String CL_CLUSTER_ID = "ev_cluster_id";

    /** Required cluster event attribute names */
    public static final String CL_EVENT_NODE = "ev_node_name";

    /** Required cluster event attribute names */
    public static final String CL_EVENT_TS_SEC = "ev_ts_sec";

    /** Required cluster event attribute names */
    public static final String CL_EVENT_TS_USEC = "ev_ts_usec";

    /** Required cluster event attribute names */
    public static final String CL_EVENT_INITIATOR = "ev_initiator";

    /** Required cluster event attribute names */
    public static final String CL_EVENT_SEVERITY = "ev_severity";

    /** Required cluster event attribute names */
    public static final String CL_EVENT_BROADCAST = "ev_broadcast";

    /* Cluster event version */

    public static final String CL_EVENT_VERSION = "ev_version";
    public static final int CL_EVENT_VERSION_NUMBER = 2;

    /* Cluster event publishers */

    public static final String CL_EVENT_PUB_RGM = "rgm";
    public static final String CL_EVENT_PUB_PMF = "pmf";
    public static final String CL_EVENT_PUB_CMM = "cmm";
    public static final String CL_EVENT_PUB_NET = "net";
    public static final String CL_EVENT_PUB_DCS = "dcs";
    public static final String CL_EVENT_PUB_DPM = "dpm";
    public static final String CL_EVENT_PUB_TP = "tp";
    public static final String CL_EVENT_PUB_UCMM = "ucmm";
    public static final String CL_EVENT_PUB_ZONES = "sc_zones";
    public static final String CL_EVENT_PUB_SLM = "slm";

    /*
     * Configuration actions for static configuration
     * change events.
     *
     */

    public static final String CL_CONFIG_ACTION = "config_action";

    /*
     * The following is a set of general purpose event attribute
     * names that should be used for publication of cluster events.
     * Before adding new names to the list, make sure a suitable
     * name is not already on the list.
     *
     */

    public static final String CL_NODE_NAME = "node_name";
    public static final String CL_RT_NAME = "rt_name";
    public static final String CL_RG_NAME = "rg_name";
    public static final String CL_R_NAME = "r_name";
    public static final String CL_QUORUM_NAME = "quorum_name";
    public static final String CL_METHOD_NAME = "method_name";
    public static final String CL_TP_PATH_NAME = "tp_path_name";
    public static final String CL_TP_IF_NAME = "tp_if_name";
    public static final String CL_AFFINITY_RG_NAME = "affinity_rg_name";
    public static final String CL_DS_NAME = "ds_name";
    public static final String CL_DS_SERVICE_CLASS = "ds_service_class";

    public static final String CL_NODE_LIST = "node_list";
    public static final String CL_NODE_STATE_LIST = "state_list";
    public static final String CL_FROM_NODE_LIST = "from_node_list";
    public static final String CL_TO_NODE_LIST = "to_node_list";

    public static final String CL_OLD_STATE = "old_state";
    public static final String CL_NEW_STATE = "new_state";
    public static final String CL_OLD_STATUS = "old_status";
    public static final String CL_NEW_STATUS = "new_status";
    public static final String CL_STATUS_MSG = "status_msg";

    public static final String CL_STEP_NAME = "step_name";
    public static final String CL_SUBSTEP_NAME = "substep_name";
    public static final String CL_START_TIME = "start_time";
    public static final String CL_DURATION = "duration";

    public static final String CL_METHOD_DURATION = "method_duration";
    public static final String CL_METHOD_PATH = "method_path";
    public static final String CL_METHOD_TIMEOUT = "method_timeout";

    public static final String CL_SCHA_API_OPTAG = "scha_api_optag";
    public static final String CL_SCHA_API_FUNC = "scha_api_func";

    public static final String CL_PMF_NAME_TAG = "pmf_name_tag";
    public static final String CL_PMF_CMD_PATH = "pmf_cmd_path";
    public static final String CL_TOTAL_ATTEMPTS = "total_attempts";
    public static final String CL_ATTEMPT_NUMBER = "attempt_number";
    public static final String CL_CMD_PATH = "cmd_path";
    public static final String CL_RETRY_NUMBER = "retry_number";
    public static final String CL_RETRY_COUNT = "retry_count";
    public static final String CL_VOTE_COUNT = "vote_count";
    public static final String CL_DESIRED_PRIMARIES = "desired_primaries";

    public static final String CL_DISK_PATH = "disk_path";

    public static final String CL_RESULT_CODE = "result_code";

    public static final String CL_PROPERTY_NAME = "property_name";
    public static final String CL_PROPERTY_VALUE = "property_value";

    public static final String CL_EVENT_ZONE_STATE = "zone_state";
    public static final String CL_EVENT_ZONE_NAME = "zone_name";

    public static final String CL_DIVIDEND = "value_dividend";
    public static final String CL_DIVISOR = "value_divisor";
    public static final String CL_OBJECT_TYPE = "object_type";
    public static final String CL_OBJECT_INSTANCE = "object_instance";
    public static final String CL_TELEMETRY_ATTRIBUTE = "telemetry_attribute";
    public static final String CL_DIRECTION = "direction";
    public static final String CL_CCR_TABLE_NAME = "ccr_table_name";

    /*
     * Reasons why a given event was generated.
     * Reason codes are used in addition to the event
     * subclass to determine the cause of complex
     * events.
     *
     */

    public static final String CL_REASON_CODE = "reason_code";


    /*
     * Failure reasons for specific types of failure
     * events.
     *
     */
    public static final String CL_FAILURE_REASON = "failure_reason";

    /*
     *  Different states that device services could be in
     *  when an event is generated.
     */
    public static final String CL_DS_STARTING = "ds_starting";
    public static final String CL_DS_STARTED = "ds_started";
    public static final String CL_DS_START_FAILED = "ds_start_failed";
    public static final String CL_DS_STOPPING = "ds_stopping";
    public static final String CL_DS_STOPPED = "ds_stopped";
    public static final String CL_DS_STOP_FAILED = "ds_stop_failed";
    public static final String CL_DS_MAINT_MODE_STARTING =
        "ds_mainenance_mode_starting";
    public static final String CL_DS_MAINT_MODE = "ds_maintenance_mode";
    public static final String CL_DS_MAINT_MODE_FAILED =
        "ds_maintenance_mode_failed";
    public static final String CL_DS_SWITCHOVER_STARTED =
        "ds_switchover_started";
    public static final String CL_DS_SWITCHOVER_ENDED = "ds_switchover_ended";
    public static final String CL_DS_SWITCHOVER_FAILED = "ds_switchover_failed";
    public static final String CL_DS_FAILOVER_STARTED = "ds_failover_started";
    public static final String CL_DS_FAILOVER_ENDED = "ds_failover_ended";
    public static final String CL_DS_FAILOVER_FAILED = "ds_failover_failed";
    public static final String CL_DS_BECOME_PRIMARY_STARTED =
        "ds_become_primary_started";
    public static final String CL_DS_BECOME_PRIMARY_ENDED =
        "ds_become_primary_ended";
    public static final String CL_DS_BECOME_PRIMARY_FAILED =
        "s_become_primary_failed";
    public static final String CL_DS_BECOME_SECONDARY_STARTED =
        "ds_become_secondary_started";
    public static final String CL_DS_BECOME_SECONDARY_ENDED =
        "ds_become_secondary_ended";
    public static final String CL_DS_BECOME_SECONDARY_FAILED =
        "ds_become_secondary_failed";


    /*
     * The following is a list of Sun Cluster 3.x event subclasses
     * that are generated by cluster components.
     *
     */

    /* Generic Events */

    public static final String ESC_CLUSTER_GENERIC_EVENT =
        "ESC_cluster_generic_event";
    public static final String ESC_CLUSTER_CONFIG_CHANGE =
        "ESC_cluster_cofig_change";
    public static final String ESC_CLUSTER_STATE_CHANGE =
        "ESC_cluster_state_change";
    public static final String ESC_ZC_STATE_CHANGE =
        "ESC_zonecluster_state_change";

    public static final String CL_TABLE_NAME = "CCR Table Name";

    /* CMM/UCMM/Quorum Events */

    public static final String ESC_CLUSTER_NODE_CONFIG_CHANGE =
        "ESC_cluster_node_config_change";
    public static final String ESC_CLUSTER_NODE_STATE_CHANGE =
        "ESC_cluster_node_state_change";

    public static final String ESC_CLUSTER_CMM_RECONFIG =
        "ESC_cluster_cmm_reconfig";
    public static final String ESC_CLUSTER_UCMM_RECONFIG =
        "ESC_cluster_ucmm_reconfig";
    public static final String ESC_CLUSTER_UCMM_RECONFIG_SUBSTEP =
        "ESC_cluster_ucmm_reconfig_substep";

    public static final String ESC_CLUSTER_QUORUM_CONFIG_CHANGE =
        "ESC_cluster_quorum_config_change";
    public static final String ESC_CLUSTER_QUORUM_STATE_CHANGE =
        "ESC_cluster_quorum_state_change";

    /* RGM Events */

    public static final String ESC_CLUSTER_MEMBERSHIP =
        "ESC_cluster_membership";

    public static final String ESC_CLUSTER_RG_STATE = "ESC_cluster_rg_state";
    public static final String ESC_CLUSTER_RG_PRIMARIES_CHANGING =
        "ESC_cluster_rg_primaries_changing";
    public static final String ESC_CLUSTER_RG_REMAINING_OFFLINE =
        "ESC_cluster_rg_remaining_offline";
    public static final String ESC_CLUSTER_RG_GIVEOVER_DEFERRED =
        "ESC_cluster_rg_giveover_deferred";
    public static final String ESC_CLUSTER_RG_NODE_REBOOTED =
        "ESC_cluster_rg_node_rebooted";
    public static final String ESC_CLUSTER_RG_CONFIG_CHANGE =
        "ESC_cluster_rg_config_change";

    public static final String ESC_CLUSTER_R_STATE = "ESC_cluster_r_state";
    public static final String ESC_CLUSTER_R_METHOD_COMPLETED =
        "ESC_cluster_r_method_completed";
    public static final String ESC_CLUSTER_R_CONFIG_CHANGE =
        "ESC_cluster_r_config_change";

    public static final String ESC_CLUSTER_FM_R_STATUS_CHANGE =
        "ESC_cluster_fm_r_status_change";
    public static final String ESC_CLUSTER_FM_R_RESTARTING =
        "ESC_cluster_fm_r_restarting";

    public static final String ESC_CLUSTER_SCHA_API_INVALID =
        "ESC_cluster_scha_api_invalid";

    /* PMF Events */

    public static final String ESC_CLUSTER_PMF_PROC_RESTART =
        "ESC_cluster_pmf_proc_restart";
    public static final String ESC_CLUSTER_PMF_PROC_NOT_RESTARTED =
        "ESC_cluster_pmf_proc_not_restarted";

    /* SC_ZONES Events */
    public static final String ESC_CLUSTER_ZONES_STATE =
        "ESC_cluster_zones_state";

    /* SC_SLM Events */

    public static final String ESC_CLUSTER_SLM_THR_STATE =
        "ESC_cluster_slm_thr_state";
    public static final String ESC_CLUSTER_SLM_CONFIG_CHANGE =
        "ESC_cluster_slm_config_change";


    /* Transport Events */

    public static final String ESC_CLUSTER_TP_PATH_CONFIG_CHANGE =
        "ESC_cluster_tp_path_config_change";
    public static final String ESC_CLUSTER_TP_IF_CONFIG_CHANGE =
        "ESC_cluster_tp_path_config_change";
    public static final String ESC_CLUSTER_TP_PATH_STATE_CHANGE =
        "ESC_cluster_tp_path_state_change";
    public static final String ESC_CLUSTER_TP_IF_STATE_CHANGE =
        "ESC_cluster_tp_if_state_change";

    /*
     *  XXX - The following IPMP event subclasses and attribute names
     *        will be handled by Clustering, until IPMP events are
     *        implemented in Solaris.
     *
     */

    public static final String ESC_CLUSTER_IPMP_GROUP_STATE =
        "ESC_cluster_ipmp_group_state";
    public static final String ESC_CLUSTER_IPMP_GROUP_CHANGE =
        "ESC_cluster_ipmp_group_change";
    public static final String ESC_CLUSTER_IPMP_GROUP_MEMBER_CHANGE =
        "ESC_cluster_ipmp_group_member_change";
    public static final String ESC_CLUSTER_IPMP_IF_CHANGE =
        "ESC_cluster_ipmp_if_change";

    public static final String IPMP_GROUP_NAME = "group_name";
    public static final String IPMP_GROUP_STATE = "group_state";
    public static final String IPMP_GROUP_OPERATION = "group_operation";
    public static final String IPMP_IF_NAME = "if_name";
    public static final String IPMP_IF_STATE = "if_state";


    /* DCS Events */

    public static final String ESC_CLUSTER_DCS_CONFIG_CHANGE =
        "ESC_cluster_dcs_config_change";
    public static final String ESC_CLUSTER_DCS_STATE_CHANGE =
        "ESC_cluster_dcs_state_change";
    public static final String ESC_CLUSTER_DCS_PRIMARIES_CHANGING =
        "ESC_cluster_dcs_primaries_changing";

    /* Disk Path Monitoring events */

    public static final String ESC_CLUSTER_DPM_DISK_PATH_OK =
        "ESC_cluster_dpm_disk_path_ok";
    public static final String ESC_CLUSTER_DPM_DISK_PATH_FAILED =
        "ESC_cluster_dpm_disk_path_failed";
    public static final String ESC_CLUSTER_DPM_DISK_PATH_MONITORED =
        "ESC_cluster_dpm_disk_path_monitored";
    public static final String ESC_CLUSTER_DPM_DISK_PATH_UNMONITORED =
        "ESC_cluster_dpm_disk_path_unmonitored";
    public static final String ESC_CLUSTER_DPM_AUTO_REBOOT_NODE =
        "ESC_cluster_dpm_auto_reboot_node";


    public static final Enum IPMP_GROUP_ADD =
        ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED;
    public static final Enum IPMP_GROUP_REMOVE =
        ClEventConfigActionEnum.CL_EVENT_CONFIG_REMOVED;
    public static final Enum IPMP_IF_ADD =
        ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED;
    public static final Enum IPMP_IF_REMOVE =
        ClEventConfigActionEnum.CL_EVENT_CONFIG_REMOVED;

    public static class ClEventConfigActionEnum extends com.sun.cacao.Enum
        implements java.io.Serializable {

        public static final ClEventConfigActionEnum CL_EVENT_CONFIG_ADDED =
            new ClEventConfigActionEnum("CL_EVENT_CONFIG_ADDED", 0);
        public static final ClEventConfigActionEnum CL_EVENT_CONFIG_REMOVED =
            new ClEventConfigActionEnum("CL_EVENT_CONFIG_REMOVED");
        public static final ClEventConfigActionEnum CL_EVENT_CONFIG_ENABLED =
            new ClEventConfigActionEnum("CL_EVENT_CONFIG_ENABLED");
        public static final ClEventConfigActionEnum CL_EVENT_CONFIG_DISABLED =
            new ClEventConfigActionEnum("CL_EVENT_CONFIG_DISABLED");
        public static final
            ClEventConfigActionEnum CL_EVENT_CONFIG_MON_ENABLED =
            new ClEventConfigActionEnum("CL_EVENT_CONFIG_MON_ENABLED");
        public static final
            ClEventConfigActionEnum CL_EVENT_CONFIG_MON_DISABLED =
            new ClEventConfigActionEnum("CL_EVENT_CONFIG_MON_DISABLED");
        public static final ClEventConfigActionEnum CL_EVENT_CONFIG_MANAGED =
            new ClEventConfigActionEnum("CL_EVENT_CONFIG_MANAGED");
        public static final ClEventConfigActionEnum CL_EVENT_CONFIG_UNMANAGED =
            new ClEventConfigActionEnum("CL_EVENT_CONFIG_UNMANAGED");
        public static final
            ClEventConfigActionEnum CL_EVENT_CONFIG_PROP_CHANGED =
            new ClEventConfigActionEnum("CL_EVENT_CONFIG_PROP_CHANGED");
        public static final
            ClEventConfigActionEnum CL_EVENT_CONFIG_VOTE_CHANGE =
            new ClEventConfigActionEnum("CL_EVENT_CONFIG_VOTE_CHANGE");

        protected ClEventConfigActionEnum(String name) {
            super(name);
        }

        protected ClEventConfigActionEnum(String name, int ord) {
            super(name, ord);
        }
    }


    public static class ClEventResultEnum extends com.sun.cacao.Enum
        implements java.io.Serializable {

        public static final ClEventResultEnum CL_RC_OK = new ClEventResultEnum(
                "CL_RC_OK", 0);
        public static final ClEventResultEnum CL_RC_FAILED =
            new ClEventResultEnum("CL_RC_FAILED");
        public static final ClEventResultEnum CL_RC_TIMEOUT =
            new ClEventResultEnum("CL_RC_TIMEOUT");
        public static final ClEventResultEnum CL_RC_RECONFIG =
            new ClEventResultEnum("CL_RC_RECONFIG");

        protected ClEventResultEnum(String name) {
            super(name);
        }

        protected ClEventResultEnum(String name, int ord) {
            super(name, ord);
        }
    }


    public static class ClEventReasonCodeEnum extends com.sun.cacao.Enum
        implements java.io.Serializable {

        public static final ClEventReasonCodeEnum CL_REASON_RG_SWITCHOVER =
            new ClEventReasonCodeEnum("CL_REASON_RG_SWITCHOVER", 0);
        public static final ClEventReasonCodeEnum CL_REASON_RG_RESTART =
            new ClEventReasonCodeEnum("CL_REASON_RG_RESTART");
        public static final ClEventReasonCodeEnum CL_REASON_RG_GIVEOVER =
            new ClEventReasonCodeEnum("CL_REASON_RG_GIVEOVER");
        public static final ClEventReasonCodeEnum CL_REASON_RG_GETOFF =
            new ClEventReasonCodeEnum("CL_REASON_RG_GETOFF");
        public static final ClEventReasonCodeEnum CL_REASON_RG_SC_RESTART =
            new ClEventReasonCodeEnum("CL_REASON_RG_SC_RESTART");
        public static final ClEventReasonCodeEnum CL_REASON_RG_NODE_DIED =
            new ClEventReasonCodeEnum("CL_REASON_RG_NODE_DIED");
        public static final ClEventReasonCodeEnum CL_REASON_RG_REBALANCE =
            new ClEventReasonCodeEnum("CL_REASON_RG_REBALANCE");
        public static final ClEventReasonCodeEnum CL_REASON_RG_FAILBACK =
            new ClEventReasonCodeEnum("CL_REASON_RG_FAILBACK");
        public static final ClEventReasonCodeEnum CL_REASON_RG_AFFINITY =
            new ClEventReasonCodeEnum("CL_REASON_RG_AFFINITY");
        public static final ClEventReasonCodeEnum CL_REASON_R_START_FAILED =
            new ClEventReasonCodeEnum("CL_REASON_R_START_FAILED");
        public static final ClEventReasonCodeEnum CL_REASON_CMM_NODE_JOINED =
            new ClEventReasonCodeEnum("CL_REASON_CMM_NODE_JOINED");
        public static final ClEventReasonCodeEnum CL_REASON_CMM_NODE_LEFT =
            new ClEventReasonCodeEnum("CL_REASON_CMM_NODE_LEFT");
        public static final ClEventReasonCodeEnum CL_REASON_CMM_NODE_SHUTDOWN =
            new ClEventReasonCodeEnum("CL_REASON_CMM_NODE_SHUTDOWN");
        public static final ClEventReasonCodeEnum CL_REASON_PMF_ACTION_SUCCESS =
            new ClEventReasonCodeEnum("CL_REASON_PMF_ACTION_SUCCESS");
        public static final ClEventReasonCodeEnum CL_REASON_PMF_RESTART =
            new ClEventReasonCodeEnum("CL_REASON_PMF_RESTART");
        public static final ClEventReasonCodeEnum CL_REASON_DS_STARTUP =
            new ClEventReasonCodeEnum("CL_REASON_DS_STARTUP");
        public static final ClEventReasonCodeEnum CL_REASON_DS_SWITCHOVER =
            new ClEventReasonCodeEnum("CL_REASON_DS_SWITCHOVER");
        public static final ClEventReasonCodeEnum CL_REASON_DS_FAILOVER =
            new ClEventReasonCodeEnum("CL_REASON_DS_FAILOVER");
        public static final ClEventReasonCodeEnum CL_REASON_UNKNOWN =
            new ClEventReasonCodeEnum("CL_REASON_UNKNOWN");

        protected ClEventReasonCodeEnum(String name) {
            super(name);
        }

        protected ClEventReasonCodeEnum(String name, int ord) {
            super(name, ord);
        }
    }


    public static class ClEventFailCodeEnum extends com.sun.cacao.Enum
        implements java.io.Serializable {

        public static final ClEventFailCodeEnum CL_FR_NO_MASTER =
            new ClEventFailCodeEnum("CL_FR_NO_MASTER", 0);
        public static final ClEventFailCodeEnum CL_FR_PINGPONG =
            new ClEventFailCodeEnum("CL_FR_PINGPONG");
        public static final ClEventFailCodeEnum CL_FR_MON_CHECK_FAILED =
            new ClEventFailCodeEnum("CL_FR_MON_CHECK_FAILED");
        public static final ClEventFailCodeEnum CL_FR_BUSY =
            new ClEventFailCodeEnum("CL_FR_BUSY");
        public static final ClEventFailCodeEnum CL_FR_FROZEN =
            new ClEventFailCodeEnum("CL_FR_FROZEN");
        public static final ClEventFailCodeEnum CL_FR_PMF_ACTION_FAILED =
            new ClEventFailCodeEnum("CL_FR_PMF_ACTION_FAILED");
        public static final ClEventFailCodeEnum CL_FR_PMF_PROC_THROTTLED =
            new ClEventFailCodeEnum("CL_FR_PMF_PROC_THROTTLED");
        public static final ClEventFailCodeEnum CL_FR_PMF_RETRIES_EXCEEDED =
            new ClEventFailCodeEnum("CL_FR_PMF_RETRIES_EXCEEDED");

        protected ClEventFailCodeEnum(String name) {
            super(name);
        }

        protected ClEventFailCodeEnum(String name, int ord) {
            super(name, ord);
        }
    }


    public static class ClEventSchaFuncEnum extends com.sun.cacao.Enum
        implements java.io.Serializable {

        public static final ClEventSchaFuncEnum CL_SCHA_API_QUERY =
            new ClEventSchaFuncEnum("CL_SCHA_API_QUERY", 0);
        public static final ClEventSchaFuncEnum CL_SCHA_API_SET_STATUS =
            new ClEventSchaFuncEnum("CL_SCHA_API_SET_STATUS");
        public static final ClEventSchaFuncEnum CL_SCHA_API_SCHA_CONTROL =
            new ClEventSchaFuncEnum("CL_SCHA_API_SCHA_CONTROL");

        protected ClEventSchaFuncEnum(String name) {
            super(name);
        }

        protected ClEventSchaFuncEnum(String name, int ord) {
            super(name, ord);
        }
    }


    public static class ClEventIpmpGroupStateEnum extends com.sun.cacao.Enum
        implements java.io.Serializable {

        public static final ClEventIpmpGroupStateEnum IPMP_GROUP_OK =
            new ClEventIpmpGroupStateEnum("IPMP_GROUP_OK", 0);
        public static final ClEventIpmpGroupStateEnum IPMP_GROUP_FAILED =
            new ClEventIpmpGroupStateEnum("IPMP_GROUP_FAILED");

        public ClEventIpmpGroupStateEnum(String name) {
            super(name);
        }

        public ClEventIpmpGroupStateEnum(String name, int ord) {
            super(name, ord);
        }
    }

    /* Cluster event severities */

    public static class ClEventSeverityEnum extends com.sun.cacao.Enum
        implements java.io.Serializable {

        public static final ClEventSeverityEnum CL_EVENT_SEV_INFO =
            new ClEventSeverityEnum("CL_EVENT_SEV_INFO", 0);
        public static final ClEventSeverityEnum CL_EVENT_SEV_WARNING =
            new ClEventSeverityEnum("CL_EVENT_SEV_WARNING");
        public static final ClEventSeverityEnum CL_EVENT_SEV_ERROR =
            new ClEventSeverityEnum("CL_EVENT_SEV_ERROR");
        public static final ClEventSeverityEnum CL_EVENT_SEV_CRITICAL =
            new ClEventSeverityEnum("CL_EVENT_SEV_CRITICAL");
        public static final ClEventSeverityEnum CL_EVENT_SEV_FATAL =
            new ClEventSeverityEnum("CL_EVENT_SEV_FATAL");

        protected ClEventSeverityEnum(String name) {
            super(name);
        }

        protected ClEventSeverityEnum(String name, int ord) {
            super(name, ord);
        }
    }

    /* Cluster event initiator codes */

    public static class ClEventInitiatorEnum extends com.sun.cacao.Enum
        implements java.io.Serializable {

        public static final ClEventInitiatorEnum CL_EVENT_INIT_UNKNOWN =
            new ClEventInitiatorEnum("CL_EVENT_INIT_UNKNOWN", 0);
        public static final ClEventInitiatorEnum CL_EVENT_INIT_SYSTEM =
            new ClEventInitiatorEnum("CL_EVENT_INIT_SYSTEM");
        public static final ClEventInitiatorEnum CL_EVENT_INIT_OPERATOR =
            new ClEventInitiatorEnum("CL_EVENT_INIT_OPERATOR");
        public static final ClEventInitiatorEnum CL_EVENT_INIT_AGENT =
            new ClEventInitiatorEnum("CL_EVENT_INIT_AGENT");

        protected ClEventInitiatorEnum(String name) {
            super(name);
        }

        protected ClEventInitiatorEnum(String name, int ord) {
            super(name, ord);
        }
    }

    public static class ClEventErrorEnum extends com.sun.cacao.Enum
        implements java.io.Serializable {
        public static final ClEventErrorEnum CL_EVENT_ENOMEM =
            new ClEventErrorEnum("CL_EVENT_ENOMEM", 0);
        public static final ClEventErrorEnum CL_EVENT_ECLADM =
            new ClEventErrorEnum("CL_EVENT_ECLADM");
        public static final ClEventErrorEnum CL_EVENT_EATTR =
            new ClEventErrorEnum("CL_EVENT_EATTR");
        public static final ClEventErrorEnum CL_EVENT_EINVAL =
            new ClEventErrorEnum("CL_EVENT_EINVAL");
        public static final ClEventErrorEnum CL_EVENT_EPOST =
            new ClEventErrorEnum("CL_EVENT_EPOST");

        protected ClEventErrorEnum(String name) {
            super(name);
        }

        protected ClEventErrorEnum(String name, int ord) {
            super(name, ord);
        }
    }
}
