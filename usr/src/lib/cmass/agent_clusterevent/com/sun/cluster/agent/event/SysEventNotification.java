//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)SysEventNotification.java 1.8     08/12/03 SMI"
//

package com.sun.cluster.agent.event;

import java.util.Date;
import java.util.Map;


/**
 * Notification generated when a service object is created/destroyed
 */
public class SysEventNotification extends javax.management.Notification
    implements java.io.Serializable {

    /** prefix for type strings */
    private static final String key = "com.sun.cluster.agent.event.";

    /** internal reference */
    private String clazz;

    /** internal reference */
    private String subclass;

    private ClEventDefs.ClEventSeverityEnum severity;


    /** internal reference */
    private String publisher;

    /** internal reference */
    private long eventSeqNo;

    /** internal reference */
    private long pid;

    /** private reference */
    private Map attrs;

    /**
     * Creates a new instance of SysEventNotification
     *
     * @param  type  String defining notification type (equivalent to subclass)
     * @param  source  object reference to source of notification
     * @param  sequenceNumber  unique for this type of notification from this
     * source (different to sysevent sequence number which is unique to the
     * sysevent class/subclass)
     * @param  clazz  the sysevent class name (not a java class)
     * @param  subclass  the sysevent subclass name (not a java class)
     * @param  eventSeqNo  the sysevent sequence number
     * @param  publisher  the sysevent publisher name (not a java class)
     * @param  pid  the process id of the sysevent publisher
     * @param  attrs  the map of event attributes
     */
    public SysEventNotification(String type, Object source, long sequenceNumber,
        String clazz, String subclass, ClEventDefs.ClEventSeverityEnum severity,
        long eventSeqNo, String publisher, long pid, Map attrs) {

        this(type, source, sequenceNumber, new Date().getTime(), clazz,
            subclass, severity, eventSeqNo, publisher, pid, attrs);
    }

    /**
     * Creates a new instance of SysEventNotification with a given timeStamp
     *
     * @param  type  String defining notification type (CREATE/DESTROY)
     * @param  source  object reference to source of notification
     * @param  sequenceNumber  unique for this type of notification from this
     * source (sysevent sequence number is used)
     * @param  timeStamp  timestamp for origin of notification
     * @param  clazz  the sysevent class name (not a java class)
     * @param  subclass  the sysevent subclass name (not a java class)
     * @param  severity  Severity of the the sysevent
     * @param  eventSeqNo  the sysevent sequence number
     * @param  publisher  the sysevent publisher name (not a java class)
     * @param  pid  the process id of the sysevent publisher
     * @param  attrs  the map of event attributes
     */

    public SysEventNotification(String type, Object source, long sequenceNumber,
        long timeStamp, String clazz, String subclass,
        ClEventDefs.ClEventSeverityEnum severity, long eventSeqNo,
        String publisher, long pid, Map attrs) {
        super(type, source, sequenceNumber, timeStamp);

        this.clazz = clazz;
        this.subclass = subclass;
        this.severity = severity;
        this.publisher = publisher;
        this.eventSeqNo = eventSeqNo;
        this.pid = pid;
        this.attrs = attrs;
    }

    /**
     * Get the value of the event class.
     *
     * @return  value of event class.
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * Get the value of the event subclass.
     *
     * @return  value of event subclass.
     */
    public String getSubclass() {
        return subclass;
    }

    /**
     * get the value of the event severity
     *
     * @return  a ClEventSeverityEnum instance
     */
    public ClEventDefs.ClEventSeverityEnum getSeverity() {
        return severity;
    }

    /**
     * Get the value of the event publisher.
     *
     * @return  value of event publisher.
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Get the value of the sysevent sequence number.
     *
     * @return  value of event originators sequence number.
     */
    public long getEventSeqNo() {
        return eventSeqNo;
    }

    /**
     * Get the value of the event originators pid.
     *
     * @return  value of event originators pid.
     */
    public long getPid() {
        return pid;
    }

    /**
     * Get the attributes Map
     *
     * <p>Attribute ordering is not maintained, attribute names are used to key
     * into the attribute map. Sysevent attributes are converted into Java
     * objects using the following mapping: <code>
     * <table>
     * <tr>
     * <td>DATA_TYPE_BOOLEAN</td>
     * <td>java.lang.Boolean</td>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_BYTE</td>
     * <td>java.lang.Byte</td>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_INT16</td>
     * <td>java.lang.Short</td>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_UINT16</td>
     * <td>java.lang.Integer</td>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_INT32</td>
     * <td>java.lang.Integer</td>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_UINT32</td>
     * <td>java.lang.Long</td>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_INT64</td>
     * <td>java.lang.Long</td>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_UINT64</td>
     * <td>java.math.BigInteger</td>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_STRING</td>
     * <td>java.lang.String</td>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_BYTE_ARRAY</td>
     * <td><i>not yet mapped</i>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_INT16_ARRAY</td>
     * <td><i>not yet mapped</i>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_UINT16_ARRAY</td>
     * <td><i>not yet mapped</i>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_INT32_ARRAY</td>
     * <td><i>not yet mapped</i>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_UINT32_ARRAY</td>
     * <td><i>not yet mapped</i>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_INT64_ARRAY</td>
     * <td><i>not yet mapped</i>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_UINT64_ARRAY</td>
     * <td><i>not yet mapped</i>
     * </tr>
     * <tr>
     * <td>DATA_TYPE_STRING_ARRAY</td>
     * <td><i>not yet mapped</i>
     * </tr>
     * </table>
     * </code>
     *
     * @return  attributes Map
     */
    public Map getAttrs() {
        return attrs;
    }

    public String toString() {
        return "TimeStamp<" + getTimeStamp() + "> " + "Seq<" +
            getSequenceNumber() + "> " + "Class<" + getClazz() + "> " +
            "SubClass<" + getSubclass() + "> " + "Severity<" + getSeverity() +
            "> " + "Pid<" + getPid() + "> " + "Publisher<" + getPublisher() +
            "> " + "EventSeqNo<" + getEventSeqNo() + "> " + "Attrs = " +
            getAttrs();
    }
}
