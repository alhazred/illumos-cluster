/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SysEventModule.java 1.10     08/12/03 SMI"
 */

package com.sun.cluster.agent.event;

// JDK

// Cacao
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// Local
import com.sun.cluster.common.ClusterRuntimeException;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.ObjectName;


/**
 * Module responsible for listening to, and forwarding, cluster events.
 */
public class SysEventModule extends Module {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.event");

    /** Name of property for event history period in milliseconds */
    public static final String EVENT_HISTORY_PERIOD =
        "event.history.milliseconds";

    /* Internal reference to our notifier object */
    private SysEventNotifier notifier;

    /* Internal reference to our objectname */
    private ObjectName objName;

    /**
     * Constructor called by the container
     *
     * @param  descriptor
     */
    public SysEventModule(DeploymentDescriptor descriptor) {
        super(descriptor);

        Map parameters = descriptor.getParameters();
        int eventHistoryPeriod = 30000; // Default to 30 seconds

        try {

            // Load the agent JNI library
            String library = (String) parameters.get("library");
            Runtime.getRuntime().load(library);

            // Set event history period (default is 30 seconds)
            eventHistoryPeriod = new Integer((String) parameters.get(
                        EVENT_HISTORY_PERIOD)).intValue();

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING,
                "Unable to load native runtime library" + ule);
            throw ule;

        } catch (Exception e) {
            logger.fine("Unable to parse property " + EVENT_HISTORY_PERIOD);
        }

        objName =
            new ObjectNameFactory(getDomainName()).getObjectName(
                SysEventNotifierMBean.class, null);
        notifier = new SysEventNotifier(objName, ClEventDefs.EC_CLUSTER,
                eventHistoryPeriod);
    }

    /**
     * Invoked by the container when starting up
     *
     * @exception  ClusterRuntimeException  if unable to start
     */
    protected void start() throws ClusterRuntimeException {

        // Register the notifier as an mbean
        try {
            getMbs().registerMBean(notifier, objName);
            notifier.start();

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING,
                "Unable to start Cluster Event module" + e);
            throw new ClusterRuntimeException(e.getMessage());
        }
    }

    protected void stop() {

        try {
            notifier.stop();
            getMbs().unregisterMBean(objName);
        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING,
                "Unable to stop Cluster Event module" + e);
            throw new ClusterRuntimeException(e.getMessage());
        }
    }
}
