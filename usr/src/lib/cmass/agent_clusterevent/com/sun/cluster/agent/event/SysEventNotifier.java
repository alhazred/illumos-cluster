/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)SysEventNotifier.java	1.14	09/03/27 SMI"
 */

package com.sun.cluster.agent.event;

// JDK
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

// JMX
import javax.management.Notification;
import javax.management.ObjectName;

// Cacao
import com.sun.cacao.Enum;

// Local
import com.sun.cluster.common.ClusterRuntimeException;



/**
 * This class provides an interface to the sysevents from the platform
 */
public class SysEventNotifier extends com.sun.cacao.element.ElementSupport
    implements SysEventNotifierMBean, javax.management.NotificationBroadcaster {

    /** tracing utility reference */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.event");

    /** private reference to singleton instance */
    private static SysEventNotifier myself;

    /** private internal reference */
    private static boolean initialized = false;

    /** private timer task for scheduling notification deliveries */
    private static Timer timer = new Timer(true);

    /** private internal reference to notification sequence number */
    private long notifCount = 0;

    /** private internal reference to event classes we must listen to */
    private Set eventClasses = new HashSet();

    /** period in milliseconds to hold on to events */
    private long eventHistoryPeriod;

    private LinkedList eventHistory = new LinkedList();

    private ObjectName objName;

    // Default values
    private boolean running = false;

    /**
     * Constructor, initializes handler, must not lock resources
     */
    public SysEventNotifier(ObjectName objName, String eventClass,
        int eventHistoryPeriod) {

        super(objName);

        if (initialized || (!ClEventDefs.EC_CLUSTER.equals(eventClass))) {
            throw new ClusterRuntimeException("only supports singleton " +
                ClEventDefs.EC_CLUSTER);
        }

        this.eventHistoryPeriod = eventHistoryPeriod;
        this.objName = objName;

        eventClasses.add(eventClass);
        event_jni_init(eventClass);
        initialized = true;
    }

    /**
     * Set the time in milliseconds for which we maintain event history
     */
    public void setEventHistoryPeriod(long milliseconds) {
        eventHistoryPeriod = milliseconds;
    }

    /**
     * Get the time in milliseconds for which we maintain event history
     */
    public long getEventHistoryPeriod() {
        return eventHistoryPeriod;
    }

    public List getEventHistory() {
        eventHistoryPurgeOldEvents();

        return (List) eventHistory.clone();
    }


    /**
     * returns the sysevent class names that this notifier is registered to
     * receive
     *
     * @return  returns a Set of Strings containing event class names
     */
    public Set getEventClasses() {
        return eventClasses;
    }

    /**
     * start the service - register to receive events
     *
     * @throws  ClusterRuntimeException  in case of initialization error
     */
    public synchronized void start() throws ClusterRuntimeException {
        event_jni_start();
        running = true;
    }

    /**
     * Stop the sysevent service, no longer send notifications
     */
    public void stop() {
        event_jni_stop();
    }

    /**
     * internal method invoked from native code to send sysevent
     *
     * @param  clazz  event classname (not a java class)
     * @param  subclass  event subclass name (not a java class)
     * @param  publisher  event publisher string
     * @param  pid  event publishers' pid value
     * @param  seqNo  sysevent sequence number
     * @param  attrs  Map containing key/value pairs for all event attributes
     */
    private void sysEventNotify(String clazz, String subclass, String publisher,
        long pid, long seqNo, Map attrs) {

        // Pick up the timestamp from the event itself
        // fallback to a locally created timestamp
        long ts_sec = 0;
        long ts_usec = 0;

        try {
            ts_sec = ((Long) attrs.get(ClEventDefs.CL_EVENT_TS_SEC))
                .longValue();
            ts_usec = ((Long) attrs.get(ClEventDefs.CL_EVENT_TS_USEC))
                .longValue();

            // If we got the time from specific attributes, remove it
            attrs.remove(ClEventDefs.CL_EVENT_TS_SEC);
            attrs.remove(ClEventDefs.CL_EVENT_TS_USEC);
        } catch (Exception e) {
            ts_usec = new java.util.Date().getTime() * 1000;
        }

        // workaround for JDK1.5/1.6 to make sure the Enum is referenced.
        ClEventDefs.ClEventSeverityEnum severity =
            ClEventDefs.ClEventSeverityEnum.CL_EVENT_SEV_INFO;
        Long severityLong = (Long) attrs.remove(ClEventDefs.CL_EVENT_SEVERITY);

        try {
            severity = (ClEventDefs.ClEventSeverityEnum)
                ClEventDefs.ClEventSeverityEnum.getEnum(
                    ClEventDefs.ClEventSeverityEnum.class,
                    severityLong.intValue());
        } catch (Exception e) {
            logger.warning("Unexpected event severity " + severityLong);
        }

        SysEventNotification notif = new SysEventNotification(subclass, objName,
                notifCount++, ((ts_sec * 1000000) + ts_usec)/1000, clazz,
                subclass, severity, seqNo, publisher, pid, attrs);

        eventHistory.add(notif);
        eventHistoryPurgeOldEvents();

        timer.schedule(new NotificationTask(notif), 0);

    }

    /**
     * Helper method, delete all events that are too old
     */
    private void eventHistoryPurgeOldEvents() {

        // Time before which all records should be deleted
        long time = new Date().getTime() - eventHistoryPeriod;

        try {

            while (true) {
                SysEventNotification notif = (SysEventNotification) eventHistory
                    .getFirst();

                if ((notif.getTimeStamp() - time) >= 0) {
                    break;
                }

                eventHistory.removeFirst();
            }
        } catch (NoSuchElementException e) {
            // Expected when no elements in list
        }
    }

    // Native method declaration
    private native void event_jni_init(String eventClass);

    private native void event_jni_start();

    private native void event_jni_stop();

    /**
     * Helper internal class for delivering notifications
     */
    private class NotificationTask extends TimerTask {

        /** Internal reference */
        private Notification notif;

        /**
         * constructor takes notif to deliver
         */
        private NotificationTask(Notification notif) {
            this.notif = notif;
        }

        /**
         * instantiated in Timer's thread when a notification requires delivery
         */
        public void run() {
            sendNotification(notif);
            logger.fine("Sent cluster notification " + notif);
        }
    }
}
