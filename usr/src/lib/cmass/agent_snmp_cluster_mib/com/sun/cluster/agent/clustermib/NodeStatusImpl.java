/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NodeStatusImpl.java 1.7     08/05/20 SMI"
 */

// JDK
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX 
import javax.management.MBeanServer;
import javax.management.snmp.SnmpStatusException;
import javax.management.snmp.SnmpString;

// JDMK 
import com.sun.jdmk.snmp.agent.SnmpMib;

// local
import com.sun.cluster.agent.clustermib.mibgen.*;


/**
 * The class is used for implementing the "NodeStatus" group. The group is
 * defined with the following oid: 1.3.6.1.4.1.42.2.80.1.1.1.2.1.
 */
public class NodeStatusImpl extends NodeStatus implements NodeStatusMBean,
    Serializable {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.clustermib");

    /**
     * Variable for storing the value of "NodeDeviceTable". The variable is
     * identified by: "1.3.6.1.4.1.42.2.80.1.1.1.2.1.2". "MIB tree branch"
     */
    protected TableNodeDeviceTable NodeDeviceTable;

    /**
     * Variable for storing the value of "NodeTable". The variable is identified
     * by: "1.3.6.1.4.1.42.2.80.1.1.1.2.1.1". "MIB tree branch"
     */
    protected TableNodeTable NodeTable;

    /**
     * Constructor for the "NodeStatus" group. If the group contains a table,
     * the entries created through an SNMP SET will be AUTOMATICALLY REGISTERED
     * in Java DMK.
     */
    public NodeStatusImpl(SnmpMib myMib, MBeanServer server) {
        super(myMib, server);
    }

    /**
     * Access the "NodeDeviceTable" variable.
     */
    public TableNodeDeviceTable accessNodeDeviceTable()
        throws SnmpStatusException {
        return NodeDeviceTable;
    }

    /**
     * Access the "NodeDeviceTable" variable as a bean indexed property.
     */
    public NodeDeviceTableEntryMBean[] getNodeDeviceTable()
        throws SnmpStatusException {
        return NodeDeviceTable.getEntries();
    }

    /**
     * Access the "NodeTable" variable.
     */
    public TableNodeTable accessNodeTable() throws SnmpStatusException {
        return NodeTable;
    }

    /**
     * Access the "NodeTable" variable as a bean indexed property.
     */
    public NodeTableEntryMBean[] getNodeTable() throws SnmpStatusException {
        return NodeTable.getEntries();
    }

}
