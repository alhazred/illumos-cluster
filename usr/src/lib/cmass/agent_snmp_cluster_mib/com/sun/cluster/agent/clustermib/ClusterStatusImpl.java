/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ClusterStatusImpl.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.agent.clustermib;

// JDK
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServer;
import javax.management.snmp.SnmpStatusException;
import javax.management.snmp.SnmpString;

// JDMK
import com.sun.jdmk.snmp.agent.SnmpMib;

// local
import com.sun.cluster.agent.JmxAgent;
import com.sun.cluster.agent.cluster.Cluster;
import com.sun.cluster.agent.cluster.ClusterMBean;
import com.sun.cluster.agent.clustermib.mibgen.ClusterStatusMBean;


/**
 * The class is used for implementing the "ClusterStatus" group. The group is
 * defined with the following oid: 1.3.6.1.4.1.42.2.80.1.1.1.1.1.
 */
public class ClusterStatusImpl
    extends com.sun.cluster.agent.clustermib.mibgen.ClusterStatus
    implements ClusterStatusMBean, Serializable {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.clustermib");

    private ClusterMBean clusterMBean = null;

    /**
     * Variable for storing the value of "ClsCurrentVotes". The variable is
     * identified by: "1.3.6.1.4.1.42.2.80.1.1.1.1.1.3". "Integer value"
     */
    protected Integer ClsCurrentVotes = new Integer(1);

    /**
     * Variable for storing the value of "ClsMinVotesRequired". The variable is
     * identified by: "1.3.6.1.4.1.42.2.80.1.1.1.1.1.2". "Minimum votes required
     * to get cluster quorum"
     */
    protected Integer ClsMinVotesRequired = new Integer(1);

    /**
     * Constructor for the "ClusterStatus" group. If the group contains a table,
     * the entries created through an SNMP SET will be AUTOMATICALLY REGISTERED
     * in Java DMK.
     */
    public ClusterStatusImpl(SnmpMib myMib, MBeanServer server) {
        super(myMib, server);

        try {

            if (server == null) {
                server = com.sun.cluster.container.Container.getMbs();
            }

            clusterMBean = (ClusterMBean) JmxAgent.getMBeanProxy(server,
                    ClusterMBean.class, null, false, false);
        } catch (Exception e) {
            logger.log(Level.WARNING, "Caught exception", e);
        }
    }

    /**
     * Getter for the "ClsCurrentVotes" variable.
     */
    public Integer getClsCurrentVotes() throws SnmpStatusException {
        return ClsCurrentVotes;
    }

    /**
     * Getter for the "ClsMinVotesRequired" variable.
     */
    public Integer getClsMinVotesRequired() throws SnmpStatusException {
        return ClsMinVotesRequired;
    }

    /**
     * Getter for the "ClsClusterName" variable. The variable is identified by:
     * "1.3.6.1.4.1.42.2.80.1.1.1.1.1.1".
     */
    public String getClsClusterName() throws SnmpStatusException {
        return clusterMBean.getName();
    }

}
