/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ClusterConfigurationImpl.java 1.7     08/05/20 SMI"
 */

// JDK
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.MBeanServer;
import javax.management.snmp.SnmpStatusException;
import javax.management.snmp.SnmpString;

// JDMK
import com.sun.jdmk.snmp.agent.SnmpMib;

// local
import com.sun.cluster.agent.JmxAgent;
import com.sun.cluster.agent.cluster.Cluster;
import com.sun.cluster.agent.cluster.ClusterMBean;
import com.sun.cluster.agent.clustermib.mibgen.*;


/**
 * The class is used for implementing the "ClusterConfiguration" group. The
 * group is defined with the following oid: 1.3.6.1.4.1.42.2.80.1.1.1.1.2.
 */
public class ClusterConfigurationImpl extends ClusterConfiguration
    implements ClusterConfigurationMBean, Serializable {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.clustermib");

    private ClusterMBean clusterMBean = null;

    /**
     * Variable for storing the value of "ClcAddNodeAuthList". The variable is
     * identified by: "1.3.6.1.4.1.42.2.80.1.1.1.1.2.6". "Character string
     * value"
     */
    protected String ClcAddNodeAuthList = new String("XXX TODO XXX");

    /**
     * Variable for storing the value of "ClcAddNodeAuthType". The variable is
     * identified by: "1.3.6.1.4.1.42.2.80.1.1.1.1.2.5". "Character string
     * value"
     */
    protected String ClcAddNodeAuthType = new String("XXX TODO XXX");

    /**
     * Variable for storing the value of "ClcPrivateNetMask". The variable is
     * identified by: "1.3.6.1.4.1.42.2.80.1.1.1.1.2.4". "Character string
     * value"
     */
    protected String ClcPrivateNetMask = new String("XXX TODO XXX");

    /**
     * Variable for storing the value of "ClcPrivateNetAddr". The variable is
     * identified by: "1.3.6.1.4.1.42.2.80.1.1.1.1.2.3". "Character string
     * value"
     */
    protected String ClcPrivateNetAddr = new String("XXX TODO XXX");

    /**
     * Variable for storing the value of "ClcInstallMode". The variable is
     * identified by: "1.3.6.1.4.1.42.2.80.1.1.1.1.2.2". "Character string
     * value"
     */
    protected String ClcInstallMode = new String("XXX TODO XXX");

    /**
     * Constructor for the "ClusterConfiguration" group. If the group contains a
     * table, the entries created through an SNMP SET will be AUTOMATICALLY
     * REGISTERED in Java DMK.
     */
    public ClusterConfigurationImpl(SnmpMib myMib, MBeanServer server) {

        super(myMib, server);

        try {

            if (server == null) {
                server = com.sun.cluster.container.Container.getMbs();
            }

            clusterMBean = (ClusterMBean) JmxAgent.getMBeanProxy(server,
                    ClusterMBean.class, null, false, false);
        } catch (Exception e) {
            logger.log(Level.WARNING, "Caught exception", e);
        }

    }

    /**
     * Get the "ClcAddNodeAuthList" variable.
     */
    public String getClcAddNodeAuthList() throws SnmpStatusException {
        return ClcAddNodeAuthList;
    }

    /**
     * Get the "ClcAddNodeAuthType" variable.
     */
    public String getClcAddNodeAuthType() throws SnmpStatusException {
        return ClcAddNodeAuthType;
    }

    /**
     * Get the "ClcPrivateNetMask" variable.
     */
    public String getClcPrivateNetMask() throws SnmpStatusException {
        return ClcPrivateNetMask;
    }

    /**
     * Get for the "ClcPrivateNetAddr" variable.
     */
    public String getClcPrivateNetAddr() throws SnmpStatusException {
        return ClcPrivateNetAddr;
    }

    /**
     * Get the "ClcInstallMode" variable.
     */
    public String getClcInstallMode() throws SnmpStatusException {
        return ClcInstallMode;
    }

    /**
     * Get the "ClcClusterName" variable. The variable is identified by:
     * "1.3.6.1.4.1.42.2.80.1.1.1.1.2.1".
     */
    public String getClcClusterName() throws SnmpStatusException {
        return clusterMBean.getName();
    }

}
