/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SUN_CLUSTER_MIB_IMPL.java 1.7     08/05/20 SMI"
 */

// Local stuff
import com.sun.cluster.agent.clustermib.mibgen.*;

// JDK
import java.io.Serializable;
import java.util.Hashtable;

// JMX
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.InstanceAlreadyExistsException;

// JDMK 
import com.sun.jdmk.snmp.agent.SnmpMib;
import com.sun.jdmk.snmp.agent.SnmpMibNode;
import com.sun.jdmk.snmp.agent.SnmpMibTable;
import com.sun.jdmk.snmp.agent.SnmpStandardObjectServer;


/**
 * The class is used for representing "SUN-CLUSTER-MIB". You can edit the file
 * if you want to modify the behaviour of the MIB.
 */
public class SUN_CLUSTER_MIB_IMPL extends SUN_CLUSTER_MIB
    implements Serializable {

    /**
     * Default constructor. Initialize the Mib tree.
     */
    public SUN_CLUSTER_MIB_IMPL() {
        super();
    }

    /**
     * Factory method for "ClusterStatus" group MBean. You can redefine this
     * method if you need to replace the default generated MBean class with your
     * own customized class.
     *
     * @param  groupName  Name of the group ("ClusterStatus")
     * @param  groupOid  OID of this group
     * @param  groupObjname  ObjectName for this group (may be null)
     * @param  server  MBeanServer for this group (may be null)
     *
     * @return  An instance of the MBean class generated for the "ClusterStatus"
     * group (ClusterStatus) Note that when using standard metadata, the
     * returned object must implement the "ClusterStatusMBean" interface.
     */
    protected Object createClusterStatusMBean(String groupName, String groupOid,
        ObjectName groupObjname, MBeanServer server) {

        return new ClusterStatusImpl(this, server);
    }

    /**
     * Factory method for "ClusterConfiguration" group MBean. You can redefine
     * this method if you need to replace the default generated MBean class with
     * your own customized class.
     *
     * @param  groupName  Name of the group ("ClusterConfiguration")
     * @param  groupOid  OID of this group
     * @param  groupObjname  ObjectName for this group (may be null)
     * @param  server  MBeanServer for this group (may be null)
     *
     * @return  An instance of the MBean class generated for the
     * "ClusterConfiguration" group (ClusterConfiguration) Note that when using
     * standard metadata, the returned object must implement the
     * "ClusterConfigurationMBean" interface.
     */
    protected Object createClusterConfigurationMBean(String groupName,
        String groupOid, ObjectName groupObjname, MBeanServer server) {

        return new ClusterConfigurationImpl(this, server);
    }

}
