/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ClusterMibModule.java 1.8     08/05/20 SMI"
 */
package com.sun.cluster.agent.clustermib;

// JDK
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.ObjectName;

// JDMK
import com.sun.jdmk.comm.SnmpAdaptorServer;

// local
import com.sun.cluster.agent.JmxAgent;
import com.sun.cluster.agent.ObjectNameFactory;
import com.sun.cluster.common.ClusterRuntimeException;


/**
 * Clustermib module using the interceptor and JNI design patterns
 */
public class ClusterMibModule extends com.sun.cluster.container.Module
    implements com.sun.cluster.container.ModuleMBean {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.clustermib");

    /** private reference to our MIB */
    private static com.sun.jdmk.snmp.agent.SnmpMib mib;

    public static final String MODULE_TYPE = "agent_snmp_clustermib";


    /**
     * Constructor called by the container.
     *
     * @param  container  backreference to our container.
     */
    public ClusterMibModule(com.sun.cluster.container.Container container,
        java.util.Properties properties) {

        super(container, properties);

    }

    /**
     * Utility for retrieving the module type.
     *
     * @return  Module instance name.
     */
    protected String getModuleInstanceName() {
        return MODULE_TYPE;
    }

    /**
     * Invoked by the container when starting up.
     *
     * @throws  ClusterRuntimeException  If unable to start.
     */
    protected void doStart()
        throws com.sun.cluster.common.ClusterRuntimeException {

        super.doStart();

        try {

            // Create an instance of the customized MIB
            //
            mib = new SUN_CLUSTER_MIB_IMPL();

            // XXX remove registration of MIB in mbean server
            ObjectName mibObjName = ObjectNameFactory.getObjectName(null,
                    SUN_CLUSTER_MIB_IMPL.class, null);
            getMbs().registerMBean(mib, mibObjName);

            // Bind the SNMP adaptor to the MIB in order to make the MIB
            // accessible through the SNMP protocol adaptor.

            JmxAgent.getSnmpAdaptorServer().addMib(mib);
            mib.setSnmpAdaptor(JmxAgent.getSnmpAdaptorServer());


        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception trying to register MIB",
                e);
            throw new ClusterRuntimeException("caught exception trying " +
                "to register MIB : " + e.getMessage());
        }
    }


    /**
     * Invoked by the container when stopping, must release all resources.
     */
    protected void doStop() {

        super.doStop();

        // Stop the mib and release them for GC
        JmxAgent.getSnmpAdaptorServer().removeMib(mib);
        mib = null;
    }
}
