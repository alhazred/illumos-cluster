/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SnmpLoadedClassData.java 1.7     08/05/20 SMI"
 */

package sun.management.snmp.util;

import java.io.Serializable;

import java.lang.ref.WeakReference;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import javax.management.snmp.SnmpOid;
import javax.management.snmp.SnmpStatusException;


/**
 * This class is used to cache LoadedClass table data. WARNING : MUST IMPLEMENT
 * THE SnmpTableHandler directly. 
 */
public final class SnmpLoadedClassData extends SnmpCachedData {

    /**
     * Constructs a new instance of SnmpLoadedClassData. Instances are
     * immutable.
     *
     * @param  lastUpdated  Time stamp as returned by
     * {@link System#currentTimeMillis System.currentTimeMillis()}
     * @param  indexMap  The table indexed table data, sorted in ascending order
     * by {@link #oidComparator}. The keys must be instances of {@link SnmpOid}.
     */
    public SnmpLoadedClassData(long lastUpdated, TreeMap indexMap) {
        super(lastUpdated, indexMap, false);
    }


    // SnmpTableHandler.getData()
    public final Object getData(SnmpOid index) {
        int pos = 0;

        try {
            pos = (int) index.getOidArc(0);
        } catch (SnmpStatusException e) {
            return null;
        }

        if (pos >= datas.length)
            return null;

        return datas[pos];
    }

    // SnmpTableHandler.getNext()
    public final SnmpOid getNext(SnmpOid index) {
        int pos = 0;

        if (index == null) {

            if ((datas != null) && (datas.length >= 1))
                return new SnmpOid(0);
        }

        try {
            pos = (int) index.getOidArc(0);
        } catch (SnmpStatusException e) {
            return null;
        }

        if (pos < (datas.length - 1))
            return new SnmpOid(pos + 1);
        else
            return null;
    }

    // SnmpTableHandler.contains()
    public final boolean contains(SnmpOid index) {
        int pos = 0;

        try {
            pos = (int) index.getOidArc(0);
        } catch (SnmpStatusException e) {
            return false;
        }

        return (pos < datas.length);
    }

}
