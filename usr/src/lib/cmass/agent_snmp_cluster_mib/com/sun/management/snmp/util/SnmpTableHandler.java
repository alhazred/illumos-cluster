/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SnmpTableHandler.java 1.6     08/05/20 SMI"
 */

package sun.management.snmp.util;

import javax.management.snmp.SnmpOid;


/**
 * Defines the interface implemented by an object that holds table data.
 */
public interface SnmpTableHandler {

    /**
     * Returns the data associated with the given index. If the given index is
     * not found, null is returned. Note that returning null does not
     * necessarily means that the index was not found.
     */
    public Object getData(SnmpOid index);

    /**
     * Returns the index that immediately follows the given <var>index</var>.
     * The returned index is strictly greater than the given <var>index</var>,
     * and is contained in the table.<br>
     * If the given <var>index</var> is null, returns the first index in the
     * table.<br>
     * If there are no index after the given <var>index</var>, returns null.
     */
    public SnmpOid getNext(SnmpOid index);

    /**
     * Returns true if the given <var>index</var> is present.
     */
    public boolean contains(SnmpOid index);

}
