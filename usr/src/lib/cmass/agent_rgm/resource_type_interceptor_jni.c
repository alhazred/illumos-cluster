/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)resource_type_interceptor_jni.c	1.31	08/08/17 SMI"

#include "com/sun/cluster/agent/rgm/ResourceTypeInterceptor.h"
#include "com/sun/cluster/agent/rgm/RtGroup.h"

#include <limits.h>
#include <jniutil.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <scha.h>
#include <stdlib.h>
#include <scadmin/scrgadm.h>
#include <rgm/rgm_common.h>
#include <unistd.h>
#if SOL_VERSION >= __s10
#include <stdbool.h>
#include <zone.h>
#include <libzccfg/libzccfg.h>
#include <sys/clconf_int.h>
#endif /* SOL_VERSION >= __s10 */

/*
 * Enable this to get lots of printf debug
 */
#define	DEBUG_RT_INTERCEPTOR 0
#define	debug if (DEBUG_RT_INTERCEPTOR)
#define	BUFFSIZE 160

/*
 * A couple of helper macros for things we do a lot,
 * * they contain references to local variables in the
 * * function below
 */
#define	PUT_ATTR(MAP, NAME, VALUE)		\
	PUT(MAP, NAME, (*env)->NewObject(env,	\
	attribute_class,			\
	attribute_constructor,		\
	(*env)->NewStringUTF(env, NAME),	\
	VALUE))

#define	PUT(MAP, NAME, VALUE)						\
	(*env)->CallObjectMethod(env,					\
				MAP,				\
				map_put,				\
				(*env)->NewStringUTF(env, NAME),	\
				VALUE)
/*
 * Helper function to convert the special RtrString type to a standard string
 */
char *
    convertRtrString(RtrString string)
{
	char *val;

	/*
	 * Sanity check
	 */
	if ((string.len == 0) || (string.start == NULL))
		return (NULL);

	switch (string.value) {
	case RTR_NULL:
		return (NULL);
	case RTR_NONE:
		return (strdup(""));
	case RTR_STRING:
		val = (char *)malloc((size_t)(string.len + 1));
		if (val == NULL)
			return (NULL);
		(void) strncpy(val, string.start, (unsigned int)string.len);
		val[string.len] = '\0';
		return (val);

	case RTR_TRUE:
	case RTR_FALSE:
	case RTR_AT_CREATION:
	case RTR_ANYTIME:
	case RTR_WHEN_DISABLED:
	case RTR_WHEN_UNMANAGED:
	case RTR_WHEN_UNMONITORED:
	case RTR_WHEN_OFFLINE:
	case RTR_RG_PRIMARIES:
	case RTR_RT_INSTALLED_NODES:
	case RTR_INT:
	case RTR_BOOLEAN:
	case RTR_ENUM:
	case RTR_STRINGARRAY:
	case RTR_LOGICAL_HOSTNAME:
	case RTR_SHARED_ADDRESS:
	case RTR_FALSE_UNSET:
	default:
		return (NULL);
	}
}

/*
 * Helper function used to retrieve the methods attribute
 * INPUTS :
 * . env	: the JNI environement
 * . map	: the map used to store the information
 * . field_name	: the name of the field in ResourceTypeMBean used to
 *                identify the method
 * . method_name: the method's name as defined in the resource type
 */
static void
    putMethodName(JNIEnv * env, jobject map, const char *field_name,
    RtrString method_name)
{

	jfieldID fid;
	jobject field_value = NULL;
	jobject method_string = NULL;
	jclass hashmap_class = NULL;
	jclass resource_type_mbean_class = NULL;
	jmethodID put;
	char *rt_string = NULL;

	/*
	 * Get HashMap and resourceTypeMBean classes
	 */
	hashmap_class = (*env)->FindClass(env, "java/util/HashMap");
	if (hashmap_class == NULL)
		goto error;

	resource_type_mbean_class = (*env)->FindClass(env,
	    "com/sun/cluster/agent/rgm/ResourceTypeMBean");
	if (resource_type_mbean_class == NULL)
		goto error;

	/*
	 * Get HashMap's put method
	 */
	put = (*env)->GetMethodID(env, hashmap_class, "put",
	    "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");

	/*
	 * Get field ID
	 */
	fid = (*env)->GetStaticFieldID(env, resource_type_mbean_class,
	    field_name, "Ljava/lang/String;");
	if (fid == NULL)
		goto error;

	/*
	 * Get field value
	 */
	field_value = (*env)->GetStaticObjectField(env,
	    resource_type_mbean_class, fid);
	if (field_value == NULL)
		goto error;

	/*
	 * Convert method name
	 */
	rt_string = convertRtrString(method_name);
	if ((rt_string == NULL) || (strlen(rt_string) == 0)) {
		method_string = NULL;
	} else {
		method_string = newObjStringParam(env, "java/lang/String",
		    rt_string);
	}

	/*
	 * Add element to hashmap
	 */
	(*env)->CallObjectMethod(env, map, put, field_value, method_string);

error:
	/*
	 * Free elements
	 */
	if (rt_string != NULL)
		free(rt_string);
	delRef(env, hashmap_class);
	delRef(env, resource_type_mbean_class);
	delRef(env, field_value);
	delRef(env, method_string);
}

#if DEBUG_RT_INTERCEPTOR
static void
    dumpProperty(RtrParam_T * property)
{
	char *rt_string;
	int i;

	(void) printf("\n");

	rt_string = convertRtrString(property->propname);
	if (rt_string != NULL) {
		(void) printf("propname		 = %s\n", rt_string);
		free(rt_string);
	} else {
		(void) printf("propname		 = NULL\n");
	}
	switch (property->extension) {
	case RTR_TRUE:
		(void) printf("extension = RTR_TRUE\n");
		break;
	case RTR_FALSE:
		(void) printf("extension = RTR_FALSE\n");
		break;
	case RTR_FALSE_UNSET:
		(void) printf("extension = RTR_FALSE_UNSET\n");
		break;
	default:
		(void) printf("extension = UNEXPECTED : %d\n",
		    property->extension);
		break;
	}

	switch (property->per_node) {
	case RTR_TRUE:
		(void) printf("per_node = RTR_TRUE\n");
		break;
	case RTR_FALSE:
		(void) printf("per_node = RTR_FALSE\n");
		break;
	case RTR_FALSE_UNSET:
		(void) printf("per_node = RTR_FALSE_UNSET\n");
		break;
	default:
		(void) printf("per_node = UNEXPECTED : %d\n",
		    property->per_node);
		break;
	}

	switch (property->sysparamid) {
	case RTR_SP_NONE:
		(void) printf("sysparamid = RTR_SP_NONE\n");
		break;
	case RTR_SP_START_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_START_TIMEOUT\n");
		break;
	case RTR_SP_STOP_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_STOP_TIMEOUT\n");
		break;
	case RTR_SP_VALIDATE_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_VALIDATE_TIMEOUT\n");
		break;
	case RTR_SP_UPDATE_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_UPDATE_TIMEOUT\n");
		break;
	case RTR_SP_INIT_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_INIT_TIMEOUT\n");
		break;
	case RTR_SP_FINI_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_FINI_TIMEOUT\n");
		break;
	case RTR_SP_BOOT_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_BOOT_TIMEOUT\n");
		break;
	case RTR_SP_MONITOR_START_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_MONITOR_START_TIMEOUT\n");
		break;
	case RTR_SP_MONITOR_STOP_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_MONITOR_STOP_TIMEOUT\n");
		break;
	case RTR_SP_MONITOR_CHECK_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_MONITOR_CHECK_TIMEOUT\n");
		break;
	case RTR_SP_PRENET_START_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_PRENET_START_TIMEOUT\n");
		break;
	case RTR_SP_POSTNET_STOP_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_POSTNET_STOP_TIMEOUT\n");
		break;
	case RTR_SP_FAILOVER_MODE:
		(void) printf("sysparamid = RTR_SP_FAILOVER_MODE\n");
		break;
	case RTR_SP_NETWORK_RESOURCES_USED:
		(void) printf("sysparamid = RTR_SP_NETWORK_RESOURCES_USED\n");
		break;
	case RTR_SP_SCALABLE:
		(void) printf("sysparamid = RTR_SP_SCALABLE\n");
		break;
	case RTR_SP_PORT_LIST:
		(void) printf("sysparamid = RTR_SP_PORT_LIST\n");
		break;
	case RTR_SP_LOAD_BALANCING_POLICY:
		(void) printf("sysparamid = RTR_SP_LOAD_BALANCING_POLICY\n");
		break;
	case RTR_SP_LOAD_BALANCING_WEIGHTS:
		(void) printf("sysparamid = RTR_SP_LOAD_BALANCING_WEIGHTS\n");
		break;
	case RTR_SP_AFFINITY_TIMEOUT:
		(void) printf("sysparamid = RTR_SP_AFFINITY_TIMEOUT\n");
		break;
	case RTR_SP_UDP_AFFINITY:
		(void) printf("sysparamid = RTR_SP_UDP_AFFINITY\n");
		break;
	case RTR_SP_WEAK_AFFINITY:
		(void) printf("sysparamid = RTR_SP_WEAK_AFFINITY\n");
		break;
	case RTR_SP_CHEAP_PROBE_INTERVAL:
		(void) printf("sysparamid = RTR_SP_CHEAP_PROBE_INTERVAL\n");
		break;
	case RTR_SP_THOROUGH_PROBE_INTERVAL:
		(void) printf("sysparamid = RTR_SP_THOROUGH_PROBE_INTERVAL\n");
		break;
	case RTR_SP_RETRY_COUNT:
		(void) printf("sysparamid = RTR_SP_RETRY_COUNT\n");
		break;
	case RTR_SP_RETRY_INTERVAL:
		(void) printf("sysparamid = RTR_SP_RETRY_INTERVAL\n");
		break;
	case RTR_SP_GLOBAL_ZONE_OVERRIDE:
		(void) printf("sysparamid = RTR_SP_GLOBAL_ZONE_OVERRIDE\n");
		break;
	default:
		(void) printf("sysparamid = UNEXPECTED : %d\n",
		    property->sysparamid);
		break;
	}
	switch (property->type) {
	case RTR_NONE:
		(void) printf("type = RTR_NONE\n");
		break;
	case RTR_INT:
		(void) printf("type = RTR_INT\n");
		break;
	case RTR_BOOLEAN:
		(void) printf("type = RTR_BOOLEAN\n");
		break;
	case RTR_STRING:
		(void) printf("type = RTR_STRING\n");
		break;
	case RTR_STRINGARRAY:
		(void) printf("type = RTR_STRINGARRAY\n");
		break;
	case RTR_ENUM:
		(void) printf("type = RTR_ENUM\n");
		break;
	default:
		(void) printf("type = UNEXPECTED : %d\n",
		    property->type);
		break;
	}

	(void) printf("enumlistsz = %d\n", property->enumlistsz);

	(void) printf("enumlist = ");
	for (i = 0; i < property->enumlistsz; i++) {
		if (i != 0)
			(void) printf(", ");
		rt_string = convertRtrString(property->enumlist[i]);
		if (rt_string == NULL) {
			(void) printf("NULL");
		} else {
			(void) printf("%s", rt_string);
			free(rt_string);
		}
	}
	(void) printf("\n");

	(void) printf("rtr_min_isset = %c\n", property->rtr_min_isset);
	(void) printf("rtr_max_isset = %c\n", property->rtr_max_isset);
	(void) printf("arraymin_isset = %c\n", property->arraymin_isset);
	(void) printf("arraymax_isset = %c\n", property->arraymax_isset);
	(void) printf("defaultint_isset = %c\n", property->defaultint_isset);

	(void) printf("rtr_min = %d\n", property->rtr_min);
	(void) printf("rtr_max = %d\n", property->rtr_max);
	(void) printf("arraymin = %d\n", property->arraymin);
	(void) printf("arraymax = %d\n", property->arraymax);

	rt_string = convertRtrString(property->defaultstr);
	if (rt_string == NULL) {
		(void) printf("defaultstr = NULL\n");
	} else {
		(void) printf("defaultstr = %s\n", rt_string);
		free(rt_string);
	}

	(void) printf("defaultint = %d\n", property->defaultint);

	switch (property->defaultboolean) {
	case RTR_NONE:
		(void) printf("defaultboolean = RTR_NONE\n");
		break;
	case RTR_TRUE:
		(void) printf("defaultboolean = RTR_TRUE\n");
		break;
	case RTR_FALSE:
		(void) printf("defaultboolean = RTR_FALSE\n");
		break;
	default:
		(void) printf("defaultboolean = UNEXPECTED : %d\n",
		    property->defaultboolean);
		break;
	}
	(void) printf("defaultarraysz = %d\n", property->defaultarraysz);

	(void) printf("defaultarray = ");
	for (i = 0; i < property->defaultarraysz; i++) {
		if (i != 0)
			(void) printf(", ");
		rt_string = convertRtrString(property->defaultarray[i]);
		if (rt_string == NULL) {
			(void) printf("NULL");
		} else {
			(void) printf("%s", rt_string);
			free(rt_string);
		}
	}
	(void) printf("\n");

	rt_string = convertRtrString(property->description);
	if (rt_string == NULL) {
		(void) printf("description = NULL\n");
	} else {
		(void) printf("description = %s\n", rt_string);
		free(rt_string);
	}

	switch (property->tunable) {
	case RTR_NONE:
		(void) printf("tunable = RTR_NONE\n");
		break;
	case RTR_AT_CREATION:
		(void) printf("tunable = RTR_AT_CREATION\n");
		break;
	case RTR_ANYTIME:
		(void) printf("tunable = RTR_ANYTIME\n");
		break;
	case RTR_WHEN_DISABLED:
		(void) printf("tunable = RTR_WHEN_DISABLED\n");
		break;
	case RTR_WHEN_UNMANAGED:
		(void) printf("tunable = RTR_WHEN_UNMANAGED\n");
		break;
	case RTR_WHEN_UNMONITORED:
		(void) printf("tunable = RTR_WHEN_UNMONITORED\n");
		break;
	case RTR_WHEN_OFFLINE:
		(void) printf("tunable = RTR_WHEN_OFFLINE\n");
		break;
	default:
		(void) printf("tunable = UNEXPECTED : %d\n",
		    property->tunable);
		break;
	}
	(void) printf("----------------------------------------------------\n");
}

#endif

/*
 * Helper function to add a property object to a list.
 * INPUT :
 * . env      : the JNI environement
 * . list     : the list used to store the properties
 * . property : the resource type's property to store
 */
static void
    addRtProperty(JNIEnv * env, jobject list, jobject property)
{
	jclass linked_list_class = NULL;
	jmethodID add;

	/*
	 * Get LinkedList class
	 */
	linked_list_class = (*env)->FindClass(env, "java/util/LinkedList");
	if (linked_list_class == NULL)
		goto error;

	/*
	 * Get LinkedList's add method
	 */
	add = (*env)->GetMethodID(env, linked_list_class, "add",
	    "(Ljava/lang/Object;)Z");

	/*
	 * Add the property object to the list
	 */
	(*env)->CallObjectMethod(env, list, add, property);

error:
	delRef(env, linked_list_class);
	delRef(env, property);
}

/*
 * Helper function to create the JAVA representation of a resource type
 * property.
 * INPUT :
 * . env      : the JNI environement
 * . property : the resource type's property to create
 */
jobject
    createRtProperty(JNIEnv * env, RtrParam_T * property)
{
	char *rt_string;
	int i;
	int is_required = !0;
	jclass linked_list_class = NULL;
	jmethodID add;
	jfieldID fid;
	jobject field_value = NULL;
	jclass property_class = NULL;
	jclass tunable_class = NULL;
	jobject property_object = NULL;

	/*
	 * common methods
	 */

	jmethodID tunable_class_const;
	jmethodID set_name, set_extension, set_required, set_description,
	    set_tunable, set_pernode;
	jobject name = NULL;
	jobject description = NULL;
	jobject tunable = NULL;

	/*
	 * types methods
	 */
	jmethodID set_min_value, set_max_value, set_default_value;
	jmethodID set_min_length, set_max_length, set_enum_list;
	jmethodID set_array_min_size, set_array_max_size;
	jobject min_value = NULL;
	jobject max_value = NULL;
	jobject default_value = NULL;
	jobject min_length = NULL;
	jobject max_length = NULL;
	jobject enum_list = NULL;
	jobject array_min_size = NULL;
	jobject array_max_size = NULL;

	char **default_array;

#if DEBUG_RT_INTERCEPTOR
	dumpProperty(property);
#endif

	/*
	 * Take appropriate action depending on the type of the property
	 */
	switch (property->type) {
	case RTR_INT:
		/*
		 * Get property class
		 */
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyInteger");
		if (property_class == NULL)
			goto error;

		/*
		 * Create property JAVA object
		 */
		property_object = newObjNullParam(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyInteger");
		if (property_object == NULL)
			goto error;

		/*
		 * Set min value
		 */
		if (property->rtr_min_isset) {
			set_min_value =
			    (*env)->GetMethodID(env, property_class,
			    "setMinValue",
			    "(Ljava/lang/Integer;)V");
			min_value = newObjInt32Param(env, "java/lang/Integer",
			    property->rtr_min);
			if (min_value == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_min_value, min_value);
		}
		/*
		 * Set max value
		 */
		if (property->rtr_max_isset) {
			set_max_value =
			    (*env)->GetMethodID(env, property_class,
			    "setMaxValue",
			    "(Ljava/lang/Integer;)V");
			max_value = newObjInt32Param(env, "java/lang/Integer",
			    property->rtr_max);
			if (max_value == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_max_value, max_value);
		}
		/*
		 * Set default value
		 */
		if (property->defaultint_isset) {
			is_required = 0;
			set_default_value = (*env)->GetMethodID(env,
			    property_class, "setDefaultValue",
			    "(Ljava/lang/Integer;)V");
			default_value = newObjInt32Param(env,
			    "java/lang/Integer",
			    property->defaultint);
			if (default_value == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_default_value, default_value);
		}
		break;

	case RTR_BOOLEAN:
		is_required = 0;
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyBoolean");
		if (property_class == NULL)
			goto error;

		property_object = newObjNullParam(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyBoolean");
		if (property_object == NULL)
			goto error;

		set_default_value = (*env)->GetMethodID(env, property_class,
		    "setDefaultValue", "(Ljava/lang/Boolean;)V");

		if (property->defaultboolean == RTR_TRUE) {
			default_value = newObjStringParam(env,
			    "java/lang/Boolean", "true");
		} else {
			default_value = newObjStringParam(env,
			    "java/lang/Boolean", "false");
		}
		if (default_value == NULL)
			goto error;

		(*env)->CallVoidMethod(env, property_object,
		    set_default_value,
		    default_value);
		break;

	case RTR_STRING:
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyString");
		if (property_class == NULL)
			goto error;

		property_object = newObjNullParam(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyString");
		if (property_object == NULL)
			goto error;

		/*
		 * Set min length value
		 */
		if (property->rtr_min_isset) {
			set_min_length = (*env)->GetMethodID(env,
			    property_class, "setMinLength",
			    "(Ljava/lang/Integer;)V");
			min_length = newObjInt32Param(env,
			    "java/lang/Integer",
			    property->rtr_min);
			if (min_length == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_min_length, min_length);
		}
		/*
		 * Set max length value
		 */
		if (property->rtr_max_isset) {
			set_max_length = (*env)->GetMethodID(env,
			    property_class, "setMaxLength",
			    "(Ljava/lang/Integer;)V");
			max_length = newObjInt32Param(env,
			    "java/lang/Integer",
			    property->rtr_max);
			if (max_length == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_max_length, max_length);
		}
		set_default_value = (*env)->GetMethodID(env, property_class,
		    "setDefaultValue", "(Ljava/lang/String;)V");

		if (property->defaultstr.value != RTR_NONE) {
			is_required = 0;
		}
		rt_string = convertRtrString(property->defaultstr);
		if (rt_string == NULL) {
			default_value = NULL;
		} else {
			default_value = newObjStringParam(env,
			    "java/lang/String", rt_string);
			free(rt_string);
			if (default_value == NULL)
				goto error;
		}

		(*env)->CallVoidMethod(env, property_object, set_default_value,
		    default_value);
		break;

	case RTR_ENUM:
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/ResourcePropertyEnum");
		if (property_class == NULL)
			goto error;

		property_object = newObjNullParam(env,
		    "com/sun/cluster/agent/rgm/property/ResourcePropertyEnum");
		if (property_object == NULL)
			goto error;

		set_enum_list = (*env)->GetMethodID(env, property_class,
		    "setEnumList", "(Ljava/util/List;)V");
		set_default_value = (*env)->GetMethodID(env, property_class,
		    "setDefaultValue", "(Ljava/lang/String;)V");

		enum_list = newObjNullParam(env, "java/util/LinkedList");
		if (enum_list == NULL)
			goto error;

		linked_list_class = (*env)->FindClass(env,
		    "java/util/LinkedList");
		if (linked_list_class == NULL)
			goto error;

		add = (*env)->GetMethodID(env, linked_list_class, "add",
		    "(Ljava/lang/Object;)Z");
		/*
		 * Specific case for failover mode.
		 * in lots of resource type descriptions,
		 * the range value of the failover mode property is
		 * not set. We check here if the range is set and if
		 * not, we set it to the default : {NONE,SOFT,HARD}
		 */
		rt_string = convertRtrString(property->propname);
		if ((rt_string != NULL) &&
		    (strcasecmp(rt_string, "Failover_mode") == 0) &&
		    (property->enumlistsz == 0)) {
			/* NONE */
			default_value = newObjStringParam(env,
				    "java/lang/String", "NONE");
			if (default_value == NULL)
				goto error;
			(*env)->CallObjectMethod(env, enum_list, add,
				    default_value);
			delRef(env, default_value);
			/* SOFT */
			default_value = newObjStringParam(env,
				    "java/lang/String", "SOFT");
			if (default_value == NULL)
				goto error;
			(*env)->CallObjectMethod(env, enum_list, add,
				    default_value);
			delRef(env, default_value);
			/* HARD */
			default_value = newObjStringParam(env,
				    "java/lang/String", "HARD");
			if (default_value == NULL)
				goto error;
			(*env)->CallObjectMethod(env, enum_list, add,
				    default_value);
			delRef(env, default_value);
			free(rt_string);
		} else {
			if (rt_string != NULL) {
				free(rt_string);
			}
			for (i = 0; i < property->enumlistsz; i++) {
				rt_string =
					convertRtrString(property->enumlist[i]);
				default_value = newObjStringParam(env,
				    "java/lang/String", rt_string);
				if (rt_string != NULL) {
					free(rt_string);
				}
				if (default_value == NULL)
					goto error;
				(*env)->CallObjectMethod(env, enum_list, add,
				    default_value);
				delRef(env, default_value);
			}
		}

		rt_string = convertRtrString(property->defaultstr);
		if (rt_string == NULL) {
			default_value = NULL;
		} else {
			is_required = 0;
			default_value = newObjStringParam(env,
			    "java/lang/String", rt_string);

			free(rt_string);

			if (default_value == NULL)
				goto error;
		}

		(*env)->CallVoidMethod(env, property_object, set_enum_list,
		    enum_list);
		(*env)->CallVoidMethod(env, property_object, set_default_value,
		    default_value);
		break;

	case RTR_STRINGARRAY:
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyStringArray");
		if (property_class == NULL)
			goto error;

		property_object = newObjNullParam(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyStringArray");
		if (property_object == NULL)
			goto error;

		/*
		 * Set min length value
		 */
		if (property->rtr_min_isset) {
			set_min_length = (*env)->GetMethodID(env,
			    property_class, "setMinLength",
			    "(Ljava/lang/Integer;)V");
			min_length = newObjInt32Param(env, "java/lang/Integer",
			    property->rtr_min);
			if (min_length == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_min_length, min_length);
		}
		/*
		 * Set max length value
		 */
		if (property->rtr_max_isset) {
			set_max_length = (*env)->GetMethodID(env,
			    property_class, "setMaxLength",
			    "(Ljava/lang/Integer;)V");
			max_length = newObjInt32Param(env, "java/lang/Integer",
			    property->rtr_max);
			if (max_length == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_max_length, max_length);
		}
		/*
		 * Set min array size value
		 */
		if (property->arraymin_isset) {
			set_array_min_size = (*env)->GetMethodID(env,
			    property_class, "setArrayMinSize",
			    "(Ljava/lang/Integer;)V");
			array_min_size = newObjInt32Param(env,
			    "java/lang/Integer",
			    property->arraymin);
			if (array_min_size == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_array_min_size, array_min_size);
		}
		/*
		 * Set max array size value
		 */
		if (property->arraymax_isset) {
			set_array_max_size = (*env)->GetMethodID(env,
			    property_class, "setArrayMaxSize",
			    "(Ljava/lang/Integer;)V");
			array_max_size = newObjInt32Param(env,
			    "java/lang/Integer",
			    property->arraymax);
			if (array_max_size == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_array_max_size, array_max_size);
		}
		/*
		 * Set default value
		 */
		if (property->defaultarray != NULL) {
			is_required = 0;
			set_default_value = (*env)->GetMethodID(env,
			    property_class,
			    "setDefaultValue",
			    "([Ljava/lang/String;)V");

			default_array = (char **)malloc(sizeof (char *) *
			    (unsigned int)property->defaultarraysz);
			if (default_array == NULL) {
				goto error;
			}
			for (i = 0; i < property->defaultarraysz; i++) {
				default_array[i] = convertRtrString(
				    property->defaultarray[i]);
			}

			default_value = newStringArray(env,
			    property->defaultarraysz,
			    default_array);

			for (i = 0; i < property->defaultarraysz; i++) {
				if (default_array[i] != NULL) {
					free(default_array[i]);
				}
			}
			free(default_array);

			if (default_value == NULL)
				goto error;

			(*env)->CallVoidMethod(env, property_object,
			    set_default_value,
			    default_value);
		}
		break;

	case RTR_NONE:
	case RTR_NULL:
	case RTR_TRUE:
	case RTR_FALSE:
	case RTR_AT_CREATION:
	case RTR_ANYTIME:
	case RTR_WHEN_DISABLED:
	case RTR_WHEN_UNMANAGED:
	case RTR_WHEN_UNMONITORED:
	case RTR_WHEN_OFFLINE:
	case RTR_RG_PRIMARIES:
	case RTR_RT_INSTALLED_NODES:
	case RTR_LOGICAL_HOSTNAME:
	case RTR_SHARED_ADDRESS:
	case RTR_FALSE_UNSET:
	default:
		goto error;
	}

	/*
	 * Now complete the property type with the common elements
	 */
	/*
	 * Define the common methods
	 */
	set_name = (*env)->GetMethodID(env, property_class, "setName",
	    "(Ljava/lang/String;)V");
	set_extension = (*env)->GetMethodID(env, property_class,
	    "setExtension",
	    "(Z)V");
	set_required = (*env)->GetMethodID(env, property_class,
	    "setRequired",
	    "(Z)V");
	set_description = (*env)->GetMethodID(env, property_class,
	    "setDescription", "(Ljava/lang/String;)V");
	set_tunable = (*env)->GetMethodID(env, property_class, "setTunable",
	    "(Lcom/sun/cluster/agent/rgm/property/ResourcePropertyEnum;)V");
	set_pernode = (*env)->GetMethodID(env, property_class,
	    "setPernode",
	    "(Z)V");

	/*
	 * Retrieve the common values
	 */
	/*
	 * Name
	 */
	rt_string = convertRtrString(property->propname);
	name = newObjStringParam(env, "java/lang/String", rt_string);
	if (rt_string != NULL) {
		free(rt_string);
	}
	if (name == NULL)
		goto error;

	/*
	 * Description
	 */
	rt_string = convertRtrString(property->description);
	if (rt_string == NULL) {
		description = NULL;
	} else {
		description = newObjStringParam(env, "java/lang/String",
		    rt_string);
		free(rt_string);
		if (description == NULL)
			goto error;
	}

	/*
	 * Tunable
	 */
	tunable_class = (*env)->FindClass(env,
	    "com/sun/cluster/agent/rgm/property/Tunable");
	if (tunable_class == NULL)
		goto error;
	switch (property->tunable) {
	case RTR_TRUE:
		fid = (*env)->GetStaticFieldID(env, tunable_class, "TRUE",
		    "Ljava/lang/String;");
		break;
	case RTR_ANYTIME:
		fid = (*env)->GetStaticFieldID(env, tunable_class, "ANYTIME",
		    "Ljava/lang/String;");
		break;
	case RTR_FALSE:
		fid = (*env)->GetStaticFieldID(env, tunable_class, "FALSE",
		    "Ljava/lang/String;");
		break;
	case RTR_AT_CREATION:
		fid = (*env)->GetStaticFieldID(env, tunable_class,
		    "AT_CREATION", "Ljava/lang/String;");
		break;
	case RTR_WHEN_DISABLED:
		fid = (*env)->GetStaticFieldID(env, tunable_class,
		    "WHEN_DISABLED", "Ljava/lang/String;");
		break;
	case RTR_NONE:
	case RTR_NULL:
	case RTR_WHEN_UNMANAGED:
	case RTR_WHEN_UNMONITORED:
	case RTR_WHEN_OFFLINE:
	case RTR_RG_PRIMARIES:
	case RTR_RT_INSTALLED_NODES:
	case RTR_INT:
	case RTR_BOOLEAN:
	case RTR_STRING:
	case RTR_ENUM:
	case RTR_STRINGARRAY:
	case RTR_LOGICAL_HOSTNAME:
	case RTR_SHARED_ADDRESS:
	case RTR_FALSE_UNSET:
	default:
		fid = (*env)->GetStaticFieldID(env, tunable_class, "NONE",
		    "Ljava/lang/String;");
		break;
	}

	field_value = (*env)->GetStaticObjectField(env, tunable_class, fid);
	if (field_value == NULL)
		goto error;

	tunable_class_const = (*env)->GetMethodID(env, tunable_class, "<init>",
	    "(Ljava/lang/String;)V");
	tunable = (*env)->NewObject(env, tunable_class, tunable_class_const,
	    field_value);
	if (tunable == NULL)
		goto error;

	/*
	 * Call the appropriate methods
	 */
	(*env)->CallVoidMethod(env, property_object, set_name, name);
	(*env)->CallVoidMethod(env, property_object, set_extension,
	    (property->extension == RTR_TRUE) ? JNI_TRUE : JNI_FALSE);
	(*env)->CallVoidMethod(env, property_object, set_pernode,
	    (property->per_node == RTR_TRUE) ? JNI_TRUE : JNI_FALSE);
	(*env)->CallVoidMethod(env, property_object, set_required,
	    is_required ? JNI_TRUE : JNI_FALSE);
	(*env)->CallVoidMethod(env, property_object, set_description,
	    description);
	(*env)->CallVoidMethod(env, property_object, set_tunable, tunable);

error:
	/*
	 * Delete local references
	 */
	delRef(env, name);
	delRef(env, description);
	delRef(env, tunable);
	delRef(env, min_value);
	delRef(env, max_value);
	delRef(env, default_value);
	delRef(env, enum_list);
	delRef(env, min_length);
	delRef(env, max_length);
	delRef(env, array_min_size);
	delRef(env, array_max_size);
	delRef(env, property_class);
	delRef(env, linked_list_class);

	return (property_object);
}

/*
 * Helper function to create the JAVA representation of a resource type
 * property.
 * INPUT :
 * . env      : the JNI environement
 * . property : the resource type's property to create
 */
jobject
    createRtPropertyFromRGM(JNIEnv *env, rgm_param_t *property)
{
	char *rt_string;
	int i;
	jclass linked_list_class = NULL;
	jmethodID add;
	jfieldID fid;
	jobject field_value = NULL;
	jclass property_class = NULL;
	jclass tunable_class = NULL;
	jobject property_object = NULL;

	/*
	 * common methods
	 */

	jmethodID tunable_class_const;
	jmethodID set_name, set_extension, set_required, set_description,
	    set_tunable, set_pernode;
	jobject name = NULL;
	jobject description = NULL;
	jobject tunable = NULL;

	/*
	 * types methods
	 */
	jmethodID set_min_value, set_max_value, set_default_value;
	jmethodID set_min_length, set_max_length, set_enum_list;
	jmethodID set_array_min_size, set_array_max_size;
	jobject min_value = NULL;
	jobject max_value = NULL;
	jobject default_value = NULL;
	jobject min_length = NULL;
	jobject max_length = NULL;
	jobject enum_list = NULL;
	jobject array_min_size = NULL;
	jobject array_max_size = NULL;

	char **default_array;

	int is_required = !0;
	if (property->p_default_isset) {
		is_required = 0;
	}

	/*
	 * Take appropriate action depending on the type of the property
	 */
	switch (property->p_type) {
	case SCHA_PTYPE_INT:
		/*
		 * Get property class
		 */
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyInteger");
		if (property_class == NULL)
			goto error;

		/*
		 * Create property JAVA object
		 */
		property_object = newObjNullParam(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyInteger");
		if (property_object == NULL)
			goto error;

		/*
		 * Set min value
		 */
		if (property->p_min_isset == B_TRUE) {
			set_min_value =
			    (*env)->GetMethodID(env, property_class,
			    "setMinValue",
			    "(Ljava/lang/Integer;)V");
			min_value = newObjInt32Param(env, "java/lang/Integer",
			    property->p_min);
			if (min_value == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_min_value, min_value);
		}
		/*
		 * Set max value
		 */
		if (property->p_max_isset == B_TRUE) {
			set_max_value =
			    (*env)->GetMethodID(env, property_class,
			    "setMaxValue",
			    "(Ljava/lang/Integer;)V");
			max_value = newObjInt32Param(env, "java/lang/Integer",
			    property->p_max);
			if (max_value == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_max_value, max_value);
		}
		/*
		 * Set default value
		 */
		if (property->p_default_isset == B_TRUE) {
			set_default_value = (*env)->GetMethodID(env,
			    property_class, "setDefaultValue",
			    "(Ljava/lang/Integer;)V");
			default_value = newObjInt32Param(env,
			    "java/lang/Integer",
			    property->p_defaultint);
			if (default_value == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_default_value, default_value);
		}
		break;

	case SCHA_PTYPE_BOOLEAN:
		is_required = 0;
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyBoolean");
		if (property_class == NULL)
			goto error;

		property_object = newObjNullParam(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyBoolean");
		if (property_object == NULL)
			goto error;

		set_default_value = (*env)->GetMethodID(env, property_class,
		    "setDefaultValue", "(Ljava/lang/Boolean;)V");

		if (property->p_defaultbool == B_TRUE) {
			default_value = newObjStringParam(env,
			    "java/lang/Boolean", "true");
		} else {
			default_value = newObjStringParam(env,
			    "java/lang/Boolean", "false");
		}
		if (default_value == NULL)
			goto error;

		(*env)->CallVoidMethod(env, property_object,
		    set_default_value,
		    default_value);
		break;

	case SCHA_PTYPE_STRING:
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyString");
		if (property_class == NULL)
			goto error;

		property_object = newObjNullParam(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyString");
		if (property_object == NULL)
			goto error;

		/*
		 * Set min length value
		 */
		if (property->p_min_isset == B_TRUE) {
			set_min_length = (*env)->GetMethodID(env,
			    property_class, "setMinLength",
			    "(Ljava/lang/Integer;)V");
			min_length = newObjInt32Param(env,
			    "java/lang/Integer",
			    property->p_min);
			if (min_length == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_min_length, min_length);
		}
		/*
		 * Set max length value
		 */
		if (property->p_max_isset == B_TRUE) {
			set_max_length = (*env)->GetMethodID(env,
			    property_class, "setMaxLength",
			    "(Ljava/lang/Integer;)V");
			max_length = newObjInt32Param(env,
			    "java/lang/Integer",
			    property->p_max);
			if (max_length == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_max_length, max_length);
		}
		set_default_value = (*env)->GetMethodID(env, property_class,
		    "setDefaultValue", "(Ljava/lang/String;)V");

		rt_string = property->p_defaultstr;
		if (rt_string == NULL) {
			default_value = NULL;
		} else {
			default_value = newObjStringParam(env,
			    "java/lang/String", rt_string);
			if (default_value == NULL)
				goto error;
		}

		(*env)->CallVoidMethod(env, property_object, set_default_value,
		    default_value);
		break;

	case SCHA_PTYPE_ENUM:
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/ResourcePropertyEnum");
		if (property_class == NULL)
			goto error;

		property_object = newObjNullParam(env,
		    "com/sun/cluster/agent/rgm/property/ResourcePropertyEnum");
		if (property_object == NULL)
			goto error;

		set_enum_list = (*env)->GetMethodID(env, property_class,
		    "setEnumList", "(Ljava/util/List;)V");
		set_default_value = (*env)->GetMethodID(env, property_class,
		    "setDefaultValue", "(Ljava/lang/String;)V");

		enum_list = newObjNullParam(env, "java/util/LinkedList");
		if (enum_list == NULL)
			goto error;

		linked_list_class = (*env)->FindClass(env,
		    "java/util/LinkedList");
		if (linked_list_class == NULL)
			goto error;

		add = (*env)->GetMethodID(env, linked_list_class, "add",
		    "(Ljava/lang/Object;)Z");
		/*
		 * Specific case for failover mode.
		 * in lots of resource type descriptions,
		 * the range value of the failover mode property is
		 * not set. We check here if the range is set and if
		 * not, we set it to the default : {NONE,SOFT,HARD}
		 */
		rt_string = property->p_name;
		if ((rt_string != NULL) &&
		    (strcasecmp(rt_string, "Failover_mode") == 0) &&
		    (property->p_enumlist == NULL)) {
			/* NONE */
			default_value = newObjStringParam(env,
				    "java/lang/String", "NONE");
			if (default_value == NULL)
				goto error;
			(*env)->CallObjectMethod(env, enum_list, add,
				    default_value);
			delRef(env, default_value);
			/* SOFT */
			default_value = newObjStringParam(env,
				    "java/lang/String", "SOFT");
			if (default_value == NULL)
				goto error;
			(*env)->CallObjectMethod(env, enum_list, add,
				    default_value);
			delRef(env, default_value);
			/* HARD */
			default_value = newObjStringParam(env,
				    "java/lang/String", "HARD");
			if (default_value == NULL)
				goto error;
			(*env)->CallObjectMethod(env, enum_list, add,
				    default_value);
			delRef(env, default_value);
		} else {
			namelist_t *p_enumlist = property->p_enumlist;
			name_t enumval;
			while (p_enumlist != NULL) {
				enumval = p_enumlist->nl_name;
				rt_string = (char*)enumval;
				default_value = newObjStringParam(env,
				    "java/lang/String", rt_string);
				if (default_value == NULL)
					goto error;
				(*env)->CallObjectMethod(env, enum_list, add,
				    default_value);
				delRef(env, default_value);
				p_enumlist = p_enumlist->nl_next;
			}
		}

		rt_string = property->p_defaultstr;
		if (rt_string == NULL) {
			default_value = NULL;
		} else {
			default_value = newObjStringParam(env,
			    "java/lang/String", rt_string);
			if (default_value == NULL)
				goto error;
		}

		(*env)->CallVoidMethod(env, property_object, set_enum_list,
		    enum_list);
		(*env)->CallVoidMethod(env, property_object, set_default_value,
		    default_value);
		break;

	case SCHA_PTYPE_STRINGARRAY:
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyStringArray");
		if (property_class == NULL)
			goto error;

		property_object = newObjNullParam(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyStringArray");
		if (property_object == NULL)
			goto error;

		/*
		 * Set min length value
		 */
		if (property->p_min_isset == B_TRUE) {
			set_min_length = (*env)->GetMethodID(env,
			    property_class, "setMinLength",
			    "(Ljava/lang/Integer;)V");
			min_length = newObjInt32Param(env, "java/lang/Integer",
			    property->p_min);
			if (min_length == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_min_length, min_length);
		}
		/*
		 * Set max length value
		 */
		if (property->p_max_isset == B_TRUE) {
			set_max_length = (*env)->GetMethodID(env,
			    property_class, "setMaxLength",
			    "(Ljava/lang/Integer;)V");
			max_length = newObjInt32Param(env, "java/lang/Integer",
			    property->p_max);
			if (max_length == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_max_length, max_length);
		}
		/*
		 * Set min array size value
		 */
		if (property->p_arraymin_isset == B_TRUE) {
			set_array_min_size = (*env)->GetMethodID(env,
			    property_class, "setArrayMinSize",
			    "(Ljava/lang/Integer;)V");
			array_min_size = newObjInt32Param(env,
			    "java/lang/Integer",
			    property->p_arraymin);
			if (array_min_size == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_array_min_size, array_min_size);
		}
		/*
		 * Set max array size value
		 */
		if (property->p_arraymax_isset == B_TRUE) {
			set_array_max_size = (*env)->GetMethodID(env,
			    property_class, "setArrayMaxSize",
			    "(Ljava/lang/Integer;)V");
			array_max_size = newObjInt32Param(env,
			    "java/lang/Integer",
			    property->p_arraymax);
			if (array_max_size == NULL)
				goto error;
			(*env)->CallVoidMethod(env, property_object,
			    set_array_max_size, array_max_size);
		}

		/*
		 * Set default value
		 */
		if (property->p_defaultarray != NULL) {
			name_t arrayval;
			int arraysize;
			namelist_t *p_array = property->p_defaultarray;
			set_default_value = (*env)->GetMethodID(env,
			    property_class,
			    "setDefaultValue",
			    "([Ljava/lang/String;)V");

			default_array = NULL;
			i = 0;
			while (p_array != NULL) {
				default_array = (char **)realloc(default_array,
						(i + 1) * sizeof (name_t));
				if (default_array == NULL) {
					goto error;
				}
				arrayval = p_array->nl_name;
				default_array[i++] = strdup(arrayval);
				p_array = p_array->nl_next;
			}
			arraysize = i;

			default_value = newStringArray(env,
			    arraysize, default_array);

			for (i = 0; i < arraysize; i++) {
				if (default_array[i] != NULL) {
					free(default_array[i]);
				}
			}
			if (default_array != NULL) {
				free(default_array);
			}

			if (default_value == NULL)
				goto error;

			(*env)->CallVoidMethod(env, property_object,
			    set_default_value,
			    default_value);
		}
		break;

	case SCHA_PTYPE_UINTARRAY:
	case SCHA_PTYPE_UINT:
	default:
		goto error;
	}

	/*
	 * Now complete the property type with the common elements
	 */
	/*
	 * Define the common methods
	 */
	set_name = (*env)->GetMethodID(env, property_class, "setName",
	    "(Ljava/lang/String;)V");
	set_extension = (*env)->GetMethodID(env, property_class,
	    "setExtension",
	    "(Z)V");
	set_required = (*env)->GetMethodID(env, property_class,
	    "setRequired",
	    "(Z)V");
	set_description = (*env)->GetMethodID(env, property_class,
	    "setDescription", "(Ljava/lang/String;)V");
	set_tunable = (*env)->GetMethodID(env, property_class, "setTunable",
	    "(Lcom/sun/cluster/agent/rgm/property/ResourcePropertyEnum;)V");
	set_pernode = (*env)->GetMethodID(env, property_class,
	    "setPernode",
	    "(Z)V");

	/*
	 * Retrieve the common values
	 */
	/*
	 * Name
	 */
	rt_string = property->p_name;
	name = newObjStringParam(env, "java/lang/String", rt_string);
	if (name == NULL)
		goto error;

	/*
	 * Description
	 */
	rt_string = property->p_description;
	if (rt_string == NULL) {
		description = NULL;
	} else {
		description = newObjStringParam(env, "java/lang/String",
		    rt_string);
		if (description == NULL)
			goto error;
	}

	/*
	 * Tunable
	 */
	tunable_class = (*env)->FindClass(env,
	    "com/sun/cluster/agent/rgm/property/Tunable");
	if (tunable_class == NULL)
		goto error;
	switch (property->p_tunable) {
	case TUNE_ANYTIME:
		fid = (*env)->GetStaticFieldID(env, tunable_class, "ANYTIME",
		    "Ljava/lang/String;");
		break;
	case TUNE_AT_CREATION:
		fid = (*env)->GetStaticFieldID(env, tunable_class,
		    "AT_CREATION", "Ljava/lang/String;");
		break;
	case TUNE_WHEN_DISABLED:
		fid = (*env)->GetStaticFieldID(env, tunable_class,
		    "WHEN_DISABLED", "Ljava/lang/String;");
		break;
	case TUNE_NONE:
	case TUNE_WHEN_OFFLINE:
	case TUNE_WHEN_UNMANAGED:
	case TUNE_WHEN_UNMONITORED:
	default:
		fid = (*env)->GetStaticFieldID(env, tunable_class, "NONE",
		    "Ljava/lang/String;");
		break;
	}

	field_value = (*env)->GetStaticObjectField(env, tunable_class, fid);
	if (field_value == NULL)
		goto error;

	tunable_class_const = (*env)->GetMethodID(env, tunable_class, "<init>",
	    "(Ljava/lang/String;)V");
	tunable = (*env)->NewObject(env, tunable_class, tunable_class_const,
	    field_value);
	if (tunable == NULL)
		goto error;

	/*
	 * Call the appropriate methods
	 */
	(*env)->CallVoidMethod(env, property_object, set_name, name);
	(*env)->CallVoidMethod(env, property_object, set_extension,
	    (property->p_extension == B_TRUE) ? JNI_TRUE : JNI_FALSE);
	(*env)->CallVoidMethod(env, property_object, set_pernode,
	    (property->p_per_node == B_TRUE) ? JNI_TRUE : JNI_FALSE);
	(*env)->CallVoidMethod(env, property_object, set_required,
	    is_required ? JNI_TRUE : JNI_FALSE);
	(*env)->CallVoidMethod(env, property_object, set_description,
	    description);
	(*env)->CallVoidMethod(env, property_object, set_tunable, tunable);

error:
	/*
	 * Delete local references
	 */
	delRef(env, name);
	delRef(env, description);
	delRef(env, tunable);
	delRef(env, min_value);
	delRef(env, max_value);
	delRef(env, default_value);
	delRef(env, enum_list);
	delRef(env, min_length);
	delRef(env, max_length);
	delRef(env, array_min_size);
	delRef(env, array_max_size);
	delRef(env, property_class);
	delRef(env, linked_list_class);

	return (property_object);
}

/*
 * Class:     com_sun_cluster_agent_rgm_ResourceTypeInterceptor
 * Method:    getDirectoryInstances
 * Signature: ()Ljava/util/HashSet;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_rgm_ResourceTypeInterceptor_getDirectoryInstances
	(JNIEnv *env, jobject this, jstring dirName) {

	jobject instances = NULL;
	jclass set_class = NULL;
	jmethodID set_add;

	DIR *dirp = NULL;
	struct dirent *dirent = NULL;
	struct dirent *dp = NULL;

	scha_errmsg_t scha_status;
	RtrFileResult *rt = NULL;
	char *rt_version = NULL;
	char *rt_name = NULL;
	const char *dir_name = NULL;
	char fullpath[PATH_MAX];
	char *dirpath = NULL;

	size_t s = 0;

	/*
	 * get our references to set_class and the add method
	 */
	set_class = (*env)->FindClass(env, "java/util/HashSet");
	if (set_class == NULL)
		goto error;

	set_add = (*env)->GetMethodID(env,
	    set_class,
	    "add",
	    "(Ljava/lang/Object;)"
	    "Z");
	if (set_add == NULL)
		goto error;

	/*
	 * Create the set that will contain the resource type's name
	 */
	instances = newObjNullParam(env, "java/util/HashSet");
	if (instances == NULL)
		goto error;

	/*
	 * Get a C version of our instance name
	 */
	dir_name =
	    (*env)->GetStringUTFChars(env, dirName, NULL);
	if (dir_name == NULL)
		goto error;
	debug printf("\n\tQuerying for Directory %s", dir_name);
	/*
	 * Browse the resource types in the install directory
	 */
	dirp = opendir(dir_name);
	dirent = (struct dirent *)malloc((size_t)(
		(long)sizeof (struct dirent) +
		pathconf(dir_name, _PC_NAME_MAX)));
	if (dirent == NULL) {
		goto error;
	}
	while (dirp != NULL) {
		dp = readdir_r(dirp, dirent);
		if (dp == NULL) {
			break;
		}
		if ((strcmp(dp->d_name, ".") == 0) ||
		    (strcmp(dp->d_name, "..") == 0))
			continue;

		/* set up for full path name construction */
		/* make sure we're not too long */
		if (PATH_MAX - strlen(dir_name) - 1 - strlen(
		    (char *)dp->d_name) > 0) {
			fullpath[0] = '\0';
			(void) strcpy(fullpath, dir_name);
			(void) strcat(fullpath, "/");
				/* note: above is unix-style specific */
			(void) strcat(fullpath, (char *)dp->d_name);
			dirpath = &fullpath[0];
		}

		/*
		 * Get the resource type information
		 */
		scha_status = scrgadm_process_rtreg(
		    (char *)dp->d_name,
		    dirpath,
		    &rt, NULL);

		if (scha_status.err_code != SCHA_ERR_NOERR) {
			rt = NULL;
			continue;
		}

		/*
		 * Construct name depending on version number
		 */
		rt_version = convertRtrString(rt->rt_registration->version);

		if ((rt_version != NULL) && (strcmp(rt_version, "1.0") != 0) &&
		    (strcmp(rt_version, "1") != 0) &&
		    (rt->sc30_rtr == B_FALSE)) {
			s = (size_t)(strlen(dp->d_name) +
			    strlen(rt_version) + 2);
			rt_name = (char *)malloc(s);
			if (rt_name != NULL) {
				int retval;
				retval = sprintf(rt_name, "%s:%s", dp->d_name,
				    rt_version);
				if (retval < 0) {
					goto error;
				}
			}
			free(rt_version);
			rt_version = NULL;
		} else {
			if (rt_version != NULL) {
				free(rt_version);
				rt_version = NULL;
			}
			rt_name = strdup(dp->d_name);
			if (rt_name == NULL) {
				goto error;
			}
		}
		debug printf("\n\t\t Writing rt_name %s to set .. ", rt_name);
		(*env)->CallObjectMethod(env,
			instances,
			set_add,
			(*env)->NewStringUTF(env, rt_name));
		free(rt_name);
		rt_name = NULL;
		scrgadm_free_rtrresult(rt);
		rt = NULL;
	}
	free(dirent);
	dirent = NULL;
	(void) closedir(dirp);
	dirp = NULL;

    error:

	delRef(env, set_class);

	if (dirp != NULL)
		(void) closedir(dirp);

	if (dirent != NULL)
		free(dirent);

	if (rt != NULL)
		scrgadm_free_rtrresult(rt);

	if (dir_name != NULL)
		(*env)->ReleaseStringUTFChars(env, dirName,
		    dir_name);
	return (instances);
} /*lint !e715 */


/*
 * Class:     com_sun_cluster_agent_rgm_RtGroup
 * Method:    getRegisteredInstances
 * Signature: (Ljava/lang/String;)Ljava/util/HashSet;
 */
JNIEXPORT jobject getRegisteredInstances
(JNIEnv *env, jobject this, jstring zcNameQ) {


	jobject instances = NULL;
	jclass set_class = NULL;
	jmethodID set_add;

	scha_err_t scha_error;
	scha_cluster_t sc_handle = NULL;
	scha_str_array_t *rt_names = NULL;
	char *cZCNameQ = NULL;
	const char *tmpS;
	int i;

	/*
	 * get our references to set_class and the add method
	 */
	set_class = (*env)->FindClass(env, "java/util/HashSet");
	if (set_class == NULL)
		goto error;

	set_add = (*env)->GetMethodID(env,
	    set_class,
	    "add",
	    "(Ljava/lang/Object;)"
	    "Z");
	if (set_add == NULL)
		goto error;

	if (zcNameQ != NULL) {
		tmpS = (*env)->GetStringUTFChars(env, zcNameQ, NULL);
		cZCNameQ = strdup(tmpS);
	}

	/*
	 * Create the set that will contain the resource type's name
	 */
	instances = newObjNullParam(env, "java/util/HashSet");
	if (instances == NULL)
		goto error;

	/*
	 * Browse the resource types in the cluster
	 */
	scha_error = scha_cluster_open_zone(cZCNameQ, &sc_handle);
	if (scha_error != SCHA_ERR_NOERR) {
		sc_handle = NULL;
		goto error;
	}


	scha_error = scha_cluster_get_zone(
			cZCNameQ, sc_handle, SCHA_ALL_RESOURCETYPES, &rt_names);

	if ((scha_error == SCHA_ERR_NOERR) && (rt_names != NULL)) {
		for (i = 0; i < (int)rt_names->array_cnt; i++) {
			(*env)->CallObjectMethod(env,
				instances,
				set_add,
				(*env)->NewStringUTF(env,
					rt_names->str_array[i]));
		}
	}
	if (sc_handle != NULL) {
		(void) scha_cluster_close_zone(cZCNameQ, sc_handle);
		sc_handle = NULL;
	}

    error:

	delRef(env, set_class);

	if (sc_handle != NULL)
		(void) scha_cluster_close_zone(cZCNameQ, sc_handle);

	return (instances);
} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_rgm_RtGroup
 * Method:    getAttributeNative
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)
 *	       Ljavax/management/Attribute;
 */
JNIEXPORT jobject
	JNICALL Java_com_sun_cluster_agent_rgm_RtGroup_getAttributeNative
	(JNIEnv *env, jobject this, jstring instanceName, jstring attributeName,
	jstring zcNameQ) {

	const char *attr_name = NULL;
	const char *instance_name = NULL;
	jclass attribute_class = NULL;
	jobject attribute = NULL;
	jmethodID attribute_constructor = NULL;
	jobject attribute_value = NULL;
	char *cZCNameQ = NULL;
	const char *tmpS;

	scha_err_t scha_error;
	scha_resourcetype_t rt_handle = NULL;
	scha_cluster_t sc_handle = NULL;

	if (zcNameQ != NULL) {
		tmpS = (*env)->GetStringUTFChars(env, zcNameQ, NULL);
		cZCNameQ = strdup(tmpS);
	}

	attr_name =
		(*env)->GetStringUTFChars(env, attributeName, NULL);
	if (attr_name == NULL)
		goto error;
	instance_name =
		(*env)->GetStringUTFChars(env, instanceName, NULL);
	if (instance_name == NULL)
		goto error;

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * Get constructor
	 */
	attribute_constructor = (*env)->GetMethodID(env, attribute_class,
	"<init>", "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;

	scha_error = scha_resourcetype_open_zone(cZCNameQ, instance_name,
							&rt_handle);

	if (strcmp(attr_name, "Registered") == 0) {
		attribute_value = newBoolean(env,
			scha_error == SCHA_ERR_NOERR);
	} else if (strcmp(attr_name, "InstalledNodeNames") == 0) {
		scha_str_array_t *nodes_array = NULL;

		if (scha_error == SCHA_ERR_NOERR) {
			scha_error = scha_resourcetype_get_zone(cZCNameQ,
				rt_handle, SCHA_INSTALLED_NODES, &nodes_array);
			if (scha_error != SCHA_ERR_NOERR)
				goto error;

			if (nodes_array->is_ALL_value) {
				/* All node names must be returned */
				scha_error = scha_cluster_open_zone(cZCNameQ,
						&sc_handle);
				if (scha_error != SCHA_ERR_NOERR) {
					sc_handle = NULL;
					goto error;
				}
				scha_error = scha_cluster_get_zone(
						cZCNameQ,
						sc_handle,
						SCHA_ALL_NODENAMES,
						&nodes_array);
				if (scha_error != SCHA_ERR_NOERR)
					goto error;
				(void) scha_cluster_close_zone(cZCNameQ,
					sc_handle);
				sc_handle = NULL;
			}
			attribute_value = newStringArray(env,
				(int)nodes_array->array_cnt,
				nodes_array->str_array);
		}
	}

	/*
	 * Now create the Attribute object
	 */
	attribute = (*env)->NewObject(env, attribute_class,
	    attribute_constructor, attributeName,
	    attribute_value);
	if (attribute == NULL)
		goto error;

error:
	if (rt_handle != NULL) {
		(void) scha_resourcetype_close_zone(cZCNameQ, rt_handle);
		rt_handle = NULL;
	}
	if (instanceName)
		(*env)->ReleaseStringUTFChars(env, instanceName,
		    instance_name);
	if (attributeName)
		(*env)->ReleaseStringUTFChars(env, attributeName,
			attr_name);

	delRef(env, attribute_class);
	return (attribute);
}

/*
 * Class:     com_sun_cluster_agent_rgm_ResourceTypeInterceptor
 * Method:    fillCacheNative
 * Signature: ([Ljava/lang/String;)
 *             Ljava/util/HashMap;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_rgm_ResourceTypeInterceptor_fillCacheNative
	(JNIEnv * env,
	jobject this,
	jobjectArray dir_names) {

	/*
	 * return a Map of Maps containing all instances
	 * key = instanceName
	 * value = Map
	 *		key = attributeName
	 *		value = corresponding Attribute object
	 */

	jobject cache_map = NULL;
	jobject instance_map = NULL;
	jclass map_class = NULL;
	jmethodID map_put;

	int i;

	const char *attr_name = NULL;

	jsize dir_names_length;
	jobject dir_name_string = NULL;

	jclass attribute_class = NULL;
	jobject attribute = NULL;
	jmethodID attribute_constructor = NULL;

	jobject methods_value = NULL;
	jobject upgrade_tunability_value = NULL;
	jobject downgrade_versions_value = NULL;
	jobject sys_prop_value = NULL;
	jobject ext_prop_value = NULL;

	scha_errmsg_t scha_status;
	RtrFileResult *rt = NULL;
	Rtr_T *rt_info;

	scha_err_t scha_error;
	char *rt_string = NULL;
	const char *dir_name = NULL;

	char **list = NULL;
	char **package_list = NULL;
	int j;
	DIR *dirp = NULL;
	struct dirent *dirent = NULL;
	struct dirent *dp = NULL;
	char *rt_version = NULL;
	char *rt_name = NULL;
	size_t s = 0;
	int x = 0;

	char fullpath[PATH_MAX];
	char *dirpath = NULL;


	/*
	 * Now get our references to 'attribute_class'
	 */
	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * Get constructor
	 */
	attribute_constructor = (*env)->GetMethodID(env, attribute_class,
	    "<init>", "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;

	/*
	* Create the map we are going to return
	 */
	cache_map = newObjNullParam(env, "java/util/HashMap");
	if (cache_map == NULL)
		goto error;

	/*
	* get our references to map_class and the put method
	 */
	map_class = (*env)->FindClass(env, "java/util/HashMap");
	if (map_class == NULL)
		goto error;
	map_put =
		(*env)->GetMethodID(env,
			map_class,
			"put",
			"(Ljava/lang/Object;Ljava/lang/Object;)"
			"Ljava/lang/Object;");
	if (map_put == NULL)
		goto error;

	dir_names_length = (*env)->GetArrayLength(env, dir_names);

	for (x = 0; x < dir_names_length; x++) {

		dir_name_string = (*env)->GetObjectArrayElement(env,
			dir_names, x);
		if (dir_name_string == NULL)
			goto error;

		dir_name =
			(*env)->GetStringUTFChars(env, dir_name_string,
			    NULL);

		if (dir_name == NULL)
			goto error;

		/*
		 * Browse the resource types in the install directory
		 */
		dirp = opendir(dir_name);
		dirent = (struct dirent *)malloc((size_t)(
			(long)sizeof (struct dirent) +
			pathconf(dir_name, _PC_NAME_MAX)));
		if (dirent == NULL) {
			goto error;
		}

		while (dirp != NULL) {
			int enum_id = 0;
			jclass enumClass = NULL;
			jobject arrayElement = NULL;

			dp = readdir_r(dirp, dirent);

			if (dp == NULL) break;
			if ((strcmp(dp->d_name, ".") == 0) ||
				(strcmp(dp->d_name, "..") == 0))
				continue;

			/* set up for full path name construction */
			/* make sure we're not too long */
			if (PATH_MAX - strlen(dir_name) - 1 - strlen(
			    (char *)dp->d_name) > 0) {
				fullpath[0] = '\0';
				(void) strcpy(fullpath, dir_name);
				(void) strcat(fullpath, "/");
					/* note: above is unix-style specific */
				(void) strcat(fullpath, (char *)dp->d_name);
				dirpath = &fullpath[0];
			}

			/*
			 * Get the resource type information
			 */
			scha_status = scrgadm_process_rtreg(
				(char *)dp->d_name, dirpath, &rt, NULL);

			if (scha_status.err_code != SCHA_ERR_NOERR) {
				rt = NULL;
				continue;
			}
			instance_map = newObjNullParam(env,
			    "java/util/HashMap");
			if (instance_map == NULL)
				goto error;
			/*
			 * Extract basic information
			 */
			rt_info = rt->rt_registration;
			rt_version = convertRtrString(rt_info->version);

			if ((rt_version != NULL) &&
			    (strcmp(rt_version, "1.0") != 0) &&
			    (strcmp(rt_version, "1") != 0) &&
			    (rt->sc30_rtr == B_FALSE)) {
				s = (size_t)(strlen(dp->d_name) +
				    strlen(rt_version) + 2);
				rt_name = (char *)malloc(s);
				if (rt_name != NULL) {
					int retval;
					retval = sprintf(rt_name,
						"%s:%s", dp->d_name,
					    rt_version);
					if (retval < 0) {
						goto error;
					}
				}
			} else {
				rt_name = strdup(dp->d_name);
				if (rt_name == NULL) {
					goto error;
				}
			}
			PUT_ATTR(instance_map, "Version",
				(*env)->NewStringUTF(env, rt_version));
			if (rt_version != NULL) {
				free(rt_version);
				rt_string = NULL;
			}

			PUT_ATTR(instance_map, "Name",
			    (*env)->NewStringUTF(env, rt_name));

			rt_string = convertRtrString(rt_info->basedir);
			PUT_ATTR(instance_map, "BaseDirectory",
				(*env)->NewStringUTF(env, rt_string));
			if (rt_string != NULL) {
				free(rt_string);
				rt_string = NULL;
			}

			methods_value = newObjNullParam(env,
			    "java/util/HashMap");
			if (methods_value == NULL)
				goto error;
			putMethodName(env, methods_value,
			    "METHOD_START", rt_info->start);
			putMethodName(env, methods_value,
			    "METHOD_STOP", rt_info->stop);
			putMethodName(env, methods_value,
			    "METHOD_ABORT", rt_info->abort);
			putMethodName(env, methods_value,
			    "METHOD_VALIDATE", rt_info->validate);
			putMethodName(env, methods_value,
			    "METHOD_UPDATE", rt_info->update);
			putMethodName(env, methods_value,
			    "METHOD_INIT", rt_info->init);
			putMethodName(env, methods_value,
			    "METHOD_FINI", rt_info->fini);
			putMethodName(env, methods_value,
			    "METHOD_BOOT", rt_info->boot);
			putMethodName(env, methods_value,
			    "METHOD_MONITOR_START",
			    rt_info->monitor_start);
			putMethodName(env, methods_value,
			    "METHOD_MONITOR_STOP",
			    rt_info->monitor_stop);
			putMethodName(env, methods_value,
			    "METHOD_MONITOR_CHECK",
			    rt_info->monitor_check);
			putMethodName(env, methods_value,
			    "METHOD_PRENET_START",
			    rt_info->prenet_start);
			putMethodName(env, methods_value,
			    "METHOD_POSTNET_STOP",
			    rt_info->postnet_stop);
			putMethodName(env, methods_value,
			    "METHOD_POSTNET_ABORT",
			    rt_info->postnet_abort);
			PUT_ATTR(instance_map, "Methods",
				methods_value);

			PUT_ATTR(instance_map, "SingleInstance",
				newBoolean(env,
				rt_info->single_instance == RTR_TRUE));

			PUT_ATTR(instance_map, "InitNodesLocation",
				getCMASSEnum(env,
				"com/sun/cluster/agent/rgm/"
				"ResourceTypeInitNodesLocationEnum",
				rt_info->init_nodes ==
					RTR_RG_PRIMARIES ? 0 : 1));

			/*
			*	"Registered" and "InstalledNodeNames"
			 *	cannot be cached, because
			 *	there are no cluster events generated
			 *	when these properties
			 *	change. These two values will be calculated
			 *	whenever a client
			 *	asks for it
			*/

			PUT_ATTR(instance_map, "Failover",
				newBoolean(env, rt_info->failover == RTR_TRUE));

			enum_id = 0;
			switch (rt_info->sysdefined_type) {
			case RTR_LOGICAL_HOSTNAME:
				enum_id = 1;
				break;
			case RTR_SHARED_ADDRESS:
				enum_id = 2;
				break;
			case RTR_NONE:
			case RTR_NULL:
			case RTR_TRUE:
			case RTR_FALSE:
			case RTR_AT_CREATION:
			case RTR_ANYTIME:
			case RTR_WHEN_DISABLED:
			case RTR_WHEN_UNMANAGED:
			case RTR_WHEN_UNMONITORED:
			case RTR_WHEN_OFFLINE:
			case RTR_RG_PRIMARIES:
			case RTR_RT_INSTALLED_NODES:
			case RTR_INT:
			case RTR_BOOLEAN:
			case RTR_STRING:
			case RTR_ENUM:
			case RTR_STRINGARRAY:
			case RTR_FALSE_UNSET:
			default:
				enum_id = 0;
				break;
			}
			PUT_ATTR(instance_map, "SystemResourceType",
				getCMASSEnum(env,
				"com/sun/cluster/agent/rgm/"
				"ResourceTypeSystemEnum",
				enum_id));

			PUT_ATTR(instance_map, "ApiVersion",
				newObjInt64Param(env,
				"java/lang/Long",
				(int64_t)
				((unsigned int)rt_info->apiversion)));


			package_list = (char **)malloc(sizeof (char *) *
			    (unsigned int)rt_info->pkglistsz);
			if (package_list == NULL) {
				goto error;
			}

			for (j = 0; j < rt_info->pkglistsz; j++) {
				package_list[j] =
				    convertRtrString(rt_info->pkglist[j]);
			}

			PUT_ATTR(instance_map, "Packages",
				newStringArray(env,
				rt_info->pkglistsz,
				package_list));

			for (j = 0; j < rt_info->pkglistsz; j++) {
				if (package_list[j] != NULL) {
					free(package_list[j]);
				}
			}
			free(package_list);

			rt_string = convertRtrString(rt_info->rt_description);
			PUT_ATTR(instance_map, "Description",
				(*env)->NewStringUTF(env, rt_string));
			if (rt_string != NULL) {
				free(rt_string);
				rt_string = NULL;
			}

			PUT_ATTR(instance_map, "Versionable",
				newBoolean(env, (rt->sc30_rtr != B_TRUE)));

			PUT_ATTR(instance_map, "System",
				newBoolean(env,
				rt_info->rt_system == RTR_TRUE));

			list = (char **)malloc(
			    (size_t) sizeof (char *) *
			    (unsigned int)rt->upgradesz);
			if (list == NULL) {
				goto error;
			}

			for (j = 0; j < (int)rt->upgradesz; j++) {
				list[j] = convertRtrString(
				    rt->upgradearray[j]->upg_version);
			}
			PUT_ATTR(instance_map, "UpgradeVersions",
				newStringArray(env, rt->upgradesz, list));

			for (j = 0; j < rt->upgradesz; j++) {
				if (list[j] != NULL) {
					free(list[j]);
				}
			}
			free(list);

			enumClass = NULL;
			arrayElement = NULL;

			/*
			 * Load Enum class
			 */
			enumClass = (*env)->FindClass(env,
			    "com/sun/cluster/agent/rgm/"
			    "ResourceTypeVersionTunabilityEnum");
			if (enumClass == NULL)
				goto error;

			/*
			 * Construct array
			 */
			upgrade_tunability_value =
			    (*env)->NewObjectArray(env,
			    rt->upgradesz, enumClass, NULL);
			delRef(env, enumClass);
			if (upgrade_tunability_value == NULL) {
				goto error;
			}

			/*
			 * Fill array
			 */
			for (j = 0; j < rt->upgradesz; j++) {
				int enum_id = 0;
				switch (rt->upgradearray[j]->upg_tunability) {
				case RTR_NONE:
					enum_id = 0;
					break;
				case RTR_AT_CREATION:
					enum_id = 1;
					break;
				case RTR_ANYTIME:
					enum_id = 2;
					break;
				case RTR_WHEN_OFFLINE:
					enum_id = 3;
					break;
				case RTR_WHEN_UNMANAGED:
					enum_id = 4;
					break;
				case RTR_WHEN_UNMONITORED:
					enum_id = 5;
					break;
				case RTR_WHEN_DISABLED:
					enum_id = 6;
					break;
				case RTR_NULL:
				case RTR_TRUE:
				case RTR_FALSE:
				case RTR_RG_PRIMARIES:
				case RTR_RT_INSTALLED_NODES:
				case RTR_INT:
				case RTR_BOOLEAN:
				case RTR_STRING:
				case RTR_ENUM:
				case RTR_STRINGARRAY:
				case RTR_LOGICAL_HOSTNAME:
				case RTR_SHARED_ADDRESS:
				case RTR_FALSE_UNSET:
					break;
				}
				arrayElement = getCMASSEnum(env,
				    "com/sun/cluster/agent/rgm/"
				    "ResourceTypeVersionTunabilityEnum",
				    enum_id);
				if (arrayElement != NULL) {
					(*env)->SetObjectArrayElement(env,
					    upgrade_tunability_value, j,
					    arrayElement);
					delRef(env, arrayElement);
				}
			}
			PUT_ATTR(instance_map, "UpgradeTunabilities",
				upgrade_tunability_value);

			list = (char **)
			    malloc((size_t) (sizeof (char *) *
				(unsigned int)rt->downgradesz));
			if (list == NULL) {
				goto error;
			}

			for (j = 0; j < rt->downgradesz; j++) {
				list[j] = convertRtrString(
				    rt->downgradearray[j]->upg_version);
			}
			PUT_ATTR(instance_map, "DowngradeVersions",
				newStringArray(env, rt->downgradesz, list));

			for (j = 0; j < rt->downgradesz; j++) {
				if (list[j] != NULL) {
					free(list[j]);
				}
			}
			free(list);

			enumClass = NULL;
			arrayElement = NULL;

			/*
			 * Load Enum class
			 */
			enumClass = (*env)->FindClass(env,
			    "com/sun/cluster/agent/rgm/"
			    "ResourceTypeVersionTunabilityEnum");
			if (enumClass == NULL)
				goto error;

			/*
			 * Construct array
			 */
			downgrade_versions_value = (*env)->NewObjectArray(env,
			    rt->downgradesz, enumClass, NULL);
			delRef(env, enumClass);
			if (downgrade_versions_value == NULL)
				goto error;

			/*
			 * Fill array
			 */
			for (j = 0; j < rt->downgradesz; j++) {
				int enum_id = 0;
				switch (rt->downgradearray[j]->
				    upg_tunability) {
				case RTR_NONE:
				enum_id = 0;
					break;
				case RTR_AT_CREATION:
					enum_id = 1;
					break;
				case RTR_ANYTIME:
					enum_id = 2;
					break;
				case RTR_WHEN_OFFLINE:
					enum_id = 3;
					break;
				case RTR_WHEN_UNMANAGED:
					enum_id = 4;
					break;
				case RTR_WHEN_UNMONITORED:
					enum_id = 5;
					break;
				case RTR_WHEN_DISABLED:
					enum_id = 6;
					break;
				case RTR_NULL:
				case RTR_TRUE:
				case RTR_FALSE:
				case RTR_RG_PRIMARIES:
				case RTR_RT_INSTALLED_NODES:
				case RTR_INT:
				case RTR_BOOLEAN:
				case RTR_STRING:
				case RTR_ENUM:
				case RTR_STRINGARRAY:
				case RTR_LOGICAL_HOSTNAME:
				case RTR_SHARED_ADDRESS:
				case RTR_FALSE_UNSET:
					break;
				}
				arrayElement = getCMASSEnum(env,
				    "com/sun/cluster/agent/rgm/"
				    "ResourceTypeVersionTunabilityEnum",
				    enum_id);
				if (arrayElement != NULL) {
					(*env)->SetObjectArrayElement(env,
					    downgrade_versions_value, j,
						arrayElement);
					delRef(env, arrayElement);
				}
			}
			PUT_ATTR(instance_map, "DowngradeTunabilities",
				downgrade_versions_value);

			sys_prop_value =
				newObjNullParam(env, "java/util/LinkedList");
			if (sys_prop_value == NULL)
				goto error;
			for (j = 0; j < rt->paramtabsz; j++) {
				if ((rt->paramtab[j]->sysparamid !=
					RTR_SP_NONE) ||
				    (rt->paramtab[j]->extension ==
					RTR_FALSE_UNSET)) {
					addRtProperty(env,
					    sys_prop_value,
					    createRtProperty(env,
						rt->paramtab[j]));
				}
			}
			PUT_ATTR(instance_map, "SystemProperties",
				sys_prop_value);

			ext_prop_value =
			    newObjNullParam(env, "java/util/LinkedList");
			if (ext_prop_value == NULL)
				goto error;

			for (j = 0; j < rt->paramtabsz; j++) {
				if (rt->paramtab[j]->extension ==
				    RTR_TRUE) {
					addRtProperty(env,
					    ext_prop_value,
					    createRtProperty(env,
						rt->paramtab[j]));
				}
			}
			PUT_ATTR(instance_map, "ExtProperties",
				ext_prop_value);

			/*
			* Put this map into the instance map
			*/
			PUT(cache_map,
				rt_name, instance_map);

			if (rt != NULL) {
				scrgadm_free_rtrresult(rt);
				rt = NULL;
			}

			if (rt_string != NULL) {
				free(rt_string);
				rt_string = NULL;
			}
			if (rt_name != NULL) {
				free(rt_name);
				rt_name = NULL;
			}

		}
		free(dirent);
		dirent = NULL;
		(void) closedir(dirp);
		dirp = NULL;
	}
error:
	if (rt != NULL) {
		scrgadm_free_rtrresult(rt);
	}
	if (rt_string != NULL) {
		free(rt_string);
	}
	if (rt_name != NULL) {
		free(rt_name);
	}
	delRef(env, attribute_class);
	delRef(env, attribute);
	return (cache_map);
} /*lint !e715 */


/*
 * Class:     com_sun_cluster_agent_rgm_ResourceTypeInterceptor
 * Method:    getRegisteredInstances
 * Signature: ()Ljava/util/HashSet;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_rgm_ResourceTypeInterceptor_getRegisteredInstances
	(JNIEnv *env, jobject this) {
	return (getRegisteredInstances(env, this, NULL));
}

JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_rgm_ResourceTypeInterceptor_getAttributeNative
	(JNIEnv * env,
	jobject this,
	jstring instanceName,
	jstring attributeName) {
    return Java_com_sun_cluster_agent_rgm_RtGroup_getAttributeNative(
		env, this, instanceName, attributeName, NULL);

}

/*
 * Class:     com_sun_cluster_agent_rgm_RtGroup
 * Method:    fillCacheNative
 * Signature: ([Ljava/lang/String;)Ljava/util/HashMap;
 */
JNIEXPORT jobject JNICALL Java_com_sun_cluster_agent_rgm_RtGroup_fillCacheNative
(JNIEnv *env, jobject this, jobjectArray tmpAr) {
    return
	Java_com_sun_cluster_agent_rgm_ResourceTypeInterceptor_fillCacheNative(
		env, this, tmpAr);
}


/*
 * Class:     com_sun_cluster_agent_rgm_RtGroup
 * Method:    getZonePath
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_sun_cluster_agent_rgm_RtGroup_getZonePath
(JNIEnv *env, jobject this, jstring zcNameQ) {

    char buff[MAXPATHLEN];
    char *cZCNameQ = NULL;
    const char *tmpS;
    jstring result_string = NULL;
    int err = 0;

#if SOL_VERSION >= __s10
    zc_dochandle_t zone_handle = NULL;
    if (zcNameQ != NULL) {
	tmpS = (*env)->GetStringUTFChars(env, zcNameQ, NULL);
	cZCNameQ = strdup(tmpS);
	}

	/*
	* Library initialization
	*/
    clconf_lib_init();

	/*
	* Open the handle to zonecfg
	*/
    zone_handle = zccfg_init_handle();
    if (zone_handle == NULL) {
	goto error;
	}
    err = zccfg_get_handle(cZCNameQ, zone_handle);
    if (err != ZC_OK) {
	goto error;
	}

	/*
	* Get the zone path for this zone cluster
	*/
    err = zccfg_get_zonepath(zone_handle, buff, sizeof (buff));
    if (err != ZC_OK) {
	goto error;
	}

    error:
		(void) zccfg_endnodeent(zone_handle);
		(void) zccfg_fini_handle(zone_handle);

#endif /* SOL_VERSION >= __s10 */

    result_string = (jstring) newObjStringParam(env,
		"java/lang/String",
		buff);
    return (result_string);
}



/*
 * Class:     com_sun_cluster_agent_rgm_RtGroup
 * Method:    getNodeList
 * Signature: (Ljava/lang/String;)Ljava/util/ArrayList;
 */
JNIEXPORT jobject JNICALL Java_com_sun_cluster_agent_rgm_RtGroup_getNodeList
(JNIEnv *env, jobject this, jstring zcNameQ) {

    jobject nodeList;
    char local_node[BUFFSIZE];
    char *cZCNameQ = NULL;
    int err = 0;
    jclass array_list_class;
    jclass localClass;
    jmethodID array_list_constructor;
    jmethodID array_list_add;

#if SOL_VERSION >= __s10

    struct zc_nodetab nodetab;
    jstring ret_string;
    zc_dochandle_t zone_handle = NULL;

#endif /* SOL_VERSION >= __s10 */

    if (zcNameQ != NULL) {
	const char *tmpS =
		(*env)->GetStringUTFChars(env, zcNameQ, NULL);
	cZCNameQ = strdup(tmpS);
	}

	/* ArrayList */
    localClass = (*env)->FindClass(env, "java/util/ArrayList");
    array_list_class = (*env)->NewGlobalRef(env, localClass);
    if (array_list_class == NULL) {
	debug printf("\n array list class is null \n");
	goto end;
	}

    array_list_constructor = (*env)->GetMethodID(
	    env, array_list_class,
	    "<init>", "()V");
    if (array_list_constructor == NULL) {
	debug printf("\n array list constructor is null \n");
	goto end;
	}
    array_list_add = (*env)->GetMethodID(env, array_list_class,
	    "add", "(Ljava/lang/Object;)Z");
    if (array_list_add == NULL) {
	debug printf("\n array list add is null \n");
	goto end;
	}


	/*
	* Initialize nodelist
	*/
    nodeList = (*env)->NewObject(env,
				array_list_class,
				array_list_constructor);
    if (nodeList == NULL) {
	goto end;
	}

    local_node[0] = '\0';
	(void) gethostname(local_node, sizeof (local_node));

#if SOL_VERSION >= __s10
	/*
	* Library initialization
	*/

    clconf_lib_init();

	/*
	* Open the handle to zonecfg
	*/
    zone_handle = zccfg_init_handle();
    if (zone_handle == NULL) {
	goto error;
	}
    err = zccfg_get_handle(cZCNameQ, zone_handle);
    if (err != ZC_OK) {
	goto error;
	}

	/*
	* Get the node list for this zone cluster
	*/
    err = zccfg_setnodeent(zone_handle);
    if (err != ZC_OK) {
	goto error;
	}

    while (zccfg_getnodeent(zone_handle, &nodetab) == ZC_OK) {
	ret_string = (jstring) newObjStringParam(env,
			"java/lang/String",
			nodetab.zc_nodename);

	if (ret_string != NULL &&
		strcmp(nodetab.zc_nodename, local_node) != 0) {
		/* Update Nodelist */
		(void) (*env)->CallObjectMethod(env,
				nodeList,
				array_list_add,
				ret_string);
	} else {
		nodeList = (*env)->NewObject(env,
				array_list_class,
				array_list_constructor);
		break;
	}

	}

    error:
	(void) zccfg_endnodeent(zone_handle);
	(void) zccfg_fini_handle(zone_handle);

#endif /* SOL_VERSION >= __s10 */

    end:
	return (nodeList);
}
