/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RgGroup.java	1.7	08/12/19 SMI"
 */
package com.sun.cluster.agent.rgm;


import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

import com.sun.cluster.common.ClusterPaths;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.TrustedMBeanModel;

// Cacao
import com.sun.cacao.invocation.InvokeCommand;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;
import com.sun.cacao.ObjectNameFactory;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;

/**
 * Class implementing MBean interface, holding utility functions to read
 * ResourceGroup data
 */
public class RgGroup implements RgGroupMBean {

    private static Logger logger =
            Logger.getLogger("com.sun.cluster.agent.rgm");
    private static final String logTag = "RgGroup";
    private static boolean quiet = false;
    private static final String lib = ClusterPaths.cmass +
                                            "libcmas_agent_rgm.so";

    private static final String DOMAIN_RGM = "com.sun.cluster.agent.rgm";

    private MBeanServer mBeanServer = null;

    private ArrayList commandList = null;

    public RgGroup() {
        this(null);
    }

    public RgGroup(MBeanServer mbs) {
        this.mBeanServer = mbs;
    }

    /**
     * Utility method to retrieve data corresponding to ResourceGroups.
     *
     * @param zoneClusterName for which the query is to be done. If this is
     *        null, the method would return all the ResourceGroups present in
     *        the cluster it is run.
     * @return HashMap with following structure
     *                  KEY                     VALUE
     *                  ResourceGroupName       ResourceGroupData Object
     */
    public HashMap readData(String zoneClusterName) {
        HashMap rgDataMap = new HashMap();
        RgGroupMBean rgGroupMBean = null;

        if (mBeanServer != null && zoneClusterName != null) {
            /**
             * If the given zone cluster is not hosted on this node, we cannot
             * get the related rg information. Hence get connection to cacao to
             * a node which hosts the given zone cluster, and then get the rg
             * information.
             */

            // get the nodelist on which given zonecluster is hosted
            // This nodelist would be empty if the zone cluster is hosted on
            // local host.
            ArrayList nodeList = getNodeList(zoneClusterName);

            if (nodeList != null && nodeList.size() > 0) {

                // Connect to any of the node in the nodelist
                ObjectNameFactory rgGroupOnf = new ObjectNameFactory(
                                                                DOMAIN_RGM);
                ObjectName rgGroupMBeanObjName = rgGroupOnf.getObjectName(
                        RgGroupMBean.class, null);

                Iterator iterNodeList = nodeList.iterator();
                while (iterNodeList.hasNext()) {
                    try {
                        MBeanServerConnection mbs = getMBeanServerConnection(
                                        (String)iterNodeList.next());
                        if (mbs == null) {
                            // unable to connect to a node in the nodelist
                            // try with next
                            continue;
                        }
                        rgGroupMBean = (RgGroupMBean)
                            MBeanServerInvocationHandler.newProxyInstance(
                                        mbs,
                                        rgGroupMBeanObjName,
                                        RgGroupMBean.class, false);
                        if (rgGroupMBean != null) {
                            // Got connection
                            break;
                        }
                    } catch (Exception ex) {
                        // Do nothing
                    }
                }
            }
        }

        // Get the rg data
        Object tmpObj = null;
        if (rgGroupMBean != null) {
            tmpObj = rgGroupMBean.readData(zoneClusterName);
        } else {
            tmpObj = readDataJNI(zoneClusterName);
        }

        if (tmpObj != null) {
            rgDataMap = (HashMap)tmpObj;
        }
        return rgDataMap;
    }

    private native HashMap readDataJNI(String zoneClusterName);
    private native ArrayList getNodeList(String zoneClusterName);

    /**
     * This method gets an instance of ResourceGroupData matching the
     * resource group name in the given zonecluster.
     *
     * @param name the name of the resource group
     * @param zoneClusterName the name of the zone cluster or null for current
     * cluster scope.
     * @return a ResourceGroupData object
     */
    public ResourceGroupData getResourceGroup(String name,
            String zoneClusterName) {
	HashMap rgDataMap = readData(zoneClusterName);
	ResourceGroupData rgData = null;
	Object obj = rgDataMap.get(name);
	if (obj != null && obj instanceof ResourceGroupData) {
	    rgData = (ResourceGroupData) obj;
	}
	return rgData;
    }

    /**
     * This method creates a new instance of a Resource Group with its default
     * system values
     *
     * @param  name  the name of the resource group. This name should be unique
     * and can not be modified after the creation of the Resource Group.
     * @param  properties  the properties associated to this resource group
     * (might be null).
     * @param zoneClusterName name of the zone cluster. If null, assume current
     * cluster scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addResourceGroup(String name, Map properties,
	String zoneClusterName, boolean commit)
	throws CommandExecutionException {

	// Verify that the resource group name is not already used by another
	// resource group.
	if (!commit) {
	    if (isResourceGroupRegistered(name, zoneClusterName)) {
		throw new IllegalArgumentException("Resource Group " + name +
		    " already registered.");
	    }
	}

	commandList = new ArrayList();
	String command[] = null;

	commandList.add(ClusterPaths.CL_RG_CMD);
	commandList.add("create");

	if (zoneClusterName != null && zoneClusterName.trim().length() > 0) {
	    commandList.add(ClusterPaths.ZC_OPTION);
	    commandList.add(zoneClusterName);
	}

	if (properties != null) {
	    Iterator propertyNames = properties.keySet().iterator();

	    while (propertyNames.hasNext()) {
		String propertyName = (String) propertyNames.next();
		commandList.add("-p");
		commandList.add(propertyName + "=" +
		    (String) properties.get(propertyName));
	    }
	}

	commandList.add(name);
	return invokeCommand(commandList, zoneClusterName, commit);
    }



    /**
     * This method checks if there is already a resource group by the specified
     * name.
     * @param name the name of the resource group.
     * @param zoneClusterName the name of the zone cluster or null for current
     * cluster scope.
     * @return a boolean; true if the resource group name already exists and
     * false otherwise.
     */
    public boolean isResourceGroupRegistered(String name,
            String zoneClusterName) {
	HashMap rgDataMap = readData(zoneClusterName);

	return (rgDataMap.get(name) != null);
    }

    /**
     * Change a set of properties for this resource group.
     *
     * @param name the name of the resource group
     * @param  newProperties  the new properties associated to this resource
     * group.
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeProperties(String name, Map newProperties,
	String zoneClusterName, boolean commit)
        throws CommandExecutionException {
	commandList = new ArrayList();
	commandList.add(ClusterPaths.CL_RG_CMD);
	commandList.add("set");

	Iterator propertyNames = newProperties.keySet().iterator();

	while (propertyNames.hasNext()) {
	    String propertyName = (String) propertyNames.next();
	    commandList.add("-p");
	    commandList.add(propertyName + "=" +
		(String) newProperties.get(propertyName));
	}
	if (zoneClusterName != null &&
	    zoneClusterName.trim().length() > 0) {
	    commandList.add(ClusterPaths.ZC_OPTION);
	    commandList.add(zoneClusterName);
	}
	commandList.add(name);
	return invokeCommand(commandList, zoneClusterName, true);
    }

    /**
     * Change resource group's dependencies on other resource groups.
     *
     * @param name the name of the resource group
     * @param  newDependencies  the new set of resource group's dependencies.
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeDependencies(String name,
            String newDependencies[],
	String zoneClusterName, boolean commit)
        throws CommandExecutionException {
	String list = "";
	if (newDependencies.length > 0) {
	    list = newDependencies[0];

	    for (int i = 1; i < newDependencies.length; i++) {
		list = list.concat("," + newDependencies[i]);
	    }
	}

	commandList = new ArrayList();
	commandList.add(ClusterPaths.CL_RG_CMD);
	commandList.add("set");
	commandList.add("-p");
	commandList.add(ResourceGroupMBean.DEPEND + "=" + list);
	if (zoneClusterName != null &&
	    zoneClusterName.trim().length() > 0) {
	    commandList.add(ClusterPaths.ZC_OPTION);
	    commandList.add(zoneClusterName);
	}
	commandList.add(name);

	return invokeCommand(commandList, zoneClusterName, true);
    }

    /**
     * Delete this resource group.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] remove(String name, String zoneClusterName)
	throws CommandExecutionException {
	commandList = constructCommand(name, zoneClusterName, "delete");
	return invokeCommand(commandList, zoneClusterName, true);
    }

    /**
     * Stop and restart this resource group.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] restart(String name, String zoneClusterName)
	throws CommandExecutionException {
	commandList = constructCommand(name, zoneClusterName, "restart");
	return invokeCommand(commandList, zoneClusterName, true);
    }

    /**
     * Put this resource group online.
     *
     * @param name the name of the resource group.
     * @param zoneClusterName the name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOnline(String name, String zoneClusterName)
	 throws CommandExecutionException {
	commandList = new ArrayList();
	commandList.add(ClusterPaths.CL_RG_CMD);
	commandList.add("online");
	commandList.add("-emM");

	if (zoneClusterName != null && zoneClusterName.trim().length() > 0) {
	    commandList.add(ClusterPaths.ZC_OPTION);
	    commandList.add(zoneClusterName);
	}

	commandList.add(name);
	return invokeCommand(commandList, zoneClusterName, true);
    }

    /**
     * Put this resource group online.
     *
     * @param name the name of the resource group.
     * @param zoneClusterName the name of the zone cluster or null for current
     * cluster scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOnline(String name, String zoneClusterName,
	boolean commit)
	 throws CommandExecutionException {
	commandList = new ArrayList();
	commandList.add(ClusterPaths.CL_RG_CMD);
	commandList.add("online");
	commandList.add("-emM");

	if (zoneClusterName != null && zoneClusterName.trim().length() > 0) {
	    commandList.add(ClusterPaths.ZC_OPTION);
	    commandList.add(zoneClusterName);
	}

	commandList.add(name);
	return invokeCommand(commandList, zoneClusterName, commit);
    }

    /**
     * Put this resource group offline.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOffline(String name, String zoneClusterName)
	throws CommandExecutionException {
	commandList = constructCommand(name, zoneClusterName, "offline");
	return invokeCommand(commandList, zoneClusterName, true);
    }

    /**
     * Put this resource group in an 'managed' state.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] manage(String name, String zoneClusterName)
	throws CommandExecutionException {
	commandList = constructCommand(name, zoneClusterName, "manage");
	return invokeCommand(commandList, zoneClusterName, true);
    }

    /**
     * Put this resource group in an 'unmanaged' state.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] unmanage(String name, String zoneClusterName)
	throws CommandExecutionException {
	commandList = constructCommand(name, zoneClusterName, "unmanage");
	return invokeCommand(commandList, zoneClusterName, true);
    }

    /**
     * Suspend this resource groups.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] suspend(String name, String zoneClusterName)
	throws CommandExecutionException {
	commandList = constructCommand(name, zoneClusterName, "suspend");
	return invokeCommand(commandList, zoneClusterName, true);
    }


    /**
     * Switch the primaries of this resource group.
     *
     * @param name the name of the resource group
     * @param  newPrimaries  an array of node names.
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] switchPrimaries(String name, String[] newPrimaries,
            String zoneClusterName) throws CommandExecutionException {
        ArrayList cmdList = new ArrayList();
	cmdList.add(ClusterPaths.CL_RG_CMD);
	cmdList.add("switch");

	if (zoneClusterName != null &&
	    zoneClusterName.trim().length() > 0) {
	    cmdList.add(ClusterPaths.ZC_OPTION);
	    cmdList.add(zoneClusterName);
	}

        cmdList.add("-n");

        StringBuffer primariesList = new StringBuffer();
        if (newPrimaries.length > 0) {
            primariesList.append(newPrimaries[0]);
            for (int i = 1; i < newPrimaries.length; i++) {
                primariesList.append(",");
                primariesList.append(newPrimaries[i]);
            }
        }
        if (primariesList.length() != 0) {
            cmdList.add(primariesList.toString());
        }

        cmdList.add(name);

        return invokeCommand(cmdList, zoneClusterName, true);
    }



    private ArrayList constructCommand(String name, String zoneClusterName,
            String cmdOption) {
	ArrayList cmdList = new ArrayList();
	cmdList.add(ClusterPaths.CL_RG_CMD);
	cmdList.add(cmdOption);

	if (zoneClusterName != null &&
	    zoneClusterName.trim().length() > 0) {
	    cmdList.add(ClusterPaths.ZC_OPTION);
	    cmdList.add(zoneClusterName);
	}

	cmdList.add(name);
	return cmdList;
    }



    private ExitStatus[] invokeCommand(List commandList, String zoneClusterName,
            boolean commit)
	throws CommandExecutionException {
	String[] command = (String[]) commandList.toArray(new String[0]);
	InvocationStatus result[] = null;
	ExitStatus resultWrapper[] = null;

	if (commit) {
	    try {
		result = InvokeCommand.execute(new String[][] { command },
			    null);
		resultWrapper = ExitStatus.createArray(result);
	    } catch (InvocationException ie) {
		result = ie.getInvocationStatusArray();
		throw new CommandExecutionException(ie.getMessage(),
		    ExitStatus.createArray(result));
	    }
	    // Refresh cache
	    readData(zoneClusterName);
	} else {
	    resultWrapper = new ExitStatus[] {
		new ExitStatus(ExitStatus.escapeCommand(command),
			ExitStatus.SUCCESS, null, null)
	    };

	}

	return resultWrapper;
    }



    private MBeanServerConnection getMBeanServerConnection(String nodeName) {

        MBeanServerConnection mbsConnection = null;
        try {
            // first get the local connection
            HashMap env = new HashMap();
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                this.getClass().getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());

            JMXConnector connectors = TrustedMBeanModel.getWellKnownConnector(
                    nodeName, env);

            // Now get a reference to the mbean server connection
            mbsConnection = connectors.getMBeanServerConnection();

        } catch (Exception ex) {
            mbsConnection = null;
        }
        return mbsConnection;
    }



    /**
     *
     * @param args  -n <n>	repeat test <n> times
     *		    -q		quiet mode
     *		    -z		zonecluster name
     *
     * @return boolean status of readData method
     */
    public static boolean test(String[] args) {
        RgGroup rg = new RgGroup();

        int iterations = 1;
        String zcNameQ = null;

        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-q")) {
                quiet = true;
            } else if (args[i].equals("-n")) {
                iterations = Integer.parseInt(args[++i]);
            } else if (args[i].equals("-z")) {
                zcNameQ = args[++i];
            }
        }

        try {
            // Load the agent JNI library
            Runtime.getRuntime().load(lib);
        } catch (UnsatisfiedLinkError ule) {
            print("load of " + lib + " failed!");
            ule.printStackTrace();
            return false;
        }

        long start = System.currentTimeMillis();
        long loopstart;
        long loopelapsed;
        int count = 0;
        print("\nReading data " + iterations + " time(s) ... ");
        for (int i = 0; i < iterations; i++) {
            try {
                loopstart = System.currentTimeMillis();
                HashMap rgDataMap;
		ArrayList nodeList;
                rgDataMap = rg.readData(zcNameQ);
		nodeList = rg.getNodeList(zcNameQ);
                if (rgDataMap == null) {
                    print("readData returned null!");
                    return false;
                }
		if (nodeList == null) {
                    print("nodeList returned null!");
                    return false;
                }
                count = i + 1;
                print("\nREAD " + count + " : returned " + rgDataMap.size()
                        + " resourceGroups");
		print("\nREAD " + count + " : returned " + nodeList.size()
                        + " nodeList");
                if (!quiet) {
                    print("ResourceGroups : \n" + rgDataMap);
		    print("NodeList : \n" + nodeList);
                }
                loopelapsed = System.currentTimeMillis() - loopstart;
                print("time = " + loopelapsed + " ms");
            } catch (Exception e) {
                print("readDataJNI failed!");
                print(e.getMessage());
                e.printStackTrace();
                return false;
            }
        }
        quiet = false;
        long elapsed = System.currentTimeMillis() - start;
        print("\nTotal Time for " + iterations + " iterations = " +
                        elapsed + " ms");

        return true;
    }

    public static void print(String msg) {
        System.out.println(msg);
        logger.fine(msg);
    }
}
