/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceRgmState.java 1.6     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm;

/**
 * Struct-like object used to represent a resource's RGM state on an identified
 * node.
 */
public class ResourceRgmState implements java.io.Serializable {

    private String nodeName;
    private ResourceRgmStateEnum state;

    /**
     * Default empty constructor
     */
    public ResourceRgmState() {
    }

    /**
     * Constructor with initial values.
     *
     * @param  nodeName  the identified node's name
     * @param  state  a {@link ResourceRgmStateEnum} element representing the
     * resource's state on this node
     */
    public ResourceRgmState(String nodeName, ResourceRgmStateEnum state) {
        this.nodeName = nodeName;
        this.state = state;
    }

    /**
     * Get the concerned node's name.
     *
     * @return  the node's name
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String getNodeName() {
        return this.nodeName;
    }

    /**
     * Get the resource's state on the concerned node.
     *
     * @return  the resource's state
     */
    public ResourceRgmStateEnum getState() {
        return this.state;
    }

    /**
     * Set the concerned node's name.
     *
     * @param  nodeName  the reference name of the node
     */
    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    /**
     * Set the resource's state.
     *
     * @param  state  a {@link ResourceRgmStateEnum} element representing the
     * resource's state
     */
    public void setState(ResourceRgmStateEnum state) {
        this.state = state;
    }

}
