/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourcePropertyString.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm.property;

import java.util.HashMap;
import java.util.Iterator;


/**
 * Object describing a string resource property, with built-in range and type
 * information and checking.
 */
public class ResourcePropertyString extends ResourceProperty {

    private Integer minLength;
    private Integer maxLength;
    private String defaultValue;
    private String value;
    private HashMap pernodeValue;
    private HashMap pernodeDefValueMap;

    public ResourcePropertyString() {
        super();
    }

    public ResourcePropertyString(String name, boolean extension,
        boolean per_node, boolean required, String description,
        ResourcePropertyEnum tunable, Integer minLength, Integer maxLength,
        String defaultValue, String value, HashMap pernodeValue) {

        super(name, extension, per_node, required, description, tunable);

        this.minLength = minLength;
        this.maxLength = maxLength;
        this.defaultValue = defaultValue;

        if (value != null) {
            setValue(value);
        } else if (pernodeValue != null) {
            setPernodeValue(pernodeValue);
        }
    }

    /**
     * constructor used for system immutable properties
     */
    public ResourcePropertyString(String name, String value) {

        super(name, false, false, false, "Immutable",
            new Tunable(Tunable.FALSE));

        this.minLength = null;
        this.maxLength = null;
        this.defaultValue = null;

        if (value != null) {
            setValue(value);
        } else if (pernodeValue != null) {
            setPernodeValue(pernodeValue);
        }
    }

    public Integer getMinLength() {
        return minLength;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public HashMap getPernodeValue() {
        return pernodeValue;
    }

    public String getValue() {
        return value;
    }

    public void setMinLength(Integer minLength) {
        this.minLength = minLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public void setValue(String newValue) {

        if ((newValue == null) ||
                ((minLength != null) &&
                    (newValue.length() < minLength.intValue())) ||
                ((maxLength != null) &&
                    (newValue.length() > maxLength.intValue()))) {

            throw new IllegalArgumentException("value out of range:" +
                newValue);
        }

        this.value = newValue;
    }

    public void setPernodeValue(HashMap pernodeValue) {

        if (pernodeValue == null) {
            this.pernodeValue = null;

            return;
        }

        Iterator i = pernodeValue.keySet().iterator();
        String nodename;
        String newValue;

        while (i.hasNext()) {
            nodename = (String) i.next();
            newValue = (String) pernodeValue.get(nodename);

            if ((newValue == null) ||
                    ((minLength != null) &&
                        (newValue.length() < minLength.intValue())) ||
                    ((maxLength != null) &&
                        (newValue.length() > maxLength.intValue()))) {

                throw new IllegalArgumentException("value out of range:" +
                    newValue);
            }
        }

        this.pernodeValue = pernodeValue;
    }

    public void storePernodeValueMap(HashMap pernodeDefValueMap) {
        this.pernodeDefValueMap = pernodeDefValueMap;
    }

    public HashMap retrievePernodeValueMap() {
        return this.pernodeDefValueMap;
    }

    public String toString() {
        return super.toString() + " MIN_LENGTH=" + minLength + " MAX_LENGTH=" +
            maxLength + " DEFAULT=" + defaultValue + " VALUE=" + value +
            "PERNODEVALUE=" + pernodeValue + ">";
    }

}
