/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourcePropertyBoolean.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm.property;

import java.util.HashMap;
import java.util.Iterator;


/**
 * object describing an enum resource property, with built-in range and type
 * information and checking.
 */
public class ResourcePropertyBoolean extends ResourceProperty {

    private Boolean defaultValue;
    private Boolean value;
    private HashMap pernodeValue;
    private HashMap pernodeDefValueMap;

    public ResourcePropertyBoolean() {
        super();
    }

    public ResourcePropertyBoolean(String name, boolean extension,
        boolean per_node, boolean required, String description,
        ResourcePropertyEnum tunable, Boolean defaultValue, Boolean value,
        HashMap pernodeValue) {

        super(name, extension, per_node, required, description, tunable);

        this.defaultValue = defaultValue;

        if (per_node)
            setPernodeValue(pernodeValue);
        else
            setValue(value);
    }

    public ResourcePropertyBoolean(String name, Boolean value) {

        super(name, false, false, false, "Immutable",
            new Tunable(Tunable.FALSE));

        this.defaultValue = null;

        setValue(value);
    }

    public Boolean getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(Boolean defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Boolean getValue() {
        return value;
    }

    public HashMap getPernodeValue() {
        return pernodeValue;
    }

    public void setValue(Boolean newValue) {
        value = newValue;
    }

    public void setPernodeValue(HashMap pernodeValue) {

        if (pernodeValue == null) {
            this.pernodeValue = null;

            return;
        }

        this.pernodeValue = pernodeValue;
    }

    public void storePernodeValueMap(HashMap pernodeDefValueMap) {
        this.pernodeDefValueMap = pernodeDefValueMap;
    }

    public HashMap retrievePernodeValueMap() {
        return this.pernodeDefValueMap;
    }

    public String toString() {
        return super.toString() + " DEFAULT=" + defaultValue + " VALUE=" +
            value + "PERNODEVALUE=" + pernodeValue + ">";
    }
}
