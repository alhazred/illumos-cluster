/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RGroup.java	1.9	09/04/22 SMI"
 */
package com.sun.cluster.agent.rgm;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import com.sun.cluster.common.ClusterPaths;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyEnum;
import com.sun.cluster.common.TrustedMBeanModel;

// Cacao
import com.sun.cacao.invocation.InvokeCommand;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;
import com.sun.cacao.ObjectNameFactory;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;

/**
 * Class implementing MBean interface, holding utility functions to read
 * Resource data
 */
public class RGroup implements RGroupMBean {

    private static Logger logger =
            Logger.getLogger("com.sun.cluster.agent.rgm");
    private static final String logTag = "RGroup";
    private static boolean quiet = false;
    private static final String lib = ClusterPaths.cmass +
                                                "libcmas_agent_rgm.so";

    private static final String DOMAIN_RGM = "com.sun.cluster.agent.rgm";

    private MBeanServer mBeanServer = null;

    private ArrayList commandList = null;

    public RGroup() {
	this(null);
    }

    public RGroup(MBeanServer mbs) {
        this.mBeanServer = mbs;
    }

    /**
     * Utility method to read data corresponding to Resources. This
     * method would return all the Resources corresponding to the given
     * ZoneCluster and ResourceGroup combination.
     *
     * @param zoneClusterName If this is null the method would return all the
     *                        resources present in the cluster it is run.
     * @param resourceGroupName If this is null the method would return all the
     *                          resources for the given ZoneCluster name.
     *
     * @return HashMap with following structure
     *         KEY                     VALUE
     *         ResourceGroupName       HashMap object with following structure
     *                                  KEY                 VALUE
     *                                  ResourceName        ResourceData Object
     */
    public HashMap readData(String zoneClusterName, String resourceGroupName) {
        HashMap rDataMap = new HashMap();
	RGroupMBean rGroupMBean = null;


        if (mBeanServer != null && zoneClusterName != null) {
            /**
             * If the given zone cluster is not hosted on this node, we cannot
             * get the related r information. Hence get connection to cacao to
             * a node which hosts the given zone cluster, and then get the r
             * information.
             */

            // get the nodelist on which given zonecluster is hosted
            // This nodelist would be empty if the zone cluster is hosted on
            // local host.
            ArrayList nodeList = getNodeList(zoneClusterName);

            if (nodeList != null && nodeList.size() > 0) {

                // Connect to any of the node in the nodelist
                ObjectNameFactory rgGroupOnf = new ObjectNameFactory(
                                                                DOMAIN_RGM);
                ObjectName rGroupMBeanObjName = rgGroupOnf.getObjectName(
                        RGroupMBean.class, null);

                Iterator iterNodeList = nodeList.iterator();
                while (iterNodeList.hasNext()) {
                    try {
                        MBeanServerConnection mbs = getMBeanServerConnection(
                                        (String)iterNodeList.next());
                        if (mbs == null) {
                            // unable to connect to a node in the nodelist
                            // try with next
                            continue;
                        }
                        rGroupMBean = (RGroupMBean)
                            MBeanServerInvocationHandler.newProxyInstance(
                                        mbs,
                                        rGroupMBeanObjName,
                                        RGroupMBean.class, false);
                        if (rGroupMBean != null) {
                            // Got connection
                            break;
                        }
                    } catch (Exception ex) {
                        // Do nothing
                    }
                }
            }
        }

        // Get the r data
        Object tmpObj = null;
        if (rGroupMBean != null) {
            tmpObj = rGroupMBean.readData(zoneClusterName, resourceGroupName);
        } else {
            tmpObj = readDataJNI(zoneClusterName, resourceGroupName);
        }

        if (tmpObj != null) {
            rDataMap = (HashMap)tmpObj;
        }
        return rDataMap;
    }


    private native HashMap readDataJNI(
	String zoneClusterName, String resourceGroupName);
    private native ArrayList getNodeList(String zoneClusterName);


    /**
     * Utility method to read ResourceData for a resource.
     *
     * @param name the name of the resource
     * @param zoneClusterName the name of the zone cluster
     *
     */

    public ResourceData getResourceData(String name, String zoneClusterName) {
        HashMap rgrDataMap = readData(zoneClusterName, null);

        Set keySet = rgrDataMap.keySet();
        Iterator iterator = keySet.iterator();
        boolean found = false;
	ResourceData rData = null;

        while (iterator.hasNext() && !found) {
            String rgName = (String) iterator.next();
            HashMap rDataMap = (HashMap) rgrDataMap.get(rgName);

	    rData = (ResourceData) rDataMap.get(name);
	    if (rData != null) {
                found = true;
            }
        }

        return rData;
    }

    /**
     * Utility function to retrieve data corresponding to given resource of the
     * given type and with a particular property name and value.
     * The method would search the resource type in the given zonecluster.
     * @param zoneClusterName If this is null the resource search would be in
     *                        physical cluster or in the cluster in which this
     *                        method is run.
     * @param resourceType the resource type to search for
     * @param propertyName the property name to search for
     * @param propertyValue the property value to search for
     * @return list of resource names
     * @throws java.io.IOException
     */
    public List getResourceList(
	String zoneClusterName,
	String resourceType,
	String propertyName,
	Object propertyValue) {

	HashMap rgrDataMap = readData(zoneClusterName, null);
	ArrayList resourceList = null;

        Set keySet = rgrDataMap.keySet();
        Iterator iterator = keySet.iterator();
        boolean found = false;
	ResourceData rData = null;

	while (iterator.hasNext()) {
	    String rgName = (String) iterator.next();
	    HashMap rDataMap = (HashMap) rgrDataMap.get(rgName);

	    Iterator iteratorRS = rDataMap.keySet().iterator();

	    while (iteratorRS.hasNext()) {
		rData = (ResourceData) rDataMap.get(iteratorRS.next());

		String type = rData.getType();

		if (type.startsWith(resourceType)) {
		    // find a matching propertyName and value
		    found = false;

		    // Find the property name in Extension Properties
		    List extProps = rData.getExtProperties();
		    Iterator extPropIter = extProps.iterator();

		    while (extPropIter.hasNext() && !found) {
			ResourceProperty rp =
                                (ResourceProperty) extPropIter.next();

			if (isPropertyValueEqual(rp,
                                propertyName, propertyValue)) {
			    if (resourceList == null) {
				resourceList = new ArrayList();
			    }

			    found = true;
			    resourceList.add(rData.getName());
			}
		    }

		    // If not found, find the property name in System Properties
		    if (!found) {
			List sysProps = rData.getSystemProperties();
			Iterator sysPropIter = sysProps.iterator();

			while (sysPropIter.hasNext() && !found) {
			    ResourceProperty rp =
                                    (ResourceProperty) sysPropIter.next();

			    if (isPropertyValueEqual(rp,
                                    propertyName, propertyValue)) {
				found = true;
				if (resourceList == null) {
				    resourceList = new ArrayList();
				}
				resourceList.add(rData.getName());
			    }
			}
		    }
		}
	    }
	}
	return resourceList;
    }

    /**
     * Checks for a Property Name and Value against a ResourceProperty Object
     *
     * @param  rp  ResourcePropertyObject
     * @param  propertyName  PropertyName to check
     * @param  propertyValue  PropertyValue to check
     */
    public boolean isPropertyValueEqual(ResourceProperty rp,
        String propertyName, Object propertyValue) {


        String entry = rp.getName().trim();

        if (entry.equals(propertyName)) {

            // If Boolean Property
            if (rp instanceof ResourcePropertyBoolean) {
                ResourcePropertyBoolean rpBool = (ResourcePropertyBoolean) rp;
                Boolean value = (Boolean) propertyValue;

                if (value.equals(rpBool.getValue())) {
                    return true;
                }
            }

            // If Integer Property
            if (rp instanceof ResourcePropertyInteger) {
                ResourcePropertyInteger rpInt = (ResourcePropertyInteger) rp;
                Integer value = (Integer) propertyValue;

                if (value.equals(rpInt.getValue())) {
                    return true;
                }
            }
            // If Enum Property
            if (rp instanceof ResourcePropertyEnum) {
                ResourcePropertyEnum rpEnum = (ResourcePropertyEnum) rp;

                if (rpEnum.getEnumList().containsAll((List) propertyValue)) {
                    return true;
                }
            }

            // If String Property
            if (rp instanceof ResourcePropertyString) {
                ResourcePropertyString rpStr = (ResourcePropertyString) rp;
                String value = (String) propertyValue;

                if (value.equals(rpStr.getValue())) {
                    return true;
                }
            }

            // If String[] Property
            if (rp instanceof ResourcePropertyStringArray) {
                ResourcePropertyStringArray rpStrArr =
                    (ResourcePropertyStringArray) rp;
                String valueArr[] = (String[]) propertyValue;
                String matchArr[] = rpStrArr.getValue();

                for (int i = 0; i < matchArr.length; i++) {
                    String match = matchArr[i];

                    for (int j = 0; j < valueArr.length; j++) {
                        String value = valueArr[j];

                        if (value.equals(match)) {
                            return true;
                        }
                    }
                }
            }
        }
	return false;
    }


    /**
     * This method creates a new instance of a Resource with its default system
     * values.
     *
     * @param  name  the name of the resource. This name should be unique and
     * can not be modified after the creation of the Resource.
     * @param  rgName  the name of the resource group owning this new resource.
     * @param  type  the resource type used for the creation of this resource.
     * @param  systemProperties  the system properties associated to this
     * resource (might be null).
     * @param  extProperties  the extended properties associated to this
     * resource (might be null).
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addResource(String name, String rgName, String type,
	String zoneClusterName, List systemProperties, List extProperties,
	boolean commit) throws CommandExecutionException {

	commandList = new ArrayList();

	// Verify that the resource name is not already used by another
	// resource.
	if (!commit) {
            if (isResourceRegistered(name, rgName, zoneClusterName)) {
		throw new IllegalArgumentException("Resource " + name +
		    " already registered.");
	    }
	}

	int j = 0;
	String clCommand = null;

	// Using boolean so as to avoid repetitive string checking
	boolean isLogHostType = false;
	boolean isSharedAddType = false;

	if (type.equals("SUNW.LogicalHostname") ||
	    type.startsWith("SUNW.LogicalHostname:")) {

	    clCommand = ClusterPaths.CL_RES_LH_CMD;
	    isLogHostType = true;
	} else if (type.equals("SUNW.SharedAddress") ||
	    type.startsWith("SUNW.SharedAddress:")) {

	    clCommand = ClusterPaths.CL_RES_SA_CMD;
	    isSharedAddType = true;
	} else {

	    clCommand = ClusterPaths.CL_RES_CMD;
	}

	commandList.add(clCommand);
	commandList.add("create");

	if (zoneClusterName != null && zoneClusterName.trim().length() > 0) {
	    commandList.add(ClusterPaths.ZC_OPTION);
	    commandList.add(zoneClusterName);
	}

	// Set special flag for LogicalHostname or SharedAddress
	if (!isLogHostType && !isSharedAddType) {
	    commandList.add("-t");
	    commandList.add(type);
	}

	int commandIndex = 2;

	commandList.add("-g");
	commandList.add(rgName);

	// Parse system properties
	if ((systemProperties != null) && (systemProperties.size() != 0)) {
	    // Use a hashmap to remove duplicate properties
	    HashMap sysPropMap = new HashMap();
	    Iterator props = systemProperties.iterator();
	    String propName = null;

	    while (props.hasNext()) {
		ResourceProperty prop = (ResourceProperty) props.next();
		propName = prop.getName();

		String currPropVal = getPropertyValue(prop);
		String prevPropVal = (String) sysPropMap.get(propName);

		if (prevPropVal != null) {
		    StringBuffer newVal = new StringBuffer();
		    newVal.append(prevPropVal);
		    newVal.append(",");
		    newVal.append(currPropVal);
		    sysPropMap.put(propName, newVal.toString());

		} else {
		    sysPropMap.put(propName, currPropVal);
		}
	    }

	    Iterator mapInter = sysPropMap.keySet().iterator();

	    while (mapInter.hasNext()) {
		propName = (String) mapInter.next();

		String propVal = (String) sysPropMap.get(propName);
		commandList.add("-p");
		commandList.add(propName + "=" + propVal);
	    }
	}

	// Parse extended properties
	if ((extProperties != null) && (extProperties.size() != 0)) {
	    Iterator props = extProperties.iterator();

	    while (props.hasNext()) {
		ResourceProperty prop = (ResourceProperty) props.next();

		if (prop.getName().equals("HostnameList")) {
		    commandList.add("-h");
		    commandList.add(getPropertyValue(prop));
		} else if (prop.getName().equals("NetIfList")) {
		    commandList.add("-N");
		    commandList.add(getPropertyValue(prop));
		} else if (prop.getName().equals("AuxNodeList")) {
		    commandList.add("-X");
		    commandList.add(getPropertyValue(prop));
		} else {
		    boolean hasPernodeValue = false;

		    // Set the per-node extension property, for properties
		    // which have been set per-node values.
		    if (prop.isPernode()) {
			if (prop instanceof ResourcePropertyString) {
			    ResourcePropertyString rps =
				(ResourcePropertyString) prop;
			    HashMap pernodeMap = rps.getPernodeValue();

			    if (pernodeMap != null) {
				Iterator i = pernodeMap.keySet().iterator();

				while (i.hasNext()) {
				    String nodename = (String) i.next();
				    String pernodeValue = (String) pernodeMap
					.get(nodename);

				    if (!pernodeValue.equals(
					rps.getDefaultValue())) {
					commandList.add("-p");
					commandList.add(prop.getName() + "{" +
					    nodename + "}" + "=" +
					    pernodeValue);
					hasPernodeValue = true;
				    }
				}

				// Go to the beginning of the property loop.
				continue;
			    }
			} else if (prop instanceof ResourcePropertyInteger) {
			    ResourcePropertyInteger rpi =
				(ResourcePropertyInteger) prop;

			    HashMap pernodeMap = rpi.getPernodeValue();

			    if (pernodeMap != null) {
				Iterator i = pernodeMap.keySet().iterator();

				while (i.hasNext()) {
				    String nodename = (String) i.next();
				    Integer pernodeValue = (Integer) pernodeMap
					.get(nodename);

				    if (!pernodeValue.equals(
					rpi.getDefaultValue())) {

					commandList.add("-p");
					commandList.add(prop.getName() + "{" +
					    nodename + "}" + "=" +
					    pernodeValue.toString());
					hasPernodeValue = true;
				    }

				}

				// Go to the beginning of the property loop.
				continue;
			    }
			} else if (prop instanceof ResourcePropertyBoolean) {
			    ResourcePropertyBoolean rpb =
				(ResourcePropertyBoolean) prop;
			    HashMap pernodeMap = rpb.getPernodeValue();

			    if (pernodeMap != null) {
				Iterator i = pernodeMap.keySet().iterator();

				while (i.hasNext()) {
				    String nodename = (String) i.next();
				    Boolean pernodeValue = (Boolean) pernodeMap
					.get(nodename);

				    if (!pernodeValue.equals(
					rpb.getDefaultValue())) {
					commandList.add("-p");

					String str = pernodeValue
					    .booleanValue() ? "TRUE" : "FALSE";
					commandList.add(prop.getName() + "{" +
					    nodename + "}" + "=" + str);
					hasPernodeValue = true;
				    }
				}

				// Go to the beginning of the property loop.
				continue;
			    }

			} else if (prop instanceof ResourcePropertyEnum) {
			    ResourcePropertyEnum rpe = (ResourcePropertyEnum)
				prop;
			    HashMap pernodeMap = rpe.getPernodeValue();

			    if (pernodeMap != null) {
				Iterator i = pernodeMap.keySet().iterator();

				while (i.hasNext()) {
				    String nodename = (String) i.next();
				    String pernodeValue = (String) pernodeMap
					.get(nodename);

				    if (!pernodeValue.equals(
					rpe.getDefaultValue())) {
					commandList.add("-p");
					commandList.add(prop.getName() + "{" +
					    nodename + "}" + "=" +
					    pernodeValue);
					hasPernodeValue = true;
				    }
				}

				// Go to the beginning of the property loop.
				continue;
			    }
			}
		    }

		    // set the extension property for values which are not
		    // per-node and for per-node
		    // values which want same value on all the nodes.
		    if (hasPernodeValue == false) {
			commandList.add("-p");
			commandList.add(prop.getName() + "=" +
			    getPropertyValue(prop));
		    }
		}
	    }
	}

	// Create the resource through the cluster.
	commandList.add(name);

	String[] commandString = (String[]) commandList.toArray(new String[0]);

	InvocationStatus result[] = null;
	ExitStatus resultWrapper[] = null;

	if (commit) {

	    try {
		result = InvokeCommand.execute(new String[][] { commandString },
			    null);
		resultWrapper = ExitStatus.createArray(result);

	} catch (InvocationException ie) {

		try {
		    ie.printStackTrace();
		    result = ie.getInvocationStatusArray();
		    resultWrapper = ExitStatus.createArray(result);
		} catch (Exception e) {
		    e.printStackTrace();
		}

		throw new CommandExecutionException(ie.getMessage(),
			    ExitStatus.createArray(result));
	    }
	} else {
	    resultWrapper = new ExitStatus[] {
		new ExitStatus(ExitStatus.escapeCommand(commandString),
		    ExitStatus.SUCCESS, null, null)
	    };

	    // Read data to refresh the cache
	    readData(zoneClusterName, rgName);
	}

	return resultWrapper;
    }

    public static String getPropertyValue(ResourceProperty property) {

	String value = null;

	if (property instanceof ResourcePropertyString) {
	    ResourcePropertyString propertyString = (ResourcePropertyString)
		property;
	    value = propertyString.getValue();

	} else if (property instanceof ResourcePropertyEnum) {
	    ResourcePropertyEnum propertyEnum = (ResourcePropertyEnum) property;
	    value = propertyEnum.getValue();

	} else if (property instanceof ResourcePropertyInteger) {
	    ResourcePropertyInteger propertyInteger = (ResourcePropertyInteger)
		property;
	    value = propertyInteger.getValue().toString();

	} else if (property instanceof ResourcePropertyBoolean) {
	    ResourcePropertyBoolean propertyBoolean = (ResourcePropertyBoolean)
		property;
	    value =
                   propertyBoolean.getValue().booleanValue() ? "TRUE" : "FALSE";

	} else if (property instanceof ResourcePropertyStringArray) {
	    ResourcePropertyStringArray propertyStringArray =
		(ResourcePropertyStringArray) property;

	    String array[] = propertyStringArray.getValue();

	    value = "";

	    if ((array != null) && (array.length > 0)) {

		if (array[0] != null) {
		    value = array[0];
		}

		for (int j = 1; j < array.length; j++) {
		    value = value.concat("," + array[j]);
		}
	    }
	}

	return value;
    }

    /**
     * This method checks if there is already a resource by the specified name.
     * @param name the name of the resource.
     * @param rgName the name of the resource group.
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @return a boolean; true if the resource name already exists and false
     * otherwise
     */
    public boolean isResourceRegistered(String name, String rgName,
            String zoneClusterName) {
	HashMap rgrDataMap = readData(zoneClusterName, rgName);

	Set keySet = rgrDataMap.keySet();
	Iterator iterator = keySet.iterator();
	boolean found = false;

	while (iterator.hasNext() && !found) {
	    String key = (String) iterator.next();
	    HashMap rDataMap = (HashMap) rgrDataMap.get(key);

	    if (rDataMap.get(name) != null) {
		found = true;
	    }
	}
	return found;
    }

    /**
     * Change some of the resource's system properties.
     *
     * @param name the name of the resource
     * @param  newSystemProperties  a <code>List</code> value containing keys
     * with instances of
     * {@link com.sun.cluster.agent.rgm.property.ResourceProperty}.
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeSystemProperties(String name,
            List newSystemProperties, String zoneClusterName, boolean commit)
        throws CommandExecutionException {

	// Used for per-node extension properties.
	commandList = new ArrayList();
	Iterator props = newSystemProperties.iterator();
	String option = "-p";

	int i = 0;
	commandList.add(ClusterPaths.CL_RES_CMD);
	commandList.add("set");

	while (props.hasNext()) {
	    ResourceProperty prop = (ResourceProperty) props.next();

	    // Set the per-node extension property, for properties
	    // which have been set per-node values.

	    boolean hasPernodeValue = false;

	    if (prop.isPernode()) {

		if (prop instanceof ResourcePropertyString) {

		    ResourcePropertyString rps = (ResourcePropertyString) prop;
		    HashMap pernodeMap = rps.getPernodeValue();
		    String pernodeDefValue = "";

		    if (pernodeMap != null) {
			Iterator it = pernodeMap.keySet().iterator();
			HashMap pernodeDefValueMap =
                                rps.retrievePernodeValueMap();

			while (it.hasNext()) {
			    String nodename = (String) it.next();
			    String pernodeValue = (String) pernodeMap
				.get(nodename);

			    if (pernodeDefValueMap != null) {
				pernodeDefValue = (String)
				    pernodeDefValueMap.get(nodename);
			    } else {
				pernodeDefValue = rps.getDefaultValue();
			    }

			    if (!pernodeValue.equals(pernodeDefValue)) {
				commandList.add(option);

				commandList.add(prop.getName() + "{" +
				    nodename + "}" + "=" +
				    pernodeValue);
				hasPernodeValue = true;
			    }
			}

			// Go to the beginning of the property loop.
			continue;

		    }
		} else if (prop instanceof ResourcePropertyInteger) {
		    ResourcePropertyInteger rpi =
                            (ResourcePropertyInteger) prop;
		    HashMap pernodeMap = rpi.getPernodeValue();
		    Integer pernodeDefValue;

		    if (pernodeMap != null) {
			Iterator it = pernodeMap.keySet().iterator();
			HashMap pernodeDefValueMap =
                                rpi.retrievePernodeValueMap();

			while (it.hasNext()) {
			    String nodename = (String) it.next();
			    Integer pernodeValue = (Integer) pernodeMap
				.get(nodename);

			    if (pernodeDefValueMap != null) {
				pernodeDefValue = (Integer)
				    pernodeDefValueMap.get(nodename);
			    } else {
				pernodeDefValue = rpi.getDefaultValue();
			    }

			    if (!pernodeValue.equals(pernodeDefValue)) {
				commandList.add(option);
				commandList.add(prop.getName() + "{" +
				    nodename + "}" + "=" +
				    pernodeValue);
				hasPernodeValue = true;
			    }
			}

			// Go to the beginning of the property loop.
			continue;
		    }
		} else if (prop instanceof ResourcePropertyBoolean) {
		    ResourcePropertyBoolean rpb =
                            (ResourcePropertyBoolean) prop;
		    HashMap pernodeMap = rpb.getPernodeValue();
		    Boolean pernodeDefValue;

		    if (pernodeMap != null) {
			Iterator it = pernodeMap.keySet().iterator();
			HashMap pernodeDefValueMap =
                                rpb.retrievePernodeValueMap();

			while (it.hasNext()) {
			    String nodename = (String) it.next();
			    Boolean pernodeValue = (Boolean) pernodeMap
				.get(nodename);

			    if (pernodeDefValueMap != null) {
				pernodeDefValue = (Boolean)
				    pernodeDefValueMap.get(nodename);
			    } else {
				pernodeDefValue = rpb.getDefaultValue();
			    }

			    if (!pernodeValue.equals(pernodeDefValue)) {
				String str = pernodeValue
				    .booleanValue() ? "TRUE" : "FALSE";
				commandList.add(option);

				commandList.add(prop.getName() + "{" +
				    nodename + "}" + "=" + str);
				hasPernodeValue = true;
			    }
			}
			// Go to the beginning of the property loop.
			continue;
		    }
		} else if (prop instanceof ResourcePropertyEnum) {
		    ResourcePropertyEnum rpe = (ResourcePropertyEnum) prop;
		    HashMap pernodeMap = rpe.getPernodeValue();
		    String pernodeDefValue = "";

		    if (pernodeMap != null) {
			Iterator it = pernodeMap.keySet().iterator();
			HashMap pernodeDefValueMap =
                                rpe.retrievePernodeValueMap();

			while (it.hasNext()) {
			    String nodename = (String) it.next();
			    String pernodeValue = (String) pernodeMap
				.get(nodename);

			    if (pernodeDefValueMap != null) {
				pernodeDefValue = (String)
				pernodeDefValueMap.get(nodename);
			    } else {
				pernodeDefValue = rpe.getDefaultValue();
			    }

			    if (!pernodeValue.equals(pernodeDefValue)) {
				commandList.add(option);
				commandList.add(prop.getName() + "{" +
				    nodename + "}" + "=" +
				    pernodeValue);
				hasPernodeValue = true;
			    }
			}

			// Go to the beginning of the property loop.
			continue;
		    }
		}
	    }

	    // set the extension property for values which are not
	    // per-node and for per-node
	    // values which want same value on all the nodes.
	    if (hasPernodeValue == false) {
		commandList.add(option);
		commandList.add(prop.getName() + "=" + getPropertyValue(prop));
	    }
	}

	if (zoneClusterName != null && zoneClusterName.trim().length() > 0) {
	    commandList.add(ClusterPaths.ZC_OPTION);
	    commandList.add(zoneClusterName);
	}

	commandList.add(name);

	return invokeCommand(commandList, zoneClusterName, commit);
    }

    private ExitStatus[] invokeCommand(List commandList, String zoneClusterName,
            boolean commit)
	throws CommandExecutionException {
        InvocationStatus result[] = null;
        ExitStatus resultWrapper[] = null;

	String[][] commands = new String[][] {
	    (String[]) commandList.toArray(new String[0])
	};

	String[] commandString = (String[]) commandList.toArray(new String[0]);

	if (commit) {
	    try {
		result = InvokeCommand.execute(commands, null);
		    resultWrapper = ExitStatus.createArray(result);

	    } catch (InvocationException ie) {
		try {
		    ie.printStackTrace();
		    result = ie.getInvocationStatusArray();
		    resultWrapper = ExitStatus.createArray(result);
		} catch (Exception e) {
		    e.printStackTrace();
		}

		throw new CommandExecutionException(ie.getMessage(),
			    ExitStatus.createArray(result));
	    }
            // Read data to refresh the cache
            readData(zoneClusterName, null);
        } else {
            resultWrapper = new ExitStatus[] {
                new ExitStatus(ExitStatus.escapeCommand(commandString),
                    ExitStatus.SUCCESS, null, null)
            };

        }

        return resultWrapper;
    }

    /**
     * Change some of the resource's extend properties.
     *
     * @param name the name of the resource
     * @param  newExtProperties  a <code>List</code> value containing keys with
     * instances of {@link com.sun.cluster.agent.rgm.property.ResourceProperty}.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     * @param zoneClusterName the name of the zone cluster or null for
     * current cluster scope.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeExtProperties(String name, List newExtProperties,
        String zoneClusterName, boolean commit)
        throws CommandExecutionException {
	return changeSystemProperties(
                name, newExtProperties, zoneClusterName, commit);
    }

    /**
     * Change the resource's strong dependencies to other resources.
     *
     * @param name the name of the resource
     * @param  newStrongDependencies  the new dependencies.
     * @param zoneClusterName the name of the zone cluster or null for
     * current cluster scope.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeStrongDependencies(
            String name, String newStrongDependencies[],
	String zoneClusterName, boolean commit)
        throws CommandExecutionException  {

	String list = "";
	if (newStrongDependencies.length > 0) {
	    list = new String(newStrongDependencies[0]);

	    for (int i = 1; i < newStrongDependencies.length; i++) {
		list = list.concat("," + newStrongDependencies[i]);
	    }
	}

	commandList = new ArrayList();
	commandList.add(ClusterPaths.CL_RES_CMD);
	commandList.add("set");
	commandList.add("-p");
	commandList.add(ResourceMBean.STRONG_DEPENDENCIES + "="+ list);

	if (zoneClusterName != null && zoneClusterName.trim().length() > 0) {
	    commandList.add(ClusterPaths.ZC_OPTION);
	    commandList.add(zoneClusterName);
	}

	commandList.add(name);

	return invokeCommand(commandList, zoneClusterName, commit);

    }

    /**
     * Disable the resource.
     *
     * @param name the name of the resource
     * @param zoneClusterName the name of the zone cluster or null for
     * current cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] disable(String name, String zoneClusterName)
	throws CommandExecutionException {
	commandList = constructCommand(name, zoneClusterName, "disable");
	return invokeCommand(commandList, zoneClusterName, true);
    }

    /**
     * Enable the resource.
     *
     * @param name the name of the resource
     * @param zoneClusterName the name of the zone cluster or null for
     * current cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] enable(String name, String zoneClusterName)
	throws CommandExecutionException {
	commandList = constructCommand(name, zoneClusterName, "enable");
	return invokeCommand(commandList, zoneClusterName, true);
    }

    /**
     * Delete this resource.
     *
     * @param name the name of the resource
     * @param zoneClusterName the name of the zone cluster or null for
     * current cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] remove(String name, String zoneClusterName)
	throws CommandExecutionException {
	commandList = constructCommand(name, zoneClusterName, "delete");
	return invokeCommand(commandList, zoneClusterName, true);
    }

    /**
     * Clear stop failed flag for the resource specified by 'name' present in
     * zonecluster specified by zoneClusterName.
     * @param name
     * @param zoneClusterName the name of the zone cluster or null for
     * current cluster scope.
     * @return  an {@link ExitStatus[]} object.
     * @throws com.sun.cluster.agent.auth.CommandExecutionException
     */
    public ExitStatus[] clearStopFailedFlag(String name, String zoneClusterName)
            throws CommandExecutionException {
        // First, get the RGM State of the resource on each node
        ResourceData rData = getResourceData(name, zoneClusterName);
        ResourceRgmState states[] = (ResourceRgmState[]) rData.getRgmStates();

        StringBuffer nodes = new StringBuffer();
        for (int i = 0; i < states.length; i++) {
            if (states[i].getState().equals(
                    ResourceRgmStateEnum.STOP_FAILED)) {
                nodes.append(states[i].getNodeName() + ",");
            }
        }

        commandList = new ArrayList();
        commandList.add(ClusterPaths.CL_RES_CMD);
        commandList.add("clear");
        commandList.add("-n");
        commandList.add(nodes.substring(0, nodes.length() - 1));
        commandList.add("-f");
        commandList.add("STOP_FAILED");

        if (zoneClusterName != null &&
	    zoneClusterName.trim().length() > 0) {
	    commandList.add(ClusterPaths.ZC_OPTION);
	    commandList.add(zoneClusterName);
	}

        commandList.add(name);
        return invokeCommand(commandList, zoneClusterName, true);
    }



    private MBeanServerConnection getMBeanServerConnection(String nodeName) {

        MBeanServerConnection mbsConnection = null;
        try {
            // first get the local connection
            HashMap env = new HashMap();
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                this.getClass().getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());

            JMXConnector connectors = TrustedMBeanModel.getWellKnownConnector(
                    nodeName, env);

            // Now get a reference to the mbean server connection
            mbsConnection = connectors.getMBeanServerConnection();

        } catch (Exception ex) {
            mbsConnection = null;
        }
        return mbsConnection;
    }



    private ArrayList constructCommand(String name, String zoneClusterName,
            String cmdOption) {
	ArrayList cmdList = new ArrayList();
	cmdList.add(ClusterPaths.CL_RES_CMD);
	cmdList.add(cmdOption);

	if (zoneClusterName != null &&
	    zoneClusterName.trim().length() > 0) {
	    cmdList.add(ClusterPaths.ZC_OPTION);
	    cmdList.add(zoneClusterName);
	}

	cmdList.add(name);
	return cmdList;
    }


    /**
     *
     * @param args  -n <n>	repeat test <n> times
     *		    -q		quiet mode
     *		    -z		zonecluster name
     *		    -r		resource group name
     *
     * @return boolean status of readData method
     */
    public static boolean test(String[] args) {
        RGroup rGroup = new RGroup();

        int iterations = 1;
	String zcNameQ = null;
	String rgNameQ = null;

        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-q")) {
                quiet = true;
            } else if (args[i].equals("-n")) {
                iterations = Integer.parseInt(args[++i]);
            } else if (args[i].equals("-z")) {
                zcNameQ = args[++i];
            } else if (args[i].equals("-r")) {
                rgNameQ = args[++i];
            }
        }

        try {
            // Load the agent JNI library
            Runtime.getRuntime().load(lib);
        } catch (UnsatisfiedLinkError ule) {
            print("load of " + lib + " failed!");
            ule.printStackTrace();
            return false;
        }

        long start = System.currentTimeMillis();
        long loopstart;
        long loopelapsed;
        int count = 0;
        print("\nReading data " + iterations + " time(s) ... ");
        for (int i = 0; i < iterations; i++) {
            try {
                loopstart = System.currentTimeMillis();
                // Get the HashMap containing RG-R Data
		HashMap rg_r_dataMap;
                rg_r_dataMap = rGroup.readData(zcNameQ, rgNameQ);
                if (rg_r_dataMap == null) {
                    print("readData returned null!");
                    return false;
                }
                count = i + 1;

                // Set of RG Names
                Set rgNameSet = rg_r_dataMap.keySet();
                print("\nREAD " + count + " : returned " +
                        rgNameSet.size() +
                        " Resource Groups with resource data.");
                if (!quiet) {
                    Iterator it1 = rgNameSet.iterator();
                    while (it1.hasNext()) {
                        String rgName = (String) it1.next();
                        print("\nFor Resource Group :" + rgName);
                        // Get the HashMap containg R Data
                        HashMap r_data_Map = (HashMap)rg_r_dataMap.get(rgName);
                        Iterator it2 = r_data_Map.keySet().iterator();
                        while (it2.hasNext()) {
                            String rName = (String) it2.next();
                            print("\tResource : " +
                                    (ResourceData)r_data_Map.get(rName));
                        }
                    }
                }
                loopelapsed = System.currentTimeMillis() - loopstart;
                print("time = " + loopelapsed + " ms");
            } catch (Exception e) {
                print("readDataJNI failed!");
                print(e.getMessage());
                e.printStackTrace();
                return false;
            }
        }
        quiet = false;
        long elapsed = System.currentTimeMillis() - start;
        print("\nTotal Time for " + iterations + " iterations = " +
                        elapsed + " ms");

        return true;
    }

    public static void print(String msg) {
        System.out.println(msg);
        logger.fine(msg);
    }
}
