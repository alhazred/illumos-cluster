/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceData.java	1.4	08/07/10 SMI"
 */
package com.sun.cluster.agent.rgm;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * ResourceData class
 */
public class ResourceData implements java.io.Serializable {

    private String name;
    private ResourceRgmStateEnum overallState;
    private ResourceRgmState[] rgmStates;
    private ResourceFaultMonitorStatus[] faultMonitorStatus;
    private String type;
    private String typeVersion;
    private String description;
    private String resourceGroupName;
    private int enabled;
    private int monitored;
    private HashMap pernodeEnabledStatus;
    private HashMap pernodeMonitoredStatus;
    private boolean detached;
    private String projectName;
    private String[] weakDependencies;
    private String[] strongDependencies;
    private String[] restartDependencies;
    private String[] offlineDependencies;
    private ArrayList systemProperties;
    private ArrayList extProperties;
    private ArrayList pernodeExtProperties;


    public String getDescription() {
        return description;
    }

    public int getEnabled() {
        return enabled;
    }

    public ArrayList getExtProperties() {
        return extProperties;
    }

    public ResourceFaultMonitorStatus[] getFaultMonitorStatus() {
        return faultMonitorStatus;
    }

    public boolean isDetached() {
        return detached;
    }

    public int getMonitored() {
        return monitored;
    }

    public String getName() {
        return name;
    }

    public String[] getOfflineDependencies() {
        return offlineDependencies;
    }

    public ResourceRgmStateEnum getOverallState() {
        return overallState;
    }

    public HashMap getPernodeEnabledStatus() {
        return pernodeEnabledStatus;
    }

    public ArrayList getPernodeExtProperties() {
        return pernodeExtProperties;
    }

    public HashMap getPernodeMonitoredStatus() {
        return pernodeMonitoredStatus;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getResourceGroupName() {
        return resourceGroupName;
    }

    public String[] getRestartDependencies() {
        return restartDependencies;
    }

    public ResourceRgmState[] getRgmStates() {
        return rgmStates;
    }

    public String[] getStrongDependencies() {
        return strongDependencies;
    }

    public ArrayList getSystemProperties() {
        return systemProperties;
    }

    public String getType() {
        return type;
    }

    public String getTypeVersion() {
        return typeVersion;
    }

    public String[] getWeakDependencies() {
        return weakDependencies;
    }

    public ResourceData() {
    }

    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("[name= " + name);
        s.append(", type= " + type);
        s.append(", description= " + description);
        s.append(", resourceGroupName= " + resourceGroupName);
        s.append(", enabled= " + enabled);
        s.append(", monitored= " + monitored);
        s.append(", weakDependencies= " + convertStringAr(weakDependencies));
        s.append(", strongDependencies= " +
                convertStringAr(strongDependencies));
        s.append(", restartDependencies= " +
                convertStringAr(restartDependencies));
        s.append(", offlineDependencies= " +
                convertStringAr(offlineDependencies));
        s.append("]");
        return s.toString();
    }


    // This utility method would go in ModelBase class once it gets created in
    // the main gate.
    public static String convertStringAr(String[] inputList) {
        String delimiter = ", ";
        StringBuffer tmpStr = new StringBuffer();
        tmpStr.append("[");
        if (inputList != null) {
            for (int i = 0; i < inputList.length; i++) {
                if (i > 0) {
                    tmpStr.append(delimiter);
                }
                tmpStr.append(inputList[i]);
            }
        }
        tmpStr.append("]");
        return tmpStr.toString();
    }
}
