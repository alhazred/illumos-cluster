/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RgmManager.java	1.25	08/09/22 SMI"
 */

package com.sun.cluster.agent.rgm;

// JDK
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;

// JMX
import javax.management.MBeanServer;
import javax.management.ObjectName;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyEnum;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.common.ClusterPaths;


/**
 * This class implements the {@link RgmManagerMBean} interface. This MBean is
 * created and register into the CMAS agent in the same way as the
 * {@link RgmModule} MBean.
 */
public class RgmManager implements RgmManagerMBean {

    /* The MBeanServer in which this MBean is inserted. */
    private MBeanServer mBeanServer;

    // The object name factory used to register this MBean
    private ObjectNameFactory onf = null;

    /**
     * The interceptor objectsb below are used by Odyssey to invalidate the
     * cache. This is not a good approach, must be replaced with a event that
     * says, "my cache has changed". Odyssey can listen to this event and
     * perform actions based on it.
     */
    private ResourceInterceptor resourceInterceptor = null;
    private ResourceGroupInterceptor resourceGroupInterceptor = null;

    /**
     * Default constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this MBean.
     */
    public RgmManager(MBeanServer mBeanServer, ObjectNameFactory onf) {
        this.mBeanServer = mBeanServer;
        this.onf = onf;
    }

    /**
     * @deprecated use RgGroupMBean.addResourceGroup().
     */
    public ExitStatus[] addResourceGroup(String name, Map properties,
        boolean commit) throws CommandExecutionException {

        // Verify that the resource group name is not already used by another
        // resource group.
        if (!commit) {
            ObjectName rgObjectName = onf.getObjectName(
                    ResourceGroupMBean.class, name);

            if (mBeanServer.isRegistered(rgObjectName)) {
                throw new IllegalArgumentException("Resource Group " + name +
                    " already registered.");
            }
        }

        String command[] = null;

        if (properties != null) {
            command = new String[3 + (properties.size() * 2)];
        } else {
            command = new String[3];
        }

        int commandIndex = 0;
        command[commandIndex++] = ClusterPaths.CL_RG_CMD;
        command[commandIndex++] = "create";

        if (properties != null) {
            Iterator propertyNames = properties.keySet().iterator();

            while (propertyNames.hasNext()) {
                String propertyName = (String) propertyNames.next();
                command[commandIndex++] = "-p";
                command[commandIndex++] = propertyName + "=" +
                    (String) properties.get(propertyName);
            }
        }

        command[commandIndex++] = name;

        InvocationStatus result[] = null;
        ExitStatus resultWrapper[] = null;

        if (commit) {

            try {
                result = InvokeCommand.execute(new String[][] { command },
                        null);
                resultWrapper = ExitStatus.createArray(result);
            } catch (InvocationException ie) {
                result = ie.getInvocationStatusArray();
                throw new CommandExecutionException(ie.getMessage(),
                    ExitStatus.createArray(result));
            }
        } else {
            resultWrapper = new ExitStatus[] {
                    new ExitStatus(ExitStatus.escapeCommand(command),
                        ExitStatus.SUCCESS, null, null)
                };
        }

        return resultWrapper;
    }

    /**
     * @deprecated use RGroupMBean.addResource() instead
     */
    public ExitStatus[] addResource(String name, String rgName, String type,
        List systemProperties, List extProperties, boolean commit)
        throws CommandExecutionException {

        LinkedList commandList = new LinkedList();
        String commands[][] = null;

        // Verify that the resource name is not already used by another
        // resource.
        if (!commit) {
            ObjectName rObjectName = onf.getObjectName(ResourceMBean.class,
                    name);

            if (mBeanServer.isRegistered(rObjectName)) {
                throw new IllegalArgumentException("Resource " + name +
                    " already registered.");
            }
        }

        int j = 0;
        String clCommand = null;

        // Using boolean so as to avoid repetitive string checking
        boolean isLogHostType = false;
        boolean isSharedAddType = false;

        if (type.equals("SUNW.LogicalHostname") ||
                type.startsWith("SUNW.LogicalHostname:")) {

            clCommand = ClusterPaths.CL_RES_LH_CMD;
            isLogHostType = true;
        } else if (type.equals("SUNW.SharedAddress") ||
                type.startsWith("SUNW.SharedAddress:")) {

            clCommand = ClusterPaths.CL_RES_SA_CMD;
            isSharedAddType = true;
        } else {

            clCommand = ClusterPaths.CL_RES_CMD;
        }

        commandList.add(clCommand);
        commandList.add("create");

        // Set special flag for LogicalHostname or SharedAddress
        if (!isLogHostType && !isSharedAddType) {
            commandList.add("-t");
            commandList.add(type);
        }

        int commandIndex = 2;

        commandList.add("-g");
        commandList.add(rgName);

        // Parse system properties
        if ((systemProperties != null) && (systemProperties.size() != 0)) {

            // Use a hashmap to remove duplicate properties
            HashMap sysPropMap = new HashMap();
            Iterator props = systemProperties.iterator();
            String propName = null;

            while (props.hasNext()) {
                ResourceProperty prop = (ResourceProperty) props.next();
                propName = prop.getName();

                String currPropVal = getPropertyValue(prop);
                String prevPropVal = (String) sysPropMap.get(propName);

                if (prevPropVal != null) {
                    StringBuffer newVal = new StringBuffer();
                    newVal.append(prevPropVal);
                    newVal.append(",");
                    newVal.append(currPropVal);
                    sysPropMap.put(propName, newVal.toString());
                } else {
                    sysPropMap.put(propName, currPropVal);
                }
            }

            Iterator mapInter = sysPropMap.keySet().iterator();

            while (mapInter.hasNext()) {
                propName = (String) mapInter.next();

                String propVal = (String) sysPropMap.get(propName);
                commandList.add("-p");
                commandList.add(propName + "=" + propVal);
            }
        }

        // Parse extended properties
        if ((extProperties != null) && (extProperties.size() != 0)) {
            Iterator props = extProperties.iterator();

            while (props.hasNext()) {
                ResourceProperty prop = (ResourceProperty) props.next();

                if (prop.getName().equals("HostnameList")) {
                    commandList.add("-h");
                    commandList.add(getPropertyValue(prop));
                } else if (prop.getName().equals("NetIfList")) {
                    commandList.add("-N");
                    commandList.add(getPropertyValue(prop));
                } else if (prop.getName().equals("AuxNodeList")) {
                    commandList.add("-X");
                    commandList.add(getPropertyValue(prop));
                } else {
                    boolean hasPernodeValue = false;

                    // Set the per-node extension property, for properties
                    // which have been set per-node values.
                    if (prop.isPernode()) {

                        if (prop instanceof ResourcePropertyString) {
                            ResourcePropertyString rps =
                                (ResourcePropertyString) prop;
                            HashMap pernodeMap = rps.getPernodeValue();

                            if (pernodeMap != null) {
                                Iterator i = pernodeMap.keySet().iterator();

                                while (i.hasNext()) {
                                    String nodename = (String) i.next();
                                    String pernodeValue = (String) pernodeMap
                                        .get(nodename);

                                    if (
                                        !pernodeValue.equals(
                                                rps.getDefaultValue())) {
                                        commandList.add("-p");
                                        commandList.add(prop.getName() + "{" +
                                            nodename + "}" + "=" +
                                            pernodeValue);
                                        hasPernodeValue = true;
                                    }
                                }

                                // Go to the beginning of the property loop.
                                continue;
                            }
                        } else if (prop instanceof ResourcePropertyInteger) {
                            ResourcePropertyInteger rpi =
                                (ResourcePropertyInteger) prop;
                            HashMap pernodeMap = rpi.getPernodeValue();

                            if (pernodeMap != null) {
                                Iterator i = pernodeMap.keySet().iterator();

                                while (i.hasNext()) {
                                    String nodename = (String) i.next();
                                    Integer pernodeValue = (Integer) pernodeMap
                                        .get(nodename);

                                    if (
                                        !pernodeValue.equals(
                                                rpi.getDefaultValue())) {
                                        commandList.add("-p");
                                        commandList.add(prop.getName() + "{" +
                                            nodename + "}" + "=" +
                                            pernodeValue.toString());
                                        hasPernodeValue = true;
                                    }
                                }

                                // Go to the beginning of the property loop.
                                continue;
                            }
                        } else if (prop instanceof ResourcePropertyBoolean) {
                            ResourcePropertyBoolean rpb =
                                (ResourcePropertyBoolean) prop;
                            HashMap pernodeMap = rpb.getPernodeValue();

                            if (pernodeMap != null) {
                                Iterator i = pernodeMap.keySet().iterator();

                                while (i.hasNext()) {
                                    String nodename = (String) i.next();
                                    Boolean pernodeValue = (Boolean) pernodeMap
                                        .get(nodename);

                                    if (
                                        !pernodeValue.equals(
                                                rpb.getDefaultValue())) {
                                        commandList.add("-p");

                                        String str = pernodeValue
                                            .booleanValue() ? "TRUE" : "FALSE";
                                        commandList.add(prop.getName() + "{" +
                                            nodename + "}" + "=" + str);
                                        hasPernodeValue = true;
                                    }
                                }

                                // Go to the beginning of the property loop.
                                continue;
                            }
                        } else if (prop instanceof ResourcePropertyEnum) {
                            ResourcePropertyEnum rpe = (ResourcePropertyEnum)
                                prop;
                            HashMap pernodeMap = rpe.getPernodeValue();

                            if (pernodeMap != null) {
                                Iterator i = pernodeMap.keySet().iterator();

                                while (i.hasNext()) {
                                    String nodename = (String) i.next();
                                    String pernodeValue = (String) pernodeMap
                                        .get(nodename);

                                    if (
                                        !pernodeValue.equals(
                                                rpe.getDefaultValue())) {
                                        commandList.add("-p");
                                        commandList.add(prop.getName() + "{" +
                                            nodename + "}" + "=" +
                                            pernodeValue);
                                        hasPernodeValue = true;
                                    }
                                }

                                // Go to the beginning of the property loop.
                                continue;
                            }
                        }
                    }

                    // set the extension property for values which are not
                    // per-node and for per-node
                    // values which want same value on all the nodes.
                    if (hasPernodeValue == false) {
                        commandList.add("-p");
                        commandList.add(prop.getName() + "=" +
                            getPropertyValue(prop));
                    }
                }
            }
        }

        // Create the resource through the cluster.
        // The associated virtual MBean and notifications will be handled by the
        // ResourceInterceptor MBean and the Cluster event listener.
        commandList.add(name);

        int commandLen = commandList.size();
        String commandString[] = new String[commandLen];
        commandString = (String[]) commandList.toArray(commandString);

        InvocationStatus result[] = null;
        ExitStatus resultWrapper[] = null;

        if (commit) {

            try {
                result = InvokeCommand.execute(new String[][] { commandString },
                        null);
                resultWrapper = ExitStatus.createArray(result);
            } catch (InvocationException ie) {

                try {
                    ie.printStackTrace();
                    result = ie.getInvocationStatusArray();
                    resultWrapper = ExitStatus.createArray(result);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                throw new CommandExecutionException(ie.getMessage(),
                    ExitStatus.createArray(result));
            }
        } else {
            resultWrapper = new ExitStatus[] {
                    new ExitStatus(ExitStatus.escapeCommand(commandString),
                        ExitStatus.SUCCESS, null, null)
                };
        }

        return resultWrapper;
    }

    public static String getPropertyValue(ResourceProperty property) {

        String value = null;

        if (property instanceof ResourcePropertyString) {
            ResourcePropertyString propertyString = (ResourcePropertyString)
                property;
            value = propertyString.getValue();

        } else if (property instanceof ResourcePropertyEnum) {
            ResourcePropertyEnum propertyEnum = (ResourcePropertyEnum) property;
            value = propertyEnum.getValue();

        } else if (property instanceof ResourcePropertyInteger) {
            ResourcePropertyInteger propertyInteger = (ResourcePropertyInteger)
                property;
            value = propertyInteger.getValue().toString();

        } else if (property instanceof ResourcePropertyBoolean) {
            ResourcePropertyBoolean propertyBoolean = (ResourcePropertyBoolean)
                property;
            value = propertyBoolean.getValue().booleanValue() ? "TRUE"
                                                              : "FALSE";

        } else if (property instanceof ResourcePropertyStringArray) {
            ResourcePropertyStringArray propertyStringArray =
                (ResourcePropertyStringArray) property;

            String array[] = propertyStringArray.getValue();
            value = "";

            if ((array != null) && (array.length > 0)) {

                if (array[0] != null)
                    value = array[0];

                for (int j = 1; j < array.length; j++) {
                    value = value.concat("," + array[j]);
                }
            }
        }

        return value;
    }

    /**
     * This method register a new resource type so that it can be used by the
     * user.
     *
     * @param  name  the name of the reosurce type to register.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] registerResourceType(String name)
        throws CommandExecutionException {
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(
                    new String[][] {
                        { ClusterPaths.CL_RT_CMD, "register", name }
                    }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * This method gets all project names on the current node of the given
     * zonecluster. Current node is the node on which this mbean is running.
     *
     * @return  an {@link Set} object.
     */
    public Set getProjectNames(String zoneClusterName) {
	Set retVal = new HashSet();
	if (zoneClusterName == null) {
	    retVal = getProjectNames();
	} else {

	    // Try getting the project info by logging into the zc

	    String[][] cmds = new String[][] {
		{
		    ClusterPaths.ZLOGIN_CMD, zoneClusterName,
		    ClusterPaths.ZC_GETPROJECTNAMES_CMD
		}
	    };

	    InvocationStatus exits[] = null;
	    try {
		exits = InvokeCommand.execute(cmds, null);
		String projects = exits[0].getStdout();
		String[] projectsAr = projects.split("\n");
		for (int i = 0; i < projectsAr.length; i++) {
		    retVal.add(projectsAr[i]);
		}
	    } catch (Exception ie) {
		// do nothing
	    }
	}
	return retVal;
    }

    public native Set getProjectNames();

    /**
     * Suspend resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] suspendRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException {
        InvocationStatus exits[];

        if (rgs == null) {
            throw new CommandExecutionException("Invalid Call to suspendRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("suspend");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * Fast suspend resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] fastsuspendRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException {
        InvocationStatus exits[];

        if (rgs == null) {
            throw new CommandExecutionException(
                "Invalid Call to fastsuspendRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("suspend");
            commandList.add("-k");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * Resume suspended resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] resumeRGs(String zoneClusterName, String rgs)
            throws CommandExecutionException {
        InvocationStatus exits[];

        if (rgs == null) {
            throw new CommandExecutionException("Invalid Call to resumeRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("resume");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Remaster resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] remasterRGs(String zoneClusterName, String rgs)
            throws CommandExecutionException {
        InvocationStatus exits[];

        if (rgs == null) {
            throw new CommandExecutionException("Invalid Call to remaster RGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("remaster");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Bring resource groups Online.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOnlineRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException {

        InvocationStatus exits[];

        if (rgs == null) {
            throw new CommandExecutionException(
		"Invalid Call to  bringOnlineRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("online");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);


    }


    /**
     * Bring Online resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeBringOnlineRGs(String zoneClusterName,
            String rgNames, String nodeList)
        throws CommandExecutionException {
	InvocationStatus exits[];

        if (rgNames == null) {
            throw new CommandExecutionException(
		"Invalid Call to  bringOnlineRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("online");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
	    if (!nodeList.equals("")) {
		commandList.add("-n");
                commandList.add(nodeList);
	    }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgNames) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Bring resource groups Offline.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOfflineRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException {

        InvocationStatus exits[];

        if (rgs == null) {
            throw new CommandExecutionException(
		"Invalid Call to bringOfflineRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("offline");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Bring Offline resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeBringOfflineRGs(String zoneClusterName,
            String rgNames, String nodeList)
        throws CommandExecutionException {
	InvocationStatus exits[];

        if (rgNames == null) {
            throw new CommandExecutionException(
		"Invalid Call to bringOfflineRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("offline");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
	    if (!nodeList.equals("")) {
		commandList.add("-n");
                commandList.add(nodeList);
	    }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgNames) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Manage resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] manageRGs(String zoneClusterName, String rgs)
            throws CommandExecutionException {

        InvocationStatus exits[];

        if (rgs == null) {
            throw new CommandExecutionException("Invalid Call to manageRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("manage");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * UnManage resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] unmanageRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException {

        InvocationStatus exits[];

        if (rgs == null) {
            throw new CommandExecutionException("Invalid Call to unmanageRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("unmanage");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Restart resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] restartRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException {

        InvocationStatus exits[];

        if (rgs == null) {
            throw new CommandExecutionException("Invalid Call to restartRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("restart");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgs) }, null);

        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Restart resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeRestartRGs(String zoneClusterName,
            String rgNames, String nodeList)
        throws CommandExecutionException {
	InvocationStatus exits[];

        if (rgNames == null) {
            throw new CommandExecutionException("Invalid Call to restartRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("restart");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
	    if (nodeList != null) {
		commandList.add("-n");
		commandList.add(nodeList);
	    }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgNames) }, null);

        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Force delete resource group.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] forceDeleteRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException {

        InvocationStatus exits[];

        if (rgs == null) {
            throw new CommandExecutionException(
                "Invalid Call to forceDeleteRGs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RG_CMD);
            commandList.add("delete");
            commandList.add("-F");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rgs) }, null);

        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * Enable resources.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] enableRs(String zoneClusterName, String rs)
            throws CommandExecutionException {

        InvocationStatus exits[];

        if (rs == null) {
            throw new CommandExecutionException("Invalid Call to enableRs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RES_CMD);
            commandList.add("enable");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Stop Monitor for resources.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] stopMonitorRs(String zoneClusterName, String rs)
        throws CommandExecutionException {

        InvocationStatus exits[];

        if (rs == null) {
            throw new CommandExecutionException(
		"Invalid Call to stopMonitorRs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RES_CMD);
            commandList.add("unmonitor");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Disable resources.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] disableRs(String zoneClusterName, String rs)
            throws CommandExecutionException {

        InvocationStatus exits[];

        if (rs == null) {
            throw new CommandExecutionException("Invalid Call to disableRs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RES_CMD);
            commandList.add("disable");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Start Monitor for resources.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] startMonitorRs(String zoneClusterName, String rs)
        throws CommandExecutionException {

        InvocationStatus exits[];

        if (rs == null) {
            throw new CommandExecutionException(
		"Invalid Call to startMonitorRs");
        }

        try {
            ArrayList commandList = new ArrayList();
            commandList.add(ClusterPaths.CL_RES_CMD);
            commandList.add("monitor");
            if (zoneClusterName != null &&
                    zoneClusterName.trim().length() > 0) {
                commandList.add(ClusterPaths.ZC_OPTION);
                commandList.add(zoneClusterName);
            }
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rs) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Enable resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeEnableRs(String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException {

        InvocationStatus exits[];

        if (rsName == null) {
            throw new CommandExecutionException(
		"Invalid Call to pernodeEnableRs");
        }

        ArrayList commandList = new ArrayList();
        commandList.add(ClusterPaths.CL_RES_CMD);
        commandList.add("enable");
        if (zoneClusterName != null &&
                zoneClusterName.trim().length() > 0) {
            commandList.add(ClusterPaths.ZC_OPTION);
            commandList.add(zoneClusterName);
        }
        if (nodeList != null) {
            commandList.add("-n");
            commandList.add(nodeList);
        }
        try {
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rsName) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Stop Monitor for resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeStopMonitorRs(String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException {

        InvocationStatus exits[];

        if (rsName == null) {
            throw new CommandExecutionException(
		"Invalid Call to pernodeStopMonitorRs");
        }

        ArrayList commandList = new ArrayList();
        commandList.add(ClusterPaths.CL_RES_CMD);
        commandList.add("unmonitor");
        if (zoneClusterName != null &&
                zoneClusterName.trim().length() > 0) {
            commandList.add(ClusterPaths.ZC_OPTION);
            commandList.add(zoneClusterName);
        }
        if (nodeList != null) {
            commandList.add("-n");
            commandList.add(nodeList);
        }
        try {
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rsName) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Disable resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeDisableRs(String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException {

        InvocationStatus exits[];

        if (rsName == null) {
            throw new CommandExecutionException(
		"Invalid Call to pernodeDisableRs");
        }

        ArrayList commandList = new ArrayList();
        commandList.add(ClusterPaths.CL_RES_CMD);
        commandList.add("disable");
        if (zoneClusterName != null &&
                zoneClusterName.trim().length() > 0) {
            commandList.add(ClusterPaths.ZC_OPTION);
            commandList.add(zoneClusterName);
        }
        if (nodeList != null) {
            commandList.add("-n");
            commandList.add(nodeList);
        }
        try {
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rsName) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Start Monitor for resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeStartMonitorRs(String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException {

        InvocationStatus exits[];

        if (rsName == null) {
            throw new CommandExecutionException(
		"Invalid Call to pernodeStartMonitorRs");
        }

        ArrayList commandList = new ArrayList();
        commandList.add(ClusterPaths.CL_RES_CMD);
        commandList.add("monitor");
        if (zoneClusterName != null &&
                zoneClusterName.trim().length() > 0) {
            commandList.add(ClusterPaths.ZC_OPTION);
            commandList.add(zoneClusterName);
        }
        if (nodeList != null) {
            commandList.add("-n");
            commandList.add(nodeList);
        }
        try {
            String[] cmd = (String[]) commandList.toArray(new String[0]);
            exits = InvokeCommand.execute(
                    new String[][] { appendObjsToCmd(cmd, rsName) }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * Called by Odyssey to invalidate the cache. This is not a good approach,
     * much be replaced with a event that says, "my cache has changed".
     * Odyssey can listen to this event and perform actions based on it.
     */
    public void refreshResourceCache() {

        if (resourceInterceptor != null) {
            resourceInterceptor.refreshCache();
        }
    }

    /**
     * Called by Odyssey to invalidate the cache. This is not a good approach,
     * much be replaced with a event that says, "my cache has changed".
     * Odyssey can listen to this event and perform actions based on it.
     */
    public void refreshResourceGroupCache() {

        if (resourceGroupInterceptor != null) {
            resourceGroupInterceptor.refreshCache();
        }
    }

    public void setResourceInterceptor(ResourceInterceptor resInterceptor) {
        this.resourceInterceptor = resInterceptor;
    }

    public void setResourceGroupInterceptor(
        ResourceGroupInterceptor resGrpInterceptor) {
        this.resourceGroupInterceptor = resGrpInterceptor;
    }

    private String[] appendObjsToCmd(String cmd[], String objs) {

        if ((cmd == null) || (cmd.length < 1)) {
            return null;
        }

        if (objs == null) {
            return cmd;
        }

        String objsArr[] = objs.split(",");
        String cmdArr[] = new String[objsArr.length + cmd.length];

        for (int nCount = 0; nCount < cmd.length; nCount++) {
            cmdArr[nCount] = cmd[nCount];
        }

        for (int nCount = 0; nCount < objsArr.length; nCount++) {
            cmdArr[nCount + cmd.length] = objsArr[nCount];
        }

        return cmdArr;
    }


    /**
     * @deprecated
     */
    public ExitStatus[] suspendRGs(String rgs)
            throws CommandExecutionException {
        return suspendRGs(null, rgs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] fastsuspendRGs(String rgs)
            throws CommandExecutionException {
        return fastsuspendRGs(null, rgs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] resumeRGs(String rgs)
            throws CommandExecutionException {
        return resumeRGs(null, rgs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] restartRGs(String rgs)
            throws CommandExecutionException {
        return restartRGs(null, rgs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] forceDeleteRGs(String rgs)
            throws CommandExecutionException {
        return forceDeleteRGs(null, rgs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] bringOnlineRGs(String rgs)
            throws CommandExecutionException {
        return bringOnlineRGs(null, rgs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] bringOfflineRGs(String rgs)
            throws CommandExecutionException {
        return bringOfflineRGs(null, rgs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] manageRGs(String rgs) throws CommandExecutionException {
        return manageRGs(null, rgs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] unmanageRGs(String rgs)
            throws CommandExecutionException {
        return unmanageRGs(null, rgs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] enableRs(String rs) throws CommandExecutionException {
        return enableRs(null, rs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] disableRs(String rs) throws CommandExecutionException {
        return disableRs(null, rs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] startMonitorRs(String rs)
            throws CommandExecutionException {
        return startMonitorRs(null, rs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] stopMonitorRs(String rs)
            throws CommandExecutionException {
        return stopMonitorRs(null, rs);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] pernodeEnableRs(String rsName, String nodeList)
            throws CommandExecutionException {
        return pernodeEnableRs(null, rsName, nodeList);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] pernodeDisableRs(String rsName, String nodeList)
            throws CommandExecutionException {
        return pernodeDisableRs(null, rsName, nodeList);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] pernodeStartMonitorRs(String rsName, String nodeList)
            throws CommandExecutionException {
        return pernodeStartMonitorRs(null, rsName, nodeList);
    }

    /**
     * @deprecated
     */
    public ExitStatus[] pernodeStopMonitorRs(String rsName, String nodeList)
            throws CommandExecutionException {
        return pernodeStopMonitorRs(null, rsName, nodeList);
    }
}
