/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RtGroup.java	1.9	09/02/10 SMI"
 */

package com.sun.cluster.agent.rgm;

// JDK
import java.io.File;
import java.io.FileFilter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


// JMX
import javax.management.Attribute;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;

import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;
import com.sun.cacao.ObjectNameFactory;

// Local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.ClusterPaths;
import com.sun.cluster.common.CMASUtil;
import com.sun.cluster.common.TrustedMBeanModel;


/**
 * RtGroup class implementing RtGroupMBean. Instance and attribute values
 * are fetched through JNI, and all operations are performed through an
 * invocation of a CLI.
 */
public class RtGroup implements RtGroupMBean, FileFilter {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.rgm");
    private final static String logTag = "RtGroup";
    private static boolean quiet = false;
    private static final String lib = ClusterPaths.cmass +
                                            "libcmas_agent_rgm.so";
    private static final String DOMAIN_RGM = "com.sun.cluster.agent.rgm";

    private MBeanServer mBeanServer = null;

    private String rtrDirectories[] = {
            ClusterPaths.SCRGADM_RTR_DIR, ClusterPaths.SCRGADM_OPT_RTR_DIR
        };
    private int rtrDirectoriesCount = rtrDirectories.length;

    private HashMap zcInstanceSet = new HashMap();
    private HashMap zcCachedDirectoryInstanceSet = new HashMap();
    private HashMap zcCacheTimestampMap = new HashMap();
    private HashMap zcDataMap = new HashMap();
    private String zcName = null;
    private static final String ROOT = "/root";

    /**
     * Default Constructor.
     */
    public RtGroup() {
	this(null);
    }

    public RtGroup(MBeanServer mbs) {
        this.mBeanServer = mbs;
	initZcVariables(null);
    }


    private void initZcVariables(String zoneClusterName) {
        this.zcName = zoneClusterName;
        HashMap cacheTimestampMap = new HashMap();

        HashSet cachedDirectoryInstanceSet[] = new HashSet[rtrDirectoriesCount];

        /* Initialize the cacheTimestamp */
        for (int i = 0; i < rtrDirectoriesCount; i++) {
            cachedDirectoryInstanceSet[i] = new HashSet();
            cacheTimestampMap.put(rtrDirectories[i], new Long(0));
        }

        if (zcInstanceSet.get(zoneClusterName) == null) {
            zcInstanceSet.put(zoneClusterName, new HashSet());
        }
        if (zcCachedDirectoryInstanceSet.get(zoneClusterName) == null) {
            zcCachedDirectoryInstanceSet.put(zoneClusterName,
                                                cachedDirectoryInstanceSet);
        }
        if (zcCacheTimestampMap.get(zoneClusterName) == null) {
            zcCacheTimestampMap.put(zoneClusterName, cacheTimestampMap);
        }
        if (zcDataMap.get(zoneClusterName) == null) {
            zcDataMap.put(zoneClusterName, new HashMap());
        }
    }


    /**
     * Utility method to unregister given rt name
     *
     * @param  rtName  a <code>String</code> value for resource type name to be
     * unregistered
     * @param zoneClusterName
     * @return Exitstatus[]
     * @throws javax.management.MBeanException
     * @throws javax.management.InstanceNotFoundException
     */
    public ExitStatus[] unregisterResourceType(
            String rtName, String zoneClusterName)
            throws InstanceNotFoundException, MBeanException {

        if (!isRegistered(rtName, zoneClusterName)) {
            throw new InstanceNotFoundException("cannot find the instance " +
                rtName);
        }

        List commandList = new ArrayList();
	commandList.add(ClusterPaths.CL_RT_CMD);

        if (zoneClusterName != null) {
            commandList.add(ClusterPaths.ZC_OPTION);
            commandList.add(zoneClusterName);
        }

        commandList.add("unregister");
        commandList.add(rtName);
        String[] command = (String[]) commandList.toArray(new String[0]);

        // Run the command(s)
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(
		    new String[][] {
			command,
		    }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new MBeanException(new CommandExecutionException(
                    ie.getMessage(), ExitStatus.createArray(exits)));
        }
        return ExitStatus.createArray(exits);
    }


    /**
     * This method register a new resource type so that it can be used by the
     * user.
     *
     * @param rType the name of the resource type to register.
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] registerResourceType(String rType,
            String zoneClusterName)
	throws CommandExecutionException {
	return registerResourceType(rType, zoneClusterName, true);
    }

    /**
     * This method register a new resource type so that it can be used by the
     * user.
     *
     * @param rType the name of the resource type to register.
     * @param zoneClusterName the name of the zone cluster
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] registerResourceType(String rType,
            String zoneClusterName, boolean commit)
	throws CommandExecutionException {

	InvocationStatus exits[];

	List commandList = new ArrayList();
	commandList.add(ClusterPaths.CL_RT_CMD);
	commandList.add("register");

	if (zoneClusterName != null && zoneClusterName.trim().length() > 0) {
	    commandList.add(ClusterPaths.ZC_OPTION);
	    commandList.add(zoneClusterName);
	}
	commandList.add(rType);

	String[] command = (String[]) commandList.toArray(new String[0]);

	if (!commit) {

	    ExitStatus resultWrapper[] = null;
	    resultWrapper = new ExitStatus[] {
		new ExitStatus(ExitStatus.escapeCommand(command),
		    ExitStatus.SUCCESS, null, null)
	    };

	    return resultWrapper;
	}

	try {
	    exits = InvokeCommand.execute(
		    new String[][] {
			command,
		    }, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * Utility method to retrieve data corresponding to ResourceTypes.
     *
     * @param zoneClusterName for which the query is to be done. If this is
     *        null, the method would return all the ResourceGroups present in
     *        the cluster it is run.
     * @param qAttribute If this is not null, return rtnames having an attribute
     * matching this one.
     * @return HashMap with following structure
     *                  KEY                     VALUE
     *                  ResourceTypeName       ResourceTypeData Object
     */
    public HashMap readData(String zoneClusterName, Attribute qAttribute) {

	HashMap retMap = new HashMap();
	RtGroupMBean rtGroupMBean = null;

	if (mBeanServer != null && zoneClusterName != null) {
            /**
             * If the given zone cluster is not hosted on this node, we cannot
             * get the related rg information. Hence get connection to cacao to
             * a node which hosts the given zone cluster, and then get the rg
             * information.
             */

            // get the nodelist on which given zonecluster is hosted
            // This nodelist would be empty if the zone cluster is hosted on
            // local host.
            ArrayList nodeList = getNodeList(zoneClusterName);

            if (nodeList != null && nodeList.size() > 0) {

                // Connect to any of the node in the nodelist
                ObjectNameFactory rgGroupOnf = new ObjectNameFactory(
                                                                DOMAIN_RGM);
                ObjectName rtGroupMBeanObjName = rgGroupOnf.getObjectName(
                        RtGroupMBean.class, null);

                Iterator iterNodeList = nodeList.iterator();
                while (iterNodeList.hasNext()) {
                    try {
                        MBeanServerConnection mbs = getMBeanServerConnection(
                                        (String)iterNodeList.next());
                        if (mbs == null) {
                            // unable to connect to a node in the nodelist
                            // try with next
                            continue;
                        }
                        rtGroupMBean = (RtGroupMBean)
                            MBeanServerInvocationHandler.newProxyInstance(
                                        mbs,
                                        rtGroupMBeanObjName,
                                        RtGroupMBean.class, false);
                        if (rtGroupMBean != null) {
                            // Got connection
                            break;
                        }
                    } catch (Exception ex) {
                        // Do nothing
                    }
                }
            }
        }

	if (rtGroupMBean != null) {
            retMap = rtGroupMBean.readData(zoneClusterName, qAttribute);
        } else {
	    initZcVariables(zoneClusterName);

	    String zonePath = (zoneClusterName == null) ? "" :
					    getZonePath(zoneClusterName);

	    HashMap dataMap = (HashMap)zcDataMap.get(zoneClusterName);

	    if ((dataMap.isEmpty()) || (!isCacheValid(zoneClusterName))) {
		dataMap = (HashMap) fillCacheNative(
				appendZonePath(rtrDirectories, zonePath));
		zcDataMap.put(zoneClusterName, dataMap);
	    }

	    // Traverse the datamap and prepare result to be returned
	    Set names = dataMap.keySet();
	    Iterator iterNames = names.iterator();
	    while (iterNames.hasNext()) {
		String name = (String) iterNames.next();
		Map instanceMap = (HashMap) dataMap.get(name);

		/**
		 *  "Registered" and "InstalledNodeNames" cannot be cached,
		 *  because there is no cluster event generated when these
		 *  properties change. These two values will be calculated
		 *  whenever a client asks for it
		 */
		instanceMap.put("Registered", getAttributeNative(
			name, "Registered", zoneClusterName));
		instanceMap.put("InstalledNodeNames", getAttributeNative(
			name, "InstalledNodeNames", zoneClusterName));

		// Check for given attribute
		if (qAttribute != null) {
		    // Return data for all those RT who have an attribute which
		    // is equal to qAttribute.
		    String attrName = qAttribute.getName();

		    Attribute tmpAttribute = (Attribute)instanceMap.get(
			attrName);
		    if (attrName.equals("Name")) {
			// Handle name attribute separately since there could
			// be a version no. there
			String attrValue = (String)qAttribute.getValue();
			String tmpAttrValue = (String)tmpAttribute.getValue();

			// the version nos. are always after a colon
			String[] tmpAttrSplitStr = tmpAttrValue.split(
							    CMASUtil.COLON);
			String[] attrSplitStr = attrValue.split(CMASUtil.COLON);

			if (tmpAttrSplitStr[0].equals(attrSplitStr[0])) {
			    retMap.put(name, getResourceTypeData(instanceMap));
			}
		    } else {
			retMap.put(name, getResourceTypeData(instanceMap));
		    }
		} else {
		    retMap.put(name, getResourceTypeData(instanceMap));
		}
	    }
	}

        return retMap;
    }


    /**
     * Method used internally for filtering files based upon modification date
     */
    public boolean accept(File pathname) {

        HashMap cacheTimestampMap = (HashMap)
                zcCacheTimestampMap.get(zcName);
        HashSet instanceSet = (HashSet) zcInstanceSet.get(zcName);

        // If we've already found a more recent file, short-cut
        if (instanceSet == null)
            return false;

        try {
            long lastModified = pathname.lastModified();
            String rtrDirPath = pathname.getParent();
            long cacheTimestamp = ((Long) cacheTimestampMap.get(rtrDirPath))
                .longValue();

            if (lastModified > cacheTimestamp) {
                cacheTimestamp = lastModified;
                cacheTimestampMap.put(rtrDirPath, new Long(cacheTimestamp));

                return true;
            }
            zcCacheTimestampMap.put(zcName, cacheTimestampMap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }



    /**
     * Function called by the isRegistered function for checking whether an
     * MBean specified by its instance name is already registered with the MBean
     * server. This is done in the sub-class as it can be specific to the MBean
     * type.
     *
     * @param  name  The name of the MBean to be checked.
     *
     * @param zoneClusterName
     * @return  True if the MBean is already registered in the MBean server,
     * false otherwise.
     */
    public boolean isRegistered(String name, String zoneClusterName) {
        boolean retVal = false;

	if (zoneClusterName == null || zoneClusterName.trim().length() == 0) {
	    Attribute nameAttr = new Attribute("Name", name);
	    HashMap rtData = readData(null, nameAttr);
	    if (!rtData.isEmpty()) {
		retVal = ((ResourceTypeData)rtData.get(name)).isRegistered();
	    }
	} else {

	    // for a zonecluster check the clrt -Z <ZC> list rtName command output
	    List commandList = new ArrayList();
	    commandList.add(ClusterPaths.CL_RT_CMD);
	    commandList.add("list");

            commandList.add(ClusterPaths.ZC_OPTION);
            commandList.add(zoneClusterName);
	    commandList.add(name);

	    String[] command = (String[]) commandList.toArray(new String[0]);

	    // Run the command(s)
	    InvocationStatus exits[];
	    try {
		exits = InvokeCommand.execute(
		    new String[][] {
			command,
		    }, null);

		// If error not thrown, means the RT has been registered.
		retVal = true;
	    } catch (InvocationException ie) {
		// don't display anything since an exception just means that
		// the RT has not been registered.
	    }

	}
        return retVal;
    }



    /**
     * Function to determine when to re-fill the cache
     *
     * @return  true if Cache is valid and can be used for requests and returns
     * false if Cache is not valid and must be refilled.
     */
    private boolean isCacheValid(String zoneClusterName) {
        boolean cacheValid = true;
        initZcVariables(zoneClusterName);

        /*
         * It takes a lot of time to parse the instance names from the
         * files, so we only regenerate this list if we have to, which
         * we determine as being when the date stamp on the directory
         * containing instances has changed, or when we've received
         * a cluster event about resource types.
         *
         * The total set of instances is those from the directory
         * merged with those in the runtime. There are no events sent
         * when the runtime configuration changes, so we have to get
         * these names dynamically each time, and not cache them.
         */

        try {
            long cacheTimestamp = 0;
            HashMap cacheTimestampMap = (HashMap)
                                    zcCacheTimestampMap.get(zoneClusterName);

            for (int i = 0; i < rtrDirectoriesCount; i++) {

                /* Check the directory timestamp */
                if (zcInstanceSet.get(zoneClusterName) != null) {
                    File directory = new File(rtrDirectories[i]);

                    cacheTimestamp =
                        ((Long) cacheTimestampMap.get(rtrDirectories[i]))
                        .longValue();

                    if (directory.lastModified() > cacheTimestamp) {
                        cacheTimestampMap.put(rtrDirectories[i],
                            new Long(directory.lastModified()));
                    } else {

                        /* Use ourselves as a filter to select newer files */
                        if (directory.listFiles(this).length < 1) {
                            continue;
                        }
                        /* cacheTimestamp has been updated by filter */
                    }
                }

                cacheValid = false;

                break;
            }
            zcCacheTimestampMap.put(zoneClusterName, cacheTimestampMap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cacheValid;
    }


    private String[] appendZonePath(String[] rtrDirectories, String zonePath) {
        String[] retVal;
        if (zonePath != null && zonePath.trim().length() > 0) {
            retVal = new String[rtrDirectories.length];
            for (int i = 0; i < rtrDirectories.length; i++) {
                retVal[i] = zonePath + ROOT + rtrDirectories[i];
            }
        } else {
            retVal = rtrDirectories;
        }
        return retVal;
    }


    private native synchronized HashMap fillCacheNative(String dirNames[]);

    private native Attribute getAttributeNative(String instanceName,
        String attributeName, String zoneClusterName);

    private native String getZonePath(String zoneClusterName);
    private native ArrayList getNodeList(String zoneClusterName);


    private MBeanServerConnection getMBeanServerConnection(String nodeName) {

        MBeanServerConnection mbsConnection = null;
        try {
            // first get the local connection
            HashMap env = new HashMap();
            env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                this.getClass().getClassLoader());
            env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                this.getClass().getClassLoader());

            JMXConnector connectors = TrustedMBeanModel.getWellKnownConnector(
                    nodeName, env);

            // Now get a reference to the mbean server connection
            mbsConnection = connectors.getMBeanServerConnection();

        } catch (Exception ex) {
            mbsConnection = null;
        }
        return mbsConnection;
    }


    private ResourceTypeData getResourceTypeData(Map attrMap) {
        ResourceTypeData rtData = new ResourceTypeData();

        rtData.setName((String)((Attribute)attrMap.get("Name")).getValue());
        rtData.setBaseDirectory((String)((Attribute)
                attrMap.get("BaseDirectory")).getValue());
        rtData.setMethods((Map)((Attribute)attrMap.get("Methods")).getValue());
        rtData.setSingleInstance(((Boolean)((Attribute)
                attrMap.get("SingleInstance")).getValue()).booleanValue());
        rtData.setRegistered(((Boolean)((Attribute)
                attrMap.get("Registered")).getValue()).booleanValue());
        rtData.setInitNodesLocation((ResourceTypeInitNodesLocationEnum)
                    ((Attribute)attrMap.get("InitNodesLocation")).getValue());
        rtData.setInstalledNodeNames((String[])((Attribute)
                attrMap.get("InstalledNodeNames")).getValue());
        rtData.setFailover(((Boolean)((Attribute)
                attrMap.get("Failover")).getValue()).booleanValue());
        rtData.setSystemResourceType((ResourceTypeSystemEnum)
                ((Attribute)attrMap.get("SystemResourceType")).getValue());
        rtData.setApiVersion(((Long)((Attribute)
                attrMap.get("ApiVersion")).getValue()).longValue());
        rtData.setVersion((String)((Attribute)
                attrMap.get("Version")).getValue());
        rtData.setPackages((String[])((Attribute)
                attrMap.get("Packages")).getValue());
        rtData.setDescription((String)((Attribute)
                attrMap.get("Description")).getValue());
        rtData.setVersionable(((Boolean)((Attribute)
                attrMap.get("Versionable")).getValue()).booleanValue());
        rtData.setSystem(((Boolean)((Attribute)
                attrMap.get("System")).getValue()).booleanValue());
        rtData.setUpgradeVersions((String[])((Attribute)
                attrMap.get("UpgradeVersions")).getValue());
        rtData.setUpgradeTunabilities((ResourceTypeVersionTunabilityEnum[])
                ((Attribute)attrMap.get("UpgradeTunabilities")).getValue());
        rtData.setDowngradeVersions((String[])((Attribute)
                attrMap.get("DowngradeVersions")).getValue());
        rtData.setDowngradeTunabilities((ResourceTypeVersionTunabilityEnum[])
                ((Attribute)attrMap.get("DowngradeTunabilities")).getValue());
        rtData.setSystemProperties((List)((Attribute)
                attrMap.get("SystemProperties")).getValue());
        rtData.setExtProperties((List)((Attribute)
                attrMap.get("ExtProperties")).getValue());

        return rtData;
    }


    /**
     *
     * @param args  -n <n>	repeat test <n> times
     *		    -q		quiet mode
     *		    -z		zonecluster name
     *
     * @return boolean status of readData method
     */
    public static boolean test(String[] args) {
        RtGroup rt = new RtGroup();

        int iterations = 1;
        String zcNameQ = null;

        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-q")) {
                quiet = true;
            } else if (args[i].equals("-n")) {
                iterations = Integer.parseInt(args[++i]);
            } else if (args[i].equals("-z")) {
                zcNameQ = args[++i];
            }
        }

        try {
            // Load the agent JNI library
            Runtime.getRuntime().load(lib);
        } catch (UnsatisfiedLinkError ule) {
            print("load of " + lib + " failed!");
            ule.printStackTrace();
            return false;
        }

        long start = System.currentTimeMillis();
        long loopstart;
        long loopelapsed;
        int count = 0;
        print("\nReading data " + iterations + " time(s) ... ");
        for (int i = 0; i < iterations; i++) {
            try {
                loopstart = System.currentTimeMillis();
                HashMap rtData;
                rtData = rt.readData(zcNameQ, null);
                if (rtData == null) {
                    print("readData returned null!");
                    return false;
                }
                count = i + 1;
                print("\nREAD " + count + " : returned " + rtData.size()
                        + " Resource Types");
                if (!quiet) {
                    print("ResourceTypes : \n" + rtData);
                }
                loopelapsed = System.currentTimeMillis() - loopstart;
                print("time = " + loopelapsed + " ms");
            } catch (Exception e) {
                print("readDataJNI failed!");
                print(e.getMessage());
                e.printStackTrace();
                return false;
            }
        }
        quiet = false;
        long elapsed = System.currentTimeMillis() - start;
        print("\nTotal Time for " + iterations + " iterations = " +
                        elapsed + " ms");

        return true;
    }

    public static void print(String msg) {
        System.out.println(msg);
        logger.fine(msg);
    }

}
