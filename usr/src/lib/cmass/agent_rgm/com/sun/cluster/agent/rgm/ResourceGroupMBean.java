/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceGroupMBean.java 1.10     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;

import java.util.Map;


/**
 * This interface defines the management operation available on a remote Cluster
 * Resource Group.
 */
public interface ResourceGroupMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public final static String TYPE = "resourcegroup";

    public final static String SYSTEM = "RG_system";
    public final static String DESCRIPTION = "RG_description";
    public final static String AUTO_START = "Auto_start_on_new_cluster";
    public final static String FAILBACK = "Failback";
    public final static String FAILURE_INTERVAL = "Pingpong_interval";
    public final static String SHARED_STORAGE_USED = "Global_resources_used";
    public final static String PATH_PREFIX = "Pathprefix";
    public final static String PROJECT_NAME = "RG_project_name";
    public final static String SLM_TYPE = "RG_SLM_type";
    public final static String SLM_PSET_TYPE = "RG_SLM_Pset_type";
    public final static String SLM_CPU_SHARES = "RG_SLM_CPU_Shares";
    public final static String SLM_PSET_MIN = "RG_SLM_Pset_Min";
    public final static String PRIMARIES = "Nodelist";
    public final static String AFFINITIES = "RG_affinities";
    public final static String DESIRED = "Desired_primaries";
    public final static String MAX = "Maximum_primaries";
    public final static String DEPEND = "RG_dependencies";
    public final static String NETWORKS_DEPEND =
        "Implicit_network_dependencies";

    /**
     * Get the resource group's key name.
     *
     * @return  resource group's key name.
     */
    public String getName();

    /**
     * Get the resource group's overall status based on the status on each node.
     * The rule is as follows :<br>
     * {@link ResourceGroupStatusEnum#UNMANAGED} &lt;
     * {@link ResourceGroupStatusEnum#OFFLINE} &lt;
     * {@link ResourceGroupStatusEnum#ONLINE} &lt;
     * {@link ResourceGroupStatusEnum#PENDING_ONLINE} &lt;
     * {@link ResourceGroupStatusEnum#PENDING_OFFLINE} &lt;
     * {@link ResourceGroupStatusEnum#ERROR_STOP_FAILED} &lt;
     * {@link ResourceGroupStatusEnum#ONLINE_FAULTED}
     *
     * @return  a {@link ResourceGroupStatusEnum} objects.
     */
    public ResourceGroupStatusEnum getOverallStatus();

    /**
     * Get the resource group's current status on each node.
     *
     * @return  an array of {@link ResourceGroupStatus} objects.
     */
    public ResourceGroupStatus[] getStatus();

    /**
     * Get the manageability status of the resource group.
     *
     * @return  <code>true</code> if the resource group is on a 'managed' state,
     * <code>false</code> if not.
     */
    public boolean isManaged();

    /**
     * Get the online status of the resource group.
     *
     * @return  <code>true</code> if the resource group is online, <code>
     * false</code> if not.
     */
    public boolean isOnline();

    /**
     * Get the type of the resource group.
     *
     * @return  a {@link ResourceGroupModeEnum} object.
     */
    public ResourceGroupModeEnum getMode();

    /**
     * Get the auto-start capability of the resource group.
     *
     * @return  <code>true</code> if the resource group must automatically start
     * when a new cluster forms, <code>false</code> if it must be manually
     * start.
     */
    public boolean isAutoStartable();

    /**
     * Get the network dependency of the resource group.
     *
     * @return  <code>true</code> if some non-network resources depends on
     * network resources, <code>false</code> otherwise.
     */
    public boolean isNetworkDependent();

    /**
     * Wether this resource group is a system resource group or not.
     *
     * @return  <code>true</code> if this resource group is a system resource
     * group, <code>false</code> otherwise.
     */
    public boolean isSystem();

    /**
     * Get the failback flag for this resource group.
     *
     * @return  <code>true</code> if the failback is enabled, in other words, if
     * the resource group shall identified the appropriate primary node to use
     * in case of cluster membership change, <code>false</code> if the failback
     * is not handled.
     */
    public boolean isFailbackEnabled();

    /**
     * Wether shared storage are used by one of the resources in this resource
     * group or not.
     *
     * @return  <code>true</code> if shared storages are used <code>false</code>
     * otherwise.
     */
    public boolean isSharedStorageUsed();

    /**
     * Wether this resource group is suspended or not.
     *
     * @return  <code>true</code> if this resource group is suspended, <code>
     * false</code> otherwise.
     */
    public boolean isSuspended();

    /**
     * Get the array of nodes names that are possible primaries for this
     * resource group.
     *
     * @return  an array of node name.
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String[] getNodeNames();

    /**
     * Get the currently assigned primary nodes on this resource group.
     *
     * @return  an array of node name.
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String[] getCurrentPrimaries();

    /**
     * Get the current number of simultaneous primaries for this resource group.
     *
     * @return  the current number of primaries.
     */
    public int getCurrentPrimariesCount();

    /**
     * Get the desired number of simultaneous primaries for this resource group.
     *
     * @return  the desired number of primaries.
     */
    public int getDesiredPrimariesCount();

    /**
     * Get the maximum number of simultaneous primaries for this resource group.
     *
     * @return  the maximum number of primaries.
     */
    public int getMaxPrimaries();

    /**
     * Get resource group's dependencies on other resource groups.
     *
     * @return  an array of resource groups name or <code>null</code> if this
     * resource group has no dependency.
     */
    public String[] getDependencies();

    /**
     * Get the current number of resource group's dependencies on other resource
     * groups.
     *
     * @return  the current number of dependencies.
     */
    public int getDependenciesCount();

    /**
     * Get path to the directory owning the administrative files for the
     * resources contained in this resource group.
     *
     * @return  the resource group's path prefix.
     */
    public String getDirectoryPath();

    /**
     * Get this resource group's description.
     *
     * @return  resource group's description.
     */
    public String getDescription();

    /**
     * Get the node failure interval for the resource group startup.
     *
     * @return  failure interval in seconds.
     */
    public int getFailureInterval();

    /**
     * Get this resource group's affinities with other resource groups. The
     * returned array contained a list of resource group's name prefixed by:
     *
     * <ul>
     * <li>++ for strong positive affinity.</li>
     * <li>+  for weak positive affinity.</li>
     * <li>-  for weak negative affinity.</li>
     * <li>-- for strong negative affinity.</li>
     * </ul>
     *
     * @return  resource group's affinities.
     */
    public String[] getAffinities();

    /**
     * Get the Solaris Resource Management project name associated to this
     * resource group.
     *
     * @return  the project name or <code>null</code> if unknown.
     */
    public String getProjectName();

    /**
     * Get the Service Level Management Type associated to this resource group.
     *
     * @return  the SLM type or <code>null</code> if unknown.
     */
    public String getSLMType();

    /**
     * Get the Service Level Management Pset Type associated to this resource
     * group.
     *
     * @return  the SLM pset type or <code>null</code> if unknown.
     */
    public String getSLMPsetType();

    /**
     * Get the Service Level Management CPU shares associated to this resource
     * group. The unit is cpu shares, and in cases where the psetType is NOT
     * set to default, and that the RG is configured to run only inside zones,
     * the shares are converted to number of CPU to be configured inside a pset,
     * for instance 125 means, 1 cpu and 25% of another.
     *
     * @return  the SLM CPU or <code>null</code> if unknown.
     */
    public int getSLMCPUShares();

    /**
     * Get the Service Level Management Pset Min associated to this resource
     * group. The unit is "one cpu", for instance 1 or 2 are accepted and means
     * at least one 1 CPU or 2 CPU must be part of the pset. 
     *
     * @return  the SLM CPU MIN or <code>null</code> if unknown.
     */
    public int getSLMPsetMin();

    /**
     * Get the set of resource names that are associated with this group.
     *
     * @return  a <code>Set</code> value containing resource's name or <code>
     * null</code> if this resource group has no resource.
     *
     * @see  com.sun.cluster.agent.rgm.ResourceMBean
     */
    public String[] getResourceNames(); // could/should use relationship obj

    /**
     * Change resource group's description.
     *
     * @param  newDescription  the new description.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeDescription(String newDescription)
        throws CommandExecutionException;

    /**
     * Change the node failure interval for the resource group startup.
     *
     * @param  newInterval  the new interval in seconds.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeFailureInterval(int newInterval)
        throws CommandExecutionException;

    /**
     * Change path to the directory owning the administrative files for the
     * resources contained in this resource group.
     *
     * @param  newDirectoryPath  the new directory path.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeDirectoryPath(String newDirectoryPath)
        throws CommandExecutionException;

    /**
     * Change the Solaris Resource Management project name associated to this
     * resource group.
     *
     * @param  newProjectName  the new project name.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeProjectName(String newProjectName)
        throws CommandExecutionException;

    /**
     * Change the Service Level Management type associated to this resource
     * group.
     *
     * @param  newSLMType  the new SLM type.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeSLMType(String newSLMType)
        throws CommandExecutionException;

    /**
     * Change the Service Level Management pset type associated to this resource
     * group.
     *
     * @param  newSLMPsetType  the new SLM type.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeSLMPsetType(String newSLMPsetType)
        throws CommandExecutionException;

    /**
     * Change the Service Level Management CPU shares associated to this
     * resource group.
     *
     * @param  newSLMCPUShares  the new SLM CPU share value.
     *
     * @return  a {@link ExitStatus[]} object.
     *
     * @throws  CommandxecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeSLMCPUShares(String newSLMCPUShares)
        throws CommandExecutionException;

    /**
     * Change the Service Level Management Pset min size associated to this
     * resource group.
     *
     * @param  newSLMPsetMin  the new SLM Pset min size.
     *
     * @return  a {@link ExitStatus[]} object.
     *
     * @throws  CommandxecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeSLMPsetMin(String newSLMPsetMin)
        throws CommandExecutionException;

    /**
     * Defines wether shared storage are used by one of the resources in this
     * resource group or not.
     *
     * @param  newFlag  <code>true</code> or <code>false</code>.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeSharedStorageUsedFlag(boolean newFlag)
        throws CommandExecutionException;

    /**
     * Change the failback flag for this resource group.
     *
     * @param  newFlag  <code>true</code> or <code>false</code>.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeFailbackFlag(boolean newFlag)
        throws CommandExecutionException;

    /**
     * Change the auto-start capability of the resource group.
     *
     * @param  newFlag  <code>true</code> or <code>false</code>.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeAutoStartableFlag(boolean newFlag)
        throws CommandExecutionException;

    /**
     * Defines wether this resource group is a system resource group or not.
     *
     * @param  newFlag  <code>true</code> or <code>false</code>.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeSystemFlag(boolean newFlag)
        throws CommandExecutionException;

    /**
     * Defines the network dependency of the resource group.
     *
     * @param  newFlag  <code>true</code> or <code>false</code>.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeNetworkDependencyFlag(boolean newFlag)
        throws CommandExecutionException;

    /**
     * Change the list of nodes that are possible primaries for this resource
     * group. The order of the names in the provided array defines the order of
     * the failback and the default primaries when the resource group is brought
     * online.
     *
     * @param  newNodeNames  an ordered array of node names.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeNodeNames(String newNodeNames[])
        throws CommandExecutionException;

    /**
     * Change the desired number of simultaneous primaries for this resource
     * group.
     *
     * @param  newCount  the new number.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeDesiredPrimariesCount(int newCount)
        throws CommandExecutionException;

    /**
     * Change the maximum number of simultaneous primaries for this resource
     * group.
     *
     * @param  newCount  the new number.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeMaxPrimaries(int newCount)
        throws CommandExecutionException;

    /**
     * Change resource group's dependencies on other resource groups.
     *
     * @param  newDependencies  the new set of resource group's dependencies.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeDependencies(String newDependencies[])
        throws CommandExecutionException;

    /**
     * Change resource group's affinities on other resource groups.
     *
     * @param  newAffinities  the new set of resource group's affinities.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeAffinities(String newAffinities[])
        throws CommandExecutionException;

    /**
     * Change a set of properties for this resource group.
     *
     * @param  newProperties  the new properties associated to this resource
     * group.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeProperties(Map newProperties, boolean commit)
        throws CommandExecutionException;

    /**
     * Delete this resource group.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] remove() throws CommandExecutionException;

    /**
     * Stop and restart this resource group.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] restart() throws CommandExecutionException;

    /**
     * Put this resource group online.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOnline() throws CommandExecutionException;

    /**
     * Put this resource group offline.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOffline() throws CommandExecutionException;

    /**
     * Put this resource group in an 'managed' state.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] manage() throws CommandExecutionException;

    /**
     * Put this resource group in an 'unmanaged' state.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] unmanage() throws CommandExecutionException;

    /**
     * Suspend this resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] suspend() throws CommandExecutionException;

    /**
     * Fast suspend this resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] fastsuspend() throws CommandExecutionException;

    /**
     * Resume this resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] resume() throws CommandExecutionException;

    /**
     * Switch the primaries of this resource group.
     *
     * @param  newPrimaries  an array of node names.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] switchPrimaries(String newPrimaries[])
        throws CommandExecutionException;

    /**
     * Enable the resources of this resource group. This interface uses scswitch
     * -e -j <rs> [-j <rs>] to accomplish the task, and thus handles dependency
     * among resources.
     *
     * @param  resourceNames  an array of resource names.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] enableResources(String resourceNames[])
        throws CommandExecutionException;

    /**
     * Disables the resources of this resource group. This interface uses
     * scswitch -n -j <rs> [-j <rs>] to accomplish the task, and thus handles
     * dependency among resources.
     *
     * @param  resourceNames  an array of resource names.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] disableResources(String resourceNames[])
        throws CommandExecutionException;

}
