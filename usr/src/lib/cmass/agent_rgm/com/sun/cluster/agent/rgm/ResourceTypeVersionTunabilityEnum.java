/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceTypeVersionTunabilityEnum.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm;

/**
 * The version tunability enum of a resource type as defined in the SCHA API.
 * File <code>usr/src/head/rgm/rgm_common.h</code>
 */
public class ResourceTypeVersionTunabilityEnum extends com.sun.cacao.Enum
    implements java.io.Serializable {

    public static final ResourceTypeVersionTunabilityEnum NONE =
        new ResourceTypeVersionTunabilityEnum("NONE", 0);

    public static final ResourceTypeVersionTunabilityEnum AT_CREATION =
        new ResourceTypeVersionTunabilityEnum("AT_CREATION");

    public static final ResourceTypeVersionTunabilityEnum ANYTIME =
        new ResourceTypeVersionTunabilityEnum("ANYTIME");

    public static final ResourceTypeVersionTunabilityEnum WHEN_OFFLINE =
        new ResourceTypeVersionTunabilityEnum("WHEN_OFFLINE");

    public static final ResourceTypeVersionTunabilityEnum WHEN_UNMANAGED =
        new ResourceTypeVersionTunabilityEnum("WHEN_UNMANAGED");

    public static final ResourceTypeVersionTunabilityEnum WHEN_UNMONITORED =
        new ResourceTypeVersionTunabilityEnum("WHEN_UNMONITORED");

    public static final ResourceTypeVersionTunabilityEnum WHEN_DISABLED =
        new ResourceTypeVersionTunabilityEnum("WHEN_DISABLED");

    private ResourceTypeVersionTunabilityEnum(String name) {
        super(name);
    }

    private ResourceTypeVersionTunabilityEnum(String name, int ord) {
        super(name, ord);
    }
}
