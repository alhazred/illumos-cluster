/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceGroupModeEnum.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm;

/**
 * The Resource Group Mode attribute enum as defined in the SCHA API
 */
public class ResourceGroupModeEnum extends com.sun.cacao.Enum
    implements java.io.Serializable {

    /**  */
    public static final ResourceGroupModeEnum NONE = new ResourceGroupModeEnum(
            "NONE", 0);

    /**  */
    public static final ResourceGroupModeEnum FAILOVER =
        new ResourceGroupModeEnum("FAILOVER");

    /**  */
    public static final ResourceGroupModeEnum SCALABLE =
        new ResourceGroupModeEnum("SCALABLE");

    private ResourceGroupModeEnum(String name) {
        super(name);
    }

    private ResourceGroupModeEnum(String name, int ord) {
        super(name, ord);
    }

}
