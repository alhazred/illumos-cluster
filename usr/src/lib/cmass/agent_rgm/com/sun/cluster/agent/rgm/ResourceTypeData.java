/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceTypeData.java	1.2	08/07/10 SMI"
 */

package com.sun.cluster.agent.rgm;

import java.util.List;
import java.util.Map;


/**
 * Resource Type Data class
 */
public class ResourceTypeData implements java.io.Serializable {

    /** Constant identifying the resource type's START method */
    public final static String METHOD_START = "START";

    /** Constant identifying the resource type's STOP method */
    public final static String METHOD_STOP = "STOP";

    /** Constant identifying the resource type's ABORT method */
    public final static String METHOD_ABORT = "ABORT";

    /** Constant identifying the resource type's VALIDATE method */
    public final static String METHOD_VALIDATE = "VALIDATE";

    /** Constant identifying the resource type's UPDATE method */
    public final static String METHOD_UPDATE = "UPDATE";

    /** Constant identifying the resource type's INIT method */
    public final static String METHOD_INIT = "INIT";

    /** Constant identifying the resource type's FINI method */
    public final static String METHOD_FINI = "FINI";

    /** Constant identifying the resource type's BOOT method */
    public final static String METHOD_BOOT = "BOOT";

    /** Constant identifying the resource type's MONITOR_START method */
    public final static String METHOD_MONITOR_START = "MONITOR START";

    /** Constant identifying the resource type's MONITOR_STOP method */
    public final static String METHOD_MONITOR_STOP = "MONITOR STOP";

    /** Constant identifying the resource type's MONITOR_CHECK method */
    public final static String METHOD_MONITOR_CHECK = "MONITOR CHECK";

    /** Constant identifying the resource type's PRENET_START method */
    public final static String METHOD_PRENET_START = "PRENET START";

    /** Constant identifying the resource type's POSTNET_STOP method */
    public final static String METHOD_POSTNET_STOP = "POSTNET STOP";

    /** Constant identifying the resource type's POSTNET_ABORT method */
    public final static String METHOD_POSTNET_ABORT = "POSTNET ABORT";


    private String name;
    private String baseDirectory;
    private Map methods;
    private boolean singleInstance;
    private boolean registered;
    private ResourceTypeInitNodesLocationEnum initNodesLocation;
    private String[] installedNodeNames;
    private boolean failover;
    private ResourceTypeSystemEnum systemResourceType;
    private long apiVersion;
    private String version;
    private String[] packages;
    private String description;
    private boolean versionable;
    private boolean system;
    private String[] upgradeVersions;
    private ResourceTypeVersionTunabilityEnum[] upgradeTunabilities;
    private String[] downgradeVersions;
    private ResourceTypeVersionTunabilityEnum[] downgradeTunabilities;
    private List systemProperties;
    private List extProperties;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseDirectory() {
        return baseDirectory;
    }

    public void setBaseDirectory(String baseDirectory) {
        this.baseDirectory = baseDirectory;
    }

    public Map getMethods() {
        return methods;
    }

    public void setMethods(Map methods) {
        this.methods = methods;
    }

    public boolean isSingleInstance() {
        return singleInstance;
    }

    public void setSingleInstance(boolean singleInstance) {
        this.singleInstance = singleInstance;
    }

    public ResourceTypeInitNodesLocationEnum getInitNodesLocation() {
        return initNodesLocation;
    }

    public void setInitNodesLocation(
            ResourceTypeInitNodesLocationEnum initNodesLocation) {
        this.initNodesLocation = initNodesLocation;
    }

    public String[] getInstalledNodeNames() {
        return installedNodeNames;
    }

    public void setInstalledNodeNames(String[] installedNodeNames) {
        this.installedNodeNames = installedNodeNames;
    }

    public boolean isFailover() {
        return failover;
    }

    public void setFailover(boolean failover) {
        this.failover = failover;
    }

    public ResourceTypeSystemEnum getSystemResourceType() {
        return systemResourceType;
    }

    public void setSystemResourceType(
            ResourceTypeSystemEnum systemResourceType) {
        this.systemResourceType = systemResourceType;
    }

    public long getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(long apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String[] getPackages() {
        return packages;
    }

    public void setPackages(String[] packages) {
        this.packages = packages;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isVersionable() {
        return versionable;
    }

    public void setVersionable(boolean versionable) {
        this.versionable = versionable;
    }

    public boolean isSystem() {
        return system;
    }

    public void setSystem(boolean system) {
        this.system = system;
    }

    public String[] getUpgradeVersions() {
        return upgradeVersions;
    }

    public void setUpgradeVersions(String[] upgradeVersions) {
        this.upgradeVersions = upgradeVersions;
    }

    public ResourceTypeVersionTunabilityEnum[] getUpgradeTunabilities() {
        return upgradeTunabilities;
    }

    public void setUpgradeTunabilities(
            ResourceTypeVersionTunabilityEnum[] upgradeTunabilities) {
        this.upgradeTunabilities = upgradeTunabilities;
    }

    public String[] getDowngradeVersions() {
        return downgradeVersions;
    }

    public void setDowngradeVersions(String[] downgradeVersions) {
        this.downgradeVersions = downgradeVersions;
    }

    public ResourceTypeVersionTunabilityEnum[] getDowngradeTunabilities() {
        return downgradeTunabilities;
    }

    public void setDowngradeTunabilities(
            ResourceTypeVersionTunabilityEnum[] downgradeTunabilities) {
        this.downgradeTunabilities = downgradeTunabilities;
    }

    public List getSystemProperties() {
        return systemProperties;
    }

    public void setSystemProperties(List systemProperties) {
        this.systemProperties = systemProperties;
    }

    public List getExtProperties() {
        return extProperties;
    }

    public void setExtProperties(List extProperties) {
        this.extProperties = extProperties;
    }


    public ResourceTypeData() {
    }

    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("[name= " + name);
        s.append(", baseDirectory= " + baseDirectory);
        s.append(", singleInstance= " + singleInstance);
        s.append(", installedNodeNames= " +
                convertStringAr(installedNodeNames));
        s.append("]");
        return s.toString();
    }


    public static String convertStringAr(String[] inputList) {
        String delimiter = ", ";
        StringBuffer tmpStr = new StringBuffer();
        tmpStr.append("[");
        if (inputList != null) {
            for (int i = 0; i < inputList.length; i++) {
                if (i > 0) {
                    tmpStr.append(delimiter);
                }
                tmpStr.append(inputList[i]);
            }
        }
        tmpStr.append("]");
        return tmpStr.toString();
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

}
