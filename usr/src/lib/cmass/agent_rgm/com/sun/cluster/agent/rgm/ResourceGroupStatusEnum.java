/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceGroupStatusEnum.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm;

/**
 * The Resource Group Status attribute enum as defined in the SCHA API
 */
public class ResourceGroupStatusEnum extends com.sun.cacao.Enum
    implements java.io.Serializable {

    /** The resource group is not managed. */
    public static final ResourceGroupStatusEnum UNMANAGED =
        new ResourceGroupStatusEnum("UNMANAGED", 0);

    /**  */
    public static final ResourceGroupStatusEnum ONLINE =
        new ResourceGroupStatusEnum("ONLINE");

    /**  */
    public static final ResourceGroupStatusEnum OFFLINE =
        new ResourceGroupStatusEnum("OFFLINE");

    /**  */
    public static final ResourceGroupStatusEnum PENDING_ONLINE =
        new ResourceGroupStatusEnum("PENDING_ONLINE");

    /**  */
    public static final ResourceGroupStatusEnum PENDING_OFFLINE =
        new ResourceGroupStatusEnum("PENDING_OFFLINE");

    /**  */
    public static final ResourceGroupStatusEnum ERROR_STOP_FAILED =
        new ResourceGroupStatusEnum("ERROR_STOP_FAILED");

    /**  */
    public static final ResourceGroupStatusEnum ONLINE_FAULTED =
        new ResourceGroupStatusEnum("ONLINE_FAULTED");

    /**  */
    public static final ResourceGroupStatusEnum PENDING_ONLINE_BLOCKED =
        new ResourceGroupStatusEnum("PENDING_ONLINE_BLOCKED");

    private ResourceGroupStatusEnum(String name) {
        super(name);
    }

    private ResourceGroupStatusEnum(String name, int ord) {
        super(name, ord);
    }
}
