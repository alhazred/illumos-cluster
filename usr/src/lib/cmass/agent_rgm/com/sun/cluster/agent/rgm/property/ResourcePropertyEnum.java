/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourcePropertyEnum.java 1.8     08/05/20 SMI"
 */
package com.sun.cluster.agent.rgm.property;

// JDK
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


/**
 * object describing an enum resource property, with built-in range and type
 * information and checking.
 */
public class ResourcePropertyEnum extends ResourceProperty {

    private List enumList;
    private String defaultValue;
    private String value;
    private HashMap pernodeValue;
    private HashMap pernodeDefValueMap;

    public ResourcePropertyEnum() {
        super();
    }

    public ResourcePropertyEnum(String name, boolean extension,
        boolean per_node, boolean required, String description,
        ResourcePropertyEnum tunable, List enumList, String defaultValue,
        String value, HashMap pernodeValue) {

        super(name, extension, per_node, required, description, tunable);

        this.enumList = enumList;
        this.defaultValue = defaultValue;

        if (value != null) {
            setValue(value);
        } else if (pernodeValue != null) {
            setPernodeValue(pernodeValue);
        }
    }

    public List getEnumList() {
        return enumList;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public HashMap getPernodeValue() {
        return pernodeValue;
    }

    public String getValue() {
        return value;
    }

    public void setEnumList(List enumList) {
        this.enumList = enumList;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public void setValue(String newValue) {

        if (!enumList.contains(newValue)) {
            throw new IllegalArgumentException("value out of range:" +
                newValue);
        }

        this.value = newValue;
    }

    public void setPernodeValue(HashMap pernodeValue) {

        if (pernodeValue == null) {
            this.pernodeValue = null;

            return;
        }

        Iterator i = pernodeValue.keySet().iterator();
        String nodename;
        String newValue;

        while (i.hasNext()) {
            nodename = (String) i.next();
            newValue = (String) pernodeValue.get(nodename);

            if (!enumList.contains(newValue)) {
                throw new IllegalArgumentException("value out of range:" +
                    newValue);
            }
        }

        this.pernodeValue = pernodeValue;
    }

    public void storePernodeValueMap(HashMap pernodeDefValueMap) {
        this.pernodeDefValueMap = pernodeDefValueMap;
    }

    public HashMap retrievePernodeValueMap() {
        return this.pernodeDefValueMap;
    }

    public String toString() {
        return super.toString() + " LIST=" + enumList + " DEFAULT=" +
            defaultValue + " VALUE=" + value + "PERNODEVALUE=" + pernodeValue +
            ">";
    }
}
