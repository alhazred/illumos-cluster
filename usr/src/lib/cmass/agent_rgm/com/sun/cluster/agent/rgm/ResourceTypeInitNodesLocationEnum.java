/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceTypeInitNodesLocationEnum.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm;

/**
 * This Enum enables to identify the source of information to locate the init
 * nodes for a resource type. File <code>usr/src/sys/scha_types.h</code>
 */
public class ResourceTypeInitNodesLocationEnum extends com.sun.cacao.Enum
    implements java.io.Serializable {

    /**
     * The init nodes are defined by the list of resource group's primary nodes.
     */
    public static final ResourceTypeInitNodesLocationEnum RG_PRIMARIES =
        new ResourceTypeInitNodesLocationEnum("RG_PRIMARIES", 0);

    /**
     * The init nodes are defined by the list of resource type's installed
     * nodes.
     */
    public static final ResourceTypeInitNodesLocationEnum RT_INSTALLED_NODES =
        new ResourceTypeInitNodesLocationEnum("RT_INSTALLED_NODES");

    private ResourceTypeInitNodesLocationEnum(String name) {
        super(name);
    }

    private ResourceTypeInitNodesLocationEnum(String name, int ord) {
        super(name, ord);
    }

}
