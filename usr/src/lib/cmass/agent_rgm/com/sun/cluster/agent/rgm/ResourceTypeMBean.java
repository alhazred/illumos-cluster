/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceTypeMBean.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * MBean interface for describing resource type instances
 */
public interface ResourceTypeMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public final static String TYPE = "resourcetype";

    /** Constant identifying the resource type's START method */
    public final static String METHOD_START = "START";

    /** Constant identifying the resource type's STOP method */
    public final static String METHOD_STOP = "STOP";

    /** Constant identifying the resource type's ABORT method */
    public final static String METHOD_ABORT = "ABORT";

    /** Constant identifying the resource type's VALIDATE method */
    public final static String METHOD_VALIDATE = "VALIDATE";

    /** Constant identifying the resource type's UPDATE method */
    public final static String METHOD_UPDATE = "UPDATE";

    /** Constant identifying the resource type's INIT method */
    public final static String METHOD_INIT = "INIT";

    /** Constant identifying the resource type's FINI method */
    public final static String METHOD_FINI = "FINI";

    /** Constant identifying the resource type's BOOT method */
    public final static String METHOD_BOOT = "BOOT";

    /** Constant identifying the resource type's MONITOR_START method */
    public final static String METHOD_MONITOR_START = "MONITOR START";

    /** Constant identifying the resource type's MONITOR_STOP method */
    public final static String METHOD_MONITOR_STOP = "MONITOR STOP";

    /** Constant identifying the resource type's MONITOR_CHECK method */
    public final static String METHOD_MONITOR_CHECK = "MONITOR CHECK";

    /** Constant identifying the resource type's PRENET_START method */
    public final static String METHOD_PRENET_START = "PRENET START";

    /** Constant identifying the resource type's POSTNET_STOP method */
    public final static String METHOD_POSTNET_STOP = "POSTNET STOP";

    /** Constant identifying the resource type's POSTNET_ABORT method */
    public final static String METHOD_POSTNET_ABORT = "POSTNET ABORT";

    /**
     * Get the resource type's key name.
     *
     * @return  resource type's key name.
     */
    public String getName();

    /**
     * Return true if the resource type is registered to the RGM. If this
     * returns false, all other attributes apart from Name are invalid.
     */
    public boolean isRegistered();

    /**
     * Get the resource type's base directory.
     *
     * @return  resource type's base directory.
     */
    public String getBaseDirectory();

    /**
     * Get a <code>Map</code> of string identifying the method names linked to
     * this resource type. Map's elements IDs correspond to one of the
     * followings :
     *
     * <ul>
     * <li>{@link #METHOD_START}</li>
     * <li>{@link #METHOD_STOP}</li>
     * <li>{@link #METHOD_ABORT}</li>
     * <li>{@link #METHOD_VALIDATE}</li>
     * <li>{@link #METHOD_UPDATE}</li>
     * <li>{@link #METHOD_INIT}</li>
     * <li>{@link #METHOD_FINI}</li>
     * <li>{@link #METHOD_BOOT}</li>
     * <li>{@link #METHOD_MONITOR_START}</li>
     * <li>{@link #METHOD_MONITOR_STOP}</li>
     * <li>{@link #METHOD_MONITOR_CHECK}</li>
     * <li>{@link #METHOD_PRENET_START}</li>
     * <li>{@link #METHOD_POSTNET_STOP}</li>
     * <li>{@link #METHOD_POSTNET_ABORT}</li>
     * </ul>
     *
     * @return  an ordered array of method's name.
     */
    public java.util.Map getMethods();

    /**
     * Give the "single instance" flag of the resource type.
     *
     * @return  <code>true</code> if this resource type is a single instance,
     * <code>false</code> otherwise.
     */
    public boolean isSingleInstance();

    /**
     * Enabled to know where to look for the list of init nodes.
     *
     * @return  a {@link ResourceTypeInitNodesLocationEnum} object.
     */
    public ResourceTypeInitNodesLocationEnum getInitNodesLocation();

    /**
     * Get the array of nodes on which the resource type is installed.
     *
     * @return  an array of node name.
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String[] getInstalledNodeNames();

    /**
     * Give the "failover" flag of the resource type.
     *
     * @return  <code>true</code> if this resource type handles failover, <code>
     * false</code> if not.
     */
    public boolean isFailover();

    /**
     * Get the type of predefined system resource types such as <code>
     * Logical_hostname</code> or <code>Shared_address</code>.
     *
     * @return  a {@link ResourceTypeSystemEnum} object.
     */
    public ResourceTypeSystemEnum getSystemResourceType();

    /**
     * Get the resource type's API version.
     *
     * @return  the API version.
     */
    public long getApiVersion();

    /**
     * Get the resource type version.
     *
     * @return  a <code>String</code> representation of the resource type
     * version.
     */
    public String getVersion();

    /**
     * Get the list of packages needed to install this resouorce type.
     *
     * @return  an array of package name.
     */
    public String[] getPackages();

    /**
     * Get the resource type's description.
     *
     * @return  the description.
     */
    public String getDescription();

    /**
     * This method enables to know if this resource type is a 'new' or 'old'
     * kind of resource type. The 'new' deals with versionning and have a
     * version number, the 'old' does not have a version number.
     *
     * @return  <code>true</code> if 'new' type, <code>false</code> if 'old'.
     */
    public boolean isVersionable();

    /**
     * Give the "system" flag of the resource type.
     *
     * @return  <code>true</code> if it's a system resource type, <code>
     * false</code> if not.
     */
    public boolean isSystem();

    /**
     * Get the list of upgrade versions for this resource type (use the
     * {@link #getUpgradeTunabilities()} method to get the associated
     * tunability).
     *
     * @return  an array of versions.
     */
    public String[] getUpgradeVersions();

    /**
     * Get the array of tunabilities associated to the versions returned by the
     * {@link #getUpgradeVersions()} method.
     *
     * @return  an array of {@link ResourceTypeVersionTunabilityEnum} objects.
     */
    public ResourceTypeVersionTunabilityEnum[] getUpgradeTunabilities();

    /**
     * Get the list of downgrade versions for this resource type (use the
     * {@link #getDowngradeTunabilities()} method to get the associated
     * tunability).
     *
     * @return  an array of versions.
     */
    public String[] getDowngradeVersions();

    /**
     * Get the array of tunabilities associated to the versions returned by the
     * {@link #getDowngradeVersions()} method.
     *
     * @return  an array of {@link ResourceTypeVersionTunabilityEnum} objects.
     */
    public ResourceTypeVersionTunabilityEnum[] getDowngradeTunabilities();

    /**
     * Get the List of system properties that are associated with this resource
     * type.
     *
     * @return  a <code>List</code> value containing keys with instances of
     * {@link com.sun.cluster.agent.rgm.property.ResourceProperty} subclasses.
     * The properties will not be initialized.
     */
    public java.util.List getSystemProperties();

    /**
     * Get the List of extension properties that are associated with this
     * resource type.
     *
     * @return  a <code>List</code> value containing keys with instances of
     * {@link com.sun.cluster.agent.rgm.property.ResourceProperty} subclasses.
     * The properties will not be initialized.
     */
    public java.util.List getExtProperties();

    /**
     * Un-register this resource type on the cluster.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] unregister() throws CommandExecutionException;

}
