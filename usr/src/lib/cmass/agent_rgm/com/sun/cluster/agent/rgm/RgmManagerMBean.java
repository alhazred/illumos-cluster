/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RgmManagerMBean.java	1.16	08/09/22 SMI"
 */

package com.sun.cluster.agent.rgm;

// JDK
import java.util.List;
import java.util.Map;
import java.util.Set;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available to handle a set of
 * remote Cluster Resource Groups, Resources or Resource Types.
 */
public interface RgmManagerMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public final static String TYPE = "rgmManager";

    /**
     * This method creates a new instance of a Resource with its default system
     * values.
     *
     * @param  name  the name of the resource. This name should be unique and
     * can not be modified after the creation of the Resource.
     * @param  rgName  the name of the resource group owning this new resource.
     * @param  type  the resource type used for the creation of this resource.
     * @param  systemProperties  the system properties associated to this
     * resource (might be null).
     * @param  extProperties  the extended properties associated to this
     * resource (might be null).
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addResource(String name, String rgName, String type,
        List systemProperties, List extProperties, boolean commit)
        throws CommandExecutionException;

    /**
     * This method register a new resource type so that it can be used by the
     * user.
     *
     * @param  name  the name of the reosurce type to register.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] registerResourceType(String name)
        throws CommandExecutionException;

    /**
     * This method gets all project names on the current node of the given
     * zonecluster. Current node is the node on which this mbean is running.
     *
     * @return  an {@link Set} object.
     */
    public Set getProjectNames(String zoneClusterName);

    /**
     * Suspend resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] suspendRGs(String zoneClusterName,
                                String rgs) throws CommandExecutionException;

    /**
     * Fast suspend resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] fastsuspendRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException;

    /**
     * Resume suspended resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] resumeRGs(String zoneClusterName, String rgs)
            throws CommandExecutionException;

    /**
     * Remaster resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] remasterRGs(String zoneClusterName, String rgs)
            throws CommandExecutionException;

    /**
     * Restart resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] restartRGs(String zoneClusterName, String rgs)
            throws CommandExecutionException;


    /**
     * Restart resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeRestartRGs(String zoneClusterName,
            String rgNames, String nodeList)
        throws CommandExecutionException;


    /**
     * Force delete resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] forceDeleteRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException;

    /**
     * Bring resource groups Online.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOnlineRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException;

    /**
     * Bring Online resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeBringOnlineRGs(String zoneClusterName,
            String rgNames, String nodeList)
        throws CommandExecutionException;

    /**
     * Bring resource groups Offline.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOfflineRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException;

    /**
     * Bring Offline resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeBringOfflineRGs(String zoneClusterName,
            String rgNames, String nodeList)
        throws CommandExecutionException;


    /**
     * Manage resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] manageRGs(String zoneClusterName, String rgs)
            throws CommandExecutionException;

    /**
     * UnManage resource groups.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] unmanageRGs(String zoneClusterName, String rgs)
        throws CommandExecutionException;

    /**
     * Enable resources.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] enableRs(String zoneClusterName, String rs)
            throws CommandExecutionException;

    /**
     * Disable resources.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] disableRs(String zoneClusterName, String rs)
            throws CommandExecutionException;

    /**
     * Start Monitors for resources.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] startMonitorRs(String zoneClusterName, String rs)
        throws CommandExecutionException;

    /**
     * Stop Monitors for resources.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] stopMonitorRs(String zoneClusterName, String rs)
        throws CommandExecutionException;

    /**
     * Enable resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeEnableRs(String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException;

    /**
     * Disable resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeDisableRs(String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException;

    /**
     * Start Monitors for resource on a node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeStartMonitorRs(String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException;

    /**
     * Stop Monitors for resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeStopMonitorRs(String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] suspendRGs(String rgs) throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] fastsuspendRGs(String rgs)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] resumeRGs(String rgs) throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] restartRGs(String rgs) throws CommandExecutionException;


    /**
     * @deprecated
     */
    public ExitStatus[] forceDeleteRGs(String rgs)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] bringOnlineRGs(String rgs)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] bringOfflineRGs(String rgs)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] manageRGs(String rgs)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] unmanageRGs(String rgs)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] enableRs(String rs) throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] disableRs(String rs) throws CommandExecutionException;


    /**
     * @deprecated
     */
    public ExitStatus[] startMonitorRs(String rs)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] stopMonitorRs(String rs)
        throws CommandExecutionException;

    /**
     * @deprecated use RgGroupMBean.addResourceGroup().
     */
    public ExitStatus[] addResourceGroup(String name, Map properties,
        boolean commit) throws CommandExecutionException;


    /**
     * @deprecated
     */
    public ExitStatus[] pernodeEnableRs(String rsName, String nodeList)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] pernodeDisableRs(String rsName, String nodeList)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] pernodeStartMonitorRs(String rsName, String nodeList)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public ExitStatus[] pernodeStopMonitorRs(String rsName, String nodeList)
        throws CommandExecutionException;

    /**
     * @deprecated
     */
    public Set getProjectNames();

    /**
     * Called by Odyssey to invalidate the cache. This is not a good approach,
     * must be replaced with a event that says, "my cache has changed". Odyssey
     * can listen to this event and perform actions based on it.
     */
    public void refreshResourceCache();

    /**
     * Called by Odyssey to invalidate the cache. This is not a good approach.
     * much be replaced with a event that says, "my cache has changed". Odyssey
     * can listen to this event and perform actions based on it.
     */
    public void refreshResourceGroupCache();

}
