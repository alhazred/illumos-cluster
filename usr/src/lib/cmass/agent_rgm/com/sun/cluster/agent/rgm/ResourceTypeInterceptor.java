/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceTypeInterceptor.java 1.16     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm;

// JDK
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX 
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.ReflectionException;

// JDMK
import com.sun.jdmk.JdmkMBeanServer;
import com.sun.jdmk.ServiceName;
import com.sun.jdmk.interceptor.MBeanServerInterceptor;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.agent.VirtualMBeanInterceptor;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.ClusterPaths;


/**
 * CMASS interceptor for the resource type MBean.  instance and attribute values
 * are fetched through JNI, and all operations are performed through an
 * invocation of a CLI.
 */
public class ResourceTypeInterceptor extends VirtualMBeanInterceptor
    implements FileFilter {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.rgm");
    private final static String logTag = "ResourceTypeInterceptor";

    private Set instanceSet = null;
    private Set cachedDirectoryInstanceSet[];
    private String rtrDirectories[] = {
            ClusterPaths.SCRGADM_RTR_DIR, ClusterPaths.SCRGADM_OPT_RTR_DIR
        };
    private int rtrDirectoriesCount = rtrDirectories.length;
    private HashMap cacheTimestampMap;
    private HashMap dataMap = null;

    /**
     * Default Constructor.
     *
     * @param  dispatcher  DomainDispatcher that this virtual mbean interceptor
     * should register into for dispatching.
     */
    public ResourceTypeInterceptor(VirtualMBeanDomainDispatcher dispatcher) {

        super(dispatcher, ResourceTypeMBean.class);
        instanceSet = new HashSet();
        cacheTimestampMap = new HashMap();

        cachedDirectoryInstanceSet = new HashSet[rtrDirectoriesCount];

        /* Initialize the cacheTimestamp */
        for (int i = 0; i < rtrDirectoriesCount; i++) {
            cachedDirectoryInstanceSet[i] = new HashSet();
            cacheTimestampMap.put(rtrDirectories[i], new Long(0));
        }
    }

    /**
     * Called by the parent interceptor class
     *
     * @param  instanceName  a <code>String</code> value
     * @param  operationName  a <code>String</code> value
     * @param  params  an <code>Object[]</code> value
     * @param  signature  a <code>String[]</code> value
     *
     * @return  an <code>Object</code> value
     *
     * @exception  InstanceNotFoundException  if an error occurs
     * @exception  MBeanException  if an error occurs
     * @exception  ReflectionException  if an error occurs
     * @exception  IOException  if an error occurs
     */
    public Object invoke(String instanceName, String operationName,
        Object params[], String signature[]) throws InstanceNotFoundException,
        MBeanException, ReflectionException, IOException {

        logger.entering(logTag, "invoke : " + operationName, signature);

        if (!isRegistered(instanceName)) {
            throw new InstanceNotFoundException("cannot find the instance " +
                instanceName);
        }

        String commands[][] = null;

        if (operationName.equals("unregister")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RT_CMD, "unregister", instanceName }
                };

        } else {
            throw new ReflectionException(new IllegalArgumentException(
                    operationName));
        }

        // Run the command(s)
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(commands, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new MBeanException(new CommandExecutionException(
                    ie.getMessage(), ExitStatus.createArray(exits)));
        }

        logger.exiting(logTag, "invoke");

        return ExitStatus.createArray(exits);
    }

    private native Set getDirectoryInstances(String dirName);

    private native Set getRegisteredInstances();

    /**
     * Function to determine when to re-fill the cache
     *
     * @return  true if Cache is valid and can be used for requests and returns
     * false if Cache is not valid and must be refilled.
     */
    private boolean isCacheValid() {
        boolean cacheValid = true;

        try {
            long cacheTimestamp = 0;

            for (int i = 0; i < rtrDirectoriesCount; i++) {

                /* Check the directory timestamp */
                if (instanceSet != null) {
                    File directory = new File(rtrDirectories[i]);

                    cacheTimestamp =
                        ((Long) cacheTimestampMap.get(rtrDirectories[i]))
                        .longValue();

                    if (directory.lastModified() > cacheTimestamp) {
                        cacheTimestampMap.put(rtrDirectories[i],
                            new Long(directory.lastModified()));
                    } else {

                        /* Use ourselves as a filter to select newer files */
                        if (directory.listFiles(this).length < 1) {
                            continue;
                        }
                        /* cacheTimestamp has been updated by filter */
                    }
                }

                cacheValid = false;

                break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return cacheValid;
    }

    /**
     * Function for retrieving all the MBean instances by their names.
     *
     * @return  The array of instance names.
     */
    public String[] getInstances() {

        /*
         * It takes a lot of time to parse the instance names from the
         * files, so we only regenerate this list if we have to, which
         * we determine as being when the date stamp on the directory
         * containing instances has changed, or when we've received
         * a cluster event about resource types.
         *
         * The total set of instances is those from the directory
         * merged with those in the runtime. There are no events sent
         * when the runtime configuration changes, so we have to get
         * these names dynamically each time, and not cache them.
         */

        try {
            Set tmpInstanceSet = null;
            long cacheTimestamp = 0;
            boolean anyDirectoryModified = false;

            for (int i = 0; i < rtrDirectoriesCount; i++) {

                /* Check the directory timestamp */
                if (instanceSet != null) {
                    File directory = new File(rtrDirectories[i]);

                    cacheTimestamp =
                        ((Long) cacheTimestampMap.get(rtrDirectories[i]))
                        .longValue();

                    if (directory.lastModified() > cacheTimestamp) {
                        cacheTimestampMap.put(rtrDirectories[i],
                            new Long(directory.lastModified()));
                    } else {

                        /* Use ourselves as a filter to select newer files */
                        if (directory.listFiles(this).length < 1) {
                            continue;
                        }
                        /* cacheTimestamp has been updated by filter */
                    }
                }

                anyDirectoryModified = true;
                tmpInstanceSet = getDirectoryInstances(rtrDirectories[i]);

                if (tmpInstanceSet != null) {
                    cachedDirectoryInstanceSet[i] = tmpInstanceSet;
                } else {
                    logger.severe("Error in getDirectoryInstances:" +
                        " tmpInstanceSet was null.");
                }
            }

            /*
             * If any directory is recently modified, clear the
             * instanceSet and update it with all the directory's
             * cachedDirectoryInstanceSet.
             */
            if (anyDirectoryModified == true) {
                instanceSet.clear();

                for (int i = 0; i < rtrDirectoriesCount; i++) {
                    instanceSet.addAll(cachedDirectoryInstanceSet[i]);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
         * Return an array containing the directory set merged with
         * the runtime set
         */
        Set resultSet = getRegisteredInstances();
        resultSet.addAll(instanceSet);

        return (String[]) resultSet.toArray(new String[] {});
    }

    /**
     * Function called by the getAttributes function which is part of the
     * MBeanInterceptor interface for retrieving all the attributes values of an
     * MBean specified by its instance name. This is done in the sub-class as it
     * can be specific to the MBean type.
     *
     * @param  name  The name of the MBean from which the attributes are
     * retrieved.
     * @param  attributes  A list of the attributes to be retrieved.
     *
     * @return  The list of the retrieved attributes.
     *
     * @throws  InstanceNotFoundException  error occurs.
     * @throws  ReflectionException  An error occurs.
     * @throws  IOException  An error occurs.
     */
    public AttributeList getAttributes(String name, String attributes[])
        throws InstanceNotFoundException, ReflectionException, IOException {
        AttributeList retList = new AttributeList();
        HashMap instanceMap = null;

        if ((dataMap == null) || (isCacheValid() == false)) {
            dataMap = (HashMap) fillCacheNative(rtrDirectories);
        }

        instanceMap = (HashMap) dataMap.get(name);

        if (instanceMap == null) {
            throw new InstanceNotFoundException();
        }

        /*
         *      "Registered" and "InstalledNodeNames" cannot be cached, because
         *      there is no cluster event generated when these properties
         *      change. These two values will be calculated whenever a client
         *      asks for it
         */

        for (int i = 0; i < attributes.length; i++) {
            String attrName = attributes[i];
            Attribute property = null;

            if (attrName.equals("Registered") ||
                    attrName.equals("InstalledNodeNames")) {
                property = getAttributeNative(name, attrName);
            } else {
                property = (Attribute) instanceMap.get(attrName);
            }

            retList.add(property);
        }

        return retList;
    }

    /**
     * Function called by the isRegistered function for checking whether an
     * MBean specified by its instance name is already registered with the MBean
     * server. This is done in the sub-class as it can be specific to the MBean
     * type.
     *
     * @param  name  The name of the MBean to be checked.
     *
     * @return  True if the MBean is already registered in the MBean server,
     * false otherwise.
     *
     * @throws  IOException  An error occurs
     */
    public boolean isRegistered(String name) throws IOException {
        return java.util.Arrays.asList(getInstances()).contains(name);
    }

    /**
     * Method used internally for filtering files based upon modification date
     */
    public boolean accept(File pathname) {

        // If we've already found a more recent file, short-cut
        if (instanceSet == null)
            return false;

        try {
            long lastModified = pathname.lastModified();
            String rtrDirPath = pathname.getParent();
            long cacheTimestamp = ((Long) cacheTimestampMap.get(rtrDirPath))
                .longValue();

            if (lastModified > cacheTimestamp) {
                cacheTimestamp = lastModified;
                cacheTimestampMap.put(rtrDirPath, new Long(cacheTimestamp));

                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private native synchronized HashMap fillCacheNative(String dirNames[]);

    private native Attribute getAttributeNative(String instanceName,
        String attributeName);

}
