/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RgGroupMBean.java	1.6	08/12/03 SMI"
 */
package com.sun.cluster.agent.rgm;

import java.util.HashMap;
import java.util.Map;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;

/**
 * MBean Interface holding utility functions to read ResourceGroup data
 */
public interface RgGroupMBean {

    /**
     * This field is used internally by the CMASS agent to identify the
     * TYPE of the module.
     */
    public static final String TYPE = "RgGroup";

    /**
     * Utility method to retrieve data corresponding to ResourceGroups.
     *
     * @param zoneClusterName for which the query is to be done. If this is
     *        null, the method would return all the ResourceGroups present in
     *        the cluster it is run.
     * @return HashMap with following structure
     *                  KEY                     VALUE
     *                  ResourceGroupName       ResourceGroupData Object
     */
    public HashMap readData(String zoneClusterName);

    /**
     * This method gets an instance of ResourceGroupData matching the
     * resource group name in the given zonecluster.
     *
     * @param name the name of the resource group
     * @param zoneClusterName the name of the zone cluster or null for a base
     * cluster.
     * @return a ResourceGroupData object
     */
    public ResourceGroupData getResourceGroup(String name,
            String zoneClusterName);

    /**
     * This method creates a new instance of a Resource Group with its default
     * system values
     *
     * @param  name  the name of the resource group. This name should be unique
     * and can not be modified after the creation of the Resource Group.
     * @param  properties  the properties associated to this resource group
     * (might be null).
     * @param zoneClusterName name of the zone cluster.
     * If null, assume current scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addResourceGroup(String name, Map properties,
	String zoneClusterName, boolean commit)
	throws CommandExecutionException;

    /**
     * This method checks if there is already a resource group by the specified
     * name.
     * @param name the name of the resource group.
     * @param zoneClusterName the name of the zone cluster or null for current
     * cluster scope.
     * @return a boolean; true if the resource group name already exists and
     * false otherwise.
     */
    public boolean isResourceGroupRegistered(String name,
            String zoneClusterName);

    /**
     * Change a set of properties for this resource group.
     *
     * @param name the name of the resource group
     * @param  newProperties  the new properties associated to this resource
     * group.
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeProperties(String name, Map newProperties,
        String zoneClusterName, boolean commit)
        throws CommandExecutionException;

    /**
     * Change resource group's dependencies on other resource groups.
     *
     * @param name the name of the resource group
     * @param  newDependencies  the new set of resource group's dependencies.
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeDependencies(String name,
            String newDependencies[],
	String zoneClusterName, boolean commit)
        throws CommandExecutionException;

    /**
     * Stop and restart this resource group.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] restart(String name, String zoneClusterName)
	throws CommandExecutionException;

    /**
     * Delete this resource group.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] remove(String name, String zoneClusterName)
	throws CommandExecutionException;

    /**
     * Put this resource group offline.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOffline(String name, String zoneClusterName)
        throws CommandExecutionException;

    /**
     * Put this resource group online.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOnline(String name, String zoneClusterName)
        throws CommandExecutionException;

    /**
     * Put this resource group online.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] bringOnline(String name, String zoneClusterName,
	boolean commit)
        throws CommandExecutionException;

    /**
     * Put this resource group in an 'managed' state.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] manage(String name, String zoneClusterName)
	throws CommandExecutionException;

    /**
     * Put this resource group in an 'unmanaged' state.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] unmanage(String name, String zoneClusterName)
	throws CommandExecutionException;

    /**
     * Suspend this resource groups.
     *
     * @param name the name of the resource group
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] suspend(String name, String zoneClusterName)
	throws CommandExecutionException;

    /**
     * Switch the primaries of this resource group.
     *
     * @param name the name of the resource group
     * @param  newPrimaries  an array of node names.
     *
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] switchPrimaries(String name, String newPrimaries[],
            String zoneClusterName)
        throws CommandExecutionException;


}
