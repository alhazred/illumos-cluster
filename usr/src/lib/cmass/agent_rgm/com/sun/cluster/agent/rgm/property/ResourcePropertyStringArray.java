/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourcePropertyStringArray.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm.property;

/**
 * Object describing a string resource property, with built-in range and type
 * information and checking.
 */
public class ResourcePropertyStringArray extends ResourceProperty {

    private Integer minLength;
    private Integer maxLength;

    private Integer arrayMinSize;
    private Integer arrayMaxSize;

    private String defaultValue[];
    private String value[];

    public ResourcePropertyStringArray() {
        super();
    }

    public ResourcePropertyStringArray(String name, boolean extension,
        boolean per_node, boolean required, String description,
        ResourcePropertyEnum tunable, Integer minLength, Integer maxLength,
        Integer arrayMinSize, Integer arrayMaxSize, String defaultValue[],
        String value[]) {

        super(name, extension, per_node, required, description, tunable);

        this.minLength = minLength;
        this.maxLength = maxLength;

        this.arrayMinSize = arrayMinSize;
        this.arrayMaxSize = arrayMaxSize;

        this.defaultValue = defaultValue;

        if (value != null) {
            setValue(value);
        }
    }

    public ResourcePropertyStringArray(String name, String value[]) {

        super(name, false, false, false, "Immutable",
            new Tunable(Tunable.FALSE));

        this.minLength = null;
        this.maxLength = null;

        this.arrayMinSize = null;
        this.arrayMaxSize = null;

        this.defaultValue = null;

        if (value != null) {
            setValue(value);
        }
    }

    public Integer getMinLength() {
        return minLength;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public Integer getArrayMinSize() {
        return arrayMinSize;
    }

    public Integer getArrayMaxSize() {
        return arrayMaxSize;
    }

    public String[] getDefaultValue() {
        return defaultValue;
    }

    public String[] getValue() {
        return value;
    }

    public void setMinLength(Integer minLength) {
        this.minLength = minLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public void setArrayMinSize(Integer arrayMinSize) {
        this.arrayMinSize = arrayMinSize;
    }

    public void setArrayMaxSize(Integer arrayMaxSize) {
        this.arrayMaxSize = arrayMaxSize;
    }

    public void setDefaultValue(String defaultValue[]) {
        this.defaultValue = defaultValue;
    }

    public void setValue(String newValue[]) {

        boolean isValid = true;

        if ((newValue == null) ||
                ((arrayMinSize != null) &&
                    (newValue.length < arrayMinSize.intValue())) ||
                ((arrayMaxSize != null) &&
                    (newValue.length > arrayMaxSize.intValue()))) {
            isValid = false;
        } else {

            for (int i = 0; i < newValue.length; i++) {

                if (newValue[i] != null) {
                    int len = newValue[i].length();

                    if (((minLength != null) && (len < minLength.intValue())) ||
                            ((maxLength != null) &&
                                (len > maxLength.intValue()))) {
                        isValid = false;
                    }
                }
            }
        }

        if (!isValid) {
            throw new IllegalArgumentException("illegal value:" + newValue);
        }

        this.value = newValue;
    }

    public String toString() {
        String val = null;
        String defaultVal = null;

        if (value != null) {
            val = "{";

            for (int i = 0; i < value.length; i++) {

                if (i != 0)
                    val = val.concat(",");

                if (value[i] == null) {
                    val = val.concat("null");
                } else {
                    val = val.concat(value[i]);
                }
            }

            val = val.concat("}");
        } else {
            val = "null";
        }

        if (defaultValue != null) {
            defaultVal = new String("{");

            for (int i = 0; i < defaultValue.length; i++) {

                if (i != 0)
                    defaultVal = defaultVal.concat(",");

                if (defaultValue[i] == null) {
                    defaultVal = defaultVal.concat("null");
                } else {
                    defaultVal = defaultVal.concat(defaultValue[i]);
                }
            }

            defaultVal = defaultVal.concat("}");
        } else {
            defaultVal = new String("null");
        }

        return super.toString() + " MIN_LENGTH=" + minLength + " MAX_LENGTH=" +
            maxLength + " ARRAY_MIN_SIZE=" + arrayMinSize + " ARRAY_MAX_SIZE=" +
            arrayMaxSize + " DEFAULT=" + defaultVal + " VALUE=" + val + ">";
    }


}
