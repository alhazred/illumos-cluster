/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RgmModule.java	1.12	08/07/14 SMI"
 */

package com.sun.cluster.agent.rgm;

// JDK
import java.util.Map;
import java.util.logging.Logger;

// JMX
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// Local
import com.sun.cluster.common.ClusterRuntimeException;
import com.sun.cluster.agent.rgm.property.*;


/**
 * Core implementation of the RGM module. This module is using CACAO's
 * interceptor and JNI design pattern for subsequent calls to the SCHA API.
 */
public class RgmModule extends Module {

    // Tracing utility.
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.rgm");
    private final static String logTag = "RgmModule";

    // Our dispatcher.
    private VirtualMBeanDomainDispatcher dispatcher;

    // Our interceptors.
    private ResourceGroupInterceptor resourceGroupInterceptor;
    private ResourceInterceptor resourceInterceptor;
    private ResourceTypeInterceptor resourceTypeInterceptor;

    // Our instances manager
    private RgmManager rgmManager;

    /**
     * Constructor called by the container
     *
     * @param  descriptor  the deployment descriptor used to load the module
     */
    public RgmModule(DeploymentDescriptor descriptor) {
        super(descriptor);
        logger.entering(logTag, "<init>", descriptor);

        Map parameters = descriptor.getParameters();

        try {

            // Load the agent JNI library
            String library = (String) parameters.get("library");
            Runtime.getRuntime().load(library);

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to load native runtime library" + ule);
            throw ule;
        }

        logger.exiting(logTag, "<init>");
    }

    /**
     * Invoked by the container when unlocking the module.
     *
     * @exception  ClusterRuntimeException  if unable to start.
     */
    protected void start() throws ClusterRuntimeException {
        logger.entering(logTag, "start");

        ObjectNameFactory objectNameFactory = new ObjectNameFactory(
                getDomainName());

        logger.finest("create VirtualMBeanDomainDispatcher");
        dispatcher = new VirtualMBeanDomainDispatcher(getMbs(),
                objectNameFactory);

        try {
            logger.fine("unlock dispatcher");
            dispatcher.unlock();

            logger.finest("create ResourceGroupInterceptor");
            resourceGroupInterceptor = new ResourceGroupInterceptor(getMbs(),
                    dispatcher, objectNameFactory);
            logger.fine("unlock ResourceGroupInterceptor");
            resourceGroupInterceptor.unlock();

            logger.finest("create ResourceInterceptor");
            resourceInterceptor = new ResourceInterceptor(getMbs(), dispatcher,
                    objectNameFactory);
            logger.fine("unlock ResourceInterceptor");
            resourceInterceptor.unlock();

            logger.finest("create ResourceTypeInterceptor");
            resourceTypeInterceptor = new ResourceTypeInterceptor(dispatcher);
            logger.fine("unlock ResourceTypeInterceptor");
            resourceTypeInterceptor.unlock();

            logger.finest("create RgmManager");
            rgmManager = new RgmManager(getMbs(), objectNameFactory);
            logger.fine("register RgmManager");
            getMbs().registerMBean(rgmManager,
                objectNameFactory.getObjectName(RgmManagerMBean.class, null));
            rgmManager.setResourceInterceptor(resourceInterceptor);
            rgmManager.setResourceGroupInterceptor(resourceGroupInterceptor);

	    logger.finest("create RgGroup");
	    RgGroup rgGroup = new RgGroup(getMbs());
	    logger.fine("register RgGroup");
	    getMbs().registerMBean(rgGroup,
		objectNameFactory.getObjectName(RgGroupMBean.class,
		null));

	    logger.finest("create RGroup");
	    RGroup rGroup = new RGroup(getMbs());
	    logger.fine("register RGroup");
	    getMbs().registerMBean(rGroup,
		objectNameFactory.getObjectName(RGroupMBean.class,
		null));

            logger.finest("create RtGroup");
	    RtGroup rtGroup = new RtGroup(getMbs());
	    logger.fine("register RtGroup");
	    getMbs().registerMBean(rtGroup,
		objectNameFactory.getObjectName(RtGroupMBean.class,
		null));

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to start RGM module : " + e.getMessage());
            throw new ClusterRuntimeException(e.getMessage());

        } finally {
            logger.exiting(logTag, "start");
        }
    }

    /**
     * Invoked by the container when locking the module.
     */
    protected void stop() {
        logger.entering(logTag, "stop");

        try {
            logger.fine("lock dispatcher");
            dispatcher.lock();
            logger.fine("lock ResourceGroupInterceptor");
            resourceGroupInterceptor.lock();
            logger.fine("lock ResourceInterceptor");
            resourceInterceptor.lock();
            logger.fine("lock ResourceTypeInterceptor");
            resourceTypeInterceptor.lock();
            logger.fine("unregister RgmManager");

            ObjectNameFactory objectNameFactory = new ObjectNameFactory(
                    getDomainName());
            getMbs().unregisterMBean(objectNameFactory.getObjectName(
                    RgmManagerMBean.class, null));
	    getMbs().unregisterMBean(
		objectNameFactory.getObjectName(RgGroupMBean.class,
		null));
	    getMbs().unregisterMBean(
		objectNameFactory.getObjectName(RGroupMBean.class,
		null));
            getMbs().unregisterMBean(
		objectNameFactory.getObjectName(RtGroupMBean.class,
		null));

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to stop RGM module : " + e.getMessage());
            throw new ClusterRuntimeException(e.getMessage());

        } finally {
            dispatcher = null;
            resourceGroupInterceptor = null;
            resourceInterceptor = null;
            resourceTypeInterceptor = null;
            logger.exiting(logTag, "stop");
        }
    }
}
