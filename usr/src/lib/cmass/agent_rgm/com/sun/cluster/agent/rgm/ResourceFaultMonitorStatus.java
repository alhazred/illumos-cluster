/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceFaultMonitorStatus.java 1.6     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm;

/**
 * Struct-like object used to represent a resource's fault monitor status on an
 * identified node.
 */
public class ResourceFaultMonitorStatus implements java.io.Serializable {

    private String nodeName;
    private ResourceFaultMonitorStatusEnum status;
    private String statusMessage;

    /**
     * Default empty constructor
     */
    public ResourceFaultMonitorStatus() {
    }

    /**
     * Constructor with initial values.
     *
     * @param  nodeName  the identified node's name
     * @param  status  a {@link ResourceFaultMonitorStatusEnum} element
     * representing the resource's status on this node
     * @param  statusMessage  the message associated to the status
     */
    public ResourceFaultMonitorStatus(String nodeName,
        ResourceFaultMonitorStatusEnum status, String statusMessage) {
        this.nodeName = nodeName;
        this.status = status;
        this.statusMessage = statusMessage;
    }

    /**
     * Get the concerned node's name.
     *
     * @return  the node's name
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String getNodeName() {
        return this.nodeName;
    }

    /**
     * Get the resource's status on the concerned node.
     *
     * @return  the resource's status
     */
    public ResourceFaultMonitorStatusEnum getStatus() {
        return this.status;
    }

    /**
     * Get the resource's status message on the concerned node.
     *
     * @return  the resource's status message
     */
    public String getStatusMessage() {
        return this.statusMessage;
    }

    /**
     * Set the concerned node's name.
     *
     * @param  nodeName  the reference name of the node
     */
    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    /**
     * Set the resource's status.
     *
     * @param  status  a {@link ResourceFaultMonitorStatusEnum} element
     * representing the resource's status
     */
    public void setStatus(ResourceFaultMonitorStatusEnum status) {
        this.status = status;
    }

    /**
     * Set the resource's status message.
     *
     * @param  statusMessage  the message associated to the status
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

}
