/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RtGroupMBean.java	1.4	08/07/10 SMI"
 */
package com.sun.cluster.agent.rgm;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import java.util.HashMap;
import javax.management.Attribute;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;

/**
 * MBean Interface holding utility functions to read Resource Type Group data
 */
public interface RtGroupMBean {

    /**
     * This field is used internally by the CMASS agent to identify the
     * TYPE of the module.
     */
    public static final String TYPE = "RtGroup";

    /**
     * Utility method to retrieve data corresponding to ResourceTypes.
     *
     * @param zoneClusterName for which the query is to be done. If this is
     *        null, the method would return all the ResourceGroups present in
     *        the cluster it is run.
     * @param qAttribute If this is not null, return rtnames having an attribute
     * matching this one.
     * @return HashMap with following structure
     *                  KEY                     VALUE
     *                  ResourceTypeName       ResourceTypeData Object
     */
    public HashMap readData(String zoneClusterName, Attribute qAttribute);


    /**
     * Function called by the isRegistered function for checking whether an
     * MBean specified by its instance name is already registered with the MBean
     * server. This is done in the sub-class as it can be specific to the MBean
     * type.
     *
     * @param  name  The name of the MBean to be checked.
     *
     * @param zoneClusterName
     * @return  True if the MBean is already registered in the MBean server,
     * false otherwise.
     */
    public boolean isRegistered(String name, String zoneClusterName);


    /**
     * Utility method to unregister given rt name
     *
     * @param  rtName  a <code>String</code> value for resource type name to be
     * unregistered
     * @param zoneClusterName
     * @return Exitstatus[]
     * @throws javax.management.MBeanException
     * @throws javax.management.InstanceNotFoundException
     */
    public ExitStatus[] unregisterResourceType(
            String rtName, String zoneClusterName)
            throws InstanceNotFoundException, MBeanException;


    /**
     * This method register a new resource type so that it can be used by the
     * user.
     *
     * @param rType the name of the resource type to register.
     * @param zoneClusterName the name of the zone cluster
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] registerResourceType(String rType,
            String zoneClusterName, boolean commit)
        throws CommandExecutionException;

    /**
     * This method register a new resource type so that it can be used by the
     * user.
     *
     * @param rType the name of the resource type to register.
     * @param zoneClusterName the name of the zone cluster
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] registerResourceType(String rType,
            String zoneClusterName) throws CommandExecutionException;
}
