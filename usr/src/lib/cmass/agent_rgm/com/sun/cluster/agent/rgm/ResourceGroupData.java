/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceGroupData.java	1.5	08/07/10 SMI"
 */
package com.sun.cluster.agent.rgm;

/**
 * ResourceGroupData class
 */
public class ResourceGroupData implements java.io.Serializable {

    public final static String SYSTEM = "RG_system";
    public final static String DESCRIPTION = "RG_description";
    public final static String AUTO_START = "Auto_start_on_new_cluster";
    public final static String FAILBACK = "Failback";
    public final static String FAILURE_INTERVAL = "Pingpong_interval";
    public final static String SHARED_STORAGE_USED = "Global_resources_used";
    public final static String PATH_PREFIX = "Pathprefix";
    public final static String PROJECT_NAME = "RG_project_name";
    public final static String SLM_TYPE = "RG_SLM_type";
    public final static String SLM_PSET_TYPE = "RG_SLM_Pset_type";
    public final static String SLM_CPU_SHARES = "RG_SLM_CPU_Shares";
    public final static String SLM_PSET_MIN = "RG_SLM_Pset_Min";
    public final static String PRIMARIES = "Nodelist";
    public final static String AFFINITIES = "RG_affinities";
    public final static String DESIRED = "Desired_primaries";
    public final static String MAX = "Maximum_primaries";
    public final static String DEPEND = "RG_dependencies";
    public final static String NETWORKS_DEPEND =
            "Implicit_network_dependencies";

    public final static String GLOBAL = "global";
    public final static String BASE = "base";

    private String name;
    private ResourceGroupStatusEnum overallStatus;
    private ResourceGroupStatus[] status;
    private boolean managed;
    private boolean online;
    private ResourceGroupModeEnum mode;
    private boolean autoStartable;
    private boolean networkDependent;
    private boolean system;
    private boolean failbackEnabled;
    private boolean sharedStorageUsed;
    private boolean suspended;
    private String[] nodeNames;
    private String[] currentPrimaries;
    private int currentPrimariesCount;
    private int desiredPrimariesCount;
    private int maxPrimaries;
    private String[] dependencies;
    private int dependenciesCount;
    private String directoryPath;
    private String description;
    private int failureInterval;
    private String[] affinities;
    private String projectName;
    private String slmType;
    private String slmpSetType;
    private int slmCPUShares;
    private int slmpSetMin;
    private String[] resourceNames;

    public String[] getAffinities() {
        return affinities;
    }

    public boolean isAutoStartable() {
        return autoStartable;
    }

    public String[] getCurrentPrimaries() {
        return currentPrimaries;
    }

    public int getCurrentPrimariesCount() {
        return currentPrimariesCount;
    }

    public String[] getDependencies() {
        return dependencies;
    }

    public int getDependenciesCount() {
        return dependenciesCount;
    }

    public String getDescription() {
        return description;
    }

    public int getDesiredPrimariesCount() {
        return desiredPrimariesCount;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public boolean isFailbackEnabled() {
        return failbackEnabled;
    }

    public int getFailureInterval() {
        return failureInterval;
    }

    public boolean isManaged() {
        return managed;
    }

    public int getMaxPrimaries() {
        return maxPrimaries;
    }

    public ResourceGroupModeEnum getMode() {
        return mode;
    }

    public String getName() {
        return name;
    }

    public boolean isNetworkDependent() {
        return networkDependent;
    }

    public String[] getNodeNames() {
        return nodeNames;
    }

    public boolean isOnline() {
        return online;
    }

    public ResourceGroupStatusEnum getOverallStatus() {
        return overallStatus;
    }

    public String getProjectName() {
        return projectName;
    }

    public String[] getResourceNames() {
        return resourceNames;
    }

    public boolean isSharedStorageUsed() {
        return sharedStorageUsed;
    }

    public int getSLMCPUShares() {
        return slmCPUShares;
    }

    public String getSLMType() {
        return slmType;
    }

    public int getSLMPsetMin() {
        return slmpSetMin;
    }

    public String getSLMPsetType() {
        return slmpSetType;
    }

    public ResourceGroupStatus[] getStatus() {
        return status;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public boolean isSystem() {
        return system;
    }



    public ResourceGroupData() {
    }


    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("[name= " + name);
        s.append(", managed= " + managed);
        s.append(", online= " + online);
        s.append(", nodeNames= " + convertStringAr(nodeNames));
        s.append(", currentPrimaries= " +
                convertStringAr(currentPrimaries));
        s.append(", currentPrimariesCount= " + currentPrimariesCount);
        s.append(", description= " + description);
        s.append(", resourceNames= " + convertStringAr(resourceNames));
        s.append(", dependencies= " + convertStringAr(dependencies));
        s.append(", affinities= " + convertStringAr(affinities));
        s.append("]\n");
        return s.toString();
    }

    // This utility method would go in ModelBase class once it gets created in
    // the main gate.
    public static String convertStringAr(String[] inputList) {
        String delimiter = ", ";
        StringBuffer tmpStr = new StringBuffer();
        tmpStr.append("[");
        if (inputList != null) {
            for (int i = 0; i < inputList.length; i++) {
                if (i > 0) {
                    tmpStr.append(delimiter);
                }
                tmpStr.append(inputList[i]);
            }
        }
        tmpStr.append("]");
        return tmpStr.toString();
    }

}
