/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceRgmStateEnum.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm;

/**
 * The Resource RGM State attribute enum as defined in the SCHA API
 */
public class ResourceRgmStateEnum extends com.sun.cacao.Enum
    implements java.io.Serializable {

    public static final ResourceRgmStateEnum ONLINE = new ResourceRgmStateEnum(
            "ONLINE", 0);

    public static final ResourceRgmStateEnum OFFLINE = new ResourceRgmStateEnum(
            "OFFLINE");

    public static final ResourceRgmStateEnum START_FAILED =
        new ResourceRgmStateEnum("START_FAILED");

    public static final ResourceRgmStateEnum STOP_FAILED =
        new ResourceRgmStateEnum("STOP_FAILED");

    public static final ResourceRgmStateEnum MONITOR_FAILED =
        new ResourceRgmStateEnum("MONITOR_FAILED");

    public static final ResourceRgmStateEnum ONLINE_NOT_MONITORED =
        new ResourceRgmStateEnum("ONLINE_NOT_MONITORED");

    public static final ResourceRgmStateEnum STARTING =
        new ResourceRgmStateEnum("STARTING");

    public static final ResourceRgmStateEnum STOPPING =
        new ResourceRgmStateEnum("STOPPING");

    public static final ResourceRgmStateEnum DETACHED =
        new ResourceRgmStateEnum("DETACHED");

    private ResourceRgmStateEnum(String name) {
        super(name);
    }

    private ResourceRgmStateEnum(String name, int ord) {
        super(name, ord);
    }
}
