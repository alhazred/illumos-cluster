/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceMBean.java	1.8	08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;

import java.util.HashMap;
import java.util.List;


/**
 * MBean interface for describing resource instances
 */
public interface ResourceMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public final static String TYPE = "resource";

    /** ID for system property START_TIMEOUT */
    public final static String START_TIMEOUT = "START_TIMEOUT";

    /** ID for system property STOP_TIMEOUT */
    public final static String STOP_TIMEOUT = "STOP_TIMEOUT";

    /** ID for system property VALIDATE_TIMEOUT */
    public final static String VALIDATE_TIMEOUT = "VALIDATE_TIMEOUT";

    /** ID for system property UPDATE_TIMEOUT */
    public final static String UPDATE_TIMEOUT = "UPDATE_TIMEOUT";

    /** ID for system property INIT_TIMEOUT */
    public final static String INIT_TIMEOUT = "INIT_TIMEOUT";

    /** ID for system property FINI_TIMEOUT */
    public final static String FINI_TIMEOUT = "FINI_TIMEOUT";

    /** ID for system property BOOT_TIMEOUT */
    public final static String BOOT_TIMEOUT = "BOOT_TIMEOUT";

    /** ID for system property MONITOR_START_TIMEOUT */
    public final static String MONITOR_START_TIMEOUT = "MONITOR_START_TIMEOUT";

    /** ID for system property MONITOR_STOP_TIMEOUT */
    public final static String MONITOR_STOP_TIMEOUT = "MONITOR_STOP_TIMEOUT";

    /** ID for system property MONITOR_CHECK_TIMEOUT */
    public final static String MONITOR_CHECK_TIMEOUT = "MONITOR_CHECK_TIMEOUT";

    /** ID for system property PRENET_START_TIMEOUT */
    public final static String PRENET_START_TIMEOUT = "PRENET_START_TIMEOUT";

    /** ID for system property POSTNET_STOP_TIMEOUT */
    public final static String POSTNET_STOP_TIMEOUT = "POSTNET_STOP_TIMEOUT";

    /** ID for system property FAILOVER_MODE */
    public final static String FAILOVER_MODE = "Failover_mode";

    /** ID for system property NETWORK_RESOURCES_USED */
    public final static String NETWORK_RESOURCES_USED =
        "Network_resources_used";

    /** ID for system property SCALABLE */
    public final static String SCALABLE = "Scalable";

    /** ID for system property PORT_LIST */
    public final static String PORT_LIST = "Port_list";

    /** ID for system property LOAD_BALANCING_POLICY */
    public final static String LOAD_BALANCING_POLICY = "Load_balancing_policy";

    /** ID for system property LOAD_BALANCING_WEIGHTS */
    public final static String LOAD_BALANCING_WEIGHTS =
        "Load_balancing_weights";

    /** ID for system property AFFINITY_TIMEOUT */
    public final static String AFFINITY_TIMEOUT = "Affinity_timeout";

    /** ID for system property UDP_AFFINITY */
    public final static String UDP_AFFINITY = "UDP_Affinity";

    /** ID for system property WEAK_AFFINITY */
    public final static String WEAK_AFFINITY = "Weak_Affinity";

    /** ID for system property CHEAP_PROBE_INTERVAL */
    public final static String CHEAP_PROBE_INTERVAL = "Cheap_probe_interval";

    /** ID for system property THOROUGH_PROBE_INTERVAL */
    public final static String THOROUGH_PROBE_INTERVAL =
        "Thorough_probe_interval";

    /** ID for system property RETRY_COUNT */
    public final static String RETRY_COUNT = "Retry_count";

    /** ID for system property RETRY_INTERVAL */
    public final static String RETRY_INTERVAL = "Retry_interval";

    /** ID for system property DESCRIPTION */
    public final static String DESCRIPTION = "R_description";

    /** ID for system property PROJECT_NAME */
    public final static String PROJECT_NAME = "Resource_project_name";

    /** ID for system property STRONG_DEPENDENCIES */
    public final static String STRONG_DEPENDENCIES = "Resource_dependencies";

    /** ID for system property WEAK_DEPENDENCIES */
    public final static String WEAK_DEPENDENCIES = "Resource_dependencies_weak";

    /** ID for system property RESTART_DEPENDENCIES */
    public final static String RESTART_DEPENDENCIES =
        "Resource_dependencies_restart";

    /** ID for system property OFFLINE_DEPENDENCIES */
    public final static String OFFLINE_DEPENDENCIES =
        "Resource_dependencies_offline_restart";

    public final static String NETWORK_AWARE = "Network_aware";

    /**
     * Get the resource's key name.
     *
     * @return  resource's key name.
     */
    public String getName();

    /**
     * Get the resource's overall state based on the state on each node. The
     * rule is as follows :<br>
     * {@link ResourceRgmStateEnum#OFFLINE} &lt;
     * {@link ResourceRgmStateEnum#STOPPING} &lt;
     * {@link ResourceRgmStateEnum#STARTING} &lt;
     * {@link ResourceRgmStateEnum#DETACHED} &lt;
     * {@link ResourceRgmStateEnum#ONLINE} &lt;
     * {@link ResourceRgmStateEnum#ONLINE_NOT_MONITORED} &lt;
     * {@link ResourceRgmStateEnum#MONITOR_FAILED} &lt;
     * {@link ResourceRgmStateEnum#START_FAILED} &lt;
     * {@link ResourceRgmStateEnum#STOP_FAILED}
     *
     * @return  a {@link ResourceRgmStateEnum} objects.
     */
    public ResourceRgmStateEnum getOverallState();

    /**
     * Get the resource's RGM states for each node listed in this resource's
     * group.
     *
     * @return  an array of {@link ResourceRgmState} object.
     */
    public ResourceRgmState[] getRgmStates();

    /**
     * Get the resource's Fault Monitor status for each node listed in this
     * resource's group.
     *
     * @return  an array of {@link ResourceFaultMonitorStatus} object.
     */
    public ResourceFaultMonitorStatus[] getFaultMonitorStatus();

    /**
     * Get the value string for the resource type
     *
     * @return  a <code>String</code> value containing the value for the
     * resource type key.
     */
    public String getType();

    /**
     * Get the value string for the resource type version
     *
     * @return  a <code>String</code> value containing the value for the
     * resource type version.
     */
    public String getTypeVersion();

    /**
     * Get resource's description
     *
     * @return  a <code>String</code> value containing the value for the
     * resource's description.
     */
    public String getDescription();

    /**
     * Get the value string for the resource group
     *
     * @return  a <code>String</code> value containing the value for the
     * resource group key
     *
     * @see  com.sun.cluster.agent.rgm.ResourceGroupMBean
     */
    public String getResourceGroupName();

    /**
     * Give the "enable" status of the resource.
     *
     * @return  <code>0/code> if the resource is disabled on all nodes, <code>
     * 1</code> if it's enabled on all the nodes, <code>2</code> if it is
     * enabled on some nodes and disabled on other nodes.
     */
    public int getEnabled();

    /**
     * Give the "monitored" status of the resource.
     *
     * @return  <code>0</code> if the resource is not monitored, <code>1</code>
     * if monitored on all the nodes, <code>2</code> if resource is monitored on
     * some nodes and not monitored on others.
     */
    public int getMonitored();

    /**
     * Give the enabled status of the resource on node basis.
     *
     * @return  <code>HashMap</code> containing the nodenames and the associated
     * enabled status.
     */
    public HashMap getPernodeEnabledStatus();

    /**
     * Give the monitoreded status of the resource on node basis.
     *
     * @return  <code>HashMap</code> containing the nodenames and the associated
     * monitored status.
     */
    public HashMap getPernodeMonitoredStatus();

    /**
     * Give the "detached" status of the resource.
     *
     * @return  <code>true</code> if the resource is detached, <code>
     * false</code> if not.
     */
    public boolean isDetached();

    /**
     * Get the Solaris Resource Management project name associated to this
     * resource.
     *
     * @return  the project name or <code>null</code> if unknown.
     */
    public String getProjectName();

    /**
     * Get the resource's weak dependencies to other resources.
     *
     * @return  an array or resource's names.
     */
    public String[] getWeakDependencies();

    /**
     * Get the resource's strong dependencies to other resources.
     *
     * @return  an array or resource's names.
     */
    public String[] getStrongDependencies();

    /**
     * Get the resource's restart dependencies to other resources.
     *
     * @return  an array or resource's names.
     */
    public String[] getRestartDependencies();

    /**
     * Get the resource's offline restart dependencies to other resources.
     *
     * @return  an array or resource's names.
     */
    public String[] getOfflineDependencies();

    /**
     * Get the List of system properties that are associated with this resource.
     *
     * @return  a <code>List</code> value containing keys with instances of
     * {@link com.sun.cluster.agent.rgm.property.ResourceProperty} subclasses;
     * the name of the system property is one of the constant defined in this
     * MBean; if a specific system property is not found in the returned List,
     * this means that this property is not defined for this resource's type.
     */
    public java.util.List getSystemProperties();

    /**
     * Get the List of per-node extension properties that are associated with
     * this resource.
     *
     * @return  a <code>List</code> value containing keys with instances of
     * {@link com.sun.cluster.agent.rgm.property.ResourceProperty} subclasses.
     */
    public java.util.List getExtProperties();

    /**
     * Get the List of extension properties that are associated with this
     * resource.
     *
     * @return  a <code>List</code> value containing the per-node extension
     * property names.
     */
    public java.util.List getPernodeExtProperties();

    /**
     * Change resource's description.
     *
     * @param  newDescription  the new description.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeDescription(String newDescription)
        throws CommandExecutionException;

    /**
     * Change resource's project name.
     *
     * @param  newName  the new name.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeProjectName(String newName)
        throws CommandExecutionException;

    /**
     * Change the resource's weak dependencies to other resources.
     *
     * @param  newWeakDependencies  the new dependencies.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeWeakDependencies(String newWeakDependencies[])
        throws CommandExecutionException;

    /**
     * Change the resource's strong dependencies to other resources.
     *
     * @param  newStrongDependencies  the new dependencies.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeStrongDependencies(String newStrongDependencies[])
        throws CommandExecutionException;

    /**
     * Change the resource's restart dependencies to other resources.
     *
     * @param  newRestartDependencies  the new dependencies.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeRestartDependencies(
        String newRestartDependencies[]) throws CommandExecutionException;

    /**
     * Change the resource's offline restart dependencies to other resources.
     *
     * @param  newOfflinerestartDependencies  the new dependencies.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeOfflineDependencies(
        String newOfflineDependencies[]) throws CommandExecutionException;


    /**
     * Change some of the resource's system properties.
     *
     * @param  newSystemProperties  a <code>List</code> value containing keys
     * with instances of
     * {@link com.sun.cluster.agent.rgm.property.ResourceProperty}.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeSystemProperties(List newSystemProperties,
        boolean commit) throws CommandExecutionException;

    /**
     * Change some of the resource's extend properties.
     *
     * @param  newExtProperties  a <code>List</code> value containing keys with
     * instances of {@link com.sun.cluster.agent.rgm.property.ResourceProperty}.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeExtProperties(List newExtProperties,
        boolean commit) throws CommandExecutionException;

    /**
     * Clear the Stop-Failed flag on the resource.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] clearStopFailedFlag() throws CommandExecutionException;

    /**
     * Enable the resource.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] enable() throws CommandExecutionException;

    /**
     * Disable the resource.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] disable() throws CommandExecutionException;

    /**
     * Start monitoring this resource.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] startMonitor() throws CommandExecutionException;

    /**
     * Stop monitoring this resource.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] stopMonitor() throws CommandExecutionException;

    /**
     * Enable the resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeEnable(String nodeList)
        throws CommandExecutionException;

    /**
     * Disable the resource on a per-node basis.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeDisable(String nodeList)
        throws CommandExecutionException;

    /**
     * Start monitoring this resource on the selected nodes.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeStartMonitor(String nodeList)
        throws CommandExecutionException;

    /**
     * Stop monitoring this resource on the selected nodes.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] pernodeStopMonitor(String nodeList)
        throws CommandExecutionException;

    /**
     * Delete this resource.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] remove() throws CommandExecutionException;

}
