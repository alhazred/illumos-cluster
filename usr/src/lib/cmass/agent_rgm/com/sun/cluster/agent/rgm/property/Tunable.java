/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)Tunable.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm.property;

/**
 * specific enum type used in resource property construction
 */
public class Tunable extends ResourcePropertyEnum
    implements java.io.Serializable {

    public final static String TRUE = "True";
    public final static String ANYTIME = "Anytime";
    public final static String NONE = "None";
    public final static String FALSE = "False";
    public final static String AT_CREATION = "At_creation";
    public final static String WHEN_DISABLED = "When_disabled";

    public Tunable(String value) {
        super("tunable", true, false, false,
            "indicates whether cluster admin " +
            "can set the value of this property " + "in a resource.", null,
            java.util.Arrays.asList(
                new String[] {
                    TRUE, ANYTIME, NONE, FALSE, AT_CREATION, WHEN_DISABLED
                }), "True", value, null);
    }

    public String toString() {
        return getValue();
    }
}
