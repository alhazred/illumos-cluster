/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceProperty.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.rgm.property;

/**
 * object describing a resource property, with built-in range and type
 * information and checking.
 */
public abstract class ResourceProperty implements java.io.Serializable {

    private String name;
    private boolean extension;
    private boolean per_node;
    private boolean required;
    private String description;
    private ResourcePropertyEnum tunable;

    public ResourceProperty() {
    }

    public ResourceProperty(String name, boolean extension, boolean per_node,
        boolean required, String description, ResourcePropertyEnum tunable) {
        this.name = name;
        this.extension = extension;
        this.per_node = per_node;
        this.required = required;
        this.description = description;
        this.tunable = tunable;
    }

    public String getName() {
        return name;
    }

    public boolean isExtension() {
        return extension;
    }

    public boolean isPernode() {
        return per_node;
    }

    public boolean isRequired() {
        return required;
    }

    public String getDescription() {
        return description;
    }

    public ResourcePropertyEnum getTunable() {
        return tunable;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExtension(boolean extension) {
        this.extension = extension;
    }

    public void setPernode(boolean per_node) {
        this.per_node = per_node;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTunable(ResourcePropertyEnum tunable) {
        this.tunable = tunable;
    }

    public String toString() {
        return " <NAME=" + name + " EXT=" + extension + " PER_NODE=" +
            per_node + " REQ=" + required + " DESC='" + description +
            "' TUNABLE=" + tunable;
    }
}
