/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RGroupMBean.java	1.6	08/07/10 SMI"
 */
package com.sun.cluster.agent.rgm;

import java.util.HashMap;
import java.util.List;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;

/**
 * MBean Interface holding utility functions to read Resource data
 */
public interface RGroupMBean {

    /**
     * This field is used internally by the CMASS agent to identify the
     * TYPE of the module.
     */
    public static final String TYPE = "RGroup";


    /**
     * Utility method to read data corresponding to Resources. This
     * method would return all the Resources corresponding to the given
     * ZoneCluster and ResourceGroup combination.
     *
     * @param zoneClusterName If this is null the method would return all the
     *                        resources present in the cluster it is run.
     * @param resourceGroupName If this is null the method would return all the
     *                          resources for the given ZoneCluster name.
     *
     * @return HashMap with following structure
     *         KEY                     VALUE
     *         ResourceGroupName       HashMap object with following structure
     *                                  KEY                 VALUE
     *                                  ResourceName        ResourceData Object
     */
    public HashMap readData(String zoneClusterName, String resourceGroupName);

    /**
     * Utility method to read ResourceData for a resource.
     *
     * @param name the name of the resource
     * @param zoneClusterName the name of the zone cluster
     * @return
     *
     */

    public ResourceData getResourceData(String name, String zoneClusterName);

    /**
     * Utility function to retrieve data corresponding to given resource of the
     * given type and with a particular property name and value.
     * The method would search the resource type in the given zonecluster.
     * @param zoneClusterName If this is null the resource search would be in
     *                        physical cluster or in the cluster in which this
     *                        method is run.
     * @param resourceType the resource type to search for
     * @param propertyName the property name to search for
     * @param propertyValue the property value to search for
     * @return list of resource names
     */
    public List getResourceList(
	String zoneClusterName,
	String resourceType,
	String propertyName,
	Object propertyValue);

    /*
     * This method creates a new instance of a Resource with its default system
     * values.
     *
     * @param  name  the name of the resource. This name should be unique and
     * can not be modified after the creation of the Resource.
     * @param  rgName  the name of the resource group owning this new resource.
     * @param  type  the resource type used for the creation of this resource.
     * @param  systemProperties  the system properties associated to this
     * resource (might be null).
     * @param  extProperties  the extended properties associated to this
     * resource (might be null).
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addResource(String name, String rgName, String type,
	String zoneClusterName, List systemProperties, List extProperties,
	boolean commit) throws CommandExecutionException;

    /**
     * This method checks if there is already a resource by the specified name.
     * @param name the name of the resource.
     * @param rgName the name of the resource group.
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @return a boolean; true if the resource name already exists and false
     * otherwise
     */
    public boolean isResourceRegistered(String name, String rgName,
            String zoneClusterName);

    /**
     * Change some of the resource's system properties.
     *
     * @param name the name of the resource
     * @param  newSystemProperties  a <code>List</code> value containing keys
     * with instances of
     * {@link com.sun.cluster.agent.rgm.property.ResourceProperty}.
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeSystemProperties(
            String name, List newSystemProperties,
	String zoneClusterName, boolean commit)
        throws CommandExecutionException;

    /**
     * Change some of the resource's extend properties.
     *
     * @param name the name of the resource
     * @param  newExtProperties  a <code>List</code> value containing keys with
     * instances of {@link com.sun.cluster.agent.rgm.property.ResourceProperty}.
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeExtProperties(String name, List newExtProperties,
	String zoneClusterName, boolean commit)
        throws CommandExecutionException;

    /**
     * Change the resource's strong dependencies to other resources.
     *
     * @param name the name of the resource
     * @param  newStrongDependencies  the new dependencies.
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeStrongDependencies(
            String name, String newStrongDependencies[],
	String zoneClusterName, boolean commit)
        throws CommandExecutionException;


    /**
     * Disable the resource.
     *
     * @param name the name of the resource
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] disable(String name, String zoneClusterName)
	throws CommandExecutionException;

    /**
     * Enable the resource.
     *
     * @param name the name of the resource
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] enable(String name, String zoneClusterName)
	throws CommandExecutionException;

    /**
     * Delete this resource.
     *
     * @param name the name of the resource
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] remove(String name, String zoneClusterName)
	throws CommandExecutionException;

    /**
     * Clear stop failed flag for the resource specified by 'name' present in
     * zonecluster specified by zoneClusterName.
     * @param name
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @return
     * @throws com.sun.cluster.agent.auth.CommandExecutionException
     */
    public ExitStatus[] clearStopFailedFlag(String name, String zoneClusterName)
            throws CommandExecutionException;
}
