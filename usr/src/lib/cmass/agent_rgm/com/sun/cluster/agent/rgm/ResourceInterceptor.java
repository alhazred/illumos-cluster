/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceInterceptor.java 1.20     09/03/27 SMI"
 */

package com.sun.cluster.agent.rgm;

// JDK
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerNotification;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;

// JDMK
import com.sun.jdmk.JdmkMBeanServer;
import com.sun.jdmk.ServiceName;
import com.sun.jdmk.interceptor.MBeanServerInterceptor;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.CachedVirtualMBeanInterceptor;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.event.ClEventDefs;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.event.SysEventNotifierMBean;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyEnum;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;
import com.sun.cluster.common.ClusterPaths;


/**
 * CMASS interceptor for the resource MBean. All instance and attribute values
 * are fetched through JNI, and all operations are performed through an
 * invocation of a CLI. This interceptor also listen for the events triggered by
 * the cluster for dynamic handle of resource's creation and deletion.
 */
public class ResourceInterceptor extends CachedVirtualMBeanInterceptor
    implements NotificationListener {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.rgm");
    private final static String logTag = "ResourceInterceptor";

    /* Internal reference to the SysEvent Notifier. */
    private ObjectName sysEventNotifier;

    /* The MBeanServer in which this interceptor is inserted. */
    private MBeanServer mBeanServer;

    /**
     * Default Constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this interceptor.
     * @param  dispatcher  DomainDispatcher that this virtual mbean interceptor
     * should register into for dispatching.
     * @param  onf  ObjectNameFactory implementation to be used by this
     * VirtualMBeanInterceptor
     */
    public ResourceInterceptor(MBeanServer mBeanServer,
        VirtualMBeanDomainDispatcher dispatcher, ObjectNameFactory onf) {
        super(mBeanServer, dispatcher, onf, ResourceMBean.class, null);
        logger.entering(logTag, "<init>",
            new Object[] { mBeanServer, dispatcher, onf });
        this.mBeanServer = mBeanServer;
        this.sysEventNotifier =
            new ObjectNameFactory("com.sun.cluster.agent.event").getObjectName(
                SysEventNotifierMBean.class, null);
        logger.exiting(logTag, "<init>");
    }

    /**
     * Called when the module is unlocked
     */
    public void unlock() throws Exception {
        super.unlock();
        logger.entering(logTag, "unlock");
        mBeanServer.addNotificationListener(sysEventNotifier, this, null, null);
        logger.exiting(logTag, "unlock");
    }

    /**
     * Called when the module is locked
     */
    public void lock() throws Exception {
        super.lock();
        logger.entering(logTag, "lock");
        mBeanServer.removeNotificationListener(sysEventNotifier, this, null,
            null);
        invalidateCache();
        logger.exiting(logTag, "lock");
    }

    /**
     * Called by Odyssey to invalidate the cache. This is not a approach,
     * must be replaced with a event that says, "my cache has changed". 
     * Odyssey can listen to this event and perform actions based on it.
     */
    public void refreshCache() {
        invalidateCache();
    }

    /**
     * The interceptor should monitor create/destroy/attribute-change events and
     * emit appropriate notifications from the mbean server (for create/destroy)
     * or from the virtual mbean (attribute change). This method implements the
     * NotificationListener interface.
     *
     * @param  notification  a <code>Notification</code> value
     * @param  handback  an <code>Object</code> value
     */
    public void handleNotification(Notification notification, Object handback) {

        logger.entering(logTag, "handleNotification",
            new Object[] { notification, handback });

        if (!(notification instanceof SysEventNotification))
            return;

        SysEventNotification clEvent = (SysEventNotification) notification;

        String subClass = clEvent.getSubclass();

        // Invalidate cache uppon status/config change
        if (subClass.equals(ClEventDefs.ESC_CLUSTER_RG_STATE) ||
                subClass.equals(
                    ClEventDefs.ESC_CLUSTER_RG_PRIMARIES_CHANGING) ||
                subClass.equals(ClEventDefs.ESC_CLUSTER_R_STATE) ||
                subClass.equals(ClEventDefs.ESC_CLUSTER_FM_R_STATUS_CHANGE) ||
                subClass.equals(ClEventDefs.ESC_CLUSTER_R_CONFIG_CHANGE)) {
            invalidateCache();
        }

        if (subClass.equals(ClEventDefs.ESC_CLUSTER_R_CONFIG_CHANGE)) {

            Map clEventAttrs = clEvent.getAttrs();

            String rname = (String) clEventAttrs.get(ClEventDefs.CL_R_NAME);

            int configActionInt =
                ((Long) (clEventAttrs.get(ClEventDefs.CL_CONFIG_ACTION)))
                .intValue();

            // workaround for JDK1.5/1.6 to make sure the Enum is referenced.
            ClEventDefs.ClEventConfigActionEnum configAction =
                ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED;
            configAction = (ClEventDefs.ClEventConfigActionEnum)
                (ClEventDefs.ClEventConfigActionEnum.getEnum(
                        ClEventDefs.ClEventConfigActionEnum.class,
                        configActionInt));

            if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED) {

                // Send an MBean registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Register MBean " + rname);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            ResourceMBean.class, rname);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.REGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "can't send notification", e);
                }

            } else if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.
                    CL_EVENT_CONFIG_REMOVED) {

                // Send an MBean registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Unregister MBean " + rname);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            ResourceMBean.class, rname);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.UNREGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "caught exception", e);
                }
            }
        }

        logger.exiting(logTag, "handleNotification");
    }

    /**
     * Called by the parent interceptor class
     *
     * @param  instanceName  a <code>String</code> value
     * @param  operationName  a <code>String</code> value
     * @param  params  an <code>Object[]</code> value
     * @param  signature  a <code>String[]</code> value
     *
     * @return  an <code>Object</code> value
     *
     * @exception  InstanceNotFoundException  if an error occurs
     * @exception  MBeanException  if an error occurs
     * @exception  ReflectionException  if an error occurs
     * @exception  IOException  if an error occurs
     */
    public Object invoke(String instanceName, String operationName,
        Object params[], String signature[]) throws InstanceNotFoundException,
        MBeanException, ReflectionException, IOException {

        logger.entering(logTag, "invoke : " + operationName, signature);

        if (!isRegistered(instanceName)) {
            throw new InstanceNotFoundException("cannot find the instance " +
                instanceName);
        }

        String commands[][] = null;
        boolean commit = true;

        if (operationName.equals("changeProjectName")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RES_CMD, "set", "-p",
                            ResourceMBean.PROJECT_NAME + "=" +
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("changeDescription")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RES_CMD, "set", "-p",
                            ResourceMBean.DESCRIPTION + "=" +
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("changeWeakDependencies")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("[Ljava.lang.String;"))) {
                String newDependencies[] = (String[]) params[0];
                String list = "";

                if (newDependencies.length > 0) {
                    list = newDependencies[0];

                    for (int i = 1; i < newDependencies.length; i++) {
                        list = list.concat("," + newDependencies[i]);
                    }
                }

                commands = new String[][] {
                        {
                            ClusterPaths.CL_RES_CMD, "set", "-p",
                            ResourceMBean.WEAK_DEPENDENCIES + "=" + list,
                            instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("changeStrongDependencies")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("[Ljava.lang.String;"))) {
                String newDependencies[] = (String[]) params[0];
                String list = "";

                if (newDependencies.length > 0) {
                    list = new String(newDependencies[0]);

                    for (int i = 1; i < newDependencies.length; i++) {
                        list = list.concat("," + newDependencies[i]);
                    }
                }

                commands = new String[][] {
                        {
                            ClusterPaths.CL_RES_CMD, "set", "-p",
                            ResourceMBean.STRONG_DEPENDENCIES + "=" + list,
                            instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("changeRestartDependencies")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("[Ljava.lang.String;"))) {
                String newDependencies[] = (String[]) params[0];
                String list = "";

                if (newDependencies.length > 0) {
                    list = new String(newDependencies[0]);

                    for (int i = 1; i < newDependencies.length; i++) {
                        list = list.concat("," + newDependencies[i]);
                    }
                }

                commands = new String[][] {
                        {
                            ClusterPaths.CL_RES_CMD, "set", "-p",
                            ResourceMBean.RESTART_DEPENDENCIES + "=" + list,
                            instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("changeOfflineDependencies")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("[Ljava.lang.String;"))) {
                String newDependencies[] = (String[]) params[0];
                String list = "";

                if (newDependencies.length > 0) {
                    list = new String(newDependencies[0]);

                    for (int i = 1; i < newDependencies.length; i++) {
                        list = list.concat("," + newDependencies[i]);
                    }
                }

                commands = new String[][] {
                        {
                            ClusterPaths.CL_RES_CMD, "set", "-p",
                            ResourceMBean.OFFLINE_DEPENDENCIES + "=" + list,
                            instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }


        } else if ((operationName.equals("changeSystemProperties")) ||
                (operationName.equals("changeExtProperties"))) {

            if ((signature != null) && (signature.length == 2) &&
                    (signature[0].equals("java.util.List")) &&
                    (signature[1].equals("boolean"))) {

                if (!(((Boolean) params[1]).booleanValue())) {
                    commit = false;
                }

                List newProperties = (List) params[0];

                // Used for per-node extension properties.
                LinkedList commandList = new LinkedList();
                Iterator props = newProperties.iterator();
                String option = "-p";

                int i = 0;
                commandList.add(ClusterPaths.CL_RES_CMD);
                commandList.add("set");

                while (props.hasNext()) {
                    ResourceProperty prop = (ResourceProperty) props.next();

                    // Set the per-node extension property, for properties
                    // which have been set per-node values.
                    boolean hasPernodeValue = false;

                    if (prop.isPernode()) {

                        if (prop instanceof ResourcePropertyString) {

                            ResourcePropertyString rps =
                                (ResourcePropertyString) prop;
                            HashMap pernodeMap = rps.getPernodeValue();
                            String pernodeDefValue = "";

                            if (pernodeMap != null) {
                                Iterator it = pernodeMap.keySet().iterator();
                                HashMap pernodeDefValueMap = rps
                                    .retrievePernodeValueMap();

                                while (it.hasNext()) {
                                    String nodename = (String) it.next();
                                    String pernodeValue = (String) pernodeMap
                                        .get(nodename);

                                    if (pernodeDefValueMap != null) {
                                        pernodeDefValue = (String)
                                            pernodeDefValueMap.get(nodename);
                                    } else {
                                        pernodeDefValue = rps.getDefaultValue();
                                    }

                                    if (!pernodeValue.equals(pernodeDefValue)) {
                                        commandList.add(option);
                                        commandList.add(prop.getName() + "{" +
                                            nodename + "}" + "=" +
                                            pernodeValue);
                                        hasPernodeValue = true;
                                    }
                                }

                                // Go to the beginning of the property loop.
                                continue;
                            }
                        } else if (prop instanceof ResourcePropertyInteger) {
                            ResourcePropertyInteger rpi =
                                (ResourcePropertyInteger) prop;
                            HashMap pernodeMap = rpi.getPernodeValue();
                            Integer pernodeDefValue;

                            if (pernodeMap != null) {

                                Iterator it = pernodeMap.keySet().iterator();
                                HashMap pernodeDefValueMap = rpi
                                    .retrievePernodeValueMap();

                                while (it.hasNext()) {
                                    String nodename = (String) it.next();
                                    Integer pernodeValue = (Integer) pernodeMap
                                        .get(nodename);

                                    if (pernodeDefValueMap != null) {
                                        pernodeDefValue = (Integer)
                                            pernodeDefValueMap.get(nodename);
                                    } else {
                                        pernodeDefValue = rpi.getDefaultValue();
                                    }

                                    if (!pernodeValue.equals(pernodeDefValue)) {
                                        commandList.add(option);
                                        commandList.add(prop.getName() + "{" +
                                            nodename + "}" + "=" +
                                            pernodeValue);
                                        hasPernodeValue = true;
                                    }
                                }

                                // Go to the beginning of the property loop.
                                continue;
                            }
                        } else if (prop instanceof ResourcePropertyBoolean) {
                            ResourcePropertyBoolean rpb =
                                (ResourcePropertyBoolean) prop;
                            HashMap pernodeMap = rpb.getPernodeValue();
                            Boolean pernodeDefValue;

                            if (pernodeMap != null) {

                                Iterator it = pernodeMap.keySet().iterator();
                                HashMap pernodeDefValueMap = rpb
                                    .retrievePernodeValueMap();

                                while (it.hasNext()) {

                                    String nodename = (String) it.next();
                                    Boolean pernodeValue = (Boolean) pernodeMap
                                        .get(nodename);

                                    if (pernodeDefValueMap != null) {
                                        pernodeDefValue = (Boolean)
                                            pernodeDefValueMap.get(nodename);
                                    } else {
                                        pernodeDefValue = rpb.getDefaultValue();
                                    }

                                    if (!pernodeValue.equals(pernodeDefValue)) {

                                        String str = pernodeValue
                                            .booleanValue() ? "TRUE" : "FALSE";
                                        commandList.add(option);
                                        commandList.add(prop.getName() + "{" +
                                            nodename + "}" + "=" + str);
                                        hasPernodeValue = true;
                                    }
                                }

                                // Go to the beginning of the property loop.
                                continue;
                            }
                        } else if (prop instanceof ResourcePropertyEnum) {
                            ResourcePropertyEnum rpe = (ResourcePropertyEnum)
                                prop;
                            HashMap pernodeMap = rpe.getPernodeValue();
                            String pernodeDefValue = "";

                            if (pernodeMap != null) {

                                Iterator it = pernodeMap.keySet().iterator();
                                HashMap pernodeDefValueMap = rpe
                                    .retrievePernodeValueMap();

                                while (it.hasNext()) {

                                    String nodename = (String) it.next();
                                    String pernodeValue = (String) pernodeMap
                                        .get(nodename);

                                    if (pernodeDefValueMap != null) {
                                        pernodeDefValue = (String)
                                            pernodeDefValueMap.get(nodename);
                                    } else {
                                        pernodeDefValue = rpe.getDefaultValue();
                                    }

                                    if (!pernodeValue.equals(pernodeDefValue)) {
                                        commandList.add(option);
                                        commandList.add(prop.getName() + "{" +
                                            nodename + "}" + "=" +
                                            pernodeValue);
                                        hasPernodeValue = true;
                                    }
                                }

                                // Go to the beginning of the property loop.
                                continue;
                            }
                        }
                    }

                    // set the extension property for values which are not
                    // per-node and for per-node
                    // values which want same value on all the nodes.
                    if (hasPernodeValue == false) {
                        commandList.add(option);
                        commandList.add(prop.getName() + "=" +
                            RgmManager.getPropertyValue(prop));
                    }
                }

                commandList.add(instanceName);
                commands = new String[][] {
                        (String[]) commandList.toArray(new String[0])
                    };

            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("clearStopFailedFlag")) {

            // First, get the RGM State of the resource on each node
            AttributeList attributeList = getAttributes(instanceName,
                    new String[] { "RgmStates" });
            Attribute attribute = (Attribute) attributeList.get(0);
            ResourceRgmState states[] = (ResourceRgmState[]) attribute
                .getValue();

            StringBuffer nodes = new StringBuffer();

            for (int i = 0; i < states.length; i++) {

                if (states[i].getState().equals(
                            ResourceRgmStateEnum.STOP_FAILED)) {
                    nodes.append(states[i].getNodeName() + ",");
                }
            }

            commands = new String[][] {
                    {
                        ClusterPaths.CL_RES_CMD, "clear", "-n",
                        nodes.substring(0, nodes.length() - 1), "-f",
                        "STOP_FAILED", instanceName
                    }
                };

        } else if (operationName.equals("enable")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RES_CMD, "enable", instanceName }
                };

        } else if (operationName.equals("disable")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RES_CMD, "disable", instanceName }
                };

        } else if (operationName.equals("startMonitor")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RES_CMD, "monitor", instanceName }
                };

        } else if (operationName.equals("stopMonitor")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RES_CMD, "unmonitor", instanceName }
                };

        } else if (operationName.equals("remove")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RES_CMD, "delete", instanceName }
                };

        } else if (operationName.equals("pernodeEnable")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RES_CMD, "enable", "-n",
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("pernodeDisable")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RES_CMD, "disable", "-n",
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("pernodeStartMonitor")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RES_CMD, "monitor", "-n",
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("pernodeStopMonitor")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RES_CMD, "unmonitor", "-n",
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else {
            throw new ReflectionException(new IllegalArgumentException(
                    operationName));
        }

        // Run the command(s)
        InvocationStatus exits[];
        ExitStatus exitsWrapper[];

        if (commit) {

            try {
                exits = InvokeCommand.execute(commands, null);
                invalidateCache();
            } catch (InvocationException ie) {
                exits = ie.getInvocationStatusArray();
                invalidateCache();
                throw new MBeanException(new CommandExecutionException(
                        ie.getMessage(), ExitStatus.createArray(exits)));
            }

            exitsWrapper = ExitStatus.createArray(exits);

        } else {
            exitsWrapper = new ExitStatus[commands.length];

            for (int i = 0; i < commands.length; i++) {
                exitsWrapper[i] = new ExitStatus(ExitStatus.escapeCommand(
                            commands[i]), ExitStatus.SUCCESS, null, null);
            }
        }

        logger.exiting(logTag, "invoke");

        return exitsWrapper;
    }

    /**
     * Fill the cache
     *
     * @return  a <code>Map</code>, key is instance name, value is a map of
     * attribute name to attribute value.
     */
    protected native synchronized Map fillCache();

}
