/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ResourceGroupInterceptor.java 1.21     09/03/27 SMI"
 */

package com.sun.cluster.agent.rgm;

// JDK
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerNotification;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;

// JDMK
import com.sun.jdmk.JdmkMBeanServer;
import com.sun.jdmk.ServiceName;
import com.sun.jdmk.interceptor.MBeanServerInterceptor;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.CachedVirtualMBeanInterceptor;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.event.ClEventDefs;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.event.SysEventNotifierMBean;
import com.sun.cluster.common.ClusterPaths;


/**
 * CACAO interceptor for the resource group module. All instance and attribute
 * values are fetched through JNI, and all operations are performed through an
 * invocation of a CLI. This interceptor also listen for the events triggered by
 * the cluster for dynamic handle of resource group's creation and deletion.
 */
public class ResourceGroupInterceptor extends CachedVirtualMBeanInterceptor
    implements NotificationListener {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.rgm");
    private final static String logTag = "ResourceGroupInterceptor";

    /* Internal reference to the SysEvent Notifier. */
    private ObjectName sysEventNotifier;

    /* The MBeanServer in which this interceptor is inserted. */
    private MBeanServer mBeanServer;

    /**
     * Default Constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this interceptor.
     * @param  dispatcher  DomainDispatcher that this virtual mbean interceptor
     * should register into for dispatching.
     * @param  onf  ObjectNameFactory implementation to be used by this
     * VirtualMBeanInterceptor
     */
    public ResourceGroupInterceptor(MBeanServer mBeanServer,
        VirtualMBeanDomainDispatcher dispatcher, ObjectNameFactory onf) {
        super(mBeanServer, dispatcher, onf, ResourceGroupMBean.class, null);
        logger.entering(logTag, "<init>",
            new Object[] { mBeanServer, dispatcher, onf });
        this.mBeanServer = mBeanServer;
        this.sysEventNotifier =
            new ObjectNameFactory("com.sun.cluster.agent.event").getObjectName(
                SysEventNotifierMBean.class, null);
        logger.exiting(logTag, "<init>");
    }

    /**
     * Called when the module is unlocked
     */
    public void unlock() throws Exception {
        super.unlock();
        logger.entering(logTag, "unlock");
        mBeanServer.addNotificationListener(sysEventNotifier, this, null, null);
        logger.exiting(logTag, "unlock");
    }

    /**
     * Called when the module is locked
     */
    public void lock() throws Exception {
        super.lock();
        logger.entering(logTag, "lock");
        mBeanServer.removeNotificationListener(sysEventNotifier, this, null,
            null);
        invalidateCache();
        logger.exiting(logTag, "lock");
    }

    /**
     * Called by Odyssey to invalidate the cache. This is not a good approach,
     * must be replaced with a event that says, "my cache has changed". Odyssey 
     * can listen to this event and perform actions based on it.
     */
    public void refreshCache() {
        invalidateCache();
    }


    /**
     * The interceptor should monitor create/destroy/attribute-change events and
     * emit appropriate notifications from the mbean server (for create/destroy)
     * or from the virtual mbean (attribute change). This method implements the
     * NotificationListener interface.
     *
     * @param  notification  a <code>Notification</code> value
     * @param  handback  an <code>Object</code> value
     */
    public void handleNotification(Notification notification, Object handback) {

        logger.entering(logTag, "handleNotification",
            new Object[] { notification, handback });

        if (!(notification instanceof SysEventNotification))
            return;

        SysEventNotification clEvent = (SysEventNotification) notification;

        String subClass = clEvent.getSubclass();

        // Invalidate cache uppon status/config change
        if (subClass.equals(ClEventDefs.ESC_CLUSTER_RG_STATE) ||
                subClass.equals(
                    ClEventDefs.ESC_CLUSTER_RG_PRIMARIES_CHANGING) ||
                subClass.equals(ClEventDefs.ESC_CLUSTER_RG_REMAINING_OFFLINE) ||
                subClass.equals(ClEventDefs.ESC_CLUSTER_RG_GIVEOVER_DEFERRED) ||
                subClass.equals(ClEventDefs.ESC_CLUSTER_RG_NODE_REBOOTED) ||
                subClass.equals(ClEventDefs.ESC_CLUSTER_RG_CONFIG_CHANGE) ||
                subClass.equals(ClEventDefs.ESC_CLUSTER_R_CONFIG_CHANGE)) {
            invalidateCache();
        }

        if (subClass.equals(ClEventDefs.ESC_CLUSTER_RG_CONFIG_CHANGE)) {

            Map clEventAttrs = clEvent.getAttrs();

            String rgname = (String) clEventAttrs.get(ClEventDefs.CL_RG_NAME);

              
            int configActionInt =
                ((Long) (clEventAttrs.get(ClEventDefs.CL_CONFIG_ACTION)))
                .intValue();

            // workaround for JDK1.5/1.6 to make sure the Enum is referenced.
            ClEventDefs.ClEventConfigActionEnum configAction =
                ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED;
            configAction = (ClEventDefs.ClEventConfigActionEnum)
                (ClEventDefs.ClEventConfigActionEnum.getEnum(
                        ClEventDefs.ClEventConfigActionEnum.class,
                        configActionInt));

            if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED) {

                // Send an MBean registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Register MBean " + rgname);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            ResourceGroupMBean.class, rgname);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.REGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "can't send notification", e);
                }

            } else if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.
                    CL_EVENT_CONFIG_REMOVED) {

                // Send an MBean registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Unregister MBean " + rgname);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            ResourceGroupMBean.class, rgname);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.UNREGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "caught exception", e);
                }
            }
        }

        logger.exiting(logTag, "handleNotification");
    }

    /**
     * Called by the parent interceptor class
     *
     * @param  instanceName  a <code>String</code> value
     * @param  operationName  a <code>String</code> value
     * @param  params  an <code>Object[]</code> value
     * @param  signature  a <code>String[]</code> value
     *
     * @return  an <code>Object</code> value
     *
     * @exception  InstanceNotFoundException  if an error occurs
     * @exception  MBeanException  if an error occurs
     * @exception  ReflectionException  if an error occurs
     * @exception  IOException  if an error occurs
     */
    public Object invoke(String instanceName, String operationName,
        Object params[], String signature[]) throws InstanceNotFoundException,
        MBeanException, ReflectionException, IOException {

        logger.entering(logTag, "invoke : " + operationName, signature);

        if (!isRegistered(instanceName)) {
            throw new InstanceNotFoundException("cannot find the instance " +
                instanceName);
        }

        // Construct the appropriate comand string depending on
        // the operation name.
        String commands[][] = null;
        boolean commit = true;

        if (operationName.equals("changeDescription")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.DESCRIPTION + "=" +
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeFailureInterval")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("int"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.FAILURE_INTERVAL + "=" +
                            params[0], instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeDirectoryPath")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.PATH_PREFIX + "=" +
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeProjectName")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.PROJECT_NAME + "=" +
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }
        } else if (operationName.equals("changeSLMType")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.SLM_TYPE + "=" +
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }
        } else if (operationName.equals("changeSLMPsetType")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.SLM_PSET_TYPE + "=" +
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }
        } else if (operationName.equals("changeSLMCPUShares")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.SLM_CPU_SHARES + "=" +
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }
        } else if (operationName.equals("changeSLMPsetMin")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("java.lang.String"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.SLM_PSET_MIN + "=" +
                            (String) params[0], instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }
        } else if (operationName.equals("changeSharedStorageUsedFlag")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("boolean"))) {

                if (((Boolean) params[0]).booleanValue()) {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "set", "-p",
                                ResourceGroupMBean.SHARED_STORAGE_USED + "=*",
                                instanceName
                            }
                        };
                } else {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "set", "-p",
                                ResourceGroupMBean.SHARED_STORAGE_USED + "=",
                                instanceName
                            }
                        };
                }
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeFailbackFlag")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("boolean"))) {

                if (((Boolean) params[0]).booleanValue()) {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "set", "-p",
                                ResourceGroupMBean.FAILBACK + "=TRUE",
                                instanceName
                            }
                        };
                } else {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "set", "-p",
                                ResourceGroupMBean.FAILBACK + "=FALSE",
                                instanceName
                            }
                        };
                }
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeAutoStartableFlag")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("boolean"))) {

                if (((Boolean) params[0]).booleanValue()) {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "set", "-p",
                                ResourceGroupMBean.AUTO_START + "=TRUE",
                                instanceName
                            }
                        };
                } else {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "set", "-p",
                                ResourceGroupMBean.AUTO_START + "=FALSE",
                                instanceName
                            }
                        };
                }
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeSystemFlag")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("boolean"))) {

                if (((Boolean) params[0]).booleanValue()) {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "set", "-p",
                                ResourceGroupMBean.SYSTEM + "=TRUE",
                                instanceName
                            }
                        };
                } else {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "set", "-p",
                                ResourceGroupMBean.SYSTEM + "=FALSE",
                                instanceName
                            }
                        };
                }
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeNetworkDependencyFlag")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("boolean"))) {

                if (((Boolean) params[0]).booleanValue()) {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "set", "-p",
                                ResourceGroupMBean.NETWORKS_DEPEND + "=TRUE",
                                instanceName
                            }
                        };
                } else {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "-g", instanceName,
                                "-p",
                                ResourceGroupMBean.NETWORKS_DEPEND + "=FALSE",
                                instanceName
                            }
                        };
                }
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeNodeNames")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("[Ljava.lang.String;"))) {
                String newNodeNames[] = (String[]) params[0];
                String list = "";

                if (newNodeNames.length > 0) {
                    list = newNodeNames[0];

                    for (int i = 1; i < newNodeNames.length; i++) {
                        list = list.concat("," + newNodeNames[i]);
                    }
                }

                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.PRIMARIES + "=" + list,
                            instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeDesiredPrimariesCount")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("int"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.DESIRED + "=" + params[0],
                            instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeMaxPrimaries")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("int"))) {
                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.MAX + "=" + params[0],
                            instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeDependencies")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("[Ljava.lang.String;"))) {
                String newDependencies[] = (String[]) params[0];
                String list = "";

                if (newDependencies.length > 0) {
                    list = newDependencies[0];

                    for (int i = 1; i < newDependencies.length; i++) {
                        list = list.concat("," + newDependencies[i]);
                    }
                }

                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.DEPEND + "=" + list, instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeAffinities")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("[Ljava.lang.String;"))) {
                String newDependencies[] = (String[]) params[0];
                String list = "";

                if (newDependencies.length > 0) {
                    list = new String(newDependencies[0]);

                    for (int i = 1; i < newDependencies.length; i++) {
                        list = list.concat("," + newDependencies[i]);
                    }
                }

                commands = new String[][] {
                        {
                            ClusterPaths.CL_RG_CMD, "set", "-p",
                            ResourceGroupMBean.AFFINITIES + "=" + list,
                            instanceName
                        }
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("changeProperties")) {

            if ((signature != null) && (signature.length == 2) &&
                    (signature[0].equals("java.util.Map")) &&
                    (signature[1].equals("boolean"))) {

                if (!(((Boolean) params[1]).booleanValue())) {
                    commit = false;
                }

                Map newProperties = (Map) params[0];

                commands = new String[1][3 + (newProperties.size() * 2)];

                int commandIndex = 0;
                commands[0][commandIndex++] = ClusterPaths.CL_RG_CMD;
                commands[0][commandIndex++] = "set";

                Iterator propertyNames = newProperties.keySet().iterator();

                while (propertyNames.hasNext()) {
                    String propertyName = (String) propertyNames.next();
                    commands[0][commandIndex++] = "-p";
                    commands[0][commandIndex++] = propertyName + "=" +
                        (String) newProperties.get(propertyName);
                }

                commands[0][commandIndex++] = instanceName;

            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("remove")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RG_CMD, "delete", instanceName }
                };

        } else if (operationName.equals("restart")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RG_CMD, "restart", instanceName }
                };

        } else if (operationName.equals("bringOnline")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RG_CMD, "online", "-emM", instanceName }
                };

        } else if (operationName.equals("bringOffline")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RG_CMD, "offline", instanceName }
                };

        } else if (operationName.equals("manage")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RG_CMD, "manage", instanceName }
                };

        } else if (operationName.equals("unmanage")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RG_CMD, "unmanage", instanceName }
                };

        } else if (operationName.equals("suspend")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RG_CMD, "suspend", instanceName }
                };

        } else if (operationName.equals("fastsuspend")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RG_CMD, "suspend", "-k", instanceName }
                };

        } else if (operationName.equals("resume")) {
            commands = new String[][] {
                    { ClusterPaths.CL_RG_CMD, "resume", instanceName }
                };

        } else if (operationName.equals("switchPrimaries")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("[Ljava.lang.String;"))) {
                String hosts[] = (String[]) params[0];
                String list = "";

                if (hosts.length > 0) {
                    list = new String(hosts[0]);

                    for (int i = 1; i < hosts.length; i++) {
                        list = list.concat("," + hosts[i]);
                    }
                }

                if (list.trim().length() == 0) {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "switch", "-n",
                                instanceName
                            }
                        };
                } else {
                    commands = new String[][] {
                            {
                                ClusterPaths.CL_RG_CMD, "switch", "-n", list,
                                instanceName
                            }
                        };
                }
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else if (operationName.equals("enableResources")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("[Ljava.lang.String;"))) {
                String resources[] = (String[]) params[0];
                ArrayList list = new ArrayList();

                list.add(ClusterPaths.CL_RES_CMD);
                list.add("enable");

                if (resources.length > 0) {

                    for (int i = 0; i < resources.length; i++) {
                        list.add(resources[i]);
                    }
                }

                commands = new String[][] {
                        (String[]) list.toArray(new String[0])
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }
        } else if (operationName.equals("disableResources")) {

            if ((signature != null) && (signature.length == 1) &&
                    (signature[0].equals("[Ljava.lang.String;"))) {
                String resources[] = (String[]) params[0];
                ArrayList list = new ArrayList();
                list.add(ClusterPaths.CL_RES_CMD);
                list.add("disable");

                if (resources.length > 0) {

                    for (int i = 0; i < resources.length; i++) {
                        list.add(resources[i]);
                    }
                }

                commands = new String[][] {
                        (String[]) list.toArray(new String[0])
                    };
            } else {
                throw new ReflectionException(new IOException(
                        "Invalid signature"));
            }

        } else {
            throw new ReflectionException(new IllegalArgumentException(
                    operationName));
        }

        // Run the command(s)
        InvocationStatus exits[];
        ExitStatus exitsWrapper[];

        if (commit) {

            try {
                exits = InvokeCommand.execute(commands, null);

                // status/config will probably change as a result of this cmd
                invalidateCache();
            } catch (InvocationException ie) {
                exits = ie.getInvocationStatusArray();
                invalidateCache();
                throw new MBeanException(new CommandExecutionException(
                        ie.getMessage(), ExitStatus.createArray(exits)));
            }

            exitsWrapper = ExitStatus.createArray(exits);

        } else {
            exitsWrapper = new ExitStatus[commands.length];

            for (int i = 0; i < commands.length; i++) {
                exitsWrapper[i] = new ExitStatus(ExitStatus.escapeCommand(
                            commands[i]), ExitStatus.SUCCESS, null, null);
            }
        }

        logger.exiting(logTag, "invoke");

        return exitsWrapper;
    }

    /**
     * Fill the cache
     *
     * @return  a <code>Map</code>, key is instance name, value is a map of
     * attribute name to attribute value.
     */
    protected native Map fillCache();

}
