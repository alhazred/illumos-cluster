/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)rgm_manager_jni.c	1.4	08/05/20 SMI"

#include "com/sun/cluster/agent/rgm/RgmManager.h"

#include <jniutil.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <project.h>

#define	PROJ_SIZE 200

/*
 * Class:     com_sun_cluster_agent_rgm_RgmManager
 * Method:    getProjectNames
 * Signature: ()Ljava/util/Set;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_rgm_RgmManager_getProjectNames
	(JNIEnv *env, jobject this) {

	jobject instances = NULL;
	jclass set_class = NULL;
	jmethodID set_add;

	struct project *proj, *projRecevied = NULL;
	void *buf;


	proj = (struct project*) calloc(1, sizeof (struct project));
	buf = (void *) calloc(PROJ_SIZE, sizeof (char));

	if (proj == NULL || buf == NULL) goto error;

	/*
	 * get our references to set_class and the add method
	 */
	set_class = (*env)->FindClass(env, "java/util/Set");
	if (set_class == NULL)
		goto error;

	set_add = (*env)->GetMethodID(env,
	    set_class,
	    "add",
	    "(Ljava/lang/Object;)"
	    "Z");
	if (set_add == NULL)
		goto error;

	/*
	 * Create the set that will contain the resource type's name
	 */
	instances = newObjNullParam(env, "java/util/HashSet");
	if (instances == NULL)
		goto error;

	setprojent();
	projRecevied = getprojent(proj, buf, PROJ_SIZE);
	while (projRecevied != NULL) {

		(*env)->CallObjectMethod(env,
			instances,
			set_add,
			(*env)->NewStringUTF(env, projRecevied->pj_name));

		projRecevied = getprojent(proj, buf, PROJ_SIZE);
	}
	endprojent();

    error:

	delRef(env, set_class);
	free(buf);
	free(proj);
	return (instances);
} 
