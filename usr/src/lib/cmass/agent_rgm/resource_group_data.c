/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * RgGroup MBean
 */

#pragma ident	"@(#)resource_group_data.c	1.7	08/08/17 SMI"

#include "com/sun/cluster/agent/rgm/RgGroup.h"

#include <jniutil.h>
#include <string.h>
#include <stdio.h>
#include <scha.h>
#include <stdlib.h>
#include <thread.h>
#include <string.h>
#include <unistd.h>
#if SOL_VERSION >= __s10
#include <stdbool.h>
#include <zone.h>
#include <libzccfg/libzccfg.h>
#include <sys/clconf_int.h>
#endif /* SOL_VERSION >= __s10 */

#define	debug if (0)
#define	BUFFSIZE 160
#define	getBooleanVal(expr) (expr)? JNI_TRUE : JNI_FALSE
#define	PUT(MAP, NAME, VALUE)						\
	(*env)->CallObjectMethod(env,					\
				    MAP,				\
				    map_put,				\
				    (*env)->NewStringUTF(env, NAME),	\
				    VALUE)

static mutex_t init_lock;
static int initialized = 0;

static jclass resourceGroupDataClass;
static jclass rgstatus_class;
static jclass map_class;
static jclass array_list_class;

static jmethodID resourceGroupDataClassConstructor;
static jmethodID rgstatus_constructor;
static jmethodID map_put;
static jmethodID map_clear;
static jmethodID array_list_constructor;
static jmethodID array_list_add;

static jfieldID nameID;
static jfieldID managedID;
static jfieldID onlineID;
static jfieldID nodeNamesID;
static jfieldID currentPrimariesID;
static jfieldID descriptionID;
static jfieldID resourceNamesID;
static jfieldID overallStatusID;
static jfieldID statusID;
static jfieldID modeID;
static jfieldID autoStartableID;
static jfieldID networkDependentID;
static jfieldID systemID;
static jfieldID failbackEnabledID;
static jfieldID sharedStorageUsedID;
static jfieldID suspendedID;
static jfieldID currentPrimariesCountID;
static jfieldID desiredPrimariesCountID;
static jfieldID maxPrimariesID;
static jfieldID dependenciesID;
static jfieldID dependenciesCountID;
static jfieldID directoryPathID;
static jfieldID failureIntervalID;
static jfieldID affinitiesID;
static jfieldID projectNameID;
static jfieldID slmTypeID;
static jfieldID slmpSetTypeID;
static jfieldID slmCPUSharesID;
static jfieldID slmpSetMinID;

static void init(JNIEnv *env) {

    jclass localClass;
    debug printf("\n BEGIN init \n");
	(void) mutex_lock(&init_lock);


    if (!initialized) {

	/* Utility Classes */
	localClass = (*env)->FindClass(env, "java/util/HashMap");
	map_class = (*env)->NewGlobalRef(env, localClass);
	if (map_class == NULL)
	    goto unlock;
	map_put =
		(*env)->GetMethodID(env,
		map_class,
		"put",
		"(Ljava/lang/Object;Ljava/lang/Object;)"
		"Ljava/lang/Object;");
	if (map_put == NULL)
	    goto unlock;

	map_clear = (*env)->GetMethodID(env,
		map_class,
		"clear",
		"()V");
	if (map_clear == NULL)
	    goto unlock;

	/* ArrayList */
	localClass = (*env)->FindClass(env, "java/util/ArrayList");
	array_list_class = (*env)->NewGlobalRef(env, localClass);
	if (array_list_class == NULL) {
	    debug printf("\n array list class is null \n");
	    goto unlock;
	}

	array_list_constructor = (*env)->GetMethodID(
		env, array_list_class,
		"<init>", "()V");
	if (array_list_constructor == NULL) {
	    debug printf("\n array list constructor is null \n");
	    goto unlock;
	}
	array_list_add = (*env)->GetMethodID(env, array_list_class,
		"add", "(Ljava/lang/Object;)Z");
	if (array_list_add == NULL) {
	    debug printf("\n array list add is null \n");
	    goto unlock;
	}

	/* find ResourceGroupData class */
	localClass = (*env)->FindClass(env,
		"com/sun/cluster/agent/rgm/ResourceGroupData");
	resourceGroupDataClass = (*env)->NewGlobalRef(env, localClass);

	if (resourceGroupDataClass == NULL) {
	    debug printf("\n ResourceGroupData class is null\n");
	    debug printf("\n END init \n");
	    goto unlock;
	}

	/* find ResourceGroupData class constructor */
	resourceGroupDataClassConstructor = (*env)->GetMethodID(env,
		resourceGroupDataClass, "<init>", "()V");

	if (resourceGroupDataClassConstructor == NULL) {
	    debug printf("\n ResourceGroupData Constructor is null\n");
	    debug printf("\n END init \n");
	    goto unlock;
	}

	/* find ResourceGroupData class fields */
	nameID = (*env)->GetFieldID(env, resourceGroupDataClass, "name",
		"Ljava/lang/String;");
	if (nameID == NULL) {
	    debug printf("\n nameID is null \n");
	    goto unlock;
	}

	managedID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"managed", "Z");
	if (managedID == NULL) {
	    debug printf("\n nameID is null \n");
	    goto unlock;
	}

	onlineID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"online", "Z");
	if (onlineID == NULL) {
	    debug printf("\n onlineID is null \n");
	    goto unlock;
	}


	nodeNamesID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"nodeNames", "[Ljava/lang/String;");
	if (nodeNamesID == NULL) {
	    debug printf("\n nodeNamesID is null \n");
	    goto unlock;
	}

	currentPrimariesID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"currentPrimaries", "[Ljava/lang/String;");
	if (currentPrimariesID == NULL) {
	    debug printf("\n currentPrimariesID is null \n");
	    goto unlock;
	}

	descriptionID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"description", "Ljava/lang/String;");
	if (descriptionID == NULL) {
	    debug printf("\n descriptionID is null \n");
	    goto unlock;
	}

	resourceNamesID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"resourceNames", "[Ljava/lang/String;");
	if (resourceNamesID == NULL) {
	    debug printf("\n resourceNamesID is null \n");
	    goto unlock;
	}

	overallStatusID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"overallStatus",
		"Lcom/sun/cluster/agent/rgm/ResourceGroupStatusEnum;");
	if (overallStatusID == NULL) {
	    debug printf("\n overallStatusID is null \n");
	    goto unlock;
	}

	statusID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"status",
		"[Lcom/sun/cluster/agent/rgm/ResourceGroupStatus;");
	if (statusID == NULL) {
	    debug printf("\n statusID is null \n");
	    goto unlock;
	}

	modeID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"mode",
		"Lcom/sun/cluster/agent/rgm/ResourceGroupModeEnum;");
	if (modeID == NULL) {
	    debug printf("\n modeID is null \n");
	    goto unlock;
	}

	autoStartableID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"autoStartable", "Z");
	if (autoStartableID == NULL) {
	    debug printf("\n autoStartableID is null \n");
	    goto unlock;
	}


	networkDependentID = (*env)->GetFieldID(env,
		resourceGroupDataClass,
		"networkDependent", "Z");
	if (networkDependentID == NULL) {
	    debug printf("\n networkDependentID is null \n");
	    goto unlock;
	}

	systemID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"system", "Z");
	if (systemID == NULL) {
	    debug printf("\n systemID is null \n");
	    goto unlock;
	}

	failbackEnabledID = (*env)->GetFieldID(env,
		resourceGroupDataClass,
		"failbackEnabled", "Z");
	if (failbackEnabledID == NULL) {
	    debug printf("\n failbackEnabledID is null \n");
	    goto unlock;
	}

	sharedStorageUsedID = (*env)->GetFieldID(env,
		resourceGroupDataClass,
		"sharedStorageUsed", "Z");
	if (sharedStorageUsedID == NULL) {
	    debug printf("\n sharedStorageUsedID is null \n");
	    goto unlock;
	}

	suspendedID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"suspended", "Z");
	if (suspendedID == NULL) {
	    debug printf("\n suspendedID is null \n");
	    goto unlock;
	}

	currentPrimariesCountID = (*env)->GetFieldID(
		env, resourceGroupDataClass,
		"currentPrimariesCount", "I");
	if (currentPrimariesCountID == NULL) {
	    debug printf("\n currentPrimariesCountID is null \n");
	    goto unlock;
	}

	desiredPrimariesCountID = (*env)->GetFieldID(
		env, resourceGroupDataClass,
		"desiredPrimariesCount", "I");
	if (desiredPrimariesCountID == NULL) {
	    debug printf("\n desiredPrimariesCountID is null \n");
	    goto unlock;
	}

	maxPrimariesID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"maxPrimaries", "I");
	if (maxPrimariesID == NULL) {
	    debug printf("\n maxPrimariesID is null \n");
	    goto unlock;
	}

	dependenciesID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"dependencies", "[Ljava/lang/String;");
	if (dependenciesID == NULL) {
	    debug printf("\n dependenciesID is null \n");
	    goto unlock;
	}

	dependenciesCountID = (*env)->GetFieldID(
		env, resourceGroupDataClass,
		"dependenciesCount", "I");
	if (dependenciesCountID == NULL) {
	    debug printf("\n dependenciesCountID is null \n");
	    goto unlock;
	}

	directoryPathID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"directoryPath", "Ljava/lang/String;");
	if (directoryPathID == NULL) {
	    debug printf("\n directoryPathID is null \n");
	    goto unlock;
	}

	failureIntervalID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"failureInterval", "I");
	if (failureIntervalID == NULL) {
	    debug printf("\n failureIntervalID is null \n");
	    goto unlock;
	}

	affinitiesID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"affinities", "[Ljava/lang/String;");
	if (affinitiesID == NULL) {
	    debug printf("\n affinitiesID is null \n");
	    goto unlock;
	}

	projectNameID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"projectName", "Ljava/lang/String;");
	if (projectNameID == NULL) {
	    debug printf("\n projectNameID is null \n");
	    goto unlock;
	}

	slmTypeID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"slmType", "Ljava/lang/String;");
	if (slmTypeID == NULL) {
	    debug printf("\n slmTypeID is null \n");
	    goto unlock;
	}

	slmpSetTypeID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"slmpSetType", "Ljava/lang/String;");
	if (slmpSetTypeID == NULL) {
	    debug printf("\n slmpSetTypeID is null \n");
	    goto unlock;
	}

	slmCPUSharesID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"slmCPUShares", "I");
	if (slmCPUSharesID == NULL) {
	    debug printf("\n slmCPUSharesID is null \n");
	    goto unlock;
	}

	slmpSetMinID = (*env)->GetFieldID(env, resourceGroupDataClass,
		"slmpSetMin", "I");
	if (slmpSetMinID == NULL) {
	    debug printf("\n slmpSetMinID is null \n");
	    goto unlock;
	}

	/*
	 * Load ResourceGroupStatus class
	 */
	localClass = (*env)->FindClass(env,
		"com/sun/cluster/agent/rgm/ResourceGroupStatus");
	rgstatus_class = (*env)->NewGlobalRef(env, localClass);
	if (rgstatus_class == NULL)
	    goto unlock;

	/*
	 * Get constructor
	 */
	rgstatus_constructor = (*env)->GetMethodID(env,
		rgstatus_class,
		"<init>",
		"(Ljava/lang/String;Lcom/sun/cluster/agent/rgm/"
		"ResourceGroupStatusEnum;)V");
	if (rgstatus_constructor == NULL)
	    goto unlock;

	initialized = 1;
	}

    unlock:
	(void) mutex_unlock(&init_lock);
    debug printf("\n END init \n");
}




/*
 * RG helper funcion to retrieve a string type value and transform it to
 * a JNI jobject element.
 */
static jobject
getRgStringAttribute(JNIEnv * env,
	scha_resourcegroup_t rg_handle, const char *attr_tag, scha_err_t *err,
	char *cZCNameQ) {

    scha_err_t scha_error;
    char *attribute_value = NULL;
    jobject result = NULL;

	/*
	* Calling SCHA API to retrieve the attribute value
	*/
    if (cZCNameQ == NULL) {
	scha_error = scha_resourcegroup_get(rg_handle, attr_tag,
		&attribute_value);
	} else {
	scha_error = scha_resourcegroup_get_zone(
		cZCNameQ, rg_handle, attr_tag, &attribute_value);
	}
	*err = scha_error;

    if (scha_error != SCHA_ERR_NOERR) {
	attribute_value = "SCHA ERROR";
	}
    result = newObjStringParam(env, "java/lang/String", attribute_value);
    return (result);
}

/*
 * RG helper funcion to retrieve a boolean type value and transform it to
 * a JNI jBoolean element.
 */
static jboolean
getRgBooleanAttribute(JNIEnv * env,
	scha_resourcegroup_t rg_handle, const char *attr_tag, scha_err_t *err,
	char *cZCNameQ) {

    scha_err_t scha_error;
    boolean_t attribute_value = B_FALSE;
    jboolean result;

	/*
	* Calling SCHA API to retrieve the attribute value
	*/
    if (cZCNameQ == NULL) {
	scha_error = scha_resourcegroup_get(rg_handle, attr_tag,
		&attribute_value);
	} else {
	scha_error = scha_resourcegroup_get_zone(
		cZCNameQ, rg_handle, attr_tag, &attribute_value);
	}
	*err = scha_error;

    if (scha_error != SCHA_ERR_NOERR) {
	return (NULL);
	}
    if (attribute_value == B_TRUE) {
	result = JNI_TRUE;
	} else {
	result = JNI_FALSE;
	}
    return (result);
}

/*
 * RG helper funcion to retrieve a int type value and transform it to
 * a JNI jint element.
 */
static jint
getRgIntAttribute(JNIEnv * env, scha_resourcegroup_t rg_handle,
	const char *attr_tag, scha_err_t *err, char *cZCNameQ) {

    scha_err_t scha_error;
    int32_t attribute_value = -1;
    jint result = NULL;

	/*
	* Calling SCHA API to retrieve the attribute value
	*/
    if (cZCNameQ == NULL) {
	scha_error = scha_resourcegroup_get(rg_handle, attr_tag,
		&attribute_value);
	} else {
	scha_error = scha_resourcegroup_get_zone(
		cZCNameQ, rg_handle, attr_tag, &attribute_value);
	}
	*err = scha_error;

    if (scha_error != SCHA_ERR_NOERR) {
	return (NULL);
	}
    result = attribute_value;
    return (result);
}

/*
 * RG helper funcion to retrieve a string array type value and transform it to
 * a JNI jobject element.
 */
static jobject
getRgStringArrayAttribute(JNIEnv * env,
	scha_resourcegroup_t rg_handle, const char *attr_tag,
	int *count, scha_err_t *err, char *cZCNameQ) {

    scha_err_t scha_error;
    scha_str_array_t *attribute_value = NULL;
    jobject result = NULL;

	/*
	* Calling SCHA API to retrieve the attribute value
	*/
    if (cZCNameQ == NULL) {
	scha_error = scha_resourcegroup_get(rg_handle, attr_tag,
		&attribute_value);
	} else {
	scha_error = scha_resourcegroup_get_zone(cZCNameQ, rg_handle, attr_tag,
		&attribute_value);
	}
	*err = scha_error;

    if (scha_error != SCHA_ERR_NOERR) {
	return (NULL);
	}

    result = newStringArray(env, (int)attribute_value->array_cnt,
	    attribute_value->str_array);

    if (count != NULL)
	*count = (int)attribute_value->array_cnt;

    return (result);
}


/*
 * Class:     com_sun_cluster_agent_rgm_RgGroup
 * Method:    getNodeList
 * Signature: (Ljava/lang/String;)Ljava/util/ArrayList;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_rgm_RgGroup_getNodeList(
JNIEnv *env, jobject this, jstring zcNameQ) {

    jobject nodeList;
    char local_node[BUFFSIZE];
    char *cZCNameQ = NULL;
    int err = 0;

#if SOL_VERSION >= __s10

    struct zc_nodetab nodetab;
    jstring ret_string;
    zc_dochandle_t zone_handle = NULL;

#endif /* SOL_VERSION >= __s10 */

    if (zcNameQ != NULL) {
	const char *tmpS =
	(*env)->GetStringUTFChars(env, zcNameQ, NULL);
	cZCNameQ = strdup(tmpS);
	}
    debug printf(" START readData method for Nodelist \n");
    if (!initialized) {
	init(env);
	}

	/*
	* Initialize nodelist
	*/
    nodeList = (*env)->NewObject(env,
	    array_list_class,
	    array_list_constructor);
    if (nodeList == NULL) {
	goto end;
	}

    local_node[0] = '\0';
	(void) gethostname(local_node, sizeof (local_node));

#if SOL_VERSION >= __s10
	/*
	* Library initialization
	*/

    clconf_lib_init();

	/*
	* Open the handle to zonecfg
	*/
    zone_handle = zccfg_init_handle();
    if (zone_handle == NULL) {
	goto error;
	}
    err = zccfg_get_handle(cZCNameQ, zone_handle);
    if (err != ZC_OK) {
	goto error;
	}
	/*
	* Get the node list for this zone cluster
	*/
    err = zccfg_setnodeent(zone_handle);
    if (err != ZC_OK) {
	goto error;
	}

    while (zccfg_getnodeent(zone_handle, &nodetab) == ZC_OK) {
	ret_string = (jstring) newObjStringParam(env,
		"java/lang/String",
		nodetab.zc_nodename);

	if (ret_string != NULL &&
		strcmp(nodetab.zc_nodename, local_node) != 0) {
	    /* Update Nodelist */
	    (void) (*env)->CallObjectMethod(env,
		    nodeList,
		    array_list_add,
		    ret_string);
	} else {
	    nodeList = (*env)->NewObject(env,
		    array_list_class,
		    array_list_constructor);
	    break;
	}

	}

    error:
	(void) zccfg_endnodeent(zone_handle);
	(void) zccfg_fini_handle(zone_handle);

#endif /* SOL_VERSION >= __s10 */

    end:
    return (nodeList);



}


/*
 * Class:     com_sun_cluster_agent_rgm_RgGroup
 * Method:    getResourceGroups
 * Signature: ()Ljava/util/HashMap;
 */
JNIEXPORT jobject JNICALL
getResourceGroups(JNIEnv *env, jobject this, char *cZCNameQ) {

	/*
	* return an array of type ResourceGroupData
	*/

    jobject resourceGroupData;
    jobject rg_data_map = NULL;


    unsigned int i, j;

    int attr_value_int = 0;
    scha_str_array_t *node_list = NULL;
    scha_str_array_t *attr_value_stringarray = NULL;
    jobject node_status;
    jobject node_status_array;
    int dependencies_count;
    char **current_primaries = NULL;
    unsigned int current_primaries_count = 0;
    scha_rgstate_t state;

	/*
	* SCHA error tag
	*/
    scha_err_t scha_error;

	/*
	* RG handle.
	*/
    scha_resourcegroup_t rg_handle = NULL;

	/*
	* Specific variable used to retrieve the "CurrentPrimaries" attribute
	*/
    scha_cluster_t sc_handle = NULL;
    scha_str_array_t *rg_names = NULL;

    debug printf(" START readData method for Resource Groups \n");
    if (!initialized) {
	init(env);
	}

	/*
	* Initialize a rg_data_map HashMap to return
	*/
    rg_data_map = newObjNullParam(env, "java/util/HashMap");
    if (rg_data_map == NULL) {
	debug printf("\n Resource Groups HashMap is NULL\n");
	goto error;
	}


	/*
	* get a handle to the Cluster
	*/

    scha_error = scha_cluster_open(&sc_handle);
    if (scha_error != SCHA_ERR_NOERR) {
	sc_handle = NULL;
	goto error;
	}


	/*
	* get rg names
	*/
    if (cZCNameQ == NULL) {
	scha_error = scha_cluster_get(sc_handle,
		SCHA_ALL_RESOURCEGROUPS, &rg_names);
	} else {
	scha_error = scha_cluster_get_zone(cZCNameQ, sc_handle,
		SCHA_ALL_RESOURCEGROUPS, &rg_names);
	}

    if ((scha_error != SCHA_ERR_NOERR) || (rg_names == NULL)) {
	goto error;
	}


    debug printf("\nReading Rg data ...%d", rg_names->array_cnt);

    for (i = 0; i < rg_names->array_cnt; i++) {

	debug printf("\nReading rg data %d ", i);

	/*
	 * CurrentPrimaries array
	 */
	current_primaries = NULL;
	current_primaries_count = 0;

	/*
	 * we are going to create an ResourceGroupData object
	 * for this instance and populate it with its attributes
	 */
	resourceGroupData = (*env)->NewObject(env,
		resourceGroupDataClass,
		resourceGroupDataClassConstructor);

	if (resourceGroupData == NULL) {
	    debug printf("\n resourceGroupData Object is NULL\n");
	    goto error;
	}


	/*
	 * Open this resource group
	 */
	if (cZCNameQ == NULL) {
	    scha_error =
		    scha_resourcegroup_open(rg_names->str_array[i],
		    &rg_handle);
	} else {
	    scha_error =
		    scha_resourcegroup_open_zone(cZCNameQ,
		    rg_names->str_array[i],
		    &rg_handle);
	}
	if (scha_error != SCHA_ERR_NOERR) {
	    rg_handle = NULL;
	    goto attr_error;
	}
	/*
	 * Get the list of nodes for this resource group
	 */
	if (cZCNameQ == NULL) {
	    scha_error = scha_resourcegroup_get(rg_handle,
		    SCHA_NODELIST,
		    &node_list);
	} else {
	    scha_error = scha_resourcegroup_get_zone(cZCNameQ,
		    rg_handle,
		    SCHA_NODELIST,
		    &node_list);
	}
	if (scha_error != SCHA_ERR_NOERR) {
	    goto attr_error;
	}

	/*
	 * Fill in the attributes
	 */
	setStringField(env, resourceGroupData,
		nameID, (char *)rg_names->str_array[i]);

	/*
	 * Deal with all attributes that necessitate  * *
	 * iterating over the nodes together
	 */

	state = SCHA_RGSTATE_UNMANAGED;

	/*
	 * Status - per node status array
	 */

	current_primaries = (char **)
		malloc(sizeof (char *) * node_list->array_cnt);
	if (current_primaries == NULL)
	    goto attr_error;

	/*
	 * Construct status array
	 */
	node_status_array = (*env)->NewObjectArray(env,
		(int)node_list->array_cnt,
		rgstatus_class, NULL);
	if (node_status_array == NULL)
	    goto attr_error;

	/*
	 * For each node, get its status and compare to the current
	 */
	for (j = 0;
	j < node_list->array_cnt;
	j++) {
		/*
		* Get status
		*/
	    if (cZCNameQ == NULL) {
		scha_error = scha_resourcegroup_get(rg_handle,
			SCHA_RG_STATE_NODE,
			node_list->str_array[j],
			&attr_value_int);
	    } else {
		scha_error = scha_resourcegroup_get_zone(cZCNameQ,
			rg_handle,
			SCHA_RG_STATE_NODE,
			node_list->str_array[j],
			&attr_value_int);
	    }
	    if (scha_error != SCHA_ERR_NOERR) {
		goto attr_error;
	    }
		/*
		* Apply the rule on overall status :
		* UNMANAGED < OFFLINE < ONLINE <
		* PENDING_ONLINE < PENDING_OFFLINE <
		* ERROR_STOP_FAILED < ONLINE_FAULTED
		*/
	    if ((attr_value_int > SCHA_RGSTATE_OFFLINE) &&
		    (attr_value_int > state))
		state = attr_value_int;
	    else if ((attr_value_int ==
		    SCHA_RGSTATE_OFFLINE) &&
		    (state == SCHA_RGSTATE_UNMANAGED))
		state = attr_value_int;
	    else if ((attr_value_int ==
		    SCHA_RGSTATE_ONLINE) &&
		    ((state ==
		    SCHA_RGSTATE_UNMANAGED) ||
		    (state == SCHA_RGSTATE_OFFLINE)))
		state = attr_value_int;

		/*
		* Now construct and add * *
		* ResourceGroupStatus object to array
		*/
	    node_status =
		    (*env)->NewObject(env, rgstatus_class,
		    rgstatus_constructor,
		    (*env)->NewStringUTF(env,
		    node_list->str_array[j]),
		    getCMASSEnum(env,
		    "com/sun/cluster/agent/rgm/"
		    "ResourceGroupStatusEnum",
		    attr_value_int));
		/*
		* Add object to the array
		*/
	    (*env)->SetObjectArrayElement(env,
		    node_status_array, (int)j,
		    node_status);

		/*
		* Add element to currentPrimaries
		*/
	    if (attr_value_int == SCHA_RGSTATE_ONLINE) {
		char *name =
		strdup(node_list->str_array[j]);
		if (name == NULL)
		    goto attr_error;
		current_primaries[
			current_primaries_count++] =
			name;
	    }
	}
	(*env)->SetObjectField(env, resourceGroupData, overallStatusID,
		getCMASSEnum(env,
		"com/sun/cluster/agent/rgm/"
		"ResourceGroupStatusEnum",
		state));

	(*env)->SetBooleanField(env, resourceGroupData, managedID,
		getBooleanVal(!(state == SCHA_RGSTATE_UNMANAGED)));

	(*env)->SetBooleanField(env, resourceGroupData, onlineID,
		getBooleanVal(state == SCHA_RGSTATE_ONLINE));


	(*env)->SetObjectField(env, resourceGroupData, statusID,
		node_status_array);

	(*env)->SetObjectField(env, resourceGroupData,
		currentPrimariesID,
		newStringArray(env,
		(int)current_primaries_count,
		current_primaries));

	(*env)->SetIntField(env, resourceGroupData,
		currentPrimariesCountID,
		(int)current_primaries_count);

	if (cZCNameQ == NULL) {
	    scha_error = scha_resourcegroup_get(rg_handle,
		    SCHA_RG_MODE,
		    &attr_value_int);
	} else {
	    scha_error = scha_resourcegroup_get_zone(cZCNameQ,
		    rg_handle,
		    SCHA_RG_MODE,
		    &attr_value_int);
	}
	if (scha_error != SCHA_ERR_NOERR) {
	    goto attr_error;
	}
	/*
	 * Get the corresponding Enum
	 */
	(*env)->SetObjectField(env, resourceGroupData, modeID,
		getCMASSEnum(env,
		"com/sun/cluster/agent/rgm/"
		"ResourceGroupModeEnum",
		attr_value_int));

	(*env)->SetBooleanField(env, resourceGroupData, autoStartableID,
		getRgBooleanAttribute(env, rg_handle,
		SCHA_RG_AUTO_START, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetBooleanField(env, resourceGroupData, networkDependentID,
		getRgBooleanAttribute(env, rg_handle,
		SCHA_IMPL_NET_DEPEND, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetBooleanField(env, resourceGroupData, systemID,
		getRgBooleanAttribute(env, rg_handle,
		SCHA_RG_SYSTEM, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetBooleanField(env, resourceGroupData, failbackEnabledID,
		getRgBooleanAttribute(env, rg_handle,
		SCHA_FAILBACK, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetBooleanField(env, resourceGroupData, suspendedID,
		getRgBooleanAttribute(env, rg_handle,
		SCHA_RG_SUSP_AUTO_RECOVERY, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	if (cZCNameQ == NULL) {
	    scha_error = scha_resourcegroup_get(rg_handle,
		    SCHA_GLOBAL_RESOURCES_USED,
		    &attr_value_stringarray);
	} else {
	    scha_error = scha_resourcegroup_get_zone(cZCNameQ, rg_handle,
		    SCHA_GLOBAL_RESOURCES_USED,
		    &attr_value_stringarray);
	}
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetBooleanField(env, resourceGroupData,
		sharedStorageUsedID,
		getBooleanVal(attr_value_stringarray->is_ALL_value));

	(*env)->SetObjectField(env, resourceGroupData, nodeNamesID,
		newStringArray(env,
		(int)node_list->array_cnt,
		node_list->str_array));

	(*env)->SetIntField(env, resourceGroupData,
		desiredPrimariesCountID,
		getRgIntAttribute(env, rg_handle,
		SCHA_DESIRED_PRIMARIES, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetIntField(env, resourceGroupData, maxPrimariesID,
		getRgIntAttribute(env, rg_handle,
		SCHA_MAXIMUM_PRIMARIES, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	dependencies_count = 0;
	(*env)->SetObjectField(env, resourceGroupData, dependenciesID,
		getRgStringArrayAttribute(env, rg_handle,
		SCHA_RG_DEPENDENCIES, &dependencies_count,
		&scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetIntField(env, resourceGroupData, dependenciesCountID,
		dependencies_count);


	(*env)->SetObjectField(env, resourceGroupData, directoryPathID,
		getRgStringAttribute(env, rg_handle, SCHA_PATHPREFIX,
		&scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetObjectField(env, resourceGroupData, descriptionID,
		getRgStringAttribute(env, rg_handle, SCHA_RG_DESCRIPTION,
		&scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetIntField(env, resourceGroupData, failureIntervalID,
		getRgIntAttribute(env, rg_handle, SCHA_PINGPONG_INTERVAL,
		&scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetObjectField(env, resourceGroupData, affinitiesID,
		getRgStringArrayAttribute(env,
		rg_handle, SCHA_RG_AFFINITIES, NULL, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetObjectField(env, resourceGroupData, projectNameID,
		getRgStringAttribute(env,
		rg_handle, SCHA_RG_PROJECT_NAME, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetObjectField(env, resourceGroupData, slmTypeID,
		getRgStringAttribute(env,
		rg_handle, SCHA_RG_SLM_TYPE, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetObjectField(env, resourceGroupData, slmpSetTypeID,
		getRgStringAttribute(env,
		rg_handle, SCHA_RG_SLM_PSET_TYPE, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetIntField(env, resourceGroupData, slmCPUSharesID,
		getRgIntAttribute(env,
		rg_handle, SCHA_RG_SLM_CPU_SHARES, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetIntField(env, resourceGroupData, slmpSetMinID,
		getRgIntAttribute(env,
		rg_handle, SCHA_RG_SLM_PSET_MIN, &scha_error, cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	(*env)->SetObjectField(env, resourceGroupData, resourceNamesID,
		getRgStringArrayAttribute(env,
		rg_handle, SCHA_RESOURCE_LIST, NULL, &scha_error,
		cZCNameQ));
	if (scha_error != SCHA_ERR_NOERR)
	    goto attr_error;

	/*
	 * Now put this ResourceGroupData object into the map
	 */
	PUT(rg_data_map, rg_names->str_array[i], resourceGroupData);


	attr_error:
    if (rg_handle != NULL) {
	(void) scha_resourcegroup_close(rg_handle);
	}
	if (current_primaries != NULL) {
	    for (j = 0; j < current_primaries_count; j++) {
		free(current_primaries[j]);
	    }
	    free(current_primaries);
	    current_primaries = NULL;
	}
	(*env)->DeleteLocalRef(env, resourceGroupData);
	}

    error:
    if (sc_handle != NULL) {
	(void) scha_cluster_close(sc_handle);
	}
    return (rg_data_map);
}



/*
 * Class:     com_sun_cluster_agent_rgm_RgGroup
 * Method:    readDataJNI
 * Signature: (Ljava/lang/String;)Ljava/util/HashMap;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_rgm_RgGroup_readDataJNI__Ljava_lang_String_2(
JNIEnv *env, jobject this, jstring zcNameQ) {

    char *cZCNameQ = NULL;
    if (zcNameQ != NULL) {
	const char *tmpS =
	(*env)->GetStringUTFChars(env, zcNameQ, NULL);
	cZCNameQ = strdup(tmpS);
	}
    return (getResourceGroups(env, this, cZCNameQ));
}
