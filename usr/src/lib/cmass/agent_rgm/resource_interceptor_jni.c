/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Node mbean interceptor
 */

#pragma ident	"@(#)resource_interceptor_jni.c	1.20	08/07/23 SMI"

#include "com/sun/cluster/agent/rgm/ResourceInterceptor.h"

#include <jniutil.h>
#include <string.h>
#include <stdio.h>
#include <scha.h>
#include <stdlib.h>
#include <scadmin/scrgadm.h>
#include <rgm/rgm_cnfg.h>

/*
 * Enable this to get printf debug
 */
#define	DEBUG_RESOURCE_INTERCEPTOR 0

#if DEBUG_RESOURCE_INTERCEPTOR
#include <assert.h>
#endif

extern jobject createRtProperty(JNIEnv * env, RtrParam_T * property);
extern jobject createRtPropertyFromRGM(JNIEnv *env, rgm_param_t *property);

/*
 * Resource helper function to retrieve a string type value and transform it
 * to a JNI jobject element.
 */
static jobject
    getResourceStringAttribute(JNIEnv * env,
    scha_resource_t r_handle,
    const char *attr_tag,
    scha_err_t *err)
{

	scha_err_t scha_error;
	char *attribute_value = NULL;
	jobject result = NULL;

	/*
	 * Calling SCHA API to retrieve the attribute value
	 */
	scha_error = scha_resource_get(r_handle, attr_tag, &attribute_value);
	*err = scha_error;

	if (scha_error != SCHA_ERR_NOERR) {
		attribute_value = "SCHA ERROR";
	}
	result = newObjStringParam(env, "java/lang/String", attribute_value);
	return (result);
}

#if 0
/*
 * Resource helper function to retrieve a int type value and transform it to
 * a JNI jobject element.
 */
static jobject
    getResourceIntAttribute(JNIEnv * env, scha_resource_t r_handle,
    const char *attr_tag, scha_err_t *err)
{

	scha_err_t scha_error;
	int attribute_value = -1;
	jobject result = NULL;

	/*
	 * Calling SCHA API to retrieve the attribute value
	 */
	scha_error = scha_resource_get(r_handle, attr_tag, &attribute_value);
	*err = scha_error;

	if (scha_error != SCHA_ERR_NOERR)
		return (NULL);

	result = newObjInt32Param(env, "java/lang/Integer", attribute_value);
	return (result);
}
#endif

/*
 * Resource helper function to retrieve a string array type value and transform
 * it to a JNI jobject element.
 */
static jobject
    getResourceStringArrayAttribute(JNIEnv * env,
    scha_resource_t r_handle, const char *attr_tag, scha_err_t *err)
{

	scha_err_t scha_error;
	scha_str_array_t *attribute_value = NULL;
	jobject result = NULL;

	/*
	 * Calling SCHA API to retrieve the attribute value
	 */
	scha_error = scha_resource_get(r_handle, attr_tag, &attribute_value);
	*err = scha_error;

	if (scha_error != SCHA_ERR_NOERR)
		return (NULL);
	/*
	 * Construct array
	 */
	result = newStringArray(env, (int)attribute_value->array_cnt,
	    attribute_value->str_array);

	return (result);
}

/*
 * Resource helper to set the current value in a ResourceProperty object
 */
static void
    setPropertyValue(JNIEnv * env, int same_value, const char *nodename,
    jobject property,
    const scha_prop_type_t value_type,
    const int int_value,
    const char *string_value,
    const boolean_t boolean_value,
    const char *enum_value,
    const scha_str_array_t *stringarray_value,
    jobject pernodeMap)
{

	jobject value = NULL;
	jobject nodeName = NULL;
	jclass property_class = NULL;
	jmethodID set_value = NULL;
	jmethodID put = NULL;

	jmethodID set_pernode_value = NULL;
	jclass pernodeClass = NULL;

	switch (value_type) {
	case SCHA_PTYPE_STRING:
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyString");
		if (property_class == NULL)
			goto error;
		value = newObjStringParam(env, "java/lang/String",
		    string_value);
		if (value == NULL)
			goto error;
		if (same_value)
			set_value = (*env)->GetMethodID(env,
			    property_class, "setValue",
			    "(Ljava/lang/String;)V");
		break;

	case SCHA_PTYPE_INT:
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyInteger");
		if (property_class == NULL)
			goto error;
		value = newObjInt32Param(env, "java/lang/Integer", int_value);
		if (value == NULL)
			goto error;
		if (same_value) {
			set_value = (*env)->GetMethodID(env, property_class,
			    "setValue",
			    "(Ljava/lang/Integer;)V");
		}
		break;

	case SCHA_PTYPE_BOOLEAN:
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyBoolean");
		if (property_class == NULL)
			goto error;
		value = newObjStringParam(env, "java/lang/Boolean",
		    boolean_value == B_TRUE ? "true" : "false");
		if (value == NULL)
			goto error;
		if (same_value)
			set_value = (*env)->GetMethodID(env, property_class,
			    "setValue",
			    "(Ljava/lang/Boolean;)V");
		break;

	case SCHA_PTYPE_ENUM:
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/ResourcePropertyEnum");
		if (property_class == NULL)
			goto error;
		value = newObjStringParam(env, "java/lang/String", enum_value);
		if (value == NULL)
			goto error;
		if (same_value)
			set_value = (*env)->GetMethodID(env,
			    property_class, "setValue",
			    "(Ljava/lang/String;)V");
		break;

	case SCHA_PTYPE_STRINGARRAY:
		property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyStringArray");
		if (property_class == NULL)
			goto error;
		set_value = (*env)->GetMethodID(env,
		    property_class, "setValue",
		    "([Ljava/lang/String;)V");
		value = newStringArray(env, (int)stringarray_value->array_cnt,
		    stringarray_value->str_array);
		if (value == NULL) {
			goto error;
		}
		break;
	default:
		goto error;
	}
	if (same_value) {
		(*env)->CallVoidMethod(env, property, set_value, value);
	} else {
		pernodeClass =  (*env)->FindClass(env, "java/util/HashMap");
		if (pernodeClass == NULL)
			goto error;
		put =
		    (*env)->GetMethodID(env, pernodeClass,
		    "put",
		    "(Ljava/lang/Object;Ljava/lang/Object;)"
		    "Ljava/lang/Object;");

		nodeName = newObjStringParam(env, "java/lang/String", nodename);
		(void) (*env)->CallObjectMethod(env, pernodeMap, put,
		    nodeName, value);

		set_pernode_value = (*env)->GetMethodID(env,
		    property_class, "setPernodeValue",
		    "(Ljava/util/HashMap;)V");
		if (set_pernode_value == NULL)
			goto error;
		(*env)->CallVoidMethod(env, property,
		    set_pernode_value, pernodeMap);
	}

error:
	delRef(env, property_class);
	delRef(env, value);
}

/*
 * Resource helper function to check whether the given value for per-node
 * extension property is same on all the nodes.
 */

static int
    is_same_per_node_ext_prop_value(scha_resource_t r_handle,
    const char *prop_name,
    scha_prop_type_t propType, const scha_str_array_t *node_list)
{
	uint_t i = 0;
	scha_extprop_value_t *prop_value = NULL;
	char *tmp_prop_value_str = NULL;
	int tmp_prop_value_int = 0;
	boolean_t tmp_prop_value_bool = B_FALSE;
	char *tmp_prop_value_enum = NULL;

	scha_err_t scha_error;
	switch (propType) {
	case SCHA_PTYPE_STRING :

		for (i = 0; i < (node_list->array_cnt); i++) {

			if (prop_value != NULL) {
				free(tmp_prop_value_str);
				tmp_prop_value_str =
				    strdup(prop_value->val.val_str);
			}
			scha_error = scha_resource_get(r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
			if (scha_error != SCHA_ERR_NOERR)
				goto error;

			if ((tmp_prop_value_str) &&
			    (strcmp(tmp_prop_value_str,
			    prop_value->val.val_str) != 0)) {
				free(tmp_prop_value_str);
				return (0);
			}
		}
		if (tmp_prop_value_str)
			free(tmp_prop_value_str);
		break;
	case SCHA_PTYPE_INT:

		for (i = 0; i < (node_list->array_cnt); i++) {
			if (i > 0)
				tmp_prop_value_int = prop_value->val.val_int;
			scha_error = scha_resource_get(r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
			if (scha_error != SCHA_ERR_NOERR)
				goto error;

			if ((i) && (tmp_prop_value_int !=
			    prop_value->val.val_int))
				return (0);
		}
		break;
	case SCHA_PTYPE_BOOLEAN:

		for (i = 0; i < ((int)node_list->array_cnt); i++) {
			if (i > 0)
				tmp_prop_value_bool =
				    prop_value->val.val_boolean;
			scha_error = scha_resource_get(r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
			if (scha_error != SCHA_ERR_NOERR)
				goto error;
			if ((i) && (tmp_prop_value_bool !=
			    prop_value->val.val_boolean))
				return (0);
		}
		break;
	case SCHA_PTYPE_ENUM:

		for (i = 0; i < ((int)node_list->array_cnt); i++) {
			if (prop_value != NULL) {
				free(tmp_prop_value_enum);
				tmp_prop_value_enum =
				    strdup(prop_value->val.val_enum);
			}
			scha_error = scha_resource_get(r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
			if (scha_error != SCHA_ERR_NOERR)
				goto error;
			if (tmp_prop_value_enum &&
			    strcmp(tmp_prop_value_enum,
			    prop_value->val.val_enum) != 0) {
				free(tmp_prop_value_enum);
				return (0);
			}
		}
		if (tmp_prop_value_enum)
			free(tmp_prop_value_enum);
		break;
	default:
		break;
	}
	return (1);
error:
	return (-1);
}

/*
 * Resource helper function to retrieve an extention property, transform it
 * into the appropriate ResourceProperty object and add it to the given List.
 */
static scha_err_t
    addExtProperty(JNIEnv * env, scha_resource_t r_handle, jobject list,
    jobject per_node_list, const scha_str_array_t *node_list,
    const char *prop_name, rgm_rt_t *rt)
{

	scha_err_t scha_error;
	scha_errmsg_t scha_status;

	scha_extprop_value_t *prop_value = NULL;
	jobject property = NULL;
	jclass linked_list_class = NULL;
	jclass per_node_class = NULL;
	jmethodID add;
	jmethodID pernodePropadd;
	int i = 0;
	int property_found = 0;

	/*
	 * For per-node extension properties.
	 */
	boolean_t pernode;
	scha_prop_type_t prop_type;
	jobject pernodePropName = NULL;
	jobject pernodeValueMap = NULL;
	int same_value = 0;
	rgm_param_t **paramtable = NULL;
	rgm_param_t *param = NULL;

	/* For per-node extension properties. */
	jobject pernodeMap;

	/*
	 * Get the extension property value
	 */
	scha_error = scha_resource_get(r_handle, SCHA_EXTENSION, prop_name,
	    &prop_value);
	if (scha_error != SCHA_ERR_NOERR)
		goto error;

	/*
	 * Get the property information in this resource type
	 */
	paramtable = rt->rt_paramtable;
	i = 0;
	if (paramtable != NULL)
		param = paramtable[i];
	while ((param != NULL) && (!property_found)) {
		char *buf = param->p_name;

		if (buf == NULL) {
			param = paramtable[++i];
			continue;
		}
		if (strcmp(prop_name, buf) == 0) {
			property = createRtPropertyFromRGM(env,
					param);
			pernode = param->p_per_node;
			prop_type = param->p_type;
			property_found = 1;
		}
		param = paramtable[++i];
	}
	if (property == NULL)
		goto error;
	/*
	 * Set the property value
	 */
	if (pernode == B_TRUE) {
		same_value = is_same_per_node_ext_prop_value(r_handle,
		    prop_name, prop_type, node_list);
		if (same_value) {
			scha_error = scha_resource_get(r_handle, SCHA_EXTENSION,
			    prop_name, &prop_value);
			setPropertyValue(env, 1, NULL, property,
			    prop_type,
			    prop_value->val.val_int,
			    prop_value->val.val_str,
			    prop_value->val.val_boolean,
			    prop_value->val.val_enum,
			    prop_value->val.val_strarray, NULL);
		} else {
			pernodeMap = newObjNullParam(env, "java/util/HashMap");
			if (pernodeMap == NULL)
				goto error;

			for (i = 0; i < ((int)node_list->array_cnt); i++)  {
				scha_error = scha_resource_get(r_handle,
				    SCHA_EXTENSION_NODE,
				    prop_name,
				    node_list->str_array[i],
				    &prop_value);
				if (scha_error != SCHA_ERR_NOERR) {
					goto error;
				}
				setPropertyValue(env, 0,
				    node_list->str_array[i],
				    property,
				    prop_type,
				    prop_value->val.val_int,
				    prop_value->val.val_str,
				    prop_value->val.val_boolean,
				    prop_value->val.val_enum,
				    prop_value->val.val_strarray,
				    pernodeMap);

				prop_value = NULL;

			}
		}

			per_node_class = (*env)->FindClass(env,
			    "java/util/LinkedList");
			if (per_node_class == NULL) {
				goto error;
			}
			pernodePropName = newObjStringParam(env,
			    "java/lang/String", prop_name);
			if (pernodePropName == NULL) {
				goto error;
			}

			pernodePropadd = (*env)->GetMethodID(env,
			    per_node_class, "add", "(Ljava/lang/Object;)Z");
			(void) (*env)->CallObjectMethod(env, per_node_list,
			    pernodePropadd, pernodePropName);
	} else {
		scha_error = scha_resource_get(r_handle, SCHA_EXTENSION,
		    prop_name, &prop_value);
		if (scha_error != SCHA_ERR_NOERR)
			goto error;
		setPropertyValue(env, 1, NULL,
		    property, prop_type,
		    prop_value->val.val_int,
		    prop_value->val.val_str,
		    prop_value->val.val_boolean,
		    prop_value->val.val_enum,
		    prop_value->val.val_strarray, NULL);
	}

	/*
	 * Add it to the list and return
	 */
	linked_list_class = (*env)->FindClass(env,
	    "java/util/LinkedList");

	if (linked_list_class == NULL)
		goto error;
	add = (*env)->GetMethodID(env, linked_list_class, "add",
	    "(Ljava/lang/Object;)Z");
	(void) (*env)->CallObjectMethod(env, list, add, property);
error:
	delRef(env, property);
	delRef(env, linked_list_class);
	return (scha_error);
}

/*
 * Resource helper function to retrieve a system property, transform it
 * into the appropriate ResourceProperty object and add it to the given List.
 */
static scha_err_t
    addSystemProperty(JNIEnv * env, scha_resource_t r_handle, jobject list,
			rgm_rt_t *rt)
{

	scha_err_t scha_error = SCHA_ERR_INTERNAL;
	scha_errmsg_t scha_status;
	char *tag = NULL;

	/*
	 * different type of property value
	 */
	scha_prop_type_t value_type;
	int int_value = 0;
	char *string_value = NULL;
	boolean_t boolean_value = B_FALSE;
	scha_str_array_t *stringarray_value = NULL;

	jobject property = NULL;
	jclass linked_list_class;
	jmethodID add;

	int i;
	rgm_param_t **paramtable = NULL;
	rgm_param_t *param = NULL;

	linked_list_class = (*env)->FindClass(env, "java/util/LinkedList");
	if (linked_list_class == NULL)
		goto error;
	add = (*env)->GetMethodID(env, linked_list_class, "add",
	    "(Ljava/lang/Object;)Z");

	/*
	 * Browse all the system properties in the resource type, create the
	 * appropriate ResourceType object, retrieve and affect the current
	 * value.
	 */
	paramtable = rt->rt_paramtable;
	i = 0;
	if (paramtable != NULL) {
		param = paramtable[i];
	}
	while (param != NULL) {
		if (param->p_extension == B_FALSE) {
			property = createRtPropertyFromRGM(env, param);
		} else {
			param = paramtable[++i];
			continue;
		}
		value_type = param->p_type;
		tag = param->p_name;

		/*
		 * Get the current value from the SCHA API
		 */
		switch (value_type) {
		case SCHA_PTYPE_ENUM:
			scha_error = scha_resource_get(r_handle, tag,
			    &int_value);
			if (scha_error != SCHA_ERR_NOERR)
				goto error;
			switch (int_value) {
			case SCHA_FOMODE_NONE:
				string_value = strdup("NONE");
				break;
			case SCHA_FOMODE_HARD:
				string_value = strdup("HARD");
				break;
			case SCHA_FOMODE_SOFT:
				string_value = strdup("SOFT");
				break;
			default:
				string_value = NULL;
				break;
			}
			if (string_value == NULL)
				goto error;
			break;

		case SCHA_PTYPE_STRING:
			scha_error = scha_resource_get(r_handle, tag,
			    &string_value);
			break;

		case SCHA_PTYPE_INT:
		case SCHA_PTYPE_UINT:
			scha_error = scha_resource_get(r_handle, tag,
			    &int_value);
			break;

		case SCHA_PTYPE_BOOLEAN:
			scha_error = scha_resource_get(r_handle, tag,
			    &boolean_value);
			break;

		case SCHA_PTYPE_STRINGARRAY:
			scha_error = scha_resource_get(r_handle, tag,
			    &stringarray_value);
			break;

		case SCHA_PTYPE_UINTARRAY:
		default:
			goto error;
		}
		if (scha_error != SCHA_ERR_NOERR)
			goto error;

		/*
		 * Set the property value
		 */
		setPropertyValue(env, 1, NULL, property, value_type,
		    int_value,
		    string_value,
		    boolean_value,
		    string_value,
		    stringarray_value, NULL);

		/*
		 * Add it to the list
		 */
		(void) (*env)->CallObjectMethod(env, list, add, property);
		param = paramtable[++i];
	}

error:
	delRef(env, property);
	delRef(env, linked_list_class);
	return (scha_error);
}

/*
 * given a resource state as input, see if the sub_state affects it
 */
scha_rsstate_t
    overall_state(scha_rsstate_t state, scha_rsstate_t sub_state)
{

	/*
	 * Apply the rule on overall state : OFFLINE < STOPPING <
	 * STARTING < DETACHED < ONLINE < * ONLINE_NOT_MONITORED <
	 * MONITOR_FAILED < * START_FAILED < STOP_FAILED
	 */
	if (state == SCHA_RSSTATE_OFFLINE) {
		state = sub_state;
	} else if (state ==
	    SCHA_RSSTATE_ONLINE) {
		state =
		    (sub_state >= SCHA_RSSTATE_START_FAILED &&
		    sub_state <= SCHA_RSSTATE_ONLINE_NOT_MONITORED) ?
		    sub_state : state;
	} else if (state == SCHA_RSSTATE_START_FAILED) {
		state = sub_state == SCHA_RSSTATE_STOP_FAILED ?
		    sub_state : state;
	} else if (state == SCHA_RSSTATE_MONITOR_FAILED) {
		state = (sub_state == SCHA_RSSTATE_START_FAILED ||
		    sub_state == SCHA_RSSTATE_STOP_FAILED) ?
		    sub_state : state;
	} else if (state == SCHA_RSSTATE_ONLINE_NOT_MONITORED) {
		state = (sub_state == SCHA_RSSTATE_START_FAILED ||
		    sub_state == SCHA_RSSTATE_STOP_FAILED ||
		    sub_state == SCHA_RSSTATE_MONITOR_FAILED) ?
		    sub_state : state;
	} else if (state == SCHA_RSSTATE_STARTING) {
		state = (sub_state != SCHA_RSSTATE_OFFLINE &&
		    sub_state != SCHA_RSSTATE_STOPPING) ?
		    sub_state : state;
	} else if (state == SCHA_RSSTATE_STOPPING) {
		state = sub_state != SCHA_RSSTATE_OFFLINE ?
		    sub_state : state;
	} else if (state == SCHA_RSSTATE_DETACHED) {
		state = (sub_state != SCHA_RSSTATE_OFFLINE &&
		    sub_state != SCHA_RSSTATE_STARTING &&
		    sub_state != SCHA_RSSTATE_STOPPING) ?
		    sub_state : state;
	}
	return (state);
}

/*
 * A couple of helper macros for things we do a lot,
 * * they contain references to local variables in the
 * * function below
 */
#define	PUT_ATTR(MAP, NAME, VALUE)		\
	PUT(MAP, NAME, (*env)->NewObject(env,	\
	    attribute_class,			\
	    attribute_constructor,		\
	    (*env)->NewStringUTF(env, NAME),	\
	    VALUE))

#define	PUT(MAP, NAME, VALUE)						\
	(*env)->CallObjectMethod(env,					\
				    MAP,				\
				    map_put,				\
				    (*env)->NewStringUTF(env, NAME),	\
				    VALUE)

/*
 * Class:     com_sun_cluster_agent_rgm_ResourceInterceptor
 * Method:    fillCache
 * Signature: ()Ljava/util/Map;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_rgm_ResourceInterceptor_fillCache
	(JNIEnv * env, jobject this) {


	/*
	 * return a Map of Maps containing all instances
	 * key = instanceName
	 * value = Map
	 *		key=attributeName
	 *		value=corresponding Attribute object
	 */

	jobject cache_map = NULL;
	jobject instance_map = NULL;
	jclass map_class = NULL;
	jmethodID map_put;

	jclass attribute_class = NULL;
	jmethodID attribute_constructor = NULL;

	int i, j;

	/*
	 * SCHA error tag
	 */
	scha_err_t scha_error;

	/*
	 * SCHA cluster handle
	 */
	scha_cluster_t sc_handle = NULL;

	/*
	 * Resource group handle.
	 */
	scha_resourcegroup_t rg_handle = NULL;
	scha_str_array_t *rg_names = NULL;
	scha_errmsg_t scha_status;

	/*
	 * Resource handle.
	 */
	scha_resource_t r_handle = NULL;
	scha_str_array_t *resource_list = NULL;

	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};

	/*
	 * Used as tmp variable storing return values
	 */
	scha_str_array_t *attr_value_stringarray = NULL;
	int attr_value_int = 0;
	char *rt_name = NULL;

	jclass state_class = NULL;
	jclass status_class = NULL;
	jmethodID state_constructor;
	jmethodID status_constructor;

	scha_str_array_t *node_list = NULL;

	/*
	 * get our references to 'attribute_class'
	 */

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;


	/*
	 * get constructor
	 */
	attribute_constructor =
	    (*env)->GetMethodID(env,
	    attribute_class,
	    "<init>",
	    "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;


	/*
	 * Create the map we are going to return
	 */
	cache_map = newObjNullParam(env, "java/util/HashMap");
	if (cache_map == NULL)
		goto error;

	/*
	 * get our references to map_class and the put method
	 */
	map_class = (*env)->FindClass(env, "java/util/HashMap");
	if (map_class == NULL)
		goto error;
	map_put =
	    (*env)->GetMethodID(env,
	    map_class,
	    "put",
	    "(Ljava/lang/Object;Ljava/lang/Object;)"
	    "Ljava/lang/Object;");
	if (map_put == NULL)
		goto error;

	/*
	 * Load ResourceRgmState class
	 */
	state_class = (*env)->FindClass(env,
	    "com/sun/cluster/agent/rgm/ResourceRgmState");
	if (state_class == NULL)
		goto error;

	/*
	 * Get constructor
	 */
	state_constructor =
	    (*env)->GetMethodID(env, state_class,
	    "<init>",
	    "(Ljava/lang/String;"
	    "Lcom/sun/cluster/agent/rgm/"
	    "ResourceRgmStateEnum;)V");

	/*
	 * Load ResourceFaultMonitorStatus class
	 */
	status_class = (*env)->FindClass(env,
	    "com/sun/cluster/agent/rgm/"
	    "ResourceFaultMonitorStatus");
	if (status_class == NULL)
		goto error;

	/*
	 * Get constructor
	 */
	status_constructor =
	    (*env)->GetMethodID(env, status_class,
	    "<init>",
	    "(Ljava/lang/String;"
	    "Lcom/sun/cluster/agent/rgm/ResourceFaultMonitorStatusEnum;"
	    "Ljava/lang/String;)V");

	/*
	 * get a handle to the Cluster
	 */
    start_fillcache:
	scha_error = scha_cluster_open(&sc_handle);
	if (scha_error != SCHA_ERR_NOERR) {
		sc_handle = NULL;
		goto error;
	}
	/*
	 * get rg names
	 */
	scha_error = scha_cluster_get(sc_handle, SCHA_ALL_RESOURCEGROUPS,
	    &rg_names);
	if ((scha_error != SCHA_ERR_NOERR) || (rg_names == NULL))
		goto error;

	/*
	 * For each resource group find its resources
	 */
	for (i = 0; i < (int)rg_names->array_cnt; i++) {

		rgm_rt_t *rt =  NULL;
		/*
		 * Get the resource group handle
		 */
		rg_handle = NULL;
		scha_error = scha_resourcegroup_open(
		    rg_names->str_array[i], &rg_handle);
		if (scha_error != SCHA_ERR_NOERR) {
			rg_handle = NULL;
			goto rg_error;
		}
		/*
		 * Getting the list of resources
		 */
		scha_error = scha_resourcegroup_get(rg_handle,
		    SCHA_RESOURCE_LIST, &resource_list);
		if (scha_error != SCHA_ERR_NOERR)
			goto rg_error;

		/* Initialize the orb */
		if (rgm_orbinit().err_code != SCHA_ERR_NOERR)
			goto rg_error;

		/*
		 * Iterate over these resources
		 */

		for (j = 0; j < (int)resource_list->array_cnt; j++) {

			/*
			 * we are going to create a map for this instance and
			 * populate it with its attributes
			 */

			instance_map = newObjNullParam(env,
			    "java/util/HashMap");
			if (instance_map == NULL)
				goto error;

			r_handle = NULL;
			scha_error =
			    scha_resource_open(resource_list->str_array[j],
			    NULL, &r_handle);

			if (scha_error != SCHA_ERR_NOERR) {
				r_handle = NULL;
				goto attr_error;
			}

			PUT_ATTR(instance_map, "Name",
			    (*env)->NewStringUTF(env, (char *)
				resource_list->str_array[j]));

			{
				int k;

				scha_rsstate_t state = SCHA_RSSTATE_OFFLINE;
				scha_status_value_t *status;
				jobject node_state;
				jobject node_status;

				/*
				 * Get the list of nodes on this * *
				 * resource group
				 */
				scha_error = scha_resourcegroup_get(rg_handle,
				    SCHA_NODELIST,
				    &attr_value_stringarray);
				if (scha_error != SCHA_ERR_NOERR)
					goto attr_error;
				node_list = attr_value_stringarray;

				/*
				 * Construct array for node states
				 */
				node_state = (*env)->NewObjectArray(env,
				    (int)attr_value_stringarray->array_cnt,
				    state_class, NULL);
				if (node_state == NULL)
					goto attr_error;
				/*
				 * Construct array for node statuses
				 */
				node_status = (*env)->NewObjectArray(env,
				    (int)attr_value_stringarray->array_cnt,
				    status_class, NULL);
				if (node_status == NULL)
					goto attr_error;

				/*
				 * For each node
				 */
				for (k = 0;
				    k <
				    (int)attr_value_stringarray->array_cnt;
				    k++) {
					/*
					 * Get state
					 */
					scha_error =
					    scha_resource_get(r_handle,
					    SCHA_RESOURCE_STATE_NODE,
					    attr_value_stringarray->
					    str_array[k],
					    &attr_value_int);

					if (scha_error != SCHA_ERR_NOERR)
						goto attr_error;

					/*
					 * Get overall state
					 */
					state = overall_state(state,
					    attr_value_int);

					/*
					 * add state to state array
					 */
					(*env)->SetObjectArrayElement(env,
					    node_state, k,
					    (*env)->NewObject(env, state_class,
						state_constructor,
						(*env)->NewStringUTF(env,
						    attr_value_stringarray->
						    str_array[k]),
						getCMASSEnum(env,
						    "com/sun/cluster/agent/"
						    "rgm/ResourceRgmStateEnum",
						    attr_value_int)));

					/*
					 * Get status
					 */
					scha_error =
					    scha_resource_get(r_handle,
					    SCHA_STATUS_NODE,
					    attr_value_stringarray->
					    str_array[k],
					    &status);

					if (scha_error != SCHA_ERR_NOERR)
						goto attr_error;
					/*
					 * add status to status array
					 */
					(*env)->SetObjectArrayElement(env,
					    node_status, k,
					    (*env)->NewObject(env,
						status_class,
						status_constructor,
						(*env)->NewStringUTF(env,
						    attr_value_stringarray->
						    str_array[k]),
						getCMASSEnum(env,
						    "com/sun/cluster/agent/"
						    "rgm/ResourceFault"
						    "MonitorStatusEnum",
						    status->status),
						(*env)->NewStringUTF(env,
						    status->status_msg)));
				}

				PUT_ATTR(instance_map, "OverallState",
				    getCMASSEnum(env,
					"com/sun/cluster/agent/rgm/"
					"ResourceRgmStateEnum",
					state));
				PUT_ATTR(instance_map, "RgmStates",
				    node_state);
				PUT_ATTR(instance_map,
				    "FaultMonitorStatus",
				    node_status);
			}

			PUT_ATTR(instance_map, "Type",
			    getResourceStringAttribute(env, r_handle,
				SCHA_TYPE, &scha_error));
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;

			PUT_ATTR(instance_map, "TypeVersion",
			    getResourceStringAttribute(env, r_handle,
				SCHA_TYPE_VERSION, &scha_error));
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;

			PUT_ATTR(instance_map, "Description",
			    getResourceStringAttribute(env, r_handle,
				SCHA_R_DESCRIPTION, &scha_error));
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;

			PUT_ATTR(instance_map, "ResourceGroupName",
			    getResourceStringAttribute(env, r_handle,
				SCHA_GROUP, &scha_error));
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;

			{
				/*
				 * Get the resource enabled status on each node.
				 */
				int k = 0;
				int enabled_state = 0;
				scha_switch_t enabled_swtch =
				    SCHA_SWITCH_DISABLED;
				uint_t enabled_count = 0;
				jclass enabledStateClass = NULL;
				jobject enabledStateMap = NULL;
				jobject  nodeName = NULL;
				jmethodID put;

				enabledStateMap = newObjNullParam(env,
				    "java/util/HashMap");

				if (enabledStateMap == NULL)
					goto attr_error;

				enabledStateClass =  (*env)->FindClass(env,
				    "java/util/HashMap");
				if (enabledStateClass == NULL)
					goto attr_error;

				put =
				    (*env)->GetMethodID(env, enabledStateClass,
				    "put",
				    "(Ljava/lang/Object;Ljava/lang/Object;)"
				    "Ljava/lang/Object;");

				for (k = 0; k < (int)node_list->array_cnt;
				    k++) {
					scha_error =
					    scha_resource_get(r_handle,
					    SCHA_ON_OFF_SWITCH_NODE,
					    node_list->str_array[k],
					    &enabled_swtch);

					if (scha_error != SCHA_ERR_NOERR) {
						goto attr_error;
					}

					if (enabled_swtch !=
					    SCHA_SWITCH_DISABLED)
						enabled_count++;

					nodeName = newObjStringParam(env,
					    "java/lang/String",
					    node_list->str_array[k]);
					(*env)->CallObjectMethod(env,
					    enabledStateMap,
					    put, nodeName, newBoolean(
					    env,
				(enabled_swtch != SCHA_SWITCH_DISABLED)));

				}
				if (enabled_count == 0)
					enabled_state = 0;
				else if (enabled_count == node_list->array_cnt)
					enabled_state = 1;
				else
					enabled_state = 2;

				PUT_ATTR(instance_map, "Enabled",
				    newObjInt32Param(env, "java/lang/Integer",
				    enabled_state));

				PUT_ATTR(instance_map, "PernodeEnabledStatus",
				    enabledStateMap);
			}
			{
				/*
				 * Get the resource monitored status
				 * on each node.
				 */
				int k = 0;
				scha_switch_t monitored_swtch =
				    SCHA_SWITCH_DISABLED;
				uint_t monitored_count = 0;
				int monitored_state = 0;
				jclass monitoredStateClass = NULL;
				jobject monitoredStateMap = NULL;
				jobject  nodeName = NULL;
				jmethodID put;

				monitoredStateMap = newObjNullParam(env,
				    "java/util/HashMap");
				if (monitoredStateMap == NULL)
					goto error;

				monitoredStateClass =  (*env)->FindClass(env,
				    "java/util/HashMap");
				if (monitoredStateClass == NULL)
					goto error;

				put =
				    (*env)->GetMethodID(env,
				    monitoredStateClass,
				    "put",
				    "(Ljava/lang/Object;Ljava/lang/Object;)"
				    "Ljava/lang/Object;");

				for (k = 0; k < (int)node_list->array_cnt;
				    k++) {
					scha_error =
					    scha_resource_get(r_handle,
					    SCHA_MONITORED_SWITCH_NODE,
					    node_list->str_array[k],
					    &monitored_swtch);


					if (scha_error != SCHA_ERR_NOERR) {
						goto attr_error;
					}

					if (monitored_swtch !=
					    SCHA_SWITCH_DISABLED)
						monitored_count++;

					nodeName = newObjStringParam(env,
					    "java/lang/String",
					    node_list->str_array[k]);
					(*env)->CallObjectMethod(env,
					    monitoredStateMap,
					    put, nodeName, newBoolean(env,
				(monitored_swtch != SCHA_SWITCH_DISABLED)));

				}
				if (monitored_count == 0)
					monitored_state = 0;
				else if (
				    monitored_count == node_list->array_cnt)
					monitored_state = 1;
				else
					monitored_state = 2;

				PUT_ATTR(instance_map, "Monitored",
				    newObjInt32Param(env, "java/lang/Integer",
				    monitored_state));

				PUT_ATTR(instance_map, "PernodeMonitoredStatus",
				    monitoredStateMap);
			}
			{
				scha_error =
				    scha_resource_get(r_handle,
				    SCHA_RESOURCE_STATE,
				    &attr_value_int);
				if (scha_error != SCHA_ERR_NOERR)
					goto attr_error;
				PUT_ATTR(instance_map, "Detached",
				    newBoolean(env,
					attr_value_int !=
					SCHA_RSSTATE_DETACHED));
			}

			PUT_ATTR(instance_map, "ProjectName",
			    getResourceStringAttribute(env, r_handle,
			    SCHA_RESOURCE_PROJECT_NAME, &scha_error));
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;

			PUT_ATTR(instance_map, "WeakDependencies",
			    getResourceStringArrayAttribute(env, r_handle,
			    SCHA_RESOURCE_DEPENDENCIES_Q_WEAK, &scha_error));
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;

			PUT_ATTR(instance_map, "StrongDependencies",
			    getResourceStringArrayAttribute(env, r_handle,
			    SCHA_RESOURCE_DEPENDENCIES_Q, &scha_error));
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;

			PUT_ATTR(instance_map, "RestartDependencies",
			    getResourceStringArrayAttribute(env, r_handle,
			    SCHA_RESOURCE_DEPENDENCIES_Q_RESTART, &scha_error));
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;

			PUT_ATTR(instance_map, "OfflineDependencies",
			    getResourceStringArrayAttribute(env, r_handle,
			    SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART,
			    &scha_error));
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;

			{
				int k;

				jobject ext_list;
				jobject per_node_ext_list;
				jobject sys_list;

				char *type;
				/*
				 * First, get the extention properties *
				 * names
				 */
				scha_error = scha_resource_get(r_handle,
				    SCHA_ALL_EXTENSIONS,
				    &attr_value_stringarray);
				if (scha_error != SCHA_ERR_NOERR)
					goto attr_error;

				/*
				 * Get the resource's type
				 */
				scha_error = scha_resource_get(r_handle,
				    SCHA_TYPE, &type);
				if (scha_error != SCHA_ERR_NOERR)
					goto attr_error;

				ext_list =
				    newObjNullParam(env,
				    "java/util/LinkedList");

				if (ext_list == NULL)
					goto attr_error;

				per_node_ext_list =
				    newObjNullParam(env,
				    "java/util/LinkedList");

				if (per_node_ext_list == NULL)
					goto attr_error;

				/*
				 * Get the RT real name
				 */
				rgm_status = rgmcnfg_get_rtrealname(type,
						&rt_name, NULL);
				if (rgm_status.err_code != SCHA_ERR_NOERR)
					goto attr_error;

				/*
				 * Get the resource type informations
				 */
				scha_status = rgm_scrgadm_getrtconf(
						(char *)rt_name, &rt, NULL);
				if (scha_status.err_code != SCHA_ERR_NOERR)
					goto attr_error;

				/*
				 * Then, for each extention property, *
				 * retrieve * its value
				 */
				for (k = 0;
				    k < (int)attr_value_stringarray->
					array_cnt; k++) {
					scha_error = addExtProperty(env,
					    r_handle, ext_list,
					    per_node_ext_list,
					    node_list,
					    attr_value_stringarray->
					    str_array[k],
					    rt);
					if (scha_error != SCHA_ERR_NOERR)
						goto attr_error;
				}
				PUT_ATTR(instance_map, "ExtProperties",
				    ext_list);
				PUT_ATTR(instance_map, "PernodeExtProperties",
				    per_node_ext_list);

				/*
				 * Add all the defined system
				 * properties with their current
				 * value.
				 */
				sys_list = newObjNullParam(env,
				    "java/util/LinkedList");
				if (sys_list == NULL)
					goto attr_error;

				scha_error = addSystemProperty(env,
				    r_handle, sys_list, rt);
				if (scha_error != SCHA_ERR_NOERR)
					goto attr_error;
				PUT_ATTR(instance_map, "SystemProperties",
				    sys_list);
			}


			/*
			 * Put this map into the instance map
			 */
			PUT(cache_map,
			    resource_list->str_array[j],
			    instance_map);

		    attr_error:
			if (r_handle != NULL) {
				/*
				 * free resource handle
				 */
				(void) scha_resource_close(r_handle);
				r_handle = NULL;
			}
			if (rt != NULL) {
				rgm_free_rt(rt);
				rt = NULL;
			}
			if (scha_error == SCHA_ERR_SEQID) {
				/* exit loop to re-run fill_cache */
				break;
			}

		}
	    rg_error:
		if (rg_handle != NULL) {
			/*
			 * free memory pointed to by rg_names
			 */
			(void) scha_resourcegroup_close(rg_handle);
		}
		if (scha_error == SCHA_ERR_SEQID) {
			/* exit loop to re-run fill_cache */
			break;
		}
	}

    error:
	if (sc_handle != NULL) {
		(void) scha_cluster_close(sc_handle);
	}

	if (scha_error == SCHA_ERR_SEQID) {
		/*
		 * clean up cache_map, then restart
		 */
		jmethodID map_clear =
		    (*env)->GetMethodID(env,
		    map_class,
		    "clear",
		    "()V");
		if (map_clear != NULL) {
		    (*env)->CallObjectMethod(env, cache_map, map_clear);
		}
		goto start_fillcache;
	}

	return (cache_map);
} /*lint !e715 */
