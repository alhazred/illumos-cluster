/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the RGroup MBean
 */

#pragma ident	"@(#)resource_data.c	1.9	08/08/17 SMI"

#include "com/sun/cluster/agent/rgm/RGroup.h"

#include <jniutil.h>
#include <string.h>
#include <stdio.h>
#include <scha.h>
#include <stdlib.h>
#include <thread.h>
#include <scadmin/scrgadm.h>
#include <rgm/rgm_cnfg.h>
#include <sys/clconf_int.h>
#include <unistd.h>
#if SOL_VERSION >= __s10
#include <stdbool.h>
#include <zone.h>
#include <libzccfg/libzccfg.h>
#include <sys/clconf_int.h>
#endif /* SOL_VERSION >= __s10 */

#define	getBooleanVal(expr) (expr)? JNI_TRUE : JNI_FALSE

/*
 * Enable this to get printf debug
 */
#define	BUFFSIZE 160
#define	debug if (0)
#define	DEBUG_RESOURCE_INTERCEPTOR 0

#if DEBUG_RESOURCE_INTERCEPTOR
#include <assert.h>
#endif


static mutex_t init_lock;
static int initialized = 0;

static jclass resourceDataClass;
static jclass map_class;
static jclass state_class;
static jclass status_class;
static jclass array_list_class;

static jmethodID resourceDataClassConstructor;
static jmethodID map_put;
static jmethodID state_constructor;
static jmethodID status_constructor;
static jmethodID map_clear;
static jmethodID array_list_constructor;
static jmethodID array_list_add;


static jfieldID nameID;
static jfieldID overallStateID;
static jfieldID rgmStatesID;
static jfieldID faultMonitorStatusID;
static jfieldID typeID;
static jfieldID typeVersionID;
static jfieldID descriptionID;
static jfieldID resourceGroupNameID;
static jfieldID enabledID;
static jfieldID monitoredID;
static jfieldID pernodeEnabledStatusID;
static jfieldID pernodeMonitoredStatusID;
static jfieldID detachedID;
static jfieldID projectNameID;
static jfieldID weakDependenciesID;
static jfieldID strongDependenciesID;
static jfieldID restartDependenciesID;
static jfieldID offlineDependenciesID;
static jfieldID systemPropertiesID;
static jfieldID extPropertiesID;
static jfieldID pernodeExtPropertiesID;



extern jobject createRtProperty(JNIEnv * env, RtrParam_T * property);
extern jobject createRtPropertyFromRGM(JNIEnv *env, rgm_param_t *property);


static void init(JNIEnv *env) {

    jclass localClass;
    debug printf("\n BEGIN init \n");
	(void) mutex_lock(&init_lock);

    if (!initialized) {

	/* Utility Classes */
	localClass = (*env)->FindClass(env, "java/util/HashMap");
	map_class = (*env)->NewGlobalRef(env, localClass);
	if (map_class == NULL)
	    goto unlock;
	map_put =
		(*env)->GetMethodID(env,
		map_class,
		"put",
		"(Ljava/lang/Object;Ljava/lang/Object;)"
		"Ljava/lang/Object;");
	if (map_put == NULL)
	    goto unlock;

	map_clear = (*env)->GetMethodID(env,
		map_class,
		"clear",
		"()V");
	if (map_clear == NULL)
	    goto unlock;

	/* ArrayList */
	localClass = (*env)->FindClass(env, "java/util/ArrayList");
	array_list_class = (*env)->NewGlobalRef(env, localClass);
	if (array_list_class == NULL) {
	    debug printf("\n array list class is null \n");
	    goto unlock;
	}

	array_list_constructor = (*env)->GetMethodID(
		env, array_list_class,
		"<init>", "()V");
	if (array_list_constructor == NULL) {
	    debug printf("\n array list constructor is null \n");
	    goto unlock;
	}
	array_list_add = (*env)->GetMethodID(env, array_list_class,
		"add", "(Ljava/lang/Object;)Z");
	if (array_list_add == NULL) {
	    debug printf("\n array list add is null \n");
	    goto unlock;
	}

	/*
	 * Load ResourceRgmState class
	 */
	localClass = (*env)->FindClass(env,
		"com/sun/cluster/agent/rgm/ResourceRgmState");
	state_class = (*env)->NewGlobalRef(env, localClass);
	if (state_class == NULL)
	    goto unlock;

	/*
	 * Get constructor
	 */
	state_constructor =
		(*env)->GetMethodID(env, state_class,
		"<init>",
		"(Ljava/lang/String;"
		"Lcom/sun/cluster/agent/rgm/"
		"ResourceRgmStateEnum;)V");

	/*
	 * Load ResourceFaultMonitorStatus class
	 */
	localClass = (*env)->FindClass(env,
		"com/sun/cluster/agent/rgm/"
		"ResourceFaultMonitorStatus");
	status_class = (*env)->NewGlobalRef(env, localClass);
	if (status_class == NULL)
	    goto unlock;

	/*
	 * Get constructor
	 */
	status_constructor =
		(*env)->GetMethodID(env, status_class,
		"<init>",
		"(Ljava/lang/String;"
		"Lcom/sun/cluster/agent/rgm/ResourceFaultMonitorStatusEnum;"
		"Ljava/lang/String;)V");

	/* find ResourceData class */
	localClass = (*env)->FindClass(env,
		"com/sun/cluster/agent/rgm/ResourceData");
	resourceDataClass = (*env)->NewGlobalRef(env, localClass);

	if (resourceDataClass == NULL) {
	    debug printf("\n ResourceData class is null\n");
	    debug printf("\n END init \n");
	    goto unlock;
	}

	/* find ResourceData class constructor */
	resourceDataClassConstructor = (*env)->GetMethodID(env,
		resourceDataClass, "<init>", "()V");

	if (resourceDataClassConstructor == NULL) {
	    debug printf("\n ResourceData Constructor is null\n");
	    debug printf("\n END init \n");
	    goto unlock;
	}

	/* find ResourceData class fields */
	nameID = (*env)->GetFieldID(env, resourceDataClass, "name",
		"Ljava/lang/String;");
	if (nameID == NULL) {
	    debug printf("\n nameID is null \n");
	    goto unlock;
	}

	overallStateID = (*env)->GetFieldID(
		env, resourceDataClass, "overallState",
		"Lcom/sun/cluster/agent/rgm/ResourceRgmStateEnum;");
	if (overallStateID == NULL) {
	    debug printf("\n overallStateID is null \n");
	    goto unlock;
	}

	rgmStatesID = (*env)->GetFieldID(
		env, resourceDataClass, "rgmStates",
		"[Lcom/sun/cluster/agent/rgm/ResourceRgmState;");
	if (rgmStatesID == NULL) {
	    debug printf("\n rgmStatesID is null \n");
	    goto unlock;
	}

	faultMonitorStatusID = (*env)->GetFieldID(
		env, resourceDataClass, "faultMonitorStatus",
		"[Lcom/sun/cluster/agent/rgm/ResourceFaultMonitorStatus;");
	if (faultMonitorStatusID == NULL) {
	    debug printf("\n faultMonitorStatusID is null \n");
	    goto unlock;
	}

	typeID = (*env)->GetFieldID(env, resourceDataClass, "type",
		"Ljava/lang/String;");
	if (typeID == NULL) {
	    debug printf("\n typeID is null \n");
	    goto unlock;
	}

	typeVersionID = (*env)->GetFieldID(
		env, resourceDataClass, "typeVersion",
		"Ljava/lang/String;");
	if (typeVersionID == NULL) {
	    debug printf("\n typeVersionID is null \n");
	    goto unlock;
	}

	descriptionID = (*env)->GetFieldID(
		env, resourceDataClass, "description",
		"Ljava/lang/String;");
	if (descriptionID == NULL) {
	    debug printf("\n descriptionID is null \n");
	    goto unlock;
	}

	resourceGroupNameID = (*env)->GetFieldID(
		env, resourceDataClass, "resourceGroupName",
		"Ljava/lang/String;");
	if (resourceGroupNameID == NULL) {
	    debug printf("\n resourceGroupNameID is null \n");
	    goto unlock;
	}

	enabledID = (*env)->GetFieldID(env, resourceDataClass, "enabled",
		"I");
	if (enabledID == NULL) {
	    debug printf("\n enabledID is null \n");
	    goto unlock;
	}

	monitoredID = (*env)->GetFieldID(
		env, resourceDataClass, "monitored",
		"I");
	if (monitoredID == NULL) {
	    debug printf("\n monitoredID is null \n");
	    goto unlock;
	}

	pernodeEnabledStatusID = (*env)->GetFieldID(
		env, resourceDataClass, "pernodeEnabledStatus",
		"Ljava/util/HashMap;");
	if (pernodeEnabledStatusID == NULL) {
	    debug printf("\n pernodeEnabledStatusID is null \n");
	    goto unlock;
	}

	pernodeMonitoredStatusID = (*env)->GetFieldID(
		env, resourceDataClass, "pernodeMonitoredStatus",
		"Ljava/util/HashMap;");
	if (pernodeMonitoredStatusID == NULL) {
	    debug printf("\n pernodeMonitoredStatusID is null \n");
	    goto unlock;
	}

	detachedID = (*env)->GetFieldID(
		env, resourceDataClass, "detached",
		"Z");
	if (detachedID == NULL) {
	    debug printf("\n detachedID is null \n");
	    goto unlock;
	}

	projectNameID = (*env)->GetFieldID(
		env, resourceDataClass, "projectName",
		"Ljava/lang/String;");
	if (projectNameID == NULL) {
	    debug printf("\n projectNameID is null \n");
	    goto unlock;
	}

	weakDependenciesID = (*env)->GetFieldID(
		env, resourceDataClass, "weakDependencies",
		"[Ljava/lang/String;");
	if (weakDependenciesID == NULL) {
	    debug printf("\n weakDependenciesID is null \n");
	    goto unlock;
	}

	strongDependenciesID = (*env)->GetFieldID(
		env, resourceDataClass, "strongDependencies",
		"[Ljava/lang/String;");
	if (strongDependenciesID == NULL) {
	    debug printf("\n strongDependenciesID is null \n");
	    goto unlock;
	}

	restartDependenciesID = (*env)->GetFieldID(
		env, resourceDataClass, "restartDependencies",
		"[Ljava/lang/String;");
	if (restartDependenciesID == NULL) {
	    debug printf("\n restartDependenciesID is null \n");
	    goto unlock;
	}

	offlineDependenciesID = (*env)->GetFieldID(
		env, resourceDataClass, "offlineDependencies",
		"[Ljava/lang/String;");
	if (offlineDependenciesID == NULL) {
	    debug printf("\n offlineDependenciesID is null \n");
	    goto unlock;
	}

	systemPropertiesID = (*env)->GetFieldID(
		env, resourceDataClass, "systemProperties",
		"Ljava/util/ArrayList;");
	if (systemPropertiesID == NULL) {
	    debug printf("\n systemPropertiesID is null \n");
	    goto unlock;
	}

	extPropertiesID = (*env)->GetFieldID(
		env, resourceDataClass, "extProperties",
		"Ljava/util/ArrayList;");
	if (extPropertiesID == NULL) {
	    debug printf("\n extPropertiesID is null \n");
	    goto unlock;
	}

	pernodeExtPropertiesID = (*env)->GetFieldID(
		env, resourceDataClass, "pernodeExtProperties",
		"Ljava/util/ArrayList;");
	if (pernodeExtPropertiesID == NULL) {
	    debug printf("\n pernodeExtPropertiesID is null \n");
	    goto unlock;
	}

	initialized = 1;
	}

    unlock:
	(void) mutex_unlock(&init_lock);
    debug printf("\n END init \n");
}



/*
 * Resource helper function to retrieve a string type value and transform it
 * to a JNI jobject element.
 */
static jobject
getResourceStringAttribute(JNIEnv * env,
	scha_resource_t r_handle,
	const char *attr_tag,
	scha_err_t *err,
	char *cZCNameQ) {

    scha_err_t scha_error;
    char *attribute_value = NULL;
    jobject result = NULL;

	/*
	* Calling SCHA API to retrieve the attribute value
	*/
    if (cZCNameQ == NULL) {
	scha_error = scha_resource_get(
		r_handle, attr_tag, &attribute_value);
	} else {
	scha_error = scha_resource_get_zone(
		cZCNameQ, r_handle, attr_tag, &attribute_value);
	}
	*err = scha_error;

    if (scha_error != SCHA_ERR_NOERR) {
	attribute_value = "SCHA ERROR";
	}
    result = newObjStringParam(env, "java/lang/String", attribute_value);
    return (result);
}


/*
 * Resource helper function to retrieve a string array type value and transform
 * it to a JNI jobject element.
 */
static jobject
getResourceStringArrayAttribute(JNIEnv * env,
	scha_resource_t r_handle, const char *attr_tag, scha_err_t *err,
	char *cZCNameQ) {

    scha_err_t scha_error;
    scha_str_array_t *attribute_value = NULL;
    jobject result = NULL;

	/*
	* Calling SCHA API to retrieve the attribute value
	*/
    if (cZCNameQ == NULL) {
	scha_error = scha_resource_get(
		r_handle, attr_tag, &attribute_value);
	*err = scha_error;
	} else {
	scha_error = scha_resource_get_zone(
		cZCNameQ, r_handle, attr_tag, &attribute_value);
	*err = scha_error;
	}

    if (scha_error != SCHA_ERR_NOERR)
	return (NULL);
	/*
	* Construct array
	*/
    result = newStringArray(env, (int)attribute_value->array_cnt,
	    attribute_value->str_array);

    return (result);
}

/*
 * Resource helper to set the current value in a ResourceProperty object
 */
static void
setPropertyValue(JNIEnv * env, int same_value, const char *nodename,
	jobject property,
	const scha_prop_type_t value_type,
	const int int_value,
	const char *string_value,
	const boolean_t boolean_value,
	const char *enum_value,
	const scha_str_array_t *stringarray_value,
	jobject pernodeMap) {

    jobject value = NULL;
    jobject nodeName = NULL;
    jclass property_class = NULL;
    jmethodID set_value = NULL;
    jmethodID put = NULL;

    jmethodID set_pernode_value = NULL;
    jclass pernodeClass = NULL;

    switch (value_type) {
	case SCHA_PTYPE_STRING:
	    property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyString");
	    if (property_class == NULL)
		goto error;
	    value = newObjStringParam(env, "java/lang/String",
		    string_value);
	    if (value == NULL)
		goto error;
	    if (same_value)
		set_value = (*env)->GetMethodID(env,
			property_class, "setValue",
			"(Ljava/lang/String;)V");
	    break;

	case SCHA_PTYPE_INT:
	    property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyInteger");
	    if (property_class == NULL)
		goto error;
	    value = newObjInt32Param(env, "java/lang/Integer", int_value);
	    if (value == NULL)
		goto error;
	    if (same_value) {
		set_value = (*env)->GetMethodID(env, property_class,
			"setValue",
			"(Ljava/lang/Integer;)V");
	    }
	    break;

	case SCHA_PTYPE_BOOLEAN:
	    property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyBoolean");
	    if (property_class == NULL)
		goto error;
	    value = newObjStringParam(env, "java/lang/Boolean",
		    boolean_value == B_TRUE ? "true" : "false");
	    if (value == NULL)
		goto error;
	    if (same_value)
		set_value = (*env)->GetMethodID(env, property_class,
			"setValue",
			"(Ljava/lang/Boolean;)V");
	    break;

	case SCHA_PTYPE_ENUM:
	    property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/ResourcePropertyEnum");
	    if (property_class == NULL)
		goto error;
	    value = newObjStringParam(env, "java/lang/String", enum_value);
	    if (value == NULL)
		goto error;
	    if (same_value)
		set_value = (*env)->GetMethodID(env,
			property_class, "setValue",
			"(Ljava/lang/String;)V");
	    break;

	case SCHA_PTYPE_STRINGARRAY:
	    property_class = (*env)->FindClass(env,
		    "com/sun/cluster/agent/rgm/property/"
		    "ResourcePropertyStringArray");
	    if (property_class == NULL)
		goto error;
	    set_value = (*env)->GetMethodID(env,
		    property_class, "setValue",
		    "([Ljava/lang/String;)V");
	    value = newStringArray(env, (int)stringarray_value->array_cnt,
		    stringarray_value->str_array);
	    if (value == NULL) {
		goto error;
	    }
	    break;
	default:
	    goto error;
	}
    if (same_value) {
	(*env)->CallVoidMethod(env, property, set_value, value);
	} else {
	pernodeClass =  (*env)->FindClass(env, "java/util/HashMap");
	if (pernodeClass == NULL)
	    goto error;
	put =
		(*env)->GetMethodID(env, pernodeClass,
		"put",
		"(Ljava/lang/Object;Ljava/lang/Object;)"
		"Ljava/lang/Object;");

	nodeName = newObjStringParam(env, "java/lang/String", nodename);
	(void) (*env)->CallObjectMethod(env, pernodeMap, put,
		nodeName, value);

	set_pernode_value = (*env)->GetMethodID(env,
		property_class, "setPernodeValue",
		"(Ljava/util/HashMap;)V");
	if (set_pernode_value == NULL)
	    goto error;
	(*env)->CallVoidMethod(env, property,
		set_pernode_value, pernodeMap);
	}

    error:
	    delRef(env, property_class);
	    delRef(env, value);
}

/*
 * Resource helper function to check whether the given value for per-node
 * extension property is same on all the nodes.
 */

static int
is_same_per_node_ext_prop_value(scha_resource_t r_handle,
	const char *prop_name,
	scha_prop_type_t propType, const scha_str_array_t *node_list,
	char *cZCNameQ) {
    uint_t i = 0;
    scha_extprop_value_t *prop_value = NULL;
    char *tmp_prop_value_str = NULL;
    int tmp_prop_value_int = 0;
    boolean_t tmp_prop_value_bool = B_FALSE;
    char *tmp_prop_value_enum = NULL;

    scha_err_t scha_error;
    switch (propType) {
	case SCHA_PTYPE_STRING :

	    for (i = 0; i < (node_list->array_cnt); i++) {

		if (prop_value != NULL) {
		    free(tmp_prop_value_str);
		    tmp_prop_value_str =
			    strdup(prop_value->val.val_str);
		}
		if (cZCNameQ == NULL) {
		    scha_error = scha_resource_get(r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
		} else {
		    scha_error = scha_resource_get_zone(
			    cZCNameQ,
			    r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
		}
		if (scha_error != SCHA_ERR_NOERR)
		    goto error;

		if ((tmp_prop_value_str) &&
			(strcmp(tmp_prop_value_str,
			prop_value->val.val_str) != 0)) {
		    free(tmp_prop_value_str);
		    return (0);
		}
	    }
	    if (tmp_prop_value_str)
		free(tmp_prop_value_str);
	    break;
	case SCHA_PTYPE_INT:

	    for (i = 0; i < (node_list->array_cnt); i++) {
		if (i > 0)
		    tmp_prop_value_int = prop_value->val.val_int;

		if (cZCNameQ == NULL) {
		    scha_error = scha_resource_get(r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
		} else {
		    scha_error = scha_resource_get_zone(
			    cZCNameQ,
			    r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
		}
		if (scha_error != SCHA_ERR_NOERR)
		    goto error;

		if ((i) && (tmp_prop_value_int !=
			prop_value->val.val_int))
		    return (0);
	    }
	    break;
	case SCHA_PTYPE_BOOLEAN:

	    for (i = 0; i < ((int)node_list->array_cnt); i++) {
		if (i > 0)
		    tmp_prop_value_bool =
			    prop_value->val.val_boolean;

		if (cZCNameQ == NULL) {
		    scha_error = scha_resource_get(r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
		} else {
		    scha_error = scha_resource_get_zone(
			    cZCNameQ,
			    r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
		}
		if (scha_error != SCHA_ERR_NOERR)
		    goto error;
		if ((i) && (tmp_prop_value_bool !=
			prop_value->val.val_boolean))
		    return (0);
	    }
	    break;
	case SCHA_PTYPE_ENUM:

	    for (i = 0; i < ((int)node_list->array_cnt); i++) {
		if (prop_value != NULL) {
		    free(tmp_prop_value_enum);
		    tmp_prop_value_enum =
			    strdup(prop_value->val.val_enum);
		}

		if (cZCNameQ == NULL) {
		    scha_error = scha_resource_get(r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
		} else {
		    scha_error = scha_resource_get_zone(
			    cZCNameQ,
			    r_handle,
			    SCHA_EXTENSION_NODE, prop_name,
			    node_list->str_array[i], &prop_value);
		}
		if (scha_error != SCHA_ERR_NOERR)
		    goto error;
		if (tmp_prop_value_enum &&
			strcmp(tmp_prop_value_enum,
			prop_value->val.val_enum) != 0) {
		    free(tmp_prop_value_enum);
		    return (0);
		}
	    }
	    if (tmp_prop_value_enum)
		free(tmp_prop_value_enum);
	    break;
	default:
	    break;
	}
    return (1);
    error:
	    return (-1);
}

/*
 * Resource helper function to retrieve an extention property, transform it
 * into the appropriate ResourceProperty object and add it to the given List.
 */
static scha_err_t
addExtProperty(JNIEnv * env, scha_resource_t r_handle, jobject list,
	jobject per_node_list, const scha_str_array_t *node_list,
	const char *prop_name, rgm_rt_t *rt, char *cZCNameQ) {

    scha_err_t scha_error;
    scha_errmsg_t scha_status;

    scha_extprop_value_t *prop_value = NULL;
    jobject property = NULL;
    jclass array_list_class = NULL;
    jclass per_node_class = NULL;
    jmethodID add;
    jmethodID pernodePropadd;
    int i = 0;
    int property_found = 0;

	/*
	* For per-node extension properties.
	*/
    boolean_t pernode;
    scha_prop_type_t prop_type;
    jobject pernodePropName = NULL;
    jobject pernodeValueMap = NULL;
    int same_value = 0;
    rgm_param_t **paramtable = NULL;
    rgm_param_t *param = NULL;

	/* For per-node extension properties. */
    jobject pernodeMap;

	/*
	* Get the extension property value
	*/
    if (cZCNameQ == NULL) {
	scha_error = scha_resource_get(r_handle, SCHA_EXTENSION, prop_name,
		&prop_value);
	} else {
	scha_error = scha_resource_get_zone(
		cZCNameQ, r_handle, SCHA_EXTENSION, prop_name,
		&prop_value);
	}
    if (scha_error != SCHA_ERR_NOERR)
	goto error;

	/*
	* Get the property information in this resource type
	*/
    paramtable = rt->rt_paramtable;
    i = 0;
    if (paramtable != NULL)
	param = paramtable[i];
    while ((param != NULL) && (!property_found)) {
	char *buf = param->p_name;

	if (buf == NULL) {
	    param = paramtable[++i];
	    continue;
	}
	if (strcmp(prop_name, buf) == 0) {
	    property = createRtPropertyFromRGM(env,
		    param);
	    pernode = param->p_per_node;
	    prop_type = param->p_type;
	    property_found = 1;
	}
	param = paramtable[++i];
	}
    if (property == NULL)
	goto error;
	/*
	* Set the property value
	*/
    if (pernode == B_TRUE) {
	same_value = is_same_per_node_ext_prop_value(r_handle,
		prop_name, prop_type, node_list, cZCNameQ);
	if (same_value) {

	    if (cZCNameQ == NULL) {
		scha_error = scha_resource_get(
			r_handle, SCHA_EXTENSION,
			prop_name, &prop_value);
	    } else {
		scha_error = scha_resource_get_zone(
			cZCNameQ, r_handle, SCHA_EXTENSION,
			prop_name, &prop_value);
	    }
	    setPropertyValue(env, 1, NULL, property,
		    prop_type,
		    prop_value->val.val_int,
		    prop_value->val.val_str,
		    prop_value->val.val_boolean,
		    prop_value->val.val_enum,
		    prop_value->val.val_strarray, NULL);
	} else {
	    pernodeMap = newObjNullParam(env, "java/util/HashMap");
	    if (pernodeMap == NULL)
		goto error;

	    for (i = 0; i < ((int)node_list->array_cnt); i++)  {

		if (cZCNameQ == NULL) {
		    scha_error = scha_resource_get(r_handle,
			    SCHA_EXTENSION_NODE,
			    prop_name,
			    node_list->str_array[i],
			    &prop_value);
		} else {
		    scha_error = scha_resource_get_zone(
			    cZCNameQ,
			    r_handle,
			    SCHA_EXTENSION_NODE,
			    prop_name,
			    node_list->str_array[i],
			    &prop_value);
		}
		if (scha_error != SCHA_ERR_NOERR) {
		    goto error;
		}
		setPropertyValue(env, 0,
			node_list->str_array[i],
			property,
			prop_type,
			prop_value->val.val_int,
			prop_value->val.val_str,
			prop_value->val.val_boolean,
			prop_value->val.val_enum,
			prop_value->val.val_strarray,
			pernodeMap);

		prop_value = NULL;

	    }
	}

	per_node_class = (*env)->FindClass(env,
		"java/util/ArrayList");
	if (per_node_class == NULL) {
	    goto error;
	}
	pernodePropName = newObjStringParam(env,
		"java/lang/String", prop_name);
	if (pernodePropName == NULL) {
	    goto error;
	}

	pernodePropadd = (*env)->GetMethodID(env,
		per_node_class, "add", "(Ljava/lang/Object;)Z");
	(void) (*env)->CallObjectMethod(env, per_node_list,
		pernodePropadd, pernodePropName);
	} else {

	if (cZCNameQ == NULL) {
	    scha_error = scha_resource_get(r_handle, SCHA_EXTENSION,
		    prop_name, &prop_value);
	} else {
	    scha_error = scha_resource_get_zone(
		    cZCNameQ, r_handle, SCHA_EXTENSION,
		    prop_name, &prop_value);
	}
	if (scha_error != SCHA_ERR_NOERR)
	    goto error;
	setPropertyValue(env, 1, NULL,
		property, prop_type,
		prop_value->val.val_int,
		prop_value->val.val_str,
		prop_value->val.val_boolean,
		prop_value->val.val_enum,
		prop_value->val.val_strarray, NULL);
	}

	/*
	* Add it to the list and return
	*/
    array_list_class = (*env)->FindClass(env,
	    "java/util/ArrayList");

    if (array_list_class == NULL)
	goto error;
    add = (*env)->GetMethodID(env, array_list_class, "add",
	    "(Ljava/lang/Object;)Z");
	(void) (*env)->CallObjectMethod(env, list, add, property);
    error:
	    delRef(env, property);
	    delRef(env, array_list_class);
	    return (scha_error);
}

/*
 * Resource helper function to retrieve a system property, transform it
 * into the appropriate ResourceProperty object and add it to the given List.
 */
static scha_err_t
addSystemProperty(JNIEnv * env, scha_resource_t r_handle, jobject list,
	rgm_rt_t *rt, char *cZCNameQ) {

    scha_err_t scha_error = SCHA_ERR_INTERNAL;
    scha_errmsg_t scha_status;
    char *tag = NULL;

	/*
	* different type of property value
	*/
    scha_prop_type_t value_type;
    int int_value = 0;
    char *string_value = NULL;
    boolean_t boolean_value = B_FALSE;
    scha_str_array_t *stringarray_value = NULL;

    jobject property = NULL;
    jclass array_list_class;
    jmethodID add;

    int i;
    rgm_param_t **paramtable = NULL;
    rgm_param_t *param = NULL;

    array_list_class = (*env)->FindClass(env, "java/util/ArrayList");
    if (array_list_class == NULL)
	goto error;
    add = (*env)->GetMethodID(env, array_list_class, "add",
	    "(Ljava/lang/Object;)Z");

	/*
	* Browse all the system properties in the resource type, create the
	* appropriate ResourceType object, retrieve and affect the current
	* value.
	*/
    paramtable = rt->rt_paramtable;
    i = 0;
    if (paramtable != NULL) {
	param = paramtable[i];
	}
    while (param != NULL) {
	if (param->p_extension == B_FALSE) {
	    property = createRtPropertyFromRGM(env, param);
	} else {
	    param = paramtable[++i];
	    continue;
	}
	value_type = param->p_type;
	tag = param->p_name;

	/*
	 * Get the current value from the SCHA API
	 */
	switch (value_type) {
	    case SCHA_PTYPE_ENUM:
		if (cZCNameQ == NULL) {
		    scha_error = scha_resource_get(r_handle, tag,
			    &int_value);
		} else {
		    scha_error = scha_resource_get_zone(
			    cZCNameQ, r_handle, tag,
			    &int_value);
		}
		if (scha_error != SCHA_ERR_NOERR)
		    goto error;
		switch (int_value) {
		    case SCHA_FOMODE_NONE:
			string_value = strdup("NONE");
			break;
		    case SCHA_FOMODE_HARD:
			string_value = strdup("HARD");
			break;
		    case SCHA_FOMODE_SOFT:
			string_value = strdup("SOFT");
			break;
		    default:
			string_value = NULL;
			break;
		}
		if (string_value == NULL)
		    goto error;
		break;

	    case SCHA_PTYPE_STRING:
		if (cZCNameQ == NULL) {
		    scha_error = scha_resource_get(r_handle, tag,
			    &string_value);
		} else {
		    scha_error = scha_resource_get_zone(
			    cZCNameQ, r_handle, tag,
			    &string_value);
		}
		break;

	    case SCHA_PTYPE_INT:
	    case SCHA_PTYPE_UINT:
		if (cZCNameQ == NULL) {
		    scha_error = scha_resource_get(r_handle, tag,
			    &int_value);
		} else {
		    scha_error = scha_resource_get_zone(
			    cZCNameQ, r_handle, tag,
			    &int_value);
		}
		break;

	    case SCHA_PTYPE_BOOLEAN:
		if (cZCNameQ == NULL) {
		    scha_error = scha_resource_get(r_handle, tag,
			    &boolean_value);
		} else {
		    scha_error = scha_resource_get_zone(
			    cZCNameQ, r_handle, tag,
			    &boolean_value);
		}
		break;

	    case SCHA_PTYPE_STRINGARRAY:
		if (cZCNameQ == NULL) {
		    scha_error = scha_resource_get(r_handle, tag,
			    &stringarray_value);
		} else {
		    scha_error = scha_resource_get_zone(
			    cZCNameQ, r_handle, tag,
			    &stringarray_value);
		}
		break;

	    case SCHA_PTYPE_UINTARRAY:
	    default:
		goto error;
	}
	if (scha_error != SCHA_ERR_NOERR)
	    goto error;

	/*
	 * Set the property value
	 */
	setPropertyValue(env, 1, NULL, property, value_type,
		int_value,
		string_value,
		boolean_value,
		string_value,
		stringarray_value, NULL);

	/*
	 * Add it to the list
	 */
	(void) (*env)->CallObjectMethod(env, list, add, property);
	param = paramtable[++i];
	}

    error:
		delRef(env, property);
		delRef(env, array_list_class);
		return (scha_error);
}

/*
 * given a resource state as input, see if the sub_state affects it
 */
scha_rsstate_t
getoverall_state(scha_rsstate_t state, scha_rsstate_t sub_state) {

	/*
	* Apply the rule on overall state : OFFLINE < STOPPING <
	* STARTING < DETACHED < ONLINE < * ONLINE_NOT_MONITORED <
	* MONITOR_FAILED < * START_FAILED < STOP_FAILED
	*/
    if (state == SCHA_RSSTATE_OFFLINE) {
	state = sub_state;
	} else if (state ==
	    SCHA_RSSTATE_ONLINE) {
	state =
		(sub_state >= SCHA_RSSTATE_START_FAILED &&
		sub_state <= SCHA_RSSTATE_ONLINE_NOT_MONITORED) ?
		    sub_state : state;
	} else if (state == SCHA_RSSTATE_START_FAILED) {
	state = sub_state == SCHA_RSSTATE_STOP_FAILED ?
	    sub_state : state;
	} else if (state == SCHA_RSSTATE_MONITOR_FAILED) {
	state = (sub_state == SCHA_RSSTATE_START_FAILED ||
		sub_state == SCHA_RSSTATE_STOP_FAILED) ?
		    sub_state : state;
	} else if (state == SCHA_RSSTATE_ONLINE_NOT_MONITORED) {
	state = (sub_state == SCHA_RSSTATE_START_FAILED ||
		sub_state == SCHA_RSSTATE_STOP_FAILED ||
		sub_state == SCHA_RSSTATE_MONITOR_FAILED) ?
		    sub_state : state;
	} else if (state == SCHA_RSSTATE_STARTING) {
	state = (sub_state != SCHA_RSSTATE_OFFLINE &&
		sub_state != SCHA_RSSTATE_STOPPING) ?
		    sub_state : state;
	} else if (state == SCHA_RSSTATE_STOPPING) {
	state = sub_state != SCHA_RSSTATE_OFFLINE ?
	    sub_state : state;
	} else if (state == SCHA_RSSTATE_DETACHED) {
	state = (sub_state != SCHA_RSSTATE_OFFLINE &&
		sub_state != SCHA_RSSTATE_STARTING &&
		sub_state != SCHA_RSSTATE_STOPPING) ?
		    sub_state : state;
	}
    return (state);
}

/*
 * A couple of helper macros for things we do a lot,
 * * they contain references to local variables in the
 * * function below
 */
#define	PUT(MAP, NAME, VALUE)						\
	(*env)->CallObjectMethod(env,					\
				    MAP,				\
				    map_put,				\
				    (*env)->NewStringUTF(env, NAME),	\
				    VALUE)

/*
 * Class:     com_sun_cluster_agent_rgm_RGroup
 * Method:    getResources
 * Signature: ()Ljava/util/HashMap;
 */
JNIEXPORT jobject JNICALL
getResources(JNIEnv *env, jobject this, char *cZCNameQ, char *cRGNameQ) {

	/*
	* return HashMap containing RG-R Data
	*/

    jobject rg_r_data_map = NULL;
    jobject r_data_map = NULL;
    jobject rData;
    int i, j;

	/*
	* SCHA error tag
	*/
    scha_err_t scha_error;

	/*
	* SCHA cluster handle
	*/
    scha_cluster_t sc_handle = NULL;

	/*
	* Resource group handle.
	*/
    scha_resourcegroup_t rg_handle = NULL;
    scha_str_array_t *rg_names = NULL;
    scha_errmsg_t scha_status;

	/*
	* Resource handle.
	*/
    scha_resource_t r_handle = NULL;
    scha_str_array_t *resource_list = NULL;

    scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};

	/*
	* Used as tmp variable storing return values
	*/
    scha_str_array_t *attr_value_stringarray = NULL;
    int attr_value_int = 0;
    char *rt_name = NULL;

    scha_str_array_t *node_list = NULL;

    debug printf(" START readData method \n");
    if (!initialized) {
	init(env);
	}

	/*
	* Create the rg_r_data_map we are going to return
	*/
    rg_r_data_map = newObjNullParam(env, "java/util/HashMap");
    if (rg_r_data_map == NULL)
	goto error;
	/*
	* get a handle to the Cluster
	*/
    start_fillcache:

			/*
			* Library initialization
			*/
		    clconf_lib_init();

    scha_error = scha_cluster_open(&sc_handle);
    if (scha_error != SCHA_ERR_NOERR) {
	sc_handle = NULL;
	goto error;
	}
	/*
	* get rg names if not already given
	*/
    if (cRGNameQ != NULL) {
	/*
	 * Need to query for only resources belonging to given rg name
	 * Hence just the fill the structure with the given rg name
	 */
	rg_names = (scha_str_array_t *) malloc(sizeof (scha_str_array_t) * 1);
	rg_names->str_array = (char **) malloc(sizeof (char *) * 1);
	rg_names->array_cnt = 1;
	rg_names->str_array[0] = cRGNameQ;

	} else {
	if (cZCNameQ == NULL) {
	    scha_error = scha_cluster_get(
		    sc_handle, SCHA_ALL_RESOURCEGROUPS, &rg_names);
	} else {
	    scha_error = scha_cluster_get_zone(
		    cZCNameQ, sc_handle, SCHA_ALL_RESOURCEGROUPS, &rg_names);
	}
	if ((scha_error != SCHA_ERR_NOERR) || (rg_names == NULL))
	    goto error;
	}


	/*
	* For each resource group find its resources
	*/
    for (i = 0; i < (int)rg_names->array_cnt; i++) {
	rgm_rt_t *rt =  NULL;
	/*
	 * Get the resource group handle
	 */
	rg_handle = NULL;

	if (cZCNameQ == NULL) {
	    scha_error = scha_resourcegroup_open(
		    rg_names->str_array[i], &rg_handle);
	} else {
	    scha_error = scha_resourcegroup_open_zone(
		    cZCNameQ, rg_names->str_array[i], &rg_handle);
	}
	if (scha_error != SCHA_ERR_NOERR) {
	    rg_handle = NULL;
	    goto rg_error;
	}
	/*
	 * Getting the list of resources
	 */
	if (cZCNameQ == NULL) {
	    scha_error = scha_resourcegroup_get(rg_handle,
		    SCHA_RESOURCE_LIST, &resource_list);
	} else {
	    scha_error = scha_resourcegroup_get_zone(
		    cZCNameQ, rg_handle, SCHA_RESOURCE_LIST, &resource_list);
	}
	if (scha_error != SCHA_ERR_NOERR)
	    goto rg_error;

	/* Initialize the orb */
	if (rgm_orbinit().err_code != SCHA_ERR_NOERR)
	    goto rg_error;

	/*
	 * Create the r_data_map for ResourceData Objects
	 */
	r_data_map = newObjNullParam(env,
		"java/util/HashMap");
	if (r_data_map == NULL)
	    goto error;

	/*
	 * Iterate over these resources
	 */

	for (j = 0; j < (int)resource_list->array_cnt; j++) {
		/*
		* we are going to create a r_data_map for this instance
		* and populate it with its attributes
		*/

	    /* Create a Resource object */
	    rData = (*env)->NewObject(env, resourceDataClass,
		    resourceDataClassConstructor);
	    if (rData == NULL) {
		debug printf("\n resourceData Object is NULL\n");
		goto error;
	    }
	    r_handle = NULL;
	    if (cZCNameQ == NULL) {
		scha_error =
			scha_resource_open(resource_list->str_array[j],
			NULL, &r_handle);
	    } else {
		scha_error =
			scha_resource_open_zone(cZCNameQ,
			resource_list->str_array[j],
			NULL, &r_handle);
	    }

	    if (scha_error != SCHA_ERR_NOERR) {
		r_handle = NULL;
		goto attr_error;
	    }

	    setStringField(env, rData, nameID,
		    (char *)resource_list->str_array[j]);

	    {
		int k;

		scha_rsstate_t state = SCHA_RSSTATE_OFFLINE;
		scha_status_value_t *status;
		jobject node_state;
		jobject node_status;

		/*
		 * Get the list of nodes on this * *
		 * resource group
		 */
		if (cZCNameQ == NULL) {
		    scha_error = scha_resourcegroup_get(rg_handle,
			    SCHA_NODELIST,
			    &attr_value_stringarray);
		} else {
		    scha_error = scha_resourcegroup_get_zone(
			    cZCNameQ,
			    rg_handle,
			    SCHA_NODELIST,
			    &attr_value_stringarray);
		}
		if (scha_error != SCHA_ERR_NOERR)
		    goto attr_error;
		node_list = attr_value_stringarray;

		/*
		 * Construct array for node states
		 */
		node_state = (*env)->NewObjectArray(env,
			(int)attr_value_stringarray->array_cnt,
			state_class, NULL);
		if (node_state == NULL)
		    goto attr_error;
		/*
		 * Construct array for node statuses
		 */
		node_status = (*env)->NewObjectArray(env,
			(int)attr_value_stringarray->array_cnt,
			status_class, NULL);
		if (node_status == NULL)
		    goto attr_error;

		/*
		 * For each node
		 */
		for (k = 0;
		k <
			(int)attr_value_stringarray->array_cnt;
		k++) {
			/*
			* Get state
			*/
		    if (cZCNameQ == NULL) {
			scha_error =
				scha_resource_get(r_handle,
				SCHA_RESOURCE_STATE_NODE,
				attr_value_stringarray->
				str_array[k],
				&attr_value_int);
		    } else {
			scha_error =
				scha_resource_get_zone(
				cZCNameQ,
				r_handle,
				SCHA_RESOURCE_STATE_NODE,
				attr_value_stringarray->
				str_array[k],
				&attr_value_int);
		    }

		    if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

			/*
			* Get overall state
			*/
		    state = getoverall_state(state,
			    attr_value_int);

			/*
			* add state to state array
			*/
		    (*env)->SetObjectArrayElement(env,
			    node_state, k,
			    (*env)->NewObject(env, state_class,
			    state_constructor,
			    (*env)->NewStringUTF(env,
			    attr_value_stringarray->
			    str_array[k]),
			    getCMASSEnum(env,
			    "com/sun/cluster/agent/"
			    "rgm/ResourceRgmStateEnum",
			    attr_value_int)));

			/*
			* Get status
			*/
		    if (cZCNameQ == NULL) {
			scha_error =
				scha_resource_get(r_handle,
				SCHA_STATUS_NODE,
				attr_value_stringarray->
				str_array[k],
				&status);
		    } else {
			scha_error =
				scha_resource_get_zone(
				cZCNameQ,
				r_handle,
				SCHA_STATUS_NODE,
				attr_value_stringarray->
				str_array[k],
				&status);
		    }

		    if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;
			/*
			* add status to status array
			*/
		    (*env)->SetObjectArrayElement(env,
			    node_status, k,
			    (*env)->NewObject(env,
			    status_class,
			    status_constructor,
			    (*env)->NewStringUTF(env,
			    attr_value_stringarray->
			    str_array[k]),
			    getCMASSEnum(env,
			    "com/sun/cluster/agent/"
			    "rgm/ResourceFault"
			    "MonitorStatusEnum",
			    status->status),
			    (*env)->NewStringUTF(env,
			    status->status_msg)));
		}

		(*env)->SetObjectField(env, rData,
			overallStateID,
			getCMASSEnum(env,
			"com/sun/cluster/agent/rgm/"
			"ResourceRgmStateEnum",
			state));
		(*env)->SetObjectField(env, rData,
			rgmStatesID,
			node_state);
		(*env)->SetObjectField(env, rData,
			faultMonitorStatusID,
			node_status);
	    }

		    (*env)->SetObjectField(env, rData, typeID,
			    getResourceStringAttribute(env, r_handle,
			    SCHA_TYPE, &scha_error, cZCNameQ));
		    if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		    (*env)->SetObjectField(env, rData, typeVersionID,
			    getResourceStringAttribute(env, r_handle,
			    SCHA_TYPE_VERSION, &scha_error, cZCNameQ));
		    if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		    (*env)->SetObjectField(env, rData, descriptionID,
			    getResourceStringAttribute(env, r_handle,
			    SCHA_R_DESCRIPTION, &scha_error, cZCNameQ));
		    if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		    (*env)->SetObjectField(env, rData, resourceGroupNameID,
			    getResourceStringAttribute(env, r_handle,
			    SCHA_GROUP, &scha_error, cZCNameQ));
		    if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		    {
			/*
			 * Get the resource enabled status on each node.
			 */
			int k = 0;
			int enabled_state = 0;
			scha_switch_t enabled_swtch =
				SCHA_SWITCH_DISABLED;
			uint_t enabled_count = 0;
			jclass enabledStateClass = NULL;
			jobject enabledStateMap = NULL;
			jobject  nodeName = NULL;
			jmethodID put;

			enabledStateMap = newObjNullParam(env,
				"java/util/HashMap");

			if (enabledStateMap == NULL)
			    goto attr_error;

			enabledStateClass =  (*env)->FindClass(env,
				"java/util/HashMap");
			if (enabledStateClass == NULL)
			    goto attr_error;

			put =
				(*env)->GetMethodID(env, enabledStateClass,
				"put",
				"(Ljava/lang/Object;Ljava/lang/Object;)"
				"Ljava/lang/Object;");

			for (k = 0; k < (int)node_list->array_cnt;
			k++) {
			    if (cZCNameQ == NULL) {
				scha_error =
					scha_resource_get(r_handle,
					SCHA_ON_OFF_SWITCH_NODE,
					node_list->str_array[k],
					&enabled_swtch);
			    } else {
				scha_error =
					scha_resource_get_zone(
					cZCNameQ,
					r_handle,
					SCHA_ON_OFF_SWITCH_NODE,
					node_list->str_array[k],
					&enabled_swtch);
			    }

			    if (scha_error != SCHA_ERR_NOERR) {
				goto attr_error;
			    }

			    if (enabled_swtch !=
				    SCHA_SWITCH_DISABLED)
				enabled_count++;

			    nodeName = newObjStringParam(env,
				    "java/lang/String",
				    node_list->str_array[k]);
			    (*env)->CallObjectMethod(env,
				    enabledStateMap,
				    put, nodeName, newBoolean(
				    env,
				    (enabled_swtch != SCHA_SWITCH_DISABLED)));

			}
			if (enabled_count == 0)
			    enabled_state = 0;
			else if (enabled_count == node_list->array_cnt)
			    enabled_state = 1;
			else
			    enabled_state = 2;

			(*env)->SetIntField(env, rData, enabledID,
				enabled_state);

			(*env)->SetObjectField(env, rData,
				pernodeEnabledStatusID,
				enabledStateMap);
		    } {
				/*
				* Get the resource monitored status
				* on each node.
				*/
			    int k = 0;
			    scha_switch_t monitored_swtch =
				    SCHA_SWITCH_DISABLED;
			    uint_t monitored_count = 0;
			    int monitored_state = 0;
			    jclass monitoredStateClass = NULL;
			    jobject monitoredStateMap = NULL;
			    jobject  nodeName = NULL;
			    jmethodID put;

			    monitoredStateMap = newObjNullParam(env,
				    "java/util/HashMap");
			    if (monitoredStateMap == NULL)
				goto error;

			    monitoredStateClass =  (*env)->FindClass(env,
				    "java/util/HashMap");
			    if (monitoredStateClass == NULL)
				goto error;

			    put =
				    (*env)->GetMethodID(env,
				    monitoredStateClass,
				    "put",
				    "(Ljava/lang/Object;Ljava/lang/Object;)"
				    "Ljava/lang/Object;");

			    for (k = 0; k < (int)node_list->array_cnt;
			    k++) {
				if (cZCNameQ == NULL) {
				    scha_error =
					    scha_resource_get(r_handle,
					    SCHA_MONITORED_SWITCH_NODE,
					    node_list->str_array[k],
					    &monitored_swtch);
				} else {
				    scha_error =
					    scha_resource_get_zone(
					    cZCNameQ,
					    r_handle,
					    SCHA_MONITORED_SWITCH_NODE,
					    node_list->str_array[k],
					    &monitored_swtch);
				}

				if (scha_error != SCHA_ERR_NOERR) {
				    goto attr_error;
				}

				if (monitored_swtch !=
					SCHA_SWITCH_DISABLED)
				    monitored_count++;

				nodeName = newObjStringParam(env,
					"java/lang/String",
					node_list->str_array[k]);
				(*env)->CallObjectMethod(env,
					monitoredStateMap,
					put, nodeName, newBoolean(env,
					(monitored_swtch !=
						    SCHA_SWITCH_DISABLED)));
			    }
			    if (monitored_count == 0)
				monitored_state = 0;
			    else if (
				    monitored_count == node_list->array_cnt)
				monitored_state = 1;
			    else
				monitored_state = 2;

			    (*env)->SetIntField(env, rData, monitoredID,
				    monitored_state);

			    (*env)->SetObjectField(env, rData,
				    pernodeMonitoredStatusID,
				    monitoredStateMap);
		    } {
			if (cZCNameQ == NULL) {
			    scha_error =
				    scha_resource_get(r_handle,
				    SCHA_RESOURCE_STATE,
				    &attr_value_int);
			} else {
			    scha_error =
				    scha_resource_get_zone(
				    cZCNameQ,
				    r_handle,
				    SCHA_RESOURCE_STATE,
				    &attr_value_int);
			}
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;
			(*env)->SetBooleanField(env, rData, detachedID,
				getBooleanVal(attr_value_int !=
				SCHA_RSSTATE_DETACHED));
		    }

		    (*env)->SetObjectField(env, rData, projectNameID,
			    getResourceStringAttribute(env, r_handle,
			    SCHA_RESOURCE_PROJECT_NAME, &scha_error, cZCNameQ));
		    if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		    (*env)->SetObjectField(env, rData, weakDependenciesID,
			    getResourceStringArrayAttribute(env, r_handle,
			    SCHA_RESOURCE_DEPENDENCIES_Q_WEAK, &scha_error,
			    cZCNameQ));
		    if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		    (*env)->SetObjectField(env, rData, strongDependenciesID,
			    getResourceStringArrayAttribute(env, r_handle,
			    SCHA_RESOURCE_DEPENDENCIES_Q, &scha_error,
			    cZCNameQ));
		    if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		    (*env)->SetObjectField(env, rData,
			    restartDependenciesID,
			    getResourceStringArrayAttribute(env, r_handle,
			    SCHA_RESOURCE_DEPENDENCIES_Q_RESTART, &scha_error,
			    cZCNameQ));
		    if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		    (*env)->SetObjectField(env, rData,
			    offlineDependenciesID,
			    getResourceStringArrayAttribute(env, r_handle,
			    SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART,
			    &scha_error, cZCNameQ));
		    if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		    {
			int k;

			jobject ext_list;
			jobject per_node_ext_list;
			jobject sys_list;

			char *type;
			/*
			 * First, get the extention properties *
			 * names
			 */
			if (cZCNameQ == NULL) {
			    scha_error = scha_resource_get(r_handle,
				    SCHA_ALL_EXTENSIONS,
				    &attr_value_stringarray);
			} else {
			    scha_error = scha_resource_get_zone(
				    cZCNameQ,
				    r_handle,
				    SCHA_ALL_EXTENSIONS,
				    &attr_value_stringarray);
			}
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;

			/*
			 * Get the resource's type
			 */
			if (cZCNameQ == NULL) {
			    scha_error = scha_resource_get(r_handle,
				    SCHA_TYPE, &type);
			} else {
			    scha_error = scha_resource_get_zone(
				    cZCNameQ,
				    r_handle,
				    SCHA_TYPE, &type);
			}
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;

			ext_list =
				newObjNullParam(env,
				"java/util/ArrayList");

			if (ext_list == NULL)
			    goto attr_error;

			per_node_ext_list =
				newObjNullParam(env,
				"java/util/ArrayList");

			if (per_node_ext_list == NULL)
			    goto attr_error;

			/*
			 * Get the RT real name
			 */
			rgm_status = rgmcnfg_get_rtrealname(type,
				&rt_name, cZCNameQ);
			if (rgm_status.err_code != SCHA_ERR_NOERR)
			    goto attr_error;

			/*
			 * Get the resource type informations
			 */
			scha_status = rgm_scrgadm_getrtconf(
				(char *)rt_name, &rt, cZCNameQ);
			if (scha_status.err_code != SCHA_ERR_NOERR)
			    goto attr_error;

			/*
			 * Then, for each extention property, *
			 * retrieve * its value
			 */
			for (k = 0;
			k < (int)attr_value_stringarray->
				array_cnt; k++) {
			    scha_error = addExtProperty(env,
				    r_handle, ext_list,
				    per_node_ext_list,
				    node_list,
				    attr_value_stringarray->
				    str_array[k],
				    rt, cZCNameQ);
			    if (scha_error != SCHA_ERR_NOERR)
				goto attr_error;
			}
			(*env)->SetObjectField(env, rData,
				extPropertiesID,
				ext_list);
			(*env)->SetObjectField(env, rData,
				pernodeExtPropertiesID,
				per_node_ext_list);

			/*
			 * Add all the defined system
			 * properties with their current
			 * value.
			 */
			sys_list = newObjNullParam(env,
				"java/util/ArrayList");
			if (sys_list == NULL)
			    goto attr_error;

			scha_error = addSystemProperty(env,
				r_handle, sys_list, rt, cZCNameQ);
			if (scha_error != SCHA_ERR_NOERR)
			    goto attr_error;
			(*env)->SetObjectField(env, rData,
				systemPropertiesID,
				sys_list);
		    }
			/*
			 * Put this rData object into the rg_r_data_map
			 * and free the memory
			 */
			PUT(r_data_map,
				resource_list->str_array[j],
				rData);
			(*env)->DeleteLocalRef(env, rData);

			attr_error:
		    if (r_handle != NULL) {
			/*
			 * free resource handle
			 */
			(void) scha_resource_close(r_handle);
			r_handle = NULL;
		    }
			if (rt != NULL) {
			    rgm_free_rt(rt);
			    rt = NULL;
			}
			if (scha_error == SCHA_ERR_SEQID) {
			    /* exit loop to re-run fill_cache */
			    break;
			}

	} /* End of Iteration over Resources. */

	/*
	 * Put r_data_map thus formed in rg_r_data_map and free the
	 * memory.
	 */
	PUT(rg_r_data_map,
		rg_names->str_array[i],
		r_data_map);
	(*env)->DeleteLocalRef(env, r_data_map);

	rg_error:
		    if (rg_handle != NULL) {
			/*
			 * free memory pointed to by rg_names
			 */
			(void) scha_resourcegroup_close(rg_handle);
		    }
	if (scha_error == SCHA_ERR_SEQID) {
	    /* exit loop to re-run fill_cache */
	    break;
	}
	} /* End of Iteration over Resource Groups. */

    error:
		    if (sc_handle != NULL) {
			(void) scha_cluster_close(sc_handle);
		    }

    if (scha_error == SCHA_ERR_SEQID) {
	/*
	 * clean up rg_r_data_map, then restart
	 */
	jmethodID map_clear =
		(*env)->GetMethodID(env,
		map_class,
		"clear",
		"()V");
	if (map_clear != NULL) {
	    (*env)->CallObjectMethod(env, rg_r_data_map, map_clear);
	}
	goto start_fillcache;
	}

    return (rg_r_data_map);
}


/*
 * Class:     com_sun_cluster_agent_rgm_RGroup
 * Method:    getNodeList
 * Signature: (Ljava/lang/String;)Ljava/util/ArrayList;
 */
JNIEXPORT jobject JNICALL
	Java_com_sun_cluster_agent_rgm_RGroup_getNodeList(
			JNIEnv *env, jobject this, jstring zcNameQ) {

    jobject nodeList;
    char local_node[BUFFSIZE];
    char *cZCNameQ = NULL;
    int err = 0;

#if SOL_VERSION >= __s10

    struct zc_nodetab nodetab;
    jstring ret_string;
    zc_dochandle_t zone_handle = NULL;

#endif /* SOL_VERSION >= __s10 */

    if (zcNameQ != NULL) {
	const char *tmpS =
	(*env)->GetStringUTFChars(env, zcNameQ, NULL);
	cZCNameQ = strdup(tmpS);
	}

    debug printf(" START readData method \n");
    if (!initialized) {
	init(env);
	}

	/*
	* Initialize nodelist
	*/
    nodeList = (*env)->NewObject(env,
	    array_list_class,
	    array_list_constructor);
    if (nodeList == NULL) {
	goto end;
	}

    local_node[0] = '\0';
	(void) gethostname(local_node, sizeof (local_node));

#if SOL_VERSION >= __s10
	/*
	* Library initialization
	*/

    clconf_lib_init();

	/*
	* Open the handle to zonecfg
	*/
    zone_handle = zccfg_init_handle();
    if (zone_handle == NULL) {
	goto error;
	}
    err = zccfg_get_handle(cZCNameQ, zone_handle);
    if (err != ZC_OK) {
	goto error;
	}

	/*
	* Get the node list for this zone cluster
	*/
    err = zccfg_setnodeent(zone_handle);
    if (err != ZC_OK) {
	goto error;
	}

    while (zccfg_getnodeent(zone_handle, &nodetab) == ZC_OK) {
	ret_string = (jstring) newObjStringParam(env,
		"java/lang/String",
		nodetab.zc_nodename);

	if (ret_string != NULL &&
		strcmp(nodetab.zc_nodename, local_node) != 0) {
	    /* Update Nodelist */
	    (void) (*env)->CallObjectMethod(env,
		    nodeList,
		    array_list_add,
		    ret_string);
	} else {
	    nodeList = (*env)->NewObject(env,
		    array_list_class,
		    array_list_constructor);
	    break;
	}

	}

    error:
		    (void) zccfg_endnodeent(zone_handle);
	(void) zccfg_fini_handle(zone_handle);

#endif /* SOL_VERSION >= __s10 */

    end:
		    return (nodeList);
}


/*
 * Class:     com_sun_cluster_agent_rgm_RGroup
 * Method:    readDataJNI
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_rgm_RGroup_readDataJNI__Ljava_lang_\
String_2Ljava_lang_String_2(
JNIEnv *env, jobject this, jstring zcNameQ, jstring rgNameQ) {

    char *cZCNameQ = NULL;
    char *cRGNameQ = NULL;
    const char *tmpS;

    if (zcNameQ != NULL) {
	tmpS = (*env)->GetStringUTFChars(env, zcNameQ, NULL);
	cZCNameQ = strdup(tmpS);
	}
    if (rgNameQ != NULL) {
	tmpS = (*env)->GetStringUTFChars(env, rgNameQ, NULL);
	cRGNameQ = strdup(tmpS);
	}
    return (getResources(env, this, cZCNameQ, cRGNameQ));
}/*lint !e715 */
