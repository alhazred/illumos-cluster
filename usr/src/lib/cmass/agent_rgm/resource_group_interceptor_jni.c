/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Node mbean interceptor
 */

#pragma ident	"@(#)resource_group_interceptor_jni.c	1.8	08/05/20 SMI"

#include "com/sun/cluster/agent/rgm/ResourceGroupInterceptor.h"

#include <jniutil.h>
#include <string.h>
#include <stdio.h>
#include <scha.h>
#include <stdlib.h>

/*
 * RG helper funcion to retrieve a string type value and transform it to
 * a JNI jobject element.
 */
static jobject
    getRgStringAttribute(JNIEnv * env,
    scha_resourcegroup_t rg_handle, const char *attr_tag, scha_err_t *err)
{

	scha_err_t scha_error;
	char *attribute_value = NULL;
	jobject result = NULL;

	/*
	 * Calling SCHA API to retrieve the attribute value
	 */
	scha_error = scha_resourcegroup_get(rg_handle, attr_tag,
	    &attribute_value);
	*err = scha_error;

	if (scha_error != SCHA_ERR_NOERR) {
		attribute_value = "SCHA ERROR";
	}
	result = newObjStringParam(env, "java/lang/String", attribute_value);
	return (result);
}

/*
 * RG helper funcion to retrieve a boolean type value and transform it to
 * a JNI jobject element.
 */
static jobject
    getRgBooleanAttribute(JNIEnv * env,
    scha_resourcegroup_t rg_handle, const char *attr_tag, scha_err_t *err)
{

	scha_err_t scha_error;
	boolean_t attribute_value = B_FALSE;
	jobject result = NULL;

	/*
	 * Calling SCHA API to retrieve the attribute value
	 */
	scha_error = scha_resourcegroup_get(rg_handle, attr_tag,
	    &attribute_value);
	*err = scha_error;

	if (scha_error != SCHA_ERR_NOERR) {
		return (NULL);
	}
	if (attribute_value == B_TRUE) {
		result = newObjStringParam(env, "java/lang/Boolean", "true");
	} else {
		result = newObjStringParam(env, "java/lang/Boolean", "false");
	}
	return (result);
}

/*
 * RG helper funcion to retrieve a int type value and transform it to
 * a JNI jobject element.
 */
static jobject
    getRgIntAttribute(JNIEnv * env, scha_resourcegroup_t rg_handle,
    const char *attr_tag, scha_err_t *err)
{

	scha_err_t scha_error;
	int32_t attribute_value = -1;
	jobject result = NULL;

	/*
	 * Calling SCHA API to retrieve the attribute value
	 */
	scha_error = scha_resourcegroup_get(rg_handle, attr_tag,
	    &attribute_value);
	*err = scha_error;

	if (scha_error != SCHA_ERR_NOERR) {
		return (NULL);
	}
	result = newObjInt32Param(env, "java/lang/Integer", attribute_value);
	return (result);
}

/*
 * RG helper funcion to retrieve a string array type value and transform it to
 * a JNI jobject element.
 */
static jobject
    getRgStringArrayAttribute(JNIEnv * env,
    scha_resourcegroup_t rg_handle, const char *attr_tag,
    int *count, scha_err_t *err)
{

	scha_err_t scha_error;
	scha_str_array_t *attribute_value = NULL;
	jobject result = NULL;

	/*
	 * Calling SCHA API to retrieve the attribute value
	 */
	scha_error = scha_resourcegroup_get(rg_handle, attr_tag,
	    &attribute_value);
	*err = scha_error;

	if (scha_error != SCHA_ERR_NOERR)
		return (NULL);

	result = newStringArray(env, (int)attribute_value->array_cnt,
	    attribute_value->str_array);

	if (count != NULL)
		*count = (int)attribute_value->array_cnt;

	return (result);
}

/*
 * A couple of helper macros for things we do a lot,
 * * they contain references to local variables in the
 * * function below
 */
#define	PUT_ATTR(MAP, NAME, VALUE)		\
	PUT(MAP, NAME, (*env)->NewObject(env,	\
	    attribute_class,			\
	    attribute_constructor,		\
	    (*env)->NewStringUTF(env, NAME),	\
	    VALUE))

#define	PUT(MAP, NAME, VALUE)						\
	(*env)->CallObjectMethod(env,					\
				    MAP,				\
				    map_put,				\
				    (*env)->NewStringUTF(env, NAME),	\
				    VALUE)

/*
 * Class:     com_sun_cluster_agent_rgm_ResourceGroupInterceptor
 * Method:    fillCache
 * Signature: ()Ljava/util/Map;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_rgm_ResourceGroupInterceptor_fillCache
	(JNIEnv * env, jobject this) {

	/*
	 * return a Map of Maps containing all instances
	 * key = instanceName
	 * value = Map
	 *		key=attributeName
	 *		value=corresponding Attribute object
	 */

	jobject cache_map = NULL;
	jobject instance_map = NULL;
	jclass map_class = NULL;
	jmethodID map_put;

	jclass attribute_class = NULL;
	jmethodID attribute_constructor = NULL;

	unsigned int i, j;

	int attr_value_int = 0;
	scha_str_array_t *node_list = NULL;
	scha_str_array_t *attr_value_stringarray = NULL;
	jclass rgstatus_class = NULL;
	jmethodID rgstatus_constructor;

	/*
	 * SCHA error tag
	 */
	scha_err_t scha_error;

	/*
	 * RG handle.
	 */
	scha_resourcegroup_t rg_handle = NULL;

	/*
	 * Specific variable used to retrieve the "CurrentPrimaries" attribute
	 */
	scha_cluster_t sc_handle = NULL;
	scha_str_array_t *rg_names = NULL;

	/*
	 * get our references to 'attribute_class'
	 */

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * get constructor
	 */
	attribute_constructor =
	    (*env)->GetMethodID(env,
	    attribute_class,
	    "<init>",
	    "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;

	/*
	 * Create the map we are going to return
	 */
	cache_map = newObjNullParam(env, "java/util/HashMap");
	if (cache_map == NULL)
		goto error;

	/*
	 * get our references to map_class and the put method
	 */
	map_class = (*env)->FindClass(env, "java/util/HashMap");
	if (map_class == NULL)
		goto error;
	map_put =
	    (*env)->GetMethodID(env,
	    map_class,
	    "put",
	    "(Ljava/lang/Object;Ljava/lang/Object;)"
	    "Ljava/lang/Object;");
	if (map_put == NULL)
		goto error;

	/*
	 * get a handle to the Cluster
	 */
	start_fillcache:
	scha_error = scha_cluster_open(&sc_handle);
	if (scha_error != SCHA_ERR_NOERR) {
		sc_handle = NULL;
		goto error;
	}

	/*
	 * get rg names
	 */
	scha_error = scha_cluster_get(sc_handle,
	    SCHA_ALL_RESOURCEGROUPS, &rg_names);
	if ((scha_error != SCHA_ERR_NOERR) || (rg_names == NULL))
		goto error;

	/*
	 * Load ResourceGroupStatus class
	 */
	rgstatus_class = (*env)->FindClass(env,
	    "com/sun/cluster/agent/rgm/ResourceGroupStatus");
	if (rgstatus_class == NULL)
		goto error;

	/*
	 * Get constructor
	 */
	rgstatus_constructor = (*env)->GetMethodID(env,
	    rgstatus_class,
	    "<init>",
	    "(Ljava/lang/String;Lcom/sun/cluster/agent/rgm/"
	    "ResourceGroupStatusEnum;)V");
	if (rgstatus_constructor == NULL)
		goto error;

	for (i = 0; i < rg_names->array_cnt; i++) {

		/*
		 * CurrentPrimaries array
		 */
		char **current_primaries = NULL;
		unsigned int current_primaries_count = 0;

		/*
		 * we are going to create a map for this instance and
		 * populate it with its attributes
		 */

		instance_map = newObjNullParam(env, "java/util/HashMap");
		if (instance_map == NULL)
			goto error;

		/*
		 * Open this resource group
		 */
		scha_error =
		    scha_resourcegroup_open(rg_names->str_array[i],
		    &rg_handle);
		if (scha_error != SCHA_ERR_NOERR) {
			rg_handle = NULL;
			goto attr_error;
		}
		/*
		 * Get the list of nodes for this resource group
		 */
		scha_error = scha_resourcegroup_get(rg_handle,
		    SCHA_NODELIST,
		    &node_list);
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		/*
		 * Fill in the attributes
		 */
		PUT_ATTR(instance_map, "Name",
		    (*env)->NewStringUTF(env, (char *)
			rg_names->str_array[i]));


		{
			/*
			 * Deal with all attributes that necessitate  * *
			 * iterating over the nodes together
			 */

			scha_rgstate_t state = SCHA_RGSTATE_UNMANAGED;

			/*
			 * Status - per node status array
			 */

			jobject node_status;
			jobject node_status_array;

			current_primaries = (char **)
			    malloc(sizeof (char *) * node_list->array_cnt);
			if (current_primaries == NULL)
				goto attr_error;

			/*
			 * Construct status array
			 */
			node_status_array = (*env)->NewObjectArray(env,
			    (int)node_list->array_cnt,
			    rgstatus_class, NULL);
			if (node_status_array == NULL)
				goto attr_error;

			/*
			 * For each node, get its status and compare * to
			 * * the current
			 */
			for (j = 0;
			    j < node_list->array_cnt;
			    j++) {
				/*
				 * Get status
				 */
				scha_error = scha_resourcegroup_get(rg_handle,
				    SCHA_RG_STATE_NODE,
				    node_list->str_array[j],
				    &attr_value_int);
				if (scha_error != SCHA_ERR_NOERR)
					goto attr_error;
				/*
				 * Apply the rule on overall status :
				 * UNMANAGED < OFFLINE < ONLINE <
				 * PENDING_ONLINE < PENDING_OFFLINE <
				 * ERROR_STOP_FAILED < ONLINE_FAULTED
				 */
				if ((attr_value_int > SCHA_RGSTATE_OFFLINE) &&
				    (attr_value_int > state))
					state = attr_value_int;
				else if ((attr_value_int ==
						SCHA_RGSTATE_OFFLINE) &&
				    (state == SCHA_RGSTATE_UNMANAGED))
					state = attr_value_int;
				else if ((attr_value_int ==
					    SCHA_RGSTATE_ONLINE) &&
					    ((state ==
					    SCHA_RGSTATE_UNMANAGED) ||
					    (state == SCHA_RGSTATE_OFFLINE)))
					state = attr_value_int;

				/*
				 * Now construct and add * *
				 * ResourceGroupStatus object to array
				 */
				node_status =
				    (*env)->NewObject(env, rgstatus_class,
				    rgstatus_constructor,
				    (*env)->NewStringUTF(env,
					node_list->str_array[j]),
				    getCMASSEnum(env,
					"com/sun/cluster/agent/rgm/"
					"ResourceGroupStatusEnum",
					attr_value_int));
				/*
				 * Add object to the array
				 */
				(*env)->SetObjectArrayElement(env,
				    node_status_array, (int)j,
				    node_status);

				/*
				 * Add element to currentPrimaries?
				 */
				if (attr_value_int == SCHA_RGSTATE_ONLINE) {
					char *name =
					    strdup(node_list->str_array[j]);
					if (name == NULL)
						goto attr_error;
					current_primaries[
					    current_primaries_count++] =
					    name;
				}
			}

			PUT_ATTR(instance_map, "OverallStatus",
			    getCMASSEnum(env,
				"com/sun/cluster/agent/rgm/"
				"ResourceGroupStatusEnum",
				state));

			PUT_ATTR(instance_map, "Managed",
			    newBoolean(env,
				!(state == SCHA_RGSTATE_UNMANAGED)));

			PUT_ATTR(instance_map, "Online",
			    newBoolean(env, (state == SCHA_RGSTATE_ONLINE)));

			PUT_ATTR(instance_map, "Status",
			    node_status_array);

			PUT_ATTR(instance_map, "CurrentPrimaries",
			    newStringArray(env,
				(int)current_primaries_count,
				current_primaries));

			PUT_ATTR(instance_map, "CurrentPrimariesCount",
			    newObjInt32Param(env, "java/lang/Integer",
				(int)current_primaries_count));
		}
		{
			scha_error = scha_resourcegroup_get(rg_handle,
			    SCHA_RG_MODE,
			    &attr_value_int);
			if (scha_error != SCHA_ERR_NOERR)
				goto attr_error;
			/*
			 * Get the corresponding Enum
			 */
			PUT_ATTR(instance_map, "Mode",
			    getCMASSEnum(env,
				"com/sun/cluster/agent/rgm/"
				"ResourceGroupModeEnum",
				attr_value_int));
		}

		PUT_ATTR(instance_map, "AutoStartable",
		    getRgBooleanAttribute(env, rg_handle,
			SCHA_RG_AUTO_START, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "NetworkDependent",
		    getRgBooleanAttribute(env, rg_handle,
			SCHA_IMPL_NET_DEPEND, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "System",
		    getRgBooleanAttribute(env, rg_handle,
			SCHA_RG_SYSTEM, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "FailbackEnabled",
		    getRgBooleanAttribute(env, rg_handle,
			SCHA_FAILBACK, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "Suspended",
		    getRgBooleanAttribute(env, rg_handle,
			SCHA_RG_SUSP_AUTO_RECOVERY, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		{
			scha_error = scha_resourcegroup_get(rg_handle,
			    SCHA_GLOBAL_RESOURCES_USED,
			    &attr_value_stringarray);
			if (scha_error != SCHA_ERR_NOERR)
				goto attr_error;

			PUT_ATTR(instance_map, "SharedStorageUsed",
			    newBoolean(env,
				attr_value_stringarray->is_ALL_value));
		}

		PUT_ATTR(instance_map, "NodeNames",
		    newStringArray(env,
			(int)node_list->array_cnt,
			node_list->str_array));

		PUT_ATTR(instance_map, "DesiredPrimariesCount",
		    getRgIntAttribute(env, rg_handle,
			SCHA_DESIRED_PRIMARIES, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "MaxPrimaries",
		    getRgIntAttribute(env, rg_handle,
			SCHA_MAXIMUM_PRIMARIES, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		{
			int dependencies_count = 0;
			PUT_ATTR(instance_map, "Dependencies",
			    getRgStringArrayAttribute(env, rg_handle,
				SCHA_RG_DEPENDENCIES, &dependencies_count,
				&scha_error));
			if (scha_error != SCHA_ERR_NOERR)
				goto attr_error;

			PUT_ATTR(instance_map, "DependenciesCount",
			    newObjInt32Param(env,
				"java/lang/Integer",
				dependencies_count));
		}


		PUT_ATTR(instance_map, "DirectoryPath",
		    getRgStringAttribute(env, rg_handle, SCHA_PATHPREFIX,
		    &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "Description",
		    getRgStringAttribute(env, rg_handle, SCHA_RG_DESCRIPTION,
		    &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "FailureInterval",
		    getRgIntAttribute(env, rg_handle, SCHA_PINGPONG_INTERVAL,
		    &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "Affinities",
		    getRgStringArrayAttribute(env,
			rg_handle, SCHA_RG_AFFINITIES, NULL, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "ProjectName",
		    getRgStringAttribute(env,
			rg_handle, SCHA_RG_PROJECT_NAME, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "SLMType",
		    getRgStringAttribute(env,
			rg_handle, SCHA_RG_SLM_TYPE, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "SLMPsetType",
		    getRgStringAttribute(env,
			rg_handle, SCHA_RG_SLM_PSET_TYPE, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "SLMCPUShares",
		    getRgIntAttribute(env,
			rg_handle, SCHA_RG_SLM_CPU_SHARES, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "SLMPsetMin",
		    getRgIntAttribute(env,
			rg_handle, SCHA_RG_SLM_PSET_MIN, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		PUT_ATTR(instance_map, "ResourceNames",
		    getRgStringArrayAttribute(env,
			rg_handle, SCHA_RESOURCE_LIST, NULL, &scha_error));
		if (scha_error != SCHA_ERR_NOERR)
			goto attr_error;

		/*
		 * Now put this map into the returned map under this * *
		 * instance
		 */
		PUT(cache_map,
		    rg_names->str_array[i],
		    instance_map);

	    attr_error:
		if (rg_handle != NULL) {
			(void) scha_resourcegroup_close(rg_handle);
		}
		if (current_primaries != NULL) {
			for (j = 0; j < current_primaries_count; j++) {
				free(current_primaries[j]);
			}
			free(current_primaries);
			current_primaries = NULL;
		}

		if (scha_error == SCHA_ERR_SEQID) {
			/* exit loop to re-run fillcache */
			break;
		}
	}

    error:

	if (sc_handle != NULL) {
		(void) scha_cluster_close(sc_handle);
	}

	if (scha_error == SCHA_ERR_SEQID) {
		/* clean up cache_map, then re-run fillcache */
		jmethodID map_clear =
		    (*env)->GetMethodID(env,
		    map_class,
		    "clear",
		    "()V");
		if (map_clear != NULL) {
		    (*env)->CallObjectMethod(env, cache_map, map_clear);
		}
		goto start_fillcache;
	}

	return (cache_map);
} /*lint !e715 */
