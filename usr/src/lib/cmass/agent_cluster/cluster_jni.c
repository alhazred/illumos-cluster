/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)cluster_jni.c	1.9	08/07/23 SMI"

#include <stdlib.h>
#include <stdio.h>

#include "com/sun/cluster/agent/cluster/Cluster.h"

#include <jniutil.h>
#include <cl_query/cl_query_types.h>

#if SOL_VERSION >= __s10
#include <sys/clconf_int.h>
#endif /* SOL_VERSION >= __s10 */

/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    getName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_getNameJNI
	(JNIEnv * env, jobject clazz) {

	jstring retval = NULL;

	cl_query_result_t cl_query_result;

	cl_query_error_t cl_query_error =
	    cl_query_get_info(CL_QUERY_CLUSTER_INFO,
	    "",
	    &cl_query_result);

	if (cl_query_error != CL_QUERY_OK) {
		throwByName(env, "java/io/IOException",
		    "unable to obtain cluster info");
	} else {
		cl_query_cluster_info_t *cl_query_cluster_info_p =
		    (cl_query_cluster_info_t *)cl_query_result.value_list;

		if (cl_query_cluster_info_p->cluster_name != NULL) {
			retval = (*env)->NewStringUTF(env,
			    cl_query_cluster_info_p->cluster_name);
		}
		(void) cl_query_free_result(CL_QUERY_CLUSTER_INFO,
		    &cl_query_result);
	}

	return (retval);
} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    getClusterId
 * Signature: ()I
 */
JNIEXPORT jstring JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_getClusterId
	(JNIEnv * env, jobject this) {

	jstring retval = NULL;

	cl_query_result_t cl_query_result;

	cl_query_error_t cl_query_error =
	    cl_query_get_info(CL_QUERY_CLUSTER_INFO,
	    (char *)NULL,
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		throwByName(env, "java/io/IOException",
		    "unable to obtain cluster ID");
	} else {
		cl_query_cluster_info_t *cl_query_cluster_info_p =
		    (cl_query_cluster_info_t *)cl_query_result.value_list;
		retval = (*env)->NewStringUTF(env,
		    cl_query_cluster_info_p->cluster_id);
		(void) cl_query_free_result(CL_QUERY_CLUSTER_INFO,
		    &cl_query_result);
	}

	return (retval);

} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    getProductVersion
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_getProductVersion
	(JNIEnv * env, jobject this) {

	jstring retval = NULL;

	cl_query_result_t cl_query_result;

	cl_query_error_t cl_query_error =
	    cl_query_get_info(CL_QUERY_CLUSTER_INFO,
	    "",
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		throwByName(env, "java/io/IOException",
		    "unable to obtain cluster info");
	} else {
		cl_query_cluster_info_t *cl_query_cluster_info_p =
		    (cl_query_cluster_info_t *)cl_query_result.value_list;
		if (cl_query_cluster_info_p->cluster_version != NULL) {
			retval = (*env)->NewStringUTF(env,
			    cl_query_cluster_info_p->cluster_version);
		}
		(void) cl_query_free_result(CL_QUERY_CLUSTER_INFO,
		    &cl_query_result);
	}

	return (retval);
} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    isInInstallMode
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_isInInstallMode
	(JNIEnv * env, jobject this) {

	jboolean retval = JNI_FALSE;

	cl_query_result_t cl_query_result;

	cl_query_error_t cl_query_error =
	    cl_query_get_info(CL_QUERY_CLUSTER_INFO,
	    "",
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		throwByName(env, "java/io/IOException",
		    "unable to obtain cluster info");
	} else {
		cl_query_cluster_info_t *cl_query_cluster_info_p =
		    (cl_query_cluster_info_t *)cl_query_result.value_list;

		retval =
		    (cl_query_cluster_info_p->install_mode ==
		    CL_QUERY_ENABLED) ?
		    JNI_TRUE : JNI_FALSE;

		(void) cl_query_free_result(CL_QUERY_CLUSTER_INFO,
		    &cl_query_result);
	}

	return (retval);
} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    getNewNodeAuthType
 * Signature: ()Lcom/sun/cluster/agent/cluster/ClusterAuthEnum;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_getNewNodeAuthType
	(JNIEnv * env, jobject this) {

	jobject retval = NULL;

	cl_query_result_t cl_query_result;

	cl_query_error_t cl_query_error =
	    cl_query_get_info(CL_QUERY_CLUSTER_INFO,
	    "",
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		throwByName(env, "java/io/IOException",
		    "unable to obtain cluster info");
	} else {
		cl_query_cluster_info_t *cl_query_cluster_info_p =
		    (cl_query_cluster_info_t *)cl_query_result.value_list;
		retval = getCMASSEnum(env,
		    "com/sun/cluster/agent/cluster/ClusterAuthEnum",
		    cl_query_cluster_info_p->cluster_auth_type);

		(void) cl_query_free_result(CL_QUERY_CLUSTER_INFO,
		    &cl_query_result);
	}

	return (retval);
} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    getNewNodeList
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_getNewNodeList
	(JNIEnv * env, jobject this) {

	jobjectArray retval = NULL;

	cl_query_result_t cl_query_result;

	cl_query_error_t cl_query_error =
	    cl_query_get_info(CL_QUERY_CLUSTER_INFO,
	    "",
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		throwByName(env, "java/io/IOException",
		    "unable to obtain cluster info");
	} else {
		cl_query_cluster_info_t *cl_query_cluster_info_p =
		    (cl_query_cluster_info_t *)cl_query_result.value_list;

		if (cl_query_cluster_info_p->cluster_join_list.list_len > 0) {
			retval = newStringArray(env,
			    (int)cl_query_cluster_info_p->
			    cluster_join_list.list_len,
			    cl_query_cluster_info_p->
			    cluster_join_list.list_val);
		} else {
			/* BugId  */
			retval = newStringArray(env, 0, NULL);
		}
		(void) cl_query_free_result(CL_QUERY_CLUSTER_INFO,
		    &cl_query_result);
	}

	return (retval);
} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    getNetAddr
 * Signature: ()I
 */
JNIEXPORT jstring JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_getNetAddr
	(JNIEnv * env, jobject this) {

	jstring retval = NULL;

	cl_query_result_t cl_query_result;

	cl_query_error_t cl_query_error =
	    cl_query_get_info(CL_QUERY_CLUSTER_INFO,
	    (char *)NULL,
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		throwByName(env, "java/io/IOException",
		    "unable to obtain cluster network address");
	} else {
		cl_query_cluster_info_t *cl_query_cluster_info_p =
		    (cl_query_cluster_info_t *)cl_query_result.value_list;
		if (cl_query_cluster_info_p->cluster_netaddr != NULL) {
			retval = (*env)->NewStringUTF(env,
			    cl_query_cluster_info_p->cluster_netaddr);
		}
		(void) cl_query_free_result(CL_QUERY_CLUSTER_INFO,
		    &cl_query_result);
	}

	return (retval);

} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    getNetmask
 * Signature: ()I
 */
JNIEXPORT jstring JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_getNetmask
	(JNIEnv * env, jobject this) {

	jstring retval = NULL;

	cl_query_result_t cl_query_result;

	cl_query_error_t cl_query_error =
	    cl_query_get_info(CL_QUERY_CLUSTER_INFO,
	    (char *)NULL,
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		throwByName(env, "java/io/IOException",
		    "unable to obtain cluster netmask");
	} else {
		cl_query_cluster_info_t *cl_query_cluster_info_p =
		    (cl_query_cluster_info_t *)cl_query_result.value_list;
		if (cl_query_cluster_info_p->cluster_netmask != NULL) {
			retval = (*env)->NewStringUTF(env,
			    cl_query_cluster_info_p->cluster_netmask);
		}
		(void) cl_query_free_result(CL_QUERY_CLUSTER_INFO,
		    &cl_query_result);
	}

	return (retval);

} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    getMaxNodes
 * Signature: ()I
 */
JNIEXPORT jstring JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_getMaxNodes
	(JNIEnv * env, jobject this) {

	jstring retval = NULL;

	cl_query_result_t cl_query_result;

	cl_query_error_t cl_query_error =
	    cl_query_get_info(CL_QUERY_CLUSTER_INFO,
	    (char *)NULL,
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		throwByName(env, "java/io/IOException",
		    "unable to obtain cluster maximum nodes");
	} else {
		cl_query_cluster_info_t *cl_query_cluster_info_p =
		    (cl_query_cluster_info_t *)cl_query_result.value_list;
		if (cl_query_cluster_info_p->cluster_max_nodes != NULL) {
			retval = (*env)->NewStringUTF(env,
			    cl_query_cluster_info_p->cluster_max_nodes);
		}
		(void) cl_query_free_result(CL_QUERY_CLUSTER_INFO,
		    &cl_query_result);
	}

	return (retval);

} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    getMaxPrivNets
 * Signature: ()I
 */
JNIEXPORT jstring JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_getMaxPrivNets
	(JNIEnv * env, jobject this) {

	jstring retval = NULL;

	cl_query_result_t cl_query_result;

	cl_query_error_t cl_query_error =
	    cl_query_get_info(CL_QUERY_CLUSTER_INFO,
	    (char *)NULL,
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		throwByName(env, "java/io/IOException",
		    "unable to obtain cluster maximum private networks");
	} else {
		cl_query_cluster_info_t *cl_query_cluster_info_p =
		    (cl_query_cluster_info_t *)cl_query_result.value_list;
		if (cl_query_cluster_info_p->cluster_max_priv_nets != NULL) {
			retval = (*env)->NewStringUTF(env,
			    cl_query_cluster_info_p->cluster_max_priv_nets);
		}
		(void) cl_query_free_result(CL_QUERY_CLUSTER_INFO,
		    &cl_query_result);
	}

	return (retval);

} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    isFMMEnabled
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_isFMMEnabled
	(JNIEnv * env, jobject this) {

	jboolean retval = JNI_FALSE;

	cl_query_result_t cl_query_result;

	cl_query_error_t cl_query_error =
	    cl_query_get_info(CL_QUERY_CLUSTER_INFO,
	    "",
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		throwByName(env, "java/io/IOException",
		    "unable to obtain cluster info");
	} else {
		cl_query_cluster_info_t *cl_query_cluster_info_p =
		    (cl_query_cluster_info_t *)cl_query_result.value_list;

		retval =
		    (cl_query_cluster_info_p->fmm_mode == 1) ?
		    JNI_TRUE : JNI_FALSE;

		(void) cl_query_free_result(CL_QUERY_CLUSTER_INFO,
		    &cl_query_result);
	}

	return (retval);
} /*lint !e715 */

/*
 * Method:    getAllZones
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_getAllZones
	(JNIEnv * env, jobject this) {
	jobjectArray retval = NULL;

	scha_cluster_t sc_handle = NULL;

	scha_err_t scha_error;

	scha_str_array_t *logical_nodenames = NULL;

	scha_error = scha_cluster_open(&sc_handle);

	if (scha_error != SCHA_ERR_NOERR) {
		sc_handle = NULL;
		goto error;
	}

	/*
	 * Get all logical nodenames known to the RGM within the cluster
	 */
	scha_error = scha_cluster_get(sc_handle,
		SCHA_ALL_ZONES,
		&logical_nodenames);

	if ((scha_error != SCHA_ERR_NOERR) ||
		(logical_nodenames == NULL)) {
		goto error;
	}
	retval = newStringArray(env,
			(int)logical_nodenames->array_cnt,
			logical_nodenames->str_array);

	error:
	/*
	 * Free the specific resources allocated in this function
	 */

	if (sc_handle != NULL) {
		/*
		 * free memory pointed to by cluster_names
		 */
		(void) scha_cluster_close(sc_handle);
	}
	return (retval);
}

#if SOL_VERSION >= __s10
/*
 * Class:     com_sun_cluster_agent_cluster_Cluster
 * Method:    getZoneClusterList
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jobjectArray JNICALL
Java_com_sun_cluster_agent_cluster_Cluster_getZoneClusterList
	(JNIEnv *env, jclass this) {

	char **zone_cluster_arr;
	unsigned int zc_count = 0;
	int i = 0;

	jobjectArray return_array = NULL;

	clconf_lib_init();
	zc_count = clconf_cluster_get_num_clusters();
	zone_cluster_arr = clconf_cluster_get_cluster_names();

	return_array = newStringArray(env,
			(int)zc_count,
			zone_cluster_arr);

	return (return_array);
}
#endif /* SOL_VERSION >= __s10 */
