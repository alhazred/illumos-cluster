/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ClusterModule.java 1.12     08/05/20 SMI"
 */

package com.sun.cluster.agent.cluster;

// JDK
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.ObjectName;

// Cacao
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.common.ClusterRuntimeException;


public class ClusterModule extends Module {

    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.cluster");

    /** used to check cluster mode */
    private static Boolean initialized = Boolean.FALSE;
    private static boolean inClusterMode = false;

    /** internal reference to our mbean object name */
    private ObjectName clusterMBeanObjectName = null;

    public ClusterModule(DeploymentDescriptor descriptor) {
        super(descriptor);

        synchronized (initialized) {

            if (!(initialized.booleanValue())) {
                inClusterMode = false;

                try {
                    InvocationStatus ret = InvokeCommand.execute(
                            new String[] { "/usr/sbin/clinfo" },
                            new String[] {});

                    if (ret.getExitValue().intValue() == 0) {
                        inClusterMode = true;
                    }
                } catch (Exception e) {
                    // Ignore - unable to launch command - not cluster mode
                }

                initialized = Boolean.TRUE;
            }
        }

        if (!inClusterMode) {
            throw new RuntimeException("Not in cluster mode");
        }

        Map parameters = descriptor.getParameters();

        try {

            // Load the agent JNI library
            String library = (String) parameters.get("library");
            Runtime.getRuntime().load(library);

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING,
                "Unable to load native runtime library" + ule);
            throw ule;
        }
    }

    protected void start() throws ClusterRuntimeException {

        try {
            ClusterMBean clusterMBean = new Cluster();

            clusterMBeanObjectName =
                new ObjectNameFactory(getDomainName()).getObjectName(
                    ClusterMBean.class, null);
            getMbs().registerMBean(clusterMBean, clusterMBeanObjectName);

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING, "Unable to start Cluster module" + e);
            throw new ClusterRuntimeException(e.getMessage());
        }
    }

    protected void stop() {

        try {
            getMbs().unregisterMBean(clusterMBeanObjectName);
        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING, "Unable to stop Cluster module" + e);
            throw new ClusterRuntimeException(e.getMessage());
        }
    }
}
