/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ClusterAuthEnum.java 1.9     08/05/20 SMI"
 */

package com.sun.cluster.agent.cluster;

/**
 * The Resource fault monitor status attribute enum as defined in the SCHA API.
 */
public class ClusterAuthEnum extends com.sun.cacao.Enum
    implements java.io.Serializable {

    public static final ClusterAuthEnum Unix = new ClusterAuthEnum("Unix", 0);

    public static final ClusterAuthEnum DES = new ClusterAuthEnum("DES");

    public static final ClusterAuthEnum Unknown = new ClusterAuthEnum(
            "Unknown");

    private ClusterAuthEnum(String name) {
        super(name);
    }

    private ClusterAuthEnum(String name, int ord) {
        super(name, ord);
    }
}
