/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)Cluster.java	1.11	08/07/23 SMI"
 */

package com.sun.cluster.agent.cluster;

// local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;

import java.util.ArrayList;

/**
 * Implementation of {@link ClusterMBean} interface.
 */
public class Cluster implements ClusterMBean {
    /** Constant defining the value for the TYPE attribute in the object name */
    public static final String TYPE = "cluster";

    /**
     * A helper method, not part of the mbean interface, for other
     * modules to use to obtain the cluster name.
     *
     * @return a <code>String</code> value
     */
    public static String getClusterName() {
	return getNameJNI();
    }

    /**
     * Return cluster name
     *
     * @return a <code>String</code> value
     */
    public String getName() {
	return getNameJNI();
    }

    /**
     * Return the product version string for this cluster
     *
     * @return a <code>String</code> value
     */
    public native String getProductVersion();

    /**
     * Get the cluster's unique ID
     */
    public native String getClusterId();

    /**
     * Find out if the cluster node is install mode
     *
     * @return true if the cluster node is in install mode
     */
    public native boolean isInInstallMode();

    /**
     * Get the cluster authorization type required for nodes to
     * join this cluster
     * @return an authorization value from the enum
     */
    public native ClusterAuthEnum getNewNodeAuthType();

    /**
     * Get the list of nodes permitted to join this cluster
     * @return an array of node names permitted to join
     */
    public native String[] getNewNodeList();

    /**
     * Get the cluster network address
     */
    public native String getNetAddr();

    /**
     * Get the cluster netmask
     */
    public native String getNetmask();

    /**
     * Get the maximum number of nodes
     */
    public native String getMaxNodes();

    /**
     * Get the maximum number of private networks.
     */
    public native String getMaxPrivNets();

    private native static String getNameJNI();

    /**
     * Determine whether FMM is enabled
     */
    public native boolean isFMMEnabled();

    /**
     * Get the list of nodes including the logical nodenames.
     * @return an array of nodes and logical nodes
     */
    public native String[] getAllZones();

    /**
     * Get the list of all zone clusters
     * @return an array of all zone clusters
     */
    public native String[] getZoneClusterList();
}

