/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ClusterMBean.java	1.11	08/07/23 SMI"
 */

package com.sun.cluster.agent.cluster;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;

import java.util.ArrayList;

/**
 * MBean interface for describing a cluster in a wide way.
 */
public interface ClusterMBean {

    /**
     * This field is used internally by the CMASS agent to identify the
     * TYPE of the module.
     */
    public final static String TYPE = "cluster";

    /**
     * Get the cluster's name.
     * @return cluster's name.
     */
    public String getName();

    /**
     * Get the product's name and version implementing the cluster.
     * @return product's name and version.
     */
    public String getProductVersion();

    /**
     * Get the unique cluster's 32-bit ID
     */
    public String getClusterId();

    /**
     * Find out if the cluster node is install mode
     *
     * @return true if the cluster node is in install mode
     */
    public boolean isInInstallMode();

    /**
     * Get the cluster authorization type required for nodes to
     * join this cluster
     * @return an authorization value from the enum
     */
    public ClusterAuthEnum getNewNodeAuthType();

    /**
     * Get the list of nodes permitted to join this cluster
     * @return an array of node names permitted to join
     */
    public String[] getNewNodeList();

    /**
     * Get the list of nodes including the logical nodenames.
     * @return an array of nodes and logical nodes
     */
    public String[] getAllZones();

    /**
     * Get the list of all zone clusters
     * @return an array of all zone clusters
     */
    public String[] getZoneClusterList();

    /**
     * Get the cluster network address
     */
    public String getNetAddr();

    /**
     * Get the cluster netmask
     */
    public String getNetmask();

    /**
     * Get the maximum number of nodes
     */
    public String getMaxNodes();

    /**
     * Get the maximum number of private networks
     */
    public String getMaxPrivNets();

    /**
     * Determine whether FMM is enabled or not
     */
    public boolean isFMMEnabled();
}
