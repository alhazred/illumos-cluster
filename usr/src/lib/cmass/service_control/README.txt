#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

 ident	"@(#)README.txt	1.2	08/05/20 SMI"


    CMAS Failover module
    ====================
    
A CMAS failover module must have in its property file 
the 2 following lines :
    
    module.state=LOCKED
    module.failover=<failover group name>
    
When the CMASS is started, this kind of module remains locked.

To manage properly this module with the RGM you have
to do the following :

- create a failover resource group <rg>
- create a SUNW.LogicalHostname resource <logicalAddress> in <rg>
- create a SUNW.scmasa resource <cmasResModule> in <rg> using the 
  network resource <logicalAddress>.    
    
  The link between <cmasResModule> and the failover module 
  is either :
      the <cmasResModule> extension property named Cmas_failover_group     
      has a value of <failover group name>
  or
      <cmasResModule> = <failover group name>     
    
Remark : more than one CMAS module can share the same value
         for <failover group name>. In that case they will all
         be activated, deactivated or checked at the same time.
 
To activate a CMAS module configured as explained above,
all you have to do is to use RGM commands to bring online 
the resource group <rg>.

To deactivate it, just take the resource group <rg> offline.

The Health check will be done through <logicalAddress> only
for online resources.

The resource type SUNW.scmasa is installed and registered with
the package SUNWscmasa. 
Its source code is located in cmd/ha-services/hamasa.

To communicate with the CMAS, the methods of this resource type
use 3 small shell scripts located in /usr/cluster/bin and named
cmas_service_ctrl_*.sh.

These 3 shell scripts all use the small java program named
ServiceControl.java to send request to the CMAS agent located
on the local node.
