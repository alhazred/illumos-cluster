/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  @(#)ServiceControl.java 1.15   08/05/20 SMI
 */

// Cacao
import com.sun.cacao.ModuleMBean;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.JmxClient;
import com.sun.cacao.container.HealthCheckFailureException;
import com.sun.cacao.element.OperationalStateEnum;

import com.sun.cluster.agent.failovercontrol.FailoverControl;
import com.sun.cluster.agent.failovercontrol.FailoverControlMBean;

// Local
import com.sun.cluster.common.TrustedMBeanModel;

import java.io.IOException;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;

// JMX
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;


public class ServiceControl {

    //
    // value coming from the SCDS toolkit
    // see manual for function scds_fm_action
    //
    static private int SCDS_PROBE_COMPLETE_FAILURE = 100;

    static private Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.servicecontrol");

    static private MBeanServerConnection mbsc = null;

    public static void main(String args[]) {

        int retval = 0; // SUCCESS
        String agentLoc = null;
        String command = null;
        String failoverGroup = null;
        FailoverControlMBean mbean = null;
        JMXConnector jmxConn = null;

        try {

            if (args.length != 3)
                usage();

            agentLoc = args[0];
            command = args[1];
            failoverGroup = args[2];

            if (failoverGroup == null)
                usage();

            jmxConn = TrustedMBeanModel.getWellKnownConnector(agentLoc);
            mbsc = jmxConn.getMBeanServerConnection();

            // Look up the service control module
            mbean = (FailoverControlMBean) JmxClient.getMBeanProxy(mbsc,
                    new ObjectNameFactory(
                        FailoverControlMBean.class.getPackage().getName()),
                    FailoverControlMBean.class, null, false);

        } catch (Exception e) {
            logger.warning(
                "Unable to connect to the CACAO agent. The agent may be down or restarting");

            if ((command.equals("unlock")) || (command.equals("check")))
                retval = SCDS_PROBE_COMPLETE_FAILURE / 2;
            else {
                retval = 0; // Stop is a success because the agent is down
                            // anyways
            }

            try {

                if (jmxConn != null)
                    jmxConn.close();
            } catch (IOException ioe) {
                logger.warning("Unable to close the JMX Connection cleanly");
            }

            System.exit(retval);
        }

        if (command.equals("unlock")) {

            if (!mbean.startFailoverGroup(failoverGroup))
                retval = SCDS_PROBE_COMPLETE_FAILURE / 2;

        } else if (command.equals("lock")) {

            mbean.stopFailoverGroup(failoverGroup);

        } else if (command.equals("check")) {

            if (!mbean.isFailoverGroupHealthy(failoverGroup))
                retval = SCDS_PROBE_COMPLETE_FAILURE;
        } else {

            usage();

        }

        try {

            if (jmxConn != null)
                jmxConn.close();
        } catch (IOException ioe) {
            logger.warning("Unable to close the JMX Connection cleanly");
        }

        System.exit(retval);
    }

    private static void usage() {
        System.out.println(
            "Usage: cmasservicectl <endpoint> ( unlock | lock | check ) <failover group name>");
        System.exit(1);
    }

}
