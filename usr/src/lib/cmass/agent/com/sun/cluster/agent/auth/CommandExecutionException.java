/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)CommandExecutionException.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.auth;

import java.util.ArrayList;
import java.util.List;


/**
 * Signals a failure when executing a command.
 */

public class CommandExecutionException
    extends com.sun.cluster.common.ClusterException
    implements java.io.Serializable {

    /** internal reference to the command's ExitStatus array */
    private ExitStatus exitStatusArray[];

    /**
     * Constructs a CommandExecutionException with the specified message. Sets
     * an empty ExitStatus array
     *
     * @param  msg  the detailed message.
     */
    public CommandExecutionException(String msg) {
        super(msg);
        exitStatusArray = new ExitStatus[0];
    }

    /**
     * Constructs a CommandExecutionException based on the values stored in the
     * ExitStatus array and adds the specified message.
     *
     * @param  msg  the detailed message.
     * @param  exitStatusArray  array of ExitStatus object
     */

    public CommandExecutionException(String msg, ExitStatus exitStatusArray[]) {
        super(msg);
        setExitStatusArray(exitStatusArray);
    }

    /**
     * Constructs a CommandExecutionException based on the values stored in
     * ExitStatus  and adds the specified message. Sets an ExitStatus array with
     * the ExitStatus object (shortcut)
     *
     * @param  msg  the detailed message.
     * @param  exitStatus  ExitStatus object
     */
    public CommandExecutionException(String msg, ExitStatus exitStatus) {
        this(msg, new ExitStatus[] { exitStatus });
    }

    /**
     * Returns ExitStatusArray object from a failed command execution.
     *
     * @return  ExitStatus array object
     */
    public ExitStatus[] getExitStatusArray() {
        return exitStatusArray;
    }

    /**
     * Initialize ExitStatus array object that can have null element at the end,
     * in this case it is internally resized.
     *
     * @param  arr  ExitStatus array object
     */
    public void setExitStatusArray(ExitStatus arr[]) {

        // either empty array or full array
        if (arr == null)
            this.exitStatusArray = new ExitStatus[0];
        else {
            List values = new ArrayList(arr.length);
            int i = 0;

            for (i = 0; i < arr.length; i++) {

                if (arr[i] == null)
                    break;

                values.add(arr[i]);
            }

            this.exitStatusArray = (ExitStatus[]) values.toArray(
                    new ExitStatus[0]);
        }
    }

}
