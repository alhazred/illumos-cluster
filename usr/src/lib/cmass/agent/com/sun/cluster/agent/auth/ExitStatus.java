/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ExitStatus.java 1.11     08/05/20 SMI"
 */

package com.sun.cluster.agent.auth;

import com.sun.cacao.invocation.InvocationStatus;

import java.util.*;


/**
 * This class presents information related to a command execution. It enables
 * the caller of a command to determine the outcome and the reasons for the
 * possible failures of the command.
 */

public class ExitStatus implements java.io.Serializable {

    /** constant value on exit when succesful */
    public static final int SUCCESS = 0;

    /** internal attr */
    private Integer returnCode = null;

    /** internal attr */
    private String cmdExecuted[] = null;

    /** internal attr (stdout output) */
    private List outStrings = null;

    /** internal attr (stderr output) */
    private List errStrings = null;

    /**
     * Constructs a default ExitStatus object.
     */
    public ExitStatus() {
    }

    /**
     * Constructs ExitStatus object with the specified command line, command
     * execution return code, execution output strings and execution error
     * strings.
     *
     * @param  cmdLine  the command that was run
     * @param  exitCode  the return code of the executed command
     * @param  out  output strings from the executed command
     * @param  errs  error  strings from the executed command
     */
    public ExitStatus(String cmdLine[], int exitCode, List out, List errs) {

        returnCode = new Integer(exitCode);
        cmdExecuted = cmdLine;
        outStrings = out;
        errStrings = errs;
    }

    /**
     * Returns the return code from a command execution, or null if the command
     * did not manage to be executed
     *
     * @return  the return code
     */

    public Integer getReturnCode() {
        return returnCode;
    }

    /**
     * Returns command string.
     *
     * @return  executed command string
     */

    public String[] getCmdExecuted() {
        return cmdExecuted;
    }

    /**
     * Returns output strings from command execution.
     *
     * @return  the output strings
     */
    public List getOutStrings() {
        return outStrings;
    }

    /**
     * Returns error strings from a failed command execution.
     *
     * @return  the error strings
     */
    public List getErrStrings() {
        return errStrings;
    }

    /**
     * Sets the return code.
     *
     * @param  rc  process's return code
     */

    public void setReturnCode(int rc) {
        returnCode = new Integer(rc);

        return;
    }

    /**
     * Sets the command string.
     *
     * @param  cmd  command line executed
     */

    public void setCmdExecuted(String cmd[]) {
        cmdExecuted = cmd;

        return;
    }

    /**
     * Sets output strings from command execution.
     *
     * @param  outstr  the stdout info obtained from process
     */
    public void setOutStrings(List outstr) {
        outStrings = outstr;

        return;
    }

    /**
     * Sets error strings from a failed command execution.
     *
     * @param  errstr  the stderr info obtained from process
     */
    public void setErrStrings(List errstr) {
        errStrings = errstr;

        return;
    }

    /**
     * Return a printable version of this object
     *
     * @return  a printable string
     */
    public String toString() {

        return "Command executed : " + getCmdExecuted() + "\n" +
            "Return code      : " + getReturnCode() + "\n";
    }

    public static ExitStatus[] createArray(InvocationStatus status[]) {
        ExitStatus exitWrapper[] = new ExitStatus[status.length];

        for (int i = 0; i < status.length; i++) {
            String outRes = status[i].getStdout();
            String errRes = status[i].getStderr();
            List stdout = Arrays.asList(outRes.split("\n"));
            List stderr = Arrays.asList(errRes.split("\n"));
            exitWrapper[i] = new ExitStatus(escapeCommand(status[i].getArgv()),
                    status[i].getExitValue().intValue(), stdout, stderr);
        }

        return exitWrapper;
    }

    /**
     * Helper to modify the input command array to escape the special character
     * in order to be able to cut'n paste the returned command.
     *
     * @param  cmd  the original command
     *
     * @return  the same command correctly escaped
     */
    public static String[] escapeCommand(String cmd[]) {
        String newCmd[] = new String[cmd.length];

        for (int i = 0; i < cmd.length; i++) {

            if (cmd[i] != null) {
                char c[] = cmd[i].toCharArray();
                StringBuffer s = new StringBuffer();
                boolean escaped = false;

                for (int j = 0; j < c.length; j++) {

                    if ((c[j] == ';') || (c[j] == '&') || (c[j] == '(') ||
                            (c[j] == ')') || (c[j] == '|') || (c[j] == '^') ||
                            (c[j] == '<') || (c[j] == '>') || (c[j] == '"') ||
                            (c[j] == '`') || (c[j] == '\'') || (c[j] == ' ') ||
                            (c[j] == '\\') || (c[j] == '\t') ||
                            (c[j] == '\n')) {

                        s.append("\\");
                        escaped = true;
                    }

                    s.append(c[j]);
                }

                if (escaped) {
                    newCmd[i] = s.toString();
                } else {
                    newCmd[i] = cmd[i];
                }
            }
        }

        return newCmd;
    }
}
