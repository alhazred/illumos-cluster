/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)common.c	1.2	08/05/20 SMI"

#include <stdio.h>
#include <strings.h>
#include <sys/varargs.h>	/* va_start, */

#include "sensors.h"	/* ERROR_STRING_MAX */

#include "common.h"	/* ERR_PFX */

char error_str[ERROR_STRING_MAX];

#define	ERROR_STR_FMT_MAX 256
static char error_str_fmt[ERROR_STR_FMT_MAX];

void
set_error_str(const char *format, ...)
{
	va_list ap;

	if (ERROR_STR_FMT_MAX < strlen(ERR_PFX) + strlen(format)) {
		(void) snprintf(error_str, (size_t)ERROR_STRING_MAX,
			"ERROR_STR_FMT_MAX too small");
		return;
	}

	(void) strcpy(error_str_fmt, ERR_PFX);
	(void) strcat(error_str_fmt, format);

	/*lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, format);
	/*lint +e40 */
	(void) vsnprintf(error_str, (size_t)ERROR_STRING_MAX,
		error_str_fmt, ap);
	va_end(ap);
}
