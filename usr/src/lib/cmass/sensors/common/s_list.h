/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_S_LIST_H
#define	_S_LIST_H

#pragma ident	"@(#)s_list.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include "s_list_impl.h"

typedef struct s_list_node s_list_node_t;
typedef struct s_list s_list_t;

void s_list_create(s_list_t *, size_t, size_t);
void s_list_destroy(s_list_t *);

void s_list_insert_after(s_list_t *, void *, void *);
void s_list_insert_before(s_list_t *, void *, void *);
void s_list_insert_head(s_list_t *, void *);
void s_list_insert_tail(s_list_t *, void *);
void s_list_remove(s_list_t *, void *);
void s_list_move_tail(s_list_t *, s_list_t *);

void *s_list_head(s_list_t *);
void *s_list_tail(s_list_t *);
void *s_list_next(s_list_t *, void *);
void *s_list_prev(s_list_t *, void *);

#define	s_list_for_each(it, s_list)\
	for (\
		it = s_list_head((s_list));\
		it != NULL;\
		it = s_list_next((s_list), it))

#ifdef __cplusplus
}
#endif

#endif	/* _S_LIST_H */
