/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef __COMMON_H
#define	__COMMON_H

#pragma ident	"@(#)common.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>	/* strlen */

#include "sensors.h"	/* ERROR_STRING_MAX */
#include "sensor.h"	/* SENSOR_NAME */

#ifndef SENSOR_NAME
#error SENSOR_NAME not defined
#endif

#define	FILE_FUNC_LINE	__FILE__, __LINE__
#define	__BUG_FMT__	"Bug, contact Sun Microsystems, Inc. (%s:%d)"
#define	__BUG__		__BUG_FMT__, FILE_FUNC_LINE
#define	__SENSOR_BUG	"Internal " __BUG__
#define	__ULAYER_BUG	"Upper Layer" __BUG__

extern char error_str[ERROR_STRING_MAX];

#define	ERR_PFX "[" SENSOR_NAME "] "

extern void set_error_str(const char *, ...);
#define	unset_error_str() (error_str[0] = '\0')
#define	error_str_is_set() (strlen(error_str) > 0)
#define	get_error_str() error_str

#ifdef __cplusplus
}
#endif

#endif /* __COMMON_H */
