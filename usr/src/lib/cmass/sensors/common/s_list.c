/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)s_list.c	1.3	08/05/20 SMI"

/*
 * Generic doubly-linked list implementation
 *
 * Note this file comes from S10-71, with a few minor modifications
 * added.
 */

#include <sys/types.h>
#include <sys/sysmacros.h>
#include <sys/debug.h>

#include "s_list.h"
#include "s_list_impl.h"

#define	s_list_d2l(a, obj) ((s_list_node_t *)(((char *)obj) + (a)->list_offset))
#define	s_list_object(a, node) ((void *)(((char *)node) - (a)->list_offset))
#define	s_list_empty(a) ((a)->list_head.list_next == &(a)->list_head)

#define	s_list_insert_after_node(list, node, object) {	\
	s_list_node_t *lnew = s_list_d2l(list, object);	\
	lnew->list_prev = node;				\
	lnew->list_next = node->list_next;		\
	node->list_next->list_prev = lnew;		\
	node->list_next = lnew;				\
}

#define	s_list_insert_before_node(list, node, object) {	\
	s_list_node_t *lnew = s_list_d2l(list, object);	\
	lnew->list_next = node;				\
	lnew->list_prev = node->list_prev;		\
	node->list_prev->list_next = lnew;		\
	node->list_prev = lnew;				\
}

void
s_list_create(s_list_t *list, size_t size, size_t offset)
{
	ASSERT(list);
	ASSERT(size > 0);
	ASSERT(size >= offset + sizeof (s_list_node_t));

	list->list_size = size;
	list->list_offset = offset;
	list->list_head.list_next = list->list_head.list_prev =
	    &list->list_head;
}

void
s_list_destroy(s_list_t *list)
{
	s_list_node_t *node = &list->list_head;

	ASSERT(list);
	ASSERT(list->list_head.list_next == node);
	ASSERT(list->list_head.list_prev == node);

	node->list_next = node->list_prev = NULL;
}

void
s_list_insert_after(s_list_t *list, void *object, void *nobject)
{
	s_list_node_t *lold = s_list_d2l(list, object);
	s_list_insert_after_node(list, lold, nobject);
}

void
s_list_insert_before(s_list_t *list, void *object, void *nobject)
{
	s_list_node_t *lold = s_list_d2l(list, object);
	s_list_insert_before_node(list, lold, nobject)
}

void
s_list_insert_head(s_list_t *list, void *object)
{
	s_list_node_t *lold = &list->list_head;
	s_list_insert_after_node(list, lold, object);
}

void
s_list_insert_tail(s_list_t *list, void *object)
{
	s_list_node_t *lold = &list->list_head;
	s_list_insert_before_node(list, lold, object);
}

void
s_list_remove(s_list_t *list, void *object)
{
	s_list_node_t *lold = s_list_d2l(list, object);
	ASSERT(!s_list_empty(list));
	lold->list_prev->list_next = lold->list_next;
	lold->list_next->list_prev = lold->list_prev;
	lold->list_next = lold->list_prev = NULL;
}

void *
s_list_head(s_list_t *list)
{
	if (s_list_empty(list))
		return (NULL);
	return (s_list_object(list, list->list_head.list_next));
}

void *
s_list_tail(s_list_t *list)
{
	if (s_list_empty(list))
		return (NULL);
	return (s_list_object(list, list->list_head.list_prev));
}

void *
s_list_next(s_list_t *list, void *object)
{
	s_list_node_t *node = s_list_d2l(list, object);

	if (node->list_next != &list->list_head)
		return (s_list_object(list, node->list_next));

	return (NULL);
}

void *
s_list_prev(s_list_t *list, void *object)
{
	s_list_node_t *node = s_list_d2l(list, object);

	if (node->list_prev != &list->list_head)
		return (s_list_object(list, node->list_prev));

	return (NULL);
}

/*
 *  Insert src list after dst list. Empty src list thereafter.
 */
void
s_list_move_tail(s_list_t *dst, s_list_t *src)
{
	s_list_node_t *dstnode = &dst->list_head;
	s_list_node_t *srcnode = &src->list_head;

	ASSERT(dst->list_size == src->list_size);
	ASSERT(dst->list_offset == src->list_offset);

	if (s_list_empty(src))
		return;

	dstnode->list_prev->list_next = srcnode->list_next;
	srcnode->list_next->list_prev = dstnode->list_prev;
	dstnode->list_prev = srcnode->list_prev;
	srcnode->list_prev->list_next = dstnode;

	/* empty src list */
	srcnode->list_next = srcnode->list_prev = srcnode;
}
