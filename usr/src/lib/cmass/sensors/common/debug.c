/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)debug.c	1.2	08/05/20 SMI"

#include <stdio.h>
#include <strings.h>
#include <sys/varargs.h>

#include "debug.h"	/* DBG_PFX */

#define	DEBUG_FMT_MAX 256
static char debug_fmt[DEBUG_FMT_MAX];

void
_dprint(const char *format, ...)
{
	va_list ap;

	if (DEBUG_FMT_MAX < strlen(DBG_PFX) + strlen(format)) {
		(void) fprintf(stderr, "FMT_MAX too small");
		return;
	}

	(void) strcpy(debug_fmt, DBG_PFX);
	(void) strcat(debug_fmt, format);

	/*lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, format);
	/*lint +e40 */
	(void) vfprintf(stderr, debug_fmt, ap);
	va_end(ap);
}
