/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef __DEBUG_H
#define	__DEBUG_H

#pragma ident	"@(#)debug.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>	/* fprintf */

#include "sensor.h"	/* SENSOR_NAME, SENSOR_DEBUG */

#ifndef SENSOR_NAME
#error SENSOR_NAME not defined
#endif

#ifndef SENSOR_DEBUG /* SENSOR_DEBUG must be defined to 0 or 1 */
#error SENSOR_DEBUG not defined
#endif

/*
 * Level is set through variable _dbg_flgs, defined in every sensor.
 */

extern int _dbg_flgs;

enum debugFlags {
	DL1 = 0x01,
	DL2 = 0x02,
	DL3 = 0x04,
	DL4 = 0x08,
	DL5 = 0x10
};

#define	DBG_PFX	"["SENSOR_NAME"] "	/* Debug prefix			*/

/* Sun Cluster Makefile framework uses DEBUG for debug code generation */
#if SENSOR_DEBUG || defined(DEBUG)

#define	DBG_1	(DL1 | 0)
#define	DBG_2	(DL1 | DL2 | 0)
#define	DBG_3	(DL1 | DL2 | DL3 | 0)
#define	DBG_4	(DL1 | DL2 | DL3 | DL4 | 0)
#define	DBG_5	(DL1 | DL2 | DL3 | DL4 | DL5 | 0)

extern void _dprint(const char *, ...);

#define	DBG(lvl, args)\
	if ((lvl) & _dbg_flgs)\
		_dprint args;\

#else /* SENSOR_DEBUG || defined(DEBUG) */

#define	DBG_1	0
#define	DBG_2	0
#define	DBG_3	0
#define	DBG_4	0
#define	DBG_5	0

#define	DBG(lvl, args)

#endif /* SENSOR_DEBUG || defined(DEBUG) */

#ifdef __cplusplus
}
#endif

#endif /* __DEBUG_H */
