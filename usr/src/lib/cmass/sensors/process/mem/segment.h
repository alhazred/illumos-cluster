/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef __SEGMENT_H
#define	__SEGMENT_H

#pragma ident	"@(#)segment.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <procfs.h>	/* prxmap_t, ... */
#include <sys/types.h>	/* offset_t */
#include <sys/param.h>  /* MAXPATHLEN */

#include "s_list.h"	/* s_list_node_t, s_list_t */

struct segment {
	s_list_node_t node;		/* segment list hook */
	char name[MAXPATHLEN + 1];
	offset_t begin;
	offset_t end;
};

void	segment_list_create(s_list_t *);
void	segment_list_destroy(s_list_t *);
int	segment_get_name(prxmap_t *, psinfo_t *, pstatus_t *, char *);
int64_t	segment_get_rss(char *, offset_t, offset_t, s_list_t *);

#ifdef __cplusplus
}
#endif

#endif /* __SEGMENT_H */
