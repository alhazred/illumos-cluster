/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)memacct.c	1.2	08/05/20 SMI"

#include <procfs.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/param.h>  /* MAXPATHLEN */

#include "s_list.h"
#include "segment.h"

void
update_rss(const psinfo_t *info, uint64_t *rss)
{
	*rss += (uint64_t)info->pr_rssize;
}

/*
 * Returns -0 on success, -1 on non-fatal failure and -2 on fatal failure.
 */
int
update_rss_accurate(psinfo_t *info, pstatus_t *status,
	prxmap_t *xmap, s_list_t *segment_list, uint64_t *rss_accurate)
{
	int64_t bytes;
	int ret;
	int writable;
	int shared = -1;
	char name[MAXPATHLEN+1];

	/*
	 * (pr_rss == 0 && pr_hatpagesize != 0) does not
	 * mean anything. Should never occur.
	 *
	 * (pr_rss == 0 && pr_hatpagesize == 0) means this
	 * memory region is not in physical memory.
	 *
	 * (pr_rss != 0 && pr_hatpagesize != 0) means this
	 * memory region is in physical memory and current
	 * process has address translation mappings to it.
	 *
	 * (pr_rss != 0 && pr_hatpagesize == 0) means this
	 * memory region is in physical memory but current
	 * process does not have any address translation
	 * mappings to it.
	 */

	if (xmap->pr_rss == 0 || xmap->pr_hatpagesize == 0)
		return (0);

	/*
	 * If segment isn't writable we're certain it
	 * is shared.
	 */
	if (!(xmap->pr_mflags & MA_WRITE)) {
		writable = 0;
		shared = 1;

		/*
		 * At this point we could jump right
		 * away to the accounting code, we
		 * continue for verification purpose.
		 */
	} else {
		writable = 1;
	}

	/*
	 * Anonymous memory is private, unless it
	 * it is shared.
	 */
	if (xmap->pr_mflags & MA_ANON)
		shared = 0;

	if (xmap->pr_mflags & MA_SHARED)
		shared = 1;

	if (writable &&
		!(xmap->pr_mflags & MA_ANON) &&
		!(xmap->pr_mflags & MA_SHARED)) {

		/*
		 * This segment is writable but is
		 * neither anonymous nor shared. It
		 * should be a data segment.
		 */

		assert(xmap->pr_mflags & MA_READ);

		/*
		 * The following assertion triggers
		 * for mappings of java jsa files.
		 * E.g. poold.
		 */

		/* assert(xmap->pr_mflags & MA_EXEC); */

		/*
		 * We assume the copy-on-write
		 * operation occured and we therefore
		 * have our own copy of the segment.
		 */

		shared = 0;
	}

	/*
	 * If this assertion occurs it means some cases
	 * were not handled. This is a bug.
	 */
	assert(shared != -1);

	/*
	 * Accounting part.
	 */

	if (!shared) {
		uint64_t tmp = (uint64_t)0;
		uint_t pagesz_k = (uint_t)(xmap->pr_pagesize / 1024);

		tmp = (uint64_t)(xmap->pr_rss * pagesz_k);

		/* in KBytes */
		*rss_accurate += tmp;

		return (0);
	}

	/*
	 * segment is shared.
	 *
	 * Add it to shared segment list and account for
	 * the physical memory it consumes if it's not
	 * already in the list.
	 */

	assert(xmap->pr_offset >= 0);
	assert(xmap->pr_size > 0);

	if ((ret = segment_get_name(xmap, info, status, name)) < 0)
		/* segment_get_name returns -2 on fatal error */
		return (ret);

	if ((bytes = segment_get_rss(name,
		xmap->pr_offset,
		xmap->pr_offset + xmap->pr_size,
		segment_list)) < 0) {

		return (-1);
	}

	*rss_accurate += (uint64_t)(bytes / 1024); /* in KBytes */

	return (0);
}

void
update_vm(const psinfo_t *info, uint64_t *vm)
{
	*vm += (uint64_t)info->pr_size;
}

void
update_vm_zealous(const prxmap_t *xmap, uint64_t *vm_zealous)
{
	*vm_zealous += (uint64_t)(xmap->pr_size + 1023) / 1024; /* in KBytes */
}

/*
 * TODO: this function need be checked, in part. it may be that we currently
 * account multiple times for shared memory swap reservations.
 */
void
update_swap(const prxmap_t *xmap, uint64_t *swap)
{
	uint64_t val = 0; /* KBytes */

	/*
	 * xmap->pr_anon is the memory size for which (virtual) swap space is
	 * *allocated*.
	 */


	if (xmap->pr_mflags & MA_SHARED) {

		/*
		 * Shared Memory.
		 *
		 * If the segvn driver is used (non-ISM) then swap is always
		 * reserved upfront (MA_NORESERVE isn't set). If the segspt
		 * driver (ISM) is used swap is reserved upfront only for
		 * Dynamic ISM (DISM).
		 */

		if (!(xmap->pr_mflags & MA_NORESERVE)) {
			val = (uint64_t)(xmap->pr_size + 1023) / 1024;
		} else {
			uint_t pagesz_k = (uint_t)(xmap->pr_pagesize / 1024);

			val = (uint64_t)(xmap->pr_anon * pagesz_k);
		}

	} else if (xmap->pr_mflags & MA_NORESERVE) {
		uint_t pagesz_k = (uint_t)(xmap->pr_pagesize / 1024);

		val = (uint64_t)(xmap->pr_anon * pagesz_k);

	} else if ((xmap->pr_mflags & MA_WRITE) != 0) {

		val = (uint64_t)(xmap->pr_size + 1023) / 1024;
	}

	*swap += val;
}
