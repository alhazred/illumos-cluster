/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)segment.c	1.3	08/05/20 SMI"

#include <stdlib.h>	/* malloc */
#include <string.h>	/* memcmp, strerror */
#include <assert.h>	/* assert */
#include <errno.h>	/* errno */
#include <stddef.h>	/* offsetof */
#include <procfs.h>	/* psinfo_t, ... */
#include <libgen.h>	/* basename */
#include <sys/mkdev.h>	/* major */
#include <sys/types.h>	/* uint64_t, size_t */
#include <sys/param.h>  /* MAXPATHLEN */
#include <sys/stat.h>	/* stat, */

/* #include "debug.h" */ /* currently unused */
#include "common.h"
#include "proc_fs.h"
#include "s_list.h"
#include "segment.h"

#define	min(a, b) ((a) < (b) ? (a) : (b))
#define	max(a, b) ((a) > (b) ? (a) : (b))

#define	SEGLEN(seg) ((seg).end - (seg).begin)

static struct segment *
segment_zalloc(void)
{
	struct segment *new;

	new = (struct segment *)malloc(sizeof (struct segment));
	if (new == NULL) {
		/*lint -e746 */
		set_error_str("malloc failed (%s)", strerror(errno));
		/*lint +e746 */
		return (NULL);
	}

	(void) memset(new, 0, sizeof (struct segment));

	return (new);
}

static struct segment *
segment_create(char *name, offset_t begin, offset_t end)
{
	struct segment *new;

	if ((new = segment_zalloc()) == NULL)
		return (NULL);

	(void) strncpy(new->name, name, (size_t)MAXPATHLEN);
	new->begin = begin;
	new->end = end;

	return (new);
}

static void
segment_free(struct segment *seg)
{
	free(seg);
}

static void
segment_copy(struct segment *to, const struct segment *from)
{
	to->begin = from->begin;
	to->end = from->end;
}

static int
segment_included(const struct segment *seg1, const struct segment *seg2)
{
	if (seg1->begin >= seg2->begin && seg1->end <= seg2->end)
		return (1);
	return (0);
}

static int
segment_disjoint(const struct segment *seg1, const struct segment *seg2)
{
	if (seg1->begin >= seg2->end)
		return (1);

	if (seg2->begin >= seg1->end)
		return (1);


	return (0);
}

/*
 * Note: seg1 and seg2 must not be disjoint.
 */
static struct segment
__segment_union(const struct segment *seg1, const struct segment *seg2)
{
	struct segment u;

	u.begin = min(seg1->begin, seg2->begin);
	u.end = max(seg1->end, seg2->end);

	return (u);
}

/*
 * Note: seg1 and seg2 must not be disjoint.
 */
static struct segment
__segment_intersection(const struct segment *seg1, const struct segment *seg2)
{
	struct segment i;

	i.begin = max(seg1->begin, seg2->begin);
	i.end = min(seg1->end, seg2->end);

	return (i);
}

/*
 * segment_intersection isn't currently used.
 */
#if 0
static struct segment
segment_intersection(const struct segment *seg1, const struct segment *seg2)
{

	if (segment_disjoint(seg1, seg2)) {
		struct segment e;

		e.begin = 0;
		e.end = 0;

		return (e);
	}

	return (__segment_intersection(seg1, seg2));
}
#endif

static struct segment *
segment_position(char *name, s_list_t *list)
{
	struct segment *seg;

	s_list_for_each(seg, list)
		if (strncmp(seg->name, name, (size_t)MAXPATHLEN) == 0)
			return (seg);

	return (NULL); /* not in list */
}

void
segment_list_create(s_list_t *list)
{
	/*lint -e413 */
	s_list_create(list, sizeof (struct segment),
		offsetof(struct segment, node));
	/*lint +e413 */
}

void
segment_list_destroy(s_list_t *list)
{
	struct segment *seg = s_list_head(list);

	while (seg != NULL) {
		struct segment *next;

		next = s_list_next(list, seg);
		s_list_remove(list, seg);
		segment_free(seg);
		seg = next;
	}

	s_list_destroy(list);
}

/*
 * segment_get_name(xmap, info, status, buf) gets the name of the segment
 * associated to xmap, info and status and copies it into buf. 0 is returned on
 * success, -1 on non-fatal error and -2 on fatal error.
 */
int
segment_get_name(
	prxmap_t *xmap,
	psinfo_t *info,
	pstatus_t *status,
	char *buf)
{

	if (xmap->pr_mflags & MA_SHARED) {
		(void) strcpy(buf, "shm");

		if (xmap->pr_mflags & MA_ISM) {
			if (xmap->pr_mflags & MA_NORESERVE) {
				/* dynamic intimate shared memory (DISM) */
				(void) strncat(buf, ".P", (size_t)MAXPATHLEN);
			}
		}

		if (xmap->pr_shmid == -1) {
			/* not SysV shared memory */
			(void) strncat(buf, ".n", (size_t)MAXPATHLEN);
		}

		if (xmap->pr_mflags & MA_ISM) {
			/* created with SHM_SHARE_MMU flag */
			(void) strncat(buf, ".i", (size_t)MAXPATHLEN);
		}

		if (xmap->pr_shmid == -1) {

			/*
			 * Not SysV shared memory.
			 */

			if (xmap->pr_mflags & MA_ISM) {
				(void) strncat(buf, ".null",
					(size_t)MAXPATHLEN);
			} else {
				char tmp[64];
				char fname[MAXPATHLEN];
				struct stat statb;
				dev_t dev;
				ino_t ino;

				proc_fs_get_path_to_map(
				    (int)info->pr_pid,
				    (char *)xmap->pr_mapname,
				    fname, (size_t)MAXPATHLEN);

				if (stat(fname, &statb) < 0) {
					if (errno == ENOENT)
						return (-1);

					set_error_str("stat failed (%s)",
						strerror(errno));
					return (-2);
				}

				dev = statb.st_dev;
				ino = statb.st_ino;
				(void) snprintf(tmp, (size_t)MAXPATHLEN,
				    ".dev:%lu,%lu.ino:%lu",
				    (ulong_t)major(dev),
				    (ulong_t)minor(dev), ino);

				(void) strncat(buf, tmp, (size_t)MAXPATHLEN);
			}
		} else {

			/*
			 * SysV shared memory.
			 */

			char tmp[32];
			(void) sprintf(tmp, ".0x%x ", xmap->pr_shmid);
			(void) strncat(buf, tmp, (size_t)MAXPATHLEN);
		}

	} else if (!(xmap->pr_mflags & MA_ANON)) {
		if (strlen(xmap->pr_mapname) > 0) {
			if (strcmp(xmap->pr_mapname, "a.out") == 0) {
				(void) strncpy(buf, "bin.", (size_t)MAXPATHLEN);
				(void) strncat(
				    buf,
				    basename(info->pr_fname),
				    (size_t)MAXPATHLEN);
			} else {
				(void) strncpy(
				    buf,
				    xmap->pr_mapname,
				    (size_t)MAXPATHLEN);
			}
		} else {
			(void) strcpy(buf, "<unknown>");
		}
	} else {
		if (xmap->pr_vaddr + xmap->pr_size > status->pr_brkbase &&
		    xmap->pr_vaddr < status->pr_brkbase + status->pr_brksize) {

			(void) strcpy(buf, "heap");

		} else if (
			(xmap->pr_vaddr + xmap->pr_size > status->pr_stkbase) &&
			(xmap->pr_vaddr < status->pr_stkbase +
				status->pr_stksize)) {

			(void) strcpy(buf, "stack");

		} else {
			(void) strcpy(buf, "anon");
		}
	}

	return (0);
}

int64_t
segment_get_rss(
	char *name,
	offset_t begin,
	offset_t end,
	s_list_t *shared_segment_list)
{
	int64_t bytes;
	struct segment *seg;
	struct segment *pos;
	s_list_t *list = shared_segment_list;

	seg = segment_create(name, begin, end);
	if (seg == NULL)
		return ((int64_t)-1);

	if ((pos = segment_position(seg->name, list)) == NULL) {
		/*
		 * No seg named name in list, insert it.
		 */
		s_list_insert_tail(list, seg);
		bytes = (int64_t)SEGLEN(*seg);
		return (bytes);
	}

	assert(pos != NULL);

	/* add seg before pos */
	s_list_insert_before(list, pos, seg);
	bytes = (int64_t)SEGLEN(*seg);

	do {
		struct segment u, i;

		assert(pos != NULL);
		assert(strncmp(pos->name, seg->name, (size_t)MAXPATHLEN) == 0);

		if (segment_included(seg, pos)) {
			s_list_remove(list, seg);
			segment_free(seg);
			return ((int64_t)0);
		}

		if (segment_disjoint(seg, pos))
			continue;

		/*
		 * seg and pos overlap
		 */

		u = __segment_union(seg, pos);

		assert((SEGLEN(u) - SEGLEN(*pos)) > 0);

		i = __segment_intersection(seg, pos);

		assert(SEGLEN(i) > 0);

		bytes -= (int64_t)SEGLEN(i);

		assert(bytes >= 0);

		s_list_remove(list, seg);
		segment_free(seg);
		segment_copy(pos, &u);
		seg = pos;

	} while ((pos = s_list_next(list, pos)) != NULL &&
			(strncmp(pos->name, name, (size_t)MAXPATHLEN) == 0));

	return (bytes);
}
