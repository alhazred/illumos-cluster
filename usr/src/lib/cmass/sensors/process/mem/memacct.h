/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	__MEMACCT_H
#define	__MEMACCT_H

#pragma ident	"@(#)memacct.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <procfs.h>		/* psinfo_t, */
#include <sys/types.h>	/* uint64_t */

#include "s_list.h"		/* s_list_t */

extern void update_rss(const psinfo_t *, uint64_t *);
extern int update_rss_accurate(const psinfo_t *, const pstatus_t *,
	const prxmap_t *, s_list_t *, uint64_t *);
extern void update_vm(const psinfo_t *, uint64_t *);
extern void update_vm_zealous(const prxmap_t *, uint64_t *);
extern void update_swap(const prxmap_t *, uint64_t *);

#ifdef __cplusplus
}
#endif

#endif /* __MEMACCT_H */
