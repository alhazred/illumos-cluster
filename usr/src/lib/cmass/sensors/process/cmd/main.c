/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)main.c	1.7	08/05/20 SMI"

#include <stdio.h>	/* fgets, fopen */
#include <string.h>	/* strerror, ... */
#include <errno.h>	/* errno */
#include <unistd.h>	/* sleep */
#include <sys/types.h>	/* DIR, opendir */
#include <stdlib.h>	/* calloc, */
#include <assert.h>	/* assert */
#include <project.h>	/* PROJNAME_MAX, */
#include <errno.h>	/* errno */
#include <time.h>	/* clock_gettime */
#include <procfs.h>	/* psinfo_t, */
#include <stddef.h>	/* offsetof (demo) */
#include <utmp.h>	/* getutent */
#include <sys/stat.h>	/* stat, */
#if 0
#include <sys/unistd.h>		/* sysconf */
#endif
#include <sys/processor.h>	/* p_online */

#include "common.h"
#include "debug.h"
#include "proc_fs.h"
#include "s_proj.h"
#include "s_task.h"
#include "s_zone.h"
#include "s_list.h"
#include "segment.h"
#include "memacct.h"
#include "cpuacct.h"

#include "sensors.h"

/*
 * CPU and Memory Process Sensor
 *
 * This sensor gathers CPU and memory data for groups of projects. Here a group
 * of projects is either an arbitrary list of projects or a Zone.
 *
 * CPU values are integer values expressed in "Fraction of CPU x 100". For
 * example, if a given monitored object consumes two entire processors, 200 is
 * returned for that monitored object.
 *
 * Memory values are integer values expressed in KBytes.
 *
 * Note: the memory process sensor must be a command because it needs to be
 * 64-bit to read /proc/<pid>/xmap. The cpu process sensor need not be 64-bit.
 * However it's worth having the cpu and mem sensors bundled together as one
 * sensor not to walk through /proc multiples times.
 */

#define	ZONENAME_DELIMITER ":"
#define	PROJNAME_DELIMITER ","
#define	RESFIELD_DELIMITER ";"

/*
 * Warning: setting _dbg_flgs to DBG_X makes debug messages being printed to
 * stderr, be absolutely sure the sensor's user reads from the "stderr pipe"
 * otherwise the sensor will be eventually blocks on write()'s to stderr.
 */
int _dbg_flgs = /* DBG_1 */ 0;

struct managed_object {
	s_list_node_t node;
	char name[SENSOR_MNG_OBJ_MAX];
	char zonename[ZONENAME_MAX];
	s_list_t proj_list;
	int n_proj;
	s_list_t segment_list;
	uint64_t mem_rss;
	uint64_t mem_rss_accurate;
	uint64_t mem_vm;
	uint64_t mem_vm_zealous;
	uint64_t mem_swap;
	uint64_t cpu_time_kernel;
	uint64_t cpu_time_user;
	uint64_t cpu_time_total;
	uint64_t cpu_load_kernel;
	uint64_t cpu_load_user;
	uint64_t cpu_load_total;
	boolean_t mem_usage_updated;
	boolean_t cpu_usage_updated;
};

#define	mo_list_for_each(mo) s_list_for_each(mo, &mo_list)

/*
 * Note: struct cpu_usage is defined in s_task.h
 *
 * - task_running contains the aggregation of the CPU usage of tasks
 *   that are running at the time of the current measurement.
 * - task_prev_running contains the aggregation of the CPU usage of
 *   tasks that were running at the time of the previous measurement.
 * - task_just_died contains the aggregation of the CPU usage of tasks
 *   that died in the interval between current and previous measurement.
 */
struct proj_private {
	s_list_t task_list;
	boolean_t tasks_ran;	/* some tasks ran during last interval */
	boolean_t prev_updated; /* previously updated? */
	struct cpu_usage task_running;
	struct cpu_usage task_prev_running;
	struct cpu_usage task_just_died;
};

#define	PRIVSIZE sizeof (struct proj_private)
#define	PRIVDATA(proj) ((struct proj_private *)(proj->priv))

struct key_indicator {
	int id;
	char name[SENSOR_KI_TYPE_MAX];
	int (*start)(void);
	int (*stop)(void);
	int xmap; /* reading xmap is needed for that KI */
	int active;
};

/*
 * RSS_ACCURATE takes shared physical memory into account when
 * counting the physical memory a group of processes uses. RSS
 * does not take such care. RSS_ACCURATE need scan all pages of
 * processes, it is therefore much more time-consuming than RSS.
 */
#define	KI_NAME_MEM_USED		"ki.sunw.mem.used"
#define	KI_NAME_MEM_USED_ACCURATE	"ki.sunw.mem.used.accurate"
#define	KI_NAME_SWAP_USED		"ki.sunw.swap.used"
#define	KI_NAME_CPU_USED		"ki.sunw.cpu.used"

enum __ki_id {
	KI_ID_MEM_USED =		(1 << 0),
	KI_ID_MEM_USED_ACCURATE =	(1 << 1),
	KI_ID_SWAP_USED =		(1 << 2),
	KI_ID_CPU_USED =		(1 << 3)
};

#define	KI_ID_MEM_MASK \
	(KI_ID_MEM_USED | KI_ID_MEM_USED_ACCURATE | KI_ID_SWAP_USED)

static const char input_start_tag[] =		"__START__\n";
static const char input_stop_tag[] =		"__STOP__\n";
static const char input_getvalues_begin_tag[] =	"__GETVALUES__\n";
static const char input_getvalues_end_tag[] =	"__END__\n";
static const char output_error_tag[] =		"__ERROR__\n";

#if SENSOR_DEBUG
static const char logfilename[] = "/tmp/procsensor.logs";
static FILE *logfile = NULL;
#endif

static boolean_t do_cpu_accounting = B_FALSE;
static boolean_t first_cpu_measurement;
static int xmap_browsing = 0;
static int n_active_ki = 0;
static int n_mo = 0;
static timespec_t meastime;
static timespec_t prev_meastime;
static uint64_t prev_ftime_cand;
static uint64_t prev_ftime;
static s_list_t mo_list;

static const time_t exacct_cleanup_period = 24 * 60 * 60; /* 1 day */
static const char *exacct_cleanup_file =
	"/var/cluster/run/SCTelemetry.exacct_cleanup_activated";

static void start(void);
static void getvalues(void);
static void stop(void);
static int ki_cpu_start_handle(void);
static int ki_cpu_stop_handle(void);
static void send_report(void);
static void report_error(void);
static void report_success(void);
static char *read_one_line(void);
static char *stringify_result(const sensor_result_t *);
static int fill_results(sensor_result_t **);
static int fill_results_one_mo(const struct managed_object *,
	sensor_result_t *);
static int fill_one_result(const struct managed_object *,
	const struct key_indicator *, sensor_result_t *);
static int gather_cpu_usage(struct managed_object *);
static int accumulate_cpu_usage(const struct proj_entity *,
	struct managed_object *);
static boolean_t proj_cpu_usage_is_valid(const struct proj_entity *);
static struct managed_object *mo_zalloc(void);
static void mo_list_create(void);
static void mo_destroy(struct managed_object *);
static void mo_list_destroy(void);
static void mo_insert(struct managed_object *);
static int mo_list_is_empty(void);
static int mo_create_insert(char *);
static void time_copy_ts(timespec_t *, const timespec_t *);
static uint64_t time_tons_u64(uint64_t, uint64_t);
static int64_t time_tsto64(const timespec_t *);
static int64_t time_cmp_u64(uint64_t, uint64_t);
static void usage_add(struct cpu_usage *, const struct cpu_usage *);
static void usage_sub(struct cpu_usage *, const struct cpu_usage *);
static void op_fix(uint64_t *, uint64_t *);
static void proj_constructor(void *);
static void proj_destructor(void *);
static struct proj_entity *lookup_insert_proj(struct s_zone_entity *, projid_t);
static struct s_zone_entity *lookup_insert_zone(const char *, int *);
static int update_cpu_usage(const struct task_info *);
static int update_mem_usage(const psinfo_t *, const pstatus_t *,
	const prxmap_t *, int, struct managed_object *);
static int acct_running(void);
static int acct_logged(void);
static int do_accounting(void);
static void update_project_base(boolean_t);
#if 0
static time_t get_boot_time(void);
static unsigned int get_nr_cpus(void);
#endif

static struct key_indicator ki_tab[] = {
	{ KI_ID_MEM_USED,
		KI_NAME_MEM_USED,
		NULL, NULL, 0, 0 },
	{ KI_ID_MEM_USED_ACCURATE,
		KI_NAME_MEM_USED_ACCURATE,
		NULL, NULL, 1, 0 },
	{ KI_ID_SWAP_USED,
		KI_NAME_SWAP_USED,
		NULL, NULL, 1, 0 },
	{ KI_ID_CPU_USED,
		KI_NAME_CPU_USED,
		ki_cpu_start_handle, ki_cpu_stop_handle, 0, 0 }
};

#define	N_KI (sizeof (ki_tab) / sizeof (struct key_indicator))
#define	ki_for_each(i) for (i = 0; i < N_KI; i++)

/* ARGSUSED0, ARGSUSED1 */
int
main(int argc, char **argv)
{
	/*
	 * Event loop.
	 */

	for (;;) {
		char *line;

		DBG(DL1, ("waiting for event\n"));

		line = read_one_line();
		if (line == NULL) {
			report_error();
			return (1);
		}

		if (strcmp(line, input_start_tag) == 0) {
			/*
			 * We got a START command.
			 */
			free(line);
			start();

		} else if (strcmp(line, input_getvalues_begin_tag) == 0) {
			/*
			 * We got a GETVALUES command.
			 */
			free(line);
			getvalues();

		} else if (strcmp(line, input_stop_tag) == 0) {
			/*
			 * We got a STOP command.
			 */
			free(line);
			stop();

		} else {
			/*
			 * Unknown command.
			 */
			assert(line[strlen(line) - 1] == '\n');
			line[strlen(line) - 1] = '\0';

			DBG(DL1, ("command %s\\n unknown\n", line));
			free(line);

			continue;
		}

		send_report();
	}

	/* Never reached */
}

/*
 * Command functions.
 */
static void
start(void)
{
	unsigned int i;
	char *name;

	unset_error_str();

	/*
	 * Read KI name.
	 */
	name = read_one_line();
	if (name == NULL)
		return;

	/*
	 * Remove terminating '\n'
	 */
	assert(name[strlen(name) - 1] == '\n');
	name[strlen(name) - 1] = '\0';

	for (i = 0; i < N_KI; i++) {
		if (strncmp(name, ki_tab[i].name,
			strlen(ki_tab[i].name) + 1) == 0)
			break;
	}

	free(name);

	if (i >= N_KI) {
		set_error_str("no such KI");
		return;
	}

	if (ki_tab[i].active == 1) {
		/* KI is already enabled, return silently */
		return;
	}

#if SENSOR_DEBUG
	if ((logfile == NULL) &&
		((logfile = fopen(logfilename, "a+")) == NULL)) {

		/*lint -e746 */
		set_error_str("fopen failed (%s)", strerror(errno));
		/*lint +e746 */
		return;
	}
#endif
	if ((ki_tab[i].start != NULL) && (ki_tab[i].start() < 0)) {
		/* we don't bother closing logfile here */
		return;
	}

	ki_tab[i].active = 1;
	xmap_browsing += ki_tab[i].xmap;

	if (ki_tab[i].id == KI_ID_CPU_USED) {
		do_cpu_accounting = B_TRUE;
		first_cpu_measurement = B_TRUE;
	}

	if (n_active_ki++ == 0) {
		s_zone_list_create();
		DBG(DL1, ("up\n"));
	}
}

static void
getvalues(void)
{
	int i;
	long n_res;
	char *line;
	sensor_result_t *result_tab;

	unset_error_str();

	/* get current measurement time */
	if (clock_gettime(CLOCK_REALTIME, &meastime) < 0) {
		/*lint -e746 */
		set_error_str("clock_gettime failed (%s)", strerror(errno));
		/*lint +e746 */
		return;
	}

	if (n_active_ki == 0)
		goto end;

	if ((line = read_one_line()) == NULL)
		goto end;

	/* create list of managed objects */
	mo_list_create();

	while (strcmp(line, input_getvalues_end_tag) != 0) {

		assert(line[strlen(line) - 1] == '\n');
		line[strlen(line) - 1] = '\0';

		if (mo_create_insert(line) < 0) {
			free(line);
			mo_list_destroy();
			goto end;
		}

		free(line);

		/* read next line */
		line = read_one_line();
		if (line == NULL) {
			mo_list_destroy();
			goto end;
		}
	}

	free(line);

	if (mo_list_is_empty()) {
		mo_list_destroy();
		goto end;
	}

	prev_ftime_cand = (uint64_t)0;

	if (do_accounting() < 0) {
		mo_list_destroy();
		goto end;
	}

	if (do_cpu_accounting == B_TRUE)
		first_cpu_measurement = B_FALSE;

	if ((n_res = fill_results(&result_tab)) < 0) {
		mo_list_destroy();
		goto end;
	}

	for (i = 0; i < n_res; i++) {
		char *str;

		if ((str = stringify_result(&result_tab[i])) == NULL) {
			free(result_tab);
			mo_list_destroy();
			goto end;
		}

		(void) fprintf(stdout, "%s\n", str);
		free(str);
	}

	free(result_tab);
	mo_list_destroy();
end:
	update_project_base(do_cpu_accounting);
	time_copy_ts(&prev_meastime, &meastime);
	prev_ftime = prev_ftime_cand;
}

static void
stop(void)
{
	unsigned int i;
	char *name;

	unset_error_str();

	/*
	 * Read KI name.
	 */
	name = read_one_line();
	if (name == NULL)
		return;

	/*
	 * Remove terminating '\n'
	 */
	assert(name[strlen(name) - 1] == '\n');
	name[strlen(name) - 1] = '\0';

	for (i = 0; i < N_KI; i++) {
		if (strncmp(name, ki_tab[i].name,
			strlen(ki_tab[i].name) + 1) == 0)
			break;
	}

	free(name);

	if (i >= N_KI) {
		set_error_str("no such KI");
		return;
	}

	if (ki_tab[i].active == 0) {
		/* KI is already disabled, return silently */
		return;
	}

	if ((ki_tab[i].stop != NULL) && (ki_tab[i].stop() < 0))
		return;

	ki_tab[i].active = 0;
	xmap_browsing -= ki_tab[i].xmap;

	if (ki_tab[i].id == KI_ID_CPU_USED) /* disable CPU accounting */
		do_cpu_accounting = B_FALSE;

	if (--n_active_ki == 0) {
#if SENSOR_DEBUG
		if (logfile != NULL)
			(void) fclose(logfile);
#endif
		s_zone_list_destroy();
		DBG(DL1, ("down\n"));
	}
}

static int
ki_cpu_start_handle(void) {
	return (activate_task_acct(B_TRUE, NULL, (size_t)0));
}

static int
ki_cpu_stop_handle(void) {
	return (deactivate_task_acct());
}

/*
 * Functions for reading from stdin and writing to stdout.
 */

static void
send_report(void)
{
	if (error_str_is_set())
		report_error();
	else
		report_success();
}

/*
 * Converts error_str to a line and writes it to stdin.
 */
static void
report_error(void)
{
	assert(strlen(error_str) < ERROR_STRING_MAX);
	assert(error_str[strlen(error_str)] == '\0');

	if (!error_str_is_set()) {
		DBG(DL1, ("report_error called while error_str is unset\n"));
		return;
	}

	/*
	 * First, notify parent process an error string
	 * is coming.
	 */
	(void) fprintf(stdout, "%s", output_error_tag);
	(void) fflush(stdout);

	/*
	 * Write the error string.
	 */
	(void) fprintf(stdout, "%s\n", error_str);
	(void) fflush(stdout);

	unset_error_str();
}

static void
report_success(void)
{
	(void) fprintf(stdout, "\n");
	(void) fflush(stdout);
}

/*
 * Returns next line from stdin, NULL on error.
 *
 * '\0' is appended to returned line.
 */
static char *
read_one_line(void)
{
#define	CMDLINE_LENGTH	2

	char *str = NULL;
	size_t len = CMDLINE_LENGTH;
	size_t off = 0;

	for (;;) {
		char *tmp1;	/* used as temp var for realloc	*/
		char *tmp2;	/* used as temp var for gets	*/

		tmp1 = (char *)realloc(str, len);
		if (tmp1 == NULL) {
			set_error_str("realloc failed (%s)", strerror(errno));
			/*lint -e644 */
			free(str);
			/*lint +e644 */
			return (NULL);
		}
		str = tmp1;

		tmp1 += off;

		tmp2 = fgets(tmp1, CMDLINE_LENGTH, stdin);

		if (feof(stdin)) {
			set_error_str("Parent process closed pipe or died");
			free(str);
			return (NULL);
		}

		if (tmp2 == NULL) {
			assert(ferror(stdin));
			set_error_str("Bug (%s)", strerror(errno));
			free(str);
			return (NULL);
		}
		assert(tmp2 == tmp1);

		/*
		 * fgets returns once it has read len - 1 bytes,
		 * and it places a '\0' in the last byte.
		 */

		/*lint -e778 */
		if (strlen(tmp1) < (CMDLINE_LENGTH - 1) ||
			tmp1[CMDLINE_LENGTH - 2] == '\n')
			break;
		/*lint +e778 */

		len += CMDLINE_LENGTH;
		off += (CMDLINE_LENGTH - 1);
	}

	return (str);

#undef	CMDLINE_LENGTH
}

/*
 * Functions for filling the final result tab.
 */

static char *
stringify_result(const sensor_result_t *result)
{
#define	BUFSIZE 128
	unsigned int multiplier = 1;
	char *format;
	char *str;

	if (sizeof (result->usage) == sizeof (unsigned long long)) {
		format = "%ld%s%ld%s%s%s%s%s%llu%s%llu%s";
	} else if (sizeof (result->usage) == sizeof (unsigned long)) {
		format = "%ld%s%ld%s%s%s%s%s%lu%s%lu%s";
	} else {
		set_error_str(__SENSOR_BUG);
		return (NULL);
	}

	while (1) {
		int ret;
		size_t sz = multiplier * (size_t)BUFSIZE;

		if ((str = (char *)malloc(sz)) == NULL) {
			set_error_str("malloc failed (%s)", strerror(errno));
			return (NULL);
		}

		if ((ret = snprintf(str, sz, format,
			result->time_current.tv_sec, RESFIELD_DELIMITER,
			result->time_current.tv_nsec, RESFIELD_DELIMITER,
			result->ki_type, RESFIELD_DELIMITER,
			result->managed_object, RESFIELD_DELIMITER,
			result->usage, RESFIELD_DELIMITER,
			result->upperbound, RESFIELD_DELIMITER)) < 0) {

			set_error_str("snprintf failed (%s)", strerror(errno));
			return (NULL);
		}

		/* double cast makes lint happy */
		if ((size_t)(uint_t)ret <= sz)
			break;

		DBG(DL1, ("need to allocate more mem for result string\n"));

		free(str);
		multiplier++;
	}

	return (str);
#undef BUFSIZE
}

static int
fill_results(sensor_result_t **result_tab_p)
{
	size_t sz;
	int n_res, max_n_res;
	sensor_result_t *ptr;
	sensor_result_t *res_tab;
	struct managed_object *mo;

	max_n_res = n_active_ki * n_mo;
	/* double cast makes lint happy */
	sz = (size_t)(uint_t)max_n_res * sizeof (sensor_result_t);
	if ((res_tab = (sensor_result_t *)malloc(sz)) == NULL) {
		set_error_str("malloc failed (%s)", strerror(errno));
		return (-1);
	}

	n_res = 0;
	ptr = res_tab;
	mo_list_for_each(mo) {
		int n;

		if ((do_cpu_accounting == B_TRUE) &&
			((n = gather_cpu_usage(mo)) < -1)) {

			/* fatal error */
			free(res_tab);
			return (-1);
		}

		if ((n = fill_results_one_mo(mo, ptr)) < 0) {
			free(res_tab);
			return (-1);
		}
		n_res += n;
		ptr += n;
	}

	assert(n_res <= max_n_res);

	*result_tab_p = res_tab;
	return (n_res);
}

static int
fill_results_one_mo(const struct managed_object *mo, sensor_result_t *res)
{
	unsigned int i;
	int n_res = 0;
	sensor_result_t *ptr = res;

	ki_for_each(i) {

		if (!ki_tab[i].active)
			continue;

		if ((ki_tab[i].id & KI_ID_MEM_MASK) != 0) {
			/* memory KI */
			if (mo->mem_usage_updated == B_FALSE)
				continue;
		} else {
			/* cpu KI */
			if (mo->cpu_usage_updated == B_FALSE)
				continue;
		}

		/* fails only on a sensor bug */
		if (fill_one_result(mo, &ki_tab[i], ptr) < 0)
			return (-1);

		n_res++;
		ptr++;
	}

	return (n_res);
}

static int
fill_one_result(
	const struct managed_object *mo,
	const struct key_indicator *ki,
	sensor_result_t *res)
{
	switch (ki->id) {
	case KI_ID_MEM_USED:
		res->usage = mo->mem_rss;
		break;
	case KI_ID_MEM_USED_ACCURATE:
		res->usage = mo->mem_rss_accurate;
		break;
	case KI_ID_SWAP_USED:
		res->usage = mo->mem_swap;
		break;
	case KI_ID_CPU_USED:
		res->usage = mo->cpu_load_total;
		break;
	default:
		set_error_str(__SENSOR_BUG);
		return (-1);
	}

	res->upperbound = (uint64_t)0;

	(void) strcpy(res->ki_type, ki->name);
	(void) strcpy(res->managed_object, mo->name);
	(void) memcpy(&res->time_current, &meastime, sizeof (timespec_t));

	return (0);
}

/*
 * gather_cpu_usage(mo) gathers the cpu usage data associated to the managed
 * object mo. 0 is returned on success, -1 on non-fatal error, and -2 on fatal
 * error.
 */
static int
gather_cpu_usage(struct managed_object *mo)
{
	int retval;
	struct s_zone_entity *zone;

	if ((zone = s_zone_lookup(mo->zonename)) == NULL)
		return (0);

	if (mo->n_proj == 0) { /* for the entire zone */
		int i;
		struct proj_entity *it;

		s_zone_for_each_proj(i, it, zone) {
			if ((retval = accumulate_cpu_usage(it, mo)) < 0)
				return (retval);
		}

	} else {
		struct proj_entity *it;

		s_list_for_each(it, &mo->proj_list) {
			struct proj_entity *p;

			if ((p = s_zone_lookup_proj_byname(zone, it->projname))
				== NULL) {

				continue;
			}
			if ((retval = accumulate_cpu_usage(p, mo)) < 0)
				return (retval);
		}
	}

	return (0);
}

/*
 * accumulate_cpu_usage(proj, mo) updates the cpu usage associated to the
 * managed object mo based on the cpu usage of project proj. 0 is returned on
 * success, -1 on non-fatal error, and -2 on fatal error.
 */
static int
accumulate_cpu_usage(const struct proj_entity *proj, struct managed_object *mo)
{
	uint64_t sys;
	uint64_t usr;
	struct cpu_usage *r;	/* running */
	struct cpu_usage *pr;	/* prev running */
	struct cpu_usage *jd;	/* just died */
	int64_t time_interval;

	if (PRIVDATA(proj)->prev_updated == B_FALSE)
		return (-1);

	if (proj_cpu_usage_is_valid(proj) == B_FALSE) {

		/*
		 * This project's cpu usage data isn't valid. This unfortunately
		 * happens sometimes, most probably because of a bug in Solaris.
		 */

#if SENSOR_DEBUG
		(void) fprintf(logfile, "%s", ctime(&meastime.tv_sec));
		(void) fprintf(logfile, "%s", ctime(&prev_meastime.tv_sec));

		(void) fprintf(logfile, "reread extacct, proj: %d\n",
			(int)proj->projid);

		/* TODO remove */
		(void) fprintf(logfile, "sys sec: %llu, %llu, %llu\n",
			PRIVDATA(proj)->task_running.cpu_sys_secs,
			PRIVDATA(proj)->task_just_died.cpu_sys_secs,
			PRIVDATA(proj)->task_prev_running.cpu_sys_secs);

		(void) fprintf(logfile, "sys nsec: %llu, %llu, %llu\n",
			PRIVDATA(proj)->task_running.cpu_sys_nsecs,
			PRIVDATA(proj)->task_just_died.cpu_sys_nsecs,
			PRIVDATA(proj)->task_prev_running.cpu_sys_nsecs);

		(void) fprintf(logfile, "usr sec: %llu, %llu, %llu\n",
			PRIVDATA(proj)->task_running.cpu_usr_secs,
			PRIVDATA(proj)->task_just_died.cpu_usr_secs,
			PRIVDATA(proj)->task_prev_running.cpu_usr_secs);

		(void) fprintf(logfile, "usr nsec: %llu, %llu, %llu\n",
			PRIVDATA(proj)->task_running.cpu_usr_nsecs,
			PRIVDATA(proj)->task_just_died.cpu_usr_nsecs,
			PRIVDATA(proj)->task_prev_running.cpu_usr_nsecs);

		(void) fflush(logfile);

		set_error_str(__SENSOR_BUG);
#endif
		return (-1);
	}

	r = &PRIVDATA(proj)->task_running;
	pr = &PRIVDATA(proj)->task_prev_running;
	jd = &PRIVDATA(proj)->task_just_died;

	sys = (r->cpu_sys_secs * NANOSEC + r->cpu_sys_nsecs) +
		(jd->cpu_sys_secs * NANOSEC + jd->cpu_sys_nsecs) -
		(pr->cpu_sys_secs * NANOSEC + pr->cpu_sys_nsecs);

	usr = (r->cpu_usr_secs * NANOSEC + r->cpu_usr_nsecs) +
		(jd->cpu_usr_secs * NANOSEC + jd->cpu_usr_nsecs) -
		(pr->cpu_usr_secs * NANOSEC + pr->cpu_usr_nsecs);

	mo->cpu_time_kernel += sys;
	mo->cpu_time_user += usr;
	mo->cpu_time_total += (sys + usr);


	/* compute the time interval between prev and current measurement */
	time_interval = time_tsto64(&meastime) - time_tsto64(&prev_meastime);

	if (time_interval > (int64_t)0) {
		uint64_t sys_load = (sys * 100) / (uint64_t)time_interval;
		uint64_t usr_load = (usr * 100) / (uint64_t)time_interval;

		mo->cpu_load_kernel += sys_load;
		mo->cpu_load_user += usr_load;
		mo->cpu_load_total += (sys_load + usr_load);
	} else {
		return (-1);
	}

	if (mo->cpu_usage_updated == B_FALSE &&
		PRIVDATA(proj)->tasks_ran == B_TRUE) {

		mo->cpu_usage_updated = B_TRUE;
	}

	return (0);
}

/*
 * proj_cpu_usage_is_valid(proj) returns B_FALSE if the cpu usage associated to
 * project proj isn't valid, which corresponds to the case where cpu usage of
 * the currently running tasks plus of that of the tasks that died in the last
 * interval is lower that the cpu usage of the tasks that were running at the
 * time of the previous measurement. B_TRUE is returned otherwise.
 */
static boolean_t
proj_cpu_usage_is_valid(const struct proj_entity *proj)
{
	struct cpu_usage *r;	/* running */
	struct cpu_usage *pr;	/* prev running */
	struct cpu_usage *jd;	/* just died */

	r = &PRIVDATA(proj)->task_running;
	pr = &PRIVDATA(proj)->task_prev_running;
	jd = &PRIVDATA(proj)->task_just_died;

	if ((r->cpu_sys_secs + jd->cpu_sys_secs < pr->cpu_sys_secs) ||
		((r->cpu_sys_secs + jd->cpu_sys_secs == pr->cpu_sys_secs) &&
		(r->cpu_sys_nsecs + jd->cpu_sys_nsecs < pr->cpu_sys_nsecs))) {

		return (B_FALSE);
	}

	if ((r->cpu_usr_secs + jd->cpu_usr_secs < pr->cpu_usr_secs) ||
		((r->cpu_usr_secs + jd->cpu_usr_secs == pr->cpu_usr_secs) &&
		(r->cpu_usr_nsecs + jd->cpu_usr_nsecs < pr->cpu_usr_nsecs))) {

		return (B_FALSE);
	}

	return (B_TRUE);
}

/*
 * Managed object-related functions.
 */

static struct managed_object *
mo_zalloc(void)
{
	struct managed_object *mo;

	if ((mo = (struct managed_object *)malloc(sizeof (*mo))) == NULL) {
		set_error_str("malloc failed (%s)", strerror(errno));
		return (NULL);
	}

	(void) memset(mo, 0, sizeof (*mo));

	return (mo);
}

static void
mo_list_create(void)
{
	/*lint -e413 */
	s_list_create(&mo_list, sizeof (struct managed_object),
		offsetof(struct managed_object, node));
	/*lint +e413 */
}

static void
mo_destroy(struct managed_object *mo)
{
	DBG(DL2, ("deleting MO %s...\n", mo->name));

	segment_list_destroy(&mo->segment_list);
	proj_list_destroy(&mo->proj_list);
	free(mo);
}

static void
mo_list_destroy(void)
{
	struct managed_object *mo = s_list_head(&mo_list);

	while (mo != NULL) {
		struct managed_object *n = s_list_next(&mo_list, mo);

		s_list_remove(&mo_list, mo);
		mo_destroy(mo);
		mo = n;
		n_mo--;
	}

	assert(n_mo == 0);

	s_list_destroy(&mo_list);
}

static void
mo_insert(struct managed_object *mo)
{
	s_list_insert_tail(&mo_list, mo);
	n_mo++;
}

static int
mo_list_is_empty(void)
{
	return (s_list_head(&mo_list) == NULL ? 1 : 0);
}

static int
mo_create_insert(char *string)
{
	char *ptr;
	char *last;
	char *delim;
	id_t zone_id;
	char *zoneroot;
	char *zonename;
	char *projname;
	struct managed_object *mo;

	if (string == NULL) {
		set_error_str(__SENSOR_BUG);
		return (-1);
	}

	DBG(DL2, ("creating MO %s...\n", string));

	if ((mo = mo_zalloc()) == NULL)
		return (-1);

	(void) strncpy(mo->name, string, (size_t)SENSOR_MNG_OBJ_MAX);
	segment_list_create(&mo->segment_list);
	proj_list_create(&mo->proj_list);
	mo->mem_usage_updated = B_FALSE;
	mo->cpu_usage_updated = B_FALSE;

	if (((delim = strstr(string, ZONENAME_DELIMITER)) == NULL) ||
		(delim == string)) {
		static const char *gz = GLOBAL_ZONENAME;

		if (delim == string)
			string++;

		zonename = (char *)gz;
		ptr = string;
	} else {
		zonename = strtok_r(string, ZONENAME_DELIMITER, &last);
		ptr = NULL;
	}
	(void) strncpy(mo->zonename, zonename, (size_t)ZONENAME_MAX);

	if ((zone_id = s_zone_getid(zonename)) < 0) {
		/*
		 * We unset the error string set by s_zone_getid() here because
		 * we're about to return silently.
		 */
		unset_error_str();
		mo_destroy(mo);
		return (0); /* return silently */
	}

	if ((zoneroot = s_zone_getroot(zone_id)) == NULL) {
		mo_destroy(mo);
		return (1);
	}

	while ((projname = strtok_r(ptr, PROJNAME_DELIMITER, &last)) != NULL) {
		projid_t projid;
		struct proj_entity *proj;

		ptr = NULL; /* for next round */

		if (proj_lookup_byname(&mo->proj_list, projname) != NULL)
			continue;

		if ((projid = s_zone_getprojidbyname(zoneroot, projname))
			< 0) {

			free(zoneroot);
			mo_destroy(mo);
			return ((projid < -1) ? -1 : 0);
		}

		if ((proj = proj_create(projid, (size_t)0, NULL, NULL))
			== NULL) {

			free(zoneroot);
			mo_destroy(mo);
			return (-1);
		}
		(void) strncpy(proj->projname, projname, (size_t)PROJNAME_MAX);

		proj_insert(&mo->proj_list, proj);
		mo->n_proj++;
	}

	free(zoneroot);
	mo_insert(mo);
	return (0);
}

/*
 * Time-related helper functions.
 */

static void
time_copy_ts(timespec_t *t1, const timespec_t *t2)
{
	t1->tv_sec = t2->tv_sec;
	t1->tv_nsec = t2->tv_nsec;
}

static uint64_t
time_tons_u64(uint64_t sec, uint64_t nsec)
{
	return (sec * (uint64_t)NANOSEC + nsec);
}

static int64_t
time_tsto64(const timespec_t *t)
{
	return ((int64_t)t->tv_sec * NANOSEC +
		(int64_t)t->tv_nsec);
}

static int64_t
time_cmp_u64(uint64_t t1, uint64_t t2)
{
	return ((int64_t)(t1 - t2));
}

/*
 * Helper functions for adding and subtracting CPU
 * usages.
 */

static void
usage_add(struct cpu_usage *u1, const struct cpu_usage *u2)
{
	u1->cpu_sys_secs += u2->cpu_sys_secs;
	u1->cpu_sys_nsecs += u2->cpu_sys_nsecs;
	op_fix(&u1->cpu_sys_secs, &u1->cpu_sys_nsecs);

	u1->cpu_usr_secs += u2->cpu_usr_secs;
	u1->cpu_usr_nsecs += u2->cpu_usr_nsecs;
	op_fix(&u1->cpu_usr_secs, &u1->cpu_usr_nsecs);
}

static void
usage_sub(struct cpu_usage *u1, const struct cpu_usage *u2)
{
	u1->cpu_sys_secs -= u2->cpu_sys_secs;
	u1->cpu_sys_nsecs -= u2->cpu_sys_nsecs;
	op_fix(&u1->cpu_sys_secs, &u1->cpu_sys_nsecs);

	u1->cpu_usr_secs -= u2->cpu_usr_secs;
	u1->cpu_usr_nsecs -= u2->cpu_usr_nsecs;
	op_fix(&u1->cpu_usr_secs, &u1->cpu_usr_nsecs);
}

static void
op_fix(uint64_t *sec, uint64_t *nsec)
{
	if ((int64_t)(*nsec) < (int64_t)0) {
		(*sec)--;
		(*nsec) += NANOSEC;
	} else {
		if ((*nsec) >= NANOSEC) {
			(*sec)++;
			(*nsec) -= NANOSEC;
		}
	}
}

/*
 * Project constructor and destructor for the projects
 * in the project base.
 */

static void
proj_constructor(void *data)
{
	struct proj_private *priv = (struct proj_private *)data;

	priv->tasks_ran = B_FALSE;
	priv->prev_updated = B_FALSE;
	task_list_create(&priv->task_list);
}

static void
proj_destructor(void *data)
{
	struct proj_private *priv = (struct proj_private *)data;

	task_list_destroy(&priv->task_list);
}

/*
 * Accounting functions.
 */

static struct proj_entity *
lookup_insert_proj(struct s_zone_entity *zone, projid_t projid)
{
	struct proj_entity *proj;

	if ((proj = s_zone_lookup_proj_byid(zone, projid)) != NULL)
		return (proj);

	if ((proj = proj_create(projid, PRIVSIZE, proj_constructor,
		proj_destructor)) == NULL)
		return (NULL);

	if (s_zone_insert_proj(zone, proj) < 0) {
		proj_destroy(proj);
		return (NULL);
	}

	return (proj);
}

static struct s_zone_entity *
lookup_insert_zone(const char *zonename, int *new)
{
	struct s_zone_entity *zone;

	if ((zone = s_zone_lookup(zonename)) != NULL)
		return (zone);

	if ((zone = s_zone_create(zonename)) == NULL)
		return (NULL);

	*new = 1;
	s_zone_insert(zone);

	return (zone);
}

static int
update_cpu_usage(const struct task_info *ti)
{
	struct s_zone_entity *zone;
	struct proj_entity *proj;
	struct task_entity *task;
	struct proj_private *pp;
	int newzone = 0;

	if ((ti->state & FINISHED) != 0) {
		uint64_t ftime; /* task finish time (ns) */

		ftime = time_tons_u64(ti->finish_secs, ti->finish_nsecs);

		if ((ti->state & FIRST) != 0)
			prev_ftime_cand = ftime;

		/*
		 * If ki.sunw.cpu.used just got enabled, first_cpu_measurement
		 * is set to B_TRUE, ensuring that we don't read the extended
		 * accounting log the first time.
		 */

		if ((first_cpu_measurement == B_TRUE) ||
			(time_cmp_u64(prev_ftime, ftime) >= (int64_t)0))
			return (1);
	}

	if ((zone = lookup_insert_zone(ti->zonename, &newzone)) == NULL)
		return (-1);

	if ((proj = lookup_insert_proj(zone, ti->projid)) == NULL) {
		if (newzone)
			s_zone_destroy(zone);
		return (-1);
	}

	pp = PRIVDATA(proj);

	/* mark that at least one task ran during the last sampling interval */
	pp->tasks_ran = B_TRUE;

	if ((task = task_lookup_byid(&pp->task_list, ti->taskid)) == NULL) {
		if ((task = task_create(ti)) == NULL) {
			if (newzone)
				s_zone_destroy(zone);
			return (-1);
		}
		task_insert(&pp->task_list, task);
	} else {
		const struct cpu_usage *prev, *curr;
		boolean_t flag = B_TRUE;

		/*
		 * Task usage already counted.
		 *
		 * Let's do the update. Instead we could check if we've already
		 * counted the usage of that task before doing the accounting
		 * for that task.
		 *
		 * Note: given that EXD_GROUP_TASK_PARTIAL|INTERVAL tasks aren't
		 * read (see comment in cpuacct.c) a task should never have more
		 * than one exacct log entry.
		 */

		if ((task->info.state & FINISHED) != 0) {
			set_error_str(__SENSOR_BUG);
			return (-1);
		}

		prev = &task->info.usage;
		curr = &ti->usage;

		if ((prev->cpu_sys_secs > curr->cpu_sys_secs) ||
			((prev->cpu_sys_secs == curr->cpu_sys_secs) &&
			(prev->cpu_sys_nsecs > curr->cpu_sys_nsecs))) {

			/*
			 * We're about to updating with a value that doesn't
			 * look as fresh as the one we already have! This looks
			 * like a Solaris bug. Let's not do the update.
			 */

			flag = B_FALSE;
#if SENSOR_DEBUG
			(void) fprintf(logfile, "%s", ctime(&meastime.tv_sec));
			(void) fprintf(logfile, "%s",
				ctime(&prev_meastime.tv_sec));

			(void) fprintf(logfile, "BUG sys, taskid: %u, "
				"projid: %u\n",
				(unsigned)task->info.taskid,
				(unsigned)task->info.projid);

			(void) fprintf(logfile, "%llu %llu\n",
				prev->cpu_sys_secs, curr->cpu_sys_secs);

			(void) fprintf(logfile, "%llu %llu\n",
				prev->cpu_sys_nsecs, curr->cpu_sys_nsecs);

			(void) fflush(logfile);
#endif
		}

		if ((prev->cpu_usr_secs > curr->cpu_usr_secs) ||
			((prev->cpu_usr_secs == curr->cpu_usr_secs) &&
			(prev->cpu_usr_nsecs > curr->cpu_usr_nsecs))) {

			/*
			 * We're about to updating with a value that doesn't
			 * look as fresh as the one we already have! This looks
			 * like a Solaris bug. Let's not do the update.
			 */

			flag = B_FALSE;
#if SENSOR_DEBUG
			(void) fprintf(logfile, "%s", ctime(&meastime.tv_sec));
			(void) fprintf(logfile, "%s",
				ctime(&prev_meastime.tv_sec));

			(void) fprintf(logfile, "BUG usr taskid: %u, "
				"projid: %u\n",
				(unsigned)task->info.taskid,
				(unsigned)task->info.projid);

			(void) fprintf(logfile, "%llu %llu\n",
				prev->cpu_usr_secs, curr->cpu_usr_secs);

			(void) fprintf(logfile, "%llu %llu\n",
				prev->cpu_usr_nsecs, curr->cpu_usr_nsecs);

			(void) fflush(logfile);
#endif
		}

		if (flag == B_TRUE) {
			usage_sub(&pp->task_running, &task->info.usage);
			(void) memcpy(&task->info, ti, sizeof (task->info));
		}
	}

	if ((ti->state & RUNNING) != 0) {
		usage_add(&pp->task_running, &task->info.usage);
	} else if ((ti->state & FINISHED) != 0) {
		usage_add(&pp->task_just_died, &task->info.usage);
	} else {
		set_error_str(__SENSOR_BUG);
		if (newzone)
			s_zone_destroy(zone);
		return (-1);
	}

	return (0);
}

/*
 * Returns 0 on success, -1 on non-fatal error, and -2 on fatal error.
 */
static int
update_mem_usage(const psinfo_t *psinfo,
	const pstatus_t *pstatus,
	const prxmap_t *prxmap,
	int prxmap_size,
	struct managed_object *mo)
{
	int i;

	/*
	 * This mo's mem usage will be updated, or mo will be destroyed in case
	 * of fatal error
	 */
	mo->mem_usage_updated = B_TRUE;

	update_rss(psinfo, &mo->mem_rss);
	update_vm(psinfo, &mo->mem_vm);

	if (prxmap == NULL) /* done */
		return (0);

	for (i = 0; i < prxmap_size; i++) {
		int ret;
		const prxmap_t *xmap = &prxmap[i];

		if ((ret = update_rss_accurate(psinfo, pstatus, xmap,
			&mo->segment_list, &mo->mem_rss_accurate)) < -1) {

			return (ret);
		}

		update_vm_zealous(xmap, &mo->mem_vm_zealous);
		update_swap(xmap, &mo->mem_swap);
	}

	return (0);
}

static int
acct_running(void)
{
	DIR *dir;
	pid_t pid;

	dir = proc_fs_open();
	if (dir == NULL)
		return (-1);

	while (proc_fs_read_one(dir, &pid) >= 0) {
		int ret;
		int xmap_size;
		prxmap_t *xmap;
		psinfo_t psinfo;
		pstatus_t pstatus;
		struct managed_object *mo;
		struct task_info taskinfo;
		char zonename[ZONENAME_MAX];

		assert(pid != NOPID);

		/* get info from process */
		(void) memset(&psinfo, 0, sizeof (psinfo_t));
		if ((ret = proc_fs_get_info(pid, &psinfo)) < -1) {
			proc_fs_close(dir);
			return (-1);
		}
		if (ret < 0)
			continue;

		/* skip zombie */
		if (psinfo.pr_nlwp == 0)
			continue;

		(void) memset(&pstatus, 0, sizeof (pstatus_t));
		if ((ret = proc_fs_get_status(pid, &pstatus)) < -1) {
			proc_fs_close(dir);
			return (-1);
		}
		if (ret < 0)
			continue;

		xmap = NULL; /* for non-conditioned free(xmap) */
		xmap_size = 0;

		if (xmap_browsing) {
			if ((ret = proc_fs_get_xmap(pid, &xmap)) < -1) {
				proc_fs_close(dir);
				return (-1);
			}
			if (ret < 0)
				continue;
			xmap_size = ret;
		}

		/*
		 * Memory accounting
		 */

		(void) memset(zonename, 0, sizeof (zonename));
		if (s_zone_getname(&psinfo, zonename, (size_t)ZONENAME_MAX)
			< 0) {

			free(xmap);
			proc_fs_close(dir);
			return (-1);
		}

		mo_list_for_each(mo) {
			if (strcmp(mo->zonename, zonename) != 0)
				continue;

			if ((mo->n_proj != 0) &&
				(proj_lookup_byid(&mo->proj_list,
					psinfo.pr_projid) == NULL))
				continue;

			DBG(DL5, ("mem usage update, pid: %d, projid: %d, "
				"zone: %s\n",
				pid, psinfo.pr_projid, zonename));

			if ((ret = update_mem_usage(&psinfo, &pstatus, xmap,
				xmap_size, mo)) < -1) {

				free(xmap);
				proc_fs_close(dir);
				return (-1);
			}
			if (ret < 0)
				goto next;
		}

		/*
		 * CPU accounting
		 */

		if (do_cpu_accounting == B_TRUE) {

			(void) memset(&taskinfo, 0, sizeof (taskinfo));
			taskinfo.state = RUNNING;
			if (get_task_info(psinfo.pr_taskid, &taskinfo) < 0) {
				free(xmap);
				proc_fs_close(dir);
				return (-1);
			}

			if ((ret = update_cpu_usage(&taskinfo)) < 0) {
				free(xmap);
				proc_fs_close(dir);
				return (-1);
			} else if (ret == 1) {
				set_error_str(__SENSOR_BUG);
				free(xmap);
				proc_fs_close(dir);
				return (-1);
			}
		}
	next:
		free(xmap);
	}

	proc_fs_close(dir);

	return (error_str_is_set() ? -1 : 0);
}

static int
acct_logged(void)
{
	struct stat statb;
	boolean_t cleanup = B_TRUE;

	if (do_cpu_accounting == B_FALSE)
		return (0);

	if (stat(exacct_cleanup_file, &statb) < 0) {

		if (errno != ENOENT) {
			set_error_str("stat failed (%s)", strerror(errno));
			return (-1);
		}
		/* file does not exist, don't do exacct cleanup */
		cleanup = B_FALSE;
	}

	return (get_tasks_info_from_log(cleanup, meastime.tv_sec,
		exacct_cleanup_period, update_cpu_usage));
}

static int
do_accounting(void)
{
	if (acct_running() < 0)
		return (-1);

	if (acct_logged() < 0)
		return (-1);

	return (0);
}

static void
update_project_base(boolean_t cpu_accounting_done)
{
	struct s_zone_entity *zone;

	s_zone_for_each(zone) {
		int i;
		struct proj_entity *proj;

		s_zone_for_each_proj(i, proj, zone) {
			struct proj_private *p = PRIVDATA(proj);
			size_t size = sizeof (struct cpu_usage);

			/* get prepared for next measurement round */
			p->tasks_ran = B_FALSE;
			p->prev_updated = cpu_accounting_done;
			task_list_empty(&p->task_list);
			(void) memcpy(&p->task_prev_running, &p->task_running,
				size);
			(void) memset(&p->task_running, 0, size);
			(void) memset(&p->task_just_died, 0, size);
		}
	}
}

#if 0
static time_t
get_boot_time(void)
{
	struct utmp *utent;
	time_t boot_time = 0;

	setutent();
	while ((utent = getutent()) != NULL) {
		if (utent->ut_type == BOOT_TIME) {
			boot_time = utent->ut_time;
			break;
		}
	}
	endutent();

	if (boot_time == 0)
		set_error_str("cannot get boot time (%s)", strerror(errno));

	return (boot_time);
}

static unsigned int
get_nr_cpus(void)
{
	unsigned int cnt = 0;
	processorid_t i, cpuid_max;

	cpuid_max = sysconf(_SC_CPUID_MAX);
	for (i = 0; i <= cpuid_max; i++)
		if (p_online(i, P_STATUS) != -1)
			cnt++;

	return (cnt);
}
#endif
