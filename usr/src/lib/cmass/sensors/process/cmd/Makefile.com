#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.com	1.4	08/05/20 SMI"
#
# lib/cmass/sensors/process/cmd/Makefile.com
#

# As opposed to the other sensors the memory sensor cannot be
# a library linked to the Cacao JVM. So the memory sensor is
# a command. However we keep the mem sensor code under the
# lib/cmass directory where all the sensors reside.



PROG = procsensor

# INSDIR used by arch-specific Makefiles
# FIXME: should not be hardcoded as such
INSTDIR=$(VROOT)/usr/cluster/lib/cmass/sensors

OBJS =	objs/main.o \
	objs/mem/segment.o \
	objs/mem/memacct.o\
	objs/cpu/s_task.o \
	objs/cpu/cpuacct.o \
	objs/s_common/s_list.o \
	objs/s_common/common.o \
	objs/s_common/debug.o \
	objs/p_common/s_proj.o \
	objs/p_common/s_zone.o \
	objs/p_common/proc_fs.o

HDRS =	objs/sensor.h \
	objs/mem/segment.h \
	objs/mem/memacct.h \
	objs/cpu/s_task.h \
	objs/cpu/cpuacct.h \
	objs/s_common/s_list.h \
	objs/s_common/s_list_impl.h \
	objs/s_common/common.h \
	objs/s_common/debug.h \
	objs/p_common/s_proj.h \
	objs/p_common/s_zone.h \
	objs/p_common/proc_fs.h

LINTFILES = $(OBJS:%.o=%.ln)
CHECK_FILES = $(OBJS:%.o=%.c_check) $(HDRS:%.h=%.h_check)

OUTPUT_OPTION = -o $@

# ASSERT in list.c (which comes from Solaris) uses libaio symbols.
# libaio is required because there are ASSERT-related symbols defined 
# there and used by libproject.
LDLIBS.cmd += -lproject -lrt -laio -lexacct

# The _r variants of strtok & co. are used with the goal of improving
# performance by avoiding COW operations. Hence REENTRANT=1 here.
CPPFLAGS += $(CL_CPPFLAGS)
CPPFLAGS += -I$(SRC)/lib/cmass/agent_ganymede
CPPFLAGS += -I.. -I../../cpu -I../../mem -I../../common -I../../../common
CPPFLAGS += -D_REENTRANT=1

.KEEP_STATE:

all: $(PROG)

# LINTFILES
# Note: LHEAD and LTAIL defined in $(SRC)/cmd/Makefile.targ
objs/mem/%.ln: ../../mem/%.c objs objs/mem
	@($(LHEAD) $(LINT.c) $< $(LTAIL))

objs/cpu/%.ln: ../../cpu/%.c objs objs/cpu
	@($(LHEAD) $(LINT.c) $< $(LTAIL))

objs/s_common/%.ln: ../../../common/%.c objs objs/s_common
	@($(LHEAD) $(LINT.c) $< $(LTAIL))

objs/p_common/%.ln: ../../common/%.c objs objs/p_common
	@($(LHEAD) $(LINT.c) $< $(LTAIL))

objs/%.ln: ../%.c objs
	@($(LHEAD) $(LINT.c) $< $(LTAIL))

# CHECK_FILES
objs/mem/%.c_check: ../../mem/%.c objs objs/mem
	$(DOT_C_CHECK)

objs/cpu/%.c_check: ../../cpu/%.c objs objs/cpu
	$(DOT_C_CHECK)

objs/s_common/%.c_check: ../../../common/%.c objs objs/s_common
	$(DOT_C_CHECK)

objs/p_common/%.c_check: ../../common/%.c objs objs/p_common
	$(DOT_C_CHECK)

objs/%.c_check: ../%.c objs
	$(DOT_C_CHECK)

objs/mem/%.h_check: ../../mem/%.h objs objs/mem
	$(DOT_H_CHECK)

objs/cpu/%.h_check: ../../cpu/%.h objs objs/cpu
	$(DOT_H_CHECK)

objs/s_common/%.h_check: ../../../common/%.h objs objs/s_common
	$(DOT_H_CHECK)

objs/p_common/%.h_check: ../../common/%.h objs objs/p_common
	$(DOT_H_CHECK)

objs/%.h_check: ../%.h objs
	$(DOT_H_CHECK)

# OBJS directories
objs:
	-$(MKDIR) objs

objs/mem:
	-$(MKDIR) objs/mem

objs/cpu:
	-$(MKDIR) objs/cpu

objs/s_common:
	-$(MKDIR) objs/s_common

objs/p_common:
	-$(MKDIR) objs/p_common

# installation
$(INSTDIR)/sparcv7:
	$(INS.dir)

$(INSTDIR)/sparcv9:
	$(INS.dir)

$(INSTDIR)/i386:
	$(INS.dir)

$(INSTDIR)/amd64:
	$(INS.dir)

$(INSTDIR):
	$(INS.dir)

clean:
	-$(RM) -r $(PROG) objs

# target clobber defined in there
include $(SRC)/cmd/Makefile.targ

