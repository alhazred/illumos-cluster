/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)s_task.c	1.3	08/05/20 SMI"

#include <stdlib.h>	/* malloc, free */
#include <errno.h>	/* errno */
#include <string.h>	/* memset */
#include <stddef.h>	/* offsetof */
#include <sys/types.h>	/* taskid_t */

#include "debug.h"		/* DBG */
#include "common.h"		/* set_error_str */
#include "s_list.h"		/* s_list_t */

#include "s_task.h"

static struct task_entity *
task_zalloc(void)
{
	struct task_entity *new;

	new = (struct task_entity *)malloc(sizeof (struct task_entity));
	if (new == NULL) {
		/*lint -e746 */
		set_error_str("malloc failed (%s)", strerror(errno));
		/*lint +e746 */
		return (NULL);
	}

	(void) memset(new, 0, sizeof (struct task_entity));

	return (new);
}

struct task_entity *
task_create(const struct task_info *taskinfo)
{
	struct task_entity *task;

	DBG(DL5, ("creating task %d\n", (int)taskinfo->taskid));

	if ((task = task_zalloc()) == NULL)
		return (NULL);

	(void) memcpy(&task->info, taskinfo, sizeof (task->info));

	return (task);
}

static void
task_free(struct task_entity *task)
{
	DBG(DL5, ("destroying task %d\n", task->info.taskid));

	free(task);
}

/*
 * Returns non-NULL if task is already in list,
 * NULL otherwise.
 */
struct task_entity *
task_lookup_byid(s_list_t *list, taskid_t id)
{
	struct task_entity *task = NULL;

	s_list_for_each(task, list)
		if (task->info.taskid == id)
			break;

	return (task);
}

void
task_insert(s_list_t *list, struct task_entity *task)
{
	s_list_insert_tail(list, task);
}

void
task_list_create(s_list_t *list)
{
	/*lint -e413 */
	s_list_create(list, sizeof (struct task_entity),
		offsetof(struct task_entity, node));
	/*lint +e413 */
}

void
task_list_empty(s_list_t *list)
{
	struct task_entity *task = s_list_head(list);

	while (task != NULL) {
		struct task_entity *next;

		next = s_list_next(list, task);
		s_list_remove(list, task);
		task_free(task);
		task = next;
	}
}

void
task_list_destroy(s_list_t *list)
{
	task_list_empty(list);
	s_list_destroy(list);
}
