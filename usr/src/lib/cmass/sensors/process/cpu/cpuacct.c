/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)cpuacct.c	1.2	08/05/20 SMI"

#include <stdlib.h>		/* free, */
#include <stdio.h>		/* rename, */
#include <unistd.h>		/* vfork, execlp */
#include <errno.h>		/* errno */
#include <assert.h>		/* assert */
#include <string.h>		/* memset */
#include <exacct.h>		/* ea_error */
#include <fcntl.h>		/* O_RDONLY */
#include <sys/wait.h>		/* wait4, WIFEXITED */
#include <sys/time.h>		/* wait4, */
#include <sys/types.h>		/* taskid_t */
#include <sys/exacct.h>		/* getacct */
#include <sys/acctctl.h>	/* acctctl, */

#include "common.h"
#include "debug.h"		/* DBG, */
#include "s_task.h"		/* struct task_info */
#include "s_zone.h"		/* ZONENAME_MAX, AC_TASK_ZONENAME */

#include "cpuacct.h"

#define	AC_LOGFILE	"/var/adm/exacct/task"
#define	AC_LOGFILE_TMP	"/var/adm/exacct/.__scslm_task"
#define	AC_BUFSIZE	(sizeof (ac_res_t) * (AC_TASK_MAX_RES + 1))
#define	ACCTADMFILE	"/usr/sbin/acctadm"

/*
 * Warning: keep AC_TASK_ZONENAME at the before last position. This is because
 * AC_TASK_ZONENAME is defined to AC_NONE for Solaris 9, see s_zone.h.
 */
static unsigned int task_resources[] = {
	AC_TASK_TASKID,
	AC_TASK_PROJID,
	AC_TASK_CPU,
	AC_TASK_TIME,
	AC_TASK_ZONENAME,
	AC_NONE
};

static void
getdata(struct task_info *info, const ea_object_t *obj)
{
	switch (obj->eo_catalog) {
	case EXT_UINT32 | EXC_DEFAULT | EXD_TASK_TASKID:
		info->taskid = (taskid_t)obj->eo_item.ei_uint32;
		break;
	case EXT_UINT32 | EXC_DEFAULT | EXD_TASK_PROJID:
		info->projid = (projid_t)obj->eo_item.ei_uint32;
		break;
	case EXT_UINT64 | EXC_DEFAULT | EXD_TASK_FINISH_SEC:
		info->finish_secs = obj->eo_item.ei_uint64;
		break;
	case EXT_UINT64 | EXC_DEFAULT | EXD_TASK_FINISH_NSEC:
		info->finish_nsecs = obj->eo_item.ei_uint64;
		break;
	case EXT_UINT64 | EXC_DEFAULT | EXD_TASK_START_SEC:
		info->start_secs = obj->eo_item.ei_uint64;
		break;
	case EXT_UINT64 | EXC_DEFAULT | EXD_TASK_START_NSEC:
		info->start_nsecs = obj->eo_item.ei_uint64;
		break;
	case EXT_UINT64 | EXC_DEFAULT | EXD_TASK_CPU_SYS_SEC:
		info->usage.cpu_sys_secs = obj->eo_item.ei_uint64;
		break;
	case EXT_UINT64 | EXC_DEFAULT | EXD_TASK_CPU_USER_SEC:
		info->usage.cpu_usr_secs = obj->eo_item.ei_uint64;
		break;
	case EXT_UINT64 | EXC_DEFAULT | EXD_TASK_CPU_SYS_NSEC:
		info->usage.cpu_sys_nsecs = obj->eo_item.ei_uint64;
		break;
	case EXT_UINT64 | EXC_DEFAULT | EXD_TASK_CPU_USER_NSEC:
		info->usage.cpu_usr_nsecs = obj->eo_item.ei_uint64;
		break;
	case EXT_STRING | EXC_DEFAULT | EXD_TASK_ZONENAME:
		(void) strncpy(info->zonename, obj->eo_item.ei_string,
			(size_t)ZONENAME_MAX);
		break;
	default:
		break;
	}
}

static char *
ea_strerror()
{
	switch (ea_error()) {
	case EXR_OK:
		return ((char *)"EXR_OK");
	case EXR_SYSCALL_FAIL:
		return ((char *)"EXR_SYSCALL_FAIL");
	case EXR_CORRUPT_FILE:
		return ((char *)"EXR_CORRUPT_FILE");
	case EXR_EOF:
		return ((char *)"EXR_EOF");
	case EXR_NO_CREATOR:
		return ((char *)"EXR_NO_CREATOR");
	case EXR_INVALID_BUF:
		return ((char *)"EXR_INVALID_BUF");
	case EXR_NOTSUPP:
		return ((char *)"EXR_NOTSUPP");
	case EXR_UNKN_VERSION:
		return ((char *)"EXR_UNKN_VERSION");
	case EXR_INVALID_OBJ:
		return ((char *)"EXR_INVALID_OBJ");
	default:
		return ((char *)"unknown error");
	}
}

static int
handle_group(ea_file_t *ef, uint_t nobjs, struct task_info *info)
{
	uint_t i;

	for (i = 0; i < nobjs; i++) {
		ea_object_t curr_obj;

		/*
		 * Solaris extended accounting code has a memory leak (CR
		 * 6248632). The HACK section below work around it.
		 */

		/* HACK section */
		curr_obj.eo_item.ei_string = NULL;
		/* HACK section */

		if (ea_get_object(ef, &curr_obj) == -1) {
			set_error_str("accounting file corrupted\n");
			return (-1);
		}

		getdata(info, &curr_obj);

		if (curr_obj.eo_type == EO_GROUP) {

			DBG(DL1, ("handle_group recursing\n"));

			if (handle_group(ef, curr_obj.eo_group.eg_nobjs,
				info) < 0) {
				return (-1);
			}
		}

		/* HACK section */
		switch (curr_obj.eo_catalog & EXT_TYPE_MASK) {
		case EXT_STRING:
		case EXT_EXACCT_OBJECT:
		case EXT_RAW:
			if (curr_obj.eo_item.ei_string != NULL) {
				ea_free(curr_obj.eo_item.ei_string,
					(size_t)curr_obj.eo_item.ei_size);
			}
			break;
		default:
			break;
		}
		/* HACK section */
	}

	return (0);
}

/* ARGSUSED1 */
static int
skip_group(ea_file_t *ef, uint_t nobjs)
{
	ea_object_t curr_obj;

	if (ea_previous_object(ef, &curr_obj) == -1) {
		set_error_str("accounting file corrupted");
		return (-1);
	}

	return (0);
}

static boolean_t
resok(ac_res_t *buf)
{
	int i;
	unsigned int id;

	for (i = 0; (id = task_resources[i]) != AC_NONE; i++) {
		ac_res_t *p;

		p = (ac_res_t *)((uintptr_t)buf +
			(sizeof (ac_res_t) * (id - 1)));

		if (p->ar_state != AC_ON)
			return (B_FALSE);
	}

	return (B_TRUE);
}

static void
resset(ac_res_t *buf)
{
	int i;
	unsigned int id;

	for (i = 0; (id = task_resources[i]) != AC_NONE; i++) {
		ac_res_t *p;

		p = (ac_res_t *)((uintptr_t)buf +
			(sizeof (ac_res_t) * (id - 1)));

		p->ar_state = AC_ON;
	}
}

/*
 * activate_task_acct(file, filename, size) activates Solaris extended
 * accounting for the resource "task". If file is true an extended accounting
 * log file is configured in the system, and if filename isn't NULL it is filled
 * with the path to that log file (up to size bytes). If file is false, the
 * extended accounting logging facility isn't enabled, and the memory pointed to
 * by filename isn't touched.
 */
int
activate_task_acct(boolean_t file, char *filename, size_t size)
{
	int state = 0; /* dbx check -all wants this intialized */
	void *buf;

	if (file == B_TRUE) {
		char *f;
		size_t sz;
		boolean_t flag; /* free f? */

		if (filename == NULL) {
			if ((f = (char *)malloc((sz = MAXPATHLEN))) == NULL) {
				/*lint -e746 */
				set_error_str("malloc failed (%s)",
					strerror(errno));
				/*lint +e746 */
				return (-1);
			}
			flag = B_TRUE;
		} else {
			f = filename;
			sz = size;
			flag = B_FALSE;
		}

		if (acctctl(AC_TASK | AC_FILE_GET, f, sz) < 0) {
			if (errno != ENOTACTIVE) {
				set_error_str("cannot get task accounting file"
					": %s", strerror(errno));
				if (flag == B_TRUE)
					free(f);
				return (-1);
			}

			(void) strlcpy(f, AC_LOGFILE, sz);

			if (acctctl(AC_TASK | AC_FILE_SET, f,
				strlen(f) + 1) < 0) {
				set_error_str("cannot set task accounting file"
					": %s", strerror(errno));
				if (flag == B_TRUE)
					free(f);
				return (-1);
			}
		}

		if (flag == B_TRUE)
			free(f);
	}

	if (acctctl(AC_TASK | AC_STATE_GET, &state, sizeof (state)) < 0) {
		set_error_str("cannot get task accounting state: %s",
			strerror(errno));
		return (-1);
	}

	if (state == AC_OFF) {
		state = AC_ON;
		if (acctctl(AC_TASK | AC_STATE_SET, &state, sizeof (state))
			< 0) {
			set_error_str("cannot set task accounting state: %s",
				strerror(errno));
			return (-1);
		}
	}

	if ((buf = malloc(AC_BUFSIZE)) == NULL) {
		set_error_str("malloc failed (%s)", strerror(errno));
		return (-1);
	}
	(void) memset(buf, 0, AC_BUFSIZE);

	if (acctctl(AC_TASK | AC_RES_GET, buf, AC_BUFSIZE) < 0) {
		set_error_str("cannot get the list of enabled resources: %s",
			strerror(errno));
		free(buf);
		return (-1);
	}

	if (resok(buf) == B_FALSE) {
		resset(buf);
		if (acctctl(AC_TASK | AC_RES_SET, buf, AC_BUFSIZE) < 0) {
			set_error_str("cannot set the list of enabled"
				" resources: %s", strerror(errno));
			free(buf);
			return (-1);
		}
	}

	free(buf);

	return (0);
}

static int
__deactivate_task_acct(void)
{
	int state = AC_OFF;

	if (acctctl(AC_TASK | AC_STATE_SET, &state, sizeof (state)) < 0) {
		set_error_str("cannot set task accounting state: %s",
			strerror(errno));
		return (-1);
	}

	if (acctctl(AC_TASK | AC_FILE_SET, NULL, (size_t)0) < 0) {
		set_error_str("cannot close task accounting log: %s",
			strerror(errno));
		return (-1);
	}

	return (0);
}

int
deactivate_task_acct(void)
{
	pid_t pid;

	DBG(DL5, ("deactivate_task_acct: begin\n"));

	/*
	 * Deactivate.
	 */

	if (__deactivate_task_acct() < 0)
		return (-1);

	/*
	 * Apply user configuration.
	 */

	DBG(DL5, ("deactivate_task_acct: vfork\n"));

	if ((pid = vfork()) == -1) {
		set_error_str("vfork failed (%s)", strerror(errno));
		return (-1);
	}

	if (pid == (pid_t)0) {
		/* child */

		(void) execlp(ACCTADMFILE, "acctadm", "-u", NULL);

		exit(1);
	} else {
		/* parent */
		int status;

		for (;;) {
			DBG(DL5, ("deactivate_task_acct: wait4\n"));

			if (wait4(pid, &status, 0, NULL) == pid)
				break; /* done */

			/* return code != pid */
			if (errno != EINTR) {
				set_error_str("wait4 failed (%s)",
					strerror(errno));
				return (-1);
			}

			/* wait4 got interrupted by a signal, try again */
		}

		if (!WIFEXITED(status)) {
			set_error_str("child process terminated abnormally");
			return (-1);
		}
	}

	DBG(DL5, ("deactivate_task_acct: ok\n"));

	return (0);
}

static int
clean_task_acct_log(char *filename, size_t sz)
{
	DBG(DL1, ("extended accounting log file cleanup"));

	if (__deactivate_task_acct() < 0)
		return (-1);

	if (rename(filename, AC_LOGFILE_TMP) < 0) {
		set_error_str("rename failed (%s)", strerror(errno));
		return (-1);
	}

	(void) strlcpy(filename, AC_LOGFILE_TMP, sz);

	if (activate_task_acct(B_TRUE, NULL, (size_t)0) < 0)
		return (-1);

	return (0);
}

int
get_tasks_info_from_log(boolean_t cleanup, time_t now, time_t period,
	action_t action)
{
	ea_file_t ef;
	ea_object_t curr_obj;
	char filename[MAXPATHLEN];
	boolean_t first = B_TRUE;
	boolean_t cleaned = B_FALSE;
	static time_t last = (time_t)-1; /* seconds */

	if (activate_task_acct(B_TRUE, filename, (size_t)MAXPATHLEN) < 0)
		return (-1);

	if (last == (time_t)-1)
		last = now;

	if ((cleanup == B_TRUE) && ((now - last) > period)) {

		/*
		 * Extended Accounting log file is older than a day,
		 * remove it and replace it by a fresh new one.
		 */

		if (clean_task_acct_log(filename, (size_t)MAXPATHLEN) < 0)
			return (-1);

		last = now;
		cleaned = B_TRUE;
	}

	DBG(DL5, ("Reading accounting file %s\n", filename));

	if (ea_open(&ef, filename, "SunOS",
		    EO_TAIL | EO_VALID_HDR, O_RDWR, 0) < 0) {
		switch (ea_error()) {
			case EXR_CORRUPT_FILE:
				set_error_str("accounting file corrupted (%s)",
					ea_strerror());
				break;
			case EXR_SYSCALL_FAIL:
				set_error_str("cannot open %s: %s",
					filename, strerror(errno));
				break;
			default:
				break;
		}
		if (cleaned == B_TRUE)
			(void) unlink(filename);
		return (-1);
	}

	while (ea_previous_object(&ef, &curr_obj) != -1) {

		if (ea_get_object(&ef, &curr_obj) == -1) {
			set_error_str("accounting file corrupted\n");
			(void) ea_close(&ef);
			if (cleaned == B_TRUE)
				(void) unlink(filename);
			return (-1);
		}

		if ((curr_obj.eo_catalog & EXT_TYPE_MASK) == EXT_GROUP) {
			uint_t nobjs = curr_obj.eo_group.eg_nobjs;

			assert(curr_obj.eo_type == EO_GROUP);

			/*
			 * Here we just skip EXD_GROUP_TASK_PARTIAL and
			 * EXD_GROUP_TASK_INTERVAL tasks. These are
			 * currently running tasks and are already
			 * counted as such (using getacct).
			 */
			if ((curr_obj.eo_catalog ==
				(EXT_GROUP | EXC_DEFAULT | EXD_GROUP_TASK))) {
				struct task_info info;

				(void) memset(&info, 0, sizeof (info));
				info.state = FINISHED;

				if (first == B_TRUE) {
					/*
					 * Tell caller this is the first task in
					 * log.
					 */
					info.state |= FIRST;
					first = B_FALSE;
				}

				if (handle_group(&ef, nobjs, &info) < 0) {
					(void) ea_close(&ef);
					if (cleaned == B_TRUE)
						(void) unlink(filename);
					return (-1);
				}

				if (action != NULL) {
					int ret;

					if ((ret = action(&info)) < 0) {
						(void) ea_close(&ef);
						if (cleaned == B_TRUE)
							(void) unlink(filename);
						return (-1);
					}

					if (ret == 1)	/* done */
						break;
				}
			} else {
				DBG(DL1, ("skip group\n"));
				if (skip_group(&ef, nobjs) < 0) {
					(void) ea_close(&ef);
					if (cleaned == B_TRUE)
						(void) unlink(filename);
					return (-1);
				}
			}
		}

		/* back up to the head of the object we just consumed */
		if (ea_previous_object(&ef, &curr_obj) == -1) {
			if (ea_error() == EXR_EOF)
				break;

			set_error_str("accounting file corrupted\n");
			(void) ea_close(&ef);
			if (cleaned == B_TRUE)
				(void) unlink(filename);
			return (-1);
		}
	}

	if (ea_error() != EXR_EOF) {
		DBG(DL5, ("EXR_EOF not reached\n"));
	}

	(void) ea_close(&ef);
	if (cleaned == B_TRUE)
		(void) unlink(filename);

	return (0);
}

static void
obj_get_info(ea_object_t *obj, struct task_info *info)
{
	ea_object_t *cur;

	assert(obj->eo_type == EO_GROUP);

	for (cur = obj->eo_group.eg_objs; cur != NULL; cur = cur->eo_next) {

		/* sanity checks */
		if (cur->eo_type != EO_ITEM) {
			DBG(DL1, ("Unexpected ea object at %s:%d\n",
				__FILE__, __LINE__));
			continue;
		}

		if ((cur->eo_catalog & EXC_CATALOG_MASK) != EXC_DEFAULT) {
			DBG(DL1, ("Unexpected ea catalog at %s:%d\n",
				__FILE__, __LINE__));
			continue;
		}

		/* now go look at the data */
		getdata(info, cur);
	}
}

int
get_task_info(taskid_t taskid, struct task_info *info)
{
	size_t len;
	size_t len_saved;
	void *buf;
	ea_object_t *obj;
	ea_object_type_t ot;

	if (activate_task_acct(B_FALSE, NULL, (size_t)0) < 0)
		return (-1);

	/*
	 * getacct return type is size_t while manpage says getacct returns a
	 * negative value on failure!
	 */

	len = getacct(P_TASKID, taskid, NULL, (size_t)0);
	if ((ssize_t)len < (ssize_t)0) {
		set_error_str("getacct failed (%s)", strerror(errno));
		return (-1);
	}
	if (len == (size_t)0) {
		DBG(DL1, ("getacct has no data for task %d\n", (int)taskid));
		return (0);
	}
again:
	len_saved = len;
	if ((buf = malloc(len)) == NULL) {
		set_error_str("malloc failed (%s)", strerror(errno));
		return (-1);
	}
	(void) memset(buf, 0, len);

	len = getacct(P_TASKID, taskid, buf, len);
	if ((ssize_t)len < (ssize_t)0) {
		set_error_str("getacct failed (%s)", strerror(errno));
		free(buf);
		return (-1);
	}
	if (len == (size_t)0) {
		DBG(DL1, ("getacct has no data for task %d\n", (int)taskid));
		free(buf);
		return (0);
	}
	if (len != len_saved) {
		DBG(DL1, ("returned len changed between two getacct calls\n"));
		free(buf);
		goto again;
	}

	ot = ea_unpack_object(&obj, EUP_NOALLOC, buf, len);

	switch (ot) {
	case EO_GROUP:
		obj_get_info(obj, info);
		break;
	case EO_NONE:
	case EO_ITEM:
		set_error_str(__SENSOR_BUG);
		break;
	case EO_ERROR:
		set_error_str("ea_unpack_object failed (%s, %ld)",
			ea_strerror(), (long)taskid);
		break;
	default:
		assert(1);
	}

	ea_free_object(obj, EUP_NOALLOC);
	free(buf);

	return (0);
}
