/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	__CPUACCT_H
#define	__CPUACCT_H

#pragma ident	"@(#)cpuacct.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>	/* taskid_t, */

#include "s_task.h"	/* struct task_info */

typedef int (*action_t)(const struct task_info *);

extern int activate_task_acct(boolean_t, char *, size_t);
extern int deactivate_task_acct(void);
extern int get_task_info(taskid_t, struct task_info *);
extern int get_tasks_info_from_log(boolean_t, time_t, time_t, action_t);

#ifdef __cplusplus
}
#endif

#endif /* __CPUACCT_H */
