/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef __TASK_H
#define	__TASK_H

#pragma ident	"@(#)s_task.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <exacct.h>		/* EXD_GROUP_TASK, */
#include <time.h>		/* timespec_t */
#include <sys/types.h>	/* taskid_t */

#include "s_list.h"		/* s_list_t */
#include "s_zone.h"		/* ZONENAME_MAX */

enum __task_state {
	RUNNING	= 	1<<0,
	FINISHED = 	1<<1,
	FIRST =		1<<2
};

struct cpu_usage {
	uint64_t cpu_sys_secs;
	uint64_t cpu_usr_secs;
	uint64_t cpu_sys_nsecs;
	uint64_t cpu_usr_nsecs;
};

struct task_info {
	int	state;
	taskid_t taskid;
	projid_t projid;
	id_t zoneid;
	char zonename[ZONENAME_MAX];
	uint64_t start_secs;
	uint64_t start_nsecs;
	uint64_t finish_secs;
	uint64_t finish_nsecs;
	struct cpu_usage usage;
};

struct task_entity {
	s_list_node_t		node;
	struct task_info	info;
};

extern void	task_list_create(s_list_t *);
extern void task_list_empty(s_list_t *);
extern void	task_list_destroy(s_list_t *);
extern struct task_entity *task_lookup_byid(s_list_t *, taskid_t);
extern struct task_entity *task_create(const struct task_info *);
extern void	task_insert(s_list_t *, struct task_entity *);

#ifdef __cplusplus
}
#endif

#endif /* __TASK_H */
