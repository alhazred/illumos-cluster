/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)s_zone.c	1.3	08/05/20 SMI"

#include <stdlib.h>	/* malloc, free */
#include <stdio.h>	/* fopen, */
#include <errno.h>	/* errno */
#include <string.h>	/* memset */
#include <stddef.h>	/* offsetof */
#include <assert.h>	/* assert */
#include <project.h>	/* fgetprojent, */
#include <procfs.h>	/* psinfo_t */
#include <sys/types.h>	/* id_t */

/*
 * NOTE: <zone.h> must not be included here because that file
 * must compile against Solaris 9 as well. On Solaris 9 zones
 * are abstracted, see "s_zone.h".
 */

#include "debug.h"		/* DBG */
#include "common.h"		/* set_error_str */
#include "s_list.h"		/* s_list_t */
#include "s_proj.h"		/* struct proj_entity */

#include "s_zone.h"

s_list_t s_zone_list;

static struct s_zone_entity *
s_zone_zalloc(void)
{
	struct s_zone_entity *new;

	new = (struct s_zone_entity *)malloc(sizeof (struct s_zone_entity));
	if (new == NULL) {
		/*lint -e746 */
		set_error_str("malloc failed (%s)", strerror(errno));
		/*lint +e746 */
		return (NULL);
	}

	(void) memset(new, 0, sizeof (struct s_zone_entity));

	return (new);
}

static int
hashfn(int input)
{
	return (input & (PROJHTAB_LEN - 1));
}

static void
s_zone_create_proj_table(struct s_zone_entity *zone)
{
	int i;

	for (i = 0; i < PROJHTAB_LEN; i++)
		proj_list_create(&zone->proj_table[i]);
}

static void
s_zone_destroy_proj_table(struct s_zone_entity *zone)
{
	int i;

	for (i = 0; i < PROJHTAB_LEN; i++)
		proj_list_destroy(&zone->proj_table[i]);
}

/* ARGSUSED0 */
int
s_zone_getname(const psinfo_t *psinfo, char *buf, size_t buflen)
{
#if SOL_VERSION >= __s10
	if (getzonenamebyid(psinfo->pr_zoneid, buf, buflen) < 0) {
		set_error_str("getzonenamebyid failed (%s)", strerror(errno));
		return (-1);
	}
#else
	(void) strncpy(buf, GLOBAL_ZONENAME, buflen);
#endif
	return (0);
}

/* ARGSUSED0 */
id_t
s_zone_getid(const char *zonename)
{
	id_t zone_id;

#if SOL_VERSION >= __s10
	if ((zone_id = (id_t)getzoneidbyname(zonename)) < (id_t)0) {
		set_error_str("getzoneidbyname failed (%s)", strerror(errno));
		return ((id_t)-1);
	}
#else
	zone_id = (id_t)GLOBAL_ZONEID;
#endif
	return (zone_id);
}

/* ARGSUSED0 */
char *
s_zone_getroot(id_t zone_id)
{
	char *root = NULL;
#if SOL_VERSION >= __s10
	{
		long ret;
		size_t sz;

		if ((ret = zone_getattr((zoneid_t)zone_id, ZONE_ATTR_ROOT, NULL,
			(size_t)0)) < 0) {

			set_error_str("zone_getattr failed (%s)",
				strerror(errno));
			return (NULL);
		}

		sz = (size_t)ret;
		if ((root = (char *)malloc(sz)) == NULL) {
			set_error_str("malloc failed (%s)", strerror(errno));
			return (NULL);
		}
		(void) memset(root, 0, sz);

		if (zone_getattr((zoneid_t)zone_id, ZONE_ATTR_ROOT, root, sz)
			!= (int)sz) {

			set_error_str("zone_getattr failed (%s)",
				strerror(errno));
			free(root);
			return (NULL);
		}
	}
#else
	if ((root = strdup("/")) == NULL) {
		set_error_str("strdup failed (%s)", strerror(errno));
		return (NULL);
	}
#endif

	return (root);
}

projid_t
s_zone_getprojidbyname(const char *zoneroot, const char *projname)
{
	FILE *file;
	char *path;
	void *buffer;
	struct project projent;
	boolean_t gotit = B_FALSE;
	size_t buffsize = PROJECT_BUFSZ;

	if ((path = (char *)malloc(strlen(zoneroot) + strlen(PROJF_PATH) + 1))
		== NULL) {

		set_error_str("malloc failed (%s)", strerror(errno));
		return (-2);
	}

	(void) strcpy(path, zoneroot);
	(void) strcat(path, PROJF_PATH);

	DBG(DL4, ("path to project database file is: %s\n", path));

	if ((file = fopen(path, "r")) == NULL) {
		set_error_str("fopen failed (%s)", strerror(errno));
		free(path);
		return (-2);
	}

	if ((buffer = malloc(buffsize)) == NULL) {
		set_error_str("malloc failed (%s)", strerror(errno));
		(void) fclose(file);
		free(path);
		return (-2);
	}

	errno = 0;
	(void) memset(&projent, 0, sizeof (projent));
	while (fgetprojent(file, &projent, buffer, buffsize) != NULL) {
		if (strcmp(projent.pj_name, projname) == 0) {
			gotit = B_TRUE;
			break;
		}
		errno = 0;
		(void) memset(&projent, 0, sizeof (projent));
	}

	if (gotit == B_FALSE) {
		int err = errno;

		endprojent();
		free(buffer);
		(void) fclose(file);
		free(path);

		if (err != 0) {
			set_error_str("fgetprojent failed (%s)",
				strerror(errno));
			return (-2);
		}
		return (-1); /* project isn't in DB */
	}

	endprojent();
	free(buffer);
	(void) fclose(file);
	free(path);

	return (projent.pj_projid);
}

struct s_zone_entity *
s_zone_create(const char *zonename)
{
	struct s_zone_entity *zone;

	if ((zone = s_zone_zalloc()) == NULL)
		return (NULL);

	if (strlen(zonename) == 0) {
		(void) strncpy(zone->zonename, GLOBAL_ZONENAME,
			(size_t)ZONENAME_MAX);
	} else {
		(void) strncpy(zone->zonename, zonename,
			(size_t)ZONENAME_MAX);
	}

	s_zone_create_proj_table(zone);

	if ((zone->id = s_zone_getid(zonename)) < (id_t)-1) {
		s_zone_destroy(zone);
		return (NULL);
	}

	if ((zone->root = s_zone_getroot(zone->id)) == NULL) {
		s_zone_destroy(zone);
		return (NULL);
	}

	DBG(DL5, ("zone %s:%d created\n", zone->zonename, (int)zone->id));

	return (zone);
}

void
s_zone_destroy(struct s_zone_entity *zone)
{
	DBG(DL5, ("destroying zone %s\n", zone->zonename));

	s_zone_destroy_proj_table(zone);
	free(zone->root);
	free(zone);
}

void
s_zone_list_create(void)
{
	/*lint -e413 */
	s_list_create(&s_zone_list, sizeof (struct s_zone_entity),
		offsetof(struct s_zone_entity, node));
	/*lint +e413 */
}

void
s_zone_list_destroy(void)
{
	struct s_zone_entity *zone = s_list_head(&s_zone_list);

	while (zone != NULL) {
		struct s_zone_entity *next;

		next = s_list_next(&s_zone_list, zone);
		s_list_remove(&s_zone_list, zone);
		s_zone_destroy(zone);
		zone = next;
	}

	s_list_destroy(&s_zone_list);
}

struct s_zone_entity *
s_zone_lookup(const char *zonename)
{
	const char *name;
	struct s_zone_entity *zone;

	if (strlen(zonename) == 0)
		name = GLOBAL_ZONENAME;
	else
		name = zonename;

	s_list_for_each(zone, &s_zone_list)
		if (strncmp(zone->zonename, name,
			(size_t)ZONENAME_MAX) == 0)
			break;

	return (zone);
}

void
s_zone_insert(struct s_zone_entity *zone)
{
	s_list_insert_tail(&s_zone_list, zone);
}

int
s_zone_insert_proj(struct s_zone_entity *zone, struct proj_entity *new)
{
	s_list_t *list;
	struct proj_entity *it;

	assert(new != NULL);

	list = &zone->proj_table[hashfn((int)new->projid)];

	if ((it = proj_lookup_byid(list, new->projid)) != NULL) {

		if (it->projid == new->projid) {
			/* already in */
			set_error_str(__SENSOR_BUG);
			return (-1);
		}

		s_list_insert_before(list, it, new);

		return (0);
	}

	s_list_insert_tail(list, new);

	return (0);
}

struct proj_entity *
s_zone_lookup_proj_byid(struct s_zone_entity *zone, projid_t id)
{
	s_list_t *list;

	list = &zone->proj_table[hashfn((int)id)];

	return (proj_lookup_byid(list, id));
}

struct proj_entity *
s_zone_lookup_proj_byname(struct s_zone_entity *zone, const char *projname)
{
	projid_t projid;

	if ((projid = s_zone_getprojidbyname(zone->root, projname)) < 0)
		return (NULL);

	return (s_zone_lookup_proj_byid(zone, projid));
}
