/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef __S_PROJ_H
#define	__S_PROJ_H

#pragma ident	"@(#)s_proj.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <project.h>	/* PROJNAME_MAX */
#include <sys/types.h>	/* projid_t */

#include "s_list.h"		/* s_list_t, s_list_node_t */

typedef void (*func_t)(void *);

struct proj_entity {
	s_list_node_t	node;
	char			projname[PROJNAME_MAX];
	projid_t		projid;
	func_t			destructor;
	void			*priv;
};

extern struct proj_entity *proj_create(projid_t, size_t, func_t, func_t);
extern void proj_destroy(struct proj_entity *);
extern void proj_list_create(s_list_t *);
extern void proj_list_destroy(s_list_t *);
extern void proj_insert(s_list_t *, struct proj_entity *);
extern struct proj_entity *proj_lookup_byname(s_list_t *, const char *);
extern struct proj_entity *proj_lookup_byid(s_list_t *, projid_t);

#ifdef __cplusplus
}
#endif

#endif /* __S_PROJ_H */
