/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef __S_ZONE_H
#define	__S_ZONE_H

#pragma ident	"@(#)s_zone.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <procfs.h>		/* pstatus_t */
#include <sys/types.h>		/* id_t */
#include <sys/sol_version.h>	/* SOL_VERSION */

/* zone abstraction for Solaris 9 */
#if SOL_VERSION >= __s10

#include <zone.h>

#else /* SOL_VERSION >= __s10 */

#define	ZONENAME_MAX		64
#define	GLOBAL_ZONENAME		"global"
#define	GLOBAL_ZONEID		0
#define	EXD_TASK_ZONENAME	0x002019

#include <sys/acctctl.h>
#define	AC_TASK_ZONENAME	AC_NONE

#endif /* SOL_VERSION >= __s10 */

#include "s_list.h"		/* s_list_t */
#include "s_proj.h"		/* struct proj_entity */

#define	PROJHTAB_LEN 64

struct s_zone_entity {
	s_list_node_t	node;
	id_t		id;
	char		zonename[ZONENAME_MAX];
	char		*root;
	s_list_t	proj_table[PROJHTAB_LEN];
};

extern id_t s_zone_getid(const char*);
extern int s_zone_getname(const psinfo_t *, char *, size_t);
extern char *s_zone_getroot(id_t);
extern projid_t s_zone_getprojidbyname(const char*, const char *);
extern void s_zone_list_create(void);
extern void s_zone_list_destroy(void);
extern struct s_zone_entity *s_zone_lookup(const char *);
extern struct s_zone_entity *s_zone_create(const char *);
extern void s_zone_destroy(struct s_zone_entity *);
extern void s_zone_insert(struct s_zone_entity *);
extern int s_zone_insert_proj(struct s_zone_entity *, struct proj_entity *);
extern struct proj_entity *s_zone_lookup_proj_byid(struct s_zone_entity *,
	projid_t);
extern struct proj_entity *s_zone_lookup_proj_byname(struct s_zone_entity *,
	const char *);

extern s_list_t s_zone_list;

#define	s_zone_for_each(zone) s_list_for_each(zone, &s_zone_list)

#define	s_zone_for_each_proj(i, proj, zone)\
	for (i = 0; i < PROJHTAB_LEN; i++)\
		s_list_for_each(proj, &(zone)->proj_table[i])

#ifdef __cplusplus
}
#endif

#endif /* __S_ZONE_H */
