/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)proc_fs.c	1.3	08/05/20 SMI"

#include <errno.h>	/* for errno */
#include <stdio.h>	/* snprintf, */
#include <string.h>	/* memset */
#include <assert.h>	/* assert */
#include <unistd.h>	/* close, read, ... */
#include <stdlib.h>	/* atoi */
#include <fcntl.h>	/* O_RDONLY */
#include <dirent.h>	/* opendir */
#include <ctype.h>	/* isdigit */
#include <procfs.h>	/* prinfo_t, */

#include <sys/types.h>	/* opendir */
#include <sys/param.h>	/* for MAXPATHLEN */
#include <sys/stat.h>	/* for stat, st_size */

#include "common.h"	/* set_error_str */
#include "proc_fs.h"

/*
 * Returns a non-null dir handle on success, and
 * NULL on failure.
 */
DIR *
proc_fs_open(void)
{
	DIR *dir;

	dir = opendir("/proc/");
	if (dir == NULL) {
		/*lint -e746 */
		set_error_str("opendir failed (%s)", strerror(errno));
		/*lint +e746 */
		return (NULL);
	}

	return (dir);
}

void
proc_fs_close(DIR * dir)
{
	(void) closedir(dir);
}

/*
 * Reads next directory entry.
 *
 * Sets *pid to this entry's name and returns 0 on success,
 * sets *pid to NOPID and returns -1 on failure.
 */
int
proc_fs_read_one(DIR *dir, pid_t *pid)
{
	struct dirent *entry;

	assert(dir != NULL);

	/* read next /proc *pid* entry */
	for (;;) {
		errno = 0;
		entry = readdir(dir);
		if (entry == NULL) {
			if (errno == 0) {
				/* end of dir */
				*pid = NOPID;
				return (-1);
			}

			/* error */
			set_error_str("readdir failed (%s)", strerror(errno));
			*pid = NOPID;
			return (-1);
		}

		assert(entry != NULL);

		/* verify it's a pid entry */
		if (entry->d_name[0] != '.')
			break;
	}

	/*
	 * Currently the only /proc entries that are
	 * not pid entries are '.' and '..'.
	 */
	assert(isdigit(entry->d_name[0]));

	*pid = (pid_t)atol(entry->d_name);

	return (0);
}

/*
 * The proc_fs_get_* functions return 0 on sucess,
 * -1 on non-fatal failure and -2 on fatal failure.
 */

/*
 * Get process info from /proc/%d/psinfo and store
 * it in *info.
 */
int
proc_fs_get_info(pid_t proc_id, psinfo_t *info)
{
	char buf[MAXPATHLEN];
	int fd;
	size_t in;

	(void) memset(info, 0, sizeof (psinfo_t));

	(void) snprintf(buf, (size_t)MAXPATHLEN,
			"/proc/%d/psinfo", (int)proc_id);

	fd = open(buf, O_RDONLY);
	if (fd == -1) {
		if (errno == ENOENT)
			return (-1);

		set_error_str("open failed (%s)", strerror(errno));
		return (-2);
	}

	in = (size_t)read(fd, (void *) info, sizeof (psinfo_t));
	if (in != sizeof (psinfo_t)) {
		if (errno == ENOENT)
			return (-1);

		set_error_str("read failed (%d, %s)",
			errno, strerror(errno));
		return (-2);
	}

	(void) close(fd);

	return (0);
}

/*
 * Get process status from /proc/%d/status and stores
 * it into *status.
 */
int
proc_fs_get_status(pid_t proc_id, pstatus_t *status)
{
	char path[MAXPATHLEN];
	int fd;
	ssize_t in;

	(void) memset(status, 0, sizeof (pstatus_t));
	(void) snprintf(path, (size_t)MAXPATHLEN, "/proc/%d/status",
		(int)proc_id);

	fd = open(path, O_RDONLY);
	if (fd == -1) {
		if (errno == ENOENT)
			return (-1);

		set_error_str("open failed (%d, %s)",
			errno, strerror(errno));
		return (-2);
	}

	in = read(fd, (void *)status, sizeof (pstatus_t));
	if (in != (ssize_t)sizeof (pstatus_t)) {
		if (errno == ENOENT)
			return (-1);

		set_error_str("read failed (%d, %s)",
			errno, strerror(errno));
		return (-2);
	}

	(void) close(fd);

	return (0);
}

int
proc_fs_get_usage(pid_t proc_id, prusage_t *usage)
{
	char path[MAXPATHLEN];
	int fd;
	size_t in;

	(void) memset(usage, 0, sizeof (prusage_t));
	(void) snprintf(path, (size_t)MAXPATHLEN, "/proc/%d/usage",
		(int)proc_id);

	fd = open(path, O_RDONLY);
	if (fd == -1) {
		if (errno == ENOENT)
			return (-1);

		set_error_str("open failed (%s)", strerror(errno));
		return (-2);
	}

	in = (size_t)read(fd, (void *)usage, sizeof (prusage_t));
	if (in != sizeof (prusage_t)) {
		if (errno == ENOENT)
			return (-1);

		set_error_str("read failed (%d, %s)",
			errno, strerror(errno));
		return (-2);
	}

	(void) close(fd);

	return (0);
}

/*
 * Get process status from /proc/%d/status and stores
 * it into *status.
 *
 * NOTE: this function's code mostly comes from Solaris' pmap.
 *
 * NOTE: sys/proc_fs.h states that the xmap structure is a
 * Sun private interface, subject to arbitrary change.
 */
int
proc_fs_get_xmap(pid_t proc_id, prxmap_t **xmap)
{
	char path[MAXPATHLEN];
	prxmap_t *content_buffer = NULL;
	int fd;
	struct stat st;
	size_t sz, size;
	ssize_t size_read = 0;

	assert(*xmap == NULL);

	(void) snprintf(path, (size_t)MAXPATHLEN, "/proc/%ld/xmap",
		(long)proc_id);

	fd = open(path, O_RDONLY);
	if (fd == -1) {
		if (errno == ENOENT)
			return (-1);

		set_error_str("open failed (%d, %s)",
			errno, strerror(errno));
		return (-2);
	}

	if (fstat(fd, &st) != 0) {
		if (errno == ENOENT)
			return (-1);

		set_error_str("fstat failed (%d, %s)",
			errno, strerror(errno));
		(void) close(fd);
		return (-2);
	}

	sz = (size_t)st.st_size;
	size = (sz / sizeof (prxmap_t)) * (size_t)2;

retry:
	content_buffer = (prxmap_t *)calloc(size + (size_t)1,
		sizeof (prxmap_t));
	if (content_buffer == NULL) {
		set_error_str("calloc failed (%s)", strerror(errno));
		(void) close(fd);
		return (-2);
	}

	size_read = pread(
		fd,
		(void *)content_buffer,
		(size + 1) * sizeof (prxmap_t),
		(off_t)0);

	if (size_read == (ssize_t)-1) {
		if (errno == ENOENT)
			return (-1);

		set_error_str("read failed (%d, %s)",
			errno, strerror(errno));
		free(content_buffer);
		(void) close(fd);
		return (-2);
	}

	sz = (size_t)size_read;
	if (size < sz / sizeof (prxmap_t)) {
		free(content_buffer);
		size *= 2;
		goto retry;
	}

	(void) close(fd);

	*xmap = content_buffer;

	return ((int)(sz / sizeof (prxmap_t)));
}

void
proc_fs_get_path_to_map(pid_t proc_id, const char *map_name,
	char *string_to_fill, size_t size)
{
	(void) snprintf(string_to_fill, size, "/proc/%d/object/%s",
		(int)proc_id, map_name);
}
