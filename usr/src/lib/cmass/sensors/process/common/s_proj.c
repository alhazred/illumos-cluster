/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)s_proj.c	1.3	08/05/20 SMI"

#include <stdlib.h>	/* calloc		*/
#include <strings.h>	/* strdup		*/
#include <string.h>	/* strerror		*/
#include <errno.h>	/* errno		*/
#include <stddef.h>	/* offsetof		*/
#include <sys/types.h>	/* size_t		*/

#include "debug.h"
#include "common.h"
#include "s_list.h"

#include "s_proj.h"

/*
 * Allocate a proj entity object and write zeros into it.
 *
 * Returns the allocated proj on success, NULL otherwise.
 */
static struct proj_entity *
proj_zalloc(size_t size)
{
	struct proj_entity *new;
	size_t truesize = sizeof (struct proj_entity) + size;

	if ((new = (struct proj_entity *)malloc(truesize)) == NULL) {
		/*lint -e746 */
		set_error_str("malloc failed (%s)", strerror(errno));
		/*lint +e746 */
		return (NULL);
	}

	(void) memset(new, 0, truesize);

	return (new);
}

struct proj_entity *
proj_create(projid_t projid, size_t size, func_t constructor, func_t destructor)
{
	struct proj_entity *proj;

	DBG(DL5, ("creating project %d\n", (int)projid));

	if ((proj = proj_zalloc(size)) == NULL)
		return (NULL);

	proj->projid = projid;
	proj->priv = (size > 0) ? (void *)(proj + 1) : NULL;
	proj->destructor = destructor;

	if (constructor != NULL)
		constructor(proj->priv);

	return (proj);
}

void
proj_destroy(struct proj_entity *proj)
{
	DBG(DL5, ("destroying project %d\n", (int)proj->projid));

	if (proj->destructor != NULL)
		proj->destructor(proj->priv);

	free(proj);
}

void
proj_list_create(s_list_t *list)
{
	/*lint -e413 */
	s_list_create(list, sizeof (struct proj_entity),
		offsetof(struct proj_entity, node));
	/*lint +e413 */
}

void
proj_list_destroy(s_list_t *list)
{
	struct proj_entity *proj = s_list_head(list);

	while (proj != NULL) {
		struct proj_entity *next;

		next = s_list_next(list, proj);
		s_list_remove(list, proj);
		proj_destroy(proj);
		proj = next;
	}

	s_list_destroy(list);
}

void
proj_insert(s_list_t *list, struct proj_entity *proj)
{
	s_list_insert_tail(list, proj);
}

struct proj_entity *
proj_lookup_byid(s_list_t *list, projid_t projid)
{
	struct proj_entity *proj;

	s_list_for_each(proj, list)
		if (proj->projid == projid)
			break;

	return (proj);
}


struct proj_entity *
proj_lookup_byname(s_list_t *list, const char *projname)
{
	struct proj_entity *proj;

	s_list_for_each(proj, list)
		if (strncmp(projname, proj->projname,
			(size_t)PROJNAME_MAX) == 0)
			break;

	return (proj);
}
