/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef __PROC_FS_H
#define	__PROC_FS_H

#pragma ident	"@(#)proc_fs.h	1.2	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <procfs.h>		/* psinfo_t */
#include <dirent.h>		/* DIR */

#include <sys/types.h>		/* pid_t, size_t */

extern DIR	*proc_fs_open(void);
extern void	proc_fs_close(DIR *);
extern int	proc_fs_read_one(DIR *, pid_t *);
extern int	proc_fs_get_info(pid_t, psinfo_t *);
extern int	proc_fs_get_status(pid_t, pstatus_t *);
extern int	proc_fs_get_usage(pid_t, prusage_t *);
extern int	proc_fs_get_xmap(pid_t, prxmap_t **);
extern void	proc_fs_get_path_to_map(pid_t, const char *, char *, size_t);

#ifdef __cplusplus
}
#endif

#endif /* __PROC_FS_H */
