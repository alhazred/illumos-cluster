/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)libzonedevsensor.c	1.3	08/05/20 SMI"

#include <time.h>
#include <stdio.h>
#include <ctype.h>		/* isdigit */
#include <errno.h>
#include <fcntl.h>
#include <kstat.h>
#include <unistd.h>
#include <stddef.h>		/* offsetof */
#include <assert.h>
#include <stdlib.h>		/* atoi */
#include <string.h>		/* strerror, */
#include <strings.h>		/* strstr, */
#include <zone.h>		/* zone_getattr */
#include <pool.h>		/* libpool */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>		/* hrtime_t */
#include <sys/unistd.h>		/* sysconf */
#include <sys/pset.h>		/* pset_getloadavg */
#include <sys/loadavg.h>	/* LOADAVG_1MIN, */

#include <sys/sol_version.h>	/* SOL_VERSION */

#include "sensors.h"
#include "common.h"
#include "debug.h"
#include "s_list.h"

/*
 * Zone Device Sensor.
 */

#define	STANDALONE 0

#if !DEBUG
#if STANDALONE
#error STANDALONE should not be defined in non-debug mode
#endif
#endif

#ifndef SOL_VERSION
#error SOL_VERSION is undefined
#endif

#if SOL_VERSION < __s10
#error cannot compile on Solaris versions lower than 10
#endif

#if (ZONENAME_MAX > SENSOR_MNG_OBJ_MAX)
#error SENSOR_MNG_OBJ_MAX in sensors.h is too small
#endif

/* debug level */
int _dbg_flgs = DBG_1;

/*
 * Key Indicator.
 */

#define	KI_ID_CPU_IDLE		0
#define	KI_ID_CPU_IOWAIT	1
#define	KI_ID_CPU_USED		2

#define	KI_TYPE_CPU_FRAC	1
#define	KI_TYPE_LOADAVG		2

struct key_indicator {
	char name[SENSOR_KI_TYPE_MAX];
	int type;
	int id;
	int active;
	char *rec_names[2]; /* needed kstat record names */
};

static struct key_indicator ki_tab[] =
{
	{"ki.sunw.cpu.idle", KI_TYPE_CPU_FRAC, KI_ID_CPU_IDLE, 0,
		{ "cpu_ticks_idle", ""}},

	{"ki.sunw.cpu.iowait", KI_TYPE_CPU_FRAC, KI_ID_CPU_IOWAIT, 0,
		{ "cpu_ticks_wait", ""}},

	{"ki.sunw.cpu.used", KI_TYPE_CPU_FRAC, KI_ID_CPU_USED, 0,
		{ "cpu_ticks_user", "cpu_ticks_kernel" }},

	{"ki.sunw.cpu.loadavg.1mn", KI_TYPE_LOADAVG, LOADAVG_1MIN, 0,
		{ "", "" }},

	{"ki.sunw.cpu.loadavg.5mn", KI_TYPE_LOADAVG, LOADAVG_5MIN, 0,
		{ "", "" }},

	{"ki.sunw.cpu.loadavg.15mn", KI_TYPE_LOADAVG, LOADAVG_15MIN, 0,
		{ "", "" }},

	{"fake", -1, -1, -1,
		{ "", "" }} /* tab end mark */
};

#define	ki_for_each(ki)\
	for (ki = &ki_tab[0]; ki->type != -1; ki++)

#define	N_KI ((int)(sizeof (ki_tab) / sizeof (struct key_indicator)))

/*
 * CPU device
 */

#define	KSTAT_TICKS_MODULE	"cpu"
#define	KSTAT_TICKS_NAME	"sys"
#define	KSTAT_TICKS_NAMELEN	(KSTAT_STRLEN)

struct cpu_statistics {
	uint64_t usage;
	uint64_t prev_ticks;
	int64_t prev_snaptime;
};

#define	CPU_STATE_ONLINE	(1 << 0)
#define	CPU_STATE_TICKS_UPDATED	(1 << 1)
#define	CPU_STATE_USAGE_UPDATED	(1 << 2)

struct cpu_dev {
	s_list_node_t node;
	int64_t id;
	unsigned int state;
	kstat_t *ksp;
	struct cpu_statistics stats[N_KI];
};

/*
 * Managed Objects.
 */

static const char *poolid_prop = "pool.sys_id";
static const char *psetid_prop = "pset.sys_id";
static const char *cpuid_prop = "cpu.sys_id";

struct zone_mo {
	s_list_node_t node;
	char name[SENSOR_MNG_OBJ_MAX];
	pool_resource_t *res;

	uint64_t usage[N_KI];
	int usage_available;
	unsigned int n_cpu;

	uint64_t loadavg[3];
};

typedef enum __passover_type_enum {
	POOL,
	RES,
	COMP
} passover_type_t;

struct passover {
	passover_type_t type;

	union {
		struct {
			pool_t *p;
			pool_t **pp;
			poolid_t poolid;
		} pool;

		struct {
			pool_resource_t *r;
			struct zone_mo *zone;
		} res;

		struct zone_mo *zone;
	} u;
};

/*
 * Global variables.
 */

static unsigned int	n_active_ki = 0;	/* # enabled KIs */
static s_list_t		cpu_list;		/* cpu list */
static kstat_ctl_t	*kc = NULL;		/* kstat control pointer */
static pool_conf_t	*pool_conf = NULL;	/* pool conf pointer */
static int		hz = 0;			/* # ticks per second */

/*
 * Functions definitions.
 */

static struct cpu_dev *cpu_zalloc(void);
static void cpu_destroy(struct cpu_dev *);
static struct cpu_dev *cpu_create(int64_t);
static void cpu_insert(struct cpu_dev *);
static void cpu_remove(struct cpu_dev *);
static struct cpu_dev *cpu_lookup(int64_t);
static int cpu_update_stats(struct cpu_dev *);
static void cpu_list_create(void);
static void cpu_list_destroy(void);
static int cpu_list_update_stats(void);
static struct zone_mo *zone_mo_zalloc(void);
static void zone_mo_free(struct zone_mo *);
static struct zone_mo *__zone_mo_create(zoneid_t);
static struct zone_mo *zone_mo_create(zoneid_t);
static void zone_mo_destroy(struct zone_mo *);
static void zone_mo_insert(s_list_t *, struct zone_mo *);
static void zone_mo_remove(s_list_t *, struct zone_mo *);
static void zone_mo_list_create(s_list_t *);
static void zone_mo_list_empty(s_list_t *);
static void zone_mo_list_destroy(s_list_t *);
static int zone_mo_list_populate(s_list_t *);
static int property_callback(pool_conf_t *, pool_elem_t *, const char *,
	pool_value_t *, void *);
static int component_callback(pool_conf_t *, pool_component_t *, void *);
static int resource_callback(pool_conf_t *, pool_resource_t *, void *);
static int pool_callback(pool_conf_t *, pool_t *, void *);
static pool_t *lookuppool(poolid_t);
static void cpy_error_str(char[]);
static int insert_result(sensor_result_t **, int *, timespec_t,
	struct key_indicator *, const char *, uint64_t, uint64_t);

#define	cpu_list_for_each(cpu) s_list_for_each(cpu, &cpu_list)


/*
 * --- API functions ---
 */

/*
 * start(ki_name, error) is used to register the KI ki_name within the sensor.
 * It returns 0 on success, and 1 with an error messsage in the string error on
 * failure.
 */
int
start(char ki_name[], char error[])
{
	int gotit = 0;
	struct key_indicator *ki;

	unset_error_str();

	ki_for_each(ki) {
		if (strncmp(ki->name, ki_name, SENSOR_KI_TYPE_MAX) == 0) {
			gotit = 1;
			break;
		}
	}

	if (!gotit) {
		set_error_str("KI %s isn't supported", ki_name);
		cpy_error_str(error);
		return (1);
	}


	if (ki->active == 0 && n_active_ki++ == 0) {

		/*
		 * Sensor initialization.
		 */

		cpu_list_create();

		DBG(DL5, ("up\n"));

	}

	ki->active = 1;

	return (0);
}

/*
 * stop(ki_name, error) is used to unregister the KI ki_name within the sensor.
 * It returns 0 on success, and 1 with an error messsage in the string error on
 * failure.
 */
int
stop(char ki_name[], char error[])
{
	int gotit = 0;
	struct key_indicator *ki;

	unset_error_str();

	ki_for_each(ki) {
		if (strncmp(ki->name, ki_name, SENSOR_KI_TYPE_MAX) == 0) {
			gotit = 1;
			break;
		}
	}

	if (!gotit) {
		set_error_str("KI %s isn't supported", ki_name);
		cpy_error_str(error);
		return (1);
	}

	if (ki->active == 1 && --n_active_ki == 0) {

		/*
		 * Sensor finalization.
		 */

		cpu_list_destroy();

		DBG(DL5, ("down\n"));
	}

	ki->active = 0;

	return (0);
}

/*
 * get_values(mobjs, n_mobjs, results, n_results, error) place CPU accounting
 * values in table results for the registered KIs and for the Managed Objects
 * given in mobjs. It returns 0 on success, and 1 with an error message in the
 * string error on failure.
 */

/* ARGSUSED0,ARGSUSED1 */
int
get_values(char **mobjs, int n_mobjs,
	sensor_result_t **results, int *n_results, char error[])
{
	int n_res;
	s_list_t list;
	struct timespec tp;
	sensor_result_t *res;
	struct key_indicator *ki;

	unset_error_str();

	/*
	 * Preliminary checks.
	 */

	if (n_active_ki == 0) {
		DBG(DL1, ("No KI enabled\n"));
		*n_results = 0;
		return (0);
	}

	if (mobjs != NULL) {
		set_error_str(__ULAYER_BUG);
		cpy_error_str(error);
		*n_results = 0;
		return (1);
	}

	/*
	 * Initialization.
	 */

	if (clock_gettime(CLOCK_REALTIME, &tp) < 0) {
		/*lint -e746 */
		set_error_str("clock_gettime failed (%s)", strerror(errno));
		/*lint +e746 */
		cpy_error_str(error);
		return (1);
	}

	if ((kc = kstat_open()) == NULL) {
		set_error_str("kstat_open failed (%s)", strerror(errno));
		cpy_error_str(error);
		return (1);
	}

	if ((pool_conf = pool_conf_alloc()) == NULL) {
		set_error_str("pool_conf_alloc failed (%s)",
			pool_strerror(pool_error()));
		cpy_error_str(error);
		(void) kstat_close(kc);
		return (1);
	}

	if (pool_conf_open(pool_conf, pool_dynamic_location(), PO_RDONLY)
		!= PO_SUCCESS) {

		set_error_str("pool_conf_open failed (%s)",
			pool_strerror(pool_error()));
		cpy_error_str(error);
		(void) kstat_close(kc);
		return (1);
	}

	if ((hz == 0) && ((hz = sysconf(_SC_CLK_TCK)) == -1)) {
		set_error_str("sysconf failed (%s)", strerror(errno));
		cpy_error_str(error);
		(void) pool_conf_close(pool_conf);
		(void) kstat_close(kc);
		return (1);
	}
	assert(hz != 0);

	/*
	 * Update CPU statistics.
	 */

	if (cpu_list_update_stats() < 0) {
		cpy_error_str(error);
		(void) pool_conf_close(pool_conf);
		(void) kstat_close(kc);
		return (1);
	}

	/*
	 * Create managed object lists and populate it with resource usage
	 * values.
	 */

	zone_mo_list_create(&list);

	if (zone_mo_list_populate(&list) < 0) {
		cpy_error_str(error);
		(void) pool_conf_close(pool_conf);
		(void) kstat_close(kc);
		return (1);
	}

	/*
	 * Fill in the result table.
	 */

	res = *results;
	n_res = 0;

	ki_for_each(ki) {
		struct zone_mo *zone;

		if (ki->active == 0)
			continue;

		s_list_for_each(zone, &list) {
			uint64_t value = (uint64_t)0;
			uint64_t upperbound = (uint64_t)0;

			switch (ki->type) {
			case KI_TYPE_LOADAVG:
				value = zone->loadavg[ki->id];
				break;
			case KI_TYPE_CPU_FRAC:
				if (!zone->usage_available)
					continue;
				value = zone->usage[ki->id];
				upperbound = (uint64_t)(zone->n_cpu * 100U);
				break;
			default:
				set_error_str(__SENSOR_BUG);
				cpy_error_str(error);
				(void) kstat_close(kc);
				(void) pool_conf_close(pool_conf);
				return (1);
			}

			if (insert_result(&res, &n_res, tp, ki, zone->name,
				value, upperbound) < 0) {

				cpy_error_str(error);
				zone_mo_list_destroy(&list);
				(void) kstat_close(kc);
				(void) pool_conf_close(pool_conf);
				return (1);
			}
		}
	}

	zone_mo_list_destroy(&list);
	(void) pool_conf_close(pool_conf);
	(void) kstat_close(kc);

	/* update caller's pointers */
	*results = res;
	*n_results = n_res;

	return (0);
}


/*
 * --- cpu_device related functions ---
 */

/*
 * cpu_zalloc() allocates a cpu_device object and places zeros into it.
 */
static struct cpu_dev *
cpu_zalloc(void)
{
	struct cpu_dev *cpu;

	if ((cpu = (struct cpu_dev *)malloc(sizeof (*cpu))) == NULL) {
		set_error_str("malloc failed (%s)", strerror(errno));
		return (NULL);
	}

	(void) memset(cpu, 0, sizeof (*cpu));

	return (cpu);
}

/*
 * cpu_destroy(cpu) frees the memory resources associated to the cpu_device
 * object cpu.
 */
static void
cpu_destroy(struct cpu_dev *cpu)
{
	DBG(DL4, ("destroying cpu %lld\n", cpu->id));
	free(cpu);
}

/*
 * cpu_create(id) creates a cpu_dev object identified by id. The created cpu_dev
 * object is returned on success, and NULL on failure.
 */
static struct cpu_dev *
cpu_create(int64_t id)
{
	struct cpu_dev *cpu;

	DBG(DL4, ("creating cpu %lld\n", id));

	if ((cpu = cpu_zalloc()) == NULL)
		return (NULL);

	cpu->id = id;

	return (cpu);
}

/*
 * cpu_update_stats(cpu) reads the kstat values associated to the object cpu,
 * and updates its usage statistics and state. 0 is returned on success, and -1
 * on failure.
 */
static int
cpu_update_stats(struct cpu_dev *cpu)
{
	kstat_t *ksp = cpu->ksp;
	int usage_updated;
	int64_t curr_snaptime;
	struct key_indicator *ki;
	int ticks_previously_updated;

	assert((cpu->state & CPU_STATE_ONLINE) != 0);

	usage_updated = 0;
	curr_snaptime = ksp->ks_snaptime;
	ticks_previously_updated = cpu->state & CPU_STATE_TICKS_UPDATED;

	ki_for_each(ki) {
		int i;
		struct cpu_statistics *stats;
		uint64_t curr_ticks = (uint64_t)0;

		if (ki->type != KI_TYPE_CPU_FRAC)
			continue;

		for (i = 0; i < 2; i++) {
			kstat_named_t *knp;
			char *krn = ki->rec_names[i];

			if (strlen(krn) == 0)
				continue;

			if ((knp = (kstat_named_t *)kstat_data_lookup(ksp, krn))
				== NULL) {

				cpu->state &= ~CPU_STATE_TICKS_UPDATED;
				set_error_str(__SENSOR_BUG);
				return (-1);
			}

			if (knp->data_type != KSTAT_DATA_UINT64) {
				cpu->state &= ~CPU_STATE_TICKS_UPDATED;
				set_error_str(__SENSOR_BUG);
				return (-1);
			}

			curr_ticks += knp->value.ui64;
		}

		stats = &cpu->stats[ki->id];

		if (ticks_previously_updated) {
			int64_t prev_snaptime = stats->prev_snaptime;
			uint64_t prev_ticks = stats->prev_ticks;

			if (curr_snaptime - prev_snaptime > (int64_t)0) {
				uint64_t ns;

				ns = (uint64_t)((curr_ticks - prev_ticks) *
					NANOSEC * 100 / (uint_t)hz);

				stats->usage = ns / (uint64_t)(curr_snaptime -
					prev_snaptime);

				usage_updated = 1;
			}
		}

		stats->prev_snaptime = curr_snaptime;
		stats->prev_ticks = curr_ticks;
	}

	cpu->state |= CPU_STATE_TICKS_UPDATED;
	if (usage_updated)
		cpu->state |= CPU_STATE_USAGE_UPDATED;

	return (0);
}

/*
 * cpu_insert(cpu) inserts the cpu_dev object cpu into the global cpu_dev list
 * cpu_list.
 */
static void
cpu_insert(struct cpu_dev *cpu)
{
	s_list_insert_tail(&cpu_list, cpu);
}

/*
 * cpu_remove(cpu) removes the cpu_dev object cpu from the global cpu_dev list
 * cpu_list.
 */
static void
cpu_remove(struct cpu_dev *cpu)
{
	s_list_remove(&cpu_list, cpu);
}

/*
 * cpu_list(id) looks up the cpu object idendified by id in the global cpu_dev
 * list cpu_list.
 */
static struct cpu_dev *
cpu_lookup(int64_t id)
{
	struct cpu_dev *cpu = s_list_head(&cpu_list);

	while (cpu != NULL) {
		if (cpu->id == id)
			break;
		cpu = s_list_next(&cpu_list, cpu);
	}

	return (cpu);
}

/*
 * cpu_list_creates() creates the global cpu_dev list cpu_list.
 */
static void
cpu_list_create(void)
{
	/*lint -e413 */
	s_list_create(&cpu_list, sizeof (struct cpu_dev),
		offsetof(struct cpu_dev, node));
	/*lint +e413 */
}

/*
 * cpu_list_empty() removes and destroys all the objects contained in the
 * cpu_list. After calling this function list is empty but not destroyed.
 */
static void
cpu_list_empty(void)
{
	struct cpu_dev *cpu = s_list_head(&cpu_list);

	while (cpu != NULL) {
		struct cpu_dev *next;

		next = s_list_next(&cpu_list, cpu);
		cpu_remove(cpu);
		cpu_destroy(cpu);
		cpu = next;
	}
}

/*
 * cpu_list_destroy() empties and destroys cpu_list.
 */
static void
cpu_list_destroy(void)
{
	cpu_list_empty();
	s_list_destroy(&cpu_list);
}

/*
 * cpu_list_update_stats() updates the usage statistics of all cpu_dev objects
 * in the global cpu_dev list cpu_list.
 */
static int
cpu_list_update_stats(void)
{
	kstat_t *ksp;
	struct cpu_dev *cpu;

	/* get prepared */
	cpu_list_for_each(cpu) {
		cpu->state &= ~CPU_STATE_ONLINE;
		cpu->state &= ~CPU_STATE_USAGE_UPDATED;
	}

	/* update cpu list */
	for (ksp = kc->kc_chain; ksp != NULL; ksp = ksp->ks_next) {
		int64_t cpuid;

		if (strcmp(ksp->ks_module, KSTAT_TICKS_MODULE) != 0)
			continue;

		if (strncmp(ksp->ks_name, KSTAT_TICKS_NAME,
			KSTAT_TICKS_NAMELEN) != 0)
			continue;

		if (kstat_read(kc, ksp, NULL) < 0)
			continue;

		cpuid = (int64_t)ksp->ks_instance;

		if ((cpu = cpu_lookup(cpuid)) == NULL) {
			if ((cpu = cpu_create(cpuid)) == NULL)
				return (-1);
			cpu_insert(cpu);
		}

		cpu->ksp = ksp;
		cpu->state |= CPU_STATE_ONLINE;
	}

	/* get cpu statistics */
	cpu_list_for_each(cpu) {

		if ((cpu->state & CPU_STATE_ONLINE) == 0) {
			/* cpu isn't in the kstat list - it's offline */
			continue;
		}

		if (cpu_update_stats(cpu) < 0)
			return (-1);
	}

	return (0);
}

/*
 * Zone functions.
 */

/*
 * zone_mo_zalloc() allocates a zone monitored object (zone_mo) and zeros it.
 */
static struct zone_mo *
zone_mo_zalloc(void)
{
	struct zone_mo *zone;

	if ((zone = (struct zone_mo *)malloc(sizeof (*zone))) == NULL) {
		set_error_str("malloc failed (%s)", strerror(errno));
		return (NULL);
	}

	(void) memset(zone, 0, sizeof (*zone));

	return (zone);
}

/*
 * zone_mo_free(zone) frees the memory associated to the zone_mo object zone.
 */
static void
zone_mo_free(struct zone_mo *zone)
{
	free(zone);
}

/*
 * __zone_mo_create(zoneid) creates a zone_mo object identified by zoneid. The
 * Solaris zone function getzonenamebyid() is called so that the zone object can
 * be later identified by its name. The zone_mo object is returned on success,
 * and NULL is returned on failure.
 */
static struct zone_mo *
__zone_mo_create(zoneid_t zoneid)
{
	struct zone_mo *zone;

	if ((zone = zone_mo_zalloc()) == NULL)
		return (NULL);

	if (getzonenamebyid(zoneid, zone->name, SENSOR_MNG_OBJ_MAX) < -1) {
		set_error_str("getzonenamebyid failed (%s)", strerror(errno));
		zone_mo_free(zone);
		return (NULL);
	}

	return (zone);
}

/*
 * zone_mo_create(zoneid) looks up the resource pool bound to the zone
 * identified by zoneid, calls __zone_mo_create(zoneid) to create the zone_mo
 * object, walks the pool resources to retrieve the statistics associated to the
 * pool's pset, and finally walks the pset components to retrieve the statistics
 * associated to the pset's cpus. The zone_mo object is returned on success, and
 * NULL is returned on failure.
 */
static struct zone_mo *
zone_mo_create(zoneid_t zoneid)
{
	pool_t *pool;
	poolid_t poolid;
	struct zone_mo *zone;

	if (zone_getattr(zoneid, ZONE_ATTR_POOLID,
		&poolid, sizeof (poolid)) < (ssize_t)0) {

		set_error_str("zone_getattr failed (%s)", strerror(errno));
		return (NULL);
	}

	if ((pool = lookuppool(poolid)) == NULL)
		return (NULL);

	if ((zone = __zone_mo_create(zoneid)) == NULL)
		return (NULL);

	if (pool_walk_resources(pool_conf, pool, zone, resource_callback) < 0) {
		if (!error_str_is_set()) {
			set_error_str("pool_walk_resources: %s\n",
				pool_strerror(pool_error()));
		}
		zone_mo_destroy(zone);
		return (NULL);
	}

	if (pool_walk_components(pool_conf, zone->res, zone,
		component_callback) < 0) {

		if (!error_str_is_set()) {
			set_error_str("pool_walk_components: %s\n",
				pool_strerror(pool_error()));
		}
		zone_mo_destroy(zone);
		return (NULL);
	}

	return (zone);
}

/*
 * zone_mo_destroy(zone) destroys the zone_mo object pointed to by zone.
 */
static void
zone_mo_destroy(struct zone_mo *zone)
{
	zone_mo_free(zone);
}

/*
 * zone_mo_insert(list, zone) inserts the zone_mo object zone into the zone list
 * list.
 */
static void
zone_mo_insert(s_list_t *list, struct zone_mo *zone)
{
	s_list_insert_tail(list, zone);
}

/*
 * zone_mo_remove(list, zone) removes the zone_mo object zone from the zone list
 * list.
 */
static void
zone_mo_remove(s_list_t *list, struct zone_mo *zone)
{
	s_list_remove(list, zone);
}

/*
 * zone_mo_list_create(list) creates a zone list using the generic list pointed
 * to by list.
 */
static void
zone_mo_list_create(s_list_t *list)
{
	/*lint -e413 */
	s_list_create(list, sizeof (struct zone_mo),
		offsetof(struct zone_mo, node));
	/*lint +e413 */
}

/*
 * zone_mo_list_empty(list) removes and destroys the zone_mo objects in the zone
 * list pointed to by list.
 */
static void
zone_mo_list_empty(s_list_t *list)
{
	struct zone_mo *zone = s_list_head(list);

	while (zone != NULL) {
		struct zone_mo *next;

		next = s_list_next(list, zone);
		zone_mo_remove(list, zone);
		zone_mo_destroy(zone);
		zone = next;
	}
}

/*
 * zone_mo_list_destroy(list) empties and destroys the zone list pointed to by
 * list.
 */
static void
zone_mo_list_destroy(s_list_t *list)
{
	zone_mo_list_empty(list);
	s_list_destroy(list);
}

/*
 * zone_mo_list_populate(list) uses Solaris zone_list() func to retrieve
 * all zones configured/booted on the system, and creates a zone_mo object for
 * each. 0 is returned on success, -1 on failure.
 */
static int
zone_mo_list_populate(s_list_t *list)
{
	uint_t i;
	zoneid_t *zids;
	uint_t nzents, nzents_saved;

	if (zone_list(NULL, &nzents) != 0) {
		set_error_str("zone_list failed (%s)",
			strerror(errno));
		return (-1);
	}

again:
	if (nzents == 0) {
		/* there should be at least one zone: the global zone */
		set_error_str(__SENSOR_BUG);
		return (-1);
	}

	if ((zids = (zoneid_t *)malloc(nzents * sizeof (zoneid_t))) == NULL) {
		set_error_str("malloc failed (%s)", strerror(errno));
		return (-1);
	}

	nzents_saved = nzents;

	if (zone_list(zids, &nzents) != 0) {
		set_error_str("zone_list failed (%s)",
			strerror(errno));
		free(zids);
		return (-1);
	}

	if (nzents != nzents_saved) {
		free(zids);
		goto again;
	}

	for (i = 0; i < nzents; i++) {
		struct zone_mo *zone;

		if ((zone = zone_mo_create(zids[i])) == NULL) {
			free(zids);
			return (-1);
		}

		zone_mo_insert(list, zone);
	}


	free(zids);
	return (0);
}

/*
 * libpool manipulation functions.
 */

/*
 * property_callback(conf, element, name, val, arg) is the callback used with
 * the libpool property walker. It behaves differently based on the property
 * type. 0 is returned on success, -1 on failure.
 */
/*ARGSUSED1*/
static int
property_callback(pool_conf_t *conf, pool_elem_t *elem, const char *name,
	pool_value_t *val, void *arg)
{
	struct passover *po = (struct passover *)arg;

	switch (po->type) {
	case POOL:
		if (strncmp(name, poolid_prop, strlen(poolid_prop)) == 0) {
			int64_t id;

			/*
			 * We got a pool id property.
			 */

			if (pool_value_get_int64(val, &id) < 0) {
				set_error_str("pool_value_get_int64 failed"
					" (%s)", pool_strerror(pool_error()));
				return (-1);
			}

			if (po->u.pool.poolid == id)
				/*
				 * The poolid corresponds to what caller is
				 * looking for.
				 */
				*po->u.pool.pp = po->u.pool.p;
		}
		break;
	case RES:
		if (strncmp(name, psetid_prop, strlen(psetid_prop)) == 0) {
			int i;
			int64_t id;
			psetid_t psetid;
			double loadavg[3];
			struct zone_mo *zone = po->u.res.zone;

			/*
			 * We got a pset id property.
			 */

			/* stash a reference to the pset resource */
			zone->res = po->u.res.r;

			/* get the pset id */
			if (pool_value_get_int64(val, &id) < 0) {
				set_error_str("(pool_value_get_int64 failed"
					" (%s)", pool_strerror(pool_error()));
				return (-1);
			}

			/* get pset cpu load average */
			if (id == (int64_t)-1) /* default pset */
				psetid = PS_NONE;
			else
				psetid = (psetid_t)id;

			if (pset_getloadavg(psetid, loadavg, 3) == -1) {
				set_error_str(__SENSOR_BUG);
				return (-1);
			}

			/* set the zone load average values */
			for (i = 0; i < 3; i++)
				zone->loadavg[i] = (uint64_t)(loadavg[i] *
					(double)1000);
		}
		break;
	case COMP:
		if (strncmp(name, cpuid_prop, strlen(cpuid_prop)) == 0) {
			int i;
			int64_t id;
			struct cpu_dev *cpu;
			struct zone_mo *zone = po->u.zone;

			/*
			 * We got a cpu id property.
			 */

			/* get the cpu id */
			if (pool_value_get_int64(val, &id) < 0) {
				set_error_str("pool_value_get_int64 failed"
					" (%s)", pool_strerror(pool_error()));
				return (-1);
			}

			/* look up the cpu object associated to id */
			if (((cpu = cpu_lookup(id)) == NULL) ||
				((cpu->state & CPU_STATE_ONLINE) == 0)) {

				/*
				 * That CPU is currently invisible/offline.
				 * Let's just skip it.
				 */

				break;
			}

			/* update zone cpu usage */

			if ((cpu->state & CPU_STATE_USAGE_UPDATED) == 0) {

				/*
				 * Cannot report usage for that zone.
				 */

				zone->usage_available = 0;
				break;
			}

			zone->usage_available = 1;
			zone->n_cpu++;

			for (i = 0; i < N_KI; i++)
				zone->usage[i] += cpu->stats[i].usage;
		}
		break;
	default:
		set_error_str(__SENSOR_BUG);
		return (-1);
	}

	return (0);
}

/*
 * component_callback(conf, comp, arg) is the callback used with the libpool
 * component walker. 0 is returned on success, -1 on failure.
 */
static int
component_callback(pool_conf_t *conf, pool_component_t *comp, void *arg)
{
	pool_elem_t *elem;
	struct passover po;

	if ((elem = pool_component_to_elem(conf, comp)) == NULL) {
		set_error_str("pool_component_to_elem: %s\n",
			pool_strerror(pool_error()));
		return (-1);
	}

	po.type = COMP;
	po.u.zone = (struct zone_mo *)arg;

	if (pool_walk_properties(conf, elem, &po, property_callback) < 0) {
		if (!error_str_is_set()) {
			set_error_str("pool_walk_properties: %s\n",
				pool_strerror(pool_error()));
		}
		return (-1);
	}

	return (0);
}

/*
 * resource_callback(conf, res, arg) is the callback used with the libpool
 * resource walker. 0 is returned on success, -1 on failure.
 */
static int
resource_callback(pool_conf_t *conf, pool_resource_t *res, void *arg)
{
	pool_elem_t *elem;
	struct passover po;

	if ((elem = pool_resource_to_elem(conf, res)) == NULL) {
		set_error_str("pool_resource_to_elem: %s\n",
			pool_strerror(pool_error()));
		return (-1);
	}

	po.type = RES;
	po.u.res.r = res;
	po.u.res.zone = (struct zone_mo *)arg;

	if (pool_walk_properties(conf, elem, &po, property_callback) < 0) {
		if (!error_str_is_set()) {
			set_error_str("pool_walk_properties: %s\n",
				pool_strerror(pool_error()));
		}
		return (-1);
	}

	return (0);
}

/*
 * pool_callback(conf, pool, arg) is the callback used with the libpool pool
 * walker. 0 is returned on success, -1 on failure.
 */
static int
pool_callback(pool_conf_t *conf, pool_t *pool, void *arg)
{
	pool_elem_t *elem;
	struct passover *po = (struct passover *)arg;

	if ((elem = pool_to_elem(conf, pool)) == NULL) {
		set_error_str("pool_to_elem: %s\n",
			pool_strerror(pool_error()));
		return (-1);
	}

	assert(po->type == POOL);

	po->u.pool.p = pool;

	if (pool_walk_properties(conf, elem, po, property_callback) < 0) {
		if (!error_str_is_set()) {
			set_error_str("pool_walk_properties: %s\n",
				pool_strerror(pool_error()));
		}
		return (-1);
	}

	return (0);
}

/*
 * lookuppool(poolid) walks through the resource pools (using Solaris
 * pool_walk_pools func) looking for the pool identified by poolid. The pool_t
 * object is returned on success, and NULL on failure.
 */
static pool_t *
lookuppool(poolid_t poolid)
{
	pool_t *p;
	struct passover po;

	po.type = POOL;
	po.u.pool.pp = &p;
	po.u.pool.poolid = poolid;

	if (pool_walk_pools(pool_conf, &po, pool_callback) < 0) {
		if (!error_str_is_set()) {
			set_error_str("pool_walk_pools: %s\n",
				pool_strerror(pool_error()));
		}
		return (NULL);
	}

	return (p);
}

/*
 * --- Helper functions ---
 */

/*
 * cpy_error_str(in) copies the content of the string error in the string in,
 * and appends a \n to it.
 */
static void
cpy_error_str(char in[])
{
	(void) strncpy(in, get_error_str(), ERROR_STRING_MAX);
	(void) strncat(in, "\n", ERROR_STRING_MAX);
}

/*
 * insert_result(results, n_results, t, ki, mo, v, u) inserts the new result
 * represented by the fields t, ki, mo, v, and u into the results table.  This
 * function is responsible for allocating memory for the results table.
 */
static int
insert_result(sensor_result_t **results, int *n_results, timespec_t t,
	struct key_indicator *ki, const char *mo, uint64_t v, uint64_t u)
{
#define	BATCHCOUNT 16
	int n_res, idx;
	sensor_result_t *ptr, *res;
	static int n_allocated_res = 0;

	assert(n_allocated_res % BATCHCOUNT == 0);

	if (*results != NULL && n_allocated_res == 0) {
		set_error_str(__SENSOR_BUG);
		return (-1);
	}

	res = *results;
	n_res = *n_results;

	/*
	 * Allocate memory if needed.
	 */
	if (++n_res > n_allocated_res) {
		sensor_result_t *tmp;
		size_t len = (size_t)((n_allocated_res + BATCHCOUNT))
			* sizeof (*tmp);

		if ((tmp = (sensor_result_t *)realloc(res, len)) == NULL) {
			set_error_str("realloc failed (%s)", strerror(errno));
			return (-1);
		}
		res = tmp;
		n_allocated_res += BATCHCOUNT;
	}

	/*
	 * Copy result into results tab.
	 */
	idx = n_res - 1;
	ptr = &res[idx];

	(void) memcpy(&ptr->time_current, &t, sizeof (ptr->time_current));
	(void) strncpy(ptr->ki_type, ki->name, SENSOR_KI_TYPE_MAX);
	(void) strncpy(ptr->managed_object, mo, SENSOR_MNG_OBJ_MAX);
	ptr->usage = v;
	ptr->upperbound = u;

	/*
	 * Update caller's pointers.
	 */
	*results = res;
	*n_results = n_res;

	return (0);
#undef	BATCHCOUNT
}

#if STANDALONE
static void
results_dump(sensor_result_t *res, int n_res)
{
	int i;

	for (i = 0; i < n_res; i++) {
		DBG(DL1, ("%ld:%ld:%s:%s:%llu:%llu\n",
			res[i].time_current.tv_sec,
			res[i].time_current.tv_nsec,
			res[i].ki_type,
			res[i].managed_object,
			res[i].usage,
			res[i].upperbound));
	}
}

int
main(int argc, char *argv[])
{
	int i;
	int retval;
	int n_mobjs;
	char **mobjs;
	int n_results;
	int n_samplings;
	struct key_indicator *ki;
	char error[ERROR_STRING_MAX];
	sensor_result_t *results = NULL;

	if (argc < 2) {
		(void) fprintf(stderr, "usage: %s <num samplings> "
			"[<dev1> <dev2> ...]\n", argv[0]);
		return (1);
	}

	retval = 0;
	n_samplings = atoi(argv[1]);

	if ((n_mobjs = argc - 2) > 0) {

		if ((mobjs = (char **)malloc(n_mobjs * sizeof (char *)))
			== NULL) {

			(void) fprintf(stderr, "malloc failed (%s)",
				strerror(errno));
			n_mobjs = 0;
			retval = 1;
			goto out;
		}

		for (i = 0; i < n_mobjs; i++) {
			if ((mobjs[i] = (char *)malloc(SENSOR_MNG_OBJ_MAX))
				== NULL) {

				(void) fprintf(stderr, "malloc failed (%s)",
					strerror(errno));
				n_mobjs = i;
				retval = 1;
				goto out;
			}
		}

	} else {
		mobjs = NULL;
		n_mobjs = 0; /* I'm paranoid */
	}

	for (i = 0; i < n_mobjs; i++)
		(void) strncpy(mobjs[i], argv[i + 2], SENSOR_MNG_OBJ_MAX);

	/* start */
	ki_for_each(ki) {

		if (start(ki->name, error) != 0) {
			(void) fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}
	}

	/* get results */
	n_results = 0;

	for (i = 0; i < n_samplings; i++) {

		if (get_values(mobjs, n_mobjs, &results, &n_results, error)
			!= 0) {

			(void) fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}

		results_dump(results, n_results);

		(void) sleep(2);
	}

	/* stop */
	ki_for_each(ki) {

		if (stop(ki->name, error) != 0) {
			(void) fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}
	}

out:
	for (i = 0; i < n_mobjs; i++)
		free(mobjs[i]);

	free(mobjs);
	free(results);
	return (retval);
}
#endif
