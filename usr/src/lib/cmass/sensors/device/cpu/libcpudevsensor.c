/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)libcpudevsensor.c	1.3	08/05/20 SMI"

#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <kstat.h>
#include <unistd.h>
#include <stddef.h>		/* offsetof */
#include <assert.h>
#include <stdlib.h>		/* atoi */
#include <string.h>		/* strerror, */
#include <strings.h>		/* strstr, */
#include <libnvpair.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>		/* hrtime_t */
#include <sys/unistd.h>		/* sysconf */
#include <sys/sysinfo.h>	/* cpu_stat_t, */
#include <sys/loadavg.h>	/* getloadavg */

#include <sys/sol_version.h>	/* SOL_VERSION */

#include "sensors.h"
#include "common.h"
#include "debug.h"
#include "s_list.h"

/*
 * CPU Device Sensor
 *
 * This sensor is able to retrieve per-processor and system-wide CPU resource
 * data.
 *
 * Two types of CPU statistics is returned: CPU fractions, and load averages
 * (processor runqueue length). Returned CPU fraction values are multiplied by
 * 100, and returned load average values are returned by 1000, so that both
 * types of data can be expressed as integer values. The upper layer is in
 * charge of dividing the values by 100, and 1000.
 */

#define	STANDALONE 0

#if !DEBUG
#if STANDALONE
#error STANDALONE should not be defined in non-debug mode
#endif
#endif

/* debug level */
int _dbg_flgs = DBG_1;

#ifndef SOL_VERSION
#error SOL_VERSION is undefined
#endif

/*
 * CPU ticks kstat changed from Solaris 9 to Solaris 10, so this sensor contains
 * specific code to be able to deal with both operating systems.
 */
#if SOL_VERSION < __s10
#define	KSTAT_TICKS_MODULE	"cpu_stat"
#define	KSTAT_TICKS_NAME	"cpu_stat"
#define	KSTAT_TICKS_NAMELEN	8
#else
#define	KSTAT_TICKS_MODULE	"cpu"
#define	KSTAT_TICKS_NAME	"sys"
#define	KSTAT_TICKS_NAMELEN	(KSTAT_STRLEN)
#endif

struct cpu_dev {
	int id;
	s_list_node_t node;
	kstat_t	*ksp;
	nvlist_t *nvlp;
};

/*
 * Key Indicator.
 */


#define	KI_TYPE_CPU_FRAC	1
#define	KI_TYPE_LOADAVG		2

/*
 * The field pos in the key_indicator structure contains the required positions
 * in the kstat data buffer. It's useful only on Solaris 9 because the latter
 * doesn't offer named kstat records for CPU statistics.
 *
 * Solaris 10 offers named kstat records for CPU statistics. The field rec_names
 * contains the nane of the records.
 *
 * Certain Key Indicator values are the sum of two kstat record values. Those
 * need two positions in the kstat buffer, in the case of Solaris 9, or two
 * record names in the case of Solaris 10.
 */
struct key_indicator {
	char name[SENSOR_KI_TYPE_MAX];
	int type;
	int active;
	int pos[2];		/* Solaris 9 only */
	char *rec_names[2];	/* Solaris 10 only */
};

/*
 * Note: CPU_IDLE, CPU_WAIT, CPU_USER, and CPU_KERNEL defined in sys/sysinfo.h
 */
static struct key_indicator ki_tab[] =
{
	{"ki.sunw.cpu.loadavg.1mn", KI_TYPE_LOADAVG, 0,
		{ LOADAVG_1MIN, -1 },
		{ "", "" }},

	{"ki.sunw.cpu.loadavg.5mn", KI_TYPE_LOADAVG, 0,
		{ LOADAVG_5MIN, -1 },
		{ "", "" }},

	{"ki.sunw.cpu.loadavg.15mn", KI_TYPE_LOADAVG, 0,
		{ LOADAVG_15MIN, -1 },
		{ "", "" }},

	{"ki.sunw.cpu.idle", KI_TYPE_CPU_FRAC, 0,
		{ CPU_IDLE, -1},
		{ "cpu_ticks_idle", ""}},

	{"ki.sunw.cpu.iowait", KI_TYPE_CPU_FRAC, 0,
		{ CPU_WAIT, -1},
		{ "cpu_ticks_wait", ""}},

	{"ki.sunw.cpu.used", KI_TYPE_CPU_FRAC, 0,
		{ CPU_USER,  CPU_KERNEL},
		{ "cpu_ticks_user", "cpu_ticks_kernel" }},

	{"fake", -1, -1,
		{-1, -1},
		{ "", "" }} /* tab end mark */
};

#define	ki_for_each(ki)\
	for (ki = &ki_tab[0]; ki->type != -1; ki++)

static unsigned int	n_active_ki = 0;	/* # enabled KIs */
static s_list_t		cpu_list;		/* cpu list */
static kstat_ctl_t	*kc = NULL;		/* kstat control pointer */
static int		hz = 0;			/* # ticks per second */

static struct cpu_dev *cpu_zalloc(void);
static void cpu_destroy(struct cpu_dev *);
static struct cpu_dev *cpu_create(int);
static void cpu_insert(s_list_t *, struct cpu_dev *);
static void cpu_remove(s_list_t *, struct cpu_dev *);
static struct cpu_dev *cpu_lookup(s_list_t *, int);
static void cpu_s_list_create(s_list_t *);
static void cpu_s_list_destroy(s_list_t *);
static int getksval(struct cpu_dev *, struct key_indicator *, uint64_t *);
static int getkival(int, char *, uint64_t, int64_t, uint64_t *);
static int putval(nvlist_t *, char *, uint64_t);
static void cpy_error_str(char[]);
static int insert_result(sensor_result_t **, int *, timespec_t, char[], char[],
	uint64_t, unsigned int);


/*
 * --- API functions ---
 */

/*
 * start(ki_name, error) is used to register the KI ki_name within the sensor.
 * It returns 0 on success, and 1 with an error messsage in the string error on
 * failure.
 */
int
start(char ki_name[], char error[])
{
	int gotit = 0;
	struct key_indicator *ki;

	unset_error_str();

	ki_for_each(ki) {
		if (strncmp(ki->name, ki_name, SENSOR_KI_TYPE_MAX) == 0) {
			gotit = 1;
			break;
		}
	}

	if (!gotit) {
		set_error_str("KI %s isn't supported", ki_name);
		cpy_error_str(error);
		return (1);
	}


	if (ki->active == 0 && n_active_ki++ == 0) {

		/*
		 * Sensor initialization.
		 */

		cpu_s_list_create(&cpu_list);

		DBG(DL5, ("up\n"));

	}

	ki->active = 1;

	return (0);
}

/*
 * stop(ki_name, error) is used to unregister the KI ki_name within the sensor.
 * It returns 0 on success, and 1 with an error messsage in the string error on
 * failure.
 */
int
stop(char ki_name[], char error[])
{
	int gotit = 0;
	struct key_indicator *ki;

	unset_error_str();

	ki_for_each(ki) {
		if (strncmp(ki->name, ki_name, SENSOR_KI_TYPE_MAX) == 0) {
			gotit = 1;
			break;
		}
	}

	if (!gotit) {
		set_error_str("KI %s isn't supported", ki_name);
		cpy_error_str(error);
		return (1);
	}

	if (ki->active == 1 && --n_active_ki == 0) {

		/*
		 * Sensor finalization.
		 */

		cpu_s_list_destroy(&cpu_list);

		DBG(DL5, ("down\n"));
	}

	ki->active = 0;

	return (0);
}

/*
 * get_values(mobjs, n_mobjs, results, n_results, error) place CPU accounting
 * values in table results for the registered KIs and for the Managed Objects
 * given in mobjs. It returns 0 on success, and 1 with an error message in the
 * string error on failure.
 */
int
get_values(char **mobjs, int n_mobjs,
	sensor_result_t **results, int *n_results, char error[])
{
	int n_res;
	kstat_t *ksp;
	s_list_t list;		/* (local) cpu device list */
	char *mo_name;		/* managed object name */
	double loadavg[3];
	struct timespec tp;
	sensor_result_t *res;
	struct cpu_dev *iter;	/* monitored object iterator */
	struct key_indicator *ki;
	unsigned int n_cpus = 0U;

	unset_error_str();

	if (n_active_ki == 0) {
		DBG(DL1, ("No KI enabled\n"));
		*n_results = 0;
		return (0);
	}

	/* sensor accepts one and only one managed object */
	if (mobjs == NULL || n_mobjs != 1) {
		set_error_str(__ULAYER_BUG);
		cpy_error_str(error);
		return (1);
	}
	mo_name = mobjs[0];

	if ((strlen(mo_name) + 1) > SENSOR_MNG_OBJ_MAX) {
		set_error_str(__ULAYER_BUG);
		cpy_error_str(error);
		return (1);
	}

	if (clock_gettime(CLOCK_REALTIME, &tp) < 0) {
		/*lint -e746 */
		set_error_str("clock_gettime failed (%s)", strerror(errno));
		/*lint +e746 */
		cpy_error_str(error);
		return (1);
	}

	if ((kc = kstat_open()) == NULL) {
		set_error_str("kstat_open failed (%s)", strerror(errno));
		cpy_error_str(error);
		return (1);
	}

	if ((hz == 0) && ((hz = sysconf(_SC_CLK_TCK)) == -1)) {
		set_error_str("sysconf failed (%s)", strerror(errno));
		cpy_error_str(error);
		return (1);
	}
	assert(hz != 0);

	cpu_s_list_create(&list);

	/*
	 * Build local cpu device list
	 */

	for (ksp = kc->kc_chain; ksp != NULL; ksp = ksp->ks_next) {
		struct cpu_dev *cpu;

		if (strcmp(ksp->ks_module, KSTAT_TICKS_MODULE) != 0)
			continue;

		if (strncmp(ksp->ks_name, KSTAT_TICKS_NAME,
			KSTAT_TICKS_NAMELEN) != 0)
			continue;

		/* get snapshot */
		if (kstat_read(kc, ksp, NULL) < 0) {

			/*
			 * kstat_read failed probably because the kernel
			 * doesn't have the appropriate kstat object.
			 * The kstat object most probably got destroyed
			 * from under us.
			 */

			DBG(DL1, ("kstat_read failed for %s (%s)\n",
				ksp->ks_name, strerror(errno)));

			continue;
		}

		if ((cpu = cpu_create(ksp->ks_instance)) == NULL) {
			cpy_error_str(error);
			cpu_s_list_destroy(&list);
			(void) kstat_close(kc);
			return (1);
		}

		cpu->ksp = ksp;
		cpu_insert(&list, cpu);

		n_cpus++;
	}

	/*
	 * Get instrumentation values.
	 */

	iter = s_list_head(&list);
	while (iter != NULL) {
		struct cpu_dev *next = s_list_next(&list, iter);

		assert(kc != NULL);
		assert(iter != NULL);
		assert(iter->ksp != NULL);

		DBG(DL2, ("cpu: %d\n", iter->id));

		ki_for_each(ki) {
			int ret;
			uint64_t ksval, kival;

			if (ki->active == 0 || (ki->type != KI_TYPE_CPU_FRAC))
				continue;

			/* get kstat value */
			if (getksval(iter, ki, &ksval) < 0) {
				cpy_error_str(error);
				cpu_s_list_destroy(&list);
				(void) kstat_close(kc);
				return (1);
			}

			/* get ki value */
			if ((ret = getkival(iter->id, ki->name, ksval,
				iter->ksp->ks_snaptime, &kival)) < 0) {

				cpy_error_str(error);
				cpu_s_list_destroy(&list);
				(void) kstat_close(kc);
				return (1);
			}

			if (ret > 0) /* no data available */
				continue;

			/*
			 * Store that KI/value pair for use in the "fill in the
			 * result table" section.
			 */
			if (putval(iter->nvlp, ki->name, kival) < 0) {
				cpy_error_str(error);
				cpu_s_list_destroy(&list);
				(void) kstat_close(kc);
				return (1);
			}
		}
		iter = next;
	}

	/*
	 * Fill in the result table.
	 */

	res = *results;
	n_res = 0;

	/*
	 * Call getloadavg() for KIs of type KI_TYPE_LOADAVG. Called
	 * whether there are KI_TYPE_LOADAVG KIs enabled or not. Could
	 * be optimized somehow.
	 */
	if (getloadavg(loadavg, 3) == -1) {
		set_error_str(__SENSOR_BUG);
		cpy_error_str(error);
		cpu_s_list_destroy(&list);
		(void) kstat_close(kc);
		return (1);
	}

	ki_for_each(ki) {
		uint64_t val = 0;
		boolean_t noinsert = B_FALSE;
		unsigned int ubound = 0L;

		if (ki->active == 0)
			continue;

		if (ki->type == KI_TYPE_LOADAVG) {

			/* value is multiplied by 1000 */
			val = (uint64_t)(loadavg[ki->pos[0]]
				* (double)1000);

		} else if (ki->type == KI_TYPE_CPU_FRAC) {

			ubound = n_cpus * 100U;

			s_list_for_each(iter, &list) {
				int err;
				uint64_t v;
				nvlist_t *nvlp = iter->nvlp;

				if ((err = nvlist_lookup_uint64(nvlp,
					ki->name, &v)) != 0) {

					if (err == ENOENT) {
						noinsert = B_TRUE;
						break;
					}

					set_error_str(__SENSOR_BUG);
					cpy_error_str(error);
					cpu_s_list_destroy(&list);
					(void) kstat_close(kc);
					return (1);

				}

				val += v;
			}
		} else {
			set_error_str(__SENSOR_BUG);
			cpy_error_str(error);
			cpu_s_list_destroy(&list);
			(void) kstat_close(kc);
			return (1);
		}

		if ((noinsert == B_FALSE) &&
			(insert_result(&res, &n_res, tp, ki->name,
				mo_name, val, ubound) < 0)) {

			cpy_error_str(error);
			cpu_s_list_destroy(&list);
			(void) kstat_close(kc);
			return (1);
		}
	}

	cpu_s_list_destroy(&list);
	(void) kstat_close(kc);

	/* update caller's pointers */
	*results = res;
	*n_results = n_res;

	return (0);
}


/*
 * --- cpu_device related functions ---
 */

/*
 * cpu_zalloc() allocates a cpu_device object and places zeros into it.
 */
static struct cpu_dev *
cpu_zalloc(void)
{
	struct cpu_dev *cpu;

	if ((cpu = (struct cpu_dev *)malloc(sizeof (*cpu))) == NULL) {
		set_error_str("malloc failed (%s)", strerror(errno));
		return (NULL);
	}

	(void) memset(cpu, 0, sizeof (*cpu));

	return (cpu);
}

/*
 * cpu_destroy(cpu) frees the memory resources associated to the cpu_device
 * object cpu.
 */
static void
cpu_destroy(struct cpu_dev *cpu)
{
	DBG(DL4, ("destroying cpu %d\n", cpu->id));

	nvlist_free(cpu->nvlp);
	free(cpu);
}

/*
 * cpu_crate(id) creates a cpu_device object identified by id
 */
static struct cpu_dev *
cpu_create(int id)
{
	nvlist_t *nvlp;
	struct cpu_dev *cpu;

	DBG(DL4, ("creating cpu %d\n", id));

	if ((cpu = cpu_zalloc()) == NULL)
		return (NULL);

	if (nvlist_alloc(&nvlp, NV_UNIQUE_NAME, 0) != 0) {
		set_error_str("nvlist_alloc failed (%s)", strerror(errno));
		free(cpu);
		return (NULL);
	}

	cpu->id = id;
	cpu->nvlp = nvlp;

	return (cpu);
}

/*
 * cpu_insert(list, cpu) inserts the cpu_dev object cpu into list.
 */
static void
cpu_insert(s_list_t *list, struct cpu_dev *cpu)
{
	s_list_insert_tail(list, cpu);
}

/*
 * cpu_remove(list, cpu) removes the cpu_dev object cpu from list.
 */
static void
cpu_remove(s_list_t *list, struct cpu_dev *cpu)
{
	s_list_remove(list, cpu);
}

/*
 * cpu_lookup(list, id) returns a reference to the cpu_dev identified by id in
 * list, and NULL if there's no such object into list.
 */
static struct cpu_dev *
cpu_lookup(s_list_t *list, int id)
{
	struct cpu_dev *cpu = s_list_head(list);

	while (cpu != NULL) {
		if (cpu->id == id)
			break;
		cpu = s_list_next(list, cpu);
	}

	return (cpu);
}

/*
 * cpu_s_list_creates(list) creates the list pointed to by list.
 */
static void
cpu_s_list_create(s_list_t *list)
{
	/*lint -e413 */
	s_list_create(list, sizeof (struct cpu_dev),
		offsetof(struct cpu_dev, node));
	/*lint +e413 */
}

/*
 * cpu_list_empty(list) removes and destroys all the objects contained in the
 * list referenced by list. After calling this function list is empty but not
 * destroyed.
 */
static void
cpu_list_empty(s_list_t *list)
{
	struct cpu_dev *cpu = s_list_head(list);

	while (cpu != NULL) {
		struct cpu_dev *next;

		next = s_list_next(list, cpu);
		cpu_remove(list, cpu);
		cpu_destroy(cpu);
		cpu = next;
	}
}

/*
 * cpu_s_list_destroy(list) empties the list referenced by list and destroys it.
 */
static void
cpu_s_list_destroy(s_list_t *list)
{
	cpu_list_empty(list);
	s_list_destroy(list);
}


/*
 * CPU statistics gathering functions.
 */

/*
 * getksval(cpu, ki, val) places into val the CPU statistics value associated to
 * the cpu_dev object cpu and the KI ki. If the the KI is associated with two
 * kstat records, the sum of the two kstat record values is returned into val.
 *
 * Two versions of that function: one for Solaris 9, which doesn't have named
 * kstat records, and one Solaris 10, which has named kstat records.
 */
#if SOL_VERSION < __s10
static int
getksval(struct cpu_dev *cpu, struct key_indicator *ki, uint64_t *val)
{
	int i;
	uint64_t v;
	cpu_stat_t *cs; /* defined in <sys/sysinfo.h> */

	cs = (cpu_stat_t *)cpu->ksp->ks_data;

	v = (uint64_t)0;
	for (i = 0; i < 2; i++) {
		int idx = ki->pos[i];

		(void) ki->rec_names[i]; /* noop, makes lint silent */

		if (idx >= 0)
			v += (uint64_t)cs->cpu_sysinfo.cpu[idx];
	}

	*val = v;

	return (0);
}
#else
static int
getksval(struct cpu_dev *cpu, struct key_indicator *ki, uint64_t *val)
{
	int i;
	uint64_t v;
	kstat_t *ksp = cpu->ksp;

	v = (uint64_t)0;
	for (i = 0; i < 2; i++) {
		kstat_named_t *knp;
		char *krn = ki->rec_names[i];

		(void) ki->pos[i]; /* noop, makes lint silent */

		if (strlen(krn) == 0)
			continue;

		if ((knp = (kstat_named_t *)kstat_data_lookup(ksp, krn))
			== NULL) {

			set_error_str(__SENSOR_BUG);
			return (-1);
		}

		if (knp->data_type != KSTAT_DATA_UINT64) {
			set_error_str(__SENSOR_BUG);
			return (-1);
		}

		v += knp->value.ui64;
	}

	*val = v;

	return (0);
}
#endif

/*
 * getkival(cpu_id, ki_name, ksval, snaptime, val) computes the KI value
 * associated to the cpu device cpu_id and the KI ki_name. The value is derived
 * from the kstat value ksval and the snapshot time snaptime, it is returned in
 * the variable pointed to by val. -1 is returned on failure, otherwise either 0
 * is returned if the data is available or 1 is returned if the data isn't
 * available.
 */
static int
getkival(int cpu_id, char *ki_name, uint64_t ksval, int64_t snaptime,
	uint64_t *val)
{
	int err;
	uint64_t ns;
	uint64_t new[2];
	struct cpu_dev *cpu;
	boolean_t inserted = B_FALSE;
	int64_t prev_snaptime = (int64_t)0;
	uint64_t prev_ksval = (uint64_t)0;

	if ((cpu = cpu_lookup(&cpu_list, cpu_id)) == NULL) {

		DBG(DL5, ("cpu %d not in cpu_list\n", cpu_id));

		if ((cpu = cpu_create(cpu_id)) == NULL)
			return (-1);

		cpu_insert(&cpu_list, cpu);
		inserted = B_TRUE;
	} else {
		uint_t n;
		uint64_t *old;

		DBG(DL5, ("read previous value for cpu %d\n", cpu->id));

		if ((err = nvlist_lookup_uint64_array(cpu->nvlp, ki_name, &old,
			&n)) == 0) {

			assert(n == (uint_t)2);

			prev_snaptime = (int64_t)old[0];
			prev_ksval = old[1];
		} else {
			switch (err) {
			case ENOENT:
				/* no entry named ki_name for that CPU */
				DBG(DL2, ("no %s prev value for cpu %d\n",
					ki_name, cpu->id));
				break;
			default:
				set_error_str(__SENSOR_BUG);
				return (-1);
			}
		}
	}

	new[0] = (uint64_t)snaptime;
	new[1] = ksval;

	if ((err = nvlist_add_uint64_array(cpu->nvlp, ki_name, new,
		(uint_t)2)) != 0) {

		switch (err) {
		case ENOMEM:
			set_error_str("nvlist_add_uint64_array failed (%s)",
				strerror(errno));
			break;
		default:
			set_error_str(__SENSOR_BUG);
		}

		if (inserted == B_TRUE)
			cpu_remove(&cpu_list, cpu);

		return (-1);
	}

	if ((prev_snaptime == (int64_t)0) ||
		(snaptime - prev_snaptime <= (int64_t)0)) {
		/* no data available */
		return (1);
	}

	ns = (uint64_t)((ksval - prev_ksval) * 1e9 * 100 / hz);
	*val = ns / (uint64_t)(snaptime - prev_snaptime);

	return (0);
}

/*
 * putval(nvlp, ki_name, val) places the pair (ki_name,val) in the name/value
 * pair list referenced by nvlp.
 */
static int
putval(nvlist_t *nvlp, char *ki_name, uint64_t val)
{
	int err;

	if ((err = nvlist_add_uint64(nvlp, ki_name, val)) != 0) {
		switch (err) {
		case ENOMEM:
			set_error_str("nvlist_add_uint64 failed (%s)",
				strerror(errno));
			break;
		default:
			set_error_str(__SENSOR_BUG);
		}

		return (-1);
	}

	return (0);
}

/*
 * --- Helper functions ---
 */

/*
 * cpy_error_str(in) copies the content of the string error in the string in,
 * and appends a \n to it.
 */
static void
cpy_error_str(char in[])
{
	(void) strncpy(in, get_error_str(), ERROR_STRING_MAX);
	(void) strncat(in, "\n", ERROR_STRING_MAX);
}

/*
 * insert_result(results, n_results, time, ki, mo, v, u) inserts the new result
 * represented by the fields time, ki, mo, v, and u into the results tabled.
 * This function is responsible for allocating memory for the results table.
 */
static int
insert_result(sensor_result_t **results, int *n_results, timespec_t t,
	char ki[], char *mo, uint64_t v, unsigned int u)
{
#define	BATCHCOUNT 16
	int n_res, idx;
	sensor_result_t *ptr, *res;
	static int n_allocated_res = 0;

	assert(n_allocated_res % BATCHCOUNT == 0);

	if (*results != NULL && n_allocated_res == 0) {
		set_error_str(__SENSOR_BUG);
		return (-1);
	}

	res = *results;
	n_res = *n_results;

	/*
	 * Allocate memory if needed.
	 */
	if (++n_res > n_allocated_res) {
		sensor_result_t *tmp;
		size_t len = (size_t)((n_allocated_res + BATCHCOUNT))
			* sizeof (*tmp);

		if ((tmp = (sensor_result_t *)realloc(res, len)) == NULL) {
			set_error_str("realloc failed (%s)", strerror(errno));
			return (-1);
		}
		res = tmp;
		n_allocated_res += BATCHCOUNT;
	}

	/*
	 * Copy result into results tab.
	 */
	idx = n_res - 1;
	ptr = &res[idx];

	(void) memcpy(&ptr->time_current, &t, sizeof (ptr->time_current));
	(void) strncpy(ptr->ki_type, ki, SENSOR_KI_TYPE_MAX);
	(void) strncpy(ptr->managed_object, mo, SENSOR_MNG_OBJ_MAX);
	ptr->usage = v;
	ptr->upperbound = (uint64_t)u;

	/*
	 * Update caller's pointers.
	 */
	*results = res;
	*n_results = n_res;

	return (0);
#undef	BATCHCOUNT
}

#if STANDALONE
static void
results_dump(sensor_result_t *res, int n_res)
{
	int i;

	for (i = 0; i < n_res; i++) {
		DBG(DL1, ("%ld:%ld:%s:%s:%llu\n",
			res[i].time_current.tv_sec,
			res[i].time_current.tv_nsec,
			res[i].ki_type,
			res[i].managed_object,
			res[i].usage));
	}
}

int
main(int argc, char *argv[])
{
	int i;
	int retval;
	int n_mobjs;
	char **mobjs;
	int n_results;
	int n_samplings;
	struct key_indicator *ki;
	char error[ERROR_STRING_MAX];
	sensor_result_t *results = NULL;

	if (argc < 2) {
		fprintf(stderr, "usage: %s <num samplings> "
			"[<dev1> <dev2> ...]\n", argv[0]);
		return (1);
	}

	retval = 0;
	n_samplings = atoi(argv[1]);

	if ((n_mobjs = argc - 2) > 0) {

		if ((mobjs = (char **)malloc(n_mobjs * sizeof (char *)))
			== NULL) {

			fprintf(stderr, "malloc failed (%s)", strerror(errno));
			n_mobjs = 0;
			retval = 1;
			goto out;
		}

		for (i = 0; i < n_mobjs; i++) {
			if ((mobjs[i] = (char *)malloc(SENSOR_MNG_OBJ_MAX))
				== NULL) {

				fprintf(stderr, "malloc failed (%s)",
					strerror(errno));
				n_mobjs = i;
				retval = 1;
				goto out;
			}
		}

	} else {
		mobjs = NULL;
		n_mobjs = 0; /* I'm paranoid */
	}

	for (i = 0; i < n_mobjs; i++)
		(void) strncpy(mobjs[i], argv[i + 2], SENSOR_MNG_OBJ_MAX);

	/* start */
	ki_for_each(ki) {

		if (start(ki->name, error) != 0) {
			fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}
	}

	/* get results */
	n_results = 0;

	for (i = 0; i < n_samplings; i++) {

		if (get_values(mobjs, n_mobjs, &results, &n_results, error)
			!= 0) {

			fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}

		results_dump(results, n_results);

		(void) sleep(1);
	}

	/* stop */
	ki_for_each(ki) {

		if (stop(ki->name, error) != 0) {
			fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}
	}

out:
	for (i = 0; i < n_mobjs; i++)
		free(mobjs[i]);

	free(mobjs);
	free(results);
	return (retval);
}
#endif
