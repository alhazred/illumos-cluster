/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)libmemdevsensor.c	1.2	08/05/20 SMI"

#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/time.h>	/* hrtime_t */
#include <sys/stat.h>	/* swapctl */
#include <sys/swap.h>	/* swapctl */

#include "sensors.h"
#include "common.h"
#include "debug.h"

/*
 * Memory Device Sensor
 *
 * This sensor retrieves physical memory and swap statistics for the system as a
 * whole.
 *
 * Note: the returned values are integer (uint64_t) values expressed in KBytes.
 */
#define	STANDALONE 0

#if !DEBUG
#if STANDALONE
#error STANDALONE should not be defined in non-debug mode
#endif
#endif

/*
 * Use sysconf instead of kstat.
 */
#define	MEMSENSOR_USE_SYSCONF

#ifndef MEMSENSOR_USE_SYSCONF
#include <kstat.h>
#endif

/* debug level */
int _dbg_flgs = DBG_1;

/*
 * Key Indicator.
 */

struct key_indicator {
	char *name;
	int id;
	int active;
};

#define	KI_NAME_MEM_USED	"ki.sunw.mem.used"
#define	KI_NAME_MEM_FREE	"ki.sunw.mem.free"
#define	KI_NAME_SWP_USED	"ki.sunw.swap.used"
#define	KI_NAME_SWP_FREE	"ki.sunw.swap.free"

#define	KI_ID_MEM_USED	1
#define	KI_ID_MEM_FREE	2
#define	KI_ID_SWP_USED	3
#define	KI_ID_SWP_FREE	4

static struct key_indicator ki_tab[] =
{
	{ KI_NAME_MEM_USED,	KI_ID_MEM_USED,	0},
	{ KI_NAME_MEM_FREE,	KI_ID_MEM_FREE,	0},
	{ KI_NAME_SWP_USED,	KI_ID_SWP_USED, 0},
	{ KI_NAME_SWP_FREE,	KI_ID_SWP_FREE, 0},
	{ NULL,			0,		0}
};

#define	ki_for_each(ki) for (ki = &ki_tab[0]; ki->name != NULL; ki++)

/*
 * Memory Device Sensor supports only one managed object: the
 * entire system.
 */

static int		n_active_ki = 0;	/* number of enabled KIs */
#ifndef MEMSENSOR_USE_SYSCONF
static kstat_ctl_t	*kc;			/* kstat control pointer */
#endif
static timespec_t	now;

static void insert_result(sensor_result_t *, char[], char *, uint64_t,
	uint64_t);
static void cpy_error_str(char[]);

/*
 * --- API functions ---
 */

/*
 * start(ki_name, error) is used to register the KI ki_name within the sensor.
 * It returns 0 on success, and 1 with an error messsage in the string error on
 * failure.
 */
int
start(char ki_name[], char error[])
{
	int gotit = 0;
	struct key_indicator *ki;

	unset_error_str();

	ki_for_each(ki) {
		if (strncmp(ki->name, ki_name, SENSOR_KI_TYPE_MAX) == 0) {
			gotit = 1;
			break;
		}
	}

	if (!gotit) {
		set_error_str("KI %s isn't supported", ki_name);
		cpy_error_str(error);
		return (1);
	}

	if (ki->active == 0) {
		ki->active = 1;
		n_active_ki++;
	}

	return (0);
}

/*
 * stop(ki_name, error) is used to unregister the KI ki_name within the sensor.
 * It returns 0 on success, and 1 with an error messsage in the string error on
 * failure.
 */
int
stop(char ki_name[], char error[])
{
	int gotit = 0;
	struct key_indicator *ki;

	unset_error_str();

	ki_for_each(ki) {
		if (strncmp(ki->name, ki_name, SENSOR_KI_TYPE_MAX) == 0) {
			gotit = 1;
			break;
		}
	}

	if (!gotit) {
		set_error_str("KI %s isn't supported", ki_name);
		cpy_error_str(error);
		return (1);
	}

	if (ki->active == 1) {
		ki->active = 0;
		n_active_ki--;
	}

	return (0);
}

/*
 * get_values(mobjs, n_mobjs, results, n_results, error) place CPU accounting
 * values in table results for the registered KIs and for the Managed Objects
 * given in mobjs. It returns 0 on success, and 1 with an error message in the
 * string error on failure.
 */
/* ARGSUSED0, ARGSUSED1 */
int
get_values(char **mobjs, int n_mobjs,
	sensor_result_t **results, int *n_results, char error[])
{
	static int n_allocated_res = 0;
	int n_res;
	long page_size;
#ifdef MEMSENSOR_USE_SYSCONF
	long phys_pages;
	long avphys_pages;
#else
	kstat_t *ksp;
	kstat_named_t *knp;
#endif
	char *mo;
	struct key_indicator *ki;
	uint32_t pagestotal, pagesfree;
	struct anoninfo ai;
	uint64_t mem_upperbound;
	uint64_t swap_upperbound;
	sensor_result_t *res; /* ptr to current result entry */

	unset_error_str();

	if (n_active_ki == 0) {
		DBG(DL1, ("No KI enabled\n"));
		*n_results = 0;
		return (0);
	}

	/* sensor accepts one and only one managed object */
	if (mobjs == NULL || n_mobjs != 1) {
		set_error_str(__ULAYER_BUG);
		cpy_error_str(error);
		return (1);
	}
	mo = mobjs[0];

	if ((strlen(mo) + 1) > SENSOR_MNG_OBJ_MAX) {
		set_error_str(__ULAYER_BUG);
		cpy_error_str(error);
		return (1);
	}

	if (clock_gettime(CLOCK_REALTIME, &now) < 0) {
		/*lint -e746 */
		set_error_str("clock_gettime failed (%s)", strerror(errno));
		/*lint +e746 */
		cpy_error_str(error);
		return (1);
	}

	/* get the page size from sysconf (in Bytes) */
	if ((page_size = sysconf(_SC_PAGE_SIZE)) < 0) {
		set_error_str("sysconf failed (%s)", strerror(errno));
		cpy_error_str(error);
		return (1);
	}
	page_size /= 1024; /* in KBytes */
	DBG(DL4, ("page_size: %ld\n", page_size));

#ifndef MEMSENSOR_USE_SYSCONF
	if ((kc = kstat_open()) == NULL) {
		set_error_str("kstat_open failed (%s)", strerror(errno));
		cpy_error_str(error);
		return (1);
	}

	/* look up kstat object */
	if ((ksp = kstat_lookup(kc, NULL, -1, "system_pages")) == NULL) {
		set_error_str(__SENSOR_BUG);
		cpy_error_str(error);
		(void) kstat_close(kc);
		return (1);
	}

	/* get snapshot */
	if (kstat_read(kc, ksp, NULL) < 0) {
		set_error_str(__SENSOR_BUG);
		cpy_error_str(error);
		(void) kstat_close(kc);
		return (1);
	}

	/* get total # of pages */
	if ((knp = (kstat_named_t *)kstat_data_lookup(ksp, "pagestotal"))
		== NULL) {

		set_error_str(__SENSOR_BUG);
		cpy_error_str(error);
		(void) kstat_close(kc);
		return (1);
	}

	if (knp->data_type != KSTAT_DATA_UINT32) {
		set_error_str(__SENSOR_BUG);
		cpy_error_str(error);
		(void) kstat_close(kc);
		return (1);
	}

	pagestotal = knp->value.ui32;
	DBG(DL4, ("pagestotal: %lu\n", (unsigned long)pagestotal));
#else
	/* get total # of pages using sysconf */
	if ((phys_pages = sysconf(_SC_PHYS_PAGES)) < 0) {
		set_error_str("sysconf failed (%s)", strerror(errno));
		cpy_error_str(error);
		return (1);
	}
	DBG(DL4, ("phys_pages: %ld\n", phys_pages));

	pagestotal = (uint32_t)phys_pages;
#endif

	mem_upperbound = (uint64_t)(pagestotal * (uint32_t)page_size);

#ifndef MEMSENSOR_USE_SYSCONF
	/* get # of available pages */
	if ((knp = (kstat_named_t *)kstat_data_lookup(ksp, "pagesfree"))
		== NULL) {

		set_error_str(__SENSOR_BUG);
		cpy_error_str(error);
		(void) kstat_close(kc);
		return (1);
	}

	if (knp->data_type != KSTAT_DATA_UINT32) {
		set_error_str(__SENSOR_BUG);
		cpy_error_str(error);
		(void) kstat_close(kc);
		return (1);
	}

	pagesfree = knp->value.ui32;
	DBG(DL4, ("pagesfree: %lu\n", (unsigned long)pagesfree));
#else
	/* get total # of available pages using sysconf */
	if ((avphys_pages = sysconf(_SC_AVPHYS_PAGES)) < 0) {
		set_error_str("sysconf failed (%s)", strerror(errno));
		cpy_error_str(error);
		return (1);
	}
	DBG(DL4, ("avphys_pages: %ld\n", avphys_pages));

	pagesfree = (uint32_t)avphys_pages;
#endif

	(void) memset(&ai, 0, sizeof (ai));
	if (swapctl(SC_AINFO, &ai) == 1) {
		set_error_str("swapctl failed (%s)", strerror(errno));
		cpy_error_str(error);
#ifndef MEMSENSOR_USE_SYSCONF
		(void) kstat_close(kc);
#endif
		return (1);
	}

	swap_upperbound = ai.ani_max * (uint32_t)page_size;

	/* allocated memory for result entries if needed */
	n_res = n_active_ki;
	if (n_res > n_allocated_res) {
		sensor_result_t *tmp;
		size_t sz = (size_t)n_res * sizeof (*tmp);

		if ((tmp = (sensor_result_t *)realloc(*results, sz)) == NULL) {
			set_error_str("realloc failed (%s)", strerror(errno));
			cpy_error_str(error);
#ifndef MEMSENSOR_USE_SYSCONF
			(void) kstat_close(kc);
#endif
			return (1);
		}

		*results = tmp;
		n_allocated_res = n_res;
	}

	res = *results;
	n_res = 0;

	ki_for_each(ki) {
		uint64_t val;
		uint64_t *uval; /* upper bound val ptr */

		if (ki->active == 0)
			continue;

		switch (ki->id) {
		case KI_ID_MEM_FREE:
			val = (uint64_t)(pagesfree * (uint32_t)page_size);
			uval = &mem_upperbound;
			break;
		case KI_ID_MEM_USED:
			val = (pagestotal - pagesfree) * (uint32_t)page_size;
			uval = &mem_upperbound;
			break;
		case KI_ID_SWP_FREE:
			val = (ai.ani_max - ai.ani_resv) * (uint32_t)page_size;
			uval = &swap_upperbound;
			break;
		case KI_ID_SWP_USED:
			val = ai.ani_resv * (uint32_t)page_size;
			uval = &swap_upperbound;
			break;
		default:
			set_error_str(__SENSOR_BUG);
			cpy_error_str(error);
#ifndef MEMSENSOR_USE_SYSCONF
			(void) kstat_close(kc);
#endif
			return (1);
		}

		insert_result(res, ki->name, mo, val, *uval);

		res++;
		n_res++;
	}

#ifndef MEMSENSOR_USE_SYSCONF
	(void) kstat_close(kc);
#endif

	if ((*n_results = n_res) != n_active_ki) {
		set_error_str(__SENSOR_BUG);
		cpy_error_str(error);
		return (1);
	}

	return (0);
}


/*
 * --- Helper functions ---
 */

/*
 * cpy_error_str(in) copies the content of the string error in the string in,
 * and appends a \n to it.
 */
static void
cpy_error_str(char in[])
{
	(void) strncpy(in, get_error_str(), ERROR_STRING_MAX);
	(void) strncat(in, "\n", ERROR_STRING_MAX);
}

/*
 * insert_result(res, ki, mo, val, uval) inserts the new result represented by
 * ki, mo, val, and uval into the result table entry res.
 */

static void
insert_result(sensor_result_t *res, char ki[], char *mo,  uint64_t val,
	uint64_t uval)
{
	res->time_current = now;
	res->usage = val;
	res->upperbound = uval;

	(void) strncpy(res->ki_type, ki, SENSOR_KI_TYPE_MAX);
	(void) strncpy(res->managed_object, mo, SENSOR_MNG_OBJ_MAX);
}


#if STANDALONE
static void
results_dump(sensor_result_t *res, int n_res)
{
	int i;

	for (i = 0; i < n_res; i++) {
		DBG(DL1, ("%ld:%ld:%s:%s:%llu\n",
			res[i].time_current.tv_sec,
			res[i].time_current.tv_nsec,
			res[i].ki_type,
			res[i].managed_object,
			res[i].usage));
	}
}

int
main(int argc, char *argv[])
{
	int i;
	int retval;
	int n_mobjs;
	char **mobjs;
	int n_results;
	int n_samplings;
	struct key_indicator *ki;
	char error[ERROR_STRING_MAX];
	sensor_result_t *results = NULL;

	if (argc < 2) {
		(void) fprintf(stderr, "usage: %s <num samplings> "
			"[<dev1> <dev2> ...]\n", argv[0]);
		return (1);
	}

	retval = 0;
	n_samplings = atoi(argv[1]);

	if ((n_mobjs = argc - 2) > 0) {

		if ((mobjs = (char **)malloc(n_mobjs * sizeof (char *)))
			== NULL) {

			(void) fprintf(stderr, "malloc failed (%s)",
				strerror(errno));
			n_mobjs = 0;
			retval = 1;
			goto out;
		}

		for (i = 0; i < n_mobjs; i++) {
			if ((mobjs[i] = (char *)malloc(SENSOR_MNG_OBJ_MAX))
				== NULL) {

				(void) fprintf(stderr, "malloc failed (%s)",
					strerror(errno));
				n_mobjs = i;
				retval = 1;
				goto out;
			}
		}

	} else {
		mobjs = NULL;
		n_mobjs = 0; /* I'm paranoid */
	}

	for (i = 0; i < n_mobjs; i++)
		(void) strncpy(mobjs[i], argv[i + 2], SENSOR_MNG_OBJ_MAX);

	/* start */
	ki_for_each(ki) {

		if (start(ki->name, error) != 0) {
			(void) fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}
	}

	/* get results */
	n_results = 0;

	for (i = 0; i < n_samplings; i++) {

		if (get_values(mobjs, n_mobjs, &results, &n_results, error)
			!= 0) {

			(void) fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}

		results_dump(results, n_results);

		(void) sleep(1);
	}

	/* stop */
	ki_for_each(ki) {

		if (stop(ki->name, error) != 0) {
			(void) fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}
	}

out:
	for (i = 0; i < n_mobjs; i++)
		free(mobjs[i]);

	free(mobjs);
	free(results);
	return (retval);
}
#endif
