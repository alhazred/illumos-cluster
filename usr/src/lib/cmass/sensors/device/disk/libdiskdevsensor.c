/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)libdiskdevsensor.c	1.4	08/05/20 SMI"

/*
 * Disk Device Sensor
 *
 * It handles the following key indicators:
 * "krs"
 * "kws"
 * "trs"
 * "tws"
 * "bcapacity"
 * "icapacity"
 * "scapacity"
 *
 * Activated globaly through disk sensor start() API,
 * disactivated globaly through disk sensor stop() API.
 *
 * Code to manage a per managed object start/stop is embeded but
 * not active (for futur use, manage(), unmanage sensor API).
 *
 * Managed objects syntax:
 *
 * dsk:<disk name>:: (whole disk)
 * or
 * dsk:<disk name>:<slice number> (a disk partitition)
 * example:
 * "dsk:c2t2d0:4" the disk c2t2d0, partition (slice) 4
 * "dsk:c2t2d0::" the whole disk c2t2d0
 *
 * mnt:<device mounted>
 * "mnt:/dev/global/dsk/d10s7"
 * "mnt:/dev/dsk/c0t0d0s0"
 *
 * swp:<swapname>
 * "swp:/dev/dsk/c0t0d0s1"
 * "swp:/mnt/titi"
 *
 *
 * 1) For devices mounted, the following key indicators are exposed:
 *
 * bcapacity: (disk capacity)
 * 	a percentage
 * 	(number of blocks used / total number of blocks)
 *  	same capacity field as the df -k output
 * icapacity: (inodes capacity)
 * 	a percentage (number of inodes used / total number of inodes)
 *
 * managed_objs syntax
 * <mnt>:<device mounted path>
 * example:
 * mnt:/dev/global/dsk/d1s7 (a global device, global access)
 * mnt:/dev/did/dsk/d10s3 (a global device, local access)
 * mnt:/dev/dsk/c0t0d0s0 (a Solaris device)
 * mnt:/dev/md/dg5/dsk/d0 (an SVM disk group)
 *
 * example:
 *
 * 	char *tobj[1] = {
 * 		"mnt:/dev/dsk/c0t0d0s0"'
 * 	};
 * 	(void) start("bcapacity", serror);
 *         (void) start("icapacity", serror);
 * 	res = get_values(tobj, 1, &results, &size, error);
 *
 * 	returns a sensor_result_t list like this:
 *
 * 	<managed_object>	<ki_type>	<usage>	<current_time>
 *
 * 	<mnt:/dev/dsk/c0t0d0s0>	<bcapacity>	11	(1098794511 78593018)
 * 	<mnt:/dev/dsk/c0t0d0s0>	<icapacity>	6	(1098794511 78593018)
 *
 * 	=> 11% of available space is used on c0t0d0s0 ("/")
 * 	=> 6% of available inodes are used on c0t0d0s0 ("/")
 *
 * 2) For swap names , the following key indicators are exposed:
 *
 * scapacity: (used capacity)
 * 	a percentage
 * 	(number of blocks used / total number of blocks)
 *
 * managed_objs syntax
 * <swp>:<swapname>
 * example:
 * (for
 * # swap -l
 * swapfile             dev  swaplo blocks   free
 * /dev/dsk/c0t0d0s1   32,121     16 1536096 1496128
 * /mnt/titi             -       16 2097136 2097136
 * #)
 *
 * swp:/dev/dsk/c0t0d0s1><scapacity>3 (1099493901 973144202)
 * swp:/mnt/titi><scapacity>0 (1099493901 973144202)
 *
 * => 3% of swapname /dev/dsk/c0t0d0s1 is used
 * => 0% of swapname /tmp/titi is used
 *
 * example:
 *
 * 	char *tobj[1] = {
 * 		"swp:/dev/dsk/c0t0d0s1"'
 * 	};
 * 	(void) start("scapacity", serror);
 * 	res = get_values(tobj, 1, &results, &size, error);
 *
 * 	returns a sensor_result_t list like this:
 *
 * 	<managed_object>	<ki_type>	<usage>	<current_time>
 *
 * 	<swp:/dev/dsk/c0t0d0s1> <scapacity>	3 (1099493901 973144202)
 *
 * 	=> 3% of available space is used on swapname /dev/dsk/c0t0d0s1
 *
 * 3) disks, svm, partitions kstats (see man iostat):
 *
 * It is the values of class 'disk' and 'partition'
 *
 * krs
 * kws
 * trs
 * tws
 *
 * krs: Kbytes read per second (the iostat kr/s field)
 * kws: Kbytes write per second (the iostat kw/s field)
 * trs: Number of reads per second (the iostat r/s field)
 * tws: Number of writes per second (the iostat w/s field)
 *
 * Note: the first usage value returned is zero
 * (on next period, computation of throughputs starts)
 *
 * managed_objs syntax
 * dsk:<disk name>:: (whole disk)
 * or
 * dsk:<disk name>:<slice number> (disk partitition)
 *
 * Example:
 *
 * 	char *tobj[2] = {
 *                 "dsk:c0t0d0::",
 *                 "dsk:c2t2d0:4"};
 * 	(void) start("krs", serror);
 *         (void) start("kws", serror);
 * 	res = get_values(tobj, 2, &results, &size, error);
 *
 * 	returns the results list:
 *
 * 	<managed_object	<ki_type>	<usage>	<current_time>
 *
 * 	<dsk:c0t0d0::>	<krs>		0	(1098793267 649960368)
 * 	<dsk:c0t0d0::>	<kws>		0	(1098793267 649960368)
 * 	<dsk:c2t2d0:4>	<krs>		1791	(1098702819 724901003)
 * 	<dsk:c2t2d0:4>	<kws>		1203	(1098702819 724901003)
 *
 *
 * 4) A get_value with managed objects list NULL and mo_size 0 returns
 *    the complete values:
 *
 * 	char *managed_objs = NULL;
 * 	res = get_values(&managed_objs, 0, &results, &size, error);
 *
 * returns a list of results:
 * ...
 * <mnt:/dev/global/dsk/d1s7><bcapacity>12 (1098702819 725266553)
 * <mnt:/dev/global/dsk/d1s7><icapacity>3 (1098702819 725266553)
 * <dsk:c0t0d0:3><krs>0 (1098702819 725331045)
 * <dsk:c0t0d0:3><trs>0 (1098702819 725331045)
 * <swp:/mnt/titi><scapacity>0 (1099493969 591083246)
 * ...
 *
 *
 * The returned list can change as disks, partitions, mounts ,
 * swap names appears or disappear in the system.
 *
 * mounts changing is detected through mnttab changes, the disk
 * sensor detects the couple <mount point><device> changes but
 * returns only the <device>, because mount point size can be very
 * big (MAXPATHLEN), breaking the managed_object max string size.
 *
 * disk/partitions changes are detected through the kstat class
 * 'disk' and 'partitions' changes. Note that removing a disk
 * or a partition doesn't make the disk or partition disapear in
 * the kstats. This is detected by opening the device before
 * he kstat_read, if the open is refused, this means the disk
 * or partition has been removed, but the kstat name is still
 * in the system.
 *
 * swapname changes are detected through the swapctl() API.
 *
 *
 * Performances:
 * =============
 *
 * mesured with ptime and gethrvtime() before and after get_values().
 *
 * a) SPARC
 * on 2 x 400Mhz sparc:
 *
 * The first get_value (NULL managed objects list) costs 10 milliseconds,
 * for 36 managed objets returning 135 ki_type usage values (135 values)
 *
 * The next get_value (NULL managed objects list) costs 1.5 milliseconds,
 * for 36 managed objets returning 135 ki_type usage values (135 values)
 *
 * b) x86
 * on 2 x 1400 Mhz x86:
 *
 * The first get_value (NULL managed objects list) costs 2 milliseconds,
 * for 24 managed objets returning 83 ki_type usage values (83 values)
 *
 * The next get_value (NULL managed objects list) costs 0.5 milliseconds,
 * for 24 managed objets returning 83 ki_type usage values (83 values)
 *
 *
 *
 * Enhancements:
 * =============
 *
 * More kstats disk values could be computed (wait queue time ?).
 */


#if 0
#define	STANDALONE
#endif

/*
 * Uncomment the following line if you don't want the sensor to convert cXtXdX
 * names to did names. did is specific to Sun Cluster, so let's keep this macro
 * around should the sensor be used for other purpose than Sun Cluster.
 */
/* #define	DISK_SENSOR_NO_DID_MAPPING */

/*
 * In addition to disk and mountpoint statistics the sensor can report
 * statistics for partitions and NFS. There's currently no need for such
 * statistics in the context of Sun Cluster. Yet, let's keep the code around as
 * it may be useful in the future.
 */
/* do not report NFS statistics */
#define	DISK_SENSOR_NO_NFS_STATS

/* do not report partition statistics */
#define	DISK_SENSOR_NO_PARTITION_STATS

/*
 *
 * Includes
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <unistd.h>
#include <libdevinfo.h>
#include <kstat.h>
#include <sys/statvfs.h>
#include <time.h>
#include <fcntl.h>
#include <sys/mnttab.h>
#include <sys/mnttab.h>
#include <sys/mntent.h>
#include <ftw.h>
#include <limits.h>
#include <nfs/nfs.h>
#include <nfs/nfs_clnt.h>
#ifndef DISK_SENSOR_NON_DID_MAPPING
#include <libdid.h>	/* Sun Cluster specific */
#endif
#include "sensors.h"


/*
 *
 * Definitions
 *
 */

/*
 * Disk Sensors hash buckets, (number is 2^DISK_SENSOR_BUCKETS_BITS)
 * Fooprint of Disk Sensors buckets is
 * (sizeof (t_i_disk_sensor) * (2 ^ DISK_SENSOR_BUCKETS_BITS))
 * 12 bits: 48 * 4096 = 192 Kbytes
 * 11 bits: 48 * 2048 = 96  Kbytes
 * 10 bits: 48 * 1024 = 48  Kbytes
 */
#define	DISK_SENSOR_BUCKETS_BITS	12	/* 2^12 = 4096 buckets */

/*
 * Various type of Disk Sensor KIs
 * DSK: from kstats class 'disk'
 * PRT: from kstats class 'partition'
 * MNT: from mnttab
 * SVM: from kstats class 'disk', metadevice module
 * NFS: from kstats class 'nfs'
 */
#define	DISK_SENSOR_DSK_TYPE		1
#define	DISK_SENSOR_PRT_TYPE		2
#define	DISK_SENSOR_SVM_TYPE		3
#define	DISK_SENSOR_NFS_TYPE		4
#define	DISK_SENSOR_MNT_TYPE		5

/*
 * Global KIs
 */
#define	DISK_SENSOR_KIS			6

#define	DISK_SENSOR_DSK_KI_START	0
#define	KI_KRS				(1<<0)
#define	KI_KWS				(1<<1)
#define	KI_TRS				(1<<2)
#define	KI_TWS				(1<<3)
#define	DISK_SENSOR_DSK_KI_END		3

#define	DISK_SENSOR_MNT_KI_START	4
#define	KI_BCAPACITY			(1<<4)
#define	KI_ICAPACITY			(1<<5)
#define	DISK_SENSOR_MNT_KI_END		5


/*
 *
 * Types definition
 *
 */

/*
 * Hash coding
 */
typedef uint32_t ub4;	/* unsigned 4-byte quantities */
typedef uint8_t ub1;	/* unsigned 1-byte quantities */
#define	hashsize(n)	((ub4)1<<(n))
#define	hashmask(n)	(hashsize(n)-1)

/*
 * Disk Sensor KI name
 */
typedef struct {
	char		*ki;
	uint32_t	nki;
} t_disk_sensor_kis;

/*
 * Disk Sensors, from kstat class 'disk' and 'partition'
 */
typedef struct {
	char		*ks_name;	/* "sd,15", "3/md0", ... */
	char		*disk;		/* "c0t0d0", "c1t1d0", ... */
#ifndef DISK_SENSOR_NO_DID_MAPPING
	int		did;		/* did instance num */
#endif
	char		*module;	/* "sd", "ssd", ... */
	char		*physname; 	/* "/pci@1f,0/.../sd@2,0" */
	int		instance;	/* module instance */
	uint32_t	slice;		/* partition number (not for meta) */
	kstat_t		*dksp;		/* disk kstat pointer */
	kstat_io_t	pkios;		/* previous io kstats */
	kstat_io_t	dkios;		/* io kstats */
	hrtime_t	last_snap;	/* previous snap time */
	hrtime_t	snap;		/* current snap time */
} t_i_dsk_disk_sensor;

/*
 * Disk Sensors, from mnttab
 */
typedef struct {
	char		*device;	/* device mounted */
	char		*mountp;	/* mount point */
	uint64_t	bcapacity;	/* disk space used */
	uint64_t	icapacity;	/* inodes space used */
} t_i_mnt_disk_sensor;

/*
 * Disk Sensors, generic (bucket)
 */
typedef struct s_i_disk_sensor {
	char			*name;
	uint32_t		kis;		/* bit field */
	int			type;
	hrtime_t		already_read;
	struct timespec		time;
	t_i_dsk_disk_sensor	*dsk;
	t_i_mnt_disk_sensor	*mnt;
	struct s_i_disk_sensor	*next;	/* buckets if hash collision */
} t_i_disk_sensor;

/*
 * A string list type, can handle three strings, typed
 */
typedef struct s_strlist {
	char			*str;
	char			*str2;
	char			*sensor;
	int			type;
	int			instance;
	int			nfs;
	struct s_strlist	*next;
} t_strlist;

/*
 * String list head and tail, to speed up tail insertion
 */
typedef struct {
	t_strlist	*head;
	t_strlist	*tail;
} t_strlistp;

/*
 * A basic description of each device found
 * on the system by walking the device tree.
 * These entries are used to find the instance
 * number by comparing the physical name.
 */
typedef struct ldinfo {
	char 		*name;
	char 		*dtype;
	int 		dnum;
	struct ldinfo	*next;
} ldinfo_t;


/*
 *
 * Global data
 *
 */

/*
 * The Disk Sensor table, accessed by a hash on managed object name
 */
static t_i_disk_sensor idisksensors[hashsize(DISK_SENSOR_BUCKETS_BITS)];

/*
 * The Disk Sensor KI name table
 */
t_disk_sensor_kis disk_sensor_kis[DISK_SENSOR_KIS] = {
	"ki.sunw.rbyte.rate",		KI_KRS,
	"ki.sunw.wbyte.rate",		KI_KWS,
	"ki.sunw.read.rate",		KI_TRS,
	"ki.sunw.write.rate",		KI_TWS,
	"ki.sunw.block.used",		KI_BCAPACITY,
	"ki.sunw.inode.used",		KI_ICAPACITY
};

/*
 * Global String lists, to detect add or remove of
 * disk and partition kstats, mounted devices, devices
 */
static t_strlistp kstlstp;	/* list of kstats class disk & partition */
static t_strlistp mntlstp;	/* list of mount points */
static t_strlistp dsklstp;	/* list of discovered disks */


/*
 * Various global data
 */
static kstat_ctl_t *kc = NULL;		/* libkstat access */
/*lint -e551 */
static uint32_t hash_collisions = 0;	/* number of hash collisions */
/*lint -e551 */
static int disk_objects_cnt = 0;	/* number of managed objects */
/*lint -e551 */
static int disk_objects_dsk_cnt = 0;	/* number of 'dsk' managed objects */
/*lint -e551 */
static int disk_objects_mnt_cnt = 0;	/* number of 'mnt' managed objects */
static hrtime_t get_value_time = 0;	/* to avoid re-read several */
					/* time the same data */
static int disk_kis_cnt = 0;	/* Number of KIs, managed objects have  */
				/* each some KIs, set by start() calls. */
static uint32_t mkis = 0;	/* KIs global mask, setup by start() */
				/* and clear by stop() */


/*
 *
 * Prototypes
 *
 */

static char *real_device_name(char *);
static int devinfo_ident_disks(di_node_t, void *);
static void pline(char *, int, char *, ldinfo_t **);
static void cleanup_ldinfo(ldinfo_t *);
static ldinfo_t *find_ldinfo_match(char *, ldinfo_t *);
static ub4 hash(register ub1 *, register ub4, register ub4);
static uint32_t get_ki_bit(char *ki);
static t_i_disk_sensor *search_disksensor(char *, uint32_t *,
    t_i_disk_sensor **);
static t_i_disk_sensor *append_disksensor(char *, int,
    t_i_disk_sensor *, t_i_disk_sensor **);
static int remove_disksensor(t_i_disk_sensor *, t_i_disk_sensor *);
static ldinfo_t *rummage_devinfo(void);
static int changed_in_kstats(t_strlistp *, t_strlistp *);
static int changed_in_mnts(t_strlistp *, t_strlistp *);
static void discover_disks();
static void add_disk_path(const char *);
static int mpath_disk(const char *, const struct stat *, int);
static t_strlist *insert_strlist(t_strlistp *);
static t_strlist *search_strlist(char *, char *, t_strlistp *);
static int remove_strlist(t_strlist *, t_strlistp *);
static void free_strlist(t_strlistp *);
static t_i_dsk_disk_sensor *create_dsk_disk_sensor(t_strlist *, ldinfo_t *);
static t_i_mnt_disk_sensor *create_mnt_disk_sensor(t_strlist *);
static void setup_ki(t_i_disk_sensor *tmp, int *count);
static void read_dsk_sensors(t_i_disk_sensor *);
static void read_mnt_sensors(t_i_disk_sensor *);
static int get_ki_usage(t_i_disk_sensor *, uint32_t, uint64_t *);
static void clear_ki(t_i_disk_sensor *, int *);
static void get_all_values(t_i_disk_sensor *, sensor_result_t **, int *);
static uint64_t ull_delta(uint64_t, uint64_t);
static uint64_t hrtime_delta(hrtime_t, hrtime_t);
static char *lookup_nfs_name(char *);
static char *get_nfs_by_minor(uint_t);
static char *cur_hostname(uint_t);
static char *cur_special(char *, char *);


/*
 *
 * Private Functions
 *
 */

static char *
lookup_nfs_name(char *ks)
{
	uint_t		vminor;
	char		*host;
	char		*path;
	char		*cp;
	char		*rstr = NULL;
	size_t		len;

	if (sscanf(ks, "nfs%u", &vminor) == 1) {
		cp = get_nfs_by_minor(vminor);
		if (cp) {
			if (strchr(cp, ',') == NULL) {
				rstr = strdup(cp);
				return (rstr);
			}
			host = cur_hostname(vminor);
			if (host) {
				if (strlen(host) != 0) {
					path = cur_special(host, cp);
					if (path) {
						len = strlen(host);
						len += strlen(path);
						len += 2;
						rstr = malloc(len);
						if (rstr == NULL) {
							free(host);
							return (rstr);
						}
						(void) snprintf(rstr, len,
						    "%s:%s", host, path);
					} else {
						rstr = strdup(cp);
					}
				} else {
					rstr = strdup(ks);
				}
				free(host);
			} else {
				rstr = strdup(cp);
			}
		}
	}
	return (rstr);
}


static char *
get_nfs_by_minor(uint_t vminor)
{
	t_strlist	*tmp;
	for (tmp = mntlstp.head; tmp != NULL; tmp = tmp->next) {
		if (tmp->nfs == 1 && tmp->instance == (int)vminor) {
			return (tmp->str);
		}
	}
	return (NULL);
}


/*
 * Read the cur_hostname from the mntinfo kstat
 */
static char *
cur_hostname(uint_t vminor)
{
	kstat_t				*ksp;
	static struct mntinfo_kstat	mik;
	char 				*rstr;
	int				inst = (int)vminor;

	for (ksp = kc->kc_chain; ksp != NULL; ksp = ksp->ks_next) {
		if (ksp->ks_type != KSTAT_TYPE_RAW)
			continue;
		if (ksp->ks_instance != inst)
			continue;
		if (strcmp(ksp->ks_module, "nfs") != 0)
			continue;
		if (strcmp(ksp->ks_name, "mntinfo") != 0)
			continue;
		if (ksp->ks_flags & KSTAT_FLAG_INVALID)
			return (NULL);
		if (kstat_read(kc, ksp, &mik) == -1)
			return (NULL);
		rstr = strdup(mik.mik_curserver);
		return (rstr);
	}
	return (NULL);
}

/*
 * Given the hostname of the mounted server, extract the server
 * mount point from the mnttab string.
 *
 * Common forms:
 *	server1,server2,server3:/path
 *	server1:/path,server2:/path
 * or a hybrid of the two
 */
static char *
cur_special(char *hostname, char *special)
{
	char *cp;
	char *path;
	size_t hlen = strlen(hostname);

	/*
	 * find hostname in string
	 */
cur_special_again:
	if ((cp = strstr(special, hostname)) == NULL)
		return (NULL);

	/*
	 * hostname must be followed by ',' or ':'
	 */
	if (cp[hlen] != ',' && cp[hlen] != ':') {
		special = &cp[hlen];
		goto cur_special_again;
	}

	/*
	 * If hostname is followed by a ',' eat all characters until a ':'
	 */
	cp = &cp[hlen];
	if (*cp == ',') {
		cp++;
		while (*cp != ':') {
			if (*cp == NULL)
				return (NULL);
			cp++;
		}
	}
	path = ++cp;			/* skip ':' */

	/*
	 * path is terminated by either 0, or space or ','
	 */
	while (*cp != '\0') {
		if (isspace(*cp) || *cp == ',') {
			*cp = '\0';
			return (path);
		}
		cp++;
	}
	return (path);
}


static uint32_t
get_ki_bit(char *ki)
{
	int i;

	for (i = 0; i < DISK_SENSOR_KIS; i++) {
		if (strcmp(ki, disk_sensor_kis[i].ki) == 0) {
			return (disk_sensor_kis[i].nki);
		}
	}
	return (0);
}


static int
remove_disksensor(t_i_disk_sensor *tmp, t_i_disk_sensor *prev)
{
	if (tmp->name == NULL) {
		return (1);
	}
	free(tmp->name);
	if (tmp->dsk != NULL) {
		if ((tmp->dsk)->ks_name != NULL) {
			free(tmp->dsk->ks_name);
		}
		if ((tmp->dsk)->disk != NULL) {
			free(tmp->dsk->disk);
		}
		if ((tmp->dsk)->module != NULL) {
			free(tmp->dsk->module);
		}
		if ((tmp->dsk)->physname != NULL) {
			free(tmp->dsk->physname);
		}
		free(tmp->dsk);
	}
	if (tmp->mnt != NULL) {
		if ((tmp->mnt)->device != NULL) {
			free((tmp->mnt)->device);
		}
		if ((tmp->mnt)->mountp != NULL) {
			free((tmp->mnt)->mountp);
		}
		free(tmp->mnt);
	}
	tmp->name = NULL;
	tmp->kis = 0;
	tmp->already_read = 0;
	(tmp->time).tv_nsec = 0;
	(tmp->time).tv_sec = 0;
	tmp->dsk = NULL;
	tmp->mnt = NULL;
	disk_objects_cnt--;
	if (tmp->type == DISK_SENSOR_MNT_TYPE) {
		disk_objects_mnt_cnt--;
	} else {
		disk_objects_dsk_cnt--;
	}
	tmp->type = 0;
	if (prev != NULL) {
		prev->next = tmp->next;
		free(tmp);
		hash_collisions--;
	} else {
		if (tmp->next != NULL) {
			t_i_disk_sensor *sv = tmp->next;
			(void) memcpy(tmp, sv, sizeof (t_i_disk_sensor));
			hash_collisions--;
			free(sv);
		}
	}
	return (0);
}


static t_i_disk_sensor *
append_disksensor(char *disksensor, int sensor_type,
	t_i_disk_sensor *tmp,  t_i_disk_sensor **iprev)
{
	*iprev = NULL;

	if (tmp->name == NULL) {
		tmp->name = strdup(disksensor);
		if (tmp->name == NULL) {
			return (NULL);
		}
		tmp->type = sensor_type;
		disk_objects_cnt++;
		if (tmp->type == DISK_SENSOR_MNT_TYPE) {
			disk_objects_mnt_cnt++;
		} else {
			disk_objects_dsk_cnt++;
		}
		return (tmp);
	}
	/* Go to the end of the list */
	while (tmp != NULL) {
		*iprev = tmp;
		tmp = tmp->next;
	}
	tmp = (t_i_disk_sensor *)malloc(sizeof (t_i_disk_sensor));
	if (tmp == NULL) {
		return (NULL);
	}
	(void) memset((void *)tmp, 0, sizeof (t_i_disk_sensor));
	tmp->name = strdup(disksensor);
	if (tmp->name == NULL) {
		free(tmp);
		return (NULL);
	}
	if (*iprev != NULL) {
		(*iprev)->next = tmp;
	}
	tmp->type = sensor_type;
	hash_collisions++;
	disk_objects_cnt++;
	if (tmp->type == DISK_SENSOR_MNT_TYPE) {
		disk_objects_mnt_cnt++;
	} else {
		disk_objects_dsk_cnt++;
	}
	return (tmp);
}


static t_i_disk_sensor *
search_disksensor(char *disksensor, uint32_t *bucket, t_i_disk_sensor **prev)
{
	t_i_disk_sensor *tmp;

	*prev = NULL;
	*bucket = hash((ub1 *)disksensor, (ub4)strlen(disksensor), 0)
	    & hashmask(DISK_SENSOR_BUCKETS_BITS);
	tmp = &(idisksensors[*bucket]);
	while (tmp != NULL) {
		if ((tmp->name != NULL) &&
		    (strcmp(tmp->name, disksensor) == 0)) {
			return (tmp);
		}
		*prev = tmp;
		tmp = tmp->next;
	}
	return (NULL);
}


static char *
real_device_name(char *dname)
{
	int nbyr;
	struct stat sbuf;
	char *npt = NULL;
	static char nmbuf[MAXPATHLEN + 1];

	/*
	 * Get the real device name for this beast
	 * by following the link into /devices.
	 */
	if (lstat(dname, &sbuf) != -1) {
		if ((sbuf.st_mode & S_IFMT) == S_IFLNK) {
			/*
			 * It's a link. Get the real name.
			 */
			if ((nbyr = readlink(dname, nmbuf,
			    sizeof (nmbuf))) != 1) {
				npt = nmbuf;
				/*
				 * readlink does not terminate
				 * the string so we have to
				 * do it.
				 */
				nmbuf[nbyr] = NULL;
			}
		}
	}
	return (npt);
}


/*
 * rummage_devinfo() is the driver routine that walks the devinfo snapshot.
 */
static ldinfo_t *
rummage_devinfo(void)
{
	di_node_t root_node;
	ldinfo_t *rv = NULL;

	if ((root_node = di_init("/", DINFOCPYALL)) != DI_NODE_NIL) {
		(void) di_walk_node(root_node, DI_WALK_CLDFIRST, (void *)&rv,
			devinfo_ident_disks);
	}
	di_fini(root_node);
	return (rv);
}


static void
cleanup_ldinfo(ldinfo_t *list)
{
	ldinfo_t *tmp;

	while (list != NULL) {
		tmp = list;
		list = list->next;
		if (tmp->name != NULL) {
			free(tmp->name);
		}
		if (tmp->dtype) {
			free(tmp->dtype);
		}
		free(tmp);
	}
}


/*
 * devinfo_ident_disks() are the callback functions we
 * use while walking the device tree snapshot provided by devinfo.  If
 * devinfo_ident_disks() identifies that the device being considered has one or
 * more minor nodes _and_ is a block device, then it is a potential disk.
 * Note: if a driver was previously loaded but is now unloaded, the kstat may
 * still be around (e.g., st) but no information will be found in the
 * libdevinfo tree.
 */
static int
devinfo_ident_disks(di_node_t node, void *arg)
{
	di_minor_t vminor = DI_MINOR_NIL;

	if ((vminor = di_minor_next(node, vminor)) != DI_MINOR_NIL) {
		int spectype = di_minor_spectype(vminor);

		if (S_ISBLK(spectype)) {
			char *physical_path = di_devfs_path(node);
			int instance = di_instance(node);
			char *driver_name = di_driver_name(node);
			pline(physical_path, instance, driver_name, arg);
			di_devfs_path_free(physical_path);
		}
	}
	return (DI_WALK_CONTINUE);
}


/*
 * pline() performs the lookup of the device path in the current list of disks,
 * and adds the appropriate information to the nms list in the case of a match.
 */
static void
pline(char *devfs_path, int instance,
	char *driver_name, ldinfo_t **list)
{
	ldinfo_t *entry;

	entry = (ldinfo_t *)malloc(sizeof (ldinfo_t));
	if (entry == NULL) {
		return;
	}
	(void) memset(entry, 0, sizeof (ldinfo_t));
	entry->dnum = instance;
	if (devfs_path != NULL) {
		entry->name = strdup(devfs_path);
		if (entry->name == NULL) {
			free(entry);
			return;
		}
	}
	if (driver_name != NULL) {
		entry->dtype = strdup(driver_name);
		if (entry->dtype == NULL) {
			if (entry->name != NULL) {
				free(entry->name);
			}
			free(entry);
			return;
		}
	}
	/* Append to the ldinfo list */
	entry->next = *list;
	*list = entry;
}


/*
 * Find an entry matching the name passed in
 */
static ldinfo_t *
find_ldinfo_match(char *name, ldinfo_t *ptoi)
{
	if (name != NULL) {
		while (ptoi != NULL) {
			if (strcmp(ptoi->name, name) != 0)
				ptoi = ptoi->next;
			else
				return (ptoi);
		}
	}
	return (NULL);
}


/*
 * --------------------------------------------------------------------
 * lookup2.c, by Bob Jenkins, December 1996, Public Domain. hash(), hash2(),
 * hash3, and mix() are externally useful functions. Routines to test the
 * hash are included if SELF_TEST is defined. You can use this free for any
 * purpose.  It has no warranty.
 * --------------------------------------------------------------------
 */
/*
 * mix 3 32-bit values reversibly. For every delta with one or two bit
 * set, and the deltas of all three high bits or all three low bits, whether
 * the original value of a,b,c is almost all zero or is uniformly
 * distributed, If mix() is run forward or backward, at least 32 bits in
 * a,b,c have at least 1/4 probability of changing. If mix() is run forward,
 * every bit of c will change between 1/3 and 2/3 of the time.  (Well, 22/100
 * and 78/100 for some 2-bit deltas.) mix() was built out of 36 single-cycle
 * latency instructions in a structure that could supported 2x parallelism,
 * like so: a -= b; a -= c; x = (c>>13); b -= c; a ^= x; b -= a; x = (a<<8);
 * c -= a; b ^= x; c -= b; x = (b>>13); ... Unfortunately, superscalar
 * Pentiums and Sparcs can't take advantage of that parallelism.  They've
 * also turned some of those single-cycle latency instructions into
 * multi-cycle latency instructions.  Still, this is the fastest good hash I
 * could find.  There were about 2^^68 to choose from.  I only looked at a
 * billion or so.
 */
#define	mix(a, b, c) { \
a -= b; a -= c; a ^= (c>>13); \
b -= c; b -= a; b ^= (a<<8); \
c -= a; c -= b; c ^= (b>>13); \
a -= b; a -= c; a ^= (c>>12);  \
b -= c; b -= a; b ^= (a<<16); \
c -= a; c -= b; c ^= (b>>5); \
a -= b; a -= c; a ^= (c>>3);  \
b -= c; b -= a; b ^= (a<<10); \
c -= a; c -= b; c ^= (b>>15); \
}


/*
 * hash a variable-length ki into a 32-bit value k : the ki
 * (the unaligned variable-length array of bytes) len : the length of the
 * ki, counting by bytes level : can be any 4-byte value Returns a 32-bit
 * value.  Every bit of the ki affects every bit of the return value.  Every
 * 1-bit and 2-bit delta achieves avalanche. About 36+6len instructions.
 *
 * The best hash table sizes are powers of 2.  There is no need to do mod a
 * prime (mod is sooo slow!).  If you need less than 32 bits, use a bitmask.
 * For example, if you need only 10 bits, do h = (h & hashmask(10)); In which
 * case, the hash table should have hashsize(10) elements.
 *
 * If you are hashing n strings (ub1 **)k, do it like this: for (i=0, h=0; i<n;
 * ++i) h = hash( k[i], len[i], h);
 *
 * By Bob Jenkins, 1996.  bob_jenkins@burtleburtle.net.  You may use this code
 * any way you wish, private, educational, or commercial.  It's free.
 *
 * See http://burlteburtle.net/bob/hash/evahash.html Use for hash table lookup,
 * or anything where one collision in 2^32 is acceptable.  Do NOT use for
 * cryptographic purposes.
 */
static ub4
hash(k, length, initval)
	register ub1   *k;	 /* the ki */
	register ub4    length;	 /* the length of the ki */
	register ub4    initval; /* the previous hash, or an arbitrary value */
{
	register ub4    a, b, c, len;

	/*
	 * Set up the internal state
	 */
	len = length;
	/* the golden ratio; an arbitrary value */
	a = b = (ub4)0x9e3779b9;
	c = initval;		/* the previous hash value */

	/* handle most of the ki */
	while (len >= 12) {
		a += (k[0] + ((ub4) k[1] << 8) + \
			((ub4) k[2] << 16) + ((ub4) k[3] << 24));
		b += (k[4] + ((ub4) k[5] << 8) + \
			((ub4) k[6] << 16) + ((ub4) k[7] << 24));
		c += (k[8] + ((ub4) k[9] << 8) + ((ub4) k[10] << 16) + \
			((ub4) k[11] << 24));
		mix(a, b, c);
		k += 12;
		len -= 12;
	}

	/* handle the last 11 bytes */
	c += length;
	switch (len) {		/* all the case statements fall through */
	case 11:
		c += ((ub4) k[10] << 24);
/* FALLTHRU */
	case 10:
		c += ((ub4) k[9] << 16);
/* FALLTHRU */
	case 9:
		c += ((ub4) k[8] << 8);
		/*
		 * the first byte of c is reserved for the length
		 */
/* FALLTHRU */
	case 8:
		b += ((ub4) k[7] << 24);
/* FALLTHRU */
	case 7:
		b += ((ub4) k[6] << 16);
/* FALLTHRU */
	case 6:
		b += ((ub4) k[5] << 8);
/* FALLTHRU */
	case 5:
		b += k[4];
/* FALLTHRU */
	case 4:
		a += ((ub4) k[3] << 24);
/* FALLTHRU */
	case 3:
		a += ((ub4) k[2] << 16);
/* FALLTHRU */
	case 2:
		a += ((ub4) k[1] << 8);
/* FALLTHRU */
	case 1:
		a += k[0];
		/*
		 * case 0: nothing left to add
		 */
	default:
		;
	}
	mix(a, b, c);
	/* report the result */
	return (c);
}


static int
changed_in_kstats(t_strlistp *added, t_strlistp *removed)
{
	kstat_t *ksp;
	t_strlistp klst;
	t_strlist *tmp;
	t_strlist *cur;
	static int kstats_once = 0;

	if (kstats_once != 0) {
		kid_t new_id;
		new_id = kstat_chain_update(kc);
		if (new_id < 0) {
			return (-1);
		}
		if (new_id == 0) {
			return (1);
		}
	} else {
		kstats_once = 1;
	}

	klst.head = klst.tail = NULL;

	for (ksp = kc->kc_chain; ksp; ksp = ksp->ks_next) {
#ifndef DISK_SENSOR_NO_NFS_STATS
		if (strcmp(ksp->ks_class, "nfs") == 0) {
			tmp = insert_strlist(&klst);
			if (tmp == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			tmp->str = strdup(ksp->ks_name);
			if (tmp->str == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			tmp->type = DISK_SENSOR_NFS_TYPE;
			tmp->instance = ksp->ks_instance;
			tmp->str2 = strdup(ksp->ks_module);
			if (tmp->str2 == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
		}
#endif
		if (strcmp(ksp->ks_class, "disk") == 0) {
			tmp = insert_strlist(&klst);
			if (tmp == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			tmp->str = strdup(ksp->ks_name);
			if (tmp->str == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			if (strstr(ksp->ks_name, "/md") != NULL) {
				tmp->type = DISK_SENSOR_SVM_TYPE;
				tmp->instance = ksp->ks_instance;
				tmp->str2 = strdup(ksp->ks_module);
				if (tmp->str2 == NULL) {
					free_strlist(&klst);
					free_strlist(added);
					free_strlist(removed);
					return (-1);
				}
			} else {
				tmp->type = DISK_SENSOR_DSK_TYPE;
			}
		}
#ifndef DISK_SENSOR_NO_PARTITION_STATS
		if (strcmp(ksp->ks_class, "partition") == 0) {
			tmp = insert_strlist(&klst);
			if (tmp == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			tmp->str = strdup(ksp->ks_name);
			if (tmp->str == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			tmp->type = DISK_SENSOR_PRT_TYPE;
		}
#endif
	}
	for (tmp = klst.head; tmp != NULL; tmp = tmp->next) {
		if (search_strlist(tmp->str, NULL, &kstlstp) == NULL) {
			cur = insert_strlist(added);
			if (cur == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->str = strdup(tmp->str);
			if (cur->str == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->type = tmp->type;
			if (cur->type == DISK_SENSOR_SVM_TYPE ||
			    cur->type == DISK_SENSOR_NFS_TYPE) {
				cur->instance = tmp->instance;
				cur->str2 = strdup(tmp->str2);
				if (tmp->str2 == NULL) {
					free_strlist(&klst);
					free_strlist(added);
					free_strlist(removed);
					return (-1);
				}
			}
		}
	}
	for (tmp = kstlstp.head; tmp != NULL; tmp = tmp->next) {
		if (search_strlist(tmp->str, NULL, &klst) == NULL) {
			cur = insert_strlist(removed);
			if (cur == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->str = strdup(tmp->str);
			if (cur->str == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->sensor = strdup(tmp->sensor);
			if (cur->str == NULL) {
				free_strlist(&klst);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->type = tmp->type;
		}
	}
	free_strlist(&klst);
	if (added->head == NULL && removed->head == NULL) {
		return (1);
	}
	return (0);
}


static void
discover_disks()
{
	(void) ftw("/dev/rdsk", mpath_disk, 5);
}


/*ARGSUSED*/
static int
mpath_disk(const char *path, const struct stat *statp, int type)
{
	if (type != FTW_F) {
		return (0);
	}
	/* any sX (X < 16) could be used */
	if (strcmp(&path[strlen(path) - 2], "s2") == 0) {
		add_disk_path(path);
	}
	return (0);
}


static void
add_disk_path(const char *cpath)
{
	int fd;
	struct stat st;
	t_strlist *tmp;
	char *disk;

	if (strstr(cpath, "/dev/rdsk/rdsk") != NULL) {
		return;
	}
	if ((fd = open(cpath, O_RDONLY|O_NDELAY)) < 0) {
		/*
		 * TODO: we sometimes get Resource Busy error here, possibly
		 * making some disks invisible. [EL]
		 */
		return;
	}
	if (stat(cpath, &st) != 0) {
		(void) close(fd);
		return;
	}
	disk = strrchr(cpath, '/');
	if (disk == NULL) {
		(void) close(fd);
		return;
	} else {
		disk++;
	}
	tmp = insert_strlist(&dsklstp);
	if (tmp == NULL) {
		(void) close(fd);
		return;
	}
	tmp->str = strdup(disk);
	if (tmp->str == NULL) {
		(void) remove_strlist(tmp, &dsklstp);
		(void) close(fd);
		return;
	}
	(void) close(fd);
}


static int
remove_strlist(t_strlist *cur, t_strlistp *lst)
{
	t_strlist *tmp = lst->head;
	t_strlist *prev = NULL;

	if (cur == NULL || tmp == NULL) {
		/* there is nothing to remove */
		return (1);
	}
	if (cur->str == NULL) {
		/* there is nothing to remove */
		return (1);
	}
	while (tmp != NULL) {
		if (strcmp(tmp->str, cur->str) == 0) {
			break;
		}
		prev = tmp;
		tmp = tmp->next;
	}
	if (tmp == NULL) {
		/* not found */
		return (1);
	}
	if (tmp == lst->tail) {
		lst->tail = prev;
	}
	if (prev != NULL) {
		prev->next = tmp->next;
	} else {
		lst->head = tmp->next;
	}
	if (tmp->str != NULL) {
		free(tmp->str);
	}
	if (tmp->str2 != NULL) {
		free(tmp->str2);
	}
	if (tmp->sensor != NULL) {
		free(tmp->sensor);
	}
	free(tmp);
	return (0);
}


static t_strlist *
insert_strlist(t_strlistp *lst)
{
	t_strlist *tmp;

	tmp = (t_strlist*)malloc(sizeof (t_strlist));
	if (tmp == NULL) {
		return (NULL);
	}
	(void) memset(tmp, 0, sizeof (t_strlist));
	if (lst->head == NULL) {
		lst->head = lst->tail = tmp;
	} else {
		(lst->tail)->next = tmp;
		lst->tail = tmp;
	}
	return (tmp);
}


static t_strlist*
search_strlist(char *str, char *str2, t_strlistp *lst)
{
	t_strlist *tmp;
	if (str == NULL && str2 == NULL) {
		return (NULL);
	}
	for (tmp = lst->head; tmp != NULL; tmp = tmp->next) {
		if (str != NULL && str2 != NULL) {
			if ((strcmp(str, tmp->str) == 0) &&
			    (strcmp(str2, tmp->str2) == 0)) {
				return (tmp);
			}
		}
		if (str != NULL) {
			if (strcmp(str, tmp->str) == 0) {
				return (tmp);
			}
		}
		if (str2 != NULL) {
			if (strcmp(str2, tmp->str2) == 0) {
				return (tmp);
			}
		}
	}
	return (NULL);
}


static void
free_strlist(t_strlistp *lst)
{
	t_strlist *tmp = lst->head;
	t_strlist *next = NULL;

	while (tmp != NULL) {
		next = tmp->next;
		if (tmp->str != NULL) {
			free(tmp->str);
		}
		if (tmp->str2 != NULL) {
			free(tmp->str2);
		}
		if (tmp->sensor != NULL) {
			free(tmp->sensor);
		}
		free(tmp);
		tmp = next;
	}
	lst->head = lst->tail = NULL;
}


static int
changed_in_mnts(t_strlistp *added, t_strlistp *removed)
{
	int		res;
	FILE		*mpt;
	struct flock	lb;
	struct extmnttab mnt;
	t_strlist	*tmp;
	t_strlist	*cur;
	t_strlistp	mntl;

	mntl.head = mntl.tail = NULL;

	if ((mpt = fopen(MNTTAB, "r")) != NULL) {
		lb.l_type = F_RDLCK;
		lb.l_whence = 0;
		lb.l_start = 0;
		lb.l_len = 0;
		(void) fcntl(fileno(mpt), F_SETLKW, &lb);
		resetmnttab(mpt);
		while ((res = getextmntent(mpt, &mnt,
			sizeof (struct extmnttab))) != -1) {
			if (res == 0) {
				if ((strcmp(
				    mnt.mnt_fstype, MNTTYPE_UFS) == 0) ||
				    (strcmp(
				    mnt.mnt_fstype, MNTTYPE_NFS) == 0) ||
				    (strcmp(
				    mnt.mnt_fstype, MNTTYPE_NFS3) == 0) ||
				    (strcmp(mnt.mnt_fstype, "nfs4") == 0) ||
				    (strcmp(mnt.mnt_fstype, "qfs") == 0) ||
				    (strcmp(mnt.mnt_fstype, "samfs") == 0) ||
				    (strcmp(mnt.mnt_fstype, "zfs") == 0) ||
				    (strcmp(mnt.mnt_fstype, "vxfs") == 0)) {
					tmp = insert_strlist(&mntl);
					if (tmp == NULL) {
						free_strlist(&mntl);
						free_strlist(added);
						free_strlist(removed);
						(void) fclose(mpt);
						return (-1);
					}
					tmp->str = strdup(mnt.mnt_special);
					if (tmp->str == NULL) {
						free_strlist(&mntl);
						free_strlist(added);
						free_strlist(removed);
						(void) fclose(mpt);
						return (-1);
					}
					tmp->str2 = strdup(mnt.mnt_mountp);
					if (tmp->str2 == NULL) {
						free_strlist(&mntl);
						free_strlist(added);
						free_strlist(removed);
						(void) fclose(mpt);
						return (-1);
					}
					tmp->type = DISK_SENSOR_MNT_TYPE;
					tmp->instance = (int)mnt.mnt_minor;
					if ((strcmp(mnt.mnt_fstype,
					    MNTTYPE_NFS) == 0) ||
					    (strcmp(mnt.mnt_fstype,
					    MNTTYPE_NFS3) == 0) ||
					    (strcmp(mnt.mnt_fstype,
					    "nfs4") == 0)) {
						tmp->nfs = 1;
					}
				}
			}
		}
		/*
		 * Lock goes away when we close the file.
		 */
		(void) fclose(mpt);
	} else {
		return (-1);
	}

	for (tmp = mntl.head; tmp != NULL; tmp = tmp->next) {
		if (search_strlist(tmp->str, tmp->str2, &mntlstp) == NULL) {
			cur = insert_strlist(added);
			if (cur == NULL) {
				free_strlist(&mntl);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->str = strdup(tmp->str);
			if (cur->str == NULL) {
				free_strlist(&mntl);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->str2 = strdup(tmp->str2);
			if (cur->str2 == NULL) {
				free_strlist(&mntl);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->type = tmp->type;
			cur->instance = tmp->instance;
			cur->nfs = tmp->nfs;
		}
	}
	for (tmp = mntlstp.head; tmp != NULL; tmp = tmp->next) {
		if (search_strlist(tmp->str, tmp->str2, &mntl) == NULL) {
			cur = insert_strlist(removed);
			if (cur == NULL) {
				free_strlist(&mntl);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->str = strdup(tmp->str);
			if (cur->str == NULL) {
				free_strlist(&mntl);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->str2 = strdup(tmp->str2);
			if (cur->str2 == NULL) {
				free_strlist(&mntl);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->sensor = strdup(tmp->sensor);
			if (cur->sensor == NULL) {
				free_strlist(&mntl);
				free_strlist(added);
				free_strlist(removed);
				return (-1);
			}
			cur->type = tmp->type;
			cur->instance = tmp->instance;
			cur->nfs = tmp->nfs;
		}
	}
	free_strlist(&mntl);
	if (added->head == NULL && removed->head == NULL) {
		/* no changes */
		return (1);
	}
	return (0);
}


static t_i_dsk_disk_sensor *
create_dsk_disk_sensor(t_strlist *toadd, ldinfo_t *ptoi)
{
	t_i_dsk_disk_sensor *res;
	t_strlist *tmp;
	char buf[MAXPATHLEN + 1];
	char diskname[KSTAT_STRLEN];
	char *real;
	char *p;
	int type;
	ldinfo_t *pld;

	if (toadd->str == NULL) {
		return (NULL);
	}
	if (toadd->type == DISK_SENSOR_MNT_TYPE) {
		return (NULL);
	}

	type = toadd->type;

	res = (t_i_dsk_disk_sensor *)malloc(sizeof (t_i_dsk_disk_sensor));
	if (res == NULL) {
		return (NULL);
	}
	(void) memset(res, 0, sizeof (t_i_dsk_disk_sensor));
	if (type == DISK_SENSOR_SVM_TYPE ||
	    type == DISK_SENSOR_NFS_TYPE) {
		res->ks_name = strdup(toadd->str);
		if (res->ks_name == NULL) {
			free(res);
			return (NULL);
		}
		res->module = strdup(toadd->str2);
		if (res->module == NULL) {
			free(res->ks_name);
			free(res);
			return (NULL);
		}
		res->instance = toadd->instance;
		res->dksp = kstat_lookup(kc, res->module,
		    res->instance, res->ks_name);
		return (res);
	}
	res->instance = -1;
	res->slice = (uint32_t)-1;

	p = strchr(toadd->str, ',');
	if (p != NULL) {
		res->slice = (uint32_t)((unsigned char)*(p + 1) - 'a');
	}

	(void) strncpy(diskname, toadd->str, KSTAT_STRLEN - 1);
	diskname[KSTAT_STRLEN - 1] = '\0';
	(void) strtok(diskname, ",");

	tmp = dsklstp.head;
	while (tmp != NULL) {
		(void) sprintf(buf, "/dev/rdsk/%s", tmp->str);
		real = real_device_name(buf);
		if (real == NULL) {
			tmp = tmp->next;
			continue;
		}
		p = strstr(real, "/devices");
		if (p == NULL) {
			tmp = tmp->next;
			continue;
		}
		p += strlen("/devices");
		p[strlen(p) - 6] = '\0';
		pld = find_ldinfo_match(p, ptoi);
		if (pld != NULL) {
			if (pld->dtype != NULL) {
				(void) sprintf(buf,
				    "%s%u", pld->dtype, pld->dnum);
				if (strcmp(buf, diskname) == 0) {
					res->disk = strdup(tmp->str);
					if (res->disk == NULL) {
						free(res);
						return (NULL);
					}
					res->disk[strlen(res->disk) - 2] = '\0';
					res->physname = strdup(p);
					if (res->physname == NULL) {
						free(res->disk);
						free(res);
						return (NULL);
					}
					res->instance = pld->dnum;
					res->module = strdup(pld->dtype);
					if (res->module == NULL) {
						free(res->physname);
						free(res->disk);
						free(res);
						return (NULL);
					}
					res->ks_name = strdup(toadd->str);
					if (res->ks_name == NULL) {
						free(res->module);
						free(res->physname);
						free(res->disk);
						free(res);
						return (NULL);
					}

					/*
					 * pld->dnum doesn't necessarily match
					 * the kstat instance, so we can't use
					 * res->instance as the kstat instance
					 * here, hence the -1. [EL]
					 */
					res->dksp = kstat_lookup(kc,
					    res->module, -1,
					    res->ks_name);
#ifndef DISK_SENSOR_NO_DID_MAPPING
					if (type == DISK_SENSOR_DSK_TYPE) {
						did_device_list_t *didl;
						didl = map_to_did_device(
							res->disk, 0);
						if (didl == NULL) {
							free(res->ks_name);
							free(res->module);
							free(res->physname);
							free(res->disk);
							free(res);
							return (NULL);
						}
						res->did = didl->instance;
					}
#endif
					return (res);
				}
			}
		}
		tmp = tmp->next;
	}
	free(res);
	return (NULL);
}


static t_i_mnt_disk_sensor *
create_mnt_disk_sensor(t_strlist *lst)
{
	t_i_mnt_disk_sensor *res;

	res = (t_i_mnt_disk_sensor *) malloc(sizeof (t_i_mnt_disk_sensor));
	if (res == NULL) {
		return (NULL);
	}
	(void) memset(res, 0, sizeof (t_i_mnt_disk_sensor));
	res->device = strdup(lst->str);
	if (res->device == NULL) {
		free(res);
		return (NULL);
	}
	res->mountp = strdup(lst->str2);
	if (res->mountp == NULL) {
		free(res->device);
		free(res);
		return (NULL);
	}
	return (res);
}


static void
setup_ki(t_i_disk_sensor *tmp, int *count)
{
	uint32_t i;
	if (tmp->type == DISK_SENSOR_DSK_TYPE ||
	    tmp->type == DISK_SENSOR_PRT_TYPE ||
	    tmp->type == DISK_SENSOR_NFS_TYPE ||
	    tmp->type == DISK_SENSOR_SVM_TYPE) {
		for (i = DISK_SENSOR_DSK_KI_START;
			i <= DISK_SENSOR_DSK_KI_END; i++) {
			if ((mkis & disk_sensor_kis[i].nki) != 0) {
				if ((tmp->kis &
				    disk_sensor_kis[i].nki) == 0) {
					tmp->kis |= disk_sensor_kis[i].nki;
					*count = *count + 1;
				}
			}
		}
	}
	if (tmp->type == DISK_SENSOR_MNT_TYPE) {
		for (i = DISK_SENSOR_MNT_KI_START;
			i <= DISK_SENSOR_MNT_KI_END; i++) {
			if (mkis & disk_sensor_kis[i].nki) {
				if ((tmp->kis &
				    disk_sensor_kis[i].nki) == 0) {
					tmp->kis |= disk_sensor_kis[i].nki;
					*count = *count + 2;
				}
			}
		}
	}
}


static void
clear_ki(t_i_disk_sensor *tmp, int *count)
{
	uint32_t i;
	if (tmp->type == DISK_SENSOR_DSK_TYPE ||
	    tmp->type == DISK_SENSOR_PRT_TYPE ||
	    tmp->type == DISK_SENSOR_NFS_TYPE ||
	    tmp->type == DISK_SENSOR_SVM_TYPE) {
		for (i = DISK_SENSOR_DSK_KI_START;
			i <= DISK_SENSOR_DSK_KI_END; i++) {
			if ((tmp->kis & disk_sensor_kis[i].nki) != 0) {
				tmp->kis &= (~(disk_sensor_kis[i].nki));
				*count = *count - 1;
			}
		}
	}
	if (tmp->type == DISK_SENSOR_MNT_TYPE) {
		for (i = DISK_SENSOR_MNT_KI_START;
			i <= DISK_SENSOR_MNT_KI_END; i++) {
			if ((tmp->kis & disk_sensor_kis[i].nki) != 0) {
				tmp->kis &= (~(disk_sensor_kis[i].nki));
				*count = *count - 2;
			}
		}
	}
}


static int
get_ki_usage(t_i_disk_sensor *tmp, uint32_t ki, uint64_t *usage)
{
	int ret = 1;
	uint64_t ldeltas;
	double val;
	double etime, hr_etime;

	etime = 0.0;
	if ((tmp->kis & ki) == 0) {
		return (ret);
	}
	if (tmp->already_read != get_value_time) {
		if (tmp->type == DISK_SENSOR_MNT_TYPE) {
			read_mnt_sensors(tmp);
		} else {
			read_dsk_sensors(tmp);
		}
	}
	if (tmp->already_read == get_value_time) {
		if (tmp->type != DISK_SENSOR_MNT_TYPE) {
			if ((tmp->dsk)->last_snap != 0) {
				hr_etime = (double)
				    (hrtime_delta((tmp->dsk)->last_snap,
				    (tmp->dsk)->snap));
				if (hr_etime == 0.0) {
					hr_etime = (double)NANOSEC;
				}
				etime = hr_etime / (double)NANOSEC;
			}
		}
		switch (ki) {
		case KI_KRS:
			if (etime == 0.0) {
				*usage = (uint64_t)0;
				ret = 0;
				break;
			}
			ldeltas = ull_delta((tmp->dsk->pkios).nread,
			    (tmp->dsk->dkios).nread);
			if (ldeltas != 0) {
				val = (double)ldeltas;
				val /= 1024;
				val /= etime;
			} else {
				val = 0.0;
			}
			*usage = (uint64_t)val;
			ret = 0;
			break;
		case KI_KWS:
			if (etime == 0.0) {
				*usage = (uint64_t)0;
				ret = 0;
				break;
			}
			ldeltas = ull_delta((tmp->dsk->pkios).nwritten,
			    (tmp->dsk->dkios).nwritten);
			if (ldeltas != 0) {
				val = (double)ldeltas;
				val /= 1024;
				val /= etime;
			} else {
				val = 0.0;
			}
			*usage = (uint64_t)val;
			ret = 0;
			break;
		case KI_TRS:
			if (etime == 0.0) {
				*usage = (uint64_t)0;
				ret = 0;
				break;
			}
			ldeltas = ull_delta(
			    (uint64_t)(tmp->dsk->pkios).reads,
			    (uint64_t)(tmp->dsk->dkios).reads);
			val = (double)ldeltas;
			val /= etime;
			*usage = (uint64_t)val;
			ret = 0;
			break;
		case KI_TWS:
			if (etime == 0.0) {
				*usage = (uint64_t)0;
				ret = 0;
				break;
			}
			ldeltas = ull_delta(
			    (uint64_t)(tmp->dsk->pkios).writes,
			    (uint64_t)(tmp->dsk->dkios).writes);
			val = (double)ldeltas;
			val /= etime;
			*usage = (uint64_t)val;
			ret = 0;
			break;
		case KI_BCAPACITY:
			*usage = (uint64_t)((tmp->mnt)->bcapacity);
			ret = 0;
			break;
		case KI_ICAPACITY:
			*usage = (uint64_t)((tmp->mnt)->icapacity);
			ret = 0;
			break;
		default:
			*usage = 0;
			break;
		}
	}
	return (ret);
}


static void
read_mnt_sensors(t_i_disk_sensor *tmp)
{
	struct statvfs64 stvfs;

	if (((tmp->mnt)->mountp != NULL) &&
	    (statvfs64((tmp->mnt)->mountp, &stvfs) == 0)) {
		(void) clock_gettime(CLOCK_REALTIME, &(tmp->time));
		if (stvfs.f_bfree == (fsblkcnt64_t)-1) {
			(tmp->mnt)->bcapacity = 100;
		} else {
			if (stvfs.f_blocks == 0) {
				(tmp->mnt)->bcapacity = 0;
			} else {
				fsblkcnt64_t reserved_blocks =
				    stvfs.f_bfree - stvfs.f_bavail;
				fsblkcnt64_t used_blocks =
				    stvfs.f_blocks - stvfs.f_bfree;
				(tmp->mnt)->bcapacity = (uint64_t)
				    ((((double)used_blocks /
				    (double)(stvfs.f_blocks -
				    reserved_blocks)) * 100.0) + 0.5);
				if ((tmp->mnt)->bcapacity > 100) {
					(tmp->mnt)->bcapacity = 100;
				}
			}
		}
		if (stvfs.f_files != 0) {
			(tmp->mnt)->icapacity = (int64_t)(100.50 -
			    ((((double)stvfs.f_ffree) * 100.00) /
			    ((double)stvfs.f_files)));
			if ((tmp->mnt)->icapacity > 100) {
				(tmp->mnt)->icapacity = 100;
			}
		} else {
			(tmp->mnt)->icapacity = 0;
		}
		tmp->already_read = get_value_time;
	}
}


static void
read_dsk_sensors(t_i_disk_sensor *tmp)
{
	kid_t id;
	if ((tmp->dsk)->dksp != NULL) {
		if ((tmp->dsk)->snap != 0) {
			(void) memcpy(&((tmp->dsk)->pkios),
			    &((tmp->dsk)->dkios), sizeof (kstat_io_t));
			(tmp->dsk)->last_snap = (tmp->dsk)->snap;
		}
		id = kstat_read(kc, (tmp->dsk)->dksp, &((tmp->dsk)->dkios));
		if (id != -1) {
			(void) clock_gettime(CLOCK_REALTIME, &(tmp->time));
			tmp->already_read = get_value_time;
			(tmp->dsk)->snap = (tmp->dsk)->dksp->ks_snaptime;
		}
	}
}


static void
get_all_values(t_i_disk_sensor *itmp, sensor_result_t **sres, int *count)
{
	uint64_t value;
	int i;

	if (itmp->type == DISK_SENSOR_MNT_TYPE) {
		for (i = DISK_SENSOR_MNT_KI_START;
			i <= DISK_SENSOR_MNT_KI_END; i++) {
			if (get_ki_usage(itmp, (1 << i), &value) == 0) {
				(void) strncpy((*sres)->managed_object,
				    itmp->mnt->device, SENSOR_MNG_OBJ_MAX);
				(void) strncpy((*sres)->ki_type,
				    disk_sensor_kis[i].ki,
				    SENSOR_KI_TYPE_MAX);
				(*sres)->usage = value;
				(*sres)->upperbound = (uint64_t)100;
				(*sres)->time_current = itmp->time;
				*sres = *sres + 1;
				*count = *count + 1;
				(void) strncpy((*sres)->managed_object,
				    itmp->mnt->mountp, SENSOR_MNG_OBJ_MAX);
				(void) strncpy((*sres)->ki_type,
				    disk_sensor_kis[i].ki,
				    SENSOR_KI_TYPE_MAX);
				(*sres)->usage = value;
				(*sres)->upperbound = (uint64_t)100;
				(*sres)->time_current = itmp->time;
				*sres = *sres + 1;
				*count = *count + 1;

			}
		}
	} else {
		char name[MAXPATHLEN + 1];
		int fd;
		if (itmp->type == DISK_SENSOR_PRT_TYPE) {
			/* Verify that this partition still exists */
			(void) sprintf(name, "/dev/rdsk/%ss%u",
			    itmp->dsk->disk, itmp->dsk->slice);
			fd = open(name, O_RDONLY|O_NDELAY);
			if (fd < 0) { /* open failed */
				return;
			}
			(void) close(fd);
		}
		if (itmp->type == DISK_SENSOR_DSK_TYPE) {
			for (i = 0; i < 16; i++) {
				/* Verify that a partition still exists */
#ifndef DISK_SENSOR_NO_DID_MAPPING
				(void) sprintf(name, "/dev/did/rdsk/d%ds%u",
					itmp->dsk->did, i);
#else
				(void) sprintf(name, "/dev/rdsk/%ss%u",
				    itmp->dsk->disk, i);
#endif
				fd = open(name, O_RDONLY|O_NDELAY);
				if (fd >= 0) { /* open succeeded */
					(void) close(fd);
					break;
				}
			}
			if (i >= 16) {
				return;
			}
		}
		for (i = DISK_SENSOR_DSK_KI_START;
			i <= DISK_SENSOR_DSK_KI_END; i++) {
			if (get_ki_usage(itmp, (1 << i), &value) == 0) {
				(void) strncpy((*sres)->managed_object,
				    itmp->name, SENSOR_MNG_OBJ_MAX);
				(void) strncpy((*sres)->ki_type,
				    disk_sensor_kis[i].ki,
				    SENSOR_KI_TYPE_MAX);
				(*sres)->usage = value;
				(*sres)->upperbound = (uint64_t)0;
				(*sres)->time_current = itmp->time;
				*sres = *sres + 1;
				*count = *count + 1;
			}
		}
	}
}


uint64_t
ull_delta(uint64_t old, uint64_t new)
{
	if (new >= old) {
		return (new - old);
	} else {
		return ((UINT64_MAX - old) + new + 1);
	}
}


/*
 * Return the number of ticks delta between two hrtime_t
 * values. Attempt to cater for various kinds of overflow
 * in hrtime_t - no matter how improbable.
 */
static uint64_t
hrtime_delta(hrtime_t old, hrtime_t new)
{
	uint64_t del;

	if ((new >= old) && (old >= 0L))
		return (uint64_t)(new - old);
	else {
		/*
		 * We've overflowed the positive portion of an
		 * hrtime_t.
		 */
		if (new < 0L) {
			/*
			 * The new value is negative. Handle the
			 * case where the old value is positive or
			 * negative.
			 */
			uint64_t n1;
			uint64_t o1;

			n1 = (uint64_t)-new;
			if (old > 0L)
				return (uint64_t)(n1 - (uint64_t)old);
			else {
				o1 = (uint64_t)-old;
				del = n1 - o1;
				return (uint64_t)(del);
			}
		} else {
			/*
			 * Either we've just gone from being negative
			 * to positive *or* the last entry was positive
			 * and the new entry is also positive but *less*
			 * than the old entry. This implies we waited
			 * quite a few days on a very fast system between
			 * iostat displays.
			 */
			if (old < 0L) {
				uint64_t o2;

				o2 = (uint64_t)-old;
				del = UINT64_MAX - o2;
			} else {
				del = UINT64_MAX - (uint64_t)old;
			}
			del += (uint64_t)new;
			return (uint64_t)(del);
		}
	}
}



/*
 *
 * Public functions: the library API
 *
 */

/*
 * The get_values sensor API
 */
int
get_values(char **managed_objects, int mo_size, sensor_result_t **results,
	int *size, char error_string[])
{
	ldinfo_t *ptoi;
	t_strlistp added;
	t_strlistp removed;
	t_strlist *lst;
	t_strlist *cur;
	t_i_dsk_disk_sensor *sdisk;
	t_i_mnt_disk_sensor *smnt;
	int res;
	ub4 bucket = 0;
	t_i_disk_sensor *itmp;
	t_i_disk_sensor *iprev;
	int sensor_type;
	sensor_result_t *sres;
	char sensor[SENSOR_MNG_OBJ_MAX];
	static sensor_result_t *iresults = NULL;
	static int iresults_cnt = 0;
	int i;
	int cnt = 0;
	static int kstat_opened = 0;

	if (kstat_opened == 0) {
		if ((kc = kstat_open()) == NULL) {
			(void) snprintf(error_string, ERROR_STRING_MAX,
				"Unable to open kstats\n");
			return (1);
		}
		kstat_opened = 1;
	}

	get_value_time = gethrtime();

	added.head = added.tail = NULL;
	removed.head = removed.tail = NULL;

	/* New or removed mount points ? */
	res = changed_in_mnts(&added, &removed);
	if (res == 0) {
		/* Something has changed */
		for (lst = added.head; lst != NULL; lst = lst->next) {
			smnt = create_mnt_disk_sensor(lst);
			if (smnt == NULL) {
				continue;
			}
			sensor_type = lst->type;
			if (snprintf(sensor, SENSOR_MNG_OBJ_MAX,
			    "mnt:%s", smnt->device) >= SENSOR_MNG_OBJ_MAX) {
				free(smnt);
				continue;
			}
			itmp = search_disksensor(sensor, &bucket,
				&iprev);
			if (itmp == NULL) {
				itmp = append_disksensor(sensor, sensor_type,
				    &(idisksensors[bucket]), &iprev);
				if (itmp != NULL) {
					itmp->mnt = smnt;
					setup_ki(itmp, &disk_kis_cnt);
					cur = insert_strlist(&mntlstp);
					if (cur != NULL) {
						cur->str = strdup(lst->str);
						if (cur->str == NULL) {
							(void) remove_strlist(
							    cur, &mntlstp);
							clear_ki(itmp,
							    &disk_kis_cnt);
							(void)
							    remove_disksensor(
							    itmp, iprev);
							continue;
						}
						cur->str2 = strdup(lst->str2);
						if (cur->str2 == NULL) {
							(void) remove_strlist(
							    cur, &mntlstp);
							clear_ki(itmp,
							    &disk_kis_cnt);
							(void)
							    remove_disksensor(
							    itmp, iprev);
							continue;
						}
						cur->sensor =
							strdup(itmp->name);
						if (cur->sensor == NULL) {
							(void) remove_strlist(
							    cur, &mntlstp);
							clear_ki(itmp,
							    &disk_kis_cnt);
							(void)
							    remove_disksensor(
							    itmp, iprev);
							continue;
						}
						cur->type = lst->type;
						cur->instance = lst->instance;
						cur->nfs = lst->nfs;
					} else {
						clear_ki(itmp, &disk_kis_cnt);
						(void) remove_disksensor(itmp,
						    iprev);
					}
				}
			}
		}
		if (removed.head != NULL) {
			for (lst = removed.head; lst != NULL; lst = lst->next) {
				itmp = search_disksensor(lst->sensor, &bucket,
				    &iprev);
				if (itmp != NULL) {
					clear_ki(itmp, &disk_kis_cnt);
					(void) remove_disksensor(itmp, iprev);
				}
				(void) remove_strlist(lst, &mntlstp);
			}
		}
	}
	free_strlist(&added);
	free_strlist(&removed);

	/* New or removed disk/part/md kstats ? */
	res = changed_in_kstats(&added, &removed);
	if (res == 0) {
		discover_disks();
		ptoi = rummage_devinfo();
		for (lst = added.head; lst != NULL; lst = lst->next) {
			sensor_type = lst->type;
			sdisk = create_dsk_disk_sensor(lst, ptoi);
			if (sdisk == NULL) {
				continue;
			}
			if (sensor_type == DISK_SENSOR_DSK_TYPE) {
#ifndef DISK_SENSOR_NO_DID_MAPPING
				if (snprintf(sensor,
					SENSOR_MNG_OBJ_MAX,
					"/dev/did/rdsk/d%d", sdisk->did) >=
					SENSOR_MNG_OBJ_MAX) {
					free(sdisk);
					continue;
				}
#else
				if (snprintf(sensor,
				    SENSOR_MNG_OBJ_MAX,
				    "dsk:%s::", sdisk->disk) >=
				    SENSOR_MNG_OBJ_MAX) {
					free(sdisk);
					continue;
				}
#endif
			}
			if (sensor_type == DISK_SENSOR_SVM_TYPE) {
				if (snprintf(sensor,
				    SENSOR_MNG_OBJ_MAX,
				    "svm:%s", sdisk->ks_name) >=
				    SENSOR_MNG_OBJ_MAX) {
					free(sdisk);
					continue;
				}
			}
			if (sensor_type == DISK_SENSOR_NFS_TYPE) {
				char *nfs_name =
				    lookup_nfs_name(sdisk->ks_name);
				if (nfs_name == NULL) {
					free(sdisk);
					continue;
				} else {
					if (snprintf(sensor,
					    SENSOR_MNG_OBJ_MAX,
					    "nfs:%s", nfs_name) >=
					    SENSOR_MNG_OBJ_MAX) {
						free(nfs_name);
						free(sdisk);
						continue;
					}
					free(nfs_name);
				}
			}
			if (sensor_type == DISK_SENSOR_PRT_TYPE) {
				if (snprintf(sensor,
				    SENSOR_MNG_OBJ_MAX,
				    "dsk:%s:%u",
				    sdisk->disk, sdisk->slice) >=
				    SENSOR_MNG_OBJ_MAX) {
					free(sdisk);
					continue;
				}
			}
			itmp = search_disksensor(sensor, &bucket,
				&iprev);
			if (itmp == NULL) {
				itmp = append_disksensor(sensor, sensor_type,
				    &(idisksensors[bucket]), &iprev);
				if (itmp != NULL) {
					itmp->dsk = sdisk;
					setup_ki(itmp, &disk_kis_cnt);
					cur = insert_strlist(&kstlstp);
					if (cur != NULL) {
						cur->str = strdup(lst->str);
						if (cur->str == NULL) {
							(void) remove_strlist(
							    cur, &kstlstp);
							clear_ki(itmp,
							    &disk_kis_cnt);
							(void)
							    remove_disksensor(
							    itmp, iprev);
							continue;
						}
						cur->sensor =
							strdup(itmp->name);
						if (cur->sensor == NULL) {
							(void) remove_strlist(
							    cur, &kstlstp);
							clear_ki(itmp,
							    &disk_kis_cnt);
							(void)
							    remove_disksensor(
							    itmp, iprev);
							continue;
						}
						cur->type = lst->type;
					} else {
						clear_ki(itmp, &disk_kis_cnt);
						(void) remove_disksensor(itmp,
						    iprev);
					}
				}
			} else {
				free(sdisk);
			}
		}
		for (lst = removed.head; lst != NULL; lst = lst->next) {
			itmp = search_disksensor(lst->sensor, &bucket, &iprev);
			if (itmp != NULL) {
				clear_ki(itmp, &disk_kis_cnt);
				(void) remove_disksensor(itmp, iprev);
			}
			(void) remove_strlist(lst, &kstlstp);
		}
		cleanup_ldinfo(ptoi);
		free_strlist(&dsklstp);
	}
	free_strlist(&added);
	free_strlist(&removed);

	/* Internal results table handling */
	if (iresults == NULL) {
		iresults = (sensor_result_t *)malloc(((size_t)(disk_kis_cnt)) *
		    sizeof (sensor_result_t));
		if (iresults != NULL) {
			iresults_cnt = disk_kis_cnt;
		}
	} else {
		if (disk_kis_cnt > iresults_cnt) {
			iresults = (sensor_result_t *)realloc(iresults,
			    ((size_t)disk_kis_cnt) * sizeof (sensor_result_t));
			if (iresults != NULL) {
				iresults_cnt = disk_kis_cnt;
			} else {
				iresults_cnt = 0;
			}
		}
	}
	if (iresults == NULL) {
		(void) snprintf(error_string, ERROR_STRING_MAX,
			"Unable to allocate internal results array\n");
		return (1);
	}
	(void) memset(iresults, 0,
	    ((unsigned int)iresults_cnt) * sizeof (sensor_result_t));

	if (managed_objects == NULL) {
		sres = iresults;
		cnt = 0;
		for (i = 0; (i < (int)(hashsize(DISK_SENSOR_BUCKETS_BITS))) &&
		    (cnt <= iresults_cnt); i++) {
			itmp = &(idisksensors[i]);
			while (itmp != NULL) {
				if (itmp->name != NULL) {
					get_all_values(itmp, &sres, &cnt);
				}
				itmp = itmp->next;
			}
		}
		*results = iresults;
		*size = cnt;
	} else {
		char **obj = managed_objects;
		sres = iresults;
		cnt = 0;
		for (i = 0; (i < mo_size) && (cnt <= iresults_cnt); i++) {
			itmp = search_disksensor(*obj, &bucket, &iprev);
			if (itmp != NULL) {
				get_all_values(itmp, &sres, &cnt);
			}
			obj++;
		}
		*results = iresults;
		*size = cnt;
	}
	return (0);
}


/*
 * The start sensor API
 */
/*ARGSUSED*/
int
start(char *KItype, char error_string[])
{
	uint32_t ki = get_ki_bit(KItype);
	if (ki != 0) {
		mkis |= ki;
	} else {
		(void) snprintf(error_string, ERROR_STRING_MAX,
		    "Unknow KItype\n");
	}
	return (0);
}


/*
 * The stop sensor API
 */
/*ARGSUSED*/
int
stop(char *KItype, char error_string[])
{
	uint32_t ki = get_ki_bit(KItype);
	if (ki != 0) {
		mkis &= (~ki);
	} else {
		(void) snprintf(error_string, ERROR_STRING_MAX,
		    "Unknow KItype\n");
	}
	return (0);
}


#ifdef STANDALONE
/*
 * For development and basic testing,
 * in standalone mode.
 */
int
main(int argc, char *argv[])
{
	unsigned int loop = 1;
	sensor_result_t *results = NULL;
	sensor_result_t *sres;
	char *managed_objs = NULL;
	char error[ERROR_STRING_MAX];
	int size = 0;
	hrtime_t hstart;
	hrtime_t hend;
	unsigned int i;
	int res;
	t_i_disk_sensor *itmp;
	char serror[32];
	char *tobj[3] = {
		"dsk:c0t0d0::",
		"dsk:c0t0d0:0",
		"mnt:/dev/dsk/c0t0d0s0" };


	(void) start("ki.sunw.rbyte.rate", serror);
	(void) start("ki.sunw.wbyte.rate", serror);
	(void) start("ki.sunw.read.rate", serror);
	(void) start("ki.sunw.write.rate", serror);
	(void) start("ki.sunw.block.used", serror);
	(void) start("ki.sunw.inode.used", serror);

	if (argc >= 2) {
		loop = (unsigned int)atoi(argv[1]);
	}

	while (loop > 0) {
		(void) printf("loop %d\n", loop);
		loop--;
		hstart = gethrvtime();
		res = get_values(NULL, 0, &results, &size, error);
		hend = gethrvtime();
		(void) printf("get_values %lld res %d size %u objects "
		    "%u (d %u m %u) kis"
		    " %u active\n",
		    hend - hstart, res, size, disk_objects_cnt,
		    disk_objects_dsk_cnt, disk_objects_mnt_cnt,
		    disk_kis_cnt);
		if (argc == 4) {
			int cnt = size;
			sres = results;
			while (cnt > 0) {
				(void) printf("(%d) <%s><%s>"
					"%lld (%u %u)\n",
					cnt,
					sres->managed_object,
					sres->ki_type,
					sres->usage,
					(sres->time_current).tv_sec,
					(sres->time_current).tv_nsec);
				sres++;
				cnt--;
			}
		}
		if (argc >= 3) {
			(void) printf("sleep %d\n", atoi(argv[2]));
			(void) sleep((unsigned int)(atoi(argv[2])));
		}
	}

	(void) printf("hash_collisions %u\n", hash_collisions);

	hstart = gethrvtime();
	res = get_values(tobj, 3, &results, &size, error);
	hend = gethrvtime();
	(void) printf("OBJS NOT NULL get_values %lld res %d size %u objects "
	    "%u (d %u m %u) kis"
	    " %u active\n",
	    hend - hstart, res, size, disk_objects_cnt,
	    disk_objects_dsk_cnt, disk_objects_mnt_cnt,
	    disk_kis_cnt);
	if (argc == 4) {
		int cnt = size;
		sres = results;
		while (cnt > 0) {
			(void) printf("(%d) <%s><%s>"
				"%lld (%u %u)\n",
				cnt,
				sres->managed_object,
				sres->ki_type,
				sres->usage,
				(sres->time_current).tv_sec,
				(sres->time_current).tv_nsec);
			sres++;
			cnt--;
		}
	}

	/* cleanup check */
	for (i = 0; i < hashsize(DISK_SENSOR_BUCKETS_BITS); i++) {
		itmp = &(idisksensors[i]);
		while (itmp != NULL) {
			t_i_disk_sensor *sv = itmp->next;
			if (itmp->name != NULL) {
				clear_ki(itmp, &disk_kis_cnt);
				free(itmp->name);
				if (itmp->dsk != NULL) {
					if (itmp->dsk->ks_name != NULL) {
						free(itmp->dsk->ks_name);
					}
					if (itmp->dsk->disk != NULL) {
						free(itmp->dsk->disk);
					}
					if (itmp->dsk->module != NULL) {
						free(itmp->dsk->module);
					}
					if (itmp->dsk->physname != NULL) {
						free(itmp->dsk->physname);
					}
					free(itmp->dsk);
				}
				if (itmp->mnt != NULL) {
					if (itmp->mnt->device != NULL) {
						free(itmp->mnt->device);
					}
					if (itmp->mnt->mountp != NULL) {
						free(itmp->mnt->mountp);
					}
					free(itmp->mnt);
				}
				itmp->name = NULL;
				itmp->dsk = NULL;
				itmp->mnt = NULL;
				itmp->time.tv_sec = 0;
				itmp->time.tv_nsec = 0;
				if (itmp != &(idisksensors[i])) {
					hash_collisions--;
					/*LINTED*/
					free(itmp);
				}
			}
			itmp = sv;
		}
	}
	free_strlist(&kstlstp);
	free_strlist(&mntlstp);
	free_strlist(&dsklstp);
	(void) printf("EXIT objects %u (d %u m %u) kis %u active\n",
	    disk_objects_cnt, disk_objects_dsk_cnt, disk_objects_mnt_cnt,
	    disk_kis_cnt);
	(void) printf("hash_collisions %u\n", hash_collisions);
	(void) kstat_close(kc);
	return (0);
}
#endif
