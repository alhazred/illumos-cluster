/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * Copyright (c) 1990  Mentat Inc.
 * netstat.c 2.2, last change 9/9/91
 * MROUTING Revision 3.5
 */

#pragma ident	"@(#)libnetdevsensor.c	1.8	08/05/20 SMI"

#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <kstat.h>
#include <unistd.h>
#include <stddef.h>	/* offsetof */
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <libnvpair.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>	/* hrtime_t */
#include <stropts.h>	/* ioctl */
#include <net/if.h>	/* LIFNAMSIZ */
#include <sys/tihdr.h>	/* PRIM_type, */
#include <inet/mib2.h>	/* Octet_t, IpAddress, Counter, */


#include "sensors.h"
#include "common.h"
#include "debug.h"
#include "s_list.h"

/*
 * Network Device Sensor
 *
 * This sensor retrives network device statistics.
 *
 * Returned values are integers expressed in bytes/s and packets/s, depending on
 * the type of key indicator.
 */

#define	STANDALONE 0

#if !DEBUG
#if STANDALONE
#error STANDALONE should not be defined in non-debug mode
#endif
#endif

static char ifspeed_kstat_name[] = "ifspeed";

/* debug level */
int _dbg_flgs = DBG_1;

struct net_device {
	char			name[SENSOR_MNG_OBJ_MAX];
	s_list_node_t		node;
	kstat_t			*ksp;
	nvlist_t		*nvlp;
};

/*
 * Key Indicator.
 */

struct key_indicator {
	char		name[SENSOR_KI_TYPE_MAX];
	char		*kdn;	 /* kstat data name */
	int		type;
	int		active;
};

#define	KI_TYPE_NO	0
#define	KI_TYPE_BT	1 /* byte */
#define	KI_TYPE_PK	2 /* packet */

static struct key_indicator ki_tab[] =
{
	{"ki.sunw.wbyte.rate",		"obytes64",	KI_TYPE_BT, 0},
	{"ki.sunw.rbyte.rate",		"rbytes64",	KI_TYPE_BT, 0},
	{"ki.sunw.ipacket.rate",	"ipackets64",	KI_TYPE_PK, 0},
	{"ki.sunw.opacket.rate",	"opackets64",	KI_TYPE_PK, 0},
	{"",				"",		KI_TYPE_NO, 0}
};

#define	ki_for_each(ki) for (ki = &ki_tab[0]; ki->type != KI_TYPE_NO; ki++)

static unsigned int	n_active_ki = 0;	/* number of enabled KIs */
static s_list_t		netdev_list;		/* netdev list */
static kstat_ctl_t	*kc;			/* kstat control pointer */

static struct net_device *netdev_zalloc(void);
static void netdev_destroy(struct net_device *);
static struct net_device *netdev_create(char *);
static void netdev_insert(s_list_t *, struct net_device *);
static void netdev_remove(s_list_t *, struct net_device *);
static struct net_device *netdev_lookup(s_list_t *, const char *);
static void netdev_list_create(s_list_t *);
static void netdev_list_destroy(s_list_t *);
static int mib_open(void);
static int mibip_read(int, s_list_t *);
static int handle_mib_item(unsigned long, void *, int, s_list_t *);
static int handle_item_v4(mib2_ipAddrEntry_t *, int, s_list_t *);
static int handle_item_v6(mib2_ipv6AddrEntry_t *, int, s_list_t *);
static char *octetstr(const Octet_t *, int, char *, uint_t);
static int get_kstat_val(kstat_t *, char *, uint64_t *);
static int comp_ki_val(struct net_device *, char *, uint64_t, uint64_t *);
static void cpy_error_str(char[]);
static int insert_result(sensor_result_t **, int *, timespec_t, const char[],
	const char[], uint64_t, uint64_t);


/*
 * --- API functions ---
 */

/*
 * start(ki_name, error) is used to register the KI ki_name within the sensor.
 * It returns 0 on success, and 1 with an error messsage in the string error on
 * failure.
 */
int
start(char ki_name[], char error[])
{
	int gotit = 0;
	struct key_indicator *ki;

	unset_error_str();

	ki_for_each(ki) {
		if (strncmp(ki->name, ki_name, SENSOR_KI_TYPE_MAX) == 0) {
			gotit = 1;
			break;
		}
	}

	if (!gotit) {
		set_error_str("KI %s isn't supported", ki_name);
		cpy_error_str(error);
		return (1);
	}


	if (ki->active == 0 && n_active_ki++ == 0) {

		/*
		 * Sensor initialization.
		 */

		netdev_list_create(&netdev_list);

		DBG(DL2, ("up\n"));

	}

	ki->active = 1;

	return (0);
}

/*
 * stop(ki_name, error) is used to unregister the KI ki_name within the sensor.
 * It returns 0 on success, and 1 with an error messsage in the string error on
 * failure.
 */
int
stop(char ki_name[], char error[])
{
	int gotit = 0;
	struct key_indicator *ki;

	unset_error_str();

	ki_for_each(ki) {
		if (strncmp(ki->name, ki_name, SENSOR_KI_TYPE_MAX) == 0) {
			gotit = 1;
			break;
		}
	}

	if (!gotit) {
		set_error_str("KI %s isn't supported", ki_name);
		cpy_error_str(error);
		return (1);
	}

	if (ki->active == 1 && --n_active_ki == 0) {

		/*
		 * Sensor finalization.
		 */

		netdev_list_destroy(&netdev_list);

		DBG(DL2, ("down\n"));
	}

	ki->active = 0;

	return (0);
}

/*
 * get_values(mobjs, n_mobjs, results, n_results, error) place CPU accounting
 * values in table results for the registered KIs and for the Managed Objects
 * given in mobjs. It returns 0 on success, and 1 with an error message in the
 * string error on failure.
 */
int
get_values(char **mobjs, int n_mobjs,
	sensor_result_t **results, int *n_results, char error[])
{
	int n_res;
	s_list_t list;	/* accounting entity list */
	struct timespec tp;
	sensor_result_t *res;
	struct net_device *iter;	/* accounting entity */

	DBG(DL2, ("sampling\n"));

	unset_error_str();

	/*
	 * Sanity checks.
	 */

	if (n_active_ki == 0) {
		DBG(DL1, ("No KI enabled\n"));
		*n_results = 0;
		return (0);
	}

	if (clock_gettime(CLOCK_REALTIME, &tp) < 0) {
		/*lint -e746 */
		set_error_str("clock_gettime failed (%s)", strerror(errno));
		/*lint +e746 */
		cpy_error_str(error);
		return (1);
	}

	if ((kc = kstat_open()) == NULL) {
		set_error_str("kstat_open failed (%s)", strerror(errno));
		cpy_error_str(error);
		return (1);
	}

	netdev_list_create(&list);

	/*
	 * Create accounting entity list.
	 */

	if (mobjs == NULL || n_mobjs == 0) {
		int fd;

		if ((fd = mib_open()) < 0) {
			cpy_error_str(error);
			netdev_list_destroy(&list);
			(void) kstat_close(kc);
			return (1);
		}

		if (mibip_read(fd, &list) < 0) {
			cpy_error_str(error);
			(void) close(fd);
			netdev_list_destroy(&list);
			(void) kstat_close(kc);
			return (1);
		}

		(void) close(fd);

	} else {
		int i;

		/*
		 * Monitor network devices we're given.
		 */

		for (i = 0; i < n_mobjs; i++) {
			kstat_t *ksp;
			struct net_device *netdev;

			/* look up kstat object */
			if ((ksp = kstat_lookup(kc, NULL, -1, mobjs[i]))
				== NULL) {

				/*
				 * No kstat object for device mobjs[i]. User
				 * gave us garbage or the kstat object has been
				 * destroyed from under his feet.
				 */

				DBG(DL1, ("kstat_lookup failed for %s (%s)\n",
					mobjs[i], strerror(errno)));

				continue;
			}

			/* get snapshot */
			if (kstat_read(kc, ksp, NULL) < 0) {

				/*
				 * kstat_read failed probably because the kernel
				 * doesn't have the appropriate kstat object.
				 * The kstat object most probably got destroyed
				 * from under us.
				 */

				DBG(DL1, ("kstat_read failed for %s (%s)\n",
					mobjs[i], strerror(errno)));

				continue;
			}

			if ((netdev = netdev_create(mobjs[i])) == NULL) {
				cpy_error_str(error);
				netdev_list_destroy(&list);
				(void) kstat_close(kc);
				return (1);
			}

			netdev->ksp = ksp;
			netdev_insert(&list, netdev);
		}
	}

	/*
	 * Walk through accounting entity list.
	 */

	res = *results;
	n_res = 0;

	iter = s_list_head(&list);

	while (iter != NULL) {
		struct key_indicator *ki;
		struct net_device *next = s_list_next(&list, iter);
		uint64_t ifspeed = (uint64_t)0;

		assert(kc != NULL);
		assert(iter != NULL);
		assert(iter->ksp != NULL);

		DBG(DL2, ("managed network device: %s\n", iter->name));

		/* get that network device's speed */
		if (get_kstat_val(iter->ksp, ifspeed_kstat_name, &ifspeed)
			< 0) {

			cpy_error_str(error);
			netdev_list_destroy(&list);
			(void) kstat_close(kc);
			return (1);
		}

		/* ifspeed is expressed in bits/s, make it bytes/s */
		ifspeed /= 8;

		ki_for_each(ki) {
			int ret;
			uint64_t kstat_val, ki_val;
			uint64_t ubound = (uint64_t)0;

			if (ki->active == 0)
				continue;

			if ((ret = get_kstat_val(iter->ksp, ki->kdn,
				&kstat_val)) < 0) {

				cpy_error_str(error);
				netdev_list_destroy(&list);
				(void) kstat_close(kc);
				return (1);
			}

			if (ret > 0) /* no data available */
				continue;

			if ((ret = comp_ki_val(iter, ki->kdn, kstat_val,
				&ki_val)) < 0) {

				cpy_error_str(error);
				netdev_list_destroy(&list);
				(void) kstat_close(kc);
				return (1);
			}

			if (ret > 0) /* no data available */
				continue;

			/* packet type has no upper bound value */
			if (ki->type == KI_TYPE_BT)
				ubound = ifspeed;

			if (insert_result(&res, &n_res, tp, ki->name,
				iter->name, ki_val, ubound) < 0) {

				cpy_error_str(error);
				netdev_list_destroy(&list);
				(void) kstat_close(kc);
				return (1);
			}
		}
		iter = next;
	}

	netdev_list_destroy(&list);
	(void) kstat_close(kc);

	/* update caller's pointers */
	*results = res;
	*n_results = n_res;

	return (0);
}


/*
 * ---	net_device related functions ---
 */

/*
 * netdev_zalloc() allocates net_device object and places zeros into it.
 */
static struct net_device *
netdev_zalloc(void)
{
	struct net_device *netdev;

	if ((netdev = (struct net_device *)malloc(sizeof (*netdev))) == NULL) {
		set_error_str("malloc failed (%s)", strerror(errno));
		return (NULL);
	}

	(void) memset(netdev, 0, sizeof (*netdev));

	return (netdev);
}

/*
 * netdev_destroy(netdev) free the memory resources associated to the net_device
 * object netdev.
 */
static void
netdev_destroy(struct net_device *netdev)
{
	DBG(DL4, ("destroying netdev %s\n", netdev->name));

	nvlist_free(netdev->nvlp);
	free(netdev);
}

/*
 * netdev_create(name) created a net_device object named name.
 */
static struct net_device *
netdev_create(char *name)
{
	nvlist_t *nvlp;
	struct net_device *netdev;

	DBG(DL4, ("creating netdev %s\n", name));

	if ((netdev = netdev_zalloc()) == NULL)
		return (NULL);

	if (nvlist_alloc(&nvlp, NV_UNIQUE_NAME, 0) != 0) {
		set_error_str("nvlist_alloc failed (%s)", strerror(errno));
		free(netdev);
		return (NULL);
	}

	(void) strncpy(netdev->name, name, SENSOR_MNG_OBJ_MAX);
	netdev->nvlp = nvlp;

	return (netdev);
}

/*
 * netdev_insert(list, netdev) inserts the net_device object netdev into list.
 */
static void
netdev_insert(s_list_t *list, struct net_device *netdev)
{
	s_list_insert_tail(list, netdev);
}

/*
 * netdev_remove(list, netdev) removes the net_device object netdev from list.
 */
static void
netdev_remove(s_list_t *list, struct net_device *netdev)
{
	s_list_remove(list, netdev);
}

/*
 * netdev_lookup(list, name) returns a reference to the net_device object named
 * name in list, and NULL if there's no such object in list.
 */
static struct net_device *
netdev_lookup(s_list_t *list, const char *name)
{
	struct net_device *netdev = s_list_head(list);

	while (netdev != NULL) {
		if (strncmp(name, netdev->name, SENSOR_MNG_OBJ_MAX) == 0)
			break;
		netdev = s_list_next(list, netdev);
	}

	return (netdev);
}

/*
 * netdev_list_create(list) created the list of net_device objects pointed to by
 * list.
 */
static void
netdev_list_create(s_list_t *list)
{
	/*lint -e413 */
	s_list_create(list, sizeof (struct net_device),
		offsetof(struct net_device, node));
	/*lint +e413 */
}

/*
 * netdev_list_destroy(list) removes and destroys all the objects contained in
 * the list referenced by list. After calling this function list is empty but
 * mot destroyed.
 */
static void
netdev_list_empty(s_list_t *list)
{
	struct net_device *netdev = s_list_head(list);

	while (netdev != NULL) {
		struct net_device *next;

		next = s_list_next(list, netdev);
		netdev_remove(list, netdev);
		netdev_destroy(netdev);
		netdev = next;
	}
}

/*
 * netdev_list_destroy(list) empties thelist referenced by list and destroys it.
 */
static void
netdev_list_destroy(s_list_t *list)
{
	netdev_list_empty(list);
	s_list_destroy(list);
}

/*
 * --- MIB related functions ---
 */

/*
 * mib_open() takes care of opening the MIB.
 */
static int
mib_open(void)
{
	int	mib_fd;

	if ((mib_fd = open("/dev/arp", O_RDWR)) < 0) {
		set_error_str("open failed (%s)", strerror(errno));
		return (-1);
	}
	if (ioctl(mib_fd, I_PUSH, "tcp") == -1) {
		set_error_str("ioctl failed (%s)", strerror(errno));
		(void) close(mib_fd);
		return (-1);
	}
	if (ioctl(mib_fd, I_PUSH, "udp") == -1) {
		set_error_str("ioctl failed (%s)", strerror(errno));
		(void) close(mib_fd);
		return (-1);
	}
	if (ioctl(mib_fd, I_PUSH, "icmp") == -1) {
		set_error_str("ioctl failed (%s)", strerror(errno));
		(void) close(mib_fd);
		return (-1);
	}
	return (mib_fd);
}

/*
 * mibip_read(mib_fd, list) walks the IP MIB (mib_fd), looks up items of types
 * MIB2_IP_ADDR and MIB2_IP6_ADDR, and updates the list of network devices
 * (list) based on the address entries found in each item.
 */
static int
mibip_read(int mib_fd, s_list_t *list)
{
	int n_item;
	uintptr_t buf[512 / sizeof (uintptr_t)];
	struct strbuf ctlbuf, databuf;
	struct T_optmgmt_req *tor = (struct T_optmgmt_req *)buf;
	struct T_optmgmt_ack *toa = (struct T_optmgmt_ack *)buf;
	struct T_error_ack *tea = (struct T_error_ack *)buf;
	struct opthdr *req;

	tor->PRIM_type = T_SVR4_OPTMGMT_REQ;
	tor->OPT_offset = sizeof (struct T_optmgmt_req);
	tor->OPT_length = sizeof (struct opthdr);
	tor->MGMT_flags = T_CURRENT;
	req = (struct opthdr *)&tor[1];
	req->level = MIB2_IP;
	req->name = 0;
	req->len = 0;

	ctlbuf.buf = (char *)buf;
	ctlbuf.len = tor->OPT_length + tor->OPT_offset;
	if (putmsg(mib_fd, &ctlbuf, (struct strbuf *)NULL, 0) != 0) {
		set_error_str("putmsg failed (%s)", strerror(errno));
		return (-1);
	}

	req = (struct opthdr *)&toa[1];
	ctlbuf.maxlen = sizeof (buf);
	n_item = 1;

	for (;;) {
		int ret;
		int flags = 0;

		ctlbuf.len = 0;
		if ((ret = getmsg(mib_fd, &ctlbuf, (struct strbuf *)NULL,
			&flags)) < 0) {

			set_error_str("getmsg failed (%s)", strerror(errno));
			return (-1);
		}

		/* check EOD */
		if (ret == 0 &&
		    (size_t)ctlbuf.len >= sizeof (struct T_optmgmt_ack) &&
		    toa->PRIM_type == T_OPTMGMT_ACK &&
		    toa->MGMT_flags == T_SUCCESS &&
		    req->len == 0L) {

			break;
		}

		/* check errors */
		if ((size_t)ctlbuf.len >= sizeof (struct T_error_ack) &&
		    tea->PRIM_type == T_ERROR_ACK) {

			DBG(DL1, ("mib %d gives T_ERROR_ACK: "
				"TLI_error = 0x%lx, "
				"UNIX_error = 0x%lx\n",
				n_item, tea->TLI_error, tea->UNIX_error));

			set_error_str(__SENSOR_BUG);
			return (-1);
		}

		if (ret != MOREDATA ||
		    (size_t)ctlbuf.len < sizeof (struct T_optmgmt_ack) ||
		    toa->PRIM_type != T_OPTMGMT_ACK ||
		    toa->MGMT_flags != T_SUCCESS) {

			DBG(DL1, ("mib %d returned %d, "
				"ctlbuf.len = %d, PRIM_type = %ld\n",
				n_item, ret, ctlbuf.len, toa->PRIM_type));

			set_error_str(__SENSOR_BUG);
			return (-1);
		}

		/* ok we have a mib entry */
		databuf.maxlen = (int)req->len;
		if ((databuf.buf = (char *)malloc((size_t)databuf.maxlen))
			== NULL) {

			set_error_str("malloc failed (%s)", strerror(errno));
			return (-1);
		}

		databuf.len = 0;
		flags = 0;
		if (getmsg(mib_fd, (struct strbuf *)NULL, &databuf, &flags)
			< 0) {

			set_error_str("getmsg failed (%s)", strerror(errno));
			free(databuf.buf);
			return (-1);
		}

		n_item++;

		/* if this mib item is not of interest we jump to next item */
		if ((req->level != MIB2_IP &&
				req->level != MIB2_IP6) ||
			(req->level == MIB2_IP &&
				req->name != MIB2_IP_ADDR) ||
			(req->level == MIB2_IP6 &&
				req->name != MIB2_IP6_ADDR)) {

			free(databuf.buf);
			continue;
		}

		if (handle_mib_item(req->level, (void *)databuf.buf,
			databuf.maxlen, list) < 0) {

			free(databuf.buf);
			return (-1);
		}

		free(databuf.buf);
	}

	return (0);
}

/*
 * handle_mib_item(level, buf, buf_len, list) handles the MIB item located in
 * the buffer buf, it calls handle_item_v4 if it's an IPV4 item or
 * handle_item_v6 if it's an IPV6 item. 0 is returned on success, -1 on failure.
 */
static int
handle_mib_item(unsigned long level, void *buf, int buf_len, s_list_t *list)
{
	int ret = 0;

	if (level == MIB2_IP) {
		mib2_ipAddrEntry_t *ae = (mib2_ipAddrEntry_t *)buf;
		int max_n_ae = buf_len / (int)sizeof (*ae);

		ret = handle_item_v4(ae, max_n_ae, list);

	} else if (level == MIB2_IP6) {
		mib2_ipv6AddrEntry_t *ae = (mib2_ipv6AddrEntry_t *)buf;
		int max_n_ae = buf_len / (int)sizeof (*ae);

		ret = handle_item_v6(ae, max_n_ae, list);

	} else {
		set_error_str(__SENSOR_BUG);
		return (-1);
	}

	return (ret);
}

/*
 * handle_item_v4(ae, max_n_ae, list) creates net_device objects based on the
 * IPV4 address entries in the buffer pointed to by ae, and inserts them into
 * list. 0 is returned on success, -1 on failure.
 */
static int
handle_item_v4(mib2_ipAddrEntry_t *ae, int max_n_ae, s_list_t *list)
{
	mib2_ipAddrEntry_t *ptr = ae;
	mib2_ipAddrEntry_t *limit = ae + max_n_ae;

	while (ptr < limit) {
		kstat_t *ksp;
		struct net_device *netdev;
		char ifname[LIFNAMSIZ + 1];
		char logintname[LIFNAMSIZ + 1];

		(void) octetstr(&ptr->ipAdEntIfIndex, 'a', logintname,
			sizeof (logintname));
		(void) strcpy(ifname, logintname);
		(void) strtok(ifname, ":");

		DBG(DL5, ("found netif %s\n", ifname));

		if (netdev_lookup(list, ifname) != NULL) {
			/*
			 * Already in list.
			 */
			DBG(DL2, ("%s already in list\n", ifname));

			ptr++;
			continue;
		}

		/* look up kstat object */
		if ((ksp = kstat_lookup(kc, NULL, -1, ifname)) == NULL) {
			/*
			 * No kstat object for device ifname - the kstat object
			 * has been destroyed from under our feet, or there's
			 * no kstat associated to cname.
			 */

			DBG(DL2, ("kstat_lookup failed for %s (%s)\n",
				ifname, strerror(errno)));

			ptr++;
			continue;
		}

		/* get snapshot */
		if (kstat_read(kc, ksp, NULL) < 0) {

			/*
			 * kstat_read failed probably because the kernel does
			 * not have the appropriate kstat object - again, the
			 * kstat object has been destroyed from under us.
			 */

			DBG(DL1, ("kstat_read failed for %s (%s)\n",
				ifname, strerror(errno)));

			ptr++;
			continue;
		}

		if ((netdev = netdev_create(ifname)) == NULL)
			return (-1);

		netdev->ksp = ksp;
		netdev_insert(list, netdev);

		ptr++;
	}

	return (0);
}

/*
 * handle_item_v6(ae, max_n_ae, list) creates net_device objects based on the
 * IPV6 address entries in the buffer pointed to by ae, and inserts them into
 * list. 0 is returned on success, -1 on failure.
 */
static int
handle_item_v6(mib2_ipv6AddrEntry_t *ae, int max_n_ae, s_list_t *list)
{
	mib2_ipv6AddrEntry_t *ptr = ae;
	mib2_ipv6AddrEntry_t *limit = ae + max_n_ae;

	while (ptr < limit) {
		kstat_t *ksp;
		struct net_device *netdev;
		char ifname[LIFNAMSIZ + 1];
		char logintname[LIFNAMSIZ + 1];

		(void) octetstr(&ptr->ipv6AddrIfIndex, 'a', logintname,
			sizeof (logintname));
		(void) strcpy(ifname, logintname);
		(void) strtok(ifname, ":");

		DBG(DL5, ("found netif %s\n", ifname));

		if (netdev_lookup(list, ifname) != NULL) {
			/*
			 * Already in list.
			 */
			DBG(DL2, ("%s already in list\n", ifname));

			ptr++;
			continue;
		}

		/* look up kstat object */
		if ((ksp = kstat_lookup(kc, NULL, -1, ifname)) == NULL) {
			/*
			 * No kstat object for device ifname - the kstat object
			 * has been destroyed from under our feet, or there's
			 * no kstat associated to cname.
			 */

			DBG(DL2, ("kstat_lookup failed for %s (%s)\n",
				ifname, strerror(errno)));

			ptr++;
			continue;
		}

		/* get snapshot */
		if (kstat_read(kc, ksp, NULL) < 0) {

			/*
			 * kstat_read failed probably because the kernel does
			 * not have the appropriate kstat object - again, the
			 * kstat object has been destroyed from under us.
			 */

			DBG(DL1, ("kstat_read failed for %s (%s)\n",
				ifname, strerror(errno)));

			ptr++;
			continue;
		}

		if ((netdev = netdev_create(ifname)) == NULL)
			return (-1);

		netdev->ksp = ksp;
		netdev_insert(list, netdev);

		ptr++;
	}

	return (0);
}

/*
 * This function is based on Open Solaris netstat.c.
 */
static char *
octetstr(const Octet_t *op, int code, char *dst, uint_t dstlen)
{
	int i;
	char *cp;
	unsigned long _cp;
	unsigned long _dst = (unsigned long)dst;
	unsigned long _dstlen = (unsigned long)dstlen;

	cp = dst;
	if (op != NULL) {
		for (i = 0; i < op->o_length; i++) {
			_cp = (unsigned long)cp;
			switch (code) {
			case 'd':
				if ((_cp - _dst) + 4UL > _dstlen) {
					*cp = '\0';
					return (dst);
				}
				(void) snprintf(cp, 5, "%d.",
				    0xff & op->o_bytes[i]);
				cp = strchr(cp, '\0');
				break;
			case 'a':
				if ((_cp - _dst) + 1UL > _dstlen) {
					*cp = '\0';
					return (dst);
				}
				*cp++ = op->o_bytes[i];
				break;
			case 'h':
			default:
				if ((_cp - _dst) + 3UL > _dstlen) {
					*cp = '\0';
					return (dst);
				}
				(void) snprintf(cp, 4, "%02x:",
				    0xff & op->o_bytes[i]);
				cp += 3;
				break;
			}
		}
	}
	if (code != 'a' && cp != dst)
		cp--;
	*cp = '\0';
	return (dst);
}

/*
 * get_kstat_val(ksp, kdn, val) retrieves the network statistics associated to
 * kdn in the kstat pointed to by ksp, and places in the memory location pointed
 * to by val.
 */
static int
get_kstat_val(kstat_t *ksp, char *kdn, uint64_t *val)
{
	kstat_named_t *knp;

	if ((knp = (kstat_named_t *)kstat_data_lookup(ksp, kdn))
		== NULL) {

		/* no data available */
		DBG(DL2, ("no statistics named %s for that kstat\n", kdn));
		return (1);
	}

	if (knp->data_type != KSTAT_DATA_UINT64) {
		set_error_str(__SENSOR_BUG);
		return (-1);
	}

	*val = knp->value.ui64;

	return (0);
}

/*
 * comp_ki_val(mobj, kstat_val, val) computes the KI value associated to the
 * Managed Object mobj and the kstat data named kdn. The KI value is derived
 * from the kstat value kstat_val, and is returned in the uint64_t pointed to by
 * val. -1 is returned on failure. Otherwise either 0 is returned if the data is
 * available or 1 if it's not available.
 */
static int
comp_ki_val(struct net_device *mobj, char *kdn, uint64_t kstat_val,
	uint64_t *val)
{
	int err;
	boolean_t inserted = B_FALSE;
	int64_t prev_snaptime = (int64_t)0;
	uint64_t prev_kstat_val = (uint64_t)0;
	uint64_t new[2];
	int64_t snaptime;
	struct net_device *netdev;

	if ((netdev = netdev_lookup(&netdev_list, mobj->name)) == NULL) {

		DBG(DL5, ("%s not in netdev_list\n", mobj->name));

		if ((netdev = netdev_create(mobj->name)) == NULL)
			return (-1);

		netdev_insert(&netdev_list, netdev);
		inserted = B_TRUE;

	} else {
		uint_t n;
		uint64_t *old;

		DBG(DL5, ("read previous value for %s\n", mobj));

		if ((err = nvlist_lookup_uint64_array(netdev->nvlp, kdn, &old,
			&n)) == 0) {

			assert(n == (uint_t)2);

			prev_snaptime = (int64_t)old[0];
			prev_kstat_val = old[1];
		} else {
			if (err != ENOENT) {
				set_error_str(__SENSOR_BUG);
				return (-1);
			}

			DBG(DL5, ("%s prev value not available for %s\n",
				kdn, netdev->name));
		}
	}

	snaptime = mobj->ksp->ks_snaptime;
	new[0] = (uint64_t)snaptime;
	new[1] = kstat_val;

	if ((err = nvlist_add_uint64_array(netdev->nvlp, kdn, new, (uint_t)2))
		!= 0) {

		switch (err) {
		case ENOMEM:
			set_error_str("nvlist_add_uint64_array failed (%s)",
				strerror(errno));
			break;
		default:
			set_error_str(__SENSOR_BUG);
		}

		if (inserted == B_TRUE)
			netdev_remove(&netdev_list, netdev);

		return (-1);
	}

	if ((prev_snaptime == (int64_t)0) ||
		((snaptime - prev_snaptime) <= (int64_t)0)) {
		/* no data available */
		return (1);
	}

	*val = (uint64_t)(((kstat_val - prev_kstat_val) * 1e9) /
		(snaptime - prev_snaptime));

	return (0);
}


/*
 * --- Helper functions ---
 */

/*
 * cpy_error_str(in) copies the content of the string error in the string in,
 * and appends a \n to it.
 */
static void
cpy_error_str(char in[])
{
	(void) strncpy(in, get_error_str(), ERROR_STRING_MAX);
	(void) strncat(in, "\n", ERROR_STRING_MAX);
}

/*
 * insert_result(results, n_results, time, ki, mo, v, ifspeed) inserts the new
 * result represented by the fields time, ki, mo, and v into the results tabled.
 * This function is responsible for allocating memory for the results table.
 */
static int
insert_result(sensor_result_t **results, int *n_results, timespec_t t,
	const char ki[], const char mo[], uint64_t v, uint64_t ubound)
{
#define	BATCHCOUNT 16
	int n_res, idx;
	sensor_result_t *ptr, *res;
	static int n_allocated_res = 0;

	assert(n_allocated_res % BATCHCOUNT == 0);

	if (*results != NULL && n_allocated_res == 0) {
		set_error_str(__SENSOR_BUG);
		return (-1);
	}

	res = *results;
	n_res = *n_results;

	/*
	 * Allocate memory if needed.
	 */
	if (++n_res > n_allocated_res) {
		sensor_result_t *tmp;
		size_t len = (size_t)(n_allocated_res + BATCHCOUNT)
			* sizeof (*tmp);

		if ((tmp = (sensor_result_t *)realloc(res, len)) == NULL) {
			set_error_str("realloc failed (%s)", strerror(errno));
			return (-1);
		}
		res = tmp;
		n_allocated_res += BATCHCOUNT;
	}

	/*
	 * Copy result into results tab.
	 */
	idx = n_res - 1;
	ptr = &res[idx];

	(void) memcpy(&ptr->time_current, &t, sizeof (ptr->time_current));
	(void) strncpy(ptr->ki_type, ki, SENSOR_KI_TYPE_MAX);
	(void) strncpy(ptr->managed_object, mo, SENSOR_MNG_OBJ_MAX);
	ptr->usage = v;
	ptr->upperbound = ubound;

	/*
	 * Update caller's pointers.
	 */
	*results = res;
	*n_results = n_res;

	return (0);
#undef	BATCHCOUNT
}


#if STANDALONE
static void
results_dump(sensor_result_t *res, int n_res)
{
	int i;

	for (i = 0; i < n_res; i++) {
		DBG(DL1, ("%ld:%ld:%s:%s:%llu:%llu\n",
			res[i].time_current.tv_sec,
			res[i].time_current.tv_nsec,
			res[i].ki_type,
			res[i].managed_object,
			res[i].usage,
			res[i].upperbound));
	}
}

int
main(int argc, char *argv[])
{
	int i;
	int retval;
	int n_mobjs;
	char **mobjs;
	int n_results;
	int n_samplings;
	struct key_indicator *ki;
	char error[ERROR_STRING_MAX];
	sensor_result_t *results = NULL;

	if (argc < 2) {
		(void) fprintf(stderr, "usage: %s <num samplings> "
			"[<dev1> <dev2> ...]\n", argv[0]);
		return (1);
	}

	retval = 0;
	n_samplings = atoi(argv[1]);

	if ((n_mobjs = argc - 2) > 0) {

		if ((mobjs = (char **)malloc(n_mobjs * sizeof (char *)))
			== NULL) {

			(void) fprintf(stderr, "malloc failed (%s)",
				strerror(errno));
			n_mobjs = 0;
			retval = 1;
			goto out;
		}

		for (i = 0; i < n_mobjs; i++) {
			if ((mobjs[i] = (char *)malloc(SENSOR_MNG_OBJ_MAX))
				== NULL) {

				(void) fprintf(stderr, "malloc failed (%s)",
					strerror(errno));
				n_mobjs = i;
				retval = 1;
				goto out;
			}
		}

	} else {
		mobjs = NULL;
		n_mobjs = 0; /* I'm paranoid */
	}

	for (i = 0; i < n_mobjs; i++)
		(void) strncpy(mobjs[i], argv[i + 2], SENSOR_MNG_OBJ_MAX);

	/* start */
	ki_for_each(ki) {

		if (start(ki->name, error) != 0) {
			(void) fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}
	}

	/* get results */
	n_results = 0;

	for (i = 0; i < n_samplings; i++) {

		if (get_values(mobjs, n_mobjs, &results, &n_results, error)
			!= 0) {

			(void) fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}

		results_dump(results, n_results);

		(void) sleep(1);
	}

	/* stop */
	ki_for_each(ki) {

		if (stop(ki->name, error) != 0) {
			(void) fprintf(stderr, "%s\n", error);
			retval = 1;
			goto out;
		}
	}

out:
	for (i = 0; i < n_mobjs; i++)
		free(mobjs[i]);

	free(mobjs);
	free(results);
	return (retval);
}
#endif
