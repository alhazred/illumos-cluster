/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Node mbean interceptor
 */

#pragma ident	"@(#)node_data.c	1.8	08/05/20 SMI"

#include "com/sun/cluster/agent/node/NodeGroup.h"
#include <jniutil.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <sys/utsname.h>
#include <sys/systeminfo.h>
#include <sys/processor.h>
#include <sys/sol_version.h>
#include <procfs.h>
#include <utmpx.h>
#include <dirent.h>
#include <pwd.h>
#include <wchar.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <thread.h>
#include <synch.h>


#include <scha.h>
#include <cl_query/cl_query_types.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#define	debug if (0)

static jclass nodeDataClass, objArrayClass;
static mutex_t init_lock;
static int initialized = 0;

static jfieldID nameID;
static jfieldID isOnlineID;
static jfieldID isEnabledID;
static jfieldID isInTransitionID;
static jfieldID nodeIdID;
static jfieldID isNodeRebootEnabledID;
static jfieldID privateHostnameID;
static jfieldID nodeTypeID;
static jfieldID quorumCurrentVotesID;
static jfieldID quorumPossibleVotesID;
static jfieldID reservationKeyID;
static jfieldID isZonesSupportedID;
static jfieldID privateAdaptersID;
static jfieldID publicInetAddressID;

static jmethodID nodeClassConstructor = NULL;


static void init(JNIEnv *env) {
	debug printf("\n BEGIN init \n");
	(void) mutex_lock(&init_lock);

    if (!initialized) {
	/* find classes */
	jclass localClass = (*env)->FindClass(env,
	    "com/sun/cluster/agent/node/NodeData");
	nodeDataClass = (*env)->NewGlobalRef(env, localClass);

	if (nodeDataClass == NULL) {
	    debug printf("\n NodeData class is null\n");
	    debug printf("\n END init \n");
	    goto unlock;
	}
	nodeClassConstructor = (*env)->GetMethodID(env, nodeDataClass,
	    "<init>", "()V");

	if (nodeClassConstructor == NULL) {
	    debug printf("\n NodeData Constructor is null\n");
	    debug printf("\n END init \n");
	    goto unlock;
	}

	/* String */
	nameID = (*env)->GetFieldID(env, nodeDataClass, "name",
	    "Ljava/lang/String;");
	if (nameID == NULL) {
	    debug printf("\n nameID is null \n");
	    goto unlock;
	}
	publicInetAddressID = (*env)->GetFieldID(env, nodeDataClass,
	    "publicInetAddress", "Ljava/lang/String;");
	if (publicInetAddressID == NULL) {
	    debug printf("\n publicInetAddressID is null \n");
	    goto unlock;
	}
	reservationKeyID = (*env)->GetFieldID(env, nodeDataClass,
	    "reservationKey", "Ljava/lang/String;");
	if (reservationKeyID == NULL) {
	    debug printf("\n reservationKeyID is null \n");
	    goto unlock;
	}
	privateAdaptersID = (*env)->GetFieldID(env, nodeDataClass,
	    "privateAdapters", "Ljava/lang/String;");
	if (privateAdaptersID == NULL) {
	    debug printf("\n privateAdaptersID is null \n");
	    goto unlock;
	}
	nodeTypeID = (*env)->GetFieldID(env, nodeDataClass, "nodeType",
	    "Ljava/lang/String;");
	if (nodeTypeID == NULL) {
	    debug printf("\n nodeTypeID is null \n");
	    goto unlock;
	}
	privateHostnameID = (*env)->GetFieldID(env, nodeDataClass,
	    "privateHostname", "Ljava/lang/String;");
	if (privateHostnameID == NULL) {
	    debug printf("\n privateHostnameID is null \n");
	    goto unlock;
	}

	/* int */
	nodeIdID = (*env)->GetFieldID(env, nodeDataClass, "nodeId", "I");
	if (nodeIdID == NULL) {
	    debug printf("\n nodeIdID is null \n");
	    goto unlock;
	}
	quorumCurrentVotesID = (*env)->GetFieldID(env, nodeDataClass,
	    "quorumCurrentVotes", "I");
	if (quorumCurrentVotesID == NULL) {
	    debug printf("\n quorumCurrentVotesID is null \n");
	    goto unlock;
	}
	quorumPossibleVotesID = (*env)->GetFieldID(env, nodeDataClass,
	    "quorumPossibleVotes", "I");
	if (quorumPossibleVotesID == NULL) {
	    debug printf("\n quorumPossibleVotesID is null \n");
	    goto unlock;
	}

	/* boolean */
	isOnlineID =  (*env)->GetFieldID(env, nodeDataClass, "isOnline", "Z");
	if (isOnlineID == NULL) {
	    debug printf("\n isOnlineID is null \n");
	    goto unlock;
	}
	isEnabledID = (*env)->GetFieldID(env, nodeDataClass, "isEnabled", "Z");
	if (isEnabledID == NULL) {
	    debug printf("\n isEnabledID is null \n");
	    goto unlock;
	}
	isInTransitionID = (*env)->GetFieldID(env, nodeDataClass,
	    "isInTransition", "Z");
	if (isInTransitionID == NULL) {
	    debug printf("\n isInTransitionID is null \n");
	    goto unlock;
	}
	isNodeRebootEnabledID = (*env)->GetFieldID(env, nodeDataClass,
	    "isNodeRebootEnabled", "Z");
	if (isNodeRebootEnabledID == NULL) {
	    debug printf("\n isNodeRebootEnabledID is null \n");
	    goto unlock;
	}
	isZonesSupportedID = (*env)->GetFieldID(env, nodeDataClass,
	    "isZonesSupported", "Z");
	if (isZonesSupportedID == NULL) {
	    debug printf("\n isZonesSupportedID is null \n");
	    goto unlock;
	}

	initialized = 1;
	}

unlock:
	(void) mutex_unlock(&init_lock);
	debug printf("\n END init \n");
}

JNIEXPORT jobjectArray JNICALL
Java_com_sun_cluster_agent_node_NodeGroup_readDataJNI
(JNIEnv *env, jobject this) {

	jobject nodeData = NULL;
	jsize nNodes = 0;

	/*
	 * CLUSTER handle.
	 */
	scha_cluster_t sc_handle = NULL;

	/*
	 * cl query node data pointer
	 */
	cl_query_node_info_t *nodeinfo = NULL;

	scha_err_t scha_error;
	cl_query_result_t cl_query_result;
	cl_query_error_t cl_query_error = -1;

	/*
	 * Some attributes are only valid on the
	 * local node so check the local node id and
	 * ignore attributes if we aren't asking about
	 * this local node
	 */
	char *local_nodename;

	jobjectArray nodes;
	int i;
	jobject result = NULL;

	debug printf(" START readData method \n");
	if (!initialized) {
	    init(env);
	}

	scha_error = scha_cluster_open(&sc_handle);

	if (scha_error != SCHA_ERR_NOERR) {
		sc_handle = NULL;
		goto error;
	}

	debug printf(" after scha_cluster_open\n");

	scha_error = scha_cluster_get(sc_handle,
	    SCHA_NODENAME_LOCAL,
	    &local_nodename);
	if (scha_error != SCHA_ERR_NOERR) {
		goto error;
	}
	debug printf(" after scha_cluster_get\n");
	cl_query_error =
	    cl_query_get_info(CL_QUERY_NODE_INFO,
	    NULL,
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		goto error;
	}
	debug printf(" after scha_cluster_get_info %d\n",
		cl_query_result.n_elements);
	debug printf(" nodeDataClass %X\n", nodeDataClass);
	debug printf(" env %X\n", env);

	nodes = (*env)->NewObjectArray(env,
	    cl_query_result.n_elements,
	    nodeDataClass,
	    NULL);
	debug printf(" after new array %X\n", cl_query_result.value_list);
	if (nodes == NULL) {
	    debug printf("\n nodeData Array Object is NULL\n");
	    goto error;
	}

	for (nodeinfo = (cl_query_node_info_t *)cl_query_result.value_list;
	    nodeinfo != NULL;
	    nodeinfo = nodeinfo->next) {

	debug printf(" inside looP %s\n", (char *)nodeinfo->node_name);

		nodeData = (*env)->NewObject(env, nodeDataClass,
			nodeClassConstructor);
		if (nodeData == NULL) {
		    debug printf("\n nodeData Object is NULL\n");
		    goto error;
		}


		setStringField(env, nodeData, nameID, nodeinfo->node_name);
		(*env)->SetBooleanField(env, nodeData, isOnlineID,
			nodeinfo->status == CL_QUERY_ONLINE);
		(*env)->SetBooleanField(env, nodeData, isEnabledID,
			nodeinfo->state == CL_QUERY_ENABLED);
		(*env)->SetBooleanField(env, nodeData, isInTransitionID,
			nodeinfo->status == CL_QUERY_TRANSITION);
		(*env)->SetIntField(env, nodeData, nodeIdID,
			(int)nodeinfo->node_id);

		(*env)->SetBooleanField(env, nodeData, isNodeRebootEnabledID,
			nodeinfo->node_reboot_flag_status ==
			CL_QUERY_REBOOT_FLAG_ENABLE);
		setStringField(env, nodeData, privateHostnameID,
			nodeinfo->private_host_name);
		setStringField(env, nodeData, nodeTypeID,
			nodeinfo->node_category);
		(*env)->SetIntField(env, nodeData, quorumCurrentVotesID,
		    (int)nodeinfo->cur_quorum_votes);
		(*env)->SetIntField(env, nodeData, quorumPossibleVotesID,
		    (int)nodeinfo->possible_quorum_votes);

		setStringField(env, nodeData, reservationKeyID,
		    nodeinfo->resv_key);

		(*env)->SetBooleanField(env, nodeData, isZonesSupportedID, 0);
#if SOL_VERSION >= __s10
		(*env)->SetBooleanField(env, nodeData, isZonesSupportedID, 1);
#endif


		if (nodeinfo->private_adapters.list_len > 0) {
		    setStringField(env, nodeData, privateAdaptersID,
			nodeinfo->private_adapters.list_val[0]);
		}

		{
		    int errno;
		    struct hostent ent;
		    struct hostent *entp;
		    struct in_addr in;
		    char *entdata = malloc(4096);
		    if (entdata == NULL) {
			    goto endpublicaddr;
		    }
		    entp = gethostbyname_r(nodeinfo->node_name,
			&ent,
			entdata,
			4096,
			&errno); /*lint !e746 */
		    if (entp == NULL) {
			goto endpublicaddr;
		    }
		    (void) memcpy(&in.s_addr,
			*(entp->h_addr_list),
			sizeof (in.s_addr));

		    setStringField(env, nodeData, publicInetAddressID,
			(char *)inet_ntoa(in));
		endpublicaddr:
		    if (entdata != NULL) {
			    free(entdata);
		    }

		}

		(*env)->SetObjectArrayElement(env, nodes, nNodes++, nodeData);
	    debug printf(" end looP\n");
	}


    error:
	/*
	 * Free the specific resources allocated in this function
	 */

	if (sc_handle != NULL) {
		/*
		 * free memory pointed to by cluster_names
		 */
		(void) scha_cluster_close(sc_handle);
	}
	/*
	 * free up cl_query struct
	 */
	if (cl_query_error == CL_QUERY_OK) {
		(void) cl_query_free_result(CL_QUERY_NODE_INFO,
		    &cl_query_result);
	}
	return (nodes);

} /*lint !e715 */
