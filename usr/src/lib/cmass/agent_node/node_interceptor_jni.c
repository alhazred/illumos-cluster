/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*	Copyright (c) 1984, 1986, 1987, 1988, 1989 AT&T */
/*	  All Rights Reserved   */

/*
 * University Copyright- Copyright (c) 1982, 1986, 1988
 * The Regents of the University of California
 * All Rights Reserved
 *
 * University Acknowledgment- Portions of this document are derived from
 * software developed by the University of California, Berkeley, and its
 * contributors.
 */


/*
 * This file contains native library functions used for
 * the Node mbean interceptor
 */

#pragma ident	"@(#)node_interceptor_jni.c	1.6	08/05/20 SMI"

#include "com/sun/cluster/agent/node/NodeInterceptor.h"

#include <jniutil.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <sys/utsname.h>
#include <sys/systeminfo.h>
#include <sys/processor.h>
#include <sys/sol_version.h>
#include <procfs.h>
#include <utmpx.h>
#include <dirent.h>
#include <pwd.h>
#include <wchar.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <scha.h>
#include <cl_query/cl_query_types.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

/*
 * A couple of helper macros for things we do a lot,
 * they contain references to local variables in the
 * function below
 */
#define	PUT_ATTR(MAP, NAME, VALUE)		\
	PUT(MAP, NAME, (*env)->NewObject(env,	\
	    attribute_class,			\
	    attribute_constructor,		\
	    (*env)->NewStringUTF(env, NAME),	\
	    VALUE))

#define	PUT(MAP, NAME, VALUE)						\
	(*env)->CallObjectMethod(env,					\
				    MAP,				\
				    map_put,				\
				    (*env)->NewStringUTF(env, NAME),	\
				    VALUE)

static int64_t boot_time = -1;
static int64_t uptime = -1;

/*
 * Class:     com_sun_cluster_agent_example_nodeInterceptor
 * Method:    fillCache
 * Signature: ()Ljava/util/Map;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_node_NodeInterceptor_fillCache
	(JNIEnv * env, jobject this) {

	/*
	 * return a Map of Maps containing all instances
	 * key = instanceName
	 * value = Map
	 *		key=attributeName
	 *		value=corresponding Attribute object
	 */

	jobject cache_map = NULL;
	jobject instance_map = NULL;
	jclass map_class = NULL;
	jmethodID map_put;

	jclass attribute_class = NULL;
	jmethodID attribute_constructor = NULL;

	/*
	 * CLUSTER handle.
	 */
	scha_cluster_t sc_handle = NULL;

	/*
	 * cl query node data pointer
	 */
	cl_query_node_info_t *nodeinfo = NULL;

	scha_err_t scha_error;
	cl_query_result_t cl_query_result;
	cl_query_error_t cl_query_error = -1;

	/*
	 * Some attributes are only valid on the
	 * local node so check the local node id and
	 * ignore attributes if we aren't asking about
	 * this local node
	 */
	char *local_nodename;

	/*
	 * get our references to 'attribute_class'
	 */

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * get constructor
	 */
	attribute_constructor =
	    (*env)->GetMethodID(env,
	    attribute_class,
	    "<init>",
	    "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;

	/*
	 * Create the map we are going to return
	 */
	cache_map = newObjNullParam(env, "java/util/HashMap");
	if (cache_map == NULL)
		goto error;

	/*
	 * get our references to map_class and the put method
	 */
	map_class = (*env)->FindClass(env, "java/util/Map");
	if (map_class == NULL)
		goto error;
	map_put =
	    (*env)->GetMethodID(env,
	    map_class,
	    "put",
	    "(Ljava/lang/Object;Ljava/lang/Object;)"
	    "Ljava/lang/Object;");
	if (map_put == NULL)
		goto error;

	/*
	 * Get node info
	 */
	scha_error = scha_cluster_open(&sc_handle);

	if (scha_error != SCHA_ERR_NOERR) {
		sc_handle = NULL;
		goto error;
	}
	scha_error = scha_cluster_get(sc_handle,
	    SCHA_NODENAME_LOCAL,
	    &local_nodename);
	if (scha_error != SCHA_ERR_NOERR) {
		goto error;
	}
	cl_query_error =
	    cl_query_get_info(CL_QUERY_NODE_INFO,
	    NULL,
	    &cl_query_result);
	if (cl_query_error != CL_QUERY_OK) {
		goto error;
	}

	for (nodeinfo = (cl_query_node_info_t *)cl_query_result.value_list;
	    nodeinfo != NULL;
	    nodeinfo = nodeinfo->next) {

		/*
		 * we are going to create a map for this instance and
		 * populate it with its attributes
		 */

		instance_map = newObjNullParam(env, "java/util/HashMap");
		if (instance_map == NULL)
			goto error;

		PUT_ATTR(instance_map, "Name",
		    (*env)->NewStringUTF(env, (char *)
			nodeinfo->node_name));

		{
			int errno;
			struct hostent ent;
			struct hostent *entp;
			struct in_addr in;
			char *entdata = malloc(4096);
			if (entdata == NULL) {
				goto endpublicaddr;
			}
			entp = gethostbyname_r(nodeinfo->node_name,
			    &ent,
			    entdata,
			    4096,
			    &errno); /*lint !e746 */
			if (entp == NULL) {
				goto endpublicaddr;
			}
			(void) memcpy(&in.s_addr,
			    *(entp->h_addr_list),
			    sizeof (in.s_addr));

			PUT_ATTR(instance_map, "PublicInetAddress",
			    (*env)->NewStringUTF(env, (char *)
				inet_ntoa(in)));
		    endpublicaddr:
			if (entdata != NULL) {
				free(entdata);
			}
		}

		PUT_ATTR(instance_map, "NodeType",
		    (*env)->NewStringUTF(env, (char *)
		    nodeinfo->node_category));

		PUT_ATTR(instance_map, "Online",
		    newBoolean(env,
			nodeinfo->status == CL_QUERY_ONLINE));

		PUT_ATTR(instance_map, "Enabled",
		    newBoolean(env,
			nodeinfo->state == CL_QUERY_ENABLED));

		PUT_ATTR(instance_map, "NodeRebootEnabled",
		    newBoolean(env,
			nodeinfo->node_reboot_flag_status ==
			CL_QUERY_REBOOT_FLAG_ENABLE));

		if (nodeinfo->private_adapters.list_len > 0) {
			PUT_ATTR(instance_map, "PrivateAdapters",
			    (*env)->NewStringUTF(env, (char *)
			    nodeinfo->private_adapters.list_val[0]));
		}

		PUT_ATTR(instance_map, "InTransition",
		    newBoolean(env,
			nodeinfo->status ==
			CL_QUERY_TRANSITION));

		PUT_ATTR(instance_map, "NodeId",
		    newObjInt32Param(env,
			"java/lang/Integer",
			(int)nodeinfo->node_id));

		PUT_ATTR(instance_map, "PrivateHostname",
		    (*env)->NewStringUTF(env, (char *)
			nodeinfo->private_host_name));

		PUT_ATTR(instance_map, "QuorumCurrentVotes",
		    newObjInt32Param(env, "java/lang/Integer",
			(int)nodeinfo->cur_quorum_votes));

		PUT_ATTR(instance_map, "QuorumPossibleVotes",
		    newObjInt32Param(env, "java/lang/Integer",
			(int)nodeinfo->possible_quorum_votes));

		PUT_ATTR(instance_map, "ReservationKey",
		    (*env)->NewStringUTF(env, (char *)
			nodeinfo->resv_key));

		/*
		 * If we are not asking about local instance,
		 * skip other attributes
		 */
		if (strcmp(local_nodename, nodeinfo->node_name) == 0) {
			struct utsname uname_info;
			char archbuf[SYS_NMLN];
			char platformbuf[SYS_NMLN];
			char *str = malloc((SYS_NMLN + 1) * 3);

			if (str == NULL)
				goto error;

			if (uname(&uname_info) == -1) {
				uname_info.machine[0] = '\0';
			}
			if (sysinfo(SI_ARCHITECTURE, archbuf,
				sizeof (archbuf)) == -1) {
				archbuf[0] = '\0';
			}
			if (sysinfo(SI_PLATFORM, platformbuf,
				sizeof (platformbuf)) == -1) {
				platformbuf[0] = '\0';
			}
			(void) sprintf(str,
			    "%s %s %s",
			    uname_info.machine,
			    archbuf,
			    platformbuf);

			PUT_ATTR(instance_map, "MachineType",
			    (*env)->NewStringUTF(env, (char *)
				str));

			(void) sprintf(str,
			    "%s %s %s",
			    uname_info.sysname,
			    uname_info.release,
			    uname_info.version);

			PUT_ATTR(instance_map, "OperatingSystemInfo",
			    (*env)->NewStringUTF(env, (char *)
				str));

			free(str);

			PUT_ATTR(instance_map, "MemorySize",
			    newObjInt64Param(env,
				"java/lang/Long",
				(int64_t)((int64_t)sysconf(_SC_PAGESIZE) *
				    (int64_t)sysconf(_SC_PHYS_PAGES))));

			PUT_ATTR(instance_map, "CpuCount",
			    newObjInt32Param(env, "java/lang/Integer",
				(int32_t)sysconf(_SC_NPROCESSORS_ONLN)));

			if (boot_time == -1) {

				/*
				 * Logic adapted from
				 * /ws/on81-patch-clone/usr/src/cmd/w/w.c
				 */
				struct utmpx *utp;

				(void) utmpxname(UTMPX_FILE);

				setutxent();
				while ((utp = getutxent()) != NULL) {
					if (utp->ut_type == BOOT_TIME) {
						boot_time = utp->ut_xtime;
						break;
					}
				}
				endutxent();
			}
			uptime = time(NULL) - boot_time + 30;
			if (uptime > 0) {
				PUT_ATTR(instance_map, "Uptime",
				    newObjInt64Param(env,
					"java/lang/Long", uptime));
			}
		}
		PUT_ATTR(instance_map, "ZonesSupported",
			newObjStringParam(env,
			"java/lang/Boolean", "false"));
#if SOL_VERSION >= __s10
		PUT_ATTR(instance_map, "ZonesSupported",
			newObjStringParam(env,
			"java/lang/Boolean", "true"));
#endif
		/*
		 * Now put this map into the returned map under this
		 * instance
		 */

		PUT(cache_map,
		    nodeinfo->node_name,
		    instance_map);
	}


    error:
	/*
	 * Free the specific resources allocated in this function
	 */

	if (sc_handle != NULL) {
		/*
		 * free memory pointed to by cluster_names
		 */
		(void) scha_cluster_close(sc_handle);
	}
	/*
	 * free up cl_query struct
	 */
	if (cl_query_error == CL_QUERY_OK) {
		(void) cl_query_free_result(CL_QUERY_NODE_INFO,
		    &cl_query_result);
	}
	return (cache_map);

} /*lint !e715 */

/*
 * max chars in a user/group name or printed u/g id
 */
#define	MAXUGNAME 10

/*
 * Class:     com_sun_cluster_agent_node_NodeInterceptor
 * Method:    obtainProcesses
 * Signature: ()[Lcom/sun/cluster/agent/node/NodeProcess;
 */
JNIEXPORT jobjectArray JNICALL
Java_com_sun_cluster_agent_node_NodeInterceptor_obtainProcesses
	(JNIEnv * env, jobject this, jstring instance_name_string) {

	const char *instance_name = NULL;
	jobjectArray process_array = NULL;
	jobjectArray return_array = NULL;
	jclass process_class;
	jclass process;
	jmethodID process_constructor;

	jclass array_list_class;
	jmethodID array_list_constructor;
	jmethodID array_list_add;
	jmethodID array_list_to_array;
	jobject process_list;

	/*
	 * Code inspired from source of
	 * /ws/on81-patch-clone/usr/src/cmd/ps/ps.c
	 */

	static char *procdir = "/proc";
	struct dirent *dentp;
	DIR *dirp;
	char pname[100];
	unsigned int pdlen;

	int bytesleft, length;
	wchar_t wchar;

	/*
	 * Get a C version of our instance name
	 */
	instance_name = (*env)->GetStringUTFChars(env, instance_name_string,
	    NULL);

	process_class =
	    (*env)->FindClass(env,
	    "com/sun/cluster/agent/node/NodeProcess");
	if (process_class == NULL)
		return (NULL);

	/*
	 * pick up a methodID to the constructor
	 */
	process_constructor =
	    (*env)->GetMethodID(env,
	    process_class,
	    "<init>",
	    "(Ljava/lang/String;JJJJFJJLjava/lang/String;)V");

	array_list_class =
	    (*env)->FindClass(env,
	    "java/util/ArrayList");
	if (array_list_class == NULL)
		return (NULL);

	/*
	 * pick up a methodID to the constructor
	 */
	array_list_constructor =
	    (*env)->GetMethodID(env,
	    array_list_class,
	    "<init>",
	    "(I)V");

	/*
	 * pick up a methodID to the 'add' method
	 */
	array_list_add =
	    (*env)->GetMethodID(env,
	    array_list_class,
	    "add",
	    "(Ljava/lang/Object;)Z");

	/*
	 * pick up a methodID to the toArray method
	 */
	array_list_to_array =
	    (*env)->GetMethodID(env,
	    array_list_class,
	    "toArray",
	    "([Ljava/lang/Object;)[Ljava/lang/Object;");

	/*
	 * Create the empty ArrayList, quite big, to try and avoid a copy
	 */
	process_list = (*env)->NewObject(env,
	    array_list_class,
	    array_list_constructor,
	    300);
	if (process_list == NULL)
		goto error;


	/*
	 * Code inspired from source of
	 * /ws/on81-patch-clone/usr/src/cmd/ps/ps.c
	 */

	if ((dirp = opendir(procdir)) == NULL) {
		goto error;
	}
	(void) strcpy(pname, procdir);
	pdlen = strlen(pname);
	pname[pdlen++] = '/';

	/*
	 * for each active process ---
	 */
	while ((dentp = readdir(dirp)) != NULL) {
		int procfd;
		psinfo_t info;
		char *cp;

#if SOL_VERSION >= __s10
		char zonename[ZONENAME_MAX];
#endif

		char username[MAXUGNAME + 1];
		struct passwd *pwd;

		if (dentp->d_name[0] == '.')
			continue;
	    retry:
		(void) strcpy(pname + pdlen, dentp->d_name);
		(void) strcpy(pname + pdlen + strlen(dentp->d_name), "/psinfo");
		if ((procfd = open(pname, O_RDONLY)) == -1)
			continue;
		/*
		 * Get the info structure for the process and close quickly.
		 */
		if (read(procfd, (char *)&info, sizeof (info)) < 0) {
			int saverr = errno; /*lint !e746 */

			(void) close(procfd);
			if (saverr == EAGAIN)
				goto retry;
			continue;
		}
		(void) close(procfd);

#if SOL_VERSION >= __s10
		if (strcmp(instance_name, "") != 0) {
			/*
			 * Zone name has been provided.
			 */
			if (getzonenamebyid(info.pr_zoneid, zonename,
			    sizeof (zonename)) < 0) {
				(void) sprintf(zonename, "%7.7d",
						(int)info.pr_zoneid);
			}
			if ((zonename != NULL) &&
			    (strcmp(zonename, instance_name) != 0)) {
				continue;
			}
		}
#endif
		if (info.pr_lwp.pr_state == 0)
			continue;

		/*
		 * get a few derived values
		 */

		pwd = getpwuid(info.pr_uid);
		if (pwd != NULL) {
			(void) strncpy(username, pwd->pw_name, MAXUGNAME);
		} else {
			(void) sprintf(username, "%5d", (int)info.pr_uid);
		}

		/*
		 * Clean up the command line to make it printable
		 */
		info.pr_psargs[PRARGSZ - 1] = '\0';
		bytesleft = PRARGSZ;
		for (cp = info.pr_psargs; *cp != '\0'; cp += length) {
			length = mbtowc(&wchar, cp, MB_LEN_MAX);
			if (length == 0)
				break;
			if (length < 0 || !iswprint(wchar)) {
				if (length < 0)
					length = 1;
				if (bytesleft <= length) {
					*cp = '\0';
					break;
				}
				/*
				 * omit the unprintable character
				 */
				(void) memmove(cp, cp + length,
				    (unsigned int)(bytesleft - length));
				length = 0;
			}
			bytesleft -= length;
		}

		/*
		 * Create the corresponding java object
		 */
		process = (*env)->NewObject(env,
		    process_class,
		    process_constructor,
		    newObjStringParam(env,
			"java/lang/String",
			username),
		    (int64_t)info.pr_pid,
		    (int64_t)info.pr_ppid,
		    (int64_t)info.pr_start.tv_sec,
		    (int64_t)info.pr_time.tv_sec,
		    (jfloat)(((float)info.pr_pctcpu) /
			0x8000 * 100),
		    (int64_t)info.pr_size,
		    (int64_t)info.pr_rssize,
		    newObjStringParam(env,
			"java/lang/String",
			info.pr_psargs));

		if (process != NULL) {
			/*
			 * add the NodeProcess object we just created to
			 * the ArrayList
			 */
			(void) (*env)->CallObjectMethod(env,
			    process_list,
			    array_list_add,
			    process);
		}
	}
	(void) closedir(dirp);

	/*
	 * convert the ArrayList to a NodeProcess array before returning
	 */

	/*
	 * create an empty array for typing info
	 */
	process_array = (*env)->NewObjectArray(env, 0, process_class, NULL);
	if (process_array == NULL) {
		goto error;
	}
	/*
	 * use toArray to map back to the underlying array
	 */

	return_array = (*env)->CallObjectMethod(env,
	    process_list,
	    array_list_to_array,
	    process_array);

	/*
	 * Now clean up
	 */
    error:
	/*
	 * Delete local refs
	 */
	delRef(env, process_class);
	delRef(env, array_list_class);
	delRef(env, process_array);

	return (return_array);
} /*lint !e715 */
