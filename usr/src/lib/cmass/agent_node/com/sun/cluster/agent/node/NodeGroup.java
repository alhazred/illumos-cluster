/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)NodeGroup.java	1.10	08/05/20 SMI"
 */
package com.sun.cluster.agent.node;

import com.sun.cluster.common.CMASMBean;

// Java
import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NodeGroup extends CMASMBean implements NodeGroupMBean {

    private static Logger logger =
        Logger.getLogger("com.sun.cluster.agent.node");
    private final static String logTag = "NodeGroup";

    /**
     * Constructor.
     */

    public NodeGroup() {
    }

    public native NodeData[] readDataJNI();

    public NodeData[] readData() {
	NodeData[] arr = null;
	arr = readDataJNI();
	return arr;
    }

    public void test(String args[]) throws Error {
	dprintln("starting NodeGroup test");

	try {
	    loadLib("libcmas_agent_node.so");
	    NodeData[] arr = readData();
	    if (arr == null) {
		println("readData returned null!");
		throw new Error("readData returned null!");
	    }
	    dprintln("readData succeeded");
	    dprintln("arr =  " + arr);

	    println("returned " + arr.length + " nodes");
	    for (int j = 0; j < arr.length; j++) {
		println("node "+j+": " + arr[j]);
	    }
	} catch (Exception e) {
	    println("readDataJNI failed!");
	    println(e.getMessage());
	    e.printStackTrace();
	    throw new Error("readDataJNI failed!");
	}

	return;
    }

}
