/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)NodeData.java	1.6	08/05/20 SMI"
 */
package com.sun.cluster.agent.node;

/**
 */
public class NodeData implements java.io.Serializable {

    public String name;

    /**
     * Get the node's default public Inet address
     */
    public String publicInetAddress;


    /**
     * Give the "online" status of the node. If this field is
     * <code>false</code>, the node might be in a transition mode; use the
     * {@link #isInTransition} field to get the information.
     */
    public boolean isOnline;

    /**
     * Give the "Enabled" status of the node.
     */
    public boolean isEnabled;


    /**
     * Enables to know if the node is in a transition mode between two states.
     */
    public boolean isInTransition;

    /**
     * Get node's ID.
     */
    public int nodeId;

    /**
     * Get Reboot once on failure of all local disks status.
     */
    public boolean isNodeRebootEnabled;
    /**
     * Get node's private host name. The name does not contain the
     * domain.
     */
    public String privateHostname;

    /**
     * Get Node Type.
     * To determine whether the node is a cluster node or a farm node
     */

    public String nodeType;

    /**
     * Get the total of votes gained from quorum.
     */
    public int quorumCurrentVotes;

    /**
     * Get the total of possible votes from quorum.
     */
    public int quorumPossibleVotes;

    /**
     * Get node's reservation number.
     */
    public String reservationKey;

    /**
     * Whether zones are supported or not.
     */
    public boolean isZonesSupported;

    /**
     * Get the set of private interconnects connected to this node
     * as String. This field applies to Farm Nodes Only
     */

    public String privateAdapters;

    public NodeData() {
    }

    public String toString() {
	StringBuffer s = new StringBuffer();
	s.append("[name="+name);
	s.append(",publicInetAddress="+publicInetAddress);
	s.append(",isOnline="+isOnline);
	s.append(",isEnabled="+isEnabled);
	s.append(",isInTransition="+isInTransition);
	s.append(",nodeId="+nodeId);
	s.append(",isNodeRebootEnabled="+isNodeRebootEnabled);
	s.append(",privateHostname="+privateHostname);
	s.append(",nodeType="+nodeType);
	s.append(",quorumCurrentVotes="+quorumCurrentVotes);
	s.append(",quorumPossibleVotes="+quorumPossibleVotes);
	s.append(",reservationKey="+reservationKey);
	s.append(",isZonesSupported="+isZonesSupported);
	s.append(",privateAdapters="+privateAdapters);
	s.append("]");
	return s.toString();
    }

}
