/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NodeProcess.java 1.6     08/05/20 SMI"
 */
package com.sun.cluster.agent.node;

import java.io.Serializable;

import java.util.Date;


/**
 * An instance of this object represent a single process running on a remote
 * Cluster Node.
 *
 * @see  NodeMBean
 */
public class NodeProcess implements Serializable {

    private String user;
    private long pid;
    private long ppid;
    private Date startTime;
    private long time;
    private String command;
    private float cpuPercent;
    private long size;
    private long residentSize;

    /**
     * Default empty constructor
     */
    public NodeProcess() {
    }

    /**
     * Full constructor used by jni
     *
     * @param  user  a <code>String</code> value
     * @param  pid  a <code>long</code> value
     * @param  ppid  a <code>long</code> value
     * @param  startTime  a <code>long</code> value in seconds, converted to a
     * Date object
     * @param  time  a <code>long</code> value
     * @param  cpuPercent  a <code>float</code> value
     * @param  size  a <code>long</code> value
     * @param  residentSize  a <code>long</code> value
     * @param  command  a <code>String</code> value
     */
    public NodeProcess(String user, long pid, long ppid, long startTime,
        long time, float cpuPercent, long size, long residentSize,
        String command) {
        this.user = user;
        this.pid = pid;
        this.ppid = ppid;
        this.startTime = new Date(startTime * 1000);
        this.time = time;
        this.cpuPercent = cpuPercent;
        this.size = size;
        this.residentSize = residentSize;
        this.command = command;
    }

    /**
     * Get the user who initiated the process.
     *
     * @return  user name.
     */
    public String getUser() {
        return this.user;
    }

    /**
     * Get the process's ID.
     *
     * @return  process ID.
     */
    public long getPID() {
        return this.pid;
    }

    /**
     * Get the process's parent's ID.
     *
     * @return  process parent ID.
     */
    public long getPPID() {
        return this.ppid;
    }

    /**
     * Get the process's start time in seconds
     *
     * @return  process's start time
     */
    public Date getStartTime() {
        return this.startTime;
    }

    /**
     * Get, the cumulative CPU time of the process in seconds
     *
     * @return  process's time.
     */
    public long getCpuTime() {
        return this.time;
    }

    /**
     * Get the percentage of recent CPU time used by the process
     *
     * @return  CPU percentage process time.
     */
    public float getCpuPercent() {
        return this.cpuPercent;
    }

    /**
     * Get the process memory size in Kbytes
     *
     * @return  process memory size in Kbytes
     */
    public long getSize() {
        return this.size;
    }

    /**
     * Get the process resident memory size in Kbytes
     *
     * @return  process resident memory size in Kbytes
     */
    public long getResidentSize() {
        return this.residentSize;
    }

    /**
     * Get the full command line executed by the process.
     *
     * @return  process's command.
     */
    public String getCommand() {
        return this.command;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the user.
     *
     * @param  user  the user name.
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the pid.
     *
     * @param  pid  the pid.
     */
    public void setPID(int pid) {
        this.pid = pid;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the ppid.
     *
     * @param  ppid  the ppid.
     */
    public void setPPID(int ppid) {
        this.ppid = ppid;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the start time.
     *
     * @param  startTime  a <code>Date</code> value
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the time.
     *
     * @param  time  the time.
     */
    public void setCpuTime(long time) {
        this.time = time;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the command.
     *
     * @param  command  the command.
     */
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the command. set the percentage of recent
     * CPU time used by the process
     *
     * @param  value  a <code>float</code> value
     */
    public void setCpuPercent(float value) {
        this.cpuPercent = value;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the command. set the process memory size
     * in Kbytes
     *
     * @param  value  a <code>long</code> value
     */
    public void setSize(long value) {
        this.size = value;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the command. set the process resident
     * memory size in Kbytes
     *
     * @param  value  a <code>long</code> value
     */
    public void setResidentSize(long value) {
        this.residentSize = value;
    }


}
