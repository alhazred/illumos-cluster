/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ZoneInterceptor.java 1.9     08/05/21 SMI"
 */
package com.sun.cluster.agent.node;

// JDK
import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerNotification;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;

// JDMK
import com.sun.jdmk.JdmkMBeanServer;
import com.sun.jdmk.ServiceName;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.CachedVirtualMBeanInterceptor;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.event.ClEventDefs;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.event.SysEventNotifierMBean;
import com.sun.cluster.common.ClusterPaths;


/*
 * A zone subclass of the CMASS intercegptor, to
 * create and manage virtual mbeans that have a
 * classic CMASS implementation - all instance and
 * attribute values are fetched through JNI, and all
 * operations are performed through an invocation of
 * a CLI.
 */
public class ZoneInterceptor extends CachedVirtualMBeanInterceptor
    implements NotificationListener {

    /* DELIMETER */
    private static String COLON = ":";

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.node");
    private final static String logTag = "ZoneInterceptor";

    /* Internal reference to the SysEvent Notifier. */
    private ObjectName sysEventNotifier;

    /* The MBeanServer in which this interceptor is inserted. */
    private MBeanServer mBeanServer;

    /*
     * Constructor.
     * @param mBeanServer The MBeanServer in which to insert this interceptor.
     * @param dispatcher DomainDispatcher that this virtual mbean interceptor
     *                   should register into for dispatching.
     * @param onf ObjectNameFactory implementation to be used by this
     *            VirtualMBeanInterceptor
     */
    public ZoneInterceptor(MBeanServer mBeanServer,
        VirtualMBeanDomainDispatcher dispatcher, ObjectNameFactory onf) {
        super(mBeanServer, dispatcher, onf, ZoneMBean.class, null);
        this.mBeanServer = mBeanServer;
        this.sysEventNotifier =
            new ObjectNameFactory("com.sun.cluster.agent.event").getObjectName(
                SysEventNotifierMBean.class, null);
    }

    /*
     * Called when the module is unlocked.
     */
    public void unlock() throws Exception {
        super.unlock();
        logger.entering(logTag, "start");

        try {

            // No need to use a proxy here, we can go directly to the
            // addNotificationListener method on the mbean server.
            mBeanServer.addNotificationListener(sysEventNotifier, this, null,
                null);
        } catch (InstanceNotFoundException e) {
            logger.warning("unable to register for zone config change events");
        }
    }

    /*
     * Called when the module is locked.
     */
    public void lock() throws Exception {
        super.lock();
        logger.entering(logTag, "stop");

        // Unregister for event listening.
        try {

            // No need to use a proxy here, we can go directly to the
            // addNotificationListener method on the mbean server.
            mBeanServer.removeNotificationListener(sysEventNotifier, this, null,
                null);
        } catch (Exception e) {
            logger.warning("Unable to unregister zone config listener");
        }

        invalidateCache();
    }


    /**
     * The interceptor should monitor create/destroy/attribute-change events and
     * emit appropriate notifications from the mbean server (for create/destroy)
     * or from the virtual mbean (attribute change).
     *
     * <p>Our interceptor monitors the zone state changes
     *
     * <p>This method implements the NotificationListener interface.
     *
     * @param  notification  a <code>Notification</code> value
     * @param  handback  an <code>Object</code> value
     */
    public void handleNotification(Notification notification, Object handback) {

        if (logger.isLoggable(Level.FINE))
            logger.fine("received cluster event " + notification);


        if (!(notification instanceof SysEventNotification))
            return;

        SysEventNotification clEvent = (SysEventNotification) notification;

        if (clEvent.getSubclass().equals(ClEventDefs.ESC_CLUSTER_ZONES_STATE)) {
            invalidateCache();
        }
    }

    /*
     * Function called by the generic part of the virtual MBean interceptor
     * (the super class) for invoking an operation of an MBean.
     * Note that this is not part of the MBeanInterceptor interface as its
     * getAttributes function has a different signature.
     * @param name The name of the MBean on which the method is to be invoked.
     * @param operationName The name of the operation to be invoked.
     * @param params An array containing the parameters to be set when the
     *        operation is invoked.
     * @param signature An array containing the signature of the operation.
     *        The class objects will be loaded using the same class loader as
     *        the one used for loading the MBean on which the operation was
     *        invoked.
     * @return The object returned by the operation, which represents the
     *         result ofinvoking the operation on the MBean specified.
     * @throws InstanceNotFoundException if an error occurs
     * @throws MBeanException An error occurs
     * @throws ReflectionException An error occurs
     * @throws IOException An error occurs
     */
    public Object invoke(String name, String operationName, Object params[],
        String signature[]) throws InstanceNotFoundException, MBeanException,
        ReflectionException, IOException {

        if (!isRegistered(name)) {
            throw new InstanceNotFoundException("cannot find the instance " +
                name);
        }

        String cmds[][] = null;

	// XXX: code below should NOT call the "zoneadm" command.
	//	instead, it should obtain the zone info from a proper
	//	cluster api (clquery or scha)
	//
        if (operationName.equals("getZoneDetails")) {

            // Check to see if zonename is passed
            String zoneName = null;

            if ((signature != null) &&
                    (signature[0].equals("java.lang.String"))) {
                zoneName = (String) params[0];
                cmds = new String[][] {
                        {
                            ClusterPaths.ZONEADM_CMD, "-z", zoneName, "list",
                            "-p"
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }
        } else if (operationName.equals("obtainZoneSchedulingClass")) {
            String zoneName = null;

            if ((signature != null) &&
                    (signature[0].equals("java.lang.String"))) {
                zoneName = (String) params[0];
                cmds = new String[][] {
                        {
                            ClusterPaths.ZLOGIN_CMD, zoneName,
                            ClusterPaths.SCSLM_GETSCHED_CMD
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }
        } else if (operationName.equals("enablePrivateIP")) {

            if ((signature != null) &&
                    (signature[0].equals("java.lang.String")) &&
                    (signature[1].equals("java.lang.String"))) {
                String nodeKeyName = (String) params[0];
                String hostName = (String) params[1];
                cmds = new String[][] {
                        {
                            ClusterPaths.CL_NODE_CMD, "set", "-p",
                            "zprivatehostname=" + hostName, nodeKeyName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }
        } else if (operationName.equals("disablePrivateIP")) {

            if ((signature != null) &&
                    (signature[0].equals("java.lang.String"))) {
                String nodeKeyName = (String) params[0];
                cmds = new String[][] {
                        {
                            ClusterPaths.CL_NODE_CMD, "set", "-p",
                            "zprivatehostname=", nodeKeyName
                        }
                    };
            } else {
                throw new IOException("Invalid signature");
            }
        } else if (operationName.equals("changePrivateIP")) {

            if ((signature != null) &&
                    (signature[0].equals("java.lang.String")) &&
                    (signature[1].equals("java.lang.String"))) {
                String nodeKeyName = (String) params[0];
                String hostName = (String) params[1];
                cmds = new String[][] {
                        {
                            ClusterPaths.CL_NODE_CMD, "set", "-p",
                            "zprivatehostname=" + hostName, nodeKeyName
                        }
                    };

            } else {
                throw new IOException("Invalid signature");
            }

        } else {
            throw new ReflectionException(new IllegalArgumentException(
                    operationName));
        }

        // Run the command
        InvocationStatus exits[] = null;

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new MBeanException(new CommandExecutionException(
                    ie.getMessage(), ExitStatus.createArray(exits)));
        }

        invalidateCache();

        return ExitStatus.createArray(exits);
    }

    /*
     * Fill the cache
     * @return a <code>Map</code>, key is instance name, value is a map
     * of attribute name to attribute value.
     */
    protected native Map fillCache();
}
