/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NodeModule.java 1.13     08/12/03 SMI"
 */
package com.sun.cluster.agent.node;

// JDK
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.ObjectName;

// Cacao
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// Local
import com.sun.cluster.common.ClusterRuntimeException;


/**
 * Node module using the interceptor and JNI design patterns
 */
public class NodeModule extends Module {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.node");

    /* Our interceptor. */
    private NodeInterceptor interceptor;

    private ZoneInterceptor zoneInterceptor;

    /* Our dispatcher. */
    private VirtualMBeanDomainDispatcher dispatcher;

    /**
     * Constructor called by the container.
     *
     * @param  descriptor
     */
    public NodeModule(DeploymentDescriptor descriptor) {
        super(descriptor);

        Map parameters = descriptor.getParameters();

        try {

            // Load the agent JNI library
            String library = (String) parameters.get("library");
            Runtime.getRuntime().load(library);

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING,
                "Unable to load native runtime library" + ule);
            throw ule;
        }
    }

    protected void start() throws ClusterRuntimeException {
        logger.fine("Create Interceptor");

        // Create the virtual MBean interceptor and start it.
        ObjectNameFactory objectNameFactory = new ObjectNameFactory(
                getDomainName());
        dispatcher = new VirtualMBeanDomainDispatcher(getMbs(),
                objectNameFactory);

        try {
            dispatcher.unlock();
            interceptor = new NodeInterceptor(getMbs(), dispatcher,
                    objectNameFactory);
            interceptor.unlock();

            zoneInterceptor = new ZoneInterceptor(getMbs(), dispatcher,
                    objectNameFactory);
            zoneInterceptor.unlock();

	    NodeGroup ng = new NodeGroup();
	    logger.fine("register NodeGroup");
	    getMbs().registerMBean(ng,
		objectNameFactory.getObjectName(NodeGroupMBean.class,
		null));

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING, "Unable to start Node module" + e);
            throw new ClusterRuntimeException(e.getMessage());
        }
    }

    protected void stop() {
        logger.fine("Stop Interceptor");

        // Stop the interceptor and dispatcher and release for GC
        try {
            dispatcher.lock();
            interceptor.lock();
            zoneInterceptor.lock();

	    ObjectNameFactory objectNameFactory =
	    new ObjectNameFactory(getDomainName());
	    getMbs().unregisterMBean(
		objectNameFactory.getObjectName(NodeGroupMBean.class,
		null));

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.log(Level.WARNING, "Unable to stop Node module" + e);
            throw new ClusterRuntimeException(e.getMessage());
        }

        dispatcher = null;
        interceptor = null;
        zoneInterceptor = null;
    }
}
