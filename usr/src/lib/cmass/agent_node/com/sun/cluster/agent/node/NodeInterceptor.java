/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NodeInterceptor.java 1.13     09/03/27 SMI"
 */
package com.sun.cluster.agent.node;

// JDK
import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerNotification;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;

// JDMK
import com.sun.jdmk.JdmkMBeanServer;
import com.sun.jdmk.ServiceName;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.CachedVirtualMBeanInterceptor;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.event.ClEventDefs;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.event.SysEventNotifierMBean;
import com.sun.cluster.common.ClusterPaths;


/**
 * An node subclass of the CMASS intercegptor, to create and manage virtual
 * mbeans that have a classic CMASS implementation - all instance and attribute
 * values are fetched through JNI, and all operations are performed through an
 * invocation of a CLI.
 */

public class NodeInterceptor extends CachedVirtualMBeanInterceptor
    implements NotificationListener {

    /* DELIMETER */
    private static String COLON = ":";

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.node");
    private final static String logTag = "NodeInterceptor";

    /* Internal reference to the SysEvent Notifier. */
    private ObjectName sysEventNotifier;

    /* The MBeanServer in which this interceptor is inserted. */
    private MBeanServer mBeanServer;

    /**
     * Constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this interceptor.
     * @param  dispatcher  DomainDispatcher that this virtual mbean interceptor
     * should register into for dispatching.
     * @param  onf  ObjectNameFactory implementation to be used by this
     * VirtualMBeanInterceptor
     */

    public NodeInterceptor(MBeanServer mBeanServer,
        VirtualMBeanDomainDispatcher dispatcher, ObjectNameFactory onf) {
        super(mBeanServer, dispatcher, onf, NodeMBean.class, null);
        this.mBeanServer = mBeanServer;
        this.sysEventNotifier =
            new ObjectNameFactory("com.sun.cluster.agent.event").getObjectName(
                SysEventNotifierMBean.class, null);
    }

    /**
     * Called when the module is unlocked.
     */
    public void unlock() throws Exception {
        super.unlock();
        logger.entering(logTag, "start");

        try {

            // No need to use a proxy here, we can go directly to the
            // addNotificationListener method on the mbean server.
            mBeanServer.addNotificationListener(sysEventNotifier, this, null,
                null);
        } catch (InstanceNotFoundException e) {
            logger.warning("unable to register for node config change events");
        }
    }

    /**
     * Called when the module is locked.
     */
    public void lock() throws Exception {
        super.lock();
        logger.entering(logTag, "stop");

        // Unregister for event listening.
        try {

            // No need to use a proxy here, we can go directly to the
            // addNotificationListener method on the mbean server.
            mBeanServer.removeNotificationListener(sysEventNotifier, this, null,
                null);
        } catch (Exception e) {
            logger.warning("Unable to unregister node config listener");
        }

        invalidateCache();
    }

    /**
     * The interceptor should monitor create/destroy/attribute-change events and
     * emit appropriate notifications from the mbean server (for create/destroy)
     * or from the virtual mbean (attribute change).
     *
     * <p>Our interceptor monitors the addition or removal of nodes
     *
     * <p>This method implements the NotificationListener interface.
     *
     * @param  notification  a <code>Notification</code> value
     * @param  handback  an <code>Object</code> value
     */
    public void handleNotification(Notification notification, Object handback) {

        if (logger.isLoggable(Level.FINE))
            logger.fine("received cluster event " + notification);


        if (!(notification instanceof SysEventNotification))
            return;

        invalidateCache();

        SysEventNotification clEvent = (SysEventNotification) notification;

        if (clEvent.getSubclass().equals(
                    ClEventDefs.ESC_CLUSTER_NODE_STATE_CHANGE)) {

            Map clEventAttrs = clEvent.getAttrs();

            String name = (String) clEventAttrs.get(ClEventDefs.CL_NODE_NAME);

            int clReasonCodeInt =
                ((Long) (clEventAttrs.get(ClEventDefs.CL_REASON_CODE)))
                .intValue();

            // workaround for JDK1.5/1.6 to make sure the Enum is referenced.
            ClEventDefs.ClEventReasonCodeEnum reasonCode = 
                ClEventDefs.ClEventReasonCodeEnum.CL_REASON_RG_SWITCHOVER;
            reasonCode = (ClEventDefs.ClEventReasonCodeEnum)
                (ClEventDefs.ClEventReasonCodeEnum.getEnum(
                        ClEventDefs.ClEventReasonCodeEnum.class,
                        clReasonCodeInt));

            if (reasonCode ==
                    ClEventDefs.ClEventReasonCodeEnum.
                    CL_REASON_CMM_NODE_JOINED) {

                // Send an MBean registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Register MBean " + name);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            NodeMBean.class, name);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.REGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "can't send notification", e);
                }
            } else if ((reasonCode ==
                        ClEventDefs.ClEventReasonCodeEnum.
                        CL_REASON_CMM_NODE_LEFT) ||
                    (reasonCode ==
                        ClEventDefs.ClEventReasonCodeEnum.
                        CL_REASON_CMM_NODE_SHUTDOWN)) {

                // Send an MBean un-registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Register MBean " + name);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            NodeMBean.class, name);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.UNREGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "can't send notification", e);
                }
            } else {
                logger.fine("NodeInterceptor: Ignoring cmm node state change " +
                    reasonCode);
            }
        }
    }

    /**
     * Function called by the generic part of the virtual MBean interceptor (the
     * super class) for invoking an operation of an MBean. Note that this is not
     * part of the MBeanInterceptor interface as its getAttributes function has
     * a different signature.
     *
     * @param  name  The name of the MBean on which the method is to be invoked.
     * @param  operationName  The name of the operation to be invoked.
     * @param  params  An array containing the parameters to be set when the
     * operation is invoked.
     * @param  signature  An array containing the signature of the operation.
     * The class objects will be loaded using the same class loader as the one
     * used for loading the MBean on which the operation was invoked.
     *
     * @return  The object returned by the operation, which represents the
     * result ofinvoking the operation on the MBean specified.
     *
     * @throws  InstanceNotFoundException  if an error occurs
     * @throws  MBeanException  An error occurs
     * @throws  ReflectionException  An error occurs
     * @throws  IOException  An error occurs
     */
    public Object invoke(String name, String operationName, Object params[],
        String signature[]) throws InstanceNotFoundException, MBeanException,
        ReflectionException, IOException {

        if (!isRegistered(name)) {
            throw new InstanceNotFoundException("cannot find the instance " +
                name);
        }

        if (operationName.equals("evacuate")) {

            String cmds[][] = {
                    { ClusterPaths.CL_NODE_CMD, "evacuate", name },
                };

            // Run the command(s)
            InvocationStatus exits[];

            try {
                exits = InvokeCommand.execute(cmds, null);
            } catch (InvocationException ie) {
                exits = ie.getInvocationStatusArray();
                throw new MBeanException(new CommandExecutionException(
                        ie.getMessage(), ExitStatus.createArray(exits)));
            }

            invalidateCache();

            return ExitStatus.createArray(exits);
        } else if (operationName.equals("saveRebootNodeProperty")) {

            if ((signature != null) && (signature.length == 2) &&
                    (signature[0].equals("java.lang.String")) &&
                    (signature[1].equals("boolean"))) {
                String nodeName = (String) params[0];

                if (((Boolean) params[1]).booleanValue()) {

                    String cmds[][] = {
                            {
                                ClusterPaths.CL_NODE_CMD, "set", "-p",
                                "reboot_on_path_failure=enabled", nodeName
                            },
                        };

                    // Run the command(s)
                    InvocationStatus exits[];

                    try {
                        exits = InvokeCommand.execute(cmds, null);
                    } catch (InvocationException ie) {
                        exits = ie.getInvocationStatusArray();
                        throw new MBeanException(new CommandExecutionException(
                                ie.getMessage(),
                                ExitStatus.createArray(exits)));
                    }

                    invalidateCache();

                    return ExitStatus.createArray(exits);
                } else {
                    String cmds[][] = {
                            {
                                ClusterPaths.CL_NODE_CMD, "set", "-p",
                                "reboot_on_path_failure=disabled", nodeName
                            },
                        };

                    // Run the command(s)
                    InvocationStatus exits[];

                    try {
                        exits = InvokeCommand.execute(cmds, null);
                    } catch (InvocationException ie) {
                        exits = ie.getInvocationStatusArray();
                        throw new MBeanException(new CommandExecutionException(
                                ie.getMessage(),
                                ExitStatus.createArray(exits)));
                    }

                    invalidateCache();

                    return ExitStatus.createArray(exits);
                }
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("obtainProcesses")) {
            String zonename = null;

            if ((signature != null) &&
                    (signature[0].equals("java.lang.String"))) {
                zonename = (String) params[0];

                return obtainProcesses(zonename);
            }
        } else if (operationName.equals("obtainSchedulingClass")) {
            String cmds[][] = new String[][] {
                    {
                        ClusterPaths.NEWTASK_CMD, "-p", "default",
                        ClusterPaths.SCSLM_GETSCHED_CMD
                    }
                };

            // Run the command(s)
            InvocationStatus exits[];

            try {
                exits = InvokeCommand.execute(cmds, null);
            } catch (InvocationException ie) {
                exits = ie.getInvocationStatusArray();
                throw new MBeanException(new CommandExecutionException(
                        ie.getMessage(), ExitStatus.createArray(exits)));
            }

            return ExitStatus.createArray(exits);
        }

        throw new ReflectionException(new IllegalArgumentException(
                operationName));
    }

    /**
     * Fill the cache
     *
     * @return  a <code>Map</code>, key is instance name, value is a map of
     * attribute name to attribute value.
     */
    protected native Map fillCache();

    /**
     * Function to return an array of NodeProcess[] objects that contain all
     * currently running processes on this node or the zonename
     */
    private native NodeProcess[] obtainProcesses(String zonename);
}
