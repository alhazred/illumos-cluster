/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NodeMBean.java 1.9     08/05/20 SMI"
 */
package com.sun.cluster.agent.node;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available on a Cluster Node.
 */
public interface NodeMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "node";

    /**
     * Get the node's key name.
     *
     * @return  node's key name.
     */
    public String getName();

    /**
     * Get the node's default public Inet address
     *
     * @return  node's default public Inet address
     */
    public String getPublicInetAddress();


    /**
     * Give the "online" status of the node. If this method returns <code>
     * false</code>, the node might be in a transition mode; use the
     * {@link #isInTransition()} method to get the information.
     *
     * @return  <code>true</code> if the node is online and accessible, <code>
     * false</code> if not.
     */
    public boolean isOnline();

    /**
     * Give the "Enabled" status of the node.
     */
    public boolean isEnabled();


    /**
     * Enables to know if the node is in a transition mode between two states.
     *
     * @return  <code>true</code> if the node is in transition, <code>
     * false</code> if not.
     */
    public boolean isInTransition();

    /**
     * Get node's ID.
     *
     * @return  node's ID number.
     */
    public int getNodeId();

    /**
     * Get Reboot once on failure of all local disks status.
     *
     * @return  status.
     */
    public boolean isNodeRebootEnabled();

    /**
     * Get node's private host name. The returned name does not contain the
     * domain.
     *
     * @return  private host name.
     */
    public String getPrivateHostname();

    /**
     * Get Node Type. To determine whether the node is a cluster node or a farm
     * node
     *
     * @return  String which determines the node type
     */

    public String getNodeType();

    /**
     * Get the total of votes gained from quorum.
     *
     * @return  the number of gained votes.
     */
    public int getQuorumCurrentVotes();

    /**
     * Get the total of possible votes from quorum.
     *
     * @return  the number of possible votes.
     */
    public int getQuorumPossibleVotes();

    /**
     * Get node's reservation number.
     *
     * @return  the reservation key.
     */
    public String getReservationKey();

    /**
     * Get machine's type using for this node. For example : "SUNWm Ultra-10"
     *
     * @return  the machine's type.
     */
    public String getMachineType();

    /**
     * Get operating system name and version, example SunOS 5.9
     * Generic_112233-03
     *
     * @return  os name and version
     */
    public String getOperatingSystemInfo();

    /**
     * Get node's memory size (only available on node local to agent)
     *
     * @return  memory size in bytes.
     */
    public long getMemorySize();

    /**
     * Get number of CPUs contained in this node. (only available on node local
     * to agent)
     *
     * @return  the number of CPUs.
     */
    public int getCpuCount();

    /**
     * Get the machine uptime in seconds since last reboot (only available on
     * node local to agent)
     *
     * @return  node's up time.
     */
    public long getUptime();

    /**
     * Whether zones are supported or not.
     */
    public boolean isZonesSupported();

    /**
     * Obtain the processes running on this node. (only available on node local
     * to agent) if a zonename is provided, the processes running on the
     * specified zone are obtained. This is implemented as an operation rather
     * than an attribute since it is expensive to calculate
     *
     * @return  an array of {@link NodeProcess} objects.
     */
    public NodeProcess[] obtainProcesses(String zonename);

    /**
     * Get ZoneSchedulingClass given the zone name
     *
     * @return  an {@link ExitStatus[][]} array,
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] obtainSchedulingClass()
        throws CommandExecutionException;

    /**
     * Evacuate the node. This will evacuate all resource groups and shared
     * storage from this node, adjust the quorum count for any shared storage
     * devices and recalculate the quorum count. We could also named this method
     * "takeOffline" but this less reflects the underlying performed operations.
     *
     * @return  an {@link ExitStatus[][]} array, multiple CLI commands may be
     * used
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] evacuate() throws CommandExecutionException;

    /**
     * Edit node property button
     */

    public ExitStatus[] saveRebootNodeProperty(String nodeName, boolean standby)
        throws CommandExecutionException;

    /**
     * Get the set of private interconnects connected to this node as String.
     * This field applies to Farm Nodes Only
     */

    public String getPrivateAdapters();

}
