/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ZoneMBean.java 1.8     08/05/21 SMI"
 */
package com.sun.cluster.agent.node;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available on a Zone.
 */
public interface ZoneMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "zone";

    /**
     * Get the zone's key name.
     *
     * @return  zone's key name.
     */
    public String getName();

    /**
     * Get zone's private host name. The returned name does not contain the
     * domain.
     *
     * @return  private host name.
     */
    public String getPrivateHostname();

    /**
     * Get the node name on which the zone is configured.
     *
     * @return  node name.
     */
    public String getNodeName();

    /**
     * Get the zone's pset's size.
     *
     * @return  zone's pset's size.
     */
    public Integer getPsetSize();

    /**
     * Get the zone's pset name.
     *
     * @return  zone's pset name.
     */
    public String getPset();

    /**
     * @return  zone's IP type: "shared" or "excl", or "unknown" if not
     *		found
     */
    public String getIpType();

    /**
     * Gets a list of IPMP groups defined in the zone. 
     * Only works if zone is Exclusive IP type.
     *
     * @return	Returns a single string consisting of a colon separated
     *		list of IPMP groups.
     *		Returns an empty string if none defined.
     */
    public String getIPMPGroups();

    /**
     * Get ZoneDetails given the zone name
     *
     * @return  an {@link ExitStatus[][]} array,
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] getZoneDetails(String zoneName)
        throws CommandExecutionException;

    /**
     * Get ZoneSchedulingClass given the zone name
     *
     * @return  an {@link ExitStatus[][]} array,
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] obtainZoneSchedulingClass(String zoneName)
        throws CommandExecutionException;

    /**
     * enable zone for private IP communication given the node:zone name and
     * optional hostname
     *
     * @return  an {@link ExitStatus[][]} array,
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] enablePrivateIP(String nodeKeyName, String hostName)
        throws CommandExecutionException;

    /**
     * disable zone for private IP communication given the node:zone name
     *
     * @return  an {@link ExitStatus[][]} array,
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] disablePrivateIP(String nodeKeyName)
        throws CommandExecutionException;

    /**
     * change private hostname assigned to the zone.
     *
     * @return  an {@link ExitStatus[][]} array,
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changePrivateIP(String nodeKeyName, String hostName)
        throws CommandExecutionException;

}
