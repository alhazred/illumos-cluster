/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)Node.java 1.6     08/05/20 SMI"
 */
package com.sun.cluster.agent.node;

/**
 * Helper class for modules to be able to recover the local node name
 */

public class Node {

    private static String nodeName = null;

    /**
     * Return the local node name
     *
     * @return  the local node name
     */
    public static String getNodeName() {

        if (nodeName == null) {
            nodeName = getNodeNameJNI();
        }

        return nodeName;
    }

    private static native String getNodeNameJNI();
}
