/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Node mbean interceptor
 */
#pragma ident	"@(#)zone_interceptor_jni.c	1.8	08/05/21 SMI"

#include "com/sun/cluster/agent/node/ZoneInterceptor.h"
#include <jniutil.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <sys/processor.h>
#include <procfs.h>
#include <utmpx.h>
#include <dirent.h>
#include <pwd.h>
#include <wchar.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pool.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif /* SOL_VERSION >= __s10 */

/*
 * A couple of helper macros for things we do a lot,
 * they contain references to local variables in the
 * function below
 */
#define	PUT_ATTR(MAP, NAME, VALUE)		\
	PUT(MAP, NAME, (*env)->NewObject(env,	\
	    attribute_class,			\
	    attribute_constructor,		\
	    (*env)->NewStringUTF(env, NAME),	\
	    VALUE))

#define	PUT(MAP, NAME, VALUE)						\
	(void) (*env)->CallObjectMethod(env,				\
				    MAP,				\
				    map_put,				\
				    (*env)->NewStringUTF(env, NAME),	\
				    VALUE)


#if SOL_VERSION >= __s10

static int logging = 0;
#define	ERROR_STRING_MAX 1024
#define	ERROR_STR_FMT_MAX 256


typedef enum {
	POOL,
	RES
} passover_type_t;

typedef struct {
	int64_t poolid;
	int pset_size;
	int found;
	char *pset_name;
} pool_inf_t;

struct passover {
	passover_type_t type;
	pool_inf_t *pool;
};

void
set_error_str(const char *format, ...)
{

	char error_str[ERROR_STRING_MAX];
	char error_str_fmt[ERROR_STR_FMT_MAX];
	va_list ap;

	if (logging == 0) {
		return;
	}

	if (ERROR_STR_FMT_MAX < strlen(format)) {
		(void) snprintf(error_str, (size_t)ERROR_STRING_MAX,
		    "ERROR_STR_FMT_MAX too small");
		return;
	}

	(void) strcpy(error_str_fmt, format);

	/*lint -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, format);
	/*lint +e40 */
	(void) vsnprintf(error_str, (size_t)ERROR_STRING_MAX,
	    error_str_fmt, ap);
	va_end(ap);

	(void) fprintf(stderr, "%s\n", error_str);
}



/*
 * libpool manipulation functions.
 */

/*
 * property_callback(conf, element, name, val, arg) is the callback used with
 * the libpool property walker. It behaves differently based on the property
 * type. 0 is returned on success, -1 on failure.
 */
/*ARGSUSED1*/
static int
property_callback(pool_conf_t *conf, pool_elem_t *elem, const char *name,
    pool_value_t *val, void *arg)
{
	static const char *pset_size_prop = "pset.size";
	static const char *pset_name_prop = "pset.name";
	static const char *pool_id_prop = "pool.sys_id";

	struct passover *po = (struct passover *)arg;

	switch (po->type) {
	case POOL:
		if (strncmp(name, pool_id_prop, strlen(pool_id_prop)) == 0) {
			int64_t id;

			/*
			 * We got a pool id property.
			 */
			if (pool_value_get_type(val) != POC_INT) exit(1);

			if (pool_value_get_int64(val, &id) < 0) {
				set_error_str("pool_value_get_int64 failed"
				    " (%s)", pool_strerror(pool_error()));
				return (-1);
			}


			if (po->pool->poolid == id) {
				/*
				 * The poolid corresponds to what caller is
				 * looking for.
				 */

				po->pool->found = 1;
			}
		}
		break;
	case RES:

		if (strncmp(name, pset_size_prop,
			strlen(pset_size_prop)) == 0) {
			uint64_t size;

			/*
			 * We got the pset size property.
			 */

			/* get the pset size */
			if (pool_value_get_uint64(val, &size) < 0) {
				set_error_str("(pool_value_get_int64 failed"
				    " (%s)", pool_strerror(pool_error()));
				return (-1);
			}

			/* store the pset size */
			po->pool->pset_size = (int)size;
		}
		if (strncmp(name, pset_name_prop,
			strlen(pset_name_prop)) == 0) {
			const char *pset_name = NULL;

			/*
			 * We got the pset name property.
			 */

			/* get the pset name */
			if (pool_value_get_string(val, &pset_name) < 0) {
				set_error_str("(pool_value_get_string failed"
				    " (%s)", pool_strerror(pool_error()));
				return (-1);
			}

			/* store the pset size */
			po->pool->pset_name = strdup(pset_name);
			if (po->pool->pset_name == NULL) {
				set_error_str("strdup failed");
			}

		}


		break;
	default:
		set_error_str("Internal bug");
		return (-1);
	}

	return (0);
}


/*
 * resource_callback(conf, res, arg) is the callback used with the libpool
 * resource walker. 0 is returned on success, -1 on failure.
 */
static int
resource_callback(pool_conf_t *conf, pool_resource_t *res, void *arg)
{
	pool_elem_t *elem;
	struct passover *po = (struct passover *)arg;

	if ((elem = pool_resource_to_elem(conf, res)) == NULL) {
		set_error_str("pool_resource_to_elem: %s\n",
		    pool_strerror(pool_error()));
		return (-1);
	}

	po->type = RES;

	if (pool_walk_properties(conf, elem, po, property_callback) < 0) {
		set_error_str("pool_walk_properties: %s\n",
		    pool_strerror(pool_error()));
		return (-1);
	}
	po->type = POOL;

	return (0);
}

/*
 * pool_callback(conf, pool, arg) is the callback used with the libpool pool
 * walker. 0 is returned on success, -1 on failure.
 */
static int
pool_callback(pool_conf_t *conf, pool_t *pool, void *arg)
{
	pool_elem_t *elem;
	struct passover *po = (struct passover *)arg;

	if ((elem = pool_to_elem(conf, pool)) == NULL) {
		set_error_str("pool_to_elem: %s\n",
		    pool_strerror(pool_error()));
		return (-1);
	}

	po->pool->found = 0;

	if (pool_walk_properties(conf, elem, po, property_callback) < 0) {
		set_error_str("pool_walk_properties: %s\n",
		    pool_strerror(pool_error()));
		return (-1);
	}

	if (po->pool->found == 1) {
		(void) pool_walk_resources(conf, pool, po, resource_callback);
	}

	return (0);
}


pool_conf_t*
initpool()
{
	pool_conf_t *pool_conf = NULL;

	if ((pool_conf = pool_conf_alloc()) == NULL) {
		set_error_str("pool_conf_alloc failed (%s)",
		    pool_strerror(pool_error()));
		return (NULL);
	}

	if (pool_conf_open(pool_conf, pool_dynamic_location(), PO_RDONLY)
	    != PO_SUCCESS) {

		set_error_str("pool_conf_open failed (%s)",
		    pool_strerror(pool_error()));
		return (NULL);
	}

	return (pool_conf);
}

void
closepool(pool_conf_t *pool_conf)
{
	(void) pool_conf_close(pool_conf);
}

/*
 * lookuppool(poolid) walks through the resource pools (using Solaris
 * pool_walk_pools func) looking for the pool identified by poolid. The pool_t
 * object is returned on success, and NULL on failure.
 */
void
lookuppool(pool_conf_t *pool_conf, pool_inf_t *pinfo)
{
	struct passover po;

	po.type = POOL;
	po.pool = pinfo;
	if (pool_walk_pools(pool_conf, &po, pool_callback) < 0) {
		set_error_str("pool_walk_pools: %s\n",
		    pool_strerror(pool_error()));
	}
}


#endif /* SOL_VERSION >= __s10 */

char *
get_iptype(pnm_zone_iptype_t iptype)
{
	char *t = NULL;
	if (iptype == SHARED_IP)
	    t = "shared";
	else if (iptype == EXCLUSIVE_IP)
	    t = "excl";
	else
	    t = "unknown";
	return (t);
}

/*
 * Class:     com_sun_cluster_agent_example_zoneInterceptor
 * Method:    fillCache
 * Signature: ()Ljava/util/Map;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_node_ZoneInterceptor_fillCache(
    JNIEnv * env, jobject this)
{

	/*
	 * return a Map of Maps containing all instances
	 * key = instanceName
	 * value = Map
	 *		key=attributeName
	 *		value=corresponding Attribute object
	 */

	jobject cache_map = NULL;
	jobject instance_map = NULL;
	jclass map_class = NULL;
	jmethodID map_put;

	jclass attribute_class = NULL;
	jmethodID attribute_constructor = NULL;

#if SOL_VERSION >= __s10
	pool_conf_t *pool_conf = NULL;
#endif /* SOL_VERSION >= __s10 */

	/*
	 * CLUSTER handle.
	 */
	scha_cluster_t sc_handle = NULL;

	scha_err_t scha_error;

	scha_str_array_t *logical_nodenames = NULL;

	unsigned int i;
	int ret;
	pnm_zone_iptype_t iptype;
	pnm_group_list_t glist = NULL;

	/*
	 * Initialize clconf library
	 */

	clconf_lib_init();

	/*
	 * get our references to 'attribute_class'
	 */

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * get constructor
	 */
	attribute_constructor =
		(*env)->GetMethodID(env,
		    attribute_class,
		    "<init>",
		    "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;

	/*
	 * Create the map we are going to return
	 */
	cache_map = newObjNullParam(env, "java/util/HashMap");
	if (cache_map == NULL)
		goto error;

	/*
	 * get our references to map_class and the put method
	 */
	map_class = (*env)->FindClass(env, "java/util/Map");
	if (map_class == NULL)
		goto error;
	map_put =
		(*env)->GetMethodID(env,
		    map_class,
		    "put",
		    "(Ljava/lang/Object;Ljava/lang/Object;)"
		    "Ljava/lang/Object;");
	if (map_put == NULL)
		goto error;

	scha_error = scha_cluster_open(&sc_handle);

	if (scha_error != SCHA_ERR_NOERR) {
		sc_handle = NULL;
		goto error;
	}

	/*
	 * Get all logical nodenames known to the RGM within the cluster
	 */
	scha_error = scha_cluster_get(sc_handle,
	    SCHA_ALL_ZONES,
	    &logical_nodenames);

	if ((scha_error != SCHA_ERR_NOERR) ||
	    (logical_nodenames == NULL)) {
		goto error;
	}

#if SOL_VERSION >= __s10
	pool_conf = initpool();
#endif /* SOL_VERSION >= __s10 */

	for (i = 0; i < logical_nodenames->array_cnt; i++) {

		/*
		 * Pointer to private hostname.
		 */
		char *clprivnet;

		/*
		 * Pointer to zonename
		 */
		char *zone_ptr = NULL;
		char *name = NULL;

#if SOL_VERSION >= __s10
		int zoneid = -1;

		/*
		 * Pool information
		 */

		poolid_t poolid;

		pool_inf_t pinfo;

#endif /* SOL_VERSION >= __s10 */

		/*
		 * we are going to create a map for this instance and
		 * populate it with its attributes
		 */

		instance_map = newObjNullParam(env, "java/util/HashMap");
		if (instance_map == NULL)
			goto error;

		name =
			strdup(logical_nodenames->str_array[i]);
		if (name == NULL)
			goto error;
		/*
		 * List returned is of the form nodename:zonename.
		 */


		zone_ptr = strchr(name, ':');
		if (zone_ptr != NULL) {
			/*
			 * Zero out the colon
			 */
			*zone_ptr = '\0';
			++zone_ptr;
			PUT_ATTR(instance_map, "Name",
			    (*env)->NewStringUTF(env, (char *)
				logical_nodenames->str_array[i]));

			PUT_ATTR(instance_map, "NodeName",
			    (*env)->NewStringUTF(env, (char *)
				name));

			scha_error = scha_cluster_get(sc_handle,
			    SCHA_PRIVATELINK_HOSTNAME_NODE,
			    logical_nodenames->str_array[i],
			    &clprivnet);

			if ((scha_error != SCHA_ERR_NOERR) ||
			    (clprivnet == NULL)) {
				/*
				 * Private IP has not been enabled.
				 */
				PUT_ATTR(instance_map, "PrivateHostname",
				    (*env)->NewStringUTF(env, NULL));
			} else {
				/*
				 * Private IP has been enabled.
				 */
				PUT_ATTR(instance_map, "PrivateHostname",
				    (*env)->NewStringUTF(env, clprivnet));
			}

#if SOL_VERSION >= __s10

			/*
			 * Retrieve Pset Information
			 */

			(void) memset(&pinfo, 0, sizeof (pinfo));

			if (pool_conf != NULL) {
				zoneid = getzoneidbyname(zone_ptr);
			}

			if (zoneid != -1) {
				ssize_t ret = 0;

				ret = zone_getattr(zoneid, ZONE_ATTR_POOLID,
				    &poolid, sizeof (poolid));

				if (ret >= (ssize_t)0) {

					/*
					 * Found poolid
					 */

					pinfo.poolid = poolid;
					lookuppool(pool_conf, &pinfo);
				}
			}

			if (pinfo.pset_name == NULL) {
				PUT_ATTR(instance_map, "Pset",
				    (*env)->NewStringUTF(env, NULL));
			} else {
				PUT_ATTR(instance_map, "Pset",
				    (*env)->NewStringUTF(env,
					pinfo.pset_name));
				free(pinfo.pset_name);
			}

			if (pinfo.pset_size == 0) {
				PUT_ATTR(instance_map, "PsetSize",
				    newObjInt32Param(env,
					"java/lang/Integer",
					NULL));
			} else {
				PUT_ATTR(instance_map, "PsetSize",
				    newObjInt32Param(env,
					"java/lang/Integer",
					pinfo.pset_size));
			}

			/*
			 * Get zone IP type
			 */

			ret = pnm_zone_init(name, zone_ptr);
			if (ret == 0) {
			    ret = pnm_get_zone_iptype(zone_ptr, &iptype);
			}

			if (ret == 0) {
			    PUT_ATTR(instance_map, "IpType",
				(*env)->NewStringUTF(env, get_iptype(iptype)));
			}

			if (iptype == EXCLUSIVE_IP) {
				/*
				 * Get zone IP type
				 */
				ret = pnm_group_list(&glist);
				if (ret == 0) {
				    PUT_ATTR(instance_map, "IPMPGroups",
					(*env)->NewStringUTF(env, glist));
				}
			}
			pnm_fini();

#endif /* SOL_VERSION >= __s10 */

			/*
			 * Now put this map into the returned map under this
			 * instance
			 */


			PUT(cache_map,
			    logical_nodenames->str_array[i],
			    instance_map);


		}
		free(name);
	}


error:
	/*
	 * Free the specific resources allocated in this function
	 */

	if (sc_handle != NULL) {
		/*
		 * free memory pointed to by cluster_names
		 */
		(void) scha_cluster_close(sc_handle);
	}

#if SOL_VERSION >= __s10
	if (pool_conf != NULL) {
		closepool(pool_conf);
	}
#endif /* SOL_VERSION >= __s10 */

	return (cache_map);

} /*lint !e715 */
