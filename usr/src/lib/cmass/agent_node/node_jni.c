/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Node mbean interceptor
 */

#pragma ident	"@(#)node_jni.c	1.4	08/05/20 SMI"

#include <stdlib.h>
#include <stdio.h>

#include "com/sun/cluster/agent/node/Node.h"

#include <jniutil.h>
#include <scha.h>

/*
 * Class: com_sun_cluster_agent_node_Node
 * Method: getNodeNameJNI
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_com_sun_cluster_agent_node_Node_getNodeNameJNI
	(JNIEnv * env, jclass clazz) {

	scha_cluster_t sc_handle = NULL;
	scha_err_t scha_error;

	char *node_name = NULL;
	jstring return_string = NULL;


	/*
	 * get a handle to the Cluster
	 */
	scha_error = scha_cluster_open(&sc_handle);
	if (scha_error != SCHA_ERR_NOERR) {
		sc_handle = NULL;
		goto error;
	}
	/*
	 * get node name
	 */
	scha_error =
	    scha_cluster_get(sc_handle, SCHA_NODENAME_LOCAL, &node_name);
	if (scha_error != SCHA_ERR_NOERR)
		goto error;

	return_string = (jstring) newObjStringParam(env,
	    "java/lang/String",
	    node_name);

    error:

	/*
	 * free the SCHA cluster handle
	 */
	if (sc_handle != NULL) {
		(void) scha_cluster_close(sc_handle);
	}
	/*
	 * node name will be freed by scha_cluster_close
	 */


	return (return_string);
} /*lint !e715 */
