/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ZoneClusterGroup.java	1.4	08/07/10 SMI"
 */
package com.sun.cluster.agent.zonecluster;

import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.ClusterPaths;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Class implementing MBean interface, holding utility functions to read
 * ZoneCluster data
 */
public class ZoneClusterGroup implements ZoneClusterGroupMBean {

    private static Logger logger =
            Logger.getLogger("com.sun.cluster.agent.zonecluster");
    private static final String logTag = "ZoneClusterGroup";
    private static boolean quiet = false;
    private static final String lib = ClusterPaths.cmass +
                                        "libcmas_agent_zonecluster.so";

    public ZoneClusterGroup() {
    }


    /**
     * Native method to read data corresponding to ZoneClusters
     * @return HashMap with following structure
     *                  KEY                     VALUE
     *                  ZoneClusterName         ZoneClusterData Object
     */
    private native HashMap readDataJNI();

    /**
     * Utility method to read data corresponding to ZoneClusters
     * @return HashMap with following structure
     *                  KEY                     VALUE
     *                  ZoneClusterName         ZoneClusterData Object
     */
    public HashMap readData() {
        HashMap zcDataMap = new HashMap();
        Object tmpObj = readDataJNI();
        if (tmpObj != null) {
            zcDataMap = (HashMap)tmpObj;
        }
        return zcDataMap;
    }

    /**
     * Utility function to retrieve scheduling class for the cluster zone
     * present on this node.
     * @param zoneClusterName
     * @return
     * @throws com.sun.cluster.agent.auth.CommandExecutionException
     */
    public ExitStatus[] obtainZoneSchedulingClass(String zoneClusterName)
            throws CommandExecutionException {
        String[][] cmds = new String[][] {
            {
                ClusterPaths.ZLOGIN_CMD, zoneClusterName,
                ClusterPaths.SCSLM_GETSCHED_CMD
            }
        };

        // Run the command
        InvocationStatus exits[] = null;

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(
                    ie.getMessage(), ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     *
     * @param args  -n <n>	repeat test <n> times
     *		    -q		quiet mode
     *
     * @return boolean status of readData method
     */
    public static boolean test(String[] args) {
        ZoneClusterGroup zc = new ZoneClusterGroup();

        int iterations = 1;

        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-q")) {
                quiet = true;
            } else if (args[i].equals("-n")) {
                iterations = Integer.parseInt(args[++i]);
            }
        }

        try {
            // Load the agent JNI library
            Runtime.getRuntime().load(lib);
        } catch (UnsatisfiedLinkError ule) {
            print("load of " + lib + " failed!");
            ule.printStackTrace();
            return false;
        }

        long start = System.currentTimeMillis();
        long loopstart;
        long loopelapsed;
        int count = 0;
        print("\nReading data " + iterations + " time(s) ... ");
        for (int i = 0; i < iterations; i++) {
            try {
                loopstart = System.currentTimeMillis();
                HashMap zcDataMap = zc.readData();
                if (zcDataMap == null) {
                    print("readData returned null!");
                    return false;
                }
                count = i + 1;
                print("\nREAD " + count + " : returned " + zcDataMap.size()
                        + " zoneclusters");
                if (!quiet) {
                    print("Zone Clusters :  \n" + zcDataMap);
                }
                loopelapsed = System.currentTimeMillis() - loopstart;
                print("time = " + loopelapsed + " ms");
            } catch (Exception e) {
                print("readDataJNI failed!");
                print(e.getMessage());
                e.printStackTrace();
                return false;
            }
        }
        quiet = false;
        long elapsed = System.currentTimeMillis() - start;
        print("\nTotal Time for " + iterations + " iterations = " +
                        elapsed + " ms");

        return true;
    }

    private static void print(String msg) {
        System.out.println(msg);
        logger.fine(msg);
    }
}
