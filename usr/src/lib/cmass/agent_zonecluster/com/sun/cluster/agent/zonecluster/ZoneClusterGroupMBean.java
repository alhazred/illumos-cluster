/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ZoneClusterGroupMBean.java	1.3	08/07/10 SMI"
 */
package com.sun.cluster.agent.zonecluster;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import java.util.HashMap;


/**
 * MBean Interface holding utility functions to read ZoneCluster data
 */
public interface ZoneClusterGroupMBean {

    /**
     * This field is used internally by the CMASS agent to identify the
     * TYPE of the module.
     */
    public static final String TYPE = "zoneclustergroup";

    /**
     * Utility method to read data corresponding to ZoneClusters
     * @return HashMap with following structure
     *                  KEY                     VALUE
     *                  ZoneClusterName         ZoneClusterData Object
     */
    public HashMap readData();

    /**
     * Utility function to retrieve scheduling class for the cluster zone
     * present on this node.
     * @param zoneClusterName
     * @return
     * @throws com.sun.cluster.agent.auth.CommandExecutionException
     */
    public ExitStatus[] obtainZoneSchedulingClass(String zoneClusterName)
            throws CommandExecutionException;
}
