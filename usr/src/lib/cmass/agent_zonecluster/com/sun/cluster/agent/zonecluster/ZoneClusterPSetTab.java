/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/**
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ZoneClusterPSetTab.java	1.2	08/07/10 SMI"
 */
package com.sun.cluster.agent.zonecluster;

import java.io.Serializable;

/**
 * An instance of this object represent pset allocation to the zonecluster.
 * This maps one-to-one with the zc_psettab struct defined in libclzonecfg.h.
 * struct zc_psettab {
 *   	char	zc_ncpu_min[MAXNAMELEN];
 * 	char	zc_ncpu_max[MAXNAMELEN];
 * 	char	zc_importance[MAXNAMELEN];
 * };
 *
 */
public class ZoneClusterPSetTab implements Serializable {

    private String ncpu_min;
    private String ncpu_max;
    private String importance;

    /** Default empty constructor */
    public ZoneClusterPSetTab() {}

    /**
     * Full constructor used by jni
     * @param ncpu_min
     * @param ncpu_max
     * @param importance
     */
    public ZoneClusterPSetTab(
			String ncpu_min,
			String ncpu_max,
			String importance) {
	this.ncpu_min = ncpu_min;
	this.ncpu_max = ncpu_max;
	this.importance = importance;
    }


    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("[ncpu_min= " + getNcpu_min());
        s.append(", ncpu_max= " + getNcpu_max());
        s.append(", importance= " + getImportance());
        s.append("]");
        return s.toString();
    }

    public String getNcpu_min() {
        return ncpu_min;
    }

    public void setNcpu_min(String ncpu_min) {
        this.ncpu_min = ncpu_min;
    }

    public String getNcpu_max() {
        return ncpu_max;
    }

    public void setNcpu_max(String ncpu_max) {
        this.ncpu_max = ncpu_max;
    }

    public String getImportance() {
        return importance;
    }

    public void setImportance(String importance) {
        this.importance = importance;
    }
}
