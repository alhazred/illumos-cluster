/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/**
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ZoneClusterModule.java	1.3	08/07/10 SMI"
 */
package com.sun.cluster.agent.zonecluster;

// JDK
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JDMK
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// Local
import com.sun.cluster.common.ClusterRuntimeException;

/**
 * ZoneCluster module using the interceptor and JNI design patterns
 */
public class ZoneClusterModule extends Module {

    /* Tracing utility. */
    private static Logger logger =
        Logger.getLogger("com.sun.cluster.agent.zonecluster");

    /**
     * Constructor called by the container.
     * @param descriptor
     */
    public ZoneClusterModule(DeploymentDescriptor descriptor) {
	super(descriptor);
	Map parameters = descriptor.getParameters();
	try {
	    // Load the agent JNI library
	    String library = (String) parameters.get("library");
	    Runtime.getRuntime().load(library);

	} catch (UnsatisfiedLinkError ule) {
	    availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
	    availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
		setOperationalState(OperationalStateEnum.DISABLED);
	    logger.log(Level.WARNING,
			"Unable to load native runtime library" + ule);
	    throw ule;
	}
    }

    protected void start() throws ClusterRuntimeException {
	logger.fine("Create MBean");
	// Create the virtual MBean interceptor and start it.
	ObjectNameFactory objectNameFactory =
	    new ObjectNameFactory(getDomainName());
	try {
            ZoneClusterGroup zcGroup = new ZoneClusterGroup();
	    logger.fine("register ZoneClusterGroup");
	    getMbs().registerMBean(zcGroup,
		objectNameFactory.getObjectName(ZoneClusterGroupMBean.class,
		null));

	} catch (Exception e) {
	    availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
	    setOperationalState(OperationalStateEnum.DISABLED);
	    logger.log(Level.WARNING, "Unable to start ZoneCluster module" + e);
		throw new ClusterRuntimeException(e.getMessage());
	}
    }

    protected void stop() {
	logger.fine("Stop MBean");

	// Stop the interceptor and dispatcher and release for GC
	try {
            ObjectNameFactory objectNameFactory =
                    new ObjectNameFactory(getDomainName());
	    getMbs().unregisterMBean(
		objectNameFactory.getObjectName(ZoneClusterGroupMBean.class,
		null));

	} catch (Exception e) {
	    availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
	    setOperationalState(OperationalStateEnum.DISABLED);
	    logger.log(Level.WARNING, "Unable to stop ZoneCluster module" + e);
		throw new ClusterRuntimeException(e.getMessage());
	}
    }
}
