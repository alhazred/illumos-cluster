/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ZoneClusterData.java	1.8	08/07/10 SMI"
 */
package com.sun.cluster.agent.zonecluster;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * ZoneClusterData Class
 */
public class ZoneClusterData implements java.io.Serializable {

    private String name;
    private int zoneClusterId;
    private String zonePath;
    private ZoneClusterPSetTab psetTab;
    private ArrayList status;
    private HashMap hostStatusMap;
    private HashMap hostNodeMap;
    private HashMap nodeHostMap;
    private ArrayList nodeNameList;
    private ArrayList hostNameList;
    private ArrayList zoneClusterFSTabList;
    private ArrayList zoneClusterDevTabList;

    public ArrayList getStatus() {
        return status;
    }

    public HashMap getHostStatusMap() {
        return hostStatusMap;
    }

    public ArrayList getHostNameList() {
        return hostNameList;
    }

    public HashMap getHostNodeMap() {
        return hostNodeMap;
    }

    public HashMap getNodeHostMap() {
        return nodeHostMap;
    }

    public String getName() {
        return name;
    }

    public ArrayList getNodeNameList() {
        return nodeNameList;
    }

    public ArrayList getZoneClusterDevTabList() {
        return zoneClusterDevTabList;
    }

    public ArrayList getZoneClusterFSTabList() {
        return zoneClusterFSTabList;
    }

    public int getZoneClusterId() {
        return zoneClusterId;
    }

    public String getZonePath() {
        return zonePath;
    }

    public ZoneClusterPSetTab getPsetTab() {
        return psetTab;
    }


    public ZoneClusterData() {
    }


    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("[name= " + name);
        s.append(", zoneClusterId= " + zoneClusterId);
        s.append(", zonePath= " + zonePath);
        s.append(", hostNameList= " + hostNameList);
        s.append(", nodeNameList= " + nodeNameList);
        s.append(", status= " + status);
        s.append(", zoneClusterFSTabList= " + zoneClusterFSTabList);
        s.append(", zoneClusterDevTabList= " + zoneClusterDevTabList);
        s.append(", hostNodeMap= " + hostNodeMap);
        s.append(", hostStatusMap= " + hostStatusMap);
        s.append(", psetTab= " + psetTab);
        s.append("]");
        return s.toString();
    }
}
