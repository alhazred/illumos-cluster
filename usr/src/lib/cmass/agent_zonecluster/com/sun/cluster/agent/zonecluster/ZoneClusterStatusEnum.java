/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ZoneClusterStatusEnum.java	1.3	08/07/10 SMI"
 */
package com.sun.cluster.agent.zonecluster;

public class ZoneClusterStatusEnum extends com.sun.cacao.Enum
        implements java.io.Serializable {

    public static final ZoneClusterStatusEnum UNKNOWN =
            new ZoneClusterStatusEnum("UNKNOWN", 0);

    public static final ZoneClusterStatusEnum CONFIGURED =
            new ZoneClusterStatusEnum("CONFIGURED");

    public static final ZoneClusterStatusEnum INCOMPLETE =
            new ZoneClusterStatusEnum("INCOMPLETE");

    public static final ZoneClusterStatusEnum INSTALLED =
            new ZoneClusterStatusEnum("INSTALLED");

    public static final ZoneClusterStatusEnum DOWN =
            new ZoneClusterStatusEnum("DOWN");

    public static final ZoneClusterStatusEnum MOUNTED =
            new ZoneClusterStatusEnum("MOUNTED");

    public static final ZoneClusterStatusEnum READY =
            new ZoneClusterStatusEnum("READY");

    public static final ZoneClusterStatusEnum SHUTTING_DOWN =
            new ZoneClusterStatusEnum("SHUTTING_DOWN");

    public static final ZoneClusterStatusEnum RUNNING =
            new ZoneClusterStatusEnum("RUNNING");

    public static final ZoneClusterStatusEnum ONLINE =
            new ZoneClusterStatusEnum("Online");

    public static final ZoneClusterStatusEnum OFFLINE =
            new ZoneClusterStatusEnum("Offline");


    private ZoneClusterStatusEnum(String name) {
        super(name);
    }

    private ZoneClusterStatusEnum(String name, int ord) {
        super(name, ord);
    }
}
