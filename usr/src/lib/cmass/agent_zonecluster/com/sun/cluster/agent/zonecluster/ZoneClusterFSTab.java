/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/**
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ZoneClusterFSTab.java	1.3	08/07/10 SMI"
 */
package com.sun.cluster.agent.zonecluster;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * An instance of this object represent a single file system entry of a
 * zone cluster. Maps one-to-one with the zone_fstab struct defined in
 * libclzonecfg.h.
 * struct zone_fstab {
 *	char            zone_fs_special[MAXPATHLEN];    /* special file
 *	char            zone_fs_dir[MAXPATHLEN];        /* mount point
 *	char            zone_fs_type[FSTYPSZ];          /* e.g. ufs
 *	zone_fsopt_t   *zone_fs_options;                /* mount options
 *	char            zone_fs_raw[MAXPATHLEN];        /* device to fsck
 * };
 *
 */
public class ZoneClusterFSTab implements Serializable {

    private String fsSpecial;
    private String mountPoint;
    private String fsType;
    private ArrayList mountOptions;
    private String devToFsck;

    /** Default empty constructor */
    public ZoneClusterFSTab() {}

    /**
     * Full constructor used by jni
     */
    public ZoneClusterFSTab(
			String fsSpecial,
			String mountPoint,
			String fsType,
			ArrayList mountOptions,
			String devToFsck) {
	this.fsSpecial = fsSpecial;
	this.mountPoint = mountPoint;
	this.fsType = fsType;
	this.mountOptions = mountOptions;
	this.devToFsck = devToFsck;
    }

    /**
     * Get the special file
     * @return special file
     */
    public String getFS() {
    	return this.fsSpecial;
    }

    /**
     * Get the mount point
     * @return mount point
     */
    public String getMountPoint() {
	return this.mountPoint;
    }

    /**
     * Get the fs type
     * @return fs type
     */
    public String getFSType() {
	return this.fsType;
    }

    /**
     * Get the mount options
     * @return mount options
     */
    public ArrayList getMountOptions() {
	return this.mountOptions;
    }

    /**
     * Get device to fsck
     * @return device to fscK
     */
    public String getDevice() {
	return this.devToFsck;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the fs.
     * @param fsSpecial the special file
     */
    public void setFS(String fsSpecial) {
	this.fsSpecial = fsSpecial;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the mount point.
     * @param mountPoint the mount point.
     */
    public void setMountPoint(String mountPoint) {
	this.mountPoint = mountPoint;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the fs type
     * @param fsType the file system type
     */
    public void setFsType(String fsType) {
	this.fsType = fsType;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the mount options
     * @param mountOptions an <code>ArrayList</code> value
     */
    public void setMountOptions(ArrayList mountOptions) {
	this.mountOptions = mountOptions;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the device to fsck.
     * @param devToFsck the device to fsck
     */
    public void setDevice(String devToFsck) {
	this.devToFsck = devToFsck;
    }

    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("[fsSpecial= " + fsSpecial);
        s.append(", mountPoint= " + mountPoint);
        s.append(", fsType= " + fsType);
        s.append(", mountOptions= " + mountOptions);
        s.append(", devToFsck= " + devToFsck);
        s.append("]");
        return s.toString();
    }
}
