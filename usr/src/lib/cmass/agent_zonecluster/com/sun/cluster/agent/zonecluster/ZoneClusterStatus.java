/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ZoneClusterStatus.java	1.2	08/07/10 SMI"
 */
package com.sun.cluster.agent.zonecluster;

/**
 * Struct-like object used to represent a ZoneCluster status on an
 * identified zone hostname.
 */
public class ZoneClusterStatus implements java.io.Serializable {

    private String hostName;
    private ZoneClusterStatusEnum status;

    /**
     * Default empty constructor
     */
    public ZoneClusterStatus() {
    }

    /**
     * Constructor with initial values.
     *
     * @param  hostName zone host name
     * @param  status  a {@link ZoneClusterStatusEnum} element representing
     * the ZoneCluster status on this zone hostname
     */
    public ZoneClusterStatus(String hostName,
        ZoneClusterStatusEnum status) {
        this.hostName = hostName;
        this.status = status;
    }

    /**
     * Get the concerned zone host name.
     *
     * @return  the zone host name.
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String getHostName() {
        return this.hostName;
    }

    /**
     * Get the ZoneCluster status on the concerned node.
     *
     * @return  the ZoneCluster status
     */
    public ZoneClusterStatusEnum getStatus() {
        return this.status;
    }

    /**
     * Set the zone host name.
     *
     * @param  hostName  the reference name of the zone host name.
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * Set the ZoneCluster status.
     *
     * @param  status  a {@link ZoneClusterStatusEnum} element representing
     * the ZoneCluster status
     */
    public void setState(ZoneClusterStatusEnum status) {
        this.status = status;
    }

    public String toString() {
        return hostName + " : " + status.toString();
    }
}
