/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Zone Cluster Group MBean
 */
#pragma ident	"@(#)zonecluster_data.c	1.13	08/08/17 SMI"

#include "com/sun/cluster/agent/zonecluster/ZoneClusterGroup.h"
#include <jniutil.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <sys/processor.h>
#include <procfs.h>
#include <utmpx.h>
#include <dirent.h>
#include <pwd.h>
#include <wchar.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/sol_version.h>
#include <thread.h>
#if SOL_VERSION >= __s10
#include <stdbool.h>
#include <zone.h>
#include <libzccfg/libzccfg.h>
#include <sys/clconf_int.h>
#endif /* SOL_VERSION >= __s10 */


#define	debug if (0)

static mutex_t init_lock;
static int initialized = 0;

static jfieldID nameID;
static jfieldID zoneClusterIdID;
static jfieldID statusID;
static jfieldID zonePathID;
static jfieldID hostNodeMapID;
static jfieldID nodeHostMapID;
static jfieldID hostStatusMapID;
static jfieldID nodeNameListID;
static jfieldID hostNameListID;
static jfieldID zoneClusterFSTabListID;
static jfieldID zoneClusterDevTabListID;
static jfieldID psetTabID;


static jmethodID zoneClusterDataClassConstructor = NULL;
static jmethodID array_list_constructor;
static jmethodID array_list_add;
static jmethodID map_put;
static jmethodID zonecluster_fstab_constructor;
static jmethodID zonecluster_psettab_constructor;
static jmethodID zcstatus_constructor;

static jclass zoneClusterDataClass;
static jclass array_list_class;
static jclass map_class;
static jclass zonecluster_fstab_class;
static jclass zonecluster_psettab_class;
static jclass zcstatus_class;

#define	PUT(MAP, NAME, VALUE)						\
	(*env)->CallObjectMethod(env,					\
				MAP,					\
				map_put,				\
				(*env)->NewStringUTF(env, NAME),	\
				VALUE)


static void init(JNIEnv *env) {
    jclass localClass;
    debug printf("\n BEGIN init \n");
	(void) mutex_lock(&init_lock);

    if (!initialized) {
	/* find ZoneClusterData class */
	localClass = (*env)->FindClass(env,
	    "com/sun/cluster/agent/zonecluster/ZoneClusterData");
	zoneClusterDataClass = (*env)->NewGlobalRef(env, localClass);

	if (zoneClusterDataClass == NULL) {
	    debug printf("\n ZoneClusterData class is null\n");
	    debug printf("\n END init \n");
	    goto unlock;
	}

	/* find ZoneClusterData class constructor */
	zoneClusterDataClassConstructor = (*env)->GetMethodID(env,
		zoneClusterDataClass, "<init>", "()V");

	if (zoneClusterDataClassConstructor == NULL) {
	    debug printf("\n ZoneClusterData Constructor is null\n");
	    debug printf("\n END init \n");
	    goto unlock;
	}

	/* find ZoneClusterData class fields */
	nameID = (*env)->GetFieldID(env, zoneClusterDataClass, "name",
	    "Ljava/lang/String;");
	if (nameID == NULL) {
	    debug printf("\n nameID is null \n");
	    goto unlock;
	}
	psetTabID = (*env)->GetFieldID(env, zoneClusterDataClass, "psetTab",
	    "Lcom/sun/cluster/agent/zonecluster/ZoneClusterPSetTab;");
	if (psetTabID == NULL) {
	    debug printf("\n psetTabID is null \n");
	    goto unlock;
	}

	statusID = (*env)->GetFieldID(env, zoneClusterDataClass,
		"status",
		"Ljava/util/ArrayList;");
	if (statusID == NULL) {
	    debug printf("\n statusID is null \n");
	    goto unlock;
	}
	zoneClusterIdID = (*env)->GetFieldID(env, zoneClusterDataClass,
	    "zoneClusterId", "I");
	if (zoneClusterIdID == NULL) {
	    debug printf("\n zoneClusterIdID is null \n");
	    goto unlock;
	}
	zonePathID = (*env)->GetFieldID(env, zoneClusterDataClass,
	    "zonePath", "Ljava/lang/String;");
	if (zonePathID == NULL) {
	    debug printf("\n zonePathID is null \n");
	    goto unlock;
	}
	hostNodeMapID = (*env)->GetFieldID(env, zoneClusterDataClass,
		"hostNodeMap", "Ljava/util/HashMap;");
	if (hostNodeMapID == NULL) {
	    debug printf("\n hostNodeMapID is null \n");
	    goto unlock;
	}
	hostStatusMapID = (*env)->GetFieldID(env, zoneClusterDataClass,
		"hostStatusMap", "Ljava/util/HashMap;");
	if (hostStatusMapID == NULL) {
	    debug printf("\n hostStatusMapID is null \n");
	    goto unlock;
	}
	nodeHostMapID = (*env)->GetFieldID(env, zoneClusterDataClass,
		"nodeHostMap", "Ljava/util/HashMap;");
	if (nodeHostMapID == NULL) {
	    debug printf("\n nodeHostMapID is null \n");
	    goto unlock;
	}
	nodeNameListID = (*env)->GetFieldID(env, zoneClusterDataClass,
		"nodeNameList", "Ljava/util/ArrayList;");
	if (nodeNameListID == NULL) {
		debug printf("\n nodeNameListID is null \n");
		goto unlock;
	}
	hostNameListID = (*env)->GetFieldID(env, zoneClusterDataClass,
		"hostNameList", "Ljava/util/ArrayList;");
	if (hostNameListID == NULL) {
	    debug printf("\n hostNameListID is null \n");
	    goto unlock;
	}
	zoneClusterFSTabListID = (*env)->GetFieldID(env, zoneClusterDataClass,
		"zoneClusterFSTabList", "Ljava/util/ArrayList;");
	if (zoneClusterFSTabListID == NULL) {
	    debug printf("\n zoneClusterFSTabListID is null \n");
	    goto unlock;
	}
	zoneClusterDevTabListID = (*env)->GetFieldID(env, zoneClusterDataClass,
		"zoneClusterDevTabList", "Ljava/util/ArrayList;");
	if (zoneClusterDevTabListID == NULL) {
	    debug printf("\n zoneClusterDevTabListID is null \n");
	    goto unlock;
	}

	/* Utility classes */

	/* ArrayList */
	localClass = (*env)->FindClass(env, "java/util/ArrayList");
	array_list_class = (*env)->NewGlobalRef(env, localClass);
	if (array_list_class == NULL) {
	    debug printf("\n array list class is null \n");
	    goto unlock;
	}

	array_list_constructor = (*env)->GetMethodID(
		env, array_list_class,
		"<init>", "()V");
	if (array_list_constructor == NULL) {
	    debug printf("\n array list constructor is null \n");
	    goto unlock;
	}
	array_list_add = (*env)->GetMethodID(env, array_list_class,
		"add", "(Ljava/lang/Object;)Z");
	if (array_list_add == NULL) {
	    debug printf("\n array list add is null \n");
	    goto unlock;
	}

	/* HashMap */
	localClass = (*env)->FindClass(env, "java/util/Map");
	map_class = (*env)->NewGlobalRef(env, localClass);
	if (map_class == NULL) {
		goto unlock;
	}

	map_put = (*env)->GetMethodID(env, map_class,
			"put",
			"(Ljava/lang/Object;Ljava/lang/Object;)"
			"Ljava/lang/Object;");
	if (map_put == NULL) {
		goto unlock;
	}

	/* ZoneClusterFStab class */
	localClass = (*env)->FindClass(env,
		"com/sun/cluster/agent/zonecluster/ZoneClusterFSTab");
	zonecluster_fstab_class = (*env)->NewGlobalRef(env, localClass);
	if (zonecluster_fstab_class == NULL) {
	    debug printf("\n zonecluster_fstab_class is null \n");
	    goto unlock;
	}
	zonecluster_fstab_constructor = (*env)->GetMethodID(env,
		zonecluster_fstab_class,
		"<init>",
"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;"
"Ljava/lang/String;)V");

	/* ZoneClusterPSetTab class */
	localClass = (*env)->FindClass(env,
		"com/sun/cluster/agent/zonecluster/ZoneClusterPSetTab");
	zonecluster_psettab_class = (*env)->NewGlobalRef(env, localClass);
	if (zonecluster_psettab_class == NULL) {
	    debug printf("\n zonecluster_psettab_class is null \n");
	    goto unlock;
	}
	zonecluster_psettab_constructor = (*env)->GetMethodID(env,
		zonecluster_psettab_class,
		"<init>",
"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");

	/*
	 * ZoneClusterStatus Class
	 */
	localClass = (*env)->FindClass(env,
	    "com/sun/cluster/agent/zonecluster/ZoneClusterStatus");
	zcstatus_class = (*env)->NewGlobalRef(env, localClass);
	if (zcstatus_class == NULL)
	    goto unlock;
	zcstatus_constructor = (*env)->GetMethodID(env,
	    zcstatus_class,
	    "<init>",
	    "(Ljava/lang/String;Lcom/sun/cluster/agent/zonecluster/"
	    "ZoneClusterStatusEnum;)V");
	if (zcstatus_constructor == NULL)
	    goto unlock;
	initialized = 1;
	}

unlock:
	(void) mutex_unlock(&init_lock);
	debug printf("\n END init \n");
}


/*
 * Class:     com_sun_cluster_agent_zonecluster_ZoneClusterGroup
 * Method:    readDataJNI
 * Signature: ()Ljava/util/HashMap;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_zonecluster_ZoneClusterGroup_readDataJNI
	(JNIEnv * env, jobject this) {

	jobject zc_data_map;

#if SOL_VERSION >= __s10

	char **zone_cluster_arr;
	int zc_count = 0;
	char *zonename = NULL;
	char *status_str = "Unknown";
	int status_int = 0;

	int err = 0;
	char buff[MAXPATHLEN];
	struct zc_nodetab nodetab;
	struct zc_fstab fstab;
	struct zc_devtab devtab;
	struct zc_psettab psettab;
	zone_cluster_state_t zc_state = NULL;
	zc_dochandle_t zone_handle = NULL;
	uint32_t cl_id = 0;

	jobject zcData = NULL;
	jobject zonecluster_fstab_list;
	jobject zonecluster_devtab_list;
	jobject nodename_list;
	jobject hostname_list;
	jobject zc_status_list;
	jobject zc_status;
	jobject hostname_map = NULL;
	jobject nodehost_map = NULL;
	jobject hoststatus_map = NULL;
	jstring ret_string;
	jstring ret_string1;
	jobject zonecluster_fstab_options_array = NULL;
	jobject zonecluster_fstab;
	jobject zonecluster_psettab;

#endif /* SOL_VERSION >= __s10 */
	debug printf(" START readData method \n");
	if (!initialized) {
	    init(env);
	}

	/*
	 * Initialize the map for storing Zone Cluster Data objects
	 */
	zc_data_map = newObjNullParam(env, "java/util/HashMap");
	if (zc_data_map == NULL) {
	    debug printf("\n Zone Cluster HashMap is NULL\n");
	    goto end;
	}

#if SOL_VERSION >= __s10

	/*
	 * Library initialization
	 */

	clconf_lib_init();
	zone_cluster_arr = clconf_cluster_get_cluster_names();
	for (; zone_cluster_arr[zc_count] != NULL; zc_count++);

	/*
	 * Initialize the map for storing Zone Cluster Data objects
	 */
	zc_data_map = newObjNullParam(env, "java/util/HashMap");
	if (zc_data_map == NULL) {
	    debug printf("\n Zone Cluster HashMap is NULL\n");
	    goto error;
	}

	for (zc_count = 0; zone_cluster_arr[zc_count] != NULL; zc_count++) {

		/*
		 * Initialization of ArrayList Objects
		 */
		hostname_list = (*env)->NewObject(env,
						array_list_class,
						array_list_constructor);
		if (hostname_list == NULL) {
			goto error;
		}

		nodename_list = (*env)->NewObject(env,
						array_list_class,
						array_list_constructor);
		if (nodename_list == NULL) {
			goto error;
		}

		zonecluster_devtab_list = (*env)->NewObject(env,
						array_list_class,
						array_list_constructor);

		if (zonecluster_devtab_list == NULL) {
			goto error;
		}

		zonecluster_fstab_options_array = (*env)->NewObject(env,
						array_list_class,
						array_list_constructor);
		if (zonecluster_fstab_options_array == NULL) {
			goto error;
		}

		zonecluster_fstab_list = (*env)->NewObject(env,
						array_list_class,
						array_list_constructor);
		if (zonecluster_fstab_list == NULL) {
			goto error;
		}

		zc_status_list = (*env)->NewObject(env,
						array_list_class,
						array_list_constructor);
		if (zc_status_list == NULL)
			goto error;

		/* zone cluster objects */
		zcData = (*env)->NewObject(env, zoneClusterDataClass,
			zoneClusterDataClassConstructor);
		if (zcData == NULL) {
		    debug printf("\n zoneClusterData Object is NULL\n");
		    goto error;
		}

		zonename = strdup(zone_cluster_arr[zc_count]);
		if (zonename == NULL) {
			goto error;
		}
		setStringField(env, zcData, nameID, zone_cluster_arr[zc_count]);

		/*
		 * Open the handle to zonecfg
		 */
		zone_handle = zccfg_init_handle();
		if (zone_handle == NULL) {
			goto error;
		}
		err = zccfg_get_handle(zonename, zone_handle);
		if (err != ZC_OK) {
			goto error;
		}

		/*
		 * Get the zone path for this zone cluster
		 */
		err = zccfg_get_zonepath(zone_handle, buff, sizeof (buff));
		if (err != ZC_OK) {
			goto error;
		}
		setStringField(env, zcData, zonePathID, (char*)buff);

		/*
		 * Get the node list for this zone cluster
		 */
		err = zccfg_setnodeent(zone_handle);
		if (err != ZC_OK) {
			goto error;
		}

		hostname_map = newObjNullParam(env, "java/util/HashMap");
		if (hostname_map == NULL) {
			goto error;
		}
		hoststatus_map = newObjNullParam(env, "java/util/HashMap");
		if (hoststatus_map == NULL) {
			goto error;
		}
		nodehost_map = newObjNullParam(env, "java/util/HashMap");
		if (nodehost_map == NULL) {
			goto error;
		}

		while (zccfg_getnodeent(zone_handle, &nodetab) == ZC_OK) {
			ret_string = (jstring) newObjStringParam(env,
					"java/lang/String",
					nodetab.zc_nodename);

			/* Update Nodelist */
			if (ret_string != NULL) {
				(void) (*env)->CallObjectMethod(env,
						nodename_list,
						array_list_add,
						ret_string);
			}
			ret_string1 = (jstring) newObjStringParam(env,
					"java/lang/String",
					nodetab.zc_hostname);

			/* Update HostName List */
			if (ret_string1 != NULL) {
				(void) (*env)->CallObjectMethod(env,
						hostname_list,
						array_list_add,
						ret_string1);
			}

			/* Update Host Node Map */
			PUT(hostname_map, nodetab.zc_hostname, ret_string);

			/* Update Node Host Map */
			PUT(nodehost_map, nodetab.zc_nodename, ret_string1);

			/* Get state of cluster zone on this node */
/*
			get_zone_state_on_node(zone_cluster_arr[zc_count],
				clconf_cluster_get_nodeid_by_nodename(
						nodetab.zc_nodename),
				&zc_state);
*/
			get_zone_cluster_state_on_node(
				zone_cluster_arr[zc_count],
				clconf_cluster_get_nodeid_by_nodename(
						nodetab.zc_nodename),
				&status_str);
			if (strcmp(status_str, "Online") == 0) {
			    status_int = 9;
			} else if (strcmp(status_str, "Offline") == 0) {
			    status_int = 10;
			}

			zc_status =
				(*env)->NewObject(env, zcstatus_class,
						zcstatus_constructor,
						(*env)->NewStringUTF(env,
						nodetab.zc_hostname),
				getCMASSEnum(env,
				"com/sun/cluster/agent/zonecluster/"
				"ZoneClusterStatusEnum",
				status_int));
			/*
			 * Add status to the list
			 */
			(*env)->CallObjectMethod(env,
						zc_status_list,
						array_list_add,
						zc_status);
			/* Update Host Status Map */
			PUT(hoststatus_map, nodetab.zc_hostname,
				getCMASSEnum(env,
				"com/sun/cluster/agent/zonecluster/"
				"ZoneClusterStatusEnum",
				status_int));
		}

		(*env)->SetObjectField(
			env, zcData, hostNodeMapID, hostname_map);
		(*env)->SetObjectField(
			env, zcData, hostStatusMapID, hoststatus_map);
		(*env)->SetObjectField(
			env, zcData, nodeHostMapID, nodehost_map);
		(*env)->SetObjectField(
			env, zcData, nodeNameListID, nodename_list);
		(*env)->SetObjectField(
			env, zcData, hostNameListID, hostname_list);
		(*env)->SetObjectField(
			env, zcData, statusID, zc_status_list);

		/*
		 * Get pset info for the zonecluster
		 */
		err = zccfg_getpsetent(zone_handle, &psettab);
		if ((err != ZC_OK) && (err != ZC_NO_ENTRY)) {
			goto error;
		}

		zonecluster_psettab = (*env)->NewObject(env,
				zonecluster_psettab_class,
				zonecluster_psettab_constructor,
				newObjStringParam(env,
					"java/lang/String",
					psettab.zc_ncpu_min),
				newObjStringParam(env,
					"java/lang/String",
					psettab.zc_ncpu_max),
				newObjStringParam(env,
					"java/lang/String",
					psettab.zc_importance));

		(*env)->SetObjectField(env, zcData, psetTabID,
					zonecluster_psettab);

		/*
		 * Get the cluster id for the zone cluster.
		 */

		err = clconf_get_cluster_id(zonename, &cl_id);

		if (err == ENOENT) {
			goto error;
		}

		(*env)->SetIntField(env, zcData, zoneClusterIdID, cl_id);

		/*
		 * Get the dev entry for the zone cluster.
		 */
		err = zccfg_setdevent(zone_handle);
		if (err != ZC_OK) {
			goto error;
		}

		while (zccfg_getdevent(zone_handle, &devtab) == ZC_OK) {
			ret_string = (jstring) newObjStringParam(env,
					"java/lang/String",
					devtab.zc_dev_match);

			if (ret_string != NULL) {
				(void) (*env)->CallObjectMethod(env,
						zonecluster_devtab_list,
						array_list_add,
						ret_string);
			}
		}
		(*env)->SetObjectField(
			env, zcData, zoneClusterDevTabListID,
			zonecluster_devtab_list);

		/*
		 * Get the fstab entry for the zone cluster.
		 */
		err = zccfg_setfsent(zone_handle);
		if (err != ZC_OK) {
			goto error;
		}

		while (zccfg_getfsent(zone_handle, &fstab) == ZC_OK) {

			zc_fsopt_t *fsopt_ptr;

			for (fsopt_ptr = fstab.zc_fs_options;
				fsopt_ptr != NULL;
				fsopt_ptr = fsopt_ptr->zc_fsopt_next) {

				ret_string = (jstring) newObjStringParam(env,
						"java/lang/String",
						fsopt_ptr->zc_fsopt_opt);

				(void) (*env)->CallObjectMethod(env,
						zonecluster_fstab_options_array,
						array_list_add,
						ret_string);
			}

			zonecluster_fstab = (*env)->NewObject(env,
				zonecluster_fstab_class,
				zonecluster_fstab_constructor,
				newObjStringParam(env,
					"java/lang/String",
					fstab.zc_fs_special),
				newObjStringParam(env,
					"java/lang/String",
					fstab.zc_fs_dir),
				newObjStringParam(env,
					"java/lang/String",
					fstab.zc_fs_type),
				zonecluster_fstab_options_array,
				newObjStringParam(env,
					"java/lang/String",
					fstab.zc_fs_raw));

			if (zonecluster_fstab != NULL) {
				(void) (*env)->CallObjectMethod(env,
						zonecluster_fstab_list,
						array_list_add,
						zonecluster_fstab);
			}
		}
		(*env)->SetObjectField(
			env, zcData, zoneClusterFSTabListID,
			zonecluster_fstab_list);

		/* Now put the object into the hashmap */
		PUT(zc_data_map, zone_cluster_arr[zc_count], zcData);

		(*env)->DeleteLocalRef(env, zonecluster_fstab_list);
		(*env)->DeleteLocalRef(env, zonecluster_fstab_options_array);
		(*env)->DeleteLocalRef(env, zonecluster_devtab_list);
/*
		(*env)->DeleteLocalRef(env, zonecluster_fstab);
*/
		(*env)->DeleteLocalRef(env, nodename_list);
		(*env)->DeleteLocalRef(env, hostname_list);
		(*env)->DeleteLocalRef(env, zcData);
		(*env)->DeleteLocalRef(env, hostname_map);
		(*env)->DeleteLocalRef(env, hoststatus_map);
		(*env)->DeleteLocalRef(env, nodehost_map);
		(*env)->DeleteLocalRef(env, ret_string);
		(*env)->DeleteLocalRef(env, ret_string1);
		free(zonename);
	}

	error:
		(void) zccfg_endnodeent(zone_handle);
		(void) zccfg_fini_handle(zone_handle);
		free(zone_cluster_arr);

#endif /* SOL_VERSION >= __s10 */

	end:
	    return (zc_data_map);
}
