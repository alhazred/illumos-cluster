/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DiskPathModule.java 1.9     08/05/20 SMI"
 */

package com.sun.cluster.agent.dpm;

// JDK
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.ObjectName;

// Cacao
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// local
import com.sun.cluster.common.ClusterRuntimeException;


/**
 * Module implementing disk-path monitoring. Since there may be a huge number of
 * disk paths, this module doesn't implement an mbean for each path, but one
 * mbean permitting the client to obtain a table of disk path states and perform
 * operations on disk paths.
 */
public class DiskPathModule extends Module {

    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.dpm");
    private final static String logTag = "DiskPathModule";

    /** internal reference to our mbean object name */
    private ObjectName objectName = null;

    /**
     * Constructor called by the container
     *
     * @param  descriptor  the deployment descriptor used to load the module
     */
    public DiskPathModule(DeploymentDescriptor descriptor) {
        super(descriptor);
        logger.entering(logTag, "<init>", descriptor);

        Map parameters = descriptor.getParameters();

        try {

            // Load the agent JNI library
            String library = (String) parameters.get("library");
            Runtime.getRuntime().load(library);

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to load native runtime library" + ule);
            throw ule;
        }

        logger.exiting(logTag, "<init>");
    }

    /**
     * Invoked by the container when unlocking the module.
     *
     * @exception  ClusterRuntimeException  if unable to start.
     */
    protected void start() throws ClusterRuntimeException {
        logger.entering(logTag, "start");

        try {
            DiskPathMonitorMBean diskPathMonitorMBean = new DiskPathMonitor();

            ObjectNameFactory objectNameFactory = new ObjectNameFactory(
                    getDomainName());

            objectName = objectNameFactory.getObjectName(
                    DiskPathMonitorMBean.class, null);
            getMbs().registerMBean(diskPathMonitorMBean, objectName);

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to start Disk Path module : " +
                e.getMessage());
            throw new ClusterRuntimeException(e.getMessage());

        } finally {
            logger.exiting(logTag, "start");
        }
    }

    /**
     * called to disactivate the module
     */
    protected void stop() {
        logger.entering(logTag, "stop");

        try {

            // unregister ourselves
            if (objectName != null) {
                getMbs().unregisterMBean(objectName);
                objectName = null;
            }
        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to stop Disk Path module : " +
                e.getMessage());
            throw new ClusterRuntimeException(e.getMessage());

        } finally {
            logger.exiting(logTag, "stop");
        }
    }
}
