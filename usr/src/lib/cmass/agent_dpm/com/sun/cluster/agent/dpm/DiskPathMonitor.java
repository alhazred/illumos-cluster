/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DiskPathMonitor.java 1.9     08/05/20 SMI"
 */

package com.sun.cluster.agent.dpm;

// JDK
import java.util.logging.Level;
import java.util.logging.Logger;

// Cacao
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.ClusterPaths;


/**
 * This interface defines the management operations available for Cluster Disk
 * Paths. Since there may be a very large number of these paths that are
 * monitored, we do not instantiate one mbean per path, but manage the paths via
 * tables.
 */
public class DiskPathMonitor implements DiskPathMonitorMBean {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.dpm");

    /**
     * Query the status of the disk paths, providing filters to select which
     * disk paths to return
     *
     * @param  nodeName  a <code>String</code> value (null = all nodes)
     * @param  diskName  a <code>String</code> value (null = all disks)
     * @param  online  a <code>Boolean</code> value  (null = both states)
     * @param  monitored  a <code>Boolean</code> value (null = both states)
     *
     * @return  a key name composed of the node name, colon separator, then the
     * disk path, eg. node:/dev/foo
     */
    public native DiskPath[] queryDiskPaths(String nodeName, String diskName,
        Boolean online, Boolean monitored);

    /**
     * Monitor a given disk path.
     *
     * @param  nodeName  a <code>String</code> value. null means all nodes
     * @param  diskName  a <code>String</code> value. May not be null
     *
     * @return  an <code>ExitStatus[]</code> value
     *
     * @exception  CommandExecutionException  if an error occurs
     */
    public ExitStatus[] monitorDiskPath(String nodeName, String diskName)
        throws CommandExecutionException {

        String cmds[][] = new String[][] {
                {
                    ClusterPaths.CL_DEVICE_CMD, "monitor",
                    ((nodeName == null) ? "" : nodeName) + DiskPath.SEPARATOR +
                    diskName
                }
            };

        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * remove monitor on a disk path
     *
     * @param  nodeName  a <code>String</code> value. null means all nodes.
     * @param  diskName  a <code>String</code> value. May not be null.
     *
     * @return  an <code>ExitStatus[]</code> value
     *
     * @exception  CommandExecutionException  if an error occurs
     */
    public ExitStatus[] unmonitorDiskPath(String nodeName, String diskName)
        throws CommandExecutionException {
        String cmds[][] = new String[][] {
                {
                    ClusterPaths.CL_DEVICE_CMD, "unmonitor",
                    ((nodeName == null) ? "" : nodeName) + DiskPath.SEPARATOR +
                    diskName
                }
            };

        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }
}
