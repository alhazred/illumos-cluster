/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)DiskPath.java 1.6     08/05/20 SMI"
 */

package com.sun.cluster.agent.dpm;

public class DiskPath implements java.io.Serializable {

    public final static String SEPARATOR = ":";

    private String name = null;

    private String diskName = null;

    String nodeName = null;

    private Boolean online = null;

    boolean monitored = false;

    /**
     * Full initialization via Constructor
     */
    public DiskPath(String nodeName, String diskName, boolean isMonitored,
        Boolean isOnline) {
        setName(nodeName + SEPARATOR + diskName);
        setDiskName(diskName);
        setNodeName(nodeName);
        setMonitored(isMonitored);
        setOnline(isOnline);
    }

    /**
     * Default constructor
     */
    public DiskPath() {
    }

    /**
     * Get the value of name.
     *
     * @return  value of name.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name.
     *
     * @param  v  Value to assign to name.
     */
    public void setName(String v) {
        this.name = v;
    }

    /**
     * Get the value of diskName.
     *
     * @return  value of diskName.
     */
    public String getDiskName() {
        return diskName;
    }

    /**
     * Set the value of diskName.
     *
     * @param  v  Value to assign to diskName.
     */
    public void setDiskName(String v) {
        this.diskName = v;
    }

    /**
     * Get the value of nodeName.
     *
     * @return  value of nodeName.
     */
    public String getNodeName() {
        return nodeName;
    }

    /**
     * Set the value of nodeName.
     *
     * @param  v  Value to assign to nodeName.
     */
    public void setNodeName(String v) {
        this.nodeName = v;
    }

    /**
     * Get the value of online. Returns null if value is unknown because
     * monitored is false or transitioning
     *
     * @return  value of online.
     */
    public Boolean isOnline() {
        return online;
    }

    /**
     * Set the value of online.
     *
     * @param  v  Value to assign to online.
     */
    public void setOnline(Boolean v) {
        this.online = v;
    }

    /**
     * Get the value of monitored.
     *
     * @return  value of monitored.
     */
    public boolean isMonitored() {
        return monitored;
    }

    /**
     * Set the value of monitored.
     *
     * @param  v  Value to assign to monitored.
     */
    public void setMonitored(boolean v) {
        this.monitored = v;
    }

    /**
     * for printing this path
     */
    public String toString() {
        return getName();
    }
}
