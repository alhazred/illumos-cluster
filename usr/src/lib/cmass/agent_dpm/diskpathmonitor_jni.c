/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Diskpath mbean
 */

#pragma ident	"@(#)diskpathmonitor_jni.c	1.3	08/05/20 SMI"

#include "com/sun/cluster/agent/dpm/DiskPathMonitor.h"

#include <jniutil.h>
#include <string.h>
#include <stdio.h>
#include <cl_query/cl_query_types.h>

/*
 * Class:     com_sun_cluster_agent_dpm_DiskPathMonitor
 * Method:    queryDiskPaths
 *
 * Signature: (Ljava/lang/String;Ljava/lang/String;
 * Ljava/lang/Boolean;Ljava/lang/Boolean;)[Lcom/sun/cluster/agent/dpm/DiskPath;
 */
JNIEXPORT jobjectArray JNICALL
Java_com_sun_cluster_agent_dpm_DiskPathMonitor_queryDiskPaths
	(JNIEnv * env,
	jobject this,
	jstring jNodeName,
	jstring jDiskName,
	jobject jIsOnline,
	jobject jIsMonitored) {

	jobjectArray return_array = NULL;

	cl_query_error_t query_retval = CL_QUERY_ERROR_MAX;
	cl_query_result_t result;
	cl_query_diskpath_info_t *diskpath = NULL;

	/*
	 * Perform a query on disk paths, with some optional filtering
	 */

	const char *node_name = NULL;
	const char *disk_name = NULL;

	jclass booleanClass;
	jmethodID booleanValueMethod;

	jboolean online;
	jboolean *online_ptr = NULL;
	jboolean monitored;
	jboolean *monitored_ptr = NULL;

	jclass array_list_class = NULL;
	jmethodID array_list_constructor;
	jmethodID array_list_add;
	jmethodID array_list_to_array;
	jobject array_list = NULL;

	jclass disk_path_class = NULL;
	jmethodID disk_path_constructor;
	jobject disk_path = NULL;

	jobjectArray disk_path_array;

	/*
	 * Convert the filters into useable C values
	 */
	if (jNodeName != NULL) {
		node_name = (*env)->GetStringUTFChars(env,
		    jNodeName,
		    NULL);
		if (strlen(node_name) == 0) {
			(*env)->ReleaseStringUTFChars(env,
			    jNodeName, node_name);
			node_name = NULL;
		}
	}
	if (jDiskName != NULL) {
		disk_name = (*env)->GetStringUTFChars(env,
		    jDiskName,
		    NULL);
		if (strlen(disk_name) == 0) {
			(*env)->ReleaseStringUTFChars(env,
			    jDiskName, disk_name);
			disk_name = NULL;
		}
	}
	booleanClass = (*env)->FindClass(env, "java/lang/Boolean");
	booleanValueMethod = (*env)->GetMethodID(env,
	    booleanClass,
	    "booleanValue",
	    "()Z");

	if (jIsOnline != NULL) {
		online = (*env)->CallBooleanMethod(env,
		    jIsOnline,
		    booleanValueMethod);
		online_ptr = &online;
	}
	if (jIsMonitored != NULL) {
		monitored = (*env)->CallBooleanMethod(env,
		    jIsMonitored,
		    booleanValueMethod);
		monitored_ptr = &monitored;
	}
	/*
	 * Set up our ArrayList where we are going to put results
	 */

	/*
	 * Pick up a reference to the ArrayList class
	 */
	array_list_class = (*env)->FindClass(env, "java/util/ArrayList");
	if (array_list_class == NULL)
		goto error;

	/*
	 * Pick up a methodID to the constructor
	 */
	array_list_constructor = (*env)->GetMethodID(env, array_list_class,
	    "<init>", "()V");
	/*
	 * Pick up a methodID to the 'add' method
	 */
	array_list_add = (*env)->GetMethodID(env, array_list_class,
	    "add", "(Ljava/lang/Object;)Z");

	/*
	 * Pick up a methodID to the toArray method
	 */
	array_list_to_array =
	    (*env)->GetMethodID(env,
	    array_list_class,
	    "toArray",
	    "([Ljava/lang/Object;)[Ljava/lang/Object;");

	/*
	 * Create the empty ArrayList
	 */
	array_list = (*env)->NewObject(env, array_list_class,
	    array_list_constructor);
	delRef(env, array_list_class);
	if (array_list == NULL)
		goto error;

	/*
	 * Find the class and constructor for a DiskPath
	 */


	disk_path_class = (*env)->FindClass(env,
	    "com/sun/cluster/agent/dpm/DiskPath");
	if (disk_path_class == NULL)
		goto error;

	/*
	 * Pick up a methodID to the constructor
	 */
	disk_path_constructor =
	    (*env)->GetMethodID(env,
	    disk_path_class,
	    "<init>",
	    "(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Boolean;)V");

	/*
	 * Now call the cl_query API and find matches
	 */

	query_retval = cl_query_get_info(CL_QUERY_DISKPATH_INFO,
	    (char *)node_name,
	    &result);
	if (query_retval != CL_QUERY_OK)
		goto error;

	/*
	 * Go through the returned values adding matches to ArrayList
	 */

	for (diskpath = (cl_query_diskpath_info_t *)result.value_list;
	    diskpath != NULL;
	    diskpath = diskpath->next) {

		int namelen;
		int is_monitored;
		jobject is_online;

		if ((disk_name != NULL) &&
		    (strcmp(disk_name, diskpath->dpath_name) != 0)) {
			continue;
		}
		if (online_ptr != NULL) {
			if (*online_ptr &&
			    !CLQUERY_DISKPATH_IS_OK(diskpath->status)) {
				continue;
			}
			if (!*online_ptr &&
			    CLQUERY_DISKPATH_IS_OK(diskpath->status)) {
				continue;
			}
		}
		if (monitored_ptr != NULL) {
			if (*monitored_ptr &&
			    CLQUERY_DISKPATH_IS_UNMONITORED(diskpath->status)) {
				continue;
			}
			if (!*monitored_ptr &&
			    CLQUERY_DISKPATH_IS_MONITORED(diskpath->status)) {
				continue;
			}
		}
		/*
		 * ok, we have a match - create a new object and add it
		 */

		/*
		 * Remove the trailing 's0' from disk path names
		 */
		namelen = (int)strlen(diskpath->dpath_name);
		if ((namelen > 2) &&
		    (strcmp(&(diskpath->dpath_name[namelen - 2]), "s0") == 0)) {
			diskpath->dpath_name[namelen - 2] = 0;
		}
		is_monitored =
		    CLQUERY_DISKPATH_IS_MONITORED(diskpath->status) ?
		    JNI_TRUE : JNI_FALSE;
		if (is_monitored == JNI_FALSE) {
			is_online = NULL;
		} else if (CLQUERY_DISKPATH_IS_OK(diskpath->status)) {
			is_online = newBoolean(env, 1);
		} else if (CLQUERY_DISKPATH_IS_FAIL(diskpath->status)) {
			is_online = newBoolean(env, 0);
		} else {
			is_online = NULL;
		}

		disk_path = (*env)->NewObject(env,
		    disk_path_class,
		    disk_path_constructor,
		    newObjStringParam(env,
			"java/lang/String",
			diskpath->node_name),
		    newObjStringParam(env,
			"java/lang/String",
			diskpath->dpath_name),
		    is_monitored,
		    is_online);

		(void) (*env)->CallObjectMethod(env,
		    array_list,
		    array_list_add,
		    disk_path);
	}

	/*
	 * Create an empty diskpath array for typing info
	 */
	disk_path_array =
	    (*env)->NewObjectArray(env, 0, disk_path_class, NULL);
	if (disk_path_array == NULL)
		goto error;

	return_array = (*env)->CallObjectMethod(env, array_list,
	    array_list_to_array,
	    disk_path_array);
    error:
	if (node_name != NULL) {
		(*env)->ReleaseStringUTFChars(env, jNodeName, node_name);
	}
	if (disk_name != NULL) {
		(*env)->ReleaseStringUTFChars(env, jDiskName, disk_name);
	}
	if (query_retval == CL_QUERY_OK) {
		(void) cl_query_free_result(CL_QUERY_DISKPATH_INFO, &result);
	}
	return (return_array);

} /*lint !e715 */
