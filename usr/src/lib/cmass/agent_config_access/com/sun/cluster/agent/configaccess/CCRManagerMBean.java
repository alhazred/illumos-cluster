/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)CCRManagerMBean.java 1.15     08/12/03 SMI"
 */

package com.sun.cluster.agent.configaccess;

// JDK
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.Map;


/**
 * This interface defines the CCR Access Utility Mbean
 */
public interface CCRManagerMBean {

    /** Part of the contract with the ObjectNameFactory */
    final public static String TYPE = "CCRManager";

    /**
     * Create a new table. If the "content" is NULL , then an empty table will
     * be created. If the table contents are provided, then table will be
     * populated with provided rows.
     *
     * @param  name
     * @param  content
     *
     * @return  result : true = table created, false = table already exists
     */
    public boolean createTable(String name, Map content) throws IOException;

    /**
     * Test whether a given table already exists in the repository
     *
     * @param  name
     *
     * @return  TRUE if table exists, FALSE otherwise
     */
    public boolean isTableExists(String name) throws IOException;

    /**
     * Delete a given table from the repository
     *
     * @param  name
     *
     * @return  TRUE if table was deleted, FALSE otherwise
     */
    public boolean deleteTable(String name) throws IOException;

    /**
     * List all the tables that already exist and match the requested filter
     *
     * @param  filter
     *
     * @return  array of table names
     */
    public String[] getTableList(String filter);

    /**
     * Read the requested key from the given table
     *
     * @param  name
     * @param  key
     *
     * @return  null (key not found) or the key value
     */
    public String readTableKey(String name, String key) throws IOException;

    /**
     * Read the whole content of a given table
     *
     * @param  name
     *
     * @return  hashtable with the full table content (all keys)
     */
    public Map readTable(String name) throws IOException;

    /**
     * Write the requested key with the given value to the given table
     *
     * @param  name
     * @param  key
     * @param  value
     *
     * @return  null or old key value
     */
    public String writeTableKey(String name, String key, String value)
        throws IOException;

    /**
     * Write the whole content of a given table The previous contents of the
     * table are deleted and the table will be populated with the new contents
     * provided. The Table must be exisiting in the repository.
     *
     * @param  name
     * @param  content in hashtable
     *
     * @return  TRUE if table was written, FALSE otherwise
     */
    public boolean writeTable(String name, Map content) throws IOException;
}
