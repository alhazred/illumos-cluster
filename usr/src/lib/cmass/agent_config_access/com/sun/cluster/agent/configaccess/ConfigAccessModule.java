/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ConfigAccessModule.java 1.15     08/05/20 SMI"
 */

package com.sun.cluster.agent.configaccess;

// JDK
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JDMK
import javax.management.ObjectName;

// Cacao
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.element.AdministrativeStateEnum;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// CMASS
import com.sun.cluster.agent.cluster.Cluster;

// ODYSSEY
import com.sun.cluster.agent.configaccess.CCRManager;
import com.sun.cluster.common.ClusterRuntimeException;


public class ConfigAccessModule extends Module {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.configaccess");

    public static final String DOMAIN_CONFIG_ACCESS =
        "com.sun.cluster.agent.configaccess";

    // parameters coming from the XML descriptor
    private Map parameters;

    private CCRManager CCRManager = null;
    private ObjectName CCRManagerName = null;

    /**
     * Constructor called by the container.
     *
     * @param  descriptor  the deployment descriptor used to load the module
     */
    public ConfigAccessModule(DeploymentDescriptor descriptor) {
        super(descriptor);

        // load parameters from  XML descriptor
        parameters = descriptor.getParameters();

        try {

            // Load the agent JNI library
            String library = (String) parameters.get("library");
            Runtime.getRuntime().load(library);

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to load native runtime library" + ule);
            throw ule;
        }

    }

    /**
     * Invoked by the container when unlocking the module.
     *
     * @exception  ClusterRuntimeException  If unable to start.
     */
    protected void start() throws ClusterRuntimeException {
        logger.fine("Starting Config Access Module");

        try {
            String localClusterName = Cluster.getClusterName();
            logger.fine("ConfigAccess module : starting on cluster " +
                localClusterName);

            // create the CCR manager
            logger.fine("ConfigAccess module : creating Config manager");
            CCRManager = new CCRManager(DOMAIN_CONFIG_ACCESS);

            // register the CCR Manager MBean
            CCRManagerName = CCRManager.createCCRManagerON();

            getMbs().registerMBean(CCRManager, CCRManagerName);

            logger.fine("ConfigAccess module : starting Config manager");
            CCRManager.startManager();

            logger.fine("ConfigAccess module started !!");

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);

            logger.warning("Unable to create/start the CCR Manager : " +
                e.toString());
            throw new ClusterRuntimeException(e.toString());
        }

    }

    /**
     * Invoked by the container when stopping, must release all resources.
     */
    protected void stop() {
        logger.fine("Stopping ConfigAccess module");

        try {
            logger.fine("ConfigAccess module : stopping CCR manager");
            CCRManager.stopManager();

            // unregister and destroy the CCR manager MBean
            getMbs().unregisterMBean(CCRManagerName);
            CCRManager = null;
            CCRManagerName = null;
            parameters = null;

            logger.fine("ConfigAccess module stopped !!");

        } catch (Exception e) {
            logger.warning("Failed to stop/destroy the Config Manager : " +
                e.toString());
        }
    }

    /**
     * Performs a 1-off health-check command
     *
     * @return  boolean value TRUE  means health-check success FALSE means
     * health-check failure If the health-check fails, the module will set its
     * operational state to DISABLED
     */
    public boolean isHealthy() {
        boolean res = true;

        logger.fine("Health check for the ConfigAccess module");

        if ((getAdministrativeState() != AdministrativeStateEnum.UNLOCKED) ||
                (getOperationalState() != OperationalStateEnum.ENABLED)) {

            logger.warning("The module state indicates a failure");
            res = false;
        }

        return res;
    }

}
