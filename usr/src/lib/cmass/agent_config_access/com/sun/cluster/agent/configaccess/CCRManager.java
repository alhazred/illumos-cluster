/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)CCRManager.java 1.20     08/12/03 SMI"
 *
 */

package com.sun.cluster.agent.configaccess;

// JDK
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JDMK
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.NotificationBroadcaster;
import javax.management.NotificationBroadcasterSupport;
import javax.management.ObjectName;
import javax.management.MBeanException;

// CACAO
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.MBeanServerInvocationHandler;

/**
 * This class implements the Generic CCR MBean Manager
 */
public class CCRManager implements CCRManagerMBean {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.configaccess.CCRManager");

    // ObjectNameFactory to use.
    private static ObjectNameFactory onf = null;

    /* CCRManager constructor */
    public CCRManager(String domainName) {
        onf = new ObjectNameFactory(domainName);
    }

    /* Helper function to create the ObjectName for the CCRManager */
    public static ObjectName createCCRManagerON() {
        ObjectName CCRObjectName = null;

        if (onf != null)
            CCRObjectName = onf.getObjectName(CCRManagerMBean.class, null);

        return CCRObjectName;
    }

    /* Helper function to create the CCRManager proxy  */
    public static CCRManagerMBean getCCRManagerProxy(
        MBeanServerConnection mbeanServer) {
        ObjectName CCROn = createCCRManagerON();

        if (CCROn == null) {
            logger.warning("CCR manager ON is null !!");

            return null;
        }

        // test first if the (remote) object exists
        try {

            if (mbeanServer.isRegistered(CCROn) == false) {
                logger.warning("CCRManager not registered");

                return null;
            }

        } catch (Exception e) {
            logger.warning(
                "Error while trying to access mbean for CCRManager " +
                e.toString());

            return null;
        }

        CCRManagerMBean CCR = null;

        CCR = (CCRManagerMBean) MBeanServerInvocationHandler.newProxyInstance(
                mbeanServer, CCROn, CCRManagerMBean.class, false);

        return CCR;
    }

    /* Activate the CCRManager */
    public void startManager() {
        logger.fine("Starting CCR Access Manager on domain " + onf.getDomain());
    }

    /* De-activate the CCRManager */
    public void stopManager() {
        logger.fine("Stopping CCR Access Manager");
    }

    // ===================== mbean interface =================================

    /**
     * Create a new table. If the "content" is NULL , then an empty table will
     * be created. If the table contents are provided, then table will be
     * populated with provided rows.
     *
     * @param  name
     * @param  content
     *
     * @return  result : true = table created, false = table already exists
     */
    public native boolean createTable(String name, Map content)
        throws IOException;

    /**
     * Test whether a given table already exists in the repository
     *
     * @param  name
     *
     * @return  TRUE if table exists, FALSE otherwise
     */
    public native boolean isTableExists(String name) throws IOException;

    /**
     * Delete a given table from the repository
     *
     * @param  name
     *
     * @return  TRUE if table was deleted, FALSE otherwise
     */
    public native boolean deleteTable(String name) throws IOException;

    /**
     * List all the tables that already exist and match the requested filter
     *
     * @param  filter
     *
     * @return  array of table names
     */
    public native String[] getTableList(String filter);

    /**
     * Read the requested key from the given table
     *
     * @param  name
     * @param  key
     *
     * @return  null (key not found) or the key value
     */
    public native String readTableKey(String name, String key)
        throws IOException;

    /**
     * Read the whole content of a given table
     *
     * @param  name
     *
     * @return  hashtable with the full table content (all keys)
     */
    public native Map readTable(String name) throws IOException;

    /**
     * Write the requested key with the given value to the given table
     *
     * @param  name
     * @param  key
     * @param  value
     *
     * @return  null or old key value
     */
    public String writeTableKey(String name, String key, String value)
        throws IOException {
        Map table = readTable(name);

        String old_value = (String) table.put(key, value);

        writeTable(name, table);

        return old_value;
    }

    /**
     * Write the whole content of a given table Table will be overwritten by the
     * new contents. All previous table values will be deleted.
     *
     * @param  name
     * @param  content in hashtable
     *
     * @return  TRUE if table was written, FALSE otherwise
     */
    public native boolean writeTable(String name, Map content)
        throws IOException;

}
