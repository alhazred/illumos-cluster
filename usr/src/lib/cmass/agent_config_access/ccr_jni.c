/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident  "@(#)ccr_jni.c 1.17 08/05/20 SMI"

#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <string.h>
#include <limits.h>

#include <jniutil.h>
#include "com/sun/cluster/agent/configaccess/CCRManager.h"

#include <cl_query/cl_query_types.h>


#define	PUT(MAP, KEY, VALUE)                                    \
	(*env)->CallObjectMethod(env,                           \
			MAP,                                	\
			map_put,                                \
			(*env)->NewStringUTF(env, KEY),    	\
			(*env)->NewStringUTF(env, VALUE))

#define	GET(MAP, KEY)						\
	(*env)->CallObjectMethod(env,				\
			MAP,					\
			map_get,				\
			KEY)



/*
 * Method:  newJNIObject
 * Helper function
 */

jobject
newJNIObject(JNIEnv * env, const char *classname)
{
	jclass	objClass = NULL;
	jmethodID	cid = NULL;
	jobject	result = NULL;


	/* Get local ref to class */
	objClass = (*env)->FindClass(env, classname);
	if (objClass == NULL) {
		return (NULL);
	}

	/* FIX_ME: Hash table creation by default creates 16 entries */
	/* Get constructor */
	cid = (*env)->GetMethodID(env, objClass, "<init>", "()V");
	if (cid != NULL) {
		result = (*env)->NewObject(env, objClass, cid);
	}

	/* Delete local refs */
	(*env)->DeleteLocalRef(env, (void *)objClass);

	/* Return the object */
	return (result);
}



jboolean
CopyHashTable(JNIEnv * env, jobject this, jobject hashTable,
		cl_query_table_t *input)
{
	cl_query_error_t	error = CL_QUERY_OK;
	char	*key = NULL, *value = NULL;
	int	i = 0, index = 0, key_size = 0;

	jobject	keySet = NULL;
	jobject	iterator_obj = NULL;

	jclass	map_class = NULL;
	jclass	iterator_class = NULL;
	jclass	hashset_class = NULL;

	jmethodID	map_iterator = NULL;
	jmethodID	map_get = 0;
	jmethodID	map_next = 0;
	jmethodID	map_hasNext = 0;
	jmethodID	map_keySet = 0;
	jmethodID	map_size = 0;
	jboolean	resFlag = JNI_FALSE;
	jboolean	keyFlag = JNI_FALSE;
	jstring	jkey = NULL, jvalue = NULL;

	/* Get the reference to the Map class */
	map_class = (*env)->FindClass(env, "java/util/Map");
	if (map_class == NULL) {
		return (JNI_FALSE);
	}

	/* Get the reference for the "get" method */
	map_get = (*env)->GetMethodID(env, map_class, "get",
				"(Ljava/lang/Object;)"
				"Ljava/lang/Object;");
	if (map_get == NULL) {
		return (NULL);
	}


	/* Get the reference for the "size" method */
	map_size = (*env)->GetMethodID(env, map_class, "size", "()I");
	if (map_size == NULL) {
		return (JNI_FALSE);
	}

	/* get the reference for the "keySet" method */
	map_keySet = (*env)->GetMethodID(env, map_class, "keySet",
			"()Ljava/util/Set;");
	if (map_keySet == NULL) {
		return (JNI_FALSE);
	}

	/* Create a HashSet to store the set of keys in the table */
	keySet = newJNIObject(env, "java/util/HashSet");
	if (keySet == NULL) {
		return (JNI_FALSE);
	}

	/* Get a reference to the Set class */
	iterator_class = (*env)->FindClass(env, "java/util/Iterator");
	if (iterator_class == NULL) {
		return (JNI_FALSE);
	}

	/* Get a reference to hasNext method */
	map_hasNext = (*env)->GetMethodID(env, iterator_class,
					"hasNext", "()Z");
	if (map_hasNext == NULL) {
		return (JNI_FALSE);
	}

	/* get a reference to the "map_next" method */
	map_next = (*env)->GetMethodID(env, iterator_class, "next",
			"()Ljava/lang/Object;");
	if (map_next == NULL) {
		return (JNI_FALSE);
	}

	/* Get a reference to the Set class */
	hashset_class = (*env)->FindClass(env, "java/util/HashSet");
	if (hashset_class == NULL) {
		return (JNI_FALSE);
	}
	/* Get a reference to the iterator method */
	map_iterator = (*env)->GetMethodID(env, hashset_class, "iterator",
				"()Ljava/util/Iterator;");
	if (map_iterator == NULL) {
		return (JNI_FALSE);
	}

	/* Get the number of keys in the table */
	key_size = (*env)->CallIntMethod(env, hashTable, map_size);

	/* Get the set of keys in the hash table */
	keySet = (*env)->CallObjectMethod(env, hashTable, map_keySet);
	if (keySet == NULL) {
		return (JNI_FALSE);
	}

	iterator_obj = (*env)->CallObjectMethod(env, keySet, map_iterator);
	if (iterator_obj == NULL) {
		return (JNI_FALSE);
	}

	input->n_rows = key_size;
	input->table_rows = (cl_query_table_elem_t **)
			calloc(input->n_rows, sizeof (cl_query_table_elem_t *));

	/* Update the keyFlag to check if there are more elements in the list */
	keyFlag = (*env)->CallBooleanMethod(env, iterator_obj, map_hasNext);

	for (index = 0; keyFlag == JNI_TRUE; index++) {
	    jkey = (*env)->CallObjectMethod(env, iterator_obj, map_next);
	    if (jkey != NULL) {
		input->table_rows[index] = (cl_query_table_elem_t *)
			calloc(1, sizeof (cl_query_table_elem_t));
		key = (char *)(*env)->GetStringUTFChars(env, jkey, NULL);
		input->table_rows[index]->key = strdup(key);

		jvalue = GET(hashTable, jkey);
		if (jvalue != NULL) {
		    value = (char *)(*env)->GetStringUTFChars(env,
						jvalue, NULL);
		    if (value != NULL) {
			input->table_rows[index]->value = strdup(value);
		    } else {
			input->table_rows[index]->value = NULL;
		    }
		}
		(*env)->ReleaseStringUTFChars(env, jkey, key);
		(*env)->ReleaseStringUTFChars(env, jvalue, value);
		key = NULL; value = NULL;
	    }

	    keyFlag = JNI_FALSE;
	    keyFlag = (*env)->CallBooleanMethod(env, iterator_obj, map_hasNext);
	}

	return (JNI_TRUE);
}

void
printErrorException(JNIEnv *env, cl_query_error_t error)
{

	char	buffer[PATH_MAX];

	memset(buffer, 0, PATH_MAX);
	switch (error) {
	case CL_QUERY_ENOMEM:
		sprintf(buffer, "Out of Memory\n");
		break;

	case CL_QUERY_ECLUSTERRECONFIG:
	case CL_QUERY_ERGRECONFIG:
	case CL_QUERY_EOBSOLETE:
	case CL_QUERY_ENOTCLUSTER:
		sprintf(buffer, "Node is not a cluster member\n");
		break;

	case CL_QUERY_ENOTCONFIGURED:
		sprintf(buffer,
		"Table is not present in the config repository\n");
		break;

	case CL_QUERY_EINVAL:
		sprintf(buffer, "Input Parameter is NULL\n");
		break;

	case CL_QUERY_EPERM:
	case CL_QUERY_EAUTH:
		sprintf(buffer, "Permission denied\n");
		break;

	case CL_QUERY_EUNEXPECTED:
		sprintf(buffer, "Unexpected internal error \n");
		break;

	case CL_QUERY_ETIMEDOUT:
	case CL_QUERY_ECOMM:
		sprintf(buffer,
		"Connection to server timedout in platform API\n");
		break;

	case CL_QUERY_EIO:
		sprintf(buffer, "I/O error \n");
		break;

	case CL_QUERY_ENOENT:
		sprintf(buffer, "Table not found in the repository\n");
		break;

	case CL_QUERY_EXISTS:
		sprintf(buffer, "Table exists in the repository\n");
		break;

	case CL_QUERY_EINVALID:
		sprintf(buffer, "Table is invalid in the repository\n");
		break;

	case CL_QUERY_EMODIFIED:
		sprintf(buffer, "Table has been modified\n");
		break;

	case CL_QUERY_ECORRUPT:
		sprintf(buffer, "Config. repository is corrupted\n");
		break;
	}
	throwByName(env, "java/io/IOException", (const char *)&buffer);
}

/*
 * Class:     com_sun_cluster_agent_configaccess_CCRManager
 * Method:    createTable
 * Signature: (Ljava/lang/String;Ljava/util/HashMap;)Z
 */
JNIEXPORT jboolean JNICALL
Java_com_sun_cluster_agent_configaccess_CCRManager_createTable
	(JNIEnv *env, jobject this, jstring tableName, jobject tableContent)
{
	cl_query_error_t	error = CL_QUERY_OK;
	char	*name = NULL;
	jboolean	res = JNI_TRUE;
	int	i = 0;
	cl_query_table_t	input;

	if (tableName == NULL) {
		throwByName(env, "java/io/IOException",
			"Error: Input parameter is NULL");
		return (JNI_FALSE);
	}

	input.n_rows = 0;
	input.table_rows = NULL;

	/* Get the table name in "C" to pass to the API */
	name = (char *)(*env)->GetStringUTFChars(env, tableName, NULL);
	if (name == NULL) {
		throwByName(env, "java/io/IOException",
		"Error: Input parameter is NULL in createTable");
		res = JNI_FALSE;
	}

	if (res == JNI_TRUE) {
		if (tableContent != NULL) {
		    res = CopyHashTable(env, this, tableContent, &input);
		    if (res == JNI_TRUE) {
				error = cl_query_create_table(name, &input);
		    }
		} else {
		    /* Create an empty table */
		    error = cl_query_create_table(name, NULL);
		}
	}

	if (res == JNI_TRUE) {
		/* Check for API failure or empty table */
		if (error != CL_QUERY_OK) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_WARNING,
					"Failed to create table %s: error = %d",
					name, error);
			res = JNI_FALSE;
		}
	}

	(*env)->ReleaseStringUTFChars(env, tableName, name);

	/* Release the buffer contents */
	if (tableContent != NULL) {
		for (i = 0; i < input.n_rows; i++) {
			if (input.table_rows[i]->key != NULL) {
			    free(input.table_rows[i]->key);
			}
			if (input.table_rows[i]->value != NULL) {
			    free(input.table_rows[i]->value);
			}
			if (input.table_rows[i] != NULL) {
			    free(input.table_rows[i]);
			}
		}
		if (input.table_rows != NULL) {
			free(input.table_rows);
		}
	}
	return (res);
}





/*
 * Class:     com_sun_cluster_agent_configaccess_CCRManager
 * Method:    isTableExists
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL
Java_com_sun_cluster_agent_configaccess_CCRManager_isTableExists
	(JNIEnv *env, jobject this, jstring tableName)
{
	jclass	config_class = NULL;
	jmethodID	configId;
	jobjectArray	tableList = NULL;
	jsize	arr_length = 0;
	jobject	tmpString = NULL;
	int	i = 0;
	char	*table_name, *tmp_name;
	jboolean	res = JNI_TRUE;

	if (tableName == NULL) {
		throwByName(env, "java/io/IOException",
		"Error: Input parameter is NULL");
		return (JNI_FALSE);
	}

	/* Get the table name in "C" to pass to the API */
	table_name = (char *)(*env)->GetStringUTFChars(env, tableName,
				NULL);
	if (table_name == NULL) {
		throwByName(env, "java/io/IOException",
			"Error: Input parameter is NULL in isTableExists");
		return (JNI_FALSE);
	}

	if (res == JNI_TRUE) {
		/* Get the reference for the "getTableList" method */
		config_class = (*env)->GetObjectClass(env, this);
		if (config_class == NULL) {
			throwByName(env, "java/io/IOException",
				"Error: Unable to find the config class");
			res = JNI_FALSE;
		}
	}

	if (res == JNI_TRUE) {
		configId = (*env)->GetMethodID(env, config_class,
				"getTableList",
				"(Ljava/lang/String;)"
				"[Ljava/lang/String;");
		if (configId == 0) {
			res = JNI_FALSE;
		}
	}

	if (res == JNI_TRUE) {
		/* Get the list of tables with the table name as the filter */
		tableList = (*env)->CallObjectMethod(env, this, configId,
					tableName);
		if (tableList == NULL) {
			res = JNI_FALSE;
		}
	}

	if (res == JNI_TRUE) {
		arr_length = (*env)->GetArrayLength(env, tableList);
		for (i = 0; i < arr_length; i++) {
			tmpString = (*env)->GetObjectArrayElement(env,
					tableList, i);
			if (tmpString != NULL) {
			    tmp_name = (char *)(*env)->GetStringUTFChars(env,
					tmpString, NULL);
			}
			if (tmp_name != NULL) {
				if (strcmp(tmp_name, table_name) == 0) {
					return (JNI_TRUE);
				}
			}
			(*env)->ReleaseStringUTFChars(env, tmpString, tmp_name);
		}
		res = JNI_FALSE;
	}

	(*env)->ReleaseStringUTFChars(env, tableName, table_name);
	return (res);
}





/*
 * Class:     com_sun_cluster_agent_configaccess_CCRManager
 * Method:    deleteTable
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL
Java_com_sun_cluster_agent_configaccess_CCRManager_deleteTable
	(JNIEnv *env, jobject this, jstring tableName)
{
	jboolean	res = JNI_TRUE;
	cl_query_error_t	error = CL_QUERY_OK;
	const char	*name = NULL;

	if (tableName == NULL) {
		throwByName(env, "java/io/IOException",
		"Error: Input parameter is NULL");
		return (JNI_FALSE);
	}

	/* Get the table name in "C" to pass to the API */
	name = (*env)->GetStringUTFChars(env, tableName, NULL);
	if (name == NULL) {
		throwByName(env, "java/io/IOException",
		"Error: Failed to get the table name from java string");
		return (JNI_FALSE);
	}

	/* Invoke the API to delete the table */
	error = cl_query_delete_table((char *)name);
	if (error != CL_QUERY_OK) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_WARNING, "Failed to delete table %s: error = %d",
			name, error);
		res = JNI_FALSE;
	}

	(*env)->ReleaseStringUTFChars(env, tableName, name);
	return (res);
}




/*
 * Class:     com_sun_cluster_agent_configaccess_CCRManager
 * Method:    getTableList
 * Signature: (Ljava/io/FilenameFilter;)[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL
Java_com_sun_cluster_agent_configaccess_CCRManager_getTableList
	(JNIEnv * env, jobject this, jobject pattern)
{
	cl_query_name_t	search_pattern;
	cl_query_table_list_t	result;
	cl_query_error_t	error = CL_QUERY_OK;
	int	i = 0;

	jobjectArray	nameList = NULL;

	/* Get the search pattern in "C" format */
	if (pattern != NULL) {
		search_pattern = (char *)
				(*env)->GetStringUTFChars(env, pattern, NULL);
	} else {
		search_pattern = NULL;
	}

	/* Invoke the API to get the table list */
	error = cl_query_table_names(search_pattern, &result);
	if (error != CL_QUERY_OK) {
		(*env)->ReleaseStringUTFChars(env, pattern, search_pattern);
		return (NULL);
	}

	if (result.table_count == 0) {
		(*env)->ReleaseStringUTFChars(env, pattern, search_pattern);
		return (NULL);
	}

	/* retrieve the result array */
	nameList = newStringArray(env, result.table_count,
				result.table_names);


	/* free the result structure */
	for (i = 0; i < result.table_count; i++) {
		if (result.table_names[i] != NULL) {
			free(result.table_names[i]);
		}
	}

	(*env)->ReleaseStringUTFChars(env, pattern, search_pattern);
	return (nameList);
}





/*
 * Class:     com_sun_cluster_agent_configaccess_CCRManager
 * Method:    readTableKey
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_com_sun_cluster_agent_configaccess_CCRManager_readTableKey
	(JNIEnv * env, jobject this, jstring tableName, jstring key)
{
	const char	*name = NULL;
	const char	*table_key = NULL;
	jclass	config_class = NULL;
	jmethodID	readTableId;
	jobject	configTable = NULL;
	jstring	res_value = NULL;

	jobject	map_class = NULL;
	jmethodID	map_get;
	jboolean	flag = JNI_FALSE;

	if (tableName == NULL || key == NULL) {
		throwByName(env, "java/io/IOException",
		"Error: Input parameter is NULL");
		return (NULL);
	}

	/* Get the reference to the Map class */
	map_class = (*env)->FindClass(env, "java/util/HashMap");
	if (map_class == NULL) {
		return (NULL);
	}

	/* Get the reference for the "get" method */
	map_get = (*env)->GetMethodID(env, map_class, "get",
				"(Ljava/lang/Object;)"
				"Ljava/lang/Object;");
	if (map_get == NULL) {
		return (NULL);
	}


	/* Get the reference for the "readTable" method */
	config_class = (*env)->GetObjectClass(env, this);
	readTableId = (*env)->GetMethodID(env, config_class, "readTable",
			"(Ljava/lang/String;)"
			"Ljava/util/Map;");
	if (readTableId == 0) {
		return (NULL);
	}

	configTable = (*env)->CallObjectMethod(env, this, readTableId,
					tableName);
	if (configTable == NULL) {
		return (NULL);
	}

	/* Get the value of the "key" in the table */
	res_value = GET(configTable, key);

	return (res_value);
}





/*
 * Class:     com_sun_cluster_agent_configaccess_CCRManager
 * Method:    readTable
 * Signature: (Ljava/lang/String;)Ljava/util/HashMap;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_configaccess_CCRManager_readTable
	(JNIEnv * env, jobject this, jstring tableName)
{
	cl_query_error_t	error = CL_QUERY_OK;
	const char	*name = NULL;
	char	*key = NULL, *value = NULL;
	cl_query_table_t	result;
	int	i = 0;

	jobject	configTable = NULL;
	jclass	map_class = NULL;
	jmethodID	map_put;
	jboolean	res = JNI_TRUE;

	if (tableName == NULL) {
		throwByName(env, "java/io/IOException",
		"Error: Input parameter is NULL");
		return (NULL);
	}

	/* Get the table name in "C" to pass to the API */
	name = (*env)->GetStringUTFChars(env, tableName, NULL);
	if (name == NULL) {
		throwByName(env, "java/io/IOException",
		"Error: Failed to get the table Name");
		return (NULL);
	}


	/* Initialize the output parameter */
	result.n_rows = 0;
	result.table_rows = NULL;

	error = cl_query_read_table((char *)name, &result);
	/* Check for API failure or empty table */
	if (error != CL_QUERY_OK) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_WARNING, "Failed to read the table %s: error = %d",
				name, error);
		res = JNI_FALSE;
	}

	if (res == JNI_TRUE) {
		/* Create the Hashmap to return the CCR Table */
		configTable = newJNIObject(env, "java/util/HashMap");
		if (configTable == NULL) {
			res = JNI_FALSE;
		}
	}

	if (res == JNI_TRUE) {
		/* Get the reference to the Map class */
		map_class = (*env)->FindClass(env, "java/util/HashMap");
		if (map_class == NULL) {
			res = JNI_FALSE;
		}
	}

	if (res == JNI_TRUE) {
		/* Get the reference for the "put" method */
		map_put = (*env)->GetMethodID(env, map_class, "put",
			"(Ljava/lang/Object;Ljava/lang/Object;)"
			"Ljava/lang/Object;");
		if (map_put == NULL) {
			res = JNI_FALSE;
		}
	}


	if (res == JNI_TRUE) {
		/* Get the table contents and populate the hash Table */
		for (i = 0; i < result.n_rows; i++) {
			key = result.table_rows[i]->key;
			value = result.table_rows[i]->value;

			/* Note: Value can be null */
			if (key != NULL) {
				PUT(configTable, key, value);
			}
		}
	}

	(*env)->ReleaseStringUTFChars(env, tableName, name);
	cl_query_free_table(&result);
	if (res == JNI_TRUE) {
		return (configTable);
	} else {
		return (NULL);
	}
}






/*
 * Class:     com_sun_cluster_agent_configaccess_CCRManager
 * Method:    writeTable
 * Signature: (Ljava/lang/String;Ljava/util/HashMap;)Z
 */
JNIEXPORT jboolean JNICALL
Java_com_sun_cluster_agent_configaccess_CCRManager_writeTable
	(JNIEnv * env, jobject this, jstring tableName,
			jobject tableContent)
{
	cl_query_error_t	error = CL_QUERY_OK;
	char	*name = NULL;
	cl_query_table_t	input;
	jboolean	result = JNI_FALSE;
	int	i = 0;

	if (tableName == NULL || tableContent == NULL) {
		throwByName(env, "java/io/IOException",
		"Error: Input parameter is NULL");
		return (NULL);
	}

	result = CopyHashTable(env, this, tableContent, &input);
	if (result == JNI_TRUE) {
		/* Get the table name in "C" to pass to the API */
		name = (char *)(*env)->GetStringUTFChars(env, tableName, NULL);
		if (name == NULL) {
			throwByName(env, "java/io/IOException",
			"Error: Input parameter is NULL in writeTable");
			result = JNI_FALSE;
		}
	}

	if (result == JNI_TRUE) {
		error = cl_query_write_table_all(name, &input);
		/* Check for API failure or empty table */
		if (error != CL_QUERY_OK) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_WARNING,
					"Failed to modify table %s: error = %d",
					name, error);
			result = JNI_FALSE;
		}
	}

	if (result == JNI_TRUE) {
		for (i = 0; i < input.n_rows; i++) {
			if (input.table_rows[i]->key != NULL) {
				free(input.table_rows[i]->key);
			}

			if (input.table_rows[i]->value != NULL) {
				free(input.table_rows[i]->value);
			}
		}
	}
	(*env)->ReleaseStringUTFChars(env, tableName, name);
	return (result);
}
