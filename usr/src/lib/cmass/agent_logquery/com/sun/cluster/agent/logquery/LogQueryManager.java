/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)LogQueryManager.java 1.11     08/12/03 SMI"
 */


package com.sun.cluster.agent.logquery;

// JDK
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.ObjectName;
import javax.management.Query;
import javax.management.QueryExp;
import javax.management.StringValueExp;
import javax.management.AttributeValueExp;

// cacao
import com.sun.cacao.ObjectNameFactory;

// local
import com.sun.cluster.common.ClusterRuntimeException;


/**
 * This class implements the interface LogQueryMBeanManagerMBean. This class is
 * mainly for creating and registering the various MBeans. The mbeans are
 * created based upon the parameters mentioned in the properties file. This
 * MBean is a manager mbean which will manage the starting , registration and
 * creation of the individual mbeans. There will be one MBean per logfile.
 */
public class LogQueryManager extends com.sun.cacao.element.ElementSupport
    implements LogQueryManagerMBean {

    /** Utility for tracing */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.logquery");

    /** Unique identifier of this mbean */
    public static String TYPE = "logquerymbeanManager";

    /** internal reference */
    private javax.management.MBeanServer mbeanServer;

    /**
     * The properties file object which tells the various mbeans to be created
     */
    private java.util.Map properties;

    /** The object name JMX related */
    private javax.management.ObjectName managerObjName;


    // Constructors

    /**
     * Default constructor.
     *
     * @param  mbs  The MBean server
     * @param  properties  The properties file as given by the module
     *
     * @throws  IllegalArgumentException  In case any argument is not as
     * expected
     */
    public LogQueryManager(javax.management.MBeanServer mbs,
        java.util.Map properties) throws IllegalArgumentException {
        super(null);

        try {
            this.mbeanServer = mbs;

            this.properties = properties;

        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);
            throw new IllegalArgumentException("illegal class type " +
                "for constructor: " + e.getMessage());
        }

        try {
            ObjectNameFactory logONF = new ObjectNameFactory(
                    "com.sun.cluster.agent.logquery");
            managerObjName = logONF.getObjectName(LogQueryManagerMBean.class,
                    null);
            mbs.registerMBean(this, managerObjName);
        } catch (Exception e) {
            logger.log(Level.WARNING,
                " caught exception during registration of " + this.toString(),
                e);
            throw new ClusterRuntimeException(e.getMessage());
        }

    }


    /**
     * Method invoked by the framework in which the various mbeans are created
     * and registered.
     */
    public void start() {

        LogQuery logQuery = null;

        try {

            if (this.properties != null) {

                for (int i = 1; i < this.properties.size(); i++) {

                    try {

                        if ((properties.get("logquery.filename." + i) !=
                                    null) &&
                                (properties.get("logquery.classname." + i) !=
                                    null) &&
                                (properties.get("logquery.chunk." + i) !=
                                    null)) {

                            // Now since the properties are as expecetd, just
                            // create
                            // the logQuery object using introspection in this
                            // method
                            // Of the logQuery object after creation is not
                            // null then
                            // else the message will get logged about the
                            // exception.

                            logQuery = createMBean((String) properties.get(
                                        ("logquery.filename." + i)),
                                    (String) properties.get(
                                        "logquery.classname." + i),
                                    new Integer(
                                        (String) properties.get(
                                            "logquery.chunk." + i)));
                            registerMBean(logQuery);
                        }

                    } catch (Exception e) {
                        logger.log(Level.WARNING,
                            "Exception while creating the logquery object for property : " +
                            "logquery.filename." + i, e);
                    }
                }
            }
        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);
            throw new ClusterRuntimeException(e.getMessage());
        }

    }


    /**
     * Public API for implementing the MBean interface. Will take three
     * paramters. One is the logfile to read from. The next is the classname
     * implementing the reading of this given filename While the third is the
     * object specific chunkSize to read from the logfile. This method will
     * return create the MBean of type LogQuery which can then be registered
     * using registerMbean API.
     *
     * @return  LogQuery - The properly created MBean with the given paramters
     * as its attributes.
     *
     * @param  fileName The name of the logfile to be parsed.
     * @param  className The implementation classname.
     * @param  chunkSize The amount of logfile in bytes to read in a single
     * read operation.
     */

    public LogQuery createMBean(String fileName, String className,
        Integer chunkSize) {

        LogQuery logQuery = null;

        try {
            logQuery = (LogQuery)
                ((Class.forName(className)).getConstructor(
                        new Class[] {
                            java.lang.String.class, java.lang.String.class,
                            java.lang.Integer.class
                        })).newInstance(
                    new Object[] { fileName, className, chunkSize });

            return logQuery;

        } catch (Exception e) {
            logger.log(Level.WARNING,
                "Could not create the MBean with the parameters : " +
                "File name : " + fileName + "class name : " + className +
                "Chunk size : " + chunkSize, e);

            return null;
        }
    }


    /**
     * During the stop this method gets invoked by the JMX framework. The
     * various mbeans are destroyed and unregistered in this method
     */
    public void stop() {

        ObjectName objName = null;

        try {
            // Get the various mbean according to the properties file
            // and unregistering according to this unique instance name

            ObjectNameFactory logONF = new ObjectNameFactory(
                    "com.sun.cluster.agent.logquery");
            objName = logONF.getObjectNamePattern(LogQueryMBean.class);

            Set mbeanSet = mbeanServer.queryNames(objName,
                    Query.match(new AttributeValueExp("TYPE"),
                        Query.value("logquery")));

            if (mbeanSet.isEmpty() == false) {
                Iterator it = mbeanSet.iterator();

                while (it.hasNext()) {
                    objName = (ObjectName) it.next();
                    mbeanServer.unregisterMBean(objName);
                }
            }
        } catch (Exception e) {
            logger.log(Level.WARNING,
                "caught exception while unregistering the individual MBeans of the various logfile",
                e);
            throw new ClusterRuntimeException(e.getMessage());
        }

        objName = null;

        try {

            // now unregister the manager
            if (mbeanServer.isRegistered(managerObjName) == true) {
                mbeanServer.unregisterMBean(managerObjName);
            }

            // Nullify the manager object here
            managerObjName = null;

        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);
            throw new ClusterRuntimeException(e.getMessage());
        }
    }


    /**
     * Implements the LogQueryMBeanManagerMBean interface. The method registers
     * the various mbeans.
     *
     * @param  logQuery  The MBean to be registered.
     */
    public void registerMBean(LogQuery logQuery) {

        ObjectName objName = null;

        try {

            ObjectNameFactory logONF = new ObjectNameFactory(
                    "com.sun.cluster.agent.logquery");
            objName = logONF.getObjectName(LogQuery.class,
                    logQuery.getBaseFilename());

            // Now register the various MBeans
            logger.fine("Registering the mbean with instance name as : " +
                logQuery.getBaseFilename());
            mbeanServer.registerMBean(logQuery, objName);
        } catch (javax.management.InstanceAlreadyExistsException e) {
            // expected race condition between initialization and notifs
        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);
            throw new ClusterRuntimeException("unexpected exception " +
                "instantiating mbean:" + e.getMessage());
        }
    }
}
