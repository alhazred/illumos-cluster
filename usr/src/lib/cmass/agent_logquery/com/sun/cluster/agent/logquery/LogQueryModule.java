/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)LogQueryModule.java 1.8     08/05/20 SMI"
 */


package com.sun.cluster.agent.logquery;

// JDK
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// Cacao
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// CMASS
import com.sun.cluster.common.ClusterRuntimeException;


/**
 * This is the agent level module class. This module will in turn initialize the
 * LogQueryMBeanManager object. This manager object will then read the
 * properties file passed by the LogQueryModule. Depending upon the varoius
 * parameters in this properties the Manager MBean will create various LogQuery
 * mbeans with ONE MBean per logfile.
 */
public class LogQueryModule extends Module {

    /** Useful utility for tracing */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.logquery");
    private final static String logTag = "LogQueryModule";

    /** Class level global object for the LogQueryMBeanManager mbean */
    com.sun.cluster.agent.logquery.LogQueryManager globalLogQueryMBeanManager =
        null;

    Map properties;

    /**
     * Constructor called by the container
     *
     * @param  descriptor  the deployment descriptor used to load the module
     */
    public LogQueryModule(DeploymentDescriptor descriptor) {
        super(descriptor);
        logger.entering(logTag, "<init>", descriptor);
        properties = descriptor.getParameters();

        try {
            globalLogQueryMBeanManager = new LogQueryManager(getMbs(),
                    properties);

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to load native runtime library" + ule);
            throw ule;
        }

        logger.exiting(logTag, "<init>");
    }

    protected void start()
        throws com.sun.cluster.common.ClusterRuntimeException {

        try {
            globalLogQueryMBeanManager.start();
        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to start LogQuery module : " +
                e.getMessage());
            throw new ClusterRuntimeException(e.getMessage());

        } finally {
            logger.exiting(logTag, "start");
        }

    }

    protected void stop() {

        globalLogQueryMBeanManager.stop();
    }
}
