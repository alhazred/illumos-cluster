/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SysLogSeverityEnum.java 1.8     08/05/20 SMI"
 */
// The package

package com.sun.cluster.agent.logquery.syslogquery;


import com.sun.cacao.Enum;

import java.io.Serializable;


/**
 * This class extends the Enum class and gets to the specifics for defining the
 * severity values for the Syslog messages. Details are as below of the various
 * levels of severities and their values. The same follows the syslog.h file of
 * Solaris. This class will extend the existing Enum class under
 * com.sun.cluster.common The various syslog severity levels are : EMERG A panic
 * condition.  This is normally broadcast to  all users. ALERT A condition that
 * should be corrected immediately, such as a corrupted system database. CRIT
 * Critical conditions, such as hard device errors. ERROR Errors. WARNING
 * Warning messages. NOTICE Conditions that are not error conditions, but that
 * may require special handling. INFO Informational messages. DEBUG Messages
 * that contain information normally of use only when debugging a program.
 */

public class SysLogSeverityEnum extends com.sun.cacao.Enum
    implements Serializable {


    /** EMERG A panic condition.  This is normally broadcast to  all users. */

    public static final SysLogSeverityEnum EMERG = new SysLogSeverityEnum(
            "emerg", 0);


    /**
     * The severity below the emergency is ALERT according to the syslog. A
     * condition that should be corrected immediately, such as a corrupted
     * system database.
     */


    public static final SysLogSeverityEnum ALERT = new SysLogSeverityEnum(
            "alert", 1);


    /** CRIT Critical conditions, such as hard device errors. */

    public static final SysLogSeverityEnum CRIT = new SysLogSeverityEnum("crit",
            2);


    /** ERROR Errors. */


    public static final SysLogSeverityEnum ERROR = new SysLogSeverityEnum(
            "error", 3);

    /** WARNING Warning messages. */

    public static final SysLogSeverityEnum WARNING = new SysLogSeverityEnum(
            "warning", 4);


    /**
     * NOTICE Conditions that are not error conditions, but that may require
     * special handling.
     */


    public static final SysLogSeverityEnum NOTICE = new SysLogSeverityEnum(
            "notice", 5);

    /** INFO Informational messages. */

    public static final SysLogSeverityEnum INFO = new SysLogSeverityEnum("info",
            6);

    /**
     * DEBUG Messages that contain information normally of use only when
     * debugging a program.
     */

    public static final SysLogSeverityEnum DEBUG = new SysLogSeverityEnum(
            "debug", 7);


    protected SysLogSeverityEnum(String name) {

        super(name);
    }


    protected SysLogSeverityEnum(String name, int ordinal) {

        super(name, ordinal);
    }


}
