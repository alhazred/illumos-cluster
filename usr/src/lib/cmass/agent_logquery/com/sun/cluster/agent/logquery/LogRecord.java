/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)LogRecord.java 1.8     08/05/20 SMI"
 */
//
// The package this class belongs to

package com.sun.cluster.agent.logquery;

import com.sun.cacao.Enum;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * This class is the structure having the various log messages. This class has
 * the attributes as per the logmessages. For known attributes it has the
 * individual references like the severity, messageText, messageID etc. For any
 * unknown attributes ( which may very well be applicable for cluster logs) it
 * provies a Map reference which can be used to store any key value values.
 */
public class LogRecord implements Serializable {


    /** The messageID. The same is shown in the syslog UI as Sequence ID. */
    private String messageID;

    /** Time of the log message. This doesn't have the calendar year. */
    private java.util.Date timestamp;

    /** The host where the message occured. */
    private String hostName;

    /** The String identifying the source which logged this message. */
    private String source;

    /** The message itself */
    private String messageText;

    /** The severity of the message. */
    private com.sun.cacao.Enum severity;

    /**
     * The internal reference for any key value pairs like the facility in the
     * case of the syslogd.
     */
    private Map attributesMap;

    // Constructors

    /**
     * Default constructor
     */
    public LogRecord() {

        super();
        attributesMap = new HashMap();

    }


    // Methods

    /**
     * Setter and getter for the message ID.
     *
     * @param  messageID  The message ID
     */
    public void setMessageID(String messageID) {

        this.messageID = messageID;

    }


    /**
     * The getter for the message ID
     *
     * @return  String  Returns the message ID
     */
    public String getMessageID() {


        return this.messageID;

    }


    /**
     * Sets the date
     *
     * @param  msgDate  The date this message was logged
     */
    public void setDate(Date msgDate) {

        this.timestamp = msgDate;

    }


    /**
     * Gets the date of the message.
     *
     * @return  Date Return the date this message was logged
     */
    public Date getDate() {


        return this.timestamp;

    }


    /**
     * Setter for the source
     *
     * @param  strSource  The source
     */
    public void setSource(String strSource) {

        this.source = strSource;

    }


    /**
     * Getter for the source
     *
     * @return  String Gets the source / utility which logged this message
     */
    public String getSource() {

        return this.source;

    }


    /**
     * Setter for the message text
     *
     * @param  message  Message text
     */
    public void setMessageText(String message) {
        this.messageText = message;
    }


    /**
     * Getter for the message text
     *
     * @return  String Returns the message text
     */
    public String getMessageText() {

        return this.messageText;

    }


    /**
     * Setter for severity based on the Enum value
     *
     * @param  severe  Severity = severe
     */
    public void setSeverity(Enum severe) {

        this.severity = severe;

    }


    /**
     * Getter for severity
     *
     * @return  Enum Returns the severity of the message
     */
    public Enum getSeverity() {

        return this.severity;

    }


    /**
     * Setter for the hostname where this message was logged
     *
     * @param  strHost  The hostname
     */
    public void setHostName(String strHost) {

        this.hostName = strHost;

    }


    /**
     * Getter for the hostname
     *
     * @return  String Returns the hostname
     */
    public String getHostName() {
        return this.hostName;
    }


    /**
     * Method to set attributes with key=value pairs in the attributesMap for
     * this logRecord. For example, in case of the syslog's message, the
     * facility like daemon.error will get stored as facility=daemon.error
     *
     * @param  key  a <code>String</code> value
     * @param  value  a <code>String</code> value
     */
    public void setAttribute(String key, String value) {

        attributesMap.put(key, value);
    }


    /**
     * Method to get the given value from the Map of this records attributes.
     * For example to get the facility pertaining to the syslog's log record, a
     * call to this method with the key equal to "facility" will be made.
     *
     * @param  key  a <code>String</code> value
     *
     * @return  String value - The value of the "key" as mentioned by the
     * parameter.
     */
    public String getAttribute(String key) {

        return (String) this.attributesMap.get(key);
    }


    /**
     * Method to get the Map of this logRecord for further processing as
     * required.
     *
     * @return  java.util.Map - The attributesMap table of this log record.
     */

    public Map getMap() {
        return this.attributesMap;

    }

    /**
     * Utility for displaying
     *
     * @return  String A String - tized representation of this logrecord.
     */
    public String toString() {

        String strTemp = new String();
        strTemp = "LOG DATE : " + getDate().toString() + "\n";
        strTemp = strTemp + "LOG HOST : " + getHostName() + "\n";
        strTemp = strTemp + "MESSAGE SEVERITY : " + getSeverity().toString() +
            "\n";
        strTemp = strTemp + "MESSAGE ID : " + getMessageID() + "\n";
        strTemp = strTemp + "MESSAGE ATTRIBUTES : " + getMap().toString() +
            "\n";
        strTemp = strTemp + "MESSAGE LOGGED BY : " + getSource() + "\n";
        strTemp = strTemp + "MESSAGE TEXT : " + getMessageText() + "\n";

        return strTemp;

    }


}
