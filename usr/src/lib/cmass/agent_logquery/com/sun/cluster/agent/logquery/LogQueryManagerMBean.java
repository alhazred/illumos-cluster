/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)LogQueryManagerMBean.java 1.8     08/12/03 SMI"
 */


package com.sun.cluster.agent.logquery;

import java.io.Serializable;

import java.util.Properties;
import java.util.Set;


/**
 * Interface to be implemented by the log query-mbeans manager. Has only one API
 * which creates and registers the mbeans
 */
public interface LogQueryManagerMBean {


    /** Unique identifier */
    public static String TYPE = "logquerymbeanManager";


    /**
     * Create and register the mbean from the parameter. The paramter will be
     * the logfile name itself.
     *
     * @param  logQuery  LogQuery which needs to be registered as a managed
     * object in this agent.
     */
    public void registerMBean(LogQuery logQuery);


    /**
     * Create the MBean from the given paramters. The MBean object will get
     * created using introspection. The parameters will denote logfile to be
     * parsed, the class implementing the parsing intelligence for this logfile.
     * The third parameter will be the amount of bytes to read from the file in
     * a single read operation. This is kept configurable
     *
     * @return  LogQuery - The MBean created from the given parameters.
     *
     * @param  fileName The logfile to be poarsed.
     * @param  className The class implementing the intelligence to
     * understand this logfile.
     * @param  chunkSize The amount of logfile in bytes o read in a single read
     * operation.
     */


    public LogQuery createMBean(String fileName, String className,
        Integer chunkSize);

}
