/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)LogQuery.java 1.10     08/12/03 SMI"
 */

package com.sun.cluster.agent.logquery;


// JDK
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.Serializable;

import java.util.Date;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

// Cacao
import com.sun.cacao.Enum;


/**
 * This class implements the LogQueryMBean interface. This is a generic class
 * which can be used by all the logfile specific implementation This class will
 * provide generic APIs to open read the logfiles. The logfiles will be read
 * from end to start, in chunks. The read of each logfile will be based on a
 * parameter in the prop file. The caller will invoke a query on this interface,
 * and the LogQuery class will decide at runtime the implementation to invoke.
 * The impl again is a configurable parameter in the prop file. The logfile name
 * is the another parameter which is present and configurable in the prop file.
 */
public abstract class LogQuery implements LogQueryMBean, Serializable {

    /** Utility for logging the messages in the cluster specific logfile */
    protected static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.logquery");


    /** The TYPE of MBean for unique identification */
    public static final String TYPE = "logquery";


    /**
     * This aatribute will be an internal handshake between the caller and the
     * agent module. Depending upon this paramter the module will return the
     * records. The figure should not be very low to avoid repetitive
     * communication between the caller and agent. From the other side it should
     * not be too high thereby degrading the performance.
     */
    private int recordCount;


    /**
     * This attribute denotes the implementation class to invoke for the given
     * logfile.
     */
    String subClassname;


    /** The severity based on which to filter the messages. */
    private Enum severity;


    /**
     * The date from which <B>and including</B> the messages are to be looked
     * at.
     */
    private java.util.Date startDate;


    /** The date till which <B>inclusive</B> the messages are to be parsed */
    private java.util.Date endDate;


    /**
     * Is set to true it means display messages which match the given severity
     * <B>exactly</B>. If set to false, then return messages equal to and above
     * the given severity.
     */
    private boolean isSeverityMatchExact;


    /**
     * If this attribute is set to true, then return messages which do not match
     * the given string. If set to false then the filter for inverse is not
     * required.
     */
    private boolean isStringMatchInverted;

    /**
     * This attribute will store the root filename and will not change for the
     * given MBean.
     */


    private String baseFileName;


    /** The substring filter to match the messages against. */
    private String stringToMatch;


    /**
     * The array of messages which will be returned to the caller based on the
     * set of filters. Each element will be a LogRecord object.
     */
    private LogRecord logRecordArray[];

    /** internal reference */
    private long offset;


    /** Internal reference */
    RandomAccessFile randomAccessFile = null;

    /** internal reference */
    boolean isLogFileOver = false;

    /**
     * Depending upon the value in the properties file initialize this variable
     * to denote the amount of logfile in bytes to read at once.
     */

    int chunk = 8192;

    // buffer for file chunks we read
    private byte charArray[] = new byte[chunk];


    // Constructors

    /**
     * Default constructor
     */
    public LogQuery() {

    }


    // Parameterized constructor

    /**
     * Constructor which will take two parameters. One for The name of the
     * logfile and the other the implementation classname which can read this
     * logfile. Based on these parameters set the appropriate vlaues in this
     * object.
     *
     * @param  fileName  the base name of the logfile.
     * @param  className The name of the class implementing reading of
     * this logfile.
     * @param  chunkSize The chunk size in bytes. The single read from the
     * logfile will be of that many bytes.
     */

    public LogQuery(String fileName, String className, Integer chunkSize) {

        super();
        this.baseFileName = fileName;

        try {
            initializeLogFile(fileName);
            setLogQueryClassName(className);
        } catch (Exception e) {
            logger.log(Level.WARNING, "Caught exception during init", e);
        }

        setLogQueryChunk(chunkSize.intValue());
    }


    /**
     * Will return the implementation class name as a string. Will usually have
     * the fully qualified name of the implementation class.
     *
     * @return  subClassName - the name of the implementation class
     */
    public String getLogQueryClassName() {

        return subClassname;

    }


    /**
     * Set the implementation classname for this MBean.
     *
     * @param  className  The name of the implementation class
     *
     * @throws  NullPointerException  in case the classname was not initialized
     * correctly.
     */
    private void setLogQueryClassName(String classname)
        throws NullPointerException {

        this.subClassname = classname;

        if (this.subClassname == null) {
            throw new NullPointerException(
                "Could not set the implementation classname as it is null");
        }
    }

    /**
     * Utility function to open and initialize the reader of the logfile. Will
     * use the RandomAccessFile class of Java.
     *
     * @throws  FileNotFoundException  - If the random access file object is not
     * initialized correctly, this exception gets thrown.
     */
    public void initializeLogFile(String logFile) throws FileNotFoundException {

        offset = 0;
        randomAccessFile = null;

        try {

            closeFileStream();
            randomAccessFile = new RandomAccessFile(logFile, "r");
            offset = randomAccessFile.length();

        } catch (Exception e) {
            logger.log(Level.FINE,
                "Exception occurred while creating the RandomAccessFile", e);
            throw new FileNotFoundException("Could not find the file : " +
                logFile + ":" + e.getMessage());
        }

        isLogFileOver = false;

    }

    /**
     * This method will close the open random acess file.
     */

    public void closeFileStream() {

        try {

            if (randomAccessFile != null) {
                randomAccessFile.close();
                randomAccessFile = null;
            }
        } catch (Exception e) {
            // Silently
        }


    }

    /**
     * Will use the reader initialized and the book marks. Depending upon the
     * two this api will return the chunk of the logfile. The data to be read at
     * a time will be based upon a parameter in the property file.
     *
     * @return  String The String which is the chunk as read from the logfile.
     */
    public String getLogFileChunk() {

        String strReturn = null;

        try {

            long start = (offset - chunk);

            if (start < 0) {
                start = 0;
                isLogFileOver = true;
            }

            int length = (int) (offset - start);

            randomAccessFile.seek(start);

            randomAccessFile.read(charArray, 0, length);

            strReturn = new String(charArray, 0, length);

            if (isLogFileOver) {

                // Since this file is done just close the stream.
                closeFileStream();
            }

            if (start != 0) {
                int newlineIndex = strReturn.indexOf("\n");

                offset = start + newlineIndex;

                strReturn = strReturn.substring(newlineIndex + 1);
            }

            return strReturn;

        } catch (IOException ioe) {
            logger.log(Level.FINE,
                "IO Exception occured while reading the log file ", ioe);
            closeFileStream();

            return "";
        } catch (Exception e) {
            logger.log(Level.WARNING, "Exception while reading the logfile", e);
            closeFileStream();

            return "";
        }
    }


    /**
     * The interface exposed to query for messages depending upon the given
     * filtering criteria. Each criteria will be looked at and the messages
     * which pass the whole list of these filters would get returned to the
     * caller.
     *
     * @param  recordCount  The number of records max to be returned.
     * @param  sinceWhen  Inclusive date from which point the messages are to be
     * filtered
     * @param  tillWhen  The inclusive date till which the messages are to be
     * returned
     * @param  severity  The severity based on which the messages are to be
     * filtered inclusive.
     * @param  isSeverityMatchExact  This flag is for the severity filter. This
     * if set then messages belonging ONLY to the given severity are to be
     * returned. If set to false then the messages having severity equal to and
     * above the given severity is to be returned.
     * @param  isStringMatchInverted  This flag is for the string to match. If
     * set to true then show only messages which DO NOT match the given string.
     * @param  strMatch  The String which is to be matched in the whole
     * message. This includes the whole record and not just the message text
     * part of the record.
     *
     * @return  com.sun.cluster.agent.logquery.LogRecord Array of log records.
     * Each element in this array matches the given filtering criteria.
     */


    public abstract LogRecord[] queryLog(int recordCount,
        java.util.Date sinceWhen, java.util.Date tillWhen,
        com.sun.cacao.Enum severity, boolean isSeverityMatchExact,
        boolean isStringMatchInverted, String strMatch);


    /**
     * Get the number of records.
     *
     * @return  int The number of records to be returned.
     */
    public int getRecordCount() {

        return this.recordCount;

    }


    /**
     * Set for the number of records
     *
     * @param  count  The number of records.
     */
    public void setRecordCount(int count) {

        this.recordCount = count;

    }


    /**
     * Get the start date
     *
     * @return  java.util.Date  The start date ( inclusive )
     */
    public Date getStartDate() {

        return this.startDate;

    }


    /**
     * Set the start date
     *
     * @param  sDate  The start date
     */
    public void setStartDate(Date sDate) {

        this.startDate = sDate;

    }


    /**
     * Get the date till which ( inclusive ) the messages are to be returned
     *
     * @return  java.util.Date  the end date till which ( inclusive ) the
     * messages are to be returned
     */
    public Date getEndDate() {

        return this.endDate;
    }


    /**
     * Set for the end date
     *
     * @param  eDate  The end Date.
     */
    public void setEndDate(java.util.Date eDate) {

        this.endDate = eDate;
    }


    /**
     * Returns the severity of the filter.
     *
     * @return  com.sun.cluster.agent.common.Enum The severity based upon which
     * the messages are to be filtered
     */
    public Enum getSeverity() {

        return this.severity;
    }


    /**
     * Set the severity
     *
     * @param  severe  The severity based upon which to filter the records.
     */
    public void setSeverity(Enum severe) {

        this.severity = severe;
    }


    /**
     * Get the inverse matching flag
     *
     * @return  boolean Will return the inverse matching flag
     */
    public boolean isStringMatchInverted() {

        return this.isStringMatchInverted;
    }


    /**
     * Set the string matching flag
     *
     * @param  value  Set this to true to get records <B>not matching</B> the
     * given string
     */
    public void setStringMatchInverted(boolean value) {

        this.isStringMatchInverted = value;
    }


    /**
     * Get for severity flag
     *
     * @return  boolean Get the severity flag
     */
    public boolean isSeverityMatchExact() {

        return this.isSeverityMatchExact;
    }


    /**
     * Set the severity flag
     *
     * @param  value  Set the severity match flag to value
     */
    public void setSeverityMatchExact(boolean value) {

        this.isSeverityMatchExact = value;
    }


    /**
     * Tells whether the end of file is reached. If the end of file is reached,
     * this method will increment the file counter. It will then check if the
     * counter is less than 7. If yes, then it will return false to denote that
     * there is still space for rolling the logfile.
     *
     * @return  boolean Will return true if end of file (s ) is reached.
     */
    public boolean isLogFileFinished() {

        return isLogFileOver;
    }

    /**
     * Get the string to match
     *
     * @return  String  Will return the string which is to be looked for in the
     * messages.
     */
    public String getStringToMatch() {

        return this.stringToMatch;
    }


    /**
     * Set the string to match variable
     *
     * @param  strMatch  Set the stringToMatch to this value
     */
    public void setStringToMatch(String strMatch) {

        stringToMatch = strMatch;
    }

    /**
     * Will return the root filename. For example /var/adm/messages for the
     * syslog logfiles.
     *
     * @return  String The root file name
     */
    public String getBaseFilename() {

        // Get the variable value from the prop file.
        return baseFileName;
    }


    /**
     * Set the chunk to read at a time.
     *
     * @param  chunk  the integer denoting the file length to read in bytes.
     */

    private void setLogQueryChunk(int chunkValue) {

        this.chunk = chunkValue;
    }

}
