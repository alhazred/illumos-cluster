/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)LogQueryMBean.java 1.9     08/12/03 SMI"
 */
// -----------------
// Package Name
// -----------------


package com.sun.cluster.agent.logquery;


/**
 * This is the MBean interface which the SPM will query on for the required log
 * records. This interface will expose the APIs which can be invoked with the
 * requisite filters set to get the LogRecord array. Each element within this
 * array will denote a log message in the logfile. The caller will pass in all
 * the filters as paramters to this interface The implementation of this
 * interface by LogQuery will be generic to apply across various types of
 * logfiles. The LogQueryMBean will be based on one MBean per logfile. The
 * various logfiles and their implementation will be configurable based on the
 * properties file. The properties file will have the mbean instance name, the
 * logfile name and the implementation which understands this type of logfile.
 * The type here denotes the specific implementation class.
 */
public interface LogQueryMBean {


    /** The unique identifier of this mbean */
    public static final String TYPE = "logquery";


    /**
     * API exposed to query on. Will return the LogRecord array matching the
     * given filtering criteria.
     *
     * @param  recordCount Total number of records to be returned
     * @param  sinceWhen Inclusive date from which messages are
     * to be returned
     * @param  tillWhen Inclusive date till when the messages
     * are to be returned
     * @param  severity Severity to be matched in the mesages
     * @param  isSeverityMatchExact Severity based flag denoting
     * whether to return messages exactly matching the severity given in the
     * parameters.
     * @param  isStringMatchInverted Flag for string matching. If set
     * to true then return records NOT matching the string expression given.
     * @param  stringToMatch String to look for or ignore in the
     * messages.
     *
     * @return  com.sun.cluster.agent.logquery.LogRecord Array equal to the
     * number of records which is returned based on the various filtering
     * criteria given
     */
    public LogRecord[] queryLog(int recordCount, java.util.Date sinceWhen,
        java.util.Date tillWhen, com.sun.cacao.Enum severity,
        boolean isSeverityMatchExact, boolean isStringMatchInverted,
        String stringToMatch);

}
