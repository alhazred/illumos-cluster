/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SysLogQueryImpl.java 1.9     08/12/03 SMI"
 */


package com.sun.cluster.agent.logquery.syslogquery;

// JDK

// Agent

import com.sun.cacao.Enum;

import com.sun.cluster.agent.logquery.LogQuery;
import com.sun.cluster.agent.logquery.LogRecord;

import java.io.File;
import java.io.Serializable;

import java.text.DateFormat;
import java.text.DateFormat.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Syslog specific implementation. This classname will be present in the
 * configurable parameters in the properties file. This implementation will read
 * and understand the logfiles of the syslog daemon. The same assumes that the
 * syslog follows the standard format of the messages as given by the SOE.
 * Depending upon the paramters passed to this class by its super class (
 * LogQuery ) the API wil return an array of the LogRecord objects matching the
 * filters.
 */
public class SysLogQueryImpl extends LogQuery implements Serializable {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat(
            "MMM dd HH:mm:ss");


    // Constructors

    /**
     * Default constructor
     */
    public SysLogQueryImpl() {
        super();
    }


    public SysLogQueryImpl(String baseName, String implementationName,
        Integer chunkSize) {

        // Call the super for the construction. Pass along these parameters
        // that we have for this object.


        super(baseName, implementationName, chunkSize);

    }


    /**
     * Standard API exposed by the MBean interface, which by this class goes
     * down to the level of the syslog specifics
     *
     * @param  recordCount  Total number of records
     * @param  sinceWhen  Inclusive starting date
     * @param  tillWhen  Inclusive end date
     * @param  severityFilter  Inclusive and higher severity filter
     * @param  isSeverityMatchExact  Severity based flag to tell whether records <B>
     * exactly equal</B> to the given severity as desired
     * @param  isStringMatchInverted  String matching based flag which tells if the
     * string given should NOT be found in the messages
     * @param  stringToMatch  Expression to search for in the messages.
     *
     * @return  LogRecord[] Array of LogRecords matching the filters given
     */
    public LogRecord[] queryLog(int recordCount, java.util.Date sinceWhen,
        java.util.Date tillWhen, com.sun.cacao.Enum severityFilter,
        boolean isSeverityMatchExact, boolean isStringMatchInverted,
        String stringToMatch) {

        // Calendar object for extrapolating the year for the records.

        Calendar previousCalendar = new GregorianCalendar();
        previousCalendar.setTime(new Date());

        // Initialization of file count and the flag

        int fileCounter = -1;
        boolean isFinalFile = false;
        String appendText = null;

        ArrayList resultList = new ArrayList(recordCount);

        String continuationString = null;

        // boolean value set to true when we must exit search
        boolean searchFinished = false;

        while (!isFinalFile && !searchFinished) {

            if (fileCounter < 0) {

                try {
                    initializeLogFile(getBaseFilename());
                    fileCounter = 0;
                } catch (Exception e) {
                    logger.log(Level.WARNING,
                        "Exception in setting the base filename. ", e);

                    return null;
                }
            } else {
                // If we have reached here , it means the required
                // records are not present. So we need to REOLL over
                // to the next logfile.  Continue the same loop with this
                // new rolled over incremented logfile.

                String strNew = getBaseFilename() + "." + fileCounter;

                try {
                    initializeLogFile(strNew);
                    fileCounter = fileCounter + 1;
                } catch (Exception e) {
                    logger.log(Level.FINE, "Could not find file : " + strNew,
                        e);
                    isFinalFile = true;
                }

            }

            while (!isLogFileFinished() && !searchFinished) {

                LinkedList stringRecords = new LinkedList();

                String strTemp = getLogFileChunk();

                StringTokenizer tokenizer = new StringTokenizer(strTemp, "\n");

                try {

                    while (tokenizer.hasMoreElements()) {
                        String strTemp1 = tokenizer.nextToken();

                        stringRecords.addFirst(strTemp1);
                    }
                } catch (Exception e) {
                    // can ignore safely
                }

                LogRecord logRecord = null;

                Iterator i = stringRecords.iterator();

                while (i.hasNext()) {

                    String stringRecord = (String) i.next();

                    try {
                        logRecord = constructLogRecord(stringRecord,
                                previousCalendar);

                        if (logRecord.getSource() == null) {

                            // This is a continuation record
                            if (continuationString == null) {
                                continuationString = "\n- " +
                                    logRecord.getMessageText();
                            } else {
                                continuationString = "\n- " +
                                    logRecord.getMessageText() +
                                    continuationString;
                            }

                            continue;
                        } else {

                            // Append any continuation strings
                            if (continuationString != null) {
                                logRecord.setMessageText(logRecord
                                    .getMessageText() + continuationString);
                            }

                            continuationString = null;
                        }

                    } catch (Exception e) {
                        logger.log(Level.FINE,
                            "Unable to parse log record : " + stringRecord, e);

                        continue;
                    }

                    // Process the string match only if specified
                    if (stringToMatch != null) {

                        if (logRecord.getMessageText().indexOf(stringToMatch) !=
                                -1) {

                            if (isStringMatchInverted) {
                                continue;
                            }
                        } else if (!isStringMatchInverted) {
                            continue;
                        }
                    }

                    // Process the severity only if it is specified.
                    if (severityFilter != null) {
                        Enum severity = logRecord.getSeverity();

                        if (severity != null) {

                            if (isSeverityMatchExact) {

                                if (!severity.equals(severityFilter)) {
                                    continue;
                                }
                            } else {

                                if (severity.compareTo(severityFilter) > 0) {
                                    continue;
                                }
                            }
                        } else {

                            /*
                             * syslog message contains no severity (it will be
                             * better if there was one)
                             */
                            if (logRecord.getSource().equals("syslogd")) {
                                continue;
                            }
                        }
                    }

                    // Process the starting date only if it is specified.

                    if (sinceWhen != null) {

                        if (logRecord.getDate().compareTo(sinceWhen) < 0) {

                            // If this record is too old, all other records
                            // will be too

                            searchFinished = true;

                            break;
                        }
                    }

                    // Process the ending date only if it is specified.

                    if (tillWhen != null) {

                        if (logRecord.getDate().compareTo(tillWhen) > 0) {
                            continue;
                        }
                    }


                    // If the record still remains after all the
                    // processing above, it means the record is to be
                    // returned.

                    resultList.add(logRecord);

                    if (resultList.size() >= recordCount) {
                        searchFinished = true;

                        break;
                    }
                }
            }
        }

        closeFileStream();

        return (LogRecord[]) resultList.toArray(new LogRecord[] {});
    }

    /**
     * This method accepts a String containing a log record and return a log
     * record object.
     *
     * <p>Nov  2 04:20:00 t1fac05 cl_eventlogd[2779]: [ID 848580 daemon.error]
     * Restarting on signal 1. Date hostname source sequenceID facility
     * messageText
     *
     * @param  stringRecord  the string formatted
     * @param  previousCalendar  reference to previously parsed Date in Calendar
     * format, previousCalendar is updated by the call to constructLogRecord
     *
     * @return  LogRecord a valid LogRecord
     *
     * @throws  ParseException  In case there is no correct token in the
     * tokenizer
     */
    public LogRecord constructLogRecord(String stringRecord,
        Calendar previousCalendar) throws ParseException {

        LogRecord logRecord = null;

        LogRecord logRecElement = new LogRecord();
        logRecElement.setSeverity(null);

        StringTokenizer tokenizer = new StringTokenizer(stringRecord, " ",
                false);

        // 1. The first to come in the format is the date. The
        // date in the first three tokens of the message.

        String strTok = tokenizer.nextToken() + " ";
        strTok = strTok + tokenizer.nextToken() + " ";
        strTok = strTok + tokenizer.nextToken() + " ";

        // These three tokens make up the date. So
        // create a date out of this and assign this
        // date to the logrecord object

        Date dateTemp = new Date();

        try {
            dateTemp = dateFormat.parse(strTok);

            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setLenient(false);
            calendar.setTime(dateTemp);

            if (calendar.get(Calendar.MONTH) >
                    previousCalendar.get(Calendar.MONTH)) {

                calendar.set(Calendar.YEAR,
                    previousCalendar.get(Calendar.YEAR) - 1);
            } else {
                calendar.set(Calendar.YEAR,
                    previousCalendar.get(Calendar.YEAR));
            }

            previousCalendar.setTime(calendar.getTime());
            logRecElement.setDate(calendar.getTime());
        } catch (ParseException pe) {
            logger.fine("Could not parse the syslog message's date");
            throw pe;
        }


        // 2. The next token tells the hostname.

        strTok = tokenizer.nextToken();

        logRecElement.setHostName(strTok);


        // 3. The next token is either a source, ending with a colon,
        // or a continuation line from the previous record...
        // "last message repeated N times" or a plain text string
        // - we set up a log record with source 'null'

        // XXX some sources have spaces in them, such as Lockhart... we
        // cannot easily deal with these, and it is not good practice on
        // their part.

        strTok = tokenizer.nextToken();

        if (!strTok.endsWith(":")) {

            if (tokenizer.hasMoreTokens()) {
                strTok = strTok + " " + tokenizer.nextToken("\n");
            }

            logRecElement.setMessageText(strTok);

            return logRecElement;
        }

        strTok = strTok.substring(0, strTok.length() - 1);
        logRecElement.setSource(strTok);

        // 4. Discard this token as it just mentions [ID
        //

        strTok = tokenizer.nextToken();

        boolean isIdPresent = false;

        if (strTok.equals("[ID")) {
            isIdPresent = true;
            // 5. This next token will contain the message ID

            strTok = tokenizer.nextToken();

            logRecElement.setMessageID(strTok);

            // 6. The next token has the severity and the
            // facility. Break both into pieces and initialize
            // accordingly.

            strTok = tokenizer.nextToken();

            StringTokenizer strTok1 = new StringTokenizer(strTok, ".", false);
            // This second level sub token's first part if the facility
            // set the same.

            logRecElement.setAttribute("facility", strTok1.nextToken());

            String strSev = (String) strTok1.nextElement();

            if (strSev.endsWith("]")) {
                strSev = strSev.substring(0, strSev.length() - 1);
            }

            try {
                logRecElement.setSeverity(SysLogSeverityEnum.getEnum(
                        SysLogSeverityEnum.class, strSev));
            } catch (IllegalArgumentException e) {
                logger.log(Level.FINE, "Exception parsing log severity", e);
            }
        }

        // 7. The rest of the remaining stuff is the message body. Just run
        // it through a quick substring statement to collect till end of the
        // string and
        // append the contents to the single string. Once accomplished
        // initialize the messageText attribute of this log record object

        if (isIdPresent) {
            logRecElement.setMessageText(tokenizer.nextToken("\n"));
        } else {
            logRecElement.setMessageText(strTok + " " +
                tokenizer.nextToken("\n"));
        }

        return logRecElement;

    }


}
