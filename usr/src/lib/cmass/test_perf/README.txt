#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2002-2004 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)README.txt	1.2	08/05/20 SMI"
#


This test must be run on a target lab machine from the current working
directory

It tests:

   connection times using different connection mechanism

	      trusted - mutual authentication (eg. agent-agent)
	      untrusted - user/password authentication (eg. spm-agent)
	      unsecure - raw JMX connection (cannot ship in product!)

   for each of these connections it measures

      time to construct a list of proxy mbeans for all node mbeans
      
      time to read the node name of the first of those proxy mbeans

You might want to play with the runtest.sh script and change the
properties given to the JVM at startup time (no jit compilation,
profiling, ...)

------------------------------------------------------------------------
------------------------------------------------------------------------

Typical output on a T1 with agent running with debug and with the
class files loaded from the workspace via NFS


root : t1fac21 37:$ sh runtest.sh
Cold client (nothing preloaded)
Starting test with trusted : Fri Aug 08 17:40:50 MEST 2003
**************** First signon time(ms) : 4537
**************** Subsequent signons (ms) : 219
**************** Obj Query time(ms) : 100
**************** Attr read (ms) : 0
**************** attr reads per second: 5417

Warm client (2nd run)
Starting test with trusted : Fri Aug 08 17:41:07 MEST 2003
**************** First signon time(ms) : 224
**************** Subsequent signons (ms) : 212
**************** Obj Query time(ms) : 92
**************** Attr read (ms) : 0
**************** attr reads per second: 6784

Cold client (nothing preloaded)
Starting test with untrusted : Fri Aug 08 17:41:19 MEST 2003
**************** First signon time(ms) : 4535
**************** Subsequent signons (ms) : 261
**************** Obj Query time(ms) : 97
**************** Attr read (ms) : 0
**************** attr reads per second: 5285

Warm client (2nd run)
Starting test with untrusted : Fri Aug 08 17:41:35 MEST 2003
**************** First signon time(ms) : 285
**************** Subsequent signons (ms) : 417
**************** Obj Query time(ms) : 90
**************** Attr read (ms) : 0
**************** attr reads per second: 6377

Cold client (nothing preloaded)
Starting test with unsecure : Fri Aug 08 17:41:47 MEST 2003
**************** First signon time(ms) : 2228
**************** Subsequent signons (ms) : 68
**************** Obj Query time(ms) : 55
**************** Attr read (ms) : 0
**************** attr reads per second: 10438

Warm client (2nd run)
Starting test with unsecure : Fri Aug 08 17:41:56 MEST 2003
**************** First signon time(ms) : 68
**************** Subsequent signons (ms) : 27
**************** Obj Query time(ms) : 51
**************** Attr read (ms) : 0
**************** attr reads per second: 22421

