/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *  @(#)Perf.java 1.11   08/05/20 SMI
 */

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.JmxClient;
import com.sun.cacao.container.Container;

// Agent
import com.sun.cluster.agent.node.NodeMBean;

import java.io.IOException;

import java.util.*;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;

// JMX
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;


public class Perf {

    static private MBeanServerConnection mbsc = null;

    public static void main(String args[]) {

        if (args.length != 4) {
            System.err.println(
                "Usage: Perf [hostname] [user] [password] [test]");
            System.exit(1);
        }

        System.out.println("Cold client (nothing preloaded)");
        doTest(args);
        System.out.println("Warm client (2nd run)");
        doTest(args);

        System.exit(0);
    }

    public static void doTest(String args[]) {

        try {

            String agentLoc = args[0];
            String userName = args[1];
            String userPass = args[2];
            String profile = args[3];

            System.setProperty(Container.CACAO_PROPERTIES,
                "/etc/opt/SUNWcacao/cacao.properties");

            Date start = new Date();
            System.err.println("Starting test with " + profile + " : " + start);

            int SIGNON_COUNT = 5;
            Date firstSignonDone = null;

            for (int i = 0; i < SIGNON_COUNT; i++) {

                if (profile.equals("trusted")) {
                    mbsc = JmxClient.getWellknownJmxClientConnection(agentLoc,
                            null, null).getMBeanServerConnection();
                } else if (profile.equals("untrusted")) {
                    mbsc = JmxClient.getUnknownJmxClientConnection(userName,
                            userPass, null, null, agentLoc, null)
                        .getMBeanServerConnection();
                } else if (profile.equals("jmxmp")) {

                    try {
                        JMXServiceURL jmxu = new JMXServiceURL("jmxmp",
                                agentLoc, 10162);
                        HashMap env = new HashMap();
                        JMXConnector jmxc = JMXConnectorFactory.connect(jmxu,
                                env);
                        mbsc = jmxc.getMBeanServerConnection();
                    } catch (java.net.MalformedURLException e) {

                        // TODO - deal with this better
                        e.printStackTrace();
                    } catch (java.io.IOException e) {
                        System.out.println("unable to contact external " +
                            "simple mbean server\n");
                    }
                } else if (profile.equals("rmi")) {

                    try {
                        HashMap env = new HashMap();
                        JMXServiceURL url = new JMXServiceURL(
                                "service:jmx:rmi:" + "///jndi/rmi://" +
                                "localhost" + ":8081/jmxserver");
                        JMXConnector jmxc = JMXConnectorFactory.connect(url,
                                env);
                        mbsc = jmxc.getMBeanServerConnection();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    throw new Exception("Unrecognised connection type");
                }

                if (i == 0)
                    firstSignonDone = new Date();

            }

            Date startLookup = new Date();

            int QUERY_COUNT = 100;
            List proxyList = null;

            for (int i = 0; i < QUERY_COUNT; i++) {
                ObjectNameFactory onf = new ObjectNameFactory(NodeMBean.class
                        .getPackage().getName());
                ObjectName pattern = onf.getObjectNamePattern(NodeMBean.class);

                proxyList = JmxClient.getMBeanProxies(mbsc, onf,
                        NodeMBean.class, false, null);
            }

            int listEntries = proxyList.size();

            NodeMBean mbean = (NodeMBean) proxyList.get(0);
            Date startAttrGets = new Date();
            int ATTR_GET_COUNT = 5000;

            for (int i = 0; i < ATTR_GET_COUNT; i++) {
                String retval = mbean.getName();
            }

            Date stopAttrGets = new Date();

            displayTime("First signon time", start, firstSignonDone, 1);
            displayTime("Subsequent signons ", firstSignonDone, startLookup,
                SIGNON_COUNT - 1);
            displayTime("Obj Query time", startLookup, startAttrGets,
                QUERY_COUNT);
            displayTime("Attr read ", startAttrGets, stopAttrGets,
                ATTR_GET_COUNT);

            long duration = (stopAttrGets.getTime() - startAttrGets.getTime());
            System.err.println("**************** attr reads per second: " +
                ((1000 * ATTR_GET_COUNT) / duration));
            System.err.println("");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void displayTime(String msg, Date start, Date end,
        int divisor) {
        System.err.println("**************** " + msg + "(ms) : " +
            ((end.getTime() - start.getTime()) / divisor));
    }
}
