/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)JunctionMBean.java 1.6     08/05/20 SMI"
 */
package com.sun.cluster.agent.transport;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available on a remote Cluster
 * junction.
 */
public interface JunctionMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "junction";

    /**
     * Get the junction's key name.
     *
     * @return  junction's key name.
     */
    public String getName();

    /**
     * Get junction's ID.
     *
     * @return  junction's ID number.
     */
    public int getID();

    /**
     * Give the "enable" status of the junction.
     *
     * @return  <code>true</code> if the junction is enabled, <code>false</code>
     * if it's disabled.
     */
    public boolean isEnabled();

    /**
     * Get the juction's type. Matching the C API, this method currently returns
     * a <code>String</code> representation of the type; should we have an
     * exhaustive list of supported types and then replace this string by a
     * defined <code>Enum</code> object ?
     *
     * @return  the junction's type (example : switch).
     */
    public String getType();

    /**
     * Get the ports linked to this junction.
     *
     * @return  an array of {@link Port} objects.
     */
    public Port[] getPorts();

    /**
     * Disable the junction.
     *
     * @return  an {@link ExitStatus} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] disable() throws CommandExecutionException;

    /**
     * Enable the junction.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] enable() throws CommandExecutionException;
}
