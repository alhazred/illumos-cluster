/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ClusterAdapterMBean.java 1.7     08/05/20 SMI"
 */
package com.sun.cluster.agent.transport;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available on a Node's
 * adapter.
 */
public interface ClusterAdapterMBean extends AdapterMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "clusteradapter";

    /**
     * Get adapter's ID.
     *
     * @return  adapter's ID number.
     */
    public int getID();

    /**
     * Give the configuration state of the adapter.
     *
     * @return  <code>true</code> if the adapter is enabled, <code>false</code>
     * if it's disabled.
     */
    public boolean isEnabled();

    /**
     * Get the ports linked to this adapter.
     *
     * @return  an array of {@link Port} objects.
     */
    public Port[] getPorts();

    /**
     * Disable the adapter.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] disable() throws CommandExecutionException;

    /**
     * Enable the adapter.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] enable() throws CommandExecutionException;

    /**
     * Change one or several private attributes of the adapter.
     *
     * @param  newAttributes  an <code>HashMap <code>containing the key/value
     * pairs to be applied to the adapter. keys and values must be of type
     * <code>String</code>.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeAttributes(java.util.HashMap newAttributes)
        throws CommandExecutionException;
}
