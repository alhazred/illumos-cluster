/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ZoneAdapterMBean.java 1.4     08/05/21 SMI"
 */
package com.sun.cluster.agent.transport;

/**
 * This interface defines the management operation available on a Zone's
 * adapter.
 */
public interface ZoneAdapterMBean {

    /**
     * This field is used internally by the CMAS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "zoneadapter";

    public AdapterData[] readData(String nodeKey, boolean allAddresses);
}
