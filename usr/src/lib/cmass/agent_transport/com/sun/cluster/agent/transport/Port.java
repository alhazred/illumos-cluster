/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)Port.java 1.7     08/05/20 SMI"
 */
package com.sun.cluster.agent.transport;

import java.io.Serializable;


/**
 * An instance of this class describes a port linked to an adapter or a
 * junction.
 *
 * @see  AdapterMBean
 * @see  JunctionMBean
 */
public class Port implements Serializable {

    private String name;
    private int id;
    private boolean enabled;

    /**
     * Default empty econstructor
     */
    public Port() {
    }

    /**
     * Full constructor used by jni
     *
     * @param  name  a <code>String</code> value
     * @param  id  a <code>int</code> value
     * @param  enabled  a <code>boolean</code> value
     */
    public Port(String name, int id, boolean enabled) {
        this.name = name;
        this.id = id;
        this.enabled = enabled;
    }

    /**
     * Get the port's name.
     *
     * @return  port's name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Get the port's ID number.
     *
     * @return  port's ID.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Get the port's "enable" status.
     *
     * @return  <code>true</code> if the port is enabled and accessible, <code>
     * false</code> else.
     */
    public boolean getEnabled() {
        return this.enabled;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : Set the port's name.
     *
     * @param  name  the new name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : the port's ID number.
     *
     * @param  id  the new ID.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * OBJECT CONSTRUCTION ONLY : the port's "enable" status.
     *
     * @param  enabled  the new status.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String toString() {
        StringBuffer s = new StringBuffer();
        s.append("[name="+name);
        s.append(",id="+id);
        s.append(",enabled="+enabled);
        s.append("]");
        return s.toString();
    }
}
