/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NodeAdapterInterceptor.java 1.12     08/05/20 SMI"
 */
package com.sun.cluster.agent.transport;

// JDK
import java.io.IOException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ReflectionException;

// JDMK
import com.sun.jdmk.interceptor.MBeanServerInterceptor;

// Cacao
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * CMASS interceptor for the transport module. All instance and attribute values
 * are fetched through JNI, and all operations are performed through an
 * invocation of a CLI. This interceptor also listen for the events triggered by
 * the cluster for dynamic handle of virtual mbeans creation and deletion.
 */
public class NodeAdapterInterceptor extends VirtualTransportInterceptor
    implements NotificationListener {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.transport");
    private final static String logTag = "NodeAdapterInterceptor";

    /* Commands */
    private final static String IFCONFIG = "/usr/sbin/ifconfig";
    private final static String IPMPSCRIPT =
        "/usr/cluster/lib/cmass/ipmpgroupmanager.sh";

    /* The MBeanServer in which this interceptor is inserted. */
    private MBeanServer mBeanServer;

    /**
     * Constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this interceptor.
     * @param  dispatcher  DomainDispatcher that this virtual mbean interceptor
     * should register into for dispatching.
     */
    public NodeAdapterInterceptor(MBeanServer mBeanServer,
        VirtualMBeanDomainDispatcher dispatcher) {

        super(mBeanServer, NodeAdapterMBean.class, dispatcher);
        this.mBeanServer = mBeanServer;
    }

    /*
     * Helper for replacing in ExitStatus the command by stdout.
     */
    public static ExitStatus scriptStderrToCmd(ExitStatus exit) {

        List outStrings = exit.getOutStrings();
        Object objs[] = outStrings.toArray();
        String cmd[] = new String[objs.length];

        for (int i = 0; i < objs.length; i++) {
            cmd[i] = (String) objs[i];
        }

        exit.setCmdExecuted(cmd);

        return exit;
    }

    /**
     * Called by the parent interceptor class
     *
     * @param  instanceName  a <code>String</code> value
     * @param  operationName  a <code>String</code> value
     * @param  params  an <code>Object[]</code> value
     * @param  signature  a <code>String[]</code> value
     *
     * @return  an <code>Object</code> value
     *
     * @exception  InstanceNotFoundException  if an error occurs
     * @exception  MBeanException  if an error occurs
     * @exception  ReflectionException  if an error occurs
     * @exception  IOException  if an error occurs
     */
    public Object invoke(String instanceName, String operationName,
        Object params[], String signature[]) throws InstanceNotFoundException,
        MBeanException, ReflectionException, IOException {

        logger.entering(logTag, "invoke: " + operationName, signature);

        // Node wide operations: local instances only
        if (!isRegistered(instanceName)) {
            throw new InstanceNotFoundException("Cannot find the instance " +
                instanceName);
        }

        // Construct the command string from the operation name.
        String cmds[][] = null;
        String instance[] = instanceName.split(AdapterMBean.SEPARATOR);
        String adapter = instance[1];

        // Support of logical addresses like hme0:1
        if (instance.length == 3)
            adapter = adapter.concat(AdapterMBean.SEPARATOR + instance[2]);

        if (operationName.equals("changeIpAddress")) {

            if ((signature != null) && (signature.length == 3) &&
                    (signature[0].equals("java.lang.String")) &&
                    (signature[1].equals("boolean")) &&
                    (signature[2].equals("boolean"))) {

                String family = "inet";

                if (((Boolean) params[1]).booleanValue())
                    family = "inet6";

                boolean persistent = ((Boolean) params[2]).booleanValue();

                if (persistent) {
                    logger.warning("RFU, persistent flag must be false.");
                    throw new ReflectionException(new IllegalArgumentException(
                            operationName));
                }

                cmds = new String[][] {
                        { IFCONFIG, adapter, family, (String) params[0] }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("changeIpmpGroup")) {

            if ((signature != null) && (signature.length == 4) &&
                    (signature[0].equals("java.lang.String")) &&
                    (signature[1].equals("boolean")) &&
                    (signature[2].equals("boolean")) &&
                    (signature[3].equals("boolean"))) {

                String group = (String) params[0];
                boolean ipv4 = false;
                boolean ipv6 = false;
                ipv4 = ((Boolean) params[1]).booleanValue();
                ipv6 = ((Boolean) params[2]).booleanValue();

                boolean persistent = ((Boolean) params[3]).booleanValue();
                String cmd1_ipv4[] = new String[] {
                        IFCONFIG, adapter, "inet", "group", group
                    };
                String cmd2_ipv4[] = new String[] {
                        IPMPSCRIPT, "grp", group, adapter, "inet"
                    };
                String cmd1_ipv6[] = new String[] {
                        IFCONFIG, adapter, "inet6", "group", group
                    };
                String cmd2_ipv6[] = new String[] {
                        IPMPSCRIPT, "grp", group, adapter, "inet6"
                    };

                if (ipv4) {

                    if (ipv6) {
                        cmds = new String[][] {
                                cmd1_ipv4, cmd2_ipv4, cmd1_ipv6, cmd2_ipv6
                            };
                    } else {
                        cmds = new String[][] { cmd1_ipv4, cmd2_ipv4 };
                    }
                } else {

                    if (ipv6) {
                        cmds = new String[][] { cmd1_ipv6, cmd2_ipv6 };
                    }
                }
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("editAdapter")) {

            if ((signature != null) && (signature.length == 3) &&
                    (signature[0].equals("java.lang.String")) &&
                    (signature[1].equals("boolean")) &&
                    (signature[2].equals("boolean"))) {

                String family = "inet";
                String test = (String) params[0];
                String standby = "-standby";

                if (((Boolean) params[1]).booleanValue())
                    standby = "standby";

                String persistent = "0";

                if (((Boolean) params[2]).booleanValue())
                    persistent = "1";

                cmds = new String[][] {
                        {
                            IPMPSCRIPT, "edit", adapter, family, test, standby,
                            persistent
                        }
                    };
            }

        } else if (operationName.equals("editIpmpGroup")) {

            if ((signature != null) && (signature.length == 8) &&
                    (signature[0].equals("java.lang.String")) &&
                    (signature[1].equals("boolean")) &&
                    (signature[2].equals("boolean")) &&
                    (signature[3].equals("boolean")) &&
                    (signature[4].equals("boolean")) &&
                    (signature[5].equals("boolean")) &&
                    (signature[6].equals("boolean")) &&
                    (signature[7].equals("java.lang.String"))) {
                boolean ipv4 = false;
                boolean ipv6 = false;
                boolean old_ipv4 = false;
                boolean old_ipv6 = false;
                String test = (String) params[0];
                String standby = "-standby";

                if (((Boolean) params[1]).booleanValue())
                    standby = "standby";

                String persistent = "0";

                if (((Boolean) params[2]).booleanValue())
                    persistent = "1";

                ipv4 = ((Boolean) params[3]).booleanValue();
                ipv6 = ((Boolean) params[4]).booleanValue();
                old_ipv4 = ((Boolean) params[5]).booleanValue();
                old_ipv6 = ((Boolean) params[6]).booleanValue();

                String group = (String) params[7];

                logger.fine("old ipv4 = " + old_ipv4);
                logger.fine("old ipv6 = " + old_ipv6);
                logger.fine("new ipv4 = " + ipv4);
                logger.fine("new ipv6 = " + ipv6);

                String addif = "";

                if ((test != null) && (test.length() > 0)) {
                    addif = "addif " + test + " deprecated";
                }

                String cmd1_ipv4_edit[] = null;
                String cmd1_ipv6_edit[] = null;
                String cmd1_ipv4_new[] = null;
                String cmd2_ipv4_new[] = null;
                String cmd1_ipv6_new[] = null;
                String cmd2_ipv6_new[] = null;
                String cmd1_ipv4_del[] = null;
                String cmd1_ipv6_del[] = null;

                // IPV4 edit
                cmd1_ipv4_edit = new String[] {
                        IPMPSCRIPT, "edit", adapter, "inet", test, standby,
                        persistent
                    };

                // IPV6 edit
                cmd1_ipv6_edit = new String[] {
                        IPMPSCRIPT, "edit", adapter, "inet6", test, standby,
                        persistent
                    };

                // Ipv4 new
                if ((test != null) && (test.length() > 0)) {
                    cmd1_ipv4_new = new String[] {
                            IFCONFIG, adapter, "inet", "plumb", "group", group,
                            "addif", test, "netmask", "+", "broadcast", "+",
                            "up", "deprecated", "-failover", standby
                        };
                } else {
                    cmd1_ipv4_new = new String[] {
                            IFCONFIG, adapter, "inet", "plumb", "group", group,
                            "netmask", "+", "broadcast", "+", "up", "-failover",
                            standby
                        };
                }

                cmd2_ipv4_new = new String[] {
                        IPMPSCRIPT, "add", group, adapter, "inet", test,
                        persistent
                    };

                // IPV4 del
                cmd1_ipv4_del = new String[] {
                        IPMPSCRIPT, "del", adapter, "inet", persistent
                    };


                // IPV6 new
                cmd1_ipv6_new = new String[] {
                        IFCONFIG, adapter, "inet6", "plumb", "up", "group",
                        group, "-failover", standby
                    };
                cmd2_ipv6_new = new String[] {
                        IPMPSCRIPT, "add", group, adapter, "inet6", test,
                        persistent
                    };


                // ipv6 del
                cmd1_ipv6_del = new String[] {
                        IPMPSCRIPT, "del", adapter, "inet6", persistent
                    };

                // Check what needs to be done based on the old IPVersions

                if (old_ipv4) {

                    if (old_ipv6) {

                        if (ipv4) {

                            if (ipv6) {

                                // edit IPV6
                                // edit IPV4
                                cmds = new String[][] {
                                        cmd1_ipv4_edit, cmd1_ipv6_edit
                                    };

                            } else {

                                // del IPV6
                                // edit IPV4
                                cmds = new String[][] {
                                        cmd1_ipv4_edit, cmd1_ipv6_del
                                    };
                            }
                        } else {

                            if (ipv6) {
                                // del IPv4
                                // edit IPv6

                                cmds = new String[][] {
                                        cmd1_ipv4_del, cmd1_ipv6_edit
                                    };

                            } else {
                                // never come here
                            }
                        }
                    } else {

                        if (ipv4) {

                            if (ipv6) {

                                // edit IPV4
                                // new IPV6
                                cmds = new String[][] {
                                        cmd1_ipv4_edit, cmd1_ipv6_new,
                                        cmd2_ipv6_new
                                    };
                            } else {

                                // edit IPV4
                                cmds = new String[][] { cmd1_ipv4_edit };
                            }
                        } else {

                            if (ipv6) {
                                // del IPV4
                                // new IPV6

                                cmds = new String[][] {
                                        cmd1_ipv4_del, cmd1_ipv6_new,
                                        cmd2_ipv6_new
                                    };

                            } else {
                                // never come here
                            }
                        }
                    }

                } else {

                    if (old_ipv6) {

                        if (ipv4) {

                            if (ipv6) {
                                // new IPV4

                                // edit IPV6
                                cmds = new String[][] {
                                        cmd1_ipv4_new, cmd2_ipv4_new,
                                        cmd1_ipv6_edit
                                    };

                            } else {

                                // del IPV6
                                // new IPV4
                                cmds = new String[][] {
                                        cmd1_ipv4_new, cmd2_ipv4_new,
                                        cmd1_ipv6_del
                                    };

                            }
                        } else {

                            if (ipv6) {

                                // edit IPV6
                                // nothing to do really.
                                cmds = new String[][] { cmd1_ipv6_edit };
                            } else {
                                // never come here
                            }
                        }
                    } else {
                        // never come here
                    }
                }

            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("removeFromIpmpGroup")) {

            if ((signature != null) && (signature.length == 3) &&
                    (signature[0].equals("boolean")) &&
                    (signature[1].equals("boolean")) &&
                    (signature[2].equals("boolean"))) {

                boolean ipv4 = false;
                boolean ipv6 = false;
                ipv4 = ((Boolean) params[0]).booleanValue();
                ipv6 = ((Boolean) params[1]).booleanValue();

                String persistent = "0";

                if (((Boolean) params[2]).booleanValue())
                    persistent = "1";

                String cmd1_ipv4[] = new String[] {
                        IFCONFIG, adapter, "inet", "group", ""
                    };
                String cmd2_ipv4[] = new String[] {
                        IPMPSCRIPT, "del", adapter, "inet", persistent
                    };
                String cmd1_ipv6[] = new String[] {
                        IFCONFIG, adapter, "inet6", "group", ""
                    };
                String cmd2_ipv6[] = new String[] {
                        IPMPSCRIPT, "del", adapter, "inet6", persistent
                    };

                if (ipv4) {

                    if (ipv6) {
                        cmds = new String[][] {
                                cmd1_ipv4, cmd2_ipv4, cmd1_ipv6, cmd2_ipv6
                            };
                    } else {
                        cmds = new String[][] { cmd1_ipv4, cmd2_ipv4 };
                    }
                } else {

                    if (ipv6) {
                        cmds = new String[][] { cmd1_ipv6, cmd2_ipv6 };
                    }
                }

            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("changeStandbyFlag")) {

            if ((signature != null) && (signature.length == 3) &&
                    (signature[0].equals("boolean")) &&
                    (signature[1].equals("boolean")) &&
                    (signature[2].equals("boolean"))) {

                String standby = "-standby";

                if (((Boolean) params[0]).booleanValue())
                    standby = "standby";

                String family = "inet";

                if (((Boolean) params[1]).booleanValue())
                    family = "inet6";

                boolean persistent = ((Boolean) params[2]).booleanValue();

                if (persistent) {
                    logger.warning("RFU, persistent flag must be false.");
                    throw new ReflectionException(new IllegalArgumentException(
                            operationName));
                }

                cmds = new String[][] {
                        { IFCONFIG, adapter, family, standby }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("changeUpFlag")) {

            if ((signature != null) && (signature.length == 3) &&
                    (signature[0].equals("boolean")) &&
                    (signature[1].equals("boolean")) &&
                    (signature[2].equals("boolean"))) {

                String up = "down";

                if (((Boolean) params[0]).booleanValue())
                    up = "up";

                String family = "inet";

                if (((Boolean) params[1]).booleanValue())
                    family = "inet6";

                boolean persistent = ((Boolean) params[2]).booleanValue();

                if (persistent) {
                    logger.warning("RFU, persistent flag must be false.");
                    throw new ReflectionException(new IllegalArgumentException(
                            operationName));
                }

                cmds = new String[][] {
                        { IFCONFIG, adapter, family, up }
                    };
            } else {
                throw new IOException("Invalid signature");
            }

        } else if (operationName.equals("unplumb")) {

            if ((signature != null) && (signature.length == 2) &&
                    (signature[0].equals("boolean")) &&
                    (signature[1].equals("boolean"))) {

                String family = "inet";

                if (((Boolean) params[0]).booleanValue())
                    family = "inet6";

                boolean persistent = ((Boolean) params[1]).booleanValue();

                if (persistent) {
                    logger.warning("RFU, persistent flag must be false.");
                    throw new ReflectionException(new IllegalArgumentException(
                            operationName));
                }

                cmds = new String[][] {
                        { IFCONFIG, adapter, family, "unplumb" }
                    };
            } else {
                throw new IOException("Invalid signature");
            }
        }

        // Run the command(s)
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new MBeanException(new CommandExecutionException(
                    ie.getMessage(), ExitStatus.createArray(exits)));
        }

        ExitStatus exitsWrapper[] = ExitStatus.createArray(exits);

        // Script command replaced by its stdout
        if (operationName.equals("changeIpmpGroup") ||
                operationName.equals("removeFromIpmpGroup")) {
            exitsWrapper[1] = scriptStderrToCmd(exitsWrapper[1]);
        } else if (operationName.equals("editIpmpGroup")) {
            exitsWrapper[0] = scriptStderrToCmd(exitsWrapper[0]);
        }

        logger.exiting(logTag, "invoke");

        return exitsWrapper;
    }

    /**
     * Called by the parent interceptor class.
     *
     * @return  a <code>String[]</code> value.
     */
    public native String[] getInstances();

    /**
     * Called by the parent interceptor class.
     *
     * @param  instanceName  a <code>String</code> value.
     * @param  attributes  a <code>String[]</code> value.
     *
     * @return  an <code>AttributeList</code> value.
     *
     * @exception  InstanceNotFoundException  if an error occurs.
     * @exception  ReflectionException  if an error occurs.
     * @exception  IOException  if an error occurs.
     */
    public native AttributeList getAttributes(String instanceName,
        String attributes[]) throws InstanceNotFoundException,
        ReflectionException, IOException;

    /**
     * Called by the parent interceptor class.
     *
     * @param  instanceName  a <code>String</code> value.
     *
     * @return  a <code>boolean</code> value.
     *
     * @exception  IOException  if an error occurs.
     */
    public native boolean isRegistered(String instanceName) throws IOException;
}
