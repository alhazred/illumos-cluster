/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)TransportPathInterceptor.java 1.8     08/05/20 SMI"
 */
package com.sun.cluster.agent.transport;

// JDK
import java.io.IOException;

import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.AttributeList;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.NotificationListener;
import javax.management.ReflectionException;

// JDMK
import com.sun.jdmk.interceptor.MBeanServerInterceptor;

// Cacao
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;


/**
 * CMASS interceptor for the transport module. All instance and attribute values
 * are fetched through JNI, and all operations are performed through an
 * invocation of a CLI. This interceptor also listen for the events triggered by
 * the cluster for dynamic handle of virtual mbeans creation and deletion.
 */
public class TransportPathInterceptor extends VirtualTransportInterceptor
    implements NotificationListener {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.transport");
    private final static String logTag = "TransportPathInterceptor";

    /* The MBeanServer in which this interceptor is inserted. */
    private MBeanServer mBeanServer;

    /**
     * Constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this interceptor.
     * @param  dispatcher  DomainDispatcher that this virtual mbean interceptor
     * should register into for dispatching.
     */
    public TransportPathInterceptor(MBeanServer mBeanServer,
        VirtualMBeanDomainDispatcher dispatcher) {

        super(mBeanServer, TransportPathMBean.class, dispatcher);
        this.mBeanServer = mBeanServer;
    }

    /**
     * Called by the parent interceptor class
     *
     * @param  instanceName  a <code>String</code> value
     * @param  operationName  a <code>String</code> value
     * @param  params  an <code>Object[]</code> value
     * @param  signature  a <code>String[]</code> value
     *
     * @return  an <code>Object</code> value
     *
     * @exception  InstanceNotFoundException  if an error occurs
     * @exception  MBeanException  if an error occurs
     * @exception  ReflectionException  if an error occurs
     * @exception  IOException  if an error occurs
     */
    public Object invoke(String instanceName, String operationName,
        Object params[], String signature[]) throws InstanceNotFoundException,
        MBeanException, ReflectionException, IOException {

        // No Transport Path operation
        throw new ReflectionException(new IllegalArgumentException(
                operationName));
    }

    /**
     * Called by the parent interceptor class.
     *
     * @return  a <code>String[]</code> value.
     */
    public native String[] getInstances();

    /**
     * Called by the parent interceptor class.
     *
     * @param  instanceName  a <code>String</code> value.
     * @param  attributes  a <code>String[]</code> value.
     *
     * @return  an <code>AttributeList</code> value.
     *
     * @exception  InstanceNotFoundException  if an error occurs.
     * @exception  ReflectionException  if an error occurs.
     * @exception  IOException  if an error occurs.
     */
    public native AttributeList getAttributes(String instanceName,
        String attributes[]) throws InstanceNotFoundException,
        ReflectionException, IOException;

    /**
     * Called by the parent interceptor class.
     *
     * @param  instanceName  a <code>String</code> value.
     *
     * @return  a <code>boolean</code> value.
     *
     * @exception  IOException  if an error occurs.
     */
    public native boolean isRegistered(String instanceName) throws IOException;
}
