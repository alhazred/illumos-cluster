/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)TransportPathMBean.java 1.6     08/05/20 SMI"
 */
package com.sun.cluster.agent.transport;

/**
 * This interface defines the management operation available on a remote Cluster
 * transport path.
 */
public interface TransportPathMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "transportpath";

    /**
     * Get the transport path's key name.
     *
     * @return  transport path's key name. This is also the MBean instance name,
     * like cluster.nodes.1.adapters.2.nodes.2.adapters.2.
     */
    public String getName();

    /**
     * Give the "online" status of the transport path.
     *
     * @return  <code>true</code> if the transport path is online, <code>
     * false</code> otherwise.
     */
    public boolean isOnline();

    /**
     * Give the "faulted" status of the transport path.
     *
     * @return  <code>true</code> if the transport path is faulted, <code>
     * false</code> otherwise.
     */
    public boolean isFaulted();

    /**
     * Give the "waiting" status of the transport path.
     *
     * @return  <code>true</code> if the transport path is waiting, <code>
     * false</code> otherwise.
     */
    public boolean isWaiting();

    /**
     * Give the first end-point name.
     *
     * @return  the first end-point name. Format is "node:adapter".
     */
    public String getEndpoint1Name();

    /**
     * Give the second end-point name.
     *
     * @return  the second end-point name. Format is "node:adapter".
     */
    public String getEndpoint2Name();
}
