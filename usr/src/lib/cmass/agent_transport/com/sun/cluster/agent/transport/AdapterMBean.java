/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)AdapterMBean.java 1.7     08/05/20 SMI"
 */
package com.sun.cluster.agent.transport;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available on a Node's
 * adapter.
 */
public interface AdapterMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "adapter";

    /**
     * String used inside Adapter's MBean instance name for separating the
     * node's name from the adapter's name.
     */
    public static final String SEPARATOR = ":";

    /**
     * Get the adapter's key name.
     *
     * @return  adapter's key name which is the MBean instance name, composed of
     * the node's name and adapter's name separated by the {@link SEPARATOR},
     * like in phys-corliss-1:qfe0 where phys-corliss-1 is a node name. This
     * name uniquely identifies the instance as, for example, qfe0 might exist
     * on both nodes phys-corliss-1 and phys-corliss-2. For
     * {@link ClusterAdapterMBean}, instances are the private interconnects. For
     * {@link NodeAdapterMBean}, instances are all interfaces: network
     * interfaces, even unplumbed, logical interfaces.
     */
    public String getName();

    /**
     * Get the name of the node owning this adapter.
     *
     * @return  the node's key name. This name is the one used for identifying
     * the MBean instance, like phys-corliss-1 in phys-corliss-1:qfe0.
     *
     * @see  com.sun.cluster.agent.node.NodeMBean
     */
    public String getNodeName();

    /**
     * Get the adapter's name.
     *
     * @return  the adapter's name, like qfe0 for instance phys-corliss-1:qfe0.
     */
    public String getInterfaceName();

    /**
     * Get the adapter's IP address.
     *
     * @return  the adapter's IP address.
     */
    public String getIpAddress();

    /**
     * Get the adapter's IPv6 address.
     *
     * @return  the adapter's IPv6 address.
     */
    public String getIpv6Address();

    /**
     * Get the adapter's netmask.
     *
     * @return  the adapter's netmask.
     */
    public String getNetmask();

    /**
     * Get the adapter's controler type. Matching the C API, this method
     * currently returns a <code>String</code> representation of the type;
     * should we have an exhaustive list of supported types and then replace
     * this string by a defined <code>Enum</code> object ?
     *
     * @return  the adapter's controler type, like dlpi.
     */
    public String getType();

    /**
     * Get adapter's attributes values.
     *
     * @return  a <code>Hashmap</code> containing the attribute's name as key
     * and attribute's value as value.
     */
    public java.util.Map getAttributes();
}
