/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)TransportModule.java 1.11     08/05/21 SMI"
 */
package com.sun.cluster.agent.transport;

// JDK
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.ObjectName;

// Cacao
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.Module;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.element.AvailabilityStatusEnum;
import com.sun.cacao.element.OperationalStateEnum;

// Local
import com.sun.cluster.common.ClusterRuntimeException;


/**
 * Core implementation of the Transport module. This module is using JDMK's
 * interceptor and JNI design pattern for subsequent calls to the SCHA API.
 */
public class TransportModule extends Module {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.transport");
    private final static String logTag = "TransportModule";

    // Our dispatcher.
    private VirtualMBeanDomainDispatcher dispatcher;

    /* Internal reference to the JDMK interceptors and MBean manager. */
    private ClusterAdapterInterceptor clusterAdapterInterceptor;
    private NodeAdapterInterceptor nodeAdapterInterceptor;
    private ZoneAdapter zoneAdapter;
    private CableInterceptor cableInterceptor;
    private JunctionInterceptor junctionInterceptor;
    private TransportPathInterceptor transportPathInterceptor;
    private TransportManager transportManager;
    private ObjectNameFactory objectNameFactory;

    /**
     * Constructor called by the container
     *
     * @param  descriptor  the deployment descriptor used to load the module
     */
    public TransportModule(DeploymentDescriptor descriptor) {
        super(descriptor);
        logger.entering(logTag, "<init>", descriptor);

        Map parameters = descriptor.getParameters();

        try {

            // Load the agent JNI library
            String library = (String) parameters.get("library");
            Runtime.getRuntime().load(library);

        } catch (UnsatisfiedLinkError ule) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            availabilityStatusSetAdd(AvailabilityStatusEnum.NOT_INSTALLED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to load native runtime library" + ule);
            throw ule;
        }

	objectNameFactory = new ObjectNameFactory(getDomainName());

        logger.exiting(logTag, "<init>");
    }

    /**
     * Invoked by the container when unlocking the module.
     *
     * @exception  ClusterRuntimeException  if unable to start.
     */
    protected void start() throws ClusterRuntimeException {
        logger.entering(logTag, "start");

	ObjectName objectName;

        logger.finest("create VirtualMBeanDomainDispatcher");
        dispatcher = new VirtualMBeanDomainDispatcher(getMbs(),
                objectNameFactory);

        try {
            logger.fine("unlock dispatcher");
            dispatcher.unlock();

            logger.finest("create ClusterAdapterInterceptor");
            clusterAdapterInterceptor = new ClusterAdapterInterceptor(getMbs(),
                    dispatcher);
            logger.fine("unlock ClusterAdapterInterceptor");
            clusterAdapterInterceptor.unlock();

            logger.finest("create NodeAdapterInterceptor");
            nodeAdapterInterceptor = new NodeAdapterInterceptor(getMbs(),
                    dispatcher);
            logger.fine("unlock NodeAdapterInterceptor");
            nodeAdapterInterceptor.unlock();

            logger.finest("create CableInterceptor");
            cableInterceptor = new CableInterceptor(getMbs(), dispatcher);
            logger.fine("unlock CableInterceptor");
            cableInterceptor.unlock();

            logger.finest("create JunctionInterceptor");
            junctionInterceptor = new JunctionInterceptor(getMbs(), dispatcher);
            logger.fine("unlock JunctionInterceptor");
            junctionInterceptor.unlock();

            logger.finest("create TransportPathInterceptor");
            transportPathInterceptor = new TransportPathInterceptor(getMbs(),
                    dispatcher);
            logger.finest("create TransportPathInterceptor");
            transportPathInterceptor.unlock();

            // Create Transport Manager
            logger.finest("create TransportManager");
            transportManager = new TransportManager(getMbs());
            objectName = objectNameFactory.getObjectName(
                    TransportManagerMBean.class, null);
            logger.fine("register TransportManager");
            getMbs().registerMBean(transportManager, objectName);

            // Create ZoneAdapter MBean
            logger.finest("create ZoneAdapter");
            zoneAdapter = new ZoneAdapter(getMbs());
            objectName = objectNameFactory.getObjectName(
		ZoneAdapterMBean.class, null);
            logger.fine("register ZoneAdapterMBean");
            getMbs().registerMBean(zoneAdapter, objectName);

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to start Transort module : " +
                e.getMessage());
            throw new ClusterRuntimeException(e.getMessage());

        } finally {
            logger.exiting(logTag, "start");
        }
    }

    /**
     * Invoked by the container when locking the module.
     */
    protected void stop() {
	ObjectName objectName;

        logger.entering(logTag, "stop");

        try {
            logger.fine("lock dispatcher");
            dispatcher.lock();
            dispatcher = null;

            logger.fine("lock clusterAdapterInterceptor");
            clusterAdapterInterceptor.lock();
            clusterAdapterInterceptor = null;

            logger.fine("lock nodeAdapterInterceptor");
            nodeAdapterInterceptor.lock();
            nodeAdapterInterceptor = null;

            logger.fine("lock cableInterceptor");
            cableInterceptor.lock();
            cableInterceptor = null;

            logger.fine("lock junctionInterceptor");
            junctionInterceptor.lock();
            junctionInterceptor = null;

            logger.fine("lock transportPathInterceptor");
            transportPathInterceptor.lock();
            transportPathInterceptor = null;

            objectName = objectNameFactory.getObjectName(
		TransportManagerMBean.class, null);
            getMbs().unregisterMBean(objectName);

            objectName = objectNameFactory.getObjectName(
		ZoneAdapterMBean.class, null);
            getMbs().unregisterMBean(objectName);

        } catch (Exception e) {
            availabilityStatusSetAdd(AvailabilityStatusEnum.FAILED);
            setOperationalState(OperationalStateEnum.DISABLED);
            logger.warning("Unable to stop RGM module : " + e.getMessage());
            throw new ClusterRuntimeException(e.getMessage());

        } finally {
            logger.exiting(logTag, "stop");
        }
    }
}
