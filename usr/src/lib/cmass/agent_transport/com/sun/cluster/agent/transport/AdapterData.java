/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)AdapterData.java	1.4	08/05/21 SMI"
 */
package com.sun.cluster.agent.transport;

/**
 */
public class AdapterData implements java.io.Serializable {

    /**
     * Name of adapter
     */
    public String name;

    /**
     * NULL or IPMP group it belongs to
     */
    public String group;

    /**
     * flags from SIOCGLIFFLAGS
     */
    public long flags;

    /**
     * true if IPv6, false if IPv4
     */
    public boolean isIPv6;

    /**
     * status
     */
    public int status;

    /**
     * subnets masks
     */
    public String ipMasks[];

    /**
     * subnets addresses
     */
    public String ipNets[];

    /**
     * subnets IP addresses
     */
    public String ipAddrs[];

    public AdapterData() {
    }

    public String toString() {
	StringBuffer s = new StringBuffer();
	s.append("[name="+name);
	s.append(",group="+group);
	s.append(",flags="+flags);
	s.append(",isIPv6="+isIPv6);
	s.append(",status="+status);
	s.append(",ipMasks="+ipMasks);
	s.append(",ipNet="+ipNets);
	s.append(",ipAddr="+ipAddrs);
	s.append("]");
	return s.toString();
    }

}
