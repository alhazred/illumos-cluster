/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)TransportManagerMBean.java 1.8     08/12/03 SMI"
 */
package com.sun.cluster.agent.transport;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available to handle Adapters,
 * Cables and Junctions.
 */
public interface TransportManagerMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public final static String TYPE = "transportManager";

    /**
     * This method creates a new instance of a Cluster Adapter.
     *
     * @param  type  the adapter's type, like dlpi.
     * @param  node  the name of the adapter's node.
     * @param  adapter  the adapter's name, like hme1, qfe0, ...
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addClusterAdapter(String type, String node,
        String adapter) throws CommandExecutionException;

    /**
     * This method creates a new instance of a Cluster Adapter.
     *
     * @param  type  the adapter's type, like dlpi.
     * @param  node  the name of the adapter's node.
     * @param  adapter  the adapter's name, like hme1, qfe0, ...
     * @param  vlan_id  the adapter's vlan_id
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addClusterAdapter(String type, String node,
        String adapter, String vlan_id) throws CommandExecutionException;

    /**
     * This method creates a new instance of a Cable.
     *
     * @param  endpoint1  the first cable endpoint's name, like phys-node1:hme1.
     * @param  endpoint2  the second cable endpoint's name, like hub0@1.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addCable(String endpoint1, String endpoint2)
        throws CommandExecutionException;

    /**
     * This method creates a new instance of a Junction.
     *
     * @param  type  the adapter's type, like dlpi.
     * @param  junction  the Junction's name, like hub0.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addJunction(String type, String junction)
        throws CommandExecutionException;


    /**
     * Remove definitely this Cluster Adapter.
     *
     * @param  node  the name of the adapter's node.
     * @param  adapter  the adapter's name, like hme1, qfe0, ...
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeClusterAdapter(String node, String adapter)
        throws CommandExecutionException;

    /**
     * Remove definitely this Cable.
     *
     * @param  endpoint1  the first cable endpoint's name, like phys-node1:hme1.
     * @param  endpoint2  the second cable endpoint's name, like hub0@1.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeCable(String endpoint1, String endpoint2)
        throws CommandExecutionException;

    /**
     * Remove definitely this Junction.
     *
     * @param  junction  the Junction's name, like hub0.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeJunction(String junction)
        throws CommandExecutionException;
}
