/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)CableMBean.java 1.6     08/05/20 SMI"
 */
package com.sun.cluster.agent.transport;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available on a remote Cluster
 * cable.
 */
public interface CableMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "cable";

    /**
     * Get the cable's key name.
     *
     * @return  cable's key name which is the MBean instance name, like
     * "cluster.cables.1".
     */
    public String getName();

    /**
     * Get cable's ID.
     *
     * @return  cable's ID number.
     */
    public int getID();

    /**
     * Give the "enable" status of the cable.
     *
     * @return  <code>true</code> if the cable is enabled, <code>false</code> if
     * it's disabled.
     */
    public boolean isEnabled();

    /**
     * Give first end-point type.
     *
     * @return  <code>true</code> if the first end-point is an adapter <code>
     * false</code> if it's a junction.
     */
    public boolean isEndpoint1Adapter();

    /**
     * Give second end-point type.
     *
     * @return  <code>true</code> if the second end-point is an adapter <code>
     * false</code> if it's a junction.
     */
    public boolean isEndpoint2Adapter();

    /**
     * Give first end-point name, referencing either an adapter or a junction.
     *
     * @return  the first end-point name. Format in case of a junction:
     * "node:junction@port". In case of an adapter, the port number is removed
     * if zero: "node:adapter" or "node:adapter@port" with port not equal to 0.
     *
     * @see  AdapterMBean
     * @see  JunctionMBean
     */
    public String getEndpoint1Name();

    /**
     * Give second end-point name, referencing either an adapter or a junction.
     *
     * @return  the second end-point name Format in case of a junction:
     * "node:junction@port". In case of an adapter, the port number is removed
     * if zero: "node:adapter" or "node:adapter@port" with port not equal to 0.
     *
     * @see  AdapterMBean
     * @see  JunctionMBean
     */
    public String getEndpoint2Name();

    /**
     * Disable the cable.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] disable() throws CommandExecutionException;

    /**
     * Enable the cable.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] enable() throws CommandExecutionException;
}
