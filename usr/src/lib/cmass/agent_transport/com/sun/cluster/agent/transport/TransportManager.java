/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)TransportManager.java 1.11     08/12/03 SMI"
 */
package com.sun.cluster.agent.transport;

// JMX
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.MBeanException;

// Cacao
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.ClusterPaths;


/**
 * This class implements the {@link TransportManagerMBean} interface. This MBean
 * is created and register into the CMAS agent in the same way as the
 * {@link TransportModule} MBean.
 */
public class TransportManager implements TransportManagerMBean {

    /* The MBeanServer in which this MBean is inserted. */

    private MBeanServer mBeanServer;

    /**
     * Default constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this MBean.
     */
    public TransportManager(MBeanServer mBeanServer) {
        this.mBeanServer = mBeanServer;
    }

    /**
     * This method creates a new instance of a Cluster Adapter.
     *
     * @param  type  the adapter's type, like dlpi.
     * @param  node  the name of the adapter's node.
     * @param  adapter  the adapter's name, like hme1, qfe0, ...
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addClusterAdapter(String type, String node,
        String adapter) throws CommandExecutionException {

        // Run the command(s)
        String cmds[][] = new String[][] {
                {
                    ClusterPaths.CL_INTR_CMD, "add",
                    node + AdapterMBean.SEPARATOR + adapter
                }
            };
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * This method creates a new instance of a Cluster Adapter.
     *
     * @param  type  the adapter's type, like dlpi.
     * @param  node  the name of the adapter's node.
     * @param  adapter  the adapter's name, like hme1, qfe0, ...
     * @param  vlan_id  the adapter's vlan_id
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addClusterAdapter(String type, String node,
        String adapter, String vlan_id) throws CommandExecutionException {

        // Run the command(s)
        StringBuffer adapterName = new StringBuffer(node);
        String cmds[][] = null;
        int vlanInstNum;

        adapterName.append(AdapterMBean.SEPARATOR);
        vlanInstNum = 1000 * Integer.parseInt(vlan_id);
        vlanInstNum = vlanInstNum +
            Character.getNumericValue(adapter.charAt(adapter.length() - 1));
        adapterName.append(adapter.substring(0, adapter.length() - 1));
        adapterName.append(vlanInstNum);

        cmds = new String[][] {
                { ClusterPaths.CL_INTR_CMD, "add", adapterName.toString() }
            };

        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * This method creates a new instance of a Cable.
     *
     * @param  endpoint1  the first cable endpoint's name, like phys-node1:hme1.
     * @param  endpoint2  the second cable endpoint's name, like hub0@1.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addCable(String endpoint1, String endpoint2)
        throws CommandExecutionException {

        // Run the command(s)
        String cmds[][] = new String[][] {
                { ClusterPaths.CL_INTR_CMD, "add", endpoint1 + "," + endpoint2 }
            };
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * This method creates a new instance of a Junction.
     *
     * @param  type  the junction's type, like switch.
     * @param  junction  the Junction's name, like hub0.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addJunction(String type, String junction)
        throws CommandExecutionException {

        // Run the command(s)
        String cmds[][] = new String[][] {
                { ClusterPaths.CL_INTR_CMD, "add", junction }
            };
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }


    /**
     * Remove definitely this Cluster Adapter.
     *
     * @param  node  the name of the adapter's node.
     * @param  adapter  the adapter's name, like hme1, qfe0, ...
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeClusterAdapter(String node, String adapter)
        throws CommandExecutionException {

        // Run the command(s)
        String cmds[][] = new String[][] {
                {
                    ClusterPaths.CL_INTR_CMD, "remove",
                    node + AdapterMBean.SEPARATOR + adapter
                }
            };

        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * Remove definitely this Cable.
     *
     * @param  endpoint1  the first cable endpoint's name, like phys-node1:hme1.
     * @param  endpoint2  the second cable endpoint's name, like hub0@1.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeCable(String endpoint1, String endpoint2)
        throws CommandExecutionException {

        // Run the command(s)

        String cmds[][] = new String[][] {
                {
                    ClusterPaths.CL_INTR_CMD, "remove",
                    endpoint1 + "," + endpoint2
                }
            };
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }

    /**
     * Remove definitely this Junction.
     *
     * @param  junction  the Junction's name, like hub0.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeJunction(String junction)
        throws CommandExecutionException {

        // Run the command(s)
        String cmds[][] = new String[][] {
                { ClusterPaths.CL_INTR_CMD, "remove", junction }
            };
        InvocationStatus exits[];

        try {
            exits = InvokeCommand.execute(cmds, null);
        } catch (InvocationException ie) {
            exits = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return ExitStatus.createArray(exits);
    }
}
