/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)NodeAdapterMBean.java 1.7     08/05/20 SMI"
 */
package com.sun.cluster.agent.transport;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;


/**
 * This interface defines the management operation available on a Node's
 * adapter.
 */
public interface NodeAdapterMBean extends AdapterMBean {

    /**
     * This field is used internally by the CMASS agent to identify the TYPE of
     * the module.
     */
    public static final String TYPE = "nodeadapter";

    /**
     * Get the adapter's IPMP group name.
     *
     * @return  the adapter's IPMP group name if any, <code>NULL</code>
     * otherwise.
     *
     * @see  com.sun.cluster.agent.ipmpgroup.IpmpGroupMBean
     */
    public String getIpmpGroup();

    /**
     * Give the adapter's configuration status: plumbed or unplumbed.
     *
     * @return  <code>true</code> if the adapter is pumbed, <code>false</code>
     * if it's unplumbed.
     */
    public boolean isPlumbed();

    /**
     * Give the interface's piece of information: logical or physical.
     *
     * @return  <code>true</code> if the adapter is logical, <code>false</code>
     * if it's physical.
     */
    public boolean isLogical();

    /**
     * Get the adapter's broadcast address.
     *
     * @return  the adapter's broadcast address.
     */
    public String getBroadcast();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag UP is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagUP();


    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag BROADCAST is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagBROADCAST();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag DEBUG is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagDEBUG();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag LOOPBACK is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagLOOPBACK();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag POINTOPOINT is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagPOINTOPOINT();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag NOTRAILERS is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagNOTRAILERS();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag RUNNING is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagRUNNING();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag NOARP is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagNOARP();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag PROMISC is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagPROMISC();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag ALLMULTI is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagALLMULTI();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag INTELLIGENT is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagINTELLIGENT();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag MULTICAST is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagMULTICAST();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag MULTI_BCAST is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagMULTI_BCAST();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag UNNUMBERED is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagUNNUMBERED();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag DHCPRUNNING is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagDHCPRUNNING();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag PRIVATE is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagPRIVATE();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag NOXMIT is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagNOXMIT();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag NOLOCAL is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagNOLOCAL();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag DEPRECATED is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagDEPRECATED();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag ADDRCONF is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagADDRCONF();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag ROUTER is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagROUTER();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag NONUD is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagNONUD();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag ANYCAST is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagANYCAST();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag NORTEXCH is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagNORTEXCH();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag IPV4 is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagIPV4();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag IPV6 is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagIPV6();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag MIPRUNNING is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagMIPRUNNING();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag NOFAILOVER is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagNOFAILOVER();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag FAILED is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagFAILED();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag STANDBY is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagSTANDBY();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag INACTIVE is set, <code>
     * false</code> otherwise.
     */
    public boolean isFlagINACTIVE();

    /**
     * Give the interface's flag setting.
     *
     * @return  <code>true</code> if the flag OFFLINE is set, <code>false</code>
     * otherwise.
     */
    public boolean isFlagOFFLINE();

    /**
     * Change the adapter's IP address.
     *
     * @param  newIpAddress  the new IP address of the adapter.
     * @param  ipv6  <code>true</code> if applies to IPv6, <code>false</code>
     * otherwise.
     * @param  persistent  <code>true</code> to make the change persistent,
     * <code>false</code> otherwise. RFU, must be set to false.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeIpAddress(String newIpAddress, boolean ipv6,
        boolean persistent) throws CommandExecutionException;

    /**
     * Move the adapter to another IPMP group (public adapter only).
     *
     * @param  newIpmpGroup  the name of the adapter's new IPMP group.
     * @param  ipv4  <code>true</code> if applies to IPV4.
     * @param  ipv6  <code>true</code> if applies to IPv6.
     * @param  persistent  <code>true</code> to make the change persistent,
     * <code>false</code> otherwise.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeIpmpGroup(String newIpmpGroup, boolean ipv4,
        boolean ipv6, boolean persistent) throws CommandExecutionException;

    /**
     * /** Change the test address and standby flag settings of the adapter's
     * IPMP group (public adapter only). If no test address is already
     * configured, create it.
     *
     * @param  test  the test address string to configure upon this adapter.
     * IPv4: mandatory, if the adapter is not already plumbed it will be the
     * physical interface's address, otherwise it will be the address of a new
     * logical address. IPv6: optional, if the adapter is not already plumbed
     * and test address is not null or empty, it will be the physical
     * interface's address, otherwise it will be the address of a new logical
     * address.
     * @param  standby  <code>true</code> to set the standby flag, <code>
     * false</code> otherwise.
     * @param  persistent  <code>true</code> to make the change persistent after
     * reboot, <code>false</code> otherwise.
     * @param  enable_ipv4  <code>true</code> for an IPV4 configuration.
     * @param  enable_ipv6  <code>true</code> for an IPV6 configuration.
     * @param  old_ipv4  old IPV4 value.
     * @param  old_ipv6  old IPV6 value.
     * @param  ipmpgroup  ipmp group name.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] editIpmpGroup(String test, boolean standby,
        boolean persistent, boolean enable_ipv4, boolean enable_ipv6,
        boolean old_ipv4, boolean old_ipv6, String ipmpgroup)
        throws CommandExecutionException;

    /**
     * Change the test address and standby flag settings of the adapter's IPMP
     * group (public adapter only). If no test address is already configured,
     * create it.
     *
     * @param  test  the test address string to configure upon this adapter.
     * IPv4: mandatory, if the adapter is not already plumbed it will be the
     * physical interface's address, otherwise it will be the address of a new
     * logical address.
     * @param  standby  <code>true</code> to set the standby flag, <code>
     * false</code> otherwise.
     * @param  persistent  <code>true</code> to make the change persistent after
     * reboot, <code>false</code> otherwise.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */

    public ExitStatus[] editAdapter(String test, boolean standby,
        boolean persistent) throws CommandExecutionException;

    /**
     * Remove the adapter from its IPMP group (public adapter only): remove its
     * test address by unplumbing any logical interface built from this adapter
     * and with the flags DEPRECATED and NOFAILOVER set, unplumb the adapter if
     * its IP version is IPv4, its address is 0.0.0.0 and it has no more logical
     * addresses, otherwise, just reset its group name.
     *
     * @param  persistent  <code>true</code> to make the change persistent,
     * <code>false</code> otherwise. RFU, must be set to false.
     * @param  enable_ipv4  <code>true</code> for an IPV4 configuration.
     * @param  enable_ipv6  <code>true</code> for an IPV6 configuration.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] removeFromIpmpGroup(boolean enable_ipv4,
        boolean enable_ipv6, boolean persistent)
        throws CommandExecutionException;

    /**
     * Change the value of the adapter's Standby flag.
     *
     * @param  standby  <code>true</code> to set the standby flag, <code>
     * false</code> to unset the standby flag.
     * @param  ipv6  <code>true</code> if applies to IPv6, <code>false</code>
     * otherwise.
     * @param  persistent  <code>true</code> to make the change persistent,
     * <code>false</code> otherwise. RFU, must be set to false.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeStandbyFlag(boolean standby, boolean ipv6,
        boolean persistent) throws CommandExecutionException;


    /**
     * Change the value of the adapter's UP flag.
     *
     * @param  up  <code>true</code> to set up the interface, <code>false</code>
     * to set it down.
     * @param  ipv6  <code>true</code> if applies to IPv6, <code>false</code>
     * otherwise.
     * @param  persistent  <code>true</code> to make the change persistent,
     * <code>false</code> otherwise. RFU, must be set to false.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] changeUpFlag(boolean up, boolean ipv6,
        boolean persistent) throws CommandExecutionException;

    /**
     * Unplumb the adapter.
     *
     * @param  ipv6  <code>true</code> if applies to IPv6, <code>false</code>
     * otherwise.
     * @param  persistent  <code>true</code> to make the change persistent,
     * <code>false</code> otherwise. RFU, must be set to false.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] unplumb(boolean ipv6, boolean persistent)
        throws CommandExecutionException;
}
