/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)VirtualTransportInterceptor.java 1.9     09/03/27 SMI"
 */
package com.sun.cluster.agent.transport;

// JDK
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.AttributeList;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerNotification;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;

// JDMK
import com.sun.jdmk.JdmkMBeanServer;
import com.sun.jdmk.ServiceName;
import com.sun.jdmk.interceptor.MBeanServerInterceptor;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.VirtualMBeanDomainDispatcher;
import com.sun.cacao.agent.VirtualMBeanInterceptor;
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;

// Local
import com.sun.cluster.agent.event.ClEventDefs;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.event.SysEventNotifierMBean;
import com.sun.cluster.agent.node.Node;


/**
 * Generic implementation of a TransportInterceptor. MBean type specific
 * MBeanServerInterceptor classes extand this class for implementation.
 */
public abstract class VirtualTransportInterceptor
    extends VirtualMBeanInterceptor implements NotificationListener {

    /* Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.transport");
    private final static String logTag = "VirtualTransportInterceptor";

    /* Internal reference to the SysEvent Notifier. */
    private ObjectName sysEventNotifier;

    /* The MBeanServer in which this interceptor is inserted. */
    private MBeanServer mBeanServer;

    /* MBean interface class. */
    private final Class mBeanInterface;

    /**
     * Default Constructor.
     *
     * @param  mBeanServer  The MBeanServer in which to insert this interceptor.
     * @param  mBeanInterface  The MBean interface class.
     * @param  dispatcher  DomainDispatcher that this virtual mbean interceptor
     * should register into for dispatching.
     */
    public VirtualTransportInterceptor(MBeanServer mBeanServer,
        Class mBeanInterface, VirtualMBeanDomainDispatcher dispatcher) {
        super(dispatcher, mBeanInterface);
        logger.entering(logTag, "<init>",
            new Object[] { mBeanServer, mBeanInterface, dispatcher });
        this.mBeanInterface = mBeanInterface;
        this.mBeanServer = mBeanServer;
        this.sysEventNotifier =
            new ObjectNameFactory("com.sun.cluster.agent.event").getObjectName(
                SysEventNotifierMBean.class, null);
        logger.exiting(logTag, "<init>");
    }

    /**
     * Called when the module is unlocked.
     */
    public void unlock() throws Exception {
        super.unlock();
        logger.entering(logTag, "unlock");
        mBeanServer.addNotificationListener(sysEventNotifier, this, null, null);
        logger.exiting(logTag, "unlock");
    }

    /**
     * Called when the module is locked
     */
    public void lock() throws Exception {
        super.lock();
        logger.entering(logTag, "lock");
        mBeanServer.removeNotificationListener(sysEventNotifier, this, null,
            null);
        logger.exiting(logTag, "lock");
    }

    /**
     * The interceptor should monitor create/destroy/attribute-change events and
     * emit appropriate notifications from the mbean server (for create/destroy)
     * or from the virtual mbean (attribute change).
     *
     * <p>This method implements the NotificationListener interface.
     *
     * @param  notification  a <code>Notification</code> value
     * @param  handback  an <code>Object</code> value
     */
    public void handleNotification(Notification notification, Object handback) {

        logger.entering(logTag, "handleNotification",
            new Object[] { notification, handback });

        if (!(notification instanceof SysEventNotification))
            return;

        SysEventNotification clEvent = (SysEventNotification) notification;

        // XXX Not sure about Transport Events to support, not unit tested
        if (clEvent.getSubclass().equals(
                    ClEventDefs.ESC_CLUSTER_TP_IF_CONFIG_CHANGE)) {

            Map clEventAttrs = clEvent.getAttrs();

            String name = (String) clEventAttrs.get(ClEventDefs.CL_TP_IF_NAME);

            int clConfigAction =
                ((Long) (clEventAttrs.get(ClEventDefs.CL_CONFIG_ACTION)))
                .intValue();

            // workaround for JDK1.5/1.6 to make sure the enum is referenced
            ClEventDefs.ClEventConfigActionEnum configAction = 
                ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED;
            configAction = (ClEventDefs.ClEventConfigActionEnum)
                (ClEventDefs.ClEventConfigActionEnum.getEnum(
                        ClEventDefs.ClEventConfigActionEnum.class,
                        clConfigAction));

            if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.CL_EVENT_CONFIG_ADDED) {

                // Send an MBean registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Register MBean " + name);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            mBeanInterface, name);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.REGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "caught exception", e);
                }
            } else if (configAction ==
                    ClEventDefs.ClEventConfigActionEnum.
                    CL_EVENT_CONFIG_REMOVED) {

                // Send an MBean registration event
                if (logger.isLoggable(Level.FINEST))
                    logger.finest("Register MBean " + name);

                try {
                    ObjectName obj = getObjectNameFactory().getObjectName(
                            mBeanInterface, name);
                    ObjectName delegateName = new ObjectName(
                            ServiceName.DELEGATE);
                    Notification notif = new MBeanServerNotification(
                            MBeanServerNotification.UNREGISTRATION_NOTIFICATION,
                            delegateName, 0, obj);
                    ((JdmkMBeanServer) mBeanServer).getMBeanServerDelegate()
                    .sendNotification(notif);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "caught exception", e);
                }
            } else
                logger.fine("Ignoring config change " + configAction);
        }

        logger.exiting(logTag, "handleNotification");
    }

    /**
     * Called by the parent interceptor class to invoke operations. Processing
     * done in MBean type specific sub-class.
     *
     * @param  instanceName  a <code>String</code> value
     * @param  operationName  a <code>String</code> value
     * @param  params  an <code>Object[]</code> value
     * @param  signature  a <code>String[]</code> value
     *
     * @return  an <code>Object</code> value
     *
     * @exception  InstanceNotFoundException  if an error occurs
     * @exception  MBeanException  if an error occurs
     * @exception  ReflectionException  if an error occurs
     * @exception  IOException  if an error occurs.
     */
    public abstract Object invoke(String instanceName, String operationName,
        Object params[], String signature[]) throws InstanceNotFoundException,
        MBeanException, ReflectionException, IOException;

    /**
     * Called by the parent interceptor class. Processing done in MBean type
     * specific sub-class.
     *
     * @return  a <code>String[]</code> value.
     */
    public abstract String[] getInstances();

    /**
     * Called by the parent interceptor class. Processing done in MBean type
     * specific sub-class.
     *
     * @param  instanceName  a <code>String</code> value.
     * @param  attributes  a <code>String[]</code> value.
     *
     * @return  an <code>AttributeList</code> value.
     *
     * @exception  InstanceNotFoundException  if an error occurs.
     * @exception  ReflectionException  if an error occurs.
     * @exception  IOException  if an error occurs.
     */
    public abstract AttributeList getAttributes(String instanceName,
        String attributes[]) throws InstanceNotFoundException,
        ReflectionException, IOException;

    /**
     * Called by the parent interceptor class. Processing done in MBean type
     * specific sub-class.
     *
     * @param  instanceName  a <code>String</code> value.
     *
     * @return  a <code>boolean</code> value.
     *
     * @exception  IOException  if an error occurs.
     */
    public abstract boolean isRegistered(String instanceName)
        throws IOException;
}
