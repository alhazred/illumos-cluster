/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ZoneAdapter.java 1.4     08/05/21 SMI"
 */
package com.sun.cluster.agent.transport;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServer;

import com.sun.cluster.common.CMASMBean;

/**
 * This interface defines the management operations available on a Zone's
 * adapter.
 */
public class ZoneAdapter extends CMASMBean implements ZoneAdapterMBean  {

    public static final String ZONE_SEPARATOR = ":";

    private static Logger logger =
        Logger.getLogger("com.sun.cluster.agent.transport");
    private final static String logTag = "ZoneAdapter";
    private MBeanServer mBeanServer;

    /**
     * Constructor with MBeanServer
     *
     * @param  mBeanServer  The MBeanServer in which to insert this MBean.
     */
    public ZoneAdapter(MBeanServer mBeanServer) {
        this.mBeanServer = mBeanServer;
    }

    public native AdapterData[] readDataJNI(String node, String zone,
	boolean allAddresses);

    public AdapterData[] readData(String nodeKey, boolean allAddresses) {
	dprintln("ZoneAdapter.readData("+nodeKey+","+allAddresses+")");
	String zone = null;
	String name[] = nodeKey.split(ZONE_SEPARATOR);
	if (name.length > 1)
	    zone = name[1];

	AdapterData[] arr = null;
	arr = readDataJNI(name[0], zone, allAddresses);
	return arr;
    }

    public void test(String args[]) throws Error {
	dprintln("starting ZoneAdapter test");
	for (int i = 0; i < args.length; i++)
	    dprintln("  arg["+i+"] = "+args[i]);

	boolean allAddrs = false;
	try {
	    loadLib("libcmas_agent_transport.so");
	    String node = "localhost";
	    String zone = "zone1a";
	    int arg = 0;
	    for (arg = 0; arg < args.length; arg++) {
		if (args[arg].equals("-a")) {
		    allAddrs = true;
		} else if (args[arg].equals("-n")) {
		    node = args[++arg];
		} else if (args[arg].equals("-z")) {
		    zone = args[++arg];
		} else {
		    break;
		}
	    }

	    String nodeKey = node + ":" + zone;
	    AdapterData[] arr = readData(nodeKey, allAddrs);
	    if (arr == null) {
		println("readData returned null!");
		throw new Error("readData returned null!");
	    }
	    dprintln("readData succeeded");
	    dprintln("arr =  " + arr);

	    println("returned " + arr.length + " adapters");
	    for (int j = 0; j < arr.length; j++) {
		println("adapter "+j+": " + arr[j]);
	    }
	} catch (Exception e) {
	    println("readData failed!");
	    println(e.getMessage());
	    e.printStackTrace();
	    throw new Error("readData failed!");
	}

	return;
    }

}
