/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Adapter mbean interceptor
 */

#pragma	ident	"@(#)clusteradapter_interceptor_jni.c 1.6	08/05/20 SMI"

#include "com/sun/cluster/agent/transport/ClusterAdapterInterceptor.h"

#include <jniutil.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/sockio.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <unistd.h>
#include <cl_query/cl_query_types.h>

/*
 * Name attribute separator: between node name and adapter name
 */
const char separator = ':';

/*
 * Helper for attribut Attributes
 */
static jobject getAttributesList(JNIEnv * env,
    cl_query_adapter_info_t *adapter);

static jobjectArray getPortsList(JNIEnv * env,
    cl_query_adapter_info_t *adapter);
/*
 * Class:     com_sun_cluster_agent_transport_ClusterAdapterInterceptor
 * Method:    getInstances
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL
Java_com_sun_cluster_agent_transport_ClusterAdapterInterceptor_getInstances
	(JNIEnv * env, jobject this) {

	const char *instance_name_utf;

	jobject instance_name = NULL;
	jclass array_list_class = NULL;
	jmethodID array_list_constructor;
	jmethodID array_list_add;
	jmethodID array_list_to_array;

	jclass string_class = NULL;
	jobjectArray string_array = NULL;

	jobject instance_names_list = NULL;

	jobjectArray return_array = NULL;

	cl_query_result_t result;
	cl_query_adapter_info_t *adapter = NULL;
	cl_query_error_t error = NULL;

	/*
	 * We must return a Java array of Strings containing the names of
	 * all the instances of our object type
	 */

	/*
	 * Local reference: need to delete it afterwards
	 */
	string_class = (*env)->FindClass(env, "java/lang/String");
	if (string_class == NULL)
		goto error;

	/*
	 * Local reference: need to delete afterwards
	 */
	array_list_class = (*env)->FindClass(env, "java/util/ArrayList");
	if (array_list_class == NULL)
		goto error;

	/*
	 * Pick up a methodID to the constructor
	 */
	array_list_constructor = (*env)->GetMethodID(env, array_list_class,
	    "<init>", "()V");

	/*
	 * Pick up a methodID to the 'add' method
	 */
	array_list_add = (*env)->GetMethodID(env, array_list_class,
	    "add", "(Ljava/lang/Object;)Z");

	/*
	 * Pick up a methodID to the toArray method
	 */
	array_list_to_array = (*env)->GetMethodID(env, array_list_class,
	    "toArray", "([Ljava/lang/Object;)[Ljava/lang/Object;");

	/*
	 * Create the empty ArrayList
	 */
	instance_names_list = (*env)->NewObject(env, array_list_class,
	    array_list_constructor);
	delRef(env, array_list_class);
	if (instance_names_list == NULL)
		goto error;

	/*
	 * Now fill it with instance names
	 */

	error = cl_query_get_info(CL_QUERY_ADAPTER_INFO, NULL, &result);
	if (error != CL_QUERY_OK)
		goto error;

	for (adapter = (cl_query_adapter_info_t *)result.value_list;
	    adapter != NULL;
	    adapter = adapter->next) {

		/*
		 * Instance name is built from node_name and adapter_name
		 */
		char *buf = NULL;
		if ((adapter->node_name != NULL) &&
		    (adapter->adapter_name != NULL)) {

			int retval;
			buf = (char *)malloc(sizeof (char) *
			    (strlen(adapter->node_name) +
				strlen(adapter->adapter_name) + 2));
			if (buf == NULL)
				goto error;
			/*
			 * Set instance_name_utf to this instance name
			 */
			retval = sprintf(buf, "%s%c%s", adapter->node_name,
			    separator, adapter->adapter_name);
			if (retval < 0) {
				free(buf);
				goto error;
			}
			instance_name_utf = buf;
		} else
			goto error;

		/*
		 * Local reference: need to delete it afterwards
		 */
		instance_name = (*env)->NewStringUTF(env, instance_name_utf);
		free(buf);

		if (instance_name == NULL)
			goto error;

		/*
		 * Add the String we just created to the ArrayList
		 */
		(void) (*env)->CallObjectMethod(env, instance_names_list,
		    array_list_add, instance_name);

		/*
		 * Delete local reference
		 */
		delRef(env, instance_name);
		instance_name = NULL;
	}

	/*
	 * Now convert the ArrayList to a String array before returning
	 */

	/*
	 * Create an empty string array for typing info
	 */
	string_array = (*env)->NewObjectArray(env, 0, string_class, NULL);
	if (string_array == NULL)
		goto error;
	delRef(env, string_class);

	/*
	 * Use toArray to map back to the underlying array
	 */

	return_array = (*env)->CallObjectMethod(env, instance_names_list,
	    array_list_to_array, string_array);

error:

	if (error == CL_QUERY_OK)
		error = cl_query_free_result(CL_QUERY_ADAPTER_INFO, &result);

	delRef(env, string_class);
	delRef(env, array_list_class);
	delRef(env, instance_names_list);
	delRef(env, string_array);

	return (return_array);
} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_transport_ClusterAdapterInterceptor
 * Method:    getAttributes
 * Signature: (Ljava/lang/String;[Ljava/lang/String;
 *            )Ljavax/management/AttributeList;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_transport_ClusterAdapterInterceptor_getAttributes
	(JNIEnv * env,
	jobject this,
	jstring instance_name_string,
	jobjectArray attribute_names) {

	int i;
	uint_t len;

	char *buf = NULL;

	const char *instance_name = NULL;
	const char *attr_name = NULL;

	jsize attribute_names_length;

	jclass attribute_list_class = NULL;
	jobject attribute_list = NULL;
	jmethodID attribute_list_constructor = NULL;
	jmethodID attribute_list_add = NULL;

	jclass attribute_class = NULL;
	jobject attribute = NULL;
	jmethodID attribute_constructor = NULL;

	jobject attribute_name_string = NULL;
	jobject attribute_value = NULL;

	cl_query_result_t result;
	cl_query_adapter_info_t *adapter = NULL;
	cl_query_error_t error = NULL;

	/*
	 * get a C version of our instance name
	 */
	instance_name = (*env)->GetStringUTFChars(env, instance_name_string,
	    NULL);

	/*
	 * Our job is to iterate through the attribute_list array which
	 * contains the String names of attributes, and for each attribute,
	 * add an 'Attribute' object to the 'AttributeList' object that
	 * we return.
	 */

	/*
	 * Create the AttributeList object and initialize references
	 */

	attribute_list_class = (*env)->FindClass(env,
	    "javax/management/AttributeList");
	if (attribute_list_class == NULL)
		goto error;

	/*
	 * Get constructor
	 */
	attribute_list_constructor = (*env)->GetMethodID(env,
	    attribute_list_class, "<init>", "()V");
	if (attribute_list_constructor == NULL)
		goto error;

	/*
	 * Get 'add' method
	 */
	attribute_list_add = (*env)->GetMethodID(env, attribute_list_class,
	    "add", "(Ljavax/management/Attribute;)V");
	if (attribute_list_add == NULL)
		goto error;

	/*
	 * Instantiate one instance
	 */

	attribute_list = (*env)->NewObject(env, attribute_list_class,
	    attribute_list_constructor);
	if (attribute_list == NULL)
		goto error;

	/*
	 * Now get our references to 'attribute_class'
	 */

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * get constructor
	 */
	attribute_constructor = (*env)->GetMethodID(env, attribute_class,
	    "<init>", "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;

	/*
	 * Now iterate through the attribute names one by one, creating *
	 * an 'Attribute' object for each one, which associates the name
	 * with its value
	 */

	attribute_names_length = (*env)->GetArrayLength(env, attribute_names);

	/*
	 * Call SCHA_API with adapter_name without node_name
	 */
	buf = strchr(instance_name, separator);
	if ((buf == NULL) || (strlen(buf) < 2))
		goto error;

	error = cl_query_get_info(CL_QUERY_ADAPTER_INFO, ++buf, &result);

	if (error != CL_QUERY_OK)
		goto error;

	/*
	 * Retrieve the adapter for this node
	 */
	len = strlen(instance_name) - strlen(buf) - 1;
	for (adapter = (cl_query_adapter_info_t *)result.value_list;
	    adapter != NULL; adapter = adapter->next) {

		if (strncmp(instance_name, adapter->node_name, len) == 0)
			break;
	}

	if (adapter == NULL)
		goto error;

	for (i = 0; i < attribute_names_length; i++) {

		attribute_name_string =
		    (*env)->GetObjectArrayElement(env, attribute_names, i);

		if (attribute_name_string == NULL)
			goto error;

		attr_name = (*env)->GetStringUTFChars(env,
		    attribute_name_string, NULL);

		if (attr_name == NULL)
			goto error;

		/*
		 * OK, we've got a C string containing the attribute name,
		 * lets fill out the appropriate value for it
		 */

		if (strcmp(attr_name, "Name") == 0)
			attribute_value = newObjStringParam(env,
			    "java/lang/String", instance_name);

		else if (strcmp(attr_name, "NodeName") == 0)
			attribute_value = newObjStringParam(env,
			    "java/lang/String", (char *)adapter->node_name);

		else if (strcmp(attr_name, "InterfaceName") == 0)
			attribute_value = newObjStringParam(env,
			    "java/lang/String", (char *)adapter->adapter_name);

		else if (strcmp(attr_name, "IpAddress") == 0) {
			if ((adapter->ip_addr != NULL) &&
			    (strlen((char *)adapter->ip_addr))) {
				attribute_value = newObjStringParam(env,
				    "java/lang/String",
				    (char *)adapter->ip_addr);
			} else {
				attribute_value = NULL;
			}

		} else if (strcmp(attr_name, "Ipv6Address") == 0) {
			int s, flags, ret;
			struct lifreq lifrl;
			char ipaddress[_SS_MAXSIZE];

			attribute_value = NULL;
			s = socket(AF_INET6, SOCK_DGRAM, 0);
			if (s != -1) {
				(void) memset(&lifrl, 0, sizeof (lifrl));
				(void) strncpy(lifrl.lifr_name,
					adapter->adapter_name,
					sizeof (lifrl.lifr_name));
				if (ioctl(s, SIOCGLIFADDR,
						(caddr_t)&lifrl) >= 0) {
					if (ioctl(s, SIOCGLIFFLAGS,
						(caddr_t)&lifrl) >= 0) {
						flags = (int)lifrl.lifr_flags;
						if (!(flags & IFF_NOLOCAL) &&
							(flags & IFF_IPV6)) {
						char abuf[INET6_ADDRSTRLEN];
						struct sockaddr_in6 *sin6;
						sin6 = (struct sockaddr_in6 *)
							&lifrl.lifr_addr;
						ret = sprintf(ipaddress,
						"%s/%d", inet_ntop(AF_INET6,
						(void *)&sin6->sin6_addr,
						abuf, sizeof (abuf)),
						lifrl.lifr_addrlen);
						if (ret >= 0) {
							attribute_value =
							newObjStringParam(env,
							"java/lang/String",
							(char *)ipaddress);
						}
					}
					}
				}
				(void) close(s);
			}

		} else if (strcmp(attr_name, "Netmask") == 0) {
			if ((adapter->netmask != NULL) &&
			    (strlen((char *)adapter->netmask))) {
				attribute_value = newObjStringParam(env,
				    "java/lang/String",
				    (char *)adapter->netmask);
			} else {
				attribute_value = NULL;
			}

		} else if (strcmp(attr_name, "Type") == 0)
			attribute_value = newObjStringParam(env,
			    "java/lang/String", (char *)adapter->type);

		else if (strcmp(attr_name, "Attributes") == 0)
			attribute_value = getAttributesList(env, adapter);

		else if (strcmp(attr_name, "Enabled") == 0)
			attribute_value = newBoolean(env,
			    adapter->status == CL_QUERY_ENABLED);

		else if (strcmp(attr_name, "Ports") == 0) {
			if (adapter->port_list.list_len == 0) {
				attribute_value = NULL;
			} else {
				attribute_value = getPortsList(env, adapter);
			}

		} else if (strcmp(attr_name, "ID") == 0)
			attribute_value = newObjInt32Param(env,
			    "java/lang/Integer", (int32_t)adapter->adapter_id);

		else {
			/*
			 * unknown attribute, skip
			 */
			(*env)->ReleaseStringUTFChars(env,
			    attribute_name_string, attr_name);
			continue;
		}

		/*
		 * Now create the Attribute object
		 */

		attribute = (*env)->NewObject(env, attribute_class,
		    attribute_constructor, attribute_name_string,
		    attribute_value);
		if (attribute == NULL)
			goto error;

		/*
		 * and add it to the AttributeList
		 */

		(*env)->CallObjectMethod(env, attribute_list,
		    attribute_list_add, attribute);

		delRef(env, attribute_value);
		attribute_value = NULL;
		delRef(env, attribute);
		attribute = NULL;
		(*env)->ReleaseStringUTFChars(env, attribute_name_string,
		    attr_name);
		attribute_name_string = NULL;
	}

	if (error == CL_QUERY_OK)
		error = cl_query_free_result(CL_QUERY_ADAPTER_INFO, &result);

	(*env)->ReleaseStringUTFChars(env, instance_name_string,
	    instance_name);

	delRef(env, attribute_list_class);
	delRef(env, attribute_class);

	return (attribute_list);

error:

	if (error == CL_QUERY_OK)
		error = cl_query_free_result(CL_QUERY_ADAPTER_INFO, &result);

	(*env)->ReleaseStringUTFChars(env, instance_name_string,
	    instance_name);
	if (attribute_name_string)
		(*env)->ReleaseStringUTFChars(env, attribute_name_string,
		    attr_name);

	delRef(env, attribute_list_class);
	delRef(env, attribute_list);
	delRef(env, attribute_class);
	delRef(env, attribute);

	return (NULL);
} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_transport_ClusterAdapterInterceptor
 * Method:    isRegistered
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL
Java_com_sun_cluster_agent_transport_ClusterAdapterInterceptor_isRegistered
	(JNIEnv * env,
	jobject this,
	jstring instance_name_string) {

	uint_t len;
	const char *instance_name;

	jboolean return_value = JNI_FALSE;

	cl_query_result_t result;
	cl_query_adapter_info_t *adapter = NULL;
	cl_query_error_t error = NULL;
	char *buf;

	/*
	 * Get a C version of our instance name
	 */
	instance_name = (*env)->GetStringUTFChars(env, instance_name_string,
	    NULL);

	/*
	 * Call SCHA_API with adapter_name without node_name
	 */
	buf = strchr(instance_name, separator);
	if ((buf == NULL) || (strlen(buf) < 2))
		goto error;

	error = cl_query_get_info(CL_QUERY_ADAPTER_INFO, ++buf, &result);

	if (error != CL_QUERY_OK)
		goto error;

	/*
	 * Retrieve the adapter for this node
	 */
	len = strlen(instance_name) - strlen(buf) - 1;
	for (adapter = (cl_query_adapter_info_t *)result.value_list;
	    adapter != NULL; adapter = adapter->next) {

		if (strncmp(instance_name, adapter->node_name, len) == 0)
			break;
	}

	if (adapter != NULL)
		return_value = JNI_TRUE;

	if (error == CL_QUERY_OK)
		error = cl_query_free_result(CL_QUERY_ADAPTER_INFO, &result);

error:
	/*
	 * Release our reference to the C version of the name
	 */
	(*env)->ReleaseStringUTFChars(env, instance_name_string,
	    instance_name);

	return (return_value);
} /*lint !e715 */

/*
 * Private helper functions to get an Attributes list, it's quite long,
 * so was separated from getAttributes function
 */
static jobject
getAttributesList(JNIEnv *env, cl_query_adapter_info_t *adapter)
{

	jclass map_class;
	jmethodID map_constructor;
	jmethodID map_put;
	jobject map = NULL;

	map_class =
	    (*env)->FindClass(env,
	    "java/util/HashMap");
	if (map_class == NULL)
		return (NULL);

	/*
	 * Pick up a methodID to the constructor
	 */
	map_constructor =
	    (*env)->GetMethodID(env, map_class, "<init>", "()V");

	/*
	 * Pick up a methodID to the 'put' method
	 */
	map_put = (*env)->GetMethodID(env, map_class, "put",
	    "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");

	/*
	 * Create the map java object
	 */
	map = (*env)->NewObject(env, map_class, map_constructor);

	(void) (*env)->CallObjectMethod(env, map, map_put,
	    newObjStringParam(env, "java/lang/String", "bandwidth"),
	    newObjInt32Param(env, "java/lang/Integer",
		(int32_t)adapter->bandwidth));

	(void) (*env)->CallObjectMethod(env, map, map_put,
	    newObjStringParam(env, "java/lang/String",
		"dlpi_heartbeat_quantum"), newObjInt32Param(env,
		"java/lang/Integer",
		(int32_t)adapter->dlpi_heartbeat_quantum));

	(void) (*env)->CallObjectMethod(env, map, map_put,
	    newObjStringParam(env, "java/lang/String",
		"dlpi_heartbeat_timeout"), newObjInt32Param(env,
		"java/lang/Integer",
		(int32_t)adapter->dlpi_heartbeat_timeout));

	(void) (*env)->CallObjectMethod(env, map, map_put,
	    newObjStringParam(env, "java/lang/String", "lazy_free"),
	    newObjInt32Param(env, "java/lang/Integer",
		(int32_t)adapter->lazy_free));

	(void) (*env)->CallObjectMethod(env, map, map_put,
	    newObjStringParam(env, "java/lang/String", "network_bandwidth"),
	    newObjInt32Param(env, "java/lang/Integer",
		(int32_t)adapter->network_bandwidth));

	(void) (*env)->CallObjectMethod(env, map, map_put,
	    newObjStringParam(env, "java/lang/String", "vlan_id"),
	    newObjInt32Param(env, "java/lang/Integer",
		(int32_t)adapter->vlan_id));

	/*
	 * Delete local refs
	 */
	delRef(env, map_class);

	return (map);
}

/*
 * ----------------------------- CLASS-SPECIFIC CODE -------------------
 */

/*
 * Private helper functions to get a Port list, it's quite long,
 * so was separated from getAttributes function
 */
static jobjectArray
    getPortsList(JNIEnv * env,
    cl_query_adapter_info_t *adapter)
{

	uint_t j = 0;

	jclass port_class;
	jmethodID port_constructor;
	jclass array_list_class;
	jmethodID array_list_constructor;
	jmethodID array_list_add;
	jmethodID array_list_to_array;
	jobjectArray return_array = NULL;

	jobject port_list;
	jclass port = NULL;
	jobjectArray port_array = NULL;

	port_class = (*env)->FindClass(env,
	    "com/sun/cluster/agent/transport/Port");
	if (port_class == NULL)
		return (NULL);
	/*
	 * Pick up a methodID to the constructor
	 */
	port_constructor = (*env)->GetMethodID(env, port_class, "<init>",
	    "(Ljava/lang/String;IZ)V");

	array_list_class = (*env)->FindClass(env, "java/util/ArrayList");
	if (array_list_class == NULL)
		return (NULL);

	/*
	 * Pick up a methodID to the constructor
	 */
	array_list_constructor = (*env)->GetMethodID(env, array_list_class,
	    "<init>", "(I)V");

	/*
	 * Pick up a methodID to the 'add' method
	 */
	array_list_add = (*env)->GetMethodID(env, array_list_class, "add",
	    "(Ljava/lang/Object;)Z");

	/*
	 * Pick up a methodID to the toArray method
	 */
	array_list_to_array = (*env)->GetMethodID(env, array_list_class,
	    "toArray", "([Ljava/lang/Object;)[Ljava/lang/Object;");

	/*
	 * Retrieve ports information
	 */
	port_list = (*env)->NewObject(env, array_list_class,
	    array_list_constructor, adapter->port_list.list_len);

	if (port_list == NULL)
		goto error;

	for (j = 0; j < adapter->port_list.list_len; j++) {

		/*
		 * Create the corresponding java object
		 */
		port = (*env)->NewObject(env, port_class, port_constructor,
		    newObjStringParam(env, "java/lang/String",
			adapter->port_list.list_val[j].port_name),
		    adapter->port_list.list_val[j].port_id,
		    adapter->port_list.list_val[j].port_state);

		if (port != NULL) {
			/*
			 * Add to the ArrayList our NodePort object
			 */
			(void) (*env)->CallObjectMethod(env, port_list,
			    array_list_add, port);
		}
	}

	/*
	 * Now convert the ArrayList to a NodePort array before returning
	 */

	/*
	 * Create an empty array for typing info
	 */
	port_array = (*env)->NewObjectArray(env, 0, port_class, NULL);
	if (port_array == NULL)
		goto error;

	/*
	 * Use toArray to map back to the underlying array
	 */
	return_array = (*env)->CallObjectMethod(env, port_list,
	    array_list_to_array, port_array);

	/*
	 * Now clean up
	 */
error:
	/*
	 * Delete local refs
	 */
	delRef(env, port_class);
	delRef(env, array_list_class);
	delRef(env, port_list);
	if (port != NULL)
		delRef(env, port);
	delRef(env, port_array);

	return (return_array);
}
/*
 * ---------------------- END-OF-CLASS-SPECIFIC CODE -------------------
 */
