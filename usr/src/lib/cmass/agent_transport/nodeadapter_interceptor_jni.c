/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Adapter mbean interceptor
 */

#pragma ident	"@(#)nodeadapter_interceptor_jni.c	1.6	08/05/20 SMI"

#include "com/sun/cluster/agent/transport/NodeAdapterInterceptor.h"
#include <jniutil.h>
#include <string.h>
#include <sys/utsname.h>
#include <errno.h>
#include <libdevinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/sockio.h>
#include <sys/types.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/sol_version.h>

/*
 * These really belong in a header file
 */
#define	INET_DEFAULT_FILE	"/etc/default/inet_type"
#define	DEFAULT_IP		"DEFAULT_IP"
#define	_B_TRUE			0x000000001
#define	_B_FALSE		0x000000000

extern void Perror0(char *);

/*
 * CMASS: definitions used in ifconfig.c but not found in header files
 */
static char *getdefault(char *name);

/*
 * CMASS: new definitions
 */
struct attribute {
	char name[LIFNAMSIZ];
	boolean_t plumbed;
	boolean_t logical;
	int flags;
	char groupname[LIFNAMSIZ];
	char ipaddress[_SS_MAXSIZE];
	char netmask[_SS_MAXSIZE];
	char broadcast[_SS_MAXSIZE];
};
struct nw_interface {
	char name[LIFNAMSIZ];
	struct nw_interface *next;
};

static int devfs_entry(di_node_t node, di_minor_t minor, void *arg);
static int free_nw_interface(struct nw_interface **nifdtp);
static int getAllInstances(struct nw_interface **nifdt);
static int getConfiguredInterfaces(struct lifconf *lifc);
static int getInterfaceAttributes(char *instance,
    struct attribute **attr);
static int getNetworkInterfaces(struct nw_interface **nwifp);
static int isConfiguredInterfaces(char *instance);
static int isInterfaceRegistered(char *instance);
static int isNetworkInterfaces(char *instance);

/*
 * Data structures and defines for device tree
 */
static const char *class = DDI_NT_NET;

static int get_compat_flag(char **);

struct net_interface {
	char *type;
	char *name;
};

#define	ni_set_type(i, s)	{ \
	if ((i)->type) \
		free((i)->type); \
	(i)->type = strdup((char *)s); \
}
#define	ni_get_type(i)	((i)->type)

#define	ni_set_name(i, s)	{ \
	if ((i)->name) \
		free((i)->name); \
	(i)->name = strdup(s); \
}
#define	ni_get_name(i)	((i)->name)

#define	ni_new(i)	{ \
	(i) = (struct net_interface *)malloc(sizeof (struct net_interface)); \
	if ((i) == (struct net_interface *)0) { \
		(void) fprintf(stderr,  \
		    "Malloc failure for net_interface\n"); \
		return (-1); \
	} \
	(void) memset((i), 0, sizeof (struct net_interface)); \
}

#define	ni_destroy(i) { \
	if ((i)->name != 0) \
		free((i)->name); \
	if ((i)->type != 0) \
		free((i)->type); \
	if ((i) != (struct net_interface *)0) \
		free((char *)(i)); \
}

struct ni_list {
	struct net_interface *nifp;
	struct ni_list *next;
};

/*
 * Access functions for nif_list struct
 */
#define	first_if(a)	(a)
#define	get_nifp(a)	((a)->nifp)
#define	set_nifp(a, i)	((a)->nifp = (i))
#define	next_if(a)	((a)->next)

#define	nil_new(i)	{ \
	(i) = (struct ni_list *)malloc(sizeof (struct ni_list)); \
	if ((i) == (struct ni_list *)0) { \
		(void) fprintf(stderr,  \
		    "Malloc failure for ni_list\n"); \
		return (-1); \
	} \
	(void) memset((i), 0, sizeof (struct ni_list)); \
}

#define	nil_destroy(i) { \
	if ((i) != (struct ni_list *)0) \
		free((char *)(i)); \
}

#define	NIL_NULL	((struct ni_list *)0)

/*
 * End defines and structure definitions for device tree
 */

/*
 * Name attribute delimiter: between node name and adapter name
 */
const char delimiter = ':';


/*
 * Class:     com_sun_cluster_agent_transport_NodeAdapterInterceptor
 * Method:    getInstances
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL
Java_com_sun_cluster_agent_transport_NodeAdapterInterceptor_getInstances
	(JNIEnv * env, jobject this) {

	int retval;

	const char *instance_name_utf;

	jobject instance_name = NULL;
	jclass array_list_class = NULL;
	jmethodID array_list_constructor;
	jmethodID array_list_add;
	jmethodID array_list_to_array;

	jclass string_class = NULL;
	jobjectArray string_array = NULL;

	jobject instance_names_list = NULL;

	jobjectArray return_array = NULL;

	struct nw_interface *nifdt = NULL;
	struct nw_interface *head = NULL;
	struct utsname *name = NULL;

	/*
	 * We must return a Java array of Strings containing the names of
	 * all the instances of our object type
	 */

	/*
	 * Local reference: need to delete it afterwards
	 */
	string_class = (*env)->FindClass(env, "java/lang/String");
	if (string_class == NULL)
		goto error;

	/*
	 * Local reference: need to delete afterwards
	 */
	array_list_class = (*env)->FindClass(env, "java/util/ArrayList");
	if (array_list_class == NULL)
		goto error;

	/*
	 * Pick up a methodID to the constructor
	 */
	array_list_constructor = (*env)->GetMethodID(env, array_list_class,
	    "<init>", "()V");

	/*
	 * Pick up a methodID to the 'add' method
	 */
	array_list_add = (*env)->GetMethodID(env, array_list_class,
	    "add", "(Ljava/lang/Object;)Z");

	/*
	 * Pick up a methodID to the toArray method
	 */
	array_list_to_array = (*env)->GetMethodID(env, array_list_class,
	    "toArray", "([Ljava/lang/Object;)[Ljava/lang/Object;");

	/*
	 * Create the empty ArrayList
	 */
	instance_names_list = (*env)->NewObject(env, array_list_class,
	    array_list_constructor);
	delRef(env, array_list_class);
	if (instance_names_list == NULL)
		goto error;

	/*
	 * Now fill it with instance names
	 */

	/*
	 * Local node is included in the instance name
	 */
	name = (struct utsname *)malloc(sizeof (struct utsname));
	if (uname(name) < 0) {
		Perror0("uname");
		goto error;
	}
	/*
	 * Get instances
	 */
	if (getAllInstances(&nifdt) < 0)
		goto error;

	/*
	 * Process each instance
	 */
	head = nifdt;
	while (nifdt != NULL) {

		char *buf = NULL;

		/*
		 * Instance name is built from node name and adapter name
		 */
		if ((name->nodename != NULL) && (nifdt->name != NULL)) {
			buf = (char *)malloc(sizeof (char) * (strlen(
				    name->nodename) + strlen(nifdt->name) + 2));
		}
		if (buf == NULL) {
			Perror0("malloc");
			goto error;
		}
		retval = sprintf(buf, "%s%c%s", name->nodename, delimiter,
		    nifdt->name);
		if (retval < 0) {
			free(buf);
			goto error;
		}
		nifdt = nifdt->next;

		/*
		 * Set instance_name_utf to this instance name
		 */
		instance_name_utf = buf;

		/*
		 * Local reference: need to delete it afterwards
		 */
		instance_name = (*env)->NewStringUTF(env, instance_name_utf);
		free(buf);

		if (instance_name == NULL)
			goto error;

		/*
		 * Add the String we just created to the ArrayList
		 */
		(void) (*env)->CallObjectMethod(env, instance_names_list,
		    array_list_add, instance_name);

		/*
		 * Delete local reference
		 */
		delRef(env, instance_name);
		instance_name = NULL;
	}

	/*
	 * Now convert the ArrayList to a String array before returning
	 */

	/*
	 * Create an empty string array for typing info
	 */
	string_array = (*env)->NewObjectArray(env, 0, string_class, NULL);
	if (string_array == NULL)
		goto error;
	delRef(env, string_class);

	/*
	 * Use toArray to map back to the underlying array
	 */

	return_array = (*env)->CallObjectMethod(env, instance_names_list,
	    array_list_to_array, string_array);

    error:

	/*
	 * Free structure allocated for uname
	 */
	if (name != NULL)
		free(name);

	/*
	 * Free nw_interface structures
	 */
	retval = free_nw_interface(&head);
	head = NULL;

	delRef(env, string_class);
	delRef(env, array_list_class);
	delRef(env, instance_names_list);
	delRef(env, string_array);

	return (return_array);
} /*lint !e715 */


/*
 * Class:     com_sun_cluster_agent_transport_NodeAdapterInterceptor
 * Method:    getAttributes
 * Signature: (Ljava/lang/String;[Ljava/lang/String;
 *            )Ljavax/management/AttributeList;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_transport_NodeAdapterInterceptor_getAttributes
	(JNIEnv * env,
	jobject this,
	jstring instance_name_string,
	jobjectArray attribute_names) {

	int i;

	char *buf = NULL;
	struct attribute *attr = NULL;

	const char *instance_name = NULL;
	const char *attr_name = NULL;

	jsize attribute_names_length;

	jclass attribute_list_class = NULL;
	jobject attribute_list = NULL;
	jmethodID attribute_list_constructor = NULL;
	jmethodID attribute_list_add = NULL;

	jclass attribute_class = NULL;
	jobject attribute = NULL;
	jmethodID attribute_constructor = NULL;

	jobject attribute_name_string = NULL;
	jobject attribute_value = NULL;

	/*
	 * get a C version of our instance name
	 */
	instance_name = (*env)->GetStringUTFChars(env, instance_name_string,
	    NULL);

	/*
	 * Our job is to iterate through the attribute_list array which
	 * contains the String names of attributes, and for each attribute,
	 * add an 'Attribute' object to the 'AttributeList' object that
	 * we return.
	 */

	/*
	 * Create the AttributeList object and initialize references
	 */

	attribute_list_class = (*env)->FindClass(env,
	    "javax/management/AttributeList");
	if (attribute_list_class == NULL)
		goto error;

	/*
	 * Get constructor
	 */
	attribute_list_constructor = (*env)->GetMethodID(env,
	    attribute_list_class, "<init>", "()V");
	if (attribute_list_constructor == NULL)
		goto error;

	/*
	 * Get 'add' method
	 */
	attribute_list_add = (*env)->GetMethodID(env, attribute_list_class,
	    "add", "(Ljavax/management/Attribute;)V");
	if (attribute_list_add == NULL)
		goto error;

	/*
	 * Instantiate one instance
	 */

	attribute_list = (*env)->NewObject(env, attribute_list_class,
	    attribute_list_constructor);
	if (attribute_list == NULL)
		goto error;

	/*
	 * Now get our references to 'attribute_class'
	 */

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * get constructor
	 */
	attribute_constructor = (*env)->GetMethodID(env, attribute_class,
	    "<init>", "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;

	/*
	 * Now iterate through the attribute names one by one, creating *
	 * an 'Attribute' object for each one, which associates the name
	 * with its value
	 */

	attribute_names_length = (*env)->GetArrayLength(env, attribute_names);

	/*
	 * Call getInterfaceAttributes() without node name
	 */
	buf = strchr(instance_name, delimiter);
	if ((buf == NULL) || (strlen(buf) < 2))
		goto error;

	if (getInterfaceAttributes(++buf, &attr) < 0)
		goto error;

	for (i = 0; i < attribute_names_length; i++) {

		attribute_name_string =
		    (*env)->GetObjectArrayElement(env, attribute_names, i);

		if (attribute_name_string == NULL)
			goto error;

		attr_name = (*env)->GetStringUTFChars(env,
		    attribute_name_string, NULL);

		if (attr_name == NULL)
			goto error;

		/*
		 * OK, we've got a C string containing the attribute name,
		 * lets fill out the appropriate value for it
		 */

		if (strcmp(attr_name, "Name") == 0)
			attribute_value = newObjStringParam(env,
			    "java/lang/String", instance_name);

		else if (strcmp(attr_name, "Plumbed") == 0)
			attribute_value = newBoolean(env, attr->plumbed);

		else if (strcmp(attr_name, "Logical") == 0)
			attribute_value = newBoolean(env, attr->logical);

		else if (strcmp(attr_name, "NodeName") == 0) {

			struct utsname *name = NULL;
			name = (struct utsname *)malloc(
			    sizeof (struct utsname));
			if (uname(name) < 0) {
				Perror0("uname");
				if (name != NULL)
					free(name);
				(*env)->ReleaseStringUTFChars(env,
				    attribute_name_string, attr_name);
				goto error;
			}
			attribute_value = newObjStringParam(env,
			    "java/lang/String", name->nodename);
			if (name != NULL)
				free(name);

		} else if (strcmp(attr_name, "InterfaceName") == 0)
			attribute_value = newObjStringParam(env,
			    "java/lang/String", (char *)attr->name);

		/*
		 * XXX Adapter type for NodeAdapterMBean
		 */
		else if (strcmp(attr_name, "Type") == 0)
			attribute_value = NULL;

		/*
		 * XXX Attributes for nodeadapter
		 */
		else if (strcmp(attr_name, "Attributes") == 0) {
			jclass map_class;
			jmethodID map_constructor;
			map_class = (*env)->FindClass(env,
			    "java/util/HashMap");
			map_constructor = (*env)->GetMethodID(env, map_class,
			    "<init>", "()V");
			attribute_value = (*env)->NewObject(env, map_class,
			    map_constructor);

		/*
		 * Not plumbed: no other attribut values available
		 */
		} else if (!attr->plumbed) {

			(*env)->ReleaseStringUTFChars(env,
			    attribute_name_string, attr_name);
			continue;

		} else if (strcmp(attr_name, "IpAddress") == 0)
			attribute_value = newObjStringParam(env,
			    "java/lang/String",
			    (char *)attr->ipaddress);

		else if (strcmp(attr_name, "Netmask") == 0)
			if (attr->flags & IFF_IPV4) {
				attribute_value = newObjStringParam(env,
				    "java/lang/String", (char *)attr->netmask);
			} else {
				attribute_value = NULL;
			}

		else if (strcmp(attr_name, "Broadcast") == 0)
			if ((attr->flags & IFF_IPV4) &&
			    (attr->broadcast != NULL) &&
			    (strlen((char *)attr->broadcast))) {
				attribute_value = newObjStringParam(env,
				    "java/lang/String",
				    (char *)attr->broadcast);
			} else {
				attribute_value = NULL;
			}

		else if (strcmp(attr_name, "IpmpGroup") == 0)
			if ((attr->groupname != NULL) &&
			    (strlen(attr->groupname)))
				attribute_value = newObjStringParam(env,
				    "java/lang/String", attr->groupname);
			else {
				attribute_value = NULL;
			}

		else if (strcmp(attr_name, "FlagUP") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_UP));

		else if (strcmp(attr_name, "FlagBROADCAST") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_BROADCAST));

		else if (strcmp(attr_name, "FlagDEBUG") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_DEBUG));

		else if (strcmp(attr_name, "FlagLOOPBACK") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_LOOPBACK));

		else if (strcmp(attr_name, "FlagPOINTOPOINT") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_POINTOPOINT));

		else if (strcmp(attr_name, "FlagNOTRAILERS") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_NOTRAILERS));

		else if (strcmp(attr_name, "FlagRUNNING") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_RUNNING));

		else if (strcmp(attr_name, "FlagNOARP") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_NOARP));

		else if (strcmp(attr_name, "FlagPROMISC") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_PROMISC));

		else if (strcmp(attr_name, "FlagALLMULTI") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_ALLMULTI));

		else if (strcmp(attr_name, "FlagINTELLIGENT") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_INTELLIGENT));

		else if (strcmp(attr_name, "FlagMULTICAST") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_MULTICAST));

		else if (strcmp(attr_name, "FlagMULTI_BCAST") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_MULTI_BCAST));

		else if (strcmp(attr_name, "FlagUNNUMBERED") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_UNNUMBERED));

		else if (strcmp(attr_name, "FlagDHCPRUNNING") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_DHCPRUNNING));

		else if (strcmp(attr_name, "FlagPRIVATE") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_PRIVATE));

		else if (strcmp(attr_name, "FlagNOXMIT") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_NOXMIT));

		else if (strcmp(attr_name, "FlagNOLOCAL") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_NOLOCAL));

		else if (strcmp(attr_name, "FlagDEPRECATED") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_DEPRECATED));

		else if (strcmp(attr_name, "FlagADDRCONF") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_ADDRCONF));

		else if (strcmp(attr_name, "FlagROUTER") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_ROUTER));

		else if (strcmp(attr_name, "FlagNONUD") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_NONUD));

		else if (strcmp(attr_name, "FlagANYCAST") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_ANYCAST));

		else if (strcmp(attr_name, "FlagNORTEXCH") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_NORTEXCH));

		else if (strcmp(attr_name, "FlagIPV4") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_IPV4));

		else if (strcmp(attr_name, "FlagIPV6") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_IPV6));

#if SOL_VERSION <= __s10
		else if (strcmp(attr_name, "FlagMIPRUNNING") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_MIPRUNNING));
#endif

		else if (strcmp(attr_name, "FlagNOFAILOVER") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_NOFAILOVER));

		else if (strcmp(attr_name, "FlagFAILED") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_FAILED));

		else if (strcmp(attr_name, "FlagSTANDBY") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_STANDBY));

		else if (strcmp(attr_name, "FlagINACTIVE") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_INACTIVE));

		else if (strcmp(attr_name, "FlagOFFLINE") == 0)
			attribute_value = newBoolean(env,
			    (attr->flags & IFF_OFFLINE));

		else {
			/*
			 * unknown attribute, skip
			 */
			(*env)->ReleaseStringUTFChars(env,
			    attribute_name_string, attr_name);
			continue;
		}

		/*
		 * Now create the Attribute object
		 */

		attribute = (*env)->NewObject(env, attribute_class,
		    attribute_constructor, attribute_name_string,
		    attribute_value);
		if (attribute == NULL)
			goto error;

		/*
		 * and add it to the AttributeList
		 */

		(*env)->CallObjectMethod(env, attribute_list,
		    attribute_list_add, attribute);

		delRef(env, attribute_value);
		attribute_value = NULL;
		delRef(env, attribute);
		attribute = NULL;
		(*env)->ReleaseStringUTFChars(env, attribute_name_string,
		    attr_name);
		attribute_name_string = NULL;
	}

	(*env)->ReleaseStringUTFChars(env, instance_name_string,
	    instance_name);

	delRef(env, attribute_list_class);
	delRef(env, attribute_class);

	if (attr != NULL)
		free(attr);

	return (attribute_list);

    error:

	if (attr != NULL)
		free(attr);

	(*env)->ReleaseStringUTFChars(env, instance_name_string,
	    instance_name);
	if (attribute_name_string)
		(*env)->ReleaseStringUTFChars(env, attribute_name_string,
		    attr_name);

	delRef(env, attribute_list_class);
	delRef(env, attribute_list);
	delRef(env, attribute_class);
	delRef(env, attribute);

	return (NULL);
}  /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_transport_NodeAdapterInterceptor
 * Method:    isRegistered
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL
Java_com_sun_cluster_agent_transport_NodeAdapterInterceptor_isRegistered
	(JNIEnv * env,
	jobject this,
	jstring instance_name_string) {

	int retval;
	char *buf;
	const char *instance_name;

	jboolean return_value = JNI_FALSE;

	/*
	 * Get a C version of our instance name
	 */
	instance_name = (*env)->GetStringUTFChars(env, instance_name_string,
	    NULL);

	/*
	 * Call isInterfaceRegistered() without without node name
	 */
	buf = strchr(instance_name, delimiter);
	if ((buf == NULL) || (strlen(buf) < 2))
		goto error;
	retval = isInterfaceRegistered(++buf);
	if (retval < 0)
		goto error;
	if (retval)
		return_value = JNI_TRUE;

    error:
	/*
	 * Release our reference to the C version of the name
	 */
	(*env)->ReleaseStringUTFChars(env, instance_name_string,
	    instance_name);

	return (return_value);
}  /*lint !e715 */


/*
 * Free the nw_interface structure provided as input parameter.
 * Returns 0.
 */
static int
    free_nw_interface(struct nw_interface **nifdtp)
{
	struct nw_interface *head = NULL;
	struct nw_interface *tmp = NULL;

	/*
	 * Process each instance
	 */
	head = *nifdtp;

	/*
	 * Free instances
	 */
	while (head != NULL) {

		tmp = head->next;
		free(head);
		head = tmp;
	}

	return (0);
}


/*
 * Retrieve all interfaces except loopbacks:
 * - network interfaces retrieved form device tree (even unplumbed)
 * - logical interfaces
 * Result is stored in the input parameter.
 * Returns 0 if successful, -1 otherwise
 */
static int
    getAllInstances(struct nw_interface **nifdtp)
{
	int n;
	struct nw_interface *nwi = NULL;
	struct lifconf lifc;
	struct lifreq *lifrp = NULL;

	/*
	 * Get Network Interfaces
	 */
	if (getNetworkInterfaces(&nwi) < 0) {
		(void) fprintf(stderr, "Can't get Network Interfaces\n");
		return (-1);
	}
	/*
	 * Get configured interfaces
	 */
	if (getConfiguredInterfaces(&lifc) < 0) {
		(void) fprintf(stderr, "Can't get Configured Interfaces\n");
		if (lifc.lifc_buf != NULL)
			free(lifc.lifc_buf);
		return (-1);
	}
	/*
	 * Go through configured interfaces to add those not in device *
	 * tree
	 */
	lifrp = lifc.lifc_req;
	/* We don't want to mix signed-unsigned with divide */
	for (n = lifc.lifc_len / (int)sizeof (struct lifreq); n > 0;
	    n--, lifrp++) {

		/*
		 * Check if we already got it from the device tree
		 */
		struct nw_interface *tmp = NULL;
		struct nw_interface *previous = NULL;
		tmp = nwi;
		while (tmp != NULL) {

			if (strncmp(lifrp->lifr_name, tmp->name, LIFNAMSIZ)
			    == 0)
				break;

			previous = tmp;
			tmp = tmp->next;
		}

		/*
		 * We didn't find it in device tree, so it is a logical *
		 * one
		 */
		if (tmp == NULL) {

			struct nw_interface *new = NULL;

			/*
			 * Create a new instance record
			 */
			new = (struct nw_interface *)malloc(
			    sizeof (struct nw_interface));
			if (new == NULL) {
				Perror0("malloc");
				if (lifc.lifc_buf != NULL)
					free(lifc.lifc_buf);
				return (-1);
			}
			(void) strncpy(new->name, lifrp->lifr_name, LIFNAMSIZ);

			new->next = NULL;
			if (previous != NULL) {
				previous->next = new;
				previous = new;
			} else if (nwi == NULL) {
				nwi = new;
				previous = new;
			}
		}
	}
	*nifdtp = nwi;
	if (lifc.lifc_buf != NULL)
		free(lifc.lifc_buf);
	return (0);
}


/*
 * Retrieve all network interfaces from device tree (even unplumbed)
 * Result is stored in the input parameter.
 * Returns 0 if successful, -1 otherwise
 */
static int
    getNetworkInterfaces(struct nw_interface **nwifp)
{

	int n;
	struct ni_list *nilp = NULL;
	struct nw_interface *current = NULL;
	struct ni_list *nil_head = NIL_NULL;

	/*
	 * Look through the kernel's devinfo tree for
	 * network devices
	 */
	di_node_t root;
	if ((root = di_init("/", DINFOSUBTREE | DINFOMINOR))
	    == DI_NODE_NIL) {
		(void) fprintf(stderr, "di_init devinfo driver failed.\n");
		return (-1);
	}
	(void) di_walk_minor(root, class, DI_CHECK_ALIAS, (void *)(&nil_head),
	    devfs_entry);
	di_fini(root);

	/*
	 * Now, translate the linked list into
	 * a struct lifreq buffer
	 */
	nilp = first_if(nil_head);
	while (nilp != NIL_NULL) {

		struct nw_interface *new = NULL;
		struct net_interface *nip = NULL;

		nip = get_nifp(nilp);
		new = (struct nw_interface *)malloc(
		    sizeof (struct nw_interface));
		if (new == NULL) {
			Perror0("malloc");
			return (-1);
		}
		(void) strncpy(new->name, ni_get_name(nip), LIFNAMSIZ);

		if (nilp == nil_head)
			*nwifp = new;
		else
			current->next = new;
		current = new;
		nilp = next_if(nilp);
	}
	if (current != NULL)
		current->next = NULL;

	/*
	 * Now clean up the network interface list
	 */
	nilp = first_if(nil_head);
	while (nilp != NIL_NULL) {
		struct net_interface *nip = NULL;
		struct ni_list *onilp = NULL;
		nip = get_nifp(nilp);
		ni_destroy(nip);
		onilp = nilp;
		nilp = next_if(nilp);
		nil_destroy(onilp);
	}
	nil_head = NIL_NULL;
	return (0);
}


/*
 * Look if the given interface exists in device tree
 * Returns 1 is found, 0 if not, -1 in case of failure.
 */
static int
    isNetworkInterfaces(char *instance)
{
	int n, retval;
	struct ni_list *nilp = NULL;
	struct ni_list *nil_head = NIL_NULL;

	/*
	 * Look through the kernel's devinfo tree for
	 * network devices
	 */
	di_node_t root;
	if ((root = di_init("/", DINFOSUBTREE | DINFOMINOR))
	    == DI_NODE_NIL) {
		(void) fprintf(stderr, "di_init devinfo driver failed.\n");
		return (-1);
	}
	(void) di_walk_minor(root, class, DI_CHECK_ALIAS, (void *)&nil_head,
	    devfs_entry);
	di_fini(root);

	/*
	 * Now, translate the linked list into
	 * a struct lifreq buffer
	 */
	retval = 0;
	nilp = first_if(nil_head);
	while (nilp != NIL_NULL) {

		struct net_interface *nip = NULL;
		nip = get_nifp(nilp);

		if (strncmp(instance, ni_get_name(nip), LIFNAMSIZ) == 0) {
			retval = 1;
			break;
		}
		nilp = next_if(nilp);
	}

	/*
	 * Now clean up the network interface list
	 */
	nilp = first_if(nil_head);
	while (nilp != NIL_NULL) {
		struct net_interface *nip = NULL;
		struct ni_list *onilp = NULL;
		nip = get_nifp(nilp);
		ni_destroy(nip);
		onilp = nilp;
		nilp = next_if(nilp);
		nil_destroy(onilp);
	}
	nil_head = NIL_NULL;
	return (retval);
}


/*
 * Retrieve all configured interfaces (except loopback)
 * Result is stored in the input parameter.
 * Returns 0 if successful, -1 otherwise
 */
static int
    getConfiguredInterfaces(struct lifconf *lifc)
{
	int s, numifs, bufsize;
	int af = AF_INET;
	int v4compat = 0;
	char *buf = NULL;
	char *default_ip_str = NULL;
	struct lifnum lifn;
	int lifc_flags = LIFC_NOXMIT;

	v4compat = get_compat_flag(&default_ip_str);
	if (v4compat < 0) {
		if (v4compat == -1) {
			(void) fprintf(stderr,
			    "%s: DEFAULT_IP environment variable bad value",
			    default_ip_str);
		} else if (v4compat == -2) {
			(void) fprintf(stderr,
			    "%s: DEFAULT_IP bad value in %s\n",
			    default_ip_str, INET_DEFAULT_FILE);
		}
		if (default_ip_str != NULL)
			free(default_ip_str);
		return (-1);
	}
	if (default_ip_str != NULL)
		free(default_ip_str);

	s = socket(af, SOCK_DGRAM, 0);
	if (s < 0) {
		Perror0("socket");
		return (-1);
	}
	/*
	 * Get the number of configured interfaces for buffer allocation
	 */
	lifn.lifn_family = AF_UNSPEC;
	lifn.lifn_flags = lifc_flags;
	if (ioctl(s, SIOCGLIFNUM, (char *)&lifn) < 0) {
		Perror0("SIOCGLIFNUM");
		(void) close(s);
		return (-1);
	}
	numifs = lifn.lifn_count;
	bufsize = numifs * (int)sizeof (struct lifreq);
	buf = malloc((uint_t)bufsize);
	if (buf == NULL) {
		Perror0("malloc\n");
		(void) close(s);
		return (-1);
	}
	/*
	 * Performs the ifconfig ioctl
	 */
	lifc->lifc_family = AF_UNSPEC;
	lifc->lifc_flags = lifc_flags;
	lifc->lifc_len = bufsize;
	lifc->lifc_buf = buf;
	if (ioctl(s, SIOCGLIFCONF, (char *)lifc) < 0) {
		Perror0("SIOCGLIFCONF");
		(void) close(s);
		if (buf != NULL)
			free(buf);
		return (-1);
	}
	/*
	 * We must close and recreate the socket each time
	 * since we don't know what type of socket it is now
	 * (each status function may change it).
	 */
	(void) close(s);

	return (0);
}


/*
 * Look if the given interface exists as a configured interface
 * Returns 1 is found, 0 if not, -1 in case of failure.
 */
static int
    isConfiguredInterfaces(char *instance)
{
	int n, retval = 0;
	struct lifconf lifc;
	struct lifreq *lifrp = NULL;

	/*
	 * Get configured interfaces
	 */
	if (getConfiguredInterfaces(&lifc) < 0) {
		(void) fprintf(stderr, "Can't get Configured Interfaces\n");
		if (lifc.lifc_buf != NULL)
			free(lifc.lifc_buf);
		return (-1);
	}
	/*
	 * Built instances list
	 */
	lifrp = lifc.lifc_req;
	/* We don't want to mix signed-unsigned with divide */
	for (n = lifc.lifc_len / (int)sizeof (struct lifreq); n > 0;
	    n--, lifrp++) {

		/*
		 * This one is configured
		 */
		if (strncmp(instance, lifrp->lifr_name, LIFNAMSIZ) == 0)
			retval = 1;
	}

	if (lifc.lifc_buf != NULL)
		free(lifc.lifc_buf);
	return (retval);
}


/*
 * Get the interface attributes.
 * Return 1 if true, 0 if false
 */
static int
    getInterfaceAttributes(char *instance, struct attribute **attr)
{
	int n, s, retval;
	int af = AF_INET;
	int flags;
	char *name = NULL;
	struct lifconf lifc;
	struct lifreq lifrl;
	struct lifreq *lifrp = NULL;
	struct attribute *new = NULL;

	/*
	 * Get configured interfaces
	 */
	if (getConfiguredInterfaces(&lifc) < 0) {
		(void) fprintf(stderr, "Can't get Configured Interfaces\n");
		if (lifc.lifc_buf != NULL)
			free(lifc.lifc_buf);
		return (-1);
	}
	lifrp = lifc.lifc_req;
	/* We don't want to mix signed-unsigned with divide */
	for (n = lifc.lifc_len / (int)sizeof (struct lifreq); n > 0;
	    n--, lifrp++) {

		/*
		 * Check if current interface is our target instance
		 */
		if (strncmp(instance, lifrp->lifr_name, LIFNAMSIZ))
			continue;

		/*
		 * We found our target network interface
		 */
		break;
	}

	/*
	 * Retrieve the information: physical interface or not
	 */
	retval = isNetworkInterfaces(instance);
	if (retval < 0) {
		(void) fprintf(stderr, "isNetworkInterfaces() failed\n");
		if (lifc.lifc_buf != NULL)
			free(lifc.lifc_buf);
		*attr = NULL;
		return (-1);
	}
	/*
	 * If this interface is not configured, it must be unplumbed
	 */
	if (n == 0) {

		/*
		 * Not a physical interface either, so it doesn't exist
		 */
		if (!retval) {
			if (lifc.lifc_buf != NULL)
				free(lifc.lifc_buf);
			*attr = NULL;
			return (-1);
		}
		/*
		 * Found it, so this interface is unplumbed
		 */
		new = (struct attribute *)malloc(sizeof (struct attribute));
		if (new == NULL) {
			Perror0("malloc");
			if (lifc.lifc_buf != NULL)
				free(lifc.lifc_buf);
			*attr = NULL;
			return (-1);
		}
		(void) memset(new, 0, sizeof (*new));
		(void) strncpy(new->name, instance, LIFNAMSIZ);
		new->plumbed = _B_FALSE;
		new->logical = _B_FALSE;
		if (lifc.lifc_buf != NULL)
			free(lifc.lifc_buf);
		*attr = new;
		return (0);
	}
	/*
	 * Re-open a socket with the right family and retrieve information
	 *
	 */
	af = lifrp->lifr_addr.ss_family;
	if (af == AF_UNSPEC) {
		af = AF_INET;
	}
	s = socket(af, SOCK_DGRAM, 0);
	if (s == -1) {
		Perror0("socket");
		if (lifc.lifc_buf != NULL)
			free(lifc.lifc_buf);
		*attr = NULL;
		return (-1);
	}
	(void) memset(&lifrl, 0, sizeof (lifrl));
	(void) strncpy(lifrl.lifr_name, lifrp->lifr_name,
	    sizeof (lifrl.lifr_name));
	if (ioctl(s, SIOCGLIFADDR, (caddr_t)&lifrl) < 0) {
		Perror0("SIOCGLIFADDR");
		if (lifc.lifc_buf != NULL)
			free(lifc.lifc_buf);
		(void) close(s);
		*attr = NULL;
		return (-1);
	}
	if (ioctl(s, SIOCGLIFFLAGS, (caddr_t)&lifrl) < 0) {
		Perror0("SIOCGLIFFLAGS");
		if (lifc.lifc_buf != NULL)
			free(lifc.lifc_buf);
		(void) close(s);
		*attr = NULL;
		return (-1);
	}
	/*
	 * Initialize a new record
	 */
	new = (struct attribute *)malloc(sizeof (struct attribute));
	if (new == NULL) {
		Perror0("malloc");
		if (lifc.lifc_buf != NULL)
			free(lifc.lifc_buf);
		*attr = NULL;
		return (-1);
	}
	/*
	 * Interface name is a MBean attribut
	 */
	name = lifrl.lifr_name;
	if (name != NULL) {
		(void) strncpy(new->name, name, LIFNAMSIZ);
		new->name[strlen(name)] = '\0';
	}
	/*
	 * Flags will be computed to build attributs
	 */
	flags = (int)lifrl.lifr_flags;
	new->flags = flags;

	/*
	 * At this point, we know that this is a configured interface
	 */
	new->plumbed = _B_TRUE;

	/*
	 * At this point, we know if this is a logical interface
	 */
	if (retval)
		new->logical = _B_FALSE;
	else
		new->logical = _B_TRUE;

	/*
	 * If there is a groupname, print it (same for IPv4 and IPv6).
	 */
	/*
	 * There is no groupname for logical adapters like hme0:1
	 */
	(void) memset(new->groupname, 0, LIFNAMSIZ);
	if ((name != NULL) && (strchr(name, ':') == NULL)) {

		struct lifreq lifr;

		/*
		 * There is not always a groupname
		 */
		(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
		if (ioctl(s, SIOCGLIFGROUPNAME, (caddr_t)&lifr) >= 0) {

			if ((lifr.lifr_groupname != NULL) &&
			    (strlen(lifr.lifr_groupname) > 0)) {
				(void) strncpy(new->groupname,
				    lifr.lifr_groupname, LIFNAMSIZ);
				new->groupname[strlen(lifr.lifr_groupname)]
				    = '\0';
			}
		}
	}
	/*
	 * IP Address only if NOLOCAL
	 */
	(void) memset(new->ipaddress, 0, _SS_MAXSIZE);
	if (!(flags & IFF_NOLOCAL)) {

		struct lifreq lifr;
		if (name == NULL) {
			Perror0("name NULL");
			if (lifc.lifc_buf != NULL)
				free(lifc.lifc_buf);
			if (new != NULL)
				free(new);
			(void) close(s);
			*attr = NULL;
			return (-1);
		} else {
			(void) strncpy(lifr.lifr_name, name,
			    sizeof (lifr.lifr_name));
		}
		if (ioctl(s, SIOCGLIFADDR, (caddr_t)&lifr) < 0) {
			Perror0("SIOCGLIFADDR");
			if (lifc.lifc_buf != NULL)
				free(lifc.lifc_buf);
			if (new != NULL)
				free(new);
			(void) close(s);
			*attr = NULL;
			return (-1);
		}
		if (flags & IFF_IPV4) {

			struct sockaddr_in *sin;

			sin = (struct sockaddr_in *)&lifr.lifr_addr;
			if (sin->sin_addr.s_addr == 0) {
				(void) strncpy(new->ipaddress, "0.0.0.0", 7);
			} else {
				(void) strncpy(new->ipaddress,
				    inet_ntoa(sin->sin_addr),
				    sizeof (struct sockaddr_in) + 1);
			}

		} else if (flags & IFF_IPV6) {

			int ret;
			char abuf[INET6_ADDRSTRLEN];
			struct sockaddr_in6 *sin6;

			sin6 = (struct sockaddr_in6 *)&lifr.lifr_addr;
			ret = sprintf(new->ipaddress, "%s/%d",
			    inet_ntop(AF_INET6, (void *)&sin6->sin6_addr,
				abuf, sizeof (abuf)),
			    lifr.lifr_addrlen);
			if (ret < 0) {
				Perror0("sprintf");
				if (lifc.lifc_buf != NULL)
					free(lifc.lifc_buf);
				if (new != NULL)
					free(new);
				(void) close(s);
				*attr = NULL;
				return (-1);
			}
		}
	}
	/*
	 * Netmask only IPv4 (Ipv6: multicast)
	 */
	(void) memset(new->netmask, 0, _SS_MAXSIZE);
	if (flags & IFF_IPV4) {

		int ret;
		struct sockaddr_in *sin;
		struct lifreq lifr;

		if (name == NULL) {
			Perror0("name NULL");
			if (lifc.lifc_buf != NULL)
				free(lifc.lifc_buf);
			if (new != NULL)
				free(new);
			(void) close(s);
			*attr = NULL;
			return (-1);
		} else {
			(void) strncpy(lifr.lifr_name, name,
			    sizeof (lifr.lifr_name));
		}
		if (ioctl(s, SIOCGLIFNETMASK, (caddr_t)&lifr) < 0) {
			Perror0("SIOCGLIFNETMASK");
			if (lifc.lifc_buf != NULL)
				free(lifc.lifc_buf);
			if (new != NULL)
				free(new);
			(void) close(s);
			*attr = NULL;
			return (-1);
		}
		sin = (struct sockaddr_in *)&lifr.lifr_addr;
		ret = sprintf(new->netmask, "%x",
		    ntohl((sin->sin_addr).s_addr));
		if (ret < 0) {
			Perror0("sprintf");
			if (lifc.lifc_buf != NULL)
				free(lifc.lifc_buf);
			if (new != NULL)
				free(new);
			(void) close(s);
			*attr = NULL;
			return (-1);
		}
	}
	/*
	 * Broadcast only IPv4 (Ipv6: multicast)
	 */
	(void) memset(new->broadcast, 0, _SS_MAXSIZE);
	if ((flags & IFF_IPV4) && (flags & IFF_BROADCAST)) {

		struct sockaddr_in *sin;
		struct lifreq lifr;

		if (name == NULL) {
			Perror0("name NULL");
			if (lifc.lifc_buf != NULL)
				free(lifc.lifc_buf);
			if (new != NULL)
				free(new);
			(void) close(s);
			*attr = NULL;
			return (-1);
		} else {
			(void) strncpy(lifr.lifr_name, name,
			    sizeof (lifr.lifr_name));
		}
		if (ioctl(s, SIOCGLIFBRDADDR, (caddr_t)&lifr) < 0) {
			Perror0("SIOCGLIFBRDADDR");
			if (lifc.lifc_buf != NULL)
				free(lifc.lifc_buf);
			if (new != NULL)
				free(new);
			(void) close(s);
			*attr = NULL;
			return (-1);
		}
		sin = (struct sockaddr_in *)&lifr.lifr_addr;
		if (sin->sin_addr.s_addr != 0)
			(void) strncpy(new->broadcast,
			    inet_ntoa(sin->sin_addr),
			    sizeof (struct sockaddr_in) + 1);
	}
	if (lifc.lifc_buf != NULL)
		free(lifc.lifc_buf);
	(void) close(s);
	*attr = new;
	return (0);
}


/*
 * Check if a network interfaces is a know instance
 * Return 1 if true, 0 if false
 */
static int
    isInterfaceRegistered(char *instance)
{
	int retval;

	/*
	 * First, see if this is a physical interface
	 */
	retval = isNetworkInterfaces(instance);
	if (retval < 0)
		return (-1);
	if (retval)
		return (retval);

	/*
	 * Otherwise, see if this is a logical interface
	 */
	retval = isConfiguredInterfaces(instance);
	if (retval < 0)
		return (-1);

	retval = 1;
	return (retval);
}


void
    Perror0(char *cmd)
{
	int save_errno;

	save_errno = errno; /*lint !e746 */
	(void) fprintf(stderr, "errno: ");
	errno = save_errno;
	switch (errno) {

	case EADDRNOTAVAIL:
		(void) fprintf(stderr, "%s no address available\n", cmd);
		break;

	case EAFNOSUPPORT:
		(void) fprintf(stderr, "%s family not supported\n", cmd);
		break;

	case ENOMEM:
		(void) fprintf(stderr, "%s not enough memory\n", cmd);
		break;

	case EAGAIN:
		(void) fprintf(stderr, "%s try again\n", cmd);
		break;

	case EFAULT:
		(void) fprintf(stderr, "%s illegal address\n", cmd);
		break;

	case ENXIO:
		(void) fprintf(stderr, "%s no such interface\n", cmd);
		break;

	case EPERM:
		(void) fprintf(stderr, "%s permission denied\n", cmd);
		break;

	default: {
			char buf[BUFSIZ];

			(void) snprintf(buf, sizeof (buf), "cmd");
			perror(buf);
		}
	}
}

/*
 * First look in the environment and then in /etc/default/.
 * If the variable is set in the environment we're done.
 * Note: Handles BOTH the same as IP_VERSION6.
 *
 * Returns 1 if IP_VERSION4; 0 for other versions.
 * Returns -1 if the value of environment variable DEFAULT_IP is invalid.
 * Returns -2 if the environment variable DEFAULT_IP is not set and the value
 * of DEFAULT_IP which is read from etc/default/inet_type is invalid.
 */
static int
    get_compat_flag(char **value)
{
	boolean_t read_env_var = _B_FALSE;

	*value = getenv(DEFAULT_IP);
	if (*value != NULL) {
		*value = strdup(*value);
		read_env_var = _B_TRUE;
	}
	if (*value == NULL) {
		*value = getdefault(DEFAULT_IP);
		read_env_var = _B_FALSE;
	}
	if (*value != NULL) {
		if (strcasecmp(*value, "IP_VERSION4") == 0) {
			return (1);
		} else if (strcasecmp(*value, "BOTH") == 0 ||
		    strcasecmp(*value, "IP_VERSION6") == 0) {
			return (0);
		} else {
			if (read_env_var)
				return (-1);
			else
				return (-2);
		}
	}
	/*
	 * No value set - default is BOTH
	 */
	return (0);
}



/*
 * Code to handle /etc/default/ file for IPv4 command output compatibility
 */

#include	<deflt.h>

static char *
    getdefault(char *name)
{
	char namebuf[BUFSIZ];
	char *value = NULL;

	if (defopen(INET_DEFAULT_FILE) == 0) {
		char *cp;
		int flags;

		/*
		 * ignore case
		 */
		flags = defcntl(DC_GETFLAGS, 0);
		TURNOFF(flags, DC_CASE);
		(void) defcntl(DC_SETFLAGS, flags);

		/*
		 * Add "=" to the name
		 */
		(void) strncpy(namebuf, name, sizeof (namebuf) - 2);
		(void) strncat(namebuf, "=", 2);

		cp = defread(namebuf);
		if (cp != NULL)
			value = strdup(cp);

		/*
		 * close
		 */
		(void) defopen((char *)NULL);
	}
	return (value);
}

/* called by getNetworkInterfaces() */
static int
    devfs_entry(di_node_t node, di_minor_t minor, void *arg)
{
	struct ni_list **nil_head_handle = (struct ni_list **)arg;
	struct net_interface *nip = NULL;
	struct ni_list *nilp = NULL;
	struct ni_list *tnilp;
	char *name;
	char *cname, *dev_ddm_name;
	size_t cnamelen;
	char last_char;
	int len;
	char *devfstype, *cp;
	char devfsnm[MAXPATHLEN];

	cp = di_devfs_path(node);
	(void) strncpy(devfsnm, cp + 1, sizeof (devfsnm));
	len = (int)strlen(cp);
	di_devfs_path_free(cp);
	devfsnm[len - 1] = ':';
	(void) strncpy(devfsnm + len, di_minor_name(minor),
	    (size_t)((int)sizeof (devfsnm) - len));

	/*
	 * Look for network devices only
	 */
	devfstype = di_minor_nodetype(minor);
	/*
	 * kludge for minor nodes with NULL nodetype
	 */
	if ((devfstype == NULL) || (strcmp(devfstype, class) != 0))
		return (DI_WALK_CONTINUE);

	/*
	 * If the ddi_minor_data name doesn't match the devinfo node name,
	 * use the ddi_minor_data name. This is needed for devices
	 * like HappyMeal that name devinfo names like
	 * SUNW,hme...:hme.
	 */
	name = di_node_name(node);
	if (di_minor_type(minor) == DDM_ALIAS &&
	    strcmp(di_minor_name(minor), name))
		name = di_minor_name(minor);

	if (di_minor_type(minor) == DDM_MINOR) {
		dev_ddm_name = di_minor_name(minor);
		last_char = dev_ddm_name[strlen(dev_ddm_name) - 1];
		if (last_char < '0' || last_char > '9') {
			return (DI_WALK_CONTINUE);
		}
	}
	/*
	 * Now, let's add the node to the interface list
	 */
	ni_new(nip);
	ni_set_type(nip, name);
	cnamelen = strlen(ni_get_type(nip)) + 3;
	cname = malloc(cnamelen);
	if (cname == NULL) {
		Perror0("could not allocate memory");
		return (-1);
	}
	if (di_minor_type(minor) == DDM_MINOR) {
		(void) strncpy(cname, di_minor_name(minor), cnamelen);
	} else {
		(void) memset(cname, 0, cnamelen);
		(void) snprintf(cname, cnamelen, "%s%d", ni_get_type(nip),
		    di_instance(node));
	}

	ni_set_name(nip, cname);
	free(cname);
	nil_new(nilp);
	set_nifp(nilp, nip);
	if (*nil_head_handle == NIL_NULL) {
		*nil_head_handle = nilp;
	} else {
		tnilp = first_if(*nil_head_handle);
		while (tnilp != NIL_NULL) {
			if (next_if(tnilp) == NIL_NULL) {
				break;
			}
			tnilp = next_if(tnilp);
		}
		next_if(tnilp) = nilp;
	}


	return (DI_WALK_CONTINUE);
} /*lint !e715 */
