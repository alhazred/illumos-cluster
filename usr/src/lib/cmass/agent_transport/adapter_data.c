/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Adapter mbeans
 */

#pragma ident	"@(#)adapter_data.c	1.6	08/05/25 SMI"

#include <sys/sol_version.h>

#if SOL_VERSION >= __s10

#include "com/sun/cluster/agent/transport/ZoneAdapter.h"
#include <jniutil.h>

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <thread.h>
#include <unistd.h>

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <scha.h>
#include <cl_query/cl_query_types.h>
#include <pnm/pnm_head.h>
#include <rgm/pnm.h>
#include <sys/clconf_int.h>
#include <sys/os_compat.h>
#include <zone.h>

/* change this to if (1) to turn on debugging */
#define	debug if (0)

static jclass adapterDataClass, objArrayClass;
static mutex_t init_lock;
static int initialized = 0;

static jfieldID nameID;
static jfieldID groupID;
static jfieldID flagsID;
static jfieldID isipv6ID;
static jfieldID statusID;
static jfieldID subnetsID;
static jfieldID ipmasksID;
static jfieldID ipnetsID;
static jfieldID ipaddrsID;

static jmethodID adapterClassConstructor = NULL;

static void init(JNIEnv *env) {

	debug printf("\n BEGIN adapter_data.c: init \n");
	(void) mutex_lock(&init_lock);

	if (!initialized) {
	    jclass localClass;

	    /* Initialize clconf library */
	    clconf_lib_init();

	    /* find classes */
	    localClass = (*env)->FindClass(env,
		"com/sun/cluster/agent/transport/AdapterData");
	    adapterDataClass = (*env)->NewGlobalRef(env, localClass);

	    if (adapterDataClass == NULL) {
		debug printf("\n AdapterData class is null\n");
		debug printf("\n END init \n");
		goto unlock;
	    }
	    adapterClassConstructor = (*env)->GetMethodID(env, adapterDataClass,
		"<init>", "()V");

	    if (adapterClassConstructor == NULL) {
		debug printf("\n adapterClassConstructor is null\n");
		debug printf("\n END init \n");
		goto unlock;
	    }

	    /* String */
	    nameID = (*env)->GetFieldID(env, adapterDataClass, "name",
		"Ljava/lang/String;");
	    if (nameID == NULL) {
		debug printf("\n nameID is null \n");
		goto unlock;
	    }
	    groupID = (*env)->GetFieldID(env, adapterDataClass,
		"group", "Ljava/lang/String;");
	    if (groupID == NULL) {
		debug printf("\n groupID is null \n");
		goto unlock;
	    }

	    /* int */
	    flagsID = (*env)->GetFieldID(env, adapterDataClass, "flags", "J");
	    if (flagsID == NULL) {
		debug printf("\n flagsID is null \n");
		goto unlock;
	    }

	    statusID = (*env)->GetFieldID(env, adapterDataClass, "status", "I");
	    if (statusID == NULL) {
		debug printf("\n statusID is null \n");
		goto unlock;
	    }

	    /* boolean */
	    isipv6ID = (*env)->GetFieldID(env, adapterDataClass, "isIPv6", "Z");
	    if (isipv6ID == NULL) {
		debug printf("\n isipv6ID is null \n");
		goto unlock;
	    }

	    /* Object arrays */
	    ipmasksID = (*env)->GetFieldID(env, adapterDataClass, "ipMasks",
		"[Ljava/lang/String;");
	    if (ipmasksID == NULL) {
		debug printf("\n ipmasksID is null \n");
		goto unlock;
	    }
	    ipnetsID = (*env)->GetFieldID(env, adapterDataClass, "ipNets",
		"[Ljava/lang/String;");
	    if (ipnetsID == NULL) {
		debug printf("\n ipnetsID is null \n");
		goto unlock;
	    }
	    ipaddrsID = (*env)->GetFieldID(env, adapterDataClass, "ipAddrs",
		"[Ljava/lang/String;");
	    if (ipaddrsID == NULL) {
		debug printf("\n ipaddrsID is null \n");
		goto unlock;
	    }

	    initialized = 1;
	}

unlock:
	(void) mutex_unlock(&init_lock);
	debug printf("\n END init \n");
}

JNIEXPORT jobjectArray JNICALL
Java_com_sun_cluster_agent_transport_ZoneAdapter_readDataJNI(JNIEnv *env,
    jobject this, jstring nodeArg, jstring zoneArg, jboolean allAddrs)
{
	jobject adapterData = NULL;
	jsize nAdapters = 0;
	jobjectArray adapters;
	jobjectArray ipMasks = NULL;
	jobjectArray ipNets = NULL;
	jobjectArray ipAddrs = NULL;
	jobject result = NULL;
	int i, ret;
	int isIPv6;
	pnm_adapterinfo_t *alist;
	pnm_adapterinfo_t *adapter;

	char *node = (char *)(*env)->GetStringUTFChars(env, nodeArg, 0);
	char *zone = (char *)(*env)->GetStringUTFChars(env, zoneArg, 0);

	debug printf(" START readData(%s, %s, %d)\n", node, zone, allAddrs);

	if (!initialized) {
	    init(env);
	}

	debug printf(" adapterDataClass %X\n", adapterDataClass);
	debug printf(" env %X\n", env);

	/*
	 * Obtain adapter info from PNM
	 */
	if (zone == NULL) {	/* adapters for the node only (global zone) */
	    ret = pnm_init(node);
	} else {		/* adapters for the non-global zone only */
	    ret = pnm_zone_init(node, zone);
	}

	debug printf(" pnm_init()/pnm_zone_init() returned %d\n", ret);
	if (ret != 0) {
	    goto error;
	}

	ret = pnm_adapterinfo_list(&alist, allAddrs);
	debug printf(" pnm_adapterinfo_list() returned %d\n", ret);
	if (ret != 0) {
	    goto error;
	}

	i = 0;
	for (adapter = alist; adapter != NULL; adapter = adapter->next) {
	    i++;
	}
	debug printf(" %d adapters returned\n", i);
	if (i == 0) {
	    goto error;
	}

	adapters = (*env)->NewObjectArray(env, i, adapterDataClass, NULL);
	debug printf(" after new array %X\n", i);

	if (adapters == NULL) {
	    debug printf(" adapters Array Object is NULL\n");
	    goto error;
	}

	for (adapter = alist; adapter != NULL; adapter = adapter->next) {
	    jobject subnet = NULL;
	    int nSubnets = 0;
	    int j;

	    debug printf(" inside looP %s\n", (char *)adapter->adp_name);

	    adapterData = (*env)->NewObject(env, adapterDataClass,
		adapterClassConstructor);
	    if (adapterData == NULL) {
		debug printf(" adapterData Object is NULL\n");
		goto error;
	    }

	    setStringField(env, adapterData, nameID, adapter->adp_name);
	    setStringField(env, adapterData, groupID, adapter->adp_group);
	    setLongField(env, adapterData, flagsID, (long)adapter->adp_flags);

	    isIPv6 = (adapter->adp_flags & IFF_IPV6);
	    setBooleanField(env, adapterData, isipv6ID, isIPv6);

		/*
		 * get subnet info;  there can be multiple subnets per
		 * adapter
		 */
	    nSubnets = adapter->adp_addrs.num_addrs;
	    debug printf("  # of subnets = %d\n", nSubnets);
	    if (nSubnets > 0) {

		char addr[INET6_ADDRSTRLEN + 1];
		char **ipmask = (char **)malloc(nSubnets * sizeof (char *));
		char **ipnet = (char **)malloc(nSubnets * sizeof (char *));
		char **ipaddr = (char **)malloc(nSubnets * sizeof (char *));
		jstring jaddr;

		for (j = 0; j < nSubnets; j++) {
		    if (isIPv6) {   /* IPv6 */
			ipmask[j] = (char *)malloc(INET6_ADDRSTRLEN + 1);
			inet_ntop(AF_INET6, &(adapter->adp_addrs.adp_ipmask[j]),
			    ipmask[j], INET6_ADDRSTRLEN);
			ipnet[j] = (char *)malloc(INET6_ADDRSTRLEN + 1);
			inet_ntop(AF_INET6, &(adapter->adp_addrs.adp_ipnet[j]),
			    ipnet[j], INET6_ADDRSTRLEN);
			ipaddr[j] = (char *)malloc(INET6_ADDRSTRLEN + 1);
			inet_ntop(AF_INET6, &(adapter->adp_addrs.adp_ipaddr[j]),
			    ipaddr[j], INET6_ADDRSTRLEN);
		    } else {	    /* IPv4 */
			struct in_addr inaddr;
			ipmask[j] = (char *)malloc(INET_ADDRSTRLEN + 1);
			IN6_V4MAPPED_TO_INADDR(
			    &(adapter->adp_addrs.adp_ipmask[j]), &inaddr);
			strcpy(ipmask[j], inet_ntoa(inaddr));
			ipnet[j] = (char *)malloc(INET_ADDRSTRLEN + 1);
			IN6_V4MAPPED_TO_INADDR(
			    &(adapter->adp_addrs.adp_ipnet[j]), &inaddr);
			strcpy(ipnet[j], inet_ntoa(inaddr));
			ipaddr[j] = (char *)malloc(INET_ADDRSTRLEN + 1);
			IN6_V4MAPPED_TO_INADDR(
			    &(adapter->adp_addrs.adp_ipaddr[j]), &inaddr);
			strcpy(ipaddr[j], inet_ntoa(inaddr));
		    }
		}

		ipMasks = newStringArray(env, nSubnets, ipmask);
		ipNets = newStringArray(env, nSubnets, ipnet);
		ipAddrs = newStringArray(env, nSubnets, ipaddr);

		setObjectField(env, adapterData, ipmasksID, ipMasks);
		setObjectField(env, adapterData, ipnetsID, ipNets);
		setObjectField(env, adapterData, ipaddrsID, ipAddrs);

		/* free all the objects that were just malloc'd */
		for (j = 0; j < nSubnets; j++) {
		    free(ipmask[j]);
		    free(ipnet[j]);
		    free(ipaddr[j]);
		}
		free(ipmask);
		free(ipnet);
		free(ipaddr);
	    }

	    (*env)->SetObjectArrayElement(env, adapters, nAdapters++,
		adapterData);
	    debug printf(" end loop\n");
	}


error:
	/*
	 * Free the specific resources allocated in this function
	 */
	pnm_fini();

	return (adapters);

} /*lint !e715 */

#endif /* SOL_VERSION >= __s10 */
