/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * This file contains native library functions used for
 * the Junction mbean interceptor
 */

#pragma	ident	"@(#)junction_interceptor_jni.c 1.3	08/05/20 SMI"

#include "com/sun/cluster/agent/transport/JunctionInterceptor.h"

#include <jniutil.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <cl_query/cl_query_types.h>


static jobjectArray getPortsList(JNIEnv * env,
    cl_query_junction_info_t *junction);

/*
 * Class:     com_sun_cluster_agent_transport_JunctionInterceptor
 * Method:    getInstances
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL
Java_com_sun_cluster_agent_transport_JunctionInterceptor_getInstances
	(JNIEnv * env, jobject this) {

	const char *instance_name_utf;

	jobject instance_name = NULL;
	jclass array_list_class = NULL;
	jmethodID array_list_constructor;
	jmethodID array_list_add;
	jmethodID array_list_to_array;

	jclass string_class = NULL;
	jobjectArray string_array = NULL;

	jobject instance_names_list = NULL;

	jobjectArray return_array = NULL;

	cl_query_result_t result;
	cl_query_junction_info_t *junction = NULL;
	cl_query_error_t error = NULL;

	/*
	 * We must return a Java array of Strings containing the names of
	 * all the instances of our object type
	 */


	/*
	 * Local reference: need to delete it afterwards
	 */
	string_class = (*env)->FindClass(env, "java/lang/String");
	if (string_class == NULL)
		goto error;

	/*
	 * Local reference: need to delete afterwards
	 */
	array_list_class = (*env)->FindClass(env, "java/util/ArrayList");
	if (array_list_class == NULL)
		goto error;

	/*
	 * Pick up a methodID to the constructor
	 */
	array_list_constructor = (*env)->GetMethodID(env, array_list_class,
	    "<init>", "()V");

	/*
	 * Pick up a methodID to the 'add' method
	 */
	array_list_add = (*env)->GetMethodID(env, array_list_class, "add",
	    "(Ljava/lang/Object;)Z");

	/*
	 * Pick up a methodID to the toArray method
	 */
	array_list_to_array = (*env)->GetMethodID(env, array_list_class,
	    "toArray", "([Ljava/lang/Object;)[Ljava/lang/Object;");

	/*
	 * Create the empty ArrayList
	 */
	instance_names_list = (*env)->NewObject(env, array_list_class,
	    array_list_constructor);
	delRef(env, array_list_class);
	if (instance_names_list == NULL)
		goto error;

	/*
	 * Now fill it with instance names
	 */

	error = cl_query_get_info(CL_QUERY_JUNCTION_INFO, NULL, &result);

	if (error != CL_QUERY_OK)
		goto error;

	for (junction = (cl_query_junction_info_t *)result.value_list;
	    junction != NULL;
	    junction = junction->next) {

		char *buf = NULL;

		if (junction->junction_name != NULL) {

			int retval;
			buf = (char *)malloc(sizeof (char) *
			    (strlen(junction->junction_name) + 2));
			if (buf == NULL)
				goto error;
			/*
			 * Set instance_name_utf to this instance name
			 */
			retval = sprintf(buf, "%s", junction->junction_name);
			if (retval < 0) {
				free(buf);
				goto error;
			}
			instance_name_utf = buf;
		} else
			goto error;

		/*
		 * Local reference: must delete
		 */
		instance_name = (*env)->NewStringUTF(env, instance_name_utf);
		free(buf);

		if (instance_name == NULL)
			goto error;

		/*
		 * Add the String we just created to the ArrayList
		 */
		(void) (*env)->CallObjectMethod(env, instance_names_list,
		    array_list_add, instance_name);

		/*
		 * Delete local ref
		 */
		delRef(env, instance_name);
		instance_name = NULL;
	}

	/*
	 * Now convert the ArrayList to a String array before returning
	 */

	/*
	 * Create an empty string array for typing info
	 */
	string_array = (*env)->NewObjectArray(env, 0, string_class, NULL);
	if (string_array == NULL)
		goto error;
	delRef(env, string_class);

	/*
	 * Use toArray to map back to the underlying array
	 */

	return_array = (*env)->CallObjectMethod(env, instance_names_list,
	    array_list_to_array, string_array);
    error:

	if (error == CL_QUERY_OK)
		error = cl_query_free_result(CL_QUERY_JUNCTION_INFO, &result);

	delRef(env, string_class);
	delRef(env, array_list_class);
	delRef(env, instance_names_list);
	delRef(env, string_array);

	return (return_array);
} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_transport_JunctionInterceptor
 * Method:    getAttributes
 * Signature: (Ljava/lang/String;[Ljava/lang/String;
 *            )Ljavax/management/AttributeList;
 */
JNIEXPORT jobject JNICALL
Java_com_sun_cluster_agent_transport_JunctionInterceptor_getAttributes
	(JNIEnv * env,
	jobject this,
	jstring instance_name_string,
	jobjectArray attribute_names) {

	int i;

	const char *instance_name = NULL;
	const char *attr_name = NULL;

	jsize attribute_names_length;

	jclass attribute_list_class = NULL;
	jobject attribute_list = NULL;
	jmethodID attribute_list_constructor = NULL;
	jmethodID attribute_list_add = NULL;

	jclass attribute_class = NULL;
	jobject attribute = NULL;
	jmethodID attribute_constructor = NULL;

	jobject attribute_name_string = NULL;
	jobject attribute_value = NULL;

	cl_query_result_t result;
	cl_query_junction_info_t *junction = NULL;
	cl_query_error_t error = NULL;

	/*
	 * Get a C version of our instance name
	 */
	instance_name = (*env)->GetStringUTFChars(env, instance_name_string,
	    NULL);

	/*
	 * Our job is to iterate through the attribute_list array which
	 * contains the String names of attributes, and for each attribute,
	 * add an 'Attribute' object to the 'AttributeList' object that
	 * we return.
	 */

	/*
	 * Create the AttributeList object and initialize references
	 */

	attribute_list_class = (*env)->FindClass(env,
	    "javax/management/AttributeList");
	if (attribute_list_class == NULL)
		goto error;

	/*
	 * Get constructor
	 */
	attribute_list_constructor = (*env)->GetMethodID(env,
	    attribute_list_class, "<init>", "()V");
	if (attribute_list_constructor == NULL)
		goto error;

	/*
	 * Get 'add' method
	 */
	attribute_list_add = (*env)->GetMethodID(env, attribute_list_class,
	    "add", "(Ljavax/management/Attribute;)V");
	if (attribute_list_add == NULL)
		goto error;

	/*
	 * Instantiate one instance
	 */

	attribute_list = (*env)->NewObject(env, attribute_list_class,
	    attribute_list_constructor);
	if (attribute_list == NULL)
		goto error;

	/*
	 * Now get our references to 'attribute_class'
	 */

	attribute_class = (*env)->FindClass(env, "javax/management/Attribute");
	if (attribute_class == NULL)
		goto error;

	/*
	 * Get constructor
	 */
	attribute_constructor = (*env)->GetMethodID(env, attribute_class,
	    "<init>", "(Ljava/lang/String;Ljava/lang/Object;)V");
	if (attribute_constructor == NULL)
		goto error;

	/*
	 * Now iterate through the attribute names one by one, creating *
	 * an 'Attribute' object for each one, which associates the name
	 * with its value
	 */

	attribute_names_length = (*env)->GetArrayLength(env, attribute_names);

	error = cl_query_get_info(CL_QUERY_JUNCTION_INFO,
	    (char *)instance_name, &result);

	if (error != CL_QUERY_OK)
		goto error;

	junction = (cl_query_junction_info_t *)result.value_list;

	if (junction == NULL)
		goto error;

	for (i = 0; i < attribute_names_length; i++) {

		attribute_name_string = (*env)->GetObjectArrayElement(env,
		    attribute_names, i);

		if (attribute_name_string == NULL)
			goto error;

		attr_name = (*env)->GetStringUTFChars(env,
		    attribute_name_string, NULL);

		if (attr_name == NULL)
			goto error;

		/*
		 * OK, we've got a C string containing the attribute name,
		 * lets fill out the appropriate value for it
		 */

		/*
		 * Our transport uses types that always have a string
		 * constructor so we can consolidate to a single construction
		 * call
		 */

		if (strcmp(attr_name, "ID") == 0)
			attribute_value = newObjInt32Param(env,
			    "java/lang/Integer",
			    (int32_t)junction->junction_id);

		else if (strcmp(attr_name, "Name") == 0)
			attribute_value = newObjStringParam(env,
			    "java/lang/String",
			    (char *)junction->junction_name);

		else if (strcmp(attr_name, "Enabled") == 0)
			attribute_value = newBoolean(env,
			    junction->status == CL_QUERY_ENABLED);

		else if (strcmp(attr_name, "Type") == 0)
			attribute_value = newObjStringParam(env,
			    "java/lang/String",
			    (char *)junction->junction_type);

		else if (strcmp(attr_name, "Ports") == 0)
			attribute_value = getPortsList(env,
			    junction);

		else {
			/*
			 * unknown attribute, skip
			 */
			(*env)->ReleaseStringUTFChars(env,
			    attribute_name_string, attr_name);
			continue;
		}

		/*
		 * Now create the Attribute object
		 */

		attribute = (*env)->NewObject(env, attribute_class,
		    attribute_constructor, attribute_name_string,
		    attribute_value);
		if (attribute == NULL)
			goto error;

		/*
		 * and add it to the AttributeList
		 */

		(*env)->CallObjectMethod(env, attribute_list,
		    attribute_list_add, attribute);

		delRef(env, attribute_value);
		attribute_value = NULL;
		delRef(env, attribute);
		attribute = NULL;
		(*env)->ReleaseStringUTFChars(env, attribute_name_string,
		    attr_name);
		attribute_name_string = NULL;
	}

	if (error == CL_QUERY_OK)
		error = cl_query_free_result(CL_QUERY_JUNCTION_INFO, &result);

	(*env)->ReleaseStringUTFChars(env, instance_name_string,
	    instance_name);

	delRef(env, attribute_list_class);
	delRef(env, attribute_class);

	return (attribute_list);

    error:

	if (error == CL_QUERY_OK)
		error = cl_query_free_result(CL_QUERY_JUNCTION_INFO, &result);

	(*env)->ReleaseStringUTFChars(env, instance_name_string,
	    instance_name);
	if (attribute_name_string)
		(*env)->ReleaseStringUTFChars(env, attribute_name_string,
		    attr_name);

	delRef(env, attribute_list_class);
	delRef(env, attribute_list);
	delRef(env, attribute_class);
	delRef(env, attribute);

	return (NULL);
} /*lint !e715 */

/*
 * Class:     com_sun_cluster_agent_transport_JunctionInterceptor
 * Method:    isRegistered
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL
Java_com_sun_cluster_agent_transport_JunctionInterceptor_isRegistered
	(JNIEnv * env,
	jobject this,
	jstring instance_name_string) {

	const char *instance_name;

	jboolean return_value = JNI_FALSE;

	cl_query_result_t result;
	cl_query_junction_info_t *junction = NULL;

	cl_query_error_t error = NULL;

	/*
	 * Get a C version of our instance name
	 */
	instance_name = (*env)->GetStringUTFChars(env, instance_name_string,
	    NULL);

	error = cl_query_get_info(CL_QUERY_JUNCTION_INFO,
	    (char *)instance_name, &result);

	if (error != CL_QUERY_OK)
		goto error;

	junction = (cl_query_junction_info_t *)result.value_list;
	if (junction != NULL)
		return_value = JNI_TRUE;

	if (error == CL_QUERY_OK)
		error = cl_query_free_result(CL_QUERY_JUNCTION_INFO, &result);

    error:
	/*
	 * release our reference to the C version of the name
	 */
	(*env)->ReleaseStringUTFChars(env, instance_name_string,
	    instance_name);

	return (return_value);
} /*lint !e715 */


/*
 * ----------------------------- CLASS-SPECIFIC CODE -------------------
 */

/*
 * Private helper functions to get a Port list, it's quite long,
 * so was separated from getAttributes function
 */
static jobjectArray
    getPortsList(JNIEnv * env,
    cl_query_junction_info_t *junction)
{

	uint_t j = 0;
	jclass port_class;
	jmethodID port_constructor;
	jclass array_list_class;
	jmethodID array_list_constructor;
	jmethodID array_list_add;
	jmethodID array_list_to_array;
	jobjectArray return_array = NULL;

	jobject port_list;
	jclass port = NULL;
	jobjectArray port_array = NULL;

	port_class = (*env)->FindClass(env,
	    "com/sun/cluster/agent/transport/Port");
	if (port_class == NULL)
		return (NULL);
	/*
	 * Pick up a methodID to the constructor
	 */
	port_constructor = (*env)->GetMethodID(env, port_class, "<init>",
	    "(Ljava/lang/String;IZ)V");

	array_list_class = (*env)->FindClass(env, "java/util/ArrayList");
	if (array_list_class == NULL)
		return (NULL);

	/*
	 * Pick up a methodID to the constructor
	 */
	array_list_constructor = (*env)->GetMethodID(env, array_list_class,
	    "<init>", "(I)V");

	/*
	 * Pick up a methodID to the 'add' method
	 */
	array_list_add = (*env)->GetMethodID(env, array_list_class, "add",
	    "(Ljava/lang/Object;)Z");

	/*
	 * Pick up a methodID to the toArray method
	 */
	array_list_to_array = (*env)->GetMethodID(env, array_list_class,
	    "toArray", "([Ljava/lang/Object;)[Ljava/lang/Object;");

	/*
	 * Retrieve ports information
	 */

	port_list = (*env)->NewObject(env, array_list_class,
	    array_list_constructor, junction->port_list.list_len);
	if (port_list == NULL)
		goto error;

	for (j = 0; j < junction->port_list.list_len; j++) {

		/*
		 * Create the corresponding java object
		 */
		port = (*env)->NewObject(env, port_class, port_constructor,
		    newObjStringParam(env, "java/lang/String",
			junction->port_list.list_val[j].port_name),
		    junction->port_list.list_val[j].port_id,
		    junction->port_list.list_val[j].port_state);

		if (port != NULL) {
			/*
			 * Add to the ArrayList our NodePort object
			 */
			(void) (*env)->CallObjectMethod(env, port_list,
			    array_list_add, port);
		}
	}

	/*
	 * Now convert the ArrayList to a NodePort array before returning
	 */

	/*
	 * Create an empty array for typing info
	 */
	port_array = (*env)->NewObjectArray(env, 0, port_class, NULL);
	if (port_array == NULL)
		goto error;

	/*
	 * Use toArray to map back to the underlying array
	 */
	return_array = (*env)->CallObjectMethod(env, port_list,
	    array_list_to_array, port_array);

	/*
	 * Now clean up
	 */
error:
	/*
	 * Delete local refs
	 */
	delRef(env, port_class);
	delRef(env, array_list_class);
	delRef(env, port_list);
	delRef(env, port);
	delRef(env, port_array);

	return (return_array);
}
/*
 * ---------------------- END-OF-CLASS-SPECIFIC CODE -------------------
 */
