/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SNMPUserObj.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.agent.event_mib;

/**
 * This simple class stores the values of the username, security setting name
 * and authentication value triplet.  It is used to return a simple object to
 * SPM from which these values can be retrieved.
 */
public class SNMPUserObj implements java.io.Serializable {

    private String username = null;
    private String security = null;
    private String auth = null;
    private boolean isDefault = false;

    /**
     * Create a new SNMPUserObj
     *
     * @param  username  the username
     * @param  security  the security value
     * @param  auth  the authentication value
     */
    SNMPUserObj(String username, String security, String auth,
        boolean isDefault) {

        this.username = username;
        this.security = security;
        this.auth = auth;
        this.isDefault = isDefault;

    }

    /**
     * Returns the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the security value.  This value will be one of the following:
     * noAuthNoPriv, authNoPriv, or authPriv
     */
    public String getSecurity() {
        return security;
    }

    /**
     * Returns the authentication value.  This value will be one of either MD5
     * or SHA
     */
    public String getAuth() {
        return auth;
    }

    /**
     * Returns whether this is default user.
     */
    public boolean isDefault() {
        return isDefault;
    }

}
