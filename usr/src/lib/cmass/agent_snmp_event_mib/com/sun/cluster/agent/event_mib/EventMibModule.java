/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)EventMibModule.java 1.11     08/05/20 SMI"
 */

package com.sun.cluster.agent.event_mib;

// JDK
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

// JMX
import javax.management.ObjectName;
import javax.management.InstanceAlreadyExistsException;

// JDMK
import com.sun.management.snmp.SnmpDefinitions;

// Cacao
import com.sun.cacao.snmpv3adaptor.SnmpV3AdaptorModule;
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.ObjectNameFactory;

// local
import com.sun.cluster.agent.event_mib.mibgen.SUN_CLUSTER_EVENTS_MIB;
import com.sun.cluster.agent.event_mib.mibgen.EnumEventSeverity;
import com.sun.cluster.common.ClusterRuntimeException;


/**
 * Module responsible for the SNMP Event MIB
 */
public class EventMibModule extends com.sun.cacao.Module {

    /** Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.event_mib");

    /** name of property defining maximum number of entries in table */
    public static final String ENTRY_COUNT = "snmp.event.table.entries";

    /** name of property defining the SNMP version to use */
    public static final String SNMP_VERSION = "snmp.version";

    /** name of the property file */
    public static final String PROP_FILE = "event_mib.property.file";

    /**
     * name of property defining the state of the module, enabled or disabled
     */
    public static final String MODULE_STATE = "eventmib.module.state";

    /** name of property defining */
    public static final String DEFAULT_USER = "eventmib.default.user";

    /** name of property defining */
    public static final String DEFAULT_SECURITY = "eventmib.default.security";


    /** private reference to our MIB */
    private static SUN_CLUSTER_EVENTS_MIB_IMPL mib;
    private static int entryCount;
    private static int SNMPVersion;
    private static boolean enabled;
    private static String defaultUser;
    private static int defaultSecurity;

    private ObjectName mibObjName;

    /**
     * Creates a new <code>EventMibModule</code> instance.
     *
     * @param  dd  a <code>com.sun.cacao.DeploymentDescriptor</code> value
     */
    public EventMibModule(DeploymentDescriptor dd) {
        super(dd);
    }

    /**
     * Return the name of this module instance
     *
     * @return  a <code>String</code> value
     */
    protected String getModuleInstanceName() {
        return "agent_snmp_event_mib";
    }

    /**
     * Called when module is to start offering service
     *
     * @exception  com.sun.cluster.common.ClusterRuntimeException  if an error
     * occurs
     */
    protected void start()
        throws com.sun.cluster.common.ClusterRuntimeException {

        try {

            // load the properties defined in the properties file
            loadProperties(getDeploymentDescriptor().getParameters()
                .getProperty(PROP_FILE));

            try {

                // Create an instance of the customized MIB
                //
                mib = new SUN_CLUSTER_EVENTS_MIB_IMPL(entryCount);

                // Register MIB into MBean Server
                mibObjName =
                    new ObjectNameFactory(
                        com.sun.cluster.agent.event_mib.EventMibModule.class)
                    .getObjectName(SUN_CLUSTER_EVENTS_MIB.class, null);
                getMbs().registerMBean(mib, mibObjName);

                // Initialize the MIB - not called if we didn't register in
                // mbean server
                mib.init();
            } catch (InstanceAlreadyExistsException iae) {
                logger.fine(
                    "MIB already registered in server, will not re-register.");
            }

            // Bind the SNMP adaptor to the MIB in order to make the MIB
            // accessible through the SNMP protocol adaptor.
            SnmpV3AdaptorModule.getSnmpV3AdaptorServer().addMib(mib, "");
            mib.setSnmpAdaptor(SnmpV3AdaptorModule.getSnmpV3AdaptorServer());

            try {

                // Register the manager
                EventMibManager emm = new EventMibManager(getMbs());

                ObjectNameFactory onf = new ObjectNameFactory(getDomainName());
                getMbs().registerMBean(emm,
                    onf.getObjectName(EventMibManagerMBean.class, null));
            } catch (InstanceAlreadyExistsException iae) {
                logger.fine(
                    "Manager MBean already registered in server, will not re-register.");
            }

            // Log start
            logger.fine("Successfully started EventMibModule");

        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception trying to register MIB",
                e);
            throw new ClusterRuntimeException("caught exception trying " +
                "to register MIB : " + e.getMessage());
        }

    }

    /**
     * Called by the <code>start</code> method to load in module properties
     *
     * @param  propFile  the property file
     */
    private void loadProperties(String propFile) {

        // Load the properties from the file
        //
        Properties props = new Properties();

        try {
            props.load(new FileInputStream(propFile));
        } catch (IOException e) {
            logger.log(Level.WARNING, "Could not load property file.", e);
        }

        entryCount = Integer.parseInt(props.getProperty(ENTRY_COUNT));
        SNMPVersion = Integer.parseInt(props.getProperty(SNMP_VERSION));
        defaultUser = props.getProperty(DEFAULT_USER);

        String security = props.getProperty(DEFAULT_SECURITY);

        if (security.equals("authPriv"))
            defaultSecurity = SnmpDefinitions.authPriv;
        else if (security.equals("authNoPriv"))
            defaultSecurity = SnmpDefinitions.authNoPriv;
        else
            defaultSecurity = SnmpDefinitions.noAuthNoPriv;

        if (props.getProperty(MODULE_STATE).toLowerCase().equals("enabled"))
            enabled = true;
        else
            enabled = false;


    }

    /**
     * Returns the number of event entries the mib is set to store
     */
    public static int getEntryCount() {
        return entryCount;
    }

    /**
     * Returns the SNMP protocol version value which traps will be sent out in
     */
    public static int getSNMPVersion() {
        return SNMPVersion;
    }

    /**
     * Sets the SNMP protocol version value which traps will be sent out in
     *
     * @param  version  the version number, either 2 or 3
     */
    public static void setSNMPVersion(int version) {
        SNMPVersion = version;
    }

    /**
     * Returns whether or not the module is enabled.  In the typical Cacao
     * sense, the module is always enabled, however, this value determines
     * whether the traps will be sent when an event is encountered.
     */
    public static boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets whether or not the module is enabled.  This value is used to
     * determine if the module will send traps or not, not if the module is
     * loaded in cacao
     */
    public static void setEnabled(boolean value) {
        enabled = value;
    }

    /**
     * Returns the minimum event severity level for which traps will be sent
     */
    public static EnumEventSeverity getMinimumSeverityLevel() {
        return new EnumEventSeverity("clEventSevWarning");
    }

    /**
     * Returns the default user which will be used when SNMPv3 traps are sent
     */
    public static String getDefaultUser() {
        return defaultUser;
    }

    /**
     * Sets the default SNMPv3 user to send traps as
     */
    public static void setDefaultUser(String user) {
        defaultUser = user;
    }

    /**
     * Returns the default secrity level associated with the default user to
     * send SNMPv3 traps as.
     */
    public static int getDefaultSecurity() {
        return defaultSecurity;
    }

    /**
     * Sets the default security level to send SNMPv3 traps in
     */
    public static void setDefaultSecurity(int sec) {
        defaultSecurity = sec;
    }

    /**
     * Called when module is to stop offering service
     */
    protected void stop() {

        // undo the start - deregister the event mib and throw it away,
        // including deregistering for notifications from the cluster
        // event mbean.

        // Stop the mib and release it for GC

        try {
            getMbs().unregisterMBean(mibObjName);
            mibObjName = null;
        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);
        }

        mib.unlock();

        if (SnmpV3AdaptorModule.getSnmpV3AdaptorServer() != null) {
            SnmpV3AdaptorModule.getSnmpV3AdaptorServer().removeMib(mib);
        }

        mib = null;

        // Log stop
        logger.fine("Successfully stopped EventMibModule");
    }
}
