/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SUN_CLUSTER_EVENTS_MIB_IMPL.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.event_mib;

// java imports
import java.io.Serializable;
import java.util.Hashtable;

// jmx imports
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.InstanceAlreadyExistsException;

// jdmk imports
import com.sun.management.snmp.agent.SnmpMib;
import com.sun.management.snmp.agent.SnmpMibNode;
import com.sun.management.snmp.agent.SnmpMibTable;
import com.sun.management.snmp.agent.SnmpStandardObjectServer;

// local imports
import com.sun.cluster.agent.event_mib.mibgen.*;


/**
 * The class is used for representing "SUN-CLUSTER-EVENTS-MIB". You can edit the
 * file if you want to modify the behaviour of the MIB.
 */
public class SUN_CLUSTER_EVENTS_MIB_IMPL extends SUN_CLUSTER_EVENTS_MIB
    implements Serializable {

    private int entriesCount;

    private ScEventsMIBObjectsImpl scEventsMIBObjectsImpl;

    /**
     * Default constructor. Initialize the Mib tree.
     */
    public SUN_CLUSTER_EVENTS_MIB_IMPL(int entriesCount) {
        super();
        this.entriesCount = entriesCount;
    }

    /**
     * Factory method for "ScEventsMIBObjects" group MBean. You can redefine
     * this method if you need to replace the default generated MBean class with
     * your own customized class.
     *
     * @param  groupName  Name of the group ("ScEventsMIBObjects")
     * @param  groupOid  OID of this group
     * @param  groupObjname  ObjectName for this group (may be null)
     * @param  server  MBeanServer for this group (may be null)
     *
     * @return  An instance of the MBean class generated for the
     * "ScEventsMIBObjects" group (ScEventsMIBObjects) Note that when using
     * standard metadata, the returned object must implement the
     * "ScEventsMIBObjectsMBean" interface.
     */
    protected Object createScEventsMIBObjectsMBean(String groupName,
        String groupOid, ObjectName groupObjname, MBeanServer server) {

        if (server == null) {
            scEventsMIBObjectsImpl = new ScEventsMIBObjectsImpl(this,
                    entriesCount);
        } else {
            scEventsMIBObjectsImpl = new ScEventsMIBObjectsImpl(this, server,
                    entriesCount);
        }

        return scEventsMIBObjectsImpl;
    }

    public void unlock() {
        scEventsMIBObjectsImpl.unlock();
        scEventsMIBObjectsImpl = null;
    }

}
