/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)SNMPHostObj.java 1.6     08/05/20 SMI"
 */

package com.sun.cluster.agent.event_mib;

/**
 * This simple class stores the values of the hostname and community name pair
 * values.  It is used to return a simple object to SPM from which these values
 * can be retrieved.
 */
public class SNMPHostObj implements java.io.Serializable {

    private String host = null;
    private String community = null;

    /**
     * Create a new SNMPHostObj object
     *
     * @param  host  the hostname
     * @param  community  the community name
     */
    SNMPHostObj(String host, String community) {

        this.host = host;
        this.community = community;

    }

    /**
     * Returns the host value stored in this object
     */
    public String getHost() {
        return host;
    }

    /**
     * Returns the community value stored in this object
     */
    public String getCommunity() {
        return community;
    }

}
