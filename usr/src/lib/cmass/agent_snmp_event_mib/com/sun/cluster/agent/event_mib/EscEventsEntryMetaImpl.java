/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)EscEventsEntryMetaImpl.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.event_mib;

// java imports
import java.io.Serializable;

// jdmk imports
import com.sun.management.snmp.agent.SnmpMib;
import com.sun.management.snmp.agent.SnmpStandardMetaServer;
import com.sun.management.snmp.agent.SnmpStandardObjectServer;

// local imports
import com.sun.cluster.agent.event_mib.mibgen.*;


/**
 * The class is used for representing SNMP metadata for the "EscEventsEntry"
 * group. The group is defined with the following oid:
 * 1.3.6.1.4.1.42.2.80.2.1.2.1.
 */
public class EscEventsEntryMetaImpl extends EscEventsEntryMeta
    implements Serializable, SnmpStandardMetaServer {

    /**
     * Constructor for the metadata associated to "EscEventsEntry".
     */
    public EscEventsEntryMetaImpl(SnmpMib myMib,
        SnmpStandardObjectServer objserv) {
        super(myMib, objserv);
    }

    /**
     * Allow to bind the metadata description to a specific object.
     */
    public void setInstance(EscEventsEntryMBean var) {
        node = var;
    }
}
