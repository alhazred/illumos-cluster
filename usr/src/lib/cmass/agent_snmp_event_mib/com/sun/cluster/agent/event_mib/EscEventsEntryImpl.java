/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)EscEventsEntryImpl.java 1.10     09/01/22 SMI"
 */

package com.sun.cluster.agent.event_mib;

// java imports
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

// jmx imports
import javax.management.MBeanServer;
import javax.management.ObjectName;

// jdmk imports
import com.sun.management.snmp.agent.SnmpMib;
import com.sun.management.snmp.SnmpStatusException;

// local imports
import com.sun.cluster.agent.event_mib.mibgen.*;


/**
 * The class is used for implementing the "EscEventsEntry" group. The group is
 * defined with the following oid: 1.3.6.1.4.1.42.2.80.2.1.1.1.
 */
public class EscEventsEntryImpl extends EscEventsEntry
    implements EscEventsEntryMBean, Serializable {

    /** Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.event_mib");

    /** ObjectName prefix */
    private String prefix = null;

    public EscEventsEntryImpl(SnmpMib myMib, int index, String clusterId,
        String clusterName, String nodeName, int version, String clazz,
        String subclass, int severity, int initiator, String publisher,
        long eventSeqNo, long pid, long timestamp, String eventData) {

        super(myMib);
        prefix = myMib.getMibName() + "/EscEventsTable:";

        this.EventIndex = new Integer(index);
        this.EventClusterId = clusterId;
        this.EventClusterName = clusterName;
        this.EventNodeName = nodeName;
        this.EventVersion = new Integer(version);
        this.EventClassName = clazz;
        this.EventSubclassName = subclass;
        this.EventSeverity = new EnumEventSeverity(severity);
        this.EventInitiator = new EnumEventInitiator(initiator);
        this.EventPublisher = publisher;
        this.EventSeqNo = new Long(eventSeqNo);
        this.EventPid = new Long(pid);
        this.EventTimeStamp = new Long(timestamp);
        this.EventData = eventData;
    }

    public ObjectName createObjectName(MBeanServer server) {

        try {

            if (server == null)
                return null;

            return new ObjectName(prefix + "EventIndex=" + EventIndex +
                    ",EventSubclassName=" + EventSubclassName);
        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);

            return null;
        }
    }
}
