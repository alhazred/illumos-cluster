/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)EventMibManagerMBean.java 1.9     08/05/20 SMI"
 */

package com.sun.cluster.agent.event_mib;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;

import java.util.List;


/**
 * Provides an interface for the sceventmib CLI and the SunPlexManager GUI to
 * manage the Event MIB module
 */
public interface EventMibManagerMBean {

    /** the TYPE field we use in mbean name */
    public static final String TYPE = "eventmib";

    /** Security level value NoAuthNoPriv */
    public static final String NOAUTHNOPRIV = "NoAuthNoPriv";

    /** Security level value AuthNoPriv */
    public static final String AUTHNOPRIV = "AuthNoPriv";

    /** Security level value AuthPriv */
    public static final String AUTHPRIV = "AuthPriv";

    /** Authorization type None */
    public static final String NO_AUTH = "None";

    /** Authorization type MD5 */
    public static final String MD5 = "MD5";

    /** Authorization type SHA */
    public static final String SHA = "SHA";

    /**
     * Get the SNMP version used
     */
    public int getSNMPVersion();

    /**
     * Set the SNMP version used
     */
    public ExitStatus setSNMPVersion(int version)
        throws CommandExecutionException;

    public void setModuleSNMPVersion(int version);

    /**
     * Get the number of entries stored in table
     */
    public int getEntryCount();

    /**
     * Get the state, enabled or disabled
     */
    public boolean isEnabled();

    /**
     * Set the state, enabled or disabled
     */
    public ExitStatus setEnabled(boolean state)
        throws CommandExecutionException;

    public void setModuleEnabled(boolean state);

    /**
     * Get a List of hosts which are set to receive traps
     */
    public List getHosts();

    /**
     * Get a List of users which have been allowed access to the MIB
     */
    public List getUsers();


    /**
     * Add a host
     */
    public ExitStatus addHost(String hostname, String community)
        throws CommandExecutionException;

    /**
     * Remove a host
     */
    public ExitStatus removeHost(String hostname, String community)
        throws CommandExecutionException;

    /**
     * Add a user
     */
    public ExitStatus addUser(String username, String auth, String password)
        throws CommandExecutionException;

    /**
     * Remove a user
     */
    public ExitStatus removeUser(String username)
        throws CommandExecutionException;

    /**
     * Set the default user and seclevel
     */
    public ExitStatus setDefaultUserAndSeclevel(String user, String security)
        throws CommandExecutionException;

    /**
     * Get the default user setting
     */
    public String getDefaultUser();

    /**
     * Set the default user setting
     */
    public void setDefaultUser(String user);

    /**
     * Get the default security setting
     */
    public String getDefaultSecurity();

    /**
     * Set the default security setting
     */
    public void setDefaultSecurity(String security);

    /**
     * Restarts the SNMPAdaptorServer
     */
    public void SNMPAdaptorServerRestart();

}
