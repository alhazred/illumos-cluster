/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)EscEventsAttributesEntryImpl.java 1.8     08/05/20 SMI"
 */

package com.sun.cluster.agent.event_mib;

// java imports
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

// jmx imports
import javax.management.MBeanServer;
import javax.management.ObjectName;

// jdmk imports
import com.sun.management.snmp.agent.SnmpMib;
import com.sun.management.snmp.SnmpStatusException;

// local imports
import com.sun.cluster.agent.event_mib.mibgen.*;


/**
 * The class is used for implementing the "EscEventsAttributesEntry" group. The
 * group is defined with the following oid: 1.3.6.1.4.1.42.2.80.2.1.2.1.
 */
public class EscEventsAttributesEntryImpl extends EscEventsAttributesEntry
    implements EscEventsAttributesEntryMBean, Serializable {

    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.event_mib");

    private String prefix;

    /**
     * Constructor for the "EscEventsAttributesEntry" group.
     */
    public EscEventsAttributesEntryImpl(SnmpMib myMib, int index, String key,
        String value) {
        super(myMib);
        prefix = myMib.getMibName() + "/EscEventsAttributesTable:";

        this.EventIndex = new Integer(index);

        this.AttributeName = key;
        this.AttributeValue = value;
    }

    public ObjectName createObjectName(MBeanServer server) {

        try {

            if (server == null)
                return null;

            return new ObjectName(prefix + "EventIndex=" + EventIndex +
                    ",AttributeName=" + AttributeName);
        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);

            return null;
        }
    }
}
