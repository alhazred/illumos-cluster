/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)EventMibManager.java 1.11     08/05/20 SMI"
 */

package com.sun.cluster.agent.event_mib;

// JDK
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

// JMX
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;

// JDMK
import com.sun.management.snmp.SnmpDefinitions;

// Cacao
import com.sun.cacao.invocation.InvocationException;
import com.sun.cacao.invocation.InvocationStatus;
import com.sun.cacao.invocation.InvokeCommand;
import com.sun.cacao.snmpv3adaptor.SnmpV3AdaptorModule;
import com.sun.cacao.element.OperationalStateEnum;

// CMAS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.ClusterPaths;


/**
 * The implementation of the <code>EventMibManagerMBean</code> interface.
 */
public class EventMibManager implements EventMibManagerMBean {

    private MBeanServer mbs = null;

    public EventMibManager(MBeanServer mbs) {
        this.mbs = mbs;
    }


    /**
     * Returns the SNMP protocol version set
     */
    public int getSNMPVersion() {
        return EventMibModule.getSNMPVersion();
    }

    /**
     * Sets the SNMP protocol version
     */
    public ExitStatus setSNMPVersion(int version)
        throws CommandExecutionException {

        // Now write it to the conf file for posterity
        String cmd[] = {
                ClusterPaths.CL_SNMP_MIB_CMD, "set", "-p",
                "version=" + Integer.toString(version), "Event"
            };

        InvocationStatus exit;

        try {
            exit = InvokeCommand.execute(cmd, null);
        } catch (InvocationException ie) {
            InvocationStatus exits[] = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return createExitStatus(exit);
    }

    public void setModuleSNMPVersion(int version) {

        // Set version in active module
        EventMibModule.setSNMPVersion(version);
    }

    /**
     * Returns the number of entries to store in the read-only table of events
     */
    public int getEntryCount() {
        return EventMibModule.getEntryCount();
    }

    /**
     * Returns the hosts set to recieve traps.  Uses the sceventmib command to
     * list the hosts on the current node and parses the output to provide a
     * List of SNMPHostObj tuples
     */
    public List getHosts() {
        String cmd[] = { ClusterPaths.CL_SNMP_HOST_CMD, "list", "-v" };

        BufferedReader output = null;

        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            output = new BufferedReader(new InputStreamReader(
                        proc.getInputStream()));
        } catch (IOException ioe) {
        }

        ArrayList list = new ArrayList();

        try {
            boolean start = false;

            String line = output.readLine();

            while (line != null) {

                if (line.startsWith("---------") && start)
                    break;

                if (start) {
                    String value[] = line.trim().split(" +");
                    list.add(new SNMPHostObj(value[0], value[1]));
                }

                if (line.startsWith("---------") && !start)
                    start = true;

                line = output.readLine();
            }
        } catch (IOException ioe) {
        }

        return list;
    }

    /**
     * Returns the users which are configured on the node for SNMPv3.  Uses the
     * sceventmib command to list the users on the node and parses the output to
     * provide a List of SNMPUserObj tuples
     */
    public List getUsers() {
        String cmd[] = { ClusterPaths.SCEVENTMIB_CMD, "-p", "users" };

        BufferedReader output = null;

        try {
            Process proc = Runtime.getRuntime().exec(cmd);
            output = new BufferedReader(new InputStreamReader(
                        proc.getInputStream()));
        } catch (IOException ioe) {
        }

        ArrayList list = new ArrayList();

        try {
            boolean start = false;

            String line = output.readLine();

            while (line != null) {

                if (line.startsWith("---------") && start)
                    break;

                if (start) {
                    String value[] = line.trim().split(" +");

                    // Check for default line
                    if (value[0].equals("*")) {
                        list.add(new SNMPUserObj(value[1], value[2], value[3],
                                true));
                    } else {
                        list.add(new SNMPUserObj(value[0], "", value[1],
                                false));
                    }
                }

                if (line.startsWith("---------") && !start)
                    start = true;

                line = output.readLine();
            }
        } catch (IOException ioe) {
        }

        return list;
    }

    /**
     * Returns whether the module is enabled or not.  This is a bit misleading
     * as the module is always loaded in the Cacao sense, but, when an event
     * notification is handled by the module, if the module returns a state of
     * disabled, it will not send any traps.
     */
    public boolean isEnabled() {
        return EventMibModule.isEnabled();
    }

    /**
     * Sets the state of the module on the node.
     */
    public ExitStatus setEnabled(boolean enabled)
        throws CommandExecutionException {
        String cmd[] = { ClusterPaths.CL_SNMP_MIB_CMD, "enable", "Event" };

        if (!enabled) {
            cmd[1] = "disable";
        }

        InvocationStatus exit;

        try {
            exit = InvokeCommand.execute(cmd, null);
        } catch (InvocationException ie) {
            InvocationStatus exits[] = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return createExitStatus(exit);
    }

    public void setModuleEnabled(boolean enabled) {

        // Set version in active module
        EventMibModule.setEnabled(enabled);
    }

    /**
     * Adds a host to the configuration on the node.  Uses the sceventmib
     * command to adds the host.
     */
    public ExitStatus addHost(String hostname, String community)
        throws CommandExecutionException {
        String cmd[] = {
                ClusterPaths.CL_SNMP_HOST_CMD, "add", "-c", community, hostname
            };

        InvocationStatus exit;

        try {
            exit = InvokeCommand.execute(cmd, null);
        } catch (InvocationException ie) {
            InvocationStatus exits[] = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return createExitStatus(exit);
    }

    /**
     * Removes a host from the configuration on the node.  Uses the clsnmphost
     * command to remove the host.
     */
    public ExitStatus removeHost(String hostname, String community)
        throws CommandExecutionException {
        String cmd[] = {
                ClusterPaths.CL_SNMP_HOST_CMD, "remove", "-c", community,
                hostname
            };

        InvocationStatus exit;

        try {
            exit = InvokeCommand.execute(cmd, null);
        } catch (InvocationException ie) {
            InvocationStatus exits[] = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return createExitStatus(exit);
    }

    /**
     * Adds a new user to the configuration on the node.  Uses the slsnmpuser
     * command to add a new user.
     */
    public ExitStatus addUser(String username, String auth, String password)
        throws CommandExecutionException {
        File file = new File("/tmp/clsnmpuser.temp");

        String cmd[] = {
                ClusterPaths.CL_SNMP_USER_CMD, "create", "-a", auth, "-f",
                file.getAbsolutePath(), username
            };

        ArrayList out = new ArrayList();
        ArrayList err = new ArrayList();

        try {
            FileWriter fw = new FileWriter(file);
            fw.write(username + ":" + password);
            fw.flush();
            fw.close();
        } catch (IOException ioe) {
            err.add(ioe.getMessage());

            return new ExitStatus(cmd, 1, out, err);
        }

        InvocationStatus exit;

        try {
            exit = InvokeCommand.execute(cmd, null);
        } catch (InvocationException ie) {
            file.delete();

            InvocationStatus exits[] = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        file.delete();

        return createExitStatus(exit);
    }


    /**
     * Removes a user from the configuration on the current node.  Uses the
     * clsnmpuser command to remove a the user.
     */
    public ExitStatus removeUser(String username)
        throws CommandExecutionException {
        String cmd[] = { ClusterPaths.CL_SNMP_USER_CMD, "delete", username };

        InvocationStatus exit;

        try {
            exit = InvokeCommand.execute(cmd, null);
        } catch (InvocationException ie) {
            InvocationStatus exits[] = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        return createExitStatus(exit);
    }

    /**
     * Set the default user and seclevel
     */
    public ExitStatus setDefaultUserAndSeclevel(String user, String security)
        throws CommandExecutionException {
        String cmd[] = {
                ClusterPaths.CL_SNMP_USER_CMD, "set-default", "-l", security,
                user
            };

        InvocationStatus exit;

        try {
            exit = InvokeCommand.execute(cmd, null);
        } catch (InvocationException ie) {
            InvocationStatus exits[] = ie.getInvocationStatusArray();
            throw new CommandExecutionException(ie.getMessage(),
                ExitStatus.createArray(exits));
        }

        // If success, set default in active module, too
        if (exit.getExitValue().intValue() == 0) {
            setDefaultUser(user);
            setDefaultSecurity(security);
        }

        return createExitStatus(exit);
    }

    /**
     * Returns the default user
     */
    public String getDefaultUser() {
        return EventMibModule.getDefaultUser();
    }


    /**
     * Sets the default user
     */
    public void setDefaultUser(String user) {
        EventMibModule.setDefaultUser(user);
    }

    /**
     * Returns the default security level
     */
    public String getDefaultSecurity() {

        switch (EventMibModule.getDefaultSecurity()) {

        case SnmpDefinitions.authPriv:
            return "authPriv";

        case SnmpDefinitions.authNoPriv:
            return "authNoPriv";

        case SnmpDefinitions.noAuthNoPriv:
            return "noAuthNoPriv";

        default:
            return null;
        }
    }


    /**
     * Sets the default security level
     */
    public void setDefaultSecurity(String security) {

        if (security.equals("authPriv"))
            EventMibModule.setDefaultSecurity(SnmpDefinitions.authPriv);
        else if (security.equals("authNoPriv"))
            EventMibModule.setDefaultSecurity(SnmpDefinitions.authNoPriv);
        else
            EventMibModule.setDefaultSecurity(SnmpDefinitions.noAuthNoPriv);
    }

    /**
     * Restarts the SNMPAdaptorServer so as to re-read its configuration files
     */
    public void SNMPAdaptorServerRestart() {

        try {
            ObjectName on = new ObjectName("com.sun.cacao:type=module," +
                    "instance=\"com.sun.cacao.snmpv3_adaptor\"");
            mbs.invoke(on, "lock", null, null);
            mbs.invoke(on, "unlock", null, null);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }


    /**
     * Helper method to create the ExitStatus from the InvocationStatus object
     */
    private ExitStatus createExitStatus(InvocationStatus status) {

        String outRes = status.getStdout();
        String errRes = status.getStderr();
        List stdout = Arrays.asList(outRes.split("\n"));
        List stderr = Arrays.asList(errRes.split("\n"));

        return new ExitStatus(status.getArgv(),
                status.getExitValue().intValue(), stdout, stderr);

    }

}
