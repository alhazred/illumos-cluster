/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)ScEventsMIBObjectsImpl.java 1.14     09/01/22 SMI"
 */

package com.sun.cluster.agent.event_mib;

// java 
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

// jmx
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;

// jdmk
import com.sun.management.snmp.agent.SnmpMib;
import com.sun.management.snmp.SnmpDefinitions;
import com.sun.management.snmp.SnmpOid;
import com.sun.management.snmp.SnmpOidRecord;
import com.sun.management.snmp.SnmpStatusException;
import com.sun.management.snmp.SnmpString;
import com.sun.management.snmp.SnmpValue;
import com.sun.management.snmp.SnmpVarBind;
import com.sun.management.snmp.SnmpVarBindList;

// cacao 
import com.sun.cacao.DeploymentDescriptor;
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.container.Container;
import com.sun.cacao.snmpv3adaptor.SnmpV3AdaptorModule;

// Local
import com.sun.cluster.agent.event.ClEventDefs;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.event.SysEventNotifierMBean;
import com.sun.cluster.agent.event_mib.mibgen.*;


/**
 * The class is used for implementing the "ScEventsMIBObjects" group.  It is
 * also responsible for handling the Event notifications, as such, this is the
 * class that creates the trap notifications and sends them to the configured
 * hosts for each event. The group is defined with the following oid:
 * 1.3.6.1.4.1.42.2.80.2.1.
 */
public class ScEventsMIBObjectsImpl extends ScEventsMIBObjects
    implements NotificationListener, ScEventsMIBObjectsMBean, Serializable {

    /** Tracing utility. */
    private static Logger logger = Logger.getLogger(
            "com.sun.cluster.agent.event_mib");

    /** reference to our MIB object */
    private SnmpMib myMib;

    /** reference to the mbean server we should register mbeans into */
    private MBeanServer server;

    private ObjectName sysEventNotifier;

    private LinkedList eventEntriesList = new LinkedList();
    private LinkedList eventAttributesEntriesList = new LinkedList();

    /** Number of entries we maintain in the event table */
    private int entriesCount;

    private int currentIndex = 0;

    private String eventsTableOid = null;

    /** The meta-data for our EscEventsEntry */
    private EscEventsEntryMetaImpl eventsEntryMeta = new EscEventsEntryMetaImpl(
            null, null);

    /** the Trap OID - have to hardcode, not machine generated */
    private SnmpOid trapOid = new SnmpOid("1.3.6.1.4.1.42.2.80.2.2.1");

    /**
     * Constructor for the "ScEventsMIBObjects" group.  If the group contains a
     * table, the entries created through an SNMP SET will not be registered in
     * Java DMK.
     *
     * @param  myMib  a <code>SnmpMib</code> value
     * @param  entriesCount  an <code>int</code> value
     */
    public ScEventsMIBObjectsImpl(SnmpMib myMib, int entriesCount) {

        super(myMib);
        init(myMib, null, entriesCount);
    }

    /**
     * Constructor for the "ScEventsMIBObjects" group.  If the group contains a
     * table, the entries created through an SNMP SET will be AUTOMATICALLY
     * REGISTERED in Java DMK.
     *
     * @param  myMib  a <code>SnmpMib</code> value
     * @param  server  a <code>MBeanServer</code> value
     * @param  entriesCount  an <code>int</code> value
     */
    public ScEventsMIBObjectsImpl(SnmpMib myMib, MBeanServer server,
        int entriesCount) {
        super(myMib, server);
        init(myMib, server, entriesCount);
    }

    /**
     * initializer for the "ScEventsMIBObjects" group.  Called by both
     * constructors.
     *
     * @param  myMib  a <code>SnmpMib</code> value
     * @param  server  a <code>MBeanServer</code> value
     * @param  entriesCount  an <code>int</code> value
     */
    private void init(SnmpMib myMib, MBeanServer server, int entriesCount) {
        this.myMib = myMib;
        this.server = server;
        this.entriesCount = entriesCount;

        try {
            eventsTableOid = (new SUN_CLUSTER_EVENTS_MIBOidTable())
                .resolveVarName("escEventsTable").getOid();
        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);
        }

        try {
            Integer value = new Integer(entriesCount);
            checkEscEventTableCount(value);
            setEscEventTableCount(value);
        } catch (SnmpStatusException e) {
            logger.warning("Illegal configuration for event table count, " +
                "using default value of 100");

            try {
                setEscEventTableCount(new Integer(100));
            } catch (Exception ex) {
                logger.log(Level.WARNING, "caught exception", ex);
            }
        }

        lock(myMib, server, entriesCount);
    }

    /**
     * Setter for the "EscEventTableCount" variable.
     *
     * @param  x  an <code>Integer</code> value
     *
     * @exception  SnmpStatusException  if an error occurs
     */
    public void setEscEventTableCount(Integer x) throws SnmpStatusException {

        EscEventTableCount = x;
    }

    /**
     * Checker for the "EscEventTableCount" variable.
     *
     * @param  x  an <code>Integer</code> value
     *
     * @exception  SnmpStatusException  if the value is illegal
     */
    public void checkEscEventTableCount(Integer x) throws SnmpStatusException {

        int value = x.intValue();

        if ((entriesCount < 20) || (entriesCount > 32767)) {
            throw new SnmpStatusException(SnmpStatusException.badValue);
        }
    }


    private void lock(SnmpMib myMib, MBeanServer server, int entriesCount) {

        // Register with the container's mbean server, not the one we were
        // passed, since it will probably be null (snmp mbeans not registered)
        MBeanServer mBeanServer = Container.getMbs();

        ObjectNameFactory onf = new ObjectNameFactory(
                com.sun.cluster.agent.event.SysEventModule.class);
        sysEventNotifier = onf.getObjectName(SysEventNotifierMBean.class,
                null /* (String) instanceName */);

        try {

            // No need to use a proxy here, we can go directly to the
            // addNotificationListener method on the mbean server.
            mBeanServer.addNotificationListener(sysEventNotifier, this, null,
                null);
        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);
        }
    }

    /**
     * Called when shutting down the module.
     */
    public void unlock() {

        try {
            // Unregister for event listening.

            // No need to use a proxy here, we can go directly to the
            // addNotificationListener method on the mbean server.
            MBeanServer mBeanServer = Container.getMbs();
            mBeanServer.removeNotificationListener(sysEventNotifier, this, null,
                null);
            sysEventNotifier = null;

            // unregister all the entries in our tables
            while (eventEntriesList.size() != 0) {
                removeFirstEntry();
            }

            // The MIB is also registered in the mbean server,
            // unregister it here also.
            String on =
                "SUN_CLUSTER_EVENTS_MIB:name=com.sun.cluster.agent.event_mib.mibgen.ScEventsMIBObjects";
            mBeanServer.unregisterMBean(new ObjectName(on));

        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);
        }
    }


    /**
     * Sun Cluster Event notification handler. This method is responsible for
     * updating the SNMP MIB tables and emitting an SNMP trap/inform when a new
     * event is recieved
     *
     * @param  notification  a <code>Notification</code> value
     * @param  handback  an <code>Object</code> value
     */
    public synchronized void handleNotification(Notification notification,
        Object handback) {

        // if not enabled, don't do anything.
        if (!(EventMibModule.isEnabled()))
            return;

        // if not SysEventNotification, bail.
        if (!(notification instanceof SysEventNotification))
            return;

        SysEventNotification clEvent = (SysEventNotification) notification;

        // if not of high enough priority, bail.
        logger.fine("Event priority: " + clEvent.getSeverity().getOrdinal());

        if (clEvent.getSeverity().getOrdinal() <
                EventMibModule.getMinimumSeverityLevel().intValue()) {
            logger.fine("Severity too low.  Not sending trap.");

            return;
        }

        Map attrs = clEvent.getAttrs();

        String clusterId;
        String clusterName;
        String nodeName;
        int version;
        int initiator;

        try {
            clusterId = (String) attrs.remove(ClEventDefs.CL_CLUSTER_ID);
            clusterName = (String) attrs.remove(ClEventDefs.CL_CLUSTER_NAME);
            nodeName = (String) attrs.remove(ClEventDefs.CL_EVENT_NODE);
            version = ((Long) attrs.remove(ClEventDefs.CL_EVENT_VERSION))
                .intValue();
            initiator = ((Long) attrs.remove(ClEventDefs.CL_EVENT_INITIATOR))
                .intValue();
        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception, not adding event", e);

            return;
        }

        /* A list of attrs that we'll put in our FIFO list of events */
        List attrEntriesList = new LinkedList();
        /*
         * Iterate over remaining (subclass-specific) attributes
         * adding them to our 2nd table
         */
        Iterator i = attrs.keySet().iterator();
        StringBuffer eventDataBuffer = new StringBuffer();

        while (i.hasNext()) {
            String key = (String) i.next();
            String valueString = (attrs.get(key)).toString();
  
            // concatenate attribute name/value for varBindList later
            eventDataBuffer.append(key + ":" + valueString + ";");

            EscEventsAttributesEntryImpl escEventsAttributesEntry =
                new EscEventsAttributesEntryImpl(myMib, currentIndex, key,
                    valueString);
            final ObjectName escEventsAttributesEntryName =
                escEventsAttributesEntry.createObjectName(server);

            try {
                EscEventsAttributesTable.addEntry(escEventsAttributesEntry,
                    escEventsAttributesEntryName);

                attrEntriesList.add(escEventsAttributesEntry);
            } catch (SnmpStatusException e) {
                logger.log(Level.WARNING, "caught exception", e);
            }

            try {

                if ((server != null) &&
                        (escEventsAttributesEntryName != null)) {
                    server.registerMBean(escEventsAttributesEntry,
                        escEventsAttributesEntryName);
                }
            } catch (Exception e) {
                logger.log(Level.WARNING, "caught exception", e);
            }
        }

        String eventData = eventDataBuffer.substring(
            0, eventDataBuffer.length()-1); // remove the last ";"

        /*
         * Add event entry to to event MIB 
         */
        EscEventsEntryImpl escEventsEntry = new EscEventsEntryImpl(myMib,
                currentIndex, clusterId, clusterName, nodeName, version,
                clEvent.getClazz(), clEvent.getSubclass(),
                clEvent.getSeverity().getOrdinal(), initiator,
                clEvent.getPublisher(), clEvent.getEventSeqNo(),
                clEvent.getPid(), clEvent.getTimeStamp(), eventData);

        final ObjectName eventsEntryName = escEventsEntry.createObjectName(
                server);

        try {
            EscEventsTable.addEntry(escEventsEntry, eventsEntryName);
        } catch (SnmpStatusException e) {
            logger.log(Level.WARNING, "caught exception", e);
        }

        try {

            if ((server != null) && (eventsEntryName != null))
                server.registerMBean(escEventsEntry, eventsEntryName);
        } catch (Exception e) {
            logger.log(Level.WARNING, "caught exception", e);
        }

        // Append this entry to our FIFO lists of events
        eventEntriesList.add(escEventsEntry);
        eventAttributesEntriesList.add(attrEntriesList);

        // And retire the first entry in the list if we've too many

        if (eventEntriesList.size() > entriesCount) {
            removeFirstEntry();
        }

        /*
         * Now that we've updated the MIB, send a trap with all the
         * fixed fields of the event
         */

        SnmpVarBindList varBindList = new SnmpVarBindList("Trap");

        eventsEntryMeta.setInstance(escEventsEntry);

        for (int id = 1; id < 100; id++) {

            try {
                String oidName = eventsTableOid + "." + currentIndex + "." + id;
                SnmpOid oid = new SnmpOid(eventsTableOid + "." + id + "." +
                        currentIndex);
                SnmpValue value = eventsEntryMeta.get(id, null);

                SnmpVarBind varBind = new SnmpVarBind(oid, value);
                varBindList.addVarBind(varBind);
            } catch (Exception e) {

                // Expected when id goes past that of last attribute
                break;
            }
        }

        try {

            // Get the SNMP trap version, if not set, default to v2.
            int trapVersion = EventMibModule.getSNMPVersion();

            // default context is always null because JDMK doesn't yet support
            // multiple contexts.
            String defaultContext = "null";
            String defaultUsername = EventMibModule.getDefaultUser();
            int defaultSecurity = EventMibModule.getDefaultSecurity();

            switch (trapVersion) {

            case 2:
                logger.fine("Sending V2 Trap...");
                SnmpV3AdaptorModule.getSnmpV3AdaptorServer().snmpV2Trap(null,
                        // IP addresses
                    null, // read in multiple community string ????
                    trapOid, varBindList);

                break;

            case 3:
                logger.fine("Sending V3 Trap...");
                SnmpV3AdaptorModule.getSnmpV3AdaptorServer().snmpV3UsmTrap(null,
                        // IP addresses
                    defaultUsername, defaultSecurity, defaultContext, trapOid,
                    varBindList);

                break;

            default:
                logger.warning("SNMP Trap version not supported: " +
                    trapVersion);

                break;
            }

        } catch (NumberFormatException nfe) {
            logger.warning("Invalid SNMP version set");
            logger.log(Level.WARNING, nfe.getMessage(), nfe);
        } catch (Exception e) {
            logger.warning("Unable to send trap for OID : " + trapOid);
            logger.log(Level.WARNING, "caught exception", e);
        }

        // Indicate that next event should have next index
        currentIndex++;

    }

    /**
     * Removes first entry from the MIB tables
     */
    private void removeFirstEntry() {
        logger.fine("removing oldest entry in MIB tables");

        try {
            EscEventsEntryImpl oldEntry = (EscEventsEntryImpl) eventEntriesList
                .removeFirst();
            EscEventsTable.removeEntry(oldEntry);
        } catch (SnmpStatusException e) {
            logger.log(Level.WARNING, "caught exception", e);
        }

        List oldAttrs = (List) eventAttributesEntriesList.removeFirst();
        Iterator i = oldAttrs.iterator();

        while (i.hasNext()) {

            try {
                EscEventsAttributesEntryImpl attr =
                    (EscEventsAttributesEntryImpl) i.next();
                EscEventsAttributesTable.removeEntry(attr);

            } catch (SnmpStatusException e) {
                logger.log(Level.WARNING, "caught exception", e);
            }
        }
    }
}
