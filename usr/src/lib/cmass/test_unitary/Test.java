//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ident	"@(#)Test.java 1.9     08/05/20 SMI"
//

// JDK
import com.sun.cacao.ModuleMBean;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cacao.agent.JmxClient;
import com.sun.cacao.container.Container;
import com.sun.cacao.element.OperationalStateEnum;

import java.io.IOException;

import java.util.*;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;

// JMX
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;


public class Test {

    static private MBeanServerConnection mbsc = null;

    public static void main(String args[]) {

        doTest(args);

        System.exit(0);
    }

    public static void doTest(String args[]) {

        try {

            String agentLoc = args[0];
            String userName = args[1];
            String userPass = args[2];

            System.setProperty(Container.CACAO_PROPERTIES,
                "/etc/opt/SUNWcacao/cacao.properties");
            mbsc = JmxClient.getWellknownJmxClientConnection(agentLoc, null,
                    null).getMBeanServerConnection();

            // Find ourselves a module to query

            ObjectNameFactory onf = new ObjectNameFactory(ModuleMBean.class
                    .getPackage().getName());

            List mbeanSet = JmxClient.getMBeanProxies(mbsc, onf,
                    ModuleMBean.class, false, null);

            ModuleMBean mbean = (ModuleMBean) (mbeanSet.iterator().next());


            System.out.println("doing query on object " + mbean.getName());
// OperationalStateEnum localState = OperationalStateEnum.ENABLED;

// System.out.println("localstate == ENABLED : "+
// localState.equals(OperationalStateEnum.ENABLED));

            OperationalStateEnum state = mbean.getOperationalState();

            System.out.println("state = " + state);


            System.out.println("state == ENABLED : " +
                state.equals(OperationalStateEnum.ENABLED));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
