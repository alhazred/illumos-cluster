#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#


#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)runtest.sh	1.5	08/12/04 SMI"
#

ROOT_DIR=
JAVA_OPTS=
#JAVA_OPTS=-Xprof
#JAVA_OPTS=-Xint -Xprof

JDMK_CLASSPATH=`echo ${ROOT_DIR}/opt/SUNWjdmk/5.1/lib/*.jar . | sed 's+  *+:+g'`
OPENJDMK_CLASSPATH=`echo ${ROOT_DIR}/usr/share/lib/jdmk/*.jar | ${SED} 's+  *+:+g'`
JMX_CLASSPATH="${ROOT_DIR}/usr/jdk/latest/jre/lib/rt.jar"
CACAO_CLASSPATH=`echo ${ROOT_DIR}/opt/SUNWcacao/lib/*.jar | sed 's+  *+:+g'`
CMASS_CLASSPATH=`echo ${ROOT_DIR}/usr/cluster/lib/cmass/*.jar | sed 's+  *+:+g'`

CLASSPATH=${JDMK_CLASSPATH}:${JMX_CLASSPATH}:${CACAO_CLASSPATH}:${CMASS_CLASSPATH}:${OPENJDMK_CLASSPATH}

usage() {
    echo "usage: `basename $0` machine user pass [ testname ]"
    exit 1;
}

if [ $# -lt 3 ] ; then
    usage();
fi


java $JAVA_OPTS -Djavax.net.ssl.trustStore=${ROOT_DIR}/etc/opt/SUNWcacao/security/truststore -Djavax.net.ssl.keyStore=${ROOT_DIR}/etc/opt/SUNWcacao/security/keystore -Djavax.net.ssl.keyStorePassword=password -classpath ${CLASSPATH} Test $1 $2 $3 $4
