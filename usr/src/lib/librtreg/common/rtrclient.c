/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rtrclient.c	1.69	08/05/20 SMI"


/*
 * Resource Type Registration File Parser and Processor
 *
 * This file's top-level entry-point is "rtreg_parse", the Resource Type
 * Registration file parser that opens an RTR file and returns
 * a data-structure with complete RT registration information.
 * rtreg_parse() invokes a yacc parser interface.  The yacc parser
 * in turrn invokes the call-back functions in this file from it's
 * action routines to build the data-structures for the rtreg_parse client.
 *
 * Correctness checking on the contents of the RTR file is also done
 * here, and default values are filled-in on the completed parse data.
 * The results returned from the parser, if there are no errors,
 * is a data-structure containing complete registration
 * information for the RT.
 * The reason that complete checking and fill-in is done at this level
 * is:
 *	The parsing already has the registration information decomposed
 *      and identified for easy content checking.
 *
 *	Error messages can be formed with context information related
 *	to the RTR file text.
 *
 * Most of the content checking is done in the check_and_set_registration()
 * and check_and_set_paramtab() routines after the file parse is completed.
 * Simple checks on property or paramtable elements (for example,
 * typechecking on boolean or integer values) are done during
 * the parse in the action call-backs.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <clcomm_cluster_vm.h>
#include "rgm/rtreg.h"	/* data structure built by call-backs */
#include "rgm/scha_strings.h"	/* for MAXRGMVALUESLEN */
#include "scha_tags.h"	/* for non-extension property names */
#include "rtrclient.h"	/* call-back interface to parser */

/*
 * Data Structures Internal to the Action Routine Modules.
 * These structures build off the exported rtrdata.h structure,
 * and will be reduced to only the rtreg.h structures as the
 * parse progresses.
 *
 * The RtrPtab_T accumulates paramtable enteries and sorts them into
 * extension properties and system_defined properties as they are
 * collected.  Properties that are not declared as extensions are
 * checked against the list of names for system_defined properites
 * If the property checks-out as being system-defined , the paramtable
 * entry is stuck into location for that property, allowing
 * the parser to keep track of the properties that are not defined
 * in the paramtable.
 *
 * Extension properties are collected in a linked list that is
 * kept in the RtrPtab_T struct.
 *
 */

typedef enum RTR_SPTYPE {
	RTR_SPT_TO = 0,	/* timeout */
	RTR_SPT_FX,	/* fixed */
	RTR_SPT_SS,	/* scalable-service-related */
	RTR_SPT_OPT	/* optional */
} RTR_SPTYPE;
#define	KEYANDLEN(s) s, sizeof (s)-1
static struct {
	const char *name;
	const int len;
	RTR_Sysparam sysparamid;
	RTR_SPTYPE type;
} nametable[] = {
/* SCALABLE *must* remain the first entry-- this is used later */
KEYANDLEN(SCHA_SCALABLE),		RTR_SP_SCALABLE,	RTR_SPT_OPT,
KEYANDLEN(SCHA_START_TIMEOUT),		RTR_SP_START_TIMEOUT,	RTR_SPT_TO,
KEYANDLEN(SCHA_STOP_TIMEOUT),		RTR_SP_STOP_TIMEOUT,	RTR_SPT_TO,
KEYANDLEN(SCHA_VALIDATE_TIMEOUT),	RTR_SP_VALIDATE_TIMEOUT,
								RTR_SPT_TO,
KEYANDLEN(SCHA_UPDATE_TIMEOUT),		RTR_SP_UPDATE_TIMEOUT,	RTR_SPT_TO,
KEYANDLEN(SCHA_INIT_TIMEOUT),		RTR_SP_INIT_TIMEOUT,	RTR_SPT_TO,
KEYANDLEN(SCHA_FINI_TIMEOUT),		RTR_SP_FINI_TIMEOUT,	RTR_SPT_TO,
KEYANDLEN(SCHA_BOOT_TIMEOUT),		RTR_SP_BOOT_TIMEOUT,	RTR_SPT_TO,
KEYANDLEN(SCHA_MONITOR_START_TIMEOUT),	RTR_SP_MONITOR_START_TIMEOUT,
								RTR_SPT_TO,
KEYANDLEN(SCHA_MONITOR_STOP_TIMEOUT),	RTR_SP_MONITOR_STOP_TIMEOUT,
								RTR_SPT_TO,
KEYANDLEN(SCHA_MONITOR_CHECK_TIMEOUT),	RTR_SP_MONITOR_CHECK_TIMEOUT,
								RTR_SPT_TO,
KEYANDLEN(SCHA_PRENET_START_TIMEOUT),	RTR_SP_PRENET_START_TIMEOUT,
								RTR_SPT_TO,
KEYANDLEN(SCHA_POSTNET_STOP_TIMEOUT),	RTR_SP_POSTNET_STOP_TIMEOUT,
								RTR_SPT_TO,
KEYANDLEN(SCHA_FAILOVER_MODE),		RTR_SP_FAILOVER_MODE,	RTR_SPT_FX,
KEYANDLEN(SCHA_NETWORK_RESOURCES_USED),	RTR_SP_NETWORK_RESOURCES_USED,
								RTR_SPT_SS,
KEYANDLEN(SCHA_PORT_LIST),		RTR_SP_PORT_LIST,	RTR_SPT_SS,
KEYANDLEN(SCHA_LOAD_BALANCING_POLICY),	RTR_SP_LOAD_BALANCING_POLICY,
								RTR_SPT_SS,
KEYANDLEN(SCHA_LOAD_BALANCING_WEIGHTS),	RTR_SP_LOAD_BALANCING_WEIGHTS,
								RTR_SPT_SS,
KEYANDLEN(SCHA_AFFINITY_TIMEOUT),	RTR_SP_AFFINITY_TIMEOUT,
								RTR_SPT_SS,
KEYANDLEN(SCHA_UDP_AFFINITY),		RTR_SP_UDP_AFFINITY,	RTR_SPT_SS,
KEYANDLEN(SCHA_WEAK_AFFINITY),		RTR_SP_WEAK_AFFINITY,	RTR_SPT_SS,
KEYANDLEN(SCHA_GENERIC_AFFINITY),	RTR_SP_GENERIC_AFFINITY,
								RTR_SPT_SS,
KEYANDLEN(SCHA_ROUND_ROBIN),		RTR_SP_ROUND_ROBIN,	RTR_SPT_SS,
KEYANDLEN(SCHA_CONN_THRESHOLD),		RTR_SP_CONN_THRESHOLD,	RTR_SPT_SS,
KEYANDLEN(SCHA_CHEAP_PROBE_INTERVAL),	RTR_SP_CHEAP_PROBE_INTERVAL,
								RTR_SPT_OPT,
KEYANDLEN(SCHA_THOROUGH_PROBE_INTERVAL),
					RTR_SP_THOROUGH_PROBE_INTERVAL,
								RTR_SPT_OPT,
KEYANDLEN(SCHA_RETRY_COUNT),		RTR_SP_RETRY_COUNT,	RTR_SPT_OPT,
KEYANDLEN(SCHA_RETRY_INTERVAL),		RTR_SP_RETRY_INTERVAL,	RTR_SPT_OPT,
KEYANDLEN(SCHA_GLOBAL_ZONE_OVERRIDE),	RTR_SP_GLOBAL_ZONE_OVERRIDE,
								RTR_SPT_OPT
};

#define	N_SYS_PARAMS (sizeof (nametable) / sizeof (nametable[0]))

/*
 * The system defined properties have predetermined type and description,
 * so these attributes are not allowed to be declared in the file paramtable.
 * Essential attributes, such as type, are filled-in during
 * RT registration file processing.
 *
 * The following data is used to fill-in the ENUM type of FAILOVER_MODE
 * paramtable entries.  As with system-define property-names, it's OK
 * that the strings are literals: memory management of Rtr data structures
 * does not include freeing of string data.  Normal Rtr strings point into
 * the input buffer holding the file.
 */
static RtrString fom_enumlist[] = {
	KEYANDLEN(SCHA_NONE),	RTR_STRING, 0,
	KEYANDLEN(SCHA_HARD),	RTR_STRING, 0,
	KEYANDLEN(SCHA_SOFT),	RTR_STRING, 0,
	KEYANDLEN(SCHA_RESTART_ONLY),	RTR_STRING, 0,
	KEYANDLEN(SCHA_LOG_ONLY),	RTR_STRING, 0
};
#define	N_FOM_ENUM (sizeof (fom_enumlist) / sizeof (fom_enumlist[0]))

/*
 * Constants for filling in default attributes values in the Paramtable
 */

#define	PARAM_TIMEOUT_DEFAULT		3600
#define	PARAM_TIMEOUT_MIN		1

/*
 * Note: any change to the set of permissible Load_balancing_policy
 * strings needs to be made here and in the ssm_wrapper program.
 * The first entry is the default.
 */
static RtrString lbp_strings[] = {
	KEYANDLEN("LB_WEIGHTED"),	RTR_STRING, 0,
	KEYANDLEN("LB_STICKY"),		RTR_STRING, 0,
	KEYANDLEN("LB_STICKY_WILD"),	RTR_STRING, 0
};
#define	N_LBP_STR (sizeof (lbp_strings) / sizeof (lbp_strings[0]))

typedef struct RtrExtlist_T RtrExtlist_T;

struct RtrExtlist_T {
	RtrExtlist_T	*next;
	RtrParam_T	*param;
};


typedef struct RtrPtab_T RtrPtab_T;

struct RtrPtab_T {
	RtrParam_T	*sys_param[N_SYS_PARAMS];
	uint_t		sys_param_count;
	uint_t		extension_listsz;
	RtrExtlist_T	*extensions;
};


/*
 * Values are simply counted while collecting them
 * into a linked list.
 */
typedef struct RtrVlist_T RtrVlist_T;

struct RtrVlist_T {
	uint_t		listsz;
	RtrVlist_T	*next;
	RtrValueToken	value;
};

typedef struct RtrUpglist_T RtrUpglist_T;

struct RtrUpglist_T {
	RtrUpg_T	*entry;
	RtrUpglist_T	*next;
};

typedef struct RtrRTUinfo_T RtrRTUinfo_T;

struct RtrRTUinfo_T {
	boolean_t	sc30_rtr;
	RtrUpglist_T	*upgradelist;
	RtrUpglist_T	*downgradelist;
};


/*
 * Local memory-managment functions.  New and Delete functions for structures
 */
static RtrPtab_T *	new_RtrPtab_T(void);
static void		delete_RtrPtab_T(RtrPtab_T*);
static RtrVlist_T*	new_RtrVlist_T(RtrVlist_T *nxt);
static void		delete_RtrVlist_T(RtrVlist_T*);
static Rtr_T *		new_Rtr_T(void);
static void		delete_Rtr_T(Rtr_T *);
static RtrRTUinfo_T *	new_RtrRTUinfo_T(void);
static void		delete_RtrRTUinfo_T(RtrRTUinfo_T *);
static RtrUpglist_T *	new_RtrUpglist_T(RtrUpglist_T *);
static void		delete_RtrUpglist_T(RtrUpglist_T *);
static RtrParam_T *	new_RtrParam_T(void);
static void		delete_RtrParam_T(RtrParam_T *);
static RtrExtlist_T *	new_RtrExtlist_T(RtrExtlist_T *nxt);
static void		delete_RtrExtlist_T(RtrExtlist_T *);
static RtrFileResult *	new_RtrFileResult(char *abuffer);

/* Parser Call-backs, and Callback Structure initialization */

static void		syntax_error(const char *msg, const char *loc, int ln);
static void		rtr_file(Rtr_T * proplist, RtrRTUinfo_T *rtugp,
						RtrPtab_T *paramtab);
static Rtr_T *		rtdec(RtrValueToken nametoken);
static Rtr_T *		propsimple(Rtr_T *, RTRP_Code propcode);
static Rtr_T *		propval(Rtr_T *, RTRP_Code propcode,
						RtrValueToken propvalue);
static Rtr_T *		proppkgs(Rtr_T *, RtrVlist_T *vlistp);
static RtrVlist_T *	valuelist(RtrVlist_T *listp, RtrValueToken element);
static RtrPtab_T *	parameter(RtrPtab_T *paramtable, RtrParam_T *entry);
static RtrParam_T *	paramdec(RtrValueToken nametoken);
static RtrParam_T *	paramsimple(RtrParam_T *, RTRP_Code propcode);
static RtrParam_T *	paramval(RtrParam_T *, RTRP_Code propcode,
						RtrValueToken propvalue);
static RtrParam_T *	paramenum(RtrParam_T *, RtrVlist_T *enumlist);
static RtrParam_T *	paramdefault(RtrParam_T *, RtrVlist_T *vlist);
static RtrRTUinfo_T *	upgheader(RtrRTUinfo_T *rtup, RTRV_Type header_found);
static RtrRTUinfo_T *	upglist(RtrRTUinfo_T *upgradelist,
			RtrValueToken rt_version, RtrValueToken tunability);
static RtrRTUinfo_T *	downglist(RtrRTUinfo_T *upgradelist,
			RtrValueToken rt_version, RtrValueToken tunability);

static Rtr_Callbacks localcbs;

static void
init_localcbs()
{
	localcbs.syntax_error	=  syntax_error;
	localcbs.rtr_file	=  rtr_file;
	localcbs.rtdec		=  rtdec;
	localcbs.propsimple	=  propsimple;
	localcbs.propval	=  propval;
	localcbs.proppkgs	=  proppkgs;
	localcbs.upgheader	=  upgheader;
	localcbs.upglist	=  upglist;
	localcbs.downglist	=  downglist;
	localcbs.valuelist	=  valuelist;
	localcbs.parameter	=  parameter;
	localcbs.paramdec	=  paramdec;
	localcbs.paramsimple	=  paramsimple;
	localcbs.paramval	=  paramval;
	localcbs.paramenum	=  paramenum;
	localcbs.paramdefault	=  paramdefault;
}

static void
assign_string_value(RtrString *strp, const RtrValueToken *valp)
{
	if (strp == NULL || valp == NULL) {
		return;
	}
	strp->start = valp->start;
	strp->len = valp->len;
	strp->lineno = valp->lineno;
	if (valp->vt == RTRV_NULL || valp->len < 1) {
		strp->value = RTR_NULL;
	} else {
		strp->value = RTR_STRING;
	}
}


/*
 * This routine initialize failover_mode property.
 * If DEFAULT is not declared, set the default value to SCHA_NONE
 */
static void
set_fomtype(RtrParam_T *param)
{
	uint_t ix = 0;
	RtrString *array = NULL;

	/*
	 * since addition of RESTART_ONLY/LOG_ONLY failover mode,
	 * the failover_mode system property doesn't use the enumlist
	 * any more.
	 * However we keep writing the old enumlist in CCR, which is
	 * only a subset of the supported values, otherwise
	 * it breaks downgrading a cluster.
	 */
	array = (RtrString *) malloc(sizeof (RtrString) * N_FOM_ENUM);
	if (array == NULL) {
		return;
	}

	for (; ix < N_FOM_ENUM; ix++) {
		array[ix] = fom_enumlist[ix];
	}

	param->enumlist = array;
	param->enumlistsz = N_FOM_ENUM;


	param->type = RTR_ENUM;
	if (param->defaultstr.value == RTR_NONE ||
	    param->defaultstr.value == RTR_NULL)
		param->defaultstr = fom_enumlist[0];
}

/*
 * Variables to hold information used in case the parse fails, or
 * other error occurs
 */
static int error_lineno;	/* line number in rtr file that idicating */
				/* the attribute failed on parser check */
static const char *error_bufloc;	/* the pointer to the property name */
					/* or attribute name which has errore */
static int error_code;
static Rtr_T *error_cleanup_rtr;
static RtrVlist_T *error_cleanup_vlist;
static RtrPtab_T *error_cleanup_ptab;
static RtrRTUinfo_T *error_cleanup_upginfo;
static RtrParam_T *error_cleanup_param;

/* convenient constants */
static const RtrString nullstring = { NULL, 0, RTR_NULL, 0 };
static const RtrString emptystring = { "", 0, RTR_STRING, 0 };

/*
 * Variables through which action routines "return" parse results
 * to the top-level parser interface.
 */
static Rtr_T 		*result_rtregistration;
static RtrRTUinfo_T	*result_rtupgradeinfo;
static RtrPtab_T	*result_paramtab;


#ifdef YYDEBUG
#define	trace(a, b, c) println(a, b, c, 0);
static void println(const char *msg, const char *start, int len, int indent);
#else
#define	trace(a, b, c)
#endif

/*
 * Error function, takes message and input buffer location
 * msg is yacc's "syntax error"
 */
static void
syntax_error(const char *msg, const char *loc, int ln)
{
	error_lineno = ln;
	error_bufloc = loc;

	/* if the buf pointer is NULL, have to set the pointer to a blank */
	if (error_bufloc == NULL)
		error_bufloc = "";
	error_code = RTR_STATUS_PARSE_ERR;
/*
 * Suppress lint Info(715) [c:0]: msg (line 316) not referenced
 */
} /*lint !e715 */

/*
 * Called at the end of input of resource type registration property list
 * and the paramtable.  Results returned to top-level parse routine
 * via file-scope varilables
 */
static void
rtr_file(Rtr_T *rtrp, RtrRTUinfo_T *rtugp, RtrPtab_T *ptabp)
{
	result_rtregistration = rtrp;
	result_rtupgradeinfo = rtugp;
	result_paramtab = ptabp;

	/* Completed parse data structures are now delivered */
	/* into the file-scope result_rtregistration, result_rtupgradeinfo */
	/* and result_paramtab variables above. */
	/* Clear-out intermediate stuff that was saved in case */
	/* a syntax-error terminated the parser before this action */
	/* returned the data-structures */
	error_cleanup_rtr = NULL;
	error_cleanup_vlist = NULL;
	error_cleanup_ptab = NULL;
	error_cleanup_upginfo = NULL;
	error_cleanup_param = NULL;
}

/*
 * The resource-type name declaration.
 * This is the start of the resource-type property list
 */
static Rtr_T *
rtdec(RtrValueToken nmtok)
{
	trace(NOI18N("rtrdec"), nmtok.start, nmtok.len)

	Rtr_T *rtd = error_cleanup_rtr = new_Rtr_T();
	if (rtd != NULL) {
		assign_string_value(&rtd->rtname, &nmtok);
	}
	return (rtd);
}

/*
 * Add a simple property to the resource type registration
 */
static Rtr_T *
propsimple(Rtr_T *rtrp, RTRP_Code propcode)
{
	trace(NOI18N("propsimple"), NULL, 0)
	if (rtrp == NULL) {
		/* Shouldn't happen. */
		rtrp = error_cleanup_rtr = new_Rtr_T();
	}
	if (rtrp == NULL) {
		return (NULL);
	}
	switch (propcode) {
	case RTRP_SINGLE_INSTANCE:
		rtrp->single_instance = RTR_TRUE;
		break;
	case RTRP_FAILOVER:
		rtrp->failover = RTR_TRUE;
		break;
	case RTRP_GLOBALZONE:
		rtrp->globalzone = RTR_TRUE;
		break;
	case RTRP_PROXY:
		rtrp->proxy = RTR_TRUE;
		break;
	case RTRP_RT_SYSTEM:
		rtrp->rt_system = RTR_TRUE;
		break;
	default:
		/* Unrecognized code */
		/* Set error information */
		error_code = RTR_STATUS_INVALID_PROPVAL;
		rtr_get_current_location(&error_lineno, &error_bufloc);
		break;
	}
	return (rtrp);
}

/*
 * Add a property with a value setting
 * to the resource type registration
 */
static Rtr_T *
propval(Rtr_T *rtrp, RTRP_Code propcode, RtrValueToken propvalue)
{
	RtrString *string_target = NULL;

	trace(NOI18N("propval"), propvalue.start, propvalue.len)
	if (rtrp == NULL) {
		/* Shouldn't happen. */
		rtrp = error_cleanup_rtr = new_Rtr_T();
	}
	if (rtrp == NULL) {
		return (NULL);
	}
	switch (propcode) {
	case RTRP_API_VERSION:
		rtrp->apiversion = propvalue.num;
		rtrp->apiversion_isset = 1;
		break;

	case RTRP_SINGLE_INSTANCE:
		if (propvalue.vt == RTRV_FALSE)
			rtrp->single_instance = RTR_FALSE;
		else if (propvalue.vt == RTRV_TRUE)
			rtrp->single_instance = RTR_TRUE;
		else {
			/* unrecognized value */
			error_code = RTR_STATUS_INVALID_PROPVAL;
			rtr_get_current_location(&error_lineno, &error_bufloc);

			/* Give the property name instead of current token */
			error_bufloc = SCHA_SINGLE_INSTANCE;
		}
		break;

	case RTRP_RT_BASEDIR:
		string_target = &rtrp->basedir;
		break;
	case RTRP_BOOT:
		string_target = &rtrp->boot;
		break;

	case RTRP_FAILOVER:
		if (propvalue.vt == RTRV_FALSE)
			rtrp->failover = RTR_FALSE;
		else if (propvalue.vt == RTRV_TRUE)
			rtrp->failover = RTR_TRUE;
		else {
			/* unrecognized value */
			error_code = RTR_STATUS_INVALID_PROPVAL;
			rtr_get_current_location(&error_lineno, &error_bufloc);

			/* Give the property name instead of current token */
			error_bufloc = SCHA_FAILOVER;
		}
		break;

	case RTRP_PROXY:
		if (propvalue.vt == RTRV_FALSE)
			rtrp->proxy = RTR_FALSE;
		else if (propvalue.vt == RTRV_TRUE)
			rtrp->proxy = RTR_TRUE;
		else {
			/* unrecognized value */
			error_code = RTR_STATUS_INVALID_PROPVAL;
			rtr_get_current_location(&error_lineno, &error_bufloc);

			/* Give the property name instead of current token */
			error_bufloc = SCHA_PROXY;
		}
		break;

	case RTRP_GLOBALZONE:
		if (propvalue.vt == RTRV_FALSE)
			rtrp->globalzone = RTR_FALSE;
		else if (propvalue.vt == RTRV_TRUE)
			rtrp->globalzone = RTR_TRUE;
		else {
			/* unrecognized value */
			error_code = RTR_STATUS_INVALID_PROPVAL;
			rtr_get_current_location(&error_lineno, &error_bufloc);

			/* Give the property name instead of current token */
			error_bufloc = SCHA_GLOBALZONE;
		}
		break;

	case RTRP_FINI:
		string_target = &rtrp->fini;
		break;
	case RTRP_INIT:
		string_target = &rtrp->init;
		break;

	case RTRP_INIT_NODES:
		if (propvalue.vt == RTRV_RG_PRIMARIES)
			rtrp->init_nodes = RTR_RG_PRIMARIES;
		else if (propvalue.vt == RTRV_RT_INSTALLED_NODES)
			rtrp->init_nodes = RTR_RT_INSTALLED_NODES;
		else {
			/* unrecognized value */
			error_code = RTR_STATUS_INVALID_PROPVAL;
			rtr_get_current_location(&error_lineno, &error_bufloc);

			/* Give the property name instead of current token */
			error_bufloc = SCHA_INIT_NODES;
		}
		break;

	case RTRP_MONITOR_CHECK:
		string_target = &rtrp->monitor_check;
		break;
	case RTRP_MONITOR_START:
		string_target = &rtrp->monitor_start;
		break;
	case RTRP_MONITOR_STOP:
		string_target = &rtrp->monitor_stop;
		break;
	case RTRP_POSTNET_STOP:
		string_target = &rtrp->postnet_stop;
		break;
	case RTRP_PRENET_START:
		string_target = &rtrp->prenet_start;
		break;
	case RTRP_RESOURCE_TYPE:
		/* shouldn't happen */
		string_target = &rtrp->rtname;
		break;
	case RTRP_RT_DESCRIPTION:
		string_target = &rtrp->rt_description;
		break;
	case RTRP_START:
		string_target = &rtrp->start;
		break;
	case RTRP_STOP:
		string_target = &rtrp->stop;
		break;

	case RTRP_SYSDEFINED_TYPE:
		if (propvalue.vt == RTRV_SHARED_ADDRESS)
			rtrp->sysdefined_type = RTR_SHARED_ADDRESS;
		else if (propvalue.vt == RTRV_LOGICAL_HOSTNAME)
			rtrp->sysdefined_type = RTR_LOGICAL_HOSTNAME;
		else if (propvalue.vt == RTRV_NULL)
			rtrp->sysdefined_type = RTR_NULL;
		else {
			/* unrecognized value */
			error_code = RTR_STATUS_INVALID_PROPVAL;
			rtr_get_current_location(&error_lineno, &error_bufloc);

			/* Give the property name instead of current token */
			error_bufloc = SCHA_SYSDEFINED_TYPE;
		}
		break;

	case RTRP_UPDATE:
		string_target = &rtrp->update;
		break;
	case RTRP_VALIDATE:
		string_target = &rtrp->validate;
		break;
	case RTRP_VENDOR_ID:
		string_target = &rtrp->vendor_id;
		break;
	case RTRP_RT_VERSION:
		string_target = &rtrp->version;
		break;
	case RTRP_RT_SYSTEM:
		if (propvalue.vt == RTRV_FALSE)
			rtrp->rt_system = RTR_FALSE;
		else if (propvalue.vt == RTRV_TRUE)
			rtrp->rt_system = RTR_TRUE;
		else {
			/* unrecognized value */
			error_code = RTR_STATUS_INVALID_PROPVAL;
			rtr_get_current_location(&error_lineno, &error_bufloc);

			/* Give the property name instead of current token */
			error_bufloc = SCHA_RT_SYSTEM;
		}
		break;

	default:
		/* Unrecognized code */
		/* Set error information */
		error_code = RTR_STATUS_INVALID_PROP;
		rtr_get_current_location(&error_lineno, &error_bufloc);
		break;
	}

	if (string_target != NULL) {
		assign_string_value(string_target, &propvalue);
	}

	return (rtrp);
}

/*
 * Add a package list property to the resource type registration
 */
static Rtr_T *
proppkgs(Rtr_T *rtrp, RtrVlist_T *vlistp)
{
	trace(NOI18N("proppkgs"), NULL, 0)

	/*
	 * vlistp may be NULL is there is an explicit empty setting.
	 * Don't bother creating an explicit NULL setting
	 * in this case, just leave the value unset
	 */

	if (rtrp == NULL) {
		/* Shouldn't happen. */
		rtrp = error_cleanup_rtr = new_Rtr_T();
	}
	if (rtrp && vlistp && !rtrp->pkglist) {
		uint_t listsz = vlistp->listsz;
		RtrVlist_T *elp = vlistp;
		RtrString *array = (RtrString *)
		    malloc(sizeof (RtrString) * listsz);
		if (array != NULL) {
			int ix = (int)(listsz - 1);

			for (; ix >= 0 && elp; ix--) {
				assign_string_value(array + ix, &elp->value);
				elp = elp ->next;
			}
			rtrp->pkglist = array;
			rtrp->pkglistsz = (int)listsz;
		}
	}
	if (vlistp) {
		delete_RtrVlist_T(vlistp);
		error_cleanup_vlist = NULL;
	}
	return (rtrp);

}

/*
 * Add to a list of string values.
 * The first argument may be NULL, indicating addition of the
 * first list element.
 */
static RtrVlist_T *
valuelist(RtrVlist_T *vlistp, RtrValueToken element)
{
	RtrVlist_T *newlist = new_RtrVlist_T(vlistp);

	trace(NOI18N("valuelist"), element.start, element.len)
	if (newlist == NULL) {
		return (vlistp);
	}
	error_cleanup_vlist = newlist;
	newlist->value = element;

	return (newlist);
}

/*
 * If Upgrade header is found in RTR file, this rtr file is SC31 or post-SC31
 * version.  Set the sc30_rtr to false.   Otherwise, this rtr file si SC30
 * version.
 */
static RtrRTUinfo_T *
upgheader(RtrRTUinfo_T *rtup, RTRV_Type header_found)
{
	if (rtup == NULL) {
		/* Either the rtr file is sc30 version or there is no */
		/* #$upgrade_from or #$downgrade_to in sc31 rtr file */
		rtup = new_RtrRTUinfo_T();
	}

	if (rtup == NULL) {
		error_code = RTR_STATUS_INTERNAL_ERR;
		rtr_get_current_location(&error_lineno, &error_bufloc);
		/* Give the property name instead of current token */
		error_bufloc = SCHA_UPGRADE_HEADER;
		return (NULL);
	}

	if (header_found == RTRV_TRUE)
		rtup->sc30_rtr = B_FALSE;
	else
		rtup->sc30_rtr = B_TRUE;

	return (rtup);
}

/*
 * Add the info of a upgrade_from or downgrade_to directive.
 */
static void
add_rtupgrade_entry(RtrUpglist_T **upgl, RtrValueToken rt_version,
    RtrValueToken tunability)
{
	RtrUpglist_T *lp;

	if (tunability.vt == RTRV_NULL)
		return;

	/* Check for duplicate enteries */

	for (lp = *upgl; lp != NULL; lp = lp->next) {
		if (lp->entry->upg_version.len == rt_version.len &&
		    strncasecmp(lp->entry->upg_version.start, rt_version.start,
		    (uint_t)rt_version.len) == 0) {
			error_code = RTR_STATUS_INVALID_PROPVAL;
			rtr_get_current_location(&error_lineno, &error_bufloc);
			return;
		}
	}

	lp = new_RtrUpglist_T(*upgl);
	if (lp == NULL) {
		error_code = RTR_STATUS_INTERNAL_ERR;
		rtr_get_current_location(&error_lineno, &error_bufloc);
		return;
	}

	*upgl = lp;

	assign_string_value(&lp->entry->upg_version, &rt_version);

	switch (tunability.vt) {
	case RTRV_ANYTIME:
		lp->entry->upg_tunability = RTR_ANYTIME;
		break;
	case RTRV_AT_CREATION:
		lp->entry->upg_tunability = RTR_AT_CREATION;
		break;
	case RTRV_WHEN_DISABLED:
		lp->entry->upg_tunability = RTR_WHEN_DISABLED;
		break;
	case RTRV_WHEN_OFFLINE:
		lp->entry->upg_tunability = RTR_WHEN_OFFLINE;
		break;
	case RTRV_WHEN_UNMANAGED:
		lp->entry->upg_tunability = RTR_WHEN_UNMANAGED;
		break;
	case RTRV_WHEN_UNMONITORED:
		lp->entry->upg_tunability = RTR_WHEN_UNMONITORED;
		break;
	case RTRV_STRING:
	case RTRV_NUM:
	case RTRV_TRUE:
	case RTRV_FALSE:
	case RTRV_RG_PRIMARIES:
	case RTRV_RT_INSTALLED_NODES:
	case RTRV_LOGICAL_HOSTNAME:
	case RTRV_SHARED_ADDRESS:
	case RTRV_NONE_STRING:
	case RTRV_NULL:
	case RTRV_NONE:
	default:
		error_code = RTR_STATUS_INVALID_ATTRVAL;
		error_bufloc = rt_version.start;
		/* if the buf pointer is NULL, have to set */
		/* the pointer to a blank */
		if (error_bufloc == NULL)
			error_bufloc = "";
		error_lineno = rt_version.lineno;
		return;
	}

	error_cleanup_upginfo = NULL;
}


/*
 * Add a upgrade_from entry to the upgradelist list.
 * The upgradelist may be NULL
 */
static RtrRTUinfo_T *
upglist(RtrRTUinfo_T *upgl, RtrValueToken rt_version, RtrValueToken tunability)
{
	error_cleanup_upginfo = upgl;

	if (upgl == NULL)
		upgl = new_RtrRTUinfo_T();

	if (upgl == NULL) {
		error_code = RTR_STATUS_INTERNAL_ERR;
		rtr_get_current_location(&error_lineno, &error_bufloc);
		return (NULL);
	}

	add_rtupgrade_entry(&upgl->upgradelist, rt_version, tunability);

	return (upgl);
}


/*
 * Add a downgrade_to entry to the downgradelist list.
 * The downgradelist may be NULL
 */
static RtrRTUinfo_T *
downglist(RtrRTUinfo_T *upgl, RtrValueToken rt_version,
    RtrValueToken tunability)
{
	error_cleanup_upginfo = upgl;

	if (upgl == NULL)
		upgl = new_RtrRTUinfo_T();

	if (upgl == NULL) {
		error_code = RTR_STATUS_INTERNAL_ERR;
		rtr_get_current_location(&error_lineno, &error_bufloc);
		return (NULL);
	}

	add_rtupgrade_entry(&upgl->downgradelist, rt_version, tunability);

	return (upgl);
}


static int
find_sysparam(RtrParam_T *entry)
{
	uint_t ix = 0;
	int found = 0;

	for (; ix < (uint_t)N_SYS_PARAMS; ix++) {
		if (nametable[ix].len == entry->propname.len &&
		    (strncasecmp(nametable[ix].name, entry->propname.start,
		    (size_t)entry->propname.len) == 0)) {
			found = 1;
			break;
		}
	}
	return (found? (int)ix : -1);
}

/*
 * Add an entry to the paramtable.
 * The paramtable ptab may be NULL
 */
static RtrPtab_T *
parameter(RtrPtab_T *ptab, RtrParam_T *entry)
{
	int found_error = 0;

	trace(NOI18N("parameter"), NULL, 0)

	if (ptab == NULL) {
		ptab = error_cleanup_ptab = new_RtrPtab_T();
	}
	if (ptab == NULL) {
		return (NULL);
	}
	if (entry == NULL) {
		return (ptab);
	}

	if (entry->extension != RTR_TRUE) {
		/*
		 * Look-up name of system-defined property,
		 * and change the name to the "standard" string
		 * capitalization may be different in the file.
		 * NOTE: The strings pointed-to by the normal
		 * propname.start method should not be deallocated
		 * when the RtrParam_T is deleted, so it is OK to set
		 * them to the nametable entries.
		 * (For normal parameters, the name strings are in the
		 *  file input buffer.)
		 *
		 * Check that Type and Description attributes are not declared
		 * for system defined properties, and that there are no
		 * duplicate enteries.
		 */
		int ix = find_sysparam(entry);

		if (entry->per_node == RTR_TRUE) {
			found_error = 1;
			error_code = RTR_STATUS_INVALID_PARAM;
		}

		if (ix >= 0 && entry->type == RTR_NONE &&
		    entry->description.value == RTR_NONE &&
		    ptab->sys_param[ix] == NULL) {
			entry->propname.start = nametable[ix].name;
			entry->propname.len = nametable[ix].len;
			entry->sysparamid = nametable[ix].sysparamid;
			ptab->sys_param[ix] = entry;
			ptab->sys_param_count++;
		} else {
			/*
			 * duplicate or erroneous non-extension property.
			 * Mark the error;
			 */

			if (ix < 0) {
				/* not an identified system-defined property */
				error_code = RTR_STATUS_INVALID_SYSDEF;
			} else if (ptab->sys_param[ix] != NULL) {
				/* duplicate entry */
				error_code = RTR_STATUS_INVALID_PARAM;
			} else {
				/* Type or description set */
				error_code = RTR_STATUS_INVALID_ATTRVAL;
			}
			found_error = 1;
		}
	} else {
		/*
		 * Extension property, check and add to extension property list
		 */
		RtrExtlist_T * exlistp = NULL;

		/* Extension properties must have a Type indicated */
		if (entry->type == RTR_NONE) {
			found_error = 1;
			error_code = RTR_STATUS_INVALID_ATTRVAL;
		}
		if ((entry->per_node == RTR_TRUE) && (entry->type
		    == RTR_STRINGARRAY)) {
			found_error = 1;
			error_code = RTR_STATUS_INVALID_ATTRVAL;
		}
		if ((entry->defaultboolean == RTR_NONE) &&
		    (entry->defaultstr.len == 0) &&
		    (entry->defaultstr.value == RTR_NONE) &&
		    (entry->defaultstr.start == NULL) &&
		    (entry->per_node == RTR_TRUE)) {
			/*
			 * Default attribute not specified!
			 */
			found_error = 1;
			error_code = RTR_STATUS_INVALID_ATTRVAL;
		}
		/* Check for duplicate enteries */
		for (exlistp = ptab->extensions;
		    exlistp != NULL && found_error == 0;
		    exlistp = exlistp->next) {
			RtrString *propname = &exlistp->param->propname;
			RtrString *entryname = &entry->propname;

			if (entryname->len == propname->len &&
			    strncasecmp(entryname->start, propname->start,
			    (uint_t)propname->len) == 0) {
				found_error = 1;
				error_code = RTR_STATUS_INVALID_PARAM;
			}
		}
		if (found_error == 0) {
			RtrExtlist_T *newexp =
			    new_RtrExtlist_T(ptab->extensions);
			if (newexp != NULL) {
				newexp->param = entry;
				ptab->extensions = newexp;
				ptab->extension_listsz++;
			} else {
				found_error = 1;
				error_code = RTR_STATUS_INTERNAL_ERR;
			}
		}
	}

	if (found_error != 0) {
		/*
		 * The error_code is already set
		 * Give property name as error location and
		 * delete the entry
		 */
		error_bufloc = entry->propname.start;

		/* if the buf pointer is NULL, have to set the pointer */
		/* to a blank */
		if (error_bufloc == NULL)
			error_bufloc = "";
		error_lineno = entry->propname.lineno;
		delete_RtrParam_T(entry);
	}
	/*
	 * The param entry is now part of the ptab table or has been deleted.
	 * Null-out the pointer that is kept to clean-up if there
	 * is a syntax error before param processing is completed
	 */
	error_cleanup_param = NULL;

	return (ptab);
}

/*
 * The property-name attribute of a paramtable item.
 * This is the start of the paramtable entry anttribute list
 */
static RtrParam_T *
paramdec(RtrValueToken nmtok)
{
	RtrParam_T *param = error_cleanup_param = new_RtrParam_T();

	trace(NOI18N("paramdec"), nmtok.start, nmtok.len)
	if (param != NULL) {
		assign_string_value(&param->propname, &nmtok);
	}
	return (param);
}

/*
 * Add a simple attribute to a parameter
 */
static RtrParam_T *
paramsimple(RtrParam_T *param, RTRP_Code propcode)
{
	trace(NOI18N("paramsimple"), NULL, 0)
	if (param == NULL) {
		/* Shouldn't happen. */
		param = error_cleanup_param = new_RtrParam_T();
	}
	if (param == NULL) {
		return (NULL);
	}
	switch (propcode) {
	case RTRP_BOOLEAN:
		param->type = RTR_BOOLEAN;
		break;
	case RTRP_EXTENSION:
		param->extension = RTR_TRUE;
		break;
	case RTRP_PER_NODE:
		param->per_node = RTR_TRUE;
		break;
	case RTRP_INT:
		param->type = RTR_INT;
		break;
	case RTRP_STRINGTYPE:
		param->type = RTR_STRING;
		break;
	case RTRP_STRINGARRAY:
		param->type = RTR_STRINGARRAY;
		break;
	case RTRP_TUNABLE:
		param->tunable = RTR_ANYTIME;
		break;
	default:
		/* Unrecognized code */
		/* Set error information */
		error_code = RTR_STATUS_INVALID_ATTRVAL;
		rtr_get_current_location(&error_lineno, &error_bufloc);
		break;
	}

	return (param);
}

/*
 * Add an attribute with a value setting to a parameter
 */
static RtrParam_T *
paramval(RtrParam_T *param, RTRP_Code propcode, RtrValueToken propvalue)
{
	trace(NOI18N("paramval"), propvalue.start, propvalue.len)
	if (param == NULL) {
		/* Shouldn't happen. */
		param = error_cleanup_param = new_RtrParam_T();
	}
	if (param == NULL) {
		return (NULL);
	}
	switch (propcode) {
	case RTRP_BOOLEAN:
		if (propvalue.vt != RTRV_FALSE) {
			param->type = RTR_BOOLEAN;
		}
		break;
	case RTRP_DESCRIPTION:
		assign_string_value(&param->description, &propvalue);
		break;
	case RTRP_EXTENSION:
		if (propvalue.vt == RTRV_FALSE)
			param->extension = RTR_FALSE;
		else if (propvalue.vt == RTRV_TRUE)
			param->extension = RTR_TRUE;
		else {
			/* unrecognized value */
			error_code = RTR_STATUS_INVALID_ATTRVAL;
			error_bufloc = propvalue.start;

			/* if the buf pointer is NULL, have to set */
			/* the pointer to a blank */
			if (error_bufloc == NULL)
				error_bufloc = "";
			error_lineno = propvalue.lineno;
		}
		break;
	case RTRP_PER_NODE:
		if (propvalue.vt == RTRV_FALSE)
			param->per_node = RTR_FALSE;
		else if (propvalue.vt == RTRV_TRUE)
			param->per_node = RTR_TRUE;
		else {
			/* unrecognised value */
			error_code = RTR_STATUS_INVALID_ATTRVAL;
			error_bufloc = propvalue.start;

			/* if the buf pointer is NULL, have to set */
			/* the pointer to a blank */
			if (error_bufloc == NULL)
				error_bufloc = "";
			error_lineno = propvalue.lineno;
		}
		break;
	case RTRP_INT:
		if (propvalue.vt != RTRV_FALSE) {
			param->type = RTR_INT;
		}
		break;
	case RTRP_MAX:
	case RTRP_MAXLENGTH:
		param->rtr_max = propvalue.num;
		param->rtr_max_isset = 1;
		break;
	case RTRP_ARRAY_MAXSIZE:
		param->arraymax = propvalue.num;
		param->arraymax_isset = 1;
		break;
	case RTRP_MIN:
	case RTRP_MINLENGTH:
		param->rtr_min = propvalue.num;
		param->rtr_min_isset = 1;
		break;
	case RTRP_ARRAY_MINSIZE:
		param->arraymin = propvalue.num;
		param->arraymin_isset = 1;
		break;
	case RTRP_PROPERTY:
		/* Shouldn't happen */
		assign_string_value(&param->propname, &propvalue);
		error_code = RTR_STATUS_INVALID_ATTRVAL;
		error_bufloc = propvalue.start;
		/* if the buf pointer is NULL, have to set */
		/* the pointer to a blank */
		if (error_bufloc == NULL)
			error_bufloc = "";
		error_lineno = propvalue.lineno;
		break;
	case RTRP_STRINGTYPE:
		if (propvalue.vt != RTRV_FALSE) {
			param->type = RTR_STRING;
		}
		break;
	case RTRP_STRINGARRAY:
		if (propvalue.vt != RTRV_FALSE) {
			param->type = RTR_STRINGARRAY;
		}
		break;
	case RTRP_TUNABLE:
		switch (propvalue.vt) {
		case RTRV_TRUE:
			param->tunable = RTR_ANYTIME;
			break;
		case RTRV_NONE_STRING:	/* Explicit NONE string */
		case RTRV_NULL:		/* Explicit NULL value */
		case RTRV_FALSE:
			param->tunable = RTR_FALSE;
			break;
		case RTRV_AT_CREATION:
			param->tunable = RTR_AT_CREATION;
			break;
		case RTRV_ANYTIME:
			param->tunable = RTR_ANYTIME;
			break;
		case RTRV_WHEN_DISABLED:
			param->tunable = RTR_WHEN_DISABLED;
			break;
		case RTRV_NONE:
			/* Unset value. Should happen here */
			/* but handle anyway. */
			/* Default will be set later */
			param->tunable = RTR_NONE;
			break;
		case RTRV_STRING:
		case RTRV_NUM:
		case RTRV_RG_PRIMARIES:
		case RTRV_RT_INSTALLED_NODES:
		case RTRV_LOGICAL_HOSTNAME:
		case RTRV_SHARED_ADDRESS:
		case RTRV_WHEN_OFFLINE:
		case RTRV_WHEN_UNMANAGED:
		case RTRV_WHEN_UNMONITORED:
		default:
			error_code = RTR_STATUS_INVALID_ATTRVAL;
			error_bufloc = propvalue.start;
			/* if the buf pointer is NULL, have to set */
			/* the pointer to a blank */
			if (error_bufloc == NULL)
				error_bufloc = "";
			error_lineno = propvalue.lineno;
			break;
		}
		break;
	default:
		error_code = RTR_STATUS_INVALID_ATTR;
		rtr_get_current_location(&error_lineno, &error_bufloc);
		break;
	}
	return (param);
}

/*
 * Add an enum type attribute to a parameter
 */
static RtrParam_T *
paramenum(RtrParam_T *param, RtrVlist_T *vlistp)
{
	trace(NOI18N("paramenum"), NULL, 0)

	if (param == NULL) {
		/* Shouldn't happen. */
		param = error_cleanup_param = new_RtrParam_T();
	}
	if (param) {
		param->type = RTR_ENUM;
	}
	if (param && vlistp && !param->enumlist) {
		uint_t listsz = vlistp->listsz;
		RtrVlist_T *elp = vlistp;
		RtrString *array = (RtrString *)
		    malloc(sizeof (RtrString) * listsz);
		if (array != NULL) {
			int ix = (int)(listsz - 1);

			for (; ix >= 0 && elp; ix--) {
				assign_string_value(array + ix, &elp->value);
				elp = elp ->next;
			}
		}
		param->enumlist = array;
		param->enumlistsz = (int)listsz;
	}
	if (vlistp) {
		delete_RtrVlist_T(vlistp);
		error_cleanup_vlist = NULL;
	}
	return (param);
}


/*
 * Add a default value attribute to a parameter
 * Could be an array.
 * No typechecking is done on default value: all possible type
 * interpretations are filled-into the param entry.
 */
static RtrParam_T *
paramdefault(RtrParam_T *param, RtrVlist_T *vlistp)
{
	trace(NOI18N("paramdefault"), NULL, 0)

	/*
	 * vlistp may be NULL if there was an explicit empty setting,
	 * If vlistp == NULL, create default values to pass on the explict
	 *  NULL setting
	 */

	if (param == NULL) {
		/* Shouldn't happen. */
		param = error_cleanup_param = new_RtrParam_T();
	}
	if (param && !param->defaultarray) {
		uint_t listsz = vlistp ? vlistp->listsz : 1;
		RtrVlist_T *elp = vlistp;
		RtrString *array = (RtrString *)
		    malloc(sizeof (RtrString) * listsz);
		if (array != NULL) {
			int ix = (int)(listsz - 1);

			for (; ix >= 0 && elp; ix--) {
				assign_string_value(array + ix, &elp->value);
				array[ix].len = elp->value.len;
				elp = elp ->next;
			}
			param->defaultarray = array;
			param->defaultarraysz = (int)listsz;
		}
		if (listsz == 1 && vlistp) {
			/* Fill in scalar default values too */
			assign_string_value(&param->defaultstr, &vlistp->value);
			if (vlistp->value.vt == RTRV_NUM) {
				param->defaultint = vlistp->value.num;
				param->defaultint_isset = 1;
			}
			if (vlistp->value.vt == RTRV_TRUE) {
				param->defaultboolean = RTR_TRUE;
			} else if (vlistp->value.vt == RTRV_FALSE) {
				param->defaultboolean = RTR_FALSE;
			}
		} else if (vlistp == NULL) {
			/* Fill-in explicit NULL values */
			*array = nullstring;
			param->defaultstr = nullstring;
			param->defaultboolean = RTR_NULL;
		}
	}
	if (vlistp) {
		delete_RtrVlist_T(vlistp);
		error_cleanup_vlist = NULL;
	}
	return (param);
}

#ifdef YYDEBUG
static void
println(const char *msg, const char *start, int len, int ident)
{
	uint_t ix;

	for (ix = ident; ix > 0; ix--) {
		putchar('\t');
	}
	printf(msg);
	putchar(' ');
	if (len) {
		for (ix = 0; ix < len; ix++) putchar(start[ix]);
	} else if (start) {
		printf(start);
	}
	printf("\n");
}
#endif


/*
 *
 *	New and Delete functions for Parse-result Structures
 *
 */

RtrFileResult *
new_RtrFileResult(char *abuffer)
{
	RtrFileResult *obj = (RtrFileResult *)malloc(sizeof (RtrFileResult));
	if (obj == NULL) {
		return (NULL);
	}
	obj->buffer = abuffer;
	obj->status = RTR_STATUS_FILE_ERR;
	obj->error_bufloc = NULL;
	obj->error_lineno = 0;
	obj->rt_registration = NULL;
	obj->sc30_rtr = B_TRUE;
	obj->nru_tunablity_changed = B_FALSE;
	obj->upgradesz = 0;
	obj->upgradearray = NULL;
	obj->downgradesz = 0;
	obj->downgradearray = NULL;
	obj->paramtabsz = 0;
	obj->paramtab = NULL;
	return (obj);
}

void
delete_RtrFileResult(RtrFileResult *obj)
{
	int ix;

	free(obj->buffer);
	if (obj->rt_registration) {
		delete_Rtr_T(obj->rt_registration);
		obj->rt_registration = NULL;
	}

	if (obj->upgradearray) {
		for (ix = 0; ix < obj->upgradesz; ix++) {
			if (obj->upgradearray[ix]) {
				free(obj->upgradearray[ix]);
			}
		}
		free(obj->upgradearray);
		obj->upgradearray = NULL;
	}

	if (obj->downgradearray) {
		for (ix = 0; ix < obj->downgradesz; ix++) {
			if (obj->downgradearray[ix]) {
				free(obj->downgradearray[ix]);
			}
		}
		free(obj->downgradearray);
		obj->downgradearray = NULL;
	}

	if (obj->paramtab) {
		for (ix = 0; ix < obj->paramtabsz; ix++) {
			if (obj->paramtab[ix]) {
				delete_RtrParam_T(obj->paramtab[ix]);
			}
		}
		free(obj->paramtab);
		obj->paramtab = NULL;
	}
	free(obj);
}

RtrPtab_T*
new_RtrPtab_T()
{
	/*
	 * NOTE: If any field is added to RtrPtab_T that should
	 * have a non-zero or non-NULL initial value, that field
	 * should be individually set after the bzero() call
	 */
	RtrPtab_T *obj = (RtrPtab_T *)malloc(sizeof (RtrPtab_T));
	if (obj != NULL) {
		bzero(obj, sizeof (RtrPtab_T));
	}
	return (obj);
}

void
delete_RtrPtab_T(RtrPtab_T *anobj)
{
	/*
	 * NOTE: params are deleted with the container.
	 * When params are copied out of the RtrPtab container, into
	 * the RtrFileResult struct, the param pointer should be
	 * pointer should be set to NULL.
	 */
	uint_t ix = 0;

	for (; ix < (uint_t)N_SYS_PARAMS; ix++) {
		if (anobj->sys_param[ix] != NULL) {
			delete_RtrParam_T(anobj->sys_param[ix]);
			anobj->sys_param[ix] = NULL;
		}
	}
	delete_RtrExtlist_T(anobj->extensions);
	anobj->extensions = NULL;
	free(anobj);
}

RtrExtlist_T*
new_RtrExtlist_T(RtrExtlist_T *nxt)
{
	RtrExtlist_T *obj = (RtrExtlist_T *)malloc(sizeof (RtrExtlist_T));
	if (obj == NULL) {
		return (NULL);
	}
	obj->next = nxt;
	obj->param = NULL;
	return (obj);
}

void
delete_RtrExtlist_T(RtrExtlist_T *anobj)
{
	RtrExtlist_T *obj = anobj;
	while (obj != NULL) {
		anobj = obj->next;
		if (obj->param != NULL) {
			delete_RtrParam_T(obj->param);
			obj->param = NULL;
		}
		obj->next = NULL;
		free(obj);
		obj = anobj;
	}
}

RtrVlist_T *
new_RtrVlist_T(RtrVlist_T *nxt)
{
	RtrVlist_T *obj = (RtrVlist_T*)malloc(sizeof (RtrVlist_T));
	if (obj == NULL) {
		return (NULL);
	}
	obj->next = nxt;
	obj->listsz = (nxt == NULL) ?
		1
		: (obj->next ->listsz) + 1;
	obj->value.start = NULL;
	obj->value.len = 0;
	obj->value.vt = RTRV_NONE;
	return (obj);
}

void
delete_RtrVlist_T(RtrVlist_T *anobj)
{
	RtrVlist_T *obj = anobj;
	while (obj != NULL) {
		anobj = obj->next;
		obj->next = NULL;
		free(obj);
		obj = anobj;
	}
}

Rtr_T *
new_Rtr_T()
{
	/*
	 * NOTE: If any field is added to Rtr_T that should
	 * have a non-zero or non-NULL initial value, that field
	 * should be individually set after the bzero() call
	 */

	Rtr_T *obj = (Rtr_T *)malloc(sizeof (Rtr_T));

	if (obj != NULL) {
		bzero(obj, sizeof (Rtr_T));
	}
	return (obj);
}

void
delete_Rtr_T(Rtr_T *obj)
{
	if (obj->pkglist) {
		free(obj->pkglist);
		obj->pkglist = NULL;
		obj->pkglistsz = 0;
	}
	free(obj);
}

RtrUpglist_T *
new_RtrUpglist_T(RtrUpglist_T *lp)
{
	RtrUpglist_T *obj = (RtrUpglist_T *)malloc(sizeof (RtrUpglist_T));
	if (obj == NULL) {
		return (NULL);
	}

	obj->entry = (RtrUpg_T *)malloc(sizeof (RtrUpg_T));
	if (obj->entry == NULL) {
		free(obj);
		return (NULL);
	}

	obj->next = lp;
	return (obj);
}

void
delete_RtrUpglist_T(RtrUpglist_T *lp)
{
	RtrUpglist_T *obj = lp;

	while (obj != NULL) {
		lp = obj->next;
		if (obj->entry) {
			free(obj->entry);
		}
		obj->next = NULL;
		free(obj);
		obj = lp;
	}
}


RtrRTUinfo_T *
new_RtrRTUinfo_T()
{
	RtrRTUinfo_T *obj = (RtrRTUinfo_T *)malloc(sizeof (RtrRTUinfo_T));
	if (obj == NULL) {
		return (NULL);
	}

	obj->sc30_rtr = B_TRUE;
	obj->upgradelist = NULL;
	obj->downgradelist = NULL;
	return (obj);
}

void
delete_RtrRTUinfo_T(RtrRTUinfo_T *lp)
{
	RtrRTUinfo_T *obj = lp;

	if (obj->upgradelist)
		delete_RtrUpglist_T(obj->upgradelist);

	if (obj->downgradelist)
		delete_RtrUpglist_T(obj->downgradelist);

	free(obj);
}


RtrParam_T *
new_RtrParam_T()
{
	/*
	 * NOTE: If any field is added to RtrParam_T that should
	 * have a non-zero or non-NULL initial value, that field
	 * should be individually set after the bzero() call
	 */

	RtrParam_T *obj = (RtrParam_T *)malloc(sizeof (RtrParam_T));

	if (obj != NULL) {
		bzero(obj, sizeof (RtrParam_T));
	}
	return (obj);
}

void
delete_RtrParam_T(RtrParam_T *obj)
{
	if (obj->enumlist) {
		free(obj->enumlist);
		obj->enumlist = NULL;
		obj->enumlistsz = 0;
	}
	if (obj->defaultarray) {
		free(obj->defaultarray);
		obj->defaultarray = NULL;
		obj->defaultarraysz = 0;
	}
	free(obj);
}


/*
 *
 *  Paramtable Building and Checking
 *
 */


/*
 * Create a record for a fixed system-defined property that
 * was not declared in the RT file's param table.
 * If the Scalable property was declared, then the other scalable-service-
 * related properties (Network_resources_used, Port_list, Load_balancing_policy,
 * Load_balancing_weights, Affinity_timeout, UDP_affinity, Weak_affinity)
 * are treated as fixed properties and
 * this function will be called to initialize them if they were undeclared.
 */
static RtrParam_T *
default_sysparam(int ix)
{
	/*
	 * NOTE: The strings pointed-to by the normal
	 * propname.start method should not be deallocated
	 * when the RtrParam_T is deleted, so it is OK to set
	 * them to the nametable entries.
	 * (For normal parameters, the name strings are in the
	 *  file input buffer.)
	 */
	RtrParam_T *param = new_RtrParam_T();

	if (param == NULL) {
		return (NULL);
	}
	param->sysparamid = nametable[ix].sysparamid;
	param->propname.start = nametable[ix].name;
	param->propname.len = nametable[ix].len;
	param->propname.value = RTR_STRING;
	param->extension = RTR_FALSE_UNSET;
	if (param->sysparamid == RTR_SP_FAILOVER_MODE) {
		set_fomtype(param);
		param->tunable = RTR_ANYTIME;
	} else if (param->sysparamid == RTR_SP_NETWORK_RESOURCES_USED) {
		param->type = RTR_STRINGARRAY;
		param->tunable = RTR_ANYTIME;
		param->rtr_min = 0;
		param->rtr_min_isset = 1;
		param->rtr_max = MAXRGMVALUESLEN;
		param->rtr_max_isset = 1;
		if (param->defaultarraysz == 0) {
			param->defaultarraysz = 1;
			param->defaultarray = (RtrString *)
			    malloc(sizeof (RtrString) *
			    (uint_t)param->defaultarraysz);
			if (param->defaultarray == NULL) {
				delete_RtrParam_T(param);
				return (NULL);
			}
		}
	} else if (param->sysparamid == RTR_SP_PORT_LIST) {
		param->type = RTR_STRINGARRAY;
		param->tunable = RTR_ANYTIME;
	} else if (param->sysparamid == RTR_SP_LOAD_BALANCING_POLICY) {
		param->type = RTR_STRING;
		param->tunable = RTR_AT_CREATION;
		param->defaultstr = lbp_strings[0];
	} else if (param->sysparamid == RTR_SP_LOAD_BALANCING_WEIGHTS) {
		param->type = RTR_STRINGARRAY;
		param->tunable = RTR_ANYTIME;
		if (param->defaultarraysz == 0) {
			param->defaultarraysz = 1;
			param->defaultarray = (RtrString *)
			    malloc(sizeof (RtrString) *
			    (uint_t)param->defaultarraysz);
			if (param->defaultarray == NULL) {
				delete_RtrParam_T(param);
				return (NULL);
			}
		}
	} else if (param->sysparamid == RTR_SP_AFFINITY_TIMEOUT) {
		param->type = RTR_INT;
		param->defaultint = 0;
		param->defaultint_isset = 1;
		param->rtr_min = -1;
		param->rtr_min_isset = 1;
		param->tunable = RTR_ANYTIME;
	} else if ((param->sysparamid == RTR_SP_UDP_AFFINITY) ||
		    (param->sysparamid == RTR_SP_WEAK_AFFINITY) ||
		    (param->sysparamid == RTR_SP_GENERIC_AFFINITY)) {
		param->type = RTR_BOOLEAN;
		param->defaultboolean = RTR_FALSE;
		param->defaultstr = emptystring;
		param->tunable = RTR_WHEN_DISABLED;
	} else if (param->sysparamid == RTR_SP_ROUND_ROBIN) {
		param->type = RTR_BOOLEAN;
		param->defaultboolean = RTR_FALSE;
		param->defaultstr = emptystring;
		param->tunable = RTR_WHEN_DISABLED;
	} else if (param->sysparamid == RTR_SP_CONN_THRESHOLD) {
		param->type = RTR_INT;
		param->defaultint = 100;
		param->defaultint_isset = 1;
		param->tunable = RTR_WHEN_DISABLED;
	} else {
		param->type = RTR_INT;
		param->defaultint = PARAM_TIMEOUT_DEFAULT;
		param->defaultint_isset = 1;
		param->rtr_min = PARAM_TIMEOUT_MIN;
		param->rtr_min_isset = 1;
		param->tunable = RTR_ANYTIME;
	}

	return (param);
}


/*
 * If a default value is declared for a stringarray property, validate
 * the default value against any rtr_min, rtr_max, array_minsize and
 * array_maxsize paramtable properties.
 * Return NULL, if DEFAULT attribute is not declared or the default
 * value is valid.
 * return SCHA_DEFAULT if the default value is invalid.
 * Return SCHA_MIN|MAX if the length of an element in the array is not in
 * the range of MIN or MAX.
 * Return SCHA_ARRAY_MINSIZE|SCHA_ARRAY_MAXSIZE if the number of element
 * in the array is not int the range of ARRAY_MINSIZE or ARRAY_MAXSIZE.
 */
static const char *
check_default_stringarray(RtrParam_T *param)
{
	uint_t i;
	/*
	 * if ARRAY_MINSIZE is set to 0 and MIN is greater than 0,
	 * it is not allowed.
	 */
	if (param->arraymin_isset == 1 && param->arraymin == 0) {
		if (param->rtr_min_isset == 1 && param->rtr_min > 0)
			return (SCHA_MIN);
	}

	/*
	 * DEFAULT attribute is not declared in rtr file
	 * Validation for default value will be done when Resource of this
	 * resource type is created.
	 */
	if (param->defaultarray == NULL)
		return (NULL);

	/*
	 * If 'DEFAULT = ;' or 'DEFAULT = "";', parser will set
	 * 	param->defaultarraysz to 1,
	 *	param->defaultarray[0].len to 0,
	 *	param->defaultarray[0].value to RTR_NULL
	 * 	and param->defaultarray[0].start to NULL.
	 * If DEFAULT is not specified, parser will set
	 * 	param->defaultarraysz to 1,
	 *	param->defaultarray[0].len to 0,
	 *	param->defaultarray[0].value to RTR_NONE
	 * 	and param->defaultarray[0].start to NULL.
	 */

	/*
	 * Even if the default is NULL or NONE, we still have to check
	 * the number of elements in default against arraymin and
	 * minlength
	 */
	if (param->defaultarray[0].value == RTR_NULL ||
	    param->defaultarray[0].value == RTR_NONE) {
		if (param->arraymin_isset == 1 && param->arraymin > 0)
			return (SCHA_DEFAULT);

		if (param->rtr_min_isset == 1 && param->rtr_min > 0)
			return (SCHA_DEFAULT);
	} else {
		/*
		 * check the arraysize against array_maxsize and array_minsize
		 */
		if (param->arraymax_isset == 1 &&
		    param->defaultarraysz > param->arraymax)
			return (SCHA_DEFAULT);

		if (param->arraymin_isset == 1 &&
		    param->defaultarraysz < param->arraymin)
			return (SCHA_DEFAULT);

		/*
		 * check the length of default value against minlength
		 * and maxlength
		 */
		for (i = 0; i < (uint_t)param->defaultarraysz; i++) {
			if (param->rtr_min_isset == 1) {
				if (param->defaultarray[i].len <
				    param->rtr_min)
					return (SCHA_DEFAULT);
			}
			if (param->rtr_max_isset == 1) {
				if (param->defaultarray[i].len >
				    param->rtr_max)
					return (SCHA_DEFAULT);
			}
		}
	}
	return (NULL);
}

/*
 * If DEFAULT attribute is declared for a BOOLEAN property, validate
 * the default value for TRUE/FALSE.
 *
 * Return NULL, if DEFAULT attribute is not declared or the default
 * value is valid.
 * Return SCHA_DEFAULT if the value is blank or a string array.
 * Return SCHA_MIN|SCHA_MAX|SCHA_ARRAY_MINSIZE|SCHA_ARRAY_MAXSIZE
 * if rtr_min/rtr_max/array_min/array_max is specifed.
 *
 * NOTES: it is not allowed to set the value to blank or NULL for BOOLEAN type.
 */
static const char *
check_default_boolean(RtrParam_T *param)
{

	/*
	 * Min, Max, Array_maxsize and Array_minsize attributes are not
	 * allowed to be declared for BOOLEAN type of property.
	 */
	if (param->rtr_min_isset != 0)
		return (SCHA_MIN);
	if (param->arraymin_isset != 0)
		return (SCHA_ARRAY_MINSIZE);

	if (param->rtr_max_isset != 0)
		return (SCHA_MAX);
	if (param->arraymax_isset != 0)
		return (SCHA_ARRAY_MAXSIZE);

	/*
	 * only one value can be specified
	 */
	if (param->defaultarraysz > 1)
		return (SCHA_DEFAULT);

	/*
	 * If 'DEFAULT = ;' or 'DEFAULT = "";', parser will set:
	 *	param->defaultstr.len to 0,
	 *	param->defaultstr.value to RTR_NULL
	 *	and param->defaultstr.start to NULL.
	 * If no DEFAULT attribute is specified, parser will set:
	 *	param->defaultboolean = RTR_NONE
	 *	param->defaultstr.len to 0,
	 *	param->defaultstr.value to RTR_NONE
	 *	and param->defaultstr.start to NULL.
	 * If 'DEFAULT = TRUE|FALSE, parser will set:
	 *	param->defaultboolean = RTR_TRUE|RTR_FALSE
	 */

	/*
	 * If DEFAULT attribute is not specified , 'DEFAULT = ;' or
	 * 'DEFAULT = "", it is not allowed.
	 *
	 * If DEFAULT attribute is specifed, the value of the default has to
	 * be TRUE or FALSE.  Other value, return error SCHA_DEFAULT.
	 */
	if (param->defaultboolean == RTR_TRUE ||
	    param->defaultboolean == RTR_FALSE ||
	    (param->defaultboolean == RTR_NONE &&
	    param->defaultstr.value == RTR_NONE))
		return (NULL);
	else
		return (SCHA_DEFAULT);
}

/*
 * If DEFAULT attribute is declared for a ENUM property, validate
 * the default value against any minlength/maxlength.
 * Return NULL, if DEFAULT attribute is not declared or the default
 * value is valid.
 * Return SCHA_DEFAULT if the default value is invalid.
 * Return SCHA_MIN|SCHA_MAX|SCHA_ARRAY_MINSIZE|SCHA_ARRAY_MAXSIZE
 * if rtr_min/rtr_max/array_min/array_max is specifed.
 *
 * NOTES: it is not allowed to set the value to blank or NULL for ENUM type.
 */
static const char *
check_default_enum(RtrParam_T *param)
{
	uint_t		i, len;
	const char	*str;
	char		*fom_values[] = { SCHA_NONE, SCHA_HARD, SCHA_SOFT,
	    SCHA_RESTART_ONLY, SCHA_LOG_ONLY };



	/*
	 * there is no ENUMLIST attribute declared.
	 */
	if (param->enumlist == NULL || param->enumlistsz == 0) {
		if (param->sysparamid == RTR_SP_FAILOVER_MODE) {
			/*
			 * Failover_mode no longer uses the enum list
			 * the enum list is null, but we keep going
			 */
		} else {
			return (SCHA_ENUMLIST);
		}
	}


	/*
	 * Min, Max, Array_maxsize and Array_minsize attributes are not
	 * allowed to be declared for ENUM type of property.
	 */
	if (param->rtr_min_isset != 0)
		return (SCHA_MIN);
	if (param->arraymin_isset != 0)
		return (SCHA_ARRAY_MINSIZE);

	if (param->rtr_max_isset != 0)
		return (SCHA_MAX);
	if (param->arraymax_isset != 0)
		return (SCHA_ARRAY_MAXSIZE);

	/*
	 * only one value can be specified
	 */
	if (param->defaultarraysz > 1)
		return (SCHA_DEFAULT);

	/*
	 * If 'DEFAULT = ;' or 'DEFAULT = "";', parser will set:
	 *	param->defaultstr.len to 0,
	 *	param->defaultstr.value to RTR_NULL
	 *	and param->defaultstr.start to NULL.
	 * If DEFAULT attribute is not specified, parser will set:
	 *	param->defaultstr.len to 0,
	 *	param->defaultstr.value to RTR_NONE
	 *	and param->defaultstr.start to NULL.
	 */

	/*
	 * If DEFAULT attribute is not specified, it is allowed.  The value
	 * has to be provided when the resource of the type is created.
	 */
	if (param->defaultstr.value == RTR_NONE)
		return (NULL);

	/*
	 * If DEFAULT attribute is specifed, the value of the default has to
	 * be specified.  If not, return error SCHA_DEFAULT.
	 */
	if (param->defaultstr.value == RTR_NULL)
		return (SCHA_DEFAULT);

	/*
	 * check the default value
	 */
	if (param->sysparamid == RTR_SP_FAILOVER_MODE) {
		/*
		 * Failover_mode no longer uses the enum list
		 * check against hard-coded list of possible values
		 */

		str = param->defaultstr.start;
		len = (uint_t)param->defaultstr.len;

		for (i = 0; i < 5; i++) {
			if ((strncmp(str, fom_values[i], len) == 0) &&
			    (len == strlen(fom_values[i]))) {
				return (NULL);
			}
		}
	} else {
		for (i = 0; i < (uint_t)param->enumlistsz; i++) {
			/*
			 * if the default value is in the enumlist,
			 * return with no error
			 */
			if (param->defaultstr.len == param->enumlist[i].len &&
			    strncmp(param->defaultstr.start,
				param->enumlist[i].start,
				(uint_t)param->enumlist[i].len) == 0)
				return (NULL);
		}
	}
	return (SCHA_DEFAULT);
}

/*
 * If DEFAULT attribute is declared for an INT property, validate
 * the default value against any minlength/maxlength
 * Return NULL, if DEFAULT attribute is not declared or the default
 * value is valid.
 * Return SCHA_DEFAULT if the default value is invalid.
 * Return SCHA_MIN|SCHA_MAX if the value is not in the range of MIN or MAX.
 * Return SCHA_ARRAY_MINSIZE|SCHA_ARRAY_MAXSIZE if array_min/array_max is
 * specifed.
 *
 * NOTES: it is not allowed to set the value to blank or NULL for INT type.
 */
static const char *
check_default_int(RtrParam_T *param)
{

	/*
	 * has more than one value is specified, return error
	 */
	if (param->defaultarraysz > 1)
		return (SCHA_DEFAULT);

	/*
	 * can not specify ARRAY_MINSIZE and ARRAY_MAXSIZE
	 */
	if (param->arraymin_isset != 0)
		return (SCHA_ARRAY_MINSIZE);
	if (param->arraymax_isset != 0)
		return (SCHA_ARRAY_MAXSIZE);

	/*
	 * if non-numeric number is specified for the default,
	 * param->defaultint_isset will not be set and param->defaultstr.value
	 * will be set. In this case, return error.
	 */
	if ((param->defaultint_isset == 0) &&
	    (param->defaultstr.value != RTR_NONE &&
	    param->defaultstr.value != RTR_NULL))
		return (SCHA_DEFAULT);

	/*
	 * It is not allowed the value is to be set to NULL or blank
	 */
	if (param->defaultint_isset == 0 &&
	    param->defaultstr.value == RTR_NULL) {
		return (SCHA_DEFAULT);
	}

	/*
	 * if default is set, check the value if Min or Max is specified.
	 */
	if (param->defaultint_isset == 1) {
		if (param->rtr_min_isset == 1 &&
		    param->defaultint < param->rtr_min)
			return (SCHA_DEFAULT);
		if (param->rtr_max_isset == 1 &&
		    param->defaultint > param->rtr_max)
			return (SCHA_DEFAULT);
	}
	return (NULL);
}

/*
 * If DEFAULT attribute is declared for a STRING property, validate
 * the default value against any minlength/maxlength.
 * Return NULL, if DEFAULT attribute is not declared or the default
 * value is valid. Otherwise,  return SCHA_DEFAULT indicating that the
 * default value is invalid.
 * Return SCHA_DEFAULT if the default value is invalid.
 * Return SCHA_MIN|SCHA_MAX if the length of the value is not in
 * the range of MIN or MAX.
 * Return SCHA_ARRAY_MINSIZE|SCHA_ARRAY_MAXSIZE if array_min/array_max is
 * specifed.
 */
static const char *
check_default_string(RtrParam_T *param)
{

	/*
	 * can not specify ARRAY_MINSIZE and ARRAY_MAXSIZE
	 */
	if (param->arraymin_isset != 0)
		return (SCHA_ARRAY_MINSIZE);
	if (param->arraymax_isset != 0)
		return (SCHA_ARRAY_MAXSIZE);

	/*
	 * DEFAULT attribute is not set in rtr file, skip the
	 * default checking.  When resource of this type is created,
	 * rgmd will do the sanity checking
	 */
	if (param->defaultstr.start == NULL &&
	    param->defaultstr.value == RTR_NONE) {
			return (NULL);
	}

	/*
	 * When DEFAULT attribute is set in rtr and value
	 * is empty string, we still need to check that
	 * the default value is in the range of MIN and MAX if MIN or
	 * MAX is specified
	 */
	if (param->defaultstr.start == NULL &&
	    param->defaultstr.value == RTR_NULL) {
		if (param->rtr_min_isset == 1 && param->rtr_min > 0)
			return (SCHA_DEFAULT);
	} else {
		if (param->rtr_min_isset == 1 &&
		    param->defaultstr.len < param->rtr_min)
			return (SCHA_DEFAULT);
		if (param->rtr_max_isset == 1 &&
		    param->defaultstr.len > param->rtr_max)
			return (SCHA_DEFAULT);
	}

	return (NULL);
}

/*
 * If a default value is declared for Load_balancing_policy, validate
 * the default value against the permissible strings.
 */
static const char *
check_lbp_default(RtrParam_T *param)
{
	uint_t ix;

	/* If no value or empty value was declared, set default */
	if (param->defaultstr.value == RTR_NONE ||
	    param->defaultstr.value == RTR_NULL) {
		param->defaultstr = lbp_strings[0];
		return (NULL);
	}

	/*
	 * A default was declared.  Make sure it matches one of the permissible
	 * values.
	 */
	for (ix = 0; ix < N_LBP_STR; ix++) {
		if (lbp_strings[ix].len == param->defaultstr.len &&
		    strncasecmp(lbp_strings[ix].start, param->defaultstr.start,
		    (uint_t)lbp_strings[ix].len) == 0) {
			/* We found a match */
			return (NULL);
		}
	}

	/* The declared value matched no permissible string.  Return error. */
	return (SCHA_DEFAULT);
}


/*
 * Check attributes and fill-in defaults for a file-declared
 * paramtable entry for a system-defined property.
 * ix is the index into the nametable for the system-defined property.
 * param is the entry as it has been filled-in so far.
 * A non-NULL return indicates that an error has been found in
 * the paramtable entry.
 * In case of NRU we return non-null if we are overriding the tunablity
 * that was set by the user.We dont treat this as error.
 * The value of the string gives the name of the attribute that
 * caused the error.
 */
static const char *
check_and_set_sysparm(uint_t ix, RtrParam_T *param)
{
	if (nametable[ix].type == RTR_SPT_TO) {
		/* Paramtable entry is for a method timeout */
		param->type = RTR_INT;
		if (param->tunable != RTR_NONE &&
		    param->tunable != RTR_ANYTIME) {
			return (SCHA_TUNABLE);
		}
		param->tunable = RTR_ANYTIME;

		/* It is illegal to set a minimum method timeout < 1 */
		if (param->rtr_min_isset == 1) {
			if (param->rtr_min < PARAM_TIMEOUT_MIN)
				return (SCHA_MIN);
		} else {
			param->rtr_min = PARAM_TIMEOUT_MIN;
			param->rtr_min_isset = 1;
		}

		/* It is illegal to set a maximum method timeout */
		if (param->rtr_max_isset != 0) {
			return (SCHA_MAX);
		}

		/*
		 * if a default value is declared and is not NULL, validate it;
		 * otherwise set it to system default.
		 */
		if (param->defaultint_isset) {
			if (param->defaultint < param->rtr_min)
				return (SCHA_DEFAULT);
		} else {
			param->defaultint = PARAM_TIMEOUT_DEFAULT;
			param->defaultint_isset = 1;
		}
	} else if (param->sysparamid == RTR_SP_FAILOVER_MODE) {
		set_fomtype(param);
		if (param->tunable != RTR_NONE &&
		    param->tunable != RTR_ANYTIME) {
			return (SCHA_TUNABLE);
		}
		param->tunable = RTR_ANYTIME;
		return (check_default_enum(param));
	} else if (param->sysparamid == RTR_SP_NETWORK_RESOURCES_USED) {
		/*
		 * To check the tunablity of NRU is to be changed.
		 */
		boolean_t nru_tunablity_changed = B_FALSE;
		const char *ret;
		param->type = RTR_STRINGARRAY;
		/*
		 * Check if the tunablity parameter is anything other than
		 * RTR_ANYTIME.If so set the nru_tunablity_changed flag.
		 */
		if (param->tunable != RTR_ANYTIME) {
			nru_tunablity_changed = B_TRUE;
			/*
			 * Set the tunablity to RTR_ANYTIME
			 */
			param->tunable = RTR_ANYTIME;
		}

		/*
		 * Min and Max attribute are not allowed to be
		 * declared for NETWORK_RESOURCES_USED property.  set the
		 * rtr_min and rtr_max for this property.
		 */
		if (param->rtr_min_isset != 0) {
			return (SCHA_MIN);
		}
		if (param->rtr_max_isset != 0) {
			return (SCHA_MAX);
		}

		/*
		 * set the Min and Max
		 */
		param->rtr_min = 0;
		param->rtr_min_isset = 1;
		param->rtr_max = MAXRGMVALUESLEN;
		param->rtr_max_isset = 1;

		if (param->defaultarraysz == 0) {
			param->defaultarraysz = 1;
			param->defaultarray = (RtrString *)
			    malloc(sizeof (RtrString) *
			    (uint_t)param->defaultarraysz);
			if (param->defaultarray == NULL) {
				delete_RtrParam_T(param);
				return (NULL);
			}
		}

		if ((ret = check_default_stringarray(param)) == NULL) {
			/*
			 * if nru_tunablity_changed is set we need to
			 * generate a warning message as we are overriding the
			 * tunablity set by the user.
			 */
			if (nru_tunablity_changed)
				return (SCHA_NRU_TUNABLITY_CHANGED);
			else
				return (NULL);
		}
		return (ret);
	} else if (param->sysparamid == RTR_SP_SCALABLE) {
		param->type = RTR_BOOLEAN;
		if (param->tunable != RTR_NONE &&
		    param->tunable != RTR_FALSE &&
		    param->tunable != RTR_AT_CREATION) {
			return (SCHA_TUNABLE);
		}
		if (param->tunable == RTR_NONE) {
			param->tunable = RTR_AT_CREATION;
		}
		/*
		 * . If the default value is set and is not TRUE or
		 *   FALSE, the defaultboolean field is set to RTR_NONE
		 *   by parser.  The default value in rtr file is put
		 *   in defaultstr.  So if defaultboolean is RTR_NONE and
		 *   defaultstr is non-null, this is an error.
		 * . If the default value is not set, defaultboolean field is
		 *   set to RTR_NONE and the defaultstr is NULL.  In this
		 *   case, take the default (TRUE).
		 * . If the default value is set and is TRUE or FALSE,
		 *   defaultboolean field is to RTR_TRUE or RTR_FALSE.
		 *   Leave it as is.
		 */
		if (param->defaultboolean == RTR_NONE) {
			if (param->defaultstr.start != NULL) {
				return (SCHA_DEFAULT);
			} else {
				param->defaultboolean = RTR_TRUE;
			}
		}
	} else if (param->sysparamid == RTR_SP_PORT_LIST) {
		param->type = RTR_STRINGARRAY;
		if (param->tunable == RTR_NONE) {
			param->tunable = RTR_ANYTIME;
		}
		return (check_default_stringarray(param));
	} else if (param->sysparamid == RTR_SP_LOAD_BALANCING_POLICY) {
		param->type = RTR_STRING;
		if (param->tunable != RTR_NONE &&
		    param->tunable != RTR_AT_CREATION) {
			return (SCHA_TUNABLE);
		}
		param->tunable = RTR_AT_CREATION;
		return (check_lbp_default(param));
	} else if (param->sysparamid == RTR_SP_LOAD_BALANCING_WEIGHTS) {
		param->type = RTR_STRINGARRAY;
		if (param->tunable == RTR_NONE) {
			param->tunable = RTR_ANYTIME;
		}
		if (param->defaultarray == NULL) {
			param->defaultarray = (RtrString *)
					malloc(sizeof (RtrString));
			if (param->defaultarray == NULL) {
				return (SCHA_DEFAULT);
			}
			param->defaultarray[0].start = emptystring.start;
			param->defaultarray[0].len = emptystring.len;
			param->defaultarray[0].value = emptystring.value;
			param->defaultarray[0].lineno = emptystring.lineno;
			param->defaultarraysz = 1;
		} else {
			return (check_default_stringarray(param));
		}
	} else if (param->sysparamid == RTR_SP_AFFINITY_TIMEOUT) {
		param->type = RTR_INT;
		if (param->tunable != RTR_NONE &&
		    param->tunable != RTR_ANYTIME) {
			return (SCHA_TUNABLE);
		}
		param->tunable = RTR_ANYTIME;
		return (check_default_int(param));
	} else if ((param->sysparamid == RTR_SP_UDP_AFFINITY) ||
		    (param->sysparamid == RTR_SP_WEAK_AFFINITY) ||
		    (param->sysparamid == RTR_SP_GENERIC_AFFINITY)) {
		param->type = RTR_BOOLEAN;
		if (param->tunable != RTR_NONE &&
		    param->tunable != RTR_WHEN_DISABLED) {
			return (SCHA_TUNABLE);
		}
		param->tunable = RTR_WHEN_DISABLED;

		if (param->defaultboolean == RTR_NONE) {
			if (param->defaultstr.start != NULL) {
				return (SCHA_DEFAULT);
			} else {
				param->defaultboolean = RTR_FALSE;
			}
		}

	} else if (param->sysparamid == RTR_SP_ROUND_ROBIN) {
		param->type = RTR_BOOLEAN;
		if (param->tunable != RTR_NONE &&
		    param->tunable != RTR_WHEN_DISABLED) {
			return (SCHA_TUNABLE);
		}
		param->tunable = RTR_WHEN_DISABLED;

		if (param->defaultboolean == RTR_NONE) {
			param->defaultboolean = RTR_FALSE;
		}
	} else if (param->sysparamid == RTR_SP_CONN_THRESHOLD) {
		param->type = RTR_INT;
		if (param->tunable != RTR_NONE &&
		    param->tunable != RTR_WHEN_DISABLED) {
			return (SCHA_TUNABLE);
		}
		param->tunable = RTR_WHEN_DISABLED;

		return (check_default_int(param));

	} else if (param->sysparamid == RTR_SP_GLOBAL_ZONE_OVERRIDE) {
		param->type = RTR_BOOLEAN;
		/*
		 * RTR_FALSE means tunability was explicitly set to "NONE"
		 * or "FALSE" or explicit NULL value.  For
		 * Global_zone_override, the default and minimum tunability is
		 * AT_CREATION; we also allow ANYTIME or WHEN_DISABLED.
		 */
		if (param->tunable == RTR_FALSE ||
		    param->tunable == RTR_NONE) {
			param->tunable = RTR_AT_CREATION;
		}
		/*
		 * Default value for Global_zone_override is TRUE.  Note that
		 * RT registration only allows Global_zone_override to be
		 * declared if "Global_zone=TRUE" is set in RTR file.
		 * So the default is to keep Global_zone=TRUE, unless the
		 * Global_zone_override property of the resource is
		 * explicitly set to FALSE.
		 */
		if (param->defaultboolean == RTR_NONE) {
			if (param->defaultstr.start != NULL) {
				return (SCHA_DEFAULT);
			} else {
				param->defaultboolean = RTR_TRUE;
			}
		}
	} else {
		/*
		 * CHEAP_PROBE_INTERVAL, THOROUGH_PROBE_INTERVAL,
		 * RETRY_COUNT and RETRY_INTERVAL
		 */
		param->type = RTR_INT;
		if (param->tunable == RTR_NONE) {
			param->tunable = RTR_WHEN_DISABLED;
		}

		/*
		 * validate default value if DEFAULT is set or DEFAULT is
		 * a NULL value
		 */
		return (check_default_int(param));
	}

	return (NULL);
}

/*
 * Check if the RT registration has a method declared
 * for a timeout property.
 * ix is the index into the nametable of system-define properties for
 * the timeout property.
 * rtreg is the RT registration information from the parser results.
 */
static int
has_method(int ix, Rtr_T *rtreg)
{
	switch (nametable[ix].sysparamid) {
	case RTR_SP_START_TIMEOUT:
		return (rtreg->start.value == RTR_STRING);
	case RTR_SP_STOP_TIMEOUT:
		return (rtreg->stop.value == RTR_STRING);
	case RTR_SP_VALIDATE_TIMEOUT:
		return (rtreg->validate.value == RTR_STRING);
	case RTR_SP_UPDATE_TIMEOUT:
		return (rtreg->update.value == RTR_STRING);
	case RTR_SP_INIT_TIMEOUT:
		return (rtreg->init.value == RTR_STRING);
	case RTR_SP_FINI_TIMEOUT:
		return (rtreg->fini.value == RTR_STRING);
	case RTR_SP_BOOT_TIMEOUT:
		return (rtreg->boot.value == RTR_STRING);
	case RTR_SP_MONITOR_START_TIMEOUT:
		return (rtreg->monitor_start.value == RTR_STRING);
	case RTR_SP_MONITOR_STOP_TIMEOUT:
		return (rtreg->monitor_stop.value == RTR_STRING);
	case RTR_SP_MONITOR_CHECK_TIMEOUT:
		return (rtreg->monitor_check.value == RTR_STRING);
	case RTR_SP_PRENET_START_TIMEOUT:
		return (rtreg->prenet_start.value == RTR_STRING);
	case RTR_SP_POSTNET_STOP_TIMEOUT:
		return (rtreg->postnet_stop.value == RTR_STRING);
	case RTR_SP_FAILOVER_MODE:
	case RTR_SP_NETWORK_RESOURCES_USED:
	case RTR_SP_SCALABLE:
	case RTR_SP_PORT_LIST:
	case RTR_SP_LOAD_BALANCING_POLICY:
	case RTR_SP_LOAD_BALANCING_WEIGHTS:
	case RTR_SP_AFFINITY_TIMEOUT:
	case RTR_SP_UDP_AFFINITY:
	case RTR_SP_WEAK_AFFINITY:
	case RTR_SP_GENERIC_AFFINITY:
	case RTR_SP_CHEAP_PROBE_INTERVAL:
	case RTR_SP_THOROUGH_PROBE_INTERVAL:
	case RTR_SP_RETRY_COUNT:
	case RTR_SP_RETRY_INTERVAL:
	case RTR_SP_GLOBAL_ZONE_OVERRIDE:
	case RTR_SP_NONE:
	default:
		return (0);
	}
}

/*
 * validate the default value for an extension property
 * check to see the default value is in the range of rtr_min and rtr_max values
 * if the Min or Max value is specified in rtr file
 */
static const char *
validate_default_value(RtrParam_T *param)
{

	/*
	 * Min can not be greater than Max
	 * Array_min can not be greater than Array_max
	 */
	if (param->rtr_min_isset == 1 && param->rtr_max_isset == 1) {
		if (param->rtr_min > param->rtr_max)
			return (SCHA_MIN);
	}

	if (param->arraymin_isset == 1 && param->arraymax_isset == 1) {
		if (param->arraymin > param->arraymax)
			return (SCHA_ARRAY_MINSIZE);
	}

	if (param->type == RTR_STRING)
		return (check_default_string(param));

	if (param->type == RTR_INT)
		return (check_default_int(param));

	if (param->type == RTR_BOOLEAN)
		return (check_default_boolean(param));

	if (param->type == RTR_STRINGARRAY)
		return (check_default_stringarray(param));

	if (param->type == RTR_ENUM)
		return (check_default_enum(param));

	return (NULL);
}

/*
 * Build paramtable array from lists in paramtab structure
 * that were built by action routines.  The list of sys_defined params
 * is an array with holes that need to be filled in to indicate default
 * settings for properties that were not declared in the paramtable.
 * Extension properties are in a linked list.
 */
static void
check_and_set_paramtable(RtrFileResult *parse_result)
{
	uint_t ix = 0, jx = 0;
	RtrParam_T **parray = NULL;
	RtrExtlist_T *extp = NULL;
	RtrParam_T **sysp = NULL;
	uint_t listsz = 0;
	uint_t n_ext = 0;
	uint_t n_sys = 0;
	RtrString *error_param_name = NULL;
	const char 	*error_attr = NULL;
	int param_error_code = 0;

	if (parse_result == NULL) {
		return;
	}
	if (result_paramtab == NULL) {
		if (parse_result->status == RTR_STATUS_PARSE_OK) {
			parse_result->status = RTR_STATUS_INTERNAL_ERR;
		}
	}
	if (parse_result->status != RTR_STATUS_PARSE_OK) {
		/* Don't Bother processing the paramtable */
		/* if there have been other errors */
		if (result_paramtab) {
			delete_RtrPtab_T(result_paramtab);
			result_paramtab = NULL;
		}
		return;
	}

	/*
	 * Check sys-defined paramtable enteries:  fill-in defaults
	 * for undeclared fixed properties and unset attributes.
	 * If the Scalable property is declared, then the other properties
	 * for scalable services (Network_resources_used, Port_list,
	 * Load_balancing_policy, Load_balancing_weights, Affinity_timeout,
	 * UDP_affinity, and Weak_affinity)
	 * are treated like fixed properties -- if undeclared, we fill-in
	 * their defaults.
	 */
	sysp = result_paramtab->sys_param;
	/*
	 * To tell if Scalable was declared, we use the fact that
	 * Scalable is the first property in nametable, therefore
	 * sysp[0] is the entry for Scalable.
	 */
	for (ix = 0; ix < (uint_t)N_SYS_PARAMS; ix++) {
		if (sysp[ix] != NULL) {
			const char *err = check_and_set_sysparm(ix, sysp[ix]);
			/*
			 * Check if we need to generate message for changing
			 * the tunablity of NRU.Check if the string returned is
			 * SCHA_NRU_TUNABLITY_CHANGED.If so set the
			 * nru_tunablity_changed flag.
			 */
			if ((err != NULL) &&
			    ((strcasecmp(err,
			    SCHA_NRU_TUNABLITY_CHANGED)) == 0)) {
				parse_result->nru_tunablity_changed = B_TRUE;
				/*
				 * Reset the err message
				 */
				err = NULL;
			}
			if (err != NULL) {
				error_param_name = &sysp[ix]->propname;
				param_error_code = RTR_STATUS_INVALID_ATTRVAL;
				error_attr = err;
				goto finished;
			}
		} else if (nametable[ix].type == RTR_SPT_SS &&
		    sysp[ix] == NULL && sysp[0] != NULL) {
			sysp[ix] = default_sysparam((int)ix);
			result_paramtab->sys_param_count++;
		} else if (nametable[ix].type == RTR_SPT_TO) {
			/*
			 * Method Timeout: Check that method exists for
			 * timeouts in file paramtable.  If needed,
			 * Create default timeout enteries.
			 */
			int method_found =
			    has_method((int)ix, parse_result->rt_registration);
			if (sysp[ix] != NULL && method_found == 0) {
				/* Timeout declared but method isn't */
				error_param_name = &sysp[ix]->propname;
				param_error_code = RTR_STATUS_INVALID_PARAM;
				error_attr = NULL;
			} else if (method_found && sysp[ix] == NULL) {
				sysp[ix] = default_sysparam((int)ix);
				result_paramtab->sys_param_count++;
			}
		} else if (nametable[ix].type == RTR_SPT_FX &&
		    sysp[ix] == NULL) {
			/* Create default entry for non-optional property */
			sysp[ix] = default_sysparam((int)ix);
			result_paramtab->sys_param_count++;
		}
	}

	n_ext = result_paramtab->extension_listsz;
	n_sys = result_paramtab->sys_param_count;
	listsz = n_ext + n_sys;

	/*
	 * listsz can not be 0, because  START and STOP method timeout
	 * and FAILOVER_MODE system defined properties will be added to
	 * result_paramtab->sys_param even these properties are not specified
	 * in rtr file.
	 */
	if (listsz == 0) {
		parse_result->status = RTR_STATUS_INTERNAL_ERR;
		parse_result->error_bufloc = "";
		delete_RtrPtab_T(result_paramtab);
		result_paramtab = NULL;
		return;
	}

	parray = (RtrParam_T **)calloc(listsz, sizeof (RtrParam_T *));

	if (parray == NULL) {
		parse_result->status = RTR_STATUS_INTERNAL_ERR;
		parse_result->error_bufloc = "";
		delete_RtrPtab_T(result_paramtab);
		result_paramtab = NULL;
		return;
	}

	/*
	 * Put extension properties at the end of the paramtab
	 * array in reverse order to undo the backward
	 * formation of the linked list.
	 * Fill in defaults.
	 */
	ix = listsz - 1;
	extp = result_paramtab->extensions;
	for (; ix >= n_sys && extp; ix--) {
		RtrParam_T *param = extp->param;

		parray[ix] = param;
		if (param->tunable == RTR_NONE) {
			param->tunable = RTR_WHEN_DISABLED;
		}
		/* Check Max length for STRINGS  */
		if (param->type == RTR_STRING ||
		    param->type == RTR_STRINGARRAY) {
			if (param->rtr_max_isset == 0) {
				param->rtr_max = MAXRGMVALUESLEN;
				param->rtr_max_isset = 1;
			}
		}

		/*
		 * validate the default value
		 */
		error_attr = validate_default_value(param);
		if (error_attr != NULL) {
			error_param_name = &param->propname;
			param_error_code = RTR_STATUS_INVALID_ATTRVAL;
			goto finished;
		}

		/* Clear pointer in container so param */
		/* is not deleted when container is deleted */
		extp->param = NULL;
		extp = extp->next;
	}
	/*
	 * Copy system defined parameters into the beginning of
	 * the paramtable array. Null enteries for properties
	 * that were not declared in file are not included in
	 * the final table.
	 */
	sysp = result_paramtab->sys_param;
	jx = 0;
	for (ix = 0; ix < N_SYS_PARAMS; ix++) {
		if (sysp[ix] != NULL) {
			parray[jx] = sysp[ix];
			/*
			 * Clear pointer in container so that the param
			 * is not deleted when the container is deleted
			 */
			sysp[ix] = NULL;
			jx ++;
			if (jx > n_sys) {
				parse_result->status = RTR_STATUS_INTERNAL_ERR;
				parse_result->error_bufloc = "";
				break;
			}
		}
	}

finished:
	delete_RtrPtab_T(result_paramtab);
	result_paramtab = NULL;
	parse_result->paramtab = parray;
	parse_result->paramtabsz = (int)listsz;

	if (error_param_name != NULL) {
		parse_result->status = param_error_code;
		parse_result->error_lineno = error_param_name->lineno;
		parse_result->error_bufloc =
		    error_attr != NULL ? error_attr : error_param_name->start;
		error_param_name = NULL;
	}
}


/*
 * incomplete_path returns the RtrString passed to it if that
 * string is a string and does not start with "/", and otherwise
 * returns NULL.  This is used to check method paths.
 */
static RtrString *
incomplete_path(RtrString *path_prop)
{
	if (path_prop != NULL &&
	    path_prop->value == RTR_STRING &&
	    path_prop->start != NULL &&
	    *(path_prop->start) != '/') {
		return (path_prop);
	} else {
		return (NULL);
	}
}


/*
 * Build upgrade and downgrade arrays in the parser_result from upgl.
 * The flag for_upgrade_from is used to distinguish which kind of directives is
 * being handled: true for upgrade_from list, and false for downgrade_to list.
 */
static void
set_rtupgrade_array(RtrFileResult *parse_result, RtrUpglist_T *upgl,
    boolean_t for_upgrade_from)
{
	RtrUpglist_T	*upgp;
	RtrUpg_T	**rtu_array = NULL;
	RtrUpg_T	**uarray = NULL;
	uint_t		i = 0;

	if (upgl == NULL) {
		return;
	}

	for (upgp = upgl; upgp; upgp = upgp->next) {
		uarray = (RtrUpg_T **)realloc(rtu_array,
		    (i + 2) * (sizeof (RtrUpg_T *)));
		if (uarray == NULL) {
			parse_result->status = RTR_STATUS_INTERNAL_ERR;
			parse_result->error_bufloc = "";
			delete_RtrUpglist_T(upgl);
			return;
		}

		rtu_array = uarray;
		rtu_array[i] = upgp->entry;
		i++;
	}

	if (for_upgrade_from) {
		parse_result->upgradearray = rtu_array;
		parse_result->upgradesz = (int)i;
	} else {
		parse_result->downgradearray = rtu_array;
		parse_result->downgradesz = (int)i;
	}

	/*
	 * Do not call delete_RtrUpglist_T() here.
	 * We don't want free the content of entry which has been
	 * kept in the parse_result.
	 */
	upgp = upgl;
	while (upgp != NULL) {
		upgl = upgp->next;
		free(upgp);
		upgp = upgl;
	}
}

/*
 * Build upgrade and downgrade arrays in the parser result from lists of
 * result_rtupgradeinfo that were built by action routines.
 */
static void
check_and_set_rtupgrade(RtrFileResult *parse_result)
{
	if (parse_result == NULL)
		return;

	if (parse_result->status != RTR_STATUS_PARSE_OK) {
		/* Don't Bother checking upgradelist content */
		/* if there have been other errors */
		return;
	}

	if (!result_rtupgradeinfo->sc30_rtr) {
		parse_result->sc30_rtr = B_FALSE;
		set_rtupgrade_array(parse_result,
		    result_rtupgradeinfo->upgradelist, B_TRUE);
		set_rtupgrade_array(parse_result,
		    result_rtupgradeinfo->downgradelist, B_FALSE);
	}
}


/*
 * Check the RT properties in the registration , fill-in defaults,
 * and add the registration information to the parse results
 */
static void
check_and_set_registration(RtrFileResult *parse_result)
{
	Rtr_T 	*rtres = result_rtregistration; /* to say some keystrokes */
	const char *error_prop = NULL;
	RtrString *error_string = NULL;

	if (parse_result == NULL || rtres == NULL) {
		return;
	}
	parse_result->rt_registration = rtres;
	if (parse_result->status != RTR_STATUS_PARSE_OK) {
		/* Don't Bother checking registration content */
		/* if there have been other errors */
		return;
	}
	if (!rtres->apiversion_isset) {
		/* Fill in Lowest supported of current API version */
		rtres->apiversion_isset = 1;
		rtres->apiversion = MIN_SCHA_API_VERSION;
	};
	/* Check for required callback methods */
	/* Missing callbacks don't have file locations, so return */
	/* a constant string for the error messages.  This is OK since */
	/* the string is not free'd through the error_bufloc pointer */
	error_prop = NULL;

	if (rtres->proxy == RTR_TRUE) {
		if (rtres->start.value == RTR_STRING ||
		    rtres->stop.value == RTR_STRING ||
		    rtres->monitor_start.value == RTR_STRING ||
		    rtres->monitor_stop.value == RTR_STRING) {
			/*
			 * Proxy resource cannot have START/STOP
			 * or MONITOR_START/MONITOR_STOP methods
			 */
			error_prop = SCHA_METHOD;
		} else if (rtres->prenet_start.value == RTR_NONE ||
		    rtres->prenet_start.value == RTR_NULL ||
		    rtres->postnet_stop.value == RTR_NONE ||
		    rtres->postnet_stop.value == RTR_NULL) {
			/*
			 * Proxy resource must have PRENET_SART
			 * and POSTNET_STOP methods.
			 */
			error_prop = SCHA_METHOD;
		}
	} else if ((rtres->start.value == RTR_NONE ||
	    rtres->start.value == RTR_NULL) &&
	    (rtres->prenet_start.value == RTR_NONE ||
	    rtres->prenet_start.value == RTR_NULL)) {
		/* Must have a START or PRENET_START */
		error_prop = SCHA_START;
	} else if ((rtres->stop.value == RTR_NONE ||
	    rtres->stop.value == RTR_NULL) &&
	    (rtres->postnet_stop.value == RTR_NONE ||
	    rtres->postnet_stop.value == RTR_NULL)) {
		/* Must have a STOP or POSTNET_STOP */
		error_prop = SCHA_STOP;
	} else if (rtres->monitor_start.value == RTR_STRING &&
	    (rtres->monitor_stop.value == RTR_NONE ||
	    rtres->monitor_stop.value == RTR_NULL)) {
		/*
		 * If there's a MONITOR_START,
		 * there must be a MONITOR_STOP
		 */
		error_prop = SCHA_MONITOR_STOP;
	} else if (rtres->monitor_stop.value == RTR_STRING &&
	    (rtres->monitor_start.value == RTR_NONE ||
	    rtres->monitor_start.value == RTR_NULL)) {
		/*
		 * If there's a MONITOR_STOP,
		 * there must be a MONITOR_START
		 */
		error_prop = SCHA_MONITOR_START;
	}
	if (error_prop != NULL) {
		parse_result->status = RTR_STATUS_INVALID_PROPVAL;
		parse_result->error_bufloc = error_prop;
		if (parse_result->error_bufloc == NULL)
			parse_result->error_bufloc = "";
		parse_result->error_lineno = 0;
		error_prop = NULL;
	}
	/* Check for complete paths for RT_basedir, and methods */
	if (rtres->basedir.value == RTR_STRING) {
		error_string = incomplete_path(&rtres->basedir);
	} else {
		/* No RT_basedir.  Each method-path therefore */
		/* must be complete */
		if ((error_string =
		    incomplete_path(&rtres->start)) != NULL)
			/*EMPTY*/;
		else if ((error_string =
		    incomplete_path(&rtres->stop)) != NULL)
			/*EMPTY*/;
		else if ((error_string =
		    incomplete_path(&rtres->validate)) != NULL)
			/*EMPTY*/;
		else if ((error_string =
		    incomplete_path(&rtres->update)) != NULL)
			/*EMPTY*/;
		else if ((error_string =
		    incomplete_path(&rtres->init)) != NULL)
			/*EMPTY*/;
		else if ((error_string =
		    incomplete_path(&rtres->fini)) != NULL)
			/*EMPTY*/;
		else if ((error_string =
		    incomplete_path(&rtres->boot)) != NULL)
			/*EMPTY*/;
		else if ((error_string =
		    incomplete_path(&rtres->monitor_start)) != NULL)
			/*EMPTY*/;
		else if ((error_string =
		    incomplete_path(&rtres->monitor_stop)) != NULL)
			/*EMPTY*/;
		else if ((error_string =
		    incomplete_path(&rtres->monitor_check)) != NULL)
			/*EMPTY*/;
		else if ((error_string =
		    incomplete_path(&rtres->prenet_start)) != NULL)
			/*EMPTY*/;
		else if ((error_string =
		    incomplete_path(&rtres->postnet_stop)) != NULL)
			/*EMPTY*/;
		else
			/*EMPTY*/;
	}
	if (error_string != NULL) {
		parse_result->status = RTR_STATUS_INVALID_PROPVAL;
		parse_result->error_bufloc = error_string->start;
		if (parse_result->error_bufloc == NULL)
			parse_result->error_bufloc = "";
		parse_result->error_lineno = error_string->lineno;
		error_string = NULL;
	}
}


#define	BUFFINCSZ 1024

/*
 *	RTR-Parser/Client Interface
 *
 *	A Resource-Type Registration File is opened and parsed.
 *	A status-code is returned, and the resulting RtrFileResult
 *	data structure through the client_parse_result reference parameter.
 *
 */
int
rtreg_parse(const char *filepath, RtrFileResult **client_parse_result)
{
	unsigned curloc = 0;
	unsigned bufsize = BUFFINCSZ;
	char *buffer = NULL;
	FILE *infile = NULL;
	int failed =  0;
	RtrFileResult *parse_result = NULL;
	int ic = 0;

	*client_parse_result = NULL;
	if (!filepath) {
		return (RTR_STATUS_FILE_ERR);
	}

	/* Open and read-in registration file */

	buffer = (char *)malloc(bufsize);
	if (buffer == NULL) {
		return (RTR_STATUS_INTERNAL_ERR);
	}

	infile = fopen(filepath, "r");
	if (infile == NULL || buffer == NULL) {
		return (RTR_STATUS_FILE_ERR);
	}
	while ((ic = getc(infile)) != EOF) {
		buffer[curloc++] = (char)ic;
		if (curloc >= bufsize) {
			char *newbuffer = NULL;
			bufsize += BUFFINCSZ;
			newbuffer = (char *)realloc(buffer, (size_t)bufsize);
			if (newbuffer == NULL) {
				/*
				 * Suppress lint Warning (644) [c:46]: buffer
				 * (line 2355) may not have been initialized
				 * From realloc(3C):
				 *	void *realloc(void *ptr, size_t size);
				 *	When realloc() returns NULL, the block
				 *	pointed to by ptr is left intact.
				 *	Therefore 'buffer', which was
				 *	initialized by malloc before entering
				 *	this loop, retains its value from
				 *	before the realloc() call, and is indeed
				 *	initialized.
				 */
				if (buffer != NULL)	/*lint !e644 */
					free(buffer);
				return (RTR_STATUS_FILE_ERR);
			} else {
				buffer = newbuffer;
			}
		}
	}
	buffer[curloc] = 0;
	if (fclose(infile))
		return (RTR_STATUS_FILE_ERR);

	infile = NULL;

	/* Initialize parser input and parser-action callbacks. */
	rtr_set_input(buffer);	/* set-up the input for the lexer */
	init_localcbs();
	rtr_set_callbacks(&localcbs); /* set-up parser action routines */

	/*
	 * Clear file variables that action routines above may set
	 * with error information.
	 * The error_cleanup_* variables hold partially built data structures
	 * that should be deleted if there's a syntax error.
	 */
	error_lineno = 0;
	error_bufloc = NULL;
	error_code = 0;
	error_cleanup_rtr = NULL;
	error_cleanup_vlist = NULL;
	error_cleanup_ptab = NULL;
	error_cleanup_param = NULL;

	/*
	 * result_rtregistration, result_rtupgradeinfo and result_paramtab are
	 * declared at file scope,
	 * above. These structures are built by the parser action routines,
	 * also above, and contain strings pointing into the input buffer.
	 * The parse_result will contain structures that point-into locations
	 * in the input buffer, so the input buffer is included as part
	 * the the parse_result.
	 * It is up to the parser client to delete parse_result,
	 * which is returned via the client_parse_result reference parameter.
	 * Deleting the RtrFileResult will also free the file input buffer.
	 */

	result_rtregistration = NULL;
	result_rtupgradeinfo = NULL;
	result_paramtab = NULL;

	failed = rtr_parse();
	parse_result = new_RtrFileResult(buffer);
	*client_parse_result = parse_result;

	if (parse_result == NULL) {
		if (buffer != NULL) {
			free(buffer);
		}
		return (RTR_STATUS_FILE_ERR);
	}

	if (failed != 0 || error_lineno != 0) {
		if (error_code == 0) {
			error_code = RTR_STATUS_INTERNAL_ERR;
		}

		parse_result->status = error_code;
		parse_result->error_bufloc = error_bufloc;
		if (parse_result->error_bufloc == NULL)
			parse_result->error_bufloc = "";
		parse_result->error_lineno = error_lineno;

		if (error_cleanup_rtr != NULL) {
			delete_Rtr_T(error_cleanup_rtr);
			error_cleanup_rtr = NULL;
		}
		if (error_cleanup_vlist != NULL) {
			delete_RtrVlist_T(error_cleanup_vlist);
			error_cleanup_vlist = NULL;
		}
		if (error_cleanup_ptab != NULL) {
			delete_RtrPtab_T(error_cleanup_ptab);
			error_cleanup_ptab = NULL;
		}
		if (error_cleanup_upginfo != NULL) {
			delete_RtrRTUinfo_T(error_cleanup_upginfo);
			error_cleanup_upginfo = NULL;
		}
		if (error_cleanup_param != NULL) {
			delete_RtrParam_T(error_cleanup_param);
			error_cleanup_param = NULL;
		}
	} else {
		parse_result->status = RTR_STATUS_PARSE_OK;
	}

	check_and_set_registration(parse_result);
	check_and_set_rtupgrade(parse_result);
	check_and_set_paramtable(parse_result);

	return (parse_result->status);
}
