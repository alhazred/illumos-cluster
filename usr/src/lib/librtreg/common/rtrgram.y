/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * ident	"@(#)rtrgram.y	1.28	08/05/20 SMI"
 */

/*
 *
 *	Yacc grammar for the Resource Type registration file
 *	format, defining the lexer/parser "rtr_parse()"
 *
 *	RTR Overview:
 *	------------
 *
 *	RTR (Resource Type Registration) is the file format for declaring
 *	the properties of a SunCluster resource type.  The file is used as
 *	input to adminstiration tools which register
 *
 *	A simple RTR description follows
 *
 *	A Comment has the form:
 *	    COMMENT	: # <anything but NEWLINE> NEWLINE
 *	And may appear after any token.
 *	They are treated as white-space
 *
 *	    type_dec	:  RESOUCE_TYPE = value ; proplist paramtable
 *
 *	    proplist	:  (empty)
 *			| proplist rtproperty
 *
 *	    rtproperty	: rtboolean_prop ;
 *			| rtvalue_prop ;
 *
 *	    rtboolean_prop	: SINGLE_INSTANCE
 *				| FAILOVER
 *				| RT_SYSTEM
 *				| GLOBALZONE
				| PROXY
 *
 *	    rtvalue_prop	: rtprop = value
 *				| PKGLIST = valuelist
 *
 *	    rtprop	: RT_BASEDIR
 *			| RT_VERSION
 *			| API_VERSION
 *			| INIT_NODES
 *			| START
 *			| STOP
 *			| VALIDATE
 *			| UPDATE
 *			| INIT
 *			| FINI
 *			| BOOT
 *			| MONITOR_START
 *			| MONITOR_STOP
 *			| MONITOR_CHECK
 *			| PRENET_START
 *			| POSTNET_STOP
 *			| RT_DESCRIPTION
 *			| SYSDEFINED_TYPE	(not in public API)
 *			| VENDOR_ID	(Used as prefix to form RT name)
 *			| rtboolean_prop
 *				(booleans need not have explict value assigned,
 *				in which casevalue is taken as TRUE)
 *
 *	    value	: <contiguous-non-ws-non-;-characters>
 *			| "<anything but quote>"
 *			| TRUE
 *			| FALSE
 *			| ANYTIME
 *			| WHEN_DISABLED
 *			| AT_CREATION
 *			| NONE			(value for TUNABLE attribute)
 *			| RG_PRIMARIES
 *			| RT_INSTALLED_NODES
 *			| SHARED_ADDRESS	(value for SYSDEFINED_TYPE)
 *			| LOGICAL_HOSTNAME	(value for SYSDEFINED_TYPE)
 *			| 			(NULL : explicit empty value)
 *
 *	    valuelist	: value
 *			| valuelist , value
 *
 *	    upgradelist	: (empty)
 *			| UPGRADE_HEADER rtupgrade (header must come first)
 *
 *	    rtupgrade	| (empty)
 *			| rtupgrade UPGRADE_FROM rt_version upgtunability
 *			| rtupgrade DOWNGRADE_TO rt_version upgtunability
 *
 *	    upgtunability	: ANYTIME
 *				| AT_CREATION
 *				| WHEN_DISABLED
 *				| WHEN_OFFLINE
 *				| WHEN_UNMANAGED
 *				| WHEN_UNMONITORED
 *
 *	    paramtable	:  (empty)
 *	                |  paramtable parameter
 *
 *	    parameter	:  { pproplist }
 *
 *	    pproplist	: PROPERTY = value ;	(property name must come first)
 *			| pproplist pproperty
 *
 *	    pproperty	: pboolean_prop ;
 *			| pvalue_prop ;
 *			| typespec ;
 *
 *	    pboolean_prop 	: EXTENSION
 *
 *	    pvalue_prop	: tunable_prop
 *			| pprop = value
 *			| pprop =		(NULL : no value setting)
 *			| DEFAULT = valuelist
 *
 *	    pprop	: DESCRIPTION
 *			| MIN
 *			| MAX
 *			| MINLENGTH
 *			| MAXLENGTH
 *			| ARRAY_MINSIZE
 *			| ARRAY_MAXSIZE
 *			| pboolean_prop
 *				( booleans may have explict value assigned )
 *
 *	    tunable_prop	: TUNABLE
 *				| TUNABLE = AT_CREATION
 *				| TUNABLE = ANYTIME
 *				| TUNABLE = WHEN_DISABLED
 *				| TUNABLE = NONE
 *
 *	    typespec	: INT
 *			| BOOLEAN
 *			| STRING
 *			| STRINGARRAY
 *			| ENUM { VALUELIST }
 *
 *	Client Interface to rtr_parse():
 *	-------------------------------
 *
 *	The interface is declared in the header-file "rtrclient.h"
 *
 *	The interface consists of:
 *		int rtr_parse()
 *			The yacc parser, produced after yacc processing
 *			of this file.
 *
 *		void rtr_set_intput(const char *)
 *			To set the input string for the parser,
 *			defined in this file.
 *
 *		void rtr_set_callbacks(const rtr_Callbacks *)
 *			To set parser call-back actions
 *			defined in this file.
 *
 *		Rtr_Callback structure
 *			Contains pointers to functions that will
 *			be called as parser-actions to build the
 *			client-defined data-structures.
 *			Defined in "rtrclient.h" in which there is
 *			more for detailed explanation of this structure.
 *
 *	The Rtr_Callback structure is "cback" in the parser-actions below.
 *
 *	The Rtr_Callback functions return pointers to client-defined
 *	structures.
 *
 *	Arguments passed to client-functions are pointers to client structure
 *	returned from actions for non-terminals,
 *	and string information (a char* start-position and string length)
 *	for token values.
 *
 *	Token strings are *not* copied by rtr_parse() or its lexer.
 *	The "char*" start-position in a token-value is within the
 *	input buffer given to the rtr_parse lexer by the client.
 *
 *	Memory management is entirely up to the client.
 *	The client is responsible for allocating and deallocating
 *	the input-buffer, copying token-strings out of that buffer,
 *	and allocating/deallocating Client objects.
 *
 *	The parser is *not* reentrant, both yacc and the lexer rtr_lex()
 *	uses global state variables.
 *
 *
 */
%{
/*
 * Copyright 2005 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 * @(#)rtrgram.y	1.28	08/05/20 SMI
 */
#pragma ident	"@(#)rtrgram.y	1.28	08/05/20 SMI"
/*

	In this section are the
	Declarations of data and functions needed by the parser rtr_parse

	In the section following the yacc grammar are
	The lexer rtr_lex, and other functions data needed for lexing.

	After being processed by yacc, this file provides the
	interface functions:
		rtr_parse
		rtr_set_intput
		rtr_set_callbacks
*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <scha_tags.h>
#include "rgm/scha_strings.h"
#include "rtrclient.h"

#define	NOI18N(s) s

/*
 * Change YACC generated names with #defines so that parser can
 * coexist with other YACC generated parsers.
 */
#define	 yyparse	rtr_parse
#define	 yylex	rtr_lex
#define	 yyerror rtr_yyerror
#define	 yy_yys	rtr_yy_yys
#define	 yy_yyv	rtr_yy_yyv
#define	 yyact	rtr_yyact
#define	 yychar	rtr_yychar
#define	 yychk	rtr_yychk
#define	 yydebug	rtr_yydebug
#define	 yydef	rtr_yydef
#define	 yyerrflag	rtr_yyerrflag
#define	 yyexca	rtr_yyexca
#define	 yylval	rtr_yylval
#define	 yynerrs	rtr_yynerrs
#define	 yypact	rtr_yypact
#define	 yypgo	rtr_yypgo
#define	 yyps	rtr_yyps
#define	 yypv	rtr_yypv
#define	 yyr1	rtr_yyr1
#define	yyr2	rtr_yyr2
#define	yys	rtr_yys
#define	yystate	rtr_yystate
#define	yytmp	rtr_yytmp
#define	yyv	rtr_yyv
#define	yyval	rtr_yyval
#define	yytoks	rtr_yytoks
#define	yyreds	rtr_yyreds


extern int rtr_parse(void);
static int rtr_lex(void);
static void rtr_yyerror(const char *);

/*
 *	The Client passes-in a structure of call-back pointers
 *	with "rtr_set_callbacks";
 *	If no Callback structure is given, the default is zero
 *	static-initialized structure.
 *
 *	Null callback pointers in the structure result in Null
 *	values being returned from parser actions.
 */

static const Rtr_Callbacks default_cback = { 0 };

static const Rtr_Callbacks *cback = &default_cback;

void
rtr_set_callbacks(const Rtr_Callbacks *cb)
{
	cback = cb ? cb : &default_cback;
}


/* macros */
#define	YYMAXDEPTH 600

#define	YYCLEAN

#ifdef DEBUG
#ifndef YYDEBUG
#define	YYDEBUG 1
#endif
#endif

%}

%union {
	RTRP_Code code;
	RtrValueToken vtok;
	Rtr_t *rtrp;
	RtrRTUinfo_t *upglp;
	RtrPtab_t *ptabp;
	RtrParam_t *paramp;
	RtrVlist_t *vlp;
}

%token ERRTOK

/* punctuation */
%token SEMI
%token ASSIGN
%token LB	/* left brace */
%token RB	/* right brace */
%token COMMA


/* keywords */
%token API_VERSION
%token ARRAY_MAXSIZE
%token ARRAY_MINSIZE
%token BOOLEAN
%token BOOT
%token DEFAULT
%token DESCRIPTION
%token ENUM
%token EXTENSION
%token FAILOVER
%token GLOBALZONE
%token FINI
%token INIT
%token INIT_NODES
%token INT
%token MAX
%token MAXLENGTH
%token MIN
%token MINLENGTH
%token MONITOR_CHECK
%token MONITOR_START
%token MONITOR_STOP
%token PER_NODE
%token PKGLIST
%token POSTNET_STOP
%token PRENET_START
%token PROPERTY
%token PROXY
%token RESOURCE_TYPE
%token RT_BASEDIR
%token RT_DESCRIPTION
%token RT_SYSTEM
%token RT_VERSION
%token SINGLE_INSTANCE
%token START
%token STOP
%token STRINGARRAY
%token STRINGTYPE
%token SYSDEFINED_TYPE
%token TUNABLE
%token UPDATE
%token UPGRADE_HEADER
%token UPGRADE_FROM
%token DOWNGRADE_TO
%token VALIDATE
%token VENDOR_ID

%token VALUE



%type <code>	API_VERSION ARRAY_MINSIZE ARRAY_MAXSIZE BOOLEAN
		BOOT DEFAULT DESCRIPTION DOWNGRADE_TO ENUM EXTENSION PER_NODE FAILOVER
		GLOBALZONE PROXY FINI INIT INIT_NODES INT
		MAX MAXLENGTH MIN MINLENGTH MONITOR_CHECK
		MONITOR_START MONITOR_STOP PKGLIST POSTNET_STOP
		PRENET_START PROPERTY RESOURCE_TYPE
		RT_BASEDIR RT_DESCRIPTION RT_SYSTEM RT_VERSION
		SINGLE_INSTANCE START STOP STRINGARRAY STRINGTYPE
		SYSDEFINED_TYPE TUNABLE UPDATE UPGRADE_FROM UPGRADE_HEADER
		VALIDATE VENDOR_ID
		prop propsimple param paramsimple parambool

%type <vtok>	VALUE

%type <rtrp>	proplist

%type <upglp>	upgradelist rtupgrade

%type <ptabp>	paramtable

%type <paramp>	pattributes

%type <vlp>	vlist


%start type_file

%%

type_file	: proplist upgradelist paramtable
			{
				/*
				 * The resource type registration file is
				 * the property list, rt upgrade
				 * tunabilities and paramtable.
				 */
				if (cback->rtr_file)
					cback->rtr_file($1, $2, $3);

			}

proplist	: RESOURCE_TYPE ASSIGN VALUE SEMI
			{
				/*
				 * First "property" declares the
				 * resource type name.
				 * This action returns the rtr_T structure that
				 * accumulates the resource-type properties
				 */
				$$ = (cback->rtdec) ?
					cback->rtdec($3)
					:0;

			}
		| proplist propsimple SEMI
			{
				$$ = (cback->propsimple) ?
					cback->propsimple($1, $2)
					: 0;

			}
		| proplist prop ASSIGN VALUE SEMI
			{
				$$ = (cback->propval) ?
					cback->propval($1, $2, $4)
					: 0;

			}
		| proplist prop ASSIGN SEMI
			{
				/*
				 * NULL value setting
				 */
				RtrValueToken novalue;
				novalue.vt = RTRV_NULL;
				novalue.start = 0;
				novalue.len = 0;
				novalue.num = 0;
				novalue.lineno = 0;
				$$ = (cback->propval) ?
					cback->propval($1, $2, novalue)
					: 0;
			}
		| proplist PKGLIST ASSIGN vlist SEMI
			{
				$$ = (cback->proppkgs) ?
					cback->proppkgs($1, $4)
					: 0;
			}
		| proplist PKGLIST ASSIGN SEMI
			{
				$$ = (cback->proppkgs) ?
					cback->proppkgs($1, NULL)
					: 0;
			}
		;

propsimple	: SINGLE_INSTANCE
		| FAILOVER
		| PROXY
		| RT_SYSTEM
		| GLOBALZONE
		;


prop		: RT_BASEDIR
		| RT_VERSION
		| API_VERSION
		| INIT_NODES
		| START
		| STOP
		| VALIDATE
		| UPDATE
		| INIT
		| FINI
		| BOOT
		| MONITOR_START
		| MONITOR_STOP
		| MONITOR_CHECK
		| PRENET_START
		| POSTNET_STOP
		| VENDOR_ID
		| RT_DESCRIPTION
		| SYSDEFINED_TYPE
		| propsimple
		;

vlist		: VALUE
			{
				$$ = (cback->valuelist) ?
					cback->valuelist(NULL, $1)
					: 0;
			}
		| vlist COMMA VALUE
			{
				$$ = (cback->valuelist) ?
					cback->valuelist($1, $3)
					: 0;
			}
		;

upgradelist	: /* empty */
			{
				$$ = (cback->upgheader) ?
					cback->upgheader(NULL, RTRV_FALSE)
					: 0;

			}

		| UPGRADE_HEADER rtupgrade
			{
				$$ = (cback->upgheader) ?
					cback->upgheader($2, RTRV_TRUE)
					: 0;
			}
		;

rtupgrade	: /* empty */
			{
				0;
			}
		| rtupgrade UPGRADE_FROM VALUE VALUE
			{
				$$ = (cback->upglist) ?
					cback->upglist($1, $3, $4)
					: 0;
			}
		| rtupgrade DOWNGRADE_TO VALUE VALUE
			{
				$$ = (cback->downglist) ?
					cback->downglist($1, $3, $4)
					: 0;
			}
		;

paramtable	:  /* empty */
			{
				$$ = (cback->parameter) ?
					cback->parameter(NULL, NULL)
					: 0;
			}
		| paramtable LB pattributes RB
			{
				$$ = (cback->parameter) ?
					cback->parameter($1, $3)
					: 0;
			}
		;

pattributes	: PROPERTY ASSIGN VALUE SEMI
			{
				/*
				 * First parameter attribute declares
				 * the resource property name.
				 * This action returns the ptab_T structure
				 * that accumulates the resource property
				 * attributes
				 */
				$$ = (cback->paramdec) ?
					cback->paramdec($3)
					: 0;
			}
		| pattributes paramsimple SEMI
			{
				$$ = (cback->paramsimple) ?
					cback->paramsimple($1, $2)
					: 0;
			}
		| pattributes param ASSIGN VALUE SEMI
			{
				$$ = (cback->paramval) ?
					cback->paramval($1, $2, $4)
					: 0;
			}
		| pattributes param ASSIGN SEMI
			{
				/*
				 * NULL value setting
				 */
				RtrValueToken novalue;
				novalue.vt = RTRV_NULL;
				novalue.start = 0;
				novalue.len = 0;
				novalue.num = 0;
				novalue.lineno = 0;
				$$ = (cback->paramval) ?
					cback->paramval($1, $2, novalue)
					: 0;
			}
		| pattributes ENUM LB vlist RB SEMI
			{
				$$ = (cback->paramenum) ?
					cback->paramenum($1, $4)
					: 0;
			}

		| pattributes DEFAULT ASSIGN vlist SEMI
			{
				$$ = (cback->paramdefault) ?
					cback->paramdefault($1, $4)
					: 0;
			}
		| pattributes DEFAULT ASSIGN SEMI
			{
				/*
				 * NULL value setting
				 */
				$$ = (cback->paramdefault) ?
					cback->paramdefault($1, NULL)
					: 0;
			}
		;

paramsimple	: parambool
		| PER_NODE
		| TUNABLE
		| INT
		| BOOLEAN
		| STRINGTYPE
		| STRINGARRAY
		;

parambool	: EXTENSION
		;
	

param		: DESCRIPTION
		| MIN
		| MAX
		| MINLENGTH
		| MAXLENGTH
		| ARRAY_MINSIZE
		| ARRAY_MAXSIZE
		| TUNABLE
		| PER_NODE

		| parambool /*
		| PER_NODE
		| parambool_pnode*/
		;



%%

/*
 *	In this section are:
 *
 *	Lexical Scanner and Auxillary Functions and Data
 *
 *	The input buffer is a NULL terminated string set by the client
 *	with a call to rtr_set_input().
 *	The current location in the buffer is pointed-to by "bufloc"
 */


static const char *bufloc = "";
static const char *last_token_start = "";  /* to report error location. */
static int linecount = 1;

/*
 * Keep track of context to make lexing easier.
 */
static enum Context {
	none,
	start_key,	/* look for property keyword after ';' */
	start_value,	/* look for value after a '=' */
	start_enum	/* look for open brace after enum keyword */
} context = start_key;

void
rtr_set_input(const char *buff)
{
	bufloc = last_token_start = buff;
	linecount = 1;
	context = start_key;
}

void
rtr_get_current_location(int *lineno, const char ** bufferloc)
{
	if (lineno != NULL) {
		*lineno = linecount;
	}
	if (bufloc != NULL) {
		*bufferloc = last_token_start;
	} else
		/* have to set it to blank in here if there is no */
		/* error buffer location is not set */
		*bufferloc = "";
}

const char *
skip_white_space(const char *p)
{
	/* white space includes comments */
AGAIN:
	while (isspace(*p)) {
		if (*p == '\n') {
			linecount++;
		}
		p += mblen(p, MB_CUR_MAX);
	}
	if (*p == '#') {
		/*
		 * Check if this '#' is followed by '$'.
		 * If it is, this is not a comment.  This is a
		 * directive of rt upgrade tunability.
		 * Just leave out "#$" and proceed.
		 */
		p += mblen(p, MB_CUR_MAX);
		if (*p == '$') {
			/* skip '$' and the following spaces */
			p += mblen(p, MB_CUR_MAX);
			while (isspace(*p))
				p += mblen(p, MB_CUR_MAX);

			/* Tell parser to look for keyword token after "#$" */
			context = start_key;
		} else {
			/* skip to end of line (or file) */
			while ((*p != '\n') && (*p != 0)) {
				p += mblen(p, MB_CUR_MAX);
			}
			if (*p == '\n') {
				p++;	/* skip over newline */
				linecount++;
				goto AGAIN;
			}
		}
	}
	return (p);
}

static int
maketok_val(int tval, const char *tstart, int tlen,
		RTRV_Type vt, int num)
{
	yylval.vtok.start = tlen > 0 ? tstart : 0;
	yylval.vtok.len = tstart ? tlen : 0;
	yylval.vtok.vt = vt;
	yylval.vtok.num = num;
	yylval.vtok.lineno = linecount;
	return (tval);
}

static int
maketok_code(int tval, RTRP_Code code)
{
	yylval.code = code;
	return (tval);
}

static int
maketok(int tval)
{
	yylval.code = RTRP_NONE;
	return (tval);
}


static int key_tok(const char *curp);
static int quoted_vtok(const char *curp);
static int value_tok(const char *curp);

static int
rtr_lex()
{
	const char *curp = skip_white_space(bufloc);

	last_token_start = curp;	/* In case of error. */

	switch (*curp) {

	default:
		if (context == start_key) {
			context = none;
			return (key_tok(curp));
		}
		return (value_tok(curp));

	case '"':
		return (quoted_vtok(curp));
	case '=':
		bufloc = curp + 1;
		context = start_value;
		return (maketok(ASSIGN));
	case ',':
		bufloc = curp + 1;
		context = start_value;
		return (maketok(COMMA));

	case ';':
		bufloc = curp + 1;
		context = start_key;
		return (maketok(SEMI));
	case '{':
		bufloc = curp + 1;
		context = (context == start_enum) ?
				start_value
				: start_key;
		return (maketok(LB));
	case '}':
		bufloc = curp + 1;
		context = none;
		return (maketok(RB));

	case '\0':
		/* leave buffer-location at end-of-input */
		return (-1);

	}
}


static const char *keybrkchars = NOI18N("=;# \t\n{");
static const char *valbrkchars = NOI18N("=;,# \t\n}");
static const char *digits = NOI18N("0123456789");

typedef struct KW {
	const char *kw;
	int len;
	int tok;		/* yacc token value */
	RTRP_Code code;		/* property code from client interface */
} KW;
#define	KEYANDLEN(s) s, sizeof (s)-1

static const KW akey[] = {
{ KEYANDLEN(SCHA_API_VERSION),		API_VERSION,	RTRP_API_VERSION },
{ KEYANDLEN(SCHA_ARRAY_MAXSIZE),	ARRAY_MAXSIZE,	RTRP_ARRAY_MAXSIZE},
{ KEYANDLEN(SCHA_ARRAY_MINSIZE),	ARRAY_MINSIZE,	RTRP_ARRAY_MINSIZE},
{0,		0,	0 },
};
static const KW bkey[] = {
{ KEYANDLEN(SCHA_BOOLEAN),		BOOLEAN,	RTRP_BOOLEAN },
{ KEYANDLEN(SCHA_BOOT),			BOOT,		RTRP_BOOT },
{0,		0,	0 },
};
static const KW dkey[] = {
{ KEYANDLEN(SCHA_DEFAULT),		DEFAULT,	RTRP_DEFAULT },
{ KEYANDLEN(SCHA_DESCRIPTION),		DESCRIPTION,	RTRP_DESCRIPTION },
{ KEYANDLEN(SCHA_DOWNGRADE_TO),		DOWNGRADE_TO,	RTRP_DOWNGRADE_TO },
{0,		0,	0 },
};
static const KW ekey[] = {

{ KEYANDLEN(SCHA_ENUM),			ENUM,		RTRP_ENUM },
{ KEYANDLEN(SCHA_EXTENSION),		EXTENSION,	RTRP_EXTENSION },
{0,		0,	0 },
};

static const KW fkey[] = {

{ KEYANDLEN(SCHA_FAILOVER),		FAILOVER,	RTRP_FAILOVER },
{ KEYANDLEN(SCHA_FINI),			FINI,		RTRP_FINI },
{0,		0,	0 },
};

static const KW gkey[] = {
{ KEYANDLEN(SCHA_GLOBALZONE),		GLOBALZONE,	RTRP_GLOBALZONE },
{0,		0,	0 },
};

static const KW ikey[] = {
{ KEYANDLEN(SCHA_INIT),			INIT,		RTRP_INIT },
{ KEYANDLEN(SCHA_INIT_NODES),		INIT_NODES,	RTRP_INIT_NODES },
{ KEYANDLEN(SCHA_INT),			INT,		RTRP_INT },
{0,		0,	0 },
};
static const KW mkey[] = {
{ KEYANDLEN(SCHA_MAX),			MAX,		RTRP_MAX },
{ KEYANDLEN(SCHA_MAXLENGTH),		MAXLENGTH,	RTRP_MAXLENGTH },
{ KEYANDLEN(SCHA_MIN),			MIN,		RTRP_MIN },
{ KEYANDLEN(SCHA_MINLENGTH),		MINLENGTH,	RTRP_MINLENGTH },
{ KEYANDLEN(SCHA_MONITOR_CHECK),	MONITOR_CHECK,	RTRP_MONITOR_CHECK },
{ KEYANDLEN(SCHA_MONITOR_START),	MONITOR_START,	RTRP_MONITOR_START },
{ KEYANDLEN(SCHA_MONITOR_STOP),		MONITOR_STOP,	RTRP_MONITOR_STOP },
{0,		0,	0 },
};
static const KW pkey[] = {
{ KEYANDLEN(SCHA_PKGLIST),		PKGLIST,	RTRP_PKGLIST },
{ KEYANDLEN(SCHA_POSTNET_STOP),		POSTNET_STOP,	RTRP_POSTNET_STOP },
{ KEYANDLEN(SCHA_PRENET_START),		PRENET_START,	RTRP_PRENET_START },
{ KEYANDLEN(SCHA_PROPERTY),		PROPERTY,	RTRP_PROPERTY },
{ KEYANDLEN(SCHA_PER_NODE),		PER_NODE,	RTRP_PER_NODE },
{ KEYANDLEN(SCHA_PROXY),		PROXY,		RTRP_PROXY },
{0,		0,	0 },
};
static const KW rkey[] = {
{ KEYANDLEN(SCHA_RESOURCE_TYPE),	RESOURCE_TYPE,	RTRP_RESOURCE_TYPE },
{ KEYANDLEN(SCHA_RT_BASEDIR),		RT_BASEDIR,	RTRP_RT_BASEDIR },
{ KEYANDLEN(SCHA_RT_DESCRIPTION),	RT_DESCRIPTION,	RTRP_RT_DESCRIPTION},
{ KEYANDLEN(SCHA_RT_SYSTEM),		RT_SYSTEM,	RTRP_RT_SYSTEM },
{ KEYANDLEN(SCHA_RT_VERSION),		RT_VERSION,	RTRP_RT_VERSION },
{0,		0,	0 },
};
static const KW skey[] = {
{ KEYANDLEN(SCHA_SINGLE_INSTANCE),	SINGLE_INSTANCE, RTRP_SINGLE_INSTANCE },
{ KEYANDLEN(SCHA_START),		START,		RTRP_START },
{ KEYANDLEN(SCHA_STOP),			STOP,		RTRP_STOP },
{ KEYANDLEN(SCHA_STRINGARRAY),		STRINGARRAY,	RTRP_STRINGARRAY },
{ KEYANDLEN(SCHA_STRING),		STRINGTYPE,	RTRP_STRINGTYPE },
{ KEYANDLEN(SCHA_SYSDEFINED_TYPE),	SYSDEFINED_TYPE, RTRP_SYSDEFINED_TYPE},
{0,		0,	0 },
};
static const KW tkey[] = {
{ KEYANDLEN(SCHA_TUNABLE),		TUNABLE,	RTRP_TUNABLE },
{0,		0,	0 },
};
static const KW ukey[] = {
{ KEYANDLEN(SCHA_UPDATE),		UPDATE,		RTRP_UPDATE },
{ KEYANDLEN(SCHA_UPGRADE_FROM),		UPGRADE_FROM,	RTRP_UPGRADE_FROM },
{ KEYANDLEN(SCHA_UPGRADE_HEADER),	UPGRADE_HEADER,	RTRP_UPGRADE_HEADER },
{0,		0,	0 },
};
static const KW vkey[] = {
{ KEYANDLEN(SCHA_VALIDATE),		VALIDATE,	RTRP_VALIDATE },
{ KEYANDLEN(SCHA_VENDOR_ID),		VENDOR_ID,	RTRP_VENDOR_ID },
{0,		0,	0 },
};

static int
key_tok(const char *curp)
{
	const KW *key = akey;
	int len = 0;
	int ix = 0;

	/* Look for a property or attribute keyword */
	curp = skip_white_space(curp);

	switch (*curp) {
	case 'a':
	case 'A':
		key = akey;
		break;
	case 'b':
	case 'B':
		key = bkey;
		break;
	case 'd':
	case 'D':
		key = dkey;
		break;
	case 'e':
	case 'E':
		key = ekey;
		break;
	case 'f':
	case 'F':
		key = fkey;
		break;
	case 'g':
	case 'G':
		key = gkey;
		break;

	case 'i':
	case 'I':
		key = ikey;
		break;
	case 'm':
	case 'M':
		key = mkey;
		break;
	case 'p':
	case 'P':
		key = pkey;
		break;
	case 'r':
	case 'R':
		key = rkey;
		break;
	case 's':
	case 'S':
		key = skey;
		break;
	case 't':
	case 'T':
		key = tkey;
		break;
	case 'u':
	case 'U':
		key = ukey;
		break;
	case 'v':
	case 'V':
		key = vkey;
		break;

	default:
		/* Not a recognized keyword */

		return (maketok(ERRTOK));
	};

	len = strcspn(curp, keybrkchars);
	for (ix = 0; key[ix].kw; ix++) {
		/* Case is ignored, in ASCII assuming strncasecmp. */
		/* To get case senitive keyword match use strncmp */
		if (len == key[ix].len &&
			strncasecmp(curp, key[ix].kw, key[ix].len) == 0) {
			break;
		}
	}
	if (key[ix].len == 0) {
		/* Not a recognized keyword */
		return (maketok(ERRTOK));
	}
	if (key[ix].tok == ENUM) {
		context = start_enum;
	}
	bufloc = curp + key[ix].len;
	return (maketok_code(key[ix].tok, key[ix].code));
}

/*
 * For keyword designated values, the parser token value is VALUE.
 * The VKW array contains the enum value designated a recognized symbolic value
 * NOTE: "RTRV_NONE" designates an unset value, so an explicit "NONE" value
 * for the tunable attribute, or as an enum value for failover-mode,
 * is indicated as RTRV_NONE_STRING.
 */
typedef struct VKW {
	const char *kw;
	int len;
	RTRV_Type vt;
} VKW;
static const VKW value[] = {
	{KEYANDLEN(SCHA_TRUE),			RTRV_TRUE },
	{KEYANDLEN(SCHA_FALSE),			RTRV_FALSE },
	{KEYANDLEN(SCHA_AT_CREATION),		RTRV_AT_CREATION },
	{KEYANDLEN(SCHA_ANYTIME),		RTRV_ANYTIME },
	{KEYANDLEN(SCHA_WHEN_DISABLED),		RTRV_WHEN_DISABLED },
	{KEYANDLEN(SCHA_WHEN_OFFLINE),		RTRV_WHEN_OFFLINE },
	{KEYANDLEN(SCHA_WHEN_UNMANAGED),	RTRV_WHEN_UNMANAGED },
	{KEYANDLEN(SCHA_WHEN_UNMONITORED),	RTRV_WHEN_UNMONITORED },
	{KEYANDLEN(SCHA_NONE),			RTRV_NONE_STRING },
	{KEYANDLEN(SCHA_RG_PRIMARIES),		RTRV_RG_PRIMARIES},
	{KEYANDLEN(SCHA_RT_INSTALLED_NODES),	RTRV_RT_INSTALLED_NODES },
	{KEYANDLEN(SCHA_LOGICAL_HOSTNAME),	RTRV_LOGICAL_HOSTNAME },
	{KEYANDLEN(SCHA_SHARED_ADDRESS),	RTRV_SHARED_ADDRESS },
	{0,	0,	0 },
};


static int
value_tok(const char *curp)
{
	int len = 0;
	RTRV_Type vt = RTRV_STRING;
	int numval = 0;

	curp = skip_white_space(curp);
	len = strcspn(curp, valbrkchars);

	/* Check if an integer, may be negative */
	if ((*curp == '-') || isdigit(*curp)) {
		/* double check that whole string forms an int value */
		int minus_char = (*curp == '-') ? 1 : 0;
		const char *numstart = curp + minus_char;
		int numlen = strspn(numstart, digits);
		if (len == (numlen + minus_char)) {
			vt = RTRV_NUM;
			numval = atoi(curp);
		}
	}
	if (vt != RTRV_NUM) {
		/* Check if a keyword symbolic value */
		int ix = 0;
		for (; value[ix].kw; ix++) {
			/* Case is ignored, in ASCII assuming strncasecmp. */
			/* To get case senitive keyword match use strncmp */
			if (len == value[ix].len &&
			    strncasecmp(curp, value[ix].kw, value[ix].len)
			    == 0) {
				vt = value[ix].vt;
				break;
			}
		}
	}
	bufloc = curp + len;
	return (maketok_val(VALUE, curp, len, vt, numval));
}

static int
quoted_vtok(const char *curp)
{
	/* NOTE: the quotes are not include in the string token value */

	const char qchar = *curp;
	const char *startp = curp + 1;
	const char *endp = startp;

	while (*endp) {
		int len = mblen(endp, MB_CUR_MAX);
		if (len == 1 && *endp == qchar&& *(endp-1) != '\\') break;
		if (len == 1 && *endp == '\n') {
			/* Newline not allowed in strings */
			return (maketok(ERRTOK));
		}
		endp += len;
	}
	if (!*endp) {
		return (maketok(ERRTOK));
	}
	bufloc = endp+1;	/* get past closing quote */

	return (maketok_val(VALUE, startp, endp-startp, RTRV_STRING, 0));
}

static void
rtr_yyerror(const char *yyerr)
{
	if (cback->syntax_error) {
		cback->syntax_error(yyerr, last_token_start, linecount);
	}
}
