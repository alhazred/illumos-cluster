/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RTRCLIENT_H
#define	_RTRCLIENT_H

#pragma ident	"@(#)rtrclient.h	1.25	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

/*
 *	Resource Type Registration File YACC Parser Interface
 *
 *	The interface to the skeletal YACC parser.
 *	Client must set its own action routines to build parse
 *	data structures.
 *
 *	USAGE:
 *	    1. Set YACC parser input to buffer containing read-in
 *	       registration file with:
 *			void rtr_set_input(const char*);
 *
 *	    2. Set the parser actions to a structure containing
 *	       pointers to callback  functions with:
 *			void rtr_set_callbacks(const Rtr_Callbacks*);
 *
 *	    3. Runn YACC parse which is:
 *			int rtr_parse(void);
 *
 *	Results are indicated by the rtr_parse yacc parser return code,
 *	and whatever actions are done in the callback routines.
 */

/* Property codes */
enum RTRP_Code {
	RTRP_NONE,
	RTRP_API_VERSION,
	RTRP_ARRAY_MAXSIZE,
	RTRP_ARRAY_MINSIZE,
	RTRP_BOOLEAN,
	RTRP_BOOT,
	RTRP_DEFAULT,
	RTRP_DESCRIPTION,
	RTRP_DOWNGRADE_TO,
	RTRP_ENUM,
	RTRP_EXTENSION,
	RTRP_FAILOVER,
	RTRP_GLOBALZONE,
	RTRP_FINI,
	RTRP_INIT,
	RTRP_INIT_NODES,
	RTRP_INT,
	RTRP_MAX,
	RTRP_MAXLENGTH,
	RTRP_MIN,
	RTRP_MINLENGTH,
	RTRP_MONITOR_CHECK,
	RTRP_MONITOR_START,
	RTRP_MONITOR_STOP,
	RTRP_PER_NODE,
	RTRP_PKGLIST,
	RTRP_POSTNET_STOP,
	RTRP_PRENET_START,
	RTRP_PROPERTY,
	RTRP_PROXY,
	RTRP_RESOURCE_TYPE,
	RTRP_RT_DESCRIPTION,
	RTRP_RT_BASEDIR,
	RTRP_RT_VERSION,
	RTRP_SINGLE_INSTANCE,
	RTRP_START,
	RTRP_STOP,
	RTRP_STRINGARRAY,
	RTRP_STRINGTYPE,
	RTRP_SYSDEFINED_TYPE,
	RTRP_TUNABLE,
	RTRP_UPDATE,
	RTRP_UPGRADE_FROM,
	RTRP_UPGRADE_HEADER,
	RTRP_VALIDATE,
	RTRP_RT_SYSTEM,
	RTRP_VENDOR_ID
};
typedef enum RTRP_Code RTRP_Code;


/*
	RtrValueToken is the yacc parser token type to hold property
	and attribute values.
	All values are strings, indicated by a pointer into the input
	buffer to the start of the string and its length.
	Some strings are recognized as specific values or an integers.
	The type code and num value hold addition information on these
	strings.
*/
typedef enum RTRV_Type {
	RTRV_STRING,
	RTRV_NUM,
	RTRV_TRUE,
	RTRV_FALSE,
	RTRV_AT_CREATION,
	RTRV_ANYTIME,
	RTRV_WHEN_DISABLED,
	RTRV_WHEN_OFFLINE,
	RTRV_WHEN_UNMANAGED,
	RTRV_WHEN_UNMONITORED,
	RTRV_RG_PRIMARIES,
	RTRV_RT_INSTALLED_NODES,
	RTRV_LOGICAL_HOSTNAME,
	RTRV_SHARED_ADDRESS,
	RTRV_NONE_STRING,	/* a string value of "NONE" */
	RTRV_NULL,
	RTRV_NONE		/* no value is set */
} RTRV_Type;

typedef struct RtrValueToken {
	RTRV_Type vt;
	const char *start;
	int len;
	int num;
	int lineno;
} RtrValueToken;

/*
 *	Parser Call-back Interface.
 *
 *	Client provides a Rtr_Callbacks structure containing pointers to the
 *	client-defined callback functions with a call to "rtr_set_callbacks"
 *
 *	A null callback pointer will result in no-action being called for the
 *	associated parser action.  An unset or null Rtr_Callback will result
 *	in no actions being done with the parse.
 *
 *	The client also defines the "Rtr_t" type that is returned-by,
 *	and is passed to, the action callbacks.
 *
 *	Callback Return Values Are:
 *
 *		Rtr_t* values that are pointers-to a client data structure for
 *			accumulating resource-type properties.
 *
 *		RtrRTUinfo_t* values that are pointers-to a client data
 *			structure for accumulating two lists of resource type
 *			versions and the tunability: one for upgrade_from and
 *			one for downgrade_to.
 *
 *		RtrPtab_t* values that are pointers-to a client data structure
 *				for accumulating paramtable elements.
 *
 *		RtrParam_t* values that are pointers-to a client data structure
 *				for accumulating attributes of a paramtable
 *				element.
 *
 *		RtrVlist_t * values that are pointer-to a client data structure
 *				for accumulating a list of string values
 *
 *
 *	Callback Arguments Are:
 *
 *		Parser Token Values, of type struct RtrValueToken,
 *			declared above.
 *			NOTE: The char* pointers in an RtrValueToken point
 *			into the file buffer
 *
 *		The Rtr_t* is the agregregate structure that accumulates the
 *			resource-type properties.
 *
 *		The RtrRTUinfo_t* is the agregregate structure that accumulates
 *			information of upgrade_from and downgrade_to directives.
 *
 *		The RtrPtab_t* is the aggregate structure that accumulates the
 *			parameter declarations.
 *
 *		The RtrParam_t* is the aggregate structure that accumulates the
 *			attributes of a parameter declaration.
 *
 *		If an action call-back is Null, Null is the value returned
 *		from the associated action value;  this Null value may be
 *		passed as an argument to a provided call-back.
 *
 */


/*
 * Data structures to be defined by callback implementation
 */

typedef struct Rtr_T Rtr_t;

typedef struct RtrRTUinfo_T RtrRTUinfo_t;

typedef struct RtrUpglist_T RtrUpglist_t;

typedef struct RtrUpg_T RtrUpg_t;

typedef struct RtrPtab_T RtrPtab_t;

typedef struct RtrParam_T RtrParam_t;

typedef struct RtrVlist_T RtrVlist_t;

typedef struct Rtr_Callbacks {

	void (*syntax_error)	(const char *msg, const char *loc, int lineno);
		/*
		 * error function, args are yacc error message, input
		 * buffer location of error, line number of buffer location
		 */

	void (*rtr_file)	(Rtr_t *rtreg, RtrRTUinfo_t *upglp,
		RtrPtab_t *paramtab);
		/*
		 * called at the end of input resource type registration
		 * properties and the paramtable
		 */

	Rtr_t *(*rtdec)	(RtrValueToken nametoken);
		/*
		 * The resource-type name declaration.
		 * This is the start of the resource-type property list
		 */

	Rtr_t *(*propsimple)	(Rtr_t *, RTRP_Code propcode);
		/* Add a simple property to the resource type registration */

	Rtr_t *(*propval)	(Rtr_t *, RTRP_Code propcode,
					RtrValueToken propvalue);
		/*
		 * Add a property with a value setting
		 * to the resource type registration
		 */

	Rtr_t *(*proppkgs)	(Rtr_t *, RtrVlist_t *propvalue);
		/*
		 * Add a package list property to the
		 * resource type registration
		 */

	RtrVlist_t *(*valuelist) (RtrVlist_t *listp, RtrValueToken element);
		/*
		 * Add to a list of string values.
		 * The first argument may be NULL, indicating addition of the
		 * first list element.
		 */

	RtrRTUinfo_t *(*upgheader) (RtrRTUinfo_t *rtupginfo,
		RTRV_Type header_found);
		/*
		 * Rt upgrade header -
		 * The start point of the upgrade_from/downgrade_to section.
		 * If Upgrade header is not found in RTR file, this rtr file
		 * is SC30 version and doen't have upgrade_from/downgrade_to
		 * section.
		 */

	RtrRTUinfo_t *(*upglist) (RtrRTUinfo_t *rtupginfo,
		RtrValueToken rt_version, RtrValueToken tunability);
		/*
		 * Add a upgrade_from entry to upgradelist in rtupginfo.
		 * The upgradelist may be NULL.
		 */

	RtrRTUinfo_t *(*downglist) (RtrRTUinfo_t *rtupginfo,
		RtrValueToken rt_version, RtrValueToken tunability);
		/*
		 * Add a downgrade_to to downgradelist in rtupginfo.
		 * The downgradelist may be NULL.
		 */

	RtrPtab_t *(*parameter) (RtrPtab_t *paramtable, RtrParam_t *entry);
		/*
		 * Add an entry to the paramtable.
		 * The paramtable may be NULL
		 */

	RtrParam_t *(*paramdec)	(RtrValueToken nametoken);
		/*
		 * The property-name attribute of a paramtable entry
		 * This is the start of the parameter's attribute list
		 */

	RtrParam_t *(*paramsimple)	(RtrParam_t *, RTRP_Code propcode);
		/* Add a simple attribute to a parameter */

	RtrParam_t *(*paramval)	(RtrParam_t *, RTRP_Code propcode,
					RtrValueToken propvalue);
		/* Add an attribute with a value setting to a parameter */

	RtrParam_t *(*paramenum)	(RtrParam_t *, RtrVlist_t *enumlist);
		/* Add an enum type attribute to a parameter */

	RtrParam_t *(*paramdefault)	(RtrParam_t *, RtrVlist_t *vlist);
		/*
		 * Add a default-value attribute to a parameter.
		 * Array-type parameters may have a default value of a list
		 */


} Rtr_Callbacks; /* end struct Rtr_Callbacks */


extern int	rtr_parse(void);
extern void	rtr_set_input(const char*);
extern void	rtr_set_callbacks(const Rtr_Callbacks*);
extern void	rtr_get_current_location(int *lineno, const char ** bufferloc);

#define	NOI18N(s)	s

#ifdef	__cplusplus
}
#endif

#endif /* _RTRCLIENT_H */
