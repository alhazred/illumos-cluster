/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)test_rtr.c	1.7	08/05/20 SMI"

/*
 *	Test program to load a Resource-Type registration file.
 *	and send it off to the registration-file parser
 *
 *	The program is built: CC test_rgr.cc rtrgram.o
 *
 *	rtrgram.o is from the parser component.
 *	An include path "-I" option needs to be added to the
 *	build if the parser component header "rtrclient.h" is
 *	not in the build directory.
 */


#include <stdio.h>
#include <malloc.h>
#include "rtrclient.h"
#include "rgm/rtreg.h"

/*
 * static decaration of Rtr_Callback object
 * for initial null values for function pointers
 */

static Rtr_Callbacks rtractions;

static void yyerror_callback(const char*msg, const char *bufloc, int lineno);

static void justparser(int argc, char **argv);
static void builddata(int artc, char **argv);


int main(int argc, char **argv) {

	justparser(argc, argv);
	builddata(argc, argv);

	return (0);

}

void
justparser(int argc, char **argv) {

	unsigned bufsize = 1024;
	char *buffer = (char *)malloc(bufsize);
	int argix = 1;

	if (buffer == NULL) {
		printf("Malloc Failed !!!\n");
		return;
	}

	rtractions.syntax_error = &yyerror_callback;
	rtr_set_callbacks(&rtractions);

	for (argix = 1; argix < argc; argix++) {

		int curloc = 0;
		int ic;
		int failed = 0;
		FILE *infile = fopen(argv[argix], "r");

		if (infile == NULL) {
			printf("Cannot open: %s!\n", argv[argix]);
			continue;
		}
		while ((ic = getc(infile)) != EOF) {
			buffer[curloc++] = ic;
			if (curloc >= bufsize) {
				bufsize  *= 2;
				buffer = (char*)realloc(buffer, bufsize);
				if (buffer == NULL) {
					printf("Realloc Failed !!!\n");
					return;
				}
			}
		}

		buffer[curloc] = 0;
		fclose(infile);
		infile = NULL;
		rtr_set_input(buffer);
		failed = rtr_parse();
		if (failed) {
			fprintf(stderr,
				"\nParser choked on input file: <%s>\n\n",
				argv[argix]);
		}
	}
	free(buffer);
}

void
yyerror_callback(const char*msg, const char *bufloc, int lineno) {
	fprintf(stderr,
		"Yacc error: %s\nFailure line %d starting at :``%.40s...''\n\n",
		msg, lineno,  bufloc);
}


void
builddata(int argc, char **argv) {

	RtrFileResult *result;

	int argix;
	for (argix = 1; argix < argc; argix++) {

		int status = rtreg_parse(argv[argix], &result);
		if (status == RTR_STATUS_PARSE_OK) {
			fprintf(stderr, "\nPARSE OK Input file: ``%s''\n",
						argv[argix]);
		} else if (status == RTR_STATUS_FILE_ERR) {
			fprintf(stderr, "\nError Reading Input file: ``%s''\n",
						argv[argix]);
		} else {
			char *error_type = "Unknown Error";

			switch (status) {
			case RTR_STATUS_PARSE_ERR:
				error_type = "Syntax Error";
				break;
			case RTR_STATUS_RTNAME_ERR:
				error_type = "Invalid Resource Type Name";
				break;
			case RTR_STATUS_INVALID_PROPVAL:
				error_type = "Invalid Property Value";
				break;
			case RTR_STATUS_INVALID_SYSDEF:
				error_type =
				    "Invalid Non-Extension Property";
				break;
			case RTR_STATUS_INVALID_ATTR:
				error_type =
				    "Invalid Attribute in Paramtable Entry";
				break;
			case RTR_STATUS_INVALID_ATTRVAL:
				error_type =
				    "Invalid Attribute Value "
				    "in Paramtable Entry";
				break;
			case RTR_STATUS_INTERNAL_ERR:
				error_type = "Internal Error";
				break;
			case RTR_STATUS_INVALID_PARAM:
				error_type = "Invalid Paramtable Entry";
				break;

			}
			fprintf(stderr,
				"\n%s in file: ``%s''\n",
						error_type, argv[argix]);
		}
		if (result && result->error_bufloc) {
			fprintf(stderr,
				"Error at line %d near:``%.40s...''\n\n",
				result->error_lineno,
				result->error_bufloc);
		}
		if (result) {
			delete_RtrFileResult(result);
		}
	}
}
