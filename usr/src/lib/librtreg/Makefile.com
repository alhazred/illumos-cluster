#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#"@(#)Makefile.com 1.23     08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# usr/src/lib/librtreg/Makefile.com
#

LIBRARY=	librtreg.a
VERS=		.1

OBJECTS= \
	rtrclient.o \
	rtrgram.o

# include library definitions
include $(SRC)/lib/Makefile.lib

YACCSRC = ../common/rtrgram.y
YACC-CSRC = $(YACCSRC:.y=.c)

SRCS= \
	../common/rtrclient.c \
	$(YACC-CSRC)

TESTSRC = ../common/test_rtr.c

# Don't check generated .c files rtrgram.c
CHECKHDRS = rtrclient.h
CHECK_FILES = rtrclient.c_check $(CHECKHDRS:%.h=%.h_check)

LINTFILES = rtrclient.ln

test:=	COPTFLAG = -g

obj/rtrclient.o:= CFLAGS += -w

CLEANFILES +=   rtrgram.c test.o
CLOBBERFILES +=   test

#
# -K PIC flags required so .so's can link with the .a
#
CFLAGS += -K PIC
CFLAGS64 += -K PIC
CPPFLAGS += -I../common -I$(SRC)/lib/libclcomm/common -I$(SRC)/head/scadmin/rpc
CPPFLAGS64 += -I../common -I$(SRC)/lib/libclcomm/common -I$(SRC)/head/scadmin/rpc

.KEEP_STATE:

all: $(LIBRARY)

test: $(LIBRARY)
	$(LINK.c) -o $@ $(TESTSRC) -L. -lrtreg

rtrgram.c: $(YACCSRC)
	$(YACC) $(YACCSRC)
	mv y.tab.c $@

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

include $(SRC)/lib/Makefile.targ
