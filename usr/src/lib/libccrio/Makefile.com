#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)Makefile.com	1.13	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libccrio/Makefile.com
#

LIBRARY = libccrio.a
VERS= .1

include $(SRC)/common/cl/Makefile.files

OBJECTS += $(CCR_UOBJS) $(MD5_UOBJS)

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib
CC_PICFLAGS = -KPIC

CLOBBERFILES +=	$(RPCFILES)

SRCS =		$(OBJECTS:%.o=../common/%.c)

#
# We're making a pic archive for libclconf
#
CPPFLAGS += $(CL_CPPFLAGS)

sparc_CCFLAGS += -xregs=no%appl
sparcv9_CCFLAGS += -xregs=no%appl

MTFLAG	= -mt

CCFLAGS += -noex -xildoff  +p +w $(CCERRWARN)
CCFLAGS += -I$(SRC)/common/cl -I$(SRC)/common/cl/interfaces/ixx
CCFLAGS += -I$(SRC)/common/cl/interfaces/$(CLASS) -I$(SRC)/common/cl/sys
CCFLAGS += $(CC_PICFLAGS) $($(NATIVE_MACH)_CCFLAGS)

CCFLAGS64 += -noex -xildoff  +p +w $(CCERRWARN)
CCFLAGS64 += -I$(SRC)/common/cl -I$(SRC)/common/cl/interfaces/ixx
CCFLAGS64 += -I$(SRC)/common/cl/interfaces/$(CLASS) -I$(SRC)/common/cl/sys
CCFLAGS64 += $(CC_PICFLAGS)

LIBS = $(LIBRARY)

CLEANFILES =
CHECKHDRS = $(CCR_HG)
PMAP =

OBJS_DIR = objs

.KEEP_STATE:

.NO_PARALLEL:

all: $(LIBS)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules
