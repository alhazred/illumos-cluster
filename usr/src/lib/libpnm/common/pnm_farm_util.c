/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnm_farm_util.c	1.4	08/05/20 SMI"

/*
 * pnm_farm_util.c
 *
 */
#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <scxcfg/scxcfg.h>
#include <dlscxcfg.h>
#include <pnm/pnm_head.h>
#include <sys/socket.h>
#include <netdb.h>

/*
 * scxcfg API.
 */
static void *instance;
static scxcfg_api_t api;

/*
 * Europa configuration handle.
 */
scxcfg Cfg;

/*
 * Create the Europa configuration cache in memory.
 * This is called once at LIBPNM initialization time. The cache is automatically
 * refreshed by the libscxcfg itselves. Each time we call the library to
 * get a property value, the cache is updated in case if configuration changes.
 */
int
farm_cfg_open()
{
	scxcfg_error error;

	/*
	 * First of all, load scxcfg dynamic library.
	 */
	if (scxcfg_load_module(&instance, &api) == -1) {
		log_err_lib("farm_cfg_open: scxcfg_load_module failed\n");
		return (-1);
	}

	if (api.scxcfg_open(&Cfg, &error) != FCFG_OK) {
		log_err_lib("farm_cfg_open: scxcfg_open failed");
		return (-1);
	}
	return (0);
}

/*
 * Close the farm configuration handle.
 */
void
farm_cfg_close()
{
	(void) api.scxcfg_close(&Cfg);
}


/*
 * Get address of a given node (Server or farm) on the private europa
 * network.
 * Don't forget that address must be freed by the caller.
 */
int
farm_cfg_getaddr(scxcfg_nodeid_t nodeid, struct in_addr *addr)
{
	char *ipaddr = NULL;
	scxcfg_error error;
	struct hostent *hp = NULL;
	char **p;
	int err, ret = 0;

	ipaddr = (char *)malloc(FCFG_MAX_LEN);
	if (ipaddr == NULL) {
		log_err_lib("farm_cfg_getaddr(%d) malloc failed", nodeid);
		ret = -1;
		goto finished;
	}

	if (api.scxcfg_get_ipaddress(&Cfg, nodeid, ipaddr, &error)
	    != FCFG_OK) {
		log_err_lib("farm_cfg_getaddr(%d): "
		    "scxcfg_get_ipaddress failed",
		    nodeid);
		ret = -1;
		goto finished;
	}

	hp = getipnodebyname(ipaddr, AF_INET, AI_ALL | AI_ADDRCONFIG, &err);

	if (hp == NULL) {
		log_err_lib("farm_cfg_getaddr(%d): "
		    "getipnodebyname failed");
		ret = -1;
		goto finished;
	}

	p = hp->h_addr_list;
	if (*p == 0) {
		log_err_lib("farm_cfg_getaddr(%d): "
		    "getipnodebyname failed", nodeid);
		ret = -1;
		goto finished;
	}

	(void) memcpy(&(addr->s_addr), *p, sizeof (addr->s_addr));

finished:
	if (hp != NULL)
		freehostent(hp);

	if (ipaddr != NULL)
		free(ipaddr);

	return (ret);
}

/*
 * Get nodeid for a given farm nodename.
 */
int
farm_cfg_getnodeid(char *nodename, scxcfg_nodeid_t *nodeidp)
{
	scxcfg_error error;

	/*
	 * A NULL nodename means the caller wants the local
	 * node ID.
	 */
	if (nodename == NULL) {
		if (api.scxcfg_get_local_nodeid(nodeidp, &error) != FCFG_OK) {
			return (-1);
		}
		return (0);
	}

	/*
	 * Then, try in Farm configuration.
	 */
	if (api.scxcfg_get_nodeid(&Cfg, nodename, nodeidp,
	    &error) != FCFG_OK) {
		return (-1);
	}
	return (0);
}
