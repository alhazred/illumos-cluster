/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnmstruct.c 1.14	08/05/20 SMI"

/*
 *     	Following code gets the IPMP state from the kernel using ioctls
 */

/*
 * Include SC header files
 */
#include <ipmp/ipmp_ld.h>
#include <struct.h>

static l_addr *get_logic_ip(char *, sa_family_t, struct lifconf *, int, int *);
static uint_t get_num_logic_ip(char *, sa_family_t, struct lifconf *);
static void delete_adp(bkg_status *group, bkg_adp *adp);
static void free_group(bkg_status *group);
static void free_adp(bkg_adp *adp);
static void append_adp(bkg_status *group, bkg_adp *adp);
static void append_group(bkg_status **list_h, bkg_status *group);
static bkg_status *add_group(bkg_status **list_h);
static bkg_adp *alloc_adp(void);
static void free_adp_inst(adp_inst *adpi);
static bkg_adp * find_adp(char *name, bkg_status *group);

void free_logic_ip(l_addr *list); /* free the logical IP's */
bkg_status *find_group(char *gname, bkg_status *list_h);
void free_grouplist(bkg_status *);


/*
 * Get all IP addresses hosted on all the adapter instances - both physical
 * and logical IP addresses in this group. If group name is NULL then get all
 * the logical IP addresses (not loopback) hosted on this node. Returns a list
 * of l_addrs. On error returns NULL. Get the IP addresses for the given family.
 * INPUT: pointer to group or NULL, family(v4/v6)
 * RETURN: pointer to a list of l_addrs structures, NULL on error.
 */
l_addr *
get_all_logic_ip(bkg_status *grp, sa_family_t af, struct lifconf *lifcp, int so,
    int *r)
{
	l_addr			*ptr, *last_ptr, *head_ptr;
	bkg_adp			*adp;

	ptr = last_ptr = head_ptr = NULL;

	/* If the group is NULL get all logical IPs on this node */
	if (!grp)
		return (get_logic_ip(NULL, af, lifcp, so, r));

	/* For all the adapters in this group */
	for (adp = grp->head; adp; adp = adp->next) {
		head_ptr = get_logic_ip(adp->adp, af, lifcp, so, r);
		if (ptr) {
			/* Goto the last laddr struct in this list */
			for (last_ptr = ptr; last_ptr->next; ) {
				last_ptr = last_ptr->next;
			}
			last_ptr->next = head_ptr;
		}
		else
			ptr = head_ptr;
	}
	return (ptr);
}

/*
 * Get all IP addresses hosted on this adapter instance - both physical and
 * logical IP addresses. If the adapter instance is NULL then get all the
 * logical IP addresses (not loopback) on this node. Input is physical adp
 * name i.e. hme0. Returns a list of l_addrs. On error returns NULL. Get the IP
 * addresses for the given family.
 * INPUT: pointer to adp name or NULL, family(v4/v6)
 * RETURN: pointer to a list of l_addrs structures, NULL on error.
 */
l_addr *
get_logic_ip(char *adp, sa_family_t af, struct lifconf *lifcp, int so, int *r)
{
	uint_t			n, phys_name_len = 0;
	char			*name;
	struct lifreq		*lifr;
	l_addr			*ptr, *last_ptr, *head_ptr;
	struct in6_addr		addr;
	struct sockaddr_in6	*sin6;
	struct sockaddr_in	*sin;
	struct lifreq		lifrflag;

	ptr = last_ptr = head_ptr = NULL;

	/* For each network i/f on machine ... */
	lifr = (struct lifreq *)lifcp->lifc_req;
	for (n = 0; n < (uint_t)lifcp->lifc_len / sizeof (struct lifreq); n++,
	    lifr++) {
		name = lifr->lifr_name;

		/* Only for the given family */
		if (af != lifr->lifr_addr.ss_family)
			continue;

		/*
		 * Get the length of the physical adapter
		 * eg for hme0 it will be 3
		 */
		phys_name_len = strcspn(name, ":");
		assert(phys_name_len != 0);

		/*
		 * If adp is NULL then we will get all the logical IPs.
		 * If the name of the i/f is not the same as the name of
		 * adp i.e. hme0 = hme0. So we are only concerned with i/f
		 * which have the same name as adp.
		 */
		if ((adp) &&
		    (phys_name_len != strlen(adp) ||
		    strncmp(adp, name, phys_name_len) != 0))
			continue;

		/* Also if the name of the adp is lo0:x then we do not care. */
		if (strncmp(name, "lo0", phys_name_len) == 0)
			continue;

		/*
		 * Retrieve the flags set on this IP address
		 * (physical or logical).
		 */
		(void) memset(&lifrflag, 0, sizeof (lifrflag));
		(void) strncpy(lifrflag.lifr_name, name,
		    sizeof (lifrflag.lifr_name));

		/*
		 * On ENXIO return 1 so that we are called again with a new
		 * list of interfaces.
		 */
		if (ioctl(so, SIOCGLIFFLAGS, (char *)&lifrflag) < 0) {
			if (errno == ENXIO) {	/*lint !e746 */
				*r = 1;
				return (NULL);
			}
			log_syserr_lib("SIOCGLIFFLAGS failed.");
			goto error_gli_exit;
		}

		ptr = (l_addr *) malloc(sizeof (l_addr));
		if (ptr == NULL) {
			log_syserr_lib("out of memory.");
			goto error_gli_exit;
		}
		bzero(ptr, sizeof (l_addr));

		/* Append l_addr structure to list */
		if (head_ptr == NULL)
			head_ptr = ptr;
		if (last_ptr != NULL)
			last_ptr->next = ptr;
		last_ptr = ptr;

		if (af == AF_INET) {	/* v4 */
			sin = (struct sockaddr_in *)&lifr->lifr_addr;
			IN6_INADDR_TO_V4MAPPED(&sin->sin_addr, &(ptr->ipaddr));
		} else {	/* v6 */
			sin6 = (struct sockaddr_in6 *)&lifr->lifr_addr;
			addr = sin6->sin6_addr;	/* structure copy */
			bcopy(&addr, &(ptr->ipaddr), CL_IPV6_ADDR_LEN);
		}

		/* Save the flags of the logical IP address */
		ptr->flags = lifrflag.lifr_flags;

		/* Store the name of the adp also */
		ptr->name = strdup(name);
		if (ptr->name == NULL) {
			log_syserr_lib("out of memory.");
			goto error_gli_exit;
		}

		ptr->inst = get_logical_inst(name);
	}

	*r = 0;
	return (head_ptr);

error_gli_exit:
	/* Error cleanup: free logic_ip list and return NULL */
	free_logic_ip(head_ptr);

	*r = -1;
	return (NULL);
}

/*
 * Free the l_addr structures list.
 * INPUT: pointer to l_addr list
 * RETURN: void
 */
void
free_logic_ip(l_addr *list)
{
	l_addr	*lp = NULL, *lp_next = NULL;

	for (lp = list; lp; lp = lp_next) {
		lp_next = lp->next;
		free(lp->name);
		free(lp);
	}
}

/*
 * To initialize the data structures. Takes a snapshot of the kernel adapters
 * and form a grouplist. Update the listhead to point to the grouplist head.
 * INPUT: pointer to pointer to bkg_status struct(pointer to the grouplist),
 *	  v4 socket id, and v6 socket id and a pointer to a pointer to the
 *        lifconf buffer.
 * RETURN: 0 on success or -1 on error. Fills in the pointer to the lifconf
 *         buffer.
 */
int
init_struct(bkg_status **list_h, int so_v4, int so_v6, struct lifconf **lifcp)
{
	uint_t			n;
	uint_t			lif_cnt; /* no. of ifs */
	uint_t			inst;	/* ordinal value of IP */
	char			*name; /* name of adp i.e. hme0 */
	char 			*buf = NULL;
	struct lifconf		*lifc = NULL;
	struct lifreq		lifr, *lifrp = NULL, lifrflg;
	bkg_status		*bgp, *exist_group = NULL;
	sa_family_t		af; /* family, v4/v6 */
	bkg_adp			*ptr, *match_adp;
	int			try = 0, so;
	struct lifnum		lifn;

	/*
	 * Here we should remember that we need two sockets for doing ioctls
	 * v4 and v6. First we do a SIOCGLIFCONF with AF_UNSPEC on a v4 socket
	 * and then for every instance (v4/v6) we must do the ioctls on the
	 * particular socket..either v4 or v6.
	 */
	/* Get kernel state into nbkg */
retry:
	/*
	 * We use SIOCGLIFNUM here. Since new interfaces may be
	 * configured after SIOCGLIFNUM, and we would get an error from
	 * SIOCGLIFCONF (EINVAL) we will try this twice. Initially we will
	 * allocate some extra buffer space.
	 */
	try++;
	lifn.lifn_family = AF_UNSPEC;
	lifn.lifn_flags = 0;

	/*
	 * Use LIFC_ALLZONES flag in the ioctl to retrieve the
	 * adapter instances that are plumbed for zones. This flag is
	 * available from Solaris 10 release.
	 */

#ifdef LIFC_ALLZONES
	lifn.lifn_flags = LIFC_ALLZONES;
#endif
	if (ioctl(so_v4, SIOCGLIFNUM, (char *)&lifn) < 0) {
		log_syserr_lib("SIOCGLIFNUM failed.");
		goto init_struct_error;
	}
	lif_cnt = (uint_t)(lifn.lifn_count + 1);

	/*
	 * Retrieve i/f configuration of machine i.e. all the networks
	 * it is connected to. Free buf if required.
	 */
	if (buf != NULL)
		free(buf);
	buf = (char *)malloc(lif_cnt * sizeof (struct lifreq));
	if (buf == NULL) {
		log_syserr_lib("out of memory.");
		goto init_struct_error;
	}

	lifc = (struct lifconf *)malloc(sizeof (struct lifconf));
	if (lifc == NULL) {
		log_syserr_lib("out of memory.");
		goto init_struct_error;
	}
	(void) memset(lifc, 0, sizeof (struct lifconf));
	lifc->lifc_family = AF_UNSPEC; /* to get v4 and v6 instances */
	lifc->lifc_len = (int)(lif_cnt * sizeof (struct lifreq));
	lifc->lifc_buf = buf;

	/*
	 * Use LIFC_ALLZONES flag in the ioctl to retrieve the
	 * adapter instances that are plumbed for zones. This flag is
	 * available from Solaris 10 release.
	 */

#ifdef LIFC_ALLZONES
	lifc->lifc_flags = LIFC_ALLZONES;
#endif

	if (ioctl(so_v4, SIOCGLIFCONF, (char *)lifc) < 0) {
		/*
		 * EINVAL is commonly encountered, when things change
		 * underneath us rapidly, (eg. at boot, when new interfaces
		 * are plumbed successively) and the kernel finds the buffer
		 * size we passed as too small. We will retry twice.
		 */
		if (errno == EINVAL && try < 2)	/*lint !e746 */
			goto retry;
		log_syserr_lib("SIOCGLIFCONF failed.");
		goto init_struct_error;
	}

	/* For each i/f on machine */
	lifrp = (struct lifreq *)lifc->lifc_req;
	for (n = 0; n < (uint_t)lifc->lifc_len / sizeof (struct lifreq);
	    n++, lifrp++) {
		af = lifrp->lifr_addr.ss_family;
		so = (af == AF_INET) ? so_v4 : so_v6;
		name = lifrp->lifr_name;

		/*
		 * Returns 0 for physical adp, otherwise returns the
		 * ordinal value of the logical IP. eg. for
		 * tr2, it will return 0, whilst for tr2:3, it will
		 * return 3. get_logical_inst is defined in str_opt.c.
		 */
		inst = get_logical_inst(name);

		/* We only want to check the physical adapters */
		if (inst != 0)
			continue;

		/* If the adapter is clprivnet or lo0 don't do anything */
		if (strncmp(name, "clprivnet", strlen("clprivnet")) == 0 ||
		    strncmp(name, "lo0", strlen("lo0")) == 0)
			continue;

		bzero(&lifr, sizeof (lifr));
		bcopy(name, lifr.lifr_name, strlen(name) + 1);

		/* Get group name of adapter instance */
		(void) memset(lifr.lifr_groupname, 0,
		    sizeof (lifr.lifr_groupname));
		if (ioctl(so, SIOCGLIFGROUPNAME, (caddr_t)&lifr) < 0) {
			if (errno == ENXIO)	/* interface gone */
				continue;	/* ignore */
			log_syserr_lib("SIOCGLIFGROUPNAME failed.");
			goto init_struct_error;
		}

		/* If the adp is not in a group then we don't do anything */
		if (strlen(lifr.lifr_groupname) == 0)
			continue;

		/* Get flags also - state of adp */
		bzero(&lifrflg, sizeof (lifr));
		bcopy(name, lifrflg.lifr_name, strlen(name) + 1);
		if (ioctl(so, SIOCGLIFFLAGS, (char *)&lifrflg) < 0) {
			if (errno == ENXIO)	/* interface gone */
				continue;	/* ignore */
			log_syserr_lib("SIOCGLIFFLAGS failed.");
			goto init_struct_error;
		}

		/*
		 * See if this adp is already there in the present grouplist.
		 * match_adp will be the adapter itself.
		 */
		match_adp = NULL;
		for (bgp = *list_h; bgp; bgp = bgp->next) {
			match_adp = find_adp(name, bgp);
			if (match_adp)
				break;
		}

		/*
		 * Now find the group in the grouplist which matches this
		 * adp's group.
		 */
		exist_group = find_group(lifr.lifr_groupname, *list_h);

		/* Allocate adp structure if needed */
		if (!match_adp) {
			if ((ptr = alloc_adp()) == NULL)
				goto init_struct_error;
			ptr->adp = strdup(name);
			if (ptr->adp == NULL) {
				log_syserr_lib("out of memory.");
				goto init_struct_error;
			}
		} else
			ptr = match_adp;

		/*
		 * If the adp's current group does not exist then
		 * we have to allocate a completely new group - bgp.
		 */
		if (!exist_group) {
			/* Create a new group */
			if ((bgp = add_group(list_h)) == NULL)
				goto init_struct_error;
			bgp->name = strdup(lifr.lifr_groupname);
			if (bgp->name == NULL) {
				log_syserr_lib("out of memory.");
				goto init_struct_error;
			}
			append_adp(bgp, ptr);
		}

		/*
		 * If the adapter was not found and the group exists then
		 * we need to append it to the group.
		 */
		if (!match_adp && exist_group)
			append_adp(exist_group, ptr);

		/* Now get the data from the kernel for this adp instance */
		switch (af) {
		case AF_INET:
			if (!ptr->pii_v4) {
				ptr->pii_v4 = (adp_inst *) malloc(
				    sizeof (adp_inst));
				if (ptr->pii_v4 == NULL) {
					log_syserr_lib("out of memory.");
					goto init_struct_error;
				}
			}
			/* Get nLpi */
			ptr->pii_v4->nLpi = get_num_logic_ip(name, af, lifc);
			break;

		case AF_INET6:
			if (!ptr->pii_v6) {
				ptr->pii_v6 = (adp_inst *) malloc(
				    sizeof (adp_inst));
				if (ptr->pii_v6 == NULL) {
					log_syserr_lib("out of memory.");
					goto init_struct_error;
				}
			}
			/* Get nLpi */
			ptr->pii_v6->nLpi = get_num_logic_ip(name, af, lifc);
			break;

		default:
			/* We could do an assert here */
			log_syserr_lib("wrong address family.");
			goto init_struct_error;
		}

		/*
		 * If the IFF_RUNNING flag is off then the adp has no carrier
		 * and has failed - so we mark it NOTRUN.
		 * If the IFF_FAILED flag is on then the adp is faulty.
		 * If the IFF_STANDBY flag is on then the kernel sets the
		 * read only flag IFF_INACTIVE on - and an adp is considered
		 * to be standby only iff both the flags are on. If the adp
		 * is standby and the IFF_INACTIVE flag is off then the
		 * adp is actually ok since it can be used for traffic.
		 * The IFF_INACTIVE flag is turned off when the other active
		 * adapters in the IPMP group have failed and this standby
		 * adp is used for traffic. Previously, The INACTIVE flag
		 * was never used without the STANDBY flag also on.
		 * With the fix of 4796820/5084073 INACTIVE flag can be on
		 * alone when initially active adapter in an active/standby
		 * IPMP group gets repaired after failure with no failback
		 * option in /etc/default/mpathd file.
		 * If the IFF_OFFLINE flag is on then the adp is offline.
		 */
		if ((lifrflg.lifr_flags & IFF_RUNNING) == 0)
			ptr->state = ADP_NOTRUN;
		else if (lifrflg.lifr_flags & IFF_FAILED)
			ptr->state = ADP_FAULTY;
		else if (lifrflg.lifr_flags & IFF_INACTIVE)
			ptr->state = ADP_STANDBY;
		else if (lifrflg.lifr_flags & (uint64_t)IFF_OFFLINE)
			ptr->state = ADP_OFFLINE;
		else
			ptr->state = ADP_OK;
	}

	*lifcp = lifc;
	return (0);

init_struct_error:
	free_grouplist(*list_h);
	*lifcp = lifc;
	return (-1);
}

/*
 * Find the group corresponding to the group->name eg: ipmp1.
 * INPUT: char pointer to group name, and pointer to grouplist.
 * RETURN: pointer to struct bkg_status, NULL if not found.
 */
struct bkg_status *
find_group(char *gname, bkg_status *list_h)
{
	bkg_status	*bkg;

	for (bkg = list_h; bkg != NULL; bkg = bkg->next) {
		if (strcmp(bkg->name, gname) == 0)
			return (bkg);
	}
	return (NULL);
}

/*
 * Find the adp corresponding to the adp->adp eg: hme0.
 * INPUT: char pointer to adp name, and pointer to group.
 * RETURN: pointer to struct bkg_adp, NULL if not found.
 */
bkg_adp *
find_adp(char *name, bkg_status *bkg)
{
	bkg_adp *adp;

	for (adp = bkg->head; adp != NULL; adp = adp->next) {
		if (strcmp(adp->adp, name) == 0)
			return (adp);
	}
	return (NULL);
}

/*
 * Free this adp and remove it from the given group's adplist.
 * If this is the last adp in the group then make group->head = NULL;
 * INPUT: pointer to a bkg_adp struct (adp which needs to be removed )
 *      : pointer to a group structure
 * RETURN: void
 */
static void
delete_adp(bkg_status *group, bkg_adp *adp)
{

	bkg_adp	*prev;
	bkg_adp *current;

	/* Remove this adp from group->head adplist */
	if (group->head == adp) {
		if (group->head->next)
			group->head = group->head->next;
		else
			group->head = NULL;
	} else {
		for (prev = group->head, current = prev->next;
		    current != NULL;
		    prev = current, current = current->next) {
			if (current == adp) {
				prev->next = current->next;
				break;
			}
		}
	}
	/* Free the memory associated with this adp structure */
	free_adp(adp);
}

/*
 * Free this group structure and the pointer also.
 * INPUT: pointer to a bkg_status struct (bkg which needs to be removed )
 * RETURN: void
 */
static void
free_group(bkg_status *group)
{
	bkg_adp *adp;
	bkg_adp *tmp;

	/* Remove the adp list in group */
	for (adp = group->head; adp != NULL; adp = tmp) {
		tmp = adp->next;
		delete_adp(group, adp);
	}
	free(group->name);
	free(group);
}

/*
 * Free this adp structure and the pointer also.
 * INPUT: pointer to a bkg_adp struct (adp which needs to be removed )
 * RETURN: void
 */
static void
free_adp(bkg_adp *adp)
{
	/* Free the memory associated with this adp structure */
	free(adp->adp);
	free_adp_inst(adp->pii_v4);
	free_adp_inst(adp->pii_v6);
	free(adp);
}

/*
 * Free this adp_inst structure.
 * INPUT: pointer to a adp_inst struct (adp_inst which needs to be removed )
 * RETURN: void
 */
static void
free_adp_inst(adp_inst *adpi)
{
	if (adpi)
		free(adpi);
}

/*
 * Free the given grouplist.
 * INPUT : pointer to pointer to bkg_status struct(grouplist)
 * RETURN :void
 */
void
free_grouplist(bkg_status *listh)
{
	bkg_status *bkg, *bkg_next;

	/* Free the group list */
	for (bkg = listh; bkg; bkg = bkg_next) {
		bkg_next = bkg->next;
		free_group(bkg);
	}
}

/*
 * Append the given adp to the given group.
 * INPUT: pointer to bkg_adp struct, pointer to bkg_status struct
 * RETURN: void
 */
static void
append_adp(bkg_status *group, bkg_adp *adp)
{
	if (group->head != NULL)
		adp->next = group->head;
	group->head = adp;
}

/*
 * Allocate memory for adp and zero it.
 * INPUT: NULL
 * RETURN: pointer to bkg_adp or NULL on error.
 */
static bkg_adp *
alloc_adp()
{
	bkg_adp	*ptr;

	ptr = (bkg_adp *) malloc(sizeof (bkg_adp));
	if (ptr == NULL) {
		log_syserr_lib("out of memory.");
		return (NULL);
	}
	bzero(ptr, sizeof (bkg_adp));
	return (ptr);
}

/*
 * Allocate a group struct and add it to the grouplist obkg.
 * INPUT:  pointer to pointer to bkg_status struct(pointer to grouplist in
 *         which to add the given group, i.e. pointer to nbkg/obkg),
 * RETURN: pointer to group added or NULL on error.
 */
static bkg_status *
add_group(bkg_status **list_h)
{
	bkg_status	*ptr;

	ptr = (bkg_status *) malloc(sizeof (bkg_status));
	if (ptr == NULL) {
		log_syserr_lib("out of memory.");
		return (NULL);
	}
	bzero((char *)ptr, sizeof (bkg_status));
	append_group(list_h, ptr);
	return (ptr);
}

/*
 * Append the given group struct to the grouplist obkg.
 * INPUT: pointer to pointer to bkg_status struct(pointer to grouplist in
 *        which to append)
 *        pointer to bkg_status struct (the group to append),
 * RETURN: void
 */
static void
append_group(bkg_status **list_h, bkg_status *group)
{
	if (*list_h != NULL)
		group->next = *list_h;
	*list_h = group;
}

/*
 * Return the number of Logical IP addresses (either v4/v6) of the adp.
 * INPUT: pointer to the adp and family.
 * RETURN: number of Lpi.
 */
uint_t
num_logic_ip(bkg_adp *adp, sa_family_t af)
{
	uint_t min = 0;

	if (af == AF_INET && adp->pii_v4)
		min = adp->pii_v4->nLpi;

	if (af == AF_INET6 && adp->pii_v6)
		min = adp->pii_v6->nLpi;

	return (min);
}

/*
 * Get the number of logical ip addresses hosted on this adapter instance
 * Returns an int. If there are no logical addresses hosted on the adp then
 * it returns zero as expected. Input is physical adp name. i.e. hme0.
 * INPUT: pointer to adp name, family(v4/v6), pointer to lifconf
 * RETURN: number of ip addresses hosted on this adp.
 */
uint_t
get_num_logic_ip(char *adp, sa_family_t af, struct lifconf *lifc)
{
	uint_t			count = 0, n, phys_name_len;
	char			*name;
	struct lifreq		*lifr;

	/*
	 * For each network i/f on machine ...
	 */
	lifr = (struct lifreq *)lifc->lifc_req;
	for (n = 0; n < (uint_t)lifc->lifc_len / sizeof (struct lifreq);
	    n++, lifr++) {
		if (af != lifr->lifr_addr.ss_family)
			continue;

		name = lifr->lifr_name;
		/*
		 * If the name of the i/f is not the same as the name of
		 * adp i.e. hme0 = hme0. So we are only concerned with i/f
		 * which have the same name as adp.
		 */
		if ((phys_name_len = strcspn(name, ":")) != strlen(adp) ||
		    strncmp(adp, name, phys_name_len) != 0)
			continue;
		count++;
	}
	return (count);
}

/*
 * Return the status of the group.
 * INPUT: pointer to the group.
 * RETURN: status of the group.
 */
int
group_status(bkg_status *group)
{
	int status = PNM_STAT_DOWN; /* Assume group is down */
	bkg_adp *adp;

	for (adp = group->head; adp != NULL; adp = adp->next) {
		if (adp->state == ADP_OK) {
			status = PNM_STAT_OK;
			break;
		}
	}
	return (status);
}
