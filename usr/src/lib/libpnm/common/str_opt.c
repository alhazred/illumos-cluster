/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)str_opt.c	1.5	08/05/20 SMI"

/*
 * The following code implements some string operations which are required and
 * used by the PNM daemon.
 */

#include <ipmp/ipmp_head.h>

/*
 * Returns the next input line from a file stream every time it is called. I
 * It looks for a non blank character and returns a line uptil the newline
 * character and returns a line uptil the newline character \n. Hence, all the
 * initial white spaces are removed.
 * INPUT: file pointer, char pointer to a line.
 * RETURN: -1 on error and 1 on success.(line has the newline from the file
 */
int
get_line(fp, line)
	FILE	*fp;
	char	*line;
{
	int	i = 0;
	int	c;

	while ((c = getc(fp)) == ' ' || c == '\t' || c == '\n')
		;
	if (c == EOF)
		return (-1);
	line[i++] = (char)c;
	while ((c = getc(fp)) != EOF && c != '\n') {
		line[i++] = (char)c;
	}
	line[i] = '\0';
	if (c == EOF)
		(void) putc(c, fp);
	return (1);
}

/*
 * Returns the names of the adp from the array in_put one after the other.
 * The name is returned in name. Basically there is a buffer containing a
 * number of adp names separated by blanks and this routine returns those adp
 * names one after the other. It is used in pnmd - ipmpd.c.
 * INPUT: char pointer to a buffer, char pointer to a adp name, flag.
 * RETURN: 0 for error and 1 for success (the adp name is returned in name)
 */
static int ptr = 0;
int
get_name(in_put, name, reset)
	char	*in_put;
	char	*name;
	int reset;
{
	int	i = ptr, j = 0;
	int	c;

	if (in_put == NULL) {
		ptr = 0;
		return (0);
	}
	/*
	 * When group create is called we set the value of reset to 1 so that
	 * get_name starts parsing the adp_buf from the beginning instead of the
	 * middle.
	 * We need to reset the value
	 * in ptr so that get_name starts parsing the line from the beginning
	 * instead of some arbitrary place in the middle.
	 */
	if (reset)
		i = ptr = 0;

	while ((c = in_put[i++]) == ' ' || c == '\n' || c == '\t')
		;
	if (c == '\0') {
		ptr = 0;
		return (0);
	}
	name[j++] = (char)c;
	while ((c = in_put[i++]) != ' ' && c != '\t' && c != '\0') {
		name[j++] = (char)c;
		if (j == NAMELEN - 1) {
			/*
			 * Found a name that's too long to be an interface
			 * name or backup group name. Stop here.
			 */
			break;
		}
	}
	name[j] = '\0';
	ptr = --i;
	return (1);
}

/*
 * Return the logical instance of the adapter eg: hme0:2 - > 2.
 * INPUT: pointer to the adapter name
 * RETURN: the logical instance of the adapter.
 */
uint_t
get_logical_inst(name)
	char	*name;
{
	int	i = 0, j = 0;
	char	c;
	char	nu[4];

	while ((c = name[i++]) != ':' && c != '\0')
		;
	if (c == '\0')
		return (0);

	while ((c = name[i++]) != '\0')
		nu[j++] = c;
	nu[j] = '\0';

	return ((uint_t)atoi(nu));
}
