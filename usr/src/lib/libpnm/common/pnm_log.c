/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnm_log.c	1.4	08/05/20 SMI"

/*
 * pnm_log.c - Logging module.
 */

#include <pnm/pnm_head.h>

/* Message handles for the sc_syslog facility */
sc_syslog_msg_handle_t	msg_libpnm;

/*
 * This is used for all the system errors. The error will be written to the
 * syslog as an error message.
 * errno will be intrepreted in the error message. This will generate a single
 * message id and so we have to write only one message explanation. However,
 * this is not such a big problem because a system error will occur for the
 * following types of errors:
 * out of memory, can't open a file descriptor, gethostbyname failed etc.
 * These types of errors can be classified into one type and have the same type
 * of user action. The user action can be specified to be different also in the
 * explanation.
 */
void
log_syserr_lib(char *fmt, ...)
{
	static char	buf[LINELEN];
	va_list		ap; /* CSTYLED */
	int		err = errno;	/*lint !e746 */

	/* CSTYLED */
	va_start(ap, fmt); /*lint !e40 !e26 !e50 !e10 */
	(void) vsnprintf(buf, LINELEN, fmt, ap);
	va_end(ap);

	if (err != 0) {
		(void) strcat(buf, ": ");
		(void) strcat(buf, (char *)strerror(err));
	}
	/*
	 * Initialize sc_syslog handle. This calls will invoke
	 * openlog(), and we shouldn't be doing openlog() ourselves.
	 */
	(void) sc_syslog_msg_initialize(&msg_libpnm, SC_SYSLOG_PNM_TAG, "");
	/* This uses the SC way of reporting syslog messages */
	/*
	 * SCMSGS
	 * @explanation
	 * A system error has occured in libpnm. This could be because of the
	 * resources on the system being very low. eg: low memory.
	 * @user_action
	 * The user of libpnm should handle these errors. However, if the
	 * message is
	 *
	 * out of memory - increase the swap space, install more memory or
	 * reduce peak memory consumption. Otherwise the error is
	 * unrecovarable, and the node needs to be rebooted.
	 *
	 * write error - check the "write" man page for possible errors.
	 *
	 * read error - check the "read" man page for possible errors.
	 *
	 * socket failed - check the "socket" man page for possible errors.
	 *
	 * TCP_ANONPRIVBIND failed - check the "setsockopt" man page for
	 * possible errors.
	 *
	 * gethostbyname failed for %s - make sure entries in /etc/hosts,
	 * /etc/nsswitch.conf and /etc/netconfig are correct to get
	 * information about this host.
	 *
	 * bind failed - check the "bind" man page for possible errors.
	 *
	 * SIOCGLIFFLAGS failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCSLIFFLAGS failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCSLIFADDR failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * open failed - check the "open" man page for possible errors.
	 *
	 * SIOCLIFADDIF failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCLIFREMOVEIF failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCSLIFNETMASK failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCGLIFNUM failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCGLIFCONF failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * SIOCGLIFGROUPNAME failed - check the "ioctl" man page for possible
	 * errors.
	 *
	 * wrong address family - check the "ioctl" man page for possible
	 * errors.
	 *
	 * Cannot open libscconf - check that /usr/cluster/lib/libscconf.so.1
	 * is properly installed.
	 *
	 * Cannot access libscconf function - check that
	 * /usr/cluster/lib/libscconf.so.1 is properly installed.
	 *
	 * Cannot access cluster configuration - check that you are in cluster
	 * mode.
	 *
	 * Failed to retrieve nodeid for %s. - Make sure that the name given
	 * is a valid node name.
	 */
	(void) sc_syslog_msg_log(msg_libpnm, SC_SYSLOG_ERROR, MESSAGE,
	    "libpnm system error: %s", buf);
	sc_syslog_msg_done(&msg_libpnm);
}

/*
 * This is used for all other errors. The error will be written to the
 * syslog as an error message. This will generate a single
 * message id and so we have to write only one message explanation. However,
 * this is not such a big problem because an error will occur for the
 * following types of errors:
 * network too slow, can't connect to the PNMd, group not found etc.
 * These types of errors can be classified into one type and have the same type
 * of user action. The user action can be specified to be different also in the
 * explanation.
 */
void
log_err_lib(char *fmt, ...)
{
	static char	buf[LINELEN];
	va_list		ap;

	/* CSTYLED */
	va_start(ap, fmt); /*lint !e40 !e26 !e50 !e10 */
	(void) vsnprintf(buf, LINELEN, fmt, ap);
	va_end(ap);

	/*
	 * Initialize sc_syslog handle. This calls will invoke
	 * openlog(), and we shouldn't be doing openlog() ourselves.
	 */
	(void) sc_syslog_msg_initialize(&msg_libpnm, SC_SYSLOG_PNM_TAG, "");
	/* This uses the SC way of reporting syslog messages */
	/*
	 * SCMSGS
	 * @explanation
	 * This means that there is an error either in libpnm being able to
	 * send the command to the PNM daemon or in libpnm receiving a
	 * response from the PNM daemon.
	 * @user_action
	 * The user of libpnm should handle these errors. However, if the
	 * message is:
	 *
	 * network is too slow - it means that libpnm was not able to
	 *
	 * read data from the network - either the network is congested or the
	 * resources on the node are dangerously low.
	 *
	 * scha_cluster_open failed - it means that the call to initialize a
	 * handle to get cluster information failed. This means that the
	 * command will not be sent to the PNM daemon.
	 *
	 * scha_cluster_get failed - it means that the call to get cluster
	 * information failed. This means that the command will not be sent to
	 * the PNM daemon.
	 *
	 * can't connect to PNMd on %s - it means that libpnm was not able to
	 * connect to the PNM daemon through the private interconnect on the
	 * given host. It could be that the given host is down or there could
	 * be other related error messages.
	 *
	 * wrong version of PNMd - it means that we connected to a PNM daemon
	 * which did not give us the correct version number.
	 *
	 * no LOGICAL PERNODE IP for %s - it means that the private
	 * interconnect LOGICAL PERNODE IP address was not found.
	 *
	 * IPMP group %s not found - either an IPMP group name has been
	 * changed or all the adapters in the IPMP group have been unplumbed.
	 * There would have been an earlier NOTICE which said that a
	 * particular IPMP group has been removed. The cl_pnmd has to be
	 * restarted. Send a KILL (9) signal to the PNM daemon. Because cl_pnmd
	 * is under PMF control, it will be restarted automatically. If the
	 * problem persists, restart the node with clnode evacuate and
	 * shutdown.
	 *
	 * no adapters in IPMP group %s - this means that the given IPMP group
	 * does not have any adapters. Please look at the error messages from
	 * LogicalHostname/SharedAddress.
	 *
	 * no public adapters on this node - this means that this node does
	 * not have any public adapters. Please look at the error messages
	 * from LogicalHostname/SharedAddress.
	 */
	(void) sc_syslog_msg_log(msg_libpnm, SC_SYSLOG_ERROR, MESSAGE,
	    "libpnm error: %s", buf);
	sc_syslog_msg_done(&msg_libpnm);
}
