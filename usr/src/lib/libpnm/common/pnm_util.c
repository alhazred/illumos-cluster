/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnm_util.c	1.3	08/05/20 SMI"

/*
 * pnm_util.c - Workhorse routines for the pnm library.
 */

#include <pnm/pnm_ld.h>
#include <libintl.h>
#include "pnm_network.h"

/*
 * Return the logical instance of the adapter eg: hme0:2 - > 2.
 * INPUT: pointer to the adapter name
 * RETURN: the logical instance of the adapter.
 */
uint_t
get_logical_inst(char *name)
{
	int	i = 0, j = 0;
	char	c;
	char	nu[4];

	while ((c = name[i++]) != ':' && c != '\0')
		;
	if (c == '\0')
		return (0);

	while ((c = name[i++]) != '\0')
		nu[j++] = c;
	nu[j] = '\0';

	return ((uint_t)atoi(nu));
}

/*
 * This is going to be a debugging CLI which will print out information about
 * the status of the adapters as seen by the pnm daemon.
 */
void
show_group_state()
{
	pnm_group_list_t glist = NULL;
	pnm_status_t status;
	pnm_callback_t *cb_bufp = NULL;
	int scerr;
	scha_cluster_t hdl;
	scha_str_array_t *all_nodenames;
	uint_t ix;
	scha_node_state_t node_state;
	char *gr_str, *next_gr_str;
	uint_t i;
	uint_t cb_cnt;
	uint_t mac_addrs_uniq;

	scerr = scha_cluster_open(&hdl);
	if (scerr != SCHA_ERR_NOERR)
		return;

	/* Get the names of all the nodes in this cluster */
	scerr = scha_cluster_get(hdl, SCHA_ALL_NODENAMES, &all_nodenames);
	if (scerr != SCHA_ERR_NOERR) {
		(void) scha_cluster_close(hdl);
		return;
	}

	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* For all nodes that are up.... */
	for (ix = 0; ix < all_nodenames->array_cnt; ix++) {
		scerr = scha_cluster_get(hdl, SCHA_NODESTATE_NODE,
		    all_nodenames->str_array[ix], &node_state);
		if (scerr != SCHA_ERR_NOERR) {
			(void) scha_cluster_close(hdl);
			return;
		}
		if (node_state == SCHA_NODE_DOWN) {
			continue;
		}
		(void) pnm_init(all_nodenames->str_array[ix]);
		(void) printf(dgettext(TEXT_DOMAIN, "Node\t%s\t"),
					all_nodenames->str_array[ix]);

		/* Get the MAC address state - unique/not unique */
		(void) pnm_mac_address(&mac_addrs_uniq);

		(void) printf(dgettext(TEXT_DOMAIN, "MAC addresses "));
		mac_addrs_uniq? (void) printf(dgettext(TEXT_DOMAIN,
							"unique\n")):
		    (void) printf(dgettext(TEXT_DOMAIN, "not unique\n"));

		/* Get all the groups */
		(void) pnm_group_list(&glist);
		next_gr_str = glist;
		while ((gr_str = strtok_r(next_gr_str, ":", &next_gr_str))
		    != NULL) {

			/* Get this group's status */
			(void) pnm_group_status(gr_str, &status);

			/* Get the callbacks registered with this group */
			(void) pnm_callback_list(gr_str, &cb_cnt, &cb_bufp);
			(void) printf(dgettext(TEXT_DOMAIN,
					"%s Group\t%s\t"), nw_disp_group_name(),
					gr_str);
			(void) printf(dgettext(TEXT_DOMAIN, "Status\t%s\t"),
						pnm_status_str(status.status));
			(void) printf(dgettext(TEXT_DOMAIN, "Adapters\t%s\n"),
								status.backups);
			(void) printf(dgettext(TEXT_DOMAIN, "Callbacks\t"));
			for (i = 0; i < cb_cnt; i++) {
				(void) printf(dgettext(TEXT_DOMAIN, "%s %s\t"),
					cb_bufp[i].id, cb_bufp[i].cmd);
			}
			(void) printf(dgettext(TEXT_DOMAIN, "\n"));
			pnm_status_free(&status);
			free(cb_bufp);
		}
		pnm_fini();
		(void) printf(dgettext(TEXT_DOMAIN, "\n\n"));
		free(glist);
	}
	(void) scha_cluster_close(hdl);
}
