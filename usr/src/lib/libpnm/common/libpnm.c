/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)libpnm.c	1.53	08/08/07 SMI"

/*
 * libpnm.so implementations. The interface is at:
 * usr/src/head/rgm/pnm.h. Currently, libpnm.so is used by the
 * SharedAddress and LogicalHostname resources, scrgadm(1M), pnmstat and
 * libscstat. We must keep in mind that this library is not MT-safe. Only one
 * client can use it at a time. Also the library will send a command and wait
 * for a response before sending another command.
 */

/*
 * LIMITATIONS:
 * libpnm doesn't pass back any error strings.
 */

#include <pnm/pnm_ld.h>
#include <sys/sc_syslog_msg.h>
#include <netinet/icmp6.h>
#include <pnm_network.h>
#include <libintl.h>
#include <rgm/libdsdev.h>
#include <dlfcn.h>
#include <scxcfg/scxcfg.h>

#ifdef linux
#include <byteswap.h>
#include <sys/clconf.h>
#endif

#include <sys/sol_version.h>    /* SOL_VERSION */

#if SOL_VERSION >= __s10
#include <zone.h>
#include <sys/vc_int.h>
#endif /* SOL_VERSION */

#define	LIBCLCONF	"/usr/cluster/lib/libclconf.so.1"
#define	LIBPNMPROXY	"/usr/cluster/lib/libclpnmproxy.so.1"
#define	FRGMD		"/usr/cluster/lib/sc/frgmd"

#define	CLCONF_IS_FARM_MGT_ENABLED	"clconf_is_farm_mgt_enabled"
static boolean_t (*dl_clconf_is_farm_mgt_enabled)();
#define	CLCONF_GET_LOCAL_NODEID	"clconf_get_local_nodeid"
static nodeid_t (*dl_clconf_get_local_nodeid)();
#define	CLCONF_GET_NODENAME	"clconf_get_nodename"
static void (*dl_clconf_get_nodename)(nodeid_t, char *);
#define	CLCONF_CLUSTER_GET_NODEID_BY_NODENAME \
	"clconf_cluster_get_nodeid_by_nodename"
static nodeid_t (*dl_clconf_cluster_get_nodeid_by_nodename)(char *);
#define	CLCONF_GET_USER_IPADDR	"clconf_get_user_ipaddr"
static int (*dl_clconf_get_user_ipaddr)(nodeid_t, struct in_addr *);

#if SOL_VERSION >= __s10

#define	CLCONF_GET_ZC_ID	"clconf_get_cluster_id"
static int (*dl_clconf_get_cluster_id)(char *, uint_t *);

/* PNM proxy functions */
#define	PROXY_PNM_MAP_ADAPTER "proxy_pnm_map_adapter"
static int (*dl_proxy_pnm_map_adapter)(char *, char *, pnm_adapter_t,
    pnm_group_t *);
#define	PROXY_PNM_GROUP_LIST "proxy_pnm_group_list"
static int (*dl_proxy_pnm_group_list)(char *, char *, pnm_group_list_t *);
#define	PROXY_PNM_ADAPTERINFO_LIST "proxy_pnm_adapterinfo_list"
static int (*dl_proxy_pnm_adapterinfo_list)(char *, char *,
    pnm_adapterinfo_t **, boolean_t);
#define	PROXY_PNM_GET_INSTANCES "proxy_pnm_get_instances"
static int (*dl_proxy_pnm_get_instances)(char *, char *, pnm_group_t, int *);
#define	PROXY_PNM_GROUP_STATUS "proxy_pnm_group_status"
static int (*dl_proxy_pnm_group_status)(char *, char *, pnm_group_t,
    pnm_status_t *);
#define	PROXY_PNM_GRP_ADP_STATUS "proxy_pnm_grp_adp_status"
static int (*dl_proxy_pnm_grp_adp_status)(char *, char *, pnm_group_t,
    pnm_grp_status_t *);
#define	PROXY_PNM_IS_ZONE_CLUSTER "is_zone_cluster"
static int (*dl_proxy_pnm_is_zone_cluster)();

#endif /* SOL_VERSION >= __s10 */

#ifndef linux
extern int bindresvport(int sd, struct sockaddr_in *sin);
#endif
/* hostname to whose PNM daemon we will connect */
static char host[LINELEN] = "localhost";

#if SOL_VERSION >= __s10
/* zone name of the exclusive-ip zone to whose PNM daemon we will connect */
static char zone_name[ZONENAME_MAX] = "";
#endif /* SOL_VERSION >= __s10 */

static int  s_global = -1; /* global socket identifier */
static char read_version[NAMELEN];	/* version read from pnmd */

/* Europa configured on this node ? */
static boolean_t iseuropa = B_FALSE;
static boolean_t first_time = B_TRUE;

static int	send_recv(commands *cmdi, responses *rspi);
static int	send_cmd(int s, commands *cmdi);
static int	recv_rsp(int s, char *buff);
static int	connect_to_pnmd(boolean_t);

/*
 * Europa configuration specific function
 */
extern int farm_cfg_open();
extern void farm_cfg_close();
extern int farm_cfg_getaddr(scxcfg_nodeid_t, struct in_addr *);
extern int farm_cfg_getnodeid(char *, scxcfg_nodeid_t *);
extern int farm_cfg_get_local_nodename(char *);

/*
 * Structure for translating between PNM group status and its string
 * representation. pnm_status_str() provides such translation for the
 * library user.
 */
typedef struct status_str {
	int		status;
	const char 	*str;
} status_str_t;

static status_str_t status_array[] = {
	{PNM_STAT_OK,		"OK"},
	{PNM_STAT_DOWN,		"DOWN"},
};

/*
 * Scan PNM param file and return true if the given parameter
 * is found in the file. PNM parameters are considered "set" iff it exists.
 */
int
pnm_param_isset(pnm_param_t param)
{
	FILE	*fp = NULL;
	char	linebuf[LINELEN + 1];
	char	*paramstr = NULL;
	size_t	paramstrlen;
	int	found = 0;

	if ((fp = fopen(PNM_PARAM_FILE, "r")) == NULL) {
		/* can't read file, param doesn't exist */
		return (0);
	}
	if (param == PNM_PARAM_MONALL)
		paramstr = "ssm_monitor_all";
	if (paramstr != NULL) {
		paramstrlen = strlen(paramstr);
		while (fgets(linebuf, LINELEN, fp) != NULL) {
			if (strncmp(linebuf, paramstr, paramstrlen) == 0) {
				found = 1;
				break;
			}
		}
	}
	(void) fclose(fp);
	return (found);
}

/*
 * Return the Public Network Group name that should be displayed in
 * messages depending on the OS/platform we are running on.
 * For Solaris, IPMP will be returned.
 * For Linux, Bonding will be returned.
 */
char *
pnm_group_name()
{
	return (nw_disp_group_name());
}

/*
 * Frees the pnm_status_t structure returned in pnm_group_status(). Note
 * that the pnm_status_t * argument itself is not freed by this routine.
 * INPUT: pointer to pnm_status_t struct.
 * RETURN: void.
 */
void
pnm_status_free(pnm_status_t *sp)
{
	if (sp->backups)
		free(sp->backups);
}

/*
 * Frees the pnm_grp_status_t structure returned in pnm_group_status(). Note
 * that the pnm_grp_status_t * argument itself is not freed by this routine.
 * INPUT: pointer to pnm_grp_status_t struct.
 * RETURN: void.
 */
void
pnm_grp_status_free(pnm_grp_status_t *gsp)
{
	uint_t i;
	for (i = 0; i < gsp->num_adps; i++) {
		if (gsp->pnm_adp_stat[i].adp_name) {
			free(gsp->pnm_adp_stat[i].adp_name);
			gsp->pnm_adp_stat[i].adp_name = NULL;
		}
	}
	if (gsp->pnm_adp_stat) {
		free(gsp->pnm_adp_stat);
		gsp->pnm_adp_stat = NULL;
	}
}

/*
 * Frees the pnm_ip_check_grplist_t structure returned in pnm_ip_check(). Note
 * that the pnm_ip_check_grplist_t * argument itself is not freed by this
 * routine.
 * INPUT: pointer to pnm_ip_check_grplist_t struct.
 * RETURN: void.
 */
void
pnm_ip_check_grplist_free(pnm_ip_check_grplist_t *glp)
{
	uint_t i;
	for (i = 0; i < glp->num_grps; i++) {
		if (glp->listp[i].grp_name) {
			free(glp->listp[i].grp_name);
			glp->listp[i].grp_name = NULL;
		}
	}
	if (glp->listp)
		free(glp->listp);
}

/*
 * Maps PNM group status to string representation.
 * INPUT: int which is used as an index into the status string array.
 * RETURN: char pointer which points to the status string.
 */
const char *
pnm_status_str(int st)
{
	status_str_t	*sp;
	int		i;

	for (sp = &status_array[0], i = 0; i < 2; i++, sp++)
		if (sp->status == st)
			break;
	return (sp->str);
}

/*
 * Frees the pnm_adp_addrs_t structure.
 * INPUT: pointer to pnm_adp_addrs_t struct.
 * RETURN: void.
 */
void
free_pnm_adp_addrs(pnm_adp_addrs_t *pat)
{
	if (pat->adp_ipmask)
		free(pat->adp_ipmask);
	if (pat->adp_ipnet)
		free(pat->adp_ipnet);
	if (pat->adp_ipaddr)
		free(pat->adp_ipaddr);
}

/*
 * Free a pnm_adapterinfo_t structure and all its next pointers.
 * The argument pointer adp is also freed by this function.
 * INPUT: pointer to a pnm_adapterinfo_t struct
 * RETURN: void.
 */
void
pnm_adapterinfo_free(pnm_adapterinfo_t *adp)
{
	pnm_adapterinfo_t	*ip, *ip_next;

	for (ip = adp; ip; ip = ip_next) {
		ip_next = ip->next;
		free(ip->adp_name);
		free(ip->adp_group);
		free_pnm_adp_addrs(&(ip->adp_addrs));
		free(ip);
	}
}


/*
 * dlopen() the PNM proxy library and initialize the function
 * pointers to the PNM proxy interfaces.
 */
#if SOL_VERSION >= __s10
/*lint -e611 */
int
pnm_proxy_lib_initialize()
{
	void *dlprxyhandle;

	if ((dlprxyhandle = dlopen(LIBPNMPROXY, RTLD_LAZY))
	    == NULL) {
		log_syserr_lib("Cannot open libpnmproxy: %s.", dlerror());
		return (-1);
	}

	if ((dl_proxy_pnm_map_adapter = (int(*)(char *, char *, pnm_adapter_t,
	    pnm_group_t *))dlsym(dlprxyhandle,
	    PROXY_PNM_MAP_ADAPTER)) == NULL) { /*lint !e611 */
		log_syserr_lib("Cannot access libpnmproxy function: %s.",
		    dlerror());
		exit(PNM_EPROG);
	}

	if ((dl_proxy_pnm_group_list = (int(*)(char *, char *,
	    pnm_group_list_t *))dlsym(dlprxyhandle,
	    PROXY_PNM_GROUP_LIST)) == NULL) { /*lint !e611 */
		log_syserr_lib("Cannot access libpnmproxy function: %s.",
		    dlerror());
		exit(PNM_EPROG);
	}

	if ((dl_proxy_pnm_adapterinfo_list = (int(*)(char *, char *,
	    pnm_adapterinfo_t **, boolean_t))dlsym(dlprxyhandle,
	    PROXY_PNM_ADAPTERINFO_LIST)) /*lint !e611 */
	    == NULL) {
		log_syserr_lib("Cannot access libpnmproxy function: %s.",
		    dlerror());
		exit(PNM_EPROG);
	}

	if ((dl_proxy_pnm_get_instances =
	    (int(*)(char *, char *, pnm_group_t, int *))dlsym(dlprxyhandle,
	    PROXY_PNM_GET_INSTANCES)) == NULL) { /*lint !e611 */
		log_syserr_lib("Cannot access libpnmproxy function: %s.",
		    dlerror());
		exit(PNM_EPROG);
	}

	if ((dl_proxy_pnm_group_status = (int(*)(char *, char *,
	    pnm_group_t, pnm_status_t *))dlsym(dlprxyhandle,
	    PROXY_PNM_GROUP_STATUS)) == NULL) { /*lint !e611 */
		log_syserr_lib("Cannot access libpnmproxy function: %s.",
		    dlerror());
		exit(PNM_EPROG);
	}

	if ((dl_proxy_pnm_grp_adp_status = (int(*)(char *, char *,
	    pnm_group_t, pnm_grp_status_t *))dlsym(dlprxyhandle,
	    PROXY_PNM_GRP_ADP_STATUS)) == NULL) { /*lint !e611 */
		log_syserr_lib("Cannot access libpnmproxy function: %s.",
		    dlerror());
		exit(PNM_EPROG);
	}

	if ((dl_proxy_pnm_is_zone_cluster =
	    (int(*)())dlsym(dlprxyhandle,
	    PROXY_PNM_IS_ZONE_CLUSTER)) == NULL) { /*lint !e611 */
		log_syserr_lib("Cannot access libpnmproxy "
		    "function: %s.", dlerror());
		exit(PNM_EPROG);
	}
	return (0);
}
/*lint +e611 */
#endif /* SOL_VERSION >= __s10 */

/*
 * The exact procedure for adding more commands to the library are:
 * First have a data structure for the command and the response.
 * Then in the library call create the command - fill in all the fields. While
 * filling in all the fields make sure to update the size of the command that
 * we want to send to the daemon.
 * Then send the command to the daemon and receive the response.
 * Now give the response back to the caller.
 * Now you could also add calls with the suffix _direct which can directly use
 * ioctls to interact with kernel networking module.
 * However these calls will only work locally.
 */

/* libpnm interface implementations. See pnm.h for more info */

/*
 * Store the hostname for the pnmd on the named host. This hostname
 * is cleared only in pnm_fini(), and there's only one hostname
 * for the entire library.
 *	hostname:	host whose pnmd is to be contacted. NULL implies
 *			"localhost".
 * Returns 0 on success and -1 on error.
 */
int
pnm_init(const char *hostname)
{
	char nodename[LINELEN];
	void *dlhandle;
	int rc;
	nodeid_t nodeid;
	struct stat filestat;

	if (first_time) {
		if (stat(FRGMD, &filestat) < 0) {
			/*
			 * Open the libclconf on the Sun Cluster nodes
			 * We can't use the dynamic linking of libclconf as
			 * libclconf is not available on the Europa farm nodes.
			 * If Europa is enabled, dlopen the Europa
			 * configuration
			 */
			if ((dlhandle = dlopen(LIBCLCONF, RTLD_LAZY))
			    == NULL) {
				log_syserr_lib("Cannot open libclconf: %s.",
				    dlerror());
				return (-1);
			}

			if ((dl_clconf_get_local_nodeid =
			    (nodeid_t(*)()) dlsym(dlhandle,
			    CLCONF_GET_LOCAL_NODEID)) /* lint !e611 */
			    == NULL) {
				log_syserr_lib("Cannot access libclconf "
				    "function: %s", dlerror());
				return (-1);
			}

			if ((dl_clconf_get_nodename =
			    (void (*)())dlsym(dlhandle,
			    CLCONF_GET_NODENAME)) /* lint !e611 */
			    == NULL) {
				log_syserr_lib("Cannot access libclconf "
				    "function: %s", dlerror());
				return (-1);
			}

			if ((dl_clconf_cluster_get_nodeid_by_nodename =
			    (nodeid_t (*)()) dlsym(dlhandle,
			    /* lint -e611 */
			    CLCONF_CLUSTER_GET_NODEID_BY_NODENAME))
			    /* lint +e611 */
			    == NULL) {
				log_syserr_lib("Cannot access libclconf "
				    "function: %s", dlerror());
				return (-1);
			}

			if ((dl_clconf_get_user_ipaddr = (int(*)())
			    dlsym(dlhandle,
			    CLCONF_GET_USER_IPADDR)) /* lint !e611 */
			    == NULL) {
				log_syserr_lib("Cannot access libssconf "
				    "function: %s", dlerror());
				return (-1);
			}

			if ((dl_clconf_is_farm_mgt_enabled = (boolean_t(*)())
			    dlsym(dlhandle,
			    CLCONF_IS_FARM_MGT_ENABLED)) /* lint !e611 */
			    == NULL) {
				log_syserr_lib("Cannot access libclconf "
				"function: %s.", dlerror());
				exit(PNM_EPROG);
			}

#if SOL_VERSION >= __s10
			if ((dl_clconf_get_cluster_id = (int(*)(char *,
			    uint_t *))dlsym(dlhandle,
			    CLCONF_GET_ZC_ID)) /*lint !e611 */
			    == NULL) {
				log_syserr_lib("Cannot access libclconf "
				"function: %s.", dlerror());
				exit(PNM_EPROG);
			}
			/*
			 * dlopen the PNM proxy library and initialize
			 * the function pointers to the PNM proxy
			 * interfaces.
			 */
			if ((rc = pnm_proxy_lib_initialize()) != 0) {
				return (rc);
			}
#endif /* SOL_VERSION >= __s10 */
			/* Check if farm mgmt enabled */
			iseuropa = dl_clconf_is_farm_mgt_enabled();
		} else {
			/*
			 * Farm node case => We are on a Europa cluster
			 */
			iseuropa = B_TRUE;
		}
		first_time = B_FALSE;
	}

	/*
	 * if the given hostname is the current node, then set host to
	 * localhost
	 */

	if (iseuropa) {
		rc = farm_cfg_open();
		if (rc == -1) {
			log_syserr_lib("Failed to open the "
			    "farm configuration");
			return (-1);
		}
	}

	if (hostname == NULL) {
		/* NULL implies "localhost" */
		(void) snprintf(host, LINELEN, "%s", "localhost");
	} else {
		/*
		 * pnm_init() is called only from server nodes
		 */
		nodeid = dl_clconf_get_local_nodeid();
		dl_clconf_get_nodename(nodeid, nodename);

		if (strcmp(hostname, nodename) == 0) {
			(void) snprintf(host, LINELEN, "%s", "localhost");
		} else {
			(void) snprintf(host, LINELEN, "%s", hostname);
		}
	}

	return (0);
}


/*
 * Store localhost as the hostname. There's only one hostname for the entire
 * library. Close the previous connection if open and reset the global socket
 * identifier which represented this open connection.
 * Also reset the zone name to a NULL string.
 */
void
pnm_fini()
{
	(void) snprintf(host, LINELEN, "%s", "localhost");
	(void) close(s_global);
	s_global = -1;	/* make the socket -1 */

	if (iseuropa) {
		farm_cfg_close();
	}
#if SOL_VERSION >= __s10
	zone_name[0] = 0;
#endif /* SOL_VERSION >= __s10 */
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - system error or send or receive error, or daemon error.
 */
int
pnm_group_list(pnm_group_list_t *listp)
{
	commands cmdi;
	responses *rspi;
	char buff[MAX_RSP_SIZE];
	int error;
	int result;
	size_t cmd_len;
#if SOL_VERSION >= __s10
	zoneid_t		zid = getzoneid();
	/*
	 * Check if the function is invoked for the zone cluster
	 * or exclusive-ip zone from the global zone. If so, the PNM proxy
	 * interface is invoked.
	 */
	if ((dl_proxy_pnm_group_list != NULL) && (zone_name[0] != 0) &&
	    (zid == 0)) {
		error = (*dl_proxy_pnm_group_list)(host, zone_name, listp);
		return (error);
	}
	/*
	 * If the function is being executed from within a zone, then
	 * it should be a cluster brand zone or ex-ip zone.
	 * If it is a cluster brand zone, then we have to contact the
	 * proxy.
	 */
	if ((dl_proxy_pnm_is_zone_cluster != NULL) && (zid != 0)) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			/* Error */
			return (PNM_EPROG);
		} else if (result > 0) {
			/* Zone cluster. */
			result = (*dl_proxy_pnm_group_list)(host, "global",
			    listp);
			return (result);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	cmd_len = sizeof (uint32_t) + sizeof (size_t);

	rspi = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

	/* Form the command */
	cmdi.command = GROUP_LIST;
	cmdi.cmd_len = cmd_len;

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	error = ntohl(rspi->u.rsp_glrsp.error);
	if (error)
		return (error);

	/* The user has to free this memory later after use */
	*listp = strdup(rspi->u.rsp_glrsp.gl);

	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - system or internal error.
 * PNM_ENOBKG - given group not found.
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_EPERM - Not permitted to use the interface.
 * This interacts directly with kernel netowrking using ioctls and
 * does not contact pnmd through TCP. This is used only by libhaip.
 */
int
pnm_group_status_direct(pnm_group_t group, pnm_status_t *statusp)
{
	int error = 0;
	bkg_status *bkg, *nbkg = NULL;
	bkg_adp *adp;
	char listbuf[LINELEN] = {0};
	int result;

	/* If no group specified then return error */
	if (group == NULL || *group == 0)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
	/*
	 * Check if the function is invoked from the zone cluster.
	 * If so, return an error as the zone cluster does not
	 * have the privileges to execute this function.
	 */
	if (dl_proxy_pnm_is_zone_cluster != NULL) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			/* Error. */
			return (PNM_EPROG);
		} else if (result > 0) {
			/* Zone cluster. */
			return (PNM_EPERM);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	/* Get current group information */
	if ((error =  nw_group_config_read(&nbkg)) != 0) {
		return (error);
	}

	/* Find the group in the grouplist */
	if ((bkg = nw_group_find(group, nbkg)) == NULL) {
		log_err_lib("%s group %s not found",
			nw_disp_group_name(),  group);
		nw_group_config_free(nbkg);
		return (PNM_ENOBKG);
	}

	if (statusp != NULL) {
		for (adp = bkg->head; adp; adp = adp->next) {
			(void) strcat(listbuf, adp->adp);
			if (adp->next)
				(void) strcat(listbuf, ":");
		}
		statusp->backups = strdup(listbuf);
		if (statusp->backups == NULL) {
			log_syserr_lib("out of memory.");
			nw_group_config_free(nbkg);
			return (PNM_EPROG);
		}
		statusp->status = nw_group_status(bkg);
	}

	nw_group_config_free(nbkg);
	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error or out of memory.
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_ENOBKG - given group not found.
 */
int
pnm_group_status(pnm_group_t group, pnm_status_t *statusp)
{
	commands cmdi;
	responses *rspi;
	char buff[MAX_RSP_SIZE];
	int error;
	int result;
#if SOL_VERSION >= __s10
	zoneid_t		zid = getzoneid();
#endif /* SOL_VERSION >= __s10 */

	size_t cmd_len = sizeof (uint32_t) + sizeof (struct cmd_group_status)
	    + sizeof (size_t);

	if (group == NULL || *group == 0)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	/*
	 * Check if the function is invoked for the zone cluster
	 * or exclusive-ip zone from the global zone. If so, the PNM proxy
	 * interface is invoked.
	 */
	if ((dl_proxy_pnm_group_status != NULL) && (zone_name[0] != 0) &&
	    (zid == 0)) {
		error = (*dl_proxy_pnm_group_status)(host, zone_name,
		    group, statusp);
		return (error);
	}
	/*
	 * If the function is being executed from within a zone, then
	 * it should be a cluster brand zone or exclusive-ip zone.
	 * If it is a cluster brand zone, then we have to contact the
	 * proxy.
	 */
	if ((dl_proxy_pnm_is_zone_cluster != NULL) && (zid != 0)) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			/* Error */
			return (PNM_EPROG);
		} else if (result > 0) {
			/* Zone cluster. */
			result = (*dl_proxy_pnm_group_status)(
			    host, "global", group, statusp);
			return (result);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	rspi = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

	/* Form the command */
	cmdi.command = GROUP_STATUS;
	cmdi.cmd_len = cmd_len;

	(void) sprintf(cmdi.u.cmd_gscmd.gname, "%s", group);

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	error = ntohl(rspi->u.rsp_gsrsp.error);
	if (error)
		return (error);

	if (statusp != NULL) {
		statusp->status = ntohl(rspi->u.rsp_gsrsp.status);
		statusp->backups = strdup(rspi->u.rsp_gsrsp.adplist);
		if (statusp->backups == NULL) {
			log_syserr_lib("out of memory.");
			return (PNM_EPROG);
		}
	}

	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error, or out of memory.
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_ENOBKG - given group not found.
 * PNM_ECALLBACK - given callback could not be added.
 * PNM_EPERM - Not permitted to use the interface.
 */
int
pnm_callback_reg(pnm_group_t group, char *id, char *cmd)
{
	commands cmdi;
	responses *rspi;
	char buff[MAX_RSP_SIZE];
	int result;

	size_t cmd_len = sizeof (uint32_t) + sizeof (struct cmd_callback_reg)
	    + sizeof (size_t);
	int error;

	if (group == NULL || *group == 0 ||
		id == NULL || cmd == NULL)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
	/*
	 * Check if the function is invoked from the zone cluster.
	 * If so, return an error as the zone cluster does not
	 * have the privileges to execute this function.
	 */
	if (dl_proxy_pnm_is_zone_cluster != NULL) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			return (PNM_EPROG);
		} else if (result > 0) {
			return (PNM_EPERM);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	if (strlen(id) + strlen(cmd) > CB_LINELEN)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
#endif
	rspi = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

	/* Form the command */
	cmdi.command = CALLBACK_REG;
	cmdi.cmd_len = cmd_len;
	(void) snprintf(cmdi.u.cmd_crcmd.gname, NAMELEN, "%s", group);
	(void) snprintf(cmdi.u.cmd_crcmd.buf, CB_LINELEN, "%s %s", id, cmd);

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	error = ntohl(rspi->u.rsp_crrsp.error);
	if (error)
		return (error);

	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error.
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_ENOBKG - given group not found.
 * PNM_EPERM - Not permitted to use the interface.
 */
int
pnm_callback_unreg(pnm_group_t group, char *id)
{
	commands cmdi;
	responses *rspi;
	char buff[MAX_RSP_SIZE];
	size_t cmd_len = sizeof (uint32_t) + sizeof (struct cmd_callback_unreg)
	    + sizeof (size_t);
	int error;
	int result;

	if (group == NULL || *group == 0 || id == NULL || *id == 0)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
	/*
	 * Check if the function is invoked from the zone cluster.
	 * If so, return an error as the zone cluster does not
	 * have the privileges to execute this function.
	 */
	if (dl_proxy_pnm_is_zone_cluster != NULL) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			return (PNM_EPROG);
		} else if (result > 0) {
			return (PNM_EPERM);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	rspi = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

	/* Form the command */
	cmdi.command = CALLBACK_UNREG;
	cmdi.cmd_len = cmd_len;
	(void) snprintf(cmdi.u.cmd_cucmd.gname, NAMELEN, "%s", group);
	(void) snprintf(cmdi.u.cmd_cucmd.id, LINELEN, "%s", id);

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	error = ntohl(rspi->u.rsp_cursp.error);
	if (error)
		return (error);

	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error, or out of memory.
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_ENOBKG - given group not found.
 * PNM_EPERM  - Not permitted to use the interface.
 */
int
pnm_callback_list(pnm_group_t group, uint_t *cnt, pnm_callback_t **buf)
{
	commands	cmdi;
	responses	*rspi;
	char		buff[MAX_RSP_SIZE];
	uint_t		i;
	uint_t		j;
	char		*p, *cb_str, *next_cb_str;
	uint_t		cb_cnt;
	pnm_callback_t	*cb_buf;
	size_t cmd_len = sizeof (uint32_t) + sizeof (struct cmd_callback_list)
	    + sizeof (size_t);
	int error;
	int result;

	if (group == NULL || *group == 0)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
#endif
	rspi = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
	/*
	 * Check if the function is invoked from the zone cluster.
	 * If so, return an error as the zone cluster does not
	 * have the privileges to execute this function.
	 */
	if (dl_proxy_pnm_is_zone_cluster != NULL) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			return (PNM_EPROG);
		} else if (result > 0) {
			return (PNM_EPERM);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	/* Form the command */
	cmdi.command = CALLBACK_LIST;
	cmdi.cmd_len = cmd_len;
	(void) snprintf(cmdi.u.cmd_clcmd.gname, NAMELEN, "%s", group);

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	error = ntohl(rspi->u.rsp_clrsp.error);
	if (error)
		return (error);

	/*
	 * This loop runs through the buf and gives us the total number of
	 * callbacks listed by counting the number of ":" and adding 1 to the
	 * total number of ":". The case of no callbacks is special - we get a
	 * count of 1 but in the next loop where we break up the buf into
	 * tokens we get a NULL token and so the count is finally 0.
	 */
	for (p = rspi->u.rsp_clrsp.cb_buf, j = 1;
	    (p = strchr(p, ':')) != NULL; p++, j++)
		;
	cb_cnt = j;
	cb_buf = (pnm_callback_t *)calloc((size_t)cb_cnt,
	    sizeof (pnm_callback_t));
	if (cb_buf == NULL) {
		log_syserr_lib("out of memory.");
		return (PNM_EPROG);
	}
	next_cb_str = rspi->u.rsp_clrsp.cb_buf;
	for (i = 0; i < cb_cnt; i++) {
		/* This gives us the first callback */
		cb_str = (char *)strtok_r(next_cb_str, ":", &next_cb_str);

		/* This is for the case of no callbacks registered. */
		if (cb_str == NULL)
			break;

		/* This gives us the callback id */
		if ((p = strtok(cb_str, " ")) != NULL) {
			cb_buf[i].id = strdup(p);
			if (cb_buf[i].id == NULL) {
				log_syserr_lib("out of memory.");
				return (PNM_EPROG);
			}
		} else
			break;

		/* This could also be compared to "\0" */
		if ((p = strtok(NULL, "\n")) != NULL) {
			cb_buf[i].cmd = strdup(p);
			if (cb_buf[i].cmd == NULL) {
				log_syserr_lib("out of memory.");
				return (PNM_EPROG);
			}
		} else {
			free(cb_buf[i].id);
			cb_buf[i].id = NULL;
			break;
		}
	}

	*cnt = i;
	*buf = cb_buf;
	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error, or out of memory.
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_ENOBKG - given group not found.
 * PNM_ENOOP - given operation is not valid.
 * PNM_EPERM - Not permitted to use this interface.
 * This interacts directly with kernel using ioctls and does not contact pnmd
 * through TCP. This is used only by libhaip.
 * This library call is provided to serialize haip code and also
 * to export the group concept to the user. This performs ifconfig commands on
 * the given list of IP addresses on the given PNM group. It gets the state of
 * the adapters. Then the given command is performed on the given list of IP
 * addresses on the given PNM group. It returns the number of IP addresses on
 * which the given ifconfig command failed (eg: if the given command failed on
 * 2 IP addresses then it returns -2). The call is idempotent in the sense
 * that if the given command has already been performed on some of the IP
 * addresses, it returns success for those IP addresses and does not count
 * those as failed.
 */
int
pnm_ifconfig_direct(pnm_group_t group, const char *zone, pnm_ifconfig_op_t op,
    uint_t ip_cnt, struct in6_addr *ip_buf)
{
	int error = 0;
	int result = 0;
	bkg_status *bkg = NULL, *nbkg = NULL;

	/* Check if the group name is valid */
	if (group == NULL || *group == 0) {
		return (PNM_EUSAGE);
	}

	/* Check if the given operation is valid */
	if (op < PNM_IFCONFIG_PLUMB || op > PNM_IFCONFIG_DOWN) {
		return (PNM_ENOOP);
	}

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
	/*
	 * Check if the function is invoked from the zone cluster.
	 * If so, return an error as the zone cluster does not
	 * have the privileges to execute this function.
	 */
	if (dl_proxy_pnm_is_zone_cluster != NULL) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			return (PNM_EPROG);
		} else if (result > 0) {
			return (PNM_EPERM);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	switch ((int)op) {
	case PNM_IFCONFIG_PLUMB:
		/* Get current group information */
		if (nw_group_config_read(&nbkg) != 0) {
			return (PNM_EPROG);
		}
		/* Check if group present */
		if ((bkg = nw_group_find(group, nbkg)) == NULL) {
			log_err_lib("%s group %s not found",
				nw_disp_group_name(),  group);
			nw_group_config_free(nbkg);
			return (PNM_ENOBKG);
		}

		error = nw_ifconfig_plumb(bkg, zone, ip_cnt,
		    ip_buf);
		nw_group_config_free(nbkg);
		break; /* End of switch */

	case PNM_IFCONFIG_UNPLUMB:

		error = nw_ifconfig_unplumb(ip_cnt, ip_buf);
		break; /* End of switch */

	case PNM_IFCONFIG_UP:
		/* Get current group information */
		if (nw_group_config_read(&nbkg) != 0) {
			return (PNM_EPROG);
		}
		/* Check if group present */
		if ((bkg = nw_group_find(group, nbkg)) == NULL) {
			log_err_lib("%s group %s not found",
				nw_disp_group_name(),  group);
			nw_group_config_free(nbkg);
			return (PNM_ENOBKG);
		}

		error = nw_ifconfig_up(bkg, ip_cnt, ip_buf);
		break; /* End of switch */

	case PNM_IFCONFIG_DOWN:

		error = nw_ifconfig_down(ip_cnt, ip_buf);
		break; /* End of switch */

	default:
		error = PNM_ENOOP;
	}	/* End of switch */

	return (error);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error, or out of memory.
 * PNM_EUSAGE - given arguments are not valid.
 * Out here we check the given version number. If the given version number
 * does not match with pnmd's version number then we error out with a message.
 */
int
pnm_adapterinfo_list(pnm_adapterinfo_t **adpp, boolean_t alladdrs)
{
	uint_t			i, j, l;
	nw_adapter_t		*ptr = NULL;
	pnm_adapterinfo_t	*adp, *head_adp, *last_adp;
	commands		cmdi;
	responses		*rspi = NULL;
	size_t cmd_len = sizeof (uint32_t) + sizeof (size_t);
	char buff[ADPLIST_BUF]; /* need a large buffer */
	size_t offset = 0;
	uint_t num_adps;
	uint64_t num_addrs;
	int error;
	int result;
#if SOL_VERSION >= __s10
	zoneid_t zid;
#endif /* SOL_VERSION >= __s10 */

	if (adpp == NULL)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	zid = getzoneid();
	/*
	 * Check if the function is invoked for the zone cluster
	 * or exclusive-ip zone from the global zone. If so, the PNM proxy
	 * interface is invoked.
	 */
	if ((dl_proxy_pnm_adapterinfo_list != NULL) &&
	    (zone_name[0] != 0) && (zid == 0)) {
		error = (*dl_proxy_pnm_adapterinfo_list)(host, zone_name,
		    adpp, B_FALSE);
		return (error);
	}
	/*
	 * If the function is being executed from within a zone, then
	 * it should be a cluster brand zone or xip zone.
	 * If it is a cluster brand zone, then we have to contact the
	 * proxy.
	 */
	if ((dl_proxy_pnm_is_zone_cluster != NULL) && (zid != 0)) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			return (PNM_EPROG);
		} else if (result > 0) {
			result = (*dl_proxy_pnm_adapterinfo_list)(host,
			    "global", adpp, alladdrs);
			return (result);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	bzero((char *)&cmdi, cmd_len);
	bzero(buff, sizeof (buff));


	/* Form the command */
	if (alladdrs)
		cmdi.command = ADDRINFO_LIST;
	else
		cmdi.command = ADAPTERINFO_LIST;
	cmdi.cmd_len = cmd_len;
	rspi = (responses *) buff;

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	error = ntohl(rspi->u.rsp_alrsp.error);
	if (error)
		return (error);

	/* Form the data structure to return - adapterinfolist */
	adp = head_adp = last_adp = NULL;
	num_adps = ntohl(rspi->u.rsp_alrsp.num_adps);
	for (i = 0; i < num_adps; i++) {
		ptr = (nw_adapter_t *)((char *)&rspi->u.rsp_alrsp.adpr +
		    offset);

		adp = (pnm_adapterinfo_t *)
		    malloc(sizeof (pnm_adapterinfo_t));
		if (adp == NULL) {
			log_syserr_lib("out of memory.");
			pnm_adapterinfo_free(head_adp);
			return (PNM_EPROG);
		}
		bzero(adp, sizeof (pnm_adapterinfo_t));

		/* Form the linked list */
		if (head_adp == NULL)
			head_adp = adp;
		if (last_adp != NULL) {
			last_adp->next = adp;
			adp->prev = last_adp;
		}
		last_adp = adp;

		adp->adp_name = strdup(ptr->adp_name);
		if (adp->adp_name == NULL) {
			log_syserr_lib("out of memory.");
			pnm_adapterinfo_free(head_adp);
			return (PNM_EPROG);
		}

		/* If groupname is non-NULL then assign a pointer to it */
		if (strcmp((ptr->adp_group), "") != 0) {
			adp->adp_group = strdup(ptr->adp_group);
			if (adp->adp_group == NULL) {
				log_syserr_lib("out of memory.");
				pnm_adapterinfo_free(head_adp);
				return (PNM_EPROG);
			}
		} else
			adp->adp_group = NULL;

#ifdef linux
		adp->adp_flags = bswap_64(ptr->adp_flags);
		num_addrs = bswap_64(ptr->num_addrs);
#else
		adp->adp_flags = ntohll(ptr->adp_flags);
		num_addrs = ntohll(ptr->num_addrs);
#endif

		adp->adp_addrs.num_addrs = num_addrs;
		adp->adp_addrs.adp_ipnet = (struct in6_addr *)
		    malloc((uint_t)num_addrs * CL_IPV6_ADDR_LEN);
		if (adp->adp_addrs.adp_ipnet == NULL) {
			log_syserr_lib("out of memory.");
			pnm_adapterinfo_free(head_adp);
			return (PNM_EPROG);
		}
		adp->adp_addrs.adp_ipmask = (struct in6_addr *)
		    malloc((uint_t)num_addrs * CL_IPV6_ADDR_LEN);
		if (adp->adp_addrs.adp_ipmask == NULL) {
			log_syserr_lib("out of memory.");
			pnm_adapterinfo_free(head_adp);
			return (PNM_EPROG);
		}
		adp->adp_addrs.adp_ipaddr = (struct in6_addr *)
		    malloc((uint_t)num_addrs * CL_IPV6_ADDR_LEN);
		if (adp->adp_addrs.adp_ipaddr == NULL) {
			log_syserr_lib("out of memory.");
			pnm_adapterinfo_free(head_adp);
			return (PNM_EPROG);
		}
		for (j = 0, l = 0; l < num_addrs; l++) { /* CSTYLED */

			/* lint -e662 */
			bcopy((char *)&ptr->adp_net + (j++ * CL_IPV6_ADDR_LEN),
			    &(adp->adp_addrs.adp_ipnet[l]),
			    (size_t)CL_IPV6_ADDR_LEN);
			bcopy((char *)&ptr->adp_net + (j++ * CL_IPV6_ADDR_LEN),
			    &(adp->adp_addrs.adp_ipmask[l]),
			    (size_t)CL_IPV6_ADDR_LEN);
			bcopy((char *)&ptr->adp_net + (j++ * CL_IPV6_ADDR_LEN),
			    &(adp->adp_addrs.adp_ipaddr[l]),
			    (size_t)CL_IPV6_ADDR_LEN);
			/* CSTYLED */
			/*lint +e662 */
		}
		offset +=  2 * (sizeof (uint64_t) + NAMELEN) +
		    3 * l * CL_IPV6_ADDR_LEN;
	}

	*adpp = head_adp;
	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error.
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_ENOADP - the given adp is not present.
 */
int
pnm_map_adapter(pnm_adapter_t adp, pnm_group_t *group)
{
	commands cmdi;
	responses *rspi;
	char buff[MAX_RSP_SIZE];
	size_t cmd_len = sizeof (uint32_t) + sizeof (size_t) +
	    sizeof (struct cmd_map_adapter);
	int error;
	int result;
#if SOL_VERSION >= __s10
	zoneid_t zid;
#endif /* SOL_VERSION >= __s10 */

	if (adp == NULL || *adp == 0)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	zid = getzoneid();
	/*
	 * Check if the function is invoked for the zone cluster
	 * or exclusive-ip zone from the global zone. If so, the PNM proxy
	 * interface is invoked.
	 */
	if ((dl_proxy_pnm_map_adapter != NULL) && (zone_name[0] != 0) &&
	    (zid == 0)) {
		error = (*dl_proxy_pnm_map_adapter)(host, zone_name, adp,
		    group);
		return (error);
	}
	/*
	 * If the function is being executed from within a zone, then
	 * it should be a cluster brand zone. If it is a cluster brand zone,
	 * then we have to contact the proxy.
	 */
	if ((dl_proxy_pnm_is_zone_cluster != NULL) && (zid != 0)) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			/* Error. */
			return (PNM_EPROG);
		} else if (result > 0) {
			/* Zone cluster. */
			result = (*dl_proxy_pnm_map_adapter)(host, "global",
			    adp, group);
			return (result);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	rspi = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

	/* Form the command */
	cmdi.command = MAP_ADAPTER;
	cmdi.cmd_len = cmd_len;
	(void) sprintf(cmdi.u.cmd_macmd.adp, "%s", adp);

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	error = ntohl(rspi->u.rsp_marsp.error);
	if (error)
		return (error);

	*group = strdup(rspi->u.rsp_marsp.gname);

	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error.
 * PNM_EPERM - Not allowed to use this interface in a zone cluster.
 */
int
pnm_mac_address(uint_t *mac_addrs_uniq)
{
	commands cmdi;
	responses *rspi;
	char buff[MAX_RSP_SIZE];
	size_t cmd_len = sizeof (uint32_t) + sizeof (size_t);
	int error;
	int result;

	if (mac_addrs_uniq == NULL)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
	/*
	 * Check if the function is invoked from the zone cluster.
	 * If so, return an error as the zone cluster does not
	 * have the privileges to execute this function.
	 */
	if (dl_proxy_pnm_is_zone_cluster != NULL) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			/* Error. */
			return (PNM_EPROG);
		} else if (result > 0) {
			/* Zone cluster. */
			return (PNM_EPERM);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	rspi = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

	/* Form the command */
	cmdi.command = MAC_ADDRESS;
	cmdi.cmd_len = cmd_len;

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	error = ntohl(rspi->u.rsp_mdrsp.error);
	if (error)
		return (error);

	*mac_addrs_uniq = rspi->u.rsp_mdrsp.mac_addrs_uniq;

	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error, or out of memory.
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_ENOBKG - given group is not present.
 */
int
pnm_grp_adp_status(pnm_group_t group, pnm_grp_status_t *grp_statusp)
{
	commands cmdi;
	responses *rspi;
	char buff[MAX_RSP_SIZE];
	uint_t i;
	size_t cmd_len = sizeof (uint32_t) +
		sizeof (struct cmd_grp_adp_status) + sizeof (size_t);
	int error;
	int result;
#if SOL_VERSION >= __s10
	zoneid_t zid;
#endif /* SOL_VERSION >= __s10 */

	/* If no group specified then return error */
	if (group == NULL || *group == 0)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	zid = getzoneid();
	/*
	 * Check if the function is invoked for the zone cluster
	 * or exclusive-ip zone from the global zone. If so, the PNM proxy
	 * interface is invoked.
	 */
	if ((dl_proxy_pnm_grp_adp_status != NULL) && (zone_name[0] != 0) &&
	    (zid == 0)) {
		error = (*dl_proxy_pnm_grp_adp_status)(host, zone_name,
		    group, grp_statusp);
		return (error);
	}
	/*
	 * If the function is being executed from within a zone, then
	 * it should be a cluster brand zone. If it is a cluster brand zone,
	 * then we have to contact the proxy.
	 */
	if ((dl_proxy_pnm_is_zone_cluster != NULL) && (zid != 0)) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			return (PNM_EPROG);
		} else if (result > 0) {
			result = (*dl_proxy_pnm_grp_adp_status)(host, "global",
			    group, grp_statusp);
			return (result);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	rspi = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

	/* Form the command */
	cmdi.command = GRP_ADP_STATUS;
	cmdi.cmd_len = cmd_len;

	(void) sprintf(cmdi.u.cmd_gdcmd.gname, "%s", group);

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	/* On error return error status */
	error = ntohl(rspi->u.rsp_gdrsp.error);
	if (error)
		return (error);

	if (grp_statusp != NULL) {
		grp_statusp->num_adps = ntohl(rspi->u.rsp_gdrsp.num_adps);
		/* Allocate memory for the adapters in the group */
		grp_statusp->pnm_adp_stat = (pnm_adp_status_t *)
		    malloc(grp_statusp->num_adps * sizeof (pnm_adp_status_t));
		if (grp_statusp->pnm_adp_stat == NULL) {
			log_syserr_lib("out of memory.");
			return (PNM_EPROG);
		}
		bzero(grp_statusp->pnm_adp_stat,
		    grp_statusp->num_adps * sizeof (pnm_adp_status_t));

		for (i = 0; i < grp_statusp->num_adps; i++) {
			grp_statusp->pnm_adp_stat[i].adp_name = strdup(
			    rspi->u.rsp_gdrsp.adp_stat[i].adp_name);
			grp_statusp->pnm_adp_stat[i].status
			    = ntohl(rspi->u.rsp_gdrsp.adp_stat[i].status);
		}
	}

	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error, or out of memory.
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_EPERM - Not permitted to use the interface from a zone cluster.
 */
int
pnm_ip_check(pnm_ip_check_grplist_t *grp_listp)
{
	commands cmdi;
	responses *rspi;
	uint_t i;
	char buff[MAX_RSP_SIZE];
	size_t cmd_len = sizeof (uint32_t) + sizeof (size_t);
	int error;
	int result;

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
	/*
	 * Check if the function is invoked from the zone cluster.
	 * If so, return an error as the zone cluster does not
	 * have the privileges to execute this function.
	 */
	if (dl_proxy_pnm_is_zone_cluster != NULL) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			/* Error. */
			return (PNM_EPROG);
		} else if (result > 0) {
			/* Zone cluster. */
			return (PNM_EPERM);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	rspi = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
#endif
	/* Form the command */
	cmdi.command = IP_CHECK;
	cmdi.cmd_len = cmd_len;

	/* Send the command */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	/* On error return error status */
	error = ntohl(rspi->u.rsp_icrsp.error);
	if (error)
		return (error);

	if (grp_listp != NULL) {
		bzero((char *)grp_listp, sizeof (grp_listp));
		grp_listp->num_grps = ntohl(rspi->u.rsp_icrsp.num_grps);
		/* Allocate memory for the group names */
		grp_listp->listp = (pnm_grp_t *)
		    malloc(grp_listp->num_grps * sizeof (pnm_grp_t));
		if (grp_listp->listp == NULL) {
			log_syserr_lib("out of memory.");
			return (PNM_EPROG);
		}
		for (i = 0; i < grp_listp->num_grps; i++) {
			grp_listp->listp[i].grp_name = strdup(
			    rspi->u.rsp_icrsp.grp[i].grp_name);
		}
	}

	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error.
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_EPERM - Not permitted to be called from within a zone cluster.
 */
int
pnm_group_create(pnm_adapter_t buf)
{
	commands cmdi;
	responses *rspi;
	char buff[MAX_RSP_SIZE];
	size_t cmd_len = sizeof (uint32_t) + sizeof (struct cmd_group_create)
	    + sizeof (size_t);
	int error;
	int result;

	if (buf == NULL || strlen(buf) == 0)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this is meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
	/*
	 * Check if the function is invoked from the zone cluster.
	 * If so, return an error as the zone cluster does not
	 * have the privileges to execute this function.
	 */
	if (dl_proxy_pnm_is_zone_cluster != NULL) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			return (PNM_EPROG);
		} else if (result > 0) {
			return (PNM_EPERM);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	/*
	 * Group can be a NULL pointer or group can point to a NULL string
	 * i.e. *group can be 0 or group can be NULL.
	 */

	rspi = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

	/* Form the command */
	cmdi.command = GROUP_CREATE;
	cmdi.cmd_len = cmd_len;

	/* Pass the adapter name */
	(void) snprintf(cmdi.u.cmd_gccmd.adp, NAMELEN, "%s", buf);

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	error = ntohl(rspi->u.rsp_gcrsp.error);
	if (error)
		return (error);

	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_ENOBKG - given group not found.
 * PNM_ENONHOMOGENOUS - all adps in the group do not have similar instances
 * (IPv4/IPv6) plumbed, hence the group is said to be non-homogenous.
 * This interacts directly with kernel networking module using ioctls
 * and does not contact pnmd through TCP. This is used only by libhaip.
 */
int
pnm_get_instances_direct(pnm_group_t group, int *resultp)
{
	int error = 0;
	bkg_status *bkg, *nbkg = NULL;
	int result;

	/* If no group specified then return error */
	if (group == NULL || *group == 0)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	/*
	 * This interface is not supported by the PNM proxy. We return
	 * an error here if this is meant to be handled by the proxy.
	 */
	if (zone_name[0] != 0) {
		return (PNM_EUSAGE);
	}
	/*
	 * Check if the function is invoked from the zone cluster.
	 * If so, return an error as the zone cluster does not
	 * have the privileges to execute this function.
	 */
	if (dl_proxy_pnm_is_zone_cluster != NULL) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			return (PNM_EPROG);
		} else if (result > 0) {
			return (PNM_EPERM);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	/* Get current group information */
	if ((error = nw_group_config_read(&nbkg)) != 0) {
		return (error);
	}

	/* Find the group in the grouplist */
	if ((bkg = nw_group_find(group, nbkg)) == NULL) {
		log_err_lib("%s group %s not found",
			nw_disp_group_name(),  group);
		nw_group_config_free(nbkg);
		return (PNM_ENOBKG);
	}

	/*
	 * Get the instances from the adps in the group. Form the result
	 * based on all the adps in the group.
	 */
	error = nw_group_instances(bkg, resultp);

	nw_group_config_free(nbkg);
	return (error);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error, or out of memory.
 * PNM_EUSAGE - given arguments are not valid.
 * PNM_ENOBKG - given group not found.
 * PNM_ENONHOMOGENOUS - all adps in the group do not have similar instances
 * PNM_EPERM - Not permitted to be called from a zone cluster.
 * (IPv4/IPv6) plumbed, hence the group is said to be non-homogenous.
 * Check the version number also. If the given version number does not match
 * pnmd's version number then we error out with an appropriate message.
 */
int
pnm_get_instances(pnm_group_t group, int *resultp)
{
	int result;
	commands cmdi;
	responses *rspi;
	char buff[MAX_RSP_SIZE];
	size_t cmd_len = sizeof (uint32_t) +
		sizeof (struct cmd_get_instances) + sizeof (size_t);
	int error;
#if SOL_VERSION >= __s10
	zoneid_t zid;
#endif /* SOL_VERSION >= __s10 */

	/* If no group specified then return error */
	if (group == NULL || *group == 0)
		return (PNM_EUSAGE);

#if SOL_VERSION >= __s10
	zid = getzoneid();
	/*
	 * Check if the function is invoked for the zone cluster
	 * or exclusive-ip zone from the global zone. If so, the PNM proxy
	 * interface is invoked.
	 */
	if ((dl_proxy_pnm_get_instances != NULL) && (zone_name[0] != 0) &&
	    (zid == 0)) {
		error = (*dl_proxy_pnm_get_instances)(host, zone_name,
		    group, resultp);
		return (error);
	}
	/*
	 * If the function is being executed from within a zone, then
	 * it should be a cluster brand zone or exclusive-ip zone.
	 * If it is a cluster brand zone, then it we should invoke
	 * the proxy interface.
	 */
	if ((dl_proxy_pnm_is_zone_cluster != NULL) && (zid != 0)) {
		if ((result = (*dl_proxy_pnm_is_zone_cluster)()) < 0) {
			return (PNM_EPROG);
		} else if (result > 0) {
			result = (*dl_proxy_pnm_get_instances)(host, "global",
			    group, resultp);
			return (result);
		}
	}
#endif /* SOL_VERSION >= __s10 */

	rspi = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

	/* Form the command */
	cmdi.command = GET_INSTANCES;
	cmdi.cmd_len = cmd_len;

	(void) sprintf(cmdi.u.cmd_gicmd.gname, "%s", group);

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, rspi))
		return (PNM_EPROG);

	/* On error return error status */
	error = ntohl(rspi->u.rsp_girsp.error);
	if (error)
		return (error);

	if (resultp != NULL)
		*resultp = ntohl(rspi->u.rsp_girsp.result);

	return (0);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_EPROG - send or receive error, or daemon error, or out of memory.
 * PNM_EUSAGE - given arguments are not valid.
 */
int
pnm_get_zone_iptype(pnm_zone_t zone, pnm_zone_iptype_t *iptypep)
{
#if SOL_VERSION < __s10
	*iptypep = SHARED_IP;
	return (0);
#else
	commands cmdi;
	responses *responsep;
	char buff[MAX_RSP_SIZE];
	size_t cmd_len = sizeof (uint32_t) +
		sizeof (struct cmd_zone_iptype) + sizeof (size_t);
	int error;

	/* If no zone specified then return error */
	if (zone == NULL || *zone == 0) {
		return (PNM_EUSAGE);
	}

	responsep = (responses *) buff;
	bzero((char *)&cmdi, cmd_len);
	bzero(buff, MAX_RSP_SIZE);

	/* Form the command */
	cmdi.command = GET_ZONE_IPTYPE;
	cmdi.cmd_len = cmd_len;

	(void) sprintf(cmdi.u.cmd_ztcmd.zonename, "%s", zone);

	/* Send the command. Receive the response */
	if (send_recv(&cmdi, responsep)) {
		return (PNM_EPROG);
	}

	/* If pnmd sends an error, return it back to the caller  */
	error = ntohl(responsep->u.rsp_ziptype.error);
	if (error) {
		return (error);
	}

	if (iptypep != NULL) {
		*iptypep = ntohl(responsep->u.rsp_ziptype.type);
	}

	return (0);

#endif	/* SOL_VERSION < __s10 */
}

/*
 * Apart from the hostname, store the zone name if the zone is an exclusive-IP
 * zone. The zone name is cleared in pnm_fini() and there is only one
 * zone name for the entire library.
 * Return value: Return 0 on success and -1 on error.
 */
int
pnm_zone_init(char *hostnamep, char *zonenamep)
{
#if SOL_VERSION < __s10
	return (pnm_init(hostnamep));
#else
	int retval;
	zoneid_t zoneid = getzoneid();
	pnm_zone_iptype_t iptype;
	uint_t	clid;

	if ((retval = pnm_init(hostnamep)) != 0) {
		return (retval);
	}
	/*
	 * Return an error if invoked within a zone or the zonename argument
	 * is invalid.
	 */
	if ((zoneid != 0) || (zonenamep == NULL) || (zonenamep[0] == 0)) {
		return (PNM_EPROG);
	}
	/*
	 * We store the zonename only if it is an exclusive-ip zone or a
	 * zone cluster zone.
	 */
	if ((retval = (*dl_clconf_get_cluster_id)(zonenamep, &clid)) < 0) {
		/* Error */
		return (PNM_EPROG);
	} else if (retval == 0) {
		/* zone cluster zone */
		if (clid >= MIN_CLUSTER_ID) {
			(void) strcpy(zone_name, zonenamep);
			return (0);
		}
	}
	if ((retval = pnm_get_zone_iptype((pnm_zone_t)zonenamep, &iptype))
	    < 0) {
		/* Error */
		return (PNM_EPROG);
	} else if (retval == 0) {
		/* Exclusive-Ip zone */
		if (iptype == EXCLUSIVE_IP) {
			(void) strcpy(zone_name, zonenamep);
		}
		return (0);
	}
	return (PNM_EPROG);
#endif	/* SOL_VERSION < __s10 */
}
/*
 * Sends the command to pnmd. If not successful, syslogs
 * an error message and returns error. Reads the response from pnmd.
 * Returns 0 on success.
 * INPUT: pointer to command data struct to send, pointer to response data
 * struct to receive.
 * RETURN: 0 on ok, -1 on error.
 */
int
send_recv(commands *cmdi, responses *responsep)
{
	long ret;
	int s;
	boolean_t vers_flg = B_FALSE;

	/* Set the version flag for certain commands */
	if (cmdi->command == GET_INSTANCES ||
	    cmdi->command == ADAPTERINFO_LIST ||
	    cmdi->command == ADDRINFO_LIST)
		vers_flg = B_TRUE;

	/*
	 * If version check is required then close the previous connected
	 * socket, if one exists.
	 */
	if (vers_flg) {
		(void) close(s_global);
		s_global = -1;
	}

	/*
	 * Make a connection to the daemon - the address of the node whose
	 * daemon we want to connect to is already specified by pnm_init() -
	 * if it has not been specified then we will by default connect to
	 * to the local daemon.
	 */
connect_again:
	if ((s = connect_to_pnmd(vers_flg)) == -1) {
		s_global = -1;
		return (s);
	}

	s_global = s; /* store the global socket identifier */

	/* Send the command */
	ret = send_cmd(s, cmdi);
	if (ret == 1)
		goto connect_again;
	if (ret == -1)
		return (-1);

	/* Receive the response */
	ret = recv_rsp(s, (char *)responsep);

	/* Keep the connection around - do not close the connection */
	return (ret);
}

/*
 * Sends the command to pnmd. If not successful, syslogs
 * an error message and returns error.
 * Returns 0 on success.
 * INPUT: pointer to command data struct to send, and socket identifier.
 * RETURN: 0 on ok, -1 on error and 1 if we should try to connect again.
 */
int
send_cmd(int s, commands *cmdi)
{
	long ret;
	int first = 0;
	size_t len;

	/*
	 * Here if we see that the connection has been dropped we should try
	 * to connect again, since maybe the s_global socket lost its
	 * connection due to too much time elapsed.
	 */
	/* Write the command */
	cmdi->command = htonl(cmdi->command);
	len = cmdi->cmd_len;
	cmdi->cmd_len = htonl(cmdi->cmd_len);
	ret = write(s, cmdi, len);
	if (ret != (long)len) {
		if (ret == (long)-1) {
			if (first++ == 0) {
				s_global = -1;
				return (1);
			}
			log_syserr_lib("write error.");
		}
		s_global = -1;
		(void) close(s);
		return (-1);
	}

	return (0);
}

/*
 * Receives the response from pnmd. If not successful, syslogs
 * an error message and returns error.
 * Returns 0 on success.
 * INPUT: pointer to char buffer to receive, and socket identifier.
 * RETURN: 0 on ok, -1 on error.
 */
int
recv_rsp(int s, char *buff)
{
	int n;
	uint_t len = 0;
	responses *tmp;
	int try = 0;

	/* Read the response */
read_again:
	if ((n = read(s, &buff[len], 32768)) < 0) {
		log_syserr_lib("read error.");
		s_global = -1;
		(void) close(s);
		return (-1);
	}

	len += (uint_t)n;
	tmp = (responses *) buff;
	tmp->rsp_size = ntohl(tmp->rsp_size);
	if (len < tmp->rsp_size && try++ < 3) {
		(void) sleep(1); /* sleep for one second */
		goto read_again;
	} else
	if (try == 3 && len < tmp->rsp_size) {
		log_err_lib("network is too slow.");
		(void) close(s);
		return (-1);
	}

	return (0);
}

/*
 * Returns -1 on failure. Returns the socket file descriptor on
 * success. We can write/read to/from the socket descriptor returned.
 * It first puts the source address as the interconnect address and then tries
 * to connect to the daemon. It gets the port number first from etc/services
 * and on failure it uses a hard-coded port number(PNMD_PORT). On connection
 * it reads a version number and stores it. If successful it returns the
 * connected socket else error. One point to note is that: we would want the
 * /etc/services file to have the entry pnmd 6499/tcp - since if this entry
 * is not present then getservbyname will first look it up in this file and
 * then will try to lookup the NIS (which might keep on trying if it can't be
 * reached) and could never cause getservbyname to fail - which will lead us
 * to hang.
 * Check the version number if required.
 * INPUT: void
 * RETURN: int - socket descriptor.
 */
static int
connect_to_pnmd(boolean_t vers_flg)
{
	int s_v4;
	struct sockaddr_in sin; /* server */
	struct sockaddr_in clin; /* client */
	uint_t addrlen;
	int ret;
	struct servent *sp = NULL;
	long n = 0;
	uint_t len = 0;
	char buff[1024] = {0};
	char *version = PNMD_VERSION;
	int try_again;
	int nop = 0;
	struct hostent *s_hp;
#ifdef linux
	struct in_addr_solaris **pptr;
#else
	struct in_addr **pptr;
#endif

#ifdef PNM_STANDALONE
	struct hostent *c_hp;
	char src_host[LINELEN];
	struct in_addr **addr;
#endif
	int rc;
	nodeid_t nodeid;

	/* First check if the connection is still alive */
	if (s_global != -1)
		return (s_global);

	s_v4 = socket(AF_INET, SOCK_STREAM, 0);
	if (s_v4 < 0) {
		log_syserr_lib("socket failed.");
		return (-1);
	}

	bzero((char *)&sin, sizeof (sin));
	bzero((char *)&clin, sizeof (clin));
	sin.sin_family = AF_INET;
	clin.sin_family = AF_INET;

	/*
	 * Need to bind to a privileged port. For non-root, this
	 * will fail. pnmd verifies that only commands coming
	 * from priveleged ports succeed so that the ordinary user
	 * can't issue libpnm commands. The following option means that the
	 * client will use a port number from 1024 - 0 and will bind to any
	 * port number which is available in that range (the kernel will do
	 * the looping to choose a free port in that range).
	 */

	/* We are going to bind to a privileged port using TCP_ANONPRIVBIND */
	clin.sin_port = htons(0);
	addrlen = sizeof (struct sockaddr_in);

	/*
	 * First get the server port number from /etc/services if possible
	 * First we are going to try with one server port number
	 * (/etc/services) and then we will try the second (hard-coded) if we
	 * fail.
	 */
	if ((sp = getservbyname("pnmd", "tcp")) == NULL) {
		/*
		 * This is only a debug message. We use syslog instead of
		 * sc_syslog because sc_syslog does not have a "DEBUG" option.
		 * Also we assume that openlog has already been called and we
		 * don't need to call openlog.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG, "getservbyname failed.");
		sin.sin_port = htons(PNMD_PORT);
	} else
		sin.sin_port = (in_port_t)sp->s_port; /* in network order */

	/*
	 * We will let the kernel set up the source address of the connection
	 * to the pnmd. In pnmd we check to see if the connection is from one of
	 * the cluster nodes by checking that the source address belongs to
	 * one of the clutser interconnects.
	 *
	 * In order to be able to unit test pnmd without the whole Sun Cluster
	 * environment, a standalone version can be built to remove scha/rgm
	 * dependencies. Instead of using the cluster private interconnect to
	 * talk with the pnmd, we are using the public network IP address,
	 * corresponding the the hostname.
	 */
#ifdef PNM_STANDALONE
	(void) gethostname(src_host, LINELEN);
	c_hp = gethostbyname(src_host);
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG, "client host: %s\n", src_host);

#ifdef linux
	pptr = (struct in_addr_solaris **)c_hp->h_addr_list;
#else
	pptr = (struct in_addr **)c_hp->h_addr_list;
#endif

	addr = (struct in_addr **)pptr;
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG, "client address: %s\n",
		inet_ntoa(**addr));
	(void) memcpy(&clin.sin_addr, *pptr, sizeof (struct in_addr));

#else
	clin.sin_addr.s_addr = INADDR_ANY;

	/*
	 * If the destination is not the localhost get the destination's
	 * private interconnect name
	 */
	if (strcmp(host, "localhost")) {
		if (iseuropa &&
		    (farm_cfg_getnodeid(host, &nodeid) == 0)) {
			rc = farm_cfg_getaddr(nodeid, &sin.sin_addr);
			if (rc != 0) {
				log_syserr_lib("Failed to get the "
				    "IP address for nodeid %d", nodeid);
				(void) close(s_v4);
				return (-1);
			}
		} else {
			/*
			 * Get the privatehostname of the
			 * destination host node
			 */
			nodeid = dl_clconf_cluster_get_nodeid_by_nodename(
			    host);
			if (nodeid == NODEID_UNKNOWN) {
				log_syserr_lib("Failed to retrieve nodeid "
				    "for %s.", host);
				(void) close(s_v4);
				return (-1);
			}
			if (dl_clconf_get_user_ipaddr(nodeid, &sin.sin_addr)
			    != 0) {
				(void) close(s_v4);
				return (-1);
			}
		}
	} else {
		if ((s_hp = gethostbyname("localhost")) == NULL) {
			log_syserr_lib("gethostbyname failed for localhost");
			(void) close(s_v4);
			return (-1);
		}
#ifdef linux
		pptr = (struct in_addr_solaris **)s_hp->h_addr_list;
#else
		pptr = (struct in_addr **)s_hp->h_addr_list;
#endif
		(void) memcpy(&sin.sin_addr, *pptr, sizeof (struct in_addr));
	}

	syslog(LOG_DEBUG, "client address: %s\n", inet_ntoa(clin.sin_addr));
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG, "server address: %s\n", inet_ntoa(sin.sin_addr));

#endif /* PNM_STANDALONE */

	/*
	 * Bind the client to the interconnect IP addr - so all packets from
	 * the client will have src addr as the interconnect IP addr - and
	 * src port as privileged.
	 */
	ret = bindresvport(s_v4, (struct sockaddr_in *)&clin);
	if (ret != 0) {
		log_syserr_lib("bind failed.");
		(void) close(s_v4);
		return (-1);
	}

	ret = connect(s_v4, (struct sockaddr *)&sin, addrlen);

	/*
	 * If we could not connect on the PNMD_PORT then return error.
	 * Remember that if sp == NULL then we have already tried to connect
	 * on the PNMD_PORT.
	 */
	if (ret != 0 && !sp) {
		log_err_lib("can't connect to PNMd on %s. (%s)",
			host, strerror(errno));
		(void) close(s_v4);
		return (-1);
	}

	try_again = 0;
	/* We come here if we need to try PNMD_PORT or we were successful */
	do {
		/*
		 * If we could not connect then try PNMD_PORT if we
		 * haven't tried it yet. If ret != 0 then we have not
		 * connected as yet. If sp == NULL then we have already tried
		 * PNMD_PORT. If try_again == 1 then we have to try PNMD_PORT
		 * since the /etc/services port was used by some service
		 * which could not return the correct version number.
		 */
		if ((ret != 0 && sp) || try_again) {
			sin.sin_port = htons(PNMD_PORT);
			/* Try again  - this time with a different port */
			ret = connect(s_v4, (struct sockaddr *)&sin, addrlen);

			/* If we get error on connect then return error. */
			if (ret != 0) {
				log_err_lib("can't connect to PNMd on %s.",
				    host);
				(void) close(s_v4);
				return (-1);
			}
		}
		/* Now we are connected - try to get the version number */
		if ((n = write(s_v4, &nop, (size_t)1)) != 1) {
			log_syserr_lib("write error.");
			(void) close(s_v4);
			return (-1);
		}
		/* Read the version number */
		while (len != sizeof (read_version) &&
		    (n = read(s_v4, &buff[len], (size_t)8192)) > 0)
			len += (uint_t)n;

		if (len != sizeof (read_version)) {
			log_syserr_lib("read error.");
			(void) close(s_v4);
			return (-1);
		}
		bcopy(buff, read_version, (size_t)len);
		/*
		 * Check the version number - if required. If version number
		 * does not match and we are using PNMD_PORT then return
		 * error.
		 */
		if (vers_flg &&
		    strcmp(read_version, version) != 0 && (!sp || try_again)) {
			log_err_lib("wrong version of PNMd.");
			(void) close(s_v4);
			return (-1);
		}
		/*
		 * If we are using the port specified in /etc/services then
		 * maybe some other service is running on that port and gave us
		 * a wrong version number so we should try again with the
		 * PNMD_PORT.
		 */
		try_again = 1;
	} /* end of do - while */
	while (vers_flg && strcmp(read_version, version) != 0 && sp);
	return (s_v4);
}
