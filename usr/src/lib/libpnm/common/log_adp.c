/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)log_adp.c	1.12	09/02/18 SMI"

/*
 * The following code implements the ifconfig commands using ioctls:
 * i.e. it will implement commands like plumb a new logical interface etc.
 */

#include <sys/sol_version.h>
#include <ipmp/ipmp_head.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>

#if (SOL_VERSION >= __s10)
#include <zone.h>
#endif


/* multicast target for all nodes */
#define	MULTICAST_TARGET "ff02::1"

static int plumb_log_if(const char *, sa_family_t af);
static int unplumb_log_if(const char *, sa_family_t af);

extern void log_sysdebug_lib(char *fmt, ...);

/*
 * Add a flag for adapter 'name'. Note that if 'value' is negative, the
 * action taken will be to OR out that flag. So -IFF_UP means take out
 * IFF_UP from the flags of the named adapter.
 */
static int
log_adp_set_flags(int s, const char *name, int value)
{
	struct lifreq	lifr;

	(void) memset(&lifr, 0, sizeof (lifr));

	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
	if (ioctl(s, SIOCGLIFFLAGS, (caddr_t)&lifr) < 0) {
		log_sysdebug_lib("SIOCGLIFFLAGS failed.");
		return (-1);
	}

	if (value < 0) {
		value = -value;
		lifr.lifr_flags &= ~((uint_t)value);
	} else
		lifr.lifr_flags |= (uint_t)value;

	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));

	/*lint -e737 */
	if (ioctl(s, SIOCSLIFFLAGS, (caddr_t)&lifr) < 0) {
	/*lint +e737 */
		log_sysdebug_lib("SIOCSLIFFLAGS failed.");
		return (-1);
	}

	return (0);
}

/*
 * Set the zone associated with interface 'name'
 * Doesn't do anything for solaris versions prior to 10.
 */
int
log_adp_setzone(int s, const char *name, const char *zone)
{
#if (SOL_VERSION >= __s10)

	struct lifreq lifr;
	id_t	zid = 0;

	if (zone == NULL) {
		log_syserr_lib("Null input value for zone name.");
		return (-1);
	}

	zid = getzoneidbyname(zone);
	if (zid == -1) {
		log_syserr_lib("zoneid lookup failed.");
		return (-1);
	}

	(void) memset(&lifr, 0, sizeof (lifr));

	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
	lifr.lifr_zoneid = zid;

	if (ioctl(s, SIOCSLIFZONE, (caddr_t)&lifr) < 0) {
		log_syserr_lib("SIOCSLIFZONE failed.");
		return (-1);
	}
	return (0);

#endif
}

int
log_adp_up(int so, const char *name, int af)
{
	if (af == AF_INET6) {
#if SOL_VERSION >= __s9
#else
		/*
		 * If an NDP cache entry exists for the address on the
		 * target interface, setting IFF_UP would fail with
		 * EEXIST. IFF_NOLOCAL overrides this check. We next
		 * clears IFF_NOLOCAL so that IP gets a chance to remove
		 * all NDP entries for the address and properly bring the
		 * interface up.
		 *
		 * Need to do this de-tour only for pre-S9. IP since S9
		 * always clean up the NDP cache when an interface is
		 * marked UP even though the current state is DOWN. In
		 * pre-S9 days, no such clean up is done, hence the
		 * problem described above.
		 *
		 */

		int rc;

		if ((rc = log_adp_set_flags(so, name,
		    IFF_NOLOCAL | IFF_UP | IFF_DEPRECATED)) != 0)
			return (rc);

		return (log_adp_set_flags(so, name, -IFF_NOLOCAL));
#endif
	}
	return (log_adp_set_flags(so, name, IFF_UP | IFF_DEPRECATED));
}

int
log_adp_down(int so, const char *name)
{
	return (log_adp_set_flags(so, name, -IFF_UP));
}

/*
 * Unplumb logical interface
 *	1) OR out IFF_UP from flags
 * 	2) set address to 0
 * This should conform to all or none semantics.
 */
int
log_adp_unplumb(int s, const char *name, sa_family_t af)
{
	struct lifreq	lifr;
	int		rcode;

	(void) memset(&lifr, 0, sizeof (lifr));
	lifr.lifr_addr.ss_family = af;

	/*
	 * Set flags to DOWN before setting addr to 0, so that this
	 * deleted entry won't show up in ifconfig(1M) output.
	 */
	if ((rcode = log_adp_set_flags(s, name, -IFF_UP)) !=  0) {
		return (rcode);
	}

	/* Set IP address to 0.0.0.0/::0 */
	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
	/*lint -e737 */
	if (ioctl(s, SIOCSLIFADDR, (caddr_t)&lifr) < 0) {
	/*lint +e737 */
		log_sysdebug_lib("SIOCSLIFADDR failed.");
		return (-1);
	}

	/* Remove the streams */
	if ((rcode = unplumb_log_if(name, af)) != 0) {
		return (rcode);
	}

	return (0);
}

/*
 * In Solaris 2.8, we have to explicitly plumb and unplumb a logical
 * interface with an ioctl. These new functions are called from
 * log_adp_plumb() and log_adp_unplumb() for this purpose.
 * Like all system calls, we use int to indicate success/failure.
 * Returns 0 for success, -1 for ioctl() failures and -2 for all
 * other errors.
 * These should only be called for logical adapters and not for physical
 * adapters.
 */

int
plumb_log_if(const char *ifname, sa_family_t af)
{
	int fd = 0;
	struct lifreq lifr;

	if (af == AF_INET)
		fd = open("/dev/ip", O_RDWR);
	else if (af == AF_INET6)
		fd = open("/dev/ip6", O_RDWR);

	if (fd == -1) {
		log_syserr_lib("open failed.");
		return (-2);
	}

	/* This adds 0.0.0.0/::0 as a logical IP address to the adapter */
	bzero(&lifr, sizeof (lifr));
	(void) strcpy(lifr.lifr_name, ifname);
	if (ioctl(fd, SIOCLIFADDIF, &lifr) == -1) {
		log_sysdebug_lib("SIOCLIFADDIF failed.");
		(void) close(fd);
		return (-1);
	}

	(void) close(fd);
	return (0);
}

int
unplumb_log_if(const char *ifname, sa_family_t af)
{
	int fd = 0;
	struct lifreq lifr;

	if (af == AF_INET)
		fd = open("/dev/ip", O_RDWR);
	else if (af == AF_INET6)
		fd = open("/dev/ip6", O_RDWR);

	if (fd == -1) {
		log_syserr_lib("open failed.");
		return (-2);
	}

	/* This removes the logical IP address */
	bzero(&lifr, sizeof (lifr));
	(void) strcpy(lifr.lifr_name, ifname);

	/*lint -e737 */
	if (ioctl(fd, SIOCLIFREMOVEIF, &lifr) == -1) {
	/*lint +e737 */
		log_sysdebug_lib("SIOCLIFREMOVEIF failed.");
		(void) close(fd);
		return (-1);
	}

	(void) close(fd);
	return (0);
}

int
log_adp_plumb_v4(int s, const char *name, struct in_addr *ip, const char *zone)
{
	struct lifreq		lifr, lifrmask;
	struct sockaddr_in 	sinbuf, *sin, mask, *netmask = NULL;
	char abuf[INET6_ADDRSTRLEN];
	struct in_addr	addr;
	int 	rcode;
	id_t 	zid = 0;

	/* Make sure we're dealing with a logical interface */
	if (strchr(name, ':') == NULL)
		return (-2);

	/* Make sure we're not setting broadcast addresses */
	(void) inet_pton(AF_INET, "255.255.255.255", &addr);
	if (ip->s_addr == addr.s_addr)
		return (-2);

	/* Plumb the logical address of 0.0.0.0 */
	if ((rcode = plumb_log_if(name, AF_INET)) != 0)
		return (rcode);

	(void) memset(&lifr, 0, sizeof (lifr));

	/*
	 * Copy IP address into the ioctl structure lifr.
	 */
	sin = &sinbuf;
	sin->sin_family = AF_INET;
	sin->sin_addr.s_addr = ip->s_addr;
	bcopy((struct sockaddr_storage *)sin, &lifr.lifr_addr,
	    sizeof (lifr.lifr_addr));
	lifr.lifr_addr.ss_family = AF_INET;

	/*
	 * getnetmaskbyaddr() could fail, in which case we will
	 * let the kernel assign the default netmask.
	 */
	if (getnetmaskbyaddr(sin->sin_addr, &mask.sin_addr) == 0)
		netmask = &mask;

	if (netmask != NULL) {
		(void) inet_ntop(AF_INET, &(netmask->sin_addr), abuf,
		    sizeof (abuf));
		netmask->sin_family = AF_INET;
		bcopy((struct sockaddr_storage *)netmask, &lifrmask.lifr_addr,
		    sizeof (lifrmask.lifr_addr));
		(void) strncpy(lifrmask.lifr_name, name,
		    sizeof (lifrmask.lifr_name));
		lifrmask.lifr_addr.ss_family = AF_INET;
		/*lint -e737 */
		if (ioctl(s, SIOCSLIFNETMASK, (caddr_t)&lifrmask) < 0) {
		/*lint +e737 */
			log_sysdebug_lib("SIOCSLIFNETMASK failed.");
			goto err;
		}
	}

	/* Set IP address of logical interface. */
	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
	/*lint -e737 */
	if (ioctl(s, SIOCSLIFADDR, (caddr_t)&lifr) < 0) {
	/*lint +e737 */
		log_sysdebug_lib("SIOCSLIFADDR failed.");
		goto err;
	}

	/*
	 * No need to set broadcast address -- it's set automatically when
	 * we set up the address above.
	 */
	if (log_adp_set_flags(s, name, -IFF_UP) == -1)
		goto err;

#if (SOL_VERSION >= __s10)

	/* Set the zone for the interface */
	if (zone != NULL) {
		if (log_adp_setzone(s, name, zone) == -1)
			goto err;
	}
#endif

	return (0);

err:
	(void) unplumb_log_if(name, AF_INET);
	return (-1);
}

int
log_adp_plumb_v6(int s, const char *name, struct in6_addr *ip, const char *zone)
{
	struct lifreq		lifr, lifrmask;
	struct sockaddr_in6 	sinbuf6, *sin6, netmask;
	int			rcode;
	id_t 			zid = 0;

	/* Make sure we're dealing with logical interface */
	if (strchr(name, ':') == NULL)
		return (-1);

	/* Plumb the logical address of ::0 */
	if ((rcode = plumb_log_if(name, AF_INET6)) != 0)
		return (rcode);

	(void) memset(&lifr, 0, sizeof (lifr));

	/*
	 * Copy IP address into the ioctl structure lifr.
	 */
	sin6 = &sinbuf6;
	sin6->sin6_family = AF_INET6;
	bcopy(ip, &(sin6->sin6_addr), CL_IPV6_ADDR_LEN);
	bcopy((struct sockaddr_storage *)sin6, &lifr.lifr_addr,
	    sizeof (lifr.lifr_addr));
	lifr.lifr_addr.ss_family = AF_INET6;

	/* Set the netmask to be 64 bits */
	netmask.sin6_family = AF_INET6;
	(void) inet_pton(AF_INET6, "FFFF:FFFF:FFFF:FFFF::0",
	    &(netmask.sin6_addr));
	bcopy((struct sockaddr_storage *)&netmask, &(lifrmask.lifr_addr),
	    sizeof (lifrmask.lifr_addr));
	(void) strncpy(lifrmask.lifr_name, name, sizeof (lifrmask.lifr_name));
	lifrmask.lifr_addr.ss_family = AF_INET6;
	/*lint -e737 */
	if (ioctl(s, SIOCSLIFNETMASK, (caddr_t)&lifrmask) < 0) {
	/*lint +e737 */
		log_sysdebug_lib("SIOCSLIFNETMASK failed.");
		goto err;
	}

	/* Set IP address of logical interface. */
	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
	/*lint -e737 */
	if (ioctl(s, SIOCSLIFADDR, (caddr_t)&lifr) < 0) {
	/*lint +e737 */
		log_sysdebug_lib("SIOCSLIFADDR failed.");
		goto err;
	}

	if (log_adp_set_flags(s, name, -IFF_UP) == -1)
		goto err;

#if (SOL_VERSION >= __s10)

	/* Set the zone associated with this interface */
	if (zone != NULL) {
		if (log_adp_setzone(s, name, zone) == -1)
			goto err;
	}

#endif
	return (0);

err:
	(void) unplumb_log_if(name, AF_INET6);
	return (-1);
}

/*
 * sends out an unsolicited neighbor advertisement for the given
 * IPv6 address, adapter and open raw socket
 * 0 = success; -1 = ioctl() failure ; -2 = other failure
 */
int
send_una(int s, char *adp, struct in6_addr ip)
{
	char buffer[256];
	nd_neighbor_advert_t *una;
	nd_opt_hdr_t *ndo;
	int hops = 255;	/* required by RFC 2461 for unsolicited neighbor adv */
	uint_t plen;
	struct sockaddr_in6 *sin6, target, myaddr;
	struct lifreq lifr;
	int ifindex;

	bzero(buffer, sizeof (buffer));
	una = (nd_neighbor_advert_t *)buffer;
	ndo = (nd_opt_hdr_t *)(una + 1);

	/* our address, to be used as source address in the packet */
	bzero(&myaddr, sizeof (myaddr));
	myaddr.sin6_family = AF_INET6;
	myaddr.sin6_addr = ip;

	/* multicast target, to be used as destination of our ip6 packet */
	bzero(&target, sizeof (target));
	target.sin6_family = AF_INET6;
	if (inet_pton(AF_INET6, MULTICAST_TARGET, &target.sin6_addr) != 1) {
		log_syserr_lib("inet_pton failed.");
		return (-2);
	}

	/* setup data structure for unsolicited advertisement */
	una->nd_na_type = ND_NEIGHBOR_ADVERT;
	una->nd_na_code = 0;
	una->nd_na_target = myaddr.sin6_addr;
	una->nd_na_cksum = 0;

	/*
	 * The flag is a combination that specifies that:
	 * a) this node is not a router (SC nodes can not be routers)
	 * b) this advertisement is unsolicited
	 * c) recipients should update the link-layer address immediately
	 */
	una->nd_na_flags_reserved =
	    ~ND_NA_FLAG_ROUTER & ~ND_NA_FLAG_SOLICITED & ND_NA_FLAG_OVERRIDE;

	ndo->nd_opt_type = ND_OPT_TARGET_LINKADDR;

	(void) strncpy(lifr.lifr_name, adp, sizeof (lifr.lifr_name));
	if (ioctl(s, SIOCGLIFINDEX, (char *)&lifr) < 0) {
		log_sysdebug_lib("SIOCGLIFINDEX failed.");
		return (-1);
	}
	ifindex = lifr.lifr_index;
#ifdef	IPV6_BOUND_PIF
	if (setsockopt(s, IPPROTO_IPV6, IPV6_BOUND_PIF, (char *)&ifindex,
	    sizeof (ifindex)) < 0) {
		log_syserr_lib("IPV6_BOUND_PIF failed.");
		return (-2);
	}
#else
	if (setsockopt(s, IPPROTO_IPV6, IPV6_BOUND_IF, (char *)&ifindex,
	    sizeof (ifindex)) < 0) {
		log_syserr_lib("IPV6_BOUND_IF failed.");
		return (-2);
	}
#endif

	if (connect(s, (struct sockaddr *)&target, sizeof (target)) < 0) {
		log_syserr_lib("connect failed.");
		return (-2);
	}

	if (setsockopt(s, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &hops,
	    sizeof (hops)) < 0) {
		log_syserr_lib("setsockopt failed");
		return (-2);
	}

	/* find out the hardware address where this ip is hosted */
	bzero(&lifr, sizeof (lifr));
	sin6 = (struct sockaddr_in6 *)&lifr.lifr_nd.lnr_addr;
	bzero(sin6, sizeof (struct sockaddr_in6));
	sin6->sin6_family = AF_INET6;
	sin6->sin6_addr = una->nd_na_target;

	(void) strncpy(lifr.lifr_name, adp, sizeof (lifr.lifr_name));
	if (ioctl(s, SIOCLIFGETND, (char *)&lifr) < 0) {
		log_sysdebug_lib("SIOCLIFGETND failed.");
		return (-1);
	}

	if (lifr.lifr_nd.lnr_hdw_len != 0) {
		/*lint -e419 */
		bcopy((char *)lifr.lifr_nd.lnr_hdw_addr,
		(char *)(ndo + 1), (uint_t)lifr.lifr_nd.lnr_hdw_len);
		/*lint +e419 */
	}

	/* Length is specified in units of 8 octets */
	ndo->nd_opt_len = (uint8_t)(lifr.lifr_nd.lnr_hdw_len + 7) / 8;

	plen = sizeof (nd_neighbor_advert_t) + sizeof (nd_opt_hdr_t)
	    + (uint_t)(ndo->nd_opt_len * 8);

	if (send(s, una, plen, 0) != (int)plen) {
		log_syserr_lib("send failed.");
		return (-2);
	}
	return (0);
}
