/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _PNM_NETGROUP_H
#define	_PNM_NETGROUP_H

#pragma ident	"@(#)pnm_network.h	1.7	09/02/18 SMI"

/*
 * pnmd_network.h - Public network group management header file.
 *
 * Provide the PNM groups data structure and all the functions needed by
 * libpnm module to manage the public network groups. It provides also an
 * abstraction layer between the libpnm and the underlying kernel/networking
 * component it is running on. This module is OS specific and must be
 * implemented for each operating system supported.
 */

#ifdef __cplusplus
extern "C" {
#endif

#define	ADP_OK		0	/* adapter is ok */
#define	ADP_FAULTY	1	/* adapter is dead IFF_FAILED = 1 */
#define	ADP_STANDBY	2	/* adapter is STANDBY and INACTIVE */
#define	ADP_OFFLINE	3	/* adapter is OFFLINE */
#define	ADP_NOTRUN	4	/* adapter has no carrier, IFF_RUNNING = 0 */


/*
 * If Clearview specific flags exist, use them.
 */

/*
 * LIBPNM_LIFC_FLAGS_INIT defines the use of LIFC_ALLZONES
 * and LIFC_UNDER_IPMP flags. Use LIFC_ALLZONES flag in the
 * ioctl to retrieve the adapter instances that are plumbed
 * for zones. This flag is available from Solaris 10 release.
 * Use LIFC_UNDER_IPMP flag to retrieve the interfaces under
 * IPMP. This flag is available in Solaris release with
 * Clearview project.
 */
#ifdef	LIFC_UNDER_IPMP
#define	LIBPNM_LIFC_FLAGS_INIT	(LIFC_UNDER_IPMP | LIFC_ALLZONES)
#else
#ifdef	LIFC_ALLZONES
#define	LIBPNM_LIFC_FLAGS_INIT	(LIFC_ALLZONES)
#else
#define	LIBPNM_LIFC_FLAGS_INIT	0
#endif
#endif

typedef struct log_addr {
	struct in6_addr		ipaddr;
	char			*name; /* adp name eg: hme0:1 */
	uint64_t		flags; /* flags of the logical Ip */
	uint_t			inst;
	struct log_addr		*next; /* next l_addr pointer */
}	l_addr;

typedef struct adp_inst {
	uint_t		nLpi; /* No. of Logical Ip addresses */
}	adp_inst;

typedef struct bkg_adp {
	char		*adp; /* eg: hme0 */
	int		state; /* NOTRUNNING, FAULTY, STANDBY, OFFLINE, OK */
	adp_inst	*pii_v4; /* pointer to v4 instance */
	adp_inst	*pii_v6; /* pointer to v6 instance */
	struct bkg_adp	*next; /* pointer to next bkg_adp */
}	bkg_adp;

typedef struct bkg_status {	/* struct for ipmp group */
	char			*name; /* ipmp group name - SIOCLIFGROUPNAME */
	bkg_adp			*head; /* pointer to the adapter list */
	struct bkg_status	*next; /* pointer to next bkg_status group */
}	bkg_status;

extern int nw_group_config_read(bkg_status **);
extern void nw_group_config_free(bkg_status *);
extern bkg_status *nw_group_find(char *, bkg_status *);
extern int nw_group_status(bkg_status *);
extern int nw_group_instances(bkg_status *, int *);

extern int nw_ifconfig_up(bkg_status *, uint_t, struct in6_addr *);
extern int nw_ifconfig_down(uint_t, struct in6_addr *);
extern int nw_ifconfig_plumb(bkg_status *, const char *, uint_t,
    struct in6_addr *);
extern int nw_ifconfig_unplumb(uint_t, struct in6_addr *);

extern char *nw_disp_group_name(void);

#ifdef __cplusplus
}
#endif

#endif	/* _PNM_NETGROUP_H */
