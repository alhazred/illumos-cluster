/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnm_network_solaris_ipmp.c	1.10	09/02/18 SMI"

/*
 * pnmd_network_solaris_ipmp.c - Public network group management module.
 *
 * Provide the PNM groups data structure and all the functions needed by
 * libpnm module to manage the public network groups. It provides also an
 * abstraction layer between the libpnm and the underlying kernel/networking
 * component it is running on.
 */

#include <sys/sol_version.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <pnm/pnm_ld.h>
#include <pnm_network.h>

#if (SOL_VERSION >= __s10)
#include <zone.h>
#endif

/* multicast target for all nodes */
#define	MULTICAST_TARGET "ff02::1"

static l_addr *nw_adp_logip_get(char *adp, sa_family_t af);
static uint_t nw_adp_logip_num_get(char *adp, sa_family_t af);
static l_addr *nw_group_logip_get(bkg_status *grp, sa_family_t af);
static uint_t nw_group_logip_num_get(bkg_adp *adp, sa_family_t af);

static bkg_adp *nw_adp_alloc();
static bkg_adp *nw_adp_find(char *name, bkg_status *grp);
static void nw_adp_append(bkg_status *grp, bkg_adp *adp);
static void nw_adp_delete(bkg_status *grp, bkg_adp *adp);
static void nw_adp_free(bkg_adp *adp);
static void nw_adp_inst_free(adp_inst *adpi);
static bkg_status * nw_group_add(bkg_status **grp_list);
static void nw_group_append(bkg_status **grp_list, bkg_status *grp);
static void nw_group_free(bkg_status *grp);
static void nw_group_logip_free(l_addr *list);

static int nw_adp_set_flags(int so, const char *name, int value);
#if (SOL_VERSION >= __s10)
static int nw_adp_set_zone(int so, const char *name, const char *zone);
#endif
static int nw_plumb_if(const char *ifname, sa_family_t af);
static int nw_unplumb_if(const char *ifname, sa_family_t af);
static int nw_ipv6_send_una(int so, char *adp, struct in6_addr ip);
static int nw_adp_up(int so, const char *name, sa_family_t af);
static int nw_adp_down(int so, const char *name);
static int nw_adp_plumb_v4(int so, const char *name, struct in_addr *ip,
    const char *zone);
static int nw_adp_plumb_v6(int so, const char *name, struct in6_addr *ip,
    const char *zone);
static int nw_adp_unplumb(int so, const char *name, sa_family_t af);

/* from solaris lib socket */
extern int getnetmaskbyaddr(const struct in_addr, struct in_addr *);

/*
 * Get all IP addresses hosted on this adapter instance - both physical and
 * logical IP addresses. If the adapter instance is NULL then get all the
 * logical IP addresses (not loopback) on this node. Input is physical adp
 * name i.e. hme0. Returns a list of l_addrs. On error returns NULL. Get the IP
 * addresses for the given family.
 * INPUT: pointer to adp name or NULL, family(v4/v6)
 * RETURN: pointer to a list of l_addrs structures, NULL on error.
 */
static l_addr *
nw_adp_logip_get(char *adp, sa_family_t af)
{
	uint_t			n, lif_cnt, phys_name_len = 0;
	int			so, try = 0;
	char			*name, *buf = NULL;
	struct lifconf		lifc;
	struct lifreq		*lifr;
	l_addr			*ptr, *last_ptr, *head_ptr;
	struct lifnum		lifn;
	struct in6_addr		addr;
	struct sockaddr_in6	*sin6;
	struct sockaddr_in	*sin;
	struct lifreq		lifrflag;

	ptr = last_ptr = head_ptr = NULL;
	if ((so = socket(af, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		goto nw_adp_logip_error;
	}

retry:
	/*
	 * We use SIOCGLIFNUM here. Since new interfaces may be
	 * configured after SIOCGLIFNUM, and we would get an error from
	 * SIOCGLIFCONF (EINVAL) we will try this twice. Initially we will
	 * allocate some extra buffer space.
	 */
	try++;
	lifn.lifn_family = af;
	lifn.lifn_flags = LIBPNM_LIFC_FLAGS_INIT;

	if (ioctl(so, SIOCGLIFNUM, (char *)&lifn) < 0) {
		log_syserr_lib("SIOCGLIFNUM failed.");
		goto nw_adp_logip_error;
	}
	lif_cnt = (uint_t)(lifn.lifn_count + 1);

	/*
	 * Retrieve i/f configuration of machine i.e. all the networks
	 * it is connected to. Free buf if required.
	 */
	if (buf != NULL)
		free(buf);
	buf = (char *)malloc(lif_cnt * sizeof (struct lifreq));
	if (buf == NULL) {
		log_syserr_lib("out of memory.");
		goto nw_adp_logip_error;
	}

	(void) memset(&lifc, 0, sizeof (lifc));
	lifc.lifc_len = (int)lif_cnt * (int)sizeof (struct lifreq);
	lifc.lifc_buf = buf;
	lifc.lifc_flags = LIBPNM_LIFC_FLAGS_INIT;

	lifc.lifc_family = af; /* Get it only for this family */
	if (ioctl(so, SIOCGLIFCONF, (char *)&lifc) < 0) {
		/*
		 * EINVAL is commonly encountered, when things change
		 * underneath us rapidly, (eg. at boot, when new interfaces
		 * are plumbed successively) and the kernel finds the buffer
		 * size we passed as too small. We will retry twice.
		 */
		/* CSTYLED */
		if (errno == EINVAL && try < 2) /*lint !e746 */
			goto retry;
		log_syserr_lib("SIOCGLIFCONF failed.");
		goto nw_adp_logip_error;
	}

	/* For each network i/f on machine ... */
	lifr = (struct lifreq *)lifc.lifc_req;
	for (n = 0; n < (uint_t)lifc.lifc_len / sizeof (struct lifreq); n++,
		lifr++) {
		name = lifr->lifr_name;
		assert(af == lifr->lifr_addr.ss_family);

		/*
		 * Get the length of the physical adapter
		 * eg for hme0 it will be 3
		 */
		phys_name_len = strcspn(name, ":");
		assert(phys_name_len != 0);

		/*
		 * If adp is NULL then we will get all the logical IPs.
		 * If the name of the i/f is not the same as the name of
		 * adp i.e. hme0 = hme0. So we are only concerned with i/f
		 * which have the same name as adp.
		 */
		if ((adp) &&
			(phys_name_len != strlen(adp) ||
			strncmp(adp, name, phys_name_len) != 0))
			continue;

		/* Also if the name of the adp is lo0:x then we do not care. */
		if (strncmp(name, "lo0", phys_name_len) == 0)
			continue;

		/*
		 * Retrieve the flags set on this IP address
		 * (physical or logical).
		 */
		(void) memset(&lifrflag, 0, sizeof (lifrflag));
		(void) strncpy(lifrflag.lifr_name, name,
			sizeof (lifrflag.lifr_name));

		/*
		 * Ignore this interface on ENXIO and continue getting
		 * the flags for the rest of the interfaces. Its possible
		 * the interface got removed after we got the list of
		 * interfaces - so we just ignore this interface silently.
		 */
		if (ioctl(so, SIOCGLIFFLAGS, (char *)&lifrflag) < 0) {
			/* CSTYLED */
			if (errno == ENXIO) /*lint !e746 */
				continue;	/* ignore this interface */
			log_syserr_lib("SIOCGLIFFLAGS failed.");
			goto nw_adp_logip_error;
		}

		/*
		 * If the interface is an IPMP psuedo-interface,
		 * ignore it and continue to get flags for other
		 * interfaces.
		 */

		ptr = (l_addr *) malloc(sizeof (l_addr));
		if (ptr == NULL) {
			log_syserr_lib("out of memory.");
			goto nw_adp_logip_error;
		}
		bzero(ptr, sizeof (l_addr));

		/* Append l_addr structure to list */
		if (head_ptr == NULL)
			head_ptr = ptr;
		if (last_ptr != NULL)
			last_ptr->next = ptr;
		last_ptr = ptr;

		if (af == AF_INET) {	/* v4 */
			sin = (struct sockaddr_in *)&lifr->lifr_addr;
			IN6_INADDR_TO_V4MAPPED(&sin->sin_addr, &(ptr->ipaddr));
		} else {	/* v6 */
			sin6 = (struct sockaddr_in6 *)&lifr->lifr_addr;
			addr = sin6->sin6_addr;	/* structure copy */
			bcopy(&addr, &(ptr->ipaddr), CL_IPV6_ADDR_LEN);
		}

		/* Save the flags of the logical IP address */
		ptr->flags = lifrflag.lifr_flags;

		/* Store the name of the adp also */
		ptr->name = strdup(name);
		if (ptr->name == NULL) {
			log_syserr_lib("out of memory.");
			goto nw_adp_logip_error;
		}

		ptr->inst = get_logical_inst(name);
	}

	(void) close(so);
	free(buf);

	return (head_ptr);

nw_adp_logip_error:
	/* Error cleanup: free logic_ip list and return NULL */

	if (so > 0)
		(void) close(so);

	if (buf != NULL)
		free(buf);

	nw_group_logip_free(head_ptr);

	return (NULL);
}

/*
 * Get the number of logical ip addresses hosted on this adapter instance
 * Returns an int. If there are no logical addresses hosted on the adp then
 * it returns zero as expected. Input is physical adp name. i.e. hme0.
 * INPUT: pointer to adp name, family(v4/v6), pointer to lifconf
 * RETURN: number of ip addresses hosted on this adp.
 */
static uint_t
nw_adp_logip_num_get(char *adp, sa_family_t af)
{
	uint_t			count = 0;
	uint_t			n;
	uint_t			lif_cnt;
	uint_t			phys_name_len;
	int			so = 0;
	char			*name;
	char			*buf = NULL;
	struct lifconf		lifc;
	struct lifreq		*lifr;
	struct lifnum		lifn;
	int			try = 0;
	uint64_t		flags;
	struct lifreq		lifrflag;

	if ((so = socket(af, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		goto nw_adp_logip_num_error;
	}

retry:
	/*
	 * We use SIOCGLIFNUM here. Since new interfaces may be
	 * configured after SIOCGLIFNUM, and we would get an error from
	 * SIOCGLIFCONF (EINVAL) we will try this twice. Initially we will
	 * allocate some extra buffer space.
	 */
	try++;
	lifn.lifn_family = af;
	lifn.lifn_flags = LIBPNM_LIFC_FLAGS_INIT;

	if (ioctl(so, SIOCGLIFNUM, (char *)&lifn) < 0) {
		log_syserr_lib("SIOCGLIFNUM failed.");
		goto nw_adp_logip_num_error;
	}
	lif_cnt = (uint_t)(lifn.lifn_count + 1);

	/*
	 * Retrieve i/f configuration of machine i.e. all the networks
	 * it is connected to. Free buf if required.
	 */
	if (buf != NULL)
		free(buf);
	buf = (char *)malloc(lif_cnt * sizeof (struct lifreq));
	if (buf == NULL) {
		log_syserr_lib("out of memory.");
		goto nw_adp_logip_num_error;
	}

	(void) memset(&lifc, 0, sizeof (lifc));
	lifc.lifc_len = (int)(lif_cnt * sizeof (struct lifreq));
	lifc.lifc_buf = buf;
	lifc.lifc_family = af; /* Get it only for this family */
	lifc.lifc_flags = LIBPNM_LIFC_FLAGS_INIT;

	if (ioctl(so, SIOCGLIFCONF, (char *)&lifc) < 0) {

		/*
		 * EINVAL is commonly encountered, when things change
		 * underneath us rapidly, (eg. at boot, when new interfaces
		 * are plumbed successively) and the kernel finds the buffer
		 * size we passed as too small. We will retry twice.
		 */
		if (errno == EINVAL && try < 2)
			goto retry;
		log_syserr_lib("SIOCGLIFCONF failed.");
		goto nw_adp_logip_num_error;
	}

	/*
	 * For each network i/f on machine ...
	 */
	lifr = (struct lifreq *)lifc.lifc_req;
	for (n = 0; n < (uint_t)lifc.lifc_len / sizeof (struct lifreq);
		n++, lifr++) {
		if (af != lifr->lifr_addr.ss_family)
			continue;
		name = lifr->lifr_name;

		/*
		 * If the name of the i/f is not the same as the name of
		 * adp i.e. hme0 = hme0. So we are only concerned with i/f
		 * which have the same name as adp.
		 */
		if ((phys_name_len = strcspn(name, ":")) != strlen(adp) ||
			strncmp(adp, name, phys_name_len) != 0)
			continue;
		count++;
	}

	(void) close(so);
	free(buf);
	return (count);

nw_adp_logip_num_error:

	if (so > 0)
		(void) close(so);
	if (buf != NULL)
		free(buf);
	return (0);
}

/*
 * Get all IP addresses hosted on all the adapter instances - both physical
 * and logical IP addresses in this group. If group name is NULL then get all
 * the logical IP addresses (not loopback) hosted on this node. Returns a list
 * of l_addrs. On error returns NULL. Get the IP addresses for the given family.
 * INPUT: pointer to group or NULL, family(v4/v6)
 * RETURN: pointer to a list of l_addrs structures, NULL on error.
 */
static l_addr *
nw_group_logip_get(bkg_status *grp, sa_family_t af)
{
	l_addr			*ptr, *last_ptr, *head_ptr;
	bkg_adp			*adp;

	ptr = last_ptr = head_ptr = NULL;

	/* If the group is NULL get all logical IPs on this node */
	if (!grp)
		return (nw_adp_logip_get(NULL, af));

	/* For all the adapters in this group */
	for (adp = grp->head; adp; adp = adp->next) {
		head_ptr = nw_adp_logip_get(adp->adp, af);
		if (ptr) {
			/* Goto the last laddr struct in this list */
			for (last_ptr = ptr; last_ptr->next; ) {
				last_ptr = last_ptr->next;
			}
			last_ptr->next = head_ptr;
		}
		else
			ptr = head_ptr;
	}
	return (ptr);
}

/*
 * Return the number of Logical IP addresses (either v4/v6) of the adp.
 * INPUT: pointer to the adp and family.
 * RETURN: number of Lpi.
 */
static uint_t
nw_group_logip_num_get(bkg_adp *adp, sa_family_t af)
{
	uint_t min = 0;

	if (af == AF_INET && adp->pii_v4)
		min = adp->pii_v4->nLpi;

	if (af == AF_INET6 && adp->pii_v6)
		min = adp->pii_v6->nLpi;
	return (min);
}


/*
 * Allocate memory for adp and zero it.
 * INPUT: NULL
 * RETURN: pointer to bkg_adp or NULL on error.
 */
static bkg_adp *
nw_adp_alloc()
{
	bkg_adp	*ptr;

	ptr = (bkg_adp *) malloc(sizeof (bkg_adp));
	if (ptr == NULL) {
		log_syserr_lib("out of memory.");
		return (NULL);
	}
	bzero(ptr, sizeof (bkg_adp));
	return (ptr);
}

/*
 * Find the adp corresponding to the adp->adp eg: hme0.
 * INPUT: char pointer to adp name, and pointer to group.
 * RETURN: pointer to struct bkg_adp, NULL if not found.
 */
static bkg_adp *
nw_adp_find(char *name, bkg_status *bkg)
{
	bkg_adp *adp;

	for (adp = bkg->head; adp != NULL; adp = adp->next) {
		if (strcmp(adp->adp, name) == 0)
			return (adp);
	}
	return (NULL);
}

/*
 * Append the given adp to the given group.
 * INPUT: pointer to bkg_adp struct, pointer to bkg_status struct
 * RETURN: void
 */
static void
nw_adp_append(bkg_status *group, bkg_adp *adp)
{
	if (group->head != NULL)
		adp->next = group->head;
	group->head = adp;
}

/*
 * Free this adp and remove it from the given group's adplist.
 * If this is the last adp in the group then make group->head = NULL;
 * INPUT: pointer to a bkg_adp struct (adp which needs to be removed )
 *	: pointer to a group structure
 * RETURN: void
 */
static void
nw_adp_delete(bkg_status *group, bkg_adp *adp)
{
	bkg_adp *prev;
	bkg_adp *current;

	/* Remove this adp from group->head adplist */
	if (group->head == adp) {
		if (group->head->next)
			group->head = group->head->next;
		else
			group->head = NULL;
	} else {
		for (prev = group->head, current = prev->next;
			current != NULL;
			prev = current, current = current->next) {
			if (current == adp) {
				prev->next = current->next;
				break;
			}
		}
	}
	/* Free the memory associated with this adp structure */
	nw_adp_free(adp);
}

/*
 * Free this adp structure and the pointer also.
 * INPUT: pointer to a bkg_adp struct (adp which needs to be removed )
 * RETURN: void
 */
static void
nw_adp_free(bkg_adp *adp)
{
	/* Free the memory associated with this adp structure */
	free(adp->adp);
	nw_adp_inst_free(adp->pii_v4);
	nw_adp_inst_free(adp->pii_v6);
	free(adp);
}

/*
 * Free this adp_inst structure.
 * INPUT: pointer to a adp_inst struct (adp_inst which needs to be removed )
 * RETURN: void
 */
static void
nw_adp_inst_free(adp_inst *adpi)
{
	if (adpi)
		free(adpi);
}

/*
 * Allocate a group struct and add it to the grouplist obkg.
 * INPUT:  pointer to pointer to bkg_status struct(pointer to grouplist in
 *         which to add the given group, i.e. pointer to nbkg/obkg),
 * RETURN: pointer to group added or NULL on error.
 */
static bkg_status *
nw_group_add(bkg_status **grp_list)
{
	bkg_status	*ptr;

	ptr = (bkg_status *) malloc(sizeof (bkg_status));
	if (ptr == NULL) {
		log_syserr_lib("out of memory.");
		return (NULL);
	}
	bzero((char *)ptr, sizeof (bkg_status));
	nw_group_append(grp_list, ptr);
	return (ptr);
}

/*
 * Append the given group struct to the grouplist obkg.
 * INPUT: pointer to pointer to bkg_status struct(pointer to grouplist in
 *        which to append)
 *        pointer to bkg_status struct (the group to append),
 * RETURN: void
 */
static void
nw_group_append(bkg_status **grp_list, bkg_status *group)
{
	if (*grp_list != NULL)
		group->next = *grp_list;
	*grp_list = group;
}

/*
 * Free this group structure and the pointer also.
 * INPUT: pointer to a bkg_status struct (bkg which needs to be removed )
 * RETURN: void
 */
static void
nw_group_free(bkg_status *group)
{
	bkg_adp *adp;
	bkg_adp *tmp;

	/* Remove the adp list in group */
	for (adp = group->head; adp != NULL; adp = tmp) {
		tmp = adp->next;
		nw_adp_delete(group, adp);
	}
	free(group->name);
	free(group);
}


/*
 * Free the l_addr structures list.
 * INPUT: pointer to l_addr list
 * RETURN: void
 */
void
nw_group_logip_free(l_addr *list)
{
	l_addr	*lp = NULL, *lp_next = NULL;

	for (lp = list; lp; lp = lp_next) {
		lp_next = lp->next;
		free(lp->name);
		free(lp);
	}
}

/*
 * Takes a snapshot of the kernel adapters and form a grouplist. Update
 * the listhead to point to the grouplist head.
 * INPUT: void
 * RETURN: 0 on success or -1 on error.
 */
int
nw_group_config_read(bkg_status **grp_list)
{
	uint_t			n;
	uint_t			lif_cnt; /* no. of ifs */
	int			so_v4 = 0; /* socket descriptor */
	int			so_v6 = 0;
	int			so;
	uint_t			inst;	/* ordinal value of IP */
	char			*name; /* name of adp i.e. hme0 */
	char			*buf = NULL;
	struct lifconf		lifc;
	struct lifreq		lifr, *lifrp = NULL, lifrflg;
	bkg_status		*bgp, *exist_group = NULL;
	sa_family_t		af; /* family, v4/v6 */
	bkg_adp			*ptr, *match_adp;
	int			try = 0;
	struct lifnum		lifn;

	/*
	 * Here we should remember that we need two sockets for doing ioctls
	 * v4 and v6. First we do a SIOCGLIFCONF with AF_UNSPEC on a v4 socket
	 * and then for every instance (v4/v6) we must do the ioctls on the
	 * particular socket..either v4 or v6.
	 */
	/* Get kernel state into nbkg */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		goto nw_group_config_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		goto nw_group_config_error;
	}
retry:
	/*
	 * We use SIOCGLIFNUM here. Since new interfaces may be
	 * configured after SIOCGLIFNUM, and we would get an error from
	 * SIOCGLIFCONF (EINVAL) we will try this twice. Initially we will
	 * allocate some extra buffer space.
	 */
	try++;
	lifn.lifn_family = AF_UNSPEC;
	lifn.lifn_flags = LIBPNM_LIFC_FLAGS_INIT;

	if (ioctl(so_v4, SIOCGLIFNUM, (char *)&lifn) < 0) {
		log_syserr_lib("SIOCGLIFNUM failed.");
		goto nw_group_config_error;
	}
	lif_cnt = (uint_t)(lifn.lifn_count + 1);

	/*
	 * Retrieve i/f configuration of machine i.e. all the networks
	 * it is connected to. Free buf if required.
	 */
	if (buf != NULL)
		free(buf);
	buf = (char *)malloc(lif_cnt * sizeof (struct lifreq));
	if (buf == NULL) {
		log_syserr_lib("out of memory.");
		goto nw_group_config_error;
	}

	(void) memset(&lifc, 0, sizeof (lifc));
	lifc.lifc_family = AF_UNSPEC; /* to get v4 and v6 instances */
	lifc.lifc_len = (int)(lif_cnt * sizeof (struct lifreq));
	lifc.lifc_buf = buf;
	lifc.lifc_flags = LIBPNM_LIFC_FLAGS_INIT;

	if (ioctl(so_v4, SIOCGLIFCONF, (char *)&lifc) < 0) {
		/*
		 * EINVAL is commonly encountered, when things change
		 * underneath us rapidly, (eg. at boot, when new interfaces
		 * are plumbed successively) and the kernel finds the buffer
		 * size we passed as too small. We will retry twice.
		 */
		if (errno == EINVAL && try < 2)
			goto retry;
		log_syserr_lib("SIOCGLIFCONF failed.");
		goto nw_group_config_error;
	}

	/* For each i/f on machine */
	lifrp = (struct lifreq *)lifc.lifc_req;
	for (n = 0; n < (uint_t)lifc.lifc_len / sizeof (struct lifreq);
		n++, lifrp++) {
		af = lifrp->lifr_addr.ss_family;
		so = (af == AF_INET) ? so_v4 : so_v6;
		name = lifrp->lifr_name;

		/*
		 * Returns 0 for physical adp, otherwise returns the
		 * ordinal value of the logical IP. eg. for
		 * tr2, it will return 0, whilst for tr2:3, it
		 * will return 3. get_logical_inst is defined in pnm_util.c.
		 */
		inst = get_logical_inst(name);

		/* We only want to check the physical adapters */
		if (inst != 0)
			continue;

		/* If the adapter is clprivnet or lo0 don't do anything */
		if (strncmp(name, "clprivnet", strlen("clprivnet")) == 0 ||
			strncmp(name, "lo0", strlen("lo0")) == 0)
			continue;

		bzero(&lifr, sizeof (lifr));
		bcopy(name, lifr.lifr_name, strlen(name) + 1);

		/*
		 * Get group name of adapter instance.
		 */
		(void) memset(lifr.lifr_groupname, 0,
			sizeof (lifr.lifr_groupname));
		if (ioctl(so, SIOCGLIFGROUPNAME, (caddr_t)&lifr) < 0) {
			if (errno == ENXIO)	/* interface gone */
				continue;	/* ignore */
			log_syserr_lib("SIOCGLIFGROUPNAME failed.");
			goto nw_group_config_error;
		}

		/* If the adp is not in a group then we don't do anything */
		if (strlen(lifr.lifr_groupname) == 0)
			continue;

		/* Get flags also - state of adp */
		bzero(&lifrflg, sizeof (lifr));
		bcopy(name, lifrflg.lifr_name, strlen(name) + 1);
		if (ioctl(so, SIOCGLIFFLAGS, (char *)&lifrflg) < 0) {
			if (errno == ENXIO)	/* interface gone */
				continue;
			log_syserr_lib("SIOCGLIFFLAGS failed.");
			goto nw_group_config_error;
		}

		/*
		 * If the interface is an IPMP psuedo-interface,
		 * ignore it and continue to get flags for other
		 * interfaces.
		 */
#ifdef	IFF_IPMP
		if (lifrflg.lifr_flags & IFF_IPMP)
			continue;
#endif
		/*
		 * See if this adp is already there in the present grouplist.
		 * match_adp will be the adapter itself.
		 */
		match_adp = NULL;
		for (bgp = *grp_list; bgp; bgp = bgp->next) {
			match_adp = nw_adp_find(name, bgp);
			if (match_adp)
				break;
		}

		/*
		 * Now find the group in the grouplist which matches this
		 * adp's group.
		 */
		exist_group = nw_group_find(lifr.lifr_groupname, *grp_list);

		/* Allocate adp structure if needed */
		if (!match_adp) {
			if ((ptr = nw_adp_alloc()) == NULL)
				goto nw_group_config_error;
			ptr->adp = strdup(name);
			if (ptr->adp == NULL) {
				log_syserr_lib("out of memory.");
				goto nw_group_config_error;
			}
		} else
			ptr = match_adp;

		/*
		 * If the adp's current group does not exist then
		 * we have to allocate a completely new group - bgp.
		 */
		if (!exist_group) {
			/* Create a new group */
			if ((bgp = nw_group_add(grp_list)) == NULL)
				goto nw_group_config_error;
			bgp->name = strdup(lifr.lifr_groupname);
			if (bgp->name == NULL) {
				log_syserr_lib("out of memory.");
				goto nw_group_config_error;
			}
			nw_adp_append(bgp, ptr);
		}

		/*
		 * If the adapter was not found and the group exists then
		 * we need to append it to the group.
		 */
		if (!match_adp && exist_group)
			nw_adp_append(exist_group, ptr);


		/*
		 * Now get the data from the kernel for this adp instance.
		 */
		switch (af) {
		case AF_INET:
			if (!ptr->pii_v4) {
				ptr->pii_v4 = (adp_inst *) malloc(
					sizeof (adp_inst));
				if (ptr->pii_v4 == NULL) {
					log_syserr_lib("out of memory.");
					goto nw_group_config_error;
				}
			}
			/* Get nLpi */
			ptr->pii_v4->nLpi = nw_adp_logip_num_get(name, af);
			break;

		case AF_INET6:
			if (!ptr->pii_v6) {
				ptr->pii_v6 = (adp_inst *) malloc(
					sizeof (adp_inst));
				if (ptr->pii_v6 == NULL) {
					log_syserr_lib("out of memory.");
					goto nw_group_config_error;
				}
			}
			/* Get nLpi */
			ptr->pii_v6->nLpi = nw_adp_logip_num_get(name, af);
			break;

		default:
			/* We could do an assert here */
			log_syserr_lib("wrong address family.");
			goto nw_group_config_error;
		}

		/*
		 * If the IFF_RUNNING flag is off then the adp has no carrier
		 * and has failed - so we mark it NOTRUN.
		 * If the IFF_FAILED flag is on then the adp is faulty.
		 * If the IFF_STANDBY flag is on then the kernel sets the
		 * read only flag IFF_INACTIVE on - and an adp is considered
		 * to be standby only if both the flags are on. If the adp
		 * is standby and the IFF_INACTIVE flag is off then the
		 * adp is actually ok since it can be used for traffic.
		 * The IFF_INACTIVE flag is turned off when the other active
		 * adapters in the IPMP group have failed and this standby
		 * adp is used for traffic. Previously, The INACTIVE flag
		 * was never used without the STANDBY flag also on.
		 * With the fix of 4796820/5084073 INACTIVE flag can be on
		 * alone when initially active adapter in an active/standby
		 * IPMP group gets repaired after failure with no failback
		 * option in /etc/default/mpathd file.
		 * If the IFF_OFFLINE flag is on then the adp is offline.
		 */
		if ((lifrflg.lifr_flags & IFF_RUNNING) == 0)
			ptr->state = ADP_NOTRUN;
		else if (lifrflg.lifr_flags & IFF_FAILED)
			ptr->state = ADP_FAULTY;
		else if (lifrflg.lifr_flags & IFF_INACTIVE)
			ptr->state = ADP_STANDBY;
		else if (lifrflg.lifr_flags & (uint64_t)IFF_OFFLINE)
			ptr->state = ADP_OFFLINE;
		else
			ptr->state = ADP_OK;
	}

	(void) close(so_v4);
	(void) close(so_v6);
	free(buf);
	return (0);

nw_group_config_error:
	nw_group_config_free(*grp_list);

	if (so_v4 > 0)
		(void) close(so_v4);
	if (so_v6 > 0)
		(void) close(so_v6);
	if (buf != NULL)
		free(buf);
	return (-1);
}

/*
 * Free the given grouplist.
 * INPUT : pointer to pointer to bkg_status struct(grouplist)
 * RETURN :void
 */
void
nw_group_config_free(bkg_status *listh)
{
	bkg_status *bkg, *bkg_next;

	/* Free the group list */
	for (bkg = listh; bkg; bkg = bkg_next) {
		bkg_next = bkg->next;
		nw_group_free(bkg);
	}
}

/*
 * Find the group corresponding to the group->name eg: ipmp1.
 * INPUT: char pointer to group name, and pointer to grouplist.
 * RETURN: pointer to struct bkg_status, NULL if not found.
 */
struct bkg_status *
nw_group_find(char *gname, bkg_status *grp_list)
{
	bkg_status	*bkg;

	for (bkg = grp_list; bkg != NULL; bkg = bkg->next) {
		if (strcmp(bkg->name, gname) == 0)
			return (bkg);
	}
	return (NULL);
}

/*
 * Return the status of the group.
 * INPUT: pointer to the group.
 * RETURN: status of the group.
 */
int
nw_group_status(bkg_status *group)
{
	int status = PNM_STAT_DOWN; /* Assume group is down */
	bkg_adp *adp;

	for (adp = group->head; adp != NULL; adp = adp->next) {
		if (adp->state == ADP_OK) {
			status = PNM_STAT_OK;
			break;
		}
	}
	return (status);
}

/*
 * Returns 0 on success and on error it returns
 * PNM_ENONHOMOGENOUS - all adps in the group do not have similar instances
 * (IPv4/IPv6) plumbed, hence the group is said to be non-homogenous.
 */
int
nw_group_instances(bkg_status *group, int *resultp)
{
	int result = 0;
	bkg_adp *adp;
	boolean_t v4 = B_FALSE;
	boolean_t v6 = B_FALSE;

	for (adp = group->head; adp; adp = adp->next) {
		if (adp->pii_v4)
			v4 = B_TRUE;
		if (adp->pii_v6)
			v6 = B_TRUE;
	}

	if (v4) {
		for (adp = group->head; adp; adp = adp->next) {
			if (!adp->pii_v4) {
				log_err_lib("All adps in %s do not have similar"
				    " instances (IPv4/IPv6) plumbed.", group);
				return (PNM_ENONHOMOGENOUS);
			}
		}
		result |= PNMV4MASK;
	}

	if (v6) {
		for (adp = group->head; adp; adp = adp->next) {
			if (!adp->pii_v6) {
				log_err_lib("All adps in %s do not have similar"
				    " instances (IPv4/IPv6) plumbed.", group);
				return (PNM_ENONHOMOGENOUS);
			}
		}
		result |= PNMV6MASK;
	}

	if (resultp != NULL)
		*resultp = result;

	return (0);
}

/*
 * Add a flag for adapter 'name'. Note that if 'value' is negative, the
 * action taken will be to OR out that flag. So -IFF_UP means take out
 * IFF_UP from the flags of the named adapter.
 */
static int
nw_adp_set_flags(int so, const char *name, int value)
{
	struct lifreq lifr;

	(void) memset(&lifr, 0, sizeof (lifr));

	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
	if (ioctl(so, SIOCGLIFFLAGS, (caddr_t)&lifr) < 0) {
		log_syserr_lib("SIOCGLIFFLAGS failed.");
		return (-1);
	}

	if (value < 0) {
		value = -value;
		lifr.lifr_flags &= ~((uint_t)value);
	} else
		lifr.lifr_flags |= (uint_t)value;

	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));

	/* CSTYLED */
	if (ioctl(so, SIOCSLIFFLAGS, (caddr_t)&lifr) < 0) { /*lint !e737 */
		log_syserr_lib("SIOCSLIFFLAGS failed.");
		return (-1);
	}

	return (0);
}

#if (SOL_VERSION >= __s10)
/*
 * Set the zone associated with interface 'name'
 * Doesn't do anything for solaris versions prior to 10.
 */
static int
nw_adp_set_zone(int so, const char *name, const char *zone)
{

	struct lifreq lifr;
	id_t	zid = 0;

	if (zone == NULL) {
		log_syserr_lib("Null input value for zone name.");
		return (-1);
	}

	zid = getzoneidbyname(zone);
	if (zid == -1) {
		log_syserr_lib("zoneid lookup failed.");
		return (-1);
	}

	(void) memset(&lifr, 0, sizeof (lifr));

	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
	lifr.lifr_zoneid = zid;

	if (ioctl(so, SIOCSLIFZONE, (caddr_t)&lifr) < 0) {
		log_syserr_lib("SIOCSLIFZONE failed.");
		return (-1);
	}
	return (0);

}
#endif

static int
nw_adp_up(int so, const char *name, sa_family_t af)
{
	if (af == AF_INET6) {
#if SOL_VERSION >= __s9
#else
		/*
		 * If an NDP cache entry exists for the address on the
		 * target interface, setting IFF_UP would fail with
		 * EEXIST. IFF_NOLOCAL overrides this check. We next
		 * clears IFF_NOLOCAL so that IP gets a chance to remove
		 * all NDP entries for the address and properly bring the
		 * interface up.
		 *
		 * Need to do this de-tour only for pre-S9. IP since S9
		 * always clean up the NDP cache when an interface is
		 * marked UP even though the current state is DOWN. In
		 * pre-S9 days, no such clean up is done, hence the
		 * problem described above.
		 *
		 * See ifconfig.c do_dad() for similar use of IFF_NOLOCAL.
		 */

		int rc;

		if ((rc = nw_adp_set_flags(so, name,
			IFF_NOLOCAL | IFF_UP | IFF_DEPRECATED)) != 0)
			return (rc);

		return (nw_adp_set_flags(so, name, -IFF_NOLOCAL));
#endif
	}

	return (nw_adp_set_flags(so, name, IFF_UP | IFF_DEPRECATED));
}

static int
nw_adp_down(int so, const char *name)
{
	return (nw_adp_set_flags(so, name, -IFF_UP));
}

/*ARGSUSED4*/
static int
nw_adp_plumb_v4(int so, const char *name, struct in_addr *ip,
    const char *zone)
{
	struct lifreq		lifr, lifrmask;
	struct sockaddr_in 	sinbuf, *sin, mask, *netmask = NULL;
	struct in_addr	addr;

	/* Make sure we're dealing with a logical interface */
	if (strchr(name, ':') == NULL)
		return (-1);

	/* Make sure we're not setting broadcast addresses */
	(void) inet_pton(AF_INET, "255.255.255.255", &addr);
	if (ip->s_addr == addr.s_addr)
		return (-1);

	/* Plumb the logical address of 0.0.0.0 */
	if (nw_plumb_if(name, AF_INET) == -1)
		return (-1);

	(void) memset(&lifr, 0, sizeof (lifr));

	/*
	 * Copy IP address into the ioctl structure lifr.
	 */
	sin = &sinbuf;
	sin->sin_family = AF_INET;
	sin->sin_addr.s_addr = ip->s_addr;
	bcopy((struct sockaddr_storage *)sin, &lifr.lifr_addr,
		sizeof (lifr.lifr_addr));
	lifr.lifr_addr.ss_family = AF_INET;

	/*
	 * getnetmaskbyaddr() could fail, in which case we will
	 * let the kernel assign the default netmask.
	 */
	if (getnetmaskbyaddr(sin->sin_addr, &mask.sin_addr) == 0)
		netmask = &mask;

	if (netmask != NULL) {
		netmask->sin_family = AF_INET;
		bcopy((struct sockaddr_storage *)netmask, &lifrmask.lifr_addr,
			sizeof (lifrmask.lifr_addr));
		(void) strncpy(lifrmask.lifr_name, name,
			sizeof (lifrmask.lifr_name));
		lifrmask.lifr_addr.ss_family = AF_INET;
		/* CSTYLED */
		/*lint -e737 */
		if (ioctl(so, SIOCSLIFNETMASK, (caddr_t)&lifrmask) < 0) {
		/* CSTYLED */
		/*lint +e737 */
			log_syserr_lib("SIOCSLIFNETMASK failed.");
			goto err;
		}
	}

	/* Set IP address of logical interface. */
	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
	/* CSTYLED */
	/*lint -e737 */
	if (ioctl(so, SIOCSLIFADDR, (caddr_t)&lifr) < 0) {
	/* CSTYLED */
	/*lint +e737 */
		log_syserr_lib("SIOCSLIFADDR failed.");
		goto err;
	}

	/*
	 * No need to set broadcast address -- it's set automatically when
	 * we set up the address above.
	 */
	if (nw_adp_set_flags(so, name, -IFF_UP) == -1)
		goto err;

#if (SOL_VERSION >= __s10)
	/* Set the zone for the interface */
	if (zone != NULL) {
		if (nw_adp_set_zone(so, name, zone) == -1)
			goto err;
	}
#endif
	return (0);

err:
	(void) nw_unplumb_if(name, AF_INET);
	return (-1);
}

/*ARGSUSED4*/
static int
nw_adp_plumb_v6(int so, const char *name, struct in6_addr *ip,
    const char *zone)
{
	struct lifreq		lifr, lifrmask;
	struct sockaddr_in6 	sinbuf6, *sin6, netmask;

	/* Make sure we're dealing with logical interface */
	if (strchr(name, ':') == NULL)
		return (-1);

	/* Plumb the logical address of ::0 */
	if (nw_plumb_if(name, AF_INET6) == -1)
		return (-1);

	(void) memset(&lifr, 0, sizeof (lifr));

	/*
	 * Copy IP address into the ioctl structure lifr.
	 */
	sin6 = &sinbuf6;
	sin6->sin6_family = AF_INET6;
	bcopy(ip, &(sin6->sin6_addr), CL_IPV6_ADDR_LEN);
	bcopy((struct sockaddr_storage *)sin6, &lifr.lifr_addr,
		sizeof (lifr.lifr_addr));
	lifr.lifr_addr.ss_family = AF_INET6;

	/* Set the netmask to be 64 bits */
	netmask.sin6_family = AF_INET6;
	(void) inet_pton(AF_INET6, "FFFF:FFFF:FFFF:FFFF::0",
		&(netmask.sin6_addr));
	bcopy((struct sockaddr_storage *)&netmask, &(lifrmask.lifr_addr),
		sizeof (lifrmask.lifr_addr));
	(void) strncpy(lifrmask.lifr_name, name, sizeof (lifrmask.lifr_name));
	lifrmask.lifr_addr.ss_family = AF_INET6;
	/* CSTYLED */
	/*lint -e737 */
	if (ioctl(so, SIOCSLIFNETMASK, (caddr_t)&lifrmask) < 0) {
	/* CSTYLED */
	/*lint +e737 */
		log_syserr_lib("SIOCSLIFNETMASK failed.");
		goto err;
	}

	/* Set IP address of logical interface. */
	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
	/* CSTYLED */
	/*lint -e737 */
	if (ioctl(so, SIOCSLIFADDR, (caddr_t)&lifr) < 0) {
	/* CSTYLED */
	/*lint +e737 */
		log_syserr_lib("SIOCSLIFADDR failed.");
		goto err;
	}

	if (nw_adp_set_flags(so, name, -IFF_UP) == -1)
		goto err;
#if (SOL_VERSION >= __s10)
	/* Set the zone associated with this interface */
	if (zone != NULL) {
		if (nw_adp_set_zone(so, name, zone) == -1)
			goto err;
	}

#endif
	return (0);

err:
	(void) nw_unplumb_if(name, AF_INET6);
	return (-1);
}

/*
 * Unplumb logical interface
 *	1) OR out IFF_UP from flags
 *	2) set address to 0
 * This should conform to all or none semantics.
 */
static int
nw_adp_unplumb(int so, const char *name, sa_family_t af)
{
	struct lifreq lifr;

	(void) memset(&lifr, 0, sizeof (lifr));
	lifr.lifr_addr.ss_family = af;

	/*
	 * Set flags to DOWN before setting addr to 0, so that this
	 * deleted entry won't show up in ifconfig(1M) output.
	 */
	if (nw_adp_set_flags(so, name, -IFF_UP) < 0) {
		return (-1);
	}

	/* Set IP address to 0.0.0.0/::0 */
	(void) strncpy(lifr.lifr_name, name, sizeof (lifr.lifr_name));
	/* CSTYLED */
	/*lint -e737 */
	if (ioctl(so, SIOCSLIFADDR, (caddr_t)&lifr) < 0) {
	/* CSTYLED */
	/*lint +e737 */
		log_syserr_lib("SIOCSLIFADDR failed.");
		return (-1);
	}

	/* Remove the streams */
	if (nw_unplumb_if(name, af) == -1) {
		return (-1);
	}

	return (0);
}

/*
 * In Solaris 2.8, we have to explicitly plumb and unplumb a logical
 * interface with an ioctl. These new functions are called from
 * log_adp_plumb() and log_adp_unplumb() for this purpose.
 * Like all system calls, we use int to indicate success/failure.
 * These should only be called for logical adapters and not for physical
 * adapters.
 */

static int
nw_plumb_if(const char *ifname, sa_family_t af)
{
	int fd = 0;
	struct lifreq lifr;

	if (af == AF_INET)
		fd = open("/dev/ip", O_RDWR);
	else if (af == AF_INET6)
		fd = open("/dev/ip6", O_RDWR);

	if (fd == -1) {
		log_syserr_lib("open failed.");
		return (-1);
	}

	/* This adds 0.0.0.0/::0 as a logical IP address to the adapter */
	bzero(&lifr, sizeof (lifr));
	(void) strcpy(lifr.lifr_name, ifname);
	if (ioctl(fd, SIOCLIFADDIF, &lifr) == -1) {
		log_syserr_lib("SIOCLIFADDIF failed.");
		(void) close(fd);
		return (-1);
	}

	(void) close(fd);
	return (0);
}

static int
nw_unplumb_if(const char *ifname, sa_family_t af)
{
	int fd = 0;
	struct lifreq lifr;

	if (af == AF_INET)
		fd = open("/dev/ip", O_RDWR);
	else if (af == AF_INET6)
		fd = open("/dev/ip6", O_RDWR);

	if (fd == -1) {
		log_syserr_lib("open failed.");
		return (-1);
	}

	/* This removes the logical IP address */
	bzero(&lifr, sizeof (lifr));
	(void) strcpy(lifr.lifr_name, ifname);

	/* CSTYLED */
	/*lint -e737 */
	if (ioctl(fd, SIOCLIFREMOVEIF, &lifr) == -1) {
	/* CSTYLED */
	/*lint +e737 */
		log_syserr_lib("SIOCLIFREMOVEIF failed.");
		(void) close(fd);
		return (-1);
	}

	(void) close(fd);
	return (0);
}

/*
 * sends out an unsolicited neighbor advertisement for the given
 * IPv6 address, adapter and open raw socket
 */
static int
nw_ipv6_send_una(int so, char *adp, struct in6_addr ip)
{
	char buffer[256];
	nd_neighbor_advert_t *una;
	nd_opt_hdr_t *ndo;
	int hops = 255;	/* required by RFC 2461 for unsolicited neighbor adv */
	uint_t plen;
	struct sockaddr_in6 *sin6, target, myaddr;
	struct lifreq lifr;
	int ifindex;

	bzero(buffer, sizeof (buffer));
	una = (nd_neighbor_advert_t *)buffer;
	ndo = (nd_opt_hdr_t *)(una + 1);

	/* our address, to be used as source address in the packet */
	bzero(&myaddr, sizeof (myaddr));
	myaddr.sin6_family = AF_INET6;
	myaddr.sin6_addr = ip;

	/* multicast target, to be used as destination of our ip6 packet */
	bzero(&target, sizeof (target));
	target.sin6_family = AF_INET6;
	if (inet_pton(AF_INET6, MULTICAST_TARGET, &target.sin6_addr) != 1) {
		log_syserr_lib("inet_pton failed.");
		return (-1);
	}

	/* setup data structure for unsolicited advertisement */
	una->nd_na_type = ND_NEIGHBOR_ADVERT;
	una->nd_na_code = 0;
	una->nd_na_target = myaddr.sin6_addr;
	una->nd_na_cksum = 0;

	/*
	 * The flag is a combination that specifies that:
	 * a) this node is not a router (SC nodes can not be routers)
	 * b) this advertisement is unsolicited
	 * c) recipients should update the link-layer address immediately
	 */
	una->nd_na_flags_reserved =
		~ND_NA_FLAG_ROUTER & ~ND_NA_FLAG_SOLICITED &
		ND_NA_FLAG_OVERRIDE;

	ndo->nd_opt_type = ND_OPT_TARGET_LINKADDR;

	(void) strncpy(lifr.lifr_name, adp, sizeof (lifr.lifr_name));
	if (ioctl(so, SIOCGLIFINDEX, (char *)&lifr) < 0) {
		log_syserr_lib("SIOCGLIFINDEX failed.");
		return (-1);
	}
	ifindex = lifr.lifr_index;
#ifdef	IPV6_BOUND_PIF
	if (setsockopt(so, IPPROTO_IPV6, IPV6_BOUND_PIF, (char *)&ifindex,
		sizeof (ifindex)) < 0) {
		log_syserr_lib("IPV6_BOUND_PIF failed.");
		return (-1);
	}
#else
	if (setsockopt(so, IPPROTO_IPV6, IPV6_BOUND_IF, (char *)&ifindex,
		sizeof (ifindex)) < 0) {
		log_syserr_lib("IPV6_BOUND_IF failed.");
		return (-1);
	}
#endif

	if (connect(so, (struct sockaddr *)&target, sizeof (target)) < 0) {
		log_syserr_lib("connect failed.");
		return (-1);
	}

	if (setsockopt(so, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &hops,
		sizeof (hops)) < 0) {
		log_syserr_lib("setsockopt failed");
		return (-1);
	}

	/* find out the hardware address where this ip is hosted */
	bzero(&lifr, sizeof (lifr));
	sin6 = (struct sockaddr_in6 *)&lifr.lifr_nd.lnr_addr;
	bzero(sin6, sizeof (struct sockaddr_in6));
	sin6->sin6_family = AF_INET6;
	sin6->sin6_addr = una->nd_na_target;

	(void) strncpy(lifr.lifr_name, adp, sizeof (lifr.lifr_name));
	if (ioctl(so, SIOCLIFGETND, (char *)&lifr) < 0) {
		log_syserr_lib("SIOCLIFGETND failed.");
		return (-1);
	}

	if (lifr.lifr_nd.lnr_hdw_len != 0) {
		/* CSTYLED */
		/*lint -e419 */
		bcopy((char *)lifr.lifr_nd.lnr_hdw_addr,
		(char *)(ndo + 1), (uint_t)lifr.lifr_nd.lnr_hdw_len);
		/* CSTYLED */
		/*lint +e419 */
	}

	/* Length is specified in units of 8 octets */
	ndo->nd_opt_len = (uint8_t)(lifr.lifr_nd.lnr_hdw_len + 7) / 8;

	plen = sizeof (nd_neighbor_advert_t) + sizeof (nd_opt_hdr_t)
		+ ndo->nd_opt_len * 8;	/* option len  in multiples of 8 */

	if (send(so, una, plen, 0) != (int)plen) {
		log_syserr_lib("send failed.");
		return (-1);
	}
	return (0);
}

/*
 * Mark UP a logical interface on an given group of
 * network interfaces.
 * INPUT: group name
 * RETURN: 0 on success, -num_failed , PNM error code otherwise.
 */
int
nw_ifconfig_up(bkg_status *grp, uint_t ip_cnt, struct in6_addr *ip_buf)
{
	uint_t i, una_cnt;
	int error = 0;
	char adpn[NAMELEN];
	l_addr *lp, *alist = NULL, *alist6 = NULL, *chlist = NULL;
	int num_failed = 0;
	int s, so, so_v4 = 0, so_v6 = 0;
	struct in6_addr *addr;
	struct in_addr ina;
	char abuf[INET6_ADDRSTRLEN];
	sa_family_t af;
	boolean_t v6_present = B_FALSE;

	/* Get all the Logical IPv4 addresses hosted on this group */
	alist = nw_group_logip_get(grp, AF_INET);

	/* Get all the Logical IPv6 addresses hosted on this group */
	alist6 = nw_group_logip_get(grp, AF_INET6);

	if (alist == NULL && alist6 == NULL) {
		log_err_lib("%s group %s not found",
			nw_disp_group_name(),  grp);
		error = PNM_EPROG;
		goto nw_ifconfig_up_error;
	}

	/* Open a v4 and a v6 socket for doing ioctls */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_up_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_up_error;
	}

	/*
	 * When we mark the logical IP address up we also want to let
	 * everybody else know about the new IP to MAC address mapping.
	 * In IPv4 the Solaris kernel does this for us by sending out
	 * gratuitous ARPs. However, in IPv6 the Solaris kernel does
	 * not do this and hence we have to send out unsolicited
	 * neighbor advertisements. We are allowed to send out 3
	 * such advertisements with a gap of 1s between each
	 * advertisement.
	 */
	if ((s = socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_up_error;
	}
	for (una_cnt = 0; una_cnt < ND_MAX_NEIGHBOR_ADVERTISEMENT;
		una_cnt++) {
		/* For all the given IP addresses.... */
		for (i = 0; i < ip_cnt; i++) {
			addr = &(ip_buf[i]);

			/* Find out if this is v4 or v6 */
			if (IN6_IS_ADDR_V4MAPPED(addr)) {
				chlist = alist;
				af = AF_INET;
				so = so_v4;
				IN6_V4MAPPED_TO_INADDR(addr, &ina);
				(void) inet_ntop(af, &ina, abuf,
					sizeof (abuf));
			} else {	/* v6 */
				chlist = alist6;
				af = AF_INET6;
				so = so_v6;
				v6_present = B_TRUE;
				(void) inet_ntop(af, addr, abuf,
					sizeof (abuf));
			}

			/* Find the given IP address in the list */
			for (lp = chlist; lp; lp = lp->next) {
				if (IN6_ARE_ADDR_EQUAL(addr,
					&(lp->ipaddr)))
					break; /* found */
			}

			/*
			 * If the address is found and it is already
			 * IPADDR_UP then try the next IP address
			 * - idempotent. Send a unsolicited neighbor
			 * advertisement for v6.
			 */
			if ((lp != NULL) && (lp->flags & IPADDR_UP)
				== IPADDR_UP) {
				if (af == AF_INET6) {
					(void) strncpy(adpn, lp->name,
						strlen(lp->name));
					(void) nw_ipv6_send_una(s,
						strtok(adpn, ":"),
						lp->ipaddr);
				}
				continue;  /* Next Ip addr, next i */
			}

			/*
			 * If we don't find the address, try the next IP
			 * address - don't error out
			 */
			if (lp == NULL)
				continue; /* Next Ip addr, next i */

			/*
			 * Set the flags to IPADDR_UP and send a
			 * unsolicited neighbor advertisement
			 */
			if (nw_adp_up(so, lp->name, af) == 0) {
				if (af == AF_INET6) {
					(void) strncpy(adpn, lp->name,
						strlen(lp->name));
					(void) nw_ipv6_send_una(s,
						strtok(adpn, ":"),
						lp->ipaddr);
				}
				lp->flags |= IPADDR_UP;
				continue; /* Next Ip addr, next i */
			}

			(void) sc_syslog_msg_log(msg_libpnm,
				SC_SYSLOG_ERROR, MESSAGE,
				"%s can't UP %s.", grp->name, abuf);
			num_failed++;
		}	/* end of for - IP addresses */
		/*
		 * We will break out of the una_cnt loop if we
		 * don't have any IPv6 addresses or we have already
		 * sent ND_MAX_NEIGHBOR_ADVERTISEMENT advertisements
		 */
		if (!v6_present ||
			una_cnt == (ND_MAX_NEIGHBOR_ADVERTISEMENT - 1))
			break; /* out of una_cnt loop */
		(void) usleep(ND_RETRANS_TIMER * 1000);
	}	/* end of for - una_cnt */
	(void) close(s);

nw_ifconfig_up_error:
	/*
	 * We make error to be the negative of the number of IP addrs that we
	 * were not able to up.
	 */
	if (num_failed > 0)
		error = -1 * num_failed;

	if (alist)
		nw_group_logip_free(alist);
	if (alist6)
		nw_group_logip_free(alist6);

	if (so_v4)
		(void) close(so_v4);
	if (so_v6)
		(void) close(so_v6);

	return (error);
}

/*
 * Mark DOWN a logical interface on an given group of
 * network interfaces.
 * RETURN: 0 on success, -num_failed , PNM error code otherwise.
 */
int
nw_ifconfig_down(uint_t ip_cnt, struct in6_addr *ip_buf)
{
	uint_t i;
	int error = 0;
	l_addr *lp, *alist = NULL, *alist6 = NULL, *chlist = NULL;
	int num_failed = 0;
	int so, so_v4 = 0, so_v6 = 0;
	struct in6_addr *addr;
	struct in_addr ina;
	char abuf[INET6_ADDRSTRLEN];
	sa_family_t af;

	/* Get all the Logical Ip addresses hosted on this node */
	alist = nw_group_logip_get(NULL, AF_INET);

	/* Get all the Logical IPv6 addresses hosted on this node */
	alist6 = nw_group_logip_get(NULL, AF_INET6);

	if (alist == NULL && alist6 == NULL) {
		log_err_lib("no public adapters on this node");
		error = PNM_EPROG;
		goto nw_ifconfig_down_error;
	}

	/* Open a v4 and a v6 socket for doing ioctls */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_down_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_down_error;
	}

	/* For all the given IP addresses.... */
	for (i = 0; i < ip_cnt; i++) {
		addr = &(ip_buf[i]);

		/* Find out if this is v4 or v6 */
		if (IN6_IS_ADDR_V4MAPPED(addr)) {	/* v4 */
			chlist = alist;
			af = AF_INET;
			so = so_v4;
			IN6_V4MAPPED_TO_INADDR(addr, &ina);
			(void) inet_ntop(af, &ina, abuf,
				sizeof (abuf));
		} else {	/* v6 */
			chlist = alist6;
			af = AF_INET6;
			so = so_v6;
			(void) inet_ntop(af, addr, abuf,
				sizeof (abuf));
		}

		/* Find the given IP address in the list */
		for (lp = chlist; lp; lp = lp->next) {
			if (IN6_ARE_ADDR_EQUAL(addr, &(lp->ipaddr)))
				break; /* found */
		}

		/*
		 * If the address is found and it is already DOWN
		 * then try the next IP address - idempotent.
		 */
		if ((lp != NULL) && (lp->flags & IPADDR_UP)
			!= IPADDR_UP)
			continue; /* Next Ip addr, next i */

		/*
		 * If we don't find the address, try the next IP
		 * address - don't error out
		 */
		if (lp == NULL)
			continue; /* Next Ip addr, next i */

		/* Set the flags to DOWN */
		if (nw_adp_down(so, lp->name) == 0)
			continue; /* Next IP addr, next i */

		(void) sc_syslog_msg_log(msg_libpnm, SC_SYSLOG_ERROR,
			MESSAGE, "%s can't DOWN.", abuf);
		num_failed++;
	}

nw_ifconfig_down_error:
	/*
	 * We make error to be the negative of the number of IP addrs that we
	 * were not able to down.
	 */
	if (num_failed > 0)
		error = -1 * num_failed;

	if (alist)
		nw_group_logip_free(alist);
	if (alist6)
		nw_group_logip_free(alist6);

	if (so_v4)
		(void) close(so_v4);
	if (so_v6)
		(void) close(so_v6);

	return (error);
}

/*
 * Plumb logical interfaces on an given group of
 * network interfaces.
 * OUTPUT: num_failed is the number of IP addresses of which ifconfig failed.
 * RETURN: 0 on success, -num_failed , PNM error code otherwise.
 */
int
nw_ifconfig_plumb(bkg_status *grp, const char *zone, uint_t ip_cnt,
    struct in6_addr *ip_buf)
{
	uint_t i;
	int error = 0;
	uint_t min_nLpi, min;
	char Lname[NAMELEN];
	l_addr *lp, *alist = NULL, *alist6 = NULL, *chlist = NULL;
	int *freelist = NULL;
	uint_t freelen = 0, find;
	int num_failed = 0;
	int so_v4 = 0, so_v6 = 0;
	bkg_adp *adp, *min_adp;
	char *name, *first_tok;
	struct in6_addr *addr;
	struct in_addr ina;
	char abuf[INET6_ADDRSTRLEN];
	sa_family_t af;

	/* Get all the Logical IPv4 addresses hosted on this group */
	alist = nw_group_logip_get(grp, AF_INET);

	/* Get all the Logical IPv6 addresses hosted on this group */
	alist6 = nw_group_logip_get(grp, AF_INET6);

	if (alist == NULL && alist6 == NULL) {
		log_err_lib("%s group %s not found",
			nw_disp_group_name(),  grp);
		error = PNM_EPROG;
		goto nw_ifconfig_plumb_error;
	}

	/* Open a v4 and a v6 socket for doing ioctls */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_plumb_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_plumb_error;
	}

	/* For all the given IP addresses.... */
	for (i = 0; i < ip_cnt; i++) {
		addr = &ip_buf[i];

		/* Find out if this addr is v4 or v6 */
		if (IN6_IS_ADDR_V4MAPPED(addr)) {	/* v4 */
			chlist = alist;
			af = AF_INET;
			IN6_V4MAPPED_TO_INADDR(addr, &ina);
			(void) inet_ntop(af, &ina, abuf, sizeof (abuf));
		} else {	/* v6 */
			chlist = alist6;
			af = AF_INET6;
			(void) inet_ntop(af, addr, abuf, sizeof (abuf));
		}

		/*
		 * Check if this IP address is already hosted.
		 * If this is the case then we do nothing
		 * For this we should check all the LIps in
		 * group.
		 */
		for (lp = chlist; lp; lp = lp->next) {
			if (IN6_ARE_ADDR_EQUAL(addr, &(lp->ipaddr)))
				break;
		}

		/*
		 * If address is already hosted then try the next
		 * address. Hence this call is idempotent.
		 */
		if (lp != NULL)
			continue;

		/*
		 * Find out the adp which has the min number
		 * of Logical IP addresses hosted. This adapter has to
		 * be ADP_OK also - if not then try another adapter.
		 * We get the logical IPs for the particular family.
		 * It is possible that a hostname which has a v4 and
		 * a v6 mapping might be hosted on two different adps.
		 * This should not cause any problems at all. However,
		 * if people complain then we can change this behavior.
		 */
		adp = grp->head;
		min_nLpi = MAX_NUMIFS;
		min_adp = adp;
		/*
		 * Under the new IPMP architecture, IPMP psuedo-
		 * interface will take over the member interfaces and hence
		 * the assigned logical hostnames.  So, even if we assign
		 * the logical hostname to an underlying interface,
		 * this will be taken over by the ipmp interface.
		 * The new IPMP architecture internally does the
		 * load-spreading by ensuring that the logical hostnames are
		 * evenly distributed over the set of active member interfaces.
		 * Hence it does not matter which underlying interface will
		 * host the logical ip address. Use LIFC_UNDER_IPMP flag to
		 * define this logic.
		 */
#ifndef	LIFC_UNDER_IPMP
		for (; adp; adp = adp->next) {
			/* if this adp is not ok try the next */
			if (adp->state != ADP_OK)
				continue;
			min = nw_group_logip_num_get(adp, af);
			if (min_nLpi > min) {
				min_nLpi = min;
				min_adp = adp;
			}
		}
#endif
		/*
		 * Find all logical instances that are
		 * currently in use on min_adp. Mark them '-1'
		 * in the freelist array.
		 */
		freelen = MAX_NUMIFS;
		freelist = (int *)calloc(freelen, sizeof (int));
		if (freelist == NULL) {
			log_syserr_lib("out of memory.");
			error = PNM_EPROG;
			goto nw_ifconfig_plumb_error;
		}
		for (lp = chlist; lp; lp = lp->next, free(name)) {
			/*
			 * We are only concerned with the LIps
			 * hosted on min_adp.
			 */
			name = strdup(lp->name);
			if (name == NULL) {
				log_syserr_lib("out of memory.");
				error = PNM_EPROG;
				goto nw_ifconfig_plumb_error;
			}
			first_tok = strtok(name, ":");
			if (first_tok == NULL)
				continue;
			if (strcmp(first_tok, min_adp->adp) != 0)
				continue;
			if (lp->inst < freelen)
				freelist[lp->inst] = -1;
		}

		/* Find the next free instance number */
		for (find = 0; find < freelen; find++) {
			if (freelist[find] != -1)
				break;
		}

		/* If no more free instance numbers */
		if (find == freelen) {
			(void) sc_syslog_msg_log(msg_libpnm,
				SC_SYSLOG_ERROR, MESSAGE,
				"%s can't plumb %s: "
				"out of instance numbers on %s.",
				grp->name, abuf, min_adp->adp);
			num_failed++;
			continue; /* Next IP addr, next i */
		}

		(void) sprintf(Lname, "%s:%d", min_adp->adp, find);

		/* Now plumb the LIp */
		if (af == AF_INET &&	/* v4 */
			nw_adp_plumb_v4(so_v4, Lname, &ina,
			zone) == 0) {
			/* Add a new laddr record */
			lp = (l_addr *) malloc(sizeof (l_addr));
			if (lp == NULL) {
				log_syserr_lib("out of memory.");
				error = PNM_EPROG;
				goto nw_ifconfig_plumb_error;
			}
			bcopy(addr, &(lp->ipaddr), CL_IPV6_ADDR_LEN);
			lp->flags = 0; /* Initialize to 0 */
			lp->name = strdup(Lname);
			if (lp->name == NULL) {
				log_syserr_lib("out of memory.");
				error = PNM_EPROG;
				goto nw_ifconfig_plumb_error;
			}
			lp->inst = find;
			min_adp->pii_v4->nLpi++;
			lp->next = alist;
			alist = lp;
			freelist[find] = -1;
			continue; /* Next Ip addr, next i */
		}
		if (af == AF_INET6 &&	/* v6 */
			nw_adp_plumb_v6(so_v6, Lname, addr,
			zone) == 0) {
			/* Add a new laddr record */
			lp = (l_addr *) malloc(sizeof (l_addr));
			if (lp == NULL) {
				log_syserr_lib("out of memory.");
				error = PNM_EPROG;
				goto nw_ifconfig_plumb_error;
			}
			bcopy(addr, &(lp->ipaddr), CL_IPV6_ADDR_LEN);
			lp->flags = 0; /* Initialize to 0 */
			lp->name = strdup(Lname);
			if (lp->name == NULL) {
				log_syserr_lib("out of memory.");
				error = PNM_EPROG;
				goto nw_ifconfig_plumb_error;
			}
			lp->inst = find;
			min_adp->pii_v6->nLpi++;
			lp->next = alist6;
			alist6 = lp;
			freelist[find] = -1;
			continue; /* Next IP addr, next i */
		}

		/*
		 * Here we might want to try the next adapter in the
		 * IPMP group since maybe we will be able to plumb a
		 * Logical IP on the next adapter - but we don't want
		 * to do this since we assume that if an adapter is UP
		 * then it is able to host an extra Logical IP address,
		 * and if it can't then it is an error.
		 */
		(void) sc_syslog_msg_log(msg_libpnm, SC_SYSLOG_ERROR,
			MESSAGE, "%s can't plumb %s.",
			grp->name, abuf);
		num_failed++;
	}

nw_ifconfig_plumb_error:

	/*
	 * We make error to be the negative of the number of IP addrs that we
	 * were not able to plumb.
	 */
	if (num_failed > 0)
		error = -1 * num_failed;

	if (freelist != NULL) {
		free(freelist);
		freelist = NULL;
	}
	if (alist)
		nw_group_logip_free(alist);
	if (alist6)
		nw_group_logip_free(alist6);

	if (so_v4)
		(void) close(so_v4);
	if (so_v6)
		(void) close(so_v6);

	return (error);
}

/*
 * Unplumb a logical interface from a given group of
 * network interfaces.
 * OUTPUT: num_failed is the number of IP addresses of which ifconfig failed.
 * RETURN: 0 on success, -num_failed , PNM error code otherwise.
 */
int
nw_ifconfig_unplumb(uint_t ip_cnt, struct in6_addr *ip_buf)
{
	uint_t i;
	int error = 0;
	l_addr *lp, *alist = NULL, *alist6 = NULL, *chlist = NULL;
	int num_failed = 0;
	int so, so_v4 = 0, so_v6 = 0;
	struct in6_addr *addr;
	struct in_addr ina;
	char abuf[INET6_ADDRSTRLEN];
	sa_family_t af;

	/* Get all the Logical Ip addresses hosted on this node */
	alist = nw_group_logip_get(NULL, AF_INET);

	/* Get all the Logical IPv6 addresses hosted on this node */
	alist6 = nw_group_logip_get(NULL, AF_INET6);

	if (alist == NULL && alist6 == NULL) {
		log_err_lib("no public adapters on this node");
		error = PNM_EPROG;
		goto nw_ifconfig_unplumb_error;
	}

	/* Open a v4 and a v6 socket for doing ioctls */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_unplumb_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_unplumb_error;
	}

	/* For all the given IP addresses.... */
	for (i = 0; i < ip_cnt; i++) {
		addr = &(ip_buf[i]);

		/* Find out if this addr is v4 or v6 */
		if (IN6_IS_ADDR_V4MAPPED(addr)) {	/* v4 */
			chlist = alist;
			af = AF_INET;
			so = so_v4;
			IN6_V4MAPPED_TO_INADDR(addr, &ina);
			(void) inet_ntop(af, &ina, abuf,
				sizeof (abuf));
		} else {	/* v6 */
			chlist = alist6;
			af = AF_INET6;
			so = so_v6;
			(void) inet_ntop(af, addr, abuf,
				sizeof (abuf));
		}

		/* Find the given IP address in the list */
		for (lp = chlist; lp; lp = lp->next) {
			if (IN6_ARE_ADDR_EQUAL(addr, &(lp->ipaddr)))
				break; /* found */
		}

		/*
		 * If we don't find the address, try the next IP
		 * address - don't error out - idempotent.
		 */
		if (lp == NULL)
			continue; /* Next Ip addr, next i */

		if (nw_adp_unplumb(so, lp->name, af) == 0)
			continue; /* Next Ip addr, next i */

		(void) sc_syslog_msg_log(msg_libpnm, SC_SYSLOG_ERROR,
			MESSAGE, "%s can't unplumb.", abuf);
		num_failed++;
	}

nw_ifconfig_unplumb_error:
	/*
	 * We make error to be the negative of the number of IP addrs that we
	 * were not able to unplumb.
	 */
	if (num_failed > 0)
		error = -1 * num_failed;

	if (alist)
		nw_group_logip_free(alist);
	if (alist6)
		nw_group_logip_free(alist6);

	if (so_v4)
		(void) close(so_v4);
	if (so_v6)
		(void) close(so_v6);

	return (error);
}

char
*nw_disp_group_name()
{
	return ("IPMP");
}
