/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pnm_network_linux_bonding.c	1.4	08/05/20 SMI"

/*
 * pnmd_network_linux_bonding.c - Public network group management module.
 *
 * Provide the PNM groups data structure and all the functions needed by
 * libpnm module to manage the public network groups. It provides also an
 * abstraction layer between the libpnm and the underlying kernel/networking
 * component it is running on.
 */

#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <pnm/pnm_ld.h>
#include <pnm_network.h>
#include <linux/if_bonding.h>
#include <linux/if_ether.h>

/*
 * Gratuitous ARP definitions ...
 */
#define	IP_ADDR_LEN	4
#define	ARP_FRAME_TYPE	0x0806
#define	ETHER_HW_TYPE	1
#define	IP_PROTO_TYPE	0x0800

struct arp_packet {
	uchar_t dest_hw_addr[IFHWADDRLEN];
	uchar_t src_hw_addr[IFHWADDRLEN];
	ushort_t frame_type;
	ushort_t hw_type;
	ushort_t prot_type;
	uchar_t hw_addr_size;
	uchar_t prot_addr_size;
	ushort_t op;
	uchar_t sndr_hw_addr[IFHWADDRLEN];
	uchar_t sndr_ip_addr[IP_ADDR_LEN];
	uchar_t targ_hw_addr[IFHWADDRLEN];
	uchar_t targ_ip_addr[IP_ADDR_LEN];
	uchar_t padding[18];
};

/* multicast target for all nodes */
#define	MULTICAST_TARGET "ff02::1"

/* Neighbor discovery protocol constants */
#ifndef ND_MAX_NEIGHBOR_ADVERTISEMENT
#define	ND_MAX_NEIGHBOR_ADVERTISEMENT	3	/* transmissions */
#endif

#ifndef ND_RETRANS_TIMER
#define	ND_RETRANS_TIMER		1000	/* milliseconds */
#endif

static l_addr *nw_adp_logip_get(char *adp, sa_family_t af);
static l_addr *nw_group_logip_get(bkg_status *grp, sa_family_t af);

static bkg_adp *nw_adp_alloc();
static bkg_adp *nw_adp_find(char *name, bkg_status *grp);
static void nw_adp_append(bkg_status *grp, bkg_adp *adp);
static void nw_adp_delete(bkg_status *grp, bkg_adp *adp);
static void nw_adp_free(bkg_adp *adp);
static void nw_adp_inst_free(adp_inst *adpi);
static bkg_status * nw_group_add(bkg_status **grp_list);
static void nw_group_append(bkg_status **grp_list, bkg_status *grp);
static void nw_group_free(bkg_status *grp);
static void nw_group_logip_free(l_addr *list);

static int nw_adp_set_flags(int so, const char *name, int value);
static int nw_ipv6_send_una(int so, char *adp, struct in6_addr ip);
static int nw_ipv4_send_arp(int so, char *adp, struct in_addr ip);
static int nw_adp_up(int so, const char *name, sa_family_t af);
static int nw_adp_down(int so, const char *name);
static int nw_adp_plumb_v4(int so, const char *name, struct in_addr *ip);
static int nw_adp_plumb_v6(int so, const char *name, struct in6_addr *ip);
static int nw_adp_unplumb(int so, const char *name, sa_family_t af);

/*
 * Port of Solaris getnetmaskbyaddr() to Linux.
 * Given a 32 bit internet network number, it finds the corresponding netmask
 * address based on the "/etc/netmasks" file.
 * Returns zero if successful, non-zero otherwise.
 */
#define	MASKFILE	"/etc/netmasks"

int
getnetmaskbyaddr(const struct in_addr addr, struct in_addr *mask)
{
	FILE *fp;
	struct stat st;
	char netmask[INET_ADDRSTRLEN];
	char network[INET_ADDRSTRLEN];
	struct in_addr netaddr;
	struct in_addr maskaddr;

	mask->s_addr = 0;

	if (stat(MASKFILE, &st) != 0 || (fp = fopen(MASKFILE, "r")) == NULL)
		return (1);

	while (fscanf(fp, "%s %s", network, netmask) == 2) {
		(void) inet_pton(AF_INET, network, &netaddr);
		(void) inet_pton(AF_INET, netmask, &maskaddr);

		if ((addr.s_addr & maskaddr.s_addr) == netaddr.s_addr) {
			mask->s_addr = maskaddr.s_addr;
			return (0);
		}
	}

	return (1);
}

#ifdef LIBPNM_DEBUG
static void
nw_group_logip_print(l_addr *list)
{
	l_addr	*lp = NULL;
	struct in_addr ina;
	char abuf[INET6_ADDRSTRLEN];

	for (lp = list; lp; lp = lp->next) {
		IN6_V4MAPPED_TO_INADDR(&lp->ipaddr, &ina);
		(void) inet_ntop(AF_INET, &ina, abuf, sizeof (abuf));
		printf("addr = %s\n", abuf);
		printf("name = %s\n", lp->name);
	}
}
#endif

char
*nw_disp_group_name()
{
	return ("Bonding");
}

/*
 * Get all IP addresses hosted on this adapter instance - both physical and
 * logical IP addresses. If the adapter instance is NULL then get all the
 * logical IP addresses (not loopback) on this node. Input is physical adp
 * name i.e. eth0. Returns a list of l_addrs. On error returns NULL. Get the IP
 * addresses for the given family.
 * INPUT: pointer to adp name or NULL, family(v4/v6)
 * RETURN: pointer to a list of l_addrs structures, NULL on error.
 */
static l_addr *
nw_adp_logip_get(char *adp, sa_family_t af)
{
	uint_t			n;
	uint_t			phys_name_len;
	int			so = 0;
	char			*name;
	int			numreqs = 30;
	l_addr			*ptr, *last_ptr, *head_ptr;
	struct ifconf		ifc;
	struct ifreq		*ifr;
	struct sockaddr_in	*sin;
	struct ifreq		ifrflag;
	struct in6_addr		addr;
	struct sockaddr_in6	*sin6;

	/*
	 * Must be initialized to NULL, in order to avoid undesirable memory
	 * in case of error.
	 */
	ifc.ifc_buf = NULL;
	ptr = last_ptr = head_ptr = NULL;

	if ((so = socket(af, SOCK_DGRAM, 0)) < 0) {
		if (errno != EAFNOSUPPORT) {
			log_syserr_lib("socket failed.");
			goto nw_adp_logip_error;
		} else {
			/* Address family not supported by protocol */
			return (NULL);
		}
	}

	for (;;) {
		ifc.ifc_len = sizeof (struct ifreq) * numreqs;
		ifc.ifc_buf = realloc(ifc.ifc_buf, ifc.ifc_len);
		if (!ifc.ifc_buf) {
			log_syserr_lib("out of virtual memory.");
			goto nw_adp_logip_error;
		}

		if (ioctl(so, SIOCGIFCONF, &ifc) < 0) {
			log_syserr_lib("SIOCGIFCONF failed.");
			goto nw_adp_logip_error;
		}
		if (ifc.ifc_len == sizeof (struct ifreq) * numreqs) {
			/* assume it overflowed and try again */
			numreqs += 10;
			continue;
		}
		break;
	}

	ifr = ifc.ifc_req;
	for (n = 0; n < ifc.ifc_len; n += sizeof (struct ifreq), ifr++) {
		name = ifr->ifr_name;
		if (af != ifr->ifr_addr.sa_family)
			continue;

		/*
		 * Get the length of the physical adapter.
		 */
		phys_name_len = strcspn(name, ":");
		assert(phys_name_len != 0);
		/*
		 * If adp is NULL then we will get all the logical IPs.
		 * If the name of the i/f is not the same as the name of
		 * adp i.e. eth0 = eth0. So we are only concerned with i/f
		 * which have the same name as adp.
		 */
		if ((adp) &&
			(phys_name_len != strlen(adp) ||
			strncmp(adp, name, phys_name_len) != 0))
			continue;

		/* Also if the name of the adp is lo:x then we do not care. */
		if (strncmp(name, "lo", phys_name_len) == 0)
			continue;

		/*
		 * Retrieve the flags set on this IP address
		 * (physical or logical).
		 */
		(void) memset(&ifrflag, 0, sizeof (ifrflag));
		(void) strncpy(ifrflag.ifr_name, name,
			sizeof (ifrflag.ifr_name));

		if (ioctl(so, SIOCGIFFLAGS, (char *)&ifrflag) < 0) {
			if (errno == ENXIO)
				continue;	/* ignore this interface */
			log_syserr_lib("SIOCGIFFLAGS failed.");
			goto nw_adp_logip_error;
		}

		ptr = (l_addr *) malloc(sizeof (l_addr));
		if (ptr == NULL) {
			log_syserr_lib("out of memory.");
			goto nw_adp_logip_error;
		}
		bzero(ptr, sizeof (l_addr));

		/* Append l_addr structure to list */
		if (head_ptr == NULL)
			head_ptr = ptr;
		if (last_ptr != NULL)
			last_ptr->next = ptr;
		last_ptr = ptr;

		if (af == AF_INET) {	/* v4 */
			sin = (struct sockaddr_in *)&ifr->ifr_addr;
			IN6_INADDR_TO_V4MAPPED(&sin->sin_addr, &(ptr->ipaddr));
		} else {	/* v6 */
			sin6 = (struct sockaddr_in6 *)&ifr->ifr_addr;
			addr = sin6->sin6_addr;	/* structure copy */
			bcopy(&addr, &(ptr->ipaddr), CL_IPV6_ADDR_LEN);
		}

		/* Save the flags of the logical IP address */
		ptr->flags = ifrflag.ifr_flags;

		/* Store the name of the adp also */
		ptr->name = (char *)strdup(name);
		if (ptr->name == NULL) {
			log_syserr_lib("out of memory.");
			goto nw_adp_logip_error;
		}

		ptr->inst = get_logical_inst(name);
	}

	(void) close(so);
	free(ifc.ifc_buf);

#ifdef LIBPNM_DEBUG
	nw_group_logip_print(head_ptr);
#endif
	return (head_ptr);

nw_adp_logip_error:

	/* Error cleanup */
	if (so > 0)
		(void) close(so);

	if (ifc.ifc_buf)
		free(ifc.ifc_buf);

	nw_group_logip_free(head_ptr);

	return (NULL);
}

/*
 * Get all IP addresses hosted on all the adapter instances - both physical
 * and logical IP addresses in this group. If group name is NULL then get all
 * the logical IP addresses (not loopback) hosted on this node. Returns a list
 * of l_addrs. On error returns NULL. Get the IP addresses for the given family.
 * INPUT: pointer to group or NULL, family(v4/v6)
 * RETURN: pointer to a list of l_addrs structures, NULL on error.
 */
static l_addr *
nw_group_logip_get(bkg_status *grp, sa_family_t af)
{
	l_addr *ptr, *last_ptr, *head_ptr;

	ptr = last_ptr = head_ptr = NULL;

	/* If the group is NULL get all logical IPs on this node */
	if (!grp)
		return (nw_adp_logip_get(NULL, af));

	ptr = nw_adp_logip_get(grp->name, af);

	return (ptr);
}

/*
 * Allocate memory for adp and zero it.
 * INPUT: NULL
 * RETURN: pointer to bkg_adp or NULL on error.
 */
static bkg_adp *
nw_adp_alloc()
{
	bkg_adp	*ptr;

	ptr = (bkg_adp *) malloc(sizeof (bkg_adp));
	if (ptr == NULL) {
		log_syserr_lib("out of memory.");
		return (NULL);
	}
	bzero(ptr, sizeof (bkg_adp));
	return (ptr);
}

/*
 * Find the adp corresponding to the adp->adp eg: eth0.
 * INPUT: char pointer to adp name, and pointer to group.
 * RETURN: pointer to struct bkg_adp, NULL if not found.
 */
static bkg_adp *
nw_adp_find(char *name, bkg_status *bkg)
{
	bkg_adp *adp;

	for (adp = bkg->head; adp != NULL; adp = adp->next) {
		if (strcmp(adp->adp, name) == 0)
			return (adp);
	}
	return (NULL);
}

/*
 * Append the given adp to the given group.
 * INPUT: pointer to bkg_adp struct, pointer to bkg_status struct
 * RETURN: void
 */
static void
nw_adp_append(bkg_status *group, bkg_adp *adp)
{
	if (group->head != NULL)
		adp->next = group->head;
	group->head = adp;
}

/*
 * Free this adp and remove it from the given group's adplist.
 * If this is the last adp in the group then make group->head = NULL;
 * INPUT: pointer to a bkg_adp struct (adp which needs to be removed )
 *	: pointer to a group structure
 * RETURN: void
 */
static void
nw_adp_delete(bkg_status *group, bkg_adp *adp)
{
	bkg_adp *prev;
	bkg_adp *current;

	/* Remove this adp from group->head adplist */
	if (group->head == adp) {
		if (group->head->next)
			group->head = group->head->next;
		else
			group->head = NULL;
	} else {
		for (prev = group->head, current = prev->next;
			current != NULL;
			prev = current, current = current->next) {
			if (current == adp) {
				prev->next = current->next;
				break;
			}
		}
	}
	/* Free the memory associated with this adp structure */
	nw_adp_free(adp);
}

/*
 * Free this adp structure and the pointer also.
 * INPUT: pointer to a bkg_adp struct (adp which needs to be removed )
 * RETURN: void
 */
static void
nw_adp_free(bkg_adp *adp)
{
	/* Free the memory associated with this adp structure */
	free(adp->adp);
	nw_adp_inst_free(adp->pii_v4);
	nw_adp_inst_free(adp->pii_v6);
	free(adp);
}

/*
 * Free this adp_inst structure.
 * INPUT: pointer to a adp_inst struct (adp_inst which needs to be removed )
 * RETURN: void
 */
static void
nw_adp_inst_free(adp_inst *adpi)
{
	if (adpi)
		free(adpi);
}

/*
 * Allocate a group struct and add it to the grouplist obkg.
 * INPUT:  pointer to pointer to bkg_status struct(pointer to grouplist in
 *         which to add the given group, i.e. pointer to nbkg/obkg),
 * RETURN: pointer to group added or NULL on error.
 */
static bkg_status *
nw_group_add(bkg_status **grp_list)
{
	bkg_status	*ptr;

	ptr = (bkg_status *) malloc(sizeof (bkg_status));
	if (ptr == NULL) {
		log_syserr_lib("out of memory.");
		return (NULL);
	}
	bzero((char *)ptr, sizeof (bkg_status));
	nw_group_append(grp_list, ptr);
	return (ptr);
}

/*
 * Append the given group struct to the grouplist obkg.
 * INPUT: pointer to pointer to bkg_status struct(pointer to grouplist in
 *        which to append)
 *        pointer to bkg_status struct (the group to append),
 * RETURN: void
 */
static void
nw_group_append(bkg_status **grp_list, bkg_status *group)
{
	if (*grp_list != NULL)
		group->next = *grp_list;
	*grp_list = group;
}

/*
 * Free this group structure and the pointer also.
 * INPUT: pointer to a bkg_status struct (bkg which needs to be removed )
 * RETURN: void
 */
static void
nw_group_free(bkg_status *group)
{
	bkg_adp *adp;
	bkg_adp *tmp;

	/* Remove the adp list in group */
	for (adp = group->head; adp != NULL; adp = tmp) {
		tmp = adp->next;
		nw_adp_delete(group, adp);
	}
	free(group->name);
	free(group);
}


/*
 * Free the l_addr structures list.
 * INPUT: pointer to l_addr list
 * RETURN: void
 */
static void
nw_group_logip_free(l_addr *list)
{
	l_addr	*lp = NULL, *lp_next = NULL;

	for (lp = list; lp; lp = lp_next) {
		lp_next = lp->next;
		free(lp->name);
		free(lp);
	}
}

/*
 * Takes a snapshot of the kernel adapters and form a grouplist. Update
 * the listhead to point to the grouplist head.
 * INPUT: void
 * RETURN: 0 on success or -1 on error.
 */
int
nw_group_config_read(bkg_status **grp_list)
{
	int			so_v4 = 0; /* socket descriptor */
	int			so_v6 = 0;
	int			so;
	uint_t			inst;	/* ordinal value of IP */
	char			*name;	/* name of adp */
	int			numreqs;
	struct ifconf		ifc;
	struct ifreq		*ifr;
	struct ifreq		ifrslave;
	int n;
	bkg_status		*match, *bgp;
	sa_family_t		af; /* family, v4/v6 */
	int 			bonding;
	bkg_adp			*match_adp, *ptr;
	ifbond			bond;
	ifslave			slave;

	/*
	 * Must be initialized to NULL, in order to avoid undesirable memory
	 * in case of error.
	 */
	ifc.ifc_buf = NULL;

	/*
	 * Here we should remember that we need two sockets for doing ioctls
	 * v4 and v6. First we do a SIOCGLIFCONF with AF_UNSPEC on a v4 socket
	 * and then for every instance (v4/v6) we must do the ioctls on the
	 * particular socket..either v4 or v6.
	 */
	/* Get kernel state into nbkg */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		goto nw_group_config_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		if (errno != EAFNOSUPPORT) {
			log_syserr_lib("socket failed.");
			goto nw_group_config_error;
		}
	}

	/* Get the number of interfaces */
	ifc.ifc_len = 0;
	if (ioctl(so_v4, SIOCGIFCONF, &ifc) < 0) {
	log_syserr_lib("SIOCGIFCONF failed.");
		goto nw_group_config_error;
	}
	numreqs = ifc.ifc_len / sizeof (struct ifreq);
	numreqs++;	/* assume 1 extra interface */

	ifc.ifc_len = sizeof (struct ifreq) * numreqs;
	ifc.ifc_buf = (char *)malloc(ifc.ifc_len);
	if (!ifc.ifc_buf) {
		log_syserr_lib("out of virtual memory.");
		goto nw_group_config_error;
	}

	if (ioctl(so_v4, SIOCGIFCONF, &ifc) < 0) {
		log_syserr_lib("SIOCGIFCONF failed.");
		goto nw_group_config_error;
	}

	ifr = ifc.ifc_req;
	for (n = 0; n < ifc.ifc_len; n += sizeof (struct ifreq), ifr++) {

		af = ifr->ifr_addr.sa_family;
		so = (af == AF_INET) ? so_v4 : so_v6;
		name = ifr->ifr_name;

		/*
		 * Returns 0 for physical adp, otherwise returns the
		 * ordinal value of the logical IP. eg. for
		 * tr2, it will return 0, whilst for tr2:3, it
		 * will return 3. get_logical_inst is defined in pnm_util.c.
		 */
		inst = get_logical_inst(name);

		/* We only want to check the physical adapters */
		if (inst != 0)
			continue;

		/*
		 * Check to see if this interface is a bonding one (bondx).
		 */
		bonding = 1;
		ifr->ifr_data = (char *)&bond;
		if (ioctl(so, SIOCBONDINFOQUERY, ifr) < 0) {
			if (errno != EOPNOTSUPP) {
				log_syserr_lib("SIOCBONDINFOQUERY on %s "
					" failed: %s\n",
					ifr->ifr_name, strerror(errno));
				goto nw_group_config_error;
			} else {
				/* not a bonding interface */
				bonding = 0;
			}
		}

		if (!bonding)
			continue;

		if (bond.num_slaves == 0)
			continue;

		/*
		 * Find group for this bonding interface
		 * If not exist, then create it.
		 */
		bgp = nw_group_find(name, *grp_list);
		if (!bgp) {
			/* Create a new group */
			if ((bgp = nw_group_add(grp_list)) == NULL)
				goto nw_group_config_error;
			bgp->name = (char *)strdup(name);
			if (bgp->name == NULL) {
				log_syserr_lib("out of memory.");
				goto nw_group_config_error;
			}
		}

		ifr->ifr_data = (char *)&slave;
		for (slave.slave_id = 0; slave.slave_id < bond.num_slaves;
			slave.slave_id ++) {
			ifr->ifr_data = (char *)&slave;
			if (ioctl(so, SIOCBONDSLAVEINFOQUERY, ifr) < 0) {
				log_syserr_lib("SIOCBONDSLAVEINFOQUERY, "
					" on %s failed: %s\n",
					ifr->ifr_name, strerror(errno));
				goto nw_group_config_error;
			}

			/*
			 * See if this adp is already there in the present
			 * *grp_list. match will be the group it is present in
			 * and match_adp will be the adapter itself.
			 */
			match_adp = NULL;
			for (match = *grp_list; match; match = match->next) {
				match_adp = nw_adp_find(slave.slave_name,
						match);
				if (match_adp)
					break;
			}

			/* Allocate adp structure if needed */
			if (!match_adp) {
				if ((ptr = nw_adp_alloc()) == NULL)
					goto nw_group_config_error;
				ptr->adp = (char *)strdup(slave.slave_name);
				if (ptr->adp == NULL) {
					log_syserr_lib("out of memory.");
					goto nw_group_config_error;
				}
				nw_adp_append(bgp, ptr);
			} else
				ptr = match_adp;

			/* Get adapter flags */
			strcpy(ifrslave.ifr_name, slave.slave_name);
			if (ioctl(so, SIOCGIFFLAGS, &ifrslave) < 0) {
				log_syserr_lib("SIOCGIFFLAGS, "
					" on %s failed: %s\n",
					ifrslave.ifr_name, strerror(errno));
				goto nw_group_config_error;
			}
			switch (slave.link) {
			case BOND_LINK_UP:
			case BOND_LINK_BACK:
				ptr->state = ADP_OK;
				break;
			case BOND_LINK_FAIL:
			case BOND_LINK_DOWN:
				ptr->state = ADP_NOTRUN;
				break;
			}
		}

		/*
		 * Information of V4/V6 addresses hosted by bond
		 * stored in first slave adapter.
		 */
		ptr = bgp->head;
		if (ptr != NULL) {
			if (af == AF_INET) {
				if (!ptr->pii_v4) {
					ptr->pii_v4 = (adp_inst *) malloc(
						sizeof (adp_inst));
					if (ptr->pii_v4 == NULL) {
					    log_syserr_lib("out of memory.");
					    goto nw_group_config_error;
					}
				}
				ptr->pii_v4->nLpi = 0;
			} else { /* AF_INET6 */
				if (!ptr->pii_v6) {
					ptr->pii_v6 = (adp_inst *) malloc(
						sizeof (adp_inst));
					if (ptr->pii_v6 == NULL) {
					    log_syserr_lib("out of memory.");
					    goto nw_group_config_error;
					}
				}
				ptr->pii_v6->nLpi = 0;
			}
		}
	}

	(void) close(so_v4);
	if (so_v6 > 0)
		(void) close(so_v6);
	free(ifc.ifc_buf);

	return (0);

nw_group_config_error:
	nw_group_config_free(*grp_list);

	if (so_v4 > 0)
		(void) close(so_v4);
	if (so_v6 > 0)
		(void) close(so_v6);

	if (ifc.ifc_buf)
		free(ifc.ifc_buf);
	return (-1);
}

/*
 * Free the given grouplist.
 * INPUT : pointer to pointer to bkg_status struct(grouplist)
 * RETURN :void
 */
void
nw_group_config_free(bkg_status *grp_list)
{
	bkg_status *bkg, *bkg_next;

	/* Free the group list */
	for (bkg = grp_list; bkg; bkg = bkg_next) {
		bkg_next = bkg->next;
		nw_group_free(bkg);
	}
}

/*
 * Find the group corresponding to the group->name eg: bond1.
 * INPUT: char pointer to group name, and pointer to grouplist.
 * RETURN: pointer to struct bkg_status, NULL if not found.
 */
struct bkg_status *
nw_group_find(char *gname, bkg_status *grp_list)
{
	bkg_status	*bkg;

	for (bkg = grp_list; bkg != NULL; bkg = bkg->next) {
		if (strcmp(bkg->name, gname) == 0)
			return (bkg);
	}
	return (NULL);
}

/*
 * Return the status of the group.
 * INPUT: pointer to the group.
 * RETURN: status of the group.
 */
int
nw_group_status(bkg_status *group)
{
	int status = PNM_STAT_DOWN; /* Assume group is down */
	bkg_adp *adp;

	for (adp = group->head; adp != NULL; adp = adp->next) {
		if (adp->state == ADP_OK) {
			status = PNM_STAT_OK;
			break;
		}
	}
	return (status);
}

/*
 * Return IPv4 or IPv6 depending on the address family used
 * for this bonding interface.
 * Returns 0 on success
 */
int
nw_group_instances(bkg_status *group, int *resultp)
{
	int result = 0;
	bkg_adp *adp;

	/*
	 * The first slave of the bond contains the information
	 * for the entire group.
	 */
	adp = group->head;
	if (adp) {
		if (adp->pii_v4)
			result |= PNMV4MASK;
		if (adp->pii_v6)
			result |= PNMV6MASK;
	}

	if (resultp != NULL)
		*resultp = result;

	return (0);
}

/*
 * Add a flag for adapter 'name'. Note that if 'value' is negative, the
 * action taken will be to OR out that flag. So -IFF_UP means take out
 * IFF_UP from the flags of the named adapter.
 */
static int
nw_adp_set_flags(int so, const char *name, int value)
{
	struct ifreq	ifr;

	(void) memset(&ifr, 0, sizeof (ifr));

	(void) strncpy(ifr.ifr_name, name, sizeof (ifr.ifr_name));
	if (ioctl(so, SIOCGIFFLAGS, (caddr_t)&ifr) < 0) {
		log_syserr_lib("SIOCGIFFLAGS failed.");
		return (-1);
	}
	if (value < 0) {
		value = -value;
		ifr.ifr_flags &= ~((uint_t)value);
	} else
		ifr.ifr_flags |= (uint_t)value;
	(void) strncpy(ifr.ifr_name, name, sizeof (ifr.ifr_name));
	if (ioctl(so, SIOCSIFFLAGS, (caddr_t)&ifr) < 0) {
		log_syserr_lib("SIOCSIFFLAGS failed.");
		return (-1);
	}
	return (0);
}

static int
nw_adp_up(int so, const char *name, sa_family_t af)
{
	return (nw_adp_set_flags(so, name, IFF_UP));
}

static int
nw_adp_down(int so, const char *name)
{
	return (nw_adp_set_flags(so, name, -IFF_UP));
}

static int
nw_adp_plumb_v4(int so, const char *name, struct in_addr *ip)
{
	struct ifreq		ifr;
	struct sockaddr_in	sinbuf, *sin = NULL;
	struct sockaddr_in	mask, bcast, *netmask = NULL;
	struct in_addr		addr;


	/* Make sure we're dealing with logical interface */
	if (strchr(name, ':') == NULL)
		return (-1);

	/* Make sure we're not setting broadcast addresses */
	(void) inet_pton(AF_INET, "255.255.255.255", &addr);
	if (ip->s_addr == addr.s_addr)
		return (-1);

	(void) memset(&ifr, 0, sizeof (ifr));

	/*
	 * Copy IP address into the ioctl structure ifr.
	 */
	sin = &sinbuf;
	sin->sin_family = AF_INET;
	sin->sin_addr.s_addr = ip->s_addr;
	bcopy((struct sockaddr_storage *)sin, &ifr.ifr_addr,
		sizeof (ifr.ifr_addr));
	ifr.ifr_addr.sa_family = AF_INET;

	/*
	 * Set IP address of logical interface.
	 */
	(void) strcpy(ifr.ifr_name, name);
	if (ioctl(so, SIOCSIFADDR, (caddr_t)&ifr) < 0) {
		log_syserr_lib("SIOCSIFADDR failed.");
		goto err;
	}

	/*
	 * Set netmask  ...
	 * getnetmaskbyaddr() could fail, in which case we will
	 * let the kernel assign the default netmask.
	 */
	if (getnetmaskbyaddr(sin->sin_addr, &mask.sin_addr) == 0)
		netmask = &mask;

	if (netmask != NULL) {
		netmask->sin_family = AF_INET;
		bcopy((struct sockaddr_storage *)netmask, &ifr.ifr_netmask,
			sizeof (ifr.ifr_netmask));
		ifr.ifr_addr.sa_family = AF_INET;
		if (ioctl(so, SIOCSIFNETMASK, (caddr_t)&ifr) < 0) {
			log_syserr_lib("SIOCSIFNETMASK failed.");
			goto err;
		}

		/*
		 * Set broadcast address  ...
		 */
		bcast.sin_family = AF_INET;
		(bcast.sin_addr).s_addr = ((sin->sin_addr).s_addr &
		(netmask->sin_addr).s_addr) | ~(netmask->sin_addr).s_addr;
		bcopy((struct sockaddr_storage *)&bcast, &ifr.ifr_broadaddr,
			sizeof (ifr.ifr_broadaddr));
		ifr.ifr_addr.sa_family = AF_INET;
		if (ioctl(so, SIOCSIFBRDADDR, (caddr_t)&ifr) < 0) {
			log_syserr_lib("SIOCSIFBRDADDR failed.");
			goto err;
		}
	}

	return (0);

err:
	return (-1);
}

static int
nw_adp_plumb_v6(int so, const char *name, struct in6_addr *ip)
{
	struct ifreq		ifr, ifrmask;
	struct sockaddr_in6 	sinbuf6, *sin6, netmask;

	/* Make sure we're dealing with logical interface */
	if (strchr(name, ':') == NULL)
		return (-1);

	(void) memset(&ifr, 0, sizeof (ifr));

	/*
	 * Copy IP address into the ioctl structure ifr.
	 */
	sin6 = &sinbuf6;
	sin6->sin6_family = AF_INET6;
	bcopy(ip, &(sin6->sin6_addr), CL_IPV6_ADDR_LEN);
	bcopy((struct sockaddr_storage *)sin6, &ifr.ifr_addr,
		sizeof (ifr.ifr_addr));
	ifr.ifr_addr.sa_family = AF_INET6;

	/* Set the netmask to be 64 bits */
	netmask.sin6_family = AF_INET6;
	(void) inet_pton(AF_INET6, "FFFF:FFFF:FFFF:FFFF::0",
		&(netmask.sin6_addr));
	bcopy((struct sockaddr_storage *)&netmask, &(ifrmask.ifr_addr),
		sizeof (ifrmask.ifr_addr));
	(void) strncpy(ifrmask.ifr_name, name, sizeof (ifrmask.ifr_name));
	ifrmask.ifr_addr.sa_family = AF_INET6;
	/* CSTYLED */
	if (ioctl(so, SIOCSIFNETMASK, (caddr_t)&ifrmask) < 0) { /*lint !e737 */
		log_syserr_lib("SIOCSIFNETMASK failed.");
		goto err;
	}

	/* Set IP address of logical interface. */
	(void) strncpy(ifr.ifr_name, name, sizeof (ifr.ifr_name));
	/* CSTYLED */
	if (ioctl(so, SIOCSIFADDR, (caddr_t)&ifr) < 0) { /*lint !e737 */
		log_syserr_lib("SIOCSIFADDR failed.");
		goto err;
	}

	return (0);

err:
	return (-1);
}

/*
 * Unplumb logical interface
 *	1) OR out IFF_UP from flags
 *	2) set address to 0
 * This should conform to all or none semantics.
 */
static int
nw_adp_unplumb(int so, const char *name, sa_family_t af)
{
struct ifreq	ifr;

	(void) memset(&ifr, 0, sizeof (ifr));
	ifr.ifr_addr.sa_family = AF_INET;

	/*
	 * Set flags to DOWN before setting addr to 0, so that this
	 * deleted entry won't show up in ifconfig(1M) output.
	 */
	if (nw_adp_set_flags(so, name, -IFF_UP) < 0) {
		return (-1);
	}

	return (0);
}

/*
 * sends out gratuitous ARP for a given IPv4 address, adapter
 * and open raw socket.
 * No more need on Solaris but needed for Linux as the kernel
 * does not do that for us.
 */
static int
nw_ipv4_send_arp(int so, char *adp, struct in_addr ip)
{
	struct arp_packet pkt;
	struct sockaddr sa;
	struct ifreq ifr;
	int skfd;

	pkt.frame_type = htons(ARP_FRAME_TYPE);
	pkt.hw_type = htons(ETHER_HW_TYPE);
	pkt.prot_type = htons(IP_PROTO_TYPE);
	pkt.hw_addr_size = IFHWADDRLEN;
	pkt.prot_addr_size = IP_ADDR_LEN;
	pkt.op = htons(ARPOP_REPLY);

	memset(pkt.dest_hw_addr, -1, IFHWADDRLEN);
	memset(&ifr, 0, sizeof (struct ifreq));
	strcpy(ifr.ifr_name, adp);

	skfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (ioctl(skfd, SIOCGIFHWADDR, (char *)&ifr) < 0) {
		log_syserr_lib("SIOCGIFHWADDR failed.");
		return (-1);
	}
	close(skfd);
	memcpy(pkt.src_hw_addr, ifr.ifr_hwaddr.sa_data, IFHWADDRLEN);

	memcpy(pkt.sndr_hw_addr, pkt.src_hw_addr, IFHWADDRLEN);
	memcpy(pkt.targ_hw_addr, pkt.src_hw_addr, IFHWADDRLEN);

	memcpy(pkt.sndr_ip_addr, &(ip.s_addr), IP_ADDR_LEN);
	memcpy(pkt.targ_ip_addr, &(ip.s_addr), IP_ADDR_LEN);

	bzero(pkt.padding, 18);

	strcpy(sa.sa_data, adp);

	if (sendto(so, &pkt, sizeof (pkt), 0, &sa, sizeof (sa)) < 0) {
		log_syserr_lib("sendto failed.");
		return (-1);
	}

	return (0);
}

/*
 * sends out an unsolicited neighbor advertisement for the given
 * IPv6 address, adapter and open raw socket
 * RFC 2461: Hop limit is set to 255 by default.
 */
static int
nw_ipv6_send_una(int so, char *adp, struct in6_addr ip)
{
	char buffer[256];
	struct nd_neighbor_advert *una;
	struct nd_opt_hdr *ndo;
	uint_t plen;
	struct sockaddr_in6 target, myaddr;
	struct ifreq ifr;
	int ifindex;

	bzero(buffer, sizeof (buffer));
	una = (struct nd_neighbor_advert *)buffer;
	ndo = (struct nd_opt_hdr *)(una + 1);

	/* our address, to be used as source address in the packet */
	bzero(&myaddr, sizeof (myaddr));
	myaddr.sin6_family = AF_INET6;
	myaddr.sin6_addr = ip;

	/* multicast target, to be used as destination of our ip6 packet */
	bzero(&target, sizeof (target));
	target.sin6_family = AF_INET6;
	if (inet_pton(AF_INET6, MULTICAST_TARGET, &target.sin6_addr) != 1) {
		log_syserr_lib("inet_pton failed.");
		return (-1);
	}

	/* setup data structure for unsolicited advertisement */
	una->nd_na_type = ND_NEIGHBOR_ADVERT;
	una->nd_na_code = 0;
	una->nd_na_target = myaddr.sin6_addr;
	una->nd_na_cksum = 0;

	/*
	 * The flag is a combination that specifies that:
	 * a) this node is not a router (SC nodes can not be routers)
	 * b) this advertisement is unsolicited
	 * c) recipients should update the link-layer address immediately
	 */
	una->nd_na_flags_reserved =
		~ND_NA_FLAG_ROUTER & ~ND_NA_FLAG_SOLICITED &
		ND_NA_FLAG_OVERRIDE;

	ndo->nd_opt_type = ND_OPT_TARGET_LINKADDR;

	(void) strncpy(ifr.ifr_name, adp, sizeof (ifr.ifr_name));
	if (ioctl(so, SIOCGIFINDEX, (char *)&ifr) < 0) {
		log_syserr_lib("SIOCGIFINDEX failed.");
		return (-1);
	}
	ifindex = ifr.ifr_ifindex;

	if (connect(so, (struct sockaddr *)&target, sizeof (target)) < 0) {
		log_syserr_lib("connect failed.");
		return (-1);
	}

	/* find out the hardware address where this ip is hosted */
	bzero(&ifr, sizeof (ifr));
	(void) strncpy(ifr.ifr_name, adp, sizeof (ifr.ifr_name));
	if (ioctl(so, SIOCGIFHWADDR, (char *)&ifr) < 0) {
		log_syserr_lib("SIOCGIFHWADDR failed.");
		return (-1);
	}
	bcopy(ifr.ifr_hwaddr.sa_data,
		(char *)(ndo + 1), IFHWADDRLEN);

	/* Length is specified in units of 8 octets */
	ndo->nd_opt_len = (uint8_t)(IFHWADDRLEN + 7) / 8;

	plen = sizeof (struct nd_neighbor_advert) + sizeof (struct nd_opt_hdr)
		+ (uint_t)IFHWADDRLEN;

	if (send(so, una, plen, 0) != (int)plen) {
		log_syserr_lib("send failed.");
		return (-1);
	}
	return (0);
}

/*
 * Mark UP a logical interface on an given group of
 * network interfaces.
 * INPUT: group name
 * RETURN: 0 on success, -num_failed , PNM error code otherwise.
 */
int
nw_ifconfig_up(bkg_status *grp, uint_t ip_cnt, struct in6_addr *ip_buf)
{
	uint_t i, una_cnt;
	int error = 0;
	char adpn[NAMELEN];
	l_addr *lp, *alist = NULL, *alist6 = NULL, *chlist = NULL;
	int num_failed = 0;
	int snba6 = 0, sarp4 = 0;
	int so, so_v4 = 0, so_v6 = 0;
	struct in6_addr *addr;
	struct in_addr ina;
	char abuf[INET6_ADDRSTRLEN];
	sa_family_t af;
	boolean_t v6_present = B_FALSE;

	/* Get all the Logical IPv4 addresses hosted on this group */
	alist = nw_group_logip_get(grp, AF_INET);

	/* Get all the Logical IPv6 addresses hosted on this group */
	alist6 = nw_group_logip_get(grp, AF_INET6);

	if (alist == NULL && alist6 == NULL) {
		log_err_lib("%s group %s not found",
			nw_disp_group_name(),  grp);
		error = PNM_EPROG;
		goto nw_ifconfig_up_error;
	}

	/* Open a v4 and a v6 socket for doing ioctls */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_up_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		if (errno != EAFNOSUPPORT) {
			log_syserr_lib("socket failed.");
			error = PNM_EPROG;
			goto nw_ifconfig_up_error;
		}
	}

	/*
	 * When we mark the logical IP address up we also want to let
	 * everybody else know about the new IP to MAC address mapping.
	 * In IPv4 the Solaris kernel does this for us by sending out
	 * gratuitous ARPs. However, in IPv6 the Solaris kernel does
	 * not do this and hence we have to send out unsolicited
	 * neighbor advertisements. We are allowed to send out 3
	 * such advertisements with a gap of 1s between each
	 * advertisement.
	 */
	if ((sarp4 = socket(AF_INET, SOCK_PACKET, htons(ETH_P_RARP))) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_up_error;
	}
	if ((snba6 = socket(AF_INET6, SOCK_RAW, IPPROTO_ICMPV6)) < 0) {
		if (errno != EAFNOSUPPORT) {
			log_syserr_lib("socket failed.");
			error = PNM_EPROG;
			goto nw_ifconfig_up_error;
		}
	}
	for (una_cnt = 0; una_cnt < ND_MAX_NEIGHBOR_ADVERTISEMENT;
		una_cnt++) {
		/* For all the given IP addresses.... */
		for (i = 0; i < ip_cnt; i++) {
			addr = &(ip_buf[i]);

			/* Find out if this is v4 or v6 */
			if (IN6_IS_ADDR_V4MAPPED(addr)) {
				chlist = alist;
				af = AF_INET;
				so = so_v4;
				IN6_V4MAPPED_TO_INADDR(addr, &ina);
				(void) inet_ntop(af, &ina, abuf,
					sizeof (abuf));
			} else {	/* v6 */
				chlist = alist6;
				af = AF_INET6;
				so = so_v6;
				v6_present = B_TRUE;
				(void) inet_ntop(af, addr, abuf,
					sizeof (abuf));
			}

			/* Find the given IP address in the list */
			for (lp = chlist; lp; lp = lp->next) {
				if (IN6_ARE_ADDR_EQUAL(addr,
					&(lp->ipaddr)))
					break; /* found */
			}

			/*
			 * If the address is found and it is already
			 * IPADDR_UP then try the next IP address
			 * - idempotent.
			 * Send a gratuitous ARP for v4.
			 * Send a unsolicited neighbor advertisement for v6.
			 */
			if ((lp != NULL) && (lp->flags & IPADDR_UP)
				== IPADDR_UP) {
				(void) strncpy(adpn, lp->name,
					strlen(lp->name));
				if (af == AF_INET) {
					(void) nw_ipv4_send_arp(sarp4,
						strtok(adpn, ":"),
						ina);
				} else if (af == AF_INET6) {
					(void) nw_ipv6_send_una(snba6,
						strtok(adpn, ":"),
						lp->ipaddr);
				}
				continue;  /* Next Ip addr, next i */
			}

			/*
			 * If we don't find the address, try the next IP
			 * address - don't error out
			 */
			if (lp == NULL)
				continue; /* Next Ip addr, next i */

			/*
			 * Set the flags to IPADDR_UP
			 * Send a gratuitous ARP for v4.
			 * Send a unsolicited neighbor advertisement for v6.
			 */
			if (nw_adp_up(so, lp->name, af) == 0) {
				(void) strncpy(adpn, lp->name,
					strlen(lp->name));
				if (af == AF_INET) {
					(void) nw_ipv4_send_arp(sarp4,
						strtok(adpn, ":"),
						ina);
				} else if (af == AF_INET6) {
					(void) nw_ipv6_send_una(snba6,
						strtok(adpn, ":"),
						lp->ipaddr);
				}
				lp->flags |= IPADDR_UP;
				continue; /* Next Ip addr, next i */
			}

			/*
			 * SCMSGS
			 * @explanation
			 * This means that the Logical IP address could not be
			 * set to UP.
			 * @user_action
			 * There could be other related error messages which
			 * might be helpful. Contact your authorized Sun
			 * service provider to determine whether a workaround
			 * or patch is available.
			 */
			(void) sc_syslog_msg_log(msg_libpnm,
				SC_SYSLOG_ERROR, MESSAGE,
				"%s can't UP %s.", grp->name, abuf);
			num_failed++;
		}	/* end of for - IP addresses */
		/*
		 * We will break out of the una_cnt loop if we
		 * have already sent ND_MAX_NEIGHBOR_ADVERTISEMENT
		 * advertisements
		 */
		if (una_cnt == (ND_MAX_NEIGHBOR_ADVERTISEMENT - 1))
			break; /* out of una_cnt loop */
		(void) usleep(ND_RETRANS_TIMER * 1000);
	}	/* end of for - una_cnt */

	(void) close(sarp4);
	if (snba6)
		(void) close(snba6);

nw_ifconfig_up_error:
	/*
	 * We make error to be the negative of the number of IP addrs that we
	 * were not able to up.
	 */
	if (num_failed > 0)
		error = -1 * num_failed;

	if (alist)
		nw_group_logip_free(alist);
	if (alist6)
		nw_group_logip_free(alist6);

	if (so_v4)
		(void) close(so_v4);
	if (so_v6)
		(void) close(so_v6);
	if (sarp4)
		(void) close(sarp4);

	return (error);
}

/*
 * Mark DOWN a logical interface on an given group of
 * network interfaces.
 * RETURN: 0 on success, -num_failed , PNM error code otherwise.
 */
int
nw_ifconfig_down(uint_t ip_cnt, struct in6_addr *ip_buf)
{
	uint_t i;
	int error = 0;
	l_addr *lp, *alist = NULL, *alist6 = NULL, *chlist = NULL;
	int num_failed = 0;
	int so, so_v4 = 0, so_v6 = 0;
	struct in6_addr *addr;
	struct in_addr ina;
	char abuf[INET6_ADDRSTRLEN];
	sa_family_t af;

	/* Get all the Logical Ip addresses hosted on this node */
	alist = nw_group_logip_get(NULL, AF_INET);

	/* Get all the Logical IPv6 addresses hosted on this node */
	alist6 = nw_group_logip_get(NULL, AF_INET6);

	if (alist == NULL && alist6 == NULL) {
		log_err_lib("no public adapters on this node");
		error = PNM_EPROG;
		goto nw_ifconfig_down_error;
	}

	/* Open a v4 and a v6 socket for doing ioctls */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_down_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		if (errno != EAFNOSUPPORT) {
			log_syserr_lib("socket failed.");
			error = PNM_EPROG;
			goto nw_ifconfig_down_error;
		}
	}

	/* For all the given IP addresses.... */
	for (i = 0; i < ip_cnt; i++) {
		addr = &(ip_buf[i]);

		/* Find out if this is v4 or v6 */
		if (IN6_IS_ADDR_V4MAPPED(addr)) {	/* v4 */
			chlist = alist;
			af = AF_INET;
			so = so_v4;
			IN6_V4MAPPED_TO_INADDR(addr, &ina);
			(void) inet_ntop(af, &ina, abuf,
				sizeof (abuf));
		} else {	/* v6 */
			chlist = alist6;
			af = AF_INET6;
			so = so_v6;
			(void) inet_ntop(af, addr, abuf,
				sizeof (abuf));
		}

		/* Find the given IP address in the list */
		for (lp = chlist; lp; lp = lp->next) {
			if (IN6_ARE_ADDR_EQUAL(addr, &(lp->ipaddr)))
				break; /* found */
		}

		/*
		 * If the address is found and it is already DOWN
		 * then try the next IP address - idempotent.
		 */
		if ((lp != NULL) && (lp->flags & IPADDR_UP)
			!= IPADDR_UP)
			continue; /* Next Ip addr, next i */

		/*
		 * If we don't find the address, try the next IP
		 * address - don't error out
		 */
		if (lp == NULL)
			continue; /* Next Ip addr, next i */

		/* Set the flags to DOWN */
		if (nw_adp_down(so, lp->name) == 0)
			continue; /* Next IP addr, next i */

		/*
		 * SCMSGS
		 * @explanation
		 * This means that the Logical IP address could not be set to
		 * DOWN.
		 * @user_action
		 * There could be other related error messages which might be
		 * helpful. Contact your authorized Sun service provider to
		 * determine whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(msg_libpnm, SC_SYSLOG_ERROR,
			MESSAGE, "%s can't DOWN.", abuf);
		num_failed++;
	}

nw_ifconfig_down_error:
	/*
	 * We make error to be the negative of the number of IP addrs that we
	 * were not able to down.
	 */
	if (num_failed > 0)
		error = -1 * num_failed;

	if (alist)
		nw_group_logip_free(alist);
	if (alist6)
		nw_group_logip_free(alist6);

	if (so_v4)
		(void) close(so_v4);
	if (so_v6)
		(void) close(so_v6);

	return (error);
}

/*
 * Plumb logical interfaces on an given group of
 * network interfaces.
 * OUTPUT: num_failed is the number of IP addresses of which ifconfig failed.
 * RETURN: 0 on success, -num_failed , PNM error code otherwise.
 */
int
nw_ifconfig_plumb(bkg_status *grp, const char *zone, uint_t ip_cnt,
    struct in6_addr *ip_buf)
{
	uint_t i;
	int error = 0;
	char Lname[NAMELEN];
	l_addr *lp, *alist = NULL, *alist6 = NULL, *chlist = NULL;
	int *freelist = NULL;
	uint_t freelen = 0, find;
	int num_failed = 0;
	int so_v4 = 0, so_v6 = 0;
	char *name, *first_tok;
	struct in6_addr *addr;
	struct in_addr ina;
	char abuf[INET6_ADDRSTRLEN];
	sa_family_t af;

	if (zone != NULL) {
		log_err_lib("No Solaris zone on Linux");
		error = PNM_EPROG;
		goto nw_ifconfig_plumb_error;
	}
	/* Get all the Logical IPv4 addresses hosted on this group */
	alist = nw_group_logip_get(grp, AF_INET);

	/* Get all the Logical IPv6 addresses hosted on this group */
	alist6 = nw_group_logip_get(grp, AF_INET6);

	if (alist == NULL && alist6 == NULL) {
		log_err_lib("%s group %s not found",
			nw_disp_group_name(),  grp);
		error = PNM_EPROG;
		goto nw_ifconfig_plumb_error;
	}

	/* Open a v4 and a v6 socket for doing ioctls */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_plumb_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		if (errno != EAFNOSUPPORT) {
			log_syserr_lib("socket failed.");
			error = PNM_EPROG;
			goto nw_ifconfig_plumb_error;
		}
	}

	/* For all the given IP addresses.... */
	for (i = 0; i < ip_cnt; i++) {
		addr = &ip_buf[i];

		/* Find out if this addr is v4 or v6 */
		if (IN6_IS_ADDR_V4MAPPED(addr)) {	/* v4 */
			chlist = alist;
			af = AF_INET;
			IN6_V4MAPPED_TO_INADDR(addr, &ina);
			(void) inet_ntop(af, &ina, abuf, sizeof (abuf));
		} else {	/* v6 */
			chlist = alist6;
			af = AF_INET6;
			(void) inet_ntop(af, addr, abuf, sizeof (abuf));
		}

		/*
		 * Check if this IP address is already hosted.
		 * If this is the case then we do nothing
		 * For this we should check all the LIps in
		 * group.
		 */
		for (lp = chlist; lp; lp = lp->next) {
			if (IN6_ARE_ADDR_EQUAL(addr, &(lp->ipaddr)))
				break;
		}

		/*
		 * If address is already hosted then try the next
		 * address. Hence this call is idempotent.
		 */
		if (lp != NULL)
			continue;


		/*
		 * Find all logical instances that are
		 * currently in use on grp->name. Mark them '-1'
		 * in the freelist array.
		 */
		freelen = MAX_NUMIFS;
		freelist = (int *)calloc(freelen, sizeof (int));
		if (freelist == NULL) {
			log_syserr_lib("out of memory.");
			error = PNM_EPROG;
			goto nw_ifconfig_plumb_error;
		}
		for (lp = chlist; lp; lp = lp->next, free(name)) {
			name = (char *)strdup(lp->name);
			first_tok = (char *)strtok(name, ":");
			if (first_tok == NULL)
				continue;
			if (strcmp(first_tok, grp->name) != 0)
				continue;
			if (lp->inst < freelen)
				freelist[lp->inst] = -1;
		}

		/* Find the next free instance number */
		for (find = 0; find < freelen; find++) {
			if (freelist[find] != -1)
				break;
		}

		/* If no more free instance numbers */
		if (find == freelen) {
			/*
			 * SCMSGS
			 * @explanation
			 * This means that we have reached the maximum number
			 * of Logical IPs allowed on an adapter.
			 * @user_action
			 * This can be increased by increasing the ndd
			 * variable ip_addrs_per_if. However the maximum limit
			 * is 8192. The default is 256.
			 */
			(void) sc_syslog_msg_log(msg_libpnm,
				SC_SYSLOG_ERROR, MESSAGE,
				"%s can't plumb %s: "
				"out of instance numbers on %s.",
				grp->name, abuf, grp->name);
			num_failed++;
			continue; /* Next Ip addr, next i */
		}

		(void) sprintf(Lname, "%s:%d", grp->name, find);

		/* Now plumb the LIp */
		if (af == AF_INET &&	/* v4 */
			nw_adp_plumb_v4(so_v4, Lname, &ina) == 0) {
			/* Add a new laddr record */
			lp = (l_addr *) malloc(sizeof (l_addr));
			if (lp == NULL) {
				log_syserr_lib("out of memory.");
				error = PNM_EPROG;
				goto nw_ifconfig_plumb_error;
			}
			bcopy(addr, &(lp->ipaddr), CL_IPV6_ADDR_LEN);
			lp->flags = 0; /* Initialize to 0 */
			lp->name = strdup(Lname);
			if (lp->name == NULL) {
				log_syserr_lib("out of memory.");
				error = PNM_EPROG;
				goto nw_ifconfig_plumb_error;
			}
			lp->inst = find;
			lp->next = alist;
			alist = lp;
			freelist[find] = -1;
			continue; /* Next Ip addr, next i */
		}
		if (af == AF_INET6 &&	/* v6 */
			nw_adp_plumb_v6(so_v6, Lname, addr) == 0) {
			/* Add a new laddr record */
			lp = (l_addr *) malloc(sizeof (l_addr));
			if (lp == NULL) {
				log_syserr_lib("out of memory.");
				error = PNM_EPROG;
				goto nw_ifconfig_plumb_error;
			}
			bcopy(addr, &(lp->ipaddr), CL_IPV6_ADDR_LEN);
			lp->flags = 0; /* Initialize to 0 */
			lp->name = strdup(Lname);
			if (lp->name == NULL) {
				log_syserr_lib("out of memory.");
				error = PNM_EPROG;
				goto nw_ifconfig_plumb_error;
			}
			lp->inst = find;
			lp->next = alist6;
			alist6 = lp;
			freelist[find] = -1;
			continue; /* Next IP addr, next i */
		}

		/*
		 * Here we might want to try the next adapter in the
		 * group since maybe we will be able to plumb a
		 * Logical IP on the next adapter - but we don't want
		 * to do this since we assume that if an adapter is UP
		 * then it is able to host an extra Logical IP address,
		 * and if it can't then it is an error.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * This means that the Logical IP address could not be plumbed
		 * on an adapter belonging to the named IPMP group.
		 * @user_action
		 * There could be other related error messages which might be
		 * helpful. Contact your authorized Sun service provider to
		 * determine whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(msg_libpnm, SC_SYSLOG_ERROR,
			MESSAGE, "%s can't plumb %s.",
			grp->name, abuf);
		num_failed++;
	}

nw_ifconfig_plumb_error:

	/*
	 * We make error to be the negative of the number of IP addrs that we
	 * were not able to plumb.
	 */
	if (num_failed > 0)
		error = -1 * num_failed;

	if (freelist != NULL) {
		free(freelist);
		freelist = NULL;
	}
	if (alist)
		nw_group_logip_free(alist);
	if (alist6)
		nw_group_logip_free(alist6);

	if (so_v4)
		(void) close(so_v4);
	if (so_v6)
		(void) close(so_v6);

	return (error);
}

/*
 * Unplumb a logical interface from a given group of
 * network interfaces.
 * OUTPUT: num_failed is the number of IP addresses of which ifconfig failed.
 * RETURN: 0 on success, -num_failed , PNM error code otherwise.
 */
int
nw_ifconfig_unplumb(uint_t ip_cnt, struct in6_addr *ip_buf)
{
	uint_t i;
	int error = 0;
	l_addr *lp, *alist = NULL, *alist6 = NULL, *chlist = NULL;
	int num_failed = 0;
	int so, so_v4 = 0, so_v6 = 0;
	struct in6_addr *addr;
	struct in_addr ina;
	char abuf[INET6_ADDRSTRLEN];
	sa_family_t af;

	/* Get all the Logical Ip addresses hosted on this node */
	alist = nw_group_logip_get(NULL, AF_INET);

	/* Get all the Logical IPv6 addresses hosted on this node */
	alist6 = nw_group_logip_get(NULL, AF_INET6);

	if (alist == NULL && alist6 == NULL) {
		log_err_lib("no public adapters on this node");
		error = PNM_EPROG;
		goto nw_ifconfig_unplumb_error;
	}

	/* Open a v4 and a v6 socket for doing ioctls */
	if ((so_v4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		log_syserr_lib("socket failed.");
		error = PNM_EPROG;
		goto nw_ifconfig_unplumb_error;
	}
	if ((so_v6 = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		if (errno != EAFNOSUPPORT) {
			log_syserr_lib("socket failed.");
			error = PNM_EPROG;
			goto nw_ifconfig_unplumb_error;
		}
	}

	/* For all the given IP addresses.... */
	for (i = 0; i < ip_cnt; i++) {
		addr = &(ip_buf[i]);

		/* Find out if this addr is v4 or v6 */
		if (IN6_IS_ADDR_V4MAPPED(addr)) {	/* v4 */
			chlist = alist;
			af = AF_INET;
			so = so_v4;
			IN6_V4MAPPED_TO_INADDR(addr, &ina);
			(void) inet_ntop(af, &ina, abuf,
				sizeof (abuf));
		} else {	/* v6 */
			chlist = alist6;
			af = AF_INET6;
			so = so_v6;
			(void) inet_ntop(af, addr, abuf,
				sizeof (abuf));
		}

		/* Find the given IP address in the list */
		for (lp = chlist; lp; lp = lp->next) {
			if (IN6_ARE_ADDR_EQUAL(addr, &(lp->ipaddr)))
				break; /* found */
		}

		/*
		 * If we don't find the address, try the next IP
		 * address - don't error out - idempotent.
		 */
		if (lp == NULL)
			continue; /* Next Ip addr, next i */

		if (nw_adp_unplumb(so, lp->name, af) == 0)
			continue; /* Next Ip addr, next i */

		/*
		 * SCMSGS
		 * @explanation
		 * This means that the Logical IP address could not be
		 * unplumbed from an adapter.
		 * @user_action
		 * There could be other related error messages which might be
		 * helpful. Contact your authorized Sun service provider to
		 * determine whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(msg_libpnm, SC_SYSLOG_ERROR,
			MESSAGE, "%s can't unplumb.", abuf);
		num_failed++;
	}

nw_ifconfig_unplumb_error:
	/*
	 * We make error to be the negative of the number of IP addrs that we
	 * were not able to unplumb.
	 */
	if (num_failed > 0)
		error = -1 * num_failed;

	if (alist)
		nw_group_logip_free(alist);
	if (alist6)
		nw_group_logip_free(alist6);

	if (so_v4)
		(void) close(so_v4);
	if (so_v6)
		(void) close(so_v6);

	return (error);
}
