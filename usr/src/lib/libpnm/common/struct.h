/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _STRUCT_H
#define	_STRUCT_H

#pragma ident	"@(#)struct.h	1.6	08/05/20 SMI"

/*
 *     	Following code is the header file for libpnm
 */

#ifdef __cplusplus
extern "C" {
#endif

#define	ADP_OK		0	/* adapter is ok */
#define	ADP_FAULTY	1	/* adapter is dead IFF_FAILED = 1 */
#define	ADP_STANDBY	2	/* adapter is STANDBY and INACTIVE */
#define	ADP_OFFLINE	3	/* adapter is OFFLINE */
#define	ADP_NOTRUN	4	/* adapter has no carrier, IFF_RUNNING = 0 */


typedef struct log_addr {
	struct in6_addr		ipaddr;
	char			*name; /* adp name eg: hme0:1 */
	uint64_t		flags; /* flags of the logical Ip */
	uint_t			inst;
	struct log_addr		*next; /* next l_addr pointer */
}	l_addr;

typedef struct adp_inst {
	uint_t		nLpi; /* No. of Logical Ip addresses */
}	adp_inst;

typedef struct bkg_adp {
	char		*adp; /* eg: hme0 */
	int		state; /* NOTRUNNING, FAULTY, STANDBY, OFFLINE, OK */
	adp_inst	*pii_v4; /* pointer to v4 instance */
	adp_inst	*pii_v6; /* pointer to v6 instance */
	struct bkg_adp	*next; /* pointer to next bkg_adp */
}	bkg_adp;

typedef struct bkg_status {	/* struct for ipmp group */
	char			*name; /* ipmp group name - SIOCLIFGROUPNAME */
	bkg_adp			*head; /* pointer to the adapter list */
	struct bkg_status	*next; /* pointer to next bkg_status group */
}	bkg_status;

#ifdef __cplusplus
}
#endif

#endif	/* _STRUCT_H */
