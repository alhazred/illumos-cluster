#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident	"@(#)Makefile.com	1.30	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libpnm/Makefile.com
#

LIBRARY= libpnm.a
VERS= .1

include ../../../Makefile.master

COMMON_OBJS = libpnm.o pnm_log.o pnm_util.o pnm_farm_util.o
SPEC_OBJS = pnm_network_solaris_ipmp.o

OBJECTS = $(COMMON_OBJS) $(SPEC_OBJS)

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

ROOTRGMHDRDIR =	$(ROOTCLUSTHDRDIR)/rgm
PNMHDRDIR = 	$(SRC)/head

MAPFILE=	../common/mapfile-vers
SRCS =		$(OBJECTS:%.o=../common/%.c)

LINTFILES += $(OBJECTS:%.o=%.ln)

LIBS = $(DYNLIB) $(LIBRARY)

MTFLAG = -mt

CPPFLAGS += $(MTFLAG) 

CPPFLAGS += -I../common -I$(ROOTRGMHDRDIR) -I$(ROOTCLUSTHDRDIR) \
	    -I$(PNMHDRDIR)
CPPFLAGS += -I$(SRC)/lib/dlscxcfg/common

LDLIBS += -lscha -lnsl -lc -lsocket -lclos -ldl -ldlscxcfg

PMAP =	-M $(MAPFILE)

.KEEP_STATE:

all:  $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
