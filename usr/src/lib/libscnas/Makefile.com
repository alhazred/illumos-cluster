#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.com	1.7	08/05/20 SMI"
#
# lib/libscnas/Makefile.com
#

LIBRARY= libscnas.a
VERS=.1

OBJECTS= \
	scnas_misc.o  \
	scnas_props.o \
	scnas_filer.o

# include library definitions
include ../../Makefile.lib

MAPFILE=	../common/mapfile-vers
SRCS=		$(OBJECTS:%.o=../common/%.c)

LIBS =		$(DYNLIB) $(LIBRARY)

LINTFLAGS +=	-I..
LINTFILES +=	$(OBJECTS:%.o=%.ln)

MTFLAG	=	-mt
CPPFLAGS +=	-v -I..
CPPFLAGS +=	-I$(SRC)/lib/libclcomm/common
CPPFLAGS +=	-I$(SRC)/head/cl_query

PMAP =	-M $(MAPFILE)

# Warning: new dependencies here should be marked in cmd/scrconf/Makefile also
# to build the static command
LDLIBS +=	-lc -ldl -lclccrad -lscconf

.KEEP_STATE:

$(DYNLIB):	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
