/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCNAS_H
#define	_SCNAS_H

#pragma ident	"@(#)scnas.h	1.7	08/05/20 SMI"

/*
 * The header file for libscnas.
 */

#ifdef __cplusplus
extern "C" {
#endif


#include <unistd.h>
#include <sys/types.h>
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <locale.h>
#include <cl_query_types.h>

/* CLI options */
#define	SUBOPT_FILERNAME	"-h filer_name"
#define	SUBOPT_FILERTYPE	"-t type"
#define	SUBOPT_SPEC_OPTIONS	"-o specific_options"
#define	SUBOPT_DIRECTORIES	"-d directory"

/* CCR key words for NAS properties */
#define	PROP_FILER_NAME		"NAS_ServiceName"
#define	PROP_FILER_TYPE		"NAS_ServiceType"
#define	PROP_DIRECTORY		"NAS_Directories"
#define	PROP_USERID		"NAS_UserName"
#define	PROP_PASSWD		"NAS_Passwd"

/* Maximum string length */
#define	SCNAS_MAXSTRINGLEN	1024

/* NAS service table */
#define	NAS_SERVICE_KEY_TABLE	"NAS_Service_keys"
#define	NAS_SERVICE_TABLE_PRE	"NAS_Service"

/* Flags */
#define	SCNAS_CMD		1
#define	SCNASDIR_CMD		2
#define	SCNAS_CMD_ADD		1
#define	SCNAS_CMD_CHANGE	2
#define	SCNASDIR_CMD_ADD	3
#define	SCNASDIR_CMD_RM		4

#define	GET_TABLE_CONTENTS	1
#define	GET_TABLE_BY_KEY	2

#define	NAS_FOUND_KEY		1
#define	NAS_NOTFOUND_KEY	0

#define	KEY_INDEX_FORM		"%d"
#define	DIR_INDEX_FORM		"NAS_Directories_%d"

/* NAS property names in command line or XML file */
#define	NAS_FILER_TAG		"filer"
#define	NAS_FILER_NAME		"name"
#define	NAS_FILER_TYPE		"type"
#define	NAS_DIRECTORIES		"directories"
#define	NAS_USERID		"userid"
#define	NAS_PASSWD		"passwd"
#define	NASDIR_ALL_DIR		"all"

/* XML related definitions */
#define	SCNAS_XML_SKIPS		"filer,directories,"
#define	SCNASDIR_XML_SKIPS	"filer,type,userid,passwd,"
#define	XML_RECORD_END		9

/* Error codes returned by scnas functions */
typedef enum scnas_errno {
	SCNAS_NOERR = 0,		/* normal return - no error */
	SCNAS_EPERM = 1,		/* permission denied */
	SCNAS_EEXIST = 2,		/* object already exists */
	SCNAS_ENOEXIST = 3,		/* object does not exist */
	SCNAS_ESETUP = 4,		/* object or handle is stale */
	SCNAS_EUNKNOWN = 5,		/* unkown type */
	SCNAS_ENOCLUSTER = 6,		/* cluster does not exist */
	SCNAS_ENODEID = 7,		/* ID used in place of node name */
	SCNAS_EINVAL = 8,		/* invalid argument */
	SCNAS_EUSAGE = 9,		/* command usage error */
	SCNAS_EDIREXIST = 10,		/* directories still exist */
	SCNAS_EAUTH = 11,		/* authentication error */
	SCNAS_ENOMEM = 12,		/* not enough memory */
	SCNAS_EUNEXPECTED = 13,		/* internal or unexpected error */
	SCNAS_EINVALPROP = 14,		/* invalid filer property */
	SCNAS_NO_BINARY = 17,		/* no netapp binary */
	SCNAS_ERECONFIG = 28,		/* cluster is reconfiguring */
	SCNAS_VP_MISMATCH = 29,		/* running verions not match */
	SCNAS_ENOFILE = 30		/* file doesn't exist */
} scnas_errno_t;

/* Filer properties */
typedef struct scnas_filer_prop {
	char			*scnas_prop_key;
	char			*scnas_prop_value;
	struct scnas_filer_prop	*scnas_prop_next;
} scnas_filer_prop_t;

/* Properties of a given filer */
typedef struct filer_prop {
	char			*filer_name;
	char			*filer_passwd;
	char			*filer_uid;
	char			*filer_type;
	uint_t			filer_dirs_count;
	char			**filer_dirs;
} filer_prop_t;

/*
 * Filer management functions
 *
 *	scnas_attach_filer
 *	scnas_remove_filer
 *	scnas_remove_filer_table
 *	scnas_remove_filer_dir
 *	scnas_change_filer
 *	scnas_read_filer_table
 *	scnas_display_filer_props
 *	scnas_get_filer_tablename
 *	scnasdir_set_filer_prop
 *	scnas_print_one_filer
 *	scnas_print_filer_by_type
 *	scnas_print_filer_all
 */

/*
 * scnas_attach_filer
 *
 *	Validate the given filer "type", and forward the adding
 *	filer request to the attaching filer function in the NAS
 *	library of this "type".
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_EEXIST		- filer is already attached
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNKNOWN		- unknow type
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_VP_MISMATCH	- version not match
 *	SCNAS_NO_BINARY		- no required binary
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
extern scnas_errno_t scnas_attach_filer(char *filer, char *type,
    scnas_filer_prop_t *proplist_p, char **messages);

/*
 * scnas_remove_filer
 *
 *	Remove the NAS filer. If the "flag" is set, removes the
 *	filer even its directories still exist. If it is not set,
 *	only remove the filer if the directories do not exist.
 *	This "Force" flag only exists for the new clnasdevice
 *	command.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_EDIREXIST		- still has directories
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
extern scnas_errno_t scnas_remove_filer(char *filer_name,
    scnas_filer_prop_t *proplist_p, char **messages, uint_t flag);

/*
 * scnas_remove_filer_table
 *
 *	Remove the NAS filer ccr table. If the "flag" is set, removes
 *	the filer even its directories still exist. If it is not set,
 *	only remove the filer if the directories do not exist.
 *	This "Force" flag only exists for the new clnasdevice
 *	command.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_EDIREXIST		- still has directories
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
extern scnas_errno_t scnas_remove_filer_table(char *filername,
    char *filer_table_name, char *nas_type, scnas_filer_prop_t *props,
    char **messages, uint_t flag);

/*
 * scnas_remove_filer_dir
 *
 *	Remove directories from a NAS filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_EDIREXIST		- still has directories
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
extern scnas_errno_t scnas_remove_filer_dir(char *filername,
    char *filer_table_name, scnas_filer_prop_t *prop_list,
    char **messages, char *dir_all, uint_t flag);

/*
 * scnas_change_filer
 *
 *	Update the properties of a filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
extern scnas_errno_t scnas_change_filer(char *filer,
    scnas_filer_prop_t *proplist_p, char **messages);

/*
 * scnas_check_nas_devicetype
 *
 *	This functions dlopen's the NAS library which corresponds to
 *	the given type. If it can find such a NAS library, the given
 *	device type is valid; Otherwise it is not a valid type. It
 *	also sets *props_required to true if the type of NAS requires
 *	certain properties, and set *props_required to false if not.
 *
 * Possible return values:
 *
 *	B_TRUE		The type is valid
 *	B_FALSE		Unknown type
 */
extern boolean_t
scnas_check_nas_devicetype(const char *type, boolean_t *props_required);

/*
 * scnas_check_nas_proprequired
 *
 *	Check if the given NAS filer name is valid, and returns
 *	true if it is, otherwise returns false. For valid NAS device,
 *	it also sets *props_required to true if the type of NAS requires
 *	certain properties, and set *props_required to false if not.
 *
 * Possible return values:
 *
 *	B_TRUE		The type is valid
 *	B_FALSE		Unknown type
 */
extern boolean_t
scnas_check_nas_proprequired(const char *nas_name, boolean_t *props_required);

/*
 * scnas_read_filer_ccr
 *
 *	Read the given filer CCR table into the cl_query_table_t
 *	structure, by the flag. For flag GET_TABLE_BY_KEY,
 *	only fill in the cl_query_table_t structure if the filer
 *	has a property matches the given key/value pair; For
 *	flag GET_TABLE_CONTENTS, just read the table and ignore
 *	the key/value.
 *
 *	Caller is responsible to free the memory allocated for
 *	cl_query_table_t.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_ENOCLUSTER	- cluster does not exist
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 *	SCNAS_EUNEXPECTED	- unexpected error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOEXIST		- table does not exist
 */
extern scnas_errno_t scnas_read_filer_table(
    char *table_name, char *key, char *value,
    cl_query_table_t *table_contents, uint_t flag);

/*
 * scnas_display_filer_props
 *
 *	Display the xml filer record data.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EUNEXPECTED	- unexpected error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNKNOWN		- unknown type
 *	SCNAS_ENOMEM		- out of memory
 */
extern scnas_errno_t
scnas_display_filer_props(FILE *out, char *filer_name, char *filer_type,
    scnas_filer_prop_t *prop_list, char **messages, uint_t cmd_flg);

/*
 * scnas_get_filer_tablename
 *
 *	For the given filer name, return its table name.
 *	Caller is responsible to free the memory allocated for
 *	the returned filer table name.
 */
extern char *
scnas_get_filer_tablename(char *filer_name);

/*
 * scnasdir_set_filer_prop
 *
 *	Make changes to the directories configuration of a filer.
 *	The change is either adding or removing depends on the
 *	flag setting.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
extern scnas_errno_t scnasdir_set_filer_prop(char *filer_name,
    scnas_filer_prop_t *prop_list, char **messages,
    char *dir_all, uint_t flag);

/*
 * scnas_print_one_filer
 *
 *	Print the properties of one filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EUNEXPECTED	- unexpected error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOEXIST		- object not exists
 */
extern scnas_errno_t
scnas_print_one_filer(char *filer_name, uint_t flg);

/*
 * scnas_get_one_filer
 *
 *	Get the properties of one filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EUNEXPECTED	- unexpected error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_ENOMEM		- out of memory
 */
extern scnas_errno_t
scnas_get_one_filer(char *filer_name, filer_prop_t **prop);

/*
 * scnas_print_filer_by_type
 *
 *	Print properties of filers of one type.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EUNEXPECTED	- unexpected error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_EUNKNOWN		- unknown type
 *	SCNAS_ENOMEM		- out of memory
 */
extern scnas_errno_t
scnas_print_filer_by_type(char *filer_type, uint_t flg);

/*
 * scnas_get_filer_by_type
 *
 *	Get the properties of filers of one type.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EUNEXPECTED	- unexpected error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_EUNKNOWN		- unknown type
 *	SCNAS_ENOMEM		- out of memory
 */
extern scnas_errno_t
scnas_get_filer_by_type(char *filer_type, filer_prop_t ***prop, uint_t *count);

/*
 * scnas_print_filer_all
 *
 *	Print all filers' properties.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EUNEXPECTED	- unexpected error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_ENOMEM		- out of memory
 */
extern scnas_errno_t
scnas_print_filer_all(uint_t flg);

/*
 * scnas_get_filer_all
 *
 *	Get all filers' properties.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EUNEXPECTED	- unexpected error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_ENOMEM		- out of memory
 */
extern scnas_errno_t
scnas_get_filer_all(filer_prop_t ***prop, uint_t *count);

/*
 * Property management functions
 *
 *	scnas_free_proplist
 *	scnas_get_indexes
 *	scnas_next_index
 *	scnas_get_prop_val
 *	scnas_get_prop_key
 *	scnas_add_one_prop
 *	scnas_clquery_add_elem
 *	scnas_clquery_add_props
 *	scnas_clquery_set_props
 *	scnas_clquery_rm_elem
 *	scnas_clquery_find_by_key
 *	scnas_clquery_find_by_keyform
 *	scnas_clquery_find_by_val
 */

/*
 * scnas_free_proplist
 *
 *	Free all memory allocated to the given proerty list.
 */
extern void scnas_free_proplist(scnas_filer_prop_t *list);

/*
 * scnas_get_indexes
 *
 *	Get the unused index numberes from the given table
 *	contents, depends on the index format specified by the
 *	table_flag.
 *
 *	Caller is responsible to free the memory allocated
 *	to the unused index list.
 *
 * Return:
 *
 *	NULL			- error
 *	non-NULL uint_t		- the index numberses
 */
extern uint_t *scnas_get_indexes(cl_query_table_t *table_contents,
    char *index_form, uint_t *index_length);

/*
 * scnas_next_index
 *
 *	Get the next usable index number from the index map.
 *
 * Return:
 *
 *	uint_t 			- the index
 */
extern uint_t scnas_next_index(uint_t start, uint_t end, uint_t *index_map);

/*
 * scnas_get_prop_val
 *
 *	From the given scnas_filer_prop_t list, return the property
 *	value for the given *key.
 */
extern char *scnas_get_prop_val(
    scnas_filer_prop_t *proplist_p, char *key, uint_t *flg);

/*
 * scnas_get_prop_key
 *
 *	From the given scnas_filer_prop_t list, return the property
 *	key for the given *value.
 */
extern char *scnas_get_prop_key(
    scnas_filer_prop_t *proplist_p, char *value, uint_t *flg);

/*
 * scnas_add_one_prop
 *
 *	Add the given "key, val" pair into the end of the property list
 *	proplist_p, if it don't exist in the list.
 *
 *	Caller is responsible to free the memory allocated for the new
 *	property.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
extern scnas_errno_t
scnas_add_one_prop(const char *key, const char *val,
    scnas_filer_prop_t **proplist_p);

/*
 * scnas_clquery_add_elem
 *
 *	Add a new element into the cl_query table contents.
 *
 *	Caller is responsible to free memory allocated for the
 *	new element.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
extern scnas_errno_t
scnas_clquery_add_elem(cl_query_table_t *table_contents,
    char *key, char *value);

/*
 * scnas_clquery_add_props
 *
 *	Add the given props_list into the cl_query table
 *	contents.
 *
 *	Caller is responsible to free memory allocated for the
 *	new elements.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
extern scnas_errno_t
scnas_clquery_add_props(cl_query_table_t *table_contents,
    scnas_filer_prop_t *prop_list, uint_t props_num);

/*
 * scnas_clquery_set_props
 *
 *	Set the key/value pairs from the given structure
 *	scnas_filer_prop_t *prop_list into the cl_query
 *	table. Change the value setting if the key exists
 *	in the cl_query table; Or add the pair if not
 *	exists.
 *
 *	Caller is repsonsible to free memory allocated for the
 *	new elements.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
extern scnas_errno_t
scnas_clquery_set_props(cl_query_table_t *table_contents,
    scnas_filer_prop_t *prop_list, uint_t props_num);

/*
 * scnas_clquery_rm_elem
 *
 *	Delete the given "key", "value" pair from the cl_query
 *	table content. The content after removing is returned
 *	in the *new_table_contents structure.
 *
 *	Caller is responsible to free the memories allocated
 *	to the *new_table_contents.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
extern scnas_errno_t
scnas_clquery_rm_elem(cl_query_table_t *table_contents,
    char *key, char *value, cl_query_table_t *new_table_contents);

/*
 * scnas_clquery_find_by_key
 *
 *	Find the value corresponding to the given key in the
 *	cl_query table structure. The index of the found value
 *	is set in *index.
 */
extern char *
scnas_clquery_find_by_key(cl_query_table_t *table_contents,
    char *key, uint_t *index);

/*
 * scnas_clquery_find_by_keyform
 *
 *	Check if the given cl_query table contains any rows
 *	whose key is of the specified *key_form;
 *
 * Return:
 *
 *	B_TRUE			- find it
 *	B_FALSE			- cannot find it
 */
extern boolean_t
scnas_clquery_find_by_keyform(cl_query_table_t *table_contents,
    char *key_form);

/*
 * scnas_clquery_find_by_val
 *
 *	Find the key corresponding to the given value in the
 *	cl_query table structure. The "length" is the number
 *	of table rows to search for. The index of the found
 *	value is set in *the_index.
 *
 */
extern char *
scnas_clquery_find_by_val(cl_query_table_t *table_contents,
    char *value, uint_t length, uint_t *index);

/*
 * Miscellaneous
 *
 *	scnas_strerr
 *	scnas_conv_scconf_err
 *	scnas_conv_clquery_err
 *	scnas_rot13_alg
 *	scnas_read_passwd_file
 */

/*
 * scnas_strerr
 *
 *	Map scnas_errno_t to a message string.
 *
 *	The supplied "errbuffer" should be of at least
 *	SCNAS_MAXSTRINGLEN in length.
 */
extern void scnas_strerr(char *errbuffer, scnas_errno_t err);

/*
 * scnas_conv_scconf_err
 *
 *	Convert scconf_errno_t to scnas_errno_t.
 */
extern scnas_errno_t scnas_conv_scconf_err(scconf_errno_t err);

/*
 * scnas_conv_clquery_err
 *
 *	Convert cl_query_error_t to scnas_errno_t.
 */
extern scnas_errno_t scnas_conv_clquery_err(cl_query_error_t err);

/*
 * scnas_rot13_alg
 *
 *	The simple Caesar-cypher encryption that replaces each
 *	English letter with the one 13 places forward or back
 *	along the alphabet. This is to accommodate the PSARC
 *	sucruity guide to save password in plain text file.
 */
extern void scnas_rot13_alg(char **string);

/*
 * read_passwd_file
 *
 *	Read the input file for the password. The password can be
 *	saved in an input file provided by "-f <passwdfile>"
 */
extern scnas_errno_t
scnas_read_passwd_file(char *input_file, char **passwd);

#ifdef __cplusplus
}
#endif

#endif /* _SCNAS_H */
