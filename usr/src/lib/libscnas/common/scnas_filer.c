/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scnas_filer.c	1.11	08/05/20 SMI"

/*
 * libscnas common functions
 */

#include "scnas.h"
#include <regex.h>
#include <dlfcn.h>
#include <dirent.h>

#define	SCNAS_DF_LIB_DIR		"/usr/cluster/lib/scnas"
#define	SCNAS_CHECK_NAS_TYPE		"scnas_chk_nas_type"
#define	SCNAS_CHECK_NAS_NAME		"scnas_chk_nas_name"
#define	SCNAS_CHECK_PROP_REQUIRED	"scnas_chk_prop_required"
#define	SCNAS_LIB_ATTACH_FILER		"scnas_lib_attach_filer"
#define	SCNAS_LIB_RM_FILER		"scnas_lib_remove_filer"
#define	SCNAS_LIB_SET_PROP		"scnas_lib_set_filer_prop"
#define	SCNASDIR_LIB_ADD_DIR		"scnasdir_lib_add_dir"
#define	SCNASDIR_LIB_RM_DIR		"scnasdir_lib_rm_dir"

#define	LABELW				30

typedef scnas_errno_t (*scnas_lib_attach_filer_t)(char *,
    scnas_filer_prop_t *, char **);
typedef scnas_errno_t (*scnas_lib_remove_filer_t)(char *,
    char *, scnas_filer_prop_t *, char **, uint_t);
typedef scnas_errno_t (*scnas_lib_set_filer_prop_t)(char *,
    char *, scnas_filer_prop_t *, char **);
typedef scnas_errno_t (*scnas_lib_get_filer_prop_t)(
    scnas_filer_prop_t *, scnas_filer_prop_t **, char **, uint_t);

typedef scnas_errno_t (*scnasdir_lib_add_dir_t)(char *,
    char *, scnas_filer_prop_t *, char **, char *, uint_t);
typedef scnas_errno_t (*scnasdir_lib_rm_dir_t)(char *,
    char *, scnas_filer_prop_t *, char **, char *, uint_t);

static void *open_nas_lib(char *arg1, char *arg2,
    char *func_name);
static char *nas_get_prop_name(char *table_prop);
static void nas_print_line(int margin, int labelw, char *prefix,
    char *label, char *value);
static scnas_errno_t
    nas_get_filer_props(char *table_name, filer_prop_t **prop);
static void
    nas_print_filer_props(filer_prop_t *prop, uint_t flg);

/*
 * scnas_attach_filer
 *
 *	Validate the given filer "type", and forward the adding
 *	filer request to the attaching filer function in the NAS
 *	library of this "type".
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_EEXIST		- filer is already attached
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNKNOWN		- unknow type
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_VP_MISMATCH	- version not match
 *	SCNAS_NO_BINARY		- no required binary
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnas_attach_filer(char *filer, char *type,
    scnas_filer_prop_t *prop_list, char **messages)
{
	void *dl_handle = NULL;
	scnas_errno_t scnaserr = SCNAS_NOERR;
	scnas_lib_attach_filer_t scnas_lib_attach_filer;

	/* Must be root */
	if (getuid() != 0) {
		return (SCNAS_EPERM);
	}

	/* Check required arguments */
	if (!filer || !type) {
		return (SCNAS_EINVAL);
	}

	/* Get the library for this device type */
	dl_handle = open_nas_lib(type, NULL, SCNAS_CHECK_NAS_TYPE);
	if (dl_handle == (void *)0) {
		return (SCNAS_EUNKNOWN);
	}

	/* Get the address of the add filer fucntion in the lib */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if ((scnas_lib_attach_filer =
	    (scnas_lib_attach_filer_t)dlsym(dl_handle,
	    SCNAS_LIB_ATTACH_FILER)) !=  NULL) {
		scnaserr = (*scnas_lib_attach_filer)(filer,
		    prop_list, messages);
	} else {
		scnaserr = SCNAS_EUNEXPECTED;
	}

	/* Close the handle */
	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}

	return (scnaserr);
}

/*
 * scnas_remove_filer
 *
 *	Remove the NAS filer. If the "flag" is set, removes the
 *	filer even its directories still exist. If it is not set,
 *	only remove the filer if the directories do not exist.
 *	This "Force" flag only exists for the new clnasdevice
 *	command.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_EDIREXIST		- still has directories
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnas_remove_filer(char *filer_name,
    scnas_filer_prop_t *proplist_p, char **messages, uint_t flag)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	void *dl_handle = NULL;
	char filer_table_name[SCNAS_MAXSTRINGLEN];
	scnas_lib_remove_filer_t scnas_lib_remove_filer;

	/* check arg */
	if (!filer_name) {
		return (SCNAS_EINVAL);
	}

	if (proplist_p) {
		scnaserr = SCNAS_NOERR;
	}

	/* Must be root */
	if (getuid() != 0) {
		return (SCNAS_EPERM);
	}

	/* Get the library for this filer */
	*filer_table_name = '\0';
	dl_handle = open_nas_lib(filer_name, filer_table_name,
	    SCNAS_CHECK_NAS_NAME);
	if ((dl_handle == (void *)0) || (filer_table_name == NULL) ||
	    (*filer_table_name == '\0')) {
		return (SCNAS_ENOEXIST);
	}

	/* Get the address of the function in the library */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if ((scnas_lib_remove_filer =
	    (scnas_lib_remove_filer_t)dlsym(
	    dl_handle, SCNAS_LIB_RM_FILER)) !=  NULL) {
		scnaserr = (*scnas_lib_remove_filer)(
		    filer_name, filer_table_name, NULL,
		    messages, flag);
	} else {
		scnaserr = SCNAS_EUNEXPECTED;
	}

	/* Close the handle */
	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}

	return (scnaserr);
}

/*
 * scnas_remove_filer_table
 *
 *	Detach a filer table from CCR.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_EDIREXIST		- still has directories
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnas_remove_filer_table(char *filername, char *filer_table_name,
    char *nas_type, scnas_filer_prop_t *props, char **messages, uint_t flag)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	cl_query_error_t cl_err = CL_QUERY_OK;
	cl_query_table_t filer_table;
	cl_query_table_t index_table;
	cl_query_table_t new_index_table;
	char filer_table_index[SCNAS_MAXSTRINGLEN];
	boolean_t succeed = B_FALSE;
	char *ccr_filer_name;
	char errbuf[SCNAS_MAXSTRINGLEN];

	/* Must be root */
	if (getuid() != 0) {
		return (SCNAS_EPERM);
	}

	/* Check args */
	if (!filername || !filer_table_name || !nas_type) {
		return (SCNAS_EINVAL);
	}

	if (props) {
		scnaserr = SCNAS_NOERR;
	}

	/* Initialzie table contents */
	index_table.n_rows = 0;
	filer_table.n_rows = 0;
	new_index_table.n_rows = 0;

	/* Read the filer table */
	scnaserr = scnas_read_filer_table(filer_table_name,
	    NULL, NULL, &filer_table, GET_TABLE_CONTENTS);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Check the filer name read from the table */
	ccr_filer_name = scnas_clquery_find_by_key(&filer_table,
	    PROP_FILER_NAME, NULL);
	if (strcmp(filername, ccr_filer_name) != 0) {
		scnaserr = SCNAS_EUNEXPECTED;
		goto cleanup;
	}

	/* See if still some directories configured for this file */
	if (!flag && scnas_clquery_find_by_keyform(&filer_table,
	    PROP_DIRECTORY)) {
		scnaserr = SCNAS_EDIREXIST;
		goto cleanup;
	}

	/* Get the index number for this filer table */
	if (sscanf(filer_table_name, NAS_SERVICE_TABLE_PRE"_%s",
	    filer_table_index) != 1) {
		scnaserr = SCNAS_EUNEXPECTED;
		goto cleanup;
	}

	/* Read the index table */
	scnaserr = scnas_read_filer_table(NAS_SERVICE_KEY_TABLE,
	    NULL, NULL, &index_table, GET_TABLE_CONTENTS);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Remove from index table contents */
	scnaserr = scnas_clquery_rm_elem(&index_table,
	    filer_table_index, nas_type,
	    &new_index_table);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/*
	 * Write the new contents to the index table. Note we should
	 * write the index table first, and then remove the filer
	 * table.
	 */
	cl_err = cl_query_write_table_all(NAS_SERVICE_KEY_TABLE,
	    &new_index_table);
	if (cl_err != CL_QUERY_OK) {
		scnaserr = scnas_conv_clquery_err(cl_err);

		/* Add error messages */
		if (messages != (char **)0) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot update the index table for NAS "
			    "device \"%s\".\n"), filername);
			scconf_addmessage(errbuf, messages);
		}
		goto cleanup;
	}

	/* Remove the filer table */
	cl_err = cl_query_delete_table(filer_table_name);
	if (cl_err != CL_QUERY_OK) {
		scnaserr = scnas_conv_clquery_err(cl_err);

		/* Add error messages */
		if (messages != (char **)0) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot remove the service table for NAS "
			    "device \"%s\".\n"), filername);
			scconf_addmessage(errbuf, messages);
		}
		goto cleanup;
	}

	succeed = B_TRUE;
	goto cleanup2;

cleanup:
	/* Free memory */
	cl_err = cl_query_free_table(&new_index_table);
	if (cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

cleanup2:
	/* Free memory */
	cl_err = cl_query_free_table(&index_table);

	/*
	 * Don't care the error of freeing memory if above
	 * table operation is not successful.
	 */
	if (!succeed && cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

	cl_err = cl_query_free_table(&filer_table);
	if (!succeed && cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

	return (scnaserr);
}

/*
 * scnas_remove_filer_dir
 *
 *	Remove directories from a NAS filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_EDIREXIST		- still has directories
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnas_remove_filer_dir(char *filername, char *filer_table_name,
    scnas_filer_prop_t *prop_list, char **messages,
    char *dir_all, uint_t uflg)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	cl_query_error_t cl_err = CL_QUERY_OK;
	cl_query_table_t filer_table;
	char *ccr_filer_name;
	cl_query_table_t new_table;
	uint_t rm_num;
	register scnas_filer_prop_t *prop;
	boolean_t rm_all = B_FALSE;
	int dir_index;
	uint_t i;
	int j;
	char errbuf[SCNAS_MAXSTRINGLEN];
	int invalid_dirs;
	int dir_num;

	/* Check arguments */
	if (!filername || !filer_table_name) {
		return (SCNAS_EINVAL);
	}

	if (!prop_list && !dir_all) {
		return (SCNAS_EINVAL);
	}

	/* Initialzie */
	filer_table.n_rows = 0;
	new_table.n_rows = 0;
	rm_num = 0;
	j = 0;

	/* Read the table */
	scnaserr = scnas_read_filer_table(filer_table_name,
	    NULL, NULL, &filer_table, GET_TABLE_CONTENTS);
	if (scnaserr != SCNAS_NOERR) {
		if (messages != (char **)0) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot read the service table.\n"));
			scconf_addmessage(errbuf, messages);
		}
		goto cleanup;
	}

	/* Check the filer name */
	ccr_filer_name = scnas_clquery_find_by_key(&filer_table,
	    PROP_FILER_NAME, NULL);
	if (strcmp(filername, ccr_filer_name) != 0) {
		scnaserr = SCNAS_EUNEXPECTED;
		goto cleanup;
	}

	/* Get the directories number */
	invalid_dirs = 0;
	dir_num = 0;
	if (!dir_all) {
		for (prop = prop_list; prop;
		    prop = prop->scnas_prop_next) {

			/* Skip */
			if (!(prop->scnas_prop_key) ||
			    !(prop->scnas_prop_value))
				continue;

			/* Count the directories */
			dir_num++;

			/* Valid? */
			if (*(prop->scnas_prop_value) != '/' ||
			    strlen(prop->scnas_prop_value) == 1) {
				if (messages != (char **)0) {
					(void) sprintf(errbuf,
					    dgettext(TEXT_DOMAIN,
					    "Invalid directory \"%s\" on "
					    "\"%s\".\n"),
					    prop->scnas_prop_value, filername);
					scconf_addmessage(errbuf, messages);
				}

				invalid_dirs++;
				continue;
			}

			/* Doesn't exist? */
			if (scnas_clquery_find_by_val(&filer_table,
			    prop->scnas_prop_value,
			    filer_table.n_rows, NULL) == NULL) {
				if (messages != (char **)0) {
					(void) sprintf(errbuf,
					    dgettext(TEXT_DOMAIN,
					    "Directory \"%s\" does not "
					    "exist on \"%s\".\n"),
					    prop->scnas_prop_value, filername);
					scconf_addmessage(errbuf, messages);
				}

				invalid_dirs++;
			}
		}

		if (invalid_dirs) {
			scnaserr =  SCNAS_EINVALPROP;
			if (invalid_dirs == dir_num) {
				goto cleanup;
			}
		}
	} else if (strcmp(dir_all, NASDIR_ALL_DIR) == 0) {
		rm_all = B_TRUE;
	}

	/* Only validate the properties */
	if (uflg)
		goto cleanup;

	/* Allocate memory for the new table */
	new_table.n_rows = filer_table.n_rows;
	new_table.table_rows = (cl_query_table_elem_t **)
	    calloc(new_table.n_rows, sizeof (cl_query_table_elem_t));
	if (new_table.table_rows == (cl_query_table_elem_t **)0) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	/* Go through the table */
	for (i = 0; i < filer_table.n_rows; i++) {

		/* Check the element key format */
		if (sscanf(filer_table.table_rows[i]->key,
		    PROP_DIRECTORY"_%d", &dir_index) != 1) {
			new_table.table_rows[j++] =
			    filer_table.table_rows[i];

			continue;
		}

		if (rm_all) {
			rm_num++;
			continue;
		}

		if (scnas_get_prop_key(prop_list,
		    filer_table.table_rows[i]->value, NULL)
		    != NULL) {
			rm_num++;
		} else {
			new_table.table_rows[j++] =
			    filer_table.table_rows[i];
		}
	}

	/* Check the size */
	if (rm_num) {
		new_table.n_rows -= rm_num;

		new_table.table_rows = (cl_query_table_elem_t **)
		    realloc(new_table.table_rows,
		    sizeof (cl_query_table_elem_t) * new_table.n_rows);
		if (new_table.table_rows ==
		    (cl_query_table_elem_t **)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	}

	/* Write the table */
	cl_err = cl_query_write_table_all(filer_table_name,
	    &new_table);

	if (cl_err != CL_QUERY_OK) {
		scnaserr = scnas_conv_clquery_err(cl_err);

		/* Add error messages */
		if ((scnaserr != SCNAS_NOERR) &&
		    (messages != (char **)0)) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot remove directories from NAS device "
			    "\"%s\"\n"), filername);
			scconf_addmessage(errbuf, messages);
		}
	}

cleanup:
	/* Free memory */
	cl_err = cl_query_free_table(&filer_table);
	if (cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

	cl_err = cl_query_free_table(&new_table);
	if (cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

	return (scnaserr);
}

/*
 * scnas_change_filer
 *
 *	Update the properties of a filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnas_change_filer(char *filer,
    scnas_filer_prop_t *proplist_p, char **messages)
{
	void *dl_handle = NULL;
	scnas_errno_t scnaserr = SCNAS_NOERR;
	scnas_lib_set_filer_prop_t scnas_lib_set_filer_prop;
	char filer_table_name[SCNAS_MAXSTRINGLEN];

	/* Must be root */
	if (getuid() != 0) {
		return (SCNAS_EPERM);
	}

	/* check arg */
	if (!filer || !proplist_p) {
		return (SCNAS_EINVAL);
	}

	/* Get the library for this device type */
	dl_handle = open_nas_lib(filer, filer_table_name,
	    SCNAS_CHECK_NAS_NAME);
	if (dl_handle == (void *)0) {
		return (SCNAS_ENOEXIST);
	}

	/* Get the address of the add filer fucntion in the lib */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if ((scnas_lib_set_filer_prop =
	    (scnas_lib_set_filer_prop_t)dlsym(dl_handle,
	    SCNAS_LIB_SET_PROP)) !=  NULL) {
		scnaserr = (* scnas_lib_set_filer_prop)(filer,
		    filer_table_name, proplist_p, messages);
	} else {
		scnaserr = SCNAS_EUNEXPECTED;
	}

	/* Close the handle */
	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}

	return (scnaserr);
}

/*
 * scnas_check_nas_devicetype
 *
 *	This functions dlopen's the NAS library which corresponds to
 *	the given type. If it can find such a NAS library, the given
 *	device type is valid; Otherwise it is not a valid type.
 *
 * Possible return values:
 *
 *	B_TRUE		The type is valid
 *	B_FALSE		Unknown type
 */
boolean_t
scnas_check_nas_devicetype(const char *type, boolean_t *props_required)
{
	void *dl_handle = NULL;
	boolean_t check_type = B_FALSE;

	/* Check arguments */
	if (type == NULL) {
		return (check_type);
	}

	/* Get the library for this device type */
	dl_handle = open_nas_lib((char *)type, (char *)props_required,
	    SCNAS_CHECK_NAS_TYPE);
	if (dl_handle != (void *)0) {
		/* Valid type */
		check_type = B_TRUE;
		(void) dlclose(dl_handle);
	}

	return (check_type);
}

/*
 * scnas_check_nas_proprequired
 *
 *	Check if the given NAS filer name is valid, and returns
 *	true if it is, otherwise returns false. For valid NAS device,
 *	it also sets *props_required to true if the type of NAS requires
 *	certain properties, and set *props_required to false if not.
 *
 * Possible return values:
 *
 *	B_TRUE		The type is valid
 *	B_FALSE		Unknown type
 */
boolean_t
scnas_check_nas_proprequired(const char *nas_name, boolean_t *props_required)
{
	void *dl_handle = NULL;
	boolean_t check_nas = B_FALSE;

	/* Check arguments */
	if (nas_name == NULL) {
		return (check_nas);
	}

	/* Get the library for this device type */
	dl_handle = open_nas_lib((char *)nas_name, (char *)props_required,
	    SCNAS_CHECK_PROP_REQUIRED);
	if (dl_handle != (void *)0) {
		/* Valid nas device */
		check_nas = B_TRUE;
		(void) dlclose(dl_handle);
	}

	return (check_nas);
}

/*
 * scnas_read_filer_table
 *
 *	Read the given filer CCR table into the cl_query_table_t
 *	structure, by the flag. For flag GET_TABLE_BY_KEY,
 *	only fill in the cl_query_table_t structure if the filer
 *	has a property matches the given key/value pair; For
 *	flag GET_TABLE_CONTENTS, just read the table and ignore
 *	the key/value.
 *
 *	Caller is responsible to free the memory allocated for
 *	cl_query_table_t.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR             - success
 *	SCNAS_ENOCLUSTER	- cluster does not exist
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 *	SCNAS_EUNEXPECTED	- unexpected error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOEXIST		- table does not exist
 */
scnas_errno_t
scnas_read_filer_table(char *table_name, char *key, char *value,
    cl_query_table_t *table_contents, uint_t flag)
{
	cl_query_error_t cl_err = CL_QUERY_OK;
	scnas_errno_t rstatus = SCNAS_NOERR;
	uint_t i;
	boolean_t found = B_FALSE;

	/* Check the args */
	if (!table_name) {
		return (SCNAS_EINVAL);
	}

	if ((flag == GET_TABLE_BY_KEY) && (!key || !value)) {
		return (SCNAS_EINVAL);
	}

	/* Read the table */
	cl_err =  cl_query_read_table(table_name, table_contents);
	if (cl_err != CL_QUERY_OK) {
		rstatus = scnas_conv_clquery_err(cl_err);
		goto cleanup;
	}

	/* What flag is it using? */
	if (flag == GET_TABLE_CONTENTS) {
		return (rstatus);
	} else if (flag == GET_TABLE_BY_KEY) {

		/* Check the key/value pair */
		for (i = 0; i < table_contents->n_rows; i++) {
			if (!(table_contents->table_rows[i]->key) ||
			    !(table_contents->table_rows[i]->value))
				continue;

			if (strcmp(table_contents->table_rows[i]->key,
			    key) != 0)
				continue;

			if (strcmp(table_contents->table_rows[i]->value,
			    value) == 0) {
				found = B_TRUE;
				break;
			}
		}

		if (!found) {
			goto cleanup;
		} else {
			return (rstatus);
		}
	} else {
		rstatus = SCNAS_EUNEXPECTED;
		goto cleanup;
	}

cleanup:

	cl_err = cl_query_free_table(table_contents);
	if (cl_err != CL_QUERY_OK) {
		rstatus = SCNAS_EUNEXPECTED;
	}
	table_contents->n_rows = 0;

	return (rstatus);
}

/*
 * scnas_display_filer_props
 *
 *	Display the filer properties.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EUNEXPECTED	- unexpected error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNKNOWN		- unknown type
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_display_filer_props(FILE *out, char *filer_name, char *filer_type,
    scnas_filer_prop_t *prop_list, char **messages, uint_t cmd_flg)
{
	register scnas_filer_prop_t *prop;
	scnas_errno_t scnaserr = SCNAS_NOERR;
	char *tmp_key;
	int margin = 0;
	void *dl_handle = NULL;
	scnasdir_lib_add_dir_t scnasdir_lib_add_dir;
	scnasdir_lib_rm_dir_t scnasdir_lib_rm_dir;
	char filer_table_name[SCNAS_MAXSTRINGLEN];

	/* Check args */
	if (!out || !filer_name) {
		return (SCNAS_EUNEXPECTED);
	}

	/* No properties, no print */
	if (!prop_list) {
		return (scnaserr);
	}

	/* Get the library for this filer */
	*filer_table_name = '\0';
	dl_handle = open_nas_lib(filer_name, filer_table_name,
	    SCNAS_CHECK_NAS_NAME);
	if (dl_handle == (void *)0) {
		return (SCNAS_ENOEXIST);
	}

	/* Get the address of the function in the library */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if (cmd_flg == SCNASDIR_CMD_ADD) {

		scnasdir_lib_add_dir = (scnasdir_lib_add_dir_t)dlsym(
		    dl_handle, SCNASDIR_LIB_ADD_DIR);
		if (scnasdir_lib_add_dir != NULL) {
			/* call the function */
			scnaserr = (*scnasdir_lib_add_dir)
			    (filer_name, filer_table_name,
			    prop_list, messages, NULL, 1);
		} else {
			scnaserr = SCNAS_EUNEXPECTED;
		}
	} else if (cmd_flg == SCNASDIR_CMD_RM) {

		scnasdir_lib_rm_dir = (scnasdir_lib_rm_dir_t)dlsym(
		    dl_handle, SCNASDIR_LIB_RM_DIR);
		if (scnasdir_lib_rm_dir != NULL) {
			/* call the function */
			scnaserr = (* scnasdir_lib_rm_dir)(filer_name,
			    filer_table_name, prop_list, messages,
			    NULL, 1);
		} else {
			scnaserr = SCNAS_EUNEXPECTED;
		}
	} else {
		scnaserr = SCNAS_EUNEXPECTED;
	}

	if (scnaserr != SCNAS_NOERR)
		goto cleanup;

	/* print the filer data */
	if (cmd_flg == SCNASDIR_CMD_ADD) {
		nas_print_line(margin, LABELW, NULL,
		    gettext("Filer directories to add:"), NULL);
	} else if (cmd_flg == SCNASDIR_CMD_RM) {
		nas_print_line(margin, LABELW, NULL,
		    gettext("Filer directories to remove:"), NULL);
	}

	/* Print the name and type */
	nas_print_line(margin + 8, LABELW, NULL, "filer name", filer_name);

	if (cmd_flg == SCNAS_CMD_ADD) {
		tmp_key = (filer_type) ? filer_type : "<NULL>";
		nas_print_line(margin + 8, LABELW, NULL,
		    NAS_FILER_TYPE, tmp_key);
	}

	/* Print the properties */
	for (prop = prop_list; prop; prop = prop->scnas_prop_next) {
		/* Skip */
		if (!(prop->scnas_prop_key))
			continue;

		if (!(prop->scnas_prop_value))
			continue;

		/* Print the property */
		if (strcmp(prop->scnas_prop_key, NAS_PASSWD) == 0) {
			nas_print_line(margin + 8, LABELW, NULL,
			    prop->scnas_prop_key, "*******");
		} else {
			nas_print_line(margin + 8, LABELW, NULL,
			    prop->scnas_prop_key,
			    prop->scnas_prop_value);
		}
	}

	(void) fprintf(out, "\n");

cleanup:
	/* Close the handle */
	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}

	return (scnaserr);
}

/*
 * scnas_get_filer_tablename
 *
 *	For the given filer name, return its table name.
 *	Caller is responsible to free the memory allocated for
 *	the returned filer table name.
 */
char *
scnas_get_filer_tablename(char *filer_name)
{
	void *dl_handle = NULL;
	char filer_table_name[SCNAS_MAXSTRINGLEN];
	char *table_name = NULL;

	/* Check args */
	if (!filer_name) {
		return (NULL);
	}

	/* Get the library for this filer */
	*filer_table_name = '\0';
	dl_handle = open_nas_lib(filer_name, filer_table_name,
	    SCNAS_CHECK_NAS_NAME);
	if ((dl_handle == (void *)0) || (filer_table_name == NULL) ||
	    (*filer_table_name == '\0')) {
		return (NULL);
	}

	/* Close the handle */
	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}

	if ((table_name = strdup(filer_table_name)) == NULL) {
		return (NULL);
	} else {
		return (table_name);
	}
}

/*
 * scnasdir_set_filer_prop
 *
 *	Make changes to the directories configuration of a filer.
 *	The change is either adding or removing depends on the
 *	flag setting.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnasdir_set_filer_prop(char *filer_name,
    scnas_filer_prop_t *prop_list, char **messages,
    char *dir_all, uint_t flag)
{
	void *dl_handle = NULL;
	scnas_errno_t scnaserr = SCNAS_NOERR;
	scnasdir_lib_add_dir_t scnasdir_lib_add_dir;
	scnasdir_lib_rm_dir_t scnasdir_lib_rm_dir;
	char filer_table_name[SCNAS_MAXSTRINGLEN];

	/* Must be root */
	if (getuid() != 0) {
		return (SCNAS_EPERM);
	}

	/* Check required arguments */
	if (!filer_name) {
		return (SCNAS_EINVAL);
	}

	/* Get the library for this filer */
	*filer_table_name = '\0';
	dl_handle = open_nas_lib(filer_name, filer_table_name,
	    SCNAS_CHECK_NAS_NAME);
	if (dl_handle == (void *)0) {
		return (SCNAS_ENOEXIST);
	}

	/* Get the address of the function in the library */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if (flag == SCNASDIR_CMD_ADD) {

		scnasdir_lib_add_dir = (scnasdir_lib_add_dir_t)dlsym(
		    dl_handle, SCNASDIR_LIB_ADD_DIR);
		if (scnasdir_lib_add_dir != NULL) {
			/* call the function */
			scnaserr = (* scnasdir_lib_add_dir)(filer_name,
			    filer_table_name, prop_list, messages,
			    NULL, 0);
		} else {
			scnaserr = SCNAS_EUNEXPECTED;
		}
	} else if (flag == SCNASDIR_CMD_RM) {

		scnasdir_lib_rm_dir = (scnasdir_lib_rm_dir_t)dlsym(
		    dl_handle, SCNASDIR_LIB_RM_DIR);
		if (scnasdir_lib_rm_dir != NULL) {
			/* call the function */
			scnaserr = (* scnasdir_lib_rm_dir)(filer_name,
			    filer_table_name, prop_list, messages,
			    dir_all, 0);
		} else {
			scnaserr = SCNAS_EUNEXPECTED;
		}
	} else {
		scnaserr = SCNAS_EUNEXPECTED;
	}

	/* Close the handle */
	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}

	return (scnaserr);
}

/*
 * scnas_get_one_filer
 *
 *	Get the properties of a given filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_get_one_filer(char *filer_name, filer_prop_t **prop)
{
	char *filer_table_name = NULL;
	scnas_errno_t scnaserr = SCNAS_NOERR;

	if (prop == NULL) {
		return (SCNAS_EINVAL);
	}

	*prop = (filer_prop_t *)calloc(1, sizeof (filer_prop_t));
	if (*prop == NULL) {
		return (SCNAS_ENOMEM);
	}

	/* Get the filer name */
	if ((filer_table_name = scnas_get_filer_tablename(filer_name))
	    == NULL) {
		return (SCNAS_ENOEXIST);
	}

	/* Get the filer table */
	scnaserr = nas_get_filer_props(filer_table_name, prop);
	if (scnaserr != SCNAS_NOERR) {
		if (filer_table_name) {
			free(filer_table_name);
		}
		return (scnaserr);
	}

	if (filer_table_name) {
		free(filer_table_name);
	}

	return (scnaserr);
}

/*
 * scnas_get_filer_by_type
 *
 *	Print properties of filers of one type.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNKNOWN		- unknown type
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_get_filer_by_type(char *filer_type, filer_prop_t ***prop,
			uint_t *num_filer)
{
	cl_query_error_t cl_err = CL_QUERY_OK;
	scnas_errno_t scnaserr = SCNAS_NOERR;
	cl_query_table_t index_table;
	char filer_table_name[SCNAS_MAXSTRINGLEN];
	uint_t i, j = 0;
	uint_t count = 0;

	/* Check the args */
	if (!filer_type) {
		return (SCNAS_EINVAL);
	}

	if (prop == NULL) {
		return (SCNAS_EINVAL);
	}

	/* Get the index table */
	index_table.n_rows = 0;
	scnaserr = scnas_read_filer_table(NAS_SERVICE_KEY_TABLE,
	    NULL, NULL, &index_table, GET_TABLE_CONTENTS);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	if (scnas_clquery_find_by_val(&index_table,
	    filer_type, index_table.n_rows, NULL) == NULL) {
		scnaserr = SCNAS_EUNKNOWN;
		goto cleanup;
	}

	/* Get the number of filers matching the given type */
	for (i = 0; i < index_table.n_rows; i++) {
		if (!(index_table.table_rows[i]->key) ||
		    !(index_table.table_rows[i]->value))
			continue;

		if (strcmp(index_table.table_rows[i]->value, filer_type)
		    == 0)
			count++;
	}

	*num_filer = count;

	/* Allocate memory */
	*prop = (filer_prop_t **)calloc(count, sizeof (filer_prop_t *));
	if (*prop == NULL) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	for (i = 0; i < count; i++) {

		(*prop)[i] = (filer_prop_t *)
				calloc(1, sizeof (filer_prop_t));

		if ((*prop)[i] == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	}

	/* Go through the index for the matched filers */
	for (i = 0; i < index_table.n_rows; i++) {

		if (!(index_table.table_rows[i]->key) ||
		    !(index_table.table_rows[i]->value))
			continue;

		if (strcmp(index_table.table_rows[i]->value, filer_type)
		    != 0)
			continue;

		/* Get the table name */
		(void) sprintf(filer_table_name, "%s_%s",
		    NAS_SERVICE_TABLE_PRE,
		    index_table.table_rows[i]->key);

		/* Get the properties of the table */
		scnaserr = nas_get_filer_props(filer_table_name, &((*prop)[j]));

		if (scnaserr != SCNAS_NOERR) {
			/* If not exist, continue to the next one */
			if (scnaserr == SCNAS_ENOEXIST) {
				continue;
			}
			goto cleanup;
		}

		j++;
	}

cleanup:
	/* Free memory */
	cl_err = cl_query_free_table(&index_table);
	if (cl_err != CL_QUERY_OK && scnaserr == SCNAS_NOERR) {
		scnaserr = scnas_conv_clquery_err(cl_err);
	}

	return (scnaserr);
}

/*
 * scnas_get_filer_all
 *
 *	Get all filers' properties.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_get_filer_all(filer_prop_t ***prop, uint_t *num_filer)
{
	cl_query_error_t cl_err = CL_QUERY_OK;
	scnas_errno_t scnaserr = SCNAS_NOERR;
	cl_query_table_t index_table;
	char filer_table_name[SCNAS_MAXSTRINGLEN];
	uint_t i;
	uint_t count = 0, k = 0;

	if (prop == NULL) {
		return (SCNAS_EINVAL);
	}

	/* Get the index table */
	index_table.n_rows = 0;
	scnaserr = scnas_read_filer_table(NAS_SERVICE_KEY_TABLE,
	    NULL, NULL, &index_table, GET_TABLE_CONTENTS);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Get the number of filers */
	for (i = 0; i < index_table.n_rows; i++) {
		if (!(index_table.table_rows[i]->key) ||
		    !(index_table.table_rows[i]->value)) {
			continue;
		} else {
			count++;
		}
	}

	*num_filer = count;

	/* Allocate memory */
	*prop = (filer_prop_t **)calloc(count, sizeof (filer_prop_t *));
	if (*prop == NULL) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	for (i = 0; i < count; i++) {

		(*prop)[i] = (filer_prop_t *)
				calloc(1, sizeof (filer_prop_t));

		if ((*prop)[i] == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	}

	/* Go through the index for all filers */
	for (i = 0; i < index_table.n_rows; i++) {

		/* Skip */
		if (!(index_table.table_rows[i]->key) ||
		    !(index_table.table_rows[i]->value)) {
			continue;
		}

		/* Get the table name */
		(void) sprintf(filer_table_name, "%s_%s",
		    NAS_SERVICE_TABLE_PRE,
		    index_table.table_rows[i]->key);

		/* Get the properties of the table */
		scnaserr = nas_get_filer_props(
			filer_table_name, &((*prop)[k]));

		if (scnaserr != SCNAS_NOERR) {
			/* If not exist, continue to the next one */
			if (scnaserr == SCNAS_ENOEXIST) {
				continue;
			} else {
				goto cleanup;
			}
		}

		k++;
	}

cleanup:
	/* Free memory */
	cl_err = cl_query_free_table(&index_table);
	if (cl_err != CL_QUERY_OK && scnaserr == SCNAS_NOERR) {
		scnaserr = scnas_conv_clquery_err(cl_err);
	}

	return (scnaserr);
}

/*
 * scnas_print_one_filer
 *
 *	Print the properties of a given filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_print_one_filer(char *filer_name, uint_t flg)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	filer_prop_t *prop = NULL;

	/* Get filer properties */
	scnaserr = scnas_get_one_filer(filer_name, &prop);
	if (scnaserr != SCNAS_NOERR) {
		return (scnaserr);
	}

	/* Print filer properties */
	nas_print_filer_props(prop, flg);

	return (scnaserr);
}

/*
 * scnas_print_filer_by_type
 *
 *	Print properties of filers of one type.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNKNOWN		- unknown type
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_print_filer_by_type(char *filer_type, uint_t flg)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	filer_prop_t **prop = NULL;
	uint_t count = 0, i = 0;

	/* Get the filer properties */
	scnaserr = scnas_get_filer_by_type(filer_type, &prop, &count);
	if (scnaserr != SCNAS_NOERR) {
		return (scnaserr);
	}

	(void) printf(gettext("Filers of type \"%s\":\n\n"), filer_type);

	/* Itertate through the list of filers and print properties */
	for (i = 0; i < count; i++) {
		nas_print_filer_props(prop[i], flg);
		(void) putchar('\n');
	}

	return (scnaserr);
}

/*
 * scnas_print_filer_all
 *
 *	Print all filers' properties.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_print_filer_all(uint_t flg)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	filer_prop_t **prop = NULL;
	uint_t i = 0;
	uint_t count = 0, j = 0, k = 0;
	filer_prop_t *temp = NULL;

	/* Get the filer properties */
	scnaserr = scnas_get_filer_all(&prop, &count);

	/* Return if there is an error or if there are no filers present */
	if ((scnaserr != SCNAS_NOERR) || (count == 0)) {
		return (scnaserr);
	}

	/* Sort the list of filers on type */
	for (i = 0; i < count; i++) {
		for (j = i + 1; j < count; j++) {
			if (strcmp(prop[i] -> filer_type,
				prop[j] -> filer_type) != 0) {

				for (k = j + 1; k < count; k++) {
					if (strcmp(prop[i] -> filer_type,
						prop[k] -> filer_type) == 0) {
						temp = prop[k];
						prop[k] = prop[j];
						prop[j] = temp;

						break;
					}
				}
			}
		}
	}

	(void) putchar('\n');
	(void) printf(gettext("Filers of type \"%s\":\n\n"),
					prop[0] -> filer_type);
	nas_print_filer_props(prop[0], flg);
	(void) putchar('\n');
	j = 1;

	/* Iterate through the list of filers and print properties */
	while (j < count) {
		if (strcmp(prop[j] -> filer_type,
			prop[j - 1] -> filer_type) == 0) {

			nas_print_filer_props(prop[j], flg);
			(void) putchar('\n');
		} else {
			(void) putchar('\n');
			(void) printf(gettext("Filers of type "
				"\"%s\":\n\n"), prop[j] -> filer_type);
			nas_print_filer_props(prop[j], flg);
			(void) putchar('\n');
		}

		j++;
	}

	return (scnaserr);
}

/*
 * open_nas_lib
 *
 *	It opens the SCNAS_DF_LIB_DIR directory, and returns a
 *	handle of the right library that contains the given
 *	func_name with B_TRUE return code, based on the given
 *	parameters.
 *
 * NOTE: The first two parameters are parameters for the func_name.
 *	Depends on what function the func_name is, these two
 *	parameters may have different meanings. Here the names
 *	of these two parameters don't mean too much.
 *
 * Possible return values:
 *	equal to zero   - error.
 *	non zero        - success. A library handle is returned.
 */
static void *open_nas_lib(char *arg1, char *arg2,
    char *func_name)
{
	DIR *dirp = NULL;
	char *pattern = "^scnas_.*.so.1$";
	regex_t re;
	struct dirent *dp = NULL;
	char dirbuf[BUFSIZ];
	char path_name[SCNAS_MAXSTRINGLEN];
	void *dl_handle = NULL;
	boolean_t (*scnas_check_nas_func)(char *, char *);
	boolean_t match_found = B_FALSE;

	/* Open the qd shared library directory */
	if ((dirp = opendir(SCNAS_DF_LIB_DIR)) == NULL)
		goto cleanup;

	/* Compile the pattern */
	if (regcomp(&re, pattern, REG_EXTENDED) != 0)
		goto cleanup;

	/* Go through the libraries */
	while ((dp = readdir(dirp)) != NULL) {

		/* Check if the library opened is in the format required */
		if (regexec(&re, dp->d_name, (size_t)0, NULL, 0) != 0)
			continue;
		if (sscanf(dp->d_name, "scnas_%s", dirbuf) != 1)
			continue;

		(void) sprintf(path_name, "%s/%s", SCNAS_DF_LIB_DIR,
		    dp->d_name);

		/*
		 * Open the shared library with RTLD_NODELETE so it
		 * won't be loaded repeatedly in subsequent calls to
		 * this function.
		 */
		if ((dl_handle = dlopen(path_name,
		    RTLD_NOW|RTLD_NODELETE)) == (void *)0)
			goto cleanup;

		/* Find the address of the function */
		if ((scnas_check_nas_func = (boolean_t(*)(char *, char *))
		    dlsym(dl_handle, func_name)) != NULL) {

			/* Call the function */
			match_found = (*scnas_check_nas_func)(arg1, arg2);
			if (match_found) {
				goto cleanup;
			} else {
				if (dl_handle != (void *)0) {
					(void) dlclose(dl_handle);
					dl_handle = NULL;
				}
				continue;
			}
		} else {
			if (dl_handle != (void *)0) {
				(void) dlclose(dl_handle);
				dl_handle = NULL;
				goto cleanup;
			}
		}
	}

cleanup:
	regfree(&re);
	if (dirp != NULL) {
		(void) closedir(dirp);
	}

	return (dl_handle);
}

/*
 * nas_get_prop_name
 *
 *	Get the filer property name from its key name in the
 *	ccr table.
 */
static char *
nas_get_prop_name(char *table_prop)
{
	if (!table_prop) {
		return (NULL);
	} else if (strcmp(table_prop, PROP_FILER_NAME) == 0) {
		return (NAS_FILER_NAME);
	} else if (strcmp(table_prop, PROP_FILER_TYPE) == 0) {
		return (NAS_FILER_TYPE);
	} else if (strcmp(table_prop, PROP_USERID) == 0) {
		return (NAS_USERID);
	} else if (strcmp(table_prop, PROP_PASSWD) == 0) {
		return (NAS_PASSWD);
	} else if (strstr(table_prop, PROP_DIRECTORY)) {
		return (NAS_DIRECTORIES);
	} else {
		return (NULL);
	}
}

/*
 * nas_print_line
 *
 *	Print a line of configuration output.
 *
 *	If no prefix or prefix is NULL string, do not print prefix.
 *	If no value, do not print value or newline.
 */
static void
nas_print_line(int margin, int labelw, char *prefix, char *label, char *value)
{
	int i;
	char labelbuff[BUFSIZ];

	/* check args */
	if (margin < 0 || labelw < margin || label == NULL || *label == '\0')
		return;

	/* Check for NULL strings */
	if (prefix && *prefix == '\0')
		prefix = NULL;
	if (value && *value == '\0')
		value = NULL;

	/* set up labelbuff, with or without prefix */
	if (prefix) {
		(void) sprintf(labelbuff, "%s %s", prefix, label);
	} else {
		(void) strcpy(labelbuff, label);
	}

	/* print margin */
	for (i = 0;  i < margin;  ++i)
		(void) putchar(' ');

	/* print label */
	(void) printf("%-*s ", labelw - margin, labelbuff);

	/* print value */
	if (value)
		(void) printf("%s\n", value);
	else
		(void) putchar('\n');
}

/*
 * nas_print_filer_props
 *
 *	print the filer properties.
 *
 * Possible return values:	void
 *
 */
void
nas_print_filer_props(filer_prop_t *prop, uint_t flg)
{
	int margin = 0;
	uint_t i = 0;

	/* Print the filer name */
	nas_print_line(margin + 4, LABELW, NULL, gettext("Filer name:"),
	    prop -> filer_name);

	/* Print all properties other than directories */
	if (flg != SCNASDIR_CMD) {
		nas_print_line(margin + 8, LABELW, NULL, gettext("type:"),
		    prop -> filer_type);
		if (prop -> filer_uid) {
			nas_print_line(margin + 8, LABELW, NULL,
			    gettext("userid:"), prop -> filer_uid);
		}
		if (prop -> filer_passwd) {
			nas_print_line(margin + 8, LABELW, NULL,
			    gettext("password:"), prop -> filer_passwd);
		}
	}

	/* Print the directories */
	if (prop -> filer_dirs_count > 0) {
		for (i = 0; i < prop -> filer_dirs_count; i++) {
			nas_print_line(margin + 8, LABELW, NULL,
			    gettext("directories:"),
			    (prop -> filer_dirs)[i]);
		}
	}
}

/*
 * nas_get_filer_props
 *
 *	Retrieve the filer properties.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_ENOEXIST		- object not exists
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
static scnas_errno_t
nas_get_filer_props(char *table_name, filer_prop_t **prop)
{
	cl_query_error_t cl_err = CL_QUERY_OK;
	cl_query_table_t table_contents;
	scnas_errno_t scnaserr = SCNAS_NOERR;
	uint_t i = 0, j = 0;
	uint_t count = 0;
	char *tmp_val;
	char *prop_name;

	/* Check the args */
	if (!table_name) {
		return (SCNAS_EUNEXPECTED);
	}

	/* Check that filer_prop_t is not NULL */
	if (*prop == NULL) {
		return (SCNAS_EUNEXPECTED);
	}

	/* Read the table */
	table_contents.n_rows = 0;
	cl_err =  cl_query_read_table(table_name, &table_contents);
	if (cl_err != CL_QUERY_OK) {
		scnaserr = scnas_conv_clquery_err(cl_err);
		goto cleanup;
	}

	/* Get the filer name */
	if (table_contents.table_rows[0]->key != NULL) {
		tmp_val = table_contents.table_rows[0]->value;
	} else {
		tmp_val = "<NULL>";
	}

	(*prop) -> filer_name = (char *)calloc(1, strlen(tmp_val) + 1);
	if ((*prop) -> filer_name == NULL) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	(void) sprintf((*prop) -> filer_name, "%s", tmp_val);

	/* Go through other properties, except directories */
	for (i = 1; i < table_contents.n_rows; i++) {
		/* Get the property name */
		prop_name =
			nas_get_prop_name(table_contents.table_rows[i]->key);

		/* Skip */
		if (!prop_name)
			continue;
		if (strcmp(prop_name, NAS_FILER_NAME) == 0)
			continue;
		if (strcmp(prop_name, NAS_DIRECTORIES) == 0) {
			continue;

		} else if (strcmp(prop_name, NAS_PASSWD) == 0) {
			(*prop) -> filer_passwd = (char *)calloc(1, 8);
			if ((*prop) -> filer_passwd == NULL) {
				scnaserr = SCNAS_ENOMEM;
				goto cleanup;
			}

			(void) sprintf((*prop) -> filer_passwd, "%s",
				"*******");

		} else if (strcmp(prop_name, NAS_USERID) == 0) {
			tmp_val =
				table_contents.table_rows[i]->value;

			(*prop) -> filer_uid = (char *)
				calloc(1, strlen(tmp_val) + 1);
			if ((*prop) -> filer_uid == NULL) {
				scnaserr = SCNAS_ENOMEM;
				goto cleanup;
			}

			(void) sprintf((*prop) -> filer_uid, "%s", tmp_val);

		} else if (strcmp(prop_name, NAS_FILER_TYPE) == 0) {
			tmp_val = table_contents.table_rows[i]->value;

			(*prop) -> filer_type = (char *)
				calloc(1, strlen(tmp_val) + 1);

			if ((*prop) -> filer_type == NULL) {
				scnaserr = SCNAS_ENOMEM;
				goto cleanup;
			}

			(void) sprintf((*prop) -> filer_type, "%s", tmp_val);
		}
	}

	/* Allocate space for the list of directories exported */
	for (i = 1; i < table_contents.n_rows; i++) {
		prop_name =
			nas_get_prop_name(table_contents.table_rows[i]->key);

		if (strcmp(prop_name, NAS_DIRECTORIES) == 0) {
			count++;
		}
	}

	(*prop) -> filer_dirs_count = count;

	if (count > 0) {
		(*prop) -> filer_dirs = (char **)calloc(1,
		    (count * sizeof (char *)));

		if ((*prop) -> filer_dirs == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Go through all properties and extract only directories */
		for (i = 1; i < table_contents.n_rows; i++) {
			/* Get the property name */
			prop_name =
			nas_get_prop_name(table_contents.table_rows[i]->key);

			if (strcmp(prop_name, NAS_DIRECTORIES) == 0) {
				tmp_val = table_contents.table_rows[i]->value;

				((*prop) -> filer_dirs)[j] = (char *)
					calloc(1, strlen(tmp_val) + 1);
				if (((*prop) -> filer_dirs)[j] == NULL) {
					scnaserr = SCNAS_ENOMEM;
					goto cleanup;
				}

				(void) sprintf(((*prop) -> filer_dirs)[j],
							"%s", tmp_val);

				j++;
			}
		}
	}

cleanup:
	/* Free memory */
	cl_err = cl_query_free_table(&table_contents);
	if (cl_err != CL_QUERY_OK && scnaserr == SCNAS_NOERR) {
		scnaserr = scnas_conv_clquery_err(cl_err);
	}

	return (scnaserr);
}
