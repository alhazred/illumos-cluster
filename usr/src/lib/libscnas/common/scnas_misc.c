/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scnas_misc.c	1.8	08/05/20 SMI"

/*
 * libscnas common functions
 */

#include "scnas.h"

/*
 * scnas_strerr
 *
 *	Map scnas_errno_t to a string.
 *
 *	The supplied "errbuffer" should be of at least
 *	SCCONF_MAXSTRINGLEN in length.
 */
void
scnas_strerr(char *errbuffer, scnas_errno_t err)
{
	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Check arguments */
	if (errbuffer == NULL)
		return;

	/* Make sure that the buffer is terminated */
	errbuffer[SCNAS_MAXSTRINGLEN - 1] = '\0';

	/* Find the string */
	switch (err) {
	case SCNAS_NOERR:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN, "no error"),
		    SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_EPERM:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "permission denied"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_EEXIST:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "already exists"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_ENOEXIST:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "does not exist"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_EUNKNOWN:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "unknown type"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_ENOCLUSTER:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "cluster does not exist"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_ENODEID:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "node ID used in place of name"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_EINVAL:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "invalid argument"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_EUSAGE:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "usage error"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_EDIREXIST:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "directories still exist"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_EAUTH:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "authentication error, access denied"),
		    SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_ENOMEM:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "not enough memory"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_EUNEXPECTED:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "unexpected error"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_EINVALPROP:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "invalid filer configuration"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_NO_BINARY:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "no required binary"), SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_ERECONFIG:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "cluster is reconfiguring, please try again later"),
		    SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_VP_MISMATCH:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "software version not match"),
		    SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_ENOFILE:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "cannot access the input file"),
		    SCNAS_MAXSTRINGLEN - 1);
		break;

	case SCNAS_ESETUP:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "NAS device configuration problem"),
		    SCNAS_MAXSTRINGLEN - 1);
		break;

	default:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "unknown error"), SCNAS_MAXSTRINGLEN - 1);
		break;
	}
}

/*
 * scnas_conv_scconf_err
 *
 *	Convert scconf_errno_t to scnas_errno_t.
 */
scnas_errno_t
scnas_conv_scconf_err(scconf_errno_t err)
{
	switch (err) {
	case SCCONF_NOERR:
		return (SCNAS_NOERR);

	case SCCONF_EPERM:
		return (SCNAS_EPERM);

	case SCCONF_ENOMEM:
		return (SCNAS_ENOMEM);

	case SCCONF_EINVAL:
		return (SCNAS_EINVAL);

	case SCCONF_ENOCLUSTER:
		return (SCNAS_ENOCLUSTER);

	case SCCONF_ENODEID:
		return (SCNAS_ENODEID);

	case SCCONF_EUSAGE:
		return (SCNAS_EUSAGE);

	default:
		return (SCNAS_EUNEXPECTED);
	}
}

/*
 * scnas_conv_clquery_err
 *
 *	Convert cl_query_error_t to scnas_errno_t.
 */
scnas_errno_t
scnas_conv_clquery_err(cl_query_error_t err)
{
	switch (err) {
	case CL_QUERY_OK:
		return (SCNAS_NOERR);

	case CL_QUERY_EPERM:
		return (SCNAS_EPERM);

	case CL_QUERY_ENOMEM:
		return (SCNAS_ENOMEM);

	case CL_QUERY_EINVAL:
		return (SCNAS_EINVAL);

	case CL_QUERY_ENOTCLUSTER:
		return (SCNAS_ENOCLUSTER);

	case CL_QUERY_ECLUSTERRECONFIG:
		return (SCNAS_ERECONFIG);

	case CL_QUERY_ENOENT:
	case CL_QUERY_ENOTCONFIGURED:
		return (SCNAS_ENOEXIST);

	case CL_QUERY_EXISTS:
		return (SCNAS_EEXIST);

	default:
		return (SCNAS_EUNEXPECTED);
	}
}

/*
 * scnas_rot13_alg
 *
 *	The simple Caesar-cypher encryption that replaces each
 *	English letter with the one 13 places forward or back
 *	along the alphabet. This is to accommodate the PSARC
 *	sucruity guide to save password in plain text file.
 */
void
scnas_rot13_alg(char **string)
{
	char a;
	char *ptr;

	if (string == NULL ||
	    *string == (char *)0 ||
	    string == (char **)0) {
		return;
	}

	ptr = *string;
	do {
		/*
		 * "a" must be signed here for this alg.
		 * So suppress the "Expected unsigned type"
		 * lint warning.
		 */
		a = ~(*ptr);				/*lint !e502 */
		*ptr++ = (~a-1/(~(a|32)/13*2-11)*13);	/*lint !e502 */
	} while (*ptr);
}

/*
 * scnas_read_passwd_file
 *
 *	Read the input file for the password. The password can be
 *	saved in an input file provided by "-f <passwdfile>"
 */
scnas_errno_t
scnas_read_passwd_file(char *input_file, char **passwd)
{
	FILE *fp;
	char input_buf[SCNAS_MAXSTRINGLEN];
	char *ptr, *tmp_p;
	int char_count, i;
	boolean_t f_passwd = B_FALSE;

	/* Check args */
	if (!input_file || !passwd) {
		return (SCNAS_EINVAL);
	}

	/* Open the file */
	if ((fp = fopen(input_file, "r")) == 0) {
		return (SCNAS_ENOFILE);
	}

	/* Read the file */
	while (fgets(input_buf, sizeof (input_buf), fp)) {
		/* Remove new line */
		tmp_p = (char *)strtok(input_buf, "\n");
		ptr = tmp_p;

		/* Remove leading spaces/Tabs */
		do {
			if (ptr == NULL)
				break;

			if (*ptr != ' ' && *ptr != '\t')
				break;
		} while ((*ptr++ = *tmp_p++) != '\0');

		/* Skip lines only has spaces/Tabs */
		if (ptr == NULL || *ptr == '\0' ||
		    *ptr == '\t' || *ptr == '#')
			continue;

		/* Remove comments */
		char_count = 0;
		*passwd = tmp_p;

		/*
		 * Here the ptr and tmp_p will not be NILL.
		 * So it is ok to suppress the lint Info(794)
		 * message.
		 */
		/*lint -e794 */
		do {
			if (ptr == NULL || *ptr == '#')
				break;
			char_count++;
		} while ((*ptr++ = *tmp_p++) != '\0');

		/* Skip line with only comments */
		if (char_count == 0)
			continue;

		/* Remove the space/Tabs at the very end */
		if (ptr != NULL && *ptr != '#')
			ptr--;
		ptr--;

		i = char_count;
		do {
			if (ptr == NULL)
				break;
			if (*ptr != ' ' && *ptr != '\t')
				break;

			ptr--;
			i--;
		} while (i > 0);
		*(ptr+1) = '\0';

		if (!(*passwd) || **passwd == '#')
			continue;

		f_passwd = B_TRUE;
		break;
	}

	if (!f_passwd) {
		*passwd = NULL;
	}

	if (fp) {
		(void) fclose(fp);
	}

	return (SCNAS_NOERR);
}
