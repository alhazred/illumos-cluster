/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scnas_props.c	1.5	08/05/20 SMI"

/*
 * libscnas common functions
 */

#include "scnas.h"

/*
 * scnas_free_proplist
 *
 *	Free all memory associated with the given property "list".
 */
void
scnas_free_proplist(scnas_filer_prop_t *list)
{
	register scnas_filer_prop_t **listp;
	register scnas_filer_prop_t *last;

	/* check arg */
	if (list == (scnas_filer_prop_t *)0) {
		return;
	}

	/* free memory for each member of the list */
	listp = &list;
	while (*listp) {
		if ((*listp)->scnas_prop_key) {
			free((*listp)->scnas_prop_key);
		}

		if ((*listp)->scnas_prop_value) {
			free((*listp)->scnas_prop_value);
		}

		last = *listp;
		*listp = (*listp)->scnas_prop_next;
		free(last);
	}
}

/*
 * scnas_get_indexes
 *
 *	Get the unused index numberes from the given table
 *	contents, depends on the index format specified by the
 *	table_flag.
 *
 *	Caller is responsible to free the memory allocated
 *	to the unused index list.
 *
 * Return:
 *
 *	NULL                    - error
 *	non-NULL uint_t         - the index numberses
 */
uint_t *
scnas_get_indexes(cl_query_table_t *table_contents, char *index_form,
    uint_t *index_length)
{
	uint_t *indexes = NULL;
	uint_t index_num;
	uint_t i;

	*index_length = 0;

	/* Find the max index */
	for (i = 0; i < table_contents->n_rows; i++) {
		if (sscanf(table_contents->table_rows[i]->key,
		    index_form, &index_num) != 1)
			continue;

		*index_length = (index_num > *index_length) ?
		    index_num : *index_length;
	}

	if (!(*index_length))
		return (NULL);

	/* Create the index map */
	indexes = (uint_t *)calloc((size_t)*index_length, sizeof (uint_t));

	if (indexes == NULL) {
		return (NULL);
	}

	for (i = 0; i < table_contents->n_rows; i++) {
		if (sscanf(table_contents->table_rows[i]->key,
		    index_form, &index_num) != 1)
			continue;

		/* Should not get here. If so, return NULL */
		if (!index_num) {
			return (NULL);
		}

		/* Set the map */
		*(indexes + index_num - 1) = 1;
	}

	return (indexes);
}

/*
 * scnas_next_index
 *
 *      Get the next usable index number from the index map.
 *
 * Return:
 *
 *	uint_t			- the index
 */
uint_t
scnas_next_index(uint_t start, uint_t end, uint_t *index_map)
{
	uint_t i;

	if (index_map == NULL) {
		return (1);
	}

	if (start > end) {
		return (start);
	}

	for (i = start; i < end; i++) {
		if (!index_map[i]) {
			return (i + 1);
		}
	}

	return (end + 1);
}

/*
 * scnas_get_prop_val
 *
 *	From the given scnas_filer_prop_t list, return the property
 *	value for the given *key.
 */
char *
scnas_get_prop_val(scnas_filer_prop_t *proplist_p, char *key, uint_t *flg)
{
	register scnas_filer_prop_t *listp;

	/* Check args */
	if (flg) {
		*flg = NAS_NOTFOUND_KEY;
	}

	if ((proplist_p == (scnas_filer_prop_t *)0) ||
	    (!key)) {
		return (NULL);
	}

	/* Go through the list */
	for (listp = proplist_p; listp;
	    listp = listp->scnas_prop_next) {
		if (!(listp->scnas_prop_key))
			continue;

		if (strcmp(listp->scnas_prop_key, key) != 0)
			continue;

		/* Find it */
		if (flg) {
			*flg = NAS_FOUND_KEY;
		}

		return (listp->scnas_prop_value);
	}

	return (NULL);
}

/*
 * scnas_get_prop_key
 *
 *	From the given scnas_filer_prop_t list, return the property
 *	key for the given *value.
 */
char *
scnas_get_prop_key(scnas_filer_prop_t *proplist_p, char *value, uint_t *flg)
{
	register scnas_filer_prop_t *listp;

	/* Check args */
	if (flg) {
		*flg = NAS_NOTFOUND_KEY;
	}

	if ((proplist_p == (scnas_filer_prop_t *)0) ||
	    (!value)) {
		return (NULL);
	}

	/* Go through the list */
	for (listp = proplist_p; listp;
	    listp = listp->scnas_prop_next) {
		if (!(listp->scnas_prop_value))
			continue;

		if (strcmp(listp->scnas_prop_value, value) != 0)
			continue;

		/* Find it */
		if (flg) {
			*flg = NAS_FOUND_KEY;
		}

		return (listp->scnas_prop_key);
	}

	return (NULL);
}

/*
 * scnas_add_one_prop
 *
 *	Add the given "key, val" pair into the end of the property list
 *	proplist_p, if it does not exist in the list.
 *
 *	Caller is responsible to free the memory allocated for the new
 *	property.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_add_one_prop(const char *key, const char *val,
    scnas_filer_prop_t **proplist_p)
{
	scnas_errno_t rstatus = SCNAS_NOERR;
	scnas_filer_prop_t *prop;
	register scnas_filer_prop_t **listp;
	int found = 0;

	/* Check arguments */
	if (!key) {
		return (SCNAS_EINVAL);
	}

	if (proplist_p == NULL) {
		return (SCNAS_EINVAL);
	}

	/* Go through the list */
	for (listp = proplist_p; *listp;
	    listp = &(*listp)->scnas_prop_next) {

		/* Skip some properties */
		if (!(*listp)->scnas_prop_key)
			continue;

		if (strcmp((*listp)->scnas_prop_key, key) != 0)
			continue;

		if ((*listp)->scnas_prop_value) {
			if (val &&
			    (strcmp(val, (*listp)->scnas_prop_value)
			    == 0)) {
				found = 1;
				break;
			}
		} else if (!val) {
			found = 1;
			break;
		}
	}

	if (found) {
		return (SCNAS_NOERR);
	} else {
		/* Allocate the new prop structure */
		prop = (scnas_filer_prop_t *)calloc(1,
		    sizeof (scnas_filer_prop_t));
		if (prop == (scnas_filer_prop_t *)0) {
			rstatus = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Duplicate the key */
		if ((key) &&
		    ((prop->scnas_prop_key = strdup(key)) == NULL)) {
			rstatus = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Dup the value */
		if ((val) &&
		    ((prop->scnas_prop_value = strdup(val)) == NULL)) {
			rstatus = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Add to the list */
		prop->scnas_prop_next = *proplist_p;
		*proplist_p = prop;

	}

	return (rstatus);

cleanup:
	/* Free property */
	if (prop) {
		if (prop->scnas_prop_key)
			free(prop->scnas_prop_key);
		if (prop->scnas_prop_value)
			free(prop->scnas_prop_value);
		free(prop);
	}

	return (rstatus);
}

/*
 * scnas_clquery_add_elem
 *
 *	Add a new element into the cl_query table contents.
 *
 *	Caller is responsible to free memory allocated for the
 *	new element.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_clquery_add_elem(cl_query_table_t *table_contents,
    char *key, char *value)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	cl_query_table_elem_t *new_row = (cl_query_table_elem_t *)0;

	/* Check args */
	if (!key) {
		return (SCNAS_EINVAL);
	}

	if ((table_contents == (cl_query_table_t *)0) ||
	    (table_contents->table_rows == NULL)) {
		return (SCNAS_EINVAL);
	}

	/* Alloca memory for the element */
	new_row = (cl_query_table_elem_t *)calloc(1,
	    sizeof (cl_query_table_elem_t));
	if (new_row == (cl_query_table_elem_t *)0) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	/* Copy the key */
	if (key) {
		if ((new_row->key = strdup(key)) == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	}

	/* Copy the value */
	if (value) {
		if ((new_row->value = strdup(value)) == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	}

	/* Set number of rows */
	table_contents->n_rows++;

	/* Realloc memory */
	table_contents->table_rows = (cl_query_table_elem_t **)
	    realloc(table_contents->table_rows,
	    sizeof (cl_query_table_elem_t) * table_contents->n_rows);
	if (table_contents->table_rows ==
	    (cl_query_table_elem_t **)0) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	/* Add the new row into table */
	table_contents->table_rows[table_contents->n_rows - 1] =
	    &(*new_row);

	return (scnaserr);

cleanup:
	/* Free memory */
	if (new_row) {
		if (new_row->key)
			free(new_row->key);
		if (new_row->value)
			free(new_row->value);
		free(new_row);
	}

	return (scnaserr);
}

/*
 * scnas_clquery_add_props
 *
 *	Add the given props_list into the cl_query table
 *	contents.
 *
 *	Caller is responsible to free memory allocated for the
 *	new elements.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_clquery_add_props(cl_query_table_t *table_contents,
    scnas_filer_prop_t *prop_list, uint_t props_num)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	cl_query_table_elem_t *new_row = (cl_query_table_elem_t *)0;
	register scnas_filer_prop_t *prop;
	uint_t i;

	/* Check args */
	if ((table_contents == (cl_query_table_t *)0) ||
	    (table_contents->table_rows == NULL)) {
		return (SCNAS_EINVAL);
	}

	if ((prop_list == (scnas_filer_prop_t *)0) ||
	    (props_num == 0)) {
		return (SCNAS_EINVAL);
	}

	i = table_contents->n_rows;

	/* Allocate memory for the table contents */
	table_contents->n_rows += props_num;
	table_contents->table_rows = (cl_query_table_elem_t **)
	    realloc(table_contents->table_rows,
	    sizeof (cl_query_table_elem_t) * table_contents->n_rows);
	if (table_contents->table_rows ==
	    (cl_query_table_elem_t **)0) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	/* Go through the prop list */
	for (prop = prop_list; prop; prop = prop->scnas_prop_next) {

		/* Skip a prop */
		if (!(prop->scnas_prop_key))
			continue;

		if (!(prop->scnas_prop_value))
			continue;

		/* Allocate a new cl_query table entry */
		new_row = (cl_query_table_elem_t *)calloc(1,
		    sizeof (cl_query_table_elem_t));
		if (new_row == (cl_query_table_elem_t *)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Copy the key */
		if ((new_row->key = strdup(
		    prop->scnas_prop_key)) == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Copy the value */
		if ((new_row->value = strdup(
		    prop->scnas_prop_value)) == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Add the new row into the table */
		table_contents->table_rows[i++] =
		    &(*new_row);
	}

	return (scnaserr);

cleanup:
	/* Free memory */
	if (new_row) {
		if (new_row->key)
			free(new_row->key);
		if (new_row->value)
			free(new_row->value);
		free(new_row);
	}

	return (scnaserr);
}

/*
 * scnas_clquery_set_props
 *
 *	Set the key/value pairs from the given structure
 *	scnas_filer_prop_t *prop_list into the cl_query
 *	table. Change the value setting if the key exists
 *	in the cl_query table; Or add the pair if not
 *	exists.
 *
 *	Caller is repsonsible to free memory allocated for the
 *	new elements.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_clquery_set_props(cl_query_table_t *table_contents,
    scnas_filer_prop_t *prop_list, uint_t props_num)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	cl_query_table_elem_t *new_row = (cl_query_table_elem_t *)0;
	register scnas_filer_prop_t *prop;
	uint_t the_index;
	uint_t old_nrows;
	uint_t new_nrows;
	char *val_found;

	/* Check args */
	if ((table_contents == (cl_query_table_t *)0) ||
	    (table_contents->table_rows == NULL)) {
		return (SCNAS_EINVAL);
	}

	if (prop_list == (scnas_filer_prop_t *)0) {
		return (SCNAS_EINVAL);
	}

	new_nrows = 0;
	old_nrows = 0;

	/* Allocate memory for the table contents */
	if (props_num) {
		old_nrows = table_contents->n_rows;

		table_contents->table_rows = (cl_query_table_elem_t **)
		    realloc(table_contents->table_rows,
		    sizeof (cl_query_table_elem_t) * (old_nrows + props_num));

		if (table_contents->table_rows ==
		    (cl_query_table_elem_t **)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	}

	/* Go through the prop list */
	for (prop = prop_list; prop; prop = prop->scnas_prop_next) {

		/* Skip a prop */
		if (!(prop->scnas_prop_key))
			continue;

		if (!(prop->scnas_prop_value))
			continue;

		/* See if it exists already */
		val_found = scnas_clquery_find_by_key(
		    table_contents, prop->scnas_prop_key, &the_index);

		if (val_found) {
			if (strcmp(prop->scnas_prop_value, val_found)
			    == 0) {
				continue;
			}

			/* Set the value */
			free(table_contents->table_rows[the_index]->value);
			table_contents->table_rows[the_index]->value =
			    strdup(prop->scnas_prop_value);

			if (table_contents->table_rows[the_index]->value ==
			    NULL) {
				scnaserr = SCNAS_ENOMEM;
				goto cleanup;
			}
		} else {
			/* Allocate memory for the element */
			new_row = (cl_query_table_elem_t *)calloc(1,
			    sizeof (cl_query_table_elem_t));
			if (new_row == (cl_query_table_elem_t *)0) {
				scnaserr = SCNAS_ENOMEM;
				goto cleanup;
			}

			/* Copy the key */
			if ((new_row->key = strdup(prop->scnas_prop_key))
			    == NULL) {
				scnaserr = SCNAS_ENOMEM;
				goto cleanup;
			}

			/* Copy the value */
			if ((new_row->value = strdup(prop->scnas_prop_value))
			    == NULL) {
				scnaserr = SCNAS_ENOMEM;
				goto cleanup;
			}

			new_nrows++;

			/* Add the new row into table */
			table_contents->table_rows[old_nrows++] =
			    &(*new_row);
		}
	}

	table_contents->n_rows += new_nrows;

	if (new_nrows != props_num) {
		table_contents->table_rows = (cl_query_table_elem_t **)
		    realloc(table_contents->table_rows,
		    sizeof (cl_query_table_elem_t) * table_contents->n_rows);

		if (table_contents->table_rows ==
		    (cl_query_table_elem_t **)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	}

	return (scnaserr);
cleanup:
	/* Free memory */
	if (new_row) {
		if (new_row->key)
			free(new_row->key);
		if (new_row->value)
			free(new_row->value);
		free(new_row);
	}

	return (scnaserr);

}


/*
 * scnas_clquery_rm_elem
 *
 *	Delete the given "key", "value" pair from the cl_query
 *	table content. The content after removing is returned
 *	in the *new_table_contents structure.
 *
 *	Caller is responsible to free the memories allocated
 *	to the *new_table_contents.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- out of memory
 */
scnas_errno_t
scnas_clquery_rm_elem(cl_query_table_t *table_contents,
    char *key, char *value, cl_query_table_t *new_table)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	boolean_t found = B_FALSE;
	uint_t i, j;

	/* Check args */
	if (!key || !value) {
		return (SCNAS_EINVAL);
	}

	if ((table_contents == (cl_query_table_t *)0) ||
	    (table_contents->n_rows == 0) ||
	    (table_contents->table_rows == NULL)) {
		return (SCNAS_EINVAL);
	}

	if (new_table == (cl_query_table_t *)0) {
		return (SCNAS_EINVAL);
	}

	/* Allocate memory for the new table */
	new_table->n_rows = table_contents->n_rows;
	new_table->table_rows = (cl_query_table_elem_t **)
	    calloc(new_table->n_rows, sizeof (cl_query_table_elem_t));
	if (new_table->table_rows == (cl_query_table_elem_t **)0) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	/* Go through the table */
	j = 0;
	for (i = 0; i < table_contents->n_rows; i++) {

		/* Skip */
		if (!(table_contents->table_rows[i]->key))
			continue;

		if (!(table_contents->table_rows[i]->value))
			continue;

		/* Delete the element */
		if ((strcmp(table_contents->table_rows[i]->key, key)
		    == 0) &&
		    (strcmp(table_contents->table_rows[i]->value, value)
		    == 0)) {
			found = B_TRUE;
		} else {
			new_table->table_rows[j++] =
			    table_contents->table_rows[i];
		}
	}

	/* If not found */
	if (!found) {
		scnaserr = SCNAS_ENOEXIST;
		goto cleanup;
	}

	/* Re-allocate memory for the new table */
	new_table->n_rows = j;
	if (new_table->n_rows) {
		new_table->table_rows = (cl_query_table_elem_t **)
		    realloc(new_table->table_rows,
		    sizeof (cl_query_table_elem_t) * new_table->n_rows);

		if (new_table->table_rows == (cl_query_table_elem_t **)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	}

	return (scnaserr);

cleanup:
	/* Free memories */
	if (new_table->table_rows) {
		free(new_table->table_rows);
	}

	return (scnaserr);
}


/*
 * scnas_clquery_find_by_key
 *
 *	Find the value corresponding to the given key in the
 *	cl_query table structure. The index of the found value
 *	is set in *index.
 */
char *
scnas_clquery_find_by_key(cl_query_table_t *table_contents,
    char *key, uint_t *the_index)
{
	uint_t i;

	/* Check the args */
	if (!key) {
		return (NULL);
	}

	if ((table_contents == (cl_query_table_t *)0) ||
	    (table_contents->table_rows == NULL) ||
	    (table_contents->n_rows == 0)) {
		return (NULL);
	}

	if (the_index) {
		*the_index = 0;
	}

	/* Go through the table */
	for (i = 0; i < table_contents->n_rows; i++) {

		/* Skip the key if empty */
		if (!(table_contents->table_rows[i]->key))
			continue;

		if (strcmp(table_contents->table_rows[i]->key,
		    key) == 0) {

			if (the_index) {
				*the_index = i;
			}
			return (table_contents->table_rows[i]->value);
		}
	}

	return (NULL);
}

/*
 * scnas_clquery_find_by_keyform
 *
 *	Check if the given cl_query table contains any rows
 *	whose key is of the specified *key_form;
 *
 * Return:
 *
 *	B_TRUE			- find it
 *	B_FALSE			- cannot find it
 */
boolean_t
scnas_clquery_find_by_keyform(cl_query_table_t *table_contents,
    char *key_form)
{
	boolean_t found = B_FALSE;
	uint_t i;

	/* Check the args */
	if (!key_form) {
		return (B_FALSE);
	}

	if ((table_contents == (cl_query_table_t *)0) ||
	    (table_contents->table_rows == NULL) ||
	    (table_contents->n_rows == 0)) {
		return (B_FALSE);
	}

	/* Go through the table */
	for (i = 0; i < table_contents->n_rows; i++) {

		/* Skip the key if empty */
		if (!(table_contents->table_rows[i]->key))
			continue;

		if (strstr(table_contents->table_rows[i]->key,
		    key_form) != NULL) {
			found = B_TRUE;
			break;
		}
	}

	return (found);
}

/*
 * scnas_clquery_find_by_val
 *
 *      Find the key corresponding to the given value in the
 *      cl_query table structure. The "length" is the number
 *	of table rows to search for. The index of the found
 *	value is set in *the_index.
 *
 */
char *
scnas_clquery_find_by_val(cl_query_table_t *table_contents,
    char *value, uint_t length, uint_t *the_index)
{
	uint_t i;
	uint_t rows;

	/* Check the args */
	if (!value) {
		return (NULL);
	}

	if ((table_contents == (cl_query_table_t *)0) ||
	    (table_contents->table_rows == NULL) ||
	    (table_contents->n_rows == 0)) {
		return (NULL);
	}

	/* Initialize */
	if (the_index) {
		*the_index = 0;
	}

	if (length) {
		rows = length;
	} else {
		rows = table_contents->n_rows;
	}

	/* Go through the table */
	for (i = 0; i < rows; i++) {

		/* skip */
		if (!(table_contents->table_rows[i]->value))
			continue;

		if (strcmp(value,
		    table_contents->table_rows[i]->value) == 0) {
			if (the_index) {
				*the_index = i;
			}

			return (table_contents->table_rows[i]->key);
		}
	}

	return (NULL);
}
