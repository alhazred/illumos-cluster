#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.43	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libscha/Makefile.com
#

LIBRARY= libscha.a
VERS= .1
include $(SRC)/Makefile.master

# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

RPC_OBJECTS =	rgm_recep_door_xdr.o
$(RPC_IMPL)RPC_OBJECTS =   rgm_recep_clnt.o rgm_recep_xdr.o

DOOR_COM_OBJECTS = rgm_recep_clnt_xdr.o
$(RPC_IMPL)DOOR_COM_OBJECTS =
COM_OBJECTS =	scha_lb_api.o scha_rs_api.o scha_rt_api.o \
		scha_rg_api.o scha_cluster_api.o scha_api_rpc.o\
		scha_control.o scha_util.o $(DOOR_COM_OBJECTS)

OBJECTS = $(COM_OBJECTS) $(RPC_OBJECTS)

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

# include the macros for doors or rpc implementation again because
# Makefile.lib screws up CFLAGS/CPPFLAGS
include $(SRC)/Makefile.ipc 

LINTFILES += $(OBJECTS:%.o=%.ln)

RPCFILE =	$(ROOTCLUSTHDRDIR)/rgm/door/rgm_recep_door.x
RPCGENFLAGS =           $(RPCGENMT) -C -K -1
RPCFILE_XDR =           ../common/rgm_recep_door_xdr.c
RPCFILES =      $(RPCFILE_XDR)

$(RPC_IMPL)RPCFILE =	$(ROOTCLUSTHDRDIR)/rgm/rpc/rgm_recep.x
$(RPC_IMPL)RPCGENFLAGS =           $(RPCGENMT) -C -K -1
$(RPC_IMPL)RPCFILE_CLNT =          ../common/rgm_recep_clnt.c
$(RPC_IMPL)RPCFILE_XDR =           ../common/rgm_recep_xdr.c
$(RPC_IMPL)RPCFILES =      $(RPCFILE_CLNT) $(RPCFILE_XDR)

CLOBBERFILES += $(RPCFILES)
CHECKHDRS = scha_lib.h
CHECK_FILES = $(COM_OBJECTS:%.o=%.c_check) $(CHECKHDRS:%.h=%.h_check)

MAPFILE=	../common/mapfile-vers
SRCS =		$(OBJECTS:%.o=../common/%.c)

LIBS = $(DYNLIB) $(SPROLINTLIB) $(LIBRARY)

$(SPROLINTLIB):= SRCS += ../common/llib-lscha

CLEANFILES =	$(SPROLINTOUT) $(SPROLINTLIB)

CPPFLAGS += -D_REENTRANT

#
# WARNING: Do not include any libraries that cause libthread to be included
#          as Oracle is a consumer of this library and cannot be linked
#          with libthread.
#
LDLIBS += -lc -lnsl -lsecurity -lclos
LDLIBS += $(DOOR_LDLIBS)
PMAP =	-M $(MAPFILE)

.KEEP_STATE:

.NO_PARALLEL:

all: $(RPCFILES) $(LIBS) $(SPROLINTLIB)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
