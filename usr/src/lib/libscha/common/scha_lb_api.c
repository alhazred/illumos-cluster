/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scha_lb_api.c	1.12	08/07/21 SMI"

#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include "scha_lib.h"
#include <scha_lb.h>
#include <sys/errno.h>

/*
 * scha_ssm_lb_get_policy()
 *
 */
int
scha_ssm_lb_get_policy_type(const char *resource, int *policyp)
{
	return (scha_ssm_lb_get_policy_type_zone(NULL, resource, policyp));
}
int
scha_ssm_lb_get_policy_type_zone(const char *cluster,
    const char *resource, int *policyp)
{
	int		*data;
	int		data_len;
	int		err;

	err = scha_lb(cluster,
	    resource, GET_POLICY_TYPE, NULL, 0, &data, &data_len);
	if (err != 0) {
		return (err);
	}

	if (data_len != 1) {
		return (EINVAL);
	}

	*policyp = data[0];
	free(data);
	return (0);
}

int
scha_ssm_lb_get_distribution(const char *resource, struct dist_info **infop)
{
	return (scha_ssm_lb_get_distribution_zone(NULL, resource, infop));
}
int
scha_ssm_lb_get_distribution_zone(const char *cluster,
    const char *resource, struct dist_info **infop)
{
	int		*data;
	int		data_len;
	int		err;
	int		num_nodes;
	struct dist_info	*info;
	int		i;

	err = scha_lb(cluster,
	    resource, GET_DISTRIBUTION, NULL, 0, &data, &data_len);
	if (err != 0) {
		return (err);
	}

	num_nodes = data_len / 2;

	info = (struct dist_info *)malloc(sizeof (struct dist_info) +
	    (uint_t)num_nodes * (sizeof (enum node_status) + sizeof (int)));
	if (info == NULL) {
		free(data);
		return (ENOMEM);
	}
	info->num_nodes = num_nodes;
	info->nodes = (enum node_status *)(info+1);
	info->weights = (int *)(info->nodes + num_nodes);

	for (i = 0; i < num_nodes; i++) {
		info->nodes[i] = data[2*i];
		info->weights[i] = data[2*i+1];
	}

	free(data);
	*infop = info;
	return (0);
}

int
scha_ssm_lb_set_distribution(const char *resource, struct weight_info *info)
{
	return (scha_ssm_lb_set_distribution_zone(NULL, resource, info));
}
int
scha_ssm_lb_set_distribution_zone(const char *cluster,
    const char *resource, struct weight_info *info)
{
	int		*args;
	int		err;
	int		i;

	args = malloc(sizeof (int) * (uint_t)info->num_nodes);
	if (args == NULL) {
		return (ENOMEM);
	}
	args[0] = 0; /* Node 0 is unused */
	for (i = 1; i < info->num_nodes; i++) {
		args[i] = info->weights[i];
	}
	err = scha_lb(cluster,
	    resource, SET_DISTRIBUTION, args, info->num_nodes, NULL,
	    NULL);
	free(args);
	if (err != 0) {
		return (err);
	}
	return (0);
}

int
scha_ssm_lb_set_one_distribution(const char *resource, int node, int weight)
{
	return (scha_ssm_lb_set_one_distribution_zone(
	    NULL, resource, node, weight));
}
int
scha_ssm_lb_set_one_distribution_zone(const char *cluster,
    const char *resource, int node, int weight)
{
	int		err;
	int		args[2];

	args[0] = node;
	args[1] = weight;
	err = scha_lb(cluster,
	    resource, SET_ONE_DISTRIBUTION, args, 2, NULL, NULL);
	if (err != 0) {
		return (err);
	}

	return (0);
}

int
scha_ssm_lb_set_perf_mon_status(const char *resource,
    int node, unsigned int load)
{
	return (scha_ssm_lb_set_perf_mon_status_zone(
	    NULL, resource, node, load));
}
int
scha_ssm_lb_set_perf_mon_status_zone(const char *cluster, const char *resource,
    int node, unsigned int load)
{
	int		err;
	int		args[2];

	args[0] = node;
	args[1] = (int)load;
	err = scha_lb(cluster,
	    resource, SET_PERF_MON_STATUS, args, 2, NULL, NULL);
	if (err != 0) {
		return (err);
	}

	return (0);
}

int
scha_ssm_lb_set_perf_mon_parameters(const char *resource,
    struct perf_mon_parameters *params)
{
	return (scha_ssm_lb_set_perf_mon_parameters_zone(
	    NULL, resource, params));
}
int
scha_ssm_lb_set_perf_mon_parameters_zone(const char *cluster,
    const char *resource,
    struct perf_mon_parameters *params)
{
	int		err;
	int		args[6];

	args[0] = (int)params->update_frequency;
	args[1] = (int)params->scale_rate;
	args[2] = (int)params->min_weight;
	args[3] = (int)params->max_weight;
	args[4] = (int)params->quiet_width;
	args[5] = (int)params->floor;

	err = scha_lb(cluster,
	    resource, SET_PERF_MON_PARAMETERS, args, 6, NULL, NULL);
	if (err != 0) {
		return (err);
	}

	return (0);
}

int
scha_ssm_lb_get_perf_mon_parameters(const char *resource,
    struct perf_mon_parameters **paramsp)
{
	return (scha_ssm_lb_get_perf_mon_parameters_zone(
	    NULL, resource, paramsp));
}
int
scha_ssm_lb_get_perf_mon_parameters_zone(const char *cluster,
    const char *resource,
    struct perf_mon_parameters **paramsp)
{
	int		*data;
	int		data_len;
	int		err;
	struct perf_mon_parameters	*params;

	params = (struct perf_mon_parameters *)malloc(
	    sizeof (struct perf_mon_parameters));
	if (params == NULL) {
		return (ENOMEM);
	}

	/*
	 * send request through RPC call to get the rs sequence id
	 */
	err = scha_lb(cluster,
	    resource, GET_PERF_MON_PARAMETERS, NULL, 0, &data,
	    &data_len);
	if (err != 0) {
		return (err);
	}

	if (data_len != 6) {
		return (EINVAL);
	}

	params->update_frequency = (unsigned int) data[0];
	params->scale_rate = (unsigned int) data[1];
	params->min_weight = (unsigned int) data[2];
	params->max_weight = (unsigned int) data[3];
	params->quiet_width = (unsigned int) data[4];
	params->floor = (unsigned int) data[5];

	free(data);
	*paramsp = params;
	return (0);
}

/*
 *	Parse a number from a sequence: num,num,num,num
 *	Return a pointer to the next number.
 *	Return NULL for an error.
 *	Before calling, check for no more data: str[0] == '\0'
 */
static const char *
parse_num(const char *str, unsigned *num_out)
{
	unsigned num = 0;
	for (; *str; str++) {
		if (*str >= '0' && *str <= '9') {
			/*lint -e737 */
			num = num*10 + ((*str)-'0');
		} else if (*str == ',' || *str == '/') {
			/* Skip over the comma. */
			str++;
			break;
		} else {
			/* Error. */
			return (NULL);
		}
	}
	*num_out = num;
	return (str);
}

/*
 *      Parse a load balancing info string.
 *      Sets buf to point to a malloc'd buffer, which must be freed later.
 *      Returns EINVAL if there is a parse error.
 */
int
scha_ssm_lb_parse_lb_info(const char *string, enum policy_type policy,
    void **bufp)
{
	return (scha_ssm_lb_parse_lb_info_zone(NULL, string, policy, bufp));
}
int
scha_ssm_lb_parse_lb_info_zone(const char *cluster,
    const char *string, enum policy_type policy,
    void **bufp)
{
	switch (policy) {
	case LB_WEIGHTED:
	case LB_ST:
	case LB_SW:
		{
		const char *str = string;
		unsigned node, weight, max_node = 0;
		struct weight_info *info;
		size_t size;
		/* Find the maximum node in the input list */
		while (str[0] != '\0') {
			str = parse_num(str, &node);
			if (str == NULL || str[0] == '\0') {
				/* Missing node or weight */
				return (EINVAL);
			}
			str = parse_num(str, &weight);
			if (str == NULL) {
				/* Missing weight */
				return (EINVAL);
			}
			if (node > max_node) {
				max_node = node;
			}
		}

		/* Create the weight_info structure */
		size = sizeof (struct dist_info) + sizeof (int) * (max_node+1);
		info = (struct weight_info *)malloc(size);
		if (info == NULL) {
			return (ENOMEM);
		}
		bzero(info, size);
		info->num_nodes = (int)max_node+1;
		/* weights points just after end of the weight_info struct */
		info->weights = (int *)(info+1);

		/* Fill in the weight table from the input string. */
		str = string;
		while (str[0] != '\0') {
			str = parse_num(str, &node);
			str = parse_num(str, &weight);
			info->weights[node] = (int)weight;
		}
		*bufp = info;
		}
		break;
	case LB_PERF:
		{
		const char *str = string;
		/* Create the params data structure */
		struct perf_mon_parameters *params =
		    (struct perf_mon_parameters *)
		    malloc(sizeof (struct perf_mon_parameters));
		if (params == NULL) {
			return (ENOMEM);
		}
		/* Fil in the params structure from the input string */
		str = parse_num(str, &params->update_frequency);
		if (str == NULL || str[0] == '\0') {
			return (EINVAL);
		}
		str = parse_num(str, &params->scale_rate);
		if (str == NULL || str[0] == '\0') {
			return (EINVAL);
		}
		str = parse_num(str, &params->min_weight);
		if (str == NULL || str[0] == '\0') {
			return (EINVAL);
		}
		str = parse_num(str, &params->max_weight);
		if (str == NULL || str[0] == '\0') {
			return (EINVAL);
		}
		str = parse_num(str, &params->quiet_width);
		if (str == NULL || str[0] == '\0') {
			return (EINVAL);
		}
		str = parse_num(str, &params->floor);
		/* str[0] should be 0 at this point, since we should have */
		/* exhausted the string. */
		if (str == NULL || str[0] != '\0') {
			return (EINVAL);
		}
		*bufp = params;
		}
		break;
	default:
		return (EINVAL);
	}
	return (0);
}

/*
 *      Set the load balancer from a previously parsed string.
 */
int
scha_ssm_lb_set_lb_info(const char *resource,
    enum policy_type policy, void *scha_ssm_lb_params)
{
	return (scha_ssm_lb_set_lb_info_zone(NULL, resource, policy,
	    scha_ssm_lb_params));
}
int
scha_ssm_lb_set_lb_info_zone(const char *cluster, const char *resource,
    enum policy_type policy, void *scha_ssm_lb_params)
{
	int err = 0;
	switch (policy) {
	case LB_WEIGHTED:
	case LB_ST:
	case LB_SW:
		err = scha_ssm_lb_set_distribution_zone(cluster, resource,
		    (struct weight_info *)scha_ssm_lb_params);
		break;
	case LB_PERF:
		err = scha_ssm_lb_set_perf_mon_parameters_zone(
		    cluster, resource,
		    (struct perf_mon_parameters *)scha_ssm_lb_params);
		break;
	default:
		err = EINVAL;
		break;
	}
	return (err);
}
