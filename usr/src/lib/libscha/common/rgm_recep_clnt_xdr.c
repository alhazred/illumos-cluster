/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_recep_clnt_xdr.c	1.7	08/09/05 SMI"

/*
 * rgm_recep_xdr.c - This file defines the functions
 * that are used for marshalling and unmarshalling of
 * data which is to be sent across door calls.
 * We use the xdr library for the marshalling and
 * unmarshalling operations
 */

#include <rgm/rgm_recep.h>
#include <sys/cl_assert.h>

bool_t
xdr_scha_result_args(XDR *xdrs, void *objp, int api_name)
{
	switch (api_name) {
		case SCHA_RS_OPEN:
			if (!xdr_rs_open_result_t(xdrs,
			    (rs_open_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET:
		case SCHA_GET_ALL_EXTPROPS:
		case SCHA_RS_GET_FAILED_STATUS:
		case SCHA_RS_GET_STATE:
		case SCHA_RT_GET:
		case SCHA_RG_GET:
		case SCHA_IS_PERNODE:
		case SCHA_RG_GET_STATE:
		case SCHA_CLUSTER_GET:
			if (!xdr_get_result_t(xdrs,
			    (get_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_SWITCH:
			if (!xdr_get_switch_result_t(xdrs,
			    (get_switch_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_EXT:
			if (!xdr_get_ext_result_t(xdrs,
			    (get_ext_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_STATUS:
			if (!xdr_get_status_result_t(xdrs,
			    (get_status_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_SET_STATUS:
			if (!xdr_set_status_result_t(xdrs,
			    (set_status_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RT_OPEN:
		case SCHA_RG_OPEN:
		case SCHA_CLUSTER_OPEN:
			if (!xdr_open_result_t(xdrs,
			    (open_result_t *)objp)) {
				return (FALSE);
			}
			break;

		case SCHA_SSM_IP:
			if (!xdr_ssm_result_t(xdrs,
			    (ssm_result_t *)objp)) {
				return (FALSE);
			}
			break;

		case SCHA_LB:
			if (!xdr_scha_ssm_lb_result_t(xdrs,
			    (scha_ssm_lb_result_t *)objp)) {
				return (FALSE);
			}
			break;

		case SCHA_CONTROL:
			if (!xdr_scha_result_t(xdrs,
			    (scha_result_t *)objp)) {
				return (FALSE);
			}
			break;
		default:
			CL_PANIC(0);
			break;

	}
	return (TRUE);
}

bool_t
xdr_scha_input_args(register XDR *xdrs, scha_input_args_t *objp)
{
	if (!xdr_int(xdrs, &objp->api_name)) {
		return ((FALSE));
	}
	switch (objp->api_name) {
		case SCHA_RS_OPEN:
			if (!xdr_rs_open_args(xdrs,
			    (rs_open_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET:
		case SCHA_RS_GET_EXT:
		case SCHA_GET_ALL_EXTPROPS:
			if (!xdr_rs_get_args(xdrs,
			    (rs_get_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_STATE:
		case SCHA_RS_GET_STATUS:
			if (!xdr_rs_get_state_status_args(xdrs,
			    (rs_get_state_status_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_FAILED_STATUS:
			if (!xdr_rs_get_fail_status_args(xdrs,
			    (rs_get_fail_status_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_SET_STATUS:
			if (! xdr_rs_set_status_args(xdrs,
			    (rs_set_status_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RT_OPEN:
			if (!xdr_rt_open_args(xdrs,
			    (rt_open_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_IS_PERNODE:
		case SCHA_RT_GET:
			if (!xdr_rt_get_args(xdrs,
			    (rt_get_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RG_OPEN:
			if (!xdr_rg_open_args(xdrs,
			    (rg_open_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RG_GET:
			if (!xdr_rg_get_args(xdrs,
			    (rg_get_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RG_GET_STATE:
			if (!xdr_rg_get_state_args(xdrs,
			    (rg_get_state_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_CLUSTER_GET:
			if (!xdr_cluster_get_args(xdrs,
			    (cluster_get_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_SWITCH:
			if (!xdr_rs_get_switch_args(xdrs,
			    (rs_get_switch_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_SSM_IP:
			if (!xdr_ssm_ip_args(xdrs,
			    (ssm_ip_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_LB:
			if (!xdr_scha_ssm_lb_args(xdrs,
			    (scha_ssm_lb_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_CLUSTER_OPEN:
			break;
		case SCHA_CONTROL:
			if (!xdr_scha_control_args(xdrs,
			    (scha_control_args *)objp->data)) {
				return (FALSE);
			}
			break;
		default:
			CL_PANIC(0);
			break;

	}
	return (TRUE);
}

/*
 * How we calculate size for xdr encoding:
 * ---------------------------------------
 * The Size calculation that we do here has to calculate
 * the sizeof the structure members and also the sizeof
 * the data that is pointed to be the pointers if any
 * int he structure.
 * For eg:
 * struct abc {
 *	int a;
 *	char *ch;
 * }
 * in this case we will find the size of the struct abc
 * which will give us the size needed for storing the
 * elements that is an int and a char pointer. And also
 * we will need to add the sizeof the array pointed to
 * by ch.
 * In addition to this, xdr encodes string in the following
 * way:
 * [integer(for size of the string) + string]
 * So, for any string say char *ch = {"example"}
 * The size needed will be :
 * sizeof (ch) + sizeof (int) + strlen(ch)
 * Here the sizeof (int) is taken for storing the sizeof
 * the string.
 */

size_t
scha_xdr_sizeof(scha_input_args_t *arg)
{
	size_t size = 0;
	uint_t i = 0;
	size = RNDUP(sizeof (arg->api_name));
	switch (arg->api_name) {
		case SCHA_RS_OPEN:
			size += RNDUP(sizeof (rs_open_args)) +
			    RNDUP(safe_strlen(((rs_open_args *)
			    (arg->data))->rs_name)) +
			    RNDUP(safe_strlen(((rs_open_args *)
			    (arg->data))->rg_name)) +
			    (2 * RNDUP(sizeof (int)));
			/*
			 * For the 2 strings
			 * in the structure
			 */

			break;
		case SCHA_RS_GET:
		case SCHA_RS_GET_EXT:
		case SCHA_GET_ALL_EXTPROPS:
			size += RNDUP(sizeof (rs_get_args)) +
			    RNDUP(safe_strlen(((rs_get_args *)
			    (arg->data))->rs_name)) +
			    RNDUP(safe_strlen(((rs_get_args *)
			    (arg->data))->rg_name)) +
			    RNDUP(safe_strlen(((rs_get_args *)
			    (arg->data))->rs_prop_name)) +
			    RNDUP(safe_strlen(((rs_get_args *)
			    (arg->data))->zonename)) +
			    RNDUP(safe_strlen(((rs_get_args *)
			    (arg->data))->rs_node_name)) +
			    (5 * RNDUP(sizeof (int)));
			/*
			 * For the 5 strings
			 * in the structure
			 */
			break;
		case SCHA_RS_GET_SWITCH:
			size += RNDUP(sizeof (rs_get_switch_args)) +
			    RNDUP(safe_strlen(((rs_get_switch_args *)
			    (arg->data))->rs_name)) +
			    RNDUP(safe_strlen(((rs_get_switch_args*)
			    (arg->data))->rg_name)) +
			    RNDUP(safe_strlen(((rs_get_switch_args *)
			    (arg->data))->node_name)) +
			    (3 * RNDUP(sizeof (int)));
			break;
		case SCHA_RS_GET_STATE:
		case SCHA_RS_GET_STATUS:
			size += RNDUP(sizeof (rs_get_state_status_args)) +
			    RNDUP(safe_strlen(((rs_get_state_status_args *)
			    (arg->data))->rs_name)) +
			    RNDUP(safe_strlen(((rs_get_state_status_args*)
			    (arg->data))->rg_name)) +
			    RNDUP(safe_strlen(((rs_get_state_status_args *)
			    (arg->data))->node_name)) +
			    RNDUP(safe_strlen(((rs_get_state_status_args *)
			    (arg->data))->zonename)) +
			    (4 * RNDUP(sizeof (int)));
			/*
			 * For the 4 strings
			 * in the structure
			 */
			break;
		case SCHA_RS_GET_FAILED_STATUS:
			size += RNDUP(sizeof (rs_get_fail_status_args)) +
			    RNDUP(safe_strlen(((rs_get_fail_status_args *)
			    (arg->data))->rs_name)) +
			    RNDUP(safe_strlen(((rs_get_fail_status_args *)
			    (arg->data))->rg_name)) +
			    RNDUP(safe_strlen(((rs_get_fail_status_args *)
			    (arg->data))->node_name)) +
			    RNDUP(safe_strlen(((rs_get_fail_status_args *)
			    (arg->data))->method_name)) +
			    RNDUP(safe_strlen(((rs_get_fail_status_args *)
			    (arg->data))->zonename)) +
			    (5 * RNDUP(sizeof (int)));
				/*
				 * For the 5 strings
				 * in the structure
				 */
			break;
		case SCHA_RS_SET_STATUS:
			size += RNDUP(sizeof (rs_set_status_args)) +
			    RNDUP(safe_strlen(((rs_set_status_args *)
			    (arg->data))->rs_name)) +
			    RNDUP(safe_strlen(((rs_set_status_args *)
			    (arg->data))->rg_name)) +
			    RNDUP(safe_strlen(((rs_set_status_args *)
			    (arg->data))->status_msg)) +
			    RNDUP(safe_strlen(((rs_set_status_args *)
			    (arg->data))->zonename)) +
			    (4 * RNDUP(sizeof (int)));
				/*
				 * For the 4 strings
				 * in the structure
				 */
			break;
		case SCHA_RT_OPEN:
			size += RNDUP(sizeof (rt_open_args)) +
			    RNDUP(safe_strlen(((rt_open_args *)
			    (arg->data))->rt_name)) +
			    RNDUP(sizeof (int));
			/*
			 * For the 1 string
			 * in the structure
			 */
			break;
		case SCHA_IS_PERNODE:
		case SCHA_RT_GET:
			size += RNDUP(sizeof (rt_get_args)) +
			    RNDUP(safe_strlen(((rt_get_args *)
			    (arg->data))->rt_name)) +
			    RNDUP(safe_strlen(((rt_get_args *)
			    (arg->data))->rt_prop_name)) +
			    (2 * RNDUP(sizeof (int)));
			/*
			 * For the 2 strings
			 * in the structure
			 */
			break;
		case SCHA_RG_OPEN:
			size += RNDUP(sizeof (rg_open_args)) +
			    RNDUP(safe_strlen(((rg_open_args *)
			    (arg->data))->rg_name)) +
			    RNDUP(sizeof (int));
			/*
			 * For the 1 string
			 * in the structure
			 */
			break;
		case SCHA_RG_GET:
			size += RNDUP(sizeof (rg_get_args)) +
			    RNDUP(safe_strlen(((rg_get_args *)
			    (arg->data))->rg_name)) +
			    RNDUP(safe_strlen(((rg_get_args *)
			    (arg->data))->rg_prop_name)) +
			    (2 * RNDUP(sizeof (int)));
			/*
			 * For the 2 strings
			 * in the structure
			 */
			break;
		case SCHA_RG_GET_STATE:
			size += RNDUP(sizeof (rg_get_state_args)) +
			    RNDUP(safe_strlen(((rg_get_state_args *)
			    (arg->data))->rg_name)) +
			    RNDUP(safe_strlen(((rg_get_state_args *)
			    (arg->data))->node_name)) +
			    RNDUP(safe_strlen(((rg_get_state_args *)
			    (arg->data))->zonename)) +
			    (3 * RNDUP(sizeof (int)));
							/*
							 * For the 3
							 * strings in
							 * structure
							 */
			break;
		case SCHA_CLUSTER_GET:
			size += RNDUP(sizeof (cluster_get_args)) +
			    RNDUP(safe_strlen(((cluster_get_args *)
			    (arg->data))->cluster_tag)) +
			    RNDUP(safe_strlen(((cluster_get_args *)
			    (arg->data))->node_name)) +
			    RNDUP(safe_strlen(((cluster_get_args *)
			    (arg->data))->zonename)) +
			    (3 * RNDUP(sizeof (int)));
			/*
			 * For the 3 strings
			 * in the structure
			 */
			break;
		case SCHA_SSM_IP:
			size += RNDUP(sizeof (ssm_ip_args)) +
			    RNDUP(safe_strlen(((ssm_ip_args *)
			    (arg->data))->rs_name)) +
			    RNDUP(safe_strlen(((ssm_ip_args *)
			    (arg->data))->rg_name)) +
			    (2 * RNDUP(sizeof (int)));
			/*
			 * For the 2 strings
			 * in the structure
			 */
			break;
		case SCHA_LB:
			size += RNDUP(sizeof (scha_ssm_lb_args)) +
			    ((((scha_ssm_lb_args *)(arg->data))->
			    op_data.op_data_len) * (RNDUP(sizeof (int *)))) +
			    RNDUP(safe_strlen(
			    ((scha_ssm_lb_args *)(arg->data))->rs_name)) +
			    (2 * RNDUP(sizeof (int)));
			/*
			 * 1 For the 1 string
			 * in the structure
			 * And the other one for the variable length
			 * int array
			 */
			break;
		case SCHA_CONTROL:
			/*
			 * For the 4 strings
			 * in the structure
			 */
			size += RNDUP(sizeof (scha_control_args)) +
			    RNDUP(safe_strlen(((scha_control_args *)
			    (arg->data))->rg_name)) +
			    RNDUP(safe_strlen(((scha_control_args *)
			    (arg->data))->r_name)) +
			    RNDUP(safe_strlen(((scha_control_args *)
			    (arg->data))->local_node)) +
			    RNDUP(safe_strlen(((scha_control_args *)
			    (arg->data))->gen_num)) +
			    (4 * RNDUP(sizeof (int)));

			/*
			 * For the 1 string array in the structure
			 */
			/* We check if the pointer is NULL we return. */
			if (((scha_control_args *)(arg->data))->r_names ==
			    NULL) {
				break;
			}
			/*
			 * If the pointer is not NULL. We traverse all the
			 * strings and get the length of each string.
			 */
			size += RNDUP(sizeof (((scha_control_args *)
			    (arg->data))->r_names->strarr_list_len));
			for (i = 0; i < ((scha_control_args *)(arg->data))->
			    r_names->strarr_list_len; i++) {
				size += RNDUP(safe_strlen(
				    ((scha_control_args *)arg->data)->r_names->
				    strarr_list_val[i]));
				size += RNDUP(sizeof (int));
				/* For the 1 string in the structure */
				}
			break;
		case SCHA_CLUSTER_OPEN:
			break;
		default:
			CL_PANIC(0);
			break;
	}
	return (size);
}
