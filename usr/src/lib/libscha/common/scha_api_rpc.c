/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scha_api_rpc.c	1.60	09/01/09 SMI"

/*
 *	RPC client interface to SC Receptionist RGM Daemon
 *	for public API functions
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <errno.h>
#include <tzfile.h>
#include "scha_lib.h"
#include "scha_ssm.h"
#include <sys/ssm.h>

#include <rgm/rgm_recep.h>
#if DOOR_IMPL
#include "rgm/door/rgm_recep_door.h"
#include <door.h>
#include <sys/sc_syslog_msg.h>
#endif
#include<sys/sol_version.h>
#if SOL_VERSION >= __s10
#include<zone.h>
#endif

static scha_str_array_t *process_result(get_result_t result);
static strarr *strlist_dup(const strarr *, uint_t);
static void scha_door_free_result_buffers(XDR *xdrs_result_p, char *return_buf,
    char *rbuf, size_t rsize);


/*
 * Note regarding memory management:
 *
 * A. In most of these functions, after we make an RPC call, we make a copy
 *    of the requested data then xdr_free() the original data returned by the
 *    RPC call.  xdr_free() is the safest way to free memory allocated by an
 *    RPC call; it ensures no memory leaks.
 *
 * B. We xdr_free() any partial results to avoid memory leaks in the case
 *    where an error code other than SCHA_ERR_NOERR was returned.
 *
 * C. The exceptions to A. above are these:
 *    the open functions rs_open(), rg_open(), rt_open();
 *    the scha_lb() and scha_ssm_*() functions.  We do not make a copy of the
 *    requested data in these functions because their result structures are
 *    less complex.
 */

/*
 * ************************************************************************
 *	rs_open - send a open request through RPC call to RGM receptionist
 *		to get the CCR sequence id
 *
 *	(i)	char *rs_name :  resource name
 *	(i)	char *rg_name :  resource group name
 *	(i/o)	char **rs_seq_id :   *rs_seq_id will contains the addr
 *				to the CCR sequence ID
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RSRC :		Resource name not found
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rt_name,
 *			rs_seq_id,
 *			rt_seq_id
 *
 * ************************************************************************
 */

scha_err_t
rs_open(const char *cluster,
    const char *rs_name, const char *rg_name, char **rt_name,
    char **rs_seq_id, char **rt_seq_id)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	scha_err_t		ret_code;
	rs_open_args 		open_args = {0};
	rs_open_result_t	result = {0};
	int 			err;

	err = scha_rpc_open(cluster, &clnt);
	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	open_args.rs_name = (char *)rs_name;
	open_args.rg_name = (char *)rg_name;
#if DOOR_IMPL
	err = scha_door_clnt(&open_args, &result, SCHA_RS_OPEN, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		/*
		 * "set_error()"  is not called when the error is
		 * INVALID_CREDENTIALS since this outputs the error
		 * message to stdout, which is undesirable.
		 */
		if (err != INVALID_CREDENTIALS) {
			set_error("scha_rs_open", err);
		}

		xdr_free((xdrproc_t)xdr_rs_open_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rt_seq_id = (char *)NULL;
		*rt_name = (char *)NULL;

		if (err == INVALID_CREDENTIALS) {
			return (SCHA_ERR_ACCESS);
		} else {
			return (SCHA_ERR_INTERNAL);
		}
	}
	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_rs_open_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rt_seq_id = (char *)NULL;
		*rt_name = (char *)NULL;
		return (ret_code);
	} else {
		*rs_seq_id = result.rs_seq_id;
		*rt_seq_id = result.rt_seq_id;
		*rt_name = result.rt_name;
	}
#else
	err = scha_rs_open_1(&open_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rs_open", err);
	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_rs_open_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rt_seq_id = (char *)NULL;
		*rt_name = (char *)NULL;
		if (err != RPC_SUCCESS) {
			return (SCHA_ERR_INTERNAL);
		}
		return (ret_code);
	} else {
		*rs_seq_id = result.rs_seq_id;
		*rt_seq_id = result.rt_seq_id;
		*rt_name = result.rt_name;
	}
#endif

	return (SCHA_ERR_NOERR);
}


/*
 * ***********************************************************************
 *	rs_get - send a request through RPC call to RGM receptionist to get
 *		the value of a resource property
 *
 *	(i)	char *rs_name :  resource name
 *	(i)	char *rg_name :  resource group name
 *	(i)	char *rs_prop_name :	property name
 *	(i)	char *zonename : zonename on the local node
 *			(used only for NUM_*_RESTART_ZONE query tags)
 *	(i/o)	char **rs_seq_id :   *rs_seq_id will contains the addr
 *				to the CCR sequence ID
 *	(i/o)	scha_str_array_t **rs_ret_buf: *rs_ret_buf will contain
 *			the address to the property value(s).
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RSRC :		Resource name not found
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rs_seq_id,
 *			rs_ret_buf
 *
 * ***********************************************************************
 */
scha_err_t
rs_get(const char *cluster,
    const char *rs_name, const char *rg_name, const char *rs_prop_name,
    const char *zonename, char **rs_seq_id, scha_str_array_t **rs_ret_buf,
    boolean_t qarg)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	scha_err_t		ret_code;
	rs_get_args		get_args = {0};
	get_result_t		result = {0, NULL, NULL};
	int 			err;

	err = scha_rpc_open(cluster, &clnt);
	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.rs_prop_name = (char *)rs_prop_name;
	get_args.rs_name = (char *)rs_name;
	get_args.rg_name = (char *)rg_name;
	get_args.qarg = qarg;

	get_args.zonename = (char *)zonename;

	/*
	value of a Resource property
	 */
#if DOOR_IMPL
	err = scha_door_clnt(&get_args, &result, SCHA_RS_GET, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_rs_get", err);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}


	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_ret_buf = (scha_str_array_t *)NULL;
		return (ret_code);
	} else {
		*rs_ret_buf = process_result(result);
		/*
		 * Note, if the RPC call succeeded, it guarantees that
		 * result.seq_id is non-null.  The following statement is just
		 * being a bit paranoid.
		 *
		 */
		*rs_seq_id = (result.seq_id ? strdup(result.seq_id) : NULL);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rs_ret_buf == NULL || *rs_seq_id == NULL) {
			strarr_free(*rs_ret_buf);
			*rs_ret_buf = NULL;
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#else
	err = scha_rs_get_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rs_get", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_ret_buf = (scha_str_array_t *)NULL;
		if (err != RPC_SUCCESS) {
			return (SCHA_ERR_INTERNAL);
		}
		return (ret_code);
	} else {
		*rs_ret_buf = process_result(result);
		/*
		 * Note, if the RPC call succeeded, it guarantees that
		 * result.seq_id is non-null.  The following statement is just
		 * being a bit paranoid.
		 */
		*rs_seq_id = (result.seq_id ? strdup(result.seq_id) : NULL);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rs_ret_buf == NULL || *rs_seq_id == NULL) {
			strarr_free(*rs_ret_buf);
			*rs_ret_buf = NULL;
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#endif
	return (SCHA_ERR_NOERR);
}

/*
 * ***********************************************************************
 *	rs_get_ext - send a request through RPC call to RGM receptionist to get
 *		the value of a resource extension property
 *
 *	(i)	char *rs_name :  resource name
 *	(i)	char *rg_name :  resource group name
 *	(i)	char *rs_prop_name :	property name
 *	(i/o)	char **rs_seq_id :   *rs_seq_id will contains the addr
 *				to the CCR sequence ID
 *	(i/o)	int *rs_prop_type : *extension property type
 *	(i/o)	scha_str_array_t **rs_ret_buf: *rs_ret_buf will contain
 *					the extension perperty
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RSRC :		Resource name not found
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rs_seq_id,
 *			rs_prop_type,
 *			rs_ret_buf
 *
 * ***********************************************************************
 */
scha_err_t
rs_get_ext(const char *cluster,
	const char *rs_name, const char *rg_name, const char *rs_prop_name,
	const char *rs_node_name, char **rs_seq_id,
	scha_prop_type_t *rs_prop_type,	scha_str_array_t **rs_ret_buf)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	char		host_name[MAXHOSTNAMELEN];
	scha_err_t		ret_code;
	rs_get_args		get_args = {0};
	get_ext_result_t	result = {0};
	int 			err;

#if SOL_VERSION >= __s10
	char zonename[ZONENAME_MAX];
	zoneid_t zoneid;
#else
	char zonename[1];	/* dummy placeholder */
#endif
	zonename[0] = '\0';

	/*
	 * Get local host name if the tag is SCHA_RESOURCE_STATE
	 */
	/*
	 * For retrieving R value on the local node,
	 * we will get the cluster nodename in receptionist code.
	 * Don't call 'gethostbyname' to get the local nodename,
	 * because it returns hostname instead of cluster nodename
	 */
	(void) strcpy(host_name, "");
#if SOL_VERSION >= __s10
	if ((zoneid = getzoneid()) < 0)
		return (SCHA_ERR_INTERNAL);
	if (zoneid != 0 && getzonenamebyid(zoneid,
	    zonename, (size_t)ZONENAME_MAX) < 0) {
		return (SCHA_ERR_INTERNAL);
	}
#endif

	err = scha_rpc_open(cluster, &clnt);


	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.rs_prop_name = (char *)rs_prop_name;
	get_args.rs_name = (char *)rs_name;
	get_args.rg_name = (char *)rg_name;
	get_args.rs_node_name = (char *)rs_node_name;
	get_args.zonename = (char *)zonename;

	/*
	 * value of an resource extension property
	 */
#if DOOR_IMPL
	err = scha_door_clnt(&get_args, &result, SCHA_RS_GET_EXT, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_rs_get_ext", err);
		xdr_free((xdrproc_t)xdr_get_ext_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_ext_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_prop_type = (scha_prop_type_t)NULL;
		*rs_ret_buf = (scha_str_array_t *)NULL;
		return (ret_code);
	} else {
		if ((*rs_ret_buf = (scha_str_array_t *)
		    calloc((size_t)1, sizeof (scha_str_array_t))) == NULL)
			return (SCHA_ERR_NOMEM);
		*rs_seq_id = strdup(result.seq_id);
		*rs_prop_type = (scha_prop_type_t)result.prop_type;

		/*
		 * if ext property has no data, set
		 * 	array_cnt to 0 and str_array to NULL
		 */
		(*rs_ret_buf)->is_ALL_value = B_FALSE;
		(*rs_ret_buf)->array_cnt = 0;
		(*rs_ret_buf)->str_array = NULL;
		if (result.value_list != NULL &&
		    result.value_list->strarr_list_len != 0) {
			(*rs_ret_buf)->array_cnt =
			    result.value_list->strarr_list_len;
			(*rs_ret_buf)->str_array = strlist_dup(
			    result.value_list->strarr_list_val,
			    result.value_list->strarr_list_len);
			if ((*rs_ret_buf)->str_array == NULL ||
			    *rs_seq_id == NULL) {
				strarr_free(*rs_ret_buf);
				*rs_ret_buf = NULL;
				free(*rs_seq_id);
				*rs_seq_id = NULL;
				xdr_free((xdrproc_t)xdr_get_ext_result_t,
				    (char *)&result);
				return (SCHA_ERR_NOMEM);
			}
		}
		xdr_free((xdrproc_t)xdr_get_ext_result_t, (char *)&result);
	}
#else
	err = scha_rs_get_ext_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rs_get_ext", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_ext_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_prop_type = (scha_prop_type_t)NULL;
		*rs_ret_buf = (scha_str_array_t *)NULL;
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	} else {
		if ((*rs_ret_buf = (scha_str_array_t *)
		    calloc((size_t)1, sizeof (scha_str_array_t))) == NULL)
			return (SCHA_ERR_NOMEM);
		*rs_seq_id = strdup(result.seq_id);
		*rs_prop_type = (scha_prop_type_t)result.prop_type;

		/*
		* if ext property has no data, set
		* array_cnt to 0 and str_array to NULL
		*/
		(*rs_ret_buf)->is_ALL_value = B_FALSE;
		(*rs_ret_buf)->array_cnt = 0;
		(*rs_ret_buf)->str_array = NULL;
		if (result.value_list != NULL &&
		    result.value_list->strarr_list_len != 0) {
			(*rs_ret_buf)->array_cnt =
			    result.value_list->strarr_list_len;
			(*rs_ret_buf)->str_array = strlist_dup(
			    result.value_list->strarr_list_val,
			    result.value_list->strarr_list_len);
			if ((*rs_ret_buf)->str_array == NULL ||
			    *rs_seq_id == NULL) {
				strarr_free(*rs_ret_buf);
				*rs_ret_buf = NULL;
				free(*rs_seq_id);
				*rs_seq_id = NULL;
				xdr_free((xdrproc_t)xdr_get_ext_result_t,
				    (char *)&result);
				return (SCHA_ERR_NOMEM);
			}
		}
		xdr_free((xdrproc_t)xdr_get_ext_result_t, (char *)&result);
	}
#endif
	return (SCHA_ERR_NOERR);
}
/*
 * ***********************************************************************
 *	rs_get_switch - send a request through RPC call to
 *			RGM receptionist to get the On_off/Monitored
 *			switch of a resource on a specified node
 *
 *	(i)	char *rs_name :  resource name
 *	(i)	char *rg_name :  resource group name
 *	(i)	char *node_name :	specified node name
 *	(i)	boolean_t on_off :	On_off (or) Monitored switch
 *	(i/o)	int  *rs_switch: *rs_switch will contain the addr of the
 *				  On_off/Monitored switch
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RSRC :		Resource name not found
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rs_seq_id
 *
 * ***********************************************************************
 */
scha_err_t
rs_get_switch(const char *cluster,
    const char *rs_name, const char *rg_name, char *node_name,
    char **rs_seq_id, boolean_t on_off, int *rs_switch)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	scha_err_t	ret_code;
	rs_get_switch_args	get_args = {0};
	get_switch_result_t	result = {0};
	int 		err;

#if SOL_VERSION >= __s10
	char zonename[ZONENAME_MAX];
	zoneid_t zoneid;
#else
	char zonename[1];	/* dummy placeholder */
#endif
	zonename[0] = '\0';

	/*
	 *	call clnt_create to get the RPC handle
	 */

#if SOL_VERSION >= __s10
	if ((zoneid = getzoneid()) < 0)
		return (SCHA_ERR_INTERNAL);
	if (zoneid != 0 && getzonenamebyid(zoneid,
	    zonename, (size_t)ZONENAME_MAX) < 0) {
		return (SCHA_ERR_INTERNAL);
	}
#endif
	err = scha_rpc_open(cluster, &clnt);
	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.rs_name = (char *)rs_name;
	get_args.rg_name = (char *)rg_name;
	get_args.node_name = (char *)node_name;
	get_args.zonename = (char *)zonename;
	get_args.on_off = on_off;

#if DOOR_IMPL
	/* Write xdr code to simulate the same as below */
	err = scha_door_clnt(&get_args, &result, SCHA_RS_GET_SWITCH, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_rs_get_switch", err);
		xdr_free((xdrproc_t)xdr_get_switch_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_switch_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_switch = (int)NULL;
		return (ret_code);
	} else {
		*rs_switch = result.rs_switch;
		*rs_seq_id = strdup(result.seq_id);
		if (*rs_seq_id == NULL) {
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			xdr_free((xdrproc_t)xdr_get_switch_result_t,
			    (char *)&result);
			return (SCHA_ERR_NOMEM);
		}
		xdr_free((xdrproc_t)xdr_get_switch_result_t, (char *)&result);
	}
#else
	err = scha_rs_get_switch_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rs_get_switch", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_switch_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_switch = (int)NULL;
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	} else {
		*rs_switch = result.rs_switch;
		*rs_seq_id = strdup(result.seq_id);
		if (*rs_seq_id == NULL) {
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			xdr_free((xdrproc_t)xdr_get_switch_result_t,
			    (char *)&result);
			return (SCHA_ERR_NOMEM);
		}
		xdr_free((xdrproc_t)xdr_get_switch_result_t, (char *)&result);
	}
#endif
	return (SCHA_ERR_NOERR);
}

/*
 * ***********************************************************************
 *	rs_get_status - send a request through RPC call to
 *				RGM receptionist to get the status
 *				of a resource on a specified node
 *
 *	(i)	char *rs_name :  resource name
 *	(i)	char *rg_name :  resource group name
 *	(i)	char *node_name :	specified node name
 *	(i/o)	int  *rs_status: *rs_status will contain the addr of the status
 *	(i/o)	char **status_msg: *status_msg will contain the status message
 *					of a RS status
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RSRC :		Resource name not found
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rs_seq_id,
 *			rs_status_msg
 *
 * ***********************************************************************
 */
scha_err_t
rs_get_status(const char *cluster,
    const char *rs_name, const char *rg_name, char *node_name,
    char *zonename, char **rs_seq_id, int *rs_status, char **rs_status_msg)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	scha_err_t	ret_code;
	rs_get_state_status_args	get_args = {0};
	get_status_result_t		result = {0};
	int 		err;


	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.rs_name = (char *)rs_name;
	get_args.rg_name = (char *)rg_name;
	get_args.node_name = (char *)node_name;
	get_args.zonename = (char *)zonename;

#if DOOR_IMPL
	err = scha_door_clnt(&get_args, &result, SCHA_RS_GET_STATUS, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_rs_get_status", err);
		xdr_free((xdrproc_t)xdr_get_status_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}


	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_status_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_status = (int)NULL;
		*rs_status_msg = (char *)NULL;
		return (ret_code);
	} else {
		*rs_status = result.rs_status;
		*rs_seq_id = strdup(result.seq_id);
		if (result.status_msg != NULL &&
		    result.status_msg[0] != '\0') {
			*rs_status_msg = strdup(result.status_msg);
			if (*rs_seq_id == NULL || *rs_status_msg == NULL) {
				free(*rs_seq_id);
				*rs_seq_id = NULL;
				free(*rs_status_msg);
				*rs_status_msg = NULL;
				xdr_free((xdrproc_t)xdr_get_status_result_t,
				    (char *)&result);
				return (SCHA_ERR_NOMEM);
			}
		} else
			*rs_status_msg = NULL;
		xdr_free((xdrproc_t)xdr_get_status_result_t, (char *)&result);
	}
#else
	err = scha_rs_get_status_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rs_get_status", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_status_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_status = (int)NULL;
		*rs_status_msg = (char *)NULL;
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	} else {
		*rs_status = result.rs_status;
		*rs_seq_id = strdup(result.seq_id);
		if (result.status_msg != NULL &&
		    result.status_msg[0] != '\0') {
			*rs_status_msg = strdup(result.status_msg);
			if (*rs_seq_id == NULL || *rs_status_msg == NULL) {
				free(*rs_seq_id);
				*rs_seq_id = NULL;
				free(*rs_status_msg);
				*rs_status_msg = NULL;
				xdr_free((xdrproc_t)xdr_get_status_result_t,
				    (char *)&result);
				return (SCHA_ERR_NOMEM);
			}
		} else
			*rs_status_msg = NULL;
		xdr_free((xdrproc_t)xdr_get_status_result_t, (char *)&result);
}
#endif
	return (SCHA_ERR_NOERR);
}

/*
 * ***********************************************************************
 *	rs_get_state - send a request through RPC call to
 *				RGM receptionist to get the state
 *				of a resource on a specified node
 *
 *	(i)	char *rs_name :  resource name
 *	(i)	char *rg_name :  resource group name
 *	(i)	char *node_name :	node name
 *	(i/o)	scha_str_array_t **rs_ret_buf: *rs_ret_buf will contain
 *			the address to the RS state.
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RSRC :		Resource name not found
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 *	Note:
 *		When call succeeds, caller is supposed to free:
 *			rs_seq_id,
 *			rs_state
 *
 * ***********************************************************************
 */
scha_err_t
rs_get_state(const char *cluster,
    const char *rs_name, const char *rg_name, char *node_name,
    char *zonename, char **rs_seq_id, scha_str_array_t **rs_state)
{
#if DOOR_IMPL
	client_handle			*clnt = NULL;
#else
	CLIENT				*clnt;
#endif
	scha_err_t			ret_code;
	rs_get_state_status_args	get_args = {0};
	get_result_t			result = {0, NULL, NULL};
	int 				err;


	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.rs_name = (char *)rs_name;
	get_args.rg_name = (char *)rg_name;
	get_args.node_name = (char *)node_name;
	get_args.zonename = (char *)zonename;

#if DOOR_IMPL
	err = scha_door_clnt(&get_args, &result, SCHA_RS_GET_STATE, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_rs_get_state", err);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}
	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_state = (scha_str_array_t *)NULL;
		return (ret_code);
	} else {
		*rs_state = process_result(result);
		*rs_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rs_state == NULL || *rs_seq_id == NULL) {
			strarr_free(*rs_state);
			*rs_state = NULL;
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#else
	err = scha_rs_get_state_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rs_get_state", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_state = (scha_str_array_t *)NULL;
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	} else {
		*rs_state = process_result(result);
		*rs_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rs_state == NULL || *rs_seq_id == NULL) {
			strarr_free(*rs_state);
			*rs_state = NULL;
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#endif
	return (SCHA_ERR_NOERR);
}


/*
 * ***********************************************************************
 *	is_pernode_prop - send a request through RPC call to
 *				RGM receptionist to get the per-node flag
 *				of the property specified from the RT
				paramtable entry.
 *
 *	(i)	char *rt_name :  resource type name
 *	(i)	char *prop_name: Property name
 *	(i/o)	scha_str_array_t **is_pernode *rs_ret_buf will contain
 *				the per-node flag.
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_PROP :		Extension property not found.
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *
 * ***********************************************************************
 */
scha_err_t
is_pernode_prop(const char *cluster, const char *rt_name, char *prop_name,
    char **rs_seq_id, scha_str_array_t **is_pernode)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	scha_err_t		ret_code;
	rt_get_args		get_args = {0};
	get_result_t		result = {0, NULL, NULL};
	int 			err;

	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.rt_name = (char *)rt_name;
	get_args.rt_prop_name = (char *)prop_name;

#if DOOR_IMPL
	err =  scha_door_clnt(&get_args, &result,
	    SCHA_IS_PERNODE, clnt);

	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("rs_get_is_pernode", err);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}
	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*is_pernode = (scha_str_array_t *)NULL;
		return (ret_code);
	} else {
		*is_pernode = process_result(result);
		*rs_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*is_pernode == NULL || *rs_seq_id == NULL) {
			strarr_free(*is_pernode);
			*is_pernode = NULL;
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#else
	err = scha_is_pernode_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "rs_get_is_pernode", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*is_pernode = (scha_str_array_t *)NULL;
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	} else {
		*is_pernode = process_result(result);
		*rs_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*is_pernode == NULL || *rs_seq_id == NULL) {
			strarr_free(*is_pernode);
			*is_pernode = NULL;
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#endif
	return (SCHA_ERR_NOERR);
}

/*
 * ***********************************************************************
 *	rs_get_failed_status - send a request through RPC call to
 *				RGM receptionist to get the failed status
 *				of the FINI, INIT, BOOT or UPDATE method on
 *				a specified node
 *
 *	(i)	char *rs_name :  resource name
 *	(i)	char *rg_name :  resource group name
 *	(i)	char *method_name :	method name
 *	(i)	char *node_name :	node name
 *	(i/o)	scha_str_array_t **rs_ret_buf: *rs_ret_buf will contain
 *			the address to the method failed status.
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RSRC :		Resource name not found
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rs_seq_id,
 *			rs_failed_status
 *
 * ***********************************************************************
 */
scha_err_t
rs_get_failed_status(const char *cluster,
    const char *rs_name, const char *rg_name,
    char *method_name, char *node_name, char **rs_seq_id,
    scha_str_array_t **rs_failed_status)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	scha_err_t		ret_code;
	rs_get_fail_status_args	get_args = {0};
	get_result_t		result = {0, NULL, NULL};
	int 			err;

	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.rs_name = (char *)rs_name;
	get_args.rg_name = (char *)rg_name;
	get_args.node_name = (char *)node_name;
	get_args.method_name = (char *)method_name;

#if DOOR_IMPL
	err =  scha_door_clnt(&get_args, &result,
	    SCHA_RS_GET_FAILED_STATUS, clnt);

	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_rs_get_failed_status", err);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}
	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_failed_status = (scha_str_array_t *)NULL;
		return (ret_code);
	} else {
		*rs_failed_status = process_result(result);
		*rs_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rs_failed_status == NULL || *rs_seq_id == NULL) {
			strarr_free(*rs_failed_status);
			*rs_failed_status = NULL;
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#else
	err = scha_rs_get_failed_status_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rs_get_failed_status", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*rs_failed_status = (scha_str_array_t *)NULL;
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	} else {
		*rs_failed_status = process_result(result);
		*rs_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rs_failed_status == NULL || *rs_seq_id == NULL) {
			strarr_free(*rs_failed_status);
			*rs_failed_status = NULL;
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#endif
	return (SCHA_ERR_NOERR);
}


/*
 * ************************************************************************
 *	rg_open - send a open request through RPC call to RGM receptionist
 *		to get the CCR sequence id
 *
 *	(i)	char *rg_name :  resource group name
 *	(i/o)	char **rg_seq_id :   *rg_seq_id will contains the addr
 *				to the CCR sequence ID
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RG :		Resource Group name not found
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rs_seq_id,
 *
 * ************************************************************************
 */
scha_err_t
rg_open(const char *cluster, const char *rg_name, char **rg_seq_id)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	scha_err_t		ret_code;
	rg_open_args 		open_args = {0};
	open_result_t		result = {0};
	int 			err;

	/*
	 * call clnt_create to get the RPC handle
	 */

	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	open_args.rg_name = (char *)rg_name;

#if DOOR_IMPL
	err = scha_door_clnt(&open_args, &result, SCHA_RG_OPEN, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		/*
		 * "set_error()"  is not called when the error is
		 * INVALID_CREDENTIALS since this outputs the error
		 * message to stdout, which is undesirable.
		 */
		if (err != INVALID_CREDENTIALS) {
			set_error("scha_rg_open", err);
		}

		xdr_free((xdrproc_t)xdr_open_result_t, (char *)&result);
		if (err == INVALID_CREDENTIALS) {
			return (SCHA_ERR_ACCESS);
		} else {
			return (SCHA_ERR_INTERNAL);
		}
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_open_result_t, (char *)&result);
		*rg_seq_id = (char *)NULL;
		return (ret_code);
	} else
		*rg_seq_id = result.seq_id;
#else
	err = scha_rg_open_1(&open_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rg_open", err);
	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_open_result_t, (char *)&result);
		*rg_seq_id = (char *)NULL;
		if (err != RPC_SUCCESS) {
			return (SCHA_ERR_INTERNAL);
		}
		return (ret_code);
	} else
		*rg_seq_id = result.seq_id;
#endif
	return (SCHA_ERR_NOERR);
}

/*
 * ********************************************************************
 *
 *	rg_get - send a request through RPC call to RGM receptionist to get
 *		the value of a resource group property
 *
 *	(i)	char *rg_name :  resource group name
 *	(i)	char *rg_prop_name :	property name
 *	(i/o)	char **rg_seq_id :   *rg_seq_id will contains the addr
 *				to the CCR sequence ID
 *	(i/o)	scha_str_array_t **rs_ret_buf: *rg_ret_buf will contain
			the address to the property value.
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RG :		Resource Group name not found
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rs_seq_id,
 *			rg_ret_buf
 *
 * ***********************************************************************
 */
scha_err_t
rg_get(const char *cluster,
    const char *rg_name, const char *rg_prop_name, char **rg_seq_id,
    scha_str_array_t **rg_ret_buf)
{
#if DOOR_IMPL
	client_handle	*clnt = NULL;
#else
	CLIENT		*clnt;
#endif
	scha_err_t	ret_code;
	rg_get_args	get_args = {0};
	get_result_t	result = {0, NULL, NULL};
	int 		err;


	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.rg_prop_name = (char *)rg_prop_name;
	get_args.rg_name = (char *)rg_name;

	/*
	 * value of a Resource group property
	 */
#if DOOR_IMPL
	err = scha_door_clnt(&get_args, &result, SCHA_RG_GET, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_rg_get", err);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rg_seq_id = (char *)NULL;
		*rg_ret_buf = (scha_str_array_t *)NULL;
		return (ret_code);
	} else {
		*rg_ret_buf = process_result(result);
		*rg_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rg_ret_buf == NULL || *rg_seq_id == NULL) {
			strarr_free(*rg_ret_buf);
			*rg_ret_buf = NULL;
			free(*rg_seq_id);
			*rg_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#else
	err = scha_rg_get_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rg_get", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rg_seq_id = (char *)NULL;
		*rg_ret_buf = (scha_str_array_t *)NULL;
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	} else {
		*rg_ret_buf = process_result(result);
		*rg_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rg_ret_buf == NULL || *rg_seq_id == NULL) {
			strarr_free(*rg_ret_buf);
			*rg_ret_buf = NULL;
			free(*rg_seq_id);
			*rg_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#endif
	return (SCHA_ERR_NOERR);
}


/*
 * ********************************************************************
 *
 *	rg_get_state - send a request through RPC call to RGM receptionist
 *			to get the state of a Resource Group
 *
 *	(i)	char *rg_name :  resource group name
 *	(i/o)	char **rg_state : *rg_state will contain the addr
 *					to the Resource Group state.
 *	(i/o)	scha_str_array_t **rg_state: *rg_state will contain the addr
 *					to the rg state.
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RG :		Resource Group name not found
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rs_seq_id,
 *			rg_state
 *
 * ***********************************************************************
 */
scha_err_t
rg_get_state(const char *cluster, const char *rg_name, char *node_name,
    char *zonename, char **rg_seq_id,
    scha_str_array_t **rg_state)
{
#if DOOR_IMPL
	client_handle	*clnt = NULL;
#else
	CLIENT		*clnt;
#endif
	scha_err_t	ret_code;
	rg_get_state_args	get_args = {0};
	get_result_t	result = {0, NULL, NULL};
	int 		err;

	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.rg_name = (char *)rg_name;
	get_args.node_name = (char *)node_name;
	get_args.zonename = (char *)zonename;

	/*
	 * value of a Resource group property
	 */
#if DOOR_IMPL
	err = scha_door_clnt(&get_args, &result, SCHA_RG_GET_STATE, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_rg_get_state", err);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rg_state = (scha_str_array_t *)NULL;
		return (ret_code);
	} else {
		*rg_state = process_result(result);
		*rg_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rg_state == NULL || *rg_seq_id == NULL) {
			strarr_free(*rg_state);
			*rg_state = NULL;
			free(*rg_seq_id);
			*rg_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#else
	err = scha_rg_get_state_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rg_get_state", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rg_state = (scha_str_array_t *)NULL;
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	} else {
		*rg_state = process_result(result);
		*rg_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rg_state == NULL || *rg_seq_id == NULL) {
			strarr_free(*rg_state);
			*rg_state = NULL;
			free(*rg_seq_id);
			*rg_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#endif
	return (SCHA_ERR_NOERR);
}
/*
 * ************************************************************************
 *	rt_open - send a open request through RPC call to RGM receptionist
 *		to get the RT CCR sequence id
 *
 *	(i)	char *rt_name :  resource type name
 *	(i/o)	char **rt_seq_id :   *rt_seq_id will contains the addr
 *				to the RT CCR sequence ID
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RT :		Resource Type name not found
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rt_seq_id,
 *
 * ************************************************************************
 */
scha_err_t
rt_open(const char *cluster, const char *rt_name, char **rt_seq_id)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	scha_err_t		ret_code;
	rt_open_args 		open_args = {0};
	open_result_t		result = {0};
	int 			err;

	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	open_args.rt_name = (char *)rt_name;
#if DOOR_IMPL
	err = scha_door_clnt(&open_args, &result, SCHA_RT_OPEN, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_rt_open", err);
		xdr_free((xdrproc_t)xdr_open_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_open_result_t, (char *)&result);
		*rt_seq_id = (char *)NULL;
		return (ret_code);
	} else
		*rt_seq_id = result.seq_id;
#else
	err = scha_rt_open_1(&open_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rt_open", err);
	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_open_result_t, (char *)&result);
		*rt_seq_id = (char *)NULL;
		if (err != RPC_SUCCESS) {
			return (SCHA_ERR_INTERNAL);
		}
		return (ret_code);
	} else
		*rt_seq_id = result.seq_id;
#endif
	return (SCHA_ERR_NOERR);
}


/*
 * ***********************************************************************
 *	rt_get - send a request through RPC call to RGM receptionist to get
 *		the value of a resource type property
 *
 *	(i)	char *rt_name :  resource type name
 *	(i)	char *rt_prop_name :	property name
 *	(i/o)	char **rt_seq_id :   *rt_seq_id will contains the addr
 *				to the RT CCR sequence ID
 *	(i/o)	scha_str_array_t **rt_ret_buf : *rt_ret_buf will contain
 *				the addr to the property value.
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RT :		Resource Type name not found
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rt_seq_id,
 *			rt_ret_buf
 *
 * ***********************************************************************
 */
scha_err_t
rt_get(const char *cluster,
    const char *rt_name, const char *rt_prop_name, char **rt_seq_id,
    scha_str_array_t **rt_ret_buf)
{
#if DOOR_IMPL
	client_handle	*clnt = NULL;
#else
	CLIENT		*clnt;
#endif
	scha_err_t	ret_code;
	rt_get_args	get_args = {0};
	get_result_t	result = {0, NULL, NULL};
	int 		err;

	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.rt_prop_name = (char *)rt_prop_name;
	get_args.rt_name = (char *)rt_name;

	/*
	 * get the value of a Resource type property
	 */
#if DOOR_IMPL
	err = scha_door_clnt(&get_args, &result, SCHA_RT_GET, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_rt_get", err);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rt_seq_id = (char *)NULL;
		*rt_ret_buf = (scha_str_array_t *)NULL;
		return (ret_code);
	} else {
		*rt_ret_buf = process_result(result);
		*rt_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rt_ret_buf == NULL || *rt_seq_id == NULL) {
			strarr_free(*rt_ret_buf);
			*rt_ret_buf = NULL;
			free(*rt_seq_id);
			*rt_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#else
	err = scha_rt_get_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rt_get", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rt_seq_id = (char *)NULL;
		*rt_ret_buf = (scha_str_array_t *)NULL;
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	} else {
		*rt_ret_buf = process_result(result);
		*rt_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*rt_ret_buf == NULL || *rt_seq_id == NULL) {
			strarr_free(*rt_ret_buf);
			*rt_ret_buf = NULL;
			free(*rt_seq_id);
			*rt_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#endif
	return (SCHA_ERR_NOERR);
}

/*
 * ***********************************************************************
 *	rs_get_all_extprops - send a request through RPC call to
 *				RGM receptionist to get all extension
 *				property names of a resource
 *
 *	(i)	char *rs_name :  resource name
 *	(i)	char *rg_name :  resource group name
 *	(i/o)	scha_str_array_t **prop_list : *all property names
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_RS :		Resource name not found
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *		SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			rs_seq_id,
 *			prop_list
 *
 * ***********************************************************************
 */
scha_err_t
rs_get_all_extprops(const char *cluster,
    const char *rs_name, const char *rg_name, char **rs_seq_id,
    scha_str_array_t **prop_list)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	scha_err_t		ret_code;
	rs_get_args		get_args = {0};
	get_result_t		result = {0, NULL, NULL};
	int 			err;

	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.rs_name = (char *)rs_name;
	get_args.rg_name = (char *)rg_name;

	/*
	value of a Resource property
	 */
#if DOOR_IMPL
	err = scha_door_clnt(&get_args, &result, SCHA_GET_ALL_EXTPROPS, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_get_all_extprops", err);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}


	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*prop_list = (scha_str_array_t *)NULL;
		return (ret_code);
	} else {
		*prop_list = process_result(result);
		*rs_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*prop_list == NULL || *rs_seq_id == NULL) {
			strarr_free(*prop_list);
			*prop_list = NULL;
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#else
	err = scha_get_all_extprops_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_get_all_extprops", err);
	security_clnt_freebinding(clnt);

	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*rs_seq_id = (char *)NULL;
		*prop_list = (scha_str_array_t *)NULL;
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	} else {
		*prop_list = process_result(result);
		*rs_seq_id = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*prop_list == NULL || *rs_seq_id == NULL) {
			strarr_free(*prop_list);
			*prop_list = NULL;
			free(*rs_seq_id);
			*rs_seq_id = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#endif
	return (SCHA_ERR_NOERR);
}

/*
 * ************************************************************************
 *	cluster_open - send a open request through RPC call to RGM receptionist
 *		to get the incarnation number of cluster
 *
 *	(o)	char **cluster_incr_num :   *cluster incarnation number
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			cluster_incr_num
 *
 * ************************************************************************
 */
scha_err_t
cluster_open(const char *cluster, char **cluster_incr_num)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	open_result_t	result = {0, NULL};
	scha_err_t		ret_code;
	int 			err;

	/*
	 * call clnt_create to get the RPC handle
	 */

	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

#if DOOR_IMPL
	err = scha_door_clnt(NULL, &result, SCHA_CLUSTER_OPEN, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_cluster_open", err);
		xdr_free((xdrproc_t)xdr_open_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_open_result_t, (char *)&result);
		*cluster_incr_num = (char *)NULL;
		return (ret_code);
	} else {
		*cluster_incr_num = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_open_result_t, (char *)&result);
		if (*cluster_incr_num == NULL) {
			return (SCHA_ERR_NOMEM);
		}
	}
#else
	err = scha_cluster_open_1(NULL, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_cluster_open", err);
	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_open_result_t, (char *)&result);
		*cluster_incr_num = (char *)NULL;
		if (err != RPC_SUCCESS) {
			return (SCHA_ERR_INTERNAL);
		}
		return (ret_code);
	} else {
		*cluster_incr_num = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_open_result_t, (char *)&result);
		if (*cluster_incr_num == NULL)
			return (SCHA_ERR_NOMEM);
	}
#endif
	return (SCHA_ERR_NOERR);
}

/*
 * ***********************************************************************
 *	cluster_get - send a request through RPC call to RGM receptionist
 *		to get the cluster configuration information
 *
 *	(i) 	cluster_tag : cluster tag
 *	(i)	node_name : node name
 *	(i)	node_id : node id
 *	(i/o)	char **ret_buf : *ret_buf will contain the addr
 *					to the cluster info of specified tag.
 *	(o)	char **cluster_incr_num :   *cluster incarnation number
 *
 *	Return errors:
 *
 *		SCHA_ERR_NOERR :	no error
 *		SCHA_ERR_AUTH :		RPC authentication failed
 *		SCHA_ERR_NOMEM :	Unable to allocate memory
 *		SCHA_ERR_INTERNAL :	RPC call failed
 *
 *	Note:
 *		When call succeeds caller is supposed to free:
 *			ret_buf,
 *			cluster_incr_num
 *
 * ***********************************************************************
 */
scha_err_t
cluster_get(const char *cluster, const char *cluster_tag, char *node_name,
    uint_t node_id,
    const char *zonename, scha_str_array_t **ret_buf, char **cluster_incr_num)
{
#if DOOR_IMPL
	client_handle	*clnt = NULL;
#else
	CLIENT		*clnt;
#endif
	scha_err_t	ret_code;
	cluster_get_args	get_args = {0};
	get_result_t		result = {0, NULL, NULL};
	int 		err;

	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	get_args.cluster_tag = (char *)cluster_tag;

	/*
	 * Caller provides either a node_name string or a node_id uint
	 * for those tags requiring that information.
	 */
	if (node_name != NULL)
		get_args.node_name = node_name;
	else {
		/* this code assumes nodeids start at 1 */
		if (node_id != 0)
			get_args.node_id = node_id;
	}
	get_args.zonename = (char *)zonename;

	/*
	 * send a RPC request to RGMd receptionist
	 */
#if DOOR_IMPL
	err = scha_door_clnt(&get_args, &result, SCHA_CLUSTER_GET, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_cluster_get", err);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}


	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*ret_buf = (scha_str_array_t *)NULL;
		*cluster_incr_num = (char *)NULL;
		return (ret_code);
	} else {
		*ret_buf = process_result(result);
		*cluster_incr_num = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*ret_buf == NULL || *cluster_incr_num == NULL) {
			strarr_free(*ret_buf);
			*ret_buf = NULL;
			free(*cluster_incr_num);
			*cluster_incr_num = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#else
	err = scha_cluster_get_1(&get_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_cluster_get", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		*ret_buf = (scha_str_array_t *)NULL;
		*cluster_incr_num = (char *)NULL;
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	} else {
		*ret_buf = process_result(result);
		*cluster_incr_num = strdup(result.seq_id);
		xdr_free((xdrproc_t)xdr_get_result_t, (char *)&result);
		if (*ret_buf == NULL || *cluster_incr_num == NULL) {
			strarr_free(*ret_buf);
			*ret_buf = NULL;
			free(*cluster_incr_num);
			*cluster_incr_num = NULL;
			return (SCHA_ERR_NOMEM);
		}
	}
#endif
	return (SCHA_ERR_NOERR);
}

/*
 * ***********************************************************************
 *	load balancer
 *	In: sg_name, rg_name, op_code, op_data, data_len
 *	Out: ret_op_data, ret_data_len  (if ret_op_data != NULL).
 *	If ret_op_data passed in and no error return, ret_op_data must be
 *	freed when done.
 *	Return an errno error code
 *
 * ***********************************************************************
 */
int
scha_lb(const char *cluster, const char *rs_name, scha_ssm_lb_op_t op_code,
    int *op_data, int data_len, int **ret_op_data, int *ret_data_len)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	scha_ssm_lb_args	scha_ssm_lb_argmnts = {0};
	scha_ssm_lb_result_t	result = {0};
	int 		err;

	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (EIO);

	scha_ssm_lb_argmnts.rs_name = (char *)rs_name;
	scha_ssm_lb_argmnts.op_code = op_code;
	if (op_data == NULL) {
		scha_ssm_lb_argmnts.op_data.op_data_len = 0;
		scha_ssm_lb_argmnts.op_data.op_data_val = NULL;
	} else {
		scha_ssm_lb_argmnts.op_data.op_data_len = (uint_t)data_len;
		scha_ssm_lb_argmnts.op_data.op_data_val = op_data;
	}

	/*
	 * send a RPC request to RGMd receptionist
	 */
#if DOOR_IMPL
	err = scha_door_clnt(&scha_ssm_lb_argmnts, &result, SCHA_LB, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_lb", err);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		return (result.ret_code);
	}

	if (ret_op_data) {
		*ret_op_data = result.ret_data.ret_data_val;
		*ret_data_len = (int)result.ret_data.ret_data_len;
	} else {
		free(result.ret_data.ret_data_val);
	}
#else
	err = scha_lb_1(&scha_ssm_lb_argmnts, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_lb", err);

	clnt_destroy(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		if (err != RPC_SUCCESS)
			return (EIO);
		return (result.ret_code);
	}

	if (ret_op_data) {
		*ret_op_data = result.ret_data.ret_data_val;
		*ret_data_len = (int)result.ret_data.ret_data_len;
	} else {
		free(result.ret_data.ret_data_val);
	}
#endif
	return (0);
}


/*
 * ***********************************************************************
 *	ssm interface: IP initialize operation
 *
 * ***********************************************************************
 */
scha_ssm_exceptions
scha_ssm_address_ip(
    const char *rs_name, const char *rg_name, int protocol,
    int nodeid)
{
	return (scha_ssm_address_ip_zone(
	    NULL, rs_name, rg_name, protocol, nodeid));
}
scha_ssm_exceptions
scha_ssm_address_ip_zone(const char *cluster,
    const char *rs_name, const char *rg_name, int protocol,
    int nodeid)
{
#if DOOR_IMPL
	client_handle		*clnt = NULL;
#else
	CLIENT			*clnt;
#endif
	ssm_ip_args	ssm_ip_argmnts = {0};
	ssm_result_t	result = {0};
	int		err;

	err = scha_rpc_open(cluster, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_SSM_COMM_ERROR);

	/* Fill in the args structure */

	ssm_ip_argmnts.rs_name = (char *)rs_name;
	ssm_ip_argmnts.rg_name = (char *)rg_name;
	ssm_ip_argmnts.protocol = protocol;
	ssm_ip_argmnts.nodeid = nodeid;

	/*
	 * send a RPC request to RGMd receptionist
	 */
#if DOOR_IMPL
	err = scha_door_clnt(&ssm_ip_argmnts, &result, SCHA_SSM_IP, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_ssm_ip", err);
		return (SCHA_SSM_COMM_ERROR);
	}
#else
	err = scha_ssm_ip_1(&ssm_ip_argmnts, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_ssm_ip", err);

	clnt_destroy(clnt);

	if (err != RPC_SUCCESS) {
		return (SCHA_SSM_COMM_ERROR);
	}
#endif

	return (result.ret_code);
}


/*
 * ***********************************************************************
 *	rs_set_status - send a request through RPC call to
 *				RGM receptionist to set the status
 *				of a resource
 *
 *	(i)	char *rs_name :  resource name
 *	(i)	char *rg_name :  resource group name
 *	(i)	scha_rsstatus status: the new value of the RS's status
 *	(i)	char *status_msg: the new value of the status message
 *
 * Return errors:
 *
 *	SCHA_ERR_NOERR :	no error
 *	SCHA_ERR_RSRC :		Resource name not found
 *	SCHA_ERR_RG :		Resource group name not found
 *	SCHA_ERR_INTERNAL :	RPC call failed
 *	SCHA_ERR_AUTH :		RPC authentication failed
 *	SCHA_ERR_NOMEM :	Unable to allocate memory
 *	SCHA_ERR_CCR :		Failed to retrieve information from CCR
 *
 * ***********************************************************************
 */
scha_err_t
rs_set_status(const char *rs_name, const char *rg_name,
    const char *zonename, scha_rsstatus_t status, const char *status_msg)
{
#if DOOR_IMPL
	client_handle	*clnt = NULL;
#else
	CLIENT		*clnt;
#endif
	scha_err_t	ret_code;
	rs_set_status_args	set_args = {0};
	set_status_result_t	result = {0};
	int			err;

	err = scha_rpc_open(zonename, &clnt);

	if (err != SCHA_ERR_NOERR)
		return (SCHA_ERR_INTERNAL);

	set_args.rs_name = (char *)rs_name;
	set_args.rg_name = (char *)rg_name;
	set_args.zonename = (char *)zonename;
	set_args.status = (int)status;
	set_args.status_msg = (char *)status_msg;
#if DOOR_IMPL
	err = scha_door_clnt(&set_args, &result, SCHA_RS_SET_STATUS, clnt);
	security_clnt_freebinding(clnt);
	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_rs_set_status", err);
		xdr_free((xdrproc_t)xdr_set_status_result_t, (char *)&result);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_set_status_result_t, (char *)&result);
		return (ret_code);
	}
#else
	err = scha_rs_set_status_1(&set_args, &result, clnt);
	if (err != RPC_SUCCESS)
		set_error(clnt, "scha_rs_set_status", err);

	security_clnt_freebinding(clnt);
	if ((err != RPC_SUCCESS) || (result.ret_code != SCHA_ERR_NOERR)) {
		ret_code = map_scha_err(result.ret_code);
		xdr_free((xdrproc_t)xdr_set_status_result_t, (char *)&result);
		if (err != RPC_SUCCESS)
			return (SCHA_ERR_INTERNAL);
		return (ret_code);
	}
#endif
	return (SCHA_ERR_NOERR);
}

/*
 * ***********************************************************************
 *	process_result - copy the result returned from RGM via RPC call
 *			 into 'scha_str_array_t' struct
 * ***********************************************************************
 */
scha_str_array_t *
process_result(get_result_t result)
{
	scha_str_array_t	*buf = NULL;

	if ((buf = (scha_str_array_t *)
	    calloc((size_t)1, sizeof (scha_str_array_t))) == NULL)
		return (NULL);
	buf->is_ALL_value = B_FALSE;
	buf->array_cnt = 0;
	buf->str_array = NULL;
	if (result.value_list != NULL && result.value_list->strarr_list_len) {
		if (result.value_list->strarr_list_len == 1 &&
		    strncasecmp(result.value_list->strarr_list_val[0],
		    SCHA_ALL_SPECIAL_VALUE, (size_t)1) == 0) {
			buf->is_ALL_value = B_TRUE;
		} else {
			buf->array_cnt = result.value_list->strarr_list_len;
			buf->str_array = strlist_dup(
			    result.value_list->strarr_list_val,
			    result.value_list->strarr_list_len);
			if (buf->str_array == NULL) {
				free(buf);
				return (NULL);
			}
		}
	}
	return (buf);
}


/*
 * Do copy of char ** list. If out of memory, return NULL.
 */
strarr *
strlist_dup(const strarr *list, uint_t n)
{
	uint_t	i;
	strarr	*mem = NULL;

	mem = (strarr *)calloc((size_t)(n + 1), sizeof (char *));

	/* if out of memory, return NULL. */
	if (mem == NULL)
		return (NULL);

	for (i = 0; i < n; i++) {
		if (list[i] != NULL) {
			mem[i] = strdup(list[i]);
			if (mem[i] == NULL) {
				/*
				 * out of memory,
				 * clean up and return NULL.
				 */
				strlist_free(mem);
				return (NULL);
			}
		}
	}
	return (mem);
}

#if DOOR_IMPL
int scha_door_clnt(void *args,
    void *result, int scha_api_name,
    client_handle *clnt)
{
	door_arg_t door_arg;

	/* XDR Streams for arguments and result */
	XDR xdrs, xdrs_result;

	/*
	 * Stores the marshalled arguments to be sent via
	 * door_call
	 */
	char *arg_buf = NULL;

	/* Buffer to hold the return value from door_call */
	char *return_buf = NULL;

	/*
	 * Container struct for storing the input arguments
	 * that are to be sent to the server via the door_call
	 */
	scha_input_args_t *scha_args = NULL;

	/* Stores the total size of the arguments being passed */
	size_t arg_size;

	/* Return code sent by the door server */
	int return_code;

	/* Number of times we want to try to encode the arguments */
	int num_retries = XDR_NUM_RETRIES;

	sc_syslog_msg_handle_t handle;

	/* Check for a valid client_handle before proceeding */
	if (clnt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_SCHA_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Invalid Client Handle");
		sc_syslog_msg_done(&handle);
		return (DOOR_INVALID_HANDLE);
	}

	/*
	 * Allocate memory for storing the arguments to be passed
	 */
	scha_args = (scha_input_args_t *)calloc((size_t)1,
	    sizeof (scha_input_args_t));
	if (scha_args == NULL) {
		return (UNABLE_TO_ALLOCATE_MEM);
	}

	/* Prepare the container structure */
	scha_args->api_name = scha_api_name;
	scha_args->data = args;

	/* Calculate the size of the arguments being passed */
	arg_size = scha_xdr_sizeof(scha_args);

	for (num_retries = XDR_NUM_RETRIES; num_retries > 0; num_retries--) {
		/*
		 * Allocate the space to store marshalled data
		 */
		arg_buf = (char *)calloc((size_t)1, arg_size);
		if (arg_buf == NULL) {
			free(scha_args);
			return (UNABLE_TO_ALLOCATE_MEM);
		}

		/* Initialize the XDR Stream for Encoding data */
		xdrmem_create(&xdrs, arg_buf, (uint_t)arg_size, XDR_ENCODE);

		/* Use xdr function to marshall the input arguments */
		if (!xdr_scha_input_args(&xdrs, scha_args)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_SCHA_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * A non-fatal error occurred in a libscha.so function
			 * while marshalling arguments for a remote procedure
			 * call. The operation will be re-tried with a larger
			 * buffer.
			 * @user_action
			 * No user action is required. If the message recurs
			 * frequently, contact your authorized Sun service
			 * provider to determine whether a workaround or patch
			 * is available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "libscha XDR Buffer Shortfall while encoding "
			    "arguments API num : %d, will retry",
			    scha_api_name);
			sc_syslog_msg_done(&handle);
			/*
			 * Will retry to encode the arguments
			 * this time with a larger buffer
			 */
			if (num_retries) {
				arg_size += EXTRA_BUFFER_SIZE;
				free(arg_buf);
				arg_buf = NULL;
				xdr_destroy(&xdrs);
				continue;
			}
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_SCHA_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "XDR Error while encoding arguments.");
			sc_syslog_msg_done(&handle);
			xdr_destroy(&xdrs);
			free(arg_buf);
			free(scha_args);
			return (XDR_ERROR_AT_CLIENT);
		} else {
			break; /* Successful Encoding */
		}
	}
	/* Prepare the door arguments before making the door call */
	door_arg.data_ptr = arg_buf;
	door_arg.data_size = xdr_getpos(&xdrs); /* arg_size; */
	door_arg.desc_ptr = NULL;
	door_arg.desc_num = 0;

	/*
	 * Allocate a buffer for door_return()
	 * If this buffer isn't large enough to hold the return value,
	 * door_return will attempt to allocate a larger buffer in our
	 * address space using mmap, and will set rbuf and rsize
	 * accordingly.  Even if we fail to allocate this buffer in the
	 * heap, we can attempt the door call.
	 */
#define	RETBUFSIZE	8192
	return_buf = (char *)calloc((size_t)1, (size_t)RETBUFSIZE);
	door_arg.rbuf = return_buf;
	door_arg.rsize = (return_buf == NULL) ? 0 : (size_t)RETBUFSIZE;

	/* Make the door call */
	if (make_door_call(clnt->door_desc, &door_arg) < 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_SCHA_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to make door call.");
		sc_syslog_msg_done(&handle);
		xdr_destroy(&xdrs);
		free(arg_buf);
		free(scha_args);
		free(return_buf);
		return (UNABLE_TO_MAKE_DOOR_CALL);
	}

	/*
	 * Now, we are back from the door_call and we need to unmarshall the
	 * data and get the return_code.
	 * But if the returned size is 0 and the rbuf = NULL, then we can be
	 * sure that a severe error has occured at the server and it has not
	 * been able to complete the call successfully.
	 */
	free(arg_buf); /* We can free this, as the door_call is completed */
	free(scha_args);
	xdr_destroy(&xdrs);

	/* Check if the returned result and rsize are NULL */
	if ((door_arg.rbuf == NULL) && (door_arg.rsize == 0)) {
		scha_door_free_result_buffers(NULL, return_buf, door_arg.rbuf,
		    door_arg.rsize);
		return (XDR_ERROR_AT_SERVER);
	}

	/* Initialize the result XDR stream */
	xdrmem_create(&xdrs_result, door_arg.rbuf, (uint_t)door_arg.rsize,
	    XDR_DECODE);

	/* Decode the return_code */
	if (!xdr_int(&xdrs_result, &return_code)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_SCHA_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "XDR Error while decoding return arguments.");
		sc_syslog_msg_done(&handle);
		scha_door_free_result_buffers(&xdrs_result, return_buf,
		    door_arg.rbuf, door_arg.rsize);
		return (XDR_ERROR_AT_CLIENT);
	} else {
		if (return_code != DOOR_CALL_SUCCESS) {
			/*
			 * Only if the return_code is DOOR_CALL_SUCCESS
			 * we proceed
			 */
			scha_door_free_result_buffers(&xdrs_result, return_buf,
			    door_arg.rbuf, door_arg.rsize);
			return (return_code);
		}
	}

	/* Now decode the other result data */
	if (!xdr_scha_result_args(&xdrs_result, result, scha_api_name)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_SCHA_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "XDR Error while decoding return arguments.");
		sc_syslog_msg_done(&handle);
		scha_door_free_result_buffers(&xdrs_result, return_buf,
		    door_arg.rbuf, door_arg.rsize);
		return (XDR_ERROR_AT_CLIENT);
	}
	scha_door_free_result_buffers(&xdrs_result, return_buf, door_arg.rbuf,
	    door_arg.rsize);
	return (DOOR_CALL_SUCCESS);
}

/*
 * Free result buffers before returning from scha_door_clnt()
 */
void
scha_door_free_result_buffers(XDR *xdrs_result_p, char *return_buf,
    char *rbuf, size_t rsize)
{
	if (xdrs_result_p != NULL) {
		xdr_destroy(xdrs_result_p);
	}
	/*
	 * If door_return allocated a new return buffer, we need to unmap it.
	 * See door_call(3DOOR) for details.
	 */
	if (rbuf != return_buf) {
		(void) munmap(rbuf, rsize);
	}
	free(return_buf);
}
#endif
