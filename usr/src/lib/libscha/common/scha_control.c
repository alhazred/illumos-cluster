/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scha_control.c	1.37	09/01/09 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <scha.h>
#include <scha_err.h>
#include <unistd.h>
#include "scha_lib.h"

#include <rgm/rgm_recep.h>

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif


/*
 * helper function to convert const char array to strarr_list
 */
static void
array_to_strarray(const char **listp, strarr_list **str_arr)
{
	uint_t		i = 0;
	const char	**p;

	*str_arr = (strarr_list *) malloc(sizeof (strarr_list));
	/* Treat NULL listp as an empty list */
	if (listp == NULL) {
		(*str_arr)->strarr_list_len = 0;
		return;
	}

	/* Compute the length of the sequence */
	p = listp;
	while (*p) {
		i++;
		p++;
	}

	/* set the length of the sequence */
	(*str_arr)->strarr_list_len = i;
	(*str_arr)->strarr_list_val = (char **)malloc(i * sizeof (char *));
	/* Fill in the sequence. */
	i = 0;
	p = listp;
	while (*p) {
		(*str_arr)->strarr_list_val[i] = strdup(*p);
		i++;
		p++;
	}
} /*lint !e818*/

static void
free_strarr_list(strarr_list *sa)
{
	uint_t	i;

	if (sa == NULL) {
		return;
	}
	for (i = 0; i < sa->strarr_list_len; i++) {
		free(sa->strarr_list_val[i]);
	}
	free(sa);
}


/*
 * tag, rgname, rname, and zonename. The zonename should be NULL on
 * pre s10 version on s10, if zonename is NULL, scha_control is run
 * on the zone from where the command is executed; otherwise it is
 * run on the zonename passed
 */
scha_err_t
scha_control_zone(const char *optag, const char *rg_name,
    const char *r_name, const char *zonename)
{
	int	err;

#if SOL_VERSION >= __s10
	char	local_zone[ZONENAME_MAX + 1 ];
	zoneid_t zoneid;
#else
	char	local_zone[1]; 	/* not used */
#endif

#if DOOR_IMPL
	client_handle	*clnt = NULL;
#else
	CLIENT		*clnt;
#endif
	scha_control_args	scha_args = {GIVEOVER, NULL, NULL, NULL, NULL};
	scha_result_t		result = {0};

	/*
	 * assume the args has already been parsed by caller.
	 */

	scha_args.gen_num = NULL;

	if (strcasecmp(optag, SCHA_GIVEOVER) == 0) {
		/*
		 * give up local rg or r.
		 */
		scha_args.action = GIVEOVER;
	} else if (strcasecmp(optag, SCHA_RESTART) == 0) {
		/*
		 * restart rg on local node.
		 */
		scha_args.action = RESTART;
	} else if (strcasecmp(optag, SCHA_RESOURCE_RESTART) == 0) {
		/*
		 * restart resource on local node.
		 */
		scha_args.action = RESOURCE_RESTART;
	} else if (strcasecmp(optag, SCHA_RESOURCE_IS_RESTARTED) == 0) {
		/*
		 * append to resource restart history on local node.
		 */
		scha_args.action = RESOURCE_IS_RESTARTED;
	} else if (strcasecmp(optag, "GETOFF") == 0) {
		/*
		 * get off rg on local node (only used for scalable services).
		 */
		scha_args.action = GETOFF;
	} else if (strcasecmp(optag, SCHA_CHECK_GIVEOVER) == 0) {
		/*
		 * sanity check only
		 */
		scha_args.action = CHECK_GIVEOVER;
	} else if (strcasecmp(optag, SCHA_CHECK_RESTART) == 0) {
		/*
		 * sanity check only
		 */
		scha_args.action = CHECK_RESTART;
	} else if (strcasecmp(optag, SCHA_CHANGE_STATE_ONLINE) == 0) {
		/*
		 * Change the resource state on the local node to ONLINE
		 */
		scha_args.action = CHANGE_STATE_ONLINE;
	} else if (strcasecmp(optag, SCHA_CHANGE_STATE_OFFLINE) == 0) {
		/*
		 * Change the resource state on the local node to OFFLINE
		 */
		scha_args.action = CHANGE_STATE_OFFLINE;
	} else if (strcasecmp(optag, SCHA_IGNORE_FAILED_START) == 0)  {
		scha_args.action = IGNORE_FAILED_START;
	} else if (strcasecmp(optag, SCHA_RESOURCE_DISABLE) == 0)  {
		/*
		 * Disable resource on the local node.
		 */
		scha_args.action = RESOURCE_DISABLE;
	} else {
		return (SCHA_ERR_INVAL);
	}

	local_zone[0] = '\0';

	if (zonename != NULL) {
#if SOL_VERSION >= __s10
		/* Note, it is OK for zonename to be "global". */
		if (strlcpy(local_zone, zonename, (ZONENAME_MAX + 1)) >=
		    (ZONENAME_MAX + 1)) {
			return (SCHA_ERR_INVAL);
		}
#else
		return (SCHA_ERR_INVAL);
#endif

	} else {
#if SOL_VERSION >= __s10
		if ((zoneid = getzoneid()) < 0)
			return (SCHA_ERR_INTERNAL);

		if (getzonenamebyid(zoneid, local_zone, ZONENAME_MAX) < 0) {
			return (SCHA_ERR_INTERNAL);
		}
#endif
	}

	if ((scha_args.local_node = strdup(local_zone)) == NULL)
		return (SCHA_ERR_NOMEM);

	if ((scha_args.rg_name = strdup(rg_name)) == NULL) {
		free(scha_args.local_node);
		return (SCHA_ERR_NOMEM);
	}

	if (r_name != NULL) {
		if ((scha_args.r_name = strdup(r_name)) == NULL) {
			free(scha_args.local_node);
			free(scha_args.rg_name);
			return (SCHA_ERR_NOMEM);
		}
	}

	err = scha_rpc_open(local_zone, &clnt);

	if (err != SCHA_ERR_NOERR) {
		free(scha_args.local_node);
		free(scha_args.rg_name);
		free(scha_args.r_name);
		return (SCHA_ERR_INTERNAL);
	}
#if DOOR_IMPL
	err = scha_door_clnt(&scha_args, &result, SCHA_CONTROL, clnt);
	security_clnt_freebinding(clnt);

	free(scha_args.local_node);
	free(scha_args.rg_name);
	free(scha_args.r_name);

	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_control", err);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		return (map_scha_err(result.ret_code));
	}
#else
	err = scha_control_1(&scha_args, &result, clnt);
	security_clnt_freebinding(clnt);

	free(scha_args.local_node);
	free(scha_args.rg_name);
	free(scha_args.r_name);

	if (err != RPC_SUCCESS) {
		set_error(clnt, "scha_control", err);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		return (map_scha_err(result.ret_code));
	}
#endif
	return (SCHA_ERR_NOERR);
}


/*
 * scha_control_zone_run_r_init()
 * Arguments are resource name list and zonename.
 * The zonename should be NULL on pre s10 versions.
 * If zonename is NULL, scha_control_zone_run_r_init
 * is run on the zone from where the command is executed; otherwise it is
 * run on the zonename passed
 */
scha_err_t
scha_control_zone_run_r_init(const char **r_list,
    const char *zonename)
{
	int	err;

#if SOL_VERSION >= __s10
	char	local_zone[ZONENAME_MAX + 1 ];
	zoneid_t zoneid;
#else
	char	local_zone[1]; 	/* not used */
#endif

#if DOOR_IMPL
	client_handle	*clnt = NULL;
#else
	CLIENT		*clnt;
#endif
	scha_control_args	scha_args = {RESOURCE_RUN_INIT_METHOD, NULL,
	    NULL, NULL, NULL};
	scha_result_t		result = {0};

	/*
	 * assume the args has already been parsed by caller.
	 */

	scha_args.gen_num = NULL;

	local_zone[0] = '\0';

	if (zonename != NULL) {
#if SOL_VERSION >= __s10
		if (strcmp(zonename, GLOBAL_ZONENAME) != 0) {
			if (strlcpy(local_zone, zonename, (ZONENAME_MAX + 1))
			    >= (ZONENAME_MAX + 1)) {
				return (SCHA_ERR_INVAL);
			}
		}
#else
		return (SCHA_ERR_INVAL);
#endif

	} else {
#if SOL_VERSION >= __s10
		if ((zoneid = getzoneid()) < 0)
			return (SCHA_ERR_INTERNAL);

		if (getzonenamebyid(zoneid, local_zone, ZONENAME_MAX) < 0) {
			return (SCHA_ERR_INTERNAL);
		}
#endif
	}

	if ((scha_args.local_node = strdup(local_zone)) == NULL)
		return (SCHA_ERR_NOMEM);

	if (r_list != NULL)
		array_to_strarray(r_list, &scha_args.r_names);

	err = scha_rpc_open(local_zone, &clnt);

	if (err != SCHA_ERR_NOERR) {
		free(scha_args.local_node);
		free_strarr_list(scha_args.r_names);
		return (SCHA_ERR_INTERNAL);
	}
#if DOOR_IMPL
	err = scha_door_clnt(&scha_args, &result, SCHA_CONTROL, clnt);
	security_clnt_freebinding(clnt);

	free(scha_args.local_node);
	free_strarr_list(scha_args.r_names);

	if (err != DOOR_CALL_SUCCESS) {
		set_error("scha_control_run_r_init", err);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		return (map_scha_err(result.ret_code));
	}
#else
	err = scha_control_1(&scha_args, &result, clnt);
	security_clnt_freebinding(clnt);

	free(scha_args.local_node);
	free_strarr_list(scha_args.r_names);

	if (err != RPC_SUCCESS) {
		set_error(clnt, "scha_control_run_r_init", err);
		return (SCHA_ERR_INTERNAL);
	}

	if (result.ret_code != SCHA_ERR_NOERR) {
		return (map_scha_err(result.ret_code));
	}
#endif
	return (SCHA_ERR_NOERR);
}


scha_err_t
scha_control(const char *optag, const char *rg_name, const char *r_name) {
	return (scha_control_zone(optag, rg_name, r_name, NULL));
}

scha_err_t
scha_control_run_r_init(const char **r_list) {
	return (scha_control_zone_run_r_init(r_list, NULL));
}
