/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#ifndef _SCHA_LIB_H_
#define	_SCHA_LIB_H_

#pragma ident	"@(#)scha_lib.h	1.48	08/06/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <netdb.h>
#include <scha.h>
#include <rgm/scha_strings.h>

#include <rgm/security.h>
#include <stdio.h>

#include <rgm/rgm_recep.h>

#define	SC30_API_VERSION	"SC3.0_API_1"

/*
 * API operation code for client rpc call
 */
typedef enum scha_op {
	RS_GET = 0,		/* get Resource property */
	RS_GET_EXT,		/* get extension property */
	RS_GET_EXT_NAMES,	/* get all extension property names */
	RS_GET_STATUS,		/* get Resource status on specified node */
	RS_GET_SWITCH,		/* get On_off/Monitored switch of a resource */
				/* on a specified node. */
	RS_GET_METHOD_T,	/* get method timeout value */
	RS_GET_STATE,		/* get Resource state on specified node */
	RS_GET_FAIL_STATUS,	/* get fail status of UPDATE, INIT and FINI */
				/* method */
	RT_GET,			/* Resource Type get */
	RT_GET_PATHNAME,	/* get Resource Type method pathename */
	RG_GET,			/* Resource Group get */
	RG_GET_STATE,		/* Resource Group get state on specified */
				/* node */
	CL_GET,			/* cluster get */
	IS_PER_NODE		/* Property per-node ? */
} scha_op_t;

/*
 * This struct is used to contain a link list of buffer address which
 * allocated when scha_*_get function is called
 */
typedef struct buf_info {
	char		*tag;
	void		*buf_addr;
	struct buf_info	*next;
} buf_info_t;

/*
 * Resource handle descriptor
 *
 * This struct is used to contain Resource information
 *
 */
typedef struct scha_rshandle {
	char		*version;	/* API version */
	char		*rs_name;	/* resource name */
	char		*rg_name;	/* resource group name */
	char		*rt_name;	/* resource type name */
	char		*rs_sequence_id; /* RS sequence id */
	char		*rt_sequence_id; /* RT sequence id */
	buf_info_t	*buf_addrs;	/* contains the link list of property */
					/* values */
} scha_rshandle_t;

/*
 * Resource Type handle descriptor
 *
 * This struct is used to contain Resource Type information
 *
 */
typedef struct scha_rthandle {
	char		*version;	/* API version */
	char		*rt_name;	/* resource name */
	char		*rt_sequence_id; /* RT sequence id */
	buf_info_t	*buf_addrs;	/* contains the link list of property */
					/* values */
} scha_rthandle_t;

/*
 * Resource Group handle descriptor
 *
 * This struct is used to contain Resource Group information
 *
 */
typedef struct scha_rghandle {
	char		*version;	/* API version */
	char		*rg_name;	/* RG name */
	char		*rg_sequence_id; /* RG sequence id */
	buf_info_t	*buf_addrs;	/* contains the link list of */
					/* property */
					/* values */
} scha_rghandle_t;

/*
 * Cluster handle descriptor
 *
 * This struct is used to contain cluster information
 *
 */
typedef struct scha_clhandle {
	char		*version;	/* API version */
	buf_info_t	*buf_addrs;	/* contains the link list of */
					/* property values */
	char		*incr_num;	/* incarnation number for cluster */
	FILE *recep_file;
} scha_clhandle_t;

/*
 * define a internal tag for scha_get_all_extprops call
 */

/*
 * tag-property function descriptor
 *
 * This struct is used to contain the tag and the sub-tags, operation code,
 * and property type information that were passed in from scha_*_get call.
 *
 */
typedef struct scha_tag {
	char			*tag;	/* tag name */
	boolean_t		extra_arg;	/* B_TREU if needs an extra */
						/* argument */
	scha_op_t		op;	/* operation code for this tag */
	scha_prop_type_t	type;	/* property type */
} scha_tag_t;

/*
 * functions handle API requests
 */
extern scha_err_t rs_open(
    const char *cluster, const char *rs_name, const char *rg_name,
    char **rt_name, char **rs_seq_id, char **rt_seq_id);
extern scha_err_t rs_get(const char *cluster, const char *rs_name,
    const char *rg_name, const char *rs_prop_name,
    const char *zonename, char **rs_seq_id,
    scha_str_array_t **rs_ret_buf, boolean_t qarg);
extern scha_err_t rs_get_ext(
    const char *cluster, const char *rs_name, const char *rg_name,
    const char *rs_prop_name, const char *rs_node_name,
    char **rs_seq_id, scha_prop_type_t *rs_prop_type,
    scha_str_array_t **buf);
extern scha_err_t rs_get_status(
    const char *cluster, const char *rs_name, const char *rg_name,
    char *node_name, char *zonename, char **rs_seq_id,
    int *rs_status, char **rs_status_msg);
extern scha_err_t rs_get_switch(
    const char *cluster, const char *rs_name, const char *rg_name,
    char *node_name, char **rs_seq_id, boolean_t on_off,
    int *rs_switch);
extern scha_err_t rs_get_state(
    const char *cluster, const char *rs_name, const char *rg_name,
    char *node_name, char *zonename, char **rs_seq_id,
    scha_str_array_t **rs_state);
extern scha_err_t rs_get_failed_status(const char *cluster,
    const char *rs_name, const char *rg_name, char *node_name,
    char *method_name, char **rs_seq_id,
    scha_str_array_t **rs_failed_status);

extern scha_err_t is_pernode_prop(const char *cluster,
    const char *rt_name, char *prop_name, char **rs_seq_id,
    scha_str_array_t **is_pernode);
extern scha_err_t rs_get_all_extprops(const char *cluster,
    const char *rs_name, const char *rg_name, char **rs_seq_id,
    scha_str_array_t **prop_list);

extern scha_err_t rg_open(const char *cluster, const char *rg_name,
    char **rg_seq_id);
extern scha_err_t rg_get(
    const char *cluster, const char *rg_name, const char *rg_prop_name,
    char **rg_seq_id, scha_str_array_t **rg_ret_buf);
extern scha_err_t rg_get_state(
    const char *cluster, const char *rg_name, char *node_name,
    char *zonename, char **rs_seq_id, scha_str_array_t **rs_state);
extern scha_err_t rt_open(const char *cluster, const char *rt_name,
    char **rt_seq_id);
extern scha_err_t rt_get(
    const char *cluster, const char *rt_name, const char *rt_prop_name,
    char **rt_seq_id, scha_str_array_t **rt_ret_buf);
extern scha_err_t rt_get_pathnames(const char *cluster,
    const char *rt_name, const char *rt_prop_name, char *method_name,
    char **rt_seq_id, scha_str_array_t **rt_ret_buf);

extern scha_err_t rs_set_status(const char *rs_name, const char *rg_name,
    const char *zonename, scha_rsstatus_t status, const char *status_msg);

extern scha_err_t cluster_open(const char *cluster, char **cluster_incr_num);
extern scha_err_t cluster_get(const char *cluster, const char *cluster_tag,
    char *node_name, uint_t node_id, const char *zonename,
    scha_str_array_t **ret_buf, char **cluster_incr_num);

extern int scha_lb(const char *cluster, const char *rs_name,
	scha_ssm_lb_op_t op_code, int *op_data,
	int data_len, int **ret_op_data, int *ret_data_len);

/*
 * internal utility functions for all commands
 */

extern scha_err_t free_rhandle(scha_resource_t handlep);
extern scha_err_t free_rthandle(scha_resourcetype_t handlep);
extern scha_err_t free_rghandle(scha_resourcegroup_t handlep);
extern scha_err_t free_clhandle(scha_cluster_t handlep);

extern int get_matched_tag(const char *tag, scha_tag_t **tag_table);
extern int get_m_timeout_name(char *method, char **timeout_name);
extern scha_err_t init_buf_addrs(buf_info_t **buf_p, char *str_buf,
	scha_str_array_t *arr_buf, scha_status_value_t *status_buf,
	scha_extprop_value_t *ext_buf, scha_uint_array_t *intarray_buf,
	const char *tag);
extern scha_err_t add_buf_addrs(buf_info_t *buf_p, char *str_buf,
		scha_str_array_t *arr_buf, scha_status_value_t *status_buf,
		scha_extprop_value_t *ext_buf, scha_uint_array_t *intarray_buf,
		const char *tag);
extern scha_err_t free_buf_addrs(buf_info_t *buf_p);
extern scha_err_t free_buf(buf_info_t *buf);

extern scha_err_t convert_ext_buf(scha_prop_type_t type, scha_str_array_t *buf,
		scha_extprop_value_t **ext_buf);
extern scha_err_t save_rs_user_buf(scha_rshandle_t *rhandle, const char *rs_tag,
		char *str_buf, scha_str_array_t *array_buf,
		scha_status_value_t *status_buf,
		scha_extprop_value_t *extprop_buf);
extern scha_err_t save_rt_user_buf(scha_rthandle_t *rthandle,
		const char *rt_tag, char *str_buf, scha_str_array_t *array_buf);
extern scha_err_t save_rg_user_buf(scha_rghandle_t *rghandle,
		const char *rg_tag, char *str_buf, scha_str_array_t *array_buf);
extern scha_err_t save_cl_user_buf(scha_clhandle_t *clhandle,
		const char *cl_tag, char *str_buf,
		scha_str_array_t *strarray_buf, scha_uint_array_t *intarry_buf);

#if DOOR_IMPL
extern void set_error(char *action, int ret);
extern int scha_rpc_open(const char *cluster, client_handle **clnt);
#else
extern void set_error(CLIENT *clnt, char *action, int ret);
extern int scha_rpc_open(CLIENT **clnt);
#endif

extern void strlist_free(strarr *list);
extern void strarr_free(scha_str_array_t *str_arr_list);
extern void extprop_free(scha_extprop_value_t *ext_buf);

extern scha_err_t map_scha_err(scha_err_t);

#ifdef __cplusplus
}
#endif

#endif /* _SCHA_LIB_H_ */
