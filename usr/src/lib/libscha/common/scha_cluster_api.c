/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scha_cluster_api.c	1.44	08/06/20 SMI"

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdarg.h>
#include <rgm/scha_priv.h>

#include "scha_lib.h"

#define	recep_file_line_sz 1028
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif
/*
 * FlexeLint doesn't understand va_arg().  The first occurrence of
 * va_arg() in a file produces FlexeLint errors e718, e747.  The
 * remaining occurrences produce e516.  We suppress all of them.
 * Lint also doesn't understand va_start(), which produces e40.
 */

static scha_err_t get_nodename_helper(const char *, char **, const char *tag);

/*
 * scha_cluster_open()
 *
 *      This function returns a "handle" to be used by
 *      other resource access function (scha_cluster_get).
 *
 *	This function calls cluster_open to send a RPC request to the
 *	rgmd receptionist to get the cluster information
 *
 *      A NULL handle is returned with the error code if open fails.
 */
scha_err_t
scha_cluster_open(scha_cluster_t *handlep)
{
	return (scha_cluster_open_zone(NULL, handlep));
}

scha_err_t
scha_cluster_open_zone(const char *cluster, scha_cluster_t *handlep)
{
	scha_clhandle_t	*new_handle = NULL;
	char		*cluster_incr_num = NULL;
	scha_err_t	err;

	new_handle = (scha_clhandle_t *)calloc((size_t)1,
	    sizeof (scha_clhandle_t));
	if (new_handle == NULL) {
		return (SCHA_ERR_NOMEM);
	}

	bzero(new_handle, sizeof (scha_clhandle_t));


	/*
	 * send request to get cluster incarnation number
	 */
	err = cluster_open(cluster, &cluster_incr_num);
	if (err != SCHA_ERR_NOERR) {
		*handlep = NULL;
		free(new_handle);
		return (err);
	}

	new_handle->incr_num = cluster_incr_num;
	new_handle->version = strdup(SC30_API_VERSION);
	new_handle->buf_addrs = NULL;

	*handlep = new_handle;
	return (SCHA_ERR_NOERR);
}

/*
 * scha_cluster_get()
 *
 *	This function accepts the cluster info handle, an API
 *	defined operation tag name(defined in scha.h file).
 *
 *	This function calls cluster_get to send a RPC request to
 *	receptionist RGMD to retrieve the cluster information
 *
 *	The OUT parameter depends on the property type; see
 *	calls to va_arg().
 *
 */


scha_err_t
scha_cluster_get_zone_helper(const char *cluster,
    scha_cluster_t handlep, const char *cluster_tag,
    va_list ap)
{
	scha_clhandle_t 	*clhandle = (scha_clhandle_t *)handlep;
	scha_tag_t		*tag_prop = NULL;
	boolean_t		extra_arg;
	scha_prop_type_t	type;
	scha_str_array_t	*buf = NULL;
	scha_uint_array_t	**uintarray_buf = NULL;
	scha_str_array_t	**strarray_buf = NULL;
	char			**str_buf = NULL;
	int			*int_buf = 0;
	uint_t			*uint_buf = 0;
	char			*sub_tag = NULL;
	uint_t			sub_uint_tag = 0;
	char			*cluster_incr_num = NULL;
	scha_err_t		err = SCHA_ERR_NOERR;
	uint_t			i;
	char			*endp;

	char line[recep_file_line_sz];
	char *lasts_node;
	char *node_data;
	uint_t nodeid, no_nodes, nodeid_i;
	FILE *recep_file = NULL;
#if SOL_VERSION >= __s10
	char zonename[ZONENAME_MAX];
	zoneid_t zoneid;
#else
	char zonename[1];
#endif
	zonename[0] = '\0';

	/*
	 * If the handle is invalid, return with error
	 */
	if ((clhandle == NULL) ||
	    (strcmp(clhandle->version, SC30_API_VERSION))) {
		return (SCHA_ERR_HANDLE);
	}

	/*
	 * Look through the tag_property table to find the matched tag name
	 * If can't find the match, return error
	 */
	if (get_matched_tag(cluster_tag, &tag_prop)) {
		return (SCHA_ERR_TAG);
	}

	extra_arg = tag_prop->extra_arg;
	type = tag_prop->type;


	/*
	 * If there is an extra argument,
	 * get it from the variable argument list.
	 * Currently the only supported extra argument is a node name
	 * or node id.
	 */
	if (extra_arg) {
		if (strcmp(cluster_tag, SCHA_NODENAME_NODEID) == 0 ||
		    strcmp(cluster_tag, SCHA_ALL_ZONES_NODEID) == 0 ||
		    strcmp(cluster_tag, SCHA_ALL_NONGLOBAL_ZONES_NODEID) == 0) {
			/*
			 * suppressing FlexeLint complaints:
			 * Info(718) [c:45]: __builtin_va_arg_incr undeclared,
			 * assumed to return int
			 * Info(746) [c:45]: call to __builtin_va_arg_incr()
			 * not made in the presence of a prototype
			 */
			sub_uint_tag =
			    va_arg(ap, uint_t);	/*lint !e718 !e746 */
		} else {
			/*
			 * suppressing FlexeLint complaint e516:
			 * __builtin_va_arg_incr() has arg. type conflict
			 * (arg. no. 1 -- ptrs to incompatible types) with
			 * [the immediately preceding va_arg line]
			 */
			sub_tag = va_arg(ap, char *);	/*lint !e516 */
			if (sub_tag == NULL) {
				err = SCHA_ERR_INVAL;
				goto errout1; /*lint !e801 */
			}
		}
	}
#if SOL_VERSION >= __s10
			if ((zoneid = getzoneid()) < 0) {
				err = SCHA_ERR_INTERNAL;
				goto errout2; /*lint !e801 */
			}
			if (zoneid != 0 && getzonenamebyid(zoneid, zonename,
			    (size_t)ZONENAME_MAX) < 0) {
				err = SCHA_ERR_INTERNAL;
				goto errout2; /*lint !e801 */
			}
#endif

	/*
	 * currently we bypass recep-file lookup for querying
	 * privatelinkhostnames since they are not updates with the local
	 * zone privatelinkhostnames.
	 * We may sometime later want to optimise this behaviour.
	 */
	if (strcasecmp(cluster_tag, SCHA_PRIVATELINK_HOSTNAME_NODE) == 0 ||
	    strcasecmp(cluster_tag, SCHA_ALL_PRIVATELINK_HOSTNAMES) == 0 ||
	    strcasecmp(cluster_tag, SCHA_PRIVATELINK_HOSTNAME_LOCAL) == 0)
		goto rpc_fetch; /*lint !e801 */
	/*
	 * if its a command running in global zone on behalf of a
	 * zone cluster, we dont
	 * want to read the global zone configuration. By the way currently
	 * commands running in a zone will anyway not have access to the recep
	 * file. Hence its only useful for physical cluster and that too only
	 * for methods running in the global zone itself.
	 */
	if (cluster != NULL)
		goto rpc_fetch;
	if ((recep_file = fopen(RGM_RECEP_FILE, "r")) == NULL) {
		/*
		 * If we didn't find the recep_file, or are unable to open
		 * it, we try the old fashioned way of using the RPC.
		 */
		/* printf("scha_cluster_get is not cached.\n"); */
		goto rpc_fetch; /*lint !e801 */
	} else {
		/* printf("scha_cluster_get is cached.\n"); */
	}

	if (strcmp(cluster_tag, SCHA_CLUSTERNAME) == 0) {
		(void) fgets(line, recep_file_line_sz, recep_file);

		/* str_buf will be the OUT parameter */
		if ((str_buf = va_arg(ap, char **)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1; /*lint !e801 */
		}

		*str_buf = NULL;

		/* To discard the trailing \n */
		line[strlen(line) - 1] = '\0';

		*str_buf = strdup(line);
		err = save_cl_user_buf(clhandle, cluster_tag, *str_buf, NULL,
		    NULL);

		goto errout1; /*lint !e801 */
	}

	if (strcmp(cluster_tag, SCHA_NODENAME_NODEID) == 0) {
		/* read Clustername */
		(void) fgets(line, recep_file_line_sz, recep_file);
		/* read the no_nodes */
		(void) fgets(line, recep_file_line_sz, recep_file);
		(void) sscanf(line, "%u", &no_nodes);

		/* str_buf will be the OUT parameter */
		if ((str_buf = va_arg(ap, char **)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1; /*lint !e801 */
		}

		*str_buf = NULL;

		for (nodeid_i = 1; nodeid_i <= no_nodes; nodeid_i++) {
			/* read all the data for node 'nodeid_i' */
			(void) fgets(line, recep_file_line_sz, recep_file);
			node_data = line;
			/* read nodeid */
			(void) strtok_r(node_data, ",", &lasts_node);
			(void) sscanf(node_data, "%u", &nodeid);
			if (nodeid == sub_uint_tag) {
				node_data = lasts_node;
				/* read nodename */
				(void) strtok_r(node_data, ",", &lasts_node);
				*str_buf = strdup(node_data);
				break;
			}
		}

		if (nodeid_i > no_nodes)
			err = SCHA_ERR_NODE;
		else
			err = save_cl_user_buf(clhandle, cluster_tag, *str_buf,
			    NULL, NULL);

		goto errout1; /*lint !e801 */
	}

	if (strcmp(cluster_tag, SCHA_PRIVATELINK_HOSTNAME_NODE) == 0) {
		/* currently this code is never accessed. */
		/* read Clustername */
		(void) fgets(line, recep_file_line_sz, recep_file);
		/* read the no_nodes */
		(void) fgets(line, recep_file_line_sz, recep_file);
		(void) sscanf(line, "%u", &no_nodes);

		/* str_buf will be the OUT parameter */
		if ((str_buf = va_arg(ap, char **)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1; /*lint !e801 */
		}

		*str_buf = NULL;

		for (nodeid_i = 1; nodeid_i <= no_nodes; nodeid_i++) {
			/* read all the data for node 'nodeid_i' */
			(void) fgets(line, recep_file_line_sz, recep_file);
			node_data = line;
			/* read nodeid */
			(void) strtok_r(node_data, ",", &lasts_node);

			node_data = lasts_node;
			/* read nodename */
			(void) strtok_r(node_data, ",", &lasts_node);


			if (strcmp(node_data, sub_tag) == 0) { /*lint !e668 */
				/* To discard the trailing \n */
				lasts_node[strlen(lasts_node) - 1] = '\0';
				*str_buf = strdup(lasts_node);
				break;
			}
		}

		if (nodeid_i > no_nodes)
			err = SCHA_ERR_NODE;
		else
			err = save_cl_user_buf(clhandle, cluster_tag, *str_buf,
			    NULL, NULL);

		goto errout1; /*lint !e801 */
	}

	if (strcmp(cluster_tag, SCHA_ALL_NODEIDS) == 0) {
		/* read Clustername */
		(void) fgets(line, recep_file_line_sz, recep_file);
		/* read the no_nodes */
		(void) fgets(line, recep_file_line_sz, recep_file);
		(void) sscanf(line, "%u", &no_nodes);

		/* uintarray_buf will be the OUT parameter  */
		if ((uintarray_buf =
		    va_arg(ap, scha_uint_array_t **))
		    == NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1; /*lint !e801 */
		}

		*uintarray_buf = NULL;

		*uintarray_buf =
		    (scha_uint_array_t *)malloc(sizeof (scha_uint_array_t));

		(*uintarray_buf)->array_cnt = no_nodes;
		(*uintarray_buf)->uint_array =
			(uint_t *)malloc(no_nodes*sizeof (uint_t));

		for (nodeid_i = 1; nodeid_i <= no_nodes; nodeid_i++) {
			/* read all the data for node 'nodeid_i' */
			(void) fgets(line, recep_file_line_sz, recep_file);
			node_data = line;
			/* read nodeid */
			(void) strtok_r(node_data, ",", &lasts_node);
			(void) sscanf(node_data, "%u", &nodeid);
			/* -1 below because the nodeid_i starts with 1 */
			(*uintarray_buf)->uint_array[nodeid_i - 1] = nodeid;
		}

		err = save_cl_user_buf(clhandle, cluster_tag, NULL,
		    NULL, *uintarray_buf);
		if (err != SCHA_ERR_NOERR) {
			free((*uintarray_buf)->uint_array);
			free(*uintarray_buf);
			goto errout1; /*lint !e801 */
		}

		goto errout1; /*lint !e801 */
	}

rpc_fetch:
	/*
	 * Call cluster_get to send the request through RPC call.
	 * If there is an error, we free memory and jump out of the
	 * switch statement:
	 * errout1: calls va_end()
	 * errout2: frees cluster_incr_num then calls va_end()
	 */
	switch (type) {
	case SCHA_PTYPE_ENUM :


		/* int_buf will be the OUT parameter */
		if ((int_buf = va_arg(ap, int *)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1; /*lint !e801 */
		}

		*int_buf = 0;


		if ((err = cluster_get(
		    cluster, cluster_tag, sub_tag, 0, zonename, &buf,
		    &cluster_incr_num)) != SCHA_ERR_NOERR)
			goto errout1; /*lint !e801 */

		if (buf == NULL)
			break;		/* no data was returned */

		if (buf->str_array[0] != NULL) {
			*int_buf = (int)strtol(buf->str_array[0], &endp, 0);
			if (*endp != '\0') {
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				err = SCHA_ERR_INTERNAL;
				goto errout2; /*lint !e801 */
			}
			free(buf->str_array[0]);
		}
		/* free everything because we're returning an int */
		free(buf->str_array);
		free(buf);

		break;

	case SCHA_PTYPE_INT :

		/* int_buf will be the OUT parameter */
		if ((int_buf = va_arg(ap, int *)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1; /*lint !e801 */
		}

		*int_buf = 0;

		if (strcasecmp(cluster_tag, SCHA_SYSLOG_FACILITY) == 0) {
			*int_buf = (int)SCHA_CLUSTER_LOGFACILITY;
			err = SCHA_ERR_NOERR;
			/* we're finished */
			break;
		}

		if ((err = cluster_get(cluster, cluster_tag, sub_tag,
		    0, NULL, &buf, &cluster_incr_num)) != SCHA_ERR_NOERR)
			goto errout1; /*lint !e801 */

		if (buf == NULL)
			break;		/* no data was returned */

		if (buf->str_array[0] != NULL) {
			*int_buf = (int)strtol(buf->str_array[0], &endp, 0);
			if (*endp != '\0') {
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				err = SCHA_ERR_INTERNAL;
				goto errout2; /*lint !e801 */
			}
			free(buf->str_array[0]);
		}
		/* free everything because we're returning an int */
		free(buf->str_array);
		free(buf);

		break;

	case SCHA_PTYPE_UINT :

		/* uint_buf will be the OUT parameter */
		if ((uint_buf =
		    va_arg(ap, uint_t *)) ==
		    NULL) {	/*lint !e516 !e662 */
			err =  SCHA_ERR_INVAL;
			goto errout1; /*lint !e801 */
		}

		*uint_buf = 0;

		if ((err = cluster_get(
		    cluster, cluster_tag, sub_tag, 0, NULL, &buf,
		    &cluster_incr_num)) != SCHA_ERR_NOERR)
			goto errout1; /*lint !e801 */

		if (buf == NULL)
			break;		/* no data was returned */

		if (buf->str_array[0] != NULL) {
			*uint_buf = (uint_t)strtol(buf->str_array[0], &endp, 0);
			if (*endp != '\0') {
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				err = SCHA_ERR_INTERNAL;
				goto errout2; /*lint !e801 */
			}
			free(buf->str_array[0]);
		}
		/* free everything because we're returning a uint */
		free(buf->str_array);
		free(buf);

		break;

	case SCHA_PTYPE_STRING :

		/* str_buf will be the OUT parameter */
		if ((str_buf = va_arg(ap, char **)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1; /*lint !e801 */
		}

		*str_buf = NULL;

		if ((err = cluster_get(cluster, cluster_tag, sub_tag,
		    sub_uint_tag, zonename, &buf, &cluster_incr_num))
		    != SCHA_ERR_NOERR)
			goto errout1; /*lint !e801 */

		if (buf == NULL)
			break;		/* no data was returned */

		if (((*str_buf) = buf->str_array[0]) != NULL) {
			/*
			 * Save the address of the returned data in the handle
			 * so a subsequent call to scha_cluster_close() can
			 * free memory.
			 */
			err = save_cl_user_buf(clhandle, cluster_tag,
			    *str_buf, NULL, NULL);
			if (err != SCHA_ERR_NOERR) {
				/*
				 * free memory and return error.
				 */
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				goto errout2; /*lint !e801 */
			}
		}
		/* free all but the string we're returning */
		free(buf->str_array);
		free(buf);

		break;

	case SCHA_PTYPE_STRINGARRAY :

		/* strarray_buf will be the OUT parameter */
		if ((strarray_buf =
		    va_arg(ap, scha_str_array_t **)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1; /*lint !e801 */
		}

		*strarray_buf = NULL;

		if ((err = cluster_get(
		    cluster, cluster_tag, sub_tag, sub_uint_tag,
		    NULL, &buf, &cluster_incr_num)) != SCHA_ERR_NOERR)
			goto errout1; /*lint !e801 */

		if (buf == NULL)
			break;

		*strarray_buf = buf;
		/*
		 * Save the address of the returned data in the handle
		 * so a subsequent call to scha_cluster_close() can
		 * free memory.
		 */
		err = save_cl_user_buf(clhandle, cluster_tag, NULL,
		    *strarray_buf, NULL);
		if (err != SCHA_ERR_NOERR) {
			/*
			 * free memory in buf and return error.
			 */
			strarr_free(buf);
			goto errout2; /*lint !e801 */
		}
		/* don't free anything; we return the whole stringarray */
		break;

	case SCHA_PTYPE_UINTARRAY :

		/* uintarray_buf will be the OUT parameter */
		if ((uintarray_buf =
		    va_arg(ap, scha_uint_array_t **)) ==
		    NULL) { /*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1; /*lint !e801 */
		}

		*uintarray_buf = NULL;

		if ((err = cluster_get(
		    cluster, cluster_tag, sub_tag, 0, NULL, &buf,
		    &cluster_incr_num)) != SCHA_ERR_NOERR)
			goto errout1; /*lint !e801 */

		if (buf == NULL)
			break;

		if ((*uintarray_buf =
		    (scha_uint_array_t *)calloc((size_t)1,
		    sizeof (scha_uint_array_t))) == NULL) {
			err = SCHA_ERR_NOMEM;
			goto errout2; /*lint !e801 */
		}

		if (((*uintarray_buf)->uint_array = (uint_t *)
		    calloc((size_t)buf->array_cnt, sizeof (uint_t)))
		    == NULL) {
			free(uintarray_buf);
			err = SCHA_ERR_NOMEM;
			goto errout2; /*lint !e801 */
		}

		(*uintarray_buf)->array_cnt = buf->array_cnt;
		for (i = 0; i < buf->array_cnt; i++) {
			(*uintarray_buf)->uint_array[i] =
			    (uint_t)strtol(buf->str_array[i], &endp, 0);
			if (*endp != '\0') {
				/*
				 * If meet any problem, clean up
				 * and return error.
				 */
				for (i = 0; i < buf->array_cnt; i++) {
					/*
					 * Since calloc will zero out
					 * the memory allocated, there
					 * will be no risk to do blind
					 * free for all of them.
					 */
					free(buf->str_array[i]);
				}
				free((*uintarray_buf)->uint_array);
				free(*uintarray_buf);
				free(buf->str_array);
				free(buf);
				err = SCHA_ERR_INTERNAL;
				goto errout2; /*lint !e801 */
			}
			free(buf->str_array[i]);
		}
		free(buf->str_array);
		free(buf);
		/*
		 * Save the address of the returned data in the handle
		 * so a subsequent call to scha_cluster_close() can
		 * free memory.
		 */
		err = save_cl_user_buf(clhandle, cluster_tag, NULL,
		    NULL, *uintarray_buf);
		if (err != SCHA_ERR_NOERR) {
			free((*uintarray_buf)->uint_array);
			free(*uintarray_buf);
			err = SCHA_ERR_INTERNAL;
			goto errout2; /*lint !e801 */
		}
		break;

	case SCHA_PTYPE_BOOLEAN:
	default :
		err = SCHA_ERR_INTERNAL;
		goto errout1; /*lint !e801 */
	}

	if (cluster_incr_num) {
		/*
		 * When info is stored in the CCR and is not tied to a
		 * particular CCR tables's sequence id, or is not stored in
		 * the CCR,
		 * we should suppress checking for SCHA_ERR_SEQID.
		 *
		 * For all other tags, if we got back a sequence
		 * id, make sure it hasn't changed since the
		 * scha_cluster_open() call.
		 */
		if ((strcasecmp(cluster_tag, SCHA_SYSLOG_FACILITY) != 0) &&
		    (strcasecmp(cluster_tag, SCHA_NODESTATE_LOCAL) != 0) &&
		    (strcasecmp(cluster_tag, SCHA_NODESTATE_NODE) != 0) &&
		    (strcasecmp(cluster_tag, SCHA_ALL_RESOURCEGROUPS) != 0) &&
		    (strcasecmp(cluster_tag, SCHA_ALL_RESOURCETYPES) != 0) &&
		    (strcasecmp(cluster_tag, SCHA_NODEID_LOCAL) != 0) &&
		    (strcasecmp(cluster_tag, SCHA_NODENAME_LOCAL) != 0) &&
		    (strcasecmp(cluster_tag, SCHA_ZONE_LOCAL) != 0) &&
		    (strcasecmp(cluster_tag, SCHA_PRIVATELINK_HOSTNAME_LOCAL)
		    != 0) &&
		    (strcasecmp(cluster_tag, SCHA_CLUSTERNAME) != 0)) {

			if (strcmp(clhandle->incr_num,
			    (char *)cluster_incr_num) != 0) {
				err = SCHA_ERR_SEQID;
			}

		}
	}

errout2:
	free(cluster_incr_num);

errout1:

	if (recep_file)
		(void) fclose(recep_file);
	va_end(ap);
	return (err);
}
scha_err_t
scha_cluster_get(scha_cluster_t handlep, const char *cluster_tag, ...)
{
	va_list			ap;
	/*
	 * Initialize the variable arguments list
	 */
	va_start(ap, cluster_tag);	/*lint !e40 !e26 !e50 !e10 */
	return (scha_cluster_get_zone_helper(NULL, handlep, cluster_tag, ap));
}

scha_err_t
scha_cluster_get_zone(
    const char *cluster, scha_cluster_t handlep, const char *cluster_tag, ...)
{
	va_list			ap;
	/*
	 * Initialize the variable arguments list
	 */
	va_start(ap, cluster_tag);	/*lint !e40 !e26 !e50 !e10 */
	return (scha_cluster_get_zone_helper(
	    cluster, handlep, cluster_tag, ap));
}

/*
 * scha_cluster_close()
 *
 * 	This function deallocates the memory associated with the handle
 *
 */
scha_err_t
scha_cluster_close(scha_cluster_t handlep)
{
	scha_clhandle_t *clhandle = (scha_clhandle_t *)handlep;
	scha_err_t	err;

	err = SCHA_ERR_NOERR;
	if (clhandle == NULL)
		return (SCHA_ERR_NOERR);

/*	fclose(clhandle->recep_file); */

	/*
	 * If the handle is invalid, return with error
	 */
	if (strcmp(clhandle->version, SC30_API_VERSION)) {
		return (SCHA_ERR_HANDLE);
	}

	err = free_clhandle(handlep);
	handlep = NULL;
	return (err);
}


/*
 * scha_cluster_getlogfacility()
 *
 * 	This function gets the syslog facility
 *
 */
scha_err_t
scha_cluster_getlogfacility(int *logfacility)
{

	*logfacility = (int)SCHA_CLUSTER_LOGFACILITY;

	return (SCHA_ERR_NOERR);
}

/*
 * scha_cluster_getzone()
 *
 * 	This function gets the name of the local cluster node
 *	in the form "nodename:zonename"
 *
 */
scha_err_t
scha_cluster_getzone(char **nodename) {
	return (get_nodename_helper(NULL, nodename, SCHA_ZONE_LOCAL));
}

/* this function has not been virtalised for 1334 or for zone cluster */

/*
 * scha_cluster_getnodename()
 *
 * 	This function gets the name of the physical node
 *	irrespective of the zone it is called from.
 *
 */
scha_err_t
scha_cluster_getnodename(char **nodename) {
	return (get_nodename_helper(NULL, nodename, SCHA_NODENAME_LOCAL));
}

scha_err_t
scha_cluster_getnodename_zone(const char *cluster, char **nodename) {
	return (get_nodename_helper(cluster, nodename, SCHA_NODENAME_LOCAL));
}

static scha_err_t
get_nodename_helper(const char *cluster, char **nodename, const char *tag)
{

	scha_cluster_t	handle;
	scha_err_t	err;
	char		*nodename_tmp;

	err = SCHA_ERR_NOERR;

	err = scha_cluster_open_zone(cluster, &handle);
	if (err != SCHA_ERR_NOERR)
		return (err);

	if (handle == NULL)
		return (SCHA_ERR_INTERNAL);

	err = scha_cluster_get_zone(cluster, handle, tag, &nodename_tmp);
	if (err != SCHA_ERR_NOERR)
		return (err);

	/*
	 * need to strdup the nodename obtained from rgm receptionist
	 * because the nodename will be freed in scha_cluster_close
	 */
	*nodename = strdup(nodename_tmp);

	err = scha_cluster_close_zone(cluster, handle);
	if (err !=  SCHA_ERR_NOERR)
		return (err);

	return (SCHA_ERR_NOERR);
}
