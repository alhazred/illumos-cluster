/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scha_util.c	1.72	09/01/09 SMI"

/*
 * scha_util.c
 *
 *	utilities for high level API
 */


#include<sys/sol_version.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/types.h>
#include <syslog.h>
#include <rpc/rpc.h>
#include <tzfile.h>
#include <unistd.h>
#include <libintl.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm/scha_priv.h>

#if SOL_VERSION >= __s10
#include<sys/zone.h>
#include<zone.h>
#endif
/*
 * The data definition of errmsg_mappings is done in
 * rgm_scha_err.h file included below. This definition is
 * in scha_strerror function below as a source of error strings.
 * rgm_scha_err.h file is to be included only in this file and
 * nowhere else.
 */

#include "rgm_scha_err.h"
#include "scha_lib.h"

/*
 * Table for tag-to-property name map
 *
 * The format of each entry in this table is defined as follows:
 *
 *      { tag name, extra_arg, function call, property type }
 *
 *      Tag name - the unique API defined function tag name
 *      extra_arg - B_TRUE if this tag needs an extra argument
 *      op - operation code for this tag
 *      property type - the type of the function tag value
 *
 */
static scha_tag_t tagprop_tbl[] = {
	/* Resource tags */
	{ SCHA_TYPE, B_FALSE, RS_GET, SCHA_PTYPE_STRING },
	{ SCHA_TYPE_VERSION, B_FALSE, RS_GET, SCHA_PTYPE_STRING },
	{ SCHA_R_DESCRIPTION, B_FALSE, RS_GET, SCHA_PTYPE_STRING },
	{ SCHA_GROUP, B_FALSE, RS_GET, SCHA_PTYPE_STRING },
	{ SCHA_STATUS, B_FALSE, RS_GET_STATUS, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_STATUS_NODE, B_TRUE, RS_GET_STATUS, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_ON_OFF_SWITCH, B_FALSE, RS_GET_SWITCH, SCHA_PTYPE_ENUM },
	{ SCHA_ON_OFF_SWITCH_NODE, B_TRUE, RS_GET_SWITCH, SCHA_PTYPE_ENUM },
	{ SCHA_MONITORED_SWITCH, B_FALSE, RS_GET_SWITCH, SCHA_PTYPE_ENUM },
	{ SCHA_MONITORED_SWITCH_NODE, B_TRUE, RS_GET_SWITCH, SCHA_PTYPE_ENUM },
	{ SCHA_RESOURCE_STATE, B_FALSE, RS_GET_STATE, SCHA_PTYPE_ENUM },
	{ SCHA_RESOURCE_STATE_NODE, B_TRUE, RS_GET_STATE, SCHA_PTYPE_ENUM },
	{ SCHA_BOOT_FAILED, B_TRUE, RS_GET_FAIL_STATUS, SCHA_PTYPE_BOOLEAN },
	{ SCHA_UPDATE_FAILED, B_TRUE, RS_GET_FAIL_STATUS, SCHA_PTYPE_BOOLEAN },
	{ SCHA_INIT_FAILED, B_TRUE, RS_GET_FAIL_STATUS, SCHA_PTYPE_BOOLEAN },
	{ SCHA_FINI_FAILED, B_TRUE, RS_GET_FAIL_STATUS, SCHA_PTYPE_BOOLEAN },
	{ SCHA_CHEAP_PROBE_INTERVAL, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_THOROUGH_PROBE_INTERVAL, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_RETRY_COUNT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_RETRY_INTERVAL, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_FAILOVER_MODE, B_FALSE, RS_GET, SCHA_PTYPE_ENUM },
	{ SCHA_RESOURCE_DEPENDENCIES, B_FALSE, RS_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RESOURCE_DEPENDENCIES_WEAK, B_FALSE, RS_GET,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RESOURCE_DEPENDENCIES_RESTART, B_FALSE, RS_GET,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART, B_FALSE, RS_GET,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RESOURCE_DEPENDENCIES_Q, B_FALSE,
	    RS_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RESOURCE_DEPENDENCIES_Q_WEAK, B_FALSE, RS_GET,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RESOURCE_DEPENDENCIES_Q_RESTART, B_FALSE, RS_GET,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART, B_FALSE, RS_GET,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RESOURCE_PROJECT_NAME, B_FALSE, RS_GET, SCHA_PTYPE_STRING },
	{ SCHA_NETWORK_RESOURCES_USED, B_FALSE, RS_GET,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_SCALABLE, B_FALSE, RS_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_PORT_LIST, B_FALSE, RS_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_LOAD_BALANCING_POLICY, B_FALSE, RS_GET, SCHA_PTYPE_STRING },
	{ SCHA_LOAD_BALANCING_WEIGHTS, B_FALSE, RS_GET,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_AFFINITY_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_UDP_AFFINITY, B_FALSE, RS_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_WEAK_AFFINITY, B_FALSE, RS_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_GENERIC_AFFINITY, B_FALSE, RS_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_ROUND_ROBIN, B_FALSE, RS_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_CONN_THRESHOLD, B_FALSE, RS_GET, SCHA_PTYPE_INT},
	{ SCHA_EXTENSION, B_TRUE, RS_GET_EXT, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_EXTENSION_NODE, B_TRUE, RS_GET_EXT,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_ALL_EXTENSIONS, B_FALSE, RS_GET_EXT_NAMES,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_START_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_STOP_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_VALIDATE_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_UPDATE_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_INIT_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_FINI_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_BOOT_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_MONITOR_START_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_MONITOR_STOP_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_MONITOR_CHECK_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_PRENET_START_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_POSTNET_STOP_TIMEOUT, B_FALSE, RS_GET, SCHA_PTYPE_INT },
	{ SCHA_NUM_RESOURCE_RESTARTS, B_FALSE, RS_GET, SCHA_PTYPE_UINT },
	{ SCHA_NUM_RG_RESTARTS, B_FALSE, RS_GET, SCHA_PTYPE_UINT },
	{ SCHA_NUM_RESOURCE_RESTARTS_ZONE, B_TRUE, RS_GET, SCHA_PTYPE_UINT },
	{ SCHA_NUM_RG_RESTARTS_ZONE, B_TRUE, RS_GET, SCHA_PTYPE_UINT },
	{ SCHA_GLOBAL_ZONE_OVERRIDE, B_FALSE, RS_GET, SCHA_PTYPE_BOOLEAN },

	/* Resource Type tags */
	{ SCHA_RT_BASEDIR, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_RT_DESCRIPTION, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_SINGLE_INSTANCE, B_FALSE, RT_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_RT_SYSTEM, B_FALSE, RT_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_INIT_NODES, B_FALSE, RT_GET, SCHA_PTYPE_ENUM },
	{ SCHA_INSTALLED_NODES, B_FALSE, RT_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_PROXY, B_FALSE, RT_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_FAILOVER, B_FALSE, RT_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_API_VERSION, B_FALSE, RT_GET, SCHA_PTYPE_INT },
	{ SCHA_RT_VERSION, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_PKGLIST, B_FALSE, RT_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_IS_LOGICAL_HOSTNAME, B_FALSE, RT_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_IS_SHARED_ADDRESS, B_FALSE, RT_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_START, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_STOP, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_VALIDATE, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_UPDATE, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_PER_NODE, B_TRUE, IS_PER_NODE, SCHA_PTYPE_BOOLEAN },
	{ SCHA_INIT, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_FINI, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_BOOT, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_MONITOR_START, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_MONITOR_STOP, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_MONITOR_CHECK, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_PRENET_START, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_POSTNET_STOP, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_GLOBALZONE, B_FALSE, RT_GET, SCHA_PTYPE_BOOLEAN },

	/* Resource Group tags */
	{ SCHA_NODELIST, B_FALSE, RG_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RG_DESCRIPTION, B_FALSE, RT_GET, SCHA_PTYPE_STRING },
	{ SCHA_MAXIMUM_PRIMARIES, B_FALSE, RG_GET, SCHA_PTYPE_INT },
	{ SCHA_DESIRED_PRIMARIES, B_FALSE, RG_GET, SCHA_PTYPE_INT },
	{ SCHA_FAILBACK, B_FALSE, RG_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_RESOURCE_LIST, B_FALSE, RG_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RG_PROJECT_NAME, B_FALSE, RG_GET, SCHA_PTYPE_STRING },
	/* SC SLM addon start */
	{ SCHA_RG_SLM_TYPE, B_FALSE, RG_GET, SCHA_PTYPE_STRING },
	{ SCHA_RG_SLM_PSET_TYPE, B_FALSE, RG_GET, SCHA_PTYPE_STRING },
	{ SCHA_RG_SLM_CPU_SHARES, B_FALSE, RG_GET, SCHA_PTYPE_INT },
	{ SCHA_RG_SLM_PSET_MIN, B_FALSE, RG_GET, SCHA_PTYPE_INT },
	/* SC SLM addon end */
	{ SCHA_RG_AFFINITIES, B_FALSE, RG_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RG_AUTO_START, B_FALSE, RG_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_RG_STATE, B_FALSE, RG_GET_STATE, SCHA_PTYPE_ENUM },
	{ SCHA_RG_STATE_NODE, B_TRUE, RG_GET_STATE, SCHA_PTYPE_ENUM },
	{ SCHA_RG_DEPENDENCIES, B_FALSE, RG_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_GLOBAL_RESOURCES_USED, B_FALSE, RG_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_RG_MODE, B_FALSE, RG_GET, SCHA_PTYPE_ENUM},
	{ SCHA_IMPL_NET_DEPEND, B_FALSE, RG_GET, SCHA_PTYPE_BOOLEAN},
	{ SCHA_PATHPREFIX, B_FALSE, RG_GET, SCHA_PTYPE_STRING },
	{ SCHA_PINGPONG_INTERVAL, B_FALSE, RG_GET, SCHA_PTYPE_INT },
	{ SCHA_RG_SYSTEM, B_FALSE, RG_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_RG_IS_FROZEN, B_FALSE, RG_GET, SCHA_PTYPE_BOOLEAN },
	{ SCHA_RG_SUSP_AUTO_RECOVERY, B_FALSE, RG_GET, SCHA_PTYPE_BOOLEAN },

	/* cluster info tags */
	{ SCHA_NODENAME_LOCAL, B_FALSE, CL_GET, SCHA_PTYPE_STRING },
	{ SCHA_NODENAME_NODEID, B_TRUE, CL_GET, SCHA_PTYPE_STRING },
	{ SCHA_NODENAME_LOCAL, B_FALSE, CL_GET, SCHA_PTYPE_STRING },
	{ SCHA_PRIVATELINK_HOSTNAME_LOCAL, B_FALSE, CL_GET,
		SCHA_PTYPE_STRING },
	{ SCHA_PRIVATELINK_HOSTNAME_NODE, B_TRUE,  CL_GET,
		SCHA_PTYPE_STRING },
	{ SCHA_NODEID_LOCAL, B_FALSE, CL_GET, SCHA_PTYPE_UINT },
	{ SCHA_NODEID_NODENAME, B_TRUE, CL_GET, SCHA_PTYPE_UINT },
	{ SCHA_NODESTATE_LOCAL, B_FALSE, CL_GET, SCHA_PTYPE_ENUM },
	{ SCHA_NODESTATE_NODE, B_TRUE, CL_GET, SCHA_PTYPE_ENUM },
	{ SCHA_SYSLOG_FACILITY, B_FALSE, CL_GET, SCHA_PTYPE_INT },
	{ SCHA_ALL_RESOURCEGROUPS, B_FALSE, CL_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_ALL_RESOURCETYPES, B_FALSE, CL_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_CLUSTERNAME, B_FALSE, CL_GET, SCHA_PTYPE_STRING },
	{ SCHA_ALL_NODENAMES, B_FALSE, CL_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_ALL_PRIVATELINK_HOSTNAMES, B_FALSE, CL_GET,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_ALL_NODEIDS, B_FALSE, CL_GET, SCHA_PTYPE_UINTARRAY },
	{ SCHA_ALL_ZONES, B_FALSE, CL_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_ALL_NONGLOBAL_ZONES, B_FALSE, CL_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_ALL_ZONES_NODEID, B_TRUE, CL_GET, SCHA_PTYPE_STRINGARRAY },
	{ SCHA_ALL_NONGLOBAL_ZONES_NODEID, B_TRUE, CL_GET,
		SCHA_PTYPE_STRINGARRAY },
	{ SCHA_ZONE_LOCAL, B_FALSE, CL_GET, SCHA_PTYPE_STRING },
	{ SCHA_ZONE_TYPE, B_FALSE, CL_GET, SCHA_PTYPE_STRING },
	{ SCHA_VERIFY_IP, B_TRUE, CL_GET, SCHA_PTYPE_UINT },
	{ SCHA_VERIFY_ADAPTER, B_TRUE, CL_GET, SCHA_PTYPE_UINT }
};

/*
*************************************************************************

	scha_rpc_open - calls RPC clnt_tp_create to get the RPC handle

*************************************************************************
*/
#if DOOR_IMPL
int
scha_rpc_open(const char *cluster, client_handle **clnt)
#else
int
scha_rpc_open(CLIENT    **clnt)
#endif
{

#if !DOOR_IMPL
	char	hostname[MAXHOSTNAMELEN];

	/*
	 * Get local host name
	 */

	if (gethostname(hostname, (int)sizeof (hostname)) != 0) {
		return (1);
	}
#else

#if SOL_VERSION >= __s10
	zoneid_t zoneid;
	char zonename[ZONENAME_MAX];
#else
	char *zonename = NULL;
#endif

#endif

	/*
	 * Make door/rpc connection and check security.
	 * The last 2 arguments are:
	 *    0 - no debug messages,
	 *    1 - write to stderr and to syslog
	 */
#if DOOR_IMPL
#if SOL_VERSION >= __s10
	if (cluster == NULL) {
		getzonenamebyid(getzoneid(), zonename, ZONENAME_MAX);
	} else {
		strcpy(zonename, cluster);
	}
#endif
	if (security_clnt_connect(clnt, RGM_SCHA_PROGRAM_CODE,
	    SEC_UNIX_STRONG, 0, 1, zonename))
#else
	if (security_clnt_connect(clnt, hostname, SCHA_PROGRAM_NAME,
	    (ulong_t)SCHA_PROGRAM, (ulong_t)SCHA_VERSION, SEC_UNIX_STRONG,
	    0, 1))
#endif
		return (0);
	else
		return (1);
}

/*
*************************************************************************

	free_rshandle - free allocated memory in the resource handle

*************************************************************************
*/
scha_err_t
free_rhandle(scha_resource_t handlep)
{

	scha_rshandle_t *rhandle = handlep;
	scha_err_t	err;

	err = SCHA_ERR_NOERR;

	if (rhandle) {
		if (rhandle->version)
			free(rhandle->version);

		if (rhandle->rs_name)
			free(rhandle->rs_name);

		if (rhandle->rg_name)
			free(rhandle->rg_name);

		if (rhandle->rs_sequence_id)
			free(rhandle->rs_sequence_id);

		if (rhandle->rt_name)
			free(rhandle->rt_name);

		if (rhandle->rt_sequence_id)
			free(rhandle->rt_sequence_id);

		if (rhandle->buf_addrs) {
			err = free_buf_addrs(rhandle->buf_addrs);
		}
		free(rhandle);
	}
	return (err);
}

/*
*************************************************************************

	free_rthandle - free allocated memory in the RT handle

*************************************************************************
*/
scha_err_t
free_rthandle(scha_resourcetype_t handlep)
{

	scha_rthandle_t *rthandle = handlep;
	scha_err_t	err;

	err = SCHA_ERR_NOERR;

	if (rthandle) {
		if (rthandle->version)
			free(rthandle->version);
		if (rthandle->rt_name)
			free(rthandle->rt_name);

		if (rthandle->rt_sequence_id)
			free(rthandle->rt_sequence_id);

		if (rthandle->buf_addrs) {
			err = free_buf_addrs(rthandle->buf_addrs);
		}
		free(rthandle);
	}
	return (err);
}

/*
*************************************************************************

	free_rghandle - free allocated memory in the RG handle

*************************************************************************
*/
scha_err_t
free_rghandle(scha_resourcegroup_t handlep)
{

	scha_rghandle_t *rghandle = handlep;
	scha_err_t	err;

	err = SCHA_ERR_NOERR;

	if (rghandle) {
		if (rghandle->version)
			free(rghandle->version);

		if (rghandle->rg_name)
			free(rghandle->rg_name);

		if (rghandle->rg_sequence_id)
			free(rghandle->rg_sequence_id);

		if (rghandle->buf_addrs) {
			err = free_buf_addrs(rghandle->buf_addrs);
		}
		free(rghandle);
	}
	return (err);
}

/*
*************************************************************************

	free_clhandle - free allocated memory in the cluster handle

*************************************************************************
*/
scha_err_t
free_clhandle(scha_cluster_t handlep)
{

	scha_clhandle_t *clhandle = handlep;
	scha_err_t	err;

	err = SCHA_ERR_NOERR;

	if (clhandle) {
		if (clhandle->incr_num)
			free(clhandle->incr_num);

		if (clhandle->version)
			free(clhandle->version);

		if (clhandle->buf_addrs) {
			err = free_buf_addrs(clhandle->buf_addrs);
		}
		free(clhandle);
	}
	return (err);
}
/*
*********************************************************************

	get_matched_tag - look through the tag_property table to find the
			matched tag name

*********************************************************************
*/
int
get_matched_tag(const char *tag, scha_tag_t **tag_table)
{

	scha_tag_t	*tablep = NULL;
	int		cnt, i;


	cnt = sizeof (tagprop_tbl) / sizeof (scha_tag_t);
	for (i = 0; i < cnt; i++) {
		tablep = &tagprop_tbl[i];

		if (strcmp(tag, tablep->tag) == 0) {
			/* find the tag */
			*tag_table = tablep;
			return (0);
		}
	}
	return (1);
}

/*
 * method name descriptor
 *
 * This struct is used to contain the method and the associated method timeout
 * name that stored in the CCR table
 *
 */
typedef struct scha_m_timeout {
	char	*method;		/* method name */
	char	*method_timeout;	/* the method timeout name in CCR */
} scha_m_timeout_t;

/*
 * Table for method timeout name
 *
 * The format of each entry in this table is defined as follows:
 *
 *	{ method name, method timeout name }
 *
 *	method name - the unique API defined method name
 *	method timeout name - the defined name for method timeout
 *			This name is defined in rgm.h file.
 *
*/
static scha_m_timeout_t m_timeout_tbl[] = {
	{ SCHA_START, SCHA_START_TIMEOUT },
	{ SCHA_STOP, SCHA_STOP_TIMEOUT },
	{ SCHA_VALIDATE, SCHA_VALIDATE_TIMEOUT },
	{ SCHA_UPDATE, SCHA_UPDATE_TIMEOUT },
	{ SCHA_INIT, SCHA_INIT_TIMEOUT },
	{ SCHA_FINI, SCHA_FINI_TIMEOUT },
	{ SCHA_BOOT, SCHA_BOOT_TIMEOUT },
	{ SCHA_MONITOR_START, SCHA_MONITOR_START_TIMEOUT },
	{ SCHA_MONITOR_STOP, SCHA_MONITOR_STOP_TIMEOUT },
	{ SCHA_MONITOR_CHECK, SCHA_MONITOR_CHECK_TIMEOUT },
	{ SCHA_PRENET_START, SCHA_PRENET_START_TIMEOUT },
	{ SCHA_POSTNET_STOP, SCHA_POSTNET_STOP_TIMEOUT },
};

/*
*********************************************************************

	get_m_timeout_name - look through the method table to find the
			matched method name

*********************************************************************
*/
int
get_m_timeout_name(char *method, char **timeout_name)
{


	scha_m_timeout_t	*timeoutp = NULL;
	int	cnt = sizeof (m_timeout_tbl) / sizeof (scha_m_timeout_t);
	int	i;


	for (i = 0; i < cnt; i++) {
		timeoutp = &m_timeout_tbl[i];
		if (strcmp(method, timeoutp->method) == 0) {
			/* find the match method name */
			*timeout_name = timeoutp->method_timeout;
			return (0);
		}
	}
	return (1);
}

/*
 * ************************************************************************
 *
 *      init_buf_addrs - initial the link list of buffer address
 *
 * ************************************************************************
 */
scha_err_t
init_buf_addrs(buf_info_t **buf_p, char *str_buf, scha_str_array_t *arr_buf,
    scha_status_value_t *status_buf, scha_extprop_value_t *ext_buf,
    scha_uint_array_t *uintarray_buf, const char *tag)
{

	buf_info_t	*bp;

	if ((bp = (buf_info_t *)malloc(sizeof (buf_info_t))) == NULL)
		return (SCHA_ERR_NOMEM);

	if (str_buf != NULL)
		bp->buf_addr = (char *)str_buf;
	else if (arr_buf != NULL)
		bp->buf_addr = (char *)arr_buf;
	else if (status_buf != NULL)
		bp->buf_addr = (char *)status_buf;
	else if (ext_buf != NULL)
		bp->buf_addr = (char *)ext_buf;
	else if (uintarray_buf != NULL)
		bp->buf_addr = (char *)uintarray_buf;
	else
		return (SCHA_ERR_INTERNAL);

	bp->next = NULL;
	bp->tag = (char *)tag;
	*buf_p = bp;
	return (SCHA_ERR_NOERR);
}

/*
 * ************************************************************************
 *
 *      add_buf_addrs - add the buffer to the last of the link list
 *
 * ************************************************************************
 */
scha_err_t
add_buf_addrs(buf_info_t *buf_p, char *str_buf, scha_str_array_t *arr_buf,
    scha_status_value_t *status_buf, scha_extprop_value_t *ext_buf,
    scha_uint_array_t *uintarray_buf, const char *tag)
{

	buf_info_t	*p, *bp;

	/*
	 * point to the last entity of the link list
	 */

	p = buf_p;

	while (p->next != NULL) {
		p = p->next;
	}

	if ((bp = (buf_info_t *)malloc(sizeof (buf_info_t))) == NULL)
		return (SCHA_ERR_NOMEM);

	if (str_buf != NULL)
		bp->buf_addr = (char *)str_buf;
	else if (arr_buf != NULL)
		bp->buf_addr = (char *)arr_buf;
	else if (status_buf != NULL)
		bp->buf_addr = (char *)status_buf;
	else if (ext_buf != NULL)
		bp->buf_addr = (char *)ext_buf;
	else if (uintarray_buf != NULL)
		bp->buf_addr = (char *)uintarray_buf;
	else
		return (SCHA_ERR_INTERNAL);

	/*
	 * update the buffer addr and set next pointer to NULL
	 */
	bp->next = NULL;
	bp->tag = (char *)tag;

	/*
	 * update the previous pointer
	 */
	p->next = bp;

	return (SCHA_ERR_NOERR);
}

/*
 * ************************************************************************
 *
 *      free_buf_addr - free all allocated buffers that in the link list
 *
 * ************************************************************************
 */
scha_err_t
free_buf_addrs(buf_info_t *buf_p)
{

	buf_info_t		*p, *next_p;
	scha_err_t		err;

	if (buf_p == NULL)
		return (SCHA_ERR_NOERR);
	else
		p = buf_p;

	while (p != NULL) {
		/*
		 * save the next pointer
		 */
		next_p = p->next;
		if ((err = free_buf(p)) == SCHA_ERR_NOERR) {
			if (p != NULL) {
				free(p);
			}
		}
		else
			return (err);
		p = next_p;
	}

	return (SCHA_ERR_NOERR);

}

/*
 * ************************************************************************
 *
 *      free_buf - free a buffer.  There are five buffer structures are
 *		   allocated by scha_*_get functions.  Each buffer has
 *		   different size and data type.
 *
 * ************************************************************************
 */
scha_err_t
free_buf(buf_info_t *buf)
{

	scha_tag_t		*tag_prop = NULL;
	scha_prop_type_t	type;
	scha_status_value_t	*status_buf;
	scha_extprop_value_t	*ext_buf;
	scha_uint_array_t	*uintarray_buf;
	scha_str_array_t	*arr_buf;
	uint_t			i;

	if (get_matched_tag(buf->tag, &tag_prop)) {
		return (SCHA_ERR_INTERNAL);
	}

	type = tag_prop->type;
	if ((strcasecmp(buf->tag, SCHA_STATUS) == 0) ||
	    (strcasecmp(buf->tag, SCHA_STATUS_NODE)) == 0) {
		status_buf = (scha_status_value_t *)buf->buf_addr;
		if (status_buf != NULL) {
			if (status_buf->status_msg != NULL) {
				free(status_buf->status_msg);
			}
			free((char *)status_buf);
		}
	} else if (strcasecmp(buf->tag, SCHA_EXTENSION) == 0 ||
	    strcasecmp(buf->tag, SCHA_EXTENSION_NODE) == 0) {
		if ((ext_buf = (scha_extprop_value_t *)buf->buf_addr)
		    != NULL) {
			extprop_free(ext_buf);
		}
	} else if (type == SCHA_PTYPE_STRING) {
		if (buf->buf_addr != NULL) {
			free(buf->buf_addr);
		}
	} else if (type == SCHA_PTYPE_UINTARRAY) {
		if ((uintarray_buf = (scha_uint_array_t *)buf->buf_addr)
		    != NULL) {
			free(uintarray_buf->uint_array);
		}
		free(uintarray_buf);
	} else if (type == SCHA_PTYPE_STRINGARRAY) {
		if ((arr_buf = (scha_str_array_t *)buf->buf_addr) != NULL) {
			if (!arr_buf->is_ALL_value) {
				for (i = 0; i < arr_buf->array_cnt; i++) {
					if (arr_buf->str_array[i] != NULL)
						free(arr_buf->str_array[i]);
				}
				free(arr_buf->str_array);
			}
			free(arr_buf);
		}
	}
	return (SCHA_ERR_NOERR);
}

/*
 * convert_ext_buf()
 *
 *	put the returned extension tag value into the user buffer
 *
 */
scha_err_t
convert_ext_buf(scha_prop_type_t type, scha_str_array_t *buf,
    scha_extprop_value_t **ext_buf)
{
	char		*endp;

	if (buf == NULL) {
		(*ext_buf) = NULL;
		return (SCHA_ERR_NOERR);
	}

	*ext_buf = (scha_extprop_value_t *)malloc
	    (sizeof (scha_extprop_value_t));
	if (*ext_buf == NULL)
		return (SCHA_ERR_NOMEM);


	switch (type) {
	case SCHA_PTYPE_INT :
		if (buf->str_array == NULL) {
			return (SCHA_ERR_INTERNAL);
		}
		if (buf->str_array[0] != NULL) {
			(*ext_buf)->val.val_int = (int)strtol(buf->str_array[0],
			    &endp, 0);
			if (*endp != '\0') {
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				free(*ext_buf);
				(*ext_buf) = NULL;
				return (SCHA_ERR_INTERNAL);
			}
			free(buf->str_array[0]);
		} else {
			(*ext_buf)->val.val_int = (int)NULL;
		}
		free(buf->str_array);
		free(buf);
		break;

	case SCHA_PTYPE_BOOLEAN :
		if (buf->str_array == NULL) {
			return (SCHA_ERR_INTERNAL);
		}
		if (buf->str_array[0] != NULL) {
			if ((strcmp(buf->str_array[0], SCHA_FALSE)) == 0)
				(*ext_buf)->val.val_boolean = B_FALSE;
			else if ((strcmp(buf->str_array[0], SCHA_TRUE)) == 0)
				(*ext_buf)->val.val_boolean = B_TRUE;
			else {
				free(*ext_buf);
				(*ext_buf) = NULL;
			}
			free(buf->str_array[0]);
		} else {
			free(*ext_buf);
			(*ext_buf) = NULL;
		}
		free(buf->str_array);
		free(buf);
		break;

	case SCHA_PTYPE_STRING :
		if (buf->str_array == NULL) {
			return (SCHA_ERR_INTERNAL);
		}
		if (buf->str_array[0] != NULL) {
			(*ext_buf)->val.val_str = buf->str_array[0];
		} else {
			(*ext_buf)->val.val_str = (char *)NULL;
		}
		free(buf->str_array);
		free(buf);
		break;

	case SCHA_PTYPE_STRINGARRAY :
		(*ext_buf)->val.val_strarray = (scha_str_array_t *)buf;
		break;

	case SCHA_PTYPE_ENUM :
		if (buf->str_array == NULL) {
			return (SCHA_ERR_INTERNAL);
		}
		if (buf->str_array[0] != NULL) {
			(*ext_buf)->val.val_enum = buf->str_array[0];
		} else {
			(*ext_buf)->val.val_enum = (char *)NULL;
		}
		free(buf->str_array);
		free(buf);
		break;

	default:
		free(*ext_buf);
		(*ext_buf) = NULL;
		return (SCHA_ERR_INTERNAL);

	}
	return (SCHA_ERR_NOERR);
}

/*
 * save_rs_user_buf()
 *
 *	Save the buffer which allocated from each scha_resource_get call
 *	They will be deallocated by scha_resource_close call.
 *
 */
scha_err_t
save_rs_user_buf(scha_rshandle_t *rhandle, const char *rs_tag, char *str_buf,
    scha_str_array_t *array_buf, scha_status_value_t *status_buf,
    scha_extprop_value_t *extprop_buf)
{

	scha_err_t	err;

	err = SCHA_ERR_NOERR;
	if (str_buf != NULL) {
		if (rhandle->buf_addrs == NULL)
			err = init_buf_addrs(&rhandle->buf_addrs,
				str_buf, NULL, NULL, NULL, NULL, rs_tag);
		else
			err = add_buf_addrs(rhandle->buf_addrs, str_buf,
				NULL, NULL, NULL, NULL, rs_tag);
	} else if (array_buf != NULL) {
		if (rhandle->buf_addrs == NULL)
			err = init_buf_addrs(&rhandle->buf_addrs, NULL,
				array_buf, NULL, NULL, NULL, rs_tag);
		else
			err = add_buf_addrs(rhandle->buf_addrs, NULL,
				array_buf, NULL, NULL, NULL, rs_tag);
	} else if (status_buf != NULL) {
		if (rhandle->buf_addrs == NULL)
			err = init_buf_addrs(&rhandle->buf_addrs, NULL,
				NULL, status_buf, NULL, NULL, rs_tag);
		else
			err = add_buf_addrs(rhandle->buf_addrs, NULL,
				NULL, status_buf, NULL, NULL, rs_tag);
	} else if (extprop_buf != NULL) {
		if (rhandle->buf_addrs == NULL)
			err = init_buf_addrs(&rhandle->buf_addrs, NULL,
				NULL, NULL, extprop_buf, NULL, rs_tag);
		else
			err = add_buf_addrs(rhandle->buf_addrs, NULL,
				NULL, NULL, extprop_buf, NULL, rs_tag);
	}
	return (err);
}

/*
 * save_rt_user_buf()
 *
 *	Save the buffer which allocated from each scha_resourcetype_get call
 *	They will be deallocated by scha_resourcetype_close call.
 *
 */
scha_err_t
save_rt_user_buf(scha_rthandle_t *rthandle, const char *rt_tag,
    char *str_buf, scha_str_array_t *array_buf)
{

	scha_err_t	err;

	err = SCHA_ERR_NOERR;
	if (str_buf != NULL) {
		if (rthandle->buf_addrs == NULL)
			err = init_buf_addrs(&rthandle->buf_addrs,
				str_buf, NULL, NULL, NULL, NULL, rt_tag);
		else
			err = add_buf_addrs(rthandle->buf_addrs, str_buf,
				NULL, NULL, NULL, NULL, rt_tag);
	} else if (array_buf != NULL) {
		if (rthandle->buf_addrs == NULL)
			err = init_buf_addrs(&rthandle->buf_addrs, NULL,
				array_buf, NULL, NULL, NULL, rt_tag);
		else
			err = add_buf_addrs(rthandle->buf_addrs, NULL,
				array_buf, NULL, NULL, NULL, rt_tag);
	}
	return (err);
}

/*
 * save_rg_user_buf()
 *
 *	Save the buffer which allocated from each scha_resourcegroup_get call
 *	They will be deallocated by scha_resourcegroup_close call.
 *
 */
scha_err_t
save_rg_user_buf(scha_rghandle_t *rghandle, const char *rg_tag,
    char *str_buf, scha_str_array_t *array_buf)
{

	scha_err_t	err;

	err = SCHA_ERR_NOERR;
	if (str_buf != NULL) {
		if (rghandle->buf_addrs == NULL)
			err = init_buf_addrs(&rghandle->buf_addrs,
				str_buf, NULL, NULL, NULL, NULL, rg_tag);
		else
			err = add_buf_addrs(rghandle->buf_addrs, str_buf,
				NULL, NULL, NULL, NULL, rg_tag);
	} else if (array_buf != NULL) {
		if (rghandle->buf_addrs == NULL)
			err = init_buf_addrs(&rghandle->buf_addrs, NULL,
				array_buf, NULL, NULL, NULL, rg_tag);
		else
			err = add_buf_addrs(rghandle->buf_addrs, NULL,
				array_buf, NULL, NULL, NULL, rg_tag);
	}
	return (err);
}

/*
 * save_rg_user_buf()
 *
 *	Save the buffer which allocated from each scha_cluster_get call
 *	They will be deallocated by scha_cluster_close call.
 *
 */
scha_err_t
save_cl_user_buf(scha_clhandle_t *clhandle, const char *cl_tag, char *str_buf,
    scha_str_array_t *strarray_buf,
    scha_uint_array_t *uintarray_buf)
{

	scha_err_t	err;

	err = SCHA_ERR_NOERR;

	if (str_buf != NULL) {
		if (clhandle->buf_addrs == NULL)
			err = init_buf_addrs(&clhandle->buf_addrs,
				str_buf, NULL, NULL, NULL, NULL, cl_tag);
		else
			err = add_buf_addrs(clhandle->buf_addrs, str_buf,
				NULL, NULL, NULL, NULL, cl_tag);
	} else if (strarray_buf != NULL) {
		if (clhandle->buf_addrs == NULL)
			err = init_buf_addrs(&clhandle->buf_addrs, NULL,
				strarray_buf, NULL, NULL, NULL, cl_tag);
		else
			err = add_buf_addrs(clhandle->buf_addrs, NULL,
				strarray_buf, NULL, NULL, NULL, cl_tag);
	} else if (uintarray_buf != NULL) {
		if (clhandle->buf_addrs == NULL)
			err = init_buf_addrs(&clhandle->buf_addrs, NULL,
				NULL, NULL, NULL, uintarray_buf, cl_tag);
		else
			err = add_buf_addrs(clhandle->buf_addrs, NULL,
				NULL, NULL, NULL, uintarray_buf, cl_tag);
	}
	return (err);
}


#if DOOR_IMPL
void
set_error(char *action, int ret)
{
	sc_syslog_msg_handle_t handle;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_SCHA_TAG, "");
	(void) fprintf(stderr, (const char *) dgettext(TEXT_DOMAIN,
	    "%s: Call failed, return code=%d\n"), action, ret);
	(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
	    "%s: Call failed, return code=%d", action, ret);
	sc_syslog_msg_done(&handle);
}
#else
void
set_error(CLIENT *clnt, char *action, int ret)
{
	char *clsp = NULL;
	sc_syslog_msg_handle_t handle;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	clsp = clnt_sperror(clnt, action);
	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_SCHA_TAG, "");
	if (clsp != NULL) {
		(void) fprintf(stderr, (const char *) dgettext(TEXT_DOMAIN,
		    "Call failed: %s\n"), clsp);
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Call failed: %s", clsp);
	} else {
		(void) fprintf(stderr, (const char *) dgettext(TEXT_DOMAIN,
		    "%s: Call failed, return code=%d\n"), action, ret);
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "%s: Call failed, return code=%d", action, ret);
	}
	sc_syslog_msg_done(&handle);
}
#endif


/*
 * scha_strerror()
 *
 * This routine translates the given scha_err_t error code to an
 * appropriate, but terse, error message.
 * Note that the char * string returned by this routine is *not*
 * internationalized, as its return value is to be used by the data
 * services for logging to the syslog.
 * Also note that if ever a change is made to this routine, please
 * ensure to make the same change in src/librgm/common/rgm_common.cc
 * file in routine rgm_error_msg.
 * There is a little duplication of code here, but there were three
 * reasons for it:
 * i) The string returned by this routine is meant for the
 * consumption of the data services for syslogging, and is therefore,
 * non-internationalized, whereas the string returned by rgm_error_msg
 * is meant to be printed to stderr by scrgadm and scswitch and,
 * therefore, has to be internationalized.
 * ii) We didn't want to make an RPC call to receptionist to retrieve
 * just the error message and, hence, introduce more failure modes.
 * iii) Also we didn't want to introduce another library just for this
 * purpose.
 * So this appeared to be a nice compromise.
 *
 */

char *
scha_strerror(scha_err_t err_code)
{
	uint_t i;

	for (i = 0; errmsg_mappings[i].errcode != ERRMSG_MAPPINGS_END; i++) {
		if (errmsg_mappings[i].errcode == err_code) {
			return (errmsg_mappings[i].errmsg);
		}
	}
	return (errmsg_mappings[i].errmsg);
}

/*
 * This routine translates the given scha_err_t error code to an
 * internationalized error message.
 *
 */
char *
scha_strerror_i18n(scha_err_t err_code)
{
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	return (dgettext(TEXT_DOMAIN, scha_strerror(err_code)));
}

/*
 * free char **list.
 */
void
strlist_free(strarr *list)
{
	int	i = 0;

	if (list == NULL)
		return;

	for (i = 0; list[i] != NULL; i++)
		free(list[i]);
	free(list);
}

/*
 * free scha_str_array_t *list
 */
void
strarr_free(scha_str_array_t *str_arr_list)
{
	if (str_arr_list == NULL)
		return;

	strlist_free(str_arr_list->str_array);
	free(str_arr_list);
}

void
extprop_free(scha_extprop_value_t *ext_buf)
{
	uint_t	i;

	switch (ext_buf->prop_type) {
	case SCHA_PTYPE_STRING:
		free(ext_buf->val.val_str);
		break;
	case SCHA_PTYPE_ENUM:
		free(ext_buf->val.val_enum);
		break;
	case SCHA_PTYPE_STRINGARRAY:
		for (i = 0; i < ext_buf->val.val_strarray->array_cnt; i++)
			free(ext_buf->val.val_strarray->str_array[i]);
		free(ext_buf->val.val_strarray->str_array);
		free(ext_buf->val.val_strarray);
		break;
	case SCHA_PTYPE_INT:
	case SCHA_PTYPE_BOOLEAN:
	case SCHA_PTYPE_UINTARRAY:
	case SCHA_PTYPE_UINT:
	default:
		break;
	}
	free(ext_buf);
}

scha_err_t
map_scha_err(scha_err_t err)
{
	switch (err) {
	/*
	 * The following are public error codes and therefore do *not*
	 * need to be mapped. Simply return the input value
	 */
	case SCHA_ERR_NOERR:
	case SCHA_ERR_NOMEM:
	case SCHA_ERR_HANDLE:
	case SCHA_ERR_INVAL:
	case SCHA_ERR_TAG:
	case SCHA_ERR_RECONF:
	case SCHA_ERR_SEQID:
	case SCHA_ERR_ACCESS:
	case SCHA_ERR_DEPEND:
	case SCHA_ERR_STATE:
	case SCHA_ERR_METHOD:
	case SCHA_ERR_NODE:
	case SCHA_ERR_RG:
	case SCHA_ERR_RT:
	case SCHA_ERR_RSRC:
	case SCHA_ERR_PROP:
	case SCHA_ERR_CHECKS:
	case SCHA_ERR_CLUSTER:
	case SCHA_ERR_ZONE_CLUSTER:
	case SCHA_ERR_ZC_DOWN:
	case SCHA_ERR_INTERNAL:
		return (err);

	/*
	 * The following are private SCHA_ERR_* error codes and therefore
	 * appropriately mapped.
	 */

	case SCHA_ERR_ORB:
		return (SCHA_ERR_INTERNAL);
	case SCHA_ERR_CCR:
		return (SCHA_ERR_INTERNAL);
	case SCHA_ERR_AUTH:
		return (SCHA_ERR_INTERNAL);
	case SCHA_ERR_RGRECONF:
		return (SCHA_ERR_RECONF);
	case SCHA_ERR_CLRECONF:
		return (SCHA_ERR_RECONF);
	case SCHA_ERR_FILE:
		return (SCHA_ERR_INVAL);
	case SCHA_ERR_DUPNODE:
		return (SCHA_ERR_NODE);
	case SCHA_ERR_MEMBER:
		return (SCHA_ERR_NODE);
	case SCHA_ERR_RGSTATE:
		return (SCHA_ERR_STATE);
	case SCHA_ERR_DETACHED:
		return (SCHA_ERR_STATE);
	case SCHA_ERR_RSTATE:
		return (SCHA_ERR_STATE);
	case SCHA_ERR_UPDATE:
		return (SCHA_ERR_PROP);
	case SCHA_ERR_VALIDATE:
		return (SCHA_ERR_METHOD);
	case SCHA_ERR_MEMBERCHG:
		return (SCHA_ERR_RECONF);
	case SCHA_ERR_INSTLMODE:
		return (SCHA_ERR_STATE);
	case SCHA_ERR_METHODNAME:
		return (SCHA_ERR_METHOD);
	case SCHA_ERR_STOPFAILED:
		return (SCHA_ERR_METHOD);
	case SCHA_ERR_NOMASTER:
		return (SCHA_ERR_CHECKS);
	case SCHA_ERR_TIMESTAMP:
		return (SCHA_ERR_INTERNAL);
	case SCHA_ERR_USAGE:
		return (SCHA_ERR_INVAL);
	case SCHA_ERR_DUPP:
		return (SCHA_ERR_PROP);
	case SCHA_ERR_PINT:
		return (SCHA_ERR_INVAL);
	case SCHA_ERR_PBOOL:
		return (SCHA_ERR_INVAL);
	case SCHA_ERR_PENUM:
		return (SCHA_ERR_INVAL);
	case SCHA_ERR_PSTR:
		return (SCHA_ERR_INVAL);
	case SCHA_ERR_PSARRAY:
		return (SCHA_ERR_INVAL);
	case SCHA_ERR_STARTFAILED:
		return (SCHA_ERR_METHOD);
	case SCHA_ERR_DELETE:
		return (SCHA_ERR_STATE);
	case SCHA_ERR_SWRECONF:
		return (SCHA_ERR_RECONF);
	case SCHA_ERR_TIMEOUT:
		return (SCHA_ERR_METHOD);
	case SCHA_ERR_NODEFAULT:
		return (SCHA_ERR_PROP);
	case SCHA_ERR_PINGPONG:
		return (SCHA_ERR_CHECKS);
	case SCHA_ERR_INVALID_REMOTE_AFF:
		return (SCHA_ERR_INVAL);
	case SCHA_ERR_INVALID_REMOTE_DEP:
		return (SCHA_ERR_INVAL);

	/*
	 * The following is a warning, hence return SCHA_ERR_NOERR
	 */
	case SCHA_ERR_RS_VALIDATE:
		return (SCHA_ERR_NOERR);

	/*
	 * The error code is out of range, return SCHA_ERR_INTERNAL
	 */
	default:
		return (SCHA_ERR_INTERNAL);

	}
}
