/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scha_rs_api.c	1.52	09/01/09 SMI"

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdarg.h>
#include "scha_lib.h"

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif
/*
 * FlexeLint doesn't understand va_arg().  The first occurrence of
 * va_arg() in a file produces FlexeLint errors e718, e747.  The
 * remaining occurrences produce e516.  We suppress all of them.
 * Lint also doesn't understand va_start(), which produces e40.
 */

static scha_err_t get_switch(const char *, const char *rs_name,
    const char *rg_name,
    const char *rs_tag, char *node_name, char **seq_id,
    scha_switch_t *rs_switch);
static scha_err_t get_state(const char *, const char *rs_name,
    const char *rg_name,
    const char *rs_tag, char *node_name, char **seq_id,
    scha_str_array_t **buf);
static scha_err_t get_status(const char *, const char *rs_name,
    const char *rg_name,
    const char *tag, char *node_name, char **seq_id,
    scha_status_value_t **rs_status);
static scha_err_t get_failed_status(const char *, const char *rs_name,
    const char *rg_name,
    const char *tag, char *node_name, char **seq_id, scha_str_array_t **buf);

/*
 * scha_resource_open()
 *
 *	This function accepts the resource name and resource group
 *	name(optional) as the arguments and, on success, returns a
 *	"handle" to be used by other resource access function
 *	(scha_resource_get).
 *
 *	This function calls scha_resource_open_zone which internally calls
 *	rs_open to send a RPC request to RGMD receptionist
 *	to get the RS sequence id from CCR and stores some information
 *	in the handle.
 *
 *	A NULL handle is returned with the error code if open fails.
 */
scha_err_t
scha_resource_open(const char *rs_name, const char *rg_name,
    scha_resource_t *handlep)
{
	/*
	 * NULL indicates the default cluster dictated by the zone in which
	 * the process is running.
	 */
	return (scha_resource_open_zone(NULL, rs_name, rg_name, handlep));
}

/*
 * scha_resource_open_zone()
 *
 *	This function accepts the cluster name, resource name and resource group
 *	name(optional) as the arguments and, on success, returns a
 *	"handle" to be used by other resource access function
 *	(scha_resource_get).  The 'cluster' argument may name any
 *	zone.  If it is a native or global zone, then queries are made
 *	in the global cluster.  If it is a 'cluster' branded zone, then
 *	queries are made in that zone cluster.
 *
 *	Based on the cluster name this function calls calls rs_open to send
 *	a RPC request to RGMD receptionist of that cluster
 *	to get the RS sequence id from CCR and stores some information
 *	in the handle.
 *
 *	If the cluster name is NULL then rs_open request is for the cluster
 *	on which the this call is executed.
 *	A NULL handle is returned with the error code if open fails.
 */
scha_err_t
scha_resource_open_zone(const char *cluster,
    const char *rs_name, const char *rg_name,
    scha_resource_t *handlep)
{
	char		*rs_sequence_id;
	char		*rt_sequence_id;
	char		*rt_name;
	scha_rshandle_t	*new_handle = NULL;
	int		err;

	if (rs_name == NULL || rs_name[0] == '\0')
		return (SCHA_ERR_RSRC);

	new_handle = (scha_rshandle_t *)calloc((size_t)1,
	    sizeof (scha_rshandle_t));
	if (new_handle == NULL) {
		return (SCHA_ERR_NOMEM);
	}

	bzero(new_handle, sizeof (scha_rshandle_t));

	/*
	 * send request through RPC call to get the rs sequence id
	 */
	err = rs_open(cluster, rs_name, rg_name, &rt_name, &rs_sequence_id,
	    &rt_sequence_id);
	if (err != SCHA_ERR_NOERR) {
		*handlep = NULL;
		free(new_handle);
		return (err);
	}

	if (rt_name == NULL || rs_sequence_id == NULL ||
	    rt_sequence_id == NULL) {
		*handlep = NULL;
		free(new_handle);
		return (SCHA_ERR_INTERNAL);
	}

	new_handle->version = strdup(SC30_API_VERSION);
	new_handle->rs_name = strdup(rs_name);
	if (rg_name == NULL)
		new_handle->rg_name = NULL;
	else
		new_handle->rg_name = strdup(rg_name);
	new_handle->rs_sequence_id = rs_sequence_id;
	new_handle->rt_sequence_id = rt_sequence_id;
	new_handle->rt_name = rt_name;
	new_handle->buf_addrs = NULL;

	*handlep = new_handle;
	return (SCHA_ERR_NOERR);
}



/*
 * scha_resource_get()
 *
 *	The scha_resource_get() function accepts the Resource handle, an API
 *	defined operation tag name (defined in scha.h file), and an extra
 *	argument for some tags that require it.
 *
 *	scha_resource_get_zone() takes an additional 'cluster' argument,
 *	which is expected to be a zonename.  If the zonename is "global"
 *	or identifies a native brand zone, then the query goes to the
 *	global cluster's rgmd.  If the zonename identifies a cluster
 *	branded zone, the query is directed to that cluster's rgmd.
 *	If the 'cluster' argument is NULL, the query goes to the local zone's
 *	rgmd, i.e., that of the zone in which the code is executing.
 *
 *	Both functions call the helper function scha_resource_get_helper_zone().
 *	This function calls rs_get to send a RPC request to the receptionist
 *	of the appropriate RGMD,
 *	to retrieve the value of the resource property indicated by the tag
 *	argument.
 *
 */

scha_err_t
scha_resource_get_helper_zone(const char *cluster,
    scha_resource_t handlep, const char *rs_tag, va_list ap)
{
	scha_rshandle_t 	*rhandle = handlep;
	scha_tag_t		*tag_prop = NULL;
	boolean_t		extra_arg;
	scha_op_t		op;
	scha_prop_type_t	type;
	scha_str_array_t	**array_buf = NULL;
	scha_str_array_t	*buf = NULL;
	char			**str_buf = NULL;
	boolean_t		*boolean_buf;
	boolean_t		swtch = B_FALSE;
	int			*int_buf = NULL;
	uint_t			*uint_buf = NULL;
	scha_status_value_t	**status_buf = NULL;
	scha_switch_t		*switch_buf = NULL;
	scha_extprop_value_t	**extprop_buf = NULL;
	scha_prop_type_t	rs_prop_type;
	char			*sub_tag = NULL;
	char			*rs_sequence_id = NULL;
	char			*rt_sequence_id = NULL;
	scha_err_t		err = SCHA_ERR_NOERR;
	int			i;
	char			*endp;
	char			*ext_node_name = NULL;
	char			*qtag = NULL;
	boolean_t		qarg = B_FALSE;

	/*
	 * If the handle is invalid, return with error
	 */
	if (rhandle == NULL ||
	    strcmp(rhandle->version, SC30_API_VERSION) != 0 ||
	    rhandle->rs_name == NULL || rhandle->rt_name == NULL) {
		return (SCHA_ERR_HANDLE);
	}

	/*
	 * Look through the tag_property table to find the matched tag name
	 * If can't find the match, return error
	 */
	if (get_matched_tag(rs_tag, &tag_prop)) {
		return (SCHA_ERR_TAG);
	}

	op = tag_prop->op;

	/*
	 * if this tag is not the RS or RT tag, return error
	 */
	switch (op) {
	case RS_GET:
	case RS_GET_EXT:
	case RS_GET_EXT_NAMES:
	case RS_GET_STATUS:
	case RS_GET_METHOD_T:
	case RS_GET_STATE:
	case RS_GET_SWITCH:
	case RS_GET_FAIL_STATUS:
	case RT_GET:
	case RT_GET_PATHNAME:
		/* these are valid operation codes */
		break;

	case RG_GET:
	case RG_GET_STATE:
	case CL_GET:
	default:
		/* all others are invalid operation codes */
		return (SCHA_ERR_TAG);
	}

	extra_arg = tag_prop->extra_arg;
	type = tag_prop->type;

	/*
	 * For SCHA_EXTENSION, SCHA_RESOURCE_STATE_NODE, SCHA_INIT_FAILED,
	 * SCHA_FINI_FAILED, SCHA_UPDATE_FAILED, SCHA_BOOT_FAILED,
	 * SCHA_STATUS_NODE, SCHA_ON_OFF_SWITCH_NODE and
	 * SCHA_MONITORED_SWITCH_NODE tags, we need to takes an extra
	 * argument(s) from the variable argument list.
	 */
	if (extra_arg) {
		/*
		 * suppressing FlexeLint complaints:
		 * Info(718) [c:45]: __builtin_va_arg_incr undeclared,
		 * assumed to return int
		 * Info(746) [c:45]: call to __builtin_va_arg_incr()
		 * not made in the presence of a prototype
		 */
		sub_tag = va_arg(ap, char *);	/*lint !e718 !e746 */
		if (sub_tag == NULL || sub_tag[0] == '\0') {
			err = SCHA_ERR_INVAL;
			goto errout;
		}
	}
	/*
	 * Based on the type of returned value, perform the following:
	 * 	. get the user buffer
	 * 	. call rs_get to send the request through RPC call
	 * 	. get the returned value of the tag from RPC call
	 *	  The RPC returned values are stored in an array of strings.
	 *	. based on the type of each tag, convert the RPC returned
	 *	  value(s) and stored them into the user buffer
	 * 	. save the returned buffer in the handle
	 *
	 * If there is an error, we free memory and jump out of the
	 * switch statement to errout in order to free memory allocated
	 * for sequence ids and to call va_end().
	 */
	switch (type) {
	case SCHA_PTYPE_INT:

		/*
		 * suppressing FlexeLint complaint e516:
		 * __builtin_va_arg_incr() has arg. type conflict
		 * (arg. no. 1 -- ptrs to incompatible types) with
		 * [the immediately preceding va_arg line]
		 */
		if ((int_buf = va_arg(ap, int *)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout;
		}

		*int_buf = 0;

		if (op == RS_GET)
			err = rs_get(cluster,
			    rhandle->rs_name, rhandle->rg_name,
			    rs_tag, NULL, &rs_sequence_id, &buf, qarg);
		else
			err = rt_get(cluster, rhandle->rt_name, rs_tag,
			    &rt_sequence_id, &buf);
		if (err != SCHA_ERR_NOERR)
			goto errout;

		if (buf == NULL)
			break;		/* no data was returned */

		if (buf->str_array[0] != NULL) {
			*int_buf = (int)strtol(buf->str_array[0], &endp, 0);
			if (*endp != '\0') {
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				err = SCHA_ERR_INTERNAL;
				goto errout;
			}
			free(buf->str_array[0]);
		}
		free(buf->str_array);
		free(buf);
		break;

	case SCHA_PTYPE_ENUM:


		switch (op) {
		case RS_GET_SWITCH:
			swtch = B_TRUE;
			if ((switch_buf =
			    va_arg(ap, scha_switch_t *)) ==
			    NULL) {	/*lint !e516 !e662 */
				err = SCHA_ERR_INVAL;
				goto errout;
			}

			if ((err = get_switch(cluster, rhandle->rs_name,
			    rhandle->rg_name, rs_tag, sub_tag,
			    &rs_sequence_id, switch_buf)) != SCHA_ERR_NOERR)
				goto errout;

			break;

		case RS_GET_STATE:
			if ((int_buf = va_arg(ap, int *)) ==
			    NULL) {	/*lint !e516 !e662 */
				err = SCHA_ERR_INVAL;
				goto errout;
			}

			*int_buf = 0;

			if ((err = get_state(cluster, rhandle->rs_name,
			    rhandle->rg_name, rs_tag, sub_tag,
			    &rs_sequence_id, &buf)) != SCHA_ERR_NOERR)
				goto errout;
			break;

		case RS_GET:
			if ((int_buf = va_arg(ap, int *)) ==
			    NULL) {	/*lint !e516 !e662 */
				err = SCHA_ERR_INVAL;
				goto errout;
			}

			*int_buf = 0;

			err = rs_get(cluster,
			    rhandle->rs_name, rhandle->rg_name, rs_tag,
			    NULL, &rs_sequence_id, &buf, qarg);
			if (err != SCHA_ERR_NOERR) {
				goto errout;
			}
			break;

		case RT_GET:
			if ((int_buf = va_arg(ap, int *)) ==
			    NULL) {	/*lint !e516 !e662 */
				err = SCHA_ERR_INVAL;
				goto errout;
			}

			*int_buf = 0;

			err = rt_get(cluster, rhandle->rt_name, rs_tag,
			    &rt_sequence_id, &buf);
			if (err != SCHA_ERR_NOERR) {
				goto errout;
			}
			break;

		case RS_GET_EXT:
		case RS_GET_EXT_NAMES:
		case RS_GET_STATUS:
		case RS_GET_METHOD_T:
		case RS_GET_FAIL_STATUS:
		case RT_GET_PATHNAME:
		case RG_GET:
		case RG_GET_STATE:
		case CL_GET:
		default:
			/*
			 * not an RS or RT SCHA_PTYPE_ENUM tag, so return error
			 */
			err = SCHA_ERR_TAG;
			goto errout;
		}

		if (swtch == B_FALSE) {
			if (buf == NULL)
				break;		/* no data was returned */

			if (buf->str_array[0] != NULL) {
				if (strcmp(buf->str_array[0], "NONE") == 0)
					*int_buf = SCHA_FOMODE_NONE;
				else if (strcmp(buf->str_array[0], "HARD") == 0)
					*int_buf = SCHA_FOMODE_HARD;
				else if (strcmp(buf->str_array[0], "SOFT") == 0)
					*int_buf = SCHA_FOMODE_SOFT;
				else if (strcmp(buf->str_array[0],
				    "RESTART_ONLY") == 0)
					*int_buf = SCHA_FOMODE_RESTART_ONLY;
				else if (strcmp(buf->str_array[0],
				    "LOG_ONLY") == 0)
					*int_buf = SCHA_FOMODE_LOG_ONLY;
				else {
					*int_buf =
					    (int)strtol(buf->str_array[0],
					    &endp, 0);
					if (*endp != '\0') {
						free(buf->str_array[0]);
						free(buf->str_array);
						free(buf);
						err = SCHA_ERR_INTERNAL;
						goto errout;
					}
				}
				free(buf->str_array[0]);
			}
			free(buf->str_array);
			free(buf);
		}
		break;

	case SCHA_PTYPE_STRING:

		if ((str_buf = va_arg(ap, char **)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout;
		}

		*str_buf = NULL;

		if (op == RS_GET)
			err = rs_get(cluster,
			    rhandle->rs_name, rhandle->rg_name,
			    rs_tag, NULL, &rs_sequence_id, &buf, qarg);
		else
			err = rt_get(cluster, rhandle->rt_name, rs_tag,
			    &rt_sequence_id, &buf);

		if (err != SCHA_ERR_NOERR)
			goto errout;

		if (buf == NULL)
			break;		/* no data was returned */

		if (((*str_buf) = buf->str_array[0]) != NULL) {
			/*
			 * Save the address of the returned data in the handle
			 * so a subsequent call to scha_resource_close()
			 * can free memory.
			 */
			err = save_rs_user_buf(rhandle, rs_tag,
			    *str_buf, NULL, NULL, NULL);
			if (err != SCHA_ERR_NOERR) {
				/*
				 * clean up memory and return error.
				 */
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				goto errout;
			}
		}
		/* free all but the string we're returning */
		free(buf->str_array);
		free(buf);

		break;

	case SCHA_PTYPE_STRINGARRAY:
		/*lint -e788 */
		switch (op) {
		case RS_GET_STATUS:
			if ((status_buf =
			    va_arg(ap, scha_status_value_t **)) ==
			    NULL) {	/*lint !e516 !e662 */
				err = SCHA_ERR_INVAL;
				goto errout;
			}

			*status_buf = NULL;

			if ((err = get_status(cluster, rhandle->rs_name,
			    rhandle->rg_name, rs_tag, sub_tag,
			    &rs_sequence_id, status_buf)) != SCHA_ERR_NOERR)
				goto errout;

			if (*status_buf == NULL)
				break;		/* no status was returned */

			/*
			 * Save the address of the returned data in the handle
			 * so a subsequent call to scha_resource_close()
			 * can free memory.
			 */
			err = save_rs_user_buf(rhandle, rs_tag, NULL,
			    NULL, *status_buf, NULL);
			if (err != SCHA_ERR_NOERR) {
				/*
				 * clean up memory and return error.
				 */
				free((*status_buf)->status_msg);
				free(*status_buf);
				goto errout;
			}
			break;

		case RS_GET_EXT:

			if ((strcasecmp(rs_tag, SCHA_EXTENSION_NODE) ==  0) &&
			    ((ext_node_name = va_arg(ap, char *))
			    == NULL)) {	/*lint !e516 !e662 !e661 */
				err = SCHA_ERR_INVAL;
				goto errout;
			}

			if ((extprop_buf =
			    va_arg(ap, scha_extprop_value_t **))
			    == NULL) {	/*lint !e516 !e662 */
				err = SCHA_ERR_INVAL;
				goto errout;
			}

			*extprop_buf = NULL;

			if ((err = rs_get_ext(cluster, rhandle->rs_name,
			    rhandle->rg_name, sub_tag, ext_node_name,
			    &rs_sequence_id, &rs_prop_type, &buf)) !=
			    SCHA_ERR_NOERR)
				goto errout;

			if ((err = convert_ext_buf(rs_prop_type, buf,
			    extprop_buf)) != SCHA_ERR_NOERR)
				goto errout;

			if ((*extprop_buf) == NULL)
				break;

			(*extprop_buf)->prop_type = rs_prop_type;
			/*
			 * Save the address of the returned data in the handle
			 * so a subsequent call to scha_resource_close()
			 * can free memory.
			 */
			err = save_rs_user_buf(rhandle, rs_tag, NULL,
			    NULL, NULL, *extprop_buf);
			if (err != SCHA_ERR_NOERR) {
				/*
				 * clean up memory and return error.
				 */
				extprop_free(*extprop_buf);
				goto errout;
			}
			break;

		case RS_GET_EXT_NAMES:
			if ((array_buf =
			    va_arg(ap, scha_str_array_t **))
			    == NULL) {	/*lint !e516 !e662 */
				err = SCHA_ERR_INVAL;
				goto errout;
			}

			*array_buf = NULL;

			if ((err = rs_get_all_extprops(
			    cluster, rhandle->rs_name,
			    rhandle->rg_name, &rs_sequence_id,
			    &buf)) != SCHA_ERR_NOERR)
				goto errout;

			if (buf == NULL)
				break;		/* no data was returned */

			*array_buf = buf;
			/*
			 * Save the address of the returned data in the handle
			 * so a subsequent call to scha_resource_close()
			 * can free memory.
			 */
			err = save_rs_user_buf(rhandle, rs_tag, NULL,
			    *array_buf, NULL, NULL);
			if (err != SCHA_ERR_NOERR) {
				/*
				 * clean up memory and return error.
				 */
				strarr_free(buf);
				goto errout;
			}

			break;

		case RS_GET:
		case RT_GET:
			if ((array_buf =
			    va_arg(ap, scha_str_array_t **))
			    == NULL) {	/*lint !e516 !e662 */
				err = SCHA_ERR_INVAL;
				goto errout;
			}

			if (op == RS_GET) {
				/*lint -e516 -e662 -e661 */
				/*lint +e516 +e662 +e661 */
				if (strcmp(rs_tag, SCHA_RESOURCE_DEPENDENCIES_Q)
				    == 0) {
					qarg = B_TRUE;
					qtag = strdup(
					    SCHA_RESOURCE_DEPENDENCIES);
				} else if (strcmp(rs_tag,
				    SCHA_RESOURCE_DEPENDENCIES_Q_WEAK) == 0) {
					qarg = B_TRUE;
					qtag = strdup(
					    SCHA_RESOURCE_DEPENDENCIES_WEAK);
				} else if (strcmp(rs_tag,
				    SCHA_RESOURCE_DEPENDENCIES_Q_RESTART)
				    == 0) {
					qarg = B_TRUE;
					qtag = strdup(
					    SCHA_RESOURCE_DEPENDENCIES_RESTART);
				} else if (strcmp(rs_tag,
				    /*CSTYLED*/
				    SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART)
				    == 0) {
					qarg = B_TRUE;
					qtag = strdup(
					    /*CSTYLED*/
					    SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART);
				} else {
					qtag = strdup(rs_tag);
				}
				err = rs_get(cluster, rhandle->rs_name,
				    rhandle->rg_name, qtag,
				    NULL, &rs_sequence_id, &buf, qarg);
				qarg = B_FALSE;
				free(qtag);

			} else	/* RT_GET */
				err = rt_get(cluster, rhandle->rt_name, rs_tag,
				    &rt_sequence_id, &buf);
			if (err != SCHA_ERR_NOERR)
				goto errout;

			if (buf == NULL)
				break;		/* no data was returned */

			*array_buf = buf;
			/*
			 * Save the address of the returned data in the handle
			 * so a subsequent call to scha_resource_close()
			 * can free memory.
			 */
			err = save_rs_user_buf(rhandle, rs_tag, NULL,
			    *array_buf, NULL, NULL);
			if (err != SCHA_ERR_NOERR) {
				/*
				 * clean up memory and return error.
				 */
				strarr_free(buf);
				goto errout;
			}

			break;

		/* these cases cannot happen with SCHA_PTYPE_STRINGARRAY */
		case RS_GET_METHOD_T:
		case RS_GET_STATE:
		case RS_GET_FAIL_STATUS:
		case RT_GET_PATHNAME:
		/* these cases were filtered out in the first switch, above */
		case RG_GET:
		case RG_GET_STATE:
		case CL_GET:
		default:
			err = SCHA_ERR_INTERNAL;
			goto errout;

		}
		/*lint +e788 */
		break;

	case SCHA_PTYPE_BOOLEAN:
		if ((boolean_buf = va_arg(ap, boolean_t *)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout;
		}

		*boolean_buf = (boolean_t)NULL;

		if (op == RS_GET_FAIL_STATUS) {
			err = get_failed_status(cluster, rhandle->rs_name,
			    rhandle->rg_name, rs_tag, sub_tag,
			    &rs_sequence_id, &buf);
		} else if (op == RS_GET ||
		    strcmp(rs_tag, SCHA_GLOBALZONE) == 0) {
				err = rs_get(cluster, rhandle->rs_name,
			    rhandle->rg_name, rs_tag,
			    NULL, &rs_sequence_id, &buf, qarg);
		} else {
				err = rt_get(cluster, rhandle->rt_name, rs_tag,
			    &rt_sequence_id, &buf);
		}
		if (err != SCHA_ERR_NOERR)
			goto errout;

		if (buf == NULL)
			break;		/* no data was returned */

		if (buf->str_array[0] != NULL) {
			/*
			 * For SCHA_IS_SHARED_ADDRESS and
			 * SCHA_IS_LOGICAL_HOSTNAME, the value is the
			 * enum value of the underlying Sysdefined_type
			 * property.  The enum value is translated to
			 * a boolean for the API.
			 */
			i = (int)strtol(buf->str_array[0], &endp, 0);
			if (*endp != '\0') {
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				err = SCHA_ERR_INTERNAL;
				goto errout;
			}
			switch (i) {
			case 1:
				*boolean_buf = B_TRUE;
				if ((strcmp(rs_tag,
				    SCHA_IS_SHARED_ADDRESS)) == 0)
					*boolean_buf = B_FALSE;
				break;
			case 2:
				if ((strcmp(rs_tag,
				    SCHA_IS_SHARED_ADDRESS)) == 0)
					*boolean_buf = B_TRUE;
				break;
			default:
				*boolean_buf = B_FALSE;
				break;
			}
			free(buf->str_array[0]);
		}
		free(buf->str_array);
		free(buf);
		break;

	case SCHA_PTYPE_UINT:

		if ((uint_buf =
		    va_arg(ap, uint_t *)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout;
		}

		*uint_buf = 0;

		if (op == RS_GET)
			err = rs_get(
			    cluster, rhandle->rs_name, rhandle->rg_name,
			    rs_tag, sub_tag, &rs_sequence_id, &buf, qarg);
		else
			err = rt_get(cluster, rhandle->rt_name, rs_tag,
			    &rt_sequence_id, &buf);
		if (err != SCHA_ERR_NOERR)
			goto errout;

		if (buf == NULL)
			break;		/* no data was returned */

		if (buf->str_array[0] != NULL) {
			*uint_buf = (uint_t)strtol(buf->str_array[0], &endp, 0);
			if (*endp != '\0') {
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				err = SCHA_ERR_INTERNAL;
				goto errout;
			}
			free(buf->str_array[0]);
		}
		free(buf->str_array);
		free(buf);
		break;

	case SCHA_PTYPE_UINTARRAY:
	default:
		err = SCHA_ERR_INTERNAL;
		goto errout;
	}

	if (rt_sequence_id && op == RT_GET) {
		/*
		 * Suppress sequence id check for all but SCHA_INSTALLED_NODES
		 */
		if (strcasecmp(rs_tag, SCHA_INSTALLED_NODES) == 0 &&
		    strcmp(rhandle->rt_sequence_id, (char *)rt_sequence_id)
		    != 0) {
			err = SCHA_ERR_SEQID;
		}
	} else {
		/*
		 * Tags (SCHA_GROUP, SCHA_TYPE, SCHA_RESOURCE_STATE,
		 * SCHA_RESOURCE_STATE_NODE, SCHA_STATUS, SCHA_STATUS_NODE,
		 * method failed state, or SCHA_NUM_{RESOURCE,RG}_RESTARTS)
		 * return dynamic information which is not stored in the CCR.
		 * Suppress sequence id check.
		 */
		if (rs_sequence_id &&
		    op != RS_GET_STATE && op != RS_GET_STATUS &&
		    op != RS_GET_FAIL_STATUS &&
		    strcasecmp(rs_tag, SCHA_GROUP) != 0 &&
		    strcasecmp(rs_tag, SCHA_TYPE) != 0 &&
		    strcasecmp(rs_tag, SCHA_NUM_RESOURCE_RESTARTS) != 0 &&
		    strcasecmp(rs_tag, SCHA_NUM_RG_RESTARTS) != 0) {
			if (strcmp(rhandle->rs_sequence_id,
			    (char *)rs_sequence_id) != 0) {
				err = SCHA_ERR_SEQID;
			}
		}
	}

errout:
	/*
	 * Since sequence id for both rt and rs is initialized to
	 * NULL and will be set only once, it's safe here to do
	 * blind free, even if sequence id never got allocated.
	 */
	free(rs_sequence_id);
	free(rt_sequence_id);

	va_end(ap);

	return (err);
}

scha_err_t
scha_resource_get(scha_resource_t handlep, const char *rs_tag, ...)
{
	va_list			ap;
	/*
	 * Initialize the variable arguments list
	 */
	va_start(ap, rs_tag);	/*lint !e40 !e26 !e50 !e10 */
	return (scha_resource_get_helper_zone(NULL, handlep, rs_tag, ap));
}

scha_err_t
scha_resource_get_zone(const char *cluster,
    scha_resource_t handlep, const char *rs_tag, ...)
{
	va_list			ap;
	/*
	 * Initialize the variable arguments list
	 */
	va_start(ap, rs_tag);	/*lint !e40 !e26 !e50 !e10 */
	return (scha_resource_get_helper_zone(cluster, handlep, rs_tag, ap));
}

/*
 * scha_resource_close()
 *
 *	This function deallocates the memory associated with the handle
 *
 */
scha_err_t
scha_resource_close(scha_resource_t handlep)
{

	scha_rshandle_t *rhandle = handlep;
	scha_err_t	err;

	err = SCHA_ERR_NOERR;
	if (rhandle == NULL ||
	    strcmp(rhandle->version, SC30_API_VERSION) != 0 ||
	    rhandle->rs_name == NULL || rhandle->rt_name == NULL) {
		return (SCHA_ERR_HANDLE);
	}

	err = free_rhandle(handlep);
	handlep = NULL;
	return (err);
}


/*
 * scha_resource_setstatus()
 *
 *	This function sets the resource's status and status messages in
 *	the cluster system log
 */
scha_err_t
scha_resource_setstatus_zone(const char *rs_name, const char *rg_name,
    const char *zonename, scha_rsstatus_t status, const char *msg)
{

	scha_err_t	err;

#if SOL_VERSION >= __s10
	char local_zone[ZONENAME_MAX + 1];
	zoneid_t zoneid;
#else
	char local_zone[1];	/* dummy placeholder */
#endif
	local_zone[0] = '\0';

	if (zonename != NULL) {
#if SOL_VERSION >= __s10
		/*
		 * Note, it is OK for zonename to be "global".  That will
		 * set status in the global zone of this node.
		 */
		if (strlcpy(local_zone, zonename, (ZONENAME_MAX + 1))
		    >= (ZONENAME_MAX + 1)) {
			return (SCHA_ERR_INVAL);
		}
#else
		return (SCHA_ERR_INVAL);
#endif
	} else {
#if SOL_VERSION >= __s10
		if ((zoneid = getzoneid()) < 0)
			return (SCHA_ERR_INTERNAL);
		if (getzonenamebyid(zoneid, local_zone, ZONENAME_MAX) < 0) {
			return (SCHA_ERR_INTERNAL);
		}
#endif
	}

	if (rs_name == NULL)
		return (SCHA_ERR_RSRC);

	if (rg_name == NULL || rg_name[0] == '\0')
		return (SCHA_ERR_RG);

	if ((status < SCHA_RSSTATUS_OK) || (status > SCHA_RSSTATUS_UNKNOWN))
		return (SCHA_ERR_RSTATUS);

	err = rs_set_status(rs_name, rg_name, (const char *)local_zone,
	    status, msg);
	return (err);
}

scha_err_t
scha_resource_setstatus(const char *rs_name, const char *rg_name,
    scha_rsstatus_t status, const char *msg) {
	return (scha_resource_setstatus_zone(rs_name, rg_name, NULL,
	    status, msg));
}

/*
 * get_switch()
 *
 *	Retrieve the On_off_switch/Monitored switch on the specified node
 */
scha_err_t
get_switch(const char *cluster, const char *rs_name, const char *rg_name,
    const char *tag, char *node_name, char **seq_id, scha_switch_t *rs_switch)
{

	char		host_name[MAXHOSTNAMELEN];
	scha_err_t	err;
	int		swtch;
	boolean_t	on_off = B_FALSE;

	/*
	 * Get local host name if the tag is SCHA_ON_OFF_SWITCH
	 * or SCHA_MONITORED_SWITCH.
	 */
	if (strcasecmp(tag, SCHA_ON_OFF_SWITCH) == 0 ||
	    strcasecmp(tag, SCHA_MONITORED_SWITCH) == 0) {
		/*
		 * For retrieving R On_off/Monitored switch on the local
		 * node, we will get the cluster nodename in receptionist
		 * code. Don't call 'gethostbyname' to get the local nodename,
		 * because it returns hostname instead of cluster nodename
		 */
		(void) strcpy(host_name, "");
	} else
		(void) strcpy(host_name, node_name);

	if (strcasecmp(tag, SCHA_ON_OFF_SWITCH) == 0 ||
	    strcasecmp(tag, SCHA_ON_OFF_SWITCH_NODE) == 0) {
		on_off = B_TRUE;
	}

	/*
	 * Get On_off/Monitored switch of a resource on the specified node
	 */
	err = rs_get_switch(cluster, rs_name, rg_name, host_name, seq_id,
	    on_off, &swtch);

	if (err == SCHA_ERR_NOERR)
		*rs_switch = swtch;

	return (err);
}

/*
 * get_status()
 *
 *	retrieve the specified RS status on the specified node
 */
scha_err_t
get_status(const char *cluster,
    const char *rs_name, const char *rg_name, const char *tag,
    char *node_name, char **seq_id, scha_status_value_t **rs_status)
{

	char		host_name[MAXHOSTNAMELEN];
	scha_err_t	err;
	int		status;
	char		*buf;
#if SOL_VERSION >= __s10
	char zonename[ZONENAME_MAX];
	zoneid_t zoneid;
#else
	char zonename[1];	/* dummy placeholder */
#endif
	zonename[0] = '\0';
	/*
	 * Get local host name if the tag is HASTATUS
	 */
	if (strcasecmp(tag, SCHA_STATUS) == 0) {
		/*
		 * For retrieving R FM status on the local node,
		 * we will get the cluster nodename in receptionist code.
		 * Don't call 'gethostbyname' to get the local nodename,
		 * because it returns hostname instead of cluster nodename
		 */
		(void) strcpy(host_name, "");
#if SOL_VERSION >= __s10
		if ((zoneid = getzoneid()) < 0)
			return (SCHA_ERR_INTERNAL);
		if (zoneid != 0 && getzonenamebyid(zoneid,
		    zonename, ZONENAME_MAX) < 0) {
			return (SCHA_ERR_INTERNAL);
		}
#endif

	} else
		(void) strcpy(host_name, node_name);

	/*
	 * Get state or status of a resource on the specified node
	 */
	err = rs_get_status(cluster,
	    rs_name, rg_name, host_name, zonename, seq_id,
	    &status, &buf);

	if (err != SCHA_ERR_NOERR)
		(*rs_status) = NULL;
	else {
		*rs_status = (scha_status_value_t *)
		    malloc(sizeof (scha_status_value_t));
		if (*rs_status == NULL)
			return (SCHA_ERR_NOMEM);

		(*rs_status)->status = status;
		if (buf != NULL)
			(*rs_status)->status_msg = buf;
		else
			(*rs_status)->status_msg = NULL;
	}

	return (err);
}

/*
 * get_failed_status()
 *
 *	retrieve the status when the FINI, INIT, BOOT or UPDATE method is
 *	called and returns a failed status or times out
 */
scha_err_t
get_failed_status(const char *cluster,
    const char *rs_name, const char *rg_name, const char *tag,
    char *node_name, char **seq_id, scha_str_array_t **buf)
{

	scha_err_t		err;
	char			method_name[MAXNAMELEN];


	if (strcasecmp(tag, SCHA_BOOT_FAILED) == 0)
		(void) strcpy(method_name, SCHA_BOOT);
	else if (strcasecmp(tag, SCHA_INIT_FAILED) == 0)
		(void) strcpy(method_name, SCHA_INIT);
	else if (strcasecmp(tag, SCHA_FINI_FAILED) == 0)
		(void) strcpy(method_name, SCHA_FINI);
	else if (strcasecmp(tag, SCHA_UPDATE_FAILED) == 0)
		(void) strcpy(method_name, SCHA_UPDATE);

	/*
	 * get a failed status of the UPDATE, INIT, FINI or BOOT method
	 */
	err = rs_get_failed_status(cluster,
	    rs_name, rg_name, method_name, node_name,
	    seq_id, buf);
	return (err);
}

/*
 * get_state()
 *
 *	retrieve the specified RS state on the specified node
 */
scha_err_t
get_state(const char *cluster, const char *rs_name, const char *rg_name,
    const char *rs_tag,
    char *node_name, char **seq_id, scha_str_array_t **buf)
{

	char		host_name[MAXHOSTNAMELEN];
	scha_err_t	err;
#if SOL_VERSION >= __s10
	char zonename[ZONENAME_MAX];
	zoneid_t zoneid;
#else
	char zonename[1];	/* dummy placeholder */
#endif
	zonename[0] = '\0';

	/*
	 * Get local host name if the tag is SCHA_RESOURCE_STATE
	 */
	if (strcasecmp(rs_tag, SCHA_RESOURCE_STATE) == 0) {
		/*
		 * For retrieving R state on the local node,
		 * we will get the cluster nodename in receptionist code.
		 * Don't call 'gethostbyname' to get the local nodename,
		 * because it returns hostname instead of cluster nodename
		 */
		(void) strcpy(host_name, "");
#if SOL_VERSION >= __s10
		if ((zoneid = getzoneid()) < 0)
			return (SCHA_ERR_INTERNAL);
		if (zoneid != 0 && getzonenamebyid(zoneid,
		    zonename, ZONENAME_MAX) < 0) {
			return (SCHA_ERR_INTERNAL);
		}
#endif

	} else
		(void) strcpy(host_name, node_name);

	/*
	 * Get state or status of a resource on the specified node
	 */
	err = rs_get_state(cluster,
	    rs_name, rg_name, host_name, zonename, seq_id, buf);

	return (err);
}
