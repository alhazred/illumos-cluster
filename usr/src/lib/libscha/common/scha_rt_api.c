/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)scha_rt_api.c	1.29	09/01/09 SMI"

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <sys/types.h>

#include <stdarg.h>
#include <scha_err.h>
#include "scha_lib.h"

/*
 * FlexeLint doesn't understand va_arg().  The first occurrence of
 * va_arg() in a file produces FlexeLint errors e718, e747.  The
 * remaining occurrences produce e516.  We suppress all of them.
 * Lint also doesn't understand va_start(), which produces e40.
 */

static scha_err_t get_is_pernode_prop(const char *cluster, const char *rs_name,
    const char *tag, char *prop_name, char **seq_id, scha_str_array_t **buf);

/*
 * scha_resourcetype_open()
 * scha_resourcetype_open_zone()
 *
 * 	scha_resourcetype_open() accepts the resource type name and
 * 	the arguments and, on success, returns a "handle" to be used by
 *	subsequent ha_resourcetype_get call.
 *	A NULL handle is returned with the error code if open fails.
 *
 *	scha_resourcetype_open_zone() takes a 'cluster' argument which is
 *	a zonename. Queries are executed in the context of the cluster
 *	(global cluster or zone cluster) to which that zone belongs.
 */
scha_err_t
scha_resourcetype_open(const char *rt_name, scha_resourcetype_t *handlep)
{
	return (scha_resourcetype_open_zone(NULL, rt_name, handlep));
}
scha_err_t
scha_resourcetype_open_zone(const char *cluster,
    const char *rt_name, scha_resourcetype_t *handlep)
{

	char		*rt_sequence_id;
	scha_rthandle_t	*new_handle = NULL;
	int		err;

	if (rt_name == NULL)
		return (SCHA_ERR_RT);

	new_handle = (scha_rthandle_t *)calloc((size_t)1,
	    sizeof (scha_rthandle_t));
	if (new_handle == NULL) {
		return (SCHA_ERR_NOMEM);
	}

	bzero(new_handle, sizeof (scha_rthandle_t));
	/*
	 * send request to RGM receptionist through RPC call to get the
	 * rt sequence id
	 */
	err = rt_open(cluster, rt_name, &rt_sequence_id);
	if (err != SCHA_ERR_NOERR) {
		*handlep = NULL;
		free(new_handle);
		return (err);
	}

	if (rt_sequence_id == NULL) {
		*handlep = NULL;
		free(new_handle);
		return (SCHA_ERR_INTERNAL);
	}

	new_handle->version = strdup(SC30_API_VERSION);
	new_handle->rt_name = strdup(rt_name);
	new_handle->rt_sequence_id = rt_sequence_id;
	new_handle->buf_addrs = NULL;

	*handlep = new_handle;
	return (SCHA_ERR_NOERR);
}

/*
 * scha_resourcetype_get()
 * scha_resourcetype_get_zone()
 *
 * 	scha_resourcetype_get() accepts the Resource Type management handle
 *	and an API defined operation tag name (defined in scha.h file).
 *
 *	This function calls rt_get to send a RPC request to receptionist RGMD
 *	to retrieve the value of the Resource Type property indicated by the tag
 *	argument.
 *
 *	scha_resourcetype_get_zone() takes a 'cluster' argument which is
 *	a zonename. Queries are executed in the context of the cluster
 *	(global cluster or zone cluster) to which that zone belongs.
 *
 *	Both of the above are implemented by the helper function
 *	scha_resourcetype_get_helper_zone().
 */

scha_err_t
scha_resourcetype_get_helper_zone(const char *cluster,
    scha_resource_t handlep, const char *rt_tag, va_list ap)
{
	scha_rthandle_t *rthandle = handlep;
	scha_tag_t	*tag_prop = NULL;
	scha_str_array_t *buf = NULL;
	scha_str_array_t **array_buf = NULL;
	char		**str_buf = NULL;
	int		*int_buf = NULL;
	boolean_t	*boolean_buf;
	scha_prop_type_t type;
	char		*rt_sequence_id = NULL;
	int		err = SCHA_ERR_NOERR;
	int		i;
	scha_op_t	op;
	char		*endp;
	char		*extra_arg = NULL;


	/* if the handle is invalid, return with error */
	if ((rthandle == NULL) ||
	    (strcmp(rthandle->version, SC30_API_VERSION) != 0)) {
		return (SCHA_ERR_HANDLE);
	}

	/*
	 * look through the tag_property table to find the matched tag name
	 * If can't find the match, return error
	 */
	if (get_matched_tag(rt_tag, &tag_prop)) {
		return (SCHA_ERR_TAG);
	}

	type = tag_prop->type;
	op = tag_prop->op;

	/*
	 * Based on the type of returned value, perform the following :
	 *	. qet the user buffer
	 *	. call rt_get to send the request through RPC call
	 *	. get the returned value of the tag from RPC call
	 *	  The RPC returned values are stored in a array of string.
	 *	. base on the type of each tag, convert the RPC returned
	 *	  value(s) and stored them into the user buffer
	 *	. save the returned buffer in the handle
	 *
	 * If there is an error, we free memory and jump out of the
	 * switch statement:
	 * errout1: calls va_end()
	 * errout2: frees rt_sequence_id then calls va_end()
	 */
	switch (type) {
	case SCHA_PTYPE_INT :
	case SCHA_PTYPE_ENUM :
		/*
		 * suppressing FlexeLint complaints:
		 * Info(718) [c:45]: __builtin_va_arg_incr undeclared,
		 * assumed to return int
		 * Info(746) [c:45]: call to __builtin_va_arg_incr()
		 * not made in the presence of a prototype
		 */
		if ((int_buf = va_arg(ap, int *))	/*lint !e718 !e746 */
		    == NULL) {
			err = SCHA_ERR_INVAL;
			goto errout1;
		}

		*int_buf = 0;

		if ((err = rt_get(cluster,
		    rthandle->rt_name, rt_tag, &rt_sequence_id,
		    &buf)) != SCHA_ERR_NOERR)
			goto errout1;

		if (buf == NULL)
			break;		/* no data was returned */

		if (buf->str_array[0] != NULL) {
			*int_buf = (int)strtol(buf->str_array[0], &endp, 0);
			if (*endp != '\0') {
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				err = SCHA_ERR_INTERNAL;
				goto errout2;
			}
			free(buf->str_array[0]);
		}
		free(buf->str_array);
		free(buf);
		break;

	case SCHA_PTYPE_STRING :
		/*
		 * suppressing FlexeLint complaint e516:
		 * __builtin_va_arg_incr() has arg. type conflict
		 * (arg. no. 1 -- ptrs to incompatible types) with
		 * [the immediately preceding va_arg line]
		 */
		if ((str_buf = va_arg(ap, char **)) == NULL) {	/*lint !e516 */
			err = SCHA_ERR_INVAL;
			goto errout1;
		}

		*str_buf = NULL;

		if ((err = rt_get(cluster,
		    rthandle->rt_name, rt_tag, &rt_sequence_id,
		    &buf)) != SCHA_ERR_NOERR)
			goto errout1;

		if (buf == NULL)
			break;		/* no data was returned */

		if (((*str_buf) = buf->str_array[0]) != NULL) {
			err = save_rt_user_buf(rthandle, rt_tag,
			    *str_buf, NULL);
			if (err != SCHA_ERR_NOERR) {
				/*
				 * clean up memory and return error.
				 */
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				goto errout2;
			}
		}
		free(buf->str_array);
		free(buf);
		break;

	case SCHA_PTYPE_STRINGARRAY :
		if ((array_buf =
		    va_arg(ap, scha_str_array_t **)) == NULL) {	/*lint !e516 */
			err = SCHA_ERR_INVAL;
			goto errout1;
		}

		*array_buf = NULL;

		if ((err = rt_get(cluster,
		    rthandle->rt_name, rt_tag, &rt_sequence_id,
		    &buf)) != SCHA_ERR_NOERR)
			goto errout1;

		if (buf == NULL)
			break;		/* no data was returned */

		*array_buf = buf;
		err = save_rt_user_buf(rthandle, rt_tag, NULL, *array_buf);
		if (err != SCHA_ERR_NOERR) {
			/*
			 * clean up memory and return error.
			 */
			strarr_free(buf);
			goto errout2;
		}
		break;

	case SCHA_PTYPE_BOOLEAN :
		if (op == IS_PER_NODE) {
			extra_arg = va_arg(ap, char *);	/*lint !e718 !e746 */
			if (extra_arg == NULL || extra_arg[0] == '\0') {
				err = SCHA_ERR_INVAL;
				goto errout1;
			}
		}

		if ((boolean_buf = va_arg(ap, boolean_t *)) == NULL) {
			err = SCHA_ERR_INVAL;
			goto errout1;
		}
		*boolean_buf = (boolean_t)NULL;

		if (op == IS_PER_NODE) {
			err = get_is_pernode_prop(cluster, rthandle->rt_name,
			    rt_tag, extra_arg, &rt_sequence_id, &buf);
		} else {
			err = rt_get(cluster, rthandle->rt_name, rt_tag,
			    &rt_sequence_id, &buf);
		}
		if (err != SCHA_ERR_NOERR)
			goto errout2;

		if (buf == NULL)
			break;		/* no data was returned */

		if (buf->str_array[0] != NULL) {
			/*
			 * For SCHA_IS_SHARED_ADDRESS and
			 * SCHA_IS_LOGICAL_HOSTNAME
			 * the value is the enum value of the underlying
			 * Sysdefined_type property.  The enum value
			 * is translated to a boolean for the API.
			 */
			i = (int)strtol(buf->str_array[0], &endp, 0);
			if (*endp != '\0') {
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				err = SCHA_ERR_INTERNAL;
				goto errout2;
			}
			switch (i) {
			case 1 :
				*boolean_buf = B_TRUE;
				if ((strcmp(rt_tag,
					SCHA_IS_SHARED_ADDRESS)) == 0)
					*boolean_buf = B_FALSE;
				break;
			case 2 :
				if ((strcmp(rt_tag,
					SCHA_IS_SHARED_ADDRESS)) == 0)
					*boolean_buf = B_TRUE;
				break;

			default :
				*boolean_buf = B_FALSE;
				break;
			}
			free(buf->str_array[0]);
		}
		free(buf->str_array);
		free(buf);
		break;

	case SCHA_PTYPE_UINTARRAY:
	case SCHA_PTYPE_UINT:
	default :
		err = SCHA_ERR_INTERNAL;
		goto errout1;
	}

	/*
	 * Suppress sequence id check for all but SCHA_INSTALLED_NODES.
	 */
	if (strcasecmp(rt_tag, SCHA_INSTALLED_NODES) == 0 &&
	    strcmp(rthandle->rt_sequence_id, (char *)rt_sequence_id) != 0) {
		err = SCHA_ERR_SEQID;
	}

errout2:
	free(rt_sequence_id);

errout1:
	va_end(ap);

	return (err);
}
scha_err_t
scha_resourcetype_get(scha_resource_t handlep, const char *rt_tag, ...)
{
	va_list		ap;
	/*
		initialize the variable arguments list
	*/
	va_start(ap, rt_tag);		/*lint !e26, !e50, !e10 */
	return (scha_resourcetype_get_helper_zone(NULL, handlep, rt_tag, ap));
}
scha_err_t
scha_resourcetype_get_zone(const char *cluster,
    scha_resource_t handlep, const char *rt_tag, ...)
{
	va_list		ap;
	/*
		initialize the variable arguments list
	*/
	va_start(ap, rt_tag);		/*lint !e26, !e50, !e10 */
	return (scha_resourcetype_get_helper_zone(
	    cluster, handlep, rt_tag, ap));
}

scha_err_t
get_is_pernode_prop(const char *cluster, const char *rt_name, const char *tag,
    char *prop_name, char **seq_id, scha_str_array_t **buf)
{
	scha_err_t		err;
	err = is_pernode_prop(cluster, rt_name, prop_name, seq_id, buf);
	return (err);
}

/*
 * scha_resourcetype_close()
 *
 * 	This function deallocates the memory associated with the handle
 *
 */
scha_err_t
scha_resourcetype_close(scha_resource_t handlep)
{

	scha_err_t	err;
	scha_rthandle_t *rthandle = handlep;

	err = SCHA_ERR_NOERR;

	/* if the handle is invalid, return with error */
	if ((rthandle == NULL) ||
	    (strcmp(rthandle->version, SC30_API_VERSION) != 0)) {
		return (SCHA_ERR_HANDLE);
	}

	err = free_rthandle(handlep);
	return (err);
}
