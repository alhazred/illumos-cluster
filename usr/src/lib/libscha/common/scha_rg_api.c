/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scha_rg_api.c	1.31	09/01/09 SMI"

#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <sys/types.h>

#include <unistd.h>
#include <stdarg.h>
#include <scha_err.h>
#include "scha_lib.h"

#include <syslog.h>

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

/*
 * FlexeLint doesn't understand va_arg().  The first occurrence of
 * va_arg() in a file produces FlexeLint errors e718, e747.  The
 * remaining occurrences produce e516.  We suppress all of them.
 * Lint also doesn't understand va_start(), which produces e40.
 */

static scha_err_t get_state(const char *cluster,
    const char *rg_name, const char *rg_tag,
    char *nodename, char **seq_id, scha_str_array_t **buf);

/*
 * scha_resourcegroup_open()
 * scha_resourcegroup_open_zone()
 *
 * 	scha_resourcegroup_open() accepts the resource group name and
 * 	the arguments and, on success, returns a "handle" to be used by
 *	subsequent ha_resourcegroup_get call.
 *	A NULL handle is returned with the error code if open fails.
 *
 *	scha_resourcegroup_open_zone() takes an extra zone argument. The
 *	handle is associated with the rgmd corresponding to that zone's cluster
 *	(either a ZC or the global cluster).  NULL indicates the cluster of
 *	the zone in which we're executing.
 */
scha_err_t
scha_resourcegroup_open(const char *rg_name, scha_resourcegroup_t *handlep)
{
	return (scha_resourcegroup_open_zone(NULL, rg_name, handlep));
}

scha_err_t
scha_resourcegroup_open_zone(const char *cluster,
    const char *rg_name, scha_resourcegroup_t *handlep)
{

	char		*rg_sequence_id;
	scha_rghandle_t	*new_handle = NULL;
	int		err;

	if (rg_name == NULL || rg_name[0] == '\0')
		return (SCHA_ERR_RG);

	new_handle = (scha_rghandle_t *)calloc((size_t)1,
	    sizeof (scha_rghandle_t));
	if (new_handle == NULL) {
		return (SCHA_ERR_NOMEM);
	}

	bzero(new_handle, sizeof (scha_rghandle_t));
	/*
	 * send request to RGM receptionist through RPC call to get the
	 * rg sequence id
	 */
	err = rg_open(cluster, rg_name, &rg_sequence_id);
	if (err != SCHA_ERR_NOERR) {
		*handlep = NULL;
		free(new_handle);
		return (err);
	}

	new_handle->version = strdup(SC30_API_VERSION);
	new_handle->rg_name = strdup(rg_name);
	new_handle->rg_sequence_id = rg_sequence_id;
	new_handle->buf_addrs = NULL;

	*handlep = new_handle;
	return (SCHA_ERR_NOERR);
}

/*
 * scha_resourcegroup_get()
 *
 * 	The scha_resourcegroup_get() function accepts the Resource Group
 *	management handle, an API defined operation tag name (defined in
 *	the scha.h file), and an extra argument for some tags that require it.
 *
 *	scha_resourcegroup_get_zone() takes an additional 'cluster' argument,
 *	which is expected to be a zonename.  If the zonename is "global"
 *	or identifies a native brand zone, then the query goes to the
 *	global cluster's rgmd.  If the zonename identifies a cluster
 *	branded zone, the query is directed to that cluster's rgmd.
 *	If the 'cluster' argument is NULL, the query goes to the local zone's
 *	rgmd, i.e., that of the zone in which the code is executing.
 *
 *	Both functions call the helper function scha_resource_get_helper_zone().
 *
 *	This function calls rg_get to send a RPC request to receptionist RGMD
 *	to retrieve the value of the Resource Group property indicated by
 *	the tag	argument.
 *
 */
scha_err_t
scha_resourcegroup_get_helper_zone(const char *cluster,
    scha_resourcegroup_t handlep, const char *rg_tag,
    va_list ap)
{
	scha_rghandle_t *rghandle = handlep;
	scha_tag_t	*tag_prop = NULL;
	scha_str_array_t *buf = NULL;
	scha_str_array_t **array_buf = NULL;
	char		**str_buf = NULL;
	int		*int_buf = NULL;
	boolean_t	*boolean_buf = NULL;
	scha_prop_type_t type;
	char		*sub_tag = NULL;
	boolean_t	extra_arg;
	char		*rg_sequence_id = NULL;
	scha_err_t	err = SCHA_ERR_NOERR;
	char		*endp;


	/* if the handle is invalid, return with error */
	if (rghandle == NULL || rghandle->rg_name == NULL ||
	    (strcmp(rghandle->version, SC30_API_VERSION) != 0)) {
		return (SCHA_ERR_HANDLE);
	}

	/*
	 * look through the tag_property table to find the matched tag name
	 * If can't find the match, return error
	 */
	if (get_matched_tag(rg_tag, &tag_prop)) {
		return (SCHA_ERR_TAG);
	}

	extra_arg = tag_prop->extra_arg;
	type = tag_prop->type;


	/*
	we need to takes extra argument(s) from the variable
	argument list
	*/
	if (extra_arg) {
		/*
		 * suppressing FlexeLint complaints:
		 * Info(718) [c:45]: __builtin_va_arg_incr undeclared,
		 * assumed to return int
		 * Info(746) [c:45]: call to __builtin_va_arg_incr()
		 * not made in the presence of a prototype
		 */
		sub_tag = va_arg(ap, char *);	/*lint !e718 !e746 */
		if (sub_tag[0] == '\0') {
			err = SCHA_ERR_INVAL;
			goto errout1;
		}
	}

	/*
	 * Based on the type of returned value, perform the folling :
	 *	. qet the user buffer
	 *	. call rg_get to send the request through RPC call
	 *	. get the returned value of the tag from RPC call
	 *	  The RPC returned values are stored in a array of string.
	 *	. base on the type of each tag, convert the RPC returned
	 *	  value(s) and stored them into the user buffer
	 *	. save the returned buffer in the handle
	 *
	 * If there is an error, we free memory and jump out of the
	 * switch statement:
	 * errout1: calls va_end()
	 * errout2: frees rg_sequence_id then calls va_end()
	 */
	switch (type) {
	case SCHA_PTYPE_INT :
	case SCHA_PTYPE_ENUM :
		/*
		 * suppressing FlexeLint complaint e516:
		 * __builtin_va_arg_incr() has arg. type conflict
		 * (arg. no. 1 -- ptrs to incompatible types) with
		 * [the immediately preceding va_arg line]
		 */
		/* int_buf will be the OUT parameter */
		if ((int_buf = va_arg(ap, int *)) ==
		    NULL) {	/*lint !516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1;
		}

		*int_buf = 0;

		if ((strcasecmp(rg_tag, SCHA_RG_STATE_NODE) == 0) ||
		    (strcasecmp(rg_tag, SCHA_RG_STATE) == 0)) {
			if ((err = get_state(cluster, rghandle->rg_name, rg_tag,
			    sub_tag, &rg_sequence_id, &buf)) != SCHA_ERR_NOERR)
				goto errout1;
		} else if ((err = rg_get(cluster, rghandle->rg_name, rg_tag,
		    &rg_sequence_id, &buf)) != SCHA_ERR_NOERR)
			goto errout1;

		if (buf == NULL)
			break;		/* no data was returned */

		if (buf->str_array[0] != NULL) {
			*int_buf = (int)strtol(buf->str_array[0], &endp, 0);
			if (*endp != '\0') {
				/*
				 * Dust off and return if we meet
				 * any problem here.
				 */
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				err = SCHA_ERR_INTERNAL;
				goto errout2;
			}
			free(buf->str_array[0]);
		}
		/* free everything because we're returning an int */
		free(buf->str_array);
		free(buf);

		break;

	case SCHA_PTYPE_STRING :
		/* str_buf will be the OUT parameter */
		if ((str_buf = va_arg(ap, char **)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1;
		}

		*str_buf = NULL;

		if ((err = rg_get(cluster, rghandle->rg_name, rg_tag,
		    &rg_sequence_id, &buf)) != SCHA_ERR_NOERR)
			goto errout1;

		if (buf == NULL)
			break;		/* no data was returned */

		if (((*str_buf) = buf->str_array[0]) != NULL) {
			/*
			 * Save the address of the returned data in the handle
			 * so a subsequent call to scha_resourcegroup_close()
			 * can free memory.
			 */
			err = (int)save_rg_user_buf(rghandle, rg_tag,
			    *str_buf, NULL);
			if (err != SCHA_ERR_NOERR) {
				/*
				 * clean up memory and return error.
				 */
				free(buf->str_array[0]);
				free(buf->str_array);
				free(buf);
				goto errout2;
			}
		}
		/* free all but the string we're returning */
		free(buf->str_array);
		free(buf);

		break;

	case SCHA_PTYPE_STRINGARRAY :
		/* array_buf will be the OUT parameter */
		if ((array_buf =
		    va_arg(ap, scha_str_array_t **)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1;
		}

		*array_buf = NULL;

		if ((err = rg_get(
		    cluster, rghandle->rg_name, rg_tag, &rg_sequence_id,
		    &buf)) != SCHA_ERR_NOERR)
			goto errout1;

		if (buf == NULL)
			break;		/* no data was returned */

		*array_buf = buf;
		/*
		 * Save the address of the returned data in the handle
		 * so a subsequent call to scha_resourcegroup_close()
		 * can free memory.
		 */
		err = save_rg_user_buf(rghandle, rg_tag, NULL, *array_buf);
		if (err != SCHA_ERR_NOERR) {
			/*
			 * clean up memory and return error.
			 */
			strarr_free(buf);
			goto errout2;
		}

		/* don't free anything; we return the whole stringarray */
		break;

	case SCHA_PTYPE_BOOLEAN :
		/* boolean_buf will be the OUT parameter */
		if ((boolean_buf =
		    va_arg(ap, boolean_t *)) ==
		    NULL) {	/*lint !e516 !e662 */
			err = SCHA_ERR_INVAL;
			goto errout1;
		}

		*boolean_buf = (boolean_t)NULL;

		if ((err = rg_get(cluster, rghandle->rg_name, rg_tag,
		    &rg_sequence_id, &buf)) != SCHA_ERR_NOERR)
			goto errout1;

		if (buf == NULL)
			break;		/* no data was returned */

		if (buf->str_array[0] != NULL) {
			if ((strcmp(buf->str_array[0], "0")) == 0)
				*boolean_buf = B_FALSE;
			else
				*boolean_buf = B_TRUE;
			free(buf->str_array[0]);
		}
		free(buf->str_array);
		free(buf);

		break;

	case SCHA_PTYPE_UINTARRAY:
	case SCHA_PTYPE_UINT:
	default :
		err = SCHA_ERR_INTERNAL;
		goto errout1;
	}

	/*
	 * If tag is RG_state or RG_state_node, we should suppress
	 * checking for SCHA_ERR_SEQID because
	 * RG state is dynamic info and is not stored in CCR table.
	 * And, they are not covered by any relevant sequence id number
	 * The exception is the UNMANAGED state, which is retrieved from
	 * the CCR; therefore we do not suppress tag checking when
	 * UNMANAGED state is returned.
	 *
	 * All RG tags other than RG_state and RG_state_node are
	 * returning information that is stored in the CCR.
	 */
	if (((strcasecmp(rg_tag, SCHA_RG_STATE) != 0) &&
	    (strcasecmp(rg_tag, SCHA_RG_STATE_NODE) != 0)) ||
	    ((*int_buf) == (int)SCHA_RGSTATE_UNMANAGED)) {
		if (strcmp(rghandle->rg_sequence_id,
		    (char *)rg_sequence_id) != 0) {
			err = SCHA_ERR_SEQID;
		}
	}

errout2:
	free(rg_sequence_id);

errout1:
	va_end(ap);

	return (err);
}
scha_err_t
scha_resourcegroup_get(scha_resourcegroup_t handlep, const char *rg_tag, ...)
{
	va_list		ap;
	/*
		initialize the variable arguments list
	*/
	va_start(ap, rg_tag);	/*lint !e40 !e26 !e50 !e10 */
	return (scha_resourcegroup_get_helper_zone(NULL, handlep, rg_tag, ap));
}
scha_err_t
scha_resourcegroup_get_zone(const char *cluster, scha_resourcegroup_t handlep,
    const char *rg_tag, ...)
{
	va_list		ap;
	/*
		initialize the variable arguments list
	*/
	va_start(ap, rg_tag);	/*lint !e40 !e26 !e50 !e10 */
	return (scha_resourcegroup_get_helper_zone(
	    cluster, handlep, rg_tag, ap));
}

/*
 * scha_resourcegroup_close()
 *
 * 	This function deallocates the memory associated with the handle
 *
 */
scha_err_t
scha_resourcegroup_close(scha_resourcegroup_t handlep)
{

	scha_err_t	err;
	scha_rghandle_t *rghandle = handlep;

	err = SCHA_ERR_NOERR;

	if (rghandle == NULL)
		return (SCHA_ERR_NOERR);

	/* if the handle is invalid, return with error */
	if (rghandle == NULL || rghandle->rg_name == NULL ||
	    (strcmp(rghandle->version, SC30_API_VERSION) != 0)) {
		return (SCHA_ERR_HANDLE);
	}

	err = free_rghandle(handlep);
	return (err);
}

/*
 * get_state()
 *
 *      retrieve the specified RG state on the specified node
 */
scha_err_t
get_state(const char *cluster, const char *rg_name,
    const char *rg_tag, char *nodename,
    char **seq_id, scha_str_array_t **buf)
{
	char	hostname[MAXHOSTNAMELEN];
	scha_err_t	err;
#if SOL_VERSION >= __s10
	char zonename[ZONENAME_MAX];
	zoneid_t zoneid;
#else
	char zonename[1];	/* dummy placeholder */
#endif
	zonename[0] = '\0';

	/*
	 * Get local host name if the tag is SCHA_RG_STATE
	 */

	/*
	 * we could move zonename calculation even deeper.
	 * see if that has advantages
	 */
	if (strcasecmp(rg_tag, SCHA_RG_STATE) == 0) {
		/*
		 * For retrieving Rg state on the local node,
		 * we will get the cluster nodename in receptionist code.
		 * Don't call 'gethostbyname' to get the local nodename,
		 * because it returns hostname instead of cluster nodename
		 */
		(void) strcpy(hostname, "");
#if SOL_VERSION >= __s10
		if ((zoneid = getzoneid()) < 0)
			return (SCHA_ERR_INTERNAL);
		if (zoneid != 0 && getzonenamebyid(zoneid,
		    zonename, ZONENAME_MAX) < 0) {
			return (SCHA_ERR_INTERNAL);
		}
#endif
	} else {
		(void) strcpy(hostname, nodename);
	}
	/*
	 * Get state or status of a resource group on the specified node
	 */
	err = rg_get_state(cluster, rg_name, hostname, zonename, seq_id, buf);

	return (err);
}
