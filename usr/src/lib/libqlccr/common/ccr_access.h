//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ccr_access.h
//

#ifndef	_CCR_ACCESS_H
#define	_CCR_ACCESS_H

#pragma ident	"@(#)ccr_access.h	1.8	08/07/24 SMI"

#include <ccr/persistent_table.h>
#include <ql_lib_log.h>

#define	CCR_DIR_PATH "/etc/cluster/ccr/global"
#define	CCR_DIRECTORY "/etc/cluster/ccr/global/directory"
#define	CCR_INFRASTRUCTURE "/etc/cluster/ccr/global/infrastructure"
#define	CCR_EPOCH "/etc/cluster/ccr/global/epoch"
#define	CCR_CLUSTER_DIRECTORY "/etc/cluster/ccr/cluster_directory"

//
// The presence of this file indicates that CCR transformation needs
// to be done. Hence the libqlccr library will also work in cluster
// mode when this file is present.
//
#define	QL_TRANSFORM_CCR_FILE "/etc/cluster/transform_ccr"

//
// The presence of this file indicates that QL should continue
// in live upgrade mode.
//
#define	QL_LU_CONTINUE	"/etc/cluster/ql_lu_continue"

#define	UNINITIALIZED_GENNUM -1000

#define	INSERT_MODE 1
#define	REMOVE_MODE 2
#define	UPDATE_MODE 3

#define	QL_LIB gettext("libqlccr")

//
// This class will be used to do directory operations on the ccr
// in quantum leap mode. All operations will be done only on the
// local copy of ccr. This runs in non cluster mode only.
//
class ccr_access_directory {
public:
	ccr_access_directory();
	~ccr_access_directory();

	int initialize();

	// To add an empty table in ccr
	int create_table(const char *);

	// To remove a table from the ccr
	int remove_table(const char *);

	// Will increment the directory file genum
	int increment_dir_gennum(int);

private:

	// This function will add/remove the table name ftom the directory
	int update_directory_file(const char *, bool);

	//
	// Called by create_table and remove_table to make sure that
	// the user is not attempting to add or remove the CCR metadata.
	//
	// XXX in the furture if more metadata files are added into CCR,
	// they should also be checked here.
	//
	bool check_tablename(const char *);

	//
	// This will store the initial gennum of directory file
	// before it is updated
	//
	int store_dir_gennum();

	// To check that the data members have been initialized
	void check_data_members();

	// gennum of the directory file
	long dir_gennum;

	// Pointer to the Quantum Leap log object.
	ql_lib_log *logp;


};


//
// This class will be used to perform read operations on an existing
// ccr table in quantum leap mode. All operations will be done only
// on the local copy of the ccr. This runs in non cluster mode only.
// This class is based on readonly_persistent_table
//
class ccr_access_readonly_table {
public:
	ccr_access_readonly_table();
	~ccr_access_readonly_table();
	int initialize(const char *);

	// Search for a specific key and return the data
	char *query_element(const char *);

	// rewind to beginning of table
	void atfirst();

	//
	// Return next element if there is one, else return
	// NULL, indicating the iteration is over.
	//
	table_element_t *next_element();

	// Returns the number of elements in the specified table
	int32_t get_num_elements();

	// Done with all read operations
	void close();

private:

	// To verify the checksum of a table
	int verify_checksum();

	// To check that the data members have been initialized
	void check_data_members();

	char *table_name;
	char *table_path;
	readonly_persistent_table ptab;

	// Pointer to the Quantum Leap log object.
	ql_lib_log *logp;


};

//
// This class will be used to update operations on an existing
// ccr table in quantum leap mode. All operations will be done only
// on the local copy of the ccr. This runs in non cluster mode only.
// This class is based on updatable_copy_impl
//
class ccr_access_updatable_table {
public:
	ccr_access_updatable_table();
	~ccr_access_updatable_table();

	int initialize(const char *);

	int add_element(const char *, const char *);
	int remove_element(const char *);
	int remove_all_elements();
	int update_element(const char *, const char *);

	// Will commit the updates and will increment the gennum
	int commit(int incr = 1);

private:

	//
	// This will be used to add, remove and update element
	// depending on the mode.
	// 1: INSERT  2:REMOVE 3: UPDATE
	//
	int modify_element(const char *, const char *, int);

	// This will store the initial gennum before the table was updated
	int store_gennum();

	// To check that the data members have been initialized
	void check_data_members();


	// The table name to be updated e.g. table_1
	char *table_name;

	//
	// The table path to be updated
	// e.g. /etc/cluster/ccr/table_1
	//
	char *table_path;

	//
	// The temp table path
	// e.g. /etc/cluster/ccr/table_1.temp
	//
	char *temp_table_path;

	//
	// The intent table path
	// All updates till the commit is stored here.
	// e.g. /etc/cluster/ccr/table_1.intent
	//
	char *intent_table_path;

	//
	// The commit table path
	// This file will be actually commiteed with proper
	// gennum and checksum.
	// e.g. /etc/cluster/ccr/table_1.commit
	//
	char *commit_table_path;

	// The initial gennum of the table
	long gennum;

	//
	// The update flag
	// false: No updates have occured
	// true: updates have occured
	//
	bool update_flag;

	// Pointer to the Quantum Leap log object.
	ql_lib_log *logp;


};

#endif /* _CCR_ACCESS_H */
