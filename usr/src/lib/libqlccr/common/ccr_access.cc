//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ccr_access.cc
//

#pragma ident	"@(#)ccr_access.cc	1.5	08/07/24 SMI"

#include <sys/os.h>
#include <ccr_access.h>
#include <libintl.h>
#include <sys/cladm_int.h>

ccr_access_directory::ccr_access_directory()
{
	logp = NULL;
	logp = ql_lib_log::get_instance();
	dir_gennum =  UNINITIALIZED_GENNUM;
}

ccr_access_directory::~ccr_access_directory() {} /*lint !e1540 */

//
// Will initialize the object.
// Currently the function stores the directory file gennum
// before it is edited.
// Will return 0 on success and 1 on error
int
ccr_access_directory::initialize()
{
	int retval = 0;
	int bootflags;

	// Get the reference to the log object
	if (logp == NULL) {
		logp = ql_lib_log::get_instance();
	}

	if ((cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) ||
	    !(bootflags & CLUSTER_BOOTED)) {
		// Node is not a cluster member
		// Do nothing
	} else {
		//
		// Check if CCR transformation needs to be done
		//
		if (access(QL_TRANSFORM_CCR_FILE, F_OK) != 0) {
			//
			// CCR transformation not required
			// Hence, do not allow cluster mode
			// CCR access.
			//
			(void) printf(gettext(
			    "Noncluster CCR access mode not allowed "
			    "in cluster mode when not supporting "
			    "live upgrade.\n"));
			logp->log_err(QL_LIB,
			    gettext("ccr_access_directory: "
			    "Noncluster CCR access mode not allowed "
			    "in cluster mode when not supporting "
			    "live upgrade.\n"));
			exit(ENOTSUP);
		} else {
			//
			// CCR transformation is required.
			// Allow, cluster mode CCR access.
			//
			logp->log_info(QL_LIB,
			    gettext("ccr_access_directory: "
			    "Will allow cluster mode CCR access\n"));
		}
	}

	// Store the directory gennum
	retval = store_dir_gennum();

	if (retval != 0) {
		// Could not store the directory file gennum
		// Log a error message
		logp->log_err(QL_LIB,
		    gettext("Failed to initialize ccr_access_directory"
		    "object\n"));
		return (1);
	}

	logp->log_info(QL_LIB, gettext("ccr_access_directory initialized\n"));

	return (0);
}

//
// This function will be used to create a new table in the ccr.
// It will create a new table file with gennum and checksum and
// add the new table entry in the directory file.
// If the table already exists, an error is returned.
//
int
ccr_access_directory::create_table(const char *table_name)
{
	updatable_persistent_table new_table;
	int retval = 0;
	char *newpath = NULL;
	char *intent_path = NULL;
	char cksm_text[CKSM_HEX_LEN + 1];

	// table name cannot be null
	ASSERT(table_name);

	if (table_name == NULL) {
		logp->log_err(QL_LIB,
		    gettext("create_table called with NULL tablename\n"));
		return (-1);
	}

	logp->log_info(QL_LIB,
	    gettext("create_table called with tablename %s\n"), table_name);

	// check if the table_name is a valid ccr table name
	if (check_tablename(table_name) == false) { /*lint !e731 */
		logp->log_err(QL_LIB,
		    gettext("The table name %s is invalid\n"), table_name);
		return (-1);
	}

	// make the new table path
	// e.g. /etc/cluster/ccr/table1
	newpath = new char[os::strlen(CCR_DIR_PATH) +
	    os::strlen(table_name) + 2];
	os::sprintf(newpath, "%s/%s", CCR_DIR_PATH, table_name);

	// make the temp_path
	// e.g. /etc/cluster/ccr/table1.commit
	intent_path = new char[os::strlen(CCR_DIR_PATH) +
		os::strlen(table_name) + 2 + 6];
	os::sprintf(intent_path, "%s/%s.intent", CCR_DIR_PATH, table_name);

	// create an empty new file
	if ((retval = new_table.initialize(intent_path)) != 0) {
		logp->log_err(QL_LIB,
		    gettext("create_table: Failed to initialize %s "
		    "Retval = %d\n"), intent_path, retval);
		(void) os::file_unlink(intent_path);
		delete [] newpath;
		delete [] intent_path;
		return (retval);
	}

	//
	// write meta entries
	//

	// insert gennum = 0
	if ((retval = new_table.insert_element("ccr_gennum", "0")) != 0) {
		logp->log_err(QL_LIB, gettext("create_table: "
		    "Failed to insert element ccr_gennum "
		    "Retval = %d\n"), retval);
		(void) new_table.close();
		(void) os::file_unlink(intent_path);
		delete [] newpath;
		delete [] intent_path;
		return (retval);
	}

	// insert ccr_checksum
	init_ccr_checksum(cksm_text);
	CL_PANIC(os::strlen(cksm_text) == CKSM_HEX_LEN);
	if ((retval =
	    new_table.insert_element("ccr_checksum", cksm_text)) != 0) {
		logp->log_err(QL_LIB, gettext("create_table: "
		    "Failed to insert element ccr_checksum "
		    "Retval = %d\n"), retval);
		(void) new_table.close();
		(void) os::file_unlink(intent_path);
		delete [] newpath;
		delete [] intent_path;
		return (retval);
	}

	//
	// insert the new table name in the directory file
	//
	if ((retval = update_directory_file(table_name, true)) != 0) {
		logp->log_err(QL_LIB,
		    gettext("create_table: Failed to update directory file "
		    "Retval = %d\n"), retval);
		(void) new_table.close();
		(void) os::file_unlink(intent_path);
		delete [] newpath;
		delete [] intent_path;
		return (retval);
	}

	(void) new_table.close();

	// Rename the intent table to the actual table
	if ((retval = os::file_rename(intent_path, newpath)) != 0) {
		logp->log_err(QL_LIB,
		    gettext("create_table: Failed to rename intent table to "
		    "actual table for %s  Retval = %d\n"), table_name, retval);
		(void) os::file_unlink(intent_path);
		delete [] newpath;
		delete [] intent_path;
		return (retval);

	}

	delete [] newpath;
	delete [] intent_path;

	logp->log_info(QL_LIB,
	    gettext("Create_table: Successfully created table %s\n"),
		    table_name);

	return (0);
} 
// End of ccr_access_directory::create_table


//
// This function will be used to remove a table from the ccr
// directory file.
//
int
ccr_access_directory::remove_table(const char *table_name)
{
	int retval = 0;

	// table_name cannot be null
	ASSERT(table_name);

	if (table_name == NULL) {
		logp->log_err(QL_LIB,
		    gettext("remove_table called with NULL tablename\n"));
		return (-1);
	}

	logp->log_info(QL_LIB,
	    gettext("remove_table called with table name %s\n"), table_name);

	// check if the table_name is a valid ccr table name
	if (check_tablename(table_name) == false) { /*lint !e731 */
		logp->log_err(QL_LIB,
		    gettext("The table name %s is invalid\n"), table_name);
		return (-1);
	}

	// remove the table name from the directory file
	if ((retval = update_directory_file(table_name, false)) != 0) {
		logp->log_err(QL_LIB,
		    gettext("remove_table: Failed to update directory file "
		    "Retval = %d\n"), retval);
		return (retval);
	}

	logp->log_info(QL_LIB,
	    gettext("remove_table: Successfully removed table %s\n"),
	    table_name);

	return (0);

}
// End of ccr_access_directory::remove_table


//
// This function will increment the gennum of the directory by incr.
// New gennum = original gennum + incr
// If the user has already added or deleted a table and wants to increase
// the directory file gennum by 1, then no need to call this function
// as the create_table, remove_table functions will increase the gennum.
// If the user wants to increase the directory file gennum by more than one
// after adding or deleting a file, then he should use this function and
// pass the desired increment as a parameter.
// This function should be called after all tables have been added and
// removed.
//
int
ccr_access_directory::increment_dir_gennum(int incr)
{
	updatable_persistent_table new_table;
	readonly_persistent_table orig_table;
	char *newpath;
	char gennum_str[CKSM_HEX_LEN + 1];
	char metastr[CKSM_HEX_LEN + 1];
	uchar_t cksm_digest[DIGEST_LEN];
	table_element_t *telp;
	int retval = 0;

	logp->log_info(QL_LIB,
	    gettext("increment_dir_gennum called with incr = %d\n"), incr);

	if (incr == 0) {
		// Nothing to do
		return (0);
	}

	// Open the original directory file
	if ((retval = orig_table.initialize(CCR_DIRECTORY)) != 0) {
		logp->log_err(QL_LIB, gettext("increment_dir_gennum: "
		    "Failed to open original directory file "
		    "Retval = %d\n"), retval);
		return (retval);
	}

	newpath = new char[os::strlen(CCR_DIRECTORY) + 5]; // room for suffix
	os::sprintf(newpath, "%s.new", CCR_DIRECTORY);

	// Make a temporary table file
	if ((retval = new_table.initialize(newpath)) != 0) {
		logp->log_err(QL_LIB, gettext("increment_dir_gennum: "
		    "Failed to make temporary file "
		    "Retval = %d\n"), retval);
		orig_table.close();
		delete [] newpath;
		return (retval);
	}

	// insert the gennum after incrementing it (gennum + incr)
	os::sprintf(gennum_str, "%ld\0", dir_gennum + incr);
	if ((retval = new_table.insert_element("ccr_gennum", gennum_str))
					!= 0) {
		logp->log_err(QL_LIB, gettext("increment_dir_gennum: "
		    "Failed to insert the gennum after incrementing it "
		    "Retval = %d\n"), retval);
		orig_table.close();
		(void) new_table.close();
		delete [] newpath;
		return (retval);

	}

	// insert the ccr_checksum
	if ((retval = orig_table.compute_checksum(cksm_digest)) != 0) {
		logp->log_err(QL_LIB, gettext("increment_dir_gennum: "
		    "Failed to compute checksum. Retval = %d\n"), retval);
		orig_table.close();
		(void) new_table.close();
		delete [] newpath;
		return (retval);
	}
	encode_ccr_checksum(cksm_digest, metastr);
	if ((retval = new_table.insert_element("ccr_checksum", metastr)) != 0) {
		logp->log_err(QL_LIB, gettext("increment_dir_gennum: "
		    "Failed to insert checksum. Retval = %d\n"), retval);
		orig_table.close();
		(void) new_table.close();
		delete [] newpath;
		return (retval);
	}

	// Copy the data elements
	orig_table.atfirst();
	while ((telp = orig_table.next_element(retval)) != NULL) {
		// ignore any stray metadata entries
		if (!is_meta_row(telp->key)) {
			if ((retval = new_table.insert_element(telp)) != 0) {
				logp->log_err(QL_LIB,
				    gettext("increment_dir_gennum: "
				    "Failed to add element "
				    "Retval = %d\n"), retval);
				orig_table.close();
				(void) new_table.close();
				delete telp;
				delete [] newpath;
				return (retval);
			}
		}
		delete telp;
	}

	// Close the directory file
	orig_table.close();

	// Close the directory.new file
	if ((retval = new_table.close()) != 0) {
		logp->log_err(QL_LIB, gettext("increment_dir_gennum: "
		    "Failed to close the file. Retval = %d\n"),
		    retval);
		delete [] newpath;
		return (retval);
	}

	// rename directory.new to directory
	retval = os::file_rename(newpath, (char *)CCR_DIRECTORY);

	if (retval != 0) {
		logp->log_err(QL_LIB, gettext("increment_dir_gennum: "
		    "Failed to rename the directory file "
		    "Retval = %d\n"), retval);
	} else {
		logp->log_info(QL_LIB,
		    gettext("Exiting increment_dir_gennum\n"));
	}
	delete [] newpath;
	return (retval);

}
// End  of ccr_access_directory::increment_dir_gennum


//
// This function will add/remove the new table name in the directory file
// The table name should be just the name e.g. table1 (not the path)
// uflag = true means insert the table name in directory file
// uflag = false means remove table name from directory file
// This is a private function.
//
int
ccr_access_directory::update_directory_file(const char *table_name, bool uflag)
{
	updatable_persistent_table new_table;
	readonly_persistent_table orig_table;
	char *commit_path;
	char *intent_path;
	char metastr[CKSM_HEX_LEN + 1];
	uchar_t cksm_digest[DIGEST_LEN];
	char gennum_str[CKSM_HEX_LEN + 1];
	int retval = 0;
	int found_flag = 0;
	table_element_t *telp;

	logp->log_info(QL_LIB, gettext("update_directory_file called with "
	    "Table name = %s and uflag = %d\n"), table_name, uflag);

	// table name can not be NULL
	ASSERT(table_name);

	if (table_name == NULL) {
		logp->log_err(QL_LIB,
		    gettext("update_directory_file called with NULL"
		    " tablename\n"));
		return (-1);
	}

	intent_path = new char[os::strlen(CCR_DIRECTORY) + 8];
	os::sprintf(intent_path, "%s.intent", CCR_DIRECTORY);

	// Open the ccr directory file
	if ((retval = orig_table.initialize(CCR_DIRECTORY)) != 0) {
		logp->log_err(QL_LIB, gettext("update_directory_file: "
		    "Failed to open the ccr directory file. "
		    "Retval = %d\n"), retval);
		delete [] intent_path;
		return (retval);
	}

	// Create the intent file
	if ((retval = new_table.initialize(intent_path)) != 0) {
		logp->log_err(QL_LIB, gettext("update_directory_file: "
		    "Failed to create the intent file "
		    "Retval = %d\n"), retval);
		orig_table.close();
		delete [] intent_path;
		return (retval);
	}

	// No need to insert ccr_gennum and ccr_checksum to the intent file

	// copy the contents of the old directory file to directory.intent
	orig_table.atfirst();
	while ((telp = orig_table.next_element(retval)) != NULL) {
		// Check if the table name already exists
		if (os::strcmp(telp->key, table_name) == 0) {
			if (uflag == true) { /*lint !e731 */
				// Table already exists. We are trying
				// to add it again. This is an error
				logp->log_err(QL_LIB,
				    gettext("update_directory_file: "
				    "Table %s already exists\n"),
				    table_name);
				orig_table.close();
				(void) new_table.close();
				(void) os::file_unlink(intent_path);
				delete [] intent_path;
				delete telp;
				return (-1);
			} else {
				// uflag is false
				// do not insert the name in directory.new
				delete telp;
				found_flag = 1;
				continue;
			}
		}
		// copy from directory to directory.intent
		// ignore any stray metadata entries
		if (!is_meta_row(telp->key)) {
			if ((retval = new_table.insert_element(telp)) != 0) {
				logp->log_err(QL_LIB,
				    gettext("update_directory_file: "
				    "Failed to insert element. "
				    "Retval = %d\n"), retval);
				orig_table.close();
				(void) new_table.close();
				(void) os::file_unlink(intent_path);
				delete [] intent_path;
				delete telp;
				return (retval);
			}
		}
		delete telp;
	}

	// close the original directory file
	orig_table.close();

	// The table to be removed is not present.
	// No change is required in the directory file.
	if ((uflag == false) && (found_flag == 0)) { /*lint !e731 */
		logp->log_err(QL_LIB, gettext("update_directory_file: "
		    "Table %s is not present\n"), table_name);
		(void) new_table.close();
		(void) os::file_unlink(intent_path);
		delete [] intent_path;
		return (-1);
	}

	// insert the new table name in directory.intent if we are
	// inserting a new table (uflag = true)
	if (uflag == true) { /*lint !e731 */
		if ((retval =
		    new_table.insert_element(table_name, NULL)) != 0) {
			logp->log_err(QL_LIB, gettext("update_directory_file: "
			    "Failed to insert table name in intent file "
			    "Retval = %d\n"), retval);
			orig_table.close();
			(void) new_table.close();
			(void) os::file_unlink(intent_path);
			delete [] intent_path;
			return (retval);
		}
	}

	// close the intent file
	if ((retval = new_table.close()) != 0) {
		logp->log_err(QL_LIB, gettext("update_directory_file: "
		    "Failed to close the intent file "
		    "Retval = %d\n"), retval);
		delete [] intent_path;
		return (retval);
	}

	//
	// Copy the intent file to commit file with proper checksum
	// The gennum will be set to original gennum + 1
	// Thus if you have added more than one update to the directory,
	// the gennum is incremented by 1
	//

	// Open the intent file
	if ((retval = orig_table.initialize(intent_path)) != 0) {
		logp->log_err(QL_LIB, gettext("update_directory_file: "
		    "Failed to open the intent file "
		    "Retval = %d\n"), retval);
		delete [] intent_path;
		return (retval);
	}

	// Create the commit file
	commit_path = new char[os::strlen(CCR_DIRECTORY) + 8];
	os::sprintf(commit_path, "%s.commit", CCR_DIRECTORY);
	if ((retval = new_table.initialize(commit_path)) != 0) {
		logp->log_err(QL_LIB, gettext("update_directory_file: "
		    "Failed to create the commit file "
		    "Retval = %d\n"), retval);
		orig_table.close();
		(void) os::file_unlink(commit_path);
		(void) os::file_unlink(intent_path);
		delete [] commit_path;
		delete [] intent_path;
		return (retval);
	}

	// Insert ccr_gennum
	// new gennum = original gennum + 1
	os::sprintf(gennum_str, "%ld\0", dir_gennum + 1);
	if ((retval = new_table.insert_element("ccr_gennum", gennum_str))
	    != 0) {
		logp->log_err(QL_LIB, gettext("update_directory_file: "
		    "Failed to insert ccr_gennum "
		    "Retval = %d\n"), retval);
		orig_table.close();
		(void) new_table.close();
		(void) os::file_unlink(commit_path);
		(void) os::file_unlink(intent_path);
		delete [] commit_path;
		delete [] intent_path;
		return (retval);
	}

	// Insert the ccr_checksum
	if ((retval = orig_table.compute_checksum(cksm_digest)) != 0) {
		logp->log_err(QL_LIB, gettext("update_directory_file: "
		    "Failed to compute checksum "
		    "Retval = %d\n"), retval);
		orig_table.close();
		(void) new_table.close();
		(void) os::file_unlink(commit_path);
		(void) os::file_unlink(intent_path);
		delete [] commit_path;
		delete [] intent_path;
		return (retval);
	}
	encode_ccr_checksum(cksm_digest, metastr);
	if ((retval = new_table.insert_element("ccr_checksum", metastr)) != 0) {
		logp->log_err(QL_LIB, gettext("update_directory_file: "
		    "Failed to insert ccr_checksum "
		    "Retval = %d\n"), retval);
		orig_table.close();
		(void) new_table.close();
		(void) os::file_unlink(commit_path);
		(void) os::file_unlink(intent_path);
		delete [] commit_path;
		delete [] intent_path;
		return (retval);
	}

	// Copy the contents of the intent file to the commit file
	orig_table.atfirst();
	while ((telp = orig_table.next_element(retval)) != NULL) {
		// ignore any stray metadata entries
		if (!is_meta_row(telp->key)) {
			if ((retval = new_table.insert_element(telp)) != 0) {
				logp->log_err(QL_LIB,
				    gettext("update_directory_file: "
				    "Failed to insert element "
				    "Retval = %d\n"), retval);
				orig_table.close();
				(void) new_table.close();
				(void) os::file_unlink(commit_path);
				(void) os::file_unlink(intent_path);
				delete [] commit_path;
				delete [] intent_path;
				delete telp;
				return (retval);
			}
		}
		delete telp;
	}

	// Close the intent file
	orig_table.close();

	// Close the commit file
	if ((retval = new_table.close()) != 0) {
		logp->log_err(QL_LIB, gettext("update_directory_file: "
		    "Failed to close the commit file "
		    "Retval = %d\n"), retval);
		delete [] commit_path;
		delete [] intent_path;
		return (retval);
	}

	// Delete the intent file
	(void) os::file_unlink(intent_path);

	// rename directory.commit to directory
	retval = os::file_rename(commit_path, (char *)CCR_DIRECTORY);
	if (retval != 0) {
		logp->log_err(QL_LIB, gettext("update_directory_file: "
		    "Failed to rename the directory file "
		    "Retval = %d\n"), retval);
	} else {
		logp->log_info(QL_LIB,
		    gettext("update_directory_file successful\n"));
	}
	delete [] commit_path;
	delete [] intent_path;
	return (retval);

}
// End of ccr_access_directory::update_directory_file


//
// This function checks that the table name entered is valid.
// It will prevent a CCR meta data table name or an empty table name.
// Will return true if the table name is valid else will return false.
// This is a private function
//
bool
ccr_access_directory::check_tablename(const char *table_name)
{

	logp->log_info(QL_LIB, "check_tablename called with %s\n",
	    table_name);

	// check if the size of table_name is zero.
	if (os::strlen(table_name) == 0) {
		logp->log_err(QL_LIB, gettext("check_tablename: "
		    "Size of table_name is zero\n"));
		return (false);
	}

	// check if the table_name starts with or contains an empty string
	if (os::strstr(table_name, " ") != NULL) {
		logp->log_err(QL_LIB, gettext("check_tablename: "
		    "Table name starts with or contains empty string\n"));
		return (false);
	}

	// Table name can't be directory
	if (os::strcmp("directory", table_name) == 0) {
		logp->log_err(QL_LIB, gettext("check_tablename: "
		    "Table name cannot be directory\n"));
		return (false);
	}

	// Table name can't be infrastructure
	if (os::strcmp("infrastructure", table_name) == 0) {
		logp->log_err(QL_LIB, gettext("check_table_name: "
		    "Table name cannot be infrastructure\n"));
		return (false);
	}

	// Table name can't be epoch
	if (os::strcmp("epoch", table_name) == 0) {
		logp->log_err(QL_LIB, gettext("check_table_name: "
		    "Table name cannot be epoch\n"));
		return (false);
	}

	//
	// Since keyword starts with "ccr_" is reserved for ccr metadata, we
	// will not allow any table with name beginning with "ccr_".
	//
	if (is_meta_row(table_name)) {
		logp->log_err(QL_LIB, gettext("check_table_name: "
		    "Table name cannot start with ccr_\n"));
		return (false);
	}

	logp->log_info(QL_LIB,
	    gettext("check_table_name: Valid table %s\n"), table_name);

	// The table name is valid
	return (true);
}
// End of ccr_access_directory::check_tablename

//
// Store the directory file gennum.
// This should be used before editing the directory file.
// This is a private function.
//
int
ccr_access_directory::store_dir_gennum()
{
	int retval = 0;
	readonly_persistent_table orig_table;
	table_element_t *telp;

	// Open the directory file
	if ((retval = orig_table.initialize(CCR_DIRECTORY)) != 0) {
		logp->log_err(QL_LIB, gettext("store_dir_gennum: "
		    "Failed to open the directory file. "
		    "Retval = %d\n"), retval);
		return (retval);
	}

	// Retrieve the gennum from the directory file
	orig_table.atfirst();
	while ((telp = orig_table.next_element(retval)) != NULL) {
		if (os::strcmp(telp->key, "ccr_gennum") == 0) {
			dir_gennum = atol(telp->data);
			delete telp;
			orig_table.close();
			logp->log_info(QL_LIB,
			    gettext("Successfully stored directory gennum\n"));
			return (0);
		}
		delete telp;
	}
	orig_table.close();

	//
	// Gennum not found in the directory table
	//
	logp->log_err(QL_LIB, gettext("store_dir_gennum: "
	    "Gennum not founf in the directory table\n"));
	return (1);
}


// To check that the data members have been initialized
// This is a private function
void
ccr_access_directory::check_data_members()
{
	ASSERT(dir_gennum != UNINITIALIZED_GENNUM);

}

//
// ---------------- ccr_access_readonly_table -----------------
//

ccr_access_readonly_table::ccr_access_readonly_table():
	table_name(NULL),
	table_path(NULL)
{
	logp = NULL;
	logp = ql_lib_log::get_instance();
}

ccr_access_readonly_table::~ccr_access_readonly_table()
{
	// This forces the user of the library to call
	// the close function.
	ASSERT(table_name == NULL);
	ASSERT(table_path == NULL);
} /*lint !e1540 */


//
// Opens the file for reading. Records the gennum of the file
// at this instant, to check for table_modified exceptions
// for later read operations on this object.
//
int
ccr_access_readonly_table::initialize(const char *t_name)
{
	int retval = 0;
	int bootflags;

	// Table name cannot be NULL
	ASSERT(t_name);

	// Get the reference to the log object
	if (logp == NULL) {
		logp = ql_lib_log::get_instance();
	}

	if (t_name == NULL) {
		logp->log_err(QL_LIB,
		    gettext("ccr_access_readonly_table::initialize called with"
		    " NULL tablename\n"));
		return (-1);
	}

	if ((cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) ||
	    !(bootflags & CLUSTER_BOOTED)) {
		// Node is not a cluster member
		// Do nothing
	} else {
		//
		// Check if CCR transformation needs to be done
		//
		if (access(QL_TRANSFORM_CCR_FILE, F_OK) != 0) {
			//
			// CCR transformation not required
			// Hence, do not allow cluster mode
			// CCR access.
			//
			(void) printf(gettext(
			    "Noncluster CCR access mode not allowed "
			    "in cluster mode when not supporting "
			    "live upgrade.\n"));
			logp->log_err(QL_LIB,
			    gettext("ccr_access_readonly_table: "
			    "Noncluster CCR access mode not allowed "
			    "in cluster mode when not supporting "
			    "live upgrade.\n"));
			exit(ENOTSUP);
		} else {
			//
			// CCR transformation is required.
			// Allow, cluster mode CCR access.
			//
			logp->log_info(QL_LIB,
			    gettext("ccr_access_readonly_table: "
			    "Will allow cluster mode CCR access\n"));
		}
	}

	if (os::strcmp(t_name, "cluster_directory") == 0) {
		//
		// TODO:
		// This is a special case. Read access to
		// cluster_directory file is being provided.
		// In future, if a write access is required,
		// then a separate class needs to be created
		// on the lines of ccr_access_directory class
		//
		table_path = new char[os::strlen(CCR_CLUSTER_DIRECTORY) + 1];
		ASSERT(table_path);
		os::sprintf(table_path, "%s", CCR_CLUSTER_DIRECTORY);
	} else {
		table_path = new char[os::strlen(CCR_DIR_PATH) +
		    os::strlen(t_name) + 2];
		ASSERT(table_path);
		os::sprintf(table_path, "%s/%s", CCR_DIR_PATH, t_name);
	}

	table_name = os::strdup(t_name);
	ASSERT(table_name);

	// Open the file.
	if ((retval = ptab.initialize(table_path)) != 0) {
		logp->log_err(QL_LIB,
		    gettext("Failed to initialize ccr_access_readonly_table "
			"%s Retval = %d\n"), t_name, retval);
		return (retval);
	}

	logp->log_info(QL_LIB,
	    gettext("ccr_access_readonly_table %s initialized\n"), t_name);

	return (retval);
}

//
// ccr_access_readonly_table::query_element()
//   Searches the table for the key and returns corresp data field.
//   Returns NULL if the key is not present or some error occured
//
char *
ccr_access_readonly_table::query_element(const char *qkey)
{
	char *data = NULL;
	table_element_t *tel = NULL;
	int errval;

	// Verify that table name and table path has been initialized
	check_data_members();

	// read elements from table file
	ptab.atfirst();
	while ((tel = ptab.next_element(errval)) != NULL) {
		if (os::strcmp(tel->key, qkey) == 0) {
			data = new char[os::strlen(tel->data) + 1];
			ASSERT(data);
			(void) os::strcpy(data, tel->data);
			delete tel;
			return (data);
		}
		delete tel;
	}

	// Key not found or some error
	logp->log_err(QL_LIB, gettext("query_element: Key %s not found\n"),
			qkey);
	return (NULL);
}


//
// Rewinds to the beginning of the table.
//
void
ccr_access_readonly_table::atfirst()
{
	ptab.atfirst();
}

//
// Retrieve one element from the ccr table. If there's no more elements
// to return NULL. This will not return meta entries.
// The caller of this function will have to free the memory area
// for the table_elemnt_t returned.
//
table_element_t *
ccr_access_readonly_table::next_element()
{
	table_element_t *tel;
	int errval = 0;

	// Verify that table name and table path has been initialized
	check_data_members();

	while ((tel = ptab.next_element(errval)) != NULL) {
		// skip meta entries
		if (!is_meta_row(tel->key)) {
			break;
		} else {
			delete tel;
		}
	}

	if (errval) {
		//
		// if root filesystem is down, we might as well die too
		//
		if (errval == EIO) {
			logp->log_err(QL_LIB, gettext("next_element: "
			    "Unable to read root file system\n"));
			CL_PANIC(0);
		} else {
			// System error
			logp->log_err(QL_LIB, gettext("next_element: "
			    "System error %d\n"), errval);
			return (NULL);
		}
	}

	if (tel) {
		return (tel);
	} else {
		logp->log_err(QL_LIB, gettext("next_element: "
			"No more elements\n"));
		// No more elements
		return (NULL);
	}
}

//
// Returns the number of elements in the table.
// If error occurs, returns -1
// It doesn't count the meta entries.
//
int32_t
ccr_access_readonly_table::get_num_elements()
{
	table_element_t *tel;
	int errval;
	int32_t num_found = 0;

	// Verify that table name and table path has been initialized
	check_data_members();

	ptab.atfirst();
	while ((tel = ptab.next_element(errval)) != NULL) {
		// skip meta entries
		if (!is_meta_row(tel->key)) {
			num_found++;
		}
		delete tel;
	}

	if (errval) {
		// system error
		logp->log_err(QL_LIB, gettext("get_num_elements: "
		    "System error %d\n"), errval);
		return (-1);
	} else {
		return (num_found);
	}
}

//
// This function is supposed to be called after all read operations
// on the table is done and also before any writes are done on the
// table via a ccr_access_updatable_table object.
//
// After this function is called, for further reads on the same table
// should be done after calling the initialize function.
//
void
ccr_access_readonly_table::close()
{
	ptab.close();
	delete table_name;
	delete [] table_path;
	table_name = NULL;
	table_path = NULL;
}

//
// Verifies that the ccr_checksum field in the table exists and
// is correct.
// This is a private function
//
int
ccr_access_readonly_table::verify_checksum()
{
	char reported_cksm[CKSM_HEX_LEN + 1], actual_cksm[CKSM_HEX_LEN + 1];
	uchar_t cksm_digest[DIGEST_LEN];
	table_element_t *next_el;
	int retval = 0;
	*reported_cksm = '\0';	// lint

	// Verify that table name and table path has been initialized
	check_data_members();

	//
	// verify checksum is correct
	//

	// read checksum from file
	while ((next_el = ptab.next_element(retval)) != NULL) {
		if (!is_meta_row(next_el->key)) {
			// no checksum found
			delete next_el;
			return (1);
		}
		if (os::strcmp(next_el->key, "ccr_checksum") == 0) {
			if (os::strlen(next_el->data) != CKSM_HEX_LEN) {
				delete next_el;
				return (1);
			}
			(void) os::strcpy(reported_cksm, next_el->data);
			delete next_el;
			break;
		} else {
			delete next_el;
		}
	}

	// compute actual checksum for table
	if ((retval = ptab.compute_checksum(cksm_digest)) != 0) {
		return (retval);
	}

	//
	// verify it matches reported checksum
	//
	encode_ccr_checksum(cksm_digest, actual_cksm);
	if (os::strcmp(reported_cksm, actual_cksm) != 0) {
		logp->log_err(QL_LIB,
		    gettext("ccr_access_readonly_table::verify_checksum : "
		    "Invalid checksum\n"));
		return (1);
	}

	return (0);
}

//
// To verify that the data members are initialised
// Should be called after initialize() is called
// This is a private member function
//
void
ccr_access_readonly_table::check_data_members()
{
	// None of these can be NULL after a call
	// to initialize
	ASSERT(table_name);
	ASSERT(table_path);
}


//
// --------- ccr_access_updatable_table ------------------
//

ccr_access_updatable_table::ccr_access_updatable_table():
	table_name(NULL),
	table_path(NULL),
	temp_table_path(NULL),
	intent_table_path(NULL),
	commit_table_path(NULL)
{
	gennum = UNINITIALIZED_GENNUM;
	update_flag = false;

	logp = NULL;
	logp = ql_lib_log::get_instance();
}

ccr_access_updatable_table::~ccr_access_updatable_table()
{
	// delete the intent file
	(void) os::file_unlink(intent_table_path);

	delete table_name;
	delete table_path;
	delete temp_table_path;
	delete intent_table_path;
	delete commit_table_path;
} /*lint !e1740 */

//
// Before using a ccr_access_updatable_table object, it needs
// to be initialised.
//
int
ccr_access_updatable_table::initialize(const char *t_name)
{
	int retval = 0;
	int bootflags;

	// Table name cannot be NULL
	ASSERT(t_name);

	// Get the reference to the log object
	if (logp == NULL) {
		logp = ql_lib_log::get_instance();
	}

	if (t_name == NULL) {
		logp->log_err(QL_LIB,
		    gettext("ccr_access_updatable_table::initialize called"
		    " NULL tablename\n"));
		return (-1);
	}

	if ((cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) ||
	    !(bootflags & CLUSTER_BOOTED)) {
		// Node is not a cluster member
		// Do nothing
	} else {
		//
		// Check if CCR transformation needs to be done
		//
		if (access(QL_TRANSFORM_CCR_FILE, F_OK) != 0) {
			//
			// CCR transformation not required
			// Hence, do not allow cluster mode
			// CCR access.
			//
			(void) printf(gettext(
			    "Noncluster CCR access mode not allowed "
			    "in cluster mode when not supporting "
			    "live upgrade.\n"));
			logp->log_err(QL_LIB,
			    gettext("ccr_access_updatable_table: "
			    "Noncluster CCR access mode not allowed "
			    "in cluster mode when not supporting "
			    "live upgrade.\n"));
			exit(ENOTSUP);
		} else {
			//
			// CCR transformation is required.
			// Allow, cluster mode CCR access.
			//
			logp->log_info(QL_LIB,
			    gettext("ccr_access_updatable_table: "
			    "Will allow cluster mode CCR access\n"));
		}
	}

	// table name cannot be directory
	if (os::strcmp(t_name, "directory") == 0) {
		logp->log_err(QL_LIB,
		    gettext("ccr_access_updatable_table::initialize "
		    "tablename cannot be directory\n"));
		ASSERT(0);
		return (-1);
	}

	table_path = new char[os::strlen(CCR_DIR_PATH) + os::strlen(t_name)
	    + 2];
	ASSERT(table_path);
	os::sprintf(table_path, "%s/%s", CCR_DIR_PATH, t_name);

	// Store the temporary table path
	temp_table_path = new char[os::strlen(CCR_DIR_PATH)
	    + os::strlen(t_name) + 2 + 5];
	ASSERT(temp_table_path);
	os::sprintf(temp_table_path, "%s/%s.%s", CCR_DIR_PATH, t_name, "temp");

	// Store the intent table path
	intent_table_path = new char[os::strlen(CCR_DIR_PATH) +
	    os::strlen(t_name) + 2 + 7];
	ASSERT(intent_table_path);
	os::sprintf(intent_table_path, "%s/%s.%s", CCR_DIR_PATH, t_name,
	    "intent");

	// Store the commit table path
	commit_table_path = new char[os::strlen(CCR_DIR_PATH) +
	    os::strlen(t_name) + 2 + 7];
	ASSERT(commit_table_path);
	os::sprintf(commit_table_path, "%s/%s.%s", CCR_DIR_PATH, t_name,
	    "commit");

	// Store the table name
	table_name = os::strdup(t_name);
	ASSERT(table_name);

	// Store the gennum of the table before editing it.
	retval = store_gennum();
	if (retval != 0) {
		// Could not store the gennum properly
		logp->log_err(QL_LIB,
		    gettext("Could not store gennum for table %s\n"), t_name);
	}

	// Copy the original table to the intent table file
	retval = os::file_copy(table_path, intent_table_path);
	if (retval != 0) {
		// Failed to copy the table to intent file.
		logp->log_err(QL_LIB, gettext("Failed to copy the table %s "
		    "to intent file\n"), t_name);
		ASSERT(0);
		return (-1);
	}

	logp->log_info(QL_LIB,
	    gettext("ccr_access_updatable_table initialized\n"));

	return (0);
}

//
// Add new element consisting of specified key and data
//
int
ccr_access_updatable_table::add_element(const char *key, const char *data)
{
	int retval = 0;

	// key cannot be NULL
	ASSERT(key);

	if (key == NULL) {
		logp->log_err(QL_LIB,
		    gettext("ccr_access_updatable_table::add_element "
		    "called with NULL key\n"));
		return (1);
	}

	if (data != NULL) {
		logp->log_info(QL_LIB, gettext("add_element called with"
		    " Key = %s Data = %s\n"), key, data);
	} else {
		logp->log_info(QL_LIB, gettext("add_element called with"
		" Key = %s Data = NULL\n"), key);
	}

	// call modify element in insert mode
	if ((retval = modify_element(key, data, INSERT_MODE)) != 0) {
		return (retval);
	}

	// Update has occured
	update_flag = true;

	logp->log_info(QL_LIB, gettext("Successfully added element\n"));

	return (retval);
}

//
// Remove element with the specified key from the table
//
int
ccr_access_updatable_table::remove_element(const char *key)
{
	int retval = 0;

	// key cannot be NULL
	ASSERT(key);

	if (key == NULL) {
		logp->log_err(QL_LIB,
		    gettext("ccr_access_updatable_table::remove_element "
		    "called with NULL key\n"));
		return (1);
	}

	logp->log_info(QL_LIB, gettext("remove_element called with %s\n"),
	    key);

	// call modify element in remove mode
	if ((retval = modify_element(key, NULL, REMOVE_MODE)) != 0) {
		return (retval);
	}

	// Update has occured
	update_flag = true;

	logp->log_info(QL_LIB, gettext("Successfully removed element\n"));

	return (retval);
}

//
// Remove all the elements from the table
// Only the gennum and checksum will remain
//
int
ccr_access_updatable_table::remove_all_elements()
{

	updatable_persistent_table new_table;
	char cksm_text[CKSM_HEX_LEN + 1];
	char gennum_str[CKSM_HEX_LEN + 1];
	int retval = 0;

	logp->log_info(QL_LIB, gettext("remove_all_elements called\n"));

	// check tablename and path are not NULL
	check_data_members();

	// Make a intent table file
	if ((retval = new_table.initialize(intent_table_path)) != 0) {
		logp->log_err(QL_LIB, gettext("remove_all_elements: "
		    "Failed to create intent file. Retval = %d\n"), retval);
		return (retval);
	}

	//
	// Write meta entries
	//

	// insert gennum (will store the original gennum)
	os::sprintf(gennum_str, "%ld\0", gennum);
	if ((retval = new_table.insert_element("ccr_gennum", gennum_str))
	    != 0) {
		logp->log_err(QL_LIB, gettext("remove_all_elements: "
		    "Failed to insert ccr gennum. Retval = %d\n"), retval);
		(void) new_table.close();
		return (retval);
	}

	// insert ccr_checksum
	init_ccr_checksum(cksm_text);
	CL_PANIC(os::strlen(cksm_text) == CKSM_HEX_LEN);
	if ((retval =
	    new_table.insert_element("ccr_checksum", cksm_text)) != 0) {
		logp->log_err(QL_LIB, gettext("remove_all_elements: "
		    "Failed to insert ccr checksum. Retval = %d\n"), retval);
		(void) new_table.close();
		return (retval);
	}

	// Close the intent file
	(void) new_table.close();

	// Update has occured
	update_flag = true;

	logp->log_info(QL_LIB, gettext("Successfully removed all elements\n"));

	return (0);
}

//
// Replace data in element associated with the specified key
//
int
ccr_access_updatable_table::update_element(const char *key, const char *data)
{
	int retval = 0;

	// key cannot be NULL
	ASSERT(key);

	if (key == NULL) {
		logp->log_err(QL_LIB, gettext("update_element called with"
		    " key = NULL\n"));
		return (1);
	}

	if (data != NULL) {
		logp->log_info(QL_LIB, gettext("update_element called with"
		    " key %s data = %s\n"), key, data);
	} else {
		logp->log_info(QL_LIB, gettext("update_element called with"
		    " key %s data = NULL\n"), key);
	}

	// call modify element in update mode
	if ((retval = modify_element(key, data, UPDATE_MODE)) != 0) {
		logp->log_err(QL_LIB, gettext("Failed to update element\n"));
		return (retval);
	}

	logp->log_info(QL_LIB, gettext("Successfully updated element\n"));

	// Update has occured
	update_flag = true;

	return (retval);
}

//
// This function will commit all the updates performed on the table.
// The gennum will be incremented by 1 by default. If user wants to specify
// the increment, he has to pass the incr as parameter.
// This function should be called after all updates on the table has been
// done.
//
// incr is an optional parameter. The default value is 1
//
int
ccr_access_updatable_table::commit(int incr) {

	updatable_persistent_table new_table;
	readonly_persistent_table orig_table;
	char gennum_str[CKSM_HEX_LEN + 1];
	char metastr[CKSM_HEX_LEN + 1];
	uchar_t cksm_digest[DIGEST_LEN];
	table_element_t *telp;
	int retval = 0;

	logp->log_info(QL_LIB, gettext("commit called for table %s "
	    "with incr = %d\n"), table_name, incr);

	// check tablename and path are not NULL
	check_data_members();

	// Check if updates have occured
	if (update_flag == false) { /*lint !e731 */
		// No updates have occured
		logp->log_info(QL_LIB, gettext("No updates have occured "
		    "on table %s\n"), table_name);
		return (retval);
	}

	// Open the intent table file
	if ((retval = orig_table.initialize(intent_table_path)) != 0) {
		logp->log_err(QL_LIB,
		    gettext("commit: Failed to open the intent table file\n"));
		return (retval);
	}

	// Make the commit table file
	if ((retval = new_table.initialize(commit_table_path)) != 0) {
		logp->log_err(QL_LIB, gettext("commit: "
		    "Failed to create the commit table file\n"));
		orig_table.close();
		return (retval);
	}

	// insert the gennum after incrementing it (gennum + incr)
	os::sprintf(gennum_str, "%ld\0", gennum + incr);
	if ((retval = new_table.insert_element("ccr_gennum", gennum_str))
	    != 0) {
		logp->log_err(QL_LIB, gettext("commit: "
		    "Failed to insert the gennum. Retval = %d\n"), retval);
		orig_table.close();
		(void) new_table.close();
		return (retval);

	}

	// insert the ccr_checksum
	if ((retval = orig_table.compute_checksum(cksm_digest)) != 0) {
		logp->log_err(QL_LIB, gettext("commit: "
		    "Failed to compute the checksum. Retval = %d\n"), retval);
		orig_table.close();
		(void) new_table.close();
		return (retval);
	}
	encode_ccr_checksum(cksm_digest, metastr);
	if ((retval = new_table.insert_element("ccr_checksum", metastr)) != 0) {
		logp->log_err(QL_LIB, gettext("commit: "
		    "Failed to insert the checksum. Retval = %d\n"), retval);
		orig_table.close();
		(void) new_table.close();
		return (retval);
	}

	// Copy the data elements
	orig_table.atfirst();
	while ((telp = orig_table.next_element(retval)) != NULL) {
		// ignore any stray metadata entries
		if (!is_meta_row(telp->key)) {
			if ((retval = new_table.insert_element(telp)) != 0) {
				logp->log_err(QL_LIB, gettext("commit: "
				    "Failed to insert element. Retval = %d\n"),
				    retval);
				orig_table.close();
				(void) new_table.close();
				delete telp;
				return (retval);
			}
		}
		delete telp;
	}

	// Close the intent table
	orig_table.close();

	// Close the commit table
	if ((retval = new_table.close()) != 0) {
		logp->log_err(QL_LIB, gettext("commit: "
		    "Failed to close the commit table. Retval = %d\n"),
		    retval);
		return (retval);
	}

	// rename commit table to orig table
	retval = os::file_rename(commit_table_path, (char *)table_path);
	if (retval != 0) {
		logp->log_err(QL_LIB, gettext("commit: "
		    "Failed to rename commit table to original table. "
		    "Retval = %d\n"), retval);
	} else {
		logp->log_info(QL_LIB, gettext("Commit successful for table "
		    "%s with gennum %d\n"), table_name, gennum + incr);
	}

	// Set the update flag to false as the updates are
	// already commited.
	update_flag = false;

	// increment the gennum cached in the object to reflect the
	// gennum of the table after the commit.
	gennum = gennum + incr;

	return (retval);

}
// End of ccr_access_updatable_table::commit


//
// This function will add, remove or update the element depending on the mode
// mode = 1 INSERT_MODE
// mode = 2 REMOVE_MODE
// mode = 3 UPDATE_MODE
// All operations are done on the intent file.
// This is a private function
//
int
ccr_access_updatable_table::modify_element(const char *key,
    const char *data, int mode)
{
	updatable_persistent_table new_table;
	readonly_persistent_table orig_table;
	table_element_t *telp;
	int retval = 0;
	int found_flag = 0;

	// key can not be null
	ASSERT(key);

	if (key == NULL) {
		logp->log_info(QL_LIB,
		    gettext("modify_element called with NULL key\n"));
		return (1);
	}

	// check tablename and path are not NULL
	check_data_members();

	if (data != NULL) {
		logp->log_info(QL_LIB, gettext("modify_element called with "
		    "key = %s data = %s mode = %d\n"), key, data, mode);
	} else {
		logp->log_info(QL_LIB, gettext("modify_element called with "
		    "key = %s data = NULL mode = %d\n"), key, mode);
	}

	// Open the intent table file
	if ((retval = orig_table.initialize(intent_table_path)) != 0) {
		logp->log_err(QL_LIB, gettext("modify_element: "
		    "Failed to open the intent table file. Retval = %d\n"),
		    retval);
		return (retval);
	}

	// Make a temporary table file
	if ((retval = new_table.initialize(temp_table_path)) != 0) {
		logp->log_err(QL_LIB, gettext("modify_element: "
		    "Failed to make the temp table file. Retval = %d\n"),
		    retval);
		return (retval);
	}

	//
	// No need to store checksum and gennum in the intent table
	//

	//
	// Copy data entries and insert, remove or update the key
	//
	orig_table.atfirst();
	while ((telp = orig_table.next_element(retval)) != NULL) {
		// ignore any stray metadata entries
		if (!is_meta_row(telp->key)) {
			// Check if the key already exists in the table
			if (os::strcmp(telp->key, key) == 0) {
				// Key is already present
				found_flag = 1;
				// In insert mode the key is already present
				// This is an error
				if (mode == INSERT_MODE) {
					logp->log_err(QL_LIB,
					    gettext("modify_element: "
					    "Key is already present\n"));
					orig_table.close();
					(void) new_table.close();
					delete telp;
					return (-1);
				}
				// In remove mode, the key data pair need not
				// be written
				if (mode == REMOVE_MODE) {
					delete telp;
					continue;
				}
				// In update mode, if key is present then
				// insert the new key and data to temp table
				if (mode == UPDATE_MODE) {
					if ((retval =
					    new_table.insert_element(key,
					    data)) != 0) {
						logp->log_err(QL_LIB,
						    gettext("modify_element: "
						    "Failed to insert new key"
						    " in temp table. Retval ="
						    " %d\n"), retval);
						orig_table.close();
						(void) new_table.close();
						delete telp;
						return (retval);
					}
				} // mode == UPDATE_MODE
			} else {
				// Key does not exist
				// So copy the existing keys to the new table
				if ((retval = new_table.insert_element(telp))
				    != 0) {
					logp->log_err(QL_LIB,
					    gettext("modify_element: "
					    "Failed to copy keys. Retval = "
					    "%d\n"), retval);
					orig_table.close();
					(void) new_table.close();
					delete telp;
					return (retval);
				}
			}
		}
		delete telp;
	} // End of while

	// In insert mode, insert the new element.
	// In update mode, since the key is not present, create a new
	// one and add it.
	if ((found_flag == 0) && ((mode == INSERT_MODE) ||
	    (mode == UPDATE_MODE))) {
		if ((retval = new_table.insert_element(key, data)) != 0) {
			logp->log_err(QL_LIB, gettext("modify_element: "
			    "Failed to insert element. Retval = %d\n"),
			    retval);
			orig_table.close();
			(void) new_table.close();
			return (retval);
		}
	}

	// Close the intent table
	orig_table.close();

	// close the temporary table
	if ((retval = new_table.close()) != 0) {
		logp->log_err(QL_LIB, gettext("modify_element: "
		    "Failed to close temp table. Retval = %d\n"), retval);
		return (retval);
	}

	// rename temp table to intent table
	retval = os::file_rename(temp_table_path, (char *)intent_table_path);
	if (retval != 0) {
		logp->log_err(QL_LIB, gettext("modify_element: "
		    "Failed to rename temp table to intent table "
		    "Retval = %d\n"), retval);
	} else {
		logp->log_info(QL_LIB, gettext("modify_element successful\n"));
	}
	return (retval);
}
// End  of ccr_access_updatable_table::modify_element


//
// This function will store the gennum of the table before it is edited
// This is called from the initialize member function.
// This is a private member function
//
int
ccr_access_updatable_table::store_gennum()
{
	int retval = 0;
	readonly_persistent_table orig_table;
	table_element_t *telp;

	// Open the original table file
	if ((retval = orig_table.initialize(table_path)) != 0) {
		logp->log_err(QL_LIB, gettext("ccr_access_updatable_table "
		    "Failed to open table file %s Retval = %d\n"),
		    table_name, retval);
		return (retval);
	}

	// Retrieve the gennum from the table
	orig_table.atfirst();
	while ((telp = orig_table.next_element(retval)) != NULL) {
		if (os::strcmp(telp->key, "ccr_gennum") == 0) {
			gennum = atol(telp->data);
			delete telp;
			orig_table.close();
			return (0);
		}
		delete telp;
	}
	orig_table.close();

	// Gennum not found in the table
	logp->log_err(QL_LIB, gettext("Failed to store gennum for "
	    "table %s\n"), table_name);
	return (1);
}

//
// To verify that the data members are initialised
// Should be called after initialize() is called
// This is a private member function
//
void
ccr_access_updatable_table::check_data_members()
{
	// None of these can be NULL after a call to initialize
	ASSERT(table_name);
	ASSERT(table_path);
	ASSERT(temp_table_path);
	ASSERT(intent_table_path);
	ASSERT(commit_table_path);

}
