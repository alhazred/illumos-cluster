#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.com	1.4	08/05/20 SMI"
#
# lib/libqlccr/Makefile.com
#

LIBRARYCCC = libqlccr.a
VERS = .1

#
# This library contain the non cluster mode ccr access files
#

OBJECTS = ccr_access.o

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

PMAP =
SRCS =		$(OBJECTS:%.o=../common/%.c)

LIBS = $(DYNLIBCCC) $(LIBRARYCCC)

CLEANFILES =

# definitions for lint
LINTFILES += $(OBJECTS:%.o=%.ln)

MTFLAG	= -mt
CPPFLAGS += $(CL_CPPFLAGS)
CPPFLAGS += -I$(SRC)/common/cl/interfaces/$(CLASS) -I$(SRC)/common/cl
CPPFLAGS += -I$(SRC)/lib/libqllog/common
CPPFLAGS += -I$(SRC)/lib/libqlccr/common
CPPFLAGS += -DSC_SRM

# Warning: new dependencies here should be marked in cmd/scrconf/Makefile also
# to build the static command
LDLIBS += -lc -ldl -lclcomm -lclconf -lclos -lqllog

.KEEP_STATE:

all:  $(LIBS)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<
