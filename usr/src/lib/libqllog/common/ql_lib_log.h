//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ql_lib_log.h
//

#ifndef	_QL_LIB_LOG_H
#define	_QL_LIB_LOG_H

#pragma ident	"@(#)ql_lib_log.h	1.4	08/10/30 SMI"

#include <stdio.h>

// The location of the QL log file
#define	QL_LIB_LOG_FILE "/var/cluster/logs/install/libqlccr_debug.log"

// The maximum length of the log message
#define	MAX_LOG_LEN 65536


//
// class definition that encapsulated the logging mechanism
// for the quantum_leap commands and the ccr access library.
// This is a singleton class. Users should not declare objects
// of this class. They should declare a pointer of this class
// and use the get_instance() method to get the reference to
// the singleton object.
//
class ql_lib_log {

public:
	// Returns a singleton instance of the ql_lib_log object.
	static ql_lib_log* get_instance();

	// log informational message to the log file.
	void log_info(const char *, const char *, ...);

	// log error message to the log file.
	void log_err(const char *, const char *, ...);

private:
	// private constructor.
	ql_lib_log();

	// private destructor.
	~ql_lib_log();

	// close the log file pointer.
	void close();

	// static ql object.
	static ql_lib_log ql_lib_log_obj;

	// static file pointer
	static FILE *logfilep;

};

#endif /* _QL_LIB_LOG_H */
