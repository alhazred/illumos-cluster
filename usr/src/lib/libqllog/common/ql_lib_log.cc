//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// ql_lib_log.cc
//

#pragma ident	"@(#)ql_lib_log.cc	1.3	08/05/20 SMI"


#include <stdarg.h>
#include <time.h>
#include <string.h>
#include <sys/cl_assert.h>
#include <ql_lib_log.h>

//
// Initialize file pointer.
//
FILE *ql_lib_log::logfilep = NULL;

//
// The sigleton Quantum Leap log object
//
ql_lib_log ql_lib_log::ql_lib_log_obj; /*lint !e1502 */

//
// Private constructor.
//
ql_lib_log::ql_lib_log()
{
}

//
// Private destructor.
//
ql_lib_log::~ql_lib_log()
{
	close();
}

//
// Get the reference to the singleton object. The object is created
// if it doesnt exist.
// All clients intending to use the logging infrastructure must call
// this function before trying to log any message
//
ql_lib_log*
ql_lib_log::get_instance()
{
	if (ql_lib_log::logfilep == NULL) {
		if ((ql_lib_log::logfilep = fopen(QL_LIB_LOG_FILE,
		    "a")) == NULL) {
			//
			// Could not open the logfile.
			// The function calling this function has to handle
			// this error.
			//
			return (NULL);
		}
	}

	return (&ql_lib_log_obj);
}

//
// This function should be used to log an informational message.
// The first parameter is the client of the library which is
// logging the message. The next parameter is the actual message.
//
void
ql_lib_log::log_info(const char *prgname, const char *format, ...)
{
	time_t		curr_time;
	char		buf[MAX_LOG_LEN];
	va_list		list;
	char		temp_time[30];

	ASSERT(prgname != NULL);
	ASSERT(format  != NULL);

	va_start(list, format);	/*lint !e40 */
	(void) vsprintf(buf, format, list);
	(void) time(&curr_time);
	(void) ctime_r(&curr_time, temp_time, 30);
	temp_time[strlen(temp_time) - 1] = '\0';

	(void) fprintf(ql_lib_log::logfilep, "%s %s: %s %s\n",
	    temp_time, prgname, "INFO ", buf);

	(void) fflush(ql_lib_log::logfilep);
	va_end(list);
}

//
// This function should be used to log an error message.
// The first parameter is the client of the library which is
// logging the message. The next parameter is the actual message.
//
void
ql_lib_log::log_err(const char *prgname, const char *format, ...)
{
	time_t		curr_time;
	char 		buf[MAX_LOG_LEN];
	va_list		list;
	char		temp_time[30];


	ASSERT(prgname != NULL);
	ASSERT(format  != NULL);

	va_start(list, format);	/*lint !e40 */
	(void) vsprintf(buf, format, list);
	(void) time(&curr_time);
	(void) ctime_r(&curr_time, temp_time, 30);
	temp_time[strlen(temp_time) - 1] = '\0';


	(void) fprintf(ql_lib_log::logfilep, "%s %s: %s %s\n",
	    temp_time, prgname, "ERROR", buf);

	(void) fflush(ql_lib_log::logfilep);
	va_end(list);
}

//
// Flush data and close the file pointer for the log file.
//
void
ql_lib_log::close()
{
	if (ql_lib_log::logfilep != NULL) {
		(void) fflush(ql_lib_log::logfilep);
		(void) fclose(ql_lib_log::logfilep);
	}

	ql_lib_log::logfilep = NULL;
}
