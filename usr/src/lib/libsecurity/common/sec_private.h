/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SEC_PRIVATE_H
#define	_SEC_PRIVATE_H

#pragma ident	"@(#)sec_private.h	1.22	08/05/20 SMI"

/*
 * sec_private.h
 * private header file for security library
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/sol_version.h>
#include <stdarg.h>

#if SOL_VERSION >= __s10
#include <zone.h>
#include <libzonecfg.h>
#endif

/*
 * This is passed as arg to security_svc_reg
 * so it can be set by user.
 */
static int debug = 1;

/*
 * client debug - is passed as arg to security_clnt_connect
 * so it can be set by user.
 */
static int clnt_debug = 1;

/*
 * client output venue - is passed as arg to security_clnt_connect
 * so it can be set by user. If true, the client messages are
 * output to stderr and syslog.
 * This is used by traditional clients like pmfadm.
 * If false, the client messages are output only to syslog. This is used by
 * clients that are themselves part of a server, like fed.
 */
static int clnt_output = 1;

#if DOOR_IMPL
static int create_handle(client_handle **h,
	int door_descriptor,
	char *program_name,
	SEC_TYPE sec_type); /* to be made SEC_TYPE */

#else
/*
 *  Client handle - keeps info tidy
 */
typedef struct clnt_handlerecord {
	char *host_name;
	CLIENT *clnt;
	const char *program_name;
	ulong_t program_num;
	ulong_t version;
	SEC_TYPE sec_type;
} clnt_handlerecord;
typedef clnt_handlerecord *clnt_h;

static void create_handle(clnt_h h,
	char *host_name,
	const char *program_name,
	ulong_t program_num,
	ulong_t version,
	SEC_TYPE sec_type);

/*
 * private functions prototypes
 */
static int security_svc_init(void);
static int clnt_init(void);
static bool_t inloopbackset(char *netid);
extern int __rpc_get_local_uid(SVCXPRT *trans, uid_t *uid_out);
static bool_t check_security(
	struct svc_req *rqstp,
	uid_t desired_uid);
static bool_t check_authsys_security(struct svc_req *rqstp,
	uid_t desired_uid);
static bool_t clnt_connect_local(clnt_h h);
static bool_t clnt_connect_remote(clnt_h h);
static bool_t trygetbinding(clnt_h h, char *transp);
static bool_t clnt_authenticate(CLIENT *clnt, SEC_TYPE sec_type);
static void print_rpc_createerr(clnt_h h);
#endif

static char *estrdup(char *s);

/*
 * error and debug reporting functions for server and client sides
 */
static void clnt_dbg_msgout(const char *msg, ...);
static void dbg_msgout(const char *msg, ...);
static void dbg_msgout_onlysyslog(int priority, const char *msg, ...);
static char *clnt_estrdup(char *s);

extern void __assert(const char *, const char *, int);

/*
 * This header file defines a macro "sec_assert".  It is similar to the
 * normal "assert" macro defined in <assert.h>, except that it is
 * evaluated, regardless of whether debugging is on or off.
 */

#if defined(__STDC__)
#define	sec_assert(EX) (void)((EX) || (__assert(#EX, __FILE__, __LINE__), 0))
#else
#define	sec_assert(EX) (void)((EX) || (_assert("EX", __FILE__, __LINE__), 0))
#endif	/* __STDC__ */

#ifdef __cplusplus
}
#endif

#endif	/* !_SEC_PRIVATE_H */
