/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)security.c	1.48	08/06/23 SMI"
/*
 *	security.c - part of libsecurity
 *	Security library for servers
 */
#include <sys/cl_assert.h>
#include <sys/cladm_int.h>
#include <sys/cladm.h>
#include <syslog.h>
#include <sys/os.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <rpc/rpc.h>
#include <rpc/types.h>
#include <rpc/svc.h>
#ifdef linux
#include <sys/utsname.h>
#include <netdb.h>
#include <netinet/in.h>
#else
#include <syslog.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <locale.h>
#include <netdb.h>
#include <pthread.h>
#include <sys/rsrc_tag.h>
#endif /* linux */

#include <sys/sc_syslog_msg.h>
#include  <rgm/security.h>

#if DOOR_IMPL
#include <door.h>
#else
#ifndef linux
#include <netconfig.h>
#endif
#include <sys/utsname.h>
#include <assert.h>
#endif /* DOOR_IMPL */

#include  "sec_private.h"
#ifdef linux
#define	AUTHSYS_PARMS "authunix_parms"
#define	AUTH_CREATE_DEFAULT "authunix_create_default"
#define	CLNT_CREATE "clnt_create"
#endif

#define	stringsize(numbytes) ((size_t)((2.41 * (numbytes)) + 1))

#define	NOGET(s)	(s)

static void syslognomem(void);

#if DOOR_IMPL

/* Update RGM_RGM_MAX_DOOR_PATHLEN in cl_rgm.h if changing pathnames below */
static char *rgm_door_paths[] =
				{
				"/var/run/rgmd_receptionist_door",
				"/var/run/zoneup_door",
				"/var/run/sysevent_proxy_door",
				"/var/run/fed_door",
				"/var/run/pmfd_door"
				};

static char *rgm_door_programs[] =
				{
				"rgmd",
				"zoneup",
				"sysevent_proxy",
				"fed",
				"pmfd"
				};


static int security_svc_door_create(door_server *proc);
static int security_svc_insert_door(
    rgm_door_code_t, const char *path, const char *);
static int security_svc_attach_door(int door_id,
    rgm_door_code_t program_num, char *);

#else
/* RPC Implementation Starts */
extern int __rpc_negotiate_uid(int fd);

/*
 * We get the list of loopback transports and cache it in loopbackset array;
 * this list is used by both server and client for local calls;
 * we only get the list of loopback transports once;
 * inited flag is true if list was obtained once.
 * The mutex is used by the server and client to restrict access
 * to one thread at a time while writing the lists of transports,
 * in order to be MT-safe. Afterwards multiple threads can simultaneously
 * read The same list of transports, because it's unique for the host.
 */
static bool_t inited = FALSE;
/*
 * A list of remote transport is kept in cache in tcpset array
 * for outbound clint connections and updated during clnt_init()
 * A new flag (clnt_inited) is added to keep state of this cache
 * because if security_svc_ininit has already been called
 * inited will be true, but tpcset will be empty.
 * Same mutex is used for both inited and clnt_inited.
 */
static bool_t clnt_inited = FALSE;
/*
 * Suppress lint message about "union initialization"
 * DEFAULTMUTEX is defined  in /usr/include/synch.h
 */
/* CSTYLED */
static mutex_t  inited_mutex = DEFAULTMUTEX; /*lint !e708 */

static struct utsname server_utsn;

typedef struct {
	int len;
	char **a;
} transp_set;

/*
 * The set of netconfig netid's which are loopback transports.
 * Used for local access by both client and server.
 */
static	transp_set	loopbackset;
/*
 * The set of netconfig netid's which are tcp transports.
 * Used for remote access by client.
 */
static	transp_set	tcpset;


#define	NUM_RETRIES	3
#define	SLEEP_SECS_BETWEEN_TRIES	10

/*
 * submodule for rpc registration using a file
 *
 * This submodule provides routines for performing rpc registration
 * in a file and looking up that registration in a file.  The
 * purpose is to avoid the continuous overhead of calling rpcbind
 * to do the lookup.  This reduces the overhead on the rpcbind daemon
 * itself, and allows some stuff to work even though rpcbind is down
 * or sluggish or unresponsive.
 * This functionality supports intra-host (same host) rpc lookups only.
 * Even though we register in the file, we also register with rpcbind
 * in the traditional manner.  One motivation for that is callers
 * who are not necessarily linked with this module to do the client
 * side lookup.  A more important motivation is that it gives a fallback
 * position for error cases, namely, that we can always fall back to
 * using rpcbind; obviously, the fall back requires rpcbind to be up
 * and responsive.
 *
 * There are two major routines: rpc_reg_file, which is to be called by
 * the rpc server daemon (callee side), and clnt_create_file, which is
 * to be called by the client (caller) side.
 *
 * The rpc binding information is recorded in files in the directory
 * RPCFILEDIR.  One file per piece of binding info.
 *
 */

#define	RPCFILEDIR	"/var/run/scrpc"
#define	RPCADDRMAXLEN	1024
/* The maximum size of the netbuf buffer for holding the address */

/*
 * computerpcfilename
 *
 * Computes the name of the file to hold the binding info.  The file
 * name is returned as a malloc'ed string; it is up to our caller
 * to free() it.  Returns NULL for errors; non-NULL for success.
 */
static char *
computerpcfilename(
	const ulong_t rpc_prog_num,
	const ulong_t rpc_prog_vers,
	struct netconfig *nconf)
{
	char *s = (char *)malloc((ulong_t)PATH_MAX);

	if (s == NULL) {
		syslognomem();
		return (NULL);
	}
	(void) sprintf(s, "%s/%lu.%lu.%s",
	    RPCFILEDIR, rpc_prog_num, rpc_prog_vers, nconf->nc_netid);
	return (s);
}

/*
 * dowrite
 *
 * Wraps write system call with loop to handle EINTR and write
 * calls that write less than the requested amount.
 */
static ssize_t
dowrite(
	int fd,
	char *buf,
	size_t nbyte)
{
	ssize_t w;
	size_t bytesleft = nbyte;

	while (bytesleft > 0) {
	w = write(fd, buf, bytesleft);
		if (w == -1) {
			/*
			 * lint doesn't understand errno.
			 */
			/* CSTYLED */
			if (errno == EINTR) /*lint !e746 */
				continue;
			else
				return (-1);
		}
		ASSERT(w >= 0);
		buf += w;
		bytesleft -= (size_t)w;
	}
	ASSERT(bytesleft == 0);
	return ((ssize_t)nbyte);
}

/*
 * doread
 *
 * Wraps read system call with loop to handle EINTR and read
 * calls that read less than the requested amount.
 */
static ssize_t
doread(
	int fd,
	char *buf,
	size_t nbyte)
{
	ssize_t w;
	size_t bytesleft = nbyte;

	while (bytesleft > 0) {
		w = read(fd, buf, bytesleft);
		if (w == -1) {
			if (errno == EINTR)
				continue;
			else
				return (-1);
		} else if (w == 0) {
			/* Hit end-of-file */
			return ((ssize_t)(nbyte - bytesleft));
		}
		ASSERT(w > 0);
		buf += w;
		bytesleft -= (size_t)w;
	}
	ASSERT(bytesleft == 0);
	return ((ssize_t)nbyte);
}

/*
 * rpc_reg_file
 *
 * Look up the the networking address that we just
 * registered with rpcbind, and record it in a file
 * in RPCFILEDIR.
 * Returns TRUE for success, FALSE for errors.
 * Any errors cause us to issue a warning and to
 * return early; the errors are not fatal because
 * clients can always fall back to using rpcbind.
 */
static bool_t
rpc_reg_file(
    const SVCXPRT* stp,
    const char *rpc_prog_name,
    const ulong_t rpc_prog_num,
    const ulong_t rpc_prog_vers,
    struct netconfig *nconf)
{
#ifdef linux
	/* Linux does not support netconfig */
	return (TRUE);
#else
	int fd = -1;
	char *rpcfilename = NULL;
	uint16_t len16;
	bool_t retval = FALSE;  /* initially FALSE to simplify error return */
	sc_syslog_msg_handle_t sys_handle;

	rpcfilename = computerpcfilename(
	    rpc_prog_num, rpc_prog_vers, nconf);
	if (rpcfilename == NULL)
	goto err_ret;

	/*
	 * Create the directory for holding rpc binding information
	 * files.  Give warning if fail to create the directory,
	 * but otherwise go on -- clients will fall back to using
	 * rpcbind.
	 */
	if (mkdir(RPCFILEDIR, 0755) == -1 && errno != EEXIST) {
		int err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rpc.pmfd, rpc.fed or rgmd server was not able to create
		 * a directory to contain "cache" files for rpcbind
		 * information. The affected component should still be able to
		 * function by directly calling rpcbind.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_WARNING, MESSAGE,
		    "in libsecurity mkdir of %s failed: %s",
		RPCFILEDIR, strerror(err));
		sc_syslog_msg_done(&sys_handle);
	}

	if ((fd = creat(rpcfilename, 0644)) == -1) {
		int err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The specified server was not able to create a cache file
		 * for rpcbind information. The affected component should
		 * continue to function by calling rpcbind directly.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_WARNING,
		    MESSAGE,
		    "in libsecurity for program %s (%lu); creat of file %s "
		    "failed: %s", rpc_prog_name, rpc_prog_num, rpcfilename,
		    strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) unlink(rpcfilename);
		goto err_ret;
	}

	/*
	 * The file contents consist of an initial length field of
	 * exactly two bytes, followed by data of exactly length
	 * bytes in length.
	 * Use a 16 bit integer for the length that gets written into the
	 * file, so that we know that that length is exactly two bytes,
	 * regardless of whether we are running 32 bit versus 64 bit
	 * Solaris.
	 */
	ASSERT(stp->xp_ltaddr.len < 1L<<16);
	len16 = (uint16_t)stp->xp_ltaddr.len;

	errno = 0;  /* clear out stale info */
	if (dowrite(fd, (char *)&len16, sizeof (uint16_t)) !=
	    (ssize_t)sizeof (uint16_t) ||
	    dowrite(fd, stp->xp_ltaddr.buf, (ulong_t)(stp->xp_ltaddr.len))
	    != (ssize_t)stp->xp_ltaddr.len) {
		int err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The specified server was not able to write to a cache file
		 * for rpcbind information. The affected component should
		 * continue to function by calling rpcbind directly.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_WARNING,
		    MESSAGE,
		    "in libsecurity for program %s (%lu); write of file %s "
		    "failed: %s", rpc_prog_name, rpc_prog_num, rpcfilename,
		    strerror(err));
		sc_syslog_msg_done(&sys_handle);
		/* remove the file because it may be empty or partial */
		(void) unlink(rpcfilename);
		goto err_ret;
	}

	/* success */
	retval = TRUE;

err_ret:
	if (fd != -1)
		(void) close(fd);
	if (rpcfilename != NULL)
		free(rpcfilename);
	return (retval);
#endif /* linux */
}

/*
 * clnt_create_file
 *
 * Tries to create a local binding for the transport nconf, by
 * checking to see if there is info about this program and
 * version in the RPCFILEDIR: if so, use that info,
 * if not, return NULL (our caller is expected to consult rpcbind).
 */
static CLIENT *
clnt_create_file(
	const char *rpc_prog_name,
	ulong_t rpc_prog_num,
	ulong_t rpc_prog_vers,
	struct netconfig *nconf)
{
	CLIENT *clnt = NULL;
#ifndef linux
	int fd = -1;
	char *rpcfilename = NULL;
	struct netbuf rpcsvcaddr;
	ulong_t cnt1 = 0, cnt2 = 0;
	uint16_t len16;
	sc_syslog_msg_handle_t sys_handle;

	(void) memset(&rpcsvcaddr, 0, sizeof (rpcsvcaddr));
	rpcsvcaddr.maxlen = RPCADDRMAXLEN;
	rpcsvcaddr.len = 0;
	rpcsvcaddr.buf = (char *)malloc((ulong_t)RPCADDRMAXLEN);
	if (rpcsvcaddr.buf == NULL) {
		syslognomem();
		goto err_ret;
	}

	rpcfilename = computerpcfilename(
	    rpc_prog_num, rpc_prog_vers, nconf);
	if (rpcfilename == NULL)
		goto err_ret;

	fd = open(rpcfilename, O_RDONLY, 0);
	if (fd == -1) {
#ifdef DEBUG
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "libsecurity: program %s (%lu) debug: clnt_create_file: "
		    "open %s failed: %m", rpc_prog_name, rpc_prog_num,
		    rpcfilename);
#endif /* DEBUG */
		goto err_ret;
	}

	errno = 0;  /* clear out stale info */
	if ((cnt1 = (ulong_t)doread(fd, (char *)&len16, sizeof (len16))) !=
	    (ssize_t)sizeof (len16) ||
	    (cnt2 = (ulong_t)doread(fd, rpcsvcaddr.buf, (ulong_t)len16)) !=
	    (ssize_t)len16) {
#ifdef DEBUG
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "in libsecurity: program %s (%lu) warning: "
		    "file %s bad content cnt1=%d cnt2=%d %m", rpc_prog_name,
		    rpc_prog_num, rpcfilename, cnt1, cnt2);
#endif
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The specified server was not able to read an rpcbind
		 * information cache file, or the file's contents are
		 * corrupted. The affected component should continue to
		 * function by calling rpcbind directly.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_WARNING, MESSAGE,
		    "in libsecurity: program %s (%lu); file %s not readable "
		    "or bad content", rpc_prog_name, rpc_prog_num,
		    rpcfilename);
		sc_syslog_msg_done(&sys_handle);
		goto err_ret;
	}

	rpcsvcaddr.len = len16;
	clnt = clnt_tli_create(RPC_ANYFD, nconf, &rpcsvcaddr,
	    (uint_t)rpc_prog_num, (uint_t)rpc_prog_vers,
	    /* sendsz defaulted */ 0,	/* recvsz defaulted */ 0);
	if (clnt == NULL) {
#ifdef DEBUG
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "in libsecurity: program %s (%lu) debug: "
		    "clnt_create_file: clnt_tli_create failed: %s",
		    rpc_prog_name, rpc_prog_num, clnt_spcreateerror(""));
#endif
		goto err_ret;
	}

#ifdef DEBUG
	if (clnt_debug)
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "in libsecurity: program %s (%lu); debug: clnt_create_file"
		    " succeeded.", rpc_prog_name, rpc_prog_num);
#endif

err_ret:
	if (fd != -1)
		(void) close(fd);
	if (rpcsvcaddr.buf != NULL)
		free(rpcsvcaddr.buf);
	if (rpcfilename != NULL)
		free(rpcfilename);
#endif /* linux */
	return (clnt);
}
/* RPC Implementation Ends */
#endif /* DOOR_IMPL */


/*
****************************************************************************
	Server functions
*****************************************************************************
*/

/*
 * initialize and register service; performed at server startup
 */
#if DOOR_IMPL
/* DOOR Implementation Starts */
int
make_door_call(int serverfd, door_arg_t *arg)
{
	sc_syslog_msg_handle_t handle;
	sigset_t sig_set, orig_set;
	int err, i;

	/* block all signals during the door call */
	if (sigfillset(&sig_set) != 0) {
		/*
		 * We know errno is legitimate.
		 * Suppress the lint error.
		 */
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigfillset: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	if ((err = pthread_sigmask(SIG_SETMASK, &sig_set, &orig_set)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_sigmask: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}


	/*
	 * We retry at most twice for EAGAIN or EINTR before giving up.
	 */
	for (i = 0; i < 3; i++) {
		if (door_call(serverfd, arg) != 0) {
			err = errno;
			if (i < 2 && (err == EAGAIN || err == EINTR)) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGMPMF_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * A door_call failed with the specified
				 * reason.
				 * @user_action
				 * No action necessary.
				 */
				(void) sc_syslog_msg_log(handle, LOG_NOTICE,
				    MESSAGE,
				    "libsecurity, door_call: %s; will retry",
				    strerror(err));
				sc_syslog_msg_done(&handle);

				/* sleep, then retry */
				(void) sleep((uint_t)i + 1);
			} else {

				/*
				 * give up and print a user friendly error
				 * message based on the return error code
				 * wherever possible.
				 */
				switch (err) {
				case EOVERFLOW:
					/*
					 * Value too large for defined datatype
					 * Memory could not be allocated for
					 * copying the results to the client.
					 * This is a fatal error.
					 */
					(void) sc_syslog_msg_initialize(&handle,
					    SC_SYSLOG_RGMPMF_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * The client (libpmf/libfe/libscha)
					 * has run out of memory and the door
					 * call could not complete.
					 * @user_action
					 * Check for swap space and/or memory
					 * usage of the client process. Try
					 * rebooting the node and/or check the
					 * client process configuration. Save
					 * the /var/adm/messages files on each
					 * node. Contact your authorized Sun
					 * service provider for assistance.
					 */
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "libsecurity, door_call: Fatal, "
					    "could not allocate space for "
					    "return arguments, Client is "
					    "out of memory.");
					sc_syslog_msg_done(&handle);
					break;
				case EBADF:
					/*
					 * Bad file number
					 * This occurs when the door_server
					 * is dead.
					 */
					(void) sc_syslog_msg_initialize(&handle,
					    SC_SYSLOG_RGMPMF_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * The client (libpmf/libfe/libscha)
					 * is trying to communicate with the
					 * server (rpc.pmfd/rpc.fed/rgmd) but
					 * is failing because the server might
					 * be down.
					 * @user_action
					 * Save the /var/adm/messages files on
					 * each node. Contact your authorized
					 * Sun service provider to determine
					 * whether a workaround or patch is
					 * available.
					 */
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "libsecurity, door_call: Fatal, "
					    "the server is not available.");
					sc_syslog_msg_done(&handle);
					break;
				default:
					/*
					 * For other errors, which are less
					 * likely to occur we donot print
					 * any custom message.
					 */
					(void) sc_syslog_msg_initialize(&handle,
					    SC_SYSLOG_RGMPMF_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * A door_call failed with the
					 * specified reason, causing a Sun
					 * Cluster daemon to be terminated,
					 * and the node to be rebooted or
					 * halted.
					 * @user_action
					 * Save a copy of the
					 * /var/adm/messages files on all
					 * nodes. Contact your authorized Sun
					 * service provider for assistance in
					 * diagnosing the problem.
					 */
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "libsecurity, door_call: %s",
					    strerror(err));
					sc_syslog_msg_done(&handle);
					break;
				}
				return (-1);
			}
		} else {
			/* successful call */
			break;
		}
	}

	/* restore the original signal mask */
	if ((err = pthread_sigmask(SIG_SETMASK, &orig_set, NULL)) != 0) {
		char *err_str = strerror(err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "pthread_sigmask: %s", err_str ? err_str : "");
		sc_syslog_msg_done(&handle);
		exit(err);
	}

	return (0);
}

int
security_svc_reg(
	door_server *proc,		/* program */
	rgm_door_code_t program_num,
	int dbg, char *zone)			/* print debug error msgs? */
{

	sc_syslog_msg_handle_t sys_handle;
	int did;
	bool_t do_insert;
	char path[MAXPATHLEN];

	debug = dbg;
	if (debug)
		dbg_msgout(NOGET("entering security_svc_reg. \n"));

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	switch (program_num) {
	case RGM_SCHA_PROGRAM_CODE:
	case RGM_ZONEUP_CODE:
	case RGM_SYSEVENT_PROXY_CODE:
	case RGM_FE_PROGRAM_CODE:
		do_insert = TRUE;
		break;
	case RGM_PMF_PROGRAM_CODE:
		do_insert = FALSE;
		break;
	default:
		ASSERT(0);
		goto err_out;
	}

	/* Create door server */
	/* yslog(LOG_ERR, "creating door for server:%d", program_num); */
	if ((did = security_svc_door_create(proc)) == -1)  {
		goto err_out;
	}

	/* Attach the door file to the door server */
	if (security_svc_attach_door(did, program_num, zone) == -1) {
		goto err_out;
	}
	if (do_insert) {
		/*
		 * yslog(LOG_ERR,
		 *   "inserting door in kernel for server:%d", program_num);
		 */
		strcpy(path, rgm_door_paths[program_num]);

#if SOL_VERSION >= __s10
		if (zone != NULL) {
			strcat(path, zone);
		}
#endif
		/* create kernel entry for zones' access */
		if (security_svc_insert_door(
		    program_num, path, (const char *)zone) == -1) {
			goto err_out;
		}
	}
	return (did);

err_out:
	(void) sc_syslog_msg_initialize(&sys_handle,
	    SC_SYSLOG_RGMPMF_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * The specified daemon was not able to start because it could not
	 * initialize a door for communication with its clients. This causes
	 * the node to be rebooted or halted.
	 * @user_action
	 * Examine other syslog messages occurring at about the same time to
	 * see if the problem can be identified. Save the /var/adm/messages
	 * file. Contact your authorized Sun service provider to determine
	 * whether a workaround or patch is available.
	 */
	(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
	    MESSAGE,
	    "in libsecurity for program %s ; unable to "
	    "register service", rgm_door_programs[program_num]);
	sc_syslog_msg_done(&sys_handle);
	door_revoke(did);
	return (-1);
}

/*
 * security_svc_door_create
 * This function will start the door server specified by
 * the calling function and return the door id to the
 * calling function
 */
int
security_svc_door_create(door_server *proc)
{
	int did, err;
	sc_syslog_msg_handle_t sys_handle;

	/* Call door_create to create door server */
	if ((did = door_create(proc, NULL, 0)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A Sun Cluster daemon was unable to create a door for
		 * communication with its clients. A following syslog message
		 * provides the name of the daemon that failed. This might
		 * cause the node to be rebooted or halted.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
		    MESSAGE,
		    "in libsecurity ; unable to create door : %s",
		    strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (-1);
	}

	/* Return the door id */
	return (did);
}

#if SOL_VERSION >= __s10
/*
 * this function is only called for 1334 zones where we may have to add the
 * same server multiple times for different zones. so the server is always
 * going to be the global zone server.
 */
int security_svc_add_zone(rgm_door_code_t program_code, const char *zonename) {
	char path[MAXPATHLEN];
	strcpy(path, rgm_door_paths[program_code]);
	strcat(path, GLOBAL_ZONENAME);
	return (security_svc_insert_door(program_code, path, zonename));
}
#endif

/* used to register door in kernel for zones' access */
int security_svc_insert_door(
    rgm_door_code_t code, const char *path, const char *zonename) {

#if SOL_VERSION >= __s10
	char arg[sizeof (int) + MAXPATHLEN + ZONENAME_MAX];
#else
	char arg[sizeof (int) + MAXPATHLEN];
#endif
	char *tmp;

	/*
	 * yslog(LOG_ERR,
	 *   "insert door for door code:%d path:%s zone:%s",
	 *   code, path, zonename);
	 */

	/*
	 * ASSERT(strlen(rgm_door_paths[code]) + strlen(zonename) <=
	 *   MAXPATHLEN);
	 */
	memcpy(arg, &code, sizeof (int));
	tmp = arg + sizeof (int);

#if SOL_VERSION >= __s10
	ASSERT(strlen(zonename) <= ZONENAME_MAX);
	strcpy(tmp, zonename);
	tmp = arg + sizeof (int) + ZONENAME_MAX;
#endif
	strcpy(tmp, path);
	if (_cladm(CL_CONFIG, CL_RGM_STORE_DOOR, arg) != 0) {
		/* yslog(LOG_ERR, "insert door failed for code:%d", code); */
		return (1);
	}
	return (0);
}

/*
 * attach_door
 * This function will be called once by the security_svc_reg
 * to attach the door file to the door_server. However, if we
 * are running on or above Sol 10 then this function will be
 * called when we want to register another zone and allow
 * that zone also to be able to communicate via a door.
 * We will attach the door file in the zone to the door_server
 * in this function
 */
static int
security_svc_attach_door(int door_id, rgm_door_code_t program_num, char *zone)
{
	int err, fd;
	sc_syslog_msg_handle_t sys_handle;
	char door_path[MAXPATHLEN];
	strcpy(door_path, rgm_door_paths[program_num]);
	ASSERT(strlen(door_path) <= RGM_MAX_DOOR_PATHLEN);
#if SOL_VERSION >= __s10
	if (zone != NULL) {
		strcat(door_path, zone);
	}
#endif
	/* Remove the door file if it already exists */
	(void) unlink(door_path);

	/* Create the door file */
	if ((fd = open(door_path, O_CREAT | O_RDWR,
	    S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) <= 0) {
		/*
		 * We know that errno is legitimate
		 * Suppress the lint error.
		 */
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A Sun Cluster daemon was unable to create a door descriptor
		 * for communication with its clients. A following syslog
		 * message provides the name of the daemon that failed. This
		 * might cause the node to be rebooted or halted.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
		    MESSAGE,
		    "in libsecurity for program %s ; creat of file %s "
		    "failed: %s", rgm_door_programs[program_num],
		    door_path, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) close(fd);
		return (-1);
	}

	(void) close(fd);

	/* Attach the file to the door desc */
	if (fattach(door_id, door_path) != 0) {
		/*
		 * We know that errno is legitimate
		 * Suppress the lint error.
		 */
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A Sun Cluster daemon was unable to attach to a door
		 * descriptor for communication with its clients. A following
		 * syslog message provides the name of the daemon that failed.
		 * This might cause the node to be rebooted or halted.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR,
		    MESSAGE,
		    "in libsecurity for program %s ; fattach of file %d"
		    "to door failed : %s", rgm_door_paths[program_num],
		    door_path, strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (-1);
	}

	return (0);
}

/*
 * Server authentication function;
 * performed at beginning of each call.
 */
SEC_ERRCODE
security_svc_authenticate(door_cred_t *d_cred, bool_t user_is_root)
{
	uid_t 	desired_uid;
	SEC_ERRCODE rc = SEC_OK;
	sc_syslog_msg_handle_t sys_handle;

	if (debug)
		dbg_msgout(NOGET("Entering security_svc_authenticate"));

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* get the credentials of the callee */
	if (door_cred(d_cred) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * While processing a call from a client program, a Sun
		 * Cluster daemon was unable to obtain the credentials of the
		 * caller. This causes the attempted operation to fail.
		 * @user_action
		 * Make sure that no unauthorized programs are running on the
		 * cluster. If the call appears to come from a legitimate data
		 * service method or administrative command, contact your
		 * authorized Sun service provider to determine whether a
		 * workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity, unable to get client credential");
		sc_syslog_msg_done(&sys_handle);
		rc = SEC_EUNIXW;
	}

	if (user_is_root) {
		desired_uid = 0;
		if (d_cred->dc_euid != desired_uid) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * A cluster daemon refused a door connection from a
			 * client because of improper credentials. This might
			 * happen if a caller program is required to run as
			 * superuser or as a specific user ID, and fails to do
			 * so.
			 * @user_action
			 * Examine other syslog messages occurring at about
			 * the same time to see if the problem can be
			 * identified. Check that the clustering software is
			 * properly installed, and that no unauthorized user
			 * processes are running on the cluster. If these
			 * checks cannot explain the error, contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			LOG_ERR, MESSAGE,
			"in libsecurity, authorization failed");
			sc_syslog_msg_done(&sys_handle);
			rc = SEC_EUNIXW;
		}
	}
	return (rc);
}
#if SOL_VERSION >= __s10
/*
 * This function can be used by the to get the client zonename
 * from which the call has come over the door interface.
 * Note, that this function call is valid only if there is
 * an active door call, and can be called by the door server
 * code only.
 * The function expects an allocated buffer(zonename) of
 * size ZONENAME_MAX from the caller.
 */
boolean_t
get_doorclient_zonename(char *zonename)
{
	ucred_t	*zone_info = NULL;
	int	res = B_FALSE;
	sc_syslog_msg_handle_t sys_handle;

	if (door_ucred(&zone_info) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "libsecurity : Unable to get door client credentials");
		sc_syslog_msg_done(&sys_handle);
		goto end;
	}
	if (getzonenamebyid(ucred_getzoneid(zone_info),
	    zonename, ZONENAME_MAX) == -1) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "libsecurity : Unable to get zonename for door client");
		sc_syslog_msg_done(&sys_handle);
		goto end;
	}
	res = B_TRUE;
end:
	ucred_free(zone_info);
	return (res);
}
#endif /* SOL_VERSION */

/* DOOR Implementation Ends */
#else
/* RPC Implementation Starts */

#ifdef linux
/*
 * Linux: initialize and register service; performed at server startup
 */
int
security_svc_reg(
	void (*svc_program)(),		/* program */
	const char *program_name_in,	/* program name */
	const ulong_t program_num,	/* program number */
	const ulong_t version,		/* version number */
	int dbg)			/* print debug error msgs? */
{
	SVCXPRT *stp;
	sc_syslog_msg_handle_t sys_handle;
	const char *program_name;
	int error = 0;

	if (dbg)
		dbg_msgout(NOGET("entering security_svc_reg.\n"));

	debug = dbg;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (program_name_in == NULL) {
		program_name = "<unknown>";
	} else {
		program_name = program_name_in;
	}

	inited = TRUE;

	/*
	 * create a server handle for the network
	 * specified,
	 */
	stp = svctcp_create(RPC_ANYSOCK, 0, 0);
	if (stp == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity for program %s (%lu); "
		    "svctcp_create failed for transport tcp.",
		    program_name, program_num);
		sc_syslog_msg_done(&sys_handle);
		return (1);
	}
	pmap_unset(program_num, version);

	/*
	 * register handle with the portmap service.
	 */
	error = svc_register(stp, program_num,
	    version, svc_program, IPPROTO_TCP);
	if (1 != error) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity for program %s (%lu); "
		    "svc_register failed for transport tcp. "
		    "Remote clients won't be able to connect",
		    program_name, program_num);
		sc_syslog_msg_done(&sys_handle);
		return (1);
	}
	return (0);
}
#else

/*
 * Solaris: initialize and register service; performed at server startup
 */
int
security_svc_reg(
	void (*svc_program)(),		/* program */
	const char *program_name_in,	/* program name */
	const ulong_t program_num,	/* program number */
	const ulong_t version,		/* version number */
	int dbg)			/* print debug error msgs? */
{
	void *hp;
	struct netconfig *nconf;
	int	regcnt = 0;
	int	didnegotiatecnt = 0;
	SVCXPRT *stp;
	sc_syslog_msg_handle_t sys_handle;
	const char *program_name;

	if (dbg)
		dbg_msgout(NOGET("entering security_svc_reg.\n"));

	debug = dbg;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (program_name_in == NULL) {
		program_name = "<unknown>";
	} else {
		program_name = program_name_in;
	}

	/*
	 * Get and cache the list of loopback transports.
	 */
	if (security_svc_init() != 0)
		return (1);

	/*
	 * get network database handle
	 */
	if ((hp = setnetconfig()) == NULL) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The specified server was not able to initiate an rpc
		 * connection, because it could not get the network database
		 * handle. The server does not start. The rpc error message is
		 * shown. An error message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "in libsecurity for program %s (%lu); setnetconfig failed: "
		    "%s", program_name, program_num, nc_sperror());
		sc_syslog_msg_done(&sys_handle);
		return (1);
	}

	/*
	 * get network info from network database
	 */
	while ((nconf = getnetconfig(hp)) != NULL) {
		if (((strcmp(nconf->nc_protofmly, NC_LOOPBACK) == 0) &&
		    ((nconf->nc_semantics == NC_TPI_COTS_ORD) ||
		    (nconf->nc_semantics == NC_TPI_COTS))) ||
		    ((strcmp(nconf->nc_protofmly, NC_INET) == 0) &&
		    (strcmp(nconf->nc_proto, NC_TCP) == 0))) {

			int error = 0;
			bool_t err = FALSE;

			/*
			 * create a server handle for the network
			 * specified,
			 */
			stp = svc_tli_create(RPC_ANYFD, nconf, NULL, 0, 0);
			if (stp == NULL) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGMPMF_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * A server (rpc.pmfd, rpc.fed or rgmd) was
				 * not able to start on the specified
				 * transport. The server might stay up and
				 * running as long as the server can start on
				 * other transports. An error message is
				 * output to syslog.
				 * @user_action
				 * Save the /var/adm/messages file. Contact
				 * your authorized Sun service provider to
				 * determine whether a workaround or patch is
				 * available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "in libsecurity for program %s (%lu); "
				    "svc_tli_create failed for transport %s",
				    program_name, program_num,
				    nconf->nc_netid);
				sc_syslog_msg_done(&sys_handle);
				continue;
			}

			/*
			 * register handle with the rpcbind service.
			 */
			error = svc_reg(stp, (uint_t)program_num,
			    (uint_t)version, svc_program, nconf);
			if (1 != error) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGMPMF_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * A server (rpc.pmfd, rpc.fed or rgmd) was
				 * not able to register with rpcbind. The
				 * server should still be able to receive
				 * requests from clients using the "cache"
				 * files under /var/run/scrpc/ but remote
				 * clients won't be able to connect. An error
				 * message is output to syslog.
				 * @user_action
				 * Save the /var/adm/messages file. Contact
				 * your authorized Sun service provider to
				 * determine whether a workaround or patch is
				 * available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "in libsecurity for program %s (%lu); "
				    "svc_reg failed for transport %s. "
				    "Remote clients won't be able to connect",
				    program_name, program_num, nconf->nc_netid);
				sc_syslog_msg_done(&sys_handle);
			}

			/*
			 * write connection information into a file
			 */
			err = rpc_reg_file(stp, program_name,
			    program_num, version, nconf);
			if (FALSE == err) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGMPMF_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * A server (rpc.pmfd, rpc.fed or rgmd) was
				 * not able to create the "cache" file to
				 * duplicate the rpcbind information. The
				 * server should still be able to receive
				 * requests from clients (using rpcbind). An
				 * error message is output to syslog.
				 * @user_action
				 * Save the /var/adm/messages file. Contact
				 * your authorized Sun service provider to
				 * determine whether a workaround or patch is
				 * available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "in libsecurity for program %s (%lu); "
				    "file registration failed for "
				    "transport %s",
				    program_name, program_num,
				    nconf->nc_netid);
				sc_syslog_msg_done(&sys_handle);
			}

			if (FALSE == err && error != 1) {
				/*
				 * Unable to register with rpcbind AND
				 * unable to write into the file.
				 * Nobody will be able to connect using this
				 * transport so we destroy it.
				 */

				svc_destroy(stp);
				continue;
			}

			if (strcmp(nconf->nc_protofmly, NC_LOOPBACK) == 0) {

				if (__rpc_negotiate_uid(stp->xp_fd) == -1) {
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_RGMPMF_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * The specified server was not able
					 * to start because it could not
					 * establish a rpc connection for the
					 * network. An error message is output
					 * to syslog.
					 * @user_action
					 * Save the /var/adm/messages file.
					 * Contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "in libsecurity for program %s "
					    "(%lu); __rpc_negotiate_uid "
					    "failed for transport %s",
					    program_name,
					    program_num,
					    nconf->nc_netid);
					sc_syslog_msg_done(&sys_handle);
				} else {
					didnegotiatecnt++;
				}
			}

			regcnt++;
		}
	}

	/*
	 * close network configuration database and associated resources
	 */
	(void) endnetconfig(hp);



	if (debug)
		dbg_msgout(NOGET("regcnt=%d and didnegotiatecnt=%d\n"),
		    regcnt, didnegotiatecnt);

	/*
	 * we return if either there are no transports at all,
	 * or there are but none is a loopback
	 */
	if (regcnt == 0 || didnegotiatecnt == 0) {
		if (regcnt == 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * A server (rpc.pmfd, rpc.fed or rgmd) was not able
			 * to register with rpcbind and not able to create the
			 * "cache" files for any transport. An error message
			 * is output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "in libsecurity for program %s (%lu); "
			    "could not register on any transport in "
			    "/etc/netconfig", program_name, program_num);
			sc_syslog_msg_done(&sys_handle);
		} else if (didnegotiatecnt == 0) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * None of the available trasnport agreed to provide
			 * the uid of the clients to the specified server.
			 * This happened because either there are no available
			 * transports at all, or there are but none is a
			 * loopback. An error message is output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "in libsecurity for program %s (%lu); "
			    "could not negotiate uid on any loopback "
			    "transport in /etc/netconfig", program_name,
			program_num);
			sc_syslog_msg_done(&sys_handle);
		}
		return (1);
	}

	return (0);
}
#endif /* linux */

/*
 * get and cache the list of loopback transports
 * performed once per machine
 */
static int
security_svc_init()
{
#ifdef linux
	return (0);
#else

	struct netconfig *nconf;
	void *hp;
	int j;
	sc_syslog_msg_handle_t sys_handle;
	int	err1;
	int	tries;
	char    **tmp_ptr;

	if (debug)
		dbg_msgout(NOGET("entering security_svc_init.\n"));

	/*
	 * get lock to ensure that only 1 thread executes this section
	 */
	(void) mutex_lock(&inited_mutex);

	/*
	 * the service was already initialized so we release lock and return
	 */
	if (inited) {
		(void) mutex_unlock(&inited_mutex);
		return (0);
	}

	/*
	 * initialize our uts nodename
	 */
	if (uname(&server_utsn) == -1) {
		int err = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A client was not able to make an rpc connection to a server
		 * (rpc.pmfd, rpc.fed or rgmd) because the host name could not
		 * be obtained. The system error message is shown. An error
		 * message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity uname sys call failed: %s",
		    strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) mutex_unlock(&inited_mutex);
		return (1);
	}

	loopbackset.len = 0;
	loopbackset.a = NULL;

	/*
	 * get network database handle
	 */
	for (tries = 0; tries < NUM_RETRIES; tries++) {
		uint_t unslept = SLEEP_SECS_BETWEEN_TRIES;
		errno = 0;
		hp = (void *)setnetconfig();
		if (hp != NULL) break;
		/*
		 * sleep returns the amount of time left to sleep.
		 * Normally 0, this value can be positive if sleep is
		 * interrupted by a signal.  In that case we should continue
		 * sleeping for the remaining time.
		 */
		while (unslept > 0) {
			unslept = sleep(unslept);
		}
	}
	if (hp == NULL) {
		err1 = errno;
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A server (rpc.pmfd, rpc.fed or rgmd) was not able to start
		 * because it could not establish a rpc connection for the
		 * network specified. An error message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "in libsecurity setnetconfig failed "
		    "when initializing the server: %s - %s",
		    nc_sperror(), strerror(err1));
		sc_syslog_msg_done(&sys_handle);
		(void) mutex_unlock(&inited_mutex);
		return (1);
	}

	/*
	 * get network info from network database
	 */
	while ((nconf = getnetconfig(hp)) != NULL) {
		if ((strcmp(nconf->nc_protofmly, NC_LOOPBACK) == 0) &&
		    ((nconf->nc_semantics == NC_TPI_COTS_ORD) ||
		    (nconf->nc_semantics == NC_TPI_COTS))) {
			loopbackset.len++;
			tmp_ptr =
			    (char **)realloc(loopbackset.a, sizeof (char *)
			    * (uint_t)loopbackset.len);
			if (tmp_ptr == NULL) {
				(void) sc_syslog_msg_initialize(
				    &sys_handle,
				    SC_SYSLOG_RGMPMF_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * A server (rpc.pmfd, rpc.fed or rgmd) was
				 * not able to start, or a client was not able
				 * to make an rpc connection to the server.
				 * This problem can occur if the machine has
				 * low memory. An error message is output to
				 * syslog.
				 * @user_action
				 * Determine if the host is low on memory. If
				 * not, save the /var/adm/messages file.
				 * Contact your authorized Sun service
				 * provider to determine whether a workaround
				 * or patch is available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "in libsecurity realloc failed");
				sc_syslog_msg_done(&sys_handle);
				(void) endnetconfig(hp);
				(void) mutex_unlock(&inited_mutex);
				goto error_cleanup;
			}
			loopbackset.a = tmp_ptr;
			loopbackset.a[loopbackset.len - 1] =
			    estrdup(nconf->nc_netid);
			if (loopbackset.a[loopbackset.len - 1] == NULL) {
				(void) endnetconfig(hp);
				(void) mutex_unlock(&inited_mutex);
				goto error_cleanup;
			}
		}
	} /* end while */

	/*
	 * close network configuration database
	 */
	/* Cast endnetconfig() to void, endnetconfig is analogous to close() */
	(void) endnetconfig(hp);

	inited = TRUE;

	/*
	 * release lock
	 */
	(void) mutex_unlock(&inited_mutex);

	if (debug) {
		dbg_msgout(NOGET("security_svc_init: loopbackset is:\n"));
		for (j = 0; j < loopbackset.len; j++)
			dbg_msgout(NOGET("'%s'\n"), loopbackset.a[j]);
		dbg_msgout(NOGET("End of loopback set.\n"));

		dbg_msgout(NOGET("inited = %d.\n"), inited);
	}

	return (0);

error_cleanup:
	if (loopbackset.a) {
		for (j = 0; j < loopbackset.len; j++)
			free(loopbackset.a[j]);
		free(loopbackset.a);
	}
	return (1);
#endif /* linux */
}

/*
 * Server authentication function;
 * performed at beginning of each call.
 */
SEC_ERRCODE
security_svc_authenticate(
	struct svc_req *rqstp,
	/* unix credentials - output argument */
	struct authunix_parms *unix_cred,
	/* strong UNIX security or weak unix security */
	SEC_TYPE sec_type,
	/*
	 * if true uid of caller should be root,
	 * else it should be client uid
	 */
	bool_t user_is_root)
{
	uid_t   desired_uid;
	SEC_ERRCODE rc = SEC_OK;
	sc_syslog_msg_handle_t sys_handle;

	if (debug)
		dbg_msgout(NOGET("Entering security_svc_authenticate\n"
		    "sec_type=%d and flavor is=%d\n"),
		    sec_type, rqstp->rq_cred.oa_flavor);

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/*
	 * perform check for each security type
	 */
	switch (rqstp->rq_cred.oa_flavor) {
	case AUTH_SYS:
		if (debug)
			dbg_msgout(NOGET("in AUTH_SYS\n"));

		/* get unix credentials */
		*unix_cred =
		    *((struct authunix_parms *)rqstp->rq_clntcred);

		/* figure out desired uid */
		if (user_is_root) {
			desired_uid = 0;
		} else {
			/* note that uid actually contains euid information */
			desired_uid = unix_cred->aup_uid;
		}

		switch (sec_type) {
		case SEC_UNIX_WEAK:
			if (!check_authsys_security(rqstp, desired_uid)) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGMPMF_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * A cluster daemon refused an RPC connection
				 * from a client because it failed weak UNIX
				 * authentication.
				 * @user_action
				 * This form of authentication is currently
				 * not used by Sun Cluster software, so this
				 * message should not occur. Contact your
				 * authorized Sun service provider to
				 * determine whether a workaround or patch is
				 * available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "in libsecurity weak UNIX "
				    "authorization failed");
				sc_syslog_msg_done(&sys_handle);

				rc = SEC_EUNIXW;
			}
			break;
		case SEC_UNIX_STRONG:
			if (!check_security(rqstp, desired_uid)) {
				(void) sc_syslog_msg_initialize(&sys_handle,
				    SC_SYSLOG_RGMPMF_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * A cluster daemon refused an RPC connection
				 * from a client because it failed UNIX
				 * authentication. This might happen if a
				 * caller program is required to run as
				 * superuser or as a specific user ID, and
				 * fails to do so; or if the caller is
				 * attempting to access a Sun Cluster daemon
				 * from a remote host, in a case where only
				 * local node access is permitted.
				 * @user_action
				 * Examine other syslog messages occurring at
				 * about the same time to see if the problem
				 * can be identified. Check that the
				 * clustering software is properly installed,
				 * and that no unauthorized user processes are
				 * running on the cluster. If these checks
				 * cannot explain the error, contact your
				 * authorized Sun service provider to
				 * determine whether a workaround or patch is
				 * available.
				 */
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "in libsecurity strong UNIX "
				    "authorization failed");
				sc_syslog_msg_done(&sys_handle);
				rc = SEC_EUNIXS;
			}
			break;
		default:
			rc = SEC_EUNKN;
			break;
		}
		break;
	default:
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * This is an internal error which shouldn't occur. An error
		 * message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_NOTICE, MESSAGE,
		    "in libsecurity unknown security type %d",
		    rqstp->rq_cred.oa_flavor);
		sc_syslog_msg_done(&sys_handle);
		rc = SEC_EUNKN;
		break;
	}
	return (rc);
}

/*
 * check that a transport is in the set found at init time
 */
static bool_t inloopbackset(char *netid)
{
	int j;
	for (j = 0; j < loopbackset.len; j++) {
		if (strcmp(netid, loopbackset.a[j]) == 0)
			return (TRUE);
	}
	return (FALSE);
}

/*
 * The function has been introduced to resolve CR 4428124
 * and other related bugs marked as unreproducible.
 */
static void
print_svcxprt_structure(SVCXPRT *transp)
{
#ifndef linux
	char *nullstring = "null";

	dbg_msgout_onlysyslog(LOG_ERR,
	    NOGET("libsecurity:SVCXPRT data:xp_fd(int):%d:"
	    "xp_tp(string):%s:xp_netid(string):%s:xp_type(int):%d:\n"),
	    transp->xp_fd, transp->xp_tp ? transp->xp_tp : nullstring,
	    transp->xp_netid ? transp->xp_netid : nullstring, transp->xp_type);

	if (transp->xp_ops == NULL)
		dbg_msgout_onlysyslog(LOG_ERR,
		    NOGET("libsecurity:SVCXPRT xp_ops=NULL\n"));
	else {
		dbg_msgout_onlysyslog(LOG_ERR,
		    NOGET("libsecurity:SVCXPRT xp_ops data:"
		    "xp_recv:%x:xp_stat:%x:xp_getargs:%x:xp_reply:%x:"
		    "xp_freeargs:%x:xp_destroy:%x:xp_control:%x:\n"),
		    transp->xp_ops->xp_recv, transp->xp_ops->xp_stat,
		    transp->xp_ops->xp_getargs, transp->xp_ops->xp_reply,
		    transp->xp_ops->xp_freeargs, transp->xp_ops->xp_destroy,
		    transp->xp_ops->xp_control);
	}
	dbg_msgout_onlysyslog(LOG_ERR,
	    NOGET("libsecurity:SVCXPRT xp_ltaddr data:"
	    "maxlen:%u:len:%u:buf:%s\n"), transp->xp_ltaddr.maxlen,
	    transp->xp_ltaddr.len,
	    transp->xp_ltaddr.buf ? transp->xp_ltaddr.buf : nullstring);

	dbg_msgout_onlysyslog(LOG_ERR,
	    NOGET("libsecurity:SVCXPRT xp_rtaddr data:"
	    "maxlen:%u:len:%u:buf:%s\n"), transp->xp_rtaddr.maxlen,
	    transp->xp_rtaddr.len,
	    transp->xp_rtaddr.buf ? transp->xp_rtaddr.buf : nullstring);
#endif /* linux */
}

/*
 * Check unix security
 *
 * Returns TRUE if and only if caller is "desired_uid".
 * When FALSE, has side-effect on
 * rqstp of doing the proper call svcerr_weakauth.
 * Uses conjunction of two checks:
 * (1) That transport is a loopback transport and that
 *     __rpc_get_local_uid yields desired_uid, and
 * (2) That credentials of caller are authsys with desired_uid.
 * This is useful to filter out people trying to spoof us by getting rpcbind
 *     to make the call on us for them -- rpcbind could possibly be tricked
 *     into using local transport with desired_uid, thus passing test (1),
 *     but rpcbind is smart enough to insist on passing null credentials.
 * Note that either (1) or (2) by themselves is incomplete, especially
 * since (2) is easily spoofed.
 */
static
bool_t check_security(
	struct svc_req *rqstp,
	uid_t   desired_uid)
{
	uid_t getlocaluid = -1;
	uid_t authsysuid = -1;
	SVCXPRT *transp = rqstp->rq_xprt;
	bool_t result = TRUE;
	sc_syslog_msg_handle_t sys_handle;

	/*
	 * check that the server was started
	 */
	sec_assert(inited);

	if (debug)
		dbg_msgout(NOGET("Entering check_security inited=%d.\n"),
		    inited);

	/*
	 * if unix_strong, check local uid
	 */
#ifndef linux
	if (__rpc_get_local_uid(transp, &getlocaluid) == -1) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A server (rpc.pmfd, rpc.fed or rgmd) refused an rpc
		 * connection from a client because it failed the UNIX
		 * authentication, because it is not making the rpc call over
		 * the loopback interface. An error message is output to
		 * syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity __rpc_get_local_uid failed");
		sc_syslog_msg_done(&sys_handle);
		print_svcxprt_structure(transp);
		result = FALSE;
	}
#else
	/*
	 * Linux: __rpc_get_local_uid() is not available.  It needs
	 * to be replaced later.
	 */
#endif

	if (debug)
		dbg_msgout(
		    "check_security: caller's uid from transport "
		    "options is %d.\n",
		getlocaluid);

	/*
	 * check that transport is a loopback transport
	 */
#ifndef linux
	if (!inloopbackset(transp->xp_netid)) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A server (rpc.pmfd, rpc.fed or rgmd) refused an rpc
		 * connection from a client because the named transport is not
		 * a loopback. An error message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity transport %s "
		    "is not a loopback transport",
		    transp->xp_netid);
		sc_syslog_msg_done(&sys_handle);
		result = FALSE;
	}
#endif

	/*
	 * enforce matching desired authsys uid
	 */
	if (rqstp->rq_cred.oa_flavor != AUTH_SYS) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A server (rpc.pmfd, rpc.fed or rgmd) refused an rpc
		 * connection from a client because because the authorization
		 * is not of UNIX type. An error message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity cred flavor is not AUTH_SYS");
		sc_syslog_msg_done(&sys_handle);
		result = FALSE;
	} else {
#ifndef linux
		authsysuid = ((struct authsys_parms *)
		    rqstp->rq_clntcred)->aup_uid;
#else
		/*
		 * Linux only supports authunix credential.
		 */
		authsysuid = ((struct authunix_parms *)
		    rqstp->rq_clntcred)->aup_uid;
#endif
		if (debug)
			dbg_msgout(
			    "check_security: Caller's uid from authsys_parms "
			    "is %d.\n", authsysuid);
	}

	/*
	 * enforce matching desired uid
	 */
	if (result) {
#ifndef linux
		if ((getlocaluid != desired_uid) ||
		    (authsysuid != desired_uid))
#else
		/*
		 * Linux: __rpc_get_local_uid does not exist.
		 * Need to find replacement later.
		 */
		if (authsysuid != desired_uid)
#endif
		{
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * A server (rpc.pmfd, rpc.fed or rgmd) refused an rpc
			 * connection from a client because it has the wrong
			 * uid. The actual and desired uids are shown. An
			 * error message is output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "in libsecurity caller has bad uid: "
			    "get_local_uid=%d authsys=%d"
			    " desired uid=%d",
			    getlocaluid, authsysuid, desired_uid);
			sc_syslog_msg_done(&sys_handle);

			result = FALSE;
		}
	} /* end if result */

	/*
	 * call weak auth error function
	 */
	if (!result)
		svcerr_weakauth(transp);
	return (result);
}

/*
 * Check authsys security
 *
 * Returns TRUE iff caller has authsys credentials of "desired_uid".
 * When FALSE, has side-effect on rqstp of doing the proper call
 * on svcerr_weakauth.
 * We know that authsys root is easily spoofed but this is better
 * than nothing.
 */
static
bool_t check_authsys_security(struct svc_req *rqstp,
    uid_t desired_uid)
{
	uid_t   authsysuid = -1;
	SVCXPRT *transp = rqstp->rq_xprt;
	bool_t  result = TRUE;
	sc_syslog_msg_handle_t sys_handle;

	if (debug)
		dbg_msgout(NOGET("entering check_authsys_security.\n"));

	/*
	 * check that the request came in with the right flavor
	 * and check the uid against the desired uid
	 */
	if (rqstp->rq_cred.oa_flavor != AUTH_SYS) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity cred flavor is not AUTH_SYS");
		sc_syslog_msg_done(&sys_handle);

		result = FALSE;
	} else {
#ifndef linux
		authsysuid = ((struct authsys_parms *)
		    rqstp->rq_clntcred)->aup_uid;
#else
		/*
		 * Linux only supports authunix credential.
		 */
		authsysuid = ((struct authunix_parms *)
		    rqstp->rq_clntcred)->aup_uid;
#endif
		if (debug)
			dbg_msgout(NOGET(
			    "Caller's uid from authsys_parms is %d.\n"),
			    authsysuid);

		if (authsysuid != desired_uid) {
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * A server (rpc.pmfd, rpc.fed or rgmd) refused an rpc
			 * connection from a client because it has the wrong
			 * uid. The actual and desired uids are shown. An
			 * error message is output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "in libsecurity caller is %d,"
			    " not the desired uid %d",
			    authsysuid, desired_uid);
			sc_syslog_msg_done(&sys_handle);

			result = FALSE;
		}
	}

	/*
	 * call weak auth error function
	 */
	if (!result)
		svcerr_weakauth(transp);
	return (result);
}

/*
 * Duplicate string and print message if error.
 */
static char *estrdup(char *s)
{

	char *r;

	r = strdup(s);
	if (r == NULL)
		syslognomem();
	return (r);
}

/*
 * debug reporting function. variant of dbg_msgout() function.
 * prints only to syslog, and not to stderr.
 */

static void
dbg_msgout_onlysyslog(int priority, const char *msg, ...)
{
	char *newmsg = NULL;
	pthread_t tid = pthread_self();

	/* allocate for "tid <msg>\0" */
	newmsg = (char *)malloc(stringsize(sizeof (pthread_t)) + 1 +
	    strlen(msg) + 1);
	if (newmsg == NULL) {	/* ignore tid */
		newmsg = (char *)msg;
	} else {
		/* initialise newmsg to empty string in case sprintf fails */
		newmsg[0] = '\0';
		(void) sprintf(newmsg, "%u %s", tid, msg);
	}

	va_list ap;
	/*lint -save -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, msg);
	/*lint -restore */

	vsyslog(priority, newmsg, ap);

	va_end(ap);
	if (newmsg != msg)
		free(newmsg);
}
/* End of RPC Implementation */
#endif /* DOOR_IMPL */

/* Code Common to RPC and DOOR Implementation */



/*
 * Syslog message that we cannot allocate memory
 */
static void
syslognomem(void)
{
	sc_syslog_msg_handle_t sys_handle;

	(void) sc_syslog_msg_initialize(&sys_handle,
	    SC_SYSLOG_RGMPMF_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * A server (rpc.pmfd, rpc.fed or rgmd) was not able to start, or a
	 * client was not able to make an rpc connection to the server. This
	 * problem can occur of the machine has low memory. An error message
	 * is output to syslog.
	 * @user_action
	 * Determine if the host is low on memory. If not, save the
	 * /var/adm/messages file. Contact your authorized Sun service
	 * provider to determine whether a workaround or patch is available.
	 */
	(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
	    "in libsecurity could not allocate memory");
	sc_syslog_msg_done(&sys_handle);
}


/*
 * debug reporting function
 * This func should be called only if the server was started in debug mode.
 * It prints the msg to syslog and stderr and prepends it with the thread id.
 */

static void
dbg_msgout(const char *msg, ...)
{

	pthread_t tid = pthread_self();
	char *newmsg = NULL;
	va_list ap;

	/* allocate for "tid <msg>\0" */
	newmsg = (char *)malloc(stringsize(sizeof (pthread_t)) + 1 +
	    strlen(msg) + 1);
	if (newmsg == NULL) {	/* ignore tid part */
		newmsg = (char *)msg;
	} else {
		/* initialise newmsg to empty string in case sprintf fails */
		newmsg[0] = '\0';
		(void) sprintf(newmsg, "%u %s", tid, msg);
	}

	/*
	 * Suppress lint message for "Undeclared identifier"
	 * (__buildin_va_alist)
	 */
	va_start(ap, msg); /*lint !e40 !e26 !e50 !e10 */

	vsyslog(LOG_DEBUG, newmsg, ap);
	(void) vfprintf(stderr, newmsg, ap);

	va_end(ap);
	if (newmsg != msg)
		free(newmsg);

}



/*
*****************************************************************************
	Client functions
*****************************************************************************
*/
#if DOOR_IMPL
/* DOOR Implementation Starts */

/*
 * client authentication function
 * performed at beginning of each call
 */
bool_t security_clnt_connect(client_handle **door_handle,
	rgm_door_code_t program_num,
	SEC_TYPE sec_type,
	int dbg,
	int out, char *zonename)
{
#ifdef linux
	return (0); /* success */
#else
	sc_syslog_msg_handle_t sys_handle;
	char *program_name = NULL;
	int door_desc;
	bool_t retval = TRUE;
	bool_t get_direct = TRUE;

#if SOL_VERSION >= __s10
	zoneid_t zone_id = 0;
#endif

	dbg = 0;

	clnt_debug = dbg;
	clnt_output = out;

	if (clnt_debug)
		clnt_dbg_msgout(NOGET(
		    "entering security_clnt_connect.\n"));

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);


#if SOL_VERSION >= __s10
	/*
	 * if zoneid = 0, or pmfd,
	 * access serverpath directly
	 */
	if (/* strcmp(zonename, GLOBAL_ZONENAME) != 0 && */
	    program_num != RGM_PMF_PROGRAM_CODE) {
		get_direct = FALSE;
	}
#endif
	if (get_direct) {
		door_desc = get_door_direct(program_num, zonename);
	} else {
		door_desc = get_door_syscall(program_num, zonename);
	}
	if (door_desc < 0)
		return (FALSE);

	/*
	 * create handle to store args
	 */
	if (create_handle(door_handle, door_desc, program_name,
	    sec_type) != 0) {
		return (FALSE);
	}
	return (TRUE);
#endif /* linux */
}

int get_door_syscall(rgm_door_code_t program_code, char *zonename) {

#if SOL_VERSION < __s10
	return (-1);
#else
	door_info_t dinfo;
	char arg[MAXPATHLEN];
	int door_desc;
	char *tmp;

	if (program_code < 0 ||
	    program_code > RGM_MAX_DOOR_SYSCALL_CODE) {
		ASSERT(0);
		return (-1);
	}

	memcpy(arg, &program_code, sizeof (int));
	tmp = arg + sizeof (int);
	strcpy(tmp, zonename);

	if (_cladm(CL_CONFIG, CL_RGM_GET_DOOR, &arg) != 0) {
		return (-1);
	}
	memcpy(&door_desc, arg, sizeof (int));
	if (door_info(door_desc, &dinfo) < 0) {
		close(door_desc);
		return (-1);
	}
	return (door_desc);
#endif
}

int get_door_direct(rgm_door_code_t program_code, char *zonename) {

	sc_syslog_msg_handle_t sys_handle;
	int door_desc = -1;
	char door_path[MAXPATHLEN];

	if (program_code < 0 ||
	    program_code > RGM_MAX_DOOR_CODE) {
		return (-1);
	}
	strcpy(door_path, rgm_door_paths[program_code]);
	if (zonename != NULL) {
#if SOL_VERSION >= __s10
		strcat(door_path, zonename);
#else
		ASSERT(0);
		return (-1);
#endif
	}
	if ((door_desc = open(door_path, O_RDONLY)) < 0) {
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A process running in a non-global zone was unable to
		 * communicate with the cluster framework in the global zone.
		 * @user_action
		 * Save the /var/adm/messages files on each node. Contact your
		 * authorized Sun service provider to determine whether a
		 * workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "Unable to open door descriptor %s", door_path);
		sc_syslog_msg_done(&sys_handle);
	}
	return (door_desc);
}

/*
 * after client call, release RPC resources
 */
void
security_clnt_freebinding(client_handle *door_handle)
{
#ifdef linux
	return;
#else
	if (clnt_debug)
		clnt_dbg_msgout(NOGET(
		    "entering security_clnt_freebinding.\n"));

	if (door_handle == NULL)
		return;
	else {
		(void) close(door_handle->door_desc);
		free(door_handle);
	}
#endif
}

/*
 * create a struct that stores the client info
 */
static int create_handle(client_handle **handle,
    int door_desc, char *program_name, SEC_TYPE sec_type)
{
	client_handle *door_handle = NULL;
	if (clnt_debug)
		clnt_dbg_msgout(NOGET(
		    "entering create_handle.\n"));
	if ((door_handle = (client_handle *)malloc(
	    sizeof (client_handle))) ==   NULL) {
		syslognomem();
		return (-1);
	}
	door_handle->door_desc = door_desc;
	door_handle->program_name = program_name;
	door_handle->sec_type = sec_type;
	*handle = door_handle;
	return (0);
}

/* Door Implementation Ends */

#else

/* RPC Implementation Starts */

/*
 * get and cache the list of loopback and tcp transports
 * performed once per machine
 */
static int clnt_init()
{
#ifdef linux
	return (0);
#else

	struct netconfig *nconf;
	void *hp;
	int j;
	sc_syslog_msg_handle_t sys_handle;
	int	err1;
	int	tries;
	char    **tmp_ptr, **tcp_tmp_ptr;

	if (clnt_debug)
		clnt_dbg_msgout(NOGET("entering clnt_init.\n"));

	/*
	 * get lock to ensure that only 1 thread executes this section
	 */
	(void) mutex_lock(&inited_mutex);

	/*
	 * the service was already initialized so we return
	 */
	if (clnt_inited) {
		(void) mutex_unlock(&inited_mutex);
		return (0);
	}

	loopbackset.len = 0;
	loopbackset.a = NULL;

	tcpset.len = 0;
	tcpset.a = NULL;

	/*
	 * get network database handle
	 */
	for (tries = 0; tries < NUM_RETRIES; tries++) {
		uint_t unslept = SLEEP_SECS_BETWEEN_TRIES;
		errno = 0;
		hp = (void *)setnetconfig();
		if (hp != NULL) break;
		/*
		 * sleep returns the amount of time left to sleep.
		 * Normally 0, this value can be positive if sleep is
		 * interrupted by a signal.  In that case we should continue
		 * sleeping for the remaining time.
		 */
		while (unslept > 0) {
			unslept = sleep(unslept);
		}
	}
	if (hp == NULL) {
		err1 = errno;
		if (clnt_output)
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "in libsecurity setnetconfig failed "
			    "when initializing the client: %s - %s\n"),
			    nc_sperror(), strerror(err1));
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A client was not able to make an rpc connection to a server
		 * (rpc.pmfd, rpc.fed or rgmd) because it could not establish
		 * a rpc connection for the network specified. The rpc error
		 * and the system error are shown. An error message is output
		 * to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity setnetconfig failed "
		    "when initializing the client: %s - %s",
		    nc_sperror(), strerror(err1));
		sc_syslog_msg_done(&sys_handle);
		(void) mutex_unlock(&inited_mutex);
		return (1);
	}

	/*
	 * get network info from network database
	 */
	while ((nconf = getnetconfig(hp)) != NULL) {
		if ((strcmp(nconf->nc_protofmly, NC_LOOPBACK) == 0) &&
		    (nconf->nc_semantics == NC_TPI_COTS_ORD)) {
			loopbackset.len++;
			tmp_ptr =
			    (char **)realloc(loopbackset.a, sizeof (char *)
			    * (uint_t)loopbackset.len);
			if (tmp_ptr == NULL) {
				if (clnt_output)
					(void) fprintf(stderr, (const char *)
					    dgettext(TEXT_DOMAIN,
					    "in libsecurity realloc "
					    "failed\n"));
				(void) sc_syslog_msg_initialize(
				    &sys_handle,
				    SC_SYSLOG_RGMPMF_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "in libsecurity realloc failed");
				sc_syslog_msg_done(&sys_handle);
				(void) endnetconfig(hp);
				(void) mutex_unlock(&inited_mutex);
				goto error_cleanup;
			}
			loopbackset.a = tmp_ptr;
			loopbackset.a[loopbackset.len - 1] =
			    clnt_estrdup(nconf->nc_netid);
			if (loopbackset.a[loopbackset.len - 1] == NULL) {
				(void) endnetconfig(hp);
				(void) mutex_unlock(&inited_mutex);
				goto error_cleanup;
			}
		} else if ((strcmp(nconf->nc_protofmly, NC_INET) == 0) &&
		    (strcmp(nconf->nc_proto, NC_TCP) == 0)) {
			tcpset.len++;
			tcp_tmp_ptr =
			    (char **)realloc(tcpset.a, sizeof (char *)
			    * (uint_t)tcpset.len);
			if (tcp_tmp_ptr == NULL) {
				if (clnt_output)
					(void) fprintf(stderr, (const char *)
					    dgettext(TEXT_DOMAIN,
						"in libsecurity realloc "
						"failed\n"));
				(void) sc_syslog_msg_initialize(
				    &sys_handle,
				    SC_SYSLOG_RGMPMF_TAG, "");
				(void) sc_syslog_msg_log(sys_handle,
				    LOG_ERR, MESSAGE,
				    "in libsecurity realloc failed");
				sc_syslog_msg_done(&sys_handle);

				(void) endnetconfig(hp);
				(void) mutex_unlock(&inited_mutex);
				goto error_cleanup;
			}
			tcpset.a = tcp_tmp_ptr;
			tcpset.a[tcpset.len - 1] =
			    clnt_estrdup(nconf->nc_netid);
			if (tcpset.a[tcpset.len - 1] == NULL) {
				(void) endnetconfig(hp);
				(void) mutex_unlock(&inited_mutex);
				goto error_cleanup;
			}
		}
	} /* end while */

	/*
	 * close network configuration database
	 */
	/* purposely ignore return value of endnetconfig; analogous to close */
	(void) endnetconfig(hp);

	clnt_inited = TRUE;

	/*
	 * release lock
	 */
	(void) mutex_unlock(&inited_mutex);

	if (clnt_debug) {
		clnt_dbg_msgout(NOGET("clnt_init: loopbackset is:\n"));
		for (j = 0; j < loopbackset.len; j++)
			clnt_dbg_msgout("'%s'\n", loopbackset.a[j]);
		clnt_dbg_msgout(NOGET("End of loopback set.\n"));
		clnt_dbg_msgout(NOGET("clnt_init: tcpset is:\n"));
		for (j = 0; j < tcpset.len; j++)
			clnt_dbg_msgout(NOGET("'%s'\n"), tcpset.a[j]);
		clnt_dbg_msgout(NOGET("End of tcp set.\n"));
		clnt_dbg_msgout(NOGET("inited = %d.\n"), inited);
	}

	return (0);

error_cleanup:
	if (loopbackset.a) {
		for (j = 0; j < loopbackset.len; j++)
			free(loopbackset.a[j]);
		free(loopbackset.a);
	}
	if (tcpset.a) {
		for (j = 0; j < tcpset.len; j++)
			free(tcpset.a[j]);
		free(tcpset.a);
	}
	return (1);
#endif /* linux */
}


/*
 * client authentication function
 * performed at beginning of each call
 */
bool_t security_clnt_connect(CLIENT **clnt,
	char *remHostName,
	const char *program_name_in,
	ulong_t program_num,
	ulong_t version,
	SEC_TYPE sec_type,
	int dbg,
	int out)
{
	struct  utsname utsn;
	clnt_handlerecord  hh;
	struct timeval timeout;
	clnt_h  h = &hh;
	sc_syslog_msg_handle_t sys_handle;
	const char *program_name;

	/*
	 * set the debug variable
	 */
	clnt_debug = dbg;

	/*
	 * set the output destination variable
	 */
	clnt_output = out;

	if (program_name_in == NULL) {
		program_name = "<unknown>";
	} else {
		program_name = program_name_in;
	}

	if (clnt_debug)
		clnt_dbg_msgout(NOGET(
		    "entering security_clnt_connect.\n"));

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/*
	 * get node name
	 */
	if (uname(&utsn) == -1) {
		int err = errno;
		if (clnt_output)
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "clnt_connect for program %s (%lu): "
			    "uname sys call failed: %s\n"), program_name,
			    program_num, strerror(err));
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A client was not able to make an rpc connection to the
		 * specified server because the host name could not be
		 * obtained. The system error message is shown. An error
		 * message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity contacting program %s (%lu); "
		    "uname sys call failed: %s", program_name, program_num,
		    strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (FALSE);
	}

	/*
	 * get and cache the list of loopback and tcp transports
	 */
	if (clnt_init() != 0)
		return (FALSE);

	/*
	 * create handle to store args
	 */
	create_handle(h, remHostName, program_name, program_num, version,
	    sec_type);
	if (h->host_name == NULL) {
		if (clnt_output)
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "in libsecurity contacting program %s (%lu): "
			    "could not copy host name\n"), program_name,
			    program_num);
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A client was not able to make an rpc connection to the
		 * specified server because the host name could not be saved.
		 * This problem can occur if the machine has low memory. An
		 * error message is output to syslog.
		 * @user_action
		 * Determine if the host is low on memory. If not, save the
		 * /var/adm/messages file. Contact your authorized Sun service
		 * provider to determine whether a workaround or patch is
		 * available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity contacting program %s (%lu): "
		    "could not copy host name", program_name, program_num);
		sc_syslog_msg_done(&sys_handle);
		return (FALSE);
	}

	/*
	 * check if we're trying to connect to the local host
	 */
	if (strcmp(utsn.nodename, remHostName) == 0) {
		if (clnt_connect_local(h))
			*clnt = h->clnt;
		else {
			free(h->host_name);
			return (FALSE);
		}
	} else {
		/*
		 * call the remote connect function
		 * log debug msgs to identify libsecurity issues
		 */

		dbg_msgout_onlysyslog(LOG_NOTICE,
		    NOGET("libsecurity: utsn.nodename:<%s>."
		    " remHostName:<%s>. Calling clnt_connect_remote()"),
		    utsn.nodename, remHostName);

		if (clnt_connect_remote(h))
			*clnt = h->clnt;
		else {
			free(h->host_name);
			return (FALSE);
		}
	} /* end if local */

	/*
	 * we sometimes (fed) block waiting for the reply, so we need to
	 * set the timeout to a large number (3 months).
	 */
	timeout.tv_usec = 0;
	timeout.tv_sec = 776000;
	if (clnt_control(*clnt, CLSET_TIMEOUT, (caddr_t)&timeout) == FALSE) {
		if (clnt_output)
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "clnt_connect for program %s (%lu); "
			    "could not set timeout: %s\n"), program_name,
			    program_num, clnt_spcreateerror("libsecurity"));
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A client was not able to make an rpc connection to the
		 * specified server because it could not set the rpc call
		 * timeout. The rpc error is shown. An error message is output
		 * to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "could not set timeout for program %s (%lu): %s",
		    program_name, program_num,
		    clnt_spcreateerror("libsecurity"));
		sc_syslog_msg_done(&sys_handle);
		return (FALSE);
	}

	/*
	 * free handle contents
	 */
	if (h->host_name)
		free(h->host_name);

	return (TRUE);
}

/*
 * trygetlocalbinding
 *
 * Tries to get a local binding for the transport named trans_name.
 * The fact that it is a local binding causes us to retry for a large
 * number of times.
 *
 * Strategy:  In a loop, do the following.  First try
 * to create the binding, with a long timeout on the clnt_create
 * request.  If that succeeds, make a NULLPROC RPC call with that
 * binding, with a long timeout.  If both of those succeed, we
 * are done: return.  Otherwise, keep retrying, i.e. keep looping.
 *
 * Note that a very limited set of clnt_create errors cause us
 * to give up after a fixed amount of attempts.
 */
static bool_t
trygetlocalbinding(clnt_h h, char *trans_name)
{
	struct netconfig *nconf = NULL;
	int tries = 0;
	struct timeval timeout;
	enum clnt_stat rpc_stat;
	bool_t authflag = B_TRUE;
	bool_t retval = B_TRUE;
	sc_syslog_msg_handle_t sys_handle;
	hrtime_t start_hr;
	bool_t time_expired = B_FALSE;
	bool_t keepgoing = B_TRUE;
	uint_t unslept;

#ifndef linux
	if ((nconf = (struct netconfig *)getnetconfigent(trans_name)) == NULL) {

		if (clnt_output)
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "libsecurity for program %s (%lu): "
			    "unexpected getnetconfigent error\n"),
			    h->program_name, h->program_num);
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A client of the specified server was not able to initiate
		 * an rpc connection, because it could not get the network
		 * information. The pmfadm or scha command exits with error.
		 * An error message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "libsecurity: program %s (%lu); unexpected getnetconfigent"
		    " error", h->program_name, h->program_num);
		sc_syslog_msg_done(&sys_handle);
		return (B_FALSE);
	}
#endif /* linux */

#define	CLNT_CREATE_TIMEOUT_SECS 300 /* Five minutes */
#define	CLNT_CALL_TIMEOUT_SECS 300 /* Five minutes */

#define	ERR_ON_TRY 6
	/*
	 * ERR_ON_TRY is big enough that we retry five times (six
	 * tries total) before printing any error messages at all.
	 * Since we sleep 10 seconds between tries, it will take
	 * us at least 50 seconds before we print any error messages.
	 * The kernel support for user-land failfast in Sun Cluster
	 * fires after 45 seconds.  Since we sleep at least 50
	 * seconds, it will usually be the case that if one
	 * of the server daemon that we were trying to RPC to
	 * dies with a failfast, that this routine will not get
	 * a chance to print messages before the kernel failfast
	 * fires.  Which is what we want, because we don't want
	 * to annoy the customer with messages from this module
	 * if the underlying cause is that the server daemon died.
	 */

#define	HR_PER_HOUR	((hrtime_t)3600*1000000000)
	/*
	 * An hr is one nanosecond.  There are one billion of them (10^9)
	 * in a second.  There are 3600 seconds in an hour.  The cast
	 * to hrtime_t is to make sure that the C compiler realizes
	 * that this constant is of type hrtime_t and is thus a
	 * very wide integer:  when C does its mixed mode arithmetic
	 * conversions, it will realize that this the arithmetic
	 * or comparisons need to be done as very wide integers.
	 */

	start_hr = gethrtime();
	tries = 0;
	while (1) {
		tries++;
		timeout.tv_sec = CLNT_CREATE_TIMEOUT_SECS;
		timeout.tv_usec = 0;

		h->clnt = NULL;
		if (tries == 1) {
			/*
			 * The first time thru the loop, we try to get
			 * the rpc info from the file.  If that info
			 * turns out to be no good downstream below
			 * when we try to make the NULL RPC, we don't
			 * want to use it again, hence, we only use
			 * it the first time thru this loop.
			 */
			h->clnt = clnt_create_file(h->program_name,
			    h->program_num, h->version, nconf);
		}
		if (h->clnt == NULL) {
			/*
			 * Either tries is >1 or clnt_create_file failed
			 * just above.
			 * Call rpc library which will consult rpcbind
			 * for the information.
			 */
#ifndef linux
			h->clnt = clnt_tp_create_timed(h->host_name,
			    (uint_t)(h->program_num),
			    (uint_t)(h->version), nconf,
			    &timeout);
#else
			h->clnt = clnt_create(h->host_name, h->program_num,
			    h->version, "tcp");
#endif
		}

		time_expired = ((gethrtime() - start_hr) > HR_PER_HOUR);
		if (h->clnt == NULL) {
			/*
			 * We log just every ERR_ON_TRY'th occurrence,
			 * and also when time has expired.
			 */
			if ((tries % ERR_ON_TRY == 0) || time_expired) {
				keepgoing = B_TRUE;

				if (time_expired) keepgoing = B_FALSE;
				/*
				 * lint doesn't understand
				 * rpc_createerr.cf_stat, so we
				 * suppress the warning.
				 */
				/* CSTYLED */
				/*lint -e746 */
				switch (rpc_createerr.cf_stat) {
				case RPC_VERSMISMATCH:
				case RPC_PROGUNAVAIL:
				case RPC_PROGVERSMISMATCH:
				case RPC_UNKNOWNPROTO:
				case RPC_PROGNOTREGISTERED:
					keepgoing = B_FALSE;
					break;
				default:
					break;
				/*
				 * Suppress lint message about "enum symbol
				 * not used within defaulted switch", there's
				 * a boatload of RPC_xx errors which don't
				 * interest us.
				 */
				/* CSTYLED */
				} /* switch */  /*lint !e788 */

				/*
				 * The decision to log here even though
				 * we are not ifdef DEBUG is deliberate.
				 * We have already retried many times
				 * at this point, so something is
				 * severely screwed up (or overloaded)
				 * about this cluster node.
				 */

				if (keepgoing) {
					if (clnt_output)
						(void) fprintf(stderr,
						    (const char *)
						    dgettext(TEXT_DOMAIN,
							"libsecurity: create "
							"of rpc handle to "
							"program %s (%lu) "
							"failed, will keep "
							"trying\n"),
						    h->program_name,
						    h->program_num);
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_RGMPMF_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * A client of the specified server
					 * was not able to initiate an rpc
					 * connection. The maximum time
					 * allowed for connecting (1 hr) has
					 * not been reached yet, and the
					 * pmfadm or scha command will retry
					 * to connect. An accompanying error
					 * message shows the rpc error data.
					 * The program number is shown. To
					 * find out what program corresponds
					 * to this number, use the rpcinfo
					 * command. An error message is output
					 * to syslog.
					 * @user_action
					 * Save the /var/adm/messages file.
					 * Contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					(void) sc_syslog_msg_log(
					    sys_handle,
					    LOG_ERR, MESSAGE,
					    "libsecurity: create of rpc "
					    "handle to program %s (%lu) "
					    "failed, will keep "
					    "trying",
					    h->program_name,
					    h->program_num);
					sc_syslog_msg_done(&sys_handle);
				} else {
					if (clnt_output)
						(void) fprintf(stderr,
						(const char *)
						    dgettext(TEXT_DOMAIN,
							"libsecurity: create "
							"of rpc handle to "
							"program %s (%lu) "
							"failed, will not "
							"retry\n"),
						    h->program_name,
						    h->program_num);
					(void) sc_syslog_msg_initialize(
					    &sys_handle,
					    SC_SYSLOG_RGMPMF_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * A client of the specified server
					 * was not able to initiate an rpc
					 * connection, after multiple retries.
					 * The maximum time allowed for
					 * connecting has been exceeded, or
					 * the types of rpc errors encountered
					 * indicate that there is no point in
					 * retrying. An accompanying error
					 * message shows the rpc error data.
					 * The pmfadm or scha command exits
					 * with error. The program number is
					 * shown. To find out what program
					 * corresponds to this number, use the
					 * rpcinfo command. An error message
					 * is output to syslog.
					 * @user_action
					 * Save the /var/adm/messages file.
					 * Contact your authorized Sun service
					 * provider to determine whether a
					 * workaround or patch is available.
					 */
					(void) sc_syslog_msg_log(sys_handle,
					    LOG_ERR, MESSAGE,
					    "libsecurity: create of rpc handle "
					    "to program %s (%lu) failed, will "
					    "not retry",
					    h->program_name,
					    h->program_num);
					sc_syslog_msg_done(&sys_handle);
				}
				print_rpc_createerr(h);

				if (! keepgoing) {
					retval = B_FALSE;
					break;
				}

			} /* if tries */
			/*
			 * sleep returns the amount of time left to sleep.
			 * Normally 0, this value can be positive if sleep is
			 * interrupted by a signal.  In that case we should
			 * continue sleeping for the remaining time.
			 */
			unslept = SLEEP_SECS_BETWEEN_TRIES;
			while (unslept > 0) {
				unslept = sleep(unslept);
			}
			continue;
		} /* if NULL */
		/*
		 * The client side binding handle has been created
		 */
		authflag = clnt_authenticate(h->clnt, h->sec_type);
		if (!authflag) {
			if (clnt_output)
				(void) fprintf(stderr, (const char *)
				    dgettext(TEXT_DOMAIN,
					"libsecurity: program %s (%lu); "
					"clnt_authenticate failed\n"),
				    h->program_name,
				    h->program_num);
			(void) sc_syslog_msg_initialize(
			    &sys_handle,
			SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * A client of the specified server was not able to
			 * initiate an rpc connection, because it failed the
			 * authentication process. The pmfadm or scha command
			 * exits with error. An error message is output to
			 * syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(
			    sys_handle, LOG_ERR, MESSAGE,
			    "libsecurity: program %s (%lu); "
			    "clnt_authenticate failed",
			    h->program_name,
			    h->program_num);
			sc_syslog_msg_done(&sys_handle);
			retval = B_FALSE;
			break;
		}

		/*
		 * Next, attempt the NULLPROC RPC call
		 */
		timeout.tv_sec = CLNT_CALL_TIMEOUT_SECS;
		timeout.tv_usec = 0;
		rpc_stat = CLNT_CALL(h->clnt, NULLPROC,
		    (xdrproc_t)xdr_void, (char *)NULL,
		    (xdrproc_t)xdr_void, (char *)NULL,
		    timeout);
		if (rpc_stat == RPC_SUCCESS) break;
		/*
		 * The NULL RPC call failed.  Log it.  The decision to
		 * log here *always* is deliberate.  The error is very
		 * rare and we must try to isolate it by getting
		 * the error logged all the time.  If the RPC
		 * error were to occur on a non-NULL call, we
		 * would be truly in trouble, because non-NULL
		 * calls are non-idempotent, in general.  For those
		 * non-idempotent non-NULL calls, retry is not
		 * possible.  Hence, even though we retry here
		 * for the NULL call, we do not want to suppress
		 * the error output.  Restating, we want the
		 * error output to come out even though we are
		 * going to retry.
		 */

		time_expired = ((gethrtime() - start_hr) > HR_PER_HOUR);
		keepgoing = ! time_expired;

		if (clnt_output) {
			if (keepgoing) {
				(void) fprintf(stderr, (const char *)
					dgettext(TEXT_DOMAIN,
					    "libsecurity: NULL RPC to "
					    "program %s (%lu) failed; "
					    "will retry %s\n"),
				    h->program_name, h->program_num,
				    clnt_sperror(h->clnt, ""));
			} else {
				(void) fprintf(stderr, (const char *)
				    dgettext(TEXT_DOMAIN,
				    "libsecurity: NULL RPC to "
				    "program %s (%lu) failed; "
				    "will not retry %s\n"),
				    h->program_name, h->program_num,
				    clnt_sperror(h->clnt, ""));
			}
		} /* if clnt_output */
		(void) sc_syslog_msg_initialize(
		    &sys_handle, SC_SYSLOG_RGMPMF_TAG, "");
		if (keepgoing) {
			/*
			 * SCMSGS
			 * @explanation
			 * A client of the specified server was not able to
			 * initiate an rpc connection, because it could not
			 * execute a test rpc call, and the program will retry
			 * to establish the connection. The message shows the
			 * specific rpc error. The program number is shown. To
			 * find out what program corresponds to this number,
			 * use the rpcinfo command. An error message is output
			 * to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(
			    sys_handle, LOG_ERR, MESSAGE,
			    "libsecurity: NULL RPC to program %s (%lu) "
			    "failed; will retry %s",
			    h->program_name, h->program_num,
			    clnt_sperror(h->clnt, ""));
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * A client of the specified server was not able to
			 * initiate an rpc connection, because it could not
			 * execute a test rpc call. The program will not retry
			 * because the time limit of 1 hr was exceeded. The
			 * message shows the specific rpc error. The program
			 * number is shown. To find out what program
			 * corresponds to this number, use the rpcinfo
			 * command. An error message is output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(
			    sys_handle, LOG_ERR, MESSAGE,
			    "libsecurity: NULL RPC to program %s (%lu) "
			    "failed; will not retry %s",
			    h->program_name, h->program_num,
			    clnt_sperror(h->clnt, ""));
		}
		sc_syslog_msg_done(&sys_handle);

		auth_destroy(h->clnt->cl_auth);
		h->clnt->cl_auth = NULL;
		clnt_destroy(h->clnt);
		h->clnt = NULL;

		if (! keepgoing) {
			retval = B_FALSE;
			break;
		}
		/*
		 * sleep returns the amount of time left to sleep.
		 * Normally 0, this value can be positive if sleep is
		 * interrupted by a signal.  In that case we should continue
		 * sleeping for the remaining time.
		 */
		unslept = SLEEP_SECS_BETWEEN_TRIES;
		while (unslept > 0) {
			unslept = sleep(unslept);
		}
	} /* while loop */
#ifndef linux
	freenetconfigent(nconf);
#endif
	return (retval);
}

/*
 * establish connection for local (client and host are on same host)
 */
static bool_t clnt_connect_local(clnt_h h)
{

	if (clnt_debug)
		clnt_dbg_msgout(NOGET("entering clnt_connect_local.\n"));

	/*
	 * Get the connection-less (pkt) binding.
	 * Just use the first one in the loopbackset (there should
	 * only be one in the set anyway).
	 */
#ifndef linux
	if (loopbackset.len == 0)
		return (FALSE);
	return (trygetlocalbinding(h, loopbackset.a[0]));
#else
	/*
	 * Linux does not support netconfig.
	 */
	return (trygetlocalbinding(h, NULL));
#endif
}


/*
 * get connection for case when client and server are not on same host
 */
static bool_t clnt_connect_remote(clnt_h h)
{
	int j = 0;
	sc_syslog_msg_handle_t sys_handle;

	if (clnt_debug)
		clnt_dbg_msgout(NOGET("entering clnt_connect_remote.\n"));

	/*
	 * Get the connection-oriented (stream) binding.
	 * go over tcp transports cached and try to get binding
	 */
#ifndef linux
	for (j = 0; j < tcpset.len; j++) {
		if (trygetbinding(h, tcpset.a[j]))
			return (TRUE);
	}
#else
	/*
	 * Linux does not support netconfig.
	 */
	if (trygetbinding(h, NULL))
		return (TRUE);
#endif

	/*
	 * Didn't find it.  Give up.
	 */
	if (clnt_output)
		(void) fprintf(stderr, (const char *) dgettext(TEXT_DOMAIN,
		    "in libsecurity for program %s (%lu); could not find any "
		    "tcp transport\n"), h->program_name, h->program_num);
	(void) sc_syslog_msg_initialize(&sys_handle,
	    SC_SYSLOG_RGMPMF_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * A client was not able to make an rpc connection to the specified
	 * server because it could not find a tcp transport. An error message
	 * is output to syslog.
	 * @user_action
	 * Save the /var/adm/messages file. Contact your authorized Sun
	 * service provider to determine whether a workaround or patch is
	 * available.
	 */
	(void) sc_syslog_msg_log(sys_handle,
	    LOG_ERR, MESSAGE,
	    "in libsecurity for program %s (%lu); could not find any tcp "
	    "transport", h->program_name, h->program_num);
	sc_syslog_msg_done(&sys_handle);
	return (FALSE);

}

/*
 * Tries to get a binding using the transport named transname
 * Returns non-NULL for success.
 */
static bool_t trygetbinding(clnt_h h, char *trans_name)
{
#ifndef linux
	struct netconfig *nconf;
#endif
	bool_t  rc = TRUE;
	sc_syslog_msg_handle_t sys_handle;

	if (clnt_debug)
		clnt_dbg_msgout(NOGET("entering trygetbinding.\n"));

#ifndef linux
	if ((nconf = (struct netconfig *)getnetconfigent(trans_name)) == NULL) {

		if (clnt_output)
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "libsecurity: getnetconfigent error: %s\n"),
			    nc_sperror());
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * A client of the specified server was not able to initiate
		 * an rpc connection, because it could not get the network
		 * information. The pmfadm or scha command exits with error.
		 * The rpc error is shown. An error message is output to
		 * syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "libsecurity: program %s (%lu); getnetconfigent error: "
		    "%s", h->program_name, h->program_num, nc_sperror());
		sc_syslog_msg_done(&sys_handle);
		return (FALSE);
	}

	h->clnt = clnt_tp_create(h->host_name, (uint_t)(h->program_num),
	    (uint_t)(h->version), nconf);
#else
	h->clnt = clnt_create(h->host_name, h->program_num,
		    h->version, "tcp");
#endif
	if (h->clnt == NULL) {
		if (clnt_output)
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "%s failed for program %s (%lu): %s"),
			    CLNT_CREATE, h->program_name, h->program_num,
			    clnt_spcreateerror("libsecurity"));
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "%s failed for program %s (%lu): %s",
		    CLNT_CREATE, h->program_name, h->program_num,
		    clnt_spcreateerror("libsecurity"));
		sc_syslog_msg_done(&sys_handle);
#ifdef DEBUG
#ifndef linux
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR,
		    "in libsecurity transport is: %s\n", trans_name);
#else
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR,
		    "in libsecurity transport is: tcp\n");
#endif

		print_rpc_createerr(h);
#endif
	}

#ifndef linux
	freenetconfigent(nconf);
#endif
	if (h->clnt == NULL)
		return (FALSE);

	if (clnt_debug)
		clnt_dbg_msgout(NOGET(
		    "%s for program %s (%lu) succeeded over "
		    "transport %s\n"), CLNT_CREATE, h->program_name,
		    h->program_num, trans_name);
	rc = clnt_authenticate(h->clnt, h->sec_type);
	return (rc);

}

/*
 * create a struct that stores the client info
 */
static void create_handle(clnt_h h,
	char    *host_name,
	const char *program_name,
	ulong_t program_num,
	ulong_t	version,
	SEC_TYPE	sec_type)
{
	if (clnt_debug)
		clnt_dbg_msgout(NOGET(
		    "entering create_handle.\n"));

	h->program_name = program_name;
	h->program_num = program_num;
	h->version = version;
	h->sec_type = sec_type;
	h->host_name = clnt_estrdup(host_name);

}

/*
 * after client call, release RPC resources
 */
void
security_clnt_freebinding(CLIENT *clnt)
{
#ifdef linux
	return;
#else
	if (clnt_debug)
		clnt_dbg_msgout(NOGET(
		    "entering security_clnt_freebinding.\n"));

	if (clnt == NULL)
		return;
	if (clnt->cl_auth != NULL) {
		auth_destroy(clnt->cl_auth);
		clnt->cl_auth = NULL;
	}
	clnt_destroy(clnt);
#endif
}

/*
 * print rpc error struct
 */
static void
print_rpc_createerr(clnt_h h)
{
	sc_syslog_msg_handle_t sys_handle;

	(void) sc_syslog_msg_initialize(&sys_handle,
	    SC_SYSLOG_RGMPMF_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * A client of the specified server was not able to initiate an rpc
	 * connection. The error message generated with a call to
	 * clnt_spcreateerror(3NSL) is appended.
	 * @user_action
	 * Save the /var/adm/messages file. Check the messages file for
	 * earlier errors related to the rpc.pmfd, rpc.fed, or rgmd server.
	 */
	(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
	    "libsecurity: program %s (%lu) rpc_createerror: %s",
	    h->program_name, h->program_num, clnt_spcreateerror(""));
	sc_syslog_msg_done(&sys_handle);

	if (clnt_debug) {
#ifdef linux
		clnt_dbg_msgout(NOGET("libsecurity: program %s (%lu) "
		    "rpc_createerror:\n\t"
		    "rpc_createerr.cf_stat=%d\n\t"
		    "rpc_createerr.cf_error.re_status=%d\n\t"
		    "rpc_createerr.cf_error.re_errno=%d\n\t"
		    "rpc_createerr.cf_error.re_why=%d\n\t"
		    "rpc_createerr.cf_error.re_vers.low=%d\n\t"
		    "rpc_createerr.cf_error.re_vers.high=%d\n\t"
		    "rpc_createerr.cf_error.re_lb.s1=%d\n\t"
		    "rpc_createerr.cf_error.re_lb.s2=%d\n"),
#else
		clnt_dbg_msgout(NOGET("libsecurity: program %s (%lu) "
		    "rpc_createerror:\n\t"
		    "rpc_createerr.cf_stat=%d\n\t"
		    "rpc_createerr.cf_error.re_status=%d\n\t"
		    "rpc_createerr.cf_error.re_errno=%d\n\t"
		    "rpc_createerr.cf_error.re_terrno=%d\n\t"
		    "rpc_createerr.cf_error.re_why=%d\n\t"
		    "rpc_createerr.cf_error.re_vers.low=%d\n\t"
		    "rpc_createerr.cf_error.re_vers.high=%d\n\t"
		    "rpc_createerr.cf_error.re_lb.s1=%d\n\t"
		    "rpc_createerr.cf_error.re_lb.s2=%d\n"),
#endif
		    h->program_name, h->program_num,
		    rpc_createerr.cf_stat, rpc_createerr.cf_error.re_status,
		    rpc_createerr.cf_error.re_errno,
#ifndef linux
		    rpc_createerr.cf_error.re_terrno,
#endif
		    rpc_createerr.cf_error.re_why,
		    rpc_createerr.cf_error.re_vers.low,
		    rpc_createerr.cf_error.re_vers.high,
		    rpc_createerr.cf_error.re_lb.s1,
		    rpc_createerr.cf_error.re_lb.s2);
	}
}

/*
 * performs client authentication
 */
static bool_t clnt_authenticate(CLIENT *clnt,
	SEC_TYPE sec_type)
{
	sc_syslog_msg_handle_t sys_handle;

	if (clnt_debug)
		clnt_dbg_msgout(NOGET("entering clnt_authenticate.\n"));

	/*
	 * if client is not null, reset it
	 */
	if (clnt->cl_auth != NULL)
		auth_destroy(clnt->cl_auth);

	/*
	 * for unix security type, create default client authentication handle
	 */
	switch (sec_type) {
	case SEC_UNIX_STRONG:
	case SEC_UNIX_WEAK:
#ifndef linux
		clnt->cl_auth = authsys_create_default();
#else
		/*
		 * Linux only supports authunix credential.
		 */
		clnt->cl_auth = authunix_create_default();
#endif
		if (clnt->cl_auth == NULL) {
			if (clnt_output)
				(void) fprintf(stderr, (const char *)
				    dgettext(TEXT_DOMAIN,
				    "in libsecurity %s"
				    " failed\n"), AUTH_CREATE_DEFAULT);
			(void) sc_syslog_msg_initialize(&sys_handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(sys_handle,
			    LOG_ERR, MESSAGE,
			    "in libsecurity %s failed", AUTH_CREATE_DEFAULT);
			sc_syslog_msg_done(&sys_handle);
			return (FALSE);
		}
		break;
	default:
		if (clnt_output)
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "in libsecurity unknown security flag %d\n"),
			    sec_type);
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * This is an internal error which shouldn't happen. An error
		 * message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle,
		    LOG_ERR, MESSAGE,
		    "in libsecurity unknown security flag %d",
		    sec_type);
		sc_syslog_msg_done(&sys_handle);
		return (FALSE);
	}

	return (TRUE);

}
/* RPC Implementation Ends */
#endif

/* Code Common To RPC And DOOR Implementation */

#define		MAXMSGSZ	4096

/*
 * Client uses this to interpret security error codes;
 * client prints error message and frees up storage.
 */
char
*security_get_errormsg(SEC_ERRCODE rc)
{
	char	buf[MAXMSGSZ], *err_str = NULL;

	if (clnt_debug)
		clnt_dbg_msgout(NOGET(
		    "entering security_clnt_print_errormsg.\n"));

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	switch (rc) {
	case SEC_EUNIXS:
		if (clnt_output)
			(void) sprintf(buf, (const char *) dgettext(TEXT_DOMAIN,
			    "Strong UNIX security error."));
		else
			(void) sprintf(buf,
			    NOGET("Strong UNIX security error."));
		break;
	case SEC_EUNIXW:
		if (clnt_output)
			(void) sprintf(buf, (const char *) dgettext(TEXT_DOMAIN,
			    "Weak UNIX security error."));
		else
			(void) sprintf(buf, NOGET("Weak UNIX security error."));
		break;
	case SEC_EUNKN:
		if (clnt_output)
			(void) sprintf(buf, (const char *) dgettext(TEXT_DOMAIN,
			    "Unknown security flavor."));
		else
			(void) sprintf(buf, NOGET("Unknown security flavor."));
		break;
	default:
		if (clnt_output)
			(void) sprintf(buf, (const char *) dgettext(TEXT_DOMAIN,
			    "Unknown security error."));
		else
			(void) sprintf(buf, NOGET("Unknown security error."));
		break;
	}

	if (buf)
		err_str = clnt_estrdup(buf);
	return (err_str);
}




bool_t
xdr_SEC_ERRCODE(register XDR *xdrs, SEC_ERRCODE *objp)
{
	if (!xdr_enum(xdrs, (enum_t *)objp))
		return (FALSE);
	return (TRUE);
}

static void
clnt_dbg_msgout(const char *msg, ...)
{
	char *newmsg = NULL;
	pthread_t tid = pthread_self();
	va_list ap;

	/* allocate for "tid <msg>\0" */
	newmsg = (char *)malloc(stringsize(sizeof (pthread_t)) + 1 +
	    strlen(msg) + 1);
	if (newmsg == NULL) {   /* ignore tid */
		newmsg = (char *)msg;
	} else {
		/* initialise newmsg to empty string in case sprintf fails */
		newmsg[0] = '\0';
		(void) sprintf(newmsg, "%u %s", tid, msg);
	}

	/*
	 * Suppress lint message for "Undeclared identifier
	 * (__builtin_va_alist)
	 */
	va_start(ap, msg); /*lint !e40 !e26 !e50 !e10 */

	vsyslog(LOG_DEBUG, newmsg, ap);

	if (clnt_output) {
		(void) vfprintf(stderr, newmsg, ap);
	}

	va_end(ap);
	if (newmsg != msg)
		free(newmsg);
}


/*
 * Duplicate string and print translated message if error.
 */
static char *clnt_estrdup(char *s)
{
	char *r;
	r = strdup(s);

	if (r == NULL) {
		if (clnt_output)
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "in libsecurity could not allocate memory\n"));
		syslognomem();
	}

	return (r);

}
