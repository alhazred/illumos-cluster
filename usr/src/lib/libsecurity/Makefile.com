#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.37	08/06/13 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libsecurity/Makefile.com
#

LIBRARY= libsecurity.a
VERS= .1

OBJECTS = security.o 

include ../../../Makefile.master
# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

MAPFILE=	../common/mapfile-vers
PMAP=	-M $(MAPFILE)
SRCS =		$(OBJECTS:%.o=../common/%.c)

CHECKHDRS = sec_private.h

LIBS = $(DYNLIB) $(LIBRARY)

# definitions for lint
LINTFLAGS += -I.
LINTFILES += $(OBJECTS:%.o=%.ln)

CPPFLAGS += -I$(SRC)/common/cl -I$(REF_PROTO)/usr/src/head
CPPFLAGS += -I$(REF_PROTO)/usr/src/lib/libbrand/common
CPPFLAGS += -I$(REF_PROTO)/usr/src/lib/libuutil/common
CPPFLAGS += -D_REENTRANT

$(POST_S9_BUILD)SECURITY_LIBZONECFG_32=$(REF_PROTO)/usr/lib/libzonecfg.so.1
$(POST_S9_BUILD)SECURITY_LIBZONECFG_64=$(REF_PROTO)/usr/lib/64/libzonecfg.so.1
#
# WARNING: Do not include any libraries that cause libthread to be included
#          as Oracle is a consumer of this library and cannot be linked
#          with libthread.
#
LDLIBS += -lnsl -lc -lclos  
LDLIBS += $(DOOR_LDLIBS)

DYNFLAGS += -M $(MAPFILE)

.KEEP_STATE:

.NO_PARALLEL:

all:  $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
