/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 **************************************************************************
 *
 * Component = clquery deamon
 *
 * Synopsis  = clquery server code for memory management.
 *
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 **************************************************************************
 */

#pragma ident "@(#)clquery_free.c 1.9 08/05/20 SMI"

#include <stdlib.h>
#include <cl_query/cl_query_types.h>


/*
 * DESCRIPTION:
 *   free a cl_query_list_t  type
 *
 *
 * INPUT:
 *    cl_query_list_t :  a cl_query_list
 *
 * OUTPUT:
 *    None.
 * RETURN:
 *   NONE
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

static void
cl_query_free_cl_query_list(cl_query_list_t *list)
{
	unsigned int i;

	if (list == NULL)
		return;
	if (list->list_len == 0)
		return;
	if (list->list_val == NULL)
		return;

	for (i = 0; i < list->list_len; i++) {
		if (list->list_val[i] != NULL)
			free(list->list_val[i]);
	}


	free(list->list_val);
	list->list_val = NULL;
	list->list_len = 0;


}


/*
 * DESCRIPTION:
 *   free a cl_query_name_t  type
 *
 *
 * INPUT:
 *    cl_query_name_t n:  a cl_query_name
 *
 * OUTPUT:
 *    None.
 * RETURN:
 *   NONE
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

static void
cl_query_free_name(cl_query_name_t *n)
{


	if ((n == NULL) || (*n == NULL))
		return;

	free(*n);

	*n = NULL;

}



/*
 * DESCRIPTION:
 *    This method frees up the memory allocated (if any) for the
 *    adapter query.
 *
 *
 * INPUT:
 *    cl_query_adapter_info_t:  Adapter property structure.
 *
 * OUTPUT:
 *    None.
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_free_adapter_info(cl_query_adapter_info_t *res)
{
	struct cl_query_adapter_info *tmp_ptr = NULL;
	unsigned int i;

	if (res == NULL) {
		return (CL_QUERY_EINVAL);
	}
	while (res != NULL) {

		cl_query_free_name(&(res->adapter_name));
		cl_query_free_name(&(res->device_name));
		cl_query_free_name(&(res->ip_addr));
		cl_query_free_name(&(res->netmask));
		cl_query_free_name(&(res->node_name));
		cl_query_free_name(&(res->type));



		if ((res->port_list.list_val != NULL) &&
		    (res->port_list.list_len > 0)) {

			for (i = 0; i < res->port_list.list_len; i++) {
				if (res->port_list.list_val[i].port_name !=
				    NULL)
					free(res->port_list.list_val[i].
					    port_name);
			}

			free(res->port_list.list_val);
			res->port_list.list_val = NULL;
			res->port_list.list_len = 0;
		}
		tmp_ptr = res->next;

		if (res != NULL) {
			free(res);
			res = NULL;
		}
		res = tmp_ptr;
	}
	return (CL_QUERY_OK);
}

/*
 * DESCRIPTION:
 *    This method frees up the memory allocated (if any) for the
 *    CL_QUERY_QUORUM_INFO.
 *
 *
 * INPUT:
 *    cl_query_quorum_info_t:  Quorum property structure.
 *
 * OUTPUT:
 *    None.
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_free_quorum_info(cl_query_quorum_info_t *res)
{
	struct cl_query_quorum_info *tmp_ptr = NULL;

	if (res == NULL) {
		return (CL_QUERY_EINVAL);
	}
	while (res != NULL) {

		cl_query_free_name(&(res->device_name));
		cl_query_free_name(&(res->key_name));
		cl_query_free_name(&(res->device_type));
		cl_query_free_name(&(res->filer_name));
		cl_query_free_name(&(res->lunid));
		cl_query_free_name(&(res->lun_name));
		cl_query_free_name(&(res->qs_host));
		cl_query_free_name(&(res->qs_port));
		cl_query_free_name(&(res->qs_name));

		cl_query_free_cl_query_list(&(res->disabled_nodes));
		cl_query_free_cl_query_list(&(res->enabled_nodes));

		cl_query_free_name(&(res->access_mode));


		tmp_ptr = res->next;
		free(res);
		res = tmp_ptr;
	}	/* End of while */
	return (CL_QUERY_OK);
}


/*
 * DESCRIPTION:
 *    This method frees up the memory allocated (if any) for the
 *    CL_QUERY_NODE_INFO.
 *
 *
 * INPUT:
 *    cl_query_node_info_t:  Node property structure.
 *
 * OUTPUT:
 *    None.
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_free_node_info(cl_query_node_info_t *res)
{

	struct cl_query_node_info *tmp_ptr = NULL;

	if (res == NULL) {
		return (CL_QUERY_EINVAL);
	}
	while (res != NULL) {

		cl_query_free_name(&(res->node_name));
		cl_query_free_name(&(res->private_host_name));
		cl_query_free_name(&(res->resv_key));

		cl_query_free_cl_query_list(&(res->private_adapters));
		cl_query_free_cl_query_list(&(res->public_adapters));

		tmp_ptr = res->next;
		free(res);
		res = tmp_ptr;
	}
	return (CL_QUERY_OK);
}

/*
 * DESCRIPTION:
 *    This method frees up the memory allocated (if any) for the
 *    CL_QUERY_DEVICE_INFO.
 *
 *
 * INPUT:
 *    cl_query_device_info_t:  Device property structure.
 *
 * OUTPUT:
 *    None.
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_free_device_info(cl_query_device_info_t *res)
{
	uint_t tmp = 0;
	struct cl_query_device_info *tmp_ptr = NULL;

	if (res == NULL) {
		return (CL_QUERY_EINVAL);
	}
	while (res != NULL) {

		cl_query_free_name(&(res->logical_disk_path));


		cl_query_free_name(&(res->device_name));
		cl_query_free_name(&(res->primary_node_name));

		cl_query_free_cl_query_list(&(res->alternate_nodes));
		cl_query_free_cl_query_list(&(res->spare_nodes));

		cl_query_free_name(&(res->device_type));
		cl_query_free_name(&(res->replication_type));


		if (((res->file_systems_path).list_val != NULL) &&
		    ((res->file_systems_path).list_len != 0)) {
			for (tmp = 0; tmp < (res->file_systems_path).list_len;
			    tmp++) {
				if ((res->file_systems_path).list_val[tmp].
				    node_name)
					free((res->file_systems_path).
					    list_val[tmp].node_name);
				if ((res->file_systems_path).list_val[tmp].
				    phys_path_name)
					free((res->file_systems_path).
					    list_val[tmp].phys_path_name);
				if ((res->file_systems_path).list_val[tmp].
				    mount_point)
					free((res->file_systems_path).
					    list_val[tmp].mount_point);


			}
			free((res->file_systems_path).list_val);
		}
		tmp_ptr = res->next;
		if (res != NULL) {
			free(res);
			res = NULL;
		}
		res = tmp_ptr;
	}
	return (CL_QUERY_OK);
}

/*
 * DESCRIPTION:
 *    This method frees up the memory allocated (if any) for the
 *    CL_QUERY_CABLE_INFO.
 *
 *
 * INPUT:
 *    cl_query_cable_info_t:  Cable property structure.
 *
 * OUTPUT:
 *    None.
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_free_cable_info(cl_query_cable_info_t *res)
{
	struct cl_query_cable_info *tmp_ptr = NULL;

	if (res == NULL) {
		return (CL_QUERY_EINVAL);
	}
	while (res != NULL) {

		cl_query_free_name(&(res->cable_name));
		cl_query_free_name(&(res->cable_epoint1_name));
		cl_query_free_name(&(res->cable_epoint2_name));

		tmp_ptr = res->next;
		free(res);
		res = tmp_ptr;
	}

	return (CL_QUERY_OK);
}


/*
 * DESCRIPTION:
 *    This method frees up the memory allocated (if any) for the
 *    CL_QUERY_JUNCTION_INFO.
 *
 *
 * INPUT:
 *    cl_query_junction_info_t:  Junction property structure.
 *
 * OUTPUT:
 *    None.
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_free_junction_info(cl_query_junction_info_t *res)
{
	uint_t tmp = 0;
	struct cl_query_junction_info *tmp_ptr = NULL;


	if (res == NULL) {
		return (CL_QUERY_EINVAL);
	}
	while (res != NULL) {

		cl_query_free_name(&(res->junction_name));
		cl_query_free_name(&(res->junction_type));
		cl_query_free_name(&(res->key_name));

		if ((res->port_list.list_val != NULL) &&
		    (res->port_list.list_len > 0)) {

			for (tmp = 0; tmp < res->port_list.list_len; tmp++) {
				if (res->port_list.list_val[tmp].port_name !=
				    NULL)
					free(res->
					    port_list.list_val[tmp].port_name);
			}

			free(res->port_list.list_val);
			res->port_list.list_val = NULL;
			res->port_list.list_len = 0;
		}
		tmp_ptr = res->next;
		if (res != NULL) {
			free(res);
			res = NULL;
		}
		res = tmp_ptr;
	}

	return (CL_QUERY_OK);
}

/*
 * DESCRIPTION:
 *    This method frees up the memory allocated (if any) for the
 *    CL_QUERY_IPMP_INFO.
 *
 *
 * INPUT:
 *   cl_query_ipmp_info_t :  ipmp property structure.
 *
 * OUTPUT:
 *    None.
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_free_ipmp_info(cl_query_ipmp_info_t *res)
{
	cl_query_ipmp_info_t *ptr = NULL;
	cl_query_ipmp_info_t *ptr2 = NULL;

	if (res == NULL)
		return (CL_QUERY_EINVAL);

	ptr = res;
	ptr2 = ptr;

	while (ptr) {

		cl_query_free_cl_query_list(&(ptr->adapter_list));

		cl_query_free_name(&(ptr->group_name));
		cl_query_free_name(&(ptr->node_name));


		ptr = ptr->next;
		free(ptr2);
		ptr2 = ptr;
	}



	return (CL_QUERY_OK);
}

/*
 * DESCRIPTION:
 *    This method frees up the memory allocated (if any) for the
 *    CL_QUERY_DISKPATH_INFO.
 *
 *
 * INPUT:
 *   cl_query_diskpath_info_t :  diskpath property structure.
 *
 * OUTPUT:
 *    None.
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_free_diskpath_info(cl_query_diskpath_info_t *res)
{

	cl_query_diskpath_info_t *ptr = NULL;
	cl_query_diskpath_info_t *ptr2 = NULL;

	if (res == NULL)
		return (CL_QUERY_EINVAL);

	ptr = res;
	ptr2 = ptr;


	while (ptr) {
		cl_query_free_name(&(ptr->dpath_name));
		cl_query_free_name(&(ptr->node_name));

		ptr = ptr->next;
		free(ptr2);
		ptr2 = ptr;
	}




	return (CL_QUERY_OK);
}

cl_query_error_t
cl_query_free_cluster_info(cl_query_cluster_info_t *cluster)
{

	if (cluster == NULL)
		return (CL_QUERY_EINVAL);

	cl_query_free_name(&(cluster->cluster_name));
	cl_query_free_name(&(cluster->cluster_version));
	cl_query_free_name(&(cluster->cluster_id));

	cl_query_free_cl_query_list(&(cluster->cluster_join_list));

	cl_query_free_name(&(cluster->cluster_netaddr));
	cl_query_free_name(&(cluster->cluster_netmask));
	cl_query_free_name(&(cluster->cluster_max_nodes));
	cl_query_free_name(&(cluster->cluster_max_priv_nets));

	free(cluster);

	return (CL_QUERY_OK);
}
cl_query_error_t
cl_query_free_transportpath_info(cl_query_transportpath_info_t *tinfo)
{

	cl_query_transportpath_info_t *ptr = NULL;
	cl_query_transportpath_info_t *ptr2 = NULL;

	if (tinfo == NULL)
		return (CL_QUERY_EINVAL);

	ptr = tinfo;
	ptr2 = ptr;


	while (ptr) {

		cl_query_free_name(&(ptr->transportpath_name));
		cl_query_free_name(&(ptr->transportpath_endpoint_1_name));
		cl_query_free_name(&(ptr->transportpath_endpoint_1_strname));
		cl_query_free_name(&(ptr->transportpath_endpoint_2_name));
		cl_query_free_name(&(ptr->transportpath_endpoint_2_strname));

		ptr = ptr->next;
		free(ptr2);
		ptr2 = ptr;
	}

	return (CL_QUERY_OK);
}
