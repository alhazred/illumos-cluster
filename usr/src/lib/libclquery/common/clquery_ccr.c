/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident "@(#)clquery_ccr.c 1.9 08/05/20 SMI"

#include "clquery.h"

#define	RPC_CALL_TIMEOUT   300

static struct timeval clnt_call_timeout   = { RPC_CALL_TIMEOUT, 0 };

static cl_query_error_t
copy_table_to_rpc_buffer(ccr_table_elem_t *, cl_query_table_t *);
static cl_query_error_t free_rpc_buffer(cl_query_ccr_param_t *result);

extern CLIENT *_cl_query_client;

/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_TABLE_NAMES. List of all the CCR Tables names in
 *    the cluster repository.
 *
 *
 * INPUT:
 *    cl_query_name_t : Search Pattern for filtering the table names.
 *
 * OUTPUT:
 *    cl_query_table_list_t: List of Tables names matching the pattern.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */


cl_query_error_t
cl_query_table_names(cl_query_name_t pattern, cl_query_table_list_t *result)
{
	enum clnt_stat	clnt_stat;
	ccr_table_list_t	output;
	cl_query_input_t	input;
	uint_t	i = 0;
	cl_query_error_t	res = CL_QUERY_OK;

	/*
	 * in_param can be NULL (default case)
	 */
	if (result == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if (pattern != NULL) {
		input.name = pattern;
	} else {
		input.name = NULL;
	}

	/*
	 * Initialize the RPC connection
	 */
	res = _cl_query_lib_init();
	if (res != CL_QUERY_OK) {
		return (res);
	}
	(void) clnt_control(_cl_query_client, CLSET_TIMEOUT,
		(char *)&clnt_call_timeout);


	/*
	 * Initialize result
	 */
	result->table_count = 0;
	result->table_names = NULL;

	/*
	 * RPC lib will core if any pointers are not NULL
	 */
	(void) memset(&output, 0, sizeof (output));

	/*
	 * RPC call to the server
	 */
	clnt_stat = table_names_get_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	/*
	 * Get the table count
	 */
	if (res == CL_QUERY_OK) {
		result->table_count = output.table_list.strarr_list_len;
		if (result->table_count == 0) {
			res = CL_QUERY_EUNEXPECTED;
		}
	}

	if (res == CL_QUERY_OK) {
		result->table_names = (char **)
		    calloc((size_t)result->table_count, sizeof (char *));
		if (result->table_names == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	}

	/*
	 * store the table names in the output param
	 */
	if (res == CL_QUERY_OK) {
		for (i = 0; i < result->table_count; i++) {
			if (output.table_list.strarr_list_val[i] != NULL) {
				result->table_names[i] =
				    strdup(output.table_list.
				    strarr_list_val[i]);
				if (result->table_names[i] == NULL) {
					res = CL_QUERY_ENOMEM;
					break;
				}
				/*
				 * Free the RPC buffer
				 */
				free(output.table_list.strarr_list_val[i]);
			}
		}
	}

	if (res != CL_QUERY_OK) {
		result->table_count = 0;
		result->table_names = NULL;
	}

	(void) _cl_query_lib_cleanup();
	return (res);
}


/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_TABLE_SIZE. It returns the table size.
 *
 *
 * INPUT:
 *    cl_query_name_t : Name of the CCR Table
 *
 * OUTPUT:
 *    n_rows:  Size of the table.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_table_size(cl_query_name_t in_param, uint_t *n_rows)
{
	enum clnt_stat	clnt_stat;
	ccr_table_size_t	output;
	cl_query_input_t	input;
	cl_query_error_t	res = CL_QUERY_OK;

	/*
	 * in_param can be NULL (default case)
	 */
	if (n_rows == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if (in_param == NULL) {
		return (CL_QUERY_EINVAL);
	} else {
		input.name = in_param;
	}

	/*
	 * Initialize the RPC connection
	 */
	res = _cl_query_lib_init();
	if (res != CL_QUERY_OK) {
		return (res);
	}
	(void) clnt_control(_cl_query_client, CLSET_TIMEOUT,
		(char *)&clnt_call_timeout);

	/*
	 * RPC lib will core if any pointers are not NULL
	 */
	(void) memset(&output, 0, sizeof (output));


	/*
	 * RPC call to the server
	 */
	clnt_stat = table_len_get_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	/*
	 * Store the table size
	 */
	if (res == CL_QUERY_OK) {
		*n_rows = output.table_len;
	}

	(void) _cl_query_lib_cleanup();
	return (res);
}



/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_READ_TABLE. Contents of the CCR Table is returned.
 *
 *
 * INPUT:
 *    cl_query_name_t : Name of the CCR Table.
 *
 * OUTPUT:
 *    cl_query_table_t:  Size and the table contents.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_read_table(cl_query_name_t in_param, cl_query_table_t *result)
{
	enum clnt_stat	clnt_stat;
	cl_query_ccr_param_t	output;
	cl_query_input_t	input;
	uint_t	i = 0;
	cl_query_error_t	res = CL_QUERY_OK;
	ccr_table_elem_t	row;

	/*
	 * in_param can be NULL (default case)
	 */
	if (in_param == NULL || result == NULL) {
		return (CL_QUERY_EINVAL);
	}

	input.name = in_param;

	/*
	 * Initialize the RPC connection
	 */
	res = _cl_query_lib_init();
	if (res != CL_QUERY_OK) {
		return (res);
	}
	(void) clnt_control(_cl_query_client, CLSET_TIMEOUT,
		(char *)&clnt_call_timeout);

	/*
	 * RPC lib will core if any pointers are not NULL
	 */
	(void) memset(&output, 0, sizeof (output));

	clnt_stat = read_table_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	if (res == CL_QUERY_OK) {
		result->n_rows = output.table_rows.table_rows_len;
		result->table_rows = calloc((size_t)result->n_rows,
		    sizeof (cl_query_table_elem_t *));
		if (result->table_rows == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	}

	if (res == CL_QUERY_OK) {
		for (i = 0; i < result->n_rows; i++) {
			row = output.table_rows.table_rows_val[i];
			result->table_rows[i] = (struct cl_query_table_elem *)
			    calloc((size_t)1,
			    sizeof (struct cl_query_table_elem));
			if (result->table_rows[i] == NULL) {
				res = CL_QUERY_ENOMEM;
				break;
			}

			if (row->key != NULL) {
				result->table_rows[i]->key =
				    (char *)strdup(row->key);
				if (result->table_rows[i]->key == NULL) {
					res = CL_QUERY_ENOMEM;
					break;
				}
			} else {
				result->table_rows[i]->key = NULL;
			}

			if (row->value != NULL) {
				result->table_rows[i]->value =
				    (char *)strdup(row->value);
				if (result->table_rows[i]->value == NULL) {
					res = CL_QUERY_ENOMEM;
					break;
				}
			} else {
				result->table_rows[i]->value = NULL;
			}
		}
	}
	(void) free_rpc_buffer(&output);

	(void) _cl_query_lib_cleanup();
	return (res);
}


/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_CREATE_TABLE. CCR Table is created as a result of
 *    the API invocation.
 *
 *
 * INPUT:
 *    cl_query_name_t : Name of the CCR Table.
 *    cl_query_table_t:  Size and the table contents.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */
cl_query_error_t
cl_query_create_table(cl_query_name_t in_param, cl_query_table_t  *result)
{
	enum clnt_stat	clnt_stat;
	cl_query_ccr_param_t	input;
	cl_query_output_t	output;
	cl_query_error_t	res = CL_QUERY_OK;

	/*
	 * Validate the input parameters
	 */
	if (in_param == NULL) {
		return (CL_QUERY_EINVAL);
	}

	input.table_name = in_param;
	input.error = CL_QUERY_OK;

	if (result != NULL) {
		input.table_rows.table_rows_len = result->n_rows;
		input.table_rows.table_rows_val = (ccr_table_elem_t *)calloc(
			(size_t)result->n_rows, sizeof (ccr_table_elem_t));
		if (input.table_rows.table_rows_val == NULL) {
			return (CL_QUERY_ENOMEM);
		}

		res = copy_table_to_rpc_buffer(input.table_rows.table_rows_val,
			result);
		if (res != CL_QUERY_OK) {
			return (res);
		}
	} else {
		input.table_rows.table_rows_len = 0;
		input.table_rows.table_rows_val = NULL;
	}

	/*
	 * Initialize the output parameter
	 */
	(void) memset(&output, 0, sizeof (output));


	/*
	 * Initialize the RPC connection
	 */
	res = _cl_query_lib_init();
	if (res != CL_QUERY_OK) {
		return (res);
	}
	(void) clnt_control(_cl_query_client, CLSET_TIMEOUT,
		(char *)&clnt_call_timeout);

	clnt_stat = create_table_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	(void) _cl_query_lib_cleanup();
	return (res);
}



/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_WRITE_TABLE_ALL. CCR Table is updated.
 *
 * INPUT:
 *    cl_query_name_t : Name of the CCR Table.
 *    cl_query_table_t:  Size and the table contents.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */
cl_query_error_t
cl_query_write_table_all(cl_query_name_t in_param, cl_query_table_t *result)
{
	enum clnt_stat	clnt_stat;
	cl_query_ccr_param_t	input;
	cl_query_output_t	output;
	cl_query_error_t	res = CL_QUERY_OK;

	/*
	 * Validate the input parameters
	 */
	if (in_param == NULL || result == NULL) {
		return (CL_QUERY_EINVAL);
	}

	input.table_name = in_param;
	input.table_rows.table_rows_len = result->n_rows;
	input.error = CL_QUERY_OK;

	input.table_rows.table_rows_val = (ccr_table_elem_t *)calloc(
		(size_t)result->n_rows, sizeof (ccr_table_elem_t));
	if (input.table_rows.table_rows_val == NULL) {
		return (CL_QUERY_ENOMEM);
	}

	/*
	 * Copy the input parameters to the RPC buffer
	 */
	res = copy_table_to_rpc_buffer(input.table_rows.table_rows_val, result);
	if (res != CL_QUERY_OK) {
		return (res);
	}

	/*
	 * Initialize the output parameter
	 */
	(void) memset(&output, 0, sizeof (output));


	/*
	 * Initialize the RPC connection
	 */
	res = _cl_query_lib_init();
	if (res != CL_QUERY_OK) {
		return (res);
	}
	(void) clnt_control(_cl_query_client, CLSET_TIMEOUT,
		(char *)&clnt_call_timeout);

	/*
	 * Invoke the RPC call to the server
	 */
	clnt_stat = write_table_all_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	(void) _cl_query_lib_cleanup();
	return (res);
}


/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_DELETE_TABLE. CCR Table is deleted.
 *
 * INPUT:
 *    cl_query_name_t : Name of the CCR Table.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_delete_table(cl_query_name_t in_param)
{
	enum clnt_stat	clnt_stat;
	cl_query_output_t	output;
	cl_query_input_t	input;
	cl_query_error_t	res = CL_QUERY_OK;

	/*
	 * Validate the input parameters
	 */
	if (in_param == NULL) {
		return (CL_QUERY_EINVAL);
	}


	input.name = in_param;

	/*
	 * Initialize the RPC connection
	 */
	res = _cl_query_lib_init();
	if (res != CL_QUERY_OK) {
		return (res);
	}
	(void) clnt_control(_cl_query_client, CLSET_TIMEOUT,
		(char *)&clnt_call_timeout);

	/*
	 * RPC Call to the server
	 */
	clnt_stat = delete_table_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	(void) _cl_query_lib_cleanup();
	return (res);
}


/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_FREE_TABLE. This routine fress up the memory allocated
 *    by the library for the previous READ_TABLE APi call
 *
 *
 * INPUT:
 *    cl_query_table_t: result structure containing the Table contents.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_free_table(cl_query_table_t *result)
{
	uint_t   i = 0;

	if (result == NULL) {
		return (CL_QUERY_EINVAL);
	}

	/*
	 * Check if the list is empty
	 */
	if (result->table_rows == NULL || result->n_rows == 0) {
		return (CL_QUERY_OK);
	}

	for (i = 0; i < result->n_rows; i++) {
		/*
		 * Free the row
		 */
		if (result->table_rows[i] != NULL) {
			if (result->table_rows[i]->key != NULL) {
				free(result->table_rows[i]->key);
				result->table_rows[i]->key = NULL;
			}

			/*
			 * Free the value field
			 */
			if (result->table_rows[i]->value != NULL) {
				free(result->table_rows[i]->value);
				result->table_rows[i]->value = NULL;
			}

			free(result->table_rows[i]);
			result->table_rows[i] = NULL;
		}
	}

	/* Free the table_rows buffer */
	if (result->table_rows != NULL) {
		free(result->table_rows);
		result->table_rows = NULL;
	}
	return (CL_QUERY_OK);
}



/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_DELETE_TABLE_ALL. CCR Table content is deleted.
 *
 * INPUT:
 *    cl_query_name_t : Name of the CCR Table.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_delete_table_all(cl_query_name_t in_param)
{
	enum clnt_stat	clnt_stat;
	cl_query_output_t	output;
	cl_query_error_t	res = CL_QUERY_OK;
	cl_query_input_t	input;


	/*
	 * Validate the input parameters
	 */
	if (in_param == NULL) {
		return (CL_QUERY_EINVAL);
	}


	input.name = in_param;

	/*
	 * Initialize the RPC connection
	 */
	res = _cl_query_lib_init();
	if (res != CL_QUERY_OK) {
		return (res);
	}
	(void) clnt_control(_cl_query_client, CLSET_TIMEOUT,
		(char *)&clnt_call_timeout);

	clnt_stat = delete_table_all_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	(void) _cl_query_lib_cleanup();
	return (res);
}


/*
 * DESCRIPTION:
 *    Utility routine to copy the CCR Table contents to the RPC buffer.
 *
 * INPUT:
 *    ccr_table_elem_t:  RPC Bufffer
 *    cl_query_table_t: User input buffer
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
copy_table_to_rpc_buffer(ccr_table_elem_t *table_rows_val,
	cl_query_table_t *result)
{
	char	*in_key = NULL, *in_value = NULL;
	uint_t	i = 0;
	cl_query_error_t	res = CL_QUERY_OK;

	/*
	 * Copy the table contents to the RPC buffer
	 */
	for (i = 0; i < result->n_rows; i++) {
		in_key = result->table_rows[i]->key;
		in_value = result->table_rows[i]->value;

		/*
		 * Allocate memory for the RPC input buffer
		 */
		table_rows_val[i] = (struct ccr_table_elem *)
			calloc(1, sizeof (struct ccr_table_elem));
		if (table_rows_val[i] == NULL) {
			res = CL_QUERY_ENOMEM;
			break;
		}

		if (res == CL_QUERY_OK) {
			/*
			 * Copy the key element
			 */
			if (in_key != NULL) {
				table_rows_val[i]->key = (char *)strdup(in_key);
				if (table_rows_val[i]->key == NULL) {
					res = CL_QUERY_ENOMEM;
					break;
				}
			} else {
				table_rows_val[i]->key = NULL;
			}

			/*
			 * Copy the value element for the row
			 */
			if (in_value != NULL) {
				table_rows_val[i]->value =
					(char *)strdup(in_value);
				if (table_rows_val[i]->value == NULL) {
					res = CL_QUERY_ENOMEM;
					break;
				}
			} else {
				table_rows_val[i]->value = NULL;
			}
		}
	}
	return (res);
}


/*
 * DESCRIPTION:
 *    Utility routine to Free the table buffer
 *
 * INPUT:
 *    ccr_table_elem_t:  RPC Bufffer
 *    cl_query_table_t: User input buffer
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
free_rpc_buffer(cl_query_ccr_param_t *result)
{
	uint_t i = 0;

	/*
	 * Nothing to free
	 */
	if (result == NULL) {
		return (CL_QUERY_OK);
	}

	/*
	 * Nothing to free
	 */
	if (result->table_rows.table_rows_val == NULL) {
		return (CL_QUERY_OK);
	}

	for (i = 0; i < result->table_rows.table_rows_len; i++) {
		if (result->table_rows.table_rows_val[i] != NULL) {
			/*
			 * Free the key buffer
			 */
			if (result->table_rows.table_rows_val[i]->key != NULL) {
				free(result->table_rows.table_rows_val[i]->key);
				result->table_rows.table_rows_val[i]->key =
					NULL;
			}
			/*
			 * Free the value buffer
			 */
			if (result->table_rows.table_rows_val[i]->value
			!= NULL) {
				free(result->table_rows.
					table_rows_val[i]->value);
				result->table_rows.
					table_rows_val[i]->value = NULL;
			}
			/*
			 * Free the table row buffer
			 */
			free(result->table_rows.table_rows_val[i]);
			result->table_rows.table_rows_val[i] = NULL;
		}
	}
	/*
	 * Free the table buffer
	 */
	if (result->table_rows.table_rows_val != NULL) {
		free(result->table_rows.table_rows_val);
		result->table_rows.table_rows_val = NULL;
	}
	return (CL_QUERY_OK);
}
