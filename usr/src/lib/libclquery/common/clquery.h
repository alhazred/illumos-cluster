/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CL_QUERY_HEADER_H_
#define	_CL_QUERY_HEADER_H_

#pragma ident	"@(#)clquery.h	1.4	08/05/20 SMI"

#include <stdlib.h>
#include <strings.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

#include <cl_query/rpc/cl_query.h>
#include <cl_query/cl_query_types.h>

/*  #define MULTIPLE_CLNT */

#ifdef MULTIPLE_CLNT
cl_query_error_t _cl_query_lib_init(CLIENT *clnt);
cl_query_error_t _cl_query_lib_cleanup(CLIENT *clnt);
#else
cl_query_error_t _cl_query_lib_init(void);
cl_query_error_t _cl_query_lib_cleanup(void);
#endif /* MULTIPLE_CLNT */
cl_query_error_t cl_query_cluster_info(cl_query_name_t, cl_query_result_t *);
cl_query_error_t cl_query_node_info(cl_query_name_t, cl_query_result_t *);
cl_query_error_t cl_query_device_info(cl_query_name_t, cl_query_result_t *);
cl_query_error_t cl_query_adapter_info(cl_query_name_t, cl_query_result_t *);
cl_query_error_t cl_query_quorum_info(cl_query_name_t, cl_query_result_t *);
cl_query_error_t cl_query_ipmp_info(cl_query_name_t, cl_query_result_t *);
cl_query_error_t cl_query_cable_info(cl_query_name_t, cl_query_result_t *);
cl_query_error_t cl_query_junction_info(cl_query_name_t, cl_query_result_t *);

extern cl_query_error_t get_transportpath_info(
	cl_query_transportpath_prop_t tmp_list,
	    cl_query_transportpath_info_t *tpath);

extern cl_query_error_t get_node_info(cl_query_node_prop_t,
    cl_query_node_info_t *);
extern cl_query_error_t get_adapter_info(cl_query_adapter_prop_t,
    cl_query_adapter_info_t *);
extern cl_query_error_t	get_quorum_info(cl_query_quorum_prop_t,
    cl_query_quorum_info_t *);
extern cl_query_error_t get_device_info(cl_query_device_prop_t,
    cl_query_device_info_t *);
extern cl_query_error_t get_cable_info(cl_query_cable_prop_t,
    cl_query_cable_info_t *);
extern cl_query_error_t get_junction_info(cl_query_junction_prop_t,
    cl_query_junction_info_t *);
extern cl_query_error_t rpc_to_clquery(enum clnt_stat);

extern cl_query_error_t cl_query_free_adapter_info(cl_query_adapter_info_t *);
extern cl_query_error_t cl_query_free_quorum_info(cl_query_quorum_info_t *);
extern cl_query_error_t cl_query_free_node_info(cl_query_node_info_t *);
extern cl_query_error_t cl_query_free_device_info(cl_query_device_info_t *);
extern cl_query_error_t cl_query_free_cable_info(cl_query_cable_info_t *);
extern cl_query_error_t cl_query_free_junction_info(cl_query_junction_info_t *);
extern cl_query_error_t cl_query_free_ipmp_info(cl_query_ipmp_info_t *);
extern cl_query_error_t cl_query_free_diskpath_info(cl_query_diskpath_info_t *);
extern cl_query_error_t cl_query_free_cluster_info(cl_query_cluster_info_t*);
extern cl_query_error_t cl_query_free_transportpath_info(
	cl_query_transportpath_info_t*);
#endif /* _CL_QUERY_HEADER_H_ */
