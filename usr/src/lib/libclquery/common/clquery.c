/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)clquery.c	1.9	08/05/20 SMI"

#include "clquery.h"

#ifndef MULTIPLE_CLNT
CLIENT *_cl_query_client = NULL;
pthread_mutex_t _cl_query_lib_lock = PTHREAD_MUTEX_INITIALIZER;	/*lint !e708 */
#else
#define	_cl_query_client _my_client
#endif

extern cl_query_error_t clquery_free_node_result(cl_query_node_output_t *node);
extern cl_query_error_t clquery_free_adapter_result(
	cl_query_adapter_output_t *adapter);
extern cl_query_error_t clquery_free_junction_result(
	cl_query_junction_output_t *junction);
extern cl_query_error_t clquery_free_cable_result(
	cl_query_cable_output_t *cable);
extern cl_query_error_t clquery_free_quorum_result(
	cl_query_quorum_output_t *quorum);
extern cl_query_error_t clquery_free_device_result(
	cl_query_device_output_t *device);
extern cl_query_error_t clquery_free_ipmp_result(
	cl_query_ipmp_output_t *out);
extern cl_query_error_t clquery_free_diskpath_result(
	cl_query_diskpath_output_t *out);
extern cl_query_error_t clquery_free_cluster_result(
	cl_query_cluster_output_t *out);

extern cl_query_error_t clquery_free_transportpath_result(
	cl_query_transportpath_output_t *tinfo);


/* ARGSUSED */
cl_query_error_t
cl_query_cluster_info(cl_query_name_t in_param, cl_query_result_t *result)
{
	cl_query_input_t input;
	cl_query_cluster_output_t output;
	cl_query_cluster_prop_t cluster_out = NULL;
	cl_query_cluster_info_t *cluster_info = NULL;
	unsigned int i;
	enum clnt_stat clnt_stat;
	cl_query_error_t res = CL_QUERY_OK;

	input.name = NULL;
	result->n_elements = 0;
	result->value_list = NULL;

	(void) memset(&output, 0, sizeof (output));

	clnt_stat = cluster_info_get_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	if (res == CL_QUERY_OK) {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}
	if (res == CL_QUERY_OK) {
		cluster_info = (cl_query_cluster_info_t *)calloc(
			(size_t)1, sizeof (cl_query_cluster_info_t));
		if (cluster_info == NULL) {
			res = CL_QUERY_ENOMEM;
		}

		cluster_out = output.current_cluster;
	}

	if (res == CL_QUERY_OK) {
		if (cluster_out->cluster_id != NULL) {
			cluster_info->cluster_id =
			    (char *)strdup(cluster_out->cluster_id);
			if (cluster_info->cluster_id == NULL) {
				res = CL_QUERY_ENOMEM;
			}
		}
	}

	if (res == CL_QUERY_OK) {
		if (cluster_out->cluster_name != NULL) {
			cluster_info->cluster_name =
			    (char *)strdup(cluster_out->cluster_name);
			if (cluster_info->cluster_name == NULL) {
				res = CL_QUERY_ENOMEM;
			}
		}
	}

	if (res == CL_QUERY_OK) {
		if (cluster_out->cluster_version != NULL) {
			cluster_info->cluster_version =
			    (char *)strdup(cluster_out->cluster_version);
			if (cluster_info->cluster_version == NULL) {
				res = CL_QUERY_ENOMEM;
			}
		}
	}

	if (res == CL_QUERY_OK) {
		cluster_info->install_mode =
		    (cl_query_state_t)cluster_out->install_mode;
	}

	if (res == CL_QUERY_OK) {
		cluster_info->cluster_auth_type =
		    (cl_query_auth_type_t)cluster_out->cluster_auth_type;
	}


	if (res == CL_QUERY_OK) {
		if ((cluster_out->cluster_join_list.strarr_list_len == 0) ||
		    (cluster_out->cluster_join_list.strarr_list_val == NULL)) {
			cluster_info->cluster_join_list.list_len = 0;
			cluster_info->cluster_join_list.list_val = NULL;
		} else {
			cluster_info->cluster_join_list.list_len =
			    cluster_out->cluster_join_list.strarr_list_len;
			cluster_info->cluster_join_list.list_val =
			    (cl_query_name_t *)calloc((size_t)cluster_out->
			    cluster_join_list.strarr_list_len,
			    sizeof (cl_query_name_t));
			if (cluster_info->cluster_join_list.list_val == NULL) {
				res = CL_QUERY_ENOMEM;
			}
			if (res == CL_QUERY_OK) {
				for (i = 0;
				    i <
				    cluster_out->cluster_join_list.
				    strarr_list_len; i++) {
					if (cluster_out->cluster_join_list.
					    strarr_list_val[i] != NULL) {
						cluster_info->cluster_join_list.
						    list_val[i] =
						    (char *)strdup(cluster_out->
						    cluster_join_list.
						    strarr_list_val[i]);
						if (cluster_info->
						    cluster_join_list.
						    list_val[i] == NULL) {
							res = CL_QUERY_ENOMEM;
							cluster_info->
							    cluster_join_list.
							    list_len = i;
							break;
						}
					}
				}
			}
		}
	}


	if (res == CL_QUERY_OK) {
		if (cluster_out->cluster_netaddr != NULL) {
			cluster_info->cluster_netaddr =
			    (char *)strdup(cluster_out->cluster_netaddr);
			if (cluster_info->cluster_netaddr == NULL) {
				res = CL_QUERY_ENOMEM;
			}
		}
	}

	if (res == CL_QUERY_OK) {
		if (cluster_out->cluster_netmask != NULL) {
			cluster_info->cluster_netmask =
			    (char *)strdup(cluster_out->cluster_netmask);
			if (cluster_info->cluster_netmask == NULL) {
				res = CL_QUERY_ENOMEM;
			}
		}
	}
	if (res == CL_QUERY_OK) {
		if (cluster_out->cluster_max_nodes != NULL) {
			cluster_info->cluster_max_nodes =
			    (char *)strdup(cluster_out->cluster_max_nodes);
			if (cluster_info->cluster_max_nodes == NULL) {
				res = CL_QUERY_ENOMEM;
			}
		}
	}

	if (res == CL_QUERY_OK) {
		if (cluster_out->cluster_max_priv_nets != NULL) {
			cluster_info->cluster_max_priv_nets =
			    (char *)strdup(cluster_out->cluster_max_priv_nets);
			if (cluster_info->cluster_max_priv_nets == NULL) {
				res = CL_QUERY_ENOMEM;
			}
		}
	}

	if (res == CL_QUERY_OK) {
		cluster_info->fmm_mode =
		    (cl_query_state_t)cluster_out->fmm_mode;
	}

	if (clnt_stat == RPC_SUCCESS) {
		(void) clquery_free_cluster_result(&output);
	}

	if (res != CL_QUERY_OK) {
		(void) cl_query_free_cluster_info(cluster_info);
	} else {
		result->n_elements = 1;
		result->value_list = (void *)cluster_info;
	}

	return (res);

}

/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_NODE_INFO. Information about all the nodes in the
 *    cluster is retrieved from the server and returned to the user.
 *
 *
 * INPUT:
 *    cl_query_name_t : Name of the node or NULL (default case)
 *
 * OUTPUT:
 *    result.n_elements:  No. of nodes
 *    result.value_list:  List of nodes properties.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */


cl_query_error_t
cl_query_node_info(cl_query_name_t in_param, cl_query_result_t *result)
{
	enum clnt_stat clnt_stat;
	cl_query_node_output_t output;
	cl_query_node_prop_t tmp_list;
	cl_query_input_t input;
	cl_query_node_info_t *node = NULL;
	uint_t i = 0;
	cl_query_error_t res = CL_QUERY_OK;

	/*
	 * in_param can be NULL (default case)
	 */
	if (result == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if (in_param != NULL) {
		input.name = in_param;
	} else {
		input.name = NULL;
	}

	/*
	 * Initialize result
	 */
	result->n_elements = 0;
	result->value_list = NULL;

	/*
	 * RPC lib will core if any pointers are not NULL
	 */
	(void) memset(&output, 0, sizeof (output));

	clnt_stat = node_info_get_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	if (res == CL_QUERY_OK) {
		result->n_elements = output.node_list.node_list_len;

		if (result->n_elements == 0) {
			res = CL_QUERY_EUNEXPECTED;
		}
	}

	if (res == CL_QUERY_OK) {
		node = (cl_query_node_info_t *)
		    calloc((size_t)1, sizeof (cl_query_node_info_t));
		if (node == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	}

	if (res == CL_QUERY_OK) {
		result->value_list = (void *)node;
		for (i = 0; i < output.node_list.node_list_len; i++) {
			if (i != 0) {
				node->next = (cl_query_node_info_t *)
				    calloc((size_t)1,
				    sizeof (cl_query_node_info_t));
				if (node->next == NULL) {
					res = CL_QUERY_ENOMEM;
					break;
				} else {
					node = node->next;
				}
			}
			tmp_list = output.node_list.node_list_val[i];


			/*
			 * Get the node info
			 */
			res = get_node_info(tmp_list, node);

		}
		if (node != NULL) {
			node->next = NULL;
		}
	}
	if (res != CL_QUERY_OK) {
		(void) cl_query_free_node_info(result->value_list);
		result->value_list = NULL;
		result->n_elements = 0;

	}
	if (clnt_stat == RPC_SUCCESS) {
		(void) clquery_free_node_result(&output);
	}
	return (res);
}


/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_ADAPTER_INFO. Information of all or specific adapter in
 *    the cluster is retrieved from the server and returned to the user.
 *
 *
 * INPUT:
 *    cl_query_name_t : Name of the adapter or NULL (default case)
 *
 * OUTPUT:
 *    result.n_elements:  No. of adapters
 *    result.value_list:  List of adapters properties.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_adapter_info(cl_query_name_t in_param, cl_query_result_t *result)
{
	enum clnt_stat clnt_stat;
	cl_query_adapter_output_t output;
	cl_query_adapter_prop_t tmp_list;
	cl_query_input_t input;
	uint_t i = 0;
	cl_query_error_t res = CL_QUERY_OK;
	cl_query_adapter_info_t *adapter = NULL;


	/*
	 * in_param can be NULL (default case)
	 */
	if (result == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if (in_param != NULL) {
		input.name = in_param;
	} else {
		input.name = NULL;
	}

	/*
	 * Initialize result
	 */
	result->n_elements = 0;
	result->value_list = NULL;

	(void) memset(&output, 0, sizeof (output));

	clnt_stat = adapter_info_get_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	if (res == CL_QUERY_OK) {
		result->n_elements = output.adapter_list.adapter_list_len;

		if (result->n_elements == 0) {
			res = CL_QUERY_EUNEXPECTED;
		}
	}

	if (res == CL_QUERY_OK) {
		adapter = (cl_query_adapter_info_t *)
		    calloc((size_t)1, sizeof (cl_query_adapter_info_t));
		if (adapter == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	}

	if (res == CL_QUERY_OK) {
		result->value_list = (void *)adapter;

		for (i = 0; i < output.adapter_list.adapter_list_len; i++) {
			if (i != 0) {
				adapter->next = (cl_query_adapter_info_t *)
				    calloc((size_t)1,
				    sizeof (cl_query_adapter_info_t));
				if (adapter->next == NULL) {
					res = CL_QUERY_ENOMEM;
					break;
				}
				adapter = adapter->next;
			}

			tmp_list = output.adapter_list.adapter_list_val[i];
			res = get_adapter_info(tmp_list, adapter);
		}
	}

	if (adapter != NULL) {
		adapter->next = NULL;
	}
	if (res != CL_QUERY_OK) {
		(void) cl_query_free_adapter_info(result->value_list);
		result->value_list = NULL;
		result->n_elements = 0;

	}

	if (clnt_stat == RPC_SUCCESS) {
		(void) clquery_free_adapter_result(&output);
	}

	return (res);
}



/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_QUORUM_INFO. Information of the quorum device in the
 *    cluster is retrieved from the server and returned to the user.
 *
 *
 * INPUT:
 *    cl_query_name_t : Name of the quorum or NULL (default case)
 *
 * OUTPUT:
 *    result.n_elements:  No. of quorums.
 *    result.value_list:  List of quorums properties.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_quorum_info(cl_query_name_t in_param, cl_query_result_t *result)
{
	enum clnt_stat clnt_stat;
	cl_query_quorum_output_t output;
	cl_query_quorum_prop_t tmp_list;
	cl_query_input_t input;
	uint_t i = 0;
	cl_query_error_t res = CL_QUERY_OK;
	cl_query_quorum_info_t *quorum = NULL;


	/*
	 * in_param can be NULL (default case)
	 */
	if (result == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if (in_param != NULL) {
		input.name = in_param;
	} else {
		input.name = NULL;
	}

	/*
	 * Initialize result
	 */
	result->n_elements = 0;
	result->value_list = NULL;

	(void) memset(&output, 0, sizeof (output));


	/*
	 * Get the quorum information from the server
	 */
	clnt_stat = quorum_info_get_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	if (res == CL_QUERY_OK) {
		result->n_elements = output.quorum_list.quorum_list_len;

		if (result->n_elements == 0) {
			res = CL_QUERY_EUNEXPECTED;
		}
	}

	if (res == CL_QUERY_OK) {
		quorum = (cl_query_quorum_info_t *)calloc((size_t)1,
		    sizeof (cl_query_quorum_info_t));
		if (quorum == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	}

	if (res == CL_QUERY_OK) {
		result->value_list = (void *)quorum;
		for (i = 0; i < output.quorum_list.quorum_list_len; i++) {
			if (i != 0) {
				quorum->next = (cl_query_quorum_info_t *)
				    calloc((size_t)1,
				    sizeof (cl_query_quorum_info_t));
				quorum = quorum->next;
			}
			tmp_list = output.quorum_list.quorum_list_val[i];
			res = get_quorum_info(tmp_list, quorum);
		}
	}

	if (res != CL_QUERY_OK) {
		(void) cl_query_free_quorum_info(result->value_list);
		result->value_list = NULL;
		result->n_elements = 0;
	}

	if (clnt_stat == RPC_SUCCESS) {
		(void) clquery_free_quorum_result(&output);
	}

	return (res);
}


/* ARGSUSED */
cl_query_error_t
cl_query_ipmp_info(cl_query_name_t in_param, cl_query_result_t *result)
{
	enum clnt_stat clnt_stat;
	cl_query_ipmp_info_t *ipmp_info = NULL;
	cl_query_ipmp_info_t *ipmp_info_ptr = NULL;
	cl_query_ipmp_prop_t buf;
	cl_query_ipmp_output_t ipmp_output;
	cl_query_input_t input;
	unsigned int cur_grp, i;
	cl_query_error_t res;

	if (result == NULL)
		return (CL_QUERY_EINVAL);

	result->n_elements = 0;
	result->value_list = NULL;

	if ((in_param != NULL) && (strlen(in_param) > 0)) {
		input.name = in_param;
	} else {
		input.name = NULL;
	}



	(void) memset(&ipmp_output, 0, sizeof (ipmp_output));

	clnt_stat = ipmp_info_get_1(&input, &ipmp_output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		return (rpc_to_clquery(clnt_stat));
	}

	if (ipmp_output.error != CL_QUERY_OK) {
		(void) clquery_free_ipmp_result(&ipmp_output);
		return (ipmp_output.error);
	}
	if (ipmp_output.ipmp_list.ipmp_list_len == 0) {
		(void) clquery_free_ipmp_result(&ipmp_output);
		return (CL_QUERY_EUNEXPECTED);
	}



	ipmp_info = NULL;
	res = CL_QUERY_OK;

	for (cur_grp = 0;
	    cur_grp < ipmp_output.ipmp_list.ipmp_list_len; cur_grp++) {



		ipmp_info_ptr =
		    (cl_query_ipmp_info_t *)calloc((size_t)1,
		    sizeof (cl_query_ipmp_info_t));
		if (ipmp_info_ptr == NULL) {
			res = CL_QUERY_EUNEXPECTED;
			break;
		}


		buf = ipmp_output.ipmp_list.ipmp_list_val[cur_grp];

		ipmp_info_ptr->status = buf->status;
		/*
		 * check alloc of strdup
		 */
		ipmp_info_ptr->group_name = (char *)strdup(buf->group_name);
		if (ipmp_info_ptr->group_name == NULL) {
			res = CL_QUERY_EUNEXPECTED;
			break;
		}
		ipmp_info_ptr->node_name = (char *)strdup(buf->node_name);
		if (ipmp_info_ptr->node_name == NULL) {
			res = CL_QUERY_EUNEXPECTED;
			break;
		}
		ipmp_info_ptr->adapter_list.list_len =
		    buf->adapter_list.strarr_list_len;
		ipmp_info_ptr->adapter_list.list_val = (cl_query_name_t *)
		    malloc(ipmp_info_ptr->adapter_list.list_len *
		    (sizeof (cl_query_name_t)));
		if (ipmp_info_ptr->adapter_list.list_val == NULL) {
			res = CL_QUERY_EUNEXPECTED;
			break;
		}
		for (i = 0; i < buf->adapter_list.strarr_list_len; i++) {
			ipmp_info_ptr->adapter_list.list_val[i] =
			    (char *)strdup(
				    buf->adapter_list.strarr_list_val[i]);
			if (ipmp_info_ptr->adapter_list.list_val[i] == NULL) {
				res = CL_QUERY_EUNEXPECTED;
				break;
			}
		}

		if (ipmp_info == NULL) {
			/*
			 * first time
			 */
			ipmp_info = ipmp_info_ptr;
		} else {
			ipmp_info_ptr->next = ipmp_info;
			ipmp_info = ipmp_info_ptr;
		}

	}


	if (res != CL_QUERY_OK) {
		(void) cl_query_free_ipmp_info(ipmp_info);
		ipmp_info = NULL;
	}



	result->n_elements = ipmp_output.ipmp_list.ipmp_list_len;
	result->value_list = (void *)ipmp_info;


	(void) clquery_free_ipmp_result(&ipmp_output);

	return (CL_QUERY_OK);


}

/* ARGSUSED */
cl_query_error_t
cl_query_diskpath_info(cl_query_name_t in_param, cl_query_result_t *result)
{
	enum clnt_stat clnt_stat;
	cl_query_diskpath_output_t diskpath_output;
	cl_query_input_t input;
	cl_query_diskpath_info_t *diskpath_list_head = NULL;
	cl_query_diskpath_info_t *diskpath_list_ptr = NULL;
	unsigned int i;
	cl_query_diskpath_prop_t cur_output;
	cl_query_error_t res = CL_QUERY_OK;
	char *sep;

	if (result == NULL)
		return (CL_QUERY_EINVAL);

	result->n_elements = 0;
	result->value_list = NULL;

	if ((in_param != NULL) && (strlen(in_param) > 0)) {
		input.name = in_param;
	} else {
		input.name = NULL;
	}

	(void) memset(&diskpath_output, 0, sizeof (diskpath_output));

	clnt_stat = disk_path_info_get_1(&input,
	    &diskpath_output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		return (rpc_to_clquery(clnt_stat));
	}

	if (diskpath_output.error != CL_QUERY_OK) {
		(void) clquery_free_diskpath_result(&diskpath_output);
		return (diskpath_output.error);
	}

	if (diskpath_output.diskpath_list.diskpath_list_len == 0) {
		(void) clquery_free_diskpath_result(&diskpath_output);
		return (CL_QUERY_EUNEXPECTED);
	}


	diskpath_list_head = NULL;

	res = CL_QUERY_OK;

	/*
	 * in case of node down server may return
	 * a path like <node id>:<path> instead of <path>
	 * cf clquery_get_monitoring_status()
	 * in that case remove leading "<node id>:"
	 */


	for (i = 0; i < diskpath_output.diskpath_list.diskpath_list_len; i++) {

		diskpath_list_ptr = (cl_query_diskpath_info_t *)calloc(
			(size_t)1, sizeof (cl_query_diskpath_info_t));
		if (diskpath_list_ptr == NULL) {
			res = CL_QUERY_EUNEXPECTED;
			break;
		}

		cur_output = diskpath_output.diskpath_list.diskpath_list_val[i];

		diskpath_list_ptr->status = cur_output->status;

		diskpath_list_ptr->node_name =
		    (char *)strdup(cur_output->node_name);
		if (diskpath_list_ptr->node_name == NULL) {
			res = CL_QUERY_EUNEXPECTED;
			break;
		}

		sep = strchr(cur_output->diskpath_name, ':');
		if (sep != NULL) {
			sep++;	/* skip ':' */
		} else {
			sep = cur_output->diskpath_name;
		}

		diskpath_list_ptr->dpath_name = (char *)strdup(sep);
		if (diskpath_list_ptr->dpath_name == NULL) {
			res = CL_QUERY_EUNEXPECTED;
			break;
		}

		if (diskpath_list_head == NULL) {
			/*
			 * first time
			 */
			diskpath_list_head = diskpath_list_ptr;
		} else {
			diskpath_list_ptr->next = diskpath_list_head;
			diskpath_list_head = diskpath_list_ptr;
		}
	}
	if (res != CL_QUERY_OK) {
		(void) cl_query_free_diskpath_info(diskpath_list_head);
		diskpath_list_head = NULL;

	}

	result->n_elements = diskpath_output.diskpath_list.diskpath_list_len;
	result->value_list = (void *)diskpath_list_head;


	(void) clquery_free_diskpath_result(&diskpath_output);

	return (CL_QUERY_OK);
}


#ifdef CL_QUERY_DEBUG
static void
print_debug(cl_query_device_output_t *output)
{
	uint_t device_count = 0;
	cl_query_device_prop_t dev_list;

	if (output == NULL) {
		(void) fprintf(stderr, "OUTPUT IS NULL\n !");
		return;
	}


	(void) fprintf(stderr, "returning %d elems\n",
	    output->device_list.device_list_len);
	for (device_count = 0;
	    device_count < output->device_list.device_list_len;
	    device_count++) {
		dev_list = output->device_list.device_list_val[device_count];
		if (dev_list == NULL) {
			(void) fprintf(stderr,
			    "DEVICE VAL NUMBER %d IS NULL !!\n", device_count);
		} else {
			(void) fprintf(stderr, "logical disk path = %s\n",
			    (dev_list->logical_disk_path == NULL ? "NULL" :
				dev_list->logical_disk_path));
			(void) fprintf(stderr, "file system path = TODO\n");
			(void) fprintf(stderr, "device name = %s\n",
			    (dev_list->device_name == NULL ? "NULL" :
				dev_list->device_name));
			(void) fprintf(stderr, "primary node name = %s\n",
			    (dev_list->primary_node_name == NULL ? "NULL" :
				dev_list->primary_node_name));
			(void) fprintf(stderr, "device type  = %s\n",
			    (dev_list->device_type == NULL ? "NULL" :
				dev_list->device_type));
			(void) fprintf(stderr,
			    " desired number of secondaries = %d\n",
			    dev_list->desired_num_secondaries);


		}

	}

}

#endif

/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_DEVICE_INFO. Information of all the disk groups in the
 *    cluster is retrieved from the server and returned to the user.
 *
 *
 * INPUT:
 *    cl_query_name_t : Name of the disk group or NULL (default case)
 *
 * OUTPUT:
 *    result.n_elements:  No. of disk groups
 *    result.value_list:  List of disks properties
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_device_info(cl_query_name_t in_param, cl_query_result_t *result)
{
	enum clnt_stat clnt_stat;
	cl_query_device_output_t output;
	cl_query_device_prop_t tmp_list;
	cl_query_input_t input;
	uint_t i = 0;
	cl_query_error_t res = CL_QUERY_OK;
	cl_query_device_info_t *dev = NULL;


	/*
	 * in_param can be NULL (default case)
	 */
	if (result == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if (in_param != NULL) {
		input.name = in_param;
	} else {
		input.name = NULL;
	}

	/*
	 * Initialize result
	 */
	result->n_elements = 0;
	result->value_list = NULL;

	(void) memset(&output, 0, sizeof (output));


	/*
	 * Get the quorum information from the server
	 */
	clnt_stat = device_info_get_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}


	if (res == CL_QUERY_OK) {
		result->n_elements = output.device_list.device_list_len;
		if (result->n_elements == 0) {
			res = CL_QUERY_EUNEXPECTED;
		}
	}
#ifdef CL_QUERY_DEBUG
	(void) fprintf(stderr, "REPSONSE FROM SERVER\n");
	print_debug(&output);
#endif

	if (res == CL_QUERY_OK) {
		dev = (cl_query_device_info_t *)
		    calloc((size_t)1, sizeof (cl_query_device_info_t));
		if (dev == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	}

	if (res == CL_QUERY_OK) {
		result->value_list = (void *)dev;
		for (i = 0; i < result->n_elements; i++) {
			if (i != 0) {
				dev->next = (cl_query_device_info_t *)
				    calloc((size_t)1,
				    sizeof (cl_query_device_info_t));
				if (dev->next == NULL) {
					res = CL_QUERY_ENOMEM;
					break;
				} else {
					dev = dev->next;
				}
			}

			tmp_list = output.device_list.device_list_val[i];
			res = get_device_info(tmp_list, dev);
		}
		if (dev != NULL) {
			dev->next = NULL;
		}
	}

	if (res != CL_QUERY_OK) {
		(void) cl_query_free_device_info(result->value_list);
		result->n_elements = 0;
		result->value_list = NULL;
	}


	if (clnt_stat == RPC_SUCCESS) {
		(void) clquery_free_device_result(&output);
	}

	return (res);
}


/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_CABLE_INFO. Information of all the cables in the
 *    cluster is retrieved from the server and returned to the user.
 *
 *
 * INPUT:
 *    cl_query_name_t : Name of the cable or NULL (default case)
 *
 * OUTPUT:
 *    result.n_elements:  No. of cables
 *    result.value_list:  List of cable properties
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_cable_info(cl_query_name_t in_param, cl_query_result_t *result)
{
	enum clnt_stat clnt_stat;
	cl_query_cable_output_t output;
	cl_query_cable_prop_t tmp_list;
	cl_query_input_t input;
	uint_t i = 0;
	cl_query_error_t res = CL_QUERY_OK;
	cl_query_cable_info_t *cable = NULL;

	/*
	 * in_param can be NULL (default case)
	 */
	if (result == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if (in_param != NULL) {
		input.name = in_param;
	} else {
		input.name = NULL;
	}

	/*
	 * Initialize result
	 */
	result->n_elements = 0;
	result->value_list = NULL;

	(void) memset(&output, 0, sizeof (output));

	/*
	 * Get the cable information from the server
	 */
	clnt_stat = cable_info_get_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	if (res == CL_QUERY_OK) {
		result->n_elements = output.cable_list.cable_list_len;

		if (result->n_elements == 0) {
			res = CL_QUERY_EUNEXPECTED;
		}
	}

	if (res == CL_QUERY_OK) {
		cable = (cl_query_cable_info_t *)
		    calloc((size_t)1, sizeof (cl_query_cable_info_t));
		if (cable == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	}

	if (res == CL_QUERY_OK) {
		result->value_list = (void *)cable;
		for (i = 0; i < result->n_elements; i++) {
			if (i != 0) {
				cable->next = (cl_query_cable_info_t *)
				    calloc((size_t)1,
				    sizeof (cl_query_cable_info_t));
				cable = cable->next;
			}

			tmp_list = output.cable_list.cable_list_val[i];
			res = get_cable_info(tmp_list, cable);
		}
	}

	if (cable != NULL) {
		cable->next = NULL;
	}

	if (res != CL_QUERY_OK) {
		(void) cl_query_free_cable_info(result->value_list);
		result->value_list = NULL;
		result->n_elements = 0;
	}

	if (clnt_stat == RPC_SUCCESS) {
		(void) clquery_free_cable_result(&output);
	}

	return (res);
}


/*
 * DESCRIPTION:
 *    This method is the library entry point for the API call
 *    CL_QUERY_JUUNCTION_INFO. Information of all the junctions in the
 *    cluster is retrieved from the server and returned to the user.
 *
 *
 * INPUT:
 *    cl_query_name_t : Name of the junction or NULL (default case)
 *
 * OUTPUT:
 *    result.n_elements:  No. of junctions
 *    result.value_list:  List of junction properties
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 */

cl_query_error_t
cl_query_junction_info(cl_query_name_t in_param, cl_query_result_t *result)
{
	enum clnt_stat clnt_stat;
	cl_query_junction_output_t output;
	cl_query_junction_prop_t tmp_list;
	cl_query_input_t input;
	uint_t count = 0;
	cl_query_error_t res = CL_QUERY_OK;
	cl_query_junction_info_t *junction = NULL;

	/*
	 * in_param can be NULL (default case)
	 */
	if (result == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if (in_param != NULL) {
		input.name = in_param;
	} else {
		input.name = NULL;
	}

	/*
	 * Initialize result
	 */
	result->n_elements = 0;
	result->value_list = NULL;

	(void) memset(&output, 0, sizeof (output));

	/*
	 * Get the junction information from the server
	 */
	clnt_stat = junction_info_get_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	if (res == CL_QUERY_OK) {
		result->n_elements = output.junction_list.junction_list_len;
		if (result->n_elements == 0) {
			res = CL_QUERY_EUNEXPECTED;
		}
	}

	if (res == CL_QUERY_OK) {
		junction = (cl_query_junction_info_t *)
		    calloc((size_t)1, sizeof (cl_query_junction_info_t));
		if (junction == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	}

	if (res == CL_QUERY_OK) {
		result->value_list = (void *)junction;
		for (count = 0; count < result->n_elements; count++) {
			if (count != 0) {
				junction->next = (cl_query_junction_info_t *)
				    calloc((size_t)1,
				    sizeof (cl_query_junction_info_t));
				junction = junction->next;
			}

			tmp_list =
			    output.junction_list.junction_list_val[count];
			res = get_junction_info(tmp_list, junction);
		}
	}
	if (junction != NULL) {
		junction->next = NULL;
	}

	if (res != CL_QUERY_OK) {
		(void) cl_query_free_junction_info(result->value_list);
		result->value_list = NULL;
		result->n_elements = 0;
	}

	if (clnt_stat == RPC_SUCCESS) {
		(void) clquery_free_junction_result(&output);
	}

	return (res);
}


cl_query_error_t
cl_query_transportpath_info(
	cl_query_name_t in_param,
	    cl_query_result_t *result) {

	enum clnt_stat clnt_stat;
	cl_query_transportpath_output_t output;
	cl_query_transportpath_prop_t tmp_list;
	cl_query_input_t input;

	uint_t count = 0;
	cl_query_error_t res = CL_QUERY_OK;
	cl_query_transportpath_info_t *tpath = NULL;

	/*
	 * in_param can be NULL (default case)
	 */
	if (result == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if (in_param != NULL) {
		input.name = in_param;
	} else {
		input.name = NULL;
	}

	/*
	 * Initialize result
	 */
	result->n_elements = 0;
	result->value_list = NULL;

	(void) memset(&output, 0, sizeof (output));

	clnt_stat =
	    transport_path_info_get_1(&input, &output, _cl_query_client);
	if (clnt_stat != RPC_SUCCESS) {
		res = rpc_to_clquery(clnt_stat);
	} else {
		if (output.error != CL_QUERY_OK) {
			res = output.error;
		}
	}

	if (res == CL_QUERY_OK) {
		result->n_elements =
		    output.transportpath_list.transportpath_list_len;
		if (result->n_elements == 0) {
			res = CL_QUERY_ENOTCONFIGURED;
		}
	}

	if (res == CL_QUERY_OK) {
		tpath = (cl_query_transportpath_info_t *)
		    calloc((size_t)1, sizeof (cl_query_transportpath_info_t));
		if (tpath == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	}

	if (res == CL_QUERY_OK) {
		result->value_list = (void *)tpath;
		for (count = 0; count < result->n_elements; count++) {
			if (count != 0) {
				tpath->next = (cl_query_transportpath_info_t *)
				    calloc((size_t)1,
				    sizeof (cl_query_transportpath_info_t));
				tpath = tpath->next;
			}

			tmp_list =
			    output.transportpath_list.
			    transportpath_list_val[count];
			res = get_transportpath_info(tmp_list, tpath);
			if (res != CL_QUERY_OK) break;
		}
	}
	if (tpath != NULL) {
		tpath->next = NULL;
	}

	if (res != CL_QUERY_OK) {
		(void) cl_query_free_transportpath_info(result->value_list);
		result->value_list = NULL;
		result->n_elements = 0;
	}

	if (clnt_stat == RPC_SUCCESS) {
		(void) clquery_free_transportpath_result(&output);
	}

	return (res);


}
/*
 * DESCRIPTION:
 *    This method initialises the libclquery library. RPC connection
 *    is established with the server. The connection is created for
 *    each API call. This routine MUST BE INVOKED at the entry point
 *    of each API call. At the end of the API call the cleanup routine
 *    must be invoked.
 *
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS: _cl_query_lib_lock is taken to make sure only one RPC
 *        call is made at any point from the library.
 */

#ifdef MULTIPLE_CLNT
cl_query_error_t
_cl_query_lib_init(CLIENT ** clnt)
{

	CLIENT *tmp = NULL;

	tmp = clnt_create("localhost", CLQUERY_SERVER, CLQUERY_VERS, "tcp");


	if (tmp == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}

	*clnt = tmp;

	return (CL_QUERY_OK);
}
#else

cl_query_error_t
_cl_query_lib_init()
{
	int res = -1;

	res = pthread_mutex_lock(&_cl_query_lib_lock);
	if (res != 0) {
		return (CL_QUERY_EUNEXPECTED);
	}

	if (_cl_query_client == NULL) {
		_cl_query_client = clnt_create("localhost",
		    CLQUERY_SERVER, CLQUERY_VERS, "tcp");
	}

	if (_cl_query_client == NULL) {
		(void) pthread_mutex_unlock(&_cl_query_lib_lock);
		return (CL_QUERY_EUNEXPECTED);
	}

	return (CL_QUERY_OK);
}
#endif

/*
 * DESCRIPTION:
 *    This method destroys the RPC connection with the server.
 *    This routine MUST BE INVOKED at the end of the each API call.
 *
 * INPUT:
 *    None.
 *
 * OUTPUT:
 *    None.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS: _cl_query_lib_lock is released. Any pending RPC request can
 *        go through.
 */

#ifdef MULTIPLE_CLNT

cl_query_error_t
_cl_query_lib_cleanup(CLIENT * clnt)
{

	if (clnt == NULL)
		return (CL_QUERY_OK);

	clnt_destroy(clnt);

	return (CL_QUERY_OK);
}

#else
cl_query_error_t
_cl_query_lib_cleanup()
{

	int res = -1;


	if (_cl_query_client != NULL) {
		clnt_destroy(_cl_query_client);
		_cl_query_client = NULL;
	}

	res = pthread_mutex_unlock(&_cl_query_lib_lock);
	if (res != 0) {
		return (CL_QUERY_EUNEXPECTED);
	}

	return (CL_QUERY_OK);
}

#endif

/*
 * DESCRIPTION:
 *    This is the private API call exposed to the clients.
 *    This acts a dispatch routine and invokes the appropriate
 *    function depending on the request.
 *
 * INPUT:
 *    cl_query_type_t:  Type of the query.(Node, Adapter, Quorum....)
 *    cl_query_name_t:  Name of the entity or NULL (default case)
 *
 * OUTPUT:
 *    cl_query_result_t:  Results returned from the server.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 *
 */

cl_query_error_t
cl_query_get_info(cl_query_type_t query_type, cl_query_name_t input,
    cl_query_result_t *result)
{
	cl_query_error_t res;

#ifdef MULTIPLE_CLNT
	CLIENT *_my_client = NULL;
#endif

	/*
	 * Input is NULL for default case
	 */
	if (result == NULL) {
		return (CL_QUERY_EINVAL);
	}
#ifdef MULTIPLE_CLNT
	res = _cl_query_lib_init(&_my_client);
#else
	res = _cl_query_lib_init();
#endif

	if (res == CL_QUERY_OK) {
		switch (query_type) {
		case CL_QUERY_CLUSTER_INFO:
			res = cl_query_cluster_info(input, result);
			break;

		case CL_QUERY_NODE_INFO:
			res = cl_query_node_info(input, result);
			break;

		case CL_QUERY_ADAPTER_INFO:
			res = cl_query_adapter_info(input, result);
			break;

		case CL_QUERY_QUORUM_INFO:
			res = cl_query_quorum_info(input, result);
			break;

		case CL_QUERY_CABLE_INFO:
			res = cl_query_cable_info(input, result);
			break;

		case CL_QUERY_JUNCTION_INFO:
			res = cl_query_junction_info(input, result);
			break;

		case CL_QUERY_IPMP_INFO:
			res = cl_query_ipmp_info(input, result);
			break;

		case CL_QUERY_DEVICE_INFO:
			res = cl_query_device_info(input, result);
			break;

		case CL_QUERY_DISKPATH_INFO:
			res = cl_query_diskpath_info(input, result);
			break;
		case CL_QUERY_TRANSPORTPATH_INFO:
			res = cl_query_transportpath_info(input, result);
			break;
		default:
			return (CL_QUERY_EINVAL);
		}
	}
#ifdef MULTIPLE_CLNT
	(void) _cl_query_lib_cleanup(_my_client);
#else
	(void) _cl_query_lib_cleanup();
#endif
	return (res);
}


/*
 * DESCRIPTION:
 *    call the appropriate free function according to the
 *    flag 'query_type'
 *
 *
 *
 * INPUT:
 *    cl_query_type_t:  Type of the query.(Node, Adapter, Quorum....)
 *
 *
 * OUTPUT:
 *    cl_query_result_t:  Results to be freed.
 *
 * RETURN:
 *    CL_QUERY_OK:  On Success
 *    CL_QUERY_EXX: On error
 *
 * SIDE EFFECTS:
 *    None
 *
 * LOCKS:
 *
 */

cl_query_error_t
cl_query_free_result(cl_query_type_t query_type, cl_query_result_t *result)
{

	cl_query_error_t res;

	/*
	 * Input is NULL for default case
	 */
	if (result == NULL) {
		return (CL_QUERY_EINVAL);
	}


	if (result->n_elements == 0)
		return (CL_QUERY_EINVAL);
	if (result->value_list == NULL)
		return (CL_QUERY_EINVAL);

	switch (query_type) {
	case CL_QUERY_CLUSTER_INFO:
		{
			res = cl_query_free_cluster_info(
			    (cl_query_cluster_info_t *)(result->value_list));
			break;
		}
	case CL_QUERY_NODE_INFO:
		{
			res = cl_query_free_node_info(
			    (cl_query_node_info_t *)(result->value_list));
			break;
		}
	case CL_QUERY_ADAPTER_INFO:
		{
			res = cl_query_free_adapter_info(
			    (cl_query_adapter_info_t *)(result->value_list));
			break;
		}
	case CL_QUERY_QUORUM_INFO:
		{
			res = cl_query_free_quorum_info(
			    (cl_query_quorum_info_t *)(result->value_list));
			break;
		}

	case CL_QUERY_CABLE_INFO:
		{
			res = cl_query_free_cable_info(
			    (cl_query_cable_info_t *)(result->value_list));
			break;
		}
	case CL_QUERY_JUNCTION_INFO:
		{
			res = cl_query_free_junction_info(
			    (cl_query_junction_info_t *)(result->value_list));
			break;
		}
	case CL_QUERY_IPMP_INFO:
		{
			res = cl_query_free_ipmp_info(
			    (cl_query_ipmp_info_t *)(result->value_list));
			break;
		}
	case CL_QUERY_DEVICE_INFO:
		{
			res = cl_query_free_device_info(
			    (cl_query_device_info_t *)(result->value_list));
			break;
		}
	case CL_QUERY_DISKPATH_INFO:
		{
			res = cl_query_free_diskpath_info(
			    (cl_query_diskpath_info_t *)(result->value_list));
			break;
		}
	case CL_QUERY_TRANSPORTPATH_INFO:
	{
		res = cl_query_free_transportpath_info(
			(cl_query_transportpath_info_t *)(result->value_list));
		break;
	}
	default:
		return (CL_QUERY_EINVAL);
	}

	if (res == CL_QUERY_OK) {
		result->n_elements = 0;
		/*
		 *value_list is a linked list ->
		 *already freeed inside free function ()
		 */
		result->value_list = NULL;
	}

	return (res);

}
