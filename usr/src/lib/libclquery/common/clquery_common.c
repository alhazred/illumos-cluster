/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)clquery_common.c	1.13	08/05/20 SMI"

#include <stdlib.h>
#include <strings.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

#include <cl_query/rpc/cl_query.h>
#include <cl_query/cl_query_types.h>

#define	STR_NOT_EMPTY(A)	((A != NULL) && (A[0] != '\0'))

static cl_query_error_t
port_prop_dup(cl_query_port_prop_list_t *src, cl_query_port_list_t *dest);

static cl_query_error_t disk_map_prop_dup(cl_query_didpath_list_t *src,
    cl_query_path_list_t *dest);

cl_query_error_t
get_node_info(cl_query_node_prop_t res_list, cl_query_node_info_t *node)
{

	uint_t count = 0;
	cl_query_error_t res = CL_QUERY_OK;

	if (res_list == NULL || node == NULL) {
		return (CL_QUERY_EINVAL);
	}

	if (res_list->node_name != NULL && strlen(res_list->node_name) != 0) {
		node->node_name = (char *)strdup(res_list->node_name);
		if (node->node_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		node->node_name = NULL;
	}

	if (res_list->node_category != NULL &&
	    strlen(res_list->node_category) != 0) {
		node->node_category = (char *)strdup(res_list->node_category);
		if (node->node_category == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		node->node_category = NULL;
	}

	/*
	 * Get private host name for the node
	 */
	if (res_list->private_host_name != NULL &&
	    strlen(res_list->private_host_name) != 0) {
		node->private_host_name =
		    (char *)strdup(res_list->private_host_name);
		if (node->private_host_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		node->private_host_name = NULL;
	}

	if ((res_list->reservation_key != NULL) &&
	    (strlen(res_list->reservation_key) != 0)) {
		node->resv_key = (char *)strdup(res_list->reservation_key);
		if (node->resv_key == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		node->resv_key = NULL;
	}




	/*
	 * Store the private adapters info
	 */
	node->private_adapters.list_len =
	    res_list->private_adapters.strarr_list_len;

	if (node->private_adapters.list_len > 0) {
		/*
		 * Allocate memory for the adapter list
		 */
		node->private_adapters.list_val = (char **)calloc((size_t)
		    node->private_adapters.list_len, sizeof (char *));
		if (node->private_adapters.list_val == NULL) {
			res = CL_QUERY_ENOMEM;
		}

		count = 0;
		for (; count < node->private_adapters.list_len; count++) {
			if (res_list->private_adapters.strarr_list_val[count] !=
			    NULL) {
				node->private_adapters.list_val[count] =
				    (char *)strdup(res_list->private_adapters.
				    strarr_list_val[count]);
				if (node->private_adapters.list_val[count] ==
				    NULL) {
					res = CL_QUERY_ENOMEM;
				}
			}
		}
	} else {
		node->private_adapters.list_val = NULL;
	}

	/*
	 * Store the public adapter info
	 */
	node->public_adapters.list_len =
	    res_list->public_adapters.strarr_list_len;

	if (node->public_adapters.list_len > 0) {
		/*
		 * Allocate memory for the adapter list
		 */
		node->public_adapters.list_val = (char **)calloc((size_t)
		    node->public_adapters.list_len, sizeof (char *));
		if (node->public_adapters.list_val == NULL) {
			res = CL_QUERY_ENOMEM;
		}

		count = 0;
		for (; count < node->public_adapters.list_len; count++) {
			if (res_list->public_adapters.strarr_list_val[count]
			    != NULL) {
				node->public_adapters.list_val[count] =
				    (char *)strdup(res_list->public_adapters.
				    strarr_list_val[count]);
				if (node->public_adapters.list_val[count] ==
				    NULL) {
					res = CL_QUERY_ENOMEM;
				}
			}
		}
	} else {
		node->public_adapters.list_val = NULL;
	}

	/*
	 * Store the quorum votes
	 */
	node->cur_quorum_votes = res_list->cur_quorum_votes;
	node->possible_quorum_votes = res_list->possible_quorum_votes;

	/*
	 * Get the node id
	 */
	node->node_id = res_list->node_id;

	node->state = res_list->state;
	node->status = res_list->status;

	/*
	 * Reboot once on failure of all local disks
	 */
	node->node_reboot_flag_status = res_list->node_reboot_flag_status;

	return (res);
}

cl_query_error_t
port_prop_dup(cl_query_port_prop_list_t *src, cl_query_port_list_t *dest)
{
	unsigned int i;
	cl_query_port_prop_elem_t buf;

	dest->list_len = src->prop_list.prop_list_len;
	dest->list_val = (cl_port_prop_t *)calloc((size_t)dest->list_len,
	    sizeof (cl_port_prop_t));
	if (dest->list_val == NULL) {
		return (CL_QUERY_ENOMEM);
	}

	for (i = 0; i < dest->list_len; i++) {
		buf = src->prop_list.prop_list_val[i];
		if (buf == NULL)
			continue;
		dest->list_val[i].port_id = buf->port_id;
		dest->list_val[i].port_state = buf->port_state;
		if (buf->port_name != NULL) {
			/*
			 * XXX TODO : why RPC may decode a empty string instead
			 * of NULL ??!
			 */
			if (strlen(buf->port_name) > 0) {
				dest->list_val[i].port_name =
				    (char *)strdup(buf->port_name);
				if (dest->list_val[i].port_name == NULL) {
					return (CL_QUERY_ENOMEM);
				}
			} else {
				dest->list_val[i].port_name = NULL;
			}
		} else {
			dest->list_val[i].port_name = NULL;
		}
	}



	return (CL_QUERY_OK);
}

static cl_query_error_t
disk_map_prop_dup(cl_query_didpath_list_t *src, cl_query_path_list_t *dest)
{
	unsigned int i;
	cl_query_didpath_list_elem_t buf;

	dest->list_len = src->cl_query_didpath_list_t_len;
	dest->list_val = (cl_query_path_t *)calloc((size_t)dest->list_len,
	    sizeof (cl_query_path_t));
	if (dest->list_val == NULL) {
		return (CL_QUERY_ENOMEM);
	}

	for (i = 0; i < dest->list_len; i++) {

		buf = src->cl_query_didpath_list_t_val[i];
		if (buf == NULL)
			continue;

		/*
		 * XXX TODO : why rpc may decode an empty string instead of
		 * NULL string ???!
		 */

		if (buf->node_name != NULL) {
			if (strlen(buf->node_name) > 0) {
				dest->list_val[i].node_name =
				    (char *)strdup(buf->node_name);
				if (dest->list_val[i].node_name == NULL) {
					return (CL_QUERY_ENOMEM);
				}
			} else {
				dest->list_val[i].node_name = NULL;
			}
		} else {
			dest->list_val[i].node_name = NULL;
		}

		if (buf->phys_path_name != NULL) {
			/*
			 * XXX TODO : why rpc may decode an empty string
			 * instead of NULL string ???!
			 */
			if (strlen(buf->phys_path_name) > 0) {
				dest->list_val[i].phys_path_name =
				    (char *)strdup(buf->phys_path_name);
				if (dest->list_val[i].phys_path_name == NULL) {
					return (CL_QUERY_ENOMEM);
				}
			} else {
				dest->list_val[i].phys_path_name = NULL;
			}
		} else {
			dest->list_val[i].phys_path_name = NULL;
		}

		if (buf->mount_point != NULL) {
			/*
			 * XXX TODO : why rpc may decode an empty string
			 * instead of NULL string ???!
			 */
			if (strlen(buf->mount_point) > 0) {
				dest->list_val[i].mount_point =
				    (char *)strdup(buf->mount_point);
				if (dest->list_val[i].mount_point == NULL) {
					return (CL_QUERY_ENOMEM);
				}
			} else {
				dest->list_val[i].mount_point = NULL;
			}
		} else {
			dest->list_val[i].mount_point = NULL;
		}




	}



	return (CL_QUERY_OK);
}


cl_query_error_t
get_adapter_info(cl_query_adapter_prop_t res_list,
    cl_query_adapter_info_t *adapter)
{
	cl_query_error_t res = CL_QUERY_OK;



	if (res_list == NULL || adapter == NULL) {
		return (CL_QUERY_EINVAL);
	}

	adapter->adapter_id = res_list->adapter_id;
	adapter->vlan_id = res_list->vlan_id;
	adapter->bandwidth = res_list->bandwidth;
	adapter->device_instance = res_list->device_instance;
	adapter->dlpi_heartbeat_quantum = res_list->dlpi_heartbeat_quantum;
	adapter->dlpi_heartbeat_timeout = res_list->dlpi_heartbeat_timeout;
	adapter->status = (cl_query_state_t)res_list->state;

	adapter->network_bandwidth = res_list->network_bandwidth;

	if (res_list->ip_addr != NULL && strlen(res_list->ip_addr) != 0) {
		adapter->ip_addr = (char *)strdup(res_list->ip_addr);
		if (adapter->ip_addr == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		adapter->ip_addr = NULL;
	}

	if (res_list->netmask != NULL && strlen(res_list->netmask) != 0) {
		adapter->netmask = (char *)strdup(res_list->netmask);
		if (adapter->netmask == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		adapter->netmask = NULL;
	}

	if (res_list->node_name != NULL && strlen(res_list->node_name) != 0) {
		adapter->node_name = (char *)strdup(res_list->node_name);
		if (adapter->node_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		adapter->node_name = NULL;
	}

	if (res_list->type != NULL && strlen(res_list->type) != 0) {
		adapter->type = (char *)strdup(res_list->type);
		if (adapter->type == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		adapter->type = NULL;
	}

	if (res_list->adapter_name != NULL &&
	    strlen(res_list->adapter_name) != 0) {
		adapter->adapter_name = (char *)strdup(res_list->adapter_name);
		if (adapter->adapter_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		adapter->adapter_name = NULL;
	}

	if ((res_list->device_name != NULL) &&
	    (strlen(res_list->device_name) != 0)) {
		adapter->device_name = (char *)strdup(res_list->device_name);
		if (adapter->device_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		adapter->device_name = NULL;
	}




	/*
	 * Store the port list
	 */
	if ((res_list->port_list.prop_list.prop_list_len > 0) &&
	    (res_list->port_list.prop_list.prop_list_val != NULL)) {

		if (port_prop_dup(&(res_list->port_list), &(adapter->port_list))
		    != CL_QUERY_OK) {
			res = CL_QUERY_ENOMEM;
		}

	} else {
		adapter->port_list.list_len = 0;
		adapter->port_list.list_val = NULL;
	}

	/*
	 * Get lazy free property
	 */
	adapter->lazy_free = res_list->lazy_free;


	return (res);
}

cl_query_error_t
get_quorum_info(cl_query_quorum_prop_t res_list,
    cl_query_quorum_info_t *quorum)
{
	uint_t count = 0;
	cl_query_error_t res = CL_QUERY_OK;


	quorum->current_votes = res_list->current_votes;
	quorum->possible_votes = res_list->possible_votes;

	/*
	 * quorum wise vote status
	 */
	quorum->votes_needed = res_list->votes_needed;
	quorum->votes_configured = res_list->votes_configured;
	quorum->votes_present = res_list->votes_present;

	quorum->status = (cl_query_state_t)res_list->status;
	quorum->state = (cl_query_state_t)res_list->state;
	quorum->owner = res_list->owner;

	/*
	 * Store the device name
	 */
	if ((res_list->device_name != NULL) &&
	    (strlen(res_list->device_name) != 0)) {
		quorum->device_name = (char *)strdup(res_list->device_name);
		if (quorum->device_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->device_name = NULL;
	}

	/*
	 * Store the key name for the quorum
	 */
	if (res_list->key_name != NULL && strlen(res_list->key_name) != 0) {
		quorum->key_name = (char *)strdup(res_list->key_name);
		if (quorum->key_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->key_name = NULL;
	}

	/*
	 * Store the device type
	 */
	if ((res_list->device_type != NULL) &&
	    (strlen(res_list->device_type) != 0)) {
		quorum->device_type = (char *)strdup(res_list->device_type);
		if (quorum->device_type == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->device_type = NULL;
	}

	/*
	 * Store the nas filer name
	 */
	if ((res_list->filer_name != NULL) &&
	    (strlen(res_list->filer_name) != 0)) {
		quorum->filer_name = (char *)strdup(res_list->filer_name);
		if (quorum->filer_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->filer_name = NULL;
	}

	/*
	 * Store the nas filer lun id
	 */
	if ((res_list->lunid != NULL) &&
	    (strlen(res_list->lunid) != 0)) {
		quorum->lunid = (char *)strdup(res_list->lunid);
		if (quorum->lunid == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->lunid = NULL;
	}

	/*
	 * Store the nas filer lun name
	 */
	if ((res_list->lun_name != NULL) &&
	    (strlen(res_list->lun_name) != 0)) {
		quorum->lun_name = (char *)strdup(res_list->lun_name);
		if (quorum->lun_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->lun_name = NULL;
	}


	/*
	 * Store Quorum server Host
	 */
	if ((res_list->qs_host != NULL) &&
	    (strlen(res_list->qs_host) != 0)) {
		quorum->qs_host = (char *)strdup(res_list->qs_host);
		if (quorum->qs_host == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->qs_host = NULL;
	}

	/*
	 * Store Quorum server port number
	 */
	if ((res_list->qs_port != NULL) &&
	    (strlen(res_list->qs_port) != 0)) {
		quorum->qs_port = (char *)strdup(res_list->qs_port);
		if (quorum->qs_port == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->qs_port = NULL;
	}

	/*
	 * Store Quorum server Host name
	 */
	if ((res_list->qs_name != NULL) &&
	    (strlen(res_list->qs_name) != 0)) {
		quorum->qs_name = (char *)strdup(res_list->qs_name);
		if (quorum->qs_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->qs_name = NULL;
	}

	/*
	 * Allocate the memory for the disabled list
	 */
	quorum->disabled_nodes.list_len =
	    res_list->disabled_nodes.strarr_list_len;
	if (quorum->disabled_nodes.list_len != 0) {
		quorum->disabled_nodes.list_val = (char **)calloc((size_t)
		    quorum->disabled_nodes.list_len, sizeof (char *));
		if (quorum->disabled_nodes.list_val == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->disabled_nodes.list_val = NULL;
	}

	/*
	 * Copy the list of disabled nodes
	 */
	if (quorum->disabled_nodes.list_val != NULL) {
		for (count = 0; count < quorum->disabled_nodes.list_len;
		    count++) {
			if (res_list->disabled_nodes.strarr_list_val[count] !=
			    NULL) {
				quorum->disabled_nodes.list_val[count] =
				    (char *)strdup(res_list->disabled_nodes.
				    strarr_list_val[count]);
				if (quorum->disabled_nodes.list_val[count] ==
				    NULL) {
					res = CL_QUERY_ENOMEM;
				}
			} else {
				quorum->disabled_nodes.list_val[count] = NULL;
			}
		}
	}

	/*
	 * Allocate the memory for the enabled list
	 */
	quorum->enabled_nodes.list_len =
	    res_list->enabled_nodes.strarr_list_len;
	if (quorum->enabled_nodes.list_len != 0) {
		quorum->enabled_nodes.list_val = (char **)calloc((size_t)
		    quorum->enabled_nodes.list_len, sizeof (char *));
		if (quorum->enabled_nodes.list_val == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->enabled_nodes.list_val = NULL;
	}

	/*
	 * Store the list of enabled nodes
	 */
	if (quorum->enabled_nodes.list_val != NULL) {
		for (
			count = 0;
			count < quorum->enabled_nodes.list_len;
			count++) {
			if (res_list->enabled_nodes.strarr_list_val[count] !=
			    NULL) {
				quorum->enabled_nodes.list_val[count] =
				    (char *)strdup(res_list->enabled_nodes.
				    strarr_list_val[count]);
				if (quorum->enabled_nodes.list_val[count] ==
				    NULL) {
					res = CL_QUERY_ENOMEM;
				}
			} else {
				quorum->enabled_nodes.list_val[count] = NULL;
			}
		}
	}

	/*
	 * Store the access mode for the quorum
	 */
	if ((res_list->access_mode != NULL) &&
	    (strlen(res_list->access_mode) != 0)) {
		quorum->access_mode = (char *)strdup(res_list->access_mode);
		if (quorum->access_mode == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		quorum->access_mode = NULL;
	}


	return (res);
}


cl_query_error_t
get_device_info(cl_query_device_prop_t res_list, cl_query_device_info_t *dev)
{
	uint_t i = 0;
	cl_query_error_t res = CL_QUERY_OK;



	if ((res_list == NULL) || (dev == NULL)) {
		return (CL_QUERY_EUNEXPECTED);
	}

	/*
	 * Get the logical disk path name
	 */
	if ((res_list->logical_disk_path != NULL) &&
	    (strlen(res_list->logical_disk_path) != 0)) {
		dev->logical_disk_path =
		    (char *)strdup(res_list->logical_disk_path);
		if (dev->logical_disk_path == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		dev->logical_disk_path = NULL;
	}


	/*
	 * Get the file system path for the device
	 */
	if (((res_list->file_system_path).cl_query_didpath_list_t_val
		!= NULL) &&
	    ((res_list->file_system_path).cl_query_didpath_list_t_len != 0)) {
		res = disk_map_prop_dup(&(res_list->file_system_path),
		    &(dev->file_systems_path));

	} else {
		(dev->file_systems_path).list_val = NULL;
		(dev->file_systems_path).list_len = 0;
	}

	/*
	 * Get the device name
	 */
	if ((res_list->device_name != NULL) &&
	    (strlen(res_list->device_name) != 0)) {
		dev->device_name = (char *)strdup(res_list->device_name);
		if (dev->device_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		dev->device_name = NULL;
	}

	/*
	 * Get the primary node name
	 */
	if ((res_list->primary_node_name != NULL) &&
	    (strlen(res_list->primary_node_name) != 0)) {
		dev->primary_node_name =
		    (char *)strdup(res_list->primary_node_name);
		if (dev->primary_node_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		dev->primary_node_name = NULL;
	}

	/*
	 * Get the desired num of secondaries
	 */
	dev->avail_alternate_nodes = res_list->desired_num_secondaries;

	/*
	 * Get the device type
	 */
	if ((res_list->device_type != NULL) &&
	    (strlen(res_list->device_type) != 0)) {
		dev->device_type = (char *)strdup(res_list->device_type);
		if (dev->device_type == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		dev->device_type = NULL;
	}

	/*
	 * Get the replication type
	 */
	if ((res_list->replication_type != NULL) &&
	    (strlen(res_list->replication_type) != 0)) {
		dev->replication_type =
		    (char *)strdup(res_list->replication_type);
		if (dev->replication_type == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		dev->replication_type = NULL;
	}

	/*
	 * Get the failback status
	 */
	dev->failback = res_list->failback;

	/*
	 * Get the ordered node list flag
	 */

	dev->OrderedNodesList = res_list->OrderedNodesList;

	/*
	 * Get the device status
	 */
	dev->status = res_list->status;

	/*
	 * Get the list of spare nodes
	 */
	dev->spare_nodes.list_len = 0;

	if ((res_list->spare_nodes.strarr_list_len == 0) ||
	    (res_list->spare_nodes.strarr_list_val == NULL)) {
		dev->spare_nodes.list_len = 0;
		dev->spare_nodes.list_val = NULL;
	} else {
		/*
		 * Get the list count
		 */
		dev->spare_nodes.list_len =
		    res_list->spare_nodes.strarr_list_len;

		dev->spare_nodes.list_val = (char **)calloc((size_t)
		    dev->spare_nodes.list_len, sizeof (char *));

		if (dev->spare_nodes.list_val == NULL) {
			res = CL_QUERY_ENOMEM;
		} else {
			/*
			 * Get the list of node names
			 */
			for (i = 0; i < res_list->spare_nodes.strarr_list_len;
			    i++) {
				if (res_list->spare_nodes.strarr_list_val[i] !=
				    NULL) {
					dev->spare_nodes.list_val[i] =
					    (char *)strdup(res_list->
					    spare_nodes.strarr_list_val[i]);
					if (dev->spare_nodes.list_val[i] ==
					    NULL) {
						res = CL_QUERY_ENOMEM;
					}
				} else {
					dev->spare_nodes.list_val[i] = NULL;
				}
			}
		}
	}


	/*
	 * Get the list of alternates nodes
	 */
	dev->alternate_nodes.list_len = 0;

	if ((res_list->alternate_nodes.strarr_list_len == 0) ||
	    (res_list->alternate_nodes.strarr_list_val == NULL)) {
		dev->alternate_nodes.list_len = 0;
		dev->alternate_nodes.list_val = NULL;
	} else {
		/*
		 * Get the list count
		 */
		dev->alternate_nodes.list_len =
		    res_list->alternate_nodes.strarr_list_len;

		dev->alternate_nodes.list_val = (char **)calloc((size_t)
		    dev->alternate_nodes.list_len, sizeof (char *));
		if (dev->alternate_nodes.list_val == NULL) {
			res = CL_QUERY_ENOMEM;
		} else {
			i = 0;
			for (; i < res_list->alternate_nodes.strarr_list_len;
			    i++) {
				if (res_list->alternate_nodes.
				    strarr_list_val[i] != NULL) {
					dev->alternate_nodes.list_val[i] =
					    strdup(res_list->alternate_nodes.
					    strarr_list_val[i]);
					if (dev->alternate_nodes.list_val[i] ==
					    NULL) {
						res = CL_QUERY_ENOMEM;
					}
				} else {
					dev->alternate_nodes.list_val[i] =
					    NULL;
				}
			}
		}
	}
	return (res);
}


cl_query_error_t
get_cable_info(cl_query_cable_prop_t res_list, cl_query_cable_info_t *cable)
{
	cl_query_error_t res = CL_QUERY_OK;


	if (res_list == NULL || cable == NULL) {
		return (CL_QUERY_EUNEXPECTED);
	}

	cable->cable_id = res_list->cable_id;
	if ((res_list->cable_name != NULL) &&
	    (strlen(res_list->cable_name) != 0)) {
		cable->cable_name = (char *)strdup(res_list->cable_name);
		if (cable->cable_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		cable->cable_name = NULL;
	}

	cable->cable_epoint1_type = res_list->cable_epoint1_type;
	cable->cable_epoint2_type = res_list->cable_epoint2_type;

	if ((res_list->cable_epoint1_name != NULL) &&
	    (strlen(res_list->cable_epoint1_name) != 0)) {
		cable->cable_epoint1_name =
		    (char *)strdup(res_list->cable_epoint1_name);
		if (cable->cable_epoint1_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		cable->cable_epoint1_name = NULL;
	}

	if ((res_list->cable_epoint2_name != NULL) &&
	    (strlen(res_list->cable_epoint2_name) != 0)) {
		cable->cable_epoint2_name = (char *)strdup(res_list->
		    cable_epoint2_name);
		if (cable->cable_epoint2_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		cable->cable_epoint2_name = NULL;
	}

	cable->state = res_list->state;

	return (res);
}


cl_query_error_t
get_junction_info(cl_query_junction_prop_t res_list,
    cl_query_junction_info_t *junction)
{
	cl_query_error_t res = CL_QUERY_OK;

	junction->junction_id = res_list->junction_id;

	/*
	 * Store the junction name
	 */
	if ((res_list->junction_name != NULL) &&
	    (strlen(res_list->junction_name) != 0)) {
		junction->junction_name =
		    (char *)strdup(res_list->junction_name);
		if (junction->junction_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		junction->junction_name = NULL;
	}

	/*
	 * Store the junction type
	 */
	if ((res_list->junction_type != NULL) &&
	    (strlen(res_list->junction_type) != 0)) {
		junction->junction_type =
		    (char *)strdup(res_list->junction_type);
		if (junction->junction_type == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		junction->junction_type = NULL;
	}

	/*
	 * Store the key name
	 */
	if ((res_list->key_name != NULL) &&
	    (strlen(res_list->key_name) != 0)) {
		junction->key_name = (char *)strdup(res_list->key_name);
		if (junction->key_name == NULL) {
			res = CL_QUERY_ENOMEM;
		}
	} else {
		junction->key_name = NULL;
	}

	/*
	 * Store the port list
	 */
	if ((res_list->port_list.prop_list.prop_list_len > 0) &&
	    ((res_list->port_list.prop_list.prop_list_val != NULL))) {

		if (port_prop_dup(&(res_list->port_list),
			&(junction->port_list))
		    != CL_QUERY_OK) {
			res = CL_QUERY_ENOMEM;
		}


	} else {
		junction->port_list.list_len = 0;
		junction->port_list.list_val = NULL;
	}

	junction->status = res_list->status;

	return (res);
}

cl_query_error_t
get_transportpath_info(
	cl_query_transportpath_prop_t tmp_list,
	    cl_query_transportpath_info_t *tpath) {

	/* RPC may return empty string instead of NULL */

	if (STR_NOT_EMPTY(tmp_list->transportpath_name)) {
		tpath->transportpath_name =
		    (char*)strdup(tmp_list->transportpath_name);
		if (tpath->transportpath_name == NULL) {
			return (CL_QUERY_ENOMEM);
		}
	}
	if (STR_NOT_EMPTY(tmp_list->transportpath_endpoint_1_name)) {
		tpath->transportpath_endpoint_1_name =
		    (char*)strdup(tmp_list->transportpath_endpoint_1_name);
		if (tpath->transportpath_endpoint_1_name == NULL) {
			return (CL_QUERY_ENOMEM);
		}
	}
	if (STR_NOT_EMPTY(tmp_list->transportpath_endpoint_1_strname)) {
		tpath->transportpath_endpoint_1_strname =
		    (char*)strdup(tmp_list->transportpath_endpoint_1_strname);
		if (tpath->transportpath_endpoint_1_strname == NULL) {
			return (CL_QUERY_ENOMEM);
		}
	}
	if (STR_NOT_EMPTY(tmp_list->transportpath_endpoint_2_name)) {
		tpath->transportpath_endpoint_2_name =
		    (char*)strdup(tmp_list->transportpath_endpoint_2_name);
		if (tpath->transportpath_endpoint_2_name == NULL) {
			return (CL_QUERY_ENOMEM);
		}
	}
	if (STR_NOT_EMPTY(tmp_list->transportpath_endpoint_2_strname)) {
		tpath->transportpath_endpoint_2_strname =
		    (char*)strdup(tmp_list->transportpath_endpoint_2_strname);
		if (tpath->transportpath_endpoint_2_strname == NULL) {
			return (CL_QUERY_ENOMEM);
		}
	}

	tpath->status = (cl_query_state_t)tmp_list->status;

	return (CL_QUERY_OK);

}

cl_query_error_t
rpc_to_clquery(enum clnt_stat clnt_error)
{
	switch (clnt_error) {
	case RPC_SUCCESS:
		return (CL_QUERY_OK);

	case RPC_CANTSEND:
	case RPC_CANTRECV:
	case RPC_CANTCONNECT:
	case RPC_XPRTFAILED:
		return (CL_QUERY_ECOMM);

	case RPC_TIMEDOUT:
		return (CL_QUERY_ETIMEDOUT);

	default:
		return (CL_QUERY_EUNEXPECTED);
	}
}
