#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#pragma ident "@(#)Makefile.com 1.6 08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libclquery/Makefile.com
#

LIBRARY= libclccrad.a
VERS= .1

RPC_OBJECTS   =   cl_query_clnt.o cl_query_xdr.o
#clquery_memory.c is located server side (rpc.clquery)
#we need this to be able to free server returned data
COM_OBJECTS   =   clquery.o clquery_common.o clquery_free.o clquery_memory.o clquery_ccr.o


EXTRA_BASE_DIR    = ../../../cmd/rpc.clquery/
EXTRA_CODE_TARGET = $(EXTRA_BASE_DIR)/clquery_memory.c
EXTRA_CODE_SRC    = ../common/clquery_memory.c

OBJECTS = $(COM_OBJECTS) $(RPC_OBJECTS)

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

LINTFILES += $(OBJECTS:%.o=%.ln)

RPCFILE =		$(ROOTCLUSTHDRDIR)/cl_query/rpc/cl_query.x
#we cannot use -A mode, the client/server will work in USER mode
RPCGENFLAGS =		-M -C -K -1

RPCFILE_CLNT =		../common/cl_query_clnt.c 
RPCFILE_XDR =		../common/cl_query_xdr.c
RPCFILES =	$(RPCFILE_CLNT) \
		$(RPCFILE_XDR)  

CLOBBERFILES +=	$(RPCFILES) $(EXTRA_CODE_SRC)
CHECKHDRS = 
CHECK_FILES = $(COM_OBJECTS:%.o=%.c_check) $(CHECKHDRS:%.h=%.h_check)

MAPFILE=	../common/mapfile-vers
SRCS =		$(OBJECTS:%.o=../common/%.c)

LIBS = $(EXTRA_OBJECTS) $(DYNLIB) $(SPROLINTLIB)

$(SPROLINTLIB):= SRCS += ../common/llib-lclccrad

CLEANFILES = $(SPROLINTOUT) $(SPROLINTLIB) $(EXTRA_CODE_SRC)

CPPFLAGS += -D_REENTRANT

$(EUROPA)CPPFLAGS += -DEUROPA

#
# WARNING: Do not include any libraries that cause libthread to be included
#	because one of the consumers of this library cannot be linked with
#	libthread.
#

LDLIBS +=  -lc -lnsl -lthread 
PMAP =	-M $(MAPFILE)

.KEEP_STATE:

.NO_PARALLEL:


all: $(EXTRA_CODE_SRC) $(RPCFILES) $(LIBS) $(SPROLINTLIB)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

$(EXTRA_CODE_SRC):$(EXTRA_CODE_TARGET)
	if [ ! -h $(EXTRA_CODE_SRC) ]; then \
		$(LN) -s $(EXTRA_CODE_TARGET) $(EXTRA_CODE_SRC); \
	fi







