#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.25	08/07/15 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libclcomm/Makefile.com
#
include $(SRC)/Makefile.master

LIBRARYCCC= libclcomm.a
VERS= .1

include $(SRC)/common/cl/Makefile.files

OBJECTS += $(ORB_BUFFERS_UOBJS) $(ORB_DEBUG_UOBJS) $(ORB_FLOW_UOBJS)
OBJECTS += $(ORB_HANDLER_UOBJS) $(ORB_IDL_DATATYPES_UOBJS)
OBJECTS += $(ORB_INFRASTRUCTURE_UOBJS) $(ORB_INVO_UOBJS) $(ORB_MSG_UOBJS)
OBJECTS += $(ORB_OBJECT_UOBJS) $(ORB_REFS_UOBJS) $(ORB_TRANSPORT_UOBJS)
OBJECTS += $(ORB_XDOOR_UOBJS) $(VM_UOBJS) $(CL_COMM_UOBJS)
OBJECTS += $(CL_SCHEMA_UOBJS) $(CL_STUBS_UOBJS) $(CL_SKELS_UOBJS)
OBJECTS += $(SERVICE_UOBJS) $(CMMNS_UOBJS) $(UTIL_UOBJS) $(NSLIB_UOBJS)
OBJECTS += clcomm_cluster_vm.o
$(FAULT_INJECTION)OBJECTS += $(ORB_FAULT_UOBJS)

include ../../Makefile.lib

CLOBBERFILES +=	$(RPCFILES)
CHECKHDRS = $(ORB_BUF_HG) $(ORB_BUF_HGIN) $(ORB_DEB_HU)
CHECKHDRS += $(ORB_FLO_HG) $(ORB_FLO_HGIN)
CHECKHDRS += $(ORB_HAN_HG) $(ORB_HAN_HGIN)
CHECKHDRS += $(ORB_IDL_HG) $(ORB_IDL_HGIN) managed_seq.h managed_seq_in.h
CHECKHDRS += $(ORB_INF_HU) $(ORB_INF_HG) $(ORB_INF_HGIN)
CHECKHDRS += $(ORB_INV_HG) $(ORB_INV_HGIN)
CHECKHDRS += $(ORB_MSG_HG) $(ORB_MSG_HGIN)
CHECKHDRS += $(ORB_OBJ_HG) $(ORB_OBJ_HGIN)
CHECKHDRS += $(ORB_REF_HG) $(ORB_REF_HGIN)
CHECKHDRS += $(ORB_TRA_HG) $(ORB_TRA_HGIN)
CHECKHDRS += $(ORB_XDO_HU) $(ORB_XDO_HG) $(ORB_XDO_HGIN)
CHECKHDRS += $(REPL_SERVICE_HG) $(REPL_SERVICE_HUIN)
CHECKHDRS += $(CMMNS_HG) $(CMMNS_HGIN)
CHECKHDRS += $(NSLIB_HU) $(NSLIB_HUIN)
$(FAULT_INJECTION)CHECKHDRS += $(ORB_FAU_HG) $(ORB_FAU_HGIN)

CHECK_FILES = $(ORB_BUFFERS_UOBJS:%.o=%.c_check) $(ORB_DEBUG_UOBJS:%.o=%.c_check) \
	      $(ORB_FLOW_UOBJS:%.o=%.c_check)
CHECK_FILES += $(ORB_HANDLER_UOBJS:%.o=%.c_check)  \
	       $(ORB_IDL_DATATYPES_UOBJS:%.o=%.c_check)
CHECK_FILES += $(ORB_INFRASTRUCTURE_UOBJS:%.o=%.c_check) \
	       $(ORB_INVO_UOBJS:%.o=%.c_check) $(ORB_MSG_UOBJS:%.o=%.c_check)
CHECK_FILES += $(ORB_OBJECT_UOBJS:%.o=%.c_check) $(ORB_REFS_UOBJS:%.o=%.c_check) \
	       $(ORB_TRANSPORT_UOBJS:%.o=%.c_check) $(CL_COMM_UOBJS)
CHECK_FILES += $(ORB_XDOOR_UOBJS:%.o=%.c_check) $(VM_UOBJS:%.o=%.c_check)
CHECK_FILES += $(SERVICE_UOBJS:%.o=%.c_check) $(CMMNS_UOBJS:%.o=%.c_check) \
	       $(UTIL_UOBJS:%.o=%.c_check) $(NSLIB_UOBJS:%.o=%.c_check)
CHECK_FILES += $(CHECKHDRS:%.h=%.h_check)
$(FAULT_INJECTION)CHECK_FILES += $(ORB_FAULT_UOBJS:%.o=%.c_check)
	       
CC_PICFLAGS = -KPIC

SRCS =		$(OBJECTS:%.o=../common/%.c)

MTFLAG	= -mt
CPPFLAGS += $(CL_CPPFLAGS) -I.
CPPFLAGS += -I$(SRC)/common/cl -I$(SRC)/common/cl/interfaces/$(CLASS)
CPPFLAGS += -I$(REF_PROTO)/usr/src/head
CPPFLAGS += -I$(REF_PROTO)/usr/src/lib/libbrand/common
CPPFLAGS += -I$(REF_PROTO)/usr/src/lib/libuutil/common

RPCX=		rpc_scadmd.x
ROOTRPCXDIR=	$(VROOT)/usr/cluster/include/scadmin/rpc

$(RPCX): $(ROOTRPCXDIR)/$(RPCX)
	$(CP) $(ROOTRPCXDIR)/$(RPCX) .
	$(CHMOD) 664 $(RPCX)

rpc_scadmd.h: $(RPCX)
	$(RPCGEN) -A -C -N -h $(RPCX) >$@

OBH = rpc_scadmd.h

#
# Overrides
#
LIBS = $(DYNLIBCCC) $(LIBRARYCCC)

$(POST_S9_BUILD)LIBZONECFG=$(REF_PROTO)/usr/lib/libzonecfg.so.1
$(POST_S9_BUILD)LIBZONECFG64=$(REF_PROTO)/usr/lib/64/libzonecfg.so.1

# Warning: new dependencies here should be marked in cmd/scrconf/Makefile also
# to build the static command
LDLIBS += -lclos -ldoor -lthread -ldl -lc

CLEANFILES =
LINTFILES += $(OBJECTS:%.o=%.ln)

PMAP =

OBJS_DIR = objs pics

.KEEP_STATE:

.NO_PARALLEL:

all: $(OBH) $(RPCFILES) $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<
