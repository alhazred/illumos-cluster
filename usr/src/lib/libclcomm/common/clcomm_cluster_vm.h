/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CLCOMM_CLUSTER_VM_H
#define	_CLCOMM_CLUSTER_VM_H

#pragma ident	"@(#)clcomm_cluster_vm.h	1.4	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <scadmin/scconf.h>
#include <rpc_scadmd.h>

typedef struct clcomm_vp_version {
	uint16_t major_num;
	uint16_t minor_num;
} clcomm_vp_version_t;

/*
 * clcomm_get_running_version
 *
 * Return running version for given versionned protocol name.
 *
 * Error Status: 0 if call succeeded, >0 if an error occurred.
 *
 *    0 - call succeeded.
 *    1 - invalid vp_name parameter (NULL).
 *    2 - internal failure (ORB initialization, vm query).
 *    3 - vp_name not found.
 *    4 - wrong mode for vp_name (nodepair vp).
 *    5 - too early (no version has been set).
 */
extern int clcomm_get_running_version(char* const vp_name,
	clcomm_vp_version_t* running_version);

/*
 * clcomm_get_cluster_vm_info
 *
 * Query the VM,CMM and Quorum objects, return information
 * about the cluster versions.
 *
 * Error Status:
 *
 *    SCCONF_NOERR       - call succeeded.
 *    SCCONF_ENOMEM      - no more memory.
 *    SCCONF_EUNEXPECTED - internal failure.
 *    SCCONF_ENOCLUSTER  - node not in cluster.
 */
extern scconf_errno_t clcomm_get_cluster_vm_info(sc_result_cluster_vm
	*result);

#ifdef __cplusplus
}
#endif

#endif /* _CLCOMM_CLUSTER_VM_H */
