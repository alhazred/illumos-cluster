/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)clcomm_cluster_vm.cc	1.5	08/05/20 SMI"

/*
 * clcomm_get_cluster_vm_info
 *
 *    Procedure for getting cmm membership, current sequence numbers,
 *    quorum and running version information from the cluster.
 */

#include <h/cmm.h>
#include <h/quorum.h>
#include <cmm/cmm_ns.h>
#include <orb/infrastructure/orb.h>
#include <scadmin/scconf.h>
#include <sys/vm_util.h>
#include <sys/cladm_int.h>
#include <strings.h>
#include <vm/versioned_protocol.h>
#include "rpc_scadmd.h"
#include "clcomm_cluster_vm.h"

extern "C" {

scconf_errno_t convert_sequences(version_manager::node_streams_seq*,
sc_result_cluster_vm*);

/*
 * clcomm_get_cluster_vm_info
 *
 * Query the VM,CMM and Quorum objects, return information
 * about the cluster versions.
 *
 * Error Status:
 *
 *    SCCONF_NOERR       - call succeeded.
 *    SCCONF_ENOMEM      - no more memory.
 *    SCCONF_EUNEXPECTED - internal failure.
 *    SCCONF_ENOCLUSTER  - node not in cluster.
 */
scconf_errno_t
clcomm_get_cluster_vm_info(sc_result_cluster_vm *result)
{
	cmm::control_var		ctrl_v;
	quorum::quorum_algorithm_var	quorum_v;
	cmm::cluster_state_t		cstate;
	quorum::quorum_status_t		*quorum_status;
	Environment			e;

	/* Check that node is in cluster. */
	int flags;
	if (_cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &flags) != 0) {
		return (SCCONF_EUNEXPECTED);
	}
	if (!(flags & CLUSTER_BOOTED)) {
		return (SCCONF_ENOCLUSTER);
	}

	/* Get information from CMM. */
	ctrl_v = cmm_ns::get_control();
	if (CORBA::is_nil(ctrl_v)) {
		return (SCCONF_EUNEXPECTED);
	}
	ctrl_v->get_cluster_state(cstate, e);
	if (e.exception()) {
		e.clear();
		return (SCCONF_EUNEXPECTED);
	}

	/* Fill in required values from cluster state into the result. */
	result->sc_current_seq = (unsigned long)cstate.curr_seqnum;
	result->sc_members.sc_members_len = NODEID_MAX + 1;
	result->sc_members.sc_members_val = new uint32_t [NODEID_MAX + 1];

	if (result->sc_members.sc_members_val == NULL) {
		return (SCCONF_ENOMEM);
	}

	for (int i = 1; i <= NODEID_MAX; i++) {
		result->sc_members.sc_members_val[i] =
			cstate.members.members[i];
	}

	/* Get information about Quorum. */
	quorum_v = cmm_ns::get_quorum_algorithm(NULL);
	if (CORBA::is_nil(quorum_v)) {
		return (SCCONF_EUNEXPECTED);
	}
	quorum_v->quorum_get_status(quorum_status, e);
	if (e.exception()) {
		e.clear();
		return (SCCONF_EUNEXPECTED);
	}
	result->sc_current_votes = quorum_status->current_votes;
	result->sc_votes_needed_for_quorum =
	    quorum_status->votes_needed_for_quorum;

	/* Get information about VM streams. */
	version_manager::vm_admin_var	vm_v;

	if (ORB::initialize()) {
		return (SCCONF_EUNEXPECTED);
	}

	vm_v = vm_util::get_vm();
	if (CORBA::is_nil(vm_v)) {
		return (SCCONF_EUNEXPECTED);
	}

	version_manager::node_streams_seq* streams = NULL;

	bool res_ok;
	res_ok = vm_v->get_streams(streams, e);

	if ((e.exception()) || !res_ok) {
		if (streams != NULL) {
			delete streams;
			streams = NULL;
		}
		if (e.exception()) {
			e.clear();
		}
		return (SCCONF_EUNEXPECTED);
	}

	scconf_errno_t res_err = convert_sequences(streams, result);

	delete streams;
	streams = NULL;

	return (res_err);
}

/*
 * clcomm_get_running_version
 *
 * Return running version for given versionned protocol name.
 *
 * Error Status: 0 if call succeeded, >0 if an error occurred.
 *
 *    0 - call succeeded.
 *    1 - invalid vp_name parameter (NULL).
 *    2 - internal failure (ORB initialization, vm query).
 *    3 - vp_name not found.
 *    4 - wrong mode for vp_name (nodepair vp).
 *    5 - too early (no version has been set).
 */
int
clcomm_get_running_version(char* const vp_name,
    clcomm_vp_version_t* running_version)
{

	if ((vp_name == NULL) || (running_version == NULL)) {
		return (1);
	}

	version_manager::vm_admin_var vm_v;

	if (ORB::initialize()) {
		return (2);
	}

	vm_v = vm_util::get_vm();
	if (CORBA::is_nil(vm_v)) {
		return (2);
	}

	Environment e;
	version_manager::vp_version_t rvers;
	vm_v->get_running_version(vp_name, rvers, e);

	if (e.exception()) {

		if (version_manager::not_found::_exnarrow(e.exception())) {
			e.clear();
			return (3);
		}
		if (version_manager::wrong_mode::_exnarrow(e.exception())) {
			e.clear();
			return (4);
		}
		if (version_manager::too_early::_exnarrow(e.exception())) {
			e.clear();
			return (5);
		}
		e.clear();
		return (2);
	}

	running_version->major_num = rvers.major_num;
	running_version->minor_num = rvers.minor_num;
	return (0);
}

scconf_errno_t
convert_sequences(version_manager::node_streams_seq* streams,
    sc_result_cluster_vm* result)
{
	result->sc_nodes_table.sc_nodes_table_val =
		new sc_node_streams[NODEID_MAX + 1];

	if (result->sc_nodes_table.sc_nodes_table_val == NULL) {
		return (SCCONF_ENOMEM);
	}

	result->sc_nodes_table.sc_nodes_table_len = NODEID_MAX + 1;

	version_manager::node_streams_seq &archive = *streams;

	// Custom version ranges are stored in sequence 0.
	for (uint32_t i = 0; i <= NODEID_MAX; ++i) {

		if (archive[i].length() != 0) {

			result->sc_nodes_table.sc_nodes_table_val[i].
				sc_streams_by_type.sc_streams_by_type_val =
				new sc_vm_stream[VP_TYPE_ARRAY_SIZE];

			if (result->sc_nodes_table.sc_nodes_table_val[i].
			    sc_streams_by_type.sc_streams_by_type_val ==
			    NULL) {
				return (SCCONF_ENOMEM);
			}

			result->sc_nodes_table.sc_nodes_table_val[i].
				sc_streams_by_type.sc_streams_by_type_len =
				VP_TYPE_ARRAY_SIZE;

			for (uint32_t j = 0; j < VP_TYPE_ARRAY_SIZE; j++) {

				if (archive[i][j].length() != 0) {

					uint32_t size = archive[i][j].length();

					char *buffer = new char[size];
					if (buffer == NULL) {
						return (SCCONF_ENOMEM);
					}

					bcopy(
					    archive[i][j].buf(),
					    buffer,
					    size); //lint !e668

					result->sc_nodes_table.
						sc_nodes_table_val[i].
						sc_streams_by_type.
						sc_streams_by_type_val[j].
						data.data_val = buffer;

					result->sc_nodes_table.
						sc_nodes_table_val[i].
						sc_streams_by_type.
						sc_streams_by_type_val[j].
						data.data_len = size;

				} else {

					result->sc_nodes_table.
						sc_nodes_table_val[i].
						sc_streams_by_type.
						sc_streams_by_type_val[j].
						data.data_val = NULL;

					result->sc_nodes_table.
						sc_nodes_table_val[i].
						sc_streams_by_type.
						sc_streams_by_type_val[j].
						data.data_len = 0;
				}
			}
		} else {

			result->sc_nodes_table.sc_nodes_table_val[i].
				sc_streams_by_type.sc_streams_by_type_val
				= NULL;

			result->sc_nodes_table.sc_nodes_table_val[i].
				sc_streams_by_type.sc_streams_by_type_len
				= 0;
		}
	}

	return (SCCONF_NOERR);
}

} // extern C
