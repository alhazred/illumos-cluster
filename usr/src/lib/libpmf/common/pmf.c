/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmf.c	1.50	09/02/27 SMI"

/*
 * pmflib(3c)
 * RPC client interface to HA Process Monitor Facility
 */
#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#ifndef linux
#include <tzfile.h>
#include <rpc/xdr.h>
#endif
#include <rpc/rpc.h>
#include <pwd.h>
#include <locale.h>
#include <netdb.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm/security.h>

#if DOOR_IMPL
#include <rgm/pmf.h>
#include "rgm/door/pmf_door.h"
#else
#include <rgm/rpc/pmf.h>
#endif
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#ifdef DEBUG
static int debug = 1;
#else
static int debug = 0;
#endif

#define		NOGET(s)	(s)

/*
 * Pool of arguments to pass into pmf_rpc_command(), mostly used by
 * the start routine.
 */
struct pmf_cmd_args {
	char *host;
	pmf_cmd_type type;
	char *identifier;
	int period_modified;
	int period;
	int retries_modified;
	int retries;
	pmf_result result;
	pmf_status_result status;
	char *action;
	PMF_ACTIONS action_type;
	char *pwd;
	char *path;
	int timewait;
	int signal;
	SEC_TYPE	sec_type;
	int flags;
	int num_env;
	char **env;
	PMF_MONITOR_CHILDREN monitor_children;
	int	monitor_level;
	bool_t	env_passed;
	char *project_name;
};
typedef struct pmf_cmd_args pmf_cmd_args;

static int pmf_rpc_command(pmf_cmd_args *pmfcmd, int argc, char *argv[]);
static char *getlocalhostname(void);

#if DOOR_IMPL
static void set_error(pmf_cmd_args *pmfcmd, char *action,
    int ret);
#else
static void set_error(CLIENT *clnt, pmf_cmd_args *pmfcmd, char *action,
    enum clnt_stat ret);
#endif

int
pmf_start(char *id, pmf_cmd *cmd, int argc, char **argv, pmf_result *res)
{
	pmf_cmd_args pmfcmd = {0};
	char pathenv[MAXPATHLEN];
	char pwd[MAXPATHLEN];
	int err;
	int i = 0;
	int j = 0;
	int c;
	pmfcmd.type = PMF_START;

	pmfcmd.sec_type = cmd->security;

	pmfcmd.flags = cmd->flags;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/*
	 * Send our current directory and PATH
	 */
	if (getcwd(pwd, (size_t)MAXPATHLEN) == NULL) {
		(void) fprintf(stderr,
		    (const char *)dgettext(TEXT_DOMAIN,
		    "getcwd failed\n"));
		return (-1);
	}
	pmfcmd.pwd = pwd;
	if ((pmfcmd.path = getenv("PATH")) != 0) {
		(void) sprintf(pathenv, "PATH=%s", pmfcmd.path);
		pmfcmd.path = pathenv;
	}

	pmfcmd.identifier = id;


	while (pmfcmd.identifier[j]) {
		c = pmfcmd.identifier[j];
		if (isspace(c)) {
			fprintf(stderr,
			    dgettext(TEXT_DOMAIN,
			    "Tag-name with"
			    " spaces is not allowed.\n"));
			return (-1);
		}
		j++;
	}


	/*
	 * Check for extra args ... default to none.
	 */
	if (cmd) {
		/*
		 * We default to the local host name.  Authentication and
		 * authorization are managed by the daemon.
		 */
		if (cmd->host)
			pmfcmd.host = cmd->host;
		else {
			pmfcmd.host = getlocalhostname();
			if (pmfcmd.host == NULL)
				return (-1);
		}

		pmfcmd.retries = cmd->retries;

		if (cmd->period != -1)
			pmfcmd.period = cmd->period * SECSPERMIN;
		else
			pmfcmd.period = cmd->period;

		if (cmd->action) {
			pmfcmd.action_type = PMFACTION_EXEC;
			pmfcmd.action = cmd->action;
		} else {
			pmfcmd.action_type = PMFACTION_NULL;
			pmfcmd.action = NULL;
		}

		if (cmd->env) {
			/*
			 * We do not send the NULL which could
			 * be at the end of the list.
			 * It will always be added by the server.
			 */
			for (i = 0; i < cmd->num_env; i++) {
				if (cmd->env[i] == NULL) {
					break;
				}
			}

			pmfcmd.env = cmd->env;
			pmfcmd.num_env = i;
		}

		pmfcmd.monitor_children = cmd->monitor_children;
		pmfcmd.monitor_level = cmd->monitor_level;
		pmfcmd.env_passed = cmd->env_passed;
		pmfcmd.project_name = cmd->project_name;
	}

	err = pmf_rpc_command(&pmfcmd, argc, argv);

	if (res)
		*res = pmfcmd.result;

	return (err);
}

/*
 * pmf_status(): Get status information on a tag.
 *
 * IMPORTANT: Caller is responsible for free()ing
 * memory allocated in the status argument.
 * Call free_rpc_args() on status to free all memory
 * allocated as a result of this call.
 */

int
pmf_status(char *id, pmf_cmd *cmd, pmf_status_result *status)
{
	pmf_cmd_args pmfcmd = {0};
	int err;

	pmfcmd.type = PMF_STATUS;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (cmd) {
		pmfcmd.sec_type = cmd->security;

		/*
		 * We default to the local host name.  Authentication and
		 * authorization are managed by the daemon.
		 */
		if (cmd->host)
			pmfcmd.host = cmd->host;
		else {
			pmfcmd.host = getlocalhostname();
			if (pmfcmd.host == NULL)
				return (-1);
		}
	}

	pmfcmd.identifier = id;

	err = pmf_rpc_command(&pmfcmd, 0, NULL);

	if (status)
		*status = pmfcmd.status;

	return (err);
}

/*
 * pmf_status_all(): Get a list of all tags.
 *
 * IMPORTANT: Caller is responsible for free()ing
 * memory allocated in the status argument.
 * Call free_rpc_args() on status to free all memory
 * allocated as a result of this call.
 */

int
pmf_status_all(pmf_cmd *cmd, pmf_status_result *status)
{
	pmf_cmd_args pmfcmd = {0};
	int err;

	pmfcmd.type = PMF_STATUS_ALL;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (cmd) {
		pmfcmd.sec_type = cmd->security;

		/*
		 * We default to the local host name.  Authentication and
		 * authorization are managed by the daemon.
		 */
		if (cmd->host)
			pmfcmd.host = cmd->host;
		else {
			pmfcmd.host = getlocalhostname();
			if (pmfcmd.host == NULL)
				return (-1);
		}
	}

	pmfcmd.identifier = NULL;

	err = pmf_rpc_command(&pmfcmd, 0, NULL);

	if (status)
		*status = pmfcmd.status;

	return (err);
}

int
pmf_modify(char *id, pmf_cmd *cmd, pmf_result *res)
{
	pmf_cmd_args pmfcmd = {0};
	int err;

	pmfcmd.type = PMF_MODIFY;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (cmd) {
		/*
		 * We default to the local host name.  Authentication and
		 * authorization are managed by the daemon.
		 */
		if (cmd->host)
			pmfcmd.host = cmd->host;
		else {
			pmfcmd.host = getlocalhostname();
			if (pmfcmd.host == NULL)
				return (-1);
		}
		pmfcmd.sec_type = cmd->security;
		pmfcmd.retries_modified = cmd->retries_modified;
		pmfcmd.retries = cmd->retries;
		pmfcmd.period_modified = cmd->period_modified;
		if (cmd->period != -1)
			pmfcmd.period = cmd->period * SECSPERMIN;
		else
			pmfcmd.period = cmd->period;
	}

	pmfcmd.identifier = id;

	err = pmf_rpc_command(&pmfcmd, 0, NULL);

	if (res)
		*res = pmfcmd.result;

	return (err);
}

int
pmf_stop(char *id, pmf_cmd *cmd, pmf_result *res)
{
	pmf_cmd_args pmfcmd = {0};
	int err;

	pmfcmd.type = PMF_STOP;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (cmd) {
		/*
		 * We default to the local host name.  Authentication and
		 * authorization are managed by the daemon.
		 */
		if (cmd->host)
			pmfcmd.host = cmd->host;
		else {
			pmfcmd.host = getlocalhostname();
			if (pmfcmd.host == NULL)
				return (-1);
		}
		pmfcmd.sec_type = cmd->security;
		pmfcmd.timewait = cmd->timewait;
		pmfcmd.signal = cmd->signal;
	}

	pmfcmd.identifier = id;

	err = pmf_rpc_command(&pmfcmd, 0, NULL);

	if (res)
		*res = pmfcmd.result;

	return (err);
}

int
pmf_kill(char *id, pmf_cmd *cmd, pmf_result *res)
{
	pmf_cmd_args pmfcmd = {0};
	int err = 0;

	pmfcmd.type = PMF_KILL;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (cmd) {
		/*
		 * We default to the local host name.  Authentication and
		 * authorization are managed by the daemon.
		 */
		if (cmd->host)
			pmfcmd.host = cmd->host;
		else {
			pmfcmd.host = getlocalhostname();
			if (pmfcmd.host == NULL)
				return (-1);
		}
		pmfcmd.sec_type = cmd->security;
		pmfcmd.timewait = cmd->timewait;
		pmfcmd.signal = cmd->signal;
	}

	pmfcmd.identifier = id;

	if (res)
		err = pmf_rpc_command(&pmfcmd, 0, NULL);

	*res = pmfcmd.result;

	return (err);
}

int
pmf_suspend(char *id, pmf_cmd *cmd, pmf_result *res)
{
	pmf_cmd_args pmfcmd = {0};
	int err = 0;

	pmfcmd.type = PMF_SUSPEND;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (cmd) {
		/*
		 * We default to the local host name.  Authentication and
		 * authorization are managed by the daemon.
		 */
		if (cmd->host)
			pmfcmd.host = cmd->host;
		else {
			pmfcmd.host = getlocalhostname();
			if (pmfcmd.host == NULL)
				return (-1);
		}
		pmfcmd.sec_type = cmd->security;
	}

	pmfcmd.identifier = id;

	if (res)
		err = pmf_rpc_command(&pmfcmd, 0, NULL);

	*res = pmfcmd.result;

	return (err);
}

int
pmf_resume(char *id, pmf_cmd *cmd, pmf_result *res)
{
	pmf_cmd_args pmfcmd = {0};
	int err = 0;

	pmfcmd.type = PMF_RESUME;

	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if (cmd) {
		/*
		 * We default to the local host name.  Authentication and
		 * authorization are managed by the daemon.
		 */
		if (cmd->host)
			pmfcmd.host = cmd->host;
		else {
			pmfcmd.host = getlocalhostname();
			if (pmfcmd.host == NULL)
				return (-1);
		}
		pmfcmd.sec_type = cmd->security;
	}

	pmfcmd.identifier = id;

	if (res)
		err = pmf_rpc_command(&pmfcmd, 0, NULL);

	*res = pmfcmd.result;

	return (err);
}

static int
pmf_rpc_command(pmf_cmd_args *pmfcmd, int argc, char *argv[])
{
#if DOOR_IMPL
#if SOL_VERSION >= __s10
	char zonename[ZONENAME_MAX];
#else
	char *zonename = NULL;
#endif
	client_handle *clnt;
#else
	CLIENT *clnt;
#endif
	pmf_start_args	pmfproc_start_2_arg;
	pmf_modify_args	 pmfproc_modify_2_arg;
	pmf_args pmfproc_status_2_arg;
	pmf_args pmfproc_stop_2_arg;
	pmf_args pmfproc_kill_2_arg;
	pmf_args pmfproc_suspend_2_arg;
	pmf_args pmfproc_resume_2_arg;
	enum clnt_stat ret;
	char *clsp = NULL;
	sc_syslog_msg_handle_t handle;

	switch (pmfcmd->type) {
	case PMF_START:
		(void) memset(&pmfproc_start_2_arg, 0,
		    sizeof (pmf_start_args));
		pmfproc_start_2_arg.identifier = pmfcmd->identifier;
		pmfproc_start_2_arg.retries = pmfcmd->retries;
		pmfproc_start_2_arg.period = pmfcmd->period;
		pmfproc_start_2_arg.action_type = pmfcmd->action_type;
		pmfproc_start_2_arg.cmd.cmd_val = argv;
		pmfproc_start_2_arg.cmd.cmd_len = (uint_t)argc + 1;
		pmfproc_start_2_arg.env.env_val = pmfcmd->env;
		/* num_env does not include the last NULL element */
		pmfproc_start_2_arg.env.env_len =
			(uint_t)(pmfcmd->num_env);
		pmfproc_start_2_arg.pwd = pmfcmd->pwd;
		pmfproc_start_2_arg.path = pmfcmd->path;
		pmfproc_start_2_arg.action = pmfcmd->action;
		pmfproc_start_2_arg.flags = pmfcmd->flags;
		pmfproc_start_2_arg.monitor_children =
			pmfcmd->monitor_children;
		pmfproc_start_2_arg.monitor_level =
			pmfcmd->monitor_level;
		pmfproc_start_2_arg.env_passed = pmfcmd->env_passed;
		pmfproc_start_2_arg.project_name = pmfcmd->project_name;
		break;
	case PMF_MODIFY:
		(void) memset(&pmfproc_modify_2_arg, 0,
		    sizeof (pmf_modify_args));
		pmfproc_modify_2_arg.identifier = pmfcmd->identifier;
		pmfproc_modify_2_arg.retries_modified =
			pmfcmd->retries_modified;
		pmfproc_modify_2_arg.retries = pmfcmd->retries;
		pmfproc_modify_2_arg.period_modified =
			pmfcmd->period_modified;
		pmfproc_modify_2_arg.period = pmfcmd->period;
		break;
	case PMF_STATUS:
		(void) memset(&pmfproc_status_2_arg, 0,
		    sizeof (pmf_args));
		pmfproc_status_2_arg.identifier = pmfcmd->identifier;
		break;
	case PMF_STATUS_ALL:
		(void) memset(&pmfproc_status_2_arg, 0,
		    sizeof (pmf_args));
		pmfproc_status_2_arg.identifier = NULL;
		pmfproc_status_2_arg.query_all = TRUE;
		break;
	case PMF_STOP:
		(void) memset(&pmfproc_stop_2_arg, 0,
		    sizeof (pmf_args));
		pmfproc_stop_2_arg.identifier = pmfcmd->identifier;
		pmfproc_stop_2_arg.signal = pmfcmd->signal;
		pmfproc_stop_2_arg.timewait = pmfcmd->timewait;
		break;
	case PMF_KILL:
		(void) memset(&pmfproc_kill_2_arg, 0,
		    sizeof (pmf_args));
		pmfproc_kill_2_arg.identifier = pmfcmd->identifier;
		pmfproc_kill_2_arg.signal = pmfcmd->signal;
		pmfproc_kill_2_arg.timewait = pmfcmd->timewait;
		break;
	case PMF_SUSPEND:
		(void) memset(&pmfproc_suspend_2_arg, 0,
		    sizeof (pmf_args));
		pmfproc_suspend_2_arg.identifier = pmfcmd->identifier;
		break;
	case PMF_RESUME:
		(void) memset(&pmfproc_resume_2_arg, 0,
		    sizeof (pmf_args));
		pmfproc_resume_2_arg.identifier = pmfcmd->identifier;
		break;
	case PMF_NULL:
	case PMF_STAT:
	default:
		return (-1);
	}

	/*
	 * connect to server
	 */
#if DOOR_IMPL
#if SOL_VERSION >= __s10
	getzonenamebyid(getzoneid(), zonename, ZONENAME_MAX);
#endif
	if (!security_clnt_connect(&clnt, RGM_PMF_PROGRAM_CODE,
	    pmfcmd->sec_type, debug, 1, zonename)) {
#else
	if (!security_clnt_connect(&clnt, pmfcmd->host, PMF_PROGRAM_NAME,
	    (ulong_t)PMF_PROGRAM, (ulong_t)PMF_VERSION, pmfcmd->sec_type,
	    debug, 1)) {
#endif
		pmfcmd->result.code.type = PMF_CONNECT;
		pmfcmd->status.code.type = PMF_CONNECT;
		/*
		 * cf_stat is set by the system, like errno.
		 * Supress the lint info, because lint doesn't understand
		 * it.
		 */
		pmfcmd->result.code.sys_errno =
			rpc_createerr.cf_stat; /*lint !e746 */
		pmfcmd->status.code.sys_errno = rpc_createerr.cf_stat;
		clsp = clnt_spcreateerror(NOGET("security_clnt_connect"));
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		if (clsp != NULL) {
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "host %s failed: %s\n"),
			    pmfcmd->host, clsp);
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "host %s failed: %s", pmfcmd->host, clsp);
		} else {
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "host %s failed, and "
			    "clnt_spcreateerror returned NULL\n"),
			    pmfcmd->host);
			/*
			 * SCMSGS
			 * @explanation
			 * The rgm is not able to establish an rpc connection
			 * to the rpc.fed server on the host shown, and the
			 * rpc error could not be read. An error message is
			 * output to syslog.
			 * @user_action
			 * Save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "host %s failed, and "
			    "clnt_spcreateerror returned NULL",
			    pmfcmd->host);
		}
		sc_syslog_msg_done(&handle);

		return (-2);
	}

#if DOOR_IMPL
	if (clnt == (client_handle *) NULL) {
#else
	if (clnt == (CLIENT *) NULL) {
#endif
		(void) fprintf(stderr,
		    (const char *)dgettext(TEXT_DOMAIN,
		    "host %s: client is null\n"),
		    pmfcmd->host);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "host %s: client is null",
		    pmfcmd->host);
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	switch (pmfcmd->type) {
	case PMF_START:
#if DOOR_IMPL
		ret = pmf_door_clnt(&pmfproc_start_2_arg,
		    &pmfcmd->result, PMFPROC_START, clnt);
		if (ret != DOOR_CALL_SUCCESS) {
			set_error(pmfcmd, NOGET("PMF_START"), ret);
		}
#else
		ret = pmfproc_start_2(&pmfproc_start_2_arg,
		    &pmfcmd->result, clnt);
		if (ret != RPC_SUCCESS)
			set_error(clnt, pmfcmd, NOGET("PMF_START"),
			    ret);
#endif
		break;
	case PMF_MODIFY:
#if DOOR_IMPL
		ret = pmf_door_clnt(&pmfproc_modify_2_arg,
		    &pmfcmd->result, PMFPROC_MODIFY, clnt);
		if (ret != DOOR_CALL_SUCCESS) {
			set_error(pmfcmd, NOGET("PMF_MODIFY"), ret);
		}
#else
		ret = pmfproc_modify_2(&pmfproc_modify_2_arg,
		    &pmfcmd->result, clnt);
		if (ret != RPC_SUCCESS)
			set_error(clnt, pmfcmd, NOGET("PMF_MODIFY"),
			    ret);
#endif
		break;
	case PMF_STATUS:
	case PMF_STATUS_ALL:
#if DOOR_IMPL
		ret = pmf_door_clnt(&pmfproc_status_2_arg,
		    &pmfcmd->status, PMFPROC_STATUS, clnt);
		if (ret != DOOR_CALL_SUCCESS) {
			set_error(pmfcmd, NOGET("PMF_STATUS"), ret);
#else
		ret = pmfproc_status_2(&pmfproc_status_2_arg,
		    &pmfcmd->status, clnt);
		if (ret != RPC_SUCCESS) {
			set_error(clnt, pmfcmd, NOGET("PMF_STATUS"),
			    ret);
#endif
			pmfcmd->result.code.sec_errno = SEC_EUNIXW;
			pmfcmd->status.code.type =
				pmfcmd->result.code.type;
			pmfcmd->status.code.sec_errno =
				pmfcmd->result.code.sec_errno;
		} else {
			pmfcmd->result.code.type =
				pmfcmd->status.code.type;
			pmfcmd->result.code.sys_errno =
				pmfcmd->status.code.sys_errno;
		}
		break;
	case PMF_STOP:
#if DOOR_IMPL
		ret = pmf_door_clnt(&pmfproc_stop_2_arg,
		    &pmfcmd->result, PMFPROC_STOP, clnt);
		if (ret != DOOR_CALL_SUCCESS) {
			set_error(pmfcmd, NOGET("PMF_STOP"), ret);
		}
#else
		ret = pmfproc_stop_2(&pmfproc_stop_2_arg,
		    &pmfcmd->result, clnt);
		if (ret != RPC_SUCCESS)
			set_error(clnt, pmfcmd, NOGET("PMF_STOP"),
			    ret);
#endif
		break;
	case PMF_KILL:
#if DOOR_IMPL
		ret = pmf_door_clnt(&pmfproc_kill_2_arg,
		    &pmfcmd->result, PMFPROC_KILL, clnt);
		if (ret != DOOR_CALL_SUCCESS) {
			set_error(pmfcmd, NOGET("PMF_KILL"), ret);
		}
#else
		ret = pmfproc_kill_2(&pmfproc_kill_2_arg,
		    &pmfcmd->result, clnt);
		if (ret != RPC_SUCCESS)
			set_error(clnt, pmfcmd, NOGET("PMF_KILL"),
			    ret);
#endif
		break;
	case PMF_SUSPEND:
#if DOOR_IMPL
		ret = pmf_door_clnt(&pmfproc_suspend_2_arg,
		    &pmfcmd->result, PMFPROC_SUSPEND, clnt);
		if (ret != DOOR_CALL_SUCCESS) {
			set_error(pmfcmd, NOGET("PMF_SUSPEND"), ret);
		}
#else
		ret = pmfproc_suspend_2(&pmfproc_suspend_2_arg,
		    &pmfcmd->result, clnt);
		if (ret != RPC_SUCCESS)
			set_error(clnt, pmfcmd, NOGET("PMF_SUSPEND"),
			    ret);
#endif
		break;
	case PMF_RESUME:
#if DOOR_IMPL
		ret = pmf_door_clnt(&pmfproc_resume_2_arg,
		    &pmfcmd->result, PMFPROC_RESUME, clnt);
		if (ret != DOOR_CALL_SUCCESS) {
			set_error(pmfcmd, NOGET("PMF_RESUME"), ret);
		}
#else
		ret = pmfproc_resume_2(&pmfproc_resume_2_arg,
		    &pmfcmd->result, clnt);
		if (ret != RPC_SUCCESS)
			set_error(clnt, pmfcmd, NOGET("PMF_RESUME"),
			    ret);
#endif
		break;
	case PMF_STAT:
	case PMF_NULL:
	default:
		pmfcmd->result.code.type = PMF_SYSERRNO;
		pmfcmd->result.code.sys_errno = ENOENT;
		(void) fprintf(stderr,
		    (const char *) dgettext(TEXT_DOMAIN,
		    "Internal: Unknown command type (%d)\n"),
		    pmfcmd->type);
		security_clnt_freebinding(clnt);
		return (-1);
	}

	/*
	 * after client call, release client handle
	 */
	security_clnt_freebinding(clnt);
#if DOOR_IMPL
	if (ret != DOOR_CALL_SUCCESS)
		return (-2);
#else
	if (ret != RPC_SUCCESS)
		return (-2);
#endif

	return (0);
}


/*
 * Returns the physical hostname of the local node.
 * If the call to gethostname() fails it returns null.
 */
static char *
getlocalhostname(void)
{
	static char Hostname[MAXHOSTNAMELEN];
	/*
	 * Suppress lint warning about "union initialization"
	 * DEFAULTMUTEX is defined in /usr/include/synch.h
	 */
	static mutex_t	host_name_mutex = DEFAULTMUTEX; /*lint !e708 */
	static bool_t	first_time = TRUE;

	/*
	 * get lock to ensure that only 1 thread executes this section
	 */
	(void) mutex_lock(&host_name_mutex);

	/*
	 * get host name;
	 * only do it the first time, then cache it.
	 */
	if (first_time) {
		first_time = FALSE;

		/*
		 * set to null, in case the call below fails and we return
		 * imemdiately; next time the function directly returns
		 * Hostname, so it should be set to NULL.
		 */
		(void) memset(Hostname, 0, (size_t)MAXHOSTNAMELEN);
		if (gethostname(Hostname, MAXHOSTNAMELEN) == -1) {
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "gethostname failed\n"));
			(void) mutex_unlock(&host_name_mutex);
			return (NULL);
		}
	}

	/*
	 * release lock
	 */
	(void) mutex_unlock(&host_name_mutex);

	return (Hostname);
}


#if DOOR_IMPL
static void
set_error(pmf_cmd_args *pmfcmd, char *action,
    int ret)
{
	sc_syslog_msg_handle_t handle;

	pmfcmd->result.code.type = PMF_ACCESS;
	pmfcmd->result.code.sec_errno = SEC_EUNIXS;

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_PMF_PMFD_TAG, "");
	(void) fprintf(stderr,
	    (const char *)dgettext(TEXT_DOMAIN,
	    "%s: Call failed, return code=%d\n"), action, ret);
	(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
	    "%s: Call failed, return code=%d", action, ret);
	sc_syslog_msg_done(&handle);
}
#else
static void
set_error(CLIENT *clnt, pmf_cmd_args *pmfcmd, char *action,
    enum clnt_stat ret)
{
	char *clsp = NULL;
	sc_syslog_msg_handle_t handle;

	pmfcmd->result.code.type = PMF_ACCESS;
	pmfcmd->result.code.sec_errno = SEC_EUNIXS;

	clsp = clnt_sperror(clnt, action);
	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_PMF_PMFD_TAG, "");
	if (clsp != NULL) {
		(void) fprintf(stderr,
		    (const char *)dgettext(TEXT_DOMAIN,
		    "Call failed: %s\n"), clsp);
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Call failed: %s", clsp);
	} else {
		(void) fprintf(stderr,
		    (const char *)dgettext(TEXT_DOMAIN,
		    "%s: Call failed, return code=%d\n"), action, (int)ret);
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "%s: Call failed, return code=%d", action, (int)ret);
	}
	sc_syslog_msg_done(&handle);

}
#endif

/*
 * free up rpc args malloc'ed on the server (file pmfd_util.c)
 */
void
free_rpc_args(pmf_status_result *result)
{
	uint_t i;

	if (!result)
		return;
	if (result->upids)
		xdr_free((xdrproc_t)xdr_string, (char *)&(result->upids));
	if (result->identifier)
		xdr_free((xdrproc_t)xdr_string, (char *)&(result->identifier));
	if (result->action)
		xdr_free((xdrproc_t)xdr_string, (char *)&(result->action));
	if (result->pids)
		xdr_free((xdrproc_t)xdr_string, (char *)&(result->pids));

	if ((result->cmd.cmd_len != 0) && (result->cmd.cmd_val != NULL)) {
		for (i = 0; i < result->cmd.cmd_len; i++) {
			if (result->cmd.cmd_val[i])
				xdr_free((xdrproc_t)xdr_string,
				    (char *)&(result->cmd.cmd_val[i]));
		}
		if (result->cmd.cmd_val)
			xdr_free((xdrproc_t)xdr_string,
			    (char *)&(result->cmd.cmd_val));
	}

	if ((result->env.env_len != 0) && (result->env.env_val != NULL)) {
		for (i = 0; i < result->env.env_len; i++) {
			if (result->env.env_val[i])
				xdr_free((xdrproc_t)xdr_string,
				    (char *)&(result->env.env_val[i]));
		}
		if (result->env.env_val)
			xdr_free((xdrproc_t)xdr_string,
			    (char *)&(result->env.env_val));
	}

	if (result->project_name)
		xdr_free((xdrproc_t)xdr_string,
		    (char *)&(result->project_name));
}

#if DOOR_IMPL
int pmf_door_clnt(void *args, void *result,
    int pmf_api_name, client_handle *clnt)
{
	door_arg_t door_arg;

	/* XDR Streams for arguments and result */
	XDR xdrs, xdrs_result;

	/*
	 * Stores the marshalled arguments to be sent via
	 * door_call
	 */
	char *arg_buf = NULL;

	/*
	 * Container struct for storing the input arguments
	 * that are to be sent to the server via the door_call
	 */
	pmf_input_args_t *pmf_args_p = NULL;

	/* Stores the total size of the arguments being passed */
	size_t arg_size;

	/* Return code sent by the door server */
	int return_code;

	/* Number of times we want to try to encode the arguments */
	int num_retries = XDR_NUM_RETRIES;

	sc_syslog_msg_handle_t handle;

	/* Check for a valid client_handle before proceeding */
	if (clnt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Invalid Client Handle");
		sc_syslog_msg_done(&handle);
		return (DOOR_INVALID_HANDLE);
	}
	/* Allocate memory for storing the arguments to be passed */
	/*
	 * Suppress unexplainable Flexelint error
	 *    Info(747) [c:47]: Significant prototype coercion (arg. no. 1)
	 *    int to unsigned long
	 */
	pmf_args_p = (pmf_input_args_t *)calloc(1,	/*lint !e747 */
	    sizeof (pmf_input_args_t));

	/* Prepare the container structure */
	pmf_args_p->api_name = pmf_api_name;
	pmf_args_p->data = args;

	/* Calculate the size of the arguments being passed */
	arg_size = pmf_xdr_sizeof(pmf_args_p);

	for (num_retries = XDR_NUM_RETRIES; num_retries > 0; num_retries--) {
		/* Allocate the space to store marshalled data */
		/*
		 * Suppress unexplainable Flexelint error
		 *    Info(747) [c:47]: Significant prototype coercion
		 *    (arg. no. 1) int to unsigned long
		 */
		arg_buf = (char *)calloc(1, arg_size);	/*lint !e747 */

		/* Initialize the XDR Stream for Encoding data */
		xdrmem_create(&xdrs, arg_buf, (uint_t)arg_size, XDR_ENCODE);

		/* Use xdr function to marshall the input arguments */
		if (!xdr_pmf_input_args(&xdrs, pmf_args_p)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * A non-fatal error occurred while rpc.pmfd was
			 * marshalling arguments for a remote procedure call.
			 * The operation will be re-tried with a larger
			 * buffer.
			 * @user_action
			 * No user action is required. If the message recurs
			 * frequently, contact your authorized Sun service
			 * provider to determine whether a workaround or patch
			 * is available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "PMF XDR Buffer Shortfall while encoding "
			    "arguments API num = %d, will retry", pmf_api_name);
			sc_syslog_msg_done(&handle);
			/*
			 * Will retry to encode the arguments
			 * this time with a larger buffer
			 */
			if (num_retries) {
				arg_size += EXTRA_BUFFER_SIZE;
				free(arg_buf);
				arg_buf = NULL;
				continue;
			}
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "XDR Error while encoding arguments.");
			sc_syslog_msg_done(&handle);
			return (XDR_ERROR_AT_CLIENT);
		} else {
			break;
		}
	}

	/* Prepare the door arguments before making the door call */
	door_arg.data_ptr = arg_buf;
	door_arg.data_size = xdr_getpos(&xdrs);
	door_arg.desc_ptr = NULL;
	door_arg.desc_num = 0;
	door_arg.rbuf = NULL;

	/*
	 * Even if we set this as null the door_return
	 * will set it to the correct size when the
	 * door server returns
	 */
	door_arg.rsize = 0;

	/* Make the door call */
	if (make_door_call(clnt->door_desc, &door_arg) < 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to make door call.");
		sc_syslog_msg_done(&handle);
		return (UNABLE_TO_MAKE_DOOR_CALL);
	}

	/*
	 * Now, we are back from the door_call and we need to unmarshall the
	 * data and get the return_code.
	 * But if the returned size is 0 and the rbuf = NULL, then we can be
	 * sure that a severe error has occured at the server and it has not
	 * been able to complete the call successfully.
	 */
	free(arg_buf); /* We can free this because the door_call is complete */
	free(pmf_args_p);

	/* Initialize the result XDR stream */

	xdrmem_create(&xdrs_result, door_arg.rbuf,
	    (uint_t)door_arg.rsize, XDR_DECODE);
	if ((door_arg.rbuf == NULL)&&(door_arg.rsize == 0)) {
		return (XDR_ERROR_AT_SERVER);
	}

	if (!xdr_int(&xdrs_result, &return_code)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "XDR Error while decoding return arguments.");
		sc_syslog_msg_done(&handle);
		return (XDR_ERROR_AT_CLIENT);
	} else {
		if (return_code != DOOR_CALL_SUCCESS) {
			/*
			 * Only if the return_code is DOOR_CALL_SUCCESS
			 * we proceed
			 */
			return (return_code);
		}
	}

	/* Now decode the other result data */
	if (!xdr_pmf_result_args(&xdrs_result, result,
	    pmf_api_name)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "XDR Error while decoding return arguments.");
		sc_syslog_msg_done(&handle);
		return (XDR_ERROR_AT_CLIENT);
	}
	xdr_destroy(&xdrs);
	xdr_destroy(&xdrs_result);
	munmap(door_arg.rbuf, door_arg.rsize);
	return (DOOR_CALL_SUCCESS);
}
#endif
