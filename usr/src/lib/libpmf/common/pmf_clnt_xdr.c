/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)pmf_clnt_xdr.c	1.3	08/05/20 SMI"

/*
 * File: pmf.x  RPC interface to HA Process Monitor Facility
 */


#include <rgm/pmf.h>
#include "rgm/door/pmf_door.h"
#include <rgm/security.h>

bool_t
xdr_pmf_input_args(register XDR *xdrs, pmf_input_args_t *objp)
{
	if (!xdr_int(xdrs, &objp->api_name)) {
		return (FALSE);
	}
	switch (objp->api_name) {
		case PMFPROC_NULL:
			break;
		case PMFPROC_START:
			if (!xdr_pmf_start_args(xdrs,
			    (pmf_start_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case PMFPROC_STATUS:
		case PMFPROC_STOP:
		case PMFPROC_KILL:
		case PMFPROC_SUSPEND:
		case PMFPROC_RESUME:
			if (!xdr_pmf_args(xdrs, (pmf_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case PMFPROC_MODIFY:
			if (!xdr_pmf_modify_args(xdrs,
			    (pmf_modify_args *)objp->data)) {
				return (FALSE);
			}
	}
	return (TRUE);
}


bool_t
xdr_pmf_result_args(XDR *xdrs, void *objp, int api_name)
{
	switch (api_name) {
		case PMFPROC_NULL:
			break;
		case PMFPROC_START:
		case PMFPROC_STOP:
		case PMFPROC_KILL:
		case PMFPROC_SUSPEND:
		case PMFPROC_RESUME:
		case PMFPROC_MODIFY:
			if (!xdr_pmf_result(xdrs, (pmf_result *)objp)) {
				return (FALSE);
			}
			break;
		case PMFPROC_STATUS:
			if (!xdr_pmf_status_result(xdrs,
			    (pmf_status_result *)objp)) {
				return (FALSE);
			}
	}
	return (TRUE);
}


/*
 * How we calculate size for xdr encoding:
 * ---------------------------------------
 * The Size calculation that we do here has to calculate
 * the sizeof the structure members and also the sizeof
 * the data that is pointed to be the pointers if any
 * int he structure.
 * For eg:
 * struct abc {
 *	int a;
 *	char *ch;
 * }
 * in this case we will find the size of the struct abc
 * which will give us the size needed for storing the
 * elements that is an int and a char pointer. And also
 * we will need to add the sizeof the array pointed to
 * by ch.
 * In addition to this, xdr encodes string in the following
 * way:
 * [integer(for size of the string) + string]
 * So, for any string say char *ch = {"example"}
 * The size needed will be :
 * sizeof (ch) + sizeof (int) + strlen(ch)
 * Here the sizeof (int) is taken for storing the sizeof
 * the string.
 */


size_t
pmf_xdr_sizeof(pmf_input_args_t *arg)
{
	size_t size = 0;

	uint_t i = 0;
	size = RNDUP(sizeof (arg->api_name));
	switch (arg->api_name) {
		case PMFPROC_NULL:
			break;
		case PMFPROC_START:
			size += RNDUP(sizeof (pmf_start_args)) +
			    RNDUP(safe_strlen(((pmf_start_args *)
			    (arg->data))->pwd)) +
			    RNDUP(safe_strlen(((pmf_start_args *)
			    (arg->data))->path)) +
			    RNDUP(safe_strlen(((pmf_start_args *)
			    (arg->data))->identifier)) +
			    RNDUP(safe_strlen(((pmf_start_args *)
			    (arg->data))->action)) +
			    RNDUP(safe_strlen(((pmf_start_args *)
			    (arg->data))->project_name)) +
			    (5 * RNDUP(sizeof (int)));
			if (((pmf_start_args *)(arg->data))->cmd.cmd_len > 0) {
				for (i = 0; i < ((pmf_start_args *)
				    (arg->data))->cmd.cmd_len; i++) {
					size += RNDUP(safe_strlen(
					    ((pmf_start_args *)
					    (arg->data))->cmd.cmd_val[i]));
					size += RNDUP(sizeof (int));
				}
			}

			if (((pmf_start_args *)(arg->data))->env.env_len
			    > 0) {
				for (i = 0; i < ((pmf_start_args *)
				    (arg->data))->env.env_len; i++) {
					size += RNDUP(safe_strlen(
					    ((pmf_start_args *)
					    (arg->data))->env.env_val[i]));
					size += RNDUP(sizeof (int));
				}
			}
			break;
		case PMFPROC_STATUS:
		case PMFPROC_STOP:
		case PMFPROC_KILL:
		case PMFPROC_SUSPEND:
		case PMFPROC_RESUME:
			size += RNDUP(sizeof (pmf_args)) +
			    RNDUP(safe_strlen(((pmf_args *)
			    (arg->data))->identifier)) +
			    RNDUP(sizeof (int));
			break;
		case PMFPROC_MODIFY:
			size += RNDUP(sizeof (pmf_modify_args)) +
			    RNDUP(safe_strlen(((pmf_modify_args *)
			    (arg->data))->identifier)) +
			    RNDUP(sizeof (int));
			break;
	}
	return (size);
}
