#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.41	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libpmf/Makefile.com
#

LIBRARY= libpmf.a
VERS= .1

include $(SRC)/Makefile.master

# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

OBJECTS = pmf.o pmf_clnt_xdr.o pmf_door_xdr.o
$(RPC_IMPL)OBJECTS = pmf.o pmf_clnt.o pmf_xdr.o

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

# include the macros for doors or rpc implementation again, because
# Makefile.lib screws up the CFLAGS/CPPFLAGS
include $(SRC)/Makefile.ipc

ROOTRGMHDRDIR =		$(ROOTCLUSTHDRDIR)/rgm	
ROOTRPCXDIR =		$(ROOTRGMHDRDIR)/door
$(RPC_IMPL)ROOTRPCXDIR =		$(ROOTRGMHDRDIR)/rpc	

RPCFILE =		$(ROOTRPCXDIR)/pmf_door.x
$(RPC_IMPL)RPCFILE =		$(ROOTRPCXDIR)/pmf.x

RPCGENFLAGS =		$(RPCGENMT) -C -K -1

RPCFILE_XDR =	../common/pmf_door_xdr.c
$(RPC_IMPL)RPCFILE_CLNT =	../common/pmf_clnt.c
$(RPC_IMPL)RPCFILE_XDR =	../common/pmf_xdr.c


RPCFILES =	$(RPCFILE_XDR)
$(RPC_IMPL)RPCFILES =	$(RPCFILE_CLNT)	$(RPCFILE_XDR)


CLOBBERFILES	+= $(RPCFILES)

MAPFILE=	../common/mapfile-vers
SRCS =		$(OBJECTS:%.o=../common/%.c)

# Don't check generated files so don't use OBJECTS for CHECK_FILES
CHECK_FILES = pmf.c_check

LIBS = $(DYNLIB)

# definitions for lint
LINTFLAGS += -I. -I$(ROOTRPCXDIR) -I$(ROOTRGMHDRDIR) -I$(ROOTCLUSTHDRDIR)
LINTFLAGS64 += -I. -I$(ROOTRPCXDIR) -I$(ROOTRGMHDRDIR) -I$(ROOTCLUSTHDRDIR)
LINTFILES = pmf.ln

CPPFLAGS += -I. -I$(ROOTRPCXDIR) -I$(ROOTRGMHDRDIR) -I$(ROOTCLUSTHDRDIR)
LDLIBS	+= -lc -lnsl -lsecurity -lclos $(DOOR_LDLIBS)
PMAP	= -M $(MAPFILE)

.KEEP_STATE:

.NO_PARALLEL:

all:  $(RPCFILES) $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
