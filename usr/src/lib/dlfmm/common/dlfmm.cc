/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * dlfmm.c
 *
 *	safclm API through dynamic linking ...
 */

#pragma ident	"@(#)dlfmm.cc	1.2	08/05/20 SMI"

#include <dlfcn.h>
#include "dlfmm.h"

static char *funcnames[] = {
	"saClmInitialize",
	"saClmSelectionObjectGet",
	"saClmDispatch",
	"saClmFinalize",
	"saClmClusterTrackStart",
	"saClmClusterTrackStop",
	"saClmClusterNodeGet",
	"saClmClusterNodeGetAsync"
};

static char path[] = SAFCLM_LIB_PATH;

/*
 * Load safclm dynamic library. Returns instance of opened library,
 * and populates the api argument with the functions addresses exported
 * by the API.
 * Return 0 in case of success, -1 otherwise.
 */
int
safclm_load_module(void **instance, safclm_api_t *api)
{
	*instance = dlopen(path, RTLD_LAZY);
	if (*instance == NULL) {
		return (-1);
	}

	if ((api->saClmInitialize = (safclm_function_initialize_t)dlsym
	    (*instance, funcnames[0])) == NULL) {
		(void) safclm_unload_module(instance, api);
		return (-1);
	}

	if ((api->saClmSelectionObjectGet =
	    (safclm_function_selectionobjectget_t)dlsym(*instance,
	    funcnames[1])) == NULL) {
		(void) safclm_unload_module(instance, api);
		return (-1);
	}

	if ((api->saClmDispatch = (safclm_function_dispatch_t)dlsym(*instance,
	    funcnames[2])) == NULL) {
		(void) safclm_unload_module(instance, api);
		return (-1);
	}

	if ((api->saClmFinalize = (safclm_function_finalize_t)dlsym(*instance,
	    funcnames[3])) == NULL) {
		(void) safclm_unload_module(instance, api);
		return (-1);
	}

	if ((api->saClmClusterTrackStart = (safclm_function_clustertrackstart_t)
	    dlsym(*instance, funcnames[4])) == NULL) {
		(void) safclm_unload_module(instance, api);
		return (-1);
	}

	if ((api->saClmClusterTrackStop = (safclm_function_clustertrackstop_t)
	    dlsym(*instance, funcnames[5])) == NULL) {
		(void) safclm_unload_module(instance, api);
		return (-1);
	}

	if ((api->saClmClusterNodeGet = (safclm_function_clusternodeget_t)
	    dlsym(*instance, funcnames[6])) == NULL) {
		(void) safclm_unload_module(instance, api);
		return (-1);
	}

	if ((api->saClmClusterNodeGetAsync =
	    (safclm_function_clusternodegetasync_t)dlsym(*instance,
	    funcnames[7])) == NULL) {
		(void) safclm_unload_module(instance, api);
		return (-1);
	}

	return (0);
}

/*
 * Unload a safclm library instance.
 */
int
safclm_unload_module(void **instance, safclm_api_t *api)
{
	static safclm_api_t null_api;

	if (dlclose(*instance) != 0) {
		return (-1);
	}

	*instance = NULL;
	*api = null_api;

	return (0);
}
