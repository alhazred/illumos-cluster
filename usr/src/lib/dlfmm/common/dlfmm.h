/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * safclm_api.h
 *
 *	safclm API through dynamic linking ...
 */

#ifndef	_DLFMM_H
#define	_DLFMM_H

#pragma ident	"@(#)dlfmm.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <fmm/saf_clm.h>

#define	SAFCLM_LIB_PATH	"/usr/cluster/lib/libfmm.so.1"

typedef SaErrorT (*safclm_function_initialize_t)(SaClmHandleT *clmHandle,
	const SaClmCallbacksT *clmCallbacks, const SaVersionT *version);
typedef SaErrorT (*safclm_function_selectionobjectget_t)(SaClmHandleT clmHandle,
	SaSelectionObjectT *selectionObject);
typedef SaErrorT (*safclm_function_dispatch_t)(SaClmHandleT clmHandle,
	SaDispatchFlagsT dispatchFlags);
typedef SaErrorT (*safclm_function_finalize_t)(SaClmHandleT clmHandle);
typedef SaErrorT (*safclm_function_clustertrackstart_t)(SaClmHandleT clmHandle,
	SaUint8T trackFlags, SaClmClusterNotificationT *notificationBuffer,
	SaUint32T numberOfItems);
typedef SaErrorT (*safclm_function_clustertrackstop_t)(SaClmHandleT clmHandle);
typedef SaErrorT (*safclm_function_clusternodeget_t)(SaClmHandleT clmHandle,
	SaClmNodeIdT nodeId, SaTimeT timeout, SaClmClusterNodeT *clusterNode);
typedef SaErrorT (*safclm_function_clusternodegetasync_t)(
	SaClmHandleT clmHandle, SaInvocationT invocation, SaClmNodeIdT nodeId,
	SaClmClusterNodeT *clusterNode);

typedef struct {
	safclm_function_initialize_t saClmInitialize;
	safclm_function_selectionobjectget_t saClmSelectionObjectGet;
	safclm_function_dispatch_t saClmDispatch;
	safclm_function_finalize_t saClmFinalize;
	safclm_function_clustertrackstart_t saClmClusterTrackStart;
	safclm_function_clustertrackstop_t saClmClusterTrackStop;
	safclm_function_clusternodeget_t saClmClusterNodeGet;
	safclm_function_clusternodegetasync_t saClmClusterNodeGetAsync;
} safclm_api_t;

int safclm_load_module(void **instance, safclm_api_t *api);
int safclm_unload_module(void **instance, safclm_api_t *api);

#ifdef __cplusplus
}
#endif

#endif /* _DLFMM_H */
