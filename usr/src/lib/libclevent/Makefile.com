#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.10	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libclevent/Makefile.com
#
include $(SRC)/Makefile.master

LIBRARYCCC= libclevent.a
VERS= .1

include $(SRC)/common/cl/Makefile.files

DOOR_OBJECTS = 
$(POST_S9_BUILD)DOOR_OBJECTS = sc_event_proxy_xdr.o

OBJECTS += $(LIBCLEVENT_UOBJS) $(DOOR_OBJECTS)

include ../../Makefile.lib

# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

CLOBBERFILES +=       $(RPCFILES)

CC_PICFLAGS = -KPIC

SRCS =		$(OBJECTS:%.o=../common/%.c)

MTFLAG	= -mt
CPPFLAGS += $(CL_CPPFLAGS)
CPPFLAGS += -I$(SRC)/common/cl

#
# Overrides
#

LIBS = $(DYNLIBCCC)

LDLIBS += -lsocket -lnsl -lposix4 -lc -ldl -lnvpair -lsysevent -ldoor
LDLIBS += -lclos -lthread
$(POST_S9_BUILD)LDLIBS += -lsecurity

CLEANFILES =
LINTFILES += $(OBJECTS:%.o=%.ln)

RPCFILE = $(SRC)/head/sc_event_proxy/sc_event_proxy.x
RPCGENFLAGS = $(RPCGENMT) -C -K -1
RPCFILE_XDR = $(SRC)/common/cl/libclevent/sc_event_proxy_xdr.c
RPCFILES = $(RPCFILE_XDR)

PMAP =

OBJS_DIR = objs pics

.KEEP_STATE:

.NO_PARALLEL:

all: $(RPCFILES) $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules
