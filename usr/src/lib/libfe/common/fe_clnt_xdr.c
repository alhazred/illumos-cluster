/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fe_clnt_xdr.c	1.6	08/05/20 SMI"

/*
 * File: fe.x  RPC interface to fork execution daemon
 */
#include <rgm/security.h>
#include <rgm/fe.h>
#include <string.h>
#include <rgm/door/fe_door.h>

bool_t
xdr_fed_input_args(register XDR *xdrs, fed_input_args_t *objp)
{
	if (!xdr_int(xdrs, &objp->api_name)) {
		return (FALSE);
	}
	switch (objp->api_name) {
		case FEPROC_NULL:
			break;
		case FEPROC_RUN:
			if (!xdr_fe_run_args(xdrs, (fe_run_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case FEPROC_SUSPEND:
		case FEPROC_RESUME:
		case FEPROC_KILL:
			if (!xdr_fe_args(xdrs, (fe_args *)objp->data)) {
				return (FALSE);
			}
			break;
		/* SC SLM addon start */
		case FEPROC_SLM_UPDATE:
			if (!xdr_fe_slm_update_args(xdrs,
			    (fe_slm_update_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case FEPROC_SLM_CONF:
			if (!xdr_fe_slm_conf_args(xdrs,
			    (fe_slm_conf_args *)objp->data)) {
				return (FALSE);
			}
			break;
		/* SC SLM addon end */
	}
	return (TRUE);
}

bool_t
xdr_fed_result_args(XDR *xdrs, void *objp, int api_name)
{
	switch (api_name) {
		case FEPROC_NULL:

			break;
		case FEPROC_RUN:
			if (!xdr_fe_run_result(xdrs, (fe_run_result *)objp)) {
				return (FALSE);
			}
			break;
		case FEPROC_SUSPEND:
		case FEPROC_RESUME:
		case FEPROC_KILL:
			if (!xdr_fe_result(xdrs, (fe_result *)objp)) {
				return (FALSE);
			}
			break;
		/* SC SLM addon start */
		case FEPROC_SLM_UPDATE:
		case FEPROC_SLM_CONF:
		case FEPROC_SLM_CHECK:
			if (!xdr_int(xdrs, (int *)objp)) {
				return (FALSE);
			}
			break;
		case FEPROC_SLM_DEBUG:
			break;
		/* SC SLM addon end */
	}
	return (TRUE);


}

/*
 * How we calculate size for xdr encoding:
 * ---------------------------------------
 * The Size calculation that we do here has to calculate
 * the sizeof the structure members and also the sizeof
 * the data that is pointed to be the pointers if any
 * int he structure.
 * For eg:
 * struct abc {
 *	int a;
 *	char *ch;
 * }
 * in this case we will find the size of the struct abc
 * which will give us the size needed for storing the
 * elements that is an int and a char pointer. And also
 * we will need to add the sizeof the array pointed to
 * by ch.
 * In addition to this, xdr encodes string in the following
 * way:
 * [integer(for size of the string) + string]
 * So, for any string say char *ch = {"example"}
 * The size needed will be :
 * sizeof (ch) + sizeof (int) + strlen(ch)
 * Here the sizeof (int) is taken for storing the sizeof
 * the string.
 */

size_t
fed_xdr_sizeof(fed_input_args_t *arg)
{
	size_t size = 0;
	uint_t i = 0;
	size += RNDUP(sizeof (arg->api_name));
	switch (arg->api_name) {
		case FEPROC_NULL:break;
		case FEPROC_RUN:
			size += RNDUP(sizeof (fe_run_args)) +
			    RNDUP(safe_strlen(((fe_run_args *)
			    (arg->data))->cmd)) +
			    RNDUP(safe_strlen(((fe_run_args *)
			    (arg->data))->tag)) +
			    RNDUP(safe_strlen(((fe_run_args *)
			    (arg->data))->corepath)) +
			    RNDUP(safe_strlen(((fe_run_args *)
			    (arg->data))->zonename)) +
			    RNDUP(safe_strlen(((fe_run_args *)
			    (arg->data))->project_name));
			size += (5 * RNDUP(sizeof (int)));
			/*
			 * The following loops go through each and every
			 * string and gets the size of the string and add
			 * to the total size
			 */
			if (((fe_run_args *)(arg->data))->arg.arg_len > 0) {
				for (i = 0; i < ((fe_run_args *)(arg->data))->
				    arg.arg_len; i++) {
					size += RNDUP(safe_strlen(
					    ((fe_run_args *)
					    (arg->data))->arg.arg_val[i]));
					size += RNDUP(sizeof (int));
				}
			}
			if (((fe_run_args *)(arg->data))->env.env_len > 0) {
				for (i = 0; i < ((fe_run_args *)(arg->data))->
				    env.env_len; i++) {
					size += RNDUP(safe_strlen(
					    ((fe_run_args *)
					    (arg->data))->env.env_val[i]));
					size += RNDUP(sizeof (int));
				}
			}
			/* SC SLM addon start */
			size += RNDUP(safe_strlen(((fe_run_args *)
			    (arg->data))->ssm_wrapper_zone));
			size += RNDUP(safe_strlen(((fe_run_args *)
			    (arg->data))->r_name));
			size += RNDUP(safe_strlen(((fe_run_args *)
			    (arg->data))->rg_name));
			size += RNDUP(safe_strlen(((fe_run_args *)
			    (arg->data))->rg_slm_type));
			size += RNDUP(safe_strlen(((fe_run_args *)
			    (arg->data))->rg_slm_pset_type));
			size += (5 * RNDUP(sizeof (unsigned int)));
			/* SC SLM addon end */
			break;
		case FEPROC_SUSPEND:
		case FEPROC_RESUME:
		case FEPROC_KILL:
			size += RNDUP(sizeof (fe_args)) +
			    RNDUP(safe_strlen(((fe_args *)
			    (arg->data))->tag)) +
			    RNDUP(sizeof (int));
			break;
		/* SC SLM addon start */
		case FEPROC_SLM_UPDATE:
			size += RNDUP(sizeof (fe_slm_update_args)) +
			    RNDUP(safe_strlen(((fe_slm_update_args *)
			    (arg->data))->rg_name)) +
			    RNDUP(safe_strlen(((fe_slm_update_args *)
			    (arg->data))->r_name)) +
			    RNDUP(safe_strlen(((fe_slm_update_args *)
			    (arg->data))->rg_slm_type)) +
			    RNDUP(safe_strlen(((fe_slm_update_args *)
			    (arg->data))->rg_slm_pset_type));
			size += (4 * RNDUP(sizeof (int)));
			break;
		case FEPROC_SLM_CONF:
			size += RNDUP(sizeof (fe_slm_conf_args));
		/* SC SLM addon end */
	}
	return (size);
}
