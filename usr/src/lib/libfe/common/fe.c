/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fe.c	1.54	09/03/02 SMI"


/*
 * fe.c - part of libfe
 *
 *	RPC client interface to SC Fork Execution Daemon
 */
#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <tzfile.h>
#include <rpc/rpc.h>
#include <pwd.h>
#include <locale.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <sys/param.h>
#include <rgm/security.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#if DOOR_IMPL
#include <rgm/rgm_recep.h>
#include <rgm/door/rgm_recep_door.h>
#endif
#include "rgm/fecl.h"

#ifdef DEBUG
static int debug = 1;
#else
static int debug = 0;
#endif

static int fe_rpc_command(char *tag, fe_cmd_type type,
    fe_cmd *cmd_opts, char *cmd_path, char *argv[],
    char *env[], fe_run_result *result);


#if !DOOR_IMPL
static void set_createrror(fe_run_result *result, char *host);
#endif


static char *set_locale_category(uint_t locale_category,
    char *locale_cat_string);

#if DOOR_IMPL
static void set_error(fe_run_result *fecmd, char *action,
    int ret);
#else
static void set_error(CLIENT *clnt, fe_run_result *result, char *action,
    enum clnt_stat ret);
#endif

/*
 * This function is the library interface for the feproc_run rpc call:
 * It is called to execute a method remotely.
 * The client calls fe_run(), which in turn calls feproc_run_2();
 * it hides from the client the details of RPC connections and security.
 */
int
fe_run(char *tag, fe_cmd *cmd_opts, char *cmd_path, char *argv[],
    char *env[], fe_run_result *res)
{
	/*	tag, type, options, cmd_path, argv, env, result	*/
	return (fe_rpc_command(tag, FE_RUN, cmd_opts, cmd_path,
	    argv, env, res));
}


/*
 * This function is the library interface for the feproc_suspend rpc call:
 * It is called to suspend monitoring of a method execution timeout.
 * The client calls fe_suspend(), which in turn calls feproc_suspend_2();
 * it hides from the client the details of RPC connections and security.
 */
int
fe_suspend(char *tag, fe_cmd *cmd_opts, fe_run_result *res)
{
	/*	tag, type, options, cmd_path, argv, env, result	*/
	return (fe_rpc_command(tag, FE_SUSPEND, cmd_opts, NULL,
	    NULL, NULL, res));
}


/*
 * This function is the library interface for the feproc_resume rpc call:
 * It is called to resume monitoring of a method execution timeout.
 * The client calls fe_resume(), which in turn calls feproc_resume_2();
 * it hides from the client the details of RPC connections and security.
 */
int
fe_resume(char *tag, fe_cmd *cmd_opts, fe_run_result *res)
{
	/*	tag, type, options, cmd_path, argv, env, result	*/
	return (fe_rpc_command(tag, FE_RESUME, cmd_opts, NULL,
	    NULL, NULL, res));
}


/*
 * This function is the library interface for the feproc_kill rpc call:
 * It is called to kill an inflight method instantly.
 * The client calls fe_kill(), which in turn calls feproc_kill_2();
 * it hides from the client the details of RPC connections and security.
 */
int
fe_kill(char *tag, fe_cmd *cmd_opts, fe_run_result *res)
{
	/*	tag, type, options, cmd_path, argv, env, result	*/
	return (fe_rpc_command(tag, FE_KILL, cmd_opts, NULL, NULL, NULL, res));
}

/* SC SLM addon start */
int
fe_slm_update(char *tag, fe_cmd *cmd_opts, fe_run_result *res)
{
	/*	tag, type, options, cmd_path, argv, env, result	*/
	return (fe_rpc_command(tag, FEPROC_SLM_UPDATE,
	    cmd_opts, NULL, NULL, NULL, res));
}

int
fe_slm_conf_global_zone_shares(char *tag, fe_cmd *cmd_opts, fe_run_result *res)
{
	/*	tag, type, options, cmd_path, argv, env, result	*/
	cmd_opts->slm_flags = FE_SLM_CONF_GLOBAL_ZONE_SHARES;
	return (fe_rpc_command(tag, FEPROC_SLM_CONF,
	    cmd_opts, NULL, NULL, NULL, res));
}

int
fe_slm_conf_pset_min(char *tag, fe_cmd *cmd_opts, fe_run_result *res)
{
	/*	tag, type, options, cmd_path, argv, env, result	*/
	cmd_opts->slm_flags = FE_SLM_CONF_PSET_MIN;
	return (fe_rpc_command(tag, FEPROC_SLM_CONF,
	    cmd_opts, NULL, NULL, NULL, res));
}

int
fe_slm_debug(char *tag, fe_cmd *cmd_opts, fe_run_result *res)
{
	/*	tag, type, options, cmd_path, argv, env, result	*/
	return (fe_rpc_command(tag, FEPROC_SLM_DEBUG,
	    cmd_opts, NULL, NULL, NULL, res));
}

int
fe_slm_check(char *tag, fe_cmd *cmd_opts, fe_run_result *res)
{
	/*	tag, type, options, cmd_path, argv, env, result	*/
	return (fe_rpc_command(tag, FEPROC_SLM_CHECK,
	    cmd_opts, NULL, NULL, NULL, res));
}
/* SC SLM addon end */

static int
fe_rpc_command(char *tag, fe_cmd_type type,
    fe_cmd *cmd_opts, char *cmd_path, char *argv[],
    char *env[], fe_run_result *result)
{

#if DOOR_IMPL
	client_handle *clnt = NULL;
#else
	CLIENT	*clnt;
#endif

	fe_run_args  feproc_run_arg = {0};
	fe_args feproc_suspend_arg = {0};
	fe_args feproc_resume_arg = {0};
	fe_args feproc_kill_arg = {0};
	/* SC SLM addon start */
	fe_slm_update_args feproc_slm_update_args = {0};
	fe_slm_conf_args feproc_slm_conf = {0};
	/* SC SLM addon end */

	int ret;
	uint_t i;
	sc_syslog_msg_handle_t handle;
	SEC_TYPE sec_type = SEC_UNIX_WEAK;
	char *host = NULL;
	fe_result sr_result;
	/* SC SLM addon start */
	int result_int = 0;
	/* SC SLM addon end */
#if SOL_VERSION >= __s10
	char *zonename = GLOBAL_ZONENAME;
#else
	char *zonename = NULL;
#endif

	if (cmd_opts) {
		sec_type = cmd_opts->security;
		if (cmd_opts->host) {
			host = cmd_opts->host;
		} else {
			/*
			 * We always default to the physical hostname of the
			 * local node. Authentication and authorization are
			 * managed by the daemon.
			 */
			host = getlocalhostname();
			if (host == NULL)
				return (-1);
		}
	}

	switch (type) {
	case FE_RUN:
		feproc_run_arg.tag = tag;
		/*
		 * we hardcode the grace period to 15 sec,
		 * same value used in HA 1.3
		 */
		feproc_run_arg.grace_period = 15;
		feproc_run_arg.cmd = cmd_path;
		if (cmd_opts) {
			feproc_run_arg.timeout = cmd_opts->timeout;
			feproc_run_arg.flags = cmd_opts->flags;
			feproc_run_arg.zonename = cmd_opts->zonename;
			feproc_run_arg.project_name = cmd_opts->project_name;
			feproc_run_arg.corepath = NULL;
			feproc_run_arg.zonepath = NULL;
			if (cmd_opts->flags & FE_PRODUCE_CORE) {
				if (cmd_opts->corepath == NULL)
					return (-1);
				feproc_run_arg.corepath = cmd_opts->corepath;
#if SOL_VERSION >= __s10
				feproc_run_arg.zonepath = cmd_opts->zonepath;
#endif
			}
			/* SC SLM addon start */
			feproc_run_arg.slm_flags = cmd_opts->slm_flags;
			feproc_run_arg.ssm_wrapper_zone =
			    cmd_opts->ssm_wrapper_zone;
			feproc_run_arg.r_name = cmd_opts->r_name;
			feproc_run_arg.rg_name = cmd_opts->rg_name;
			feproc_run_arg.rg_slm_type = cmd_opts->rg_slm_type;
			feproc_run_arg.rg_slm_pset_type =
			    cmd_opts->rg_slm_pset_type;
			feproc_run_arg.rg_slm_cpu =
			    cmd_opts->rg_slm_cpu;
			feproc_run_arg.rg_slm_cpu_min =
			    cmd_opts->rg_slm_cpu_min;
			/* SC SLM addon end */
		}

		feproc_run_arg.arg.arg_val = argv;
		/* count length of arg array */
		/* suppress lint error */
		for (i = 0; feproc_run_arg.arg.arg_val[i] != NULL;
		    i++); /*lint !e722 */
		feproc_run_arg.arg.arg_len = i + 1; /* +1 for null at the end */

		feproc_run_arg.env.env_val = env;
		/* count length of env array */
		/* suppress lint error */
		for (i = 0; feproc_run_arg.env.env_val[i] != NULL;
		    i++); /*lint !e722 */
		feproc_run_arg.env.env_len = i + 1; /* +1 for null at the end */

		if (debug) {
			dbg_msgout(NOGET("%s:argv val:\n"), tag);
			for (i = 0; i < feproc_run_arg.arg.arg_len - 1; i++) {
				dbg_msgout(NOGET("%s:%s \n"), tag,
					feproc_run_arg.arg.arg_val[i]);
			}
			dbg_msgout(NOGET("%s:arg len is %u\n"), tag,
				feproc_run_arg.arg.arg_len);

			dbg_msgout(NOGET("%senv val:\n"), tag);
			for (i = 0; i < feproc_run_arg.env.env_len - 1; i++) {
				dbg_msgout(NOGET("%s:%s \n"), tag,
					feproc_run_arg.env.env_val[i]);
			}
			dbg_msgout(NOGET("%s:env len is %u\n"), tag,
				feproc_run_arg.env.env_len);
		}
		break;

	case FE_SUSPEND:
		feproc_suspend_arg.tag = tag;
		break;

	case FE_RESUME:
		feproc_resume_arg.tag = tag;
		break;
	case FE_KILL:
		feproc_kill_arg.tag = tag;
		break;

	/* SC SLM addon start */
	case FEPROC_SLM_UPDATE:
		feproc_slm_update_args.type = cmd_opts->slm_flags;
		feproc_slm_update_args.rg_name = cmd_opts->rg_name;
		feproc_slm_update_args.r_name = cmd_opts->r_name;
		feproc_slm_update_args.rg_slm_type = cmd_opts->rg_slm_type;
		feproc_slm_update_args.rg_slm_pset_type =
		    cmd_opts->rg_slm_pset_type;
		feproc_slm_update_args.rg_slm_cpu =
		    cmd_opts->rg_slm_cpu;
		feproc_slm_update_args.rg_slm_cpu_min =
		    cmd_opts->rg_slm_cpu_min;
		break;

	case FEPROC_SLM_CONF:
		switch (cmd_opts->slm_flags) {
			case FE_SLM_CONF_GLOBAL_ZONE_SHARES:
				feproc_slm_conf.type =
				    FE_SLM_CONF_GLOBAL_ZONE_SHARES;
				feproc_slm_conf.global_zone_shares =
				    cmd_opts->global_zone_shares;
				break;
			case FE_SLM_CONF_PSET_MIN:
				feproc_slm_conf.type =
				    FE_SLM_CONF_PSET_MIN;
				feproc_slm_conf.pset_min = cmd_opts->pset_min;
				break;
			default:
				break;
		}
		break;

	case FEPROC_SLM_DEBUG:
		break;
	case FEPROC_SLM_CHECK:
		break;
	/* SC SLM addon end */

	case FE_NULL:
	default:
		return (-1);
	}

	/*
	 * connect to server
	 * log hostname irrespective of debug mode to help identify libsecurity
	 * bugs related to hostname.
	 */
	dbg_msgout_onlysyslog(LOG_DEBUG, NOGET("fe_rpc_command: cmd_type(enum)"
	    ":<%d>:cmd=<%s>:tag=<%s>: Calling security_clnt_connect(..., "
	    "host=<%s>, sec_type {0:WEAK, 1:STRONG, 2:DES} =<%d>, "
	    "...)\n"), type, cmd_path ? cmd_path : "null",
	    tag ? tag : "null", host ? host : "null", sec_type);
#if DOOR_IMPL
	if (!security_clnt_connect(&clnt, RGM_FE_PROGRAM_CODE,
	    sec_type, debug, 0, zonename)) {
		return (-2);
	}

#else
	if (!security_clnt_connect(&clnt, host, FE_PROGRAM_NAME,
	    (ulong_t)FE_PROGRAM, (ulong_t)FE_VERSION, sec_type, debug, 0)) {
	    set_createrror(result, host);
	    return (-2);
	}
#endif


#if DOOR_IMPL
	if (clnt == (client_handle *) NULL) {
#else
	if (clnt == (CLIENT *) NULL) {
#endif
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The rgm is not able to obtain an rpc client handle to
		 * connect to the rpc.fed server on the named host. An error
		 * message is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "host %s: client is null", host);
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	if (debug) {
		dbg_msgout(NOGET("fe_rpc_command: Trying null rpc... "));
#if DOOR_IMPL
		ret = fed_door_clnt(NULL, NULL, FEPROC_NULL, clnt);
		if (ret == DOOR_CALL_SUCCESS)
#else
		ret = feproc_null_2(NULL, NULL, clnt);
		if (ret == RPC_SUCCESS)
#endif
			dbg_msgout(NOGET("Succeeded.\n"));
		else dbg_msgout(NOGET("Failed.\n"));
	}

	switch (type) {
	case FE_RUN:
#if DOOR_IMPL
		ret = fed_door_clnt(&feproc_run_arg,
		    result, FEPROC_RUN, clnt);
		if (ret != DOOR_CALL_SUCCESS) {
			set_error(result, NOGET("FE_RUN"), ret);
		}

#else
		ret = feproc_run_2(&feproc_run_arg,
		    result, clnt);
		if (ret != RPC_SUCCESS)
			set_error(clnt, result, NOGET("FE_RUN"),
			    ret);
#endif

		break;
	case FE_SUSPEND:
#if DOOR_IMPL
		ret = fed_door_clnt(&feproc_suspend_arg,
		    &sr_result, FEPROC_SUSPEND, clnt);

		if (ret == DOOR_CALL_SUCCESS) {
			result->type = sr_result.type;
			result->sys_errno = sr_result.sys_errno;
			result->sec_errno = sr_result.sec_errno;
		} else {
			set_error(result, NOGET("FE_SUSPEND"), ret);
		}

#else
		ret = feproc_suspend_2(&feproc_suspend_arg,
		    &sr_result, clnt);
		result->type = sr_result.type;
		result->sys_errno = sr_result.sys_errno;
		result->sec_errno = sr_result.sec_errno;
		if (ret != RPC_SUCCESS)
			set_error(clnt, result, NOGET("FE_SUSPEND"),
			    ret);
#endif

		break;
	case FE_RESUME:

#if DOOR_IMPL
		ret = fed_door_clnt(&feproc_resume_arg,
		    &sr_result, FEPROC_RESUME, clnt);
		if (ret == DOOR_CALL_SUCCESS) {
			result->type = sr_result.type;
			result->sys_errno = sr_result.sys_errno;
			result->sec_errno = sr_result.sec_errno;
		} else {
			set_error(result, NOGET("FE_RESUME"), ret);
		}
#else
		ret = feproc_resume_2(&feproc_resume_arg,
		    &sr_result, clnt);
		result->type = sr_result.type;
		result->sys_errno = sr_result.sys_errno;
		result->sec_errno = sr_result.sec_errno;
		if (ret != RPC_SUCCESS)
			set_error(clnt, result, NOGET("FE_RESUME"),
			    ret);
#endif

		break;

	case FE_KILL:

#if DOOR_IMPL
		ret = fed_door_clnt(&feproc_kill_arg,
		    &sr_result, FEPROC_KILL, clnt);
		if (ret == DOOR_CALL_SUCCESS) {
			result->type = sr_result.type;
			result->sys_errno = sr_result.sys_errno;
			result->sec_errno = sr_result.sec_errno;
		} else {
			set_error(result, NOGET("FE_KILL"), ret);
		}
#else
		ret = feproc_kill_2(&feproc_kill_arg,
		    &sr_result, clnt);
		result->type = sr_result.type;
		result->sys_errno = sr_result.sys_errno;
		result->sec_errno = sr_result.sec_errno;
		if (ret != RPC_SUCCESS)
			set_error(clnt, result, NOGET("FE_KILL"),
			    ret);
#endif
		break;

	/* SC SLM addon start */
	case FEPROC_SLM_UPDATE:

#if DOOR_IMPL
		ret = fed_door_clnt(&feproc_slm_update_args,
		    &result_int, FEPROC_SLM_UPDATE, clnt);
		if (ret == DOOR_CALL_SUCCESS) {
			result->type = FE_OKAY;
			result->sys_errno = result_int;
		} else {
			set_error(result, NOGET("FEPROC_SLM_UPDATE"), ret);
		}
#else
		ret = feproc_slm_update_2(&feproc_slm_update_args,
		    &result_int, clnt);
		if (result_int == 0) {
			result->type = FE_OKAY;
		} else {
			result->type = FE_SLM_ERROR;
		}
		if (ret != RPC_SUCCESS)
			set_error(clnt, result, NOGET("FEPROC_SLM_UPDATE"),
			    ret);
#endif
		break;

	case FEPROC_SLM_CONF:

#if DOOR_IMPL
		ret = fed_door_clnt(&feproc_slm_conf,
		    &result_int, FEPROC_SLM_CONF, clnt);
		if (ret == DOOR_CALL_SUCCESS) {
			result->type = FE_OKAY;
			result->sys_errno = result_int;
		} else {
			set_error(result,
			    NOGET("FEPROC_SLM_CONF"), ret);
		}
#else
		ret = feproc_slm_conf_2(&feproc_slm_conf,
		    &result_int, clnt);
		if (result_int == 0) {
			result->type = FE_OKAY;
		} else {
			result->type = FE_SLM_ERROR;
		}
		if (ret != RPC_SUCCESS)
			set_error(clnt, result,
			    NOGET("FEPROC_SLM_CONF"), ret);
#endif
		break;

	case FEPROC_SLM_DEBUG:

#if DOOR_IMPL
		ret = fed_door_clnt(NULL,
		    &result_int, FEPROC_SLM_DEBUG, clnt);
		if (ret == DOOR_CALL_SUCCESS) {
			result->type = FE_OKAY;
		} else {
			set_error(result, NOGET("FEPROC_SLM_DEBUG"), ret);
		}
#else
		ret = feproc_slm_debug_2(NULL, NULL, clnt);
		result->type = FE_OKAY;
		if (ret != RPC_SUCCESS)
			set_error(clnt, result, NOGET("FEPROC_SLM_DEBUG"),
			    ret);
#endif
		break;

	case FEPROC_SLM_CHECK:

#if DOOR_IMPL
		ret = fed_door_clnt(NULL,
		    &result_int, FEPROC_SLM_CHECK, clnt);
		if (ret == DOOR_CALL_SUCCESS) {
			result->type = FE_OKAY;
			result->sys_errno = result_int;
		} else {
			set_error(result, NOGET("FEPROC_SLM_CHECK"), ret);
		}
#else
		ret = feproc_slm_check_2(NULL, NULL, clnt);
		result->type = FE_OKAY;
		if (ret != RPC_SUCCESS)
			set_error(clnt, result, NOGET("FEPROC_SLM_CHECK"),
			    ret);
#endif
		break;
	/* SC SLM addon end */

	case FE_NULL:
	default:
		result->type = FE_UNKNC;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred in the rgmd while trying to
		 * connect to the rpc.fed server.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "Internal: Unknown command type (%d)",
		    type);
		sc_syslog_msg_done(&handle);
		security_clnt_freebinding(clnt);
		return (-1);
	}

	/*
	 * after client call, release client handle
	 */
	security_clnt_freebinding(clnt);
#if DOOR_IMPL
	if (ret != DOOR_CALL_SUCCESS)

#else
	if (ret != RPC_SUCCESS)
#endif

		return (-2);

	return (0);

}



/*
 * assemble name of full path:
 * if method name starts with "/" it already is a full name, just copy it;
 * otherwise concatenate basedir and method name.
 * Returns the full method name;
 * the memory has to be freed by the caller.
 *
 * If method name is NULL (as may be the case for a scalable service),
 * return NULL.
 */
char *
fe_method_full_name(char *basedir, char *method_name)
{
	char	*target;
	sc_syslog_msg_handle_t handle;

	if (method_name == NULL) {
		return (NULL);
	}

	if (method_name[0] == '/') {
		target = strdup(method_name);
		if (target == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rgmd server was not able to create the full
			 * name of the method, while trying to connect to the
			 * rpc.fed server, possibly due to low memory. An
			 * error message is output to syslog.
			 * @user_action
			 * Determine if the host is running out of memory. If
			 * not save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "method_full_name: strdup failed");
			sc_syslog_msg_done(&handle);
			return (NULL);
		}
	} else {
		/* add 2 bytes for the '/' character and the terminating NULL */
		target = (char *)malloc(strlen(basedir) +
		    strlen(method_name) + 2);
		if (target == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rgmd server was not able to create the full
			 * name of the method, while trying to connect to the
			 * rpc.fed server. This problem can occur if the
			 * machine has low memory. An error message is output
			 * to syslog.
			 * @user_action
			 * Determine if the host is running out of memory. If
			 * not save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "method_full_name: malloc failed");
			sc_syslog_msg_done(&handle);
			return (NULL);
		}
		(void) sprintf(target, "%s/%s", basedir, method_name);
	}

	return (target);

}


/*
 * Returns an array of strings containing the environment variables.
 * The values are computed for the first time only, then cached.
 */
char **
fe_set_env_vars(void)
{
	/*
	 * Suppress lint message about "union initialization"
	 * DEFAULTMUTEX is defined in /usr/include/synch.h
	 */
	/* CSTYLED */
	static mutex_t	env_vars_mutex = DEFAULTMUTEX; /*lint !e708 */
	static bool_t 	first_time = TRUE;
	static	char	**env_name;
	uint_t 		i, j;
	uint_t		num_env_vars;
	size_t		env_vars_size;
	sc_syslog_msg_handle_t handle;

	/*
	 * get lock to ensure that only 1 thread executes this section
	 */
	(void) mutex_lock(&env_vars_mutex);

	/*
	 * Set environment for method execution;
	 * only do it the first time, then cache it.
	 */
	if (first_time) {
		first_time = FALSE;

		/*
		 * We are explicitly setting each LC_* variable by
		 * querying for its current value through setlocale.
		 * We don't need to explicitly check LC_ALL because
		 * setlocale will first query LC_ALL, then the specific
		 * LC_ that we have specified.
		 *
		 * Getting the LC_MESSAGES from the environment
		 * here doesn't do much good in most cases because this daemon
		 * will be running in the default locale.
		 * Sometimes administrative commands tell the rgm
		 * what LC_MESSAGES value to use, which needs to be passed
		 * to the fed.
		 *
		 * Thus, we need to make sure the array is always the same
		 * length, with LC_MESSAGES in the same spot, so that calling
		 * functions can copy the array and overwrite the LC_MESSAGES
		 * explicitly when need be.
		 */

		num_env_vars = NUM_ENV_VARS;

		/*
		 * allocate array of appropriate size
		 */
		env_vars_size = (num_env_vars + 1) * sizeof (char *);
		env_name = (char **)malloc(env_vars_size);
		if (env_name == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The rgmd server was not able to allocate memory for
			 * the environment name, while trying to connect to
			 * the rpc.fed server, possibly due to low memory. An
			 * error message is output to syslog.
			 * @user_action
			 * Determine if the host is running out of memory. If
			 * not save the /var/adm/messages file. Contact your
			 * authorized Sun service provider to determine
			 * whether a workaround or patch is available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "in fe_set_env_vars malloc failed");
			sc_syslog_msg_done(&handle);
			(void) mutex_unlock(&env_vars_mutex);
			return (NULL);
		}
		(void) memset(env_name, 0, env_vars_size);

		env_name[0] = strdup("HOME=/");
		env_name[1] = strdup("PATH=/usr/bin:/usr/cluster/bin");
		env_name[2] = strdup("LD_LIBRARY_PATH=/usr/cluster/lib");

		env_name[3] = set_locale_category(LC_CTYPE, "LC_CTYPE");
		env_name[4] = set_locale_category(LC_NUMERIC, "LC_NUMERIC");
		env_name[5] = set_locale_category(LC_TIME, "LC_TIME");
		env_name[6] = set_locale_category(LC_COLLATE, "LC_COLLATE");
		env_name[7] = set_locale_category(LC_MONETARY, "LC_MONETARY");

		/*
		 * NOTE:
		 * LC_MESSAGES always needs to be the last one in the array,
		 * so it can be modified by clients of this function who need
		 * to set an explicit messages locale for the fed.
		 * It should always be at index NUM_ENV_VARS - 1.
		 *
		 * Clients will, of course, need to make a local copy of
		 * this array before modifying it for their own purposes.
		 */
		env_name[8] = set_locale_category(LC_MESSAGES, "LC_MESSAGES");

		/*
		 * check that all var names are non-null
		 */
		for (i = 0; i < num_env_vars; i++) {
			if (env_name[i] == NULL) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_FED_TAG, "");
				/*
				 * SCMSGS
				 * @explanation
				 * The rgmd server was not able to allocate
				 * memory for an environment variable, while
				 * trying to connect to the rpc.fed server,
				 * possibly due to low memory. An error
				 * message is output to syslog.
				 * @user_action
				 * Determine if the host is running out of
				 * memory. If not save the /var/adm/messages
				 * file. Contact your authorized Sun service
				 * provider to determine whether a workaround
				 * or patch is available.
				 */
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "in fe_set_env_vars malloc of "
				    "env_name[%d] failed", i);
				sc_syslog_msg_done(&handle);
				for (j = 0; j < num_env_vars; j++)
					free(env_name[i]);
				free(env_name);
				(void) mutex_unlock(&env_vars_mutex);
				return (NULL);
			}
		}

		if (debug) {
			dbg_msgout(NOGET("in fe_set_env_vars env_name:\n"));
			for (i = 0; i < num_env_vars; i++)
				dbg_msgout(NOGET("\t%s\n"), env_name[i]);
			dbg_msgout(NOGET("\tNULL\n"));
		}
	}
	/*
	 * release lock
	 */
	(void) mutex_unlock(&env_vars_mutex);

	return (env_name);
}

#if !DOOR_IMPL
static void
set_createrror(fe_run_result *result, char *host)
{
	char *clsp = NULL;
	sc_syslog_msg_handle_t handle;

	result->type = FE_CONNECT;
	/*
	 * cf_stat is set by the system, like errno.
	 * Suppress the lint info because lint doesn't understand
	 * it.
	 */
	result->sys_errno =
	    rpc_createerr.cf_stat; /*lint !e746 */
	clsp = clnt_spcreateerror(NOGET("security_clnt_connect"));

	(void) sc_syslog_msg_initialize(&handle,
	    SC_SYSLOG_RGM_FED_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * The rgm is not able to establish an rpc connection to the rpc.fed
	 * server on the host shown, and the error message is shown. An error
	 * message is output to syslog.
	 * @user_action
	 * Save the /var/adm/messages file. Contact your authorized Sun
	 * service provider to determine whether a workaround or patch is
	 * available.
	 */
	(void) sc_syslog_msg_log(&handle, LOG_ERR, MESSAGE,
	    "host %s failed: %s", host, clsp ? clsp :
	    "clnt_spcreateerror returned NULL");
	sc_syslog_msg_done(&handle);
}
#endif

#if DOOR_IMPL
static void
set_error(fe_run_result *result, char *action, int ret)
{
	sc_syslog_msg_handle_t handle;

	result->type = FE_ACCESS;
	result->sec_errno = SEC_EUNIXS;

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_FED_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * A client was not able to make an RPC or door connection to a server
	 * (rpc.pmfd, rpc.fed or rgmd) to execute the action shown. The error
	 * number is shown.
	 * @user_action
	 * Examine other syslog messages occurring at about the same time to
	 * see if the problem can be identified. Save the /var/adm/messages
	 * file. Contact your authorized Sun service provider to determine
	 * whether a workaround or patch is available.
	 */
	(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
	    "%s: Call failed, return code=%d", action, ret);
	sc_syslog_msg_done(&handle);
}
#else
static void
set_error(CLIENT *clnt, fe_run_result *result, char *action, enum clnt_stat ret)
{
	char *clsp = NULL;
	sc_syslog_msg_handle_t handle;

	result->type = FE_ACCESS;
	result->sec_errno = SEC_EUNIXS;

	clsp = clnt_sperror(clnt, action);
	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_FED_TAG, "");
	if (clsp != NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * A client was not able to make an rpc connection to a server
		 * (rpc.pmfd, rpc.fed or rgmd) to execute the action shown.
		 * The rpc error message is shown.
		 * @user_action
		 * Examine other syslog messages occurring at about the same
		 * time to see if the problem can be identified. Save the
		 * /var/adm/messages file. Contact your authorized Sun service
		 * provider to determine whether a workaround or patch is
		 * available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Call failed: %s", clsp);
	} else {
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "%s: Call failed, return code=%d", action, (int)ret);
	}
	sc_syslog_msg_done(&handle);
}
#endif


/*
 * Returns a string containing the environment variable
 * value on the current host for the locale category passed as arg.
 * "locale_cat_string" is the string representation of the enum
 * "locale_category".
 */
static char
*set_locale_category(uint_t locale_category, char *locale_cat_string)
{
	char *lc, *env_var, tmp_buff[2 * MAXNAMELEN];

	/*
	 * Get value of current locale category
	 * in which rgmd is running.
	 */
	lc = setlocale((int)locale_category, NULL);

	/*
	 * Set locale category to same value.
	 */
	if (lc == NULL)
		(void) snprintf(tmp_buff, (size_t)(2 * MAXNAMELEN), "%s=",
		    locale_cat_string);
	else
		(void) snprintf(tmp_buff, (size_t)(2 * MAXNAMELEN), "%s=%s",
		    locale_cat_string, lc);

	/* Copy the string */
	env_var = strdup(tmp_buff);

	return (env_var);
}

#if DOOR_IMPL

int fed_door_clnt(void *args,
    void *result, int api_name,
    client_handle *clnt)
{
	door_arg_t door_arg;

	/* XDR Streams for arguments and result */
	XDR xdrs, xdrs_result;

	/*
	 * Stores the marshalled arguments to be sent via
	 * door_call
	 */
	char *arg_buf = NULL;

	/*
	 * Container struct for storing the input arguments
	 * that are to be sent to the server via the door_call
	 */
	fed_input_args_t *fed_args = NULL;

	/* Stores the total size of the arguments being passed */
	size_t arg_size;

	/* Return code sent by the door server */
	int return_code;

	/* Number of times we want to try to encode the arguments */
	int num_retries = XDR_NUM_RETRIES;

	sc_syslog_msg_handle_t handle;

	/* Check for a valid client_handle before proceeding */
	if (clnt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred in the inter-process
		 * communication between cluster processes.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Invalid Client Handle");
		sc_syslog_msg_done(&handle);
		return (DOOR_INVALID_HANDLE);
	}

	/* Allocate memory for storing the arguments to be passed */
	fed_args = (fed_input_args_t *)
	    calloc(1, sizeof (fed_input_args_t));

	/* Prepare the container structure */
	fed_args->api_name = api_name;
	fed_args->data = args;

	/*
	 * Calculate the size of the arguments being passed
	 *
	 */
	arg_size = fed_xdr_sizeof(fed_args);

	for (num_retries = XDR_NUM_RETRIES; num_retries > 0; num_retries--) {
		/* Allocate the space to store marshalled data */
		arg_buf = (char *)calloc(1, arg_size);

		/* Initialize the XDR Stream for Encoding data */
		xdrmem_create(&xdrs, arg_buf, arg_size, XDR_ENCODE);

		/* Use xdr function to marshall the input arguments */
		if (!xdr_fed_input_args(&xdrs, fed_args)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * A non-fatal error occurred while rpc.fed was
			 * marshalling arguments for a remote procedure call.
			 * The operation will be re-tried with a larger
			 * buffer.
			 * @user_action
			 * No user action is required. If the message recurs
			 * frequently, contact your authorized Sun service
			 * provider to determine whether a workaround or patch
			 * is available.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "XDR FE Buffer Shortfall while encoding "
			    "arguments API num = %d, will retry", api_name);
			sc_syslog_msg_done(&handle);
			/*
			 * Will retry to encode the arguments
			 * this time with a larger buffer
			 */
			if (num_retries) {
				arg_size += EXTRA_BUFFER_SIZE;
				free(arg_buf);
				arg_buf = NULL;
				continue;
			}
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "XDR Error while encoding arguments.");
			sc_syslog_msg_done(&handle);
			return (XDR_ERROR_AT_CLIENT);
		} else {
			break; /* Successful Encoding */
		}
	}

	/* Prepare the door arguments before making the door call */
	door_arg.data_ptr = arg_buf;
	door_arg.data_size = xdr_getpos(&xdrs);
	door_arg.desc_ptr = NULL;
	door_arg.desc_num = 0;
	door_arg.rbuf = NULL;

	/*
	 * Even if we set this as null the door_return
	 * will set it to the correct size when the
	 * door server returns
	 */
	door_arg.rsize = 0;

	/* Make the door call */
	if (make_door_call(clnt->door_desc, &door_arg) < 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred in the inter-process
		 * communication between Sun Cluster processes. Related error
		 * messages might be found near this one in the syslog output.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to make door call.");
		sc_syslog_msg_done(&handle);
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "Error making door call");
		return (UNABLE_TO_MAKE_DOOR_CALL);
	}

	/*
	 * Now, we are back from the door_call and we need to unmarshall the
	 * data and get the return_code.
	 * But if the returned size is 0 and the rbuf = NULL, then we can be
	 * sure that a severe error has occured at the server and it has not
	 * been able to complete the call successfully.
	 */
	free(arg_buf); /* Freeing this since the door_call is complete */
	free(fed_args);

	/* Initialize the result XDR stream */
	xdrmem_create(&xdrs_result, door_arg.rbuf,
	    door_arg.rsize, XDR_DECODE);

	/* Check if the returned result and rsize are NULL */
	if ((door_arg.rbuf == NULL) && (door_arg.rsize == 0)) {
		return (XDR_ERROR_AT_SERVER);
	}

	/* Decode the return_code */
	if (!xdr_int(&xdrs_result, &return_code)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_FED_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "XDR Error while decoding return arguments.");
		sc_syslog_msg_done(&handle);
		return (XDR_ERROR_AT_CLIENT);
	} else {
		if (return_code != DOOR_CALL_SUCCESS) {
			/*
			 * Only if the return_code is DOOR_CALL_SUCCESS
			 */
			return (return_code);
		}
	}

	/* Now decode the other result data */
	if (!xdr_fed_result_args(&xdrs_result, result, api_name)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_SCHA_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "XDR Error while decoding return arguments.");
		sc_syslog_msg_done(&handle);
		return (XDR_ERROR_AT_CLIENT);
	}
	xdr_destroy(&xdrs);
	xdr_destroy(&xdrs_result);
	munmap(door_arg.rbuf, door_arg.rsize);
	return (DOOR_CALL_SUCCESS);
}

#endif
