#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)Makefile.com	1.44	08/05/20 SMI"
#
# Use is subject to license terms.
#
# lib/libfe/Makefile.com
#

LIBRARY= libfe.a
VERS= .1

include $(SRC)/Makefile.master

# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

OBJECTS = fe.o fe_clnt_xdr.o fe_door_xdr.o
$(RPC_IMPL)OBJECTS = fe.o fe_clnt.o fe_xdr.o

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

# include the macros for doors or rpc implementation again because
# Makefile.lib screws up CFLAGS/CPPFLAGS
include $(SRC)/Makefile.ipc 

RPCFILE =		$(ROOTCLUSTHDRDIR)/rgm/door/fe_door.x
$(RPC_IMPL)RPCFILE =		$(ROOTCLUSTHDRDIR)/rgm/rpc/fe.x

RPCGENFLAGS =		$(RPCGENMT) -C -K -1


RPCFILE_XDR =	../common/fe_door_xdr.c
RPCFILE_CLNT =
$(RPC_IMPL)RPCFILE_CLNT =	../common/fe_clnt.c
$(RPC_IMPL)RPCFILE_XDR =	../common/fe_xdr.c

RPCFILES =	$(RPCFILE_CLNT) $(RPCFILE_XDR)

CLOBBERFILES	+= $(RPCFILES)

# Don't check generated files, so don't use the OBJECTS.
CHECK_FILES	= fe.c_check

MAPFILE=	../common/mapfile-vers
SRCS =		$(OBJECTS:%.o=../common/%.c)

LINTFILES =	fe.ln

CLOBBERFILES += llib-lfe.ln lint.out

LIBS = $(DYNLIB)

MTFLAG = -mt

CPPFLAGS += $(MTFLAG)
LDLIBS	+= -lnsl -lc -lscutils -lsecurity -lclos
LDLIBS += $(DOOR_LDLIBS)

PMAP = -M $(MAPFILE)

.KEEP_STATE:

all:  $(RPCFILES) $(LIBS)
lint: $(LINTLIB)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
