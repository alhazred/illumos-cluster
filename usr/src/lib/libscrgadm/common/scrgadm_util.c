/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scrgadm_util.c	1.51	09/04/20 SMI"

/*
 *	scrgadm_util.c
 *
 *		support routines for scrgadm CLI & GUI
 */

#include <sys/types.h>
#include <ctype.h>
#include <limits.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/socket.h>		/* for AF_INET */
#include <netdb.h>
#include <arpa/inet.h>		/* for IP mappings */
#include <netinet/in.h>		/* for socket options */
#include <net/if.h>
#include <locale.h>
#include <errno.h>
#include <scadmin/scrgadm.h>
#include <rgm/pnm.h>
#include <rgm/rgm_common.h>
#include <rgm/rgm_cnfg.h>
#include <sys/sol_version.h>

#if (SOL_VERSION >= __s10)
#include <stdbool.h>
#include <libzccfg/libzccfg.h>
#endif

#define	FLG_VID			(1 << 0)
#define	FLG_VERSION		(1 << 1)
#define	FLG_VID_MATCHED		(1 << 2)
#define	FLG_VERSION_MATCHED	(1 << 3)

#define	SCRGADM_RT_MAXNAMELEN		MAXCCRTBLNMLEN

static boolean_t xip_zone(char *node, char *zone, int *err);
static scha_errmsg_t scrgadm_process_rtreg_results(char *rt_name,
    char *rt_file, int rtreg_status, RtrFileResult *rtr_results);
static scha_errmsg_t is_regular_file(char *filename);
static void rtrstring_to_string(RtrString *rtrstring, char *buffer,
    int bufflen);
static void split_rtname(char *rt_name, char **vidp, char **typep,
    char **versionp, char *buffer, int bufflen);
static void process_rtr_dir(const char *rtr_path,
    RtrFileResult **saved_rtr_results_p, int *ambiguous_p,
    uint_t *summary_flag_p, uint_t *saved_flag_p, const char *name,
    const char *vid, const char *version);
static scha_errmsg_t validate_unique_haip_pernode(
    scrgadm_hostname_list_t *hostlist, scrgadm_name_t node,
    char *zone);

scha_errmsg_t construct_adapterinfo_list(name_t rg_name,
    scrgadm_adapterinfo_list_t **ailist, char *zc_name);
scha_errmsg_t validate_netadapter_list(scrgadm_node_ipmp_list_t *,
    scrgadm_adapterinfo_list_t *ailist, char *zc_name);
scha_errmsg_t validate_hostname_list(scrgadm_hostname_list_t *);
scha_errmsg_t select_adapterinfo_by_hostname_list(
	scrgadm_adapterinfo_list_t *ailist,
	scrgadm_hostname_list_t *hnlist);
scha_errmsg_t select_adapterinfo_by_netadapter_list(
    scrgadm_adapterinfo_list_t *ailist,
    scrgadm_node_ipmp_list_t *nalist);
scha_errmsg_t validate_adapterinfo_list(
    scrgadm_adapterinfo_list_t *ailist);
void free_netadapter_list(scrgadm_node_ipmp_list_t *);
void free_pnm_adapter_instances(pnm_adapter_instances_t **list);
void free_adp(pnm_adapter_instances_t *hp,
    pnm_adapter_instances_t **list,
    pnm_adapterinfo_t **ailist);
scha_errmsg_t validate_adp_homogeneity(scrgadm_node_ipmp_list_t *,
    scrgadm_hostname_list_t *hnlist, char *zc_name);
void remove_dup_instance(pnm_adapterinfo_t *adp,
    pnm_adapterinfo_t **list);
boolean_t ipmp_list_dup_grp(scrgadm_node_ipmp_list_t *ipmp_list,
    char *node, char *zone, char *group);
scha_errmsg_t validate_netadapter_entry(scrgadm_node_ipmp_list_t *, char *);
boolean_t is_netadapter_found(scrgadm_node_ipmp_list_t *,
    scrgadm_adapterinfo_list_t *ailist);
scha_errmsg_t resolve_hostname_IP(scrgadm_hostname_list_t *);
adapter_host_status_t can_adp_host_hostname_list(
    pnm_adapter_instances_t *hp,
    scrgadm_hostname_list_t *hnlist);
void remove_adp_from_adapterinfolist(pnm_adapterinfo_t *adp,
    pnm_adapterinfo_t **ai_list);
boolean_t more_than_one_adp(pnm_adapterinfo_t *head);
scha_errmsg_t validate_nodepart(scrgadm_node_ipmp_list_t *, char *);
scha_errmsg_t map_IPv4_to_hostname(scrgadm_hostname_list_t *);
scha_errmsg_t map_IPv6_to_hostname(scrgadm_hostname_list_t *);
scha_errmsg_t map_hostname_to_IP(scrgadm_hostname_list_t *);
adapter_host_status_t can_v4inst_host_hostname_list(
    pnm_adapterinfo_t *adp,
    scrgadm_hostname_list_t *hnlist);
adapter_host_status_t can_v6inst_host_hostname_list(
    pnm_adapterinfo_t *adp,
    scrgadm_hostname_list_t *hnlist);
boolean_t subnet_match_v4(pnm_adapterinfo_t *adp,
    scrgadm_hostname_list_t *hn);
boolean_t subnet_match_v6(pnm_adapterinfo_t *adp,
    scrgadm_hostname_list_t *hn);
scha_errmsg_t parse_raw_adapt_node(char *, char *, char *);


/*
 * scrgadm_process_rtreg()
 *
 * Input: RT name and optional RT reg file path
 *
 * Action:
 *	resolve path to file if needed
 *	parse file and fill RtrFileResult
 *	if !SCHA_ERR_NOERR then scha_errmsg_t will hold eror string
 *
 * Caller must call scrgadm_free_rtrresult(RtrFileResult)
 *	or free(scha_errmsg_t err_msg)
 *
 * Output:  RtrFileResult or scha_errmsg_t
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_INTERNAL
 */
scha_errmsg_t
scrgadm_process_rtreg(char *rt_name, char *rt_file,
    RtrFileResult **rtr_resultsp, char *zc_name)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t tmp_status;
	char saved_filebuf[SCRGADM_RT_MAXNAMELEN];
	char errbuff[BUFSIZ];
	RtrFileResult *saved_rtr_results = (RtrFileResult *)0;
	uint_t summary_flag = 0;
	int ambiguous = 0;
	char copy_rt_name[SCRGADM_RT_MAXNAMELEN];
	char *vid;
	char *name;
	char *version;
	int rtreg_status;
	int notallowed = 0;
	Rtr_T *rtr_reg;

#if (SOL_VERSION >= __s10)
	char zc_root[MAXPATHLEN];
	char *restrict absolute_path;
	zc_dochandle_t		handle;
	int err = ZC_OK, libzccfg_err = ZC_OK;
#endif
	char zc_rtr_dir[MAXPATHLEN];
	char zc_rtr_opt_dir[MAXPATHLEN];

	/* I18N housekeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Other housekeeping */
	saved_filebuf[0] = '\0';

	/* Check arguments */
	if (rt_name == NULL) {
		status.err_code = SCHA_ERR_INVAL;
		return (status);
	}

	/* If rt file was given, make sure that we have a match */
	if (rt_file) {

		/* Make a copy of the rtreg file name to "saved_filebuf" */
		(void) strcpy(saved_filebuf, rt_file);

		/* If it is not a regular file, we are done */
		tmp_status = is_regular_file(saved_filebuf);
		if (tmp_status.err_code != SCHA_ERR_NOERR) {
			return (tmp_status);
		}

		/* Attempt to parse the rtreg file */
		rtreg_status = rtreg_parse(saved_filebuf, &saved_rtr_results);
		if (rtreg_status == RTR_STATUS_FILE_ERR ||
		    saved_rtr_results == NULL) {
			status.err_code = SCHA_ERR_INVAL;
			(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
			    "Unable to open or parse registration file %s."),
			    saved_filebuf);
			status.err_msg = strdup(errbuff);
			if (saved_rtr_results)
				scrgadm_free_rtrresult(saved_rtr_results);
			return (status);
		}

		/* Check for errors */
		status = scrgadm_process_rtreg_results(rt_name, saved_filebuf,
		    rtreg_status, saved_rtr_results);
		if (status.err_code != SCHA_ERR_NOERR) {
			if (saved_rtr_results)
				scrgadm_free_rtrresult(saved_rtr_results);
			return (status);
		}

		//
		// If we are registering for a zone cluster and
		// If the RTR file contents are intact
		// we need to verify the following :
		// If the RTR file was found inside SCRGADM_RTR_DIR
		//	We are good to go, just return without any checks
		// Else If the RTR file was found inside <zoneroot> path
		// 	RTR file does not contain
		// 	GLOBAL_ZONE property set to TRUE or SYSDEFINED_TYPE
		// 	is not set to LOGICAL_HOSTNAME (or) SHARED_ADDRESS
		//
#if (SOL_VERSION >= __s10)
		if (zc_name) {
			absolute_path = (char *)
				malloc(sizeof (char) * MAXPATHLEN);
			realpath(saved_filebuf, absolute_path);

			if (absolute_path == NULL) {
				if (saved_rtr_results)
					scrgadm_free_rtrresult
					    (saved_rtr_results);

				status.err_code = SCHA_ERR_INVAL;

				(void) sprintf(errbuff,
					dgettext(TEXT_DOMAIN,
				    "Cannot obtain absolute path "
				    "of resource type registration"
				    "file."));

				status.err_msg = strdup(errbuff);
				return (status);
			}

			if (strncmp(absolute_path, SCRGADM_RTR_DIR,
				strlen(SCRGADM_RTR_DIR))) {

				//
				// Get the zone root path corresponding
				// to the zone cluster specified
				//
				if ((handle = zccfg_init_handle()) == NULL) {
					sprintf(errbuff, dgettext(TEXT_DOMAIN,
					    "Failed to get zonepath for "
					    "zone cluster "
					    "%s."), zc_name);
					status.err_code = SCHA_ERR_INVAL;
					status.err_msg = strdup(errbuff);

					if (saved_rtr_results)
						scrgadm_free_rtrresult
						(saved_rtr_results);

					return (status);
				}

				if ((libzccfg_err = zccfg_get_handle(zc_name,
					handle))
						!= ZC_OK) {
					sprintf(errbuff, dgettext(TEXT_DOMAIN,
					    "Failed to get zonepath "
					    "for zone cluster "
					    "%s."), zc_name);
					status.err_code = SCHA_ERR_INVAL;
					status.err_msg = strdup(errbuff);

					if (saved_rtr_results)
						scrgadm_free_rtrresult
						(saved_rtr_results);

					return (status);
				}

				if ((err = zccfg_get_zonepath(handle, zc_root,
					sizeof (zc_root))) != ZC_OK) {
					sprintf(errbuff, dgettext(TEXT_DOMAIN,
					    "Failed to get zonepath for "
					    "zone cluster "
					    "%s."), zc_name);
					status.err_code = SCHA_ERR_INVAL;
					status.err_msg = strdup(errbuff);

					if (saved_rtr_results)
						scrgadm_free_rtrresult
						(saved_rtr_results);

					return (status);
				}

				zccfg_fini_handle(handle);

				//
				// Make sure the RTR file we got here is
				// under the Zone Root Path of the given
				// zone cluster. If not we have to error
				// out because the user specified RTR
				// file location is outside the zone
				// root path.
				//
				if (strncmp(absolute_path, zc_root,
					strlen(zc_root))) {
					sprintf(errbuff, dgettext(TEXT_DOMAIN,
					    "Resource registration file not "
					    "present under zone root path "
					    "%s."), zc_root);
					status.err_code = SCHA_ERR_INVAL;
					status.err_msg = strdup(errbuff);

					if (saved_rtr_results)
						scrgadm_free_rtrresult
						(saved_rtr_results);

					return (status);
				}

				//
				// RTR file given is not inside the
				// trusted location
				// (SCRGADM_RTR_DIR). Check if we are
				// allowed to register
				// this RTR file.
				//
				status = scrgadm_get_globalzone_in_file
					(saved_filebuf, &notallowed);

				if (status.err_code != SCHA_ERR_NOERR ||
					notallowed == 1) {
					//
					// RTR file has GLOBAL_ZONE flag set
					// or it is a SYSTEM
					// RT. Deny registration of this RT.
					//
					if (saved_rtr_results)
						scrgadm_free_rtrresult
						    (saved_rtr_results);

					status.err_code = SCHA_ERR_ACCESS;

					(void) sprintf(errbuff,
						dgettext(TEXT_DOMAIN,
					    "Cannot register network "
					    "address resource type or "
					    "resource types with Global_zone "
					    "property set to TRUE from a "
					    "custom location"));

					status.err_msg = strdup(errbuff);

					return (status);
				}

			}
		}
#endif
	/* Only rt name given, find the reg file */
	} else {
		uint_t saved_flag = 0;

		/* Split the rt_name into its possible three components */
		name = NULL;
		split_rtname(rt_name, &vid, &name, &version, copy_rt_name,
		    sizeof (copy_rt_name));
		if (name == NULL) {
			status.err_code = SCHA_ERR_INVAL;
			return (status);
		}

		//
		// If we find any RT file matching the given RT then
		// we can go ahead and register even when the registration
		// has to be done for a zone cluster
		//
		process_rtr_dir(SCRGADM_RTR_DIR, &saved_rtr_results,
		    &ambiguous, &summary_flag, &saved_flag, name, vid,
		    version);

		//
		// If we are registering RT for a zone cluster
		// skip searching for a RTR file inside
		// SCRGADM_RTR_OPT_DIR
		//
		if (!zc_name) {
			process_rtr_dir(SCRGADM_RTR_OPT_DIR, &saved_rtr_results,
			    &ambiguous, &summary_flag, &saved_flag, name, vid,
			    version);
		}

		//
		// If we did not get any results from scanning through the
		// SCRGADM_RTR_DIR and SCRGADM_RTR_OPT_DIR while registering
		// for a global zone then do nothing. More scanning is done if
		// if we are registering the resource type for a zone cluster
		//
		if (saved_rtr_results == NULL) {
			//
			// If we are registering for a zone cluster and
			// If we are here then we dont have a match for the
			// RT inside any of the trusted locations.
			//
			// If we have a match and the RTR file contents
			// are intact
			// we need to verify the following :
			// If the RTR file was found inside SCRGADM_RTR_DIR
			// We are good to go, just return without any checks
			// Else If the RTR file was found inside <zoneroot> path
			// RTR file does not contain
			// GLOBAL_ZONE property set to TRUE or SYSDEFINED_TYPE
			// is not set to LOGICAL_HOSTNAME (or) SHARED_ADDRESS
			//
			if (zc_name) {
#if (SOL_VERSION >= __s10)
				//
				// scan through the RTR files
				// inside the following
				// <zoneroot>/SCRGADM_RTR_DIR
				// <zoneroot>/SCRGADM_RTR_OPT_DIR
				//
				if ((handle = zccfg_init_handle()) == NULL) {
					sprintf(errbuff, dgettext(TEXT_DOMAIN,
					    "Failed to get zonepath for "
					    "zone cluster "
					    "%s."), zc_name);
					status.err_code = SCHA_ERR_INVAL;
					status.err_msg = strdup(errbuff);

					return (status);
				}

				if ((libzccfg_err = zccfg_get_handle(zc_name,
					handle))
						!= ZC_OK) {
					sprintf(errbuff, dgettext(TEXT_DOMAIN,
					    "Failed to get zonepath "
					    "for zone cluster "
					    "%s."), zc_name);
					status.err_code = SCHA_ERR_INVAL;
					status.err_msg = strdup(errbuff);

					return (status);
				}

				if ((err = zccfg_get_zonepath(handle, zc_root,
					sizeof (zc_root))) != ZC_OK) {
					sprintf(errbuff, dgettext(TEXT_DOMAIN,
					    "Failed to get zonepath for "
					    "zone cluster "
					    "%s."), zc_name);
					status.err_code = SCHA_ERR_INVAL;
					status.err_msg = strdup(errbuff);

					return (status);
				}

				zccfg_fini_handle(handle);

				sprintf(zc_rtr_dir, "%s/root%s",
					zc_root, SCRGADM_RTR_DIR);
				process_rtr_dir(zc_rtr_dir, &saved_rtr_results,
					&ambiguous, &summary_flag, &saved_flag,
					name, vid, version);

				sprintf(zc_rtr_opt_dir, "%s/root%s", zc_root,
					SCRGADM_RTR_OPT_DIR);
				process_rtr_dir(zc_rtr_opt_dir,
					&saved_rtr_results,
					&ambiguous, &summary_flag,
					&saved_flag, name, vid,
					version);

				if (saved_rtr_results == NULL) {
					//
					// Modify error message
					//
					(void) sprintf(errbuff,
						dgettext(TEXT_DOMAIN,
						"Registration file not "
						"found for \"%s\" in %s, "
						"%s or %s."), rt_name,
						SCRGADM_RTR_DIR,
						zc_rtr_dir, zc_rtr_opt_dir);

					status.err_code = SCHA_ERR_INVAL;
					status.err_msg = strdup(errbuff);

					return (status);
				}

				rtr_reg = saved_rtr_results->rt_registration;
				if (rtr_reg->globalzone == RTR_TRUE ||
					rtr_reg->sysdefined_type ==
					    RTR_LOGICAL_HOSTNAME ||
					rtr_reg->sysdefined_type ==
					    RTR_SHARED_ADDRESS) {

					status.err_code = SCHA_ERR_ACCESS;

					(void) sprintf(errbuff, dgettext
						(TEXT_DOMAIN,
					    "Cannot register network address "
					    "resource types or resource types "
					    "with Global_zone "
					    "property set to TRUE from "
					    "a custom location"));

					status.err_msg = strdup(errbuff);

					if (saved_rtr_results)
						scrgadm_free_rtrresult
						    (saved_rtr_results);

					return (status);
				}
#endif
			} else {
				status.err_code = SCHA_ERR_INVAL;
				(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
				    "Registration file not "
				    "found for \"%s\" in %s or "
				    "%s."), rt_name, SCRGADM_RTR_DIR,
				    SCRGADM_RTR_OPT_DIR);
				status.err_msg = strdup(errbuff);

				return (status);
			}

		}

		/* If ambiguous, we are done */
		if (ambiguous) {

			/* Would better overall qualification help? */
			if (vid == NULL && version == NULL &&
			    (summary_flag & FLG_VID) &&
			    (summary_flag & FLG_VERSION)) {
				(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
				    "Use a vendor ID prefix and version "
				    "suffix with resource type \"%s\"."),
				    rt_name);

			/* Would a version help? */
			} else if (version == NULL &&
			    (summary_flag & FLG_VERSION)) {
				(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
				    "Use a version suffix with resource "
				    "type \"%s\"."), rt_name);

			/* Would a vendor ID help? */
			} else if (vid == NULL &&
			    (summary_flag & FLG_VID)) {
				(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
				    "Use a vendor ID prefix with resource "
				    "type \"%s\"."), rt_name);

			/* It is likely that nothing will help! */
			} else {
				(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
				    "Duplicate registration "
				    "files for \"%s\" in %s and/or %s."),
				    rt_name, SCRGADM_RTR_DIR,
				    SCRGADM_RTR_OPT_DIR);
			}

			/* Finish up error processing */
			status.err_code = SCHA_ERR_INVAL;
			status.err_msg = strdup(errbuff);
			scrgadm_free_rtrresult(saved_rtr_results);
			return (status);
		}
	}

	/* We are good to go */
	if (saved_rtr_results) {
		*rtr_resultsp = saved_rtr_results;
	} else {
		status.err_code = SCHA_ERR_INVAL;
	}

	return (status);
}

/*
 * process_rtr_dir()
 *
 * Input:
 *	RT reg file path, name (plit into name, vendor id, and version)
 *
 * Action:
 *	Parse all files in the directory and fill RtrFileResult with
 *	the best match. Set the ambiguous flag if there are ambiguous
 *	matches. Set the summary and saved flags as appropriate.
 *
 * Caller must call scrgadm_free_rtrresult(RtrFileResult)
 *
 * Output:  (via out params)
 *	RtrFileResult and three vars representing ambiguous results and flags
 *
 * Returns:
 *	Nothing
 */
static void
process_rtr_dir(const char *rtr_path, RtrFileResult **saved_rtr_results_p,
    int *ambiguous_p, uint_t *summary_flag_p, uint_t *saved_flag_p,
    const char *name, const char *vid, const char *version)
{
	DIR *dirp = NULL;
	struct dirent *dentry = NULL;
	RtrFileResult *tmp_rtr_results = NULL;
	char tmp_filebuf[SCRGADM_RT_MAXNAMELEN];
	scha_errmsg_t tmp_status;
	int rtreg_status;
	Rtr_T *rtr_reg;
	char tmp_vid[SCRGADM_RT_MAXNAMELEN];
	char tmp_name[SCRGADM_RT_MAXNAMELEN];
	char tmp_version[SCRGADM_RT_MAXNAMELEN];
	uint_t tmp_flag;
	int count_saved;
	int count_tmp;

	/* Open default registration directory */
	dirp = opendir(rtr_path);
	if (dirp == NULL) {
		/*
		 * Just return here. The caller will generate the
		 * appropriate error code if all directories are
		 * not found.
		 */
		return;
	}

	/* Inspect each file in the registration directory */
	while ((dentry = readdir(dirp)) != NULL) {

		/* Free last results, if necessary */
		if (tmp_rtr_results) {
			scrgadm_free_rtrresult(tmp_rtr_results);
			tmp_rtr_results = NULL;
		}

		/* Skip standard entries */
		if (dentry->d_name == NULL ||
		    strcmp(dentry->d_name, ".") == 0 ||
		    strcmp(dentry->d_name, "..") == 0)
			continue;

		/* Construct complete file name */
		(void) sprintf(tmp_filebuf, "%s/%s",
		    rtr_path, dentry->d_name);

		/* Skip it if not a regular file */
		tmp_status = is_regular_file(tmp_filebuf);
		if (tmp_status.err_code != SCHA_ERR_NOERR) {
			if (tmp_status.err_msg)
				free(tmp_status.err_msg);
			continue;
		}

		/* Parse the file */
		tmp_rtr_results = NULL;
		rtreg_status = rtreg_parse(tmp_filebuf, &tmp_rtr_results);

		/*
		 * If there are errors, skip the file.
		 * The rt_file name (-f to scrgadm) must be
		 * given to get a report of syntax errors in
		 * an rtreg file.
		 */
		if (rtreg_status != RTR_STATUS_PARSE_OK ||
		    tmp_rtr_results == NULL ||
		    tmp_rtr_results->rt_registration == NULL)
			continue;

		/* Initialize tmp results variables */
		rtr_reg = tmp_rtr_results->rt_registration;

		rtrstring_to_string(&rtr_reg->vendor_id, tmp_vid,
		    sizeof (tmp_vid));
		rtrstring_to_string(&rtr_reg->rtname, tmp_name,
		    sizeof (tmp_name));
		rtrstring_to_string(&rtr_reg->version,
		    tmp_version, sizeof (tmp_version));

		/* Initialize tmp_flag */
		tmp_flag = 0;
		if (*tmp_vid) {
			tmp_flag |= FLG_VID;
		}
		if (*tmp_version) {
			tmp_flag |= FLG_VERSION;
		}

		/* Check for a match on rtname */
		if (*tmp_name == '\0' || (strcmp(tmp_name, name) != 0))
			continue;

		/* Check for a match on vendor id */
		if (vid) {
			if (*tmp_vid == '\0' || (strcmp(tmp_vid, vid) != 0)) {
				continue;
			}
			tmp_flag |= FLG_VID_MATCHED;
		}

		/* Check for a match on version */
		if (version) {
			if (*tmp_version == '\0' ||
			    (strcmp(tmp_version, version) != 0)) {
					continue;
			}
			tmp_flag |= FLG_VERSION_MATCHED;
		}

		/* Add result flags to summary result flags */
		*summary_flag_p |= (tmp_flag & (FLG_VID | FLG_VERSION));

		/*
		 * Check against any previous matches.
		 *
		 * If the saved results have more matches
		 * than the current results, skip the current.
		 *
		 * If the current results have more matches
		 * than the saved results, replace saved with current
		 * and clear the ambiguous flag.
		 *
		 * If the saved result and current results have
		 * the same number of matches, set the ambiguous flag.
		 */
		if (*saved_rtr_results_p != NULL) {
			count_saved = 0;
			if (*saved_flag_p & FLG_VID_MATCHED) {
				++count_saved;
			}
			if (*saved_flag_p & FLG_VERSION_MATCHED) {
				++count_saved;
			}

			count_tmp = 0;
			if (tmp_flag & FLG_VID_MATCHED) {
				++count_tmp;
			}
			if (tmp_flag & FLG_VERSION_MATCHED) {
				++count_tmp;
			}

			/* Do we have an ambiguous match? */
			if (count_saved == count_tmp) {
				++(*ambiguous_p);

				/* Current results provide a better match? */
			} else if (count_tmp > count_saved) {
				scrgadm_free_rtrresult(*saved_rtr_results_p);
				*saved_rtr_results_p = tmp_rtr_results;
				tmp_rtr_results = NULL;
				*saved_flag_p = tmp_flag;
				*ambiguous_p = 0;
			}

		} else {
			*saved_rtr_results_p = tmp_rtr_results;
			tmp_rtr_results = NULL;
			*saved_flag_p = tmp_flag;
		}
	}

	/* Cleanup */
	(void) closedir(dirp);

	if (tmp_rtr_results) {
		scrgadm_free_rtrresult(tmp_rtr_results);
		tmp_rtr_results = NULL;
	}
}

/*
 * scrgadm_process_rtreg_results()
 *
 * Input: rt_name, rt_file, status from call to rtreg_parse(),
 *		and ptr to RtrFileResult struct from call to rtreg_parse()
 *
 * Action: Check for errors and reflect them in the results
 *
 * Output: scha_errmsg_t
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_INTERNAL
 */
static scha_errmsg_t
scrgadm_process_rtreg_results(char *rt_name, char *rt_file, int rtreg_status,
    RtrFileResult *rtr_results)
{
	scha_errmsg_t status = {SCCONF_NOERR, NULL};
	char errbuff[BUFSIZ];
	char *msg;
	char copy_rt_name[SCRGADM_RT_MAXNAMELEN];
	char *vid;
	char *name;
	char *version;
	Rtr_T *rtr_reg;
	char results_vid[SCRGADM_RT_MAXNAMELEN];
	char results_name[SCRGADM_RT_MAXNAMELEN];
	char results_version[SCRGADM_RT_MAXNAMELEN];

	/* I18N housekeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Check arguments */
	if (rt_name == NULL || rt_file == NULL || rtr_results == NULL) {
		status.err_code = SCHA_ERR_INVAL;
		return (status);
	}

	/* Check for parsing errors */
	if (rtreg_status != RTR_STATUS_PARSE_OK) {
		msg = NULL;
		switch (rtreg_status) {
		case RTR_STATUS_PARSE_ERR:
			status.err_code = SCHA_ERR_INVAL;
			msg = dgettext(TEXT_DOMAIN,
			    "Syntax error "
			    "in registration file at line:");
			break;

		case RTR_STATUS_RTNAME_ERR:
			status.err_code = SCHA_ERR_INVAL;
			msg = dgettext(TEXT_DOMAIN,
			    "Resource type name error "
			    "in registration file at line:");
			break;

		case RTR_STATUS_INVALID_PROPVAL:
			status.err_code = SCHA_ERR_INVAL;
			msg = dgettext(TEXT_DOMAIN,
			    "Invalid property value "
			    "in registration file at line:");
			break;

		case RTR_STATUS_INVALID_SYSDEF:
			status.err_code = SCHA_ERR_INVAL;
			msg = dgettext(TEXT_DOMAIN,
			    "Invalid system defined property "
			    "in registration file at line:");
			break;

		case RTR_STATUS_INVALID_ATTR:
			status.err_code = SCHA_ERR_INVAL;
			msg = dgettext(TEXT_DOMAIN,
			    "Invalid param table attribute "
			    "in registration file at line:");
			break;

		case RTR_STATUS_INVALID_ATTRVAL:
			status.err_code = SCHA_ERR_INVAL;
			msg = dgettext(TEXT_DOMAIN,
			    "Invalid param table attribute value "
			    "in registration file at line:");
			break;

		case RTR_STATUS_INVALID_PARAM:
			status.err_code = SCHA_ERR_INVAL;
			msg = dgettext(TEXT_DOMAIN,
			    "Invalid param table entry "
			    "in registration file at line:");
			break;

		case RTR_STATUS_INVALID_PROP:
			status.err_code = SCHA_ERR_INVAL;
			msg = dgettext(TEXT_DOMAIN,
			    "Invalid or missing property "
			    "in registration file at line:");
			break;

		case RTR_STATUS_INTERNAL_ERR:
			status.err_code = SCHA_ERR_INTERNAL;
			msg = dgettext(TEXT_DOMAIN,
			    "Internal error "
			    "in registration file at line:");
			break;

		default:
			/* Unknown error.  Give generic message */
			status.err_code = SCHA_ERR_INVAL;
			msg = dgettext(TEXT_DOMAIN,
			    "Error "
			    "in registration file at line:");
			break;

		} /* switch */

		if (msg != NULL) {
			(void) sprintf(errbuff, "%s %d\n%s\t%.15s\n",
			    msg, rtr_results->error_lineno,
			    dgettext(TEXT_DOMAIN, "Near:"),
			    rtr_results->error_bufloc);
			status.err_msg = strdup(errbuff);
		}

		return (status);
	}

	/*
	 * Check for a good match to the rt name
	 */

	/* Make sure that there is registration data */
	if (rtr_results->rt_registration == NULL) {
		status.err_code = SCHA_ERR_INVAL;
		(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
		    "No registration data in %s."), rt_file);
		status.err_msg = strdup(errbuff);
		return (status);
	}

	/* Split the rt_name into it's possible three components */
	name = NULL;
	split_rtname(rt_name, &vid, &name, &version, copy_rt_name,
	    sizeof (copy_rt_name));
	if (name == NULL) {
		status.err_code = SCHA_ERR_INVAL;
		return (status);
	}

	/* Initialize results variables */
	rtr_reg = rtr_results->rt_registration;

	rtrstring_to_string(&rtr_reg->vendor_id,
	    results_vid, sizeof (results_vid));
	rtrstring_to_string(&rtr_reg->rtname,
	    results_name, sizeof (results_name));
	rtrstring_to_string(&rtr_reg->version,
	    results_version, sizeof (results_version));

	/* Check for a match on rt name */
	if (*results_name == '\0' || (strcmp(name, results_name) != 0)) {
		status.err_code = SCHA_ERR_INVAL;
		(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
		    "%s is not the rt_reg file for \"%s\"."),
		    rt_file, rt_name);
		status.err_msg = strdup(errbuff);
		return (status);
	}

	/* Check for a match on vendor ID */
	if (vid) {
		if (*results_vid) {
			if (strcmp(vid, results_vid) != 0) {
				status.err_code = SCHA_ERR_INVAL;
				(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
				    "\"%s\" does not match the VENDOR_ID "
				    "setting in %s."), vid, rt_file);
				status.err_msg = strdup(errbuff);
				return (status);
			}
		} else {
			status.err_code = SCHA_ERR_INVAL;
			(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
			    "There is no VENDOR_ID in %s."), rt_file);
			status.err_msg = strdup(errbuff);
			return (status);
		}
	}

	/* Check for a match on RT version */
	if (version) {
		if (*results_version) {
			if (strcmp(version, results_version) != 0) {
				status.err_code = SCHA_ERR_INVAL;
				(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
				    "\"%s\" does not match the RT_VERSION "
				    "setting in %s."), version, rt_file);
				status.err_msg = strdup(errbuff);
				return (status);
			}
		} else {
			status.err_code = SCHA_ERR_INVAL;
			(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
			    "There is no RT_VERSION in %s."), rt_file);
			status.err_msg = strdup(errbuff);
			return (status);
		}
	}

	/* Print warning if NRU tunablity was changed */
	if (rtr_results->nru_tunablity_changed) {
		(void) sprintf(errbuff, dgettext(TEXT_DOMAIN,
		    "Notice: Tunablity of Network_resources_used for this "
		    " resource type is changed to ANYTIME"));
		status.err_msg = strdup(errbuff);
	}
	/* Done */
	return (status);
}

/*
 * is_regular_file()
 *
 * Input: filename
 *
 * Action: stat filename: is regular file? readable?
 *
 * Output: scha_errmsg_t
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 */
static scha_errmsg_t
is_regular_file(char *filename)
{
	scha_errmsg_t status = {SCCONF_NOERR, NULL};
	char errbuf[BUFSIZ];
	struct stat stat_buf;

	if (stat(filename, &stat_buf) == -1) {
		status.err_code = SCHA_ERR_INVAL;
		(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
		    "Unable to find registration file: %s"), filename);
		status.err_msg = strdup(errbuf);

	/* not a regular file */
	} else if ((stat_buf.st_mode & S_IFREG) == 0) {
		status.err_code = SCHA_ERR_INVAL;
		(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
		    "Registration file must be a regular file."));
		status.err_msg = strdup(errbuf);
	}

	return (status);

}

/*
 * scrgadm_free_rtrresult()
 *
 * Input: RtrFileResult*
 *
 * Action: free the data structure created by scrgadm_process_rtreg()
 *
 * Output: none
 *
 * Returns: nothing
 */
void
scrgadm_free_rtrresult(RtrFileResult *rtr_results)
{
	if (rtr_results)
		delete_RtrFileResult(rtr_results);
}

/*
 * rtrstring_to_string()
 *
 * Input:	Pointer to an RtrString, pointer to a character buffer,
 *		and the length of the buffer.
 *
 * Action:	Copy the string from RtrString to the character buffer,
 *		and add a terminating null.   If the RTR_Value of
 *		the RtrString is not RTR_STRING, the null string is
 *		returned in the character buffer.  If the buffer
 *		is not long enough, the null string is returned
 *		in the character buffer.
 *
 * Output:	Upon success, the character buffer is filled a null
 *		terminated copy of the string from RtrString.
 *
 * Returns:	nothing
 */
static void
rtrstring_to_string(RtrString *rtrstring, char *buffer, int bufflen)
{
	/* Check arguments */
	if (buffer == NULL || bufflen < 1)
		return;

	/* Anything to return? */
	if (rtrstring == NULL ||
	    rtrstring->value != RTR_STRING ||
	    rtrstring->len == 0 || rtrstring->start == 0 ||
	    (rtrstring->len + 1) > bufflen) {
		*buffer = '\0';
		return;
	}

	(void) strncpy(buffer, rtrstring->start, (uint_t)rtrstring->len);
	buffer[rtrstring->len] = '\0';
}

/*
 * split_rtname()
 *
 * Input:	The RT name, location to place pointer to "vendor ID"
 *		portion of the RT name, location to place the "type"
 *		portion of the RT name, location to place the "version"
 *		portion of the RT name, a buffer to work in, and the
 *		length of the buffer.
 *
 * Action:	Copy the "rt_name" into the supplied "buffer".  Then,
 *		using the copy in the buffer, split the name into
 *		it's three possible components, vendor ID prefix,
 *		resource type, and version suffix.   Pointers to
 *		these null-terminated strings are returned in
 *		"vidp", "typep", and "versionp", respectively.
 *
 *		If there is an error or if the buffer is not large enough,
 *		"vidp", "typep", and "versionp" will all be set to NULL.
 *
 *		If vendor ID prefix or version suffix are not part of
 *		the "rt_name", "vidp" and/or "versionp" will be set to
 *		NULL, respectively.
 *
 * Output:	Upon success, "typep" will point to the resource type
 *		name.   If there is a vendor ID prefix in the "rt_name",
 *		"vidp" will be set to point to a copy of it.   If there
 *		is a version suffix in the "rt_name", "versionp"
 *		will be set to point to a copy of it.
 *
 *		The supplied "buffer" is used as a working buffer for
 *		storing the "type", "rt_name", and "vid".   No attempt
 *		should be made by the caller to free these pointers;
 *		the caller may free the entire "buffer", if necessary.
 *
 * Returns:	nothing
 */
static void
split_rtname(char *rt_name, char **vidp, char **typep, char **versionp,
    char *buffer, int bufflen)
{
	char *s;

	/* Check arguments */
	if (rt_name == NULL || vidp == NULL || typep == NULL ||
	    versionp == NULL || buffer == NULL || bufflen < 1)
		return;

	/* Initialize */
	*vidp = (char *)0;
	*typep = (char *)0;
	*versionp = (char *)0;
	*buffer = '\0';

	/* Make sure that the buffer is large enough */
	if (((int)strlen(rt_name) + 1) > bufflen)
		return;

	/* Make a copy of the rt name into the buffer */
	(void) strcpy(buffer, rt_name);

	/*
	 * Split the rt_name into it's possible three components.
	 *
	 * Change "<vid>.<type>:<version>\0" to "<vid>\0<type>\0<version>\0",
	 * and set pointers to start of each string.
	 */

	/* version starts at ':' plus 1;  null-terminate the type */
	if ((s = strchr(buffer, ':')) != NULL) {
		*versionp = s + 1;
		*s = '\0';
	}

	/* type starts at '.' plust 1;  null-terminate the vid */
	if ((s = strchr(buffer, '.')) != NULL) {
		*vidp = buffer;
		*typep = s + 1;
		*s = '\0';
	} else {
		*typep = buffer;
	}
}

/*
 * scrgadm_rdep_list_to_string
 *
 *      Return a single string for the list of resource dependency names.
 *      The caller is responsible for freeing the list of names.
 */
char *
scrgadm_rdep_list_to_string(rdeplist_t *r_dependencies, char *null_str,
    char *separator)
{
	rdeplist_t *r_dep;
	size_t len = 0;
	char *string;
	const char *local_node = gettext("{local_node}");
	const char *any_node = gettext("{any_node}");

	/* Get the length required for the string buffer. */
	for (r_dep = r_dependencies; r_dep; r_dep = r_dep->rl_next) {
		if (r_dep->rl_name && *r_dep->rl_name) {
			len += strlen(r_dep->rl_name) + 1;
		} else {
			len += sizeof (null_str) + 1;
		}

		switch (r_dep->locality_type) {
		case LOCAL_NODE:
			len += strlen(local_node);
			break;
		case ANY_NODE:
			len += strlen(any_node);
			break;
		case FROM_RG_AFFINITIES:
		default:
			break;
		}
	}

	/* Allocate the string buffer */
	if ((string = (char *)calloc(1, len)) == NULL) {
		return ((char *)0);
	}

	/*
	 * Add values to the string buffer. The string buffer contains the
	 * list of suffixed qualifier with the dependencies name.
	 */
	for (r_dep = r_dependencies; r_dep; r_dep = r_dep->rl_next) {
		/* trailing space */
		if (*string != '\0')
			(void) strcat(string, separator);

		/* name (or NULL) */
		if (r_dep->rl_name && *r_dep->rl_name) {
			(void) strcat(string, r_dep->rl_name);
		} else {
			(void) strcat(string, null_str);
		}

		switch (r_dep->locality_type) {
		case LOCAL_NODE:
			(void) strcat(string, local_node);
			break;
		case ANY_NODE:
			(void) strcat(string, any_node);
			break;
		case FROM_RG_AFFINITIES:
		default:
			break;
		}
	}

	return (string);
}

/*
 * Fetch type name by looking into the rtr file.
 *
 * Output: There should be enough space in full_rtname, the resourcetype
 *	   name is copied there.
 *
 * Returns: SCHA_ERR_NOERR
 *	    SCHA_ERR_INVAL
 */
scha_errmsg_t
scrgadm_get_type_in_file(char *rtrfile, char *full_rtname)
{
	char vid[SCRGADM_RT_MAXNAMELEN];
	char vers[SCRGADM_RT_MAXNAMELEN];
	char rtname[SCRGADM_RT_MAXNAMELEN];
	RtrFileResult *tmp_rtr_results = (RtrFileResult *)0;
	Rtr_T *rtr_reg;
	int rtreg_status;
	scha_errmsg_t status = {SCCONF_NOERR, NULL};
	char saved_filebuf[SCRGADM_RT_MAXNAMELEN];

	status.err_code = SCHA_ERR_NOERR;
	rtreg_status = rtreg_parse(rtrfile, &tmp_rtr_results);
	if ((rtreg_status != RTR_STATUS_PARSE_OK) ||
	    (tmp_rtr_results == (RtrFileResult *)0) ||
	    (tmp_rtr_results->rt_registration == NULL)) {
		/* We know we can't use this file, try to get detailed error */
		saved_filebuf[0] = '\0';
		status = scrgadm_process_rtreg_results("Invalid_rt",
		    saved_filebuf, rtreg_status, tmp_rtr_results);
		if (tmp_rtr_results)
			scrgadm_free_rtrresult(tmp_rtr_results);
		return (status);
	}

	rtr_reg = tmp_rtr_results->rt_registration;
	rtrstring_to_string(&rtr_reg->vendor_id, vid, sizeof (vid));
	rtrstring_to_string(&rtr_reg->rtname, rtname, sizeof (rtname));
	rtrstring_to_string(&rtr_reg->version, vers, sizeof (vers));

	if (*vid) {
		(void) strcat(full_rtname, vid);
		(void) strcat(full_rtname, ".");
	}
	(void) strcat(full_rtname, rtname);
	if (*vers) {
		(void) strcat(full_rtname, ":");
		(void) strcat(full_rtname, vers);
	}

	if (tmp_rtr_results)
		scrgadm_free_rtrresult(tmp_rtr_results);

	return (status);
}

/*
 * Sets zc_allowed parameter to 1 if either the GLOBAL_ZONE property
 * is set to TRUE or if the RTR File belongs to a network address
 * resource type
 * Otherwise zc_allowed is set to 0
 */
scha_errmsg_t
scrgadm_get_globalzone_in_file(char *rtrfile, int *zc_allowed)
{
	char rtname[SCRGADM_RT_MAXNAMELEN];
	RtrFileResult *tmp_rtr_results = (RtrFileResult *)0;
	Rtr_T *rtr_reg;
	int rtreg_status;
	scha_errmsg_t status = {SCCONF_NOERR, NULL};
	char saved_filebuf[SCRGADM_RT_MAXNAMELEN];

	*zc_allowed = 0;
	status.err_code = SCHA_ERR_NOERR;
	rtreg_status = rtreg_parse(rtrfile, &tmp_rtr_results);
	if ((rtreg_status != RTR_STATUS_PARSE_OK) ||
	    (tmp_rtr_results == (RtrFileResult *)0) ||
	    (tmp_rtr_results->rt_registration == NULL)) {
		/* We know we can't use this file, try to get detailed error */
		saved_filebuf[0] = '\0';
		status = scrgadm_process_rtreg_results("Invalid_rt",
		    saved_filebuf, rtreg_status, tmp_rtr_results);
		if (tmp_rtr_results)
			scrgadm_free_rtrresult(tmp_rtr_results);
		return (status);
	}

	rtr_reg = tmp_rtr_results->rt_registration;

	/*
	 * Check if the GLOBAL_ZONE property is set to TRUE
	 * or check if the SYSDEFINED_TYPE property is set to
	 * either LogicalHostname or SharedAddress
	 */
	if (rtr_reg->globalzone == RTR_TRUE ||
		rtr_reg->sysdefined_type == RTR_LOGICAL_HOSTNAME ||
		rtr_reg->sysdefined_type == RTR_SHARED_ADDRESS) {

		*zc_allowed = 1;
		status.err_code = SCHA_ERR_ACCESS;
	} else {
		*zc_allowed = 0;
	}

	if (tmp_rtr_results)
		scrgadm_free_rtrresult(tmp_rtr_results);

	return (status);
}

/*
 * process_netadapter_list()
 *
 * Perform validation of netadapter list (-n option) against hostname list
 * (-l option) for LHRS and SARS using adapterinfo gathered from the PNM
 * daemon.
 *
 *	- gather adapterinfo on all RG nodes
 *		(see construct_adapterinfo_list())
 *	- use the -l argument to truncate this list, so that only
 *		adapters that can host all hostnames remain
 *		(see select_adapterinfo_by_hostname_list())
 *	- use the (optional) -n argument to further truncate
 *		this list, to allow validation of the -n argument
 *		(see select_adapterinfo_by_netadapter_list())
 *	- check remaining list. Each node should have exactly
 *		one adapter or one ipmp group left by now.
 *		(see validate_adapterinfo_list())
 *
 * On success, the returned ailist contains only adapters that are capable
 * of hosting all the hostnames. Singleton ipmp groups are NOT created in
 * this routine; use convert_adapterinfo_to_ipmp() for that.
 *
 * Return:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_NOMEM
 */
scha_errmsg_t
process_netadapter_list(const scrgadm_name_t rg_name,
    scrgadm_hostname_list_t *lhnames,
    scrgadm_node_ipmp_list_t *lipmp,
    scrgadm_adapterinfo_list_t **ailistp,
    char *zc_name)
{
	scha_errmsg_t			status = {SCHA_ERR_NOERR, NULL};
	scrgadm_adapterinfo_list_t	*ailist = NULL;

	status = construct_adapterinfo_list(rg_name, &ailist, zc_name);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	status = validate_netadapter_list(lipmp, ailist, zc_name);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	status = validate_hostname_list(lhnames);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	status = select_adapterinfo_by_hostname_list(ailist, lhnames);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	status = select_adapterinfo_by_netadapter_list(ailist, lipmp);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	status = validate_adapterinfo_list(ailist);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto clean_up;
	}

	*ailistp = ailist;
	return (status);

clean_up:

	free_adapterinfo_list(ailist);
	*ailistp = NULL;
	return (status);
}

/*
 * validate_unique_haip()
 * This function is used to validate uniqueness of the LogicalHostname
 * or Shared Address to be added in the cluster.
 *
 */
scha_errmsg_t
validate_unique_haip(scrgadm_hostname_list_t *hostlist)
{
	scconf_errno_t			scconf_error = SCCONF_NOERR;
	scha_errmsg_t 			status = {SCHA_ERR_NOERR, NULL};
	scconf_cfg_cluster_t 		*pcluster_cfg = NULL;
	uint_t 				ismember = 0;
	scconf_cfg_node_t 		*pnode_cfg = NULL;
	scrgadm_name_t			node;
	char				**zonelist = (char **)0;
	char				**zl = (char **)NULL;
	int				i;
	boolean_t			cl_zone_flag;

	/* make sure that we are active in the cluster */
	scconf_error = scconf_ismember(0, &ismember);
	status = scconf_err_to_scha_err(scconf_error);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto cleanup;
	}

	if (!ismember) {
		status.err_code = SCHA_ERR_MEMBER;
		goto cleanup;
	}

	/*
	 * Get all cluster configuration. Will extract all cluster  node names
	 * from this configuration very soon
	 */
	scconf_error = scconf_get_clusterconfig(&pcluster_cfg);
	status = scconf_err_to_scha_err(scconf_error);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto cleanup;
	}

	/*
	 * Iterate through the list of cluster 'all' cluster nodes
	 * irrespective of member of the resource group
	 */
	pnode_cfg = pcluster_cfg->scconf_cluster_nodelist;
	while (pnode_cfg != NULL) {

		node = strdup(pnode_cfg->scconf_node_nodename);
		if (node == NULL) {
			status.err_code = SCHA_ERR_NOMEM;
			goto cleanup;
		}
		status = validate_unique_haip_pernode(hostlist, node, NULL);
		if (status.err_code != SCHA_ERR_NOERR) {
			free(node);
			goto cleanup;
		}

		status = validate_unique_haip_pernode(hostlist, node, NULL);
		if (status.err_code != SCHA_ERR_NOERR) {
			free(node);
			goto cleanup;
		}

		cl_zone_flag = B_FALSE;
#if SOL_VERSION >= __s10
		(void) scconf_zone_cluster_check(&cl_zone_flag);
#endif

		/* Do it for excl ip zones too */
		if (!cl_zone_flag &&
			scconf_get_xip_zones(node, &zonelist) == SCCONF_NOERR) {
			for (zl = zonelist, i = 0; zl[i]; i++) {
				status = validate_unique_haip_pernode(hostlist,
				    node, zl[i]);
				if (status.err_code != SCHA_ERR_NOERR) {
					free(node);
					goto cleanup;
				}
			}
		}

		pnode_cfg = pnode_cfg->scconf_node_next;
		free(node);

	}	/* Loop over all cluster nodes */

cleanup:

	if (pcluster_cfg != NULL)
		scconf_free_clusterconfig(pcluster_cfg);
	if (zonelist)
		scconf_free_zones(zonelist);
	return (status);
}

/*
 * validate_unique_haip_pernode()
 * Implement validate_unique_haip_pernode() for a node or zone.
 *
 */
static scha_errmsg_t
validate_unique_haip_pernode(scrgadm_hostname_list_t *hostlist,
    scrgadm_name_t node, char *zone)
{
	scha_errmsg_t 			status = {SCHA_ERR_NOERR, NULL};
	pnm_adapterinfo_t		*ipl = NULL, *p = NULL;
	scrgadm_hostname_list_t 	*ptr;
	struct in6_addr			ina;
	struct in_addr			*p4;
	char				errbuff[BUFSIZ];
	struct in6_addr			*ip6addr;
	uint64_t			inx;
	int				pnm_ret;

	p4 = (struct in_addr *)malloc(sizeof (struct in_addr *));

	ptr = hostlist;

	/*
	 * Get actual adapter info from PNM
	 *
	 * Access pnmd on the node using the private interconnect name
	 */
	if (zone)
		pnm_ret = pnm_zone_init(node, zone);
	else
		pnm_ret = pnm_init(node);

	if (pnm_ret == 0) {
		if (pnm_adapterinfo_list(&ipl, B_TRUE) != 0) {
			/*
			 * We don't return error here; rather,
			 * we skip the node silently, as
			 * the node could be down. PNM prints
			 * error in syslog in such cases.
			 * We can't abort the process here
			 * because this node may not belong
			 * to the resource group. So the command
			 * should not fail if we can't contact
			 * a non-member node. Only, we can't
			 * be able to validate in this node.
			 */
			pnm_fini();
			goto cleanup;
		}
	} else {
		goto cleanup;
	}
	pnm_fini();

	/*
	 * Assuming resolve_hostname_IP() is already called
	 * through process_netadapter_list() and
	 * validate_hostname_list, so the ip_addr/ip6_addr
	 * field is already populated
	 *
	 * Loop over list of user supplied hostnames with
	 * -l option to become Logical Hostname or Shared Address
	 */

	while (ptr != NULL) {
		if (ptr->ip_addr != NULL) {
			/*
			 * v4 address is converted to v4mapped
			 * address to make it comparable
			 */
			bzero(p4, sizeof (p4));
			p4->s_addr = *(ptr->ip_addr);
			IN6_INADDR_TO_V4MAPPED(p4, &ina);
		} else if (ptr->ip6_addr != NULL) {
			bcopy(ptr->ip6_addr, &ina, sizeof (in6_addr_t));
		} else {
			/* error condition - neither v4 nor v6 */
			status.err_code = SCHA_ERR_INTERNAL;
			goto cleanup;
		}

		/*
		 * Loop over list of adapters present in each node
		 * of the cluster. The list is retrieved from
		 * function call pnm_ipaddr_list()
		 */
		p = ipl;
		inx = 0;
		while (p != NULL) {
			ip6addr = &(p->adp_addrs.adp_ipaddr[inx++]);
			if (IN6_ARE_ADDR_EQUAL(ip6addr, &ina)) {
				/*
				 * this buf is good enough for
				 * ipv4 addrs too
				 */
				char ipa[INET6_ADDRSTRLEN];
				const char *ipstr;

				ipstr = ptr->ip_addr != NULL ?
				    inet_ntop(AF_INET, ptr->ip_addr,
				    ipa, sizeof (ipa)) :
				    inet_ntop(AF_INET6,
				    ptr->ip6_addr, ipa, sizeof (ipa));
				if (zone)
					(void) sprintf(errbuff,
					    gettext("IP Address %s is "
					    "already plumbed on zone %s "
					    "at host: %s"),
					    ipstr == NULL ? "NULL" :ipstr,
					    zone, node);
				else
					(void) sprintf(errbuff,
					    gettext("IP Address %s is "
					    "already plumbed at host: %s"),
					    ipstr == NULL ? "NULL" :ipstr,
					    node);
				status.err_code = SCHA_ERR_INVAL;
				status.err_msg = strdup(errbuff);
				goto cleanup;
			}	/* if IN6_ARE_ADDR_EQUAL */
			if (inx == p->adp_addrs.num_addrs) {
				p = p->next;
				inx = 0;
			}

		}	/* Loop over the list of adapters */
		ptr = ptr->next;
	}	/* Loop over list of user supplied list of hostnames */

cleanup:

	free(p4);
	if (ipl)
		pnm_adapterinfo_free(ipl);
	return (status);
}

/*
 * convert_adapterinfo_to_ipmp()
 *
 * Input: scrgadm_adapterinfo_list_t *ailist
 *	  scrgadm_hostname_list_t *lhnames
 *
 * Output: scrgadm_node_ipmp_list_t **nnlist
 *
 * Action: A new scrgadm_node_ipmp_list_t is created, whose elements all have a
 *	non-NULL ipmp_group field. The input list *nnlist is freed upon
 *	successful creation of the new list. Every nodes' ai_list will have
 *	at most 1 adapter. It could have two instances of the same adp though.
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Return:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 *	SCHA_ERR_INVAL
 */
scha_errmsg_t
convert_adapterinfo_to_ipmp(scrgadm_adapterinfo_list_t *ailist,
	scrgadm_node_ipmp_list_t **nnlist, scrgadm_hostname_list_t *lhnames,
	char *zc_name)
{
	char				errbuff[BUFSIZ] = { 0 };
	scha_errmsg_t			status = {SCHA_ERR_NOERR, NULL};
	scrgadm_adapterinfo_list_t	*ai;
	scrgadm_node_ipmp_list_t	*nn;
	scrgadm_node_ipmp_list_t	*nnhead, *nnlast;
	pnm_adapterinfo_t		*adp;
	boolean_t			auto_create = B_TRUE;
	int				sock;
	in_addr_t			myaddr;

	/*
	 * Try to set the new sock option IP_DONTFAILOVER_IF - if this works
	 * then we know that we can support auto-create. bugid 4511634.
	 */
	sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	myaddr = inet_addr("127.0.0.1");

/*
 * We want to provide auto_create to customers and since our ref_proto area
 * is not S9U2 (in which we have #define IP_DONTFAILOVER_IF 0x44) we need to
 * define our own. This part will be removed once we move our ref_proto area
 * to S9U2.
 */
#ifndef IP_DONTFAILOVER_IF
#define	IP_DONTFAILOVER_IF	0x44
#endif

#ifndef IFF_IPMP
	if (setsockopt(sock, IPPROTO_IP, IP_DONTFAILOVER_IF,
	    (char *)&myaddr, sizeof (struct in_addr)) < 0 &&
	    errno == ENOPROTOOPT) /* lint !e746 */ {
		auto_create = B_FALSE;
	}
#endif
	(void) close(sock);


	/*
	 * Build a new scrgadm_node_ipmp_list. If an adapter is not part
	 * of a ipmp group, print an error message.
	 */
	nnhead = nnlast = NULL;

	for (ai = ailist; ai; ai = ai->next) {
		for (adp = ai->ai_list; adp; adp = adp->next) {

			/*
			 * Remove the duplicate instance for this adp.
			 * Auto-create will create IPMP groups for both
			 * the instances if present
			 */
			remove_dup_instance(adp, &ai->ai_list);

			/* check if ipmp group is present */
			if (adp->adp_group == NULL ||
			    strlen(adp->adp_group) == 0) {
				/* pre-print error message */
				(void) snprintf(errbuff, BUFSIZ, gettext(
				    "IPMP group not found for %s on %s\n"),
				    adp->adp_name, ai->ai_nodezone);
				status.err_code = SCHA_ERR_INVAL;
				if (auto_create) {
					/* Access pnmd on the node */
					if (pnm_init(ai->ai_node) != 0) {
						status.err_code =
						    SCHA_ERR_INTERNAL;
						goto convert_error;
					}
					if (pnm_group_create(adp->adp_name)
					    != 0) {
						status.err_code =
						    SCHA_ERR_INTERNAL;
						goto convert_error;
					}
					if (pnm_map_adapter(adp->adp_name,
					    &adp->adp_group) != 0) {
						status.err_code =
						    SCHA_ERR_INTERNAL;
						goto convert_error;
					}
					pnm_fini();

					/* clear error message */
					errbuff[0] = 0;
				} else
					goto convert_error;
			}	/* end of if */

			/* add this IPMP group to list only once */
			if (!ipmp_list_dup_grp(
			    nnhead, ai->ai_node, ai->ai_zone, adp->adp_group)) {

				/* allocate and initialize nn */
				nn = (scrgadm_node_ipmp_list_t *)
					malloc(
					    sizeof (scrgadm_node_ipmp_list_t));
				if (nn == NULL) {
					status.err_code = SCHA_ERR_NOMEM;
					goto convert_error;
				}
				bzero(nn, sizeof (*nn));

				/* append nn to the nn list */
				if (nnhead == NULL)
					nnhead = nn;
				if (nnlast != NULL)
					nnlast->next = nn;
				nnlast = nn;

				/* fill in nn as if user has entered it */
				nn->raw_netid = strdup(adp->adp_name);
				nn->ipmp_group = strdup(adp->adp_group);
				nn->raw_node = strdup(ai->ai_node);
				if (nn->raw_netid == NULL ||
				    nn->ipmp_group == NULL ||
				    nn->raw_node == NULL) {
					status.err_code = SCHA_ERR_NOMEM;
					goto convert_error;
				}
				nn->zone = NULL;
				if (ai->ai_zone) {
					nn->zone = strdup(ai->ai_zone);
					if (nn->zone == NULL) {
						status.err_code =
						    SCHA_ERR_NOMEM;
						goto convert_error;
					}
				}

			} /* if (!ipmp_list_dup_grp() */
		} /* next adp */
	} /* next ai */

	/*
	 * run validate_netadapter_list to resolve nodenames to nodeids
	 * IPMP group substitution is already done above (nn->ipmp_group).
	 */
	status = validate_netadapter_list(nnhead, NULL, zc_name);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto convert_error;
	}

	/*
	 * Check to see that the selected/specified IPMP groups are homogenous
	 * across all nodes. If the IPMP groups are non-homogenous then check
	 * to see if the given hostnamelist can still be hosted by the IPMP
	 * groups on all the nodes.
	 */
	status = validate_adp_homogeneity(nnhead, lhnames, zc_name);
	if (status.err_code != SCHA_ERR_NOERR)
		goto convert_error;

	/*
	 * Free the old netadapter list. Return the newly built one.
	 */
	free_netadapter_list(*nnlist);

	*nnlist = nnhead;
	status.err_code = SCHA_ERR_NOERR;
	return (status);

convert_error:
	free_netadapter_list(nnhead);
	if (strlen(errbuff) != 0)
		status.err_msg = strdup(errbuff);
	*nnlist = NULL;
	return (status);
}

/*
 * construct_adapterinfo_list()
 *
 * Action: Look up the nodes for the named RG and make call into libpnm to
 *	get a list of all public adapters on these nodes. Formulate results
 * 	into scrgadm_adapterinof_list_t and return. The list of adapters
 *	returned are not filtered in any way.
 *
 *	If there is no error in the rgm function call the scha_errmsg_t
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Return:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 *	SCHA_ERR_INTERNAL
 */
scha_errmsg_t
construct_adapterinfo_list(name_t rg_name,
	scrgadm_adapterinfo_list_t **ailist, char *zc_name)
{
	char				errbuff[BUFSIZ] = { 0 };
	scha_errmsg_t			status = {SCHA_ERR_NOERR, NULL};
	rgm_rg_t			*rg = NULL;
	nodeidlist_t			*nl;
	scrgadm_adapterinfo_list_t	*ai, *aihead, *ailast;
	int				rc = 0;
	char				*nodename = NULL;
	scconf_errno_t			scconf_status =	SCCONF_NOERR;
	uint_t				nodeid;
	boolean_t			iseuropa;
	int				pnm_ret;
	boolean_t			excl;
	int				err = 0;


	aihead = ailast = NULL;

	status = rgm_scrgadm_getrgconf(rg_name, &rg, zc_name);
	if (status.err_code != SCHA_ERR_NOERR) {
		return (status);
	}

	/* Check if farm management is enabled */
	scconf_status = scconf_iseuropa(&iseuropa);
	if (scconf_status != SCCONF_NOERR) {
		status.err_code = SCHA_ERR_INTERNAL;
		goto construct_error;
	}

	for (nl = rg->rg_nodelist; nl; nl = nl->nl_next) {
		/* allocate a new ai and set all fields to 0 */
		ai = (scrgadm_adapterinfo_list_t *)
			malloc(sizeof (scrgadm_adapterinfo_list_t));
		if (ai == NULL) {
			status.err_code = SCHA_ERR_NOMEM;
			goto construct_error;
		}
		bzero(ai, sizeof (*ai));

		/* append new ai to the ai list */
		if (aihead == NULL)
			aihead = ai;
		if (ailast != NULL)
			ailast->next = ai;
		ailast = ai;

		/* invalid nodeid, bail out */
		if (zc_name) {
#if SOL_VERSION >= __s10
		    scconf_status = scconf_get_zc_nodename_by_nodeid(
					zc_name, nl->nl_nodeid, &nodename);
#endif
		} else {
		    scconf_status = scconf_get_nodename(nl->nl_nodeid,
			&nodename);
		}

		if (scconf_status != SCCONF_NOERR) {
			if (iseuropa && scconf_status == SCCONF_ENOEXIST) {
				scconf_status = scconf_get_farmnodename(
				    nl->nl_nodeid, &nodename);
			}
			if (scconf_status != SCCONF_NOERR) {
				status.err_code = SCHA_ERR_NODE;
				goto construct_error;
			}
		}
		/* fill in node name */
		ai->ai_node = strdup(nodename);
		ai->ai_nodezone = strdup(nodename);
		nodename = NULL;
		if ((ai->ai_node == NULL) || (ai->ai_nodezone == NULL)) {
			status.err_code = SCHA_ERR_NOMEM;
			goto construct_error;
		}

		/*
		 * Fill in the zone name, we need this for exclusive-ip zone
		 * support.
		 */
		if (nl->nl_zonename != NULL) {
			excl = xip_zone(ai->ai_node, nl->nl_zonename, &err);
			if (err) {
				status.err_code = SCHA_ERR_INVAL;
				goto construct_error;
			}
			if (excl == B_TRUE) {
				ai->ai_zone = strdup(nl->nl_zonename);
				if (ai->ai_zone == NULL) {
					status.err_code = SCHA_ERR_NOMEM;
					goto construct_error;
				}
				ai->ai_nodezone = calloc(strlen(ai->ai_node) +
				    strlen(nl->nl_zonename) + 3, sizeof (char));
				if (!ai->ai_nodezone) {
					status.err_code = SCHA_ERR_NOMEM;
					goto construct_error;
				}
				(void) strcat(ai->ai_nodezone, ai->ai_node);
				(void) strcat(ai->ai_nodezone, ":");
				(void) strcat(ai->ai_nodezone, nl->nl_zonename);
			}
		}

		/*
		 * get actual adapter info from PNM
		 *
		 * Access pnmd on the node using the private interconnect name
		 */
		if (zc_name)
			pnm_ret = pnm_zone_init(ai->ai_node, zc_name);
		else if (ai->ai_zone)
			pnm_ret = pnm_zone_init(ai->ai_node, ai->ai_zone);
		else
			pnm_ret = pnm_init(ai->ai_node);

		if (pnm_ret == 0) {
			if ((rc = pnm_adapterinfo_list(&ai->ai_list,
			    B_FALSE)) != 0) {
				if (rc == PNM_ENOBUF)
					(void) snprintf(errbuff, BUFSIZ,
					    gettext(
					    "adapter/subnet list on %s is too "
					    "big for convenience options. Use "
					    "the -y and -x options instead."
					    "\n"), ai->ai_nodezone);
				else
					(void) snprintf(errbuff, BUFSIZ,
					    gettext(
					    "can't get adapter list on %s\n"),
					    ai->ai_nodezone);
				status.err_code = SCHA_ERR_INTERNAL;
				goto construct_error;
			}
		} else {
			(void) snprintf(errbuff, BUFSIZ, gettext(
			    "can't get adapter list on %s\n"), ai->ai_nodezone);
			status.err_code = SCHA_ERR_INTERNAL;
			goto construct_error;
		}
		pnm_fini();
	}

	rgm_free_rg(rg);
	*ailist = aihead;
	return (status);

construct_error:
	if (NULL != nodename) {
		free(nodename);
		nodename = NULL;
	}
	if (rg != NULL)
		rgm_free_rg(rg);
	free_adapterinfo_list(aihead);
	if (strlen(errbuff) != 0)
		status.err_msg = strdup(errbuff);
	*ailist = NULL;
	return (status);
}


/*
 * validate_netadapter_list()
 *
 * Input: address of scrgadm_name_valuel_list
 *
 * Action: for each elt on nalist (the -n argument):
 *	- call validate_netadapter() to translate nodeid to nodename
 *	- check if it exists on the given node (using ailist)
 *
 * Output:
 *	nodeid and ipmp filled in for each element
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Returns:
 *  SCHA_ERR_NOERR
 *  SCHA_ERR_ACCESS
 *  SCHA_ERR_MEMBER
 *  SCHA_ERR_INTERNAL
 *  SCHA_ERR_NOMEM
 *  SCHA_ERR_INVAL
 */
scha_errmsg_t
validate_netadapter_list(scrgadm_node_ipmp_list_t *nalist,
	scrgadm_adapterinfo_list_t *ailist, char *zc_name)
{
	char				errbuff[BUFSIZ] = { 0 };
	scrgadm_adapterinfo_list_t	*ai;
	scrgadm_node_ipmp_list_t	*nap;
	int				found;
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	/*
	 * Nothing to validate if user didn't specify -n arguments.
	 */
	if (nalist == NULL) {
		status.err_code = SCHA_ERR_NOERR;
		return (status);
	}

	/*
	 * Check all nodenames/nodeids.
	 */
	for (nap = nalist; nap; nap = nap->next) {
		/* resolve and verify node name */
		status = validate_netadapter_entry(nap, zc_name);
		if (status.err_code != SCHA_ERR_NOERR) {
			return (status);
		}
	}

	/*
	 * Check that all specified netadapter's exist
	 * Take each of them, try to find it in the ai list.
	 */
	for (nap = nalist; nap; nap = nap->next) {
		/* is netid empty? */
		if (!nap->raw_netid || !strlen(nap->raw_netid)) {
			(void) snprintf(errbuff, BUFSIZ, gettext(
				"must specify an adapter or IPMP group\n"));
			status.err_code = SCHA_ERR_INVAL;
			goto validate_error;
		}

		/* hostname must have been filled in before this */
		if (!nap->hostname || !strlen(nap->hostname)) {
			(void) snprintf(errbuff, BUFSIZ, gettext(
				"must specify a valid nodename or nodeid\n"));
			status.err_code = SCHA_ERR_INVAL;
			goto validate_error;
		}

		/* is adapter or ipmp group configured? */
		if (ailist && !is_netadapter_found(nap, ailist)) {
			if (nap->zone) {
				(void) snprintf(errbuff, BUFSIZ, gettext(
				    "%s not configured on %s:%s\n"),
				    nap->raw_netid, nap->hostname, nap->zone);
			} else {
				(void) snprintf(errbuff, BUFSIZ, gettext(
				    "%s not configured on %s\n"),
				    nap->raw_netid, nap->hostname);
			}
			status.err_code = SCHA_ERR_INVAL;
			goto validate_error;
		}
	}

	/*
	 * Make sure there's exactly one adapter or ipmp group for each node.
	 */
	for (ai = ailist; ai; ai = ai->next) {
		found = 0;
		for (nap = nalist; nap; nap = nap->next) {
			if (strcmp(nap->hostname, ai->ai_node) == 0) {
				if (!nap->zone && !ai->ai_zone) {
					found++;
				} else if (nap->zone && ai->ai_zone) {
					if (strcmp(nap->zone, ai->ai_zone) == 0)
						found++;
				}
			}
		}
		if (found == 0) {
			(void) snprintf(errbuff, BUFSIZ,
				gettext("no adapter or "
				"ipmp group specified for %s\n"),
				ai->ai_nodezone);
			status.err_code = SCHA_ERR_INVAL;
			goto validate_error;
		}
		if (found > 1) {
			(void) snprintf(errbuff, BUFSIZ,
				gettext("multiple adapters or "
				"ipmp groups specified for %s\n"),
				ai->ai_nodezone);
			status.err_code = SCHA_ERR_INVAL;
			goto validate_error;
		}
	}

validate_error:

	if (strlen(errbuff) != 0)
		status.err_msg = strdup(errbuff);
	return (status);
} /* validate_netadapter_list */

/*
 * validate_hostname_list()
 *
 * Input: scrgadm_hostname_list_t of hostnames/IP's from command line
 *
 * Action:
 *	for each name on list: call resolve_hostname_IP()
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_NOMEM
 *
 */

scha_errmsg_t
validate_hostname_list(scrgadm_hostname_list_t *list_p)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	char errbuff[BUFSIZ];

	scrgadm_hostname_list_t *ptr = list_p;

	if (!ptr || !ptr->raw_name) {
		status.err_code = SCHA_ERR_INVAL;
		(void) sprintf(errbuff, "%s\n",
		    gettext("At least one hostname is required with -l"));
		    status.err_msg = strdup(errbuff);
	}
	while (ptr != NULL) {
		status = resolve_hostname_IP(ptr);
		if (status.err_code != SCHA_ERR_NOERR) {
			goto clean_up;
		}
		ptr = ptr->next;
	}

	return (status);
clean_up:
	/* nothing special to do */
	return (status);
} /* validate_hostname_list */

/*
 * select_adapterinfo_by_hostname_list()
 *
 * Input: scrgadm_adapterinfo_list_t *ailist
 *	  scrgadm_hostname_list_t *hnlist
 * Action: Remove from ailist adapter instances that cannot host all hostnames
 *      as given by hnlist using the can_adapter_host_hostname_list()
 *	call. If all adapter instances on a given node are removed in this
 *	function, processing stops, and SCHA_ERR_INVAL is returned. In this we
 *	first form a new linked-list of all the adapters (with their instance
 *	pointers). Then based on whether this adapter can host all the
 *	hostnames we either remove it from out list or keep it around. We need
 *	to from this new linked-list to do proper subnet matching.
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Return:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_NOMEM
 */
scha_errmsg_t
select_adapterinfo_by_hostname_list(scrgadm_adapterinfo_list_t *ailist,
	scrgadm_hostname_list_t *hnlist)
{
	char				errbuff[BUFSIZ] = { 0 };
	scha_errmsg_t			status = {SCHA_ERR_NOERR, NULL};
	scrgadm_adapterinfo_list_t	*ai;
	scrgadm_hostname_list_t		*hn;
	pnm_adapterinfo_t		*adp;
	pnm_adapter_instances_t		*head, *hp, *hp_next;
	adapter_host_status_t		hstatus;

	for (ai = ailist; ai; ai = ai->next) {
		for (head = NULL, adp = ai->ai_list; adp; adp = adp->next) {
			/* Form our own pnm_adapter_instances list */
			for (hp = head; hp; hp = hp->next) {
				if (hp->v4instp) {
					if (strcmp(adp->adp_name,
					    hp->v4instp->adp_name) == 0) {
						hp->v6instp = adp;
						break;
					}
				}
				if (hp->v6instp) {
					if (strcmp(adp->adp_name,
					    hp->v6instp->adp_name) == 0) {
						hp->v4instp = adp;
						break;
					}
				}
			}
			/* Not found - so malloc a new structure */
			if (hp == NULL) {
				hp = (pnm_adapter_instances_t *)malloc(
				    sizeof (pnm_adapter_instances_t));
				if (hp == NULL) {
					status.err_code = SCHA_ERR_NOMEM;
					goto select_error;
				}
				bzero(hp, sizeof (pnm_adapter_instances_t));
				if (adp->adp_flags & IFF_IPV4)
					hp->v4instp = adp;
				else
					hp->v6instp = adp;
				/* Add it to the list */
				hp->next = head;
				head = hp;
			}
		}

		/*
		 * Now check to see if the adapters can host the
		 * given hostnames. Get the adp instances from
		 * the capable adapters.
		 */
		for (hp = head; hp; hp = hp_next) {
			hp_next = hp->next;
			hstatus = can_adp_host_hostname_list(hp, hnlist);
			if (hstatus != ADAPTER_HOST_OK)
				free_adp(hp, &head, &ai->ai_list);
		}

		/*
		 * If all adapters are removed, then we error out with
		 * an appropriate message
		 */
		if (head == NULL) {
			(void) snprintf(errbuff, BUFSIZ, gettext(
			    "Specified hostname(s) cannot be "
			    "hosted by any adapter on %s\n"), ai->ai_nodezone);
			(void) strlcat(errbuff, gettext("Hostnames:"),
			    BUFSIZ);
			for (hn = hnlist; hn; hn = hn->next) {
				(void) strlcat(errbuff, " ", BUFSIZ);
				(void) strlcat(errbuff, hn->raw_name, BUFSIZ);
			}
			status.err_code = SCHA_ERR_INVAL;
			goto select_error;
		}
		/* Free the list of adapters that we created */
		free_pnm_adapter_instances(&head);
	}
	status.err_code = SCHA_ERR_NOERR;
	return (status);

select_error:
	if (strlen(errbuff) != 0)
		status.err_msg = strdup(errbuff);
	return (status);
}

/*
 * select_adapterinfo_by_netadapter_list()
 *
 * Action: Remove from the ailist any adapters that are not specified in
 * the -n option, if -n is used.
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Return:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 */
scha_errmsg_t
select_adapterinfo_by_netadapter_list(scrgadm_adapterinfo_list_t *ailist,
	scrgadm_node_ipmp_list_t *nalist)
{
	char				errbuff[BUFSIZ] = { 0 };
	scha_errmsg_t			status = {SCHA_ERR_NOERR, NULL};
	scrgadm_adapterinfo_list_t	*ai;
	pnm_adapterinfo_t		*adp, *adp_next;
	scrgadm_node_ipmp_list_t	*nap;

	/* do not remove any adapterinfo if nalist is empty */
	if (nalist == NULL)
		return (status);

	/*
	 * ailist now contains only adapters that can host all
	 * hostnames. Check that all -n arguments are in ailist.
	 */
	for (nap = nalist; nap; nap = nap->next) {
		if (!is_netadapter_found(nap, ailist)) {
			(void) snprintf(errbuff, BUFSIZ, gettext(
				"%s on %s cannot host "
				"specified hostname(s)\n"),
				nap->raw_netid, nap->hostname);
			status.err_code = SCHA_ERR_INVAL;
			goto select_error;
		}
	}

	/*
	 * Remove ai's that are not in the netadapter list.
	 * What remain in the ai list will be what the user specify in the
	 * command line -n option.
	 */
	for (ai = ailist; ai; ai = ai->next) {
		for (adp = ai->ai_list; adp; adp = adp_next) {
			adp_next = adp->next;

			for (nap = nalist; nap; nap = nap->next) {
				if (strcmp(nap->hostname, ai->ai_node) != 0)
					continue;
				if (ai->ai_zone && !nap->zone)
					continue;
				if (!ai->ai_zone && nap->zone)
					continue;
				if (ai->ai_zone && nap->zone) {
					if (strcmp(nap->zone, ai->ai_zone)
					    != 0)
						continue;
				}

				if (strcmp(nap->raw_netid,
					adp->adp_name) == 0 ||
					(adp->adp_group != NULL &&
					strcmp(nap->raw_netid,
					adp->adp_group) == 0))
					break;
			}

			/* remove adp if not found in netadapter's */
			if (nap == NULL) {
				remove_adp_from_adapterinfolist(adp,
				    &ai->ai_list);
				adp_next = ai->ai_list;
			}
		}
	}

	status.err_code = SCHA_ERR_NOERR;
	return (status);

select_error:
	if (strlen(errbuff) != 0)
		status.err_msg = strdup(errbuff);
	return (status);
}

/*
 * validate_adapterinfo_list()
 *
 * Action: Validate the input ailist for use in LHRS and SARS. Each node
 *	must have exactly one adapter/ipmp group in ailist.
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Return:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCAH_ERR_NOMEM
 */
scha_errmsg_t
validate_adapterinfo_list(scrgadm_adapterinfo_list_t *ailist)
{
	char				errbuff[BUFSIZ] = { 0 };
	scha_errmsg_t			status = {SCHA_ERR_NOERR, NULL};
	scrgadm_adapterinfo_list_t	*ai;
	pnm_adapterinfo_t		*adapter;
	char				*ipmp = NULL;

	for (ai = ailist; ai; ai = ai->next) {

		/* check if no adapter is determined */
		if (ai->ai_list == NULL) {
			(void) snprintf(errbuff, BUFSIZ, gettext(
				"no appropriate adapter or IPMP group "
				"found on %s\n"),
				ai->ai_node);
			status.err_code = SCHA_ERR_INVAL;
			goto validate_error;
		}

		/*
		 * check if more than one adapter is found
		 * If so, then check to ensure two conditions are met:
		 * 	that all adapters have an IPMP group configured
		 * 	that all adapters belong to the same IPMP group.
		 */
		if (more_than_one_adp(ai->ai_list)) {
			/* Remember the first ipmp group, if its configured */
			if (ai->ai_list->adp_group != NULL) {
				ipmp = strdup(ai->ai_list->adp_group);
				if (ipmp == NULL) {
					status.err_code = SCHA_ERR_NOMEM;
					goto validate_error;
				}
			}
			for (adapter = ai->ai_list; adapter;
				adapter = adapter->next) {
				if (adapter->adp_group == NULL) {
				/* IPMP group is NULL for this adapter */
					(void) snprintf(errbuff, BUFSIZ,
						gettext("multiple adapters on "
						    "node %s were found "
						    "with at least one adapter"
						    " %s not in an "
						    "IPMP group.\n"),
						    ai->ai_node,
						    adapter->adp_name);
					status.err_code = SCHA_ERR_INVAL;
					goto validate_error;
				} else if ((ipmp) &&
				    strcmp(adapter->adp_group, ipmp) != 0) {
				/*
				 * IPMP group is defined, so check that all
				 * adapters are in the same IPMP group
				 */
					(void) snprintf(errbuff, BUFSIZ,
						gettext("multiple IPMP groups "
						    "are available on node "
						    "%s which can host given "
						    "hostname(s). Specify the "
						    "NetIfList property to "
						    "resolve the ambiguity.\n"),
						    ai->ai_node);
					status.err_code = SCHA_ERR_INVAL;
					goto validate_error;
				}	/* end of else */
			}	/* end of for - adapterlist */
			if (ipmp)
				free(ipmp);
		}	/* end of if - more_than_one_adp */
	}	/* end of for - ailist */

	status.err_code = SCHA_ERR_NOERR;
	return (status);

validate_error:
	if (strlen(errbuff) != 0)
		status.err_msg = strdup(errbuff);
	if (ipmp)
		free(ipmp);
	return (status);
}

/*
 * free_adapterinfo_list()
 *
 * Input: scrgadm_adapterinfo_list_t
 *
 * Action: Frees memory associated with input list.
 *
 * Return: none
 */
void
free_adapterinfo_list(scrgadm_adapterinfo_list_t *aihead)
{
	scrgadm_adapterinfo_list_t	*ai, *ai_next;

	for (ai = aihead; ai; ai = ai_next) {
		ai_next = ai->next;
		free(ai->ai_node);
		free(ai->ai_zone);
		free(ai->ai_nodezone);
		pnm_adapterinfo_free(ai->ai_list);
		ai->next = NULL;
		free(ai);
	}
}


/*
 * free_netadapter_list()
 *
 * Input: scrgadm_node_ipmp_list
 *
 * Action: Free memory associated with the input list. To free an element
 *	alone, make sure its next field is NULL.
 *
 * Return: none
 */
void
free_netadapter_list(scrgadm_node_ipmp_list_t *nnlist)
{
	scrgadm_node_ipmp_list_t	*nn, *nn_next;

	for (nn = nnlist; nn; nn = nn_next) {
		nn_next = nn->next;
		free(nn->raw_netid);
		free(nn->ipmp_group);
		free(nn->raw_node);
		free(nn->zone);
		free(nn->hostname);
		free(nn);
	}
}

/*
 * free_pnm_adapter_instances()
 *
 * Input:
 *	  pnm_adapter_instances_t: adapter list
 *
 * Action: Frees the adapter list - does not free the actual instances though.
 *
 * Return: none
 */
void
free_pnm_adapter_instances(pnm_adapter_instances_t **list)
{
	pnm_adapter_instances_t	*hp, *hp_next, *head;

	head = *list;
	for (hp = head; hp; hp = hp_next) {
		hp_next = hp->next;
		free(hp);
		hp = NULL;
	}
	*list = head;
}


/*
 * free_adp()
 *
 * Input: pnm_adapter_instances_t: adp to remove
 *	  pnm_adapter_instances_t: adapter list
 *	  pnm_adapterinfo_t: adapter instance list
 *
 * Action: Removes the given adp from the adapter list and also removes the
 *	  insatnces of the adapter from the instances list.
 *
 * Return: none
 */
void
free_adp(pnm_adapter_instances_t *hp, pnm_adapter_instances_t **list,
    pnm_adapterinfo_t **ailist)
{
	pnm_adapter_instances_t	*prev, *current;
	pnm_adapterinfo_t	*adp, *head;

	head = *ailist;
	if (*list == hp)
		*list = (*list)->next;
	else {
		for (prev = *list, current = (*list)->next;
		    current != NULL; prev = current, current = current->next) {
			if (current == hp) {
				prev->next = current->next;
				break;
			}
		}
	}

	/* Now free the instances in the structure */
	if (hp->v4instp) {
		adp = hp->v4instp;
		if (head == adp)
			head = adp->next;
		if (adp->prev != NULL)
			adp->prev->next = adp->next;
		if (adp->next != NULL)
			adp->next->prev = adp->prev;

		adp->prev = adp->next = NULL;
		pnm_adapterinfo_free(adp);
	}
	if (hp->v6instp) {
		adp = hp->v6instp;
		if (head == adp)
			head = adp->next;
		if (adp->prev != NULL)
			adp->prev->next = adp->next;
		if (adp->next != NULL)
			adp->next->prev = adp->prev;

		adp->prev = adp->next = NULL;
		pnm_adapterinfo_free(adp);
	}
	free(hp);

	*ailist = head;
}

/*
 * remove_dup_instance()
 *
 * Input: pnm_adapterinfo_t
 *
 * Action: Checks the adpinfolist to see if there are more than one adapter
 *         instances and removes the duplicate adapter instance (v4/v6).
 *	   Keep in mind that the adpinfolist has a list of adapter instances
 *	   and not adapters.
 *
 * Return: none
 */
void
remove_dup_instance(pnm_adapterinfo_t *adp, pnm_adapterinfo_t **list)
{
	pnm_adapterinfo_t *nadp, *nadp_next, *head;

	head = *list;

	for (nadp = head; nadp; nadp = nadp_next) {
		nadp_next = nadp->next;
		/* if another instance of the same adp exists */
		if (adp != nadp &&
		    strcmp(adp->adp_name, nadp->adp_name) == 0) {
			/* remove the second instance */
			if (head == nadp)
				head = nadp->next;
			if (nadp->prev != NULL)
				nadp->prev->next = nadp->next;
			if (nadp->next != NULL)
				nadp->next->prev = nadp->prev;
			nadp->prev = nadp->next = NULL;
			pnm_adapterinfo_free(nadp);
			break;
		}
	}
	*list = head;
}

/*
 * ipmp_list_dup_grp()
 *
 *	Action: search the given ipmp_list to see if the given
 *		node identifier and group name are already present.
 *
 *	Return: boolean_t
 */
boolean_t
ipmp_list_dup_grp(scrgadm_node_ipmp_list_t *ipmp_list,
    char *node, char *zone, char *group)
{
	scrgadm_node_ipmp_list_t *list = ipmp_list;


	/*
	 * walk list: for each entry compare nodename
	 * if same then compare group name
	 * if same then Return 1
	 * if no match Return 0
	 */
	while (list != NULL) {
		if (strcmp(node, list->raw_node) == 0 &&
		    strcmp(group, list->ipmp_group) == 0) {
			if ((!zone && !list->zone) ||
			    ((zone && list->zone) &&
			    (strcmp(zone, list->zone) == 0)))
				return (B_TRUE);
		}
		list = list->next;
	}
	return (B_FALSE);

} /* ipmp_list_dup_grp */

/*
 * validate_adp_homogeneity()
 *
 * Input: scrgadm_name_valuel_list
 *
 * Action: Checks the list to see if there are any non-homogenous IPMP groups
 *         in any of the nodes. By non-homogenous we mean that all the adps in
 *         the IPMP group do not have similar instances plumbed. eg: say group
 *         sc_ipmp0 has hme0 and qfe0. Now if hme0 has a v4 and v6 instance
 *         plumbed and qfe0 has only a v4 instance plumbed then sc_ipmp0 is
 *         non-homogenous. Also verifies adapter homogeneity across nodes
 *         to see that all IP (v4 and/or v6) mappings of the hostnames
 *         can be hosted by all the nodes. eg: say node 1 and node 2 have 1
 *         IPMP group each sc_ipmp0 with 1 adp hme0. Now on node 1 say hme0 has
 *         a v4 instance plumbed and on node 2 hme0 has a v4 and v6 instance
 *         plumbed. If we try to host a hostname which has both v4 and v6
 *         mapping then this vaildate will fail since on node 1 we will not be
 *         able to host the hostname's v6 mapping. But if the hostname had only
 *         a v4 mapping then this validate will pass since we can host the v4
 *         address on both the nodes. Hence the requirement for cross-node
 *         homogeneity check. Also if the hostname had a v4 and v6 mapping and
 *	   all the IPMP groups have v4 only or v6 only then we allow it to
 *	   pass - haip will warn.
 *	   So the idea is that we first check to see if the selected IPMP
 *	   groups across nodes are homogenous. If not then we see if the given
 *	   hostnames (v4 and v6) can be hosted by all the selected IPMP groups.
 *	   Please remember that if you use the scrgadm non-convenience options
 *	   then there will be warnings but no real failures. scrgadm convenience
 *	   options will be strict.
 *
 *
 *         If validated, it returns
 *         SCHA_ERR_NOERR else it prints an error message and returns
 *         SCHA_ERR_INVAL or SCHA_ERR_INTERNAL
 *
 * Returns:
 *  SCHA_ERR_NOERR
 *  SCHA_ERR_INVAL
 *  SCHA_ERR_INTERNAL
 */
scha_errmsg_t
validate_adp_homogeneity(scrgadm_node_ipmp_list_t *nalist,
    scrgadm_hostname_list_t *lhnames, char *zc_name)
{
	char				errbuff[BUFSIZ] = { 0 };
	scrgadm_node_ipmp_list_t	*nap;
	scha_errmsg_t 			status = {SCHA_ERR_NOERR, NULL};
	int 				rc = 0, currinst = 0;
	boolean_t			v4 = B_FALSE;
	boolean_t			v6 = B_FALSE;
	int				previnst = 0;
	scrgadm_hostname_list_t 	*hn;

	for (hn = lhnames; hn; hn = hn->next) {
		if (hn->ip6_addr)
			v6 = B_TRUE;
		if (hn->ip_addr)
			v4 = B_TRUE;
		if (v4 && v6)
			break;
	}

	/* Call pnm_get_instances to see that this group is homogenous */
	for (nap = nalist; nap; nap = nap->next) {
		if (zc_name) {
			if (pnm_zone_init(nap->hostname, zc_name) != 0) {
			    (void) snprintf(errbuff, BUFSIZ, gettext(
				    "can't get adapter instances on %s"),
				    nap->hostname);
				status.err_code = SCHA_ERR_INTERNAL;
				goto check_error;
			}
		} else if (nap->zone) {
			if (pnm_zone_init(nap->hostname, nap->zone) != 0) {
				(void) snprintf(errbuff, BUFSIZ, gettext(
				    "can't get adapter instances on %s:%s"),
				    nap->hostname, nap->zone);
				status.err_code = SCHA_ERR_INTERNAL;
				goto check_error;
			}
		} else {
			if (pnm_init(nap->hostname)) {
				(void) snprintf(errbuff, BUFSIZ, gettext(
				    "can't get adapter instances on %s"),
				    nap->hostname);
				status.err_code = SCHA_ERR_INTERNAL;
				goto check_error;
			}
		}
		if ((rc = pnm_get_instances(nap->ipmp_group,
		    &currinst)) != 0) {
			if (rc == PNM_ENONHOMOGENOUS) {
				(void) snprintf(errbuff, BUFSIZ, gettext(
				    "All adapters in %s on %s do not have"
				    " similar instances plumbed."),
				    nap->ipmp_group, nap->hostname);
				status.err_code = SCHA_ERR_INVAL;
			} else {
				(void) snprintf(errbuff, BUFSIZ, gettext(
				    "can't get adapter instances on %s"),
				    nap->hostname);
				status.err_code = SCHA_ERR_INTERNAL;
			}
			goto check_error;
		}

		/* Initialize previnst if it has not been initialized */
		if (!previnst)
			previnst = currinst;

		/*
		 * If the instances are not equal and the hostname has a v4
		 * mapping then check to see if the v4 mapping can be hosted
		 * by the adp instances on both the nodes
		 */
		if (previnst != currinst && v4) {
			if (!(currinst & PNMV4MASK) ||
			    !(previnst & PNMV4MASK)) {
				(void) snprintf(errbuff, BUFSIZ, gettext(
				    "The given list of hostnames has IPv4 "
				    "addresses which cannot be hosted by "
				    "the IPMP groups in all nodes."));
				status.err_code = SCHA_ERR_INVAL;
				goto check_error;
			}
		}

		/*
		 * If the instances are not equal and the hostname has a v6
		 * mapping then check to see if the v6 mapping can be hosted
		 * by the adp instances on both the nodes
		 */
		if (previnst != currinst && v6) {
			if (!(currinst & PNMV6MASK) ||
			    !(previnst & PNMV6MASK)) {
				(void) snprintf(errbuff, BUFSIZ, gettext(
				"The given list of hostnames has IPv6 "
				    "addresses which cannot be hosted by "
				    "the IPMP groups in all nodes."));
				status.err_code = SCHA_ERR_INVAL;
				goto check_error;
			}
		}
		pnm_fini();

		/* Save the currinst as previnst for the next node */
		previnst = currinst;
	}	/* next node */

	status.err_code = SCHA_ERR_NOERR;
	return (status);

check_error:
	if (strlen(errbuff) != 0)
		status.err_msg = strdup(errbuff);
	return (status);
} /* vaildate_adp_homogeneity */

/*
 * validate_netadapter_entry()
 *
 * Input: an individual scrgadm_node_ipmp_list elt
 *
 * each elt holds strings of the following the forms:
 *	raw_node may hold a nodename or nodeid
 *	raw_netid may hold a netif or 'ipmpN'
 *
 * Action:
 *
 *	validate node identifier:
 *		make sure we get a good nodeid out of the deal
 *		and fill in elt->nodeid
 *
 * Output:
 *		scrgadm_node_ipmp_list validated fields are filled in
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Returns:
 *  SCHA_ERR_NOERR
 *  SCHA_ERR_NOMEM
 *  SCHA_ERR_ACCESS
 *  SCHA_ERR_MEMBER
 *  SCHA_ERR_INTERNAL
 *  SCHA_ERR_INVAL
 */
scha_errmsg_t
validate_netadapter_entry(scrgadm_node_ipmp_list_t *nn_elt, char *zc_name)
{

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	/*
	 * make sure that raw_node can be mapped to a valid nodeid
	 * fill in nodeid & hostname fields
	 */
	status = validate_nodepart(nn_elt, zc_name);
	if (status.err_code != SCHA_ERR_NOERR) {
		goto exit_err;
	}

	return (status);
exit_err:
	/* nothing special to do */
	return (status);
} /* validate_netadapter_entry */

/*
 * is_netadapter_found()
 *
 * Action: search the given ailist for occurrences of the given adapter or
 *	ipmp group.
 *
 * Return: boolean_t
 *
 */
boolean_t
is_netadapter_found(scrgadm_node_ipmp_list_t *nap,
	scrgadm_adapterinfo_list_t *ailist)
{
	scrgadm_adapterinfo_list_t	*ai;
	pnm_adapterinfo_t		*adp;
	int				found;

	found = 0;
	for (ai = ailist; ai && !found; ai = ai->next) {
		/* are we talking about the same node */
		if (strcmp(ai->ai_node, nap->hostname) != 0)
			continue;
		if (nap->zone && !ai->ai_zone)
			continue;
		if (!nap->zone && ai->ai_zone)
			continue;
		if ((nap->zone && ai->ai_zone) &&
		    (strcmp(nap->zone, ai->ai_zone) != 0))
			continue;

		/* valid if it's an adapter name or ipmp group */
		for (adp = ai->ai_list; adp; adp = adp->next) {
			if (strcmp(nap->raw_netid,
				adp->adp_name) == 0 ||
				(adp->adp_group != NULL &&
				strcmp(nap->raw_netid,
				adp->adp_group) == 0)) {
				found++;
				break;
			}
		}
	}

	return (found? B_TRUE: B_FALSE);
}

/*
 * resolve_hostname_IP()
 *
 * Input: a scrgadm_hostname_list element which holds
 *	either a hostname or a IPv4/IPv6 address in 'raw_name'
 *
 * Action: figure out hostname or IPv4 and/or IPv6 address and
 *	then fill in the hostname and address fields
 *
 * Output: fills in scrgadm_hostname_list fields
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 *	SCHA_ERR_NOMEM
 *
 */
scha_errmsg_t
resolve_hostname_IP(scrgadm_hostname_list_t *host)
{

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	char errbuff[BUFSIZ];

	/*
	 * try as IPv4 first
	 */
	status = map_IPv4_to_hostname(host);
	if (status.err_code == SCHA_ERR_NOERR) {
		goto done;
	}

	/*
	 * try as IPv6 next
	 */
	status = map_IPv6_to_hostname(host);
	if (status.err_code == SCHA_ERR_NOERR) {
		goto done;
	}

	/*
	 * well, try as hostname
	 */
	status = map_hostname_to_IP(host);

	if (status.err_code == SCHA_ERR_INVAL) {
		(void) sprintf(errbuff, "%s %s\n",
		    host->raw_name,
		    gettext("Cannot be mapped to an IP address."));
		status.err_msg = strdup(errbuff);
	}

done:
	return (status);
} /* resolve_hostname_IP */

/*
 * can_adp_host_hostname_list()
 *
 * Input: pnm_adapter_instances_t *hp: adapter with its instances
 *	  scrgadm_hostname_list_t *hnlist: list of hostnames (-l option)
 * Action: Check to see if the given adapter's v4 and v6 instance if present
 *	can host the given hostnamelist. If either instance cannot then
 *	return ADAPTER_HOST_SUBNET else return ADAPTER_HOST_OK.
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Return:
 *	ADAPTER_HOST_OK, ADAPTER_HOST_SUBNET
 */
adapter_host_status_t
can_adp_host_hostname_list(pnm_adapter_instances_t *hp,
    scrgadm_hostname_list_t *hnlist)
{
	adapter_host_status_t hpv4status, hpv6status;

	hpv4status = hpv6status = ADAPTER_HOST_UNINITIATED;

	if (hp->v4instp)
		hpv4status = can_v4inst_host_hostname_list(hp->v4instp, hnlist);
	if (hp->v6instp)
		hpv6status = can_v6inst_host_hostname_list(hp->v6instp, hnlist);

	/* If any of the instances have a mismatch then return mismatch */
	if (hpv4status == ADAPTER_HOST_SUBNET ||
	    hpv6status == ADAPTER_HOST_SUBNET)
		return (ADAPTER_HOST_SUBNET);

	/* If both instances are uninitiated then return mismatch */
	if (hpv4status == ADAPTER_HOST_UNINITIATED &&
	    hpv6status == ADAPTER_HOST_UNINITIATED)
		return (ADAPTER_HOST_SUBNET);

	return (ADAPTER_HOST_OK);
}

/*
 * remove_adp_from_adapterinfolist()
 *
 * Action: Removes from the adpinfolist the given adp instance and also the
 *	   duplicate adapter instance (v4/v6).
 *	   Keep in mind that the adpinfolist has a list of adapter instances
 *	   and not adapters.
 *
 * Return: none
 */
void
remove_adp_from_adapterinfolist(pnm_adapterinfo_t *adp,
    pnm_adapterinfo_t **listhead)
{
	pnm_adapterinfo_t *adp_next, *head, *padp;
	char *adp_name = NULL;

	adp_name = strdup(adp->adp_name);
	head = *listhead;

	if (head == adp)
		head = adp->next;
	if (adp->prev != NULL)
		adp->prev->next = adp->next;
	if (adp->next != NULL)
		adp->next->prev = adp->prev;

	adp->prev = adp->next = NULL;
	pnm_adapterinfo_free(adp);

	/* remove the other instance if available */
	for (padp = head; padp; padp = adp_next) {
		adp_next = padp->next;
		if (strcmp(adp_name, padp->adp_name) == 0) {
			/* remove this instance */
			if (head == padp)
				head = padp->next;
			if (padp->prev != NULL)
				padp->prev->next = padp->next;
			if (padp->next != NULL)
				padp->next->prev = padp->prev;

			padp->prev = padp->next = NULL;
			pnm_adapterinfo_free(padp);
			break;
		}
	}
	free(adp_name);
	*listhead = head;
}

/*
 * more_than_one_adp()
 *
 * Input: pnm_adapterinfo_t
 *
 * Action: Checks the adpinfolist to see if there are more than one adapters.
 *	   Keep in mind that the adpinfolist has a list of adapter instances
 *	   and not adapters.
 *
 * Return: boolean_t
 */
boolean_t
more_than_one_adp(pnm_adapterinfo_t *head)
{
	char *adp_name;
	pnm_adapterinfo_t *adp;

	adp_name = head->adp_name;
	for (adp = head->next; adp; adp = adp->next) {
		if (strcmp(adp_name, adp->adp_name) != 0)
			return (B_TRUE);
	}
	return (B_FALSE);
}

/*
 * validate_nodepart()
 *
 * Input: scrgadm_node_ipmp_list element
 *
 * Action: try to map raw_node to a valid nodeid
 *
 * Output: nodeid & hostname fields filled in
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 *
 * Returns:
 *  SCHA_ERR_NOERR
 *  SCHA_ERR_NOMEM
 *  SCHA_ERR_ACCESS
 *  SCHA_ERR_MEMBER
 *  SCHA_ERR_INTERNAL
 *  SCHA_ERR_INVAL
 */

scha_errmsg_t
validate_nodepart(scrgadm_node_ipmp_list_t *nn_elt, char *zc_name)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	scconf_errno_t scconf_status = SCCONF_EINVAL; /* note unusual init */
	scconf_nodeid_t nodeid;
	char *nodename = NULL;
	char errbuff[BUFSIZ];
	boolean_t iseuropa;
	char *chptr = (char *)NULL;
	char *zonepart = (char *)NULL;
	char *nonzonepart = (char *)NULL;

	/* Check if farm management is enabled */
	scconf_status = scconf_iseuropa(&iseuropa);
	if (scconf_status != SCCONF_NOERR) {
		status.err_code = SCHA_ERR_INTERNAL;
		goto cleanup;
	}

	zonepart = strdup(nn_elt->raw_node);
	nonzonepart = strdup(nn_elt->raw_node);
	if (!zonepart || !nonzonepart) {
		status.err_code = SCHA_ERR_NOMEM;
		goto cleanup;
	}
	chptr = strchr(nonzonepart, ':');
	if (chptr) {
		*chptr = '\0';
		(void) strcpy(zonepart, chptr + 1);
		nn_elt->zone = strdup(zonepart);
		if (!nn_elt->zone) {
			status.err_code = SCHA_ERR_NOMEM;
			goto cleanup;
		}
	}

	/*
	 * call scconf_get_nodename() first
	 * because scconf_get_nodeid() won't exit
	 * with failure if we send in a nodeid!
	 */
	/*
	 * try treating raw_node as a nodeid
	 *   first:  try to convert char* raw input to type scconf_nodeid_t
	 *   second: ask scconf if we're a legit nodeid
	 *	if yes, hnameptr will hold nodename
	 */
	if (str_to_nodeid(nonzonepart, &nodeid)) {
		scconf_status = scconf_get_nodename(nodeid, &nodename);
		if (iseuropa && scconf_status != SCCONF_NOERR) {
			if (scconf_status == SCCONF_ENOEXIST) {
				scconf_status = scconf_get_farmnodename(
				    nodeid, &nodename);
			}
		}
		if (scconf_status == SCCONF_NOERR) {
			/*
			 * input was a good nodeid
			 * so now we have good nodename also
			 * to overwrite our earlier hope
			 */
			nn_elt->nodeid = nodeid;
			nn_elt->hostname = nodename;
			if (!nn_elt->hostname) {
				status.err_code = SCHA_ERR_NOMEM;
			}
			goto cleanup;
		}
	}
	/*
	 * now try raw_node as a nodename
	 * if successful, nodeid will hold value
	 */
	if (zc_name) {
#if SOL_VERSION >= __s10
	    nodeid = clconf_zc_get_nodeid_by_nodename(zc_name, nonzonepart);
#endif
	} else {
	    scconf_status = scconf_get_nodeid(nonzonepart, &nodeid);
	    if (iseuropa && scconf_status != SCCONF_NOERR) {
		    if (scconf_status == SCCONF_ENOEXIST) {
			    scconf_status = scconf_get_farmnodeid(
				nonzonepart, &nodeid);
		    }
	    }
	}
	if (scconf_status == SCCONF_NOERR) {
		/*
		 * raw was good name
		 */
		nn_elt->nodeid = nodeid;
		nn_elt->hostname = strdup(nonzonepart);
		if (!nn_elt->hostname) {
			status.err_code = SCHA_ERR_NOMEM;
			goto cleanup;
		}
	} else {
		/*
		 * Found bad name or bad nodeid
		 */
		status = scconf_err_to_scha_err(scconf_status);
		goto cleanup;
	}

cleanup:
	if (status.err_code != SCHA_ERR_NOERR) {
		if (nn_elt && nn_elt->raw_node) {
			(void) sprintf(errbuff, "%s %s %s\n",
			    gettext("Unable to validate "),
			    nonzonepart,
			    gettext("as nodename or nodeid."));
			status.err_msg = strdup(errbuff);
		}
	}
	free(zonepart);
	free(nonzonepart);
	return (status);
} /* validate_nodepart */

/*
 * map_IPv4_to_hostname()
 *
 * Input: scrgadm_hostname_list_t element
 *
 * Action: treat raw_name as IPv4 and attempt to
 *	convert to hostname
 *
 * Output: write hostname into hostname and ip_addr into ip_addr
 *
 * Returns:
 *	SCHA_ERR_NOERR - conversion successful
 *	SCHA_ERR_INVAL - can't convert
 *	SCHA_ERR_NOMEM
 */
scha_errmsg_t
map_IPv4_to_hostname(scrgadm_hostname_list_t *host)
{

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	struct hostent *hp;
	int error_num;
	in_addr_t addr;

	addr = inet_addr(host->raw_name);
	if (addr == (uint32_t)-1) {
		status.err_code = SCHA_ERR_INVAL;
	} else {
		/* got a result; convert to hostname */
		hp = getipnodebyaddr((char *)&addr, 4, AF_INET, &error_num);
		if (hp && hp->h_name) {
			host->hostname =  strdup(hp->h_name);
			if (!host->hostname) {
				status.err_code = SCHA_ERR_NOMEM;
				return (status);
			}
		} else {
			status.err_code = SCHA_ERR_INVAL;
			return (status);
		}
		/* store ip_addr */
		host->ip_addr = (in_addr_t *)malloc(sizeof (in_addr_t));
		if (host->ip_addr == NULL) {
			status.err_code = SCHA_ERR_NOMEM;
			return (status);
		}
		*host->ip_addr = addr;
	}
	return (status);
} /* map_IPv4_to_hostname */


/*
 * map_IPv6_to_hostname()
 *
 * Input: scrgadm_hostname_list_t element
 *
 * Action: treat raw_name as IPv6 and attempt to
 *	convert to hostname
 *
 * Output: write hostname into hostname and ip6_addr into ip6_addr
 *
 * Returns:
 *	SCHA_ERR_NOERR - conversion successful
 *	SCHA_ERR_INVAL - can't convert
 *	SCHA_ERR_NOMEM
 */
scha_errmsg_t
map_IPv6_to_hostname(scrgadm_hostname_list_t *host)
{

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	in6_addr_t addr;
	struct hostent *hp;
	int error_num;

	if (inet_pton(AF_INET6, (const char *) &host->raw_name,
	    (void *) &addr) != 1) {
		status.err_code = SCHA_ERR_INVAL;
	} else {
		/* got a result; convert to hostname */
		hp = getipnodebyaddr((char *)&addr, 16, AF_INET6, &error_num);
		if (hp && hp->h_name) {
			host->hostname =  strdup(hp->h_name);
			if (!host->hostname) {
				status.err_code = SCHA_ERR_NOMEM;
				return (status);
			}
		} else {
			status.err_code = SCHA_ERR_INVAL;
			return (status);
		}
		/* store ip6_addr */
		host->ip6_addr = (in6_addr_t *)malloc(sizeof (in6_addr_t));
		if (host->ip6_addr == NULL) {
			status.err_code = SCHA_ERR_NOMEM;
			return (status);
		}
		bcopy(&addr, host->ip6_addr, 16);
	}
	return (status);
} /* map_IPv6_to_hostname */

/*
 * map_hostname_to_IP()
 *
 * Input: scrgadm_hostname_list_t element
 *
 * Action: treat raw_name as hostname and attempt to
 *	get ip_addr and ip6_addr
 *
 * Output: write ip_addr into ip_addr and ip6_addr to ip6_addr
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 *	SCHA_ERR_INVAL
 */
scha_errmsg_t
map_hostname_to_IP(scrgadm_hostname_list_t *host)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	struct hostent *hp;
	int error_num;

	/* get the first IPv4 address */
	hp = getipnodebyname(host->raw_name, AF_INET, 0, &error_num);
	if (hp && hp->h_length == 4) {
		host->ip_addr = (in_addr_t *)malloc(sizeof (in_addr_t));
		if (host->ip_addr == NULL) {
			status.err_code = SCHA_ERR_NOMEM;
			return (status);
		}
		bcopy(hp->h_addr, host->ip_addr, (size_t)hp->h_length);
	} else {
		host->ip_addr = NULL;
	}

	/* get the first IPv6 address */
	hp = getipnodebyname(host->raw_name, AF_INET6, 0, &error_num);
	if (hp && hp->h_length == 16) {
		host->ip6_addr = (in6_addr_t *)malloc(sizeof (in6_addr_t));
		if (host->ip6_addr == NULL) {
			status.err_code = SCHA_ERR_NOMEM;
			return (status);
		}
		bcopy(hp->h_addr, host->ip6_addr, (size_t)hp->h_length);
	} else {
		host->ip6_addr = NULL;
	}

	if (host->ip_addr == NULL && host->ip6_addr == NULL) {
		/* couldn't get v4 or v6 */
		status.err_code = SCHA_ERR_INVAL;
	} else {
		host->hostname = strdup(host->raw_name);
		if (!host->hostname) {
			status.err_code = SCHA_ERR_NOMEM;
		}
	}

	return (status);
} /* map_hostname_to_IP */

/*
 * can_v4inst_host_hostname_list()
 *
 * Input:
 *	pnm_adapterinfo_t *adp: v4 adapter instance to check
 *	scrgadm_hostname_list_t *hnlist: list of hostnames (-l option).
 *
 * Action: Check if the given v4 adapter instance can be used to host all the
 *      hostnames given by hnlist.
 *
 * Return: ADAPTER_HOST_OK, ADAPTER_HOST_SUBNET, ADAPTER_HOST_UNINITIATED
 */

adapter_host_status_t
can_v4inst_host_hostname_list(pnm_adapterinfo_t *adp,
    scrgadm_hostname_list_t *hnlist)
{
	scrgadm_hostname_list_t		*hn;
	adapter_host_status_t		status = ADAPTER_HOST_UNINITIATED;

	/*
	 * check to see if v4 addresses of all the hostnames
	 * can be hosted by the adp
	 */
	for (hn = hnlist; hn; hn = hn->next) {
		if (!hn->ip_addr)
			continue;
		if (!subnet_match_v4(adp, hn))
			return (ADAPTER_HOST_SUBNET);
		status = ADAPTER_HOST_OK;
	}	/* end of for - hostname_list */

	return (status);
}

/*
 * can_v6inst_host_hostname_list()
 *
 * Input:
 *	pnm_adapterinfo_t *adp: v6 adapter instance to check
 *	scrgadm_hostname_list_t *hnlist: list of hostnames (-l option).
 *
 * Action: Check if the given v6 adapter instance can be used to host all the
 *      hostnames given by hnlist.
 *
 * Return: ADAPTER_HOST_OK, ADAPTER_HOST_SUBNET, ADAPTER_HOST_UNINITIATED
 */

adapter_host_status_t
can_v6inst_host_hostname_list(pnm_adapterinfo_t *adp,
    scrgadm_hostname_list_t *hnlist)
{
	scrgadm_hostname_list_t		*hn;
	adapter_host_status_t		status = ADAPTER_HOST_UNINITIATED;

	/*
	 * check to see if v6 addresses of all the hostnames
	 * can be hosted by the adp
	 */
	for (hn = hnlist; hn; hn = hn->next) {
		if (!hn->ip6_addr)
			continue;
		if (!subnet_match_v6(adp, hn))
			return (ADAPTER_HOST_SUBNET);
		status = ADAPTER_HOST_OK;
	}	/* end of for - hostname_list */

	return (status);
}

/*
 * str_to_nodeid()
 *
 * Input: string purporting to represent a nodeid
 *
 * Action: attempt to create a scconf_nodeid from the input
 *
 *  string must be of the form:
 *    "<n>" where <n> is an integer
 *
 * Output:
 *	If name can be converted to a scconf_nodeid_t
 *	it is placed into scconf_nodeid.
 *
 * Possible return values:
 *	B_TRUE
 *	B_FALSE
 */
boolean_t
str_to_nodeid(char *name, scconf_nodeid_t *scconf_nodeid)
{
	char *s;
	boolean_t result = B_TRUE;
	scconf_nodeid_t nodeid = 0;

	for (s = name;  *s;  ++s) {
		if (!isdigit(*s)) {
			result = B_FALSE;
			break;  /* can't be a nodeid */
		}
	}
	if (result == B_TRUE) {
		nodeid = (uint_t)atoi(name);
		if (nodeid < 1) {
			result = B_FALSE;
		} else {
			*scconf_nodeid = nodeid;
		}
	}
	return (result);
} /* str_to_nodeid */

/*
 * Map a scconf_status code to a scha_errmsg_t code.
 */
scha_errmsg_t
scconf_err_to_scha_err(scconf_errno_t scconf_status)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	switch (scconf_status) {
	case SCCONF_NOERR:
		status.err_code = SCHA_ERR_NOERR;
		break;
	case SCCONF_EUSAGE:
		status.err_code = SCHA_ERR_INVAL;
		break;
	case SCCONF_ENOEXIST:
		status.err_code = SCHA_ERR_INVAL;
		break;
	case SCCONF_EPERM:
		status.err_code = SCHA_ERR_ACCESS;
		break;
	case SCCONF_ENOCLUSTER:
		status.err_code = SCHA_ERR_MEMBER;
		break;
	case SCCONF_ENOMEM:
		status.err_code = SCHA_ERR_NOMEM;
		break;
	case SCCONF_EINVAL:
		status.err_code = SCHA_ERR_INVAL;
		break;
	case SCCONF_EUNEXPECTED:
		status.err_code = SCHA_ERR_INTERNAL;
		break;
	case SCCONF_ENGZONE:
		status.err_code = SCHA_ERR_ACCESS;
		break;
	case SCCONF_ECZONE:
		status.err_code = SCHA_ERR_ACCESS;
		break;
	default:
		status.err_code = SCHA_ERR_INTERNAL;
		break;
	} /* endwsitch */
	return (status);
} /* scconf_err_to_scha_err */

/*
 * subnet_match_v4()
 *
 * Input:
 *	pnm_adapterinfo_t *adp: v4 adapter instance to check
 *	scrgadm_hostname_list_t *hn: hostname
 *
 * Action: Check if the given v4 adapter instance can be used to host the
 *      hostname.
 *
 * Return: B_TRUE, B_FALSE
 */
boolean_t
subnet_match_v4(pnm_adapterinfo_t *adp,
    scrgadm_hostname_list_t *hn)
{
	uint_t				i;
	ipaddr_t			addr_mask;
	ipaddr_t			addr_net;

	/* Even if one of the IP addresses matches it is fine */
	for (i = 0; i < adp->adp_addrs.num_addrs; i++) {
		IN6_V4MAPPED_TO_IPADDR(&(adp->adp_addrs.adp_ipmask[i]),
		    addr_mask);
		IN6_V4MAPPED_TO_IPADDR(&(adp->adp_addrs.adp_ipnet[i]),
		    addr_net);
		if (addr_net && (*hn->ip_addr & addr_mask) == addr_net)
			return (B_TRUE);
	}
	return (B_FALSE);
}

/*
 * subnet_match_v6()
 *
 * Input:
 *	pnm_adapterinfo_t *adp: v6 adapter instance to check
 *	scrgadm_hostname_list_t *hn: hostname
 *
 * Action: Check if the given v6 adapter instance can be used to host the
 *      hostname.
 *
 * Return: B_TRUE, B_FALSE
 */
boolean_t
subnet_match_v6(pnm_adapterinfo_t *adp,
    scrgadm_hostname_list_t *hn)
{
	uint_t i;

	/* Even if one of the IP addresses matches it is fine */
	for (i = 0; i < adp->adp_addrs.num_addrs; i++) {
		if (CL_V6_MASK_EQ(*(hn->ip6_addr),
		    adp->adp_addrs.adp_ipmask[i],
		    adp->adp_addrs.adp_ipnet[i])) {
			return (B_TRUE);
		}
	}	/* end of for - adp_addrs */
	return (B_FALSE);
}

/*
 * process_namelist_to_nodeids()
 *
 * Input: namelist_t which may contain names or nodeid's
 *
 * Action:
 *	for each elt:
 *		convert entry from name to nodeid if needed
 *		make sure name is in cluster
 *
 * Output: same list, all elts guaranteed to hold nodeids, no names
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Returns:
 *	scha_errmsg_t
 */
scha_errmsg_t
process_namelist_to_nodeids(namelist_t *list)
{

	scconf_errno_t scconf_status = SCCONF_EINVAL; /* note unusual init */
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	scconf_nodeid_t nodeid;
	char nodeidstr[BUFSIZ];
	char *nodename = NULL;
	char errbuff[BUFSIZ];

	while (list) {
		/*
		 * two scconf calls: get nodename_from_nodeid & get_
		 * _nodeid_from_nodename. The second has the quirk
		 * that if 'nodename' is actually nodeid we get nodeid
		 * back w/ success, therefore, in order to truly check
		 * for nodename we have to first eliminate nodeid's as input
		 */

		/*
		 *  try treating nl_name as a nodeid
		 *   first:  try to convert char* raw input to
		 *	type scconf_nodeid_t
		 *   second: ask scconf if we're a legit nodeid
		 *	if yes, nodename will hold nodename
		 */
		if (str_to_nodeid(list->nl_name, &nodeid)) {
			scconf_status = scconf_get_nodename(nodeid, &nodename);
			if (scconf_status == SCCONF_NOERR) {
				/*
				 * nl_name was a nodeid; keep it
				 * discard what sconf allocated
				 */
				free(nodename);
				list = list->nl_next;
				continue;
			}
		}
		/*
		 * this next block is not an 'else' to str_to_node()
		 * because we want to execute it even if str_to_nodeid()
		 * succeeded but scconf_get_nodename() didn't
		 */

		/*
		 * our raw entry wasn't a nodeid so
		 * now try it as a nodename
		 * if successful, nodeid will be loaded
		 */
		scconf_status = scconf_get_nodeid(list->nl_name, &nodeid);
		if (scconf_status == SCCONF_NOERR) {
			/*
			 *list->nl_name held name
			 * now swap it for nodeid
			 */
			(void) sprintf(nodeidstr, "%d", nodeid);
			free(list->nl_name);
			list->nl_name = strdup(nodeidstr);
			if (!list->nl_name) {
				scconf_status = SCCONF_ENOMEM;
				goto cleanup;
			}
		} else {
			goto cleanup;
		}
		list = list->nl_next;
	} /* while list */

cleanup:
	if (scconf_status != SCCONF_NOERR) {
		status = scconf_err_to_scha_err(scconf_status);
		if (list && list->nl_name) {
			(void) sprintf(errbuff, "%s %s %s\n",
			    gettext("Unable to convert "),
			    list->nl_name,
			    gettext("to a nodeid."));
			status.err_msg = strdup(errbuff);
		}
	}
	return (status);
} /* process_namelist_to_nodeids */

/*
 * validate_auxnodelist()
 *
 * Input:
 *	scrgadm_adapterinfo_list_t *ailist
 *	namelist_t *laux
 *
 * Action:
 *	validate ailist and laux to be mutually exclusive
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Returns:
 *	scha_errmsg_t
 */
scha_errmsg_t
validate_auxnodelist(scrgadm_adapterinfo_list_t *ailist,
    namelist_t *laux)
{
	scrgadm_adapterinfo_list_t *ai;
	namelist_t *list;
	scconf_nodeid_t nodeid = 0;
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	scconf_errno_t scconf_status;
	char *nodename = NULL;

	for (ai = ailist; ai; ai = ai->next) {
		for (list = laux; list; list = list->nl_next) {
			if (str_to_nodeid(list->nl_name, &nodeid)) {
				/*
				 * Only node id is provided. Obtain the
				 * nodename
				 */
				scconf_status = scconf_get_nodename(nodeid,
				    &nodename);
				if (scconf_status != SCCONF_NOERR) {
					return (scconf_err_to_scha_err(
					    scconf_status));
				}
				if (strcmp(ai->ai_node, nodename) == 0) {
					status.err_code = SCHA_ERR_DUPP;
					status.err_msg = strdup(
gettext("IPMP list and Auxillary Nodelist must be mutually exclusive"));
					free(nodename);
					return (status);
				}
			} else {
			/*
			 * If the list is populated using the function
			 * process_namelist_to_nodeids(), this ensures that the
			 * list contains only node ids and not node names.
			 * This code part takes care of the list containing
			 * node names
			 */
				if (strcmp(ai->ai_node, list->nl_name) == 0) {
					status.err_code = SCHA_ERR_DUPP;
					status.err_msg = strdup(
gettext("IPMP list and Auxillary Nodelist must be mutually exclusive"));
					return (status);
				}
			}
		}
	}
	return (status);
} /* validate_auxnodelist */

/*
 * load_node_ipmp_elt()
 *
 * Input: ptr to scrgadm_node_ipmp_list_t element; ptr to user's input
 *
 * Action:
 *	parse input into netid part & nodeidentifier part
 *	copy these fields into the elt
 *
 * Output: loads two "raw" fields of scrgadm_node_ipmp_list_t element
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 *	SCHA_ERR_INVAL
 */
scha_errmsg_t
load_node_ipmp_elt(scrgadm_node_ipmp_list_t *elt, char *raw_input)
{

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	char raw_name_cp[BUFSIZ];
	char adapter_part[BUFSIZ];
	char node_part[BUFSIZ];

	/* make a private copy to tinker with */
	(void) strcpy(raw_name_cp, raw_input);

	/* parse into adapter and node identifier parts */
	status = parse_raw_adapt_node(raw_name_cp, adapter_part, node_part);
	if (status.err_code == SCHA_ERR_NOERR) {
		elt->raw_node = strdup(node_part);
		elt->raw_netid = strdup(adapter_part);
		if (!elt->raw_node || ! elt->raw_netid) {
			status.err_code = SCHA_ERR_NOMEM;
		}
		elt->zone = NULL;
	}

	return (status);
} /* load_node_ipmp_elt */

/*
 * parse_raw_adapt_node()
 *
 * Input: buffer holding user's entry for adapter@node info
 *
 * Output: 'adapter_part' and 'node_part' buffers
 *	filled appropriately
 *
 *	This function does not call any rgm functions and the rgm
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Returns:
 *  SCHA_ERR_NOERR
 *  SCHA_ERR_INVAL
 */
scha_errmsg_t
parse_raw_adapt_node(char *raw_name, char *adapter_part,
    char *node_part)
{

	char *netadapter, *node, *at;
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	char errbuff[BUFSIZ];

	netadapter = raw_name;
	node = NULL;
	at = strchr(raw_name, '@');
	if (at == NULL) {
		status.err_code = SCHA_ERR_INVAL;
		(void) sprintf(errbuff,
		    gettext("value '%s' is not of the form 'ipmp@node'"),
		    raw_name);
		status.err_msg = strdup(errbuff);
	} else {
		*at = '\0';
		node = at + 1;
		if ((node == NULL) || (strlen(node) == 0)) {
			status.err_code = SCHA_ERR_INVAL;
			(void) sprintf(errbuff,
			    gettext("value '%s' needs node identifier"),
			    raw_name);
			status.err_msg = strdup(errbuff);
		} else {
			/* send back the answers, Mr. Wizard! */
			(void) strcpy(adapter_part, netadapter);
			(void) strcpy(node_part, node);
		}
	}
	return (status);
} /* parse_raw_adapt_node */

/*
 * create_hnlist_val()
 *
 * Input: list of hostnames
 *
 * Action: walk hostname list, creating and appending entries to output list
 *
 * Output: write comma-separated list of node identifiers, preserving
 *	user's input form: either hostname or dotted IP
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
scha_errmsg_t
create_hnlist_val(const scrgadm_hostname_list_t *hostnames,
    char **hostname_list)
{

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	size_t len, newlen;

	len = newlen = 0;
	while (hostnames) {
		len = strlen(hostnames->raw_name);
		newlen += len;
		if (newlen != len) {
			/* all non-first entries get leading comma */
			newlen++;
		}
		*hostname_list = (char *)realloc(*hostname_list,
		    (newlen + 1) * sizeof (char));
		if (hostname_list == NULL) {
			status.err_code = SCHA_ERR_NOMEM;
			goto exit_err;
		}

		if (newlen == len) {
			/* first entry needs cleanser */
			(void) strcpy(*hostname_list, "");
		} else {
			/* non-first entries get leading comma */
			(void) strcat(*hostname_list, ",");
		}
		(void) strcat(*hostname_list, hostnames->raw_name);

		hostnames = hostnames->next;
	}

	return (status);
exit_err:
	/* nothing to do: hostname_list will be NULL if we get here */
	return (status);
} /* create_hnlist_val */

/*
 * create_netiflist_val()
 *
 * Input: list of ipmp's
 *
 * Action: walk ipmp list, creating and appending entries to output list
 *
 * Output: write comma-separated list of ipmpX@nodeidN's
*
* Note: input list is valuel_list but each valuel will have only one entry
*	name holds ipmp-group name
*	valuel, elt #1, holds nodeidentifier
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
scha_errmsg_t
create_netiflist_val(const scrgadm_node_ipmp_list_t *ipmps,
    char **netif_list)
{

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	char nodeidbuff[MAXNAMELEN];
	size_t len, newlen;

	len = newlen = 0;
	while (ipmps) {
		/* need enough space to write ipmpX plus @ plus nodeid */
		len = strlen(ipmps->ipmp_group);
		len++;
		if (ipmps->zone) {
			(void) sprintf(nodeidbuff, "%d:%s", ipmps->nodeid,
			    ipmps->zone);
		} else {
			(void) sprintf(nodeidbuff, "%d", ipmps->nodeid);
		}
		len += strlen(nodeidbuff);
		newlen += len;
		if (newlen != len) {
			/* all non-first entries get leading comma */
			newlen++;
		}
		*netif_list = (char *)realloc(*netif_list,
		    (newlen + 1) * sizeof (char));
		if (netif_list == NULL) {
			status.err_code = SCHA_ERR_NOMEM;
			goto exit_err;
		}
		if (newlen == len) {
			/* first entry needs cleanser */
			(void) strcpy(*netif_list, "");
		} else {
			/* non-first entries get leading comma */
			(void) strcat(*netif_list, ",");
		}
		(void) strcat(*netif_list, ipmps->ipmp_group);
		(void) strcat(*netif_list, "@");
		(void) strcat(*netif_list, nodeidbuff);

		ipmps = ipmps->next;
	}

	return (status);
exit_err:
	/* nothing to do: netif_list will be NULL if we get here */
	return (status);
} /* create_netiflist_val */

/*
 * Given a base rt name, returns the full rtname with the largest version
 * number. eg if two versions of SUNW.LogicalHostname (plain and v2) are
 * registered, this function will return SUNW.LogicalHostname:2
 * If "zc_name" is not null, this method will search for the rt name in
 * the specified zone cluster.
 */
char *
get_latest_rtname(const char *rt, char *zc_name)
{
	static char rtname[MAXCCRTBLNMLEN]; /* on the heap */
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = (char **)0;
	char **rt_namep;
	uint_t len;
	uint_t nodeid;

	len = strlen(rt);
	(void) strncpy(rtname, rt, sizeof (rt));

	/* Get the list of all rt names */
	rgm_status = rgm_scrgadm_getrtlist(&rt_names, zc_name);
	if (rgm_status.err_code != SCHA_ERR_NOERR) {
		print_scha_error(rgm_status);
		return (rtname);
	}
	if (rt_names == NULL)
		return (rtname);

	for (rt_namep = rt_names;  rt_namep && *rt_namep;  ++rt_namep) {
		char *rt_from_list = *rt_namep;
		/*
		 * We are interested in the name if it matches rt
		 * exactly or is of the form rt:something
		 */
		if ((strncmp(rt, rt_from_list, len) == 0) &&
		    ((rt_from_list[len] == 0) || (rt_from_list[len] == ':'))) {
			/*
			 * if the name is alphanumerically greater than what
			 * we have so far, save it
			 */
			if (strcmp(rt_from_list, rtname) > 0) {
				(void) strncpy(rtname, rt_from_list,
				    sizeof (rtname));
			}
		}
	}

	/* Free the list of rt names */
	rgm_free_strarray(rt_names);

	return (rtname);
}

/*
 * print_scha_error()
 *
 * print librgm error by type
 *
 * Function to map error code to printable string to be provided
 * by librgm.
 */
void
print_scha_error(scha_errmsg_t error)
{

	if (error.err_msg) {
		(void) fprintf(stderr, "%s\n", error.err_msg);
		free(error.err_msg);
		error.err_msg = NULL;
	} else {
		char *msg = NULL;
		/* ask rgm for an error message to print for this code */
		if (error.err_code != SCHA_ERR_NOERR) {
			msg = rgm_error_msg(error.err_code);
			if (msg) {
				(void) fprintf(stderr, "%s\n", msg);
			}
		}
	}
} /* print_scha_error */

/*
 * validate_nodelist()
 *
 * Checks that all rg nodes for RG rg_name are cluster members.
 *
 *	If there is no error in the rgm function call the scha_errmsg_t
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Return:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NODE
 *	SCHA_ERR_MEMBER
 */
scha_errmsg_t
validate_nodelist(name_t rg_name, char *zc_name)
{
	scha_errmsg_t			status = {SCHA_ERR_NOERR, NULL};
	char				errbuff[BUFSIZ] = { 0 };
	scconf_errno_t			scconf_status;
	rgm_rg_t			*rg;
	nodeidlist_t			*nl;
	uint_t				mbr = 0;
	char				*nodename = NULL;

	uint_t nodeid;
	/* Get RG structure, which contains list of RG nodes */
	status = rgm_scrgadm_getrgconf(rg_name, &rg, zc_name);
	if (status.err_code != SCHA_ERR_NOERR) {
		return (status);
	}

	/* For each RG node, check its cluster membership */
	for (nl = rg->rg_nodelist; nl; nl = nl->nl_next) {
		scconf_status = scconf_get_nodename(nl->nl_nodeid, &nodename);
		if (scconf_status != SCCONF_NOERR) {
			(void) snprintf(errbuff, BUFSIZ, gettext(
				"cannot determine nodename for %d\n"),
				nl->nl_nodeid);
			status.err_code = SCHA_ERR_NODE;
			goto cleanup;
		}

		/* Check cluster membership */
		scconf_status = scconf_ismember(nl->nl_nodeid, &mbr);
		if (scconf_status != SCCONF_NOERR) {
			(void) snprintf(errbuff, BUFSIZ, gettext(
				"cannot determine cluster "
				"membership for %s\n"), nodename);
			status = scconf_err_to_scha_err(scconf_status);
			goto cleanup;
		}
		if (!mbr) {
			(void) snprintf(errbuff, BUFSIZ, gettext(
				"%s not a cluster member\n"),
				nodename);
			status.err_code = SCHA_ERR_MEMBER;
			goto cleanup;
		}
		free(nodename);
		nodename = NULL;
	}

cleanup:
	free(nodename);
	nodename = NULL;

	rgm_free_rg(rg);
	if (strlen(errbuff) != 0)
		status.err_msg = strdup(errbuff);
	return (status);
}

/*
 * get_ip_type()
 *
 * Help check that there is no mix of exclusive IP and shared IP zones in
 * the RG nodelist: return the iptype of the RG:
 *	IP_TYPE_SHARED	-	All are nodes, or the zones are shared
 *	IP_TYPE_EXCL	-	All are exclusive ip-type zones
 *	IP_TYPE_MIX	-	Mix of shared and exclusive ip-type zones
 *	IP_TYPE_UNKNOWN	-	Could not find out the ip-type of one or
 *				more zone.
 */
rg_ip_type_t
get_ip_type(name_t rg_name, char *zc_namep)
{
	scha_errmsg_t			status = {SCHA_ERR_NOERR, NULL};
	scconf_errno_t			scconf_status;
	rgm_rg_t			*rg;
	nodeidlist_t			*nl;
	char				*nodename = NULL;
	int				node_found = 0;
	int				zone_found = 0;
	int				excl_found = 0;
	int				shared_found = 0;
	int				err_found = 0;
	int				err = 0;
	rg_ip_type_t			iptyp = IP_TYPE_UNKNOWN;
	boolean_t			rv;

	/* Get RG structure, which contains list of RG nodes */
	status = rgm_scrgadm_getrgconf(rg_name, &rg, zc_namep);
	if (status.err_code != SCHA_ERR_NOERR) {
		return (iptyp);
	}

	for (nl = rg->rg_nodelist; nl; nl = nl->nl_next) {
		if (nodename)
			free(nodename);
		nodename = NULL;

		/*
		 * Make sure there is no mix of zones with exclusive and
		 * shared ip-type
		 */
		if (nl->nl_zonename != NULL) {
			zone_found++;
			scconf_status = scconf_get_nodename(nl->nl_nodeid,
			    &nodename);
			if (scconf_status != SCCONF_NOERR) {
				err_found++;
				continue;
			}

			rv = xip_zone(nodename, nl->nl_zonename, &err);
			if (err)
				err_found++;
			else
				if (rv == B_TRUE)
					excl_found++;
				else
					shared_found++;
		} else {
			node_found++;
		}
	}

	if (excl_found) {
		if (shared_found || node_found) {
			iptyp = IP_TYPE_MIX;
			goto cleanup;
		}
		if (!err_found) {
			iptyp = IP_TYPE_EXCL;
		}
	} else if (!err_found) {
		iptyp = IP_TYPE_SHARED;
	}

cleanup:
	if (nodename)
		free(nodename);

	rgm_free_rg(rg);
	return (iptyp);
}

/*
 * xip_zone()
 *
 * Figure out if the provided zone is of ip-type exclusive.
 */
static boolean_t
xip_zone(char *node, char *zone, int *err)
{
	boolean_t excl;
	pnm_zone_iptype_t iptype;

	*err = 0;
	if (pnm_init(node) != 0) {
		*err = 1;
		return (B_FALSE);
	}

	if (pnm_get_zone_iptype((pnm_zone_t)zone, &iptype) != 0) {
		*err = 1;
		return (B_FALSE);
	}
	pnm_fini();

	if (iptype == EXCLUSIVE_IP)
		excl = B_TRUE;
	else
		excl = B_FALSE;

	return (excl);
}

/*
 * validate_farmnodelist()
 *
 * Checks that all rg nodes for RG rg_name are farm members.
 *
 *	If there is no error in the rgm function call the scha_errmsg_t
 *	structure can be used for returning the error message. This
 *	memory will be released after printing the error message.
 *
 * Return:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NODE
 *	SCHA_ERR_MEMBER
 */
scha_errmsg_t
validate_farmnodelist(name_t rg_name)
{
	scha_errmsg_t			status = {SCHA_ERR_NOERR, NULL};
	char				errbuff[BUFSIZ] = { 0 };
	scconf_errno_t			scconf_status;
	rgm_rg_t			*rg;
	nodeidlist_t			*nl;
	uint_t				mbr = 0;
	char				*nodename = NULL;

	uint_t nodeid;
	char *zonename = NULL;
#if SOL_VERSION >= __s10
	populate_zone_cred(&nodeid, &zonename);
#endif
	/* Get RG structure, which contains list of RG nodes */
	status = rgm_scrgadm_getrgconf(rg_name, &rg, zonename);
	if (status.err_code != SCHA_ERR_NOERR) {
		return (status);
	}

	/* Initialize fmm handle */
	scconf_fmm_initialize();

	/* For each Farm RG node, check its farm node membership */
	for (nl = rg->rg_nodelist; nl; nl = nl->nl_next) {
		scconf_status = scconf_get_farmnodename(nl->nl_nodeid,
		    &nodename);
		if (scconf_status != SCCONF_NOERR) {
			(void) snprintf(errbuff, BUFSIZ, gettext(
			    "cannot determine nodename for %d\n"),
			    nl->nl_nodeid);
			status.err_code = SCHA_ERR_NODE;
			goto cleanup;
		}

		/* Check farm membership */
		scconf_status = scconf_isfarmmember(nl->nl_nodeid, &mbr);
		if (scconf_status != SCCONF_NOERR) {
			(void) snprintf(errbuff, BUFSIZ, gettext(
			    "Failed to determine farm "
			    "membership for %s\n"), nodename);
			status = scconf_err_to_scha_err(scconf_status);
			goto cleanup;
		}

		if (!mbr) {
			(void) snprintf(errbuff, BUFSIZ, gettext(
			    "%s not a farm member\n"),
			    nodename);
			status.err_code = SCHA_ERR_MEMBER;
			goto cleanup;
		}

		if (nodename) {
			free(nodename);
			nodename = NULL;
		}
	}

cleanup:
	/* Finalize fmm handle */
	scconf_fmm_finalize();

	if (nodename) {
		free(nodename);
	}

	if (rg) {
		rgm_free_rg(rg);
	}

	if (strlen(errbuff) != 0) {
		status.err_msg = strdup(errbuff);
	}

	return (status);
}

/*
 * get_rg_nodetype()
 *
 * Get the (server/farm) type of RG nodes.
 *
 * Return:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_INVAL
 */
scha_errmsg_t
get_rg_nodetype(name_t rg_name, scconf_nodetype_t *rg_nodetype, char *zc_name)
{
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scconf_errno_t scconf_status = SCCONF_NOERR;
	char *nodename = NULL;
	rgm_rg_t *rg = NULL;

	uint_t nodeid;
	/* Get RG structure, which contains list of RG nodes */
	scha_status = rgm_scrgadm_getrgconf(rg_name, &rg, zc_name);
	if (scha_status.err_code != SCHA_ERR_NOERR) {
		return (scha_status);
	}

	/*
	 * Get the first node from the RG nodelist and determine if the
	 * node is of server or farm type. Since RG nodelist cannot contain
	 * a mix of server and farm nodes, checking for one node is enough.
	 */
	scconf_status = scconf_get_nodename(rg->rg_nodelist->nl_nodeid,
	    &nodename);
	if (scconf_status == SCCONF_NOERR) {
		*rg_nodetype = SCCONF_NODE_CLUSTER;
	} else if (scconf_status == SCCONF_ENOEXIST) {
		scconf_status = scconf_get_farmnodename(
		    rg->rg_nodelist->nl_nodeid, &nodename);
		if (scconf_status == SCCONF_NOERR) {
			*rg_nodetype = SCCONF_NODE_FARM;
		}
	}

cleanup:
	if (nodename)
		free(nodename);
	if (rg)
		rgm_free_rg(rg);

	/* Convert the scconf error to scha error. */
	if (scconf_status != SCCONF_NOERR) {
		scha_status = scconf_err_to_scha_err(scconf_status);
	}

	return (scha_status);
}

/*
 * create_auxnodelist_val()
 *
 * Input: namelist_t guaranteed to hold nodeid's in string form
 *
 * Action: walk auxnodes list, creating and appending entries to output list
 *
 * Output: write comma-separated list of scconf_nodeid_t
 *
 * Returns:
 *	SCHA_ERR_NOERR
 *	SCHA_ERR_NOMEM
 */
scha_errmsg_t
create_auxnodelist_val(const namelist_t *auxnodes,
    char **auxnode_list)
{

	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	size_t len, newlen;

	len = newlen = 0;
	while (auxnodes) {
		len = strlen(auxnodes->nl_name);
		newlen += len;
		if (newlen != len) {
			/* all non-first entries get leading comma */
			newlen++;
		}
		*auxnode_list = (char *)realloc(*auxnode_list,
		    (newlen + 1) * sizeof (char));
		if (auxnode_list == NULL) {
			status.err_code = SCHA_ERR_NOMEM;
			goto exit_err;
		}

		if (newlen == len) {
			/* first entry needs cleanser */
			(void) strcpy(*auxnode_list, "");
		} else {
			/* non-first entries get leading comma */
			(void) strcat(*auxnode_list, ",");
		}
		(void) strcat(*auxnode_list, auxnodes->nl_name);
		auxnodes = auxnodes->nl_next;
	}

	return (status);
exit_err:
	/* nothing to do: auxnode_list will be NULL if we get here */
	return (status);
} /* create_auxnodelist_val */


/*
 * Replace '-' by '_' in the string.
 * Solaris projects names do not support '-', only '_'.
 * As SLM generated project name are base on resource group
 * name, a special handling is done for resource group names with '-'.
 * This function is just used to display the SLM generated project
 * name, RG_SLM_projectname, which is not a CCR property, it is
 * just a display that shows for a resource group SLM automated
 * the name of project that has or will be generated by SLM.
 */
void
slm_replace_dash_in_str(char *s)
{
	if (s == NULL) {
		return;
	}
	while (*s != '\0') {
		if (*s == '-') {
			*s = '_';
		}
		s++;
	}
}
/* SC SLM addon end */

#ifdef WEAK_MEMBERSHIP
/*
 * validate_ping_target
 *
 * Validates whether the given hostname (ping_targets)
 * is present in the same primary subnet as the
 * cluster nodes.
 *
 * Input : 	ping_target
 *
 * Return :
 * 		SCHA_NOERR	- Validation passed
 * 		SCHA_ENOMEM	- Lack of memory
 *		SCHA_ERR_INTERNAL  - Any other unexpected errors
 */
scha_errmsg_t
validate_ping_target(char *ping_target) {
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	char err_msg[BUFSIZ];
	scrgadm_adapterinfo_list_t *ai = NULL;
	scrgadm_adapterinfo_list_t *aihead = NULL;
	scrgadm_adapterinfo_list_t *ailast = NULL;
	scrgadm_adapterinfo_list_t *ailist = NULL;
	int rc = 0;
	const char *nodename = NULL;
	char *nodename_nonconst = NULL;
	int pnm_ret;

	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node = NULL;
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;

	scrgadm_hostname_list_t *hostnamelist = NULL;
	scconf_nodeid_t nodeid;
	quorum_status_t *quorum_status = (quorum_status_t *)0;
	boolean_t skip_node = B_FALSE;
	int nodecount = 0;

	/*
	 * Get all the nodes of the cluster
	 * construct the scrgadm_adapterinfo_list_t
	 * structure for the adapter instances
	 */
	rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR) {
		status.err_code = SCHA_ERR_INTERNAL;
		(void) sprintf(err_msg, dgettext(TEXT_DOMAIN,
				"Unexpected error occured.\n"));
		status.err_msg = strdup(err_msg);
		return (status);
	}

	/*
	 * Check if the node is Online
	 * before we try to contact the pnm deamon
	 */
	if (cluster_get_quorum_status(&quorum_status) != SCCONF_NOERR) {
		status.err_code = SCHA_ERR_INTERNAL;
		(void) sprintf(err_msg, dgettext(TEXT_DOMAIN,
				"Unexpected error occured.\n"));
		status.err_msg = strdup(err_msg);
		return (status);
	}

	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {
		nodename = clconf_obj_get_name(node);
		nodename_nonconst = strdup(nodename);

		rstatus = scconf_get_nodeid(nodename_nonconst, &nodeid);
		if (rstatus != SCCONF_NOERR) {
			status.err_code = SCHA_ERR_INTERNAL;
			(void) sprintf(err_msg, dgettext(TEXT_DOMAIN,
				"Unexpected error occured.\n"));
			status.err_msg = strdup(err_msg);
			return (status);
		}

		skip_node = B_FALSE;
		for (nodecount = 0; nodecount < quorum_status->num_nodes;
				nodecount++) {
			if (quorum_status->nodelist[nodecount].nid ==
					nodeid) {
				if (quorum_status->nodelist[nodecount].state !=
					QUORUM_STATE_ONLINE) {
					skip_node = B_TRUE;
					break;
				}
			}
		}

		if (skip_node == B_TRUE) {
			clconf_iter_advance(currnodesi);
			continue;
		}

		ai = (scrgadm_adapterinfo_list_t *)
			malloc(sizeof (scrgadm_adapterinfo_list_t));
		if (ai == NULL) {
			status.err_code = SCHA_ERR_NOMEM;
			(void) sprintf(err_msg, dgettext(TEXT_DOMAIN,
				"Unexpected error occured.\n"));
			status.err_msg = strdup(err_msg);
			return (status);
		}

		bzero(ai, sizeof (*ai));

		if (aihead == NULL)
			aihead = ai;
		if (ailast != NULL)
			ailast->next = ai;
		ailast = ai;

		/*
		 * fill in the node name
		 */
		ai->ai_node = strdup(nodename);
		ai->ai_nodezone = strdup(nodename);
		if ((ai->ai_node == NULL) || (ai->ai_nodezone == NULL)) {
			status.err_code = SCHA_ERR_NOMEM;
			(void) sprintf(err_msg, dgettext(TEXT_DOMAIN,
				"Unexpected error occured.\n"));
			status.err_msg = strdup(err_msg);
			return (status);
		}

		/*
		 * get actual adapter info from PNM
		 * Access pnmd on the node using the private
		 * interconnect name
		 */
		pnm_ret = pnm_init(ai->ai_node);

		if (pnm_ret == 0) {
			if ((rc = pnm_adapterinfo_list(&ai->ai_list,
				B_FALSE)) != 0) {
				/*
				 * Internal error occurred
				 * Unable to get adapter list
				 */
				status.err_code = SCHA_ERR_INTERNAL;
				(void) sprintf(err_msg, dgettext(TEXT_DOMAIN,
					"Unable to get the adapter list.\n"));
				status.err_msg = strdup(err_msg);
				return (status);
			}
		} else {
			/*
			 * Internal error occurred
			 * Unable to get adapter list
			 */
			status.err_code = SCHA_ERR_INTERNAL;
			(void) sprintf(err_msg, dgettext(TEXT_DOMAIN,
				"Unable to get the adapter list.\n"));
			status.err_msg = strdup(err_msg);
			return (status);
		}

		pnm_fini();

		clconf_iter_advance(currnodesi);
	}

	ailist = aihead;

	/*
	 * Construct the  scrgadm_hostname_list_t
	 * with the ping_target to be validated
	 */
	hostnamelist = (scrgadm_hostname_list_t *)
			calloc(1, sizeof (scrgadm_hostname_list_t));

	if (hostnamelist) {
		hostnamelist->raw_name = strdup(ping_target);
		hostnamelist->next = NULL;
		hostnamelist->ip_addr = NULL;
		hostnamelist->ip6_addr = NULL;
	} else {
		status.err_code = SCHA_ERR_NOMEM;
		(void) sprintf(err_msg, dgettext(TEXT_DOMAIN,
			"Unexpected error occured.\n"));
		status.err_msg = strdup(err_msg);
		return (status);
	}

	/*
	 * validate_hostname_list method updates
	 * other fields of the hostnamelist structure
	 */
	status = validate_hostname_list(hostnamelist);
	if (status.err_code != SCHA_ERR_NOERR) {
		return (status);
	}

	/*
	 * Call the select_adapterinfo_by_hostname_list
	 * to validate if the given ping_target is resident
	 * on the same subnet as the cluster nodes are
	 */
	status = select_adapterinfo_by_hostname_list(ailist,
			hostnamelist);
	if (status.err_code != SCHA_ERR_NOERR) {
		(void) sprintf(err_msg, dgettext(TEXT_DOMAIN,
			"Ping target \"%s\" is not "
			"in the same primary subnet as the "
			"cluster nodes.\n"), ping_target);
		status.err_msg = strdup(err_msg);
		return (status);
	}

	return (status);
}
#endif /* WEAK_MEMBERSHIP */
