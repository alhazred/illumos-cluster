#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.23	09/04/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libscrgadm/Makefile.com
#

LIBRARY=	libscrgadm.a
VERS=		.1

OBJECTS=	scrgadm_util.o

# include library definitions
include ../../Makefile.lib

MAPFILE=	../common/mapfile-vers
$(WEAK_MEMBERSHIP) MAPFILE=	../common/mapfile-vers.s11

SRCS=		$(OBJECTS:%.o=../common/%.c)

LINTFILES	+= $(OBJECTS:%.o=%.ln)

SCRGADMLIBS	= -L$(VROOT)/usr/cluster/lib/rgm -lrtreg -lrgm -lpnm -lscconf -lclconf \
		-lc -lnsl -lsocket

LIBS =		$(DYNLIB)

# We need C99 features here. Unset the i386_SPACEFLAG
$(POST_S9_BUILD)i386_SPACEFLAG =

MTFLAG	=	-mt
CFLAGS +=	-v
CFLAGS64 +=	-v
CPPFLAGS +=	-I..
CPPFLAGS +=	-I. 
$(POST_S9_BUILD)CPPFLAGS +=	-D__C99FEATURES__ -D_STDC_C99
PMAP =	-M $(MAPFILE)
LDLIBS +=       $(SCRGADMLIBS) -lclcomm  
$(POST_S9_BUILD)LDLIBS += 	-lzccfg

.KEEP_STATE:

all: $(LIBS)

$(DYNLIB):	$(MAPFILE)

FRC:

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
