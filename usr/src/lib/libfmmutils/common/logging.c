/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)logging.c	1.3	08/05/20 SMI"

#include <sys/varargs.h>
#include <string.h>

#include <fmmutils.h>


static int log_level = LOG_ERR;
static char *log_tag = NULL;


void
fmm_log_get_level(int *level)
{
	*level = log_level;
}

void
fmm_log_set_level(int level)
{
	log_level = level;
}

void
fmm_log_set_tag(char *tag)
{
	log_tag = strdup(tag);
}

void
fmm_log(int const pri, char const * const fmt, ...)
{
	va_list ap;

	if (pri > log_level)
		return;

#ifdef linux
	va_start(ap, fmt);
#else /* linux */
	va_start(ap, (void) fmt); /*lint !e40 */
#endif /* linux */

	openlog(log_tag, LOG_CONS | LOG_PID, LOG_DAEMON);

#ifdef linux
	syslog(pri, fmt, ap);
#else	/* linux */
	vsyslog(pri, fmt, ap);
#endif	/* linux */
	closelog();

	va_end(ap);
}
