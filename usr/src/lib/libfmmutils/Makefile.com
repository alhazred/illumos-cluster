#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)Makefile.com	1.2	08/05/20 SMI"
#

#
# Set LIBRARY to the name of the static library (.a), even
# if only the dynamic library (.so) will be produced.
#
LIBRARY =	libfmmutils.a

#
# $(VERS) will be appended to the name of the dynamic library
# (libtemplate.so.1).
#
VERS =		.1

#
# List of objects for the library.
#
OBJECTS = \
	logging.o

SRCS = $(OBJECTS:%.o=%.c)
LINTFILES = $(OBJECTS:%.o=%.ln)

include ../../Makefile.lib

CPPFLAGS += -I$(ROOTCLUSTINC)/fmm

#
# Use "LIBS += $(DYNLIB) ..." to produce the static library
# AND the dynamic library.
# Use "LIBS = $(DYNLIB) ..." to produce only the dynamic
# library.
#
LIBS +=		$(DYNLIB)

PMAP =

#
# Libraries needed for linking $(LIBRARY) and $(DYNLIB).
# '+=' must be used rather than '='.
#
LDLIBS += -lc

CPPFLAGS += -D_REENTRANT -KPIC -DPIC -D_TS_ERRNO
CFLAGS += -D_REENTRANT -KPIC -DPIC -D_TS_ERRNO

OBJS_DIR = objs
OBJS_DIR += pics
.KEEP_STATE:

.PARALLEL: $(OBJECTS)

all: $(LIBS)

include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

