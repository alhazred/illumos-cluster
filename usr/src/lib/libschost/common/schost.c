/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)schost.c	1.4	08/05/20 SMI"

#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <dlfcn.h>
#include <errno.h>
#include <sys/utsname.h>
#include <sys/systeminfo.h>

/*lint -e611 */
/*
 * From FlexLint book
 * Warning(611) Suspicious cast -- Either a pointer to a function is
 * being cast to a pointer to an object or vice versa. This is
 * regarded as questionnable by ANSI standard. If this is not a user
 * error, suppress this warning.
 */

static int (*uname_handle)(struct utsname *);
static int (*sysinfo_handle)(int command, char *buf, long count);

int getloghostname(char *, size_t);

/*
 * Calls sysinfo() function to retrieve host name.
 */
int
gethostname(char *hostname, int namelen)
{
	return (sysinfo(SI_HOSTNAME, hostname, (long)namelen) == -1 ? -1 : 0);
}

/*
 * The symbol for uname differs based on the platform. Define
 * the symbol that is needed for dlsym to use.
 */
#if defined(__i386)
static char *unamesym = "_nuname";
#else
static char *unamesym = "uname";
#endif

/*
 * Calls the system provided uname() function to fill in the 'utsname'
 * structure provided as input.  Then, it copies the value of SC_LHOSTNAME
 * environment variable into the 'nodename' member of the structure.
 *
 * If the SC_LHOSTNAME variable is not set, the values retrieved by the system
 * provided implementation of uname() is passed unchanged.
 */
int
#if defined(__i386)
_nuname(struct utsname *name)
#else
uname(struct utsname *name)
#endif
{
	int rc;

	/*
	 * Get the next caller in the linking chain. We do this
	 * one time to minimize performance impact.
	 */
	if (uname_handle == NULL) {
		uname_handle = (int(*)(struct utsname *))
		    dlsym(RTLD_NEXT, unamesym);
		if (uname_handle == NULL) {
			return (-1);
		}
	}

	/*
	 * Call the underlying function and reset the 'nodename' member
	 * of the structure to the value of SC_LHOSTNAME environment variable,
	 * if SC_LHOSTNAME environment variable is set.
	 */
	if ((rc = uname_handle(name)) != -1) {
		if ((rc = getloghostname(name->nodename,
		    (size_t)SYS_NMLN)) == -1) {
			errno = EFAULT; /*lint !e746 */
		}
	}
	return (rc);
}

/*
 * If this function is called to fetch the value of the host name, then it
 * returns the value stored in SC_LHOSTNAME environment variable.
 *
 * If this function is called to fetch any other information, or the
 * SC_LHOSTNAME environment variable is not set, it calls the system provided
 * implementation of sysinfo().
 */
int
sysinfo(int command, char *buf, long count)
{

	int rc;

	if (command == SI_HOSTNAME) {
		buf[0] = NULL;
		if ((rc = getloghostname(buf, (size_t)count)) == -1) {
			errno = EFAULT;
		}

		/*
		 * If we have obtained the value of SC_LHOSTNAME environment
		 * variable, then return it.
		 */
		if (buf[0] != NULL) {
			return (rc);
		}
	}

	/*
	 * Get the next caller in the linking chain. We do this
	 * one time to minimize performance impact.
	 */
	if (sysinfo_handle == NULL) {
		sysinfo_handle = (int(*)(int, char *, long))
		    dlsym(RTLD_NEXT, "sysinfo");
		if (sysinfo_handle == NULL) {
			return (-1);
		}
	}

	/*
	 * Call system provided implementation of sysinfo().
	 */
	return (sysinfo_handle(command, buf, count));
}

/*
 * Return back the value of SC_LHOSTNAME environment variable, if it is set.
 *
 * If the size of the provided buffer is not enough to hold entire SC_LHOSTNAME
 * environment variable, we truncate the value of SC_LHOSTNAME and return -1.
 */
int
getloghostname(char *hostname, size_t namelen)
{
	char *hname = NULL;
	int rc = 0;

	hname = getenv("SC_LHOSTNAME");
	if (hname != NULL) {
		if (strlcpy(hostname, hname, (size_t)namelen)
			>= (size_t)namelen) {
			rc = -1;
		}
	}
	return (rc);
}
