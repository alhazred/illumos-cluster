//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _CL_CONTRACT_EVENT_LISTENER_H
#define	_CL_CONTRACT_EVENT_LISTENER_H

#pragma ident	"@(#)cl_contract_event_listener.h	1.2	08/07/23 SMI"

#include <sys/threadpool.h>
#include <libclcontract/cl_contract_table.h>
#include <libclcontract/cl_contract.h>

const int NUM_EVENT_THREADS = 16;

//
// event_task
//
// This task is used to represent one contract event
// that needs to be processed
//
class event_task : public defer_task {
public:
	event_task(ct_evthdl_t ev_in) : ev(ev_in) {};
	~event_task();

	void execute();
private:
	// Disallow assignment and pass by value
	event_task(const event_task &);

	// CSTYLED
	event_task &operator=(event_task &);

	ct_evthdl_t ev;
};

//
// The cl_contract_event_listener class listens for contract events
// for contracts owned by this process.
// All events are placed on one of the NUM_EVENT_THREAD
// threadpools for processing. The processing consists of looking up the
// event in the cl_contract_table by contract id and calling dispatch_event()
// on the resulting cl_contract object.
//
// The class follows the singleton pattern.
//
class cl_contract_event_listener {
public:
	static void create();
	static cl_contract_event_listener& instance();
	void listen_for_events();

protected:
	cl_contract_event_listener();

	// Don't provide implementations for these
	cl_contract_event_listener(const cl_contract_event_listener& src);
	cl_contract_event_listener& operator =
	    (const cl_contract_event_listener& rhs);

	// keep an array of single-threaded threadpools so we can control
	// the allocation of tasks to threads
	threadpool tps[NUM_EVENT_THREADS];
	pthread_t receptionist;

	static const int num_retries = 5;
	static const int backoff_time = 5;

	static cl_contract_event_listener* the_listener;
	static pthread_mutex_t create_lock;
};

#endif /* _CL_CONTRACT_EVENT_LISTENER_H */
