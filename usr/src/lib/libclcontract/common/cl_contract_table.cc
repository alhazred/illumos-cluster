//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_contract_table.cc	1.2	08/07/23 SMI"

#include <libclcontract/cl_contract_table.h>
#include <errno.h>
#include <sys/cl_assert.h>

using namespace std;

// The one cl_contract_table instance
cl_contract_table* cl_contract_table::the_table = NULL;
pthread_mutex_t cl_contract_table::create_lock = PTHREAD_MUTEX_INITIALIZER;

cl_contract_table&
cl_contract_table::instance()
{
	if (the_table == NULL) {
		create();
	}

	return (*the_table);
}

void
cl_contract_table::create()
{
	// allow only one thread at a time to attempt to create
	// the table in order to avoid creating multiple tables
	// in a race condition.
	CL_PANIC(pthread_mutex_lock(&create_lock) == 0);
	if (the_table == NULL) {
		the_table = new cl_contract_table();
	}
	CL_PANIC(pthread_mutex_unlock(&create_lock) == 0);
}

// All access to the maps protected by a single mutex

int
cl_contract_table::add_contract(smart_ptr<cl_contract> contract)
{
	int err = 0;

	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	// Figure out if we already have an entry with this key
	if (contracts.count(contract->get_nametag()) != 0) {
		CL_PANIC(pthread_mutex_unlock(&lock) == 0);
		return (1);
	}
	try {
		(void) contracts.insert(make_pair(contract->get_nametag(),
		    contract));
	// CSTYLED
	} catch (bad_alloc&) {
		err = ENOMEM;
	}
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (err);
}

int
cl_contract_table::add_mapping(ctid_t cid, const string &name)
{
	int err = 0;

	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	// Figure out if we already have an entry with this key
	if (names.count(cid) != 0) {
		CL_PANIC(pthread_mutex_unlock(&lock) == 0);
		return (1);
	}

	try {
		(void) names.insert(make_pair(cid, name));
	// CSTYLED
	} catch (bad_alloc&) {
		err = ENOMEM;
	}
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (err);
}

// erase() should never throw
int
cl_contract_table::remove_mapping(ctid_t cid)
{
	int num_removed;
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	num_removed = names.erase(cid);
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (num_removed);
}


int
cl_contract_table::lookup(const string &name, smart_ptr<cl_contract>& sp)
    const
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	std::map<string, smart_ptr<cl_contract> >::const_iterator it =
	    contracts.find(name);
	if (it == contracts.end()) {
		CL_PANIC(pthread_mutex_unlock(&lock) == 0);
		return (1);
	}
	sp = it->second;
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (0);
}

int
cl_contract_table::lookup(ctid_t cid, smart_ptr<cl_contract>& sp) const
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	std::map<ctid_t, string>::const_iterator it = names.find(cid);
	if (it == names.end()) {
		CL_PANIC(pthread_mutex_unlock(&lock) == 0);
		return (1);
	}

	// can't just call the other lookup here because we need to
	// do the whole thing atomically while holding the lock

	std::map<string, smart_ptr<cl_contract> >::const_iterator it2 =
	    contracts.find(it->second);
	if (it2 == contracts.end()) {
		CL_PANIC(pthread_mutex_unlock(&lock) == 0);
		return (1);
	}
	sp = it2->second;
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (0);
}

// erase() should never throw an exception
int
cl_contract_table::remove(const string &name)
{
	int num_removed;
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	num_removed = contracts.erase(name);
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (num_removed);
}

// construct a list of all the smart_ptrs currently stored

int
cl_contract_table::get_all(std::vector<smart_ptr<cl_contract> >& ret)
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	// resize the vector so we only have to worry about
	// exceptions once.
	try {
		ret.reserve(contracts.size());
	// CSTYLED
	} catch (bad_alloc&) {
		CL_PANIC(pthread_mutex_unlock(&lock) == 0);
		return (1);
	}

	// We need an explicit loop because we're inserting only one
	// part of the pair in the map into the vector.
	// CSTYLED
	for (std::map<string, smart_ptr<cl_contract> >::iterator it =
	    contracts.begin(); it != contracts.end(); ++it) {
		ret.push_back(it->second);
	}
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (0);
}

cl_contract_table::cl_contract_table()
{
	(void) pthread_mutex_init(&lock, NULL);
}
