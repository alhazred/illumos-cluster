/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cl_contract_main.cc	1.2	08/07/23 SMI"

#include <libclcontract/cl_contract_main.h>
#include <libclcontract/cl_contract_table.h>
#include "cl_contract_event_listener.h"
#include <malloc.h>

#include <new>
using namespace std;

//
// The only thing we need to do here is create the two singleton objects.
//
void
libclcontracts_initialize()
{
	cl_contract_table::create();
	cl_contract_event_listener::create();
}

//
// free_string_arr()
//
// Helper function that frees an array of char *s.
//
void
free_string_arr(char **arr)
{
	if (arr) {
		for (int i = 0; arr[i]; i++) {
			delete arr[i];
		}
		delete arr;
	}
}

void
free_string_arr_c(char **arr)
{
	if (arr) {
		for (int i = 0; arr[i]; i++) {
			free(arr[i]);
		}
		free(arr);
	}
}

//
// strdup_nothrow()
//
// Version of strdup() that uses nothrow new instead of malloc.
//
char *
strdup_nothrow(const char *src)
{
	if (src == NULL) {
		return (NULL);
	}

	// lint says 'nothrow' is a undeclared identifier
	char *dest = new (nothrow) char[strlen(src) + 1];	//lint !e40
	if (dest != NULL) {
		(void) strcpy(dest, src);
	}
	return (dest);
}
