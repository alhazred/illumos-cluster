//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_contract_event_listener.cc	1.3	08/07/28 SMI"

#include "cl_contract_event_listener.h"
#include <fcntl.h>

// The one cl_contract_event_listener object
cl_contract_event_listener* cl_contract_event_listener::the_listener = NULL;
pthread_mutex_t cl_contract_event_listener::create_lock =
    PTHREAD_MUTEX_INITIALIZER;

cl_contract_event_listener&
cl_contract_event_listener::instance()
{
	if (the_listener == NULL) {
		create();
	}

	return (*the_listener);
}

void
cl_contract_event_listener::create()
{
	// allow only one thread at a time to attempt to create
	// the listener in order to avoid creating multiple listeners
	// in a race condition.
	CL_PANIC(pthread_mutex_lock(&create_lock) == 0);
	if (the_listener == NULL) {
		the_listener = new cl_contract_event_listener();
	}
	CL_PANIC(pthread_mutex_unlock(&create_lock) == 0);
}

//
// Wrapper function for thread creation.
//
// Defined with "C" linkage because it's called by a C function
// (pthread_create).
//
extern "C" void *event_listener_wrapper(void *)
{
	cl_contract_event_listener::instance().listen_for_events();
	return (NULL);
}

cl_contract_event_listener::cl_contract_event_listener()
{
	char thr_pool_buf[128];

	// initialize the NUM_EVENT_THREADS thread pools
	for (int i = 0; i < NUM_EVENT_THREADS; i++) {
		// Construct the threadpool name
		(void) snprintf(thr_pool_buf, 128,
		    "libclcontract event listener %d", i);

		// Specify that the threadpool should not grow dynamically
		// and it should have one thread. Also specify the name.
		(void) tps[i].modify(false, 1, thr_pool_buf);
	}

	int ret;
	// Create the event receptionist thread
	if ((ret = pthread_create(&receptionist, NULL,
	    event_listener_wrapper, NULL)) != 0) {
		char *err_str = strerror(ret);
		os::sc_syslog_msg logger(syslog_tag, "", NULL);
		//
		// SCMSGS
		// @explanation
		// The libclcontract code was unable to create a thread
		// as pthread_create(3C) failed.
		// The message contains the system error.
		// Consequently, the process that is using libclcontract
		// was unable to receive contract events, and will exit.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "pthread_create error : %s", err_str);
		exit(ret);
	}
}

void
cl_contract_event_listener::listen_for_events()
{
	ct_evthdl_t ev;
	int err;
	int retries_attempted = 0;
	int cur_backoff = backoff_time;

	os::sc_syslog_msg logger(syslog_tag, "", NULL);

	// Open the pbundle file in ctfs for this process, which allows
	// us to receive all events for all contracts owned by this process.
	int efd = open64("/system/contract/process/pbundle", O_RDONLY);
	if (efd == -1) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// The libclcontract code was unable to open a file in the
		// /system/contract/process/ directory. Consequently, the
		// process that is using libclcontract was unable to
		// receive contract events. That process will exit.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "open64 pbundle: %s",
		    strerror(err));
		//
		// SCMSGS
		// @explanation
		// The libclcontract code is permanently unable to
		// read contract events. The process that is using
		// libclcontract will exit.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Unrecoverable "
		    "error: unable to read contract events.");

		// If we can't listen for events, we have serious problems
		exit(err);
	}

	// Don't need to bother setting close-on-exec on the fd
	// because forked children do that on all open fds before execing.

	while (true) {
		// Read the next event
		err = ct_event_read(efd, &ev);
		if (err) {
			//
			// SCMSGS
			// @explanation
			// The libclcontract code was unable to read
			// the next contract event. It will retry.
			// @user_action
			// Search for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) logger.log(LOG_ERR, MESSAGE, "ct_event_read: %s",
			    strerror(err));

			// If we've exceeded our retries, we have to exit
			if (retries_attempted >= num_retries) {
				(void) logger.log(
				    LOG_ERR, MESSAGE, "Unrecoverable "
				    "error: unable to read contract events.");
				exit(1);
			}

			// Sleep, then retry
			(void) sleep((unsigned int)cur_backoff);
			retries_attempted++;
			cur_backoff *= 2;

		} else {
			// reset the retry logic
			retries_attempted = 0;
			cur_backoff = backoff_time;

			//
			// Create a new task and stick it on the queue
			// for the threadpool to process
			//
			// In order to obtain "sticky" threads, such
			// that the same thread processes all events for
			// a given contract, we keep an array of single-thread
			// thread pools, and hash the contract id to a
			// threadpool.
			//
			event_task *et = new event_task(ev);
			ctid_t ev_ctid = ct_event_get_ctid(ev);

			//
			// lint says dbg_print_func is a undeclared identifier.
			// Hence masking the lint noise here.
			//lint -e40 -e10
			(*dbg_print_func)(NOGET("Queueing event for ctid %d\n"),
			    ev_ctid);
			//lint +e40 +e10

			tps[ev_ctid % NUM_EVENT_THREADS].defer_processing(et);
		}
	}
}

void
event_task::execute()
{
	ctid_t ev_ctid = ct_event_get_ctid(ev);

	smart_ptr<cl_contract> sp_clcontract(NULL);

	// Look up by ctid_t
	if (cl_contract_table::instance().lookup(ev_ctid, sp_clcontract) != 0) {
		//
		// That ctid_t does not exist.
		//
		// lint says dbg_print_func is a undeclared identifier.
		// Hence masking the lint noise here.
		//lint -e40 -e10
		(*dbg_print_func)(
		    NOGET("No cl_contract for ctid %d\n"), ev_ctid);
		//lint +e40 +e10

		delete this;
		return;
	}

	// dispatch the event
	sp_clcontract->event_dispatch(ev);
	delete this;
}

event_task::~event_task()
{
	ct_event_free(ev);
}
