/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _CL_SHARED_H
#define	_CL_SHARED_H

#pragma ident	"@(#)cl_shared.h	1.2	08/07/23 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <locale.h>
#include <strings.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <synch.h>

#include <sys/sol_version.h>

#include <libclcontract/cl_contract_main.h>

/*
 * Used to mark messages that are considered "normal" and would have
 * been targets for i18n using gettext(). SYSTEXT distinguishes this
 * category of messages from unusual or failure messages.
 */
#define	SYSTEXT(s)	s

extern int debug;

/* these functions are called from a file with C linkage */

void cl_contract_free_sema(sema_t *sema);
int cl_contract_alloc_sema(sema_t **sema);

#ifdef __cplusplus
}
#endif

#endif /* _CL_SHARED_H */
