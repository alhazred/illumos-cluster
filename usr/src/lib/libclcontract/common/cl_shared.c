/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cl_shared.c	1.2	08/07/23 SMI"

#include "cl_shared.h"
#include <sys/sc_syslog_msg.h>
#include <sys/mman.h>

/* Populated by whatever tag the user of this library passes in for use */
extern char *syslog_tag;

/*
 * Allocate a semaphore in mapped memory to be shared between this process
 * and a child process.
 */
int
cl_contract_alloc_sema(sema_t **sema)
{
	int err;
	sema_t *ptr;
	sc_syslog_msg_handle_t sys_handle;

	/*
	 * Initialize the semaphore to NULL in case we fail to initialize
	 * it in the code below.
	 */
	*sema = NULL;

	/*
	 * The interprocess semaphores need to reside in
	 * a shared mapping between the two processes.
	 */
	if ((ptr = (sema_t *)mmap(0, sizeof (sema_t), PROT_READ|PROT_WRITE,
	    MAP_SHARED|MAP_ANON, -1, NULL)) == MAP_FAILED) {
		/*
		 * lint doesn't understand errno
		 */
		err = errno; /*lint !e746 */
		(void) sc_syslog_msg_initialize(&sys_handle, syslog_tag, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The libclcontract code was not able to allocate shared memory
		 * for a semaphore, possibly due to low memory, and the system
		 * error is shown. The server does not perform the action
		 * requested by the client.
		 * An error message is also output to syslog.
		 * @user_action
		 * Determine if the machine is running out of memory. If this
		 * is not the case, save the /var/adm/messages file. Contact
		 * your authorized Sun service provider to determine whether a
		 * workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "mmap failure: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		return (err);
	}

	/*
	 * Note that sema_init returns an error code directly, not via
	 * errno.
	 */
	if ((err = sema_init(ptr, 0, USYNC_PROCESS, NULL)) != 0) {
		(void) sc_syslog_msg_initialize(&sys_handle, syslog_tag, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The libclcontract code was not able to initialize a
		 * semaphore, possibly due to low memory,
		 * and the system error is shown.
		 * The server does not perform the action requested by the
		 * client. An error message is also output to syslog.
		 * @user_action
		 * Determine if the machine is running out of memory. If this
		 * is not the case, save the /var/adm/messages file. Contact
		 * your authorized Sun service provider to determine whether a
		 * workaround or patch is available.
		 */
		(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
		    "sema_init failure: %s", strerror(err));
		sc_syslog_msg_done(&sys_handle);
		(void) munmap((caddr_t)ptr, sizeof (sema_t));
		return (err);
	}

	*sema = ptr;

	return (0);
}

void
cl_contract_free_sema(sema_t *sema)
{
	sc_syslog_msg_handle_t sys_handle;
	int	err;

	if (sema) {
		/*
		 * Destroy the semaphore, but ignore the return value.
		 * There's nothing we can do if it fails.
		 */
		(void) sema_destroy(sema);
		if (munmap((caddr_t)sema, sizeof (sema_t)) == -1) {
			err = errno;
			(void) sc_syslog_msg_initialize(
			    &sys_handle, syslog_tag, "");
			/*
			 * SCMSGS
			 * @explanation
			 * The libclcontract code was not able to delete shared
			 * memory for a semaphore, possibly due to low memory,
			 * and the system error is shown. This is part of the
			 * cleanup after a client call, so the operation might
			 * have gone through. An error message is output to
			 * syslog.
			 * @user_action
			 * Determine if the machine is running out of memory.
			 * If this is not the case, save the /var/adm/messages
			 * file. Contact your authorized Sun service provider
			 * to determine whether a workaround or patch is
			 * available.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_ERR, MESSAGE,
			    "munmap failure: %s", strerror(err));
			sc_syslog_msg_done(&sys_handle);
		}
	}
}
