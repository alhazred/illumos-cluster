//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)cl_contract.cc	1.3	08/07/28 SMI"

#include <libclcontract/cl_contract.h>
#include "cl_shared.h"
#include <libclcontract/cl_contract_main.h>

#include <errno.h>

#include <sys/os.h>
#include <sys/cl_assert.h>
#include <sys/sc_syslog_msg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>

#include <sys/contract/process.h>
#include <libcontract.h>

#include <libclcontract/cl_contract_table.h>

using namespace std;

//
// lint tool considers dbg_print_func_t as an identifier, although
// it has been typedef'ed as a function pointer prototype in cl_contract.h
// Hence lint tool throws warnings/errors all throughout this file
// wherever any identifier is declared with type dbg_print_func_t.
// We mask all such lint noise here.
//lint -e129 -e19 -e40 -e601 -e110 -e48 -e10 -e530 -e522
dbg_print_func_t dbg_print_func;

//
// The global syslog tag to be used when syslogging messages.
// This is set by what the user of this library specifies.
//
char *syslog_tag = NULL;

//
// cl_contract constructor
//
cl_contract::cl_contract(
    char *identifier_in, char **cmd_in,
    uint_t fatal, uint_t critical, uint_t informative, int &err,
    char *syslog_tag_in, dbg_print_func_t func,
    char **env_in, const char *pwd_in, const char *path_in):
    wdir(NULL), path(NULL), nametag("")
{
	ASSERT(func != NULL);
	dbg_print_func = func;
	(*dbg_print_func)("cl_contract constructor for %s\n", identifier_in);

	contract_id = 0;
	contract_fd = 0;
	stop_monitoring = false;
	is_running = false;
	incr_restart_cnt_flag = false;
	parent_pid = 0;
	parent_return_code = 0;
	waiters = 0;

	fatal_events = fatal;
	critical_events = critical;
	informative_events = informative;

	cmd = cmd_in;
	syslog_tag = syslog_tag_in;
	err = 0;
	lock_inited = cond_inited = false;
	sema_p2c = NULL;

	//
	// Create the command string for event generation.
	// Strings are default initialized to "", so we can append
	// to it here.
	//
	for (int i = 0; cmd[i] != NULL; ++i) {
		if (i != 0) {
			cmd_string += " ";
		}
		cmd_string += cmd[i];
	}

	env = env_in;

	if (pwd_in && *pwd_in) {
		wdir = strdup_nothrow(pwd_in);
		if (wdir == NULL) {
			err = ENOMEM;
			return;
		}
	}

	if (path_in && *path_in) {
		path = strdup_nothrow(path_in);
		if (path == NULL) {
			err = ENOMEM;
			return;
		}
	}

	if (identifier_in && *identifier_in) {
		//
		// nametag is a string, not a char *.
		// Assignment to strings does a deep copy.
		//
		nametag = identifier_in;
	} else {
		//
		// Subsequent code cannot handle a NULL here,
		// so we must bail out.
		//
		err = EINVAL;
		// Let the destructor clean up
		return;
	}

	err = pthread_mutex_init(&lock, NULL);
	if (err != 0) {
		return;
	}
	lock_inited = true;

	err = pthread_cond_init(&cond, NULL);
	if (err != 0) {
		return;
	}
	cond_inited = true;
	err = cl_contract_alloc_sema(&sema_p2c);
	if (err != 0) {
		return;
	}
}

//
// Destructor
//
// lint throws informational message (1740) that 'env' and 'cmd' are not freed.
// However, we are freeing these variables by calling another routine
// in the destructor. So, we mask this lint message.
//lint -e1740
cl_contract::~cl_contract()
{
	//
	// lint says the debug print function might throw exceptions
	// and hence should not be called in a destructor.
	//lint -e1551
	(*dbg_print_func)("cl_contract destructor for %s\n",
	    nametag.c_str());
	//lint +e1551

	delete path;
	delete wdir;
	free_string_arr(env);
	free_string_arr(cmd);

	if (lock_inited) {
		(void) pthread_mutex_destroy(&lock);
	}
	if (cond_inited) {
		(void) pthread_cond_destroy(&cond);
	}
	cl_contract_free_sema(sema_p2c);
}
//lint +e1740

//
// start()
//
// Simply a wrapper for start_process() that grabs the lock and verifies
// that a process is not currently running and we're not supposed to stop
// monitoring.
//
// Returns 0 if the process forks correctly, a non-0 errno otherwise.
//
int
cl_contract::start(pid_t &forked_pid)
{
	int ret = 0;
	forked_pid = 0;

	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	if (is_running) {
		// This should never happen
		os::sc_syslog_msg logger(syslog_tag, "", NULL);
		//
		// SCMSGS
		// @explanation
		// libclcontract code encountered an internal logic error.
		// There should be no impact on the processes being monitored
		// by the process that is using libclcontract.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Internal error: trying to "
		    "start nametag %s that is already running",
		    nametag.c_str());
		ret = EINPROGRESS;

	} else if (stop_monitoring) {
		//
		// This could happen when called by the throttle_wait thread,
		// if the user stops monitoring while the tag is in throttle
		// wait.
		//
		(*dbg_print_func)("Not starting %s because stop_monitoring "
		    "is set.\n", nametag.c_str());
		ret = ECANCELED;
	} else {
		(*dbg_print_func)("starting\n");
		for (char** aptr = cmd; *aptr; aptr++) {
			(*dbg_print_func)("\t%s\n", *aptr);
		}
		(*dbg_print_func)("");

		ret = start_process(cmd, forked_pid);
	}
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (ret);
}

//
// start_process()
//
// The meat of the start functionality -- called by start()
//
// forks the specified cmd, which is expected to be the user-specified
// "real" command
//
// Calls helper methods to handle the contracts stuff.
//
// Parent sets is_running to true and stores the parent_pid.
// Child calls setup_child() to setup and exec().
//
// Caller should hold the lock.
//
int
cl_contract::start_process(char **cmd_to_run, pid_t &forked_pid)
{
	int err;
	forked_pid = 0;

	// Configure the process template for the new process tree
	if ((err = set_up_process_template()) != 0) {
		return (err);
	}

	// Reset these members in case they were left over from the
	// previous process tree
	parent_return_code = 0;
	parent_pid = 0;

	//
	// Note that we use fork1 instead of fork to duplicate
	// only the calling lwp. Because we link with pthreads, this behavior
	// would be the same with fork, but we use fork1 for explicitness.
	//
	if ((parent_pid = fork1()) != 0) {
		// Parent
		if (parent_pid == -1) {
			// major error
			err = errno;
			os::sc_syslog_msg logger(syslog_tag, "", NULL);
			//
			// SCMSGS
			// @explanation
			// The libclcontract code was not able to fork
			// the specified process.
			// The message contains the system error.
			// The server process (which is using libclcontract)
			// does not perform the action requested by the client,
			// and an error message is output to syslog.
			// @user_action
			// Determine if the machine is running out of memory.
			// If this is not the case, save the /var/adm/messages
			// file. Contact your authorized Sun service provider
			// to determine whether a workaround or patch is
			// available.
			//
			(void) logger.log(LOG_ERR, MESSAGE, "fork error: %s",
			    strerror(err));
			parent_pid = 0;
			return (err);
		}

		(*dbg_print_func)("Forked process %d\n", parent_pid);
		forked_pid = parent_pid;

		//
		// Add the child process (with a depth of 0) to the
		// list of pids
		try {
			procs.insert(make_pair(parent_pid, 0));
		} catch(bad_alloc&) {
			(*dbg_print_func)("Unrecoverable error: "
			    "sending KILL to %d\n", parent_pid);
			(void) send_signal(parent_pid, SIGKILL);
			return (ENOMEM);
		}

		if ((err = retrieve_process_template_info()) != 0) {
			//
			// Something went wrong with the template
			// stuff. Kill the parent process we just
			// forked. We know it hasn't execed yet,
			// so we don't have to worry about child
			// processes.
			//
			(*dbg_print_func)("Unrecoverable error: "
			    "sending KILL to %d\n", parent_pid);
			(void) send_signal(parent_pid, SIGKILL);
			return (err);
		}

		// started successfully
		is_running = true;

		(*dbg_print_func)("%s: everything's ok: post to child.\n",
		    nametag.c_str());

		//
		// Let child continue execution (into exec).
		// Note that sema_post returns an error code directly,
		// not via errno.
		//
		if ((err = sema_post(sema_p2c)) != 0) {
			os::sc_syslog_msg logger(syslog_tag, "", NULL);
			//
			// SCMSGS
			// @explanation
			// The libclcontract code was not able to act on a
			// semaphore. The message contains the system error.
			// The server process (which is using libclcontract)
			// does not perform the action requested by the client,
			// and an error message is output to syslog.
			// @user_action
			// Determine if the machine is running out of memory.
			// If this is not the case, save the /var/adm/messages
			// file. Contact your authorized Sun service provider
			// to determine whether a workaround or patch is
			// available.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "sema_post parent: %s", strerror(err));

			//
			// Kill the child so it doesn't wait on semaphore
			// forever
			//
			(void) send_signal(parent_pid, SIGKILL);
			return (err);
		}

	} else {
		// Child
		setup_child();

		//
		// Wait here until our parent has had a chance to add
		// the contract mapping to the table. We don't want to exec
		// until then, because we might fork immediately
		// afterward, generating an event which would need to be
		// handled.
		//
		// Note that sema_wait returns an error code
		// directly, not via errno.
		//
		while ((err = sema_wait(sema_p2c)) != 0) {
			//
			// The only error code that we expect is EINTR.
			// If we get anything else, it's a serious error.
			// With EINTR, we just try the sema_wait again.
			//
			if (err == EINTR) {
				continue;
			}

			os::sc_syslog_msg logger(syslog_tag, "", NULL);
			//
			// SCMSGS
			// @explanation
			// The libclcontract code was not able to act on a
			// semaphore. The message contains the system error.
			// The server process (which is using libclcontract)
			// does not perform the action requested by the client,
			// and an error message is output to syslog.
			// @user_action
			// Save the /var/adm/messages file. Contact your
			// authorized Sun service provider to determine
			// whether a workaround or patch is available.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "sema_wait child: %s", strerror(err));
			_exit(1);
		}

		(*dbg_print_func)("%s: got the post -- about to exec\n",
		    nametag.c_str());

		//
		// if environment settings are passed in,
		// call execve and pass the environment; else call
		// execvp, and inherit the parent's environment, plus
		// the path set to that of the caller.
		//
		if (env) {
			if (execve(cmd_to_run[0], cmd_to_run, env) == -1) {
				err = errno;
				os::sc_syslog_msg logger(syslog_tag, "", NULL);
				//
				// SCMSGS
				// @explanation
				// The libclcontract code was not able to exec
				// the specified process, possibly due to bad
				// arguments. The message contains the system
				// error. The server process (which is using
				// libclcontract) does not perform the
				// action requested by the client, and an
				// error message is output to syslog.
				// @user_action
				// Verify that the file path to be executed
				// exists. If all looks correct, save the
				// /var/adm/messages file. Contact your
				// authorized Sun service provider to
				// determine whether a workaround or patch is
				// available.
				//
				(void) logger.log(LOG_ERR, MESSAGE, "Unable to "
				    "execve %s: %s", cmd_to_run[0],
				    strerror(err));
				_exit(1);
			}
		} else {
			if (execvp(cmd_to_run[0], cmd_to_run) == -1) {
				err = errno;
				os::sc_syslog_msg logger(syslog_tag, "", NULL);
				//
				// SCMSGS
				// @explanation
				// The libclcontract code was not able to exec
				// the specified process, possibly due to bad
				// arguments. The message contains the system
				// error. The server process (which is using
				// libclcontract) does not perform the
				// action requested by the client, and an
				// error message is output to syslog.
				// @user_action
				// Verify that the file path to be executed
				// exists. If all looks correct, save the
				// /var/adm/messages file. Contact your
				// authorized Sun service provider to
				// determine whether a workaround or patch is
				// available.
				//
				(void) logger.log(LOG_ERR, MESSAGE, "Unable to "
				    "execvp %s: %s", cmd_to_run[0],
				    strerror(err));
				_exit(1);
			}
		}

		// NOTREACHED
		_exit(1);
	}

	if (incr_restart_cnt_flag) {
		incr_restart_cnt_flag = B_FALSE;
		if ((err =
		    incr_restart_and_generate_event(B_FALSE)) != 0) {
			return (err);
		}
	}
	return (0);
}

int
cl_contract::incr_restart_and_generate_event(boolean_t)
{
	(*dbg_print_func)(
	    "incr_restart_and_generate_event of base class invoked\n");
	return (0);
}

//
// set_up_process_template()
//
// Makes active a process template with the following configuration:
// Flags: "Process Group Only"
// Fatal errors : as provided by user of libclcontract
// Critical Events: as provided by user of libclcontract
// Informative Events: as provided by user of libclcontract
//
// Returns 0 on success. Returns an errno error code on error.
//
int
cl_contract::set_up_process_template()
{
	int fd, err;
	os::sc_syslog_msg logger(syslog_tag, "", NULL);

	//
	// Set up the process template
	//
	fd = open64("/system/contract/process/template", O_RDWR);
	if (fd == -1) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// The libclcontract code was unable to open a file in the
		// /system/contract/process/ directory. Consequently, the
		// process that is using libclcontract was unable to
		// launch the requested process under its control.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. Retry the requested action.
		// If there are many instances of the message,
		// there is a problem with the contract file system.
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "open64 while setting up "
		    "process template: %s", strerror(err));
		return (err);
	}

	// Don't need to bother closing on exec because child does that
	// on all open fds before execing.

	//
	// Set the "process group only" flag so that only processes in the
	// same process group are killed if one process receives a fatal
	// error.
	//
	if ((err = ct_pr_tmpl_set_param(fd, CT_PR_PGRPONLY)) != 0) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// The libclcontract code was unable to configure a process
		// template in which to launch the requested service.
		// Consequently, the process that is using libclcontract
		// was unable to launch the process under its control.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. Retry the requested action.
		// If there are many instances of the message,
		// there is a problem with the contract file system.
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "ct_pr_tmpl_set_param: %s",
		    strerror(err));
		return (err);
	}

	if ((err = ct_pr_tmpl_set_fatal(fd, fatal_events)) != 0) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// The libclcontract code was unable to configure a process
		// template in which to launch the requested service.
		// Consequently, the process that is using libclcontract
		// was unable to launch the process under its control.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. Retry the requested action.
		// If there are many instances of the message,
		// there is a problem with the contract file system.
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "ct_pr_tmpl_set_fatal: %s",
		    strerror(err));
		return (err);
	}

	if ((err = ct_tmpl_set_critical(fd, critical_events)) != 0) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// The libclcontract code was unable to configure a process
		// template in which to launch the requested service.
		// Consequently, the process that is using libclcontract
		// was unable to launch the process under its control.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. Retry the requested action.
		// If there are many instances of the message,
		// there is a problem with the contract file system.
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "ct_tmpl_set_critical: %s",
		    strerror(err));
		return (err);
	}

	if ((err = ct_tmpl_set_informative(fd, informative_events)) != 0) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// The libclcontract code was unable to configure a process
		// template in which to launch the requested service.
		// Consequently, the process that is using libclcontract
		// was unable to launch the process under its control.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. Retry the requested action.
		// If there are many instances of the message,
		// there is a problem with the contract file system.
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "ct_tmpl_set_informative: %s", strerror(err));
		return (err);
	}

	if ((err = ct_tmpl_activate(fd)) != 0) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// The libclcontract code was unable to configure a process
		// template in which to launch the requested service.
		// Consequently, the process that is using libclcontract
		// was unable to launch the process under its control.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. Retry the requested action.
		// If there are many instances of the message,
		// there is a problem with the contract file system.
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "ct_tmpl_activate: %s", strerror(err));
		return (err);
	}

	// I assume it's safe to close it here
	(void) close(fd);

	return (0);
}

//
// retrieve_process_template_info()
//
// Obtains the contract id of the new contract created by the fork,
// opens the ctl file for the contract, and adds the contract->nametag
// mapping to the contract_table.
//
// Returns 0 on success. Returns an error error code on error.
//
int
cl_contract::retrieve_process_template_info()
{
	ct_stathdl_t st;
	int statusfd;
	os::sc_syslog_msg logger(syslog_tag, "", NULL);
	int err;

	// get the id of the contract we just created.
	if ((statusfd = open64("/system/contract/process/latest",
	    O_RDONLY)) == -1) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// The libclcontract code was unable to open a file in the
		// /system/contract/process/ directory.
		// Consequently, the process that is using libclcontract
		// was unable to launch the requested process under its control.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. Retry the requested action.
		// If there are many instances of the message,
		// there is a problem with the contract file system.
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "open64 while retrieving "
		    "template info: %s", strerror(err));
		return (err);
	}

	if ((err = ct_status_read(statusfd, CTD_COMMON, &st)) != 0) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// The libclcontract code was unable to retrieve the status
		// of the newly created process template.
		// The libclcontract code will kill the newly forked process
		// because the process using the libclcontract code is unable
		// to monitor the forked process.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. Retry the requested action.
		// If there are many instances of the message,
		// there is a problem with the contract file system.
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "ct_status_read: %s",
		    strerror(err));
		return (err);
	}

	contract_id = ct_status_get_id(st);
	ct_status_free(st);
	(void) close(statusfd);

	if ((contract_fd = contract_open(contract_id, "ctl", O_WRONLY)) == -1) {
		err = errno;
		//
		// SCMSGS
		// @explanation
		// The libclcontract code was unable to retrieve the status
		// of the newly created process template.
		// The libclcontract code will kill the newly forked process
		// because the process using the libclcontract code is unable
		// to monitor the forked process.
		// @user_action
		// If the message is isolated, it was probably a transient
		// error. Retry the requested action.
		// If there are many instances of the message,
		// there is a problem with the contract file system.
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "contract_open: %s",
		    strerror(err));
		return (err);
	}

	// Add the contract to name mapping
	return (cl_contract_table::instance().add_mapping(contract_id,
	    nametag));
}

//
// contract_open()
//
// cribbed from libcontract_priv.c
// The code borrowed is not "outside-Sun" code.
//
// static
int
cl_contract::contract_open(ctid_t ctid, const char *file, int oflag)
{
	char ct_file_path[PATH_MAX];
	int n, fd;

	CL_PANIC((oflag & O_CREAT) == 0);

	n = snprintf(ct_file_path, PATH_MAX, "/system/contract/process/%ld/%s",
	    ctid, file);
	if (n >= PATH_MAX) {
		errno = ENAMETOOLONG;
		return (-1);
	}

	fd = open64(ct_file_path, oflag);
	//
	// don't need to bother setting close on exec because we do
	// it for all files after a fork.
	//
	return (fd);
}

//
// setup_child()
//
// Called by a newly forked child process.
//
void
cl_contract::setup_child()
{
	(*dbg_print_func)("setup_child of base class invoked\n");
	closefrom(3);	// Close all open file descriptors >= 3
}

// static
char **
cl_contract::copy_char_arr(char** src, int &length)
{
	char **res;
	char **aptr;
	uint_t i;

	// loop body intentionally empty
	for (aptr = src, i = 0; aptr && *aptr; aptr++, i++);
	res = (char **)malloc(i * sizeof (char *));
	if (res == NULL) {
		//
		// Here we haven't successfully malloced anything
		// before so there's nothing to free.
		//
		length = -1;
		return (NULL);
	}
	length = (int)i;

	for (aptr = src, i = 0; aptr && *aptr; aptr++, i++) {
		res[i] = strdup(*aptr);
		if (res[i] == NULL) {
			//
			// Here we have to free what was previously
			// allocated.
			//
			free_string_arr_c(res);
			length = -1;
			return (NULL);
		}
	}
	return (res);
}

char**
cl_contract::get_cmd_copy(int &length)
{
	char **res;
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	res = copy_char_arr(cmd, length);
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (res);
}

char**
cl_contract::get_env_copy(int &length)
{
	char **res = NULL;
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	// The env is allowed to be NULL if there were no environment vars
	if (env == NULL) {
		length = 0;
	} else {
		res = copy_char_arr(env, length);
	}
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	return (res);
}

void
cl_contract::stop()
{
	CL_PANIC(pthread_mutex_lock(&lock) == 0);
	(*dbg_print_func)("%s: stop()\n", nametag.c_str());
	stop_monitoring = true;
	// If it's not running, it's in throttle_wait. We need to
	// end the monitoring ourself.
	if (!is_running) {
		end_monitoring();
	}
	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
}

int
cl_contract::wait(int seconds)
{
	timestruc_t timeout;
	int err = 0;

	CL_PANIC(pthread_mutex_lock(&lock) == 0);

	(*dbg_print_func)("%s: wait()\n", nametag.c_str());

	// pre-check our condition
	if (!is_running) {
		CL_PANIC(pthread_mutex_unlock(&lock) == 0);
		(*dbg_print_func)("%s: not running\n", nametag.c_str());
		return (0);
	}

	//
	// Wait the specified time, or until all processes exit.
	//
	timeout.tv_sec = time(NULL);
	if (timeout.tv_sec == (time_t)-1) {
		err = errno;
		os::sc_syslog_msg logger(syslog_tag, "", NULL);
		//
		// SCMSGS
		// @explanation
		// The time(2) function failed with the specified error. This
		// error will cause the process that is using libclcontract
		// to return immediately from a wait() command.
		// @user_action
		// Search for other syslog error messages on the same node.
		// Save a copy of the /var/adm/messages files on all nodes,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "time in wait(): %s",
		    strerror(err));
		CL_PANIC(pthread_mutex_unlock(&lock) == 0);
		return (err);
	}

	timeout.tv_sec += seconds;
	timeout.tv_nsec = 0;

	//
	// Increment the number of waiters so the thread that broadcasts
	// knows to wait for us before restarting the process and resetting
	// is_running to true.
	//
	waiters++;
	// Wait for is_running to be false.
	while (is_running) {
		//
		// If seconds is -1, we're supposed to wait forever,
		// so just use a normal pthread_cond_wait.
		//
		// If seconds is not -1, we are to wait for the condition
		// and the timeout, so use a pthread_cond_timedwait.
		//
		if (seconds == -1) {
			err = pthread_cond_wait(&cond, &lock);
		} else {
			err = pthread_cond_timedwait(&cond, &lock, &timeout);
		}
		// We break for any error, not just ETIMEDOUT
		if (err != 0) {
			break;
		}
	}
	waiters--;

	//
	// ETIMEDOUT prints a confusing message:
	//
	//	"Connection timed out"
	//
	// We'll convert it to ETIME to make it clearer:
	//
	//	"timer expired"
	//
	//
	if (err == ETIMEDOUT) {
		err = ETIME;
	}

	CL_PANIC(pthread_mutex_unlock(&lock) == 0);
	(*dbg_print_func)("%s: done waiting; err = %d\n", nametag.c_str(), err);
	return (err);
}

//
// event_dispatch()
// This method is intended to be overridden by derived classes.
//
void
cl_contract::event_dispatch(ct_evthdl_t ev)
{
	(*dbg_print_func)("event_dispatch() of cl_contract class called\n");
}

//
// ack_event()
//
// Assumes the lock is held.
// Acks the specified event on the contract_fd.
//
void
cl_contract::ack_event(ct_evthdl_t ev)
{
	(*dbg_print_func)("%s: acking event %d\n", nametag.c_str(),
	    ct_event_get_type(ev));
	ctevid_t evid = ct_event_get_evid(ev);
	(void) ct_ctl_ack(contract_fd, evid);
}

//
// end_monitoring()
//
// Assumes the lock is held
// Just removes this object from the cl_contract_table, and removes the
// contract id / nametag mapping if one exists still.
//
void
cl_contract::end_monitoring()
{
	(*dbg_print_func)("end_monitoring() for %s\n", nametag.c_str());

	if (contract_id != 0) {
		// Remove the contract / nametag mapping
		// from the contract table
		(void) cl_contract_table::instance().remove_mapping(
		    contract_id);
	}

	// remove this entry from the table
	(void) cl_contract_table::instance().remove(nametag);

	// Don't need to worry about deleting this object
	// because the smart pointers should take care of it
}

//
// Assumes the lock is held
//
// Sends the specified signal to the specified process id.
// Returns 0 on success. Returns an errno error code on failure.
// If kill() fails with ESRCH, returns 0.
//
int
cl_contract::send_signal(pid_t pid_to_kill, int signal_to_send) const
{
	int err1 = 0;

	(*dbg_print_func)("Sending signal %d to process %d\n",
	    signal_to_send, pid_to_kill);

	//
	// If kill fails with ESRCH, we ignore
	// the error because the process has
	// exited while we walk the list.
	// Otherwise, print message, set result
	// and return.
	//
	if (kill(pid_to_kill, signal_to_send) != 0) {
		err1 = errno;
		os::sc_syslog_msg logger(syslog_tag, "", NULL);
		//
		// SCMSGS
		// @explanation
		// An error occured while the process using libclcontract
		// attempted to send a signal to one of the processes
		// of the given tag. The reason for the failure is also given.
		// @user_action
		// Save the /var/adm/messages file. Contact your authorized
		// Sun service provider to determine whether a workaround or
		// patch is available.
		//
		(void) logger.log(err1 == ESRCH ? LOG_NOTICE : LOG_ERR, MESSAGE,
		    "Error signaling <%s>: %s",
		    nametag.c_str(), strerror(err1));

		// Don't return ESRCH
		if (err1 == ESRCH) {
			err1 = 0;
		}
	}
	return (err1);
}
//lint +e129 +e19 +e40 +e601 +e110 +e48 +e10 +e530 +e522
