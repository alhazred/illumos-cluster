#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.3	08/07/26 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libclcontract/Makefile.com
#
include $(SRC)/Makefile.master

LIBRARYCCC= libclcontract.a
VERS= .1

#include $(SRC)/common/cl/Makefile.files

OBJECTS += cl_contract.o cl_contract_table.o cl_contract_main.o cl_shared.o cl_contract_event_listener.o

include ../../Makefile.lib

CHECK_FILES += $(CHECKHDRS:%.h=%.h_check)
	       
CC_PICFLAGS = -KPIC

SRCS =		$(OBJECTS:%.o=../common/%.c)

#MTFLAG	= -mt
CPPFLAGS += $(CL_CPPFLAGS) -I$(SRC)/common/cl -I.

#
# Overrides
#
LIBS = $(DYNLIBCCC) $(LIBRARYCCC)

LDLIBS += -lclos -lpthread -ldl -lc -lclcomm_compat
LDLIBS += -lcontract

CLEANFILES =
LINTFILES += $(OBJECTS:%.o=%.ln)

PMAP =

OBJS_DIR = objs pics

.KEEP_STATE:

.NO_PARALLEL:

all: $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<
