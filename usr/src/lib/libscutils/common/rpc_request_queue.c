/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rpc_request_queue.c	1.3	08/05/20 SMI"

/*
 * rpc_request_queue.c - This file contains codes to manipulate
 *                       rpc client request queue.
 */

#include <rgm/rpc_services.h>
#include <sys/sc_syslog_msg.h>
#include <semaphore.h>
#include <pthread.h>
#include <errno.h>
#include "rpc_request_queue.h"
#include <assert.h>

static rpc_req_queue_t *rpc_reqq_head;
static rpc_req_queue_t *rpc_reqq_tail;
static int rpc_reqq_count;
static sem_t rpc_reqq_sem;
static pthread_mutex_t rpc_reqq_mutex = PTHREAD_MUTEX_INITIALIZER;
extern int debug;

static rpc_error_t rpc_req_queue_lock();
static rpc_error_t rpc_req_queue_unlock();
static rpc_error_t rpc_req_queue_wait();
static rpc_error_t rpc_req_queue_post();

/* for tracking of strings that are not translated */
#define	NOGET(s)	s

/*
 * Initialize the rpc request queue
 */
rpc_error_t
rpc_init_req_queue()
{
	rpc_error_t res = RPC_SERVICES_OK;

	rpc_reqq_head = NULL;
	rpc_reqq_tail = NULL;
	rpc_reqq_count = 0;

	/*
	 * Initialize a semaphore
	 */
	res = sem_to_rpc_error(sem_init(&rpc_reqq_sem, 0, 0));
	return (res);
}

/*
 * Lock the mutex for request queue
 */
static rpc_error_t
rpc_req_queue_lock()
{
	int res;
	if (debug) {
		dbg_msgout_nosyslog(NOGET("[%d] before rpc_req_queue_lock\n"),
		    pthread_self());
	}

	res = pthread_mutex_lock(&rpc_reqq_mutex);
	if (debug) {
		dbg_msgout_nosyslog(NOGET("[%d] after rpc_req_queue_lock\n"),
		    pthread_self());
	}
	switch (res) {
		case 0:
			return (RPC_SERVICES_OK);
		case EINVAL:
			return (RPC_SERVICES_EINVAL);
		case EDEADLK:
			return (RPC_SERVICES_EDEADLK);
		default:
			return (RPC_SERVICES_EUNEXPECTED);
	}
}

/*
 * Unlock the mutex for request queue
 */
static rpc_error_t
rpc_req_queue_unlock()
{
	int res;

	if (debug) {
		dbg_msgout_nosyslog(NOGET("[%d] before rpc_req_queue_unlock\n"),
		    pthread_self());
	}
	res = pthread_mutex_unlock(&rpc_reqq_mutex);

	if (debug) {
		dbg_msgout_nosyslog(NOGET("[%d] after rpc_req_queue_unlock\n"),
		    pthread_self());
	}
	switch (res) {
		case 0:
			return (RPC_SERVICES_OK);
		case EINVAL:
			return (RPC_SERVICES_EINVAL);
		default:
			return (RPC_SERVICES_EUNEXPECTED);
	}
}

/*
 * Wait on the semaphore
 */
static rpc_error_t
rpc_req_queue_wait()
{
	int retval;
	rpc_error_t res;

	retval = sem_wait(&rpc_reqq_sem);

	if (debug) {
		dbg_msgout_nosyslog(NOGET("rpc_req_queue_wait: sem_wait "
		    "returns [retval=%d, errno=%d]\n"), retval, errno);
	}

	res = sem_to_rpc_error(retval);

	return (res);
}

/*
 * Post on the semaphore
 */
static rpc_error_t
rpc_req_queue_post()
{
	int retval;
	rpc_error_t res;

	retval = sem_post(&rpc_reqq_sem);

	if (debug) {
		dbg_msgout_nosyslog(NOGET("rpc_req_queue_post: sem_post "
		    "returns [retval=%d, errno=%d]\n"), retval, errno);
	}

	res = sem_to_rpc_error(retval);

	return (res);
}

/*
 * Append the rpc request at the end of the rpc request queue
 */
rpc_error_t
rpc_req_queue_append(register SVCXPRT *transp, int arg_size,
    xdrproc_t xdr_arg, xdrproc_t xdr_result, int res_size,
    dispatch_call_t func_ptr, struct svc_req *svc_reqs)
{
	rpc_error_t res;
	rpc_req_queue_t *req = NULL;
	sc_syslog_msg_handle_t handle;

	if (transp == NULL) {
		/* Should not happen */
		if (debug) {
			dbg_msgout_nosyslog(NOGET("rpc_req_queue_append: NULL "
			    "transp\n"));
		}
		return (RPC_SERVICES_EINVAL);
	}

	if (func_ptr == NULL) {
		/* Should not happen */
		if (debug) {
			dbg_msgout_nosyslog(NOGET("rpc_req_queue_append: NULL "
			    "func_ptr\n"));
		}
		return (RPC_SERVICES_EINVAL);
	}


	/*
	 * Allocate memory for request queue
	 */
	req = (rpc_req_queue_t *)malloc(sizeof (rpc_req_queue_t));
	if (req == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rpc_req_queue_append: not enough memory to malloc");
		sc_syslog_msg_done(&handle);

		return (RPC_SERVICES_ENOMEM);
	}

	/*
	 * Allocate memory for argument
	 */
	req->arguments = malloc(arg_size);
	if (req->arguments == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rpc_req_queue_append: not enough memory to malloc");
		sc_syslog_msg_done(&handle);
		return (RPC_SERVICES_ENOMEM);
	}
	(void) memset((char *)req->arguments, 0, arg_size);

	/*
	 * Allocate memory for result
	 */
	req->result = malloc(res_size);
	if (req->result == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rpc_req_queue_append: not enough memory to malloc");
		sc_syslog_msg_done(&handle);
		return (RPC_SERVICES_ENOMEM);
	}
	(void) memset((char*)req->result, 0, res_size);

	/*
	 * Get client request's argument
	 */
	if (!svc_getargs(transp, xdr_arg, (caddr_t)req->arguments)) {
		svcerr_decode(transp);
		return (RPC_SERVICES_EINVAL);
	}

	req->transp = transp;
	req->xdr_arg = xdr_arg;
	req->xdr_result = xdr_result;
	req->svc_reqs = svc_reqs;
	req->dispatch_func_ptr = func_ptr;
	req->dispatched = FALSE;
	req->next = NULL;

	/*
	 * Lock the request queue
	 */
	res = rpc_req_queue_lock();
	assert(res == RPC_SERVICES_OK);

	/*
	 * Insert the rpc request to the end of the queue
	 */
	if (rpc_reqq_head == NULL) {
		rpc_reqq_head = req;
		rpc_reqq_tail = rpc_reqq_head;
	} else {
		rpc_reqq_tail->next = req;
		rpc_reqq_tail = req;
	}

	/*
	 * Increment the request count in the queue
	 */
	rpc_reqq_count++;
	if (debug) {
		dbg_msgout_nosyslog(NOGET(
		    "[%d] rpc_req_queue_append: no of request in the request"
		    " queue [%d]\n"), pthread_self(), rpc_reqq_count);
	}

	/*
	 * Unlock the request queue
	 */
	res = rpc_req_queue_unlock();
	assert(res == RPC_SERVICES_OK);

	/*
	 * Wake up the thread(s) waiting to service the request
	 */
	return (rpc_req_queue_post());
}

/*
 * Get the first request from the request queue
 */
rpc_error_t
rpc_req_queue_get(rpc_req_queue_t **req)
{
	rpc_error_t res;
	rpc_req_queue_t *req_ptr;

	/*
	 * Wait for request to come
	 */
	res = rpc_req_queue_wait();
	if (res != RPC_SERVICES_OK) {
		return (res);
	}

	/*
	 * Lock the rpc request queue
	 */
	res = rpc_req_queue_lock();
	assert(res == RPC_SERVICES_OK);

	if (rpc_reqq_head == NULL) {
		/*
		 * Unlock the rpc request queue
		 */
		rpc_req_queue_unlock();
		return (RPC_SERVICES_EINVAL);
	}

	req_ptr = rpc_reqq_head;

	while ((req_ptr != NULL) && (req_ptr->dispatched)) {
		req_ptr = req_ptr->next;
	}
	if (req_ptr == NULL) {
		/*
		 * Should not be here
		 */
		/*
		 * Unlock the rpc request queue
		 */
		rpc_req_queue_unlock();

		return (RPC_SERVICES_EUNEXPECTED);
	}
	req_ptr->dispatched = TRUE;

	*req = req_ptr;

	/*
	 * Unlock the rpc request queue
	 */
	res = rpc_req_queue_unlock();
	assert(res == RPC_SERVICES_OK);

	return (RPC_SERVICES_OK);
}

/*
 * Remove the given request from request queue.
 */
rpc_error_t
rpc_req_queue_remove(rpc_req_queue_t *req)
{
	rpc_error_t res;
	sc_syslog_msg_handle_t handle;
	rpc_req_queue_t *q_ptr = NULL;
	rpc_req_queue_t *q_ptr2 = NULL;

	/*
	 * Lock the request queue
	 */
	res = rpc_req_queue_lock();
	assert(res == RPC_SERVICES_OK);

	q_ptr = rpc_reqq_head;

	while ((q_ptr != NULL) && (q_ptr != req)) {
		q_ptr = q_ptr->next;
	}

	if (q_ptr == NULL) {
		/*
		 * Should not be here
		 */
		/*
		 * Unlock the request queue
		 */
		rpc_req_queue_unlock();
		return (RPC_SERVICES_EUNEXPECTED);
	}

	/*
	 * If match the head of the queue, remove the first request
	 * from the queue
	 */
	if (q_ptr == rpc_reqq_head) {
		rpc_reqq_head = rpc_reqq_head->next;

		if (rpc_reqq_head == NULL) {
			/*
			 * The request being removed is the last one.
			 */
			rpc_reqq_tail = NULL;
		}
	} else {
		q_ptr2 = rpc_reqq_head;
		while (q_ptr2->next != q_ptr) {
			q_ptr2 = q_ptr2->next;
		}
		if (q_ptr == rpc_reqq_tail) {
			rpc_reqq_tail = q_ptr2;
		}
		q_ptr2->next = q_ptr->next;
	}

	if (q_ptr != NULL) {
		free(q_ptr);
	}

	/*
	 * Decrement the request count in the queue
	 */
	rpc_reqq_count--;

	if (debug) {
		dbg_msgout_nosyslog(NOGET("[%d] rpc_req_queue_remove: no of "
		    " request in the request queue [%d]\n"),
		    pthread_self(), rpc_reqq_count);
	}

	/*
	 * Unlock the request queue
	 */
	res = rpc_req_queue_unlock();
	assert(res == RPC_SERVICES_OK);

	return (RPC_SERVICES_OK);
}

/*
 * Cleanup client request queue and semaphore
 */
rpc_error_t
rpc_cleanup_req_queue()
{
	int result;
	rpc_error_t res;

	/* clean up the request queue */
	rpc_req_queue_t *ptr;

	ptr = rpc_reqq_head;
	while (ptr != NULL) {
		rpc_reqq_head = rpc_reqq_head->next;
		ptr->next = NULL;
		free(ptr);
		ptr = rpc_reqq_head;
	}
	rpc_reqq_head = rpc_reqq_tail = NULL;

	/* destroy semaphore */
	result = sem_destroy(&rpc_reqq_sem);
	res = sem_to_rpc_error(result);

	return (res);
}
