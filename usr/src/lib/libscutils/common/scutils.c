/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scutils.c	1.42	08/05/20 SMI"


/*
 * scutils.c
 * libscutils -  utilities for Sun Cluster pmf, fed daemons.
 */

#include <sys/os.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <locale.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <synch.h>
#include <netdb.h>	/* for definition of MAXHOSTNAMELEN */
#ifndef linux
/* SC SLM addon start */
#include <procfs.h> /* for PR_MSACCT & PCSET */
/* SC SLM addon end */
#endif


#if defined(DEBUG) && !defined(LOCK_DEBUG)
#include <sys/cl_assert.h>
#endif

#ifndef linux
#include<rgm/sczones.h>
#endif

#include <rgm/scutils.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>

#ifdef linux
#include <sys/resource.h>
#include <signal.h>
#include <sys/param.h>
#endif


/* Used to define __s9, __s10, etc. */
#include <sys/sol_version.h>

#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#ifdef linux
#define	SC_SCHED_POLICY	SCHED_RR
#endif

/*
 * debug is 1 if server is started with -d option.
 */
static int debug;
static int svc_setschedprio(void);

#define	stringsize(numbytes) ((size_t)((2.41 * (numbytes)) + 1))


/*
 * debug reporting function
 * This func should be called only if the server was started in debug mode.
 * It prints the msg to syslog and stderr and prefixes it with the thread id.
 *
 * This version should not be called after a fork1, because it calls syslog,
 * which is not fork1-safe.
 */
void
dbg_msgout(const char *msg, ...)
{
	pthread_t tid = pthread_self();
	char *newmsg = NULL;
	va_list ap;

	/* allocate for "tid <msg>\0" */
	newmsg = (char *)malloc(stringsize(sizeof (pthread_t)) + 1 +
	    strlen(msg) + 1);
	if (newmsg == NULL) {	/* ignore tid */
		newmsg = (char *)msg;
	} else {
		/* initialise newmsg to empty string in case sprintf fails */
		newmsg[0] = '\0';
		(void) sprintf(newmsg, "%u %s", tid, msg);
	}

	/*lint -save -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, msg);
	/*lint -restore */

	vsyslog(LOG_DEBUG, newmsg, ap);
	(void) vfprintf(stderr, newmsg, ap);

	va_end(ap);
	if (newmsg != msg)
		free(newmsg);
}

/*
 * debug reporting function
 * A version of dbg_msgout that only syslogs the message and doesnot print
 * to stderr. This version should not be called after a fork1, because
 * it calls syslog, which is not fork1-safe.
 */
void
dbg_msgout_onlysyslog(int priority, const char *msg, ...)
{
	pthread_t tid = pthread_self();
	char *newmsg = NULL;
	va_list ap;

	/* allocate for "tid <msg>\0" */
	newmsg = (char *)malloc(stringsize(sizeof (pthread_t)) + 1 +
	    strlen(msg) + 1);
	if (newmsg == NULL) {	/* ignore tid */
		newmsg = (char *)msg;
	} else {
		/* initialise newmsg to empty string in case sprintf fails */
		newmsg[0] = '\0';
		(void) sprintf(newmsg, "%u %s", tid, msg);
	}

	/*lint -save -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, msg);
	/*lint -restore */

	vsyslog(priority, newmsg, ap);

	va_end(ap);
	if (newmsg != msg)
		free(newmsg);
}

/*
 * debug reporting function
 *
 * A version of dbg_msgout that does not syslog the message.  This version
 * should be used between fork1 and exec calls in the child, so as not
 * to incur a deadlock situation in syslog (syslog is not fork1-safe).
 */
void
dbg_msgout_nosyslog(const char *msg, ...)
{
	pthread_t tid = pthread_self();
	char *newmsg = NULL;
	va_list ap;

	/* allocate for "tid <msg>\0" */
	newmsg = (char *)malloc(stringsize(sizeof (pthread_t)) + 1 +
	    strlen(msg) + 1);
	if (newmsg == NULL) {	/* ignore tid */
		newmsg = (char *)msg;
	} else {
		/* initialise newmsg to empty string in case sprintf fails */
		newmsg[0] = '\0';
		(void) sprintf(newmsg, "%u %s", tid, msg);
	}

	/*lint -save -e40 Undeclared identifier (__builtin_va_alist) */
	va_start(ap, msg);
	/*lint -restore */

	(void) vfprintf(stderr, newmsg, ap);

	va_end(ap);
	if (newmsg != msg)
		free(newmsg);
}


/*
 * Initialize server:
 * if debug flag is on, set the debug mode and return;
 * otherwise set RT scheduling, and set debugging mode.
 */
int
svc_init(int dbg)
{
	/*
	 * set the value of the global variable debug
	 */
	debug = dbg;

	if (debug)
		return (0);

	/*
	 * Setup our scheduling priority.
	 */
	if (svc_setschedprio()) {
		return (-1);
	}

	return (0);

}


#ifdef linux
static sigset_t saved_sigmask;

/*
 * Set the current signal mask and save current one.
 */
int
svc_sigprocmask(sigset_t *set)
{
	sc_syslog_msg_handle_t handle;
	int err;

	if (sigprocmask(SIG_SETMASK, set, &saved_sigmask) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "svc_sigprocmask: Could not save original "
		    "signal mask: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		return (1);
	}
	return (0);
}

/*
 * Restore the signal mask.
 */
int
svc_sigrestoremask(void)
{
	sc_syslog_msg_handle_t handle;
	int err;

	if (sigprocmask(SIG_SETMASK, &saved_sigmask, NULL) != 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "svc_sigrestoremask: Could not restore original "
		    "signal mask: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		return (1);
	}
	return (0);
}
#endif

#ifdef linux
static struct sched_param saved_schedparms;
static int saved_schedpolicy;
#else
static pcparms_t saved_schedparms;
static pcparms_t ts_schedparms;
/*
 * makes a human readable string representation of the schedparams data
 * buffer size required: sizeof(parms) * 2 + 1
 */
static void
convert_saved_parms_to_string(char *buffer, const pcparms_t *schedparms_ptr)
{
	uint_t i;

	for (i = 0; i < sizeof (pcparms_t); i++) {
		(void) sprintf(buffer, "%2x", ((char *)schedparms_ptr)[i]);
		buffer = buffer + 2;
	}
}
#endif /* linux */

/*
 * change run priority to the one we had before we ran in RT mode
 */
int
svc_restore_priority()
{
	sc_syslog_msg_handle_t handle;
	int	err;
#ifdef linux
	char buffer[5];
#else
	char buffer[2 * sizeof (saved_schedparms) + 1];
#endif

#ifdef linux
	if (sched_setscheduler(0, saved_schedpolicy, &saved_schedparms) == -1) {
		err = errno; /*lint !e746 */
		strcpy(buffer, "NONE");
#else
	if ((err = sc_priocntl(P_MYID, &saved_schedparms)) != 0) {
		convert_saved_parms_to_string(buffer, &saved_schedparms);
#endif /* linux */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The server was not able to restore the original scheduling
		 * mode. The system error message is shown. An error message
		 * is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "svc_restore_priority: Could not set required "
		    "scheduling parameters: %s. Diagnostic information (%s).",
		    strerror(err), buffer);
		sc_syslog_msg_done(&handle);
		return (1);
	}
	return (0);
}

/*
 * Change run priority to RT mode and save current one.
 */
static int
svc_setschedprio(void)
{
#ifdef linux
	struct sched_param pc_parms;
#else
	pcparms_t parms;
#endif
	sc_syslog_msg_handle_t handle;
	int	err;

	/*
	 * Obtain, and save, old sched parameters.
	 */
#ifdef linux
	if (sched_getparam(0, &saved_schedparms) == -1) {
#else
	saved_schedparms.pc_cid = PC_CLNULL;
	if (priocntl(P_PID, P_MYID, PC_GETPARMS, (caddr_t)&saved_schedparms)
	    == -1) {
#endif
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * The server was not able to save the original scheduling
		 * mode. The system error message is shown. An error message
		 * is output to syslog.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "svc_setschedprio: Could not save current "
		    "scheduling parameters: %s",
		    strerror(err));
		sc_syslog_msg_done(&handle);
		return (1);

	}

#ifdef linux
	if ((saved_schedpolicy = sched_getscheduler(0)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");

		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "svc_setschedprio: Could not save current "
		    "scheduling parameters: %s",
		    strerror(err));
		sc_syslog_msg_done(&handle);
		return (1);
	}

	pc_parms = saved_schedparms;
	pc_parms.sched_priority = sched_get_priority_min(SC_SCHED_POLICY);

	if (pc_parms.sched_priority == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "svc_setschedprio: Could not get min priority for RT "
		    "(real time) scheduling policy: %s",
		    strerror(err));
		sc_syslog_msg_done(&handle);
		return (1);
	}

	if (sched_setscheduler(0, SC_SCHED_POLICY, &pc_parms) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");

		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Could not setup scheduling parameters: %s",
		    strerror(err));
		sc_syslog_msg_done(&handle);
		return (1);
	}
	return (0);
#else /* linux */
	if (sc_get_rtparameters(&parms) != 0)
		return (1);
	return (sc_priocntl(P_MYID, &parms));
#endif /* linux */
}

/*
 * memory allocating, deallocating and tracking routines
 */
#ifdef DEBUG

#define	MALLOC_HIST_SIZE 1024
struct memhist {
	void (*caller)();
	char *ptr;
	size_t size;
} memhist[MALLOC_HIST_SIZE];
int memhist_cnt = 0;
int memhist_miss = 0;

/*lint -save -e708 */
/*
 * Suppress FlexeLint informational message 708.
 * Some compilers don't handle this kind of initialization.
 * pthread_mutex_init(3THR) example does it this way.
 */
static mutex_t svc_alloc_lock = DEFAULTMUTEX;
/*lint -restore */

#ifdef LOCK_DEBUG
#define	LOCK_ALLOC() \
	if (debug) dbg_msgout(NOGET("%s/%d LOCK ALLOC (0x%x)\n"), \
	__FILE__, __LINE__, &svc_alloc_lock); \
	pthread_mutex_lock(&svc_alloc_lock);
#define	UNLOCK_ALLOC() \
	if (debug) dbg_msgout(NOGET("%s/%d UNLOCK ALLOC (0x%x)\n"), \
	__FILE__, __LINE__, &svc_alloc_lock); \
	pthread_mutex_unlock(&svc_alloc_lock);
#else
#define	LOCK_ALLOC(h) (CL_PANIC(pthread_mutex_lock(&svc_alloc_lock) == 0));
#define	UNLOCK_ALLOC(h) (CL_PANIC(pthread_mutex_unlock(&svc_alloc_lock) == 0));
#endif

#endif

/*
 * Set _DEBUG to control access to svc_caller.s.
 * Keep code compatible with x86 platform
 */
/* ARGSUSED */
void *
svc_alloc(size_t size, int *alloc_cnt)
{
	char *newptr;
#ifdef DEBUG
	int i;

	LOCK_ALLOC();
	for (i = 0; i < MALLOC_HIST_SIZE; i++) {
		if (memhist[i].ptr == NULL)
			break;
	}
	*alloc_cnt += 1;
#endif
	/*
	 * checking of the success of malloc is done after this call returns
	 */
	newptr = (char *)malloc(size);
#ifdef DEBUG
	memhist_cnt++;
	memhist[i].ptr = newptr;
	memhist[i].size = size;
#ifdef _DEBUG
	memhist[i].caller = (void (*)())svc_caller();
#endif
	UNLOCK_ALLOC();
#endif
	return (newptr);
}


/* ARGSUSED */
void
svc_free(void *old, int *alloc_cnt)
{
#ifdef DEBUG
	int i;

	LOCK_ALLOC();
	for (i = 0; i < MALLOC_HIST_SIZE; i++) {
		if (memhist[i].ptr == old) {
			memhist[i].ptr = NULL;
			memhist[i].caller = NULL;
			memhist_cnt--;
			break;
		}
	}
	if (i == MALLOC_HIST_SIZE) {
		dbg_msgout((const char *) NOGET("memhist miss on 0x%x\n"),
			old);
		memhist_miss++;
	}
#endif
	free(old);
#ifdef DEBUG
	*alloc_cnt -= 1;
	UNLOCK_ALLOC();
#endif
}


/* ARGSUSED */
char *
svc_strdup(const char *str, int *alloc_cnt)
{
	char *newptr;
#ifdef DEBUG
	int i;

	LOCK_ALLOC();
	for (i = 0; i < MALLOC_HIST_SIZE; i++) {
		if (memhist[i].ptr == NULL)
			break;
	}
	*alloc_cnt += 1;
#endif
	newptr = strdup(str);
#ifdef DEBUG
	memhist_cnt++;
	memhist[i].ptr = newptr;
	memhist[i].size = strlen(str) + 1;
#ifdef _DEBUG
	memhist[i].caller = (void (*)())svc_caller();
#endif
	UNLOCK_ALLOC();
#endif
	return (newptr);
}


/*
 * Returns the physical hostname of the local node.
 * if the call to gethostname() fails it returns zero.
 */
char *
getlocalhostname()
{
	static char Hostname[MAXHOSTNAMELEN];

	/* OK to initialize this way; see mutex_init(3THR) */
	static mutex_t  host_name_mutex = DEFAULTMUTEX;		/*lint !e708 */

	static boolean_t   first_time = B_TRUE;

	/*
	 * get lock to ensure that only 1 thread executes this section
	 */
	(void) mutex_lock(&host_name_mutex);

	/*
	 * get host name;
	 * only do it the first time, then cache it.
	 */
	if (first_time) {
		first_time = B_FALSE;

		/*
		 * set to null, in case the call below fails and we return
		 * immediately; next time the function directly returns
		 * Hostname, so it should be set to NULL.
		 */
		(void) memset(Hostname, 0, (size_t)MAXHOSTNAMELEN);
		if (gethostname(Hostname, MAXHOSTNAMELEN) == -1) {
			(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
			(void) fprintf(stderr,
			    (const char *)dgettext(TEXT_DOMAIN,
			    "gethostname failed\n"));
			(void) mutex_unlock(&host_name_mutex);
			return (NULL);
		}
	}

	/*
	 * release lock
	 */
	(void) mutex_unlock(&host_name_mutex);

	return (Hostname);
}



/*
 * Create a daemon process and detach it from the controlling terminal.
 */
void
make_daemon()
{
	struct rlimit rl;
	int	fd;
	pid_t	pd;
	int	err;
	pid_t	rc;
	sc_syslog_msg_handle_t handle;
#if SOL_VERSION < __s9 || defined(linux)
	int i;
#endif

	pd = fork();
	if (pd < 0) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fork: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * parent exits after fork
	 */
	if (pd > 0)
		exit(0);		/* parent */

	/*
	 * In the child, redirect stdin to /dev/null so that if
	 * the daemon reads from stdin, it doesn't block.
	 * Redirect stdout, stderr to /dev/console so that any
	 * output printed by the daemon goes somewhere.
	 */

	if ((fd = open("/dev/null", O_RDONLY)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to open /dev/null: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	if (dup2(fd, 0) == -1)
		(void) close(fd);

	/*
	 * redirect stdout and stderr by opening /dev/console
	 * and duping to the appropriate descriptors.
	 */
	fd = open("/dev/console", O_WRONLY);
	if (fd == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to open /dev/console: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}
	if (dup2(fd, 1) == -1 || dup2(fd, 2) == -1)
		(void) close(fd);

	/*
	 * child closes rest of file descriptors
	 */
	rl.rlim_max = 0;
	(void) getrlimit(RLIMIT_NOFILE, &rl);
	if ((int)rl.rlim_max == 0)
		exit(1);

#if SOL_VERSION < __s9 || defined(linux)
	for (i = 3; i < (int)rl.rlim_max; i++) {
		(void) close(i);
	}
#else
	closefrom(3);
#endif

	/*
	 * child sets this process as group leader
	 */
	rc = setsid();
	if (rc == (pid_t)-1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "setsid: %s", strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

}

/*
 * This function opens a file and locks it.
 * This will prevent another rgmd daemon from starting while we are running
 * This must be called after the fork (used to daemonize)
 * as the lock is not inherited by the child.
 */
void
make_unique(char *lock_filename)
{
	int file;
	int err;
	struct flock lock_attr;
	sc_syslog_msg_handle_t handle;

	file = open(lock_filename, O_RDWR|O_CREAT, S_IWUSR);
	if (-1 == file) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to create the lock file (%s): %s\n",
		    lock_filename, strerror(err));
		sc_syslog_msg_done(&handle);
		exit(1);
	}

	/*
	 * Force the mode mask in case umask has been specified.
	 * Will not exit if chmod does not succeed.
	 */
	(void) chmod(lock_filename, S_IWUSR);

	lock_attr.l_type = F_WRLCK;
	lock_attr.l_whence = 0;
	lock_attr.l_start = 0;
	lock_attr.l_len = 0;

	err = fcntl(file, F_SETLK, &lock_attr);
	if (-1 == err) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "There is already an instance of this daemon running\n");
		sc_syslog_msg_done(&handle);
		exit(1);
	}
}


/*
 * pre_alloc_heap()
 * This function is used to pre-allocate a chunk of memory
 * Currently used by pmfd and fed daemons.
 *
 * The routine attempts to first allocate "max_swap_size". If this fails,
 * it attempts to allocate smaller swap_sizes until "min_swap_size" is reached.
 * If "min_swap_size" cannot be allocated the routine fails.
 *
 * Arguments:
 *	max_swap_size	initial (largest) swap size to try (in MBytes)
 *	min_swap_size	final (smallest) swap size to try (in MBytes)
 */
int
pre_alloc_heap(size_t max_swap_size, size_t min_swap_size)
{
	size_t	swap_size = 0;
	void	*pre_alloc_buf = NULL;

	/*
	 * First try the max_buf_size, if this fails, try smaller sizes until
	 * reaching min_buf_size.
	 */
	for (swap_size = max_swap_size; swap_size >= min_swap_size;
	    swap_size /= 2) {
		pre_alloc_buf =
		    (void *)malloc(
		    (size_t)swap_size * 1024 * 1024);
	}
	if (pre_alloc_buf == NULL) {
		return (1);
	}
	free(pre_alloc_buf);
	return (0);
}

#ifndef linux
/* SC SLM addon start */
/*
 * Enable the microstate accounting for the processes
 * started by PMF and fed. This will provide a better
 * accounting for the CPU time used by the processes.
 *
 * This is not necessary on S10 as microstate accounting
 * became the default on S10. (This function does not exists
 * for S10 or above)
 *
 * This function has been added for the Ganymede project
 * that instrument the system resources consumed by the RGs
 * on Sun Cluster.
 */

#if SOL_VERSION < __s10

int
set_microstate_accounting(void)
{
	char ctl_file[MAXPATHLEN];
	sc_syslog_msg_handle_t handle;
	int err = 0;
	int ctlpfd = 0;
	long control[2] = {PCSET, PR_MSACCT};

	pid_t pid = getpid();
	if (pid == (pid_t)-1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Unable to retrieve my pid : %s",
		    strerror(err));
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	(void) sprintf(ctl_file, "/proc/%d/ctl", (int)pid);

	if ((ctlpfd = open(ctl_file, O_WRONLY)) == -1) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Error opening procfs control file (for process %d)"
		    " : %s", pid, strerror(err));
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	if (write(ctlpfd, &control, sizeof (control)) < 0) { /*lint !e545 */
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "PCSET failed for PR_MSACCT %s", strerror(err));
		sc_syslog_msg_done(&handle);
		close(ctlpfd);
		return (-1);
	}

	close(ctlpfd);

	return (0);
}
/* SC SLM addon end */
#endif

#endif
