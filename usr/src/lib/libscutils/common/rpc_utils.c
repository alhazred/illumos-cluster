/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)rpc_utils.c 1.3     08/05/20 SMI"

/*
 * rpc_utils.c - utilities for MT RPC codes
 */

#include <errno.h>
#include <rgm/rpc_services.h>
#include "rpc_utils.h"

/*
 * Map the error from sem_* call to rpc_error_t
 */
rpc_error_t
sem_to_rpc_error(int res)
{
	switch (res) {
		case 0:
			return (RPC_SERVICES_OK);
		default:
			return (RPC_SERVICES_ERROR);
	}
}

/*
 * Map the error code of pthread_ calls to rpc_error_t
 */
rpc_error_t
pthread_to_rpc_error(int res)
{
	switch (res) {
		case 0:
			return (RPC_SERVICES_OK);
		case EPERM:
			return (RPC_SERVICES_EPERM);
		case EINVAL:
			return (RPC_SERVICES_EINVAL);
		case ENOMEM:
			return (RPC_SERVICES_ENOMEM);
		case ETIMEDOUT:
			return (RPC_SERVICES_ETIMEDOUT);
		case ENOSYS:
			return (RPC_SERVICES_ENOSYS);
		case EDEADLK:
			return (RPC_SERVICES_EDEADLK);
		default:
			return (RPC_SERVICES_EUNEXPECTED);
	}
}

int
rpc_server_1_freeresult(SVCXPRT * transp, xdrproc_t xdr_result, caddr_t result)
{
	xdr_free(xdr_result, result);

	return (TRUE);
}
