/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rpc_services.c	1.3	08/05/20 SMI"
/*
 * rpc_services.c - MT RPC implementation on Linux
 * bugId 5095064 (Linux: need to provide MT RPC)
 *
 * Linux does not support MT RPC services.  Hence, we need to provide our
 * own MT RPC implementation.  The RPC server will initially create a thread
 * pool which is intended to service the rpc client requests.  The number of
 * threads in the thread pool is depended on the individual rpc server's needs.
 * When rpc server receives rpc client request, it places the request in the
 * request queue and signals the thread pool to service this particular
 * request.  Whichever thread servicing the request will be responsible
 * to send the reply back to the client.
 *
 * There are three public interfaces that each individual rpc server needs to
 * call.  Call rpc_init_rpc_mgr() at the startup to initialize the rpc
 * manager.  Call rpc_req_queue_append() when receives rpc client request.
 * Call rpc_cleanup_rpc_mgr() to clean up the rpc manager at shut down.
 */

#include <errno.h>
#include <pthread.h>

#include <rgm/rpc_services.h>
#include <sys/rsrc_tag.h>
#include <sys/sc_syslog_msg.h>
#include "rpc_request_queue.h"

static pthread_t *thread_ids;  /* store thread ids */
int debug;

static rpc_error_t rpc_create_threads_pool(int thread_count, int stacksz);
static rpc_error_t rpc_cleanup_threads_pool(int thread_count);
static void *rpc_process(void *nothing);

/* for tracking of strings that are not translated */
#define	NOGET(s)	s

/*
 * Initialize the threads pool for rpc services
 */
rpc_error_t
rpc_init_rpc_mgr(int thread_count, int dbg, int stksz)
{
	rpc_error_t res;
	sc_syslog_msg_handle_t handle;

	debug = dbg;

	/*
	 * Initialize the rpc request queue
	 */
	res = rpc_init_req_queue();
	if (res != RPC_SERVICES_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rpc_init_rpc_mgr: failed to initialize rpc request queue");
		sc_syslog_msg_done(&handle);
		return (res);
	}

	/*
	 * Create thread pool
	 */
	res = rpc_create_threads_pool(thread_count, stksz);
	if (res != RPC_SERVICES_OK) {
		(void) rpc_cleanup_threads_pool(thread_count);
		return (res);
	}

	if (debug)
		dbg_msgout_nosyslog(NOGET("MT RPC initialization done!\n"));
	return (RPC_SERVICES_OK);
}

/*
 * Process the client request
 */
static void *
rpc_process(void *nothing)
{
	bool_t retval;
	rpc_req_queue_t *req;
	int res;
	sc_syslog_msg_handle_t handle;

	while (1) {
		/*
		 * Don't want to be disturbed while processing client request
		 * Thread is created with cancellation enabled.
		 */
		(void) pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
		pthread_testcancel();
		(void) pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

		res = rpc_req_queue_get(&req);
		if ((res != RPC_SERVICES_OK) || (req == NULL)) {
			continue;
		}

		if (debug)
			dbg_msgout_nosyslog(
			    NOGET("[%d] rpc_process: processing request, proc "
			    "[%d]\n"), pthread_self(), req->svc_reqs->rq_proc);

		/*
		 * Execute rpc dispatch function
		 */
		retval = (bool_t)(*req->dispatch_func_ptr) ((void *)
		    (req->arguments), (void *)(req->result), req->svc_reqs);

		if (!svc_freeargs(req->transp, req->xdr_arg,
		    (caddr_t)(req->arguments))) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "rpc_process: unable to free arguments");
			sc_syslog_msg_done(&handle);
		}

		if (retval > 0) {
			/*
			 * Reply to client
			 */
			if (!svc_sendreply(req->transp, req->xdr_result,
			    (char *)(req->result))) {
				svcerr_systemerr(req->transp);
			} else {
				if (debug)
					dbg_msgout_nosyslog(NOGET("[%d] "
					    "rpc_process: send reply to client "
					    "for proc [%d]\n"), pthread_self(),
					    req->svc_reqs->rq_proc);
			}
		}

		if (!rpc_server_1_freeresult(req->transp,
		    req->xdr_result, (char *)req->result)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "rpc_process: unable to free results");
			sc_syslog_msg_done(&handle);
		}

		if (req->arguments != NULL)
			free(req->arguments);
		if (req->result != NULL)
			free(req->result);

		/*
		 * Remove request from rpc request queue
		 */
		(void) rpc_req_queue_remove(req);
	}
}


/*
 * Create a thread pool based on the given threads count
 */
static rpc_error_t
rpc_create_threads_pool(int thread_count, int stacksz)
{
	pthread_attr_t attr;
	int i = 0;
	int result = 0;
	rpc_error_t res;
	sc_syslog_msg_handle_t handle;

	thread_ids = (pthread_t *)malloc(thread_count * sizeof (pthread_t));
	if (thread_ids == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rpc_create_threads_pool: thread ids malloc failed");
		sc_syslog_msg_done(&handle);
		return (RPC_SERVICES_ENOMEM);
	}

	/* Initialize thread ids in the thread pool */
	for (i = 0; i < thread_count; i++) {
		thread_ids[i] = 0;
	}
	result = pthread_attr_init(&attr);
	res = pthread_to_rpc_error(result);
	if (res != RPC_SERVICES_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rpc_create_threads_pool: pthread_attr_init failed [%s]",
		    strerror(result));
		sc_syslog_msg_done(&handle);
		return (res);
	}

	/*
	 * The default stack size on Solaris is about 1m while it is
	 * about 10m on Linux.  On Linux, with that much default stack
	 * size, we are not able to create couple hundred threads (e.g. 300).
	 * So, we need to reduce the default stack size to the given stacksz.
	 * If the stacksz is 0, no need to change the stack size.
	 */
	if (stacksz != 0) {
		result = pthread_attr_setstacksize(&attr, stacksz);
		res = pthread_to_rpc_error(result);
		if (res != RPC_SERVICES_OK) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "rpc_create_threads_pool: \
			    pthread_attr_setstacksize failed [%s]",
			    strerror(result));
			sc_syslog_msg_done(&handle);
			return (res);
		}
	}

	/*
	 * Create a bound thread
	 */
	result = pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
	res = pthread_to_rpc_error(result);
	if (res != RPC_SERVICES_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rpc_create_threads_pool: pthread_attr_setscope failed \
		    [%s]\n", strerror(result));
		sc_syslog_msg_done(&handle);
		return (res);
	}
	for (i = 0; i < thread_count; i++) {
		result =
		    pthread_create(&thread_ids[i], &attr, rpc_process, NULL);
		res = pthread_to_rpc_error(result);
		if (res != RPC_SERVICES_OK) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGMPMF_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "rpc_create_threads_pool: pthread_create failed \
			    [%s]\n", strerror(result));
			sc_syslog_msg_done(&handle);
			return (res);
		}
	}

	if (debug)
		dbg_msgout_nosyslog(NOGET("Have created [%d] threads in "
		    "the pool\n"), thread_count);

	result = pthread_attr_destroy(&attr);
	res = pthread_to_rpc_error(result);
	if (res != RPC_SERVICES_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGMPMF_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rpc_create_threads_pool: pthread_attr_destroy failed \
		    [%s]\n", strerror(result));
		sc_syslog_msg_done(&handle);
		return (res);
	}

	return (res);
}

/*
 * Cancel the execution of all threads in the thread pool
 */
static rpc_error_t
rpc_cleanup_threads_pool(int thread_count)
{
	int i;
	int result;
	rpc_error_t res;
	sc_syslog_msg_handle_t handle;

	if (thread_ids != NULL) {
		for (i = 0; i < thread_count; i++) {
			if (thread_ids[i] != 0) {
				result = pthread_cancel(thread_ids[i]);
				res = pthread_to_rpc_error(result);
				if (res != RPC_SERVICES_OK) {
					(void) sc_syslog_msg_initialize(&handle,
					    SC_SYSLOG_RGMPMF_TAG, "");
					/*
					 * SCMSGS
					 * @explanation
					 * Need explanation of this message!
					 * @user_action
					 * Need a user action for this
					 * message.
					 */
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "rpc_cleanup_threads_pool: \
					    pthread_cancel failed [%s]\n",
					    strerror(res));
					sc_syslog_msg_done(&handle);
					return (res);
				}
			}
		}
		free(thread_ids);
		thread_ids = NULL;
	}

	if (debug)
		dbg_msgout_nosyslog(NOGET("Cancelled all threads in the rpc "
		    "thread pool\n"));

	return (RPC_SERVICES_OK);
}

/*
 * Clean up the rpc manager
 */
rpc_error_t
rpc_cleanup_rpc_mgr(int thread_count)
{

	/* clean up threads pool */
	rpc_cleanup_threads_pool(thread_count);

	/* clean up request queue */
	rpc_cleanup_req_queue();

	if (debug)
		dbg_msgout_nosyslog(NOGET("MT RPC cleanup done!\n"));
	return (RPC_SERVICES_OK);
}
