/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */


/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef _RPC_REQUEST_QUEUE_H
#define	_RPC_REQUEST_QUEUE_H

#pragma ident	"@(#)rpc_request_queue.h	1.2	08/05/20 SMI"

#include <rpc/rpc.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Element in the RPC client request queue
 */
typedef struct rpc_req_queue {

	/*
	 * Buffer to be passed to "svc_getargs()"
	 */
	void *arguments;

	/*
	 * Buffer to be passed to "svc_sendreply()"
	 */
	void *result;

	/*
	 * XDR routine for argument structure
	 */
	xdrproc_t xdr_arg;

	/*
	 * XDR routine for result structure
	 */
	xdrproc_t xdr_result;

	/*
	 * Service request pointer
	 */
	struct svc_req *svc_reqs;

	/*
	 * Function pointer for RPC dispatch routine
	 */
	dispatch_call_t dispatch_func_ptr;

	/*
	 * RPC transport handler
	 */
	SVCXPRT *transp;

	/*
	 * Indicate the request being dispatched (for multiple requests)
	 */
	bool_t dispatched;

	struct rpc_req_queue *next;

} rpc_req_queue_t;

rpc_error_t rpc_init_req_queue();
rpc_error_t rpc_req_queue_get(rpc_req_queue_t **req);
rpc_error_t rpc_req_queue_remove(rpc_req_queue_t *req);

#ifdef __cplusplus
}
#endif


#endif /* _RPC_REQUEST_QUEUE_H */
