#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.33	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libscutils/Makefile.com
#

LIBRARY= libscutils.a
VERS= .1

include         $(SRC)/Makefile.master

COBJECTS = scutils.o
OBJECTS = $(COBJECTS)

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

OBJECTS += svc_caller.o

LINTFILES = $(COBJECTS:%.o=%.ln)
CHECK_FILES = $(COBJECTS:%.o=%.c_check)

MAPFILE=	../common/mapfile-vers
PMAP=		-M $(MAPFILE)
SRCS =		$(COBJECTS:%.o=../common/%.c)

LIBS = $(DYNLIB)

# definitions for lint
LINTFLAGS += -Dlint

MTFLAG = -mt

CPPFLAGS += $(MTFLAG)
CPPFLAGS += -I.

LDLIBS += -lnsl -lpthread -lc -lclos
LDLIBS += -lsczones

AS_CPPFLAGS     += -D_ASM
# need to set this to NULL for lint
lint:= AS_CPPFLAGS     =
ASFLAGS         += -P
pics/svc_caller.o:= MTFLAG=

.KEEP_STATE:

.NO_PARALLEL:

all:  $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

objs/%.o pics/%.o: %.s
	$(COMPILE.s) -o $@ $<
