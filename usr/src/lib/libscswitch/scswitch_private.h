/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCSWITCH_PRIVATE_H
#define	_SCSWITCH_PRIVATE_H

#pragma ident	"@(#)scswitch_private.h	1.12	08/05/20 SMI"

/*
 * libscswitch private functions
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <locale.h>
#include <libintl.h>
#include <sys/types.h>
#include <rgm/rgm_scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <scadmin/scstat.h>

/*
 * Convert dcs error codes to scswitch error codes.
 *
 * Possible return values:
 *
 *      SCSWITCH_NOERR          - success
 *      SCSWITCH_ENOMEM         - not enough memory
 *      SCSWITCH_ENOTCLUSTER    - not a cluster node
 *      SCSWITCH_EINVAL         - scconf: invalid argument
 *      SCSWITCH_EPERM          - not root
 *      SCSWITCH_ECCR           - error accessing the CCR
 *      SCSWITCH_ENAME          - service name invalid
 *      SCSWITCH_EBUSY          - service busy
 *      SCSWITCH_EINACTIVE      - service has not yet been started
 *      SCSWITCH_ENODEID        - Node id does not exist in the cluster
 *      SCSWITCH_EINVALID_STATE - Node is in a invalid state
 *      SCSWITCH_EREPLICA_FAILED- Node failed to become the primary
 *      SCSWITCH_EUNEXPECTED    - internal or unexpected error
 *
 */
extern scswitch_errno_t scswitch_convert_dcs_error_code(dc_error_t);

/*
 * scswitch_validate_service_to_switch
 *
 * validates whether the service is running on the current node, i.e.
 * checks to see if the current node is the priamry node of the dcs
 * service.
 *
 * Possible return values:
 *
 *      SCSWITCH_NOERR          - success
 *      SCSWITCH_ENOTCLUSTER    - not a cluster node
 *      SCSWITCH_ENAME          - service name invalid
 *
 */
extern scswitch_errno_t scswitch_validate_service_to_switch(const char *,
    nodeid_t, const scconf_cfg_ds_t *, boolean_t *);

/*
 * switch_free_arrlist
 *
 *      This function free the memory for a two dimensional array.
 *
 * Possible return values:
 *      NONE
 */
extern void switch_free_arrlist(char **);

/*
 * scswitch_convert_scstat_error_code
 *
 * convert a scstat error code to scswitch error code
 *
 * Possible return values:
 *
 *      SCSWITCH_NOERR          - success
 *      SCSWITCH_ENOMEM         - not enough memory
 *      SCSWITCH_EINVAL         - scshutdown: invalid argument
 *      SCSWITCH_ERECONFIG      - cluster is reconfiguring
 *      SCSWITCH_EOBSOLETE      - Resource/RG has been updated
 *      SCSWITCH_EUNEXPECTED    - internal or unexpected error
 */
extern scswitch_errno_t scswitch_convert_scstat_error_code(
    const scstat_errno_t);

#ifdef __cplusplus
}
#endif

#endif	/* _SCSWITCH_PRIVATE_H */
