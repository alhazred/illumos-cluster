/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scswitch_misc.c	1.28	08/06/05 SMI"

/*
 * libscswitch support functions
 */

#include "scswitch_private.h"

static boolean_t is_local(
    const char *service_name, const scconf_cfg_ds_t *dsconfig);

/*
 * scswitch_strerr
 *
 * Map scswitch_errno_t to a string.
 *
 * The supplied "text" should be of at least SCSWITCH_MAX_STRING_LEN
 * in length.
 */
void
scswitch_strerr(char *name, char *text, scswitch_errno_t err)
{
	/* Check arguments */
	if (text == NULL)
		return;

	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Make sure that the buffer is terminated */
	bzero(text, BUFSIZ);

	if (name) {
		(void) sprintf(text, "%s:", name);
		text += strlen(text);
	}

	/* Find the string */
	switch (err) {
	case SCSWITCH_NOERR:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "no error"));
		break;
	case SCSWITCH_ENOMEM:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "not enough memory"));
		break;
	case SCSWITCH_ENOCLUSTER:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "not a cluster node"));
		break;
	case SCSWITCH_EUNEXPECTED:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "unexpected error"));
		break;
	case SCSWITCH_EINVAL:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "invalid argument"));
		break;
	case SCSWITCH_EPERM:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "permission denied"));
		break;
	case SCSWITCH_ENOEXIST:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "the nodename or nodeid"
		    " is not found"));
		break;
	case SCSWITCH_ECCR:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "error accessing the CCR"));
		break;
	case SCSWITCH_ENAME:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "service name invalid"));
		break;
	case SCSWITCH_EBUSY:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "service is busy"));
		break;
	case SCSWITCH_EINACTIVE:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "service has not yet been"
		    " started"));
		break;
	case SCSWITCH_ENODEID:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "node does not exist in"
		    " the cluster"));
		break;
	case SCSWITCH_EINVALSTATE:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "The primary node for"
			" this device group could not be assigned."));
		break;
	case SCSWITCH_EREPLICA_FAILED:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "node failed to become the"
		    " primary"));
		break;
	case SCSWITCH_RGSWITCH_FAILED:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "switch rg failed"));
		break;
	case SCSWITCH_ERECONFIG:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "cluster is reconfiguring"));
		break;
	case SCSWITCH_EOBSOLETE:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN,
		    "resource/RG has been updated"));
		break;
	case SCSWITCH_ERG:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "invalid rg"));
		break;
	case SCSWITCH_ERGNODE:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "no valid nodelist for rg"));
		break;
	case SCSWITCH_ENULLRGLIST:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "empty rg list"));
		break;
	case SCSWITCH_EDUPNODE:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "duplicate node name for rg"));
		break;
	case SCSWITCH_EDUPRG:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "duplicate rg name"));
		break;
	case SCSWITCH_EDETACHED:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "rg contains detached"
		    " resource"));
		break;
	case SCSWITCH_EMAXPRIMARIES:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "exceeds maximum_primaries"
		    " for rg"));
		break;
	case SCSWITCH_EMASTER:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "node is not a potential"
		    " master of rg"));
		break;
	case SCSWITCH_ESTOPFAILED:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "rg ERROR_STOP_FAILED"));
		break;
	case SCSWITCH_ERGRECONF:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "one or more RGs are"
		    " reconfiguring"));
		break;
	case SCSWITCH_ERGSTATE:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "unmanaged rg"));
		break;
	case SCSWITCH_EUSAGE:
	case SCSWITCH_ESCHA_ERR:
		break;
	case SCSWITCH_EMASTER_SERVICE:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "node is not allowed to be a"
		    " primary"));
		break;
	case SCSWITCH_ECZONE:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "You cannot run this command"
			" from a zone cluster."));
		break;
	default:
		(void) sprintf(text, "%s\n",
		    dgettext(TEXT_DOMAIN, "unknown error"));
		break;
	}
}

/*
 * scswitch_convert_scconf_error_code
 *
 * convert a scconf error code to scstat error code
 *
 * Possible return values:
 *
 *	SCSWITCH_NOERR		- success
 *	SCSWITCH_ENOMEM		- not enough memory
 *	SCSWITCH_ENOCLUSTER	- not a cluster node
 *	SCSWITCH_EINVAL		- scshutdown: invalid argument
 *	SCSWITCH_EPERM		- not root
 *	SCSWITCH_EUNEXPECTED	- internal or unexpected error
 *	SCSWITCH_ENOEXIST	- the nodename (or, id) is not found
 */
scswitch_errno_t
scswitch_convert_scconf_error_code(const scconf_errno_t scconf_error)
{
	scswitch_errno_t error = SCSWITCH_EUNEXPECTED;

	switch (scconf_error) {
	case SCCONF_NOERR:
		error = SCSWITCH_NOERR;
		break;

	case SCCONF_EPERM:
		error = SCSWITCH_EPERM;
		break;

	case SCCONF_ENOCLUSTER:
		error = SCSWITCH_ENOCLUSTER;
		break;

	case SCCONF_ENOMEM:
		error = SCSWITCH_ENOMEM;
		break;

	case SCCONF_EINVAL:
		error = SCSWITCH_EINVAL;
		break;

	case SCCONF_EUNEXPECTED:
		error = SCSWITCH_EUNEXPECTED;
		break;

	case SCCONF_ENOEXIST:
		error = SCSWITCH_ENOEXIST;
		break;

	case SCCONF_ECZONE:
		error = SCSWITCH_ECZONE;
		break;
	default:
		break;
	}
	return (error);
}

/*
 * scswitch_convert_scstat_error_code
 *
 * convert a scstat error code to scswitch error code
 *
 * Possible return values:
 *
 *	SCSWITCH_NOERR		- success
 *	SCSWITCH_ENOMEM		- not enough memory
 *	SCSWITCH_EINVAL		- scshutdown: invalid argument
 *	SCSWITCH_ERECONFIG	- cluster is reconfiguring
 *	SCSWITCH_EOBSOLETE	- Resource/RG has been updated
 *	SCSWITCH_EUNEXPECTED	- internal or unexpected error
 */
scswitch_errno_t
scswitch_convert_scstat_error_code(const scstat_errno_t scstat_error)
{
	scswitch_errno_t error = SCSWITCH_EUNEXPECTED;

	switch (scstat_error) {
	case SCSTAT_ENOERR:
		error = SCSWITCH_NOERR;
		break;

	case SCSTAT_ENOMEM:
		error = SCSWITCH_ENOMEM;
		break;

	case SCSTAT_EINVAL:
		error = SCSWITCH_EINVAL;
		break;

	case SCSTAT_ERGRECONFIG:
		error = SCSWITCH_ERECONFIG;
		break;

	case SCSTAT_EOBSOLETE:
		error = SCSWITCH_EOBSOLETE;
		break;

	case SCSTAT_EUNEXPECTED:
		error = SCSWITCH_EUNEXPECTED;
		break;

	default:
		break;
	}
	return (error);
}

/*
 * scswitch_convert_dcs_error_code
 *
 * Convert dcs error codes to scswitch error codes.
 *
 * Possible return values:
 *
 *	SCSWITCH_NOERR		- success
 *	SCSWITCH_ENOMEM		- not enough memory
 *	SCSWITCH_ENOCLUSTER	- not a cluster node
 *	SCSWITCH_EINVAL		- scconf: invalid argument
 *	SCSWITCH_EPERM		- not root
 *	SCSWITCH_ECCR		- error accessing the CCR
 *	SCSWITCH_ENAME		- service name invalid
 *	SCSWITCH_EBUSY		- service busy
 *	SCSWITCH_EINACTIVE	- service has not yet been started
 *	SCSWITCH_ENODEID	- Node id does not exist in the cluster
 *	SCSWITCH_EINVALSTATE    - Node is in an invalid state
 *	SCSWITCH_EREPLICA_FAILED- Node failed to become the primary
 *	SCSWITCH_EUNEXPECTED	- internal or unexpected error
 *	SCSWITCH_ERECONFIG	- DCS freeze operation in progress
 *
 */
scswitch_errno_t
scswitch_convert_dcs_error_code(dc_error_t dc_err)
{

	scswitch_errno_t error = SCSWITCH_EUNEXPECTED;

	switch (dc_err) {
		case DCS_SUCCESS:
			error = SCSWITCH_NOERR;
			break;

		case DCS_ERR_NOT_CLUSTER:
			error = SCSWITCH_ENOCLUSTER;
			break;

		case DCS_ERR_CCR_ACCESS:
			error = SCSWITCH_ECCR;
			break;

		case DCS_ERR_SERVICE_NAME:
			error = SCSWITCH_ENAME;
			break;

		case DCS_ERR_SERVICE_BUSY:
			error = SCSWITCH_EBUSY;
			break;

		case DCS_ERR_SERVICE_INACTIVE:
			error = SCSWITCH_EINACTIVE;
			break;

		case DCS_ERR_NODE_ID:
			error = SCSWITCH_ENODEID;
			break;

		case DCS_ERR_INVALID_STATE:
			error = SCSWITCH_EINVALSTATE;
			break;

		case DCS_ERR_REPLICA_FAILED:
			error = SCSWITCH_EREPLICA_FAILED;
			break;

		case DCS_FREEZE_IN_PROGRESS:
			error = SCSWITCH_ERECONFIG;
			break;

		default:
			break;
	}
	return (error);
}

/*
 * scswitch_validate_service_to_switch
 *
 * validates whether the service is running on the current node, i.e.
 * checks to see if the current node is the priamry node of the dcs
 * service.
 * Input: service_name: name of the dcs device group.
 *	  nodeid: node from where the command is issued.
 *
 * Output: switch_flg: set to true if the dcs group is mastered by the node
 *	   id of which is passed using nodeid.
 *
 * Possible return values:
 *
 *	SCSWITCH_NOERR		- success
 *	SCSWITCH_ENOCLUSTER	- not a cluster node
 *	SCSWITCH_ENAME		- service name invalid
 *
 */
scswitch_errno_t
scswitch_validate_service_to_switch(const char *service_name, nodeid_t nodeid,
    const scconf_cfg_ds_t *dsconfig, boolean_t *switch_flg)
{
	scswitch_errno_t scswitcherr = SCSWITCH_NOERR;
	dc_error_t dc_err = DCS_SUCCESS;
	dc_replica_status_seq_t *active_replicas;
	int i, is_suspended;
	sc_state_code_t state;

	*switch_flg = B_FALSE;

	/* local services should not be switched */
	if (is_local(service_name, dsconfig)) {
		return (scswitcherr);
	}
	/* get the dcs service status */
	dc_err = dcs_get_service_status(service_name,
	    &is_suspended, &active_replicas, &state, NULL);

	if (dc_err != DCS_SUCCESS) {
		scswitcherr = scswitch_convert_dcs_error_code(dc_err);
		if (scswitcherr != SCSWITCH_NOERR)
			goto cleanup;
	}

	/* checks if the service is active on the node */
	for (i = 0; i < (int)active_replicas->count; i++) {
		if (((active_replicas->replicas)[i].id == nodeid) &&
		    ((active_replicas->replicas)[i].state == PRIMARY)) {
			/* set the flag if service is active on the node */
			*switch_flg = B_TRUE;
			goto cleanup;
		}
	}

cleanup:
	dcs_free_dc_replica_status_seq(active_replicas);
	return (scswitcherr);
}

/*
 * free_arrlist
 *
 *	This function free the memory for a two dimensional array.
 *
 * Possible return values:
 *	NONE
 */
void
switch_free_arrlist(char **data)
{
	int i = 0;

	if (!data) {
		return;
	}

	while (data[i] != NULL) {
		free(data[i]);
		i++;
	}
	free((char *)data);
}

/*
 * scswitch_getrglist
 *
 * This function returns a single list of all known resource group names.
 * Both managed and unmanaged RGs are returned in the single list.
 *
 * The structure of the new list is a NULL terminated array of pointers
 * to strings.  Upon success, a pointer to the list is placed
 * in "all_rgnamesp".   It is the caller's responsibility to
 * free all memory associated with the list.
 * If "zc_name", zone cluster name, was specified, then all the
 * resource group names will be pre-pended with the zone cluster name
 * in the format <zone cluster name>:<rg name>
 */
scha_errmsg_t
scswitch_getrglist(char **all_rgnamesp[], boolean_t only_local_rgs,
    char *zc_name)
{
	scha_errmsg_t scha_err = { SCHA_ERR_NOERR, NULL };
	char **managed_rgnames = (char **)0;
	char **unmanaged_rgnames = (char **)0;
	char **all_rgnames = NULL;
	char **rgnamesp1, **rgnamesp2;
	int i, count, str_len;
	char *rg_name;

	uint_t nodeid;
	/*
	 * Get all resource groups
	 */
	scha_err = rgm_scrgadm_getrglist(&managed_rgnames,
	    &unmanaged_rgnames, only_local_rgs, zc_name);

	/* If no error, but no rgs returned, return NULL list */
	if (scha_err.err_code == SCHA_ERR_NOERR &&
	    !managed_rgnames && !unmanaged_rgnames) {
		*all_rgnamesp = all_rgnames;
		return (scha_err);
	}

	/* If error, take error return */
	if (scha_err.err_code != SCHA_ERR_NOERR)
		goto cleanup;

	/*
	 * Combine both managed and unmanaged RGs into a single list
	 */
	count = 0;
	for (i = 0;  i < 2;  ++i) {
		rgnamesp1 = (i == 0) ? managed_rgnames : unmanaged_rgnames;
		if (rgnamesp1) {
			while (*rgnamesp1) {
				++count;
				++rgnamesp1;
			}
		}
	}

	/* Allocate memory for combined list */
	all_rgnames = (char **)calloc((size_t)(count + 1), sizeof (char *));
	if (all_rgnames == (char **)0) {
		scha_err.err_code = SCHA_ERR_NOMEM;
		scha_err.err_msg = NULL;
		goto cleanup;
	}

	/* Generate the combined list */
	rgnamesp2 = all_rgnames;
	for (i = 0;  i < 2;  ++i) {
		rgnamesp1 = (i == 0) ? managed_rgnames : unmanaged_rgnames;
		if (rgnamesp1) {
			while (*rgnamesp1) {
				/* Make a copy of rg names */
				if (zc_name) {
					str_len = strlen(zc_name) + 2;
					str_len += strlen(*rgnamesp1);
					rg_name = calloc(1, str_len);
					strcat(rg_name, zc_name);
					strcat(rg_name, ":");
					strcat(rg_name, *rgnamesp1);
					*rgnamesp2++ = rg_name;
					*rgnamesp1++;
				} else {
					*rgnamesp2++ = strdup(*rgnamesp1++);
				}
			}
		}
	}

	/* Set the return value pointer */
	*all_rgnamesp = all_rgnames;

cleanup:
	/* Free array managed RG names */
	if (managed_rgnames)
		rgm_free_strarray(managed_rgnames);

	/* Free array unmanaged RG names */
	if (unmanaged_rgnames)
		rgm_free_strarray(unmanaged_rgnames);

	return (scha_err);
}

/*
 * is_local()
 *
 * Input: device group name and pointer to cluster device configuration
 *
 * Action:
 *	determine if a device group named by service_name is a Local Disk group
 *
 * Returns: B_TRUE/B_FALSE
 *
 */
static boolean_t is_local(
    const char *service_name, const scconf_cfg_ds_t *dsconfig) {

	boolean_t is_local_type = B_FALSE;
	const scconf_cfg_ds_t *device_group;

	if (!service_name || dsconfig == NULL)
		return (is_local_type);

	for (device_group = dsconfig; device_group;
	    device_group = device_group->scconf_ds_next) {

		if (!device_group->scconf_ds_name ||
		    (strcmp(service_name, device_group->scconf_ds_name) != 0))
			continue;

		if (device_group->scconf_ds_type &&
		    (strcmp(device_group->scconf_ds_type, "Local_Disk") == 0)) {
			is_local_type = B_TRUE;
			break;
		}
	}
	return (is_local_type);
} /* is_local */
