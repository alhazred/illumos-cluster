/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)libscswitch.c	1.39	08/08/07 SMI"

/*
 * libscswitch evacuate dgs and rgs functions
 */

#include <scadmin/scswitch.h>
#include <dc/libdcs/libdcs.h>
#include <scadmin/scconf.h>
#include <libintl.h>
#include <strings.h>
#include "scswitch_private.h"

/*
 * scswitch_switch_service
 *
 * Switchover the service specified by "slist" to the node specified by
 * "node_name".  If "node_name" is NULL, the service is switched over to the
 * next preferred node.
 *
 * If the service specified is currently offline, this call will start it up.
 *
 * Possible return values:
 *	SCSWITCH_NOERR		- success
 *	SCSWITCH_ENOCLUSTER	- node is not in the cluster
 *	SCSWITCH_ENODEID	- node specified is invalid
 *	SCSWITCH_EINVALSTATE	- service is in an invalid state
 *	SCSWITCH_EMASTER_SERVICE- node is not allowed to be primary
 *
 */
scswitch_errno_t
scswitch_switch_service(scswitch_service_t *slist,
	char *node_name)
{
	int dcerr = 0;
	scconf_nodeid_t nodeid = (scconf_nodeid_t)0;
	scconf_errno_t scconferr;
	dc_replica_seq_t *nodes_seq;
	boolean_t node_found;
	unsigned int i;
	scswitch_service_t *slist_cp = slist;

	/* initialize the dcs library */
	dcerr = dcs_initialize();
	if (dcerr != 0)
		return (SCSWITCH_ENOCLUSTER);

	/* Get the node id for "node_name" */
	if (node_name != NULL) {
		scconferr = scconf_get_nodeid(node_name, &nodeid);
		if (scconferr != SCCONF_NOERR) {
			return (SCSWITCH_ENODEID);
		}
	}

	if (nodeid != 0) {
		while (slist_cp) {
			dcerr = dcs_get_service_parameters(
			    slist_cp->scswitch_service_name, NULL, NULL,
			    &nodes_seq, NULL, NULL, NULL);
			if (dcerr != DCS_SUCCESS)
				return (scswitch_convert_dcs_error_code(dcerr));

			node_found = B_FALSE;
			for (i = 0; i < nodes_seq->count; i++) {
				if ((nodes_seq->replicas)[i].id == nodeid) {
					node_found = B_TRUE;
					break;
				}
			}
			dcs_free_dc_replica_seq(nodes_seq);
			if (node_found == B_FALSE)
				return (SCSWITCH_EMASTER_SERVICE);

			slist_cp = slist_cp->next;
		}
	}

	/* Iterate through slist and perform the switchovers */
	while (slist) {
		dcerr = dcs_resume_service(slist->scswitch_service_name);
		if (dcerr != DCS_SUCCESS) {
			return (scswitch_convert_dcs_error_code(dcerr));
		}

		dcerr = dcs_startup_service(slist->scswitch_service_name,
		    (nodeid_t)nodeid);
		if (dcerr != DCS_SUCCESS) {
			return (scswitch_convert_dcs_error_code(dcerr));
		}
		slist = slist->next;
	}

	return (SCSWITCH_NOERR);
}

/*
 * scswitch_resume_service
 *
 * resumes a dcs service that is currently suspended.
 *
 * Possible return values :
 * SCSWITCH_NOERR		- Success
 * SCSWITCH_ECLUSTER		- Not a cluster member
 * SCSWITCH_EINVAL		- Service is in invalid state
 * SCSWITCH_EMASTER_SERVICE	- node is not allowed to be primary
 * SCSWITCH_EUNEXPECTED		- Bad call
 *
 */
scswitch_errno_t
scswitch_resume_service(char *service)
{
	int dcerr = DCS_SUCCESS;

	if (!service)
		return (SCSWITCH_EUNEXPECTED);

	dcerr = dcs_resume_service(service);
	if (dcerr != DCS_SUCCESS) {
		return (scswitch_convert_dcs_error_code(dcerr));
	}

	return (SCSWITCH_NOERR);
}


/*
 * scswitch_take_service_offline
 *
 * Shutdown the services specified by "slist".
 *
 * Possible return values:
 *	SCSWITCH_NOERR		- success
 *	SCSWITCH_ENOCLUSTER	- node not in cluster
 *	SCSWITCH_EINVALSTATE	- service is in an invalid state
 *
 */
scswitch_errno_t
scswitch_take_service_offline(scswitch_service_t *slist)
{
	int dcerr = 0;

	/* initialize the dcs library */
	dcerr = dcs_initialize();
	if (dcerr != 0)
		return (SCSWITCH_ENOCLUSTER);

	while (slist) {
		dcerr = dcs_shutdown_service(slist->scswitch_service_name, 0);
		if (dcerr != DCS_SUCCESS) {
			return (scswitch_convert_dcs_error_code(dcerr));
		}
		slist = slist->next;
	}

	return (SCSWITCH_NOERR);
}

/*
 * scswitch_suspend_service
 *
 * Suspend the services specified by "slist".
 *
 * Possible return values:
 *	SCSWITCH_NOERR		- success
 *	SCSWITCH_ENOCLUSTER	- node not in cluster
 *	SCSWITCH_EINVALSTATE	- service is in an invalid state
 *
 */
scswitch_errno_t
scswitch_suspend_service(scswitch_service_t *slist)
{
	int dcerr = 0;

	/* initialize the dcs library */
	dcerr = dcs_initialize();
	if (dcerr != 0)
		return (SCSWITCH_ENOCLUSTER);

	while (slist) {
		dcerr = dcs_shutdown_service(slist->scswitch_service_name, 1);
		if (dcerr != DCS_SUCCESS) {
			return (scswitch_convert_dcs_error_code(dcerr));
		}
		slist = slist->next;
	}

	return (SCSWITCH_NOERR);
}

/*
 * scswitch_evacuate_devices
 *
 *  evacuate devices from current primary to secondary or shudown all the
 *  devices in the cluster  based on the value passed to 'nodeid' parameter.
 *
 * input: if (nodeid) => evacuate devices from current primary to secondary
 *        if (!nodeid) => shutdown all the devices in the cluster.
 *
 * Possible return values:
 *      SCSWITCH_NOERR          - success
 *      SCSWITCH_ENOMEM         - not enough memory
 *      SCSWITCH_ENOTCLUSTER    - not a cluster node
 *      SCSWITCH_EINVAL         - scshutdown: invalid argument
 *      SCSWITCH_EPERM          - not root
 *      SCSWITCH_ECCR           - error accessing the CCR
 *      SCSWITCH_ENAME          - service name invalid
 *      SCSWITCH_EBUSY          - service busy
 *      SCSWITCH_EINACTIVE      - service has not yet been started
 *      SCSWITCH_ENODEID        - Node id does not exist in the cluster
 *      SCSWITCH_EINVALSTATE - Node is in a invalid state
 *      SCSWITCH_EREPLICA_FAILED- Node failed to become the primary
 *      SCSWITCH_EUNEXPECTED    - internal or unexpected error
 *
 */
scswitch_errbuff_t
scswitch_evacuate_devices(nodeid_t nodeid)
{
	scswitch_errbuff_t scshutdown_err = { SCSWITCH_NOERR, NULL };
	dc_error_t dc_err = DCS_SUCCESS;
	dc_string_seq_t *service_names = NULL;
	char *service_name = NULL;
	boolean_t switch_flg = B_FALSE;
	int service_cnt = 0;
	scconf_cfg_ds_t *dsconfig = NULL;
	scconf_errno_t scconferr;

	/*
	 * Need to pre-allocate space now for any error message string,
	 * as you will not be able to allocate later if you've errored
	 * because of ENOMEM.
	 */
	scshutdown_err.err_msg = (char *)malloc(sizeof (char) * BUFSIZ);
	if (scshutdown_err.err_msg == NULL) {
		scshutdown_err.err_code = SCSWITCH_ENOMEM;
		goto cleanup;
	}

	scconferr = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG, &dsconfig);
	if (scconferr != SCCONF_NOERR) {
		scconf_strerr(scshutdown_err.err_msg, scconferr);
		scshutdown_err.err_code = scswitch_convert_scconf_error_code(
		    scconferr);
		goto cleanup;
	}

	dc_err = dcs_get_service_names(&service_names);

	scshutdown_err.err_code = scswitch_convert_dcs_error_code(dc_err);
	if (scshutdown_err.err_code != SCSWITCH_NOERR) {
		scswitch_strerr(NULL, scshutdown_err.err_msg,
		    scshutdown_err.err_code);
		goto cleanup;
	}

	for (service_cnt = 0; service_cnt < (int)service_names->count;
	    service_cnt++) {

		/* get the name of the dcs device group */
		service_name = (service_names->strings)[service_cnt];

		/* If evacuate all the dgs from the node */
		if (nodeid) {
			/* validates which dgs are online on the node */
			scshutdown_err.err_code =
			    scswitch_validate_service_to_switch(
			    service_name, nodeid,
			    (const scconf_cfg_ds_t *)dsconfig, &switch_flg);
			/* If no error and switch flag is not set */
			if ((scshutdown_err.err_code == SCSWITCH_NOERR) &&
			    (!switch_flg))
				continue;
			/* If no error and switch flag is set */
			else if ((scshutdown_err.err_code == SCSWITCH_NOERR) &&
			    (switch_flg)) {
				dc_err = dcs_switchover_service_from_node(
				    service_name, nodeid);
				if (dc_err == DCS_ERR_INVALID_STATE) {
					/*
					 * There is no other replica to
					 * switchover to.  Try shutting down
					 * the service.  This will fail if the
					 * service is currently in use.
					 */
					dc_err = dcs_shutdown_service(
					    service_name, 0);
				}
				if (dc_err != DCS_SUCCESS) {
					scshutdown_err.err_code =
					    scswitch_convert_dcs_error_code(
					    dc_err);
					scswitch_strerr(service_name,
					    scshutdown_err.err_msg,
					    scshutdown_err.err_code);
					goto cleanup;
				}
			} else {
				if (scshutdown_err.err_code != SCSWITCH_NOERR) {
					scswitch_strerr(service_name,
					    scshutdown_err.err_msg,
					    scshutdown_err.err_code);
					goto cleanup;
				}
			}

		/* If shutdown the cluster */
		} else {
			/* Shutdown all the dgs in the cluster */
			dc_err = dcs_shutdown_service(service_name, 0);
			if (dc_err != DCS_SUCCESS) {
				scshutdown_err.err_code =
				    scswitch_convert_dcs_error_code(dc_err);
				scswitch_strerr(service_name,
				    scshutdown_err.err_msg,
				    scshutdown_err.err_code);
				goto cleanup;
			}

		}
	}

cleanup:
	dcs_free_string_seq(service_names);
	if (scshutdown_err.err_code == SCSWITCH_NOERR) {
		/*
		 * No error, free the pre-allocated space for the error
		 * message string.
		 */
		free(scshutdown_err.err_msg);
	}
	return (scshutdown_err);
}

/*
 * scswitch_evacuate_rgs
 *
 *  evacuate rgs from current primary to secondary or bring all the
 *  rgs offline in the cluster  based on the value passed to 'nodename'
 *  parameter.
 *
 * input: if (nodename) => evacuate rgs from nodename
 *        if (!nodename) => bring all the rgs in the cluster offline.
 *
 * v_flag is used by the newcli for the verbose print.
 *
 * Return value is scha_errmsg_t passed back from the rgmd.
 */
scha_errmsg_t
scswitch_evacuate_rgs(const char *nodename, ushort_t evac_timeout,
    boolean_t v_flag, char *cluster)
{
	scha_errmsg_t scha_err = { SCHA_ERR_NOERR, NULL };
	char **managed_rgs = NULL;
	char **unmanaged_rgs = NULL;
	char *nodebuff = NULL;
	const char *evacnode[2] = {NULL, NULL};	/* hold the single evac node */

	/* Evacuate all the rgs mastered by nodename */
	if (nodename != NULL) {
		evacnode[0] = nodename;
		scha_err = rgm_scswitch_switch_rg(evacnode, NULL,
		    RGACTION_EVACUATE, evac_timeout, v_flag, cluster);
		if (scha_err.err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}

	/* If cluster shutdown (called by scshutdown.c) */
	} else {
		/* Get list of all the rgs in the right format */
		scha_err = rgm_scrgadm_getrglist(&managed_rgs,
		    &unmanaged_rgs, B_FALSE, cluster);
		if (scha_err.err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}
		/* Check to see if there are no managed resource group's */
		if (managed_rgs != NULL) {
			/*
			 * Switch all the rgs in the cluster to offline state.
			 * Set node evacuate and rebalance flags false, because
			 * we do not want to rebalance any RGs in this case.
			 */
			scha_err = rgm_scswitch_switch_rg((const char **)
			    &nodebuff, (const char **)managed_rgs,
			    RGACTION_CLUSTER_SHUTDOWN, 0, v_flag, cluster);
			if (scha_err.err_code != SCHA_ERR_NOERR) {
				goto cleanup;
			}
		}
	}

cleanup:
	switch_free_arrlist(managed_rgs);
	switch_free_arrlist(unmanaged_rgs);
	return (scha_err);
}

/*
 * scswitch_evacuate
 *
 *  Evacuate all resource and device groups from the given "nodename" which
 *  may indicate a node or zone.
 *  The 'nodeid' argument gives the physical nodeid of the node, and
 *  'zonename' is the name of the non-global zone, or NULL if it is the
 *  global zone.  'timeout' provides the sticky evacuation timeout.
 *
 *  NOTE - The following comments might not apply any more (at least to
 *  evacuation of RGs) now that it is done in the rgmd.  However, I
 *  am leaving the comments here in case they are applicable to device
 *  group evacuation.
 *
 *  XXX - All of the evacuate functions SHOULD attempt to evacuate
 *  as many resource and device groups as they possibly can.  The
 *  associated bug is:
 *
 *	4297983 scshutdown -f stops switching over device groups once
 *	    a switchover fails
 *
 *  Since the evacuate node functionality has moved from scshutdown
 *  to scswitch, this bug applies to scswitch.   In order to fix this,
 *  we need a much more reasonable way of bubbling messages back up to
 *  the caller than we currently have today.   Moving the evacuate
 *  node functionality from scshutdown to scswitch should maintain
 *  bug-for-bug compatability.
 *
 * Possible return values:
 *      SCSWITCH_NOERR          - success
 *      SCSWITCH_EINVAL         - scshutdown: invalid argument
 *      SCSWITCH_ENOMEM         - not enough memory
 *      SCSWITCH_EPERM          - not root
 *      SCSWITCH_ENOEXIST       - the nodename (or, id) is not found
 *      SCSWITCH_ENOTCLUSTER    - not a cluster node
 *      SCSWITCH_ERECONFIG      - cluster is reconfiguring
 *      SCSWITCH_EOBSOLETE      - Resource/RG has been updated
 *      SCSWITCH_EUNEXPECTED    - unexpected error
 *
 *  RG evacuation is now performed by the rgmd, so we pass back
 *  a scha_errmsg_t result (thru the scha_err argument) as well as
 *  as the scswitch_errbuf_t return value.  If the scha_err code
 *  is non-zero, then the function returns early and the caller should
 *  use the scha error code/message.
 *
 *  v_flag is used by the newcli for the verbose print.
 */
scswitch_errbuff_t
scswitch_evacuate(nodeid_t nodeid, const char *zonename, const char *nodename,
    ushort_t timeout, scha_errmsg_t *scha_err, boolean_t v_flag, char *cluster)
{
	scswitch_errbuff_t scshutdownerr = { SCSWITCH_NOERR, NULL };

	/* Evacuate resource groups for the node */
	*scha_err = scswitch_evacuate_rgs(nodename, timeout, v_flag, cluster);
	if (scha_err->err_code != SCSWITCH_NOERR)
		/* caller will print the scha error message */
		return (scshutdownerr);

	/* Evacuate device groups for physical nodes only */
	if (zonename == NULL) {
		scshutdownerr = scswitch_evacuate_devices(nodeid);
	}

	return (scshutdownerr);
}

/*
 * is_current_cluster_member()
 *
 * Input: nodename
 *
 * Action:
 *	determine if machine named by nodename is a current cluster member
 *
 * Returns: B_TRUE/B_FALSE
 *
 * Note that if scconf returns other than SCCONF_NOERR we return B_FALSE.
 *	If we're out of memory it will show up in other calls and be reported
 *	from them. This effectively masks SCCONF_EINVAL and SCCONF_EUNEXPECTED
 *	but this shouldn't really be a problem...
 */
boolean_t is_current_cluster_member(char *nodename) {
	scconf_errno_t scconf_status = SCCONF_NOERR;
	boolean_t is_member = B_TRUE;
	scconf_nodeid_t nodeid;
	uint_t mbr = 0;

	scconf_status = scconf_get_nodeid(nodename, &nodeid);

	if (scconf_status == SCCONF_NOERR) {
		scconf_status = scconf_ismember(nodeid, &mbr);
	}
	if (scconf_status != SCCONF_NOERR || !mbr) {
		is_member = B_FALSE;
	}

	return (is_member);
} /* is_current_cluster_member */
