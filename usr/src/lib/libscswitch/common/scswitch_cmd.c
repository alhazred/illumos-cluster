/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scswitch_cmd.c	1.27	08/06/20 SMI"
/*
 * scswitch_cmd.c
 *
 * libscswitch functions for scswitch(1M)
 */

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>

#include <dc/libdcs/libdcs.h>

#include <scadmin/scconf.h>
#include <scadmin/scswitch.h>
#include <rgm/rgm_scswitch.h>
#include <rgm/sczones.h>
#include "scswitch_private.h"
#include <rgm/rgm_errmsg.h>
#include <rgm/rgm_common.h>

/*
 * scswitch_switch_rg
 *	Requests the RGM to switch RGs to specified nodes.
 *
 *
 * Except during an uncommitted rolling upgrade, This function is *not*
 * used to evacuate nodes or to rebalance RGs; those are performed by
 * direct calls to rgm_scswitch_switch_rg() with appropriate arguments.
 *
 * verbose_flag is used by the newcli for the verbose print.
 *  Possible return values:
 *	scha_errmsg_t
 */

scha_errmsg_t
scswitch_switch_rg(char *scswitch_node_list, char *scswitch_rg_list,
boolean_t verbose_flag, char *cluster)
{
	char **nodename_buff = NULL;
	char **rgname_buff = NULL;
	scha_errmsg_t scha_error = { SCHA_ERR_NOERR, NULL };

	/* node list may be empty */
	if (scswitch_node_list != NULL) {
		scha_error = scswitch_dup_list(&nodename_buff,
		    scswitch_node_list);
		if (scha_error.err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}
	}

	/* rg list must be non-empty */
	if (scswitch_rg_list == NULL) {
		scha_error.err_code = SCHA_ERR_INVAL;
		goto cleanup;
	}

	scha_error = scswitch_dup_list(&rgname_buff, scswitch_rg_list);
	if (scha_error.err_code != SCHA_ERR_NOERR)
		goto cleanup;

	/* Node evauate and rebalance flags are set false */
	scha_error = rgm_scswitch_switch_rg((const char **)nodename_buff,
	    (const char **)rgname_buff, RGACTION_NONE, 0, verbose_flag,
	    cluster);

cleanup:
	rgm_free_strarray(nodename_buff);
	rgm_free_strarray(rgname_buff);
	return (scha_error);
}

/*
 * scswitch_switch_device_service
 *	Switch device services to specified node.
 *
 *  Possible return values:
 *	SCSWITCH_NOERR
 *	SCSWITCH_EUSAGE
 *	SCSWITCH_ENOMEM
 *	SCSWITCH_ENOCLUSTER
 *	SCSWITCH_ENODEID
 *	SCSWITCH_EINVALSTATE
 *
 */
scswitch_errno_t
scswitch_switch_device_service(char *scswitch_node_list,
	char *scswitch_service_list)
{
scswitch_service_t *slist = NULL;
scswitch_errno_t scswitcherr = SCSWITCH_NOERR;

	/* check whether more than one node is specified */
	if (strchr(scswitch_node_list, ',')) {
		return (SCSWITCH_EUSAGE);
	}
	scswitcherr = scswitch_alloc_service_list(&slist,
	    scswitch_service_list);
	if (scswitcherr != SCSWITCH_NOERR)
		return (scswitcherr);
	scswitcherr = scswitch_switch_service(slist, scswitch_node_list);
	scswitch_free_service_list(slist);
	return (scswitcherr);
}


/*
 * scswitch_restart_rg
 *	Restart RGs on the specified nodes.
 *
 * Possible return values:
 *	SCSWITCH_NOERR		- no error is found
 *	SCSWITCH_RG		- invalid RG
 *	SCSWITCH_ENODEID	- node doesn't master the RG
 *
 * verbose_flag is used by the newcli for the verbose print.
 */
scha_errmsg_t
scswitch_restart_rg(char *scswitch_node_list, char *scswitch_rg_list,
    boolean_t verbose_flag, char *cluster)
{
	char **nodename_buff = NULL;
	char **rgname_buff = NULL;
	scha_errmsg_t scha_error = { SCHA_ERR_NOERR, NULL };

	if (scswitch_node_list != NULL) {
		scha_error = scswitch_dup_list(&nodename_buff,
		    scswitch_node_list);
		if (scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;
	}

	if (scswitch_rg_list != NULL) {
		scha_error = scswitch_dup_list(&rgname_buff,
		    scswitch_rg_list);
		if (scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;
	}

	scha_error = rgm_scswitch_restart_rg((const char **)nodename_buff,
	    (const char **)rgname_buff, verbose_flag, cluster);

cleanup:
	rgm_free_strarray(nodename_buff);
	rgm_free_strarray(rgname_buff);
	return (scha_error);
}

/*
 * scswitch_cli_take_service_offline
 *	Take specified device group offline or in maintenace mode.
 *	This function is been called from the command scswitch(1M)
 *	with the argument of type char *
 *
 *  Possible return values:
 *	SCSWITCH_NOERR
 *	SCSWITCH_ENOMEM
 *	SCSWITCH_EINVAL
 *	SCSWITCH_ENOCLUSTER
 *	SCSWITCH_EINVALSTATE
 *
 */
scswitch_errno_t
scswitch_cli_take_service_offline(char *scswitch_service_list, int offline_flg)
{
scswitch_service_t *slist = NULL;
scswitch_errno_t scswitcherr = SCSWITCH_NOERR;

	scswitcherr = scswitch_alloc_service_list(&slist,
	    scswitch_service_list);
	if (scswitcherr != SCSWITCH_NOERR)
		return (scswitcherr);
	if (offline_flg != 0)
		scswitcherr = scswitch_suspend_service(slist);
	else
		scswitcherr = scswitch_take_service_offline(slist);

	scswitch_free_service_list(slist);
	return (scswitcherr);

}

/*
 * scswitch_set_resource_switch
 *	Enable or disable resource.
 *	This function sets the on/off_switch to DISABLED or ENABLE of the
 *	given Resource.
 *
 * FBC 1403: Get the nodelist and separate the nodes. Pass it to the rgm
 * function rgm_scswitch_set_resource_switch.
 * Call the rgm_free_strarray for the nodelist.
 *
 * recursive_flg is used for recursive enabling/disabling of
 * dependee/dependents.
 *
 * verbose_flag is used by the newcli for the verbose print.
 *
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_set_resource_switch(char *scswitch_resource_list, scha_switch_t rs,
    boolean_t recursive_flg, char *scswitch_node_list, boolean_t verbose_flag,
    char *cluster)
{
	char **resourcename_buff = NULL;
	char **nodename_buff = NULL;

	scha_errmsg_t scha_error = { SCHA_ERR_NOERR, NULL };

	if (scswitch_resource_list != NULL) {
		scha_error = scswitch_dup_list(&resourcename_buff,
		    scswitch_resource_list);
		if (scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;
	}

	/* node list may be empty */
	if (scswitch_node_list != NULL) {
		scha_error = scswitch_dup_list(&nodename_buff,
		    scswitch_node_list);
		if (scha_error.err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}
	}

	/* FBC 1403 changes */
	scha_error = rgm_scswitch_set_resource_switch(
	    (const char **)resourcename_buff, (const scha_switch_t)rs,
	    B_FALSE, recursive_flg, (const char **)nodename_buff,
	    verbose_flag, cluster);

cleanup:
	if (resourcename_buff)
		rgm_free_strarray(resourcename_buff);
	if (nodename_buff)
		rgm_free_strarray(nodename_buff);
	return (scha_error);
}

/*
 * scswitch_set_monitor_switch
 *
 *	Enable or disable resource fault monitoring.
 *	This function sets the value of Monitored_switch of the resource
 *	to suppress or unsupress the invocation of MONITOR_INIT and
 *	MONITOR_START method of the given resource.
 *
 * FBC 1403: Get the nodelist and separate the nodes. Pass it to the rgm
 * function rgm_scswitch_set_resource_switch.
 * Call the rgm_free_strarray for the nodelist.
 *
 * verbose_flag is used by the newcli for the verbose print.
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_set_monitor_switch(char *scswitch_resource_list, scha_switch_t rs,
    char *scswitch_node_list, boolean_t verbose_flag, char *cluster)
{
	char **resourcename_buff = NULL;
	char **nodename_buff = NULL;

	scha_errmsg_t scha_error = { SCHA_ERR_NOERR, NULL };

	if (scswitch_resource_list != NULL) {
		scha_error = scswitch_dup_list(&resourcename_buff,
		    scswitch_resource_list);
		if (scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;
	}

	/* node list may be empty */
	if (scswitch_node_list != NULL) {
		scha_error = scswitch_dup_list(&nodename_buff,
		    scswitch_node_list);
		if (scha_error.err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}
	}
	/* FBC 1403 changes */

	scha_error = rgm_scswitch_set_resource_switch(
	    (const char **)resourcename_buff, (const scha_switch_t)rs,
	    B_TRUE, B_FALSE, (const char **)nodename_buff,
	    verbose_flag, cluster);

cleanup:
	if (resourcename_buff)
		rgm_free_strarray(resourcename_buff);
	if (nodename_buff)
		rgm_free_strarray(nodename_buff);
	return (scha_error);
}


/*
 * scswitch_change_rg_state
 *	Switch specified RGs from unmanaged state to offline state or
 *	from offline state to unmanaged state.
 *
 *	The precondition for switching a managed RG to the UNMANAGED state
 *	is that all Resources contained in the RG must be disabled.
 *
 * verbose_flag is used by the newcli for the verbose print.
 *
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_change_rg_state(char *scswitch_rg_list,
	scha_scswitch_obj_state_t state, boolean_t verbose_flag, char *cluster)
{
	char **rgname_buff = NULL;
	scha_errmsg_t scha_error = { SCHA_ERR_NOERR, NULL };

	if (scswitch_rg_list == NULL || *scswitch_rg_list == '\0') {
		/* empty gopts list */
		scha_error.err_code = SCHA_ERR_INVAL;
		return (scha_error);
	}

	scha_error = scswitch_dup_list(&rgname_buff, scswitch_rg_list);
	if (scha_error.err_code != SCHA_ERR_NOERR)
		goto cleanup;

	scha_error = rgm_scswitch_change_rg_state((const char **)rgname_buff,
	    state, verbose_flag, cluster);

cleanup:
	rgm_free_strarray(rgname_buff);
	return (scha_error);
}


/*
 * scswitch_clear
 *	Clear error condition.
 *	(See the comments for scswitch_error_flag_t.)
 *
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_clear(char *scswitch_node_list, char *scswitch_resource_list,
	scswitch_error_flag_t flag, char *cluster)
{
	char **nodename_buff = NULL;
	char **resourcename_buff = NULL;
	scha_errmsg_t scha_error = { SCHA_ERR_NOERR, NULL };

	if (scswitch_node_list != NULL) {
		scha_error = scswitch_dup_list(&nodename_buff,
		    scswitch_node_list);
		if (scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;
	}

	if (scswitch_resource_list != NULL) {
		scha_error = scswitch_dup_list(&resourcename_buff,
		    scswitch_resource_list);
		if (scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;
	}

	scha_error = rgm_scswitch_clear((const char **)nodename_buff,
	    (const char **)resourcename_buff, flag, cluster);

cleanup:
	rgm_free_strarray(nodename_buff);
	rgm_free_strarray(resourcename_buff);
	return (scha_error);
}

/*
 * scswitch_take_resourcegrp_offline
 *	 Bring the listed RGs offline from all of their current masters.
 *	 Caller guarantees that scswitch_rg_list is non-NULL.
 *
 * scswitch_node_list is the nodelist specified.This should be
 * set when the action is RGACTION_OFFLINE.
 *
 * verbose_flag is used by the newcli for the verbose print.
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_take_resourcegrp_offline(char *scswitch_rg_list,
    char *scswitch_node_list, switch_rg_action_t action, boolean_t verbose_flag,
    char *cluster)
{
	char **nodename_buff = NULL;
	char **rgname_buff = NULL;
	scha_errmsg_t scha_error = { SCHA_ERR_NOERR, NULL };

	if (scswitch_rg_list == NULL) {
		/* should not occur -- caller excluded this case */
		scha_error.err_code = SCHA_ERR_INVAL;
		goto cleanup;
	}

	scha_error = scswitch_dup_list(&rgname_buff, scswitch_rg_list);
	if (scha_error.err_code != SCHA_ERR_NOERR)
		goto cleanup;

	if (action == RGACTION_OFFLINE) {
		scha_error = scswitch_dup_list(&nodename_buff,
		    scswitch_node_list);
		if (scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;
	}
	/* Set node evacuate and rebalance flags to false */
	scha_error = rgm_scswitch_switch_rg((const char **)nodename_buff,
	    (const char **)rgname_buff, action, 0, verbose_flag, cluster);

cleanup:
	rgm_free_strarray(rgname_buff);
	if (action == RGACTION_OFFLINE) {
		rgm_free_strarray(nodename_buff);
	}
	return (scha_error);
}

/*
 * scswitch_dup_list
 *
 * This function allocates memory and converts a comma-separated list into
 * a NULL-terminated string array.
 *
 * Possible return values:
 *	scha_errmsg_t
 *
 */
scha_errmsg_t
scswitch_dup_list(char **list[], char *node_list)
{
	char **temp_listp;
	char *listp, *name;
	int i = 0;
	int  count = 0;
	char *s;
	scha_errmsg_t scha_error = { SCHA_ERR_NOERR, NULL };

	if (!node_list) {
		scha_error.err_code = SCHA_ERR_INVAL;
		return (scha_error);
	}

	/* Get number of ',' in the list */
	for (s = node_list;  *s;  ++s) {
		if (*s == ',')
			++count;
	}

	/* alloc memory for the list */
	if ((*list = temp_listp = (char **)calloc((size_t)count + 2,
	    sizeof (char *))) == NULL) {
		scha_error.err_code = SCHA_ERR_NOMEM;
		return (scha_error);
	}

	/* scan thru the list and store each if_name */
	listp = node_list;
	name = strtok(listp, ",");
	while ((i < (count + 1)) && (name != NULL)) {
		temp_listp[i++] = strdup(name);
		name = strtok(NULL, ",");
	}
	/*
	* Set last element to NULL.
	*/
	temp_listp[i] = NULL;

	return (scha_error);
}


/*
 * scswitch_alloc_service_list
 *
 * This function builds the list scswitch_service_t. Caller is responsible
 * for freeing the memory.
 *
 * Possible return values:
 *      SCSWITCH_NOERR
 *      SCSWITCH_EINVAL
 *      SCSWITCH_ENOMEM
 *
 */
scswitch_errno_t
scswitch_alloc_service_list(
	scswitch_service_t	**slist,
	char		*service_list)
{
char *name;
scswitch_service_t *tmplist;
	if (!service_list)
		return (SCSWITCH_EINVAL);
	tmplist = (scswitch_service_t *)malloc(sizeof (scswitch_service_t));
	if (tmplist == NULL)
		return (SCSWITCH_ENOMEM);
	*slist = tmplist;
	tmplist -> scswitch_service_name = strtok(service_list, ",");

	while ((name = strtok(NULL, ",")) != NULL) {
		tmplist -> next = (scswitch_service_t *)
		    malloc(sizeof (scswitch_service_t));
		if (tmplist -> next == NULL) {
			scswitch_free_service_list(*slist);
			return (SCSWITCH_ENOMEM);
		}
		tmplist = tmplist -> next;
		tmplist -> scswitch_service_name = name;
	}
	tmplist -> next = NULL;
	return (SCSWITCH_NOERR);
}


/*
 * scswitch_free_service_list
 *
 * Possible return values:
 *	NONE
 */
void
scswitch_free_service_list(scswitch_service_t *slist)
{
scswitch_service_t *tmplist;
	for (tmplist = slist; tmplist != NULL; tmplist = slist) {
		slist = slist -> next;
		free(tmplist);
	}
}

/*
 * scswitch_rgmerrmsg
 *
 * Function to obtain the scha error message for the scha error
 * This function is provided as a wrapper function for the rgm
 * function to avoide linking of all the libraries in
 * cluster/src/cmd/scswitch.c. With this scswitch.c needs to be
 * linked only with libscswitch.
 *
 * Returns :
 *	char * : Error Message
 *
 */
/* lint shows a C++ error */
/*lint -save -e1746 */
char *
scswitch_rgmerrmsg(scha_errmsg_t error)
{
	return (rgm_error_msg(error.err_code));
}
/*lint -restore */


/*
 * scswitch_quiesce_resourcegrp
 *
 * Function which calls into the newer idl_switch_primaries to quiecse RGs.
 * If scswitch_rg_list is null, quiesce all RGs.
 * Fast quiesce is only performed if kill_flag is specified. If kill_flag
 * is specified, the running start method of all the resources in the RGs
 * are killed.
 * scswitch_r_list is ignored for now. It is always passed as NULL. It
 * can be used in the future if we provide resouces level selction.
 *
 * verbose_flag is used by the newcli for the verbose print.
 */

scha_errmsg_t
scswitch_quiesce_resourcegrp(char *scswitch_rg_list, char *scswitch_r_list,
    uint_t kill_flg, boolean_t verbose_flag, char *cluster)
{
	char **rgname_buff = NULL;
	char **rname_buff = NULL;
	scha_errmsg_t scha_error = { SCHA_ERR_NOERR, NULL };

	if (scswitch_rg_list) {
		scha_error = scswitch_dup_list(&rgname_buff, scswitch_rg_list);
		if (scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;
	}

	if (scswitch_r_list != 0) {
		scha_error = scswitch_dup_list(&rname_buff, scswitch_r_list);
		if (scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;
	}

	/* Set node Quiesce */
	scha_error = rgm_scswitch_quiesce_rgs((const char **)rgname_buff,
	    (const char **)rname_buff, kill_flg, verbose_flag, cluster);

cleanup:
	rgm_free_strarray(rgname_buff);
	rgm_free_strarray(rname_buff);

	return (scha_error);
}

/*
 * scswitch_suspend_resume_resourcegrp
 *
 * Perform suspend or resume by updating RG's Suspend_automatic_recovery
 * property according the the cmd_flg setting. cmd_flg indicates
 * if it is suspend or resume operation. *scswitch_r_list is always
 * passed as NULL for now. If kflg is set, it suspend the RGs by killing
 * all the running start methods of their resources.
 */
scha_errmsg_t
scswitch_suspend_resume_resourcegrp(char *scswitch_rg_list,
    char *scswitch_r_list, uint_t cmd_flg, uint_t kill_flg, char *cluster)
{
	char **rgname_buff = NULL;
	char **rname_buff = NULL;
	scha_property_t *prop_p = NULL;
	scha_errmsg_t scha_error = { SCHA_ERR_NOERR, NULL };
	scha_errmsg_t tmp_scha_error = { SCHA_ERR_NOERR, NULL };
	char **rg_name;
	rgm_rg_t *rg_cfg = (rgm_rg_t *)0;
	boolean_t only_local_rgs = B_FALSE;

	if (scswitch_rg_list == NULL) {
		/* get all RGs in the system */
		if (sc_nv_zonescheck() != 0) {
			only_local_rgs = B_TRUE;
			scconf_addmessage((char *)RWM_RG_NGZONE,
			    &(scha_error.err_msg));
		}
		tmp_scha_error = scswitch_getrglist(&rgname_buff,
		    only_local_rgs, NULL);
		scha_error.err_code = tmp_scha_error.err_code;
		if (tmp_scha_error.err_msg) {
			scconf_addmessage(tmp_scha_error.err_msg,
			    &(scha_error.err_msg));
			free(tmp_scha_error.err_msg);
		}
		if (scha_error.err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}

		/* No RGs? Just return */
		if (rgname_buff == NULL) {
			return (scha_error);
		}
	} else {
		tmp_scha_error = scswitch_dup_list(&rgname_buff,
		    scswitch_rg_list);
		scha_error.err_code = tmp_scha_error.err_code;
		if (tmp_scha_error.err_msg) {
			scconf_addmessage(tmp_scha_error.err_msg,
			    &(scha_error.err_msg));
			free(tmp_scha_error.err_msg);
		}
		if (scha_error.err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}
	}

	if (scswitch_r_list) {
		tmp_scha_error = scswitch_dup_list(&rname_buff,
		    scswitch_r_list);
		scha_error.err_code = tmp_scha_error.err_code;
		if (tmp_scha_error.err_msg) {
			scconf_addmessage(tmp_scha_error.err_msg,
			    &(scha_error.err_msg));
			free(tmp_scha_error.err_msg);
		}
		if (scha_error.err_code != SCHA_ERR_NOERR) {
			goto cleanup;
		}
	}

	/* Prepare the property */
	prop_p = (scha_property_t *)calloc(2, sizeof (scha_property_t));
	if (prop_p == NULL) {
		scha_error.err_code = SCHA_ERR_NOMEM;
		goto cleanup;
	}
	prop_p->sp_name = strdup("Suspend_automatic_recovery");
	if (!prop_p->sp_name) {
		scha_error.err_code = SCHA_ERR_NOMEM;
		goto cleanup;
	}
	prop_p->sp_value = (namelist_t *)calloc(1, sizeof (namelist_t));
	if (!(prop_p->sp_value)) {
		scha_error.err_code = SCHA_ERR_NOMEM;
		goto cleanup;
	}
	prop_p->sp_value->nl_next = NULL;
	if (cmd_flg == CMD_SUSPEND) {
		prop_p->sp_value->nl_name = strdup("TRUE");
	} else if (cmd_flg == CMD_RESUME) {
		prop_p->sp_value->nl_name = strdup("FALSE");
	} else {
		scha_error.err_code = SCHA_ERR_INTERNAL;
		goto cleanup;
	}
	if (!(prop_p->sp_value->nl_name)) {
		scha_error.err_code = SCHA_ERR_NOMEM;
		goto cleanup;
	}

	/* Go through the RG list to update the property */
	for (rg_name = rgname_buff; *rg_name; rg_name++) {

		/* Get the RG configure */
		tmp_scha_error = rgm_scrgadm_getrgconf(
		    *rg_name, &rg_cfg, cluster);
		if (tmp_scha_error.err_msg) {
			scconf_addmessage(tmp_scha_error.err_msg,
			    &(scha_error.err_msg));
			free(tmp_scha_error.err_msg);
		}
		if (tmp_scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;

		if (cmd_flg == CMD_RESUME) {
			if (!rg_cfg->rg_suspended) {
				continue;
			}
		} else if (rg_cfg->rg_suspended) {
			continue;
		}

		/*
		 * Update the property. There could be some warning messages
		 * here when update the RG property even the
		 * scha_error.err_code is 0, and we want to concatenate them.
		 */
		tmp_scha_error = rgm_scrgadm_update_rg_property(
		    *rg_name, &prop_p, FROM_SCSWITCH, NULL, NULL);
		if (tmp_scha_error.err_msg) {
			scconf_addmessage(tmp_scha_error.err_msg,
			    &(scha_error.err_msg));
			free(tmp_scha_error.err_msg);
		}
		if (tmp_scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;
	}

	/*
	 * Quiece the RG for "suspend". Pass null for all RGs and the
	 * RGM president will generate the list itself. This is
	 * because idl_scswitch_quiesce_rgs() cannot handle unmanaged
	 * RGs.
	 */
	if (cmd_flg == CMD_SUSPEND) {
		tmp_scha_error = rgm_scswitch_quiesce_rgs(
		    (scswitch_rg_list) ? (const char **)rgname_buff : NULL,
		    (const char **)rname_buff, kill_flg, B_FALSE, cluster);
		if (tmp_scha_error.err_msg) {
			scconf_addmessage(tmp_scha_error.err_msg,
			    &(scha_error.err_msg));
			free(tmp_scha_error.err_msg);
		}
		if (tmp_scha_error.err_code != SCHA_ERR_NOERR)
			goto cleanup;
	}

cleanup:
	if (scha_error.err_code == SCHA_ERR_NOERR) {
		scha_error.err_code = tmp_scha_error.err_code;
	}

	rgm_free_strarray(rgname_buff);
	rgm_free_strarray(rname_buff);
	if (prop_p->sp_value)
		rgm_free_nlist(prop_p->sp_value);
	if (prop_p->sp_name)
		free(prop_p->sp_name);
	if (prop_p)
		free(prop_p);
	if (rg_cfg != (rgm_rg_t *)0)
		rgm_free_rg(rg_cfg);

	return (scha_error);
}
