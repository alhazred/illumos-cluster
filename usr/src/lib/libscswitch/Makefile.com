#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.36	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libscswitch/Makefile.com
#

LIBRARYCCC= libscswitch.a
VERS=.1

OBJECTS= \
	libscswitch.o \
	scswitch_misc.o \
	scswitch_cmd.o

# include library definitions
include ../../Makefile.lib

POFILE=		libscswitchmsg.po
MAPFILE=	../common/mapfile-vers
SRCS=		$(OBJECTS:%.o=../common/%.c)

LIBS =		$(DYNLIBCCC)

MTFLAG	=	-mt
CPPFLAGS +=	-I.. -I$(SRC)/common/cl
LINTFLAGS +=	-I..
LINTFILES += 	$(OBJECTS:%.o=%.ln)
PMAP =	-M $(MAPFILE)
LDLIBS += -lrgm -lscconf -lc -lclconf -lscstat
LDLIBS += -lclcomm -lclos -lnsl -lrt -lgen -ldl -lscha -lsczones

.KEEP_STATE:

$(DYNLIB):	$(MAPFILE)

FRC:

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
