/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)nss_cluster.c 1.35     08/10/07 SMI"

/*
 * simple name service backend for application use of cluster
 * interconnect
 */

#include <netdb.h>

#include <string.h>
#include <strings.h>
#include <stddef.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <nss_common.h>
#include <nss_dbdefs.h>
#include <stdio.h>
#include <dlfcn.h>
#include <sys/clconf.h>
#include <sys/cladm_int.h>
#include <sys/clconf_int.h>
#include <sys/cl_ipspace.h>
#include <arpa/inet.h>
#include "nss_cluster.h"

static const struct in_addr *get_cluster_pernode_netmask(void);
static const struct in_addr *get_cluster_pairwise_netmask(void);
static const struct in_addr *get_cluster_physical_netmask(void);
static int is_cluster_addr(const struct in_addr *addr);
static int is_pernode_network(const struct in_addr *addr);
static int is_pernode_netaddr(const struct in_addr *addr);
static int is_pairwise_netaddr(const struct in_addr *addr);
static nss_status_t _nss_cluster_destr(cluster_backend_ptr_t be, void *dummy);
static nss_backend_t *_nss_cluster_constr(cluster_backend_op_t ops[],
    size_t n_ops);
static nss_status_t _nss_cluster_getbyname(cluster_backend_ptr_t be, void *a);
static nss_status_t _nss_cluster_getbyaddr(cluster_backend_ptr_t be, void *a);
static nss_status_t _nss_cluster_getent(cluster_backend_ptr_t be, void *args);
static nss_status_t _nss_cluster_setent(cluster_backend_ptr_t be, void *dummy);
static nss_status_t _nss_cluster_endent(cluster_backend_ptr_t be, void *dummy);
static int ent2result(struct hostent *he, nss_XbyY_args_t *argp);
#if SOL_VERSION >= __s10
static int ent2str(struct hostent *he, nss_XbyY_args_t *argp);
static int netmask2str(const struct in_addr *mask, nss_XbyY_args_t *argp);
#endif
static int cluster_netdb_aliases(char **from_list, char **to_list,
    char **aliaspp, int type, int *count);
static void init_mh(min_hostent_t *ptr);
static nss_status_t _nss_cluster_getnetmask(cluster_backend_ptr_t be,
    void *a);
static int inet_atonet(const char *in, struct in_addr *net);

/*
 * If this NULL valued symbol exists, nscd will treat the nsswitch backend
 * as eligble for being dlopen'd and used per NSS2 specifications.
 * The results from lookups will be cached along with other lookup results
 * as applicable.
 */
void *_nss_cluster_version = NULL;

static cluster_backend_op_t host_ops[] = {
	_nss_cluster_destr,
	_nss_cluster_endent,
	_nss_cluster_setent,
	_nss_cluster_getent,
	_nss_cluster_getbyname,
	_nss_cluster_getbyaddr
};

static cluster_backend_op_t netmask_ops[] = {
	_nss_cluster_destr,
	_nss_cluster_getnetmask
};

/*
 * ROUND_UP and ROUND_DOWN were used, and they're defined in Solaris
 * nss_dbdefs.h.  It generated lint complaint: "Unparenthesized parameter 1".
 * Define our own here.
 */
#define	SC_ROUND_DOWN(n, align)	(((uintptr_t)(n)) & ~((align) - 1l))
#define	SC_ROUND_UP(n, align)	SC_ROUND_DOWN(((uintptr_t)n) + (align) - 1l, \
				(align))

static const struct in_addr *
get_cluster_pernode_netmask()
{
	static struct in_addr foo;

	if (flex_ip_address()) {
		(void) clconf_get_user_netmask_noorb(&foo);
	}
	else
		foo._S_un._S_addr = htonl(LOGICAL_PERNODE_NETWORK_NETMASK);

	return (&foo);
}

static const struct in_addr *
get_cluster_pairwise_netmask()
{
	static struct in_addr foo;

	foo._S_un._S_addr = htonl(LOGICAL_PAIRWISE_NETMASK);

	return (&foo);
}

static const struct in_addr *
get_cluster_physical_netmask()
{
	static struct in_addr foo;

	if (flex_ip_address()) {
		(void) clconf_get_subnet_netmask(&foo);
	}
	else
		foo._S_un._S_addr = htonl(CL_PHYSICAL_NETMASK);

	return (&foo);
}

static int
is_cluster_addr(const struct in_addr *addr)
{
	const struct in_addr *mynet = NULL, *mynetmask = NULL;

	mynet = clconf_get_cluster_network_addr_noorb();
	mynetmask = clconf_get_cluster_network_netmask_noorb();

	if (mynet == NULL || mynetmask == NULL)
		return (0);

	/*
	 * All bit operations are done in network byte order
	 * (which addr is already in).
	 */
	if ((in_addr_t)(addr->_S_un._S_addr & mynetmask->_S_un._S_addr)
		!= (in_addr_t)mynet->_S_un._S_addr) {
			return (0);
	}

	return (1);

}

static int
is_pernode_network(const struct in_addr *addr)
{
	struct in_addr myusernet;
	const struct in_addr *mynet = NULL;

	nodeid_t max = clconf_maximum_nodeid();
	if (max == 0)
		return (0);

	if (flex_ip_address()) {
		(void) clconf_get_user_network_noorb(&myusernet);
		/*
		 * All bit operations are done in network byte order
		 * (which addr is already in).
		 */
		if (addr->_S_un._S_addr != myusernet._S_un._S_addr)
			return (0);
		else
			return (1);
	} else {
		mynet = clconf_get_cluster_network_addr_noorb();
		if (mynet == NULL)
			return (0);
		/*
		 * All bit operations are done in network byte order
		 * (which addr is already in).
		 */
		if (addr->_S_un._S_un_w.s_w1 != mynet->_S_un._S_un_w.s_w1) {
			return (0);
		}
		/*
		 * Verify that this is a logical pernode address
		 */
		return ((addr->_S_un._S_un_b.s_b3 == LOGICAL_PERNODE_B3) &&
				(addr->_S_un._S_un_b.s_b4 == 0));

	}
}

static int
is_pernode_netaddr(const struct in_addr *addr)
{
	const struct in_addr *mynet = NULL;
	struct in_addr myusernet, myusernetmask;
	nodeid_t max = clconf_maximum_nodeid();

	if (max == 0)
		return (0);

	if (flex_ip_address()) {
		(void) clconf_get_user_network_noorb(&myusernet);
		(void) clconf_get_user_netmask_noorb(&myusernetmask);
		/*
		 * All bit operations are done in network byte order
		 * (which addr is already in.
		 */
		if ((in_addr_t)(addr->_S_un._S_addr &
				myusernetmask._S_un._S_addr) !=
				(in_addr_t)myusernet._S_un._S_addr)
			return (0);
		else
			return (1);
	} else {
		mynet = clconf_get_cluster_network_addr_noorb();
		if (mynet == NULL)
			return (0);
		/*
		 * All bit operations are done in network byte order
		 * (which addr is already in).
		 */
		if (addr->_S_un._S_un_w.s_w1 != mynet->_S_un._S_un_w.s_w1) {
			return (0);
		}
		/*
		 * Verify that this is a logical pernode address
		 */
		return (addr->_S_un._S_un_b.s_b3 == LOGICAL_PERNODE_B3);

	}
}

static int
is_pairwise_netaddr(const struct in_addr *addr)
{
	nodeid_t	max		= clconf_maximum_nodeid();
	in_addr_t	nlpm		= htonl(LOGICAL_PAIRWISE_MASK);

	const struct in_addr *mynet = NULL;

	if (max == 0)
		return (0);

	mynet = clconf_get_cluster_network_addr_noorb();
	if (mynet == NULL)
		return (0);

	/*
	 * All bit operations are done in network byte order
	 * (which addr is already in).
	 */
	if (addr->_S_un._S_un_w.s_w1 != mynet->_S_un._S_un_w.s_w1) {
		return (0);
	}

	/*
	 * Verify that this is a logical pairwise address
	 */
	return (((addr->_S_un._S_addr & nlpm) == nlpm) &&
	    (addr->_S_un._S_un_b.s_b3 != LOGICAL_PERNODE_B3));
}

int
is_physical_netaddr(const struct in_addr *addr)
{
	const struct in_addr *mynet = NULL;
	struct in_addr myusernet, myusernetmask;
	in_addr_t nlpm = htonl(LOGICAL_PAIRWISE_MASK);

	if (flex_ip_address()) {
		(void) clconf_get_user_network_noorb(&myusernet);
		(void) clconf_get_user_netmask_noorb(&myusernetmask);

		/*
		 * All bit operations are done in network byte order
		 * (which addr is already in).
		 */
		if ((in_addr_t)(addr->_S_un._S_addr &
			myusernetmask._S_un._S_addr) !=
				(in_addr_t)myusernet._S_un._S_addr)
			return (1);
		else
			return (0);
	} else {
		mynet = clconf_get_cluster_network_addr_noorb();
		if (mynet == NULL)
			return (0);
		/*
		 * All bit operations are done in network byte order
		 * (which addr is already in).
		 */
		if (addr->_S_un._S_un_w.s_w1 != mynet->_S_un._S_un_w.s_w1) {
			return (0);
		}
		/*
		 * Verify that this is a physical address
		 */
		return ((addr->_S_un._S_addr & nlpm) != nlpm);
	}
}

/*
 * Verify if the passed hostname fits the private physical hostname format
 */
int
is_physical_hostname(const char *name)
{
	int	i, n;
	char	*cp;
	char	rname[MAXHOSTNAMELEN];

	if (name == NULL) {
		return (0);
	}

	/* Reverse the name */
	n = (int)strlen(name);
	if (n >= MAXHOSTNAMELEN) {
		return (0);
	}

	for (i = 0; i < n; i++) {
		rname[i] = name[n - i - 1];
	}
	rname[i] = '\0';

	/*
	 * Name in reverse should have the following format:
	 *	A positive integer followed by the physical hostname
	 *	suffix in the reverse.
	 */
	cp = rname;
	while (isdigit(*cp)) {
		cp++;
	}
	if (cp == rname) {
		return (0);
	}
	if (strncmp(cp, PHYSICAL_HOSTNAME_SUFFIX_REVERSED,
	    strlen(PHYSICAL_HOSTNAME_SUFFIX_REVERSED)) != 0) {
		return (0);
	}

	return (1);
}

/*
 * Generates the private physical hostname given the private logical hostname
 * and the adapter id on which the physical hostname is hosted.
 */
char *
get_physical_hostname_from_logical(char *lname, int adapterid)
{
	char *pname;

	if (lname == NULL) {
		return (NULL);
	}
	pname = (char *)malloc(strlen(lname) + 16);
	if (pname != NULL) {
		(void) sprintf(pname, "%s%s%d", lname,
		    PHYSICAL_HOSTNAME_SUFFIX,  adapterid);
	}
	return (pname);
}

int
get_nodeid_by_logical_inaddr(const struct in_addr *addr)
{
	nodeid_t	max		= clconf_maximum_nodeid();
	in_addr_t	dsttag;
	uint_t		srcnode = 0, highnode, lownode;
	const struct in_addr *mynetmask = NULL;

	if (max == 0)
		return (0);


	if (is_pernode_netaddr(addr)) {

		mynetmask = get_cluster_pernode_netmask();
		srcnode = ntohl(addr->_S_un._S_addr) &
			(in_addr_t)(~(ntohl(mynetmask->_S_un._S_addr)));

	} else if (is_pairwise_netaddr(addr)) {

		lownode = (ntohl(addr->s_addr) >> LOGICAL_PAIRWISE_LOW_SHIFT) &
		    ~LOGICAL_PAIRWISE_NODE_MASK;

		highnode = (ntohl(addr->s_addr) >> LOGICAL_PAIRWISE_HIGH_SHIFT)
		    & ~LOGICAL_PAIRWISE_NODE_MASK;

		/* We encode NODEID_MAX as 0 */
		if (highnode == 0)
			highnode = NODEID_MAX;

		/* Sanity check */
		if (highnode <= lownode)
			return (0);

		dsttag = ntohl(addr->s_addr) & ~LOGICAL_PAIRWISE_NETMASK;

		if (dsttag == LOGICAL_PAIRWISE_DST_LOW_TAG)
			srcnode = highnode;
		else if (dsttag == LOGICAL_PAIRWISE_DST_HIGH_TAG)
			srcnode = lownode;
		else
			return (0);
	} else {
		return (0);
	}

	if (srcnode > max)
		return (0);
	else
		return ((int)srcnode);
}

/* Just return the pernode address */
int
get_logical_pernode_inaddr(nodeid_t nodeid, struct in_addr *addr)
{
	return (clconf_get_user_ipaddr_noorb(nodeid, addr));
}

/*
 * Return the pairwise address.
 * This routine is same as the one in src/orb/ipconf.cc
 */
int
get_logical_pairwise_inaddr(nodeid_t src, nodeid_t dst, struct in_addr *addr)
{
	nodeid_t low_id, high_id;
	const struct in_addr *mynet = NULL;

	addr->s_addr = 0;
	if ((src < 1) || (src > NODEID_MAX) || (dst < 1) || (dst > NODEID_MAX))
		return (-1);
	mynet = clconf_get_cluster_network_addr_noorb();
	if (mynet == NULL)
		return (0);
	if (src == dst)
		return (0);

	addr->_S_un._S_un_w.s_w1 = mynet->_S_un._S_un_w.s_w1;
	addr->_S_un._S_un_w.s_w2 = 0;

	/* Convert to host order before doing bit wise operations */
	addr->s_addr = ntohl(addr->s_addr);
	addr->s_addr |= (uint32_t)LOGICAL_PAIRWISE_MASK;

	if (src > dst) {
		addr->s_addr |= (uint32_t)LOGICAL_PAIRWISE_DST_LOW_TAG;
		high_id = src;
		low_id = dst;
	} else {
		addr->s_addr |= (uint32_t)LOGICAL_PAIRWISE_DST_HIGH_TAG;
		high_id = dst;
		low_id = src;
	}

	if (low_id == NODEID_MAX)
		return (-1);

	if (high_id == NODEID_MAX)
		high_id = 0;

	addr->s_addr |= (uint32_t)(high_id << LOGICAL_PAIRWISE_HIGH_SHIFT);
	addr->s_addr |= (uint32_t)(low_id << LOGICAL_PAIRWISE_LOW_SHIFT);

	/* Convert back to network order */
	addr->s_addr = htonl(addr->s_addr);
	return (0);
}

/*
 * Get the list of all of physical private IP addresses associated with a
 * cluster node given the nodeid. The list is returned in an array indexed
 * by adapter ids. Memory for the array is allocated by the routine. The
 * return value if the maximum adapter id. The array allocated is one larger
 * than the maximum adapter id as the 0th entry is left blank.
 * Note that there can be holes in the array in the sense that some adapter
 * ids might be unused and hence might not have any IP addresses associated
 * with them. The unused entries in the array are zero-ed out. A -1 is
 * returned on error.
 */
int
get_physical_inaddrs(nodeid_t nodeid, struct in_addr **addrp)
{
	int	i;
	int	max_adapid;
	struct in_addr	*addrs;

	max_adapid = clconf_get_maximum_adapterid(nodeid);
	if (max_adapid < 0) {
		return (-1);
	}
	if (max_adapid == 0) {
		*addrp = NULL;
		return (0);
	}
	addrs = (struct in_addr *)calloc((size_t)((uint_t)max_adapid + 1),
	    sizeof (struct in_addr));
	if (addrs == NULL) {
		return (-1);
	}
	for (i = 1; i <= max_adapid; i++) {
		if (clconf_get_physical_addr(nodeid, i, &addrs[i]) < 0) {
			bzero(&addrs[i], sizeof (struct in_addr));
		}
	}
	*addrp = addrs;
	return (max_adapid);
}

/*ARGSUSED*/
nss_backend_t *
_nss_cluster_hosts_constr(const char *dummy1, const char *dummy2,
	const char *dummy3)
{
	return (_nss_cluster_constr(host_ops,
		sizeof (host_ops) / sizeof (host_ops[0])));
}



/*ARGSUSED*/
static nss_status_t
_nss_cluster_destr(cluster_backend_ptr_t be, void *dummy)
{
	if (be != 0) {
		free(be);
		/*
		 * anything else needed here?
		 */
	}
	return (NSS_SUCCESS);   /* In case anyone is dumb enough to check */
}

static nss_backend_t *
_nss_cluster_constr(cluster_backend_op_t ops[], size_t n_ops)
{
	cluster_backend_ptr_t	be;

	if ((be = (cluster_backend_ptr_t)malloc(sizeof (*be))) == 0)
		return (0);

	be->ops = ops;
	be->n_ops = (int)n_ops;
	return ((nss_backend_t *)be);
}


/*ARGSUSED*/
static nss_status_t
_nss_cluster_getbyname(cluster_backend_ptr_t be, void *a)
{
	min_hostent_t min_host;

	nss_XbyY_args_t	*argp = (nss_XbyY_args_t *)a;
	int		ret;

	init_mh(&min_host);

	if (!clconf_is_cluster_member()) {
		argp->h_errno = HOST_NOT_FOUND;
	} else {
		if (_gethostbyname(&min_host, &argp->h_errno,
		    argp->key.name) != NULL) {
			if (argp->buf.result != NULL) {
				ret = ent2result(&min_host.mh_he,
					(nss_XbyY_args_t *)a);
				if (ret == NSS_STR_PARSE_SUCCESS) {
					argp->returnval = argp->buf.result;
				} else {
					argp->h_errno = HOST_NOT_FOUND;
					if (ret == NSS_STR_PARSE_ERANGE)
						argp->erange = 1;
				}
#if SOL_VERSION >= __s10
			} else {
				/*
				 * If called by nscd's switch engine,
				 * to support NSS2(Sparks project)
				 * return data in string format by
				 * converting hostent structure to
				 * string data
				 */
				ret = ent2str(&min_host.mh_he,
						(nss_XbyY_args_t *)a);
				if (ret == NSS_STR_PARSE_SUCCESS) {
					argp->returnval = argp->buf.buffer;
				} else {
					argp->h_errno = HOST_NOT_FOUND;
					if (ret == NSS_STR_PARSE_ERANGE)
						argp->erange = 1;
				}
#endif
			}
		}
	}
	return (_herrno2nss(argp->h_errno));
}


/*ARGSUSED*/
static nss_status_t
_nss_cluster_getbyaddr(cluster_backend_ptr_t be, void *a)
{
	min_hostent_t min_host;
	nss_XbyY_args_t	*argp = (nss_XbyY_args_t *)a;
	int		ret;

	init_mh(&min_host);

	if (!clconf_is_cluster_member()) {
		argp->h_errno = HOST_NOT_FOUND;
	} else {
		if (_gethostbyaddr(&min_host, &argp->h_errno,
		    argp->key.hostaddr.addr, argp->key.hostaddr.len,
		    argp->key.hostaddr.type) != NULL) {
			if (argp->buf.result != NULL) {
				ret = ent2result(&min_host.mh_he,
						(nss_XbyY_args_t*)a);
				if (ret == NSS_STR_PARSE_SUCCESS) {
					argp->returnval = argp->buf.result;
				} else {
					argp->h_errno = HOST_NOT_FOUND;
					if (ret == NSS_STR_PARSE_ERANGE)
						argp->erange = 1;
				}
#if SOL_VERSION >= __s10
			} else {
				/*
				 * If called by nscd's switch engine,
				 * to support NSS2(Sparks project)
				 * return data in string format by
				 * converting hostent structure to
				 * string data
				 */
				ret = ent2str(&min_host.mh_he,
						(nss_XbyY_args_t *)a);
				if (ret == NSS_STR_PARSE_SUCCESS) {
					argp->returnval = argp->buf.buffer;
				} else {
					argp->h_errno = HOST_NOT_FOUND;
					if (ret == NSS_STR_PARSE_ERANGE)
						argp->erange = 1;
				}
#endif
			}
		}
	}
	return (_herrno2nss(argp->h_errno));
}


/*ARGSUSED*/
static nss_status_t
_nss_cluster_getent(cluster_backend_ptr_t be, void *args)
{
	return (NSS_UNAVAIL);
}


/*ARGSUSED*/
static nss_status_t
_nss_cluster_setent(cluster_backend_ptr_t be, void *dummy)
{
	return (NSS_UNAVAIL);
}


/*ARGSUSED*/
static nss_status_t
_nss_cluster_endent(cluster_backend_ptr_t be, void *dummy)
{
	return (NSS_UNAVAIL);
}

static int
ent2result(struct hostent *he, nss_XbyY_args_t *argp)
{
	char		*buffer, *limit;
	int		buflen = argp->buf.buflen;
	int		ret, count;
	size_t len;
	struct hostent 	*host;
	struct in_addr	*addrp;

	limit = argp->buf.buffer + buflen;
	host = (struct hostent *)argp->buf.result;
	buffer = argp->buf.buffer;

	/* h_addrtype and h_length */
	host->h_addrtype = AF_INET;
	host->h_length = sizeof (uint_t);

	/* h_name */
	len = strlen(he->h_name) + 1;
	host->h_name = buffer;
	if (host->h_name + len >= limit)
		return (NSS_STR_PARSE_ERANGE);
	(void) memcpy(host->h_name, he->h_name, len);
	buffer += len;

	/* h_addr_list */
	addrp = (struct in_addr *)SC_ROUND_DOWN(limit, sizeof (*addrp));
	host->h_addr_list = (char **)SC_ROUND_UP(buffer, sizeof (char **));
	ret = cluster_netdb_aliases(he->h_addr_list, host->h_addr_list,
		(char **)&addrp, CLUSTER_ADDRLIST, &count);
	if (ret != NSS_STR_PARSE_SUCCESS)
		return (ret);

	/* h_aliases */
	host->h_aliases = host->h_addr_list + count + 1;
	ret = cluster_netdb_aliases(he->h_aliases, host->h_aliases,
		(char **)&addrp, CLUSTER_ALIASES, &count);
	if (ret == NSS_STR_PARSE_PARSE)
		ret = NSS_STR_PARSE_SUCCESS;

	return (ret);

}

#if SOL_VERSION >= __s10
/*
 * To support NSS2(Spraks project) architecture
 * hostent structure needs to converted to string
 * format so the data marshaller in nsswitch
 * str2hostent function can convert the string
 * back to the original hostent format and send
 * it to the application.
 */

static int
ent2str(struct hostent *he, nss_XbyY_args_t *argp)
{
	char		**buffer;
	char		**q;
	char		str[INET_ADDRSTRLEN];
	int		n;
	const char	*res;
	int		l = argp->buf.buflen;
	char		*s = argp->buf.buffer;
	void		*addr;


	for (buffer = he->h_addr_list; *buffer != 0; buffer++) {

		if (buffer != he->h_addr_list) {
			*s = '\n';
			s++;
			l--;
		}

		addr = *buffer;
		res = inet_ntop(AF_INET, addr, str, sizeof (str));
		if (res == NULL)
			return (NSS_STR_PARSE_PARSE);

		if ((n = snprintf(s, l, "%s", res)) >= l)
			return (NSS_STR_PARSE_ERANGE);

		l -= n;
		s += n;
		if (he->h_name != NULL && *he->h_name != '\0') {
			if ((n = snprintf(s, l, " %s", he->h_name)) >= l)
				return (NSS_STR_PARSE_ERANGE);
			l -= n;
			s += n;
		}
		if (buffer == he->h_addr_list) {
			for (q = he->h_aliases; *q != 0; q++) {
				if ((n = snprintf(s, l, " %s", *q)) >= l)
					return (NSS_STR_PARSE_ERANGE);
				l -= n;
				s += n;
			}
		}
	}
	argp->returnlen = s - argp->buf.buffer;
	return (NSS_STR_PARSE_SUCCESS);

}

/*
 * To support NSS2(Sparks).
 */
static int
netmask2str(const struct in_addr *mask, nss_XbyY_args_t *argp)
{
	const char	*res;

	res = inet_ntop(AF_INET, mask, argp->buf.buffer,
			argp->buf.buflen);

	if (res == NULL)
		return (NSS_STR_PARSE_PARSE);

	argp->returnlen = strlen(res);
	argp->returnval = argp->buf.buffer;
	return (NSS_STR_PARSE_SUCCESS);
}
#endif

static int
cluster_netdb_aliases(char **from_list, char **to_list, char **aliaspp,
	int type, int *count)
{
	char	*fstr;
	int	cnt = 0;
	size_t len;

	*count = 0;
	if ((char *)to_list >= *aliaspp)
		return (NSS_STR_PARSE_ERANGE);

	for (fstr = from_list[cnt]; fstr != NULL; fstr = from_list[cnt]) {
		if (type == CLUSTER_ALIASES)
			len = strlen(fstr) + 1;
		else
			len = sizeof (ulong_t);
		*aliaspp -= len;
		to_list[cnt] = *aliaspp;
		if (*aliaspp <= (char *)&to_list[cnt+1])
			return (NSS_STR_PARSE_ERANGE);
		(void) memcpy (*aliaspp, fstr, len);
		++cnt;
	}
	to_list[cnt] = NULL;

	*count = cnt;
	if (cnt == 0)
		return (NSS_STR_PARSE_PARSE);

	return (NSS_STR_PARSE_SUCCESS);
}

static void
init_mh(min_hostent_t *ptr)
{
	ptr->mh_he.h_name = &ptr->mh_namebuff[0];
	ptr->mh_he.h_aliases = &ptr->mh_addrlist[2];
	ptr->mh_he.h_addr_list = &ptr->mh_addrlist[0];

	ptr->mh_he.h_addr_list[0] = (char *)&ptr->mh_addr[0];
	ptr->mh_he.h_addr_list[1] = (char *)&ptr->mh_addr[1];
	ptr->mh_he.h_addr_list[2] = NULL;


	ptr->mh_he.h_addrtype = AF_INET;
	ptr->mh_he.h_length = sizeof (int);
}

/*ARGSUSED*/
static nss_status_t
_nss_cluster_getnetmask(cluster_backend_ptr_t be, void * a)
{
	nss_XbyY_args_t		*argp = (nss_XbyY_args_t *)a;

	struct in_addr net;
	int		ret;

	if (argp->buf.result != NULL) {
		argp->buf.buffer = NULL;
		argp->buf.buflen = 0;
	}

	/*
	 * convert string arg to struct in_addr
	 */

	if (inet_atonet(argp->key.name, &net) != 0)
		return (NSS_NOTFOUND);

	/*
	 * check if it's one of ours
	 */

	if (!is_cluster_addr(&net)) {
		return (NSS_NOTFOUND);
	}
	if (is_pernode_network(&net)) {
		if (argp->buf.result != 0) {
			(void) memcpy(argp->buf.result,
				get_cluster_pernode_netmask(),
				sizeof (struct in_addr));
			argp->returnval = 0;
#if SOL_VERSION >= __s10
		} else {
			/*
			 * To support NSS2.
			 */
			ret = netmask2str(get_cluster_pernode_netmask(),
					argp);
			if (ret != NSS_STR_PARSE_SUCCESS)
				return (NSS_ERROR);
#endif
		}
		return (NSS_SUCCESS);
	} else if (is_pernode_netaddr(&net)) {
		if (argp->buf.result != 0) {
			(void) memcpy(argp->buf.result,
				get_cluster_pernode_netmask(),
				sizeof (struct in_addr));
			argp->returnval = 0;
#if SOL_VERSION >= __s10
		} else {
			/*
			 * To support NSS2.
			 */
			ret = netmask2str(get_cluster_pernode_netmask(),
					argp);
			if (ret != NSS_STR_PARSE_SUCCESS)
				return (NSS_ERROR);
#endif
		}
		return (NSS_SUCCESS);
	} else if (is_pairwise_netaddr(&net)) {
		if (argp->buf.result != 0) {
			(void) memcpy(argp->buf.result,
				get_cluster_pairwise_netmask(),
				sizeof (struct in_addr));
			argp->returnval = 0;
#if SOL_VERSION >= __s10
		} else {
			/*
			 * To support NSS2.
			 */
			ret = netmask2str(get_cluster_pairwise_netmask(),
					argp);
			if (ret != NSS_STR_PARSE_SUCCESS)
				return (NSS_ERROR);
#endif
		}
		return (NSS_SUCCESS);
	} else if (is_physical_netaddr(&net)) {
		if (argp->buf.result != 0) {
			(void) memcpy(argp->buf.result,
				get_cluster_physical_netmask(),
				sizeof (struct in_addr));
			argp->returnval = 0;
#if SOL_VERSION >= __s10
		} else {
			/*
			 * To support NSS2.
			 */
			ret = netmask2str(get_cluster_physical_netmask(),
						argp);
			if (ret != NSS_STR_PARSE_SUCCESS)
				return (NSS_ERROR);
#endif
		}
		return (NSS_SUCCESS);
	} else {
		return (NSS_NOTFOUND);
	}

}

/*ARGSUSED*/
nss_backend_t *
_nss_cluster_netmasks_constr(const char *dummy1, const char *dummy2,
    const char *dummy3)
{
	return (_nss_cluster_constr(netmask_ops,
		sizeof (netmask_ops) / sizeof (netmask_ops[0])));
}


static int
inet_atonet(const char *in, struct in_addr *net)
{
	int i, n;
	uchar_t *up = (uchar_t *)net;

	int data[4];

	for (i = 0; i < (int)(sizeof (data) / sizeof (data[0])); i++)
		data[i] = 0;
	/*
	 * count periods
	 */

	for (i = 0, n = 0; in[i] != '\0'; i++)
		if (in[i] == '.')
			n++;

	switch (n) {
	case 0:
		if (sscanf(in, "%d", data) != 1)
			return (-1);
		break;
	case 1:
		if (sscanf(in, "%d.%d", data, data + 1) != 2)
			return (-1);
		break;
	case 2:
		if (sscanf(in, "%d.%d.%d", data, data + 1, data + 2) != 3)
			return (-1);
		break;
	case 3:
		if (sscanf(in, "%d.%d.%d.%d", data, data + 1, data + 2,
		    data + 3) != 4)
			return (-1);
		break;
	default:
		return (-1);
	}

	for (i = 0; i < 4; i++)
		up [i] = (unsigned char) data[i];

	return (0);
}
