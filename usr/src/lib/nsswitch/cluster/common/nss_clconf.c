/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)nss_clconf.c	1.26	08/07/22 SMI"

/*
 * non-root clconf routines to snarf out cluster info
 * so name service can work.
 */

#include <netdb.h>

#include <strings.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <nss_common.h>
#include <nss_dbdefs.h>
#include <stdio.h>
#include <dlfcn.h>
#include <sys/clconf.h>
#include <sys/cladm_int.h>
#include <sys/clconf_int.h>
#include <arpa/inet.h>
#include <syslog.h>
#include <unistd.h>
#include "nss_cluster.h"
#if SOL_VERSION >= __s10
#include <privip_data.h>
#define	PRIVIP_GET_CCR_RETRY			3
#define	PRIVIP_GET_CCR_ADDITIONAL_ENTRIES	32
#endif

/*
 * 4829869 Need interposition support for _cladm
 */
#ifndef linux
extern int _cladm(int fac, int cmd, void *arg);
#endif

#ifdef DEBUG
/*
 * Maximum arg size passed in / expected out.
 */
#define	MAXARGS 32
#endif


/*
 * Get values for ccr entries
 * This function was moved from clconf to this file because we dont want to
 * link nsswitch with libclconf. Note that this was done because there was
 * no 64-bit version for libclconf. Now that there is a 64-bit version of
 * libclconf and nss_cluster.so.1 links with it indirectly via libscprivip
 * at least for S10 and above, we get a conflict when declaring some of
 * the clconf functions below. To avoid these conflicts, these functions
 * are being renamed to have the _noorb suffix to indicate that they use
 * the cladm interface instead. These include clconf_get_cluster_network_addr,
 * clconf_get_cluster_network_netmask, clconf_get_user_ipaddr,
 * clconf_get_user_network, and clconf_get_user_netmask.
 */

static clconf_errnum_t
clconf_get_ccr_entry_noorb(char *table_name, char *entry_name,
    char *outbuf, size_t outbuf_len)
{
	/*
	 * Executing in user-space
	 */
	int		error;
	get_ccr_entry_t	cladm_arg;

	/*
	 * make sure we're booted as a cluster
	 */
	if (!clconf_is_cluster_member())
		return (CL_NO_CLUSTER);

	/* Build structure to pass to cladm */
	if ((table_name == NULL) || (entry_name == NULL)) {
		return (CL_BADNAME);
	}

	cladm_arg.table_name = table_name;
	cladm_arg.table_name_len = strlen(table_name) + 1;
	cladm_arg.entry_name = entry_name;
	cladm_arg.entry_name_len = strlen(entry_name) + 1;
	cladm_arg.buf = outbuf;
	cladm_arg.buf_len = outbuf_len;

	error = nss_cladm(CL_CONFIG, CL_GET_CCR_ENTRY, &cladm_arg);
	if (error) {
		return (CL_NO_CLUSTER);
	}

	return (cladm_arg.rslt);
}


/*
 * get bootflags from kernel just once
 */
static mutex_t bootflags_lock;
static int
getbootflags()
{
	static int bootflags = 0, ready = 0;

	(void) mutex_lock(&bootflags_lock);
	if (!ready) { /* check in case of race */
		if (nss_cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags)
		    != 0) {
			bootflags = 0;
		}
		else
			ready = 1;
	}
	(void) mutex_unlock(&bootflags_lock);
	return (bootflags);
}

/*
 * return my nodeid
 */
static mutex_t nodeid_lock;
nodeid_t
clconf_get_nodeid()
{
	static nodeid_t current_nodeid = 0;

	(void) mutex_lock(&nodeid_lock);
	if (current_nodeid == 0) {
		if (nss_cladm(CL_CONFIG, CL_NODEID, &current_nodeid) != 0) {
			current_nodeid = 0;
			(void) mutex_unlock(&nodeid_lock);
			return (0);
		}
	}

	(void) mutex_unlock(&nodeid_lock);
	return (current_nodeid);
}

/*
 * return highest nodeid supported by kernel
 */
static mutex_t max_nodeid_lock;
nodeid_t
clconf_maximum_nodeid()
{
	static nodeid_t nid = 0;

	if (!clconf_is_cluster_member())
		return (0);

	(void) mutex_lock(&max_nodeid_lock);
	if (nid == 0) {
		if (nss_cladm(CL_CONFIG, CL_HIGHEST_NODEID, &nid) != 0) {
			nid = 0;
			(void) mutex_unlock(&max_nodeid_lock);
			return (0);
		}
	}

	(void) mutex_unlock(&max_nodeid_lock);
	return (nid);
}

/*
 * return 1 if member of cluster, else 0
 */

int
clconf_is_cluster_member()
{
	return ((getbootflags() & CLUSTER_BOOTED) != 0);
}

int
flex_ip_address(void)
{
	char buffer[16];

	if (clconf_get_ccr_entry_noorb("infrastructure",
	    CCRENT_PRIVATE_SUBNET_NETMASK, buffer, sizeof (buffer))
	    == CL_NOERROR)
		return (1);
	return (0);
}

static mutex_t network_netmask_lock;
const struct in_addr *
clconf_get_cluster_network_netmask_noorb()
{
	static struct in_addr foo;
	static uint8_t ready = 0;
	in_addr_t net, lna;
	char ccrent_private_netmask[16];

	(void) mutex_lock(&network_netmask_lock);
	if (ready == 0) /* first time ? */ {

		/*
		 * initialise in_addr
		 */
		(void) bzero(&foo, sizeof (foo));

		/*
		 * read this info from the ccr
		 */
		if (clconf_get_network_netmask(ccrent_private_netmask,
		    sizeof (ccrent_private_netmask)) != 0) {

			/*
			 * Couldn't read CCR, perhaps we haven't joined
			 * cluster yet. Set ready to 0 so that subsequent
			 * calls will trigger another CCR read instead of
			 * returning the default from 'foo'.
			 */
			ready = 0;
			(void) mutex_unlock(&network_netmask_lock);
			return (NULL);

		} else {
			/*
			 * Use the ccr value read
			 * net and lna should be in host byte order
			 */
			net = inet_network(ccrent_private_netmask);
			lna = ntohl(inet_addr(ccrent_private_netmask));
			if ((net == (in_addr_t)-1) ||
			    (lna == (in_addr_t)-1)) {
				/*
				 * Error in the value read from the ccr
				 * The node will die soon, so try to get a
				 * syslog entry out.
				 */
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_ALERT, "ccr infrastructure entry %s"
				    " is not a valid ip address: %s",
				    CCRENT_PRIVATE_NETMASK,
				    ccrent_private_netmask);
				(void) mutex_unlock(&network_netmask_lock);
				return (NULL);
			} else {
				foo = inet_makeaddr(net, lna);
				ready = foo._S_un._S_un_b.s_b1;
			}
		}
	}
	(void) mutex_unlock(&network_netmask_lock);

	return (&foo);
}

static mutex_t network_addr_lock;
const struct in_addr *
clconf_get_cluster_network_addr_noorb()
{
	static struct in_addr foo;
	static uint8_t ready = 0;
	in_addr_t net, lna;
	char ccrent_private_net_number[16];

	(void) mutex_lock(&network_addr_lock);
	if (ready == 0) /* first time ? */ {

		/*
		 * initialise in_addr
		 */
		(void) bzero(&foo, sizeof (foo));

		/*
		 * read this info from the ccr
		 */
		if (clconf_get_network_addr(ccrent_private_net_number,
		    sizeof (ccrent_private_net_number)) != 0) {

			/*
			 * Couldn't read CCR, perhaps we haven't joined
			 * cluster yet. Set ready to 0 so that subsequent
			 * calls will trigger another CCR read instead of
			 * returning the default from 'foo'.
			 */
			ready = 0;
			(void) mutex_unlock(&network_addr_lock);
			return (NULL);

		} else {
			/*
			 * Use the ccr value read
			 * net and lna should be in host byte order
			 */
			net = inet_network(ccrent_private_net_number);
			lna = ntohl(inet_addr(ccrent_private_net_number));
			if ((net == (in_addr_t)-1) ||
			    (lna == (in_addr_t)-1)) {
				/*
				 * Error in the value read from the ccr
				 * The node will die soon, so try to get a
				 * syslog entry out.
				 */
				syslog(LOG_ALERT, "ccr infrastructure entry %s"
				    " is not a valid ip address: %s",
				    CCRENT_PRIVATE_NET_NUMBER,
				    ccrent_private_net_number);
				(void) mutex_unlock(&network_addr_lock);
				return (NULL);
			} else {
				foo = inet_makeaddr(net, lna);
				ready = foo._S_un._S_un_b.s_b1;
			}
		}
	}
	(void) mutex_unlock(&network_addr_lock);

	return (&foo);
}

int
clconf_get_ccr_generation(char *table)
{
	char outbuf[80];
	int result;

	result = clconf_get_ccr_entry_noorb(table, "ccr_gennum", outbuf,
	    sizeof (outbuf));

	switch (result) {
	case CL_NOERROR:
		return (atoi(outbuf));
	default:
		return (-1);
	}
}

int
clconf_get_network_netmask(char *buffer, size_t len)
{
	if (clconf_get_ccr_entry_noorb("infrastructure",
	    CCRENT_PRIVATE_NETMASK, buffer, len) != CL_NOERROR)
		return (-1);
	return (0);
}

int
clconf_get_network_addr(char *buffer, size_t len)
{
	if (clconf_get_ccr_entry_noorb("infrastructure",
	    CCRENT_PRIVATE_NET_NUMBER, buffer, len) != CL_NOERROR)
		return (-1);
	return (0);
}

int
clconf_get_private_nodename_from_ccr(nodeid_t nid, char *buf, size_t buflen)
{
	char qbuf[255];

	(void) sprintf(qbuf, "cluster.nodes.%d.properties.private_hostname",
		nid);

	if (clconf_get_ccr_entry_noorb("infrastructure", qbuf, buf, buflen)
	    != CL_NOERROR)
		return (-1);
	return (0);
}

/*
 * Get the logical IP address for the specified node.
 */
int
clconf_get_user_ipaddr_noorb(nodeid_t nodeid, struct in_addr *addr)
{
	char qbuf[255];
	char netnumber[16];
	in_addr_t net;
	const struct in_addr *mynet = NULL;

	if (nodeid < 1 || nodeid > NODEID_MAX)
		return (-1);

	if (flex_ip_address()) {
		(void) sprintf(qbuf,
			"cluster.properties.private_user_net_number");
		if (clconf_get_ccr_entry_noorb("infrastructure",
			qbuf, netnumber, sizeof (netnumber)) != CL_NOERROR)
			return (-1);

		net = inet_network(netnumber);
		if (net == (in_addr_t)-1) {
			return (-1);
		}
		*addr = inet_makeaddr(net, nodeid);

	} else {
		mynet = clconf_get_cluster_network_addr_noorb();

		if (mynet == NULL)
			return (-1);
		addr->_S_un._S_un_w.s_w1 = mynet->_S_un._S_un_w.s_w1;
		addr->_S_un._S_un_b.s_b3 = LOGICAL_PERNODE_B3;
		addr->_S_un._S_un_b.s_b4 = (uint8_t)nodeid;
	}

	return (0);
}

/*
 * The following function is called from functions in nss_cluster.c
 * only when flex-ip addressing exists. Hence here we can simply
 * return the user net number from the infrastructure file
 */
int
clconf_get_user_network_noorb(struct in_addr *addr)
{
	char qbuf[255];
	char netnumber[16];
	in_addr_t net, lna;

	(void) sprintf(qbuf, "cluster.properties.private_user_net_number");

	if (clconf_get_ccr_entry_noorb("infrastructure", qbuf, netnumber,
		sizeof (netnumber)) != CL_NOERROR)
		return (-1);

	net = inet_network(netnumber);
	lna = ntohl(inet_addr(netnumber));
	if (net == (in_addr_t)-1 || lna == (in_addr_t)-1) {
		return (-1);
	}
	*addr = inet_makeaddr(net, lna);
	return (0);
}

/*
 * The following function is called from functions in nss_cluster.c
 * only when flex-ip addressing exists. Hence here we can simply
 * return the user netmask from the infrastructure file
 */
int
clconf_get_user_netmask_noorb(struct in_addr *addr)
{
	char qbuf[255];
	char netmask[16];
	in_addr_t net, lna;

	(void) sprintf(qbuf, "cluster.properties.private_user_netmask");

	if (clconf_get_ccr_entry_noorb("infrastructure", qbuf, netmask,
		sizeof (netmask)) != CL_NOERROR)
		return (-1);

	net = inet_network(netmask);
	lna = ntohl(inet_addr(netmask));
	if (net == (in_addr_t)-1 || lna == (in_addr_t)-1) {
		return (-1);
	}
	*addr = inet_makeaddr(net, lna);
	return (0);
}

/*
 * The following function is called from functions in nss_cluster.c
 * only when flex-ip addressing exists. Hence here we can simply
 * return the subnet netmask from the infrastructure file
 */
int
clconf_get_subnet_netmask(struct in_addr *addr)
{
	char qbuf[255];
	char netmask[16];
	in_addr_t net, lna;

	(void) sprintf(qbuf, "cluster.properties.private_subnet_netmask");

	if (clconf_get_ccr_entry_noorb("infrastructure", qbuf, netmask,
		sizeof (netmask)) != CL_NOERROR)
		return (-1);

	net = inet_network(netmask);
	lna = ntohl(inet_addr(netmask));
	if (net == (in_addr_t)-1 || lna == (in_addr_t)-1) {
		return (-1);
	}
	*addr = inet_makeaddr(net, lna);
	return (0);
}

int
clconf_get_maximum_adapterid(nodeid_t nodeid)
{
	int	error;
	get_clconf_child_stats_t cladm_arg;

	/* make sure we're booted as a cluster */
	if (!clconf_is_cluster_member()) {
		return (0);
	}

	/* Sanity check before diving into the kernel */
	if (nodeid < 1 || nodeid > NODEID_MAX) {
		return (0);
	}

	/* Make the system call */
	cladm_arg.path_len = 1;
	cladm_arg.path_node_types[0] = CL_NODE;
	cladm_arg.path_node_indices[0] = (int)nodeid;
	cladm_arg.child_type = CL_ADAPTER;
	error = nss_cladm(CL_CONFIG, CL_GET_CLCONF_CHILD_STATS, &cladm_arg);

	if (error || cladm_arg.rslt != CL_NOERROR) {
		return (0);
	}

	return (cladm_arg.max_child_index);
}

int
get_version(uint16_t *majp, uint16_t *minp, nodeid_t nodeid)
{
	int				error = 0;
	get_clconf_proto_vers_num_t	cladm_arg;
	int				try = 0;

	/* make sure we're booted as a cluster */
	if (!clconf_is_cluster_member()) {
		return (-1);
	}

	/* Sanity check before diving into the kernel */
	if (nodeid < 1 || nodeid > NODEID_MAX) {
		return (-1);
	}

	/* Make the system call */
	(void) strncpy(cladm_arg.vp_name, "ipconf", (strlen("ipconf") + 1));
	cladm_arg.mode = (uint16_t)VM_NODEPAIR;
	cladm_arg.nodeid = nodeid;

	while (try++ < 3) {	/* try 3 times if we get EAGAIN error */
		error = nss_cladm(CL_CONFIG, CL_GET_VP_RUNNING_VERSION,
		    &cladm_arg);
		/*
		 * If we were not able to get the version number then we try a
		 * few times if the error was EAGAIN.
		 */
		if (error) {
			if (errno == EAGAIN) { /* lint !e746 */
				(void) sleep(5);
				continue;
			} else
				return (-1);
		} else {
			*majp =	cladm_arg.maj_num;
			*minp =	cladm_arg.min_num;
			return (0);
		}
	}
	return (-1);	/* error if we got here */
}

/*
 * Extracts the physical IP address assigned to an private interconnect
 * adapter identified by the nodeid and the adapterid. The IP address
 * information is read out from the infrastructure table.
 *
 * Returns 0 on success, -1 otherwise.
 */
int
clconf_get_physical_addr(nodeid_t nodeid, int adapid, struct in_addr *addrp)
{
	char qbuf[512];
	char physical_addr[16];
	in_addr_t net, lna;

	(void) sprintf(qbuf,
		"cluster.nodes.%d.adapters.%d.properties.ip_address",
		nodeid, adapid);

	if (clconf_get_ccr_entry_noorb("infrastructure", qbuf,
		physical_addr, sizeof (physical_addr)) != 0) {
		return (-1);
	}
	net = inet_network(physical_addr);
	lna = ntohl(inet_addr(physical_addr));
	if (net == (in_addr_t)-1 || lna == (in_addr_t)-1) {
		return (-1);
	}
	*addrp = inet_makeaddr(net, lna);
	return (0);
}


#if SOL_VERSION >= __s10

struct nss_privip **
scprivip_get_all_mappings_noorb(uint_t *cnt, void **data)
{
	struct nss_privip **nss_table = NULL;
	size_t size = 0;
	get_ccr_privip_t ccr_privip;
	int error;
	struct nss_privip *nss_data = NULL;
	uint_t i;
	uint_t retry = 0;

	/*
	 * make sure we're booted as a cluster
	 */
	if (!clconf_is_cluster_member()) {
		return (NULL);
	}

	ccr_privip.count = 0;
retry_get_privip_ccr:
	*cnt = 0;
	if (*data != NULL) {
		free(*data);
		*data = NULL;
	}
	if (++retry > PRIVIP_GET_CCR_RETRY) {
		/* Never loop for ever, give up */
		return (NULL);
	}
	size = (size_t)(ccr_privip.count +
	    PRIVIP_GET_CCR_ADDITIONAL_ENTRIES) * sizeof (struct nss_privip);
	*data = malloc(size);
	if (*data == NULL) {
		/* No memory, give up */
		return (NULL);
	}
	ccr_privip.buf = *data;
	ccr_privip.buf_len = size;
	ccr_privip.count = 0;
	ccr_privip.rslt = CL_NOERROR;
	error = nss_cladm(CL_CONFIG, CL_GET_CCR_PRIVIP, &ccr_privip);
	if (error) {
		/* Not expected, retry */
		goto retry_get_privip_ccr; /* lint !e801 */
	}
	if (ccr_privip.rslt != CL_NOERROR) {
		/* Not expected, retry */
		goto retry_get_privip_ccr; /* lint !e801 */
	}
	if (ccr_privip.count == 0) {
		/* no entries in the privip_ccr table, done */
		if (*data != NULL) {
			free(*data);
			*data = NULL;
		}
		return (NULL);
	}
	nss_table = (struct nss_privip **)calloc((size_t)ccr_privip.count,
	    sizeof (struct nss_privip *));
	if (nss_table == NULL) {
		/* No memory, give up */
		if (*data != NULL) {
			free(*data);
			*data = NULL;
		}
		return (NULL);
	}
	nss_data = (struct nss_privip *)*data;
	for (i = 0; i < ccr_privip.count; i++) {
		nss_table[i] = &(nss_data[i]);
		nss_table[i]->priv_ipaddr =
		    inet_makeaddr(nss_table[i]->inaddr, 0);
	}
	*cnt = ccr_privip.count;
	return (nss_table);
}

/*
 * Given a zone cluster name, retrieves the subnet assigned to this zone
 * cluster from the CCR file.
 */
clconf_errnum_t
clconf_get_zone_subnet_from_ccr(char *zone_cluster, char *outbuf)
{
	cz_subnet_t	cznet_arg;
	clconf_errnum_t		error;

	(void) strcpy(cznet_arg.name, zone_cluster);

	error = nss_cladm(CL_CONFIG, CL_GET_CZ_SUBNET, &cznet_arg);
	if (error) {
		return (CL_NO_CLUSTER);
	}

	(void) strcpy(outbuf, cznet_arg.sub_net);
	return (0);
}

/*
 * Returns the cluster id for the given cluster name.
 */
int
clconf_get_cluster_id_noorb(char *zone_cluster, uint_t *cl_id)
{
	cz_id_t		cz_id;
	int		error;

	strcpy(cz_id.name, zone_cluster);
	error = nss_cladm(CL_CONFIG, CL_GET_ZC_ID, &cz_id);
	if (error != 0) {
		error = errno;
		*cl_id = cz_id.clid;
		return (error);
	}
	*cl_id = cz_id.clid;
	return (0);
}

#endif

/*
 * nss_cladm is a 'C' version of cladm_impl().
 * It was created to remove the dependency on libclos which is a C++ lirbary.
 * By linking with a C++ library it means that libCstd is used, then any
 * application linked with stlport4 which is run on a cluster will crash as
 * stlport4 is not compatible with libCstd.
 */
int
nss_cladm(int fac, int cmd, void *arg)
{
#ifdef linux
	return (-1);
#else
#ifdef DEBUG
	char *unode_dir = getenv("UNODE_DIR");
	char *cluster_name = getenv("CLUSTER_NAME");
	char *node_id = getenv("NODE_ID");
	char unode_path[MAXPATHLEN];
	int unode_gateway_fd;
	int error = 0;
	int args[MAXARGS];
	int *argp = args;
	int *facp = &args[0];
	int *cmdp = &args[1];

	if (node_id == NULL)
#endif
		return (_cladm(fac, cmd, arg));
#ifdef DEBUG
	*facp = fac;
	*cmdp = cmd;

	/*
	 * Copy input argument into local storage so we can prepend
	 * the facility and command in the door invocation.
	 */
	if (arg) {
		bcopy(arg, (void *)&args[2],
		    sizeof (args) - (2 * sizeof (int)));
	}

	/*
	 * We default to /tmp/{cluster}/node-{nodeid}
	 */
	sprintf(unode_path, "%s/%s/node-%s/cladm",
	    (unode_dir) ? unode_dir : "/tmp",
	    (cluster_name) ? cluster_name : "ucluster",
	    node_id);

	/*
	 * Open up the cladm door
	 */
	if ((unode_gateway_fd = open(unode_path, O_RDONLY)) < 0) {
		error = errno;
	} else {

		door_arg_t params;
		/*
		 * XXX A few cladm calls expect that we're in the kernel
		 * XXX and can simply copyin/copyout information.  This
		 * XXX should be changed, but in the mean time we simply
		 * XXX rewrap the arguments and send the strings out.
		 */
		struct name_buf {
			int fac;
			int cmd;
			int nodeid;
			int len;
			char name[MAXPATHLEN];
		} name_buf, *name_bufp;

		params.desc_ptr = NULL;
		params.desc_num = 0;

		/*
		 * XXX Currently CL_GET_NODE_NAME is the only command which
		 * we call that requires special treatment.  Getting the
		 * cluster name has the same problem and needs to be fixed
		 * as well.
		 */
		if (fac == CL_CONFIG && cmd == CL_GET_NODE_NAME) {
			clnode_name_t *in_args = (clnode_name_t *)arg;

			/*
			 * Fill in facility and command parameters
			 */
			name_buf.fac = fac;
			name_buf.cmd = cmd;

			/*
			 * Populate nodeid/len/string
			 */
			name_buf.nodeid = in_args->nodeid;
			name_buf.len = MAXPATHLEN;
			bcopy((void *)in_args->name, (void *)name_buf.name,
			    strlen(in_args->name) + 1);

			/*
			 * Populate params structure
			 */
			params.data_ptr = (char *)&name_buf;
			params.data_size = sizeof (name_buf);
			params.rbuf = (char *)&name_buf;
			params.rsize = sizeof (name_buf);
		} else {
			/*
			 * Use standard arguments array
			 */
			params.data_ptr = (char *)&args;
			params.data_size = sizeof (args);
			params.rbuf = (char *)&args;
			params.rsize = sizeof (args);
		}

		/*
		 * Send the cladm command to unode
		 */
		if (door_call(unode_gateway_fd, &params) == -1) {
			error = errno;
			close(unode_gateway_fd);
			goto out;
		}

		/*
		 * Close the unode gateway file descriptor
		 */
		if (close(unode_gateway_fd) < 0) {
			error = errno;
			goto out;
		}

		/*
		 * Extract error/exit return valued and results
		 */
		facp = &((int *)(params.data_ptr))[0];
		cmdp = &((int *)(params.data_ptr))[1];
		argp = &((int *)(params.data_ptr))[2];

		/*
		 * errno is passed back in storage for facility.
		 * exit code is in command, but effective is returned as -1
		 */
		if (*cmdp != 0) {
			error = *facp;
		} else {
			/*
			 * Door handle passed back in CL_GET_NS_DATA
			 * XXX Need to fix this to handle CL_GET_LOCAL_NS
			 * XXX Need to generally fix CL_GET_NODE_NAME as well
			 */
			if (fac == CL_CONFIG && cmd == CL_GET_NS_DATA) {
				cllocal_ns_data_t *ns_data =
				    (cllocal_ns_data_t *)arg;
				bcopy(&((int *)(params.data_ptr))[2],
				    ns_data->tid, sizeof (ns_data->tid));
				if (params.desc_num == 1) {
					door_info_t info;
					if (door_info(params.desc_ptr->
					    d_data.d_desc.d_descriptor,
					    &info) < 0) {
						error = errno;
						goto out;
					}
					ns_data->filedesc =
					    params.desc_ptr->
					    d_data.d_desc.d_descriptor;
				}
			} else if (fac == CL_CONFIG &&
			    cmd == CL_GET_NODE_NAME) {
				clnode_name_t *out_args = (clnode_name_t *)arg;
				name_bufp = (struct name_buf *)facp;
				out_args->nodeid = name_bufp->nodeid;
				if (name_bufp->len < out_args->len)
					out_args->len = name_bufp->len;
				bcopy((void *)&name_bufp->name,
				    (void *)out_args->name, out_args->len);
			} else {
				bcopy((void *)argp, arg,
				    params.data_size - (2 * sizeof (int)));
			}
		}
	}

out:
	if (error) {
		errno = error;
		return (-1);
	} else {
		return (0);
	}
#endif /* DEBUG */
#endif /* linux */
}
