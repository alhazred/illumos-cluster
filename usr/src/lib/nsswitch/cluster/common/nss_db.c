/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)nss_db.c	1.25	08/09/01 SMI"

/*
 * routines to manage ip addresses safely
 */

#include <netdb.h>

#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <nss_common.h>
#include <nss_dbdefs.h>
#include <dlfcn.h>
#include <sys/clconf.h>
#include <sys/cladm_int.h>
#include <sys/clconf_int.h>
#include <sys/sol_version.h>
#include <arpa/inet.h>
#if SOL_VERSION >= __s10
#include <privip_map.h>
#include <privip_data.h>
#include <zone.h>
#include <sys/vc_int.h>
#endif
#include "nss_cluster.h"

/*
 * The cluster nsswitch library maintains one cl_db_t structure for each node
 * in the cluster.
 */
typedef struct cl_db {
	/*
	 * The private logical host name, specified in the infrastructure file.
	 * -physicalN suffix is added to this name to derive the private
	 * physical hostname for the IP hosted on the adapter with id N on this
	 * host.
	 */
	char 		*priv_hostname;

	/* The logical pernode IP address */
	struct in_addr	logical_pernode_ipaddr;

	/* The logical pairwise address the local node uses for this node */
	struct in_addr	logical_pairwise_ipaddr;

	/*
	 * The maximum adapter id associated with any private interconnect
	 * adapter at this node in the infrastructure table. This number
	 * indicates the size of the following two arrays.
	 */
	int		maximum_adapterid;

	/*
	 * Array of private physical IP addresses assigned to adapters on this
	 * node in the infrastructure table. The array is indexed by adapter
	 * ids. The size of the array is maximum_adapterid+1. The entries for
	 * unused adapter ids are zero-ed out.
	 */
	struct in_addr	*physical_ipaddrs;

	/*
	 * Array of private physical hostnames. Indexed by adapter ids. The
	 * physical hostnames are derived from the logical hostname.
	 */
	char		**physical_hostnames;
} cl_db_t;

/* Mutex to protect the data structure. */
static mutex_t	db_lock;

/* Total number of cl_db_t entries in the database. */
static int	db_count	= 0;

/* The array of cl_db_t entries. */
static cl_db_t *db_table	= NULL;

/*
 * The CCR generation number the current incore database corresponds to. Used
 * to determine when the CCR should be reread for the infrastructure table.
 */
static int	db_generation	= 0;

/*
 * Whether the current incore database has information about physical private
 * IPs. For performance reasons the physical IP information is read in only
 * when needed.
 */
static int	physical_ready	= 0;

/* XXX - Workaround to temporarly disable version manager code */
int		cl_nss_get_version = 0;

#if SOL_VERSION >= __s10

/* Mutex to protect the private IP address data structure. */
static mutex_t	privip_lock;

/* Total number of cl_db_t entries in the private IP address database. */
static uint_t	privip_count	= 0;

/* The array of private IP entries. */
static struct nss_privip **privip_table	= NULL;
static void *privip_data = NULL;

/*
 * The CCR generation number the current incore database corresponds to. Used
 * to determine when the privip_ccr table should be reread
 */
static int	privip_gennum	= 0;

static struct nss_privip *zc_table	= NULL;

/* Mutex to protect the private IP address data structure. */
static mutex_t	zc_privip_lock;

/*
 * The CCR generation number the current incore database corresponds to. Used
 * to determine when the CCR should be reread for the infrastructure table.
 */
static int	zc_generation	= 0;
static int	zc_count	= 0;

#endif

/*
 * Destroy the incore database.
 */
static void
destroy_table()
{
	int i;
	if (db_table) {
		for (i = 0; i <= db_count; i++) {
			if (db_table[i].priv_hostname != NULL) {
				free(db_table[i].priv_hostname);
			}
			if (db_table[i].physical_ipaddrs != NULL) {
				free(db_table[i].physical_ipaddrs);
			}
		}
		free(db_table);
		db_table	= NULL;
		db_generation	= 0;
	}
}

#if SOL_VERSION >= __s10

/*
 * Destroy the zone cluster privip info
 */
static void
destroy_zone_cluster_table()
{
	int i;

	if (zc_table) {
		free(zc_table);
		zc_table = NULL;
		zc_generation   = 0;
	}
}

int
update_zone_cluster_db(char *zone_cluster)
{
	int success = 0;
	in_addr_t ip_addr;
	char    qbuf[512];
	char  sub_net[INET6_ADDRSTRLEN];

	if (zc_table != NULL &&
	    zc_generation == clconf_get_ccr_generation("infrastructure")) {
		return (0);
	}

	while (!success) {
		int j;
		/*
		 * remove existing db
		 */
		destroy_zone_cluster_table();


		/* Suppress lint:"suspicious casting" from this point */
		/* lint -e571 */
		if (((zc_generation =
		    clconf_get_ccr_generation("infrastructure")) < 0) ||
		    ((zc_count = (int)clconf_maximum_nodeid()) < 1) ||
		    ((zc_table = (struct nss_privip *)
		    calloc((size_t)zc_count + 1,
		    sizeof (struct nss_privip))) == NULL)) {
			return (-1);
		}

		if (clconf_get_zone_subnet_from_ccr(zone_cluster, sub_net)
		    != 0) {
			return (-1);
		}

		for (j = 1; j <= zc_count; j++) {
			/* Read in hostname */
			if (clconf_get_private_nodename_from_ccr((nodeid_t)j,
				qbuf, sizeof (qbuf)) != 0) {
				continue;
			}
			strcpy(zc_table[j].priv_hostname, strdup(qbuf));

			/*
			 * Get the private IP address for these hostnames.
			 * These are derived from the subnet assigned
			 * for this zone cluster and nodeid.
			 * The subnet value for this zone cluster is stored
			 * in the global zone ccr file cz_network_ccr.
			 */
			ip_addr = ntohl((inet_addr(sub_net))) | (nodeid_t)j;
			if (ip_addr != (in_addr_t)(-1)) {
				zc_table[j].priv_ipaddr =
				    inet_makeaddr(ip_addr, 0);
			}
		}
		if (zc_generation == clconf_get_ccr_generation(
		    "infrastructure")) {
			success = 1;
		}
	}
	return (0);
}
#endif

/*
 * Synchronize the incore database with the underlying CCR infrastructure table.
 * The whole table is read only if it has been updated since the last successful
 * read. The argument indicates whether information about the physical private
 * hostnames is needed in the database. Since reading this information is
 * expensive it is read in only when needed.
 */
int
update_db(int physical_needed)
{
	int success = 0;
	nodeid_t lndid;

	if (db_table != NULL &&
	    db_generation == clconf_get_ccr_generation("infrastructure") &&
	    (!physical_needed || physical_ready)) {
		return (0);
	}

	while (! success) {
		int j;

		/*
		 * remove existing db
		 */
		destroy_table();

		/* Suppress lint:"suspicious casting" from this point */
		/* lint -e571 */
		if (((db_generation =
		    clconf_get_ccr_generation("infrastructure")) < 0) ||
		    ((db_count = (int)clconf_maximum_nodeid()) < 1) ||
		    ((db_table = (cl_db_t *)calloc((size_t)db_count + 1,
		    sizeof (cl_db_t))) == NULL)) {
			return (-1);
		}

		lndid = clconf_get_nodeid();
		for (j = 1; j <= db_count; j++) {
			int	k;
			char	qbuf[MAXHOSTNAMELEN];

			/* Read in logical hostname */
			if (clconf_get_private_nodename_from_ccr((nodeid_t)j,
			    qbuf, sizeof (qbuf)) != 0) {
				continue;
			}
			if ((db_table[j].priv_hostname = strdup(qbuf))
			    == NULL) {
				/* no memory */
				destroy_table();
				return (-1);
			}

			/* Compute logical pernode address */
			if (get_logical_pernode_inaddr((nodeid_t)j,
			    &db_table[j].logical_pernode_ipaddr) != 0) {
				destroy_table();
				return (-1);
			}

			/* Compute logical pairwise address */
			if (get_logical_pairwise_inaddr((nodeid_t)j, lndid,
			    &db_table[j].logical_pairwise_ipaddr) != 0) {
				destroy_table();
				return (-1);
			}

			/* Do we need physical IP information? */
			if (!physical_needed) {
				continue;
			}

			/*
			 * Read in maximum adapter id and the physical IP
			 * associated with the adapters.
			 */
			db_table[j].maximum_adapterid =
			    get_physical_inaddrs((nodeid_t)j,
				&db_table[j].physical_ipaddrs);
			if (db_table[j].maximum_adapterid < 0) {
				destroy_table();
				return (-1);
			}

			/* Compute physical hostnames */
			db_table[j].physical_hostnames = (char **)
			    calloc((size_t)(db_table[j].maximum_adapterid + 1),
			    sizeof (char *));
			if (db_table[j].physical_hostnames == NULL) {
				destroy_table();
				return (-1);
			}
			for (k = 1; k <= db_table[j].maximum_adapterid; k++) {
				if (db_table[j].physical_ipaddrs[k].s_addr ==
				    (in_addr_t)0) {
					continue;
				}
				db_table[j].physical_hostnames[k] =
				    get_physical_hostname_from_logical(
					db_table[j].priv_hostname, k);
				if (db_table[j].physical_hostnames[k] == NULL) {
					destroy_table();
					return (-1);
				}
			}
		}

		if (db_generation == clconf_get_ccr_generation(
		    "infrastructure"))
			success = 1;
	}

	physical_ready = physical_needed;

	return (0);
}

/*
 * Try to resolve the passed IP address as if it were a logical address.
 */
/*ARGSUSED*/
static struct hostent *
get_logical_host_by_addr(min_hostent_t *mh, int *h_errnop, const char *addr,
    int len, int type)
{
	int nodeid;
	nodeid_t lndid;

	/*LINTED*/
	if ((nodeid = get_nodeid_by_logical_inaddr((struct in_addr *)addr))
	    == 0) {
		/*
		 *  not found
		 */
		*h_errnop = HOST_NOT_FOUND;

		return (NULL);
	}

	(void) mutex_lock(&db_lock);

	if (update_db(0) != 0 ||
	    db_table[nodeid].priv_hostname == NULL) {
		(void) mutex_unlock(&db_lock);
		*h_errnop = HOST_NOT_FOUND;
		return (NULL);
	}

	/*
	 * it's ours - copy in the data
	 */
	*h_errnop = 0;
	(void) strcpy(mh->mh_he.h_name, db_table[nodeid].priv_hostname);

	(void) memcpy(mh->mh_he.h_addr_list[0],
	    &db_table[nodeid].logical_pernode_ipaddr,
	    (size_t)len);

	/*
	 * if src==dst, the logical_pairwise_ipaddr is 0
	 * No need to copy address that is 0
	 * If we are running SC3.1U1 onwards then we don't need
	 * the pairwise address since no communication can
	 * happen over the pairwise address. Hence use cladmin
	 * to get the version of ipconf and if the version is
	 * greater than 1.0 then we do not need pairwise addresses
	 * else we need pairwise addresses.
	 */
	lndid = clconf_get_nodeid();
	if (db_table[nodeid].logical_pairwise_ipaddr.s_addr != 0 &&
	    pairwise_needed((nodeid_t)nodeid, lndid)) {
		(void) memcpy(mh->mh_he.h_addr_list[1],
		    &db_table[nodeid].logical_pairwise_ipaddr,
		    (size_t)len);
	} else {
		mh->mh_he.h_addr_list[1] = NULL;
	}
	(void) mutex_unlock(&db_lock);
	return (&mh->mh_he);
}

/*
 * Try to resolve the passed IP address as if it were a physical address.
 */
/*ARGSUSED*/
static struct hostent *
get_physical_host_by_addr(min_hostent_t *mh, int *h_errnop, const char *addr,
    int len, int type)
{
	int nodeid;
	int adpid;

	/* Verify if it fits the private physical address format */
	if (!is_physical_netaddr((struct in_addr *)addr)) {
		*h_errnop = HOST_NOT_FOUND;
		return (NULL);
	}

	(void) mutex_lock(&db_lock);

	if (update_db(1) != 0) {
		(void) mutex_unlock(&db_lock);
		*h_errnop = HOST_NOT_FOUND;
		return (NULL);
	}

	for (nodeid = 1; nodeid <= db_count; nodeid++) {
		if (db_table[nodeid].priv_hostname == NULL) {
			continue;
		}

		for (adpid = 1;
		    adpid <= db_table[nodeid].maximum_adapterid; adpid++) {
			if (!db_table[nodeid].physical_hostnames[adpid]) {
				continue;
			}
			if (memcmp(&db_table[nodeid].physical_ipaddrs[adpid],
			    addr, sizeof (struct in_addr)) != 0) {
				continue;
			}
			(void) strcpy(mh->mh_he.h_name,
			    db_table[nodeid].physical_hostnames[adpid]);
			(void) memcpy(mh->mh_he.h_addr_list[0],
			    &db_table[nodeid].physical_ipaddrs[adpid],
			    sizeof (struct in_addr));
			mh->mh_he.h_addr_list[1] = NULL;

			(void) mutex_unlock(&db_lock);

			*h_errnop = 0;
			return (&mh->mh_he);
		}
	}

	(void) mutex_unlock(&db_lock);

	*h_errnop = HOST_NOT_FOUND;
	return (NULL);
}

/*
 * Try to resolve the passed hostname as if it were a logical hostname.
 */
static struct hostent *
get_logical_host_by_name(min_hostent_t *mh, int *h_errnop, const char *name)
{
	int nodeid;
	nodeid_t lndid;

	(void) mutex_lock(&db_lock);

	if (update_db(0) != 0) {
		(void) mutex_unlock(&db_lock);
		*h_errnop = HOST_NOT_FOUND;
		return (NULL);
	}

	for (nodeid = 1; nodeid <= db_count; nodeid++) {
		if (db_table[nodeid].priv_hostname == NULL) {
			continue;
		}
		if (strcmp(db_table[nodeid].priv_hostname, name) != 0) {
			continue;
		}
		*h_errnop = 0;
		(void) strcpy(mh->mh_he.h_name,
		    db_table[nodeid].priv_hostname);
		(void) memcpy(mh->mh_he.h_addr_list[0],
		    &db_table[nodeid].logical_pernode_ipaddr,
		    sizeof (db_table[nodeid].logical_pernode_ipaddr));

		/*
		 * if src==dst, the logical_pairwise_ipaddr is 0
		 * No need to copy address that is 0
		 * If we are running SC3.1U1 onwards then we don't need
		 * the pairwise address since no communication can
		 * happen over the pairwise address. Hence use cladmin
		 * to get the version of ipconf and if the version is
		 * greater than 1.0 then we do not need pairwise addresses
		 * else we need pairwise addresses.
		 */
		lndid = clconf_get_nodeid();
		if (db_table[nodeid].logical_pairwise_ipaddr.s_addr
		    != 0 && pairwise_needed((nodeid_t)nodeid, lndid)) {
			(void) memcpy(mh->mh_he.h_addr_list[1],
			    &db_table[nodeid].logical_pairwise_ipaddr,
			    /* CSTYLED */
			    sizeof (db_table[nodeid].
			    logical_pairwise_ipaddr));
		} else {
			mh->mh_he.h_addr_list[1] = NULL;
		}
		(void) mutex_unlock(&db_lock);
		return (&mh->mh_he);
	}

	(void) mutex_unlock(&db_lock);

	*h_errnop = HOST_NOT_FOUND;
	return (NULL);
}

/*
 * Try to resolve the passed hostname as if it were a physical hostname.
 */
static struct hostent *
get_physical_host_by_name(min_hostent_t *mh, int *h_errnop, const char *name)
{
	int nodeid;
	int adpid;

	/* Verify if the name fits the private physical hostname format */
	if (!is_physical_hostname(name)) {
		*h_errnop = HOST_NOT_FOUND;
		return (NULL);
	}

	(void) mutex_lock(&db_lock);

	if (update_db(1) != 0) {
		(void) mutex_unlock(&db_lock);
		*h_errnop = HOST_NOT_FOUND;
		return (NULL);
	}

	for (nodeid = 1; nodeid <= db_count; nodeid++) {
		if (db_table[nodeid].priv_hostname == NULL) {
			continue;
		}

		for (adpid = 1; adpid <= db_table[nodeid].maximum_adapterid;
		    adpid++) {
			if (!db_table[nodeid].physical_hostnames[adpid]) {
				continue;
			}
			if (strcmp(name,
			    db_table[nodeid].physical_hostnames[adpid]) != 0) {
				continue;
			}

			(void) strcpy(mh->mh_he.h_name,
			    db_table[nodeid].physical_hostnames[adpid]);
			(void) memcpy(mh->mh_he.h_addr_list[0],
			    &db_table[nodeid].physical_ipaddrs[adpid],
			    sizeof (struct in_addr));
			mh->mh_he.h_addr_list[1] = NULL;

			(void) mutex_unlock(&db_lock);

			*h_errnop = 0;
			return (&mh->mh_he);
		}
	}
	(void) mutex_unlock(&db_lock);

	*h_errnop = HOST_NOT_FOUND;
	return (NULL);
}

#if SOL_VERSION >= __s10
/*
 * Destroy the incore database for the private IP table.
 */
static void
destroy_privip_table()
{
	if (privip_table != NULL) {
		free(privip_table);
		privip_table	= NULL;
		privip_count = 0;
		privip_gennum	= 0;
		free(privip_data);
		privip_data = NULL;
	}
}

/*
 * Synchronize the incore database with the underlying CCR privip table.
 * The whole table is read only if it has been updated since the last
 * successful read.
 */
int
update_privip_table()
{
	int new_version;
	uint_t count;

	/*
	 * Try to get the privip_ccr gennum using cladm. cladm will return
	 * error if the ORB hasn't been initialized. If so, return error
	 * from this function so that we do not attempt to read the
	 * privip_ccr when the ORB hasn't been initialized.
	 */
	new_version = clconf_get_ccr_generation("privip_ccr");
	if (new_version == -1) {
		return (-1);
	}

	if (privip_gennum == new_version) {
		return (0);
	}

	destroy_privip_table();
	privip_gennum = new_version;
	privip_table = scprivip_get_all_mappings_noorb(&count, &privip_data);
	privip_count = count;

	return (0);
}

/*
 * Try to resolve the passed hostname as if it were a private hostname
 * assigned to a local zone for private IP communication.
 */
static struct hostent *
get_privip_host_by_name(min_hostent_t *mh, int *h_errnop, const char *name)
{
	uint_t i;

	(void) mutex_lock(&privip_lock);

	if (update_privip_table() != 0) {
		(void) mutex_unlock(&privip_lock);
		*h_errnop = HOST_NOT_FOUND;
		return (NULL);
	}

	for (i = 0; i < privip_count; i++) {
		if (strcmp(privip_table[i]->priv_hostname, name)) {
				continue;
		}
		(void) strcpy(mh->mh_he.h_name,
		    privip_table[i]->priv_hostname);
		(void) memcpy(mh->mh_he.h_addr_list[0],
		    &privip_table[i]->priv_ipaddr,
		    sizeof (struct in_addr));
		mh->mh_he.h_addr_list[1] = NULL;
		(void) mutex_unlock(&privip_lock);
		*h_errnop = 0;
		return (&mh->mh_he);
	}

	(void) mutex_unlock(&privip_lock);

	*h_errnop = HOST_NOT_FOUND;
	return (NULL);
}

/*
 * Try to resolve the passed hostname as if it were a private hostname
 * assigned to a zone cluster.
 */
static struct hostent *
get_zone_cluster_privip_host_by_name(
    min_hostent_t *mh, int *h_errnop, const char *name,
    char *zone_cluster_name) {
	uint_t i;
	nodeid_t nodeid;
	int ret;

	(void) mutex_lock(&zc_privip_lock);

	if (update_zone_cluster_db(zone_cluster_name) != 0) {
		(void) mutex_unlock(&zc_privip_lock);
		*h_errnop = HOST_NOT_FOUND;
		return (NULL);
	}
	for (nodeid = 1; nodeid <= zc_count; nodeid++) {
		if (zc_table[nodeid].priv_hostname == NULL) {
			continue;
		}
		ret = strcmp(zc_table[nodeid].priv_hostname, name);
		if (strcmp(zc_table[nodeid].priv_hostname, name) != 0) {
			continue;
		}
		(void) strcpy(mh->mh_he.h_name,
		    zc_table[nodeid].priv_hostname);
		(void) memcpy(mh->mh_he.h_addr_list[0],
		    &zc_table[nodeid].priv_ipaddr,
		    sizeof (struct in_addr));
		mh->mh_he.h_addr_list[1] = NULL;
		(void) mutex_unlock(&zc_privip_lock);
		*h_errnop = 0;
		return (&mh->mh_he);
	}

	(void) mutex_unlock(&zc_privip_lock);
	*h_errnop = HOST_NOT_FOUND;
	return (NULL);
}

/*
 * Try to resolve the passed IP address as if it were a private IP
 * address assigned to a local zone for private IP communication.
 */
/*ARGSUSED*/
static struct hostent *
get_privip_host_by_addr(min_hostent_t *mh, int *h_errnop, const char *addr,
    int len, int type)
{
	uint_t i;

	(void) mutex_lock(&privip_lock);

	if (update_privip_table() != 0) {
		(void) mutex_unlock(&privip_lock);
		*h_errnop = HOST_NOT_FOUND;
		return (NULL);
	}

	for (i = 0; i < privip_count; i++) {
		if (memcmp(&privip_table[i]->priv_ipaddr,
			    addr, sizeof (struct in_addr)) != 0) {
				continue;
		}
		(void) strcpy(mh->mh_he.h_name,
		    privip_table[i]->priv_hostname);
		(void) memcpy(mh->mh_he.h_addr_list[0],
			    &privip_table[i]->priv_ipaddr,
			    sizeof (struct in_addr));
		mh->mh_he.h_addr_list[1] = NULL;

		(void) mutex_unlock(&privip_lock);

		*h_errnop = 0;
		return (&mh->mh_he);
	}

	(void) mutex_unlock(&privip_lock);

	*h_errnop = HOST_NOT_FOUND;
	return (NULL);
}

/*ARGSUSED*/
static struct hostent *
get_zone_cluster_privip_host_by_addr(
    min_hostent_t *mh, int *h_errnop, const char *addr,
    int len, int type, char *zone_cluster)
{
	nodeid_t nodeid;
	char str[512];


	(void) mutex_lock(&zc_privip_lock);

	if (update_zone_cluster_db(zone_cluster) != 0) {
		(void) mutex_unlock(&zc_privip_lock);
		*h_errnop = HOST_NOT_FOUND;
		return (NULL);
	}

	for (nodeid = 1; nodeid <= zc_count; nodeid++) {
		if (memcmp(&zc_table[nodeid].priv_ipaddr,
		    addr, sizeof (struct in_addr)) != 0) {
			continue;
		}
		(void) strcpy(mh->mh_he.h_name,
		    zc_table[nodeid].priv_hostname);
		(void) memcpy(mh->mh_he.h_addr_list[0],
		    &zc_table[nodeid].priv_ipaddr,
		    sizeof (struct in_addr));
		mh->mh_he.h_addr_list[1] = NULL;

		(void) mutex_unlock(&zc_privip_lock);

		*h_errnop = 0;
		return (&mh->mh_he);
	}

	(void) mutex_unlock(&zc_privip_lock);

	*h_errnop = HOST_NOT_FOUND;
	return (NULL);
}

#endif

/*
 * Try to resolve the passed IP address. Attempt is made to resolve it as a
 * logical address first. If that fails we try to resolve it as a physical
 * address.
 */
struct hostent *
_gethostbyaddr(min_hostent_t *mh, int *h_errnop, const char *addr, int len,
    int type)
{
	struct hostent *hent;

#if SOL_VERSION >= __s10
	/*
	 * Verify the zone name belongs to a zone cluster. If yes,
	 * call
	 * get_zone_cluster_privip_hostbyaddr()
	 */

	/* Get the zoneid of the calling process */
	zoneid_t zoneid = getzoneid();
	char zonename[ZONENAME_MAX];
	uint_t cl_id;
	int error;

	/*
	 * If this is zone cluster, use get_zone_cluster_privip_host_by_addr.
	 * Otherwise try other functions including get_privip_host_by_addr
	 * which is required for resolving the 1334 zone private hostnames.
	 */
	if (getzonenamebyid(zoneid, zonename, sizeof (zonename)) > 0) {
		/* Verify whether zone belongs to a zone cluster */
		error = clconf_get_cluster_id_noorb(zonename, &cl_id);
		if ((error == 0) && (cl_id >= MIN_CLUSTER_ID)) {
			/* Zone cluster */
			hent = get_zone_cluster_privip_host_by_addr
			    (mh, h_errnop, addr, len, type, zonename);
			if (hent != NULL) {
				return (hent);
			}
		} else if ((strcmp(zonename, "global") == 0) ||
		    ((error == 0) && (cl_id == BASE_NONGLOBAL_ID))) {
			/* 1334 zone or global */
			hent = get_privip_host_by_addr(
			    mh, h_errnop, addr, len, type);
			if (hent != NULL) {
				return (hent);
			}
		}
	}
#endif
	hent = get_logical_host_by_addr(mh, h_errnop, addr, len, type);
	if (hent != NULL) {
		return (hent);
	}
	hent = get_physical_host_by_addr(mh, h_errnop, addr, len, type);
	if (hent != NULL) {
		return (hent);
	}
	return (hent);
}

/*
 * Try to resolve the passed hostname. Attempt is made to resolve it as a
 * logical hostname first. If that fails we try to resolve it as a physical
 * hostname.
 */
struct hostent *
_gethostbyname(min_hostent_t *mh, int *h_errnop, const char *name)
{
	struct hostent *hent;

#if (SOL_VERSION >= __s10)
	zoneid_t zoneid = getzoneid();
	char zonename[ZONENAME_MAX];
	uint_t cl_id;
	clconf_errnum_t error;

	/*
	 * If this a zone belonging to the zone cluster, call
	 * get_zone_cluster_privip_host_by_name() to resolve.
	 * If this a valid zone but does not belong to the zone cluster,
	 * assume it's a 1334 zone and call get_privip_host_by_name().
	 */
	if (getzonenamebyid(zoneid, zonename, sizeof (zonename)) > 0) {
		/* Verify whether zone belongs to a zone cluster */
		error = clconf_get_cluster_id_noorb(zonename, &cl_id);
		if ((error == 0) && (cl_id >= MIN_CLUSTER_ID)) {
			/* Zone cluster */
			hent = get_zone_cluster_privip_host_by_name
			    (mh, h_errnop, name, zonename);
			if (hent != NULL) {
				return (hent);
			}
		} else if ((strcmp(zonename, "global") == 0) ||
		    ((error == 0) && (cl_id == BASE_NONGLOBAL_ID))) {
			/* 1334 zone or global */
			hent = get_privip_host_by_name
			    (mh, h_errnop, name);
			if (hent != NULL) {
				return (hent);
			}
		}
	}
#endif
	hent = get_logical_host_by_name(mh, h_errnop, name);
	if (hent != NULL) {
		return (hent);
	}
	hent = get_physical_host_by_name(mh, h_errnop, name);
	if (hent != NULL) {
		return (hent);
	}
	return (hent);
}

/*
 * Try to get the ipconf version number. If the major number of ipconf is 1
 * we know that pairwise is needed else we know pairwise is not needed.
 * Basically we are responding with the information we have at this point
 * - since the per node address is always applicable, we always return that
 * address. We don't know whether pairwise addresses are applicable until we
 * can access the version manager, hence we don't return that unless
 * we can access the vm.
 */
int
pairwise_needed(nodeid_t src, nodeid_t dest)
{
	uint16_t maj_num, min_num;

	if (src == dest)
		return (0); /* not needed */

	/*
	 * XXX - This is a hack to disable the version manager code for
	 *	 the nameserver switch code.  The version maneger code
	 *	 was reintroduced into 3.2 but the pairwise address scheme
	 *	 has not been used since 3.1u1.  But for now, the ipconf
	 *	 pairwise code is being left in since there may be
	 *	 dependencies on it later.
	 */
	if (!cl_nss_get_version)
		return (0);

	/*
	 * If we can't get the version number then we return 0
	 * this means that we assume we are running version 3.1u1
	 * onwards. The pairwise address is only needed if we can
	 * access the version manager and find out that the version
	 * of ipconf is 1.0 (i.e. SC3.1).
	 */
	if (get_version(&maj_num, &min_num, src))
		return (0); /* not needed */

	if (maj_num > 1)
		return (0);	/* not needed */
	else
		return (1);	/* needed */
}
