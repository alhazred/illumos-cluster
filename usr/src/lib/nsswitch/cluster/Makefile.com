#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.19	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/nsswitch/cluster/Makefile.com
#
LIBRARY= nss_cluster.a
VERS= .1

OBJECTS=nss_cluster.o nss_db.o nss_clconf.o

# include library definitions, do not change order of include and DYNLIB
include ../../../Makefile.lib

POFILE=		nss_cluster_msgs.po
SRCS=		$(OBJECTS:%.o=../common/%.c)
MAPFILE=	../common/mapfile-vers
CLOBBERFILES += $(PIFILES)
CHECKHDRS=	nss_cluster.h

# if we call this "DYNLIB", it will automagically be expanded to
# libnss_files.so*, and we don't have a target for that.

LIBS = $(DYNLIB)

CPPFLAGS += -D_REENTRANT -I$(SRC)/common/cl
$(POST_S9_BUILD)CPPFLAGS += -I$(SRC)/common/cl/privip
LDLIBS += -lnsl -lc -lsocket
PMAP =	-M $(MAPFILE)

LINTFILES += $(OBJECTS:%.o=%.ln)

.KEEP_STATE:

all: $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
