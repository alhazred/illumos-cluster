/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _HBIO_H
#define	_HBIO_H

#pragma ident	"@(#)hbio.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <stropts.h> /* strbuf - getmsg */

typedef unsigned long hbio_dev_handle_t;

/*
 * Initialize the communication with the heartbeat device
 */
int
hbio_init(char *device, hbio_dev_handle_t *handle);

/*
 * Close the heartbeat device communication channel
 */
int
hbio_finish(hbio_dev_handle_t *handle);

/*
 * IOCTL function
 */
int
hbio_ioctl(hbio_dev_handle_t *handle, int request, char *argp, size_t datalen);

/*
 * READ function
 */
int
hbio_read(hbio_dev_handle_t *handle, char *buf, size_t len);


/*
 * getmsg function (stream interface)
 */
int
hbio_getmsg(hbio_dev_handle_t *handle, struct strbuf *ctlptr,
    struct strbuf *dataptr, int *flagsptr);

#ifdef __cplusplus
}
#endif

#endif /* _HBIO_H */
