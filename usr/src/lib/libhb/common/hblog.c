/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)hblog.c 1.3     08/05/20 SMI"

#include <fmm/hblog.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <libintl.h>
#include <locale.h>

/*
 * Module initializer
 */
void _hblog_init(void);
#pragma init(_hblog_init)

/* debug flag */
int hblog_debug = 0;
#ifdef linux
int hblog_init_done = 0;
#endif

void
_hblog_init(void)
{
	char *pdebug;

	/*
	 * Initialize internationalization
	 */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	pdebug = getenv("DEBUG");
	if (pdebug) {
		hblog_debug = 1;
	}
}

void
hblog_error(int priority, char *format, ...)
{
	va_list args;

#ifdef linux
	if (!hblog_init_done) {
		_hblog_init();
		hblog_init_done = 1;
	}
#endif

	if ((priority == LOG_DEBUG) && ! hblog_debug) {
		goto skip_traces;
	}

	va_start(args, format);
	vsyslog(priority, gettext(format), args);
	va_end(args);

skip_traces:
	/*
	 * Abort the process for the following priorities
	 */
	if ((priority == LOG_CRIT) || (priority == LOG_EMERG) ||
	    (priority == LOG_ALERT)) {
		abort();
	}
}
