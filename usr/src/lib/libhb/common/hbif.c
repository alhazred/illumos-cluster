/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)hbif.c	1.4	08/05/20 SMI"

#include <sys/socket.h>
#ifdef Solaris
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stropts.h>
#include <sys/dlpi.h>
#include <sys/types.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <inet/ip.h>
#include <sys/sockio.h>
#include <ctype.h>
#endif

#ifdef linux
#include <netinet/ip.h>
#include <linux/sockios.h>
#include <linux/if.h>
#include <linux/if_arp.h>
#include <sys/time.h>
#endif

#include <assert.h>
#include <errno.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>		/* strerror */
#include <strings.h>		/* bzero */
#include <stropts.h>
#include <sys/sysmacros.h>
#include <time.h>		/* nanosleep */
#include <unistd.h>
#include <netinet/in.h>

#include <fmm/hbargs.h>
#include <fmm/hbif.h>
#include <fmm/hblog.h>
#include "hbio.h"

/* Global variables */
static struct in_addr hbif_mcast_addr[HB_MAX_PHYS_LINKS];
static struct in_addr hbif_local_addr[HB_MAX_PHYS_LINKS];
static struct in_addr hbif_netmask[HB_MAX_PHYS_LINKS];

/* Multicast groups constants */
#define	MC_LINK_OFF	0
#define	MC_LINK_MASK	0xFF

/* HB device name */
#ifdef Solaris
static char HBIF_DEV_NAME[] = "/dev/hbxdrv";
#endif
#ifdef linux
static char HBIF_DEV_NAME[] = "/dev/hbtdrv0";
#endif

/* Global flags */
static volatile enum hbx_mode_t hbif_mode = HBX_MODE_OFF;
static boolean_t		hbif_init_done = B_FALSE;

/* Reception thread's id */
static pthread_t		hbif_rcv_tid = (pthread_t)-1;

/* hbdrv device handle */
static hbio_dev_handle_t	hbif_hbdev_handle;

/* Event handler */
static hbif_callback		*hbif_event_handler;

/* List of devices and sockets */
static hbx_dev_t		hbif_devices[HB_MAX_PHYS_LINKS];
static int			hbif_sockets[HB_MAX_PHYS_LINKS];
static enum hbx_emit_state	hbif_devices_state[HB_MAX_PHYS_LINKS];

/* hb message dst port */
static in_port_t		hbif_udp_dst_port = HBX_UDP_DST_PORT;

/* ip pacakets ttl */
static uint8_t			hbif_ttl = HBX_IP_TTL;

/*
 * Library global variable initializer
 */
void _hbif_init(void);
#pragma init(_hbif_init)

/*
 * *****************************************************************
 * Local functions
 * *****************************************************************
 */

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Initialize global variables
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
void
_hbif_init(void)
{
	int l;

	for (l = 0; l < HB_MAX_PHYS_LINKS; l++) {
		hbif_devices[l] = 0;
		hbif_devices_state[l] = HBX_EMIT_STATE_NONE;
	}
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Create sockets with the  physical links of the current node
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK:	Success
 *   o HB_ENOTSETUP: Failed
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbif_create_socket(void)
{
	uchar_t	   loop;
	hb_error_t status;
	uint8_t    linkCount = hbargs_get_nic_count();
	int		i;

	for (i = 0; i < linkCount; i++) {
		hbif_sockets[i] = socket(AF_INET, SOCK_DGRAM, 0);
		if (hbif_sockets[i] == -1) {
			hblog_error(LOG_ERR,
			    "Cannot create socket, errno = [%s]",
			    strerror(errno)); /*lint !e746 */
			(void) hbio_finish(&hbif_hbdev_handle);
			status = HB_ENOTSUP;
			goto terminate;
		}

		loop = 0;
		if (setsockopt(hbif_sockets[i], IPPROTO_IP,
		    IP_MULTICAST_LOOP, &loop, sizeof (loop)) == -1) {
			hblog_error(LOG_ERR,
			    "Create socket: cannot set socket option, "
			    "link=%d errno = [%s]",
			    link, strerror(errno));
			(void) hbio_finish(&hbif_hbdev_handle);
			status = HB_ENOTSUP;
			goto terminate;
		}
	}

	status = HB_OK;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Join/drop the multicast group
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o B_TRUE:  Success
 *   o B_FALSE: Failed
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static boolean_t
hbif_set_multicast(int opt_name)
{
	int		l;
	struct ip_mreq	mreq;
	hb_error_t	status;
	uint8_t	 linkCount = hbargs_get_nic_count();

	for (l = 0; l < linkCount; l++) {

		hblog_error(LOG_DEBUG,	"Multicast addr 0x%x",
			    htonl(hbif_mcast_addr[l].s_addr));
		/*
		 * If a Broadcast address is configured, nothing to do.
		 */
		if (!IN_MULTICAST(htonl(hbif_mcast_addr[l].s_addr))) {
			hblog_error(LOG_NOTICE,
			    "Multicast link %d group %s <= %s",
			    l, inet_ntoa(hbif_mcast_addr[l]),
			    "Not Multicast -> will use BROADCAST");
			/*
			 * Nothing to do.
			 */
			continue;
		}
		mreq.imr_multiaddr = hbif_mcast_addr[l];
		mreq.imr_interface = hbif_local_addr[l];

		hblog_error(LOG_DEBUG,
		    "Multicast link %d group %s <= %s",
		    l, inet_ntoa(hbif_mcast_addr[l]),
		    (opt_name == IP_ADD_MEMBERSHIP ? "ADD" :
		    (opt_name == IP_DROP_MEMBERSHIP ? "DROP" : "UNKNOWN")));

		if (setsockopt(hbif_sockets[l], IPPROTO_IP,
		    opt_name, &mreq, sizeof (mreq)) == -1) {
			hblog_error(LOG_ERR,
			    "Set_multicast: Cannot set socket option, "
			    "link = %d errno = [%s]",
			    l, strerror(errno));
			status = HB_ENOTSUP;
			goto terminate;
		}
	}

	status = HB_OK;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Compute the multicast group ids
 *
 * Returns:
 *   o B_TRUE:  Success
 *   o B_FALSE: Failed
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static boolean_t
hbif_multicast_groups(void)
{
	int		l;
	in_addr_t	fixed = hbargs_get_mca();
	uint8_t		linkCount = hbargs_get_nic_count();
	boolean_t	status = B_TRUE;

	for (l = 0; l < linkCount; l++) {
		hbif_mcast_addr[l].s_addr =
		    fixed | (in_addr_t)((l & MC_LINK_MASK) << MC_LINK_OFF);
		hblog_error(LOG_DEBUG,
		    "Multicast address for link %u: %s",
		    l, inet_ntoa(hbif_mcast_addr[l]));

	}

	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the used local addresses in order to bound the sending sockets.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Also verify that the used interfaces have multicast support enabled,
 * else returns 0.
 * Note : the list is correctly computed if each underlying
 * interface supports multicast
 *
 * Returns:
 *   o B_TRUE:  Success
 *   o B_FALSE: Failed
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
#ifdef Solaris
static boolean_t
hbif_get_local_addr(void)
{
	struct lifreq		*pliflist;
	struct lifconf		lifconf;
	struct lifreq		*lifr;
	struct lifnum		lifn;
	struct lifreq		lifc;
	int			lifconf_size;
	int			n;
	int			result;
	unsigned int		found_link_count = 0;
	int			fd;
	unsigned int		jj;
	char			*nic;
	uint8_t			linkCount;
	hb_nic_list_t		nicList;
	boolean_t		status;

	if ((fd = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
		hblog_error(LOG_ERR,
		    "Cannot get PF_IFNET list, errno = [%s]",
		    strerror(errno));
		status = B_FALSE;
		goto terminate;
	}
	/*
	 * Get interface list.
	 */
	lifn.lifn_family = AF_UNSPEC;
	lifn.lifn_flags  = LIFC_NOXMIT;

	if (ioctl(fd, SIOCGLIFNUM, &lifn) < 0) {
		hblog_error(LOG_ERR,
		    "ioctl SIOCGLIFNUM failed: %s",
		    strerror(errno));
		status = B_FALSE;
		goto terminate;
	}

	lifconf_size = (int)(sizeof (struct lifreq)) * lifn.lifn_count;
	pliflist = (struct lifreq *)malloc((size_t)lifconf_size);
	if (!pliflist) {
		hblog_error(LOG_ERR,
		    "Cannot alloc pliflist (no more memory), errno = [%s]",
		    strerror(errno));
		status = B_FALSE;
		goto terminate;
	}

	lifconf.lifc_family = AF_UNSPEC;
	lifconf.lifc_flags = LIFC_NOXMIT;
	lifconf.lifc_len = lifconf_size;
	lifconf.lifc_buf = (char *)pliflist;
	if (ioctl(fd, SIOCGLIFCONF, &lifconf) < 0) {
		hblog_error(LOG_ERR,
		    "Cannot get IFNET list, errno = [%s]",
		    strerror(errno));
		status = B_FALSE;
		goto terminate;
	}

	linkCount = hbargs_get_nic_list(nicList);
	lifr = lifconf.lifc_req;
	for (n = lifconf.lifc_len / (int)(sizeof (struct lifreq)); n > 0;
	    n--, lifr++) {
		(void) memcpy(&lifc, lifr, sizeof (lifc));
		result = ioctl(fd, SIOCGLIFADDR, (char *)&lifc);
		if (result != 0) {
			continue;
		}
		for (jj = 0; jj < linkCount; jj++) {
			nic = nicList[jj];
			if (strcmp(nic, lifc.lifr_name) != 0) {
				continue;
			}
			/* get the address */
			(void) memcpy(&hbif_local_addr[jj],
			    &(((struct sockaddr_in *)
			    &(lifc.lifr_addr))->sin_addr),
			    sizeof (struct in_addr));
			hblog_error(LOG_DEBUG,
			    "Address for %s: %s",
			    nic, inet_ntoa(hbif_local_addr[jj]));
			/* get the netmask */
			result = ioctl(fd, SIOCGLIFNETMASK, (char *)&lifc);
			if (result != 0) {
				hblog_error(LOG_ERR,
				    "Cannot get the netmask for %s, "
				    "errno = [%s]",
				    nic, strerror(errno));
				status = B_FALSE;
				goto terminate;
			}

			(void) memcpy(&hbif_netmask[jj],
			    &(((struct sockaddr_in *)
			    &(lifc.lifr_addr))->sin_addr),
			    sizeof (struct in_addr));
			hblog_error(LOG_DEBUG,
			    "Netmask for %s: %s",
			    nic, inet_ntoa(hbif_netmask[jj]));

			/* get the flags */
			result = ioctl(fd, SIOCGLIFFLAGS, (char *)&lifc);
			if (result != 0) {
				hblog_error(LOG_ERR,
				    "Cannot get flags for interface "
				    "%s, errno = %s",
				    nic, strerror(errno));
				status = B_FALSE;
				goto terminate;
			}
			if (!(lifc.lifr_flags & IFF_MULTICAST)) {
				hblog_error(LOG_ERR,
				    "Interface %s doesn't support multicast",
				    nic);
				status = B_FALSE;
				goto terminate;
			}

			found_link_count++;
			if (found_link_count == linkCount) {
				status = hbif_multicast_groups();
				goto terminate;
			}
		}
	}

	status = B_TRUE;

terminate:
	(void) close(fd);
	return (status);
}
#endif /* Solaris */

#ifdef linux
static boolean_t
hbif_get_local_addr(void)
{
	struct ifreq		*pliflist;
	struct ifconf		lifconf;
	struct ifreq		*lifr;
	struct ifreq		lifc;
	int			lifconf_size;
	int			n;
	int			result;
	int			found_link_count = 0;
	int			fd;
	int			jj;
	char			*nic;
	uint8_t		 linkCount;
	hb_nic_list_t	   nicList;
	boolean_t		status;

	if ((fd = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
		hblog_error(LOG_ERR,
		    "Cannot get PF_IFNET list, errno = [%s]",
		    strerror(errno));
		status = B_FALSE;
		goto terminate;
	}


	lifconf_size = sizeof (struct ifreq) * HB_MAX_NIC;
	pliflist = (struct ifreq*)malloc(lifconf_size);
	if (!pliflist) {
		hblog_error(LOG_ERR,
		    "Cannot alloc pliflist (no more memory), errno = [%s]",
		    strerror(errno));
		status = B_FALSE;
		goto terminate;
	}

	/*
	 * Get interface list.
	 */
	lifconf.ifc_len = lifconf_size;
	lifconf.ifc_buf = (char *)pliflist;
	if (ioctl(fd, SIOCGIFCONF, &lifconf) < 0) {
		hblog_error(LOG_ERR,
		    "Cannot get IFNET list, errno = [%s]",
		    strerror(errno));
		status = B_FALSE;
		goto terminate;
	}

	hblog_error(LOG_DEBUG,
	    "Got %d network interfaces config data\n",
	    lifconf.ifc_len / sizeof (struct ifreq));

	linkCount = hbargs_get_nic_list(nicList);
	lifr = lifconf.ifc_req;
	for (n = lifconf.ifc_len / sizeof (struct ifreq); n > 0; n--, lifr++) {
		(void) memcpy(&lifc, lifr, sizeof (lifc));
		result = ioctl(fd, SIOCGIFADDR, (char *)&lifc);
		if (result != 0) {
			continue;
		}
		for (jj = 0; jj < linkCount; jj++) {
			nic = nicList[jj];
			if (strcmp(nic, lifc.ifr_name) != 0) {
				continue;
			}
			/* get the address */
			(void) memcpy(&hbif_local_addr[jj],
			    &(((struct sockaddr_in *)
			    &(lifc.ifr_addr))->sin_addr),
			    sizeof (struct in_addr));
			hblog_error(LOG_DEBUG,
			    "Address for %s: %s",
			    nic, inet_ntoa(hbif_local_addr[jj]));
			/* get the netmask */
			result = ioctl(fd, SIOCGIFNETMASK, (char *)&lifc);
			if (result != 0) {
				hblog_error(LOG_ERR,
				    "Cannot get the netmask for %s, "
				    "errno = [%s]",
				    nic, strerror(errno));
				status = B_FALSE;
				goto terminate;
			}

			(void) memcpy(&hbif_netmask[jj],
			    &(((struct sockaddr_in *)
			    &(lifc.ifr_addr))->sin_addr),
			    sizeof (struct in_addr));
			hblog_error(LOG_DEBUG,
			    "Netmask for %s: %s",
			    nic, inet_ntoa(hbif_netmask[jj]));

			/* get the flags */
			result = ioctl(fd, SIOCGIFFLAGS, (char *)&lifc);
			if (result != 0) {
				hblog_error(LOG_ERR,
				    "Cannot get flags for interface "
				    "%s, errno = %s",
				    nic, strerror(errno));
				status = B_FALSE;
				goto terminate;
			}
			if (!(lifc.ifr_flags & IFF_MULTICAST)) {
				hblog_error(LOG_ERR,
				    "Interface %s doesn't support multicast",
				    nic);
				status = B_FALSE;
				goto terminate;
			}

			found_link_count++;
			if (found_link_count == linkCount) {
				status = hbif_multicast_groups();
				goto terminate;
			}
		}
	}

terminate:
	(void) close(fd);
	return (status);
}
#endif /* linux */

#ifdef Solaris

#define	MAXDLBUF 256

static boolean_t
hbif_get_ether_addr(char *ifname, struct ether_addr *mac_addr)
{
	boolean_t status = B_TRUE;
	int ppa, flags;
	int fd = -1;
	long buffer[MAXDLBUF];
	struct strbuf ctl;
	dl_phys_addr_req_t phys_addr_req;
	dl_attach_req_t attach_req;
	dl_phys_addr_ack_t *dlpadd;

	char *device;
	char *full_device;
	char *device_name = NULL;
	char *unit;
	unsigned int i = 0;

	full_device = strdup(ifname);
	device = strtok(full_device, ":");

	i = strlen(device);
	while (isdigit(device[i - 1]) && (i > 0))
		i--;

	if (i == 0) {
		hblog_error(LOG_ERR, "Wrong format for device %s", ifname);
		status = B_FALSE;
		goto terminate;
	}

	device_name = malloc(strlen("/dev/") + i + 1);
	if (device_name == NULL) {
		hblog_error(LOG_ERR, "malloc error");
		status = B_FALSE;
		goto terminate;
	}

	(void) strcpy(device_name, "/dev/");
	(void) strncpy(device_name + strlen("/dev/"), device, i);
	device_name[strlen("/dev/") + i] = '\0';

	/* Add /dev/ at the beginning */
	if ((fd = open(device_name, O_RDWR, 0)) < 0) {
		hblog_error(LOG_ERR, "Opening device %s failed", device_name);
		status = B_FALSE;
		goto terminate;
	}

	device = device + i;
	i = 0;
	while (isdigit(device[i]))
		i++;

	if (i == 0) {
		hblog_error(LOG_ERR, "Wrong format for device %s", ifname);
		status = B_FALSE;
		goto terminate;
	}

	unit = malloc(i+1);
	if (unit == NULL) {
		hblog_error(LOG_ERR, "malloc error");
		status = B_FALSE;
		goto terminate;
	}

	(void) strncpy(unit, device, i);
	unit[i] = '\0';
	ppa = atoi(unit);
	attach_req.dl_primitive = DL_ATTACH_REQ;
	attach_req.dl_ppa = (t_uscalar_t)ppa;

	ctl.maxlen = 0;
	ctl.len = sizeof (attach_req);
	ctl.buf = (char *)&attach_req;
	flags = 0;

	if (putmsg(fd, &ctl, (struct strbuf*) NULL, flags) < 0) {
		hblog_error(LOG_ERR, "dlattachreq error");
		status = B_FALSE;
		goto terminate;
	}

	ctl.maxlen = MAXDLBUF;
	ctl.len = 0;
	ctl.buf = (char *)buffer;

	if (getmsg(fd, &ctl, (struct strbuf *)0, &flags) < 0) {
		hblog_error(LOG_ERR, "dlattachack error");
		status = B_FALSE;
		goto terminate;
	}

	if (ctl.len > (int)sizeof (dl_ok_ack_t)) {
		hblog_error(LOG_ERR, "dlattachokack too long");
		status = B_FALSE;
		goto terminate;
	}

	if (flags != RS_HIPRI) {
		hblog_error(LOG_ERR, "dlattachokack not RS_HIPRI");
		status = B_FALSE;
		goto terminate;
	}

	if (ctl.len < (int)sizeof (dl_ok_ack_t)) {
		hblog_error(LOG_ERR, "dlattachokack too short");
		status = B_FALSE;
		goto terminate;
	}

	phys_addr_req.dl_primitive = DL_PHYS_ADDR_REQ;
	phys_addr_req.dl_addr_type = DL_CURR_PHYS_ADDR;

	ctl.maxlen = 0;
	ctl.len = sizeof (phys_addr_req);
	ctl.buf = (char *)&phys_addr_req;
	flags = 0;

	if (putmsg(fd, &ctl, (struct strbuf*)NULL, flags) < 0) {
		hblog_error(LOG_ERR, "dlphysaddreq error");
		status = B_FALSE;
		goto terminate;
	}

	ctl.maxlen = MAXDLBUF;
	ctl.len    = 0;
	ctl.buf    = (char *)buffer;

	if (getmsg(fd, &ctl, (struct strbuf *)0, &flags) < 0) {
		hblog_error(LOG_ERR, "dlinfoack error");
		status = B_FALSE;
		goto terminate;
	}

	if (flags != RS_HIPRI) {
		hblog_error(LOG_ERR, "dlgetpyhsaddr not RS_HIPRI");
		status = B_FALSE;
		goto terminate;
	}
	if (ctl.len < (int)sizeof (dl_phys_addr_ack_t)) {
		hblog_error(LOG_ERR, "dlgetphysaddr too short");
		status = B_FALSE;
		goto terminate;
	}
	dlpadd = (dl_phys_addr_ack_t *)ctl.buf;

	(void) memcpy(mac_addr, ctl.buf+dlpadd->dl_addr_offset,
	    sizeof (struct ether_addr));
terminate:
	if (fd > 0)
		(void) close(fd);

	return (status);
}
#endif

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Links the devices with the hb's internal structures
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * To do so, the values in hbif_devices are re-ordered respecting the same
 * order used in the rest of the arrays containing information about the
 * links.
 *
 * Returns:
 *   o B_TRUE:  Success
 *   o B_FALSE: Failed
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static boolean_t
hbif_link_devices(void)
{
	int			idev, l;
	int			sock;
	hbx_ioc_get_dev_t	gdev;
	hbx_ioc_set_id_t	sdev;
#ifdef Solaris
	unsigned char mac_addr[6];
#endif
#ifdef linux
	struct ifreq		ifr;
	struct sockaddr		*saddr;
	unsigned char *mac_addr;
#endif
	unsigned char		*mac_dev;
	hbx_dev_t		temp_dev[HB_MAX_PHYS_LINKS];
	boolean_t		status;
	hb_nic_list_t		nic_list;
	uint8_t			linkCount = hbargs_get_nic_list(nic_list);

	(void) memset(&temp_dev[0], 0, sizeof (hbx_dev_t) * HB_MAX_PHYS_LINKS);

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock == -1) {
		hblog_error(LOG_ERR,
		    "Cannot create socket, errno = [%s]",
		    strerror(errno));
		status = B_FALSE;
		goto terminate;
	}

	for (l = 0; l < linkCount; l++) {
#ifdef Solaris
		if (hbif_get_ether_addr(nic_list[l],
		    (struct ether_addr*)mac_addr) != B_TRUE) {
			hblog_error(LOG_ERR,
			    "Cannot get MAC address of interface %s",
			    nic_list[l]);
			status = B_FALSE;
			goto terminate;
		}
#endif

#ifdef linux
		strcpy(ifr.ifr_name, nic_list[l]);
		if (ioctl(sock, SIOCGIFHWADDR, &ifr) < 0) {
			hblog_error(LOG_ERR,
			    "Cannot get MAC address of interface %s",
			    nic_list[l]);
			(void) close(sock);
			status = B_FALSE;
			goto terminate;
		}
		saddr = (struct sockaddr*)malloc(sizeof (struct sockaddr));
		if (!saddr) {
			hblog_error(LOG_CRIT,
			    "No more memory");
			status = B_FALSE;
			goto terminate;
		}
		memcpy((char *)saddr, (char*)&(ifr.ifr_hwaddr),
		    sizeof (struct sockaddr));
		mac_addr = (unsigned char *)&saddr->sa_data[0];
#endif
		for (idev = 0; idev < linkCount; idev++) {
			gdev.dev = hbif_devices[idev];
			if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_GET_DEV,
				(char *)&gdev, sizeof (gdev)) < 0) {
				hblog_error(LOG_ERR,
				    "Cannot get link %u device %p",
				    idev, hbif_devices[idev]);
				(void) close(sock);
				status = B_FALSE;
				goto terminate;
			}

			mac_dev = gdev.mac_src;

			if (mac_dev[0] == mac_addr[0] &&
			    mac_dev[1] == mac_addr[1] &&
			    mac_dev[2] == mac_addr[2] &&
			    mac_dev[3] == mac_addr[3] &&
			    mac_dev[4] == mac_addr[4] &&
			    mac_dev[5] == mac_addr[5]) {

				temp_dev[l] = hbif_devices[idev];
				hbif_devices_state[l] = gdev.state;

				/* Set the link number as the device ID */
				sdev.dev = temp_dev[l];
				sdev.id = (unsigned int)l;

				if (hbio_ioctl(&hbif_hbdev_handle,
					HBX_IOC_SET_ID,
					(char *)&sdev, sizeof (sdev)) < 0) {
					hblog_error(LOG_ERR,
					    "Cannot set id for "
					    "link %d", l);
					(void) close(sock);
					status = B_FALSE;
					goto terminate;
				}

				hblog_error(LOG_DEBUG,
				    "Setting id %u to device %p",
				    l, temp_dev[l]);

				break; /* leave the inner "for" loop */
			}
		}

		if (idev == linkCount) {
			/* Couldn't find the mac address */
			hblog_error(LOG_ERR, "Cannot match MAC address "
			    "for link %d (%02X:%02X:%02X:%02X:%02X:%02X)",
			    l, mac_addr[0], mac_addr[1], mac_addr[2],
			    mac_addr[3], mac_addr[4], mac_addr[5]);
			(void) close(sock);
			status = B_FALSE;
			goto terminate;
		}
	}

	/*
	 * Reset hbif_devices
	 */
	(void) memset(&hbif_devices[0], 0, sizeof (hbx_dev_t)*linkCount);
	(void) memcpy(&hbif_devices[0], &temp_dev[0],
	    sizeof (hbx_dev_t)*linkCount);
	(void) close(sock);

	hblog_error(LOG_DEBUG,
	    "Devices linked");

	status = B_TRUE;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the devices attached to the hb module
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o B_TRUE:  Success
 *   o B_FALSE: Failed
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static boolean_t
hbif_list_devices(void)
{
	hbx_ioc_list_dev_t	ldev;
	uint8_t			linkCount = hbargs_get_nic_count();
	boolean_t		status;

	ldev.dev_nb = sizeof (hbif_devices) / sizeof (hbx_dev_t);
	ldev.dev = &hbif_devices[0];

	if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_LIST_DEV,
		(char *)&ldev, sizeof (ldev)) != 0) {
		hblog_error(LOG_ERR,
		    "Cannot get list of devices");
		status = B_FALSE;
		goto terminate;
	}
#ifdef Solaris
	/* This restriction should be removed in the future. */
	if (linkCount != ldev.real_dev_nb) {
		hblog_error(LOG_ERR,
		    "Number of config devices and number of avail devices "
		    "don't match (%d, %d)",
		    linkCount, ldev.real_dev_nb);
		status = B_FALSE;
		goto terminate;
	}
#endif
	if (ldev.real_dev_nb != 0) {
		/* Re-order devices list if not empty */
		if (!hbif_link_devices()) {
			status = B_FALSE;
			goto terminate;
		}
	}

	hblog_error(LOG_DEBUG,
	    "Devices read from driver");

	status = B_TRUE;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the devices attached to the hb module and configure each device
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o B_TRUE:  Success
 *   o B_FALSE: Failed
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static boolean_t
hbif_set_devices_emit_parameters(enum hbx_mode_t mode,
    in_port_t udp_dst_port, uint8_t ttl)
{
	hbx_ioc_emit_cmd_t	emit;
	int			l;
	uint8_t			linkCount = hbargs_get_nic_count();
	boolean_t		status;
	struct timeval		tv;
#ifdef Solaris
	int			dummy;
#endif
#ifdef linux
	struct timezone		dummy;
#endif
	/*
	 * Get the devices attached to the hb module
	 */
	if (hbif_list_devices() == B_FALSE) {
		status = B_FALSE;
		hblog_error(LOG_ERR,
		    "Failed to read devices from the driver");
		goto terminate;
	}

	/*
	 * Compute an incarnation number
	 */
#ifdef linux
	if (gettimeofday(&tv, &dummy) != 0) {
#endif
#ifdef Solaris
	if (gettimeofday(&tv, (void *)&dummy) != 0) {
#endif
		status = B_FALSE;
		hblog_error(LOG_ERR,
		    "Failed to compute incarnation number");
		goto terminate;
	}

	/*
	 * Configure each device
	 */
	for (l = 0; l < linkCount; l++) {
		/*
		 * Set the emission params only if the device
		 * is in the right state:
		 *    o EMIT_STATE_NONE at first start
		 *    o EMIT_STATE_READY at restart
		 */
		if ((hbif_devices_state[l] == HBX_EMIT_STATE_NONE) ||
		    (hbif_devices_state[l] == HBX_EMIT_STATE_READY)) {
			emit.cmd = HBX_EMIT_CTL_SET_PARAMS;
			emit.dev = hbif_devices[l];
			emit.src = ntohl(hbif_local_addr[l].s_addr);
			emit.dst = ntohl(hbif_mcast_addr[l].s_addr);
			emit.mode = mode;
			emit.udp_dst_port = udp_dst_port;
			emit.ttl = ttl;
			emit.in = (uint32_t)tv.tv_sec;

			if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_EMIT_CTL,
				(char *)&emit, sizeof (emit)) < 0) {
				hblog_error(LOG_ERR,
				    "Cannot configure link %d",
				    l);
				status = B_FALSE;
				goto terminate;
			}
		}
	}

	hblog_error(LOG_DEBUG,
	    "All links configured");

	status = B_TRUE;

terminate:
	return (status);
}


/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Send a command to a device
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Param IN:
 *   o l   : The link-number
 *   o cmd : The hbdrv command
 *
 * Returns:
 *   o B_TRUE:  Success
 *   o B_FALSE: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static boolean_t
hbif_cmd_device(int l, enum hbx_ioctl_emit_type cmd)
{
	hbx_ioc_emit_cmd_t emit;
	boolean_t status = B_TRUE;

	emit.cmd = cmd;
	emit.dev = hbif_devices[l];

	if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_EMIT_CTL,
		(char *)&emit, sizeof (emit)) < 0) {
		hblog_error(LOG_ERR,
		    "Cannot command link %u device %p",
		    l, emit.dev);
		status = B_FALSE;
	}

	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Set global emission frequency
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o delay:   the detection delay in ms (-1 = not set)
 *   o numhb:   number of heartbeats per detection delay (-1 = not set)
 *
 * Returns:
 *   o HB_OK
 *   o HB_EINVAL: wrong parameters
 *   o HB_ENOTSUP: unexpected error
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbif_set_frequency(hbx_params_t *params)
{
	hbx_ioc_freq_t	freq;
	hb_error_t		status;

	/* Set emission frequency */
	freq.numhb = (params->numhb != 0 ? params->numhb : 1);
	freq.delay = (params->delay != 0 ? params->delay :
	    (params->numhb + 1) * 150);

	hblog_error(LOG_DEBUG,
	    "Setting freq params numhb \"%d\" delay \"%d\"",
	    freq.numhb, freq.delay);

	if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_EMIT_SET_FREQ,
		(char *)&freq, sizeof (freq)) < 0) {
		assert(errno == EINVAL || errno == ENOTSUP);
		if (errno == EINVAL) {
			status = HB_EINVAL;
			goto terminate;
		} else {
			status = HB_ENOTSUP;
			goto terminate;
		}
	}

	status = HB_OK;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get global emission frequency
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o pfreq: pointer to ioc-frequency data structure
 *
 * Returns:
 *   o HB_OK
 *   o HB_EINVAL:  wrong parameters
 *   o HB_ENOTSUP: unexpected error
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbif_get_frequency(hbx_ioc_freq_t *pfreq)
{
	hb_error_t status;

	if (pfreq == NULL) {
		hblog_error(LOG_ERR,
		    "hbif_get_frequency input parameter is NULL");
		status = HB_EINVAL;
		goto terminate;
	}

	/* Reset emission frequency */
	bzero((void *)pfreq, sizeof (hbx_ioc_freq_t));

	if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_EMIT_GET_FREQ,
		(char *)pfreq, sizeof (*pfreq)) < 0) {
		if (errno == EINVAL) {
			status = HB_EINVAL;
			goto terminate;
		} else {
			status = HB_ENOTSUP;
			goto terminate;
		}
	}

	hblog_error(LOG_DEBUG,
	    "Getting freq params numhb \"%d\" delay \"%d\" set \"%d\"",
	    pfreq->numhb, pfreq->delay, pfreq->set);

	status = HB_OK;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Check if frequency parameter is set
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * OUT Param:
 *   is_set : o B_TRUE : frequency parameter set
 *	    o B_FALSE: frequency parameter not set
 *
 * Returns:
 *   o HB_OK
 *   o HB_EINVAL : wrong parameters
 *   o HB_ENOTSUP: unexpected error
 *
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_is_frequency_set(boolean_t *is_set)
{
	hbx_ioc_freq_t freq;
	hb_error_t	status;

	if (is_set == NULL) {
		hblog_error(LOG_ERR,
		    "hbif_is_frequency_set input param is NULL");
		status = HB_EINVAL;
		goto terminate;
	}

	status = hbif_get_frequency(&freq);
	if (status != HB_OK) {
		hblog_error(LOG_ERR,
		    "hbif_is_frequency_set error %d\n",
		    status);
		goto terminate;
	}

	*is_set = (freq.set == 1 ? B_TRUE : B_FALSE);

terminate:
	return (status);
}

/*
 * **********************************************************************
 * Reception thread
 * **********************************************************************
 */

/* ********************************************************************* */

static void *
hbif_rcpt_thr(void *data)
{
	static hbx_event_t	event;
	static struct strbuf	strbuf;
	int			flagsp;
	int			ret;
	struct timespec		rqtp;

	/* Wait until probe is started */
	rqtp.tv_sec = 0;
	rqtp.tv_nsec = 200000000L;
	while (!(hbif_mode | HBX_RCV_MODE_ON)) {
		(void) nanosleep(&rqtp, NULL);
	}

	strbuf.maxlen = sizeof (event);
	strbuf.buf = (char *)&event;

	hblog_error(LOG_DEBUG,
	    "Start processing events from the driver %d", (int*)data);

	/* Start processing events from the driver */
	while (hbif_mode & HBX_RCV_MODE_ON) {
#ifdef Solaris
		/* Read the next event */
		flagsp = 0;
		ret = hbio_getmsg(&hbif_hbdev_handle, NULL, &strbuf, &flagsp);
#endif
#ifdef linux
		/* Read the next event */
		ret = hbio_read(&hbif_hbdev_handle, (char *)&strbuf.buf,
		    strbuf.maxlen);
#endif
		if (ret < 0) {
			if (ret != EINTR) {
				hblog_error(LOG_ERR,
				    "Error reading event: %s",
				    strerror(errno));
			}
			continue;
		}
		/* Ignore malformed events */
		if (strbuf.len != (int)(sizeof (event))) {
			hblog_error(LOG_ERR,
			    "Malformed event len = %d. Expecting %d",
			    strbuf.len, sizeof (event));
			continue;
		}

		/* If rcv mode is off, ignore the events */
		if (!(hbif_mode | HBX_RCV_MODE_ON)) {
			continue;
		}

		/*
		 * Send notification to client if needed
		 */
		if (hbif_event_handler != NULL) {
			hbif_event_handler(event);
		}
	}

	return (NULL);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Create reception thread
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbif_start_rcv_thread(void)
{
	int			res;
	pthread_attr_t		attr;
	int			policy;
	struct sched_param	param;
	hb_error_t		status;

	if (hbif_rcv_tid == (pthread_t)-1) {
		res = pthread_attr_init(&attr);
		if (res < 0) {
			hblog_error(LOG_ERR,
			    "pthread_attr_init returned %d %s",
			    res, strerror(errno));
			status = HB_ENOTSUP;
			goto terminate;
		}

		res = pthread_attr_setdetachstate(&attr,
		    PTHREAD_CREATE_DETACHED);
		if (res  < 0) {
			hblog_error(LOG_ERR,
			    "pthread_attr_setdetachstate returned %d %s",
			    res, strerror(errno));
			status = HB_ENOTSUP;
			goto terminate;
		}

		res = pthread_attr_setinheritsched(&attr,
		    PTHREAD_EXPLICIT_SCHED);
		if (res  < 0) {
			hblog_error(LOG_ERR,
			    "pthread_attr_setinheritsched returned %d %s",
			    res, strerror(errno));
			status = HB_ENOTSUP;
			goto terminate;
		}

		res = pthread_getschedparam(pthread_self(), &policy, &param);
		if (res < 0) {
			hblog_error(LOG_ERR,
			    "pthread_attr_getschedparam returned %d %s",
			    res, strerror(errno));
			status = HB_ENOTSUP;
			goto terminate;
		}

		param.sched_priority++;
		res = pthread_attr_setschedparam(&attr, &param);
		if (res < 0) {
			hblog_error(LOG_ERR,
			    "pthread_attr_setschedparam returned %d %s",
			    res, strerror(errno));
			status = HB_ENOTSUP;
			goto terminate;
		}

		(void) pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

		/* handler thread creation */
		res = pthread_create(&hbif_rcv_tid, &attr,
		    hbif_rcpt_thr, NULL);
		if (res < 0) {
			hblog_error(LOG_ERR,
			    "Reception thread : pthread_create "
			    "returned %d %s",
			    res, strerror(errno));
			status = HB_ENOTSUP;
			goto terminate;
		}
		(void) pthread_attr_destroy(&attr);
	}

	hblog_error(LOG_DEBUG,
	    "rcv_thread created and started %d\n",
	    hbif_rcv_tid);

	status = HB_OK;

terminate:
	return (status);
}


/*
 * *****************************************************************
 * Exported functions
 * *****************************************************************
 */

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Initialize hbdrv emission/reception parameters and init. network IF
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_init(hbif_callback *cb)
{
	hb_error_t status = HB_OK;

	/*
	 * Register client callback
	 */
	if (!cb) {
		hbif_event_handler = NULL;
	} else {
		hblog_error(LOG_DEBUG,
		    "Registering callback");
		hbif_event_handler = cb;
	}

	/*
	 * Initialize the devices list
	 */
	bzero(hbif_devices, sizeof (hbif_devices));

	/*
	 * Open the heartbeat driver
	 */
	if (hbio_init(HBIF_DEV_NAME, &hbif_hbdev_handle) < 0) {
		hblog_error(LOG_ERR,
		    "Cannot open heartbeat driver");
		status = HB_ENOTSUP;
		goto terminate;
	}

#ifdef linux
	status = hbif_list_devices();
	if (status != B_TRUE) {
		goto terminate;
	}
	if (hbif_devices[0] == 0) {
		/*
		 * Configure the network interfaces managed by the HB driver
		 * if not already done
		 */
		status = hbif_set_network_interfaces();
		if (status != HB_OK) {
			goto terminate;
		}
	}
#endif
	/*
	 * Check and get nic parameters
	 */
	if (!hbif_get_local_addr()) {
		hblog_error(LOG_ERR,
		    "Local addresses cannot be computed, "
		    "please check current node and interface configuration");
		status = HB_ENOTSUP;
		goto terminate;
	}

	/*
	 * Create the sockets with the local links
	 */
	status = hbif_create_socket();
	if (status != HB_OK) {
		goto terminate;
	}

	/*
	 * Join the multicast group
	 */
	status = hbif_set_multicast(IP_ADD_MEMBERSHIP);
	if (status != HB_OK) {
		goto terminate;
	}
	/*
	 * Set the init flag
	 */
	hbif_init_done = B_TRUE;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the identity parameters
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN param:
 *   o pident: pointer to an identity parameter data structure
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_get_identity(hbx_ioc_ident_t *pident)
{
	hb_error_t status;

	if (pident == NULL) {
		hblog_error(LOG_ERR,
		    "hbif_get_identity input param is NULL");
		status = HB_EINVAL;
		goto terminate;
	}

	bzero((void *)pident, sizeof (hbx_ioc_ident_t));

	if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_GET_IDENT,
		(char *)pident, sizeof (*pident)) < 0) {
		if (errno == EINVAL) {
			status = HB_EINVAL;
			goto terminate;
		} else {
			status = HB_ENOTSUP;
			goto terminate;
		}
	}

	hblog_error(LOG_DEBUG,
	    "Getting ident params node_id \"%d\" "
	    "clid \"0x%x\" version \"0x%x\" set \"%d\"",
	    pident->node_id, pident->clid,
	    pident->version, pident->set);

	status = HB_OK;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Set the identity parameters
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_set_identity(hbx_params_t *params)
{
	hb_error_t	status;
	hbx_ioc_ident_t ident;

	if (params == NULL) {
		hblog_error(LOG_ERR,
		    "hbif_set_identity input param is NULL");
		status = HB_EINVAL;
		goto terminate;
	}

	bzero((void *)&ident, sizeof (hbx_ioc_ident_t));
	ident.clid = params->clid;
	ident.version = params->version;
	ident.node_id = params->node_id;

	hblog_error(LOG_DEBUG,
	    "Setting ident params node_id \"%d\" "
	    "clid \"0x%x\" version \"0x%x\"",
	    ident.node_id, ident.clid, ident.version);

	if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_SET_IDENT,
		(char*) &ident, sizeof (ident)) < 0) {
		if (errno == EINVAL) {
			status = HB_EINVAL;
			goto terminate;
		} else {
			status = HB_ENOTSUP;
			goto terminate;
		}
	}

	status = HB_OK;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Check if identity parameter is set
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * OUT Param:
 *   is_set : o B_TRUE : identity parameter set
 *	    o B_FALSE: identity parameter not set
 *
 * Returns:
 *   o HB_OK
 *   o HB_EINVAL : wrong parameters
 *   o HB_ENOTSUP: unexpected error
 *
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_is_identity_set(boolean_t *is_set)
{
	hbx_ioc_ident_t ident;
	hb_error_t	status;

	if (is_set == NULL) {
		hblog_error(LOG_ERR,
		    "hbif_is_identity_set input param is NULL");
		status = HB_EINVAL;
		goto terminate;
	}

	status = hbif_get_identity(&ident);
	if (status != HB_OK) {
		hblog_error(LOG_ERR,
		    "hbif_is_identity_set error %d\n",
		    status);
		goto terminate;
	}

	*is_set = (ident.set == 1 ? B_TRUE : B_FALSE);

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Initialize the private key used to authenticate the hb packets
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_set_auth_params(hbx_params_t *params,
    enum hbx_auth_cmd_t cmd)
{
	hb_error_t	status;
	hbx_ioc_auth_t auth;

	if (params == NULL) {
		hblog_error(LOG_ERR,
		    "hbif_set_auth_params input param is NULL");
		status = HB_EINVAL;
		goto terminate;
	}

	bzero((void *)&auth, sizeof (hbx_ioc_auth_t));
	if (params->pkey_addr) {
		bcopy((void *)params->pkey_addr, (void *)&auth.pkey[0],
		    strlen((char *)params->pkey_addr));
	}
	if (params->alt_pkey_addr) {
		bcopy((void *)params->alt_pkey_addr, (void *)&auth.alt_pkey[0],
		    strlen((char *)params->alt_pkey_addr));
	}
	auth.cmd = cmd;

	if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_SET_AUTH,
		(char *)&auth, sizeof (auth)) < 0) {
		if (errno == EINVAL) {
			status = HB_EINVAL;
			goto terminate;
		} else {
			status = HB_ENOTSUP;
			goto terminate;
		}
	}

	status = HB_OK;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Set the hbdrv debug mode
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_set_debug(boolean_t debug_flag)
{
	hb_error_t status;

	hblog_error(LOG_DEBUG,
	    "Setting hbdrv debug mode");

	if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_SET_DEBUG,
		(char *)&debug_flag, sizeof (debug_flag)) < 0) {
		if (errno == EINVAL) {
			status = HB_EINVAL;
			goto terminate;
		} else {
			status = HB_ENOTSUP;
			goto terminate;
		}
	}

	status = HB_OK;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Set the syslog error logging period
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_set_syslog_period(hbx_delay_t syslog_period)
{
	hb_error_t status;
#ifdef Solaris
	hblog_error(LOG_DEBUG,
	    "Setting syslog period %d",
	    syslog_period);
	if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_SET_SYSLOG_PERIOD,
		(char *)&syslog_period, sizeof (hbx_delay_t)) < 0) {
		if (errno == EINVAL) {
			status = HB_EINVAL;
			goto terminate;
		} else {
			status = HB_ENOTSUP;
			goto terminate;
		}
	}
#endif
	status = HB_OK;

terminate:
	return (status);
}

#ifdef linux
/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Initialize the network interfaces to be considered by the HB driver
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *   o HB_EEXIST:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_set_network_interfaces()
{
	hb_error_t result;
	hb_nic_list_t nic_list;
	uint8_t nic_count = hbargs_get_nic_list(nic_list);
	int i;
	char *pnic;

	for (i = 0; i < nic_count; i++) {
		/* Suppress if aliasing substring is needed */
		pnic = nic_list[i];
		strtok(pnic, ":");

		hblog_error(LOG_DEBUG,
		    "Adding network interface to %s (%s)",
		    nic_list[i], pnic);

		if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_ADD_DEV,
			pnic, sizeof (*pnic)) < 0) {
			if (errno == EINVAL) {
				result = HB_EINVAL;
				goto terminate;
			} else {
				if (errno == EINVAL) {
					result = HB_EEXIST;
					goto terminate;
				} else {
					result = HB_ENOTSUP;
					goto terminate;
				}
			}
		}
	}

	result = HB_OK;

terminate:
	return (result);
}
#endif

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Initialize hbdrv parameters
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_set_params(hbx_params_t *params)
{
	hb_error_t status;
	boolean_t  is_set;

	if (params == NULL) {
		hblog_error(LOG_ERR,
		    "hbif_set_params input param is NULL");
		status = HB_EINVAL;
		goto terminate;
	}

	/*
	 * Set the debug mode on if required
	 */
	if (params->debug_flag) {
		status = hbif_set_debug(params->debug_flag);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_set_debug error %d",
			    status);
			goto terminate;
		}
	}

	/*
	 * Set heartbeat driver optional parameters
	 */
	if (params->udp_dst_port != 0) {
		hbif_udp_dst_port = params->udp_dst_port;
	}

	if (params->ttl != 0) {
		hbif_ttl = params->ttl;
	}

	if ((params->syslog_period) != 0) {
		status = hbif_set_syslog_period(params->syslog_period);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_set_syslog_period error %d",
			    status);
			goto terminate;
		}
	}

	/* Set authentication key if configured */
	if (params->pkey_addr) {
		status = hbif_set_pkey(params->pkey_addr);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_set_pkey error %d",
			    status);
			goto terminate;
		}
	}
	/* Set alternate authentication key if configured */
	if (params->alt_pkey_addr) {
		status = hbif_set_alt_pkey(params->alt_pkey_addr);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_set_alt_pkey error %d",
			    status);
			goto terminate;
		}
	}

	/*
	 * Set heartbeat message and driver mandatory parameters (if not set)
	 */

	/* Frequency parameters */
	status = hbif_is_frequency_set(&is_set);
	if (status != HB_OK) {
		goto terminate;
	} else {
		if (!is_set) {
			status = hbif_set_frequency(params);
			if (status != HB_OK) {
				hblog_error(LOG_ERR,
				    "hbif_set_frequency error %d",
				    status);
				goto terminate;
			}
		}
	}

	/* Identity parameters */
	status = hbif_is_identity_set(&is_set);
	if (status != HB_OK) {
		goto terminate;
	} else {
		if (!is_set) {
			if ((params != 0) &&
			    (params->node_id != 0) &&
			    (params->version != 0)) {
				status = hbif_set_identity(params);
				if (status != HB_OK) {
					hblog_error(LOG_ERR,
					    "hbif_set_identity error %d",
					    status);
					goto terminate;
				}
			} else {
				hblog_error(LOG_ERR,
				    "Invalid identity parameter(s) value(s)");
				status = HB_EINVAL;
				goto terminate;
			}
		}
	}

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Start hb emission and reception
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_start(enum hbx_mode_t mode)
{
	int	l;
	hb_error_t status = HB_OK;
	uint8_t    linkCount = hbargs_get_nic_count();

	/*
	 * Check if the probe init has been done
	 */
	if (!hbif_init_done) {
		hblog_error(LOG_ERR,
		    "Cannot start: library not initialized");
		status = HB_ENOTSUP;
		goto terminate;
	}

	/*
	 * Set the devices emission parameters
	 */
	if (!hbif_set_devices_emit_parameters
	    (mode, hbif_udp_dst_port, hbif_ttl)) {
		hblog_error(LOG_ERR,
		    "Failed to configure devices");
		status = HB_ENOTSUP;
		goto terminate;
	}

	/*
	 * Start hb emission/reception on every link
	 */
	for (l = 0; l < linkCount; l++) {
		hblog_error(LOG_DEBUG,
		    "Starting link %u device %p",
		    l, hbif_devices[l]);
		if (!hbif_cmd_device(l, HBX_EMIT_CTL_START)) {
			status = HB_ENOTSUP;
			goto terminate;
		}
	}

	/*
	 * Create reception thread (debug mode on Farm nodes;
	 * standard mode on Server nodes)
	 */
	if (mode & HBX_RCV_MODE_ON) {
		if (hbif_event_handler != NULL) {
			status = hbif_start_rcv_thread();
			if (status != HB_OK) {
				goto terminate;
			}
		}
		/*
		 * Synchronize with thread event-reception loop
		 */
		hbif_mode = mode;
		hblog_error(LOG_DEBUG,
		    "Setting mode to 0x%x",
		    mode);
	}

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Stop hb emission and reception
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_stop(void)
{
	int l;
	hb_error_t status = HB_OK;
	uint8_t linkCount = hbargs_get_nic_count();

	/*
	 * Stop the probe only if it has been initialized
	 */
	if (!hbif_init_done) {
		hblog_error(LOG_ERR,
		    "Cannot stop: library not initialized");
		status = HB_ENOTSUP;
		goto terminate;
	}

	/*
	 * Get the configured devices
	 */
	if (!hbif_list_devices()) {
		hblog_error(LOG_ERR,
		    "Failed to read devices from the driver");
		status = HB_ENOTSUP;
		goto terminate;
	}

	/*
	 * Stop the hearbeat driver for each managed device
	 */
	for (l = 0; l < linkCount; l++) {
		hblog_error(LOG_DEBUG,
		    "Stopping link %u device %p",
		    l, hbif_devices[l]);
		if (hbif_devices_state[l] == HBX_EMIT_STATE_RUN) {
			if (!hbif_cmd_device(l, HBX_EMIT_CTL_STOP)) {
				status = HB_ENOTSUP;
				goto terminate;
			}
		} else {
			hblog_error(LOG_ERR,
			    "Link %u device %p is not in the appropriate "
			    "state for stopping",
			    l, hbif_devices[l], l);
		}
	}

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Reset hb emission and reception parameters
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_reset(void)
{
	int l;
	uint8_t linkCount = hbargs_get_nic_count();
	hb_error_t status = HB_OK;

	/*
	 * Stop the probe only if it has been initialized
	 */
	if (!hbif_init_done) {
		hblog_error(LOG_ERR,
		    "Cannot reset: library not initialized");
		status = HB_ENOTSUP;
		goto terminate;
	}

	/*
	 * Get the configured devices
	 */
	if (!hbif_list_devices()) {
		/*
		 * The probe has not been previously started
		 */
		hblog_error(LOG_ERR,
		    "Failed to read devices from the driver");
		status = HB_ENOTSUP;
		goto terminate;
	}

	/*
	 * Reset hb configuration for each managed device
	 */
	for (l = 0; l < linkCount; l++) {
		hblog_error(LOG_DEBUG,
		    "Resetting link %u device %p",
		    l, hbif_devices[l]);
		if (!hbif_cmd_device(l, HBX_EMIT_CTL_RESET)) {
			status = HB_ENOTSUP;
			goto terminate;
		}
	}

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Read state of a host on a specified link
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o host: host identifier
 *   o link: link to consider
 *
 * Returns:
 *   o Current host state
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
enum hbx_host_state
hbif_get_host_state(ipaddr_t host, unsigned int l)
{
	hbx_ioc_get_host_t	get_host;

	get_host.dev  = hbif_devices[l];
	get_host.host = ntohl(host);

	if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_GET_HOST,
		(char *)&get_host, sizeof (get_host)) < 0) {
		hblog_error(LOG_DEBUG,
		    "Cannot get host %x",
		    host);
		get_host.state = HOST_STATE_DOWN;
	}

	return (get_host.state);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Delete a host from the driver
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o host: host identifier
 *   o link: link to consider
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_EINVAL: error
 */
hb_error_t
hbif_delete_host(ipaddr_t host, unsigned int l)
{
	hb_error_t status = HB_OK;
	hbx_ioc_del_host_t	del_host;

	del_host.dev  = hbif_devices[l];
	del_host.host = ntohl(host);

	if (hbio_ioctl(&hbif_hbdev_handle, HBX_IOC_DEL_HOST,
		(char *)&del_host, sizeof (del_host)) < 0) {
		hblog_error(LOG_DEBUG,
		    "Cannot del host %x",
		    host);
		status = HB_EINVAL;
	}

	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Provide the private key for hb msg authentication
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o pkey: private key
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_set_pkey(hbx_pkey_t *pkey)
{
	hb_error_t status;
	hbx_params_t params;

	if ((pkey) &&
	    (strlen((char*)pkey) <= HBX_PKEY_LEN)) {
		params.pkey_addr = pkey;
		params.alt_pkey_addr = NULL;
		status = hbif_set_auth_params(&params, HBX_AUTH_INIT_KEY);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_set_pkey error %d",
			    status);
			goto terminate;
		}
	} else {
		hblog_error(LOG_ERR,
		    "Invalid authentication parameter(s) value(s)");
		status = HB_EINVAL;
		goto terminate;
	}

	status = HB_OK;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Provide an alternate private key for hb msg authentication
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o pkey: private key
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_set_alt_pkey(hbx_pkey_t *pkey)
{
	hb_error_t status;
	hbx_params_t params;

	if ((pkey) &&
	    (strlen((char*)pkey) <= HBX_PKEY_LEN)) {
		params.pkey_addr = NULL;
		params.alt_pkey_addr = pkey;
		status = hbif_set_auth_params(&params, HBX_AUTH_CHANGE_KEY);
		if (status != HB_OK) {
			hblog_error(LOG_ERR,
			    "hbif_set_alt_pkey error %d",
			    status);
			goto terminate;
		}
	} else {
		hblog_error(LOG_ERR,
		    "Invalid authentication parameter(s) value(s)");
		status = HB_EINVAL;
		goto terminate;
	}

	status = HB_OK;

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Commit new private key for hb msg authentication
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK: Success
 *   o HB_ENOTSUP: Failure
 *   o HB_EINVAL:  Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbif_commit_pkey(void)
{
	hb_error_t status;
	hbx_params_t params;

	params.pkey_addr = NULL;
	params.alt_pkey_addr = NULL;

	status = hbif_set_auth_params(&params, HBX_AUTH_COMMIT_KEY);
	if (status != HB_OK) {
		hblog_error(LOG_ERR,
		    "hbif_commit_pkey error %d",
		    status);
	}

	return (status);
}
