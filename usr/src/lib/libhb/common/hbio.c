/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>

#include <fmm/hblog.h>
#include "hbio.h"

#pragma	ident	"@(#)hbio.c 1.2     08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Data type used to store device informations
 */
typedef struct hbio_device_t {
	char *device;
	int fd;
} hbio_device_t;

static hbio_device_t hbio_G_dev;

/*
 * Module initializer
 */
#ifdef __GNUC__
void _hbio_init(void) __attribute__((constructor));
#else
void _hbio_init(void);
#pragma init(_hbio_init)
#endif


/*
 * Initialize global variables
 */
void _hbio_init(void) {
	hbio_G_dev.device = NULL;
	hbio_G_dev.fd = -1;
}

/*
 * Initialize the communication with the device
 */
int
hbio_init(char *device, hbio_dev_handle_t *handle)
{
	int status = 0;

	/* Check input parameters */
	if ((handle == NULL) || (device == NULL)) {
		hblog_error(LOG_ERR, "Invalid parameters");
		status = -1;
		goto terminate;
	}

	if (hbio_G_dev.device) {
		status = -1;
		handle = (hbio_dev_handle_t *)NULL;
		goto terminate;
	}

	hbio_G_dev.fd = open(device, O_RDWR);
	if (hbio_G_dev.fd < 0) {
		hblog_error(LOG_ERR, "Cannot open %s", device);
		status = hbio_G_dev.fd;
		goto terminate;
	} else {
		hbio_G_dev.device = malloc(strlen(device) + 1);
		if (!hbio_G_dev.device) {
			hblog_error(LOG_ERR, "No more memory");
			status = -1;
			goto terminate;
		}
		hbio_G_dev.device = strcpy(hbio_G_dev.device, device);
		*handle = (hbio_dev_handle_t)&hbio_G_dev;
	}

terminate:
	return (status);
}

/*
 * Check hbio_dev_handle_t validity
 */
int
hbio_check_handle(hbio_dev_handle_t *handle)
{
	int status = 0;
	if (((hbio_device_t*)*handle != &hbio_G_dev) ||
	    (hbio_G_dev.fd <= 0)) {
		hblog_error(LOG_ERR, "Bad device handle");
		status = -1;
	}
	return (status);
}

/*
 * Close the device communication channel
 */
int
hbio_finish(hbio_dev_handle_t *handle)
{
	int status = 0;
	if (hbio_check_handle(handle)) {
		status = -1;
		goto terminate;
	}
	status = close(hbio_G_dev.fd);
	if (status < 0) {
		hblog_error(LOG_ERR, "Cannot close device %s",
		    hbio_G_dev.device);
		goto terminate;
	}
	free(hbio_G_dev.device);
	_hbio_init();

terminate:
	return (status);
}

/*
 * IOCTL function
 */
int
hbio_ioctl(hbio_dev_handle_t *handle, int request, char *argp,
    size_t datalen)
{
	int status = 0;
#ifdef Solaris
	struct strioctl io;
#endif
	if (hbio_check_handle(handle)) {
		status = -1;
		goto terminate;
	}
#ifdef linux
	status = ioctl(hbio_G_dev.fd, request, argp);
#endif
#ifdef Solaris
	io.ic_timout = -1;
	io.ic_cmd = (int)request;
	io.ic_len = (int)datalen;
	io.ic_dp = argp;
	status = ioctl(hbio_G_dev.fd, I_STR, &io);
#endif

terminate:
	return (status);
}

/*
 * READ function
 */
int
hbio_read(hbio_dev_handle_t *handle, char *buf, size_t len)
{
	int status = 0;
	if (hbio_check_handle(handle)) {
		status = -1;
		goto terminate;
	}
	status = read(hbio_G_dev.fd, buf, len);

terminate:
	return (status);
}

/*
 * getmsg function (stream interface)
 */
int
hbio_getmsg(hbio_dev_handle_t *handle, struct strbuf *ctlptr,
    struct strbuf *dataptr, int *flagsptr)
{
	int status = 0;
#ifdef linux
	status = -1;
	hblog_error(LOG_CRIT, "Not implemented");
	goto terminate;
#endif
	if (hbio_check_handle(handle)) {
		status = -1;
		goto terminate;
	}
	status = getmsg(hbio_G_dev.fd, ctlptr, dataptr, flagsptr);

terminate:
	return (status);
}

#ifdef __cplusplus
}
#endif
