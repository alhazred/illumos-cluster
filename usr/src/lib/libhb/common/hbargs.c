/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)hbargs.c 1.4     08/05/20 SMI"

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

#include <fmm/hbargs.h>
#include <fmm/hblog.h>
#include <scxcfg/scxcfg.h>

/*
 * ---------------------------------------------------------------------------
 * Global variables
 */
hb_nic_list_t   hbargs_nic_list;
uint8_t		hbargs_nic_count = 0;
hb_pkey_list_t  hbargs_pkey_list;
uint8_t		hbargs_pkey_count = 0;
hbx_clid_t	hbargs_clid = 0;
uint8_t		hbargs_ip_ttl = HBX_IP_TTL;
uint16_t	hbargs_udp_dst_port = HBX_UDP_DST_PORT;
hbx_delay_t	hbargs_syslog_period = HBX_SYSLOG_PERIOD;
hbx_node_id_t	hbargs_node_id = 0;
boolean_t	hbargs_debug = B_FALSE;
boolean_t	hbargs_init_done = B_FALSE;
hbx_delay_t	hbargs_delay = HBX_MIN_DELAY;
hbx_numhb_t	hbargs_numhb = HBX_DEF_DELAY / HBX_DEF_FREQ;
hbx_version_t	hbargs_version = HBX_PROTOCOL_VERSION;
ipaddr_t	hbargs_mca = (ipaddr_t)0xea007b00;

/*
 * ---------------------------------------------------------------------------
 * Property used by the hb module
 */

#define	PROP_FARM_PX	"farm.nodes."
#define	PROP_SERV_PX	"farm.servers."
#define	PROP_IFTYPE	"%d.network.interface.1.type"
#define	PROP_IFADAPTER	"%d.network.interface.1.adapters.%d.name"
#define	PROP_IFNAME	"%d.network.interface.1.name"
#define	PROP_AUTHKEY	"farm.properties.authentication_key"
#define	PROP_CLID	"farm.properties.cluster_id"
#define	PROP_QUANTUM	"farm.properties.heartbeat.quantum"
#define	PROP_NUMHB	"farm.properties.heartbeat.frequency"
#define	PROP_VERSION	"farm.properties.heartbeat.version"
#define	PROP_UDPDPORT	"farm.properties.heartbeat.udp_dest_port"
#define	PROP_IPTTL	"farm.properties.heartbeat.ttl"
#define	PROP_SYSLOGPER	"farm.properties.heartbeat.syslog_period"
#define	PROP_MCA	"farm.properties.heartbeat.mca"

/*
 * **************************************************************************
 * Local functions
 */

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the network interface names
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o cfg: config library handle
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_ENOMEM: Failure
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_nic_list(scxcfg_t cfg)
{
	hb_error_t	status = HB_OK;
	char		ifname[FCFG_MAX_LEN];
	int		i;
	char		property[64];
	char		prop_iftype[64];
	char		prop_ifadapter[64];
	char		prop_ifname[64];
	char		iftype[FCFG_MAX_LEN];
	scxcfg_error	error;
	scxcfg_nodeid_t	nodeid = 0;

	(void) memset(&hbargs_nic_list[0], 0, HB_MAX_NIC);
	(void) memset(&ifname[0], 0, FCFG_MAX_LEN);

	for (i = 0; i < HB_MAX_NIC; i++) {
		hbargs_nic_list[i] =  malloc(HB_NIC_LENGTH);
		if (!hbargs_nic_list[i]) {
			status = HB_ENOMEM;
			goto terminate;
		}
		(void) memset(hbargs_nic_list[i], 0, HB_NIC_LENGTH);
	}

	if (scxcfg_get_local_nodeid(&nodeid, &error) != FCFG_OK) {
		hblog_error(LOG_ERR,
		    "Cannot get nic list: %s",
		    scxcfg_error2string(&error));
		status = HB_EINVAL;
		goto terminate;
	}
	if (scxcfg_isfarm()) {
		(void) sprintf(prop_iftype, "%s%s", PROP_FARM_PX, PROP_IFTYPE);
		(void) sprintf(prop_ifadapter,
		    "%s%s", PROP_FARM_PX, PROP_IFADAPTER);
		(void) sprintf(prop_ifname, "%s%s", PROP_FARM_PX, PROP_IFNAME);
	} else {
		(void) sprintf(prop_iftype, "%s%s", PROP_SERV_PX, PROP_IFTYPE);
		(void) sprintf(prop_ifadapter,
		    "%s%s", PROP_SERV_PX, PROP_IFADAPTER);
		(void) sprintf(prop_ifname, "%s%s", PROP_SERV_PX, PROP_IFNAME);
	}
	/*
	 * If network.interface.1.type="simple", read network.interface.1.name
	 * else loop on network.interface.1.adapter.%d.name
	 */
	(void) sprintf(property, prop_iftype, nodeid);
	if ((scxcfg_getproperty_value(cfg, NULL, property, iftype, &error)
		== FCFG_OK) && (strcmp(iftype, "redundant") == 0)) {
		for (i = 0; i < HB_MAX_NIC; i++) {
			(void) sprintf(property,
			    prop_ifadapter, nodeid, i + 1);
			if (scxcfg_getproperty_value(cfg, NULL, property,
				ifname, &error) == FCFG_OK) {
				hblog_error(LOG_DEBUG, "Nic %s registered",
				    ifname);
				if (strlen(ifname) <= HB_NIC_LENGTH) {
					(void) strcpy(hbargs_nic_list[i],
					    ifname);
				} else {
					hblog_error(LOG_ERR,
					    "Nic %s is too long (max=%d)",
					    ifname, HB_NIC_LENGTH);
					status = HB_EINVAL;
					goto terminate;
				}
				hbargs_nic_count++;
			}
		}
	} else {
		(void) sprintf(property, prop_ifname, nodeid);
		if (scxcfg_getproperty_value(cfg, NULL, property, ifname,
			&error) == FCFG_OK) {
			hblog_error(LOG_DEBUG, "Nic %s registered",
			    ifname, NULL, NULL);
			if (strlen(ifname) <= HB_NIC_LENGTH) {
				(void) strcpy(hbargs_nic_list[0], ifname);
			} else {
				hblog_error(LOG_ERR,
				    "Nic %s is too long (max=%d)",
				    ifname, HB_NIC_LENGTH, NULL);
				status = HB_EINVAL;
				goto terminate;
			}
			hbargs_nic_count++;
		}
	}

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the authentication key
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o cfg: config library handle
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_ENOMEM: Failure
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_pkey_list(scxcfg_t cfg)
{
	hb_error_t	status = HB_OK;
	int		i;
	char		pkey[FCFG_MAX_LEN];
	scxcfg_error	error;

	(void) memset(&hbargs_pkey_list[0], 0, HB_MAX_PKEY);
	(void) memset(&pkey[0], 0, FCFG_MAX_LEN);

	for (i = 0; i < HB_MAX_PKEY; i++) {
		hbargs_pkey_list[i] =  malloc(HBX_PKEY_LEN);
		if (!hbargs_pkey_list[i]) {
			status = HB_ENOMEM;
			goto terminate;
		}
		(void) memset(hbargs_pkey_list[i], 0, HBX_PKEY_LEN);
	}

	for (i = 0; i < HB_MAX_PKEY; i++) {
		if ((scxcfg_getproperty_value(cfg, NULL,
		    PROP_AUTHKEY, pkey, &error) == FCFG_OK) && (pkey)) {
			hblog_error(LOG_DEBUG,
			    "Pkey registered");
			if (strlen(pkey) <= HBX_PKEY_LEN) {
				(void) strcpy((char *)hbargs_pkey_list[i],
				    pkey);
			} else {
				hblog_error(LOG_ERR,
				    "Pkey is too long (max=%d)",
				    HBX_PKEY_LEN);
				status = HB_EINVAL;
				goto terminate;
			}
			hbargs_pkey_count++;
		}
	}

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the HB UDP destination port
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o cfg: config library handle
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_udp_dst_port(scxcfg_t cfg)
{
	hb_error_t	status = HB_OK;
	int		fcfg_status;
	scxcfg_error	error;
	char		port[FCFG_MAX_LEN];

	(void) memset(&port[0], 0, FCFG_MAX_LEN);
	fcfg_status = scxcfg_getproperty_value(cfg, NULL, PROP_UDPDPORT,
	    port, &error);

	if (fcfg_status == FCFG_OK) {
		hbargs_udp_dst_port = (uint16_t)atoi(&port[0]);
	} else if (fcfg_status != FCFG_ENOTFOUND) {
		hblog_error(LOG_ERR,
		    "Cannot get udp dest port: %s",
		    scxcfg_error2string(&error));
		status = HB_EINVAL;
	}

	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the local node id
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_node_id(void)
{
	hb_error_t	status = HB_OK;
	scxcfg_error	error;

	if (scxcfg_get_local_nodeid(&hbargs_node_id, &error) != FCFG_OK) {
		hblog_error(LOG_ERR,
		    "Cannot get local node id: %s",
		    scxcfg_error2string(&error));
		status = HB_EINVAL;
	}

	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the HB IP ttl
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o cfg: config library handle
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_ttl(scxcfg_t cfg)
{
	hb_error_t	status = HB_OK;
	int		fcfg_status;
	scxcfg_error	error;
	char		ttl[FCFG_MAX_LEN];

	(void) memset(&ttl[0], 0, FCFG_MAX_LEN);
	fcfg_status = scxcfg_getproperty_value(cfg, NULL, PROP_IPTTL,
	    ttl, &error);

	if (fcfg_status == FCFG_OK) {
		hbargs_ip_ttl = (uint8_t)atoi(&ttl[0]);
	} else if (fcfg_status != FCFG_ENOTFOUND) {
		hblog_error(LOG_ERR,
		    "Cannot get ttl: %s",
		    scxcfg_error2string(&error));
		status = HB_EINVAL;
	}

	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the HB detection delay
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o cfg: config library handle
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_hb_detection_delay(scxcfg_t cfg)
{
	hb_error_t	status = HB_OK;
	int		fcfg_status;
	scxcfg_error	error;
	char		quantum[FCFG_MAX_LEN];

	(void) memset(&quantum[0], 0, FCFG_MAX_LEN);
	fcfg_status = scxcfg_getproperty_value(cfg, NULL, PROP_QUANTUM,
	    quantum, &error);

	if (fcfg_status == FCFG_OK) {
		hbargs_delay = (hbx_delay_t)atoi(&quantum[0]);
	} else if (fcfg_status != FCFG_ENOTFOUND) {
		hblog_error(LOG_ERR,
		    "Cannot get quantum: %s",
		    scxcfg_error2string(&error));
		status = HB_EINVAL;
	}

	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the HB frequency
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o cfg: config library handle
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_hb_frequency(scxcfg_t cfg)
{
	hb_error_t	status = HB_OK;
	int		fcfg_status;
	scxcfg_error	error;
	char		numhb[FCFG_MAX_LEN];

	(void) memset(&numhb[0], 0, FCFG_MAX_LEN);
	fcfg_status = scxcfg_getproperty_value(cfg, NULL, PROP_NUMHB,
	    numhb, &error);

	if (fcfg_status == FCFG_OK) {
		hbargs_numhb = (hbx_numhb_t)atoi(&numhb[0]);
	} else if (fcfg_status != FCFG_ENOTFOUND) {
		hblog_error(LOG_ERR,
		    "Cannot get frequency: %s",
		    scxcfg_error2string(&error));
		status = HB_EINVAL;
	}

	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the HB multicast address
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o cfg: config library handle
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_hb_mca(scxcfg_t cfg)
{
	hb_error_t	status = HB_OK;
	int		fcfg_status;
	scxcfg_error	error;
	char		mca[FCFG_MAX_LEN];

	(void) memset(&mca[0], 0, FCFG_MAX_LEN);
	fcfg_status = scxcfg_getproperty_value(cfg, NULL, PROP_MCA,
	    mca, &error);

	if (fcfg_status == FCFG_OK) {
		hbargs_mca = inet_addr(&mca[0]);
	} else if (fcfg_status != FCFG_ENOTFOUND) {
		hblog_error(LOG_ERR,
		    "Cannot get hb mca: %s",
		    scxcfg_error2string(&error));
		status = HB_EINVAL;
	}

	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the HB protocol version
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o cfg: config library handle
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_hb_version(scxcfg_t cfg)
{
	hb_error_t	status = HB_OK;
	int		fcfg_status;
	scxcfg_error	error;
	char		version[FCFG_MAX_LEN];
	char		*pstr;
	char		major[10];
	char		minor[10];
	unsigned int	l;
	uint8_t		maj, min;

	(void) memset(&version[0], 0, FCFG_MAX_LEN);
	fcfg_status = scxcfg_getproperty_value(cfg, NULL, PROP_VERSION,
	    version, &error);

	if (fcfg_status == FCFG_OK) {
		if (strlen(version) > 20) {
			status = EINVAL;
			hblog_error(LOG_ERR,
			    "Invalid protocol version string: %s",
			    version);
			goto terminate;
		}
		(void) memset(&major[0], 0, 10);
		(void) memset(&minor[0], 0, 10);
		pstr = strchr(version, '.');
		if (pstr == (char *)NULL) {
			hblog_error(LOG_ERR,
			    "Invalid protocol version string: %s",
			    version);
			goto terminate;
		}
		if (strlen(pstr) > 10) {
			status = EINVAL;
			hblog_error(LOG_ERR,
			    "Invalid protocol version string: %s",
			    version);
			goto terminate;
		}
		l = strcspn(version, ".");
		if (l > 10) {
			status = EINVAL;
			hblog_error(LOG_ERR,
			    "Invalid protocol version string: %s",
			    version);
			goto terminate;
		}
		(void) strncpy(&major[0], &version[0], l);
		(void) strcpy(&minor[0], pstr + 1);
		maj = (uint8_t)atoi(&major[0]);
		min = (uint8_t)atoi(&minor[0]);
		hbargs_version = (maj << 8) + min;
	} else if (fcfg_status != FCFG_ENOTFOUND) {
		hblog_error(LOG_ERR,
		    "Cannot get hb protocol version: %s",
		    scxcfg_error2string(&error));
		status = HB_EINVAL;
	}

terminate:
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the hbr syslog period
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o cfg: config library handle
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_syslog_period(scxcfg_t cfg)
{
	hb_error_t	status = HB_OK;
	int		fcfg_status;
	scxcfg_error	error;
	char		period[FCFG_MAX_LEN];

	(void) memset(&period[0], 0, FCFG_MAX_LEN);
	fcfg_status = scxcfg_getproperty_value(cfg, NULL, PROP_SYSLOGPER,
	    period, &error);

	if (fcfg_status == FCFG_OK) {
		hbargs_syslog_period = (hbx_delay_t)atoi(&period[0]);
	} else if (fcfg_status != FCFG_ENOTFOUND) {
		hblog_error(LOG_ERR,
		    "Cannot get syslog period: %s",
		    scxcfg_error2string(&error));
		status = HB_EINVAL;
	}

	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the libhb debug flag
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK:     Success
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_debug(void)
{
	hb_error_t	status = HB_OK;
	char		*pdebug;

	pdebug = getenv("DEBUG");
	if (pdebug) {
		hblog_error(LOG_DEBUG,
		    "Debug mode set");
		hbargs_debug = B_TRUE;
	}

	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Get the cluster id
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN Params:
 *   o cfg: config library handle
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static hb_error_t
hbargs_set_clid(scxcfg_t cfg)
{
	hb_error_t	status = HB_OK;
	scxcfg_error	error;
	char		clid[FCFG_MAX_LEN];
	char		*pstr;

	if ((scxcfg_getproperty_value(cfg, NULL,
	    PROP_CLID, clid, &error) == FCFG_OK) && (clid)) {
		hbargs_clid = (hbx_clid_t)strtol(clid, &pstr, 0);
		if (hbargs_clid != 0) {
			hblog_error(LOG_DEBUG, "Cluster ID %x registered",
			    hbargs_clid);
		} else {
			hblog_error(LOG_ERR, "Cluster ID is badly defined");
			status = HB_EINVAL;
		}
	} else {
		hblog_error(LOG_ERR,
		    "Cannot get cluster id: %s",
		    scxcfg_error2string(&error));
		status = HB_EINVAL;
	}

	return (status);
}

/*
 * ***************************************************************************
 * Exported functions
 */

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Read the hb parameters from the cluster config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbargs_init(void) {
	hb_error_t	status = HB_OK;
	scxcfg		cluster_cfg;
	scxcfg_error	error;
	int		s;

	if (hbargs_init_done) {
		goto terminate;
	}

	status = hbargs_set_debug();
	if (status != HB_OK) {
		goto terminate;
	}

	if (scxcfg_open(&cluster_cfg, &error) != FCFG_OK) {
		goto terminate;
	}

	status = hbargs_set_nic_list(&cluster_cfg);
	if (status != HB_OK) {
		goto terminate;
	}

	status = hbargs_set_pkey_list(&cluster_cfg);
	if (status != HB_OK) {
		goto terminate;
	}

	status = hbargs_set_clid(&cluster_cfg);
	if (status != HB_OK) {
		goto terminate;
	}

	status = hbargs_set_node_id();
	if (status != HB_OK) {
		goto terminate;
	}

	status = hbargs_set_udp_dst_port(&cluster_cfg);
	if (status != HB_OK) {
		goto terminate;
	}

	status = hbargs_set_ttl(&cluster_cfg);
	if (status != HB_OK) {
		goto terminate;
	}

	status = hbargs_set_syslog_period(&cluster_cfg);
	if (status != HB_OK) {
		goto terminate;
	}

	status = hbargs_set_hb_detection_delay(&cluster_cfg);
	if (status != HB_OK) {
		goto terminate;
	}

	status = hbargs_set_hb_frequency(&cluster_cfg);
	if (status != HB_OK) {
		goto terminate;
	}

	status = hbargs_set_hb_mca(&cluster_cfg);
	if (status != HB_OK) {
		goto terminate;
	}

	status = hbargs_set_hb_version(&cluster_cfg);
	if (status != HB_OK) {
		goto terminate;
	}

	hbargs_init_done = B_TRUE;

terminate:
	s = scxcfg_close(&cluster_cfg);
	if (s != 0) {
		status = HB_EINVAL;
	}
	return (status);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the node id read from the cluster config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o node id
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hbx_node_id_t
hbargs_get_node_id(void) {
	return (hbargs_node_id);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the detection delay read from the cluster config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o detection delay
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hbx_delay_t
hbargs_get_detectiondelay(void) {
	return (hbargs_delay);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the hb frequency read from the cluster config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o hb frequency
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hbx_numhb_t
hbargs_get_numhb(void) {
	return (hbargs_numhb);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the hb protocol version read from the cluster config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o hb protocol version
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hbx_version_t
hbargs_get_version(void) {
	return (hbargs_version);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the debug file (env. variable DEBUG)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o debug flag
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
boolean_t
hbargs_get_debug(void) {
	return (hbargs_debug);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the hb udp dest. port read from the config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o debug flag
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
uint16_t
hbargs_get_udp_dst_port(void) {
	return (hbargs_udp_dst_port);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the hb ip ttl read from the config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o debug flag
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
uint8_t
hbargs_get_ttl(void) {
	return (hbargs_ip_ttl);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the hb syslog logging period read from the config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o debug flag
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hbx_delay_t
hbargs_get_syslog_period(void) {
	return (hbargs_syslog_period);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the multicast address base read from the config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o multicast address base
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
ipaddr_t
hbargs_get_mca(void) {
	return (hbargs_mca);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the network interface list read from the config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN-OUT params:
 *   o network interface list
 *
 * Returns:
 *   o network interface list
 *   o network interface count
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
uint8_t
hbargs_get_nic_list(hb_nic_list_t nic_list)
{
	int i;
	for (i = 0; i < HB_MAX_NIC; i++) {
		nic_list[i] = hbargs_nic_list[i];
	}

	return (hbargs_nic_count);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the number of network interfaces configured
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o network interface count
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
uint8_t
hbargs_get_nic_count(void)
{
	return (hbargs_nic_count);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the cluster id read from the cluster config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o cluster id
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hbx_clid_t
hbargs_get_clid(void)
{
	return (hbargs_clid);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the authentication key read from the cluster config file
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN-OUT params:
 *   o authentication key list
 *
 * Returns:
 *   o authentication key
 *   o authentication key count
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
uint8_t
hbargs_get_pkey_list(hb_pkey_list_t pkey_list)
{
	int i;
	for (i = 0; i < HB_MAX_PKEY; i++) {
		pkey_list[i] = hbargs_pkey_list[i];
	}

	return (hbargs_pkey_count);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the number of authentication key configured
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Returns:
 *   o authentication key count
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
uint8_t
hbargs_get_pkey_count(void)
{
	return (hbargs_pkey_count);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Return the hb parameters configured
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * IN-OUT params:
 *   o hb parameters
 *
 * Returns:
 *   o HB_OK:     Success
 *   o HB_EINVAL: Failure
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
hb_error_t
hbargs_get_heartbeat_params(hbx_params_t *params)
{
	hb_error_t	status = HB_OK;
	uint8_t		count;

	if (params == NULL) {
		hblog_error(LOG_ERR,
		    "hbargs_get_params input param is NULL");
		status = HB_EINVAL;
		goto terminate;
	}

	/* Set heartbeat driver parameters */
	(void) memset((void *)params, 0, sizeof (hbx_params_t));
	params->delay = hbargs_get_detectiondelay();
	if (params->delay == 0) {
		hblog_error(LOG_ERR,
		    "hbargs_get_params detection delay is not set");
		status = EINVAL;
		goto terminate;
	}
	params->numhb = hbargs_get_numhb();
	if (params->numhb == 0) {
		hblog_error(LOG_ERR,
		    "hbargs_get_params numhb is not set");
		status = EINVAL;
		goto terminate;
	}
	params->clid = hbargs_get_clid();
	if (params->clid == 0) {
		hblog_error(LOG_ERR,
		    "hbargs_get_params cluster id is not set");
		status = EINVAL;
		goto terminate;
	}
	count = hbargs_get_pkey_count();
	if (count != 0) {
		hb_pkey_list_t pkey_list;
		count = hbargs_get_pkey_list(pkey_list);
		params->pkey_addr = pkey_list[0];
	} else {
		hblog_error(LOG_WARNING,
		    "hbargs_get_params private key is not set");
	}

	/* Those two params have default values */
	params->udp_dst_port = hbargs_get_udp_dst_port();
	params->ttl = hbargs_get_ttl();

	params->version = hbargs_get_version();
	if (params->version == 0) {
		hblog_error(LOG_ERR,
		    "hbargs_get_params version is not set");
		status = EINVAL;
		goto terminate;
	}

	params->node_id = hbargs_get_node_id();
	if (params->node_id == 0) {
		hblog_error(LOG_ERR,
		    "hbargs_get_params node_id is not set");
		status = EINVAL;
		goto terminate;
	}

	params->syslog_period = hbargs_get_syslog_period();

	params->debug_flag = hbargs_get_debug();

terminate:
		return (status);
}
