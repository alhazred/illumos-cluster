/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scnas_sun.c	1.3	08/05/20 SMI"

/*
 * scnas_sun.so Sun NAS filer config functions
 */

#include "../../scnas_common.h"
#include <clcomm_cluster_vm.h>
#include <wait.h>

#define	SCNAS_NAS_TYPE		"sun"

static boolean_t is_sun_NAS_usable(void);
static scnas_errno_t nas_validate_filer(char *filer_name, char **messages);
static scnas_errno_t nas_validate_filerdir(char *filer_name,
    scnas_filer_prop_t *props, scnas_filer_prop_t **new_props,
    uint_t *length, char **messages);

/*
 * scnas_chk_nas_type
 *
 *	This function confirms the NAS type.
 *
 * Return values:
 *
 *	boolean_t
 */
boolean_t
scnas_chk_nas_type(const char *scnas_type, boolean_t *props_required)
{
	/* Sun NAS requires properties */
	if (props_required) {
		*props_required = B_FALSE;
	}

	/* Check to enaure the device type is Sun NAS */
	if (scnas_type && (strcmp(scnas_type, SCNAS_NAS_TYPE) == 0)) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}

/*
 * scnas_chk_nas_name
 *
 *	Confirm if the given NAS filer is of the type of the
 *	library that calls this function. Returns the CCR
 *	table name in table_name if the table exists.
 *
 * Return values:
 *	boolean_t
 */
boolean_t
scnas_chk_nas_name(const char *scnas_filer, char *table_name)
{
	scnas_errno_t rstatus = SCNAS_NOERR;
	cl_query_table_t index_table;
	cl_query_table_t filer_table;
	boolean_t found = B_FALSE;
	uint_t i;
	char filer_table_name[SCNAS_MAXSTRINGLEN];

	/* Check the arguments */
	if (!scnas_filer || table_name == NULL) {
		return (B_FALSE);
	}

	/* Initialize */
	index_table.n_rows = 0;
	filer_table.n_rows = 0;

	/* Get the index table */
	rstatus = scnas_read_filer_table(NAS_SERVICE_KEY_TABLE,
	    NULL, NULL, &index_table, GET_TABLE_CONTENTS);
	if (rstatus != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Find the filer table from the index table and read */
	for (i = 0; i < index_table.n_rows; i++) {
		if (!(index_table.table_rows[i]->key) ||
		    !(index_table.table_rows[i]->value))
			continue;

		if (strcmp(index_table.table_rows[i]->value,
		    SCNAS_NAS_TYPE) != 0)
			continue;

		/* Get the filer table name */
		(void) sprintf(filer_table_name, "%s_%s",
		    NAS_SERVICE_TABLE_PRE,
		    index_table.table_rows[i]->key);

		/* Read this filer table */
		rstatus = scnas_read_filer_table(filer_table_name,
		    PROP_FILER_NAME, (char *)scnas_filer,
		    &filer_table, GET_TABLE_BY_KEY);
		if ((rstatus == SCNAS_NOERR) &&
		    (filer_table.n_rows > 0)) {
			found = B_TRUE;
			(void) strcpy(table_name, filer_table_name);
			break;
		} else {
			if (cl_query_free_table(&filer_table)
			    != CL_QUERY_OK) {
				found = B_FALSE;
				goto cleanup;
			}
		}
	}

cleanup:
	/* Release the memory allocated for the table */
	if (cl_query_free_table(&index_table) != CL_QUERY_OK) {
		found = B_FALSE;
	}

	if (cl_query_free_table(&filer_table) != CL_QUERY_OK) {
		found = B_FALSE;
	}

	return (found);
}

/*
 * scnas_chk_prop_required
 *
 *	Confirm if the given NAS filer is of the type of the
 *	library that calls this function, and sets whether
 *	this type requires property in the 2nd parameter.
 *
 * Return values:
 *
 *	boolean_t
 */
boolean_t
scnas_chk_prop_required(const char *scnas_filer, boolean_t *prop_required)
{
	boolean_t chk_name;
	char filer_table_name[SCNAS_MAXSTRINGLEN];

	/* Check if the given nas filer is valid */
	chk_name = scnas_chk_nas_name(scnas_filer, filer_table_name);
	if (prop_required) {
		*prop_required = B_FALSE;
	}

	return (chk_name);
}

/*
 * scnas_lib_attach_filer
 *
 *	Attach the given "filername" to cluster configuration if
 *	the filer is not already attached, along with its properties.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- no permission
 *	SCNAS_EINVAL		- invalid arguments
 *	SCNAS_EEXIST		- filer is already attached
 *	SCNAS_EUNKNOWN		- unknow type
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_VP_MISMATCH	- unmatched running version
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnas_lib_attach_filer(char *filername,
    scnas_filer_prop_t *prop_list, char **messages)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	char filer_table_name[SCNAS_MAXSTRINGLEN];
	cl_query_table_t filer_table;
	cl_query_table_t index_table;
	cl_query_error_t cl_err = CL_QUERY_OK;
	uint_t *index_nums = NULL;
	uint_t m_index;
	uint_t the_index;
	boolean_t create_key_table = B_FALSE;
	boolean_t create_filer_table = B_FALSE;
	char index_key[SCNAS_MAXSTRINGLEN];
	char errbuf[SCNAS_MAXSTRINGLEN];
	scnas_filer_prop_t *prop;

	/* Check arguments */
	if (!filername) {
		return (SCNAS_EINVAL);
	}

	/* Check if NAS version is valid */
	if (!is_sun_NAS_usable()) {
		return (SCNAS_VP_MISMATCH);
	}

	/* Sun NAS has no properties */
	for (prop = prop_list; prop; prop = prop->scnas_prop_next) {
		if (!(prop->scnas_prop_key))
			continue;

		if (messages != (char **)0) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Invalid property \"%s\" for \"%s\" NAS "
			    "device \"%s\".\n"),
			    prop->scnas_prop_key, SCNAS_NAS_TYPE,
			    filername);
			scconf_addmessage(errbuf, messages);
			scnaserr = SCNAS_EINVALPROP;
		}
	}
	if (scnaserr != SCNAS_NOERR) {
		return (scnaserr);
	}

	/* Initialzie table contents */
	index_table.n_rows = 0;
	filer_table.n_rows = 0;
	m_index = 0;

	/* Check if the filer already exist */
	if (scnas_chk_nas_name(filername, filer_table_name)) {
		scnaserr = SCNAS_EEXIST;
		goto cleanup;
	}

	/* Validate the nas device */
	scnaserr = nas_validate_filer(filername, messages);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Read the index table */
	scnaserr = scnas_read_filer_table(NAS_SERVICE_KEY_TABLE,
	    NULL, NULL, &index_table, GET_TABLE_CONTENTS);
	if (scnaserr != SCNAS_NOERR) {
		if (scnaserr == SCNAS_ENOEXIST) {
			create_key_table = B_TRUE;
		} else {
			goto cleanup;
		}
	}

	/* Get the table name for this filer */
	if (create_key_table) {
		the_index = 1;

		/* Allocate memory for index table */
		index_table.table_rows = (cl_query_table_elem_t **)
		    calloc(1, sizeof (cl_query_table_elem_t));
		if (index_table.table_rows ==
		    (cl_query_table_elem_t **)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	} else {
		if (index_table.n_rows) {
			index_nums = scnas_get_indexes(&index_table,
			    KEY_INDEX_FORM, &m_index);
			if (index_nums == NULL || !m_index) {
				scnaserr = SCNAS_EUNEXPECTED;
				goto cleanup;
			}

			/* Find the first available index */
			the_index = scnas_next_index(0, m_index, index_nums);
		} else {
			the_index = 1;
		}
	}

	/* Prepare the index table contents to update */
	(void) sprintf(index_key, "%d", the_index);
	scnaserr = scnas_clquery_add_elem(&index_table,
	    index_key, SCNAS_NAS_TYPE);

	/* Allocate memory for the filer table */
	filer_table.table_rows = (cl_query_table_elem_t **)
	    calloc(1, sizeof (cl_query_table_elem_t));
	if (filer_table.table_rows ==
	    (cl_query_table_elem_t **)0) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	/* Set the filer name */
	scnaserr = scnas_clquery_add_elem(&filer_table,
	    PROP_FILER_NAME, filername);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Set filer type */
	scnaserr = scnas_clquery_add_elem(&filer_table,
	    PROP_FILER_TYPE, SCNAS_NAS_TYPE);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Get the filer table name */
	(void) sprintf(filer_table_name, "%s_%d",
	    NAS_SERVICE_TABLE_PRE, the_index);

	/*
	 * Write the ccr tables. Since there are two tables
	 * to write, FBC1255 team decided to create the
	 * filer_table first, and then update the index table.
	 */

	/* Create the filer table */
	cl_err = cl_query_create_table(filer_table_name,
	    &filer_table);

	scnaserr = scnas_conv_clquery_err(cl_err);
	if (scnaserr == SCNAS_NOERR) {
		create_filer_table = B_TRUE;
	} else if (scnaserr == SCNAS_EEXIST) {

		/*
		 * Likely this is an orphan table. So remove it
		 * and try again.
		 */
		cl_err = cl_query_delete_table(filer_table_name);

		if (cl_err != CL_QUERY_OK) {
			scnaserr = scnas_conv_clquery_err(cl_err);

			/* Add error messages */
			if (messages != (char **)0) {
				(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				    "Cannot remove an existing service table "
				    "to create NAS device \"%s\".\n"),
				    filername);
				scconf_addmessage(errbuf, messages);
			}
			goto cleanup;
		}

		/* Try again. */
		cl_err = cl_query_create_table(filer_table_name,
		    &filer_table);
		scnaserr = scnas_conv_clquery_err(cl_err);
		if (scnaserr == SCNAS_NOERR) {
			create_filer_table = B_TRUE;
		} else if (messages != (char **)0) {

			/* Add error messages */
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot create the service table for NAS "
			    "device \"%s\".\n"), filername);
			scconf_addmessage(errbuf, messages);

			goto cleanup;
		}
	} else if (messages != (char **)0) {

		/* Add error messages */
		(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
		    "Cannot create the service table for NAS "
		    "device \"%s\".\n"), filername);
		scconf_addmessage(errbuf, messages);

		goto cleanup;
	}

	/* Write the index table */
	if (create_key_table) {
		/* New index table */
		cl_err = cl_query_create_table(NAS_SERVICE_KEY_TABLE,
		    &index_table);
		scnaserr = scnas_conv_clquery_err(cl_err);

		/* Add error messages */
		if ((scnaserr != SCNAS_NOERR) &&
		    (messages != (char **)0)) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot create the index table "
			    "for NAS device \"%s\".\n"), filername);
			scconf_addmessage(errbuf, messages);
		}
	} else {
		/* Update existing index table */
		cl_err = cl_query_write_table_all(
		    NAS_SERVICE_KEY_TABLE, &index_table);
		scnaserr = scnas_conv_clquery_err(cl_err);

		/* Add error messages */
		if ((scnaserr != SCNAS_NOERR) &&
		    (messages != (char **)0)) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot update the index table "
			    "for NAS device \"%s\".\n"), filername);
			scconf_addmessage(errbuf, messages);
		}
	}

	/* If error, reverse the filer table creation */
	if (scnaserr != SCNAS_NOERR) {

		/* Remove the filer table, if created */
		if (create_filer_table) {
			cl_err = cl_query_delete_table(
			    filer_table_name);

			if ((cl_err != CL_QUERY_OK) &&
			    (messages != (char **)0)) {

				/* Add error messages */
				(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				    "Cannot remove the service table for "
				    "NAS device \"%s\".\n"), filername);
				scconf_addmessage(errbuf, messages);
				goto cleanup;
			}
		}

		goto cleanup;
	}

cleanup:

	/* Free memory */
	if (index_nums) {
		free(index_nums);
	}

	cl_err = cl_query_free_table(&index_table);
	if (cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

	cl_err = cl_query_free_table(&filer_table);
	if (cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

	return (scnaserr);
}

/*
 * scnas_lib_remove_filer
 *
 *	Detach a filer from cluster configuration.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_EDIREXIST		- still has directories
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnas_lib_remove_filer(char *filername, char *filer_table_name,
    scnas_filer_prop_t *props, char **messages, uint_t flag)
{
	return (scnas_remove_filer_table(filername, filer_table_name,
	    SCNAS_NAS_TYPE, props, messages, flag));
}

/*
 * scnas_lib_set_filer_prop
 *
 *	Sun NAS does not have any properties.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- object not found
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- not enough memory
 *	SCNAS_EUNEXPECTED	- internal or unexpected error
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnas_lib_set_filer_prop(char *filername, char *filer_table_name,
    scnas_filer_prop_t *props, char **messages)
{
	/* Must be root */
	if (getuid() != 0) {
		return (SCNAS_EPERM);
	}

	/* Check arguments */
	if (!filername || !filer_table_name || props || !messages) {
		return (SCNAS_EINVAL);
	}

	return (SCNAS_NOERR);
}

/*
 * scnasdir_lib_add_dir
 *
 *	Add directories into an existing filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- no permission
 *	SCNAS_EINVAL		- invalid arguments
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 *	SCNAS_EINVALPROP	- invalid filer property
 */
scnas_errno_t
scnasdir_lib_add_dir(char *filername, char *filer_table_name,
    scnas_filer_prop_t *prop_list, char **messages,
    char *dir_all, uint_t uflg)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	cl_query_error_t cl_err = CL_QUERY_OK;
	cl_query_table_t filer_table;
	uint_t *index_nums = NULL;
	uint_t m_index;
	uint_t the_index;
	char *ccr_filer_name;
	scnas_filer_prop_t *prop;
	scnas_filer_prop_t *new_props = (scnas_filer_prop_t *)0;
	uint_t props_num;
	uint_t old_nrows;
	char dir_key[SCNAS_MAXSTRINGLEN];
	cl_query_table_elem_t *new_row = (cl_query_table_elem_t *)0;
	uint_t i;
	char errbuf[SCNAS_MAXSTRINGLEN];
	uint_t dir_count = 0;

	/* Check arguments */
	if (!filername || !filer_table_name ||
	    !prop_list) {
		return (SCNAS_EINVAL);
	}

	if (dir_all != NULL && *dir_all != '\0') {
		return (SCNAS_EUNEXPECTED);
	}

	/* Initialzie */
	filer_table.n_rows = 0;
	the_index = 0;
	props_num = 0;

	/* Read the table */
	scnaserr = scnas_read_filer_table(filer_table_name,
	    NULL, NULL, &filer_table, GET_TABLE_CONTENTS);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	old_nrows = filer_table.n_rows;
	i = old_nrows;

	/* Check the filer name */
	ccr_filer_name = scnas_clquery_find_by_key(&filer_table,
	    PROP_FILER_NAME, NULL);
	if (strcmp(filername, ccr_filer_name) != 0) {
		scnaserr = SCNAS_EUNEXPECTED;
		goto cleanup;
	}

	/* Verify the directory config on the filer */
	scnaserr = nas_validate_filerdir(filername, prop_list,
	    &new_props, &props_num, messages);
	if (!new_props || !props_num) {
		goto cleanup;
	}

	/* Only check the properties */
	if (uflg)
		goto cleanup;

	/* Re-allocate memory for the table */
	filer_table.table_rows = (cl_query_table_elem_t **)
	    realloc(filer_table.table_rows,
	    sizeof (cl_query_table_elem_t) * (old_nrows + props_num));
	if (filer_table.table_rows ==
	    (cl_query_table_elem_t **)0) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	/* Get the indexes */
	index_nums = scnas_get_indexes(&filer_table,
	    DIR_INDEX_FORM, &m_index);

	/* Go through the directories */
	for (prop = new_props; prop; prop = prop->scnas_prop_next) {
		/* Skip */
		if (!(prop->scnas_prop_key) ||
		    !(prop->scnas_prop_value))
			continue;

		if (scnas_clquery_find_by_val(&filer_table,
		    prop->scnas_prop_value, old_nrows, NULL)
		    != NULL) {
			/* Already exists */
			if (messages != (char **)0) {
				(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				    "Directory \"%s\" is already added to "
				    "NAS device \"%s\".\n"),
				    prop->scnas_prop_value, filername);
				scconf_addmessage(errbuf, messages);
			}
			if (scnaserr == SCNAS_NOERR) {
				scnaserr = SCNAS_EEXIST;
			}
			continue;
		}

		/* Next index to use */
		if (the_index < m_index) {
			the_index = scnas_next_index(the_index,
			    m_index, index_nums);
		} else {
			the_index++;
		}

		/* Allocate memory for the new element */
		new_row = (cl_query_table_elem_t *)calloc(1,
		    sizeof (cl_query_table_elem_t));
		if (new_row == (cl_query_table_elem_t *)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Add the directory property */
		(void) sprintf(dir_key, "%s_%d",
		    PROP_DIRECTORY, the_index);

		if ((new_row->key = strdup(dir_key)) == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		if ((new_row->value = strdup(prop->scnas_prop_value))
		    == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Add the new row into table */
		filer_table.table_rows[i++] = &(*new_row);

		/* Count the directories */
		dir_count++;
	}

	/* Any valid directories to add? */
	if (!dir_count) {
		goto cleanup;
	}

	/* Check the size */
	filer_table.n_rows = i;

	if (i != (old_nrows + props_num)) {
		filer_table.table_rows = (cl_query_table_elem_t **)
		    realloc(filer_table.table_rows,
		    sizeof (cl_query_table_elem_t) * i);

		if (filer_table.table_rows ==
		    (cl_query_table_elem_t **)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	}

	/* Write the table */
	cl_err = cl_query_write_table_all(filer_table_name,
	    &filer_table);

	if (cl_err != CL_QUERY_OK) {
		scnaserr = scnas_conv_clquery_err(cl_err);

		/* Add error messages */
		if ((scnaserr != SCNAS_NOERR) &&
		    (messages != (char **)0)) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot add directories to NAS device "
			    "\"%s\".\n"), filername);
			scconf_addmessage(errbuf, messages);
		}
	}

cleanup:
	/* Free memory */
	if (index_nums) {
		free(index_nums);
	}

	cl_err = cl_query_free_table(&filer_table);
	if (cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

	if (new_row) {
		if (new_row->key)
			free(new_row->key);
		if (new_row->value)
			free(new_row->value);
		free(new_row);
	}

	if (new_props) {
		scnas_free_proplist(new_props);
	}

	return (scnaserr);
}

/*
 * scnasdir_lib_rm_dir
 *
 *	Remove directories from an existing filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- no permission
 *	SCNAS_EINVAL		- invalid arguments
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 *	SCNAS_EINVALPROP	- invalid filer property
 */
scnas_errno_t
scnasdir_lib_rm_dir(char *filername, char *filer_table_name,
    scnas_filer_prop_t *prop_list, char **messages,
    char *dir_all, uint_t uflg)
{
	return (scnas_remove_filer_dir(filername, filer_table_name,
	    prop_list, messages, dir_all, uflg));
}

/*
 * is_sun_NAS_usable
 *
 *      Check if sun NAS support is available.
 *
 * Return:
 *      B_TRUE          Sun NAS is supported
 *      B_FALSE         Sun NAS isn't available
 */
static boolean_t
is_sun_NAS_usable(void) {
	clcomm_vp_version_t sun_nas_version;

	if (clcomm_get_running_version("scnas",
	    &sun_nas_version) != 0) {
		return (B_FALSE);
	}

	return ((boolean_t)(sun_nas_version.major_num >= 3));
}

/*
 * nas_validate_filer
 *
 *	Verify the nas device is valid.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_INVAL		- invalid arguments
 *	SCNAS_ENOEXIST		- NAS device does not exist
 *	SCNAS_EUNEXPECTED	- internal error
 */
static scnas_errno_t
nas_validate_filer(char *filer_name, char **messages)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	char *nodename = NULL;
	char buffer[SCNAS_MAXSTRINGLEN];
	char nas_cmd[SCNAS_MAXSTRINGLEN];
	char output_buf[SCNAS_MAXSTRINGLEN];
	int nas_err = 0;
	scconf_nodeid_t node_id;
	FILE *fd_p;

	/* Check args */
	if (!filer_name) {
		return (SCNAS_EINVAL);
	}

	/*
	 * Verify the NAS device by calling the "fence" command at the
	 * device. The fence command expects directories to be specified.
	 * So if it gives error of "ERROR: Export path not specified"
	 * then it is valid.
	 */

	/* Get this nodename */
	if (scconf_get_nodeid(NULL, &node_id) != SCCONF_NOERR) {
		return (SCNAS_EUNEXPECTED);
	}
	if (scconf_get_nodename(node_id, &nodename) != SCCONF_NOERR) {
		return (SCNAS_EUNEXPECTED);
	}

	/* Call the fence command */
	(void) sprintf(nas_cmd, "/usr/bin/rsh %s fence -c join -h %s 2>&1",
	    filer_name, nodename);

	if ((fd_p = popen(nas_cmd, "r")) == NULL) {
		free(nodename);
		return (SCNAS_EUNEXPECTED);
	}

	/* Read the output */
	*output_buf = '\0';
	(void) fgets(output_buf, BUFSIZ, fd_p);
	nas_err = pclose(fd_p);

	if (nas_err != 0) {
		/* Cannot rsh to the device */
		scnas_err = SCNAS_ENOEXIST;
		if (strncmp(output_buf, "permission denied", 17) == 0) {
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "\"%s\" is not a \"%s\" NAS device.\n"),
				    filer_name, SCNAS_NAS_TYPE);
				scconf_addmessage(buffer, messages);
			}
		} else {
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Cannot connect to NAS device \"%s\".\n"),
				    filer_name);
				scconf_addmessage(buffer, messages);
				scconf_addmessage(dgettext(TEXT_DOMAIN, "The "
				    "device name must be correct, and it "
				    "must be setup for HTTP and rsh access.\n"),
				    messages);
			}
		}
	} else if (strncmp(output_buf, "ERROR: Export path not specified",
	    32) == 0) {
		/* Expected. Valid nas device */
		scnas_err = SCNAS_NOERR;
	} else if (strncmp(output_buf, "ERROR: Cannot find node", 23) == 0) {
		/*
		 * rsh to the device but there is setup problem, and
		 * this is the only setup problem reported by "fence".
		 */
		scnas_err = SCNAS_ESETUP;
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "Cannot resolve node \"%s\" to a valid IP address "
			    "on NAS device \"%s\".\n"),
			    nodename, filer_name);
			scconf_addmessage(buffer, messages);
		}
	} else if (strncmp(output_buf, "ERROR: ", 7) == 0) {
		/* Other errors are internal NAS error */
		scnas_err = SCNAS_EUNEXPECTED;
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "Unexpected error on NAS device \"%s\".\n"),
			    filer_name);
			scconf_addmessage(buffer, messages);
		}
	} else {
		/* Not a nas device */
		scnas_err = SCNAS_ENOEXIST;
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "\"%s\" is not a \"%s\" NAS device.\n"),
			    filer_name, SCNAS_NAS_TYPE);
			scconf_addmessage(buffer, messages);
		}
	}

	free(nodename);
	return (scnas_err);
}

/*
 * nas_validate_filerdir
 *
 *	Verify the exported directories on the given filer, by calling
 *	Netapp binary NTAPfence.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_INVAL		- invalid arguments
 *	SCNAS_EINVALPROP	- invalid filer property
 *	SCNAS_EUNEXPECTED	- internal error
 */
static scnas_errno_t
nas_validate_filerdir(char *filer_name,
    scnas_filer_prop_t *prop_list, scnas_filer_prop_t **new_props,
    uint_t *length, char **messages)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	char nas_cmd[SCNAS_MAXSTRINGLEN];
	char buffer[SCNAS_MAXSTRINGLEN];
	scnas_filer_prop_t *node_list_p, *prop;
	scnas_filer_prop_t *node_list = (scnas_filer_prop_t *)0;
	FILE *fd_p;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_iter_t *currnodesi;
	clconf_obj_t *cl_node;
	const char *conf_val;
	char directory[SCNAS_MAXSTRINGLEN], node[SCNAS_MAXSTRINGLEN];
	scnas_filer_prop_t *nas_list = (scnas_filer_prop_t *)0;
	scnas_filer_prop_t *nas_list_p;
	boolean_t found;
	boolean_t invalid;
	scnas_filer_prop_t *t_new_props = (scnas_filer_prop_t *)0;
	scnas_filer_prop_t **new_prop;

	/* Check args */
	if (!filer_name || !prop_list || !new_props) {
		return (SCNAS_EINVAL);
	}

	if (length != NULL) {
		*length = 0;
	}

	/* Call the "approve list" command */
	(void) sprintf(nas_cmd,
	    "/usr/bin/rsh %s approve list | /usr/bin/grep '^files[ 	]'",
	    filer_name);
	if ((fd_p = popen(nas_cmd, "r")) == NULL) {
		return (SCNAS_EUNEXPECTED);
	}

	/* Get clconf handler */
	if (scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf) !=
	    SCCONF_NOERR) {
		(void) pclose(fd_p);
		return (SCNAS_EUNEXPECTED);
	}

	/* Read the nodes */
	currnodesi = clconf_cluster_get_nodes(clconf);
	while ((cl_node = clconf_iter_get_current(currnodesi)) != NULL) {
		clconf_iter_advance(currnodesi);

		/* Get the node name */
		conf_val = clconf_obj_get_name(cl_node);
		if (!conf_val) {
			continue;
		}

		/* Add this node to the node list */
		scnas_err = scnas_add_one_prop(conf_val, NULL, &node_list);
		if (scnas_err != SCNAS_NOERR) {
			clconf_obj_release((clconf_obj_t *)clconf);
			(void) pclose(fd_p);
			scnas_free_proplist(node_list);
			return (scnas_err);
		}
	}

	/* Release the cluster config */
	clconf_obj_release((clconf_obj_t *)clconf);

	/* Read the output */
	*buffer = '\0';
	while (fgets(buffer, SCNAS_MAXSTRINGLEN, fd_p) != NULL) {
		/* Find the directory and node */
		(void) sscanf(buffer, "files %s %s",
		    directory, node);

		/* Add to the directories and nodes in nas_list */
		scnas_err = scnas_add_one_prop(directory, node, &nas_list);
		if (scnas_err != SCNAS_NOERR) {
			scnas_free_proplist(node_list);
			scnas_free_proplist(nas_list);
			(void) pclose(fd_p);
			return (scnas_err);
		}
	}
	(void) pclose(fd_p);

	/* Go through the directories saved in prop_list */
	new_prop = &t_new_props;
	for (prop = prop_list; prop; prop = prop->scnas_prop_next) {
		/* Skip */
		if (!(prop->scnas_prop_key) ||
		    !(prop->scnas_prop_value))
			continue;

		/* Check if the directory is in nas_list */
		found = B_FALSE;
		for (nas_list_p = nas_list; nas_list_p;
		    nas_list_p = nas_list_p->scnas_prop_next) {
			if (strcmp(prop->scnas_prop_value,
			    nas_list_p->scnas_prop_key) == 0) {
				found = B_TRUE;
				break;
			}
		}
		if (!found) {
			if (scnas_err == SCNAS_NOERR) {
				scnas_err = SCNAS_EINVALPROP;
			}
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Cannot find directory \"%s\" in the "
				    "configuration on NAS device \"%s\".\n"),
				    prop->scnas_prop_value, filer_name);
				scconf_addmessage(buffer, messages);
			}
			continue;
		}

		/* Check if all the nodes are set for this directory */
		invalid = B_FALSE;
		for (node_list_p = node_list; node_list_p;
		    node_list_p = node_list_p->scnas_prop_next) {
			found = B_FALSE;
			for (nas_list_p = nas_list; nas_list_p;
			    nas_list_p = nas_list_p->scnas_prop_next) {
				/* Check the directory first */
				if (strcmp(prop->scnas_prop_value,
				    nas_list_p->scnas_prop_key) != 0) {
					continue;
				}

				/* Check the node */
				if (strcmp(node_list_p->scnas_prop_key,
				    nas_list_p->scnas_prop_value) == 0) {
					found = B_TRUE;
					break;
				}
			}

			/* This node is not configured for the directory */
			if (!found) {
				if (scnas_err == SCNAS_NOERR) {
					scnas_err = SCNAS_EINVALPROP;
				}
				if (messages != (char **)0) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Node \"%s\" is not configured to "
					    "access directory \"%s\" on "
					    "NAS device \"%s\".\n"),
					    node_list_p->scnas_prop_key,
					    prop->scnas_prop_value,
					    filer_name);
					scconf_addmessage(buffer, messages);
				}

				invalid = B_TRUE;
			}
		}

		/* Skip if invalid */
		if (invalid) {
			continue;
		}

		/* This is a valid directory, add to the new list */
		*new_prop = (scnas_filer_prop_t *)calloc(1,
		    sizeof (scnas_filer_prop_t));
		if (*new_prop == (scnas_filer_prop_t *)0) {
			if (scnas_err == SCNAS_NOERR) {
				scnas_err = SCNAS_ENOMEM;
			}
			scnas_free_proplist(node_list);
			scnas_free_proplist(nas_list);
			scnas_free_proplist(t_new_props);
			return (scnas_err);
		}

		(*new_prop)->scnas_prop_key = strdup(prop->scnas_prop_key);
		(*new_prop)->scnas_prop_value = strdup(prop->scnas_prop_value);
		if (!(*new_prop)->scnas_prop_key ||
		    !(*new_prop)->scnas_prop_value) {
			if (scnas_err == SCNAS_NOERR) {
				scnas_err = SCNAS_ENOMEM;
			}
			scnas_free_proplist(node_list);
			scnas_free_proplist(nas_list);
			scnas_free_proplist(t_new_props);
			return (scnas_err);
		}

		/* Count this directory */
		if (length != NULL) {
			(*length)++;
		}

		/* Next */
		new_prop = &(*new_prop)->scnas_prop_next;
	}

	/* Set the new list */
	*new_props = t_new_props;

	/* Free memory */
	scnas_free_proplist(node_list);
	scnas_free_proplist(nas_list);

	return (scnas_err);
}
