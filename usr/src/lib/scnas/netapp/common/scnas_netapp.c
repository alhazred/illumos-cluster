/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scnas_netapp.c	1.8	08/05/20 SMI"

/*
 * scnas_netapp.so NetApp NAS filer config functions
 */

#include "../../scnas_common.h"
#include <clcomm_cluster_vm.h>
#include <wait.h>

#define	SCNAS_NAS_TYPE		"netapp"
#define	NETAPP_BINARY		"/usr/sbin/NTAPfence"
#define	NAS_NEW_FORM		1

/*
 * !!! Following errors correspond to NTAPfence exit codes. Do
 *     NOT change unless Netapp changes them.
 */
#define	NTAP_EFILER		2	/* cannot connect to filer */
#define	NTAP_EAUTH		3	/* Authorization failed */
#define	NTAP_ENOEXIST_DIR	4	/* Directory not available */
#define	NTAP_EDIR_RW		5	/* Directory has "rw" wildcard */
#define	NTAP_EDIR_RO		6	/* Directory has "ro" wildcard */

static boolean_t is_netapp_NAS_usable(void);
static boolean_t is_valid_filer_prop(char *prop);

static char *nas_get_ccr_prop_name(char *prop_key);
static scnas_errno_t nas_get_props_no_dup(
    scnas_filer_prop_t *old_list, scnas_filer_prop_t **new_list,
    uint_t *length, uint_t flg, char **messages);
static scnas_errno_t nas_validate_filerprop(char *filer_name,
    scnas_filer_prop_t *props, char **messages);
static scnas_errno_t nas_validate_filerdir(char *filer_name,
    cl_query_table_t *filer_table, scnas_filer_prop_t *props,
    scnas_filer_prop_t **new_props, uint_t *length, char **messages);

/*
 * scnas_chk_nas_type
 *
 *	This function confirms the NAS type.
 *
 * Return values:
 *
 *	boolean_t
 */
boolean_t
scnas_chk_nas_type(const char *scnas_type, boolean_t *props_required)
{
	/* NetApp NAS requires properties */
	if (props_required) {
		*props_required = B_TRUE;
	}

	/* Check to enaure the device type is NetApp NAS */
	if (scnas_type && (strcmp(scnas_type, SCNAS_NAS_TYPE) == 0)) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}

/*
 * scnas_chk_nas_name
 *
 *	Confirm if the given NAS filer is of the type of the
 *	library that calls this function. Returns the CCR
 *	table name in table_name if the table exists.
 *
 * Return values:
 *	boolean_t
 */
boolean_t
scnas_chk_nas_name(const char *scnas_filer, char *table_name)
{
	scnas_errno_t rstatus = SCNAS_NOERR;
	cl_query_table_t index_table;
	cl_query_table_t filer_table;
	boolean_t found = B_FALSE;
	uint_t i;
	char filer_table_name[SCNAS_MAXSTRINGLEN];

	/* Check the arguments */
	if (!scnas_filer || table_name == NULL) {
		return (B_FALSE);
	}

	/* Initialize */
	index_table.n_rows = 0;
	filer_table.n_rows = 0;

	/* Get the index table */
	rstatus = scnas_read_filer_table(NAS_SERVICE_KEY_TABLE,
	    NULL, NULL, &index_table, GET_TABLE_CONTENTS);
	if (rstatus != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Find the filer table from the index table and read */
	for (i = 0; i < index_table.n_rows; i++) {
		if (!(index_table.table_rows[i]->key) ||
		    !(index_table.table_rows[i]->value))
			continue;

		if (strcmp(index_table.table_rows[i]->value,
		    SCNAS_NAS_TYPE) != 0)
			continue;

		/* Get the filer table name */
		(void) sprintf(filer_table_name, "%s_%s",
		    NAS_SERVICE_TABLE_PRE,
		    index_table.table_rows[i]->key);

		/* Read this filer table */
		rstatus = scnas_read_filer_table(filer_table_name,
		    PROP_FILER_NAME, (char *)scnas_filer,
		    &filer_table, GET_TABLE_BY_KEY);
		if ((rstatus == SCNAS_NOERR) &&
		    (filer_table.n_rows > 0)) {
			found = B_TRUE;
			(void) strcpy(table_name, filer_table_name);
			break;
		} else {
			if (cl_query_free_table(&filer_table)
			    != CL_QUERY_OK) {
				found = B_FALSE;
				goto cleanup;
			}
		}
	}

cleanup:
	/* Release the memory allocated for the table */
	if (cl_query_free_table(&index_table) != CL_QUERY_OK) {
		found = B_FALSE;
	}

	if (cl_query_free_table(&filer_table) != CL_QUERY_OK) {
		found = B_FALSE;
	}

	return (found);
}

/*
 * scnas_chk_prop_required
 *
 *	Confirm if the given NAS filer is of the type of the
 *	library that calls this function, and sets whether
 *	this type requires property in the 2nd parameter.
 *
 * Return values:
 *
 *	boolean_t
 */
boolean_t
scnas_chk_prop_required(const char *scnas_filer, boolean_t *prop_required)
{
	boolean_t chk_name;
	char filer_table_name[SCNAS_MAXSTRINGLEN];

	/* Check if the given nas filer is valid */
	chk_name = scnas_chk_nas_name(scnas_filer, filer_table_name);
	if (prop_required) {
		*prop_required = B_FALSE;
		if (chk_name) {
			*prop_required = B_TRUE;
		}
	}

	return (chk_name);
}

/*
 * scnas_lib_attach_filer
 *
 *	Attach the given "filername" to cluster configuration if
 *	the filer is not already attached, along with its properties.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- no permission
 *	SCNAS_EINVAL		- invalid arguments
 *	SCNAS_EEXIST		- filer is already attached
 *	SCNAS_EUNKNOWN		- unknow type
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_VP_MISMATCH	- unmatched running version
 *	SCNAS_NO_BINARY		- required binary does not exist
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnas_lib_attach_filer(char *filername,
    scnas_filer_prop_t *prop_list, char **messages)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	struct stat stat_buf;
	char filer_table_name[SCNAS_MAXSTRINGLEN];
	cl_query_table_t filer_table;
	cl_query_table_t index_table;
	cl_query_error_t cl_err = CL_QUERY_OK;
	scnas_filer_prop_t *new_proplist = (scnas_filer_prop_t *)0;
	uint_t *index_nums = NULL;
	uint_t m_index;
	uint_t the_index;
	uint_t props_num;
	boolean_t create_key_table = B_FALSE;
	boolean_t create_filer_table = B_FALSE;
	char index_key[SCNAS_MAXSTRINGLEN];
	char errbuf[SCNAS_MAXSTRINGLEN];

	/* Must be root */
	if (getuid() != 0) {
		return (SCNAS_EPERM);
	}

	/* Check arguments */
	if (!filername || !prop_list) {
		return (SCNAS_EINVAL);
	}

	/* Check if NAS version is valid */
	if (!is_netapp_NAS_usable()) {
		return (SCNAS_VP_MISMATCH);
	}

	/* Check if the Netapp binary exists */
	if ((stat(NETAPP_BINARY, &stat_buf) != 0) ||
	    (access(NETAPP_BINARY, X_OK) != 0)) {
		return (SCNAS_NO_BINARY);
	}

	/*
	 * We are root, so need to check execute permissions
	 * because access(2) will return success for X_OK even if
	 * none of the execute file permission bits are set.
	 */
	if ((stat_buf.st_mode & (S_IXUSR|S_IXGRP|S_IXOTH)) == 0) {
		return (SCNAS_NO_BINARY);
	}

	/* Initialzie table contents */
	index_table.n_rows = 0;
	filer_table.n_rows = 0;
	m_index = 0;

	/* Check if the filer already exist */
	if (scnas_chk_nas_name(filername, filer_table_name)) {
		scnaserr = SCNAS_EEXIST;
		goto cleanup;
	}

	/* Process the properties */
	scnaserr = nas_get_props_no_dup(prop_list,
	    &new_proplist, &props_num, NAS_NEW_FORM, messages);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Verify the properties */
	scnaserr = nas_validate_filerprop(filername, new_proplist,
	    messages);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Read the index table */
	scnaserr = scnas_read_filer_table(NAS_SERVICE_KEY_TABLE,
	    NULL, NULL, &index_table, GET_TABLE_CONTENTS);
	if (scnaserr != SCNAS_NOERR) {
		if (scnaserr == SCNAS_ENOEXIST) {
			create_key_table = B_TRUE;
		} else {
			goto cleanup;
		}
	}

	/* Get the table name for this filer */
	if (create_key_table) {
		the_index = 1;

		/* Allocate memory for index table */
		index_table.table_rows = (cl_query_table_elem_t **)
		    calloc(1, sizeof (cl_query_table_elem_t));
		if (index_table.table_rows ==
		    (cl_query_table_elem_t **)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	} else {
		if (index_table.n_rows) {
			index_nums = scnas_get_indexes(&index_table,
			    KEY_INDEX_FORM, &m_index);
			if (index_nums == NULL || !m_index) {
				scnaserr = SCNAS_EUNEXPECTED;
				goto cleanup;
			}

			/* Find the first available index */
			the_index = scnas_next_index(0, m_index, index_nums);
		} else {
			the_index = 1;
		}
	}

	/* Prepare the index table contents to update */
	(void) sprintf(index_key, "%d", the_index);
	scnaserr = scnas_clquery_add_elem(&index_table,
	    index_key, SCNAS_NAS_TYPE);

	/* Allocate memory for the filer table */
	filer_table.table_rows = (cl_query_table_elem_t **)
	    calloc(1, sizeof (cl_query_table_elem_t));
	if (filer_table.table_rows ==
	    (cl_query_table_elem_t **)0) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	/* Set the filer name */
	scnaserr = scnas_clquery_add_elem(&filer_table,
	    PROP_FILER_NAME, filername);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Set filer type */
	scnaserr = scnas_clquery_add_elem(&filer_table,
	    PROP_FILER_TYPE, SCNAS_NAS_TYPE);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Add the filer properties to filer contents */
	scnaserr = scnas_clquery_add_props(&filer_table,
	    new_proplist, props_num);
	if (scnaserr != SCNAS_NOERR)
		goto cleanup;

	/* Get the filer table name */
	(void) sprintf(filer_table_name, "%s_%d",
	    NAS_SERVICE_TABLE_PRE, the_index);

	/*
	 * Write the ccr tables. Since there are two tables
	 * to write, FBC1255 team decided to create the
	 * filer_table first, and then update the index table.
	 */

	/* Create the filer table */
	cl_err = cl_query_create_table(filer_table_name,
	    &filer_table);

	scnaserr = scnas_conv_clquery_err(cl_err);
	if (scnaserr == SCNAS_NOERR) {
		create_filer_table = B_TRUE;
	} else if (scnaserr == SCNAS_EEXIST) {

		/*
		 * Likely this is an orphan table. So remove it
		 * and try again.
		 */
		cl_err = cl_query_delete_table(filer_table_name);

		if (cl_err != CL_QUERY_OK) {
			scnaserr = scnas_conv_clquery_err(cl_err);

			/* Add error messages */
			if (messages != (char **)0) {
				(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				    "Cannot remove an existing CCR table "
				    "to create NAS device \"%s\".\n"),
				    filername);
				scconf_addmessage(errbuf, messages);
			}
			goto cleanup;
		}

		/* Try again. */
		cl_err = cl_query_create_table(filer_table_name,
		    &filer_table);
		scnaserr = scnas_conv_clquery_err(cl_err);
		if (scnaserr == SCNAS_NOERR) {
			create_filer_table = B_TRUE;
		} else if (messages != (char **)0) {

			/* Add error messages */
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot create CCR table for NAS "
			    "device \"%s\".\n"), filername);
			scconf_addmessage(errbuf, messages);

			goto cleanup;
		}
	} else if (messages != (char **)0) {

		/* Add error messages */
		(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
		    "Cannot create CCR table for NAS "
		    "device \"%s\".\n"), filername);
		scconf_addmessage(errbuf, messages);

		goto cleanup;
	}

	/* Write the index table */
	if (create_key_table) {
		/* New index table */
		cl_err = cl_query_create_table(NAS_SERVICE_KEY_TABLE,
		    &index_table);
		scnaserr = scnas_conv_clquery_err(cl_err);

		/* Add error messages */
		if ((scnaserr != SCNAS_NOERR) &&
		    (messages != (char **)0)) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot create the index table "
			    "for NAS device \"%s\".\n"), filername);
			scconf_addmessage(errbuf, messages);
		}
	} else {
		/* Update existing index table */
		cl_err = cl_query_write_table_all(
		    NAS_SERVICE_KEY_TABLE, &index_table);
		scnaserr = scnas_conv_clquery_err(cl_err);

		/* Add error messages */
		if ((scnaserr != SCNAS_NOERR) &&
		    (messages != (char **)0)) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot update the index table "
			    "for NAS device \"%s\".\n"), filername);
			scconf_addmessage(errbuf, messages);
		}
	}

	/* If error, reverse the filer table creation */
	if (scnaserr != SCNAS_NOERR) {

		/* Remove the filer table, if created */
		if (create_filer_table) {
			cl_err = cl_query_delete_table(
			    filer_table_name);

			if ((cl_err != CL_QUERY_OK) &&
			    (messages != (char **)0)) {

				/* Add error messages */
				(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				    "Cannot remove the CCR table for "
				    "NAS device \"%s\".\n"), filername);
				scconf_addmessage(errbuf, messages);
				goto cleanup;
			}
		}

		goto cleanup;
	}

cleanup:

	/* Free memory */
	if (index_nums) {
		free(index_nums);
	}

	if (new_proplist != (scnas_filer_prop_t *)0) {
		scnas_free_proplist(new_proplist);
	}

	cl_err = cl_query_free_table(&index_table);
	if (cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

	cl_err = cl_query_free_table(&filer_table);
	if (cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

	return (scnaserr);
}

/*
 * scnas_lib_remove_filer
 *
 *	Detach a filer from cluster configuration.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_EDIREXIST		- still has directories
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnas_lib_remove_filer(char *filername, char *filer_table_name,
    scnas_filer_prop_t *props, char **messages, uint_t flag)
{
	return (scnas_remove_filer_table(filername, filer_table_name,
	    SCNAS_NAS_TYPE, props, messages, flag));
}

/*
 * scnas_lib_set_filer_prop
 *
 *	Set the property name and value from structure
 *	scnas_filer_prop_t *props for the NAS filer. Property
 *	values can only be set for a device that already exsists.
 *	If the property already exists, its value is overwritten
 *	with the value given. If the property does not exist
 *	it is created.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- object not found
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- not enough memory
 *	SCNAS_EUNEXPECTED	- internal or unexpected error
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
scnas_errno_t
scnas_lib_set_filer_prop(char *filername, char *filer_table_name,
    scnas_filer_prop_t *props, char **messages)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	cl_query_table_t filer_table;
	cl_query_error_t cl_err = CL_QUERY_OK;
	scnas_filer_prop_t *new_proplist;
	uint_t props_num;
	char *ccr_filer_name;
	char errbuf[SCNAS_MAXSTRINGLEN];

	/* Must be root */
	if (getuid() != 0) {
		return (SCNAS_EPERM);
	}

	/* Check arguments */
	if (!filername || !filer_table_name || !props) {
		return (SCNAS_EINVAL);
	}

	/* Initialzie table contents */
	filer_table.n_rows = 0;

	/* Process the properties */
	scnaserr = nas_get_props_no_dup(props,
	    &new_proplist, &props_num, NAS_NEW_FORM, messages);
	if ((scnaserr != SCNAS_NOERR) ||
	    (!new_proplist)) {
		goto cleanup;
	}

	/* Verify the properties */
	scnaserr = nas_validate_filerprop(filername, new_proplist,
	    messages);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Read the filer table */
	scnaserr = scnas_read_filer_table(filer_table_name,
	    NULL, NULL, &filer_table, GET_TABLE_CONTENTS);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Check the filer name read from the table */
	ccr_filer_name = scnas_clquery_find_by_key(&filer_table,
	    PROP_FILER_NAME, NULL);
	if (strcmp(filername, ccr_filer_name) != 0) {
		scnaserr = SCNAS_EUNEXPECTED;
		goto cleanup;
	}

	/* Set the properties */
	scnaserr = scnas_clquery_set_props(&filer_table,
	    new_proplist, props_num);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	/* Update the table */
	cl_err = cl_query_write_table_all(filer_table_name,
	    &filer_table);
	if (cl_err != CL_QUERY_OK) {
		scnaserr = scnas_conv_clquery_err(cl_err);

		/* Add error messages */
		if (messages != (char **)0) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot update the filer table for NAS "
			    "device \"%s\".\n"), filername);
			scconf_addmessage(errbuf, messages);
		}

		goto cleanup;
	}

cleanup:
	/* Free memory */
	if (new_proplist != (scnas_filer_prop_t *)0) {
		scnas_free_proplist(new_proplist);
	}

	cl_err = cl_query_free_table(&filer_table);
	if (cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

	return (scnaserr);
}

/*
 * scnasdir_lib_add_dir
 *
 *	Add directories into an existing filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- no permission
 *	SCNAS_EINVAL		- invalid arguments
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 *	SCNAS_EINVALPROP	- invalid filer property
 */
scnas_errno_t
scnasdir_lib_add_dir(char *filername, char *filer_table_name,
    scnas_filer_prop_t *prop_list, char **messages,
    char *dir_all, uint_t uflg)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	cl_query_error_t cl_err = CL_QUERY_OK;
	cl_query_table_t filer_table;
	uint_t *index_nums = NULL;
	uint_t m_index;
	uint_t the_index;
	char *ccr_filer_name;
	register scnas_filer_prop_t *prop;
	uint_t props_num;
	uint_t old_nrows;
	char dir_key[SCNAS_MAXSTRINGLEN];
	cl_query_table_elem_t *new_row = (cl_query_table_elem_t *)0;
	uint_t i;
	char errbuf[SCNAS_MAXSTRINGLEN];
	scnas_filer_prop_t *new_props = (scnas_filer_prop_t *)0;
	uint_t dir_count = 0;

	/* Must be root */
	if (getuid() != 0) {
		return (SCNAS_EPERM);
	}

	/* Check arguments */
	if (!filername || !filer_table_name ||
	    !prop_list) {
		return (SCNAS_EINVAL);
	}

	if (dir_all != NULL && *dir_all != '\0') {
		return (SCNAS_EUNEXPECTED);
	}

	/* Initialzie */
	filer_table.n_rows = 0;
	the_index = 0;
	props_num = 0;

	/* Read the table */
	scnaserr = scnas_read_filer_table(filer_table_name,
	    NULL, NULL, &filer_table, GET_TABLE_CONTENTS);
	if (scnaserr != SCNAS_NOERR) {
		goto cleanup;
	}

	old_nrows = filer_table.n_rows;
	i = old_nrows;

	/* Check the filer name */
	ccr_filer_name = scnas_clquery_find_by_key(&filer_table,
	    PROP_FILER_NAME, NULL);
	if (strcmp(filername, ccr_filer_name) != 0) {
		scnaserr = SCNAS_EUNEXPECTED;
		goto cleanup;
	}

	/* Verify the directory config on the filer */
	scnaserr = nas_validate_filerdir(filername, &filer_table,
	    prop_list, &new_props, &props_num, messages);
	if (!new_props || !props_num) {
		goto cleanup;
	}

	/* Only check the properties */
	if (uflg)
		goto cleanup;

	/* Re-allocate memory for the table */
	filer_table.table_rows = (cl_query_table_elem_t **)
	    realloc(filer_table.table_rows,
	    sizeof (cl_query_table_elem_t) * (old_nrows + props_num));
	if (filer_table.table_rows ==
	    (cl_query_table_elem_t **)0) {
		scnaserr = SCNAS_ENOMEM;
		goto cleanup;
	}

	/* Get the indexes */
	index_nums = scnas_get_indexes(&filer_table,
	    DIR_INDEX_FORM, &m_index);

	/* Go through the directories */
	for (prop = new_props; prop; prop = prop->scnas_prop_next) {
		/* Skip */
		if (!(prop->scnas_prop_key) ||
		    !(prop->scnas_prop_value))
			continue;

		if (scnas_clquery_find_by_val(&filer_table,
		    prop->scnas_prop_value, old_nrows, NULL)
		    != NULL) {
			/* Already exists */
			if (messages != (char **)0) {
				(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				    "Directory \"%s\" is already added to "
				    "filer \"%s\".\n"),
				    prop->scnas_prop_value, filername);
				scconf_addmessage(errbuf, messages);
			}
			if (scnaserr == SCNAS_NOERR) {
				scnaserr = SCNAS_EEXIST;
			}
			continue;
		}

		/* Next index to use */
		if (the_index < m_index) {
			the_index = scnas_next_index(the_index,
			    m_index, index_nums);
		} else {
			the_index++;
		}

		/* Allocate memory for the new element */
		new_row = (cl_query_table_elem_t *)calloc(1,
		    sizeof (cl_query_table_elem_t));
		if (new_row == (cl_query_table_elem_t *)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Add the directory property */
		(void) sprintf(dir_key, "%s_%d",
		    PROP_DIRECTORY, the_index);

		if ((new_row->key = strdup(dir_key)) == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		if ((new_row->value = strdup(prop->scnas_prop_value))
		    == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Add the new row into table */
		filer_table.table_rows[i++] = &(*new_row);

		/* Count the directories */
		dir_count++;
	}

	/* Any valid directories to add? */
	if (!dir_count) {
		goto cleanup;
	}

	/* Check the size */
	filer_table.n_rows = i;

	if (i != (old_nrows + props_num)) {
		filer_table.table_rows = (cl_query_table_elem_t **)
		    realloc(filer_table.table_rows,
		    sizeof (cl_query_table_elem_t) * i);

		if (filer_table.table_rows ==
		    (cl_query_table_elem_t **)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}
	}

	/* Write the table */
	cl_err = cl_query_write_table_all(filer_table_name,
	    &filer_table);

	if (cl_err != CL_QUERY_OK) {
		scnaserr = scnas_conv_clquery_err(cl_err);

		/* Add error messages */
		if ((scnaserr != SCNAS_NOERR) &&
		    (messages != (char **)0)) {
			(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
			    "Cannot add directories for filer "
			    "\"%s\".\n"), filername);
			scconf_addmessage(errbuf, messages);
		}
	}

cleanup:
	/* Free memory */
	if (index_nums) {
		free(index_nums);
	}

	cl_err = cl_query_free_table(&filer_table);
	if (cl_err != CL_QUERY_OK) {
		if (scnaserr == SCNAS_NOERR) {
			scnaserr = SCNAS_EUNEXPECTED;
		} else {
			scnaserr = scnas_conv_clquery_err(cl_err);
		}
	}

	if (new_row) {
		if (new_row->key)
			free(new_row->key);
		if (new_row->value)
			free(new_row->value);
		free(new_row);
	}

	if (new_props) {
		scnas_free_proplist(new_props);
	}

	return (scnaserr);
}

/*
 * scnasdir_lib_rm_dir
 *
 *	Remove directories from an existing filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- no permission
 *	SCNAS_EINVAL		- invalid arguments
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 *	SCNAS_EINVALPROP	- invalid filer property
 */
scnas_errno_t
scnasdir_lib_rm_dir(char *filername, char *filer_table_name,
    scnas_filer_prop_t *prop_list, char **messages,
    char *dir_all, uint_t uflg)
{
	return (scnas_remove_filer_dir(filername, filer_table_name,
	    prop_list, messages, dir_all, uflg));
}


/*
 * is_netapp_NAS_usable
 *
 *	Check if Netapp NAS support is available.
 *
 * Return:
 *	B_TRUE		Netapp NAS is supported
 *	B_FALSE		Netapp NAS isn't available
 */
static boolean_t
is_netapp_NAS_usable(void) {
	clcomm_vp_version_t netapp_nas_version;

	if (clcomm_get_running_version("scnas",
	    &netapp_nas_version) != 0) {
		return (B_FALSE);
	}

	return ((boolean_t)(netapp_nas_version.major_num >= 2));
}

/*
 * is_valid_filer_prop
 *
 *	Check if the property is valid for netapp NAS.
 *
 * Return:
 *	B_TRUE		property is valid
 *	B_FALSE		not valid property
 */
static boolean_t
is_valid_filer_prop(char *prop)
{
	if (!prop) {
		return (B_FALSE);
	} else if (strcmp(prop, NAS_USERID) == 0) {
		return (B_TRUE);
	} else if (strcmp(prop, NAS_PASSWD) == 0) {
		return (B_TRUE);
	}

	return (B_FALSE);
}

/*
 * nas_get_ccr_prop_name
 *
 *	For the given filer property key, returns the corresponding
 *	property name used for it in the filer's ccr table.
 */
static char *
nas_get_ccr_prop_name(char *prop_key)
{
	/* check args */
	if (!prop_key) {
		return (NULL);
	} else if (strcmp(prop_key, NAS_USERID) == 0) {
		return (PROP_USERID);
	} else if (strcmp(prop_key, NAS_PASSWD) == 0) {
		return (PROP_PASSWD);
	} else if (strcmp(prop_key, NAS_DIRECTORIES) == 0) {
		return (PROP_DIRECTORY);
	} else if (strcmp(prop_key, NAS_FILER_NAME) == 0) {
		return (PROP_FILER_NAME);
	} else if (strcmp(prop_key, NAS_FILER_TYPE) == 0) {
		return (PROP_FILER_TYPE);
	}

	return (NULL);
}

/*
 * nas_get_props_no_dup
 *
 *	For the given NAS properties list, removes the duplicated
 *	entries and the invalid entries, and create a new
 *	list. Also set the *length for the new_list.
 *
 *	Caller is responsible to free the memory allocated for
 *	the new list.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EINVALPROP	- invalid filer property
 */
static scnas_errno_t
nas_get_props_no_dup(scnas_filer_prop_t *old_list,
    scnas_filer_prop_t **new_list, uint_t *length,
    uint_t flg, char **messages)
{
	scnas_errno_t scnaserr = SCNAS_NOERR;
	register scnas_filer_prop_t *prop;
	scnas_filer_prop_t *t_new_list = (scnas_filer_prop_t *)0;
	scnas_filer_prop_t **new_prop;
	char *tmp_key = NULL;
	uint_t found;
	int invalid_keys = 0;
	char errbuf[SCNAS_MAXSTRINGLEN];

	/* Check args */
	if (new_list == NULL) {
		return (SCNAS_EINVAL);
	}

	if (old_list == NULL) {
		return (SCNAS_EINVAL);
	}

	if (length != NULL) {
		*length = 0;
	}

	/* Go through the list */
	new_prop = &t_new_list;
	for (prop = old_list; prop; prop = prop->scnas_prop_next) {

		/* Skip */
		if (!(prop->scnas_prop_key))
			continue;

		if (!is_valid_filer_prop(prop->scnas_prop_key)) {
			invalid_keys++;

			/* Add error messages */
			if (messages != (char **)0) {
				(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				    "Invalid property \"%s\".\n"),
				    prop->scnas_prop_key);
				scconf_addmessage(errbuf, messages);
			}

			continue;
		}

		if (!(prop->scnas_prop_value) ||
		    (strcmp(prop->scnas_prop_value, "") == 0)) {
			invalid_keys++;

			/* Add error messages */
			if (messages != (char **)0) {
				(void) sprintf(errbuf, dgettext(TEXT_DOMAIN,
				    "Property \"%s\" has no value.\n"),
				    prop->scnas_prop_key);
				scconf_addmessage(errbuf, messages);
			}

			continue;
		}

		/* Copy the key */
		tmp_key = nas_get_ccr_prop_name(prop->scnas_prop_key);
		if (tmp_key == NULL) {
			scnaserr = SCNAS_EUNEXPECTED;
			goto cleanup;
		}

		/* Check if already exist */
		found = NAS_NOTFOUND_KEY;
		if (flg == NAS_NEW_FORM) {
			(void) scnas_get_prop_val(t_new_list,
			    tmp_key, &found);
		} else {
			(void) scnas_get_prop_val(t_new_list,
			    prop->scnas_prop_key, &found);
		}
		if (found == NAS_FOUND_KEY)
			continue;

		/* Fresh property */
		*new_prop = (scnas_filer_prop_t *)calloc(1,
		    sizeof (scnas_filer_prop_t));
		if (*new_prop == (scnas_filer_prop_t *)0) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Copy the key */
		if (flg == NAS_NEW_FORM) {
			(*new_prop)->scnas_prop_key = strdup(tmp_key);
		} else {
			(*new_prop)->scnas_prop_key = strdup(
			    prop->scnas_prop_key);
		}
		if ((*new_prop)->scnas_prop_key == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Copy the value */
		(*new_prop)->scnas_prop_value = strdup(
		    prop->scnas_prop_value);
		if ((*new_prop)->scnas_prop_value == NULL) {
			scnaserr = SCNAS_ENOMEM;
			goto cleanup;
		}

		/* Count it */
		if (length != NULL) {
			(*length)++;
		}

		/* Next */
		new_prop = &(*new_prop)->scnas_prop_next;
	}

	/* Set the new list */
	*new_list = t_new_list;
	scnaserr = SCNAS_NOERR;

	if (invalid_keys) {
		scnaserr = SCNAS_EINVALPROP;
	}

cleanup:
	/* Free memory */
	if (scnaserr != SCNAS_NOERR) {
		if (t_new_list) {
			scnas_free_proplist(t_new_list);
		}

		if (*new_prop) {
			if ((*new_prop)->scnas_prop_key)
				free((*new_prop)->scnas_prop_key);
			if ((*new_prop)->scnas_prop_value)
				free((*new_prop)->scnas_prop_value);
			free (*new_prop);
		}
	}

	return (scnaserr);
}

/*
 * nas_validate_filerprop
 *
 *	Verify the filer properties given by scnas command, inclluding
 *	filer name, userid and password, by calling Netapp binary NTAPfence.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_INVAL		- invalid arguments
 *	SCNAS_EUSAGE		- usage error
 *	SCNAS_EINVALPROP	- invalid filer property
 *	SCNAS_EUNEXPECTED	- internal error
 */
static scnas_errno_t nas_validate_filerprop(char *filer_name,
    scnas_filer_prop_t *prop_list, char **messages)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	FILE *tmp_fp;
	char *chk_userid = NULL;
	char *chk_passwd = NULL;
	uint_t found_prop;
	char cfg_fname[SCNAS_MAXSTRINGLEN];
	char buffer[SCNAS_MAXSTRINGLEN];
	char ntap_cmd[SCNAS_MAXSTRINGLEN];
	int ntap_err = 0;
	FILE *fd_p;

	/* Check args */
	if (!filer_name || !prop_list) {
		return (SCNAS_EINVAL);
	}

	/* Get the userid property */
	found_prop = NAS_NOTFOUND_KEY;
	chk_userid = scnas_get_prop_val(prop_list, PROP_USERID,
	    &found_prop);

	if (!chk_userid) {
		/* userid is required. Give error if not present */
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "An userid must be specified to access "
			    "NAS device \"%s\".\n"),
			    filer_name);
			scconf_addmessage(buffer, messages);
		}
		return (SCNAS_EUSAGE);
	}

	/*
	 * Verify the filer properties using NTAPfence
	 */

	/* Create the temp config file. */
	(void) sprintf(cfg_fname, "/var/cluster/run/.scnas_tmp.%d", getpid());
	if ((tmp_fp = fopen(cfg_fname, "w")) == NULL) {
		return (SCNAS_EUNEXPECTED);
	}

	(void) fprintf(tmp_fp, "directories\n\t");

	/* Write the properties */
	if (chk_userid) {
		/* Get password */
		found_prop = NAS_NOTFOUND_KEY;
		chk_passwd = scnas_get_prop_val(prop_list, PROP_PASSWD,
		    &found_prop);

		if (chk_passwd) {
			/* Passwd is obfuscated. Get it back */
			scnas_rot13_alg(&chk_passwd);
			(void) fprintf(tmp_fp, "%s unexistingdir %s %s\n",
			    filer_name, chk_userid, chk_passwd);
			scnas_rot13_alg(&chk_passwd);
		} else {
			(void) fprintf(tmp_fp, "%s unexistingdir %s\n",
			    filer_name, chk_userid);
		}
	}

	(void) fclose(tmp_fp);

	/* Generate and call NTAPfence command */
	(void) sprintf(ntap_cmd, "%s -V -i %s 2>&1",
	    NETAPP_BINARY, cfg_fname);

	if ((fd_p = popen(ntap_cmd, "r")) == NULL) {
		(void) unlink(cfg_fname);
		return (SCNAS_EUNEXPECTED);
	}

	/*
	 * NTAPfence does not provide a contracted exit code in
	 * the following situation, so we reply on its output.
	 */
	*buffer = '\0';
	(void) fgets(buffer, SCNAS_MAXSTRINGLEN, fd_p);
	if (strncmp(buffer, "ERROR: HTTP POST", 16) == 0) {
		scnas_err = SCNAS_ENOEXIST;
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "\"%s\" is not a valid \"%s\" NAS "
			    "device.\n"),
			    filer_name, SCNAS_NAS_TYPE);
			scconf_addmessage(buffer, messages);
		}
		(void) pclose(fd_p);
		return (scnas_err);
	}

	ntap_err = pclose(fd_p);

	/* Remove the tmp file */
	(void) unlink(cfg_fname);

	if (WIFEXITED(ntap_err) && WEXITSTATUS((uint_t)ntap_err)) {
		scnas_err = SCNAS_EAUTH;

		switch (WEXITSTATUS((uint_t)ntap_err)) {
		case NTAP_EFILER:
			/* Filer name wrong */
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Cannot connect to NAS device \"%s\".\n"),
				    filer_name);
				scconf_addmessage(buffer, messages);
				scconf_addmessage(dgettext(TEXT_DOMAIN, "The "
				    "device name must be correct, and the "
				    "device must be setup for HTTP access.\n"),
				    messages);
			}
			break;

		case NTAP_EAUTH:
			/* Userid or password wrong */
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Authorization failed to access NAS device "
				    "\"%s\".\n"), filer_name);
				scconf_addmessage(buffer, messages);
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				    "The userid and password must be "
				    "correct, and the user must have "
				    "privileges to access the device.\n"),
				    messages);
			}
			scnas_err = SCNAS_EINVALPROP;
			break;

		case NTAP_ENOEXIST_DIR:
			/* Non existing directory. This is expected */
			scnas_err = SCNAS_NOERR;
			break;

		default:
			/*
			 * We know all the errors related to filer, userid
			 * and password. We don't worry about other errors.
			 */
			scnas_err = SCNAS_NOERR;
			break;
		}
	} else {
		scnas_err = SCNAS_NOERR;
	}

	return (scnas_err);
}

/*
 * nas_validate_filerdir
 *
 *	Verify the exported directories on the given filer, by calling
 *	Netapp binary NTAPfence.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_INVAL		- invalid arguments
 *	SCNAS_EINVALPROP	- invalid filer property
 *	SCNAS_EUNEXPECTED	- internal error
 */
static scnas_errno_t
nas_validate_filerdir(char *filer_name,
    cl_query_table_t *filer_table, scnas_filer_prop_t *prop_list,
    scnas_filer_prop_t **new_props, uint_t *length, char **messages)
{
	scnas_errno_t scnas_err = SCNAS_NOERR;
	char *nas_useid = NULL;
	char *nas_passwd = NULL;
	char cfg_fname[SCNAS_MAXSTRINGLEN];
	char ntap_cmd[SCNAS_MAXSTRINGLEN];
	char buffer[SCNAS_MAXSTRINGLEN];
	register scnas_filer_prop_t *prop;
	FILE *tmp_fp;
	int ntap_err = 0;
	FILE *fd_p;
	scnas_filer_prop_t *t_new_props = (scnas_filer_prop_t *)0;
	scnas_filer_prop_t **new_prop;

	/* Check args */
	if (!filer_name || !prop_list || !filer_table) {
		return (SCNAS_EINVAL);
	}

	if (length != NULL) {
		*length = 0;
	}

	/* Get the user id and password */
	nas_useid = scnas_clquery_find_by_key(filer_table,
	    PROP_USERID, NULL);
	nas_passwd = scnas_clquery_find_by_key(filer_table,
	    PROP_PASSWD, NULL);
	if (nas_passwd) {
		scnas_rot13_alg(&nas_passwd);
	}

	/* Get the temp config file name. */
	(void) sprintf(cfg_fname,
	    "/var/cluster/run/.scdirnas_tmp.%d", getpid());

	/* Go through the properties */
	new_prop = &t_new_props;
	for (prop = prop_list; prop; prop = prop->scnas_prop_next) {
		/* Skip */
		if (!(prop->scnas_prop_key) ||
		    !(prop->scnas_prop_value))
			continue;

		/* Write to the config file */
		if ((tmp_fp = fopen(cfg_fname, "w")) == NULL) {
			return (SCNAS_EUNEXPECTED);
		}
		if (nas_useid) {
			if (nas_passwd) {
				(void) fprintf(tmp_fp,
				    "directories\n\t%s %s %s %s\n",
				    filer_name, prop->scnas_prop_value,
				    nas_useid, nas_passwd);
			} else {
				(void) fprintf(tmp_fp,
				    "directories\n\t%s %s %s\n",
				    filer_name, prop->scnas_prop_value,
				    nas_useid);
			}
		} else {
			(void) fprintf(tmp_fp, "directories\n\t%s %s\n",
			    filer_name, prop->scnas_prop_value);
		}
		(void) fclose(tmp_fp);

		/* Generate and call NTAPfence command */
		(void) sprintf(ntap_cmd, "%s -V -i %s >/dev/null 2>&1",
		    NETAPP_BINARY, cfg_fname);

		if ((fd_p = popen(ntap_cmd, "r")) == NULL) {
			(void) unlink(cfg_fname);
			return (SCNAS_EUNEXPECTED);
		}
		ntap_err = pclose(fd_p);

		/* If no error, save this directory */
		if (!(WIFEXITED(ntap_err)) ||
		    (WEXITSTATUS((uint_t)ntap_err) == 0)) {
			*new_prop = (scnas_filer_prop_t *)calloc(1,
			    sizeof (scnas_filer_prop_t));
			if (*new_prop == (scnas_filer_prop_t *)0) {
				if (scnas_err == SCNAS_NOERR) {
					scnas_err = SCNAS_ENOMEM;
				}
				break;
			}
			(*new_prop)->scnas_prop_key =
			    strdup(prop->scnas_prop_key);
			(*new_prop)->scnas_prop_value =
			    strdup(prop->scnas_prop_value);
			if (!(*new_prop)->scnas_prop_key ||
			    !(*new_prop)->scnas_prop_value) {
				if (scnas_err == SCNAS_NOERR) {
					scnas_err = SCNAS_ENOMEM;
				}
				break;
			}
			new_prop = &(*new_prop)->scnas_prop_next;

			/* Count this directory */
			if (length != NULL) {
				(*length)++;
			}

			continue;
		}

		/* Write the error messages */
		if (scnas_err == SCNAS_NOERR) {
			scnas_err = SCNAS_EINVALPROP;
		}
		switch (WEXITSTATUS((uint_t)ntap_err)) {
		case NTAP_EFILER:
			/* Filer name wrong */
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Cannot connect to NAS device \"%s\".\n"),
				    filer_name);
				scconf_addmessage(buffer, messages);
				scconf_addmessage(dgettext(TEXT_DOMAIN, "The "
				    "device name must be correct, and the "
				    "device must be setup for HTTP access.\n"),
				    messages);
			}
			break;

		case NTAP_EAUTH:
			/* Userid or password wrong */
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Authorization failed to access NAS device "
				    "\"%s\".\n"), filer_name);
				scconf_addmessage(buffer, messages);
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				    "The userid and password must be "
				    "correct, and the user must have "
				    "privileges to access the device.\n"),
				    messages);
			}
			break;

		case NTAP_ENOEXIST_DIR:
			/* Directory not exist */
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Directory \"%s\" does not exist on "
				    "\"%s\". \n"),
				    prop->scnas_prop_value, filer_name);
				scconf_addmessage(buffer, messages);
			}
			break;

		case NTAP_EDIR_RW:
			/* Directory has "rw" */
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Directory \"%s\" on \"%s\" has invalid "
				    "\"-rw\" wildcard access.\n"),
				    prop->scnas_prop_value, filer_name);
				scconf_addmessage(buffer, messages);
			}
			break;

		case NTAP_EDIR_RO:
			/* Directory has "ro" */
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Directory \"%s\" on \"%s\" has invalid "
				    "\"-ro\" wildcard access.\n"),
				    prop->scnas_prop_value, filer_name);
				scconf_addmessage(buffer, messages);
			}
			break;

		default:
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "NAS device \"%s\" has unclear "
				    "configuration problems.\n"),
				    filer_name);
				scconf_addmessage(buffer, messages);
			}
			break;
		}

	}
	/* Set the new list */
	*new_props = t_new_props;

	/* Remove the tmp file */
	(void) unlink(cfg_fname);

	/* Put the password back to obfuscated format */
	scnas_rot13_alg(&nas_passwd);

	return (scnas_err);
}
