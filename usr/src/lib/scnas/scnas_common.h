/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCNAS_COMMON_H
#define	_SCNAS_COMMON_H

#pragma ident	"@(#)scnas_common.h	1.6	08/05/20 SMI"

/*
 * This is the header file for specific NAS device type
 * functions. Currently the following functions that contains
 * NAS type specific tasks are defined here, and implemented
 * in the device type scnas library:
 *
 *	scnas_chk_nas_type		Validate the filer type
 *	scnas_chk_nas_name		Validate the filer name
 *	scnas_chk_prop_required		Check if the filer requries property
 *	scnas_lib_attach_filer		Attach a filer with its properties
 *	scnas_lib_remove_filer		Remove a filer
 *	scnas_lib_set_filer_prop	Change a filer's properties
 *	scnasdir_lib_add_dir		Add directories to a filer
 *	scnasdir_lib_rm_dir		Remove directories from a filer
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/types.h>

#include <scnas.h>

/*
 * scnas_chk_nas_type
 *
 *	This function confirms the NAS type.
 *
 * Return values:
 *
 *	boolean_t
 */
extern boolean_t scnas_chk_nas_type(const char *, boolean_t *);

/*
 * scnas_chk_nas_name
 *
 *	Confirm if the given NAS filer is of the type of the
 *	library that calls this function.
 *
 * Return values:
 *
 *	boolean_t
 */
extern boolean_t scnas_chk_nas_name(const char *, char *);

/*
 * scnas_chk_prop_required
 *
 *	Confirm if the given NAS filer is of the type of the
 *	library that calls this function, and sets whether
 *	this type requires property in the 2nd parameter.
 *
 * Return values:
 *
 *	boolean_t
 */
extern boolean_t scnas_chk_prop_required(const char *, boolean_t *);

/*
 * scnas_lib_attach_filer
 *
 *	Attach the given "filername" to cluster configuration if
 *	the filer is not already attached, along with its properties.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- no permission
 *	SCNAS_EINVAL		- invalid arguments
 *	SCNAS_EEXIST		- filer is already attached
 *	SCNAS_EUNKNOWN		- unknow type
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_VP_MISMATCH	- unmatched running version
 *	SCNAS_NO_BINARY		- required binary does not exist
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
extern scnas_errno_t scnas_lib_attach_filer(char *filername,
    scnas_filer_prop_t *prop_list, char **messages);

/*
 * scnas_lib_remove_filer
 *
 *	Detach a filer from cluster configuration.
 *	If "flag" is set, remove the filer even it has existing
 *	directories; Otherwise only remove the filer if no
 *	exiting directories.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- filer doesn't exist
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_EDIREXIST		- still has directories
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
extern scnas_errno_t scnas_lib_remove_filer(char *filername,
    char *table_name, scnas_filer_prop_t *props, char **messages,
    uint_t flag);

/*
 * scnas_lib_set_filer_prop
 *
 *	Update the specified properties for the given filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- not root
 *	SCNAS_ENOEXIST		- object not found
 *	SCNAS_EINVAL		- invalid argument
 *	SCNAS_ENOMEM		- not enough memory
 *	SCNAS_EUNEXPECTED	- internal or unexpected error
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 */
extern scnas_errno_t scnas_lib_set_filer_prop(char *filername,
    char *table_name, scnas_filer_prop_t *props, char **messages);

/*
 * scnasdir_lib_add_dir
 *
 *	Add directories into an existing filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- no permission
 *	SCNAS_EINVAL		- invalid arguments
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 *	SCNAS_EINVALPROP	- invalid filer property
 */
extern scnas_errno_t scnasdir_lib_add_dir(char *filername,
    char *table_name, scnas_filer_prop_t *props,
    char **messages, char *, uint_t uflg);

/*
 * scnasdir_lib_rm_dir
 *
 *	Remove directories from an existing filer.
 *
 * Possible return values:
 *
 *	SCNAS_NOERR		- success
 *	SCNAS_EPERM		- no permission
 *	SCNAS_EINVAL		- invalid arguments
 *	SCNAS_EUNEXPECTED	- internal error
 *	SCNAS_ENOMEM		- out of memory
 *	SCNAS_ENOCLUSTER	- not a cluster node
 *	SCNAS_ERECONFIG		- cluster is reconfiguring
 *	SCNAS_EINVALPROP	- invalid filer property
 */
extern scnas_errno_t scnasdir_lib_rm_dir(char *filername,
    char *table_name, scnas_filer_prop_t *props,
    char **messages, char *, uint_t uflg);


#ifdef __cplusplus
}
#endif

#endif /* _SCNAS_COMMON_H */
