/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)create_set.c	1.33	08/05/20 SMI"


/*
 * This file provides the implementation for a service create transaction.
 * The main entry points are at the end of the file while the local routines
 * which are declared static are up front.
 */
#define	CLUSTER_LIBRARY_SOURCE

#include <syslog.h>
#include <meta.h>
#include <sys/param.h>		/* For MAXPATHLEN */
#include <sys/stat.h>
#include <sdssc.h>

rval_e _sdssc_generic_create_begin(char *, int, char **, int, char *);

#ifndef SDSSC_MO_CLASS
#define	SDSSC_MO_CLASS NULL
#endif

char *service_table[] = {
	SDSSC_CLASS,
	SDSSC_MO_CLASS,
	NULL
};

/*
 * l_get_property -- Return a pointer to the value for the property found.
 */
rval_e
l_get_property(scconf_cfg_ds_t *s, char *key, char **val)
{
	scconf_cfg_prop_t	*props;

	props = s->scconf_ds_propertylist;
	while (props) {
		if (strcmp(key, props->scconf_prop_key) == 0) {
			*val = props->scconf_prop_value;
			break;
		}
		props = props->scconf_prop_next;
	}
	return ((props == NULL) ? SDSSC_ERROR : SDSSC_OKAY);
}

/*
 * get_index_bitmap -- find a bitmap of all set numbers
 *
 * LIMITS - The bit map is using an int which means only 32 set can be
 * used. Currently this is fine because DiskSuite has a limit of 32 sets.
 * That might change in the future.
 */
static rval_e
get_index_bitmap(int *bitmap)
{
	char		*value,
			**i;
	set_t		set_bit_mask,
			set_idx;
	scconf_cfg_ds_t	*n,
			*sn;


	/*
	 * Always mark set 0, which is the local set, as in use
	 */
	set_bit_mask = 1;

	for (i = service_table; *i != NULL; i++) {
		if (scconf_get_ds_config(*i, SCCONF_BY_TYPE_FLAG,
		    &sn) != SCCONF_NOERR) {
			scconf_free_ds_config(sn);
			return (SDSSC_ERROR);
		}

		n = sn;
		while (n) {
			if (l_get_property(n, SDSSC_PROP_INDEX, &value)
			    == SDSSC_ERROR) {
				scconf_free_ds_config(sn);
				return (SDSSC_ERROR);
			}

			set_idx = strtol(value, 0, 0);

			if (set_bit_mask & (1 << set_idx)) {
				/*
				* At this point we've got a major error
				* here. Somehow the DCS database has multiple
				* disksets with the same index value.
				*/
				scconf_free_ds_config(sn);
				return (SDSSC_ERROR);
			}

			set_bit_mask |= (1 << set_idx);

			n = n->scconf_ds_next;
		}
		scconf_free_ds_config(sn);
	}

	*bitmap = set_bit_mask;

	return (SDSSC_OKAY);
}

/*
 * garbage_collection -- find and free lost entries
 */
static rval_e
garbage_collection(void)
{
	int		nodeid,
			freed_space;
	char		*pval,
			**i;
	void		*create_incarnation,
			*current_incarnation;
	scconf_cfg_ds_t	*sn,
			*n;

	freed_space = 0;
	for (i = service_table; *i != NULL; i++) {
		if (scconf_get_ds_config(*i, SCCONF_BY_TYPE_FLAG,
		    &sn) != SCCONF_NOERR) {
			scconf_free_ds_config(sn);
			return (SDSSC_ERROR);
		}

		n = sn;
		while (n) {

			if (l_get_property(n, SDSSC_PROP_STATE, &pval)
			    == SDSSC_ERROR) {
				scconf_free_ds_config(sn);
				goto exit_routine;
			}

			if (strcmp(pval, SDSSC_STATE_COMMIT) == 0) {
				/*
				 * A committed service can't be removed by
				 * the garbage collector.
				 */
				n = n->scconf_ds_next;
				continue;
			}

			if (l_get_property(n, SDSSC_PROP_NODE, &pval)
			    == SDSSC_ERROR) {
				scconf_free_ds_config(sn);
				goto exit_routine;
			}

			nodeid = strtol(pval, 0, 0);

			if (l_get_property(n, SDSSC_PROP_INCAR, &pval)
			    == SDSSC_ERROR) {
				scconf_free_ds_config(sn);
				goto exit_routine;
			}

			create_incarnation = l_prop_to_incarnation(pval);

			current_incarnation = l_get_incarnation(nodeid);

			if (l_compare_incarnation(create_incarnation,
			    current_incarnation) == SDSSC_False) {

			/*
			 * If the state isn't committed and the incarnation
			 * numbers are different than the node rebooted
			 * and this entry can be freed.
			 */
				freed_space++;
			}
			n = n->scconf_ds_next;
		}
		scconf_free_ds_config(sn);
	}

exit_routine:

	return (freed_space ? SDSSC_OKAY : SDSSC_ERROR);


}
/*
 * create_begin -- allocate set number for service
 */
rval_e
_sdssc_create_begin(char *service, int nhosts, char **hosts,
    int request_setno)
{
	/* Create a traditional set */
	return (_sdssc_generic_create_begin(service, nhosts, hosts,
	    request_setno, SDSSC_CLASS));
}
/*
 * mo_create_begin -- allocate set number for service
 */
rval_e
_sdssc_mo_create_begin(char *service, int nhosts, char **hosts,
    int request_setno)
{
	/* Create a multi-owner set */
	return (_sdssc_generic_create_begin(service, nhosts, hosts,
	    request_setno, SDSSC_MO_CLASS));
}

/*
 * generic_create_set_begin -- allocate set number for service
 */
static rval_e
_sdssc_generic_create_begin(char *service, int nhosts, char **hosts,
    int request_setno, char *sds_class_type)
{
	sdssc_boolean_e		garbage_done_once;
	int			set_bitmap,
				set_idx,
				x,
				retry_count = 2,
				prop_idx;
	char			*prog,
				*pval,
				path[MAXPATHLEN],
				errbuf[BUFSIZ],
				**nodelist = NULL;
	struct stat		s;
	void			*cookie;
	scconf_errno_t		rval;
	scconf_cfg_ds_t		*sn;
	scconf_cfg_prop_t	props[SDSSC_PROP_COUNT];
	scconf_gdev_range_t	range;
	scconf_nodeid_t		nodeid;
	scconf_errno_t		sc_rval;



	if (sds_class_type == NULL)
		return (SDSSC_ERROR);

	/*
	 * During error clean-up if the first two property values are
	 * non zero memory will be freed. So don't remove this memset
	 * without changing the error clean-up.
	 */
	memset(props, 0, sizeof (props));

	if ((sc_rval = scconf_get_ds_config(sds_class_type,
	    SCCONF_BY_TYPE_FLAG, &sn)) != SCCONF_NOERR) {
		/*
		* The service class must exists else something
		* is really screwed up.
		*/

		scconf_free_ds_config(sn);
		scconf_strerr(errbuf, sc_rval);
		printf(gettext("DCS service class for %s does not exist "
		    "or is not available. (%s)\n"), sds_class_type, errbuf);
		return (SDSSC_ERROR);
	} else {
		scconf_free_ds_config(sn);
	}


	/*
	 * Set up a state flag in case we ever run the garbage collector.
	 * If it gets run once and the bitmap is still full then there's
	 * nothing more that we can do except bail out.
	 */
	garbage_done_once = SDSSC_False;

retry:

	if (get_index_bitmap(&set_bitmap) == SDSSC_ERROR) {
		return (SDSSC_ERROR);
	}

	/*
	 * If the bitmap is full try to garbage collect some old entries.
	 * Failure to garbage collect means that there are no more sets
	 * available and we must return an error.
	 */
	if (set_bitmap == 0xffffffff) {
		if ((garbage_done_once == SDSSC_True) ||
		    (garbage_collection() == SDSSC_ERROR))
			return (SDSSC_ERROR);
		else {
			garbage_done_once = SDSSC_True;
			goto retry;
		}
	}

	if (request_setno == SDSSC_PICK_SETNO) {
		/*
		 * Find first empty set in bitmap and then
		 * set the property.
		 */
		set_idx = 0;
		for (x = 0; x < 32; x++) {
			if ((set_bitmap & 1) == 0)
				break;
			set_bitmap >>= 1;
			set_idx++;
		}
	} else {
		/*
		 * A specific set number has been requested so
		 * check to see if the that index is available.
		 */
		if (set_bitmap & (1 << request_setno)) {
			/*
			 * Looks like this set number has already
			 * been allocated.
			 */

			return (SDSSC_ERROR);
		} else {
			set_idx = request_setno;
		}
	}

	/*
	 * Initialize property index so that the code doesn't need
	 * to concern itself with which property the subsection
	 * is working on now.
	 */
	prop_idx = 0;

	/*
	 * Create this service's index value
	 */
	props[prop_idx].scconf_prop_key = SDSSC_PROP_INDEX;
	if ((props[prop_idx].scconf_prop_value =
	    (char *)malloc(SDSSC_NODE_INDEX_LEN)) == NULL) {
		goto exit_routine_w_err;
	}
	sprintf(props[prop_idx].scconf_prop_value, "%d", set_idx);
	props[prop_idx].scconf_prop_next = &props[prop_idx + 1];
	prop_idx++;

	/*
	 * Determine this hosts node id and create property
	 */
	if (scconf_get_nodeid(NULL, &nodeid) != SCCONF_NOERR)
		return (SDSSC_ERROR);
	props[prop_idx].scconf_prop_key = SDSSC_PROP_NODE;
	if ((props[prop_idx].scconf_prop_value =
	    (char *)malloc(SDSSC_NODE_INDEX_LEN)) == NULL) {
		goto exit_routine_w_err;
	}
	sprintf(props[prop_idx].scconf_prop_value, "%d", nodeid);
	props[prop_idx].scconf_prop_next = &props[prop_idx + 1];
	prop_idx++;

	/*
	 * Get the incarnation data and create the prop
	 */
	props[prop_idx].scconf_prop_key = SDSSC_PROP_INCAR;
	props[prop_idx].scconf_prop_value =
		l_incarnation_to_prop(nodeid);
	props[prop_idx].scconf_prop_next = &props[prop_idx + 1];
	prop_idx++;

	/*
	 * Set the state property
	 */
	props[prop_idx].scconf_prop_key = SDSSC_PROP_STATE;
	props[prop_idx].scconf_prop_value = SDSSC_STATE_CREATE;
	props[prop_idx].scconf_prop_next = (scconf_cfg_prop_t *)0;
	prop_idx++;

	/*
	 * Validate that the number of properties created equals the
	 * number of properties allocated. Just in case the header
	 * changes the number, but the code doesn't.
	 */
	if (prop_idx != SDSSC_PROP_COUNT) {
		printf(gettext("Error: Bad property count\n"));
		goto exit_routine_w_err;
	}

	/*
	 * Build device list. By agreement with the Cluster group we'll
	 * create the full range of devices from 0 to 8191 in each
	 * service. It's not expected that customers will have their
	 * md.conf file always maxed out, but this will already be
	 * set up if they do.
	 */
	sprintf(path, "%s/admin", SDSSC_MD_DIR);
	if (stat(path, &s) == -1) {
		goto exit_routine_w_err;
	}

	range.major_no = major(s.st_rdev);
	range.start_minor = set_idx * SDSSC_METADDEV_MAX;
	range.end_minor = range.start_minor + SDSSC_METADDEV_MAX - 1;
	range.scconf_gdev_next = (scconf_gdev_range_t *)0;

	/*
	 * hosts doesn't have to be a null terminated string
	 */
	nodelist = calloc(nhosts + 1, sizeof (char *));
	memcpy(nodelist, hosts, (nhosts + 1) * sizeof (char *));

	if ((sc_rval = scconf_add_ds(sds_class_type, service, nodelist,
	    SCCONF_STATE_ENABLED, &range, SCCONF_STATE_UNCHANGED,
	    props, 0)) != SCCONF_NOERR) {
		printf("sc_rval = %d\n", sc_rval);
		goto exit_routine_w_err;
	}


	free(nodelist);
	free(props[0].scconf_prop_value);
	free(props[1].scconf_prop_value);

	/*
	 * This failure is a little pessimistic. If three people were
	 * creating sets and by chance this set was unique, but the other
	 * two collided this set would fail to be created during the
	 * first attempt. A very unlikely event which would sort itself
	 * out and eventually work.
	 */
	if (get_index_bitmap(&set_bitmap) == SDSSC_ERROR) {
		scconf_rm_ds(service, NULL, NULL, NULL, 0);
		/* XXX need ramdom backoff here */
		if (retry_count-- > 0)
			goto retry;
		else
			return (SDSSC_ERROR);
	}

	return (SDSSC_OKAY);

exit_routine_w_err:

	if (nodelist)
		free(nodelist);
	if (props[0].scconf_prop_value)
		free(props[0].scconf_prop_value);
	if (props[1].scconf_prop_value)
		free(props[1].scconf_prop_value);

	return (SDSSC_ERROR);
}


/*
 * sdssc_create_end -- either mark the service as committed or remove it
 */
rval_e
_sdssc_create_end(char *service, dcs_set_state_e d)
{
	char			**dsnodes;
	int			n;
	scconf_cfg_ds_t		*config;
	scconf_cfg_prop_t	prop;
	mdc_err_t		**elp,
				**elp_orig;
	rval_e			rval;
	scconf_errno_t		sc_rval;

	switch (d) {
	    case SDSSC_COMMIT:
		prop.scconf_prop_key = SDSSC_PROP_STATE;
		prop.scconf_prop_value = SDSSC_STATE_COMMIT;
		prop.scconf_prop_next = (scconf_cfg_prop_t *)0;

		if (scconf_change_ds(service, NULL, SCCONF_STATE_UNCHANGED,
		    NULL, SCCONF_STATE_UNCHANGED, &prop,
		    NULL) != SCCONF_NOERR) {

			/*
			 * XXX Should we remove the service on failure here?
			 */
			return (SDSSC_ERROR);
		} else {

			if (_sdssc_bindclusterdevs(&elp_orig) != SDSSC_OKAY) {
				elp = elp_orig;
				while (elp && (*elp != NULL)) {
					if ((*elp)->mdc_errno != 0) {
						/*
						 * SCMSGS
						 * @explanation
						 * Need explanation of this
						 * message!
						 * @user_action
						 * Need a user action for this
						 * message.
						 */
						syslog(LOG_ERR,
						    gettext(
						    "ERROR: %s: %s %d\n"),
						    (*elp)->mdc_node,
						    (*elp)->mdc_misc,
						    (*elp)->mdc_errno);
						printf(gettext(
						    "ERROR: %s: %s %d\n"),
						    (*elp)->mdc_node,
						    (*elp)->mdc_misc,
						    (*elp)->mdc_errno);
					}
					elp++;
				}
				_sdssc_free_mdcerr_list(elp_orig);
			}

			return (SDSSC_OKAY);
		}

	    case SDSSC_CLEANUP:
		    return (scconf_rm_ds(service, NULL, NULL, NULL, 0) ==
			    SCCONF_NOERR ? SDSSC_OKAY : SDSSC_ERROR);

	    default:
		    return (SDSSC_ERROR);
	}
}
