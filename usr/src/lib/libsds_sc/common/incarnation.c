/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)incarnation.c 1.9     08/05/20 SMI"

/*
 * Provide a set of interfaces to hide the data from the rest of
 * libsds_sc. Currently this is required because DiskSuite hasn't
 * been able to get a commitment from SC3.0 to expose the nodes
 * incarnation number. Hopefully, this will still happen at a
 * future date and these routines can change to use the real
 * node incarnation number.
 */
#define	CLUSTER_LIBRARY_SOURCE
#include <meta.h>
#include <sys/types.h>
#include <time.h>
#include <sdssc.h>

static char timebuf[20];

/*
 * l_get_incarnation -- return an incarnation number for a given node
 */
void *
l_get_incarnation(int nid)
{
	return ((void *)time(0));
}

/*
 * clamdin_incarnation_to_prop -- create a property string based on node incar
 */
char *
l_incarnation_to_prop(int nid)
{
	sprintf(timebuf, "%d", l_get_incarnation(nid));
	return (timebuf);
}

/*
 * l_prop_to_incarnation -- convert property string to incarnation #
 */
void *
l_prop_to_incarnation(char *s)
{
	time_t t = strtol(s, 0, 0);
	return ((void *)t);
}

/*
 * l_compare_incarnation -- see if a node has been rebooted between
 * the time a service was created and committed.
 */
sdssc_boolean_e
l_compare_incarnation(void *i1, void *i2)
{
	time_t	t1,
		t2,
		save;

	t1 = (time_t)i1;
	t2 = (time_t)i2;

	if (t1 == t2) {
		/*
		 * Damn unlikely, but that's okay
		 */
		return (SDSSC_True);
	}

	/*
	 * No need to require a specific order for the
	 * comparison.
	 */
	if (t2 < t1) {
		save = t2;
		t2 = t1;
		t1 = save;
	}

	/*
	 * Check to see if t1 (the time the service was created, but not
	 * completed) is within an hour of this check. If an hour has gone
	 * by it should be safe to indicate that this incarnation is stale.
	 * NOTE: This assumes that the nodes will synchronize time to within
	 * a second or two.
	 */
	return (((t1 + (60 * 60)) > t2) ? SDSSC_True : SDSSC_False);

}
