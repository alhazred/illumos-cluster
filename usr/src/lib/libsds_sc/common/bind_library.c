/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)bind_library.c	1.12	08/05/20 SMI"

#define	CLUSTER_LIBRARY_SOURCE
#include <meta.h>
#include <sdssc.h>
#include <sys/cladm_int.h>
#include <sys/cladm.h>

/*
 * bind_library -- check to see if we're part of a cluster
 */
int
_bind_library(void)
{
	int			bootflags;
	clcluster_name_t	clname;
	char			name[64] = "";

	/* Check if the node has been booted into cluster mode */
	if ((cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) ||
		!(bootflags & CLUSTER_BOOTED)) {
		return (1);
	}

	/* Check if the cluster framework is up */
	clname.name = name;
	clname.len = sizeof (name);
	if (cladm(CL_CONFIG, CL_GET_CLUSTER_NAME, &clname)) {
		return (1);
	}

	return (scconf_cltr_openhandle(0) == SCCONF_NOERR ? 0 : 1);
}
