/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)did_interfaces.c	1.8	08/05/20 SMI"

/*
 * Block comment which describes the contents of this file.
 */

#define	CLUSTER_LIBRARY_SOURCE
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <sdssc.h>
#include <libdid.h>
#include <didprivate.h>

#define	BASENAME_CTD_PATH	"/dev/rdsk/"
#define	BASENAME_DID_PATH	"/dev/did/rdsk/d"
/*
 * sdssc_convert_ctd_path - convert CTD path to DID name
 *
 * This routine is expecting the argument ctd in one of two forms.
 * a) The final component: 'c0t0d0'
 * b) Full pathname to the raw device: /dev/rdsk/c0t0d0
 * Note that both version are missing the slice information
 */
rval_e
_sdssc_convert_ctd_path(const char *ctd, char **did)
{
	char			*working_ctd = NULL,
				*working_did = NULL,
				*didname;
	rval_e			rval = SDSSC_ERROR;
	did_device_list_t	*result_device = NULL;
	char			*did_instance = NULL;
	int			rem, count = 0;

	*did = (char *)0;
	if (*ctd == '/') {
		if ((working_ctd = strdup(ctd)) == NULL) {
			return (SDSSC_ERROR);
		}
	} else if ((working_ctd = malloc(strlen(BASENAME_CTD_PATH) +
	    strlen(ctd) + 1)) == NULL) {
		return (SDSSC_ERROR);
	} else {
		sprintf(working_ctd, "%s%s", BASENAME_CTD_PATH, ctd);
	}

	result_device = map_to_did_device(working_ctd, 0);
	if (result_device == NULL) {
		goto exit_routine;
	}

	/*
	 *  Find the number of characters in instance number to allocate
	 *  memory appropriately.
	 */
	rem = result_device->instance;
	do {
	    count++;
	    rem = rem/10;
	} while (rem != 0);

	if ((working_did =
	    malloc(strlen(BASENAME_DID_PATH) + count + 1)) == NULL) {
		*did = NULL;
	} else {
		sprintf(working_did, "%s%d",
		    BASENAME_DID_PATH,
		    result_device->instance);
		*did = strdup(working_did);
		rval = SDSSC_OKAY;
	}

exit_routine:
	if (result_device)
		did_device_list_free(result_device);
	if (working_ctd)
		free(working_ctd);
	if (working_did)
		free(working_did);
	return (rval);
}

/*
 * sdssc_convert_cluster_path -- convert DID name to CTD path
 *  This function can made more efficient by using map_from_did_device()
 *  This change is not being done now to limit the amount of code changes
 *  in a patch.
 */
rval_e
_sdssc_convert_cluster_path(const char *old, char **new)
{
	char			*working_copy = strdup(old),
				*sub_start,
				*sub_end;
	scconf_cfg_did_t	*didp = NULL;
	scconf_cfg_devices_t	*devices;
	scconf_nodeid_t		nodeid;
	rval_e			rval = SDSSC_ERROR;

	/*
	 * Make sure we're dealing with a DID path. Our test suites use the
	 * same final component name as the DID driver. I'd hate to screwup
	 * the test suites if they were run on a clustering machine.
	 */
	if (strstr(working_copy, "/did/") == NULL) {
		*new = working_copy;
		return (SDSSC_OKAY);
	}

	if (sub_start = strrchr(working_copy, '/'))
		*sub_start++;
	else {
		/*
		 * This should be impossible because of the check for
		 * "/did/" above.
		 */

		sub_start = working_copy;
	}

	/*
	 * Remove the slice component of the name
	 */
	if (sub_end = strrchr(sub_start, 's'))
		*sub_end = '\0';

	/*
	 * Go ahead a duplicate the old pointer now. If a match is found
	 * then this pointer will be freed first before a copy of the
	 * conversion is made.
	 */
	*new = strdup(old);

	if (scconf_get_did_config(sub_start, &didp) != SCCONF_NOERR)
		goto exit_routine;

	if (scconf_get_nodeid(NULL, &nodeid) != SCCONF_NOERR)
		goto exit_routine;

	for (devices = didp->scconf_devlist; devices;
	    devices = devices->scconf_device_next) {
		if (devices->nodeid == nodeid) {
			free(*new);
			*new = strdup(devices->devname);
			rval = SDSSC_OKAY;
		}
	}

exit_routine:
	if (working_copy)
		free(working_copy);
	if (didp)
		scconf_free_did_config(didp);
	return (rval);
}

void
_sdssc_convert_path_free(char *source)
{
	free(source);
}
