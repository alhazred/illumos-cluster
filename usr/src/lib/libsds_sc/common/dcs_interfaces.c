/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)dcs_interfaces.c	1.14	08/05/20 SMI"

/*
 * This file contains simple interface functions to the clustering
 * software. Hopefully if the underlying software for clusters changes
 * these routines can still store and retrieve property information.
 */

#define	CLUSTER_LIBRARY_SOURCE
#include <stdlib.h>
#include <sys/types.h>
#include <sdssc.h>

extern char *service_table[];


/*
 * sdssc_suspend -- stop the service
 */
rval_e
_sdssc_suspend(const char *setname)
{
	scswitch_service_t	service;

	service.scswitch_service_name = (char *)setname;
	service.next = (scswitch_service_t *)0;

	return (scswitch_take_service_offline(&service) == SCSWITCH_NOERR ?
	    SDSSC_OKAY : SDSSC_ERROR);
}

/*
 * sdssc_property_get -- return the value associated with name
 */
rval_e
_sdssc_property_get(char *service, char *name, char **storage)
{
	char		*propval;
	rval_e		rval;
	scconf_cfg_ds_t	*config = NULL;

	if ((scconf_get_ds_config(service, SCCONF_BY_NAME_FLAG, &config) ==
	    SCCONF_NOERR) &&
	    (l_get_property(config, name, &propval) == SDSSC_OKAY)) {
		*storage = strdup(propval);
		if (*storage == NULL) {
			rval = SDSSC_ERROR;
		} else {
			rval = SDSSC_OKAY;
		}
	} else {
		*storage = NULL;
		rval = SDSSC_ERROR;
	}

	if (config)
		scconf_free_ds_config(config);
	return (rval);
}

/*
 * sdssc_property_set -- change properties value
 */
rval_e
_sdssc_property_set(char *service, char *key, char *value)
{
	scconf_cfg_prop_t	prop;

	prop.scconf_prop_key = key;
	prop.scconf_prop_value = value;
	prop.scconf_prop_next = (scconf_cfg_prop_t *)0;

	return (scconf_change_ds(service, 0, SCCONF_STATE_UNCHANGED,
	    0, SCCONF_STATE_UNCHANGED, &prop, 0) == SCCONF_NOERR ?
	    SDSSC_OKAY : SDSSC_ERROR);
}

/*
 * sdssc_get_serivces -- get a list of all services in cluster
 */
rval_e
_sdssc_get_services(char ***storage)
{
	char		**names,
			**i;
	int		x;
	scconf_cfg_ds_t	*sn,
			*n;

	*storage = (char **)0;

	x = 0;
	for (i = service_table; *i != NULL; i++) {
		if (scconf_get_ds_config(*i, SCCONF_BY_TYPE_FLAG,
		    &sn) != SCCONF_NOERR) {
			scconf_free_ds_config(sn);
			return (SDSSC_ERROR);
		}
		for (n = sn; n; n = n->scconf_ds_next) {
			x++;
		}
		scconf_free_ds_config(sn);
	}
	if ((names = (char **)calloc(x + 1, sizeof (char *))) == NULL) {
		return (SDSSC_ERROR);
	}
	*storage = names;

	for (i = service_table; *i != NULL; i++) {
		if (scconf_get_ds_config(*i, SCCONF_BY_TYPE_FLAG,
		    &sn) != SCCONF_NOERR) {
			scconf_free_ds_config(sn);
			return (SDSSC_ERROR);
		}
		for (n = sn; n; n = n->scconf_ds_next, names++) {
			if ((*names = strdup(n->scconf_ds_name)) == NULL) {
				scconf_free_ds_config(sn);
				_sdssc_get_services_free(*storage);
				return (SDSSC_ERROR);
			}
		}
		scconf_free_ds_config(sn);
	}

	return (SDSSC_OKAY);
}

/*
 * sdssc_get_services_free -- free up allocated storage
 */
rval_e
_sdssc_get_services_free(char **storage)
{
	char **p;

	if (p) {
		for (p = storage; *p; p++)
			free(*p);
		free(storage);
	}
	return (SDSSC_OKAY);
}

/*
 * sdssc_notify_service -- notify DCS that we're doing a take/release
 */
rval_e
_sdssc_notify_service(const char *service, sdssc_dcs_notify_e take_op)
{
	char			*nodename = NULL;
	scconf_nodeid_t		nodeid;
	scstat_ds_t		*status = NULL;
	scswitch_service_t	servicelist;
	rval_e			rval = SDSSC_ERROR;

	if (take_op == Shutdown_Services) {
		scconf_cltr_releasehandle(0);
		return (SDSSC_OKAY);
	}

	if (scstat_get_ds_status((scstat_ds_name_t *)service,
	    &status) != SCSTAT_ENOERR)
		goto exit_routine;

	servicelist.scswitch_service_name = (char *)service;
	servicelist.next = NULL;

	if (take_op == Make_Primary) {
		if (scconf_get_nodeid(NULL, &nodeid) != SCCONF_NOERR)
			goto exit_routine;

		if (scconf_get_nodename(nodeid, &nodename) !=
		    SCCONF_NOERR)
			goto exit_routine;

		if (scswitch_switch_service(&servicelist, nodename) !=
		    SCCONF_NOERR)
			goto exit_routine;

		rval = SDSSC_OKAY;
	} else if (take_op == Release_Primary) {
		/*
		 * If it is already offline
		 */
		if (status->scstat_ds_status == SCSTAT_OFFLINE)
			rval = SDSSC_OKAY;
		else if (scswitch_take_service_offline(&servicelist)
			== SCSWITCH_NOERR)
			rval = SDSSC_OKAY;
	}

exit_routine:
	if (nodename)
		free(nodename);
	if (status)
		scstat_free_ds_status(status);
	return (rval);
}
