/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)binddevs.c	1.21	08/05/20 SMI"

#define	CLUSTER_LIBRARY_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <metacl.h>
#include <sdssc.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <syslog.h>
#endif

/* Forward declarations */
mdc_errno_t verify_clustered(int hostnumber);
mdc_errno_t verify_set(char *cur_set, char *cur_number);
void augment_name_list(char *name);
void free_name_list(void);
void scrub_directory();

#if SOL_VERSION >= __s10
/*
 * Maximum number of shared metaset.
 */
#define	SDSSC_SHARED_METASET_MAX	32
mdc_errno_t verify_dev(char *target, mode_t mode, dev_t dev);
mdc_errno_t verify_set_devs(char *setnumber, scconf_cfg_prop_t *props);
#if SOL_VERSION > __s10
mdc_errno_t sdssc_makegdev(int set_number, int disk_number, dev_t dev,
							char *device_name);
#else
mdc_errno_t sdssc_makegdev(int set_number, int disk_number, dev_t dev);
#endif
static int sdssc_gdev_exist(dev_t dev);
static int sdssc_replace_props(int set, dc_property_seq_t *proplist);
static dc_property_seq_t *disk_list[SDSSC_SHARED_METASET_MAX];
#endif /* SOL_VERSION >= __s10 */

/* Malloc defines */
#define	BUFENTRIES	100
#define	BUFINCREMENT	50

extern char *service_table[];



/*
 * This function thinks globally and acts locally. It inquires of
 * libdcs in order to acquire a list of disk set names (services in
 * the the SDS class). It then inspects the local /dev/md directory
 * to confirm or repair the symlinks that point to the various global
 * diskset directories.
 */
mdc_errno_t
_sdssc_binddevs()
{
	mdc_errno_t	return_value = MDC_NOERROR;
	unsigned int   	counter;
	char		*current_set,
			*current_number,
			**i;
	int		hostnumber;
	scconf_cfg_ds_t	*sn,
			*n;
	scconf_nodeid_t	nodeid;

	/* Get the node number of this host. */
	if (scconf_get_nodeid(NULL, &nodeid) != SCCONF_NOERR) {
		return (MDC_NOTINCLUSTER);
	} else {
		hostnumber = (int)nodeid;
	}

	/*
	 * At this point we know that the clustering software thinks we are
	 * clustered and also that there is at least a defined class for
	 * disksets. So this verifies the generic cluster links.
	 */
	switch (verify_clustered(hostnumber)) {
		case MDC_NOERROR:
			break;

		case MDC_NOACCESS:
			return (BIND_NOACCESS_SHARED);

		case BIND_LINKISDIR:
			return (BIND_LOCALSET);

		default:
			break;
	}

	/*
	 * Initialize the name_list for the scrub at the end of the
	 * binding.
	 */
	augment_name_list(".");
	augment_name_list("..");
	augment_name_list("admin");
	augment_name_list("dsk");
	augment_name_list("rdsk");
	augment_name_list("shared");

	/*
	 * Go through the list of disk set names and get the
	 * corresponding numbers. Given the name/number pairs confirm
	 * the symlinks to assure that the global names are visible
	 * from the local host.
	 */

	for (i = service_table; *i != NULL; i++) {
		if (scconf_get_ds_config(*i, SCCONF_BY_TYPE_FLAG,
	    &sn) != SCCONF_NOERR) {
			scconf_free_ds_config(sn);
			return (MDC_NOTINCLUSTER);
		}
		n = sn;
		while (n) {

			augment_name_list(n->scconf_ds_name);

			current_set = n->scconf_ds_name;
			if (l_get_property(n, SDSSC_PROP_INDEX, &current_number)
			    == SDSSC_ERROR) {
				return_value = BIND_NOACCESS_SHARED;
				break;
			}

			/* Verify the symlink for this Disk Set. */
			switch (verify_set(current_set, current_number)) {
			case MDC_NOACCESS:
				return_value = BIND_NOACCESS_DEVICE;
				break;

			case MDC_NOERROR:
				break;

			default:
				return_value = BIND_BADDEVICE;
				break;
			}

			if (return_value != MDC_NOERROR) {
				break;
			}
#if SOL_VERSION >= __s10
			/*
			 * Verify that the global device nodes and links for
			 * this set exist and are correct. Create the nodes
			 * and links if needed.
			 */
			verify_set_devs(current_number,
			    n->scconf_ds_propertylist);
#endif
			n = n->scconf_ds_next;
		}
		scconf_free_ds_config(sn);
	}

	/* This may go up a bit */
	scrub_directory();
	free_name_list();

	return (return_value);

}

/*
 * Verify that the provided target is in fact linked to the provided
 * source. If it isn't, try to construct such a link.
 */
mdc_errno_t
verify_symlink(char *source, char *target)
{
	struct stat statbuf;
	int is_a_directory = 0;	/* The link is really a directory. */
	int is_populated = 0;	/* The link is a populated directory. */
	int is_present = 0;	/* A thing of some sort is in place. */
	int is_a_link = 0;	/* It is a symlink. */
	int is_valid = 0;	/* Symlink is correct in every way. */

	int n;

	char buf[PATH_MAX];	/* Buffer for the actual symlink source. */
	size_t path_length = PATH_MAX-1;
	char *strptr;		/* scratch pointer */


	/*
	 * First stat the link to global namespace. If no error, we move
	 * on to check the source of the link.
	 */
	if (lstat(target, &statbuf) == -1) {
		switch (errno) {
		case EACCES:
		case ENOTDIR:
		case EINTR:
			/* There's nothing we can do about this. */
			return (MDC_NOACCESS);

		case ELOOP:
		case EMULTIHOP:
		case ENOLINK:
			/*
			 * These indicate the current symlink is broken but
			 * possibly repairable.
			 */
			is_present = 1;
			break;

		case ENOENT:
			/* The link does not currently exist. */
			break;

		default:
			is_present = 1;
		}
	} else {
		is_present = 1;
	}

	/* Now confirm that this is really a symbolic link. */
	if (is_present) {
		if (!(statbuf.st_mode & S_IFLNK)) {
			if (S_ISDIR(statbuf.st_mode)) {
				DIR *dirp;
				dirent_t *dp;

				is_a_directory = 1;

				/*
				 * It's a directory but is it populated?
				 * If so, we're wedged because there's no
				 * way to know if the set numbers in this
				 * directory are synchronized with the set
				 * numbers in the global namespace. If
				 * not, this can be  unlinked.
				 */

				dirp = opendir(target);

				while ((dp = readdir(dirp)) != NULL)
					if (strcmp(dp->d_name, ".") == 0 ||
					    strcmp(dp->d_name, "..") == 0) {
						continue;
					} else {
						is_populated = 1;
						break;
					}

				closedir(dirp);

			}
		} else {
			/*
			 * So this is a symlink to something. Now we
			 * check what it's a link to and determine
			 * if that's OK.
			 */
			size_t link_length;

			is_a_link = 1;
			link_length = strlen(target);

			n = readlink(target, buf, path_length);

			if (strncmp(buf, source, link_length) == 0) {
				/* This symlink is entirely OK. */
				is_valid = 1;
			}
		}
	}

	/*
	 * If there's something there and it isn't valid we try to
	 * clear it out of the way here. The only thing we won't clear
	 * is a populated directory. If there's a populated directory
	 * in place, we have no idea what that means. Do we relocate
	 * it to the global space, is it from an old version, is it
	 * just a big mistake. We return the error and let the user
	 * sort that one out.
	 */
	if (is_present && !is_valid) {
		if (is_a_directory) {
			if (!is_populated) {
				rmdir(target);
			} else {
				return (BIND_LINKISDIR);
			}
		} else {
			unlink(target);
		}

		is_present = 0;
		is_a_directory = 0;
	}

	/*
	 * Make sure that the directory to hold the link is present.
	 */
	strcpy(buf, target);
	strptr = strrchr(buf, '/');
	if (strptr != NULL) {
		*strptr = 0;
		if (lstat(buf, &statbuf) == -1) {
			if (errno == ENOENT) {
				if (mkdirp(buf, 0755) == -1)
					return (MDC_NOACCESS);
			} else
				return (MDC_NOACCESS);
		} else {
			if (!S_ISDIR(statbuf.st_mode)) {
				return (MDC_NOACCESS);
			}
		}
	}

	/*
	 * At this point there is no valid symlink in place and there's
	 * also no other filesystem artifact to prevent us from creating
	 * the correct symlink so we create the symlink here.
	 */
	if (!is_valid)
		symlink(source, target);

	return (MDC_NOERROR);
}

/*
 * This verifies the symlink that connects the local shared directory
 * to the global directory.
 */

mdc_errno_t
verify_clustered(int hostnumber) {
	char globbuf[PATH_MAX];
	char *devbuf = SDSSC_SET_PATH;	/* where the set numbers are */

	/*
	 * First, format the link-referred general-purpose global path
	 * for this host.
	 */
	sprintf(globbuf, "../..%s%d", SDSSC_CL_GLOBAL, hostnumber);

	/* Now tag on the shared directory. */
	strcat(globbuf, SDSSC_SET_PATH);

	return (verify_symlink(globbuf, devbuf));
}

/*
 * This verifies a symlink from a particular set name to its associated
 * number.
 */
mdc_errno_t
verify_set(char *setname, char *setnumber) {
	char numbuf[PATH_MAX];
	char namebuf[PATH_MAX];

	sprintf(numbuf, "%s/%s", SDSSC_SETNO_LINK, setnumber);

	sprintf(namebuf, "%s/%s", SDSSC_MD_DIR, setname);

	return (verify_symlink(numbuf, namebuf));
}

void
remove_regardless(char *dir_entry) {
	char cmd[PATH_MAX+13];
	int  i;

	struct stat statbuf;

	if (lstat(dir_entry, &statbuf) == -1) {
		return;
	}

	if (S_ISDIR(statbuf.st_mode)) {
		(void) sprintf(cmd, "/bin/rm -rf %s", dir_entry);

		/*
		 * This might not work, but there's nothing we can
		 * do about it, so we ignore the return code for now.
		 */
		(void) system(cmd);
	} else {
		unlink(dir_entry);
	}
}

char **		name_list = NULL;

/*
 * This makes sure that only valid entries are in the directory. If
 * a directory entry is found that does not conform to our current
 * state, that entry is deleted.
 */
void
scrub_directory()
{
	boolean_t	entry_OK;
	char		full_path[PATH_MAX];
	DIR *dirp;
	dirent_t *dp;
	dirp = opendir(SDSSC_MD_DIR);

	while ((dp = readdir(dirp)) != NULL) {
		int count;

		entry_OK = B_FALSE;

		/* Compare this directory entry to all OK entries. */
		for (count = 0; name_list[count] != NULL; count++) {
			if (strcmp(name_list[count], dp->d_name) == 0) {
				entry_OK = B_TRUE;
				break;
			}
		}

		if (entry_OK == B_TRUE) {
			continue;
		}

		/* If we're here, it's a bad entry and must be expunged. */

		sprintf(full_path, "%s/%s", SDSSC_MD_DIR, dp->d_name);

		remove_regardless(full_path);
	}

	closedir(dirp);
}

/*
 * This adds a valid name to the list
 */
void
augment_name_list(char *name)
{
	static int entry_count;
	static int entries_allocated;

	if (name_list == NULL) {
		entry_count = 0;
		entries_allocated = BUFENTRIES;
		name_list = (char **)malloc(entries_allocated *
					    sizeof (char **));
	}

	name_list[entry_count++] = strdup(name);

	if (entry_count == entries_allocated) {
		entries_allocated = entries_allocated + BUFINCREMENT;
		name_list = realloc(name_list,
				    (entries_allocated * sizeof (char **)));
	}

	name_list[entry_count] = NULL;
}

/*
 * This frees the name list.
 */
void
free_name_list()
{
	int entry_count;

	for (entry_count = 0;
		name_list[entry_count] != NULL;
		entry_count++) {
		free(name_list[entry_count]);
	}

	free(name_list);

	name_list = NULL;
}

#if SOL_VERSION >= __s10
/*
 * make global device nodes and links for SVM metadisks.
 */
#define	SVMBLKDEV	"/devices/pseudo/md@0:%d,%d,blk"
#define	SVMCHRDEV	"/devices/pseudo/md@0:%d,%d,raw"
#if SOL_VERSION > __s10
#define	SVMBLKLINK	"%s%d/dev/md/shared/%d/dsk/%s"
#define	SVMCHRLINK	"%s%d/dev/md/shared/%d/rdsk/%s"
#else
#define	SVMBLKLINK	"%s%d/dev/md/shared/%d/dsk/d%d"
#define	SVMCHRLINK	"%s%d/dev/md/shared/%d/rdsk/d%d"
#endif
#define	SVM_ADMIN	"/devices/pseudo/md@0:admin"
#define	DEFAULTMODE 0640
#define	DEFAULT_MAJOR	(major_t)85

/*
 * Find the Major number of SVM by stating the admin instance of the file.
 */
major_t
find_svm_maj() {
	struct stat admin_stat;
	int stat_err;
	stat_err = stat(SVM_ADMIN, &admin_stat);
	if (stat_err == -1) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "stat %s failed: errno %d\n", SVM_ADMIN, errno);
		return (DEFAULT_MAJOR);
	}
	if ((admin_stat.st_mode & S_IFMT) != S_IFCHR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_ERR, "%s: not a Charater device\n", SVM_ADMIN);
		return (DEFAULT_MAJOR);
	}
	return (major(admin_stat.st_rdev));
}

/*
 * Given a set number, ensure that all of the configured metadisks
 * have global device node and links. This is called at boot by
 * metaclusterbinddevs.
 */
mdc_errno_t
verify_set_devs(char *setnumberbuf, scconf_cfg_prop_t *props) {
	int setnumber;
	dev_t dev;
	char *disk;
	int disk_number;
	major_t svm_maj;
	char device_name[PATH_MAX];

	setnumber = atoi(setnumberbuf);
	svm_maj = find_svm_maj();
	while (props) {
		if (strncmp("d_", props->scconf_prop_key, 2) == 0) {
			disk = props->scconf_prop_key + 2;
			disk_number = atoi(disk);
			dev = makedev(svm_maj,
			    (minor_t)(SDSSC_METADDEV_MAX * setnumber +
			    disk_number));
#if SOL_VERSION > __s10
			if (props->scconf_prop_value == NULL ||
			    (strcmp(props->scconf_prop_value, "") == 0)) {
				(void) snprintf(device_name, PATH_MAX, "d%d",
								disk_number);
				sdssc_makegdev(setnumber, disk_number, dev,
								device_name);
			} else {
				sdssc_makegdev(setnumber, disk_number, dev,
						props->scconf_prop_value);
			}
#else /* !SOL_VERSION > __s10 */
			sdssc_makegdev(setnumber, disk_number, dev);
#endif /* !SOL_VERSION > __s10 */
		}
		props = props->scconf_prop_next;
	}
}

/*
 * make the character and block device global devices for the given metadisk.
 * called from both devfsadm and metaclusterbind.
 */
#if SOL_VERSION > __s10
mdc_errno_t
sdssc_makegdev(int set_number, int disk_number, dev_t dev, char *device_name)
#else
mdc_errno_t
sdssc_makegdev(int set_number, int disk_number, dev_t dev)
#endif
{
	mode_t mode;
	int hostnumber;
	char devbuf[PATH_MAX];
	char linkbuf[PATH_MAX];
	scconf_nodeid_t nodeid;

	if (scconf_get_nodeid(NULL, &nodeid) != SCCONF_NOERR) {
		/*
		 * Can't run if we are not in cluster mode.
		 */
		exit(1);
	}
	hostnumber = (int)nodeid;

	/*
	 * block special device node and link.
	 */
	sprintf(devbuf, "%s%d" SVMBLKDEV, SDSSC_CL_GLOBAL, hostnumber,
	    set_number, disk_number);
#if SOL_VERSION > __s10
	sprintf(linkbuf, SVMBLKLINK, SDSSC_CL_GLOBAL, hostnumber, set_number,
	    device_name);
#else
	sprintf(linkbuf, SVMBLKLINK, SDSSC_CL_GLOBAL, hostnumber, set_number,
	    disk_number);
#endif
	verify_dev(devbuf, S_IFBLK | DEFAULTMODE, dev);
	sprintf(devbuf, "../../../../.." SVMBLKDEV, set_number, disk_number);
	verify_symlink(devbuf, linkbuf);

	/*
	 * character special node and link.
	 */
	sprintf(devbuf, "%s%d" SVMCHRDEV, SDSSC_CL_GLOBAL, hostnumber,
	    set_number, disk_number);
#if SOL_VERSION > __s10
	sprintf(linkbuf, SVMCHRLINK, SDSSC_CL_GLOBAL, hostnumber, set_number,
	    device_name);
#else
	sprintf(linkbuf, SVMCHRLINK, SDSSC_CL_GLOBAL, hostnumber, set_number,
	    disk_number);
#endif
	verify_dev(devbuf, S_IFCHR | DEFAULTMODE, dev);
	sprintf(devbuf, "../../../../.." SVMCHRDEV, set_number, disk_number);
	verify_symlink(devbuf, linkbuf);
	return (MDC_NOERROR);
}

/*
 * Verify that the provided device node exists. If it does not,
 * make the node.
 */
mdc_errno_t
verify_dev(char *target, mode_t mode, dev_t dev)
{
	struct stat statbuf;

	/*
	 * First stat the device node in the global namespace.
	 * If it exists, then we leave the existing dev there.
	 */
	if (lstat(target, &statbuf) == -1) {
		switch (errno) {
		case EACCES:
		case ENOTDIR:
		case EINTR:
			/* There's nothing we can do about this. */
			return (MDC_NOACCESS);

		case ELOOP:
		case EMULTIHOP:
		case ENOLINK:
			return (MDC_NOERROR);

		case ENOENT:
			/* The link does not currently exist. */
			break;

		default:
			return (MDC_NOERROR);
		}
	} else {
		return (MDC_NOERROR);
	}

	/* Device node does not exist. Create it. */

	if (mknod(target, mode, dev) == -1) {
		switch errno {
		case EACCES:
			return (MDC_NOACCESS);
		default:
			return (MDC_NOACCESS);
		}
	}
	return (MDC_NOERROR);
}

#if SOL_VERSION > __s10
void
sdssc_create_device_nodes(dev_t dev, int set, char *device_name)
#else
void
sdssc_create_device_nodes(dev_t dev, int set)
#endif
{
	char *type, *dir;
	dc_error_t dc_err = 0;
	dc_property_seq_t *meta_disk_list = NULL;

#if SOL_VERSION > __s10
	/*
	 * For Friendly names this test for existance is more complicated
	 * so for now, just always call DCS. Experience has indicated that
	 * every time devfsadm gets events, it reloads the modules, so this
	 * cache is cleared anyway.
	 */
	{
		dc_err = dcs_create_device_nodes(dev, &meta_disk_list,
								device_name);
#else
	if (!sdssc_gdev_exist(dev)) {
		dc_err = dcs_create_device_nodes(dev, &meta_disk_list);
#endif
		if (dc_err != DCS_SUCCESS) {
			syslog(LOG_ERR, dcs_error_to_string(dc_err));
		}
		sdssc_replace_props(set, meta_disk_list);
	}
}

/*
 * check to see if the links for this device has already been created.
 */
static int
sdssc_gdev_exist(dev_t dev)
{
	char prop_string[8];
	int i;
	minor_t meta_minor;
	int metadisk;
	int metaset;
	dc_property_seq_t *props;

	meta_minor = minor(dev);
	metadisk = meta_minor % SDSSC_METADDEV_MAX;
	metaset = meta_minor / SDSSC_METADDEV_MAX;

	sprintf(prop_string, "d_%d", metadisk);

	props = disk_list[metaset];
	if (props == NULL)
		return (0);
	for (i = 0; i < props->count; i++) {
		if (strcmp(prop_string, props->properties[i].name) == 0)
			return (1);
	}
	return (0);
}

/*
 * cache the list of devices already created.
 */
static int
sdssc_replace_props(int set, dc_property_seq_t *proplist)
{
	dcs_free_properties(disk_list[set]);
	disk_list[set] = proplist;
}
#endif /* SOL_VERSION >= __s10 */
