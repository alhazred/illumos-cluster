/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)cladmin.c 1.16     08/05/20 SMI"

/*
 * cladmin support routines
 */

#define	CLUSTER_LIBRARY_SOURCE
#include <meta.h>
#include <sdssc.h>
#include <sys/cladm.h>
#include <sys/cladm_int.h>

/*
 * cladmin_node_id -- return the current node id
 */
rval_e
cladmin_node_id(int *nid)
{
	return (_cladm(CL_CONFIG, CL_NODEID, nid) ? SDSSC_ERROR : SDSSC_OKAY);
}

/*
 * cladmin_name_list_start -- create a list of nodeid/hostname pairs
 */
rval_e
cladmin_name_list_start(void **cookie)
{
	clnode_name_t	*node_list;
	int		highest,
			x;
	clnode_name_t	*np;

	*cookie = (void *)0;
	if (_cladm(CL_CONFIG, CL_HIGHEST_NODEID, &highest) != 0)
		return (SDSSC_ERROR);

	/*
	 * Allocate space for nodes plus one which we'll used for a
	 * end of the list indicator.
	 */
	if ((node_list = (clnode_name_t *)calloc(highest + 1,
	    sizeof (clnode_name_t))) == NULL) {
		return (SDSSC_ERROR);
	}

	memset(node_list, 0, sizeof (clnode_name_t) * (highest + 1));

	for (x = 0, np = node_list; x < highest; x++) {
		np->nodeid = x + 1;	/* nodes start at 1 not zero */

		if ((np->name = (char *)malloc(SDSSC_NODE_NAME_LEN)) == NULL) {
			return (SDSSC_ERROR);
		}

		memset(np->name, 0, SDSSC_NODE_NAME_LEN);
		np->len = SDSSC_NODE_NAME_LEN;

		if (_cladm(CL_CONFIG, CL_GET_NODE_NAME, np) != 0)
			return (SDSSC_ERROR);

		/*
		 * Only advance the pointer when a valid entry was returned
		 * from _cladm(). Otherwise free the memory and continue.
		 * NOTE: we could skip the following free and just made an
		 * extra check above to see if np->name was zero before
		 * allocating memory. The only problem with that is the end
		 * case which might have memory allocated to np->name, but
		 * nothing in stored there.
		 */
		if (np->len)
			np++;
		else {
			free(np->name);
			np->name = (char *)0;
		}
	}

	*cookie = (void *)node_list;
	return (SDSSC_OKAY);
}

/*
 * cladmin_name_list_end -- free space allocated by cladmin_name_list_start
 */
void
cladmin_name_list_end(void *cookie)
{
	clnode_name_t	*p;

	if (cookie != (void *)0) {
		for (p = (clnode_name_t *)cookie; p->name; p++) {
			free(p->name);
		}
		free(cookie);
	}
}

/*
 * cladmin_name_to_id -- scan though list to determine node id from host
 */
int
cladmin_name_to_id(void *cookie, char *host)
{
	clnode_name_t	*p;

	for (p = (clnode_name_t *)cookie; p->name; p++) {
		if (strcmp(p->name, host) == 0) {
			return (p->nodeid);
		}
	}
	return (0);		/* invalid node id */
}

/*
 * cladmin_id_to_name -- given node id return name
 */
char *
cladmin_id_to_name(int nid)
{
	clnode_name_t	n;

	n.nodeid = nid;
	if ((n.name = (char *)malloc(SDSSC_NODE_NAME_LEN)) == NULL)
		return ((char *)0);
	n.len = SDSSC_NODE_NAME_LEN;

	if (_cladm(CL_CONFIG, CL_GET_NODE_NAME, &n) == 0)
		return (n.name);
	else {
		free(n.name);
		return ((char *)0);
	}
}
