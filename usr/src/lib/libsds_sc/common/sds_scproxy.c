/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sds_scproxy.c	1.27	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/utsname.h>
#include <locale.h>
#include <sys/types.h>

#define	CLUSTER_LIBRARY_SOURCE
#include <meta.h>
#include <sdssc.h>

/* These variables need to get from eval_env to setup_env */
char *path = NULL;
char *dsp = NULL, *lang = NULL, *term = NULL, *nlspath = NULL;
char *lc_all = NULL, *lc_collate = NULL;
char *lc_ctype = NULL, *lc_messages = NULL;
char *lc_monetary = NULL, *lc_numeric = NULL, *lc_time = NULL;

static int
eval_env()
{
	int   env_count = 0;

	if ((path = getenv("PATH")) != (char *)NULL)
		env_count++;
	if ((dsp = getenv("DISPLAY")) != (char *)NULL)
		env_count++;
	if ((lang = getenv("LANG")) != (char *)NULL)
		env_count++;
	if ((nlspath = getenv("NLSPATH")) != (char *)NULL)
		env_count++;
	if ((term = getenv("TERM")) != (char *)NULL)
		env_count++;
	if ((lc_all = setlocale(LC_ALL, NULL)) != (char *)NULL)
		env_count++;
	if ((lc_collate = setlocale(LC_COLLATE, NULL)) != (char *)NULL)
		env_count++;
	if ((lc_ctype = setlocale(LC_CTYPE, NULL)) != (char *)NULL)
		env_count++;
	if ((lc_messages = setlocale(LC_MESSAGES, NULL)) != (char *)NULL)
		env_count++;
	if ((lc_monetary = setlocale(LC_MONETARY, NULL)) != (char *)NULL)
		env_count++;
	if ((lc_numeric = setlocale(LC_NUMERIC, NULL)) != (char *)NULL)
		env_count++;
	if ((lc_time = setlocale(LC_TIME, NULL)) != (char *)NULL)
		env_count++;

	return (env_count);
}

static char **
setup_env(int env_count)
{
	int lstr, entry_no = 0;
	char **return_ptr;

	if ((return_ptr = (char **)malloc(sizeof (char *) *
	    (env_count + 1))) == NULL)
		return (NULL);

	if (path != (char *)NULL) {
		lstr = strlen(path) + strlen("PATH=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "PATH=%s", path);
	}

	if (dsp != (char *)NULL) {
		lstr = strlen(dsp) + strlen("DISPLAY=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "DISPLAY=%s", dsp);
	}

	if (lang != (char *)NULL) {
		lstr = strlen(lang) + strlen("LANG=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "LANG=%s", lang);
	}

	if (nlspath != (char *)NULL) {
		lstr = strlen(nlspath) + strlen("NLSPATH=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "NLSPATH=%s", nlspath);
	}

	if (term != (char *)NULL) {
		lstr = strlen(term) + strlen("TERM=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "TERM=%s", term);
	}

	if (lc_all != (char *)NULL) {
		lstr = strlen(lc_all) + strlen("LC_ALL=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "LC_ALL=%s", lc_all);
	}

	if (lc_collate != (char *)NULL) {
		lstr = strlen(lc_collate) + strlen("LC_COLLATE=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "LC_COLLATE=%s",
			lc_collate);
	}

	if (lc_ctype != (char *)NULL) {
		lstr = strlen(lc_ctype) + strlen("LC_CTYPE=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "LC_CTYPE=%s", lc_ctype);
	}

	if (lc_messages != (char *)NULL) {
		lstr = strlen(lc_messages) + strlen("LC_MESSAGES=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "LC_MESSAGES=%s",
			lc_messages);
	}

	if (lc_monetary != (char *)NULL) {
		lstr = strlen(lc_monetary) + strlen("LC_MONETARY=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "LC_MONETARY=%s",
			lc_monetary);
	}


	if (lc_numeric != (char *)NULL) {
		lstr = strlen(lc_numeric) + strlen("LC_NUMERIC=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "LC_NUMERIC=%s",
			lc_numeric);
	}

	if (lc_time != (char *)NULL) {
		lstr = strlen(lc_time) + strlen("LC_TIME=") + 1;
		return_ptr[entry_no] = (char *)malloc(lstr);
		sprintf(return_ptr[entry_no++], "LC_TIME=%s", lc_time);
	}

	/* Terminate the list. */
	return_ptr[entry_no] = NULL;

	return (return_ptr);
}

static void
list_free(char **list)
{
	char **original = list;

	while (*list != NULL) {
		free((char *)(*list));
		list++;
	}

	free(original);
}

rval_e
_sdssc_cmd_proxy(int argc, char  *argv[], char  *hostname, int *error)
{
	uint_t	list_count = 0,
			env_count;
	int		i;
	char		**argvlist,
			**environment,
			*cmd,
			c,
			primary_node[SDSSC_NODE_NAME_LEN],
			*set_name = NULL,
			*transportname;
	struct utsname	myname;
	sdssc_version_t ver;
	mdc_err_t	*ep;

	/* The command is proxied only if major number is at least 3 */
	_sdssc_version(&ver);
	if (ver.major < 3)
		return (SDSSC_OKAY);

	optind = 1;
	opterr = 0;

	/* There must be a set specified in the argv */
	while ((c = getopt(argc, argv, "s:")) != -1) {
		switch (c) {
			case 's':
				set_name = optarg;
				break;
		}
	}

	/* Identify the proxy host. */
	if (hostname == SDSSC_PROXY_PRIMARY) {
		if (set_name == NULL) {
			/*
			 * If no hostname was provided then a set_name
			 * is required to determine where to send the
			 * command. If neither then just return.
			 */

			return (SDSSC_OKAY);
		}
		if (_sdssc_get_primary_host(set_name, primary_node,
		    SDSSC_NODE_NAME_LEN) != SDSSC_OKAY)
			return (SDSSC_OKAY);
		hostname = primary_node;
	}

	/* Identify the current host. */
	if (uname(&myname) == -1)
		return (SDSSC_OKAY);

	/* If they're the same, there's no need to proxy. */
	if (strcmp(myname.nodename, hostname) == 0)
		return (SDSSC_OKAY);

	/*
	 * OK, they aren't the same, so we need to construct the
	 * argument list. First we allocate the space for the list
	 * of pointers.
	 */
	if ((argvlist =
	    (char **)malloc(sizeof (char *) * (argc + 1))) == NULL)
	    return (SDSSC_OKAY);

	if ((ep =
	    (mdc_err_t *)malloc(sizeof (mdc_err_t))) == NULL)
		return (SDSSC_OKAY);

	memset(ep, 0, sizeof (mdc_err_t));

	/*
	 * Check to see if argv[0] is a relative path. If
	 * so we need to prepend  the full path name to it.
	 */
	if (*argv[0] != '/') {
		if ((cmd = (char *)malloc(strlen(argv[0])
		    + strlen("/usr/sbin/") + 1)) == NULL)
			return (SDSSC_OKAY);
		(void) sprintf(cmd, "/usr/sbin/%s", argv[0]);
	} else {
		cmd = argv[0];
	}

	/*
	 * Now construct the environment array. First count
	 * valid entries then allocate the space and fill the array.
	 */
	env_count = eval_env();
	environment = setup_env(env_count);

	/* Insert the command into the argument list. */
	argvlist[0] = (char *)strdup(cmd);

	/* Load each argument into the list. */
	for (list_count = 1; list_count < argc; list_count++)
	    argvlist[list_count] =
		    (char *)strdup(argv[list_count]);

	/* Terminate the list. */
	argvlist[list_count] = NULL;

	transportname = strdup(hostname);

	printf(gettext("Proxy command to: %s\n"), hostname);

	/* Execute the command on the proxy. */
	(void) _sdssc_clnt_proxy_cmd(list_count + 1, argvlist,
	    env_count + 1, environment, transportname, ep);

	/* Free all that memory we malloc'ed */
	list_free(argvlist);
	list_free(environment);

	/*
	 * mdc_misc will contain any output from the remote command so we
	 * need to display it now.
	 */
	printf("%s", ep->mdc_misc);

	*error = ep->mdc_exitcode;

	free(ep);
	free(transportname);

	return (SDSSC_PROXY_DONE);
}
