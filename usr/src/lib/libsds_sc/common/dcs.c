/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)dcs.c 1.14     08/05/20 SMI"

/*
 * Set of routines to help us emulate libdcs for our debugging
 * purposes.
 *
 * NOTE: A lot of leeway was given to these routine in terms of
 *	error checking.
 */

#define	CLUSTER_LIBRARY_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/utsname.h>
#include <fcntl.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <strings.h>
#include <libdcs.h>
#include <sys/cladm.h>
#include <sys/cladm_int.h>

/*
 * test directory to hold service files.
 */

#define	COMMON_CLUSTER_PATH "/tmp/cluster"
#define	DCS_LINE_SIZE	128

static char *
l_change_prop(const dc_property_seq_t *props, const char *name_value)
{
	int		x;
	char		*new_prop;
	dc_property_t	*pp;

	new_prop = (char *)malloc(DCS_LINE_SIZE);

	for (x = 0, pp = props->properties; x < props->count; x++, pp) {
		if (strncmp(name_value, pp->name, strlen(pp->name)) == 0) {
			sprintf(new_prop, "prop=%s,%s\n", pp->name, pp->value);
			return (new_prop);
		}
	}

	sprintf(new_prop, "prop=%s", name_value);
	return (new_prop);
}

int
dcs_initialize(void)
{
	return (0);
}

const char *
dcs_error_to_string(dc_error_t foobar)
{
	return ("Better luck next time");
}

dc_error_t
dcs_create_service_class(const char *class, const char *command,
    dc_ha_type_t type)
{
	struct stat	s;
	char		classpath[MAXPATHLEN];

	sprintf(classpath, "%s/%s", COMMON_CLUSTER_PATH, class);
	if (stat(classpath, &s) == -1) {
		/*
		 * Doesn't exist so create it.
		 */
		if (mkdir(classpath, 0755) == -1) {
			printf("Failed to create %s\n", classpath);
			return (DCS_ERR_SERVICE_CLASS_NAME);
		}
	} else {
		return (DCS_SUCCESS);
	}
}

dc_error_t
dcs_set_service_class_user_program(const char *class, const char *prog)
{
	return (DCS_ERR_CCR_ACCESS);
}

dc_error_t
dcs_create_service(const char *service, const char *class, int failback,
    const dc_replica_seq_t *nodes, unsigned int num_secs,
    const dc_gdev_range_seq_t *gdev, const dc_property_seq_t *props)
{
	struct stat	s;
	char		servicepath[MAXPATHLEN],
			data[DCS_LINE_SIZE];
	int		fd,
			count;
	dc_replica_t	*r;
	dc_property_t	*p;

	sprintf(servicepath, "%s/%s/%s", COMMON_CLUSTER_PATH, class, service);
	if ((fd = open(servicepath, O_RDWR | O_CREAT | O_EXCL, 0644)) == -1) {
		printf("Can't open %s\n", servicepath);
		return (DCS_ERR_SERVICE_NAME);
	}

	for (r = nodes->replicas, count = nodes->count; count; r++, count--) {
		sprintf(data, "node=%d,%d,SECONDARY\n", r->id, r->preference);
		write(fd, data, strlen(data));
	}

	for (p = props->properties, count = props->count;
	    count; p++, count--) {
		sprintf(data, "prop=%s,%s\n", p->name, p->value);
		write(fd, data, strlen(data));
	}

	close(fd);
	return (DCS_SUCCESS);
}

dc_error_t
dcs_remove_service(const char *service)
{
	char	servicepath[MAXPATHLEN];

	sprintf(servicepath, "%s/SUNWmd/%s", COMMON_CLUSTER_PATH, service);
	return (unlink(servicepath) ? DCS_ERR_SERVICE_NAME : DCS_SUCCESS);
}

dc_error_t
dcs_get_service_names_of_class(const char *class, dc_string_seq_t **list)
{
	DIR		*dirp;
	char		classpath[MAXPATHLEN];
	int		nentries;
	struct dirent	*dp;
	dc_string_seq_t	*lp;

	sprintf(classpath, "%s/%s", COMMON_CLUSTER_PATH, class);
	if ((dirp = opendir(classpath)) == NULL) {
		return (DCS_ERR_SERVICE_CLASS_NAME);
	}

	nentries = 0;
	while ((dp = readdir(dirp)) != NULL) {
		if ((strcmp(dp->d_name, ".") == 0) ||
		    (strcmp(dp->d_name, "..") == 0))
			continue;
		nentries++;
	}
	rewinddir(dirp);

	lp = (dc_string_seq_t *)malloc(sizeof (dc_string_seq_t));
	lp->strings = (char **)calloc(nentries, sizeof (char *));
	lp->count = nentries;

	nentries = 0;
	while ((dp = readdir(dirp)) != NULL) {
		if ((strcmp(dp->d_name, ".") == 0) ||
		    (strcmp(dp->d_name, "..") == 0))
			continue;
		lp->strings[nentries] = (char *)malloc(strlen(dp->d_name) + 1);
		strcpy(lp->strings[nentries], dp->d_name);
		nentries++;
	}

	*list = lp;
	closedir(dirp);
	return (DCS_SUCCESS);
}

dc_error_t
dcs_get_property(const char *service, const char *name, char **value)
{
	char	servicepath[MAXPATHLEN],
		data[DCS_LINE_SIZE],
		*p;
	FILE	*fp;

	sprintf(servicepath, "%s/SUNWmd/%s", COMMON_CLUSTER_PATH, service);
	if ((fp = fopen(servicepath, "r")) ==  NULL)
		return (DCS_ERR_SERVICE_CLASS_NAME);

	while (fgets(data, DCS_LINE_SIZE, fp) != NULL) {
		if (strncmp(data, "prop=", 5) == 0) {
			p = strchr(data, '=') + 1;
			if (strncmp(name, p, strlen(name)) == 0) {
				p = strchr(p, ',') + 1;
				*value = (char *)malloc(strlen(p));
				strcpy(*value, p);
				if (p = strchr(*value, '\n'))
					*p = '\0';
				fclose(fp);
				return (DCS_SUCCESS);
			}
		}
	}
	printf("%s: not prop %s\n", service, name);
	*value = '\0';
	fclose(fp);
	return (DCS_ERR_PROPERTY_NAME);
}

dc_error_t
dcs_set_properties(const char *service, const dc_property_seq_t *props)
{
	char	read_servicepath[MAXPATHLEN],
		write_servicepath[MAXPATHLEN],
		data[DCS_LINE_SIZE],
		*p;
	FILE	*read_service,
		*write_service;

	sprintf(read_servicepath, "%s/SUNWmd/%s", COMMON_CLUSTER_PATH,
		service);
	if ((read_service = fopen(read_servicepath, "r")) == NULL)
		return (DCS_ERR_SERVICE_NAME);

	sprintf(write_servicepath, "%s/SUNWmd/%s.new", COMMON_CLUSTER_PATH,
	    service);
	if ((write_service = fopen(write_servicepath, "w")) == NULL)
		return (DCS_ERR_CCR_ACCESS);

	while (fgets(data, DCS_LINE_SIZE, read_service) != NULL) {
		if (strncmp(data, "prop=", 5) == 0) {
			p = strchr(data, '=') + 1;
			p = l_change_prop(props, p);
			fwrite(p, 1, strlen(p), write_service);
			free(p);
		} else {
			fwrite(data, 1, strlen(data), write_service);
		}
	}

	fclose(read_service);
	fclose(write_service);
	unlink(read_servicepath);
	rename(write_servicepath, read_servicepath);
	return (DCS_SUCCESS);
}	

dc_error_t
dcs_get_service_class_parameters(const char *class, char **prog,
    dc_ha_type_t *type)
{
	char	classpath[MAXPATHLEN];

	/*
	 * Arbitrary allocation. Not used anywhere except to free
	 * later on.
	 */
	*prog = (char *)malloc(20);

	sprintf(classpath, "%s/%s", COMMON_CLUSTER_PATH, class);
	return (access(classpath, F_OK) ? DCS_ERR_SERVICE_CLASS_NAME :
		DCS_SUCCESS);
}

dc_error_t
dcs_add_node(const char *service, dc_replica_t *node)
{
	char	servicepath[MAXPATHLEN],
		data[DCS_LINE_SIZE];
	FILE	*fp;

	sprintf(servicepath, "%s/SUNWmd/%s", COMMON_CLUSTER_PATH, service);
	if ((fp = fopen(servicepath, "a+")) == NULL)
		return (DCS_ERR_SERVICE_NAME);

	sprintf(data, "node=%d,%d,SECONDARY\n", node->id,
		node->preference);
	fwrite(data, 1, strlen(data), fp);
	fclose(fp);

	return (DCS_SUCCESS);
}

dc_error_t
dcs_remove_node(const char *service, nodeid_t nodeid)
{
	char	read_servicepath[MAXPATHLEN],
		write_servicepath[MAXPATHLEN],
		data[DCS_LINE_SIZE],
		*p;
	int	check_node;
	FILE	*read_service,
		*write_service;

	sprintf(read_servicepath, "%s/SUNWmd/%s", COMMON_CLUSTER_PATH,
		service);
	if ((read_service = fopen(read_servicepath, "r")) == NULL)
		return (DCS_ERR_SERVICE_NAME);

	sprintf(write_servicepath, "%s/SUNWmd/%s.new", COMMON_CLUSTER_PATH,
	    service);
	if ((write_service = fopen(write_servicepath, "w")) == NULL)
		return (DCS_ERR_CCR_ACCESS);

	while (fgets(data, DCS_LINE_SIZE, read_service) != NULL) {
		if (strncmp(data, "prop=node,", 10) == 0) {
			p = strchr(data, ',') + 1;
			check_node = strtol(p, 0, 0);
			if (check_node == nodeid)
				continue;
		}
		fwrite(data, 1, strlen(data), write_service);
	}

	fclose(read_service);
	fclose(write_service);
	unlink(read_servicepath);
	rename(write_servicepath, read_servicepath);
	return (DCS_SUCCESS);
}

dc_error_t
dcs_get_service_status(const char *service, int *is_suspended,
    dc_replica_status_seq_t **active, sc_state_code_t *state, char *state_str)
{
	FILE			*fp;
	char			servicepath[MAXPATHLEN],
				data[DCS_LINE_SIZE],
				*p;
	int			nodes,
				toss_value;
	dc_replica_status_seq_t	*reps;
	dc_replica_status_t	*rp;

	sprintf(servicepath, "%s/SUNWmd/%s", COMMON_CLUSTER_PATH, service);
	if ((fp = fopen(servicepath, "r")) == NULL) {
		*active = 0;
		return (DCS_ERR_SERVICE_NAME);
	}

	nodes = 0;
	while (fgets(data, DCS_LINE_SIZE, fp) != NULL) {
		if (strncmp(data, "node=", 5) == 0)
			nodes++;
	}
	rewind(fp);

	reps = (dc_replica_status_seq_t *)malloc(sizeof (*reps));
	reps->replicas = (dc_replica_status_t *)malloc(sizeof (*rp) * nodes);
	reps->count = nodes;
	rp = reps->replicas;

	while (fgets(data, DCS_LINE_SIZE, fp) != NULL) {
		if (strncmp(data, "node=", 5) == 0) {
			p = strchr(data, '=') + 1;
			rp->id = strtol(p, &p, 0);
			if (*p == ',')
				p++;
			else
				continue;
			toss_value = strtol(p, &p, 0);
			if (*p == ',')
				p++;
			else
				continue;
			rp->state = (*p == 'P') ? PRIMARY : SECONDARY;
			rp++;
		}
	}
	*active = reps;
	fclose(fp);
	return (DCS_SUCCESS);
}

dc_error_t
dcs_add_devices(const char *service, const dc_gdev_range_seq_t *gdev)
{
	return (DCS_SUCCESS);
}

dc_error_t
dcs_shutdown_service(const char *service, int suspend)
{
	return (DCS_SUCCESS);
}

dc_error_t
dcs_resume_service(const char *service)
{
	return (DCS_SUCCESS);
}

void
dcs_free_string(char *s)
{
	free(s);
}

void
dcs_free_string_seq(dc_string_seq_t *seq)
{
	int	x;

	for (x = 0; x < seq->count; x++)
		free(seq->strings[x]);
	free(seq->strings);
	free(seq);
}

void
dcs_free_dc_replica_status_seq(dc_replica_status_seq_t *rep)
{
	int	x;

	if (!rep)
		return;
	free(rep->replicas);
	free(rep);
}

/*
 * the following routines provide a false interface for cladm. the only
 * commands supported are those used by the libsds_sc.
 */

static FILE *
open_cluster_file()
{
	struct utsname	u;
	char		pathname[MAXPATHLEN];
	FILE		*fp;

	uname(&u);

	sprintf(pathname, "%s/%s", COMMON_CLUSTER_PATH,
		u.nodename);
	return (fopen(pathname, "r"));
}

static int
name_to_nodeid(int *np)
{
	char		data[DCS_LINE_SIZE],
			*p;
	FILE		*fp;
	struct utsname	u;

	uname(&u);

	if (fp = open_cluster_file()) {
		while (fgets(data, DCS_LINE_SIZE, fp) != NULL) {
			if (p = strchr(data, '='))
				*p++ = '\0';
			if (strncmp(u.nodename, data, strlen(data)) == 0) {
				*np = strtol(p, 0, 0);
				fclose(fp);
				return (0);
			}
		}
		fclose(fp);
	}
	return (-1);
}

static int
nodeid_to_name(clnode_name_t *cnp)
{
	char		data[DCS_LINE_SIZE],
			*p;
	FILE		*fp;
	int		nodeid;

	if (fp = open_cluster_file()) {
		while (fgets(data, DCS_LINE_SIZE, fp) != NULL) {
			if (p = strchr(data, '='))
				*p++ = '\0';
			nodeid = strtol(p, 0, 0);
			if (nodeid == cnp->nodeid) {
				strcpy(cnp->name, data);
				break;
			}
		}
		fclose(fp);
		return (0);
	} else
		return (-1);
}

int
_cladm(int c, int s, void *v)
{
	int	*np = (int *)v;
	switch (c) {
	case CL_CONFIG:
		switch (s) {
		case CL_NODEID:
			return (name_to_nodeid(np));

		case CL_HIGHEST_NODEID:
			*np = 16;
			break;

		case CL_GET_NODE_NAME:
			return (nodeid_to_name((clnode_name_t *)v));
		}
	}
	return (0);
}
