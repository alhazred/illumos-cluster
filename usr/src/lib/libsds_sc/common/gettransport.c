/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident "@(#)gettransport.c   1.10     08/05/20 SMI"

#define	CLUSTER_LIBRARY_SOURCE
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <sdssc.h>

#define	HOSTLENSTART	1000
#define	HOSTLENINC	200
#define	HOSTLENLIMIT	5000

/*
 * Get the hostname corresponding to the private cluster transport for a
 * given node number. This returns a NULL pointer if no such mapping
 * exists. Caller must free() the provided string when finished with it.
 * This returns NULL if it was not possible to get the transport.
 *
 * This assumes that all host requests within a cluster for a member of
 * the cluster return the name of the high-speed transport.
 */
rval_e
_sdssc_gettransportbynode(int nodenumber, char **transport_name)
{
	char		*name_returned = NULL,
			*buffer = NULL;
	struct hostent	host_struct;
	struct hostent	*hostp;
	int		h_errnop,
			hostlen = HOSTLENSTART;
	scconf_nodeid_t	nodeid;
	rval_e		rval = SDSSC_ERROR;

	if (scconf_get_nodename((scconf_nodeid_t)nodenumber,
	    &name_returned) != SCCONF_NOERR)
		goto exit_routine;

	for (hostlen = HOSTLENSTART;
	    hostlen < HOSTLENLIMIT;
	    hostlen += HOSTLENINC) {
		if ((buffer = malloc(hostlen)) == NULL) {
			goto exit_routine;
		}

		hostp = gethostbyname_r(name_returned, &host_struct,
		    buffer, hostlen, &h_errnop);

		if (hostp == NULL) {
			if (errno == ERANGE) {
				/*
				 * The only problem was that the
				 * supplied buffer wasn't big enough,
				 * so extend it a bit and try again.
				 */
				free(buffer);
				continue;
			} else {
				goto exit_routine;
			}
		} else {
			break;
		}
	}

	*transport_name = strdup(hostp->h_name);

exit_routine:
	if (name_returned)
		free((void *)name_returned);
	if (buffer)
		free(buffer);

	return (rval);
}
