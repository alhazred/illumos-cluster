/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)sdssc_host.c 1.30     08/05/20 SMI"

/*
 * Provides interfaces for manipulating host for a DCS "service"
 *
 *	- add host(s) to a "service"
 *	- delete host(s) from a "service"
 *	- get "primary" host for a "service"
 */

#define	CLUSTER_LIBRARY_SOURCE
#include <meta.h>
#include <sys/param.h>		/* For MAXPATHLEN */
#include <sys/stat.h>
#include <sdssc.h>


rval_e
_sdssc_add_hosts(char *service, int nhosts, char **hosts)
{
	rval_e		rval = SDSSC_ERROR;
	int		n;
	char		**new_host_list = NULL,
			**nhlp;
	scconf_cfg_ds_t	*config = NULL;
	scconf_nodeid_t	*nodelist;

	/*
	 * Hopefully "nhosts" contains something that we can proceed
	 */

	if (nhosts < 1)
		return (SDSSC_ERROR);

	if (scconf_get_ds_config(service, SCCONF_BY_NAME_FLAG, &config) !=
	    SCCONF_NOERR) {
		goto exit_routine;
	}

	/*
	 * Count the number of nodes in the current list
	 */
	for (n = 0, nodelist = config->scconf_ds_nodelist;
	    nodelist && nodelist[n] != 0; n++)
		;

	if ((new_host_list = (char **)calloc(n + nhosts + 1,
	    sizeof (char *))) == NULL) {
		goto exit_routine;
	}
	nhlp = new_host_list;

	for (n = 0, nodelist = config->scconf_ds_nodelist;
	    nodelist && nodelist[n] != 0; n++, nhlp++) {
		if (scconf_get_nodename(nodelist[n], nhlp) != SCCONF_NOERR) {
			goto exit_routine;
		}
	}

	for (; nhosts; nhosts--)
		*nhlp++ = *hosts++;

	if (scconf_change_ds(service, new_host_list, SCCONF_STATE_UNCHANGED,
	    NULL, SCCONF_STATE_UNCHANGED, NULL, 0) == SCCONF_NOERR)
		rval = SDSSC_OKAY;

exit_routine:
	if (new_host_list) {

		/*
		 * Only free the memory that was allocated when
		 * scconf_get_nodename was called. The other pointers
		 * have been allocated externally to this routine and
		 * should be touched.
		 */
		for (n = 0, nodelist = config->scconf_ds_nodelist,
		    nhlp = new_host_list;
		    nodelist && nodelist[n] != 0;
		    n++, nhlp++) {

			/*
			 * It possible that during the collection of
			 * nodenames that scconf_get_nodename has failed.
			 * This would mean that only part of the new_host_list
			 * arrary would have allocated memory, so only free
			 * things that have a non-zero pointer.
			 */
			if (*nhlp)
				free(*nhlp);
		}
		free(new_host_list);
	}
	if (config)
		scconf_free_ds_config(config);
	return (rval);
}

/*
 * sdssc_delete_hosts -- delete host(s) from service
 */
rval_e
_sdssc_delete_hosts(char *service, int nhosts, char **hosts)
{
	char		**new_host_list = NULL,
			**nhlp;
	int		x, n;
	rval_e		rval = SDSSC_ERROR;
	scconf_cfg_ds_t	*config = NULL;

	/*
	 * There's no guarantee that 'hosts' is a null terminated arrary
	 * of pointers so we need to build one.
	 */
	if ((new_host_list = calloc(nhosts + 1, sizeof (char **))) == NULL)
		goto exit_routine;

	nhlp = new_host_list;
	for (x = 0; x < nhosts; x++)
		*nhlp++ = strdup(hosts[x]);

	if (scconf_get_ds_config(service, SCCONF_BY_NAME_FLAG, &config) !=
		SCCONF_NOERR)
		goto exit_routine;

	if (config->scconf_ds_nodelist != NULL) {
		for (n = 0; config->scconf_ds_nodelist[n] != 0; n++)
				;
	} else {
		goto exit_routine;
	}

	/*
	 * If we are deleting the set, notify DCS
	 *
	 * It is possible that n == 0 because the DCS state could
	 * be offline and have no primary, if this is the case
	 * DCS still needs to be notified that the set is being
	 * deleted.
	 */
	if (nhosts == n || n == 0) {
		(void) meta_unlock(metasetname(service, NULL), NULL);
		if (_sdssc_notify_service(service, Release_Primary) ==
			SDSSC_ERROR)
			printf(gettext(
				"metaset: failed to notify DCS of release\n"));
	}

	if (scconf_rm_ds(service, new_host_list, NULL, NULL, NULL)
	    == SCCONF_NOERR)
		rval = SDSSC_OKAY;

exit_routine:

	if (new_host_list) {
		for (x = 0; new_host_list[x]; x++)
			free(new_host_list[x]);
		free(new_host_list);
	}

	if (config)
		scconf_free_ds_config(config);

	return (rval);
}

rval_e
_sdssc_get_primary_host(char *service, char *host, int nbytes)
{
	char			*nodename = NULL,
				*current_nodename = NULL;
	rval_e			rval = SDSSC_ERROR;
	scstat_ds_t		*status = NULL;
	scstat_ds_node_state_t	*nslp;
	scconf_nodeid_t		nodeid,
				*np;
	scconf_errno_t		sc_rval;
	scconf_cfg_ds_t		*config = NULL;

	if (scstat_get_ds_status((scstat_ds_name_t *)service,
	    &status) != SCSTAT_ENOERR) {
		rval = SDSSC_NO_SERVICE;
		goto exit_routine;
	}

	/*
	 * First search to see if one of the nodes for this
	 * service is the primary.
	 */
	for (nslp = status->scstat_node_state_list; nslp;
	    nslp = nslp->scstat_node_next) {
		if (nslp->scstat_node_state == SCSTAT_PRIMARY) {
			nodename = nslp->scstat_node_name;
			break;
		}
	}

	/*
	 * If no primary is found. See if this node is listed in the
	 * service. If it is then use this node as the primary
	 */
	if (nodename == NULL) {
		if ((sc_rval = scconf_get_nodeid(NULL, &nodeid))
		    != SCCONF_NOERR) {
			goto exit_routine;
		}

		if (scconf_get_ds_config(service, SCCONF_BY_NAME_FLAG,
		    &config) != SCCONF_NOERR) {
			rval = SDSSC_NO_SERVICE;
			goto exit_routine;
		}

		for (np = config->scconf_ds_nodelist; np && *np; np++) {
			if (*np == nodeid) {
				if (scconf_get_nodename(nodeid,
				    &current_nodename) != SCCONF_NOERR)
					goto exit_routine;
				nodename = current_nodename;
				break;
			}
		}
	}

	/*
	 * If the current node is not in the list then use the first
	 * node associated with this service.
	 */
	if ((nodename == NULL) && config &&
	    (config->scconf_ds_nodelist != NULL)) {

		/*
		 * At this point current_nodename had better be null because
		 * if it's set then nodename should also be set.
		 */
		assert(current_nodename == NULL);

		if (scconf_get_nodename(config->scconf_ds_nodelist[0],
		    &current_nodename) != SCCONF_NOERR)
			goto exit_routine;
		nodename = current_nodename;
	}

	if ((nodename != NULL) && (strlen(nodename) < nbytes)) {
		strcpy(host, nodename);
		rval = SDSSC_OKAY;
	}

	/*
	 * If at this point we don't have a primary host that can only mean
	 * that nobody is associated with this service. By returning
	 * SDSSC_NO_SERVICE the caller then needs to decide what to do next.
	 */
	if (nodename == NULL)
		rval = SDSSC_NO_SERVICE;

exit_routine:

	if (status)
		scstat_free_ds_status(status);
	if (current_nodename)
		free(current_nodename);
	if (config)
		scconf_free_ds_config(config);

	return (rval);
}

void
_sdssc_cm_nm2nid(char *name)
{
	scconf_nodeid_t	nodeid;
	scconf_errno_t	rval;

	/*
	 * Try public name first of all
	 */
	rval = scconf_get_nodeid(name, &nodeid);
	if (rval == SCCONF_ENOEXIST) {
		/*
		 * Try on the private name
		 */
		rval = scconf_get_phost_nodeid(name, &nodeid);
	}

	if (rval == SCCONF_NOERR)
		sprintf(name, "%d", nodeid);
}


void
_sdssc_cm_sr_nm2nid(md_set_record *sr)
{
	scconf_nodeid_t	nodeid;
	int		i, j;

	/*
	 * Sanity check
	 */
	assert(sr != NULL);

	/*
	 * Search the list for nodename -> nodeid mapping
	 */
	for (i = 0; i < MD_MAXSIDES; i++) {
		if (sr->sr_nodes[i][0] == '\0')
			continue;
		_sdssc_cm_nm2nid(sr->sr_nodes[i]);
	}

	for (i = 0; i < sr->sr_med.n_cnt; i++) {
		for (j = 0; j < sr->sr_med.n_lst[i].a_cnt; j++) {
			_sdssc_cm_nm2nid(sr->sr_med.n_lst[i].a_nm[j]);
		}
	}
}


void
_sdssc_cm_nid2nm(char *nid)
{
	char	*name;
	int	id;

	/*
	 * Sanity check
	 */
	assert(nid != NULL);

	/*
	 * Map nodeid -> nodename
	 * Convert id string to int id
	 */
	id = atoi(nid);

	if (scconf_get_nodename((scconf_nodeid_t)id, &name) ==
	    SCCONF_NOERR) {

		/*
		 * Return valid node name
		 */
		sprintf(nid, "%s", name);
		free(name);
	}
}

void
_sdssc_cm_sr_nid2nm(md_set_record *sr)
{
	char	*name;
	int	id;
	int	i, j;

	/*
	 * Sanity check
	 */
	assert(sr != NULL);

	/*
	 * Map nodeid -> nodename
	 */
	for (i = 0; i < MD_MAXSIDES; i++) {
		if (sr->sr_nodes[i][0] == '\0')
			continue;
		/*
		 * Convert id string to int id
		 */
		_sdssc_cm_nid2nm(sr->sr_nodes[i]);
	}

	for (i = 0; i < sr->sr_med.n_cnt; i++) {
		for (j = 0; j < sr->sr_med.n_lst[i].a_cnt; j++) {
			/*
			 * Convert id string to int id
			 */

			_sdssc_cm_nid2nm(sr->sr_med.n_lst[i].a_nm[j]);
		}
	}
}

/*
 * sdssc_get_priv_ipaddr -- given a node name return the private IP addr
 */
rval_e
_sdssc_get_priv_ipaddr(char *nodename, struct in_addr *ip)
{
	scconf_nodeid_t	nodeid;
	scconf_errno_t	e;

	if ((scconf_get_nodeid(nodename, &nodeid) != SCCONF_NOERR) ||
	    (scconf_get_node_privateipaddr(nodeid, ip) != SCCONF_NOERR))
		return (SDSSC_ERROR);
	else
		return (SDSSC_OKAY);
}
