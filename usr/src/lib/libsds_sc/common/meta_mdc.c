/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

#pragma ident	"@(#)meta_mdc.c	1.14	08/05/20 SMI"

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * These are all the client-side rpc calls for device binding and
 * potentially other cluster-based rpc's.
 */

#define	CLUSTER_LIBRARY_SOURCE
#include <rpc/rpc.h>
#include <dlfcn.h>
#include <meta.h>
#include <metacl.h>
#include <sdssc.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/*
 * Fake up an exit code for RPC failures in case we
 * need to rely on representative exit codes.
 */
static const int dummy_exit_code = 1;

/*
 * RPC handle
 */
typedef struct {
	char	*hostname;
	CLIENT	*clntp;
} mdc_handle_t;

/*
 * Construct an error structure for an client timeout modification failure.
 * As with rpc failure, we don't get back a response.
 */
void
metacldstoerror(char *hostname, mdc_err_t *mdcreturn_p)
{
	/* Set "dummy exit" code for metacldprinterror(). */
	mdcreturn_p->mdc_exitcode = dummy_exit_code;
	mdcreturn_p->mdc_errno = MDC_RPCFAILED;
	mdcreturn_p->mdc_node = strdup(hostname);
	mdcreturn_p->mdc_misc = strdup(gettext("metacld client set timeout"));
}

/*
 * free and clear error
 */
static void
mdc_clrerror(mdc_err_t *mdcep)
{
	if (mdcep->mdc_node != NULL) {
		free(mdcep->mdc_node);
		mdcep->mdc_node = NULL;
	}

	if (mdcep->mdc_misc != NULL) {
		free(mdcep->mdc_misc);
		mdcep->mdc_misc = NULL;
	}

	(void) memset(mdcep, 0, sizeof (*mdcep));
}

/*
 * close RPC connection
 */
static void
close_metacld(mdc_handle_t *hp)
{
	assert(hp != NULL);
	if (hp->hostname != NULL) {
		free(hp->hostname);
	}
	if (hp->clntp != NULL) {
		auth_destroy(hp->clntp->cl_auth);
		clnt_destroy(hp->clntp);
	}
	free(hp);
}

/*
 * Set the timeout value for this client handle.
 */
static int
cl_sto_mdc(CLIENT *clntp, char *hostname, const struct timeval *time_out,
    mdc_err_t *ep)
{
	if (clnt_control(clntp, CLSET_TIMEOUT, (char *)time_out) != TRUE) {
		metacldstoerror(hostname, ep);
		return (1);
	}

	return (0);
}

/*
 * open RPC connection to rpc.metacld
 */
static mdc_handle_t *
open_metacld(char *hostname)
{
	CLIENT		*clntp;
	mdc_handle_t	*hp;

	struct in_addr p_ip;
	struct hostent *p_host = NULL;

	if ((hostname == NULL) || (*hostname == '\0')) {
		hostname = mynode();
	} else {
		/*
		 *  Convert the hostname into the private
		 *  interconnect.  No need to check for clustermode.
		 *  We are always in a cluster if we are here.
		 *  We fail back on trying the public connection, should
		 *  there be a failuring looking up the private interconnect.
		 */

		if (_sdssc_get_priv_ipaddr(hostname, &p_ip) == SDSSC_OKAY) {
			p_host = gethostbyname(inet_ntoa(p_ip));
			if (p_host != NULL) {
				hostname = strdup(p_host->h_name);
				freehostent(p_host);
			}
		}
	}
	/* open RPC connection */
	if ((clntp = clnt_create(hostname, MDC_PROG, MDC_VERS,
	    "tcp")) == NULL) {
		if (rpc_createerr.cf_stat != RPC_PROGNOTREGISTERED)
		    clnt_pcreateerror(hostname);
		return (NULL);
	} else {
		auth_destroy(clntp->cl_auth);
		clntp->cl_auth = authsys_create_default();
		assert(clntp->cl_auth != NULL);
	}

	/* return connection */
	hp = memset(malloc(sizeof (*hp)), 0, sizeof (*hp));
	hp->hostname = strdup(hostname);
	hp->clntp = clntp;

	return (hp);
}

/*
 * Print error and free mdc_err_t
 */
void
metacldprinterror(mdc_err_t *mdcep)
{
	/* no error */
	if (mdcep->mdc_errno == 0) {
		goto out;
	}

	/* steal error */
	if ((mdcep->mdc_node != NULL) && (mdcep->mdc_node[0] != '\0')) {
		(void) printf(gettext("Info: %s: %s\n"),
		    mdcep->mdc_node, mdcep->mdc_misc);
	}

	/* cleanup, return success */
out:
	if (mdcep->mdc_node != NULL)
		free(mdcep->mdc_node);
	if (mdcep->mdc_misc != NULL)
		free(mdcep->mdc_misc);

	(void) memset(mdcep, 0, sizeof (*mdcep));
}

/*
 * Construct an error structure for an rpc failure. This is special
 * because upon rpc failure, we don't get back a response.
 */
void
metacldrpcerror(char *hostname, mdc_err_t *mdcreturn_p)
{
	/*
	 * set a "dummy" non-zero exit code for RPC failures
	 * otherwise exit code always zero.
	 */
	mdcreturn_p->mdc_exitcode = dummy_exit_code;
	mdcreturn_p->mdc_errno = MDC_RPCFAILED;
	mdcreturn_p->mdc_node = strdup(hostname);
	mdcreturn_p->mdc_misc = strdup(gettext("rpc failure"));
}

/*
 * Put status data from mdc_err_t into a permanent location provided by
 * the calling module. Allocate space for all strings and then free
 * the original structure and strings.
 */
void
metacldstoreerror(mdc_err_t *mdcep, mdc_err_t *mdcreturn_p)
{
	mdcreturn_p->mdc_errno = mdcep->mdc_errno;
	mdcreturn_p->mdc_exitcode = mdcep->mdc_exitcode;

	/* & now the strings */
	if (mdcep->mdc_node != NULL)
		mdcreturn_p->mdc_node = strdup(mdcep->mdc_node);
	else
		mdcreturn_p->mdc_node = strdup("NO ENTRY");

	if (mdcep->mdc_misc != NULL)
		mdcreturn_p->mdc_misc = strdup(mdcep->mdc_misc);
	else
		mdcreturn_p->mdc_misc = strdup("NO ENTRY");

	mdc_clrerror(mdcep);
}

/*
 * NULLPROC - just returns a response
 */
rval_e
_sdssc_clnt_mdc_null(char *hostname, mdc_err_t *mdcep)
{
	mdc_handle_t		*hp;
	mdc_bind_res_t		res;
	rval_e			rval = SDSSC_OKAY;

	/* initialize */
	(void) mdc_clrerror(mdcep);
	(void) memset(&res, 0, sizeof (res));

	/* do it */
	if ((hp = open_metacld(hostname)) == NULL) {
		rval = SDSSC_ERROR;
	} else {
		if (mdc_null_1(NULL, &res, hp->clntp) != RPC_SUCCESS) {
			(void) metacldrpcerror(hostname, mdcep);
			rval = SDSSC_ERROR;
		} else {
			(void) metacldstoreerror(&res.mdc_status, mdcep);
		}
		(void) close_metacld(hp);
		(void) xdr_free(xdr_mdc_err_t, (char *)&res);
	}
	return (rval);
}

/*
 * Connect to the server and do the deed. This recieves a pointer to
 * an allocated section of memory for storing the returned mdc_err_t
 * (error_status). This function is expected to allocate space for
 * any strings that need to be inserted into that structure.
 */
rval_e
_sdssc_clnt_bind_devs(char *transportname, mdc_err_t *ep)
{
	mdc_handle_t		*hp;
	mdc_bind_res_t		res;
	rval_e			rval = SDSSC_OKAY;
	void			*argp;

	/* initialize */
	argp = NULL;

	(void) mdc_clrerror(ep);
	(void) memset(&res, 0, sizeof (res));

	/* do it */
	if ((hp = open_metacld(transportname)) == NULL) {
		rval = SDSSC_ERROR;
	} else {
		if (mdc_bind_devs_1(argp, &res, hp->clntp) != RPC_SUCCESS) {
			(void) metacldrpcerror(transportname, ep);
			rval = SDSSC_ERROR;
		} else {
			(void) metacldstoreerror(&res.mdc_status, ep);
		}
		(void) close_metacld(hp);
		(void) xdr_free(xdr_mdc_bind_res_t, (char *)&res);
	}
	return (rval);
}

/*
 * Connect to the server and do the deed. This recieves a pointer to
 * an allocated section of memory for storing the returned mdc_err_t
 * (ep).
 */
rval_e
_sdssc_clnt_proxy_cmd(uint_t arg_size, char **arg_list, uint_t env_size,
    char **env_list, char *transportname, mdc_err_t *ep)
{
	int rpcresult;
	mdc_handle_t		*hp;
	mdc_bind_res_t		res;
	rval_e			rval = SDSSC_OKAY;
	mdcrpc_proxy_args_t	argp;

	/* initialize */
	(void) mdc_clrerror(ep);
	(void) memset(&argp, 0, sizeof (argp));
	(void) memset(&res, 0, sizeof (res));

	/* build args */
	argp.argvlist.argvlist_len = arg_size;
	argp.argvlist.argvlist_val = arg_list;
	argp.environment.environment_len = env_size;
	argp.environment.environment_val = env_list;

	/* do it */
	if ((hp = open_metacld(transportname)) == NULL) {
		(void) metacldrpcerror(transportname, ep);
		rval = SDSSC_ERROR;
	} else {
		/* Use extended timeout. */
		if (cl_sto_mdc(hp->clntp, transportname,
		    &md_mdc_proxy_timeout, ep)) {
			    (void) metacldprinterror(ep);
			    (void) mdc_clrerror(ep);
		}

		rpcresult = mdc_proxy_1(&argp, &res, hp->clntp);
		if (rpcresult != RPC_SUCCESS) {
			(void) metacldrpcerror(transportname, ep);
			rval = SDSSC_ERROR;
		} else {
			(void) metacldstoreerror(&res.mdc_status, ep);
		}
		(void) close_metacld(hp);
		(void) xdr_free(xdr_mdc_bind_res_t, (char *)&res);
	}
	return (rval);
}
