/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)getnodelist.c	1.13	08/05/20 SMI"

#define	CLUSTER_LIBRARY_SOURCE
#include <meta.h>
#include <sdssc.h>

/*
 * sdssc_getnodelist -- provide a complete list of node numbers for this
 * cluster. This function allocates a contiguous swatch of memory and
 * places the array of ints there. The caller is expected to free()
 * the array after use. The array is terminated with a -1.
 *
 * XXX - Can it be argued that this list will always be contiguous
 * meaning that this function could just return the highest node number
 * (a single int)? I have to believe that at some point node n could be
 * semi-permanently withdrawn from the cluster so I'm currently
 * convinced that an actual list of node numbers is necessary in order
 * to capture such a situation - JST.
 */
rval_e
_sdssc_getnodelist(int **node_list_out)
{
	int		*nnp,
			*nnp_out = NULL,
			list_size;
	scstat_node_t	*node_list_p = NULL,
			*nlp;
	scconf_nodeid_t	nodeid;
	rval_e		rval = SDSSC_ERROR;

	*node_list_out = (int *)0;

	if (scstat_get_nodes(&node_list_p) != SCSTAT_ENOERR)
		goto exit_routine;

	for (nlp = node_list_p, list_size = 0;
	    nlp; nlp = nlp->scstat_node_next) {
		if (nlp->scstat_node_status == SCSTAT_ONLINE) {
			list_size++;
		}
	}

	/* Allocate room for the array of nodes and the terminator. */
	nnp = nnp_out = (int *)calloc(list_size + 1, sizeof (int));

	for (nlp = node_list_p; nlp; nlp = nlp->scstat_node_next) {
		if (nlp->scstat_node_status != SCSTAT_ONLINE)
			continue;
		if (scconf_get_nodeid(nlp->scstat_node_name, &nodeid) !=
		    SCCONF_NOERR) {
			goto exit_routine;
		}

		*nnp = nodeid;
		nnp++;
	}

	*nnp = -1;	/* mark the end */

	*node_list_out = nnp_out;	/* set the user's pointer. */
	rval = SDSSC_OKAY;

exit_routine:
	if (nnp_out && (rval == SDSSC_ERROR)) {

		/*
		 * Only free the memeory associated with nnp_out if we've
		 * had an error. Otherwise that memory will be freed and
		 * the caller of this routine will try to use it.
		 */
		free(nnp_out);
	}
	if (node_list_p)
		scstat_free_nodes(node_list_p);
	return (rval);
}

void
_sdssc_freenodelist(int *node_list)
{
	if (node_list != (int *)0)
		free(node_list);
}
