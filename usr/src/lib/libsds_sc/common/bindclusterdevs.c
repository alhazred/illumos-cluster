/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

#pragma ident "@(#)bindclusterdevs.c   1.17     08/05/20 SMI"

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * Set device bindings correctly throughout a cluster
 */

#define	CLUSTER_LIBRARY_SOURCE
#include <stdlib.h>
#include <meta.h>
#include <metacl.h>
#include <sys/types.h>
#include <sys/mhd.h>
#include <scadmin/scconf.h>
#include <sdssc.h>
#include <thread.h>

struct thread_args {
	thread_t	id;
	cond_t		cv;
	mutex_t		mtx;
	int		ta_target_node,
			ta_this_node;
	mdc_err_t	*ta_error_p;
};

typedef struct thread_args thread_args_t;

/*
 * This function is a royal hack and will be removed once I've
 * modified the server side RPC's to multi-thread. Right now, I
 * can't call an RPC from an RPC so I have to expose this
 * server-side data to this client for now.
 */
static char *
errnum_to_str(int errnum)
{
	switch (errnum) {
	    case MDC_NOERROR:
		return (gettext("No Error"));
	    case BIND_BADDEVICE:
		return (gettext("a local device is not configurable"));
	    case BIND_NOACCESS_DEVICE:
		return (gettext("a local device cannot be accessed"));
	    case MDC_NOACCESS_CCR:
		return (gettext("cannot access the cluster configuration"));
	    case MDC_RPCFAILED:
		return (gettext("rpc failure"));
	    case BIND_NODISKSETCLASS:
		return (gettext("this cluster does not have any disk sets"));
	    case BIND_LOCALSET:
		return (gettext("cannot convert local diskset to global"));
	    case BIND_NOACCESS_SHARED:
		return (gettext("metadevice shared area is not accessible"));
	    case BIND_LINKISDIR:
		return (gettext("cannot convert local diskset to global"));
	    case MDC_NOACCESS:
		return (gettext("access failure"));
	    case MDC_NOTINCLUSTER:
		return (gettext("this host is not in a cluster"));
	    case MDC_PROXYFAILED:
		return (gettext("remote command exit code was non-zero"));
	    case MDC_PROXYNOFORK:
		return (gettext("remote daemon could not fork"));
	    case MDC_PROXYKILLED:
		return (gettext("remote daemon killed by signal"));
	    default:
		return (NULL);
	}
}

/*
 * This, unfortunately, is a continuation of the above hack. This
 * function should simply call the RPC but since the current daemon
 * is not multi-threaded it would not be able to service this
 * request if it is already executing a proxy. So, until the daemon
 * is fixed, this will call _sdssc_binddevs() locally if possible.
 */
static void *
_bind_by_thread(void *args)
{
	char			*transportname = NULL;
	int			target_node,
				this_node;
	mdc_err_t		*error_p;

	/* initialize */
	target_node = ((struct thread_args *)args)->ta_target_node;
	this_node = ((struct thread_args *)args)->ta_this_node;
	error_p = ((struct thread_args *)args)->ta_error_p;

	/* Get the transport to use for rpc's. */
	_sdssc_gettransportbynode(target_node, &transportname);

	if (transportname != NULL) {
		if (this_node == target_node) {
			error_p->mdc_node = strdup(transportname);
			error_p->mdc_errno = _sdssc_binddevs();
			error_p->mdc_misc = strdup(errnum_to_str(
			    error_p->mdc_errno));
		} else {
			_sdssc_clnt_bind_devs(transportname, error_p);
		}

		free(transportname);
	}

	return (NULL);
}

/*
 * Find all the nodes in this cluster and sync each of them up with the
 * global device space. This returns an allocated array of mdc_err_t
 * structures (one for each host) in error_status. If everything is OK,
 * the call returns SDSSC_OKAY and the caller can be sure that no
 * mdc_err_t structure indicates an error. If there is an error on any
 * host, the function returns SDSSC_ERROR and the caller needs to scan
 * the array of structures for the problem. If the function ran out of
 * memory, it returns SDSSC_ERROR and an empty array of structures (the
 * first entry in the array is NULL). If there are no nodes in the
 * cluster, the function returns SDSSC_OKAY and an empty array of
 * structures.
 */
rval_e
_sdssc_bindclusterdevs(mdc_err_t ***error_status)
{
	boolean_t	error_occurred = B_FALSE;
	int		i, thr_entry,
			*target_node_list,
			*cur_node_list, threrror = 0, *threrrorp,
			this_node;
	mdc_err_t	*error_array_entry;
	mdc_err_t	**error_list, **error_list_counter;
	scconf_nodeid_t	nodeid;

	/* If everything is broken, we return a NULL. */
	*error_status = NULL;

	/* Get this node number. */
	if (scconf_get_nodeid(NULL, &nodeid) != SCCONF_NOERR)
		return (SDSSC_ERROR);
	this_node = (int)nodeid;

	/*
	 * Get an array of all member node numbers and do the binding.
	 */
	if (_sdssc_getnodelist(&cur_node_list) == 0) {
		int		node_count;
		thread_args_t	*arg_array;

		/* count the nodes */
		for (node_count = 0,
		    target_node_list = cur_node_list;
		    *target_node_list != -1;
		    target_node_list++,
		    node_count++);

		/*
		 * Allocate space for the mdc_err_t pointers and for
		 * the structure itself. We allocate space for node_count
		 * structures and node_count+1 pointers since the last
		 * pointer is the null end-of-list marker. The client
		 * function will allocate space for the strings within
		 * these structures.
		 */
		arg_array = (thread_args_t *)calloc(node_count,
		    sizeof (thread_args_t));

		if (((error_list = calloc((node_count + 1),
		    (sizeof (mdc_err_t *)))) == NULL) ||
		    ((error_array_entry = calloc((node_count),
		    (sizeof (mdc_err_t)))) == NULL)) {
			/* If that doesn't work, we're hosed. */
			error_occurred = B_TRUE;
		} else {
			error_list_counter = error_list;

			for (thr_entry = 0,
			    target_node_list = cur_node_list;
			    *target_node_list != -1;
			    thr_entry++,
			    target_node_list++,
			    error_list_counter++,
			    error_array_entry++) {
				*error_list_counter = error_array_entry;

				(void) cond_init(&arg_array[thr_entry].cv,
				    USYNC_THREAD, 0);
				(void) mutex_init(&arg_array[thr_entry].mtx,
				    USYNC_THREAD, 0);

				arg_array[thr_entry].ta_target_node =
				    *target_node_list;
				arg_array[thr_entry].ta_this_node =
				    this_node;
				arg_array[thr_entry].ta_error_p =
				    error_array_entry;

				if (thr_create(NULL, NULL,
				    _bind_by_thread,
				    (void *)&(arg_array[thr_entry]),
				    THR_NEW_LWP,
				    &arg_array[thr_entry].id) != 0) {
					error_occurred = B_TRUE;
				}
			}

			/*
			 * With all threads started, we now wait for
			 * them to exit.
			 */
			threrrorp = &threrror;
			for (i = 0; i < node_count; i++) {
				if (thr_join(arg_array[i].id, NULL,
				    (void **)&threrrorp) == 0) {
					if (threrror != 0) {
						error_occurred = B_TRUE;
					}
				}
				(void) cond_destroy(&arg_array[i].cv);
				(void) mutex_destroy(&arg_array[i].mtx);

				if (arg_array[i].ta_error_p->mdc_errno !=
				    MDC_NOERROR) {
					error_occurred = B_TRUE;
				}
			}
		}
		/* Transfer the pointer to the caller. */
		*error_status = error_list;

		free(arg_array);

		_sdssc_freenodelist(cur_node_list);
	} else {
		error_occurred = B_TRUE;
	}

	if (error_occurred == B_TRUE)
		return (SDSSC_ERROR);
	else
		return (SDSSC_OKAY);
}

/*
 * Free the list of mdc_err_t structures allocated above. Note that the
 * array of pointers to structures points to a contiguous swatch of
 * memory that was allocated all at once.
 */
void
_sdssc_free_mdcerr_list(mdc_err_t **error_list)
{
	mdc_err_t	**this_error_list = error_list;
	mdc_err_t	*this_error;	/* readability */

	if (error_list == NULL)
		return;

	while (*this_error_list != NULL) {
		this_error = *this_error_list;

		if (this_error->mdc_node != NULL) {
			free(this_error->mdc_node);
			this_error->mdc_node = NULL;
		}

		if (this_error->mdc_misc != NULL) {
			free(this_error->mdc_misc);
			this_error->mdc_misc = NULL;
		}

		this_error_list++;
	}

	free(*error_list);
	free(error_list);
}
