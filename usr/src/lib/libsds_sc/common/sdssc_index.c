/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sdssc_index.c	1.10	08/05/20 SMI"

/*
 * This interface returns an index "setno" for service "set" which is unique
 * across the whole cluster configuration.
 */

#define	CLUSTER_LIBRARY_SOURCE
#include <meta.h>
#include <sys/param.h>		/* For MAXPATHLEN */
#include <sys/stat.h>
#include <sdssc.h>

rval_e
_sdssc_get_index(char *service, set_t *index_val)
{
	scconf_cfg_ds_t	*config = NULL;
	char		*prop_val = NULL;
	rval_e		rval = SDSSC_ERROR;

	/*
	 * The service "set" should exist
	 */

	if ((scconf_get_ds_config(service, SCCONF_BY_NAME_FLAG, &config) ==
	    SCCONF_NOERR) &&
	    (l_get_property(config, SDSSC_PROP_INDEX, &prop_val) ==
	    SDSSC_OKAY)) {

		/*
		 * We have value for property "setno" for this service
		 */

		*index_val = strtol(prop_val, 0, 0);
		rval = SDSSC_OKAY;
	}

	/*
	 * Free any memory allocated
	 */
	if (config)
		scconf_free_ds_config(config);

	return (rval);
}
