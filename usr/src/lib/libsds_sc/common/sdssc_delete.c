/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sdssc_delete.c	1.16	08/05/20 SMI"

/*
 * Interface implements a "deletion" of "set" service
 */

#define	CLUSTER_LIBRARY_SOURCE
#include <meta.h>
#include <syslog.h>
#include <sys/param.h>		/* For MAXPATHLEN */
#include <sys/stat.h>
#include <sdssc.h>

rval_e
l_build_hostlist(scconf_nodeid_t *nlp, char ***return_list)
{
	int	n;
	char	**new_host_list = NULL,
		**nhlp,
		*nodename;

	for (n = 0; nlp[n]; n++)
		;

	if ((new_host_list = (char **)calloc(n + 1, sizeof (char **))) ==
	    NULL)
		goto exit_routine;

	nhlp = new_host_list;
	for (n = 0; nlp[n]; n++) {
		if (scconf_get_nodename(nlp[n], &nodename) != SCCONF_NOERR)
			goto exit_routine;

		if (nodename) {
			*nhlp++ = nodename;
		}
	}

	*return_list = new_host_list;
	return (SDSSC_OKAY);

exit_routine:
	if (new_host_list) {
		for (n = 0; new_host_list[n]; n++)
			free(new_host_list[n]);
		free(new_host_list);
	}
	return (SDSSC_ERROR);
}

rval_e
_sdssc_delete_begin(char *service)
{
	char			*prop_val = NULL;
	char			*prop_incar = NULL;
	scconf_cfg_ds_t		*config = NULL;
	scconf_nodeid_t		nodeid;
	scconf_cfg_prop_t	props[2];
	scswitch_service_t	servicelist;
	rval_e			rval = SDSSC_ERROR;

	/*
	 * Go ahead a prep this structure now instead of doing it twice
	 * later on.
	 */
	servicelist.scswitch_service_name = service;
	servicelist.next = NULL;

	if (scconf_get_ds_config(service, SCCONF_BY_NAME_FLAG, &config) !=
	    SCCONF_NOERR)
		goto exit_routine;

	if (l_get_property(config, SDSSC_PROP_STATE, &prop_val)
	    != SDSSC_OKAY)
		goto exit_routine;

	/*
	 * We have the value for the property "state" for this service
	 * "service" needs to be in the appropriate state for "delete"
	 */

	if (strcmp(prop_val, SDSSC_STATE_COMMIT) == 0) {

		/*
		 * "nodeid" for this node
		 */

		if (scconf_get_nodeid(NULL, &nodeid) != SCCONF_NOERR)
			goto exit_routine;

		props[0].scconf_prop_key = SDSSC_PROP_STATE;
		props[0].scconf_prop_value = SDSSC_STATE_DEL;
		props[0].scconf_prop_next = &props[1];

		props[1].scconf_prop_key = SDSSC_PROP_INCAR;
		props[1].scconf_prop_value =
			l_incarnation_to_prop((int)nodeid);
		props[1].scconf_prop_next = (scconf_cfg_prop_t *)0;

		if (scconf_change_ds(service, NULL, SCCONF_STATE_UNCHANGED,
		    NULL, SCCONF_STATE_UNCHANGED, props, 0) != SCCONF_NOERR)
			goto exit_routine;

		rval = SDSSC_OKAY;

	} else if (strcmp(prop_val, SDSSC_STATE_DEL) == 0) {

		/*
		 * Check the following situations:
		 *
		 *	- on-going
		 *	- uncommitted
		 */

		if (scconf_get_nodeid(NULL, &nodeid) != SCCONF_NOERR)
			goto exit_routine;

		if (l_get_property(config, SDSSC_PROP_INCAR, &prop_incar)
		    != SDSSC_OKAY)
			goto exit_routine;

		/*
		 * If deletion is in progress then simply just return
		 */
		if (l_compare_incarnation(
		    l_prop_to_incarnation(prop_incar),
		    l_get_incarnation(nodeid)) == SDSSC_False) {

			/*
			 * The node may have "reboot" since the mark of "del"
			 * Go ahead remove the definition from DCS
			 */
			if (scconf_rm_ds(service, NULL, NULL, NULL, 0) !=
			    SCCONF_NOERR)
				goto exit_routine;
		}

		rval = SDSSC_OKAY;
	}

exit_routine:
	if (config)
		scconf_free_ds_config(config);

	return (rval);
}

void
_bind_devices()
{
	mdc_err_t	**elp,
			**elp_orig;

	if (_sdssc_bindclusterdevs(&elp) != SDSSC_OKAY) {
		elp_orig = elp;

		while (elp && (*elp != NULL)) {
			if ((*elp)->mdc_errno != 0) {
				syslog(LOG_ERR,
				    gettext("ERROR: %s: %s %d\n"),
				    (*elp)->mdc_node,
				    (*elp)->mdc_misc,
				    (*elp)->mdc_errno);
				printf(gettext("ERROR: %s: %s %d\n"),
				    (*elp)->mdc_node,
				    (*elp)->mdc_misc,
				    (*elp)->mdc_errno);
			}
			elp++;
		}
		_sdssc_free_mdcerr_list(elp_orig);
	}
}

rval_e
_sdssc_delete_end(char *service, dcs_set_state_e state)
{
	char			*prop_val;
	scconf_cfg_ds_t		*config = NULL;
	scconf_cfg_prop_t	prop;
	scconf_errno_t		sc_rval;
	scswitch_service_t	servicelist;
	rval_e			rval = SDSSC_ERROR;

	if (scconf_get_ds_config(service, SCCONF_BY_NAME_FLAG, &config) !=
	    SCCONF_NOERR) {
		goto exit_routine;
	}

	if (l_get_property(config, SDSSC_PROP_STATE, &prop_val)
	    != SDSSC_OKAY) {
		goto exit_routine;
	}

	/*
	 * We have value for property "state"
	 */

	if (strcmp(prop_val, SDSSC_STATE_DEL) == 0) {

		/*
		 * Okay to remove the service
		 */
		if (state == SDSSC_COMMIT) {

			servicelist.scswitch_service_name = service;
			servicelist.next = NULL;

			if (scswitch_take_service_offline(&servicelist) !=
			    SCSWITCH_NOERR)
				goto exit_routine;

			/*
			 * Commit the "delete"
			 */
			if ((sc_rval = scconf_rm_ds(service,
			    NULL, NULL, NULL, 0)) != SCCONF_NOERR) {
				goto exit_routine;
			} else {
				_bind_devices();
				rval = SDSSC_OKAY;
			}
		} else if (state == SDSSC_CLEANUP) {

			prop.scconf_prop_key = SDSSC_PROP_STATE;
			prop.scconf_prop_value = SDSSC_STATE_COMMIT;
			prop.scconf_prop_next = NULL;

			if (scconf_change_ds(service, NULL,
			    SCCONF_STATE_UNCHANGED, NULL,
			    SCCONF_STATE_UNCHANGED, &prop, 0)
			    == SCCONF_NOERR) {
				goto exit_routine;
			}
		}
	}

	/*
	 * Free any memory allocated
	 */

exit_routine:
	if (config)
		scconf_free_ds_config(config);

	return (rval);
}
