/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident   "@(#)sdssc_version.c 1.11     08/05/20 SMI"

/*
 * This interface returns structure sdssc_version.
 * Currently this interface returns "3" for major number and "0" for minor
 * for Solaris Cluster "3.0" release.  This interface also returns the library
 * increment.
 */

#define	CLUSTER_LIBRARY_SOURCE
#include <meta.h>
#include <metadyn.h>
#include <sys/param.h>		/* For MAXPATHLEN */
#include <sys/stat.h>
#include <dlfcn.h>
#include <sdssc.h>

static char *libsds_sc_path = SDSSC_CL_LIBDIR "/sc/libsds_sc.so.1";

rval_e
_sdssc_version(sdssc_version_t *version)
{
	void		*dlhandle;
	const char	*lib_vers = "_lib_version";
	int		*level;

	/*
	 * Currently this routine returns
	 * maj = 3, min = 0 for SC"3.0"
	 *
	 * Load the dl object
	 */

	if ((dlhandle = dlopen(libsds_sc_path, RTLD_LAZY)) == NULL)
		return (SDSSC_ERROR);

	/*
	 * We now have handle that we can do some dlsym search for
	 * data symbol "_lib_version_"
	 */

	if ((level = (int *)dlsym(dlhandle, lib_vers)) == NULL)
		return (SDSSC_ERROR);
	else {
		/*
		 * fill in sdssc_version structure
		 */

		version->major = 3;
		version->minor = 0;
		version->library_level = *level;
	}

	return (SDSSC_OKAY);
}
