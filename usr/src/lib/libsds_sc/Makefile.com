#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.20	08/08/05 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libsds_sc/Makefile.com
#
LIBRARY= libsds_sc.a
VERS= .1

METACL_OBJS = metacl_clnt.o metacl_xdr.o
SDSSC_OBJS =			\
	bind_library.o		\
	create_set.o		\
	sds_scproxy.o		\
	incarnation.o		\
	gettransport.o		\
	getnodelist.o		\
	binddevs.o		\
	bindclusterdevs.o	\
	sdssc_version.o		\
	sdssc_host.o		\
	sdssc_index.o		\
	sdssc_delete.o		\
	meta_mdc.o		\
	dcs_interfaces.o	\
	did_interfaces.o
OBJECTS = $(METACL_OBJS) $(SDSSC_OBJS)

# include library definitions
include $(SRC)/lib/Makefile.lib

CPPFLAGS += $(MDM_FLAGS) -I../common -I$(REF_PROTO)/usr/src/head
CPPFLAGS += -I$(SRC)/common/cl -I../../libdid/include
LDLIBS += -ldl -lelf -lnsl -lgen -lc $(LIBCC)
LDLIBS += -lscconf -lscstat -lscswitch -lmeta -lclconf -lclos -ldid

MAPFILE		= ../common/mapfile-vers
$(POST_S9_BUILD)MAPFILE	= ../common/mapfile-vers.s10
PMAP	= -M $(MAPFILE)
SRCS		= $(OBJECTS:%.o=../common/%.c)

RPCGENFLAGS	= -C -M $(MDM_FLAGS)

ROOTLVMDIR	= $(VROOT)/usr/lib
ROOTLVMLIBS	= $(LIBS:%=$(ROOTLVMDIR)/%)

CHECK_FILES	= $(SDSSC_OBJS:%.o=%.c_check)

#
# metacl.x is available from the s9 reference proto.
#
METACL_X = metacl.x
METACL_X_SRC = $(REF_PROTO)/usr/src/head/$(METACL_X)

LIBS = $(DYNLIB)

CLOBBERFILES += metacl_clnt.c metacl_xdr.c $(METACL_X)

.KEEP_STATE:

all: $(LIBS)

$(DYNLIB):	$(MAPFILE)

#
# Create a symbolic link in the build area to the .x file
# in the Solaris reference proto area.
# This eliminates the need to awk the include
# directives created by rpcgen.
#
$(METACL_X): $(METACL_X_SRC)
	$(RM) -f $@
	$(LN) -s $(METACL_X_SRC) $@

metacl_clnt.c: $(METACL_X)
	$(RPCGEN) $(RPCGENFLAGS) -l $(METACL_X) > $@

metacl_xdr.c: $(METACL_X)
	$(RPCGEN) $(RPCGENFLAGS) -c $(METACL_X) > $@

# include library targets
include $(SRC)/lib/Makefile.targ

objs/%.o pics/%.o: %.c
	$(COMPILE.c) -o $@ $<

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

$(ROOTLVMDIR)/%: % $(ROOTLVMDIR)
	$(INS.file)

$(ROOTLVMDIR):
	$(INS.dir)
