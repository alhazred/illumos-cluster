#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.5	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libclcomm_compat/Makefile.com
#

LIBRARY = libclcomm_compat.a
VERS = .1

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

#  thr_pool.o
CLCOMM_COMPAT_OBJS +=  threadpool.o list.o clusterproc.o \
	sequence.o idl_strings.o string_seq.o \
	dbg_printf.o

OBJECTS += $(CLCOMM_COMPAT_OBJS)

OBJS_DIR = objs

CLUTIL_SRC_DIR =	$(SRC)/common/cl/util
CLDT_SRC_DIR =		$(SRC)/common/cl/orb/idl_datatypes
CLINF_SRC_DIR =		$(SRC)/common/cl/orb/infrastructure


CPPFLAGS += $(CL_CPPFLAGS)

MAPFILE=	../common/mapfile-vers
PMAP =

LIBS = $(LIBRARY)

CLEANFILES =

# definitions for lint
LINTFILES += $(CLCOMM_COMPAT_OBJS:%.o=%.ln)
LINTS_DIR = .

CC_PICFLAGS = -KPIC
C_PICFLAGS = -KPIC

CPPFLAGS += $(MTFLAG)
CFLAGS += $(C_PICFLAGS)
CFLAGS64 += $(C_PICFLAGS)
CCFLAGS += $(CC_PICFLAGS)
CCFLAGS64 += $(CC_PICFLAGS)
CPPFLAGS += -DPIC -D_TS_ERRNO
CPPFLAGS += -I$(SRC)/common/cl

CPPFLAGS += -DCLCOMM_COMPAT

CPPFLAGS += -DSC_SRM

LDLIBS += 

.KEEP_STATE:

all:  $(LIBS)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules
