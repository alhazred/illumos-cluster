#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.34	08/10/06 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/librgmserver/Makefile.com
#

LIBRARY = librgmserver.a
VERS = .1

#include ../../Makefile.lib

include $(SRC)/Makefile.master

# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

RGMSERVER_OBJS += cm_callback_impl.o rgm_callerr.o rgm_confcdb.o rgm_error.o rgm_rg_util.o
RGMSERVER_OBJS += rgm_main.o rgm_starter.o rgm_schedprio.o rgm_comm.o rgm_comm_impl.o rgm_comm_util.o
RGMSERVER_OBJS += rgm_global_res_used.o rgm_intention.o rgm_intention_impl.o
RGMSERVER_OBJS += rgm_launch_method.o rgm_lb.o rgm_lock_state.o rgm_president.o
RGMSERVER_OBJS += rgm_process_resource.o rgm_process_rgs.o rgm_readccr.o
RGMSERVER_OBJS += rgm_recep_cluster.o rgm_recep_rg.o rgm_recep_rs.o rgm_recep_rt.o
RGMSERVER_OBJS += rgm_recep_start.o rgm_recep_utils.o rgm_receptionist.o rgm_rg.o
RGMSERVER_OBJS += rgm_rg_impl.o rgm_rs.o rgm_rs_impl.o rgm_rt_impl.o rgm_run_state.o
RGMSERVER_OBJS += rgm_scha_control.o rgm_scha_control_impl.o rgm_scswitch_impl.o
RGMSERVER_OBJS += rgm_ssm.o rgm_threads.o rgm_util.o rgm_cluster_impl.o
RGMSERVER_OBJS += rgm_event_gen.o rgm_event_impl.o rgm_fencing.o
RGMSERVER_OBJS += rgm_block.o
RGMSERVER_OBJS += rgm_logical_node.o rgm_logical_nodeset.o
RGMSERVER_OBJS += rgm_logical_node_manager.o
RGMSERVER_OBJS += rgm_version.o

DOOR_RGMSERVER_OBJS = rgm_recep_serv_xdr.o
$(RPC_IMPL)DOOR_RGMSERVER_OBJS =
RGMSERVER_OBJS += $(DOOR_RGMSERVER_OBJS)
$(POST_S9_BUILD)RGMSERVER_OBJS += rgm_zone_state.o

RGMRPC_OBJS	= rgm_recep_door_xdr.o
$(RPC_IMPL)RGMRPC_OBJS	= rgm_recep_svc.o rgm_recep_xdr.o

OBJECTS += $(RGMSERVER_OBJS) $(RGMRPC_OBJS) $(CMM_UOBJS)

TEMPLATE_SRCS = rgm_array_free_list.cc rgm_obj_manager.cc

include $(SRC)/common/cl/Makefile.files

OBJS_DIR = objs

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

# include the macros for doors or rpc implementation again because
# Makefile.lib screws up CFLAGS/CPPFLAGS
include $(SRC)/Makefile.ipc 

CPPFLAGS += $(CL_CPPFLAGS)

MAPFILE=	../common/mapfile-vers
PMAP =
SRCS =		$(OBJECTS:%.o=../common/%.c)

#LIBS =  $(DYNLIB) $(LIBRARY)
LIBS =  $(DYNLIB)

CLEANFILES =

CHECKHDRS = cm_callback_impl.h rgm_api.h rgm_comm.h rgm_global_res_used.h \
	rgm_intention.h rgm_proto.h rgm_state.h rgm_state_strings.h \
	rgm_threads.h rgmd_util.h rgm_logical_nodeset.h rgm_logical_node.h \
	rgm_obj_manager.h rgm_logical_node_manager.h rgm_array_free_list.h

CHECKHDRS += $(CMM_HU)
CHECK_FILES = $(RGMSERVER_OBJS:%.o=%.c_check) \
	$(CMM_UOBJS:%.o=%.c_check) $(CHECKHDRS:%.h=%.h_check)
CHECK_FILES += $(TEMPLATE_SRCS:%.cc=%.c_check)

# definitions for lint
LINTFILES += $(OBJECTS:%.o=%.ln) $(TEMPLATE_SRCS:%.cc=%.ln)
LINTS_DIR = .

CC_PICFLAGS = -KPIC
C_PICFLAGS = -KPIC

MTFLAG = -mt

CPPFLAGS += $(MTFLAG)
CFLAGS += $(C_PICFLAGS)
CFLAGS64 += $(C_PICFLAGS)
CCFLAGS += $(CC_PICFLAGS)
CCFLAGS64 += $(CC_PICFLAGS)
CPPFLAGS += -DPIC -D_TS_ERRNO -D_POSIX_PTHREAD_SEMANTICS
CPPFLAGS += -I$(SRC)/lib/librgmx/common
CPPFLAGS += -I../common -I$(SRC)/common/cl/interfaces/$(CLASS)
CPPFLAGS += -I$(SRC)/common/cl
$(POST_S9_BUILD)CPPFLAGS += -I$(REF_PROTO)/usr/src/head
$(POST_S9_BUILD)CPPFLAGS += -I$(REF_PROTO)/usr/src/lib/libbrand/common
$(POST_S9_BUILD)CPPFLAGS += -I$(REF_PROTO)/usr/src/lib/libuutil/common
CPPFLAGS += -DSC_SRM
$(POST_S9_BUILD)CPPFLAGS += -I$(SRC)/common/cl/privip

LDLIBS += -lc -lclcomm -lclconf -lclos -lnsl -lscconf $(REF_USRLIB:%=%/libCrun.so.1)
$(POST_S9_BUILD)LDLIBS += -lzccfg -lsocket
LDLIBS += $(DOOR_LDLIBS)
LDLIBS += -lrt -lclevent -ldl -lsecurity -lscutils -lfe -lrgm \
          -lclevent -lscrgadm -lsczones
LDLIBS += -lgen

##########################################
LIBRGMX = $(SRC)/lib/librgmx/$(MACH)/librgmx.a
DLFMM = $(SRC)/lib/dlfmm/$(MACH)/libdlfmm.a
DLSCXCFG = $(SRC)/lib/dlscxcfg/$(MACH)/libdlscxcfg.a
DLFENCE = $(SRC)/lib/dlfence/$(MACH)/libdlfence.a

LDLIBS += -lrgmx $(LIBRGMX)
LDLIBS += $(DLFMM)
LDLIBS += $(DLSCXCFG)
LDLIBS += $(DLFENCE)

LDLIBS += -lproject

$(POST_S9_BUILD)LDLIBS += $(REF_PROTO)/usr/lib/libzonecfg.so.1
$(POST_S9_BUILD)LDLIBS += -lscprivip -lc $(REF_USRLIB:%=%/libCstd.so.1)

#############################################

RPCGENFLAGS	= $(RPCGENMT) -C -K -1

RPCFILE_XDR     = $(RPCFILE:$(SRC)/head/rgm/door/%.x=../common/%_xdr.c)
RPCFILE = $(SRC)/head/rgm/door/rgm_recep_door.x
RPCFILE_SVC = 

$(RPC_IMPL)RPCFILE_SVC	= $(RPCFILE:$(SRC)/head/rgm/rpc/%.x=../common/%_svc.c)
$(RPC_IMPL)RPCFILE_XDR	= $(RPCFILE:$(SRC)/head/rgm/rpc/%.x=../common/%_xdr.c) 
$(RPC_IMPL)RPCFILE = $(SRC)/head/rgm/rpc/rgm_recep.x

CLOBBERFILES += $(RPCFILE_SVC) $(RPCFILE_XDR)

.KEEP_STATE:

$(DYNLIB): $(MAPFILE) 

all:  $(LIBS)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
