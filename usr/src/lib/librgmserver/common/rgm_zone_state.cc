//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_zone_state.cc	1.22	08/11/06 SMI"

// SC includes
#include <rgm_proto.h>
#include <rgm/sczones.h>
#include <rgm/rgm_msg.h>
#include <sys/vc_int.h>

// standard includes
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_pres.h>
#endif

static void process_zone_state_helper(sol::nodeid_t mynodeid,
    const char *zonename, rgm::lni_t assigned_lni,
    rgm::rgm_ln_state state, rgm::rgm_ln_state old_state,
    bool state_set, rgm::ln_incarnation_t incarn);

void
process_zone_state_change(const char *zonename, rgm::rgm_ln_state state)
{
#ifdef EUROPA_FARM
	rgmx_zone_state_change_args arg;
	rgmx_zone_state_change_result result;
	CLIENT *clnt = NULL;
	enum clnt_stat rpc_res;
#else
	rgm::rgm_comm_var prgm_pres_v;
#endif

	rgm::lni_t assigned_lni = LNI_UNDEF;
	sol::nodeid_t mynodeid = get_local_nodeid();
	char *ln = NULL;
	LogicalNode *lnNode = NULL;
	bool state_set = false;
	// old_state defaults to UNCONFIGURED for a new LN
	rgm::rgm_ln_state old_state = rgm::LN_UNCONFIGURED;

	// default to zero if this is a new zone
	rgm::ln_incarnation_t incarn = 0;

	// set to 0 initially
	sol::nodeid_t presid = 0;

	// first, grab the lock and lookup the logical node
	rgm_lock_state();

	// zone membership is only updated for the global zone rgmd.
	CL_PANIC(strcmp(ZONE, GLOBAL_ZONENAME) == 0);


	// ignore zonename string component, zone refers to their node itself
	lnNode = lnManager->findLogicalNode(mynodeid, zonename, &ln);

	if (lnNode != NULL) {
		//
		// Tell ourself first so our local concept of the state is
		// correct, to reduce the time window during which we could
		// be trying to execute stuff on behalf of that zone (if
		// it goes down).
		//
		// retrieve the old state so we can see if we should check
		// for running boot methods
		//
		old_state = lnNode->getStatus();

		// We reject a transition from a down state to SHUTTING_DOWN
		if (old_state < rgm::LN_SHUTTING_DOWN &&
		    state == rgm::LN_SHUTTING_DOWN) {
			ucmm_print("process_zone_state_change()", NOGET(
			    "ignoring zone %s from down to SHUTTING_DOWN"),
			    zonename);
			rgm_unlock_state();
			return;
		}

		//
		// If we already know about the node, tell the pres the
		// incarnation number we know about
		//
		// However, if we are running at a "pre-ZC" version, leave
		// incarn set to 0, so the president will set that value.
		// In the old code, the slave side always sets the
		// incarnation number of an existing zone back to 0.
		// See CR 6620504 for details.
		//
		if (is_rgm_zc_version()) {
			incarn = lnNode->getIncarnation();
		}

		ucmm_print("process_zone_state_change()", NOGET(
		    "after setting state: \n"));

		lnManager->debug_print();
	}

	//
	// If old_state is LN_UNCONFIGURED, exit early.  We won't
	// contact the president or create an LN until the zone has transitioned
	// to LN_UP status for the first time.  The zone is considered to be
	// "non-existent" from the scha API point of view.  See CR 6591545
	// for more info.
	//
	// This also avoids rolling upgrade problems.  In SC 3.2 GA, the
	// state LN_UNCONFIGURED was never passed to idl_zone_state_change(),
	// in fact that function would get an assertion failure if that
	// state were passed to it.  In SC 3.2 update 1, we modified
	// idl_zone_state_change() so that the LN_UNCONFIGURED state might
	// be passed to it.  However, during a rolling upgrade, the slave
	// node could be running the later version (3.2u1 or above) while
	// the president node ran the earlier version (3.2 GA).  In that
	// case, you could get an assertion failure, reported as CR 6743453.
	// By avoiding passing LN_UNCONFIGURED zone state to
	// idl_zone_state_change(), we avoid this bug.
	//
	if (old_state == rgm::LN_UNCONFIGURED && state != rgm::LN_UP) {
		ucmm_print("process_zone_state_change()", NOGET(
		    "UNCONFIGURED zone %s not yet fully booted"), zonename);
		rgm_unlock_state();
		return;
	}

	// now unlock the state mutex so we can call the pres
	rgm_unlock_state();

	//
	// We do not hold the lock during this phase, because
	// that could cause a deadlock. The president needs to grab
	// its lock to process the method. It could be in the middle
	// of a call to this slave, which is waiting for a lock on this
	// side.
	//
	// There might not be a president registered yet, or the
	// president might have died. Loop until we hit an
	// unrecoverable error or we contact the president.
	//

#ifndef EUROPA_FARM
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
#endif
	while (1) {
#ifdef EUROPA_FARM
		clnt = rgmx_comm_getpres(ZONE);
		if (clnt != NULL) {
			// In case of RPC timeout, we assume that the president
			// died and we retry the call to the new president.
			arg.node = mynodeid;
			arg.zonename = (char *)zonename;
			arg.state = (rgm_ln_state)state;
			arg.incarn = incarn;
			rpc_res = rgmx_zone_state_change_1(&arg, &result, clnt);
			if ((rpc_res != RPC_SUCCESS) &&
			    (rpc_res != RPC_TIMEDOUT)) {
				//
				// Crash and burn.
				//
				os::sc_syslog_msg logger(
				    SC_SYSLOG_RGM_RGMD_TAG, "", NULL);
				//
				// SCMSGS
				// @explanation
				// The rgmd on this node was unable to make an
				// inter-node IDL call. The rgmd will abort
				// and the node will panic.
				// @user_action
				// Save a copy of the /var/adm/messages files
				// on all nodes and contact your authorized
				// Sun service provider for assistance in
				// diagnosing and correcting the problem.
				//
				(void) logger.log(LOG_ERR, MESSAGE,
				    "fatal: cannot contact president");
				abort();
				// NOTREACHED
			}
			if (rpc_res == RPC_SUCCESS) {
				assigned_lni = result.assigned_lni;

				// everything went fine -- create the logical
				// node on this zone (if we haven't already)
				rgm_lock_state();

				process_zone_state_helper(mynodeid, zonename,
				    assigned_lni, state, old_state, state_set,
				    incarn);
				rgm_unlock_state();
				xdr_free((xdrproc_t)
				    xdr_rgmx_zone_state_change_result,
				    (char *)&result);
				rgmx_comm_freecnt(clnt);
				return;
			}

			// some unexpected RPC error
			// node has died; wait and try again
			ucmm_print("process_zone_state_change()", NOGET(
			    "unable to contact pres -- will retry\n"));

			xdr_free((xdrproc_t)xdr_rgmx_zone_state_change_result,
			    (char *)&result);
			rgmx_comm_freecnt(clnt);
		} else {
			//
			// sleep to avoid spin-looping in case
			// rgmx_comm_getpres() returned NULL
			//
			(void) sleep(1);
		}
#else
		Environment e;

		if (!CORBA::is_nil(prgm_pres_v)) {
			//
			// the incarnation is either the one we grabbed
			// from the zone after setting its state or
			// the initial zero if we don't yet know about the
			// zone. In that case, both the president and slave
			// will start out with the incarnation of 0.
			//
			prgm_pres_v->idl_zone_state_change(mynodeid, zonename,
			    state, incarn, assigned_lni, e);

			//
			// grab the lock to grab the president id
			//
			rgm_lock_state();
			presid = Rgm_state->rgm_President;
			rgm_unlock_state();

			switch (except_to_errcode(e, presid)) {
			case RGMIDL_NODE_DEATH:
			case RGMIDL_RGMD_DEATH:
				// node has died; wait and try again
				ucmm_print("process_zone_state_change()", NOGET(
				    "unable to contact pres -- will retry\n"));
				break;
			case RGMIDL_OK:
				//
				// If the local node is the president, then
				// the call to idl_zone_state_change() has
				// set the state of the LN.  We set
				// state_set to true so we won't try to set
				// it again in process_zone_state_helper().
				//
				if (mynodeid == presid) {
					state_set = true;
				}
				//
				// everything went fine -- create the logical
				// node on this zone (if we haven't already)
				//
				rgm_lock_state();
				process_zone_state_helper(mynodeid, zonename,
				    assigned_lni, state, old_state, state_set,
				    incarn);
				rgm_unlock_state();
				return;

			case RGMIDL_UNKNOWN:
			case RGMIDL_NOREF:
			default:
				//
				// Crash and burn.
				//
				os::sc_syslog_msg logger(
				    SC_SYSLOG_RGM_RGMD_TAG, "", NULL);
				(void) logger.log(LOG_ERR, MESSAGE,
				    "fatal: cannot contact president");
				abort();
				// NOTREACHED
			}
		}

		// President is not yet registered, or has died.  In either
		// case, a new president will be elected soon.
		// Sleep until new president is registered in name service,
		// then loop back and retry.
		//
		// Note that rgm_comm_waitforpres() is guaranteed to return
		// a new president object pointer, different from the current
		// value of prgm_pres_v.
		//
		ucmm_print("process_zone_state_change()",
		    NOGET("process_zone_state_change: "
		    "about to call rgm_comm_waitforpres\n"));
		prgm_pres_v = rgm_comm_waitforpres(prgm_pres_v);
#endif // EUROPA_FARM
	} // while (1)
}

void
process_zone_state_helper(sol::nodeid_t mynodeid, const char *zonename,
    rgm::lni_t assigned_lni, rgm::rgm_ln_state state,
    rgm::rgm_ln_state old_state, bool state_set, rgm::ln_incarnation_t incarn)
{
	char *ln = NULL;
	LogicalNode *lnNode = lnManager->findLogicalNode(mynodeid, zonename,
	    &ln);

	//
	// it's ok if it's there already -- just
	// assert that our lni matches the pres
	// and set the state if not set earlier
	// (this would only occur if, in the time
	// that we contacted the pres, the pres
	// told us about the mapping for this
	// LN, in the read_Ccr call)
	//
	if (lnNode != NULL) {
		CL_PANIC(lnNode->getLni() == assigned_lni);
		if (!state_set) {
			//
			// Set the incarnation number before invoking
			// setStatus(); if the status goes from down to up,
			// the incarnation number will be incremented.
			// The same action was taken on the president,
			// so the incarnation numbers will stay in sync.
			//
			// However, if we are running at a pre-zc version,
			// we have to set the incarnation number after setting
			// the status, to match what the president did.
			//
			boolean_t zc_version_running = is_rgm_zc_version();
			if (zc_version_running) {
				lnNode->setIncarnation(incarn);
			}
			old_state = lnNode->getStatus();
			lnNode->setStatus(state);
			if (!zc_version_running) {
				lnNode->setIncarnation(incarn);
			}
		}
	} else {
		// otherwise, create it
		lnNode = lnManager->createLogicalNodeFromMapping(mynodeid,
		    zonename, assigned_lni, state);

		//
		// If the zone is being created in the UP state, we need to
		// increment the incarnation number, because we won't be
		// invoking setStatus() [which would have incremented it].
		//
		// However, if we are running at a pre-ZC version, don't
		// increment it, because it won't be incremented on the
		// president side either.
		//
		if (is_rgm_zc_version()) {
			if (state == rgm::LN_UP) {
				incarn++;
			}
		}
		lnNode->setIncarnation(incarn);
	}

	ucmm_print("process_zone_state_change()", NOGET(
	    "after calling pres: \n"));

	//
	// If the zone is in LN_UP
	// state, and it wasn't LN_UP before,
	// we need to possibly run boot methods
	//
	if (state == rgm::LN_UP && state != old_state) {
		lnNode->check_run_boot_meths();
	}

	lnManager->debug_print();
}

void
up_callback(const char *zonename)
{

	int ret;
	uint_t id;
	ucmm_print("up_callback()", NOGET("Received UP callback for zone %s\n"),
	    zonename);

	CL_PANIC(strcmp(ZONE, GLOBAL_ZONENAME) == 0);
	if (((ret = clconf_get_cluster_id((char *)zonename, &id)) == 0) &&
	    (id >= MIN_CLUSTER_ID)) {
		// its a CZ. ignore the zone
		return;
	}
	ucmm_print("up_callback()", NOGET("this is a native zone"));
	if (security_svc_add_zone(RGM_SCHA_PROGRAM_CODE, zonename) != 0) {
		ucmm_print("up_callback()", NOGET(
		    "security_svc_add_zone failed"));
		return;
	}
	ucmm_print("up_callback()", NOGET(
	    "adding %s to rgm's zonelist"), zonename);
	process_zone_state_change((char *)zonename, rgm::LN_UP);
}

void
down_callback(const char *zonename)
{
	int ret;
	uint_t id;
	ucmm_print("down_callback()", NOGET("Received DOWN callback for "
	    "zone %s\n"), zonename);
	CL_PANIC(strcmp(ZONE, GLOBAL_ZONENAME) == 0);
	if (((ret = clconf_get_cluster_id((char *)zonename, &id)) == 0) &&
	    (id >= MIN_CLUSTER_ID)) {
		// its a CZ. ignore the zone
		return;
	}
	ucmm_print("down callback()", "zone %s processed", zonename);
	process_zone_state_change(zonename, rgm::LN_DOWN);
}

void
shutting_down_callback(const char *zonename)
{
	int ret;
	uint_t id;
	ucmm_print("shutting_down_callback()", NOGET("Received SHUTTING_DOWN "
	    "callback for zone %s\n"),  zonename);
	CL_PANIC(strcmp(ZONE, GLOBAL_ZONENAME) == 0);
	if (((ret = clconf_get_cluster_id((char *)zonename, &id)) == 0) &&
	    (id >= MIN_CLUSTER_ID)) {
		// its a CZ. ignore the zone
		return;
	}
	ucmm_print("shutting_down_callback()", "zone %s being processed\n",
	    zonename);
	process_zone_state_change(zonename, rgm::LN_SHUTTING_DOWN);

}

void
stuck_callback(const char *zonename)
{
	int ret;
	uint_t id;
	ucmm_print("stuck_callback()", NOGET("Received STUCK callback for zone "
	    "%s\n"), zonename);
	CL_PANIC(strcmp(ZONE, GLOBAL_ZONENAME) == 0);
	if (((ret = clconf_get_cluster_id((char *)zonename, &id)) == 0) &&
	    (id >= MIN_CLUSTER_ID)) {
		// its a CZ. ignore the zone
		return;
	}
	process_zone_state_change(zonename, rgm::LN_STUCK);
}

void
initialize_zone_callbacks(void)
{
	CL_PANIC(strcmp((char *)ZONE, GLOBAL_ZONENAME) == 0);
	// we don't care about STARTING
	zone_state_callback_t callbacks[5] = {NULL, up_callback, down_callback,
	    shutting_down_callback, stuck_callback};

	ucmm_print("initialize_zone_callbacks()", NOGET("Registering for "
	    "zone callbacks\n"));

	if (register_zone_state_callbacks(callbacks, 5) != 0) {
		os::sc_syslog_msg logger(SC_SYSLOG_RGM_RGMD_TAG, "", NULL);
		(void) logger.log(LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: Error registering for zone state change "
			"notifications"));
		exit(1);
	}
}
