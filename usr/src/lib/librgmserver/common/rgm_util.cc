//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//


#pragma ident	"@(#)rgm_util.cc	1.307	09/04/22 SMI"

#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#ifndef EUROPA_FARM
#include <orb/fault/fault_injection.h>
#endif
#include <signal.h>
#include <sys/rsrc_tag.h>
#include <h/ccr.h>
#include <rgm_proto.h>
#include <rgm_api.h>
#include <rgm_threads.h>
#include <rgm/rgm_util.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <scadmin/scconf.h>
#include <string.h>
#include <rgm/rgm_errmsg.h>
#include <rgm_state_strings.h>
#include <sys/cl_events.h>
#include <sys/systeminfo.h>
#include <unistd.h>
#include <thread.h>
#include <synch.h>
#include <wait.h>
#ifndef linux
#include <sys/uadmin.h>
#endif
#ifndef EUROPA_FARM
#include <orb/fault/fault_injection.h>
#endif
#include <nslib/ns.h>
#include <h/ccr.h>
#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_pres.h>
#include <netdb.h>
#else
#include <orb/object/adapter.h>
#endif
#include <stdio.h>

#if SOL_VERSION >= __s10
#include <libzonecfg.h>
#endif



#include <orb/infrastructure/orb.h>
#include <cmm/cmm_ns.h>
#include <h/membership.h>

/* zone support */
#include <rgm_logical_node.h>
#include <rgm_logical_node_manager.h>
#include <rgm_logical_nodeset.h>
#include<rgm/rgm_private.h>
#include <sys/vc_int.h>

// SC SLM addon start
#include <rgm/fecl.h>
// SC SLM addon end
using namespace std;

#include <rgmx_hook.h>

#define	LONG_INT_PRINT_WIDTH	15

//
// rgmd registers for CCR infrastructure table callbacks.
// When CCR delivers callback to rgmd on infrastructure table changes,
// rgmd services this callback in a dedicated thread (infr_table_cb_thread).
// The infr_table_cb_thread uses these synchronization primitives.
//
static os::mutex_t infr_table_cb_lock;
static os::condvar_t infr_table_cb_cv;
static bool no_new_infr_table_cb = true;

#ifndef EUROPA_FARM
// local helper functions
static void check_dependencies_rg(rglist_p_t rgp,
    rgm::rgm_rg_state rgx_state_old, rgm::rgm_rg_state rgx_state_new);
static void check_starting_dependencies_rg(rglist_p_t rgp);
static void check_stopping_dependencies_rg(rglist_p_t rgp);
static void check_dependencies_resource(rgm::lni_t lni, rlist_p_t rp,
    rgm::rgm_r_state rx_state);
static scha_errmsg_t set_resource_switch(name_t rname, name_t rgname,
    char *rswitch, scha_switch_t swtch, LogicalNodeset *ns);
void pnext2str(std::map<rgm::lni_t, char *> &ptr, name_t key, char **res);

static void check_starting_dependencies_from_list(rdeplist_t *deps_list,
    name_t r_name, deptype_t deps_kind);
static void check_stopping_dependencies_from_list(rdeplist_t *deps_list,
    name_t r_name, deptype_t deps_kind);
const char *rgm_lni_to_nodename(rgm::lni_t lni);
static void check_starting_dependencies(rlist_p_t rp, LogicalNodeset &ns);
static void start_dependencies_resolved(rlist_p_t rp, LogicalNodeset &ns);
static void check_stopping_dependencies(rlist_p_t rp, LogicalNodeset &ns);
static void stop_dependencies_resolved(rlist_p_t rp, LogicalNodeset &ns);
static boolean_t are_stopping_dependencies_resolved(rlist_p_t rp,
    rdeplist_t *deps_list, deptype_t deps_kind, LogicalNodeset &ns);
static boolean_t is_dep_local_only(rlist_p_t rp, rlist_p_t rp_dep,
    boolean_t check_strong);
static void restart_deps(rlist_p_t, rdeplist_t *, boolean_t, LogicalNodeset &);

static boolean_t is_switch_disabled(std::map<rgm_lni_t, scha_switch_t> &,
    LogicalNodeset &);
static boolean_t can_rg_have_restarting_r(rgm::rgm_rg_state rg_state);
static void check_just_started(rlist_p_t rp);
static void add_phys_zones_to_nodeset(LogicalNodeset &nset);

#endif // !EUROPA_FARM

static rgm_timelist_t *rgm_clone_timelist(rgm_timelist_t *tlist);
//
// Utility functions for mdb logging
//
static void mdb_logrxstate(rlist_p_t rp, rgm::lni_t lni,
	boolean_t update_backup);
void clone_rxentry(mdb_rxstate_t& mdb_rx_state,
	r_xstate_t& r_xstate, rgm::lni_t lni);
void clone_rgxentry(mdb_rgxstate_t& mdb_rgx_state,
	rg_xstate_t& rg_xstate, rgm::lni_t lni);


static scha_errmsg_t set_affinitents_helper(char *rg_name,
    namelist_t *affinity_list, affinitytype_t aff_type);

static boolean_t combined_cycles_visit_r_node(namelist_t **visited_nodes,
    rglist_p_t rgp, rdeplist_t *deps_list, boolean_t has_rg_dep,
    const char *originating_rg_name, scha_errmsg_t *res);
static boolean_t detect_cycles_visit(namelist_t **visited_nodes,
    rglist_p_t curr_rg, boolean_t has_rg_depend,
    const char *originating_rg_name, scha_errmsg_t *res);
static void abort_zone(boolean_t reboot_node, const char *zonename);

// SC SLM addon start
// function used to notify local fed that scconf SCSLM per node properties have
// changed.
static void scslm_get_scconf_props(scconf_cfg_node_t *);
static unsigned int slm_global_zone_shares;
static unsigned int slm_default_pset_min;
static unsigned int prev_slm_global_zone_shares = 1;
static unsigned int prev_slm_default_pset_min = 1;
// SC SLM addon end

//
// sets the corepath and the coredump flags into the fe_cmd structure.
// in case of error, ignores the error; and doesn't set core dump flag.
//
void
set_fed_core_props(fe_cmd &fed_cmd, const char *fed_tag, char *zonename,
    boolean_t globalzoneflag)
{
	char			zonepath[MAXPATHLEN];
	sc_syslog_msg_handle_t	handle;

	if (!Method_dumpflag)
		return;

#if SOL_VERSION >= __s10
	// if we have a valid non global zonename and the method doesn't run
	// in the global zone (GLOBAL_ZONE RT), then add zonepath
	if (zonename && !globalzoneflag &&
	    strcmp(GLOBAL_ZONENAME, zonename) != 0) {
		if (zone_get_rootpath(zonename, zonepath,
		    MAXPATHLEN) != Z_OK) {
			ucmm_print("set_fed_core_props",
			    "zone_get_rootpath failed:%s", zonename ? zonename :
			    "null");
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The RGM could not query the zonepath for setting
			// the location where cores for the data service
			// process tree can get stored, if the method times out.
			// @user_action
			// The core collection facility will be disabled for
			// this method tag.
			// Save a copy of /var/adm/messages files on this
			// node and Contact your authorized Sun service
			// provider for assistance in diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("Failed to set core location properties "
			    "for the method tag <%s>. Error:<%d>"),
			    fed_tag, errno);
			sc_syslog_msg_done(&handle);
			return;
		}
		fed_cmd.zonepath = strdup(zonepath);
	}
#endif
	fed_cmd.corepath = strdup(DEFAULT_COREPATH);
	fed_cmd.flags |= FE_PRODUCE_CORE;
}


//
// is_local_mon_enabled - test whether the Resource has monitoring enabled
//
boolean_t
is_local_mon_enabled(rlist_p_t r, rgm::lni_t lni)
{

	if (((rgm_switch_t *)r->rl_ccrdata->r_monitored_switch)
	    ->r_switch[lni] == SCHA_SWITCH_ENABLED) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}


//
// is_r_monitored
// If the resource is enabled and has monitoring enabled and has a
// monitor_start method registered, return TRUE.
// Otherwise, return FALSE.
//
// If the resource is scalable, we consider a monitor_start
// method to be registered (even if there is none) because the ssm_wrapper
// will be called to perform monitor_start.
//
// If pointer rt_meth_name is non-NULL and a MONITOR_START method is
// registered, duplicates the method pathname for the caller.  The caller
// is responsible for freeing the storage.  We do this because we cannot
// give launch_method a pointer into the state structure (launch_method
// doesn't hold the lock at that point).
//
// Note if r is enabled and has monitoring enabled, and MONITOR_START is not
// registered, and rt_meth_name is non-NULL, then is_method_reg will set
// *rt_meth_name to NULL.  This should be a no-op because the caller should
// have already initialized *rt_meth_name to NULL.
//
boolean_t
is_r_monitored(rlist_p_t r, name_t *rt_meth_name, rgm::lni_t lni)
{
	if (((rgm_switch_t *)r->rl_ccrdata->r_onoff_switch)->r_switch[lni]
	    == SCHA_SWITCH_ENABLED &&
	    is_local_mon_enabled(r, lni) &&
	    (is_method_reg(r->rl_ccrtype, METH_MONITOR_START, rt_meth_name) ||
	    is_r_scalable(r->rl_ccrdata))) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}

//
// RGM state lock should be held before invoking
// this function.
//
const char *
rgm_lni_to_nodename(rgm::lni_t lni)
{
	LogicalNode *ln = lnManager->findObject(lni);
	return (ln->getFullName());
}

//
// is_r_monitor_checked
// If the resource is enabled and has monitoring enabled and has a
// monitor_check method registered, return TRUE.
// Otherwise, return FALSE.
//
// If the resource is scalable, we consider a monitor_check
// method to be registered (even if there is none) because the ssm_wrapper
// will be called to perform monitor_check.
//
boolean_t
is_r_monitor_checked(rlist_p_t r, rgm::lni_t lni)
{
	if (((rgm_switch_t *)r->rl_ccrdata->r_onoff_switch)
	    ->r_switch[lni] == SCHA_SWITCH_ENABLED &&
	    is_local_mon_enabled(r, lni) &&
	    (is_r_scalable(r->rl_ccrdata) ||
	    is_method_reg(r->rl_ccrtype, METH_MONITOR_CHECK, NULL))) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}


//
// rt_method_name
//
// Given a resource type 'prt' and the method type 'umethod',
// returns the name of the method to call.
//
name_t
rt_method_name(rgm_rt_t *prt, method_t umethod)
{
	name_t methname = NULL;

	switch (umethod) {

	case METH_START:
		methname = prt->rt_methods.m_start;
		break;

	case METH_STOP:
		methname = prt->rt_methods.m_stop;
		break;

	case METH_VALIDATE:
		methname = prt->rt_methods.m_validate;
		break;

	case METH_UPDATE:
		methname = prt->rt_methods.m_update;
		break;

	case METH_INIT:
		methname = prt->rt_methods.m_init;
		break;

	case METH_FINI:
		methname = prt->rt_methods.m_fini;
		break;

	case METH_BOOT:
		methname = prt->rt_methods.m_boot;
		break;

	case METH_MONITOR_START:
		methname = prt->rt_methods.m_monitor_start;
		break;

	case METH_MONITOR_STOP:
		methname = prt->rt_methods.m_monitor_stop;
		break;

	case METH_MONITOR_CHECK:
		methname = prt->rt_methods.m_monitor_check;
		break;

	case METH_PRENET_START:
		methname = prt->rt_methods.m_prenet_start;
		break;

	case METH_POSTNET_STOP:
		methname = prt->rt_methods.m_postnet_stop;
		break;

	default:
		return (NULL);
	}

	// emptry string => method not registered
	if ((methname == NULL) || methname[0] == '\0')
		return (NULL);

	return (methname);
}


//
// Convert a given name, which happens to have a list of string values, into
// a linked list of names, suitable to be passed to lower level APIs
// which work with CCR. Memory is allocated by this function. The caller
// should make sure that it gets freed properly.
//
//	Typical usage of this function is to convert a sequence (used in IDL)
// to a namelist array (used in the lower level APIs).
//
// This function is just a wrapper for two helper functions:
//
// get_value_list_ext searches the nv_seq for the nameval_node_t with key that
// matches property.
//
// convert_value_string_array does the real work of actually converting the
// nameval_node_t's value (an idl_string_seq_t) to a namelist_t.
//
namelist_t *
get_value_string_array_ext(const char *property,
    const rgm::idl_nameval_node_seq_t &nv_seq)
{
	const rgm::idl_string_seq_t & valseq =
	    get_value_list_ext(property, nv_seq);
	return (convert_value_string_array(valseq));
}

//
// is_method_reg
//
// Tests whether the given method 'm' is registered for the resource
// type 'rt'.  Returns TRUE if it is; otherwise FALSE.
// Further, if pointer rt_meth_name is non-NULL, duplicates the string
// for the caller.  The caller is responsible for freeing the storage.
// We do all this because we cannot give launch_method a pointer into
// the state structure (launch_method doesn't hold the lock at that point).
//
// If m is not registered and rt_meth_name is non-NULL, sets *rt_meth_name
// to NULL.
//
boolean_t
is_method_reg(
	rgm_rt_t	*rt,
	method_t	m,
	name_t	*rt_meth_name)
{
	name_t methname;

	if ((methname = rt_method_name(rt, m)) != NULL) {

		//
		// The method is registered; make a copy of it for
		// the caller if requested (if rt_meth_name not null).
		//
		if (rt_meth_name != NULL) {
			*rt_meth_name = strdup(methname);
		}
		return (B_TRUE);
	} else {
		// The method isn't registered.  Set NULL name for
		// caller if requested (if rt_meth_name not null).
		if (rt_meth_name != NULL) {
			*rt_meth_name = NULL;
		}
		return (B_FALSE);
	}
}


//
// find_rt_in_memory
//
// Search in our internal state structure for RT 'rtname'.
//
// Returns the pointer to the RT in memory if the RT is found in memory,
// otherwise returns NULL.
//
rgm_rt_t *
find_rt_in_memory(const char *rtname)
{
	rgm_rt_t	*rtp;

	for (rtp = Rgm_state->rgm_rtlist; rtp != NULL; rtp = rtp->rt_next) {
		if (strcmp(rtp->rt_name, rtname) == 0) {
			// we found the RT in our internal state structure
			break;
		}
	}
	return (rtp);
}



//
// rtname_to_rt
//
// Given the name of an RT as input, convert it into a pointer to
// the corresponding RT entry in our internal state structure.
//
// If the RT is not found in memory, the CCR is read to add the RT
// to the in memory RGM state. In this case the RGM state will be
// modified so the RGM state lock must be held by the caller while
// this function is called.
// errcode, if not NULL will be used to pass the error encountered
//
rgm_rt_t *
rtname_to_rt(const char *rtname, scha_err_t *errcode)
{
	rgm_rt_t	*rtp = NULL;
	scha_err_t	err = SCHA_ERR_NOERR;

	if (rtname == NULL) {
		err = SCHA_ERR_INVAL;
		goto ret; //lint !e801
	}

	if ((rtp = find_rt_in_memory(rtname)) != NULL) {
		goto ret; //lint !e801
	}
	ucmm_print("rtname_to_rt()", NOGET("couldn't find RT in memory, "
	    "reading from CCR <%s>\n"), rtname);

	err = rgm_add_rt((char *)rtname, &rtp).err_code;

ret:
	if (err != SCHA_ERR_NOERR)
		rtp = NULL;
	if (errcode != NULL)
		*errcode = err;
	return (rtp);
}


//
// rgname_to_rg
// Given the name of an RG as input, convert it into a pointer to
// the corresponding RG entry in our internal state structure.
// Returns NULL if the entry is not found.  The caller treats NULL
// as an error.
//
rglist_p_t
rgname_to_rg(const char *rgname)
{
	rglist_p_t rglp;

	if (rgname == NULL)
		return (NULL);

	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		if (strcmp(rglp->rgl_ccr->rg_name, rgname) == 0)
			break;
	}
	return (rglp);
}


//
// rname_to_r
//
// The caller of this function holds the state structure mutex.
//
// Given a pointer to an RG and a resource name as input, traverse the
// list of resources for that RG in our internal state structure and
// return a pointer to the resource entry matching the name.
// Returns NULL if the entry is not found.  The caller treats NULL
// as an error.
//
// If the first argument to the function is a NULL pointer, we will
// search the entire internal state structure for an RG containing
// this R.
//
rlist_p_t
rname_to_r(rglist_p_t rgp, const char *rname)
{
	rlist_p_t rp = NULL;
	rglist_p_t rg = NULL;

	if (rgp == NULL) {
		// We don't know the containing RG, so search the whole RG list
		for (rg = Rgm_state->rgm_rglist; rg != NULL;
		    rg = rg->rgl_next) {
			for (rp = rg->rgl_resources; rp != NULL;
			    rp = rp->rl_next) {
				if (strcmp(rp->rl_ccrdata->r_name, rname) == 0)
					goto brk; //lint !e801
			}
		}
	} else for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		// We do know the containing RG
		if (strcmp(rp->rl_ccrdata->r_name, rname) == 0)
			break;
	}
brk:
	return (rp);
}


//
// Convert RG state enum type to RG state string (for printing).
//
char *
pr_rgstate(rgm::rgm_rg_state val)
{
	uint_t i;

	for (i = 0; i < RG_ENTRIES; i++) {
		if (rgstatenames[i].rgm_val == val)
			return (rgstatenames[i].statename);
	}
	return (STATE_NOT_FOUND);
}


//
// get_rgstate
//
// input parameters :
// 	rgm::rgm_rg_state	the rg state enum defined in rgm.h
//	char *			the rg state string
//	scha_rgstate_t		the rg state enum defined in scha.h
// if the value of input parameter is '-1', this parameter will is ignored and
// the next input parameter will be checked.  If none of input parameter has
// valild value, return NULL.   The first valid input parameter is used
// to check if there is a match in the array rgstatenames[].
// If found, return the pointer to the array which contain the idl enum,
// string, and api enum of rg state.
//
rgstatename_t *
get_rgstate(rgm::rgm_rg_state rgm_val, char *statename,
		scha_rgstate_t api_val, rglist_p_t rgp,
		rgm::lni_t lni)
{
	uint_t		i;
	rgstatename_t	*state_tbl = NULL;
	rgstatename_t	*fault_tbl = NULL;

	for (i = 0; i < RG_ENTRIES; i++) {
		state_tbl = &rgstatenames[i];
		if (rgm_val != ((rgm::rgm_rg_state)-1) &&
		    state_tbl->rgm_val == rgm_val) {
			goto finished; //lint !e801
		} else if (statename != NULL &&
		    (strcmp(state_tbl->statename, statename) == 0)) {
			goto finished; //lint !e801
		} else if (api_val != (scha_rgstate_t)-1 &&
		    state_tbl->api_val == api_val) {
			goto finished; //lint !e801
		}
	}
	return (NULL);

finished:
	if ((state_tbl->api_val == SCHA_RGSTATE_ONLINE) &&
	    is_any_rs_faulted(rgp, lni)) {
		fault_tbl = &rgfaultstates;
		return (fault_tbl);
	} else {
		return (state_tbl);
	}
}


//
// Convert R state enum type to R state string (for printing).
//
char *
pr_rstate(rgm::rgm_r_state val)
{
	uint_t i;

	for (i = 0; i < R_ENTRIES; i++) {
		if (rstatenames[i].rgm_val == val)
			return (rstatenames[i].statename);
	}
	return (STATE_NOT_FOUND);
}


//
// get_rstate
//
// input parameters :
//	rgm::rgm_r_state	the r state enum defined in rgm.h
//	char *			the r state string
//	scha_rstate_t		the r state enum defined in scha.h
// if the value of input parameter is '-1' in rgm_val or api_val and NULL in
// statename, this parameter will is ignored and
// the next input parameter will be checked.  If none of input parameter has
// valild value, return NULL.   The first valid input parameter is used
// to check if there is a match in the array rstatenames[].
// If found, return the pointer to the array which contain the idl enum,
// string, and api enum of r state.
//
rstatename_t *
get_rstate(rgm::rgm_r_state rgm_val, char *statename, scha_rsstate_t api_val)
{
	uint_t		i;
	rstatename_t	*state_tbl = NULL;

	for (i = 0; i < R_ENTRIES; i++) {
		state_tbl = &rstatenames[i];
		if (rgm_val != (rgm::rgm_r_state)-1 &&
		    state_tbl->rgm_val == rgm_val)
			return (state_tbl);
		else if (statename != NULL &&
		    (strcmp(state_tbl->statename, statename) == 0))
			return (state_tbl);
		else if (api_val != (scha_rsstate_t)-1 &&
		    state_tbl->api_val == api_val)
			return (state_tbl);
	}
	return (NULL);
}


//
// Convert R FM status enum type to R FM status string (for printing).
//
char *
pr_rfmstatus(rgm::rgm_r_fm_status val)
{

	uint_t		i;

	for (i = 0; i < R_FM_ENTRIES; i++) {
		if (rfmstatusnames[i].rgm_val == val)
			return (rfmstatusnames[i].statusname);
	}
	return (FM_STATUS_NOT_FOUND);
}


//
// get_rfmstatus
//
// input parameters :
//	rgm::rgm_r_fm_status	the r fm status enum defined in rgm.h
//	char *			the r fm status string
//	scha_rsstatus_t		the r fm status enum defined in scha.h
// if the value of input parameter is '-1' in rgm_val or api_val and NULL in
// statusname, this parameter will is ignored and
// the next input parameter will be checked.  If none of input parameter has
// valild value, return NULL.   The first valid input parameter is used
// to check if there is a match in the array rfmstatusnames[].
// If found, return the pointer to the array which contain the idl enum,
// string, and api enum of r fm status.
//
rfmstatusname_t *
get_rfmstatus(rgm::rgm_r_fm_status rgm_val, char *statusname,
		scha_rsstatus_t	api_val)
{

	uint_t		i;
	rfmstatusname_t	*status_tbl = NULL;

	for (i = 0; i < R_FM_ENTRIES; i++) {
		status_tbl = &rfmstatusnames[i];
		if (rgm_val != (rgm::rgm_r_fm_status)-1 &&
		    status_tbl->rgm_val == rgm_val) {
			return (status_tbl);
		} else
		if (statusname != NULL &&
		    strcmp(status_tbl->statusname, statusname) == 0) {
			return (status_tbl);
		} else
		if (api_val != (scha_rsstatus_t)-1 &&
		    status_tbl->api_val == api_val) {
			return (status_tbl);
		}
	}
	return (NULL);
}


//
// reset_fm_status
// Reset the fault monitor status of a resource on the local node to newstatus.
// The status message is reset to "Starting", "Stopping", or NULL as indicated
// by the 'direction' argument.
//
// The fault monitor status and status string are set by monitors and methods
// using the API scha_resource_setstatus function.  When a resource is starting
// or stopping, the status is reset by calling this function.  The following
// scheme is used:
//
// When the resource is starting to be brought online, the state machine
// calls this function to reset the status to R_FM_UNKNOWN and the message
// to "Starting".
// When the resource in a PENDING_ONLINE RG reaches ONLINE_UNMON state, the
// Status is reset to ONLINE and the message to NULL, but *only* if the
// starting methods did not themselves set a status (as indicated by the
// current status still being UNKNOWN/"Starting").
//
// When the resource is starting to be brought offline, the state machine
// calls this function to reset the status to R_FM_UNKNOWN and the message
// to "Stopping".
// When the resource in a PENDING_OFFLINE RG reaches OFFLINE state, the
// Status is reset to OFFLINE and the message to NULL, but *only* if the
// stopping methods did not themselves set a status (as indicated by the
// current status still being UNKNOWN/"Stopping").
//
// If this function is called on a node that is not the president, it
// must notify the president of the change in status via an idl call.
//
// If the node is the president, we don't set the new status directly,
// but instead queue up a status change on the status change threadpool
// for later processing.
//
// In neither case do we log a message or generate an event here, because
// that will be done by the president when it actually processes the
// status change.
//
void
reset_fm_status(rgm::lni_t lni, rlist_p_t r,
    rgm::rgm_r_fm_status newstatus, rgm_direction_t direction)
{
	char *message;
#ifdef EUROPA_FARM
	rgmx_rs_set_status_args arg;
	rgmx_set_status_result_t result_v = {0};
	CLIENT *clnt = NULL;
#else
	rgm::idl_rs_set_status_args arg;
	rgm::idl_set_status_result_t_var result_v;
	rgm::rgm_comm_var prgm_pres_v;
	Environment e;
#endif
	rfmstatusname_t *status_tbl;
	char strbuf[BUFSIZ];
#ifndef EUROPA_FARM
	notify_status_change_task *notify_task = NULL;
#endif

	ucmm_print("RGM", NOGET("reset_fm_status(): setting status to "
	    "<%s> for resource <%s>\n"), pr_rfmstatus(newstatus),
	    r->rl_ccrdata->r_name);

	switch (direction) {
	case RGM_DIR_STARTING:
		message = strdup(RGM_MSG_STARTING);
		break;
	case RGM_DIR_STOPPING:
		message = strdup(RGM_MSG_STOPPING);
		break;
	case RGM_DIR_NULL:
		message = NULL;
		break;
	default:
		// Coding error
		CL_PANIC(0);
		// NOTREACHED
	}

	//
	// If we are the president, we queue a status change task.
	// Otherwise, we set the state directly.
	//
	if (am_i_president()) {
#ifdef EUROPA_FARM
		ASSERT(0);
#else
		//
		// Queue a status change task.
		//
		notify_task = new notify_status_change_task(
		    lni, r->rl_ccrdata->r_name, r->rl_rg->rgl_ccr->rg_name,
		    newstatus, message);

		notify_status_change_threadpool::the().defer_processing(
		    notify_task);
		// notify_task will be deleted by the thread that processes it.
#endif

	} else {
		//
		// Reset the fault monitor status string. Free any pre-existing
		// string first.
		//
		r->rl_xstate[lni].rx_fm_status = newstatus;
		if (r->rl_xstate[lni].rx_fm_status_msg != NULL) {
			free(r->rl_xstate[lni].rx_fm_status_msg);
		}
		r->rl_xstate[lni].rx_fm_status_msg = message;
	}

	//
	// If we are not the president, we must notify the president of the
	// change via an idl call.
	// The president will post an event when it processes the change,
	// so we don't need to post the event here on the slave also.
	//
	if (!am_i_president()) {
		//
		// Notify the president.
		//
		ucmm_print("RGM", NOGET("reset_fm_status(): notifying pres of "
		    "new status <%s> for resource <%s>\n"),
		    pr_rfmstatus(newstatus), r->rl_ccrdata->r_name);
		arg.idlstr_rs_name = new_str(r->rl_ccrdata->r_name);
		arg.idlstr_rg_name = new_str(r->rl_rg->rgl_ccr->rg_name);
		arg.idlstr_node_name =
		    new_str((lnManager->findObject(lni))->getFullName());

		//
		// We need to obtain the equivalent api val for this
		// status and convert it to a string to ship to the
		// president in the idl call.
		//
		if ((status_tbl = get_rfmstatus(newstatus, NULL,
		    (scha_rsstatus_t)-1)) == NULL) {
			ucmm_print("RGM",
			    NOGET("reset_fm_status(): Invalid FM status "
				"<%d>\n"), newstatus);
			return;
		}

		(void) sprintf(strbuf, "%d", status_tbl->api_val);
		arg.idlstr_status = new_str(strbuf);

		if (message == NULL) {
			arg.idlstr_status_msg = (char *)NULL;
		} else {
			arg.idlstr_status_msg = new_str(message);
		}

#ifdef EUROPA_FARM
		clnt = rgmx_comm_getpres(ZONE);
		if (clnt == NULL) {
#else
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
		if (CORBA::is_nil(prgm_pres_v)) {
#endif
			//
			// Just give up.  The new president will fetch
			// all the state, and so will get this status msg.
			//
			ucmm_print("RGM",
			    NOGET("reset_fm_status(): pres ref is nil "));
			return;
		}

#ifdef EUROPA_FARM
		if (rgmx_scha_rs_set_status_1(&arg, &result_v, clnt)
			!= RPC_SUCCESS) {
			//
			// We don't take any action on failure, but figure out
			// if it failed so we can log an appropriate debug msg.
			// If the president has died, the new president will
			// fetch state from us, which will give it the updated
			// status.

			// An unexpected RPC error.  Just give up.
			//
			ucmm_print("RGM",
			    NOGET("reset_fm_status(): RPC error"));
		} else if (result_v.ret_code != SCHA_ERR_NOERR) {
			//
			// The call on the pres failed.	 Just give up.
			//
			ucmm_print("RGM",
			    NOGET("reset_fm_status(): RPC call "
			    "failed with error: %d"),
			    result_v.ret_code);
		} else {
			ucmm_print("RGM",
			    NOGET("reset_fm_status(): ORB call succeeded"));
		}
#else
		//
		// Make the actual idl call.
		//
		prgm_pres_v->idl_scha_rs_set_status(arg, result_v, e);

		//
		// We don't take any action on failure, but figure out if
		// it failed so we can log an appropriate debug msg.
		// If the president has died, the new president will
		// fetch state from us, which will give it the updated
		// status.
		//
		if (e.exception() != NULL) {
			//
			// An unexpected ORB error.  Just give up.
			//
			ucmm_print("RGM",
			    NOGET("reset_fm_status(): ORB exception"));
			e.clear();
		} else if (result_v->ret_code != SCHA_ERR_NOERR) {
			//
			// The call on the pres failed.  Just give up.
			//
			ucmm_print("RGM",
			    NOGET("reset_fm_status(): ORB call "
				"failed with error: %d"),
			    result_v->ret_code);
		} else {
			ucmm_print("RGM",
			    NOGET("reset_fm_status(): ORB call succeeded"));
		}
#endif
	} else {
		ucmm_print("RGM", NOGET("reset_fm_status(): did not notify "
		    "pres"));
	}
#ifdef EUROPA_FARM
	delete [] arg.idlstr_rs_name;
	delete [] arg.idlstr_rg_name;
	delete [] arg.idlstr_node_name;
	delete [] arg.idlstr_status;
	if (arg.idlstr_status_msg != (char *)NULL)
		delete [] arg.idlstr_status_msg;
	xdr_free((xdrproc_t)xdr_rgmx_set_status_result_t, (char *)&result_v);
	rgmx_comm_freecnt(clnt);
#endif
}


//
// is_fm_status_unchanged
//
// When a resource is starting to be brought online, the state machine
// calls reset_fm_status() to reset the status to R_FM_UNKNOWN and the message
// to "Starting".
// When a resource is starting to be brought offline, the state machine
// calls reset_fm_status() to reset the status to R_FM_UNKNOWN and the message
// to "Stopping".
//
// This function is called only with direction=RGM_DIR_STARTING or
// RGM_DIR_STOPPING.  It tests whether the resource status still has the same
// value of R_FM_UNKNOWN and the message is still set to "Starting" or
// "Stopping" respectively.  If so it returns B_TRUE; otherwise B_FALSE.
//
// If this function returns B_FALSE, that implies that a starting or stopping
// method has changed the status so it should be left intact by the rgmd.
//
boolean_t
is_fm_status_unchanged(sol::nodeid_t nodeid, rlist_p_t r,
    rgm_direction_t direction)
{
	char *message = NULL;

	// First check if status was changed from UNKNOWN
	if (r->rl_xstate[nodeid].rx_fm_status != rgm::R_FM_UNKNOWN) {
		return (B_FALSE);
	}

	// Check if status message still corresponds to its previous setting
	switch (direction) {
	case RGM_DIR_STARTING:
		message = RGM_MSG_STARTING;
		break;
	case RGM_DIR_STOPPING:
		message = RGM_MSG_STOPPING;
		break;
	case RGM_DIR_NULL:
		// We are not currently using this case-- coding error.
		// FALLTHRU
	default:
		// Coding error
		CL_PANIC(0);
		// NOTREACHED
	}
	if (strcmp(r->rl_xstate[nodeid].rx_fm_status_msg, message) == 0) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}

//
// Convert rdeplist_t to a string and each value is separated by ','.
// Free memory is needed.
//
char *
rdeplist_to_str(rdeplist_t *l)
{
	char		*ptr;
	rdeplist_t	*curr;
	uint_t		len = 0, size = 0;
	boolean_t	need_free = B_FALSE;
	if (l == NULL) {
		return (strdup(""));
	}

	// first calculate the length of the total string
	for (curr = l; curr != NULL; curr = curr->rl_next) {
		if (curr->rl_name != NULL) {
			//
			// get the length of this string plus 1 for ','
			// or \0
			//
			size += (uint_t)strlen(curr->rl_name) + 1;
			len++;	// count the total number of elements
		}
	}

	// allocate space for ptr
	ptr = (char *)malloc(size);
	CL_PANIC(ptr != NULL);

	ptr[0] = '\0';
	// fill the data into ptr
	for (curr = l; curr != NULL; curr = curr->rl_next, len--) {
		if (curr->rl_name != NULL) {
			(void) strcat(ptr, curr->rl_name);
			if (len > 1) {
				// no trailing ',' for the last element
				(void) strcat(ptr, ",");
			}
		}
	}

	return (ptr);
}

//
// Convert rdeplist_t along with the qualifiers to a string and each value is
// separated by ','. String will be of the form <rs_name>{qual},.... Free
// memory is needed.
//
char *
rdeplistQ_to_str(rdeplist_t *l)
{
	char		*ptr = NULL;
	rdeplist_t	*curr = NULL;
	uint_t		len = 0, size = 0;
	if (l == NULL) {
		return (strdup(""));
	}

	// first calculate the length of the total string
	for (curr = l; curr != NULL; curr = curr->rl_next) {
		if (curr->rl_name != NULL) {
			//
			// get the length of the string plus 1 for ',' or \0
			// If the locality type is LOCAL_NODE or ANY_NODE, then
			// add the length of the locality_type plus 2 for '{'
			// and '}'
			//
			if (curr->locality_type == LOCAL_NODE)
				size += (uint_t)strlen(curr->rl_name) +
				    strlen(SCHA_LOCAL_NODE) + 3;
			else if (curr->locality_type == ANY_NODE)
				size += (uint_t)strlen(curr->rl_name) +
				    strlen(SCHA_ANY_NODE) + 3;
			else
				size += (uint_t)strlen(curr->rl_name) + 1;
			len++;	// count the total number of elements
		}
	}

	// allocate space for ptr
	ptr = (char *)malloc(size);
	ptr[0] = '\0';

	// fill the data into ptr
	for (curr = l; curr != NULL; curr = curr->rl_next, len--) {
		if (curr->rl_name != NULL) {
			(void) strcat(ptr, curr->rl_name);
			if (curr->locality_type != FROM_RG_AFFINITIES) {
				(void) sprintf(ptr + strlen(ptr), "{%s}",
				    (curr->locality_type == LOCAL_NODE) ?
				    SCHA_LOCAL_NODE : SCHA_ANY_NODE);
			}
			if (len > 1) {
				// no trailing ',' for the last element
				(void) strcat(ptr, ",");
			}
		}
	}
	return (ptr);
}

//
// Convert namelist_t to a string and each value is separated by ','
// Free memory is needed.
//
char *
namelist_to_str(namelist_t *l)
{
	char		*ptr;
	namelist_t	*curr;
	uint_t		len = 0, size = 0;

	if (l == NULL) {
		return (strdup(""));
	}

	// first calculate the length of the total string
	for (curr = l; curr != NULL; curr = curr->nl_next) {
		if (curr->nl_name != NULL) {
			//
			// get the length of this string plus 1 for ','
			// or \0
			//
			size += (uint_t)strlen(curr->nl_name) + 1;
			len++;	// count the total number of elements
		}
	}

	// allocate space for ptr
	ptr = (char *)malloc(size);
	CL_PANIC(ptr != NULL);

	ptr[0] = '\0';
	// fill the data into ptr
	for (curr = l; curr != NULL; curr = curr->nl_next, len--) {
		if (curr->nl_name != NULL) {
			(void) strcat(ptr, curr->nl_name);
			if (len > 1) {
				// no trailing ',' for the last element
				(void) strcat(ptr, ",");
			}
		}
	}

	return (ptr);
}

// Convert namelist_all_t to the designated "ALL" value, or regular
// list with each value separated by ','
// Free memory is needed.
//
static char *
namelist_all_to_str(namelist_all_t *l)
{
	char *ptr = (l->is_ALL_value == B_TRUE) ?
	    strdup(SCHA_ALL_SPECIAL_VALUE) :
	    namelist_to_str(l->names);
	return (ptr);
}


//
// strseq_to_str
//
// Convert a sequence of names to a comma-separated string of names.
// The caller is responsible for freeing memory allocated by this routine.
//
char *
strseq_to_str(const rgm::idl_string_seq_t &nodeseq)
{
	char *ptr;
	uint_t i, len;
	uint_t size = 0;

	len = nodeseq.length();
	if (len == 0) {
		ptr = strdup("");
		return (ptr);
	}

	// compute the size of the resulting string
	for (i = 0; i < len; i++)
		size += (uint_t)strlen(nodeseq[i]) + 1;

	if ((ptr = (char *)malloc_nocheck(size)) == NULL)
		return (NULL);

	ptr[0] = '\0';
	for (i = 0; i < len; i++) {
		(void) strcat(ptr, nodeseq[i]);
		if (i < len - 1)
			(void) strcat(ptr, ",");
	}

	return (ptr);
}
/*
 * Called from idl_scrgadm_rs_add function to update the per-node
 * extension property values. The validate call back would have an additional
 * flag of -X that carries the all-nodes, per-node extenion properties.
 */
void update_all_nodes_val_add(rgm_resource_t *rsrc,
    rgm::idl_string_seq_t &all_nodes_ext)
{
	rgm_property_list_t *p = NULL;
	rgm_property_t *ptr = NULL;
	int  index = 0;
	rglist_p_t rgl;
	LogicalNodeset *rg_ns = new LogicalNodeset;

	// Fill in the sequence
	rgl = rgname_to_rg(rsrc->r_rgname);
	rg_ns = get_logical_nodeset_from_nodelist(rgl->rgl_ccr->rg_nodelist);

	for (p = rsrc->r_ext_properties; p != 0; p = p->rpl_next) {
		ptr = p->rpl_property;
		if (ptr->rp_type == SCHA_PTYPE_STRINGARRAY) {
			continue;
		}
		if (ptr->is_per_node == B_TRUE) {
			update_all_nodes_value(((rgm_value_t *)
			    ptr->rp_value)->rp_value,
			    ptr->rp_key, all_nodes_ext, &index, rg_ns);
		}

	}
}

//
// Search for string in namelist
//
boolean_t
namelist_contains(
	const namelist_t	*nl,
	const char		*what)
{
	if (what != NULL) {
		while (nl != NULL) {
			if (strcmp(what, nl->nl_name) == 0) {
				return (B_TRUE);
			}
			nl = nl->nl_next;
		}
	}

	return (B_FALSE);
}

// Utility routines for the rdeplist_t data structure

//
// Search for string in rdeplist
//
boolean_t
rdeplist_contains(
	const rdeplist_t	*nl,
	const char		*what)
{
	if (what != NULL) {
		while (nl != NULL) {
			if (strcmp(what, nl->rl_name) == 0) {
				return (B_TRUE);
			}
			nl = nl->rl_next;
		}
	}

	return (B_FALSE);
}


//
// Checks to see if the given name exists in the curr_name_list
// The second argument is either rg name or resource name.
// Since it shouldn't be NULL or empty string, we just return false.
// Note: in some cases, a NULL could be passed as the second argument.
// It is by scrgadm.  When scrgadm sees an empty property value is specified,
// it passes one element of value list holding an empty string.
// Therefore we can't do CL_PANIC(name != NULL) here.
//
boolean_t
rdeplist_exists(rdeplist_t *curr_name_list, name_t name)
{
	rdeplist_t *curr_node = curr_name_list;
	boolean_t found = B_FALSE;

	if (name == NULL || *name == '\0')
		return (B_FALSE);

	while (curr_node != NULL) {
		if (strcmp(curr_node->rl_name, name) == 0) {
			found = B_TRUE;
			break;
		}
		curr_node = curr_node->rl_next;
	}
	return (found);
}

//
// rdeplist_compare
//
// Compare two rdeplists. The rdeplists "match" if the all following are true:
//  1. they contain the same number of nodes
//  2. for all i: 0 <= i <= (length - 1), the rl_name fields of the "ith" node
//	match (using strcmp).
//
//  B_TRUE is returned if the rdeplists match, B_FALSE is returned otherwise.
//
//  Note that if the two lists contain the same names but in different orders,
//  then B_FALSE will be returned.
//

boolean_t
rdeplist_compare(rdeplist_t *rdeplist_1, rdeplist_t *rdeplist_2)
{
	rdeplist_t *nl1, *nl2;

	nl1 = rdeplist_1;
	nl2 = rdeplist_2;

	while (nl1 != NULL && nl2 != NULL) {
		// check for NULL nl_names
		if (nl1->rl_name == NULL || nl2->rl_name == NULL)
			if (nl1->rl_name != nl2->rl_name)
				return (B_FALSE);
		if ((strcmp(nl1->rl_name, nl2->rl_name) != 0) ||
		    (nl1->locality_type != nl2->locality_type)) {
			return (B_FALSE);
		}
		nl1 = nl1->rl_next;
		nl2 = nl2->rl_next;
	}
	if (nl1 == NULL && nl2 == NULL) {
		return (B_TRUE);
	}
	return (B_FALSE);
}


//
// Finds and deletes the node containing the given name in curr_rdeplist.
// It takes care to reassign curr_rdeplist in case the first node is
// being deleted.
// The delete of the node also deletes the name within it. This is OK
// assuming that that node in the rdeplist was added using rdeplist_add_name
// which makes local copy of the name being passed.
//
void
rdeplist_delete_name(rdeplist_t **curr_rdeplist, const name_t name)
{
	rdeplist_t *curr_node = *curr_rdeplist;
	rdeplist_t *prev_node = NULL;

	while (curr_node) {
		if (strcmp(curr_node->rl_name, name) == 0) {
			if (curr_node == *curr_rdeplist) {
				// first node in the list
				*curr_rdeplist = (*curr_rdeplist)->rl_next;
			} else {
				// adjust the pointers to skip over the
				// node being deleted
				prev_node->rl_next = curr_node->rl_next;
			}
			free(curr_node->rl_name);
			free(curr_node);
			break;
		}
		prev_node = curr_node;
		curr_node = curr_node->rl_next;
	}
}


//
// Adds the given name to the end of the curr_rdeplist by converting it
// into a rdeplist_t structure.
// It makes local copy of the name passed in.
// This routine does not check for duplicates. If the duplicate check is
// needed, it can be done using the previous routine -- namelist_exists
//
scha_errmsg_t
rdeplist_add_name(rdeplist_t **curr_rdeplist, name_t name,
    rdep_locality_t dep_type)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	rdeplist_t *new_node = NULL;
	if ((new_node = (rdeplist_t *)
		malloc_nocheck(sizeof (rdeplist_t))) == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	if ((new_node->rl_name = strdup_nocheck(name)) == NULL) {
		free(new_node);
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	new_node->locality_type = dep_type;

	new_node->rl_next = NULL;

	if (*curr_rdeplist == NULL)
		// first element in the list hence set curr_namelist
		*curr_rdeplist = new_node;
	else {
		rdeplist_t *curr_node = *curr_rdeplist;
		// append the new node to the end of the list
		while (curr_node->rl_next != NULL)
			curr_node = curr_node->rl_next;
		curr_node->rl_next = new_node;
	}
	res.err_code = SCHA_ERR_NOERR;
	return (res);
}


//
// Searches for the logical node name 'what' in the nodeidlist.
// Returns B_TRUE if the LNI corresponding to the name is found in the
// list, B_FALSE otherwise.
//
boolean_t
nodeidlist_contains(const nodeidlist_t *nl, const char *what)
{
	rgm::lni_t lni;

	if (what == NULL)
		return (B_FALSE);
	lni = rgm_get_lni(what);
	if (lni == LNI_UNDEF)
		return (B_FALSE);
	while (nl != NULL) {
		if (lni == nl->nl_lni)
			return (B_TRUE);
		nl = nl->nl_next;
	}
	return (B_FALSE);
}


//
// May be used on president or on slave (for local zones defined on
// the slave).  Panic the node if LNI should be defined, but isn't.
//
boolean_t
nodeidlist_contains_lni(const nodeidlist_t *nl, rgm::lni_t lni)
{
	while (nl != NULL) {
		if (nl->nl_lni == LNI_UNDEF) {
			//
			// The only case in which the nl_lni field should be
			// uninitialized is if the local node is not the
			// president, and lni is a nonglobal zone not hosted
			// on the local node,
			//

			sol::nodeid_t nid = get_local_nodeid();

			// All LNIs (physical+logical) that are not
			// on this farm node are undefined (LNI_UNDEF).
#ifndef EUROPA_FARM
			CL_PANIC(nid != Rgm_state->rgm_President &&
			    nl->nl_nodeid != nid && nl->nl_zonename != NULL);
#endif
		} else if (nl->nl_lni == lni) {
			return (B_TRUE);
		}
		nl = nl->nl_next;
	}
	return (B_FALSE);
}


//
// the function transfers the error message from temp_res to res
// and frees the error message in temp_res. If the operation succeeded
// res.err_code is unchanged, otherwise res.err_code is modified
// return value depends on success or failure.
//
scha_err_t
transfer_message(scha_errmsg_t *res, scha_errmsg_t *temp_res)
{
	scha_err_t old_err = res->err_code;

	if (temp_res->err_msg == NULL) {
		return (SCHA_ERR_NOERR);
	}
	res->err_code = SCHA_ERR_NOERR;
	rgm_sprintf(res, "%s", temp_res->err_msg);
	free(temp_res->err_msg);
	temp_res->err_msg = NULL;
	if (res->err_code == SCHA_ERR_NOERR) {
		res->err_code = old_err;
		return (SCHA_ERR_NOERR);
	}
	return (res->err_code);
}

//
// Convert a list of names [namelist_t]
// to a sequence of names [rgm::idl_string_seq_t]
//
void
namelist_to_seq(namelist_t *inlist, rgm::idl_string_seq_t &outlist)
{
	namelist_t *p;
	uint_t i = 0;

	// Compute the length of the sequence
	for (p = inlist; p != NULL; p = p->nl_next) {
		i++;
	}

	// Set the length of the sequence
	outlist.length(i);

	// Fill in the sequence
	i = 0;
	for (p = inlist; p != NULL; p = p->nl_next) {
		outlist[i] = new_str((const char *)p->nl_name);
		i++;
	}
}


//
// Traverse a sequence of name value pairs to get the value of
// a property with a given name. If the value is indeed found to
// contain a non-NULL value, new memory is allocated and returned
// to the caller. It is caller's headache to manage it.
//
// If the property is not found, return SCHA_ERR_PROP error.
//
// This function is for retrieve the property value which is INT, BOOLEAN,
// ENUM and STRING type.  These type of property actually have just one value.
// If the property has more than one value or the value is NULL,
// return SCHA_ERR_INVAL.
//
// Note2:
//	If the property(type is STRINGARRAY or UNITARRAY) contain a list
//	of values, call the get_value_list() utility function instead of this
//	routine.
//
// This function calls the helper function convert_prop_value to actually
// do the checks on the value and copy it to new memory.
//
scha_errmsg_t
get_prop_value(const char *property, const rgm::idl_nameval_seq_t &nv_seq,
    char **val, scha_prop_type_t type)
{
	uint_t		i, len;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	*val = NULL;
	len = nv_seq.length();
	ucmm_print("RGM_get_prop_value()", NOGET("name <%s>, len <%d>\n"),
	    property, len);

	for (i = 0; i < len; i++) {
		if (strcasecmp(property, nv_seq[i].idlstr_nv_name) == 0) {
			return (convert_prop_value(property, nv_seq[i],
			    val, type));
		}
	}

	ucmm_print("RGM_get_prop_value()", NOGET("<%s>: Not found\n"),
	    property);
	res.err_code = SCHA_ERR_PROP;
	return (res);
}

//
// Traverse a sequence of name-value-nodes sequence to get the value of
// a property with a given name. If the value is indeed found to
// contain a non-NULL value, new memory is allocated and returned
// to the caller. The caller must free the returned value.
//
// If the property is not found, return SCHA_ERR_PROP error.
//
// This function is for retrieve the property value which is INT, BOOLEAN,
// ENUM and STRING type.  These type of property actually have just one value.
// If the property has more than one value or the value is NULL,
// return SCHA_ERR_INVAL.
//
// This function calls the helper function convert_prop_value_ext to actually
// do the checks on the value and copy it to new memory.
//
// e_ns is a set of nodes on which the resource is enabled. e_ns
// is used to check if the property update is performed on a node
// on which the resource is enabled and the TUNABLE flag is WHEN_DISABLED.
// type argument is the input property datatype. STRING datatype can
// contain comma as part of the value. type argument is used to validate
// the same.
//
scha_errmsg_t
get_prop_value_ext(const char *property, const rgm::idl_nameval_node_seq_t
	&nv_seq, std::map<rgm::lni_t, char *> &val, rgm_property_list_t *ptr,
	LogicalNodeset *ns, LogicalNodeset e_ns, scha_prop_type_t type,
	rgm_tune_t tuneable)
{
	int		i, len;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	boolean_t	is_first = B_TRUE;

	len = nv_seq.length();
	ucmm_print("RGM_get_prop_value()", NOGET("name <%s>, len <%d>\n"),
	    property, len);


	for (i = len-1; i >= 0; i--) {
		if (strcasecmp(property, nv_seq[i].idlstr_nv_name) == 0) {
			res = convert_prop_value_ext(property, nv_seq[i],
			    val, ptr, is_first, ns, e_ns, type, tuneable);
			if (!nv_seq[i].is_per_node) {
				return (res);
			}
			is_first = B_FALSE;
			if (res.err_code == SCHA_ERR_CHECKS) {
				rgm_format_errmsg(&res,
				    REM_PROP_TUNE_WHEN_DISABLED, property);
			}
			if (res.err_code != SCHA_ERR_NOERR)
				return (res);
		}
	}
	if (is_first) {
		ucmm_print("RGM_get_prop_value()", NOGET("<%s>: Not found\n"),
		    property);
		res.err_code = SCHA_ERR_PROP;
		return (res);
	}
	return (res);
}

//
// convert_prop_value
//
// Converts the value in nv.nv_val to a string.
// Does error checking to ensure that there is exactly one value.
//
// Returns a scha_errmsg_t to express whether the operation succeeded or
// failed, and why it failed (if it failed).
//
// If no error code is returned, *val is guarenteed to be non-NULL.
//
scha_errmsg_t
convert_prop_value(const char *property, const rgm::idl_nameval &nv,
    char **val, scha_prop_type_t type)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	uint_t len;


	//
	// Check to see if the property has more than one
	// value.  If it does, return an error because there should be
	// only one value for INT, BOOLEAN and ENUM
	// type of property.
	//
	len = nv.nv_val.length();
	if ((type != SCHA_PTYPE_STRING) && (len > 1)) {
		ucmm_print("RGM_convert_prop_value()", NOGET(
		    "name <%s> : property has more than one "
		    " value\n"), property);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES, property);
		return (res);
	}
	if (type == SCHA_PTYPE_STRING)
		*val = strseq_to_str(nv.nv_val);
	else
		*val = strdup_nocheck(nv.nv_val[0]);
	if (*val == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		return (res);
	}
	ucmm_print("RGM_convert_prop_value()",
	    NOGET("name <%s>, val <%s>\n"), property, *val);
	return (res);
}
//
// Copy the all nodes extension property of the resource
// into t. This is done to incorporate the incremental
// updates of the resource extension property.
//

static scha_errmsg_t
copy_all_nodes_ext_prop(std::map<rgm::lni_t, char *> p,
    std::map<rgm::lni_t, char *> &t,
    LogicalNodeset *ns)
{
	uint_t i;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		if (!ns->containLni(it->second))
			continue;
		if (p[it->second])
			t[it->second] = strdup_nocheck(p[it->second]);
	}
	return (res);
}

//
// convert_prop_value_ext
//
// Converts the value in nv.nv_val to a string corresponding the nodes on
// which the value is updated.
// Does error checking to ensure that there is exactly one value.
//
// Returns a scha_errmsg_t to express whether the operation succeeded or
// failed, and why it failed (if it failed).
//
// If no error code is returned, *val is guarenteed to be non-NULL.
//
scha_errmsg_t
convert_prop_value_ext(const char *property, const rgm::idl_nameval_node &nv,
    std::map<rgm::lni_t, char *> &val, rgm_property_list_t *ptr,
    boolean_t is_first, LogicalNodeset *ns, LogicalNodeset e_ns,
    scha_prop_type_t type, rgm_tune_t tuneable)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	uint_t len, i;
	char *name = NULL;
	rgm::lni_t lni;
	char *value = NULL;

	if (nv.nv_val[0] == (const char *) NULL) {
		ucmm_print("RGM_convert_prop_value()", NOGET("name <%s> : "
		    "property value = NULL\n"), property);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES, property);
		return (res);
	}

	//
	// Check to see if the property has more than one
	// value.  If it does, return an error because there should be
	// only one value for INT, BOOLEAN, ENUM and STRING
	// type of property.
	//
	len = nv.nv_val.length();
	if ((type != SCHA_PTYPE_STRING) && (len > 1)) {
		ucmm_print("RGM_convert_prop_value()", NOGET(
		    "name <%s> : property has more than one "
		    " value\n"), property);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES, property);
		return (res);
	}
	if (type == SCHA_PTYPE_STRING)
		value = strseq_to_str(nv.nv_val);
	else
		value = strdup_nocheck(nv.nv_val[0]);

	if (nv.is_per_node) {
		if (is_first) {
			res = copy_all_nodes_ext_prop(((rgm_value_t *)
			    ptr->rpl_property
			    ->rp_value)->rp_value, val, ns);
			ptr->rpl_property->is_per_node = B_TRUE;
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}
		}
		len = nv.nv_nodes.length();
		if (len == 0) {
			//
			// Per node extension property being updated globably.
			//
			for (LogicalNodeManager::iter it = lnManager->begin();
			    it != lnManager->end(); ++it) {
				LogicalNode *ln = lnManager->
				    findObject(it->second);
				if (!ns->containLni(it->second))
					continue;
				if (e_ns.containLni(it->second) &&
				    tuneable == TUNE_WHEN_DISABLED) {
					ucmm_print("RGM", NOGET(
					    "check_tunable_flag: "
					    "Cannot update property "
					    "<%s> on node <%s> when tunable "
					    "flag is WHEN_DISABLED\n"),
					    property, ln->getFullName());
					res.err_code = SCHA_ERR_CHECKS;
					continue;
				}
				if (val[it->second]) {
					free(val[it->second]);
					val[it->second] = NULL;
				}
				val[it->second] = strdup_nocheck(value);
			}
		} else {
			for (i = 0; i < len; i++) {
				name = strdup_nocheck(nv.nv_nodes[i]);
				if ((lni = rgm_get_lni(name)) ==
				    LNI_UNDEF) {
					// no such node
					res.err_code = SCHA_ERR_NODE;
					free(name);
					return (res);
				} else {
					if (!ns->containLni(lni)) {
						// Node not part of RG nodelist
						res.err_code = SCHA_ERR_NODE;
						free(name);
						return (res);
					}
					// Incremental update.
					if (e_ns.containLni(lni) &&
					    tuneable == TUNE_WHEN_DISABLED) {
						LogicalNode *ln =
						    lnManager->findObject(lni);
						ucmm_print("RGM", NOGET(
						    "check_tunable_flag: "
						    "Cannot update property "
						    "<%s> on node <%s> when"
						    " tunable flag is "
						    "WHEN_DISABLED\n"),
						    property,
						    ln->getFullName());
						res.err_code = SCHA_ERR_CHECKS;
						continue;
					}
					val[lni] = strdup_nocheck(value);
					ucmm_print(
					    "RGM_convert_prop_value_ext()",
					    NOGET("name <%s>, val <%s>\n"),
					    property, val[lni]);
				}
			}
		}
	} else {
		val[0] = strdup_nocheck(value);
		ucmm_print("RGM_convert_prop_value()",
		    NOGET("name <%s>, val <%s>\n"), property, val[0]);
	}
	free(name);
	free(value);
	return (res);
}
#ifndef EUROPA_FARM

//
// get_bool_value(): Updates the passed in boolean pointer with the value
// looked up in the passed in sequence. Returns SCHA_ error if the property
// value contains an invalid data - not true or false, or has more than
// one value
//
scha_errmsg_t
get_bool_value(const char *property, const rgm::idl_nameval_seq_t &nv_seq,
    boolean_t *retval)
{
	char 	*val;
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};

	status = get_prop_value(property, nv_seq, &val, SCHA_PTYPE_BOOLEAN);
	if (status.err_code != SCHA_ERR_NOERR)
		return (status);

	if (strcasecmp(val, "True") == 0) {
		free(val);
		*retval = B_TRUE;
		return (status);
	}
	if (strcasecmp(val, "False") == 0) {
		free(val);
		*retval = B_FALSE;
		return (status);
	}
	free(val);	// Was newly allocated in get_prop_val()

	status.err_code = SCHA_ERR_INVAL;
	rgm_format_errmsg(&status, REM_INVALID_PROP_VALUES, property);

	return (status);
}


//
// get_int_value(): Same semantics as get_bool_value() but works
// with integers.
//
scha_errmsg_t
get_int_value(const char *property,
	const rgm::idl_nameval_seq_t &nv_seq, int *retval)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	long		rc;
	char 	*val;

	res = get_prop_value(property, nv_seq, &val, SCHA_PTYPE_INT);
	if (res.err_code != SCHA_ERR_NOERR) {
		goto cleanup; //lint !e801
	}

	// if the value is blank, return error.
	if ((*val == '\0') || (!is_an_int_value(val))) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
		    property);
		goto cleanup; //lint !e801
	}

	rc = strtol(val, NULL, 10);
	if (errno == ERANGE || rc > INT32_MAX) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_VALUE_OUT_OF_RANGE,
		    property, INT32_MAX);
		goto cleanup; //lint !e801
	}

	*retval = rc;

cleanup:
	free(val);
	return (res);
}
#endif // !EUROPA_FARM


//
// Traverse a sequence of name-value pair and return a reference to the
// value-list for the specified name. A convenience function for extracting
// the value-list.
//
// Note: Everything is a reference here, thus no new memory is being
// allocated/de-allocated here!! This works, because in order to create
// a sequence, we FIRST need to know its length and thus the memory MUST
// be allocated there.
//
const rgm::idl_string_seq_t &
get_value_list(const char *property, const rgm::idl_nameval_seq_t &nv_seq)
{
	uint_t	i, len;
	static	rgm::idl_string_seq_t nullseq(0, 0);

	len = nv_seq.length();
	ucmm_print("RGM_get_value_list()", NOGET("name %s, len %d\n"),
	    property, len);
	for (i = 0; i < len; i++) {
		if (strcasecmp(property, nv_seq[i].idlstr_nv_name) == 0) {
			return (nv_seq[i].nv_val);
		}
	}
	return (nullseq);
}

//
// Traverse a sequence of name-value-node pair and return a reference to the
// value-list for the specified name. A convenience function for extracting
// the value-list.
//
// Note: Everything is a reference here, thus no new memory is being
// allocated/de-allocated here!! This works, because in order to create
// a sequence, we FIRST need to know its length and thus the memory MUST
// be allocated there.
//
const rgm::idl_string_seq_t &
get_value_list_ext(const char *property,
    const rgm::idl_nameval_node_seq_t &nv_seq)
{
	uint_t	i, len;
	static	rgm::idl_string_seq_t nullseq(0, 0);

	len = nv_seq.length();
	ucmm_print("RGM_get_value_list()", NOGET("name %s, len %d\n"),
	    property, len);
	for (i = 0; i < len; i++) {
		if (strcasecmp(property, nv_seq[i].idlstr_nv_name) == 0) {
			return (nv_seq[i].nv_val);
		}
	}
	return (nullseq);
}

//
// Convert a given name, which happens to have a list of string values, into
// a linked list of names, suitable to be passed to lower level APIs
// which work with CCR. Memory is allocated by this function. The caller
// should make sure that it gets freed properly.
//
//	Typical usage of this function is to convert a sequence (used in IDL)
// to a namelist array (used in the lower level APIs).
//
// This function is just a wrapper for two helper functions:
//
// get_value_list searches the nv_seq for the nameval_t with key that
// matches property.
//
// convert_value_string_array does the real work of actually converting the
// nameval_t's value (an idl_string_seq_t) to a namelist_t.
//
namelist_t *
get_value_string_array(const char *property,
	const rgm::idl_nameval_seq_t &nv_seq)
{
	const rgm::idl_string_seq_t & valseq =
	    get_value_list(property, nv_seq);
	return (convert_value_string_array(valseq));
}

//
// Convert a given name, which happens to have a list of string values, into
// a linked list of names, suitable to be passed to lower level APIs
// which work with CCR. Memory is allocated by this function. The caller
// should make sure that it gets freed properly.
//
//	Typical usage of this function is to convert a sequence (used in IDL)
// to a rdeplist array (used in the lower level APIs).
//
// This function is just a wrapper for two helper functions:
//
// get_value_list_dep searches the nv_seq for the nameval_t with key that
// matches property.
//
// convert_value_string_array_dep does the real work of actually converting the
// nameval_t's value (an idl_string_seq_t) to a rdeplist_t.
//
rdeplist_t *
get_value_string_array_dep(const char *property,
	const rgm::idl_nameval_seq_t &nv_seq, scha_errmsg_t &res)
{
	const rgm::idl_string_seq_t & valseq =
	    get_value_list(property, nv_seq);
	return ((rdeplist_t *)convert_value_string_array_dep(valseq, res));
}

//
// convert_value_string_array
//
// Converts the valseq to a namelist_t.
//
namelist_t *
convert_value_string_array(const rgm::idl_string_seq_t &valseq)
{
	namelist_t	*arrayptr, *p, *q;
	uint_t		i, len;

	len = valseq.length();

	// Cannot allocate all elements of the namelist in a single
	// chunk, because the "free()er" of this data structure assumes
	// all the elements to be separately malloc()ed.

	q = NULL;	// Previous element
	arrayptr = NULL;	// First element
	for (i = 0; i < len; i++) {
		p = (namelist_t *)malloc(sizeof (namelist_t));
		p->nl_name = strdup(valseq[i]);	// XXX Should this be strdup?

		// rgm_free_nlist() doesn't free this
		p->nl_next = NULL;
		if (q)
			q->nl_next = p;
		if (arrayptr == NULL)
			arrayptr = p;
		q = p;
	}

	return (arrayptr);
}

//
// convert_value_string_array
//
// Converts the valseq to a rdeplist_t.
//
rdeplist_t *
convert_value_string_array_dep(const rgm::idl_string_seq_t &valseq,
    scha_errmsg_t &result)
{
	rdeplist_t	*arrayptr, *restart_deps, *q;
	char *name, *loc_type, *val;
	uint_t		i, len;

	len = valseq.length();

	// Cannot allocate all elements of the rdeplist in a single
	// chunk, because the "free()er" of this data structure assumes
	// all the elements to be separately malloc()ed.

	q = NULL;	// Previous element
	arrayptr = NULL;	// First element
	for (i = 0; i < len; i++) {
		restart_deps = (rdeplist_t *)malloc(sizeof (rdeplist_t));
		val = strdup(valseq[i]);
		name = my_strtok(&val, RGM_DEP_SEPARATOR_PRE);
		restart_deps->rl_name = strdup(name);

		if (val == NULL) {
			// Opening curly brace not found.
			restart_deps->locality_type = FROM_RG_AFFINITIES;
		} else {
			loc_type = my_strtok(&val, RGM_DEP_SEPARATOR_POST);
			if ((val == NULL) || (strlen(val) != 0)) {
				// Closing curly brace not present or
				// the property does not end with '}'
				result.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&result,
				    REM_INVALID_PROP_VALUES,
				    restart_deps->rl_name);
				free(restart_deps->rl_name);
				free(restart_deps);
				return (NULL);
			} else {
				if (strcasecmp(SCHA_LOCAL_NODE, loc_type) == 0)
					restart_deps->locality_type =
					    LOCAL_NODE;
				else if (strcasecmp(SCHA_ANY_NODE,
				    loc_type) == 0)
					restart_deps->locality_type = ANY_NODE;
				else if (strcasecmp(SCHA_FROM_RG_AFFINITIES,
				    loc_type)
				    == 0)
					restart_deps->locality_type =
					    FROM_RG_AFFINITIES;
				else {
					result.err_code = SCHA_ERR_INVAL;
					rgm_format_errmsg(&result,
					    REM_INVALID_PROP_VALUES,
					    restart_deps->rl_name);
					free(restart_deps->rl_name);
					free(restart_deps);
					return (NULL);
				}
			}
		}

		// rgm_free_nlist() doesn't free this
		restart_deps->rl_next = NULL;
		if (q)
			q->rl_next = restart_deps;
		if (arrayptr == NULL)
			arrayptr = restart_deps;
		q = restart_deps;
	}

	return (arrayptr);
}


//
// nv_seq contains a list of name-value pairs.  This function converts
// the value corresponding to the given property name (which is a comma-
// delimited LN list) to a nodeidlist_t, suitable to be passed to lower
// level APIs
// which work with CCR. Memory is allocated by this function. The caller
// should make sure that it gets freed properly.
//
// This function is just a wrapper for two helper functions:
//
// get_value_list searches the nv_seq for the nameval_t with key that
// matches property.
//
// convert_value_modeid_array does the real work of actually converting the
// nameval_t's value (an idl_string_seq_t) to a nodeidlist_t.
//
scha_errmsg_t
get_value_nodeid_array(char *rg_name, const char *property,
    const rgm::idl_nameval_seq_t &nv_seq, nodeidlist_t **nodelist)
{
	const rgm::idl_string_seq_t & valseq = get_value_list(property, nv_seq);

	return (convert_value_nodeid_array(rg_name, valseq, nodelist));
}


//
// Given a logical nodename string 'ln' in any of the following forms:
//	nodename:zonename
//	nodeid:zonename
//	nodename
//	nodeid
// where 'nodeid' is a decimal string, finds the corresponding nodeid
// (if a nodename was provided) or checks the validity of the provided
// nodeid string, and returns the nodeid and and zonename individually
// through the reference out parameters.
//
// If ln contains no ":zonename" substring, zonenamep is set to NULL.
// To avoid memory leaks, zonenamep should not initially point to allocated
// memory.
//
// If the nodename or nodeid portion of ln is invalid, returns
// SCHA_ERR_NODE.  If swap is exhausted, returns SCHA_ERR_NOMEM.
//

static scha_err_t
rgm_logicalnode_to_nodeidzone(const char *ln, sol::nodeid_t &nodeid,
    char *&zonenamep)
{
	char 		*ptr = NULL;
	char		nodename[MAX_LN_LEN];

	zonenamep = NULL;
	if (strlcpy(nodename, ln, MAX_LN_LEN) >= MAX_LN_LEN) {
		return (SCHA_ERR_NODE);
	}
	ptr = strchr(nodename, LN_DELIMITER);
	if (NULL != ptr) {
		*ptr++ = '\0';
		if (!is_valid_zonename(ptr)) {
			return (SCHA_ERR_NODE);
		}
	}

	// either a valid nodeid string or a valid nodename string
	if (rgm_get_nodeid(nodename, &nodeid) != 0) {
		return (SCHA_ERR_NODE);
	}
	// we treat nodeid:global the same as nodeid, and
	// similarly nodename:global the same as nodename
	// Following this, for CZs, we will do the comparison with the CZ zone.
	// and n:z will be treated as n if the z belongs to the CZ.
	if (ptr != NULL && strcmp(ptr, ZONE) != 0) {
		zonenamep = strdup_nocheck(ptr);
		if (zonenamep == NULL) {
			return (SCHA_ERR_NOMEM);
		}
	}
	return (SCHA_ERR_NOERR);
}

/*
 * The per-node values are passed in 2 forms to the validate method.
 * 1. With the -X option, it is passed as prop_name{node_name}=value;
 * 2. With the -x option, the value on the local node is passed.
 *  update_local_node_value_add adds the property value on the localnode
 * onto the -x sequence (to be passed to the validate method).
 */
void
update_local_node_value(rgm::idl_string_seq_t &str_seq,
    rgm_property_list_t *ptr_ext,
    const rgm::idl_nameval_node_seq_t &nv_seq, rgm_resource_t *rsrc,
    rgm::lni_t lni, uint_t len)
{
	rgm_property_list_t *p;
	rgm_property_t *ptr;
	char *buff = NULL, *tmp = NULL;
	char *node_name = NULL;
	std::map<rgm::lni_t, char *> val;


	for (p = rsrc->r_ext_properties; p != 0; p = p->rpl_next) {
		ptr = p->rpl_property;
		if (!ptr->is_per_node) {
			continue;
		}

		if (is_ext_prop_updated(ptr->rp_key, nv_seq)) {
			tmp = strdup(ptr->rp_key);
			val = find_ext_prop(ptr_ext, ptr->rp_key);
			if (val[lni] == NULL) {
				if (((rgm_value_t *)
				    ptr->rp_value)->rp_value[lni]
				    == NULL)
					continue;
				buff = new char[strlen(tmp) +2];
				CL_PANIC(buff != NULL);
				(void) sprintf(buff, "%s=", tmp);
				str_seq.length(len + 1);
				str_seq[len++] = buff;
			} else if (((rgm_value_t *)ptr->rp_value)->rp_value[lni]
			    == NULL) {
				buff = new char[strlen(tmp)+ 2 +
				    strlen(val[lni])];
				CL_PANIC(buff != NULL);
				(void) sprintf(buff, "%s=%s", tmp,
				    val[lni]);
				str_seq.length(len + 1);
				str_seq[len++] = buff;
			} else if (strcmp(((rgm_value_t *)
			    ptr->rp_value)->rp_value[lni],
			    val[lni]) != 0) {
				buff = new char[strlen(tmp) +2 +
				    strlen(val[lni])];
				CL_PANIC(buff != NULL);
				(void) sprintf(buff, "%s=%s", tmp, val[lni]);
				str_seq.length(len + 1);
				str_seq[len++] = buff;
			}
			free(tmp);
		}
	}
}

//
// Update the validate method idl structure with the all-nodes value
// of per-node extension property. The all nodes value is passed to the
// validate call-back with a -X flag.
//
void
update_all_nodes_value(std::map<rgm::lni_t, char *> &ptr, name_t key,
    rgm::idl_string_seq_t &all_nodes_ext, int *index, LogicalNodeset *ns)
{
	uint_t i;
	char *node_name = NULL;
	int len = all_nodes_ext.length();

		for (LogicalNodeManager::iter it = lnManager->begin();
		    it != lnManager->end(); ++it) {
			if (!ns->containLni(it->second))
				continue;
			node_name = strdup((it->first).data());
			CL_PANIC(node_name != NULL);
			if (ptr[it->second] != NULL) {
				all_nodes_ext.length(len + 1);
				all_nodes_ext[len] = new char[strlen(key) +
				    strlen(ptr[it->second]) +
				    strlen(node_name) + 4];
				CL_PANIC((char *)all_nodes_ext[len]);
				(void) sprintf(all_nodes_ext[len++],
				    "%s{%s}=%s", key, node_name,
				    ptr[it->second]);
			} else {
				all_nodes_ext.length(len + 1);
				all_nodes_ext[len] = new char[strlen(key) +
				    strlen(node_name) + 4];
				CL_PANIC((char *)all_nodes_ext[len]);
				(void) sprintf(all_nodes_ext[len++],
				    "%s{%s}=", key, node_name);
			}

		}

}

/*
 * The per-node values are passed in 2 forms to the validate method.
 * 1. With the -X option, it is passed as prop_name{node_name}=value;
 * 2. With the -x option, the value on the local node is passed.
 *  update_local_node_value_add adds the property value on the localnode
 * onto the -x sequence (to be passed to the validate method).
 */
void
update_local_node_value_add(rgm_resource_t *rsrc,
    rgm::idl_string_seq_t &ext_props,
    rgm::lni_t lni, int i)
{
	rgm_property_list_t *p;
	rgm_property_t *ptr;

	// Fill in the sequence
	for (p = rsrc->r_ext_properties; p != 0; p = p->rpl_next) {
		ptr = p->rpl_property;
		if (ptr->rp_type == SCHA_PTYPE_STRINGARRAY) {
			continue;
		}
		if (ptr->is_per_node == B_TRUE) {
			if (((rgm_value_t *)ptr->rp_value)->rp_value[lni] ==
			    NULL) {
				ext_props.length(i + 1);
				ext_props[i] = new char[strlen(ptr->rp_key)
				    + 2];
				CL_PANIC((char *)ext_props[i]);
				(void) sprintf(ext_props[i++], "%s=",
				    ptr->rp_key);
			} else {
				ext_props.length(i + 1);
				ext_props[i] = new char[strlen(ptr->rp_key)
				    + strlen(((rgm_value_t *)ptr->rp_value)
				    ->rp_value[lni]) + 2];
				CL_PANIC((char *)ext_props[i]);
				(void) sprintf(ext_props[i++], "%s=%s",
				    ptr->rp_key,
				    ((rgm_value_t *)ptr->rp_value)
				    ->rp_value[lni]);
			}
		}
	}

}

//
// Update the validate method idl structure with the all-nodes value
// of per-node extension property. The all nodes value is passed to
// the validate call-back
// with a -X flag.
//
void
update_all_nodes_val(const rgm::idl_nameval_node_seq_t &str_seq,
    rgm_property_list_t *rs_conf, rgm_property_list_t *rs,
    rgm::idl_string_seq_t &str_seq_all_nodes, LogicalNodeset *ns)
{
	rgm_property_t  *prop = NULL;
	rgm_property_list_t *rps = NULL;
	std::map<rgm::lni_t, char *> val;
	char *node_name = NULL, *buff = NULL, *tmp = NULL;
	uint_t len_all_nodes = str_seq_all_nodes.length();

	for (rps = rs_conf; rps; rps = rps->rpl_next) {
		prop = rps->rpl_property;
		if (!prop->is_per_node) {
			continue;
		}
		if (!is_ext_prop_updated(prop->rp_key, str_seq))
			continue;
		val = find_ext_prop(rs, prop->rp_key);

		for (LogicalNodeManager::iter it = lnManager->begin();
		    it != lnManager->end(); ++it) {
			if (!ns->containLni(it->second))
				continue;
			tmp = strdup(prop->rp_key);
			node_name = strdup((it->first).data());
			CL_PANIC(node_name != NULL);
			if (((rgm_value_t *)prop->rp_value)->
			    rp_value[it->second] == NULL) {
				if (val[it->second] == NULL)
					continue;
				buff = new char[strlen(tmp) + 4 +
				    strlen(node_name)];
				CL_PANIC(buff != NULL);
				(void) sprintf(buff, "%s{%s}=", tmp, node_name);
				str_seq_all_nodes.length(len_all_nodes + 1);
				str_seq_all_nodes[len_all_nodes++] = buff;
				free(tmp);
				free(node_name);
				continue;
			} else if ((val[it->second] == NULL)) {
				buff = new char[strlen(tmp)+ 4 +
				    strlen(node_name) +
				    strlen(((rgm_value_t *)prop->rp_value)
				    ->rp_value[it->second])];
				CL_PANIC(buff != NULL);
				(void) sprintf(buff, "%s{%s}=%s", tmp,
				    node_name,
				    ((rgm_value_t *)
				    prop->rp_value)->rp_value[it->second]);
				str_seq_all_nodes.length(len_all_nodes + 1);
				str_seq_all_nodes[len_all_nodes++] = buff;
				free(tmp);
				free(node_name);
				continue;
			} else if (strcmp(((rgm_value_t *)prop->rp_value)->
			    rp_value[it->second], val[it->second]) != 0) {
				buff = new char[strlen(tmp) + 4 +
				    strlen(node_name) +
				    strlen(((rgm_value_t *)prop->rp_value)
				    ->rp_value[it->second])];
				CL_PANIC(buff != NULL);
				(void) sprintf(buff, "%s{%s}=%s",
				    tmp, node_name,
				    ((rgm_value_t *)prop->rp_value)->
				    rp_value[it->second]);
				str_seq_all_nodes.length(len_all_nodes + 1);
				str_seq_all_nodes[len_all_nodes++] = buff;
				free(tmp);
				free(node_name);
				continue;
			}
		}

	}

}



//
// Convert a idl_nameval_seq_t to idl_string_seq_t
//
uint_t
nvseq_to_strseq_ext(const rgm::idl_nameval_node_seq_t &nv_seq,
    rgm::idl_string_seq_t &str_seq, rgm_resource_t *rsrc)
{
	uint_t	i, j, len = 1, act_len, size = 0, all_nodes = 1;
	uint_t	k, node_len;
	char	*buff, *s;
	char	*tmp;

	act_len = nv_seq.length();
	for (i = 0; i < act_len; i++) {
		if ((nv_seq[i].is_per_node) &&
		    (nv_seq[i].nv_nodes.length() != 0)) {
			// Count of the number of  per_node values updated.
			all_nodes++;
			continue;
		}
		// Count the number of non-per node
		// values being updated.
		len++;
	}
	len = str_seq.length();
	k = 0;

	for (i = 0; i < act_len; i++) {
		tmp = strdup(nv_seq[i].idlstr_nv_name);
		node_len = 0;
		if (nv_seq[i].is_per_node) {
			continue;
		}
		if (nv_seq[i].nv_val[0] == (const char *)NULL) {
			// 2 is for '=' and '\0'
			buff = new char[strlen(tmp) + 2];
			CL_PANIC(buff != NULL);
			(void) sprintf(buff, "%s=", tmp);
			str_seq.length(len + 1);
			str_seq[len++] = buff;
			free(tmp);
			continue;
		}

		// Calculate the total size
		for (j = 0; j < nv_seq[i].nv_val.length(); j++) {
			size += (uint_t)strlen(nv_seq[i].nv_val[j]) + 1;
		}

		// Allocate the buff with calculated size
		buff = new char[size];
		CL_PANIC(buff != NULL);

		// fill the buff
		buff[0] = '\000';
		if (nv_seq[i].nv_val.length() > 0) {
			(void) strcat(buff, nv_seq[i].nv_val[0]);
		}
		for (j = 1; j < nv_seq[i].nv_val.length(); j++) {
			(void) strcat(buff, ",");
			(void) strcat(buff, nv_seq[i].nv_val[j]);
		}

		s = new char[strlen(tmp) + strlen(buff) + 2];
		CL_PANIC(s != NULL);
		(void) sprintf(s, "%s=%s", tmp, buff);
		str_seq.length(len + 1);
		str_seq[len++] = s;
		free(tmp);
		delete [] buff;
	}

	return (len);
}

// Given the rgm_property_list_t structure, return the rp_value
// for the key specified.
//
std::map<rgm::lni_t, char *> find_ext_prop(rgm_property_list_t *rp, char *key)
{
	rgm_property_list_t *rps = NULL;
	rgm_property_t *prop = NULL;
	std::map<rgm::lni_t, char *> t;
	for (rps = rp; rps; rps = rps->rpl_next) {
		prop = rps->rpl_property;
		if (strcmp(prop->rp_key, key) == 0) {
			return (((rgm_value_t *)prop->rp_value)
			    ->rp_value);

		}
	}
	return (t);
}

//
// get_ln
// Given a logical nodename string 'lnName' in any of the following forms:
//	nodename:zonename
//	nodeid:zonename
//	nodename
//	nodeid
// where 'nodeid' is a decimal string, returns a pointer to the
// LogicalNode corresponding to the lnName.  Returns NULL if the provided
// logical nodename is not configured.  Returns an error code
// through the out parameter err_out, if err_out is provided and is not NULL.
//
// If lnName does not describe a configured node or zone, *err_out is set to
// SCHA_ERR_NODE.  If swap is exhausted, *err_out is set to SCHA_ERR_NOMEM.
// (Note, low swap might crash the node because computeLn calls the
// interposed malloc).  No other error codes are currently returned.
//
LogicalNode *
get_ln(const char *lnName, scha_err_t *err_out)
{
	sol::nodeid_t	nodeid;
	char		*zonename = NULL;
	LogicalNode	*ln = NULL;

	scha_err_t res_code = rgm_logicalnode_to_nodeidzone(lnName, nodeid,
	    zonename);
	//
	// Note that rgm_logicalnode_to_nodeidzone() returns SCHA_ERR_NODE if
	// the physical node portion of lnName is not a valid cluster node.
	//
	if (res_code == SCHA_ERR_NOERR) {
		ln = lnManager->findLogicalNode(nodeid, zonename, NULL);
		if (ln == NULL) {
			ucmm_print("RGM", NOGET("get_ln: bad zonename\n"));
			res_code = SCHA_ERR_NODE;
		}
	} else if (res_code != SCHA_ERR_NOMEM) {
		ucmm_print("RGM",
		    NOGET("get_ln: bad nodename\n"));
	}

	if (err_out != NULL) {
		*err_out = res_code;
	}
	free(zonename);
	return (ln);
}


//
// convert_value_nodeid_array
//
// Converts the rgm::idl_string_seq_t to a nodeidlist_t.
//
scha_errmsg_t
convert_value_nodeid_array(char *rg_name,
    const rgm::idl_string_seq_t &valseq, nodeidlist_t **nodelist)
{
	scha_errmsg_t   res = {SCHA_ERR_NOERR, NULL};
	nodeidlist_t	*arrayptr, *p = NULL, *q;
	uint_t		i, len;
	sol::nodeid_t	nodeid;
	char		*zonename = NULL;

	len = valseq.length();

	// Cannot allocate all elements of the nodeidlist in a single
	// chunk, because the "free()er" of this data structure assumes
	// all the elements to be separately malloc()ed.

	q = NULL;	// Previous element
	arrayptr = NULL;	// First element
	for (i = 0; i < len; i++) {
		p = (nodeidlist_t *)malloc(sizeof (nodeidlist_t));
		if (p == NULL) {
			res.err_code = 	SCHA_ERR_NOMEM;
			goto fini_convert_value_nodeid_array; //lint !e801
		}

		res.err_code = rgm_logicalnode_to_nodeidzone(valseq[i],
		    nodeid, zonename);
		if (res.err_code == SCHA_ERR_NODE) {
			ucmm_print("RGM",
			    NOGET("convert_value_nodeid_array():"
			    " RG <%s> has bad nodename <%s> in nodelist\n"),
			    rg_name, (const char *)valseq[i]);
			rgm_format_errmsg(&res, REM_INVALID_NODE,
			    (const char *)valseq[i]);
			goto fini_convert_value_nodeid_array; //lint !e801
		} else if (res.err_code != SCHA_ERR_NOERR) {
			goto fini_convert_value_nodeid_array; //lint !e801
		}

		p->nl_nodeid = nodeid;
		p->nl_zonename = zonename;
		// lni will be assigned by create_lni_mappings_from_nodeidlist()
		p->nl_lni = LNI_UNDEF;
		p->nl_next = NULL;

		if (q)
			q->nl_next = p;
		if (arrayptr == NULL)
			arrayptr = p;
		q = p;
	}
	*nodelist = arrayptr;

fini_convert_value_nodeid_array:
	if (SCHA_ERR_NOERR != res.err_code) {
		q = arrayptr;
		free(p);
		while (q) {
			p = q->nl_next;
			free(q->nl_zonename);
			free(q);
			q = p;
		}
	}
	return (res);
}


//
// convert_seq_to_rlistp
//
// Converts the valseq to an array of pointers of rlist_p_t struct.
//
rlist_p_t *
convert_seq_to_rlistp(const rgm::idl_string_seq_t &valseq)
{
	rlist_p_t	*arrp;
	uint_t		i, len;

	len = valseq.length();

	arrp = (rlist_p_t *)malloc(sizeof (rlist_p_t) * (len + 1));
	for (i = 0; i < len; i++) {
		arrp[i] = rname_to_r((rglist_t *)NULL, valseq[i]);
		CL_PANIC(arrp[i] != NULL);
	}
	arrp[len] = NULL;

	return (arrp);
}


//
// Convert a given name, which happens to have a list of string values or
// the special designated ALL value, into a namelist_all_t structure.
// suitable to work with lower-level APIs which work with CCR.
// This structure contains a regular linked list of names if the
// special value isn't designated.
//
// Handle the access as for a regular list, but then check for the
// special case and convert the value.
//
// Typical usage of this function is to convert a sequence (used in IDL)
// to a namelist_all_t list (used in the lower level APIs).
//
// If update_op is FALSE, this routine is being called on RG creation.  If
// TRUE, it's being called on RG update.
//
//
namelist_all_t
get_value_string_all_list(const char *property,
	const rgm::idl_nameval_seq_t &nv_seq, boolean_t update_op)
{
	namelist_all_t	alist;
	namelist_t *regular_list = NULL;

	//
	// RG creation cases:
	//	set the is_ALL_value to B_TRUE,
	//	if the given property is specified in scrgadm -y
	//	and wildcard is specified,
	//	or if the given property is not specified in scrgadm -y
	//
	// RG update case:
	// 	set the is_ALL_value to B_TRUE,
	//	if the given property is specified in scrgadm -y
	//	and wildcard is specified.
	//
	alist.is_ALL_value = B_TRUE;
	alist.names = NULL;

	regular_list = get_value_string_array(property, nv_seq);

	//
	// RG update:
	// 	is_ALL_value is set to B_FALSE,
	// 	if the given property is not specified in scrgadm -y
	//
	if ((update_op) && (regular_list == NULL)) {
		alist.is_ALL_value = B_FALSE;
		return (alist);
	}

	//
	// RG update and creation cases:
	//	is_ALL_value is set to B_FALSE and names is set to specified
	// 	values,	if the given property is specified in scrgadm -y
	//	and wildcard is not specified,
	//
	if ((regular_list != NULL) &&
	    (strcmp(regular_list->nl_name, SCHA_ALL_SPECIAL_VALUE) != 0)) {
		/*
		 * Single element list containing not "*" value
		 */
		alist.is_ALL_value = B_FALSE;
		alist.names = regular_list;
	}
	return (alist);
}


#ifndef EUROPA_FARM
//
// get_static_nodelist(): Creates a nodeidlist of the nodeids of all the
// nodes in the cluster
// (static membership, NOT current). Used to create default value of
// RG Nodelist, which contains all physical nodes.
//
// Retrieves physical nodes only, not zones.
//
// Note: This function gets all physical nodes as a default value for
// the RG nodelist (on a newly created RG) when no nodelist has been
// specified in scrgadm.
// Using all physical nodes (i.e., global zones) seems somewhat
// questionable as a default for nodelist on a system with non-global zones.
// Perhaps in that case we should require the user to specify the nodelist
// and not use a default value.
// There is a check for empty nodelist in the function rg_sanity_checks(),
// and an error message REM_EMPTY_NODELIST.  It would appear that this
// message is never produced, because an empty nodelist gets defaulted
// to the list of all physical nodes.
//
// For more information, see CR 6326003 .
//
// on Europa cluster, this function returns only the list of Server nodes.
// Cannot be used as a default value, as a RG can be created to be instancianted
// only on a list of Farm nodes, the mix between Server and Farm nodes is not
// allowed.
//
nodeidlist_t *
get_static_nodelist(void)
{
	nodeidlist_t	*arrayptr, *p, *q;
	sol::nodeid_t	i;

	q = NULL;	// Previous element
	arrayptr = NULL;	// First element

	for (i = 1; i <= MAXNODES; i++) {

		// Check if this is a 'configured node'.
		if (!nodeset_contains(Rgm_state->thecluststate.rp_all_nodes,
		    i)) {
			continue;
		}

		p = (nodeidlist_t *)malloc(sizeof (nodeidlist_t));
		p->nl_nodeid = i;
		p->nl_zonename = NULL;
		// The following might not be correct for Europa
		p->nl_lni = p->nl_nodeid;
		p->nl_next = NULL;

		if (arrayptr == NULL) {
			arrayptr = p;
		} else {
			q->nl_next = p;
		}
		q = p;
	}

	return (arrayptr);
}


//
// String sequence to an RPC style array of chars.
//
uint_t
strseq_to_array(const rgm::idl_string_seq_t &str_seq, strarr_list **str_list)
{
	uint_t		i, l;
	strarr_list	*p;

	l = str_seq.length();
	if (l == 0) {
		*str_list = NULL;
		return (0);
	}

	p = (strarr_list *) malloc(sizeof (strarr_list));
	p->strarr_list_len = l;
	p->strarr_list_val = (char **)malloc(l * sizeof (char *));
	for (i = 0; i < l; i++) {
		p->strarr_list_val[i] = strdup(str_seq[i]);
	}
	*str_list = p;
	return (l);
}
#endif // !EUROPA_FARM


//
// Returns the LogicalNodeset for all the LN in the specified nodelist.
// On a non-president node, only includes zones which are on the local node.
//
// Allocates memory for the returned LogicalNodeset, which must be deleted
// by the caller.
//
LogicalNodeset*
get_logical_nodeset_from_nodelist(nodeidlist_t *nodelist)
{
	LogicalNodeset *nset = new LogicalNodeset;
	nodeidlist_t *nl;
	CL_PANIC(nset != NULL);

	for (nl = nodelist; nl != NULL; nl = nl->nl_next) {
		//
		// skip logical nodes for which the lni is LNI_UNDEF -- those
		// are not on this physical node
		//
		if (nl->nl_lni == LNI_UNDEF) {

#ifndef EUROPA_FARM
			// assert that it's a non-global zone
			CL_PANIC(nl->nl_zonename != NULL);
			ucmm_print("RGM", "glnfn: skipping %d:%s\n",
			    nl->nl_nodeid, nl->nl_zonename);
#else
			// All LNIs (physical+logical) that are not
			// on this farm node are undefined (LNI_UNDEF).
			ucmm_print("RGM", "glnfn: skipping %d\n",
			    nl->nl_nodeid);
#endif
		} else {
			nset->addLni(nl->nl_lni);
		}
	}
	return (nset);
}


//
// Returns the bitmask of installed nodes LNIs for the specified RT.
//
LogicalNodeset*
installed_nodes(rgm_rt_t *rt)
{
	LogicalNodeset *nset = NULL;
	nodeidlist_t *nl;

	if (rt->rt_instl_nodes.is_ALL_value) {
		nset = lnManager->
		    get_all_server_logical_nodes(rgm::LN_DOWN);
		return (nset);
	}

	nset = new LogicalNodeset;

	if (rt->rt_instl_nodes.nodeids == NULL) {
		return (nset);
	}

	// If specified, get all the nodes in the list.
	for (nl = rt->rt_instl_nodes.nodeids; nl != NULL; nl = nl->nl_next) {
		nset->addLni(nl->nl_lni);
	}
	return (nset);
}


//
// compute_meth_nodes
//
// Returns the bitmask of nodeids on which init/fini/validate is configured to
// run, depending on whether rt_init_nodes is set to RG_PRIMARIES or
// INSTALLED_NODES.
//
// If the associated resource is not a scalable service (as indicated by the
// is_scalable parameter) and 'meth' is not registered, returns the empty
// nodeset.  If the associated resource is a scalable service, then the SSM
// wrapper will have to be launched whether or not the method is registered,
// so we return the populated nodeset.
//
LogicalNodeset*
compute_meth_nodes(
	rgm_rt_t	*rt,		// the resource type
	rgm_rg_t	*rgl_ccr,	// the RG
	method_t	meth,		// the method
	boolean_t	is_scalable)	// is the resource a scalable service?
{
	LogicalNodeset *nset = NULL;
	scha_initnodes_flag_t inodes;

	// Return the empty nodeset if the method is not registered and
	// the associated resource is not a scable service.
	if (!is_method_reg(rt, meth, NULL) && !is_scalable) {
		nset = new LogicalNodeset;
		return (nset);
	}

	inodes = rt->rt_init_nodes;

	switch (inodes) {
	case SCHA_INFLAG_RG_PRIMARIES:
		nset = get_logical_nodeset_from_nodelist(rgl_ccr->rg_nodelist);
		return (nset);

	case SCHA_INFLAG_RT_INSTALLED_NODES:
		nset = installed_nodes(rt);
		return (nset);
	}
	// Empty Nodeset;
	nset = new LogicalNodeset;
	return (nset);
}

#ifndef EUROPA_FARM
//
// in_endish_rg_state
//
// Returns TRUE if the specified RG is in an "endish" state on all nodes;
// otherwise returns FALSE.  The following states are defined to be
// "endish":
//	ONLINE
//	OFFLINE
//	OFF_BOOTED
//	OFFLINE_START_FAILED
//	ERROR_STOP_FAILED
//	OFF_PENDING_BOOT
//	ON_PENDING_R_RESTART
//
// Note that the last two of these states (OFF_PENDING_BOOT and
// ON_PENDING_R_RESTART) are not really
// "endish" states because the node is running methods on the RG's resources.
// The additional out-parameters pending_boot_nodes and r_restart_nodes
// (see below) return the set of nodes on which the RG is in OFF_PENDING_BOOT
// or ON_PENDING_R_RESTART respectively.  Some callers will consider
// the RG to be a non-endish state if one of these nodesets is non-empty.
// However, some callers (such as scha_control_restart) can run even if the
// RG is running BOOT methods on another node or restarting a resource on any
// node.  For another example, a scha_control giveover or scswitch -z
// needs to be able to proceed with a switch
// even if the RG is booting on some nodes, provided that we are not
// attempting to make such a node a new master of the RG.
//
// All other RG states not listed above are considered "non-endish".
//
// The RG might be managed or unmanaged, this does not affect the
// determination of whether it is in an endish state.  (For example, an
// unmanaged RG might be SW_UPDATING while a resource is being added to
// it).
//
// If the RG is being switched or updated (as indicated by the
// rgl_switching flag having a value other than SW_NONE), returns FALSE.
//
// This function takes four additional (LogicalNodeset *) parameters as
// "out" parameters, which are set only when non-NULL, and only when the
// function returns TRUE:
//	pending_boot_nodes: returns the set of nodes on which the RG
//		is in OFF_PENDING_BOOT state, i.e., the nodes that are
//		currently running BOOT methods on this RG's resources.
//	stop_failed_nodes: returns the set of nodes on which this RG is
//		in ERROR_STOP_FAILED state.
//	offline_nodes: returns the set of nodes on which the RG is either
//		OFFLINE, OFFLINE_START_FAILED, or OFF_BOOTED.
//	r_restart_nodes: returns the set of nodes on which the RG is
//		in ON_PENDING_R_RESTART state.
//
// NOTE: The current state machine logic implies that resources cannot
// be in non-endish states in an RG that is in an endish state.
// So, theoretically, this function should not have to check
// resource states but only RG states.  However we will leave the
// R checks in for now as a sanity-check and debugging aid.  We handle
// ON_PENDING_R_RESTART as a special case, since stopping or starting
// methods may be running in that case.
//
// The caller is running on the president and holds the RGM state mutex.
//
boolean_t
in_endish_rg_state(
	rglist_p_t rg_ptr,		// pointer to the RG
	LogicalNodeset *pending_boot_nodes,	// (out) nodes on which RG is
					//  OFF_PENDING_BOOT
	LogicalNodeset *stop_failed_nodes,	// (out) nodes on which RG is
					//  ERROR_STOP_FAILED
	LogicalNodeset *offline_nodes,	// (out) nodes on which RG is OFFLINE,
					//  OFFLINE_START_FAILED, or OFF_BOOTED
	LogicalNodeset *r_restart_nodes)	// (out) nodes on which RG is
					//  ON_PENDING_R_RESTART
{
	rlist_p_t	rsrc = NULL;
	LogicalNodeset	pending_boot_ns;
	LogicalNodeset	stop_failed_ns;
	LogicalNodeset	offline_ns;
	LogicalNodeset	r_restart_ns;
	sc_syslog_msg_handle_t handle;

	// It should not be in the middle of a switch or update
	if (rg_ptr->rgl_switching != SW_NONE) {
		ucmm_print("endish", NOGET("rgl_switching not clear"));
		return (B_FALSE);
	}

	//
	// Check the state of the RG and its underlying Rs on each
	// logical node in the cluster
	//
	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);

		rgm::lni_t node = lnNode->getLni();

		//
		// If node is a non-global zone, then we have to check
		// the RG state if the physical node is up, even if the zone
		// itself is dead -- the RG might be in pending offline or
		// error_stop_failed on the dead zone.
		//
		if (!lnNode->isPhysNodeUp()) {
			continue;
		}

		switch (rg_ptr->rgl_xstate[node].rgx_state) {
		case rgm::RG_ONLINE:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
			// these states are "endish"
			break;

		case rgm::RG_OFFLINE:
		case rgm::RG_OFF_BOOTED:
		case rgm::RG_OFFLINE_START_FAILED:
			// these states are "endish" and set an out-parameter
			offline_ns.addLni(node);
			break;

		case rgm::RG_ERROR_STOP_FAILED:
			// this state is "endish" and sets an out-parameter
			stop_failed_ns.addLni(node);
			break;

		case rgm::RG_OFF_PENDING_BOOT:
			// considered "endish", and sets an out-parameter
			pending_boot_ns.addLni(node);
			break;

		case rgm::RG_ON_PENDING_R_RESTART:
			// considered "endish", and sets an out-parameter
			r_restart_ns.addLni(node);
			break;

		case rgm::RG_PENDING_ONLINE:
		case rgm::RG_PENDING_OFFLINE:
		case rgm::RG_PENDING_OFF_STOP_FAILED:
		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_ON_PENDING_MON_DISABLED:
		case rgm::RG_OFF_PENDING_METHODS:
		case rgm::RG_PENDING_OFF_START_FAILED:
		case rgm::RG_PENDING_ON_STARTED:
			// these states are not "endish"
			ucmm_print("endish", NOGET("RG <%s> in state <%s>"),
			    rg_ptr->rgl_ccr->rg_name,
			    pr_rgstate(rg_ptr->rgl_xstate[node].rgx_state));
			return (B_FALSE);
		}
	}

#ifdef DEBUG
	//
	// The following code is a sanity check only; that is, it is
	// logically redundant with the RG state checks above.
	// In all of our testing we have never encountered the
	// Internal Error below.  Therefore, we are now conditionally
	// compiling the code below for debug builds only.
	//

	//
	// The RG state is "endish" on all nodes.
	// The resource state of each of its resources must also
	// be "endish" on ALL nodes.  If not, report an internal
	// error (should never happen).
	//
	// Since this is a sanity check, we include DELETED Rs in
	// the check (they shouldn't be non-endish either).
	// UNINITED is an endish state for deleted resources.
	//
	// PRENET_STARTED is endish, because a resource
	// can end up in this state if it has a strong R-dependency
	// on another resource that is START_FAILED.
	// Similarly, a resource can end up in STOPPED if a resource
	// with a strong R-dependency on it went to STOP_FAILED while
	// running POSTNET_STOP.
	//
	// PENDING_BOOT and BOOTING are considered endish because
	// an OFF_PENDING_BOOT RG is considered endish (see header
	// comment of in_endish_rg_state, above).
	//
	rsrc = rg_ptr->rgl_resources;
	while (rsrc) {

		for (LogicalNodeManager::iter it = lnManager->begin();
		    it != lnManager->end(); ++it) {
			LogicalNode *lnNode = lnManager->findObject(it->second);
			CL_PANIC(lnNode != NULL);

			rgm::lni_t node = lnNode->getLni();

			if (!lnNode->isInCluster()) {
				continue;
			}

			switch (rsrc->rl_xstate[node].rx_state) {
			case rgm::R_MON_FAILED:
			case rgm::R_OFFLINE:
			case rgm::R_ONLINE:
			case rgm::R_ONLINE_STANDBY:
			case rgm::R_START_FAILED:
			case rgm::R_STOP_FAILED:
			case rgm::R_ONLINE_UNMON:
			case rgm::R_PRENET_STARTED:
			case rgm::R_STOPPED:
			case rgm::R_UNINITED:
			case rgm::R_PENDING_BOOT:
			case rgm::R_BOOTING:
			//
			// R_STARTING_DEPEND can be endish if
			// the RG is PENDING_ONLINE_BLOCKED
			//
			case rgm::R_STARTING_DEPEND:
				// endish states
				break;

			case rgm::R_MON_STARTING:
			case rgm::R_MON_STOPPING:
			case rgm::R_POSTNET_STOPPING:
			case rgm::R_PRENET_STARTING:
			case rgm::R_STARTING:
			case rgm::R_STOPPING:
			case rgm::R_STOPPING_DEPEND:
			case rgm::R_OK_TO_STOP:
			case rgm::R_OK_TO_START:
				//
				// considered 'endish' only if the resource
				// might be restarting on the node; error
				// otherwise
				//
				if (rg_ptr->rgl_xstate[node].rgx_state ==
				    rgm::RG_ON_PENDING_R_RESTART) {
					break;
				}

				// FALLTHRU

			case rgm::R_PENDING_INIT:
			case rgm::R_PENDING_FINI:
			case rgm::R_INITING:
			case rgm::R_FINIING:
			case rgm::R_PENDING_UPDATE:
			case rgm::R_ON_PENDING_UPDATE:
			case rgm::R_ONUNMON_PENDING_UPDATE:
			case rgm::R_MONFLD_PENDING_UPDATE:
			case rgm::R_UPDATING:
			//
			// JUST_STARTED is not endish because we
			// are waiting for the president to ack it.
			//
			case rgm::R_JUST_STARTED:
				// not endish states
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The rgmd has discovered that the indicated
				// resource group's state information appears
				// to be incorrect. This may prevent any
				// administrative actions from being performed
				// on the resource group.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "INTERNAL ERROR: resource group "
				    "<%s> state <%s> node <%s> contains "
				    "resource <%s> in state <%s>",
				    rg_ptr->rgl_ccr->rg_name,
				    pr_rgstate(rg_ptr->rgl_xstate[node].
					rgx_state), lnNode->getFullName(),
				    rsrc->rl_ccrdata->r_name,
				    pr_rstate(rsrc->rl_xstate[node].rx_state));
				sc_syslog_msg_done(&handle);
				return (B_FALSE);
			}
		}
		rsrc = rsrc->rl_next;
	}
#endif	// DEBUG

	// Copy results (operator=)
	if (pending_boot_nodes != NULL) {
		*pending_boot_nodes = pending_boot_ns;
	}
	if (stop_failed_nodes != NULL) {
		*stop_failed_nodes = stop_failed_ns;
	}
	if (offline_nodes != NULL) {
		*offline_nodes = offline_ns;
	}
	if (r_restart_nodes != NULL) {
		*r_restart_nodes = r_restart_ns;
	}
	return (B_TRUE);
}
#endif // !EUROPA_FARM


//
// use_logicalnode_affinities
// Returns B_TRUE if logical-node affinities (i.e. zone affinities) are to
// be used; returns B_FALSE if physical node affinities should be used.
//
// Logical node affinities are used between two RGs in the same zone cluster.
// For affinities between RGs in different zone clusters, a different
// mechanism (inter-cluster RG affinities) is applied.
//
// In a base cluster, physical node affinities are used in either of the
// following two cases:
// 1) There is more than one voting node (i.e., physical node or Solaris host)
// configured in the base cluster; or
// 2) The file USE_PHYSNODE_FILE (defined in rgm_intention.cc) exists in the
// global zone of the single physical node and is readable.
//
// Otherwise logical node affinities are used.
//
boolean_t
use_logicalnode_affinities(void)
{
	if (strcmp(ZONE, GLOBAL_ZONENAME) == 0 &&
	    (Rgm_state->use_physnode || Rgm_state->num_static_nodes > 1)) {
		return (B_FALSE);
	} else {
		return (B_TRUE);
	}
}


//
// get_all_zones_on_phys_node
// Adds to nset all base cluster zones (including the global zone) that are on
// the same physical node as 'lni'.
//
void
get_all_zones_on_phys_node(rgm::lni_t lni, LogicalNodeset &nset)
{
	LogicalNode *ln = lnManager->findObject(lni);
	rgm::lni_t physLni = ln->getNodeid();
	LogicalNode *physLn = lnManager->findObject(physLni);
	std::list<LogicalNode*>::const_iterator it;

	nset.addLni(physLni);
	for (it = physLn->getLocalZones().begin();
	    it != physLn->getLocalZones().end(); ++it) {
		nset.addLni((*it)->getLni());
	}
}


//
// add_phys_zones_to_nodeset
// If physical-node affinities are enabled, adds to nset all base cluster
// zones (including the global zone) that are on the same physical node as
// any node that is initially in nset.
//
// Otherwise, leaves nset unchanged.
//
void
add_phys_zones_to_nodeset(LogicalNodeset &nset)
{
	if (use_logicalnode_affinities()) {
		return;
	}

	LogicalNodeset init_nset = nset;
	rgm::lni_t lni = 0;

	while ((lni = init_nset.nextLniSet(lni + 1)) != 0) {
		get_all_zones_on_phys_node(lni, nset);
	}
}


//
// is_rg_online_on_node
// Return B_TRUE if the RG is ONLINE or in an online-equivalent state on the
// given node, including busy states and error states.  These are states in
// which the RG is still "attempting" to be online or provide service.
// Return B_FALSE otherwise.
//
// If 'logical_node' is false, rgp may be online on any base cluster zone in
// the same physical node as 'lni'; otherwise it must be online on 'lni' itself.
//
// The caller must already hold the RGM state lock.  If called on a
// non-president node, 'lni' must be on the local node.
//
// If the RG is PENDING_OFF_STOP_FAILED or ERROR_STOP_FAILED on the node,
// we consider this an "on-ish" state because the RG did not successfully
// stop on this node, nor can it start on any other node.
// We also treat PENDING_OFF_START_FAILED as "on-ish" because the RGM will
// try to bring the RG online somewhere.
//
// The caller may rule out busy or errored states by other means, e.g.,
// by calling in_endish_rg_state.
//
boolean_t
is_rg_online_on_node(rglist_p_t rgp, rgm::lni_t lni, boolean_t logical_node)
{
	LogicalNodeset nset;

	if (logical_node) {
		nset.addLni(lni);
	} else {
		get_all_zones_on_phys_node(lni, nset);
	}

	rgm::lni_t currNode = 0;
	while ((currNode = nset.nextLniSet(currNode + 1)) != 0) {
		switch (rgp->rgl_xstate[currNode].rgx_state) {

		// "on-ish" states
		case rgm::RG_ONLINE:
		case rgm::RG_PENDING_ONLINE:
		case rgm::RG_PENDING_ON_STARTED:
		case rgm::RG_PENDING_OFF_START_FAILED:
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_ON_PENDING_MON_DISABLED:
		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_PENDING_OFF_STOP_FAILED:
		case rgm::RG_ERROR_STOP_FAILED:
		case rgm::RG_ON_PENDING_R_RESTART:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
			return (B_TRUE);

		// "off-ish" states
		case rgm::RG_OFFLINE:
		case rgm::RG_PENDING_OFFLINE:
		case rgm::RG_OFF_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_BOOT:
		case rgm::RG_OFF_BOOTED:
		case rgm::RG_OFFLINE_START_FAILED:
		default:
			continue;
		}
	}
	return (B_FALSE);
}


//
// is_rg_online_not_errored
// ------------------------
// Like is_rg_online_on_node above, except that it excludes error states
// RG_PENDING_OFF_START_FAILED, RG_PENDING_OFF_STOP_FAILED, and
// RG_ERROR_STOP_FAILED.
//
// If logical_node is false, rgp may be online on any base cluster zone in the
// same physical node as 'lni'; otherwise it must be online on 'lni' itself.
//
// Boolean variable count_pending_offline indicates whether to count
// PENDING_OFFLINE and PENDING_OFF_START_FAILED as online or offline.
//
// If 'error_node' is non-null, it contains a pointer to an lni_t.
// This indicates that we should check for STOP_FAILED error states:
// If the RG is found to be in PENDING_OFF_STOP_FAILED or ERROR_STOP_FAILED
// state on any of the checked zones, return B_FALSE but set *error_node
// to the LNI on which the RG is in the error state.
//
boolean_t
is_rg_online_not_errored(rglist_p_t rgp, rgm::lni_t lni,
    boolean_t count_pending_offline, boolean_t logical_node,
    rgm::lni_t *error_node)
{
	LogicalNodeset nset;

	if (logical_node) {
		nset.addLni(lni);
	} else {
		get_all_zones_on_phys_node(lni, nset);
	}

	rgm::lni_t currNode = 0;
	while ((currNode = nset.nextLniSet(currNode + 1)) != 0) {
		switch (rgp->rgl_xstate[currNode].rgx_state) {

		// "on-ish" states
		case rgm::RG_ONLINE:
		case rgm::RG_PENDING_ONLINE:
		case rgm::RG_PENDING_ON_STARTED:
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_ON_PENDING_MON_DISABLED:
		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_ON_PENDING_R_RESTART:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
			return (B_TRUE);

		// pending off states
		case rgm::RG_PENDING_OFFLINE:
		case rgm::RG_PENDING_OFF_START_FAILED:
			if (count_pending_offline) {
				return (B_TRUE);
			} else {
				continue;
			}

		// stop_failed error states
		case rgm::RG_PENDING_OFF_STOP_FAILED:
		case rgm::RG_ERROR_STOP_FAILED:
			if (error_node != NULL) {
				*error_node = currNode;
				return (B_FALSE);
			} else {
				continue;
			}

		// "off-ish" states
		case rgm::RG_OFFLINE:
		case rgm::RG_OFF_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_BOOT:
		case rgm::RG_OFF_BOOTED:
		case rgm::RG_OFFLINE_START_FAILED:
		default:
			continue;
		}
	}
	return (B_FALSE);
}


#ifndef EUROPA_FARM
//
// rg_on_or_r_restart
// Return B_TRUE if the RG is ONLINE, ON_PENDING_R_RESTART, or
// PENDING_ONLINE_BLOCKED on the given node; return B_FALSE otherwise.
// Used in contexts in which these three states are equivalently considered
// "online".
//
// The caller must already hold the RGM state lock.  If called on a
// non-president node, 'nodeid' must be the local node.
//
boolean_t
rg_on_or_r_restart(rglist_p_t rgp, rgm::lni_t lni)
{
	boolean_t retval = B_FALSE;
	sc_syslog_msg_handle_t handle;

	switch (rgp->rgl_xstate[lni].rgx_state) {

	// "online" states
	case rgm::RG_ONLINE:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
	case rgm::RG_ON_PENDING_R_RESTART:
		retval = B_TRUE;
		break;

	// "non-online" states
	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_PENDING_OFF_START_FAILED:
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
	case rgm::RG_ON_PENDING_METHODS:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_OFFLINE:
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_BOOT:
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_OFFLINE_START_FAILED:
		break;

	default:
		// This should not occur
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: %s: internal error: bad state <%d> for "
		    "resource group <%s>", "rg_on_or_r_restart",
		    rgp->rgl_xstate[lni].rgx_state,
		    rgp->rgl_ccr->rg_name);
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}
	return (retval);
}
#endif // !EUROPA_FARM


//
// is_rg1_dep_on_rg2
//
// Returns TRUE if rg2 is found in rg1's RG_dependencies list, or if rg1
// contains a scalable service resource which is dependent on a
// SharedAddress resource in rg2.
//
// Otherwise returns FALSE.
//
// Caller should check 'res' argument for error code before using the
// returned Boolean value.
//
boolean_t
is_rg1_dep_on_rg2(scha_errmsg_t *res, rgm_rg_t *rg1, rgm_rg_t *rg2)
{
	namelist_t *nl;
	namelist_t *dependee_rgs = NULL;
	boolean_t is_dep = B_FALSE;

	//
	// Compute the list of dependee RGs including RG_dependencies
	// plus "implict" dependencies of scalable service RG upon
	// SharedAddress RG.
	//
	rglist_t *rg;
	if ((rg = rgname_to_rg(rg1->rg_name)) != NULL) {
		dependee_rgs = add_sa_dependee_rgs_to_list(res, rg, NULL);
		if (res->err_code != SCHA_ERR_NOERR) {
			// error is NOMEM or internal -- fatal
			goto bailout; //lint !e801
		}
	}

	//
	// Iterate over the list of explicit & implicit dependee RGs
	//
	for (nl = dependee_rgs; nl != NULL; nl = nl->nl_next) {
		if (strcmp(nl->nl_name, rg2->rg_name) == 0)
			is_dep = B_TRUE;
			break;
	}
bailout:
	rgm_free_nlist(dependee_rgs);
	return (is_dep);
}


//
// Obtain value of Failover_mode property from resource structure
//
scha_errmsg_t
get_fo_mode(
	rgm_resource_t		*r,
	scha_failover_mode_t	*fo_mode)
{
	scha_prop_type_t	prop_type;
	std::map<rgm::lni_t, char *>	val_str;
	namelist_t		*val_array = NULL;
	rdeplist_t		*val_array_rdep = NULL;
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};
	boolean_t		need_free = B_FALSE;

	res = get_rs_prop_val(SCHA_FAILOVER_MODE, r, &prop_type, val_str,
	    &val_array, &need_free, &val_array_rdep);

	if (res.err_code != SCHA_ERR_NOERR) {
		goto finished; //lint !e801
	}
	if (prop_type != SCHA_PTYPE_STRING) {
		res.err_code = SCHA_ERR_PROP;
		goto finished; //lint !e801
	}

	if (strcmp(val_str[0], SCHA_NONE) == 0) {
		*fo_mode = SCHA_FOMODE_NONE;
	} else if (strcmp(val_str[0], SCHA_HARD) == 0) {
		*fo_mode = SCHA_FOMODE_HARD;
	} else if (strcmp(val_str[0], SCHA_SOFT) == 0) {
		*fo_mode = SCHA_FOMODE_SOFT;
	} else if (strcmp(val_str[0], SCHA_RESTART_ONLY) == 0) {
		*fo_mode = SCHA_FOMODE_RESTART_ONLY;
	} else if (strcmp(val_str[0], SCHA_LOG_ONLY) == 0) {
		*fo_mode = SCHA_FOMODE_LOG_ONLY;
	} else {
		ucmm_print("RGM",
		    NOGET("couldn't get Failover_mode for <%s>\n"), r->r_name);
		res.err_code = SCHA_ERR_PROP;
		goto finished; //lint !e801
	}

	res.err_code = SCHA_ERR_NOERR;
finished:

	/*
	 * If need_free is TRUE, need to free the buffer 'val_str'
	 */
	if (need_free) {
		if (val_str[0] != NULL) {
			free(val_str[0]);
		}
	}
	return (res);
}


//
// Obtain value of resource's Retry_interval property as a time_t.
// Return SCHA_ERR_RT if this property is not declared by the resource's type.
//
scha_errmsg_t
get_retry_interval(rlist_p_t rp, time_t *retry_int)
{
	rgm_resource_t	*r = rp->rl_ccrdata;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_prop_type_t	prop_type;
	std::map<rgm::lni_t, char *>	val_str;
	rdeplist_t		*val_array_rdep = NULL;
	namelist_t	*val_array = NULL;
	boolean_t	need_free = B_FALSE;

	// Fetch Retry_interval for this resource
	res = get_rs_prop_val(SCHA_RETRY_INTERVAL, r, &prop_type, val_str,
	    &val_array, &need_free, &val_array_rdep);
	//
	// Note, we ignore the value of need_free because we know that
	// Retry_interval value is not malloc'd by get_rs_prop_val.
	//

	//
	// If Retry_interval is not declared for the resource type, return
	// SCHA_ERR_RT to indicate "invalid resource type".
	//
	if (res.err_code == SCHA_ERR_PROP)
		res.err_code = SCHA_ERR_RT;
	if (res.err_code != SCHA_ERR_NOERR)
		goto finished; //lint !e801

	if (sscanf(val_str[0], "%d", retry_int) != 1) {
		/* Should not happen -- invalid Retry_interval value */
		ucmm_print("RGM",
		    NOGET("resource <%s> Retry_interval invalid"),
		    r->r_name);
		res.err_code = SCHA_ERR_RSRC;
	}
	ucmm_print("RGM", NOGET("resource <%s> Retry_interval=%d"), r->r_name,
	    *retry_int);

finished:
	return (res);
}


//
// Obtain value of resource's Retry_count property as a integer.
// Return SCHA_ERR_RT if this property is not declared by the resource's type.
//
scha_errmsg_t
get_retry_count(rlist_p_t rp, int *retry_count)
{
	rgm_resource_t	*r = rp->rl_ccrdata;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_prop_type_t	prop_type;
	std::map<rgm::lni_t, char *>	val_str;
	rdeplist_t	*val_array_rdep = NULL;
	namelist_t	*val_array = NULL;
	boolean_t	need_free = B_FALSE;

	// Fetch Retry_count for this resource
	res = get_rs_prop_val(SCHA_RETRY_COUNT, r, &prop_type, val_str,
	    &val_array, &need_free, &val_array_rdep);
	//
	// Note, we ignore the value of need_free because we know that
	// Retry_count value is not malloc'd by get_rs_prop_val.
	//

	//
	// If Retry_count is not declared for the resource type, return
	// SCHA_ERR_RT to indicate "invalid resource type".
	//
	if (res.err_code == SCHA_ERR_PROP)
		res.err_code = SCHA_ERR_RT;
	if (res.err_code != SCHA_ERR_NOERR)
		goto finished; //lint !e801

	if (sscanf(val_str[0], "%d", retry_count) != 1) {
		/* Should not happen -- invalid Retry_count value */
		ucmm_print("RGM",
		    NOGET("resource <%s> Retry_count invalid"),
		    r->r_name);
		res.err_code = SCHA_ERR_RSRC;
	}
	ucmm_print("RGM", NOGET("resource <%s> Retry_count=%d"), r->r_name,
	    *retry_count);

finished:
	return (res);
}

#ifndef EUROPA_FARM
static char*
rgm_nodeidzone_to_nodename(int nodeid, char *zonename) {

	char *nodename;

	nodename = rgm_get_nodename(nodeid);
	// check if nodename will ever be null
	if (nodename == NULL || zonename == NULL)
		return (nodename);

	nodename = (char *)realloc_err(nodename, strlen(nodename) +
	    strlen(zonename) + 2);
	strcat(nodename,  LN_DELIMITER_STR);
	strcat(nodename, zonename);
	return (nodename);
}

// also for nodename_fast
static char*
rgm_nodeidzone_to_nodename_cache(int nodeid, char *zonename)
{
	char *nodename;

	nodename = rgm_get_nodename_cache(nodeid);
	// check if nodename will ever be null
	if (nodename == NULL || zonename == NULL)
		return (nodename);

	nodename = (char *)realloc_err(nodename, strlen(nodename) +
	    strlen(zonename) + 2);
	strcat(nodename,  LN_DELIMITER_STR);
	strcat(nodename, zonename);
	return (nodename);
}


char *
rgm_nodeidlist_to_commastring(nodeidlist_t *l)
{
	uint_t		size = 0;
	nodeidlist_t	*nl;
	char		*ptr = 0;
	char		*nodename = NULL;

	if (l == NULL) {
		return (strdup(""));
	}

	// first calculate the length of the total string
	for (nl = l; nl != NULL; nl = nl->nl_next) {

		nodename = rgm_nodeidzone_to_nodename(nl->nl_nodeid,
		    nl->nl_zonename);
		CL_PANIC(nodename != NULL);

		// + 1 for separator comma
		size += (uint_t)strlen(nodename) + 1;

		// Note, realloc_err aborts rgmd on NOMEM error
		ptr = (char *)realloc_err(ptr, size);

		// initialize first char to 0 so that strcat() behaves
		// as strcpy() the first time.
		if (nl == l) {
			ptr[0] = '\0';
		} else {
			strcat(ptr, ",");
		}

		(void) strcat(ptr, nodename);

		free(nodename);

		// Append  "," except the last time.
	}
	return (ptr);
}
#endif


//
// Create a sequence of LN existing on the specified
// cluster node 'nodeid'. Called:
// - By president to let the joiners know about the various
//   LN on their nodes (joiners_read_ccr_v2())
// - By the slave, to return the list of LN on their nodes
//   to the president (rgm_comm_impl::idl_retrieve_lni_mappings())
//
void
create_sequence_lni_mappings(sol::nodeid_t nodeid,
    rgm::lni_mappings_seq_t **lni_mappings_out)
{
	LogicalNode		*clusterNode;
	rgm::lni_mappings_seq_t *lni_mappings;
	uint_t i = 0;
	std::list<LogicalNode*>::const_iterator it;

	clusterNode = lnManager->findObject(nodeid);
	CL_PANIC(clusterNode != NULL);

	ucmm_print("RGM", NOGET("create_sequence_lni_mappings for node %d "
	    "with %d non-global zones\n"), nodeid,
	    clusterNode->getLocalZones().size());

	lni_mappings = new rgm::lni_mappings_seq_t();
	lni_mappings->length(clusterNode->getLocalZones().size());

	for (i = 0, it = clusterNode->getLocalZones().begin(); it !=
	    clusterNode->getLocalZones().end(); ++it, ++i) {

		ucmm_print("RGM", NOGET("cslm: adding zone %s lni %d to "
		    "node %d\n"), (*it)->getZonename(), (*it)->getLni(),
		    nodeid);
		rgm::lni_mappings &currMapping = (*lni_mappings)[i];
		currMapping.lni = (*it)->getLni();

		currMapping.idlstr_zonename =
		    os::strdup((*it)->getZonename());
		currMapping.state = (*it)->getStatus();

		currMapping.incarn = (*it)->getIncarnation();
	}

	*lni_mappings_out = lni_mappings;
}


#ifndef EUROPA_FARM
//
// . Create Logical Node if non existent
// . Set zonename, nodename, and lni for the current nodeidlist obj
// Called by rgm president when creating the rgm_rg_t/rgm_rt_t in memory.
// Called by slave when reading RG/RT from the CCR.
//
// RGM lock is taken by callers
//	(idl_scrgadm_rt_create, ..rt_update, ..rg_create, ..rg_update)
//
// 'propagate' is set to TRUE when the lni mappings should be sent to
// the relevant slaves.
//
void
create_lni_mappings_from_nodeidlist(nodeidlist_t *nodeidlst,
    boolean_t propagate)
{
	sc_syslog_msg_handle_t	handle;
	rgm::rgm_comm_var	prgm_serv;
	Environment		e;
	LogicalNode		*lnNode;
	nodeidlist_t		*list;
	char			*ln = NULL;
	uint_t			*seq_len;
	rgm::lni_mappings_seq_t **lni_mappings;
	sol::nodeid_t		i;
	idlretval_t		retval;
	bool			is_farmnode;

	ucmm_print("RGM", NOGET("Create_lni_mappings_from_nodeidlist"));

	//
	// If we are executing on the president, reclaim unused LNIs.
	// Invoke reclaimOff() to prevent any new LNIs that we parse in this
	// nodelist from being reclaimed.
	// We need to reclaim the unused lni's now so that we do not use
	// any of the unused but assigned lni's while we are creating the
	// sequence below.
	//
	if (am_i_president()) {
		lnManager->reclaimOff();
	}

	seq_len = (uint_t *)malloc((LogicalNodeset::maxNode() + 1) *
	    sizeof (uint_t));
	lni_mappings = (rgm::lni_mappings_seq_t **)
	    malloc((LogicalNodeset::maxNode() + 1) *
	    sizeof (rgm::lni_mappings_seq_t *));

	bzero(seq_len, sizeof (uint_t) * (LogicalNodeset::maxNode() + 1));
	bzero(lni_mappings, sizeof (rgm::lni_mappings_seq_t *) *
	    (LogicalNodeset::maxNode() + 1));

	if (propagate) {
		//
		// Go through Nodelist to figure out the length of each
		// sequence.
		// Store length for each physical node in seq_len[] array.
		//
		for (list = nodeidlst; list != NULL; list = list->nl_next) {
			//
			// Lookup for LogicalNode.
			//
			lnNode = lnManager->findLogicalNode(list->nl_nodeid,
			    list->nl_zonename, NULL);

			// we only send mapping info for new LNs on nodes
			// other than our own
			if ((NULL != lnNode) ||
			    (get_local_nodeid() == list->nl_nodeid)) {
				continue;
			}
			seq_len[list->nl_nodeid]++;
		}
		//
		// Allocate memory for each sequence
		// and set the length of the sequence.
		//
		for (i = 1; i <= LogicalNodeset::maxNode(); i++) {
			if (seq_len[i] != 0) {
				lni_mappings[i] =
				    new rgm::lni_mappings_seq_t();
				lni_mappings[i]->length(seq_len[i]);
				// Reset it to 0, so we can use it later.
				seq_len[i] = 0;
			}
		}
	}


	// Loop through nodelist:
	// . Allocate new LogicalNode if needed
	// . Add entry for LNI in the nodeidlist
	//
	for (list = nodeidlst; list != NULL; list = list->nl_next) {
		//
		// Lookup for LogicalNode.
		//
		lnNode = lnManager->findLogicalNode(list->nl_nodeid,
		    list->nl_zonename, &ln);
		if (NULL != lnNode) {
			//
			// LN is already known to this node.
			// We do not need to add it or send its
			// mapping to the slave (the slave should already
			// know about it too).
			// Fill LNI for nodeidlist and continue.
			//
			list->nl_lni = lnNode->getLni();
			continue;
		}

		//
		// We should not reach this point if list->nl_zonename is
		// NULL, because LogicalNodes for all physical node nodeids
		// should have been pre-allocated.
		//
		// However, as a precautionary measure, make sure we don't
		// attempt to send an lni mapping for a physical node.
		//
		if (list->nl_zonename == NULL) {
			ucmm_print("create_lni_mappings_from_nodeidlist",
			    NOGET("Missing LogicalNode for nodeid %d!"),
			    list->nl_nodeid);
			continue;
		}

		//
		// No such LogicalNode, create it with status
		// UNCONFIGURED. If it were UP or DOWN, we would know
		// about it.
		//

		lnNode = lnManager->createLogicalNode(list->nl_nodeid,
		    list->nl_zonename, rgm::LN_UNCONFIGURED, ln);
		// assign lni to the nodeidlist element
		list->nl_lni = lnNode->getLni();
		free(ln);
		ln = NULL;

		//
		// Fill the new sequence element
		//
		if (propagate && list->nl_nodeid != get_local_nodeid()) {
			CL_PANIC(lni_mappings[list->nl_nodeid] != NULL);
			rgm::lni_mappings &currMapping =
			    (*lni_mappings[list->nl_nodeid])[
			    seq_len[list->nl_nodeid]];
			currMapping.lni = lnNode->getLni();
			currMapping.idlstr_zonename =
			    os::strdup(lnNode->getZonename());
			currMapping.state = rgm::LN_UNCONFIGURED;
			seq_len[list->nl_nodeid]++;
		}
	}

	//
	// If executing on the president, re-enable LNI reclaims which we
	// disabled above.
	//
	if (am_i_president()) {
		lnManager->reclaimOn();
	}

	//
	// Send the sequences
	//
	if (propagate) {
		for (i = 1; i <= LogicalNodeset::maxNode(); i++) {
			if (seq_len[i] != 0) {
				if ((rgmx_hook_call(rgmx_hook_send_lni_mappings,
				    i, lni_mappings[i], &retval,
				    &is_farmnode) == RGMX_HOOK_DISABLED) ||
				    (!is_farmnode)) {
					prgm_serv = rgm_comm_getref(i);
					if (CORBA::is_nil(prgm_serv)) {
						// node is dead
						continue;
					}
					prgm_serv->idl_send_lni_mappings(
					    get_local_nodeid(),
					    Rgm_state->node_incarnations.
					    members[get_local_nodeid()],
					    *lni_mappings[i], e);
					retval = except_to_errcode(e, i);
				}

				delete(lni_mappings[i]);

				switch (retval) {
				case RGMIDL_NODE_DEATH:
				case RGMIDL_RGMD_DEATH:
					// node has died; proceed to next node
				case RGMIDL_OK:
					break;

				case RGMIDL_UNKNOWN:
				case RGMIDL_NOREF:
				default:
					//
					// Crash and burn.
					//
					(void) sc_syslog_msg_initialize(&handle,
					    SC_SYSLOG_RGM_RGMD_TAG, "");
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "fatal: cannot send mappings to "
					    "remote nodes");
					sc_syslog_msg_done(&handle);
					abort();
					// NOTREACHED
				}
			}
		}
	}
	lnManager->debug_print();

	free(ln);
	free(seq_len);
	free(lni_mappings);
}
#endif // !EUROPA_FARM




//
// Convert a nodeidlist_t to a nodeidseq.
// nlst must be non-NULL.  Allocate the memory for *nlst.
// Caller must free *nlst unless an error is returned.
//
void
nodeidlist_to_nodeidseq(nodeidlist_t *nidlst, rgm::lni_seq_t &nodeidseq)
{
	nodeidlist_t *nidl;
	scha_err_t errcode = SCHA_ERR_NOERR;
	uint_t i = 0;

	for (nidl = nidlst; nidl != NULL; nidl = nidl->nl_next) {
		i ++;
	}

	nodeidseq.length(i);
	i = 0;

	for (nidl = nidlst; nidl != NULL; nidl = nidl->nl_next) {
		nodeidseq[i++] = nidl->nl_nodeid;
	}
}


//
// Convert a nodeidlist_t to a namelist_t.
// nlst must be non-NULL.  Allocate the memory for *nlst.
// Caller must free *nlst unless an error is returned.
//
scha_err_t
nodeidlist_to_namelist(nodeidlist_t *nidlst, namelist_t **nlst)
{
	nodeidlist_t *nidl;
	namelist_t *nl;
	namelist_t *prev = NULL;
	char *nodenamep = NULL;
	scha_err_t errcode = SCHA_ERR_NOERR;

	ASSERT(nlst);
	*nlst = NULL;
	for (nidl = nidlst; nidl != NULL; nidl = nidl->nl_next) {

		nl = (namelist_t *)calloc_nocheck(1, sizeof (namelist_t));
		if (nl == NULL) {
			errcode = SCHA_ERR_NOMEM;
			goto bailout; //lint !e801
		}

		nodenamep = rgm_get_nodename_cache(nidl->nl_nodeid);
		if (NULL == nodenamep) {
			free(nl);
			errcode = SCHA_ERR_NODE;
			goto bailout; //lint !e801
		}
		if (nidl->nl_zonename) {
			nl->nl_name = (char *)malloc(strlen(nodenamep) + 1 +
			    strlen(nidl->nl_zonename) + 1);
			sprintf(nl->nl_name, "%s:%s", nodenamep,
			    nidl->nl_zonename);
		} else {
			nl->nl_name = (char *)malloc(strlen(nodenamep) + 1);
			sprintf(nl->nl_name, "%s", nodenamep);
		}

		free(nodenamep);
		nodenamep = NULL;

		if (*nlst == NULL) {
			*nlst = nl;
		} else {
			prev->nl_next = nl;
		}
		prev = nl;
	}
	return (SCHA_ERR_NOERR);

bailout:
	rgm_free_nlist(*nlst);
	*nlst = NULL;
	return (errcode);
}



#ifndef EUROPA_FARM
//
//
// Convert a rgm_resource_t to idl_string_seq_t
//
void
r_to_strseq(rgm_resource_t *rsrc, rgm::idl_string_seq_t &props,
    rgm::idl_string_seq_t &xprops, rgm::idl_string_seq_t &all_nodes_xprops,
    LogicalNodeset *ns)
{
	char	*val;
	rgm_property_list_t *p;
	rgm_property_t *ptr;
	uint_t i;

	if (rsrc == NULL) {
		props = NULL;
		xprops = NULL;
		all_nodes_xprops = NULL;
		return;
	}

	//
	// Compute the length of the system-defined properties:
	// project_name, strong_depend, weak_depend, restart_depend,
	// Type_version
	//
	i = 10;
	for (p = rsrc->r_properties; p != 0; p = p->rpl_next)
		i++;

	// Create a sequence of length i of system defined properties
	props = rgm::idl_string_seq_t(i);
	props.length(i);

	// Fill in the sequence
	i = 0;
	val = rdeplist_to_str(rsrc->r_dependencies.dp_strong);
	// 2 is for '=' and '\0'
	props[i] = new char[strlen(SCHA_RESOURCE_DEPENDENCIES)
	    + strlen(val) + 2];
	(void) sprintf(props[i++], "%s=%s", SCHA_RESOURCE_DEPENDENCIES, val);
	free(val);

	val = rdeplist_to_str(rsrc->r_dependencies.dp_weak);
	// 2 is for '=' and '\0'
	props[i] = new char[strlen(SCHA_RESOURCE_DEPENDENCIES_WEAK)
	    + strlen(val) + 2];
	(void) sprintf(props[i++], "%s=%s", SCHA_RESOURCE_DEPENDENCIES_WEAK,
	    val);
	free(val);

	val = rdeplist_to_str(rsrc->r_dependencies.dp_restart);
	// 2 is for '=' and '\0'
	props[i] = new char[strlen(SCHA_RESOURCE_DEPENDENCIES_RESTART)
	    + strlen(val) + 2];
	(void) sprintf(props[i++], "%s=%s", SCHA_RESOURCE_DEPENDENCIES_RESTART,
	    val);
	free(val);

	val = rdeplist_to_str(rsrc->r_dependencies.dp_offline_restart);
	// 2 is for '=' and '\0'
	props[i] = new char[strlen(SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART)
	    + strlen(val) + 2];
	(void) sprintf(props[i++], "%s=%s",
	    SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART, val);
	free(val);

	val = rdeplistQ_to_str(rsrc->r_dependencies.dp_strong);
	// 2 is for '=' and '\0'
	props[i] = new char[strlen(SCHA_RESOURCE_DEPENDENCIES_Q)
	    + strlen(val) + 2];
	(void) sprintf(props[i++], "%s=%s", SCHA_RESOURCE_DEPENDENCIES_Q, val);
	free(val);

	val = rdeplistQ_to_str(rsrc->r_dependencies.dp_weak);
	// 2 is for '=' and '\0'
	props[i] = new char[strlen(SCHA_RESOURCE_DEPENDENCIES_Q_WEAK)
	    + strlen(val) + 2];
	(void) sprintf(props[i++], "%s=%s", SCHA_RESOURCE_DEPENDENCIES_Q_WEAK,
	    val);
	free(val);

	val = rdeplistQ_to_str(rsrc->r_dependencies.dp_restart);
	// 2 is for '=' and '\0'
	props[i] = new char[strlen(SCHA_RESOURCE_DEPENDENCIES_Q_RESTART)
	    + strlen(val) + 2];
	(void) sprintf(props[i++], "%s=%s",
	    SCHA_RESOURCE_DEPENDENCIES_Q_RESTART, val);
	free(val);

	val = rdeplistQ_to_str(rsrc->r_dependencies.dp_offline_restart);
	// 2 is for '=' and '\0'
	props[i] = new char[strlen(SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART)
	    + strlen(val) + 2];
	(void) sprintf(props[i++], "%s=%s",
	    SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART, val);
	free(val);

	// 2 is for '=' and '\0'
	props[i] = new char[strlen(SCHA_RESOURCE_PROJECT_NAME)
	    + strlen(rsrc->r_project_name) + 2];
	(void) sprintf(props[i++], "%s=%s", SCHA_RESOURCE_PROJECT_NAME,
	    rsrc->r_project_name);

	// 2 is for '=' and '\0'
	props[i] = new char[strlen(SCHA_TYPE_VERSION)
	    + strlen(rsrc->r_type_version) + 2];
	(void) sprintf(props[i++], "%s=%s", SCHA_TYPE_VERSION,
	    rsrc->r_type_version);

	for (p = rsrc->r_properties; p != 0; p = p->rpl_next) {
		ptr = p->rpl_property;
		if (ptr->rp_type == SCHA_PTYPE_STRINGARRAY) {
			//
			// Compute the NRU from dependency lists  if NRU value
			// is NULL or if NRU is an empty string.
			//
			if ((strcasecmp(SCHA_NETWORK_RESOURCES_USED,
			    ptr->rp_key) == 0) &&
			    ((ptr->rp_array_values == NULL) || (strcmp(
			    namelist_to_str(ptr->rp_array_values), "") == 0))) {
				namelist_t *nru_list = NULL;
				compute_NRUlst_frm_deplist(
				    &(rsrc->r_dependencies), &nru_list,
				    B_FALSE);
				ptr->rp_array_values = nru_list;
			}
			val = namelist_to_str(ptr->rp_array_values);
			// 2 is for '=' and '\0'
			props[i] = new char[strlen(ptr->rp_key)
			    + strlen(val) + 2];
			(void) sprintf(props[i], "%s=%s", ptr->rp_key, val);
			free(val);
		} else {
			if (((rgm_value_t *)ptr->rp_value)
			    ->rp_value[0] == NULL) {
				// 2 is for '=' and '\0'
				props[i] = new char[strlen(ptr->rp_key) + 2];
				(void) sprintf(props[i], "%s=", ptr->rp_key);
			} else {
				// 2 is for '=' and '\0'
				props[i] = new char[strlen(ptr->rp_key)
				    + strlen(((rgm_value_t *)ptr->rp_value)
				    ->rp_value[0]) + 2];
				(void) sprintf(props[i], "%s=%s",
				    ptr->rp_key, ((rgm_value_t *)ptr->rp_value)
				    ->rp_value[0]);
			}
		}
		i++;
	}

	// Fill in the sequence
	i = 0;
	for (p = rsrc->r_ext_properties; p != 0; p = p->rpl_next) {
		ptr = p->rpl_property;
		if (ptr->rp_type == SCHA_PTYPE_STRINGARRAY) {
			val = namelist_to_str(ptr->rp_array_values);
			// 2 is for '=' and '\0'
			xprops.length(i + 1);
			xprops[i] = new char[strlen(ptr->rp_key)
			    + strlen(val) + 2];
			(void) sprintf(xprops[i], "%s=%s", ptr->rp_key, val);
			free(val);
		} else {
			if (ptr->is_per_node)
				continue;
			if (((rgm_value_t *)ptr->rp_value)->rp_value[0]
			    == NULL) {
				// 2 is for '=' and '\0'
				xprops.length(i + 1);
				xprops[i] = new char[strlen(ptr->rp_key) + 2];
				(void) sprintf(xprops[i], "%s=", ptr->rp_key);
			} else {
				xprops.length(i + 1);
				xprops[i] = new char[strlen(ptr->rp_key)
				    + strlen(((rgm_value_t *)ptr->rp_value)
				    ->rp_value[0]) + 2];
				(void) sprintf(xprops[i], "%s=%s",
				    ptr->rp_key, ((rgm_value_t *)ptr->rp_value)
				    ->rp_value[0]);
			}
		}
		i++;
	}
}


//
// Convert the rg properties from rgm_rg_t to idl_string_seq_t
//  20 rg properties: rg_nodelist, rg_max_primaries, rg_desired_primaries,
//  rg_failback, rg_dependencies, rg_glb_rsrcused, rg_mode,
//  rg_impl_net_depend, rg_pathprefix, rg_description, rg_ppinterval,
//  rg_project_name, rg_affinities, rg_auto_start, rg_system,
//  rg_slm_type, rgm_slm_pset_type, rgm_slm_cpu, rgm_slm_cpu_min,
//  rg_affinitents.
//
void
rg_to_strseq(rgm_rg_t *rg, rgm::idl_string_seq_t &props)
{
	char	*val;
	uint_t len = 20;

	if (rg == NULL) {
		props = 0;
		return;
	}

	// Create a sequence of length 'len' of system defined properties
	props = rgm::idl_string_seq_t(len);
	props.length(len);

	// Fill in the sequence
	val = rgm_nodeidlist_to_commastring(rg->rg_nodelist);
	// 2 is for '=' and '\0'
	props[0] = new char[strlen(SCHA_NODELIST) + strlen(val) + 2];
	CL_PANIC((char *)props[0] != NULL);
	(void) sprintf(props[0], "%s=%s", SCHA_NODELIST, val);
	free(val);

	props[1] = new char[strlen(SCHA_MAXIMUM_PRIMARIES)
	    + LONG_INT_PRINT_WIDTH + 2];
	CL_PANIC((char *)props[1] != NULL);
	(void) sprintf(props[1], "%s=%d", SCHA_MAXIMUM_PRIMARIES,
	    rg->rg_max_primaries);

	props[2] = new char[strlen(SCHA_DESIRED_PRIMARIES)
	    + LONG_INT_PRINT_WIDTH + 2];
	CL_PANIC((char *)props[2] != NULL);
	(void) sprintf(props[2], "%s=%d", SCHA_DESIRED_PRIMARIES,
	    rg->rg_desired_primaries);

	if (rg->rg_failback) {
		// 6 is for "=TRUE" and '\0'
		props[3] = new char[strlen(SCHA_FAILBACK) + 6];
		CL_PANIC((char *)props[3] != NULL);
		(void) sprintf(props[3], "%s=TRUE", SCHA_FAILBACK);
	} else {
		// 7 is for "=FALSE" and '\0'
		props[3] = new char[strlen(SCHA_FAILBACK) + 7];
		CL_PANIC((char *)props[3] != NULL);
		(void) sprintf(props[3], "%s=FALSE", SCHA_FAILBACK);
	}

	val = namelist_to_str(rg->rg_dependencies);
	props[4] = new char[strlen(SCHA_RG_DEPENDENCIES) + strlen(val) + 2];
	CL_PANIC((char *)props[4] != NULL);
	(void) sprintf(props[4], "%s=%s", SCHA_RG_DEPENDENCIES, val);
	free(val);

	val = namelist_all_to_str(&rg->rg_glb_rsrcused);
	props[5] = new char[strlen(SCHA_GLOBAL_RESOURCES_USED)
	    + strlen(val) + 2];
	CL_PANIC((char *)props[5] != NULL);
	(void) sprintf(props[5], "%s=%s", SCHA_GLOBAL_RESOURCES_USED, val);
	free(val);

	props[6] = new char[strlen(SCHA_RG_MODE)
	    + strlen(rgmode2str(rg->rg_mode)) + 2];
	CL_PANIC((char *)props[6] != NULL);
	(void) sprintf(props[6], "%s=%s", SCHA_RG_MODE,
	    rgmode2str(rg->rg_mode));

	if (rg->rg_impl_net_depend) {
		// 6 is for "=TRUE" and '\0'
		props[7] = new char[strlen(SCHA_IMPL_NET_DEPEND) + 6];
		CL_PANIC((char *)props[7] != NULL);
		(void) sprintf(props[7], "%s=TRUE", SCHA_IMPL_NET_DEPEND);
	} else {
		// 7 is for "=FALSE" and '\0'
		props[7] = new char[strlen(SCHA_IMPL_NET_DEPEND) + 7];
		CL_PANIC((char *)props[7] != NULL);
		(void) sprintf(props[7], "%s=FALSE", SCHA_IMPL_NET_DEPEND);
	}

	props[8] = new char[strlen(SCHA_PATHPREFIX)
	    + strlen(rg->rg_pathprefix) + 2];
	CL_PANIC((char *)props[8] != NULL);
	(void) sprintf(props[8], "%s=%s", SCHA_PATHPREFIX, rg->rg_pathprefix);

	props[9] = new char[strlen(SCHA_RG_DESCRIPTION)
	    + strlen(rg->rg_description) + 2];
	CL_PANIC((char *)props[9] != NULL);
	(void) sprintf(props[9], "%s=%s", SCHA_RG_DESCRIPTION,
	    rg->rg_description);

	props[10] = new char[strlen(SCHA_PINGPONG_INTERVAL) +
	    LONG_INT_PRINT_WIDTH + 2];
	CL_PANIC((char *)props[10] != NULL);
	(void) sprintf(props[10], "%s=%d", SCHA_PINGPONG_INTERVAL,
	    rg->rg_ppinterval);

	props[11] = new char[strlen(SCHA_RG_PROJECT_NAME) +
	    strlen(rg->rg_project_name) + 2];
	CL_PANIC((char *)props[11] != NULL);
	(void) sprintf(props[11], "%s=%s", SCHA_RG_PROJECT_NAME,
	    rg->rg_project_name);

	val = namelist_to_str(rg->rg_affinities);
	props[12] = new char[strlen(SCHA_RG_AFFINITIES) + strlen(val) + 2];
	CL_PANIC((char *)props[12] != NULL);
	(void) sprintf(props[12], "%s=%s", SCHA_RG_AFFINITIES, val);
	free(val);

	if (rg->rg_auto_start) {
		// 6 is for "=TRUE" and '\0'
		props[13] = new char[strlen(SCHA_RG_AUTO_START) + 6];
		CL_PANIC((char *)props[13] != NULL);
		(void) sprintf(props[13], "%s=TRUE", SCHA_RG_AUTO_START);
	} else {
		// 7 is for "=FALSE" and '\0'
		props[13] = new char[strlen(SCHA_RG_AUTO_START) + 7];
		CL_PANIC((char *)props[13] != NULL);
		(void) sprintf(props[13], "%s=FALSE", SCHA_RG_AUTO_START);
	}

	//
	// RG_System Property
	//
	if (rg->rg_system) {
		// 6 is for "=TRUE" and '\0'
		props[14] = new char[strlen(SCHA_RG_SYSTEM) + 6];
		CL_PANIC((char *)props[14] != NULL);
		(void) sprintf(props[14], "%s=TRUE", SCHA_RG_SYSTEM);
	} else {
		// 7 is for "=FALSE" and '\0'
		props[14] = new char[strlen(SCHA_RG_SYSTEM) + 7];
		CL_PANIC((char *)props[14] != NULL);
		(void) sprintf(props[14], "%s=FALSE", SCHA_RG_SYSTEM);
	}

// SC SLM addon start
	props[15] = new char[strlen(SCHA_RG_SLM_TYPE) +
	    strlen(rg->rg_slm_type) + 2];
	CL_PANIC((char *)props[15] != NULL);
	(void) sprintf(props[15], "%s=%s", SCHA_RG_SLM_TYPE,
	    rg->rg_slm_type);

	props[16] = new char[strlen(SCHA_RG_SLM_PSET_TYPE) +
	    strlen(rg->rg_slm_pset_type) + 2];
	CL_PANIC((char *)props[16] != NULL);
	(void) sprintf(props[16], "%s=%s", SCHA_RG_SLM_PSET_TYPE,
	    rg->rg_slm_pset_type);

	props[17] = new char[strlen(SCHA_RG_SLM_CPU_SHARES)
	    + LONG_INT_PRINT_WIDTH + 2];
	CL_PANIC((char *)props[17] != NULL);
	(void) sprintf(props[17], "%s=%d", SCHA_RG_SLM_CPU_SHARES,
	    rg->rg_slm_cpu);

	props[18] = new char[strlen(SCHA_RG_SLM_PSET_MIN) +
	    + LONG_INT_PRINT_WIDTH + 2];
	CL_PANIC((char *)props[18] != NULL);
	(void) sprintf(props[18], "%s=%d", SCHA_RG_SLM_PSET_MIN,
	    rg->rg_slm_cpu_min);
// SC SLM addon end
	val = namelist_to_str(rg->rg_affinitents);
	props[19] = new char[strlen(SCHA_IC_RG_AFFINITY_SOURCES) +
	    strlen(val) + 2];
	CL_PANIC((char *)props[19] != NULL);
	(void) sprintf(props[19], "%s=%s", SCHA_IC_RG_AFFINITY_SOURCES, val);
	free(val);

}


//
// similar to rg_to_strseq, but only includes properties that
// are allowed to be updated, and only if they have changed:
//   rg_nodelist, rg_max_primaries, rg_desired_primaries,
//   rg_failback, rg_impl_net_depend, rg_dependencies,
//   rg_glb_rsrcused, rg_description, rg_pathprefix, rg_ppinterval,
//   rg_system, rg_slm_type, rg_slm_pset_type, rg_slm_cpu, rg_slm_cpu_min
// As a result, props has variable length.
// If you add any new properties make sure you add them after incrementing
// the index counter ix.
//
void
update_rg_to_strseq(rgm_rg_t *rg, rgm::idl_string_seq_t &props,
    changed_rg_prop *changed)
{
	char	*val;
	uint_t	ix = 0, i = 0;

	if (rg == NULL) {
		props = 0;
		return;
	}

	// Create a sequence of length i of system defined properties
	props = rgm::idl_string_seq_t(changed->len);
	props.length(changed->len);

	// Fill in the sequence
	if (changed->rg_nodelist) {
		val = rgm_nodeidlist_to_commastring(rg->rg_nodelist);
		props[ix] = new char[strlen(SCHA_NODELIST) + strlen(val) + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++], "%s=%s", SCHA_NODELIST, val);
		free(val);
	}

	if (changed->rg_max_primaries) {
		props[ix] = new char[strlen(SCHA_MAXIMUM_PRIMARIES) +
		    LONG_INT_PRINT_WIDTH + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++], "%s=%d", SCHA_MAXIMUM_PRIMARIES,
		    rg->rg_max_primaries);
	}

	if (changed->rg_desired_primaries) {
		props[ix] = new char[strlen(SCHA_DESIRED_PRIMARIES) +
		    LONG_INT_PRINT_WIDTH + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++], "%s=%d", SCHA_DESIRED_PRIMARIES,
		    rg->rg_desired_primaries);
	}

	if (changed->rg_impl_net_depend) {
		if (rg->rg_impl_net_depend) {
			// 6 is for "=TRUE" and '\0'
			props[ix] = new char[strlen(SCHA_IMPL_NET_DEPEND) + 6];
			CL_PANIC((char *)props[ix] != NULL);
			(void) sprintf(props[ix++],
			    "%s=TRUE", SCHA_IMPL_NET_DEPEND);
		} else {
			// 7 is for "=FALSE" and '\0'
			props[ix] = new char[strlen(SCHA_IMPL_NET_DEPEND) + 7];
			CL_PANIC((char *)props[ix] != NULL);
			(void) sprintf(props[ix++],
			    "%s=FALSE", SCHA_IMPL_NET_DEPEND);
		}
	}

	if (changed->rg_failback) {
		if (rg->rg_failback) {
			// 6 is for "=TRUE" and '\0'
			props[ix] = new char[strlen(SCHA_FAILBACK) + 6];
			CL_PANIC((char *)props[ix] != NULL);
			(void) sprintf(props[ix++], "%s=TRUE", SCHA_FAILBACK);
		} else {
			// 7 is for "=FALSE" and '\0'
			props[ix] = new char[strlen(SCHA_FAILBACK) + 7];
			CL_PANIC((char *)props[ix] != NULL);
			(void) sprintf(props[ix++], "%s=FALSE", SCHA_FAILBACK);
		}
	}

	//
	// RG_System Property
	//
	if (changed->rg_system) {
		if (rg->rg_system) {
			// 6 is for "=TRUE" and '\0'
			props[ix] = new char[strlen(SCHA_RG_SYSTEM) + 6];
			CL_PANIC((char *)props[ix] != NULL);
			(void) sprintf(props[ix++], "%s=TRUE", SCHA_RG_SYSTEM);
		} else {
			// 7 is for "=FALSE" and '\0'
			props[ix] = new char[strlen(SCHA_RG_SYSTEM) + 7];
			CL_PANIC((char *)props[ix] != NULL);
			(void) sprintf(props[ix++], "%s=FALSE", SCHA_RG_SYSTEM);
		}
	}

	if (changed->rg_project_name) {
		props[ix] = new char[strlen(SCHA_RG_PROJECT_NAME)
		    + strlen(rg->rg_project_name) + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++], "%s=%s", SCHA_RG_PROJECT_NAME,
		    rg->rg_project_name);
	}

	if (changed->rg_affinities) {
		val = namelist_to_str(rg->rg_affinities);
		props[ix] = new char[strlen(SCHA_RG_AFFINITIES)
		    + strlen(val) + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++],
		    "%s=%s", SCHA_RG_AFFINITIES, val);
		free(val);
	}

	if (changed->rg_affinitents) {
		val = namelist_to_str(rg->rg_affinitents);
		props[ix] = new char[strlen(SCHA_IC_RG_AFFINITY_SOURCES)
		    + strlen(val) + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++],
		    "%s=%s", SCHA_IC_RG_AFFINITY_SOURCES, val);
		free(val);
	}

	if (changed->rg_auto_start) {
		if (rg->rg_auto_start) {
			// 6 is for "=TRUE" and '\0'
			props[ix] = new char[strlen(SCHA_RG_AUTO_START) + 6];
			CL_PANIC((char *)props[ix] != NULL);
			(void) sprintf(props[ix++], "%s=TRUE",
			    SCHA_RG_AUTO_START);
		} else {
			// 7 is for "=FALSE" and '\0'
			props[ix] = new char[strlen(SCHA_RG_AUTO_START) + 7];
			CL_PANIC((char *)props[ix] != NULL);
			(void) sprintf(props[ix++], "%s=FALSE",
			    SCHA_RG_AUTO_START);
		}
	}

	if (changed->rg_dependencies) {
		val = namelist_to_str(rg->rg_dependencies);
		props[ix] = new char[strlen(SCHA_RG_DEPENDENCIES)
		    + strlen(val) + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++],
		    "%s=%s", SCHA_RG_DEPENDENCIES, val);
		free(val);
	}

	if (changed->rg_glb_rsrcused) {
		val = namelist_all_to_str(&rg->rg_glb_rsrcused);
		props[ix] = new char[strlen(SCHA_GLOBAL_RESOURCES_USED)
		    + strlen(val) + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++],
		    "%s=%s", SCHA_GLOBAL_RESOURCES_USED, val);
		free(val);
	}

	if (changed->rg_description) {
		props[ix] = new char[strlen(SCHA_RG_DESCRIPTION)
		    + strlen(rg->rg_description) + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++], "%s=%s", SCHA_RG_DESCRIPTION,
		    rg->rg_description);
	}

	if (changed->rg_pathprefix) {
		props[ix] = new char[strlen(SCHA_PATHPREFIX)
		    + strlen(rg->rg_pathprefix) + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++], "%s=%s", SCHA_PATHPREFIX,
		    rg->rg_pathprefix);
	}

	if (changed->rg_ppinterval) {
		props[ix] = new char[strlen(SCHA_PINGPONG_INTERVAL) +
		    LONG_INT_PRINT_WIDTH + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++], "%s=%d", SCHA_PINGPONG_INTERVAL,
		    rg->rg_ppinterval);
	}

// SC SLM addon start
	if (changed->rg_slm_type) {
		props[ix] = new char[strlen(SCHA_RG_SLM_TYPE)
		    + strlen(rg->rg_slm_type) + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++], "%s=%s", SCHA_RG_SLM_TYPE,
		    rg->rg_slm_type);
	}

	if (changed->rg_slm_pset_type) {
		props[ix] = new char[strlen(SCHA_RG_SLM_PSET_TYPE)
		    + strlen(rg->rg_slm_pset_type) + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++], "%s=%s", SCHA_RG_SLM_PSET_TYPE,
		    rg->rg_slm_pset_type);
	}

	if (changed->rg_slm_cpu) {
		props[ix] = new char[strlen(SCHA_RG_SLM_CPU_SHARES) +
		    LONG_INT_PRINT_WIDTH + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++], "%s=%d", SCHA_RG_SLM_CPU_SHARES,
		    rg->rg_slm_cpu);
	}

	if (changed->rg_slm_cpu_min) {
		props[ix] = new char[strlen(SCHA_RG_SLM_PSET_MIN) +
		    LONG_INT_PRINT_WIDTH + 2];
		CL_PANIC((char *)props[ix] != NULL);
		(void) sprintf(props[ix++], "%s=%d", SCHA_RG_SLM_PSET_MIN,
		    rg->rg_slm_cpu_min);
	}
// SC SLM addon end

	if (ix == 0)
		return;

	ucmm_print("RGM",
	    NOGET("update_rg_to_strseq changed: %d %d %d %d %d %d %d %d "
	    " %d %d %d %d %d %d\n"),
	    changed->rg_nodelist,
	    changed->rg_max_primaries,
	    changed->rg_desired_primaries,
	    changed->rg_failback,
	    changed->rg_auto_start,
	    changed->rg_dependencies,
	    changed->rg_glb_rsrcused,
	    changed->rg_description,
	    changed->rg_pathprefix,
	    changed->rg_ppinterval,
	    changed->rg_slm_type,
	    changed->rg_slm_pset_type,
	    changed->rg_slm_cpu,
	    changed->rg_slm_cpu_min);
	for (i = 0; i < ix; i++)
		ucmm_print("RGM",
		    NOGET("update_rg_to_strseq props[%d]: %s\n"),
		    i, (char *)props[i]);

}

//
// compute_NRU_and_add
//
// Computes the NRU list and adds it to the validation args
// only if there is a change in NRU value due to updation in
// any of the resource dependencies value. The fourth argument
// triggered_by_dep_change is TRUE if this function is called
// due to change in any of the resource dependencies. This value
// will be false if this function is called due to updation of
// NRU with empty string value.
//
static void
compute_NRU_and_add(rgm::idl_string_seq_t &str_seq, uint_t i,
    rgm_resource_t *rs, boolean_t triggered_by_dep_change)
{
	char *buff = NULL, *s = NULL;
	namelist_t *nrulist = NULL;
	namelist_t *orignrulist = NULL;
	rgm_resource_t *rs_original = NULL;
	scha_errmsg_t   res = {SCHA_ERR_NOERR, NULL};

	// Compute the NRU from dependency list.
	compute_NRUlst_frm_deplist(&(rs->r_dependencies), &nrulist, B_FALSE);

	//
	// Check if this is called due to change in any of
	// resource dependencies.
	//
	if (triggered_by_dep_change) {
		// Get the existing values for NRU.
		if ((res = rgmcnfg_get_resource_no_defaults(rs->r_name,
		    rs->r_rgname,
		    &rs_original, ZONE)).err_code != SCHA_ERR_NOERR) {
			rgm_format_errmsg(&res,
			    "Internal error: Could not read CCR.");
			goto finished; //lint !e801
		}
		// Compute the NRU from dependency list.
		compute_NRUlst_frm_deplist(&(rs_original->r_dependencies),
		    &orignrulist, B_FALSE);
		//
		// Compare the existing list with the new list.
		// If there is a change in the list then we need
		// to add NRU to validation args.
		//
		if (!namelist_compare(orignrulist, nrulist)) {
			// Increment the length to accomodate NRU.
			str_seq.length(i + 1);
		} else {
			goto finished; //lint !e801
		}
	}

	// Check if NRU is NULL value.
	if (!nrulist) {
		s = new char[
		    strlen(SCHA_NETWORK_RESOURCES_USED)
		    + 2];
		CL_PANIC(s != NULL);
		(void) sprintf(s, "%s=",
		    SCHA_NETWORK_RESOURCES_USED);
		str_seq[i] = s;
	} else {
		buff = namelist_to_str(nrulist);
		CL_PANIC(buff != NULL);
		s = new char[strlen
		    (SCHA_NETWORK_RESOURCES_USED) +
		    strlen(buff) + 2];
		CL_PANIC(s != NULL);
		(void) sprintf(s, "%s=%s",
		    SCHA_NETWORK_RESOURCES_USED, buff);
		str_seq[i] = s;
		delete [] buff;
	}
finished:
	// Free the allocated memory.
	if (rs_original)
		rgm_free_resource(rs_original);
	if (nrulist)
		rgm_free_nlist(nrulist);
	if (orignrulist)
		rgm_free_nlist(orignrulist);
}
//
// add_NRU_to_val_args
//
// Add NRU list to the validation args.Called by nvseq_to_strseq
// in case if any of the dependency list is updated.
//
static void
add_NRU_to_val_args(rgm::idl_string_seq_t &str_seq, uint_t len,
    rgm_resource_t *rs)
{
	rgm_property_list_t *ptr = NULL;
	char  *key;

	ptr = rs->r_properties;
	// Traverse the property list
	while (ptr) {
		key = ptr->rpl_property->rp_key;
		// Check for the NRU property.
		if ((ptr->rpl_property->rp_type
		    == SCHA_PTYPE_STRINGARRAY) &&
		    (strcasecmp(SCHA_NETWORK_RESOURCES_USED,
		    key) == 0) && (ptr->rpl_property->rp_array_values
		    == NULL)) {
			compute_NRU_and_add(str_seq, len, rs, B_TRUE);
			//
			// Set the boolean value to B_TRUE to prevent
			// this function call if more than one
			// dependency list has changed and also
			// to prevent the truncation of strseq.
			//
			break;
		}
		ptr = ptr->rpl_next;
	}
}


//
// Copies idl_nameval_seq_t
//
void
nvseq_copy(const rgm::idl_nameval_seq_t &nv_seq,
    rgm::idl_nameval_seq_t &nv_out_seq)
{
	uint_t	i, j, len, size = 0;

	len = nv_seq.length();
	// set the length of the sequence
	nv_out_seq.length(len);

	for (i = 0; i < len; i++) {
		nv_out_seq[i].idlstr_nv_name = nv_seq[i].idlstr_nv_name;
		size = nv_seq[i].nv_val.length();
		nv_out_seq[i].nv_val.length(size);

		for (j = 0; j < size; j++) {
			nv_out_seq[i].nv_val[j] = nv_seq[i].nv_val[j];
		}
	}
}


//
// Convert a idl_nameval_seq_t to idl_string_seq_t
//
uint_t
nvseq_to_strseq(const rgm::idl_nameval_seq_t &nv_seq,
    rgm::idl_string_seq_t &str_seq, rgm_resource_t *rs)
{
	uint_t	i, j, r, len, size = 0;
	char	*buff, *s, *new_buff;
	char	*tmp, *ptr = NULL;
	rdeplist_t *rdep = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	//
	// variable to check if NRU is to be added to the validate list.
	// in case there is a change in any of the resource dependencies
	// value.
	//
	boolean_t update_valargs_with_NRU = B_FALSE;

	// variable to check if NRU was explicitly specified as a parameter
	// during the update operation.
	boolean_t empty_NRU_update_val_args = B_FALSE;

	//
	// Note that this code assumes that if get_value_string_array
	// returns NULL, the property in question is not in the args.
	//
	// This assumption requires that get_value_string_array allocate
	// memory with the interposed malloc call, which will exit the daemon
	// on memory allocation failure.
	//
	// This function is tweaked for adding the NRU list for validation
	// whenever any dependency list is changed.
	//

	// Calculate the number of properties user has entered.
	len = nv_seq.length();
	str_seq = rgm::idl_string_seq_t(len);
	str_seq.length(len);

	//
	// 'i' is used to traverse through nv_seq while 'r' is used to create
	// str_seq. 'len' indicates the total number of elements in str_seq.
	//
	for (i = 0, r = 0; r < len; i++, r++) {
		tmp = strdup(nv_seq[i].idlstr_nv_name);
		//
		// Check if the property is any among the four flavours of
		// dependencies.If the dependency has changed, NRU
		// recomputation might be required.
		//
		if ((!update_valargs_with_NRU) &&
		    ((!strcasecmp(SCHA_RESOURCE_DEPENDENCIES, tmp)) ||
		    (!strcasecmp(SCHA_RESOURCE_DEPENDENCIES_RESTART, tmp)) ||
		    (!strcasecmp(SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART,
		    tmp)) ||
		    (!strcasecmp(SCHA_RESOURCE_DEPENDENCIES_WEAK, tmp)))) {
			//
			// Update the flag the True.Change in any of the
			// dependencies might affect the NRU property.
			//
			update_valargs_with_NRU = B_TRUE;
		}
		if (nv_seq[i].nv_val[0] == (const char *)NULL) {
			// 2 is for '=' and '\0'
			buff = new char[strlen(tmp) + 2];
			(void) sprintf(buff, "%s=", tmp);
			str_seq[r] = buff;
			free(tmp);
			continue;
		}

		// If NRU is explicitly set as empty string compute
		// the NRU based on the dependency lists and add the
		// computed NRU to validation args.
		if (!strcasecmp(SCHA_NETWORK_RESOURCES_USED, tmp)) {
			//
			// Set the flag to true.We check for this flag below
			// before deciding to add NRU for validation.
			// In this case we do not need to add NRU for
			// validation.
			//
			empty_NRU_update_val_args = B_TRUE;
			if ((strcmp((const char*)nv_seq[i].nv_val[0],
			    "")) == 0) {
				compute_NRU_and_add(str_seq, r, rs, B_FALSE);
				free(tmp);
				continue;
			}
		}

		// Calculate the total size
		for (j = 0; j < nv_seq[i].nv_val.length(); j++) {
			size = size + (uint_t)strlen(nv_seq[i].nv_val[j]) + 1;
		}

		// Allocate the buff with calculated size
		buff = new char[size];
		// fill the buff
		buff[0] = '\0';

		if (nv_seq[i].nv_val.length() > 0) {
			(void) strcat(buff, nv_seq[i].nv_val[0]);
		}
		for (j = 1; j < nv_seq[i].nv_val.length(); j++) {
			(void) strcat(buff, ",");
			(void) strcat(buff, nv_seq[i].nv_val[j]);
		}

		if (!strcasecmp(SCHA_RESOURCE_DEPENDENCIES, tmp)) {
			ptr = strdup(SCHA_RESOURCE_DEPENDENCIES_Q);
		} else if (!strcasecmp(tmp,
		    SCHA_RESOURCE_DEPENDENCIES_RESTART)) {
			ptr = strdup(SCHA_RESOURCE_DEPENDENCIES_Q_RESTART);
		} else if (!strcasecmp(tmp,
		    SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART)) {
			ptr = strdup(
			    SCHA_RESOURCE_DEPENDENCIES_Q_OFFLINE_RESTART);
		} else if (!strcasecmp(SCHA_RESOURCE_DEPENDENCIES_WEAK, tmp)) {
			ptr = strdup(SCHA_RESOURCE_DEPENDENCIES_Q_WEAK);
		}

		if (ptr != NULL) {
			s = new char[strlen(ptr) + strlen(buff) + 2];
			(void) sprintf(s, "%s=%s", ptr, buff);
			str_seq[r] = s;
			str_seq.length(++len);
			free(ptr);
			ptr = NULL;

			if (strchr(buff, '{') != NULL) {
				rdep = convert_value_string_array_dep(
				    nv_seq[i].nv_val, res);
				ASSERT(res.err_code == SCHA_ERR_NOERR);
				new_buff = rdeplist_to_str(rdep);
				s = new char[strlen(tmp) +
				    strlen(new_buff) + 2];
				(void) sprintf(s, "%s=%s", tmp, new_buff);
				str_seq[++r] = s;
				free(new_buff);
				rgm_free_rdeplist(rdep);
			} else {
				s = new char[strlen(tmp) + strlen(buff) + 2];
				(void) sprintf(s, "%s=%s", tmp, buff);
				str_seq[++r] = s;
			}
		} else {
			s = new char[strlen(tmp) + strlen(buff) + 2];
			(void) sprintf(s, "%s=%s", tmp, buff);
			str_seq[r] = s;
		}
		free(tmp);
		delete [] buff;
	}
	// If dependency list has been changed for a scalable resource
	// and the NRU is not explicitly specified as one of the update
	// parameters we need to add NRU to validation args.
	if ((update_valargs_with_NRU) && (!empty_NRU_update_val_args) &&
	    (is_r_scalable(rs))) {
		//
		// We allocate one extra so as to accomodate for NRU
		// property if any dependency list is updated.
		//
		add_NRU_to_val_args(str_seq, len, rs);
	}
	return (len);
}
#endif // !EUROPA_FARM


//
// Queue a request to the state machine thread to run the state
// machine on the given zone when it is free.
// If lni == 0, then state machine will be run on all zones on local node.
//
void
run_state_machine(rgm::lni_t lni, const char *msg)
{
	state_machine_task *sm_task = new state_machine_task(lni);

	ucmm_print("run_state_machine", NOGET("lni %d (%s)\n"), lni, msg);

	state_machine_threadpool::the().defer_processing(sm_task);

	// sm_task will be deleted by the thread that processes it.
}

static sol::nodeid_t Nodeid = 0; // local nodeid

#ifdef EUROPA_FARM
//
// Nodename of the delegate RGM (frgmd)
// Retrieved from the Europa configuration cache on the Farm node.
//
static char *Farm_local_nodename = NULL;

//
// rgm_get_nodename
// rgm_get_nodename_cache
//
// If called with our nodeid, return our nodename string.
// Else, get the nodename information of the given nodeid
// from the president.
//
char *
rgm_get_nodename(sol::nodeid_t n)
{
	return (rgm_get_nodename_cache(n));
}
char *
rgm_get_nodename_cache(sol::nodeid_t n)
{
	char *nodenamep = NULL;

	if (n == Nodeid) {
		nodenamep = strdup(Farm_local_nodename);
	} else {
		CLIENT *clnt;
		cluster_get_args arg = {0};
		get_result_t result;

		clnt = rgmx_comm_getpres(ZONE);
		if (clnt == NULL) {
			ucmm_print("RGM", "rgm_get_nodename_cache failed\n");
			return (NULL);
		}

		arg.cluster_tag = strdup(SCHA_NODENAME_NODEID);
		arg.node_id = n;
		clnt->cl_auth = authunix_create_default();
		bzero(&result, sizeof (get_result_t));
		if (rgmx_scha_cluster_get_1(&arg, &result, clnt)
			!= RPC_SUCCESS) {
			ucmm_print("RGM", "rgm_get_nodename_cache failed\n");
		}
		free(arg.cluster_tag);
		nodenamep = (char *)strdup(
			(result.value_list->strarr_list_val)[0]);
		xdr_free((xdrproc_t)xdr_get_result_t,
				(char *)&result);
		rgmx_comm_freecnt(clnt);
	}
	return (nodenamep);
}

//
// rgm_get_nodeid
//
// If called with our nodename (or NULL), return our nodeid.
// Else, get the nodeid information of the given nodename
// from the president.
//
int
rgm_get_nodeid(char *nodename, scconf_nodeid_t *nodeidp)
{
	int rc = 0;
	char *s;

	if (nodename == NULL) {
		*nodeidp = Nodeid;
	} else {
		if (strcmp(nodename, Farm_local_nodename) == 0) {
			*nodeidp = Nodeid;
		} else {
			// Check to see if nodename is a nodeid (number > 0)
			for (s = nodename;  *s;  ++s)
				if (!isdigit(*s))
					break;
			if (*s == '\0') {
				*nodeidp = (scconf_nodeid_t)atoi(nodename);
				if (*nodeidp == -1) {
					*nodeidp = 0;
				}
				return (0);
			}

			// Call the president the retrieve the nodeid.
			CLIENT *clnt;
			cluster_get_args arg = {0};
			get_result_t result;

			clnt = rgmx_comm_getpres(ZONE);
			if (clnt == NULL) {
				ucmm_print("RGM", "rgm_get_nodeid failed\n");
				return (-1);
			}

			arg.cluster_tag = strdup(SCHA_NODEID_NODENAME);
			arg.node_name = (char *)strdup(nodename);
			clnt->cl_auth = authunix_create_default();
			bzero(&result, sizeof (get_result_t));
			if (rgmx_scha_cluster_get_1(&arg, &result, clnt)
				!= RPC_SUCCESS) {
				ucmm_print("RGM", "rgm_get_nodeid failed\n");
				rc = -1;
			} else {
				if (result.ret_code == SCHA_ERR_NOERR) {
					*nodeidp = atoi((result.value_list
					    ->strarr_list_val)[0]);
				} else {
					rc = -1;
				}
			}
			free(arg.node_name);
			free(arg.cluster_tag);
			xdr_free((xdrproc_t)xdr_get_result_t,
					(char *)&result);
			rgmx_comm_freecnt(clnt);
		}
	}
	return (rc);
}

#else // EUROPA_FARM

//
// the cache of node names/node IDs can be updated by a CCR callback
// if the infrastructure table is updated. So the cache needs to be protected
// by a mutex.
// Nodenames is an array containing all the names of the nodes in the cluster,
//   indexed by ID
//
static os::mutex_t cache_lock;
static boolean_t NodeCacheInitialized = B_FALSE;
static char *Nodenames[MAXNODES + 1];

class rgm_callback_impl : public McServerof<ccr::callback> {
public:
	void did_update_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void did_update(const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery(const char *, ccr::ccr_update_type,
	    Environment &);
	void _unreferenced(unref_t);
};



//
// rgm_get_nodename_cache
//
// Returns the nodename string
// Useful for printing in syslog messages.
// Caller must free allocated memory.
//
char *
rgm_get_nodename_cache(sol::nodeid_t n)
{
	char *nodenamep = NULL;

	if (!NodeCacheInitialized) {
		ucmm_print("get_nodename_cache", NOGET(
		    "node cache not yet inited"));
		ASSERT(0);
		return (NULL);
	}
	if ((n > 0) && (n <= MAXNODES)) {
		cache_lock.lock();
		if (Nodenames[n] != NULL && nodeset_contains(
		    Rgm_state->thecluststate.rp_all_nodes, n)) {
			nodenamep = strdup(Nodenames[n]);
			cache_lock.unlock();
			return (nodenamep);
		}
		cache_lock.unlock();
		return (NULL);
	}
	// Europa - lookup in Farm configuration.
	if (rgmx_hook_call(rgmx_hook_get_nodename, n, &nodenamep) ==
	    RGMX_HOOK_ENABLED) {
		return (nodenamep);
	}
	return (NULL);
}

#ifdef FUTURE
//
// get_nodename_fast
//
// This wraps rgm_get_nodename_cache().  A pointer to this function may
// be passed into certain librgm functions, so that rgm_get_nodename_cache()
// will be called instead of the much less efficient get_nodename() -- the
// latter function uses libclconf interfaces.
//
// Currently this is not used anywhere. Probably it was intended to be used in
// an optimization so that calls to get_nodename (in librgm) would instead call
// rgm_get_nodename_cache when called from librgmserver. This could possibly
// be done also by a weak pragma (similar to CR 6493759)
// Currently, we have ifdefed it out
//

scha_errmsg_t
get_nodename_fast(uint_t nodeid, char **nodenamep)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	*nodenamep = rgm_get_nodename_cache(nodeid);
	return (res);
}
#endif // FUTURE


//
// rgm_get_nodename
//
// Returns the nodename string, or a nodeid string if nodename isn't available.
// Useful for printing in syslog messages.
// Caller must free allocated memory.
//
char *
rgm_get_nodename(sol::nodeid_t n)
{
	char *nodenamep = NULL;
	char nodeidname[32];

	nodenamep = rgm_get_nodename_cache(n);

	if (nodenamep == NULL) {
		(void) sprintf(nodeidname, "%d", n);
		// crash and burn if we cannot return the nodeid string
		nodenamep = strdup(nodeidname);
	}

	return (nodenamep);
}
#endif

//
// The following could be superseded by get_ln().
// The latter has the advantage of returning a scha error
// which can differentiate NOMEM from invalid nodename.
// Meanwhile, we just define this function as a wrapper
// around get_ln().
//
rgm::lni_t
rgm_get_lni(const char *nodename)
{
	LogicalNode *lnNode = get_ln(nodename, NULL);
	ucmm_print("RGM", "rgm_get_lni %s \n", nodename);

	return (lnNode == NULL ? LNI_UNDEF : lnNode->getLni());
}

#ifndef EUROPA_FARM
//
// rgm_get_nodeid
//
// mimic the behavior of scconf_get_nodeid, but based on the content of the
// node info cache, for faster access.
//
// nodename can be both a real node name or a node id.
// if it is a node name, find the corresponding id in the cache (if valid)
// if it is a node id, verify its validity
//
// Returns 0 if a node ID is found, -1 otherwise
//
int
rgm_get_nodeid(char *nodename, scconf_nodeid_t *nodeidp)
{
	char *s;
	scconf_nodeid_t nodeid = 0;
	unsigned int i;

	if (!NodeCacheInitialized) {
		ucmm_print("rgm_get_nodeid", NOGET(
		    "node cache not yet inited"));
		ASSERT(0);
		return (-1);
	}

	if (nodeidp == NULL) {
		return (-1);
	}

	if (nodename == NULL) {
		// NULL nodename means the caller wants the local node ID
		*nodeidp = Nodeid;
		return (0);
	}

	// Check to see if nodename is a nodeid (number > 0)
	for (s = nodename;  *s;  ++s) {
		if (!isdigit(*s)) {
			break;
		}
	}
	cache_lock.lock();
	if (*s == '\0') {
		// nodename looks like a number. Make sure it is valid.
		nodeid = (scconf_nodeid_t)atoi(nodename);
		if (nodeid < 1 || nodeid > MAXNODES) {
			cache_lock.unlock();
			goto check_europa_nodes;
		}
		if (nodeset_contains(Rgm_state->thecluststate.rp_all_nodes,
		    nodeid)) {
			cache_lock.unlock();
			*nodeidp = nodeid;
			return (0);
		} else {
			cache_lock.unlock();
			goto check_europa_nodes; //lint !e801
		}
	}

	for (i = 1; i <= MAXNODES; i++) {
		if (Nodenames[i] != NULL && strcmp(Nodenames[i],
		    nodename) == 0) {
			cache_lock.unlock();
			if (!nodeset_contains(
			    Rgm_state->thecluststate.rp_all_nodes, i))
				goto check_europa_nodes; //lint !e801
			*nodeidp = i;
			return (0);
		}
	}
	cache_lock.unlock();

check_europa_nodes:
	// Europa - lookup in Farm configuration.
	if (rgmx_hook_call(rgmx_hook_get_nodeid,
		&nodename, nodeidp) == RGMX_HOOK_ENABLED) {
		return (*nodeidp == 0 ? -1 : 0);
	}

	return (-1);
}
#endif // EUROPA_FARM

// Return local nodeid.
sol::nodeid_t get_local_nodeid()
{
	return (Nodeid);
}

#ifdef EUROPA_FARM
//
// Caches the nodename and the nodeid of the local Farm node.
// This function is called during the frgmd initialization
// phase, before any other threads are created: (startrgmd).
//
// The nodeid and the nodename of a Farm node are cached during
// the installation phase of the Farm node. When installing a
// Farm node, a request is sent to the Europa Server to get
// the identification (nodeid/nodename) of the new Farm node.
//
// Note that this cache is refreshed at any reboot of the Farm node.
//
void
cache_local_nodename_nodeid()
{
	sc_syslog_msg_handle_t handle;

	if (rgmx_cfg_getnode((scxcfg_nodeid_t *)&Nodeid, &Farm_local_nodename)
	    == -1) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// An internal error has occurred. The rgmd will produce a
		// core file and will force the node to halt or reboot to
		// avoid the possibility of data corruption.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes,
		// and of the rgmd core file. Contact your authorized Sun
		// service provider for assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: Cannot get local nodeid/nodename");
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHED
	}
}
#else
//
// Caches the nodeid of the local node.
// This function is called soon after initializing ORB,
// before any interesting work is done in the rgmd.
//
void
cache_local_nodeid()
{
	Nodeid = orb_conf::node_number();
}

//
// free the memory allocated for the nodenames in the cache
// It is assumed that the cache lock is held and that the cache
// has already been initialized at least once
//
static void
free_cache()
{
	for (int i = 0; i <= MAXNODES; i++) {
		if (Nodenames[i] != NULL) {
			free(Nodenames[i]);
		}
	}
}


//
// create_cache
//
// 1) cache all the node names/node IDs pair, to avoid calling scconf
//    all the time. It was shown to seriously hurt performance on
//    big cluster, or with configuration with high number of RGs.
//
// 2) create cache file containing some information from the
//    CCR infrastructure table
//
// Returns 0 if successful
// Returns error if the node name/node IDs cache has been updated, but the
//	recep_file was not created (can be ignored)
// aborts if node name/node ID cache has not been updated.
//
// Caller should hold global rgm lock (needed for calling updateCachedNames
// on the lnManager) if file_only is false.
//
int
create_cache(boolean_t file_only)
{
	scconf_errno_t scconf_err;
	scconf_cfg_cluster_t *clconfigp = NULL;
	scconf_cfg_node_t *curr_p = NULL;
	FILE *recep_file = NULL;
	int no_nodes = 0;
	sc_syslog_msg_handle_t handle;
	int err = 0;

	ucmm_print("RGM", NOGET("in create_cache\n"));

	cache_lock.lock();

	if ((scconf_err = scconf_get_virtualclusterconfig(ZONE, &clconfigp)) !=
	    SCCONF_NOERR) {
		char err_buff[SCCONF_MAXSTRINGLEN];
		scconf_strerr(err_buff, scconf_err);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A call to an internal library to get the cluster
		// configuration unexpectedly failed. RGMD will exit causing
		// the node to reboot.
		// @user_action
		// Report the problem to your authorized Sun service provider.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: Unable to get the cluster configuration (%s).",
		    err_buff);
		sc_syslog_msg_done(&handle);
		abort();
		// UNREACHABLE
	}

// SC SLM addon start
	if ((clconfigp != NULL) &&
	    (clconfigp->scconf_cluster_nodelist != NULL)) {
		//
		// function used to read scconf SCSLM
		// per node properties.
		//
		scslm_get_scconf_props(clconfigp->scconf_cluster_nodelist);
	}
// SC SLM addon end

	if (!file_only) {

		if (NodeCacheInitialized) {
			// release previously allocated memory
			free_cache();
		}

		//
		// the Nodenames array is indexed by node IDs.
		// It is possible to have holes, so reset the array first
		//
		(void) memset(Nodenames, 0, (MAXNODES+1) * sizeof (char *));

		for (curr_p =  clconfigp->scconf_cluster_nodelist;
		    curr_p != NULL;
		    curr_p = curr_p->scconf_node_next) {

			if ((curr_p->scconf_node_nodeid > MAXNODES)) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The internal cluster configuration is
				// erroneous and a node carries an invalid ID.
				// RGMD will exit causing the node to reboot.
				// @user_action
				// Report the problem to your authorized Sun
				// service provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "fatal: Invalid node ID in "
				    "infrastructure table");
				sc_syslog_msg_done(&handle);
				abort();
				// UNREACHABLE
			}
			Nodenames[curr_p->scconf_node_nodeid] = strdup(
			    curr_p->scconf_node_nodename);
			ucmm_print("create_cache", NOGET(
			    "node cache: id %d = %s"),
			    curr_p->scconf_node_nodeid,
			    Nodenames[curr_p->scconf_node_nodeid]);
		}
		NodeCacheInitialized = B_TRUE;
	}
	// if zone cluster, dont create file cache
	if (strcmp(GLOBAL_ZONENAME, ZONE) != 0) {
		goto finished;
	}

	if ((recep_file = fopen(RGM_RECEP_FILE_TMP, "w"))
	    == NULL) {
		err = errno;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// RGM is unable to open the file used for caching the data
		// between the rgmd and the scha_cluster_get.
		// @user_action
		// Since this file is used only for improving the
		// communication between the rgmd and scha_cluster_get,
		// failure to open it doesn't cause any significant harm. rgmd
		// will retry to open this file on a subsequent occasion.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "Error in opening temporary RGM cache file %s: %s",
		    RGM_RECEP_FILE_TMP, strerror(err));
		sc_syslog_msg_done(&handle);
		goto finished; //lint !e801
	}

	(void) fprintf(recep_file, "%s\n",
	    clconfigp->scconf_cluster_clustername);

	for (no_nodes = 0, curr_p = clconfigp->scconf_cluster_nodelist;
	    curr_p != NULL;
	    curr_p = curr_p->scconf_node_next, no_nodes++) {
		// empty loop, used only to calculate no_nodes
	}

	(void) fprintf(recep_file, "%d\n", no_nodes);

	for (curr_p =  clconfigp->scconf_cluster_nodelist;
	    curr_p != NULL;
	    curr_p = curr_p->scconf_node_next) {
		(void) fprintf(recep_file, "%d,%s,%s\n",
		    curr_p->scconf_node_nodeid,
		    curr_p->scconf_node_nodename,
		    curr_p->scconf_node_privatehostname ?
		    curr_p->scconf_node_privatehostname : "null");
	}

	if (fclose(recep_file) != 0) {
		err = errno;
		goto finished; //lint !e801
	}

	if (rename(RGM_RECEP_FILE_TMP, RGM_RECEP_FILE) != 0) {
		err = errno;
		goto finished; //lint !e801
	}

	if (chmod(RGM_RECEP_FILE, 00644) != 0) {
		err = errno;
		goto finished; //lint !e801
	}

finished:
	scconf_free_clusterconfig(clconfigp);

finished_no_free:
	if (err != 0) {
		(void) unlink(RGM_RECEP_FILE_TMP);
		(void) unlink(RGM_RECEP_FILE);
	}

	cache_lock.unlock();

	// Must do this after we release the lock
	if (!file_only) {
		lnManager->updateCachedNames();
	}
	return (err);
}

extern char rgm_user_name[];

//
// Whenever membership changes, call this function to update membership.
// The (server) membership notion stored in Rgm_state->thecluststate and
// Rgm_state->static_membership should only be modified from this function. This
// function creates dummy nodenames for nodes that don't have mapping in the
// CCR (yet). This ensures that all cluster member will always have a nodename
// mapping
// If the flag init_europa_nodes is TRUE, the farm membership stored in the
// Rgm_state->static_membership is removed. This is done only in a context where
// rgmx_hook_update_configured_nodes() is called immediately afterwards.
//
// rgm lock should be held when the function is called.
//

void
update_cluster_state(
    boolean_t init_europa_nodes, boolean_t use_membership_api)
{
	char temp_nodename[32];
	boolean_t update_needed = B_FALSE;
	nodeid_t nid;

	cache_lock.lock();
	//
	// Cache the current cluster state (static members:rp_all_nodes, highest
	// configured node:rp_nnodes) from CMM automaton.
	//

#if (SOL_VERSION >= __s10)
	if (use_membership_api) {
		//
		// We need to fetch the static membership for this
		// cluster. We should not query the process
		// membership for this cluster, since the
		// process membership will contain nodes which are
		// in cluster membership only. For zone clusters,
		// this will also include nodes which run proxy
		// rgmds. We should always query scconf to fetch
		// the static membership.
		//
		clconf_cluster_t *cl_handle;
		const char *nodeName = NULL;

		//
		// initialize rp_all_nodes to empty nodeset
		//
		Rgm_state->thecluststate.rp_all_nodes = 0;
		Rgm_state->thecluststate.rp_nnodes = 0;
		//
		// get clconf handle for this cluster
		//
		cl_handle = clconf_cluster_get_vc_current(ZONE);
		//
		// iterate over all the nodes to see which are
		// in static membership
		//
		for (nid  = 1; nid <= NODEID_MAX; nid++) {
			nodeName = clconf_cluster_get_nodename_by_nodeid(
					cl_handle, nid);
			if (nodeName == NULL) {
				// This node is not a member of
				// this cluster.
				continue;
			}

			nodeset_add(Rgm_state->thecluststate.rp_all_nodes, nid);
			Rgm_state->thecluststate.rp_nnodes = nid;
		}
	} else {
		ucmm_getcluststate(&Rgm_state->thecluststate);
	}
#else	// (SOL_VERSION >= __s10)
	ucmm_getcluststate(&Rgm_state->thecluststate);
#endif	// (SOL_VERSION >= __s10)

	if (init_europa_nodes) {
		Rgm_state->static_membership =
		    Rgm_state->thecluststate.rp_all_nodes;
	} else {
		LogicalNodeset ns = Rgm_state->static_membership.getNodeset();
		Rgm_state->static_membership -= ns;
		Rgm_state->static_membership |=
		    Rgm_state->thecluststate.rp_all_nodes;
	}
	Rgm_state->num_static_nodes = Rgm_state->static_membership.count();

	//
	// Create dummy nodenames for those nodes which haven't yet got a name
	// in the CCR. This can happen, for example, if the CCR callback hasn't
	// been received/processed yet for a configuration change.
	//
	for (int i = 1; i <= MAXNODES; i++) {
		if (!nodeset_contains(Rgm_state->thecluststate.rp_all_nodes,
		    i)) {
			continue;
		}
		if (Nodenames[i] == NULL) {
			ucmm_print("update_cluster_state",
			    NOGET("calling create_cache for node:%d"), i);
			(void) create_cache(B_FALSE);
			update_needed = B_TRUE;
		}
	}
	cache_lock.unlock();
	if (update_needed)
		lnManager->updateCachedNames();
}

//
// Register CCR callbacks for infrastructure file.
// Caller should hold the global rgm lock. Abort if attempt
// fails
//
void
register_ccr_callbacks()
{
	static boolean_t ccr_registration_done = B_FALSE;
	rgm_callback_impl *infcb;
	ccr::callback_var cbptr;
	ccr::readonly_table_var tab_v;
	static ccr::directory_var ccr_directory_var = ccr::directory::_nil();
	static os::mutex_t ccr_mutex;
	Environment e;

	ccr_mutex.lock();

	if (ccr_registration_done) {
		ASSERT(0);	// should be called only once.
		ccr_mutex.unlock();
		return;
	}

	if (CORBA::is_nil(ccr_directory_var)) {
		naming::naming_context_var ctxp = ns::local_nameserver();
		CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
		if (e.exception()) {
			sc_syslog_msg_handle_t handle;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// An error occurred in a CCR related operation while
			// registering the callbacks for updating nodename
			// cache. Hence RGM will abort.
			// @user_action
			// Contact your authorized Sun service representative
			// for resolution.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: CCR callback registration error: %s",
			    "Unable to open CCR directory file.");
			sc_syslog_msg_done(&handle);
			abort();
			// UNREACHABLE
		}
		ccr_directory_var = ccr::directory::_narrow(obj);
		CL_PANIC(!CORBA::is_nil(ccr_directory_var));
	}

	tab_v = ccr_directory_var->lookup("infrastructure", e);
	if (e.exception()) {
		sc_syslog_msg_handle_t handle;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: CCR callback registration error: %s",
		    "Unable to lookup infrastructure file.");
		sc_syslog_msg_done(&handle);
		abort();
		// UNREACHABLE
	}

	infcb = new rgm_callback_impl;
	cbptr = infcb->get_objref();

	tab_v->register_callbacks(cbptr, e);
	if (e.exception()) {
		sc_syslog_msg_handle_t handle;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: CCR callback registration error: %s",
		    "callback registration for infrastructure table failed.");
		sc_syslog_msg_done(&handle);
		abort();
		// UNREACHABLE
	}

	ccr_registration_done = B_TRUE;
	ccr_mutex.unlock();
}


void
rgm_callback_impl::did_update_zc(const char *, const char *,
    ccr::ccr_update_type, Environment &)
{
}

void
rgm_callback_impl::did_update(const char *, ccr::ccr_update_type type,
    Environment &)
{

	switch (type) {
	case CCR_TABLE_REMOVED:
		ucmm_print("did_update", NOGET("the cluster has been deleted. "
		    "aborting"));
		abort();
		// UNREACHABLE
	case CCR_TABLE_ADDED:
		ucmm_print("did_update", NOGET("fatal: infrastructure table "
		    "added."));
		abort();
	case CCR_TABLE_MODIFIED:
	case CCR_RECOVERED:
	case CCR_INV_UPDATE:
		break;
	}

	infr_table_cb_lock.lock();
	no_new_infr_table_cb = false;
	infr_table_cb_cv.signal();
	infr_table_cb_lock.unlock();

	ucmm_print("did_update", NOGET("return.\n"));
}


void
rgm_callback_impl::did_recovery_zc(const char *, const char *,
    ccr::ccr_update_type, Environment &)
{
}

void
rgm_callback_impl::did_recovery(const char *, ccr::ccr_update_type,
    Environment &)
{
	ucmm_print("RGM", NOGET("rgm did_recovery called.\n"));
}


void
rgm_callback_impl::_unreferenced(unref_t)
{
	ucmm_print("RGM", NOGET("rgm _unreferenced called.\n"));
	delete this;
}
#endif // !EUROPA_FARM


//
// Function to check if an IDL call is from a current node in the
// cluster. When a server (President or slave rgm) gets an IDL request
// from another node that requests a state change on the server, this
// function must be called to check if the requesting node is still
// a member of the cluster. For the kernel clients of the ORB, the
// function is_orphan() accomplishes this. Due to code complexity,
// is_orphan() is not implemented for userland clients of the ORB.
// Therefore, userland ORB users need to protect against stale callers
// if needed.
//
// The IDL caller identifies itself with its node ID and an RGMD
// incarnation number (provided by the RGMD's UCMM module). The
// incarnation number allows detection of a node rebooting and rejoining
// the cluster.
//
// We do not get node incarnation numbers (including our own) until
// the first reconfiguration. UCMM gives us the incarnation numbers
// of all cluster members in the set_reconf_data() call just before the
// step1 transition call. Since nodes update their references to other nodes
// only in step2 (by calling rgm_comm_update), a booting node cannot get an
// IDL call from other nodes until it has completed step1 of its first
// reconfiguration.
//
// Returns false if "node" still has the specified incarnation number
// and true otherwise.
//
// When a node leaves the cluster, its incarnation number is set to
// INCN_UNKNOWN.
//
// This function must be called with the rgm lock held and the command
// requested by the IDL call should be completed without dropping the
// lock. If the caller dies while the callee is executing the IDL function,
// the resulting reconfiguration is blocked because the callee is holding
// the lock. The callee cannot mark the caller down until the lock is
// released.
//
boolean_t
stale_idl_call(sol::nodeid_t node, sol::incarnation_num incarnation)
{
	if (Rgm_state->node_incarnations.members[node] == incarnation)
		return (B_FALSE);
	else {
		// if this is a request from the ZC president.

		if (allow_inter_cluster() &&
			(strcmp(ZONE, GLOBAL_ZONENAME) != 0) && (
			Rgm_state->rgm_President == node)) {
			return (B_FALSE);
		}
		ucmm_print("stale_idl_call()",
		    NOGET("Stale IDL call from node %d, incarnation %ld; "
		    "current incarnation %ld\n"), node, incarnation,
		    Rgm_state->node_incarnations.members[node]);
		return (B_TRUE);
	}
}


#ifndef EUROPA_FARM
//
// Fault point function to test stale IDL calls. This should be a
// no-op unless debugging is enabled.
//
// It checks if there exists a file in /tmp with the name of the IDL
// function being checked. If so, it prints a debug message and sleeps
// for the amount of time (in seconds) specified in the file (with a
// default of 60 seconds) before returning.
//
// The function should be called before getting the rgm lock in slave IDL
// functions invoked by the President and causing a state change.
//
void
FAULT_POINT_FOR_ORPHAN_ORB_CALL(char *function,
    sol::nodeid_t node, sol::incarnation_num incarnation)
{
	if (Debugflag == B_TRUE) {

		FILE *fd;
		char filename[MAXNAMELEN];
		int seconds_to_sleep = 0;

		(void) sprintf(filename, "/tmp/%s", function);
		if ((fd = fopen(filename, "r")) != NULL) {
			(void) fscanf(fd, "%d", &seconds_to_sleep);
			(void) fclose(fd);
			if (seconds_to_sleep <= 0)
				seconds_to_sleep = 60 /* default */;
			ucmm_print("RGM", NOGET(
			    "FAULT_POINT_FOR_ORPHAN_ORB_CALL: got %s IDL call "
			    "from node %d, incarnation # %ld; "
			    "sleeping for %d seconds.\n"),
			    function, node, incarnation,
			    seconds_to_sleep);
			os::usecsleep(seconds_to_sleep * 1000 * 1000);
		}
	}
}


#ifdef DEBUG
//
// XXXXXX This function should be eliminated and calls to it should be
// XXXXXX replaced by proper FI Framework fault points.
//
// Fault point function to test scrgadm/scswitch calls occurring to the
// receptionist of a live rgmd (on a slave), at a point where the president
// has not yet registered on any node.  This function is used to test the
// fix for bugid 4335829 .
//
// It checks if there exists a file in /var/tmp with the name
// DONT_REGISTER_PRESIDENT.  If so, it prints a debug message and sleeps
// for the amount of time (in seconds) specified in the file (with a
// default of 300 seconds) before returning.
//
// The function only provides a useful test when called by a newly joining
// president in the case that all nodes are joining.  To test this fault, create
// this file on the lowest-numbered node, then halt and reboot the
// entire cluster.  Once the new president's rgmd has reached this fault point,
// attempted scrgadm or scswitch commands should return SCHA_ERR_CLRECONF
// with the message "RGM has not yet started."  Attempted API calls (except
// for scha_control) should wait patiently for the new president to register,
// then return successfully.  scha_control calls should fail with the
// error code SCHA_ERR_CLRECONF and should syslog a message
// "ERROR: scha_control() was called on resource group <%s>, resource <%s>
// before the RGM started".
//
void
FAULT_POINT_FOR_UNREGISTERED_PRES()
{
	FILE *fd;
	char filename[MAXNAMELEN];
	int seconds_to_sleep = 0;
	uint_t unslept;

	(void) sprintf(filename, "/var/tmp/DONT_REGISTER_PRESIDENT");
	if ((fd = fopen(filename, "r")) != NULL) {
		(void) fscanf(fd, "%d", &seconds_to_sleep);
		(void) fclose(fd);
		if (seconds_to_sleep <= 0) {
			unslept = 300 /* default */;
		} else {
			unslept = (uint_t)seconds_to_sleep;
		}
		ucmm_print("RGM", NOGET(
		    "FAULT_POINT_FOR_UNREGISTERED_PRES: "
		    "sleeping for %d seconds."),
		    unslept);
		while (unslept > 0) {
			// In case we are wakened by a signal, keep sleeping
			unslept = sleep(unslept);
		}
	}
}
#endif // DEBUG
#endif // !EUROPA_FARM


// Utility routines for the namelist_t data structure

//
// Checks to see if the given name exists in the curr_name_list
// The second argument is either rg name or resource name.
// Since it shouldn't be NULL or empty string, we just return false.
// Note: in some cases, a NULL could be passed as the second argument.
// It is by scrgadm.  When scrgadm sees an empty property value is specified,
// it passes one element of value list holding an empty string.
// Therefore we can't do CL_PANIC(name != NULL) here.
//
boolean_t
namelist_exists(namelist_t *curr_name_list, name_t name)
{
	namelist_t *curr_node = curr_name_list;
	boolean_t found = B_FALSE;

	if (name == NULL || *name == '\0')
		return (B_FALSE);

	while (curr_node != NULL) {
		if (strcmp(curr_node->nl_name, name) == 0) {
			found = B_TRUE;
			break;
		}
		curr_node = curr_node->nl_next;
	}
	return (found);
}


//
// Finds and deletes the node containing the given name in curr_namelist.
// It takes care to reassign curr_namelist in case the first node is
// being deleted.
// The delete of the node also deletes the name within it. This is OK
// assuming that that node in the namelist was added using namelist_add_name
// which makes local copy of the name being passed.
//
void
namelist_delete_name(namelist_t **curr_namelist, const char *name)
{
	namelist_t *curr_node = *curr_namelist;
	namelist_t *prev_node = NULL;

	while (curr_node) {
		if (strcmp(curr_node->nl_name, name) == 0) {
			if (curr_node == *curr_namelist) {
				// first node in the list
				*curr_namelist = (*curr_namelist)->nl_next;
			} else {
				// adjust the pointers to skip over the
				// node being deleted
				prev_node->nl_next = curr_node->nl_next;
			}
			free(curr_node->nl_name);
			free(curr_node);
			break;
		}
		prev_node = curr_node;
		curr_node = curr_node->nl_next;
	}
}


//
// namelist_copy
//
// Copy a given namelist to a new namelist_t struct, and also copy the
// strings of the orig. namelist (the new namelist is still valid after
// the original one and its strings are freed).
//
//
namelist_t *
namelist_copy(namelist_t *curr_namelist)
{
	namelist_t *curr_p, *new_p = NULL, *next_p = NULL;
	namelist_t *new_namelist = NULL;

	for (curr_p = curr_namelist; curr_p != NULL; curr_p = curr_p->nl_next) {
		new_p = (namelist_t *)malloc(sizeof (namelist_t));

		// copy nl_name
		new_p->nl_name = strdup(curr_p->nl_name);
		new_p->nl_next = NULL;

		// link in this node at end of list
		if (new_namelist == NULL) {
			new_namelist = new_p;
		} else {
			next_p->nl_next = new_p;
		}
		next_p = new_p;
	}

	return (new_namelist);
}


#ifndef EUROPA_FARM
//
// namelist_append
//
// Add the first given namelist to the end of the second namelist_t struct
// Caller needs to free the memory allocated by namelist_copy().
//
void
namelist_append(namelist_t *my_namelist, namelist_t **result_namelist)
{
	namelist_t *tail_p;

	if (my_namelist == NULL)
		return;

	if (*result_namelist == NULL) {
		*result_namelist = namelist_copy(my_namelist);
		return;
	}

	// Search for the end of result_namelist
	for (tail_p = *result_namelist; tail_p->nl_next != NULL;
	    tail_p = tail_p->nl_next) {
	}

	tail_p->nl_next = namelist_copy(my_namelist);
}
#endif // !EUROPA_FARM


//
// namelist_compare
//
// Compare two namelists. The namelists "match" if the all following are true:
//  1. they contain the same number of nodes
//  2. for all i: 0 <= i <= (length - 1), the nl_name fields of the "ith" node
//	match (using strcmp).
//
//  B_TRUE is returned if the namelists match, B_FALSE is returned otherwise.
//
//  Note that if the two lists contain the same names but in different orders,
//  then B_FALSE will be returned.
//
boolean_t
namelist_compare(namelist_t *namelist_1, namelist_t *namelist_2)
{
	namelist_t *nl1, *nl2;

	nl1 = namelist_1;
	nl2 = namelist_2;

	while (nl1 != NULL && nl2 != NULL) {
		// check for NULL nl_names
		if (nl1->nl_name == NULL || nl2->nl_name == NULL)
			if (nl1->nl_name != nl2->nl_name)
				return (B_FALSE);
		if (strcmp(nl1->nl_name, nl2->nl_name) != 0) {
			return (B_FALSE);
		}
		nl1 = nl1->nl_next;
		nl2 = nl2->nl_next;
	}
	if (nl1 == NULL && nl2 == NULL) {
		return (B_TRUE);
	}
	return (B_FALSE);
}


//
// Utilities for "rl_dependents" and "r_dependencies"
//

//
#ifndef EUROPA_FARM
// detect_rg_dependencies_cycles -
//
// Computes whether the proposed RG, proposed_rg, the new rg dependencies and
// the strong positive/negative affinities it's about to introduce, create
// any cycles in the RG dependencies graph.
// This is a special-purpose routine for cycle detection which assumes that
// there are no existing cycles in the graph, as opposed to a full blown
// version of cycle detection algorithm. Since, we apply this algorithm every
// time we introduce new dependencies in the graph (i.e. we search for
// cycles pro-actively), it can be proven that:
// Given a graph G=(V,E), if G does not contain any cycles, and we are trying
// to introduce a new set of directed edges (En) such that they start at a
// given vertex N (- V, then any cycle created in G'=(V,E+En) will contain N.
//
// Therefore, in the cycle detection algorithm below, we only search for cycles
// in the RG dependencies graph "terminating" on the RG being updated or
// created.
// Also since we don't allow a dependency to be created from a managed RG to
// an unmanaged RG, a corollary to the above will be that any cycle that might
// ever occur in the RG dependency graph will consist of all unmanaged RGs.
//
// Note that the depedee_rgs passed to this function is an expanded list of
// RG dependencies including the implicit RG dependencies.
//
// The following implementation is a variant of BFS (or Breadth First Search),
// which starts out at the proposed_rg (the RG being updated or created) and
// adds all the RGs reachable from it to the to_visit_nodes queue. It then
// removes the RG at the head of the queue and adds all the RGs reachable from
// it to the end of the queue and so on... until either we exhaust
// to_visit_nodes, or we find a cycle. The RG removed from the to_visit_nodes
// queue is added to the visited_nodes queue. Before adding a new RG to the
// to_visit_nodes we check to see if that RG is proposed_rg itself, in which
// we found a cycle and we bail out. If not, it checks to see if the new RG
// already exists in the either of the two queues, in which case we simply
// ignore it as we have already "discovered" that RG.
//
scha_errmsg_t
detect_rg_dependencies_cycles(rgm_rg_t *proposed_rg,
    rgm_affinities_t *affinities, namelist_t *dependee_rgs)
{
	// contains all the RGs reachable from proposed_rg.
	namelist_t *to_visit_nodes = NULL;
	// contains all the RGs which have already been "visited" i.e. whose
	// RG_dependecies RGs have already been added to the to_visit_nodes
	namelist_t *visited_nodes = NULL;
	namelist_t *deps = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rglist_t *curr_rg = NULL;
	name_t curr_node_name = NULL;


	// Here we essentially do the first iteration of the loop ourselves.
	// This is because the first (manual) iteration below operates on the
	// new proposed_rg which is not yet part of the Rgm_state structure.
	ucmm_print("detect_rg_dependencies_cycles",
	    NOGET("adding node %s to visited_nodes list\n"),
	    proposed_rg->rg_name);
	if ((res = namelist_add_name(&visited_nodes,
	    proposed_rg->rg_name)).err_code != SCHA_ERR_NOERR)
		goto finished; //lint !e801

	//
	// Iterate thro' the list of implicit dependees for scalable
	// services.
	//
	for (deps = dependee_rgs; deps != NULL; deps = deps->nl_next) {
		if (deps->nl_name == NULL || deps->nl_name[0] == '\0')
			continue;
		if (strcmp(deps->nl_name, proposed_rg->rg_name) == 0) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RG_DEP_CYCLIC,
			    proposed_rg->rg_name);
			goto finished; //lint !e801
		}
		ucmm_print("detect_rg_dependencies_cycles",
		    NOGET("adding node %s to to_visit_nodes list\n"),
		    deps->nl_name);
		if ((res = namelist_add_name(&to_visit_nodes,
		    deps->nl_name)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
	}

	for (deps = proposed_rg->rg_dependencies;
	    deps != NULL; deps = deps->nl_next) {
		if (deps->nl_name == NULL || deps->nl_name[0] == '\0')
			continue;
		if (strcmp(deps->nl_name, proposed_rg->rg_name) == 0) {
			// this takes care of the case where the user makes
			// proposed_rg dependent on itself.
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RG_DEP_CYCLIC,
			    proposed_rg->rg_name);
			goto finished; //lint !e801
		}
		ucmm_print("detect_rg_dependencies_cycles",
		    NOGET("adding node %s to to_visit_nodes list\n"),
		    deps->nl_name);
		if (!namelist_exists(to_visit_nodes, deps->nl_name)) {
			if ((res = namelist_add_name(&to_visit_nodes,
			    deps->nl_name)).err_code != SCHA_ERR_NOERR)
				goto finished; //lint !e801
		}
	}

	for (deps = affinities->ap_strong_pos;
	    deps != NULL; deps = deps->nl_next) {
		if (deps->nl_name == NULL || deps->nl_name[0] == '\0')
			continue;
		if (strcmp(deps->nl_name, proposed_rg->rg_name) == 0) {
			// this takes care of the case where the user makes
			// proposed_rg dependent on itself.
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RG_DEP_CYCLIC,
			    proposed_rg->rg_name);
			goto finished; //lint !e801
		}
		ucmm_print("detect_rg_dependencies_cycles",
		    NOGET("adding node %s to to_visit_nodes list\n"),
		    deps->nl_name);
		if ((res = namelist_add_name(&to_visit_nodes,
		    deps->nl_name)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
	}

	for (deps = affinities->ap_strong_neg;
	    deps != NULL; deps = deps->nl_next) {
		if (deps->nl_name == NULL || deps->nl_name[0] == '\0')
			continue;
		if (strcmp(deps->nl_name, proposed_rg->rg_name) == 0) {
			// this takes care of the case where the user makes
			// proposed_rg dependent on itself.
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RG_DEP_CYCLIC,
			    proposed_rg->rg_name);
			goto finished; //lint !e801
		}
		ucmm_print("detect_rg_dependencies_cycles",
		    NOGET("adding node %s to to_visit_nodes list\n"),
		    deps->nl_name);
		if ((res = namelist_add_name(&to_visit_nodes,
		    deps->nl_name)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
	}

	// Loop through all the RGs in to_visit_nodes
	while (to_visit_nodes != NULL) {
		// save the name of the current "node" here, as the current
		// node itself is being deleted from the to_visit_nodes list
		curr_node_name = strdup(to_visit_nodes->nl_name);

		// remove the RG at the head of the to_visit_nodes queue
		// and add it to the visited_nodes queue.
		ucmm_print("detect_rg_dependencies_cycles",
		    NOGET("adding node %s to visited_nodes list\n"),
		    curr_node_name);
		if ((res = namelist_add_name(&visited_nodes,
		    curr_node_name)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
		namelist_delete_name(&to_visit_nodes, curr_node_name);

		// retrieve the RG being added to the visited_nodes queue.
		if (is_enhanced_naming_format_used(curr_node_name))
			continue;
		curr_rg = rgname_to_rg(curr_node_name);
		if (curr_rg == NULL) {
			ucmm_print("detect_rg_dependencies_cycles", NOGET(
			    "RG <%s> not exist"), curr_node_name);
			res.err_code = SCHA_ERR_RG;
			rgm_format_errmsg(&res, REM_INVALID_RG,
			    curr_node_name);
			goto finished; //lint !e801
		}

		//
		// Add "implicit" dependees for scalable services.
		//
		dependee_rgs = add_sa_dependee_rgs_to_list(&res, curr_rg,
		    NULL);
		if (res.err_code != SCHA_ERR_NOERR) {
			goto finished; //lint !e801
		}

		for (deps = dependee_rgs; deps != NULL; deps = deps->nl_next) {
			if (strcmp(deps->nl_name, proposed_rg->rg_name) == 0) {
				// one of dependencies (reachable from
				// proposed_rg) is proposed_rg itself,
				// therefore we found a cycle.
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_RG_DEP_CYCLIC,
				    proposed_rg->rg_name);
				goto finished; //lint !e801
			}
			// if the dependencies RG already exists in either of
			// to_visit_nodes or visited_nodes, then we have
			// already "discovered" that RG and we don't need to
			// do anything with it.
			if (!namelist_exists(to_visit_nodes, deps->nl_name) &&
			    !namelist_exists(visited_nodes, deps->nl_name)) {
				ucmm_print("detect_rg_dependencies_cycles",
				    NOGET("adding node %s to to_visit_nodes"
				    "list\n"), deps->nl_name);
				if ((res = namelist_add_name(&to_visit_nodes,
				    deps->nl_name)).err_code != SCHA_ERR_NOERR)
					goto finished; //lint !e801
			}
		}

		for (deps = curr_rg->rgl_affinities.ap_strong_pos;
		    deps != NULL; deps = deps->nl_next) {
			if (strcmp(deps->nl_name, proposed_rg->rg_name) == 0) {
				// a cycle is found
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_RG_DEP_CYCLIC,
				    proposed_rg->rg_name);
				goto finished; //lint !e801
			}
			// do nothing if the affinity RG already visited
			if (!namelist_exists(to_visit_nodes, deps->nl_name) &&
			    !namelist_exists(visited_nodes, deps->nl_name)) {
				ucmm_print("detect_rg_dependencies_cycles",
				    NOGET("adding node %s to to_visit_nodes"
				    "list\n"), deps->nl_name);
				if ((res = namelist_add_name(&to_visit_nodes,
				    deps->nl_name)).err_code != SCHA_ERR_NOERR)
					goto finished; //lint !e801
			}
		}

		for (deps = curr_rg->rgl_affinities.ap_strong_neg;
		    deps != NULL; deps = deps->nl_next) {
			if (strcmp(deps->nl_name, proposed_rg->rg_name) == 0) {
				// a cycle is found
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_RG_DEP_CYCLIC,
				    proposed_rg->rg_name);
				goto finished; //lint !e801
			}
			// do nothing if the affinity RG already visited
			if (!namelist_exists(to_visit_nodes, deps->nl_name) &&
			    !namelist_exists(visited_nodes, deps->nl_name)) {
				ucmm_print("detect_rg_dependencies_cycles",
				    NOGET("adding node %s to to_visit_nodes"
				    "list\n"), deps->nl_name);
				if ((res = namelist_add_name(&to_visit_nodes,
				    deps->nl_name)).err_code != SCHA_ERR_NOERR)
					goto finished; //lint !e801
			}
		}
		free(curr_node_name);
		curr_node_name = NULL;
	}

finished:
	free(curr_node_name);
	rgm_free_nlist(to_visit_nodes);
	rgm_free_nlist(visited_nodes);

	return (res);
}
#endif // !EUROPA_FARM


//
// detect_cycles_visit
// ------------------
// This recursive function handles one RG node in the depth-first search
// for cycles.  It first checks if the RG is already in the visited_nodes list.
// If so, we've found a cycle.  If the has_rg_depend flag is true, at least
// one of the edges in the cycle contains an rg_dependency, so we want to
// count the cycle (cycles with only inter-rg dependency edges don't count).
//
// If the RG is not already in visited_nodes, we add it to visited nodes,
// then make recursive calls for each of the RGs on which we have
// rg_dependencies, and on each of the RGs which contain Rs on which the Rs
// in the RG have inter-rg dependencies.  This check is performed by the
// combined_cycles_visit_r_node helper function.
//
// If we don't find any cycles, "back-out" the changes from this level of
// the recursion by removing the RG from the visited_nodes list.
//
static boolean_t
detect_cycles_visit(namelist_t **visited_nodes, rglist_p_t curr_rg,
    boolean_t has_rg_depend, const char *originating_rg_name,
    scha_errmsg_t *res)
{
	namelist_t *deps;
	rglist_p_t dep_rg;
	rlist_p_t rp;

	ucmm_print("detect_combined_dependencies_cycles",
	    NOGET("in detect_cycles_visit: rg=<%s>\n"),
	    curr_rg->rgl_ccr->rg_name);

	//
	// First check for the current rg on the visited_nodes list.
	//
	if (namelist_exists(*visited_nodes, curr_rg->rgl_ccr->rg_name)) {
		//
		// We've found a cycle, so we don't need to check any
		// further.  If has_rg_depend is true and the current rg
		// is the originating rg, we return true because
		// the originating rg has created an illegal cycle.
		// Otherwise return false (to keep checking for other
		// cycles that might include rg dependencies, but to
		// bail out of this particular recursive path).
		//
		// We can return here no matter what (even if we haven't
		// found a "bad" cycle that includes an rg_dependency)
		// because by definition of finding a cycle, we've seen this
		// RG before.  That means that somewhere up the recursive
		// calls, there's another level of recursion processing this
		// RG.  Thus, by returning here, we trust that previous
		// level to process all edges out of this RG.
		//
		ucmm_print("detect_combined_dependencies_cycles",
		    NOGET("found a cycle: has_rg_depend==%s\n"),
		    has_rg_depend ? "TRUE" : "FALSE");
		return (has_rg_depend && (strcmp(originating_rg_name,
		    curr_rg->rgl_ccr->rg_name) == 0) ? B_TRUE : B_FALSE);
	}

	//
	// Add this RG to the visited_nodes list
	//
	*res = namelist_add_name(visited_nodes, curr_rg->rgl_ccr->rg_name);
	if (res->err_code != SCHA_ERR_NOERR) {
		//
		// Return true so that the search will end.  We'll figure
		// out that it's a NOMEM error at the top of the recursion.
		//
		return (B_TRUE);
	}

	//
	// Make the recursive call for each rg_dependency
	//
	for (deps = curr_rg->rgl_ccr->rg_dependencies;
	    deps != NULL; deps = deps->nl_next) {
		//
		// When processing the rg_dependencies list before it has
		// been saved to CCR and retrieved, it can have NULL
		// or empty string elements.
		//
		if (deps->nl_name == NULL || deps->nl_name[0] == '\0') {
			continue;
		}
		dep_rg = rgname_to_rg(deps->nl_name);
		CL_PANIC(dep_rg != NULL);
		if (detect_cycles_visit(visited_nodes, dep_rg, B_TRUE,
		    originating_rg_name, res)) {
			return (B_TRUE);
		}
	}

	//
	// Make the recursive call for each inter-rg resource dependency.
	// Note that combined_cycles_visit_r_node ignores intra-rg
	// dependencies.
	//
	for (rp = curr_rg->rgl_resources; rp != NULL; rp = rp->rl_next) {
		rgm_resource_t *temp_r;

		temp_r = rp->rl_ccrdata;
		if (combined_cycles_visit_r_node(visited_nodes, curr_rg,
		    temp_r->r_dependencies.dp_weak, has_rg_depend,
		    originating_rg_name, res) ||
		    combined_cycles_visit_r_node(visited_nodes, curr_rg,
		    temp_r->r_dependencies.dp_strong, has_rg_depend,
		    originating_rg_name, res) ||
		    combined_cycles_visit_r_node(visited_nodes, curr_rg,
		    temp_r->r_dependencies.dp_restart, has_rg_depend,
		    originating_rg_name, res) ||
		    combined_cycles_visit_r_node(visited_nodes, curr_rg,
		    temp_r->r_dependencies.dp_offline_restart, has_rg_depend,
		    originating_rg_name, res)) {

			return (B_TRUE);
		}
	}

	//
	// We didn't find any cycles down this path.  Pop the name off
	// and return false.
	//
	namelist_delete_name(visited_nodes, curr_rg->rgl_ccr->rg_name);
	return (B_FALSE);
}


//
// combined_cycles_visit_r_node
// ----------------------------
// Helper function to iterate through a list of resource dependencies
// (strong, weak, or restart), and call detect_cycles_visit on the
// containing RG for each resourace.  Skips RGs that are the same as
// rgp (skips intra-rg dependencies).
//
static boolean_t
combined_cycles_visit_r_node(namelist_t **visited_nodes, rglist_p_t rgp,
    rdeplist_t *deps_list, boolean_t has_rg_dep,
    const char *originating_rg_name, scha_errmsg_t *res)
{
	rdeplist_t *r_dep;
	rlist_p_t rp;

	ucmm_print("detect_combined_dependencies_cycles",
	    NOGET("in combined_cycles_visit_r_node\n"));

	//
	// iterate through the list of dependencies, calling
	// detect_cycles_visit for each one.  We pass along the has_rg_dep
	// flag.
	//
	for (r_dep = deps_list; r_dep != NULL; r_dep = r_dep->rl_next) {
		//
		// When processing the dependencies list before it has
		// been saved to CCR and retrieved, it can have NULL
		// or emptry string elements.
		//
		if (r_dep == NULL || r_dep->rl_name[0] == '\0') {
			continue;
		}
		if (is_enhanced_naming_format_used(r_dep->rl_name))
			continue;
		rp = rname_to_r(NULL, r_dep->rl_name);
		CL_PANIC(rp != NULL);

		//
		// Skip intra-rg dependencies
		//
		if (rp->rl_rg == rgp) {
			continue;
		}
		//
		// If the recursive call returns true, we must return
		// true immediately.
		//
		if (detect_cycles_visit(visited_nodes, rp->rl_rg,
		    has_rg_dep, originating_rg_name, res)) {
			return (B_TRUE);
		}
	}
	//
	// We didn't find a cycle down this path.
	//
	return (B_FALSE);
}


#ifndef EUROPA_FARM
//
// detect_combined_dependencies_cycles
// -----------------------------------
// Performs a depth-first search to detect RG cycles where the edges can
// be rg_dependencies or inter-rg resource dependencies.  However, we only
// care about cycles in which at least one edge is an rg_dependency.
//
// Cycles of rg_dependencies with rg_affinities are checked elsewhere.
// Resource cycles where edges are resource dependencies are checked
// elsewhere.
//
// This function is called in three scenarios:
// 1. RG update: rgp has ptrs to all resources; proposed_rg has all
// rg dependencies.
// 2. R create: rgp has ptrs to all resources except for extra_r; proposed_rg
// has all rg dependencies; extra_r is the new resource, in rgp.
// 3. R update: rgp has ptrs to all resources, but the info for extra_r
// might not be right; proposed_rg has all rgdependencies; extra_r has
// up-to-date dependency info for that r.
//
// The depth first search uses two helper functions: the recursive
// detect_cycles_visit, and the helper combined_cycles_visit_r_node.
//
// It keeps a list of visited_nodes, that is passed down through the
// recursive calls.  At each level, the RG node is added to the list,
// then removed at the end of processing that level.  At each level of the
// recursion there are multiple recursive calls: 1 for each RG reacable by
// RG dependencies, and 1 for each RG reachable by inter-rg resource deps.
// The base-case of the recursion is finding a cycle or when there are
// no more RGs reachable by dependencies.
//
// Because we care only about cycles in which at least one rg_dependency is
// involved, we pass a flag down through the recursion specifying whether
// or not we've found one edge in that particular path that is an
// rg_depenedency.
//
// Note that this function must only be called after the rg_dependencies
// list in proposed_rg has been checked to ensure that all RGs listed
// are valid (or the empty string).
//
scha_errmsg_t
detect_combined_dependencies_cycles(rglist_p_t rgp, rgm_rg_t *proposed_rg,
    rgm_resource_t *extra_r)
{
	//
	// Contains all the RGs which have already been "visited."
	//
	namelist_t *visited_nodes = NULL;

	namelist_t *deps = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rglist_t *curr_rg = NULL;


	//
	// We start out with just the root node in the visited_nodes list.
	//
	ucmm_print("detect_combined_dependencies_cycles",
	    NOGET("Starting by adding node %s (our root) to the "
	    "visited_nodes list\n"), proposed_rg->rg_name);

	if ((res = namelist_add_name(&visited_nodes,
	    proposed_rg->rg_name)).err_code != SCHA_ERR_NOERR) {
		goto finished; //lint !e801
	}

	//
	// First make the recursive call for each RG that can be reached
	// via RG dependencies.
	//
	for (deps = proposed_rg->rg_dependencies;
	    deps != NULL; deps = deps->nl_next) {
		//
		// When processing the rg_dependencies list before it has
		// been saved to CCR and retrieved, it can have NULL
		// or emptry string elements.
		//
		if (deps->nl_name == NULL || deps->nl_name[0] == '\0') {
			continue;
		}

		if (is_enhanced_naming_format_used(deps->nl_name)) {
			if (res.err_code == SCHA_ERR_NOERR) {
				res.err_code = SCHA_ERR_INVAL;
				// it's an R request
				rgm_format_errmsg(&res,
				    REM_IC_RG_DEP_NOT_SUPPORTED);
			}
			goto finished; //lint !e801

		}
		// retrieve the RG being added to the visited_nodes queue.
		curr_rg = rgname_to_rg(deps->nl_name);
		CL_PANIC(curr_rg != NULL);

		//
		// Make the recursive call, passing TRUE for the third
		// parameter, because we are reaching nodes via
		// rg dependencies.
		//
		if (detect_cycles_visit(&visited_nodes, curr_rg, B_TRUE,
		    proposed_rg->rg_name, &res)) {
			//
			// If we get back an error in res, it's a memory
			// error, so just bail out.
			//
			if (res.err_code == SCHA_ERR_NOERR) {
				res.err_code = SCHA_ERR_INVAL;
				if (extra_r == NULL) {
					// it's an RG request
					rgm_format_errmsg(&res,
					    REM_RG_CYCLIC_COMBINED,
					    proposed_rg->rg_name);
				} else {
					// it's an R request
					rgm_format_errmsg(&res,
					    REM_RS_CYCLIC_COMBINED,
					    extra_r->r_name);
				}
			}
			goto finished; //lint !e801
		}
	}

	//
	// If the "extra resource" is not NULL, we check if it introduced
	// any cycles here.  This case handles the resource create and update,
	// where at the time this function is called, the new resource or
	// resource state is not yet pointed to by the resource group
	// structure.
	//
	// We use the helper function combined_cycles_visit_r_node, which
	// checks for (and does not make the recursive call for) intra-rg
	// dependencies.
	//
	if (extra_r != NULL) {
		if (combined_cycles_visit_r_node(&visited_nodes, rgp,
		    extra_r->r_dependencies.dp_weak, B_FALSE,
		    proposed_rg->rg_name, &res) ||
		    combined_cycles_visit_r_node(&visited_nodes, rgp,
		    extra_r->r_dependencies.dp_strong, B_FALSE,
		    proposed_rg->rg_name, &res) ||
		    combined_cycles_visit_r_node(&visited_nodes, rgp,
		    extra_r->r_dependencies.dp_restart, B_FALSE,
		    proposed_rg->rg_name, &res) ||
		    combined_cycles_visit_r_node(&visited_nodes, rgp,
		    extra_r->r_dependencies.dp_offline_restart, B_FALSE,
		    proposed_rg->rg_name, &res)) {
			if (res.err_code == SCHA_ERR_NOERR) {
				res.err_code = SCHA_ERR_INVAL;
				// it's an R request
				rgm_format_errmsg(&res,
				    REM_RS_CYCLIC_COMBINED,
				    extra_r->r_name);
			}
			goto finished; //lint !e801
		}
	}

finished:
	rgm_free_nlist(visited_nodes);

	return (res);
}
#endif // !EUROPA_FARM


//
// check_impl_net_depend():
// r_dep_list is a dependency list of the resource proposed_r.
// This function checks for one particlar case of cyclic dependency;
// it returns SCHA_ERROR_INVAL with a corresponding error message if one
// of the dependees in r_dep_list has an implicit dependency on proposed_r.
// This function is called by detect_r_dependencies_cycles, and it has
// already checked that either the RG of proposed_r has the
// rg_impl_net_depend property set to true or the implicit_dep_check
// parameter set to true.
//
static scha_errmsg_t
check_impl_net_depend(rgm_resource_t *proposed_r, rdeplist_t *r_dep_list,
    boolean_t implicit_dep_check)
{
	rdeplist_t	*deps = NULL;
	rlist_p_t	rp = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	for (deps = r_dep_list; deps != NULL; deps = deps->rl_next) {
		if (is_enhanced_naming_format_used(deps->rl_name))
			continue;
		rp = rname_to_r(NULL, deps->rl_name);
		// rp has been checked, it shouldn't be NULL.
		CL_PANIC(rp != NULL);

		// Implicit network dependencies apply only within the same RG
		if (strcmp(proposed_r->r_rgname, rp->rl_ccrdata->r_rgname) != 0)
			continue;

		if (rp->rl_ccrtype->rt_sysdeftype != SYST_LOGICAL_HOSTNAME &&
		    rp->rl_ccrtype->rt_sysdeftype != SYST_SHARED_ADDRESS) {
			// There is dependency from IP address
			// R to non-IP address R in RG with
			// rg_impl_net_depend flag turned on.
			res.err_code = SCHA_ERR_INVAL;
			if (implicit_dep_check) {
				rgm_format_errmsg(&res,
				    REM_RG_CYCLIC_IMPL_DEP,
				    proposed_r->r_rgname);
			} else {
				rgm_format_errmsg(&res, REM_RS_DEP_CYCLIC,
				    proposed_r->r_name);
			}
			return (res);
		}
	}

	return (res);
}


//
// compute_visit_nodes()
// Adds all the resources in the dependee list of the given resource to the
// to_visit_nodes queue.  Before adding a R to the to_visit_nodes, we
// check to see if that R is proposed_r itself, in which we found a cycle
// and we return error.
//
static scha_errmsg_t
compute_visit_nodes(name_t proposed_rname, rdeplist_t *r_dep_list,
    namelist_t **to_visit_nodes)
{
	rdeplist_t *deps = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	for (deps = r_dep_list; deps != NULL; deps = deps->rl_next) {
		if (strcmp(deps->rl_name, proposed_rname) == 0) {
			// this takes care of the case where the
			// user makes proposed_rname dependent on itself.
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RS_DEP_CYCLIC,
			    proposed_rname);
			return (res);
		}

		ucmm_print("compute_visit_nodes",
		    NOGET("adding node %s to to_visit_nodes list\n"),
		    deps->rl_name);

		if ((res = namelist_add_name(to_visit_nodes,
		    deps->rl_name)).err_code != SCHA_ERR_NOERR)
			return (res);
	}

	return (res);
}


//
// check_r_in_visited_nodes()
// The given r_dep_list is a dependency list(restart/strong/weak) of one
// of the dependees of proposed_rname.  We check to see if any R in r_dep_list
// is proposed_rname itself, in which we found a cycle and we return error.
// Otherwise, we add the resources in to_visit_nodes, the caller will call
// this function again to do the dependency cycle checking with the
// dependency lists of these newly added resources.
// This method will look for a zone cluster name in the resource name as well.
// If such a format is found (<ZC name>:<R name>), this method will parse
// the ZC name and the R name and resource name checks accordingly
//
static scha_errmsg_t
check_r_in_visited_nodes(name_t proposed_rname, rdeplist_t *r_dep_list,
    namelist_t *visited_nodes, namelist_t **to_visit_nodes)
{
	rdeplist_t *deps = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *zc_name, *zc_rs_name;

	zc_name = zc_rs_name = NULL;
	for (deps = r_dep_list; deps != NULL; deps = deps->rl_next) {
		if (zc_name) {
			free(zc_name);
			zc_name = NULL;
		}
		if (zc_rs_name) {
			free(zc_rs_name);
			zc_rs_name = NULL;
		}

		if (is_enhanced_naming_format_used(deps->rl_name)) {
#if SOL_VERSION >= __s10
			// This R name is in the <ZC name>:<R name> format
			res = get_remote_cluster_and_rorrg_name(
				deps->rl_name, &zc_name, &zc_rs_name);
			if (res.err_code != SCHA_ERR_NOERR) {
				goto finished; //lint !e801
			}

			// We have to compare both ZC name and R name
			if ((strcmp(zc_rs_name, proposed_rname) == 0) &&
				(strcmp(ZONE, zc_name) == 0)) {
				// We have found a cyclic dependency
				// This dependee R has a dependency on
				// "proposed_r"
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_RS_DEP_CYCLIC,
							proposed_rname);
				goto finished; //lint !e801
			}
#endif
		} else if (strcmp(deps->rl_name, proposed_rname) == 0) {
			// one of dependencies (reachable from
			// proposed_r) is proposed_r itself,
			// therefore we found a cycle.
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RS_DEP_CYCLIC,
			    proposed_rname);
			goto finished; //lint !e801
		}
		// if the dependencies R already exists in either of
		// to_visit_nodes or visited_nodes, then we have
		// already "discovered" that R and we don't need to
		// do anything with it.
		if (!namelist_exists(*to_visit_nodes, deps->rl_name) &&
		    !namelist_exists(visited_nodes, deps->rl_name)) {
			ucmm_print("check_r_in_visited_nodes",
			    NOGET("adding node %s to to_visit_nodes"
			    "list\n"), deps->rl_name);
			if ((res = namelist_add_name(to_visit_nodes,
			    deps->rl_name)).err_code != SCHA_ERR_NOERR)
				goto finished; //lint !e801
		}
	}

finished:
	if (zc_name)
		free(zc_name);

	if (zc_rs_name)
		free(zc_rs_name);

	return (res);
}


// This routine is a clone of the above detect_rg_dependencies_cycles routine
// in almost all respects, except that it acts on Resources and takes the
// combined graph of restart, strong and weak dependencies into account for
// cycle detection.
// Please see the header comments of the above routine for more details on
// the algorithm and the premise underlying both these cycle detection
// routines.
// Notes:
//	skip_dep_restart flag indicates that the restart dependencies list
//	is a empty list, don't do the checking on this list
//	skip_off_restart_dep flag indicates that offline restart dependencies
//	is a empty list. don't do the checking for this list.
//	skip_dep_strong flag indicates that the strong dependencies list
//	is a empty list, don't do the checking on this list
//	skip_dep_weak flag indicates that the weak dependencies list
//	is a empty list, don't do the checking on this list
//	implicit_dep_check flag indicates that the
//	implicit_network_dependencies has been updated from false to true.
//	check_icrd_flag indicates whether the dependency cycle check logic
//	should involve resources from other clusters or not. If set to
//	B_FALSE, this routine will not consider any resource from other
//	clusters. If set to B_TRUE, this method will add any resource
//	encoutered in the dependency chain from a different cluster to
//	"to_visit_nodes" and will fetch information about this resource
//	from the remote cluster by making idl calls.
//
scha_errmsg_t
detect_r_dependencies_cycles(rgm_resource_t *proposed_r,
    boolean_t skip_dep_restart, boolean_t skip_off_restart_dep,
    boolean_t skip_dep_strong, boolean_t skip_dep_weak,
    boolean_t implicit_dep_check, boolean_t check_icrd_flag)
{
	namelist_t *to_visit_nodes = NULL;
	// contains all the Rs which have already been "visited" i.e. whose
	// R_dependecies Rs have already been added to the to_visit_nodes
	// contains all the Rs reachable from proposed_r.
	namelist_t *visited_nodes = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rlist_p_t	curr_r = NULL;
	rgm_rt_t	*rt;
	rglist_p_t	curr_rg = NULL;
	name_t curr_node_name = NULL;

#if SOL_VERSION >= __s10
	char *zc_name, *zc_rs_name;
	rgm::rgm_comm_var zc_rgm_pres;
	rgm::idl_rs_get_icrd_rsrcs_args arg;
	rgm::idl_rs_get_icrd_rsrcs_result_t_var result_v;
	Environment e;
	rdeplist_t *icrd_r_dep_list = NULL;
#endif
	// In RG with rg_impl_net_depend set, all loghost/sharedaddr Rs are
	// implicitly depended upon by other types of Rs. Check here if
	// there is any attempt to setup dependency from loghost/sharedaddr
	// R to non-loghost/sharedaddr Rs.
	// The same check needs to be done if the implicit_dep_check is set.
	//
	// Since the implicit dependency always exists in RG with
	// rg_impl_net_depend flag set and contains loghost/sharedaddr type Rs,
	// no matter whether R is enable or not, there should be no dependency
	// from loghost/sharedaddr R to non-loghost/sharedaddr R.
#if SOL_VERSION >= __s10
	zc_name = zc_rs_name = NULL;
#endif
	curr_rg = rgname_to_rg(proposed_r->r_rgname);
	rt = rtname_to_rt(proposed_r->r_type);
	if (((curr_rg->rgl_ccr->rg_impl_net_depend) ||
	    (implicit_dep_check)) &&
	    (rt->rt_sysdeftype == SYST_LOGICAL_HOSTNAME ||
	    rt->rt_sysdeftype == SYST_SHARED_ADDRESS)) {

		//
		// Since RG has rg_impl_net_depend set, and proposed R is
		// loghost or sharedaddr type, we need to check
		// for implicit dependency.
		//
		// If the dependencies is a empty list, skip the checking
		//
		if (!skip_dep_restart &&
		    proposed_r->r_dependencies.dp_restart != NULL) {
			if ((res = check_impl_net_depend(proposed_r,
			    proposed_r->r_dependencies.dp_restart,
			    implicit_dep_check)).err_code !=
			    SCHA_ERR_NOERR)
				goto finished; //lint !e801
		}

		if (!skip_off_restart_dep &&
		    proposed_r->r_dependencies.dp_offline_restart != NULL) {
			if ((res = check_impl_net_depend(proposed_r,
			    proposed_r
			    ->r_dependencies.dp_offline_restart,
			    implicit_dep_check)).err_code != SCHA_ERR_NOERR)
				goto finished; //lint !e801
		}


		if (!skip_dep_strong &&
		    proposed_r->r_dependencies.dp_strong != NULL) {
			if ((res = check_impl_net_depend(proposed_r,
			    proposed_r->r_dependencies.dp_strong,
			    implicit_dep_check)).err_code != SCHA_ERR_NOERR)
				goto finished; //lint !e801
		}

		if (!skip_dep_weak &&
		    proposed_r->r_dependencies.dp_weak != NULL) {
			if ((res = check_impl_net_depend(proposed_r,
			    proposed_r->r_dependencies.dp_weak,
			    implicit_dep_check)).err_code != SCHA_ERR_NOERR)
				goto finished; //lint !e801
		}
	}

	// First, add proposed_r to visited_nodes list.

	ucmm_print("is_r_dependencies_cyclic",
	    NOGET("adding node %s to visited_nodes list\n"),
	    proposed_r->r_name);
	if ((res = namelist_add_name(&visited_nodes,
	    proposed_r->r_name)).err_code != SCHA_ERR_NOERR)
		goto finished; //lint !e801

	// Then add all dependees of proposed_r to the to_visit_nodes queue.

	if (!skip_dep_weak) {
		if ((res = compute_visit_nodes(proposed_r->r_name,
		    proposed_r->r_dependencies.dp_weak,
		    &to_visit_nodes)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
	}

	if (!skip_dep_strong) {
		if ((res = compute_visit_nodes(proposed_r->r_name,
		    proposed_r->r_dependencies.dp_strong,
		    &to_visit_nodes)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
	}

	if (!skip_dep_restart) {
		if ((res = compute_visit_nodes(proposed_r->r_name,
		    proposed_r->r_dependencies.dp_restart,
		    &to_visit_nodes)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
	}

	if (!skip_off_restart_dep) {
		if ((res = compute_visit_nodes(proposed_r->r_name,
		    proposed_r->r_dependencies.dp_offline_restart,
		    &to_visit_nodes)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
	}


	// Loop through all the Rs in to_visit_nodes
	while (to_visit_nodes != NULL) {
		// save the name of the current "node" here, as the current
		// node itself is being deleted from the to_visit_nodes list
		curr_node_name = strdup(to_visit_nodes->nl_name);

		// remove the R at the head of the to_visit_nodes queue
		// and add it to the visited_nodes queue.
		ucmm_print("is_r_dependencies_cyclic",
		    NOGET("adding node %s to visited_nodes list\n"),
		    curr_node_name);
		if ((res = namelist_add_name(&visited_nodes,
		    curr_node_name)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
		namelist_delete_name(&to_visit_nodes, curr_node_name);
		// Check whether this is a local R or an R from a
		// different cluster

#if SOL_VERSION >= __s10
		if ((is_enhanced_naming_format_used(curr_node_name)
		    == B_TRUE) &&
		    (is_this_local_cluster_r_or_rg(curr_node_name, B_FALSE)
				== B_FALSE)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "detect_r_dependencies_cycles "
				    "Running at too low a version:R <%s> "
				    "has dependency on R <%s> "
				    "which is in not in local cluster\n"),
				    proposed_r->r_name, curr_node_name);
				continue;
			}
			// We will check whether we have to ensure that there
			// wont be any R dependency cycles spanning clusters
			// as well
			if (check_icrd_flag == B_FALSE) {
				continue;
			}

			// If we are here, it means we have to check for
			// inter-cluster-R-dependency cycles as well. This means
			// that we have to make idl calls to the remote cluster.
			if (zc_name) {
				free(zc_name);
				zc_name = NULL;
			}
			if (zc_rs_name) {
				free(zc_rs_name);
				zc_rs_name = NULL;
			}
			if (icrd_r_dep_list != NULL) {
				rgm_free_rdeplist(icrd_r_dep_list);
				icrd_r_dep_list = NULL;
			}

			// First get the cluster name and the resource name
			res = get_remote_cluster_and_rorrg_name(
			    curr_node_name, &zc_name, &zc_rs_name);
			if (res.err_code != SCHA_ERR_NOERR) {
				goto finished; //lint !e801
			}
			// Now get the remote cluster's rgmd president
			zc_rgm_pres = rgm_comm_getpres_zc(zc_name);
			if (CORBA::is_nil(zc_rgm_pres)) {
				// Looks like the zone cluster is down.
				// By the time this method is called, we should
				// have verified the validity of zone clusters.
				// So, the only reason why we are not able to
				// contact the zone cluster's rgmd is because
				// it might be down.
				res.err_code = SCHA_ERR_ZC_DOWN;
				goto finished;
			}
			// Get the list of resources which fall in the
			// dependency line of this ZC resource and which also
			// span different clusters.
			arg.idlstr_rs_name = new_str(zc_rs_name);
			zc_rgm_pres->idl_get_remote_rsrcs_in_dep_line_of_r(arg,
								result_v, e);
			res = except_to_scha_err(e);
			if (res.err_code != SCHA_ERR_NOERR) {
				goto finished; //lint !e801
			}

			if (result_v->idlstr_err_msg != NULL) {
				res.err_msg = strdup_nocheck(
						result_v->idlstr_err_msg);
			}

			if (result_v->ret_code != SCHA_ERR_NOERR) {
				res.err_code = result_v->ret_code;
				goto finished; //lint !e801
			}

			// Convert string sequence to rdeplist_t
			icrd_r_dep_list = convert_value_string_array_dep(
						result_v->value_list, res);

			if (res.err_code != SCHA_ERR_NOERR) {
				goto finished; //lint !e801
			}

			// Now, we have to check for a cyclic dependency
			if ((res = check_r_in_visited_nodes(proposed_r->r_name,
						icrd_r_dep_list, visited_nodes,
						&to_visit_nodes)).err_code !=
					SCHA_ERR_NOERR) {
				goto finished; //lint !e801
			}

			// The code further below deals with a local resource.
			// We can continue here.
			continue;
		}
#endif
		// If we are here, it means "curr_r" is a local resource.
		// retrieve the R being added to the visited_nodes queue.
		curr_r = rname_to_r(NULL, curr_node_name);

		if ((res = check_r_in_visited_nodes(proposed_r->r_name,
		    curr_r->rl_ccrdata->r_dependencies.dp_weak,
		    visited_nodes, &to_visit_nodes)).err_code !=
		    SCHA_ERR_NOERR) {
			goto finished; //lint !e801
		}

		if ((res = check_r_in_visited_nodes(proposed_r->r_name,
		    curr_r->rl_ccrdata->r_dependencies.dp_strong,
		    visited_nodes, &to_visit_nodes)).err_code !=
		    SCHA_ERR_NOERR) {
			goto finished; //lint !e801
		}

		if ((res = check_r_in_visited_nodes(proposed_r->r_name,
		    curr_r->rl_ccrdata->r_dependencies.dp_restart,
		    visited_nodes, &to_visit_nodes)).err_code !=
		    SCHA_ERR_NOERR) {
			goto finished; //lint !e801
		}

		if ((res = check_r_in_visited_nodes(proposed_r->r_name,
		    curr_r->rl_ccrdata->r_dependencies.dp_offline_restart,
		    visited_nodes, &to_visit_nodes)).err_code !=
		    SCHA_ERR_NOERR) {
			goto finished; //lint !e801
		}

		free(curr_node_name);
		curr_node_name = NULL;
	}

finished:
	free(curr_node_name);
	rgm_free_nlist(to_visit_nodes);
	rgm_free_nlist(visited_nodes);

#if SOL_VERSION >= __s10
	if (zc_name)
		free(zc_name);

	if (zc_rs_name)
		free(zc_rs_name);

	if (icrd_r_dep_list != NULL)
		rgm_free_rdeplist(icrd_r_dep_list);
#endif
	return (res);
}


//
// Free the namelists in an rgm_dependencies_t struct
// Also, set all fields of this struct to NULL
//
//
void
free_rgm_dependencies(rgm_dependencies_t *rgm_dep)
{
	if (rgm_dep->dp_strong != NULL) {
		rgm_free_rdeplist(rgm_dep->dp_strong);
		rgm_dep->dp_strong = NULL;
	}

	if (rgm_dep->dp_weak != NULL) {
		rgm_free_rdeplist(rgm_dep->dp_weak);
		rgm_dep->dp_weak = NULL;
	}

	if (rgm_dep->dp_restart != NULL) {
		rgm_free_rdeplist(rgm_dep->dp_restart);
		rgm_dep->dp_restart = NULL;
	}

	if (rgm_dep->dp_offline_restart != NULL) {
		rgm_free_rdeplist(rgm_dep->dp_offline_restart);
		rgm_dep->dp_offline_restart = NULL;
	}

}


// set_rl_dependents_for_resource
//
// Given a resource (dep_rp), compute and set the "rl_dependents" relation
// which is implied by the "r_dependencies" list for this resource.
//
// (only the "r_dependencies" relation is externally visible/settable)
//

scha_errmsg_t
set_rl_dependents_for_resource(rlist_p_t dep_rp)
{
	char		*indep_name;
	rlist_p_t	indep_rp;
	rdeplist_t	*dependencies;
	char		*dep_name;
	rgm_dependencies_t	rgm_deps;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	CL_PANIC(dep_rp != NULL);
	dep_name = dep_rp->rl_ccrdata->r_name;
	rgm_deps = dep_rp->rl_ccrdata->r_dependencies;
	ucmm_print("RGM",
	    NOGET("set_rl_dependents_for_resource <%s>\n"), dep_name);

	// Process weak dependencies
	for (dependencies = rgm_deps.dp_weak;
	    dependencies != NULL; dependencies = dependencies->rl_next) {
		indep_name = dependencies->rl_name;
		if (is_enhanced_naming_format_used(indep_name)) {

			continue;
		}
		indep_rp = rname_to_r(NULL, indep_name);
		CL_PANIC(indep_rp != NULL);
		ucmm_print("RGM", NOGET("Found weak dependee <%s>\n"),
		    indep_name);
		if (!rdeplist_exists(indep_rp->rl_dependents.dp_weak,
		    dep_name)) {
			ucmm_print("RGM",
			    NOGET("Adding myself to weak dependents "
			    "list of <%s>\n"), indep_name);
			res = rdeplist_add_name(
			    &(indep_rp->rl_dependents.dp_weak), dep_name,
			    dependencies->locality_type);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}
		}
	}

	// Process strong dependencies
	for (dependencies = rgm_deps.dp_strong;
	    dependencies != NULL; dependencies = dependencies->rl_next) {
		indep_name = dependencies->rl_name;
		if (is_enhanced_naming_format_used(indep_name)) {
			continue;
		}
		indep_rp = rname_to_r(NULL, indep_name);
		CL_PANIC(indep_rp != NULL);
		ucmm_print("RGM", NOGET("Found strong dependee <%s>\n"),
		    indep_name);
		if (!rdeplist_exists(indep_rp->rl_dependents.dp_strong,
		    dep_name)) {
			ucmm_print("RGM",
			    NOGET("Adding myself to strong dependents "
			    "list of <%s>\n"), indep_name);
			res = rdeplist_add_name(
			    &(indep_rp->rl_dependents.dp_strong), dep_name,
			    dependencies->locality_type);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}
		}
	}

	// Process restart dependencies
	for (dependencies = rgm_deps.dp_restart;
	    dependencies != NULL; dependencies = dependencies->rl_next) {
		indep_name = dependencies->rl_name;
		if (is_enhanced_naming_format_used(indep_name)) {
			continue;
		}
		indep_rp = rname_to_r(NULL, indep_name);
		CL_PANIC(indep_rp != NULL);
		ucmm_print("RGM", NOGET("Found restart dependee <%s>\n"),
		    indep_name);
		if (!rdeplist_exists(indep_rp->rl_dependents.dp_restart,
		    dep_name)) {
			ucmm_print("RGM",
			    NOGET("Adding myself to restart dependents "
			    "list of <%s>\n"), indep_name);
			res = rdeplist_add_name(
			    &(indep_rp->rl_dependents.dp_restart), dep_name,
			    dependencies->locality_type);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}
		}
	}

	// Process offline-restart dependencies
	for (dependencies = rgm_deps.dp_offline_restart;
	    dependencies != NULL; dependencies = dependencies->rl_next) {
		indep_name = dependencies->rl_name;
		if (is_enhanced_naming_format_used(indep_name)) {
			continue;
		}
		indep_rp = rname_to_r(NULL, indep_name);
		CL_PANIC(indep_rp != NULL);
		ucmm_print("RGM", NOGET("Found Offline restart dependee "
		    "<%s>\n"),
		    indep_name);
		if (!rdeplist_exists(indep_rp->rl_dependents.dp_offline_restart,
		    dep_name)) {
			ucmm_print("RGM",
			    NOGET("Adding myself to Offline restart dependents "
			    "list of <%s>\n"), indep_name);
			res = rdeplist_add_name(
			    &(indep_rp->rl_dependents.dp_offline_restart),
			    dep_name,
			    dependencies->locality_type);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}
		}
	}
	return (res);
}


//
// compare_rgm_dependencies
//
// Given two rgm_dependencies structs, compare the rdeplists of both
// structs, and see if they "match" (using rdeplist_compare).
// Note that rdeplist_compare() is sensitive to the ordering of the rdeplists.
//
// Return B_TRUE if all namelists match, B_FALSE otherwise
//
boolean_t
compare_rgm_dependencies(rgm_dependencies_t *rgm_dep1,
    rgm_dependencies_t *rgm_dep2)
{
	if (!rdeplist_compare(rgm_dep1->dp_restart, rgm_dep2->dp_restart)) {
		return (B_FALSE);
	}
	if (!rdeplist_compare(rgm_dep1->dp_strong, rgm_dep2->dp_strong)) {
		return (B_FALSE);
	}
	if (!rdeplist_compare(rgm_dep1->dp_weak, rgm_dep2->dp_weak)) {
		return (B_FALSE);
	}
	if (!rdeplist_compare(rgm_dep1->dp_offline_restart,
	    rgm_dep2->dp_offline_restart)) {
		return (B_FALSE);
	}

	return (B_TRUE);
}


//
// unlink_dependents_for_resource
//
// Given a resource, remove the resource name from the r_dependents lists
// of all resources in all RGs.
// This function is called when removing a resource from memory.
//
void
unlink_dependents_for_resource(rlist_p_t dep_rp)
{
	rlist_p_t	indep_rp;
	rglist_p_t	rglp;
	char		*dep_name;

	dep_name = dep_rp->rl_ccrdata->r_name;

	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		// For all resources in this RG ...
		for (indep_rp = rglp->rgl_resources; indep_rp != NULL;
		    indep_rp = indep_rp->rl_next) {
			// Process restart dependents
			rdeplist_delete_name(&(indep_rp->
			    rl_dependents.dp_restart), dep_name);
			// Process offline-restart dependents
			rdeplist_delete_name(&(indep_rp->
			    rl_dependents.dp_offline_restart), dep_name);
			// Process strong dependents
			rdeplist_delete_name(&(indep_rp->
			    rl_dependents.dp_strong), dep_name);
			// Process weak dependents
			rdeplist_delete_name(&(indep_rp->
			    rl_dependents.dp_weak), dep_name);
		}
	}
}


//
// update_rl_dependents_for_resource
//
// Given a resource (dep_rp) make any necessary changes to the
// "rl_dependents" lists of its new and previous dependees.
//
// i.e., if dependencies were removed, then this resource must be
// removed from the rl_dependents lists of these resources.
// If resources were added, then this resource must be added to the
// rl_dependents list of these resources.
//
// (only the "r_dependencies" relation is externally visible/settable)
//

scha_errmsg_t
update_rl_dependents_for_resource(rlist_p_t dep_rp)
{
	unlink_dependents_for_resource(dep_rp);
	return (set_rl_dependents_for_resource(dep_rp));
}



#ifdef	DEBUG
//
// check_rl_dependents_for_resource
//
// Given a resource (rp), check that the inverse of the "rl_dependents"
// for this resource is a sub-relation of the "r_dependencies" relation for
// all resources. Conversely, chech that the inverse of the "r_dependencies"
// relation for this resource is a sub-relation of the "rl_dependents" relation
// for all resources.
//
// Returns B_TRUE if rl_dependents and r_dependencies relations are consistent
// for this resource, returns B_FALSE otherwise
//
// This function is intended to serve as a subroutine for
// "check_rl_dependents()" which performs a global check over all resources.
//

static boolean_t
check_rl_dependents_for_resource(rlist_p_t rp)
{
	char		*dep_name, *indep_name;
	rlist_p_t	indep_rp, dep_rp;
	rdeplist_t	*dependencies, *dependencies_ptr;
	rdeplist_t	*dependents, *dependents_ptr;
	boolean_t	retval = B_TRUE;

	// Check weak r_dependencies
	dep_rp = rp;
	dep_name = dep_rp->rl_ccrdata->r_name;
	for (dependencies = dep_rp->rl_ccrdata->r_dependencies.dp_weak;
	    dependencies != NULL; dependencies = dependencies->rl_next) {
		indep_name = dependencies->rl_name;
		if (is_enhanced_naming_format_used(indep_name))
			continue;
		indep_rp = rname_to_r(NULL, indep_name);
		CL_PANIC(indep_rp != NULL);
		dependents_ptr = indep_rp->rl_dependents.dp_weak;
		if (!rdeplist_exists(dependents_ptr, dep_name)) {
			ucmm_print("check_rl_dependents_for_resource()",
			NOGET("found <%s> in 'r_dependencies' list for <%s>,"
			" but did not find <%s> in 'rl_dependents' list for"
			" <%s> \n"),
			indep_name, dep_name, dep_name, indep_name);
			retval = B_FALSE;
		}
	}

	// Check weak rl_dependents
	indep_rp = rp;
	indep_name = indep_rp->rl_ccrdata->r_name;
	for (dependents = indep_rp->rl_dependents.dp_weak;
	    dependents != NULL; dependents = dependents->rl_next) {
		dep_name = dependents->rl_name;
		if (is_enhanced_naming_format_used(dep_name))
			continue;
		dep_rp = rname_to_r(NULL, dep_name);
		CL_PANIC(dep_rp != NULL);
		dependencies_ptr = dep_rp->rl_ccrdata->r_dependencies.dp_weak;
		if (!rdeplist_exists(dependencies_ptr, indep_name)) {
			ucmm_print("check_rl_dependents_for_resource()",
			NOGET("found <%s> in 'rl_dependents' list for <%s>,"
			" but did not find <%s> in 'r_dependencies' list for"
			" <%s> \n"),
			dep_name, indep_name, indep_name, dep_name);
			retval = B_FALSE;
		}
	}

	// Check strong r_dependencies
	dep_rp = rp;
	dep_name = dep_rp->rl_ccrdata->r_name;
	for (dependencies = dep_rp->rl_ccrdata->r_dependencies.dp_strong;
	    dependencies != NULL; dependencies = dependencies->rl_next) {
		indep_name = dependencies->rl_name;
		if (is_enhanced_naming_format_used(indep_name))
			continue;
		indep_rp = rname_to_r(NULL, indep_name);
		CL_PANIC(indep_rp != NULL);
		dependents_ptr = indep_rp->rl_dependents.dp_strong;
		if (!rdeplist_exists(dependents_ptr, dep_name)) {
			ucmm_print("check_rl_dependents_for_resource()",
			NOGET("found <%s> in 'r_dependencies' list for <%s>,"
			" but did not find <%s> in 'rl_dependents' list for"
			" <%s> \n"),
			indep_name, dep_name, dep_name, indep_name);
			retval = B_FALSE;
		}
	}

	// Check strong rl_dependents
	indep_rp = rp;
	indep_name = indep_rp->rl_ccrdata->r_name;
	for (dependents = indep_rp->rl_dependents.dp_strong;
	    dependents != NULL; dependents = dependents->rl_next) {
		dep_name = dependents->rl_name;
		if (is_enhanced_naming_format_used(dep_name))
			continue;
		dep_rp = rname_to_r(NULL, dep_name);
		CL_PANIC(dep_rp != NULL);
		dependencies_ptr = dep_rp->rl_ccrdata->r_dependencies.dp_strong;
		if (!rdeplist_exists(dependencies_ptr, indep_name)) {
			ucmm_print("check_rl_dependents_for_resource()",
			NOGET("found <%s> in 'rl_dependents' list for <%s>,"
			" but did not find <%s> in 'r_dependencies' list for"
			" <%s> \n"),
			dep_name, indep_name, indep_name, dep_name);
			retval = B_FALSE;
		}
	}

	// Check Offline-restart r_dependencies
	dep_rp = rp;
	dep_name = dep_rp->rl_ccrdata->r_name;
	for (dependencies =
	    dep_rp->rl_ccrdata->r_dependencies.dp_offline_restart;
	    dependencies != NULL; dependencies = dependencies->rl_next) {
		indep_name = dependencies->rl_name;
		if (is_enhanced_naming_format_used(indep_name))
			continue;
		indep_rp = rname_to_r(NULL, indep_name);
		CL_PANIC(indep_rp != NULL);
		dependents_ptr = indep_rp->rl_dependents.dp_offline_restart;
		if (!rdeplist_exists(dependents_ptr, dep_name)) {
			ucmm_print("check_rl_dependents_for_resource()",
			NOGET("found <%s> in 'r_dependencies' list for <%s>,"
			" but did not find <%s> in 'rl_dependents' list for"
			" <%s> \n"),
			indep_name, dep_name, dep_name, indep_name);
			retval = B_FALSE;
		}
	}

	// Check Offline_rl_dependents
	indep_rp = rp;
	indep_name = indep_rp->rl_ccrdata->r_name;
	for (dependents = indep_rp->rl_dependents.dp_offline_restart;
	    dependents != NULL; dependents = dependents->rl_next) {
		dep_name = dependents->rl_name;
		if (is_enhanced_naming_format_used(dep_name))
			continue;
		dep_rp = rname_to_r(NULL, dep_name);
		CL_PANIC(dep_rp != NULL);
		dependencies_ptr =
		    dep_rp->rl_ccrdata->r_dependencies.dp_offline_restart;
		if (!rdeplist_exists(dependencies_ptr, indep_name)) {
			ucmm_print("check_rl_dependents_for_resource()",
			NOGET("found <%s> in 'rl_dependents' list for <%s>,"
			" but did not find <%s> in 'r_dependencies' list for"
			" <%s> \n"),
			dep_name, indep_name, indep_name, dep_name);
			retval = B_FALSE;
		}
	}

	// Check restart r_dependencies
	dep_rp = rp;
	dep_name = dep_rp->rl_ccrdata->r_name;
	for (dependencies = dep_rp->rl_ccrdata->r_dependencies.dp_restart;
	    dependencies != NULL; dependencies = dependencies->rl_next) {
		indep_name = dependencies->rl_name;
		if (is_enhanced_naming_format_used(indep_name))
			continue;
		indep_rp = rname_to_r(NULL, indep_name);
		CL_PANIC(indep_rp != NULL);
		dependents_ptr = indep_rp->rl_dependents.dp_restart;
		if (!rdeplist_exists(dependents_ptr, dep_name)) {
			ucmm_print("check_rl_dependents_for_resource()",
			NOGET("found <%s> in 'r_dependencies' list for <%s>,"
			" but did not find <%s> in 'rl_dependents' list for"
			" <%s> \n"),
			indep_name, dep_name, dep_name, indep_name);
			retval = B_FALSE;
		}
	}

	// Check restart rl_dependents
	indep_rp = rp;
	indep_name = indep_rp->rl_ccrdata->r_name;
	for (dependents = indep_rp->rl_dependents.dp_restart;
	    dependents != NULL; dependents = dependents->rl_next) {
		dep_name = dependents->rl_name;
		if (is_enhanced_naming_format_used(dep_name))
			continue;
		dep_rp = rname_to_r(NULL, dep_name);
		CL_PANIC(dep_rp != NULL);
		dependencies_ptr =
		    dep_rp->rl_ccrdata->r_dependencies.dp_restart;
		if (!rdeplist_exists(dependencies_ptr, indep_name)) {
			ucmm_print("check_rl_dependents_for_resource()",
			NOGET("found <%s> in 'rl_dependents' list for <%s>,"
			" but did not find <%s> in 'r_dependencies' list for"
			" <%s> \n"),
			dep_name, indep_name, indep_name, dep_name);
			retval = B_FALSE;
		}
	}

	return (retval);
}

//
// check_rl_dependents
//
// Do a global check (over all resources, in all resource groups) of the
// "rl_dependents" and "r_dependencies" relations to make sure they are
// consistent.
//
// Return B_TRUE if relations are consistent, otherwise return B_FALSE
//

boolean_t
check_rl_dependents(void)
{
	rglist_p_t	rglp;
	rlist_p_t	rp;

	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		for (rp = rglp->rgl_resources; rp != NULL; rp = rp->rl_next) {
			if (!check_rl_dependents_for_resource(rp))
				return (B_FALSE);
		}
	}
	return (B_TRUE);
}
#endif /* DEBUG */


//
// compute_rl_dependents
//
// Derives and sets the "rl_dependents" relation from the "r_dependencies"
// relation for all resources in all resource groups.
//
// (only the "r_dependencies" relation is externally visible/settable)
//
//
scha_errmsg_t
compute_rl_dependents(void)
{
	rglist_p_t	rglp;
	rlist_p_t	rp;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	// No need to free the existing 'r_dependents' first because this
	// function is only called from rgm_init_ccr() when you're reading
	// in the whole CCR.

	//
	// Build up entirely new 'r_dependents' relation
	//
	// For all resource groups...
	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		// For all resources in this RG ...
		for (rp = rglp->rgl_resources; rp != NULL; rp = rp->rl_next) {
			res = set_rl_dependents_for_resource(rp);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}
		}
	}
	return (res);
}


//
// Helper functions for affinities and affinitents
//

//
// set_affinitents_helper
// ---------------------
// For each rg "indep_rg" in affinity_list, add rg_name to the
// appropriate affinities list (ap_stong_pos, ap_weak_pos, etc) of the
// rgl_affinitents structure of the rg "indep_rg".
//
// The affinity type is specified as aff_type.
//
// This function processes one affinities list (either strong pos, strong neg,
// weak pos, or weak neg).
//
static scha_errmsg_t
set_affinitents_helper(char *rg_name, namelist_t *affinity_list,
    affinitytype_t aff_type)
{
	rglist_p_t	indep_rgp;
	namelist_t	*affinity;
	namelist_t	**temp_list;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	// iterate through the list of affinities
	for (affinity = affinity_list;
	    affinity != NULL; affinity = affinity->nl_next) {
		if (is_enhanced_naming_format_used(affinity->nl_name)) {
			continue;
		}
		indep_rgp = rgname_to_rg(affinity->nl_name);
		CL_PANIC(indep_rgp != NULL);
		//
		// Figure out the type of affinity, and store a pointer
		// to the appropriate namelist in the "affinitent".
		//
		switch (aff_type) {
		case AFF_STRONG_POS:
			temp_list = &(indep_rgp->
			    rgl_affinitents.ap_strong_pos);
			break;
		case AFF_WEAK_POS:
			temp_list = &(indep_rgp->
			    rgl_affinitents.ap_weak_pos);
			break;
		case AFF_STRONG_NEG:
			temp_list = &(indep_rgp->
			    rgl_affinitents.ap_strong_neg);
			break;
		case AFF_WEAK_NEG:
			temp_list = &(indep_rgp->
			    rgl_affinitents.ap_weak_neg);
			break;
		default:
			ucmm_print("RGM", NOGET("set_affinitents_helper: "
			    "invalid affinity type\n"));
			res.err_code = SCHA_ERR_INTERNAL;
			return (res);
		}
		//
		// If rg_name is not already in the list, add it.
		//
		if (!namelist_exists(*temp_list, rg_name)) {
			res = namelist_add_name(temp_list, rg_name);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}
		}
	}
	return (res);
}

//
// set_ic_affinitents_for_resource_group
//
// Given a resource group (rgp), add its name to the appropriate
// "rgl_affinitents" list (ap_strong_pos, ap_strong_neg, etc)
// in all resource groups for which rgp has an affinity.
//
//
static scha_errmsg_t
set_ic_affinitents_for_resource_group(rglist_p_t rgp)
{

	namelist_t	*affinity_list = NULL;
	namelist_t *aff = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm_affinities_t affinities = {NULL, NULL, NULL, NULL, NULL};

	CL_PANIC(rgp != NULL);

	ucmm_print("RGM",
	    NOGET("set_affinitents_for_resource_group <%s>\n"),
	    rgp->rgl_ccr->rg_name);


	if ((res = convert_affinities_list(rgp->rgl_ccr->rg_name,
	    rgp->rgl_ccr->rg_affinitents, &affinities)).err_code !=
	    SCHA_ERR_NOERR)
		return (res);

	affinity_list = affinities.ap_failover_delegation;

	for (aff = affinity_list; aff != NULL; aff = aff->nl_next) {
		if (!namelist_exists(rgp->rgl_affinitents.
		    ap_failover_delegation, aff->nl_name))
			namelist_add_name(
			    &(rgp->rgl_affinitents.ap_failover_delegation),
			    aff->nl_name);
	}


	affinity_list = affinities.ap_strong_pos;
	for (aff = affinity_list; aff != NULL; aff = aff->nl_next) {
		if (!namelist_exists(rgp->rgl_affinitents.ap_strong_pos,
		    aff->nl_name))
			namelist_add_name(
			    &(rgp->rgl_affinitents.ap_strong_pos),
			    aff->nl_name);
	}


	affinity_list = affinities.ap_strong_neg;
	for (aff = affinity_list; aff != NULL; aff = aff->nl_next) {
		if (!namelist_exists(rgp->rgl_affinitents.ap_strong_neg,
		    aff->nl_name))
			namelist_add_name(
			    &(rgp->rgl_affinitents.ap_strong_neg),
			    aff->nl_name);
	}


	affinity_list = affinities.ap_weak_neg;
	for (aff = affinity_list; aff != NULL; aff = aff->nl_next) {
		if (!namelist_exists(rgp->rgl_affinitents.ap_weak_neg,
		    aff->nl_name))
			namelist_add_name(
			    &(rgp->rgl_affinitents.ap_weak_neg),
			    aff->nl_name);
	}

	affinity_list = affinities.ap_weak_pos;
	for (aff = affinity_list; aff != NULL; aff = aff->nl_next) {
		if (!namelist_exists(rgp->rgl_affinitents.ap_weak_pos,
		    aff->nl_name))
			namelist_add_name(
			    &(rgp->rgl_affinitents.ap_weak_pos),
			    aff->nl_name);
	}
	return (res);
}


static scha_errmsg_t
ulink_ic_affinitents_for_resource_group(rglist_p_t rgp)
{
	namelist_t	*affinity_list = NULL;
	namelist_t *aff = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm_affinities_t affinities = {NULL, NULL, NULL, NULL, NULL};

	CL_PANIC(rgp != NULL);
	ucmm_print("RGM",
	    NOGET("set_affinitents_for_resource_group <%s>\n"),
	    rgp->rgl_ccr->rg_name);
	if ((res = convert_affinities_list(rgp->rgl_ccr->rg_name,
	    rgp->rgl_ccr->rg_affinitents, &affinities)).err_code
	    != SCHA_ERR_NOERR)
		return (res);

	affinity_list = affinities.ap_failover_delegation;

	for (aff = affinity_list; aff != NULL; aff = aff->nl_next) {
		if (namelist_exists(rgp->rgl_affinitents.ap_failover_delegation,
		    aff->nl_name))
			namelist_delete_name(
			    &(rgp->rgl_affinitents.
			    ap_failover_delegation),
			    aff->nl_name);
	}


	affinity_list = affinities.ap_strong_pos;
	for (aff = affinity_list; aff != NULL; aff = aff->nl_next) {
		if (namelist_exists(rgp->rgl_affinitents.ap_strong_pos,
		    aff->nl_name))
			namelist_delete_name(&
			    (rgp->rgl_affinitents.ap_strong_pos),
			    aff->nl_name);
	}


	affinity_list = affinities.ap_strong_neg;
	for (aff = affinity_list; aff != NULL; aff = aff->nl_next) {
		if (namelist_exists(rgp->rgl_affinitents.ap_strong_neg,
		    aff->nl_name))
			namelist_delete_name(
			    &(rgp->rgl_affinitents.ap_strong_neg),
			    aff->nl_name);
	}


	affinity_list = affinities.ap_weak_neg;
	for (aff = affinity_list; aff != NULL; aff = aff->nl_next) {
		if (namelist_exists(rgp->rgl_affinitents.ap_weak_neg,
		    aff->nl_name))
			namelist_delete_name(
			    &(rgp->rgl_affinitents.ap_weak_neg),
			    aff->nl_name);
	}

	affinity_list = affinities.ap_weak_pos;
	for (aff = affinity_list; aff != NULL; aff = aff->nl_next) {
		if (namelist_exists(rgp->rgl_affinitents.ap_weak_pos,
		    aff->nl_name))
			namelist_delete_name(
			    &(rgp->rgl_affinitents.ap_weak_pos),
			    aff->nl_name);
	}
	return (res);
}


//
// set_affinitents_for_resource_group
//
// Given a resource group (rgp), add its name to the appropriate
// "rgl_affinitents" list (ap_strong_pos, ap_strong_neg, etc)
// in all resource groups for which rgp has an affinity.
//
// Use the set_affinitents_helper function to process each of the four
// types of affinities.
//
scha_errmsg_t
set_affinitents_for_resource_group(rglist_p_t rgp)
{
	char		*rg_name;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	CL_PANIC(rgp != NULL);
	rg_name = rgp->rgl_ccr->rg_name;

	ucmm_print("RGM",
	    NOGET("set_affinitents_for_resource_group <%s>\n"), rg_name);

	// Process strong positive affinities
	if ((res = set_affinitents_helper(rg_name,
	    rgp->rgl_affinities.ap_strong_pos, AFF_STRONG_POS)).err_code !=
	    SCHA_ERR_NOERR) {
		return (res);
	}

	// process weak positive affinities
	if ((res = set_affinitents_helper(rg_name,
	    rgp->rgl_affinities.ap_weak_pos, AFF_WEAK_POS)).err_code !=
	    SCHA_ERR_NOERR) {
		return (res);
	}

	// process strong negative affinities
	if ((res = set_affinitents_helper(rg_name,
	    rgp->rgl_affinities.ap_strong_neg, AFF_STRONG_NEG)).err_code !=
	    SCHA_ERR_NOERR) {
		return (res);
	}

	// process weak negative affinities
	if ((res = set_affinitents_helper(rg_name,
	    rgp->rgl_affinities.ap_weak_neg, AFF_WEAK_NEG)).err_code !=
	    SCHA_ERR_NOERR) {
		return (res);
	}

	res = set_ic_affinitents_for_resource_group(rgp);
	return (res);
}


//
// compute_all_affinities
//
// Sets the affinities and affinitents (inverse pointers for affinities)
// on all resource groups, based on the affinities data from the CCR.
//
// This function should be called only after the CCR is read to initialize
// all resource groups.  It should not be called for incremental calculations.
//
// We iterate once through the list of RGs, updating each RGs in-memory
// affinities structure, and then setting its name in the affinitents
// structures of all groups for which it has affinities (by using
// set_affinitents_for_rg).
//
scha_errmsg_t
compute_all_affinities(void)
{
	rglist_p_t	rglp;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	ucmm_print("compute_all_affinites", NOGET("Compute_all_affinities\n"));

	//
	// No need to free the existing rgl_affinities and rgl_affinitents,
	// because this function is only called from rgm_init_ccr() after
	// newly creating all resource groups.  However, note that
	// convert_affinities_list will automatically free memory for us if
	// needed.
	//

	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		//
		// First, set the in-memory rgl_affinities lists for this RG.
		//
		res = convert_affinities_list(rglp->rgl_ccr->rg_name,
		    rglp->rgl_ccr->rg_affinities, &(rglp->rgl_affinities));
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
		//
		// Now we can add this RG to the affinitents list of all its
		// affinities.
		//
		res = set_affinitents_for_resource_group(rglp);
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
	}
	return (res);
}


//
// unlink_affinitents_for_resource_group
//
// Given a resource group, remove the resource group name from the
// rgl_affinitents lists of all resource groups.
//
// This function is called when removing a resource group from memory.
//
void
unlink_affinitents_for_resource_group(rglist_p_t rgp)
{
	rglist_p_t	rgp_temp;
	char		*rg_name;

	rg_name = rgp->rgl_ccr->rg_name;

	for (rgp_temp = Rgm_state->rgm_rglist; rgp_temp != NULL;
	    rgp_temp = rgp_temp->rgl_next) {
		// Process strong positive affinities
		namelist_delete_name(&(rgp_temp->
		    rgl_affinitents.ap_strong_pos), rg_name);
		// Process weak positive affinities
		namelist_delete_name(&(rgp_temp->
		    rgl_affinitents.ap_weak_pos), rg_name);
		// Process strong negative affinities
		namelist_delete_name(&(rgp_temp->
		    rgl_affinitents.ap_strong_neg), rg_name);
		// Process weak negative affinities
		namelist_delete_name(&(rgp_temp->
		    rgl_affinitents.ap_weak_neg), rg_name);
	}

	ulink_ic_affinitents_for_resource_group(rgp);
}


//
// update_affinitents_for_resource_group
//
// Given a resource group, remove its name from all RGs' affinitents lists
// by calling unlink_affinitents_for_resource_group.  Then create new inverse
// pointer links for the new affinities of RG by calling
// set_affinitents_for_resource_group.
//
scha_errmsg_t
update_affinitents_for_resource_group(rglist_p_t rgp)
{
	unlink_affinitents_for_resource_group(rgp);
	return (set_affinitents_for_resource_group(rgp));
}


//
// convert_affinities_list
// -----------------------
// Frees the five namelists in the rgm_affinities_t structure, and creates
// new ones to represent the affinities specified in the raw_list.
// This function expects each name in raw_list to look like this:
// <affinity-type><rg-name>
//  affinity-type can be either +++, ++, +, -, or --.
// rg-name is a resource group name.
//
// This function checks that each name in the namelist is formatted correctly,
// and that it is a valid RG name.  It returns SCHA_ERR_INVAL and an
// appropriate message if it is not.  If this function returns an error,
// the affinities structure will be unchanged.
//
scha_errmsg_t
convert_affinities_list(const char *rg_name, namelist_t *raw_list,
    rgm_affinities_t *affinities)
{
	namelist_t *raw_name = NULL;
	namelist_t *new_strong_pos = NULL, *new_weak_pos = NULL;
	namelist_t *new_strong_neg = NULL, *new_weak_neg = NULL;
	namelist_t *new_fo_delegation = NULL;
	namelist_t **temp_namelist;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *temp_name;
	int type_length;
	boolean_t failover_delegation;

	char *affinities_list = namelist_to_str(raw_list);
	ucmm_print("convert_affinities_list", NOGET("Converting affinities "
	    "list %s for resource group %s\n"), affinities_list, rg_name);
	free(affinities_list);

	// iterate over each name in the namelist
	for (raw_name = raw_list; raw_name != NULL; raw_name =
	    raw_name->nl_next) {
		temp_name = raw_name->nl_name;
		//
		// We ignore names that are NULL or empty strings.
		//
		if (temp_name == NULL || temp_name[0] == '\0') {
			continue;
		}
		//
		// The name must be at least two characters long to be valid
		// (at least one character for the affinity-type, and at least
		// one character for the rg name).
		//
		if (strlen(temp_name) < 2) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
			    SCHA_RG_AFFINITIES);
			goto err_convert_affinity; //lint !e801
		}

		//
		// Figure out which type of affinity this name represents.
		// It will either start with + or - (if not, it's an error).
		//
		type_length = 0;
		failover_delegation = B_FALSE;
		if (temp_name[0] == '+') {
			//
			// If the name started with +, it could have 1 or 2
			// more '+'.
			// Note that a valid name may have only 2 characters
			// so we need to be careful if testing more than that.
			//
			if (temp_name[1] == '+') {
				if ((strlen(temp_name) > 2) &&
				    temp_name[2] == '+') {
					//
					// We have a strong positive affinity
					// with failover delegation.
					//
					// A +++ affinity is added to 2 lists:
					// - list of strong positive affinity
					//   so that the state machine takes
					//   it into account
					// - list of delegate (list of 1
					//   element), for easy access
					//
					// The multiple lists are not visible
					// from outside the RGM
					//
					type_length = 3;
					failover_delegation = B_TRUE;
				} else {
					// we have a strong positive affinity
					type_length = 2;
				}
				temp_namelist = &new_strong_pos;
			} else {
				// we have a weak positive affinity
				type_length = 1;
				temp_namelist = &new_weak_pos;
			}
		} else if (temp_name[0] == '-') {
			if (temp_name[1] == '-') {
				// we have a strong negative affinity
				type_length = 2;
				temp_namelist = &new_strong_neg;
			} else {
				// we have a weak negative affinity
				type_length = 1;
				temp_namelist = &new_weak_neg;
			}
		} else {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
			    SCHA_RG_AFFINITIES);
			goto err_convert_affinity; //lint !e801
		}
		//
		// Now that we know where the rg name starts, verify that it's
		// a valid rg.
		//
		temp_name += type_length;
		if (!is_enhanced_naming_format_used(temp_name)) {
			if (rgname_to_rg(temp_name) == NULL) {
				ucmm_print("RGM", NOGET(
				    "convert_affinities_list "
				    "RG <%s> has affinity for RG <%s>"
				    "which does not exist\n"),
				    rg_name, temp_name);
				res.err_code = SCHA_ERR_RG;
				rgm_format_errmsg(&res, REM_RG_AFF_NON_RG,
				    rg_name, temp_name);
				goto err_convert_affinity; //lint !e801
			}
		} else {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "convert_affinities_list "
				    "Running at too low a version:RG <%s> "
				    "has affinity for RG <%s> "
				    "which is in not in local cluster\n"),
				    rg_name, temp_name);
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_RGM_IC_NOT_ALLOWED);
				goto err_convert_affinity; //lint !e801
			}
#if SOL_VERSION >= __s10
			if (failover_delegation) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
				    SCHA_RG_AFFINITIES);
				goto err_convert_affinity; //lint !e801
			}

			if (is_this_local_cluster_r_or_rg(temp_name, B_TRUE)) {
				res.err_code = SCHA_ERR_RG;
				rgm_format_errmsg(&res,
				    REM_CREATE_IC_AFF_LOCAL_CLUSTER,
				    temp_name, rg_name);
				goto err_convert_affinity; //lint !e801
			}
#endif
		}

		//
		// Now that we know the name is valid, actually add it to
		// our temporary namelist.
		//
		res = namelist_add_name(temp_namelist, temp_name);
		if (res.err_code != SCHA_ERR_NOERR) {
			goto err_convert_affinity; //lint !e801
		}
		if (failover_delegation) {
			res = namelist_add_name(&new_fo_delegation, temp_name);
			if (res.err_code != SCHA_ERR_NOERR) {
				goto err_convert_affinity; //lint !e801
			}
		}
	}

	//
	// Free the old affinity lists, and assign the new ones to it
	// instead.  Do not free the temporary lists, as we do shallow
	// assignment here.
	//
	free_rgm_affinities(affinities);
	affinities->ap_strong_pos = new_strong_pos;
	affinities->ap_weak_pos = new_weak_pos;
	affinities->ap_strong_neg = new_strong_neg;
	affinities->ap_weak_neg = new_weak_neg;
	affinities->ap_failover_delegation = new_fo_delegation;

	return (res);

err_convert_affinity:
	//
	// Free our temporary lists, as we are not using them
	//
	rgm_free_nlist(new_strong_pos);
	rgm_free_nlist(new_weak_pos);
	rgm_free_nlist(new_strong_neg);
	rgm_free_nlist(new_weak_neg);
	rgm_free_nlist(new_fo_delegation);

	ucmm_print("convert_affinities_list", NOGET("Error converting "
	    "affinities list for resource group %s\n"), rg_name);

	return (res);
}


//
// free_rgm_affinities
// -------------------
// Frees the five affinities namelists.  Does not free the affinities structure
// itself.
//
void
free_rgm_affinities(rgm_affinities_t *affinities)
{
	if (affinities->ap_strong_pos != NULL) {
		rgm_free_nlist(affinities->ap_strong_pos);
		affinities->ap_strong_pos = NULL;
	}

	if (affinities->ap_weak_pos != NULL) {
		rgm_free_nlist(affinities->ap_weak_pos);
		affinities->ap_weak_pos = NULL;
	}

	if (affinities->ap_strong_neg != NULL) {
		rgm_free_nlist(affinities->ap_strong_neg);
		affinities->ap_strong_neg = NULL;
	}

	if (affinities->ap_weak_neg != NULL) {
		rgm_free_nlist(affinities->ap_weak_neg);
		affinities->ap_weak_neg = NULL;
	}

	if (affinities->ap_failover_delegation != NULL) {
		rgm_free_nlist(affinities->ap_failover_delegation);
		affinities->ap_failover_delegation = NULL;
	}
}


//
// sp_affinities_satisfied
// ------------------------
// Returns B_TRUE if all rgs for which rgp has a strong positive affinity
// are online or pending online on node nodeid.  Otherwise return B_FALSE.
//
// With physical node affinities (the default for base clusters with more than
// one physical node), the affinity RGs may be online on any zone on the
// same physical node as 'nodeid'.  Otherwise they must be online on
// 'nodeid' itself.
//
// The extras_online parameter contains a list of RGs that are going to be
// switched on to node nodeid at the same time as rgp.
//
// If returns B_FALSE, *offenders will contain a list of all RGs for which
// rgp has a strong, positive, affinity, that are not online on node nodeid,
// unless err is set to SCHA_ERR_NOMEM, in which case there was a severe
// memory error.
//
// The caller is responsible for freeing the memory in the list.
//
boolean_t
sp_affinities_satisfied(scha_err_t &err, rglist_p_t rgp, rgm::lni_t nodeid,
    namelist_t *extras_online, namelist_t **offenders)
{
	namelist_t *affs;
	rglist_p_t rg_aff;
	boolean_t all_ok = B_TRUE;
	*offenders = NULL;
	err = SCHA_ERR_NOERR;

	for (affs = rgp->rgl_affinities.ap_strong_pos; affs != NULL;
	    affs = affs->nl_next) {
#if SOL_VERSION >= __s10
		if (is_enhanced_naming_format_used(affs->nl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "sp_affinities_satisfied "
				    "Running at too low a version:RG <%s> "
				    "has affinity for RG <%s> "
				    "which is in not in local cluster\n"),
				    rgp->rgl_ccr->rg_name, affs->nl_name);
				continue;
			}
			boolean_t rg_status = B_FALSE;
			rg_status = get_remote_rg_status(affs->nl_name, nodeid,
				RG_ONLINE_STATUS);
			if (!rg_status && !namelist_exists(
			    extras_online, affs->nl_name)) {
				ucmm_print("sp_affinities_satisifed",
				    NOGET("RG <%s> is not online on node "
				    "<%d>\n"),
				    affs->nl_name, nodeid);
				if (namelist_add_name(offenders,
				    affs->nl_name).err_code !=
				    SCHA_ERR_NOERR) {
					err = SCHA_ERR_NOMEM;
					rgm_free_nlist(*offenders);
					*offenders = NULL;
					return (B_FALSE);
				}
				all_ok = B_FALSE;
			}
			continue;
		}
#endif
		rg_aff = rgname_to_rg(affs->nl_name);
		CL_PANIC(rg_aff != NULL);
		//
		// Use is_rg_online_not_errored, which rules out error
		// or pending error states (like stop_failed).
		//
		// If it's not already online, or a member of the list of
		// RGs that will be going online at the same time, it's an
		// error.
		//
		if (!is_rg_online_not_errored(rg_aff, nodeid, B_FALSE,
		    use_logicalnode_affinities(), NULL) &&
		    !namelist_exists(extras_online, affs->nl_name)) {
			ucmm_print("sp_affinities_satisifed",
			    NOGET("RG <%s> is not online on node <%d>\n"),
			    rg_aff->rgl_ccr->rg_name, nodeid);
			if (namelist_add_name(offenders, rg_aff->
			    rgl_ccr->rg_name).err_code != SCHA_ERR_NOERR) {
				err = SCHA_ERR_NOMEM;
				rgm_free_nlist(*offenders);
				*offenders = NULL;
				return (B_FALSE);
			}
			all_ok = B_FALSE;
		}
	}
	return (all_ok);
}


//
// sn_affinities_satisfied
// ------------------------
// Returns B_TRUE if all rgs for which rgp has a strong negative affinity
// are offline or pending_offline on node nodeid.  Otherwise return B_FALSE.
//
// With physical node affinities (the default for base clusters with more than
// one physical node), the affinity RGs must be offline on all zones on the
// same physical node as 'nodeid'.  Otherwise they need be offline only on
// 'nodeid' itself.
//
// The extras_online parameter contains a list of RGs that are going to be
// switched on to node nodeid at the same time as rgp is switched on.
//
// If returns B_FALSE, *offenders will contain a list of all RGs for which
// rgp has a strong, negative affinity, that are online on node nodeid, unless
// err is set to SCHA_ERR_NOMEM, in which case there was a memory error.
//
// The caller is responsible for freeing the memory in the list.
//
boolean_t
sn_affinities_satisfied(scha_err_t &err, rglist_p_t rgp, rgm::lni_t nodeid,
    namelist_t *extras_online, namelist_t **offenders)
{
	namelist_t *affs;
	rglist_p_t rg_aff;
	boolean_t all_ok = B_TRUE;
	*offenders = NULL;
	err = SCHA_ERR_NOERR;

	for (affs = rgp->rgl_affinities.ap_strong_neg; affs != NULL;
	    affs = affs->nl_next) {
#if SOL_VERSION >= __s10
		if (is_enhanced_naming_format_used(affs->nl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "sn_affinities_satisfied "
				    "Running at too low a version:RG <%s> "
				    "has affinity for RG <%s> "
				    "which is in not in local cluster\n"),
				    rgp->rgl_ccr->rg_name, affs->nl_name);
				continue;
			}
			boolean_t rg_status = B_FALSE;
			rg_status = get_remote_rg_status(affs->nl_name, nodeid,
			    RG_ONLINE_STATUS);
			if (rg_status || namelist_exists(extras_online,
			    affs->nl_name)) {
				ucmm_print("sn_affinities_satisifed",
				    NOGET("RG <%s> is online on node "
				    "<%d>\n"),
				    affs->nl_name, nodeid);
				if (namelist_add_name(offenders,
				    affs->nl_name).err_code !=
				    SCHA_ERR_NOERR) {
					err = SCHA_ERR_NOMEM;
					rgm_free_nlist(*offenders);
					*offenders = NULL;
					return (B_FALSE);
				}
				all_ok = B_FALSE;
			}
			continue;
		}
#endif
		rg_aff = rgname_to_rg(affs->nl_name);
		CL_PANIC(rg_aff != NULL);
		//
		// Use is_rg_online_on_node, which includes error state like
		// ERROR_STOP_FAILED, which we want to avoid.
		//
		// If it's not already offline, or it is going to be switched
		// online at the same time, it's an error.
		//
		if (is_rg_online_on_node(rg_aff, nodeid,
		    use_logicalnode_affinities()) ||
		    namelist_exists(extras_online, affs->nl_name)) {
			ucmm_print("sn_affinities_satisifed",
			    NOGET("RG <%s> is online on node <%d>\n"),
			    rg_aff->rgl_ccr->rg_name, nodeid);
			if (namelist_add_name(offenders, rg_aff->
			    rgl_ccr->rg_name).err_code != SCHA_ERR_NOERR) {
				err = SCHA_ERR_NOMEM;
				rgm_free_nlist(*offenders);
				*offenders = NULL;
				return (B_FALSE);
			}
			all_ok = B_FALSE;
		}
	}
	return (all_ok);
}

#if SOL_VERSION >= __s10
//
// Helper function to get the switching, online, error stop failed status
// of remote rg.
//
boolean_t get_remote_rg_status(char *rg_name, sol::nodeid_t nodeid,
    status_type status)
{
	rgm::rgm_comm_var prgm_pres_v;
	rgm::idl_remote_rg_get_state_args gets_args;
	rgm::idl_remote_rg_get_state_result_t_var gets_res;
	Environment e;
	char *rc = NULL, *rr = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	(void) get_remote_cluster_and_rorrg_name(
	    rg_name, &rc, &rr);
	if ((rr == NULL) || (rc == NULL))
		return (B_FALSE);

	prgm_pres_v = rgm_comm_getpres_zc(rc);
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_IC_UNREACH,
		    rc);
		free(rr);
		free(rc);
		return (B_FALSE);
	}

	gets_args.idlstr_cluster_name = strdup(rc);
	gets_args.idlstr_rg_name = strdup(rr);
	if (nodeid != NULL)
		gets_args.nid = nodeid;
	gets_args.flag = 0;
	if (status == RG_ONLINE_STATUS) {
		gets_args.flag |=  rgm::RG_ONLINE_STATUS;
	} else if (status == RG_SWITCH_STATUS) {
		gets_args.flag |=  rgm::RG_SWITCH_STATUS;
	} else {
		gets_args.flag |=  rgm::RG_ERR_STOP_FAILED_STATUS;
	}

	prgm_pres_v->idl_get_remote_rg_state(gets_args,
	    gets_res, e);
	if ((res = except_to_scha_err(e)).err_code !=
	    SCHA_ERR_NOERR) {
		rgm_format_errmsg(&res,
		    REM_RGM_GET_REMOTE_R_STATE_FAILED,
		    rc, rr);
		free(rr);
		free(rc);
		return (B_FALSE);
	}

	res.err_code = (scha_err_t)gets_res->ret_code;
	if (res.err_code == SCHA_ERR_NOERR) {
		if (status == RG_ONLINE_STATUS) {
			if (gets_res->status == rgm::RG_ONLINE) {
				free(rr);
				free(rc);

				return (B_TRUE);
			}
		} else if (status == RG_SWITCH_STATUS) {
				if (gets_res->switch_flag) {
					free(rr);
					free(rc);
					return (B_TRUE);
				}
		} else {
				if ((gets_res->status ==
				    rgm::RG_ERROR_STOP_FAILED) ||
				    (gets_res->status ==
				    rgm::RG_PENDING_OFF_STOP_FAILED)) {
					free(rr);
					free(rc);
					return (B_TRUE);
				}
		}
	}
	free(rr);
	free(rc);
	return (B_FALSE);
}


scha_errmsg_t
process_remote_rg_sp_affinities(char *rg_name,  namelist_t **nl,
    rgm_lni_t nodeid, boolean_t chk_stop_failed)
{
	rgm::rgm_comm_var prgm_pres_v;
	rgm::idl_remote_rg_sp_aff_check_args gets_args;
	rgm::idl_remote_rg_sp_aff_check_result_t_var gets_res;
	Environment e;
	char *rc = NULL, *rr = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	(void) get_remote_cluster_and_rorrg_name(
	    rg_name, &rc, &rr);

	if ((rr == NULL) || (rc == NULL)) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	prgm_pres_v = rgm_comm_getpres_zc(rc);
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_IC_UNREACH,
		    rc);
		free(rr);
		free(rc);
		return (res);
	}

	gets_args.idlstr_cluster_name = strdup(rc);
	gets_args.idlstr_rg_name = strdup(rr);
	gets_args.nid = nodeid;
	gets_args.chk_stop_failed_flag = chk_stop_failed;


	prgm_pres_v->idl_check_remote_rg_sp_aff(gets_args,
	    gets_res, e);
	if ((res = except_to_scha_err(e)).err_code !=
	    SCHA_ERR_NOERR) {
		rgm_format_errmsg(&res,
		    REM_RGM_GET_REMOTE_R_STATE_FAILED,
		    rc, rr);
		free(rr);
		free(rc);
		return (res);
	}

	res.err_code = (scha_err_t)gets_res->ret_code;

	if (res.err_code == SCHA_ERR_NOERR) {
		int len = gets_res->rg_list.length();
		for (int i = 0; i < len; i++)
			namelist_add_name(nl, gets_res->rg_list[i]);
		free(rr);
		free(rc);
		return (res);
	}

	if ((const char *)gets_res->idlstr_err_msg)
		res.err_msg = strdup(gets_res->idlstr_err_msg);

	free(rr);
	free(rc);
	return (res);
}
#endif

//
// compute_sn_violations
// ----------------------
// Returns a list of all the RGs that have a strong negative affinity for
// rgp that are online or pending_online on node nodeid.
//
// With physical node affinities (the default for base clusters with more than
// one physical node), the affinity RGs may be online on any zone on the
// same physical node as 'nodeid'.  Otherwise, we only check their state
// on 'nodeid' itself.
//
// If any of the affinitent RGs is found to be STOP_FAILED or
// PENDING_OFF_STOP_FAILED on one of the checked zones, aborts
// the function and returns B_FALSE without populating the violations list.
// Otherwise, continues checking all SN affinitent RGs.
//
// RGs that are PENDING_OFFLINE get added to the namelist only
// if the count_pending_offline parameter is B_TRUE.
//
// If the violations list is non-NULL, the caller is responsible for freeing
// the memory.
//
// Note that err can be set to SCHA_ERR_NOMEM, in which case violations
// will not contain any names.
//
boolean_t
compute_sn_violations(scha_err_t &err, rglist_p_t rgp,
    rgm::lni_t nodeid, namelist_t **violations, int &count,
    boolean_t count_pending_offline)
{
	namelist_t *affs;
	rglist_p_t rg_aff;
	rgm::lni_t error_node = 0;

	*violations = NULL;
	count = 0;
	char *name_str = NULL;
	err = SCHA_ERR_NOERR;

	for (affs = rgp->rgl_affinitents.ap_strong_neg; affs != NULL;
	    affs = affs->nl_next) {
#if SOL_VERSION >= __s10
		if (is_enhanced_naming_format_used(affs->nl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "compute_sn_violations "
				    "Running at too low a version:RG <%s> "
				    "has affinity for RG <%s> "
				    "which is in not in local cluster\n"),
				    rgp->rgl_ccr->rg_name, affs->nl_name);
				continue;
			}
			if (get_remote_rg_status(affs->nl_name, nodeid,
				RG_ONLINE_STATUS)) {
				//
				// The RG is online on the node.  Add it to the
				// namelist.
				//
				ucmm_print("compute_sn_violations",
				    NOGET("RG <%s> is online on node <%d>\n"),
				    affs->nl_name, nodeid);
				if (namelist_add_name(violations,
				    affs->nl_name).err_code !=
				    SCHA_ERR_NOERR) {
					err = SCHA_ERR_NOMEM;
					goto compute_sn_violations_err;
				}
				count++;
			} else if (get_remote_rg_status(affs->nl_name, nodeid,
			    RG_ERROR_STOP_FAILED_STATUS)) {
				//
				// We found an error state.
				//
				ucmm_print("compute_sn_violations",
				    NOGET("RG <%s> is errored on node <%d>\n"),
				    affs->nl_name, nodeid);
				goto compute_sn_violations_err; //lint !e801
			}
			continue;
		}
#endif
		rg_aff = rgname_to_rg(affs->nl_name);
		CL_PANIC(rg_aff != NULL);
		if (is_rg_online_not_errored(rg_aff, nodeid,
		    count_pending_offline, use_logicalnode_affinities(),
		    &error_node)) {
			//
			// The RG is online on the node.  Add it to the
			// namelist.
			//
			ucmm_print("compute_sn_violations",
			    NOGET("RG <%s> is online on node <%d>\n"),
			    rg_aff->rgl_ccr->rg_name, nodeid);
			if (namelist_add_name(violations, affs->nl_name).
			    err_code != SCHA_ERR_NOERR) {
				err = SCHA_ERR_NOMEM;
				goto compute_sn_violations_err; //lint !e801
			}
			count++;
		} else if (error_node != 0) {
			//
			// We found an error state.
			//
			ucmm_print("compute_sn_violations",
			    NOGET("RG <%s> is errored on node <%d>\n"),
			    rg_aff->rgl_ccr->rg_name, error_node);
			goto compute_sn_violations_err; //lint !e801
		}
	}
	name_str = namelist_to_str(*violations);
	ucmm_print("compute_sn_violations",
	    NOGET("Found SN violations <%s> for RG <%s> on node <%d>\n"),
	    name_str ? name_str : "", rgp->rgl_ccr->rg_name, nodeid);
	free(name_str);
	return (B_TRUE);

compute_sn_violations_err:

	// Free the namelist and return B_FALSE.
	rgm_free_nlist(*violations);
	*violations = NULL;
	count = 0;
	return (B_FALSE);
}


#ifndef EUROPA_FARM
//
// compute_sp_transitive_closure
// -----------------------------
// Computes a list of all RGs who have a strong positive affinity for
// an RG in start_list (recursively), and who are ONLINE or PENDING_ONLINE
// on node nodeid.
//
// With physical node affinities (the default for base clusters with more than
// one physical node), the affinitent RGs may be online on any zone on the
// same physical node as 'nodeid'.  Otherwise, they must be online on
// 'nodeid' itself.
//
// If the check_stop_failed flag is set, if any RG in the list is STOP_FAILED
// or PENDING_OFF_STOP_FAILED, aborts the function and returns B_FALSE.
// Otherwise returns B_TRUE.
//
// Note that RGs that are PENDING_OFFLINE do not get added to the namelist.
//
// If the tclist is non-NULL, the caller is responsible for freeing
// the memory.
//
// If check_start_list is set to B_TRUE, validates that start_list contains
// only RGs that are online or pending_online.  Does not change start_list.
//
// If err is set to SCHA_ERR_NOMEM, there was a severe memory error.
// tclist will not contain any names.
//
boolean_t
compute_sp_transitive_closure(scha_err_t &err, rglist_p_t rgp,
    sol::nodeid_t nodeid, namelist_t *start_list, namelist_t **tclist,
    int &count, boolean_t check_stop_failed, boolean_t check_start_list)
{
	//
	// Start off the list with a copy of the start_list.
	//
	namelist_t *new_list = NULL;
	namelist_t *remote_list = NULL;
	namelist_t *temp_list;
	namelist_t *t_list;
	namelist_t *affs;
	rglist_p_t cur_rg, rg_aff;
	rgm::lni_t error_node;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	*tclist = NULL;
	count = 0;
	char *name_str = NULL;
	err = SCHA_ERR_NOERR;

	//
	// If we're supposed to check the start list, we iterate through
	// it, only copying names to the new_list if the RG is online on
	// the node and not start failed.
	//
	// Otherwise we just copy the start list to the new_list.
	//
	if (check_start_list) {
		for (temp_list = start_list; temp_list != NULL;
		    temp_list = temp_list->nl_next) {
#if SOL_VERSION >= __s10
			if (is_enhanced_naming_format_used(
			    temp_list->nl_name)) {
				if (!allow_inter_cluster()) {
					ucmm_print("RGM", NOGET(
					    "compute_sp_transitive_closure "
					    "Running at too low a version:RG "
					    "<%s> has affinity for RG <%s> "
					    "which is not in local cluster\n"),
					    rgp->rgl_ccr->rg_name,
					    temp_list->nl_name);
					continue;
				}
				if (get_remote_rg_status(temp_list->nl_name,
				    nodeid, RG_ONLINE_STATUS)) {
					//
					// The RG is online on the node.
					// Add it to the namelist.
					//
					ucmm_print(
					    " compute_sp_transitive_closure",
					    NOGET("RG <%s> is online on "
					    " node <%d>\n"),
					    temp_list->nl_name, nodeid);
					if (!namelist_exists(new_list,
					    temp_list->nl_name)) {
						if (namelist_add_name(
						    &new_list,
						    temp_list->nl_name).
						    err_code !=
						    SCHA_ERR_NOERR) {
							err = SCHA_ERR_NOMEM;
							goto tclist_err;
						}
					}
				} else if (get_remote_rg_status
				    (temp_list->nl_name, nodeid,
				    RG_ERROR_STOP_FAILED_STATUS)) {
					//
					// We found an error state.
					//
					ucmm_print("compute_sn_violations",
					    NOGET("RG <%s> is errored on "
					    "node <%d>\n"),
					    temp_list->nl_name, nodeid);
					goto tclist_err; //lint !e801
				}
				continue;
			}
#endif
			cur_rg = rgname_to_rg(temp_list->nl_name);
			CL_PANIC(cur_rg != NULL);
			error_node = 0;
			if (is_rg_online_not_errored(cur_rg, nodeid,
			    B_FALSE, use_logicalnode_affinities(),
			    &error_node)) {
				//
				// The RG is online on the node.  Add it to the
				// namelist.
				//
				ucmm_print("compute_sp_transitive_closure",
				    NOGET("RG <%s> is online on phys node "
				    "of lni <%d>\n"),
				    cur_rg->rgl_ccr->rg_name, nodeid);
				if (namelist_add_name(&new_list,
				    temp_list->nl_name).err_code !=
				    SCHA_ERR_NOERR) {
					err = SCHA_ERR_NOMEM;
					goto tclist_err; //lint !e801
				}
			} else if (check_stop_failed && (error_node != 0)) {
				//
				// We found an error state.
				//
				ucmm_print("compute_sp_transitive_closure",
				    NOGET("RG <%s> is errored on node <%d>\n"),
				    cur_rg->rgl_ccr->rg_name, error_node);
				goto tclist_err; //lint !e801
			}
		}
	} else {
		new_list = namelist_copy(start_list);
	}
	//
	// Now we find all RGs that are online (or pending_online) on the
	// node that have strong positive affinities (perhaps transitively)
	// to any RG in the list.
	//
	// For each RG in the list, add all its strong positive affinitents
	// to the list (if they are online or pending online, and are not
	// already in the list).
	//
	// Note that this loop through the list, we add names to the end of the
	// list.  This is ok because:
	// 1. namelist_add adds names only to the end of the list.
	// 2. We are guarenteed to terminate because the number of RGs is
	// finite, and because we check for duplicates before re-adding an RG.
	//
	for (temp_list = new_list; temp_list != NULL;
	    temp_list = temp_list->nl_next) {
		count++;
#if SOL_VERSION >= __s10
		if (is_enhanced_naming_format_used(temp_list->nl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "compute_sp_transitive_closure "
				    "Running at too low a version:RG <%s> "
				    "has affinity for RG <%s> "
				    "which is in not in local cluster\n"),
				    rgp->rgl_ccr->rg_name, temp_list->nl_name);
				continue;
			}
			res = process_remote_rg_sp_affinities(
			    temp_list->nl_name, &remote_list, nodeid,
			    check_stop_failed);
			if (res.err_code != SCHA_ERR_NOERR) {
				err = res.err_code;
				goto tclist_err;
			}
			for (t_list = remote_list; t_list != NULL;
			    t_list = t_list->nl_next) {
				if (!namelist_exists(new_list,
				    t_list->nl_name)) {
					if (namelist_add_name(&new_list,
						t_list->nl_name).err_code !=
					    SCHA_ERR_NOERR) {
						err = SCHA_ERR_NOMEM;
						goto tclist_err; //lint !e801
					}
				}
			}
			continue;
		}
#endif
		cur_rg = rgname_to_rg(temp_list->nl_name);
		CL_PANIC(cur_rg != NULL);
		for (affs = cur_rg->rgl_affinitents.ap_strong_pos;
		    affs != NULL; affs = affs->nl_next) {
#if SOL_VERSION >= __s10
			//
			// If this affinitent is a remote RG,
			// then we have to contact the remote cluster
			// to fetch the affinitent's state.
			//
			if (is_enhanced_naming_format_used(affs->nl_name)) {
				if (!allow_inter_cluster()) {
					ucmm_print("RGM", NOGET(
					    "compute_sp_transitive_closure "
					    "Running at too low a version:RG "
					    "<%s> has affinity for RG <%s> "
					    "which is not in local cluster\n"),
					    temp_list->nl_name, affs->nl_name);
					continue;
				}
				if (get_remote_rg_status(affs->nl_name,
				    nodeid, RG_ONLINE_STATUS)) {
					//
					// The RG is online on the node.
					// Add it to the namelist.
					//
					ucmm_print(
					    "compute_sp_transitive_closure",
					    NOGET("RG <%s> is online on "
					    "node <%d>\n"),
					    affs->nl_name, nodeid);
					if (!namelist_exists(new_list,
					    affs->nl_name)) {
						if (namelist_add_name(&new_list,
						    affs->nl_name).err_code !=
						    SCHA_ERR_NOERR) {
							err = SCHA_ERR_NOMEM;
							goto tclist_err;
						}
					}
				} else if (get_remote_rg_status
				    (affs->nl_name, nodeid,
				    RG_ERROR_STOP_FAILED_STATUS)) {
					//
					// We found an error state.
					//
					ucmm_print("compute_sn_violations",
					    NOGET("RG <%s> is errored on "
					    "node <%d>\n"),
					affs->nl_name, nodeid);
					goto tclist_err; //lint !e801
				}
				continue;
			}
#endif
			//
			// This affinitent is a local cluster RG
			//
			rg_aff = rgname_to_rg(affs->nl_name);
			CL_PANIC(rg_aff != NULL);
			error_node = 0;
			if (is_rg_online_not_errored(rg_aff, nodeid,
			    B_FALSE, use_logicalnode_affinities(),
			    &error_node)) {
				//
				// The RG is online on the node.  Add it to the
				// namelist.
				//
				ucmm_print("compute_sp_transitive_closure",
				    NOGET("RG <%s> is online on node <%d>\n"),
				    rg_aff->rgl_ccr->rg_name, nodeid);
				if (!namelist_exists(new_list,
				    affs->nl_name)) {
					if (namelist_add_name(&new_list,
					    affs->nl_name).err_code !=
					    SCHA_ERR_NOERR) {
						err = SCHA_ERR_NOMEM;
						goto tclist_err; //lint !e801
					}
				}
			} else if (check_stop_failed && (error_node != 0)) {
				//
				// We found an error state.
				//
				ucmm_print("compute_sp_transitive_closure",
				    NOGET("RG <%s> is errored on node <%d>\n"),
				    rg_aff->rgl_ccr->rg_name, error_node);
				goto tclist_err; //lint !e801
			}
		}
	}
	*tclist = new_list;
	name_str = namelist_to_str(*tclist);
	ucmm_print("compute_sp_transitive_closure",
	    NOGET("Found tclist <%s> for RG <%s> on node <%d>\n"),
	    name_str ? name_str : "", rgp->rgl_ccr->rg_name, nodeid);
	free(name_str);

	return (B_TRUE);

tclist_err:
	// Free the namelist and return B_FALSE.
	rgm_free_nlist(new_list);
	*tclist = NULL;
	count = 0;
	return (B_FALSE);
}
#endif // !EUROPA_FARM

//
// compute_enabled_resources
// -------------------------
// Score starts at 0. Add 1 to the score for each resource of the
// resource group enabled on the lni.
//
uint_t
compute_enabled_resources(const rglist_p_t rgp, const rgm::lni_t lni)
{
	uint_t count = 0;
	rlist_p_t rp;

	CL_PANIC(rgp != NULL);

	for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		if (((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->
		    r_switch[lni] == SCHA_SWITCH_ENABLED)
			count++;
	}
	return (count);
}

//
// compute_outgoing_weak_affinities
// --------------------------------
// Computes a score as follows:
//
// Score starts at 0.
// a) Add 1 to score for each RG rg1 that meets the following criteria:
//    rg1 is currently online or pending online on node nodeid
//    rg1 is not a member of offlist
//    rgp declares a weak positive affinity for rg1
//
// b) Subtract 1 from score for each RG rg2 such that:
//    rg2 is currently offline or pending offline on node nodeid or
//    is a member of offlist
//    rgp declares a weak positive affinity for rg2
//
// c) Subtract 1 from score for each RG rg3 such that:
//    rg3 is currently online or pending online on node nodid
//    rg3 is not a member of offlist
//    rgp declares a weak negative affinity for rg3
//
int
compute_outgoing_weak_affinities(rglist_p_t rgp, sol::nodeid_t nodeid,
    namelist_t *offlist)
{
	namelist_t *affs;
	rglist_p_t cur_rg;
	int count = 0;

	ucmm_print("compute_outgoing_weak_affinities",
	    NOGET("compute_outgoing_weak_affinities called for RG <%s> on "
	    "node <%d>."), rgp->rgl_ccr->rg_name, nodeid);

	// first, process weak positive affinities
	for (affs = rgp->rgl_affinities.ap_weak_pos; affs != NULL;
	    affs = affs->nl_next) {
#if SOL_VERSION >= __s10
		if (is_enhanced_naming_format_used(affs->nl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "compute_outgoing_weak_affinities "
				    "Running at too low a version:RG <%s> "
				    "has affinity for RG <%s> "
				    "which is in not in local cluster\n"),
				    rgp->rgl_ccr->rg_name, affs->nl_name);
				continue;
			}
			boolean_t rg_status = B_FALSE;
			rg_status = get_remote_rg_status(affs->nl_name, nodeid,
				RG_ONLINE_STATUS);
			if (rg_status && !namelist_exists(
			    offlist, affs->nl_name)) {
				ucmm_print("compute_outgoing_weak_affinities",
				    NOGET("Found WP RG <%s> on or arriving\n"),
				    affs->nl_name);
				count++;
			} else {
				ucmm_print("compute_outgoing_weak_affinities",
				    NOGET("Found WP RG <%s> off or leaving\n"),
				    affs->nl_name);
				// otherwise, it counts -1.
				count--;
			}
			continue;
		}
#endif
		cur_rg = rgname_to_rg(affs->nl_name);
		CL_PANIC(cur_rg != NULL);
		//
		// If the RG is online or pending online, and is
		// not in offlist, it counts +1.
		//
		if (is_rg_online_not_errored(cur_rg, nodeid, B_FALSE,
		    use_logicalnode_affinities(), NULL) &&
		    !namelist_exists(offlist, affs->nl_name)) {
			ucmm_print("compute_outgoing_weak_affinities",
			    NOGET("Found WP RG <%s> on or arriving\n"),
			    affs->nl_name);
			count++;
		} else {
			ucmm_print("compute_outgoing_weak_affinities",
			    NOGET("Found WP RG <%s> off or leaving\n"),
			    affs->nl_name);
			// otherwise, it counts -1.
			count--;
		}
	}

	//
	// now process weak negative affinities
	//
	for (affs = rgp->rgl_affinities.ap_weak_neg; affs != NULL;
	    affs = affs->nl_next) {
#if SOL_VERSION >= __s10
		if (is_enhanced_naming_format_used(affs->nl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "compute_outgoing_weak_affinities "
				    "Running at too low a version:RG <%s> "
				    "has affinity for RG <%s> "
				    "which is in not in local cluster\n"),
				    rgp->rgl_ccr->rg_name, affs->nl_name);
				continue;
			}
			boolean_t rg_status = B_FALSE;
			rg_status = get_remote_rg_status(affs->nl_name, nodeid,
				RG_ONLINE_STATUS);
			if (rg_status && !namelist_exists(
			    offlist, affs->nl_name)) {
				ucmm_print("compute_outgoing_weak_affinities",
				    NOGET("Found WN RG <%s> on or arriving\n"),
				    affs->nl_name);
				// otherwise, it counts -1.
				count--;
			}
			continue;
		}
#endif
		cur_rg = rgname_to_rg(affs->nl_name);
		CL_PANIC(cur_rg != NULL);
		//
		// If the RG is online or pending online, and is
		// not in offlist, it counts -1.  Otherwise it doesn't
		// count positive or negative.
		//
		if (is_rg_online_not_errored(cur_rg, nodeid, B_FALSE,
		    use_logicalnode_affinities(), NULL) &&
		    !namelist_exists(offlist, affs->nl_name)) {
			ucmm_print("compute_outgoing_weak_affinities",
			    NOGET("Found WN RG <%s> on or arriving\n"),
			    affs->nl_name);
			count--;
		}
	}
	return (count);
}


//
// compute_incoming_weak_affinities
// --------------------------------
// Computes a score as follows:
//
// Score starts at 0.
// a) Add 1 to score for each RG rg1 that meets the following criteria:
//    rg1 is currently online or pending online on node nodeid
//    rg1 is not a member of offlist
//    rg1 declares a weak positive affinity for rgp
//
// c) Subtract 1 from score for each RG rg3 such that:
//    rg3 is currently online or pending online on node nodid
//    rg3 is not a member of offlist
//    rg3 declares a weak negative affinity for rgp
//
int
compute_incoming_weak_affinities(rglist_p_t rgp, sol::nodeid_t nodeid,
    namelist_t *offlist)
{
	namelist_t *affs;
	rglist_p_t cur_rg;
	int count = 0;

	ucmm_print("compute_incoming_weak_affinities",
	    NOGET("compute_incoming_weak_affinities called for RG <%s> on "
	    "node <%d>."), rgp->rgl_ccr->rg_name, nodeid);

	// first, process weak positive affinities
	for (affs = rgp->rgl_affinitents.ap_weak_pos; affs != NULL;
	    affs = affs->nl_next) {
#if SOL_VERSION >= __s10
		if (is_enhanced_naming_format_used(affs->nl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "compute_incoming_weak_affinities "
				    "Running at too low a version:RG <%s> "
				    "has affinity for RG <%s> "
				    "which is in not in local cluster\n"),
				    rgp->rgl_ccr->rg_name, affs->nl_name);
				continue;
			}
			boolean_t rg_status = B_FALSE;
			rg_status = get_remote_rg_status(affs->nl_name, nodeid,
				RG_ONLINE_STATUS);
			if (rg_status && !namelist_exists(
			    offlist, affs->nl_name)) {
				ucmm_print("compute_incoming_weak_affinities",
				    NOGET("Found WP RG <%s> on or arriving\n"),
				    affs->nl_name);
				count++;
			}
			continue;
		}
#endif
		cur_rg = rgname_to_rg(affs->nl_name);
		CL_PANIC(cur_rg != NULL);
		//
		// If the RG is online or pending online, and is
		// not in offlist, it counts +1.  Otherwise it doesn't
		// count positive or negative.
		//
		if (is_rg_online_not_errored(cur_rg, nodeid, B_FALSE,
		    use_logicalnode_affinities(), NULL) &&
		    !namelist_exists(offlist, affs->nl_name)) {
			ucmm_print("compute_incoming_weak_affinities",
			    NOGET("Found WP RG <%s> on or arriving\n"),
			    affs->nl_name);
			count++;
		}
	}


	//
	// now process weak negative affinities
	//
	for (affs = rgp->rgl_affinitents.ap_weak_neg; affs != NULL;
	    affs = affs->nl_next) {
#if SOL_VERSION >= __s10
		if (is_enhanced_naming_format_used(affs->nl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "compute_incoming_weak_affinities "
				    "Running at too low a version:RG <%s> "
				    "has affinity for RG <%s> "
				    "which is in not in local cluster\n"),
				    rgp->rgl_ccr->rg_name, affs->nl_name);
				continue;
			}
			boolean_t rg_status = B_FALSE;
			rg_status = get_remote_rg_status(
			    affs->nl_name, nodeid, RG_ONLINE_STATUS);
			if (rg_status && !namelist_exists(
			    offlist, affs->nl_name)) {
				ucmm_print("compute_incoming_weak_affinities",
				    NOGET("Found WN RG <%s> on or arriving\n"),
				    affs->nl_name);
				count++;
			}
			continue;
		}
#endif
		cur_rg = rgname_to_rg(affs->nl_name);
		CL_PANIC(cur_rg != NULL);
		//
		// If the RG is online or pending online, and is
		// not in offlist, it counts -1.  Otherwise it doesn't
		// count positive or negative.
		//
		if (is_rg_online_not_errored(cur_rg, nodeid, B_FALSE,
		    use_logicalnode_affinities(), NULL) &&
		    !namelist_exists(offlist, affs->nl_name)) {
			ucmm_print("compute_incoming_weak_affinities",
			    NOGET("Found WN RG <%s> on or arriving\n"),
			    affs->nl_name);
			count--;
		}
	}
	return (count);
}


//
// check_nw_res_used
//
// First, check for duplicate names in the nw_res_used list.
//
// Then, perform the following checks for each resource in the
// nw_res_used list:
// 1. Make sure it's a valid resource.
// 2. Make sure it's a network resource.
// 3. If src_res is not enabled, make sure all the network res' are disabled.
//
scha_errmsg_t
check_nw_res_used(rglist_p_t rgp, rgm_resource_t *src_res,
    namelist_t *nw_res_used)
{

	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rlist_p_t rp = NULL;
	const char 	*res_dup_name;

	if (nw_res_used == NULL)
		return (res);

	//
	// if the network resource used is set to empty, the name in the first
	// array is NULL.  We can skip the checking
	//
	if ((nw_res_used->nl_name[0] == '\0') && nw_res_used->nl_next == NULL)
		return (res);

	//
	// First check: for duplicate names.
	// Get the duplicate name, if any.
	//
	res_dup_name = get_dup_name(nw_res_used);

	// It's an error to have a duplicate
	if (res_dup_name != NULL) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_DUP_RS_NETRESUSED, src_res->r_name,
		    res_dup_name);
		return (res);
	}


	for (namelist_t *nr_used = nw_res_used; nr_used != NULL;
	    nr_used = nr_used->nl_next) {
		//
		// Check #1: lookup the resource
		//

		//
		// The resource can be in any RG
		//
		rp = rname_to_r(NULL, nr_used->nl_name);
		if (rp == NULL) {
			//
			// The resource doesn't exist.
			//
			ucmm_print("check_nw_res_used()",
			    NOGET("Invalid Resource <%s>\n"),
			    nr_used->nl_name);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INVALID_RS,
			    nr_used->nl_name);
			return (res);
		}

		//
		// Check #2: make sure it's a network resource.
		//
		if (!((rp->rl_ccrtype->rt_sysdeftype ==
		    SYST_LOGICAL_HOSTNAME) ||
		    (rp->rl_ccrtype->rt_sysdeftype ==
		    SYST_SHARED_ADDRESS))) {
			ucmm_print("check_nw_res_used()",
			    NOGET("Res %s has nru %s that is not a nr\n"),
			    src_res->r_name, nr_used->nl_name);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RS_NOT_NR,
			    src_res->r_name, nr_used->nl_name);
			return (res);
		}

	}
	return (res);
}

// get_dup_rdepname
// Function to get duplicated name from rdep list.
// Return the duplicated name if a name duplication has been found;
// otherwise, return NULL.  Note that the returned value is a
// const char *, so it cannot be modified by the caller.
// This function replaces the previous check_dup_name function which
// did not return the duplicate name found.
const char *
get_dup_rdepname(rdeplist_t *rdep_list)
{
	rdeplist_t	*ptr0, *ptr1;

	for (ptr0 = rdep_list; ptr0; ptr0 = ptr0->rl_next) {
		for (ptr1 = ptr0->rl_next; ptr1; ptr1 = ptr1->rl_next) {
			if (strcmp(ptr0->rl_name, ptr1->rl_name) == 0)
				return (ptr0->rl_name);
		}
	}
	return (NULL);
}


//
// Function to simulate CCR read/update failures. This is a no-op unless
// debugging is enabled.
//
// It checks if there exists a file /tmp/rgm_debug_ccr. If so, it returns
// SCHA_ERR_CCR for every Nth CCR read/write call. The number N can be
// specified in the file and defaults to 3.
//
boolean_t
fail_ccr_read_write(void)
{
	static int callnum = 0;

	if (Debugflag == B_TRUE) {

		//
		// Avoid returning errors until rgmd has read the CCR for
		// the first time. Errors before that could be fatal to the
		// rgmd.
		//
		// No need to grab the rgm state lock since we are only
		// reading a flag that never gets reset once set. Since we
		// may be called with the lock already held, it could be
		// incorrect to grab the lock here.
		//
		if ((Rgm_state == NULL) || !Rgm_state->ccr_is_read)
			return (B_FALSE);

		FILE *fd;
		int callnum_to_fail = 0;

		if ((fd = fopen("/tmp/rgm_debug_ccr", "r")) != NULL) {
			(void) fscanf(fd, "%d", &callnum_to_fail);
			(void) fclose(fd);
			if (callnum_to_fail <= 0)
				callnum_to_fail = 3 /* default */;
			callnum++;
			if (callnum == callnum_to_fail) {
				// Reset callnum and cause the call to fail.
				callnum = 0;
				return (B_TRUE);
			}
		}
	}

	return (B_FALSE);
}


// get_dup_name
// Function to get duplicated name from name list.
// Return the duplicated name if a name duplication has been found;
// otherwise, return NULL.  Note that the returned value is a
// const char *, so it cannot be modified by the caller.
// This function replaces the previous check_dup_name function which
// did not return the duplicate name found.
const char *
get_dup_name(namelist_t *name_list)
{
	namelist_t	*ptr0, *ptr1;

	for (ptr0 = name_list; ptr0; ptr0 = ptr0->nl_next) {
		for (ptr1 = ptr0->nl_next; ptr1; ptr1 = ptr1->nl_next) {
			if (strcmp(ptr0->nl_name, ptr1->nl_name) == 0)
				return (ptr0->nl_name);
		}
	}
	return (NULL);
}

//
// get_dup_lni
// Function to get duplicated lni from nodeidlist, if found.
// Otherwise, returns 0.
// Only executed on president where all the LNIs are known
//
rgm::lni_t
get_dup_lni(nodeidlist_t *nl)
{
	nodeidlist_t	*ptr0, *ptr1;

	for (ptr0 = nl; ptr0; ptr0 = ptr0->nl_next) {
		for (ptr1 = ptr0->nl_next; ptr1; ptr1 = ptr1->nl_next) {
			if (ptr0->nl_lni == ptr1->nl_lni) {
				return (ptr0->nl_lni);
			}
		}
	}
	return (0);
}

//
// is_r_scalable - test whether the resource is a scalable service.
// Returns true if the Scalable system-defined property exists and is
// set to true; otherwise, returns false.
//
boolean_t
is_r_scalable(rgm_resource_t *r)
{
	rgm_property_list_t	*pp;

	/*
	 * system-defined properties are stored in r->r_properties.
	 * The value of the property is stored in ASCII form.
	 */
	pp = rgm_find_property_in_list(SCHA_SCALABLE, r->r_properties);

	// If cannot find the Scalable property, resource is not scalable
	if (pp == NULL) {
		return (B_FALSE);
	} else if (((rgm_value_t *)pp->rpl_property->rp_value)
	    ->rp_value[0] == NULL) {
		return (B_FALSE);
	} else {
		return (str2bool(((rgm_value_t *)pp->rpl_property
		    ->rp_value)->rp_value[0]));
	}
}


//
// rt_method_string()
//
// Convert the method type to the generic method name ("START", "STOP", etc.)
//
const char *
rt_method_string(method_t meth)
{
	char *methname;

	switch (meth) {
	case METH_START:
		methname = SCHA_START;
		break;
	case METH_STOP:
		methname = SCHA_STOP;
		break;
	case METH_VALIDATE:
		methname = SCHA_VALIDATE;
		break;
	case METH_UPDATE:
		methname = SCHA_UPDATE;
		break;
	case METH_INIT:
		methname = SCHA_INIT;
		break;
	case METH_FINI:
		methname = SCHA_FINI;
		break;
	case METH_BOOT:
		methname = SCHA_BOOT;
		break;
	case METH_MONITOR_START:
		methname = SCHA_MONITOR_START;
		break;
	case METH_MONITOR_STOP:
		methname = SCHA_MONITOR_STOP;
		break;
	case METH_MONITOR_CHECK:
		methname = SCHA_MONITOR_CHECK;
		break;
	case METH_PRENET_START:
		methname = SCHA_PRENET_START;
		break;
	case METH_POSTNET_STOP:
		methname = SCHA_POSTNET_STOP;
		break;
	default:
		methname = "unknown_method";
		break;
	}

	return ((const char *)methname);
}

//
// set_bool_value
//	set BOOLEAN property value to SCHA_TRUE or SCHA_FALSE
//	We allow lower case or upper case of boolean value.  However,
//	we only store upper case of boolean value in ccr.
//
scha_errmsg_t
set_bool_value(char *old_value,
    std::map<rgm::lni_t, char *> &new_value,
    char *prop_name)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	if (strcasecmp(old_value, SCHA_TRUE) == 0)
		new_value[0] = strdup_nocheck(SCHA_TRUE);
	else {
		if (strcasecmp(old_value, SCHA_FALSE) == 0)
			new_value[0] = strdup_nocheck(SCHA_FALSE);
		else {
			res.err_code = SCHA_ERR_PBOOL;
			rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
			    prop_name);
			return (res);
		}
	}
	if (new_value[0] == NULL) {
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		res.err_code = SCHA_ERR_NOMEM;
	} else
		res.err_code = SCHA_ERR_NOERR;
	return (res);
}

//
// set_bool_value_ext
//	set BOOLEAN property value to SCHA_TRUE or SCHA_FALSE
//	We allow lower case or upper case of boolean value.  However,
//	we only store upper case of boolean value in ccr.
//
scha_errmsg_t
set_bool_value_ext(std::map<rgm::lni_t, char *> &old_value,
    std::map<rgm::lni_t, char *> &new_value,
    char *prop_name, boolean_t is_per_node, LogicalNodeset *ns,
    LogicalNodeset e_ns, rgm_tune_t tuneable)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm::lni_t lni;

	if (is_per_node) {
		for (LogicalNodeManager::iter it = lnManager->begin();
		    it != lnManager->end(); ++it) {
			lni = it->second;
			if (!ns->containLni(lni))
				continue;
			if (old_value[lni] == NULL)
				continue;
			if (e_ns.containLni(it->second) &&
			    tuneable == TUNE_WHEN_DISABLED) {
				res.err_code = SCHA_ERR_CHECKS;
				rgm_format_errmsg(&res,
				    REM_PROP_TUNE_WHEN_DISABLED,
				    prop_name);
				return (res);
			}
			if (strcasecmp(old_value[lni], SCHA_TRUE) == 0)
				new_value[lni] = strdup_nocheck(SCHA_TRUE);
			else {
				if (strcasecmp(old_value[lni], SCHA_FALSE) == 0)
					new_value[lni] =
					    strdup_nocheck(SCHA_FALSE);
				else {
					res.err_code = SCHA_ERR_PBOOL;
					rgm_format_errmsg(&res,
					    REM_INVALID_PROP_VALUES,
					    prop_name);
					return (res);
				}
			}
			if (new_value[lni] == NULL) {
				rgm_format_errmsg(&res, REM_INTERNAL_MEM);
				res.err_code = SCHA_ERR_NOMEM;
			} else
				res.err_code = SCHA_ERR_NOERR;
		}
	} else {
		if (strcasecmp(old_value[0], SCHA_TRUE) == 0)
			new_value[0] = strdup_nocheck(SCHA_TRUE);
		else {
			if (strcasecmp(old_value[0], SCHA_FALSE) == 0)
				new_value[0] = strdup_nocheck(SCHA_FALSE);
			else {
				res.err_code = SCHA_ERR_PBOOL;
				rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
				    prop_name);
				return (res);
			}
		}
		if (new_value[0] == NULL) {
			rgm_format_errmsg(&res, REM_INTERNAL_MEM);
			    res.err_code = SCHA_ERR_NOMEM;
			} else
				res.err_code = SCHA_ERR_NOERR;
		}
	return (res);
}

#ifndef EUROPA_FARM
//
// Returns a string computed by appending all the LN
// from each element in the LogicalNodeset
//
char *
get_string_from_nodeset(LogicalNodeset &nset)
{
	char *nodeidlist_str = NULL;
	nodeidlist *arrayptr = NULL;
	nodeidlist_t *prev = NULL;
	nodeidlist_t *curr;
	rgm::lni_t currNode = 0;

	while ((currNode = nset.nextLniSet(currNode + 1)) != 0) {
		LogicalNode *lnNode = lnManager->findObject(currNode);
		// Only executed on the president. lnNode should not be NULL.
		CL_PANIC(lnNode != NULL);
		curr = (nodeidlist_t *)calloc(1, sizeof (nodeidlist_t));
		curr->nl_nodeid = lnNode->getNodeid();
		curr->nl_zonename = (lnNode->getZonename()) ?
		    strdup(lnNode->getZonename()) : NULL;

		if (prev) {
			prev->nl_next = curr;
		}
		if (!arrayptr) {
			arrayptr = curr;
		}
		prev = curr;
	}

	nodeidlist_str = rgm_nodeidlist_to_commastring(arrayptr);
	rgm_free_nodeid_list(arrayptr);
	return (nodeidlist_str);
}


//
// append_busted_msg
//
// Called by the scswitch functions.
// errcode is an error code returned by map_busted_err().
// Append an appropriate error message to res->err_msg corresponding to
// the "busted" error code:
//  SCHA_ERR_STARTFAILED switch busted by start failure
//  SCHA_ERR_STOPFAILED switch busted by stop failure
//  SCHA_ERR_SWRECONF switch busted by node death
//
// If a NOMEM or INTERNAL error occurs (in rgm_format_errmsg), sets
// res->err_code accordingly.  Otherwise, leaves res->err_code
// unchanged.
//
void
append_busted_msg(
	scha_errmsg_t	*res,
	rglist_p_t	rgp,
	scha_err_t	errcode)
{
	if (errcode == SCHA_ERR_STARTFAILED) {
		rgm_format_errmsg(res, REM_RG_FO_STARTFAIL,
		    rgp->rgl_ccr->rg_name);
	} else if (errcode == SCHA_ERR_STOPFAILED) {
		rgm_format_errmsg(res, REM_RG_TO_STOPFAILED,
		    rgp->rgl_ccr->rg_name);
	} else if (errcode == SCHA_ERR_SWRECONF) {
		rgm_format_errmsg(res, REM_RG_SW_BUSTED,
		    rgp->rgl_ccr->rg_name);
	}
}
#endif // !EUROPA_FARM


//
// set_rx_state
//
// This is a "setter" function for the state (rx_state)
// of a resource (rlist_p_t) on a given node.  This is called both on
// a slave node (to set local state in memory) and on the president node
// (to update a slave's state in the president's memory).
//
// On a slave that is not the president, any "interesting" state change
// is notified immediately to the president by an IDL call.
//
// If this function is called on the president, and the resource is
// transitioning from an online state directly to R_OFFLINE, this implies
// that the resource is being set offline because the node or zone 'lni'
// died.  In this case, trigger offline-restart dependencies of other
// resources upon this one.  If force_trigger_offline_restart_deps is true,
// then trigger offline-restart dependents of this resource regardless of
// whether its previous state was online or not; this is used only by a new
// president when the old president has died.
//
// A message is logged using sc_syslog_msg_log if we are the president and
// the state has changed from its previous value.  Otherwise, a message is
// written only to the debug buffer (ucmm_print).
//
// Our caller holds the lock on the state structure.
//
// Generating state change events: If the API representation
// of the R state changes, post_event_rs_state() is
// called to generate a sysevent with the new state
// of the R.
//
// Also, if the change in resource state causes the API representation of
// the RG state to change to or from ONLINE_FAULTED, we generate a
// sysevent to represent that change.
//
void
set_rx_state(
	rgm::lni_t		lni,
	rlist_p_t		rp,
	rgm::rgm_r_state	rx_state,
	boolean_t		force_trigger_offline_restart_deps,
	boolean_t		mutex_held)
{
	char			*resname;
	char			*statename;
#ifndef EUROPA_FARM
	rstatename_t		*old_state, *new_state;
	rgstatename_t		*old_rgstate, *new_rgstate;
	scha_errmsg_t		res;
	notify_status_change_task *notify_task = NULL;
#endif // !EUROPA_FARM
	sc_syslog_msg_handle_t	handle;
	rgm::rgm_r_state	old_rx_state;
	rgm::rgm_rg_state	rgx_state;
	char			*status_msg = NULL;

	resname = rp->rl_ccrdata->r_name;
	statename = pr_rstate(rx_state);
	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	const char *nodename = lnNode->getFullName();

	old_rx_state = rp->rl_xstate[lni].rx_state;

	//
	// On president, trigger offline-restart dependencies if necessary.
	// See function header comment above.
	//
	if (am_i_president() &&
	    (force_trigger_offline_restart_deps ||
	    (is_r_online(old_rx_state) && rx_state == rgm::R_OFFLINE))) {
		LogicalNodeset nset;
		nset.addLni(lni);

		trigger_restart_dependencies(rp, nset, B_TRUE);
	}

	// If the state didn't change, just log a debug message and return
	if (old_rx_state == rx_state) {
		ucmm_print("RGM",
		    NOGET("resource %s state on node %s remains at %s"),
		    resname, nodename, statename);
		return;
	}

	if ((rx_state == rgm::R_START_FAILED && !is_fm_status_unchanged(lni,
	    rp, RGM_DIR_STARTING)) || (rx_state == rgm::R_STOP_FAILED &&
	    !is_fm_status_unchanged(lni, rp, RGM_DIR_STOPPING))) {
		status_msg = rp->rl_xstate[lni].rx_fm_status_msg;
	}

	//
	// If the new R state is START_FAILED or STOP_FAILED, set FM status
	// to FAULTED and retain the status message if set by the monitor.
	//
	if (rx_state == rgm::R_START_FAILED ||
	    rx_state == rgm::R_STOP_FAILED) {
		//
		// If we are the president, we queue a status change task.
		// Otherwise, we set the status directly.
		//
		if (am_i_president()) {
#ifndef EUROPA_FARM
			notify_task = new notify_status_change_task(
			    lni, rp->rl_ccrdata->r_name,
			    rp->rl_rg->rgl_ccr->rg_name, rgm::R_FM_FAULTED,
			    status_msg);

			notify_status_change_threadpool::the().defer_processing
			    (notify_task);
#endif
		} else {
			rp->rl_xstate[lni].rx_fm_status = rgm::R_FM_FAULTED;
			rp->rl_xstate[lni].rx_fm_status_msg = status_msg;
		}
	}

#ifndef EUROPA_FARM
	//
	// Retrieve the old API representation of the RG state.
	// We must make this call before we set the new resource state,
	// because that could affect the API visible RG state (specifically
	// regarding ONLINE_FAULTED).
	//
	old_rgstate = get_rgstate(rp->rl_rg->rgl_xstate[lni].rgx_state,
	    NULL, (scha_rgstate_t)-1, rp->rl_rg, lni);
#endif // !EUROPA_FARM

	// Set the new R state
	rp->rl_xstate[lni].rx_state = rx_state;


	//
	// If I am not the president, log a debug message and, if necessary,
	// notify the president of the state change.
	//
	if (!am_i_president()) {
		ucmm_print("RGM",
		    NOGET("resource %s state on node %s change to %s"),
		    resname, nodename, statename);

		mdb_logrxstate(rp, lni, B_FALSE);
		// call notify_pres_state_change to notify the president of
		// "interesting" resource state changes.
		switch (rx_state) {

		//
		// The following states are "interesting" to the president
		// because they occur on the slave without waking the president:
		//
		case rgm::R_MON_FAILED:
		case rgm::R_OFFLINE:
		case rgm::R_ONLINE:
		case rgm::R_ONLINE_STANDBY:
		case rgm::R_ONLINE_UNMON:
		case rgm::R_STOPPED:
		case rgm::R_PRENET_STARTED:
		case rgm::R_START_FAILED:
		case rgm::R_STOP_FAILED:
		case rgm::R_POSTNET_STOPPING:
		case rgm::R_PRENET_STARTING:
		case rgm::R_STARTING:
		case rgm::R_STOPPING:
		case rgm::R_UNINITED:
		case rgm::R_JUST_STARTED:
		case rgm::R_STOPPING_DEPEND:
		case rgm::R_STARTING_DEPEND:
		case rgm::R_PENDING_INIT:
		case rgm::R_PENDING_FINI:
		case rgm::R_INITING:
		case rgm::R_FINIING:
			//
			// The above states are interesting.
			//
			// Note, we pass the actual RG state of the resource's
			// RG, although this is currently ignored by the
			// president for a resource state change.
			//
			rgx_state = rp->rl_rg->rgl_xstate[lni].rgx_state;
			notify_pres_state_change(lni, rgm::NOTIFY_RES, resname,
			    rx_state, rgx_state);
			break;

		//
		// The following states are "uninteresting" in that they do not
		// affect the API's view of the resource state, do not
		// affect inter-RG dependencies, and cannot exist in
		// an RG that is terminal.
		//
		case rgm::R_PENDING_UPDATE:
		case rgm::R_ON_PENDING_UPDATE:
		case rgm::R_ONUNMON_PENDING_UPDATE:
		case rgm::R_MONFLD_PENDING_UPDATE:
		case rgm::R_PENDING_BOOT:
		case rgm::R_BOOTING:
		case rgm::R_MON_STARTING:
		case rgm::R_MON_STOPPING:
		case rgm::R_UPDATING:
			//
			// We don't need notification of OK_TO_*, because
			// we have already set that state ourselves after
			// notifying the slave.
			//
		case rgm::R_OK_TO_START:
		case rgm::R_OK_TO_STOP:
		default:
			break;
		}

		return;
	}

	if ((old_rx_state == rgm::R_ONLINE_STANDBY) &&
	    (rx_state == rgm::R_JUST_STARTED)) {
		reset_fm_status(lni, rp, rgm::R_FM_ONLINE,
		    RGM_DIR_NULL);
	} else if ((old_rx_state == rgm::R_ONLINE) &&
	    (rx_state == rgm::R_ONLINE_STANDBY)) {
		reset_fm_status(lni, rp, rgm::R_FM_OFFLINE,
		    RGM_DIR_NULL);
	}

#ifndef EUROPA_FARM
	// The remainder of this function is executed by the president only

	old_state = get_rstate(old_rx_state, NULL, (scha_rsstate_t)-1);
	new_state = get_rstate(rx_state, NULL, (scha_rsstate_t)-1);

	//
	// If there has been a change in the API visible state
	// of the R, generate a sysevent
	//
	if (strcmp(old_state->api_statename,
		new_state->api_statename) != 0) {
		res = post_event_rs_state(resname);
		if (res.err_msg)
			free(res.err_msg);
	}

	//
	// Retrieve the new API representation of the RG state.
	//
	new_rgstate = get_rgstate(rp->rl_rg->rgl_xstate[lni].rgx_state,
	    NULL, (scha_rgstate_t)-1, rp->rl_rg, lni);

	//
	// If there has been a change in the API visible state of
	// the RG to or from ONLINE_FAULTED, generate a sysevent.
	// We must generate the event here because ONLINE_FAULTED is a
	// "fake" state, and will not be set anywhere explicitly.
	//
	if (old_rgstate->api_val != new_rgstate->api_val &&
	    (old_rgstate->api_val == SCHA_RGSTATE_ONLINE_FAULTED ||
	    new_rgstate->api_val == SCHA_RGSTATE_ONLINE_FAULTED)) {
		res = post_event_rg_state(rp->rl_rg->rgl_ccr->rg_name);
		if (res.err_msg)
			free(res.err_msg);
	}

	//
	// I am the president--syslog the state change
	// Generate the sc_syslog message for the state change
	//
	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG,
	    resname);
	int log_level = get_log_level_for_res_state(rx_state);
	//
	// SCMSGS
	// @explanation
	// This is a notification from the rgmd that a resource's state has
	// changed. This may be used by system monitoring tools.
	// @user_action
	// This is an informational message; no user action is needed.
	// In a non-debug build, RGM syslogs internal Resource States at a
	// lower priority (LOG_DEBUG) level.
	//
	(void) sc_syslog_msg_log(handle, log_level, STATE_CHANGED,
	    SYSTEXT("resource %s state on node %s change to %s"),
	    resname, nodename, statename);
	sc_syslog_msg_done(&handle);

	//
	// Now that the resource has changed state on the slave,
	// reset the "beginning to restart" flag here on the president
	// This is used by r_is_restarting().
	//
	rp->rl_xstate[lni].rx_restarting = B_FALSE;
	mdb_logrxstate(rp, lni, B_FALSE);


	//
	// If new state indicates a method failure, do a cond_broadcast
	// so that idl_scha_control_restart() can note this state and exit
	// with appropriate error code
	//
	// If mutex_held is false, we are a new president doing an "immediate
	// fetch" of state from slaves.  The rgm state mutex is not held by
	// the currently executing thread; rather it is held by
	// rgm_reconfig_helper().  In that case, we avoid executing the
	// cond_slavewait broadcast() now.  It will be done later by
	// rgm_reconfig_helper.
	//
	if (mutex_held && (rx_state == rgm::R_STOP_FAILED ||
	    rx_state == rgm::R_START_FAILED || rx_state == rgm::R_MON_FAILED)) {
		cond_slavewait.broadcast();
	}

	//
	// Perform "needy resource" processing on the president for
	// inter-RG and restart dependencies.  Make the call only if
	// we're not in the midst of reconfiguring.
	//
	if (!Rgm_state->pres_reconfig) {
		check_dependencies_resource(lni, rp, rx_state);
	}

	//
	// rl_is_deleted indicates that the resource is running FINI methods
	// on one or more nodes or zones.  If this is the case, and if the
	// resource is no longer
	// PENDING_FINI or FINIING on any node or zone, then delete it from
	// the president's memory.  This is done in launch_method() or in
	// unlatch_intention_helper() on a non-president slave.  On the
	// president, we do it here, because we would rather not delete
	// the resource from memory until all slaves have finished running
	// Fini on it.
	//
	// If the RG's rgl_no_rebalance flag is true, it means that this node
	// is a newly elected president fetching state from a slave.
	// In that case, we skip the following code -- it would be premature
	// to delete the resource from memory if we haven't finished fetching
	// its state from all the slaves.  Then in complete_intention(),
	// we will either remove the resource (if no FINI methods are to
	// run), or set rl_is_deleted, and revisit this code path when
	// the FINI methods complete.
	//
	if (!rp->rl_rg->rgl_no_rebalance && rp->rl_is_deleted) {
		rgm_rt_t *rt = rtname_to_rt(rp->rl_ccrdata->r_type);
		CL_PANIC(rt != NULL);
		boolean_t is_scalable = is_r_scalable(rp->rl_ccrdata);
		LogicalNodeset *ns = compute_meth_nodes(rt, rp->rl_rg->rgl_ccr,
		    METH_FINI, is_scalable);

		//
		// Scalable services without an RT-defined fini method execute
		// ssm fini on the president, so we add our own nodeid to the
		// nodeset in that case.  [See corresponding code in
		// idl_scrgadm_rs_delete() where is_scalable_but_no_fini
		// is assigned B_TRUE].
		//
		if (is_scalable && !is_method_reg(rt, METH_FINI, NULL)) {
			ns->addLni(get_local_nodeid());
		}

		//
		// Iterate through all nodes in ns to check resource state
		//
		bool fini_completed = true;
		rgm::lni_t n = 0;
		while ((n = ns->nextLniSet(n + 1)) != 0) {
			if (rp->rl_xstate[n].rx_state == rgm::R_PENDING_FINI ||
			    rp->rl_xstate[n].rx_state == rgm::R_FINIING) {
				fini_completed = false;
				break;
			}
		}
		if (fini_completed) {
			ucmm_print("set_rx_state",
			    NOGET("Destroy RS <%s> from memory"), resname);
			CL_PANIC(rgm_delete_r(resname,
			    rp->rl_rg->rgl_ccr->rg_name).err_code ==
			    SCHA_ERR_NOERR);
		} else {
			ucmm_print("set_rx_state",
			    NOGET("Not destroying RS <%s> from memory because "
			    "fini not completed on all zones"), resname);
		}
	}

#endif // !EUROPA_FARM
}

/*
 * Log the resource state information in the mdb in memory log
 * rp->rl_ccrdata->log_rxstate.
 * When the size of the in-memory log has reached MDB_RGM_LOGSIZE,
 * we copy the state information (from the resource state structure)
 * into the mdb backup log(rp->rl_ccrdata->rx_back) and then restart
 * logg'ing the state information in the in-memory log. This is done in an
 * effort to compress the in memory log)
 */
void
mdb_logrxstate(rlist_p_t rp, rgm::lni_t lni, boolean_t update_backup)
{
	int size;
	LogicalNodeset *ns = NULL;
	int ln;

	if (!update_backup) {
		size = rp->log_rxstate.size;
		if (size < MDB_RGM_LOGSIZE) {
			rp->log_rxstate.rx_state =
			    (mdb_rxstate_t *)realloc(
			    rp->log_rxstate.rx_state,
			    sizeof (mdb_rxstate_t) * (size + 1));
			clone_rxentry(rp->log_rxstate.rx_state[size],
			    rp->rl_xstate[lni], lni);
			rp->log_rxstate.size++;
			return;
		}
	}
	free_mdbrxstate(rp->log_rxstate);
	free_mdbrxstate(rp->mdb_rxback);

	ns = get_logical_nodeset_from_nodelist(
	    rp->rl_rg->rgl_ccr->rg_nodelist);

	rp->mdb_rxback.size = size = ns->count();
	rp->mdb_rxback.rx_state =
	    (mdb_rxstate_t *)malloc(sizeof (mdb_rxstate_t) * size);

	for (ln = 0, size = 0; (ln = ns->nextLniSet(ln + 1)) != 0; ++size) {
		clone_rxentry(rp->mdb_rxback.rx_state[size], rp->rl_xstate[ln],
		    ln);
	}
}

void
clone_rxentry(mdb_rxstate_t& mdb_rx_state, r_xstate_t& r_xstate, rgm::lni_t lni)
{
	mdb_rx_state.lni = lni;
	mdb_rx_state.rx_state = r_xstate.rx_state;
	mdb_rx_state.rx_fm_status = r_xstate.rx_fm_status;
	mdb_rx_state.rx_fm_status_msg = NULL;
	if (r_xstate.rx_fm_status_msg != NULL)
		mdb_rx_state.rx_fm_status_msg =
		    strdup(r_xstate.rx_fm_status_msg);
	else
		mdb_rx_state.rx_fm_status_msg = NULL;
	mdb_rx_state.rx_restarting = r_xstate.rx_restarting;
	mdb_rx_state.rx_last_restart_time = r_xstate.rx_last_restart_time;
	mdb_rx_state.rx_throttle_restart_counter =
	    r_xstate.rx_throttle_restart_counter;
	mdb_rx_state.rx_restart_flg = r_xstate.rx_restart_flg;
	mdb_rx_state.rx_ignore_failed_start = r_xstate.rx_ignore_failed_start;
	mdb_rx_state.rx_ok_to_start = r_xstate.rx_ok_to_start;
	mdb_rx_state.rx_rg_restarts = rgm_clone_timelist
	    (r_xstate.rx_rg_restarts);
	mdb_rx_state.rx_r_restarts = rgm_clone_timelist(r_xstate.rx_r_restarts);
	mdb_rx_state.rx_blocked_restart = rgm_clone_timelist(r_xstate.
	    rx_blocked_restart);
}

void
free_mdbrxstate(mdb_rxlogs_t& rx_logs)
{
	int i = 0;
	while (i < rx_logs.size) {
		free(rx_logs.rx_state[i].rx_fm_status_msg);
		rgm_free_timelist(&rx_logs.rx_state[i].rx_rg_restarts);
		rgm_free_timelist(&rx_logs.rx_state[i].rx_blocked_restart);
		rgm_free_timelist(&rx_logs.rx_state[i].rx_r_restarts);
		i++;
	}
	free(rx_logs.rx_state);
	rx_logs.rx_state = NULL;
	rx_logs.size = 0;
}
/*
 * Log the resource group state information in the mdb in memory log
 * rgp->rgl_ccr->log_rgxstate.
 * When the size of the in-memory log has reached MDB_RGM_LOGSIZE,
 * we copy the state information (from the resource group state structure)
 * into the mdb backup log(rgp->rgl_ccr->rgx_back) and then restart
 * logg'ing the state information in the in-memory log. This is done in an
 * effort to compress the in memory log)
 */
void
mdb_logrgxstate(rglist_p_t rgp, rgm::lni_t lni, boolean_t update_backup)
{
	int size;
	LogicalNodeset *ns = NULL;
	uint_t ln;

	if (!update_backup) {
		size = rgp->log_rgxstate.size;
		if (size < MDB_RGM_LOGSIZE) {
			rgp->log_rgxstate.rgx_state =
			    (mdb_rgxstate_t *)realloc(
			    rgp->log_rgxstate.rgx_state,
			    sizeof (mdb_rgxstate_t) * (size+1));
			clone_rgxentry(rgp->log_rgxstate.
			    rgx_state[size], rgp->rgl_xstate[lni], lni);
			rgp->log_rgxstate.size++;
			return;
		}
	}
	free_mdbrgxstate(rgp->log_rgxstate);
	free_mdbrgxstate(rgp->mdb_rgxback);

	ns = get_logical_nodeset_from_nodelist(
	    rgp->rgl_ccr->rg_nodelist);

	rgp->mdb_rgxback.size = size = ns->count();
	rgp->mdb_rgxback.rgx_state =
	    (mdb_rgxstate_t *)malloc(sizeof (mdb_rgxstate_t) * size);

	for (ln = 0, size = 0; (ln = ns->nextLniSet(ln + 1)) != 0; ++size) {
		clone_rgxentry(rgp->mdb_rgxback.rgx_state[size],
		    rgp->rgl_xstate[ln], ln);
	}
}

void
clone_rgxentry(mdb_rgxstate_t& mdb_rgx_state, rg_xstate_t& rg_xstate,
	rgm::lni_t lni)
{
	mdb_rgx_state.lni = lni;
	mdb_rgx_state.rgx_state = rg_xstate.rgx_state;
	mdb_rgx_state.rgx_net_relative_restart =
	    rg_xstate.rgx_net_relative_restart;
	mdb_rgx_state.rgx_postponed_stop = rg_xstate.rgx_postponed_stop;
	mdb_rgx_state.rgx_postponed_boot = rg_xstate.rgx_postponed_boot;
	mdb_rgx_state.rgx_start_fail =
	    rgm_clone_timelist(rg_xstate.rgx_start_fail);
}

void
free_mdbrgxstate(mdb_rgxlogs_t& rgx_logs)
{
	int i = 0;
	while (i < rgx_logs.size) {
		rgm_free_timelist(&rgx_logs.rgx_state[i].rgx_start_fail);
		i++;
	}
	free(rgx_logs.rgx_state);
	rgx_logs.rgx_state = NULL;
	rgx_logs.size = 0;
}

//
// set_rgx_state
//
// This is a "setter" function for the state (rgx_state)
// of an RG (rglist_p_t) on a given lni.  This is called both on
// a slave node (to set local state in memory) and on the president node
// (to update a slave's state in the president's memory).
//
// On a slave that is not the president, any "interesting" state change
// is notified immediately to the president by an IDL call.
//
// A message is logged using sc_syslog_msg_log if we are the president and
// the state has changed from its previous value.  Otherwise, a message is
// written only to the debug buffer (ucmm_print).
//
// Our caller holds the lock on the state structure.
//
// If this node is the president, this function also calls process_needy_rg
// to do various kinds of processing on the resource group based upon its
// new state.  For details, see the header comment of process_needy_rg().
//
// If this node is the president, and the RG's state makes a transition from
// ON_PENDING_METHODS to another state or from OFF_PENDING_METHODS to another
// state,
// and the RG's rgl_new_pres_rebalance flag is true, then we call rebalance()
// on the RG with the i_am_new_pres parameter set to true.  In this scenario,
// rebalance() will be a no-op (and the rgl_new_pres_rebalance flag remains
// true) if the RG is still in one of the above "PENDING" states on any
// node.  Each time the RG transitions to endish state on a slave, the slave
// will wake the president, which will enter this function and call
// rebalance() again.  When the RG finally moves out of PENDING state on all
// such nodes, rebalance() will actually rebalance the RG and will reset the
// rgl_new_pres_rebalance flag to false.
//
void
set_rgx_state(
	rgm::lni_t		lni,
	rglist_p_t		rgp,
	rgm::rgm_rg_state	rgx_state)
{
	char			*rgname;
	char			*statename;
#ifndef EUROPA_FARM
	char			*resourcename = NULL;
	rgstatename_t		*old_state, *new_state;
	rlist_p_t		r_ptr = NULL;
	scha_errmsg_t		res;
#endif // !EUROPA_FARM
	sc_syslog_msg_handle_t	handle;
	rgm::rgm_rg_state	old_rgx_state;
	LogicalNodeset current_online_ns;
	LogicalNodeset to_ns;
	LogicalNodeset emptyNodeset;

	rgname = rgp->rgl_ccr->rg_name;
	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	const char *nodename = lnNode->getFullName();
#ifndef EUROPA_FARM

	//
	// If I am the president and the new RG state on the slave is
	// PENDING_ONLINE_BLOCKED, it is possible that all starting
	// dependencies have already been resolved for this RG.  In that
	// case, we must have already called
	// start_dependencies_resolved(), but the resulting state change
	// of the RG (from PENDING_ONLINE_BLOCKED back to PENDING_ONLINE)
	// is further down in the president's state change queue so we
	// haven't seen it yet.  So, we will set the RG state for the slave
	// directly to PENDING_ONLINE to avoid having it pass through
	// an endish state of PENDING_ONLINE_BLOCKED.
	//
	// This prevents the phenomenon of an scswitch -R -g exiting
	// prematurely (bugid 4988839) and may avoid misleading
	// error traces caused by an RG being transitorily blocked
	// and then unblocked.
	//
	if (am_i_president() && rgx_state == rgm::RG_PENDING_ONLINE_BLOCKED) {
		if (!any_res_starting_depend(rgp, lni)) {
			ucmm_print("RGM", NOGET("RG %s on node %s unblocked"),
			    rgname, nodename);
			rgx_state = rgm::RG_PENDING_ONLINE;
		}
	}
#endif // !EUROPA_FARM
	statename = pr_rgstate(rgx_state);

	old_rgx_state = rgp->rgl_xstate[lni].rgx_state;

	// If the state didn't change, just log a debug message and return
	if (old_rgx_state == rgx_state) {
		ucmm_print("RGM",
		    NOGET("resource group %s state on node %s remains at %s"),
		    rgname, nodename, statename);
		return;
	}

	// Set the new RG state
	rgp->rgl_xstate[lni].rgx_state = rgx_state;

	//
	// If the new state is RG_OFFLINE and the zone is shutting down
	// or down, reset the per-zone state of the RG and its resources
	// for that zone.
	//
	if (lnNode->getStatus() < rgm::LN_UP && rgx_state == rgm::RG_OFFLINE) {
		reset_rg_per_zone_state(rgp, lni);

	//
	// If the new state is RG_OFFLINE, and the zone is still up, and
	// the RG's postponed boot flag is set for this zone, then prepare
	// to launch boot methods.  The new state is set to
	// OFF_PENDING_BOOT or OFF_BOOTED instead of OFFLINE.
	// Note: the rgx_postponed_boot flag is set only on the slave side.
	//
	} else if (lnNode->getStatus() == rgm::LN_UP &&
	    rgx_state == rgm::RG_OFFLINE &&
	    rgp->rgl_xstate[lni].rgx_postponed_boot) {
		//
		// To make sure that our assumptions about rgx_postponed_boot
		// are correct, assert that the lni is a non-global or ZC zone
		// on the local node, and that the RG is in a stopping or
		// stop-failed state on the lni.
		//
		ASSERT(!lnNode->isGlobalZone() &&
		    lnNode->getNodeid() == get_local_nodeid() &&
		    (old_rgx_state == rgm::RG_PENDING_OFFLINE ||
		    old_rgx_state == rgm::RG_PENDING_OFF_STOP_FAILED ||
		    old_rgx_state == rgm::RG_ERROR_STOP_FAILED));
		// clear the postponed_boot flag
		rgp->rgl_xstate[lni].rgx_postponed_boot = B_FALSE;
		if (check_boot_meths_on_rg(rgp, lni)) {
			//
			// Queue a state machine task to run after
			// the lock is released.
			//
			run_state_machine(lni, "postponed boot");
		} else {
			ucmm_print("set_rgx_state",
			    NOGET("postponed boot rg %s node %s no boot meths"),
			    rgname, nodename);
		}
		// If this this node is not the president, notify the president
		// that the new RG state is OFF_PENDING_BOOT.  OFF_PENDING_BOOT
		// is usually an "uninteresting" state which is not reported
		// to the president, because the president itself would
		// set this state in the function set_booting_state() when
		// the zone joins the cluster membership.
		// However, in the case of posponed boot, the zone has not
		// just joined the membership, and the RG is in a pending
		// offline or error_stop_failed state, and we want the RG
		// state on the president to move directly to OFF_PENDING_BOOT;
		// so we have to do the notification.
		// We pass a dummy resource state of OFFLINE.
		//
		if (!am_i_president()) {
			notify_pres_state_change(lni, rgm::NOTIFY_RG, rgname,
			    rgm::R_OFFLINE, rgm::RG_OFF_PENDING_BOOT);
		}
	}

	//
	// If I am not the president, log a debug message and, if necessary,
	// notify the president of the RG state change.
	//
	if (!am_i_president()) {
		ucmm_print("RGM",
		    NOGET("resource group %s state on node %s change to %s"),
		    rgname, nodename, statename);

		//
		// Notify the president of "interesting" RG state changes.
		//
		// One class of "interesting" RG state changes are those in
		// which the RG changes state to a "pending offline error"
		// state due to a START or STOP method failure.  Such changes
		// occur on the slave without waking the president until the
		// RG reaches terminal state (OFFLINE_START_FAILED or
		// ERROR_STOP_FAILED).  We notify the president so that API
		// queries (and other president-side threads) will immediately
		// reflect the "pending offline error" state of the RG.
		//
		// Another interesting state change is a change to
		// ON_PENDING_R_RESTART state; the president needs to know
		// about it to prevent updates while the R is restarting.
		//
		// We push more states, because the president
		// no longer fetches state following a wake_president call.
		//
		// Thus, all terminal states are interesting.
		//
		// RG_ON_PENDING_METHODS and RG_OFF_PENDING_METHODS are
		// interesting states when we run UPDATE/INIT/FINI methods,
		// and want to wait until they are finished.  We need to get
		// the state change into the PENDING_ state before entering
		// the cond_wait loop.
		//
		// Similary, ON_PENDING_*_DISABLED are interesting states
		// when we are disabling resources.
		//
		// RG_OFF_BOOTED is interesting when we're running BOOT
		// methods on joiners.  Normally, the slave will call
		// wake_president when the RG boots, but if there are no
		// boot methods to run, the slave just moves the RG directly
		// to OFF_BOOTED, so it needs to be an interesting state.
		//
		// Note that OFF_PENDING_BOOT is _not_ an interesting state.
		// We explicitly set that state on the pres after telling the
		// slave to run BOOT methods.
		//
		mdb_logrgxstate(rgp, lni, B_FALSE);
		switch (rgx_state) {
		case rgm::RG_ON_PENDING_R_RESTART:
		case rgm::RG_PENDING_OFF_STOP_FAILED:
		case rgm::RG_PENDING_OFF_START_FAILED:
		case rgm::RG_PENDING_ONLINE:
		case rgm::RG_PENDING_OFFLINE:
		case rgm::RG_OFFLINE:
		case rgm::RG_ONLINE:
		case rgm::RG_OFFLINE_START_FAILED:
		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_METHODS:
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_ON_PENDING_MON_DISABLED:
		case rgm::RG_OFF_BOOTED:
		case rgm::RG_ERROR_STOP_FAILED:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
			//
			// For the above state changes, we notify the
			// president.  Pass a dummy resource state.
			//
			notify_pres_state_change(lni, rgm::NOTIFY_RG, rgname,
			    rgm::R_OFFLINE, rgx_state);
			break;
		case rgm::RG_OFF_PENDING_BOOT:
		case rgm::RG_PENDING_ON_STARTED:
		default:
			//
			// For these states we do not need to notify the
			// president.
			//
			break;
		}
		return;
	}
#ifndef EUROPA_FARM
	// I am the president--process the state change

	if (rgx_state == rgm::RG_PENDING_OFF_START_FAILED) {
		// Post a primaries changing event

		// To post a primaries changing event we need
		// the resource that caused this event and also the
		// set of nodes on which the RG is currently online.

		// Get the set of nodes where this RG will be online

		for (LogicalNodeManager::iter it = lnManager->begin();
		    it != lnManager->end(); ++it) {
			LogicalNode *lnNode = lnManager->findObject(it->second);
			CL_PANIC(lnNode != NULL);

			rgm::lni_t n = lnNode->getLni();

			if (was_rg_available(rgp, n)) {
				// Add this node to the remaining masters
				// nodeset i.e the nodeset where the
				// RG will be available.
				to_ns.addLni(n);
			}
		}
		// Iterate thru the resources in this RG and find out
		// the resource which is in start failed state

		for (r_ptr = rgp->rgl_resources; r_ptr != NULL;
			r_ptr = r_ptr->rl_next) {
			if (r_ptr->rl_xstate[lni].rx_state ==
			    rgm::R_START_FAILED) {
				resourcename = r_ptr->rl_ccrdata->r_name;
				break;
			}
		}

		// Get the nodeset where the RG was available.
		current_online_ns = to_ns;
		current_online_ns.addLni(lni);

		// Post the event
		(void) post_primaries_changing_event(resourcename,
		    rgp->rgl_ccr->rg_name,
		    CL_REASON_R_START_FAILED,
		    nodename,
		    rgp->rgl_ccr->rg_desired_primaries,
		    current_online_ns, to_ns);
	}

	//
	// If there has been a change in the API visible state
	// of the RG, generate a sysevent
	//

	old_state = get_rgstate(old_rgx_state, NULL,
		(scha_rgstate_t)-1, rgp, lni);
	new_state = get_rgstate(rgx_state, NULL,
		(scha_rgstate_t)-1, rgp, lni);

	mdb_logrgxstate(rgp, lni, B_FALSE);
	if (strcmp(old_state->api_statename, new_state->api_statename) != 0) {
		res = post_event_rg_state(rgname);
		if (res.err_msg)
			free(res.err_msg);
	}

	// Generate the sc_syslog message for the state change
	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG,
	    rgname);
	//
	// SCMSGS
	// @explanation
	// This is a notification from the rgmd that a resource group's state
	// has changed. This message can be used by system monitoring tools.
	// @user_action
	// This is an informational message; no user action is needed.
	// RGM should syslog Failed RG status messages with LOG_ERR priority

	int log_level = LOG_NOTICE;
	if ((rgx_state == rgm::RG_ERROR_STOP_FAILED) ||
	    (rgx_state == rgm::RG_PENDING_OFF_STOP_FAILED) ||
	    (rgx_state == rgm::RG_PENDING_OFF_START_FAILED) ||
	    (rgx_state == rgm::RG_OFFLINE_START_FAILED)) {
		log_level = LOG_ERR;
	}
	(void) sc_syslog_msg_log(handle, log_level, STATE_CHANGED,
	    SYSTEXT("resource group %s state on node %s change to %s"),
	    rgname, nodename, statename);
	sc_syslog_msg_done(&handle);

	// Take actions required by new RG state on the president side

	// Skip over RGs in unmanaged state
	if (rgp->rgl_ccr->rg_unmanaged) {
		ucmm_print("RGM",
		    NOGET("set_rgx_state skipping UNMANAGED RG <%s>"),
		    rgname);
		return;
	}

	// Bust switches, clear start-failure history, etc.
	process_needy_rg(lni, rgp, old_rgx_state);

	//
	// Check if we need to call delayed rebalance for new president.
	// See header comment above for details.
	//
	if (rgp->rgl_new_pres_rebalance &&
	    (old_rgx_state == rgm::RG_ON_PENDING_METHODS ||
	    old_rgx_state == rgm::RG_OFF_PENDING_METHODS)) {
		//
		// No need for rebalance to ignore joiners, since either we're
		// outside of the reconfiguration step, or we're within the
		// reconfiguration step but a new president only fetches state
		// from non-joiners; so pass it an empty nodeset.
		//
		// We set the is_new_president argument to true so it will
		// check that the RG has moved out of the PENDING_METHODS
		// state on all nodes before actually doing the rebalance.
		//
		// Note: lint doesn't understand default arguments
		//
		rebalance(rgp, emptyNodeset, B_TRUE, B_TRUE); /*lint !e118 */
	}

	//
	// Call check_dependencies_rg to determine whether this RG state
	// change fulfilled any dependencies for any resource on the system.
	// Make the call only if we're not a undergoing a reconfiguration.
	//
	if (!Rgm_state->pres_reconfig) {
		check_dependencies_rg(rgp, old_rgx_state, rgx_state);
	}
#endif // !EUROPA_FARM
}


//
// Convert rgm::change_tag enum type to string (for printing).
//
const char *
pr_change_tag(rgm::notify_change_tag ct)
{
	char *ctname;

	switch (ct) {
	case rgm::NOTIFY_WAKE:
		ctname = "NOTIFY_WAKE";
		break;

	case rgm::NOTIFY_RES:
		ctname = "NOTIFY_RES";
		break;

	case rgm::NOTIFY_RG:
		ctname = "NOTIFY_RG";
		break;

	default:
		ctname = "invalid change tag";
		break;
	}
	return ((const char *)ctname);
}


//
// If any Resource in the RG is in START_FAILED or MONITOR_FAILED
// state or if the Fault Monitor state indicates faults return TRUE.
//
boolean_t
is_any_rs_faulted(rglist_p_t rgp, rgm::lni_t lni)
{
	rlist_p_t	r_ptr = NULL;
	rfmstatusname_t *status_tbl;

	for (r_ptr = rgp->rgl_resources; r_ptr != NULL;
	    r_ptr = r_ptr->rl_next) {
		// If any Resource is in START_FAILED
		// state or if the Monitor indicates failure
		// return TRUE.

		if ((r_ptr->rl_xstate[lni].rx_state ==
		    rgm::R_START_FAILED) ||
		    (r_ptr->rl_xstate[lni].rx_state ==
		    rgm::R_MON_FAILED) ||
		    (r_ptr->rl_xstate[lni].rx_state ==
		    rgm::R_ONLINE_STANDBY))
			return (B_TRUE);

		// If any Fault Monitor status indicates
		// faults return TRUE.

		if ((status_tbl = get_rfmstatus
		    (r_ptr->rl_xstate[lni].rx_fm_status,
		    NULL, (scha_rsstatus_t)-1)) == NULL) {
			ucmm_print("RGM",
			    NOGET("Invalid R status in memory"));
			return (B_FALSE);
		}
		if (status_tbl->api_val == SCHA_RSSTATUS_FAULTED)
			return (B_TRUE);
	}

	return (B_FALSE);
}


//
// To support Solaris 9 SRM.
// get_project_name(): Return the project name of the given resource.
// If the resource project name is NULL (never been set), find the
// project name of the containing RG.  If the RG project name is also
// not set, return the system default project name "default".
// Caller must free allocated memory.
//
char *
get_project_name(const rgm_resource_t *rs, const rgm_rg_t *rg)
{
	char *proj_name = NULL;
	rlist_p_t	rp = NULL;

	if (rs == NULL && rg == NULL) {
		proj_name = strdup(RGM_DEFAULT_PROJ);
		if (proj_name == NULL)
			// Out of memory
			abort();
		return (proj_name);
	}

	if (rs != NULL) {
		if (rs->r_project_name != NULL && *rs->r_project_name != '\0')
			proj_name = strdup(rs->r_project_name);
		else {
			// rs project not set; look for rg project
			rp = rname_to_r(NULL, rs->r_name);
			if (rp->rl_rg->rgl_ccr->rg_project_name != NULL &&
			    *rp->rl_rg->rgl_ccr->rg_project_name != '\0') {
				proj_name = strdup(
				    rp->rl_rg->rgl_ccr->rg_project_name);
			} else
				// rg project not set; return default project
				proj_name = strdup(RGM_DEFAULT_PROJ);
		}

		if (proj_name == NULL)
			// strdup failed
			abort();

		return (proj_name);
	}

	if (rg != NULL && rg->rg_project_name != NULL &&
	    *(rg->rg_project_name) != '\0')
		proj_name = strdup(rg->rg_project_name);
	else
		proj_name = strdup(RGM_DEFAULT_PROJ);

	if (proj_name == NULL)
		// strdup failed
		abort();

	return (proj_name);
}


//
// The mutex and cond_t for use by the sync_thread.
//
static os::mutex_t sync_mutex;
static os::condvar_t sync_cond_var;

//
// The sync thread just waits on a condition variable until signaled.
// Then it calls sync(2).
//
// We have a dedicated thread to make this sync call because sync on
// pxfs can block indefinitely.  In the rgmd, we only call sync directly
// before aborting the node.  In those cases, we want to be able to abort
// even if sync fails or blocks.
//
void *
sync_thread_start(void *arg)
{
	sync_mutex.lock();
	while (B_TRUE) {
		sync_cond_var.wait(&sync_mutex);
		sync();
	}

	/* NOTREACHED */
	return (arg);
}

//
// rgm_fatal_reboot
//
// This is a wrapper for abort_node(), when the rgmd calls it for a fatal
// error, which would most likely be a NOMEM error.
// Previously we had been calling abort_node in many places without having
// written any syslog message; this would cause the node to reboot without
// giving any clue of why it did so.
//
// If the error is truly NOMEM, the attempt to syslog the message here might
// fail.  However, if the error is caused by some other problem, this
// syslog message will allow the error to be debugged.
//
// The string "what" should indicate the name of the function in which
// the error occurred, or any other short string which would uniquely
// identify the point of error in the source code.
//
// The other arguments are passed on to abort_node().
//
void
rgm_fatal_reboot(const char *what, boolean_t reboot_node, boolean_t take_dump,
    const char *zonename)
{
	sc_syslog_msg_handle_t handle;

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");
	//
	// SCMSGS
	// @explanation
	// The rgm daemon encountered a non-recoverable error, possibly
	// because the system ran out of swap space. The rgm daemon will force
	// the node to halt or reboot to avoid the possibility of data
	// corruption. A core file might be produced in some cases, but will
	// not usually be produced in the case of low swap space.
	// @user_action
	// A low swap condition might have been temporarily remedied by
	// rebooting. If the problem recurs, you might need to increase swap
	// space by configuring additional swap devices. See swap(1M) for more
	// information. If it appears that exhaustion of swap space was not
	// the cause of the problem, contact your authorized Sun service
	// provider to determine whether a workaround or patch is available.
	//
	(void) sc_syslog_msg_log(handle, LOG_EMERG, MESSAGE,
	    "%s: fatal: reboot", what);
	sc_syslog_msg_done(&handle);

	abort_node(reboot_node, take_dump, zonename);
}


//
// abort_node
// If reboot_node is TRUE, node is rebooted else it halts. If take_dump
// is TRUE, system wide dump is taken before halt/reboot.
// If zonename is non-null, reboot the indicated nonglobal zone instead
// of rebooting the physical node.  In this case, take_dump is ignored.
//
// On a physical node reboot:
// Sleeps for 5 seconds to allow syslogd to finish writing out the syslog
// messages.  Then signals the sync thread, to force a sync call to
// be made from a separate thread, sleeps for 10 seconds to allow the sync
// to complete, then calls uadmin with an A_REBOOT/AD_BOOT disposition, to
// reboot the node.
//
// This function should be called after syslogging all relevant messages.
//
void
abort_node(boolean_t reboot_node, boolean_t take_dump, const char *zonename)
{
	FILE *fd;
	int i, result = 0;
	int cmd;
	int fcn;

#if SOL_VERSION >= __s10
	//
	// Check for the non-global zone case
	//
	if (zonename) {
		abort_zone(reboot_node, zonename);
		return;
	}
#endif

	/*
	 * syslogd.pid file contains the pid of syslogd daemon.
	 * The pid is used to send SIGHUP signal to the syslogd.
	 * This should cause the syslogd to flush pending outputs
	 * to /var/adm/messages
	 */

	if ((fd = fopen("/etc/syslogd.pid", "r")) != NULL) {
		result = fscanf(fd, "%d", &i);
		(void) fclose(fd);
		if (result == 1) {
			(void) kill(i, SIGHUP);
		}
	}
	/*
	 * First we sleep for a short amount of time to give syslogd
	 * a chance to write out the syslog message.
	 */

	(void) sleep(5 /* seconds */);

	/*
	 * wake up the sync_thread, so that it can sync outstanding
	 * writes to disk.
	 */
	sync_mutex.lock();
	sync_cond_var.signal();
	sync_mutex.unlock();
	/*
	 * To allow the sync to work, wait for a little while
	 * before shutting down the node.
	 */
	(void) sleep(10 /* seconds */);

	/*
	 * Now do the actual reboot/halt.
	 */
#ifndef linux
	cmd = A_REBOOT;
	/*
	 * A_REBOOT stops (not necessarily reboots) the system immediately.
	 * We don't use A_SHUTDOWN flavor of uadmin because an active remote
	 * invocation can block it from succeeding. See bug 4452908.
	 * The final state of system depends on the value of fcn
	 */

	if (take_dump) {
		cmd = A_DUMP;	// panic the system. takes dump
	}

	if (reboot_node) {
		fcn = AD_BOOT; // final system state is rebooted
	} else {
		fcn = AD_HALT;	// final system state is halted
	}

	(void) uadmin(cmd, fcn, (uintptr_t)0);
#else
	abort();
#endif

	/*
	 * In case the command above hangs
	 * or does not work for some reason, exit
	 * the rgmd process; this will cause the
	 * node to shut down with a deferred
	 * failfast.
	 */
	_exit(1);

}

//
// abort_zone()
// ------------
// Runs zoneadm reboot (or zoneadm halt) through the fed.
// Cannot use popen or any other command that forks
// because a CORBA server is not allowed to fork!
//
static void
abort_zone(boolean_t reboot_node, const char *zonename)
{
	sc_syslog_msg_handle_t handle;

	//
	// Tag is "zoneadm.zonename"
	// ZONENAME_MAX + strlen("zoneadm") + 1 (for .) + 1 for NULL char
	//
	char tag[ZONENAME_MAX + 9];

	(void) snprintf(tag, ZONENAME_MAX + 9, "zoneadm.%s", zonename);

	//
	// Most of the fields of fed_cmd are not necessary
	//
	fe_cmd fed_cmd = {0};

	fed_cmd.host = getlocalhostname(); // doesn't need to be freed
	if (fed_cmd.host == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");

		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to obtain the name of
		// the local host, causing a zoneadm reboot
		// invocation to fail. The zone will stay up,
		// but the rgmd will not attempt to start services on it.
		// @user_action
		// Reboot the zone.
		// Examine other syslog messages occurring at about
		// the same time to see if the problem can be
		// identified. Save a copy of the /var/adm/messages
		// files on all nodes and contact your authorized Sun
		// service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ALERT, MESSAGE,
		    SYSTEXT("getlocalhostname() failed; unable to execute "
			    "zoneadm reboot for zone %s"), zonename);
		sc_syslog_msg_done(&handle);
		return;
	}
	//
	// Since we are launching the command in background, the timeout
	// should have no effect; but make it amply long just in case.
	//
	fed_cmd.timeout = 3600;

	fed_cmd.security = SEC_UNIX_STRONG;

	// Execute the command in background.
	fed_cmd.flags = FE_ASYNC;

	fed_cmd.slm_flags = METH_FEDSLM_BYPASS;

	//
	// create the arguments to the zoneadm command
	//
	char *fed_opts_argv[5];

	fed_opts_argv[0] = "zoneadm";
	fed_opts_argv[1] = "-z";
	fed_opts_argv[2] = const_cast<char *>(zonename);
	fed_opts_argv[3] = reboot_node ? "reboot" : "halt";
	fed_opts_argv[4] = NULL;

	//
	// set environment for method execution
	//
	char **fed_opts_env = NULL;
	if ((fed_opts_env = fe_set_env_vars()) == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");

		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to set up environment
		// variables for executing zoneadm. The zone will stay
		// up, but the rgmd will not attempt to start services
		// on it.
		// @user_action
		// Reboot the zone.
		// Examine other syslog messages occurring at about
		// the same time to see if the problem can be
		// identified. Save a copy of the /var/adm/messages
		// files on all nodes and contact your authorized Sun
		// service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ALERT, MESSAGE,
		    SYSTEXT("fe_set_env_vars() failed for "
			"zoneadm for zone %s"), zonename);
		sc_syslog_msg_done(&handle);

		return;
	}

	//
	// Since fe_run() is invoked in 'async' (non-blocking) mode, there
	// should be no need to retry on FE_DUP error.  By the time fe_run()
	// returns, rpc.fed will already have deleted the handle for the
	// currently executing command and will no longer monitor it.
	//
	// The rgmd will not attempt to make multiple reboot calls for the same
	// zone incarnation, because the first call will set the LN status to
	// SHUTTING_DOWN, and subsequent calls (in launch_method) check the
	// status before making the reboot call.
	//
	fe_run_result fed_result = {FE_OKAY, 0, 0, SEC_OK, NULL};

	if (fe_run(tag, &fed_cmd, "/usr/sbin/zoneadm",
		fed_opts_argv, fed_opts_env, &fed_result) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");

		//
		// SCMSGS
		// @explanation
		// The rgmd failed in an attempt to execute a
		// zoneadm reboot call, due to a failure to
		// communicate with the rpc.fed daemon. The zone will
		// stay up, but the rgmd will not attempt to start
		// services on the zone.
		// @user_action
		// Reboot the zone.
		// Examine other syslog messages occurring at about
		// the same time to see if the problem can be
		// identified. Save a copy of the /var/adm/messages
		// files on all nodes and contact your authorized Sun
		// service provider for assistance in diagnosing the
		// problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ALERT, MESSAGE,
		    "Failed to execute zoneadm reboot for zone %s",
		    zonename);
		sc_syslog_msg_done(&handle);

		return;
	}

	if (fed_result.type != FE_OKAY) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The attempted zoneadm call failed. The
		// zone will stay up, but the rgmd will not
		// attempt to start services on the zone.
		// @user_action
		// Reboot the zone.
		// Examine other syslog messages occurring at
		// about the same time to see if the problem
		// can be identified. Save a copy of the
		// /var/adm/messages files on all nodes and
		// contact your authorized Sun service
		// provider for assistance in diagnosing the
		// problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ALERT,
		    MESSAGE, "zoneadm call failed for zone "
		    "%s: %d", zonename, fed_result.type);
		sc_syslog_msg_done(&handle);
	}

	//
	// We cannot check the zoneadm exit status; the ASYNC flag to fe_run
	// does not reap any status from the child process.
	//
}

//
// is_r_started
//
// Returns true if rx_state indicates that 'r' is fully started.
// 'r' is considered started if rx_state is ONLINE (with monitoring enabled)
// or ONLINE_UNMON (or JUST_STARTED) (with monitoring disabled).
// Returns false otherwise.
//
boolean_t
is_r_started(rlist_p_t r, rgm::rgm_r_state rx_state, rgm::lni_t lni)
{
	boolean_t is_mon = is_r_monitored(r, NULL, lni);

	if (r->rl_ccrtype->rt_proxy == B_TRUE) {
		if (rx_state == rgm::R_ONLINE ||
		    rx_state == rgm::R_JUST_STARTED ||
		    rx_state == rgm::R_ONLINE_STANDBY)
			return (B_TRUE);
		else
			return (B_FALSE);
	} else if ((is_mon && rx_state == rgm::R_ONLINE) ||
	    (!is_mon && (rx_state == rgm::R_ONLINE_UNMON ||
	    rx_state == rgm::R_JUST_STARTED))) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}

//
//  Utilities for 'rgm_timelist_t'
//

//
// rgm_prune_timelist
//
// Given a ptr to a ptr to an rgm_timlelist_t (rgtp), and a time_t
// prune_tm, prune and delete all elements of the list that have a timestamp
// older than prune_tm seconds before the current time.
// The timelist_t is assumed to be in "oldest elements first" order.
//
// If prune_tm is zero, then all elements of the list are pruned.
//
// The memory pointed to by rgtp is set to point to the head of the pruned list,
// or to NULL if the pruned list is empty.
//
void
rgm_prune_timelist(rgm_timelist_t **rgtp, time_t prune_tm)
{
	time_t			curr_tm;
	boolean_t		prune_all = (prune_tm == 0 ? B_TRUE : B_FALSE);
	struct rgm_timelist	*prev_rgt, *rgt;

	curr_tm = time((time_t *)0);
	rgt = *rgtp;

	// Visit all nodes in list which are more than prune_tm older
	// than the current time.
	while ((rgt != NULL) && (prune_all || (curr_tm - rgt->tl_time >
	    prune_tm))) {
		prev_rgt = rgt;
		rgt = rgt->tl_next;
		delete (prev_rgt);
	}
	*rgtp = rgt;
}

//
// rgm_count_timelist
//
// Given a ptr to an rgm_timelist_t (rgtimep), count all elements of the
// list, and return this count.
//
// In order to ensure the correct count w.r.t a "history interval",
// rgm_prune_timelist should be called with the desired "history interval"
// before calling rgm_count_timelist.
//
uint_t
rgm_count_timelist(rgm_timelist_t *rgtimep)
{
	uint_t			count = 0;
	struct rgm_timelist	*rgt;

	rgt = rgtimep;
	// Visit all nodes in list, and count the nodes.
	while (rgt != NULL) {
		count++;
		rgt = rgt->tl_next;
	}
	return (count);
}

rgm_timelist_t *
rgm_clone_timelist(rgm_timelist_t *tlist)
{
	rgm_timelist_t *curr_p, *new_p = NULL, *next_p = NULL;
	rgm_timelist_t *new_timelist = NULL;

	for (curr_p = tlist; curr_p != NULL; curr_p = curr_p->tl_next) {
		new_p = (rgm_timelist_t *)malloc(sizeof (rgm_timelist_t));


		new_p->tl_time = curr_p->tl_time;
		new_p->tl_next = NULL;

		// link in this node at end of list
		if (new_timelist == NULL) {
			new_timelist = new_p;
		} else {
			next_p->tl_next = new_p;
		}
		next_p = new_p;
	}

	return (new_timelist);
}

//
// rgm_append_to_timelist
//
// Given a ref to a ptr to an rgm_timelist_t (rgtp), append a new
// element to the end of the list with a timestamp of the current time.
// If the operation is successful, 0 is returned. Otherwise the
// (non-zero) errno is returned.
//
int
rgm_append_to_timelist(rgm_timelist_t **rgtp)
{
	rgm_timelist_t	*rgt_last, *new_rgt;
	int		myerrno;

	// create new rgm_timelist_t element
	// -- Check for new/malloc failure
	new_rgt = new rgm_timelist_t;
	if (new_rgt == NULL) {
		myerrno = ENOMEM;
		return (myerrno);
	}
	new_rgt->tl_next = NULL;

	// set timestamp on new elt.
	// -- check for error
	if (time(&(new_rgt->tl_time)) < 0) {
		myerrno = errno;
		delete(new_rgt);
		return (myerrno);
	}

	// If current list is empty, then set rgtp to point
	// to new elt. Otherwise, append new elt. to end of list
	if (*rgtp == NULL) {
		*rgtp = new_rgt;
	} else {
		// Set rgt_last to point to the last (newest) node
		// in the list
		rgt_last = *rgtp;
		while (rgt_last->tl_next != NULL) {
			rgt_last = rgt_last->tl_next;
		}
		rgt_last->tl_next = new_rgt;
	}
	return (0);
}

//
// end rgm_timelist_t utilities
//

#ifndef EUROPA_FARM
//
// check_dependencies_rg
// ---------------------
// Some inter-RG dependencies can be resolved by changes in RG state,
// so whenever there is an RG state change, we need to check if it is
// possible that it resolved inter-RG dependencies, and, if so, do
// a more detailed check.
//
// Details about the types of RG state changes that can affect inter-RG
// dependencies are inline.
//
static void
check_dependencies_rg(rglist_p_t rgp, rgm::rgm_rg_state rgx_state_old,
    rgm::rgm_rg_state rgx_state_new)
{

	//
	// If the RG in question was moving offline, but now reached
	// some form of offline, or changed direction somehow, we might
	// have fulfilled stopping dependencies for one of the RG's resources'
	// dependents.  eg: a resource in this group is dependent on a resource
	// in another group.  That resource was trying to go offline, but
	// because this RG was going offline, that resource waited.  Now
	// that this RG is no longer trying to go offline, we don't need
	// to wait anymore, because the dependent resource is either offline
	// (about which we would have been notified through
	// check_dependencies_resource) or will no longer go offline (if
	// the RG changed direction).  Note that the case we're really trying
	// to catch here is the RG changing direction, because the
	// normal movement to offline will have been handled by the resourace
	// state changes.
	//
	// Also, we might have fulfilled starting dependencies for a resource
	// dependent on one of this rg's resources.  That could happen if
	// a resource dependent on a resource in this RG noticed that this
	// RG was moving offline, so it waited until things quiesced.  Now
	// that the RG is no longer moving offline, we need to check if
	// the resource for which we were waiting is still online, in which
	// case we can go ahead and fulfill our dependencies.
	//
	if (is_rg_offlining(rgx_state_old) &&
	    !is_rg_offlining(rgx_state_new)) {
		ucmm_print("RGM", NOGET("DEPENDENCIES: check_dependencies_rg: "
		    "<%s> change from offlining to not offlining"),
		    rgp->rgl_ccr->rg_name);
		check_stopping_dependencies_rg(rgp);
		check_starting_dependencies_rg(rgp);
	}

	//
	// If the RG in question started moving offline, we may have fulfilled
	// weak starting dependencies for a resource dependent on one of
	// the resources in this RG.  That is because weak starting deps
	// wait only while the dependee is also starting.  If the RG
	// changes direction, we don't neccesarily need to wait anymore.
	//
	if (is_rg_offlining(rgx_state_new) &&
	    !is_rg_offlining(rgx_state_old)) {
		ucmm_print("RGM", NOGET("DEPENDENCIES: check_dependencies_rg: "
		    "<%s> change from not offlining to offlining"),
		    rgp->rgl_ccr->rg_name);
		check_starting_dependencies_rg(rgp);
	}

	//
	// If the RG in question was previously ON_PENDING_R_RESTART,
	// PENDING_ONLINE, PENDING_ON_STARTED, ON_PENDING_DISABLED,
	// or ON_PENDING_MON_DISABLED
	// but now isn't, we may have fulfilled starting dependencies
	// for a resource that is dependent on a resource in this RG.
	// That is because resources trying to start wait for dependee
	// resources that are restarting.  If the RG decides not to restart,
	// or finishes restarting, we need to check if dependencies are
	// fulfilled.
	if (can_rg_have_restarting_r(rgx_state_old) &&
	    !can_rg_have_restarting_r(rgx_state_new)) {
		ucmm_print("RGM", NOGET("DEPENDENCIES: check_dependencies_rg: "
		    "<%s> change from state where r was possibly restarting"),
		    rgp->rgl_ccr->rg_name);
		check_starting_dependencies_rg(rgp);
	}
}


//
// check_starting_dependencies_rg
// ------------------------------
// For each resource in this RG, check starting dependencies on all
// resources that depend on the resource (all resources in the rl_dependents
// lists).
//
static void
check_starting_dependencies_rg(rglist_p_t rgp)
{
	rlist_p_t rp = NULL;

	ucmm_print("RGM", NOGET("DEPENDENCIES: "
	    "check_starting_dependencies_rg: <%s> "), rgp->rgl_ccr->rg_name);

	//
	// iterate through all the resources of the RG
	//
	for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		//
		// See if we've fulfilled starting dependencies
		// for any of our dependents: weak, strong, and restart.
		//
		check_starting_dependencies_from_list(
		    rp->rl_dependents.dp_weak, rp->rl_ccrdata->r_name,
		    DEPS_WEAK);
		check_starting_dependencies_from_list(
		    rp->rl_dependents.dp_strong, rp->rl_ccrdata->r_name,
		    DEPS_STRONG);
		check_starting_dependencies_from_list(
		    rp->rl_dependents.dp_restart, rp->rl_ccrdata->r_name,
		    DEPS_RESTART);
		check_starting_dependencies_from_list(
		    rp->rl_dependents.dp_offline_restart,
		    rp->rl_ccrdata->r_name,
		    DEPS_OFFLINE_RESTART);

		check_starting_dependencies_from_list(
		    rp->rl_ccrdata->r_inter_cluster_dependents.dp_weak,
		    rp->rl_ccrdata->r_name, DEPS_WEAK);
		check_starting_dependencies_from_list(
		    rp->rl_ccrdata->r_inter_cluster_dependents.dp_strong,
		    rp->rl_ccrdata->r_name, DEPS_STRONG);
		check_starting_dependencies_from_list(
		    rp->rl_ccrdata->r_inter_cluster_dependents.dp_restart,
		    rp->rl_ccrdata->r_name, DEPS_RESTART);
		check_starting_dependencies_from_list(
		    rp->rl_ccrdata->r_inter_cluster_dependents.
		    dp_offline_restart,
		    rp->rl_ccrdata->r_name, DEPS_OFFLINE_RESTART);

	}
}


//
// check_stopping_dependencies_rg
// -------------------------------
// For each resource in this RG, check if any resources that it depends on
// have their stopping dependencies resolved.
//
void
check_stopping_dependencies_rg(rglist_p_t rgp)
{
	rlist_p_t rp = NULL;

	ucmm_print("RGM", NOGET("DEPENDENCIES: "
	    "check_stopping_dependencies_rg: <%s> "), rgp->rgl_ccr->rg_name);

	//
	// iterate through all the resources of the RG
	//
	for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		//
		// See if we've fulfilled any stopping dependencies
		// for any of the resources on which we depend: weak,
		// strong, and restart.
		//
		check_stopping_dependencies_from_list(
		    rp->rl_ccrdata->r_dependencies.dp_weak,
		    rp->rl_ccrdata->r_name, DEPS_WEAK);
		check_stopping_dependencies_from_list(
		    rp->rl_ccrdata->r_dependencies.dp_strong,
		    rp->rl_ccrdata->r_name, DEPS_STRONG);
		check_stopping_dependencies_from_list(
		    rp->rl_ccrdata->r_dependencies.dp_restart,
		    rp->rl_ccrdata->r_name, DEPS_RESTART);
	}
}


//
// check_dependencies_resource
// ------------------------
// If the resource just changed state to STOP_FAILED or OFFLINE,
// checks if the new state for the resource fulfills stopping
// dependencies for any resource on which it depends.
//
// If the resource just changed state to JUST_STARTED or START_FAILED,
// checks if the new state for the resource fulfills starting dependencies
// for any resource that depends on it.
//
// If the resource just changed state to STARTING_DEPEND or
// STOPPING_DEPEND, checks if the dependencies are already fulfilled.
//
// If it is determined that dependencies are fulfilled for any resource,
// the slave (or slaves) are notified via an
// rgm_notify_dependencies_resolved idl call.
//
// All the work on dependencies checking and slave notification is done
// by the helper functions check_starting_dependencies and
// check_stopping_dependencies (and the wrappers
// check_starting_dependencies_from_list and
// check_stoping_dependencies_from_list).
//
// Note: if the state of the resource has just changed to JUST_STARTED,
// we make a call to the idl helper call_ack_start, to tell the slave
// that it can move the state to ONLINE_UNMON.
//
// If the resource has just started, trigger restart dependencies.
// If the resource is about to stop, trigger offline restart dependencies.
//
static void
check_dependencies_resource(rgm::lni_t lni, rlist_p_t rp,
    rgm::rgm_r_state rx_state)
{
	LogicalNodeset nset;

	ucmm_print("RGM", NOGET("DEPENDENCIES: "
	    "check_dependencies_resource: <%s> "), rp->rl_ccrdata->r_name);

	switch (rx_state) {
	case rgm::R_JUST_STARTED:
		//
		// Trigger restart dependencies.  We do this before calling
		// check_starting_dependencies() because we don't want the
		// dependent resource to move into a starting state and then
		// get a triggered restart.  Dependents that are currently in
		// R_STARTING_DEPEND will not get restarted.
		//

		FAULTPT_RGM(FAULTNUM_RGM_BEFORE_TRIGGER_RESTART_DEPS,
		    FaultFunctions::generic);

		nset.addLni(lni);
		trigger_restart_dependencies(rp, nset, B_FALSE);

		// FALLTHRU
	case rgm::R_START_FAILED:
		//
		// See if we've fulfilled starting dependencies
		// for any of our dependents: weak, strong, and restart.
		//
		// Note: START_FAILED could posibly fulfill weak dependencies.
		//
		check_starting_dependencies_from_list(
		    rp->rl_dependents.dp_weak, rp->rl_ccrdata->r_name,
		    DEPS_WEAK);
		check_starting_dependencies_from_list(
		    rp->rl_dependents.dp_strong, rp->rl_ccrdata->r_name,
		    DEPS_STRONG);
		check_starting_dependencies_from_list(
		    rp->rl_dependents.dp_restart, rp->rl_ccrdata->r_name,
		    DEPS_RESTART);
		check_starting_dependencies_from_list(
		    rp->rl_dependents.dp_offline_restart,
		    rp->rl_ccrdata->r_name, DEPS_OFFLINE_RESTART);

		// See for inter-cluster dependents as well.
		check_starting_dependencies_from_list(
		    rp->rl_ccrdata->r_inter_cluster_dependents.dp_weak,
		    rp->rl_ccrdata->r_name, DEPS_WEAK);
		check_starting_dependencies_from_list(
		    rp->rl_ccrdata->r_inter_cluster_dependents.dp_strong,
		    rp->rl_ccrdata->r_name, DEPS_STRONG);
		check_starting_dependencies_from_list(
		    rp->rl_ccrdata->r_inter_cluster_dependents.dp_restart,
		    rp->rl_ccrdata->r_name, DEPS_RESTART);
		check_starting_dependencies_from_list(
		    rp->rl_ccrdata->r_inter_cluster_dependents.
		    dp_offline_restart, rp->rl_ccrdata->r_name,
		    DEPS_OFFLINE_RESTART);

		//
		// Tell the slave that we're done processing the JUST_STARTED
		// state, so it can move to ONLINE_UNMON.
		//
		if (rx_state == rgm::R_JUST_STARTED) {
			(void) call_ack_start(lni, rp->rl_ccrdata->r_name);
		}

		break;
	case rgm::R_ONLINE_STANDBY:
		nset.addLni(lni);
		trigger_restart_dependencies(rp, nset, B_TRUE);
		break;
	case rgm::R_STARTING:
	case rgm::R_OFFLINE:
	case rgm::R_STOP_FAILED:
		//
		// See if we've fulfilled any stopping dependencies
		// for any of the resources on which we depend: weak,
		// strong, restart and offline restart.
		//
		check_stopping_dependencies_from_list(
		    rp->rl_ccrdata->r_dependencies.dp_weak,
		    rp->rl_ccrdata->r_name, DEPS_WEAK);
		check_stopping_dependencies_from_list(
		    rp->rl_ccrdata->r_dependencies.dp_strong,
		    rp->rl_ccrdata->r_name, DEPS_STRONG);
		check_stopping_dependencies_from_list(
		    rp->rl_ccrdata->r_dependencies.dp_restart,
		    rp->rl_ccrdata->r_name, DEPS_RESTART);
		check_stopping_dependencies_from_list(
		    rp->rl_ccrdata->r_dependencies.dp_offline_restart,
		    rp->rl_ccrdata->r_name, DEPS_OFFLINE_RESTART);
		break;
	case rgm::R_STARTING_DEPEND:
		nset.addLni(lni);
		check_starting_dependencies(rp, nset);
		break;
	case rgm::R_STOPPING_DEPEND:
		nset.addLni(lni);
		trigger_restart_dependencies(rp, nset, B_TRUE);
		check_stopping_dependencies(rp, nset);
		break;
	default:
		// nothing to do for all other states
		break;
	}
}

#if SOL_VERSION >= __s10
//
// handle_inter_cluster_dependency_r
// Notification function which makes an idl call to the remote rgmd to
// notify the satisfaction of any dependencies to the inter cluster
// dependent resource.
//
void
handle_inter_cluster_dependency_r(rdeplist_t *dep_list, name_t r_name,
	deptype_t deps_kind, rgm::remote_rs_status_flag_t status,
	LogicalNodeset &nset, rlist_p_t rp, LogicalNodeset *ns_nodelistp) {

	rgm::rgm_comm_var prgm_pres_v;
	rgm::idl_rs_get_state_args gets_args;
	rgm::idl_regis_result_t_var gets_res;
	Environment e;
	boolean_t is_rp_last_inst_stopped = B_TRUE;
	char *rc = NULL, *rr = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	int err;
	uint32_t clid;

	// Get the remote cluster name and resource name.
	(void) get_remote_cluster_and_rorrg_name(
	    dep_list->rl_name, &rc, &rr);
	if ((rr == NULL) || (rc == NULL))
		return;

	// Check if the remote zone cluster is valid.
	err = clconf_get_cluster_id(rc, &clid);
	if (((err == 0) && (clid != 0) && (clid < MIN_CLUSTER_ID)) ||
	    ((err != 0) && (err != EACCES))) {
		// get the stale dependent R.
		// invoke the cleanup thread.
		cleanup_r_rg(r_name, dep_list->rl_name, B_FALSE, deps_kind);
		free(rr);
		free(rc);
		return;
	}

	prgm_pres_v = rgm_comm_getpres_zc(rc);
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_IC_UNREACH,
		    rc);
		free(rr);
		free(rc);
		return;
	}


	// fill in the args for the idl call.
	if (deps_kind == DEPS_OFFLINE_RESTART ||
	    deps_kind == DEPS_RESTART) {
		rgm::lni_t lni;
		lni = LNI_UNDEF;
		int i = 0;
		gets_args.nodeset_seq.length(nset.count());
		while ((lni = nset.nextLniSet(lni + 1)) !=
		    0) {
			gets_args.nodeset_seq[i++] = lni;
		}
	}

	gets_args.idlstr_cluster_name = strdup(rc);
	gets_args.idlstr_rs_name = strdup(rr);
	gets_args.idlstr_src_cluster_name = strdup(ZONE);
	gets_args.idlstr_src_rs_name = strdup(r_name);
	gets_args.depskind = deps_kind;
	gets_args.flags = 0;
	gets_args.flags |= status;

	if (deps_kind == DEPS_OFFLINE_RESTART) {
		int i = 0;
		if (ns_nodelistp != NULL) {
			while ((i = ns_nodelistp->nextLniSet
			    (i + 1)) != 0) {
				if (!nset.containLni(i) &&
				    is_r_online(
				    rp->rl_xstate[i].rx_state)) {
					is_rp_last_inst_stopped =
					    B_FALSE;
					break;
				}
			}
		}
	}
	gets_args.r_last_inst_stopped = is_rp_last_inst_stopped;
	prgm_pres_v->idl_notify_remote_r(gets_args, gets_res,
	    e);
	if ((res = except_to_scha_err(e)).err_code !=
	    SCHA_ERR_NOERR) {
		rgm_format_errmsg(&res,
		    REM_RGM_NOTIFY_REMOTE_R_STATE_FAILED,
		    rc, rr);
		free(rr);
		free(rc);
		return;
	}

	res.err_code = (scha_err_t)gets_res->ret_code;
	if ((const char *)gets_res->idlstr_err_msg)
		res.err_msg = strdup(gets_res->idlstr_err_msg);

	if (res.err_code != SCHA_ERR_NOERR) {
		// if this notification was invalid then need to remove the
		// dependent information.
		if ((res.err_code == SCHA_ERR_INVALID_REMOTE_DEP) ||
		    (res.err_code == SCHA_ERR_RSRC)) {
			cleanup_r_rg(r_name, dep_list->rl_name, B_FALSE,
			    deps_kind);
			free(rr);
			free(rc);
			return;
		}
		rgm_format_errmsg(&res, REM_RGM_NOTIFY_REMOTE_R_STATE_FAILED,
		    rc, rr);
		free(rr);
		free(rc);
	}
}

// handle_inter_cluster_rg_affinities
// This function contacts the remote cluster president rgmd and
// notifies the source affinity rg to go offline or rebalance based
// on the status flag. cleanup_r_rg is called only when cleanup_stale_affs
// is true. cleanup flag will be set if cleanup of stale inter cluster
// affinitents was carried out.
//
scha_err_t
handle_inter_cluster_rg_affinities(const char *rg_name,
    name_t target_rg_name, rgm::remote_rg_status_flag_t status,
    boolean_t *is_changed, rgm::lni_t mynodeid,
    boolean_t ignore_stop_failed, boolean_t *cleanup_flag,
    boolean_t cleanup_stale_affs)
{

	rgm::rgm_comm_var prgm_pres_v;
	rgm::idl_notify_remote_rg_args gets_args;
	rgm::idl_notify_remote_rg_result_t_var gets_res;
	Environment e;
	char *rc = NULL, *rr = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm::lni_t lni;
	LogicalNode	*ln;
	int err;
	uint32_t clid;

	// get the remote cluster name.
	(void) get_remote_cluster_and_rorrg_name(
	    target_rg_name, &rc, &rr);
	if ((rr == NULL) || (rc == NULL))
		return (res.err_code);

	// Check if the remote zone cluster is valid.
	err = clconf_get_cluster_id(rc, &clid);
	if (((err == 0) && (clid != 0) && (clid < MIN_CLUSTER_ID)) ||
	    ((err != 0) && (err != EACCES))) {

		// get the stale source affinity RG.
		char *affinitent_rg_name = (char *)malloc(
		    strlen(target_rg_name) + 3);
		if (ignore_stop_failed)
			sprintf(affinitent_rg_name, "%s%s", SP,
			    target_rg_name);
		else
			sprintf(affinitent_rg_name, "%s%s", SN,
			    target_rg_name);
		// Invoke the cleanup thread.
		if (cleanup_stale_affs) {
			cleanup_r_rg(rg_name, affinitent_rg_name, B_TRUE,
			    (deptype_t)NULL);
			*cleanup_flag = B_TRUE;
		}
		free(affinitent_rg_name);
		free(rr);
		free(rc);
		return (res.err_code);
	}

	// get the rgm president reference.
	prgm_pres_v = rgm_comm_getpres_zc(rc);
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_IC_UNREACH, rc);
		free(rr);
		free(rc);
		return (res.err_code);
	}

	// fill in the args
	gets_args.idlstr_cluster_name = strdup(rc);
	gets_args.idlstr_rg_name = strdup(rr);
	gets_args.flag = 0;
	gets_args.flag |= status;

	// if the status flag is FORCE_OFFLINE
	if (status == rgm::FORCE_OFFLINE) {
		*is_changed = B_TRUE;
		gets_args.nid = mynodeid;
		// if the status flag is REBALANCE_TARGET_AFFINITY_RG
	} else if (status == rgm::REBALANCE_TARGET_AFFINITY_RG) {
		lni = LNI_UNDEF;
		int i = 0;
		rglist_p_t rgp = rgname_to_rg(rg_name);
		gets_args.ignore_nodelist.length(
		    rgp->rgl_planned_on_nodes.count());
		while ((lni = rgp->rgl_planned_on_nodes.nextLniSet(
		    lni + 1)) != 0) {
			gets_args.ignore_nodelist[i++] = lni;
		}
		gets_args.rgl_no_rebalance = rgp->rgl_no_rebalance;
	}

	gets_args.idlstr_src_cluster_name = strdup(ZONE);
	gets_args.idlstr_src_rg_name = strdup(rg_name);
	gets_args.positive_aff = ignore_stop_failed;

	prgm_pres_v->idl_notify_remote_rg(gets_args, gets_res, e);


	if ((res = except_to_scha_err(e)).err_code !=
	    SCHA_ERR_NOERR) {
		rgm_format_errmsg(&res,
		    REM_RGM_NOTIFY_REMOTE_R_STATE_FAILED, rc, rr);
		free(rr);
		free(rc);
		return (res.err_code);
	}

	res.err_code = (scha_err_t)gets_res->ret_code;
	if ((const char *)gets_res->idlstr_err_msg)
		res.err_msg = strdup(gets_res->idlstr_err_msg);

	if (res.err_code != SCHA_ERR_NOERR) {
		//
		// if this notification was invalid or the remote rg
		// was invalidthen need to remove the
		// source affinity information.
		//
		if ((res.err_code == SCHA_ERR_INVALID_REMOTE_AFF) ||
		    (res.err_code == SCHA_ERR_RG)) {
			char *affinitent_rg_name = (char *)malloc(
				strlen(target_rg_name) + 3);
			if (ignore_stop_failed)
				sprintf(affinitent_rg_name, "%s%s", SP,
				    target_rg_name);
			else
				sprintf(affinitent_rg_name, "%s%s", SN,
				    target_rg_name);
			if (cleanup_stale_affs) {
				cleanup_r_rg(rg_name, affinitent_rg_name,
				    B_TRUE, (deptype_t)NULL);
				*cleanup_flag = B_TRUE;
			}
			free(affinitent_rg_name);
			free(rr);
			free(rc);
			return (res.err_code);
		}
		rgm_format_errmsg(&res, REM_RGM_NOTIFY_REMOTE_R_STATE_FAILED,
		    rc, rr);
		free(rr);
		free(rc);
		return (res.err_code);
	}
	free(rr);
	free(rc);
	return (res.err_code);
}


//
// get_remoter_r_state_helper
//
boolean_t
get_remote_r_state_helper(rlist_p_t rp, name_t r_name, rdep_locality_t loc_type,
    deptype_t deps_kind, LogicalNodeset &nset, boolean_t start_dep_check,
    boolean_t *is_invalid_rs)
{
	rgm::rgm_comm_var prgm_pres_v;
	rgm::idl_rs_get_state_args gets_args;
	rgm::idl_rs_get_state_result_t_var gets_res;
	Environment e;
	char *rc = NULL, *rr = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	gets_args.flags = 0;
	(void) get_remote_cluster_and_rorrg_name(
	    r_name, &rc, &rr);
	if ((rr == NULL) || (rc == NULL)) {
		if (start_dep_check)
			return (B_FALSE);
		else
			return (B_TRUE);
	}

	prgm_pres_v = rgm_comm_getpres_zc(rc);
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_IC_UNREACH,
		    rc);
		free(rr);
		free(rc);
		if (start_dep_check)
			return (B_FALSE);
		else
			return (B_TRUE);
	}

	rgm::lni_t lni;
	lni = LNI_UNDEF;
	int i = 0;


	if (start_dep_check) {
		if (loc_type == LOCAL_NODE) {
			gets_args.nodeset_seq.length(nset.count());
			while ((lni = nset.nextLniSet(lni + 1)) != 0) {
				gets_args.nodeset_seq[i++] = lni;
			}
			gets_args.flags |=
			    rgm::ONLINE_STATUS_LOCAL_NODE;
		} else if (loc_type == ANY_NODE) {
			gets_args.flags |= rgm::ONLINE_STATUS;
		} else {
			gets_args.nodeset_seq.length(nset.count());
			while ((lni = nset.nextLniSet(lni + 1)) != 0) {
				gets_args.nodeset_seq[i++] = lni;
			}
			gets_args.flags |=
			    rgm::ONLINE_STATUS_DEP_LOC_TYPE_UNKNOWN;
		}
	} else {
		gets_args.nodeset_seq.length(nset.count());
		while ((lni = nset.nextLniSet(lni + 1)) != 0) {
			gets_args.nodeset_seq[i++] = lni;
		}
		gets_args.flags |= rgm::OFFLINE_STATUS;
	}

	gets_args.idlstr_cluster_name = strdup(rc);
	gets_args.idlstr_rs_name = strdup(rr);
	gets_args.idlstr_src_cluster_name = strdup(ZONE);
	gets_args.idlstr_src_rs_name =
	    strdup(rp->rl_ccrdata->r_name);
	gets_args.idlstr_src_rg_name =
	    strdup(rp->rl_ccrdata->r_rgname);
	gets_args.depskind = deps_kind;
	gets_args.idlstr_cluster_name = strdup(rc);
	gets_args.idlstr_rs_name = strdup(rr);
	prgm_pres_v->idl_get_remote_r_state(gets_args,
	    gets_res, e);
	if ((res = except_to_scha_err(e)).err_code !=
	    SCHA_ERR_NOERR) {
		rgm_format_errmsg(&res,
		    REM_RGM_GET_REMOTE_R_STATE_FAILED,
		    rc, rr);
		free(rr);
		free(rc);
		if (start_dep_check)
			return (B_FALSE);
		else
			return (B_TRUE);
	}


	res.err_code = (scha_err_t)gets_res->ret_code;
	if (res.err_code == SCHA_ERR_RSRC)
		// Check if the resource is invalid.
		if (is_invalid_rs != NULL)
			*is_invalid_rs = B_TRUE;
	if (res.err_code == SCHA_ERR_NOERR) {
		if ((!start_dep_check) && (deps_kind == DEPS_OFFLINE_RESTART)) {
			if (gets_res->status != rgm::R_OFFLINE)
				return (B_FALSE);
		} else if (start_dep_check) {
				if (gets_res->status != rgm::R_ONLINE)
					return (B_FALSE);
		}
	}
	return (B_TRUE);
}
#endif

//
// check_starting_dependencies_from_list
// -------------------------------------
// Iterates over the list of resources.  For each resource, calls
// check_blocked_dependencies to see if any dependencies need to be checked,
// and to check them.
//
static void
check_starting_dependencies_from_list(rdeplist_t *deps_list, name_t r_name,
    deptype_t deps_kind)
{
	rdeplist_t *curr_node = NULL;
	rlist_p_t rp = NULL;

	//
	// iterate through all the resources in the dep list (could be
	// either dependencies or dependents).  We also
	// don't care whether they represent weak, strong, or restart
	// dependents, because we treat them the same at this point.
	//
	for (curr_node = deps_list; curr_node != NULL;
	    curr_node = curr_node->rl_next) {
		if (!is_enhanced_naming_format_used(curr_node->rl_name)) {
			rp = rname_to_r(NULL, curr_node->rl_name);
			CL_PANIC(rp != NULL);

			check_blocked_dependencies(rp, B_TRUE, B_FALSE);
		} else {
#if SOL_VERSION >= __s10
			// handle remote dependencies.
			// Check if any inter-cluster resource is in
			// starting depend. If so notify it.
			if (!allow_inter_cluster()) {
				ucmm_print(
				    "check_starting_dependencies_from_list",
				    NOGET(
				    "Running at too low a version:R <%s> "
				    "has dependency on R <%s> "
				    "which is in not in local cluster\n"),
				    r_name, curr_node->rl_name);
				continue;
			}
			LogicalNodeset nset;
			handle_inter_cluster_dependency_r(curr_node, r_name,
			    deps_kind, rgm::STARTING_DEPENDENCIES_SATISFIED,
			    nset, NULL, NULL);
#endif
		}
	}
}


//
// check_stopping_dependencies_from_list
// -------------------------------------
// Iterates over the list of resources.  For each resource, calls
// check_blocked_dependencies to see if any dependencies need to be checked,
// and to check them.
//
static void
check_stopping_dependencies_from_list(rdeplist_t *deps_list, name_t r_name,
    deptype_t deps_kind)
{
	rdeplist_t *curr_node = NULL;
	rlist_p_t rp = NULL;

	//
	// iterate through all the resources in the dep list (could be
	// either dependencies or dependents).  We also
	// don't care whether they represent weak, strong, or restart
	// dependents, because we treat them the same at this point.
	//
	for (curr_node = deps_list; curr_node != NULL;
	    curr_node = curr_node->rl_next) {
		if (!is_enhanced_naming_format_used(curr_node->rl_name)) {
			rp = rname_to_r(NULL, curr_node->rl_name);
			CL_PANIC(rp != NULL);

			check_blocked_dependencies(rp, B_FALSE, B_TRUE);
		} else {
#if SOL_VERSION >= __s10
			if (!allow_inter_cluster()) {
				ucmm_print(
				    "check_stopping_dependencies_from_list",
				    NOGET(
				    "Running at too low a version:R <%s> "
				    "has dependency on R <%s> "
				    "which is in not in local cluster\n"),
				    r_name, curr_node->rl_name);
				continue;
			}
			LogicalNodeset nset;
			handle_inter_cluster_dependency_r(curr_node, r_name,
			    deps_kind, rgm::STOPPING_DEPENDENCIES_SATISFIED,
			    nset, NULL, NULL);
#endif
		}
	}
}


//
// check_starting_dependencies
// ---------------------------
// Precondition: the resource in question is R_STARTING_DEPEND on at
// least one node, and the nset contains one or more nodes on which the resource
// is R_STARTING_DEPEND.
//
// Check if all of weak, strong, and restart dependencies have been resolved.
// Note that, in the case of local-node dependencies, a resource can have
// starting dependencies resolved on a subset of the nodes on which it is
// R_STARTING_DEPEND.
//
// are_starting_dependencies_resolved will remove nodes from the
// nodeset on which the resource does not have starting_dependencies_resolved,
// but will return true as long as they are resolved on at least one of the
// nodes in nset.
//
// If dependencies are resolved for at least one node in nset, call the
// start_dependencies_resolved function to tell those nodes that this
// resource can be started.
//
static void
check_starting_dependencies(rlist_p_t rp, LogicalNodeset &nset)
{
	ucmm_print("RGM", NOGET("DEPENDENCIES: "
	    "check_starting_dependencies: <%s> "), rp->rl_ccrdata->r_name);

	if (are_starting_dependencies_resolved(rp,
	    rp->rl_ccrdata->r_dependencies.dp_weak, DEPS_WEAK, nset) &&
	    are_starting_dependencies_resolved(rp,
	    rp->rl_ccrdata->r_dependencies.dp_strong, DEPS_STRONG, nset) &&
	    are_starting_dependencies_resolved(rp,
	    rp->rl_ccrdata->r_dependencies.dp_restart, DEPS_RESTART, nset) &&
	    are_starting_dependencies_resolved(rp,
	    rp->rl_ccrdata->r_dependencies.dp_offline_restart,
	    DEPS_OFFLINE_RESTART, nset)) {
		start_dependencies_resolved(rp, nset);
	}
}


//
// start_dependencies_resolved
// --------------------------
// For each node, checks if this resource is R_STARTING_DEPEND on that node,
// and is in nset. If so, calls call_notify_dependencies_resolved to take care
// of notifying the slave, then sets the state to R_OK_TO_START, so we
// don't waste time checking dependencies for this resource again.
//
static void
start_dependencies_resolved(rlist_p_t rp, LogicalNodeset &nset)
{
	ucmm_print("RGM", NOGET("DEPENDENCIES: "
	    "start_dependencies_resolved: <%s> "), rp->rl_ccrdata->r_name);

	// iterate through the state of the resource on all nodes


	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);

		rgm::lni_t i = lnNode->getLni();

		if (!lnNode->isInCluster()) {
			continue;
		}

		r_xstate_t r_state = rp->rl_xstate[i];

		//
		// If we come accross a resource that is STARTING_DEPEND,
		// and is in nset, call the call_notify_dependencies_resolved
		// function to handle the idl to the slave.
		//
		// If the call completes successfully, a post-condition
		// is that the state of the resource on the slave has been
		// set to R_OK_TO_START.  So we go ahead and set that state
		// here on the president immediately so that we don't waste
		// time checking this dependency again (if there are other
		// state transitions queued before we would get the OK_TO_START
		// transition).
		//
		// Set the state by calling set_rx_state recursively.  It's
		// ok because we've held the lock the entire time.  Also,
		// OK_TO_START is not an interesting state for the dependency
		// checking, so the recursion is guarenteed to stop after
		// one level.
		//
		if (r_state.rx_state == rgm::R_STARTING_DEPEND &&
		    nset.containLni(i)) {
			if (call_notify_dependencies_resolved(i,
			    rp->rl_ccrdata->r_name) == RGMIDL_OK) {
				set_rx_state(i, rp, rgm::R_OK_TO_START);
			}
		}
	}
}


//
// are_starting_dependencies_resolved
// ----------------------------------
//
// There are several conditions that affect the semantics of resolving
// starting dependencies.
//
// A resource dependency is considered to be "local-node" if the containing
// RG of the dependent has a strong positive affinity for the containing RG
// of the dependee. If the resource dependency is "local-node," then the
// starting dependency of the resource on each node depends only on the
// state of its dependees on that node.  Therefore, a starting dependency
// might be satisfied on one node but not on a different node.
//
// If physical-node affinities are enabled, then a local-node dependency can
// be satisfied by a dependee on any zone on the same physical node as the
// dependent.
//
// A resource dependency is considered to be "local-node-weak" if the
// containing RG of the dependent has a weak positive affinity for the
// containing RG of the dependee.  In that case, the starting dependency
// of the resource on each node depends only on the state of its dependees on
// that node _if_ they are starting at the same time.  Otherwise, it defaults
// to the non-local-node dependency.
// With the implementation of FBC 1403, the user is now allowed
// to specify the flavour of local-node/any-node dependency. The locality_type
// field of the rdeplist_t has the local-node/any-node settings
// for the dependency.
//
// Also, we treat weak resource dependencies differently from strong and
// restart resource dependencies.
//
// strong and restart dependencies: we wait (potentially forever)
// until all resources on which we depend are "at least" JUST_STARTED/
// ONLINE_UNMON and their containing Resource Groups are in some form of
// ONLINE or PENDING_ONLINE (on at least one node or on the local node,
// depending on the semantics described above).
//
// weak dependencies: we wait until the resource on which we depend
// is online on at least one node or on the local node (same condition as
// for strong and restart) OR we can tell that on all nodes (or on the
// local node) it's not heading online (see below).
//
// To check starting dependencies, iterate through each resource in the
// dep_list.  Ignore resources that are in the same RG as us. Depending
// on whether the dependency type is local-node or any-node, checks are
// made to find out as to whether the dependee is disabled on the
// required nodes. Since an enabled dependent can depend on a disabled
// dependee (project 1403), we would return B_FALSE as the dependent
// cannot run its start method before the dependee runs its start method
// in case of strong, restart and offline-restart dependencies. For weak
// dependency, the dependent could start even if the dependee is disabled
// on the required nodes or has not started up on those nodes.
//
// Foreach dependee resource, iterate through each node, checking its state.
// Keep track of the nodes on which it's online and on which it's moving
// online.  Then check if the dependency is satisfied for that dependee.
//
// If all of the following are true for a resource, it counts as online:
//
// * R is "at least" JUST_STARTED/ UNLINE_UNMON
// * Containing RG is some form of ONLINE or some form of PENDING_ONLINE.
// * This resource is not restarting.
//
// The first condition is pretty straightforward: we wait if the R is not
// online.  The second condition makes this check even more stringent: we
// wait if the R is online, but we know that it will shortly be offline.
// The third condition adds to the second condition, by waiting until the
// resource gets stopped and restarted before we start our dependent resource.
// We check if the resource in question is the one being restarted by checking
// the rx_restarting and rx_just_started flags.
//
// A resource is considered to be moving online if:
//
// The resource is "less than" JUST_STARTED/ONLINE_UNMON AND the RG is in
// some form of PENDING_ONLINE or ONLINE (OR the resource is restarting).
//
// Note that if R is moving in the stopping direction it doesn't count
// as heading online (unless it's restarting).  That is because there is no
// indication that the R will ever start again.
//
// Note on scalable services:  if a resource in the dep_list is scalable, and
// there are no positive affinities between the resource groups,
// we wait until the resource is "online" on at least one node
// or (for weak deps) we stop waitin if the resource is not coming online on
// any node.
//
// If the dependee has a positive affinity for the dependent, then the
// semantics are local-node, as described above.
//
boolean_t
are_starting_dependencies_resolved(rlist_p_t rp, rdeplist_t *deps_list,
    deptype_t deps_kind, LogicalNodeset &nset)
{
	rdeplist_t *curr_name = NULL;
	rlist_p_t rp_dep = NULL;
	rglist_p_t rgp = rp->rl_rg;
	boolean_t is_local_node, is_local_node_weak;
	LogicalNodeset satisfied_ns, moving_on_ns;
	char *ucmm_buffer = NULL;
	rglist_p_t rgp_dep = NULL;



	if (nset.display(ucmm_buffer) == 0) {
		ucmm_print("RGM", NOGET("DEPENDENCIES: "
		    "are_starting_dependencies_resolved: Checking <%s> "
		    "on nodes %s"),
		    rp->rl_ccrdata->r_name, ucmm_buffer);
		free(ucmm_buffer);
	}

	for (curr_name = deps_list; curr_name != NULL;
	    curr_name = curr_name->rl_next) {
		boolean_t is_r_remote = B_FALSE;

		if (is_enhanced_naming_format_used(curr_name->rl_name)) {
			is_r_remote = B_TRUE;
		}

		if (!is_r_remote) {
			rp_dep = rname_to_r(NULL, curr_name->rl_name);
			CL_PANIC(rp_dep != NULL);
			//
			// skip it if it's in the same RG.  Those dependencies
			// are checked by the slave in the state machine.
			//
			// Note: just compare rg pointers.
			//
			rgp_dep = rp_dep->rl_rg;
			if (rgp_dep == rgp) {
				continue;
			}
		} else {
#if SOL_VERSION >= __s10
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "check_starting_dependencies_from_list "
				    "Running at too low a version:R <%s> "
				    "has dependency on R <%s> "
				    "which is in not in local cluster\n"),
				    rp->rl_ccrdata->r_name, curr_name->rl_name);
				return (B_FALSE);
			}
			if (get_remote_r_state_helper(rp, curr_name->rl_name,
			    curr_name->locality_type, deps_kind, nset,
			    B_TRUE, NULL)) {
				continue;
			} else {
				return (B_FALSE);
			}
#endif
		}
		//
		// Figure out whether or not the dependency is local-node.
		//
		check_dep_local(rp_dep, rp, curr_name->locality_type,
		    &is_local_node, &is_local_node_weak);

		satisfied_ns.reset();
		moving_on_ns.reset();

		//
		// Now loop through the state of the dependee resource on all
		// nodes.  We keep track of the nodes on which the dependee
		// resource is online in satisfied_ns.  We also keep track
		// of the nodes on which it's moving online in moving_on_ns.
		//
		for (LogicalNodeManager::iter it = lnManager->begin();
		    it != lnManager->end(); ++it) {
			LogicalNode *lnNode = lnManager->findObject(it->second);
			CL_PANIC(lnNode != NULL);

			rgm::lni_t node = lnNode->getLni();

			if (!lnNode->isInCluster()) {
				continue;
			}
			//
			// If the dependee is disabled on a node, then the node
			// cannot be in satisfied_ns nor in moving_on_ns.
			//

			if (((rgm_switch_t *)rp_dep->rl_ccrdata->r_onoff_switch)
			    ->r_switch[node] ==
			    SCHA_SWITCH_DISABLED) {
				continue;
			}

			rg_xstate_t rg_state = rgp_dep->rgl_xstate[node];
			r_xstate_t r_state = rp_dep->rl_xstate[node];

			//
			// Check that the resource is online and not planning
			// to move offline soon.
			//
			if (is_r_online(r_state.rx_state) &&
			    !is_rg_offlining(rg_state.rgx_state)) {
				//
				// If the resource is restarting and
				// heading in the stopping direction,
				// we need to wait (at least for this
				// node).
				//
				if (get_r_restarting(curr_name->rl_name,
				    rg_state.rgx_state, node) !=
				    rgm::RR_STOPPING) {
					//
					// Add this node to our list
					// of nodes on which the dependency
					// is satisfied.
					//
					satisfied_ns.addLni(node);

					//
					// For non local-node deps,
					// we only need the deps satisfied
					// on one node.  As an optimization,
					// we break out of the loop here.
					//
					if (!is_local_node &&
					    !is_local_node_weak) {
						break;
					}
				}
			}
			//
			// Check if the resource is coming online
			// on the node.
			//
			rgm::r_restart_t rstart_flag = rgm::RR_NONE;
			(void) call_fetch_restarting_flag(node,
			    curr_name->rl_name, rstart_flag);
			if ((is_r_starting(r_state.rx_state) &&
			    !is_rg_offlining(rg_state.rgx_state)) ||
			    rstart_flag == rgm::RR_STOPPING) {
				moving_on_ns.addLni(node);
			}
		} // nodes loop

		//
		// Now that we've checked the state of the dependee on
		// all nodes, see if it satisfied dependencies for us.
		//

		//
		// If physical-node affinities are enabled, add to satisfied_ns
		// all zones that are on the same physical node as
		// an initial member of satisfied_ns.  We do the same for
		// moving_on_ns.
		// This means that LOCAL_NODE dependencies can be satisfied
		// by the dependee on any zone on the same physical node as the
		// dependent.
		//
		add_phys_zones_to_nodeset(satisfied_ns);
		add_phys_zones_to_nodeset(moving_on_ns);

		//
		// First, if this dependency was local node, we are much more
		// stringent than the normal (non local-node) case.  We need to
		// look at each node individually, and make sure that the
		// dependee is online on that node (or, in the case of
		// weak resource dependencies, the dependee is not moving
		// online).  This check will remove nodes from the nodelist
		// on which the dependency is not satisfied.
		//
		if (is_local_node) {
			if (deps_kind != DEPS_WEAK) {
				//
				// If we're not checking weak dependencies,
				// remove all nodes from nset on
				// which the dependency is not satisfied.
				//
				nset &= satisfied_ns;
			} else {
				//
				// We are checking weak deps.  Here we care
				// only that the dependee is not moving
				// online on the nodes of interest.  All
				// other nodes (whether the dependee is
				// online or offline) are fine.
				//
				nset -= moving_on_ns;
			}

			if (nset.display(ucmm_buffer) == 0) {
				ucmm_print("RGM", NOGET("DEPENDENCIES: "
				    "are_starting_dependencies_resolved: after "
				    "processing local-node dependencies for "
				    "<%s> on <%s>, ns = %s\n"),
				    rp->rl_ccrdata->r_name,
				    curr_name->rl_name, ucmm_buffer);
				    free(ucmm_buffer);
			}
		}

		//
		// If we're checking local-node-weak dependencies, we are only
		// slightly more stringent than the normal (non local-node)
		// checks. Specifically, not only must the dependency be
		// satisfied by the state of the dependee on some node, but
		// the dependee must not be moving online on the local
		// node.  Thus, if we are checking local-node-weak deps.,
		// remove those nodes on which the dependee is moving online
		// (we must wait until the dependee quiesces on that node).
		// Then, fall through to the normal (non local-node) case,
		// but with those particular nodes removed.
		//
		if (is_local_node_weak) {
			nset -= moving_on_ns;

			if (nset.display(ucmm_buffer) == 0) {
				ucmm_print("RGM", NOGET("DEPENDENCIES: "
				    "are_starting_dependencies_resolved: after "
				    "processing local-node-weak dependencies "
				    "for <%s> on <%s>, ns = %s"),
				    rp->rl_ccrdata->r_name,
				    curr_name->rl_name, ucmm_buffer);
				    free(ucmm_buffer);
			}
		}

		//
		// If we have no more nodes, we can't satisfy
		// deps on any node, so return false.  Note that, as long
		// as we have some nodes in nset, we have the possibility of
		// satisfying dependencies on those nodes.
		//
		if (nset.isEmpty()) {
			ucmm_print("RGM", NOGET("DEPENDENCIES: "
			    "are_starting_dependencies_resolved: "
			    "local-node dependency of <%s> on "
			    "<%s> not resolved on any node."),
			    rp->rl_ccrdata->r_name, curr_name->rl_name);
			return (B_FALSE);
		}


		//
		// Here we're checking non local-node semantics, so the
		// "satisfaction" is all or nothing (satisfied for all nodes or
		// for none).   Note that if we are checking local-node-weak
		// semantics, we want to do the following check.  We have
		// already handled the more stringent requirement for
		// local-node-weak semantics above.
		//
		if (!is_local_node) {
			if (!satisfied_ns.isEmpty() ||
			    (deps_kind == DEPS_WEAK &&
			    moving_on_ns.isEmpty())) {
				//
				// The dependency is satisfied on all nodes.
				//
				ucmm_print("RGM", NOGET("DEPENDENCIES: "
				    "are_starting_dependencies_resolved: "
				    "non local-node dependency of <%s> on "
				    "<%s> resolved."), rp->rl_ccrdata->r_name,
				    curr_name->rl_name);
			} else {
				//
				// Our dependency is not satisfied.
				// We return immediately.  There's no
				// point in checking other dependencies
				// when one of them is not satisfied.
				//
				ucmm_print("RGM", NOGET("DEPENDENCIES: "
				    "are_starting_dependencies_resolved: "
				    "non local-node dependency of <%s> on "
				    "<%s> not resolved yet."),
				    rp->rl_ccrdata->r_name,
				    curr_name->rl_name);
				return (B_FALSE);
			}
		}
	} // dependencies list loop

	//
	// We've checked all the resources in the list, and they've all
	// passed.  Return B_TRUE.
	//
	if (nset.display(ucmm_buffer) == 0) {
		ucmm_print("RGM", NOGET("DEPENDENCIES: "
		    "are_starting_dependencies_resolved: all dependencies "
		    " of <%s> resolved for deps kind <%d> on nodes %s"),
		    rp->rl_ccrdata->r_name, deps_kind, ucmm_buffer);
		free(ucmm_buffer);
	}
	return (B_TRUE);
}


//
// checks whether dependency is of type local node or any node.
// from_rg_affinities is considered to be one of these depending on certain
// characteristics.
// results are stored in is_local_node, and is_local_node_weak if they are
// non-NULL
//
void
check_dep_local(rlist_p_t rp, rlist_p_t rp_dep, rdep_locality_t type,
    boolean_t *is_local_node, boolean_t *is_local_node_weak)
{
	boolean_t local_node, local_node_weak;
	switch (type) {
	case LOCAL_NODE:
		local_node = B_TRUE;
		local_node_weak = B_FALSE;
		break;
	case ANY_NODE:
		local_node = B_FALSE;
		local_node_weak = B_FALSE;
		break;
	case FROM_RG_AFFINITIES:
		if (is_local_node != NULL) {
			local_node = is_dep_local_only(rp, rp_dep, B_TRUE);
		}
		if (is_local_node_weak != NULL) {
			local_node_weak =
			    is_dep_local_only(rp, rp_dep, B_FALSE);
		}
		break;
	default:
		CL_PANIC(0);
		// UNREACHABLE
	}
	if (is_local_node != NULL)
		*is_local_node = local_node;
	if (is_local_node_weak != NULL)
		*is_local_node_weak = local_node_weak;
}

//
// check_stopping_dependencies
// ---------------------------
// Precondition: the resource in question is R_STOPPING_DEPEND on at
// least one node, and the nset contains one or more nodes on which the
// resource is R_STOPPING_DEPEND.
//
// Check if all of weak, strong, and restart dependents are in a proper
// state for us to go offline.  Note that we check resources that
// have dependencies on us, not resource on which we are dependent.
//
// Note that, in the case of local-node dependencies, a resource can have
// stopping dependencies resolved on a subset of the nodes on which it is
// R_STOPPING_DEPEND.
//
// are_stopping_dependencies_resolved will remove nodes from the
// nodeset on which the resource does not have stopping dependencies resolved,
// but will return true as long as they are resolved on at least one of the
// nodes in nset.
//
// If dependencies are resolved for at least one node in nset, call the
// stop_dependencies_resolved function to tell those nodes that this
// resource can be stopped.
//

static void
check_stopping_dependencies(rlist_p_t rp, LogicalNodeset &nset)
{
	ucmm_print("RGM", NOGET("DEPENDENCIES: "
	    "check_stopping_dependencies: <%s> "), rp->rl_ccrdata->r_name);
	if (are_stopping_dependencies_resolved(rp, rp->rl_dependents.dp_weak,
	    DEPS_WEAK, nset) &&
	    are_stopping_dependencies_resolved(rp,
	    rp->rl_dependents.dp_offline_restart, DEPS_OFFLINE_RESTART,
	    nset) &&
	    are_stopping_dependencies_resolved(rp,
	    rp->rl_dependents.dp_strong, DEPS_STRONG, nset) &&
	    are_stopping_dependencies_resolved(rp,
	    rp->rl_dependents.dp_restart, DEPS_RESTART, nset) &&
	    are_stopping_dependencies_resolved(rp,
	    rp->rl_ccrdata->r_inter_cluster_dependents.dp_restart,
	    DEPS_RESTART, nset) &&
	    are_stopping_dependencies_resolved(rp,
	    rp->rl_ccrdata->r_inter_cluster_dependents.dp_offline_restart,
	    DEPS_OFFLINE_RESTART, nset) &&
	    are_stopping_dependencies_resolved(rp,
	    rp->rl_ccrdata->r_inter_cluster_dependents.dp_strong,
	    DEPS_STRONG, nset) &&
	    are_stopping_dependencies_resolved(rp,
	    rp->rl_ccrdata->r_inter_cluster_dependents.dp_weak,
	    DEPS_WEAK, nset)) {
		stop_dependencies_resolved(rp, nset);
	}
}


//
// stop_dependencies_resolved
// --------------------------
// For each node, checks if this resource is R_STOPPING_DEPEND on that node
// and is in nset. If so, calls call_notify_dependencies_resolved to take care
// of notifying the slave, then sets the state to R_OK_TO_STOP, so we
// don't waste time checking dependencies for this resource again.
//
static void
stop_dependencies_resolved(rlist_p_t rp, LogicalNodeset &nset)
{
	ucmm_print("RGM", NOGET("DEPENDENCIES: "
	    "stop_dependencies_resolved: <%s> "), rp->rl_ccrdata->r_name);

	// iterate through the state of the resource on all nodes
	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);

		rgm::lni_t i = lnNode->getLni();

		//
		// We can skip notifying physical nodes that are down; however,
		// it is possible for the RG to be stopping on a dead zone
		// if it contains resources that have Global_zone=TRUE (thus
		// which are executing their methods in the global zone).
		//
		if ((strcmp(ZONE, "global") == 0) && !lnNode->isPhysNodeUp()) {
			continue;
		}

		r_xstate_t r_state = rp->rl_xstate[i];

		//
		// If we come accross a resource that is STOPPING_DEPEND,
		// and is in nset, call the call_notify_dependencies_resolved
		// function to handle the idl to the slave.
		//
		// If the call completes successfully, a post-condition
		// is that the state of the resource on the slave has been
		// set to R_OK_TO_STOP.  So we go ahead and set that state
		// here on the president immediately so that we don't waste
		// time checking this dependency again (if there are other
		// state transitions queued before we would get the OK_TO_STOP
		// transition).
		//
		// Set the state by calling set_rx_state.  Depending on
		// the code path that got us here, we could be calling
		// it recursively.  It's ok because we've held the lock the
		// entire time.  Also, OK_TO_STOP is not an interesting state
		// for the dependency checking, so the recursion won't go
		// very far.
		//
		if (r_state.rx_state == rgm::R_STOPPING_DEPEND &&
		    nset.containLni(i)) {
			if (call_notify_dependencies_resolved(i,
			    rp->rl_ccrdata->r_name) == RGMIDL_OK) {
				set_rx_state(i, rp, rgm::R_OK_TO_STOP);
			}
		}
	}
}


//
// are_stopping_dependencies_resolved
// ----------------------------------
// When stopping, we wait for a resource that has a dependency on us only
// if that resource is "heading towards" the OFFLINE state.  If the resource
// is not going anywhere, or is "heading" ONLINE, we don't wait for it.
//
// This behavior is the same for all three types of dependencies (strong, weak,
// and restart).  Although the deps_kind parameter is not used, we leave it
// in case we want to change the behavior of one of the dependency types in
// the future.
//
// The rationale for not blocking indefinitely for stopping dependencies
// is that we don't want to impede a resource from going offline,
// because it is doing so either because there is something wrong, or because
// a user has manually requested this change.  In either case, we should
// allow the resource to come offline.  Forcing all dependent resources of
// a resource to come offline first would prevent quick failovers or restart,
// and would obviate the need for restart dependencies.
//
// To check stopping dependencies, iterate through each resource in the
// dep_list.  Ignore resources that are in the same RG as us.  We must wait
// (stopping dependencies are not resolved) if any of the following are true
// for any of the resources:
// * Containing RG is some form of OFFLINE or PENDING_OFFLINE and R is not
//   yet OFFLINE or STOP_FAILED.
// * R is STOPPING or POSTNET_STOPPING
// * R is disabled, and R is not yet OFFLINE or STOP_FAILED.
//
// Note that we don't wait if R is moving in the starting direction.  That is
// because there is no indication that the R will ever stop, so there's no
// point in waiting.  Similarly, if the R is restarting, it will most likely
// not end up stopped, so there is no point in waiting.
//
// Note on scalable services:  if a resource in the dep_list is scalable, and
// the dependency is non local-node, we wait until the above conditions are
// fulfilled for _all_ instance of the resource.  If the dependency is
// local-node or local-node-weak, we wait until it is fulfilled only on the
// local-node.
//
// If physical-node affinities are enabled, a local-node dependency applies
// to the dependent on any zone on the same physical node as the dependee.
//
// This function is passed a list of nodes on which we are checking the
// dependency.  The function removes nodes from the list if the dependency
// is not satisfied there (for local-node dependencies), but can still return
// true, as long as the dep is satisfied on one or more nodes.
//
static boolean_t
are_stopping_dependencies_resolved(rlist_p_t rp, rdeplist_t *deps_list,
    deptype_t deps_kind, LogicalNodeset &nset)
{
	rdeplist_t *curr_name = NULL;
	rlist_p_t rp_dep = NULL;
	rglist_p_t rgp = rp->rl_rg;
	LogicalNodeset unsatisfied_ns;
	boolean_t is_local_node, is_local_node_weak;
	rglist_p_t rgp_dep = NULL;
	boolean_t is_r_remote;

	for (curr_name = deps_list; curr_name != NULL;
	    curr_name = curr_name->rl_next) {
		is_r_remote = B_FALSE;
		if (is_enhanced_naming_format_used(curr_name->rl_name)) {
			is_r_remote = B_TRUE;
		}

		if (!is_r_remote) {
			rp_dep = rname_to_r(NULL, curr_name->rl_name);
			CL_PANIC(rp_dep != NULL);
			//
			// skip it if it's in the same RG.  Those dependencies
			// are checked by the slave in the state machine.
			//
			// Note: just compare rg pointers.
			//
			rgp_dep = rp_dep->rl_rg;
			if (rgp_dep == rgp) {
				continue;
			}
		} else {
#if SOL_VERSION >= __s10
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "check_starting_dependencies_from_list "
				    "Running at too low a version:R <%s> "
				    "has dependency on R <%s> "
				    "which is in not in local cluster\n"),
				    rp->rl_ccrdata->r_name, curr_name->rl_name);
				return (B_FALSE);
			}
			if (get_remote_r_state_helper(rp, curr_name->rl_name,
			    curr_name->locality_type, deps_kind, nset,
			    B_FALSE, NULL)) {
				continue;
			} else {
				return (B_FALSE);
			}
#endif
		}

		//
		// Figure out whether or not the dependency is local-node.
		// For stopping dependencies, we treat either local-node
		// or local-node-weak dependencies as local-node.
		//
		// Note that we check for affinities from the dependent
		// to the dependee.  In this case rp is the dependee.
		//
		check_dep_local(rp, rp_dep, curr_name->locality_type,
		    &is_local_node, &is_local_node_weak);


		//
		// Now loop through the state of the resource on all nodes,
		// building up a nodeset of all nodes on which the stopping
		// dependency is not yet satisfied.
		//

		for (LogicalNodeManager::iter it = lnManager->begin();
		    it != lnManager->end(); ++it) {
			LogicalNode *lnNode = lnManager->findObject(it->second);
			CL_PANIC(lnNode != NULL);

			rgm::lni_t node = lnNode->getLni();

			//
			// We do not have to check physical nodes which are
			// down.  However, we must not exclude non-global
			// zones even if they are down, because it is possible
			// for an RG (containing Global_zone=TRUE resources)
			// to be pending offline on such a zone.
			//
			if (!lnNode->isPhysNodeUp()) {
				continue;
			}

			rg_xstate_t rg_state = rgp_dep->rgl_xstate[node];
			r_xstate_t r_state = rp_dep->rl_xstate[node];

			//
			// First, check if r is disabled
			//
			// If so, we need to wait for it to go OFFLINE or
			// any offline equivalent state.
			// We don't care about the RG state.
			//
			if (((rgm_switch_t *)rp_dep->rl_ccrdata->r_onoff_switch)
			    ->r_switch[node] == SCHA_SWITCH_DISABLED) {
				if (!is_r_offline(r_state.rx_state)) {

					// The resource is disabled and
					// still heading offline on this
					// node, so we should wait for it.

					unsatisfied_ns.addLni(node);
				}
				// The resource is disabled and offline.
				// No need to wait for it on this node.
				continue;
			}

			//
			// Check if the dependent resource is in
			// the stopping phase
			// of restart. Add this node to the list of
			// unsatisfied nodesets.
			//

			rgm::r_restart_t rstart_flag = rgm::RR_NONE;
			if ((call_fetch_restarting_flag(node,
			    curr_name->rl_name, rstart_flag) != RGMIDL_OK)) {
				//
				// IDL error; we assume that the slave physical
				// node is down, therefore the stopping
				// dependency is satisfied on that node.
				//
				continue;
			}
			if (rstart_flag == rgm::RR_STOPPING &&
			    !is_r_offline(rp_dep->rl_xstate[node].rx_state)) {
				unsatisfied_ns.addLni(node);
				continue;
			}

			//
			// If the RG is offlining, we need to wait
			// for the resource to become offline.
			//
			if (is_rg_offlining(rg_state.rgx_state)) {
				if (!is_r_offline(r_state.rx_state)) {
					unsatisfied_ns.addLni(node);
				}

			// If the RG is not offline, but for some
			// reason the resource is stopping, wait
			// for it to stop.
			} else if (is_r_stopping(r_state.rx_state)) {
				unsatisfied_ns.addLni(node);
			}
		} // nodes loop

		//
		// If physical-node affinities are enabled, add to
		// unsatisfied_ns all zones that are on the same
		// physical node as a zone already in unsatisfied_ns.
		//
		add_phys_zones_to_nodeset(unsatisfied_ns);

		//
		// Now that we have a list of all the nodes on which the
		// dependency is not satisfied, figure out if the dependency
		// is satisfied on any node.
		//
		// For local node dependencies, the dependency need only be
		// satisfied on the local node.
		//
		if (is_local_node || is_local_node_weak) {
			nset -= unsatisfied_ns;
			if (nset.isEmpty()) {

				char *ucmm_buffer = NULL;
				if (unsatisfied_ns.display(ucmm_buffer) == 0) {
					ucmm_print("RGM", NOGET("DEPENDENCIES: "
					    "local-node stopping dependency of "
					    "<%s> on <%s> not yet resolved on "
					    "any node. Dependent moving offline"
					    " on nodes %s."),
					    rp->rl_ccrdata->r_name,
					    curr_name->rl_name, ucmm_buffer);
					free(ucmm_buffer);
				}
				return (B_FALSE);
			}
		} else if (!unsatisfied_ns.isEmpty()) {
			//
			// For non local-node deps, the dependency must
			// be satisfied on all nodes.
			//

			char *ucmm_buffer = NULL;
			if (unsatisfied_ns.display(ucmm_buffer) == 0) {

				ucmm_print("RGM", NOGET("DEPENDENCIES:"
				    " stopping dependency of <%s> on "
				    "<%s> not yet resolved. Dependent "
				    "moving offline on nodes %s\n."),
				    rp->rl_ccrdata->r_name, curr_name->rl_name,
				    ucmm_buffer);
				free(ucmm_buffer);
			}
			return (B_FALSE);
		}
	} // dependencies list loop

	ucmm_print("RGM", NOGET("DEPENDENCIES:"
	    " stopping dependencies of <%s> resolved for deptype <%d>.\n"),
	    rp->rl_ccrdata->r_name, deps_kind);

	return (B_TRUE);
}

#endif // !EUROPA_FARM

//
// is_r_offline
// ------------
// Returns B_TRUE if the resource is in any of a list
// of offline-equivalent states.
// Returns B_FALSE otherwise.
//
boolean_t
is_r_offline(rgm::rgm_r_state r_state)
{
	return ((boolean_t)(r_state == rgm::R_OFFLINE ||
	    r_state == rgm::R_PENDING_INIT ||
	    r_state == rgm::R_PENDING_FINI ||
	    r_state == rgm::R_UNINITED ||
	    r_state == rgm::R_PENDING_BOOT ||
	    r_state == rgm::R_STARTING_DEPEND ||
	    r_state == rgm::R_OK_TO_START ||
	    r_state == rgm::R_INITING ||
	    r_state == rgm::R_FINIING ||
	    r_state == rgm::R_BOOTING ||
	    r_state == rgm::R_STOP_FAILED));
}

boolean_t
is_switch_disabled(std::map<rgm_lni_t, scha_switch_t> &swtch,
    LogicalNodeset &ns)
{
	int i = 0;
	while ((i = ns.nextLniSet(i + 1)) != 0) {
		if (swtch[i] == SCHA_SWITCH_ENABLED)
			return (B_FALSE);
	}
	return (B_TRUE);
}
//
// is_r_stopping
// -------------
// Returns B_TRUE if the resource is actively stopping or waiting to stop.
// Does not include monitor stopping.
//
boolean_t
is_r_stopping(rgm::rgm_r_state r_state)
{
	return ((boolean_t)(r_state == rgm::R_STOPPING || r_state ==
	    rgm::R_STOPPED || r_state == rgm::R_POSTNET_STOPPING || r_state ==
	    rgm::R_STOPPING_DEPEND || r_state == rgm::R_OK_TO_STOP));
}

#ifndef EUROPA_FARM

//
// is_r_starting
// --------------
// Returns B_TRUE if the resource is actively starting or waiting to start.
// Does not include monitor starting.
//
boolean_t
is_r_starting(rgm::rgm_r_state r_state)
{
	return ((boolean_t)(r_state == rgm::R_PRENET_STARTED || r_state ==
	    rgm::R_PRENET_STARTING || r_state == rgm::R_STARTING || r_state ==
	    rgm::R_OK_TO_START));
}


//
// is_rg_offlining
// ---------------
// An RG is offlining if it is in any of the PENDING_OFFLINE states.
//
boolean_t
is_rg_offlining(rgm::rgm_rg_state rg_state)
{
	switch (rg_state) {
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_PENDING_OFF_START_FAILED:
		return (B_TRUE);
	case rgm::RG_OFFLINE:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_BOOT:
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_OFFLINE_START_FAILED:
	case rgm::RG_ONLINE:
	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_ON_PENDING_METHODS:
	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
	case rgm::RG_ON_PENDING_R_RESTART:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
		return (B_FALSE);
	default:
		// illegal state!
		CL_PANIC(B_FALSE);
	}
	return (B_FALSE);
}


//
// can_rg_have_restarting_r
// ------------------------
// Returns true if it's possible for a resource in the rg to be restarting,
// based on the RG state.
//
// A resource in a pending offline RG might have its restart flag set to
// RR_STOPPING but it is not actually going to restart unless the RG changes
// direction to pending online.  We do not count this as a "restarting"
// resource.
//
// A resource may be flagged as restarting in an ON_PENDING_METHODS RG; the
// restart will be postponed until after the pending init, fini or update
// methods have completed.  However we still count such a resource as
// restarting.
//
static boolean_t
can_rg_have_restarting_r(rgm::rgm_rg_state rg_state)
{
	switch (rg_state) {
	case rgm::RG_ON_PENDING_R_RESTART:
	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
	case rgm::RG_ON_PENDING_METHODS:
		return (B_TRUE);
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_PENDING_OFF_START_FAILED:
	case rgm::RG_OFFLINE:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_BOOT:
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_OFFLINE_START_FAILED:
	case rgm::RG_ONLINE:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
	case rgm::RG_ERROR_STOP_FAILED:
		return (B_FALSE);
	default:
		// illegal state!
		CL_PANIC(B_FALSE);
	}
	return (B_FALSE);
}

#endif

//
// is_r_online
// -----------
// A resource is online if it is "at least" ONLINE_UNMON.
//
// Do not count STOPPING_DEPEND and OK_TO_STOP as online, because
// we know it's about to head offline.  Note: verify that this won't cause
// possible deadlocks!!
//
boolean_t
is_r_online(rgm::rgm_r_state r_state)
{
	switch (r_state) {
	case rgm::R_ONLINE_UNMON:
	case rgm::R_MON_FAILED:
	case rgm::R_ONLINE:
	case rgm::R_ON_PENDING_UPDATE:
	case rgm::R_ONUNMON_PENDING_UPDATE:
	case rgm::R_MONFLD_PENDING_UPDATE:
	case rgm::R_UPDATING:
	case rgm::R_MON_STARTING:
	case rgm::R_MON_STOPPING:
	case rgm::R_JUST_STARTED:
		return (B_TRUE);
	case rgm::R_PRENET_STARTED:
	case rgm::R_STOPPED:
	case rgm::R_OFFLINE:
	case rgm::R_START_FAILED:
	case rgm::R_ONLINE_STANDBY:
	case rgm::R_STOP_FAILED:
	case rgm::R_PENDING_INIT:
	case rgm::R_PENDING_FINI:
	case rgm::R_UNINITED:
	case rgm::R_PENDING_BOOT:
	case rgm::R_INITING:
	case rgm::R_FINIING:
	case rgm::R_BOOTING:
	case rgm::R_POSTNET_STOPPING:
	case rgm::R_PRENET_STARTING:
	case rgm::R_STARTING:
	case rgm::R_STOPPING:
	case rgm::R_PENDING_UPDATE:
	case rgm::R_STOPPING_DEPEND:
	case rgm::R_STARTING_DEPEND:
	case rgm::R_OK_TO_START:
	case rgm::R_OK_TO_STOP:
		return (B_FALSE);
	default:
		// illegal state!
		CL_PANIC(0);
		break;
	}
	return (B_FALSE);
}

#ifndef EUROPA_FARM

//
// get_r_restarting
// ----------------
// Calls the helper function fetch_restarting_flag() to ask
// the slave whether or not the resource is restarting.  Before doing
// so, checks whether the RG is in one of the states which would allow
// a resource to restart.
//
// Assumes that the resource is enabled (should be checked prior to calling).
//
rgm::r_restart_t
get_r_restarting(name_t r_name, rgm::rgm_rg_state rg_state,
    rgm::lni_t lni)
{
	if (!can_rg_have_restarting_r(rg_state)) {
		return (rgm::RR_NONE);
	}
	rgm::r_restart_t restart_flag = rgm::RR_NONE;
	(void) call_fetch_restarting_flag(lni, r_name, restart_flag);
	return (restart_flag);
}


void
check_blocked_dependencies(rlist_p_t rp, boolean_t check_starting,
    boolean_t check_stopping)
{
	ucmm_print("RGM", NOGET("DEPENDENCIES: "
	    "check_blocked_dependencies: r=<%s>\n"), rp->rl_ccrdata->r_name);

	//
	// check whether this particular resource is in STOPPING_DEPEND or
	// STARTING_DEPEND on any nodes, depending on our flags
	// (check_stopping and check_starting).  If so, we need
	// to call check_stopping_dependencies or check_starting_dependencies
	// for the resource, passing a nodeset of the nodes on which the
	// resource is STARTING_DEPEND or STOPPING_DEPEND.
	//

	LogicalNodeset start_ns, stop_ns;

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);

		rgm::lni_t i = lnNode->getLni();

		//
		// For STARTING_DEPEND, we can skip nodes or zones that
		// are down or shutting down.
		// However, for STOPPING_DEPEND, we must not exclude non-global
		// zones even if they are down (if the physical node is up),
		// because it is possible for an RG containing
		// Global_zone=TRUE resources to be pending offline on such
		// a zone.
		//
		if (!lnNode->isPhysNodeUp()) {
			continue;
		}
		if (check_stopping && rp->rl_xstate[i].rx_state ==
		    rgm::R_STOPPING_DEPEND) {
			ucmm_print("RGM", NOGET("DEPENDENCIES: "
			    "check_blocked_dependencies: node <%d> has "
			    "R_STOPPING_DEPEND\n"), i);
			stop_ns.addLni(i);
		}
		if (check_starting && lnNode->getStatus() == rgm::LN_UP &&
		    rp->rl_xstate[i].rx_state == rgm::R_STARTING_DEPEND) {
			ucmm_print("RGM", NOGET("DEPENDENCIES: "
			    "check_blocked_dependencies: node <%d> has "
			    "R_STARTING_DEPEND\n"), i);
			start_ns.addLni(i);
		}
	}
	if (check_stopping && !stop_ns.isEmpty()) {
		check_stopping_dependencies(rp, stop_ns);
	}

	if (check_starting && !start_ns.isEmpty()) {
		check_starting_dependencies(rp, start_ns);
	}
}


//
// check_all_dependencies
// ----------------------
// Assumes the caller holds the lock.  Iterates through every resource in
// the system, calling check_blocked_dependencies and check_just_started
// on each one.  We specify that check_blocked_dependencies should check both
// starting and stopping dependencies.
//
// It's ok to call check_just_started (which can tell a resource to move
// to online_unmon, and possibly trigger restarts in dependent resources)
// because we hold the lock the entire time.  So the state changes on
// the slaves will not get added to our state until after we finish processing
// all resources and release the lock (except when the slave is the president,
// in which case the resources might move from JUST_STARTED to ONLINE_UNMON,
// which is fine for our purposes).
//
void
check_all_dependencies(void)
{
	rglist_p_t rgp;
	rlist_p_t rp;

	ucmm_print("RGM", NOGET("DEPENDENCIES: check_all_dependencies."));

	for (rgp = Rgm_state->rgm_rglist; rgp != NULL; rgp = rgp->rgl_next) {
		for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
			check_blocked_dependencies(rp, B_TRUE, B_TRUE);
			check_just_started(rp);
		}
	}
}


//
// check_just_started
// -------------------
// Iterates through all the nodes in the cluster, searching for all nodes
// on which the resource pointed to by rp is in the state R_JUST_STARTED.
// For each such node, call call_ack_start to tell the slave that it
// can move to R_ONLINE_UNMON.  Also call trigger_restart_dependencies to
// force all resources with restart dependencies on this one to restart.
//
// We ack the JUST_STARTED state separately on all nodes that are JUST_STARTED.
// However, we only call trigger_restart_dependencies at most once.
//
void
check_just_started(rlist_p_t rp)
{
	LogicalNodeset nset;

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);

		rgm::lni_t i = lnNode->getLni();

		if (!lnNode->isInCluster()) {
			continue;
		}

		if (rp->rl_xstate[i].rx_state == rgm::R_JUST_STARTED) {
			ucmm_print("RGM", NOGET("DEPENDENCIES: "
			    "found resource <%s> in JUST_STARTED on node "
			    "<%d>\n"), rp->rl_ccrdata->r_name, i);
			nset.addLni(i);
			(void) call_ack_start(i, rp->rl_ccrdata->r_name);
		}
	}
	if (!nset.isEmpty()) {
		trigger_restart_dependencies(rp, nset, B_FALSE);
	}
}


//
// is_dep_local_only
// -----------------
// resource rp_dep has a declared dependency on resource rp.  Return B_TRUE
// if the dependency applies on the local node only, otherwise return
// B_FALSE.
//
static boolean_t
is_dep_local_only(rlist_p_t rp, rlist_p_t rp_dep, boolean_t check_strong)
{
	rglist_p_t rgp = rp->rl_rg;
	rglist_p_t dep_rgp = rp_dep->rl_rg;

	// If rp and rp_dep are in the same RG, return B_TRUE
	if (rgp == dep_rgp) {
		return (B_TRUE);
	}

	//
	// If we're supposed to check strong affinities, and rp_dep's RG
	// declares a strong positive affinity for rp's RG, return B_TRUE.
	//
	if (check_strong &&
	    namelist_contains(dep_rgp->rgl_affinities.ap_strong_pos,
	    rgp->rgl_ccr->rg_name)) {
		return (B_TRUE);
	}

	//
	// If we're supposed to check weak affinities, and rp_dep's RG
	// declares a weak positive affinity for rp's RG, return B_TRUE.
	//
	if (!check_strong &&
	    namelist_contains(dep_rgp->rgl_affinities.ap_weak_pos,
	    rgp->rgl_ccr->rg_name)) {
		return (B_TRUE);
	}

	return (B_FALSE);
}
#endif
/*
 * is_ext_prop_updated.
 * The function searches the nameval_node sequence for the "key"
 */
boolean_t
is_ext_prop_updated(name_t key, const  rgm::idl_nameval_node_seq_t & nv_seq)
{
	uint_t len, i;
	len = nv_seq.length();
	for (i = 0; i < len; i++) {
		if (!strcasecmp(nv_seq[i].idlstr_nv_name, key)) {
			return (B_TRUE);
		}
	}
	return (B_FALSE);
}

//
// queries zonename from door cred structure, and verifies if nodelist
// contains that zone.
//
#if SOL_VERSION >= __s10
scha_err_t
authenticate_client_cred(char *rg_name)
{
	rglist_p_t	rglp = NULL;
	char client_zonename[ZONENAME_MAX];

	if (!get_doorclient_zonename(client_zonename)) {
		ucmm_print("authenticate_client_cred",
		    NOGET("Unable to get door client zonename"));
		    return (SCHA_ERR_INTERNAL);
	}

	// global zone is allowed all powers.
	if (strcmp(client_zonename, GLOBAL_ZONENAME) == 0)
		return (SCHA_ERR_NOERR);
	// if request from correct CZ, no need for further nodelist check.
	if (strcmp(client_zonename, ZONE) == 0) {
		return (SCHA_ERR_NOERR);
	}
	// client is not global zone nor does it belong to the appropriate CZ

	// if CZ, we can't expect native zones.
	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		return (SCHA_ERR_ACCESS);
	}

	// native zone, check for nodelist permissions.

	rgm_lock_state();

	rglp = rgname_to_rg(rg_name);
	if (rglp == NULL) {
		rgm_unlock_state();
		return (SCHA_ERR_RG);
	}
	if (nodeidlist_contains(rglp->rgl_ccr->rg_nodelist,
	    lnManager->computeLn(get_local_nodeid(), client_zonename))) {
		rgm_unlock_state();
		return (SCHA_ERR_NOERR);
	}
	rgm_unlock_state();
	return (SCHA_ERR_ACCESS);
}
#endif

#ifndef EUROPA_FARM
//
// trigger_restart_dependencies
// -----------------------------
// 1. For Restart dependencies:
// 	Preconditions: rp has just started or restarted on the node(s)
// 	in non-empty nodeset nset.
// 	Postconditions: we have ordered every resource that has
// 	restart dependencies on rp to restart on appropriate nodes.
//
// 	Searches through the rl_dependees list of restart dependencies on rp.
// 	If the dependent has a "local node only" restart dependency
// 	on rp, then restart it only on the node(s) in nset.
// 	Otherwise, ignore nset and restart the dependent on all slave
// 	nodes where it is in appropriate state.
// 2. For Offline restart dependencies:
// 	Preconditions: rp is about to stop/restart on the node(s)
// 	in non-empty nodeset nset; or rp has gone (or might have gone)
//	offline on the node(s) in nset, due to node death.
// 	Postconditions: we have ordered every resource that has
//	offline-restart dependencies on rp to restart on appropriate
//	nodes if the dependency is no longer satisfied.
// 3. Shared address resource.
//	Preconditions: rp is DISABLEd on node(s) in non-empty
//	nodeset nset.
//	Postconditions: we have ordered every resource that has restart/strong
//	dependencies on rp to restart on appropriate nodes.
// Note that when a shared address resource is DISABLED, the strong/restart/
// offline-restart dependents of the shared address resource will be restarted.
//
// 	The semantics of localnode/anynode described above will
// 	be followed for	offline-restart dependents also.
//
void
trigger_restart_dependencies(rlist_p_t rp, LogicalNodeset &nset,
    boolean_t is_off_restart)
{
	rdeplist_t	*curr_name = NULL;
	rlist_p_t	rp_dep = NULL;
	LogicalNodeset	*restart_nsp = NULL;
	LogicalNodeset	*ns_nodelistp = NULL;		// nodelist for rp
	boolean_t	is_rp_first_inst_started = B_TRUE;
	boolean_t	is_rp_last_inst_stopped = B_TRUE;
	boolean_t	is_local_node = B_FALSE;
	sc_syslog_msg_handle_t handle;
	boolean_t	is_local_node_weak = B_FALSE, is_shared_add = B_FALSE;
	rgm::lni_t	i;

	ns_nodelistp = get_logical_nodeset_from_nodelist(
	    rp->rl_rg->rgl_ccr->rg_nodelist);
	if (rp->rl_ccrtype->rt_sysdeftype ==
	    SYST_SHARED_ADDRESS && is_switch_disabled((
	    (rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->r_switch, nset)) {
		//
		// Shared address resource is to be brought OFFLINE.
		// Treat the strong/restart dependents as offline-restart
		// dependency and trigger the restarts of the same.
		//
		is_shared_add = B_TRUE;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RS_TAG, rp->rl_ccrdata->r_name);

		//
		// SCMSGS
		// @explanation
		// The resource being disabled is a
		// SHARED ADDRESS resource. The strong and
		// the restart dependents of this resource
		// will be brought offline. The dependents
		// will automatically be brought ONLINE once
		// this resource is re-enabled.
		// @user_action
		// None.This is just an informational message
		//

		(void) sc_syslog_msg_log(handle, LOG_INFO,
		    MESSAGE, SYSTEXT("Shared address resource"
		    " %s is DISABLED. Its strong/restart "
		    "dependents will be brought OFFLINE"),
		    rp->rl_ccrdata->r_name);
		sc_syslog_msg_done(&handle);
	}

	if (is_off_restart) {
		//
		// The offline-restart dependents of rp are to be restarted.
		//
		for (curr_name = rp->rl_dependents.dp_offline_restart;
		    curr_name != NULL;
		    curr_name = curr_name->rl_next) {
			if (is_enhanced_naming_format_used(curr_name->rl_name))
				continue;
			rp_dep = rname_to_r(NULL, curr_name->rl_name);
			CL_PANIC(rp_dep != NULL);
			//
			// If this is the last instance rp that is stopping,
			// and the locality of the dependency is ANY_NODE,
			// restart rp_dep on all nodes on which it's in a
			// restartable state. In case where the locality of
			// the dependency is LOCAL_NODE, restart rp_dep on
			// only nset.
			//
			check_dep_local(rp, rp_dep, curr_name->locality_type,
			    &is_local_node, &is_local_node_weak);
			if (is_local_node || is_local_node_weak) {
				//
				// The type of dependency is local_node.
				// Trigger restart of the dependent on that
				// node only.
				// If physical-node affinities are enabled,
				// add to *restart_nsp all zones that are on
				// the same physical node as any member of
				// *restart_nsp.
				//
				restart_nsp = new LogicalNodeset(nset);
				add_phys_zones_to_nodeset(*restart_nsp);
			} else {
				//
				// The dependency is any_node.  Trigger
				// restart of the dependent on all nodes in
				// its nodelist where it is online, but
				// only if the dependency is no longer
				// satisfied (i.e., the instance of rp that
				// has stopped or is about to stop is the last
				// online instance of rp).
				//
				i = 0;
				while ((i = ns_nodelistp->nextLniSet
				    (i + 1)) != 0) {
					if (!nset.containLni(i) &&
					    is_r_online(
					    rp->rl_xstate[i].rx_state)) {
						is_rp_last_inst_stopped =
						    B_FALSE;
						break;
					}
				}

				if (is_rp_last_inst_stopped) {
					// Rp is the last instance
					// being stopped.
					restart_nsp =
					    get_logical_nodeset_from_nodelist(
					    rp_dep->rl_rg->rgl_ccr->
					    rg_nodelist);
				}
			}
			if (restart_nsp != NULL) {
				LogicalNodeset	actual_off_restart;
				i = 0;
				while ((i =
				    restart_nsp->nextLniSet(i + 1)) != 0) {
					//
					// Even though ONLINE_STANDBY
					// doesn't "officially" count
					// as online, we need to
					// trigger the restart in that
					// state.
					//
					if (!is_r_online(rp_dep->rl_xstate[i].
					    rx_state) &&
					    !(rp_dep->rl_xstate[i].rx_state ==
					    rgm::R_ONLINE_STANDBY)) {
						continue;
					}
					actual_off_restart.addLni(i);
				}
				ucmm_print("RGM", NOGET("trigger_restart_deps: "
				    "trigger restart of <%s> because of "
				    "restart dep"
				    " on <%s>"), rp_dep->rl_ccrdata->r_name,
				    rp->rl_ccrdata->r_name);

				trigger_restart(rp_dep, actual_off_restart);
				delete (restart_nsp);
				restart_nsp = NULL;
			}
		}
		if (allow_inter_cluster()) {
			for (curr_name = rp->rl_ccrdata->
			    r_inter_cluster_dependents.dp_offline_restart;
			    curr_name != NULL;
			    curr_name = curr_name->rl_next) {
				if (!is_enhanced_naming_format_used(
				    curr_name->rl_name))
					continue;

#if SOL_VERSION >= __s10
				handle_inter_cluster_dependency_r(curr_name,
				    rp->rl_ccrdata->r_name,
				    DEPS_OFFLINE_RESTART,
				    rgm::RESTART_DEPENDENCIES_SATISFIED,
				    nset, rp, ns_nodelistp);
				continue;
#endif
			}
		}
	} else {

		//
		// Bug 5041013
		// If the resource is scalable mode (multi-mastered), and its
		// dependents are not 'local-dep' only, we only restart the
		// dependents when the first instance of the scalable resource
		// starts/restarts.
		//
		// Check here if this is the first instance which
		// starts/restarts
		//
		if (rp->rl_rg->rgl_ccr->rg_max_primaries > 1) {
			i = 0;
			while ((i = ns_nodelistp->nextLniSet(i + 1)) != 0) {
				if (!nset.containLni(i) &&
				    is_r_online(
				    rp->rl_xstate[i].rx_state)) {
					is_rp_first_inst_started = B_FALSE;
					break;
				}
			}
		}
		restart_deps(rp, rp->rl_dependents.dp_restart,
		    is_rp_first_inst_started, nset);
		// Restart inter-cluster resource dependents.
		if (allow_inter_cluster())
			restart_deps(rp, rp->rl_ccrdata->
			    r_inter_cluster_dependents.dp_restart,
			    is_rp_first_inst_started, nset);
	}

	if (is_shared_add) {
		//
		// When is_shared_add is TRUE, is_off_restart is also TRUE
		// we've already executed the code to restart the
		// offline-restart dependencies.
		//
		restart_deps(rp, rp->rl_dependents.dp_restart,
		    B_TRUE, nset);

		restart_deps(rp, rp->rl_dependents.dp_strong,
		    B_TRUE, nset);

		// Restart inter-cluster resource dependents.
		if (allow_inter_cluster()) {
			restart_deps(rp, rp->rl_ccrdata->
			    r_inter_cluster_dependents.dp_restart,
			    B_TRUE, nset);

			restart_deps(rp, rp->rl_ccrdata->
			    r_inter_cluster_dependents.dp_strong,
			    B_TRUE, nset);
		}
	}
	delete (ns_nodelistp);
}

//
// iterate through all the resources in the deps list and restart
// each of the dependents based on the locality types.
//
void
restart_deps(rlist_p_t rp, rdeplist_t *deps, boolean_t restart_rgnodelist,
    LogicalNodeset &nset)
{
	rdeplist_t *curr_name = NULL;
	rlist_p_t rp_dep = NULL;
	boolean_t is_local_node_weak = B_FALSE, is_local_node = B_FALSE;
	LogicalNodeset	*restart_nsp = NULL;

	//
	// iterate through all the resources in the deps dependents
	// list.
	//
	for (curr_name = deps; curr_name != NULL;
	    curr_name = curr_name->rl_next) {
#if SOL_VERSION >= __s10
		if (is_enhanced_naming_format_used(curr_name->rl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "check_starting_dependencies_from_list "
				    "Running at too low a version:R <%s> "
				    "has dependency on R <%s> "
				    "which is in not in local cluster\n"),
				    rp->rl_ccrdata->r_name, curr_name->rl_name);
				continue;
			}
			handle_inter_cluster_dependency_r(curr_name,
			    rp->rl_ccrdata->r_name, DEPS_RESTART,
			    rgm::RESTART_DEPENDENCIES_SATISFIED, nset, NULL,
			    NULL);
			return;
		}
#endif
		rp_dep = rname_to_r(NULL, curr_name->rl_name);
		CL_PANIC(rp_dep != NULL);
		if (rp_dep->rl_rg->rgl_ccr->rg_suspended) {
			ucmm_print("RGM", NOGET("DEPENDENCIES: "
			"skipping trigger restart of <%s> "
			"by dep on <%s>, as the containing "
			"resource group is suspended\n"),
			rp_dep->rl_ccrdata->r_name,
			rp->rl_ccrdata->r_name);
			continue;
		}

		//
		// If the dependency is local-node, then restart rp_dep
		// on all node(s) in nset on which it's in a restartable state.
		// If physical-node affinities are used, then we do the
		// restart on all zones that are on the same physical node as
		// a member of nset.
		//
		// Otherwise, if this is the first instance started of
		// rp, then restart rp_dep on all nodes on which it's
		// in a restartable state.
		//
		// Otherwise (rp is not the first instance started and
		// rp_dep is not in the same group and there is no
		// positive affinity) do nothing.
		//
		(void) check_dep_local(rp, rp_dep,
			curr_name->locality_type, &is_local_node,
			&is_local_node_weak);
		if (is_local_node || is_local_node_weak) {
			restart_nsp = new LogicalNodeset(nset);
			add_phys_zones_to_nodeset(*restart_nsp);
		} else if (restart_rgnodelist) {
			restart_nsp = get_logical_nodeset_from_nodelist(
				rp_dep->rl_rg->rgl_ccr->rg_nodelist);
		}
		if (restart_nsp != NULL) {
			// we may also like to print nodeset later.
			ucmm_print("RGM", NOGET("trigger_restart_deps: "
				"trigger restart of <%s> by dep on <%s>"),
				rp_dep->rl_ccrdata->r_name,
				rp->rl_ccrdata->r_name);

			trigger_restart(rp_dep, *restart_nsp);
			delete (restart_nsp);
			restart_nsp = NULL;
		}
	}
}


//
// trigger_restart
// ---------------
// This function should be called for a resource if one of the resource's
// dependees has just come online.  We tell all slaves in the nodeset nset
// to restart this resource.  The slave will restart the resource if it is
// in a restartable state.
// This function is also called from idl_scha_control_restart. The restart of
// offline-restart intra-rg dependents of the resource (whose restart was
// triggered by scha_control RESOURCE_RESTART) is triggered.
//
// This is called by the president only, and not by farm nodes.
//
void
trigger_restart(rlist_p_t rp, LogicalNodeset &nset)
{
	char *ucmm_buffer = NULL;
	if (nset.display(ucmm_buffer) == 0) {
		ucmm_print("RGM",
		    NOGET("trigger_restart of <%s> on nodeset %s"),
		    rp->rl_ccrdata->r_name, ucmm_buffer);
		free(ucmm_buffer);
	}

	// iterate through all nodes in nset
	rgm::lni_t i = 0;
	while ((i = nset.nextLniSet(i + 1)) != 0) {
		//
		// If the resource is disabled on this node, we skip it.
		//
		if (((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->
		    r_switch[i] == SCHA_SWITCH_DISABLED) {
			continue;
		}

		//
		// If the RG is currently being updated, yield the lock and
		// yield to the updating thread before triggering the restart
		// on the remote node.  This gives the updating thread a
		// chance to initiate Update methods on the slave before this
		// restart is triggered.
		//
		if (rp->rl_rg->rgl_switching == SW_UPDATING) {
			rgm_unlock_state();
			thr_yield();
			rgm_lock_state();
		}

		//
		// Call the helper function to tell the slave to restart the
		// resource.  The slave checks the RG state and resource state
		// to make sure the restart is actually required.
		//
		(void) rgm_resource_restart(i, rgm::RGM_RRF_TR,
		    rp->rl_ccrdata->r_name);
	}
}


//
// node_is_evacuating
//
// Return B_TRUE if the resource group rgp is currently evacuating from
// node nodeid, or if node nodeid is currently subject to an unexpired
// "sticky" evacuation that prevents RGs from failing over onto the node.
//
boolean_t
node_is_evacuating(const rgm::lni_t lni, const rglist_p_t rgp)
{
	const LogicalNode *lnNode = lnManager->findObject(lni);

	// Is rgp currently evacuating from lni?
	if (rgp->rgl_evac_nodes.containLni(lni)) {
		return (B_TRUE);
	}

	// Is nodeid currently subject to a sticky node evacuation?
	return (lnNode->isEvacuating() ? B_TRUE : B_FALSE);
}


//
// any_res_starting_depend
// -----------------------
// Iterates through all the resources that belong to the specified RG.
// Returns B_TRUE if any of the resources are in STARTING_DEPEND on the
// specified node. Otherwise returns B_FALSE.
//
boolean_t
any_res_starting_depend(const rglist_p_t rgp, sol::nodeid_t nodeid)
{
	rlist_p_t rp;

	ucmm_print("RGM", NOGET("any_res_starting_depend for RG %s\n"),
	    rgp->rgl_ccr->rg_name);

	for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		if (rp->rl_xstate[nodeid].rx_state ==
		    rgm::R_STARTING_DEPEND) {
			ucmm_print("RGM", NOGET("Found resource %s "
			    "in STARTING_DEPEND on node %d\n"),
			    rp->rl_ccrdata->r_name, nodeid);
			return (B_TRUE);
		}
	}
	ucmm_print("RGM", NOGET("No resources in STARTING_DEPEND\n"));
	return (B_FALSE);
}
#endif // !EUROPA_FARM


//
// get_deepest_delegate
//
// when a RG delegates its failover request, find who to delegate the request
// to. This is to handle the case where RG1 +++> RG2 +++> RG3, where RG1 is
// the requesting RG for the failover request, but the giveover is delegated
// down to RG3.
//
// If lni is non-zero, it specifies the LNI on which RG1 is currently online
// and from which it is requesting the giveover.  In this case, the
// out-parameter toRemove (allocated by the caller) is set to contain
// the set of zones from which the delegate needs to execute a giveover;
// and the out-parameter delegate_r is set to point to a resource
// within the delegate RG.  The caller must not modify the memory pointed
// to by *delegate_r, since it is part of the rgmd's in-memory state.
//
// Edge case: If the delegate RG contains no resources, *delegate_r is set
// to NULL.
//
// If lni is zero, then toRemove and delegate_r are ignored, and may be NULL.
//
// If physical node affinities are enabled, RG3 might be online
// on different base cluster zone(s) (in the same physical node) than RG1.
// If RG3 is online on more than one zone on the physical node
// (which is an edge case and would not normally occur), the giveover will
// take RG3 offline from all of those zones.
//
rglist_p_t
get_deepest_delegate(const char *requesting_rg, rgm::lni_t lni,
    LogicalNodeset *toRemove, rlist_p_t *delegate_r)
{
	rglist_p_t rgp;
	namelist_t *delegate = NULL;

	rgp = rgname_to_rg(requesting_rg);

	while (rgp->rgl_affinities.ap_failover_delegation != NULL) {
		delegate = rgp->rgl_affinities.ap_failover_delegation;
		if (is_enhanced_naming_format_used(delegate->nl_name)) {
			continue;
		}
		rgp = rgname_to_rg(delegate->nl_name);
	}
	if (rgp == NULL) {
		return (NULL);
	}

	if (lni) {
		if (use_logicalnode_affinities()) {
			toRemove->reset();
			toRemove->addLni(lni);
		} else {
			LogicalNodeset zset;

			// Get set of zones on the physical node
			get_all_zones_on_phys_node(lni, zset);

			// Get online nodeset of rgp
			get_rg_online_nodeset(rgp, toRemove);

			//
			// Use set intersection to subtract out zones not on the
			// physical node from the online nodeset
			//
			*toRemove &= zset;
		}

		//
		// If there is a delegate RG, set *delegate_r to point to
		// a resource in the delegate RG.  With physical node
		// affinities, the delegate RG might be failing over onto
		// different zone(s) than the requestor RG.  Even if the
		// delegate runs in the same zone as the requestor, it might
		// well be the case that the requestor's monitor_check method
		// cannot succeed until *after* the delegate has failed over.
		// Therefore, we only attempt to do checks on the delegate.
		//
		// For performing pingpong checks and monitor_check callbacks,
		// the delegate_r resource will act as a stand-in for the
		// requesting resource.
		//
		// We use the first resource in the delegate RG's resource list,
		// which is the last resource added to that RG.  The rationale
		// is that the last-created resource is more likely to be
		// an "interesting" (dependent, or application-level) resource.
		// However, the choice of delegate resource is not critical.
		// Regardless of which resource we pick, monitor_check() will
		// invoke the monitor_check method on all resources in the RG
		// that have that method declared.
		//
		// Note: If the delegate RG has no resources, *delegate_r
		// will be set to NULL.
		//
		if (delegate_r) {
			*delegate_r = rgp->rgl_resources;
		}
	}
	return (rgp);
}


#ifndef EUROPA_FARM
//
// get_rg_online_nodeset
// This is to get the set of nodes in which the RGs are online.
// This function can only be called on the president node.
// and the caller must hold RGM state lock prior calling this.
// The online_nodes argument points to a LogicalNodeset which has
// been allocated by the caller.  This should not be NULL.
//
void
get_rg_online_nodeset(rglist_p_t rg_ptr, LogicalNodeset *online_nodes)
{
	CL_PANIC(online_nodes);
	online_nodes->reset();

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);

		rgm::lni_t i = lnNode->getLni();

		if (!lnNode->isInCluster()) {
			continue;
		}

		if (is_rg_online_on_node(rg_ptr, i, B_TRUE)) {
			online_nodes->addLni(i);
		}
	}
}
#endif // !EUROPA_FARM


//
// compute_NRUlst_frm_deplist()
//
// Compute the NRU list from the dependency lists (STRONG/WEAK/RESTART/
// OFFLINE-RESTART). This is a cloned version of compute_NRUlist_frm_deplist
// from librgm. The differences are:
//   - this function uses in-memory structures for computation of NRU
//   - this function takes an additional Boolean argument SA_only.
//	If SA_only is true, then return only a list of SharedAddress dependees.
//	Otherwise the returned list contains all network dependees.
//
// The callers of this function do not check for errors.  The only
// possible error is NOMEM; as is sometimes done elsewhere in librgmserver,
// we abort on that error although we could have tried to return it
// to the caller, in some code paths.
//
// The caller should have initialized *nru_list to NULL.  It remains NULL if
// there are no network dependees.  The caller is responsible for freeing
// *nru_list.
//
void
compute_NRUlst_frm_deplist(rgm_dependencies_t *r_dep_list_ptr,
    namelist_t **nru_list, boolean_t SA_only)
{
	int i;
	rdeplist_t *nlp = NULL;
	scha_errmsg_t scha_err = {SCHA_ERR_NOERR, NULL};
	rlist_p_t rp;
	// traverse the four dependency lists
	for (i = DEPS_WEAK; i <= DEPS_OFFLINE_RESTART; i++) {
		switch (i) {
		case DEPS_STRONG:
			nlp = r_dep_list_ptr->dp_strong;
			break;
		case DEPS_WEAK:
			nlp = r_dep_list_ptr->dp_weak;
			break;
		case DEPS_RESTART:
			nlp = r_dep_list_ptr->dp_restart;
			break;
		case DEPS_OFFLINE_RESTART:
			nlp = r_dep_list_ptr->dp_offline_restart;
			break;
		default:
			break;
		}

		// Traverse through each dependency list
		for (; nlp != NULL; nlp = nlp->rl_next) {
			if (is_enhanced_naming_format_used(nlp->rl_name))
				continue;
			rp = rname_to_r(NULL, nlp->rl_name);
			// Check if resource is network resource.
			// If yes, then add this to NRU list.
			if (rp == NULL) {
				// Should never happen
				continue;
			}
			if (((!SA_only && rp->rl_ccrtype->rt_sysdeftype ==
			    SYST_LOGICAL_HOSTNAME) ||
			    (rp->rl_ccrtype->rt_sysdeftype ==
			    SYST_SHARED_ADDRESS)) && (!namelist_contains(
			    *nru_list, nlp->rl_name))) {
				scha_err = namelist_add_name(nru_list,
				    nlp->rl_name);
				//
				// namelist_add_name() uses malloc_nocheck.
				// The only possible error is NOMEM.
				//
				if (scha_err.err_code == SCHA_ERR_NOMEM) {
					rgm_fatal_reboot(
					    "compute_NRUlst_frm_deplist",
					    B_TRUE, B_FALSE, NULL);
				}
			}
		}
	}
}

//
// run_boot_meths_on_logical_node
//
// Expects the caller to hold the lock
//
// Looks at all managed RGs, checks for the BOOT states, and sets the state of
// the RG and the Resources within it accordingly. Runs the state machine
// only if some boot method needs to be run, else it depends on the President
// to process needy RGs when the President fetches this slave's state.
//
// Calls the subroutine check_boot_meths_on_rg() to process each rg.
//
void
run_boot_meths_on_logical_node(rgm::lni_t lni)
{
	boolean_t need_to_run_state_machine = B_FALSE;
	rglist_t *rgp;
	sc_syslog_msg_handle_t handle;

	ucmm_print("run_boot_meths_on_logical_node", "lni %d\n", lni);

	for (rgp = Rgm_state->rgm_rglist; rgp != NULL;
	    rgp = rgp->rgl_next) {
		if (rgp->rgl_ccr->rg_unmanaged)
			// skip unmanaged RG
			continue;

		rgm::rgm_rg_state rgx_state = rgp->rgl_xstate[lni].rgx_state;

		//
		// If the RG is not offline on lni, this implies that lni
		// is a non-global zone, and there are resources with
		// Global_zone=TRUE which are still running stopping methods
		// in the global zone; or which encountered stop method
		// failure in the global zone.  In this case, we
		// set the rgx_postponed_boot flag and do not change the
		// RG state.  When the RG finally goes offline on the zone,
		// we will call check_boot_meths_on_rg() from set_rgx_state()
		// to run the postponed Boot methods.
		//
		if (rgx_state != rgm::RG_OFFLINE) {
			LogicalNode *lnNode = lnManager->findObject(lni);
			CL_PANIC(lnNode != NULL);
			//
			// To make sure that our assumptions are correct,
			// assert that lni is a non-global or ZC zone, and the
			// RG is in a stopping or stop_failed state on lni.
			//
			ASSERT(!lnNode->isGlobalZone() &&
			    (rgx_state == rgm::RG_PENDING_OFFLINE ||
			    rgx_state == rgm::RG_PENDING_OFF_STOP_FAILED ||
			    rgx_state == rgm::RG_ERROR_STOP_FAILED));

			rgp->rgl_xstate[lni].rgx_postponed_boot = B_TRUE;
			continue;
		}

		//
		// If this logical node needs to run the BOOT method on any
		// resource in the RG as implied by the RT's Init_nodes
		// property, set the resource state to
		// rgm::R_PENDING_BOOT and set the RG state to
		// rgm::RG_OFF_PENDING_BOOT on this node.
		//
		// Otherwise, set the RG state to OFF_BOOTED.
		//
		if (check_boot_meths_on_rg(rgp, lni)) {
			need_to_run_state_machine = B_TRUE;
		} else {
			set_rgx_state(lni, rgp, rgm::RG_OFF_BOOTED);
		}
	}

	if (need_to_run_state_machine) {
		if (rgm_run_state(lni).err_code != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd state machine has encountered an error on
			// this node.
			// @user_action
			// Look for preceding syslog messages on the same
			// node, which may provide a description of the error.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "ERROR: rgm_run_state() returned non-zero "
			    "while running boot methods");
			sc_syslog_msg_done(&handle);
		}
	}
}


//
// check_boot_meths_on_rg
//
// Called on slave side, while holding the rgm state lock.
// Caller has determined that Boot methods should be executed for resource
// group rglp on logical node lni.
//
// Check each resource in rglp to see if the resource needs to run its Boot
// method on lni, as implied by its RT's Init_nodes property.
// If so, set the resource state to R_PENDING_BOOT.
// If any resource was set to R_PENDING_BOOT, set RG state to
// RG_OFF_PENDING_BOOT and return true; otherwise, return false.
//
// The caller must run the state machine if we return true.
//
bool
check_boot_meths_on_rg(rglist_t *rglp, rgm::lni_t lni)
{
	rlist_t *rl_ptr = NULL;
	bool retval = false;
	LogicalNodeset *nl_nset =
	    get_logical_nodeset_from_nodelist(rglp->rgl_ccr->rg_nodelist);

	for (rl_ptr = rglp->rgl_resources; rl_ptr != NULL;
	    rl_ptr = rl_ptr->rl_next) {
		//
		// If the Boot method is not registered and the resource is
		// not a scalable service, skip this resource.
		//
		rgm_rt_t *rt = rl_ptr->rl_ccrtype;
		if (!is_method_reg(rt, METH_BOOT, NULL) &&
		    !is_r_scalable(rl_ptr->rl_ccrdata)) {
			continue;
		}

		//
		// If Init_nodes = RG_PRIMARIES, look for lni in rg nodelist;
		// otherwise, look for it in RT installed_nodes.
		// If lni is not in the list, skip this resource.
		//
		if (rt->rt_init_nodes == SCHA_INFLAG_RG_PRIMARIES) {
			if (!nl_nset->containLni(lni))
				continue;
		} else if (!rt->rt_instl_nodes.is_ALL_value &&
		    !nodeidlist_contains_lni(rt->rt_instl_nodes.nodeids, lni)) {
			continue;
		}

		// We are running Boot on this resource
		retval = true;
		set_rx_state(lni, rl_ptr, rgm::R_PENDING_BOOT);
	}
	if (retval) {
		set_rgx_state(lni, rglp, rgm::RG_OFF_PENDING_BOOT);
	}
	delete(nl_nset);
	return (retval);
}


//
// Convert a per_node extension property to a string
// Per node extension properties are stored in the CCR as
// though they were a separate property.
// <prop_name>{<nodeid1>} = <value1>;<propname>{<nodeid2>}=<value2>;..
//
void
pnext2str(std::map<rgm::lni_t, char *> &ptr, name_t key, char **res)
{
	char  *temp = NULL;
	uint_t		strsize = 0;

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		if (ptr[it->second] != NULL) {
			var_strcat(res,
			    "%s{%s}=%s"RGM_PROP_SEPARATOR_STR,
			    key, (it->first).data(), ptr[it->second]);
		}
	}
}

//
// The CCR will contain only those nodes (in the form of "nodeid:zonename"), for
// which the switch is enabled. This list will only include such nodes which
// are in the nodelist of the the containing resourcegroup.
// FORMAT: On_Off_switch=ln1,ln2,ln3,...,lnN;
// assumption: called from RGMD, with overloaded malloc/calloc/etc
// if we ensure that the map will only contain enabled elements from rg nodelist
// then we could have just iterated over the map instead of using rg nodelist.
//
char *
enum2str(std::map<rgm::lni_t, scha_switch_t> &l)
{

	char *ptr;
	uint_t size = 1;
	LogicalNode *lnNode;
	char *nodename;

	ptr = (char *)malloc(sizeof (char));
	ptr[0] = '\0';

	std::map<rgm::lni_t, scha_switch_t>::iterator it;
	for (it = l.begin(); it != l.end(); ++it) {
		if (it->second ==  SCHA_SWITCH_DISABLED)
			continue;
		lnNode = lnManager->findObject(it->first);
		// do we have a better optimised way??
		//  write another utility into objectManager??
		nodename = lnManager->computeLn(lnNode->getNodeid(),
		    lnNode->getZonename());
		size += strlen(nodename) + 1; 	// 1 for comma
		ptr = (char *)realloc(ptr, size);
		strcat(ptr, nodename);
		free(nodename);
		strcat(ptr, ",");
	}

	if (size > 1) {
		ptr[size-2] = '\0'; // last comma replaced with semicolon
	}
	return (ptr);
}

//
// set_booting_state
// Called only on president node, for RGs on a newly joining zone
//
void
set_booting_state(rgm::lni_t lni)
{
	sol::nodeid_t   mynodeid = get_local_nodeid();
	rglist_p_t new_rg = NULL;
	LogicalNode *lnNode = lnManager->findObject(lni);

	ucmm_print("RGM", "set_booting_state for lni %d\n", lni);
	if (lnNode->getNodeid() == mynodeid) {
		ucmm_print("RGM", "set_booting_state called for myself. "
		    "ignoring.\n");
		return;
	}
	//
	// The state will get pushed to us, so we just set our local
	// notion of the state to OFF_PENDING_BOOT
	// (see comments below).
	//
	// Don't fetch state from the slave. That
	// leads to synchronization issues.  Instead,
	// if we're not the slave to whom we're
	// talking, set our idea of the slave RG state
	// to OFF_PENDING_BOOT for all RGs.
	// This state will change to OFF_BOOTED when
	// the slave finishes running BOOT, and
	// notifies us of that fact.
	//
	for (new_rg = Rgm_state->rgm_rglist; new_rg != NULL;
	    new_rg = new_rg->rgl_next) {
		if (new_rg->rgl_ccr->rg_unmanaged) {
			continue;
		}
		rgm::rgm_rg_state rgx_state =
		    new_rg->rgl_xstate[lni].rgx_state;

		//
		// If the zone is non-global, and the physical node of the
		// zone is up, and the RG state on the zone is PENDING_OFFLINE
		// or PENDING_OFF_STOP_FAILED or ERROR_STOP_FAILED; then
		// the RG has been designated for "postponed boot" on the
		// slave side.  In this case, we do not change the RG state
		// in this function.  The slave will launch boot methods for
		// the zone after the RG finally moves to OFFLINE state.
		// (This is handled in set_rgx_state() on the slave side).
		// At that time, the slave will notify the president to move
		// the RG state to OFF_PENDING_BOOT.
		//
		// This also applies to ZC nodes.
		//
		if (!lnNode->isGlobalZone() &&
		    lnNode->isPhysNodeUp() &&
		    (rgx_state == rgm::RG_PENDING_OFFLINE ||
		    rgx_state == rgm::RG_PENDING_OFF_STOP_FAILED ||
		    rgx_state == rgm::RG_ERROR_STOP_FAILED)) {
			continue;
		}

		//
		// Note: we must set the state of all RGs to
		// RG_OFF_PENDING_BOOT, not RG_OFF_BOOTED. If we
		// set an RG to OFF_BOOTED, it will immediately
		// change to OFFLINE, and change_mastery will be called
		// on the slave. However, the slave may not have entered
		// this lni into its ln manager yet.
		//
		set_rgx_state(lni, new_rg, rgm::RG_OFF_PENDING_BOOT);
	}
}

#ifndef EUROPA_FARM
//
// Look up the CCR directory in the local name server. Once looked up, the
// reference is cached. This function should never return a NULL pointer.
//
static ccr::directory_var
lookup_ccrdir()
{
	static ccr::directory_var ccr_directory_var = ccr::directory::_nil();
	static os::mutex_t ccr_mutex;

	if (CORBA::is_nil(ccr_directory_var)) {

		ccr_mutex.lock();

		//
		// With the lock held, check again if the pointer is NULL.
		// If it is still NULL, look up the reference from the name
		// server. If not, just return the pointer.
		//
		if (!CORBA::is_nil(ccr_directory_var)) {
			ccr_mutex.unlock();
			return (ccr_directory_var);
		}

		naming::naming_context_var ctxp = ns::local_nameserver();
		Environment e;
		CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
		if (e.exception()) {
			sc_syslog_msg_handle_t handle;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("fatal: Unable to open CCR"));
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHABLE
		} else {
			ccr_directory_var = ccr::directory::_narrow(obj);
			CL_PANIC(!CORBA::is_nil(ccr_directory_var));
		}

		ccr_mutex.unlock();
	}

	return (ccr_directory_var);
}

//
// rgmcnfg_set_onoffswitch -
// Set on_off_switch flag to ON/OFF for given R on the specified set of nodes.
// If ns is empty, then set the flag on all nodes.
//
scha_errmsg_t
rgmcnfg_set_onoffswitch(name_t rname, name_t rgname, scha_switch_t swtch,
    LogicalNodeset *ns)
{
	scha_errmsg_t res;

	res = set_resource_switch(rname, rgname, SCHA_ON_OFF_SWITCH,
	    swtch, ns);

	FAULTPT_RGM(FAULTNUM_RGM_AFTER_RGMCNFG_SET_ONOFFSWITCH,
		FaultFunctions::generic);

	return (res);
}

//
// rgmcnfg_set_monitoredswitch -
// Set monitor_switch flag to ON/OFF for given R on the specified set of nodes.
// If ns is empty, then set the flag on all nodes.
//
scha_errmsg_t
rgmcnfg_set_monitoredswitch(name_t rname, name_t rgname, scha_switch_t swtch,
    LogicalNodeset *ns)
{
	scha_errmsg_t res;

	res = set_resource_switch(rname, rgname, SCHA_MONITORED_SWITCH,
	    swtch, ns);

	FAULTPT_RGM(FAULTNUM_RGM_AFTER_RGMCNFG_SET_MONITOREDSWITCH,
		FaultFunctions::generic);

	return (res);
}

//
// set_resource_switch -
//	set the specified switch(on_off_switch or monitor_switch)
//	flag for the given R on the specified set of nodes.
//
static scha_errmsg_t
set_resource_switch(name_t rname, name_t rgname, char *rswitch,
	scha_switch_t swtch, LogicalNodeset *ns)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rgm_resource_t *resource;
	rgm::lni_t lni = LNI_UNDEF;
	rglist_p_t rgp;
	int modified = 0;
	char	mod_key[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RS)];

	if (rname == NULL || rgname == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	//
	// Note: we use the no_defaults form of rgmcnfg_get_resource,
	// because we are only changing the r_onoff_switch or
	// r_monitored_switch, then turning around and saving the
	// resource back to the CCR. We do not want to fill in the
	// RT defaults, because we do not want to save them to the CCR.
	//
	rgp = rgname_to_rg(rgname);
	res = rgmcnfg_get_resource_no_defaults(rname, rgname, &resource, ZONE);
	if (res.err_code != SCHA_ERR_NOERR)
		return (res);
	update_onoff_monitored_switch(resource, rgp->rgl_ccr->rg_nodelist);

	update_pn_default_value(resource->r_ext_properties,
	    rgp->rgl_ccr->rg_nodelist);
	if (strcmp(rswitch, SCHA_ON_OFF_SWITCH) ==  0) {
		lni = 0;
		while ((lni = ns->nextLniSet(lni + 1)) != 0) {
			if (((rgm_switch_t *)resource->r_onoff_switch)->
			    r_switch[lni] != swtch) {
				((rgm_switch_t *)resource->r_onoff_switch)->
				    r_switch[lni] = swtch;
				modified = 1;
			}
		}
	} else if (strcmp(rswitch, SCHA_MONITORED_SWITCH) ==  0) {
		lni = 0;
		while ((lni = ns->nextLniSet(lni + 1)) != 0) {
			if (((rgm_switch_t *)resource->r_monitored_switch)->
			    r_switch[lni] != swtch) {
				((rgm_switch_t *)resource->r_monitored_switch)->
				    r_switch[lni] = swtch;
				modified = 1;
			}
		}
	}

	if (modified == 1) {
		char *s = rsrc2str(resource);
		// Note that we prefix the string RGM_PREFIX_RS to the resource
		// name before storing it in the CCR. Hence, we need to prefix
		// it here too before updating it back in the CCR.
		(void) sprintf(mod_key, "%s%s", RGM_PREFIX_RS, rname);
		res = update_property(rgname, O_RG, mod_key, s,
		    (const char*)ZONE);
		rgm_free_resource(resource);
		free(s);
		return (res);
	}

	rgm_free_resource(resource);
	res.err_code = SCHA_ERR_NOERR;
	return (res);
}
#endif // !EUROPA_FARM

/*
 * The function populates the <lni_t, name_t> map of rp_value)->rp_value.
 * Internal to rgm, the rp_value is accessed using the lni map.
 * External functions populates/access the rp_value_nname map.
 * RT defaults are stored in the "0"th index of the rp_value_nname map.
 */
void
update_pn_default_value(rgm_property_list_t *ptr, nodeidlist_t *ndl)
{
	rgm_property_list_t *p;
	rgm::lni_t lni;
	rgm_property_t *q;
	nodeidlist_t *nl = NULL;
	LogicalNodeset *rg_ns = NULL;

	rg_ns = get_logical_nodeset_from_nodelist(ndl);
	for (p = ptr; p; p = p->rpl_next) {
		q = p->rpl_property;
		if (!q->is_per_node)
			continue;
		std::map<std::string, name_t>::iterator it;
		for (it = ((rgm_value_t *)q->rp_value)
		    ->rp_value_node.begin(); it !=
		    ((rgm_value_t *)q->rp_value)->rp_value_node.end();
		    it++) {
			if (it->second != NULL) {
				lni = rgm_get_lni(it->first.data());
				if (lni != LNI_UNDEF) {
					if (!rg_ns->containLni(lni))
						continue;
					((rgm_value_t *)q->rp_value)->
					    rp_value[lni] = it->second;
				}
			}
		}
		nl = ndl;
		while (nl) {
			if ((((rgm_value_t *)q->rp_value)
			    ->rp_value[nl->nl_lni] == NULL) &&
			    (((rgm_value_t *)q->rp_value)->rp_value_node["0"]
			    != NULL))
				((rgm_value_t *)q->rp_value)
				    ->rp_value[nl->nl_lni] =
				    strdup(((rgm_value_t *)q->rp_value)
				    ->rp_value_node["0"]);
			nl = nl->nl_next;
		}
		((rgm_value_t *)q->rp_value)->rp_value_node.clear();
	}
	delete (rg_ns);
}

/*
 * This function copies the contents of the map r_switch_node
 * to the map r_switch.
 * New memory is allocated to r_switch map elements. Caller is responsible of
 * free'ing the memory allocated here!
 */
void
update_onoff_monitored_switch(rgm_resource_t *rsrc, nodeidlist *rg_nl)
{
	std::map<std::string, scha_switch_t>::iterator it;
	rgm::lni_t lni;
	LogicalNodeset *rg_ns = NULL;

	rg_ns = get_logical_nodeset_from_nodelist(rg_nl);
	for (it = ((rgm_switch_t *)rsrc->r_onoff_switch)
	    ->r_switch_node.begin(); it != ((rgm_switch_t *)
	    rsrc->r_onoff_switch)->r_switch_node.end(); it++) {
		if (it->second == SCHA_SWITCH_DISABLED)
			continue;
		// Default value is DISABLED.
		lni = rgm_get_lni((char *)it->first.data());
		if (lni == LNI_UNDEF)
			continue;
		if (rg_ns->containLni(lni)) {
			((rgm_switch_t *)rsrc->r_onoff_switch)
			    ->r_switch[lni] = it->second;
		}
	}

	for (it = ((rgm_switch_t *)rsrc->r_monitored_switch)
	    ->r_switch_node.begin(); it !=
	    ((rgm_switch_t *)rsrc->r_monitored_switch)
	    ->r_switch_node.end(); it++) {
		if (it->second == SCHA_SWITCH_DISABLED)
			continue;
		// Default value is DISABLED.
		lni = rgm_get_lni((char *)it->first.data());
		if (lni == LNI_UNDEF)
			continue;
		if (!rg_ns->containLni(lni))
			continue;
		((rgm_switch_t *)rsrc->r_monitored_switch)
		    ->r_switch[lni] = it->second;
	}
	delete (rg_ns);
}

/*
 * Convert the data of rgm_resource_t format to a string with
 * each property separated by ','
 * If realloc_err fails, process will exit.
 * Note: caller needs to free memory.
 */
char *
rsrc2str(rgm_resource_t *rsrc)
{
	uint_t bufsize;
	char	*val;
	char	*ptr = NULL;
	rgm_property_list_t *p;

	if (rsrc == NULL) {
		ptr = strdup("");
		return (ptr);
	}

	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_TYPE, rsrc->r_type);
	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_TYPE_VERSION, rsrc->r_type_version);
	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_R_DESCRIPTION, rsrc->r_description);

	val = enum2str(((rgm_switch_t *)rsrc->r_onoff_switch)->r_switch),

	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_ON_OFF_SWITCH, val);
	free(val);

	val = enum2str(((rgm_switch_t *)rsrc->r_monitored_switch)
	    ->r_switch);
	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_MONITORED_SWITCH, val);
	free(val);

	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_RESOURCE_PROJECT_NAME, rsrc->r_project_name);

	val = nstr2str_dep(rsrc->r_dependencies.dp_strong);
	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_RESOURCE_DEPENDENCIES, val);
	free(val);

	val = nstr2str_dep(rsrc->r_dependencies.dp_weak);
	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_RESOURCE_DEPENDENCIES_WEAK, val);
	free(val);

	val = nstr2str_dep(rsrc->r_dependencies.dp_restart);
	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_RESOURCE_DEPENDENCIES_RESTART, val);
	free(val);

	val = nstr2str_dep(rsrc->r_dependencies.dp_offline_restart);
	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART, val);
	free(val);

	val = nstr2str_dep(rsrc->r_inter_cluster_dependents.dp_strong);
	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_IC_RESOURCE_DEPENDENTS, val);
	free(val);

	val = nstr2str_dep(rsrc->r_inter_cluster_dependents.dp_weak);
	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_IC_RESOURCE_DEPENDENTS_WEAK, val);
	free(val);

	val = nstr2str_dep(rsrc->r_inter_cluster_dependents.dp_restart);
	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_IC_RESOURCE_DEPENDENTS_RESTART, val);
	free(val);

	val = nstr2str_dep(rsrc->r_inter_cluster_dependents.dp_offline_restart);
	var_strcat(&ptr, "%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_IC_RESOURCE_DEPENDENTS_OFFLINE_RESTART, val);
	free(val);

	for (p = rsrc->r_properties; p != 0; p = p->rpl_next) {
		if (p->rpl_property->rp_type == SCHA_PTYPE_STRINGARRAY) {
			if (p->rpl_property->rp_array_values == NULL)
				var_strcat(&ptr,
				    "%s="RGM_PROP_SEPARATOR_STR,
				    p->rpl_property->rp_key);
			else {
				val =
				    nstr2str(p->rpl_property->rp_array_values);
				var_strcat(&ptr,
				    "%s=%s"RGM_PROP_SEPARATOR_STR,
				    p->rpl_property->rp_key, val);
				free(val);
			}
		} else {
			if (((rgm_value_t *)p->rpl_property->rp_value)
			    ->rp_value[0] == NULL)
				var_strcat(&ptr,
				    "%s="RGM_PROP_SEPARATOR_STR,
				    p->rpl_property->rp_key);
			else
				var_strcat(&ptr,
				    "%s=%s"RGM_PROP_SEPARATOR_STR,
				    p->rpl_property->rp_key,
				    ((rgm_value_t *)p->rpl_property->rp_value)
				    ->rp_value[0]);
		}
	}

	// Add the SCHA_EXTENSION and RGM_PROP_SEPARATOR_STR
	// The + 2 is for the separator and for the  NULL char.
	bufsize = strlen(ptr) + strlen(SCHA_EXTENSION) + 2;
	ptr = (char *)realloc_err(ptr, bufsize);
	(void) strcat(ptr, SCHA_EXTENSION);
	(void) strcat(ptr, RGM_PROP_SEPARATOR_STR);

	for (p = rsrc->r_ext_properties; p != 0; p = p->rpl_next) {
		if (p->rpl_property->is_per_node == B_TRUE) {
			val = NULL;
			(void) pnext2str(((rgm_value_t *)p->rpl_property
			    ->rp_value)->rp_value, p->rpl_property->rp_key,
			    &val);
			if (val == NULL) {
				var_strcat(&ptr,
				    "%s="RGM_PROP_SEPARATOR_STR,
				    p->rpl_property->rp_key);
			} else {
				var_strcat(&ptr, "%s", val);
				free(val);
			}
		} else if (p->rpl_property->rp_type == SCHA_PTYPE_STRINGARRAY) {
			if (p->rpl_property->is_per_node == B_TRUE) {
				// Should never reach here!
				ASSERT(0);
			}
			if (p->rpl_property->rp_array_values == NULL)
				var_strcat(&ptr,
				    "%s="RGM_PROP_SEPARATOR_STR,
				    p->rpl_property->rp_key);
			else {
				val =
				    nstr2str(p->rpl_property->rp_array_values);
				var_strcat(&ptr,
				    "%s=%s"RGM_PROP_SEPARATOR_STR,
				    p->rpl_property->rp_key, val);
				free(val);
			}
		} else if (((rgm_value_t *)p->rpl_property->rp_value)
		    ->rp_value[0] == NULL)
				var_strcat(&ptr,
				    "%s="RGM_PROP_SEPARATOR_STR,
				    p->rpl_property->rp_key);
		else
			var_strcat(&ptr,
			    "%s=%s"RGM_PROP_SEPARATOR_STR,
			    p->rpl_property->rp_key,
			    ((rgm_value_t *)p->rpl_property->rp_value)
			    ->rp_value[0]);

	}

	// overwrite the last separator with a NUL
	ptr[strlen(ptr) - 1] = '\0';	/* remove the last separator */

	return (ptr);
}

#ifndef EUROPA_FARM
scha_errmsg_t
ccr_put_resource(const ccr::updatable_table_var &transptr, char *key,
	rgm_resource_t *value)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	CORBA::Exception *ex;
	char	mod_key[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RS)];

	if (value == NULL) {
		res.err_code = SCHA_ERR_INTERNAL;
		return (res);
	}

	char *s = rsrc2str(value);
	// Prefix the RGM_PREFIX_RS to the resource name.
	// This is to guarantee the uniqueness of resource name and ensure that
	// there are no conflicts with some of the RG property names.
	// Note that we only modify the resource name "key" in the RG table,
	// leaving the "value", which is the entire resource, unchanged.
	(void) sprintf(mod_key, "%s%s", RGM_PREFIX_RS, key);
	transptr->add_element(mod_key, s, e);
	free(s);
	if ((ex = e.exception()) != NULL) {
		if (ccr::key_exists::_exnarrow(ex)) {
			// duplicate resource
			res.err_code = SCHA_ERR_RSRC;
		} else {
			// ccr add element fails
			res.err_code = SCHA_ERR_CCR;
		}
		e.clear();
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}

//
// rgmcnfg_update_resource -
//	Update the resource properties for the given R in RG table
//
scha_errmsg_t
rgmcnfg_update_resource(rgm_resource_t *resource, const char *zone)
{
	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_15,
		FaultFunctions::generic);

	Environment e;
	char buff[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RG)];
	char *s;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char	mod_key[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RS)];

	if (resource == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		return (res);
	}

	ccr::updatable_table_var transptr = NULL;
	ccr::directory_var ccrdir = lookup_ccrdir();

	(void) sprintf(buff, "%s%s", RGM_PREFIX_RG, resource->r_rgname);

	transptr = ccrdir->begin_transaction_zc(ZONE, buff, e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_RG;
		return (res);
	}

	s = rsrc2str(resource);
	// Note that we prefix the string RGM_PREFIX_RS to the resource
	// name before storing it in the CCR. Hence, we need to prefix
	// it here too before updating it in the CCR.
	(void) sprintf(mod_key, "%s%s", RGM_PREFIX_RS, resource->r_name);
	transptr->update_element(mod_key, s, e);
	free(s);
	if (e.exception()) {
		e.clear();
		transptr->abort_transaction(e);
		res.err_code = SCHA_ERR_CCR;
		return (res);
	}

	transptr->commit_transaction(e);
	if (e.exception()) {
		e.clear();
		transptr->abort_transaction(e);
		res.err_code = SCHA_ERR_CCR;
		return (res);
	}

	return (res);
}

//
// rgmcnfg_update_rgtable -
//	Update RG properties in CCR for the given RG
//	rg_aff_unset is set during removal of inter cluster source affinity
//	rg.
//
scha_errmsg_t
rgmcnfg_update_rgtable(rgm_rg_t *rg, const char *zone, boolean_t rg_aff_unset)
{
	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_14,
	    FaultFunctions::generic);

	Environment e;
	CORBA::Exception *ex;
	char tablename[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RG)];
	char mod_key[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RS)];
	char *s = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t rgp = NULL;
	rlist_p_t rp = NULL;
	boolean_t all_nodes_enabled;
	boolean_t any_monitor_enabled;
	rgm::lni_t lni = 0;
	LogicalNodeset *old_ns, *new_ns;
	LogicalNodeset added_ns, removed_ns;
	rgm_resource_t *resource = NULL;
	namelist_t *orig_rg_aff = NULL;
	ccr::updatable_table_var transptr = NULL;
	ccr::directory_var ccrdir = lookup_ccrdir();

	(void) sprintf(tablename, "%s%s", RGM_PREFIX_RG, rg->rg_name);

	transptr = ccrdir->begin_transaction_zc(ZONE, tablename, e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
		return (res);
	}

	s = nodeidlist2str(rg->rg_nodelist);

	transptr->update_element(SCHA_NODELIST, s, e);
	free(s);
	if ((ex = e.exception()) != NULL)
		goto abort_update_rg; //lint !e801

	//
	// Whenever the RG nodelist is changed, we need to update the
	// onoff_switch and monitored_switch of all resources in CCR so that
	// they are also in sync with changes in resource group nodelist.
	//
	rgp = rgname_to_rg(rg->rg_name);
	CL_PANIC(rgp != NULL);
	old_ns = get_logical_nodeset_from_nodelist(rgp->rgl_ccr->rg_nodelist);
	new_ns = get_logical_nodeset_from_nodelist(rg->rg_nodelist);
	added_ns = *new_ns - *old_ns;
	removed_ns = *old_ns - *new_ns;

	if (!added_ns.isEmpty() || !removed_ns.isEmpty()) {
		for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
			rgmcnfg_copy_resource(rp->rl_ccrdata, &resource);
			CL_PANIC(resource != NULL);
			all_nodes_enabled = B_TRUE;
			any_monitor_enabled = B_FALSE;
			if (!added_ns.isEmpty()) {
				//
				// Nodes have been added to RG nodelist. If the
				// resource is enabled on all nodes in RG
				// nodelist, then enable it on the new node
				// which is added to RG nodelist. Else, disable
				// it on the newly added node.
				// If the resource is monitored on atleast one
				// node in RG nodelist, then enable monitoring
				// on the newly added node. Else, disable it.
				//
				lni = 0;
				while ((lni = old_ns->nextLniSet(lni + 1)) !=
				    0) {
					if (((rgm_switch_t *)resource->
					    r_onoff_switch)->r_switch[lni] ==
					    SCHA_SWITCH_DISABLED) {
						all_nodes_enabled = B_FALSE;
					}
					if (((rgm_switch_t *)resource->
					    r_monitored_switch)->
					    r_switch[lni] ==
					    SCHA_SWITCH_ENABLED) {
						any_monitor_enabled = B_TRUE;
					}
					if (any_monitor_enabled &&
					    !all_nodes_enabled)
						break;
				}
				lni = 0;
				while ((lni = added_ns.nextLniSet(
				    lni + 1)) != 0) {
					((rgm_switch_t *)resource->
					    r_onoff_switch)->r_switch[lni] =
					    all_nodes_enabled ?
					    SCHA_SWITCH_ENABLED :
					    SCHA_SWITCH_DISABLED;

					((rgm_switch_t *)resource->
					    r_monitored_switch)->
					    r_switch[lni] =
					    any_monitor_enabled ?
					    SCHA_SWITCH_ENABLED :
					    SCHA_SWITCH_DISABLED;
				}
			}
			if (!removed_ns.isEmpty()) {
				//
				// If nodes are removed from RG nodelist, then
				// disable the resources along with monitoring
				// on those nodes.
				//
				lni = 0;
				while ((lni = removed_ns.nextLniSet(lni + 1))
				    != 0) {
					((rgm_switch_t *)resource->
					    r_onoff_switch)->r_switch[lni] =
					    SCHA_SWITCH_DISABLED;
					((rgm_switch_t *)resource->
					    r_monitored_switch)->r_switch[lni] =
					    SCHA_SWITCH_DISABLED;
				}
			}

			s = rsrc2str(resource);
			//
			// Note that we prefix the string RGM_PREFIX_RS to the
			// resource name before storing it in CCR. Hence, we
			// need to prefix it here before updating it in CCR.
			//
			(void) sprintf(mod_key, "%s%s", RGM_PREFIX_RS,
			    resource->r_name);
			transptr->update_element(mod_key, s, e);
			free(s);
			free(resource);
			if ((ex = e.exception()) != NULL)
				goto abort_update_rg; //lint !e801
		}
	}

	s = rgm_itoa(rg->rg_max_primaries);
	transptr->update_element(SCHA_MAXIMUM_PRIMARIES, s, e);
	free(s);
	if ((ex = e.exception()) != NULL)
		goto abort_update_rg; //lint !e801

	s = rgm_itoa(rg->rg_desired_primaries);
	transptr->update_element(SCHA_DESIRED_PRIMARIES, s, e);
	free(s);
	if ((ex = e.exception()) != NULL)
		goto abort_update_rg; //lint !e801

	transptr->update_element(SCHA_FAILBACK,
	    bool2str(rg->rg_failback), e);
	if ((ex = e.exception()) != NULL)
		goto abort_update_rg; //lint !e801

	s = nstr2str(rg->rg_dependencies);
	transptr->update_element(SCHA_RG_DEPENDENCIES, s, e);
	free(s);
	if ((ex = e.exception()) != NULL)
		goto abort_update_rg; //lint !e801

	s = nstr_all2str(&rg->rg_glb_rsrcused);
	transptr->update_element(SCHA_GLOBAL_RESOURCES_USED, s, e);
	free(s);
	if ((ex = e.exception()) != NULL)
		goto abort_update_rg; //lint !e801

	transptr->update_element(SCHA_RG_MODE,
	    rgmode2str(rg->rg_mode), e);
	if ((ex = e.exception()) != NULL)
		goto abort_update_rg; //lint !e801

	transptr->update_element(SCHA_IMPL_NET_DEPEND,
	    bool2str(rg->rg_impl_net_depend), e);
	if ((ex = e.exception()) != NULL)
		goto abort_update_rg; //lint !e801

	transptr->update_element(SCHA_PATHPREFIX, rg->rg_pathprefix, e);
	if ((ex = e.exception()) != NULL)
		goto abort_update_rg; //lint !e801

	transptr->update_element(SCHA_RG_DESCRIPTION, rg->rg_description, e);
	if ((ex = e.exception()) != NULL) {
		goto abort_update_rg; //lint !e801
	}

	s = rgm_itoa(rg->rg_ppinterval);
	transptr->update_element(SCHA_PINGPONG_INTERVAL, s, e);
	free(s);
	if ((ex = e.exception()) != NULL)
		goto abort_update_rg; //lint !e801

	transptr->update_element(SCHA_RG_PROJECT_NAME, rg->rg_project_name, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {
			e.clear();
			res = ccr_put_string(transptr, SCHA_RG_PROJECT_NAME,
			    rg->rg_project_name);
			if (res.err_code != SCHA_ERR_NOERR)
				goto abort_update_rg; //lint !e801
		} else
			goto abort_update_rg; //lint !e801
	}

// SC SLM addon start
	transptr->update_element(SCHA_RG_SLM_TYPE, rg->rg_slm_type, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {
			e.clear();
			res = ccr_put_string(transptr, SCHA_RG_SLM_TYPE,
			    rg->rg_slm_type);
			if (res.err_code != SCHA_ERR_NOERR)
				goto abort_update_rg; //lint !e801
		} else
			goto abort_update_rg; //lint !e801
	}

	transptr->update_element(
	    SCHA_RG_SLM_PSET_TYPE, rg->rg_slm_pset_type, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {
			e.clear();
			res = ccr_put_string(transptr, SCHA_RG_SLM_PSET_TYPE,
			    rg->rg_slm_pset_type);
			if (res.err_code != SCHA_ERR_NOERR)
				goto abort_update_rg; //lint !e801
		} else
			goto abort_update_rg; //lint !e801
	}

	s = rgm_itoa(rg->rg_slm_cpu);
	transptr->update_element(SCHA_RG_SLM_CPU_SHARES, s, e);
	free(s);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {
			e.clear();
			res = ccr_put_int(transptr, SCHA_RG_SLM_CPU_SHARES,
			    rg->rg_slm_cpu);
			if (res.err_code != SCHA_ERR_NOERR)
				goto abort_update_rg; //lint !e801
		} else
			goto abort_update_rg; //lint !e801
	}

	s = rgm_itoa(rg->rg_slm_cpu_min);
	transptr->update_element(SCHA_RG_SLM_PSET_MIN, s, e);
	free(s);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {
			e.clear();
			res = ccr_put_int(transptr, SCHA_RG_SLM_PSET_MIN,
			    rg->rg_slm_cpu_min);
			if (res.err_code != SCHA_ERR_NOERR)
				goto abort_update_rg; //lint !e801
		} else
			goto abort_update_rg; //lint !e801
	}
// SC SLM addon end

	s = nstr2str(rg->rg_affinities);
	transptr->update_element(SCHA_RG_AFFINITIES, s, e);
	free(s);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {
			e.clear();
			res = ccr_put_nstring(transptr, SCHA_RG_AFFINITIES,
			    rg->rg_affinities);
			if (res.err_code != SCHA_ERR_NOERR)
				goto abort_update_rg; //lint !e801
		} else
			goto abort_update_rg; //lint !e801
	}

	if (allow_inter_cluster()) {
		namelist_t *rg_copied_affs = NULL;
		rg_copied_affs = namelist_copy(rgp->rgl_ccr->rg_affinitents);
		if (rg_aff_unset) {
			namelist_t *affs = NULL;
			// Delete only those entries in the CCR
			// which match the rg->rg_affinitents.
			for (affs = rg->rg_affinitents; affs != NULL;
			    affs = affs->nl_next) {
				if (namelist_exists(
				    rg_copied_affs, affs->nl_name))
					namelist_delete_name(&(
					    rg_copied_affs),
					    affs->nl_name);
			}
		} else {
			namelist_t *affs = NULL;
			for (affs = rg->rg_affinitents; affs != NULL;
			    affs = affs->nl_next) {
				if (!namelist_exists(rg_copied_affs,
				    affs->nl_name))
					namelist_add_name(&(rg_copied_affs),
					    affs->nl_name);
			}
		}
		s = nstr2str(rg_copied_affs);
		transptr->update_element(SCHA_IC_RG_AFFINITY_SOURCES, s, e);
		free(s);
		if ((ex = e.exception()) != NULL) {
			if (ccr::no_such_key::_exnarrow(ex)) {
				e.clear();
				res = ccr_put_nstring(transptr,
				    SCHA_IC_RG_AFFINITY_SOURCES,
				    rg_copied_affs);
				if (res.err_code != SCHA_ERR_NOERR) {
					rgm_free_nlist(rg_copied_affs);
					goto abort_update_rg; //lint !e801
				}
			} else  {
				rgm_free_nlist(rg_copied_affs);
				goto abort_update_rg; //lint !e801
			}
		}
		rgm_free_nlist(rg_copied_affs);
	}

	transptr->update_element(SCHA_RG_AUTO_START,
	    bool2str(rg->rg_auto_start), e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {
			e.clear();
			res = ccr_put_bool(transptr, SCHA_RG_AUTO_START,
			    rg->rg_auto_start);
			if (res.err_code != SCHA_ERR_NOERR)
				goto abort_update_rg; //lint !e801
		} else
			goto abort_update_rg; //lint !e801
	}

	transptr->update_element(SCHA_RG_SUSP_AUTO_RECOVERY,
	    bool2str(rg->rg_suspended), e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {
			e.clear();
			res = ccr_put_bool(transptr, SCHA_RG_SUSP_AUTO_RECOVERY,
			    rg->rg_suspended);
			if (res.err_code != SCHA_ERR_NOERR)
				goto abort_update_rg; //lint !e801
		} else
			goto abort_update_rg; //lint !e801
	}

	//
	// RG_System Property
	//
	transptr->update_element(SCHA_RG_SYSTEM, bool2str(rg->rg_system), e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {
			e.clear();
			res = ccr_put_bool(transptr, SCHA_RG_SYSTEM,
			    rg->rg_system);
			if (res.err_code != SCHA_ERR_NOERR)
				goto abort_update_rg; //lint !e801
		} else
			goto abort_update_rg; //lint !e801
	}

	if (rg->rg_auto_start) {
		// If Auto_start_on_new_cluster is TRUE, add Ok_To_Start in ccr.
		res = ccr_put_string(transptr, RG_OK_TO_START, "");
		if (res.err_code != SCHA_ERR_NOERR)
			if (res.err_code == SCHA_ERR_PROP)
				// Ok if Ok_To_Start flag already in ccr
				res.err_code = SCHA_ERR_NOERR;
			else
				goto abort_update_rg; //lint !e801
	}

	transptr->commit_transaction(e);
	if ((ex = e.exception()) != NULL)
		goto abort_update_rg; //lint !e801

	return (res);

abort_update_rg:
	//
	// If we got here, we either got
	// a) an error when writing the CCR (res.err_code != SCHA_ERR_NOERR) or
	// b) an exception from update_element.
	// In the latter case, save and convert the exception type to a return
	// error code before clearing the exception and aborting the transaction
	//
	if (res.err_code == SCHA_ERR_NOERR) {
		// b) got exception from update_element
		if (ccr::no_such_key::_exnarrow(ex))
			// no such key maps to internal error
			res.err_code = SCHA_ERR_INTERNAL;
		else
			// other errors map to CCR error
			res.err_code = SCHA_ERR_CCR;

		e.clear();
	}

	if (transptr) {
		transptr->abort_transaction(e);
		if (e.exception())
			// this exception is not important
			e.clear();
	}
	return (res);
}

//
// rgmcnfg_add_resource -
//	Add the specified R in the RG table and add resource name in
//	ResourceList in RG table
//
scha_errmsg_t
rgmcnfg_add_resource(rgm_resource_t *resource)
{
	Environment e;
	CORBA::Exception *ex;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	char buff[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RG)];
	char *rs_buff;
	char *r_list = 0;

	if (resource == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		return (res);
	}


	ccr::updatable_table_var transptr = NULL;
	ccr::directory_var ccrdir = lookup_ccrdir();

	(void) sprintf(buff, "%s%s", RGM_PREFIX_RG,
	    resource->r_rgname);

	transptr = ccrdir->begin_transaction_zc(ZONE, buff, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(ex)) {
			e.clear();
			res.err_code = SCHA_ERR_RG;
			return (res);
		} else {
			e.clear();
			// table_invalid or unexpected exception
			res.err_code = SCHA_ERR_CCR;
			return (res);
		}
	}

	res = ccr_put_resource(transptr, resource->r_name, resource);
	if (res.err_code != SCHA_ERR_NOERR) {
		transptr->abort_transaction(e);
		if (e.exception())
			// this exception is not important
			e.clear();
		return (res);
	}

	// add resource name in ResourceList
	res = get_property_value(resource->r_rgname, O_RG,
	    SCHA_RESOURCE_LIST, &r_list, ZONE);
	if (r_list == NULL || r_list[0] == '\0') {
		rs_buff = new char[strlen(resource->r_name) + 1];
		if (rs_buff == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}
		(void) strcpy(rs_buff, resource->r_name);
	} else {
		// We have allocate buff big enough to hold r_list, r_name and
		// separator.
		rs_buff = new char[strlen(resource->r_name) +
		    strlen(r_list) + 2];
		if (rs_buff == NULL) {
			delete [] r_list;
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}
		(void) sprintf(rs_buff, "%s%c%s", r_list, RGM_VALUE_SEPARATOR,
		    resource->r_name);
	}
	delete [] r_list;

	transptr->update_element(SCHA_RESOURCE_LIST, rs_buff, e);
	delete [] rs_buff;
	if (e.exception()) {
		e.clear();
		transptr->abort_transaction(e);
		if (e.exception())
			// this exception is not important
			e.clear();
		res.err_code = SCHA_ERR_CCR;
		return (res);
	}

	transptr->commit_transaction(e);
	if (e.exception()) {
		e.clear();
		transptr->abort_transaction(e);
		if (e.exception())
			// this exception is not important
			e.clear();
		res.err_code = SCHA_ERR_CCR;
		return (res);
	}

	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_10,
		FaultFunctions::generic);

	return (res);

}

#endif
// SC SLM addon start

//
// function used to store local per node scconf SCSLM property
//
static void
scslm_get_scconf_props(scconf_cfg_node_t *curr_p)
{
	for (; curr_p != NULL;
	    curr_p = curr_p->scconf_node_next) {
		if (curr_p->scconf_node_nodeid == Nodeid) {
			slm_global_zone_shares =
			    curr_p->scslm_global_zone_shares;
			slm_default_pset_min = curr_p->scslm_default_pset_min;
			break;
		}
	}
}


//
// function used to read stored scconf SCSLM properties
// Caller must hold rgm_lock_state
//
void
scslm_get_scconf(unsigned int *global_zone_shares,
    unsigned int *default_pset_min)
{
	*global_zone_shares = 0;
	*default_pset_min = 0;
	if (slm_global_zone_shares != prev_slm_global_zone_shares) {
		prev_slm_global_zone_shares = slm_global_zone_shares;
		*global_zone_shares = slm_global_zone_shares;
	}
	if (slm_default_pset_min != prev_slm_default_pset_min) {
		prev_slm_default_pset_min = slm_default_pset_min;
		*default_pset_min = slm_default_pset_min;
	}
}


//
// function used to notify local fed per node scconf SCSLM properties
// changes.
// Caller must not hold rgm_lock_state
//
void
scslm_set_scconf(unsigned int global_zone_shares,
    unsigned int default_pset_min)
{
	fe_cmd args;
	fe_run_result res;
	char *nodename = rgm_get_nodename(Nodeid);

	if (global_zone_shares != 0) {
		//
		// scconf -c -S node=XX,globalzoneshares=10,
		// for example, will update the CCR SCSLM per node
		// property globalzoneshares. This is detected in rgmd
		// by CCR scconf cache changes.
		// Here we check that we are node XX, as we only have
		// to contact local fed if we are node XX.
		//
		bzero(&args, sizeof (args));
		args.host = nodename;
		args.security = SEC_UNIX_STRONG;
		args.global_zone_shares  = global_zone_shares;
		//
		// Notify local fed about current per node SCSLM scconf
		// properties value
		//
		(void) fe_slm_conf_global_zone_shares("", &args, &res);
	}
	if (default_pset_min != 0) {
		//
		// scconf -c -S node=XX,defaultpsetmin=3,
		// for example, will update the CCR SCSLM per node
		// property defaultpsetmin. This is detected in rgmd
		// by CCR scconf cache changes.
		// Here we check that we are node XX, as we only have
		// to contact local fed if we are node XX.
		//
		bzero(&args, sizeof (args));
		args.host = nodename;
		args.security = SEC_UNIX_STRONG;
		args.pset_min = default_pset_min;
		//
		// Notify local fed about current per node SCSLM scconf
		(void) fe_slm_conf_pset_min("", &args, &res);
	}
	free(nodename);
}
// SC SLM addon end

// zone privilege check functionality
//
// sets if call is from global zone, and if nodelist of rg contains that zone
//
void
ngzone_nodelist_check(const rglist_p_t rg_ptr, uint_t nodeid,
    const char *zonename, boolean_t &globalzone, boolean_t &nodelist_contains)

{
	ngzone_check(zonename, globalzone);
	nodelist_check(rg_ptr, nodeid, zonename, nodelist_contains);
}

void
ngzone_check(const char *zonename, boolean_t &globalzone)
{
	globalzone = B_FALSE;
	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		// no checks for cz
		globalzone = B_TRUE;
	} else if (zonename == NULL ||
	    zonename[0] == '\0' ||
	    strcmp(zonename, GLOBAL_ZONENAME) == 0) {
		globalzone = B_TRUE;
	}

}

void
nodelist_check(const rglist_p_t rg_ptr, uint_t nodeid, const char *zonename,
    boolean_t &nodelist_contains)
{
	// if its a clusterized zone, we are sure that it can only reach the
	// rgmd which is running for this zone. so we are sure that such
	// resources can indeed be managed from that zone.
	// so essentially this function is useful for non clusterized zones
	// for global zone, the ngzone_check will override the nodelist check,
	// so it wouldn't matter
	//

	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0)
		nodelist_contains = B_TRUE;
	else
		nodelist_contains = nodeidlist_contains(
		    rg_ptr->rgl_ccr->rg_nodelist,
		    lnManager->computeLn(nodeid, zonename));
}

//
// is_zone_up
//
// Similar to in_current_logical_membership(), but returns TRUE only if
// the zone is a cluster member and not in SHUTTING_DOWN state.
// The caller must hold the rgm state lock.
//
boolean_t
is_zone_up(rgm::lni_t node)
{
	LogicalNode *lnNode = lnManager->findObject(node);
	// lnNode could be NULL if we don't know about the zone yet
	return ((lnNode != NULL && lnNode->getStatus() == rgm::LN_UP) ?
	    B_TRUE : B_FALSE);
}

//
// Utility functions for inter cluster dependencies and inter cluster
// rg affinities.
// Check if cluster:resource format is used for a resource in dependency
// configuration.
//
boolean_t
is_enhanced_naming_format_used(const char *rname)
{

	const char *ptr = NULL;
	// check for ":"
	ptr = strchr(rname, ':');
	if (ptr != NULL)
		return (B_TRUE);
	else
		return (B_FALSE);
}

#if SOL_VERSION >= __s10
//
// Check if the resource/rg is a local cluster resoure/rg
// takes  rname in format cluster:resource/cluster:rg
// When rg_flag is set to B_TRUE the check is made for the rg
// When rg_flag is set to B_FALSE the check is made for the
// resource.
// Returns true if the resource/rg is of local cluster.
//
boolean_t
is_this_local_cluster_r_or_rg(const char *r_or_rg_name,
    boolean_t rg_flag)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *r_c_name = NULL;
	char *r_r_name = NULL;

	// Get the remote cluster and resource/rg name.
	res = get_remote_cluster_and_rorrg_name(r_or_rg_name,
	    &r_c_name, &r_r_name);
	if ((res.err_code == SCHA_ERR_NOERR) && (r_c_name != NULL) &&
		(r_r_name != NULL)) {
		// Check for the cluster name and resource name.
		if (!rg_flag) {
			if (strcasecmp(r_c_name, ZONE) == 0) {
				free(r_c_name);
				free(r_r_name);
				return (B_TRUE);
			}
		} else {
		// Check for the cluster name and rg name.
			if (strcasecmp(r_c_name, ZONE) == 0) {
				free(r_c_name);
				free(r_r_name);
				return (B_TRUE);
			}
		}
	}
	free(r_c_name);
	free(r_r_name);
	return (B_FALSE);
}

//
// Fetches the remote cluster name as well as remote resource name
// takes  res_name in format cluster:resource and returns cluster and
// resource/rg name in rc and rr respectively.
//
scha_errmsg_t
get_remote_cluster_and_rorrg_name(const char *res_name, char **rc, char **rr)
{
	const char 		*ptr = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	ptr = (const char *)strchr(res_name, ':');

	if (ptr != NULL) {
		// Get the resource/rg name.
		char *tmp = (char *)malloc(strlen(res_name) + 1);
		strcpy(tmp, res_name);
		*rr = (char *)malloc(res_name + strlen(res_name) - ptr);
		if (*rr == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}
		strncpy(*rr, ptr + 1, res_name + strlen(res_name) - ptr);
		if ((strlen(*rr) == 0) || (strcmp(*rr, "") == 0))
			*rr = NULL;

		// Get the cluster name.
		*rc = (char *)malloc(ptr-res_name+1);
		if (*rc == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}
		strtok(tmp, ":");
		strcpy(*rc, tmp);
		if ((strlen(*rc) == 0) || (strcmp(*rc, "") == 0))
			*rc = NULL;
		free(tmp);
	}
	return (res);
}

//
// Fetch the remote cluster name.
// takes  res_name in format cluster:resource/rg and returns cluster name
// in the variable rc.
//
scha_errmsg_t
get_remote_cluster_name(const char *res_name, char **rc)
{
	const char 		*ptr = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	ptr = strchr(res_name, ':');
	if (ptr != NULL) {
		char *tmp = (char *)malloc(strlen(res_name) + 1);
		strcpy(tmp, res_name);
		*rc = (char *)malloc(ptr-res_name+1);
		if (*rc == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}
		strtok(tmp, ":");
		strcpy(*rc, tmp);
		if ((strlen(*rc) == 0) || (strcmp(*rc, "") == 0))
			*rc = NULL;
		free(tmp);
	}
	return (res);
}

//
// Fetch the remote resource name
// takes  the input in format cluster:resource/rg and returns
// resource name/rg name in variable rr.
//
scha_errmsg_t
get_remote_resource_or_rg_name(const char *r_rg_name,  char **rr)
{

	const char 		*ptr = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	ptr = strchr(r_rg_name, ':');
	if (ptr != NULL) {
		*rr = (char *)malloc(r_rg_name + strlen(r_rg_name) - ptr);
		if (*rr == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}
		strncpy(*rr, ptr + 1, r_rg_name + strlen(r_rg_name) - ptr);
		if ((strlen(*rr) == 0) || (strcmp(*rr, "") == 0))
			*rr = NULL;
	}
	return (res);
}

//
// Fetches the dependency list which contains only the remote resource
// dependencies.
//
scha_errmsg_t
get_intercluster_rdep_lists(rgm_dependencies_t r_deps,
	namelist_t **strong, namelist_t **weak, namelist_t **restart,
	namelist_t **offline_restart)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	// get the list for strong inter cluster dependencies.
	if (strong != NULL) {
		res = get_rdeplist_helper(r_deps.dp_strong, strong);
		if (res.err_code != SCHA_ERR_NOERR)
			return (res);
	}

	// get the list for weak inter cluster dependencies.
	if (weak != NULL) {
		res = get_rdeplist_helper(r_deps.dp_weak, weak);
		if (res.err_code != SCHA_ERR_NOERR)
			return (res);
	}

	// get the list for inter cluster restart dependencies.
	if (restart != NULL) {
		res = get_rdeplist_helper(r_deps.dp_restart, restart);
		if (res.err_code != SCHA_ERR_NOERR)
			return (res);
	}

	// get the list for inter cluster offline restart dependencies.
	if (offline_restart != NULL) {
		res = get_rdeplist_helper(r_deps.dp_offline_restart,
		    offline_restart);
		if (res.err_code != SCHA_ERR_NOERR)
			return (res);
	}
	return (res);
}

//
// given an resource name or rg name checks if the resource or rg
// is of local cluster. If the resource or rg is of remote cluster the
// resource/rg is added on to the namelist res_list.
//
scha_errmsg_t
get_list_helper(name_t r_or_rg_name, namelist **res_list,
    boolean_t rg_flag)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	if (is_this_local_cluster_r_or_rg(r_or_rg_name, rg_flag))  {
		res.err_code = SCHA_ERR_DEPEND;
		if (rg_flag) {
			rgm_format_errmsg(&res,
			    REM_UPDATE_ICAFF_LOCAL_CLUSTER,
			    r_or_rg_name);
		} else {
			rgm_format_errmsg(&res,
			    REM_UPDATE_ICDEP_LOCAL_CLUSTER,
			    r_or_rg_name);
		}
		return (res);
	}	else 	{
		// remote resource.
		char *rc = NULL, *rr = NULL;
		uint32_t clid;
		int err;
		res = get_remote_cluster_and_rorrg_name(
		    r_or_rg_name,  &rc, &rr);
		if ((res.err_code == SCHA_ERR_NOERR) &&
		    (rc != NULL) && (rr != NULL)) {
			err = clconf_get_cluster_id(rc, &clid);
			if (((err == 0) && (clid != 0) && (clid <
			    MIN_CLUSTER_ID)) || (
				(err != 0) && (err != EACCES))) {
				// Inside non-clusterized zone
				res.err_code = SCHA_ERR_CLUSTER;
				rgm_format_errmsg(&res, REM_RGM_IC_UNREACH, rc);
				free(rr);
				free(rc);
				return (res);
			} else if (err == EACCES) {
				ASSERT(0); // Cannot happen
			}
			res = namelist_add_name(
			    res_list, r_or_rg_name);
			if (res.err_code != 0) {
				res.err_code =
				    SCHA_ERR_NOMEM;
				return (res);
			}
			if (rc == NULL) {
				res.err_code = SCHA_ERR_INVAL;
				free(rr);

			}
			if (rr == NULL) {
				rgm_format_errmsg(&res,
				    REM_INVALID_REMOTE_R);
				res.err_code = SCHA_ERR_INVAL;
				free(rc);

			}
		}
	}
	return (res);
}

//
// helper function to get inter cluster resource dependencies. deps_list
// has the list of dependency resources of one of the foir flavours.
// res_list will have only the inter cluster resource dependencies.
//
scha_errmsg_t
get_rdeplist_helper(rdeplist_t *deps_list, namelist **res_list)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	for (rdeplist_t *deps = deps_list; deps != NULL; deps = deps->rl_next) {
		// check if the dependency is specified on remote
		// cluster resource.
		if (!is_enhanced_naming_format_used(deps->rl_name)) {
			continue;
		} else {
			res = get_list_helper(deps->rl_name, res_list, B_FALSE);
		}
	}
	return (res);
}

// helper function to get the list of inter cluster rg affinities.
scha_errmsg_t
get_aff_list_helper(namelist *aff_list, namelist **res_list)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	for (namelist *aff = aff_list; aff != NULL; aff = aff->nl_next) {
		// check if the affinity is specified on remote
		// cluster resource group.
		if (!is_enhanced_naming_format_used(aff->nl_name)) {
			continue;
		} else {
			res = get_list_helper(aff->nl_name, res_list, B_TRUE);
		}
	}
	return (res);
}

//
// Fetches the affinity list which contains only the remote rg
// affinities.
//
scha_errmsg_t
get_intercluster_rg_aff_lists(rgm_affinities_t aff,
	namelist_t **spd, namelist_t **sp, namelist_t **wp,
	namelist_t **sn, namelist_t **wn)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	// get the "+++" inter-cluster rg affinities.
	if (spd != NULL) {
		res = get_aff_list_helper(aff.ap_failover_delegation, spd);
		if (res.err_code != SCHA_ERR_NOERR)
			return (res);
	}

	// get the "++" inter-cluster rg affinities.
	if (sp != NULL) {
		res = get_aff_list_helper(aff.ap_strong_pos, sp);
		if (res.err_code != SCHA_ERR_NOERR)
			return (res);
	}

	// get the "+" inter-cluster rg affinities.
	if (wp != NULL) {
		res = get_aff_list_helper(aff.ap_weak_pos, wp);
		if (res.err_code != SCHA_ERR_NOERR)
			return (res);
	}

	// get the "--" inter-cluster rg affinities.
	if (sn != NULL) {
		res = get_aff_list_helper(aff.ap_strong_neg, sn);
		if (res.err_code != SCHA_ERR_NOERR)
			return (res);
	}

	// get the "-" inter-cluster rg affinities.
	if (wn != NULL) {
		res = get_aff_list_helper(aff.ap_weak_neg, wn);
		if (res.err_code != SCHA_ERR_NOERR)
			return (res);
	}
	return (res);
}

//
// if an invalid notification error is returned then the rg source affinity
// or dependent r information has to be removed from the rg's/r's list.
// This function is called only on the rgm president node.
//
void
cleanup_r_rg(const char*r_rg_name, const char *dep_aff_name, boolean_t rg_flag,
    deptype_t deps)
{

	rgm::idl_nameval_seq_t	prop_val;

	ucmm_print("cleanup_r_rg()", NOGET(
	    "for RG/R <%s>, dependent_rs/target affinity%s, "
	    "rg_flag %d\n"), r_rg_name, dep_aff_name, rg_flag);

	if (rg_flag) {
		prop_val.length(1);
		prop_val[0].idlstr_nv_name = new_str(
			SCHA_IC_RG_AFFINITY_SOURCES);

		prop_val[0].nv_val.length(1);
		prop_val[0].nv_val[0] =
		    new_str(dep_aff_name);
		cleanup_aff_info(r_rg_name, dep_aff_name, prop_val);
	} else {
		prop_val.length(1);
		switch (deps) {
			case DEPS_OFFLINE_RESTART :
				prop_val[0].idlstr_nv_name = new_str(
				SCHA_IC_RESOURCE_DEPENDENTS_OFFLINE_RESTART);
				break;
			case DEPS_RESTART :
				prop_val[0].idlstr_nv_name = new_str(
					SCHA_IC_RESOURCE_DEPENDENTS_RESTART);
				break;
			case DEPS_WEAK :
				prop_val[0].idlstr_nv_name = new_str(
					SCHA_IC_RESOURCE_DEPENDENTS_WEAK);
				break;
			case DEPS_STRONG :
				prop_val[0].idlstr_nv_name = new_str(
					SCHA_IC_RESOURCE_DEPENDENTS);
		}
		prop_val[0].nv_val.length(1);
		prop_val[0].nv_val[0] =
		    new_str(dep_aff_name);
		cleanup_dep_info(r_rg_name,  dep_aff_name, deps, prop_val);
	}
}
#endif

//
// The dedicated thread that services CCR callbacks
// for infrastructure table changes.
// This thread never needs to exit. It will die when rgmd dies.
//
static void *
infr_table_cb_thread(void *)
{
	while (1) {
		infr_table_cb_lock.lock();
		while (no_new_infr_table_cb) {
			infr_table_cb_cv.wait(&infr_table_cb_lock);
		}

		ucmm_print("RGM",
		    "infr_table_cb_thread woke up to service a callback\n");
		//
		// A callback has happened, so we woke up.
		// We will do some tasks now.
		// Mark that we do not have pending callbacks
		// to service, and then leave the lock
		// so that another callback from CCR does not block.
		//
		no_new_infr_table_cb = true;
		infr_table_cb_lock.unlock();

		// SC SLM addon start
		unsigned int saved_slm_global_zone_shares;
		unsigned int saved_slm_default_pset_min;
		// SC SLM addon end

		//
		// must grab the lock to synchronize the calls to
		// ucmm_getcluststate and lnManager.updateConfiguredNodes().
		//
		rgm_lock_state();
		(void) create_cache(B_FALSE);

		//
		// rgmd registers for these callbacks, but not rgm starter.
		// So we should check if rgmd has decided to use
		// process membership APIs, and accordingly
		// pass B_TRUE/B_FALSE to this function in the second argument.
		// XXX Check this logic
		//
		if (process_membership_support_available()) {
			update_cluster_state(B_FALSE, B_TRUE);
		} else {
			update_cluster_state(B_FALSE, B_FALSE);
		}
		lnManager->updateConfiguredNodes(Rgm_state->static_membership);

		// SC SLM addon start
		scslm_get_scconf(&saved_slm_global_zone_shares,
		    &saved_slm_default_pset_min);
		// SC SLM addon end
		rgm_unlock_state();
		// SC SLM addon start
		scslm_set_scconf(saved_slm_global_zone_shares,
		    saved_slm_default_pset_min);
		// SC SLM addon end
		ucmm_print("RGM",
		    "infr_table_cb_thread finished servicing callback\n");
	}
	return (NULL);
}

void
start_infr_table_cb_thread()
{
	if (thr_create(NULL, 0, infr_table_cb_thread,
	    NULL, THR_BOUND, NULL) != 0) {
		sc_syslog_msg_handle_t handle;
		(void) sc_syslog_msg_initialize(
		    &handle, SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Could not start the rgmd thread that will service
		// infrastructure table callbacks from CCR.
		// This might be due to inadequate memory on the system.
		// @user_action
		// Add more memory to the system. If that does not resolve the
		// problem, contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: could not start "
		    "infrastructure table callback handler thread"));
		sc_syslog_msg_done(&handle);
		abort();
	}
}

int
get_log_level_for_res_state(rgm::rgm_r_state r_state)
{

// To set the severity of syslog messages for resource state changes
#ifdef DEBUG
	const int DEFAULT_LOG_LEVEL = LOG_NOTICE;
#else
	const int DEFAULT_LOG_LEVEL = LOG_DEBUG;
#endif
	switch (r_state) {

	case rgm::R_MON_FAILED:
		return (LOG_WARNING);

	case rgm::R_START_FAILED:
	case rgm::R_STOP_FAILED:
		return (LOG_ERR);

	case rgm::R_OFFLINE:
	case rgm::R_ONLINE:
	case rgm::R_ONLINE_UNMON:
	case rgm::R_STARTING:
	case rgm::R_STOPPING:
		return (LOG_NOTICE);

	default:
		return (DEFAULT_LOG_LEVEL);
	}
}
