/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)rgm_recep_serv_xdr.c	1.8	08/05/20 SMI"
/*
 * rgm_recep_xdr.c - This file defines the functions
 * that are used for marshalling and unmarshalling of
 * data which is to be sent across door calls.
 * We use the xdr library for the marshalling and
 * unmarshalling operations
 */

#include <rgm/rgm_recep.h>
#include <stdlib.h>
#include <sys/cl_assert.h>
void
scha_result_args_xdr_free(int api_num, void *result)
{
	switch (api_num) {
		case SCHA_RS_OPEN:
			xdr_free((xdrproc_t)xdr_rs_open_result_t,
			    (char *)result);
			break;
		case SCHA_RS_GET:
		case SCHA_GET_ALL_EXTPROPS:
		case SCHA_RS_GET_STATE:
		case SCHA_RS_GET_FAILED_STATUS:
		case SCHA_RT_GET:
		case SCHA_RG_GET:
		case SCHA_RG_GET_STATE:
		case SCHA_CLUSTER_GET:
		case SCHA_IS_PERNODE:
			xdr_free((xdrproc_t)xdr_get_result_t,
			    (char *)result);
			break;
		case SCHA_RS_GET_EXT:
			xdr_free((xdrproc_t)xdr_get_ext_result_t,
			    (char *)result);
			break;
		case SCHA_RS_GET_STATUS:
			xdr_free((xdrproc_t)xdr_get_status_result_t,
			    (char *)result);
			break;
		case SCHA_RS_SET_STATUS:
			xdr_free((xdrproc_t)xdr_set_status_result_t,
			    (char *)result);
			break;
		case SCHA_RT_OPEN:
		case SCHA_RG_OPEN:
		case SCHA_CLUSTER_OPEN:
			xdr_free((xdrproc_t)xdr_open_result_t,
			    (char *)result);
			break;
		case SCHA_RS_GET_SWITCH:
			xdr_free((xdrproc_t)xdr_get_switch_result_t,
			    (char *)result);
			break;
		case SCHA_SSM_IP:
			xdr_free((xdrproc_t)xdr_ssm_result_t,
			    (char *)result);
			break;
		case SCHA_LB:
			xdr_free((xdrproc_t)xdr_scha_ssm_lb_result_t,
			    (char *)result);
			break;
		case SCHA_CONTROL:
			xdr_free((xdrproc_t)xdr_scha_result_t,
			    (char *)result);
			break;
		default:
			CL_PANIC(0);
			break;
	}
}

void
scha_input_args_xdr_free(scha_input_args_t *scha_args)
{
	switch (scha_args->api_name) {
		case SCHA_RS_OPEN:
			xdr_free((xdrproc_t)xdr_rs_open_args,
			    scha_args->data);
			break;
		case SCHA_RS_GET:
		case SCHA_RS_GET_EXT:
		case SCHA_GET_ALL_EXTPROPS:
			xdr_free((xdrproc_t)xdr_rs_get_args,
			    scha_args->data);
			break;
		case SCHA_RS_GET_STATE:
		case SCHA_RS_GET_STATUS:
			xdr_free((xdrproc_t)xdr_rs_get_state_status_args,
			    scha_args->data);
			break;
		case SCHA_RS_GET_FAILED_STATUS:
			xdr_free((xdrproc_t)xdr_rs_get_fail_status_args,
			    scha_args->data);
			break;
		case SCHA_RS_SET_STATUS:
			xdr_free((xdrproc_t)xdr_rs_set_status_args,
			    scha_args->data);
			break;
		case SCHA_RT_OPEN:
			xdr_free((xdrproc_t)xdr_rt_open_args,
			    scha_args->data);
			break;
		case SCHA_RT_GET:
		case SCHA_IS_PERNODE:
			xdr_free((xdrproc_t)xdr_rt_get_args,
			    scha_args->data);
			break;
		case SCHA_RG_OPEN:
			xdr_free((xdrproc_t)xdr_rg_open_args,
			    scha_args->data);
			break;
		case SCHA_RS_GET_SWITCH:
			xdr_free((xdrproc_t)xdr_rs_get_switch_args,
			    scha_args->data);
			break;
		case SCHA_RG_GET:
			xdr_free((xdrproc_t)xdr_rg_get_args,
			    scha_args->data);
			break;
		case SCHA_RG_GET_STATE:
			xdr_free((xdrproc_t)xdr_rg_get_state_args,
			    scha_args->data);
			break;
		case SCHA_CLUSTER_GET:
			xdr_free((xdrproc_t)xdr_cluster_get_args,
			    scha_args->data);
			break;
		case SCHA_SSM_IP:
			xdr_free((xdrproc_t)xdr_ssm_ip_args,
			    scha_args->data);
			break;
		case SCHA_LB:
			xdr_free((xdrproc_t)xdr_scha_ssm_lb_args,
			    scha_args->data);
			break;
		case SCHA_CLUSTER_OPEN:
			break;
		case SCHA_CONTROL:
			xdr_free((xdrproc_t)xdr_scha_control_args,
			    scha_args->data);
			break;
		default:
			CL_PANIC(0);
			break;
	}
	free(scha_args->data);
}

bool_t
xdr_scha_result_args(XDR *xdrs, void *objp, int api_name)
{
	switch (api_name) {
		case SCHA_RS_OPEN:
			if (!xdr_rs_open_result_t(xdrs,
			    (rs_open_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET:
		case SCHA_GET_ALL_EXTPROPS:
		case SCHA_RS_GET_FAILED_STATUS:
		case SCHA_RS_GET_STATE:
		case SCHA_RT_GET:
		case SCHA_IS_PERNODE:
		case SCHA_RG_GET:
		case SCHA_RG_GET_STATE:
		case SCHA_CLUSTER_GET:
			if (!xdr_get_result_t(xdrs,
			    (get_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_SWITCH:
			if (!xdr_get_switch_result_t(xdrs,
			    (get_switch_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_EXT:
			if (!xdr_get_ext_result_t(xdrs,
			    (get_ext_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_STATUS:
			if (!xdr_get_status_result_t(xdrs,
			    (get_status_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_SET_STATUS:
			if (!xdr_set_status_result_t(xdrs,
			    (set_status_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_RT_OPEN:
		case SCHA_RG_OPEN:
		case SCHA_CLUSTER_OPEN:
			if (!xdr_open_result_t(xdrs,
			    (open_result_t *)objp)) {
				return (FALSE);
			}
			break;

		case SCHA_SSM_IP:
			if (!xdr_ssm_result_t(xdrs,
			    (ssm_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_LB:
			if (!xdr_scha_ssm_lb_result_t(xdrs,
			    (scha_ssm_lb_result_t *)objp)) {
				return (FALSE);
			}
			break;
		case SCHA_CONTROL:
			if (!xdr_scha_result_t(xdrs,
			    (scha_result_t *)objp)) {
				return (FALSE);
			}
			break;
		default:
			ASSERT(0);
			break;
		}
	return (TRUE);
}


bool_t
xdr_scha_input_args(register XDR *xdrs, scha_input_args_t *objp)
{
#if defined(_LP64) || defined(_KERNEL)
	register int *buf;
#else
	register long *buf;
#endif

	if (!xdr_int(xdrs, &objp->api_name)) {
		return (FALSE);
	}
	switch (objp->api_name) {
		case SCHA_RS_OPEN:
			objp->data = (rs_open_args *)
			    calloc(1, sizeof (rs_open_args));
			if (!xdr_rs_open_args(xdrs,
			    (rs_open_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET:
		case SCHA_RS_GET_EXT:
		case SCHA_GET_ALL_EXTPROPS:
			objp->data = (rs_get_args *)
			    calloc(1, sizeof (rs_get_args));
			if (!xdr_rs_get_args(xdrs,
			    (rs_get_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_STATE:
		case SCHA_RS_GET_STATUS:
			objp->data = (rs_get_state_status_args *)
			    calloc(1, sizeof (rs_get_state_status_args));

			if (!xdr_rs_get_state_status_args(xdrs,
			    (rs_get_state_status_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_FAILED_STATUS:
			objp->data = (rs_get_fail_status_args *)
			    calloc(1, sizeof (rs_get_fail_status_args));

			if (!xdr_rs_get_fail_status_args(xdrs,
			    (rs_get_fail_status_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_SET_STATUS:
			objp->data = (rs_set_status_args *)
			    calloc(1, sizeof (rs_set_status_args));
			if (! xdr_rs_set_status_args(xdrs,
			    (rs_set_status_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RT_OPEN:
			objp->data = (rt_open_args *)
			    calloc(1, sizeof (rt_open_args));
			if (!xdr_rt_open_args(xdrs,
			    (rt_open_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_IS_PERNODE:
		case SCHA_RT_GET:
			objp->data = (rt_get_args *)
			    calloc(1, sizeof (rt_get_args));
			if (!xdr_rt_get_args(xdrs,
			    (rt_get_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RG_OPEN:
			objp->data = (rg_open_args *)
			    calloc(1, sizeof (rg_open_args));
			if (!xdr_rg_open_args(xdrs,
			    (rg_open_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RS_GET_SWITCH:
			objp->data = (rs_get_switch_args *)
			    calloc(1, sizeof (rs_get_switch_args));
			if (!xdr_rs_get_switch_args(xdrs,
			    (rs_get_switch_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RG_GET:
			objp->data = (rg_get_args *)
			    calloc(1, sizeof (rg_get_args));
			if (!xdr_rg_get_args(xdrs,
			    (rg_get_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_RG_GET_STATE:
			objp->data = (rg_get_state_args *)
			    calloc(1, sizeof (rg_get_state_args));
			if (!xdr_rg_get_state_args(xdrs,
			    (rg_get_state_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_CLUSTER_GET:
			objp->data = (cluster_get_args *)
			    calloc(1, sizeof (cluster_get_args));
			if (!xdr_cluster_get_args(xdrs,
			    (cluster_get_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_SSM_IP:
			objp->data = (ssm_ip_args *)
			    calloc(1, sizeof (ssm_ip_args));
			if (!xdr_ssm_ip_args(xdrs,
			    (ssm_ip_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_LB:
			objp->data = (scha_ssm_lb_args *)
			    calloc(1, sizeof (scha_ssm_lb_args));
			if (!xdr_scha_ssm_lb_args(xdrs,
			    (scha_ssm_lb_args *)objp->data)) {
				return (FALSE);
			}
			break;
		case SCHA_CLUSTER_OPEN:
			objp->data = (void *)calloc(1, sizeof (void *));
			break;
		case SCHA_CONTROL:
			objp->data = (scha_control_args *)
			    calloc(1, sizeof (scha_control_args));
			if (!xdr_scha_control_args(xdrs,
			    (scha_control_args *)objp->data)) {
				return (FALSE);
			}
			break;
		default:
			CL_PANIC(0);
			break;
	}
	return (TRUE);
}

/*
 * How we calculate size for xdr encoding:
 * ---------------------------------------
 * The Size calculation that we do here has to calculate
 * the sizeof the structure members and also the sizeof
 * the data that is pointed to be the pointers if any
 * int he structure.
 * For eg:
 * struct abc {
 *	int a;
 *	char *ch;
 * }
 * in this case we will find the size of the struct abc
 * which will give us the size needed for storing the
 * elements that is an int and a char pointer. And also
 * we will need to add the sizeof the array pointed to
 * by ch.
 * In addition to this, xdr encodes string in the following
 * way:
 * [integer(for size of the string) + string]
 * So, for any string say char *ch = {"example"}
 * The size needed will be :
 * sizeof (ch) + sizeof (int) + strlen(ch)
 * Here the sizeof (int) is taken for storing the sizeof
 * the string.
 */

size_t
scha_xdr_sizeof_result(void *data, int api)
{
	size_t size = 0;
	uint_t i = 0;
	size = RNDUP(sizeof (int));

	switch (api) {
		case SCHA_RS_OPEN:
			size += RNDUP(sizeof (rs_open_result_t)) +
			    RNDUP(safe_strlen(((rs_open_result_t *)
			    (data))->rt_name)) +
			    RNDUP(safe_strlen(((rs_open_result_t *)
			    (data))->rs_seq_id)) +
			    RNDUP(safe_strlen(((rs_open_result_t *)
			    (data))->rt_seq_id)) +
			    (3 * RNDUP(sizeof (int)));
			/*
			 * For the 3 strings
			 * in the structure.
			 * seqid is also char*
			 */
			break;

		case SCHA_RS_GET_EXT:
			size += RNDUP(sizeof (get_ext_result_t)) +
			    RNDUP(safe_strlen(((get_ext_result_t *)
			    (data))->seq_id)) +
			    RNDUP(sizeof (int));
			/*
			 * For the 1 string
			 * in the structure.
			 */
			/* We check if the pointer is NULL we return. */
			if (((get_ext_result_t *)(data))->value_list == NULL) {
				break;
			}
			/*
			 * If the pointer is not NULL. We traverse all the
			 * strings and get the length of each string.
			 */
			size += RNDUP(sizeof (((get_ext_result_t *)
			    (data))->value_list->strarr_list_len));
			if (((get_ext_result_t *)(data))->value_list->
			    strarr_list_len > 0) {
				for (i = 0; i < ((get_ext_result_t *)(data))->
				    value_list->strarr_list_len; i++) {
					size += RNDUP(safe_strlen(
					    ((get_ext_result_t *)data)->
					    value_list->strarr_list_val[i]));
					size += RNDUP(sizeof (int));
				}
			}
			break;

		case SCHA_RS_GET:
		case SCHA_GET_ALL_EXTPROPS:
		case SCHA_IS_PERNODE:
		case SCHA_RS_GET_STATE:
		case SCHA_RS_GET_FAILED_STATUS:
		case SCHA_RT_GET:
		case SCHA_RG_GET:
		case SCHA_RG_GET_STATE:
		case SCHA_CLUSTER_GET:
			size += RNDUP(sizeof (get_result_t)) +
			    RNDUP(safe_strlen(((get_result_t *)
			    (data))->seq_id)) +
			    RNDUP(sizeof (int));
			/*
			 * For the 1 string
			 * in the structure
			 */
			/* We check if the pointer is NULL we return. */
			if (((get_result_t *)(data))->value_list == NULL) {
				break;
			}
			/*
			 * If the pointer is not NULL. We traverse all the
			 * strings and get the length of each string.
			 */
			size += RNDUP(sizeof (((get_result_t *)(data))->
			    value_list->strarr_list_len));
			if (((get_result_t *)(data))->value_list->
			    strarr_list_len > 0) {
				for (i = 0; i < ((get_result_t *)(data))->
				    value_list->strarr_list_len; i++) {
					size += RNDUP(safe_strlen(
					    ((get_result_t *)data)->value_list->
					    strarr_list_val[i]));
					size += RNDUP(sizeof (int));
					/* For the 1 string in the structure */
				}
			}
			break;
		case SCHA_RS_GET_SWITCH:
			size += RNDUP(sizeof (get_switch_result_t)) +
			    RNDUP(safe_strlen(((get_switch_result_t *)
			    (data))->seq_id)) +
			    RNDUP(sizeof (int));
			/*
			 * For the 1 string
			 * in the structure
			 */
			break;
		case SCHA_RS_GET_STATUS:
			size += RNDUP(sizeof (get_status_result_t)) +
			    RNDUP(safe_strlen(((get_status_result_t *)
			    (data))->status_msg)) +
			    RNDUP(safe_strlen(((get_status_result_t *)
			    (data))->seq_id)) +
			    (2 *RNDUP(sizeof (int)));
			/*
			 * For the 2 strings
			 * in the structure
			 */
			break;

		case SCHA_RS_SET_STATUS:
			size += RNDUP(sizeof (set_status_result_t));
			break;
		case SCHA_RT_OPEN:
		case SCHA_RG_OPEN:
		case SCHA_CLUSTER_OPEN:
			size += RNDUP(sizeof (open_result_t)) +
			    RNDUP(safe_strlen(((open_result_t *)
			    (data))->seq_id)) +
			    RNDUP(sizeof (int));
			/*
			 * For the 1 string
			 * in the structure
			 */
			break;
		case SCHA_SSM_IP:
			size += RNDUP(sizeof (ssm_result_t));
			break;
		case SCHA_LB:
			size += RNDUP(sizeof (scha_ssm_lb_result_t)) +
			    (RNDUP(((scha_ssm_lb_result_t *)
			    (data))->ret_data.ret_data_len) *
			    RNDUP(sizeof (int *))) +
			    RNDUP(sizeof (int));
			/*
			 * Extra sizeof (int) for the variable len
			 * int array
			 */
			break;
		case SCHA_CONTROL:
			size += RNDUP(sizeof (scha_result_t));
			break;
	}
	return (size);
}
