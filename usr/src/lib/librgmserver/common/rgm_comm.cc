/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_comm.cc	1.128	08/10/29 SMI"

/*
 * rgm_comm.cc
 *
 * This file contains the client side of RGM internode communication functions.
 */

#ifndef EUROPA_FARM
#include <orb/fault/fault_injection.h>
#endif
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <synch.h>

#include <sys/cladm.h>
#include <orb/invo/common.h>
#include <h/sol.h>
#include <h/naming.h>
#include <nslib/ns.h>
#ifndef EUROPA_FARM
#include <rgm/rgm_comm_impl.h>
#endif
#include <rgm_proto.h>
#include <scadmin/scconf.h>
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#ifndef EUROPA_FARM
#include <sys/rsrc_tag.h>
#include <rgm/scha_priv.h>
#endif
#include <sys/cl_events.h>
#include <rgm_threads.h>

#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_pres.h>
#include <rgm/rpc/rgmx_comm_farm.h>
#endif
#include <ucontext.h>


#include <rgmx_hook.h>

#ifndef EUROPA_FARM
//
// rgm_comm_getstate requests R and RG state from physical node 'i'.
// Called in rgm_president.cc
// Target (server) node invokes the rgm_xmit_state() method.
// State is transmitted for all zones on the slave, in a single call.
// Only "interesting" state -- non-offline R/RG state, non-empty status --
// is transmitted.
//
idlretval_t
rgm_comm_getstate(sol::nodeid_t i, rgm::node_state *&state)
{
	rgm::rgm_comm_var prgm_serv;
	Environment e;
	idlretval_t retval = RGMIDL_OK;
	bool is_farmnode;
	rgm::node_state *tmp_state;

	if ((rgmx_hook_call(rgmx_hook_xmit_state, i, &tmp_state,
		&retval, &is_farmnode)
		== RGMX_HOOK_ENABLED) && (is_farmnode)) {
		state = tmp_state;
		return (retval);
	}

	prgm_serv = rgm_comm_getref(i);
	if (CORBA::is_nil(prgm_serv)) {
		return (RGMIDL_NOREF);
	}

	//
	// Turn pointers to structs into structs.
	// rgm_xmit_state routine expects references, so we're really
	// not passing whole structs, just pointers to them.
	//

	prgm_serv->rgm_xmit_state(state, e);
	retval = except_to_errcode(e, i);

	return (retval);
}


//
// rgm_comm_I_am_President tells node 'i' I'm President.
// Called in rgm_president.cc.
// Target (server) node invokes the rgm_set_pres() method.
// Returns 0 on success; non-zero on failure.
//
idlretval_t
rgm_comm_I_am_President(sol::nodeid_t i, sol::nodeid_t pres)
{
	rgm::rgm_comm_var prgm_serv;
	Environment e;
	idlretval_t retval = RGMIDL_OK;
	bool is_farmnode;

	if ((rgmx_hook_call(rgmx_hook_set_pres, i, pres,
		&retval, &is_farmnode) == RGMX_HOOK_ENABLED) &&
		(is_farmnode)) {
		return (retval);
	}

	prgm_serv = rgm_comm_getref(i);
	if (CORBA::is_nil(prgm_serv)) {
		return (RGMIDL_NOREF);
	}

	prgm_serv->rgm_set_pres(pres,
	    Rgm_state->node_incarnations.members[orb_conf::local_nodeid()], e);
	retval = except_to_errcode(e, i);

	return (retval);
}


//
// President-side function that requests a node to change mastery
// of an RG.
//
idlretval_t
rgm_change_mastery(rgm::lni_t lni, namelist_t *on_list, namelist_t *off_list)
{
	rgm::rgm_comm_var prgm_serv;
	Environment e;
	idlretval_t retval = RGMIDL_OK;
	rgm::idl_string_seq_t onlist;
	rgm::idl_string_seq_t offlist;
	bool is_farmnode;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	sol::nodeid_t slave = lnNode->getNodeid();

	if ((rgmx_hook_call(rgmx_hook_chg_mastery,
		slave, lni, lnNode->getIncarnation(),
		on_list, off_list,
		&retval, &is_farmnode) == RGMX_HOOK_ENABLED) &&
		(is_farmnode)) {
		return (retval);
	}

	// convert namelist to sequence of strings
	namelist_to_seq(on_list, onlist);
	namelist_to_seq(off_list, offlist);

	prgm_serv = rgm_comm_getref(slave);
	if (CORBA::is_nil(prgm_serv)) {
		return (RGMIDL_NOREF);
	}

	prgm_serv->rgm_chg_mastery(orb_conf::local_nodeid(),
	    Rgm_state->node_incarnations.members[orb_conf::local_nodeid()],
	    lni, lnNode->getIncarnation(), onlist, offlist, e);

	// onlist, offlist, allocated on stack, so no need to free
	retval = except_to_errcode(e, slave, lni);

	return (retval);
}
#endif

//
// rgm_resource_restart
//
// President-side function that requests a node to restart
// a resource, or notifies the node that the RG is being restarted by
// this resource.
//
// If rrflag is RGM_RRF_RG, then this is an RG restart.  The calling
// function (idl_scha_control_restart) will drive the restart by making
// change_mastery calls.  The call made here to rgm_r_restart is only to
// notify the slave to update the RG restart history for this resource.
//
// If rrflag is RGM_RRF_RIR, then this is a RESOURCE_IS_RESTARTED call.
// This tells the slave to update the resource restart history without
// restarting the resource.
//
// If rrflag is RGM_RRF_R or RGM_RRF_TR, then this is a resource restart.
// The call to rgm_r_restart tells the slave to restart the resource as well as
// updating the restart history on the slave.
//
idlretval_t
rgm_resource_restart(rgm::lni_t lni, rgm::rgm_restart_flag rrflag,
    const char *r_name)
{
#ifndef EUROPA_FARM
	rgm::rgm_comm_var prgm_serv;
	Environment e;
	idlretval_t retval = RGMIDL_OK;
	bool is_farmnode;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	sol::nodeid_t slave = lnNode->getNodeid();

	if ((rgmx_hook_call(rgmx_hook_r_restart,
		slave, lni, lnNode->getIncarnation(),
		rrflag, r_name,
		&retval, &is_farmnode) == RGMX_HOOK_ENABLED) &&
		(is_farmnode)) {
		return (retval);
	}

	prgm_serv = rgm_comm_getref(slave);
	if (CORBA::is_nil(prgm_serv)) {
		return (RGMIDL_NOREF);
	}

	ucmm_print("RGM",
	    NOGET("prgm_serv->rgm_r_restart lni %d node %d res %s"),
	    lni, slave, r_name);

	prgm_serv->rgm_r_restart(orb_conf::local_nodeid(),
	    Rgm_state->node_incarnations.members[orb_conf::local_nodeid()],
	    lni, lnNode->getIncarnation(), rrflag, r_name, e);

	// Handle exception
	retval = except_to_errcode(e, slave, lni);

	return (retval);
#else
	idlretval_t retval = RGMIDL_OK;
	rgmx_r_restart_args rpc_args;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	sol::nodeid_t slave = lnNode->getNodeid();

	ucmm_print("RGM",
	    NOGET("rpc->rgmx_r_restart lni %d node %d res %s"),
	    lni, slave, r_name);

	rpc_args.president = Rgm_state->rgm_President;
	rpc_args.incarnation =
	    Rgm_state->node_incarnations.members[rpc_args.president];
	rpc_args.lni = lni;
	rpc_args.ln_incarnation = lnNode->getIncarnation();
	rpc_args.rrflag = (rgm_restart_flag)rrflag;
	rpc_args.R_name = strdup(r_name);

	if (rgmx_r_restart_1_svc(&rpc_args, NULL, NULL)
	    != RPC_SUCCESS) {
		retval = RGMIDL_NODE_DEATH;
	}

	return (retval);
#endif
}

#ifndef EUROPA_FARM
//
// rgm_comm_getrsswitch()
//
//	This function retrieves R's On_off/Monitored switch in the RS
//	state struture in memory and returns the On_off/Monitored switch
//	to the caller according to the boolean argument on_off. This
//	function grabs the lock on state structure before retrieving R
//	On_off/Monitored switch and holds the lock until the operation is done.
//
scha_errmsg_t
rgm_comm_getrsswitch(char *rs_name, char *rg_name, char *node_name,
	scha_switch_t *swtch, bool on_off, char **seq_id)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm::lni_t	lni;
	rglist_p_t	rgl = NULL;
	rlist_p_t	rl;
	rgm_resource_t  *resource = NULL;
	LogicalNodeset	*switch_ns = NULL;


	rgm_lock_state();

	if (node_name == NULL || node_name[0] == '\0') {
		// don't need to call in_current_membership()
		// because I know I'm a current member
		lni = orb_conf::local_nodeid();
	} else {
		if ((lni = rgm_get_lni(node_name)) == LNI_UNDEF) {
			// no such node
			res.err_code = SCHA_ERR_NODE;
			goto finished; //lint !e801
		}
	}

	if (rs_name == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		goto finished; //lint !e801
	}

	if (rg_name == NULL || rg_name[0] == '\0') {
		if ((rl = rname_to_r(NULL, rs_name)) == NULL) {
			res.err_code = SCHA_ERR_RSRC;
			goto finished; //lint !e801
		}
	} else {
		rgl = rgname_to_rg(rg_name);
		if (rgl != NULL) {
			rl = rname_to_r(rgl, rs_name);
			if (rl == NULL) {
				ucmm_print("RGM", NOGET(
				    "RS <%s> is not in memory"), rs_name);
				res.err_code = SCHA_ERR_RSRC;
				goto finished; //lint !e801
			}
		} else {
			ucmm_print("RGM",
			    NOGET("RG <%s> is not in memory"), rg_name);
			res.err_code = SCHA_ERR_RG;
			goto finished; //lint !e801
		}
	}


	resource = rl->rl_ccrdata;
	switch_ns = get_logical_nodeset_from_nodelist(
		rl->rl_rg->rgl_ccr->rg_nodelist);

	if (!switch_ns->containLni(lni)) {
		*swtch = SCHA_SWITCH_DISABLED;
	} else {
		if (on_off) {
			*swtch = ((rgm_switch_t *)resource->
			r_onoff_switch)->r_switch[lni];
		} else {
			*swtch = ((rgm_switch_t *)resource->
			r_monitored_switch)->r_switch[lni];
		}
	}

	// Since all Rs in one RG share the same seq_id.
	*seq_id = rl->rl_rg->rgl_ccr->rg_stamp;

	ucmm_print("RGM", NOGET("rgm_comm_getrsswitch: get switch success"));

finished:

	rgm_unlock_state();
	delete (switch_ns);

	return (res);
}

//
// rgm_comm_getext()
//
//	President side function that retrieves R's extension property in the RS
//	state struture in memory.
//	Its the callers responsibility to free the memory allocated to *value
//	in this function. The caller must hold the lock before calling this
//	function.
//
scha_errmsg_t
rgm_comm_getext(char *rs_name, char *rg_name, char *node_name,
	char *prop_name, char **value, char **seq_id)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm::lni_t	lni;
	rglist_p_t	rgl = NULL;
	rlist_p_t	rl;
	std::map<rgm::lni_t, char *> val;
	rgm_property_list_t *pp = NULL;
	LogicalNode	*ln = NULL;


	*value = NULL;
	if ((lni = rgm_get_lni(node_name)) == LNI_UNDEF) {
		// no such node
		res.err_code = SCHA_ERR_NODE;
		goto finished; //lint !e801
	}

	if (rs_name == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		goto finished; //lint !e801
	}

	if (rg_name == NULL || rg_name[0] == '\0') {
		if ((rl = rname_to_r(NULL, rs_name)) == NULL) {
			res.err_code = SCHA_ERR_RSRC;
			goto finished; //lint !e801
		}
	} else {
		rgl = rgname_to_rg(rg_name);
		if (rgl != NULL) {
			rl = rname_to_r(rgl, rs_name);
			if (rl == NULL) {
				ucmm_print("RGM", NOGET(
				    "RS <%s> is not in memory"), rs_name);
				res.err_code = SCHA_ERR_RSRC;
				goto finished; //lint !e801
			}
		} else {
			ucmm_print("RGM",
			    NOGET("RG <%s> is not in memory"), rg_name);
			res.err_code = SCHA_ERR_RG;
			goto finished; //lint !e801
		}
	}

	for (pp = rl->rl_ccrdata->r_ext_properties; pp != NULL;
	    pp = pp->rpl_next) {
		if (strcasecmp(prop_name, pp->rpl_property->rp_key) == 0)
			break;
	}

	/*
	 * If can not find the property in the list, return error
	 */
	if (pp == NULL) {
		res.err_code = SCHA_ERR_PROP;
		ucmm_print("rgm_comm_getext",
			NOGET("couldn't find ext property: returning\n"));
		goto finished; //lint !e801
	}

	if (pp->rpl_property->rp_type == SCHA_PTYPE_STRINGARRAY ||
	    pp->rpl_property->rp_type == SCHA_PTYPE_UINTARRAY) {
		//
		// Return SCHA_ERR_NOERR in case of the property type
		// being one of SCHA_PTYPE_STRINGARRAY or
		// SCHA_PTYPE_UINTARRAY. These would be handled
		// upon return to get_res_ext in rgm_recep_rs.cc
		// file.
		//
		*seq_id = rl->rl_rg->rgl_ccr->rg_stamp;
		return (res);
	}
	val = ((rgm_value_t *)pp->rpl_property->rp_value)->rp_value;

	if (pp->rpl_property->is_per_node) {
		if (val[lni] == NULL) {
			if ((res.err_code = copy_default_value(prop_name, rl,
			    value)) != SCHA_ERR_NOERR) {
				return (res);
			}
		} else {
			*value = strdup(val[lni]);
		}
	} else {
		if (val[0] == NULL) {
			//
			// Should never reach here.
			//
			res.err_code = SCHA_ERR_INTERNAL;
			return (res);
		} else {
			*value = strdup(val[0]);
		}
	}
	// Since all Rs in one RG share the same seq_id.
	*seq_id = rl->rl_rg->rgl_ccr->rg_stamp;

	ucmm_print("rgm_comm_getext", NOGET("rgm_comm_getext: "
	    "get ext success"));

finished:


	return (res);
}

#endif // EUROPA_FARM
//
// Updates the default value from the RT param table into value.
// Would be called when the query is made on a node that is not part of
// RG's nodelist. In that case we would return the RT default specified
// in the RTR file. Note that it is mandatory that we provide a default
// value for each of the per-node properties while registering the RT.
//
scha_err_t
copy_default_value(const char *prop_name, rlist_p_t r, char **value)
{
	rgm_param_t	*param = NULL;
	scha_err_t res = SCHA_ERR_NOERR;

	param = (rgm_param_t *)get_param_prop(prop_name, B_TRUE,
	    r->rl_ccrtype->rt_paramtable);
	if (param == NULL)
		return (SCHA_ERR_PROP);

	switch (param->p_type) {
	case SCHA_PTYPE_STRING:
	case SCHA_PTYPE_ENUM:
		if (param->p_defaultstr == NULL) {
			res = SCHA_ERR_INTERNAL;
			goto finished; //lint !e801
		}
		if ((*value = strdup_nocheck(param->p_defaultstr)) == NULL) {
			res = SCHA_ERR_NOMEM;
			ucmm_print("copy_default_value",
			    NOGET("couldn't strdup; returning\n"));

			goto finished; //lint !e801
		}
		break;
	case SCHA_PTYPE_UINT:
	case SCHA_PTYPE_INT:
		if ((*value = rgm_itoa(param->p_defaultint)) == NULL) {
			res = SCHA_ERR_NOMEM;
			ucmm_print("copy_default_value",
			    NOGET("couldn't strdup; returning\n"));
			goto finished; //lint !e801
		}
		break;
	case SCHA_PTYPE_BOOLEAN:
		if ((*value = strdup_nocheck(bool2str(param->p_defaultbool)))
		    == NULL) {
			res = SCHA_ERR_NOMEM;
			ucmm_print("copy_default_value",
			    NOGET("couldn't strdup; returning\n"));
			goto finished; //lint !e801
		}
		break;
	case SCHA_PTYPE_UINTARRAY:
	case SCHA_PTYPE_STRINGARRAY:
		res = SCHA_ERR_INVAL;
		break;
	}
	return (res);

finished:
	return (res);
}

#ifndef EUROPA_FARM
//
// who_is_joining
//
// The President node calls each other node during reconfiguration to find
// out who is currently joining the cluster.  The input argument is the
// bitmask of current cluster nodes.  This function returns the bitmask of
// joining nodes in its out parameter.  It bails out early if an
// exception is encountered, because we expect another reconfiguration.
//
// XXX could be optimized by saving results from earlier calls
// XXX to are_you_joining().
//
idlretval_t
who_is_joining(LogicalNodeset &curr_ns, LogicalNodeset *out_joining_ns)
{
	LogicalNodeset joining_ns;
	rgm::rgm_comm_var prgm_serv;
	bool is_joining;
	idlretval_t retval;
	sol::nodeid_t i;
	Environment e;
	bool is_farmnode;

	i = 0;
	while ((i = curr_ns.nextPhysNodeSet(i + 1)) != 0) {

		if ((rgmx_hook_call(rgmx_hook_am_i_joining, i, &is_joining,
			&retval, &is_farmnode) == RGMX_HOOK_ENABLED) &&
			(is_farmnode)) {
			if (retval != RGMIDL_OK) {
				return (retval);
			}
		} else {
			//
			// 'i' is in the current membership; get the
			// reference for node 'i'.
			//
			prgm_serv = rgm_comm_getref(i);
			//
			// Ensure that the object reference is not null.
			// If it is null, it means that this node died
			// while we are processing this reconfiguration.
			//
			if (CORBA::is_nil(prgm_serv)) {
				return (RGMIDL_NODE_DEATH);
			}
			prgm_serv->rgm_am_i_joining(is_joining, e);

			// handle exception
			if ((retval = except_to_errcode(e, i)) != RGMIDL_OK) {
				return (retval);
			}
		}

		if (is_joining)
			joining_ns.addLni(i);
	}

	*out_joining_ns = joining_ns;
	return (RGMIDL_OK);
}


//
// are_you_joining
//
// Each node calls each other node during reconfiguration to find
// out who is currently joining the cluster.
// The function bails out early if an exception is encountered, because we
// expect another reconfiguration.
//
idlretval_t
are_you_joining(sol::nodeid_t n, boolean_t *n_is_joining)
{
	rgm::rgm_comm_var prgm_serv;
	idlretval_t retval;
	bool is_joining;
	Environment e;
	bool is_farmnode;

	*n_is_joining = B_FALSE;

	if ((rgmx_hook_call(rgmx_hook_am_i_joining, n, &is_joining,
		&retval, &is_farmnode) == RGMX_HOOK_ENABLED) &&
		(is_farmnode)) {
		ucmm_print("are you joining", "inside here");
		if (retval != RGMIDL_OK) {
			return (retval);
		}
		if (is_joining) {
			*n_is_joining = B_TRUE;
		}
		return (RGMIDL_OK);
	}
	ucmm_print("are you joining", "call rgm_comm_getref for %d", n);

	//
	// 'n' is in the current membership; Get the reference
	// for node 'n'
	//
	prgm_serv = rgm_comm_getref(n);
	//
	// Ensure that the object reference is not null.
	// If it is null, it means that this node died
	// while we are processing this reconfiguration.
	//
	if (CORBA::is_nil(prgm_serv)) {
		return (RGMIDL_NODE_DEATH);
	}
	ucmm_print("are you joining", "call rgm_am_i_joining for %d", n);
	prgm_serv->rgm_am_i_joining(is_joining, e);
	ucmm_print("are you joining",
	    "check exception in rgm_am_i_joining for %d", n);

	// handle exception
	if ((retval = except_to_errcode(e, n)) != RGMIDL_OK) {
		// return early
		ucmm_print("are you joining",
		    "error: prgm_serv:%p", (void *)prgm_serv);
		return (retval);
	}

	ucmm_print("are you joining",
	    "rgm_am i joining returns:%d", is_joining);
	if (is_joining)
		*n_is_joining = B_TRUE;

	return (RGMIDL_OK);
}

//
// joiners_read_ccr
//
// This function is only called by the President in the big
// reconfiguration step.  The President doesn't call himself because he
// already read the CCR.  The lock is already held.  We return early on
// rgmd or node death because the UCMM will call us back again shortly.
//
idlretval_t
joiners_read_ccr(LogicalNodeset &join_ns)
{
	rgm::rgm_comm_var prgm_serv;
	Environment e;
	idlretval_t retval;
	sol::nodeid_t n;
	sc_syslog_msg_handle_t handle;
	rgm::lni_mappings_seq_t *lni_mappings;
	bool is_farmnode;

	lni_mappings = NULL;

	n = 0;
	while ((n = join_ns.nextPhysNodeSet(n + 1)) != 0) {
		// skip myself because I already read the CCR
		if (n == orb_conf::local_nodeid())
			continue;

		create_sequence_lni_mappings(n, &lni_mappings);

		if ((rgmx_hook_call(rgmx_hook_read_ccr,
			n, lni_mappings, &retval,
			&is_farmnode) == RGMX_HOOK_DISABLED) ||
			(!is_farmnode)) {

			//
			// 'n' is in the current membership; Get reference
			// of node 'n'.
			//
			prgm_serv = rgm_comm_getref(n);
			//
			// Ensure that the object reference is not null.
			// If it is null, it means that this node died
			// while we are processing this reconfiguration.
			//
			if (CORBA::is_nil(prgm_serv)) {
				return (RGMIDL_NODE_DEATH);
			}
			prgm_serv->rgm_read_ccr(orb_conf::local_nodeid(),
			    Rgm_state->node_incarnations.members[orb_conf::
			    local_nodeid()], *lni_mappings, e);

			delete lni_mappings;
			lni_mappings = NULL;

			//
			// Handle exception.
			// Return early on node or rgmd death.
			// Crash and burn on unknown exception.
			//
			retval = except_to_errcode(e, n);
		}

		switch (retval) {
		case RGMIDL_OK:
			break;
		case RGMIDL_NODE_DEATH:
		case RGMIDL_RGMD_DEATH:
			//
			// return early
			//
			return (retval);
		case RGMIDL_UNKNOWN:
		case RGMIDL_NOREF:
			//
			// Crash and burn.
			//
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The low-level cluster machinery has encountered a
			// fatal error. The rgmd will produce a core file and
			// will cause the node to halt or reboot.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: joiners_read_ccr: exiting early because of "
			    "unexpected exception");
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHED
		}
	}

	return (RGMIDL_OK);
}

//
// joiners_run_boot_methods
//
// This function is only called by the President in the big
// reconfiguration step.  The lock is already held.  We return
// early on rgmd or node death because the UCMM will call us
// back again shortly.
//
// Boot methods on the slave are executed only in the global zone and
// in any non-global zones that are already up.
//
// This function will set the president's idea of the state of the RG for each
// global zone (and all its non-global zones that are up) on which it calls
// rgm_run_boot_meths to RG_OFF_PENDING_BOOT.
//
idlretval_t
joiners_run_boot_methods(LogicalNodeset &join_ns)
{
	rgm::rgm_comm_var prgm_serv;
	Environment e;
	idlretval_t retval = RGMIDL_OK;
	sol::nodeid_t n;
	sc_syslog_msg_handle_t handle;
	sol::nodeid_t   mynodeid = orb_conf::node_number();
	bool is_farmnode;

	n = 0;
	while ((n = join_ns.nextPhysNodeSet(n + 1)) != 0) {
		if ((rgmx_hook_call(rgmx_hook_run_boot_meths, n, &retval,
			&is_farmnode) == RGMX_HOOK_DISABLED) ||
			(!is_farmnode)) {

			//
			// 'n' is in the current membership; Get reference
			// of node 'n'.
			//
			prgm_serv = rgm_comm_getref(n);
			//
			// Ensure that the object reference is not null.
			// If it is null, it means that this node died
			// while we are processing this reconfiguration.
			//
			if (CORBA::is_nil(prgm_serv)) {
				return (RGMIDL_NODE_DEATH);
			}
			prgm_serv->rgm_run_boot_meths(mynodeid,
			    Rgm_state->node_incarnations.members[mynodeid], e);

			//
			// Handle exception.
			// Return early on node or rgmd death.
			// Crash and burn on unknown exception.
			//
			retval = except_to_errcode(e, n);
		}

		switch (retval) {
		case RGMIDL_OK:
			break;
		case RGMIDL_NODE_DEATH:
		case RGMIDL_RGMD_DEATH:
			//
			// return early
			//
			return (retval);
		case RGMIDL_UNKNOWN:
		case RGMIDL_NOREF:
			//
			// Crash and burn.
			//
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The low-level cluster machinery has encountered a
			// fatal error. The rgmd will produce a core file and
			// will cause the node to halt or reboot to avoid the
			// possibility of data corruption.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: joiners_run_boot_methods: exiting "
			    "early because of unexpected exception");
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHED
		}

		set_booting_state(n);
		// we must call set_booting_state for all non-global zones
		// that are up on the node
		LogicalNode *ln = lnManager->findObject(n);
		CL_PANIC(ln != NULL);

		// Now iterate through the non-global zones
		std::list<LogicalNode*>::const_iterator it;
		for (it = ln->getLocalZones().begin();
		    it != ln->getLocalZones().end(); ++it) {
			if ((*it)->getStatus() == rgm::LN_UP) {
				set_booting_state((*it)->getLni());
				// signal failback threads that zone has joined
				ucmm_print("joiners_run_boot_methods",
				    NOGET("broadcast cond_zone_bootdelay"));
				cond_zone_bootdelay.broadcast();
			}
		}
	}
	return (RGMIDL_OK);
}


//
// slaves_record_membership
//
// This function is only called by the President in the big
// reconfiguration step.  The lock is already held.
// The President informs slaves that it is OK to overwrite their
// idea of the previous membership now, because we are far enough
// along in reconfiguration that another reconfiguration at this
// time won't hurt us.
// The function bails out early if an exception is encountered, because we
// expect another reconfiguration.
//
idlretval_t
slaves_record_membership(nodeset_t curr_ns)
{
	rgm::rgm_comm_var prgm_serv;
	Environment e;
	idlretval_t retval;
	sol::nodeid_t n;

	for (n = 1; n <= MAXNODES; n++) {

		// skip over non-members
		if (!nodeset_contains(curr_ns, n))
			continue;

		// skip myself because I already recorded the membership
		if (n == orb_conf::local_nodeid())
			continue;

		//
		// 'n' is in the current membership; Get reference
		// of node 'n'.
		//
		prgm_serv = rgm_comm_getref(n);
		//
		// Ensure that the object reference is not null.
		// If it is null, it means that this node died
		// while we are processing this reconfiguration.
		//
		if (CORBA::is_nil(prgm_serv)) {
			return (RGMIDL_NODE_DEATH);
		}
		prgm_serv->rgm_slave_record_memb(orb_conf::local_nodeid(),
		    Rgm_state->node_incarnations.members[orb_conf::
		    local_nodeid()], curr_ns, e);

		    FAULTPT_RGM(FAULTNUM_RGM_AFTER_RGM_SLAVE_RECORD_MEMB,
			FaultFunctions::generic);

		//
		// Handle exception;
		// return early if anything goes wrong.
		//
		if ((retval = except_to_errcode(e, n)) != RGMIDL_OK)
			return (retval);
	}

	return (RGMIDL_OK);
}
#endif // EUROPA_FARM

//
// wake_president
// -----------------
// Inform the President that I have completed orders.
// Send the President my nodeid so he can call me back and request state.
// The lock on the state structure should be held when this function is called
// on the president node.
// If I, myself, am the president, I call cond_broadcast to awaken any threads
// sleeping in a cond_wait.
//
// The president will not request state from us, because we have pushed all
// the state changes to it already.  Instead, the pres will just call
// cond_broadcast.
//
// However, a new president during reconfiguration will request state from
// us immediately if we specify an r/rg name of "fetch".  In this case,
// rgm_reconfig_helper() is holding the state mutex and is blocking on
// an IDL call to rgm_comm_I_am_president(), which will invoke this
// method on the slave side with fetch_immediate set to B_TRUE.
// Currently, this is the only case in which notify_me_state_change() is
// invoked with the NOTIFY_WAKE flag.  The code may be cleaned up later
// (see CR 6419313).
//
idlretval_t
wake_president(const rgm::lni_t lni, boolean_t fetch_immediate)
{
#ifdef EUROPA_FARM
	CLIENT *clnt;
	rgmx_notify_me_state_change_args rpc_args;
#else
	rgm::rgm_comm_var prgm_serv;
	Environment e;
#endif
	idlretval_t retval = RGMIDL_OK;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	sol::nodeid_t slave = lnNode->getNodeid();

#ifdef EUROPA_FARM
	clnt = rgmx_comm_getpres(ZONE);
	if (clnt == NULL) {
#else
	//
	// If I'm the President node, my internal state structure
	// is already updated, and the state mutex is held by the caller.
	// Broadcast the cond_slavewait condition variable.
	//
	if (slave == Rgm_state->rgm_President) {
		//
		// I'm the President.
		// send out signal to release cond_wait().
		//
		ucmm_print("RGM", NOGET("wake_president: call broadcast"));
		cond_slavewait.broadcast();

		return (RGMIDL_OK);
	}

	prgm_serv = rgm_comm_getref(Rgm_state->rgm_President);
	if (CORBA::is_nil(prgm_serv)) {
#endif
		return (RGMIDL_NOREF);
	}

	ucmm_print("RGM",
	    NOGET("prgm_serv->notify_me_state_change(NOTIFY_WAKE)"));

	//
	// Call notify_me_state_change with NOTIFY_WAKE tag on president.
	// With this tag, the states are ignored, so we pass dummy states.
	// The president will look at the r/rg name
	// in order to decide whether to fetch state immediately or not.
	// If the r/rg name is "fetch", then the pres will fetch state
	// immediately.  In this case the mutex is not held by this thread,
	// but by the rgm_reconfig_helper() thread further up the orb call
	// stack.  The call has gone from president to slave and back to the
	// president.
	//
#ifdef EUROPA_FARM
	rpc_args.change_tag = NOTIFY_WAKE;
	rpc_args.rstate = R_OFFLINE;
	rpc_args.rgstate = RG_OFFLINE;
#endif
	if (fetch_immediate) {
		//
		// This case is called only when a slave is uploading its
		// state to a new president.  In this case, the slave
		// passes its physical nodeid.
		//
		CL_PANIC(lni == slave);
#ifdef EUROPA_FARM
		rpc_args.lni = slave;
		rpc_args.R_RG_name = "fetch";
#else
		prgm_serv->notify_me_state_change(slave, rgm::NOTIFY_WAKE,
		    "fetch", rgm::R_OFFLINE, rgm::RG_OFFLINE, e);
#endif
	} else {
#ifdef EUROPA_FARM
		rpc_args.lni = lni;
		rpc_args.R_RG_name = "(none)";
#else
		prgm_serv->notify_me_state_change(lni, rgm::NOTIFY_WAKE,
		    "(none)", rgm::R_OFFLINE, rgm::RG_OFFLINE, e);
#endif
	}

#ifdef EUROPA_FARM
	if (rgmx_notify_me_state_change_1(&rpc_args, NULL, clnt)
	    != RPC_SUCCESS) {
		retval = RGMIDL_NODE_DEATH;
	} else
		retval = RGMIDL_OK;
	rgmx_comm_freecnt(clnt);
#else
	retval = except_to_errcode(e, Rgm_state->rgm_President);
#endif

	return (retval);
}


//
// notify_pres_state_change
//
// Inform the President of an "interesting" resource or RG state change.
// "Interesting" state changes are those that would affect the API's view
// of resource or RG state in a resource group that is switching online
// or offline (see bugid 4344345).
//
// "Interesting" resource state changes will also include those that involve
// inter-RG resource dependencies (see bugid 4340165).
//
// Called by set_rx_state or set_rgx_state, only from a slave that is not
// the president.
//
// The lock on the state structure is held when this function is called.
//
// If we can't get a reference to the president or if the idl call fails,
// just return.  The new president will fetch all state from slaves when
// it initializes.
//
// 'lni' is the lni of the slave zone on which the state change occurred.
// change_tag is rgm::NOTIFY_RES or rgm::NOTIFY_RG, indicating a resource
// or RG state change, respectively.
// r_rg_name is the name of the resource or RG, respectively.
// rstate is the new resource state for a resource update; it is ignored for
// RG update.
// rgstate is the new RG state for an RG update; it is ignored for resource
// update.
//
// If the suppress_notification flag is set, do not send the state change to
// the president.  The suppress_notification flag is set only when the
// president is about to fetch the total state from us.  We don't want
// to send incremental notifications, because the president would process
// them after processing the bulk state, which would lead to out-of-order
// state change processing.
//
void
notify_pres_state_change(
	rgm::lni_t lni,
	rgm::notify_change_tag change_tag,
	const char *r_rg_name,
	rgm::rgm_r_state rstate,
	rgm::rgm_rg_state rgstate)
{
#ifdef EUROPA_FARM
	CLIENT *clnt;
	rgmx_notify_me_state_change_args rpc_args;
#else
	rgm::rgm_comm_var prgm_serv;
	Environment e;
#endif

	if (Rgm_state->suppress_notification) {
		return;
	}

#ifdef EUROPA_FARM
	clnt = rgmx_comm_getpres(ZONE);
	if (clnt == NULL) {
#else
	prgm_serv = rgm_comm_getref(Rgm_state->rgm_President);
	if (CORBA::is_nil(prgm_serv)) {
#endif
		return;
	}

	ucmm_print("RGM", NOGET("prgm_serv->notify_me_state_change"));
#ifdef EUROPA_FARM
	rpc_args.lni = lni;
	rpc_args.change_tag = (notify_change_tag)change_tag;
	rpc_args.R_RG_name = strdup(r_rg_name);
	rpc_args.rstate = (rgm_r_state)rstate;
	rpc_args.rgstate = (rgm_rg_state)rgstate;

	(void) rgmx_notify_me_state_change_1(&rpc_args, NULL, clnt);

	free(rpc_args.R_RG_name);
	rgmx_comm_freecnt(clnt);
#else
	prgm_serv->notify_me_state_change(lni, change_tag, r_rg_name,
	    rstate, rgstate, e);

	// If there was an exception, clear it
	(void) except_to_errcode(e, Rgm_state->rgm_President);
#endif
}

#ifndef EUROPA_FARM
//
// except_to_errcode
//
// Map exceptions to error codes.
// We only handle COMM_FAILURE, INV_OBJREF, and zone_death (a
// user exception); we don't expect anything else and don't handle
// anything else.
// We need to pass a reference to the object
// for this to work properly.
//
// 'nodeid' is the nodeid of the remote node with which we were
// communicating when the exception occurred.
//
// IMPORTANT: Keep this function in sync with except_to_scha_err().
//
idlretval_t
except_to_errcode(Environment &e, sol::nodeid_t nodeid, rgm::lni_t lni)
{
	idlretval_t retval = RGMIDL_OK;
	sc_syslog_msg_handle_t handle;
	// Get the nodename based on nodeid
	char *nodename = rgm_get_nodename(nodeid);

	const char *zonename = "UNKNOWN ZONE";
	// get the zonename based on the lni
	if (lni != LNI_UNDEF) {
		LogicalNode *node = lnManager->findObject(lni);
		CL_PANIC(node != NULL);
		zonename = node->getZonename();
	}

	if (e.exception() == NULL) {
		free(nodename);
		return (RGMIDL_OK);
	}

	if (CORBA::COMM_FAILURE::_exnarrow(e.exception())) {
		// node death
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// An inter-node communication failed because another cluster
		// node died.
		// @user_action
		// No action is required. The cluster will reconfigure
		// automatically. Examine syslog output on the rebooted node
		// to determine the cause of node death.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("remote node %s died"), nodename);
		sc_syslog_msg_done(&handle);
		retval = RGMIDL_NODE_DEATH;
	} else if (CORBA::INV_OBJREF::_exnarrow(e.exception())) {
		// rgmd death
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// An inter-node communication failed because the rgmd died on
		// another node. To avoid data corruption, the failfast
		// mechanism will cause that node to halt or reboot.
		// @user_action
		// No action is required. The cluster will reconfigure
		// automatically. Examine syslog output on the rebooted node
		// to determine the cause of node death. The syslog output
		// might indicate further remedial actions.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("rgmd died on node %s"), nodename);
		sc_syslog_msg_done(&handle);
		retval = RGMIDL_RGMD_DEATH;
	} else if (rgm::zone_died::_exnarrow(e.exception())) {
		// zone death or death and reboot
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		if (strcmp(ZONE, GLOBAL_ZONENAME) == 0) {
			//
			// SCMSGS
			// @explanation
			// The RGM has detected that the indicated zone
			// has halted or rebooted. If any resource groups
			// were online in that zone, they might fail over
			// to alternate zones or nodes.
			// @user_action
			// This is an informational message; no user action
			// is needed.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("zone %s died on node %s"), zonename,
			    nodename);
		} else {
			//
			// SCMSGS
			// @explanation
			// The RGM has detected that the indicated zone
			// cluster node has halted or rebooted. If any
			// resource groups were online in that zone cluster
			// node, they might fail over to other nodes of the
			// zone cluster.
			// @user_action
			// This is an informational message; no user action
			// is needed.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("node %d of zone cluster %s died"),
			    lni, ZONE);
		}
		sc_syslog_msg_done(&handle);
		retval = RGMIDL_NODE_DEATH;
	} else {
		// unknown, unexpected
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// An inter-node communication failed with an unknown
		// exception.
		// @user_action
		// Examine syslog output for related error messages. Save a
		// copy of the /var/adm/messages files on all nodes, and of
		// the rgmd core file (if any). Contact your authorized Sun
		// service provider for assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("got unexpected exception %s"),
		    e.exception()->_name());
		sc_syslog_msg_done(&handle);
		retval = RGMIDL_UNKNOWN;
	}

	e.clear();
	free(nodename);
	return (retval);
}

//
// call_ack_start
// --------------
// Helper function calls the rgm_ack_start idl method on the slave node
// specified.
//
idlretval_t
call_ack_start(rgm::lni_t lni, const char *r_name)
{
	rgm::rgm_comm_var prgm_serv;
	Environment e;
	idlretval_t retval = RGMIDL_OK;
	bool is_farmnode;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	sol::nodeid_t slave = lnNode->getNodeid();

	if ((rgmx_hook_call(rgmx_hook_ack_start,
		slave, lni, lnNode->getIncarnation(), r_name,
		&retval, &is_farmnode) == RGMX_HOOK_ENABLED) &&
		(is_farmnode)) {
		return (retval);
	}

	prgm_serv = rgm_comm_getref(slave);
	if (CORBA::is_nil(prgm_serv)) {
		return (RGMIDL_NOREF);
	}

	ucmm_print("RGM",
	    NOGET("prgm_serv->rgm_ack_start node %d lni %d res %s"),
	    slave, lni, r_name);

	prgm_serv->rgm_ack_start(orb_conf::local_nodeid(),
	    Rgm_state->node_incarnations.members[orb_conf::local_nodeid()],
	    lni, lnNode->getIncarnation(), r_name, e);

	// Handle exception
	retval = except_to_errcode(e, slave, lni);

	return (retval);
}


//
// call_notify_dependencies_resolved
// ----------------------------------
// Helper function calls the rgm_notify_dependencies_resolved idl call
// on the slave zone specified.
//
idlretval_t
call_notify_dependencies_resolved(rgm::lni_t lni, const char *r_name)
{
	rgm::rgm_comm_var prgm_serv;
	Environment e;
	idlretval_t retval = RGMIDL_OK;
	bool is_farmnode;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	sol::nodeid_t slave = lnNode->getNodeid();

	if ((rgmx_hook_call(rgmx_hook_notify_dependencies_resolved,
		slave, lni, lnNode->getIncarnation(), r_name,
		&retval, &is_farmnode) == RGMX_HOOK_ENABLED) &&
		(is_farmnode)) {
		return (retval);
	}

	prgm_serv = rgm_comm_getref(slave);
	if (CORBA::is_nil(prgm_serv)) {
		return (RGMIDL_NOREF);
	}

	ucmm_print("RGM", NOGET("prgm_serv->rgm_notify_dependencies_resolved"
	    "node %d lni %d res %s"), slave, lni, r_name);

	prgm_serv->rgm_notify_dependencies_resolved(orb_conf::local_nodeid(),
	    Rgm_state->node_incarnations.members[orb_conf::local_nodeid()],
	    lni, lnNode->getIncarnation(), r_name, e);

	// Handle exception
	retval = except_to_errcode(e, slave, lni);

	return (retval);
}

//
// call_fetch_restarting_flag
// ---------------------------
// Helper function to call the fetch_restarting_flag idl call on the
// slave specified.  If the call goes ok, sets restart_flag to the value
// of the flag retrieved from the call.  Otherwise, sets restart_flag to
// RR_NONE.
//
idlretval_t
call_fetch_restarting_flag(rgm::lni_t lni, const char *r_name,
    rgm::r_restart_t &restart_flag)
{
	rgm::rgm_comm_var prgm_serv;
	Environment e;
	idlretval_t retval = RGMIDL_OK;
	rgm::r_restart_t flag_param = rgm::RR_NONE;
	bool is_farmnode;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	sol::nodeid_t slave = lnNode->getNodeid();

	// Our default return, if something goes wrong, is RR_NONE.
	restart_flag = rgm::RR_NONE;

	if ((rgmx_hook_call(rgmx_hook_fetch_restarting_flag,
		slave, lni, lnNode->getIncarnation(),
		r_name, &flag_param,
		&retval, &is_farmnode) == RGMX_HOOK_ENABLED) &&
		(is_farmnode)) {
		if (retval == RGMIDL_OK) {
			restart_flag = flag_param;
		}
		return (retval);
	}

	prgm_serv = rgm_comm_getref(slave);
	if (CORBA::is_nil(prgm_serv)) {
		return (RGMIDL_NOREF);
	}

	ucmm_print("RGM", NOGET("prgm_serv->rgm_fetch_restarting_flag"
	    "node %d lni %d res %s"), slave, lni, r_name);

	prgm_serv->rgm_fetch_restarting_flag(orb_conf::local_nodeid(),
	    Rgm_state->node_incarnations.members[orb_conf::local_nodeid()],
	    lni, lnNode->getIncarnation(), r_name, flag_param, e);

	// Handle exception
	retval = except_to_errcode(e, slave, lni);

	// Only set the flag from the flag param if everything went ok.
	// Otherwise, leave it at RR_NONE.
	if (retval == RGMIDL_OK) {
		restart_flag = flag_param;
	}

	return (retval);
}



//
// register_president
//
// Register the president into the nameserver under the name RGM_PRESIDENT.
// This is done so that the lower level API can make direct calls into
// the RGM President without having to go thru the RPC, receptionist and
// then the ORB and all the way back the same way.
//
void
register_president(void)
{
	sol::nodeid_t nid;
	rgm::rgm_comm_var rgm_comm_server_obj_var;
	naming::naming_context_var ctxp;
	Environment e;
	sc_syslog_msg_handle_t handle;
	char pres_string[100];

	nid = orb_conf::local_nodeid();
	sprintf(pres_string, "RGM_PRESIDENT");

	rgm_comm_server_obj_var = rgm_comm_getref(nid);
	if (CORBA::is_nil(rgm_comm_server_obj_var)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The low-level cluster machinery has encountered a fatal
		// error. The rgmd will produce a core file and will cause the
		// node to halt or reboot to avoid the possibility of data
		// corruption.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes,
		// and of the rgmd core file. Contact your authorized Sun
		// service provider for assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: register_president: "
		    "Don't have reference to myself");
		sc_syslog_msg_done(&handle);
		abort();
	}
	// Get a pointer to the global nameserver object

#if SOL_VERSION >= __s10
	if (process_membership_support_available()) {
		ctxp = ns::root_nameserver_cz(ZONE);
	} else {
		ctxp = ns::root_nameserver();
	}
#else
	ctxp = ns::root_nameserver();
#endif
	if (CORBA::is_nil(ctxp)) {
		//
		// Need to decide what to do for
		// release code. For debug code, assert
		//
		ASSERT(0);
	}

	// bind the new object with the nametag
	ctxp->rebind(pres_string, rgm_comm_server_obj_var, e);
	if (e.exception() != NULL) {
		e.clear();
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The low-level cluster machinery has encountered a fatal
		// error. The rgmd will produce a core file and will cause the
		// node to halt or reboot to avoid the possibility of data
		// corruption.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes,
		// and of the rgmd core file. Contact your authorized Sun
		// service provider for assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: Unable to bind president to nameserver");
		sc_syslog_msg_done(&handle);
		abort();
	}
}
#endif // EUROPA_FARM


//
// rgm_comm_setfmstatus()
//
// This function is a wrapper for either setfmstatus_helper (if we're
// a slave node) or for queuing a status change task (if we're the president).
// All status changes are done asynchronously on the president through
// a status change threadpool.
//
// It does some basic sanity checking, and converts the char *status
// to an enum.
//
scha_errmsg_t
rgm_comm_setfmstatus(char *rs_name, char *rg_name, char *node_name,
			char *fmstatus, char *status_msg)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm::lni_t	lni;
	rfmstatusname_t *status_tbl;
	int	status;
	char	*endp;

#ifndef EUROPA_FARM
	notify_status_change_task *notify_task = NULL;
#endif

#ifdef EUROPA_FARM
	lni = get_local_nodeid();
#else
	if (node_name == NULL || node_name[0] == '\0') {
		// Local node
		lni = orb_conf::local_nodeid();
	} else {
		lni = rgm_get_lni(node_name);
		if (lni == LNI_UNDEF) {
			res.err_code = SCHA_ERR_NODE;
			goto finished; //lint !e801
		}
	}
#endif

	if (rs_name == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		goto finished; //lint !e801
	}

	if (rg_name == NULL || rg_name[0] == '\0') {
		res.err_code = SCHA_ERR_RG;
		goto finished; //lint !e801
	}

	//
	// convert fm status from string to int
	//
	status = (int)strtol(fmstatus, &endp, 0);
	if (*endp != '\0') {
		ucmm_print("RGM",
		    NOGET("rgm_comm_setfmstatus(): strtol error \n"));
		res.err_code = SCHA_ERR_INTERNAL;
		goto finished; //lint !e801
	}

	if ((status_tbl = get_rfmstatus((rgm::rgm_r_fm_status)-1,
	    NULL, (scha_rsstatus_t)status)) == NULL) {
		ucmm_print("RGM",
		    NOGET("rgm_comm_setfmstatus(): Invalid FM status <%d>\n"),
		    status);
		res.err_code = SCHA_ERR_RSTATUS;
		goto finished; //lint !e801
	}

	if (am_i_president()) {
#ifdef EUROPA_FARM
		ASSERT(0);
#else
		//
		// Queue this status change on the status change threadpool.
		//
		notify_task = new notify_status_change_task(
		    lni, rs_name, rg_name, status_tbl->rgm_val, status_msg);
		notify_status_change_threadpool::the().defer_processing(
		    notify_task);
		// notify_task will be deleted by the thread that processes it.
#endif // EUROPA_FARM
	} else {
		//
		// Process the status change directly
		//
		res = setfmstatus_helper(lni, rs_name, rg_name,
		    status_tbl->rgm_val, status_msg);
	}

finished:

	return (res);
}

//
// setfmstatus_helper()
//
//	This function perform the updating of R FM status in the internal
//	R state struture in memory
//	This function grabs the lock on state structure before updating
//	R FM status and holds the lock until the operation is done.
//
scha_errmsg_t
setfmstatus_helper(rgm::lni_t lni, const char *rs_name,
    const char *rg_name, rgm::rgm_r_fm_status status,
    const char *status_msg)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t	rgl;
	rlist_p_t	rl;
#ifndef EUROPA_FARM
	char	*event_status_message = NULL, *old_status_msg = NULL;
	rgstatename_t		*old_rgstate, *new_rgstate;
	const char *nodename = NULL;
#endif

	rgm_lock_state();

	rgl = rgname_to_rg(rg_name);
	if (rgl != NULL) {
		rl = rname_to_r(rgl, rs_name);
		if (rl == NULL) {
			ucmm_print("RGM",
			    NOGET("RS <%s> is not in memory"), rs_name);
			res.err_code = SCHA_ERR_RSRC;
			goto finished; //lint !e801
		}
	} else {
		ucmm_print("RGM",
		    NOGET("RG <%s> is not in memory"), rg_name);
		res.err_code = SCHA_ERR_RG;
		goto finished; //lint !e801
	}

	//
	// Retrieve the old API representation of the RG state.
	// We must make this call before we set the new resource status,
	// because that could affect the API visible RG state (specifically
	// regarding ONLINE_FAULTED).
	//
#ifndef EUROPA_FARM
	old_rgstate = get_rgstate(rgl->rgl_xstate[lni].rgx_state,
	    NULL, (scha_rgstate_t)-1, rgl, lni);
#endif


	//
	// do syslog only in the case where the slave itself is the president;
	// and then only call syslog if the status or message was changed from
	// its previous value
	//
	if (am_i_president()) {
#ifdef EUROPA_FARM
		ASSERT(0);
#else
		old_status_msg = rl->rl_xstate[lni].rx_fm_status_msg;

		nodename = (lnManager->findObject(lni))->getFullName();

		syslog_fmstatus_msg((char *)rs_name, (char *)nodename,
		    rl->rl_xstate[lni].rx_fm_status, status,
		    old_status_msg, (char *)status_msg);

		//
		// Post an event if the old status and the new
		// status are different or if the old message and
		// new message are different.
		//
		// Note that either or both of the old and new status
		// messages might be NULL.
		//
		if (rl->rl_xstate[lni].rx_fm_status != status ||
		    old_status_msg == NULL && status_msg != NULL ||
		    old_status_msg != NULL && status_msg == NULL ||
		    (old_status_msg != NULL && status_msg != NULL &&
		    strcmp(old_status_msg, status_msg) != 0)) {

			if (status_msg == NULL)
				event_status_message = strdup("null");
			else
				event_status_message = strdup(status_msg);


			//
			// rgm_comm_setfmstatus is called on the slave;
			// if the slave is not the president, it is called
			// a second time on the president.  Since we do not
			// want to post the event twice, we post it only when
			// we are called on the president.
			//

#if SOL_VERSION >= __s10
			(void) sc_publish_zc_event(ZONE,
			    ESC_CLUSTER_FM_R_STATUS_CHANGE,
			    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_AGENT, 0, 0, 0,
			    CL_R_NAME, SE_DATA_TYPE_STRING, rs_name,
			    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
			    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
			    CL_OLD_STATUS, SE_DATA_TYPE_STRING,
			    pr_rfmstatus(rl->rl_xstate[lni].rx_fm_status),
			    CL_NEW_STATUS, SE_DATA_TYPE_STRING,
			    pr_rfmstatus(status),
			    CL_STATUS_MSG, SE_DATA_TYPE_STRING,
			    event_status_message, NULL);
#else
			(void) sc_publish_event(ESC_CLUSTER_FM_R_STATUS_CHANGE,
			    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_AGENT, 0, 0, 0,
			    CL_R_NAME, SE_DATA_TYPE_STRING, rs_name,
			    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
			    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename,
			    CL_OLD_STATUS, SE_DATA_TYPE_STRING,
			    pr_rfmstatus(rl->rl_xstate[lni].rx_fm_status),
			    CL_NEW_STATUS, SE_DATA_TYPE_STRING,
			    pr_rfmstatus(status),
			    CL_STATUS_MSG, SE_DATA_TYPE_STRING,
			    event_status_message, NULL);
#endif
			free(event_status_message);
		}
#endif // EUROPA_FARM
	}

	rl->rl_xstate[lni].rx_fm_status = status;

#ifndef EUROPA_FARM
	//
	// Retrieve the new API representation of the RG state.
	//
	new_rgstate = get_rgstate(rgl->rgl_xstate[lni].rgx_state,
	    NULL, (scha_rgstate_t)-1, rgl, lni);

	//
	// If there has been a change in the API visible state of
	// the RG to or from ONLINE_FAULTED, generate a sysevent.
	// We must generate the event here because ONLINE_FAULTED is a
	// "fake" state, and will not be set anywhere explicitly.
	// We generate the state only if we are the president. If we are
	// a slave, the president will process this status also and
	// will generate the event at that point.
	//
	if (old_rgstate->api_val != new_rgstate->api_val &&
	    (old_rgstate->api_val == SCHA_RGSTATE_ONLINE_FAULTED ||
	    new_rgstate->api_val == SCHA_RGSTATE_ONLINE_FAULTED)) {
		if (am_i_president()) {
			res = post_event_rg_state(rg_name);
			if (res.err_msg)
				free(res.err_msg);
		}
	}
#endif

	if (rl->rl_xstate[lni].rx_fm_status_msg != NULL)
		free(rl->rl_xstate[lni].rx_fm_status_msg);

	if (status_msg == NULL)
		rl->rl_xstate[lni].rx_fm_status_msg = NULL;
	else
		rl->rl_xstate[lni].rx_fm_status_msg = strdup(status_msg);

finished:

	rgm_unlock_state();

	return (res);
}

//
// flush_status_change_queue
// ------------------------
// Caller should not hold the lock.  Caller must release the lock before
// calling this function, otherwise this function will deadlock.
//
// All this function does is call the quiesce method on the status change
// threadpool, with the QFENCE option (which will process everything currently
// in the queue, will allow new tasks to be queued, but will not process
// those new tasks).
//
void
flush_status_change_queue()
{
#ifndef EUROPA_FARM
	ucmm_print("RGM", NOGET("Beginning flush_status_change_queue\n"));
	notify_status_change_threadpool::the().quiesce(threadpool::QFENCE);
	ucmm_print("RGM", NOGET("Ending flush_status_change_queue\n"));
#endif
}

#ifndef EUROPA_FARM
//
// rgm_comm_getfmstatus()
//
//	This function retrieves R FM status in the internal
//	state struture in memory and returns the
//	FM status and the status messages to the caller
//	This function grabs the lock on state structure before updating
//	R FM status and holds the lock until the operation is done.
//
scha_errmsg_t
rgm_comm_getfmstatus(char *rs_name, char *rg_name, char *node_name,
	scha_rsstatus_t *status, char **status_msg, char **seq_id)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm::lni_t lni;
	rfmstatusname_t	*status_tbl;
	rglist_p_t	rgl;
	rlist_p_t	rl;


	//
	// Flush the status change queue to make sure we have the latest
	// state before grabbing the lock.  It will be a no-op if we're
	// not the president (only the pres uses the status change queue).
	//
	flush_status_change_queue();
	rgm_lock_state();

	if (node_name == NULL || node_name[0] == '\0') {
		// don't need to call in_current_membership()
		// because I know I'm a current member
		lni = orb_conf::local_nodeid();
	} else {
		lni = rgm_get_lni(node_name);
		if (lni == LNI_UNDEF) {
			res.err_code = SCHA_ERR_NODE;
			goto finished; //lint !e801
		}

		//
		// if target node is not a current member, that is OK.
		// The resource status on that node has already been reset
		// to OFFLINE, and the status message reset to the empty
		// string, in president's memory.
		// So just proceed and report that status back to the caller.
		//
	}

	if (rs_name == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		goto finished; //lint !e801
	}

	if (rg_name == NULL || rg_name[0] == '\0') {
		if ((rl = rname_to_r(NULL, rs_name)) == NULL) {
			res.err_code = SCHA_ERR_RSRC;
			goto finished; //lint !e801
		}
	} else {
		rgl = rgname_to_rg(rg_name);
		if (rgl != NULL) {
			rl = rname_to_r(rgl, rs_name);
			if (rl == NULL) {
				ucmm_print("RGM", NOGET(
				    "RS <%s> is not in memory"), rs_name);
				res.err_code = SCHA_ERR_RSRC;
				goto finished; //lint !e801
			}
		} else {
			ucmm_print("RGM",
			    NOGET("RG <%s> is not in memory"), rg_name);
			res.err_code = SCHA_ERR_RG;
			goto finished; //lint !e801
		}
	}

	if ((status_tbl = get_rfmstatus
	    (rl->rl_xstate[lni].rx_fm_status,
	    NULL, (scha_rsstatus_t)-1)) == NULL) {
		ucmm_print("RGM", NOGET("Invalid R status in memory"));
		res.err_code = SCHA_ERR_INTERNAL;
		return (res);
	}

	*status = status_tbl->api_val;
	if (rl->rl_xstate[lni].rx_fm_status_msg == NULL) {
		*status_msg = NULL;
	} else
		*status_msg = rl->rl_xstate[lni].rx_fm_status_msg;

	// Since all Rs in one RG share the same seq_id.
	*seq_id = rl->rl_rg->rgl_ccr->rg_stamp;

	ucmm_print("RGM", NOGET("rgm_comm_getfmstatus: get FM status <%s>"),
	    status_tbl->statusname);
finished:

	rgm_unlock_state();

	return (res);
}

//
// rgm_comm_getrsstate()
//
//	This function retrieves R state in the internal
//	state struture in memory and returns the
//	R state to the caller.
//	This function grabs the lock on state structure at the beginning
//	of this function and holds the lock until the operation is done.
//
scha_errmsg_t
rgm_comm_getrsstate(char *rs_name, char *rg_name, char *node_name,
			scha_rsstate_t *state, char **seq_id)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm::lni_t	lni;
	rstatename_t	*state_tbl;
	rglist_p_t	rgl;
	rlist_p_t	rl;

	//
	// First make sure we have all the latest state change info
	// processed.
	//
	flush_state_change_queue();
	rgm_lock_state();


	if (node_name == NULL || node_name[0] == '\0') {
		// don't need to call in_current_membership()
		// because I know I'm a current member
		lni = orb_conf::local_nodeid();
	} else {
		lni = rgm_get_lni(node_name);
		if (lni == LNI_UNDEF) {
			res.err_code = SCHA_ERR_NODE;
			goto finished; //lint !e801
		}

		//
		// if target node is not a current member, that is OK.
		// The resource state on that node has already been reset to
		// OFFLINE in president's memory.  So just proceed and
		// report that state back to the caller.
		//
	}
	if (rs_name == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		goto finished; //lint !e801
	}

	if (rg_name == NULL || rg_name[0] == '\0') {
		if ((rl = rname_to_r(NULL, rs_name)) == NULL) {
			res.err_code = SCHA_ERR_RSRC;
			goto finished; //lint !e801
		}
	} else {
		rgl = rgname_to_rg(rg_name);
		if (rgl != NULL) {
			rl = rname_to_r(rgl, rs_name);
			if (rl == NULL) {
				ucmm_print("RGM", NOGET("rgm_comm_getstate: "
				    "RS <%s> is not in memory"), rs_name);
				res.err_code = SCHA_ERR_RSRC;
				goto finished; //lint !e801
			}
		} else {
			ucmm_print("RGM", NOGET(
			    "rgm_comm_getstate: RG <%s> is not in memory"),
			    rg_name);
			res.err_code = SCHA_ERR_RG;
			goto finished; //lint !e801
		}
	}

	if ((state_tbl = get_rstate(rl->rl_xstate[lni].rx_state,
	    NULL, (scha_rsstate_t)-1)) == NULL) {
		ucmm_print("RGM",
		    NOGET("get_comm_getstate: Invalid R state"));
		res.err_code = SCHA_ERR_INTERNAL;
		goto finished; //lint !e801
	}

	*state = state_tbl->api_val;
	*seq_id = rl->rl_rg->rgl_ccr->rg_stamp;
	ucmm_print("RGM", NOGET("rgm_comm_getrsstate: went OK"));

finished:

	rgm_unlock_state();

	return (res);
}

//
// rgm_comm_getfailedstatus()
//
//	This function retrieves the status of failed FINI, INIT, BOOT or UPDATE
//	method.
//	This function grabs the lock on state structure at the beginning
//	of this function and holds the lock until the operation is done.
//
scha_errmsg_t
rgm_comm_getfailedstatus(char *rs_name, char *rg_name, char *node_name,
	char *method_name, boolean_t *failstatus, char **seq_id)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm::lni_t	lni;
	rglist_p_t	rgl;
	rlist_p_t	rl;

	rgm_lock_state();

	if (node_name == NULL || node_name[0] == '\0') {
		// don't need to call in_current_membership()
		// because I know I'm a current member
		lni = orb_conf::local_nodeid();
	} else {
		lni = rgm_get_lni(node_name);
		if (lni == LNI_UNDEF) {
			res.err_code = SCHA_ERR_NODE;
			goto finished; //lint !e801
		}

		//
		// if target node is not a current member, that is OK.
		// The "failed" status on that node is either retained by
		// the president through node reboots, or is reset by the
		// president when the node leaves the cluster.  So just
		// proceed and report that status back to the caller.
		//
	}

	if (method_name == NULL) {
		res.err_code = SCHA_ERR_METHODNAME;
		goto finished; //lint !e801
	}

	if (rs_name == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		goto finished; //lint !e801
	}

	if (rg_name == NULL || rg_name[0] == '\0') {
		if ((rl = rname_to_r(NULL, rs_name)) == NULL) {
			res.err_code = SCHA_ERR_RSRC;
			goto finished; //lint !e801
		}
	} else {
		rgl = rgname_to_rg(rg_name);
		if (rgl != NULL) {
			rl = rname_to_r(rgl, rs_name);
			if (rl == NULL) {
				ucmm_print("RGM",
				    NOGET("rgm_comm_getfailstatus: "
				    "RS <%s> is not in memory"), rs_name);
				res.err_code = SCHA_ERR_RSRC;
				goto finished; //lint !e801
			}
		} else {
			ucmm_print("RGM", NOGET("rgm_comm_getfailstatus: "
			    "RG <%s> is not in memory"), rg_name);
			res.err_code = SCHA_ERR_RG;
			goto finished; //lint !e801
		}
	}

	//
	// XXX 4215776 Implement FYI states in rgmd
	// XXX until implemented, just report "true".
	// XXX If we end up not implementing the FYI states in 3.0, these
	// XXX optags (UPDATE_FAILED, INIT_FAILED, FINI_FAILED, BOOT_FAILED)
	// XXX should be disabled in the API.
	//
	*failstatus = B_TRUE;
	*seq_id = rl->rl_rg->rgl_ccr->rg_stamp;
	ucmm_print("RGM", NOGET("rgm_comm_getfailedstatus: went OK"));

finished:

	rgm_unlock_state();

	return (res);
}

//
// rgm_comm_getrgstate()
//
//	This function retrieves RG state from the internal
//	state struture in memory and returns it to the caller.
//	This function grabs the lock on state structure at the beginning
//	of this function and holds the lock until the operation is done.
//
scha_errmsg_t
rgm_comm_getrgstate(char *rg_name, char *node_name,
			scha_rgstate_t *rgstate, char **seq_id)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm::lni_t lni;
	rgstatename_t	*rgstate_tbl;
	rglist_p_t	rgl;

	//
	// First make sure we have all the latest state change info
	// processed.
	//
	flush_state_change_queue();
	rgm_lock_state();


	if (node_name == NULL || node_name[0] == '\0') {
		// don't need to call in_current_membership()
		// because I know I'm a current member
		lni = orb_conf::local_nodeid();
	} else {
		lni = rgm_get_lni(node_name);
		if (lni == LNI_UNDEF) {
			res.err_code = SCHA_ERR_NODE;
			goto finished; //lint !e801
		}

		//
		// if target node is not a current member, that is OK.
		// The RG state on that node has already been reset to
		// OFFLINE in president's memory.  So just proceed and
		// report that state back to the caller.
		//
	}

	rgl = rgname_to_rg(rg_name);
	if (rgl == NULL) {
		ucmm_print("RGM", NOGET("rgm_comm_getrgstate: "
		    "RG <%s> is not in memory"), rg_name);
		res.err_code = SCHA_ERR_RG;
		goto finished; //lint !e801
	}

	if ((rgstate_tbl = get_rgstate(rgl->rgl_xstate[lni].rgx_state,
	    NULL, (scha_rgstate_t)-1, rgl, lni)) == NULL) {
		ucmm_print("RGM",
		    NOGET("get_comm_getrgstate: Invalid RG state"));
		res.err_code = SCHA_ERR_INTERNAL;
		goto finished; //lint !e801
	}

	// If the RG is unmanaged, scha_resourcegroup_get returns "unmanaged"
	// as the state.  Otherwise return the actual state.
	if (rgl->rgl_ccr->rg_unmanaged) {
		*rgstate = SCHA_RGSTATE_UNMANAGED;
	} else {
		*rgstate = rgstate_tbl->api_val;
	}
	*seq_id = rgl->rgl_ccr->rg_stamp;
	ucmm_print("RGM", NOGET("rgm_comm_getrgstate: went OK"));

finished:

	rgm_unlock_state();

	return (res);
}
#endif // EUROPA_FARM
