//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_logical_nodeset.cc	1.5	08/08/16 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include <rgm_logical_nodeset.h>
#include <rgm_proto.h>

#define	PRIME_AFTER_32 37

#define	LOG2_BITSINWORD	5	// log2(32)

//
// Static data holding the maxmimum number of physical nodes supported.
// On a vanilla Sun Cluster, the maximum supported number of nodes is set
// to MAXNODES (i.e. 64).
// On a Europa cluster, the maximum number of physical nodes is the sum of
// the maximum number of Server nodes (i.e. MAXNODES) and the maximum number
// of Farm nodes (i.e. MAXFARMNODES).
//
rgm::lni_t LogicalNodeset::theMaxNode = MAXNODES;

//
// Initial maximum number of LNs.
// When the current quota is exhausted, the number is increased on creation
// of the next LN.  Number should be a multiple of the LogicalNodeset
// word size (currently 32).  Theoretically, this could be as low as 32
// to start with, since we will grow it dynamically.  However, it might
// as well start at a larger value since
// we currently pre-allocate theMaxNode LNs for physical nodes --
// see LogicalNodeManager::initialize().
//
#define	INITIAL_MAX_LNI	128;

//
// This static data member holds the highest LNI that is currently
// allocatable.  This number is increased dynamically when the current
// limit of LNIs is exhausted.  The value can be queried by the maxLni()
// static method of this class.
// It can be increased by calling the constructor which takes a new
// maxLni value.  Only arrayFreeList uses this constructor, on behalf
// of lnManager.
//
// All new LogicalNodesets will initially be of this size, including the
// free_list of the intial lnManager object.
//
rgm::lni_t LogicalNodeset::theMaxLni = INITIAL_MAX_LNI;

//
// Computed so that ((val) & (-(val))) % PRIME_AFTER_32] returns the first
// bit set. For example, with val = 4:
// => val = 4  (0100) => (-val) = (1100) => (val) & (-val) = 0100,
//    (val & (-val)) % 37 = 4, and we can verify that bitStrTable[4] = 2,
//    which corresponds to the first bit set in '4'.
//
static const int bitStrTable[PRIME_AFTER_32] = {
	-1, 0, 1, 26, 2, 23, 27, -1, 3, 16, 24, 30, 28, 11, -1, 13, 4, 7, 17,
	-1, 25, 22, 31, 15, 29, 10, 12, 6, -1, 21, 14, 9, 5, 20, 8, 19, 18
};

//
// bitScanf32 gives the location of the least significant bit that is set,
// ranging from 0 to 31.
//
// . Note that (val & (-val)) chops all bits except the least significant one
// . '37' is the first prime above 32, producing a perfect hash
// . The value of (-val) is undefined if (val) happens to equal the maximum
//	negative int (i.e., 0x80000000) which is a valid uint32_t value.
//	To avoid this problem, the argument (val) should be cast to an int type
//	which is larger than BITSPERWORD.  I.e., use int64_t.
//
#define	bitScanf32(val) (bitStrTable[((val) & (-(val))) % PRIME_AFTER_32])

//
// Reset all bits in 'words' to 0.
//
void
LogicalNodeset::reset()
{
	for (uint_t word = 0; word < nbWords; word++) {
		words[word] = 0;
	}
}


//
// Find the first set bit starting at the given word offset.
// Returns -1 if not found.
//
int
LogicalNodeset::firstSet(const uint_t startOffset) const
{
	uint32_t wd;
	uint32_t *w = &words[startOffset];
	int n = (int)(nbWords - startOffset);
	int firstbit;

	while (--n >= 0 && *w++ == 0)
		continue;

	if (n < 0)
		return (-1);

	wd = *(w - 1);

	firstbit = bitScanf32((int64_t)wd);
	return (firstbit == -1 ? -1 : (w - &words[1]) * BITSPERWORD + firstbit);
}

//
// Find the first clear bit starting at the given word offset.
// Returns -1 if not found.
// Correctly handles the case where the size of the LogicalNodeset
// is less than the current theMaxLni.
//
int
LogicalNodeset::firstClr(const uint_t startOffset) const
{
	uint32_t wd;
	uint32_t *w = (uint32_t*)(words + startOffset);
	int n = (int)nbWords - (int)startOffset;
	int firstbit;

	while (--n >= 0 && *w++ == (uint32_t)0xffffffff)
		continue;

	if (n < 0) {
		//
		// w is "off the end" of the words array -- i.e., the
		// existing words (up to nbWords) are all '1's.
		// If the words array is smaller than the current default size,
		// return the bit position of the first bit of word 'w'.
		// Otherwise, return -1 indicating 'not found'.
		//
		if (nbWords < NBWORDS(theMaxLni)) {
			return ((w - &words[0]) * BITSPERWORD + 1);
		} else {
			return (-1);
		}
	}

	wd = ~(*(w - 1));

	firstbit = bitScanf32((int64_t)wd);
	return (firstbit == -1 ? -1 : (w - &words[1]) * BITSPERWORD + firstbit);
}


//
// Sets the bit at the given location.
// Caller must guarantee that the current size of 'words' is sufficient to
// hold the bit.  Currently, called only by addLni().
//
void
LogicalNodeset::setBit(const uint_t indx)
{
	uint_t word  = (indx >> LOG2_BITSINWORD);
	uint_t bit   = indx & 0x1f;
	uint_t value = 1 << bit;

	words[word] |= value;
}


//
// Clears the bit at the given location
// Caller must guarantee that the current size of 'words' is sufficient to
// hold the bit.  Currently, called only by delLni().
//
void
LogicalNodeset::clrBit(const uint_t indx)
{
	uint_t word  = (indx >> LOG2_BITSINWORD);
	uint_t bit   = indx & 0x1f;
	uint_t value = 1 << bit;

	words[word] &= ~value;
}


//
// Tests if a given bit location is set or cleared
// Caller must guarantee that the current size of 'words' is sufficient to
// hold the bit -- we don't repeat that check here.
// Returns 0 if bit is clear, and 1 if bit is set.
//
int
LogicalNodeset::tstBit(const uint_t indx) const
{
	uint_t word  = (indx >> LOG2_BITSINWORD);
	uint_t bit   = indx & 0x1f;	// assumes 32-bit word
	uint_t value = 1 << bit;

	return ((words[word] & value) ? 1 : 0);
}


//
// Return the first cleared bit after a given bit location.
// Caller must guarantee that indx <= maxLni().
// Returns -1 if no bit is cleared from the given position to the end of words.
//
// This method could be used to implement a nextLniClear(), analagous
// to nextLniSet().  This is currently not implemented.
//
int
LogicalNodeset::nextClr(const uint_t indx) const
{
	uint_t index2;
	uint_t indx_words = NBWORDS(indx + 1);

	if (indx_words >= nbWords) {
		//
		// We're "off the end" of the words array, so this bit
		// is implicitly zero.  Return it.
		//
		return (indx);
	}
	for (index2 = indx; index2 < indx_words * BITSPERWORD; ++index2) {
		if (tstBit(index2) == 0) {
			return ((int)index2);
		}
	}
	return (firstClr(NBWORDS(index2)));
}

//
// Return the next bit set, starting at bit location 'indx' (0 based).
// Caller must guarantee that the bit 'indx' does not fall outside of the
// current size of the 'words' array -- we don't repeat that check here.
// Currently, called only by nextLniSet().
//
// Returns -1 if no bit is set from the given position to the end of words.
//
int
LogicalNodeset::nextSet(const uint_t indx) const
{
	uint_t index2;
	uint_t indx_words = NBWORDS(indx + 1);

	for (index2 = indx; index2 < indx_words * BITSPERWORD; ++index2) {
		if (tstBit(index2) == 1) {
			return ((int)index2);
		}
	}
	return ((int)firstSet(indx_words));
}

//
// Checks if bit string is empty
// Returns 0 if true and -1 if not true.
//
bool
LogicalNodeset::isEmpty() const
{
	for (uint_t i = 0; i < nbWords; i++) {
		if (words[i] != 0) {
			return (false);
		}
	}
	return (true);
}


//
// Operations on Nodeset
//
bool
LogicalNodeset::isEqual(const LogicalNodeset &ns) const
{
	uint32_t *longer_words;
	uint_t longer_length, shorter_length;
	int n;

	if (nbWords > ns.nbWords) {
		longer_words = &words[0];
		longer_length = nbWords;
		shorter_length = ns.nbWords;
	} else {
		longer_words = &ns.words[0];
		longer_length = ns.nbWords;
		shorter_length = nbWords;
	}
	// Words that are "off the end" of shorter array are treated as 0
	n = longer_length;
	while (n-- > shorter_length) {
		if (longer_words[n] != 0) {
			return (false);
		}
	}
	while (n >= 0) {
		if (words[n] != ns.words[n]) {
			return (false);
		}
		n--;
	}
	return (true);
}


bool
LogicalNodeset::isSuperset(const LogicalNodeset &ns) const
{
	int n = ns.nbWords;

	while (n-- > nbWords) {
		if (ns.words[n] != 0) {
			return (false);
		}
	}
	while (n >= 0) {
		if ((words[n] & ns.words[n]) != ns.words[n]) {
			return (false);
		}
		n--;
	}
	return (true);
}


bool
LogicalNodeset::isSubset(const LogicalNodeset &ns) const
{
	int n = nbWords;

	while (n-- > ns.nbWords) {
		if (words[n] != 0) {
			return (false);
		}
	}
	while (n >= 0) {
		if ((words[n] & ns.words[n]) != words[n]) {
			return (false);
		}
		n--;
	}
	return (true);
}

const LogicalNodeset
operator|(const LogicalNodeset &ns1, const LogicalNodeset &ns2)
{
	LogicalNodeset nsUnion;
	uint32_t *longer_words;
	uint_t longer_length, shorter_length;
	uint_t n;

	if (ns1.nbWords > ns2.nbWords) {
		longer_words = &ns1.words[0];
		longer_length = ns1.nbWords;
		shorter_length = ns2.nbWords;
	} else {
		longer_words = &ns2.words[0];
		longer_length = ns2.nbWords;
		shorter_length = ns1.nbWords;
	}
	// Words that are "off the end" of shorter array are treated as 0
	for (n = longer_length; n > shorter_length; n--) {
		nsUnion.words[n - 1] = longer_words[n - 1];
	}
	while (n-- > 0) {
		nsUnion.words[n] = ns1.words[n] | ns2.words[n];
	}
	return (nsUnion);
}

LogicalNodeset &
/* CSTYLED */
LogicalNodeset::operator|=(const LogicalNodeset &ns1)
{
	if (nbWords < ns1.nbWords) {
		// Need to enlarge the current nodeset array
		uint32_t *oldWords = &words[0];
		size_t oldSize = sizeof (uint32_t) * nbWords;
		initialize();
		(void) memcpy(words, oldWords, oldSize);
		delete [] oldWords;
	}
	CL_PANIC(nbWords >= ns1.nbWords);
	uint_t n = ns1.nbWords;

	while (n-- > 0) {
		words[n] |= ns1.words[n];
	}
	return (*this);
}


const LogicalNodeset
operator&(const LogicalNodeset &ns1, const LogicalNodeset &ns2)
{
	uint_t n =  ns1.nbWords;
	LogicalNodeset nsIntersect;
	uint_t shorter_length;

	shorter_length = ns1.nbWords > ns2.nbWords ? ns2.nbWords : ns1.nbWords;

	for (n = 0; n < shorter_length; n++) {
		nsIntersect.words[n] = ns1.words[n] & ns2.words[n];
	}
	return (nsIntersect);
}

LogicalNodeset &
/* CSTYLED */
LogicalNodeset::operator&=(const LogicalNodeset &ns1)
{
	int n = nbWords;

	while (n-- > ns1.nbWords) {
		words[n] = 0;
	}
	while (n >= 0) {
		words[n] &= ns1.words[n];
		n--;
	}
	return (*this);
}


const LogicalNodeset
operator-(const LogicalNodeset &ns1, const LogicalNodeset &ns2)
{
	int n = ns1.nbWords;
	LogicalNodeset nsSub;

	while (n-- > ns2.nbWords) {
		nsSub.words[n] = ns1.words[n];
	}
	while (n >= 0) {
		nsSub.words[n] = ns1.words[n] & ~ns2.words[n];
		n--;
	}
	return (nsSub);
}

LogicalNodeset &
/* CSTYLED */
LogicalNodeset::operator-=(const LogicalNodeset &ns1)
{
	// Start at the shorter of the two lengths
	uint_t n = nbWords > ns1.nbWords ? ns1.nbWords : nbWords;

	while (n-- > 0) {
		words[n] = words[n] & ~ns1.words[n];
	}
	return (*this);
}


//
// Display the content of the Logical Nodeset.
// buffer needs to be freed by caller.
//
int
LogicalNodeset::display(char *&buffer)
{
	const size_t MAX_CHAR_PER_LINE = 51;
	uint_t word = 0;
	uint_t bit  = 0;
	char strng[BITSPERWORD + 1]; // maximum size allowed
	char linestr[MAX_CHAR_PER_LINE];

	strng[BITSPERWORD] = '\0';

	CL_PANIC(nbWords <= NBWORDS(theMaxLni));
	buffer = (char *)calloc(1, MAX_CHAR_PER_LINE * nbWords);
	if (NULL == buffer) {
		return (-1);
	}
	while (word < nbWords) {
		bit = 0;
		while (bit < BITSPERWORD) {
			strng[bit] = '0' +
			    (char)tstBit(((word * BITSPERWORD) + bit));
			bit++;
		}

		(void) sprintf(linestr, "%5d --- %-5d  %s\n",
		    word * BITSPERWORD + 1,
		    (word + 1) * BITSPERWORD, strng);
		(void) strcat(buffer, linestr);
		word++;

		if (firstSet(word) == -1) {
			break;
		}
	}
	return (0);
}

uint_t
LogicalNodeset::count() const
{
	rgm::lni_t lni = 0;
	uint_t count = 0;

	while ((lni = nextLniSet(lni + 1)) != 0) {
		count++;
	}

	return (count);
}


//
// Return next nodeid_t (physical nodeid) in set.  Return 0 if no more.
// Can be used to iterate through just the physical nodes while ignoring zones.
//
// Note for Europa:
// Currently, we assume that physical nodes have lni <= MAXNODES.
// For Europa, we probably need to be able to iterate through all
// physical nodes (server+farm); all server nodes; or all farm nodes.
// I.e., implement nextFarmNodeSet(), nextServerNodeSet(), nextPhysNodeSet().
//
// We might not want to assume that the LNIs of physical nodes,
// particularly those of farm nodes, fall within a certain range.
// We could intermix the LNIs of zones and farm nodes, using some other
// mechanism -- e.g. a type field in the LN, or a separate LogicalNodeset
// of farm nodes, to determine which of those LNs are farm nodes vs. zones.
// Or, Europa could reserve an additional range of LNIs (for example,
// 65 through 512) for farm nodes.
//
sol::nodeid_t
LogicalNodeset::nextServerNodeSet(sol::nodeid_t n) const
{
	rgm::lni_t nextnode;

	CL_PANIC(n <= (MAXNODES + 1));
	nextnode = nextLniSet((rgm::lni_t)n);
	if (nextnode > MAXNODES) {
		return (0);
	} else {
		return (nextnode);
	}
}

sol::nodeid_t
LogicalNodeset::nextFarmNodeSet(sol::nodeid_t n) const
{
	rgm::lni_t nextnode;

	CL_PANIC(n <= (theMaxNode + 1));
	nextnode = nextLniSet((rgm::lni_t)n);
	if (nextnode > theMaxNode) {
		return (0);
	} else {
		return (nextnode);
	}
}

sol::nodeid_t
LogicalNodeset::nextPhysNodeSet(sol::nodeid_t n) const
{
	rgm::lni_t nextnode;

	CL_PANIC(n <= (theMaxNode + 1));
	nextnode = nextLniSet((rgm::lni_t)n);
	if (nextnode > theMaxNode) {
		return (0);
	} else {
		return (nextnode);
	}
}

void
LogicalNodeset::addLni(rgm::lni_t n)
{
	CL_PANIC(n > 0 && n <= theMaxLni);
	if (NBWORDS(n) > nbWords) {
		// Need to enlarge the the nodeset array to hold this bit
		uint32_t *oldWords = words;
		size_t oldSize = sizeof (uint32_t) * nbWords;
		initialize();
		(void) memcpy(words, oldWords, oldSize);
		delete [] oldWords;
	}

	setBit(n - 1);
}

void
LogicalNodeset::setMaxLni(rgm::lni_t maxLni)
{
	theMaxLni = maxLni;
}

void
LogicalNodeset::setMaxNode(sol::nodeid_t maxNode)
{
	theMaxNode = (rgm::lni_t)maxNode;
}
