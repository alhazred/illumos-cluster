//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

/*
 * rgm_error.c
 *
 *	RGM error reporting functions
 */

#pragma ident	"@(#)rgm_error.cc	1.48	08/07/21 SMI"

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <syslog.h>
#include <sys/syslog.h>
#include <sys/sc_syslog_msg.h>
#include <sys/dbg_printf.h>
#include <rgm_proto.h>

#define	CMM_MAXMSGIDLEN	64

static void log_syslog(int log_option, char *messageID, char *fmt, va_list ap);

dbg_print_buf *rgm_dbg_buf;	// ring buffer for tracing

static void
log_syslog(int log_option, char *messageID, char *fmt, va_list ap)
{
	char id[100];
	char buff[1024];

	(void) sprintf(id, "ID[%s]", messageID);
	(void) vsprintf(buff, fmt, ap);
	syslog(log_option, buff);
}


/*
 * Put a formatted error message into syslog
 */
void
log_error(int syslog_priority, char *messageID, char *msg, ...)
{
	va_list ap;
	int errno_sav = errno;
	va_start(ap, msg);	/*lint !e40 */
	log_syslog(syslog_priority, messageID, msg, ap);
	va_end(ap);

	errno = errno_sav;
}


char *
rgm_msgid(const char *msgid)
{
	static char msgbuf[CMM_MAXMSGIDLEN];

	(void) strcpy(msgbuf, rgm_product_str);
	(void) strncat(msgbuf, msgid,
		CMM_MAXMSGIDLEN - (1 + strlen(rgm_product_str)));

	return ((char *)msgbuf);
}

#define	UCMM_BUFSIZ	256

void
ducmm_print(const char *msgid, const char *fmt, ...)
{
}
/*
 * Routine for tracing/logging debug info.
 * Tracing is always on. Logging (into syslog and the console) is turned on
 * only if Debugflag is turned on.
 */
void
ucmm_print(const char *msgid, const char *fmt, ...)
{
	uint_t	len = 0;
	char	str1[UCMM_BUFSIZ], fmt1[UCMM_BUFSIZ];
	va_list args;

	// If msgid is "RGM", we don't bother to put it into debug buf.
	if (strcmp(msgid, "RGM") != 0)
		(void) snprintf(fmt1, UCMM_BUFSIZ,
		    "tid:%d,%s: %s", pthread_self(), msgid, fmt);
	else
		(void) snprintf(fmt1, UCMM_BUFSIZ,
		    "tid:%d, %s", pthread_self(), fmt);

	// Add newline if there isn't in 'fmt'.
	len = (uint_t)strlen(fmt1);
	if (fmt1[len - 1] != '\n' && len < UCMM_BUFSIZ - 1) {
		fmt1[len] = '\n';
		fmt1[len+1] = '\0';
	}

	va_start(args, fmt);	/*lint !e40 */
	if (vsnprintf(str1, UCMM_BUFSIZ, fmt1, args) >= UCMM_BUFSIZ) {
		str1[UCMM_BUFSIZ - 1] = '\0';
	}
	va_end(args);

	// Make sure attributes with '%' are handled correctly.
	rgm_dbg_buf->dbprintf("%s", str1);

	if (Debugflag == B_TRUE)
		log_error(LOG_ERR, rgm_msgid(msgid), "%s", str1);
}
