/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_version.cc	1.7	08/11/13 SMI"

/*
 * rgm_version.cc
 * --------------
 * This file contains the definition and implementation of the
 * rgm_upgrade_callback class, as well as various helper routines for managing
 * versioning of the rgm.
 */

#include <sys/vm_util.h>
#include <rgm/rgm_msg.h>
#include <orb/object/adapter.h>
#include <rgm_proto.h>
#include <rgm/rgm_cnfg.h>
#include <sys/sol_version.h>

#if (SOL_VERSION >= __s10)
#include <cmm/membership_engine_impl.h>
#endif

#if (SOL_VERSION >= __s10)
/*
 * The name of our service, according to the version mananger.
 */
static const char *vp_name = "rgm";

/*
 * The names of the callback steps.  These names should not ever need to
 * change.
 */
static char *ucc_step1 = "rgm_step1";
static char *ucc_step2 = "rgm_step2";
static char *ucc_step3 = "rgm_step3";

/*
 * The name of the upgrade callback for rgm starter.
 * This name should not ever need to change.
 */
static char *ucc_rgm_starter = "rgm_starter";

/*
 * The highest version that we currently support.
 * This number must be changed whenever we re-version the rgm;
 * also change the vp-idl mapping below in such a case.
 */
#define	RGM_VP_MAX_MAJOR	2
#define	RGM_VP_MAX_MINOR	6
static version_manager::vp_version_t highest_version =
	{RGM_VP_MAX_MAJOR, RGM_VP_MAX_MINOR};

/*
 * Used by the rgm starter only, and not by rgmd itself
 */
// XXX
// XXX Should we have locks to update current version
// XXX
static version_manager::vp_version_t current_version = {0, 0};

/*
 * VP to IDL mapping
 * -----------------
 * The numbers below must change whenever the highest version
 * supported is changed.
 */
int rgm_vp_to_idl_mapping[RGM_VP_MAX_MAJOR+1][RGM_VP_MAX_MINOR+1] = {
	{ 0, 0, 0, 0, 0, 0, 0 },
				// VP version 0.0 to 0.6 defined for indexing
	{ 0, 0, 0, 0, 0, 0, 0 },
				// VP version 1.0 to 1.6 defined for indexing
	{ 1, 1, 1, 1, 1, 1,
				// VP version 2.0 to 2.5,IDL version <1> methods
	2 }
				// VP version 2.6, IDL version <2> methods,
				// process membership and RGM reconfig thread
    };

static void ucc_step1_callback_action();
static void ucc_step2_callback_action();
static void ucc_step3_callback_action();

void rgm_starter_upgrade_callback_func(const char *ucc_name,
    const version_manager::vp_version_t &new_version);
void rgm_upgrade_callback_func(const char *ucc_name,
    const version_manager::vp_version_t &new_version);
#endif


//
// Returns true if the rgmd's running vp_version is high enough to include
// zone cluster support and other sc3.2u2 functionality.  This function is
// usable even on an S9 system.  It can be used to test for support of
// features other than zone clusters during a rolling upgrade.
//
boolean_t
is_rgm_zc_version(void)
{
	return ((Rgm_state->version.current_version.major_num > 2) ||
	    ((Rgm_state->version.current_version.major_num == 2) &&
	    (Rgm_state->version.current_version.minor_num >= 6)) ?
	    B_TRUE : B_FALSE);
}


//
// Returns true if process membership support is available.
//
// For S10 and above, the following two scenarios mean
// that membership subsystem is ready to provide process membership
// and that rgmd is ready to use process membership :
// (1) if rolling upgrade is in progress, and rgmd has received
// step 3 upgrade callback from the version manager
// (2) if the current version of RGM mentioned in CCR is
// the version supporting process membership.
// In the above two cases, this method returns true.
//
// Since process membership feature is not available prior to S10,
// so returns false for Solaris versions older than S10.
//
boolean_t
use_ucmm_membership(void)
{
#if (SOL_VERSION >= __s10)
	bool do_locking = false;
	if (!rgm_state_lock_held()) {
		rgm_lock_state();
		do_locking = true;
	}

	if ((Rgm_state->version.current_version.major_num == 2) &&
	    (Rgm_state->version.current_version.minor_num < 6)) {
		//
		// Running one of versions 2.0-2.5.
		// If we are upgrading to 2.6, then process membership support
		// is available in step3 onwards of the upgrade callbacks.
		//
		if (Rgm_state->version.current_upgrade_cb_step == NULL) {
			//
			// We are running the old version and have not yet
			// received upgrade callbacks. So return false.
			//
			if (do_locking) {
				rgm_unlock_state();
			}
			return ((boolean_t)true);
		}

		if ((os::strcmp(Rgm_state->version.current_upgrade_cb_step,
		    ucc_step2) == 0) ||
		    (os::strcmp(Rgm_state->version.current_upgrade_cb_step,
		    ucc_step3) == 0)) {
			if (do_locking) {
				rgm_unlock_state();
			}
			return ((boolean_t)false);
		} else {
			if (do_locking) {
				rgm_unlock_state();
			}
			return ((boolean_t)true);
		}
	} else {
		boolean_t retval = B_FALSE;
		if (process_membership_support_available()) {
			retval = B_FALSE;
		} else {
			retval = B_TRUE;
		}

		if (do_locking) {
			rgm_unlock_state();
		}
		return (retval);
	}
#else
	return ((boolean_t)true);
#endif
}

boolean_t
process_membership_support_available(void)
{
#if (SOL_VERSION >= __s10)
	bool do_locking = false;
	if (!rgm_state_lock_held()) {
		rgm_lock_state();
		do_locking = true;
	}

	boolean_t retval = is_rgm_zc_version();

	if (do_locking) {
		rgm_unlock_state();
	}
	return (retval);
#else
	return ((boolean_t)false);
#endif
}


boolean_t
allow_inter_cluster(void)
{
	return (process_membership_support_available());
}

#if (SOL_VERSION >= __s10)
boolean_t
membership_api_available_for_rgmd_starter(void)
{
	if ((current_version.major_num > 2) ||
	    ((current_version.major_num == 2) &&
	    (current_version.minor_num >= 6))) {
		return ((boolean_t)true);
	} else {
		return ((boolean_t)false);
	}
}

/*
 * class rgm_upgrade_callback
 * ---------------------------
 * The template for the object that will receive the upgrade callbacks.
 */
class rgm_upgrade_callback :
    public McServerof<version_manager::upgrade_callback> {
public:
	rgm_upgrade_callback();
	rgm_upgrade_callback(upgrade_callback_funcp_t cb_funcp);

	//
	// The callback object can go away once all the associated uccs have
	// unregistered.
	//
	void _unreferenced(unref_t cookie);

	//
	// Actual callback method which will be invoked by the callback
	// framework during upgrade_commit.
	//
	void do_callback(const char *ucc_name,
	    const version_manager::vp_version_t &new_version, Environment &e);

private:
	upgrade_callback_funcp_t funcp;
};

rgm_upgrade_callback::rgm_upgrade_callback(upgrade_callback_funcp_t cb_funcp)
{
	funcp = cb_funcp;
}

// Not used
rgm_upgrade_callback::rgm_upgrade_callback()
{
	CL_PANIC(0);
}

//
// This is where we receive the upgrade callback.
// This method calls the function that is stored in
// the upgrade callback object.
// That is, this function will call the rgm_starter
// upgrade function for the rgm_starter process,
// and the rgm upgrade function for the rgmd.
//
void
rgm_upgrade_callback::do_callback(const char *ucc_name,
    const version_manager::vp_version_t &new_version,
    Environment &)
{
	(*funcp)(ucc_name, new_version);
}

void
rgm_upgrade_callback::_unreferenced(unref_t cookie)
{
	ucmm_print("RGM", NOGET("in rgm_upgrade_callback::_unreferenced\n"));
	ASSERT(_last_unref(cookie));
	delete this;
}

//
// The upgrade callback function for rgm_starter process
//
void
rgm_starter_upgrade_callback_func(const char *ucc_name,
    const version_manager::vp_version_t &new_version)
{
	if ((current_version.major_num < new_version.major_num) ||
	    (current_version.major_num == new_version.major_num &&
	    current_version.minor_num < new_version.minor_num)) {
		// Callback for higher version
		ucmm_print("RGM",
		    NOGET("In rgm_starter_upgrade_callback::do_callback: "
		    "ucc_name=%s; new_version=%d.%d; current_version=%d.%d\n"),
		    ucc_name, new_version.major_num, new_version.minor_num,
		    current_version.major_num, current_version.minor_num);
	} else {
		// Callback not for higher version; ignore it
		ucmm_print("RGM",
		    NOGET("In rgm_starter_upgrade_callback::do_callback: "
		    "ucc_name=%s; new_version=%d.%d; current_version=%d.%d, "
		    "ignoring stale version upgrade callback\n"),
		    ucc_name, new_version.major_num, new_version.minor_num,
		    current_version.major_num, current_version.minor_num);
		return;
	}

	current_version = new_version;
	rgm_starter_upgrade();
	ucmm_print("RGM", NOGET("In rgm_starter_upgrade_callback::do_callback: "
	    "finished %s callback\n"), ucc_name);
}

//
// The upgrade callback function for the rgm process.
//
// Following are the assumptions and explanations for the callbacks:
//
// Callback 1:
// At this step, rgmd registers with process membership subsystem.
// It also spawns the rgmd step reconfiguration thread, which will consume
// process membership callbacks to drive rgmd reconfigurations.
// Note that as soon as rgmd registers itself with process membership
// and tells that it is up and running, the process membership callback
// can happen soon after that (although asynchronously). And if the rgmd
// step reconfiguration thread processes such callbacks immediately,
// then rgmd will unnecessarily do multiple reconfigurations when
// it does not need to really. We know that upgrade callbacks are happening
// and each rgmd will finally register with process membership in this step1
// upgrade callback. So, we make the rgm step reconfiguration thread not
// proceed with its reconfiguration until some time after step1 upgrade
// callback.
//
// The implementation sets the new version in the "new_version" field, but
// does not change the "current_version."
//
// Callback 2:
// Once we receive this callback, we are sure that all rgmds have registered
// with process membership subsystem. From now on, we do not need to use
// the ucmm automaton-style reconfiguration.
// XXX Note that there is a window in which some rgmds have received step1
// XXX upgrade callback and some have received step2 upgrade callback.
// XXX In that window, if a rgmd reconfiguration triggers, the rgmds that
// XXX have received step1 upgrade callback will have the ucmm automaton
// XXX reconfiguring, and rgmds that received step2 upgrade callback
// XXX will also have their ucmm automaton reconfiguring. But the automaton
// XXX on rgmds that received step2 upgrade callback will not update rgm
// XXX data structures will trigger rgm reconfiguration. So, to be sure
// XXX of a correct reconfiguration, we should probably make rgmds that have
// XXX received step2 upgrade callback not make their ucmm automaton reconfig
//
// Callback 3 :
// Here we discard the automaton thread and cmm callback threads, etc.
// XXX This is to be done
// XXX Even if this does not happen, the automata will do their own reconfig
// XXX but will not drive rgmd reconfiguration. rgmd reconfiguration will be
// XXX triggered by rgmd step reconfiguration thread.
//
// In this step, we also signal the rgmd step reconfiguration thread to
// start servicing process membership callbacks. There is a chance that
// some rgmds might have died during the time we came from step1 upgrade
// callback to step3. Also, since the process membership reconfiguration
// happens asynchronously and membership callbacks are also received
// asynchronously, we make the rgmd step synchronization thread wait
// for a few seconds, before it wakes up and drives a reconfiguration
// with the latest process membership data.
//
// Change the "current_version" to the "new_version."
//
// XXX Think if we can do the upgrade in two steps instead of three
//
void
rgm_upgrade_callback_func(const char *ucc_name,
    const version_manager::vp_version_t &new_version)
{
	sc_syslog_msg_handle_t handle;

	//
	// Grab the lock.
	//
	rgm_lock_state();

	//
	// If the version upgrade callback is not for a higher version
	// than our current version, then ignore it.
	//
	if ((Rgm_state->version.current_version.major_num <
	    new_version.major_num) ||
	    (Rgm_state->version.current_version.major_num ==
	    new_version.major_num &&
	    Rgm_state->version.current_version.minor_num <
	    new_version.minor_num)) {
		// Callback is for a higher version
		ucmm_print("RGM",
		    NOGET("In rgm_upgrade_callback::do_callback: ucc_name=%s; "
		    "new_version=%d.%d; Rgm_state.new_version=%d.%d;"
		    "Rgm_state.current_version=%d.%d\n"), ucc_name,
		    new_version.major_num, new_version.minor_num,
		    Rgm_state->version.new_version.major_num,
		    Rgm_state->version.new_version.minor_num,
		    Rgm_state->version.current_version.major_num,
		    Rgm_state->version.current_version.minor_num);

	} else {
		// Callback is not for a higher version; ignore it
		ucmm_print("RGM",
		    NOGET("In rgm_upgrade_callback::do_callback: ucc_name=%s; "
		    "new_version=%d.%d; Rgm_state.new_version=%d.%d;"
		    "Rgm_state.current_version=%d.%d; ignoring stale "
		    "version upgrade callback\n"), ucc_name,
		    new_version.major_num, new_version.minor_num,
		    Rgm_state->version.new_version.major_num,
		    Rgm_state->version.new_version.minor_num,
		    Rgm_state->version.current_version.major_num,
		    Rgm_state->version.current_version.minor_num);

		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Version manager delivered upgrade callback to rgmd
		// about a new version that is not higher than
		// the current version of rgmd.
		// @user_action
		// No user action is required.
		// This message is for informational purpose only.
		//
		(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
		    "Stale upgrade callback from version manager: "
		    "upgrade version is %d.%d while current version is %d.%d",
		    new_version.major_num, new_version.minor_num,
		    Rgm_state->version.current_version.major_num,
		    Rgm_state->version.current_version.minor_num);
		sc_syslog_msg_done(&handle);
		goto done_callback;
	}

	//
	// Check for the step 1 callback
	//
	if (strcmp(ucc_name, ucc_step1) == 0) {

		//
		// Sanity check that the version is lower than or equal to
		// our highest version.
		//
		if (new_version.major_num > highest_version.major_num ||
		    (new_version.major_num == highest_version.major_num &&
		    new_version.minor_num > highest_version.minor_num)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// Version manager delivered upgrade callback to rgmd
			// about a new version that is higher than the highest
			// version supported by rgmd.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Invalid upgrade callback from version manager: "
			    "version %d.%d higher than highest version",
			    new_version.major_num, new_version.minor_num);
			sc_syslog_msg_done(&handle);
			goto done_callback;
		}

		//
		// Assign the new version to our "new version" internally.
		//
		Rgm_state->version.new_version = new_version;
		Rgm_state->version.current_upgrade_cb_step = ucc_step1;

		ucc_step1_callback_action();

	} else if (strcmp(ucc_name, ucc_step2) == 0) {

		//
		// Sanity check that the new version specified is what we
		// think the new version should be.
		//
		if (new_version.major_num !=
		    Rgm_state->version.new_version.major_num ||
		    new_version.minor_num !=
		    Rgm_state->version.new_version.minor_num) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// Version manager delivered upgrade callback to rgmd
			// about a new version that is not the expected new
			// version after callback step 1 for rgmd.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Invalid upgrade callback from version manager: "
			    "new version %d.%d is not the expected new "
			    "version %d.%d from callback step 1.",
			    new_version.major_num, new_version.minor_num,
			    Rgm_state->version.new_version.major_num,
			    Rgm_state->version.new_version.minor_num);
			sc_syslog_msg_done(&handle);
			goto done_callback;
		}

		Rgm_state->version.current_upgrade_cb_step = ucc_step2;

		ucc_step2_callback_action();

	} else if (strcmp(ucc_name, ucc_step3) == 0) {

		//
		// Sanity check that the new version specified is what we
		// think the new version should be.
		//
		if (new_version.major_num !=
		    Rgm_state->version.new_version.major_num ||
		    new_version.minor_num !=
		    Rgm_state->version.new_version.minor_num) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// Version manager delivered upgrade callback to rgmd
			// about a new version that is not the expected new
			// version after callback step 2 for rgmd.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Invalid upgrade callback from version manager: "
			    "new version %d.%d is not the expected new "
			    "version %d.%d from callback step 2.",
			    new_version.major_num, new_version.minor_num,
			    Rgm_state->version.new_version.major_num,
			    Rgm_state->version.new_version.minor_num);
			sc_syslog_msg_done(&handle);
			goto done_callback;
		}

		//
		// Make the current version the same as the new version.
		//
		Rgm_state->version.current_version = new_version;
		Rgm_state->version.current_upgrade_cb_step = ucc_step3;

		// Update the version numbers in the ccr table.
		if (am_i_president()) {
			rgm_set_vp_version(
			    Rgm_state->version.current_version.major_num,
			    Rgm_state->version.current_version.minor_num);
			ucmm_print("RGM", NOGET(
			    "In rgm_upgrade_callback::do_callback: "
			    "update the version in ccr to the new version\n"));
		}

		ucc_step3_callback_action();

	} else {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Version manager delivered upgrade callback to rgmd
		// passing an upgrade callback object name that was
		// not registered by rgmd.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Invalid upgrade callback from version manager: "
		    "unrecognized ucc_name %s", ucc_name);
		sc_syslog_msg_done(&handle);
		goto done_callback;
	}

	ucmm_print("RGM", NOGET("In rgm_upgrade_callback::do_callback: "
	    "finished %s callback\n"), ucc_name);

done_callback:
	rgm_unlock_state();
}

//
// rgm_starter_initialize_versioning
//
// Creates and registers the callback object for rgm_starter.
// Also retrieves the current version from the version manager,
// and stores it in the current_version.
//
void
rgm_starter_initialize_versioning(void)
{
	sc_syslog_msg_handle_t handle;
	Environment e;

	// Get a pointer to the local version manager
	version_manager::vm_admin_var vm_adm = vm_util::get_vm();
	if (CORBA::is_nil(vm_adm)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// RGM got a nil reference to the Version manager and
		// hence cannot register for version upgrade callbacks.
		// @user_action
		// Contact your authorized Sun service provider to determine
		// whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Got nil reference to version manager");
		sc_syslog_msg_done(&handle);
		abort_node(B_FALSE, B_FALSE, NULL);
	}

	// create our callback object
	rgm_upgrade_callback *cbp =
	    new rgm_upgrade_callback(rgm_starter_upgrade_callback_func);
	version_manager::upgrade_callback_var cb_v = cbp->get_objref();

	//
	// Create our ucc sequence.  We have only one ucc.
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via os::strdup).
	//
	version_manager::ucc_seq_t ucc_seq(1, 1);
	ucc_seq[0].ucc_name = os::strdup(ucc_rgm_starter);
	ucc_seq[0].vp_name = os::strdup(vp_name);

	//
	// Add the names of membership ucc, ccr ucc, and naming subsystem ucc
	// to the upgrade_before list of the rgm starter ucc,
	// so that rgm starter gets an upgrade callback only after
	// membership, ccr and naming subsystems finish
	// their upgrade callback work.
	//
	char **rgm_starter_dependency_name = new char *[3];
	//
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via os::strdup).
	//
	rgm_starter_dependency_name[0] = os::strdup(MEMBERSHIP_UCC_NAME);
	rgm_starter_dependency_name[1] = os::strdup("ccr");
	rgm_starter_dependency_name[2] = os::strdup("naming");
	version_manager::string_seq_t rgm_starter_ucc_before_seq(
	    3, 3, rgm_starter_dependency_name, true);
	ucc_seq[0].upgrade_before = rgm_starter_ucc_before_seq;

	//
	// Actually register the callbacks and retrieve the current version.
	//
	vm_adm->register_upgrade_callbacks(ucc_seq, cb_v, highest_version,
	    current_version, e);
	if (e.exception()) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		if (version_manager::invalid_ucc::
		    _exnarrow(e.exception())) {
			//
			// SCMSGS
			// @explanation
			// An exception was raised when RGM tried to register
			// for version upgrade callbacks with
			// the Version Manager.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "invalid ucc exception");
		} else if (version_manager::unknown_vp::
		    _exnarrow(e.exception())) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "unknown vp exception");
		} else if (version_manager::not_found::
		    _exnarrow(e.exception())) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "vp not found exception");
		} else if (version_manager::wrong_mode::
		    _exnarrow(e.exception())) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "wrong mode exception");
		} else if (version_manager::too_early::
		    _exnarrow(e.exception())) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "too early exception");
		} else {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "unknown exception");
		}
		e.clear();
		sc_syslog_msg_done(&handle);

		abort_node(B_FALSE, B_FALSE, NULL);
	}

	// print out the version, for debugging purposes
	ucmm_print("RGM", NOGET("rgm version: %d.%d\n"),
	    current_version.major_num, current_version.minor_num);
}

//
// rgm_initialize_versioning
//
// Creates and registers the callback object for rgmd.
// Also retrieves the current version from the version manager,
// and stores it in both the current_version
// and new_version fields of the version struct (that is part of the
// rgm_state_t struct).
//
void
rgm_initialize_versioning(void)
{
	sc_syslog_msg_handle_t handle;
	Environment e;
	boolean_t need_update_version = B_FALSE;

	// Get a pointer to the local version manager
	version_manager::vm_admin_var vm_adm = vm_util::get_vm();
	if (CORBA::is_nil(vm_adm)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Got nil reference to version manager");
		sc_syslog_msg_done(&handle);
		abort_node(B_FALSE, B_FALSE, NULL);
	}

	// create our callback object
	rgm_upgrade_callback *cbp =
	    new rgm_upgrade_callback(rgm_upgrade_callback_func);
	version_manager::upgrade_callback_var cb_v = cbp->get_objref();

	//
	// Create our ucc sequence.  We have two uccs.  The first must
	// be called before the second.
	//
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via os::strdup).
	//
	version_manager::ucc_seq_t ucc_seq(3, 3);
	ucc_seq[0].ucc_name = os::strdup(ucc_step1);
	ucc_seq[0].vp_name = os::strdup(vp_name);
	ucc_seq[1].ucc_name = os::strdup(ucc_step2);
	ucc_seq[1].vp_name = os::strdup(vp_name);
	ucc_seq[2].ucc_name = os::strdup(ucc_step3);
	ucc_seq[2].vp_name = os::strdup(vp_name);

	//
	// Add the names of membership ucc, ccr ucc and naming subsystem ucc
	// to the upgrade_before list of the step 1 rgm ucc,
	// so that rgm gets step1 upgrade callback only after membership,
	// ccr and naming subsystems finish their upgrade callback work.
	//
	char **step1_dependency_name = new char *[3];
	//
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via os::strdup).
	//
	step1_dependency_name[0] = os::strdup(MEMBERSHIP_UCC_NAME);
	step1_dependency_name[1] = os::strdup("ccr");
	step1_dependency_name[2] = os::strdup("naming");
	version_manager::string_seq_t rgm_step1_ucc_before_seq(
	    3, 3, step1_dependency_name, true);
	ucc_seq[0].upgrade_before = rgm_step1_ucc_before_seq;

	//
	// Add the first ucc's name to the upgrade_before list of the second
	// ucc.
	//
	char **step2_dependency_name = new char *[1];
	//
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via os::strdup).
	//
	step2_dependency_name[0] = os::strdup(ucc_step1);
	version_manager::string_seq_t
	    step2_before_seq(1, 1, step2_dependency_name, true);
	ucc_seq[1].upgrade_before = step2_before_seq;

	//
	// Add the second ucc's name to the upgrade_before list of the third
	// ucc.
	//
	char **step3_dependency_name = new char *[1];
	//
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via os::strdup).
	//
	step3_dependency_name[0] = os::strdup(ucc_step2);
	version_manager::string_seq_t
	    step3_before_seq(1, 1, step3_dependency_name, true);
	ucc_seq[2].upgrade_before = step3_before_seq;

	//
	// Actually register the callbacks and retrieve the current version.
	//
	vm_adm->register_upgrade_callbacks(ucc_seq, cb_v, highest_version,
	    Rgm_state->version.current_version, e);
	if (e.exception()) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		if (version_manager::invalid_ucc::
		    _exnarrow(e.exception())) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "invalid ucc exception");
		} else if (version_manager::unknown_vp::
		    _exnarrow(e.exception())) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "unknown vp exception");
		} else if (version_manager::not_found::
		    _exnarrow(e.exception())) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "vp not found exception");
		} else if (version_manager::wrong_mode::
		    _exnarrow(e.exception())) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "wrong mode exception");
		} else if (version_manager::too_early::
		    _exnarrow(e.exception())) {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "too early exception");
		} else {
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Unable to register for version upgrade callbacks "
			    "with version manager: %s",
			    "unknown exception");
		}
		sc_syslog_msg_done(&handle);

		abort_node(B_FALSE, B_FALSE, NULL);
	}

	//
	// To start with, the new version and the current version are the same.
	// They are only different during an upgrade.
	//
	Rgm_state->version.new_version = Rgm_state->version.current_version;

	//
	// We initialize the step to null.
	// If we are already running the latest version,
	// then the current_upgrade_cb_step will not be used anyway.
	// Otherwise, it will be set in the upgrade callbacks.
	//
	Rgm_state->version.current_upgrade_cb_step = NULL;

	// print out the version, for debugging purposes
	ucmm_print("RGM", NOGET("rgm version: %d.%d\n"),
	    Rgm_state->version.current_version.major_num,
	    Rgm_state->version.current_version.minor_num);

	//
	// Validate if the version stored in ccr table is lower than or
	// equals to the current version.
	//
	need_update_version = rgm_check_vp_version(
	    Rgm_state->version.current_version.major_num,
	    Rgm_state->version.current_version.minor_num);

	if (need_update_version) {
		ucmm_print("RGM", NOGET("rgm_initialize_versioning: "
		    "the CCR table for versioning doesn't exist or the version "
		    "in CCR is lower than the current version; will update CCR "
		    "later\n"));
	} else {
		ucmm_print("RGM", NOGET("rgm_initialize_versioning: "
		    "the version in CCR is the same as the current version\n"));
	}
}

//
// In the first step, rgmd registers with process membership.
//
static void
ucc_step1_callback_action()
{
	//
	// Process membership setup action for rgmd :
	// ------------------------------------------
	// If we are upgrading from any of versions 2.0-2.5
	// to >=2.6 (versions supporting process membership),
	// then we setup process membership infrastructure for RGM.
	//
	bool old_version_allowed =
	    (Rgm_state->version.current_version.major_num == 2) &&
	    (Rgm_state->version.current_version.minor_num < 6);
	bool new_version_allowed =
	    // XXX should we allow RU to versions >= 3.0?
	    (Rgm_state->version.new_version.major_num > 2) ||
	    ((Rgm_state->version.new_version.major_num == 2) &&
	    (Rgm_state->version.new_version.minor_num >= 6));
	if (old_version_allowed && new_version_allowed) {
		rgm_ru_transition_in_progress = B_TRUE;
		setup_membership(true);
	}
}

//
// In the second step, rgmd marks that it will use process membership
// feature henceforth, and not do reconfiguration as a result of
// old style ucmm step callback. Since the step number is marked
// before doing this method (and the check for process membership
// support will check the step number during a rolling upgrade),
// we do not need to do anything more here.
// Just ensure that we have not started using process membership
// for rgmd reconfigurations.
// The rgmd step synchronization reconfiguration thread will
// do its action only after we get step3 upgrade callback.
//
static void
ucc_step2_callback_action()
{
	if (!process_membership_support_available()) {
		CL_PANIC(!Rgm_state->using_process_membership);
	}
}

//
// In the third step, rgmd discards its old style
// ucmm threads and data structures.
// XXX To implement this part
//
static void
ucc_step3_callback_action()
{
	// XXX TODO : check what methods to call to shutdown automaton

	// Signal the rgmd step synchronization thread to start its action
	rgm_reconfig_process_membership_after_upgrade();
}
#endif	// (SOL_VERSION >= __s10)
