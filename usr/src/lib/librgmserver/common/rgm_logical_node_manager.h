//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _RGM_LOGICAL_NODE_MANAGER_H
#define	_RGM_LOGICAL_NODE_MANAGER_H

#pragma ident	"@(#)rgm_logical_node_manager.h	1.5	08/05/20 SMI"

#include <rgm/rgm_common.h>

#include <rgm_logical_nodeset.h>
#include <rgm_logical_node.h>
#include <rgm_obj_manager.h>
#include <cmm/ucmm_api.h>

// Default LN for unconfigured cluster nodes.
#define	UNCONFIG_CLUSTER_NODE "unconfigured-node"

//
// There is one such object created at rgm intialization.
// Its role is to manage the LogicalNodes
//
// We cannot use the base class because some methods need
// to be redefined in order to make sure the cluster nodes
// have a LNI equal to their nodeid
//
// Keyed on nodeid:zonename strings
//
class LogicalNodeManager : public ObjManager<LogicalNode, uint_t>
{
private:


public:
	LogicalNodeManager();
	virtual ~LogicalNodeManager();

#ifndef EUROPA_FARM
	// creates the dummy logical nodes for the physical nodes
	void initialize();
#endif

	char *computeLn(sol::nodeid_t nodeid, const char *zonename);
	LogicalNode* findLogicalNode(sol::nodeid_t nodeid,
	    const char *zonename, char **lnp = NULL);

#ifndef EUROPA_FARM
	LogicalNode* createLogicalNode(sol::nodeid_t nodeid,
	    const char *zonename, rgm::rgm_ln_state status,
	    const char *ln_in = NULL);
#endif

	LogicalNode* createLogicalNodeFromMapping(sol::nodeid_t nodeid,
	    const char *zonename, rgm::lni_t lni,
	    rgm::rgm_ln_state status, const char *nodename = NULL);

	// Must be used only by rgm_comm_impl::idl_reclaim_lnis()
	LogicalNode* removeElement(const char *name);

#ifndef EUROPA_FARM
	//
	// The updateMembership method updates physical nodes only.
	//
	void updateMembership(LogicalNodeset &nset);
	//
	// revert the states back to what they were previously
	// This function should be called after updateMembership, without
	// releasing the lock in between
	//
	void revertMembership();

	void updateConfiguredNodes(LogicalNodeset &ns);
#endif

	LogicalNodeset *get_all_logical_nodes(rgm::rgm_ln_state at_least);
	LogicalNodeset *get_all_server_logical_nodes
	    (rgm::rgm_ln_state at_least);

#ifndef EUROPA_FARM
	void updateCachedNames();

	void reclaimLnis();
	void reclaimOff();
	void reclaimOn();
#endif

	void debug_print();

protected:
	bool allowReclaim;
	int lniReclaimCount;
	uint_t addElement(const char *name, LogicalNode *lnNode, uint_t ngrow);
	uint_t addElement(const char *name, LogicalNode *lnNode);
	uint_t addElementReserved(const char *name, LogicalNode *lnNode,
	    uint_t reservedIdx, uint_t ngrow = 0);
};

inline
LogicalNodeManager::LogicalNodeManager()
{
	lniReclaimCount = 0;
	allowReclaim = true;
}

inline
LogicalNodeManager::~LogicalNodeManager()
{
}

inline uint_t
LogicalNodeManager::addElement(const char *name, LogicalNode *lnNode)
{
	return (addElement(name, lnNode, 0));
}

#endif /* _RGM_LOGICAL_NODE_MANAGER_H */
