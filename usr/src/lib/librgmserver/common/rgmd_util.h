/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGMD_UTIL_H
#define	_RGMD_UTIL_H

#pragma ident	"@(#)rgmd_util.h	1.40	08/07/22 SMI"

#include <h/rgm.h>
#include <scha.h>
#include <rgm/scha_strings.h>
#include <rgm/scha_priv.h>

#ifdef __cplusplus
extern "C" {
#endif

// path where method cores are stored when timed out
#define	DEFAULT_COREPATH	"/var/cluster/core"

#include <sys/sol_version.h>
// fedtag = zonename.rgname.rname.methno
#if SOL_VERSION >= __s10
#include <zone.h>
//
// tag format is zonename.rgname.rname.methodnum, where methodnum is a
// 1- or 2-digit integer string
//
#define	MAXFEDTAGLEN	(ZONENAME_MAX + 2 * MAXRGMOBJNAMELEN + 2 + 4)
#else
// tag format is rgname.rname.methodnum
#define	MAXFEDTAGLEN	(2 * MAXRGMOBJNAMELEN + 2 + 3)
#endif

//
// defines RG state
//
typedef struct rgstatename {
	rgm::rgm_rg_state	rgm_val;
	char			*statename;
	char			*api_statename;
	scha_rgstate_t		api_val;
} rgstatename_t;

//
// defines R states
//
typedef struct rstatename {
	rgm::rgm_r_state	rgm_val;
	char			*statename;
	char			*api_statename;
	scha_rsstate_t		api_val;
} rstatename_t;

//
// defines R FM status
//
typedef struct rfmstatusname {
	rgm::rgm_r_fm_status	rgm_val;
	char			*statusname;
	scha_rsstatus_t		api_val;
} rfmstatusname_t;

//
// Pending "direction" of an RG changing state
//
typedef enum rgm_direction {
	RGM_DIR_STARTING,
	RGM_DIR_STOPPING,
	RGM_DIR_NULL
} rgm_direction_t;

//
// Default status message strings for RGs that are starting or stopping
//
#define	RGM_MSG_STARTING	"Starting"
#define	RGM_MSG_STOPPING	"Stopping"

typedef struct updatable_prop {
	const char		*prop_name;
	boolean_t		updatable;
} updatable_prop_t;

//
// flags used to remember what properties have been updated;
// used when updating an rg, when running validate method on its
// resources.
//
typedef struct changed_rg_prop {
	uint_t		len;
	boolean_t	rg_max_primaries;
	boolean_t	rg_desired_primaries;
	boolean_t	rg_impl_net_depend;
	boolean_t	rg_failback;
	boolean_t	rg_dependencies;
	boolean_t	rg_glb_rsrcused;
	boolean_t	rg_nodelist;
	boolean_t	rg_description;
	boolean_t	rg_pathprefix;
	boolean_t	rg_project_name;
	boolean_t	rg_affinities;
	boolean_t	rg_auto_start;
	boolean_t	rg_ppinterval;
	boolean_t	rg_system;
	boolean_t	rg_suspended;
	boolean_t	rg_affinitents;
	/* SC SLM addon start */
	boolean_t	rg_slm_type;
	boolean_t	rg_slm_pset_type;
	boolean_t	rg_slm_cpu;
	boolean_t	rg_slm_cpu_min;
	/* SC SLM addon end */
} changed_rg_prop_t;


#ifdef __cplusplus
}
#endif

#endif	/* !_RGMD_UTIL_H */
