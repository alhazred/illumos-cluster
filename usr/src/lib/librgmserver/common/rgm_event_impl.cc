//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_event_impl.cc	1.10	08/05/20 SMI"

//
// rgm_event_impl.cc
//
// The functions in this file implement the server side of
// internode communication through the ORB.
// The subset of implementations deal with those
// interfaces having to do with genereation of event
// snapshots.
//

#include <rgm/rgm_comm_impl.h>
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#include <rgm_proto.h>

//
// Generate a sysevent with the current
// cluster membership information.
//
void
rgm_comm_impl::idl_event_gen_membership(
	rgm::idl_regis_result_t_out ev_res, Environment &)
{
	rgm::idl_regis_result_t	*retval = new rgm::idl_regis_result_t();
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	ucmm_print("RGM",
		NOGET("idl_event_gen_membership() called\n"));

	rgm_lock_state();

	if ((res = post_event_membership(Rgm_state->current_membership,
		Rgm_state->current_membership)).err_code
		!= SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("idl_event_gen_membership() "
			" failed to post event: error %d"),
			res.err_code);
		goto finished; //lint !e801
	}

	ucmm_print("RGM",
	    NOGET("idl_event_gen_membership() "
		" posted event: returning\n"));

finished:

	rgm_unlock_state();

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	ev_res = retval;
}

//
// Generate a sysevent with the current
// state of the specified RG.
//

void
rgm_comm_impl::idl_event_gen_rg_state(
	const char *rg_name,
	rgm::idl_regis_result_t_out ev_res, Environment &)
{
	rgm::idl_regis_result_t	*retval = new rgm::idl_regis_result_t();
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	ucmm_print("RGM",
		NOGET("idl_event_gen_rg_state() "
		"called for RG <%s>\n"), rg_name ? rg_name : "NULL");

	rgm_lock_state();

	if ((res = post_event_rg_state(rg_name)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("idl_event_gen_rg_state() "
			"fail to post event: error %d"),
			res.err_code);
		goto finished; //lint !e801
	}

	ucmm_print("RGM",
	    NOGET("idl_event_gen_rg_state() "
		"posted event: returning\n"));

finished:

	rgm_unlock_state();

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	ev_res = retval;
}

//
// Generate a sysevent with the current
// state of the specified R.
//

void
rgm_comm_impl::idl_event_gen_rs_state(
	const char *rs_name,
	rgm::idl_regis_result_t_out ev_res, Environment &)
{
	rgm::idl_regis_result_t	*retval = new rgm::idl_regis_result_t();
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	ucmm_print("RGM",
		NOGET("idl_event_gen_rs_state() "
		"called for RS <%s>\n"), rs_name ? rs_name : "NULL");

	rgm_lock_state();

	if ((res = post_event_rs_state(rs_name)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("idl_event_gen_rs_state() "
			"fail to post event: error %d"),
			res.err_code);
		goto finished; //lint !e801
	}

	ucmm_print("RGM",
	    NOGET("idl_event_gen_rs_state() "
		"posted event: returning\n"));

finished:

	rgm_unlock_state();

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	ev_res = retval;
}
