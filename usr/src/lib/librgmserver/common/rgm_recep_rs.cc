/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_recep_rs.cc	1.70	09/01/15 SMI"

/*
 * rgm_recep_rs.cc is the receptionist component of rgmd which
 *	handles the requests from LIBSCHA API functions
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <rpc/rpc.h>
#include <netdb.h>

#include <rgm/rgm_common.h>
#ifdef EUROPA_FARM
#include <cmm/ucmm_api.h>
#endif
#include <rgm/rgm_cnfg.h>
#include <rgm_state.h>
#include <rgm/rgm_call_pres.h>
#include <rgm_api.h>
#include <rgm_proto.h>
#include <rgm/rgm_msg.h>

#include <rgm/rgm_recep.h>

/* zone support */
#include <rgm_logical_node.h>
#include <rgm_logical_node_manager.h>
#include <rgm_logical_nodeset.h>


/*
 * open_rs() -
 *
 *	get sequence id of RG from RG state structure in memory
 *
 *	Notes: R within a RG shares the same sequence id with RG.
 */
void
open_res(rs_open_args *args, rs_open_result_t *result)
{
	rgm_rt_t	*rt = NULL;
	rgm_resource_t	*resource = NULL;
	rlist_p_t	r = NULL;
	rglist_p_t	rgp = NULL;

	result->ret_code = SCHA_ERR_NOERR;

	rgm_lock_state();

	//
	// If this node has just joined the cluster, there is a window of time
	// in which the president has not yet ordered this node to read the
	// CCR.  If this is the case, sleep until the CCR has been read.
	//
	wait_until_ccr_is_read();

	/*
	 * Get pointer to resource in our in-memory state structure.
	 */
	if ((r = rname_to_r(NULL, args->rs_name)) == NULL) {
		result->ret_code = SCHA_ERR_RSRC;
		ucmm_print("RGM",
		    NOGET("couldn't get resource <%s>\n"), args->rs_name);
		goto finished; //lint !e801
	}
	resource = r->rl_ccrdata;

	if ((rt = rtname_to_rt(resource->r_type)) == NULL) {
		ucmm_print("RGM", NOGET("RT doesn't exist.\n"));
		result->ret_code = SCHA_ERR_RT;
		goto finished; //lint !e801
	}

	/*
	 * all Rs within a RG share the same seq_id with RG.
	 */
	rgp = r->rl_rg;
	result->rs_seq_id = strdup_nocheck(rgp->rgl_ccr->rg_stamp);
	if (result->rs_seq_id == NULL) {
		result->ret_code = SCHA_ERR_NOMEM;
		ucmm_print("RGM",
		    NOGET("get_res: failed on strdup\n"));
		goto finished; //lint !e801
	}

	/*
	 * All RT properties can be retrieved by scha_resource_get command
	 * Need to get rt sequence id
	 */
	result->rt_seq_id = strdup_nocheck(rt->rt_stamp);
	result->rt_name = strdup_nocheck(resource->r_type);
	if (result->rt_seq_id == NULL || result->rt_name == NULL) {
		result->ret_code = SCHA_ERR_NOMEM;
		if (result->rs_seq_id != NULL)
			free(result->rs_seq_id);
		if (result->rt_seq_id != NULL)
			free(result->rt_seq_id);
		if (result->rt_name != NULL)
			free(result->rt_name);
		ucmm_print("RGM",
		    "ret code from strdup is SCHA_ERR_NOMEM: returning\n");
	}

finished :
	rgm_unlock_state();

}
/*
 * get_res_switch()
 *
 *	get resource On_off/Monitored switch from President node
 */
void
get_res_switch(rs_get_switch_args *args, get_switch_result_t *result)
{

	bzero(result, sizeof (get_switch_result_t));
	result->ret_code = SCHA_ERR_NOERR;

	/*
	 * send a request to the President via ORB RPC call to get the
	 * the given R On_off/Monitored switch
	 */
	result->ret_code = call_scha_rs_get_switch(args, result).err_code;
	if (args->on_off) {
		if (result->ret_code != SCHA_ERR_NOERR)
			ucmm_print("RGM", NOGET("couldn't get "
			    "R On_off_switch. Error=%d\n"),
			    result->ret_code);
		else
			ucmm_print("RGM", NOGET(
			    "get_res_status: On_off_switch = %d\n"),
			    result->rs_switch);
	} else {
		if (result->ret_code != SCHA_ERR_NOERR)
			ucmm_print("RGM", NOGET("couldn't get "
			    "R Monitored_switch. Error=%d\n"),
			    result->ret_code);
		else
			ucmm_print("RGM", NOGET(
			    "get_res_status: Monitored_switch = %d\n"),
			    result->rs_switch);
	}
}

/*
 * get_res_ext()
 *
 *	get value of the given extension property from R state structure
 *	in memory
 */
void
get_res_ext(rs_get_args *args, get_ext_result_t *result)
{
	rgm_property_list_t *pp = NULL;
	strarr_list	*ret_list = NULL;
	rgm_resource_t	*resource = NULL;
	rlist_p_t	r = NULL;
	rglist_p_t	rgp = NULL;
	char *node_name = NULL;
	char *zonename;
	LogicalNode *ln = NULL;

	result->ret_code = SCHA_ERR_NOERR;

	rgm_lock_state();

	/*
	 * Get pointer to resource in our in-memory state structure.
	 */
	if ((r = rname_to_r(NULL, args->rs_name)) == NULL) {
		result->ret_code = SCHA_ERR_RSRC;
		ucmm_print("RGM",
		    NOGET("couldn't get resource <%s>\n"), args->rs_name);
		goto finished; //lint !e801
	}
	resource = r->rl_ccrdata;

	/*
	 * Traverse the list of properties searching for the right one,
	 *
	 */
	for (pp = resource->r_ext_properties; pp != NULL;
	    pp = pp->rpl_next) {
		if (strcasecmp(args->rs_prop_name, pp->rpl_property->rp_key)
		    == 0)
			/* found it */
			break;
	}

	/*
	 * If can not find the property in the list, return error
	 */
	if (pp == NULL) {
		result->ret_code = SCHA_ERR_PROP;
		ucmm_print("RGM",
			NOGET("couldn't find ext property: returning\n"));
		goto finished;
	}


	/*
	 * All Rs within a RG share the same seq_id.
	 */
	rgp = r->rl_rg;

	if (args->rs_node_name == NULL ||
	    args->rs_node_name[0] == '\0') {
		if (args->zonename == NULL ||
		    args->zonename[0] == '\0')
			zonename = NULL;
		else
			zonename = args->zonename;
		// Caller didn't provide nodename, so use our own.
		node_name = lnManager->computeLn(
		    get_local_nodeid(), zonename);
	} else
		node_name = new_str(args->rs_node_name);


	//
	// if type of extension property is :
	// - SCHA_PTYPE_ENUM, SCHA_PTYPE_BOOLEAN, SCHA_PTYPE_STRING,
	//   SCHA_PTYPE_INT, or SCHA_PTYPE_UINT, the value is stored in
	//   pp->rpl_property->rp_value (name_t struct),
	// - SCHA_PTYPE_STRINGARRAY, the value is stored
	//   in pp->rpl_property->rp_array_values (namelist_t struct)
	// If the user has used the "EXTENSION" tag in scha_resource_get to
	// retrieve the non-pernode extension property value, retrieve the
	// value irrespective of where the command is executed.
	//
	// A scha_* query to retrieve a per-node property value using
	// "EXTENSION"/"EXTENSION_NODE"  tags would return the the default
	// value if the node on which the command was executed was not in
	// the RG's nodelist. Note that "DEFAULT" value specification for the
	// per-node properties. is mandatory while registering the RT.
	//
	ln = get_ln(node_name, &result->ret_code);

	if (result->ret_code != SCHA_ERR_NOERR && (result->ret_code !=
	    SCHA_ERR_NODE || am_i_president())) {
		goto finished; //lint !e801
	}

	//
	// We need to goto the president to retrieve the property values
	// in case,
	// 1. get_ln on this node returns SCHA_ERR_NODE.
	// 2. The scha_* query using EXTENSION_NODE tag.(This is ratified
	// by looking at the input args as args->rs_node_name would be
	// populated only in case of EXTENSION_NODE tag).
	// We need to goto the president in case of the user having used
	// EXTENSION_NODE query to get a property value. The validity of
	// the node that he might have specified using EXTENSION_NODE has
	// to be ratified by going to the president ( if the lni for the
	// same is not resolved locally at the slave).
	//
	//

	if ((result->ret_code == SCHA_ERR_NODE) && (args->rs_node_name
	    != NULL && args->rs_node_name[0] != '\0')) {
		//
		// If the rgmd instance is a non-president instance,
		// get_ln call above might have returned SCHA_ERR_NODE.
		// We would make an idl call to the president instance
		// to get the LogicaNode *pertaining to node_name.
		//
		result->ret_code = call_scha_rs_get_ext(args, result).err_code;
		result->prop_type = (int)pp->rpl_property->rp_type;

		if (result->ret_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801

		if (pp->rpl_property->rp_type != SCHA_PTYPE_STRINGARRAY &&
		    pp->rpl_property->rp_type != SCHA_PTYPE_UINTARRAY) {
			//
			// The property type is one of Stringarray or
			// Uintarray. call_scha_rs_get_ext does'nt takecare
			// of these two property types.(Per-node properties
			// are barred from having these as property types).
			// StringArray/UnitArray datatypes are handled by
			// the call to the store_prop_val funtion call below.
			//
			goto finished; //lint !e801
		}
	}

	result->seq_id = strdup_nocheck(rgp->rgl_ccr->rg_stamp);
	if (result->seq_id == NULL) {
		result->ret_code = SCHA_ERR_NOMEM;
		ucmm_print("RGM",
		    NOGET("get_res_ext: failed on strdup\n"));
		goto finished; //lint !e801
	}

	result->ret_code = store_prop_val(
	    pp->rpl_property->rp_type,
	    pp->rpl_property->rp_array_values,
	    ((rgm_value_t *)pp->rpl_property->rp_value)->rp_value,
	    pp->rpl_property->is_per_node,
	    node_name, &ret_list, pp->rpl_property->rp_key, r).err_code;

	if (result->ret_code != SCHA_ERR_NOERR) {
		if (result->seq_id != NULL)
			free(result->seq_id);
		goto finished; //lint !e801
	}

	result->value_list = ret_list;
	result->prop_type = (int)pp->rpl_property->rp_type;
finished:
	rgm_unlock_state();
	free(node_name);
}


/*
 * get_res()
 *
 *	get the value of the given R property from R state structure in memory
 */
void
get_res(rs_get_args *args, get_result_t *result)
{
	rgm_resource_t	*resource = NULL;
	std::map<rgm::lni_t, char *> val_str;
	namelist_t	*val_array = NULL;
	rdeplist_t	*val_array_rdep = NULL;
	strarr_list	*ret_list = NULL;
	scha_prop_type_t	type;
	rlist_p_t	r = NULL;
	boolean_t	need_free = B_FALSE;
	rgm_timelist_t	**rgtp = NULL;
	uint_t		count;
	time_t		max_age;
	char		*zonename = NULL;
	boolean_t	num_r_flg = B_FALSE;

	result->ret_code = SCHA_ERR_NOERR;
	result->value_list = NULL;

	//
	// Note, args->zonename is non-NULL only for the NUM_*_RESTARTS_ZONE
	// query tags.  The extra argument is a zonename on the local node.
	// The validity of the zonename will be checked by findLogicalNode,
	// below.
	// However, it appears that xdr changes a null value to "", so
	// we must treat an empty string value as equivalent to NULL.
	//
	if (args->zonename != NULL && args->zonename[0] != '\0') {
		zonename = args->zonename;
	}

	rgm_lock_state();

	/*
	 * Get pointer to resource in our in-memory state structure.
	 */
	if ((r = rname_to_r(NULL, args->rs_name)) == NULL) {
		result->ret_code = SCHA_ERR_RSRC;
		ucmm_print("RGM",
		    NOGET("couldn't get resource <%s>\n"), args->rs_name);
		goto finished; //lint !e801
	}
	resource = r->rl_ccrdata;

	/*
	 * If the query is NUM_RESOURCE_RESTARTS or NUM_RG_RESTARTS, property
	 * is not stored in the CCR.  Set num_r_flg and special-case it below.
	 */
	if (strcmp(args->rs_prop_name, SCHA_NUM_RESOURCE_RESTARTS) == 0 ||
	    strcmp(args->rs_prop_name, SCHA_NUM_RESOURCE_RESTARTS_ZONE) == 0) {
		num_r_flg = B_TRUE;
		LogicalNode *lnNode =
		    lnManager->findLogicalNode(get_local_nodeid(), zonename);
		if (lnNode == NULL) {
			result->ret_code = SCHA_ERR_NODE;
			goto finished; //lint !e801
		}
		rgm::lni_t lni = lnNode->getLni();
		rgtp = &r->rl_xstate[lni].rx_r_restarts;
	} else if (strcmp(args->rs_prop_name, SCHA_NUM_RG_RESTARTS) == 0 ||
	    strcmp(args->rs_prop_name, SCHA_NUM_RG_RESTARTS_ZONE) == 0) {
		num_r_flg = B_TRUE;
		LogicalNode *lnNode =
		    lnManager->findLogicalNode(get_local_nodeid(),
			zonename);
		if (lnNode == NULL) {
			result->ret_code = SCHA_ERR_NODE;
			goto finished; //lint !e801
		}
		rgm::lni_t lni = lnNode->getLni();
		rgtp = &r->rl_xstate[lni].rx_rg_restarts;
	}

	//
	// If the query is GLOBAL_ZONE, call is_r_globalzone to check
	// both the RT's Global_zone property and the resource's
	// Global_zone_override property.
	//
	if (strcmp(args->rs_prop_name, SCHA_GLOBALZONE) == 0) {
		//
		// Store the Boolean Global_zone value into the returned
		// RPC buffer
		//
		val_str[0] = rgm_itoa((int)is_r_globalzone(r));
		if (val_str[0] == NULL) {
			result->ret_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}
		need_free = B_TRUE;
		result->ret_code = store_prop_val(SCHA_PTYPE_BOOLEAN, NULL,
		    val_str, B_FALSE, NULL, &ret_list, NULL, NULL).err_code;
	} else if (num_r_flg) {
		/* NUM_RESOURCE_RESTARTS or NUM_RG_RESTARTS */

		/* Fetch Retry_interval for this resource */
		result->ret_code = get_retry_interval(r, &max_age).err_code;

		/*
		 * The 'num_restarts' queries are invalid if Retry_interval is
		 * not declared for the resource type.
		 */
		if (result->ret_code != SCHA_ERR_NOERR) {
			ucmm_print("RGM",
			    NOGET("couldn't get resource <%s> Retry_interval"),
			    args->rs_name);
			goto finished; //lint !e801
		}

		/* Prune the timelist, then count the entries in it */
		rgm_prune_timelist(rgtp, max_age);
		count = rgm_count_timelist(*rgtp);
		ucmm_print("RGM",
		    NOGET("get_res: pruned timelist rs <%s> %d secs count=%d"),
		    args->rs_name, max_age, count);

		/* Convert count to string; need to free it later */
		val_str[0] = rgm_itoa((int)count);
		if (val_str[0] == NULL) {
			result->ret_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}
		need_free = B_TRUE;

		/* store the value of 'count' to the returned RPC buffer */
		result->ret_code = store_prop_val(SCHA_PTYPE_UINT, NULL,
		    val_str, B_FALSE, NULL, &ret_list, NULL, NULL).err_code;
	} else {
		/*
		 * Property is a CCR property.
		 * Retrieve property value from in-memory RS.
		 * If the given property is not specified in RT RTR file,
		 * will get error SCHA_ERR_PROP.
		 */
		result->ret_code = get_rs_prop_val(args->rs_prop_name,
		    resource, &type, val_str, &val_array, &need_free,
		    &val_array_rdep).err_code;

		if (result->ret_code != SCHA_ERR_NOERR) {
			ucmm_print("RGM", NOGET(
			    "couldn't find property: returning\n"));
			goto finished; //lint !e801
		}

		/*
		 * store the value of the given property to the returned RPC
		 * buffer
		 */
		if (val_array_rdep) {
			// The structure to be stored to the returned
			// RPC buffer is
			// of type rdeplist. The property being referenced
			// is one of
			// the four types of dependencies.
			result->ret_code = store_prop_val_dep(type,
			    val_array_rdep, &ret_list,
			    (boolean_t)args->qarg).err_code;
		} else {
			result->ret_code = store_prop_val(type, val_array,
			    val_str, B_FALSE, NULL, &ret_list, NULL,
			    NULL).err_code;
		}
	}

	if (result->ret_code != SCHA_ERR_NOERR) {
		if (result->seq_id)
			free(result->seq_id);
		goto finished; //lint !e801
	}
	result->value_list = ret_list;

	/*
	 * All Rs within a RG share the same seq_id.
	 *
	 * Note, for the NUM_*_RESTARTS queries, seq_id will be ignored
	 * by the caller because the value is fetched from memory rather than
	 * the CCR.
	 */
	result->seq_id = strdup_nocheck(r->rl_rg->rgl_ccr->rg_stamp);
	if (result->seq_id == NULL) {
		result->ret_code = SCHA_ERR_NOMEM;
		ucmm_print("RGM", NOGET("get_res: failed on strdup\n"));
		goto finished; //lint !e801
	}

finished :

	/*
	 * If need_free is TRUE, need to free the buffer 'val_str'
	 * or val_array whichever is not null.
	 */
	if (need_free) {
		if (val_str[0] != NULL)
			free(val_str[0]);
		if (val_array != NULL)
			rgm_free_nlist(val_array);
	}
	rgm_unlock_state();

}

/*
 * get_all_extprops()
 *
 *	This routine gets all resource extension property names from
 *	the state structure in memory.
 */
void
get_all_extprops(rs_get_args *args, get_result_t *result)
{

	rgm_resource_t	*resource = NULL;
	rgm_property_list_t *pp = NULL;
	strarr_list	*ret_list = NULL;
	int		i, cnt;
	rlist_p_t	r = NULL;
	rglist_p_t	rgp = NULL;

	result->ret_code = SCHA_ERR_NOERR;
	result->value_list = NULL;

	rgm_lock_state();

	/*
	 * Get pointer to resource in our in-memory state structure.
	 */
	if ((r = rname_to_r(NULL, args->rs_name)) == NULL) {
		result->ret_code = SCHA_ERR_RSRC;
		ucmm_print("RGM",
		    NOGET("couldn't get resource <%s>\n"), args->rs_name);
		goto finished; //lint !e801
	}
	resource = r->rl_ccrdata;

	/*
	 * Traverse the list of properties getting all property names
	 *
	 */
	cnt = 0;
	for (pp = resource->r_ext_properties; pp != NULL;
	    pp = pp->rpl_next) {
		cnt++;
	}

	if ((ret_list = (strarr_list *) calloc_nocheck(1,
	    sizeof (strarr_list))) == NULL) {
		ucmm_print("RGM", NOGET("couldn't calloc: returning\n"));
		result->ret_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	/*
	 * all Rs share the same seq_id with RG.
	 */
	rgp = r->rl_rg;
	result->seq_id = strdup_nocheck(rgp->rgl_ccr->rg_stamp);
	if (result->seq_id == NULL) {
		result->ret_code = SCHA_ERR_NOMEM;
		if (ret_list != NULL)
			free(ret_list);
		ucmm_print("RGM",
		    NOGET("get_res: failed on strdup\n"));
		goto finished; //lint !e801
	}

	/*
	 * If no extension property found, set ret_list->strarr_list_len to 0
	 * and ret_list->strarr_list_val to NULL and return
	 */
	if (cnt == 0) {
		ret_list->strarr_list_len = 0;
		ret_list->strarr_list_val = NULL;
		result->value_list = ret_list;
		ucmm_print("RGM", NOGET(
		    "no any extension property found: returning\n"));
		goto finished; //lint !e801
	}

	if ((ret_list->strarr_list_val =
	    (char **)calloc_nocheck((size_t)cnt, sizeof (char *))) == NULL) {
		ucmm_print("RGM", NOGET("couldn't calloc: returning\n"));
		if (ret_list != NULL)
			free(ret_list);
		if (result->seq_id)
			free(result->seq_id);
		result->ret_code = SCHA_ERR_NOMEM;
		goto finished;
	}

	ret_list->strarr_list_len = (uint_t)cnt;
	i = 0;
	for (pp = resource->r_ext_properties; pp != NULL;
	    pp = pp->rpl_next) {
		if ((ret_list->strarr_list_val[i] =
		    strdup_nocheck(pp->rpl_property->rp_key)) == NULL) {
			if (ret_list->strarr_list_val != NULL)
				free(ret_list->strarr_list_val);
			if (ret_list != NULL)
				free(ret_list);
			if (result->seq_id)
				free(result->seq_id);
			ucmm_print("RGM",
			    NOGET("couldn't strdup: returning\n"));
			result->ret_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}
		i++;
	}
	result->value_list = ret_list;

finished :
	rgm_unlock_state();

}

/*
 * get_res_status()
 *
 *	get resource FM status and status messsage from President node
 */
void
get_res_status(rs_get_state_status_args *args, get_status_result_t *result)
{

	result->ret_code = SCHA_ERR_NOERR;

	/*
	 * send a request to the President via ORB RPC call to get the
	 * the given R FM status and status message
	 */
	result->ret_code = call_scha_rs_get_status(args, result).err_code;
	if (result->ret_code != SCHA_ERR_NOERR)
		ucmm_print("RGM", NOGET(
		    "couldn't get R FM status. Error=%d\n"), result->ret_code);
	else
		ucmm_print("RGM", NOGET(
		    "get_res_status: FM status = %d\n"), result->rs_status);
}

/*
 * get_res_state()
 *
 *	get resource state from President node
 */
void
get_res_state(rs_get_state_status_args *args, get_result_t *result)
{

	result->ret_code = SCHA_ERR_NOERR;

	/*
	 * send a request to the President via ORB RPC call to get the
	 * then given RS state
	 */
	result->ret_code = call_scha_rs_get_state(args, result).err_code;
	if (result->ret_code != SCHA_ERR_NOERR)
		ucmm_print("RGM", NOGET("couldn't get RS state. error = %d\n"),
		    result->ret_code);
	else
		ucmm_print("RGM", NOGET(
		    "get_res_state: state = %s\n"),
		    result->value_list->strarr_list_val[0]);
}

/*
 * get_failed_status()
 *
 *	get the FINI, INIT, BOOT or UPDAGE method failed status from
 *	President node
 */
void
get_failed_status(rs_get_fail_status_args *args, get_result_t *result)
{

	result->ret_code = SCHA_ERR_NOERR;

	/*
	 * send the request to the President via ORB RPC call to get
	 * the failed FINT, INIT, BOOT and UPDATE state
	 */
	result->ret_code = call_scha_rs_get_fail_status(args, result).err_code;
	if (result->ret_code != SCHA_ERR_NOERR)
		ucmm_print("get_failed_status()", NOGET(
		    "couldn't get failed status.  error = %d\n"),
		    result->ret_code);
	else
		ucmm_print("RGM", NOGET(
		    "get_failed_status: status = %s\n"),
		    result->value_list->strarr_list_val[0]);
}

/*
 * set_res_status()
 *
 *	Update the resource status and the status message on the local node
 *	and on the president.
 */
void
set_res_status(rs_set_status_args *args, set_status_result_t *result)
{

	/*
	 * Call call_scha_rs_set_status() to update the resource status
	 * and status message on the local node and on the president.
	 *
	 * call_scha_rs_set_status checks for president node death or rgmd
	 * death, sleeps until new president is elected, and re-tries the
	 * call.  So we don't have to check for rgmd/node death here.
	 */
	result->ret_code = call_scha_rs_set_status(args, result).err_code;
	if (result->ret_code == SCHA_ERR_NOERR)
		ucmm_print("RGM", NOGET("set_res_status(): set RS FM status: "
		    "<%d>\n"), args->status);
}

/*
 * get_rgname()
 *
 *	obtain the RG name associated with the Resource name passed in
 *	to this routine as an argument.  The RG sequence id is used
 *	as the Resource sequence id.
 */
void
get_rgname(rs_get_args *args, get_result_t *result)
{
	char		*rg_name;
	strarr_list	*ret_list;
	std::map<rgm::lni_t, char *> tmp;
	rglist_p_t	rgp = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	result->ret_code = SCHA_ERR_NOERR;
	result->value_list = NULL;

	rgm_lock_state();

	/*
	 * read the CCR to get the RG name
	 * ignoring the return value, res, because we are
	 * checking for rg_name not being null, which is the
	 * same as res.err_code != SCHA_ERR_NOERR
	 */
	res = rgm_rsrc_on_which_rg(args->rs_name, &rg_name, ZONE);
	if (rg_name == NULL) {
		ucmm_print("RGM", NOGET(
		    "get_rgname: cannot find RG in CCR\n"));
		result->ret_code = SCHA_ERR_RSRC;
		goto finished; //lint !e801
	}

	/*
	 * For each RG, all Rs share the same seq_id with RG.
	 * The RG name argument is optional; use the rg_name
	 * returned from rgm_rsrc_on_which_rg().
	 */
	if ((rgp = rgname_to_rg(rg_name)) == NULL) {
		ucmm_print("RGM", SYSTEXT(
		    "RG <%s> doesn't exist."), rg_name);
		result->ret_code = SCHA_ERR_RG;
		goto finished; //lint !e801
	}
	result->seq_id = strdup_nocheck(rgp->rgl_ccr->rg_stamp);
	if (result->seq_id == NULL) {
		result->ret_code = SCHA_ERR_NOMEM;
		ucmm_print("RGM",
		    NOGET("get_rgname: failed on strdup\n"));
		goto finished;
	}
	if (rg_name != NULL) {
		tmp[0] = strdup(rg_name);
		result->ret_code = store_prop_val(SCHA_PTYPE_STRING,
		    NULL, tmp, B_FALSE, NULL, &ret_list, NULL, NULL).err_code;
		if (result->ret_code != SCHA_ERR_NOERR) {
			if (result->seq_id)
				free(result->seq_id);
			goto finished; //lint !e801
		}
		result->value_list = ret_list;
	} else
		result->value_list = NULL;
finished:
	if (rg_name != NULL)
		free(rg_name);
	if (tmp[0] != NULL)
		free(tmp[0]);
	free(res.err_msg);
	rgm_unlock_state();
}
