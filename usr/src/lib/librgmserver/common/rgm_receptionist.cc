/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_receptionist.cc	1.112	09/04/13 SMI"

/*
 * rgm_receptionist.cc is receptionist component of rgmd
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <netdb.h>
#include <rgm/security.h>
#include <sys/vc_int.h>

#include <rgm/rgm_recep.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <libzccfg/libzccfg.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif
#if DOOR_IMPL
#include "rgm/door/rgm_recep_door.h"
#include <alloca.h>
#include <string.h>
#endif

#include <sys/os.h>
#include <rgm/rgm_msg.h>
#include <rgm_proto.h>
#include <rgm_state.h>
#include <rgm_api.h>
#include <rgm_threads.h>
#include <rgm_proto.h>
#include <scadmin/scconf.h>
#include <scha.h>

#include <rgmx_hook.h>

#if SOL_VERSION >= __s10
#include <privip_map.h>
#endif

typedef enum scha_req_type {
	SCHA_REQ_TYPE_RG = 0,
	SCHA_REQ_TYPE_RS
} scha_req_type_t;

static void get_priv_hostnames(char *nodename, char *zonename,
    get_result_t *result);
#if SOL_VERSION >= __s10
static boolean_t client_has_authorization(void *scha_args,
    ucred_t *ucred, scha_req_type obj_type);
static boolean_t does_r_have_dependents_in_zc(rlist_t *res_info, char *zc_name);
static boolean_t does_zc_have_dependent_rs(rdeplist_t *dep_res, char *zc_name);
#endif

#if DOOR_IMPL
#include <alloca.h>
#include <string.h>


// Helper function to convert strarr_list to idl_string_seq_t
void
array_to_strseq_2(strarr_list *str_list, rgm::idl_string_seq_t &str_seq)
{
	uint_t i = 0;
	char **p;

	// Treat NULL as an empty list
	if (str_list == NULL) {
		str_seq.length(0);
		return;
	}

	// Set the length of the sequence
	i = str_list->strarr_list_len;
	str_seq.length(i);

	// Fill in the sequence
	i = 0;
	p = str_list->strarr_list_val;

	while (i < str_list->strarr_list_len) {
		str_seq[i] = new_str(*p);
		i++;
		p++;
	}
}


/*
 * recep_door_server :
 *
 * This is the process which acts as the server for the
 * rgm_receptionist. All the libscha calls which are made
 * will come to this server which will determine the API
 * that has been called and then appropriately call the
 * corresponding function. And after the function returns
 * it will send back the results.
 * All the data that comes from the libscha is marshalled
 * using the xdr library and so the server needs to unmarshall
 * it before it can call the api functions.
 * Similarly, when the server wants to return the results
 * back to the calling function it marshall's the results
 * which are unmarshalled at the other end to get the
 * results.
*/
void recep_door_server(void *,
    char *dataptr, size_t data_size,
    door_desc_t *, size_t)
{
	scha_input_args_t *scha_args = NULL;
		// Used to store the input args after unmarshalling

	XDR xdrs, xdrs_result, xdrs_error;
		// XDR Streams for marshalling and unmarshalling
	void *result = NULL;
		// Used for pointing to the result structure which differs
		// based on the api that has been called
	char *result_buf = NULL;
	char *error_buf = NULL;
		// result_buf is used for storing the result data after
		// marshalling error_buf is used for returning the error
		// or return_code to the callee

	size_t result_size;
		// this stores the result data size that is returned to the
		// callee we will allocate this on the stack (using alloca)
		// because this data will be returned using door_return,
		// since we wont be able to come back and free this. Being
		// on the stack it will be freed automatically.
	int return_code = 0;
	int num_retries = XDR_NUM_RETRIES;
	sc_syslog_msg_handle_t handle;
#if SOL_VERSION >= __s10
	ucred_t *ucred = NULL;
		// ucred will contain the credentials of the client making
		// this door call.
#endif

	if (data_size == 0) {
		/*
		 * This should not happen but we check for it, just
		 * to be sure that the size of the arguments is not
		 * nil.
		 */
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Incoming argument size is zero."));
		sc_syslog_msg_done(&handle);
		return_code = DOOR_INCOMING_ARG_SIZE_NULL;
		goto error_return; //lint !e801
	}

#if SOL_VERSION >= __s10
	// We have to get the credentials of the client since we will
	// check for authorization later.
	if (door_ucred(&ucred) != 0) {
		// We could not read the credentials. We have to report
		// this as an error.
		return_code = INVALID_CREDENTIALS;
		goto error_return; //lint !e801
	}
#endif

	scha_args = (scha_input_args_t *)calloc(1, sizeof (scha_input_args_t));
	if (scha_args == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Unable to allocate memory at door server"));
		sc_syslog_msg_done(&handle);
		return_code = UNABLE_TO_ALLOCATE_MEM;
		goto error_return; //lint !e801
	}

	xdrmem_create(&xdrs, dataptr, data_size, XDR_DECODE);

	if (!xdr_scha_input_args(&xdrs, scha_args)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_PMF_PMFD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("XDR Error while decoding arguments."));
		sc_syslog_msg_done(&handle);
		return_code = XDR_ERROR_AT_SERVER;
		goto error_return; //lint !e801
	}

	switch (scha_args->api_name) {
	case SCHA_RS_OPEN:
		result = (rs_open_result_t *)calloc(1,
		    sizeof (rs_open_result_t));
#if SOL_VERSION >= __s10
		// Check for authorization
		if (!client_has_authorization((void *)
				(scha_args->data), ucred,
				SCHA_REQ_TYPE_RS)) {
			return_code = INVALID_CREDENTIALS;
			goto error_return;
		}
		// If we are here, then this client request has
		// enough authorization.
#endif
		if (!scha_rs_open_1_svc((rs_open_args *)(scha_args->data),
		    (rs_open_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_RS_GET:
		result = (get_result_t *)calloc(1, sizeof (get_result_t));
		if (!scha_rs_get_1_svc((rs_get_args *)(scha_args->data),
		    (get_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_RS_GET_EXT:
		result = (get_ext_result_t *)calloc(1,
		    sizeof (get_ext_result_t));
		if (!scha_rs_get_ext_1_svc((rs_get_args *)(scha_args->data),
		    (get_ext_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_RS_GET_STATE:
		result = (get_result_t *)calloc(1, sizeof (get_result_t));
		if (!scha_rs_get_state_1_svc((rs_get_state_status_args *)
		    (scha_args->data), (get_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_RS_GET_STATUS:
		result = (get_status_result_t *)calloc(1,
		    sizeof (get_status_result_t));
		if (!scha_rs_get_status_1_svc((rs_get_state_status_args *)
		    (scha_args->data), (get_status_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_RS_GET_FAILED_STATUS:
		result = (get_result_t *)calloc(1, sizeof (get_result_t));
		if (!scha_rs_get_failed_status_1_svc((rs_get_fail_status_args *)
		    (scha_args->data), (get_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_GET_ALL_EXTPROPS:
		result = (get_result_t *)calloc(1, sizeof (get_result_t));
		if (!scha_get_all_extprops_1_svc((rs_get_args *)(
		    scha_args->data), (get_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}
		break;
	case SCHA_IS_PERNODE:
		result = (get_result_t *)calloc(1, sizeof (get_result_t));
		if (!scha_is_pernode_1_svc((rt_get_args *)(
		    scha_args->data), (get_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}
		break;
	case SCHA_RS_SET_STATUS:
		result = (set_status_result_t *)calloc(1,
		    sizeof (set_status_result_t));
		if (!scha_rs_set_status_1_svc((rs_set_status_args *)(
		    scha_args->data), (set_status_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_RT_OPEN:
		result = (open_result_t *)calloc(1, sizeof (open_result_t));
		if (!scha_rt_open_1_svc((rt_open_args *)(scha_args->data),
		    (open_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_RT_GET:
		result = (get_result_t *)calloc(1, sizeof (get_result_t));
		if (!scha_rt_get_1_svc((rt_get_args *)(scha_args->data),
		    (get_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_RG_OPEN:
#if SOL_VERSION >= __s10
		// Check for authorization
		if (!client_has_authorization((void *)
				(scha_args->data), ucred,
				SCHA_REQ_TYPE_RG)) {
			return_code = INVALID_CREDENTIALS;
			goto error_return;
		}

		// The client was authorized.
#endif
		result = (open_result_t *)calloc(1, sizeof (open_result_t));
		if (!scha_rg_open_1_svc((rg_open_args *)(scha_args->data),
		    (open_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_RG_GET:
		result = (get_result_t *)calloc(1, sizeof (get_result_t));
		if (!scha_rg_get_1_svc((rg_get_args *)(scha_args->data),
		    (get_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}
		break;
	case SCHA_RS_GET_SWITCH:
		result = (get_switch_result_t *)
		    calloc(1, sizeof (get_switch_result_t));
		if (!scha_rs_get_switch_1_svc((rs_get_switch_args *)
		    (scha_args->data),
		    (get_switch_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}
		break;
	case SCHA_RG_GET_STATE:

		result = (get_result_t *)calloc(1, sizeof (get_result_t));
		if (!scha_rg_get_state_1_svc((rg_get_state_args *)(
		    scha_args->data), (get_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_CLUSTER_GET:
		result = (get_result_t *)calloc(1, sizeof (get_result_t));
		if (!scha_cluster_get_1_svc((cluster_get_args *)(
		    scha_args->data), (get_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_SSM_IP:
		result = (ssm_result_t *)calloc(1, sizeof (ssm_result_t));
		if (!scha_ssm_ip_1_svc((ssm_ip_args *)(scha_args->data),
		    (ssm_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}

		break;
	case SCHA_LB:
		result = (scha_ssm_lb_result_t *)calloc
		    (1, sizeof (scha_ssm_lb_result_t));
		if (!scha_lb_1_svc((scha_ssm_lb_args *)(scha_args->data),
		    (scha_ssm_lb_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}
		break;
	case SCHA_CLUSTER_OPEN:
		result = (open_result_t *)calloc(1, sizeof (open_result_t));
		if (!scha_cluster_open_1_svc((void *)(scha_args->data),
		    (open_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}
		break;
	case SCHA_CONTROL:
		result = (scha_result_t *)calloc(1, sizeof (open_result_t));
		if (!scha_control_1_svc((scha_control_args *)(scha_args->data),
		    (scha_result_t *)result)) {
			return_code = FALSE_RET_FROM_API_FN;
			goto error_return; //lint !e801
		}
		break;
	default:
		return_code = INVALID_API_NAME;
		goto error_return; //lint !e801
	}

	result_size = scha_xdr_sizeof_result(result, scha_args->api_name);

	//
	// For encoding the arguments we will try XDR_NUM_RETRIES times
	//
	for (num_retries = XDR_NUM_RETRIES; num_retries > 0; num_retries--) {
		result_buf = (char *)alloca(result_size);
		if (result_buf == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("Unable to allocate memory at door "
			    "server"));
			sc_syslog_msg_done(&handle);
			return_code = UNABLE_TO_ALLOCATE_MEM;
			goto error_return; //lint !e801
		}
		(void) memset(result_buf, 0, result_size);

		xdrmem_create(&xdrs_result, result_buf, result_size,
		    XDR_ENCODE);

		if (!xdr_int(&xdrs_result, &return_code)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("XDR Error while encoding return "
			    "arguments."));
			sc_syslog_msg_done(&handle);
			return_code = XDR_ERROR_AT_SERVER;
			goto error_return; //lint !e801
		}

		if (!xdr_scha_result_args(&xdrs_result, result,
		    scha_args->api_name)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// A non-fatal error occurred while rgmd daemon was
			// marshalling arguments for a remote procedure call.
			// The operation will be re-tried with a larger
			// buffer.
			// @user_action
			// No user action is required. If the message recurs
			// frequently, contact your authorized Sun service
			// provider to determine whether a workaround or patch
			// is available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("RGMD XDR Buffer Shortfall while "
			    "encoding return arguments API num = %d. "
			    "Will retry"), scha_args->api_name);
			sc_syslog_msg_done(&handle);
			//
			// Will retry to encode the result
			// this time with a larger buffer
			//
			if (num_retries) {
				result_size += EXTRA_BUFFER_SIZE;
				free(result_buf);
				continue;
			}
			//
			// Unable to encode the arguments even after
			// retry.
			//
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("XDR Error while encoding return "
			    "arguments."));
			sc_syslog_msg_done(&handle);
			return_code = XDR_ERROR_AT_SERVER;
			goto error_return; //lint !e801
		} else {
			break; // Successful Encoding of the arguments
		}
	}
	result_size = xdr_getpos(&xdrs_result);

	// Need to do an xdr_free for the allocated memory
	if (result != NULL) {
		scha_result_args_xdr_free(scha_args->api_name, result);
		free(result);
	}
	if (scha_args->data != NULL) {
		scha_input_args_xdr_free(scha_args);
		free(scha_args);
	}
	xdr_destroy(&xdrs);
	xdr_destroy(&xdrs_result);

#if SOL_VERSION >= __s10
	/* free ucred struct */
	if (ucred != NULL)
		ucred_free(ucred);
#endif

	(void) door_return(result_buf, result_size, NULL, 0);

error_return:
	error_buf = (char *)alloca(RNDUP(sizeof (int)));
	xdrmem_create(&xdrs_error, error_buf, RNDUP(sizeof (int)), XDR_ENCODE);
	if (!xdr_int(&xdrs_error, &return_code)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Severe XDR Error, Cannot recover."));
		sc_syslog_msg_done(&handle);
#if SOL_VERSION >= __s10
		/* free ucred struct */
		if (ucred != NULL)
			ucred_free(ucred);
#endif
		//
		// Since there is some error while calling
		// XDR lib we will not be able to encode any
		// error code to the calling client function.
		// Hence, We will return with all parameters zero
		// this has to be checked at the client end
		// and the appropriate error has to be
		// sent to the caller.
		//
		(void) door_return(NULL, 0, NULL, 0);
	}

	xdr_destroy(&xdrs_error);

#if SOL_VERSION >= __s10
	/* free ucred struct */
	if (ucred != NULL)
		ucred_free(ucred);
#endif

	(void) door_return(error_buf, RNDUP(sizeof (int)), NULL, 0);
}

#if SOL_VERSION >= __s10
// client_has_authorization(res_args)
// This method will check whether the client which is making
// this door call has enough credentials to contact this rgmd
// or not. Essentially, this check is here to prevent a zone cluster
// door call from sending R/RG information to another zone cluster.
// The only place where a zone cluster 'a' can see some information
// about another zone cluster 'b' is when a resource in 'a' has
// a dependency on a resource in 'b'.
// This function will return true, if the client has the necessary
// authorization, else it will return a false.
boolean_t client_has_authorization(void *args, ucred_t *ucred,
    scha_req_type obj_type)
{

	zoneid_t zone_id_client;
	char zc_name[ZONENAME_MAX];
	int clconf_retval = 0;
	boolean_t client_authorized = B_FALSE;
	uint_t zc_id = 0;
	rglist_t *rg_ptr;
	rlist_t *rs_ptr;
	rs_open_args *res_args;
	rg_open_args *rg_args;

	if (ucred == NULL) {
		return (B_FALSE);
	}

	zone_id_client = ucred_getzoneid(ucred);

	if (zone_id_client < 0) {
		// zone ID is not available.
		return (B_FALSE);
	}

	if (zone_id_client == 0) {
		// This request was made from a global zone.
		return (B_TRUE);
	}

	// If we are here, it means that this request came from
	// a zone cluster. We have to check whether this rgmd is
	// running for the same zone cluster or not.
	if ((getzonenamebyid(zone_id_client, zc_name, ZONENAME_MAX)) == -1) {
		// Invalid zone name??
		goto finish_authority_check; //lint !e801
	}

	// zone cluster check.
	clconf_retval = clconf_get_cluster_id(zc_name, &zc_id);
	if ((clconf_retval != 0) || (zc_id < MIN_CLUSTER_ID)) {
		// this request was not made from a zone cluster.
		// If we are the global zone rgmd, then we have to
		// allow requests from native zones.
		if (strcmp(ZONE, GLOBAL_ZONENAME) == 0) {
			client_authorized = B_TRUE;
		}

		goto finish_authority_check; //lint !e801
	}

	// If we are here, it means that this request was made from
	// a valid zone cluster.
	if (strcmp(zc_name, ZONE) == 0) {
		// This request was from our zone cluster member.
		client_authorized = B_TRUE;
		goto finish_authority_check; //lint !e801
	}

	// So, now we are at a point where the request came from
	// the member of a zone cluster which we are not managing.
	// We will allow this guy only if this guy has a resource
	// which is dependent on this R.

	// If this was a scha_resource request, then we have to check
	// the dependency only for this R.
	// If this was a scha_resourcegroup request, then we have to
	// check the dependency for all the Rs in this RG

	if (obj_type == SCHA_REQ_TYPE_RS) {
		// This was a scha_resource request.
		res_args = (rs_open_args *)args;
		if ((rs_ptr = rname_to_r(NULL, res_args->rs_name)) == NULL) {
			// This R does not exist or we are not able to fetch
			// it right now.
			client_authorized = B_FALSE;
			goto finish_authority_check; //lint !e801
		}
		// Now check whether any of the Rs in the ZC have a dependency
		// on this R.
		if (does_r_have_dependents_in_zc(rs_ptr, zc_name)) {
			// Ok, there was a dependency, allow this guy.
			client_authorized = B_TRUE;
		}

		// We are done checking for R
		goto finish_authority_check; //lint !e801
	}

	// If we are here,  then this was a scha_resourcegroup request.
	rg_args = (rg_open_args *)args;
	if ((rg_ptr = rgname_to_rg(rg_args->rg_name)) == NULL) {
		// This R does not exist or we are not able to fetch
		// it right now.
		client_authorized = B_FALSE;
		goto finish_authority_check; //lint !e801
	}

	// Now, we have to go through all the Rs in this RG and check
	// whether any R has a dependent in the ZC zc_name
	for (rs_ptr = rg_ptr->rgl_resources; rs_ptr != NULL;
		rs_ptr = rs_ptr->rl_next) {
		// check for dependents in ZC
		if (does_r_have_dependents_in_zc(rs_ptr, zc_name)) {
			// This R has a dependent in zc_name.
			client_authorized = B_TRUE;
			break;
		}
	}

finish_authority_check:

	return (client_authorized);
}

boolean_t
does_r_have_dependents_in_zc(rlist_t *res_info, char *zc_name)
{
	// Invalid params??
	if ((res_info == NULL) || (zc_name == NULL)) {
		return (B_FALSE);
	}

	// First, we check whether this R has any external dependencies.
	// If it does not even have any external dependencies, then there
	// no need to check any further.
	if (res_info->rl_ccrdata->r_inter_cluster_dependents.dp_strong
		== NULL &&
	    res_info->rl_ccrdata->r_inter_cluster_dependents.dp_weak
		== NULL &&
	    res_info->rl_ccrdata->r_inter_cluster_dependents.
	    dp_offline_restart == NULL &&
	    res_info->rl_ccrdata->r_inter_cluster_dependents.
	    dp_restart == NULL) {
		// There are no inter-cluster-resources dependent on this
		// R. So, there wont be any external dependents.
		return (B_FALSE);
	}

	// Ok. So, it boils down to checking the resource dependents now.
	if (does_zc_have_dependent_rs(
		res_info->rl_ccrdata->r_inter_cluster_dependents.dp_strong,
		zc_name)) {
		// There was a strong dependency.
		return (B_TRUE);
	}
	if (does_zc_have_dependent_rs(
		res_info->rl_ccrdata->r_inter_cluster_dependents.dp_weak,
		zc_name)) {
		// There was a weak dependency.
		return (B_TRUE);
	}
	if (does_zc_have_dependent_rs(
		res_info->rl_ccrdata->r_inter_cluster_dependents.
		dp_offline_restart, zc_name)) {
		// There was an offline restart dependency.
		return (B_TRUE);
	}
	if (does_zc_have_dependent_rs(
		res_info->rl_ccrdata->r_inter_cluster_dependents.dp_restart,
		zc_name)) {
		// There was a restart dependency.
		return (B_TRUE);
	}

	// If we are here, it means there are no dependents of this R
	// in zc_name
	return (B_FALSE);
}

boolean_t
does_zc_have_dependent_rs(rdeplist_t *dep_res, char *zc_name)
{
	boolean_t zc_res_flag = B_FALSE;
	rdeplist_t *curr_res = NULL;
	char *curr_zc = NULL;

	if (zc_name == NULL) {
		// We should not be here ideally.
		return (zc_res_flag);
	}

	// Iterate through the dependent Rs list
	for (curr_res = dep_res; curr_res != NULL;
	    curr_res = curr_res->rl_next) {

		if (curr_zc) {
			free(curr_zc);
			curr_zc = NULL;
		}

		// Lets parse the ZC name from the resource name.
		get_remote_cluster_name(curr_res->rl_name, &curr_zc);

		if (curr_zc == NULL) {
			continue;
		}

		if (strcmp(zc_name, curr_zc) == 0) {
			// Ok. Here it is...
			zc_res_flag = B_TRUE;
			break;
		}
	}

	// Free any memory
	if (curr_zc) {
		free(curr_zc);
	}

	return (zc_res_flag);
}
// End of Sol10 ifdef
#endif
// End of door ifdef
#endif

#ifndef EUROPA_FARM
/*
 * get_logicalnodesfrompres
 * ------------------------
 * Makes an idl call to the rgmd president to retrieve a list
 * of all logical nodes known to the rgmd. Only the president
 * has a complete list of all logical nodes, including all zones on
 * all physical nodes.
 */
static scha_err_t
get_logicalnodesfrompres(sol::nodeid_t nodeid, bool exclude_global,
    strarr_list *ret_list)
{
	rgm::idl_string_seq_t *arg;
	Environment e;
	rgm::rgm_comm_var prgm_pres_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};



	ret_list->strarr_list_val = NULL;
	// check president itself. in that case read directly
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
	if (CORBA::is_nil(prgm_pres_v)) {
		return (SCHA_ERR_INTERNAL);
	}
	prgm_pres_v->idl_get_logicalnodes(nodeid, exclude_global, arg, e);
	res = except_to_scha_err(e);
	if (res.err_code != SCHA_ERR_NOERR) {
		return (res.err_code);
	}
	// copy the string seq to array
	ret_list->strarr_list_len = arg->length();
	if ((ret_list->strarr_list_val = (char **)calloc_nocheck(
		(size_t)ret_list->strarr_list_len, sizeof (char *))) == NULL) {
		ucmm_print("RGM",
		    NOGET("SCHA_ALL_NODENAMES, couldn't calloc\n"));
		return (SCHA_ERR_NOMEM);
	}
	for (uint_t i = 0; i < ret_list->strarr_list_len; i++) {
		ret_list->strarr_list_val[i] = strdup((*arg)[i]);
	}
	return (SCHA_ERR_NOERR);

}
#endif // !EUROPA_FARM

/*
 * This method validates that the scha_control (IGNORE_FAILED_START)
 * corresponds to a start/prenet_start method.
 * Returns:
 * - SCHA_ERR_INTERNAL: Cannot retrieve information from the list
 *			of in-flight methods.
 * - SCHA_ERR_NOMEM: Not enough memory
 * - SCHA_ERR_INVAL: This is not a start/prenet_start method
 * - SCHA_ERR_NOERR: Validation succesful
 */
static scha_err_t
is_start_method(rglist_p_t rg, char *r_name, char *zonename)
{
	char		*fed_opts_id;
	char		*resource_str;
	char		*method_str;
	char		*zonename_str;
	methodinvoc_t	*current_mip;
	int		method_id;
	char		*str;

	/*
	 * Iterate through the list of fed tags in-flight methods
	 * until we find the one corresponding for this resource.
	 *
	 */
	for (current_mip = rg->rgl_methods_in_flight; current_mip != NULL;
	    current_mip = current_mip->mi_next) {

		/*
		 * This is a string of the form
		 * "[zonename.]rg_name.rs_name.method_id" ([%s.]%s.%s.%d).
		 */
		fed_opts_id = current_mip->mi_tag;
		if (NULL == fed_opts_id) {
			ucmm_print("RGM",
			    NOGET("is_start_method: NULL pointer in the "
				"list of in-flight methods\n"));
			return (SCHA_ERR_INTERNAL);
		}

		/* Copy the string to perform manipulation. */
		str = strdup_nocheck(fed_opts_id);
		if (NULL == str) {
			return (SCHA_ERR_NOMEM);
		}

		/* Extract method id (string), resource name, zonename */
		method_str = strrchr(str, '.');
		*method_str = '\0';
		method_str++;

		resource_str = strrchr(str, '.');
		*resource_str = '\0';
		resource_str++;

		zonename_str = strrchr(str, '.');
		if (zonename_str != NULL) {
			/* We have a non-global zonename */
			*zonename_str = '\0';
			zonename_str = str;
		}

		/* Make sure we're in the correct zone */
		if ((zonename == NULL && zonename_str != NULL) ||
		    (zonename != NULL && zonename_str == NULL) ||
		    (zonename && zonename_str &&
		    strcmp(zonename, zonename_str) != 0)) {
			free(str);
			continue;
		}

		/*
		 * If this is the resource, check if this is a
		 * start/prenet_start method.
		 */
		if (strcmp(r_name, resource_str) == 0) {

			method_id = atoi(method_str);
			free(str);

			if ((METH_START == method_id) ||
			    (METH_PRENET_START == method_id)) {
				return (SCHA_ERR_NOERR);
			} else {
				return (SCHA_ERR_INVAL);
			}
		}
		free(str);
	}
	return (SCHA_ERR_INVAL);
} //lint !e818


/*
 * scha_rs_open_1_svc() -
 *
 *	this function calls open_rs routine to retrieve the
 *	sequence id from in-memory state structure
 *
 */
#if DOOR_IMPL
bool_t
scha_rs_open_1_svc(rs_open_args *arg, rs_open_result_t *result)
#else
bool_t
scha_rs_open_1_svc(rs_open_args *arg,
    rs_open_result_t *result, struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (rs_open_result_t));

	/*
	 * Check security:
	 * SEC_UNIX_STRONG means that we're checking if
	 * the call came from the loopback transport,
	 * and that the userid matches the one requested - in this
	 * case root. The last arg value indicates that the caller
	 * doesn't have to be root.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred, SEC_UNIX_STRONG,
	    FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}
	open_res(arg, result);
	if (result->ret_code == SCHA_ERR_NOERR) {
		ducmm_print("RGM", NOGET("scha_rs_open: "
		    "RG = %s, R = %s, R seqid = %s, RT seqid = %s\n"),
		    arg->rg_name, arg->rs_name, result->rs_seq_id,
		    result->rt_seq_id);
	} else {
		ducmm_print("RGM",
		    NOGET("scha_rs_open: failed\n"));
	}
	return (TRUE);
}

/*
 * scha_rs_get_ext_1_svc() -
 *
 *	this function calls get_rs_ext routine to retrieve the
 *	sequence id and the value of the given resource extension property
 *	from in-memory state structure.
 *
 */
#if DOOR_IMPL
bool_t
scha_rs_get_ext_1_svc(rs_get_args *arg, get_ext_result_t *result)
#else
bool_t
scha_rs_get_ext_1_svc(rs_get_args *arg, get_ext_result_t *result,
    struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (get_ext_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}

	//
	// Note, for CZ rgmd, node n:z is equivalent to node n.
	//
	get_res_ext(arg, result);
	if (result->ret_code == SCHA_ERR_NOERR)
		ducmm_print("RGM", NOGET("scha_rs_get_ext: "
		    "RG = %s, R = %s, TAG = %s, seqid = %s\n"),
		    arg->rg_name, arg->rs_name,
		    arg->rs_prop_name, result->seq_id);
	else {
		ducmm_print("RGM",
		    NOGET("scha_rs_get_ext: failed\n"));
	}
	return (TRUE);
}

/*
 * scha_rs_get_1_svc() -
 *
 *	this function calls get_rs routine to retrieve the
 *	sequence id and the value of an Resource property from RS CCR table
 *
 *	This function is also used to retrive "node local" information about
 *	a resource (not stored in ccr, not retrieved from president)
 */
#if DOOR_IMPL
bool_t
scha_rs_get_1_svc(rs_get_args *arg, get_result_t *result)
#else
bool_t
scha_rs_get_1_svc(rs_get_args *arg, get_result_t *result,
    struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (get_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}

	//
	// Note, for CZ rgmd, node n:z is equivalent to node n.
	//
	if ((strcmp(arg->rs_prop_name, SCHA_GROUP)) == 0)
		get_rgname(arg, result);
	else
		get_res(arg, result);

	if (result->ret_code == SCHA_ERR_NOERR)
		ducmm_print("RGM", NOGET("scha_rs_get: "
		    "RG = %s, R = %s, TAG = %s, seqid = %s\n"),
		    arg->rg_name, arg->rs_name,
		    arg->rs_prop_name,
		    result->seq_id ? result->seq_id : "(null)");
	else {
		ducmm_print("RGM",
		    NOGET("scha_rs_get: failed\n"));
	}

	return (TRUE);

}

/*
 * scha_rs_get_switch_1_svc() -
 *
 *      this function calls get_res_switch routine to retrieve the
 *      sequence id and the On_off/Monitored switch of a Resource.
 */
#if DOOR_IMPL
bool_t
scha_rs_get_switch_1_svc(rs_get_switch_args *arg,
    get_switch_result_t *result)
#else
bool_t
scha_rs_get_switch_1_svc(rs_get_switch_args *arg,
    get_switch_result_t *result, struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (get_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}

	//
	// Note, for CZ rgmd, node n:z is equivalent to node n.
	//
	get_res_switch(arg, result);
	if (result->ret_code == SCHA_ERR_NOERR) {
		ucmm_print("RGM", NOGET("scha_rs_get_switch: "
		    "RG = %s, R = %s, seqid = %s\n"),
		    arg->rg_name, arg->rs_name, result->seq_id);
	} else {
		ucmm_print("RGM",
		    NOGET("scha_rs_get_switch: failed\n"));
	}
	return (TRUE);
}

/*
 * scha_rs_get_state_1_svc() -
 *
 *	this function calls get_rs_state routine to retrieve the
 *	sequence id and the state of a Resource
 *
 */
#if DOOR_IMPL
bool_t
scha_rs_get_state_1_svc(rs_get_state_status_args *arg, get_result_t *result)
#else
bool_t
scha_rs_get_state_1_svc(rs_get_state_status_args *arg, get_result_t *result,
    struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (get_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}

	//
	// Note, for CZ rgmd, node n:z is equivalent to node n.
	//
	get_res_state(arg, result);
	if (result->ret_code == SCHA_ERR_NOERR) {
		ducmm_print("RGM", NOGET("scha_rs_get_state: "
		    "RG = %s, R = %s, seqid = %s\n"),
		    arg->rg_name, arg->rs_name, result->seq_id);
	} else {
		ducmm_print("RGM",
		    NOGET("scha_rs_get_state: failed\n"));
	}
	return (TRUE);

}

/*
 * scha_rs_get_status_1_svc() -
 *
 *	this function calls get_rs_state routine to retrieve the
 *	sequence id and the status of a Resource
 *
 */
#if DOOR_IMPL
bool_t
scha_rs_get_status_1_svc(rs_get_state_status_args *arg,
    get_status_result_t *result)
#else
bool_t
scha_rs_get_status_1_svc(rs_get_state_status_args *arg,
    get_status_result_t *result, struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (get_status_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}

	//
	// Note, for CZ rgmd, node n:z is equivalent to node n.
	//
	get_res_status(arg, result);
	if (result->ret_code == SCHA_ERR_NOERR) {
		ducmm_print("RGM", NOGET("scha_rs_get_status: "
		    "RG = %s, R = %s, seqid = %s\n"),
		    arg->rg_name, arg->rs_name, result->seq_id);
	} else {
		ducmm_print("RGM",
		    NOGET("scha_rs_get_status: failed\n"));
	}
	return (TRUE);
}

/*
 * scha_is_pernode_1_svc() -
 *
 *	this function calls get_rs_ispernode routine to retrieve the
 *	sequence id and is_pernode flag of the given property.
 *
 */
#if DOOR_IMPL
bool_t
scha_is_pernode_1_svc(rt_get_args *arg,
    get_result_t *result)
#else
bool_t
scha_is_pernode_1_svc(rt_get_args *arg,
    get_result_t *result, struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (get_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}
	get_ispernode_prop(arg, result);
	if (result->ret_code == SCHA_ERR_NOERR)
		ducmm_print("RGM",
		    NOGET("scha_is_pernode_1_svc: "
			"RT = %s seqid = %s\n"),
		    arg->rt_name, result->seq_id);
	else {
		ducmm_print("RGM",
		    NOGET("scha_is_pernode_1_svc: failed\n"));
	}

	return (TRUE);
}

/*
 * scha_rs_get_failed_status_1_svc() -
 *
 *	this function calls get_rs_failed_state routine to retrieve the
 *	sequence id and the failed state of a BOOT, INIT, FINI or UPDATE method
 *
 */
#if DOOR_IMPL
bool_t
scha_rs_get_failed_status_1_svc(rs_get_fail_status_args *arg,
    get_result_t *result)
#else
bool_t
scha_rs_get_failed_status_1_svc(rs_get_fail_status_args *arg,
    get_result_t *result, struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (get_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}

	//
	// Note, for CZ rgmd, node n:z is equivalent to node n.
	//
	get_failed_status(arg, result);
	if (result->ret_code == SCHA_ERR_NOERR)
		ducmm_print("RGM",
		    NOGET("scha_rs_get_failed_status: "
			"RG = %s, R = %s, seqid = %s\n"),
		    arg->rg_name, arg->rs_name, result->seq_id);
	else {
		ducmm_print("RGM",
		    NOGET("scha_rs_get_failed_status: failed\n"));
	}

	return (TRUE);
}

/*
 * scha_rs_set_status_1_svc() -
 *
 *	this function calls set_rs_state routine to set the
 *	the resource status and status message
 *
 */
#if DOOR_IMPL
bool_t
scha_rs_set_status_1_svc(rs_set_status_args *arg, set_status_result_t *result)
#else
bool_t
scha_rs_set_status_1_svc(rs_set_status_args *arg,
    set_status_result_t *result, struct svc_req *rqstp)
#endif
{

	bzero(result, sizeof (set_status_result_t));

	/*
	 * Check security.
	 * The last arg indicates that the user has to be root.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, TRUE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, TRUE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}

#if SOL_VERSION >= __s10
	result->ret_code = authenticate_client_cred(arg->rg_name);
	if (result->ret_code != SCHA_ERR_NOERR)
		return (TRUE);
#endif

	//
	// Note, for CZ rgmd, node n:z is equivalent to node n.
	//
	set_res_status(arg, result);
	if (result->ret_code != SCHA_ERR_NOERR)
		ducmm_print("RGM",
		    NOGET("scha_rs_set_status: failed\n"));
	else {
		ducmm_print("RGM", NOGET("scha_rs_set_status: "
		    " went ok; retunning\n"));
	}

	return (TRUE);
}

/*
 * scha_get_all_extprops_1_svc() -
 *
 *	this function calls get_all_extprops routine to retrieve the
 *	sequence id and all extension property names of a Resource
 *
 */
#if DOOR_IMPL
bool_t
scha_get_all_extprops_1_svc(rs_get_args *arg, get_result_t *result)
#else
bool_t
scha_get_all_extprops_1_svc(rs_get_args *arg, get_result_t *result,
    struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (get_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}

	//
	// Note, for CZ rgmd, node n:z is equivalent to node n.
	//
	get_all_extprops(arg, result);
	if (result->ret_code == SCHA_ERR_NOERR) {
		ducmm_print("RGM", NOGET("scha_rs_get_all_extprops: "
		    "RG = %s, R = %s, seqid = %s\n"),
		    arg->rg_name, arg->rs_name, result->seq_id);
		ducmm_print("RGM",
		    NOGET("scha_rs_get_all_extprops: OK \n"));
	} else {
		ducmm_print("RGM",
		    NOGET("scha_rs_get_all_extprops: failed\n"));
	}

	return (TRUE);
}

/*
 * scha_rt_open_1_svc() -
 *
 *	this function calls open_rt routine to retrieve the
 *	sequence id from the specified RT CCR table
 *
 */
#if DOOR_IMPL
bool_t
scha_rt_open_1_svc(rt_open_args *arg, open_result_t *result)
#else
bool_t
scha_rt_open_1_svc(rt_open_args *arg,
    open_result_t *result, struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (open_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}
	open_rt(arg, result);
	if (result->ret_code == SCHA_ERR_NOERR)
		ducmm_print("RGM",
		    NOGET("scha_rt_open: RT = %s, seqid = %s\n"),
		    arg->rt_name, result->seq_id);
	else {
		ducmm_print("RGM",
		    NOGET("scha_rt_open: failed\n"));
	}

	return (TRUE);
}

/*
 * scha_rt_get_1_svc() -
 *
 *	this function calls get_rt_get routine to retrieve the
 *	sequence id and the value of a specified Resource Type property
 *
 */
#if DOOR_IMPL
bool_t
scha_rt_get_1_svc(rt_get_args *arg, get_result_t *result)
#else
bool_t
scha_rt_get_1_svc(rt_get_args *arg, get_result_t *result,
    struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (get_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}
	get_rt(arg, result);

	if (result->ret_code == SCHA_ERR_NOERR)
		ducmm_print("RGM", NOGET("scha_rt_get: "
		    "RT = %s, TAG = %s, seqid = %s\n"),
		    arg->rt_name, arg->rt_prop_name, result->seq_id);
	else {
		ducmm_print("RGM",
		    NOGET("scha_rt_get: failed\n"));
	}

	return (TRUE);
}

/*
 * scha_rg_open_1_svc() -
 *
 *	this function calls open_rg routine to retrieve the
 *	RG sequence id from the specified RG CCR table
 *
 */
#if DOOR_IMPL
bool_t
scha_rg_open_1_svc(rg_open_args *arg, open_result_t *result)
#else
bool_t
scha_rg_open_1_svc(rg_open_args *arg, open_result_t *result,
    struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (open_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}
	open_rg(arg, result);

	if (result->ret_code == SCHA_ERR_NOERR)
		ducmm_print("RGM", NOGET("scha_rg_open: "
		    "RG = %s, seqid = %s\n"),
		    arg->rg_name, result->seq_id);
	else {
		ducmm_print("RGM",
		    NOGET("scha_rg_open: failed\n"));
	}

	return (TRUE);
}

/*
 * scha_rg_get_1_svc() -
 *
 *	this function calls get_rg_get routine to retrieve the RG
 *	sequence id and the value of a specified Resource Group property
 *
 */
#if DOOR_IMPL
bool_t
scha_rg_get_1_svc(rg_get_args *arg, get_result_t *result)
#else
bool_t
scha_rg_get_1_svc(rg_get_args *arg, get_result_t *result,
    struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (get_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}
	get_rg(arg, result);
	if (result->ret_code == SCHA_ERR_NOERR)
		ducmm_print("RGM", NOGET("scha_rg_get: "
		    "RG = %s, TAG = %s, seqid = %s\n"),
		    arg->rg_name, arg->rg_prop_name, result->seq_id);
	else {
		ducmm_print("RGM",
		    NOGET("scha_rg_get: failed\n"));
	}

	return (TRUE);
}

/*
 * scha_rg_get_state_1_svc() -
 *
 *	this function calls get_rg_state routine to retrieve the
 *	RG sequence id and the state of a Resource Group
 *
 */
#if DOOR_IMPL
bool_t
scha_rg_get_state_1_svc(rg_get_state_args *arg, get_result_t *result)
#else
bool_t
scha_rg_get_state_1_svc(rg_get_state_args *arg, get_result_t *result,
    struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (get_result_t));

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}

	//
	// Note, for CZ rgmd, node n:z is equivalent to node n.
	//
	get_rg_state(arg, result);
	if (result->ret_code == SCHA_ERR_NOERR)
		ducmm_print("RGM", NOGET("scha_rg_get_state: "
		    "RG = %s, seqid = %s\n"),
		    arg->rg_name, result->seq_id);
	else {
		ducmm_print("RGM",
		    NOGET("scha_rg_get_state: failed\n"));
	}

	return (TRUE);
}


/*
 * scha_control_1_svc() -
 *
 *	this function calls scha_control_action() to carry out a
 *	scha_control request.
 */
#if DOOR_IMPL
bool_t
scha_control_1_svc(scha_control_args *arg, scha_result_t *result)
#else
bool_t
scha_control_1_svc(scha_control_args *arg, scha_result_t *result,
    struct svc_req *rqstp)
#endif
{
	static int 	cur_nb_scha_ctl_calls = 0;
	int 		cur_max_concurrent_scha_control;
	bool_t		retval = TRUE;
#if DOOR_IMPL
	door_cred_t	dcred;
#else
	struct authunix_parms 	rpc_cred;
#endif
	int		concurrency;
	rglist_p_t	rglp;
	rlist_p_t 	rp;
	scha_err_t 	err;
	char 		*zonename = NULL;
	rgm::lni_t	lni;

	ucmm_print("RGM", NOGET("scha_control_1_svc: rg: %s zone: %s\n"),
	    arg->rg_name, arg->local_node);

	/*
	 * Check security.
	 * The last arg indicates that the user has to be root.
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&dcred, TRUE) != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, TRUE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}

	bzero(result, sizeof (scha_result_t));
	result->ret_code = SCHA_ERR_NOERR;

	//
	// Note, for CZ rgmd, node n:z is equivalent to node n.
	//
	if (arg->local_node != NULL && arg->local_node[0] != '\0')
		zonename = arg->local_node;

#if SOL_VERSION >= __s10
	result->ret_code = authenticate_client_cred(arg->rg_name);
	if (result->ret_code != SCHA_ERR_NOERR)
		return (TRUE);
#endif

	rgm_lock_state();

	sol::nodeid_t mynodeid = get_local_nodeid();

	LogicalNode *lnNode = lnManager->findLogicalNode(
	    mynodeid, zonename, NULL);

	if (lnNode == NULL) {
		result->ret_code = SCHA_ERR_NODE;
		rgm_unlock_state();
		return (TRUE);
	}

	lni = lnNode->getLni();
	CL_PANIC(lni != LNI_UNDEF);

	/*
	 * If action is IGNORE_FAILED_START, simply set
	 * the flag 'rx_ignore_failed_start' for the resource/zone
	 * and return. This will be checked and reset by
	 * this same rgmd in launch_method().
	 */
	if (arg->action == IGNORE_FAILED_START) {

		rglp = rgname_to_rg(arg->rg_name);
		if (rglp == NULL) {
			ucmm_print("RGM",
			    NOGET("\tscha_control_1_svc (IGNORE_FAILED_START) "
				"failed: No such RG\n"));
			result->ret_code = SCHA_ERR_RG;
			rgm_unlock_state();
			return (TRUE);
		}

		err = is_start_method(rglp, arg->r_name, zonename);
		if (err != SCHA_ERR_NOERR) {
			result->ret_code = err;
			rgm_unlock_state();
			return (TRUE);
		}

		/* Get Resource. */
		rp = rname_to_r(rglp, arg->r_name);
		if (rp == NULL) {
			ucmm_print("RGM",
			    NOGET("\tscha_control_1_svc (IGNORE_FAILED_START) "
				"failed: No such resource in memory\n"));
			result->ret_code = SCHA_ERR_RSRC;
			rgm_unlock_state();
			return (TRUE);
		}
		rp->rl_xstate[lni].rx_ignore_failed_start = B_TRUE;
		rgm_unlock_state();
		return (TRUE);
	} else if (arg->action == CHANGE_STATE_ONLINE ||
	    arg->action == CHANGE_STATE_OFFLINE) {

		/* Get RG. */
		rglp = rgname_to_rg(arg->rg_name);
		if (rglp == NULL) {
			ucmm_print("RGM",
			    NOGET("\tscha_control_1_svc (CHANGE_STATE_ONLINE "
			    "or CHANGE_STATE_OFFLINE) failed: No such "
			    "RG in memory\n"));
			result->ret_code = SCHA_ERR_RG;
			rgm_unlock_state();
			return (retval);
		}

		/* Get Resource. */
		rp = rname_to_r(rglp, arg->r_name);
		if (rp == NULL) {
			ucmm_print("RGM",
			    NOGET("\tscha_control_1_svc (CHANGE_STATE_ONLINE "
			    "or CHANGE_SATTE_OFFLINE failed: No such "
			    "resource in memory\n"));
			result->ret_code = SCHA_ERR_RSRC;
			rgm_unlock_state();
			return (retval);
		}

		if (rp->rl_ccrtype->rt_proxy == B_FALSE) {
			ucmm_print("RGM",
			    NOGET("\tscha_control_1_svc CHANGE_STATE_ONLINE "
			    "or CHANGE_SATTE_OFFLINE failed: Resource "
			    "type is not proxy\n"));
			result->ret_code = SCHA_ERR_RT;
			rgm_unlock_state();
			return (retval);
		}

		if (arg->action == CHANGE_STATE_ONLINE) {
			switch (rp->rl_xstate[lni].rx_state) {
				case rgm::R_ONLINE_STANDBY:
					set_rx_state(lni, rp,
					    rgm::R_JUST_STARTED);
					break;
				case rgm::R_PENDING_UPDATE:
					set_rx_state(lni, rp,
					    rgm::R_ON_PENDING_UPDATE);
					break;
				case rgm::R_ONLINE:
				case rgm::R_ON_PENDING_UPDATE:
					break;
				case rgm::R_UPDATING:
					rp->succ_state = rgm::R_ONLINE;
					rp->fail_state = rgm::R_ONLINE;
					break;
				case rgm::R_PRENET_STARTING:
					rp->succ_state = rgm::R_JUST_STARTED;
					break;
				default :
					result->ret_code = SCHA_ERR_STATE;
					break;
			} //lint !e788
		} else {
			switch (rp->rl_xstate[lni].rx_state) {
				case rgm::R_ONLINE:
					set_rx_state(lni, rp,
					    rgm::R_ONLINE_STANDBY);
					break;
				case rgm::R_ON_PENDING_UPDATE:
					set_rx_state(lni, rp,
					    rgm::R_PENDING_UPDATE);
					break;
				case rgm::R_ONLINE_STANDBY:
				case rgm::R_PENDING_UPDATE:
					break;
				case rgm::R_PRENET_STARTING:
				case rgm::R_UPDATING:
					rp->succ_state =
					    rgm::R_ONLINE_STANDBY;
					rp->fail_state =
					    rgm::R_ONLINE_STANDBY;
					break;
				default :
					result->ret_code = SCHA_ERR_STATE;
					break;
			} //lint !e788
		}
		rgm_unlock_state();
		return (retval);
	} else if (arg->action == RESOURCE_RUN_INIT_METHOD) {
		rgm_rt_t *rt = NULL;
		LogicalNodeset *nodes = NULL;
		boolean_t need_launch = B_FALSE;	// need to launch
		rgm::idl_string_seq_t	r_name_list, outlist;
		uint_t i, numrs = 0;
		scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

		// get the resource name list from the args.
		array_to_strseq_2(arg->r_names, r_name_list);
		// check for invalid or duplicate Rs.
		res = prune_operand_list(r_name_list, outlist, B_TRUE);
		result->ret_code = res.err_code;
		free(res.err_msg);

		//
		// Failure of prune_operand_list() means that invalid or
		// duplicate r names were pruned from the list.
		// We keep going to process the valid operands, but also
		// return an error code to the caller to aid in debugging.
		// However if the error was NOMEM then we return early.
		//
		if (res.err_code == SCHA_ERR_NOMEM) {
			rgm_unlock_state();
			return (TRUE);
		} else if (res.err_code != SCHA_ERR_NOERR) {
			ucmm_print("scha_control_run_r_init()",
			    NOGET("skipping invalid or dup r name(s) in "
			    "operand list"));
		}

		// get the number of resources.
		numrs = (uint_t)outlist.length();

		for (i = 0; i < numrs; i++) {
			char *r_name = NULL;
			r_name = strdup(outlist[i]);
			rp = rname_to_r(NULL, r_name);
			free(r_name);

			//
			// Get the corresponding rg for the r and the rg state
			// on local node.
			//
			rglp = rp->rl_rg;
			rgm::rgm_rg_state state =
			    rglp->rgl_xstate[lni].rgx_state;

			rgm::rgm_r_state r_state =
			    rp->rl_xstate[lni].rx_state;

			//
			// If the RG is already OFF_PENDING_BOOT, we skip
			// that resource without an error, since it will
			// run its Boot method in lieu of Init.
			//
			if (state == rgm::RG_OFF_PENDING_BOOT) {
				ucmm_print("scha_control_run_r_init()",
				    NOGET("rg %s already RG_OFF_PENDING_BOOT "
				    "on node %d; skipping rs %s"),
				    rglp->rgl_ccr->rg_name, lni,
				    rp->rl_ccrdata->r_name);
				continue;

			//
			// If the RG is OFF_PENDING_METHODS, it might have been
			// set by a previous resource in the operand list.
			// We continue to process this resource if its
			// state is OFFLINE, otherwise skip it; and if it
			// is not already running its INIT method, set
			// the return code to SCHA_ERR_STATE.
			//
			} else if (state == rgm::RG_OFF_PENDING_METHODS) {
				if (r_state == rgm::R_PENDING_INIT ||
				    r_state == rgm::R_INITING) {
					ucmm_print("scha_control_run_r_init()",
					    NOGET("rs %s already %s on node %d;"
					    " skipping"),
					    rp->rl_ccrdata->r_name,
					    pr_rstate(r_state), lni);
					continue;
				} else if (r_state != rgm::R_OFFLINE) {
					ucmm_print("scha_control_run_r_init()",
					    NOGET("rs %s in state %s on node "
					    "%d; skipping"),
					    rp->rl_ccrdata->r_name,
					    pr_rstate(r_state), lni);
					if (result->ret_code ==
					    SCHA_ERR_NOERR) {
						result->ret_code =
						    SCHA_ERR_STATE;
					}
					continue;
				}

			//
			// If the RG is any other state besides OFFLINE,
			// skip the resource and set return code to
			// SCHA_ERR_STATE.
			//
			} else if (state != rgm::RG_OFFLINE &&
			    state != rgm::RG_OFF_PENDING_METHODS) {
				ucmm_print("scha_control_run_r_init()",
				    NOGET("rg %s not offline on node %d; "
				    "skipping rs %s"),
				    rglp->rgl_ccr->rg_name, lni);
				if (result->ret_code == SCHA_ERR_NOERR) {
					result->ret_code = SCHA_ERR_STATE;
				}
				continue;
			}
			rt = rp->rl_ccrtype;

			//
			// Note: If an init method is not registered,
			// compute_method_nodes will return empty nodelist
			// Compute whether we need to run the init method.
			//
			nodes = compute_meth_nodes(rt, rglp->rgl_ccr,
			    METH_INIT, is_r_scalable(rp->rl_ccrdata));

			//
			// If I'm not in the list of nodes, continue on
			// to the next resource.
			//
			if (!nodes->containLni(lni)) {
				delete (nodes);
				continue;
			}
			delete (nodes);

			// need to run state machine
			need_launch = B_TRUE;
			ucmm_print("scha_control_run_r_init()", NOGET(
			    "setting to pending init for resource %s.\n"),
			    rp->rl_ccrdata->r_name);

			//
			// Above we confirmed that RG is OFFLINE or
			// OFF_PENDING_METHODS, and R is OFFLINE.
			// Set RG state to OFF_PENDING_METHODS and
			// set R state to PENDING_INIT.
			//
			set_rgx_state(lni, rglp, rgm::RG_OFF_PENDING_METHODS);
			set_rx_state(lni, rp, rgm::R_PENDING_INIT);
		}
		if (need_launch)
			run_state_machine(lni, "scha_control_run_r_init");
		rgm_unlock_state();
		return (TRUE);
	}

	/*
	 * scha_control can trigger the running of methods, and
	 * those methods in turn can make calls into the
	 * receptionist, all while the original scha_control
	 * is still running.  The RPC machinery permits to adjust the
	 * maximum number of concurrent calls into the receptionist.
	 * If we were to allow all of those to be incoming
	 * scha_control calls, then the calls into the receptionist
	 * that the methods need to make would block, causing a deadlock.
	 * To avoid  that deadlock, the number of concurrent
	 * scha_control is adjusted dynamically:
	 * - cur_max_concurrent_scha_control = max (MAX_SCHA_CONTROLS_INIT,
	 *					cur number of resources)
	 *
	 * We use a static variable to hold the current number
	 * of scha_calls. This variable is protected with the
	 * global mutex.
	 *
	 */

	cur_max_concurrent_scha_control = (Rgm_state->rgm_total_rs <
	    MAX_SCHA_CONTROLS_INIT) ? MAX_SCHA_CONTROLS_INIT :
	    Rgm_state->rgm_total_rs;

	if (cur_nb_scha_ctl_calls >= cur_max_concurrent_scha_control) {
		rgm_unlock_state();
		ucmm_print("RGM",
		    NOGET("\tscha_control_1_svc too many active failed"));
		result->ret_code = SCHA_ERR_RECONF;
		return (TRUE);
	}
	cur_nb_scha_ctl_calls++;
	rgm_unlock_state();

	/*
	 *
	 * We also have to deal with another problem: the RPC server-side
	 * library uses unbound threads and only allocates 4 lwps for its 16
	 * unbound threads.  If each scha_control is blocked in the ORB waiting
	 * for a long-running MONITOR_CHECK method, the result is that only
	 * four scha_control invocations can execute concurrently before
	 * exhausting all of the lwps and causing any further attempted
	 * receptionist call to block.	To prevent this, we call
	 * thr_setconcurrency(3T) to bump the number of lwps up by 1.
	 * However, we will not increase the number of lwps if
	 * thr_getconcurrency() is greater than or equal to
	 * RGM_MAX_THR_CONCURRENCY.  Since no more than MAX_SCHA_CONTROLS
	 * scha_control threads can be running at one time (and
	 * MAX_SCHA_CONTROLS is much less than RGM_MAX_THR_CONCURRENCY),
	 * we will likely not consume all of the lwps.
	 *
	 * After the lwps go idle for awhile, libthread automatically reduces
	 * the number of lwps.
	 *
	 * With the use of the new threads library in Solaris 9, there are
	 * no longer any unbound threads -- every thread is bound to an lwp.
	 * Therefore, thr_setconcurrency() effectively becomes a no-op.
	 * Some time in the future when the old libthread is no longer
	 * supported, we can get rid of the following
	 * thr_getconcurrency/thr_setconcurrency check, and
	 * just call scha_control_action() immediately.
	 */

	concurrency = os::thread::getconcurrency();
	if (concurrency < RGM_MAX_THR_CONCURRENCY &&
	    os::thread::setconcurrency(concurrency + 1) != 0) {
		// Failed to allocate additional lwp; bail out
		ucmm_print("RGM",
		    NOGET("\tscha_control_1_svc can't create lwp failed"));
		result->ret_code = SCHA_ERR_RECONF;
	} else {
		/*
		 * call the real scha_control function
		 */
		scha_control_action(arg->action, arg->rg_name, arg->r_name,
		    lni, result);
		if (result->ret_code == SCHA_ERR_NOERR)
			ucmm_print("RGM",
			    NOGET("\tscha_control_1_svc succeed\n"));
		else
			ucmm_print("RGM",
			    NOGET("\tscha_control_1_svc failed\n"));
	}

	rgm_lock_state();
	cur_nb_scha_ctl_calls--;
	rgm_unlock_state();

	return (TRUE);

}


#ifndef EUROPA_FARM
/*
 * scha_ssm_ip_1_svc() -
 *
 *	Do a SSM ip op.
 */
#if DOOR_IMPL
bool_t
scha_ssm_ip_1_svc(ssm_ip_args *arg, ssm_result_t *result)
#else
bool_t
scha_ssm_ip_1_svc(ssm_ip_args *arg, ssm_result_t *result,
    struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (ssm_result_t));

	/*
	 * Check security.
	 * The last arg indicates that the user has to be root.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, TRUE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, TRUE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}
	/*
	 * call the real SSM function
	 */
	rgm_ssm_ip_op(arg, result);
	if (result->ret_code != SCHA_ERR_NOERR) {
		ducmm_print("RGM",
		    NOGET("\tscha_ssm_1_svc failed\n"));
	}
	return (TRUE);
}

/*
 * scha_lb_1_svc() -
 *
 *	Do a load balancer op.
 */
#if DOOR_IMPL
bool_t
scha_lb_1_svc(scha_ssm_lb_args *arg, scha_ssm_lb_result_t *result)
#else
bool_t
scha_lb_1_svc(scha_ssm_lb_args *arg, scha_ssm_lb_result_t *result,
    struct svc_req *rqstp)
#endif
{
	/*
	 * Check security.
	 * The last arg indicates that the user has to be root.
	 */
#if DOOR_IMPL
	door_cred_t	dcred;
	if (security_svc_authenticate(&dcred, TRUE) != SEC_OK) {
#else
	struct authunix_parms rpc_cred;
	if (security_svc_authenticate(rqstp, &rpc_cred,
	    SEC_UNIX_STRONG, TRUE) != SEC_OK) {
#endif
		bzero(result, sizeof (scha_ssm_lb_result_t));
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}
	/*
	 * call the real load balancer function
	 * The lb_op function will zero the result argument
	 * before setting any results.
	 */
	lb_op(arg, result);
	if (result->ret_code != SCHA_ERR_NOERR) {
		ducmm_print("RGM",
		    NOGET("\tscha-lb_1_svc failed\n"));
	}

	return (TRUE);
}

/*
 * scha_translate_error() -
 *
 *	this function translates the scconf error codes into scha error codes
 *
 */
scha_err_t
scha_translate_error(scconf_errno_t scconf_err)
{
	switch (scconf_err) {
	case SCCONF_NOERR : return (SCHA_ERR_NOERR);
	case SCCONF_EPERM : return (SCHA_ERR_ACCESS);
	case SCCONF_ENOCLUSTER : return (SCHA_ERR_INTERNAL);
	case SCCONF_EUNEXPECTED: return (SCHA_ERR_INTERNAL);
	case SCCONF_ENOMEM : return (SCHA_ERR_NOMEM);
	case SCCONF_EINVAL : return (SCHA_ERR_INVAL);
	case SCCONF_ENOEXIST : return (SCHA_ERR_NODE);
	default:
		ucmm_print("RGM", NOGET(
			"\tunknown scconf error --%d\n"), scconf_err);
		return (SCHA_ERR_INTERNAL);
	}
}


/*
 * scha_cluster_open_1_svc() -
 *
 *	this function calls cluster_open to get
 *	cluster incarnation number
 *
 */
#if DOOR_IMPL
bool_t
scha_cluster_open_1_svc(void *, open_result_t *result)
#else
bool_t
scha_cluster_open_1_svc(void *, open_result_t *result, struct svc_req *rqstp)
#endif
{
	bzero(result, sizeof (open_result_t));

	// Europa calls this function from both local server node and farm
	// nodes. We don't check security.
	if (!rgmx_hook_enabled()) {

		/*
		 * Check security:
		 * SEC_UNIX_STRONG means that we're checking if
		 * the call came from the loopback transport,
		 * and that the userid matches the one requested - in this
		 * case root. The last arg value indicates that the caller
		 * doesn't have to be root.
		 */
#if DOOR_IMPL
		door_cred_t	dcred;
		if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
		struct authunix_parms rpc_cred;
		if (security_svc_authenticate(rqstp, &rpc_cred,
		    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
			result->ret_code = SCHA_ERR_ACCESS;
			ducmm_print("RGM", "SCHA_ERR_ACCESS\n");
			return (FALSE);
		}
	}
	cluster_open(result);
	if (result->ret_code == SCHA_ERR_NOERR) {
		ducmm_print("RGM", NOGET("SCHA_CLUSTER_OPEN: "
		    "seq_id = %s\n"), result->seq_id);
	} else {
		ducmm_print("RGM",
		    NOGET("scha_cluster_open: failed\n"));
	}
	return (TRUE);
}

/*
 * scha_cluster_get_1_svc() -
 *
 *	this function calls get_cluster routine to retrieve the
 *	rgmd generation number and the cluster information
 *
 */
#if DOOR_IMPL
bool_t
scha_cluster_get_1_svc(cluster_get_args *arg, get_result_t *result)
#else
bool_t
scha_cluster_get_1_svc(cluster_get_args *arg, get_result_t *result,
    struct svc_req *rqstp)
#endif
{
	bool_t retval = TRUE;
#if DOOR_IMPL
	door_cred_t	dcred;
#else
	struct authunix_parms rpc_cred;
#endif
	scconf_errno_t scconf_err;
	scconf_cfg_cluster_t *clconfigp = NULL;
	char *nodenamep = NULL;
	uint_t incr_num = 0;

	bzero(result, sizeof (get_result_t));
	result->ret_code = SCHA_ERR_NOERR;
	char *node_name = NULL;
	char *zoneptr = NULL;

	// Europa calls this function from both local server node and farm
	// nodes. We don't check security.
	if (!rgmx_hook_enabled()) {

		/*
		 * Check security.
		 */

#if DOOR_IMPL
		if (security_svc_authenticate(&dcred, FALSE) != SEC_OK) {
#else
		if (security_svc_authenticate(rqstp, &rpc_cred,
		    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
			result->ret_code = SCHA_ERR_ACCESS;
			return (FALSE);
		}
	}

	incr_num = scconf_get_incarnation(NULL);
	if (incr_num == (uint_t)0) {
		result->ret_code = SCHA_ERR_INTERNAL;
		goto cleanup; //lint !e801
	}
	ducmm_print("RGM",
		NOGET("SCHA_CLUSTER_GET(): seq_id = %d\n"),
		incr_num);

	if ((strcmp(arg->cluster_tag, SCHA_ALL_RESOURCEGROUPS)) == 0) {
		cluster_get_rgs(result);
	}

	if ((strcmp(arg->cluster_tag, SCHA_ALL_RESOURCETYPES)) == 0) {
		cluster_get_rts(result);
	}

	if ((strcmp(arg->cluster_tag, SCHA_NODESTATE_LOCAL)) == 0 ||
	    (strcmp(arg->cluster_tag, SCHA_NODESTATE_NODE)) == 0) {
		cluster_get_node_state(arg, result);
	}

	if (strcmp(arg->cluster_tag, SCHA_CLUSTERNAME) == 0 ||
	    strcmp(arg->cluster_tag, SCHA_ALL_NODEIDS) == 0 ||
	    strcmp(arg->cluster_tag, SCHA_NODENAME_NODEID) == 0 ||
	    strcmp(arg->cluster_tag, SCHA_PRIVATELINK_HOSTNAME_NODE) == 0) {
		if (access(RGM_RECEP_FILE, F_OK) != 0) {
			(void) create_cache(B_TRUE);
		}
	}

	if (strcmp(arg->cluster_tag, SCHA_CLUSTERNAME) == 0) {
		if ((scconf_err = scconf_get_virtualclusterconfig(ZONE,
		    &clconfigp))
		    != SCCONF_NOERR) {
			result->ret_code = scha_translate_error(scconf_err);
			ducmm_print("RGM", NOGET("scha_error=%d\n"),
			    result->ret_code);
			goto cleanup; //lint !e801
		}

		ducmm_print("RGM",
		    NOGET("SCHA_CLUSTERNAME, cluster name = %s \n"),
		    clconfigp->scconf_cluster_clustername);

		result->value_list =
		    (strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));
		(result->value_list->strarr_list_val)[0] =
		    strdup(clconfigp->scconf_cluster_clustername);

		goto cleanup; //lint !e801
	}
	if (strcmp(arg->cluster_tag, SCHA_ZONE_TYPE) == 0) {
		result->value_list =
		    (strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));
		if (strcmp(ZONE, GLOBAL_ZONENAME) == 0) {
			(result->value_list->strarr_list_val)[0] =
			    strdup("NATIVE");
		} else {
			(result->value_list->strarr_list_val)[0] =
			    strdup("CLUSTER");
		}

		goto cleanup; //lint !e801
	}

	if (strcmp(arg->cluster_tag, SCHA_NODENAME_LOCAL) == 0 ||
	    strcmp(arg->cluster_tag, SCHA_ZONE_LOCAL) == 0) {
		scconf_nodeid_t nodeid;

		// currently scconf is not virtualised, hence we will use the
		// rgm api. even if it is, we should probably prefer to use the
		// rgm internal api which will be faster
		if ((nodenamep = rgm_get_nodename(
		    get_local_nodeid())) == NULL) {
			result->ret_code = SCHA_ERR_NODE;
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
			    result->ret_code);
			goto cleanup; //lint !e801
		}
		if (strcmp(arg->cluster_tag, SCHA_ZONE_LOCAL) == 0) {
			char *new_nodenamep;
			if (arg->zonename != NULL && arg->zonename[0] != '\0' &&
			    strcmp(ZONE, GLOBAL_ZONENAME) == 0) {
				// append zonename:
				new_nodenamep = (char *)malloc(
				    strlen(nodenamep) +
				    strlen(arg->zonename) + 1 + 1);
				sprintf(new_nodenamep,
				    "%s:%s", nodenamep, arg->zonename);
				free(nodenamep);
				nodenamep = new_nodenamep;
			}
		}

		ducmm_print("RGM",
		    NOGET("SCHA_NODENAME_LOCAL, localnodename = %s\n"),
		    nodenamep);

		result->value_list =
		    (strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));
		(result->value_list->strarr_list_val)[0] = strdup(nodenamep);

		goto cleanup; //lint !e801
	}

	if (strcmp(arg->cluster_tag, SCHA_NODEID_LOCAL) == 0) {
		scconf_nodeid_t nodeid;
		char nodeid_str[10];

		nodeid = get_local_nodeid();

		ducmm_print("RGM",
		    NOGET("SCHA_NODEID_LOCAL, localnodeid = %d\n"),
		    nodeid);

		(void) sprintf(nodeid_str, "%d", nodeid);

		result->value_list =
		    (strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));
		(result->value_list->strarr_list_val)[0] = strdup(nodeid_str);

		goto cleanup; //lint !e801
	}

	if (strcmp(arg->cluster_tag, SCHA_VERIFY_IP) == 0) {
#if SOL_VERSION >= __s10
		char ret_str[2]; // we know its a 0 or 1
		struct hostent *userhp;
		struct hostent *zccfghp;
		zc_dochandle_t handle;
		char addrbuf[INET6_ADDRSTRLEN];
		char zccfg_addrbuf[INET6_ADDRSTRLEN];
		struct in6_addr in6;
		struct zc_nwiftab nwiftab;
		boolean_t found = B_FALSE;
		int ret;
		//
		// We get IP address or hostname in arg->node_name
		// Here we are overloading the use of the field.
		// If arg->node_name it has an IP address, then it can
		// be an IPv4 or IPv6 address.
		// The zone configuration information about the IP
		// address can also be a hostname, an IPv4 or IPv6
		// address. So we have to walk through all the IP's
		// configured for the zone cluster, convert them
		// to a standard IPv6 format and compare them
		// with the given IP (arg->node_name). The given
		// IP also is converted to a standard IPv6 format
		// before the comparison.
		//
		userhp = getipnodebyname(arg->node_name, AF_INET6,
		    AI_ALL | AI_ADDRCONFIG | AI_V4MAPPED, &ret);

		if (userhp == NULL) {
			ucmm_print("scha_cluster_get",
			    NOGET("VERIFY_IP/ADAPTER, getipnodebyname() error "
			    "arg = %s, ret = %d\n"), arg->node_name, ret);
			result->ret_code = SCHA_ERR_INTERNAL;
			goto cleanup;
		}
		if (*(userhp->h_addr_list) == 0) {
			ucmm_print("scha_cluster_get",
			    NOGET("VERIFY_IP/ADAPTER, getipnodebyname() error,"
			    "Got null address\n"));
			result->ret_code = SCHA_ERR_INTERNAL;
			freehostent(userhp);
			goto cleanup;
		}
		bcopy(*(userhp->h_addr_list), (caddr_t)&in6, userhp->h_length);
		(void) inet_ntop(AF_INET6, (void *)&in6, addrbuf,
		    sizeof (addrbuf));
		ucmm_print("scha_cluster_get",
		    NOGET("VERIFY_IP/ADAPTER, getipnodebyname() "
		    "arg = %s, ip = %s\n"), arg->node_name, addrbuf);

		freehostent(userhp);
		if ((handle = zccfg_init_handle()) == NULL) {
			result->ret_code = SCHA_ERR_INTERNAL;
			goto cleanup;
		}
		if (zccfg_get_handle(ZONE, handle) != ZC_OK) {
			zccfg_fini_handle(handle);
			result->ret_code = SCHA_ERR_INTERNAL;
			goto cleanup;
		}
		if ((ret = zccfg_setnwifent(handle)) != ZC_OK) {
			ucmm_print("scha_cluster_get",
			    NOGET("VERIFY_IP/ADAPTER, arg = %s, ret = %d\n"),
			    arg->node_name, ret);
			result->ret_code = SCHA_ERR_INTERNAL;
			goto cleanup;
		}
		//
		// Walk through each entry in the zone config, convert
		// the IP address entry to a IPv6 format and compare it
		// till we either find a match or there are no more entries.
		//
		while (zccfg_getnwifent(handle, &nwiftab) == ZC_OK) {

			zccfghp = getipnodebyname(nwiftab.zc_nwif_address,
			    AF_INET6, AI_ALL | AI_ADDRCONFIG | AI_V4MAPPED,
			    &ret);

			if (zccfghp == NULL) {
				ucmm_print("scha_cluster_get",
				    NOGET("VERIFY_IP/ADAPTER, "
				    "getipnodebyname() ERROR arg = %s, "
				    "ret = %d\n"), nwiftab.zc_nwif_address,
				    ret);
				continue;
			}
			if (*(zccfghp->h_addr_list) == 0) {
				ucmm_print("scha_cluster_get",
				    NOGET("VERIFY_IP/ADAPTER, "
				    "getipnodebyname() error, "
				    "Got null address from a zone "
				    "configuration entry.\n"));
				freehostent(zccfghp);
				continue;
			}
			bcopy(*(zccfghp->h_addr_list), (caddr_t)&in6,
			    zccfghp->h_length);

			(void) inet_ntop(AF_INET6, (void *)&in6, zccfg_addrbuf,
			    sizeof (zccfg_addrbuf));

			ucmm_print("scha_cluster_get",
			    NOGET("VERIFY_IP/ADAPTER, getipnodebyname() "
			    "ARG = %s, IP = %s\n"), nwiftab.zc_nwif_address,
			    zccfg_addrbuf);

			freehostent(zccfghp);

			if (strcmp(addrbuf, zccfg_addrbuf) == 0) {
				found = B_TRUE;
				break;
			}
		}
		zccfg_endnwifent(handle);
		zccfg_fini_handle(handle);

		strcpy(ret_str, found == B_TRUE ? "0" : "1");

		ucmm_print("scha_cluster_get",
		    NOGET("VERIFY_IP/ADAPTER, arg = %s, ret = %d retstr=%s\n"),
		    arg->node_name, ret, ret_str);

		result->value_list =
		    (strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));
		(result->value_list->strarr_list_val)[0] = strdup(ret_str);
		ucmm_print("scha_cluster_get",
		    NOGET("SCHA_VERIFY_IP, ip = %s, ret = %s\n"),
		    arg->node_name, ret_str);
		goto cleanup; //lint !e801
#else
		result->ret_code = SCHA_ERR_INVAL;
		goto cleanup;
#endif
	}

	if (strcmp(arg->cluster_tag, SCHA_VERIFY_ADAPTER) == 0) {
#if SOL_VERSION >= __s10
		// always success
		result->value_list =
		    (strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));
		(result->value_list->strarr_list_val)[0] = strdup("0");
		goto cleanup; //lint !e801
#else
		result->ret_code = SCHA_ERR_INVAL;
		goto cleanup;
#endif
	}

	if (strcmp(arg->cluster_tag, SCHA_NODEID_NODENAME) == 0) {
		scconf_nodeid_t nodeid;
		char nodeid_str[10];

		if (arg->node_name == NULL || arg->node_name[0] == '\0') {
			result->ret_code = SCHA_ERR_NODE;
			goto cleanup; //lint !e801
		}
		node_name = strdup(arg->node_name); // should abort if ENOMEM
		if ((zoneptr = strchr(node_name, LN_DELIMITER)) != NULL) {
			// ignore zonename component for this call
			*zoneptr = '\0';
		}

		//
		// For a given nodename, scconf_get_nodeid() returns the
		// nodeid.  If this function finds that "nodename"
		// is all numeric, it will assume that the caller has provided
		// a nodeid in "nodename"; it checks for the existence of the
		// nodeid, then returns the integer value in "nodeidp".	  This
		// is useful to most of our commands which will accept either
		// a "nodeid" or "nodename" when referencing a node.  However,
		// this is not useful behavior for the scha API, which demands
		// a valid nodename for the SCHA_NODEID_NODENAME optag.	 To
		// rule out this case, we check to make sure that the first
		// character of "nodename" is not a digit.
		//
		// A valid nodename cannot start with a digit.	hosts(4), which
		// references RFC 921, states that a hostname must start with
		// an alpha character.
		//
		if (isdigit(*node_name)) { //lint !e746 !e1055
			// node_name starts with a digit
			result->ret_code = SCHA_ERR_NODE;
			goto cleanup; //lint !e801
		}
		// currently scconf is not virtualised, hence we will use the
		// rgm api. even if it is, we should probably prefer to use the
		// rgm internal api which will be faster
#if 0

		if ((scconf_err = scconf_get_nodeid(node_name, &nodeid))
		    != SCCONF_NOERR) {
			result->ret_code = scha_translate_error(scconf_err);
			// map "invalid argument" error to "invalid node"
			if (result->ret_code == SCHA_ERR_INVAL) {
				result->ret_code = SCHA_ERR_NODE;
			}
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
			    result->ret_code);
			goto cleanup; //lint !e801
#endif
		if (rgm_get_nodeid(node_name, &nodeid) != 0) {
			result->ret_code = SCHA_ERR_NODE;
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
			    result->ret_code);
			goto cleanup; //lint !e801
		}

		(void) sprintf(nodeid_str, "%d", nodeid);

		result->value_list =
		    (strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));

		ducmm_print("RGM",
		    NOGET("SCHA_NODEID_NODENAME, name = %s, id = %s\n"),
		    arg->node_name, nodeid_str);
		(result->value_list->strarr_list_val)[0] = strdup(nodeid_str);
		goto cleanup; //lint !e801
	}

	if (strcmp(arg->cluster_tag, SCHA_NODENAME_NODEID) == 0) {
		if ((nodenamep = rgm_get_nodename_cache(arg->node_id)) ==
		    NULL) {
			result->ret_code = SCHA_ERR_NODE;
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
			    result->ret_code);
			goto cleanup; //lint !e801
		}

		ducmm_print("RGM",
		    NOGET("SCHA_NODENAME_NODEID, id = %d, name = %s\n"),
		    arg->node_id, nodenamep);

		result->value_list =
		    (strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));
		(result->value_list->strarr_list_val)[0] = strdup(nodenamep);

		goto cleanup; //lint !e801
	}

	if (strcmp(arg->cluster_tag, SCHA_ALL_ZONES) == 0 ||
	    strcmp(arg->cluster_tag, SCHA_ALL_ZONES_NODEID) == 0 ||
	    strcmp(arg->cluster_tag, SCHA_ALL_NONGLOBAL_ZONES_NODEID) == 0 ||
	    strcmp(arg->cluster_tag, SCHA_ALL_NONGLOBAL_ZONES) == 0) {
		sol::nodeid_t nodeid = 0;
		bool exclude_global = false;
		strarr_list	*ret_list = NULL;

		if ((ret_list = (strarr_list *)
		    calloc_nocheck(1, sizeof (strarr_list))) == NULL) {
			ucmm_print("scha_cluster_get_1_svc",
			    NOGET("ALL_ZONES. couldn't calloc\n"));
			result->ret_code = SCHA_ERR_NOMEM;
			goto cleanup; //lint !e801
		}
		if (strcmp(arg->cluster_tag, SCHA_ALL_ZONES_NODEID) == 0 ||
		    strcmp(arg->cluster_tag, SCHA_ALL_NONGLOBAL_ZONES_NODEID) ==
		    0) {
			nodeid = arg->node_id;
			//
			// verify that nodeid is configured in cluster. static
			// membership check done only after verifying range.
			//
			if (nodeid > LogicalNodeset::maxNode() || nodeid == 0 ||
			    !Rgm_state->static_membership.containLni(
			    nodeid)) {
				result->ret_code = SCHA_ERR_NODE;
				goto cleanup; //lint !e801
			}
		}
		if (strcmp(arg->cluster_tag, SCHA_ALL_NONGLOBAL_ZONES) == 0 ||
		    strcmp(arg->cluster_tag, SCHA_ALL_NONGLOBAL_ZONES_NODEID) ==
		    0) {
			exclude_global = true;
		}

		if ((result->ret_code = get_logicalnodesfrompres(nodeid,
		    exclude_global, ret_list)) != SCHA_ERR_NOERR) {
			free(ret_list);
			goto cleanup; //lint !e801
		}
		result->value_list = ret_list;

		goto cleanup; //lint !e801
	}
	if (strcmp(arg->cluster_tag, SCHA_ALL_NODENAMES) == 0) {
		uint_t no_nodes = 0;
		int i = 0;
		scconf_cfg_node_t *curr_p = NULL;
		strarr_list	*ret_list = NULL;

		if ((scconf_err = scconf_get_virtualclusterconfig(ZONE,
		    &clconfigp))
		    != SCCONF_NOERR) {
			result->ret_code = scha_translate_error(scconf_err);
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
			    result->ret_code);
			goto cleanup; //lint !e801
		}

		for (curr_p = clconfigp->scconf_cluster_nodelist;
		    curr_p != NULL;
		    curr_p = curr_p->scconf_node_next) {
			if (curr_p->scconf_node_nodename)
				no_nodes++;
		}

		if (no_nodes == 0) {
			ducmm_print("RGM",
			    NOGET("SCHA_ALL_NODENAMES: no data\n"));
			result->value_list = NULL;
			goto cleanup; //lint !e801
		} else
			ducmm_print("RGM",
			    NOGET("SCHA_ALL_NODENAMES, no_nodes=%d\n"),
			    no_nodes);

		if ((ret_list = (strarr_list *)
		    calloc_nocheck(1, sizeof (strarr_list))) == NULL) {
			ucmm_print("RGM",
			    NOGET("SCHA_ALL_NODENAMES, couldn't calloc\n"));
			result->ret_code = SCHA_ERR_NOMEM;
			goto cleanup; //lint !e801
		}

		if ((ret_list->strarr_list_val = (char **)calloc_nocheck(
			(size_t)no_nodes, sizeof (char *))) == NULL) {
			ucmm_print("RGM",
			    NOGET("SCHA_ALL_NODENAMES, couldn't calloc\n"));
			result->ret_code = SCHA_ERR_NOMEM;
			free(ret_list);
			goto cleanup; //lint !e801
		}

		for (i = 0, curr_p = clconfigp->scconf_cluster_nodelist;
		    curr_p != NULL;
		    curr_p = curr_p->scconf_node_next, i++) {
			if (curr_p->scconf_node_nodename) {
				ducmm_print("RGM", NOGET(
					"SCHA_ALL_NODENAMES, node %d is %s\n"),
				    i, curr_p->scconf_node_nodename);
				ret_list->strarr_list_val[i] =
				    strdup(curr_p->scconf_node_nodename);
			}
		}
		ret_list->strarr_list_len = no_nodes;
		result->value_list = ret_list;
		goto cleanup; //lint !e801
	}

	if (strcmp(arg->cluster_tag, SCHA_ALL_NODEIDS) == 0) {
		uint_t no_nodes = 0;
		int i = 0;
		char nodeid_str[10];
		scconf_cfg_node_t *curr_p = NULL;

		if ((scconf_err = scconf_get_virtualclusterconfig(ZONE,
		    &clconfigp))
		    != SCCONF_NOERR) {
			result->ret_code = scha_translate_error(scconf_err);
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
			    result->ret_code);
			goto cleanup; //lint !e801
		}

		for (curr_p = clconfigp->scconf_cluster_nodelist;
		    curr_p != NULL;
		    curr_p = curr_p->scconf_node_next, no_nodes++) {
			// empty loop, used only to calculate no_nodes
		}

		ducmm_print("RGM",
		    NOGET("SCHA_ALL_NODEIDS, no_nodes = %d\n"),
		    no_nodes);

		result->value_list = (strarr_list *)
		    malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = no_nodes;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));

		for (i = 0, curr_p = clconfigp->scconf_cluster_nodelist;
		    curr_p != NULL;
		    curr_p = curr_p->scconf_node_next, i++) {
			ducmm_print("RGM", NOGET(
				"SCHA_ALL_NODEIDS, node %d is %d\n"),
			    i, curr_p->scconf_node_nodeid);
			(void) sprintf(nodeid_str, "%d",
			    curr_p->scconf_node_nodeid);
			(result->value_list->strarr_list_val)[i] =
			    strdup(nodeid_str);
		}

		goto cleanup; //lint !e801
	}

	if (strcmp(arg->cluster_tag, SCHA_ALL_PRIVATELINK_HOSTNAMES) == 0) {
		get_priv_hostnames(NULL, NULL, result);
		goto cleanup; //lint !e801
	}


	if (strcmp(arg->cluster_tag, SCHA_PRIVATELINK_HOSTNAME_LOCAL) == 0) {
		scconf_nodeid_t nodeid;
		char *zonename = NULL;
		if (arg->zonename != NULL && arg->zonename[0] != '\0') {
#if SOL_VERSION >= __s10
			if (strcmp(arg->zonename, GLOBAL_ZONENAME) != 0)
				zonename = arg->zonename;
#else
			result->ret_code = SCHA_ERR_NODE;
			goto cleanup; //lint !e801
#endif
		}

		// currently scconf is not virtualised, hence we will use the
		// rgm api. even if it is, we should probably prefer to use the
		// rgm internal api which will be faster
#if 0
		if ((scconf_err = scconf_get_nodeid(NULL, &nodeid))
		    != SCCONF_NOERR) {
			result->ret_code = scha_translate_error(scconf_err);
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
			    result->ret_code);
			goto cleanup; //lint !e801
		}
		if ((scconf_err =
		    scconf_get_nodename(nodeid, &node_name))
		    != SCCONF_NOERR) {
			result->ret_code = scha_translate_error(scconf_err);
			if (result->ret_code == SCHA_ERR_INVAL)
				result->ret_code = SCHA_ERR_NODE;
			goto cleanup; //lint !e801
		}
#endif
		if ((node_name = rgm_get_nodename(
		    get_local_nodeid())) == NULL) {
			result->ret_code = SCHA_ERR_NODE;
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
			    result->ret_code);
			goto cleanup; //lint !e801
		}
		get_priv_hostnames(node_name, zonename, result);
		goto cleanup; //lint !e801

	}

	if (strcmp(arg->cluster_tag, SCHA_PRIVATELINK_HOSTNAME_NODE) == 0) {
		if (arg->node_name == NULL || arg->node_name[0] == '\0') {
			result->ret_code = SCHA_ERR_NODE;
			goto cleanup; //lint !e801
		}
		node_name = strdup(arg->node_name);
		zoneptr = strchr(node_name, LN_DELIMITER);

		if (zoneptr != NULL) {
#if SOL_VERSION >= __s10
			*zoneptr++ = '\0';
			if (strcmp(zoneptr, GLOBAL_ZONENAME) == 0)
				zoneptr = NULL;
#else
			result->ret_code = SCHA_ERR_NODE;
			goto cleanup; //lint !e801
#endif
		}
		get_priv_hostnames(node_name, zoneptr, result);
		goto cleanup; //lint !e801
	}

cleanup:
	if (clconfigp)
		scconf_free_clusterconfig(clconfigp);
	if (nodenamep)
		free(nodenamep);
	if (result->ret_code != SCHA_ERR_NOERR)
		ucmm_print("RGM",
		    NOGET("scha_cluster_get_1_svc failed\n"));
	else {
		result->seq_id = rgm_itoa((int)incr_num);
		if (result->seq_id == NULL) {
			result->ret_code = SCHA_ERR_NOMEM;
			ucmm_print("RGM",
			    NOGET("scha_cluster_get_1_svc failed\n"));
		}
	}

	if (node_name)
		free(node_name);

	(void) rgmx_hook_call(rgmx_hook_scha_farm_get, arg, result,
		&retval);

	return (retval);
}

/*
 * get the privatelink hostname for the node, zone provided. If zone is NULL, it
 * refers to the global zone (physical node). non null zonename should only be
 * passed in post s10 version.
 * if node is null, then zone should always be null, and we return the private
 * hostnames for all nodes/zones that have one.
 */
void
get_priv_hostnames(char *nodename, char *zonename, get_result_t *result)
{
	strarr_list nglist = {0};
	scconf_errno_t scconf_err;
	sol::nodeid_t nodeid = 0;
	scconf_cfg_node_t *curr_p = NULL;
	cluster_get_args arg = {0};
	uint_t no_nodes = 0;
	scconf_cfg_cluster_t *clconfigp = NULL;
	uint_t i = 0, j;
	char **array = NULL;

	if (zonename != NULL && strcmp(zonename, ZONE) == 0)
		zonename = NULL;

	if (nodename == NULL) {
		if (zonename != NULL) {
			result->ret_code = SCHA_ERR_INVAL;
			return;
		}
	} else {
		if (rgm_get_nodeid(nodename, &nodeid) != 0) {
			result->ret_code = SCHA_ERR_NODE;
			return;
		}

		if (zonename != NULL) {
			// native zone
#if SOL_VERSION >= __s10
			// node and zone are provided
			i = 0;
			no_nodes = 1;
			array = (char **)calloc(no_nodes, sizeof (char *));
			arg.cluster_tag = SCHA_NODESTATE_NODE;
			arg.node_name = lnManager->computeLn(nodeid, zonename);
			cluster_get_node_state(&arg, result);
			if (result->ret_code != SCHA_ERR_NOERR)
				goto cleanup; //lint !e801
			array[i++] = scprivip_get_hostname(zonename, nodeid);
#else
			ASSERT(0);
			result->ret_code = SCHA_ERR_INTERNAL;

#endif
			goto cleanup; //lint !e801
		}
	}

	//
	// atleast zone is NULL. either get it for physical node or for all
	// nodes
	//

	if ((scconf_err = scconf_get_virtualclusterconfig(ZONE, &clconfigp))
	    != SCCONF_NOERR) {
		result->ret_code = scha_translate_error(scconf_err);
		goto cleanup; //lint !e801
	}

	if (nodename == NULL) {
		for (no_nodes = 0, curr_p = clconfigp->scconf_cluster_nodelist;
		    curr_p != NULL; curr_p = curr_p->scconf_node_next) {
			if (curr_p->scconf_node_privatehostname)
				no_nodes++;
		}
#if SOL_VERSION >= __s10
		if ((result->ret_code = get_logicalnodesfrompres(0, true,
		    &nglist)) != SCHA_ERR_NOERR) {
			goto cleanup; //lint !e801
		} else {
			no_nodes += nglist.strarr_list_len;
		}
#endif
	} else {
		no_nodes = 1;
	}
	if (no_nodes == 0) {
		result->value_list = NULL;
		goto cleanup; //lint !e801
	}

	array = (char **)calloc(no_nodes, sizeof (char *));

	for (i = 0, curr_p = clconfigp->scconf_cluster_nodelist;
		    curr_p != NULL; curr_p = curr_p->scconf_node_next) {
		if (nodename != NULL) {
			// we provided a nodename.
			if (curr_p->scconf_node_nodeid == nodeid) {
				// copy hostname and break out of loop
				if (curr_p->scconf_node_privatehostname) {
					array[i++] = strdup(curr_p->
					    scconf_node_privatehostname);
				} else {
					array[i++] = NULL;
				}
				break;
			}
			continue;
		}
		// no nodename provided. assimilate all.
		if (curr_p->scconf_node_privatehostname == NULL)
			continue;

		array[i++] = strdup(curr_p->scconf_node_privatehostname);
	}
	if (nodename != NULL) {
		if (i != 1) {
		//
		// serious error; if nodename is not null, we should match
		// exactly one entry
		//
			result->ret_code = SCHA_ERR_INTERNAL;
		}
		goto cleanup; //lint !e801
	}

#if SOL_VERSION >= __s10
	for (j = 0; j < nglist.strarr_list_len; j++) {
		char *hn;
		char *zoneptr = strchr(nglist.strarr_list_val[j], LN_DELIMITER);
		*zoneptr++ = '\0';
		if (rgm_get_nodeid(nglist.strarr_list_val[j], &nodeid) == -1) {
			result->ret_code = SCHA_ERR_NODE;
			goto cleanup; //lint !e801
		}
		if ((hn = scprivip_get_hostname(zoneptr, nodeid)) != NULL)
			array[i++] = hn;
	}
#endif

cleanup:
	if (clconfigp)
		scconf_free_clusterconfig(clconfigp);

	if (result->ret_code != SCHA_ERR_NOERR) {
		if (array != NULL) {
			for (j = 0; j < no_nodes; j++) {
				//
				// calloc zeros the array, and it's
				// ok to call free on a NULL ptr
				//
				free(array[j]);
			}
		}
		return;
	}

	if (i == 0) {
		result->value_list = NULL;
		return;
	}
	result->value_list = (strarr_list *) malloc(sizeof (strarr_list));
	result->value_list->strarr_list_len = i;
	result->value_list->strarr_list_val = (strarr *)array;
}
#endif // !EUROPA_FARM


/*
 * scha_program_1_freeresult() -
 *
 *	the strings malloced by strdup on the server are automatically freed
 *	by xdr_free on the server;
 *	we still need to free them after they're read on the client
 */
int
scha_program_1_freeresult(SVCXPRT *, xdrproc_t xdr_result,
    caddr_t res)
{

	/*
	 * Insert additional freeing code here, if needed
	 */
	xdr_free(xdr_result, res);

	return (1);
}
