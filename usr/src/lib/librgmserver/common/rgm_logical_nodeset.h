//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_RGM_LOGICAL_NODESET_H
#define	_RGM_LOGICAL_NODESET_H

#pragma ident	"@(#)rgm_logical_nodeset.h	1.4	08/08/16 SMI"

#include <sys/types.h>

#include <cmm/ucmm_api.h>
#include <h/rgm.h>

// BITSPERWORD is currently 32; this size is hard-coded in various places
#define	BITSPERWORD	32
//
// Use integer division, which truncates, to compute the number of 32-bit words
// necessary to represent LNI (x).
//
#define	NBWORDS(x)	(((uint_t)(x) + BITSPERWORD - 1) / BITSPERWORD)

//
// This class provides similar functionality to nodeset_t,
// but extends that to a larger, variable number of entries (to
// represent LNIs) instead of MAXNODES = 64.
//
// Newly created LogicalNodeset objects are sized to the value theMaxLni
// which is stored as a static member of this class.
// theMaxLni may be increased dynamically
// as additional LNs are created.  It is never decreased while the rgmd
// is up and running, so any existing LogicalNodeset must have its
// max lni <= theMaxLni.
//
class LogicalNodeset {
protected:
	static rgm::lni_t theMaxNode;
	static rgm::lni_t theMaxLni;

	uint_t		nbWords;
	uint32_t	*words;

	void setBit(const uint_t indx);

	void clrBit(const uint_t indx);

	int tstBit(const uint_t indx) const;

	int nextClr(const uint_t) const;

	int nextSet(const uint_t) const;

	int firstSet(const uint_t = 0) const;

	int firstClr(const uint_t = 0) const;

public:
	LogicalNodeset();
	LogicalNodeset(const LogicalNodeset &nset);
	LogicalNodeset(const nodeset_t ns);

	//
	// the following ctor is used only by ArrayFreeList when it is
	// growing the number of LNs
	//
	LogicalNodeset(uint_t newMaxLni);

	virtual ~LogicalNodeset();

	void initialize();

	LogicalNodeset& operator= (const LogicalNodeset& nset);
	LogicalNodeset& operator= (nodeset_t nset);

	const LogicalNodeset operator~() const;
	uint32_t operator[] (int index);

	nodeset_t getNodeset() const;

	void addLni(rgm::lni_t n);

	void delLni(rgm::lni_t n);

	bool containLni(rgm::lni_t n) const;

	rgm::lni_t nextLniSet(rgm::lni_t n) const;
	sol::nodeid_t nextPhysNodeSet(sol::nodeid_t n) const;
	sol::nodeid_t nextServerNodeSet(sol::nodeid_t n) const;
	sol::nodeid_t nextFarmNodeSet(sol::nodeid_t n) const;

	bool isEmpty() const;

	void reset();

	bool isEqual(const LogicalNodeset &ns) const;

	// Is this LogicalNodeset a subset of ns?
	bool isSubset(const LogicalNodeset &ns) const;

	// Is this LogicalNodeset a superset of ns?
	bool isSuperset(const LogicalNodeset &ns) const;

	//
	// set union, via overloaded operator| and |=.
	friend const LogicalNodeset operator|(const LogicalNodeset &ns1,
	    const LogicalNodeset &ns2);
	/* CSTYLED */
	LogicalNodeset &operator|=(const LogicalNodeset &ns1);

	// set intersection, via overloaded operator& and &=
	friend const LogicalNodeset operator&(const LogicalNodeset &ns1,
	    const LogicalNodeset &ns2);
	/* CSTYLED */
	LogicalNodeset &operator&=(const LogicalNodeset &ns1);


	// set subtraction, via overloaded operator- and -=
	friend const LogicalNodeset operator-(const LogicalNodeset &ns1,
	    const LogicalNodeset &ns2);
	/* CSTYLED */
	LogicalNodeset &operator-=(const LogicalNodeset &ns1);

	int display(char *&buffer);
	uint_t count() const;

	static sol::nodeid_t firstFarmNodeSet() { return (MAXNODES + 1); }

	// Get current max LNI
	static rgm::lni_t maxLni() { return (theMaxLni); }

	// Get max physical Node
	static rgm::lni_t maxNode() { return (theMaxNode); }

	// Set current max LNI
	static void setMaxLni(rgm::lni_t maxLni);

	// Set max physical Node
	static void setMaxNode(sol::nodeid_t maxNode);
};

// Default CTOR
inline
LogicalNodeset::LogicalNodeset()
{
	initialize();
}

//
// Ctor of default LogicalNodeset which grows theMaxLni to the specified size.
// This is used only by ArrayFreeList when it is growing the number of LNs.
// The argument newMaxLni indicates the new maximum number of LNIs, which
// must be greater than the current theMaxLni and should be a multiple of 32.
//
inline
LogicalNodeset::LogicalNodeset(uint_t newMaxLni)
{
	CL_PANIC(newMaxLni > theMaxLni);
	theMaxLni = newMaxLni;
	initialize();
}

// CTOR from nodeset_t
// . Intialize the first 64 bits with nodeset_t
// . Remaining bits are set to 0
//
inline
LogicalNodeset::LogicalNodeset(const nodeset_t ns)
{
	initialize();

	words[0] = (uint32_t)(ns & 0xffffffff);
	words[1] = (uint32_t)(ns >> 32);
}

// CTOR from LogicalNodeset_t
inline
LogicalNodeset::LogicalNodeset(const LogicalNodeset &nset)
{
	initialize();
	CL_PANIC(nbWords >= nset.nbWords);
	(void) memcpy(words, nset.words, sizeof (uint32_t) * nset.nbWords);
}

//
// Destructor for LogicalNodeset
//
inline
LogicalNodeset::~LogicalNodeset()
{
	delete [] words;
}

inline void
LogicalNodeset::initialize()
{
	nbWords = NBWORDS(theMaxLni);
	words = new uint32_t[nbWords];
	reset();
}


//
// Inline functions which convert a logical nodeid (LNI) to a
// index; for instance LNI '3' corresponds
// to offset '2'. nodeset_t are implemented this
// way, so keep it homogeneous.
//
// Caller must guarantee that 1 <= n <= maxLni().
//

inline void
LogicalNodeset::delLni(rgm::lni_t n)
{
	CL_PANIC(n > 0 && n <= theMaxLni);
	if (NBWORDS(n) <= nbWords) {
		clrBit(n - 1);
	}
}

inline bool
LogicalNodeset::containLni(rgm::lni_t n) const
{
	CL_PANIC(n > 0 && n <= theMaxLni);
	return (NBWORDS(n) <= nbWords && (tstBit(n - 1) == 1));
}

//
// - Return the next LNI set in the LogicalNodeset starting at LNI 'n'.
// - Returns 0 if there are no more entries
//
inline rgm::lni_t
LogicalNodeset::nextLniSet(rgm::lni_t n) const
{
	int index;

	if (NBWORDS(n) > nbWords) {
		// n is already off the end of the array
		return (0);
	}
	index = nextSet(n - 1);
	//
	// . Convert index into LNI by adding 1
	// . If no more entries, nextSet returns -1, which is converted to '0'
	//
	return ((rgm::lni_t)(index + 1));
}


//
// Truncate a LogicalNodeset into a nodeset_t
// If the LogicalNodeset is greater than the nodeset,
// only the first 64 bits are taken into account.
//
inline nodeset_t
LogicalNodeset::getNodeset() const
{
	nodeset_t ns = EMPTY_NODESET;
	nodeset_t ns1 = EMPTY_NODESET;
	nodeset_t ns2 = EMPTY_NODESET;
	ns1 = (nodeset_t)words[0];
	ns2 = (nodeset_t)words[1];
	ns2 = ns2 << 32;
	ns = ns1 | ns2;
	return (ns);
}

//
// Redefine a few operators
//

// Assignments between two LogicalNodesets
inline LogicalNodeset&
LogicalNodeset::operator = (const LogicalNodeset &nset)
{
	// check for self-assignment
	if (this == &nset) {
		return (*this);
	}
	if (nset.nbWords > nbWords) {
		// Need to enlarge the LHS to hold the RHS
		delete [] words;
		initialize();
		CL_PANIC(nset.nbWords <= nbWords);
	} else if (nset.nbWords < nbWords) {
		// Zero out the current bitset
		reset();
	}
	(void) memcpy(words, nset.words, sizeof (uint32_t) * nset.nbWords);
	return (*this);
}

// Assignment between a nodeset_t and a LogicalNodeset
// Remaining bits in the LogicalNodeset are set to NULL.
//
inline LogicalNodeset&
LogicalNodeset::operator = (nodeset_t nset)
{
	reset();

	words[0] = (uint32_t)(nset & 0xffffffff);
	words[1] = (uint32_t)(nset >> 32);
	return (*this);

}

// Return (copy of) the 32 bit word at offset 'index'
inline uint32_t
LogicalNodeset::operator[] (int index)
{
	// If current words smaller than index, return 0
	return (index >= nbWords ? 0 : words[index]);
}

//
// Performs binary complement of the LogicalNodeset
//
inline const LogicalNodeset
LogicalNodeset::operator~() const
{
	LogicalNodeset inverse;

	uint_t word = 0;
	while (word < nbWords) {
		inverse.words[word] = ~words[word];
		word++;
	}
	//
	// If nodeset is smaller than theMaxLni, fill in high-order words
	// with complement of 0
	//
	while (word < NBWORDS(theMaxLni)) {
		inverse.words[word] = ~0;
		word++;
	}
	return (inverse);
}


#endif /* _RGM_LOGICAL_NODESET_H */
