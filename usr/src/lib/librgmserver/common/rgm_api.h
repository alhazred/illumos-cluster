/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_API_H
#define	_RGM_API_H

#pragma ident	"@(#)rgm_api.h	1.34	08/05/20 SMI"

#include <rgm/security.h>

#include <rgm/rgm_recep.h>

#include <rgm/rgm_common.h>
#include <rgm/rgm_cnfg.h>
#include <scha.h>
#include <scha_err.h>
#include <rgm/scha_strings.h>
#include <rgm/scha_priv.h>

#ifdef __cplusplus
extern "C" {
#endif


/* rgm_recep_api.cc */
extern void open_res(rs_open_args *args, rs_open_result_t *result);
extern void get_res(rs_get_args *args, get_result_t *result);
extern void get_ispernode_prop(rt_get_args *args, get_result_t *result);
extern void get_rgname(rs_get_args *args, get_result_t *result);
extern void get_res_ext(rs_get_args *args, get_ext_result_t *result);
extern void get_all_extprops(rs_get_args *args, get_result_t *result);
extern void get_res_status(rs_get_state_status_args *args,
    get_status_result_t *result);
extern void get_res_switch(rs_get_switch_args *args,
    get_switch_result_t *result);
extern void get_res_state(rs_get_state_status_args *args, get_result_t *result);
extern void get_failed_status(rs_get_fail_status_args *args,
    get_result_t *result);
extern void set_res_status(rs_set_status_args *args,
    set_status_result_t *result);

/* rgm_recep_rt.cc */
extern void open_rt(rt_open_args *args, open_result_t *result);
extern void get_rt(rt_get_args *args, get_result_t *result);

/* rgm_recep_rg.cc */
extern void open_rg(rg_open_args *args, open_result_t *result);
extern void get_rg(rg_get_args *args, get_result_t *result);
extern void get_rg_state(rg_get_state_args *args, get_result_t *result);

/* rgm_scha_control.cc */
extern void scha_control_action(scha_control_action_t action,
    const char *rg_name, const char *r_name, rgm::lni_t lni,
    scha_result_t *result);

/* rgm_recep_utils.cc */
extern scha_errmsg_t get_rs_prop_val(
    const char *prop_name, rgm_resource_t *resource,
    scha_prop_type_t *type, std::map<rgm::lni_t, char *> &val_str,
    namelist_t **val_arr, boolean_t *need_free, rdeplist_t **val_array_rdep);

/* rgm_recep_cluster.cc */
extern void cluster_open(open_result_t *result);
extern void cluster_get_rgs(get_result_t *result);
extern void cluster_get_rts(get_result_t *result);
extern void cluster_get_node_state(const cluster_get_args *gets_args,
    get_result_t *result);

#ifndef EUROPA_FARM
/* rgm_lb.cc */
extern void lb_op(scha_ssm_lb_args *arg, scha_ssm_lb_result_t *result);

/* rgm_ssm.cc */
extern void rgm_ssm_ip_op(ssm_ip_args *args, ssm_result_t *result);
#endif /* EUROPA_FARM */

/* common functions */
extern scha_errmsg_t get_rt_prop_val(char *prop_name, rgm_rt_t *rt,
    scha_prop_type_t *type, name_t *val_str, namelist_t **val_array,
    boolean_t *need_free);
extern void get_rt_method_pathname(char *method_name, rgm_rt_t *rt,
    name_t *val_str);

extern scha_errmsg_t get_rg_prop_val(char *prop_name, rglist_t *rg,
    scha_prop_type_t *type, name_t *val_str, namelist_t **val_array,
    boolean_t *need_free);

extern scha_errmsg_t store_prop_val(scha_prop_type_t type,
    namelist_t *val_array, std::map<rgm::lni_t, char *> &val_str_prop,
    boolean_t is_pn, name_t node_name, strarr_list **ret_list,
    char *prop_name, rlist_p_t rl);

extern scha_errmsg_t store_prop_val_dep(scha_prop_type_t type,
    rdeplist_t *val_array, strarr_list **ret_list, boolean_t qarg);

extern char *rgm_itoa(int i);

#ifdef __cplusplus
}
#endif

#endif	/* !_RGM_API_H */
