/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * rgm_main.cc
 *
 *	rgmd startup functions
 */


#pragma ident	"@(#)rgm_main.cc	1.134	09/01/06 SMI"

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/clconf_int.h>

#include <cmm/cmm_ns.h>
#include <h/membership.h>
#include <cmm/membership_client.h>

#include <sys/resource.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/procfs.h>
#ifndef linux
#include <sys/int_types.h>
#endif
#include <orb/invo/common.h>
#ifndef EUROPA_FARM
#include <cmm/cmm_ns.h>
#include <cmm/ff_signal.h>
#endif
#include <rgm_proto.h>
#include <libintl.h>
#include <new.h>
#include <rgm/rgm_msg.h>
#ifndef EUROPA_FARM
#include <rgm/rgm_cnfg.h>
#include <cm_callback_impl.h>
#endif
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm/scutils.h>
#include <h/naming.h>
#include <nslib/ns.h>

#ifdef EUROPA_FARM
#include <sys/st_failfast.h>
#endif

/* Zone support */
#include <rgm_logical_node_manager.h>
#include <rgm_logical_node.h>
#include <rgm/sczones.h>

#include <rgmx_hook.h>

#include <sys/sol_version.h>

#define	debuglog	syslog
#if (SOL_VERSION < __s10)
bool IS_GLOBAL_ZONE = B_TRUE;
char RGMD_RECEPTIONIST_NAME[DIR_PATH_LEN + 66]; // Should hold the scha
						// progname.
char RGMPRESIDENT[DIR_PATH_LEN + 16]; // 15 is for "." and
					//  string "RGM_PRESIDENT"
char RGMLOGDIR[DIR_PATH_LEN + 18]; // 17 is for string "/var/cluster/rgm/"
char RGMD_LOCK[DIR_PATH_LEN + 28]; // 17 is for string "/var/cluster/rgm/"
					// and 10 for string ".rgmd.lock"
char *ZONE = NULL;
char *progname;
char rgm_user_name[DIR_PATH_LEN + 5]; // 4 for string "rgmd"
char RGMD_SYSLOG_TAG[DIR_PATH_LEN + 18]; // 18 for "Cluster.RGM..rgmd"
const char *rgm_product_str = RGMD_SYSLOG_TAG;
const char *syslog_tag = RGMD_SYSLOG_TAG;
#else
/* global variables */
bool IS_GLOBAL_ZONE = B_TRUE;
bool IS_PROXY_RGMD = B_FALSE;
char RGMD_RECEPTIONIST_NAME[ZONENAME_MAX + 66]; // Should hold the scha
						// progname.
char RGMPRESIDENT[ZONENAME_MAX + 16]; // 15 is for "." and
					// string "RGM_PRESIDENT"
char RGMLOGDIR[ZONENAME_MAX + 18]; // 17 is for string "/var/cluster/rgm/"
char RGMD_LOCK[ZONENAME_MAX + 28]; // 17 is for string "/var/cluster/rgm/"
					// and 10 for string ".rgmd.lock"
char *ZONE = NULL;
char *progname;
char rgm_user_name[ZONENAME_MAX + 5]; // 4 for string "rgmd"
char RGMD_SYSLOG_TAG[ZONENAME_MAX + 18]; // 18 for "Cluster.RGM..rgmd"
const char *rgm_product_str = RGMD_SYSLOG_TAG;
const char *syslog_tag = RGMD_SYSLOG_TAG;
#endif

char *rgmd_sched_class = "RT";
pri_t rgmd_sched_priority = 0;

int *node_incarnation;
int *rgm_incarnation;
int *pid_incarnation;
static sc_syslog_msg_handle_t handle;

#if (SOL_VERSION >= __s10)
#define	zone_is_up(i)	(node_incarnation[i] != INCN_UNKNOWN)
#define	need_new_rgm(i)	(pid_incarnation[i] == 0 && rgm_incarnation[i] \
	< node_incarnation[i])
#define	pid_matches(pid, i)	(pid_incarnation[i] == pid)
unsigned int max_wait_period_for_step_sync = 5;	// seconds
static void register_zone_membership(
    char *zone, mem_callback_funcp_t callback_func);
static void register_membership(char *zone, mem_callback_funcp_t callback_func);
static void tell_membership_subsystem_to_create_membership(
    char *zonenamep, char *process_namep);
static void start_rgm_reconfig_thread();
static void rgm_register_cluster_dir_callbacks();

boolean_t rgm_ru_transition_in_progress = B_FALSE;
#define	RGM_RU_TRANSITION_SLEEP_PERIOD	10	// In seconds
#endif


#ifndef EUROPA_FARM
cmm::callback_info cbinfo;
#endif

/*
 * New Tables for zone support:
 * - lnManager defines the mapping between
 *   LN (Logical Nodename) and LNI (Logical Node Id)
 *
 * . The president keeps the list of all the
 *   zones in the cluster
 * . The slaves only deal with the zones
 *   configured on their current cluster node.
 *
 * The allocation of the 'lni' (LogicalNode
 *  Identification) is done by the president
 *  with the following rules:
 * . LN can be a cluster node (lni = nodeid)
 * . Ln can be a zone (lni > MAXNODES)
 *
 */
LogicalNodeManager *lnManager;


static void init_signal_handlers(void);
static void sig_handler(int sig);
static void set_signal_handler(int sig);


uint_t	cmtrace_mask = 0xffff;

void create_check_dir_files() {
	sprintf(RGMD_LOCK, "/var/cluster/run/%s.rgmd.lock",
	    ZONE ?  ZONE : GLOBAL_ZONENAME);
	sprintf(PP_PATH, "/var/cluster/rgm/%s/pingpong_log",
	    ZONE ? ZONE : GLOBAL_ZONENAME);
	sprintf(RGMLOGDIR, "/var/cluster/rgm/%s",
	    ZONE ? ZONE : GLOBAL_ZONENAME);
	sprintf(RGMPRESIDENT, "%s.RGM_PRESIDENT",
	    ZONE ? ZONE : GLOBAL_ZONENAME);
	mkdir(RGMLOGDIR, S_IRWXU);
	if (access(RGMLOGDIR, W_OK) != 0) {
		abort();
	}
}


#if (SOL_VERSION >= __s10)
//
// Tell the membership subsystem to start
// maintaining process membership for rgmd.
//
static void
tell_membership_subsystem_to_create_membership(
    char *zonenamep, char *process_namep)
{
	//
	// XXX Make sure that the zonenamep and process_namep below
	// XXX are not freed multiple times.
	//
	uint32_t clid = 0;
	if (clconf_get_cluster_id(zonenamep, &clid)) {
		// Must be able to convert cluster name to cluster id
		ASSERT(0);
	}
	membership::engine_var engine_v = cmm_ns::get_membership_engine_ref();
	ASSERT(!CORBA::is_nil(engine_v));
	membership::engine_event_info data;
	data.mem_manager_info.mem_type = membership::PROCESS_UP_ZONE_DOWN;
	data.mem_manager_info.cluster_id = clid;
	data.mem_manager_info.process_namep = os::strdup(process_namep);
	Environment e;
	CORBA::Exception *exp = NULL;
	//
	// Deliver creation event to membership engine.
	// If engines are reconfiguring, this call would wait.
	// If engine returns a retry exception, then this code
	// would retry the creation request to engine.
	//
	while (1) {
		engine_v->deliver_event_to_engine(
		    membership::CREATE_MEMBERSHIP, data, e);
		exp = e.exception();
		if (exp == NULL) {
			break;
		}
		if (membership::retry_create_or_delete::_exnarrow(exp)) {
			// Retry creation
			ucmm_print(
			    "tell_membership_subsystem_to_create_membership",
			    "Received retry_create_or_delete exception "
			    "while requesting membership engine "
			    "to create PROCESS_UP_ZONE_DOWN membership "
			    "for <%s, zone %s>\n", process_namep, zonenamep);
		} else {
			ucmm_print(
			    "tell_membership_subsystem_to_create_membership",
			    "Received unknown exception "
			    "while requesting membership engine "
			    "to create PROCESS_UP_ZONE_DOWN membership "
			    "for <%s, zone %s>\n", process_namep, zonenamep);
			ASSERT(0);
		}
		e.clear();
	}
}
#endif

//
// Setup the membership for RGM.
//
// For S10 and higher versions, process membership support exists.
// So, for S10 and higher versions, we check if rgm is running
// a software version that supports process membership.
// If so, then this method does all the actions required
// for the process membership of rgmd to work.
// Else if rgm is running a software version that does not support
// process membership, then this method will setup the ucmm-style
// of automaton membership; and all the process membership actions will
// be done during the version upgrade callbacks while upgrading
// to a version that support process membership.
//
// For Solaris versions older than S10, we do not have process membership.
// So for those versions, this method will setup the ucmm-style
// of automaton membership.
//
void
setup_membership(bool setup_process_membership)
{
#if (SOL_VERSION >= __s10)
	if (setup_process_membership) {
		start_rgm_reconfig_thread();
		tell_membership_subsystem_to_create_membership(
		    ZONE, RGMD_PROC_NAME);
		{
			// Query zone cluster membership.
			Environment e;
			CORBA::Exception *exp;
			cmm::membership_t membership;
			cmm::seqnum_t seqnum;
			LogicalNodeset mem_nodeset;

			membership::api_var membership_api_v =
			    cmm_ns::get_membership_api_ref();
			if (CORBA::is_nil(membership_api_v)) {
				abort();
			}

			membership_api_v->get_cluster_membership(
			    membership, seqnum, ZONE, e);
			exp = e.exception();
			if (exp != NULL) {
				abort();
			}
			for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
				if (membership.members[nid] != INCN_UNKNOWN) {
					mem_nodeset.addLni(nid);
				}
			}
			Rgm_state->current_zc_membership = mem_nodeset;
		}
		register_membership(ZONE, rgm_process_membership_callback);
		// For Zone clusters register for the zone up/down callbacks.
		if (!IS_GLOBAL_ZONE) {
			register_zone_membership(ZONE, rgm_zone_reconfig);
		}

		//
		// Notify membership subsystem that process has come up
		//
		uint32_t clid = 0;
		if (clconf_get_cluster_id(ZONE, &clid)) {
			// Must be able to convert cluster name to cluster id
			ASSERT(0);
		}
		membership::engine_var engine_v =
		    cmm_ns::get_membership_engine_ref();
		ASSERT(!CORBA::is_nil(engine_v));
		membership::engine_event_info event_info;
		event_info.mem_manager_info.mem_type =
		    membership::PROCESS_UP_ZONE_DOWN;
		event_info.mem_manager_info.cluster_id = clid;
		event_info.mem_manager_info.process_namep =
		    os::strdup(RGMD_PROC_NAME);
		event_info.event = membership::PROCESS_UP;
		Environment e;
		CORBA::Exception *exp = NULL;
		engine_v->deliver_event_to_engine(
		    membership::DELIVER_MEMBERSHIP_CHANGE_EVENT, event_info, e);
		if ((exp = e.exception()) != NULL) {
			e.clear();
			ASSERT(0);
		}
		return;
	}
#endif	// (SOL_VERSION >= __s10)

	//
	// Reaching here means either we are in Solaris version older than S10,
	// or rgm is running a version that does not support process membership.
	// So we setup the ucmm-style of automaton membership.
	//

	/*
	 * Initialize the userland CMM that will provide the reconfiguration
	 * callbacks when cluster membership changes.
	 */
#if (SOL_VERSION >= __s10)
	//
	// rgmd sets up its own process membership. So we do not need
	// ucmm code (common to rgmd and ucmmd) to setup process membership
	// again. Hence we pass false in the last argument.
	//
	if (ucmm_initialize(rgm_user_name, syslog_tag,
	    (new(std::nothrow) cm_callback_impl())->get_objref(),
	    cbinfo, B_FALSE) != 0) {
#else
	if (ucmm_initialize(rgm_user_name, syslog_tag,
	    (new(std::nothrow) cm_callback_impl())->get_objref(),
	    cbinfo) != 0) {
#endif
		//
		// SCMSGS
		// @explanation
		// The daemon indicated in the message tag (rgmd or ucmmd) was
		// unable to initialize its interface to the low-level cluster
		// membership monitor. This is a fatal error, and causes the
		// node to be halted or rebooted to avoid data corruption. The
		// daemon produces a core file before exiting.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes,
		// and of the core file generated by the daemon. Contact your
		// authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: ucmm_initialize() failed");
		sc_syslog_msg_done(&handle);
		abort();
	}
}

//
// at this stage all the stuff like ZONE, CZLIST, etc should be set up
// as also orb initialisation, etc.
//
int startrgmd() {
	ucmm_print("startrgmd", "new rgmd for zone:%s:", ZONE);
	sprintf(RGMD_RECEPTIONIST_NAME, "%s_%s", ZONE, SCHA_PROGRAM_NAME);

	if (strcmp(ZONE, GLOBAL_ZONENAME) == 0)
		IS_GLOBAL_ZONE = B_TRUE;
	else
		IS_GLOBAL_ZONE = B_FALSE;

	int retval;
	// SC SLM addon start
	unsigned int saved_slm_global_zone_shares;
	unsigned int saved_slm_default_pset_min;
	// SC SLM addon end

	/*
	 * Check if we are proxy rgmd
	 */
	if (IS_GLOBAL_ZONE == B_FALSE) {
#if (SOL_VERSION >= __s10)
		if (!sc_zone_configured_on_cur_node(ZONE)) {
			IS_PROXY_RGMD = B_TRUE;
		}
#endif
	}
	/*
	 * Check if the process was started by the superuser.
	 */
	if (getuid() != 0) {
		//
		// SCMSGS
		// @explanation
		// The rgmd can only be started by the super-user.
		// @user_action
		// This probably occured because a non-root user
		// attempted to start rgmd manually. Normally the
		// rgmd is started automatically when the node is
		// booted.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: must be superuser to start %s"),
		    progname);
		exit(1);
	}

	(void) set_new_handler(&abort);
	create_check_dir_files();

	/*
	 * Obtain cluster configuration information
	 * before detaching from terminal.
	 */
#ifndef EUROPA_FARM
	get_cluster_info();
#endif
	/*
	 * ensure that another rgmd daemon is not running
	 * This must be done after the fork!
	 */
	make_unique(RGMD_LOCK);

	//
	// Arm a failfast device to kill the node if the rgmd process
	// dies either due to hitting a fatal bug or due to being killed
	// inadvertently by an operator.
	//
	// This needs to be done after initializing the ORB since the ORB
	// is needed to lookup the failfast admin object.
	//
#ifdef EUROPA_FARM
	if (st_ff_arm("frgmd") != 0) {
		//
		// SCMSGS
		// @explanation
		// The rgmd program could not enable the failfast mechanism.
		// The failfast mechanism is designed to prevent data
		// corruption by causing the node to be shutdown in the
		// event that the rgmd program dies.
		// @user_action
		// To avoid data corruption, the rgmd will halt or reboot
		// the node. Contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"fatal: could not arm failfast");
		abort();
	}
#else
		char failfast_string[ZONENAME_MAX + 5];
	strcpy(failfast_string, ZONE);
	strcat(failfast_string, "rgmd");
	register_for_failfast(failfast_string);
#endif
	/*
	 * Change process scheduling class and process priority.
	 */
	sched_init();

	/*
	 * Set up signal handlers.
	 */
	init_signal_handlers();

	/*
	 * initialize RGM and the rgm_state structure.
	 */
	rgm_setup();

	/*
	 * Now that the mutex has been initialized, grab it and hold it while
	 * we register CCR callbacks and initializing the ucmm callbacks. We
	 * don't want a reconfiguration or callback thread to be able to run
	 * before we've finished initializing everything.
	 */
#ifndef EUROPA_FARM
	rgm_lock_state();
#endif


	// Europa - Initialize RGMX hook managemement.
	rgmx_hook_init();

	// Europa - Read configuration, initialize fencing.
	// and allocate ring buffer for tracing.
	(void) rgmx_hook_call(rgmx_hook_cfg_init);

	// Europa - Maximum number of farm nodes are added to the
	// maximum number of nodes.
	(void) rgmx_hook_call(rgmx_hook_set_maxnodes);

	lnManager = new LogicalNodeManager;

#ifndef EUROPA_FARM
	/*
	 * Must call this after initializing ORB.
	 * cache the local nodeid to avoid CCR read overhead
	 */
	cache_local_nodeid();
#else
	cache_local_nodename_nodeid();
#endif

	/*
	 * Create sync thread.
	 *
	 * The sync thread will be idle until we signal its cond_var.
	 * We create the thread now so that we can sync to disk before
	 * aborting the node, even if we are too low on memory at that
	 * time to create a new thread.
	 */
	thr_create_err(NULL, 0, sync_thread_start, NULL, THR_BOUND, NULL);

	/*
	 * Create infr_table_cb_thread
	 *
	 * The infr_table_cb_thread will be idle until a CCR callback
	 * for the infrastructure table signals the condition variable
	 * for the infr_table_cb_thread.
	 */
	start_infr_table_cb_thread();

#if (SOL_VERSION >= __s10)
	/*
	 * Currently, we do not need this for Solaris versions older than S10.
	 * initialize the rgm version and register for the version
	 * manager callbacks.  Must do _after_ we create the sync thread,
	 * because if the versioning initialization fails, we call
	 * abort_node.
	 */
	rgm_initialize_versioning();
#endif

#ifndef EUROPA_FARM
	/*
	 * Check that all our ccr tables are valid by calling the
	 * utility rgmcnfg_get_invalid_tables.  If it returns any
	 * tables, or if it returns an error, we must log a severe error
	 * and halt the node.  We cannot just abort the rgmd and use
	 * the failfast mechanism to bring down the node, because that
	 * would cause a reboot, at which point the same error would probably
	 * be encountered.
	 */
	namelist_t *invalid_names;
	scha_errmsg_t res;

	res = rgmcnfg_get_invalid_tables(&invalid_names, ZONE);
	if (res.err_code != SCHA_ERR_NOERR) {
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to check the validity of the CCR
		// tables representing Resource Types and Resource Groups. The
		// node will be halted to prevent further errors.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes.
		// Contact your authorized Sun service provider for assistance
		// in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ALERT, MESSAGE,
		    "Could not validate CCR tables; halting node");
		abort_node(B_FALSE, B_FALSE, NULL);

		/* NOT REACHED */
	}
	if (invalid_names != NULL) {
		/*
		 * Report all the invalid tables.
		 */
		for (; invalid_names; invalid_names = invalid_names->nl_next) {
			//
			// SCMSGS
			// @explanation
			// The CCR reported to the rgmd that the CCR table
			// specified is invalid or corrupted. The node will be
			// halted to prevent further errors.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes. Contact your authorized Sun service provider
			// for assistance in diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ALERT, MESSAGE,
			    "CCR reported invalid table %s; halting node",
			    invalid_names->nl_name);
		}
		/*
		 * Don't bother freeing the namelist, since we'll be bringing
		 * the entire node down shortly.
		 */
		abort_node(B_FALSE, B_FALSE, NULL);

		/* NOT REACHED */
	}

	//
	// We don't need to free the invalid_names list because we know
	// it's empty.
	//

	//
	// cache nodename-id mapping from CCR. aborts if failure creating cache
	// Must call this function before initialising the rgm comm server and
	// the receptionist. This will ensure that any request which may require
	// nodename mapping will not be initiated before mapping is done.
	//
	create_cache(B_FALSE);


	rgm_comm_init();

	//
	// Setup the membership infrastructure of RGM, passing true if
	// process membership support is available in this RGM software version.
	//
	// ------------------------------------------------------
	// Setting the Rgm_state->using_process_membership flag :
	// ------------------------------------------------------
	// WHY WE NEED THIS FLAG :
	// This flag has important implications during the step synchronized
	// rgmd reconfiguration that happens as a result of the new process
	// membership callbacks. In step1 of the reconfiguration,
	// the seqnum received from callback is matched with the seqnum
	// known by the rgmd (stored in Rgm_state). If the received seqnum
	// is older than the known seqnum, then the reconfiguration is
	// not done. All rgmds ignore the reconfiguration in that case.
	//
	// During rolling upgrade version commit, all rgmds switch to
	// using the new process membership and discard the ucmm-style
	// automaton membership. In one step of the upgrade callback,
	// rgmds register with process membership. As a result, the process
	// membership subsystem establishes a seqnum and membership
	// for the rgmds, and these data is stored in the process
	// membership data structures in kernel. However, rgmds ignore
	// these process membership callbacks until step2 of upgrade callback.
	// It is possible that the seqnum in process membership is less
	// than the seqnum known by rgm previously from ucmm-style membership.
	// So, the next time a process membership callback arrives after
	// rgmds have done step2 upgrade callback, they will try to reconfigure
	// and find that the received seqnum is less than the seqnum they
	// know about and hence ignore the reconfiguration. To avoid this
	// we use this 'using_process_membership' flag.
	//
	// HOW IS THIS FLAG SET :
	// This flag is not set at bootup.
	// At this point in code, rgmd is just starting up.
	// (1) If we are running the new version software
	// that supports process membership, then we know that all rgmds
	// are using process membership callbacks, and not the ucmm-style
	// automaton membership. Hence we mark in the rgm state structure
	// that we are using process membership. This flag is checked
	// during rgm reconfiguration.
	// (2) If we are not yet running the version that supports process
	// membership, then we can be sure that none of the rgmds will be
	// supporting process membership feature now. Sometime in future,
	// a upgrade might take place.
	// (2.1) In case of non-rolling upgrade, all rgmds will boot up
	// running the version that supports process membership feature.
	// (2.2) In case of rolling upgrade, this flag will be set in
	// the first step of the first rgmd reconfiguration that happens
	// as a result of process membership callback.
	//
	// While checking for the latest seqnum in the first step of
	// reconfiguration due to process membership callbacks,
	// if this flag is not set, we do not ensure that the seqnum
	// received from callback is higher than known seqnum.
	// We do this as we know that we are beginning to use the new
	// process membership feature. And we set this flag in the first
	// step, so that next time we reconfigure, we will ensure
	// that the seqnum received from callback is newer than
	// the already known seqnum.
	//
	boolean_t process_membership = process_membership_support_available();
	if (process_membership) {
		setup_membership(true);
	} else {
		setup_membership(false);
	}
#if (SOL_VERSION >= __s10)
	if (process_membership) {
		Rgm_state->using_process_membership = (boolean_t)true;
	} else {
		Rgm_state->using_process_membership = (boolean_t)false;
	}
#endif


	//
	// register for CCR callbacks for infrastructure file modification.
	// abort if function fails, since we need to update nodename to nodeid
	// mapping when it changes so as not to get stale values.
	//
	register_ccr_callbacks();

#if (SOL_VERSION >= __s10)
	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		// if this rgmd is for a zone cluster, then it should register
		// for cluster_directory callbacks as well. This is required
		// because this rgmd has to stop running as soon as the zone
		// cluster is deleted.
		rgm_register_cluster_dir_callbacks();
	}
#endif
	//
	// get cluster membership information. Must be called only after
	// initialising the ucmm from where it loads the membership information.
	//
	if (process_membership) {
		update_cluster_state(B_TRUE, B_TRUE);
	} else {
		update_cluster_state(B_TRUE, B_FALSE);
	}

	// Europa - Add the configured farm nodes to the static membership.
	if (rgmx_hook_call(rgmx_hook_update_configured_nodes,
	    &Rgm_state->static_membership, &retval) == RGMX_HOOK_ENABLED) {
		if (retval == -1) {
			//
			// SCMSGS
			// @explanation
			// The RGM couldn't update membership information for
			// farm nodes and will abort to avoid undesired results.
			// @user_action
			// Contact your authorized SUN representatives for
			// resolution of the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: rgmx_update_configured_nodes() : failed");
			abort();
		}
		Rgm_state->num_static_nodes =
		    Rgm_state->static_membership.count();
	}

	//
	// Initialize new tables for Zone support. Must do after the static
	// membership (Rgm_state->static_membership) has been populated with
	// information from ucmm_getcluststate.
	//
	lnManager->initialize();

#endif // !EUROPA_FARM

	/*
	 * If at S10 or higher, register with libsczones for the zone state
	 * change callbacks
	 */
#if SOL_VERSION >= __s10
	ASSERT(ZONE != NULL);
	if (strcmp(ZONE, GLOBAL_ZONENAME) == 0)
		initialize_zone_callbacks();
#endif

	// Europa - startup phase, configuration/communication setup ...
	(void) rgmx_hook_call(rgmx_hook_startup);

	/*
	 * ok to release the lock before creating the receptionist thread
	 */
#ifndef EUROPA_FARM
// SC SLM addon start
	scslm_get_scconf(&saved_slm_global_zone_shares,
	    &saved_slm_default_pset_min);
// SC SLM addon end

	rgm_unlock_state();

// SC SLM addon start
	scslm_set_scconf(saved_slm_global_zone_shares,
	    saved_slm_default_pset_min);
// SC SLM addon end
#endif

	/*
	 * launch receptionist
	 */
	recep_thread_start();

	// Europa - start RPC services.
	// This function never returns ...
	(void) rgmx_hook_call(rgmx_hook_rpc_start);
#if !DOOR_IMPL
	svc_run();
#else
	thr_exit(NULL);
#endif

	/* NOTREACHED */
	return (0);
}

/*
 * Mark a file descriptor to be closed by the exec system call.
 */
void
close_on_exec(int fd)
{
	if (fcntl(fd, F_SETFD, 1) == -1) {
		//
		// SCMSGS
		// @explanation
		// This error should not occur. The rgmd will produce a core
		// file and will force the node to halt or reboot to avoid the
		// possibility of data corruption.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes,
		// and of the rgmd core file. Contact your authorized Sun
		// service provider for assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: fcntl(F_SETFD): %s (UNIX error %d)",
		    strerror(errno), errno);
		abort();
	}
}

/*
 * Set up signal handlers and do other signal related initializations.
 */
static void
init_signal_handlers()
{
	set_signal_handler(SIGTERM);
	set_signal_handler(SIGINT);
	set_signal_handler(SIGALRM);
	set_signal_handler(SIGPOLL);
}

/*
 * Set up a signal handler in the SA_RESTART mode.
 */
static void
set_signal_handler(int sig)
{
	struct sigaction act;

	(void) sigemptyset(&act.sa_mask);
	act.sa_handler = (void(*)(int))sig_handler;
	act.sa_flags = SA_RESTART;

	if (sigaction(sig, &act, NULL) == -1) {
		//
		// SCMSGS
		// @explanation
		// The rgmd has failed to initialize signal handlers by a call
		// to sigaction(2). The error message indicates the reason for
		// the failure. The rgmd will produce a core file and will
		// force the node to halt or reboot to avoid the possibility
		// of data corruption.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes,
		// and of the rgmd core file. Contact your authorized Sun
		// service provider for assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: sigaction: %s (UNIX errno %d)",
		    strerror(errno), errno);
		abort();
	}
}

/*
 * Signal handler for the SIGTERM, SIGPOLL, SIGALRM, and SIGINT signals.
 */
static void
sig_handler(int sig)
{
	ucmm_print("RGM", NOGET("sig_handler: signal %d caught\n"), sig);

	switch (sig) {
	case SIGTERM:
#ifdef EUROPA_FARM
		st_ff_disarm();
#endif
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: received signal %d"), sig);
		_exit(1);
		break;
	case SIGALRM:
		// Fall through
	case SIGPOLL:
		// Fall through
	case SIGINT:
		// Fall through
	default:
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("received signal %d"), sig);
	}
}



#if SOL_VERSION >= __s10
//
// Register with Membership API subsystem for zone cluster membership callback
// so that we will get a callback whenever the zone cluster node boots up
// or shuts down. This membership information is required as rgmd continues to
// run in case of zone cluster node shutdown/death.
//
static void register_zone_membership(char *zone, mem_callback_funcp_t funcp) {
	nodeid_t nid;

	ASSERT(zone != NULL);
	//
	// Get a reference to the membership API object
	// on the local node from nameserver
	//
	membership::api_var membership_api_v = cmm_ns::get_membership_api_ref();

	Environment e;
	CORBA::Exception *exp;
	// This callback object should be hosted by the client
	membership::client_ptr client_ptr =
	    (new membership_client_impl(rgm_user_name, membership::ZONE_CLUSTER,
	    zone, NULL, funcp))->get_objref();

	// Register for membership callbacks for a cluster
	membership_api_v->register_for_cluster_membership(client_ptr, zone, e);
	exp = e.exception();
	if (exp != NULL) {
		abort();
	}
}

//
// Register with Membership API subsystem for process-zone membership callbacks
// so that we will get a callback whenver there are process membership changes
// for this rgmd.
//
static void register_membership(char *zone, mem_callback_funcp_t funcp) {

	Environment e;
	CORBA::Exception *exp;

	ASSERT(zone != NULL);

	membership::client_ptr client_p = membership::client::_nil();
	client_p = (new membership_client_impl(
	    rgm_user_name, membership::PROCESS_UP_ZONE_DOWN, zone,
	    RGMD_PROC_NAME, funcp))->get_objref();
	membership::api_var membership_api_v = cmm_ns::get_membership_api_ref();
	ASSERT(!CORBA::is_nil(membership_api_v));
	membership_api_v->register_for_process_zone_membership(
	    client_p, zone, RGMD_PROC_NAME, e);
	exp = e.exception();
	if (exp != NULL) {
		abort();
	}
}
#endif
#if (SOL_VERSION >= __s10)
//
// For S10 and above, RGM uses process membership feature provided
// by the kernel membership subsystem. RGM does not use the ucmm-style
// of automaton membership and step callbacks anymore.
// Since the process membership callbacks from the kernel membership
// subsystem are not synchronized across nodes, it could happen that
// one RGM receives the membership callback before one of its peer RGMs.
// If RGMs do not have step synchronization during membership reconfiguration
// (president election, etc.), then it might lead to problems as different
// RGMs could be operating on a different membership/seqnum.
// Hence we have a light-weight reconfiguration thread (similar in spirit
// to the CMM automaton) that handles the RGM membership reconfiguration
// after receiving a membership callback. The reconfiguration threads
// on peer RGMs synchronize steps among themselves.
// They use two steps essentially :
// (i) In the first step, the reconfiguration thread copies the membership
// data received from membership callback on to RGM's local membership
// data structures.
// (ii) In the second step, the reconfiguration threads drive the big
// uninterruptible RGM reconfiguration method, which does president election
// and other RGM reconfiguration work.
//
os::mutex_t rgm_reconfig_lock;
os::condvar_t rgm_reconfig_cv;
rgm_reconfig_membership_callback_data_t rgm_reconfig_membership_cb_data;
boolean_t rgm_reconfig_membership_changed = B_FALSE;

//
// Lock and condition variable used to protect :
// (i) states of rgmd peers, maintained on rgmd on lowest nodeid
// (ii) information received from lowest nodeid rgmd by other rgmds
//
os::mutex_t peer_rgm_state_lock;
os::condvar_t peer_rgm_state_cv;

// Used on the rgmd on lowest nodeid
peer_rgm_state_t peer_rgm_state[NODEID_MAX+1] = {{0, 0}};

// Used on the rgmds that are not on the lowest nodeid
uint32_t lowest_nid_step_num = 0;
cmm::seqnum_t lowest_nid_seqnum = 0;
rgm::sync_result_t lowest_nid_sync_result = rgm::SYNC_UNSTABLE;

#define	RGM_RECONFIG_MAX_STEP_NUM 2
typedef bool (*rgm_reconfig_step_handler_t)(
    const cmm::membership_t, const cmm::seqnum_t);
rgm_reconfig_step_handler_t rgm_reconfig_steps[RGM_RECONFIG_MAX_STEP_NUM+1] = {
    NULL,			// For indexing
    rgm_reconfiguration_step1,	// Step 1
    rgm_reconfiguration_step2	// Step 2
    };

// Helper methods during reconfiguration
static bool
inform_rgm_that_ready_to_do_step(
    nodeid_t target_nid, uint32_t step_num, cmm::seqnum_t seqnum);
static rgm::sync_result_t
step_sync_for_rgm(nodeid_t lowest_nid, uint32_t step_num,
    cmm::seqnum_t seqnum, cmm::membership_t membership);
static rgm::rgm_comm_var lookup_rgm_comm_ref(nodeid_t nid);

//
// This method is used by the rgmd reconfig thread while communicating
// with other the reconfig thread of other rgmds. Since we lookup nodeids
// that are part of membership, and since the membership we got came from
// process membership subsystem (which would have marked a rgmd up only
// after it finished its initialization, including binding rgm_comm ref
// in nameserver), so we are sure to find the rgm_comm ref for the target
// rgmd from the nameserver.
//
// XXX The other alternative to this method is to use rgm_getcommref()
// XXX but that is done before rgm_comm_update() during step2 of reconfig.
// XXX Hence the refs would not be initialized in rgm_comm_list.
// XXX Maybe call rgm_comm_update() once we receive process membership callback.
// XXX That is no need to synchronize rgmds before they do rgm_comm_update().
// XXX Think more about it.
//
static rgm::rgm_comm_var
lookup_rgm_comm_ref(nodeid_t nid)
{
	char nametag[32];
	rgm::rgm_comm_var rgm_comm_v;
	naming::naming_context_var context_v;
	Environment e;
	CORBA::Object_var obj_v;

	context_v = ns::root_nameserver_cz(ZONE);
	(void) sprintf(nametag, "RGM_COMM_SERVER_%d", nid);
	obj_v = context_v->resolve(nametag, e);
	if (e.exception() != NULL) {
		return (rgm::rgm_comm::_nil());
	}
	rgm_comm_v = rgm::rgm_comm::_narrow(obj_v);
	if (CORBA::is_nil(rgm_comm_v)) {
		return (rgm::rgm_comm::_nil());
	}
	return (rgm_comm_v);
}

//
// Inform the rgmd on the specified node that
// we are ready to do a step.
// Returns true on success, false on failure.
//
static bool
inform_rgm_that_ready_to_do_step(
    nodeid_t target_nid, uint32_t step_num, cmm::seqnum_t seqnum)
{
	Environment env;
	CORBA::Exception *exp = NULL;

	ASSERT(step_num >= 0);

	ucmm_print("RGM", NOGET("In inform_rgm_that_ready_to_do_step : "
	    "target_nid = %d, step_num = %d, seqnum = %d\n"),
	    target_nid, step_num, (uint32_t)seqnum);

	rgm::rgm_comm_var target_rgm_comm_v = lookup_rgm_comm_ref(target_nid);
	if (CORBA::is_nil(target_rgm_comm_v)) {
		ASSERT(0);
		ucmm_print("RGM", NOGET("In inform_rgm_that_ready_to_do_step : "
		    "lookup of comm ref for nid %d failed\n"), target_nid);
		return (false);
	}

	target_rgm_comm_v->idl_reconfig_ready_to_do_step(
	    orb_conf::local_nodeid(), step_num, seqnum, env);
	if ((exp = env.exception()) != NULL) {
		// XXX What exceptions?
		ucmm_print("RGM",
		    NOGET("In inform_rgm_that_ready_to_do_step : "
		    "exception raised\n"));
		return (false);
	}

	return (true);
}

static void
lowest_nid_rgmd_sends_info(
    cmm::seqnum_t seqnum, nodeset_t mem_nodeset,
    uint32_t step_num, rgm::sync_result_t sync_result)
{
	ASSERT(step_num >= 0);
	ASSERT(seqnum >= 0);

	ucmm_print("RGM", NOGET("In lowest_nid_rgmd_sends_info : "
	    "seqnum = %d, mem_nodeset = %llx, step_num = %d, "
	    "sync_result = %s\n"), (uint32_t)seqnum, mem_nodeset, step_num,
	    (sync_result == rgm::SYNC_UNSTABLE)?
	    "SYNC_UNSTABLE" : "SYNC_PROCEED");

	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (!nodeset_contains(mem_nodeset, nid)) {
			continue;
		}
		if (nid == orb_conf::local_nodeid()) {
			//
			// Lowest nodeid does not tell itself.
			// It already knows whether to proceed
			// or to restart reconfiguration.
			//
			continue;
		}
		Environment env;
		CORBA::Exception *exp = NULL;
		rgm::rgm_comm_var target_rgm_comm_v = lookup_rgm_comm_ref(nid);
		if (CORBA::is_nil(target_rgm_comm_v)) {
			//
			// We thought that a rgmd was alive, but it has probably
			// gone down. Anyway continue to tell other rgmds.
			// The rgm reconfig threads will anyway see that
			// a newer process membership callback has happened,
			// and will restart their reconfiguration.
			//
			continue;
		}
		ucmm_print("RGM", NOGET("lowest nid sending to nid %d\n"), nid);
		target_rgm_comm_v->idl_reconfig_do_step(
		    step_num, seqnum, sync_result, env);
		if ((exp = env.exception()) != NULL) {
			//
			// We could get COMM_FAILURE or INV_OBJREF if
			// the target rgmd dies while we are making invocations
			// on it. We ignore such exceptions and proceed to
			// inform others.
			// The rgm reconfig threads will anyway see that
			// a newer process membership callback has happened,
			// and will restart their reconfiguration.
			//
			ucmm_print("RGM", NOGET("exception raised by "
			    "idl_reconfig_do_step, target nid %d\n"), nid);
			env.clear();
			continue;
		}
	}
}

static nodeset_t
which_rgmds_are_alive()
{
	nodeset_t alive_rgmds = 0;
	Environment env;
	CORBA::Exception *exp = NULL;

	// Find what rgmds have their rgm_comm references in nameserver
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		rgm::rgm_comm_var target_rgm_comm_v = lookup_rgm_comm_ref(nid);
		if (!CORBA::is_nil(target_rgm_comm_v)) {
			//
			// This rgmd could be alive; do a dummy IDL invocation
			// to confirm that the rgmd is really alive.
			//
			CL_PANIC(env.exception() == NULL);
			target_rgm_comm_v->idl_is_rgm_alive(env);
			if (env.exception() == NULL) {
				//
				// This rgmd is alive; since its rgm_comm
				// reference is in nameserver, the node is
				// present in base cluster membership
				// and an IDL invocation on the rgm_comm ref
				// succeeds. This signifies that our peer
				// rgmd is running on that node.
				//
				nodeset_add(alive_rgmds, nid);
			} else {
				env.clear();
			}
		}
	}

	ucmm_print("RGM", NOGET("In which_rgmds_are_alive : "
	    "alive_rgmds = %llx\n"), alive_rgmds);
	return (alive_rgmds);
}

//
// If we are the lowest nodeid in membership received from callback,
// this method will wait until all peer rgms are ready to do the specified step.
// If all rgms are ready to do the specified step in the specified seqnum,
// then return rgm::SYNC_PROCEED.
// If any rgm's seqnum is higher than the specified seqnum, we know
// another reconfiguration has triggered and we return rgm::SYNC_UNSTABLE.
//
// If we are not the lowest nodeid in membership received from callback,
// then we wait in this method until we hear from the lowest nodeid
// XXX that means lowest nodeid should always tell others without fail
//
rgm::sync_result_t
step_sync_for_rgm(nodeid_t lowest_nid, uint32_t step_num,
    cmm::seqnum_t seqnum, cmm::membership_t membership)
{
	ASSERT(step_num >= 0);
	ASSERT(seqnum >= 0);

	ucmm_print("RGM", NOGET("In step_sync_for_rgmd : lowest_nid = %d, "
	    "step_num = %d, seqnum = %d\n"), lowest_nid,
	    step_num, (uint32_t)seqnum);

	peer_rgm_state_lock.lock();

	if (lowest_nid == orb_conf::local_nodeid()) {
		rgm::sync_result_t retval = rgm::SYNC_PROCEED;
		nodeset_t alive_rgmds = which_rgmds_are_alive();
		while (1) {

			//
			// Before waiting for every rgmd in membership to inform
			// us about what step they are in, we should check
			// whether we have received a newer membership callback.
			//
			// If so, we do not send info to peer rgmds.
			// The new lowest nodeid rgmd in the newer
			// reconfiguration would do the step sync.
			// We simply return SYNC_UNSTABLE so that
			// we restart our reconfiguration process.
			//
			rgm_reconfig_lock.lock();
			if (rgm_reconfig_membership_cb_data.seqnum > seqnum) {
				cmm::seqnum_t new_seqnum =
				    rgm_reconfig_membership_cb_data.seqnum;
				rgm_reconfig_lock.unlock();
				ucmm_print("RGM", NOGET("lowest nid saw "
				    "new process membership callback, "
				    "new seqnum = %d\n"), (uint32_t)new_seqnum);
				retval = rgm::SYNC_UNSTABLE;
				break;
			}
			rgm_reconfig_lock.unlock();

			bool do_wait = false;
			alive_rgmds = which_rgmds_are_alive();

			for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
				if (!nodeset_contains(alive_rgmds, nid)) {
					continue;
				}
				if ((peer_rgm_state[nid].seqnum < seqnum) ||
				    ((peer_rgm_state[nid].seqnum == seqnum) &&
				    (peer_rgm_state[nid].step_num
				    != step_num))) {
					//
					// Lowest nodeid rgmd has not heard from
					// at least one rgmd about either :
					// (i) the same seqnum and step_num, or
					// (ii) a newer seqnum
					// So lowest nodeid rgmd waits.
					//
					do_wait = true;
					break;
				}
			}

			if (do_wait) {
				//
				// We wait until one of our peer rgmds
				// sends us information about its state,
				// or till a max wait period of 5 seconds.
				//
				os::systime sys_timeout;
				sys_timeout.setreltime((os::usec_t)
				    max_wait_period_for_step_sync * MICROSEC);
				(void) peer_rgm_state_cv.timedwait(
				    &peer_rgm_state_lock, &sys_timeout);
			} else {
				break;
			}
		}

		//
		// We exit out of the while loop, in two cases :
		//
		// (1) We have heard from every rgmd (retval == SYNC_PROCEED).
		// So we do the following :
		// (i) Check the highest seqnum that any rgmd has heard about.
		// If that seqnum is higher than what we are sync'ing for,
		// then it means newer membership callbacks have happened.
		// If so, we do not send info to peer rgmds. The new lowest
		// nodeid rgmd in the newer reconfiguration would do
		// the step sync. We simply return SYNC_UNSTABLE so that
		// we restart our reconfiguration process. Peer rgmds would
		// in time see the newer membership callback and restart
		// their reconfiguration process.
		// (ii) If all rgmds see the same seqnum,
		// then all rgmds must be ready to do the same step.
		// Send every rgmd this step number and the seqnum and
		// rgm::SYNC_PROCEED, so that everyone executes the step action.
		//
		// (2) We have detected a newer membership callback
		// (retval == SYNC_UNSTABLE).
		// If so, we do not send info to peer rgmds. The new lowest
		// nodeid rgmd in the newer reconfiguration would do
		// the step sync. We simply return SYNC_UNSTABLE so that
		// we restart our reconfiguration process. Peer rgmds would
		// in time see the newer membership callback and restart
		// their reconfiguration process.
		//

		if (retval != rgm::SYNC_UNSTABLE) {
			CL_PANIC(seqnum ==
			    peer_rgm_state[orb_conf::local_nodeid()].seqnum);
			CL_PANIC(step_num ==
			    peer_rgm_state[orb_conf::local_nodeid()].step_num);
			bool same_seqnum_seen = true;
			for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
				if (!nodeset_contains(alive_rgmds, nid)) {
					continue;
				}
				if (peer_rgm_state[nid].seqnum != seqnum) {
					same_seqnum_seen = false;
				}
			}
			if (!same_seqnum_seen) {
				retval = rgm::SYNC_UNSTABLE;
			}
		}

		if (retval == rgm::SYNC_PROCEED) {
			//
			// We have heard from all rgmds. Every rgmd is seeing
			// the same seqnum and is at the same step number.
			//
			lowest_nid_rgmd_sends_info(
			    seqnum, alive_rgmds, step_num, rgm::SYNC_PROCEED);
			peer_rgm_state_lock.unlock();
			ucmm_print("RGM",
			    NOGET("lowest nid returning SYNC_PROCEED\n"));
			return (rgm::SYNC_PROCEED);

		} else {
			//
			// Either we detected a newer membership callback; or
			// we heard from all rgmds, not all of which
			// saw the same seqnum.
			// We do not send info to peer rgmds. The new lowest
			// nodeid rgmd in the newer reconfiguration would do
			// the step sync. We simply return SYNC_UNSTABLE so that
			// we restart our reconfiguration process.
			// Peer rgmds would in time see the newer membership
			// callback and restart their reconfiguration process.
			//
			peer_rgm_state_lock.unlock();
			ucmm_print("RGM",
			    NOGET("lowest nid returning SYNC_UNSTABLE\n"));
			return (rgm::SYNC_UNSTABLE);
		}

	} else {
		while (1) {
			//
			// Before waiting for rgmd on lowest nodeid
			// we ensure that the rgmd is alive.
			// If it dies while we are waiting, then we will
			// wakeup after a max timeout period and recheck anyway.
			//
			rgm::rgm_comm_var lowest_nid_rgm_comm_v =
			    lookup_rgm_comm_ref(lowest_nid);
			if (!CORBA::is_nil(lowest_nid_rgm_comm_v)) {
				//
				// lowest nodeid rgmd could be alive;
				// do a dummy IDL invocation to confirm
				// that the rgmd is really alive.
				//
				Environment env;
				lowest_nid_rgm_comm_v->idl_is_rgm_alive(env);
				if (env.exception() != NULL) {
					env.clear();
					peer_rgm_state_lock.unlock();
					ucmm_print("RGM", NOGET(
					    "lowest nid %d died, "
					    "curr seqnum = %d\n"),
					    lowest_nid, (uint32_t)seqnum);
					return (rgm::SYNC_UNSTABLE);
				}
			} else {
				peer_rgm_state_lock.unlock();
				ucmm_print("RGM", NOGET("could not get "
				    "lowest nid %d comm ref, "
				    "curr seqnum = %d\n"),
				    lowest_nid, (uint32_t)seqnum);
				return (rgm::SYNC_UNSTABLE);
			}

			//
			// Before waiting for rgmd on lowest nodeid in
			// membership to inform us about step synchronization,
			// we should check whether we have received a newer
			// membership callback.
			// If so, we return SYNC_UNSTABLE from this method
			// so that we try starting the next reconfiguration.
			//
			rgm_reconfig_lock.lock();
			if (rgm_reconfig_membership_cb_data.seqnum > seqnum) {
				cmm::seqnum_t new_seqnum =
				    rgm_reconfig_membership_cb_data.seqnum;
				rgm_reconfig_lock.unlock();
				peer_rgm_state_lock.unlock();
				ucmm_print("RGM", NOGET("saw new process "
				    "membership callback, new seqnum = %d\n"),
				    (uint32_t)new_seqnum);
				return (rgm::SYNC_UNSTABLE);
			}
			rgm_reconfig_lock.unlock();

			//
			// If we have heard from lowest nodeid rgmd
			// about this step and seqnum, then do not wait.
			// We also do not wait if we have heard from
			// the lowest nodeid rgmd about a newer seqnum.
			//
			bool do_wait = true;
			if ((lowest_nid_seqnum > seqnum) ||
			    ((lowest_nid_seqnum == seqnum) &&
			    (lowest_nid_step_num == step_num))) {
				do_wait = false;
			}
			if (do_wait) {
				//
				// We wait until the lowest nodeid rgmd
				// we are waiting for notifies us,
				// or till a max wait period of 5 seconds.
				// After wake up, we check whether the lowest
				// nodeid rgmd is still alive; if yes, we wait
				// again, else we return unstable.
				//
				os::systime sys_timeout;
				sys_timeout.setreltime((os::usec_t)
				    max_wait_period_for_step_sync * MICROSEC);
				(void) peer_rgm_state_cv.timedwait(
				    &peer_rgm_state_lock, &sys_timeout);
			} else {
				break;
			}
		}

		//
		// We heard from lowest nodeid rgmd about this step_num
		// and seqnum, or about a newer seqnum
		//
		if (lowest_nid_seqnum > seqnum) {
			// lowest nodeid rgmd has heard about a newer seqnum
			ucmm_print("RGM", NOGET("lowest nid told us that "
			    "it heard about newer seqnum %d, "
			    "returning SYNC_UNSTABLE\n"),
			    (uint32_t)lowest_nid_seqnum);
			peer_rgm_state_lock.unlock();
			return (rgm::SYNC_UNSTABLE);

		} else if (lowest_nid_seqnum < seqnum) {
			//
			// Should not happen, as we have checked for
			// this condition in the while loop above.
			//
			CL_PANIC(0);
		}
		ASSERT(lowest_nid_seqnum == seqnum);
		if (lowest_nid_sync_result == rgm::SYNC_UNSTABLE) {
			//
			// lowest nodeid rgmd sees that
			// there has been a newer seqnum
			//
			peer_rgm_state_lock.unlock();
			ucmm_print("RGM",
			    NOGET("lowest nid told us that there is "
			    "a newer seqnum, returning SYNC_UNSTABLE\n"));
			return (rgm::SYNC_UNSTABLE);
		} else {
			ASSERT(lowest_nid_sync_result == rgm::SYNC_PROCEED);
			if (lowest_nid_step_num != step_num) {
				//
				// Should not happen, as we have checked for
				// this condition in the before.
				//
				CL_PANIC(0);
			}
			peer_rgm_state_lock.unlock();
			ucmm_print("RGM",
			    NOGET("heard from lowest nid to proceed, "
			    "returning SYNC_PROCEED\n"));
			return (rgm::SYNC_PROCEED);
		}
	}
}

void
rgm_reconfig_process_membership_after_upgrade()
{
	ucmm_print("RGM",
	    NOGET("In rgm_reconfig_process_membership_after_upgrade\n"));
	rgm_reconfig_lock.lock();
	rgm_reconfig_cv.signal();
	rgm_reconfig_lock.unlock();
}

static void
rgm_ru_transition()
{
	CL_PANIC(!rgm_reconfig_lock.lock_held());

	ucmm_print("RGM", NOGET("Sleeping in rgm_ru_transition\n"));
	sleep(RGM_RU_TRANSITION_SLEEP_PERIOD);
	ucmm_print("RGM", NOGET("Woke up in rgm_ru_transition\n"));

	rgm_reconfig_lock.lock();
	rgm_ru_transition_in_progress = B_FALSE;
	rgm_reconfig_membership_changed = B_TRUE;
	rgm_reconfig_lock.unlock();
}

static void *
rgm_reconfig_thread(void *)
{
	cmm::seqnum_t rgm_seqnum = 0;
	cmm::membership_t rgm_membership;
	for (nodeid_t nid = 0; nid <= NODEID_MAX; nid++) {
		rgm_membership.members[nid] = INCN_UNKNOWN;
	}

	while (1) {
		rgm_reconfig_lock.lock();
		while (!rgm_reconfig_membership_changed) {
			rgm_reconfig_cv.wait(&rgm_reconfig_lock);
		}

		//
		// We woke up, it means we got a membership callback.
		// Check to see that we are running the version that supports
		// process membership. If so, proceed with the step sync
		// membership reconfiguration.
		//
		if (!process_membership_support_available()) {
			rgm_reconfig_membership_changed = B_FALSE;
			rgm_reconfig_lock.unlock();
			continue;
		}

		if (rgm_ru_transition_in_progress) {
			rgm_reconfig_lock.unlock();
			rgm_ru_transition();
			CL_PANIC(!rgm_reconfig_lock.lock_held());
			continue;
		}

		//
		// Copy out the membership and seqnum and leave lock
		// to allow next membership callbacks to come in
		// while we are reconfiguring.
		//
		cmm::membership_t rgm_membership =
		    rgm_reconfig_membership_cb_data.membership;
		cmm::seqnum_t rgm_seqnum =
		    rgm_reconfig_membership_cb_data.seqnum;
		rgm_reconfig_membership_changed = B_FALSE;
		rgm_reconfig_lock.unlock();

		//
		// If we reach here, we know that RGM is ready to support
		// process membership. This means the rgm_comm references
		// will have been refreshed to use the latest IDL version.
		// So we can use them to do step synchronization.
		//

		// Find lowest nodeid in membership received from callback
		nodeid_t lowest_nid = 0;
		for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
			if (rgm_membership.members[nid] != INCN_UNKNOWN) {
				lowest_nid = nid;
				break;
			}
		}

		if (lowest_nid == 0) {
			//
			// We are reconfiguring for a null membership.
			// So lets pick up the lowest nodeid of the alive
			// rgmds to sync up.
			//
			nodeset_t alive_rgmds = which_rgmds_are_alive();
			for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
				if (nodeset_contains(alive_rgmds, nid)) {
					lowest_nid = nid;
					break;
				}
			}
			ASSERT(lowest_nid != 0);
		}

		uint32_t next_step_num = 1;
		while (next_step_num <= RGM_RECONFIG_MAX_STEP_NUM) {

			bool success = inform_rgm_that_ready_to_do_step(
			    lowest_nid, next_step_num, rgm_seqnum);
			if (!success) {
				//
				// Failed to inform lowest nodeid rgmd.
				// The lowest nodeid rgmd could have died.
				// So check if any new membership callback
				// has arrived :
				// - if yes, break out of the loop and
				// start all over again
				// - if no, try getting the lowest nid
				// out of the alive rgmds again
				//
				rgm_reconfig_lock.lock();
				if (rgm_reconfig_membership_cb_data.seqnum
				    > rgm_seqnum) {
					// new membership callback has happened
					rgm_reconfig_lock.unlock();
					break;
				}
				ASSERT(rgm_reconfig_membership_cb_data.seqnum
				    == rgm_seqnum);
				rgm_reconfig_lock.unlock();
				nodeset_t alive_rgmds = which_rgmds_are_alive();
				for (nodeid_t nid = 1;
				    nid <= NODEID_MAX; nid++) {
					if (nodeset_contains(
					    alive_rgmds, nid)) {
						lowest_nid = nid;
						break;
					}
				}
				ASSERT(lowest_nid != 0);
				continue;
			}

			//
			// Sync with all rgmds so that everyone knows whether
			// to proceed to do the step action, or whether
			// to restart the reconfiguration.
			//
			rgm::sync_result_t result =
			    step_sync_for_rgm(lowest_nid,
			    next_step_num, rgm_seqnum, rgm_membership);
			if (result == rgm::SYNC_UNSTABLE) {
				rgm_reconfig_lock.lock();
				rgm_reconfig_membership_changed = B_TRUE;
				rgm_reconfig_lock.unlock();
				break;
			} else {
				if (result != rgm::SYNC_PROCEED) {
					CL_PANIC(0);
				}
			}

			// Do the action of the step
			rgm_reconfig_step_handler_t handler =
			    rgm_reconfig_steps[next_step_num];
			bool retval = (*handler)(rgm_membership, rgm_seqnum);

			if (!retval) {
				//
				// XXX Do not proceed.
				// XXX Wait for next membership callback.
				//
				break;
			}
			next_step_num++;
		}
	}
	return (NULL);
}

static void
start_rgm_reconfig_thread()
{
	if (thr_create(NULL, 0, rgm_reconfig_thread,
	    NULL, THR_BOUND, NULL) != 0) {
		//
		// SCMSGS
		// @explanation
		// Could not start RGM reconfiguration thread.
		// This may be due to inadequate memory on the system.
		// @user_action
		// Add more memory to the system. If that does not resolve the
		// problem, contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: could not start rgm reconfig thread"));
		abort();
	}
}

class rgm_clust_dir_cb_impl : public McServerof<ccr::callback> {
public:
	void did_update_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void did_update(const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery(const char *, ccr::ccr_update_type,
	    Environment &);
	void _unreferenced(unref_t);
};

void
rgm_clust_dir_cb_impl::did_update_zc(const char *, const char *,
    ccr::ccr_update_type type, Environment &)
{
}

void
rgm_clust_dir_cb_impl::did_update(const char *cluster,
    ccr::ccr_update_type type,
    Environment &)
{
	switch (type) {
	case CCR_TABLE_ADDED:
		return;
	case CCR_TABLE_REMOVED:
		if (strcmp(cluster, ZONE) != 0) {
			return;
		}

		debuglog(LOG_ERR, "zone cluster %s was removed", cluster);
		failfast_disarm();
		_exit(1);
		break;
	case CCR_TABLE_MODIFIED:
		return;
	CCR_RECOVERED:
	CCR_INV_UPDATE:
		ASSERT(0);
		return;
	}
}

void
rgm_clust_dir_cb_impl::did_recovery_zc(const char *, const char *,
    ccr::ccr_update_type, Environment &)
{
}

void
rgm_clust_dir_cb_impl::did_recovery(const char *, ccr::ccr_update_type,
    Environment &)
{
}

static void
rgm_clust_dir_cb_impl::_unreferenced(unref_t)
{
	delete this;
}

static void rgm_register_cluster_dir_callbacks() {
	Environment e;
	rgm_clust_dir_cb_impl *infcb;
	ccr::callback_var cbptr;
	static ccr::directory_var ccr_directory_var = ccr::directory::_nil();
	infcb = new rgm_clust_dir_cb_impl;
	cbptr = infcb->get_objref();
	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	ccr_directory_var = ccr::directory::_narrow(obj);
	CL_PANIC(!CORBA::is_nil(ccr_directory_var));

	ccr_directory_var->register_cluster_callback(cbptr, e);
	if (e.exception()) {
		debuglog(LOG_ERR, "fatal: couldn't register for clusters."
		    " abort");
		abort();
		// UNREACHABLE
	}
}

#endif	// (SOL_VERSION >= __s10)
