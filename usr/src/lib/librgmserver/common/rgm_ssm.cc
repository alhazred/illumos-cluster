//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_ssm.cc	1.31	08/05/20 SMI"

//
// rgm_ssm.cc
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/cladm.h>
#include <orb/invo/common.h>
#include <h/sol.h>
#include <rgm/rgm_comm_impl.h>
#include <rgm_proto.h>
#include <scha_err.h>
#include <rgm_api.h>
#include <sys/ssm.h>
#include <scha_ssm.h>
#include <arpa/inet.h>
#include <scadmin/scconf.h>
#include <netdb.h>


static int get_ext_res_int(get_ext_result_t **get_ext_result,
    char *rs_name, char *rg_name, char *name,
    int type, ssm_result_t *result);

static unsigned int hostip_string_to_binary(in6_addr_t *, char *hostorip);

static int rgm_ssm_ip_op_int(in6_addr_t *in_list_val, unsigned int in_list_len,
    unsigned char protocol, nodeid_t nodeid);


#define	SCDS_EXTPROP_HOSTNAMELIST	"HostnameList"


//
// rgm_ssm_ip_op
//
// Called by scha_ssm_ip_1_svc() in the receptionist.
//
// Via the prenet_start method, hascip calls the private API routine
// scha_ssm_address_ip(), which calls into the RGM receptionist.  The
// receptionist calls rgm_ssm_ip_op() to attach all ip addresses to the
// node.  Nobody can be modifying resources and resource groups while
// state changes of said resources and resource groups are in progress;
// therefore, there is no need to check sequence ids here.
//

void
rgm_ssm_ip_op(ssm_ip_args *args, ssm_result_t *result)
{
	get_ext_result_t *get_ext_result = NULL;
	in6_addr_t *in_list_val = NULL;
	unsigned int in_list_len;
	unsigned int i;
	ssm_result_t iresult;
	unsigned int vi = 0, cnt = 0;

	if (get_ext_res_int(&get_ext_result, args->rs_name, args->rg_name,
	    SCDS_EXTPROP_HOSTNAMELIST, SCHA_PTYPE_STRINGARRAY, &iresult) != 0) {
		result->ret_code = iresult.ret_code;
		goto cleanup;
	}

	//
	// A hostname can have both a V4 and a V6 IP address mapping.
	// We allocate enough storage in case all hostnames have 2
	// mappings.
	//
	in_list_len = get_ext_result->value_list->strarr_list_len;
	in_list_val = (in6_addr_t *)malloc_nocheck(2 * in_list_len *
	    sizeof (in6_addr_t));
	if (in_list_val == NULL) {
		result->ret_code = SCHA_SSM_NO_MEMORY;
		goto cleanup;
	}

	for (vi = 0, i = 0; i < in_list_len; i++, vi += cnt) {
		if ((cnt = hostip_string_to_binary(&in_list_val[vi],
		    get_ext_result->value_list->strarr_list_val[i])) == 0)
			goto cleanup;
	}

	result->ret_code = rgm_ssm_ip_op_int(in_list_val, vi,
	    (unsigned char)args->protocol, (nodeid_t)args->nodeid);

cleanup:
	if (in_list_val != NULL)
		free(in_list_val);
	if (get_ext_result != NULL) {
		xdr_free((xdrproc_t)xdr_get_ext_result_t,
		    (char *)get_ext_result);
		free(get_ext_result);
	}
}


//
// Called only by rgm_ssm_ip_op() in this module.
//

int
rgm_ssm_ip_op_int(in6_addr_t *in_list_val, unsigned int in_list_len,
    unsigned char protocol, nodeid_t nodeid)
{
	unsigned int i;
	scha_ssm_exceptions rc;
	ssm_service_t sap;
	int result = SCHA_SSM_SUCCESS;

	sap.port = 0;
	sap.protocol = protocol;

	/* Get args for resource */

	for (i = 0; i < in_list_len; i++) {
		sap.ipaddr = in_list_val[i];
		rc = ssm_set_primary_gifnode(&sap, (nodeid_t)nodeid);
		if (rc == SCHA_SSM_NO_SERVICE) {
			// Ignore
		} else if (rc != SCHA_SSM_SUCCESS) {
			// Log error and continue
			result = rc;
		}
	}
	return (result);
}


// Get an ext property
// Args:
//	get_ext_result: malloc'd buffer
//	args: input args
//	name:	property name
//	type:	desired type
//	result:	return result
// Return: 1 for error
//
// Called only by rgm_ssm_ip_op() in this module.
//

int
get_ext_res_int(get_ext_result_t **get_ext_result, char *rs_name,
    char *rg_name, char *name, int type, ssm_result_t *result)
{
	rs_get_args rs_args = {0};

	result->ret_code = SCHA_SSM_SUCCESS;

	// Cleanup any old value
	if (*get_ext_result != NULL) {
		xdr_free((xdrproc_t)xdr_get_ext_result_t,
		    (char *)*get_ext_result);
		free(*get_ext_result);
	}

	*get_ext_result = (get_ext_result_t *)calloc_nocheck(1,
	    sizeof (get_ext_result_t));
	if (*get_ext_result == NULL) {
		result->ret_code = SCHA_SSM_NO_MEMORY;
		goto cleanup;
	}

	rs_args.rs_name = rs_name;
	rs_args.rg_name = rg_name;
	rs_args.rs_prop_name = name;
	get_res_ext(&rs_args, *get_ext_result);

	if ((*get_ext_result)->ret_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM", NOGET("ret code from get_rg is %d\n"),
		    (*get_ext_result)->ret_code);
		result->ret_code = SCHA_SSM_INTERNAL_ERROR;
		goto cleanup;
	}

	if ((*get_ext_result)->value_list == NULL) {
		ucmm_print("RGM", NOGET("no rgnlist from get_rg\n"));
		result->ret_code = SCHA_SSM_INTERNAL_ERROR;
		goto cleanup;
	}
	if ((*get_ext_result)->prop_type != type) {
		ucmm_print("RGM", NOGET("wrong type %d vs %d for %s\n"),
		    (*get_ext_result)->prop_type, type, name);
		result->ret_code = SCHA_SSM_INTERNAL_ERROR;
		goto cleanup;
	}
	if (type == SCHA_PTYPE_INT || type == SCHA_PTYPE_STRING) {
		if ((*get_ext_result)->value_list->strarr_list_len != 1) {
			ucmm_print("RGM", NOGET("wrong number params for %s\n"),
			    name);
			result->ret_code = SCHA_SSM_INTERNAL_ERROR;
		}
	}

cleanup:

	if (result->ret_code != SCHA_SSM_SUCCESS) {
		xdr_free((xdrproc_t)xdr_get_ext_result_t,
		    (char *)*get_ext_result);
		free(*get_ext_result);
		*get_ext_result = NULL;
		return (1);
	} else {
		return (0);
	}
}

//
// hostip_string_to_binary()
// Convert a hostname or a string respresentation of an IP address into
// binary form (in6_addr_t).
// If the hostname maps to an IPv6 address, the binary form of the
// address is returned in 'ip[0]'. 'ip[1]' will be zero'ed out.
// If the hostname maps to an IPv4 address, the V4-mapped version of the
// address is returned in 'ip[0]'. 'ip[1]' will be zero'ed out. The
// v4-mapped address is expected by ssm.h for handling of V4 addresses.
// If the hostname maps to both V4 and V6, then both are returned (V4 as
// V4-mapped). The order in 'ip' is not definite.
//
// 'ip' must point to an array of at least 2 in6_addr_t's.
// Note: An in6_addr_t of all zero's is a valid IPv6 address -- the V6
// unspecified address. Thus, a hostname that maps to that
// address won't be supported properly. (V4 INADDR_ANY will be fine
// though, since it'll be V4-mapped, and so it won't be all zero's.
//
// Returns number of mappings found; 0 if none.
//
unsigned int
hostip_string_to_binary(in6_addr_t *ip, char *hostorip)
{
	struct hostent	*hp = NULL;
	int		i, rc;
	in6_addr_t	cur;
	unsigned short	ip_cnt = 1;

	if (hostorip == NULL || ip == NULL) {
		return (0);
	}

	//
	// Convert address to binary form. This call will succeed even if
	// hostorip is an IP address in string form instead of hostname.
	// getipnodebyname() is used to support both V4 and V6 addresses.
	//
	hp = getipnodebyname(hostorip, AF_INET6, AI_DEFAULT|AI_ALL, &rc);
	if (hp == NULL || hp->h_addr_list[0] == NULL) {
		if (hp) {
			freehostent(hp);
		}
		return (0);
	}

	(void) memcpy(&ip[0], hp->h_addr_list[0], sizeof (in6_addr_t));
	(void) memset(&ip[1], 0, sizeof (in6_addr_t));

	for (i = 1; hp->h_addr_list[i] != NULL; i++) {
		//
		// Need to memcpy the address into a in6_addr_t structure,
		// since h_addr_list[] aren't always aligned properly.
		//
		(void) memcpy(&cur, hp->h_addr_list[i], sizeof (in6_addr_t));

		if (IN6_IS_ADDR_V4MAPPED(ip)) {
			// Looking for the first V6 address
			if (!IN6_IS_ADDR_V4MAPPED(&cur)) {
				ip[ip_cnt++] = cur;
				break;
			}
		} else {
			// Looking for the first V4 address
			if (IN6_IS_ADDR_V4MAPPED(&cur)) {
				ip[ip_cnt++] = cur;
				break;
			}
		}
	}
	freehostent(hp);
	return (ip_cnt);
}
