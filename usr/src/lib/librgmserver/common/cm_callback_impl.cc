//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma	ident	"@(#)cm_callback_impl.cc	1.40	08/06/23 SMI"

#include <h/sol.h>
#include <cm_callback_impl.h>
#include <rgm_proto.h>
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>


cm_callback_impl *cm_callback_impl::the_cm_callback_implp = NULL;

// ucmm cluster membership: set in set_reconf_data(); passed to rgm_reconfig()
static nodeset_t ucmm_nodeset = 0;

cm_callback_impl::cm_callback_impl()
{
	if (the_cm_callback_implp != NULL) {
		os::panic(NOGET("the_cm_callback_impl already allocated"));
	}
	the_cm_callback_implp = this;
}

cm_callback_impl::~cm_callback_impl()
{
}

cm_callback_impl *
cm_callback_impl::thep()
{
	return (the_cm_callback_implp);
}


// cm_callback_impl(cmm::callback::start_trans)
void
cm_callback_impl::start_trans(Environment&)
{
	ucmm_print("RGM",
	    NOGET("node %d: cm_callback_impl start transition\n"),
	    orb_conf::local_nodeid());
}


// + cm_callback_impl(cmm::callback::set_reconf_data)
//
// Part of the CMM callback interface.  Called by the CMM automaton
// before step1 of the CMM reconfiguration with the current membership
// and reconfiguration sequence number.
//
void
cm_callback_impl::set_reconf_data(
    cmm::seqnum_t sn,
    const cmm::membership_t &membership,
    Environment &)
{
	// step_trans() will pass this to rgm_reconfig()
	ucmm_nodeset = 0;

	for (sol::nodeid_t i = 1; i <= MAXNODES; i++) {
		if (membership.members[i] != INCN_UNKNOWN)
			nodeset_add(ucmm_nodeset, i);
	}

	//
	// The local node must be in the membership.
	//
	CL_PANIC(nodeset_contains(ucmm_nodeset, orb_conf::local_nodeid()));

	if (use_ucmm_membership()) {
		rgm_lock_state();
		Rgm_state->rgm_membership_generation = sn;
		Rgm_state->node_incarnations = membership;
		rgm_unlock_state();
	}

	ucmm_print("RGM",
	    NOGET("node %d: cm_callback_impl set_reconf_data sn:%d, "
	    "nodeset:%lld\n"),
	    orb_conf::local_nodeid(), (uint32_t)sn, ucmm_nodeset);
}


//
// + cm_callback_impl(cmm::callback::step_trans)
//
// rgmd uses two reconfiguration steps.
//
// step1 makes sure that all nodes in the cluster get the latest ucmm
// membership and the latest incarnation numbers. The latest incarnation
// number is used to check if IDL calls from another node are stale or
// not. No real rgmd work is done in this step.
//
// step2 invokes the big rgmd reconfiguration step. When step2 starts
// on any node, the ucmm guarantees that all nodes in the cluster have
// completed step1. In step2, a node can become the President or take
// over from a dead President knowing that all nodes in the cluster
// know of the latest membership and incarnation numbers.
//
void
cm_callback_impl::step_trans(
    uint32_t step_number,
    cmm::timeout_t tmout,
    Environment&)
{
	ucmm_print("RGM",
	    NOGET("node %d: cm_callback_impl step transition %d tm:%ld\n"),
	    orb_conf::local_nodeid(), step_number, tmout);

	if (step_number == 1)
		return; // no work to do
	else
		CL_PANIC(step_number == 2); // rgmd uses only 2 steps

	// execute RGM's big, uninterruptible reconfiguration step
	// europa support: the second parameter means that the reconfiguration
	// is triggered by the Server.
	if (use_ucmm_membership()) {
		rgm_reconfig((LogicalNodeset)ucmm_nodeset, B_TRUE);
	}

	ucmm_print("RGM", NOGET("node %d: cm_callback_impl step %d done\n"),
	    orb_conf::local_nodeid(), step_number);
}

// cm_callback_impl(cmm::callback::return_trans)
void
cm_callback_impl::return_trans(cmm::timeout_t tmout, Environment&)
{
	ucmm_print("RGM",
	    NOGET("node %d: cm_callback_impl return transition tm:%d\n"),
	    orb_conf::local_nodeid(), tmout);
}

// cm_callback_impl(cmm::callback::stop_trans)
void cm_callback_impl::stop_trans(cmm::timeout_t tmout, Environment&)
{
	ucmm_print("RGM",
	    NOGET("node %d: cm_callback_impl stop transition tm:%d\n"),
	    orb_conf::local_nodeid(), tmout);
}

// cm_callback_impl(cmm::callback::abort_trans)
void cm_callback_impl::abort_trans(cmm::timeout_t tmout, Environment&)
{
	sc_syslog_msg_handle_t handle;

	ucmm_print("RGM",
	    NOGET("node %d: cm_callback_impl abort transition tm:%d\n"),
	    orb_conf::local_nodeid(), tmout);

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");
	//
	// SCMSGS
	// @explanation
	// A fatal error has occurred in the rgmd. The rgmd will produce a
	// core file and will force the node to halt or reboot to avoid the
	// possibility of data corruption.
	// @user_action
	// Save a copy of the /var/adm/messages files on all nodes, and of the
	// rgmd core file. Contact your authorized Sun service provider for
	// assistance in diagnosing the problem.
	//
	(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
	    SYSTEXT("RGM aborting"));
	sc_syslog_msg_done(&handle);
	//
	// We need to exit here. Failfast will note that the daemon died after
	// the failfast timeout and will abort the node.
	// We will call abort() here to generate a coredump and avoid ORB
	// cleanup which exit() touches off and which hits assert failures
	// which might muddy the waters when trying to debug the cause of
	// this abort_trans callback.
	//
	abort();
}

//
// Function that can be called by the CMM automaton to signal that the current
// step needs to be terminated. This is invoked when a new reconfiguration
// needs to be started due to the failure of a node while the current
// reconfiguration is in progress.
//
// For the userland CMM, this is a no-op.
//
void cm_callback_impl::terminate_step(Environment &e)
{
	ucmm_print("RGM",
	    NOGET("cm_callback_impl::terminate_step: deprecated method "
	    "called.\n"));
	e.system_exception(CORBA::VERSION(1, CORBA::COMPLETED_YES));
}

void cm_callback_impl::terminate_step_this_seqnum(cmm::seqnum_t, Environment &)
{
}
