//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_event_gen.cc	1.14	08/05/27 SMI"

//
// rgm_event_gen.cc
//
// RGM Server side functions to post Solaris
// sysevents about state changes.
//

#include <rgm/rgm_comm_impl.h>
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#include <rgm_proto.h>

#include <sys/cl_events.h>

/* Zone support */
#include <rgm_logical_nodeset.h>
#include <rgm_logical_node.h>
#include <rgm_logical_node_manager.h>


static cl_event_severity_t get_rs_severity(rstatename_t *rstate);
static cl_event_severity_t get_rg_severity(rgstatename_t *rgstate);
static cl_event_initiator_t get_initiator(cl_event_reason_code_t reason);

//
// Generate a sysevent with the current
// cluster membership information.
//
scha_errmsg_t
post_event_membership(LogicalNodeset &prev_ns, LogicalNodeset &current_ns)
{
	scha_errmsg_t ev_res = {SCHA_ERR_NOERR, NULL};
	char	*nlist = NULL, *nodename = NULL;
	char	*slist = NULL, state[32];
	char	*newptr = NULL;
	size_t	newlen, oldlen;
	unsigned int	nodeid, l;
	cl_event_severity_t severity = CL_EVENT_SEV_INFO;
	int err;

	ucmm_print("RGM",
		NOGET("post_event_membership() called\n"));

	//
	// Look thru RGM's notion of the current membership
	// and create two strings, containing nodenames
	// and nodestates (nodestates are essentially
	// incarnation numbers, -1 for dead nodes, positive
	// for nodes which are part of the cluster).
	//
	nodeid = 0;
	LogicalNodeset conf_ns(Rgm_state->static_membership);
	while ((nodeid = conf_ns.nextPhysNodeSet(nodeid + 1)) != 0) {

		nodename = rgm_get_nodename(nodeid);

		// If the nodeid is in the previous nodeset and not in
		// current nodeset set the severity to WARNING.

		if (severity != CL_EVENT_SEV_WARNING) {

			if (prev_ns.containLni(nodeid) &&
				!current_ns.containLni(nodeid)) {

				severity = CL_EVENT_SEV_WARNING;
			}
		}

		l = (unsigned int)sprintf(state, "%d",
			Rgm_state->node_incarnations.members[nodeid]);

		oldlen = 0;
		newlen = l + 1 + 1;	// End NULL and comma
		if (slist) {
			(void) strcat(slist, ",");
			oldlen = strlen(slist) + 1;
			newlen += oldlen;
		}

		newptr = (char *)realloc(slist, newlen);
		if (newptr == NULL) {
			ev_res.err_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}

		bzero(&newptr[oldlen], newlen - oldlen);
		slist = newptr;
		newptr = NULL;

		(void) strcat(slist, state);

		oldlen = 0;
		newlen = strlen(nodename) + 1 + 1;
		if (nlist) {
			(void) strcat(nlist, ",");
			oldlen = strlen(nlist) + 1;
			newlen += oldlen;
		}
		newptr = (char *)realloc(nlist, newlen);

		if (newptr == NULL) {
			ev_res.err_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}

		bzero(&newptr[oldlen], newlen - oldlen);
		nlist = newptr;
		newptr = NULL;

		(void) strcat(nlist, nodename);
		free(nodename);
		nodename = NULL;
	}



#if SOL_VERSION >= __s10
	if ((err = sc_publish_zc_event(ZONE, ESC_CLUSTER_MEMBERSHIP,
		CL_EVENT_PUB_RGM, severity,
		CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		CL_NODE_LIST, SE_DATA_TYPE_STRING, nlist,
		CL_NODE_STATE_LIST, SE_DATA_TYPE_STRING, slist, NULL)) != 0) {
		ev_res.err_code = SCHA_ERR_INTERNAL;
		rgm_format_errmsg(&ev_res,
		    NOGET("sc_publish_zc_event() "
			"failed to publish event for nlist=%s slist=%s: "
			"error %d\n"), nlist, slist, err);
		goto finished; //lint !e801
	}

#else
	if ((err = sc_publish_event(ESC_CLUSTER_MEMBERSHIP,
		CL_EVENT_PUB_RGM, severity,
		CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		CL_NODE_LIST, SE_DATA_TYPE_STRING, nlist,
		CL_NODE_STATE_LIST, SE_DATA_TYPE_STRING, slist, NULL)) != 0) {
		ev_res.err_code = SCHA_ERR_INTERNAL;
		rgm_format_errmsg(&ev_res,
		    NOGET("sc_publish_event() "
			"failed to publish event for nlist=%s slist=%s: "
			"error %d\n"), nlist, slist, err);
		goto finished; //lint !e801
	}

#endif


#if SOL_VERSION >= __s10
	ucmm_print("RGM",
	    NOGET("sc_publish_zc_event() "
		"published event: returning\n"));
#else
	ucmm_print("RGM",
	    NOGET("sc_publish_event() "
		"published event: returning\n"));
#endif

finished:

	if (nodename)
		free(nodename);
	//
	// Lint warning 644 says checking variable
	// (e.g. nlist, slist) which may not have
	// been initialized yet. The point is that these
	// are initialized after allocating new memory
	// which may or may-not happen depending on whether
	// or not the code above enters the for() loops.
	// Lint shouldnt be complaining that it is
	// not initialized since we are setting them to
	// NULL in the beginning. It is all right to suppress
	// these.
	//

	if (nlist)	//lint !e644
		free(nlist);
	if (slist)	//lint !e644
		free(slist);

	return (ev_res);
}

//
// post_event_rg_state()
// Generate a sysevent with the current
// state of the specified RG.
// If the RG is not specified (rg_name is NULL)
// post one event for EACH RG in the system.
// Must be called with the global RGM mutex held.
//
// Should be called only on the president. Is a no-op on slave nodes.
//
scha_errmsg_t
post_event_rg_state(const char *rg_name)
{
	scha_errmsg_t ev_res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t  rg_ptr = NULL;
	nodeidlist_t	*nl;
	char	*nlist = NULL;
	char	*slist = NULL;
	char	*newptr = NULL;
	const char *temp_rg_name = NULL;
	rgstatename_t   *rgstate = NULL;
	const char *rgstate_string = NULL;
	size_t	newlen, oldlen;
	cl_event_severity_t	severity = CL_EVENT_SEV_INFO;
	cl_event_severity_t	curnt_severity = CL_EVENT_SEV_INFO;
	int err;
	boolean_t found_match = B_FALSE;
	const char *name;
	sol::nodeid_t current_nodeid = orb_conf::local_nodeid();

	if (!am_i_president()) {
		ucmm_print("RGM", NOGET("Not running post_event_rg_state() "
		    "because I am not president.\n"));
		return (ev_res);
	}
	ucmm_print("RGM",
		NOGET("post_event_rg_state() "
		"called for RG <%s>\n"),
		(rg_name == NULL ? "NULL" : rg_name));

	// Go over the list of ALL RGs
	for (rg_ptr = Rgm_state->rgm_rglist; rg_ptr != NULL;
		rg_ptr = rg_ptr->rgl_next) {

		// Look for the specific RG, if specified
		if (rg_name != NULL) {
			if (strcmp(rg_ptr->rgl_ccr->rg_name, rg_name) != 0) {
				continue;
			}
			temp_rg_name = rg_name;
			found_match = B_TRUE;
		} else {
			temp_rg_name = rg_ptr->rgl_ccr->rg_name;
		}

		//
		// Go over RG's nodelist and prepare a comma
		// separated list of nodenames and RG state
		// for each node.
		//
		for (nl = rg_ptr->rgl_ccr->rg_nodelist; nl != NULL;
			nl = nl->nl_next) {

			// If the RG is unmanaged, we need to return
			// "unmanaged" as the state.
			if (rg_ptr->rgl_ccr->rg_unmanaged) {
				rgstate_string = SCHA_UNMANAGED;
			} else {
				rgstate = get_rgstate(rg_ptr->rgl_xstate[
				    nl->nl_lni].rgx_state,
				    NULL, (scha_rgstate_t)-1, rg_ptr,
				    nl->nl_lni);

				if (rgstate == NULL) {
					ev_res.err_code = SCHA_ERR_INTERNAL;
					goto finished; //lint !e801
				}

				// save out the portion of the state
				// structure that we need
				rgstate_string = rgstate->api_statename;

				//
				// Get the severity of the RG state. If the
				// severity is CL_EVENT_SEV_CRITICAL then check
				// if it is on the current_nodeid. If the
				// critical state is not on the current node
				// then set the state to CL_EVENT_SEV_WARNING
				// because the critical RG states on other
				// nodes would have been published already.
				//
				// Note that we only check the severity if
				// we're not in UNMANAGED state
				// (get_rg_severity does not handle the
				// UNMANAGED state).
				//
				if (severity != CL_EVENT_SEV_CRITICAL) {

					curnt_severity = get_rg_severity(
					    rgstate);

					if (curnt_severity > severity)
						severity = curnt_severity;

					if (severity ==
					    CL_EVENT_SEV_CRITICAL &&
					    nl->nl_nodeid != current_nodeid)
						severity = CL_EVENT_SEV_WARNING;

				}
			}

			oldlen = 0;

			LogicalNode *lnNode =
			    lnManager->findObject(nl->nl_lni);
			name = lnNode->getFullName();

			newlen = strlen(name) + 1 + 1;
			if (nlist) {
				(void) strcat(nlist, ",");
				oldlen = strlen(nlist) + 1;
				newlen += oldlen;
			}
			newptr = (char *)realloc(nlist, newlen);
			if (newptr == NULL) {
				ev_res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}
			bzero(&newptr[oldlen], newlen - oldlen);
			nlist = newptr;
			newptr = NULL;

			(void) strcat(nlist, name);
			name = NULL;

			oldlen = 0;
			newlen = strlen(rgstate_string) + 1 + 1;
			if (slist) {
				(void) strcat(slist, ",");
				oldlen = strlen(slist) + 1;
				newlen += oldlen;
			}
			newptr = (char *)realloc(slist, newlen);
			if (newptr == NULL) {
				ev_res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}
			bzero(&newptr[oldlen], newlen - oldlen);
			slist = newptr;
			newptr = NULL;

			(void) strcat(slist, rgstate_string);
		}

		// Legal for RG Nodelist to be empty
		if ((nlist == NULL) && (slist == NULL)) {
			nlist = strdup_nocheck("");
			slist = strdup_nocheck("");
		}

		if ((nlist == NULL) || (slist == NULL)) {
			ev_res.err_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}

		// Post the event

#if SOL_VERSION >= __s10

		if ((err = sc_publish_zc_event(ZONE, ESC_CLUSTER_RG_STATE,
			CL_EVENT_PUB_RGM, severity,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_RG_NAME, SE_DATA_TYPE_STRING, temp_rg_name,
			CL_NODE_LIST, SE_DATA_TYPE_STRING, nlist,
			CL_NODE_STATE_LIST, SE_DATA_TYPE_STRING,
			slist, NULL)) != 0) {
			ev_res.err_code = SCHA_ERR_INTERNAL;

			rgm_format_errmsg(&ev_res,
			    NOGET("Failed to publish event for rg_name=%s "
			    "nlist=%s slist=%s: error %d\n"), temp_rg_name,
			    nlist, slist, err);
			goto finished; //lint !e801
		}
#else

		if ((err = sc_publish_event(ESC_CLUSTER_RG_STATE,
			CL_EVENT_PUB_RGM, severity,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_RG_NAME, SE_DATA_TYPE_STRING, temp_rg_name,
			CL_NODE_LIST, SE_DATA_TYPE_STRING, nlist,
			CL_NODE_STATE_LIST, SE_DATA_TYPE_STRING,
			slist, NULL)) != 0) {
			ev_res.err_code = SCHA_ERR_INTERNAL;

			rgm_format_errmsg(&ev_res,
			    NOGET("Failed to publish event for rg_name=%s "
			    "nlist=%s slist=%s: error %d\n"), temp_rg_name,
			    nlist, slist, err);
			goto finished; //lint !e801
		}
#endif
		free(nlist);
		nlist = NULL;
		free(slist);
		slist = NULL;
	}

	// If RGname is invalid, log error and return error
	if (rg_name != NULL && !found_match) {
		ev_res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&ev_res, NOGET(
		    "Invalid RG <%s> when trying to post event\n"), rg_name);
		goto finished; //lint !e801
	}
	ucmm_print("RGM",
	    NOGET("post_event_rg_state() "
		"posted event: returning\n"));

finished:

	//
	// Lint warning 644 says checking variable
	// (e.g. nlist, slist) which may not have
	// been initialized yet. The point is that these
	// are initialized after allocating new memory
	// which may or may-not happen depending on whether
	// or not the code above enters the for() loops.
	// Lint shouldnt be complaining that it is
	// not initialized since we are setting them to
	// NULL in the beginning. It is all right to suppress
	// these.
	//

	if (nlist)		//lint !e644
		free(nlist);
	if (slist)		//lint !e644
		free(slist);

	return (ev_res);
}

//
// Generate a sysevent with the current
// state of the specified R.
// If the R name is not specified (is NULL)
// generate an event for EACH R in the system.
//
// Should be called only on the president. Is a no-op if called on a slave.
//
scha_errmsg_t
post_event_rs_state(const char *r_name)
{
	scha_errmsg_t ev_res = {SCHA_ERR_NOERR, NULL};
	rlist_p_t  r_ptr = NULL;
	rglist_p_t  rg_ptr = NULL;
	nodeidlist_t	*nl;
	char	*nlist = NULL;
	char	*slist = NULL;
	char	*newptr = NULL;
	char	*rg_name = NULL;
	const char *temp_r_name = NULL;
	rstatename_t   *rstate = NULL;
	size_t	newlen, oldlen;
	cl_event_severity_t	severity = CL_EVENT_SEV_INFO;
	cl_event_severity_t	curnt_severity = CL_EVENT_SEV_INFO;
	int err;
	const char *name;
	sol::nodeid_t current_nodeid = orb_conf::local_nodeid();

	if (!am_i_president()) {
		ucmm_print("RGM", NOGET("Not running post_event_rs_state() "
		    "because I am not president.\n"));
		return (ev_res);
	}

	ucmm_print("RGM",
		NOGET("post_event_rs_state() "
		"called for r <%s>\n"),
		(r_name == NULL ? "NULL" : r_name));

	// Go over the list of ALL RGs
	for (rg_ptr = Rgm_state->rgm_rglist; rg_ptr != NULL;
		rg_ptr = rg_ptr->rgl_next) {

		// Go over all Rs in the RG
		for (r_ptr = rg_ptr->rgl_resources; r_ptr != NULL;
			r_ptr = r_ptr->rl_next) {

			// If R name is specified, look for it
			if (r_name) {
				if (strcmp(r_name, r_ptr->rl_ccrdata->r_name)
					!= 0) {
					continue;
				}
				temp_r_name = r_name;
			} else {
				temp_r_name = r_ptr->rl_ccrdata->r_name;
			}
			rg_name = rg_ptr->rgl_ccr->rg_name;

			for (nl = rg_ptr->rgl_ccr->rg_nodelist; nl != NULL;
				nl = nl->nl_next) {

				rstate = get_rstate(r_ptr->rl_xstate[
				    nl->nl_lni].rx_state, NULL,
				    (scha_rsstate_t)-1);

				if (rstate == NULL) {
					ev_res.err_code = SCHA_ERR_INTERNAL;
					goto finished; //lint !e801
				}

				// Get the severity of the RS state if the
				// severity is not already
				// CL_EVENT_SEV_CRITICAL. If the RS state is
				// CL_EVENT_SEV_CRITICAL then check if it is
				// on the current_nodeid. If the critical
				// state is not on the current node then
				// set the state to CL_EVENT_SEV_WARNING because
				// the critical RS states on other nodes would
				// have been posted already. The set_rx_state()
				// is called per nodeid.

				if (severity != CL_EVENT_SEV_CRITICAL) {

					curnt_severity =
						get_rs_severity(rstate);

					if (curnt_severity > severity)
						severity = curnt_severity;

					if (severity == CL_EVENT_SEV_CRITICAL &&
						nl->nl_nodeid != current_nodeid)
						severity = CL_EVENT_SEV_WARNING;
				}

				oldlen = 0;
				LogicalNode *lnNode =
				    lnManager->findObject(nl->nl_lni);
				name = lnNode->getFullName();
				newlen = strlen(name) + 1 + 1;
				if (nlist) {
					(void) strcat(nlist, ",");
					oldlen = strlen(nlist) + 1;
					newlen += oldlen;
				}
				newptr = (char *)realloc(nlist, newlen);
				if (newptr == NULL) {
					ev_res.err_code = SCHA_ERR_NOMEM;
					goto finished; //lint !e801
				}
				bzero(&newptr[oldlen], newlen - oldlen);
				nlist = newptr;
				newptr = NULL;

				(void) strcat(nlist, name);
				name = NULL;

				oldlen = 0;
				newlen = strlen(rstate->api_statename) + 1 + 1;
				if (slist) {
					(void) strcat(slist, ",");
					oldlen = strlen(slist) + 1;
					newlen += oldlen;
				}
				newptr = (char *)realloc(slist, newlen);
				if (newptr == NULL) {
					ev_res.err_code = SCHA_ERR_NOMEM;
					goto finished; //lint !e801
				}
				bzero(&newptr[oldlen], newlen - oldlen);
				slist = newptr;
				newptr = NULL;

				(void) strcat(slist, rstate->api_statename);
			}	// For each node in RG Nodelist

			// Legal for RG Nodelist to be empty?
			if ((nlist == NULL) && (slist == NULL)) {
				nlist = strdup_nocheck("");
				slist = strdup_nocheck("");
			}

			if ((nlist == NULL) || (slist == NULL)) {
				ev_res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

#if SOL_VERSION >= __s10

			if ((err = sc_publish_zc_event(ZONE,
			    ESC_CLUSTER_R_STATE,
			    CL_EVENT_PUB_RGM, severity,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_R_NAME, SE_DATA_TYPE_STRING, temp_r_name,
			    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
			    CL_NODE_LIST, SE_DATA_TYPE_STRING, nlist,
			    CL_NODE_STATE_LIST, SE_DATA_TYPE_STRING,
			    slist, NULL)) != 0) {
				rgm_format_errmsg(&ev_res,
				    NOGET("sc_publish_zc_event() "
				    "failed to publish event for rg_name=%s "
				    "r_name=%s nlist=%s slist=%s: error %d\n"),
				    rg_name, temp_r_name, nlist, slist, err);
				ev_res.err_code = SCHA_ERR_INTERNAL;
				goto finished; //lint !e801
			}
#else

			if ((err = sc_publish_event(ESC_CLUSTER_R_STATE,
			    CL_EVENT_PUB_RGM, severity,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_R_NAME, SE_DATA_TYPE_STRING, temp_r_name,
			    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
			    CL_NODE_LIST, SE_DATA_TYPE_STRING, nlist,
			    CL_NODE_STATE_LIST, SE_DATA_TYPE_STRING,
			    slist, NULL)) != 0) {
				rgm_format_errmsg(&ev_res,
				    NOGET("sc_publish_event() "
				    "failed to publish event for rg_name=%s "
				    "r_name=%s nlist=%s slist=%s: error %d\n"),
				    rg_name, temp_r_name, nlist, slist, err);
				ev_res.err_code = SCHA_ERR_INTERNAL;
				goto finished; //lint !e801
			}
#endif
			free(nlist);
			nlist = NULL;
			free(slist);
			slist = NULL;

		}	// For all Rs in the RG
	}	// For each RG in the system

	// Log error if invalid R was specified.
	if ((r_name) && (rstate == NULL)) {
		ucmm_print("RGM",
			NOGET("post_event_rs_state() "
			"Invalid R <%s>\n"), r_name);
		ev_res.err_code = SCHA_ERR_INVAL;
		goto finished; //lint !e801
	}

	ucmm_print("RGM",
		NOGET("post_event_rs_state() "
		"posted event: returning\n"));

finished:

	//
	// Lint warning 644 says checking variable
	// (e.g. nlist, slist) which may not have
	// been initialized yet. The point is that these
	// are initialized after allocating new memory
	// which may or may-not happen depending on whether
	// or not the code above enters the for() loops.
	// Lint shouldnt be complaining that it is
	// not initialized since we are setting them to
	// NULL in the beginning. It is all right to suppress
	// these.
	//

	if (nlist)		//lint !e644
		free(nlist);
	if (slist)		//lint !e644
		free(slist);

	return (ev_res);
}

static char *
logicalnodeset_to_str(const LogicalNodeset &nodeset)
{
	char		*nodelist = NULL;
	size_t		oldlen;
	size_t		newlen;
	const char *fullname = NULL;
	rgm::lni_t	prevNode = 1;
	rgm::lni_t	currNode = 0;
	char		*newptr = NULL;

	while ((currNode = nodeset.nextLniSet(prevNode)) != 0) {

		//
		// This is run in the president ONLY. All the LNI
		// mappings should exist
		//
		LogicalNode *lnNode = lnManager->findObject(currNode);
		CL_PANIC(lnNode);

		fullname = lnNode->getFullName();

		oldlen = 0;
		newlen = strlen(fullname) + 1 + 1;
		// End NULL and comma

		if (nodelist) {
			(void) strcat(nodelist, ",");
			oldlen = strlen(nodelist) + 1;
			newlen += oldlen;
		}
		newptr = (char *)realloc(nodelist, newlen);
		if (newptr == NULL) {
			return (NULL);
		}
		bzero(&newptr[oldlen], newlen - oldlen);
		nodelist = newptr;
		newptr = NULL;

		(void) strcat(nodelist, fullname);

		prevNode = currNode + 1;
	}
	return (nodelist);
}


//
// Post an esc_cluster_rg_primaries_changing event.
// If a NULL value is passed it means that the information
// is not available for the caller. Post a "null" string for
// NULL values.
//
scha_errmsg_t
post_primaries_changing_event(const char *rs_name,
	const char *rg_name, cl_event_reason_code_t reason,
	const char *nodename, int rg_desired_primaries,
	const LogicalNodeset &current_nodeset,
	const LogicalNodeset &new_nodeset)
{

	char	*curr_nodelist = NULL;
	char	*new_nodelist = NULL;
	char	*temp_nodename = NULL;
	char	*resource_name = NULL;
	char	*resourcegroup_name = NULL;
	char	*event_nodename = NULL;
	cl_event_initiator_t initiator = CL_EVENT_INIT_SYSTEM;
	scha_errmsg_t ev_res = {SCHA_ERR_NOERR, NULL};


	// If a NULL value is passed we set it to
	// a empty string.

	if (rs_name == NULL)
		resource_name = strdup_nocheck("null");
	else
		resource_name = strdup_nocheck(rs_name);

	if (resource_name == NULL) {
		ev_res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	if (rg_name == NULL)
		resourcegroup_name = strdup_nocheck("null");
	else
		resourcegroup_name = strdup_nocheck(rg_name);

	if (resourcegroup_name == NULL) {
		ev_res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	if (nodename == NULL)
		event_nodename = strdup_nocheck("null");
	else
		event_nodename = strdup_nocheck(nodename);

	if (event_nodename == NULL) {
		ev_res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	if (current_nodeset.isEmpty()) {
		curr_nodelist = strdup_nocheck("null");
	} else {
		curr_nodelist = logicalnodeset_to_str(current_nodeset);
	}

	if (NULL == curr_nodelist) {
		ev_res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}


	if (new_nodeset.isEmpty()) {
		new_nodelist = strdup_nocheck("null");
	} else {
		new_nodelist = logicalnodeset_to_str(new_nodeset);
	}

	if (NULL == new_nodelist) {
		ev_res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}


	initiator = get_initiator(reason);

	// Now that we have the current nodelist and the new
	// node list, post the event. We dont care about the
	// return value.

#if SOL_VERSION >= __s10

	if (sc_publish_zc_event(ZONE, ESC_CLUSTER_RG_PRIMARIES_CHANGING,
	    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
	    initiator, 0, 0, 0,
	    CL_R_NAME, SE_DATA_TYPE_STRING, resource_name,
	    CL_RG_NAME, SE_DATA_TYPE_STRING, resourcegroup_name,
	    CL_REASON_CODE, SE_DATA_TYPE_UINT32, reason,
	    CL_NODE_NAME, SE_DATA_TYPE_STRING, event_nodename,
	    CL_DESIRED_PRIMARIES, SE_DATA_TYPE_UINT32,
	    rg_desired_primaries,
	    CL_FROM_NODE_LIST, SE_DATA_TYPE_STRING,
	    curr_nodelist,
	    CL_TO_NODE_LIST, SE_DATA_TYPE_STRING,
	    new_nodelist, NULL) != 0) {
		rgm_format_errmsg(&ev_res,
		    NOGET("post_primaries_changing_event() "
		    "failed to post event: error %s\n"),
		    strerror(errno));
		ev_res.err_code = SCHA_ERR_INTERNAL;
	}

#else

	if (sc_publish_event(ESC_CLUSTER_RG_PRIMARIES_CHANGING,
	    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
	    initiator, 0, 0, 0,
	    CL_R_NAME, SE_DATA_TYPE_STRING, resource_name,
	    CL_RG_NAME, SE_DATA_TYPE_STRING, resourcegroup_name,
	    CL_REASON_CODE, SE_DATA_TYPE_UINT32, reason,
	    CL_NODE_NAME, SE_DATA_TYPE_STRING, event_nodename,
	    CL_DESIRED_PRIMARIES, SE_DATA_TYPE_UINT32,
	    rg_desired_primaries,
	    CL_FROM_NODE_LIST, SE_DATA_TYPE_STRING,
	    curr_nodelist,
	    CL_TO_NODE_LIST, SE_DATA_TYPE_STRING,
	    new_nodelist, NULL) != 0) {
		rgm_format_errmsg(&ev_res,
		    NOGET("post_primaries_changing_event() "
		    "failed to post event: error %s\n"),
		    strerror(errno));
		ev_res.err_code = SCHA_ERR_INTERNAL;
	}
#endif

finished:

	//
	// Lint warning 644 says checking variable
	// (e.g. curr_nodelist, new_nodelist) which may not have
	// been initialized yet. The point is that these
	// are initialized after allocating new memory
	// which may or may-not happen depending on whether
	// or not the code above enters the for() loops.
	// Lint shouldnt be complaining that it is
	// not initialized since we are setting them to
	// NULL in the beginning. It is all right to suppress
	// these.
	//

	if (curr_nodelist)		//lint !e644
		free(curr_nodelist);
	if (new_nodelist)		//lint !e644
		free(new_nodelist);
	if (temp_nodename)
		free(temp_nodename);
	if (resource_name)
		free(resource_name);
	if (resourcegroup_name)
		free(resourcegroup_name);
	if (event_nodename)
		free(event_nodename);

	return (ev_res);

}

cl_event_initiator_t
get_initiator(cl_event_reason_code_t reason)
{
	cl_event_initiator_t initiator;

	switch (reason) {

	case CL_REASON_RG_SWITCHOVER:
	case CL_REASON_RG_RESTART:
		initiator = CL_EVENT_INIT_OPERATOR;
		break;

	case CL_REASON_RG_GIVEOVER:
	case CL_REASON_RG_GETOFF:
	case CL_REASON_RG_SC_RESTART:
		initiator = CL_EVENT_INIT_AGENT;
		break;
	default:
		initiator = CL_EVENT_INIT_SYSTEM;
		break;
	}
	return (initiator);
}



cl_event_severity_t
get_rg_severity(rgstatename_t *rgstate)
{
	cl_event_severity_t severity = CL_EVENT_SEV_INFO;

	switch (rgstate->api_val) {

	case SCHA_RGSTATE_ONLINE_FAULTED:
	case SCHA_RGSTATE_ERROR_STOP_FAILED:
		severity = CL_EVENT_SEV_CRITICAL;
		break;

	case SCHA_RGSTATE_UNMANAGED:
	case SCHA_RGSTATE_PENDING_ONLINE_BLOCKED:
		severity = CL_EVENT_SEV_WARNING;
		break;

	case SCHA_RGSTATE_ONLINE:
	case SCHA_RGSTATE_OFFLINE:
	case SCHA_RGSTATE_PENDING_ONLINE:
	case SCHA_RGSTATE_PENDING_OFFLINE:
		severity = CL_EVENT_SEV_INFO;
		break;
	default:
		break;
	}

	return (severity);
}

cl_event_severity_t
get_rs_severity(rstatename_t *rstate)
{
	cl_event_severity_t severity = CL_EVENT_SEV_INFO;

	switch (rstate->api_val) {

	case SCHA_RSSTATE_START_FAILED:
	case SCHA_RSSTATE_STOP_FAILED:
		severity = CL_EVENT_SEV_CRITICAL;
		break;
	case SCHA_RSSTATE_MONITOR_FAILED:
		severity = CL_EVENT_SEV_ERROR;
		break;

	case SCHA_RSSTATE_ONLINE_NOT_MONITORED:
		severity = CL_EVENT_SEV_WARNING;
		break;

	case SCHA_RSSTATE_STARTING:
	case SCHA_RSSTATE_STOPPING:
	case SCHA_RSSTATE_ONLINE:
	case SCHA_RSSTATE_OFFLINE:
	case SCHA_RSSTATE_DETACHED:
		severity = CL_EVENT_SEV_INFO;
		break;
	default:
		break;
	}

	return (severity);

}
