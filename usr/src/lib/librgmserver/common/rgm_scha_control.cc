/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_scha_control.cc	1.67	08/06/20 SMI"

//
// rgm_scha_control.cc
// It calls the real scha_control method on president node to do
// the RG failover work.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/cladm.h>
#include <orb/invo/common.h>
#include <h/sol.h>
#ifndef EUROPA_FARM
#include <rgm/rgm_comm_impl.h>
#endif
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm_proto.h>
#include <scha_err.h>
#include <scha.h>
#include <rgm_api.h>
#include <stdlib.h>

#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_pres.h>
#endif

static void restart_sleep_wait(const char *rname, rgm::lni_t lni,
    const char *nodename);

//
// log_blocked_scha_control()
//
// check the syslog history of the resource and log a trace in syslog
// if the resource hasn't had a similar failure in the not too distant past.
//
// the slave (and slave only) keeps a record of the last 2 attempts to call
// scha_control. If the last 2 failures a separated by T, the new failure is
// logged only if it happens at least 2*T after the last failure (or 30 minutes,
// whichever comes first). The intent here is to avoid filling up syslog in
// case a probe is stuck in a loop, requesting restart even though it
// Failover_mode doesn't allow it. The heuristic will block repeated traces
// and restore the syslog privilege when the probe is becoming more quiet
//
// rgm lock must be held
//
static void
log_blocked_scha_control(rlist_p_t rp, int retry_count, time_t retry_interval,
    scha_control_action_t action, rgm::lni_t lni, scha_failover_mode_t fo_mode)
{
	time_t	blocked_req, suspension_interval = 0, err1 = 0, err2 = 0;
	sc_syslog_msg_handle_t handle;
	char *scha_control_msg, *scha_fomode;

	if (time(&blocked_req) < 0) {
		// serious system error. Return ASAP.
		return;
	}

	rgm_timelist_t **rgtp = &rp->rl_xstate[lni].rx_blocked_restart;

	if (*rgtp && (*rgtp)->tl_next) {
		err1 = (*rgtp)->tl_time;
		err2 = (*rgtp)->tl_next->tl_time;
		suspension_interval = (err2 - err1) * 2;
		// time() returns a number of seconds. We minimize the
		// suspension interval to 3 seconds to work around the round-off
		// errors
		if (suspension_interval < 3) {
			suspension_interval = 3;
		}
		if (suspension_interval > 1800) {
			// max out the suspension interval to 30 minutes
			suspension_interval = 1800;
		}
	}

	if ((suspension_interval > 0) &&
	    (blocked_req - err2 < suspension_interval)) {
		// requests are blocked too frequently. We skip the log this
		// time.
	} else {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		switch (fo_mode) {
		case SCHA_FOMODE_NONE:
			scha_fomode = "NONE";
			break;

		case SCHA_FOMODE_HARD:
			scha_fomode = "HARD";
			break;

		case SCHA_FOMODE_SOFT:
			scha_fomode = "SOFT";
			break;

		case SCHA_FOMODE_RESTART_ONLY:
			scha_fomode = "RESTART_ONLY";
			break;

		case SCHA_FOMODE_LOG_ONLY:
			scha_fomode = "LOG_ONLY";
			break;
		default:
			ucmm_print("RGM", NOGET(
			    "Internal Error. Invalid Failover mode\n"));
			sc_syslog_msg_done(&handle);
			return;
		}

		switch (action)  {
		case GETOFF:
			// Getoff action mapped to Giveover.
			// fall through
		case GIVEOVER:
		case CHECK_GIVEOVER:
			scha_control_msg = "group failover";
			break;

		case CHECK_RESTART:
		case RESTART:
			scha_control_msg = "group restart";
			break;

		case RESOURCE_RESTART:
			scha_control_msg = "restart";
			break;

		// This function is never called with the following two actions.
		case RESOURCE_IS_RESTARTED:
		case IGNORE_FAILED_START:
		default:
			ucmm_print("RGM", NOGET(
			    "Internal Error. Invalid scha_control"
			    "action\n"));
			sc_syslog_msg_done(&handle);
			return;
		}

		// create "zonename" substring if lni is non-global zone
		LogicalNode *lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);

		if (rp->rl_rg->rgl_ccr->rg_suspended) {
			//
			// SCMSGS
			// @explanation
			// The operation requested by scha_control is
			// prevented because the resource group is suspended.
			// @user_action
			// No action required. To enable automatic
			// recovery actions, use 'clresourcegroup resume'
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "The resource group %s containing"
			    " the resource %s is suspended. This has"
			    " blocked a resource %s attempt on %s ",
			    rp->rl_ccrdata->r_rgname,
			    rp->rl_ccrdata->r_name, scha_control_msg,
			    lnNode->getFullName());
		} else if ((retry_count != 0) && (retry_interval != 0)) {
			//
			// SCMSGS
			// @explanation
			// The rgmd is enforcing the RESTART_ONLY value
			// for the Failover_mode system property. A
			// request to restart a resource is denied
			// because the resource has already been
			// restarted Retry_count times within the past
			// Retry_interval seconds.
			// @user_action
			// No action required. If desired, use
			// clresourcegroup to change the Failover_mode
			// setting.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "A resource restart attempt on "
			    "resource %s in resource group %s on node "
			    "%s with Failover_mode=RESTART_ONLY has "
			    "been blocked because the number of "
			    "restarts within the past Retry_interval "
			    "(%d seconds) would exceed "
			    "Retry_count (%d)",
			    rp->rl_ccrdata->r_name,
			    rp->rl_ccrdata->r_rgname,
			    lnNode->getFullName(), retry_interval,
			    retry_count);
		} else {
			//
			// SCMSGS
			// @explanation
			// The rgmd is enforcing the RESTART_ONLY or
			// LOG_ONLY value for the Failover_mode system
			// property. Those settings may prevent some
			// operations initiated by scha_control.
			// @user_action
			// No action required. If desired, use
			// clresourcegroup to change the Failover_mode
			// setting.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "The Failover_mode setting of %s "
			    "for resource %s in resource group %s has "
			    "blocked a resource %s attempt on %s",
			    scha_fomode, rp->rl_ccrdata->r_name,
			    rp->rl_ccrdata->r_rgname, scha_control_msg,
			    lnNode->getFullName());
		}
		sc_syslog_msg_done(&handle);
	}

	//
	// add current blocked request time to the history
	// we only keep trace of the last 2 blocked requests to be able
	// to compute the suspension interval
	//
	if (*rgtp) {
		if ((*rgtp)->tl_next) {
			(*rgtp)->tl_time = err2;
			(*rgtp)->tl_next->tl_time = blocked_req;
		} else {
			(*rgtp)->tl_next = new rgm_timelist_t;
			// abort is called if no memory is available
			// see rgm_main.cc, call to set_new_handler()
			(*rgtp)->tl_next->tl_time = blocked_req;
			(*rgtp)->tl_next->tl_next = NULL;
		}
	} else {
		(*rgtp) = new rgm_timelist_t;
		// abort is called if no memory is available
		// see rgm_main.cc, call to set_new_handler()
		(*rgtp)->tl_time = blocked_req;
		(*rgtp)->tl_next = NULL;
	}
}


//
// clear_blocked_msg_history
//
// clear the history of blocked scha_control request for the resource
//
// rgm lock must be held
//
void
clear_blocked_msg_history(rlist_p_t rp, rgm::lni_t lni)
{
	rgm_timelist_t **rgtp = &rp->rl_xstate[lni].rx_blocked_restart;
	if (*rgtp) {
		if ((*rgtp)->tl_next) {
			delete (*rgtp)->tl_next;
		}
		delete *rgtp;
	}
	*rgtp = NULL;
}


//
// check_fom_before_scha_control()
//
// check the failover mode of the resource to see if the scha_control request
// is valid. Check on the slave to avoid sending msg to president, as a
// probe may be stuck in a loop and uselessly crowd the network.
// If the request is blocked, only log a trace in
// syslog if no such trace has been recorded recently. This is to avoid filling
// up syslog
//
// returns true if the request is valid with re: Failover_mode settings
// false if not
//
static boolean_t
check_fom_before_scha_control(const char *rg_name, const char *r_name,
    scha_control_action_t action, rgm::lni_t lni)
{
	rlist_p_t		rp;
	rglist_p_t		rgp;
	boolean_t		res = B_TRUE;
	scha_failover_mode_t	fo_mode;
	int			retry_count, count;
	time_t			retry_interval;
	scha_errmsg_t		retry_count_res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t		retry_interval_res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t		fo_mode_res = {SCHA_ERR_NOERR, NULL};


	rgm_lock_state();

	rgp = rgname_to_rg(rg_name);
	if (rgp == NULL) {
		// let the president handle it
		goto check_end; //lint !e801
	}

	rp = rname_to_r(rgp, r_name);
	if (rp == NULL) {
		// let the president handle it
		goto check_end; //lint !e801
	}

	retry_count_res = get_retry_count(rp, &retry_count);
	retry_interval_res = get_retry_interval(rp, &retry_interval);

	fo_mode_res = get_fo_mode(rp->rl_ccrdata, &fo_mode);
	if (fo_mode_res.err_code != SCHA_ERR_NOERR) {
		// this is an internal error. Let request
		// go through and the president handle it
		goto check_end; //lint !e801
	}

	// get the value of property to check if RG is suspended
	if (rgp->rgl_ccr->rg_suspended) {
		fo_mode = SCHA_FOMODE_LOG_ONLY;
	}

	// Failover_mode=RESTART_ONLY with a retry_count set to
	// zero (if defined) is treated like LOG_ONLY
	if (fo_mode == SCHA_FOMODE_LOG_ONLY ||
	    (fo_mode == SCHA_FOMODE_RESTART_ONLY &&
		retry_count_res.err_code == SCHA_ERR_NOERR &&
		retry_count == 0)) {
		// we should block RESTART, CHECK_RESTART,
		// GIVEOVER, CHECK_GIVEOVER, RESOURCE_RESTART
		if (action == RESTART || action == CHECK_RESTART ||
		    action == GIVEOVER || action == CHECK_GIVEOVER ||
		    action == RESOURCE_RESTART || action == RESOURCE_DISABLE) {
			log_blocked_scha_control(rp, 0, 0, action, lni,
			    fo_mode);
			res = B_FALSE;
		}
	} else if (fo_mode == SCHA_FOMODE_RESTART_ONLY) {
		// we should block GIVEOVER, CHECK_GIVEOVER
		if (action == GIVEOVER || action == CHECK_GIVEOVER ||
		    action == GETOFF) {
			log_blocked_scha_control(rp, 0, 0, action, lni,
			    fo_mode);
			res = B_FALSE;
		} else {
			// Failover_mode = RESTART_ONLY and
			// we are trying to restart a resource.
			// We still need to check if retry_count has
			// been reached within retry_interval.
			// if retry_count and retry_interval are not
			// defined (they are optional), we accept the
			// scha_control request
			if ((action == RESOURCE_RESTART) &&
			    (retry_count_res.err_code == SCHA_ERR_NOERR &&
			    retry_interval_res.err_code == SCHA_ERR_NOERR &&
			    retry_count >= 0)) {
				/*
				 * Prune the resource restart timelist,
				 * then count the entries in it
				 */

				rgm_timelist_t **rgtp =
				    &(rp->rl_xstate[lni].rx_r_restarts);
				rgm_prune_timelist(rgtp, retry_interval);
				count = (int)rgm_count_timelist(*rgtp);
				if (count >= retry_count) {
					log_blocked_scha_control(rp,
					    retry_count, retry_interval,
					    action, lni, fo_mode);
					res = B_FALSE;
				}
			}
		}

	}

	if (res) {
		/*
		 * the request for the resource is not blocked
		 * clear the history of blocked msg, so that the next blocked
		 * request will be logged
		 */
		clear_blocked_msg_history(rp, lni);
	}

check_end:
	rgm_unlock_state();

	return (res);
}


//
// scha_control_action()
// This function will call the relevent IDL function on president to
// perform the right action for scha_control
//
// For GIVEOVER (and related) action, 'idl_scha_control_result' is used
// to pass back the log message for sanity check methods (pingpong check,
// monitor check, etc) running on each candidate node.
//


void
scha_control_action(scha_control_action_t action, const char *rg_name,
    const char *r_name, rgm::lni_t lni, scha_result_t *result)
{
#ifdef EUROPA_FARM
	rgmx_scha_control_args schactl_args;
	rgmx_scha_control_result schactl_giveover_res_v;
	rgmx_regis_result_t schactl_res_v;
	CLIENT *clnt;
#else
	Environment			e;
	rgm::rgm_comm_var		prgm_pres_v;
	rgm::idl_string_seq_t		node_seq;
	rgm::idl_string_seq_t		rg_seq;
	rgm::idl_scha_control_args	schactl_args;
	rgm::idl_scha_control_result_var	schactl_giveover_res_v;
	rgm::idl_regis_result_t_var	schactl_res_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
#endif
	sc_syslog_msg_handle_t		handle;
	boolean_t			do_restart_call = B_FALSE;
	rglist_p_t			rglp;
	rlist_p_t			rp = NULL;
	rgm_timelist_t			**rtp = NULL;
	LogicalNode			*lnNode;
	char				*nodename = NULL;

	schactl_args.idlstr_rg_name = new_str(rg_name);
	schactl_args.idlstr_R_name = new_str(r_name);
	schactl_args.flags = 0;
	schactl_args.lni = lni;

	rgm_lock_state();
	lnNode = lnManager->findObject(lni);
	if (lnNode == NULL) {
		result->ret_code = SCHA_ERR_NODE;
		rgm_unlock_state();
		return;
	}
	nodename = strdup(lnNode->getFullName());
	rgm_unlock_state();

#ifdef EUROPA_FARM
	clnt = rgmx_comm_getpres(ZONE);
	if (clnt == NULL) {
		result->ret_code = SCHA_ERR_CLRECONF;
		free(nodename);
		return;
	}
#else
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
	if (CORBA::is_nil(prgm_pres_v)) {
		//
		// president has not registered yet
		// Normally, this case should not happen because
		// if the RGM hasn't started up yet, why is someone
		// calling scha_control?
		// However it is possible that some program or script
		// (outside of any data service method) calls scha_control
		// before the rgmd has started up.  We will syslog an
		// error message and return non-zero.
		//
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// This message most likely indicates that a program called
		// scha_control(1ha,3ha) before the RGM had started up.
		// Normally, scha_control is called by a resource monitor to
		// request failover or restart of a resource group. If the RGM
		// had not yet started up on the cluster, no resources or
		// resource monitors should have been running on any node. The
		// scha_control call will fail with a SCHA_ERR_CLRECONF error.
		// @user_action
		// On the node where this message appeared, confirm that rgmd
		// daemon was not yet running (i.e., the cluster was just
		// booting up) when this message was produced. Find out what
		// program called scha_control. If it was a customer-supplied
		// program, this most likely represents an incorrect program
		// behavior which should be corrected. If there is no such
		// customer-supplied program, or if the cluster was not just
		// starting up when the message appeared, contact your
		// authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "ERROR: scha_control() was called on resource group <%s>,"
		    " resource <%s>, node <%s> before the RGM started",
		    (const char *)schactl_args.idlstr_rg_name,
		    (const char *)schactl_args.idlstr_R_name, nodename);
		sc_syslog_msg_done(&handle);
		result->ret_code = SCHA_ERR_CLRECONF;
		free(nodename);
		return;
	}
#endif

	ucmm_print("RGM", NOGET(
	    "prgm_pres_v->idl_scha_control()\n"));


	if (check_fom_before_scha_control(rg_name, r_name,
		action, lni)) {

		switch (action) {
		case GETOFF:
			schactl_args.flags |= rgm::SCHACTL_GETOFF;
			//lint -fallthrough
		case CHECK_GIVEOVER:
			if (schactl_args.flags != rgm::SCHACTL_GETOFF)
				schactl_args.flags |= rgm::SCHACTL_CHECKONLY;
			//lint -fallthrough
		case GIVEOVER:
			//
			// Num_resource_restarts should not be reset
			// on a CHECK_GIVEOVER call
			//
			if (action != CHECK_GIVEOVER) {
				// Reset the Num_resource_restarts
				rgm_lock_state();
				rglp = rgname_to_rg(rg_name);
				if (rglp != NULL) {
					rp = rname_to_r(rglp, r_name);
					if (rp != NULL) {
						rtp = &rp->
						    rl_xstate[lni].
						    rx_r_restarts;
						rgm_prune_timelist(rtp, 0);
					}
				}
				rgm_unlock_state();

				if (rglp == NULL) {
					ucmm_print("RGM",
					    NOGET("RG <%s> is not in memory"),
					    rg_name);
					result->ret_code = SCHA_ERR_RG;
					break;
				}
				if (rp == NULL) {
					ucmm_print("RGM",
					    NOGET("RS <%s> is not in memory"),
					    r_name);
					result->ret_code = SCHA_ERR_RSRC;
					break;
				}
			}

			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The RGM has recieved a request for the specified
			// scha_control(1HA) or scha_control(3HA) operation.
			// @user_action
			// No user action required.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    MESSAGE, "resource %s in resource group %s has "
			    "requested %s on %s.", r_name, rg_name,
			    "failover of the resource group", nodename);
			sc_syslog_msg_done(&handle);

#ifdef EUROPA_FARM
			bzero(&schactl_giveover_res_v,
				sizeof (rgmx_scha_control_result));
			if (rgmx_scha_control_giveover_1(&schactl_args,
				&schactl_giveover_res_v, clnt)
			    != RPC_SUCCESS) {
				result->ret_code = SCHA_ERR_ORB;
				xdr_free((xdrproc_t)
				    xdr_rgmx_scha_control_result,
				    (char *)&schactl_giveover_res_v);
				break;
			}

			//
			// If there are any syslog message which returned
			// from idl call, we will post it on the local node.
			//
			if ((const char *) schactl_giveover_res_v.
			    idlstr_syslog_msg != NULL) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, NO_SC_SYSLOG_MESSAGE("%s"),
				    (const char *) schactl_giveover_res_v.
				    idlstr_syslog_msg);
				sc_syslog_msg_done(&handle);
			}

			result->ret_code = schactl_giveover_res_v.ret_code;

			xdr_free((xdrproc_t)xdr_rgmx_scha_control_result,
				(char *)&schactl_giveover_res_v);
#else
			prgm_pres_v->idl_scha_control_giveover(schactl_args,
			    schactl_giveover_res_v, e);
			if ((res = except_to_scha_err(e)).err_code !=
			    SCHA_ERR_NOERR) {
				result->ret_code = res.err_code;
				break;
			}


			//
			// If there are any syslog message which returned
			// from idl call, we will post it on the local node.
			//
			if ((const char *) schactl_giveover_res_v->
			    idlstr_syslog_msg != NULL) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, NO_SC_SYSLOG_MESSAGE("%s"),
				    (const char *) schactl_giveover_res_v->
				    idlstr_syslog_msg);
				sc_syslog_msg_done(&handle);
			}

			result->ret_code = schactl_giveover_res_v->ret_code;
#endif // EUROPA_FARM
			break;

		case RESOURCE_DISABLE:
			schactl_args.flags = rgm::SCHACTL_R_DISABLE;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The fault monitor has detected an unrecoverable
			// fault with the specified resource. As a result, the
			// fault monitor issues a request to the RGM to
			// disable the resource.
			// @user_action
			// No user action required.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    MESSAGE, "Fault with resource detected. "
			    "Disabling resource %s.", r_name);
			sc_syslog_msg_done(&handle);
#ifdef EUROPA_FARM
			bzero(&schactl_res_v, sizeof (rgmx_regis_result_t));
			if (rgmx_scha_control_disable_1(&schactl_args,
			    &schactl_res_v, clnt)) {
				result->ret_code = SCHA_ERR_ORB;
			} else {
				result->ret_code = schactl_res_v.ret_code;
			}
			xdr_free((xdrproc_t)xdr_rgmx_regis_result_t,
			    (char *)&schactl_res_v);
#else
			prgm_pres_v->idl_scha_control_disable(schactl_args,
			    schactl_res_v, e);
			if ((res = except_to_scha_err(e)).err_code
			    != SCHA_ERR_NOERR)
				result->ret_code = res.err_code;
			else
				result->ret_code = schactl_res_v->ret_code;
#endif // EUROPA_FARM
			break;

		case RESOURCE_RESTART:
			schactl_args.flags = rgm::SCHACTL_R_RESTART;
			do_restart_call = B_TRUE;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    MESSAGE, "resource %s in resource group %s has "
			    "requested %s on %s.", r_name, rg_name,
			    "restart of the resource", nodename);
			sc_syslog_msg_done(&handle);

			break;

		case RESOURCE_IS_RESTARTED:
			schactl_args.flags = rgm::SCHACTL_R_IS_RESTARTED;
			do_restart_call = B_TRUE;
			break;

		case CHECK_RESTART:
			schactl_args.flags = rgm::SCHACTL_CHECKONLY;
			do_restart_call = B_TRUE;
			break;

		case RESTART:
			do_restart_call = B_TRUE;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    MESSAGE, "resource %s in resource group %s has "
			    "requested %s on %s.", r_name, rg_name,
			    "restart of the resource group", nodename);
			sc_syslog_msg_done(&handle);

			break;

			// IGNORE_FAILED_START is caught before
		case IGNORE_FAILED_START:
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The scha_control function has encountered an
			// internal logic error. This will cause scha_control
			// to fail with a SCHA_ERR_INTERNAL error, thereby
			// preventing a resource-initiated failover.
			// @user_action
			// Please save a copy of the /var/adm/messages files
			// on all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "INTERNAL ERROR: scha_control_action: "
			    "invalid action <%d>", action);
			sc_syslog_msg_done(&handle);
			result->ret_code = SCHA_ERR_INTERNAL;
			break;
		}
	} else {
		//
		// the call to scha_control has not been propagated to the
		// president because of the setting of Failover_mode. We
		// return SCHA_ERR_CHECKS.
		//
		// msg has already been logged if necessary
		//
		result->ret_code = SCHA_ERR_CHECKS;
	}

	if (action == RESTART || action == RESOURCE_RESTART ||
	    action == RESOURCE_IS_RESTARTED) {
	    // these actions need slave side throttling
		restart_sleep_wait(r_name, lni, nodename);
	}

	if (do_restart_call) {
#ifdef EUROPA_FARM
		bzero(&schactl_res_v, sizeof (rgmx_regis_result_t));
		if (rgmx_scha_control_restart_1(&schactl_args,
			&schactl_res_v, clnt)) {
			result->ret_code = SCHA_ERR_ORB;
		} else {
			result->ret_code = schactl_res_v.ret_code;
		}

		xdr_free((xdrproc_t)xdr_rgmx_regis_result_t,
			(char *)&schactl_res_v);
#else
		prgm_pres_v->idl_scha_control_restart(schactl_args,
		    schactl_res_v, e);
		if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
			result->ret_code = res.err_code;
		else
			result->ret_code = schactl_res_v->ret_code;
#endif // EUROPA_FARM
	}

	free(nodename);

#ifdef EUROPA_FARM
	delete []schactl_args.idlstr_rg_name;
	delete [] schactl_args.idlstr_R_name;
	rgmx_comm_freecnt(clnt);
#endif
}


#define	TRACK_HISTORY_THROTTLE_RESTART	3600
#define	MAX_THROTTLE_RESTART		300
#define	NUM_RESTARTS_WO_THROTTLE	5 // DON'T SET BELOW 1
//
// For throttling algorithm, we keep 2 state variables (only) on the slave side
// in the resource per zone data structure. "rx_throttle_restart_counter" tells
// us which phase of throttling algorithm we are in; and "rx_last_restart_time"
// tells us the last restart time.
// Throttling logic: In each successive call, the rx_throttle_restart_counter is
// incremented by 1. Allow NUM_RESTARTS_WO_THROTTLE restarts with zero delay.
// In successive restarts after that, the request is made to wait for
// pow(2, rx_throttle_restart_counter - NUM_RESTARTS_WO_THROTTLE) seconds.
// However if the wait_time increases beyond MAX_THROTTLE_RESTART, we clamp the
// sleep time down to this value itself and also stop incrementng the throttle
// restart counter.
// If there is no restart in the last TRACK_HISTORY_THROTTLE_RESTART time,
// reinitialize the counter to 0, and the algorithm starts afresh at the next
// restart.
// If the restarter itself has waited for a while since the last restart, wait
// only for the differential amount of time. Thus if restarter has slept for
// MAX_THROTTLE_RESTART, then the restart is attempted without any delay.
//
// (counter, sleep_time(in secs)} = { {0, 0}, {1, 0}, {2, 0},
// {3, 0}, {4, 0}, {5, 1}, {6,2}, {7, 4}, {8, 8}, {9, 16}, {10, 32}, {11, 64}
// {12, 128}, {13, 256} {14, 300} (converges here)
//
// The macros for thresholds associated with the algorithm, can be fine tuned at
// a later time, if desired.
//
static void
restart_sleep_wait(const char *rname, rgm::lni_t lni, const char *nodename) {

	time_t			curr_time;
	time_t			sleep_time;
	time_t			time_since_last_restart;
	rlist_p_t		rp;
	sc_syslog_msg_handle_t	handle;

	ucmm_print("restart_sleep_wait()", NOGET("entry"));
	ASSERT(NUM_RESTARTS_WO_THROTTLE > 0);

	rgm_lock_state();
	if ((rp = rname_to_r(NULL, rname)) == NULL) {
		ucmm_print("restart_sleep_wait()", NOGET("unknown rname"));
		rgm_unlock_state();
		return; // rname invalid. return. scha_control will fail anyway.
	}

	curr_time = time(NULL);
	if (rp->rl_xstate[lni].rx_last_restart_time == (time_t)-1 ||
	    curr_time == (time_t)-1) {
		// in case of erroroneous time queries, we assume worst case
		time_since_last_restart = 0;
	} else {
		time_since_last_restart =
		    curr_time - rp->rl_xstate[lni].rx_last_restart_time;
	}

	if (time_since_last_restart > TRACK_HISTORY_THROTTLE_RESTART) {
		// forget all throttling history and start afresh.
		ucmm_print("restart_sleep_wait()", NOGET("reset history"));
		rp->rl_xstate[lni].rx_throttle_restart_counter = sleep_time = 0;
	} else {
		sleep_time = rp->rl_xstate[lni].rx_throttle_restart_counter;
	}

	sleep_time = (time_t)((sleep_time < NUM_RESTARTS_WO_THROTTLE) ?
	    0 : 1 << (sleep_time - NUM_RESTARTS_WO_THROTTLE));

	if (sleep_time > MAX_THROTTLE_RESTART) {
		//
		// clamp the sleep wait to MAX_DELAY allowed. Also don't
		// increase throttle_counter since it has reached max limit.
		//
		sleep_time = MAX_THROTTLE_RESTART;
	} else {
		rp->rl_xstate[lni].rx_throttle_restart_counter++;
	}

	//
	// if the restart request has come after some delay, we sleep
	// for a lesser time to give it benifit of its own throttling behaviour.
	// If "time_since_last_restart" is negative, this implies that another
	// thread is already in queue, sleep_waiting for restart. Hence this
	// thread will sleep an additional amount of time.
	//
	sleep_time -= time_since_last_restart;
	if (sleep_time < 0)
		sleep_time = 0;
	rp->rl_xstate[lni].rx_last_restart_time = time(NULL);
	if (rp->rl_xstate[lni].rx_last_restart_time != (time_t)-1) {
		// if error, let it remain negative.
		rp->rl_xstate[lni].rx_last_restart_time += sleep_time;
	}

	if (sleep_time > 0) {
		//
		// SCMSGS
		// @explanation
		// A misconfiguration in the application and/or its monitor
		// probe, or an administrative action is causing continuous
		// restart requests to be sent to the RGM. Hence RGM is blocking
		// the requests for some time, so that too many restarts
		// are prevented.
		// @user_action
		// Check accompanying syslog messages being issued from the
		// application or the fault monitor of the data service to
		// diagnose any misconfiguration issues.
		//
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    "Resource <%s> is restarting too often on <%s>. "
		    "Sleeping for <%d> seconds.", rp->rl_ccrdata->r_name,
		    nodename, sleep_time);
		sc_syslog_msg_done(&handle);

		ucmm_print("restart_sleep_wait()", NOGET("sleep for %d secs"),
		    sleep_time);
	}

	rgm_unlock_state();
	while (sleep_time > 0) {
		sleep_time = sleep(sleep_time);
	}
}
