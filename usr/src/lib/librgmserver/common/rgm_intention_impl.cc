//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_intention_impl.cc	1.111	09/04/22 SMI"

//
// rgm_intention_impl.cc
//
// Implementation of latch/process/unlatch IDL interfaces. Called
// by the president at the time of making updates to the CCR
// during create/update/delete operations on RGs and RSs.
//

#include <rgm/rgm_msg.h>
#include <rgm_proto.h>
#ifndef EUROPA_FARM
#include <rgm/rgm_comm_impl.h>
#endif
#include <rgm_intention.h>
#include <rgm/rgm_cnfg.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_farm.h>
#endif

// prototypes for static functions
static bool process_intention_helper(rgm::lni_t lni);
static void intention_update_rg(char *en_name, rgm::intention_flag_t flags,
    rgm::lni_t lni);
static void intention_update_rs(char *en_name, rgm::intention_flag_t flags,
    rgm::lni_t lni);
static void intention_update_rt(char *en_name, rgm::intention_flag_t flags,
    rgm::lni_t lni);
static void intention_create_rg(char *en_name, rgm::intention_flag_t flags);
static void intention_create_rs(char *en_name, rgm::intention_flag_t flags,
    rgm::lni_t lni);
static void intention_delete_rg(char *en_name, rgm::intention_flag_t flags);
static bool intention_delete_rs(char *en_name, rgm::intention_flag_t flags,
    rgm::lni_t lni);
static void intention_delete_rt(char *en_name);
static void intention_manage(char *en_name, rgm::intention_flag_t flags,
    rgm::lni_t lni);
static void intention_unmanage(char *en_name, rgm::intention_flag_t flags,
    rgm::lni_t lni);
static void intention_enable_resource(char *en_name);
static void intention_disable_resource(char *en_name);
static void intention_enable_monitor(char *en_name);
static void intention_disable_monitor(char *en_name);
static LogicalNodeset *intention_required(char *en_name, rlist_p_t rp,
    bool onoff, bool enable);
static void unlatch_intention_helper(rgm::lni_t lni);

#ifdef EUROPA_FARM
bool_t
rgmx_latch_intention_1_svc(
	rgmx_latch_intention_args *rpc_args,
	rgmx_latch_intention_result *rpc_res,
	struct svc_req *)
#else
void
rgm_comm_impl::idl_latch_intention(
    sol::nodeid_t president, sol::incarnation_num incarnation,
    rgm::lni_t lni, rgm::ln_incarnation_t ln_incarnation,
    const rgm::rgm_intention &intent, Environment &env)
#endif
{
	char	*en_name = NULL;
	slave_intention *Rgm_Intention;
#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	rgm::lni_t lni = rpc_args->lni;
	rgm::ln_incarnation_t ln_incarnation = rpc_args->ln_incarnation;
	rgm::rgm_intention intent;

	intent.entity_type = (rgm::rgm_entity)rpc_args->intent.entity_type;
	intent.operation_type = (rgm::rgm_operation)
		rpc_args->intent.operation_type;
	intent.idlstr_entity_name = strdup(rpc_args->intent.idlstr_entity_name);
	intent.flags = rpc_args->intent.flags;

	rpc_res->retval = RGMRPC_OK;
#endif

#ifndef EUROPA_FARM
	FAULT_POINT_FOR_ORPHAN_ORB_CALL("idl_latch_intention", president,
		incarnation);
#endif

	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		slave_unlock_state();
#ifdef EUROPA_FARM
		rpc_res->retval = RGMRPC_NOREF;
		return (TRUE);
#else
		return;
#endif
	}

	//
	// Return early with IDL exception if zone died or is shutting down.
	// We do this only in the base-cluster.  In a zone cluster,
	// we must latch the intention on every node whether the zone
	// has died or not, so that the rgmd's in-memory state can be updated.
	//
#ifndef EUROPA_FARM
	if (strcmp(ZONE, GLOBAL_ZONENAME) == 0 &&
	    zone_died(lni, ln_incarnation, env)) {
		slave_unlock_state();
		return;
	}
#else
	if (zone_died(lni, ln_incarnation, &rpc_res->retval)) {
		//
		// Sends back RGMRPC_ZONE_DEATH so that the caller
		// can take the appropriate decision
		//
		slave_unlock_state();
		return (TRUE);
	}
#endif
	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);

	en_name = strdup(intent.idlstr_entity_name);
	ucmm_print("idl_latch_intention)",
	    NOGET("Entity = <%s>, Type = <%d>, Op = <%d>, flags=<0x%x>\n"),
	    en_name, intent.entity_type, intent.operation_type, intent.flags);
	if (lnNode->getIntention() != NULL) {
		ucmm_print("idl_latch_intention()",
		    NOGET("Error: Already latched\n"));
		goto finished; //lint !e801
	}
	Rgm_Intention = (struct slave_intention *)malloc(
	    sizeof (struct slave_intention));
	Rgm_Intention->intention = new rgm::rgm_intention();
	Rgm_Intention->have_processed = 0;

	//
	// The rgm_intention operator= performs a proper deep copy.
	//
	*(Rgm_Intention->intention) = intent;

	// Store intention pointer in the LN
	lnNode->setIntention(Rgm_Intention);

finished:
	free(en_name);

	slave_unlock_state();
#ifdef EUROPA_FARM
	return (TRUE);
#endif
}


//
// This is a static routine which checks to see if the given resource, rp,
// has the UPDATE method registered. If it does, then it sets the state of
// of the resource and its contaning RG appropriately. If the state of the
// R on the local node is ONLINE, ONLINE_UNMON or MON_FAILED, only then we
// need to call the UPDATE method.  Otherwise this is not Dynamic Update.
// Note that we are only interested in the state on the local node here,
// as the intention will be processed on all the nodes and each node will
// take appropriate action based on the R state on that node.
//
// For proxy resources, UPDATE method should be called even if the state
// of the R is ONLINE_STANDBY in addition to the above mentioned states.
//
static boolean_t
r_check_and_set_state(rlist_t *rp, rgm::lni_t lni)
{
	boolean_t need_launch = B_FALSE;

	//
	// check if the UPDATE method is registered (a scalable service
	// always calls the SSM wrapper for update).
	// DU is applicable only if it is.
	// Else we simply log a message.
	//
	if (is_r_scalable(rp->rl_ccrdata) ||
	    is_method_reg(rp->rl_ccrtype, METH_UPDATE, NULL)) {

		ucmm_print("r_check_and_set_state()",
		    NOGET("UPDATE meth is registered. "
		    "Resource <%s> state: %s\n"), rp->rl_ccrdata->r_name,
		    pr_rstate(rp->rl_xstate[lni].rx_state));

		//
		// The president-side functions that update RGs or Rs
		// [scrgadm_rg_update_prop_helper() and
		// idl_scrgadm_rs_update_prop(), respectively]
		// go to some pains to bail out early if a resource restart
		// is initiated in the RG before the update intention is
		// processed.
		//
		// Just in case that logic didn't work: if the RG
		// state is ON_PENDING_R_RESTART, we will avoid attempting
		// to launch the update methods, and log an error to syslog.
		//
		if (rp->rl_rg->rgl_xstate[lni].rgx_state ==
		    rgm::RG_ON_PENDING_R_RESTART) {
			const char *r_name = rp->rl_ccrdata->r_name;
			const char *rg_name = rp->rl_rg->rgl_ccr->rg_name;
			sc_syslog_msg_handle_t handle;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RS_TAG, r_name);
			//
			// SCMSGS
			// @explanation
			// Following the update of a resource group or resource
			// property, rgmd is unable to execute an Update method
			// on the indicated resource because the resource group
			// is currently busy on this node executing a resource
			// restart.  This should not occur, and might indicate
			// a logic error in rgmd.  The rgmd suppresses
			// execution of the Update method on this resource.
			// Depending on what property was updated, this might
			// prevent this resource or its monitor from
			// functioning properly on this node.
			// @user_action
			// To recover from this error, execute another update
			// of a resource group or resource property.  You
			// may re-execute the same update that produced the
			// error.  Save a copy of the /var/adm/messages files
			// on all nodes, and contact your Sun service provider
			// to determine whether a workaround or patch is
			// available.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, SYSTEXT(
			    "Resource <%s> Update method suppressed because "
			    "a resource is restarting in resource group <%s>"),
			    r_name, rg_name);
			sc_syslog_msg_done(&handle);
			return (B_FALSE);
		}

		//
		// we set the resource's state to
		// R_ON_PENDING_UPDATE,R_ONUNMON_PENDING_UPDATE
		// or R_MONFLD_PENDING_UPDATE depending on
		// whether the R is in R_ONLINE, R_ONLINE_UNMON
		// or R_MON_FAILED. The RG state is set to
		// RG_ON_PENDING_METHODS in all the cases.
		//
		// We don't need to handle R_JUST_STARTED, because the RG
		// should never be in a terminal state that allows updates
		// while an R is in R_JUST_STARTED.
		//
		if (rp->rl_ccrtype->rt_proxy == B_TRUE &&
		    rp->rl_xstate[lni].rx_state == rgm::R_ONLINE_STANDBY) {
			need_launch = B_TRUE;
			// set RG state to ON_PENDING_METHODS
			set_rgx_state(lni, rp->rl_rg,
			    rgm::RG_ON_PENDING_METHODS);
			// set R state to PENDING_UPDATE
			set_rx_state(lni, rp, rgm::R_PENDING_UPDATE);
		} else if (rp->rl_xstate[lni].rx_state == rgm::R_ONLINE) {
			need_launch = B_TRUE;
			// set RG state to ON_PENDING_METHODS
			set_rgx_state(lni, rp->rl_rg,
			    rgm::RG_ON_PENDING_METHODS);
			// set R state to ON_PENDING_UPDATE
			set_rx_state(lni, rp, rgm::R_ON_PENDING_UPDATE);
		} else if (rp->rl_xstate[lni].rx_state ==
		    rgm::R_ONLINE_UNMON) {
			need_launch = B_TRUE;
			// set RG state to ON_PENDING_METHODS
			set_rgx_state(lni, rp->rl_rg,
			    rgm::RG_ON_PENDING_METHODS);
			// set R state to ONUNMON_PENDING_UPDATE
			set_rx_state(lni, rp, rgm::R_ONUNMON_PENDING_UPDATE);
		} else if (rp->rl_xstate[lni].rx_state ==
		    rgm::R_MON_FAILED) {
			need_launch = B_TRUE;
			// set RG state to ON_PENDING_METHODS
			set_rgx_state(lni, rp->rl_rg,
			    rgm::RG_ON_PENDING_METHODS);
			// set R state to MONFLD_PENDING_UPDATE
			set_rx_state(lni, rp, rgm::R_MONFLD_PENDING_UPDATE);
		}
	} else
		ucmm_print("r_check_and_set_state()",
		    NOGET("DU: no update meth registered\n"));

	ucmm_print("r_check_and_set_state()",
	    NOGET("lni <%d> resource <%s> new state %s.\n"),
	    lni, rp->rl_ccrdata->r_name,
	    pr_rstate(rp->rl_xstate[lni].rx_state));

	return (need_launch);
}

//
// intention_update_rg
//
// Five possible flags:
// INTENT_OK_FLAG means the rgl_ok_to_start flag is being set to true.
// INTENT_RUN_UPDATE flag means properties were changed, and we need
// to run update on all the resources.
// INTENT_RUN_INIT means we need to run INIT on all the resources.
// INTENT_RUN_FINI means we need to run FINI on all the resources
// INTENT_READ_RS means we need to read resource from CCR to update
// in-memory resource structure.
//
// The lni indicates the zone currently being processed.
// We read the new RG CCR structure into memory and set the rgl_ok_to_start
// flag only if lni indicates a ZC or global zone -- thus, we do it only once
// per physical node.
// However we may run INIT, FINI, or UPDATE methods in any zone.
//
void
intention_update_rg(char *en_name, rgm::intention_flag_t flags, rgm::lni_t lni)
{
	rglist_p_t rgp = NULL;
	rlist_p_t rp = NULL;
	char *r_name;
	scha_errmsg_t err = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	boolean_t need_launch = B_FALSE;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);

	rgp = rgname_to_rg(en_name);
	CL_PANIC(rgp != NULL);

	// Re-read CCR & set flag only if lni is ZC or global zone
	if (lnNode->isZcOrGlobalZone()) {
		err = rgm_update_rg(en_name);
		if (err.err_code != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RG_TAG, en_name);
			//
			// SCMSGS
			// @explanation
			// Rgmd failed to read updated resource group from the
			// CCR on this node.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, SYSTEXT(
			    "fatal: Resource group <%s> update failed "
			    "with error <%d>; aborting node"), en_name,
			    err.err_code);
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHED
		} else {
			ucmm_print("RGM", NOGET("RG <%s> update succeeded\n"),
			    en_name);
			if (flags & rgm::INTENT_READ_RS) {
				for (rp = rgp->rgl_resources; rp != NULL;
				    rp = rp->rl_next) {
					r_name = strdup(
					    rp->rl_ccrdata->r_name);
					err = rgm_update_r(r_name, en_name);
					if (err.err_code != SCHA_ERR_NOERR) {
						(void)
						    sc_syslog_msg_initialize(
						    &handle,
						    SC_SYSLOG_RGM_RS_TAG,
						    r_name);
						//
						// SCMSGS
						// @explanation
						// Rgmd failed to read updated
						// resource from the CCR on
						// this node.
						// @user_action
						// Save a copy of the
						// /var/adm/messages files on
						// all nodes, and of the rgmd
						// core file. Contact your
						// authorized Sun service
						// provider for assistance in
						// diagnosing the problem.
						//
						(void) sc_syslog_msg_log(
						    handle, LOG_ERR, MESSAGE,
						    SYSTEXT("fatal: Resource "
						    "<%s> update failed with "
						    "error <%d>; aborting "
						    "node"), r_name,
						    err.err_code);
						sc_syslog_msg_done(&handle);
						abort();
					}
					ucmm_print("intention_update_rg()",
					    NOGET("Re-read RS <%s> From CCR"),
					    r_name);
					free(r_name);
				}
			}
		}

		if (flags & rgm::INTENT_OK_FLAG) {
			ucmm_print("intention_update_rg()",
			    NOGET("rgl_ok_to_start of RG <%s> is set "
			    "to TRUE.\n"), en_name);

			rgp->rgl_ok_to_start = B_TRUE;
		}
	}

	//
	// Exit if we're not supposed to run update, init, or fini.
	//
	if (!((flags & rgm::INTENT_RUN_UPDATE) ||
	    (flags & rgm::INTENT_ACTION) || (flags & rgm::INTENT_RUN_FINI))) {
		ucmm_print("intention_update_rg()",
		    NOGET("Not running update, init, or fini\n"));
		return;
	}

	//
	// Return if the zone is down.
	// Wake the president to let it know we are finished.
	//
	if (!lnNode->isUp()) {
		ucmm_print("intention_update_rg()",
		    NOGET("Zone is down; wake pres."));
		(void) wake_president(lni);
		return;
	}

	ucmm_print("intention_update_rg()", NOGET("No of R's in the RG %d.\n"),
	    rgp->rgl_total_rs);

	//
	// We iterate over all the Resources in the RG
	// and set their states appropriately if they need
	// to run UPDATE, INIT, or FINI.
	//
	// In order to run UPDATE, the INTENT_RUN_UPDATE flag must
	// be set, the resource must have the UPDATE method registered,
	// and the state of the R on the local lni is ONLINE,
	// ONLINE_UNMON or MON_FAILED. Otherwise this is not
	// Dynamic Update. Note that we are only interested in
	// the state on the local lni here, as the intention
	// will be processed on all the zones and each zone
	// will take appropriate action based on the R state
	// on that zone.
	//
	// In order to run INIT, the INTENT_ACTION flag must be set and
	// the resource must have INIT registered.
	//
	// In order to run FINI, the INTENT_RUN_FINI flag must be set and
	// the resource must have FINI registered.
	//
	// NOTE: Running INIT and UPDATE are mutually exclusive because
	// there's no way that the resource on which we run INIT could
	// already be online and need to run UPDATE. In fact, if we're running
	// INIT, this zone was just added to the nodelist, so the RG must be
	// offline as well.
	//
	// FINI and UPDATE are also mutually exclusive, because the RG must
	// be offline for a zone to be removed from the nodelist.
	//
	// FINI and INIT are mutually exclusive, because a zone can't be
	// added and removed at the same time.
	//
	for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		if ((flags & rgm::INTENT_RUN_UPDATE) &&
		    r_check_and_set_state(rp, lni)) {
			need_launch = B_TRUE;
		} else if ((flags & rgm::INTENT_ACTION) &&
		    is_method_reg(rp->rl_ccrtype, METH_INIT, NULL)) {
			// need to run state machine
			need_launch = B_TRUE;

			//
			// The RG must be offline because this zone was just
			// added to the nodelist.
			//
			CL_PANIC(rgp->rgl_xstate[lni].rgx_state ==
			    rgm::RG_OFFLINE ||
			    rgp->rgl_xstate[lni].rgx_state ==
			    rgm::RG_OFF_BOOTED ||
			    rgp->rgl_xstate[lni].rgx_state ==
			    rgm::RG_OFFLINE_START_FAILED ||
			    rgp->rgl_xstate[lni].rgx_state ==
			    rgm::RG_OFF_PENDING_METHODS);
			ucmm_print("RGM", NOGET("setting pend init\n"));
			// set RG state to OFF_PENDING_METHODS
			set_rgx_state(lni, rgp,
			    rgm::RG_OFF_PENDING_METHODS);
			// set R state to PENDING_INIT
			set_rx_state(lni, rp, rgm::R_PENDING_INIT);
		} else if ((flags & rgm::INTENT_RUN_FINI) &&
		    is_method_reg(rp->rl_ccrtype, METH_FINI, NULL)) {
			// need to run state machine
			need_launch = B_TRUE;

			//
			// The RG must be offline because this zone was just
			// removed from the nodelist.
			//
			CL_PANIC(rgp->rgl_xstate[lni].rgx_state ==
			    rgm::RG_OFFLINE ||
			    rgp->rgl_xstate[lni].rgx_state ==
			    rgm::RG_OFF_BOOTED ||
			    rgp->rgl_xstate[lni].rgx_state ==
			    rgm::RG_OFFLINE_START_FAILED ||
			    rgp->rgl_xstate[lni].rgx_state ==
			    rgm::RG_OFF_PENDING_METHODS);
			ucmm_print("RGM", NOGET("setting pend fini\n"));
			// set RG state to OFF_PENDING_METHODS
			set_rgx_state(lni, rgp,
			    rgm::RG_OFF_PENDING_METHODS);
			// set R state to PENDING_FINI
			set_rx_state(lni, rp, rgm::R_PENDING_FINI);
		}
	}

	ucmm_print("intention_update_rg()", NOGET("RG <%s> new state %s.\n"),
	    rgp->rgl_ccr->rg_name,
	    pr_rgstate(rgp->rgl_xstate[lni].rgx_state));

	if (need_launch) {
		// call run_state in new thread
		ucmm_print("RGM", NOGET("RGM_OP_UPDATE run new thread\n"));
		run_state_machine(lni, "RGM_OP_UPDATE");
	} else {
		// An intention is latched on all nodes,
		// but all nodes may not end up having to
		// launch methods.  Therefore we wake the
		// President node here to let it know
		// we are finished.
		ucmm_print("RGM", NOGET("UPDATE no need to spin off thread; "
		    "wake pres\n"));
		(void) wake_president(lni);
	}
}

//
// intention_update_rs
//
// No flags are expected.
//
// The lni indicates the zone currently being processed.
// We read the new RG CCR structure into memory only if lni indicates
// a ZC or global zone -- thus, we do it only once per physical node.
// However we may run UPDATE methods in any zone.
//
static void
intention_update_rs(char *en_name, rgm::intention_flag_t flags, rgm::lni_t lni)
{
	rlist_p_t rp = NULL;
	rglist_p_t rgp = NULL;
	rgm_rt_t *rt = NULL;
	scha_errmsg_t err = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	boolean_t need_launch = B_FALSE;	// need to launch
	char *rg_name;

	rp = rname_to_r(NULL, en_name);
	CL_PANIC(rp != NULL);

	rgp = rp->rl_rg;
	rg_name = rgp->rgl_ccr->rg_name;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);

	// Re-read CCR only if lni is ZC or global zone
	if (lnNode->isZcOrGlobalZone()) {
		err = rgm_update_r(en_name, rg_name);
		if (err.err_code != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RS_TAG, en_name);
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, SYSTEXT("fatal: Resource <%s> update "
			    "failed with error <%d>; aborting node"), en_name,
			    err.err_code);
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHED
		}
		ucmm_print("RGM", NOGET("R <%s> update succeeded\n"), en_name);

		// We need to update the pointer to the resource type
		// stored in the resource in-memory structure.
		// This is only required for RT upgrade, but we do in
		// all updates.
		// Also, notice that rl_ccrtype is a pointer to another
		// in-memory structure and hence doesn't need to be
		// freed; we simply overwrite the pointer.
		rt = rtname_to_rt(rp->rl_ccrdata->r_type);
		CL_PANIC(rt != NULL);
		rp->rl_ccrtype = rt;

		ucmm_print("intention_update_rs()",
		    NOGET("Re-read RS <%s> From CCR"), en_name);
	}

	if (lnNode->isUp()) {

		//
		// Run the update method only if the update flag was
		// set.
		//

		if (flags & rgm::INTENT_RUN_UPDATE) {
			need_launch = r_check_and_set_state(rp, lni);
		}
	} else {
		ucmm_print("intention_update_rs()", NOGET("Zone is down."));
		need_launch = B_FALSE;
	}

	ucmm_print("intention_update_rs()",
	    NOGET("RG <%s> new state %s.\n"), rgp->rgl_ccr->rg_name,
	    pr_rgstate(rgp->rgl_xstate[lni].rgx_state));

	if (need_launch) {
		// call run_state in new thread
		ucmm_print("RGM", NOGET("RGM_OP_UPDATE run new thread\n"));
		run_state_machine(lni, "RGM_OP_UPDATE");
	} else {
		// An intention is latched on all nodes,
		// but all nodes may not end up having to
		// launch methods.  Therefore we wake the
		// President node here to let it know
		// we are finished.
		ucmm_print("RGM", NOGET("UPDATE no need to spin off thread; "
		    "wake pres\n"));
		(void) wake_president(lni);
	}
}

//
// The RT update intention is used both when the RT_System property and
// the RT_Installed_nodes are updated.
//
// The flag INTENT_SYSTEM_ONLY says that only the RT_system property was
// updated. The flag INTENT_ACTION means that this node should run
// INIT on all resources of this type.
//
// The flag INTENT_RUN_FINI means that this node should run FINI on all
// resources of this type.
//
// The lni indicates the zone currently being processed.
// We read the new RT CCR structure into memory only if lni indicates
// a ZC or global zone -- thus, we do it only once per physical node.
// However we may run INIT or FINI methods in any zone.
//
void
intention_update_rt(char *en_name, rgm::intention_flag_t flags, rgm::lni_t lni)
{
	scha_errmsg_t err = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	boolean_t need_launch = B_FALSE;	// need to launch
	boolean_t set_this_rg_pending = B_FALSE;

	rglist_p_t rgp = NULL;
	rgm_rt_t *rt = NULL;
	rlist_p_t rp = NULL;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);

	// Re-read CCR only if lni is ZC or global zone
	if (lnNode->isZcOrGlobalZone()) {
		err = rgm_update_rt(en_name);
		if (err.err_code != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RT_TAG, en_name);
			//
			// SCMSGS
			// @explanation
			// Rgmd failed to read updated resource type from the
			// CCR on this node.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("fatal: Resource type <%s> update failed "
			    "with error <%d>; aborting node"), en_name,
			    err.err_code);
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHED

		} else {
			ucmm_print("RGM", NOGET("RT <%s> update succeeded\n"),
			    en_name);
		}
	}

	//
	// If we're only updating the system property, just return.
	//
	if (flags & rgm::INTENT_SYSTEM_ONLY) {
		return;
	}

	rt = rtname_to_rt(en_name);
	CL_PANIC(rt != NULL);

	//
	// If we're not supposed to take any action (INTENT_ACTION and
	// INTENT_RUN_FINI are not set), then we can return immediately.
	//
	if (!((flags & rgm::INTENT_ACTION) || (flags & rgm::INTENT_RUN_FINI))) {
		return;
	}

	//
	// Return if the zone is down.
	//
	if (!lnNode->isUp()) {
		ucmm_print("intention_update_rt()", NOGET("Zone is down."));
		return;
	}

	//
	// Iterate through all Rs in all RG and check if
	// we need to launch INIT or FINI on the Rs of this type.
	//
	// If INTENT_ACTION is set, since this node is being added to
	// RT_installed_nodes, and resource rp is of this
	// type, therefore (by the rule enforced by the
	// RGM) this node cannot be a member of the RG's
	// Nodelist.  The Nodelist must be a subset of the
	// Installed_nodes of all contained resources.
	//
	// Therefore, the RG must be OFFLINE on the
	// newly-added nodes.  (Note, the president already
	// does a check to make sure that the RG is in an
	// endish state and is not running BOOT methods).
	//
	// If INTENT_RUN_FINI is set, by similar logic, any RGs containing
	// Rs of this RT must be OFFLINE.
	//
	need_launch = B_FALSE;
	for (rgp = Rgm_state->rgm_rglist; rgp != NULL;
	    rgp = rgp->rgl_next) {
		set_this_rg_pending = B_FALSE;

		if (rgp->rgl_ccr->rg_unmanaged)
			continue;	// skip unmanaged rg

		for (rp = rgp->rgl_resources; rp != NULL;
		    rp = rp->rl_next) {
			// Only the resources of this rt type
			// need to run INIT or FINI method.
			if (strcmp(rp->rl_ccrtype->rt_name, en_name) != 0)
				continue;

			if (flags & rgm::INTENT_ACTION) {
				set_rx_state(lni, rp,
				    rgm::R_PENDING_INIT);
				set_this_rg_pending = B_TRUE;
			} else if (flags & rgm::INTENT_RUN_FINI) {
				set_rx_state(lni, rp,
				    rgm::R_PENDING_FINI);
				set_this_rg_pending = B_TRUE;
			}
		}

		if (set_this_rg_pending) {
			// Need to run state machine
			need_launch = B_TRUE;
			CL_PANIC(rgp->rgl_xstate[lni].
			    rgx_state == rgm::RG_OFFLINE ||
			    rgp->rgl_xstate[lni].rgx_state
			    == rgm::RG_OFF_BOOTED ||
			    rgp->rgl_xstate[lni].rgx_state
			    == rgm::RG_OFFLINE_START_FAILED);
			set_rgx_state(lni, rgp,
			    rgm::RG_OFF_PENDING_METHODS);
		}
	}

	if (need_launch) {
		// Request the state machine to run.
		run_state_machine(lni, "RGM_OP_UPDATE");
	}
}

//
// intention_create_rg
//
// No flags are expected.
//
// This is not called per-zone; just once per physical node.
//
void
intention_create_rg(char *en_name, rgm::intention_flag_t flags)
{
	scha_errmsg_t err = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	rglist_p_t rgp = NULL;

	err = rgm_add_rg(en_name, &rgp);
	CL_PANIC(err.err_code == SCHA_ERR_NOERR);
	CL_PANIC(rgp != NULL);
	ucmm_print("intention_create_rg()",
	    NOGET("Read RG <%s> From CCR"), en_name);

	//
	// Convert the affinities list from the ccr format
	// to a usable in-memory format.
	//
	err = convert_affinities_list(en_name, rgp->
	    rgl_ccr->rg_affinities, &(rgp->rgl_affinities));
	if (err.err_code != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RG_TAG, en_name);
		//
		// SCMSGS
		// @explanation
		// Rgmd failed to read new resource group from the CCR on this
		// node.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes,
		// and of the rgmd core file. Contact your authorized Sun
		// service provider for assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, SYSTEXT(
			    "fatal: Resource group <%s> create failed "
				"with error <%s>; aborting node"),
		    en_name, err.err_msg ? err.err_msg : "");
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHED
	}

	// Update 'rgl_affinitents' info for other RGs
	//
	// Because this RG is newly created, no other RG
	// should have it in its current rgl_affinitents
	// relation.  Thus, we only need to add this
	// RG to the rgl_affinitents relation of all
	// RGs for which this RG has affinities.
	//
	err = set_affinitents_for_resource_group(rgp);
	if (err.err_code != SCHA_ERR_NOERR) {
		ucmm_print("intention_create_rg()", NOGET(
			"set_affinitents_for_resource_group <%s> "
			    "failed: <%d>\n"), en_name, err.err_code);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RG_TAG, en_name);
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, SYSTEXT(
			    "fatal: Resource group <%s> create failed "
				"with error <%s>; aborting node"),
		    en_name, err.err_msg ? err.err_msg : "");
		sc_syslog_msg_done(&handle);
		abort();
				// NOTREACHED
	}

	debug_print_affinities();
}

//
// intention_create_rs
//
// The INTENT_ACTION flag specifies that this node should run INIT.
//
// The lni indicates the zone currently being processed.
// We read the new RT CCR structure into memory only if lni indicates
// a ZC or global zone -- thus, we do it only once per physical node.
// However we may run INIT methods in any zone.
//
void
intention_create_rs(char *en_name, rgm::intention_flag_t flags, rgm::lni_t lni)
{
	scha_errmsg_t err = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;

	rglist_p_t rgp = NULL;
	rlist_p_t rp = NULL;

	char *rg_name = NULL;
	rgm::rgm_rg_state orig_rgstate;

	//
	// The new created r is not in memory yet.
	// Use rgm_rsrc_on_which_rg() to find out which
	// RG contains r
	//
	// Note: The call to rgm_rsrc_on_which_rg() opens and
	// reads multiple CCR tables, which is inefficient.
	// It could be avoided if we extend the intention structure
	// to include the RG name.  Then we could directly call rgname_to_rg().
	// This is filed as CR# 6343562.
	//
	err = rgm_rsrc_on_which_rg(en_name, &rg_name, ZONE);
	CL_PANIC(err.err_code == SCHA_ERR_NOERR);
	rgp = rgname_to_rg(rg_name);
	CL_PANIC(rgp != NULL);
	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);

	// Re-read CCR only if lni is ZC or global zone
	if (lnNode->isZcOrGlobalZone()) {
		ucmm_print("intention_create_rs()",
		    NOGET("Read RS <%s> From CCR\n"), en_name);
		CL_PANIC(rgm_add_r(en_name, rg_name, NULL).err_code ==
		    SCHA_ERR_NOERR);
	}

	//
	// The reason we are removing the pingpong history
	// here as well as when a resource is removed is because
	// a resource can be deleted when the CCR file of the
	// containing RG is removed in non-cluster mode, in such
	// case, the pingpong history files will still remain.
	//
	remove_pingpong_history(en_name, lni);

	// Lookup the resource by name.
	rp = rname_to_r(rgp, en_name);

	//
	// Update of rl_dependents need be done only once per physical
	// node, so do it only for a ZC or global zone.
	//

	if (lnNode->isZcOrGlobalZone()) {
		ucmm_print("intention_create_rs()",
		    NOGET("Before set_rl_deps...: rname=<%s>\n"),
		    rp->rl_ccrdata->r_name);

		// Update 'rl_dependents' info
		//
		// No resources can yet depend on this resource,
		// so we are only concerned with the r_dependencies
		// of this (newly created) resource. Also, no
		// r_dependencies can have been deleted for this
		// resource, only new ones created.
		//
		err = set_rl_dependents_for_resource(rp);
		if (err.err_code != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RS_TAG, en_name);
			//
			// SCMSGS
			// @explanation
			// Rgmd failed to read new resource from the CCR on
			// this node.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, SYSTEXT("fatal: Resource <%s> create "
			    "failed with error <%d>; aborting node"), en_name,
			    err.err_code);
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHED
		}
	}

	// If RG is UNMANAGED, return
	if (rgp->rgl_ccr->rg_unmanaged) {
		ucmm_print("intention_create_rs()",
		    NOGET("RG is UNMANAGED - %s\n"), rg_name);
		free(rg_name);
		return;
	}

	free(rg_name);

	//
	// Check whether we need to run the init method.
	// If no action is specified, just return.
	//
	if (!(flags & rgm::INTENT_ACTION)) {
		ucmm_print("RGM",
		    NOGET("I'm not supposed to run init\n"));
		return;
	}

	//
	// Return if the zone is down.
	// Wake the president to let it know we are finished.
	//
	if (!lnNode->isUp()) {
		ucmm_print("intention_create_rs()",
		    NOGET("Zone is down; wake pres."));
		(void) wake_president(lni);
		return;
	}

	orig_rgstate = rgp->rgl_xstate[lni].rgx_state;

	switch (orig_rgstate) {
	case rgm::RG_ONLINE:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
		//
		// set RG state to ON_PENDING_METHODS
		//
		// We treat ONLINE and PENDING_ONLINE_BLOCKED
		// identically here.  The state machine will
		// return the RG state to the correct state
		// after the methods complete.
		//
		set_rgx_state(lni, rgp, rgm::RG_ON_PENDING_METHODS);
		break;

		// Note state will revert to OFFLINE after methods
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_OFFLINE_START_FAILED:
	case rgm::RG_OFFLINE:
		// set RG state to OFF_PENDING_METHODS
		set_rgx_state(lni, rgp, rgm::RG_OFF_PENDING_METHODS);
		break;

	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_ON_PENDING_METHODS:
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_BOOT:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_PENDING_OFF_START_FAILED:
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
	case rgm::RG_ON_PENDING_R_RESTART:
		// all these states should be ruled out by pres.
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RS_TAG, en_name);
		//
		// SCMSGS
		// @explanation
		// While creating or deleting a resource, the rgmd discovered
		// the containing resource group to be in an unexpected state
		// on the local node. As a result, the rgmd did not run the
		// INIT or FINI method (as indicated in the message) on that
		// resource on the local node. This should not occur, and may
		// indicate an internal logic error in the rgmd.
		// @user_action
		// The error is non-fatal, but it may prevent the indicated
		// resource from functioning correctly on the local node. Try
		// deleting the resource, and if appropriate, re-creating it.
		// If those actions succeed, then the problem was probably
		// transitory. Since this problem may indicate an internal
		// logic error in the rgmd, save a copy of the
		// /var/adm/messages files on all nodes, and the output of
		// clresourcegroup status +, clresourcetype show -v and
		// clresourcegroup show -v +. Report the problem to your
		// authorized Sun service provider.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, SYSTEXT("resource group <%s> in illegal state "
			"<%s>, will not run %s on resource <%s>"),
		    rgp->rgl_ccr->rg_name,
		    pr_rgstate(orig_rgstate), "INIT", en_name);
		sc_syslog_msg_done(&handle);
		// Wake pres to make sure it won't be stuck in cond_wait
		(void) wake_president(lni);
		return;
	}

	// set R state to PENDING_INIT
	set_rx_state(lni, rp, rgm::R_PENDING_INIT);

	// Request the state machine to run.
	run_state_machine(lni, "RGM_OP_CREATE");
}


#ifdef FUTURE
//
// intention_create_rt
//
// No flags expected.
//
// This function is currently unused, since RT creation is handled
// directly by scrgadm or clresourcetype.
//
void
intention_create_rt(char *en_name, rgm::intention_flag_t flags)
{
	scha_errmsg_t	err = {SCHA_ERR_NOERR, NULL};
	rgm_rt_t	*rtp;

	err = rgm_add_rt((char *)en_name, &rtp);
	CL_PANIC(err.err_code == SCHA_ERR_NOERR);
}
#endif	// FUTURE

//
// intention_delete_rg
//
// No flags expected.
//
// This is not called per-zone; just once per physical node.
//
void
intention_delete_rg(char *en_name, rgm::intention_flag_t flags)
{
	ucmm_print("intention_delete_rg()",
	    NOGET("Destroy RG <%s> From Memory"), en_name);

	CL_PANIC(rgm_delete_rg(en_name).err_code == SCHA_ERR_NOERR);
}

//
// intention_delete_rs
//
// The INTENT_ACTION flag specifies that this node should run FINI.
//
// Return true if the resource is already gone from memory.
// Otherwise, return false.
//
bool
intention_delete_rs(char *en_name, rgm::intention_flag_t flags, rgm::lni_t lni)
{
	sc_syslog_msg_handle_t handle;

	rglist_p_t rgp = NULL;
	rlist_p_t rp = NULL;

	rgm::rgm_rg_state orig_rgstate;

	rp = rname_to_r(NULL, en_name);

	//
	// Resource is already gone from memory; do nothing.
	//
	// We are called first on the global or ZC zone, then (in a base
	// cluster) on the nonglobal zones.
	// By returning true we tell the caller to return early without
	// processing the nonglobal zones.
	//
	if (rp == NULL) {
		ucmm_print("intention_delete_rs()",
		    NOGET("RS <%s> already removed from memory"), en_name);
		return (true);
	}

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);

	// Remove the pingpong history files.  This is called for each zone.
	remove_pingpong_history(en_name, lni);

	rgp = rp->rl_rg;

	//
	// We have to sync up the RG sequence number since
	// when the resource has been removed from CCR, the
	// sequence number is changed.
	//
	// This need be done only once per physical node, so we do it
	// only for a ZC or global zone.
	//
	if (lnNode->isZcOrGlobalZone()) {
		(void) update_rg_seq_id(rgp);
	}

	//
	// Return if the zone is down.
	// Wake the president to let it know we are finished.
	//
	if (!lnNode->isUp()) {
		ucmm_print("intention_delete_rs()",
		    NOGET("Zone is down; wake pres."));
		(void) wake_president(lni);
		return (false);
	}

	//
	// Check whether we need to run the fini method in this zone.
	//
	// If the action is not specified, or if the R is already UNINITED,
	// no need to run FINI.
	//
	// We don't delete the r from memory until we have processed
	// all zones on this node -- so we do it in unlatch_intention_helper().
	//
	if (!(flags & rgm::INTENT_ACTION) ||
	    rp->rl_xstate[lni].rx_state == rgm::R_UNINITED) {
		//
		// If the R is UNINITED, set it to OFFLINE and wake the
		// president.  This gives set_rx_state() a chance to delete
		// the resource from memory if all FINI methods have completed;
		// and it wakes idl_scrgadm_rs_delete() from its cond_wait.
		//
		if (rp->rl_xstate[lni].rx_state == rgm::R_UNINITED) {
			set_rx_state(lni, rp, rgm::R_OFFLINE);
			(void) wake_president(lni);
		}
		ucmm_print("intention_delete_rs",
		    NOGET("RS %s lni %d Either action not specified "
			"or R is already uninited"), en_name, lni);
		return (false);
	}

	//
	// Note if fini method is not registered, INTENT_ACTION flag
	// would not be set, so we wouldn't get here.
	//
	// Invoke the FINI method; rgm_run_state will wake pres.
	// Note, if this node dies, cmm will wake president.
	//

	orig_rgstate = rgp->rgl_xstate[lni].rgx_state;

	switch (orig_rgstate) {
	case rgm::RG_ONLINE:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
		//
		// set RG state to ON_PENDING_METHODS
		//
		// We treat ONLINE and PENDING_ONLINE_BLOCKED
		// identically here.  The state machine will
		// return the RG state to the correct state
		// after the methods complete.
		//
		set_rgx_state(lni, rgp, rgm::RG_ON_PENDING_METHODS);
		break;

		// Note state will revert to OFFLINE after methods
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_OFFLINE_START_FAILED:
	case rgm::RG_OFFLINE:
	case rgm::RG_OFF_PENDING_BOOT:
		// set RG state to OFF_PENDING_METHODS
		set_rgx_state(lni, rgp, rgm::RG_OFF_PENDING_METHODS);
		break;

	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_ON_PENDING_METHODS:
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_PENDING_OFF_START_FAILED:
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
	case rgm::RG_ON_PENDING_R_RESTART:
		// all these states should be ruled out by pres.
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RS_TAG, en_name);
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, SYSTEXT("resource group <%s> in illegal state "
			"<%s>, will not run %s on resource <%s>"),
		    rgp->rgl_ccr->rg_name, pr_rgstate(orig_rgstate),
		    "FINI", en_name);
		sc_syslog_msg_done(&handle);
		// Wake pres to make sure it won't be stuck in cond_wait
		(void) wake_president(lni);
		return (false);
	}

	// set R state to PENDING_FINI
	set_rx_state(lni, rp, rgm::R_PENDING_FINI);

	//
	// Set the deleted flag in slave's memory.  This flag indicates
	// that fini methods are being run for this resource on this slave.
	// The r structure will not be removed from this node's memory
	// until fini has finished running on all relevant zones on this slave.
	// Note: r has been removed from CCR by the president.
	// Later this flag will be checked in launch_method when
	// fini returns.  If it is set, r is removed from memory.
	//
	rp->rl_is_deleted = B_TRUE;

	// Request the state machine to run.
	run_state_machine(lni, "RGM_OP_DELETE");

	return (false);
}

//
// intention_delete_rt
//
// No flags expected.
//
// This is not called per-zone; just once per physical node.
//
void
intention_delete_rt(char *en_name)
{
	// remove RT from internal state
	ucmm_print("intention_delete_rt()",
	    NOGET("Destroy RT <%s> From Memory"), en_name);
	CL_PANIC(rgm_delete_rt(en_name).err_code == SCHA_ERR_NOERR);
}

//
// Intention to manage the RG
//
// INTENT_MANAGE_RG flag should always be set
// Meaning: RG is being managed.
//
void
intention_manage(char *en_name, rgm::intention_flag_t flags, rgm::lni_t lni)
{
	scha_errmsg_t err = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	boolean_t need_launch = B_FALSE;	// need to launch

	rglist_p_t rgp = NULL;
	rgm_rt_t *rt = NULL;
	rlist_p_t rp = NULL;

	LogicalNodeset *nodes = NULL;

	// get pointer to RG in state structure
	rgp = rgname_to_rg(en_name);
	CL_PANIC(rgp != NULL);
	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);

	if (flags & rgm::INTENT_MANAGE_RG) {
		//
		// For moving RG to managed state.
		// Re-read the CCR to get the updated sequence id
		// and the new value of rg_unmanaged.
		// This need be done only once per physical node, so it
		// is done only for a ZC or global zone.
		//
		if (lnNode->isZcOrGlobalZone()) {
			err = rgm_update_rg(en_name);
			if (err.err_code != SCHA_ERR_NOERR) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RG_TAG, en_name);
				//
				// SCMSGS
				// @explanation
				// Rgmd failed to read updated resource from
				// the CCR on this node.
				// @user_action
				// Save a copy of the /var/adm/messages files
				// on all nodes, and of the rgmd core file.
				// Contact your authorized Sun service
				// provider for assistance in diagnosing the
				// problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    SYSTEXT("fatal: Got error <%d> trying to "
				    "read CCR when making resource group <%s> "
				    "managed; aborting node"),
				    err.err_code, en_name);
				sc_syslog_msg_done(&handle);
				abort();
				// NOTREACHED
			}
		}
	} else {
		// should always be managing the RG
		CL_PANIC(false);
	}

	//
	// Return if the zone is down.
	// Wake the president to let it know we are finished.
	//
	if (!lnNode->isUp()) {
		ucmm_print("intention_manage()",
		    NOGET("Zone is down; wake pres."));
		(void) wake_president(lni);
		return;
	}

	// Iterate through all Rs in the RG and
	// determine whether we need to launch INIT
	// on the R.
	need_launch = B_FALSE;
	for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		ucmm_print("RGM", NOGET("in r loop\n"));

		rt = rp->rl_ccrtype;

		// For bringing RG to managed state -
		// Note: If an init method is not registered,
		// compute_method_nodes will return empty
		// nodelist
		// Compute whether we need to run the init
		// method.

		nodes = compute_meth_nodes(rt, rgp->rgl_ccr,
		    METH_INIT, is_r_scalable(rp->rl_ccrdata));

		char *ucmm_buffer = NULL;
		if (nodes->display(ucmm_buffer) == 0) {
			ucmm_print("RGM",
			    NOGET("intention_manage: nodes is %s\n"),
			    ucmm_buffer);
			free(ucmm_buffer);
		}


		// If I'm not in the list of nodes, continue on
		// to the next resource.

		if (!nodes->containLni(lni))
			continue;

		delete (nodes);

		// need to run state machine
		need_launch = B_TRUE;

		//
		// The RG must be offline
		//
		ucmm_print("RGM", NOGET("MANAGE setting pend init\n"));
		// set RG state to OFF_PENDING_METHODS
		set_rgx_state(lni, rgp, rgm::RG_OFF_PENDING_METHODS);
		// set R state to PENDING_INIT
		set_rx_state(lni, rp, rgm::R_PENDING_INIT);
	}

	if (need_launch) {
		// Request the state machine to run.
		run_state_machine(lni, "RGM_OP_MANAGE");
	} else {
		//
		// An intention is latched on all nodes, but all nodes
		// may not end up having to launch methods.  Therefore
		// we awaken the President node here to let it know
		// we are finished.
		//
		ucmm_print("RGM", NOGET(
			"MANAGE no need to spin off thread; wake pres\n"));
		(void) wake_president(lni);
	}
}

//
// intention_unmanage
//
// Processes the RG UNMANAGE intention
// No flags expected.
//
void
intention_unmanage(char *en_name, rgm::intention_flag_t flags, rgm::lni_t lni)
{
	scha_errmsg_t err = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	rglist_p_t rgp = NULL;
	rlist_p_t rp = NULL;
	LogicalNodeset *nodes = NULL;
	boolean_t need_launch = B_FALSE;	// need to launch

	// get pointer to RG in state structure
	rgp = rgname_to_rg(en_name);
	CL_PANIC(rgp != NULL);
	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);

	//
	// Re-read the CCR to get the updated sequence id
	// and the new value of rg_unmanaged.
	//
	// This need be done only once per physical node, so do it only
	// for a ZC or global zone.
	//
	if (lnNode->isZcOrGlobalZone()) {
		err = rgm_update_rg(en_name);
		if (err.err_code != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RG_TAG, en_name);
			//
			// SCMSGS
			// @explanation
			// Rgmd failed to read updated resource from the CCR
			// on this node.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE,
			    SYSTEXT("fatal: Got error <%d> trying to read CCR"
			    " when making resource group <%s> unmanaged; "
			    "aborting node"), err.err_code, en_name);
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHED
		}

		// reinitialize RG state to unfrozen
		rgp->rgl_is_frozen = FROZ_FALSE;
	}

	//
	// Return if the zone is down.
	// Wake the president to let it know we are finished.
	//
	if (!lnNode->isUp()) {
		ucmm_print("intention_unmanage()",
		    NOGET("Zone is down; wake pres."));
		(void) wake_president(lni);
		return;
	}

	// Iterate through all Rs in the RG and
	// determine whether we need to launch FINI
	// on the R.
	for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		//
		// Compute whether we need to run the fini method on rp.
		//

		// If rp is already UNINITED, no need to run FINI
		if (rp->rl_xstate[lni].rx_state == rgm::R_UNINITED) {
			continue;
		}

		//
		// fininodes are only written/read in the "old" (pre-nodeidlist)
		// CCR table format.
		//
		// we do not use fininodes; instead, the FINI method
		// is executed on all nodes in Nodelist or Installed_nodes.
		// This might mean that FINI will be re-executed on a node on
		// which it was already executed.
		// This is harmless, due to the required idempotency of fini.
		//
		rgm_rt_t *rt = NULL;

		rt = rtname_to_rt(rp->rl_ccrdata->r_type);
		CL_PANIC(rt);
		nodes = compute_meth_nodes(rt, rgp->rgl_ccr, METH_FINI,
		    is_r_scalable(rp->rl_ccrdata));

		char *lnbuff = NULL;
		if (nodes->display(lnbuff) == 0) {
			ucmm_print("RGM",
			    NOGET("intention_unmanage: nodes is <%s>\n"),
			    lnbuff);
			free(lnbuff);
		}

		//
		// If I'm not in the list of nodes, continue on to
		// the next resource.
		//
		if (!nodes->containLni(lni)) {
			continue;
		}

		// need to run state machine
		need_launch = B_TRUE;
		ucmm_print("RGM", NOGET("UNMANAGE setting pend fini\n"));
		// set RG state to OFF_PENDING_METHODS
		set_rgx_state(lni, rgp, rgm::RG_OFF_PENDING_METHODS);
		// set R state to PENDING_FINI
		set_rx_state(lni, rp, rgm::R_PENDING_FINI);
	}
	delete (nodes);

	if (need_launch) {
		// Request the state machine to run.
		run_state_machine(lni, "RGM_OP_UNMANAGE");
	} else {
		//
		// An intention is latched on all nodes, but all nodes
		// may not end up having to launch methods.  Therefore
		// we awaken the President node here to let it know
		// we are finished.
		//
		ucmm_print("RGM", NOGET("UNMANAGE no need to spin off thread; "
		    "wake pres\n"));
		(void) wake_president(lni);
	}
}

void
swdump(std::map<rgm::lni_t, scha_switch_t> &s)
{
	std::map<rgm::lni_t, scha_switch_t>::iterator it;

	for (it = s.begin(); it != s.end(); it++) {
		ucmm_print("RGM", "Switch lni map rp_value[%d] = %d: ",
		    it->first, it->second);
	}
}

//
// updates the resource structure from the CCR data, and returns a nodeset
// containing all the lnis on the local physical node where on_off/monitored
// switch has been modified.
// onoff is true if r_onoff_switch is changed, and false if monitored_switch
// is changed
// enable is true if switch is being enabled and vice versa
//
LogicalNodeset *
intention_required(char *en_name, rlist_p_t rp, bool onoff, bool enable)
{

	char *rg_name = NULL;
	rglist_p_t rgp = NULL;
	std::map<rgm::lni_t, scha_switch_t> old_switch;
	std::map<rgm::lni_t, scha_switch_t> new_switch;
	LogicalNodeset *nset = new LogicalNodeset;
	scha_errmsg_t err = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;

	rgp = rp->rl_rg;
	CL_PANIC(rgp != NULL);
	rg_name = rgp->rgl_ccr->rg_name;
	CL_PANIC(rg_name != NULL);

	//
	// Store the old value of Onoff_switch/monitored_switch on all
	// the zones for the node, and compare the same after re-reading
	// the CCR. Return the nodeset containing lnis for all the zones
	// where the old and new value differ.
	//

	if (onoff) {
		old_switch = ((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->
		    r_switch;
	} else {
		old_switch = ((rgm_switch_t *)
		    rp->rl_ccrdata->r_monitored_switch)->
		    r_switch;
	}

	// Re-read the CCR to get the updated sequence id
	// and the new value of r_onoff_switch.
	err = rgm_update_r(en_name, rg_name);

	if (err.err_code != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RS_TAG, en_name);
		//
		// SCMSGS
		// @explanation
		// Rgmd failed to read updated resource from the CCR on this
		// node.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes,
		// and of the rgmd core file. Contact your authorized Sun
		// service provider for assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: Got error <%d> trying to read "
		    "CCR when <%s> <%s> resource <%s>; aborting node"),
		    err.err_code, enable ? "enabling" : "disabling",
		    onoff ? "" : "monitor of", en_name);
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHED
	}

	if (onoff) {
		new_switch = ((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->
		    r_switch;
	} else {
		new_switch = ((rgm_switch_t *)
		    rp->rl_ccrdata->r_monitored_switch)->
		    r_switch;
	}

	rgm::lni_t lni = get_local_nodeid();
	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);

	// compare for global or ZC zone
	if (old_switch[lni] != new_switch[lni] &&
	    nodeidlist_contains_lni(rgp->rgl_ccr->rg_nodelist, lni)) {
		nset->addLni(lni);
	}
	// compare for all non global base-cluster zones
	std::list<LogicalNode*>::const_iterator it;
	for (it = lnNode->getLocalZones().begin(); it !=
	    lnNode->getLocalZones().end(); ++it) {
		lni = (*it)->getLni();

		if (old_switch[lni] != new_switch[lni] &&
		    nodeidlist_contains_lni(rgp->rgl_ccr->rg_nodelist, lni)) {
			nset->addLni(lni);
		}
	}
	return (nset);
}


void
intention_enable_resource(char *en_name)
{
	rglist_p_t rgp = NULL;
	rlist_p_t rp = NULL;
	char *r_name = NULL;
	char *rg_name = NULL;
	LogicalNodeset *nodeset;
	rgm::lni_t lni = 0;

	rp = rname_to_r((rglist_t *)NULL, en_name);
	CL_PANIC(rp != NULL);
	rgp = rp->rl_rg;
	CL_PANIC(rgp != NULL);
	rg_name = rgp->rgl_ccr->rg_name;
	CL_PANIC(rg_name != NULL);
	r_name = rp->rl_ccrdata->r_name;

	ucmm_print("intention_enable_resource()", NOGET(
	    "R = <%s>, RG = <%s>"), r_name, rg_name);
	nodeset = intention_required(en_name, rp, B_TRUE, B_TRUE);

	//
	// Have to re-fetch because rgm_update_r in intention_required
	// changed the r_name
	//
	r_name = rp->rl_ccrdata->r_name;

	while ((lni = nodeset->nextLniSet(lni + 1)) != 0) {

		//
		// If the zone is up,
		// we check whether the RG is ONLINE or PENDING_ONLINE_BLOCKED
		// on the zone.  If it is, we run the state machine to
		// bring the enabled resource online.
		//
		// If we are enabling multiple resources in a single scswitch,
		// the group might already be in PENDING_ONLINE or
		// PENDING_ON_STARTED state on the zone.
		// In this case, there is no need to run the state machine
		// because it is already running.
		//
		// We don't have to check for other
		// "online-ish" RG states, because the enable is not
		// permitted if the RG is in any state other than ONLINE,
		// OFFLINE, or PENDING_ONLINE_BLOCKED.
		//
		LogicalNode *lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);
		if (lnNode->isUp() &&
		    (rgp->rgl_xstate[lni].rgx_state == rgm::RG_ONLINE ||
		    rgp->rgl_xstate[lni].rgx_state ==
		    rgm::RG_PENDING_ONLINE_BLOCKED)) {
			ucmm_print("intention_enable_resource()", NOGET(
			    "ENABLE R <%s> run on lni <%d>"), r_name, lni);
			//
			// set RG state to PENDING_ONLINE
			//
			// The state machine will return the RG state to
			// ONLINE or PENDING_ONLINE_BLOCKED depending on
			// the states of the resources.
			//
			set_rgx_state(lni, rgp, rgm::RG_PENDING_ONLINE);

			// The R should be in OFFLINE state.
			CL_PANIC(rp->rl_xstate[lni].rx_state == rgm::R_OFFLINE);

			ucmm_print("intention_enable_resource()", NOGET(
			    "RGM_OP_ENABLE_R run state machine on new "
			    "thread\n"));

			run_state_machine(lni, "RGM_OP_ENABLE_R");
		} else {
			//
			// If there is no change to the RG state, there is
			// no need to run state machine.
			//
			ucmm_print("intention_enable_resource()",
			    NOGET("enable R <%s> lni %d: no need to run "
			    "state machine"), r_name, lni);
		}
	}
	delete nodeset;
}

void
intention_disable_resource(char *en_name)
{
	rglist_p_t rgp = NULL;
	rlist_p_t rp = NULL;
	char *r_name = NULL;
	char *rg_name = NULL;
	LogicalNodeset *nodeset;
	rgm::lni_t lni = 0;

	rp = rname_to_r((rglist_t *)NULL, en_name);
	CL_PANIC(rp != NULL);
	rgp = rp->rl_rg;
	CL_PANIC(rgp != NULL);
	rg_name = rgp->rgl_ccr->rg_name;
	CL_PANIC(rg_name != NULL);
	r_name = rp->rl_ccrdata->r_name;

	ucmm_print("intention_disable_resource()", NOGET(
	    "R = <%s>, RG = <%s>"), r_name, rg_name);
	nodeset = intention_required(en_name, rp, B_TRUE, B_FALSE);

	//
	// Have to re-fetch because rgm_update_r in intention_required
	// changed the r_name
	//
	r_name = rp->rl_ccrdata->r_name;

	while ((lni = nodeset->nextLniSet(lni + 1)) != 0) {
		//
		// Resource rp has just been disabled.
		// If the zone is up, check the RG state on
		// the zone to see if we need to run the state machine.
		// We need to do so if the RG is ONLINE, PENDING_ONLINE_BLOCKED
		// or ERROR_STOP_FAILED (the latter state might occur if the
		// user is disabling multiple resources and one of the earlier
		// ones has gone stop_failed).
		//
		// If we are disabling multiple resources in a single scswitch,
		// the group might already be in ON_PENDING_DISABLED state on
		// the zone.  In this case, there is no need to run the
		// state machine because it is already running.  It cannot
		// have moved to a state other than those named above, because
		// the president holds its state lock while executing all of
		// the process_intention calls.
		//
		// We currently don't have to check for other "online-ish" RG
		// states, because the disable is not permitted if the RG is
		// in a non-endish state.
		// XXXX This assumption -- and code -- must change when
		// XXXX "force disable" (bugid 4274541) is implemented.
		//
		// We move the RG to ON_PENDING_DISABLED state.  The state
		// machine will move it back to ERROR_STOP_FAILED, ONLINE,
		// ON_PENDING_R_RESTART, or PENDING_ONLINE_BLOCKED after
		// stopping the disabled resource(s), depending on the states
		// of its resources.
		//
		LogicalNode *lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);
		if (lnNode->isUp() &&
		    (rgp->rgl_xstate[lni].rgx_state == rgm::RG_ONLINE ||
		    rgp->rgl_xstate[lni].rgx_state ==
		    rgm::RG_PENDING_ONLINE_BLOCKED ||
		    rgp->rgl_xstate[lni].rgx_state ==
		    rgm::RG_ERROR_STOP_FAILED)) {
			ucmm_print("intention_disable_resource()", NOGET(
			    "DISABLE R <%s> run on lni <%d>"), r_name, lni);

			//
			// We set RG state to ON_PENDING_DISABLED,
			// but there is no need to set R state.  The state
			// machine will attempt to stop the R from whatever
			// state it is currently in.
			//
			set_rgx_state(lni, rgp, rgm::RG_ON_PENDING_DISABLED);

			ucmm_print("intention_disable_resource()", NOGET(
			    "RGM_OP_DISABLE_R run state machine on new "
			    "thread\n"));

			run_state_machine(lni, "RGM_OP_DISABLE_R");
		} else {
			// If there is no change to the RG state, there is
			// no need to run state machine.
			ucmm_print("intention_disable_resource()", NOGET(
			    "disable R <%s> lni %d: no need to run state "
			    "machine"), r_name, lni);
		}
	}
	delete nodeset;
}

void
intention_enable_monitor(char *en_name)
{
	rglist_p_t rgp = NULL;
	rlist_p_t rp = NULL;
	char *r_name = NULL;
	char *rg_name = NULL;
	LogicalNodeset *nodeset;
	rgm::lni_t lni = 0;

	rp = rname_to_r((rglist_t *)NULL, en_name);
	CL_PANIC(rp != NULL);
	rgp = rp->rl_rg;
	CL_PANIC(rgp != NULL);
	rg_name = rgp->rgl_ccr->rg_name;
	CL_PANIC(rg_name != NULL);
	r_name = rp->rl_ccrdata->r_name;

	ucmm_print("inside intention_enable_monitor()", NOGET(
	    "R = <%s>, RG = <%s>"), r_name, rg_name);
	nodeset = intention_required(en_name, rp, B_FALSE, B_TRUE);

	//
	// Have to re-fetch because rgm_update_r in intention_required
	// changed the r_name
	//
	r_name = rp->rl_ccrdata->r_name;

	while ((lni = nodeset->nextLniSet(lni + 1)) != 0) {
		//
		// If the zone is up and
		// the current RG is ONLINE or PENDING_ONLINE_BLOCKED
		// and the R is ONLINE_UNMON on the
		// zone, we run the state machine to start the monitor.
		// Otherwise, no state machine needs to be run.
		//
		// Note that we don't need to handle R_JUST_STARTED, because
		// the RG would not be RG_ONLINE or PENDING_ONLINE_BLOCKED
		// in that case.
		//
		// Note that the state machine will return the RG to the
		// correct state depending on the states of the contained
		// resources.
		//
		// If we are enabling monitoring on multiple resources in a
		// single scswitch command, the group might already be in
		// PENDING_ONLINE or PENDING_ON_STARTED state on the zone.
		// In this case, there is no need to run the state machine
		// because it is already running.
		//
		// We don't have to check for other "online-ish" RG states,
		// because the enable is not permitted if the RG is in any
		// state other than ONLINE, OFFLINE, or PENDING_ONLINE_BLOCKED.
		//
		LogicalNode *lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);
		if (lnNode->isUp() &&
		    ((rgp->rgl_xstate[lni].rgx_state == rgm::RG_ONLINE ||
		    rgp->rgl_xstate[lni].rgx_state ==
		    rgm::RG_PENDING_ONLINE_BLOCKED) &&
		    (rp->rl_xstate[lni].rx_state == rgm::R_ONLINE_UNMON))) {
			ucmm_print("inside intention_enable_monitor()", NOGET(
			    "ENABLE R <%s> monitor run on lni <%d>"), r_name,
			    lni);

			// We set RG state to PENDING_ONLINE
			set_rgx_state(lni, rgp, rgm::RG_PENDING_ONLINE);

			ucmm_print("inside intention_enable_monitor()", NOGET(
			    "RGM_OP_ENABLE_M_R run state machine on new "
			    "thread\n"));

			run_state_machine(lni, "RGM_OP_ENABLE_M_R");
		} else {
			// If there is no change to the RG state, there is
			// no need to run state machine.
			ucmm_print("inside intention_enable_monitor()", NOGET(
			    "RGM_OP_ENABLE_M_R no need to run state machine"));
		}
	}
	delete nodeset;
}

void
intention_disable_monitor(char *en_name)
{
	rglist_p_t rgp = NULL;
	rlist_p_t rp = NULL;
	char *r_name = NULL;
	char *rg_name = NULL;
	LogicalNodeset *nodeset;
	rgm::lni_t lni = 0;

	rp = rname_to_r((rglist_t *)NULL, en_name);
	CL_PANIC(rp != NULL);
	rgp = rp->rl_rg;
	CL_PANIC(rgp != NULL);
	rg_name = rgp->rgl_ccr->rg_name;
	CL_PANIC(rg_name != NULL);
	r_name = rp->rl_ccrdata->r_name;

	ucmm_print("intention_disable_monitor()", NOGET(
	    "R = <%s>, RG = <%s>"), r_name, rg_name);
	nodeset = intention_required(en_name, rp, B_FALSE, B_FALSE);

	//
	// Have to re-fetch because rgm_update_r in intention_required
	// changed the r_name
	//
	r_name = rp->rl_ccrdata->r_name;

	while ((lni = nodeset->nextLniSet(lni + 1)) != 0) {

		//
		// If the zone is up and
		// the current RG is ONLINE or PENDING_ONLINE_BLOCKED or
		// ERROR_STOP_FAILED (the latter state might occur if the user
		// is disabling monitoring on multiple resources and one of
		// them goes stop_failed) on the zone and the R is
		// ONLINE or MON_FAILED, we run the state_machine to disable
		// monitor.  Otherwise, no state machine needs to be run.
		//
		// Note that the state machine will return the RG to the
		// correct state (ONLINE or PENDING_ONLINE_BLOCKED or
		// ERROR_STOP_FAILED etc.) depending on the states of the
		// contained resources.
		//
		// If we are disabling monitoring on multiple resources in
		// a single scswitch command, the group might already be in
		// ON_PENDING_MON_DISABLED state on
		// the zone.  In this case, there is no need to run the
		// state machine because it is already running.  It cannot
		// have moved to a state other than those named above, because
		// the president holds its state lock while executing all of
		// the process_intention calls.
		//
		// We currently don't have to check for other "online-ish" RG
		// states, because the disable is not permitted if the RG is
		// in a non-endish state.
		//
		LogicalNode *lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);
		if (lnNode->isUp() &&
		    ((rgp->rgl_xstate[lni].rgx_state == rgm::RG_ONLINE ||
		    rgp->rgl_xstate[lni].rgx_state ==
		    rgm::RG_PENDING_ONLINE_BLOCKED ||
		    rgp->rgl_xstate[lni].rgx_state ==
		    rgm::RG_ERROR_STOP_FAILED) &&
		    (rp->rl_xstate[lni].rx_state == rgm::R_ONLINE ||
		    rp->rl_xstate[lni].rx_state == rgm::R_MON_FAILED))) {
			ucmm_print("intention_disable_monitor()", NOGET(
			    "DISABLE R <%s> monitor run on node <%d>"), r_name,
			    lni);

			// We set RG state to ON_PENDING_MON_DISABLED
			set_rgx_state(lni, rgp,
			rgm::RG_ON_PENDING_MON_DISABLED);

			ucmm_print("intention_disable_monitor()", NOGET(
			    "RGM_OP_DISABLE_M_R run state machine "
			    "on new thread\n"));
			run_state_machine(lni, "RGM_OP_DISABLE_M_R");
		} else {
			// If there is no change to the RG state, there is
			// no need to run state machine.
			ucmm_print("intention_disable_monitor()", NOGET(
			    "RGM_OP_DISABLE_M_R no need to run state "
			    "machine"));
		}
	}
	delete nodeset;
}


//
// process_intention_helper():  Helper function for
// idl_process_intention(), which is called by the
// president to tell the slaves to read in the changes committed to CCR
// and/or to run methods like INIT, FINI, and UPDATE.
// This helper function is called once for each zone on the local node.
// On a global or ZC zone, it reads the CCR and runs any applicable methods.
// On base-cluster non-global zones, it only runs the methods (since CCR data
// is shared by all zones on the local node).
//
// If the president who originally committed the transaction to CCR died,
// we would be called by the "new" president. The new president
// would have already done the relevant checks in the CCR to make
// sure that the transaction actually got committed to CCR.
// If the transaction did not get committed to CCR, it would
// NOT call this function, rather it would simply unlatch the
// intention via idl_unlatch_intention() interface.
//
// This function is called for each cluster zone on the local node. It is
// possible that it will be called for a zone that didn't latch the intention
// because it was not a cluster member at the time the intention
// was latched by the president (this can happen if the president
// dies and a new node or zone joins at the same time).  This is OK;
// idl_process_intention() and idl_unlatch_intention() do nothing
// but return if the intention is not latched.
//
bool
process_intention_helper(rgm::lni_t lni)
{
	char *en_name = NULL;
	rgm::rgm_intention intent;
	bool return_early = false;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	slave_intention *Rgm_Intention = lnNode->getIntention();
	if (Rgm_Intention == NULL) {
		ucmm_print("process_intention_helper()",
		    NOGET("Nothing is latched\n"));
		//
		// Since unlatch_intention() operates on all zones on the node,
		// if the intention is unlatched on this zone it should be
		// unlatched on all zones.  So the caller can return early.
		//
		return (true);
	}

	if (Rgm_Intention->have_processed) {
		ucmm_print("process_intention_helper()",
			NOGET("Have Already processed intention\n"));
		//
		// Since process_intention() operates on all zones on the node,
		// if the intention is already processed on this zone it
		// should already be processed on all zones.  So the caller
		// can return early.
		//
		return (true);
	}

	intent = *(Rgm_Intention->intention);
	en_name = strdup(intent.idlstr_entity_name);

	switch (intent.operation_type) {

	case rgm::RGM_OP_UPDATE:

		switch (intent.entity_type) {
		case rgm::RGM_ENT_RG:
			intention_update_rg(en_name, intent.flags, lni);
			break;

		case rgm::RGM_ENT_RS:
			intention_update_rs(en_name, intent.flags, lni);
			break;

		case rgm::RGM_ENT_RT:
			intention_update_rt(en_name, intent.flags, lni);
			break;
		} // switch on entity type

		break;

	case rgm::RGM_OP_CREATE:

		switch (intent.entity_type) {
		case rgm::RGM_ENT_RG:
			//
			// This only reads from the CCR -- no methods are
			// executed.  Therefore, execute it only on a ZC or
			// global zone.
			//
			if (lnNode->isZcOrGlobalZone()) {
				intention_create_rg(en_name, intent.flags);
			}
			break;

		case rgm::RGM_ENT_RS:
			intention_create_rs(en_name, intent.flags, lni);
			break;

		case rgm::RGM_ENT_RT:
#ifdef FUTURE
			//
			// This only reads from the CCR -- no methods are
			// executed.  Therefore, execute it only on a ZC
			// or global zone.
			//
			// This case is currently unused and could be removed
			// from the code.
			//
			if (lnNode->isZcOrGlobalZone()) {
				intention_create_rt(en_name, intent.flags);
			}
#endif	// FUTURE
			break;

		} // switch on entity type

		break;

	case rgm::RGM_OP_DELETE:

		switch (intent.entity_type) {
		case rgm::RGM_ENT_RG:
			//
			// This only deletes the RG from memory -- no methods
			// are executed.  Therefore, execute it only on a
			// ZC or global zone.
			//
			if (lnNode->isZcOrGlobalZone()) {
				intention_delete_rg(en_name, intent.flags);
			}
			break;

		case rgm::RGM_ENT_RS:
			return_early =
			    intention_delete_rs(en_name, intent.flags, lni);
			break;

		case rgm::RGM_ENT_RT:
			//
			// This only deletes the RT from memory -- no methods
			// are executed.  Therefore, execute it only on a
			// ZC or global zone.
			//
			if (lnNode->isZcOrGlobalZone()) {
				intention_delete_rt(en_name);
			}
			break;
		} // switch on entity type

		break;

	case rgm::RGM_OP_MANAGE:
		intention_manage(en_name, intent.flags, lni);
		break;

	case rgm::RGM_OP_UNMANAGE:
		intention_unmanage(en_name, intent.flags, lni);
		break;

	case rgm::RGM_OP_ENABLE_R:
		intention_enable_resource(en_name);
		return_early = true;
		break;

	case rgm::RGM_OP_DISABLE_R:
		intention_disable_resource(en_name);
		return_early = true;
		break;

	case rgm::RGM_OP_ENABLE_M_R:
		intention_enable_monitor(en_name);
		return_early = true;
		break;

	case rgm::RGM_OP_DISABLE_M_R:
		intention_disable_monitor(en_name);
		return_early = true;
		break;

	case rgm::RGM_OP_SYNCUP:
	case rgm::RGM_OP_FORCE_ENABLE_R:
	case rgm::RGM_OP_FORCE_DISABLE_R:
	default:
		ucmm_print("idl_process_intention()",
		    NOGET("Unknown operation <%d> on entity <%s>"),
		    intent.operation_type, en_name);
		break;
	}

	// Record the fact that we have already handled the intention
	Rgm_Intention->have_processed = 1;

	if (en_name)
		free(en_name);

	return (return_early);
}

//
// idl_process_intention
// This function is called by the president on each physical member node.
// It invokes process_intention_helper() upon each zone on the local node.
// See comments for that function.
//
#ifdef EUROPA_FARM
bool_t
rgmx_process_intention_1_svc(
	rgmx_process_intention_args *rpc_args,
	void *,
	struct svc_req *)
#else
void
rgm_comm_impl::idl_process_intention(
	sol::nodeid_t president, sol::incarnation_num incarnation,
	Environment &)
#endif
{
	bool return_early;

#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
#endif

#ifndef EUROPA_FARM
	FAULT_POINT_FOR_ORPHAN_ORB_CALL("process_intention", president,
		incarnation);
#endif

	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		slave_unlock_state();
#ifdef EUROPA_FARM
		return (TRUE);
#else
		return;
#endif
	}

	//
	// Note: the code in this file assumes that the global zone is
	// processed before any of the non-global base-cluster zones.
	//
	//
	// Process intention for the global or ZC zone.
	//
	// If process_intention_helper returns true for the global or ZC zone,
	// the intention has already been processed on all zones on this
	// physical node, so we
	// return early without processing it on the local zones.
	// This can occur when a new president calls complete_intention(),
	// or in cases of on_off/monitored switch, where all the zones are
	// processed in a single call to process_intention_helper().
	//
	rgm::lni_t myLni = get_local_nodeid();
	return_early = process_intention_helper(myLni);

	if (!return_early) {
		// process intention for each local zone on this node
		LogicalNode *lnNode = lnManager->findObject(myLni);
		CL_PANIC(lnNode != NULL);
		std::list<LogicalNode*>::const_iterator it;
		for (it = lnNode->getLocalZones().begin(); it !=
		lnNode->getLocalZones().end(); ++it) {
			(void) process_intention_helper((*it)->getLni());
		}
	}

	slave_unlock_state();
#ifdef EUROPA_FARM
	return (TRUE);
#endif
}


//
// unlatch_intention_helper
//
// This function is called on all physical members.  It is possible
// that it will be called on a node that didn't latch the intention
// because it was not a cluster member at the time the intention
// was latched by the president (this can happen if the president
// dies and a new node joins at the same time).  This is OK;
// idl_process_intention() and idl_unlatch_intention() do nothing
// and return success if the intention is not latched.
//
// If the intention was an intention to delete a resource, then this
// function removes the resource data from memory.
//
void
unlatch_intention_helper(rgm::lni_t lni)
{
	char	*en_name = NULL;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	slave_intention *Rgm_Intention = lnNode->getIntention();
	if (Rgm_Intention == NULL) {
		ucmm_print("unlatch_intention_helper()",
		    NOGET("Nothing is latched\n"));
		return;
	}

	en_name = strdup(Rgm_Intention->intention->idlstr_entity_name);

	ucmm_print("unlatch_intention_helper()",
	    NOGET("Entity = <%s>, Type = <%d>, Op = <%d>, lni %d"),
	    en_name, Rgm_Intention->intention->entity_type,
	    Rgm_Intention->intention->operation_type, lni);

	// Sanity check
	if (Rgm_Intention->have_processed == 0) {
		ucmm_print("unlatch_intention_helper()",
		    NOGET("Warning: Unlatching an un-processed intention\n"));
	}

	//
	// If we are deleting a resource, we cannot remove the resource's
	// structure from slave memory until we have finished running Fini
	// methods in all zones on this node.  If any Fini methods are
	// being run, then the rl_is_deleted flag would have been set
	// on the resource in the function intention_delete_rs().
	// If the rl_is_deleted flag is not set, there are no fini
	// methods to run, so we delete r from memory immediately.
	// This is done on both president and slaves.
	//
	// If Fini methods have completed, launch_method() will have deleted
	// the resource. If so, we just issue a debug message.
	//
	// If Fini methods are being run (hence rl_is_deleted is true),
	// a similar check will be made in launch_method() on the slave
	// side, or in set_rx_state() on the president side.
	//
	// We need do this check only once per physical node, therefore we do
	// it only for a ZC or global zone.
	//
	if (lnNode->isZcOrGlobalZone() &&
	    Rgm_Intention->intention->operation_type == rgm::RGM_OP_DELETE &&
	    Rgm_Intention->intention->entity_type == rgm::RGM_ENT_RS) {
		rlist_p_t rp = rname_to_r(NULL, en_name);
		if (rp == NULL) {
			ucmm_print("unlatch_intention()",
			    NOGET("Not destroying RS <%s> from memory because "
				"it has already been destroyed.\n"), en_name);
		} else if (!rp->rl_is_deleted) {
			rglist_p_t rgp = rp->rl_rg;
			CL_PANIC(rgp != NULL);
			char *rg_name = rgp->rgl_ccr->rg_name;
			ucmm_print("unlatch_intention()",
			    NOGET("Destroy RS <%s> From Memory"), en_name);
			CL_PANIC(rgm_delete_r(en_name, rg_name).err_code ==
			    SCHA_ERR_NOERR);
		}
	}

	delete Rgm_Intention->intention;
	free(Rgm_Intention);
	lnNode->setIntention(NULL);
	free(en_name);
}

//
// idl_unlatch_intention
//
// This function is called by the president on each physical member node.
// It invokes unlatch_intention_helper() upon each zone on the local node.
// See comments for that function.
//
#ifdef EUROPA_FARM
bool_t
rgmx_unlatch_intention_1_svc(
    rgmx_process_intention_args *rpc_args,
    void *,
    struct svc_req *)
#else
void
rgm_comm_impl::idl_unlatch_intention(
    sol::nodeid_t president, sol::incarnation_num incarnation,
    Environment &)
#endif
{
#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
#endif

#ifndef EUROPA_FARM
	FAULT_POINT_FOR_ORPHAN_ORB_CALL("unlatch_intention", president,
		incarnation);
#endif

	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		slave_unlock_state();
#ifdef EUROPA_FARM
		return (TRUE);
#else
		return;
#endif
	}

	// unlatch intention for the ZC or global zone
	rgm::lni_t myLni = get_local_nodeid();
	unlatch_intention_helper(myLni);

	// unlatch intention for each base-cluster non-global zone on this node
	LogicalNode *lnNode = lnManager->findObject(myLni);
	CL_PANIC(lnNode != NULL);
	std::list<LogicalNode*>::const_iterator it;
	for (it = lnNode->getLocalZones().begin(); it !=
	    lnNode->getLocalZones().end(); ++it) {
		unlatch_intention_helper((*it)->getLni());
	}
	slave_unlock_state();
#ifdef EUROPA_FARM
	return (TRUE);
#endif
}

#ifndef EUROPA_FARM

//
// Called by a new president to complete any intention that might have been
// in process by the old president when it died.
// The global lock is already held when this is called
// in rgm_reconfig() in rgm_president.cc
//
int
complete_intention(void)
{
	boolean_t	doflag;
	boolean_t	intent_unmanage = B_TRUE;
	char		*en_name = NULL;
	char		*rg_name = NULL;
	rgm_rg_t	*rg_tbl;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm::rgm_intention	intent;
	int ret_code = 0;

	//
	// To retrieve the current intention, get it from the global
	// zone of the local node.
	//
	LogicalNode *lnNode = lnManager->findObject(orb_conf::local_nodeid());
	CL_PANIC(lnNode != NULL);
	slave_intention *Rgm_Intention = lnNode->getIntention();
	if (Rgm_Intention == NULL)
		return (ret_code);

	intent = *(Rgm_Intention->intention);

	en_name = strdup(Rgm_Intention->intention->idlstr_entity_name);
	ucmm_print("complete_intention()",
	    NOGET("Entity <%d> Operation <%d> Name <%s>\n"),
	    intent.entity_type, intent.operation_type, en_name);

	doflag = B_FALSE;

	switch (intent.operation_type) {

	case rgm::RGM_OP_UPDATE:
		//
		// basically what we are doing here is to run process_intention
		// again, regardless of whether the UPDATE was committed to the
		// CCR or not. We'll get here only in case when an update was
		// initiated and the President died midway. Now there are two
		// interesting cases here: 1) the update was committed to the
		// CCR, in which case it's perfectly OK, and even required,
		// to call process_intention. 2) Alternately, if the
		// update was not committed to the CCR (we can't have partial
		// updates, as that's taken care of by CCR), we will end up
		// making redundant UPDATE callbacks, which is harmless as the
		// RT's are anyway supposed to diff the new value with the old
		// value in their implementation of the UPDATE method.
		//
		doflag = B_TRUE;
		break;

	case rgm::RGM_OP_CREATE:

		switch (intent.entity_type) {

		case rgm::RGM_ENT_RG:

			doflag = rgmcnfg_rg_exist(intent.idlstr_entity_name,
			    &res, ZONE);
			if (res.err_code != SCHA_ERR_NOERR) {
				os::sc_syslog_msg logger(
					SC_SYSLOG_RGM_RGMD_TAG, "", NULL);
				//
				// SCMSGS
				// @explanation
				// The rgmd failed to detect the existence of
				// the specified resource group, so was unable
				// to complete the administrative request. The
				// node will be aborted.
				// @user_action
				// Examine other syslog messages occurring at
				// about the same time to see if the problem
				// can be identified which is preventing the
				// CCR access. Save a copy of the
				// /var/adm/messages files on all nodes and
				// contact your authorized Sun service
				// provider for assistance in diagnosing and
				// correcting the problem.
				//
				logger.log(LOG_ERR, MESSAGE, "fatal: unable to "
				    "determine if resource group %s exists.\n",
				    intent.idlstr_entity_name);
				abort_node(B_TRUE, B_FALSE, NULL);
			}
			ucmm_print("complete_intention()",
			    NOGET("Check RG <%s> in CCR"), en_name);

			break;

		case rgm::RGM_ENT_RS:

			// ignoring the return value, res, because we are
			// checking for rg_name not being null, which is the
			// same as res.err_code != SCHA_ERR_NOERR
			res = rgm_rsrc_on_which_rg(intent.idlstr_entity_name,
			    &rg_name, ZONE);
			if (rg_name == NULL)
				doflag = B_FALSE;
			else {
				doflag = B_TRUE;
				free(rg_name);
			}
			ucmm_print("complete_intention()",
			    NOGET("Check RS <%s> in CCR"), en_name);

			break;

		case rgm::RGM_ENT_RT:

			// Coding error: we should never hit this case
			// because we read RTs in on demand via rtname_to_rt().
			CL_PANIC(0);
			break;

		}	// switch on entity type

		break;		// XXX Read stuff from CCR

	case rgm::RGM_OP_DELETE:

		switch (intent.entity_type) {

		case rgm::RGM_ENT_RG:
			//
			// If the RG still exists, means the old
			// president didn't delete it successfully,
			// no process_intention() needs to run to
			// update memory structure.
			// Otherwise, we have to run process_intention()
			// to remove RG from the memory.
			//
			doflag = (boolean_t)!(rgmcnfg_rg_exist(
			    intent.idlstr_entity_name, &res, ZONE));
			if (res.err_code != SCHA_ERR_NOERR) {
				os::sc_syslog_msg logger(
					SC_SYSLOG_RGM_RGMD_TAG, "", NULL);
				logger.log(LOG_ERR, MESSAGE, "fatal: unable to "
				    "determine if resource group %s exists.\n",
				    intent.idlstr_entity_name);
				abort_node(B_TRUE, B_FALSE, NULL);
			}

			ucmm_print("complete_intention()",
			    NOGET("Check RG <%s> in CCR"),
			    en_name);

			break;

		case rgm::RGM_ENT_RS:

			CL_PANIC(en_name != NULL);

			//
			// If the resource still exists in the CCR, then the
			// old president died before it was able to carry out
			// the deletion; therefore, the new president should
			// just exit without doing anything.  If the resource
			// is not in the CCR, then the new president needs to
			// re-process the intention so the slaves can finish
			// running FINI methods and delete the resource from
			// memory.  Since process_intention is idempotent it's
			// OK if the slave already did it.
			//
			ucmm_print("complete_intention()",
			    NOGET("Check RS <%s> in CCR"), en_name);

			res = rgm_rsrc_on_which_rg(en_name, &rg_name, ZONE);

			//
			// ignoring the return value, res, because we
			// are checking for rg_name not being null, which
			// is the same as res.err_code != SCHA_ERR_NOERR
			//
			if (rg_name == NULL) {
				// Deletion in CCR succeeded.
				// need to run process_intention
				doflag = B_TRUE;

				//
				// Check if the resource is already deleted
				// from memory on this node.
				//
				rlist_p_t rp = rname_to_r(NULL, en_name);
				if (rp == NULL) {
					break;
				}

				//
				// At this point, the new president has
				// already fetched state from all slaves.
				// If any FINI method is in progress, then
				// we set the rl_is_deleted flag true, to make
				// sure that the resource gets deleted from
				// memory after the last fini method has run
				// on it.  If no FINI method is in progress on
				// this resource, then we set rl_is_deleted to
				// false so that the resource will get
				// removed from memory by
				// unlatch_intention_helper().
				//
				LogicalNodeset *ns = compute_meth_nodes(
				    rp->rl_ccrtype, rp->rl_rg->rgl_ccr,
				    METH_FINI, is_r_scalable(rp->rl_ccrdata));

				//
				// Iterate through all nodes in ns to check
				// resource state
				//
				boolean_t fini_running = B_FALSE;
				rgm::lni_t n = 0;
				while ((n = ns->nextLniSet(n + 1)) != 0) {
					if (rp->rl_xstate[n].rx_state ==
					    rgm::R_PENDING_FINI ||
					    rp->rl_xstate[n].rx_state ==
					    rgm::R_FINIING) {
						fini_running = B_TRUE;
						break;
					}
				}
				rp->rl_is_deleted = fini_running;
			} else {
				// Deletion failed.
				doflag = B_FALSE;
				free(rg_name);
			}

			break;

		case rgm::RGM_ENT_RT:
			//
			// If the RT still exists in the CCR, then the
			// deletion of the RT by the previous President
			// failed and slaves don't need to do anything.
			// But if the RT does not exist in the CCR, its
			// deletion by the previous President succeeded
			// and slaves still need to remove it from their
			// internal state structures.
			//
			doflag = (boolean_t)!(rgmcnfg_rt_exist(
			    intent.idlstr_entity_name, &res, ZONE));
			if (res.err_code != SCHA_ERR_NOERR) {
				os::sc_syslog_msg logger(
					SC_SYSLOG_RGM_RGMD_TAG, "", NULL);
				//
				// SCMSGS
				// @explanation
				// The rgmd failed to detect the existence of
				// the specified resource type, so was unable
				// to complete the administrative request. The
				// node will be aborted.
				// @user_action
				// Examine other syslog messages occurring at
				// about the same time to see if the problem
				// can be identified which is preventing the
				// CCR access. Save a copy of the
				// /var/adm/messages files on all nodes and
				// contact your authorized Sun service
				// provider for assistance in diagnosing and
				// correcting the problem.
				//
				logger.log(LOG_ERR, MESSAGE, "fatal: unable to "
				    "determine if resource type %s exists.\n",
				    intent.idlstr_entity_name);
				abort_node(B_TRUE, B_FALSE, NULL);
			}

			ucmm_print("complete_intention()",
			    NOGET("Check RT <%s> in CCR"),
			    en_name);

			break;

		}	// switch on entity type

		break;

	case rgm::RGM_OP_MANAGE:
		intent_unmanage = B_FALSE;
		// Fall through
	case rgm::RGM_OP_UNMANAGE:
		// Check CCR for managed/unmanaged state.
		if (rgmcnfg_read_rgtable(intent.idlstr_entity_name,
		    &rg_tbl, ZONE).err_code != SCHA_ERR_NOERR) {
			// XXXX
			// If CCR can not be read, we should return.
			// But what code should we return? - BugId 4217516
			ret_code = 1;
			goto complete_intention_end; //lint !e801
		}

		// Check intent vs. current managed/unmanaged state.
		if (rg_tbl->rg_unmanaged == intent_unmanage)
			doflag = B_TRUE;	// unmanage or manage succeeded
		else
			doflag = B_FALSE;	// unmanage or manage failed
		rgm_free_rg(rg_tbl);
		break;

	case rgm::RGM_OP_ENABLE_R:
	case rgm::RGM_OP_DISABLE_R:
	case rgm::RGM_OP_ENABLE_M_R:
	case rgm::RGM_OP_DISABLE_M_R:

		//
		// What we are doing here is to run process_intention again,
		// regardless of whether the update was committed to the CCR or
		// not. We'll get here only in case when an enable/disable of
		// On_off/Monitored switch was initiated and the President died
		// midway. Now there are two intersting cases here:
		// 1) the update was committed to the CCR, in which case it is
		// perfectly OK, and even required, to call process_intention.
		// 2) Alternately if update was not committed to the CCR (we
		// cannot have partial updates, as that is taken care of by the
		// CCR), we will end up making redundant process_intention and
		// update CCR calls. This is needed as we don't know the nodes
		// on which the update of On_off/Monitored switch happened
		// (supposed to happen). But the state machine is not run if the
		// old value of the switch equals the new value in CCR.
		//
		doflag = B_TRUE;
		break;

	case rgm::RGM_OP_SYNCUP:
	case rgm::RGM_OP_FORCE_ENABLE_R:
	case rgm::RGM_OP_FORCE_DISABLE_R:
	default:
		ucmm_print("complete_intention()",
		    NOGET("Unknown operation <%d> on entity <%s>"),
		    intent.operation_type, en_name);
		ret_code = 1;
		break;
	}

	if (doflag) {
		// Make slaves re-read CCR
		//
		(void) process_intention();
	}

	// New pres always unlatches
	(void) unlatch_intention();

complete_intention_end:

	free(en_name);

	free(res.err_msg);

	return (ret_code);
}
#endif
