/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_STATE_H
#define	_RGM_STATE_H

#pragma ident	"@(#)rgm_state.h	1.100	09/02/19 SMI"

#ifndef EUROPA_FARM
#include <cmm/ucmm_api.h>
#endif
#include <h/rgm.h>
#include <rgm/rgm_common.h>
#include <h/version_manager.h>
#include <rgm/rgm_methods.h>
#include <sys/sol_version.h>
/* Zone suppport */
#include <rgm_logical_nodeset.h>


#include <map>

/* PRES_UNKNOWN is like BAD_NODEID: an unreal nodeid */
#define	PRES_UNKNOWN	0

/* Currently we only distinguish between 0 (success) and nonzero (failure) */
#define	EMETH_SUCCESS	0
#define	EMETH_FAILURE	1

/*
 * Kludge for idl version 0 -- flags passed as nodeset to intention calls
 */
#define	OKFLAG_NOCHANGE	0
#define	OKFLAG_UPDATE	1
#define	OKFLAG_SCSWITCH	2

/* Default method timeout in seconds (if fail to obtain it from the CCR) */
const int RGM_DEFMETHTIMEOUT = 3600;

/*
 * START_FAILED ping-pong prevention
 * The rebalance() function uses an algorithm to prevent the following
 * sort of "ping-pong" behavior: Suppose that RG's nodelist contains
 * node1, node2, node3, node4.  Suppose that RG fails to start on node 1.
 * RG will move to RG_OFFLINE_START_FAILED on node 1.  rebalance()
 * might try to start RG on node2.  If that also fails, we do not
 * want to try node1 again but should rather try node3 in
 * case it is healthier than node1 and node2.
 * Rebalance uses an algorithm with two parameters, RG_START_RETRY_MAX
 * and property Pingpong_interval.  If RG has failed to start more
 * than RG_START_RETRY_MAX times within the past Pingpong_interval
 * seconds on node n, then that node is considered ineligible to host
 * RG and rebalance() will look for another master.
 * Also, rebalance() will give highest preference to nodes with the
 * lowest number of recent start-failures.
 * For SC 3.0, we will define the retry maximum to be 1.
 * Pingpong_interval is a tunable property; its default is one hour.
 * This means that we will try
 * to host the RG even after one recent failure on a node, but give
 * preference to nodes on which there have been no recent failures.
 *
 * Note that this algorithm is independent of the scha_control
 * ping-pong prevention algorithm, which uses a file on each node's
 * local disk to remember when an RG was last failed off of that node
 * by a scha_control call.  The failure of a START method might not
 * correlate with a resource monitor's decision to call scha_control().
 * The scha_control ping-pong algorithm
 * persists through reboots of any node including the president, while
 * this rebalance() ping-pong algorithm forgets its history when the
 * president dies.
 *
 * We might consider having scha_control examine the start-failure
 * history and/or have rebalance() examine the scha_control giveover
 * history in considering whether to use a given node as a new master
 * of the RG, but currently we do not do so.
 *
 * Note that scswitch does *not* look at failure history as a precondition
 * for doing a switch; the rationale is that scswitch is human-initiated
 * so the human operator can decide on what nodes to attempt to start the
 * RG and can deal with start failures that might ensue.
 *
 * Opportunities for future enhancement:
 * 1. Make RG_START_RETRY_MAX a tunable per-RG
 *    property rather than a defined constant.
 * 2. Have rebalance() use the scha_control() giveover history as a
 *    criterion for whether to use a node as a new master of the RG.
 * 3. Have scha_control() use the start-failure history as a criterion
 *    for whether to use a node as a new master of the RG.
 */
const int RG_START_RETRY_MAX = 1;



/*
 * rg_xstate_t
 * Extended RG state per zone maintained by RGM.
 * Element of std::map, which is part of the rglist_t RG list structure.
 * President accesses state information for all nodes; other nodes access
 * just their own zones' state information.
 *
 * rgm::rgm_rg_state is defined in rgm.h generated from src/interfaces/rgm.idl.
 */
struct rg_xstate {
	rgm::rgm_rg_state rgx_state;	 /* RG state */
	rgm_timelist_t	*rgx_start_fail; /* recent start failures, used */
					/* by president rebalance() only */
	boolean_t	rgx_net_relative_restart;
					/* Is RG Pending_offline w/net- */
					/* relative dependent resources? */
	boolean_t	rgx_postponed_stop;	/* set if we try to stop RG */
						/* while ON_PENDING_METHODS */
	boolean_t	rgx_postponed_boot;
				/* set if we try to run boot methods while */
				/* PENDING_OFFLINE or ERROR_STOP_FAILED in */
				/* a non-global zone, due to Global_zone=TRUE */
				/* resource types */

	// ctor, so we can use overloaded operator[] in the map.
	inline rg_xstate();
};

typedef struct rg_xstate rg_xstate_t;

// set the members to the default values
rg_xstate::rg_xstate() : rgx_state(rgm::RG_OFFLINE), rgx_start_fail(NULL),
    rgx_net_relative_restart(B_FALSE), rgx_postponed_stop(B_FALSE),
    rgx_postponed_boot(B_FALSE)
{
}

/*
 * Extended (per-zone) R state maintained by RGM.
 * President accesses state information for all zones;
 * slave nodes access just their own zones' state information.
 * A map of these structures is part of the R list structure.
 * rgm::rgm_r_state is defined in rgm.h generated from src/interfaces/rgm.idl.
 *
 * The 'rx_restarting' flag is kept only on the president side.  It is set
 * true when the president initiates a restart of the resource on the slave.
 * It is reset to false as soon as the president receives a state change
 * for the resource on the slave.  This flag is used on the president to
 * help determine whether a restart has completed (since the president does
 * not have access to the rx_restart_flg on the slave).
 *
 * This structure has be kept in sync with the mdb_rxstate_t structure
 * defined in rgm_common.h
 */
struct r_xstate {
	rgm::rgm_r_state	rx_state;	/* extended r state for this */
						/* node */
#ifdef FUTURE
/* XXX - bugid 4215776-- replace with boolean flags: */
	rgm::rgm_r_state	rx_fyi_state;	/* more state for sys admin; */
						/* not interesting to state */
						/* machine */
#endif /* FUTURE */
	rgm::rgm_r_fm_status	rx_fm_status;	/* R fault montor status for */
						/* a node */
	char			*rx_fm_status_msg;	/* R fault monitor */
							/* status messages */
	boolean_t		rx_restarting;	/* R will restart on slave */
	rgm_timelist_t	*rx_rg_restarts;	/* history of recent scha_ */
						/* control RG restart */
						/* attempts by this resource */
						/* on local zone */
	time_t		rx_last_restart_time;
	time_t		rx_throttle_restart_counter;
		/* above two fields are used to quench scha_control restarts */

	rgm_timelist_t	*rx_r_restarts;	/* history of recent scha_control */
					/* resource_restart attempts by this */
					/* resource on local zone */
	rgm_timelist_t	*rx_blocked_restart;	/* history of recent blocked */
						/* calls to scha_control */
						/* because of setting of */
						/* Failover_mode. Used to */
						/* avoid overflowing syslog */
	rgm::r_restart_t	rx_restart_flg;	/* See r_restart_t declaratn */
						/* in rgm.idl */
	boolean_t	rx_ignore_failed_start; /* Set to B_TRUE when the */
						/* scha_control */
						/* (SCHA_IGNORE_FAILED_START) */
						/* has been received. */
	boolean_t	rx_ok_to_start;		/* Set to B_TRUE when a */
						/* resource is in */
						/* PRENET_STARTED state */

	// default ctor so it can be used in map
	inline r_xstate();
};

r_xstate::r_xstate() : rx_state(rgm::R_OFFLINE),
    rx_fm_status(rgm::R_FM_OFFLINE), rx_fm_status_msg(NULL),
    rx_restarting(B_FALSE), rx_rg_restarts(NULL), rx_last_restart_time(-1),
    rx_throttle_restart_counter(0), rx_r_restarts(NULL),
    rx_blocked_restart(NULL), rx_restart_flg(rgm::RR_NONE),
    rx_ignore_failed_start(B_FALSE), rx_ok_to_start(B_FALSE)
{
}

typedef struct r_xstate r_xstate_t;

/*
 * Define pointers to rlist_t and rglist_t types here
 * because an element of each structure points to the other type.
 */
typedef struct rlist *rlist_p_t;
typedef struct rglist *rglist_p_t;


/*
 * rlist_t - Resource state information.
 * Includes extended, local resource state,
 * static config data from CCR,
 * pointer to RG of which this R is a member.
 *
 * 'rl_dependents' is an "implicit" property in that it is always
 * derived from the resource property r_dependencies for all resources.
 * It is not directly set or viewable to the user, nor is it stored in the
 * CCR.  To maintain consistency, the 'rl_dependents' for all resources
 * must be re-computed on all slaves (including the president)
 * whenever the 'r_dependencies' for any resource changes.
 */
typedef struct rlist {
	rgm_resource_t	*rl_ccrdata;	/* pointer to static R config */
					/* data stored in CCR */
	rgm_rt_t	*rl_ccrtype;	/* pointer to corresponding RT config */
					/* data stored in CCR */
	rglist_p_t	rl_rg;		/* back pointer to the RG structure */
	rgm::rgm_r_state	succ_state;	// Proxy resource Success state
	rgm::rgm_r_state	fail_state;	// Proxy resource Failure state
	std::map<rgm::lni_t, r_xstate_t> rl_xstate; /* R extended state for */
							/* each zone */
	boolean_t	rl_is_deleted;	/* true if R has been deleted and is */
						/* running FINI methods */
	rgm_dependencies_t rl_dependents;	/* resources which depend on */
						/* this resource */
	rlist_p_t	rl_next;	/* (null-terminated) */
	mdb_rxlogs_t		log_rxstate;
	mdb_rxlogs_t		mdb_rxback;
} rlist_t;


typedef enum rg_switch {
	SW_NONE,		/* RG is not being switched or updated */
	SW_IN_PROGRESS,		/* scswitch or scha_control in progress */
	SW_UPDATING,		/* RG or R being updated */
	SW_IN_FAILBACK,		/* RG is under failback */
	SW_BUSTED_RECONF,	/* switch busted by node death */
	SW_UPDATE_BUSTED,	/* update busted by node death */
	SW_BUSTED_START,	/* switch busted: start method failed */
				/* causing giveover/rebalance */
	SW_BUSTED_STOP,		/* switch busted: stop method failed causing */
				/* the RG to go to ERROR_STOP_FAILED */
	SW_EVACUATING		/* RG is being evacuated from node(s). */
				/* This is treated like a "BUSTED" flag by */
				/* any in-progress switching or updating */
				/* thread; */
				/* it also prevents any new switch, update, */
				/* giveover or rebalance from starting. */
} rg_switch_t;

/*
 * Following typedef indicates (by a nonzero value) if the RG is currently
 * frozen.  FROZ_SET and FROZ_REMAINS are used by rgm_chg_freeze() [in
 * rgm_comm_impl.cc] to update freeze values based on the list of frozen
 * RGs passed by the president.
 * See header comments of rgm_chg_freeze() for details.
 */
typedef enum frozen {
	FROZ_FALSE = 0,	/* The RG is unfrozen */
	FROZ_TRUE,	/* The RG is frozen */
	FROZ_SET,	/* The RG has just been updated from unfrozen to */
			/* frozen */
	FROZ_REMAINS	/* The RG has just been updated and remains frozen */
} rg_frozen_t;

/*
 * The methodinvoc_t struct is an element of a linked list
 * (rgl_methods_in_flight) stored in the RG state (rglist_t)
 * structure on each slave.  The methodinvoc_t struct is
 * both added and removed by launch_method() in rgm_launch_method.cc.
 * The fed tag stored in mi_tag enables freeze_adjust_timeouts() [in
 * rgm_global_res_used.cc] to suspend/resume timeouts on the in-flight
 * method.  See launch_method() and freeze_adjust_timeouts() for more
 * details.
 */
typedef struct methodinvoc {
	char			*mi_tag;
	boolean_t		mi_completed;
	struct methodinvoc	*mi_next;
} methodinvoc_t;


/*
 * rgm_affinities_t -
 *
 * This type is used to store the affinities and "affinitents"
 * (inverse pointers for affinities).
 *
 * ap_strong_pos, ap_weak_pos, ap_strong_neg, and ap_weak_neg and
 * ap_failover_delegation are all pointers to a list of strings representing
 * resource group names.
 *
 */
typedef struct rgm_affinities {
	namelist_t	*ap_strong_pos;
	namelist_t	*ap_weak_pos;
	namelist_t	*ap_strong_neg;
	namelist_t	*ap_weak_neg;
	namelist_t	*ap_failover_delegation;
} rgm_affinities_t;

/*
 * affinitytype_t
 *
 * This type is used to represent the type of affinity.
 */
typedef enum affinitytype {
	AFF_STRONG_POS,
	AFF_WEAK_POS,
	AFF_STRONG_NEG,
	AFF_WEAK_NEG
} affinitytype_t;


/*
 * rglist_t
 *
 * This structure includes the static RG structure (rgm_rg) from rgm_common.h,
 * along with the per-node extended RG state.  [On a slave, rgl_xstate stores
 * the state for the local node only; on the president, rgl_xstate stores the
 * state of the RG for all member nodes.]  This structure is part of
 * a linked list of RG information, whose head is pointed to by the rgm_state
 * structure.  It also points to a linked list of rlist_t structures whose
 * resources are part of this resource group.
 * This structure has be kept in sync with the mdb_rgxstate_t structure
 * in rgm_common.h.
 */
typedef struct rglist {
	rgm_rg_t	*rgl_ccr;	/* rg struct extracted from CCR */
	std::map<rgm::lni_t, rg_xstate> rgl_xstate;
					/* rg ext state for each node */
	rg_switch_t	rgl_switching;	/* switch in progress or busted? */
	boolean_t	rgl_failback_running; /* failback running or not? */
	uint_t		rgl_failback_interrupt;	/* used by failback() to */
						/* preempt previous failback */
						/* thread */
	LogicalNodeset	rgl_joining_nodes;	/* keeps track of the joining */
						/* nodes for failback */
	LogicalNodeset	rgl_evac_nodes;	/* keeps track of the nodes from */
					/* which this RG is being evacuated. */
	LogicalNodeset	rgl_planned_on_nodes; /* keeps track of the nodes */
						/* on which this RG is */
						/* planning to go online */
						/* with a do_rg_switch */
						/* command. */
	boolean_t	rgl_new_pres_rebalance;
					/* Need to run rebalance(), but we're */
					/* a new pres, and the old pres was */
					/* updating. */
	boolean_t	rgl_delayed_rebalance;
					/* Can't run rebalance right now, */
					/* but need to as soon as possible. */
					/* Can't reuse rgl_new_pres_rebalance */
					/* flag, because it needs to persist */
					/* longer. */
	boolean_t	rgl_no_rebalance;
					/* set if we want to temporarily */
					/* prevent rebalance from being */
					/* called. */
	boolean_t	rgl_no_triggered_rebalance;
					/* set if we want to temporarily */
					/* prevent rebalance from being */
					/* called */
					/* on affinitents for an RG that */
					/* changes state. */
	boolean_t	rgl_ok_to_start; /* ok to start RG */
	rg_frozen_t	rgl_is_frozen;	/* depends on frozen global resource? */
	time_t		rgl_last_frozen; /* time of last unfreeze of this RG */
	methodinvoc_t	*rgl_methods_in_flight;	/* fed tags of in-flight */
						/* methods */
	rgm_affinities_t rgl_affinities; /* RGs for whom this RG has */
						/* affinities */
	rgm_affinities_t rgl_affinitents; /* RGs who have affinities for */
						/* this RG */

	boolean_t	rgl_quiescing;	/* The RG is being quiesced, */
					/* nameserver tag is set. Do not do */
					/* rebalance. */
	int		rgl_total_rs;	/* total number of Rs in this RG */
	rlist_p_t	rgl_resources;	/* extended r states, this node only */
	rglist_p_t	rgl_next;	/* (null-terminated) */
	mdb_rgxlogs_t		log_rgxstate; /* MDB logs for RG state */
	mdb_rgxlogs_t		mdb_rgxback; /* MDB backup for RG state */

} rglist_t;

/*
 * rgm_version_t
 *
 * Stores information about the versions, as retrieved from the version
 * manager, and their implications for rgm features.  Currently stores
 * the current version at which the rgmd on this node is running, and a new
 * version, which will only be different from the current version during an
 * upgrade.
 */
typedef struct rgm_version_t
{
	version_manager::vp_version_t current_version;
	version_manager::vp_version_t new_version;
	char *current_upgrade_cb_step;
} rgm_version_t;

/*
 * rgm_state_t
 *
 * This is the big state structure.  Each RGM has its own local copy.
 * The structure is protected by a mutex when the state machine is running.
 * Its components are the mutex, the nodeid of the president,
 * the current UCMM sequence number, a
 * bool indicating that the RGM is starting up on this node,
 * a pointer to a linked list of all RGs configured in the system,
 * a pointer to a linked list of all RTs configured in the system,
 * the nodeset and node incarnations of the current node membership,
 * a bool indicating whether the ccr has been read or not, the
 * static cluster state, a bool indicating whether the node is a new
 * president and reconfiguring or not, and the version info.
 */
typedef struct rgm_state {
	sol::nodeid_t	rgm_President;		/* nodeid of President */
	/*
	 * The pres field should be the first element in the structure
	 * so that president_node query in rgmd_debug.sh doesn't break
	 */
	os::mutex_t	rgm_mutex;		/* lock on state structure */
	cmm::seqnum_t	rgm_membership_generation; /* UCMM sequence number */
	int		rgm_total_rgs;		/* total number of RGs */
	int		rgm_total_rs;		/* total number of resources */
	boolean_t	is_thisnode_booting; /* true only when RGM starting */
	rglist_p_t	rgm_rglist;		/* pointer to list of RGs */
	rgm_rt_t	*rgm_rtlist;		/* pointer to list of RTs; */
	cmm::membership_t
			node_incarnations;	/* incn # of current members */
	LogicalNodeset	current_membership;	/* current members nodeset */
#if (SOL_VERSION >= __s10)
	LogicalNodeset	current_zc_membership;	/* current ZC members nodeset */
	cmm::seqnum_t	zc_membership_generation;
						/* current ZC sequence number */
	cmm::membership_t
			zc_node_incarnations;
						/* incn # of ZC members */
	boolean_t	using_process_membership;
						/* true if using process */
						/* membership feature */
#endif
	LogicalNodeset	static_membership;	/* configured members nodeset */
	uint_t		num_static_nodes;	/* # of configured members */
	boolean_t	use_physnode;		/* force phys node affinities */
	boolean_t	ccr_is_read;		/* TRUE if CCR has been read */
	struct clust_state
			thecluststate;	/* cluster state from cmm_impl */
	boolean_t	pres_reconfig;		/* true when this rgm is */
						/* a president and is */
						/* reconfiguring */
	boolean_t	suppress_notification;	/* true when we are _not_ */
						/* supposed to push state to */
						/* the pres (because we've */
						/* called wake_president */
						/* without holding the lock) */
	rgm_version_t	version;	/* version manager version info */

} rgm_state_t;


/*
 * launchargs_t - arguments for launching method-invoking thread.
 * A pointer to this structure is passed to the launch_method() routine.
 *
 * If the resource type has Scalable=true, then l_is_scalable will be set
 * to true.  In this case, launch_method() will invoke ssm_wrapper as
 * the method, and pass the real method pathname as a command line argument.
 * If there is no "real" method registered, then l_rt_meth_name will be NULL
 * but we will still invoke the SSM wrapper to perform scha_ssm_<method>.
 */
typedef struct launchargs {
	name_t		l_rname;	/* the Resource name */
	name_t		l_rgname;	/* the Resource Group name */
	method_t	l_method;	/* type of method to launch */
	name_t		l_rt_meth_name;	/* pathname of method to launch */
	boolean_t	l_is_scalable;	/* is R scalable? (for ssm_api) */
	rgm::rgm_r_state l_succstate; /* new R state if method succeeds */
	rgm::rgm_r_state l_failstate; /* new R state if failure occurs */
	rgm::rgm_r_state l_workstate; /* working state while exec-ing method */
} launchargs_t;


/*
 * launchvalargs_t - arguments for launching validate method-invoking thread.
 * A pointer to this structure is passed to the launch_validate_method().
 *
 * If lv_is_scalable is true, the resource is a scalable service.
 * launch_validate_method() will then invoke the ssm_wrapper program as
 * the validate method, passing it the real VALIDATE method pathname
 * (from lv_rt_meth_name) as a command line argument.
 * Otherwise, it just invokes lv_rt_meth_name directly.
 */
typedef struct launchvalargs {
	name_t		lv_rname;	/* the Resource name */
	name_t		lv_rtname;	/* the Resource Type name */
	name_t		lv_rgname;	/* the Resource Group name */
	method_t	lv_method;	/* type of method to launch */
	name_t		lv_rt_meth_name;	/* name of method to launch */
	boolean_t	lv_is_scalable;	/* is the resource scalable? */
	rgm::rgm_operation	lv_opcode;	/* RGM_OP_CREATE: R creation */
						/* RGM_OP_UPDATE: R update */
	rgm::idl_string_seq_t	lv_r_props;	/* resource predefined props */
	rgm::idl_string_seq_t	lv_x_props;	/* resource extention props */
	rgm::idl_string_seq_t	lv_allnodes_x_props; /* All nodes ext props */
	rgm::idl_string_seq_t	lv_g_props;	/* RG property-list */
	char *locale;
} launchvalargs_t;

/*
 * Candidate node for new master of RG
 * Used by sort_candidate_nodes
 */
typedef struct candidate {
	rgm::lni_t	lni;		/* The logical node id */
	uint_t		pref;		/* Position of node in RG's Nodelist */
	uint_t		start_fail;	/* recent start failures of RG on */
					/* node */
	time_t		recent_fail;	/* Most recent start failure time */
	namelist_t	*tclist;
	int		V;	/* strong affinity violations */
	int		OW;	/* outgoing weak affinity satisfaction */
	int		IW;	/* incoming weak affinity satisfaction */
	bool		down_ng_zone;	/* dead non-global zone flag, used by */
					/* failback() */
	uint_t		E;	/* Number of resources enabled */
} candidate_t;


extern rgm_state_t *Rgm_state;

/*
 * President threads wait on this variable while holding the big state machine
 * lock.  The variable is broadcast when the president is notified of an
 * R/RG state change on a slave.
 */
extern os::condvar_t cond_slavewait;

/*
 * failback() does a timed wait on this variable during zone bootdelay.
 * It is broadcast on the president when a non-global zone joins the cluster
 * membership.
 */
extern os::condvar_t cond_zone_bootdelay;

// data structure to store cluster information.
typedef struct {
	int node_incarnation;
	int rgm_incarnation;
	int pid_incarnation;
	char *clustername;
} cluster_info_t;

#endif	/* !_RGM_STATE_H */
