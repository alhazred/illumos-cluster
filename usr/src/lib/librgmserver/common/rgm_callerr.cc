/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * rgm_callerr.c
 *
 *	Wrappers for frequent library calls. Abort rgmd on error.
 */

#pragma	ident	"@(#)rgm_callerr.cc	1.34	08/05/20 SMI"

#include <sys/sc_syslog_msg.h>
#include <string.h>
#include <rgm_proto.h>

//
// Default stack size for threads created via thr_create().
// We pre-allocate a stack of this size to prevent the thread and the
// process from dying due to failure to grow the stack (due to the
// node running out of swap space)
//
#define	RGM_THRSTACKSIZE	(64 * 1024)

//
// This routine tries to create a thread and exits the process on errors.
// It can be used to create threads that MUST exist for the process to
// function correctly.
//
// If no thread stack is specified, it first tries to pre-allocate a stack
// for the thread to use. This prevents the thread and the process dying
// later due to failure to grow the stack (when the system is out of swap
// space).
//
// If the stack allocation fails or the thread creation fails, the routine
// exits with an error message.
//
extern "C" void
thr_create_err(void *stack_base, size_t stack_size,
    void *(*start_routine)(void *), void *arg, long flags,
    thread_t *new_thread)
{
	size_t	stksize;
	int	error;
	sc_syslog_msg_handle_t handle;

	stksize = (stack_size > (size_t)0) ? stack_size : RGM_THRSTACKSIZE;

	if (stack_base == NULL) {
		stack_base = malloc_nocheck(stksize);
		if (stack_base == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    syslog_tag, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd daemon was unable to create a thread
			// stack, most likely because the system has run out
			// of swap space. The rgmd will produce a core file
			// and will force the node to halt or reboot to avoid
			// the possibility of data corruption.
			// @user_action
			// Rebooting the node has probably cured the problem.
			// If the problem recurs, you might need to increase
			// swap space by configuring additional swap devices.
			// See swap(1M) for more information.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: thr_create stack allocation failure: "
			    "%s (UNIX error %d)",
			    strerror(errno), errno);
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHABLE
		}
	}

	error = os::thread::create(stack_base, stksize, start_routine, arg,
	    flags, new_thread);

	if (error != 0) {
		(void) sc_syslog_msg_initialize(&handle, syslog_tag, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd failed in an attempt to create a thread. The rgmd
		// will produce a core file and will force the node to halt or
		// reboot to avoid the possibility of data corruption.
		// @user_action
		// Fix the problem described by the UNIX error message. The
		// problem may have already been corrected by the node reboot.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: thr_create returned error: %s (UNIX error %d)",
		    strerror(errno), errno);
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}
}


//
// This function is provided for callers that can handle calloc() failing
// to allocate memory (returning NULL). RGMD callers who cannot handle
// memory allocation failures should call calloc() which calls malloc() to
// allocate memory. In the rgmd process, malloc() will exit() if it
// cannot allocate memory and will not return NULL.
//
// The code is cloned from libc with malloc() replaced by malloc_nocheck().
//
void *
calloc_nocheck(size_t num, size_t size)
{
	void *mp;
	size_t total;

	if (num == 0 || size == 0)
		total = 0;
	else {
		total = num * size;

		/* check for overflow */
		if (total / num != size)
		    return (NULL);
	}

	mp = malloc_nocheck(total);

	return ((mp == NULL) ? mp : memset(mp, 0, total));
}
