/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _RGM_THREADS_H
#define	_RGM_THREADS_H

#pragma ident	"@(#)rgm_threads.h	1.20	09/01/28 SMI"

#include <h/sol.h>
#include <sys/threadpool.h>
#include <rgm_state.h>
#include <rgm_proto.h>


#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

// Swap space reserved by the RGMD to avoid malloc() failures.
const int RGM_MIN_RESERVED_SWAP_SIZE = 16 * 1024 * 1024; // 16 MB
const int RGM_MAX_RESERVED_SWAP_SIZE = 64 * 1024 * 1024; // 64 MB
const int RGM_SWAPSPACE_PER_RESOURCE = 1024 * 1024; // 1 MB

// Limits on number of threads to handle RG failback
const int RGM_MIN_FAILBACK_THREADS = 4;
const int RGM_MAX_FAILBACK_THREADS = 17;

// Limits on number of threads to launch resource callback methods
const int RGM_MIN_LAUNCH_METHOD_THREADS = 16;
const int RGM_MAX_LAUNCH_METHOD_THREADS = 64;

//
// The following constant is used to limit the number of threads based on
// the size of physical memory in the system.
//
const int RGM_MEGABYTES_PER_LAUNCH_METHOD_THREAD = 16;

const int RATIO_NB_THREADS_VS_MAX_SCHA_CONTROL = 2;
const int MAX_SCHA_CONTROLS_INIT = 32;

//
// Each scha_control call increments the libthread "concurrency" (number of
// lwps for unbound threads) by 1.  We limit the maximum level of concurrency
// to RGM_MAX_THR_CONCURRENCY to avoid the situation that a poorly written
// monitor can make failed scha_control calls in a tight loop, thereby
// exhausting system resources by creating too many lwps.  Such a situation
// can crash the rgmd in an unpredictable manner.
//
const int RGM_MAX_THR_CONCURRENCY = 50;

//
// state_machine_task
//
// This task is used to queue a request to run the state machine.
//
class state_machine_task : public defer_task {
public:
	// Runs state machine on particular zone on slave node
	state_machine_task(rgm::lni_t _lni) : lni(_lni) {};

	void execute();

private:
	rgm::lni_t	lni;

	// Disallow assignment and pass by value
	state_machine_task(const state_machine_task &);
	state_machine_task &operator = (state_machine_task &);
};

//
// state_machine_threadpool
//
// This threadpool is used to run the state machine.
//
class state_machine_threadpool : public threadpool {
public:
	static state_machine_threadpool &the()
	    {return state_machine_threadpool::the_state_machine_threadpool; }

	state_machine_threadpool();

	static void initialize();

private:
	static state_machine_threadpool the_state_machine_threadpool;

	// Disallow assignment and pass by value
	state_machine_threadpool(const state_machine_threadpool &);
	state_machine_threadpool &operator = (state_machine_threadpool &);
};

#ifndef EUROPA_FARM
//
// notify_state_change_task
//
// This task is used to queue a request to wake up the President (i.e., the
// president fetches all R/RG state from the slave), or to notify the
// president of an "interesting" state change on a specific R or RG
//
// It runs only on the President.
//
class notify_state_change_task : public defer_task {
public:
	notify_state_change_task(sol::nodeid_t _node,
	    rgm::notify_change_tag _change_tag,
	    const char *_R_RG_name,
	    rgm::rgm_r_state _rstate,
	    rgm::rgm_rg_state _rgstate,
	    rgm::node_state *_full_state) :
	    node(_node),
	    change_tag(_change_tag),
	    // We make our own copy of R_RG_name,
	    // which must be deleted by destructor
	    R_RG_name(new_str(_R_RG_name)),
	    rstate(_rstate),
	    rgstate(_rgstate),
	    // We make a shallow copy of state.  We will free it.
	    // Caller should not free it.
	    state(_full_state)
	    {};

	~notify_state_change_task()
	{
		delete [] (char *)R_RG_name;
		delete state;
	}

	void execute();
private:
	sol::nodeid_t		node;
	rgm::notify_change_tag	change_tag;
	const char		*R_RG_name;
	rgm::rgm_r_state	rstate;
	rgm::rgm_rg_state	rgstate;
	rgm::node_state		*state;

	// Disallow assignment and pass by value
	notify_state_change_task(const notify_state_change_task &);
	notify_state_change_task &operator = (notify_state_change_task &);
};

//
// notify_state_change_threadpool
//
// This threadpool is used to asynchronously notify the President
// of "interesting" state changes on an or RG
//
class notify_state_change_threadpool : public threadpool {
public:
	static notify_state_change_threadpool &the()
	    {return notify_state_change_threadpool
	    ::the_notify_state_change_threadpool; }

	notify_state_change_threadpool();

	static void initialize();

private:
	static notify_state_change_threadpool
	    the_notify_state_change_threadpool;

	// Disallow assignment and pass by value
	notify_state_change_threadpool(
	    const notify_state_change_threadpool &);
	notify_state_change_threadpool &operator
	    = (notify_state_change_threadpool &);
};

//
//
// notify_status_change_task
//
// This task is used to queue a status change on the president.
//
class notify_status_change_task : public defer_task {
public:
	notify_status_change_task(sol::nodeid_t _node,
	    const char *_R_name,
	    const char *_RG_name,
	    rgm::rgm_r_fm_status _status,
	    const char *_status_msg) :
	    node(_node),
	    // We make our own copy of R_name, RG_name, and status_msg
	    // which must be deleted by destructor
	    R_name(new_str(_R_name)), RG_name(new_str(_RG_name)),
	    status(_status)
	    { status_msg = _status_msg ? new_str(_status_msg) : NULL; }

	~notify_status_change_task()
	{
		delete [] (char *)R_name;
		delete [] (char *)RG_name;
		delete [] (char *)status_msg;
	}

	void execute();
private:
	sol::nodeid_t		node;
	const char		*R_name;
	const char		*RG_name;
	const char		*status_msg;
	rgm::rgm_r_fm_status	status;

	// Disallow assignment and pass by value
	notify_status_change_task(const notify_status_change_task &);
	notify_status_change_task &operator = (notify_status_change_task &);
};

//
// notify_status_change_threadpool
//
// This threadpool is used to asynchronously notify the President
// of status changes on a resource.
//
class notify_status_change_threadpool : public threadpool {
public:
	static notify_status_change_threadpool &the()
	    {return notify_status_change_threadpool
	    ::the_notify_status_change_threadpool; }

	notify_status_change_threadpool();

	static void initialize();

private:
	static notify_status_change_threadpool
	    the_notify_status_change_threadpool;

	// Disallow assignment and pass by value
	notify_status_change_threadpool(
	    const notify_status_change_threadpool &);
	notify_status_change_threadpool &operator
	    = (notify_status_change_threadpool &);
};

//
// failback_task
//
// This task is used to queue a request to failback an RG.
// It runs only on the President.
//
class failback_task : public defer_task {
public:
	failback_task(rgm::lni_t _lni, rglist_p_t rgp);
	~failback_task();
	void execute();
private:
	rgm::lni_t	lni;
	char		*rgname;

	// Disallow assignment and pass by value
	failback_task(const failback_task &);
	failback_task &operator = (failback_task &);
};

//
// failback_threadpool
//
// This threadpool is used to asynchronously run failback for RGs when
// slave nodes boot. These threads run only on the President.
//
// While the API imposes no limit on the number of resource groups that can
// be created, we limit the number of threads that can run concurrently to
// process failbacks to a small value at RGMD initialization time.
//
class failback_threadpool : public threadpool {
public:
	static failback_threadpool &the()
	    {return failback_threadpool::the_failback_threadpool; }

	failback_threadpool();

	static void initialize(int num_RGs);

private:
	static failback_threadpool the_failback_threadpool;

	// Disallow assignment and pass by value
	failback_threadpool(const failback_threadpool &);
	failback_threadpool &operator = (failback_threadpool &);
};
#endif // !EUROPA_FARM

//
// launch_method_task
//
// This task is used to queue a request to call a resource's callback
// method. It can be run on all nodes in the cluster.
//
class launch_method_task : public defer_task {
public:
	launch_method_task(launchargs_t *args, rgm::lni_t _lni) :
	    launch_args(args), lni(_lni)
	    {};

	void execute();
private:
	launchargs_t	*launch_args;
	rgm::lni_t	lni;

	// Disallow assignment and pass by value
	launch_method_task(const launch_method_task &);
	launch_method_task &operator = (launch_method_task &);
};

//
// launch_method_threadpool
//
// This thread pool is used to call the callback methods of resources.
// While the API imposes no limit on the number of resources that can be
// created, we limit the number of threads that can run concurrently to
// handle callbacks to a manageable value at RGMD initialization time.
//
class launch_method_threadpool : public threadpool {
public:
	static launch_method_threadpool &the()
	    {return launch_method_threadpool::the_launch_method_threadpool; }

	launch_method_threadpool();

	static void initialize(int num_Rs);

private:
	static launch_method_threadpool the_launch_method_threadpool;

	// Disallow assignment and pass by value
	launch_method_threadpool(const launch_method_threadpool &);
	launch_method_threadpool &operator = (launch_method_threadpool &);
};
#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif /* _RGM_THREADS_H */
