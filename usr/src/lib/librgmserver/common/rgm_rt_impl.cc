//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_rt_impl.cc	1.55	08/07/28 SMI"

//
// rgm_rt_impl.cc
//
// The functions in this file implement the President side of
// scrgadm RT operations.
//
#include <syslog.h>

#include <rgm/rgm_comm_impl.h>
#include <rgm_proto.h>
#include <rgm/rgm_cnfg.h>
#include <rgm_intention.h>
#include <sys/rsrc_tag.h>
#include <rgm/rgm_msg.h>
#include <scadmin/scconf.h>
#include <scadmin/scrgadm.h>
#include <rgm/rgm_errmsg.h>
#include <rgm/rgm_util.h>
#include <vector>
using namespace std;

// Zones support
#include <rgm_logical_node_manager.h>
#include <rgm_obj_manager.h>

//
// rgnodelist_subset_rtnodelist
//
// Returns SCHA_ERR_NOERR if the RG nodelist pointed to by 'rg'
// is a subset of the RT installed nodelist pointed to by 'rt'.
// Otherwise returns SCHA_ERR_INVAL and formats an explicit error message.
//
// Called when adding a resource or updating a resource group.
//
scha_errmsg_t
rgnodelist_subset_rtnodelist(char *rg_name, nodeidlist_t *nodelist,
    rgm_rt_t *rt)
{
	nodeidlist_t *nl, *rt_nl;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	LogicalNode *lnNode;


	//
	// if 'ALL' nodes are in the RT installed node list,
	// then the RG nodelist is automatically a subset
	//
	if (rt->rt_instl_nodes.is_ALL_value)
		return (res);

	// for all nodes within the RG nodelist
	//
	// Called on the President ONLY.
	// All the LNIs should have been set so we can use them
	// for the comparison when zones are supported.
	//

	// preliminary check
	for (rt_nl = rt->rt_instl_nodes.nodeids; rt_nl != NULL;
	    rt_nl = rt_nl->nl_next)
		CL_PANIC(rt_nl->nl_lni != LNI_UNDEF);

	for (nl = nodelist; nl != NULL; nl = nl->nl_next) {
		CL_PANIC(nl->nl_lni != LNI_UNDEF);
		if (!nodeidlist_contains_lni(rt->rt_instl_nodes.nodeids,
		    nl->nl_lni)) {
			res.err_code = SCHA_ERR_INVAL;
			// need a utility function in lnManager to simplify this
			lnNode = lnManager->findObject(nl->nl_lni);
			CL_PANIC(lnNode != NULL);
			rgm_format_errmsg(&res, REM_RT_NODELIST,
			    rg_name, rt->rt_name, lnNode->getFullName());
			return (res);
		}
	}
	return (res);

}



//
// is_logical_node_in_rg_nodelist
//
// Called by idl_scrgadm_rt_update_instnodes() when deleting a node from
// the installed nodes list to verify that no resource of type 'rt' is
// configured in any RG whose Nodelist contains the node 'nname'.
//
// The lock is already held.
//
static scha_errmsg_t
is_logical_node_in_rg_nodelist(const rgm_rt_t *rt, rgm::lni_t lni)
{
	rglist_p_t rg;
	rlist_p_t r;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	LogicalNode *lnNode;

	for (rg = Rgm_state->rgm_rglist; rg != NULL; rg = rg->rgl_next) {
		for (r = rg->rgl_resources; r != NULL; r = r->rl_next) {
			if (r->rl_ccrtype != rt)
				continue;

			//
			// Found an R of this resource type;
			// Does its containing RG
			// have 'lni' in its nodelist?
			//
			if (nodeidlist_contains_lni(rg->rgl_ccr->rg_nodelist,
			    lni)) {
				// yes, it does
				res.err_code = SCHA_ERR_INVAL;

				lnNode = lnManager->findObject(lni);
				// All the LNI should exist on the president
				// at this stage.
				CL_PANIC(lnNode != NULL);
				rgm_format_errmsg(&res, REM_RT_NODELIST,
				    rg->rgl_ccr->rg_name, rt->rt_name,
				    lnNode->getFullName());
				return (res);
			}
		}
	}
	// We found no resources of this type configured in an RG whose
	// nodelist contains 'nname', so it's OK to delete 'nname' from
	// the installed nodes list.
	return (res);

}

//
// update_instnodes_add_checks
//
// Called by idl_scrgadm_rt_update_instnodes().
// Performs sanity checks on names being added to installed nodes list:
// valid hostname; duplicates.  These checks are not required when the
// new list is the 'ALL' value.
//
static scha_errmsg_t
update_instnodes_add_checks(const nodeidlist_t *instnodes_id)
{
	const nodeidlist_t *ndl1 = NULL, *ndl2 = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	for (ndl1 = instnodes_id; ndl1 != NULL; ndl1 = ndl1->nl_next) {
		for (ndl2 = ndl1->nl_next;
		    ndl2 != NULL; ndl2 = ndl2->nl_next) {
			if (ndl1->nl_lni == ndl2->nl_lni) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    REM_DUPNODE_IN_NODELIST);
				return (res);
			}
		}
	}
	return (res);
}

//
// update_instnodes_del_checks
//
// Called by idl_scrgadm_rt_update_instnodes().
// Performs sanity check on names being deleted from installed nodes list:
// it's illegal to delete a node that appears in the Nodelist of the
// containing RG of an R of this type.
//
//
//
static scha_errmsg_t
update_instnodes_del_checks(const nodeidlist_t *instnodes_nl,
    const rgm_rt_t *rt, rgm::lni_t lni)
{
	const nodeidlist_t *nl = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	for (nl = instnodes_nl; nl != NULL; nl = nl->nl_next) {
		CL_PANIC(nl->nl_lni != LNI_UNDEF);
		if (nl->nl_lni == lni) {
			//
			// name from old list exists in new list;
			// so it's not being deleted
			//
			return (res);
		}
	}
	if ((res = is_logical_node_in_rg_nodelist(rt, lni)).err_code !=
	    SCHA_ERR_NOERR) {
		return (res);
	}

	return (res);
}

//
// idl_scrgadm_rt_update_instnodes
//
// Nodes may be added/deleted to/from the RT Installed_nodes list at run
// time, except during reconfiguration.  Deletion requires that no
// resource of this type is configured in any RG whose Nodelist contains
// the node to be deleted.
//
// Handle 4 different cases:
// 1. the old list was the 'ALL' value and the new list is the 'ALL' value
//	no checks are necessary
// 2. the old list was the 'ALL' value and the new list is an explicit list
//	do end-ish RG checks, deletion checks and add checks
// 3. the old list was an explicit list and the new list is an explicit list
//	do end-ish RG checks, deletion checks and add checks
// 4. the old list was an explicit list and the new list is the 'ALL' value
//	do end-ish RG checks.  Add checks are not necessary because the
//	new list is 'ALL', a known good value.  Deletion checks are not
//	necessary because 'ALL' implies no deletions.
//
//	We are now using the "installed_nodes" property to pass the value
//	of RT_System. Before doing any actual "installed_nodes" work,
//	check the installed_nodes namelist for node names which represent
//	RT_System values, set the RT_System property of the RT as needed,
//	and strip these "synthetic" node names from the namelist before
//	further processing. We can't modify the passed in node_name list,
//	so copy it to a namelist_t which we can then modify if required.
//
void
rgm_comm_impl::idl_scrgadm_rt_update_instnodes(
    const rgm::idl_rt_update_instnode_args &rt_args,
    rgm::idl_regis_result_t_out rtui_res, Environment &)
{
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	rgm_rt_t *rt;
	rglist_p_t rg;
	rlist_p_t r;
	boolean_t rt_system = B_FALSE;
	nodeidlist_t *old_nl = NULL;
	namelist_t *synth_nl = NULL;
	char *nodeidstr;
	rgm::idl_string_seq_t nseq;
	LogicalNodeset pending_boot_ns;
	LogicalNodeset r_restart_ns;
	char *rtname;
	namelist_t *instnodes_nl = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	bool need_cond_wait = false;
	LogicalNodeset intention_ns;
	nodeidlist_t *instnodes_nodeidlist = NULL;
	uint_t		prevNode;
	uint_t		currNode;
	LogicalNodeset addedNs;
	LogicalNodeset removedNs;
	LogicalNodeset emptyNodeset;
	bool old_list_was_explicit = false;

	rgm_lock_state();

	//
	// strdup to avoid Error: Non-const function
	// CORBA::String_field::operator char*() called for const object
	//
	if ((rtname = strdup_nocheck(rt_args.idlstr_rt_name)) == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto update_rt_fin; //lint !e801
	}

	//
	// scrgadm already looked up the real name for this RT,
	// so we can use the name as-is.
	//
	if ((rt = rtname_to_rt(rt_args.idlstr_rt_name)) == NULL) {
		res.err_code = SCHA_ERR_RT;
		rgm_format_errmsg(&res, REM_INVALID_RT,
		    (const char *)rt_args.idlstr_rt_name);
		goto update_rt_fin; //lint !e801
	}

	if (rt_args.node_names.length() == 0) {
		// zero-length nodelist (shouldn't ever happen)
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RT_0_NODELIST,
		    (const char *)rt_args.idlstr_rt_name);
		goto update_rt_fin; //lint !e801
	}

	//
	//	Handle RT_System "synthetic" node name:
	//		1. Copy/convert idl_string_seq_t node_names to
	//		   "namelist_t"
	//		(can't modify original idl_string_seq_t)
	//
	//		2. Check for RT_System "synthetic" nodename in
	//		   node_names list.
	//
	//		3. If found, set RT_System property as needed.
	//
	//		4. Strip this nodename from the namelist so "real"
	//		   installed_nodes processing of the namelist can
	//		   proceeed normally.
	//
	//	convert_value_string_array converts idl_string_seq_t to
	//	namelist_t.
	//
	//	Don't need to check return value for NULL - if malloc fails
	//	convert_value_string_array will "self-destruct"
	//
	// (This code assumes that "scrgadm" does not permit an
	// empty node list with the "-h" flag. If this changes,
	// then this section of code must also be changed.)
	//

	//
	// Note that we use namelist_t and not nodeidlist_t because
	// of the special values SCHA_RTSYS_TRUE, SCHA_RTSYS_FALSE,
	// SCHA_ALL_SPECIAL_VALUE.
	//
	instnodes_nl = convert_value_string_array(rt_args.node_names);

	//
	// Initialize rt_system var to the current value of rt->rt_system.
	// This is necessary whether or not we are updating RT_System because
	// any call to rgmcnfg_update_rttable always updates RT_System property.
	//
	rt_system = rt->rt_system;

	//
	// Check for "synthetic" node name. If present, "scrgadm" will have
	// placed it at the head of the namelist. (there will be at most one
	// synthetic node name in the list).
	//
	synth_nl = instnodes_nl;
	if ((strcmp(synth_nl->nl_name, SCHA_RTSYS_TRUE) == 0) ||
	    (strcmp(synth_nl->nl_name, SCHA_RTSYS_FALSE) == 0)) {
		//
		// Synthetic node name was found.
		// set rt_system var appropriately
		//
		if (strcmp(synth_nl->nl_name, SCHA_RTSYS_TRUE) == 0)
			rt_system = B_TRUE;
		else
			rt_system = B_FALSE;

		//
		// Remove synthetic node name elt. from the namelist,
		// and free it.
		//
		instnodes_nl = synth_nl->nl_next;

		synth_nl->nl_next = NULL;
		rgm_free_nlist(synth_nl);

		if (instnodes_nl == NULL) {
			//
			// (This code assumes that "scrgadm" does not permit an
			// empty node list with the "-h" flag. If this changes,
			// then this section of code must also be changed.)
			//
			// If instnodes_nl is now empty, then we are updating
			// RT_System property only, not the installed_nodes
			// property.
			//
			// write to CCR here, and bypass downstream
			// processing of "installed_nodes"
			//
			// The intention.nset is empty, since we are not adding
			// any nodes to "installed_nodes". This also means that
			// no INIT methods will be run as result of update, and
			// so don't need to cond-wait for INIT methods to
			// finish.
			//
			// Latch the intention on all slaves

			(void) latch_intention(rgm::RGM_ENT_RT,
			    rgm::RGM_OP_UPDATE,
			    (const char *)rt_args.idlstr_rt_name,
			    &emptyNodeset, rgm::INTENT_SYSTEM_ONLY);
			//
			// Don't update "installed_nodes", so "nodestr" arg.
			// is NULL. rt_system var. is set to new value of
			// RT_System.
			//
			res = rgmcnfg_update_rttable(rt, NULL, rt_system, ZONE);
			if (res.err_code != SCHA_ERR_NOERR) {
				ucmm_print("RGM", NOGET(
				    "CCR update of RT_System failed of "
				    "RT %s.\n"), rtname);
				(void) unlatch_intention();
				goto update_rt_fin; //lint !e801
			}
			(void) process_intention();
			(void) unlatch_intention();

			//
			// Bypass "installed_nodes" property updating
			//
			goto update_rt_fin; //lint !e801

		} /* if (instnodes_nl == NULL) */

	} /* if (synth_nl != NULL) */

	//
	// case 1
	//
	if (rt->rt_instl_nodes.is_ALL_value &&
	    (strcmp(instnodes_nl->nl_name, SCHA_ALL_SPECIAL_VALUE) == 0)) {
		//
		// there is no work to do
		//
		res.err_code = SCHA_ERR_NOERR;
		goto update_rt_fin; //lint !e801
	}

	//
	// For cases 2, 3, 4,
	// affected RGs must be in end-ish states.
	//
	for (rg = Rgm_state->rgm_rglist; rg != NULL; rg = rg->rgl_next) {
		for (r = rg->rgl_resources; r != NULL; r = r->rl_next) {
			if (r->rl_ccrtype != rt)
				continue;
			//
			// found an R of this resource type;
			// make sure its containing RG is endish
			//
			if (!in_endish_rg_state(rg, &pending_boot_ns, NULL,
			    NULL, &r_restart_ns) ||
			    !pending_boot_ns.isEmpty() ||
			    !r_restart_ns.isEmpty()) {
				res.err_code = SCHA_ERR_RGRECONF;
				rgm_format_errmsg(&res, REM_RT_RG_BUSY,
				    rg->rgl_ccr->rg_name,
				    r->rl_ccrdata->r_name, rt->rt_name);
				goto update_rt_fin; //lint !e801
			}
		} /* for r = ... ) */
	} /* for (rg = ... ) */

	//
	// Convert the namelist to a nodeidlist and
	// create the missing LNI mappings if the RT_instlnodes is not
	// the special value, including ALL Logical Nodes.
	//
	// The nodeidlist will be used later in the call to
	// rgmcnfg_update_rttable, so it's important to keep the code
	// that converts the namelist to nodeidlist, even if other code
	// changes here.
	//
	if (strcmp(instnodes_nl->nl_name, SCHA_ALL_SPECIAL_VALUE) != 0) {
#if SOL_VERSION >= __s10
		res = convert_zc_namelist_to_nodeidlist(instnodes_nl,
		    &instnodes_nodeidlist, ZONE);
#else
		res = convert_namelist_to_nodeidlist(instnodes_nl,
		    &instnodes_nodeidlist);
#endif
		if (SCHA_ERR_NOERR != res.err_code) {
			goto update_rt_fin; //lint !e801
		}

		//
		// Create LNI mappings and send them to the slaves
		//
		// Ideally, we'd wait until we're sure the update will
		// succeed, but later code uses the lnis
		//
		create_lni_mappings_from_nodeidlist(
		    instnodes_nodeidlist, B_TRUE);
	}

	//
	// case 2
	//
	if (rt->rt_instl_nodes.is_ALL_value) {
		//
		// The old value is the 'ALL' value but the new isn't.
		// (If it were, we wouldn't get here because we returned
		// in case 1, above.)
		// We are not adding any new names, but we still need
		// to check for duplicates and bogus names.
		// We might be deleting some names, so do deletion checks.
		//

		// do add checks
		if ((res = update_instnodes_add_checks(
		    instnodes_nodeidlist)).err_code != SCHA_ERR_NOERR) {
			goto update_rt_fin; //lint !e801
		}

		// do deletion checks
		LogicalNodeset *nset = lnManager->get_all_server_logical_nodes(
		    rgm::LN_UNCONFIGURED);
		prevNode = 1;
		while ((currNode = nset->nextLniSet(prevNode)) != 0) {
			res = update_instnodes_del_checks(
			    instnodes_nodeidlist, rt, currNode);

			if (res.err_code != SCHA_ERR_NOERR) {
				delete (nset);
				goto update_rt_fin; //lint !e801
			}
			prevNode = currNode + 1;
		}
		delete (nset);
	} else {
		//
		// The old list was not the 'ALL' value.  Set a flag to
		// indicate LNIs may be reclaimed.
		//
		old_list_was_explicit = true;
	}

	//
	// case 3
	//
	// Neither value is the 'ALL' value.
	// Do sanity checks of new nodenames.
	// Determine which nodenames are being deleted and do deletion checks.
	//
	if (!rt->rt_instl_nodes.is_ALL_value &&
	    (strcmp(instnodes_nl->nl_name, SCHA_ALL_SPECIAL_VALUE) != 0)) {
		//
		// Do sanity check of new nodenames.
		//
		if ((res = update_instnodes_add_checks(
		    instnodes_nodeidlist)).err_code != SCHA_ERR_NOERR)
			goto update_rt_fin; //lint !e801

		//
		// Determine which nodenames are being deleted
		// and do sanity checks.
		//
		for (old_nl = rt->rt_instl_nodes.nodeids; old_nl != NULL;
		    old_nl = old_nl->nl_next) {

			CL_PANIC(old_nl->nl_lni != LNI_UNDEF);
			res = update_instnodes_del_checks(
				instnodes_nodeidlist, rt, old_nl->nl_lni);
			if (res.err_code != SCHA_ERR_NOERR) {
				goto update_rt_fin; //lint !e801
			}
		}
	} /* if (!rt->rt_instl_nodes.is_ALL_value && ... */

	//
	// case 4
	//
	// The new list is the 'ALL' value, but the old is not.
	// (If it were, we wouldn't get here because we returned
	// in case 1, above.)
	// We are not deleting any names,
	// but we might be adding new nodenames to the list.
	// We don't need to do add checks because 'ALL' is a known good value.
	// We already did end-ish RG checks, above.
	//

	//
	// For all 4 cases,
	// If Init_nodes=RT_installed_nodes, all resources of this type
	// residing in managed RGs need to have INIT run on the nodes that
	// are newly added to the list.
	//
	// Additionally, FINI must be run on all deleted nodes.
	//

	if (rt->rt_init_nodes == SCHA_INFLAG_RT_INSTALLED_NODES) {

		LogicalNodeset *oldInstlNs = NULL;
		// original RT_installed_nodes
		if (rt->rt_instl_nodes.is_ALL_value) {
			oldInstlNs = lnManager->get_all_server_logical_nodes(
				rgm::LN_DOWN);
		} else {
			oldInstlNs = get_logical_nodeset_from_nodelist(
				rt->rt_instl_nodes.nodeids);
		}

		LogicalNodeset *newInstlNs = NULL;
		if (strcmp(instnodes_nl->nl_name,
		    SCHA_ALL_SPECIAL_VALUE) == 0) {
			newInstlNs = lnManager->get_all_server_logical_nodes(
				rgm::LN_DOWN);
		} else {
			newInstlNs = get_logical_nodeset_from_nodelist(
				instnodes_nodeidlist);
		}
		addedNs = *newInstlNs - *oldInstlNs;
		removedNs = *oldInstlNs - *newInstlNs;

		delete(newInstlNs);
		delete(oldInstlNs);


		char *ucmm_buffer1 = NULL;
		char *ucmm_buffer2 = NULL;
		if ((addedNs.display(ucmm_buffer1) == 0) &&
		    (removedNs.display(ucmm_buffer2) == 0)) {
			ucmm_print("RGM",
			    NOGET("Updating RT Installed_nodes: "
				"run init on nodeset %s\nrun fini "
				"on nodset %s\n"), ucmm_buffer1, ucmm_buffer2);
			free(ucmm_buffer1);
			free(ucmm_buffer2);
		}
	} /* if (rt->rt_init_nodes == SCHA_INFLAG_RT_INSTALLED_NODES) */

	//
	// Latch the intention on all slaves
	//
	// If init node is RT_INSTALLED_NODES, we must run INIT on
	// all resources of this RT on all nodes newly added.
	//
	// Additionally, if we're at version 1, we must run FINI on
	// all resources of this RT on all nodes newly removed.
	//
	// No need to run INIT or FINI if methods are not registered or if
	// init_nodes is RG_PRIMARIES, not RT_INSTALLED_NODES.
	//
	if (rt->rt_init_nodes == SCHA_INFLAG_RT_INSTALLED_NODES) {
		//
		// We tell the slave to run INIT on added nodes
		// and FINI on removed nodes.
		//
		PerNodeFlagsT node_flags;

		if (is_method_reg(rt, METH_INIT, NULL)) {
			node_flags.push_back(make_pair(&addedNs,
			    rgm::INTENT_ACTION));
			need_cond_wait = true;
		}
		if (is_method_reg(rt, METH_FINI, NULL)) {
			node_flags.push_back(make_pair(&removedNs,
			    rgm::INTENT_RUN_FINI));
			need_cond_wait = true;
		}

		//
		// Make a call directly to the helper version of this
		// function.
		//
		(void) latch_intention_pernodeflags(rgm::RGM_ENT_RT,
		    rgm::RGM_OP_UPDATE, (const char *)rt_args.
		    idlstr_rt_name, 0, node_flags);
	} else {
		//
		// Not running any methods.
		//
		(void) latch_intention(rgm::RGM_ENT_RT, rgm::RGM_OP_UPDATE,
		    (const char *)rt_args.idlstr_rt_name, &emptyNodeset, 0);
	}

	if (strcmp(instnodes_nl->nl_name, SCHA_ALL_SPECIAL_VALUE) == 0) {
		// write the CCR
		//
		// If we are updating RT_System, then rt_system var. is the
		// new value for this property. Otherwise it is the current
		// value of rt->rt_system.
		//
		res = rgmcnfg_update_rttable(rt, SCHA_ALL_SPECIAL_VALUE,
		    rt_system, ZONE);
	} else {
		// We know instnodes_nodeidlist will be initialized if
		// we are in this else statement, because it's initialized
		// above if the nodelist is not ALL.
		nodeidstr = nodeidlist2str(instnodes_nodeidlist);
		// write the CCR
		res = rgmcnfg_update_rttable(rt, nodeidstr, rt_system, ZONE);
		free(nodeidstr);
	}

	if (res.err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM", NOGET(
		    "CCR update of RT_install_nodes failed of RT %s.\n"),
		    rtname);
		(void) unlatch_intention();
		goto update_rt_fin; //lint !e801
	}

	(void) process_intention();

	// Unlatch the intention on all slaves
	(void) unlatch_intention();

	if (!need_cond_wait) {
		// no need to call cond_wait
		goto update_rt_fin; //lint !e801
	}

	//
	// Wait for all slaves in ns to finish running INIT and FINI
	// methods on the resources of this rt type.
	//
	//
	// No need to fetch state: the state will have been pushed to us
	// already.
	//
	// flush the state change queue first, so that we get all the latest
	// changes from the slaves.
	//
	rgm_unlock_state();
	flush_state_change_queue();
	rgm_lock_state();

	// Sleep on slavewait condition variable until all nodes in
	// ns have run init or fini methods or died.

	//
	// Technically, we should only wait on resource groups containing
	// resources of the edited type.  However, the chances of multiple
	// RT/RG edits going on in parallel is slight, so it's very unlikely
	// that we'll be waiting on RGs that were not operated on above.
	//
	for (rg = Rgm_state->rgm_rglist; rg != NULL; rg = rg->rgl_next) {
		LogicalNodeset unionNodeset = addedNs | removedNs;

		rgm::lni_t j = 0;
		while ((j = unionNodeset.nextLniSet(j + 1)) != 0) {
			while (rg->rgl_xstate[j].rgx_state ==
			    rgm::RG_OFF_PENDING_METHODS) {
				ucmm_print("idl_scrgadm_rt_update_instnodes",
				    NOGET("update_instnodes wait slavewait\n"));
				cond_slavewait.wait(&Rgm_state->rgm_mutex);
				ucmm_print("idl_scrgadm_rt_update_instnodes",
				    NOGET("after update_instnodes wait "
					"slavewait\n"));
			}
		}
	} /* for (rg = ... ) */

	//
	// If the old installed_nodes list was an explicit list (case 3
	// or case 4), then we might have deleted a zonename which can
	// now be reclaimed.
	//
	if (old_list_was_explicit) {
		lnManager->reclaimLnis();
	}

update_rt_fin:

	retval->ret_code = res.err_code;
	if (res.err_code == SCHA_ERR_NOERR) {
		retval->idlstr_err_msg = (char*)NULL;
	} else {
		retval->idlstr_err_msg = new_str(res.err_msg);
	}

	if (res.err_msg != NULL)
		free(res.err_msg);

	if (instnodes_nl != NULL)
		rgm_free_nlist(instnodes_nl);

	if (instnodes_nodeidlist != NULL) {
		rgm_free_nodeid_list(instnodes_nodeidlist);
	}

	if (rtname != NULL)
		free(rtname);

	rtui_res = retval;

	rgm_unlock_state();
}

//
// idl_scrgadm_rt_delete
//
// Delete a resource type from the system,
// provided there are no resources of this type.
//
// locale is not used yet: don't provide the name in the
// arg list to avoid spurious warnings.
//
void
rgm_comm_impl::idl_scrgadm_rt_delete(const char *rtname,
	const char *, rgm::idl_regis_result_t_out rtui_res, Environment &)
{
	rgm_rt_t *rtp;
	rglist_p_t rgp;
	rlist_p_t rp;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *rtname_dup = NULL;
	sc_syslog_msg_handle_t handle;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	LogicalNodeset emptyNodeset;


	rgm_lock_state();

	ucmm_print("RGM", NOGET("rt_delete():name <%s>\n"), rtname);

	//
	// scrgadm already looked up the real name for this RT,
	// so we can use the name as-is.
	//
	if ((rtp = rtname_to_rt(rtname)) == NULL) {
		res.err_code = SCHA_ERR_RT;
		rgm_format_errmsg(&res, REM_INVALID_RT, rtname);
		goto delete_rt_fin; //lint !e801
	}

	//
	// "system" property checks:
	//
	//	Can't delete a "system" RT
	//
	if (rtp->rt_system) {
		res.err_code = SCHA_ERR_ACCESS;
		rgm_format_errmsg(&res, REM_SYSTEM_RT, rtname);
		ucmm_print("RGM",
		    NOGET("Cannot delete RT <%s> whose RT_SYSTEM property "
		    "is TRUE\n"), rtname);
		goto delete_rt_fin; //lint !e801
	}

	//
	// Check to see that there are no resources of this type.
	//
	// We also check that no RG is currently busy "UPDATING".  We have to
	// check this because it's possible that the thread that set the
	// UPDATING flag is *adding* a resource of the same type that we're
	// trying to delete; this other thread might currently be running
	// VALIDATE methods, and therefore would have temporarily released
	// the rgm state mutex.  Since deletion of an RT is relatively rare,
	// it seems acceptable to require that no RG is being updated at
	// the same time.
	//
	for (rgp = Rgm_state->rgm_rglist; rgp != NULL; rgp = rgp->rgl_next) {
		if (rgp->rgl_switching == SW_UPDATING ||
		    rgp->rgl_switching == SW_UPDATE_BUSTED) {
			res.err_code = SCHA_ERR_RGRECONF;
			rgm_format_errmsg(&res, REM_RT_DEL_RG_BUSY, rtname,
			    rgp->rgl_ccr->rg_name);
			goto delete_rt_fin; //lint !e801
		}
		for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
			if (rp->rl_ccrtype == rtp) {
				// found an R of this type, so
				// cannot delete the RT
				res.err_code = SCHA_ERR_DELETE;
				rgm_format_errmsg(&res, REM_RT_DEL_R_EXISTS,
				    rp->rl_ccrdata->r_name, rtname);
				ucmm_print("RGM",
				    NOGET("Cannot delete RT <%s> "
				    "because of R <%s>\n"),
				    rtname, rp->rl_ccrdata->r_name);
				goto delete_rt_fin; //lint !e801
			}
		}
	}

	if ((rtname_dup = strdup_nocheck(rtname)) == NULL) {
		// we're out of memory
		res.err_code = SCHA_ERR_NOMEM;
		goto delete_rt_fin; //lint !e801
	}

	// Do the latch/unlatch processing,
	// and remove RT from the CCR.  The slave side
	// of intention processing will remove the RT
	// from the internal state structure on each slave.

	// Latch the intention on all slaves.

	(void) latch_intention(rgm::RGM_ENT_RT, rgm::RGM_OP_DELETE,
	    rtname, &emptyNodeset, 0);

	if ((res = rgmcnfg_remove_rttable(rtname_dup, ZONE)).err_code !=
	    SCHA_ERR_NOERR)
		ucmm_print("RGM", NOGET("remove RT <%s> CCR Failed\n"),
		    rtname);
	else {
		ucmm_print("RGM", NOGET("remove RT <%s> Processing\n"),
		    rtname);

		// Log resource type REMOVED event for SC Manager.

		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RT_TAG,
		    rtname);
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that a resource type
		// has been deleted. This message can be used by system
		// monitoring tools.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, REMOVED,
		    SYSTEXT("resource type %s removed."), rtname);
		sc_syslog_msg_done(&handle);

		(void) process_intention();
	}

	// Unlatch the intention on all slaves
	(void) unlatch_intention();

	// reclaim LNIs that might have been freed by RT removal
	lnManager->reclaimLnis();

delete_rt_fin:

	free(rtname_dup);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	rtui_res = retval;
	rgm_unlock_state();
}
