//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//


#pragma ident	"@(#)rgm_rg_util.cc	1.2	09/02/19 SMI"

#include <rgm_proto.h>
#include <rgm/rgm_cnfg.h>

//
// Concats "clustername" and "objname" to the format
// <clustername>:<objname>. If "clustername" is NULL, this method
// just copies "objname" to "fullname". It is the callers
// responsibility to free the memory of "fullname".
//
scha_errmsg_t
rgm_add_cluster_scope_to_obj_name(const char *objname,
    const char *clustername, char **fullname) {

	char *tmp_name;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	int len_count;

	//
	// 1 for the seperator and 1 for NULL
	//
	len_count = 2;
	if (objname == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	//
	// Check whether "clustername" was specified.
	//
	if (clustername == NULL) {
		//
		// There was no cluster name specified.
		//
		*fullname = strdup(objname);
		return (res);
	}

	//
	// If we are here, then scoping is required.
	//
	len_count += strlen(objname) + strlen(clustername);
	*fullname = (char *)calloc(len_count, 1);

	if (NULL == *fullname) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	strcat(*fullname, clustername);
	strcat(*fullname, CLUSTER_NAME_DELIMITER_STR);
	strcat(*fullname, objname);
	return (res);
}

//
// This method searches through the list of affinitents of
// "affinity_rg" for the affinitent RG "affinitnent_rg" and the
// affinity type "aff_type". This method can also be used to determine
// the affinity type a remote cluster RG has on "affinity_rg". This
// method sets the value of "affinity_exists" to true if the
// affinity type exists (specified by "aff_type"). Else it sets
// "affinity_exists" to false.
//
scha_errmsg_t
check_rg_affinity_type(const rglist_p_t affinity_rg, const char *affinitent_rg,
    const char *affinitent_cluster, const affinitytype_t aff_type,
    boolean_t *affinity_exists) {


	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	namelist_t *rg_affinitents;
	char *full_rg_name = NULL;

	if (affinity_rg == NULL || affinitent_rg == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	//
	// First determine what is the type of affinity we are
	// looking for.
	//
	switch (aff_type) {
	case AFF_STRONG_POS:
		rg_affinitents = affinity_rg->rgl_affinitents.ap_strong_pos;
		break;
	case AFF_WEAK_POS:
		rg_affinitents = affinity_rg->rgl_affinitents.ap_weak_pos;
		break;
	case AFF_STRONG_NEG:
		rg_affinitents = affinity_rg->rgl_affinitents.ap_strong_neg;
		break;
	case AFF_WEAK_NEG:
		rg_affinitents = affinity_rg->rgl_affinitents.ap_weak_neg;
		break;
	default:
		//
		// Unknown affinity type
		//
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	//
	// Set the default value
	//
	*affinity_exists = B_FALSE;

	//
	// Get the fully qualified name of the affinitent RG.
	// We will have to seach for the affinitent rg using its
	// fully qualified name.
	//
	if ((affinitent_cluster == NULL) ||
	    (strcmp(affinitent_cluster, ZONE) == 0)) {
		full_rg_name = strdup(affinitent_rg);
	} else {
		res = rgm_add_cluster_scope_to_obj_name(affinitent_rg,
		    affinitent_cluster,
		    &full_rg_name);

		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
	}

	//
	// Search in affinities list
	//
	for (; rg_affinitents != NULL;
	    rg_affinitents = rg_affinitents->nl_next) {
		if (strcmp(rg_affinitents->nl_name, full_rg_name) != 0) {
			continue;
		}

		//
		// If we are here, then we have found the affinitent
		//
		*affinity_exists = B_TRUE;
		break;
	}

	//
	// free memory
	//
	if (full_rg_name) {
		free(full_rg_name);
		full_rg_name = NULL;
	}

	return (res);
}

//
// This method sets the ok_to_start flag for all the
// resource groups in Rgm_state. If this method is
// called on a president node, then it will update
// the value of Ok_to_start in the ccr. Ok_to_start
// will added in an rg table, if the rg has
// auto_start_on_new_cluster as true. Else, this
// method will remove the Ok_to_start entry. This
// method iterates of the list of RG in Rgm_state
// and invokes rgmcnfg_set_ok_to_start() to
// achieve this functionality.
//
scha_errmsg_t
rgm_set_ok_to_start_for_all_rg() {

	rglist_p_t rg_ptr;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	ucmm_print("RGM", "In rgm_set_ok_to_start_for_all_rg");

	for (rg_ptr = Rgm_state->rgm_rglist; rg_ptr != NULL;
	    rg_ptr = rg_ptr->rgl_next) {
		//
		// If the cluster is rebooting, the president adds/removes
		// Ok_To_Start entry in ccr table depending on the setting of
		// auto_start_on_new_cluster and syncs-up the ok_to_start flag.
		// New president on old cluster and slaves set the in-memory
		// ok_to_start flag according to the existence of Ok_To_Start
		// in the ccr.
		//
		if (am_i_president() & Rgm_state->is_thisnode_booting) {
			rg_ptr->rgl_ok_to_start =
			    rg_ptr->rgl_ccr->rg_auto_start;
			ucmm_print("RGM", "ok to start:%d",
			    rg_ptr->rgl_ok_to_start);
			res = rgmcnfg_set_ok_start_rg(
			    rg_ptr->rgl_ccr->rg_name,
			    rg_ptr->rgl_ok_to_start, ZONE);
			if (res.err_code != SCHA_ERR_NOERR) {
				// CCR update failed. Stop further processing
				// and return early.
				ucmm_print("RGM", NOGET("CCR update of"
				    "Ok_To_Start flag failed for resource"
				    "group %s.\n"), rg_ptr->rgl_ccr->rg_name);
				return (res);
			}
			//
			// Re-read rg_stamp from the CCR.  We have to do this
			// here on the president because the value of rg_stamp
			// will now become stale. The slaves have not yet read
			// CCR at this point, so they will pick up the correct
			// rg_stamp value.
			//
			res = update_rg_seq_id(rg_ptr);
			if (res.err_code != SCHA_ERR_NOERR)
				return (res);
		} else
			rg_ptr->rgl_ok_to_start =
			    rgmcnfg_is_ok_start_rg(rg_ptr->rgl_ccr->rg_name,
				ZONE);
	}

	return (res);
}
