/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)rgm_rs.cc	1.52	08/06/20 SMI"

//
// rgm_rs.cc
//
// These call_xxx() functions for RSs are needed for the public API.  They
// are called by the receptionist thread, which provides storage for the
// arguments.  These functions then directly contact the President node.
// The IDL invocation on the president node fills in the result structure,
// including a return code.  IDL errors or other communication errors may
// also be returned by these functions.
//
// Important Note: Assignment of const char * to an IDL String_field
// (idlstr_xxx is our RGM naming convention) makes a copy of the string,
// which is deleted by the [IDL compiler-generated] destructor upon
// leaving the function.  Assignment of char * to idlstr does NOT
// automatically make a copy of the string, but the destructor still does
// a delete upon leaving the function.  To simplify coding and avoid
// coding errors, we ALWAYS make a copy of the string via new_str()
// regardless of whether the right hand side is a char * or a const char *.
// The right thing will happen.  Upon leaving the function, the
// destructor will delete the new-ed memory and there will be no memory
// leaks.
//
// Rule: Subroutine MUST make copies (new_str)of input strings before
// assigning them to String_fields.  However, the subroutine doesn't need
// to explicitly delete them, because the [IDL compiler-generated]
// destructor frees them upon leaving the subroutine.
//


#include <rgm/rgm_msg.h>
#include <rgm_proto.h>
#include <rgm_state.h>
#include <rgm/rgm_call_pres.h>

#include <rgm/rgm_recep.h>

#include <rgm/rgm_errmsg.h>


#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_pres.h>
#endif

//
// call_scha_rs_get_switch
//	send the 'get res switch' request to President
//
scha_errmsg_t
call_scha_rs_get_switch(const rs_get_switch_args *getst_args,
    get_switch_result_t *getst_res)
{
#ifdef EUROPA_FARM
	rgmx_rs_get_switch_args arg;
	rgmx_rs_get_switch_result_t result_v;
	CLIENT *clnt = NULL;
	enum clnt_stat rpc_res;
#else
	rgm::idl_rs_get_switch_args arg;
	rgm::idl_get_switch_result_t_var result_v;
	rgm::rgm_comm_var prgm_pres_v;
	Environment e;
#endif
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char	*endp;
	char *zonename = NULL;

	//
	// These new_str-ed strings will be deleted by the destructor
	// upon leaving this function.
	//
	arg.idlstr_rs_name = new_str(getst_args->rs_name);
	arg.idlstr_rg_name = new_str(getst_args->rg_name);
	arg.on_off = (bool)getst_args->on_off;

	if (getst_args->node_name == NULL ||
	    getst_args->node_name[0] == '\0') {
		// Caller didn't provide nodename, so use our own.
		if (getst_args->zonename == NULL ||
		    getst_args->zonename[0] == '\0') {
			zonename = NULL;
		} else {
			zonename = getst_args->zonename;
		}
		arg.idlstr_node_name = lnManager->computeLn(
		    get_local_nodeid(), zonename);
	} else {
		arg.idlstr_node_name = new_str(getst_args->node_name);
	}

#ifndef EUROPA_FARM
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
#endif
	while (1) {
#ifdef EUROPA_FARM
		// In case we have a RPC timeout below we assume that the
		// president died and we will retry the call.
		clnt = rgmx_comm_getpres(ZONE);
		if (clnt != NULL) {
			// If president has not died, proceed.
			bzero(&result_v, sizeof (rgmx_rs_get_switch_result_t));
			rpc_res = rgmx_scha_rs_get_switch_1(&arg, &result_v,
			    clnt);
			// Call delete []: there is no destructor for arg
			// in the RPC EUROPA_FARM case.
			delete [] arg.idlstr_rs_name;
			delete [] arg.idlstr_rg_name;
			delete [] arg.idlstr_node_name;

			if ((rpc_res != RPC_SUCCESS) &&
			    (rpc_res != RPC_TIMEDOUT)) {
				// some unexpected RPC error
				res.err_code = SCHA_ERR_ORB;
				xdr_free(
				    (xdrproc_t)xdr_rgmx_rs_get_switch_result_t,
				    (char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			if (rpc_res == RPC_SUCCESS) {
				if (result_v.ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM", NOGET(
					    "call_scha_rs_get_switch(): %d"),
					    result_v.ret_code);
					res.err_code =
					    (scha_err_t)result_v.ret_code;
					goto get_switch_done;
				}
				getst_res->seq_id =
				    strdup_nocheck(
				    result_v.idlstr_seq_id);
				if (getst_res->seq_id == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto get_switch_done;
				}

				getst_res->ret_code = result_v.ret_code;

				getst_res->rs_switch =
				    strtol(result_v.idlstr_switch, &endp, 0);
				if (*endp != '\0') {
					ucmm_print("RGM",
					    NOGET("call_scha_rs_get_switch(): "
					    "strtol error"));
					res.err_code = SCHA_ERR_INTERNAL;
					goto get_switch_done;
				}

				ucmm_print("RGM",
				    NOGET("call_scha_rs_get_switch(): "
				    "switch = %d"),
				    getst_res->rs_switch);

				res.err_code = (scha_err_t)result_v.ret_code;
				if ((const char *)result_v.idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v.idlstr_err_msg);
				}
get_switch_done:
				xdr_free(
				    (xdrproc_t)xdr_rgmx_rs_get_switch_result_t,
				    (char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			xdr_free((xdrproc_t)xdr_rgmx_rs_get_switch_result_t,
			    (char *)&result_v);
			rgmx_comm_freecnt(clnt);
		}
#else // EUROPA_FARM
		if (!CORBA::is_nil(prgm_pres_v)) {
			prgm_pres_v->idl_scha_rs_get_switch(arg, result_v, e);
			res = except_to_scha_err(e);

			// If president has not died, proceed.
			if (res.err_code != SCHA_ERR_CLRECONF) {
				if (res.err_code != SCHA_ERR_NOERR) {
					// some unexpected ORB error
					return (res);
				}

				if (result_v->ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM", NOGET(
					    "call_scha_rs_get_switch(): %d\n"),
					    result_v->ret_code);
					res.err_code =
					    (scha_err_t)result_v->ret_code;
					return (res);
				}

				getst_res->seq_id =
				    strdup_nocheck(result_v->idlstr_seq_id);
				if (getst_res->seq_id == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					return (res);
				}
				getst_res->ret_code = result_v->ret_code;
				getst_res->rs_switch =
				    strtol(result_v->idlstr_switch, &endp, 0);
				if (*endp != '\0') {
					ucmm_print("RGM",
					    NOGET("call_scha_rs_get_switch(): "
					    "strtol error"));
					res.err_code = SCHA_ERR_INTERNAL;
					return (res);
				}

				ucmm_print("RGM",
				    NOGET("call_scha_rs_get_switch(): "
				    "switch = %d"),
				    getst_res->rs_switch);

				res.err_code = (scha_err_t)result_v->ret_code;
				if ((const char *)result_v->idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v->idlstr_err_msg);
				}
				return (res);
			}
		}

		//
		// President is not yet registered, or has died.  In either
		// case, a new president will be elected soon.
		// Sleep until new president is registered in name service,
		// then loop back and retry.
		//
		// Note that rgm_comm_waitforpres() is guaranteed to return
		// a new president object pointer, different from the current
		// value of prgm_pres_v.
		//
		prgm_pres_v = rgm_comm_waitforpres(prgm_pres_v);
#endif // EUROPA_FARM
	} // while (1)

	// NOTREACHED
}

//
// call_scha_rs_get_ext
// send the 'get extension prop' request to President
//
scha_errmsg_t
call_scha_rs_get_ext(const rs_get_args *rs_args,
    get_ext_result_t *rs_res)
{
#ifdef EUROPA_FARM
	rgmx_rs_get_args arg;
	rgmx_rs_get_ext_result_t result_v;
	CLIENT *clnt = NULL;
	enum clnt_stat rpc_res;
#else
	rgm::idl_rs_get_args arg;
	rgm::idl_rs_get_ext_result_t_var result_v;
	rgm::rgm_comm_var prgm_pres_v;
	Environment e;
#endif
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *node_name = NULL;
	char *zonename = NULL;

	//
	// These new_str-ed strings will be deleted by the destructor
	// upon leaving this function.
	//
	arg.idlstr_rs_name = new_str(rs_args->rs_name);
	arg.idlstr_rg_name = new_str(rs_args->rg_name);
	arg.idlstr_rs_prop_name = new_str(rs_args->rs_prop_name);

	if (rs_args->rs_node_name == NULL ||
	    rs_args->rs_node_name[0] == '\0') {
		if (rs_args->zonename == NULL ||
		    rs_args->zonename[0] == '\0')
			zonename = NULL;
		else
			zonename = rs_args->zonename;
		// Caller didn't provide nodename, so use our own.
		node_name = lnManager->computeLn(
		    get_local_nodeid(), zonename);
	} else
		node_name = new_str(rs_args->rs_node_name);

	arg.idlstr_rs_node_name = node_name;

#ifndef EUROPA_FARM
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
#endif
	while (1) {
		// In case of RPC timeout, we assume that the president
		// died and we retry the call to the new president.
#ifdef EUROPA_FARM

		clnt = rgmx_comm_getpres(ZONE);
		if (clnt != NULL) {
			// If president has not died, proceed.
			bzero(&result_v, sizeof (rgmx_rs_get_ext_result_t));
			rpc_res = rgmx_scha_rs_get_ext_1(&arg, &result_v,
			    clnt);
			// Call delete []: there is no destructor for arg
			// in the RPC EUROPA_FARM case.
			delete [] arg.idlstr_rs_name;
			delete [] arg.idlstr_rg_name;
			delete [] arg.idlstr_rs_node_name;
			delete [] arg.idlstr_rs_prop_name;
			if ((rpc_res != RPC_SUCCESS) &&
			    (rpc_res != RPC_TIMEDOUT)) {
				// some unexpected RPC error
				res.err_code = SCHA_ERR_ORB;
				xdr_free(
				    (xdrproc_t)xdr_rgmx_rs_get_ext_result_t,
				    (char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			if (rpc_res == RPC_SUCCESS) {
				if (result_v.ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM", NOGET(
					    "call_scha_rs_get_ext(): %d"),
					    result_v.ret_code);
					res.err_code =
					    (scha_err_t)result_v.ret_code;
					goto get_ext_done;
				}
				rs_res->seq_id =
				    strdup_nocheck(
				    result_v.idlstr_seq_id);
				if (rs_res->seq_id == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto get_ext_done;
				}

				if ((rs_res->value_list =
				    (strarr_list *)calloc_nocheck(1,
				    sizeof (strarr_list))) == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto get_ext_done;
				}

				if ((rs_res->value_list->strarr_list_val =
				    (char **)calloc_nocheck(1,
				    sizeof (char *))) == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto get_ext_done;
				}

				if (result_v.prop_val == NULL) {
					rs_res->value_list->strarr_list_val[0]
					    = NULL;
					rs_res->value_list->strarr_list_len
					    = 0;
				} else {
					if ((rs_res->value_list->
					    strarr_list_val[0] =
					    strdup_nocheck(result_v.prop_val))
					    == NULL) {
						res.err_code = SCHA_ERR_NOMEM;
						goto get_ext_done;
					}
					rs_res->value_list->strarr_list_len
					    = 1;
				}

				if ((rs_res->value_list->strarr_list_val[0] =
				    strdup_nocheck(result_v.prop_val))
				    == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto get_ext_done;
				}
				rs_res->ret_code =
				    (scha_err_t)result_v.ret_code;
				rs_res->prop_type = result_v.prop_type;
get_ext_done:
				xdr_free(
				    (xdrproc_t)xdr_rgmx_rs_get_ext_result_t,
				    (char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			xdr_free((xdrproc_t)xdr_rgmx_rs_get_ext_result_t,
			    (char *)&result_v);
			rgmx_comm_freecnt(clnt);
		}
#else // EUROPA_FARM
		if (!CORBA::is_nil(prgm_pres_v)) {
			prgm_pres_v->idl_scha_rs_get_ext(arg, result_v, e);
			res = except_to_scha_err(e);

			if (res.err_code != SCHA_ERR_CLRECONF) {
				if (res.err_code != SCHA_ERR_NOERR)
					return (res);
				if (result_v->ret_code != NULL) {
					res.err_code = result_v->ret_code;
					return (res);
				}
				rs_res->seq_id =
				    strdup_nocheck(result_v->idlstr_seq_id);
				if (rs_res->seq_id == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					return (res);
				}

				if ((rs_res->value_list =
				    (strarr_list *)calloc_nocheck(1,
				    sizeof (strarr_list))) == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					free(rs_res->seq_id);
					return (res);
				}

				if ((rs_res->value_list->strarr_list_val =
				(char **)calloc_nocheck(1, sizeof (char *)))
				    == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					free(rs_res->seq_id);
					return (res);
				}

				if (result_v->prop_val == NULL) {
					rs_res->value_list->strarr_list_val[0]
					    = NULL;
					rs_res->value_list->strarr_list_len
					    = 0;
				} else {
					if ((rs_res->value_list
					    ->strarr_list_val[0] =
					strdup_nocheck(result_v->prop_val))
					== NULL) {
						res.err_code = SCHA_ERR_NOMEM;
						free(rs_res->seq_id);
						return (res);
					}
					rs_res->value_list->strarr_list_len
					    = 1;
				}

				rs_res->ret_code =
				    (scha_err_t)result_v->ret_code;
				rs_res->prop_type = result_v->prop_type;
				delete result_v->idlstr_seq_id;
				return (res);
			}
		}

		//
		// President is not yet registered, or has died.  In either
		// case, a new president will be elected soon.
		// Sleep until new president is registered in name service,
		// then loop back and retry.
		//
		// Note that rgm_comm_waitforpres() is guaranteed to return
		// a new president object pointer, different from the current
		// value of prgm_pres_v.
		//
		prgm_pres_v = rgm_comm_waitforpres(prgm_pres_v);
#endif // EUROPA_FARM
	} // while (1)

	// NOTREACHED
}

//
// call_scha_rs_get_state
//	send the 'get state' request to President
//
scha_errmsg_t
call_scha_rs_get_state(const rs_get_state_status_args *gets_args,
    get_result_t *gets_res)
{
#ifdef EUROPA_FARM
	rgmx_rs_get_state_status_args arg;
	rgmx_get_result_t result_v;
	CLIENT *clnt = NULL;
	enum clnt_stat rpc_res;
#else
	rgm::idl_rs_get_state_status_args arg;
	rgm::idl_get_result_t_var result_v;
	rgm::rgm_comm_var prgm_pres_v;
	Environment e;
#endif
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	const char *zonename;

	//
	// These new_str-ed strings will be deleted by the destructor
	// upon leaving this function.
	//
	arg.idlstr_rs_name = new_str(gets_args->rs_name);
	arg.idlstr_rg_name = new_str(gets_args->rg_name);
	if (gets_args->node_name == NULL ||
	    gets_args->node_name[0] == '\0') {
		if (gets_args->zonename == NULL ||
		    gets_args->zonename[0] == '\0')
			zonename = NULL;
		else
			zonename = gets_args->zonename;
		// Caller didn't provide nodename, so use our own.
		arg.idlstr_node_name = lnManager->computeLn(
		    get_local_nodeid(), zonename);
	} else
		arg.idlstr_node_name = new_str(gets_args->node_name);

#ifndef EUROPA_FARM
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
#endif
	while (1) {
#ifdef EUROPA_FARM
		// In case we have a RPC timeout below we assume that the
		// president died and we will retry the call.
		clnt = rgmx_comm_getpres(ZONE);
		if (clnt != NULL) {
			// If president has not died, proceed.
			bzero(&result_v, sizeof (rgmx_get_result_t));
			rpc_res = rgmx_scha_rs_get_state_1(&arg,
				&result_v, clnt);
			// Call delete [], there is no destructor for
			// arg in the RPC EUROPA_FARM case.
			delete [] arg.idlstr_rs_name;
			delete [] arg.idlstr_rg_name;
			delete [] arg.idlstr_node_name;
			if ((rpc_res != RPC_SUCCESS) &&
			    (rpc_res != RPC_TIMEDOUT)) {
				// some unexpected RPC error
				res.err_code = SCHA_ERR_ORB;
				xdr_free((xdrproc_t)xdr_rgmx_get_result_t,
					(char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			if (rpc_res == RPC_SUCCESS) {
				if (result_v.ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM", NOGET(
					    "call_scha_rs_get_state(): %d"),
					    result_v.ret_code);
					res.err_code =
					    (scha_err_t)result_v.ret_code;
				} else {
					gets_res->seq_id =
						strdup_nocheck(
						result_v.idlstr_seq_id);
					if (gets_res->seq_id == NULL) {
						res.err_code = SCHA_ERR_NOMEM;
					} else {
						gets_res->ret_code =
							result_v.ret_code;
						copy_string_array(
							result_v.value_list,
							&gets_res->value_list);
						res.err_code = (scha_err_t)
						    result_v.ret_code;
					}
				}
				if ((const char *)result_v.idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v.idlstr_err_msg);
				}
				xdr_free((xdrproc_t)xdr_rgmx_get_result_t,
					(char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			xdr_free((xdrproc_t)xdr_rgmx_get_result_t,
				(char *)&result_v);
			rgmx_comm_freecnt(clnt);
		}
#else // EUROPA_FARM
		if (!CORBA::is_nil(prgm_pres_v)) {
			prgm_pres_v->idl_scha_rs_get_state(arg, result_v, e);
			res = except_to_scha_err(e);

			// If president has not died, proceed.
			if (res.err_code != SCHA_ERR_CLRECONF) {
				if (res.err_code != SCHA_ERR_NOERR) {
					// some unexpected ORB error
					return (res);
				}
				if (result_v->ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM", NOGET(
					    "call_scha_rs_get_state(): %d"),
					    result_v->ret_code);
					res.err_code =
					    (scha_err_t)result_v->ret_code;
					return (res);
				}

				gets_res->seq_id =
				    strdup_nocheck(result_v->idlstr_seq_id);
				if (gets_res->seq_id == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					return (res);
				}
				gets_res->ret_code = result_v->ret_code;
				(void) strseq_to_array(result_v->value_list,
				    &gets_res->value_list);

				res.err_code = (scha_err_t)result_v->ret_code;
				if ((const char *)result_v->idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v->idlstr_err_msg);
				}
				return (res);
			}
		}

		//
		// President is not yet registered, or has died.  In either
		// case, a new president will be elected soon.
		// Sleep until new president is registered in name service,
		// then loop back and retry.
		//
		// Note that rgm_comm_waitforpres() is guaranteed to return
		// a new president object pointer, different from the current
		// value of prgm_pres_v.
		//
		prgm_pres_v = rgm_comm_waitforpres(prgm_pres_v);
#endif // EUROPA_FARM
	} // while (1)

	// NOTREACHED
}


//
// call_scha_rs_get_status
//	send the 'get rs status' request to President
//
scha_errmsg_t
call_scha_rs_get_status(const rs_get_state_status_args *getst_args,
    get_status_result_t *getst_res)
{
#ifdef EUROPA_FARM
	rgmx_rs_get_state_status_args arg;
	rgmx_get_status_result_t result_v;
	CLIENT *clnt = NULL;
	enum clnt_stat rpc_res;
#else
	rgm::idl_rs_get_state_status_args arg;
	rgm::idl_get_status_result_t_var result_v;
	rgm::rgm_comm_var prgm_pres_v;
	Environment e;
#endif
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char	*endp;
	const char	*zonename;

	//
	// These new_str-ed strings will be deleted by the destructor
	// upon leaving this function.
	//
	arg.idlstr_rs_name = new_str(getst_args->rs_name);
	arg.idlstr_rg_name = new_str(getst_args->rg_name);

	if (getst_args->node_name == NULL ||
	    getst_args->node_name[0] == '\0') {
		if (getst_args->zonename == NULL ||
		    getst_args->zonename[0] == '\0')
			zonename = NULL;
		else
			zonename = getst_args->zonename;

		arg.idlstr_node_name = lnManager->computeLn(
		    get_local_nodeid(), zonename);
	} else
		arg.idlstr_node_name = new_str(getst_args->node_name);



#ifndef EUROPA_FARM
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
#endif
	while (1) {
#ifdef EUROPA_FARM
		// In case we have a RPC timeout below we assume that the
		// president died and we will retry the call.
		clnt = rgmx_comm_getpres(ZONE);
		if (clnt != NULL) {
			// If president has not died, proceed.
			bzero(&result_v, sizeof (rgmx_get_status_result_t));
			rpc_res = rgmx_scha_rs_get_status_1(&arg,
					&result_v, clnt);
			// Call delete [], there is no destructor for
			// arg in the RPC EUROPA_FARM case.
			delete [] arg.idlstr_rs_name;
			delete [] arg.idlstr_rg_name;
			delete [] arg.idlstr_node_name;

			if ((rpc_res != RPC_SUCCESS) &&
			    (rpc_res != RPC_TIMEDOUT)) {
				// some unexpected RPC error
				res.err_code = SCHA_ERR_ORB;
				xdr_free((xdrproc_t)
					xdr_rgmx_get_status_result_t,
					(char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			if (rpc_res == RPC_SUCCESS) {
				if (result_v.ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM", NOGET(
					    "call_scha_rs_get_status(): %d\n"),
					    result_v.ret_code);
					res.err_code =
					    (scha_err_t)result_v.ret_code;
					goto get_status_done;
				}

				getst_res->seq_id =
				    strdup_nocheck(result_v.idlstr_seq_id);
				if (getst_res->seq_id == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto get_status_done;
				}
				getst_res->ret_code = result_v.ret_code;
				getst_res->rs_status =
				    strtol(result_v.idlstr_status, &endp, 0);
				if (*endp != '\0') {
					ucmm_print("RGM",
					    NOGET("call_scha_rs_get_status(): "
					    "strtol error"));
					res.err_code = SCHA_ERR_INTERNAL;
					goto get_status_done;
				}

				if ((char *)result_v.idlstr_status_msg ==
				    NULL) {
					getst_res->status_msg = NULL;
					ucmm_print("RGM",
					    NOGET("call_scha_rs_get_status(): "
					    "status = %d, msg = NULL"),
					    getst_res->rs_status);
				} else {
					getst_res->status_msg = strdup_nocheck(
					    result_v.idlstr_status_msg);
					if (getst_res->status_msg == NULL) {
						res.err_code = SCHA_ERR_NOMEM;
						goto get_status_done;
					}
					ucmm_print("RGM",
					    NOGET("call_scha_rs_get_status(): "
					    "status = %d, msg = %s\n"),
					    getst_res->rs_status,
					    getst_res->status_msg);
				}

				res.err_code = (scha_err_t)result_v.ret_code;
				if ((const char *)result_v.idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v.idlstr_err_msg);
				}
get_status_done:
				xdr_free((xdrproc_t)
					xdr_rgmx_get_status_result_t,
					(char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			xdr_free((xdrproc_t)xdr_rgmx_get_status_result_t,
				(char *)&result_v);
			rgmx_comm_freecnt(clnt);
		}
#else // EUROPA_FARM
		if (!CORBA::is_nil(prgm_pres_v)) {
			prgm_pres_v->idl_scha_rs_get_status(arg, result_v, e);
			res = except_to_scha_err(e);

			// If president has not died, proceed.
			if (res.err_code != SCHA_ERR_CLRECONF) {
				if (res.err_code != SCHA_ERR_NOERR) {
					// some unexpected ORB error
					return (res);
				}

				if (result_v->ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM", NOGET(
					    "call_scha_rs_get_status(): %d\n"),
					    result_v->ret_code);
					res.err_code =
					    (scha_err_t)result_v->ret_code;
					return (res);
				}

				getst_res->seq_id =
				    strdup_nocheck(result_v->idlstr_seq_id);
				if (getst_res->seq_id == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					return (res);
				}
				getst_res->ret_code = result_v->ret_code;
				getst_res->rs_status =
				    strtol(result_v->idlstr_status, &endp, 0);
				if (*endp != '\0') {
					ucmm_print("RGM",
					    NOGET("call_scha_rs_get_status(): "
					    "strtol error"));
					res.err_code = SCHA_ERR_INTERNAL;
					return (res);
				}

				if ((char *)result_v->idlstr_status_msg ==
				    NULL) {
					getst_res->status_msg = NULL;
					ucmm_print("RGM",
					    NOGET("call_scha_rs_get_status(): "
					    "status = %d, msg = NULL"),
					    getst_res->rs_status);
				} else {
					getst_res->status_msg = strdup_nocheck(
					    result_v->idlstr_status_msg);
					if (getst_res->status_msg == NULL) {
						res.err_code = SCHA_ERR_NOMEM;
						return (res);
					}
					ucmm_print("RGM",
					    NOGET("call_scha_rs_get_status(): "
					    "status = %d, msg = %s\n"),
					    getst_res->rs_status,
					    getst_res->status_msg);
				}

				res.err_code = (scha_err_t)result_v->ret_code;
				if ((const char *)result_v->idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v->idlstr_err_msg);
				}
				return (res);
			}
		}

		//
		// President is not yet registered, or has died.  In either
		// case, a new president will be elected soon.
		// Sleep until new president is registered in name service,
		// then loop back and retry.
		//
		// Note that rgm_comm_waitforpres() is guaranteed to return
		// a new president object pointer, different from the current
		// value of prgm_pres_v.
		//
		prgm_pres_v = rgm_comm_waitforpres(prgm_pres_v);
#endif // EUROPA_FARM
	} // while (1)

	// NOTREACHED
}


//
// call_scha_rs_get_fail_status
//	send the 'get failed flag' request to President
//
scha_errmsg_t
call_scha_rs_get_fail_status(const rs_get_fail_status_args *getf_args,
    get_result_t *getf_res)
{
#ifdef EUROPA_FARM
	rgmx_rs_get_fail_status_args arg;
	rgmx_get_result_t result_v;
	CLIENT *clnt = NULL;
	enum clnt_stat rpc_res;
#else
	rgm::idl_rs_get_fail_status_args arg;
	rgm::idl_get_result_t_var result_v;
	rgm::rgm_comm_var prgm_pres_v;
	Environment e;
#endif
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	const char *zonename;

	//
	// These new_str-ed strings will be deleted by the destructor
	// upon leaving this function.
	//
	arg.idlstr_rs_name = new_str(getf_args->rs_name);
	arg.idlstr_rg_name = new_str(getf_args->rg_name);
	if (getf_args->node_name == NULL ||
	    getf_args->node_name[0] == '\0') {
		if (getf_args->zonename == NULL ||
		    getf_args->zonename[0] == '\0')
			zonename = NULL;
		else
			zonename = getf_args->zonename;

		// Caller didn't provide nodename, so use our own.
		arg.idlstr_node_name = lnManager->computeLn(
		    get_local_nodeid(), zonename);
	} else
		arg.idlstr_node_name = new_str(getf_args->node_name);

	arg.idlstr_method_name = new_str(getf_args->method_name);

#ifndef EUROPA_FARM
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
#endif
	while (1) {
#ifdef EUROPA_FARM
		// In case we have a RPC timeout below we assume that the
		// president died and we will retry the call.
		clnt = rgmx_comm_getpres(ZONE);
		if (clnt != NULL) {
			// If president has not died, proceed.
			bzero(&result_v, sizeof (rgmx_get_result_t));
			rpc_res = rgmx_scha_rs_get_fail_status_1(&arg,
					&result_v, clnt);
			// Call delete [], there is no destructor for
			// arg in the RPC EUROPA_FARM case.
			delete [] arg.idlstr_rs_name;
			delete [] arg.idlstr_rg_name;
			delete [] arg.idlstr_node_name;
			delete [] arg.idlstr_method_name;

			if ((rpc_res != RPC_SUCCESS) &&
			    (rpc_res != RPC_TIMEDOUT)) {
				// some unexpected RPC error
				res.err_code = SCHA_ERR_ORB;
				xdr_free((xdrproc_t)xdr_rgmx_get_result_t,
					(char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			if (rpc_res == RPC_SUCCESS) {
				if (result_v.ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM",
					    NOGET("call_scha_rs_get_fail_status"
					    "(): %d\n"), result_v.ret_code);
					res.err_code =
					    (scha_err_t)result_v.ret_code;
					goto get_fail_status_done;
				}

				getf_res->ret_code = result_v.ret_code;
				getf_res->seq_id =
				    strdup_nocheck(result_v.idlstr_seq_id);
				if (getf_res->seq_id == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto get_fail_status_done;
				}
				copy_string_array(result_v.value_list,
				    &getf_res->value_list);

				res.err_code = (scha_err_t)result_v.ret_code;
				if ((const char *)result_v.idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v.idlstr_err_msg);
				}
get_fail_status_done:
				xdr_free((xdrproc_t)
					xdr_rgmx_get_result_t,
					(char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			xdr_free((xdrproc_t)xdr_rgmx_get_result_t,
				(char *)&result_v);
			rgmx_comm_freecnt(clnt);
		}
#else // EUROPA_FARM
		if (!CORBA::is_nil(prgm_pres_v)) {
			prgm_pres_v->idl_scha_rs_get_fail_status(arg,
			    result_v, e);
			res = except_to_scha_err(e);

			// If president has not died, proceed.
			if (res.err_code != SCHA_ERR_CLRECONF) {
				if (res.err_code != SCHA_ERR_NOERR) {
					// some unexpected ORB error
					return (res);
				}

				if (result_v->ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM",
					    NOGET("call_scha_rs_get_fail_status"
					    "(): %d\n"), result_v->ret_code);
					res.err_code =
					    (scha_err_t)result_v->ret_code;
					return (res);
				}

				getf_res->ret_code = result_v->ret_code;
				getf_res->seq_id =
				    strdup_nocheck(result_v->idlstr_seq_id);
				if (getf_res->seq_id == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					return (res);
				}
				(void) strseq_to_array(result_v->value_list,
				    &getf_res->value_list);

				res.err_code = (scha_err_t)result_v->ret_code;
				if ((const char *)result_v->idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v->idlstr_err_msg);
				}
				return (res);
			}
		}

		//
		// President is not yet registered, or has died.  In either
		// case, a new president will be elected soon.
		// Sleep until new president is registered in name service,
		// then loop back and retry.
		//
		// Note that rgm_comm_waitforpres() is guaranteed to return
		// a new president object pointer, different from the current
		// value of prgm_pres_v.
		//
		prgm_pres_v = rgm_comm_waitforpres(prgm_pres_v);
#endif // EUROPA_FARM
	} // while (1)

	// NOTREACHED
}

//
// call_scha_rs_set_status
//
// Update the resource status and status message on the local node.
// Also (if local node is not the president),
// send a request via the ORB RPC to the President to udpate
// resource status and status message.
//
// We have to update status on the slave as well as the president,
// because when the president fetches state from the slave it also
// fetches status.  The fetched status from the slave will overwrite
// the status that is stored in the president's local memory.
//
// Check for president node death or rgmd death; if so, sleep until new
// president is elected and re-try the call.
//
// XXX - It appears that the setst_res parameter is not used; instead we
//	 return the error status as our return value.
//
scha_errmsg_t
call_scha_rs_set_status(const rs_set_status_args *setst_args,
    set_status_result_t *setst_res)
{
	char	buff[BUFSIZ];
#ifdef EUROPA_FARM
	rgmx_rs_set_status_args arg;
	rgmx_set_status_result_t result_v = {0};
	CLIENT *clnt = NULL;
	enum clnt_stat rpc_res;
#else
	rgm::idl_rs_set_status_args arg;
	rgm::idl_set_status_result_t_var result_v;
	rgm::rgm_comm_var prgm_pres_v;
	Environment e;
#endif
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *rs_name, *rg_name, *node_name, *fmstatus, *status_msg;
	boolean_t nomem = B_FALSE;
	rglist_p_t rgl = NULL;
	rlist_p_t  rl = NULL;
	char *zonename;

	ucmm_print("RGM",
	    NOGET(" call_scha_rs_set_status: rs = %s, rg = %s\n"),
	    setst_args->rs_name, setst_args->rg_name);

	//
	// Create idlstrings for arguments to idl_scha_rs_set_status()
	// for setting status on the president.
	// Also create C strings for arguments to rgm_comm_setfmstatus()
	// for setting status on the local node.  We might be able to use
	// the existing strings in setst_args without strdup'ing them, but
	// since rgm_comm_setfmstatus() declares its args as (char *) and
	// not (const char *), we're playing it safe in case
	// rgm_comm_setfmstatus() modifies its arguments.
	//

	arg.idlstr_rs_name = new_str(setst_args->rs_name);
	arg.idlstr_rg_name = new_str(setst_args->rg_name);
	rs_name = strdup_nocheck(setst_args->rs_name);
	rg_name = strdup_nocheck(setst_args->rg_name);
	setst_res->ret_code = SCHA_ERR_NOERR;

	// Fix for bug 4993882
	rgm_lock_state();
	rgl = rgname_to_rg(rg_name);
	if (rgl != NULL) {
		rl = rname_to_r(rgl, rs_name);
	}
	rgm_unlock_state();

	if (rgl == NULL) {
		setst_res->ret_code = SCHA_ERR_RG;
		res.err_code = SCHA_ERR_RG;
		res.err_msg = NULL;
		return (res);
	}
	if (rl == NULL) {
		setst_res->ret_code = SCHA_ERR_RSRC;
		res.err_code = SCHA_ERR_RSRC;
		res.err_msg = NULL;
		return (res);
	}

	//
	// scha_resource_setstatus sets R status for the local node.
	// We pass the name of this node as an argument to
	// idl_scha_rs_set_status and rgm_comm_setfmstatus.
	//
	if (setst_args->zonename == NULL || setst_args->zonename[0] == '\0')
		zonename = NULL;
	else
		zonename = setst_args->zonename;
	node_name = lnManager->computeLn(get_local_nodeid(),
	    zonename);
	arg.idlstr_node_name = new_str(node_name);

	//
	// convert the API FM status enum defined in scha.h to
	// ASCII which can be passed to president via ORB
	//
	(void) sprintf(buff, "%d", setst_args->status);
	arg.idlstr_status = new_str(buff);
	fmstatus = strdup_nocheck(buff);

	if ((char *)setst_args->status_msg == NULL) {
		arg.idlstr_status_msg = (char *)NULL;
		status_msg = (char *)NULL;
	} else {
		arg.idlstr_status_msg = new_str(setst_args->status_msg);
		status_msg = strdup_nocheck(setst_args->status_msg);
		if (status_msg == NULL) {
			nomem = B_TRUE;
		}
	}

	// Check for string allocation error for the strdup_nocheck calls
	if (rs_name == NULL || rg_name == NULL || node_name == NULL ||
	    fmstatus == NULL || nomem) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		return (res);
	}

	//
	// Set status and message on the local node
	//
	res = rgm_comm_setfmstatus(rs_name, rg_name, node_name, fmstatus,
	    status_msg);

	if (res.err_code == SCHA_ERR_NOERR) {
		ucmm_print("call_scha_rs_set_status()",
		    NOGET("Status of resource <%s> on local node <%s> was "
		    "updated successfully"), rs_name, node_name);
	} else {
		ucmm_print("call_scha_rs_set_status()",
		    NOGET("Update of resource <%s> status on local node <%s> "
		    "failed with err : %d"),
		    rs_name, node_name, res.err_code);
	}

	free(rs_name);
	free(rg_name);
	free(node_name);
	free(fmstatus);
	free(status_msg);

	if (res.err_code != SCHA_ERR_NOERR) {
		return (res);
	}

	//
	// Set status and message on the president.  Return early if this
	// node already is, or becomes, the president.
	//
#ifndef EUROPA_FARM
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
#endif
	while (1) {
#ifdef EUROPA_FARM
		// In case we have a RPC timeout below we assume that the
		// president died and we will retry the call.
		clnt = rgmx_comm_getpres(ZONE);
		if (clnt != NULL) {
			// Note:
			// We cannot be president in EUROPA_FARM case.

			// If president has not died, proceed.
			bzero(&result_v, sizeof (rgmx_set_status_result_t));
			rpc_res = rgmx_scha_rs_set_status_1(&arg,
			    &result_v, clnt);
			// Call delete [], there is no destructor for
			// arg in the RPC EUROPA_FARM case.
			delete [] arg.idlstr_rs_name;
			delete [] arg.idlstr_rg_name;
			delete [] arg.idlstr_node_name;
			delete [] arg.idlstr_status;

			if (arg.idlstr_status_msg)
				delete [] arg.idlstr_status_msg;

			if ((rpc_res != RPC_SUCCESS) &&
			    (rpc_res != RPC_TIMEDOUT)) {
				// some unexpected RPC error
				res.err_code = SCHA_ERR_ORB;
				xdr_free((xdrproc_t)
				    xdr_rgmx_set_status_result_t,
				    (char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			if (rpc_res == RPC_SUCCESS) {
				if (result_v.ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM",
					    NOGET("call_scha_rs_set_status(): "
					    "failed with error: %d"),
					    result_v.ret_code);
				} else {
					ucmm_print("RGM",
					    NOGET("call_scha_rs_set_status(): "
					    "succeeded"));
				}
				res.err_code = (scha_err_t)result_v.ret_code;
				if ((const char *)result_v.idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v.idlstr_err_msg);
				}
				xdr_free((xdrproc_t)
				    xdr_rgmx_set_status_result_t,
				    (char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			xdr_free((xdrproc_t)xdr_rgmx_set_status_result_t,
				(char *)&result_v);
			rgmx_comm_freecnt(clnt);
		}
#else // EUROPA_FARM
		if (!CORBA::is_nil(prgm_pres_v)) {
			// If the local node is now the president, we are done.
			if (am_i_president()) {
				res.err_code = SCHA_ERR_NOERR;
				// free is a no-op if err_msg is already NULL
				free(res.err_msg);
				res.err_msg = NULL;
				return (res);
			}
			prgm_pres_v->idl_scha_rs_set_status(arg, result_v, e);
			res = except_to_scha_err(e);

			// If president has not died, proceed.
			if (res.err_code != SCHA_ERR_CLRECONF) {
				if (res.err_code != SCHA_ERR_NOERR) {
					// some unexpected ORB error
					return (res);
				}

				if (result_v->ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM",
					    NOGET("call_scha_rs_set_status(): "
					    "failed with error: %d"),
					    result_v->ret_code);
				} else {
					ucmm_print("RGM",
					    NOGET("call_scha_rs_set_status(): "
					    "succeeded"));
				}

				res.err_code = (scha_err_t)result_v->ret_code;
				if ((const char *)result_v->idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v->idlstr_err_msg);
				}
				return (res);
			}
		}

		//
		// President is not yet registered, or has died.  In either
		// case, a new president will be elected soon.
		// Sleep until new president is registered in name service,
		// then loop back and retry.
		//
		// Note that rgm_comm_waitforpres() is guaranteed to return
		// a new president object pointer, different from the current
		// value of prgm_pres_v.
		//
		prgm_pres_v = rgm_comm_waitforpres(prgm_pres_v);
#endif // EUROPA_FARM
	} // while (1)

	// NOTREACHED
}
