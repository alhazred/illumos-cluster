//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_logical_node_manager.cc	1.17	08/09/16 SMI"

#include <string.h>
#include <sys/types.h>
#include <rgm_logical_node_manager.h>
#include <rgm_proto.h>
#include <rgm/scutils.h>
#include <string>
#ifndef EUROPA_FARM
#include <rgm/rgm_comm_impl.h>
#endif
using namespace std;

#include <rgmx_hook.h>

#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_farm.h>
#endif


//
// Creates the LN string, using the nodeid/zonename info.
// The LN string is nodeid:zonename
// Due to interposed malloc, will never return NULL.
//
char*
LogicalNodeManager::computeLn(sol::nodeid_t nodeid, const char *zonename)
{
	char		nodename[16];	// larger than any int nodeid string
	char		*ln;

	(void) sprintf(nodename, "%d", nodeid);

	if (zonename && strcmp(ZONE, zonename) != 0) {
		ln = (char *)malloc(strlen(nodename) + strlen(zonename) + 2);
		(void) strcpy(ln, nodename);
		(void) strcat(ln, LN_DELIMITER_STR);
		(void) strcat(ln, zonename);
	} else {
		ln = (char *)malloc(strlen(nodename) + 1);
		(void) strcpy(ln, nodename);
	}
	return (ln);
}


//
// Lookup for a LogicalNode using nodeid/zonename
// Cannot use the base class findObject() directly
// because it takes the "nodeid:zonename" string as key.
//
LogicalNode*
LogicalNodeManager::findLogicalNode(sol::nodeid_t nodeid,
    const char *zonename, char **lnp)
{
	LogicalNode	*node;
	char		*ln;

	ln = computeLn(nodeid, zonename);
	node = findObject(ln);

	//
	// Return LN if pointer non NULL;
	// in this case, caller will free memory.
	// Otherwise, free the memory.
	//
	if (lnp) {
		*lnp = ln;
	} else {
		free(ln);
	}
	return (node);
}


#ifndef EUROPA_FARM
//
// Create a new LogicalNode
// If ln_in is not NULL, it will be a nodeid:zonename string
//
LogicalNode*
LogicalNodeManager::createLogicalNode(sol::nodeid_t nodeid,
    const char *zonename, rgm::rgm_ln_state status, const char *ln_in)
{
	LogicalNode	*lnNode;
	int		err = 0;
	char		*ln = NULL;
	rgm::lni_t	lni;

	// we cant handle zones in virtual clusters
	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0)
		CL_PANIC(zonename == NULL);
	//
	// If the ln string is passed, we want to avoid computing it again.
	// If not passed, we need to compute it here:
	//
	if (NULL == ln_in) {
		ln = computeLn(nodeid, zonename);
	}
	lnNode = new LogicalNode(nodeid, zonename, status);

	lni = addElement(ln_in ? ln_in : ln, lnNode, 0);
	if (lni == 0) {
		// We've run out of LNIs -- try garbage collecting
		reclaimLnis();
		lni = addElement(ln_in ? ln_in : ln, lnNode, 0);
		if (lni == 0) {
			// Still out of LNIs -- allocate some more of them
			lni = addElement(ln_in ? ln_in : ln, lnNode,
			    BITSPERWORD);
		}
	}
	// At this point, we should have an available LNI
	CL_PANIC(lni != 0);

	lnNode->setLni(lni);
	free(ln);

	ucmm_print("createLogicalNode", "exit");
	//
	// This creates too much noise in the debug buffer.  Uncomment it
	// only for debugging LN Manager code.
	//
	// debug_print();

	return (lnNode);
}
#endif // !EUROPA_FARM

//
// Called when an existing lni mapping already exists
// . When creating LogicalNode for cluster nodes at init time
// . When president retrieves existing mappings from slaves.
//
LogicalNode*
LogicalNodeManager::createLogicalNodeFromMapping(sol::nodeid_t nodeid,
    const char *zonename, rgm::lni_t lni, rgm::rgm_ln_state status,
    const char *nodename)
{
	LogicalNode	*lnNode;
	int		err = 0;
	char		*ln;

	// we cant handle zones in virtual clusters
	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0)
		CL_PANIC(zonename == NULL);

	ln = computeLn(nodeid, zonename);
	lnNode = new LogicalNode(nodeid, zonename, nodename, status);

	//
	// We must set the lni member in lnNode before calling
	// addElementReserved(), because it calls isZcOrGlobalZone(), which
	// requires the lni to have been set.
	//
	CL_PANIC(lni != 0);
	lnNode->setLni(lni);
	CL_PANIC(addElementReserved(ln, lnNode, lni));
	free(ln);
	return (lnNode);
}

#ifndef EUROPA_FARM
//
// Set all physical nodes in ns UP and all physical nodes not in nodeset
// DOWN (or leave them UNCONFIGURED).  For those that are set DOWN, also
// sets DOWN all zones within that node.
//
void
LogicalNodeManager::updateMembership(LogicalNodeset &ns)
{
	sol::nodeid_t	i;
	LogicalNode	*lnNode;

	for (i = 1; i <= LogicalNodeset::maxNode(); i++) {
		// We've created dummy nodes for all the physical nodes,
		// so findObject should succeed for all of them.
		lnNode = findObject(i);

		CL_PANIC(lnNode != NULL);

		// first save the state on all nodes
		lnNode->saveState();

		// Skip nodes that are not configured
		if (!lnNode->isConfigured()) {
			continue;
		}

		// If the node is in the nodeset, enter the cluster,
		// otherwise leave it. Those ops are idempotent if the
		// node is already in or out of the cluster (assuming
		// it's configured).
		if (ns.containLni(i)) {
			lnNode->enterCluster();
		} else {
			lnNode->leaveCluster();
		}
	}
	ucmm_print("updateMembership", "return");
	debug_print();
}

//
// Set each of the physical nodes in ns configured, and all other
// physical nodes unconfigured.  For each node that is set unconfigured,
// also sets unconfigured all zones within that node.
//
void
LogicalNodeManager::updateConfiguredNodes(LogicalNodeset &ns)
{
	sol::nodeid_t	i;
	LogicalNode	*lnNode;

	for (i = 1; i <= LogicalNodeset::maxNode(); i++) {
		// We've created dummy nodes for all the physical nodes,
		// so findObject should succeed for all of them.
		lnNode = findObject(i);

		CL_PANIC(lnNode != NULL);


		if (ns.containLni(i) && !lnNode->isConfigured()) {
			lnNode->configure();
		} else if (!ns.containLni(i)) {
			lnNode->unconfigure();
		}
	}

	debug_print();
	ucmm_print("updateConfiguredNodes()", "exit");
}
#endif // !EUROPA_FARM


//
// Overload base class to maintain the list of zones
// for each physical node.
//
LogicalNode*
LogicalNodeManager::removeElement(const char *name)
{
	LogicalNode *lnNode =
	    ObjManager<LogicalNode, uint_t>::removeElement(name);

	//
	// Remove the LogicalNode from the local zone list maintained
	// in the physical node's LogicalNode.
	//
	if (lnNode != NULL && !lnNode->isZcOrGlobalZone()) {
		LogicalNode *clusterNode = findObject(lnNode->getNodeid());
		CL_PANIC(clusterNode != NULL);
		clusterNode->removeLocalZone(lnNode);
	}
	ucmm_print("removeElement()", "exit");

	return (lnNode);
}

uint_t
LogicalNodeManager::addElement(const char *name, LogicalNode *lnNode,
    uint_t ngrow)
{
	return (addElementReserved(name, lnNode, NO_RESERVED_INDEX, ngrow));
}


//
// Overload base class
// . to take advantage of the 'reservedIdx'
// . To manage the list of LocalNode representing the local zones.
//
// If reservedIdx is not equal to NO_RESERVED_INDEX, the caller already
// knows the LNI and is assigning this value to it.  In that case, the
// caller must guarantee that the lni has already been filled into the
// LogicalNode object.  (See "Europa" comment below).
//
uint_t
LogicalNodeManager::addElementReserved(const char *name, LogicalNode *lnNode,
    uint_t reservedIdx, uint_t ngrow)
{
	LogicalNode *clusterNode;

	// Insert Object and retrieve value
	rgm::lni_t lni = array.insertObject(lnNode, reservedIdx, ngrow);
	if (0 == lni) {
		return (0);
	}

	// Add Element name in hash table
	string key(name);
	hash[key] = lni;

	//
	// If Logical Node is a base-cluster zone, add it to the local zone
	// list maintained in physical node's LogicalNode object.
	//
	// If reservedIdx is NO_RESERVED_INDEX, we were called by
	// addElement().  In this case, we know that lnNode is a non-global
	// zone, because only non-global zones are assigned LNIs dynamically.
	// In this case, we avoid calling isZcOrGlobalZone() because
	// the lni field of lnNode has not been filled in yet.
	//
	// For Europa: must make sure that the LNI is already filled in
	// for a farm node's global zone (i.e., pre-assign LNIs similar to
	// what we do for physical nodes).  Or if farm node LNIs are to be
	// assigned dynamically by addElement(), then Europa must modify
	// isGlobalZone() so that it does not depend upon the lni already
	// having been set in the LN.  For example, a separate "globalzone"
	// or "zonetype" field could be added to the LN object; it could be
	// set at LN creation time before calling addElement or
	// addElementReserved; and isGlobalZone could check that field
	// instead of comparing lni to nodeid.
	//
	if (reservedIdx == NO_RESERVED_INDEX || !lnNode->isZcOrGlobalZone()) {
		clusterNode = findObject(lnNode->getNodeid());
		CL_PANIC(NULL != clusterNode);
		clusterNode->insertLocalZone(lnNode);
	}

	return (lni);
}

#ifndef EUROPA_FARM
void
LogicalNodeManager::initialize()
{
	int		res;
	LogicalNode	*lnNode;
	sol::nodeid_t	i;

	//
	// Precreate all the entries for the cluster nodes.
	// There is a bit of waste here but this is not much.
	//
	// Create all the possible physical nodes as UNCONFIGURED or
	// DOWN, depending on whether or not they are configured.
	// At the first reconfiguration, we'll mark them as UP if they
	// are in the cluster.
	//
	LogicalNodeset conf_ns(Rgm_state->static_membership);
	for (i = 1; i <= LogicalNodeset::maxNode(); i++) {
		if (conf_ns.containLni(i)) {
			lnNode = createLogicalNodeFromMapping(i, NULL,
			    (rgm::lni_t)i, rgm::LN_DOWN);
		} else {
			//
			// The node isn't currently in the cluster, so
			// won't have a name associated with the id.
			// Pass a "dummy" name to use.
			//
			// I don't think we'll ever have more than a 10-digit
			// nodeid (plus 1 for \0 plus 1 for -)
			//
			char *nn_temp = (char *)
			    malloc(strlen(UNCONFIG_CLUSTER_NODE) + 12);
			(void) sprintf(nn_temp, "%s-%d",
			    UNCONFIG_CLUSTER_NODE, i);
			lnNode = createLogicalNodeFromMapping(i, NULL,
			    (rgm::lni_t)i, rgm::LN_UNCONFIGURED, nn_temp);
			free(nn_temp);
		}
	}

	ucmm_print("initialize", "exit");
	debug_print();
}
#endif // !EUROPA_FARM

void
LogicalNodeManager::debug_print()
{
	ucmm_print("debug_print", NOGET("lnm contents:\n"));
	for (iter it = hash.begin(); it != hash.end(); ++it) {
		LogicalNode *ln = findObject(it->second);
		CL_PANIC(ln != NULL);
		ln->debug_print();
	}
}

//
// Gets the set of all logical nodes whose state is at least the given
// state.
//
LogicalNodeset *
LogicalNodeManager::get_all_logical_nodes(rgm::rgm_ln_state at_least)
{
	LogicalNodeset *nset = new LogicalNodeset;

	for (iter it = hash.begin(); it != hash.end(); ++it) {
		LogicalNode *lnNode = findObject(it->second);
		CL_PANIC(lnNode != NULL);
		if (lnNode->getStatus() >= at_least) {
			nset->addLni(lnNode->getLni());
		}
	}
	return (nset);
}

//
// Gets the set of all logical nodes whose state is at least the given
// state.
// We exclude here all the Farm nodes and their logical nodes.
//
LogicalNodeset *
LogicalNodeManager::get_all_server_logical_nodes(rgm::rgm_ln_state at_least)
{
	LogicalNodeset *nset = new LogicalNodeset;

	for (iter it = hash.begin(); it != hash.end(); ++it) {
		LogicalNode *lnNode = findObject(it->second);
		CL_PANIC(lnNode != NULL);
		if ((lnNode->getNodeid() <= MAXNODES) &&
		    (lnNode->getStatus() >= at_least)) {
			nset->addLni(lnNode->getLni());
		}
	}
	return (nset);
}


#ifndef EUROPA_FARM
void
LogicalNodeManager::updateCachedNames()
{
	// iterate through all logical nodes, telling them to
	// update their cached names
	for (iter it = hash.begin(); it != hash.end(); ++it) {
		LogicalNode *ln = findObject(it->second);
		CL_PANIC(ln != NULL);
		ln->updateCachedNames();
	}
}

//
// Iterate through all physical nodes, calling revertState() on all
// of them.
//
void
LogicalNodeManager::revertMembership()
{
	sol::nodeid_t	i;
	LogicalNode	*lnNode;

	for (i = 1; i <= LogicalNodeset::maxNode(); i++) {
		// We've created dummy nodes for all the physical nodes,
		// so findObject should succeed for all of them.
		lnNode = findObject(i);

		CL_PANIC(lnNode != NULL);

		lnNode->revertState();
	}
	ucmm_print("revertMembership()", "exit");
	debug_print();
}
//
// Since multiple threads might call reclaimOff
// and reclaimOn we use a counter which is incremented
// in case of reclaimOff and decremented in case of
// reclaimOn. The allowReclaim is set to true if the
// lniReclaimCount is 0.
//
void
LogicalNodeManager::reclaimOff()
{
	lniReclaimCount++;
	allowReclaim = false;
}

void
LogicalNodeManager::reclaimOn()
{
	lniReclaimCount--;
	CL_PANIC(lniReclaimCount >= 0);
	// lnis make sense only for global rgmd.
	if (lniReclaimCount == 0 && ZONE != NULL)
		allowReclaim = true;
}

//
// Find all LNs that represent non-global zones that are down, which are not
// referenced by any RG or RT, and whose physical node is currently up.
// Remove those LNs, reclaiming their LNIs.
// Notify each slave of LNIs, representing non-global zones on that slave,
// which are to be deleted.
//
// This method must be called only by the president, while holding the RGM
// state lock.
//
void
LogicalNodeManager::reclaimLnis()
{
	uint_t			num_reclaimed = 0;
	LogicalNodeset		nset;
	rgm::lni_t		lni, physLni;
	LogicalNode		*lnNode;
	uint_t			*seq_len;
	rgm::lni_seq_t		**lni_reclaim;
	sol::nodeid_t		i;
	sc_syslog_msg_handle_t	handle;
	rgm::rgm_comm_var	prgm_serv;
	Environment		e;
	idlretval_t		retval = RGMIDL_OK;
	bool			is_farmnode;

	//
	// If reclaims are currently disabled, just return.
	//
	if (!allowReclaim) {
		return;
	}

	seq_len = new uint_t [LogicalNodeset::maxNode() + 1];
	bzero(seq_len, (LogicalNodeset::maxNode() + 1) * sizeof (uint_t));
	lni_reclaim = new rgm::lni_seq_t *[LogicalNodeset::maxNode() + 1];
	bzero(lni_reclaim, (LogicalNodeset::maxNode() + 1) *
	    sizeof (rgm::lni_seq_t *));

	//
	// Create logical nodeset of base-cluster non-global zones which are
	// down, and whose physical node is up.
	//
	for (iter it = hash.begin(); it != hash.end(); ++it) {
		lnNode = findObject(it->second);
		CL_PANIC(lnNode != NULL);
		if (lnNode->isZcOrGlobalZone()) {
			continue;
		}
		physLni = lnNode->getNodeid();
		LogicalNode *physNode = findObject(physLni);
		if (!lnNode->isInCluster() && physNode->isInCluster()) {
			nset.addLni(it->second);
		}
	}

	// Delete from nset all LNIs referenced by RG or RT nodelists
	rglist_p_t rgp;
	for (rgp = Rgm_state->rgm_rglist; rgp != NULL; rgp = rgp->rgl_next) {
		nodeidlist_t *nlp;
		for (nlp = rgp->rgl_ccr->rg_nodelist; nlp != NULL;
		    nlp = nlp->nl_next) {
			if (nlp->nl_lni != LNI_UNDEF)
				nset.delLni(nlp->nl_lni);
		}
	}
	rgm_rt_t *rtp;
	for (rtp = Rgm_state->rgm_rtlist; rtp != NULL; rtp = rtp->rt_next) {
		if (rtp->rt_instl_nodes.is_ALL_value) {
			continue;
		}
		nodeidlist_t *nlp;
		for (nlp = rtp->rt_instl_nodes.nodeids; nlp != NULL;
		    nlp = nlp->nl_next) {
			if (nlp->nl_lni != LNI_UNDEF)
				nset.delLni(nlp->nl_lni);
		}
	}

	//
	// Go through nset to figure out the length of each sequence.
	// The lengths are stored in the seq_len[] array.
	//
	// At this point, we know that we are going to deallocate all
	// of the LNIs in nset, so we can use this same loop to also
	// delete any existing per-lni RG or R state for each lni in nset.
	//
	lni = 0;
	while ((lni = nset.nextLniSet(lni + 1)) != 0) {
		lnNode = findObject(lni);
		CL_PANIC(lnNode != NULL);
		seq_len[lnNode->getNodeid()]++;

		// Remove state for this lni from all RGs and Rs
		for (rgp = Rgm_state->rgm_rglist; rgp != NULL;
		    rgp = rgp->rgl_next) {
			if (rgp->rgl_xstate.count(lni) > 0)
				rgm_free_timelist(
				    &rgp->rgl_xstate[lni].rgx_start_fail);
				rgp->rgl_xstate.erase(lni);
			for (rlist_p_t rp = rgp->rgl_resources; rp != NULL;
			    rp = rp->rl_next) {
				if (rp->rl_xstate.count(lni) > 0) {
					rp->rl_xstate.erase(lni);
				}
			}
		}
	}

	//
	// Allocate memory for each lni_reclaim sequence (one for each
	// physical node) and set the length of the sequence.
	//
	for (i = 1; i <= LogicalNodeset::maxNode(); i++) {
		if (seq_len[i] != 0) {
			lni_reclaim[i] = new rgm::lni_seq_t();
			lni_reclaim[i]->length(seq_len[i]);
			// Reset it to 0, so we can use it later.
			seq_len[i] = 0;
		}
	}

	// Fill the sequences with the LNIs to be reclaimed
	lni = 0;
	while ((lni = nset.nextLniSet(lni + 1)) != 0) {
		lnNode = findObject(lni);
		CL_PANIC(lnNode != NULL);

		i = lnNode->getNodeid();

		//
		// Assert that our logic in creating these sequences was
		// correct.
		//
		CL_PANIC(lni_reclaim[i] != NULL);

		(*lni_reclaim[i])[seq_len[i]] = lni;
		seq_len[i]++;
	}

	//
	// Send the sequences to all slaves, including
	// the president (myself).
	//
	// For each slave which is not the president, also reclaim the
	// LNIs on the president after the slave has been notified.
	//
	// If slave has died, just reclaim the LNIs on the president.
	// When slave reboots, it will obtain LNIs from president.
	//
	for (i = 1; i <= LogicalNodeset::maxNode(); i++) {
		if (seq_len[i] == 0) {
			continue;
		}

		if ((rgmx_hook_call(rgmx_hook_reclaim_lnis,
		    i, lni_reclaim[i],
		    &retval, &is_farmnode) == RGMX_HOOK_DISABLED) ||
		    (!is_farmnode)) {
			retval = RGMIDL_OK;
			prgm_serv = rgm_comm_getref(i);
			if (!CORBA::is_nil(prgm_serv)) {
				prgm_serv->idl_reclaim_lnis(
				    orb_conf::local_nodeid(),
				    Rgm_state->node_incarnations.members
				    [orb_conf::local_nodeid()],
				    *lni_reclaim[i], e);
				retval = except_to_errcode(e, i);
			}
		}

		switch (retval) {
		case RGMIDL_NODE_DEATH:
		case RGMIDL_RGMD_DEATH:
			// node has died; proceed
		case RGMIDL_OK:
			break;

		case RGMIDL_UNKNOWN:
		case RGMIDL_NOREF:
		default:
			//
			// Crash and burn.  Use same syslog message
			// as create_lni_mappings_from_nodeidlist(),
			// since this error is rare and the user
			// explanation is the same.
			//
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// This node encountered an unexpected error while
			// communicating with other cluster nodes during a
			// cluster reconfiguration. The rgmd will produce a
			// core file and will cause the node to halt or
			// reboot.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle,
			    LOG_ERR, MESSAGE,
			    "fatal: cannot send mappings to "
			    "remote nodes");
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHED
		}

		//
		// If i is a slave other than the local node (the president),
		// we need to also reclaim the lni here on the president.
		//
		if (i != orb_conf::local_nodeid()) {
			for (uint_t j = 0; j < seq_len[i]; j++) {
				lni = (*lni_reclaim[i])[j];
				if (lni == 0) {
					ucmm_print("reclaimLnis",
					    "Not reclaiming LNI at spot %d in "
					    "the seq because slave rejected "
					    "it.\n", j);
					continue;
				}

				lnNode = findObject(lni);
				CL_PANIC(lnNode != NULL);
				LogicalNode *lnReclaim =
				    removeElement(computeLn(lnNode->getNodeid(),
				    lnNode->getZonename()));
				CL_PANIC(lnReclaim == lnNode);
				delete lnNode;
			}
		}

		delete lni_reclaim[i];
	}

	delete [] seq_len;
	delete [] lni_reclaim;
	ucmm_print("reclaimLnis", NOGET("exit"));
	// debug_print();
}
#endif // !EUROPA_FARM


//
// President sends a list of deleted LNIs to the slave.
// Slave deletes the corresponding LogicalNode from the lnManager.
// This is called on all slaves including the president itself.
//
#ifdef EUROPA_FARM
bool_t
rgmx_reclaim_lnis_1_svc(rgmx_reclaim_lnis_args *rpc_args,
	rgmx_reclaim_lnis_result *rpc_res,
	struct svc_req *)
#else
void
rgm_comm_impl::idl_reclaim_lnis(sol::nodeid_t node,
    sol::incarnation_num incarnation, rgm::lni_seq_t &lni_seq,
    Environment &)
#endif
{
#ifdef EUROPA_FARM
	sol::nodeid_t node = rpc_args->node;
	sol::incarnation_num incarnation =  rpc_args->incarnation;
	rgm::lni_seq_t lni_seq;

	lni_seq.length(rpc_args->lni_seq.lni_seq_t_len);
	for (uint_t i = 0; i < lni_seq.length(); i++) {
		lni_seq[i] = rpc_args->lni_seq.lni_seq_t_val[i];
	}
#endif
	slave_lock_state();

	if (stale_idl_call(node, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		rgm_unlock_state();
#ifdef EUROPA_FARM
		return (TRUE);
#else
		return;
#endif
	}

	for (uint_t i = 0; i < lni_seq.length(); i++) {
		rgm::lni_t lni = lni_seq[i];
		LogicalNode *lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);

		//
		// Check the state of the LogicalNode. There's a chance
		// that the president is attempting to reclaim an lni
		// for a LogicalNode that's actually UP, but the president
		// doesn't yet know that. The slave is allowed to reject an
		// attempt to reclaim the node by setting the lni in the
		// in/out sequence parameter to 0.
		//
		if (lnNode->getStatus() == rgm::LN_UP) {
			ucmm_print("idl_reclaim_lnis", "Not reclaiming "
			    "lni %d because the state is LN_UP\n", lni);
			lni_seq[i] = 0;
		} else {

			//
			// Remove the pingpong directory corresponding to
			// the deconfigured zone.
			// President has already checked that lni refers to a
			// zone on this node; but we check again just to be
			// sure.
			//
			if (lnNode->getNodeid() == get_local_nodeid()) {
				remove_zone_pingpong_dir(lni);
			}

			LogicalNode *lnReclaim =
			    lnManager->removeElement(lnManager->computeLn(
			    lnNode->getNodeid(), lnNode->getZonename()));
			CL_PANIC(lnReclaim == lnNode);
			delete lnNode;
		}
	}

	slave_unlock_state();

#ifdef EUROPA_FARM
	rpc_res->lni_seq.lni_seq_t_len = lni_seq->length();
	rpc_res->lni_seq.lni_seq_t_val =
		(lni_t *)malloc(
		rpc_res->lni_seq.lni_seq_t_len *
		sizeof (lni_t));
	for (uint_t i = 0; i < lni_seq->length(); i++) {
		rpc_res->lni_seq.lni_seq_t_val[i] = lni_seq[i];
	}
	return (TRUE);
#endif
}
