/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_scswitch_impl.cc	1.262	09/01/28 SMI"

//
// rgm_scswitch_impl.cc
//
// The functions in this file implement the rgmd side of the
// scswitch(1m) command.
//

#ifndef EUROPA_FARM
#include <orb/fault/fault_injection.h>
#endif
#include <sys/os.h>
#ifndef EUROPA_FARM
#include <rgm_global_res_used.h>
#include <rgm/rgm_comm_impl.h>
#endif
#include <rgm_state.h>
#include <rgm_proto.h>
#ifndef EUROPA_FARM
#include <rgm_intention.h>
#endif
#include <thread.h>

#include <scha_err.h>
#include <scha.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>
#include <rgm/fecl.h>

#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <scadmin/scconf.h>
#include <rgm/rgm_msg.h>
#include <rgm/rgm_errmsg.h>
#include <rgm_api.h>
#include <sys/cl_events.h>

#ifndef EUROPA_FARM
#include <nslib/ns_interface.h>
#endif

#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_farm.h>
#endif

#include <rgmx_hook.h>

#include <string.h>
#include <stdlib.h>


// Zones support
#include <rgm_logical_nodeset.h>

#define	smart_overlay(x, y)	if ((x).err_code == SCHA_ERR_NOERR) \
					(x).err_code = (y).err_code
#define	smart_overlay_err(x, y) if ((x).err_code == SCHA_ERR_NOERR) \
					(x).err_code = (y)


#ifndef EUROPA_FARM
static scha_errmsg_t switchback(const char *rgname, namelist_t *rgnames,
    bool verbose_flag);
static void scswitch_examine_rg(scha_errmsg_t *res, const rglist_p_t rg_ptr,
    const char *rg_name, boolean_t firsttime, LogicalNodeset &opnodes,
    LogicalNodeset &p_onnodes, LogicalNodeset &p_offnodes, int *p_numon,
    int *p_numpendon, int *p_rgmasters, boolean_t *p_alldone,
    boolean_t is_offlining, boolean_t is_onlining);

static void check_starterrs(scha_errmsg_t *res, const rglist_p_t rgp,
    boolean_t check_offline);
static void check_pending_online_blocked(scha_errmsg_t *res,
    const rglist_p_t rgp);
static void check_clear_stopfail(scha_errmsg_t *errmsg,
    const rglist_p_t rgp, LogicalNodeset mnodeset);
#endif
static void rg_clear_stopfailed(rglist_p_t, rgm::lni_t);
static boolean_t check_dependees(rlist_p_t r_ptr, rdeplist_t *deps,
    boolean_t is_weak_deps);
#ifndef EUROPA_FARM
static boolean_t has_man_dependents(scha_errmsg_t *err, rglist_p_t rg,
    rglist_p_t *rglistp, namelist_t *failed_op_list);
static boolean_t is_r_monitorable(const rlist_p_t r_ptr);
static boolean_t is_rg_online(rglist_p_t rg);
static bool is_r_online_anywhere(rlist_p_t rp);
static boolean_t rgs_are_stopping(scha_errmsg_t *res, const rglist_p_t *rglistp,
    const LogicalNodeset &evac_nodeset, bool verbose_flag);
static boolean_t rgs_are_rebalancing(scha_errmsg_t *res, rglist_p_t *rglistp,
    namelist_t *oplist, bool skip_suspended, bool verbose_flag,
    boolean_t switch_back);
static void check_online_dependents(scha_errmsg_t *res, rglist_p_t *rglistp,
    int i);
static void check_online_resource_dependents(scha_errmsg_t *res,
    rglist_p_t rgp, rglist_p_t *rglistp, namelist_t *failed_op_list);
static void find_online_resources(scha_errmsg_t *res, rdeplist_t *resources,
    rglist_p_t *rglistp, char **onlines);
static void add_string_to_list(scha_errmsg_t *res, char *new_string,
    char **commalist);
#endif
static void rgm_clear_util(rgm::lni_t lni,
    const rgm::idl_string_seq_t &scsw_clr_rs, rgm::scswitch_clear_flags flag,
    rgm::idl_regis_result_t_out scsw_clr_ret);
#ifndef EUROPA_FARM
static scha_errmsg_t check_rg_affinities_onlining(rglist_p_t rgp,
    LogicalNodeset &onlist, namelist_t *rg_names);
static scha_errmsg_t check_rg_affinities_offlining(rglist_p_t rgp,
    LogicalNodeset &offlist, namelist_t *rg_names);
static scha_errmsg_t rg_switch_check_affinities(rglist_p_t rgp,
    LogicalNodeset &onlist, rglist_p_t *rgs, int num_rgs);
static boolean_t rg_names_in_list(namelist_t *violations, rglist_p_t *rgs,
    int num_rgs);

static scha_errmsg_t quiesce_set_nameserver(rglist_p_t rgp,
    boolean_t is_fast_quiesce);
static scha_errmsg_t set_ok_to_start(rglist_p_t rgp);

static void switch_order_rsarray(rlist_p_t rss[]);
static void switch_shift_netrs(rlist_p_t rss[]);
static void switch_move_element_forward(int src_index,
    int dst_index, void *array[]);
static scha_errmsg_t add_switchover_delegate(const char ***rg_list,
    uint_t *numrgs, scha_errmsg_t *warning_res);

static scha_err_t rgnames_to_rglist(const rgm::idl_string_seq_t rgnames,
    rglist_p_t **rglistpp, uint_t *numrgsp, uint_t nodeid, const char *zonename,
    scha_errmsg_t *errors);

static scha_errmsg_t quiesce_set_nameserver_tags(rglist_p_t *rglistp,
    uint_t numrgs, boolean_t is_fast_quiesce, scha_errmsg_t *warning_res);
static void quiesce_methods(rglist_p_t rg_ptr, const char *rname);
static scha_errmsg_t rnames_to_rlist(const rgm::idl_string_seq_t rnames,
    rlist_p_t **rlistpp, uint_t *numresourcesp, rglist_p_t *rglistp,
    uint_t numrgs, scha_errmsg_t *warning_res);

static scha_err_t get_rglist(rglist_p_t **rglistpp,
    uint_t *numrgsp, uint_t nodeid, const char *zonename,
    scha_errmsg_t *warning_res);

static void kill_methods(rglist_p_t rg_ptr, rgm::idl_string_seq_t &rsnames,
    scha_errmsg_t *warning_res);

static void rptrs_to_strseq(rlist_p_t *rlistp, rgm::idl_string_seq_t &rsnames);
static void quiesce_clear_nameserver_tags(rglist_p_t *rglistp,
    boolean_t is_fast_quiesce);
static boolean_t is_rg_quiesced(const rglist_p_t rg_ptr,
    LogicalNodeset &start_failed_ndoes, LogicalNodeset &stop_failed_nodes);
static void rglist_remove_rg(rglist_p_t *rglistp, uint_t i);
static void is_dependent_online(rlist_p_t r_ptr, LogicalNodeset *ns,
    rdeplist_t *depl, std::list<char *> *rnamelist, deptype_t deps_kind);
static scha_errmsg_t is_strong_dependent_online(rlist_p_t r_ptr,
	LogicalNodeset *ns);

//
// add_switchover_delegate
//
// the function takes an array of RG names and check if any of them delegates
// their switchover request to another RG. If so, add the delegate RG name at
// the end of the array.
// The function also updates the number of RG names in the array (numrgs)
// and adds a warning message to warning_res.
//
// rg_list is a pointer to the array of RG names
// numrgs is a pointer to the number of RGs in the array. It is an IN and OUT
//	parameter
// warning_res is a pointer to a scha_errmsg_t structure. If it used to add
//	a warning if a RG name is added to the array
//
static scha_errmsg_t
add_switchover_delegate(const char ***rg_list, uint_t *numrgs,
    scha_errmsg_t *warning_res)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	const char	**rg_list_tmp;
	uint_t		i, j, numrgs_tmp, init_nb;
	rglist_p_t	rg_p, delegate_rg;
	char		*delegate_name;
	boolean_t	already_in_list;

	init_nb = *numrgs;

	for (i = 0; i < init_nb; i++) {

		rg_p = rgname_to_rg((*rg_list)[i]);
		//
		// support for inter cluster failover delegation is not yet
		// implemented.
		//
		if ((rg_p != NULL) &&
		    (rg_p->rgl_affinities.ap_failover_delegation != NULL) &&
		    (!is_enhanced_naming_format_used(
		    rg_p->rgl_affinities.ap_failover_delegation->nl_name))) {


			delegate_rg = get_deepest_delegate(
			    rg_p->rgl_affinities.ap_failover_delegation->
			    nl_name);
			if (delegate_rg) {

				//
				// add the delegate RG at the end of the array
				// if it is not already part of it
				//
				delegate_name = delegate_rg->rgl_ccr->rg_name;

				already_in_list = B_FALSE;
				for (j = 0; j < *numrgs; j++) {
					if (strcmp((*rg_list)[j],
						delegate_name) == 0) {
						already_in_list = B_TRUE;
						break;
					}
				}

				if (!already_in_list) {
					numrgs_tmp = *numrgs + 1;
					//
					// reallocate a new array with an extra
					// element. The new RG name is added at
					// the end of the array
					//
					rg_list_tmp = (const char **)realloc(
					    *rg_list,
					    (numrgs_tmp + 1) * sizeof (char *));
					if (rg_list_tmp == NULL) {
						res.err_code = SCHA_ERR_NOMEM;
						return (res);
					}
					rg_list_tmp[*numrgs] = delegate_name;
					rg_list_tmp[*numrgs + 1] = NULL;

					*rg_list = rg_list_tmp;
					*numrgs = numrgs_tmp;

					rgm_format_errmsg(warning_res,
					    RWM_AFF_SWITCH_DELEGATE_USED,
					    delegate_name, (*rg_list)[i]);
				}
			}
		}
	}

	return (res);
}


// This function is used to build the list of operands that have failed.
static scha_errmsg_t
add_op_to_failed_list(namelist **failed_op_list, const char *rg_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	if (!namelist_exists(*failed_op_list, (char *)rg_name)) {
		res = namelist_add_name(failed_op_list, (char *)rg_name);
	} else {
		res.err_code = SCHA_ERR_NOERR;
	}
	return (res);
}

//
// idl_scswitch_primaries
// Run on the President.  Called by scswitch(1M).
//
// If the NODE_EVACUATE and REBALANCE flags in the scswp_args.flags argument
// are false,
// brings the specified RG(s) online on the specified node(s) and
// offline on all other nodes.  Returns after all switches are complete.
// Determines which nodes (including itself) need to take which
// RGs online or offline.  Calls do_rg_switch() to send out orders to
// slave nodes by calling rgm_change_mastery().  If the set of nodes is
// empty, the RGs are taken offline.
//
// If the REBALANCE flag is true, brings each specified RG online on its
// "most preferred" master(s) by running rebalance() on the RG.
// If the lists of RGs is empty, rebalances all managed RGs.
// For this option, the node_names argument should be empty (and is
// ignored).  This option is mutually exclusive with NODE_EVACUATE.
//
// If the NODE_EVACUATE flag is true, scswp_args.RG_names should be empty,
// and is ignored.  scswp.node_names contains a list of logical nodes
// to be evacuated.  Switches all RGs offline on every logical node in the
// list.  If an element of the list is a physical node (global zone), we
// evacuate all the zones on that node.
// As RGs go offline, they are rebalanced by the president.
// The command returns when all RGs are no longer in
// pending offline state on any evacuating node, even though the RGs
// have not yet finished coming online on other nodes.
//
// The SHUTDOWN flag indicates that the cluster is shutting down.
// In this case, we bypass the RG_system check and allow all RGs to be
// taken offline.
//
// The ONLINE flag indicates that there is a request for specific Rgs to be
// brought online on the specified nodes.
// The operation fails if the desired primaries for the RG would be
// exceeded.
//
// The OFFLINE flag indicates that there is a request for specific Rgs to be
// brought offline on the specified nodes.
//
// The SWITCH_BACK flag indicates that there is a request for specific Rgs to be
// remastered on their preferred primaries.
//
// During node evacuation, RGs are prevented from
// failing over onto any node that is in the process of being evacuated.
// The EVAC_STICKY_INTERVAL, which is encoded into 16 bits of the
// scswp_args.flags argument, provides a time interval in seconds for which
// the RGM continues to prevent RGs from failing over onto a node after the
// evacuation of the node has been completed.  The sticky interval is
// immediately terminated if the node reboots, or when the administrator
// makes an explicit request to switch an RG onto the node.
//
// verbose_flag is used to print the success message.
//
// Return SCHA_ERR_NOERR (thru out parameter 'result') if everything
// went as planned.  Otherwise return an error code and/or message.
//
void
rgm_comm_impl::idl_scswitch_primaries(
	const rgm::idl_scswitch_primaries_args &scswp_args,
	rgm::idl_regis_result_t_out result,
	bool verbose_flag,
	Environment &)
{
	uint_t		numrgs, i;
	rglist_p_t	*rglistp = NULL; // null-term-ed array of rglist_t ptrs
	//
	// This rglist is used for checking
	// if the specified Rgs are brought online
	// or offline on specific nodes during clrg
	// online offline operation.
	//
	rglist_p_t	*orig_rglistp = NULL;
	rglist_p_t	rgp;
	LogicalNodeset	mnodeset;	// nodeset of intended new masters or
					// evacuating zones
	LogicalNodeset	evac_nodeset;	// evacuating zones including non-global
					// zones
	LogicalNodeset	target_nodeset;	// nodeset of intended new masters for
					// onlineing or offlining operation.
	rgm::lni_t	lni;
	LogicalNode	*ln;
	const char	**complete_RG_names = NULL;
	char		*nodename = NULL;
	LogicalNodeset	*current_nodeset = NULL;
	LogicalNodeset	*nodelist_ns;
	std::map<rglist_p_t, LogicalNodeset> save_evac;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	event_res;
	scha_errmsg_t	warning_res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	temp_res = {SCHA_ERR_NOERR, NULL};
	uint_t		nummasters;
	namelist_t	*rg_names = NULL;
	namelist_t	*temp_rg_name;
	rglist_p_t	rg_ptr;
	scha_err_t	busted_err = SCHA_ERR_NOERR;
	timestruc_t	to;
	boolean_t	evac_suspended_rg_warning = B_FALSE;
	LogicalNodeset	emptyNodeset;
	LogicalNodeset	onnodes;
	LogicalNodeset	offnodes;
	sc_syslog_msg_handle_t handle;
	boolean_t globalflag, nodelist_contains;

	//
	// Used for New CLI in case of multiple operands.
	// In the course of handling multiple operands the failed
	// operands get added into the failed_op_list.Any further
	// process checks if the operand exists in the failed_op_list.
	// That operand will not be processed if its in the failed_op_list.
	//
	namelist_t *failed_op_list = NULL;

	rgm::idl_string_seq_t rgnames_seq;

	boolean_t is_evacuating = (scswp_args.flags & rgm::NODE_EVACUATE) ?
	    B_TRUE : B_FALSE;
	boolean_t is_rebalancing = (scswp_args.flags & rgm::REBALANCE) ?
	    B_TRUE : B_FALSE;
	boolean_t is_cluster_shutting = (scswp_args.flags &
	    rgm::SHUTDOWN) ? B_TRUE : B_FALSE;
	// New flag for OFFLINE operation.
	boolean_t is_offlining = (scswp_args.flags &
	    rgm::OFFLINE) ? B_TRUE : B_FALSE;
	// New flag for online operation.
	boolean_t is_onlining = (scswp_args.flags &
	    rgm::ONLINE) ? B_TRUE : B_FALSE;

	// check if this is a rg remastering operation.
	boolean_t switch_back = (scswp_args.flags &
	    rgm::SWITCH_BACK) ? B_TRUE : B_FALSE;

	time_t evac_sticky_timeout =
	    (time_t)GET_EVAC_STICKY_TIMEOUT(scswp_args.flags);

	ucmm_print("scswitch_primaries()", NOGET("entry\n"));

	// lock state structure before accessing President info
	rgm_lock_state();

	//
	// Check that the nodenames of new masters or evacuating nodes are
	// valid cluster members and add their LNIs to mnodeset.
	//
	nummasters = scswp_args.node_names.length();

	// get lni where the call originated
	ngzone_check(scswp_args.idlstr_zonename, globalflag);

	//
	// If the evacuation node argument is a global zone, we also evacuate
	// all non-global zones on the same node.
	//
	for (i = 0; i < nummasters; i++) {
		//
		// have to strdup; otherwise get
		// "Non-const function CORBA::String_field::operator char*()
		// called for const object"
		//
		nodename = strdup_nocheck(scswp_args.node_names[i]);
		if (nodename == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto switch_primaries_end; //lint !e801
		}
		ln = get_ln(nodename, &temp_res.err_code);

		if (temp_res.err_code == SCHA_ERR_NODE) {
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_INVALID_NODE, nodename);
			temp_res.err_code = SCHA_ERR_NOERR;
			free(nodename);
			continue;
		} else if (temp_res.err_code != SCHA_ERR_NOERR) {
			// NOMEM or internal error -- bail out
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			goto switch_primaries_end; //lint !e801
		}
		lni = ln->getLni();

		if (mnodeset.containLni(lni)) {
			ucmm_print("scswitch_primaries()", NOGET(
			    "duplicate node <%d>\n"), lni);
			temp_res.err_code = SCHA_ERR_INVAL;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_DUPNODE, nodename);
			temp_res.err_code = SCHA_ERR_NOERR;
			free(nodename);
			continue;
		}

		//
		// Test membership in cluster.
		// Unless we are evacuating, also rule out SHUTTING_DOWN state.
		//
		if (!ln->isInCluster() ||
		    (!is_evacuating && ln->getStatus() < rgm::LN_UP)) {
			ucmm_print("scswitch_primaries()", NOGET(
			    "node %d not in cluster\n"),
			    lni);
			temp_res.err_code = SCHA_ERR_MEMBER;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_NODE_NOT_MEMBER, nodename);
			free(nodename);
			temp_res.err_code = SCHA_ERR_NOERR;
			continue;
		}
		mnodeset.addLni(lni);
		free(nodename);
	}

	//
	// check for invalid node entry.
	// If mnodeset is empty and res.err_code is set it means that
	// user has entered invalid node(s). None of the node is valid.
	//
	if ((mnodeset.isEmpty()) &&
	    (res.err_code != SCHA_ERR_NOERR)) {
		// Invalid node(s) specified.
		rgm_format_errmsg(&res, REM_ALLNODE_INVAL);
		goto switch_primaries_end; //lint !e801
	}

	//
	// Handle node evacuation.  Multiple logical nodes can be specified.
	// For global zone operands, add all nonglobal zones to evac nodeset.
	// Examine all managed RGs to see which ones need to be evacuated.
	// Build a list of rgnames.
	//
	// Call rgm_change_mastery() to switch RGs offline on the
	// evacuating nodes.  Wait for evacuation to complete.
	//
	if (is_evacuating) {
		char *ns_buff = NULL;

		if (mnodeset.display(ns_buff) == 0) {
			ucmm_print("scswitch_primaries", NOGET(
			    "evacuating nodes %s"),
			    ns_buff);
			free(ns_buff);
		}

		uint_t count = mnodeset.count();
		if (count == 0) {
			// There is no work to do.
			goto switch_primaries_end; //lint !e801
		}
		if (!globalflag) {
			//
			// can only evacuate itself. so count should be 1
			// and the nodeset should contain the calling zone
			//
			LogicalNode *calling_ln = lnManager->findLogicalNode(
			    scswp_args.nodeid, scswp_args.idlstr_zonename);
			if (calling_ln == NULL || count > 1 ||
			    !mnodeset.containLni(calling_ln->getLni())) {
				res.err_code = SCHA_ERR_ACCESS;
				rgm_format_errmsg(&res, REM_EVAC_NGZONE);
				goto switch_primaries_end; //lint !e801
			}
		}

		evac_nodeset = mnodeset;
		//
		// For each physical node (global zone) in mnodeset, add its
		// nonglobal zones which are cluster members, and not shutting
		// down, to evac_nodeset.
		//
		sol::nodeid_t n = 0;
		while ((n = mnodeset.nextPhysNodeSet(n + 1)) != 0) {
			ln = lnManager->findObject(n);
			CL_PANIC(ln != NULL);
			std::list<LogicalNode*>::const_iterator it;
			for (it = ln->getLocalZones().begin(); it !=
			    ln->getLocalZones().end(); ++it) {
				if (!(*it)->isInCluster()) {
					continue;
				}
				evac_nodeset.addLni((*it)->getLni());
			}
		}

		//
		// Node evacuation is idempotent, so we don't need to check
		// if the zone is already evacuating, and we don't generate
		// an error for that case.
		//

		numrgs = 0;
		//
		// Iterate through all RGs to find out which ones are online
		// on evacuating nodes and need to be evacuated.  For each
		// such RG, add it to rglistp; update its
		// rgl_switching flag and rgl_evac_nodes nodeset.
		//
		for (rg_ptr = Rgm_state->rgm_rglist; rg_ptr != NULL;
		    rg_ptr = rg_ptr->rgl_next) {
			LogicalNodeset target_ns;

			// Skip over RGs in unmanaged state
			if (rg_ptr->rgl_ccr->rg_unmanaged) {
				continue;
			}

			//
			// Skip RG if it is already being evacuated on all
			// nodes of the evac_nodeset by another thread
			//
			if ((evac_nodeset - rg_ptr->rgl_evac_nodes).isEmpty()) {
				rgm_format_errmsg(&res,
				    RWM_SKIP_RG_EVAC_THREAD,
				    rg_ptr->rgl_ccr->rg_name);
				continue;
			}

			// Obtain the RG nodelist as a LogicalNodeset
			nodelist_ns = get_logical_nodeset_from_nodelist(
			    rg_ptr->rgl_ccr->rg_nodelist);

			// Skip RG if no element of evac_nodeset is in Nodelist
			if ((*nodelist_ns & evac_nodeset).isEmpty()) {
				rgm_format_errmsg(&res, RWM_SKIP_RG,
				    rg_ptr->rgl_ccr->rg_name);
				continue;
			}
			delete nodelist_ns;

			//
			// Add this RG to rglistp.
			// If we get a NOMEM error, we will restore the
			// previous rgl_evac_nodes and rgl_switching
			// for RGs already modified.
			//
			res = rglist_add_rg(rglistp, rg_ptr, numrgs++);
			if (res.err_code != SCHA_ERR_NOERR) {
				goto stop_node_evacuate; //lint !e801
			}

			//
			// Save a copy of the current rgl_evac_nodes; if
			// we have to exit early, we will restore
			// rgl_evac_nodes & rgl_switching from this.
			//
			save_evac[rg_ptr] = rg_ptr->rgl_evac_nodes;

			//
			// Add evac_nodeset to RG's evac_nodes nodeset
			//
			rg_ptr->rgl_evac_nodes |= evac_nodeset;

			//
			// Set the rgl_switching flag to SW_EVACUATING.
			// This will bust any in-progress switches or updates,
			// as well as making the RG busy to prevent new
			// switches or updates from starting.
			//
			// We don't check whether the rgl_switching flag was
			// already set to SW_EVACUATING.  Multiple offlining
			// calls to rgm_change_mastery() on the same slave
			// can do no harm.
			// If the evacuations are on different nodes,
			// there is no conflict.  Rebalance will exclude
			// all evacuating nodes when re-mastering the RG.
			//
			rg_ptr->rgl_switching = SW_EVACUATING;

			//
			// Since the president might not have completely
			// up to date R/RG state for the slaves, we will
			// issue the change_mastery call to take the RG
			// offline from the evacuating node, regardless of
			// what its current state appears to be.
			//

			//
			// generate a nodeset of "current masters"
			// (master_ns) for event generation. master_ns is not
			// guaranteed to be completely accurate if the RG is
			// currently reconfiguring, however it's the best we
			// can do.
			//
			LogicalNodeset	*master_ns = new LogicalNodeset;
			get_rg_online_nodeset(rg_ptr, master_ns);
			target_ns = *master_ns - evac_nodeset;

			//
			// If the RG appears to be already offline on
			// evac_nodeset, we will suppress the primaries_changing
			// event to avoid confusion for event post-processors.
			// However, we will still call change_mastery on this
			// RG; if the RG is really offline on the slave,
			// change_mastery will be a no-op for it.
			//
			if (!(*master_ns).isEqual(target_ns)) {
				event_res = post_primaries_changing_event(NULL,
				    rg_ptr->rgl_ccr->rg_name,
				    CL_REASON_RG_SWITCHOVER, NULL,
				    rg_ptr->rgl_ccr->rg_desired_primaries,
				    *master_ns, target_ns);
				if (event_res.err_code == SCHA_ERR_NOMEM) {
					//
					// Set the error code to this
					// even if res.err_code
					// was set earlier as this is critical.
					//
					res.err_code = SCHA_ERR_NOMEM;
					goto stop_node_evacuate; //lint !e801
				}
				if (rg_ptr->rgl_ccr->rg_suspended) {
					evac_suspended_rg_warning = B_TRUE;
				}
			}
			delete master_ns;
		}

		if (numrgs == 0) {
			// There is no work to do.
			goto switch_primaries_end; //lint !e801
		}

		if (evac_suspended_rg_warning) {
			// display warning for suspended RGs that were evacuated
			warning_res.err_code = SCHA_ERR_NOERR;
			rgm_format_errmsg(&warning_res, RWM_EVAC_SUSPENDED_RG);
			if (warning_res.err_code != SCHA_ERR_NOERR) {
				goto stop_node_evacuate; //lint !e801
			}
		}

		//
		// Iterate through evac_nodeset and initiate evacuation of all
		// RGs from each node.
		//
		lni = 0;
		while ((lni = evac_nodeset.nextLniSet(lni + 1)) != 0) {
			ln = lnManager->findObject(lni);
			CL_PANIC(ln != NULL);
			ln->beginEvac();

			//
			// Bring all RGs offline on lni.
			// Passing NULL for both onlist and offlist indicates
			// that all RGs on zone should be evacuated.
			// Each slave will calculate the correct set of RGs.
			//
			switch (rgm_change_mastery(lni, NULL, NULL)) {
			case RGMIDL_OK:
				continue;
			case RGMIDL_NODE_DEATH:
			case RGMIDL_RGMD_DEATH:
				//
				// For these errors, slave node died or rgmd
				// died.  Just proceed to the next node.
				//
				ucmm_print("scswitch_primaries", NOGET(
				    "lni %d evac failed with NODE_DEATH"
				    " or RGMD_DEATH"), lni);
				continue;
			case RGMIDL_NOREF:
			case RGMIDL_UNKNOWN:
			default:
				//
				// For these errors, something went wrong in
				// ORB communication. Syslog an error message,
				// and bail out.  Use the existing syslog
				// message used by do_rg_switch().
				//
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// An inter-node communication failed with an
				// unknown exception while the rgmd daemon was
				// attempting to execute an operator-requested
				// switch of the primaries of a resource
				// group, or was attempting to "fail back" a
				// resource group onto a node that just
				// rejoined the cluster. This will cause the
				// attempted switching action to fail.
				// @user_action
				// Examine other syslog messages occurring
				// around the same time on the same node, to
				// see if the cause of the problem can be
				// identified. If the switch was
				// operator-requested, retry it. If the same
				// error recurs, you might have to reboot the
				// affected node. Since this problem might
				// indicate an internal logic error in the
				// clustering software, save a copy of the
				// /var/adm/messages files on all nodes and
				// the output of clresourcetype show -v,
				// clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "scswitch: rgm_change_mastery() failed "
				    "with NOREF, UNKNOWN, or invalid error on "
				    "node %s", ln->getFullName());
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_ORB;
				rgm_format_errmsg(&res, REM_INTERNAL);
				goto end_evac_orb_error; //lint !e801
			}
		}

		//
		// Flush the state change queue before entering
		// the cond_wait loop.
		//
		rgm_unlock_state();
		flush_state_change_queue();
		rgm_lock_state();

		//
		// Loop on cond_wait until all RGs are no longer pending
		// offline on any node in evac_nodeset.
		//
		// rgs_are_stopping() may set an error code/message in res
		// when it returns B_FALSE.
		// process_needy_rg() runs rebalance on each RG in
		// rglistp that achieves OFFLINE state.  rebalance excludes
		// as candidates all evacuating nodes.
		// Verbose flag is passed to print verbose message.
		//
		while (rgs_are_stopping(&temp_res, rglistp, evac_nodeset,
		    verbose_flag)) {
			cond_slavewait.wait(&Rgm_state->rgm_mutex);
		}

		// Check for returned error and update res.
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
		}

		// Append error message to res if there was any.
		(void) transfer_message(&res, &temp_res);

		// reset temp_res.
		temp_res.err_code = SCHA_ERR_NOERR;

		//
		// At this point, rgs_are_stopping() has removed the members
		// of evac_nodeset from the per-RG rgl_evac_nodes nodeset
		// for each RG in rglistp.
		//
		// Iterate through evac_nodeset, invoking endEvac() on each
		// logical node.  This ends the evacuation proper and begins
		// the sticky evac timeout period.  Note, if another thread
		// is simultaneously evacuating the same node, endEvac() will
		// set the sticky timeout to the larger of those specified
		// by the two evacuation commands.
		//
		lni = 0;
		while ((lni = evac_nodeset.nextLniSet(lni + 1)) != 0) {
			ln = lnManager->findObject(lni);
			CL_PANIC(ln != NULL);
			ln->endEvac(evac_sticky_timeout);
		}

		//
		// Clear the rgl_switching flags of the arguments
		// and return.
		//
		goto clear_rgl_switching; //lint !e801
	} // if (is_evacuating)

	//
	// If we are rebalancing or remastering and no RGs are specified,
	// rebalance or remaster all RGs.
	// If we are not rebalancing or remastering, this is an scswitch -z
	// or scswitch -F|Q request (or the equivalent request from
	// from scshutdown), in which case RGs must be specified.
	//

	numrgs = (uint_t)scswp_args.RG_names.length();

	// check for invalid or duplicate RGs.
	if (numrgs > 0) {
		temp_res = prune_operand_list(scswp_args.RG_names,
		    rgnames_seq, B_FALSE);

		if (temp_res.err_code != SCHA_ERR_NOERR) {
			smart_overlay(res, temp_res);
			temp_res.err_code = SCHA_ERR_NOERR;
		}

		(void) transfer_message(&res, &temp_res);

		numrgs = (uint_t)rgnames_seq.length();
		if (numrgs == 0)
			goto switch_primaries_end; //lint !e801
	} else {
		if (!is_rebalancing && !switch_back)
			goto switch_primaries_end; //lint !e801
	}

	complete_RG_names = (const char **)calloc_nocheck(numrgs + 1,
	    sizeof (char *));
	if (complete_RG_names == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto switch_primaries_end; //lint !e801
	}

	for (i = 0; i < numrgs; i++) {
		complete_RG_names[i] = rgnames_seq[i];
	}

	//
	// If rebalancing or remastering and the list of RGs specified is
	// non-null check if any RG delegates its switchover requests to
	// another RG, and add it to the list if needed.
	// There's no need to worry about the order in which the RGs are
	// listed, as they will be re-ordered later.
	//
	if ((switch_back || is_rebalancing) && (numrgs > 0)) {
		temp_res = add_switchover_delegate(&complete_RG_names, &numrgs,
		    &warning_res);
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			goto switch_primaries_end; //lint !e801
		}
	}

	// Check rgname args and build pointer list rglistp
	if (numrgs > 0) {
		//
		// Note that calloc initializes all elements to NULL;
		// the last element rglistp[numrgs] will remain NULL.
		//
		rglistp = (rglist_p_t *)calloc_nocheck((size_t)(numrgs + 1),
		    sizeof (rglist_p_t));

		if (rglistp == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto switch_primaries_end; //lint !e801
		}

		// Savet rglipstp into orig_rglistp as rglistp would be
		// changed below.
		// This is intented to check for clrg online / clrg offline
		//  for specified rgs.
		orig_rglistp = (rglist_p_t *)calloc_nocheck(
		    (size_t)(numrgs + 1), sizeof (rglist_p_t));

		if (orig_rglistp == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto switch_primaries_end; //lint !e801
		}

		current_nodeset = new LogicalNodeset[numrgs];
		if (current_nodeset == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto switch_primaries_end; //lint !e801
		}
		uint_t k;
		for (i = 0, k = 0; i < numrgs; i++) {
			uint_t j;
			LogicalNodeset pending_boot_ns;
			LogicalNodeset stop_failed_ns;
			LogicalNodeset offline_ns;

			//
			// Check that the RG name exists and is not UNMANAGED
			//
			rg_ptr = rgname_to_rg(complete_RG_names[i]);

			if (!globalflag) {
				nodelist_check(rg_ptr, scswp_args.nodeid,
				    scswp_args.idlstr_zonename,
				    nodelist_contains);
				if (!nodelist_contains) {
					temp_res.err_code = SCHA_ERR_ACCESS;
					if (res.err_code == SCHA_ERR_NOERR)
						res.err_code =
						    temp_res.err_code;
					rgm_format_errmsg(&res,
					    REM_RG_NODELIST_NGZONE,
					    rg_ptr->rgl_ccr->rg_name,
					    (const char *)
					    scswp_args.idlstr_zonename);
					temp_res.err_code = SCHA_ERR_NOERR;
					continue;
				}

			}
			if (rg_ptr->rgl_ccr->rg_unmanaged) {
				ucmm_print("scswitch_primaries()",
				    NOGET("RG %s is "
				    "unmanaged\n"), rg_ptr->rgl_ccr->rg_name);
				temp_res.err_code = SCHA_ERR_STATE;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res, REM_RG_UNMANAGED,
				    complete_RG_names[i]);
				temp_res.err_code = SCHA_ERR_NOERR;
				//  continue instead of bailing out
				continue;
			}

			//
			// Make sure the RG is not currently busy.
			//
			// Note, in_endish_rg_state also checks rgl_switching
			// flag.
			//
			// We don't check for nodes running resource_restart
			// because the state machine handles RG switches
			// to/from such nodes.
			//
			if (!in_endish_rg_state(rg_ptr, &pending_boot_ns,
			    &stop_failed_ns, &offline_ns, NULL)) {
				ucmm_print("scswitch_primaries()", NOGET(
				    "RG <%s> is busy\n"),
				    rg_ptr->rgl_ccr->rg_name);
				temp_res.err_code = SCHA_ERR_RGRECONF;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res, REM_RG_BUSY,
				    rg_ptr->rgl_ccr->rg_name);
				temp_res.err_code = SCHA_ERR_NOERR;
				//  continue instead of bailing out
				continue;
			}

			//
			// If RG is ERROR_STOP_FAILED on any node then it is
			// illegal to attempt to switch it ONLINE on any node:
			// it must not be switched ONLINE on a different node
			// (which is currently OFFLINE) or on a node which it
			// is ERROR_STOP_FAILED. It is OK to attempt to switch
			// it OFFLINE.
			//
			// If we are only rebalancing or remastering, then
			// mnodeset is empty, but we still want to return
			// an error here.
			//
			LogicalNodeset intersect1 = offline_ns & mnodeset;
			LogicalNodeset intersect2 = stop_failed_ns & mnodeset;

			if (!is_offlining && !stop_failed_ns.isEmpty() &&
			    (is_rebalancing || switch_back ||
			    !intersect1.isEmpty() || !intersect2.isEmpty())) {
				ucmm_print("scswitch_primaries()",
				    NOGET("RG <%s> is "
				    "ERROR_STOP_FAILED\n"),
				    rg_ptr->rgl_ccr->rg_name);
				temp_res.err_code = SCHA_ERR_STOPFAILED;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res, REM_RG_STOPFAILED,
				    rg_ptr->rgl_ccr->rg_name);
				temp_res.err_code = SCHA_ERR_NOERR;
				//  continue instead of bailing out
				continue;
			}

			//
			// Add the RG name to our namelist
			//
			const char *temp_name = complete_RG_names[i];
			if ((temp_res = namelist_add_name(&rg_names,
			    (char *)temp_name)).err_code != SCHA_ERR_NOERR) {
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				goto switch_primaries_end; //lint !e801
			}


			rglistp[k] = rg_ptr;
			orig_rglistp[k++] = rg_ptr;

			//
			// The remaining checks only apply when we are
			// switching primaries (not rebalancing or switch_back)
			//
			if (is_rebalancing || switch_back) {
				continue;
			}


			//
			// If an online operation is requested and the
			// rg is a failover rg check if the failover
			// given RG is already online on any node.
			//
			if (is_onlining) {
				LogicalNodeset	tmp_online_ns;
				LogicalNodeset	tmp_target_ns;
				get_rg_online_nodeset(rglistp[k-1],
				    &tmp_online_ns);
				tmp_target_ns = tmp_online_ns | mnodeset;
				if (tmp_target_ns.count() >
				    rglistp[k-1]->rgl_ccr->
				    rg_max_primaries) {
					ucmm_print("scswitch_primaries:",
					    NOGET("exceeds Maximum_primaries "
					    "for RG %s\n"),
					    rglistp[k-1]->rgl_ccr->rg_name);
					temp_res.err_code = SCHA_ERR_INVAL;
					if (res.err_code == SCHA_ERR_NOERR)
						res.err_code =
						    temp_res.err_code;
					rgm_format_errmsg(&res,
					    REM_RG_MAXPRIMARIES,
					    rglistp[k-1]->rgl_ccr->rg_name);

					//
					// reset rglist[k-1] to NULL and
					// decrement k
					//
					rglistp[k-1] = NULL;
					orig_rglistp[k-1] = NULL;
					k--;
					temp_res.err_code = SCHA_ERR_NOERR;
					// continue instead of bailing out.
					continue;
				}
			} else {

				//
				// Check that the request does not cause the
				// RG to exceed its Maximum_primaries setting.
				//
				if ((int)nummasters >
				    rglistp[k-1]->rgl_ccr->rg_max_primaries) {
					ucmm_print("scswitch_primaries:",
					    NOGET("exceeds Maximum_primaries "
					    "for RG %s\n"),
					    rglistp[k-1]->rgl_ccr->rg_name);

					temp_res.err_code = SCHA_ERR_INVAL;
					if (res.err_code == SCHA_ERR_NOERR)
						res.err_code =
						    temp_res.err_code;
					rgm_format_errmsg(&res,
					    REM_RG_MAXPRIMARIES,
					    rglistp[k-1]->rgl_ccr->rg_name);

					//
					// reset rglist[k-1] to NULL and
					// decrement k
					//
					rglistp[k-1] = NULL;
					orig_rglistp[k-1] = NULL;
					k--;
					temp_res.err_code = SCHA_ERR_NOERR;
					// continue instead of bailing out.
					continue;
				}
			}
			//
			// Check that all intended new masters are potential
			// primaries of the RG.
			//
			nodelist_ns = get_logical_nodeset_from_nodelist(
			    rglistp[k-1]->rgl_ccr->rg_nodelist);
			LogicalNodeset badNs = mnodeset - *nodelist_ns;
			if (!badNs.isEmpty()) {
				//
				// produce a message for each bad node
				//
				temp_res.err_code = SCHA_ERR_NODE;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				lni = 0;
				while ((lni = badNs.nextLniSet(lni + 1)) != 0) {
					ln = lnManager->findObject(lni);
					CL_PANIC(ln);
					rgm_format_errmsg(&res, REM_RG_MASTERS,
					    ln->getFullName(),
					    rglistp[k-1]->rgl_ccr->rg_name);
				}

				// reset rglist[k-1] to NULL and decrement k
				rglistp[k-1] = NULL;
				orig_rglistp[k-1] = NULL;
				k--;
				temp_res.err_code = SCHA_ERR_NOERR;
				// continue instead of bailing out.
				continue;
			}
			delete nodelist_ns;

			//
			// "system" property checks:
			// Switch is allowed unless "new master list" (mnodeset)
			// is empty and it is not a cluster shutdown.
			//
			if (is_offlining) {
				LogicalNodeset	tmp_online_ns;
				LogicalNodeset	tmp_target_ns;
				get_rg_online_nodeset(rglistp[k-1],
				    &tmp_online_ns);
				tmp_target_ns = tmp_online_ns - mnodeset;

				if ((rglistp[k-1]->rgl_ccr->rg_system) &&
				    tmp_target_ns.isEmpty() &&
				    stop_failed_ns.isEmpty() &&
				    !is_cluster_shutting) {

					temp_res.err_code = SCHA_ERR_ACCESS;
					if (res.err_code == SCHA_ERR_NOERR)
						res.err_code =
						    temp_res.err_code;
					rgm_format_errmsg(&res,
					    REM_SYSTEM_RG_SWPRIM,
					    rglistp[k-1]->rgl_ccr->rg_name);

					char *ucmm_buffer = NULL;
					if (mnodeset.display
					    (ucmm_buffer) == 0) {
						ucmm_print("scswitch_primaries",
						    NOGET("RG: <%s> RG_SYSTEM "
						    "is TRUE and new_masters"
						    " list: %s\n is empty.\n"),
						    rglistp[k-1]->rgl_ccr->
						    rg_name,
						    ucmm_buffer);
						free(ucmm_buffer);
					}

					// reset rglist[k-1] to NULL. decr k
					rglistp[k-1] = NULL;
					orig_rglistp[k-1] = NULL;
					k--;
					temp_res.err_code = SCHA_ERR_NOERR;
					// continue instead of bailing out.
					continue;
				}
			} else {

				if ((rglistp[k-1]->rgl_ccr->rg_system) &&
				    mnodeset.isEmpty() &&
				    stop_failed_ns.isEmpty() &&
				    !is_cluster_shutting) {
					temp_res.err_code = SCHA_ERR_ACCESS;
					if (res.err_code == SCHA_ERR_NOERR)
						res.err_code =
						    temp_res.err_code;
					rgm_format_errmsg(&res,
					    REM_SYSTEM_RG_SWPRIM,
					    rglistp[k-1]->rgl_ccr->rg_name);

					char *ucmm_buffer = NULL;
					if (mnodeset.display(ucmm_buffer)
					    == 0) {
						ucmm_print("RGM",
						    NOGET("scswitch_primaries:"
						    " RG: <%s>"
						    " RG_SYSTEM property is "
						    "TRUE and "
						    "\"new masters list\" %s\n"
						    "is empty.\n"),
						    rglistp[k-1]->rgl_ccr->
						    rg_name,
						    ucmm_buffer);
						free(ucmm_buffer);
					}
					//
					// reset rglist[k-1] to NULL and
					// decrement k
					//
					rglistp[k-1] = NULL;
					orig_rglistp[k-1] = NULL;
					k--;
					temp_res.err_code = SCHA_ERR_NOERR;
					// continue instead of bailing out.
					continue;
				}
			}
			//
			// If RG is PENDING_BOOT on any node, then it is
			// illegal to attempt to switch it online on that node.
			//
			LogicalNodeset intersect3 = pending_boot_ns & mnodeset;
			if (!intersect3.isEmpty()) {
				ucmm_print("RGM", NOGET(
				    "scswitch_primaries: RG <%s> is BOOTing\n"),
				    rglistp[k-1]->rgl_ccr->rg_name);
				temp_res.err_code = SCHA_ERR_CLRECONF;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res, REM_RG_BUSY,
				    rglistp[k-1]->rgl_ccr->rg_name);

				// reset rglist[k-1] to NULL and decrement k?
				rglistp[k-1] = NULL;
				orig_rglistp[k-1] = NULL;
				k--;
				// continue instead of bailing out.
				temp_res.err_code = SCHA_ERR_NOERR;
				continue;
			}

			// Get the current nodeset needed for posting the event.
			LogicalNodeset	*master_ns = lnManager->
			    get_all_logical_nodes(rgm::LN_SHUTTING_DOWN);
			*master_ns -= offline_ns;
			*master_ns -= pending_boot_ns;
			current_nodeset[k-1] = *master_ns;
			delete master_ns;
		}
		numrgs = k;
	}

	//
	// If we were called with the rgm::REBALANCE flag, sort and rebalance
	// the RGs in rglistp.  If rglistp is NULL, rebalance all
	// managed rgs.
	// If we were called with the rgm::SWITCH_BACK flag remaster the rgs
	// onto their preffered primaries.
	//
	if (is_rebalancing || switch_back) {
		//
		// If rglistp is NULL, populate it with pointers to all
		// managed RGs that are not busy or suspended.
		//
		if (rglistp == NULL) {
			numrgs = 0;
			if (!globalflag) {
				rgm_format_errmsg(&warning_res, RWM_RG_NGZONE);
				if (warning_res.err_code != SCHA_ERR_NOERR) {
					smart_overlay(res, warning_res);
					goto switch_primaries_end; //lint !e801
				}
			}

			for (rg_ptr = Rgm_state->rgm_rglist; rg_ptr != NULL;
			    rg_ptr = rg_ptr->rgl_next) {

				if (!globalflag) {
					nodelist_check(rg_ptr,
					    scswp_args.nodeid,
					    scswp_args.idlstr_zonename,
					    nodelist_contains);
					if (!nodelist_contains) {
						ucmm_print("idl_swtch_prim()",
						    NOGET("RG <%s> failed "
						    "nodelist check; not "
						    "rebalancing"),
						    rg_ptr->rgl_ccr->rg_name);
						continue;
					}
				}
				// Skip over RGs in unmanaged state
				if (rg_ptr->rgl_ccr->rg_unmanaged) {
					ucmm_print("RGM", NOGET(
					    "scswitch_primaries: RG <%s> is "
					    "unmanaged; not rebalancing"),
					    rg_ptr->rgl_ccr->rg_name);
					rgm_format_errmsg(&warning_res,
					    RWM_RG_NOREBAL_UNMAN,
					    rg_ptr->rgl_ccr->rg_name);
					if (warning_res.err_code !=
					    SCHA_ERR_NOERR) {
						smart_overlay(res, warning_res);
						//lint -save -e801
						goto switch_primaries_end;
						//lint -restore
					}
					continue;
				}


				//
				// Skip RG that is busy.  We skip RGs
				// that are evacuating (even though they could
				// theoretically be rebalanced while evacuating)
				// because the evacuate thread will rebalance
				// them, so there is no need to do so here.
				// Append a message to warning_res for each RG
				// that is skipped.
				//
				if (rg_ptr->rgl_switching != SW_NONE) {
					ucmm_print("RGM", NOGET(
					    "scswitch_primaries: RG <%s> is "
					    "busy; not rebalancing"),
					    rg_ptr->rgl_ccr->rg_name);
					rgm_format_errmsg(&warning_res,
					    RWM_RG_NOREBALANCE,
					    rg_ptr->rgl_ccr->rg_name);
					if (warning_res.err_code !=
					    SCHA_ERR_NOERR) {
						if (res.err_code ==
						    SCHA_ERR_NOERR)
							res.err_code =
							warning_res.err_code;
						//lint -save -e801
						goto switch_primaries_end;
						//lint -restore
					}
					continue;
				}

				//
				// Skip RG that is suspended
				//
				if (rg_ptr->rgl_ccr->rg_suspended) {
					ucmm_print("RGM", NOGET(
					    "scswitch_primaries: RG <%s> is "
					    "suspended; not rebalancing"),
					    rg_ptr->rgl_ccr->rg_name);
					rgm_format_errmsg(&warning_res,
					    RWM_RG_NOREBAL_SUSP,
					    rg_ptr->rgl_ccr->rg_name);
					if (warning_res.err_code !=
					    SCHA_ERR_NOERR) {
						if (res.err_code ==
						    SCHA_ERR_NOERR)
							res.err_code =
							warning_res.err_code;
						//lint -save -e801
						goto switch_primaries_end;
						//lint -restore
					}
					continue;
				}

				//
				// Add this RG to rglistp and rg_names
				//
				temp_res = rglist_add_rg(rglistp, rg_ptr,
				    numrgs++);
				if (temp_res.err_code != SCHA_ERR_NOERR) {
					if (res.err_code == SCHA_ERR_NOERR)
						res.err_code =
						    temp_res.err_code;
					goto switch_primaries_end; //lint !e801
				}
				if ((temp_res = namelist_add_name(&rg_names,
				    rg_ptr->rgl_ccr->rg_name)).err_code !=
				    SCHA_ERR_NOERR) {
					if (res.err_code == SCHA_ERR_NOERR)
						res.err_code =
						    temp_res.err_code;
					goto switch_primaries_end; //lint !e801
				}
			}

			//
			// Append warning messages (from above for-loop) to res
			//
			if (warning_res.err_msg) {
				rgm_sprintf(&res, "%s", warning_res.err_msg);
				free(warning_res.err_msg);
				warning_res.err_msg = NULL;
				if (res.err_code != SCHA_ERR_NOERR) {
					goto switch_primaries_end; //lint !e801
				}
			}
		}

		//
		// If rglistp is still NULL, there is no work to do.
		//
		if (rglistp == NULL)
			goto switch_primaries_end; //lint !e801

		//
		// RGs in rglistp are being rebalanced, which means
		// that the RGM will attempt to bring them online
		// on one or more nodes.
		// Check their depended-on RGs to make sure that
		// they are (or will be) mastered on one or more nodes.
		//
		for (i = 0; i < numrgs; i++) {
			check_offline_dependees(&temp_res, rglistp[i],
			    rglistp, emptyNodeset);

			if (temp_res.err_code != SCHA_ERR_NOERR) {
				if (temp_res.err_code == SCHA_ERR_NOMEM)
					goto switch_primaries_end; //lint !e801
				// Add this RG to failed rg list.
				add_op_to_failed_list(&failed_op_list,
				    rglistp[i]->rgl_ccr->rg_name);
				(void) transfer_message(&res, &temp_res);
				smart_overlay(res, temp_res);

				// continue instead of bailing out.
				temp_res.err_code = SCHA_ERR_NOERR;
				continue;
			}
		}

		//
		// Reset  non failure RG's ok_to_start flag if necessary.
		//
		// If set_ok_to_start returns an error, it is a fatal error
		// (e.g., NOMEM or INTERNAL) and takes precedence over any
		// warning messages generated above.  We discard the warning
		// messages so that scswitch will write the more serious
		// error message to stderr.
		//
		for (i = 0; i < numrgs; i++) {
			// dont process if RG is in failed RG list.
			if (namelist_exists(failed_op_list,
			    rglistp[i]->rgl_ccr->rg_name))
				continue;
			temp_res = set_ok_to_start(rglistp[i]);
			if (temp_res.err_code != SCHA_ERR_NOERR) {
				// This is a critical error.
				// Update the status in res itself.
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code =
					    temp_res.err_code;
				(void) transfer_message(&res, &temp_res);
				goto switch_primaries_end; //lint !e801
			}
		}


		// check if this is a rg remastering request.
		if (switch_back) {
			//
			// Set the rgl_switching flag of each RG in rglistp to
			// SW_IN_PROGRESS.
			//

			for (i = 0; i < numrgs; i++) {
				// dont process if RG is in failed RG list.
				if (namelist_exists(failed_op_list,
				    rglistp[i]->rgl_ccr->rg_name))
					continue;
				rglistp[i]->rgl_switching = SW_IN_PROGRESS;
			}

			//
			// Sort the RG list by strong affinities and
			// rg_dependencies.
			//
			temp_res = switch_order_rgarray(rglistp);
			if (temp_res.err_code != SCHA_ERR_NOERR) {
				// This error occurs because of low memory.
				// Stop processing.
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				goto switch_primaries_end; //lint !e801
			}

			for (i = 0; i < numrgs; i++) {
				// dont process if RG is in failed RG list.
				if (namelist_exists(failed_op_list,
				    rglistp[i]->rgl_ccr->rg_name))
					continue;
				// perform switchback.
				temp_res = switchback(
				    rglistp[i]->rgl_ccr->rg_name, rg_names,
				    verbose_flag);

				if (temp_res.err_code != SCHA_ERR_NOERR) {
					if (temp_res.err_msg) {
						rgm_sprintf(&res, "%s",
						    temp_res.err_msg);
						free(temp_res.err_msg);
						temp_res.err_msg = NULL;
						if (res.err_code ==
						    SCHA_ERR_NOERR)
							res.err_code =
							    temp_res.err_code;
					}

					temp_res.err_code = SCHA_ERR_NOERR;
					//  continue instead of bailing out.
					continue;
				} else if (temp_res.err_msg != NULL) {
					rgm_sprintf(&warning_res, "%s",
					    temp_res.err_msg);
					free(temp_res.err_msg);
					temp_res.err_msg = NULL;

					if (warning_res.err_code !=
					    SCHA_ERR_NOERR) {
						res.err_code =
						    warning_res.err_code;
						//lint -save -e801
						goto switch_primaries_end;
						//lint -restore
					}
				}

			}
		} else {

			//
			// rebalance if this is not a switch_back operation.
			// We have to call rebalance before setting the
			// rgl_switching flag, otherwise rebalance would
			// refuse to operate on the RG.
			//
			// Note, rebalance() will post an event for non
			// failed RG.
			//
			// We pass B_TRUE to rebalance_rgs() for the
			// set_planned_on_nodes parameter.  This causes
			// rebalance to add the onlining nodes to
			// rgl_planned_on_nodes for each rg.  We will
			// clear rgl_planned_on_nodes below when the
			// command is completed.
			//
			rebalance_rgs(rglistp, B_FALSE, emptyNodeset, B_FALSE,
			    B_FALSE, B_TRUE);
			//
			// Set the rgl_switching flag of each RG in rglistp to
			// SW_IN_PROGRESS.
			//
			// Note, in all other cases where rebalance is run,
			// we do not set rgl_switching because there is no
			// thread waiting for the rebalance to finish.
			// We only set it here so we can generate an
			// appropriate error message if we are "busted",
			// e.g., by a Start or Stop method failure or node
			// death.
			//
			for (i = 0; i < numrgs; i++) {
				// dont process if RG is in failed RG list.
				if (namelist_exists(failed_op_list,
				    rglistp[i]->rgl_ccr->rg_name))
					continue;
				rglistp[i]->rgl_switching = SW_IN_PROGRESS;
			}
		}

		//
		// Flush the state change queue before entering the
		// cond_wait loop.
		//
		rgm_unlock_state();
		flush_state_change_queue();
		rgm_lock_state();

		//
		// Loop on cond_wait until all RGs are no longer rebalancing.
		// Although "no longer rebalancing" is not a well-defined
		// state, we wait until each of the non failed RGs being
		// rebalanced
		// has moved out of a pending_online state on all nodes, or
		// the switch on that RG is "busted" by a node death or
		// failure of STOP or START method.
		//
		// rgs_are_rebalancing() may set an error code and messages
		// in res.  We save a non-zero error code so that we may
		// return it.  If the error code is SCHA_ERR_NOMEM, the
		// function returns B_FALSE and we return immediately.
		// rgs_are_rebalancing() also resets the rgl_switching flag
		// and rgl_planned_on_nodes nodeset
		// on each RG, and removes each non failed RG from the list
		// (except for PENDING_ONLINE_BLOCKED RGs) as it is
		// done processing it.
		//
		// Since suspended RGs may be rebalanced if named specifically
		// in the command line, we set the third argument of
		// rgs_are_rebalancing() to false, indicating that it should
		// check the status of any suspended RGs in the list.
		//

		// rgs_are_rebalancing is enhanced to skip failed RGs.
		while (*rglistp && rgs_are_rebalancing(&temp_res, rglistp,
		    failed_op_list, B_FALSE, verbose_flag, switch_back)) {
			if (temp_res.err_code != SCHA_ERR_NOERR) {
				if (res.err_code == SCHA_ERR_NOERR) {
					res.err_code = temp_res.err_code;
				}
				busted_err = temp_res.err_code;
				temp_res.err_code = SCHA_ERR_NOERR;
			}
			cond_slavewait.wait(&Rgm_state->rgm_mutex);
		}

		if (temp_res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_NOERR) {
				res.err_code = temp_res.err_code;
			}
			temp_res.err_code = SCHA_ERR_NOERR;
		}

		if (res.err_code == SCHA_ERR_NOERR &&
		    busted_err != SCHA_ERR_NOERR) {
			res.err_code = busted_err;
		}

		(void) transfer_message(&res, &temp_res);

		//
		// Check for PENDING_ONLINE_BLOCKED.
		//
		for (temp_rg_name = rg_names; temp_rg_name != NULL;
		    temp_rg_name = temp_rg_name->nl_next) {
			rgp = rgname_to_rg(temp_rg_name->nl_name);
			if (rgp != NULL) {
				//
				// The res.err_code is not updated in the
				// below called function.Use res instead
				// of temp_res.
				//
				check_pending_online_blocked(&res, rgp);
			}
		}

		//
		// In case we are exiting early (e.g. NOMEM error), or if
		// there are PENDING_ONLINE_BLOCKED RGs still in rglistp, make
		// sure rgl_switching flags and rgl_planned_on_nodes get
		// cleared.
		//
		goto clear_rgl_switching; //lint !e801
	}


	//
	// We are not rebalancing or evacuating. Execute scswitch -z -g -h.
	//
	//
	// Clear evacuation flags for nodes in mnodeset.  An explicit
	// scswitch of any RG onto a node has the side effect of immediately
	// expiring any previous "sticky" evacuation of that node which has
	// not yet expired.
	//
	lni = 0;
	while ((lni = mnodeset.nextLniSet(lni + 1)) != 0) {
		ln = lnManager->findObject(lni);
		CL_PANIC(ln);
		ln->clearEvac();
	}


	for (i = 0; i < numrgs; i++) {
		LogicalNodeset	tmp_online_ns;
		LogicalNodeset	tmp_target_ns;

		get_rg_online_nodeset(rglistp[i],
		    &tmp_online_ns);

		if (is_offlining) {
			tmp_target_ns = tmp_online_ns - mnodeset;
		} else if (is_onlining) {
			tmp_target_ns = tmp_online_ns | mnodeset;
		} else {
			tmp_target_ns = mnodeset;
		}

		//
		// check if the RG is already offline. If so the calls to
		// check_online_dependents and check_online_resource_dependents
		// should not be made.
		//
		if (tmp_target_ns.isEmpty() && !tmp_online_ns.isEmpty()) {
			check_online_dependents(&temp_res, rglistp,
			    (int)i);
			if (temp_res.err_code == SCHA_ERR_NOMEM) {
				// fatal error
				goto switch_primaries_end; //lint !e801
			}
			if (temp_res.err_code != SCHA_ERR_NOERR) {
				// Add the RG to failed rg list.
				add_op_to_failed_list(&failed_op_list,
				    rglistp[i]->rgl_ccr->rg_name);
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code =
					    temp_res.err_code;

				(void) transfer_message(&res, &temp_res);

				temp_res.err_code = SCHA_ERR_NOERR;
				//  continue instead of bailing out.
				continue;
			}
			check_online_resource_dependents(&temp_res,
			    rglistp[i], rglistp, failed_op_list);
			if (temp_res.err_code != SCHA_ERR_NOERR) {
				// Add the RG to failed rg list.
				add_op_to_failed_list(&failed_op_list,
				    rglistp[i]->rgl_ccr->rg_name);
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code =
					    temp_res.err_code;
				(void) transfer_message(&res, &temp_res);

				temp_res.err_code = SCHA_ERR_NOERR;
				//  continue instead of bailing out.
				continue;
			}

		} else {
			LogicalNodeset	online_nodeset = tmp_target_ns -
			    current_nodeset[i];
			if (!online_nodeset.isEmpty())
				check_offline_dependees(&temp_res, rglistp[i],
				    rglistp, online_nodeset);
			if (temp_res.err_code == SCHA_ERR_NOMEM) {
				// fatal error
				goto switch_primaries_end; //lint !e801
			}
			if (temp_res.err_code != SCHA_ERR_NOERR) {
				// Add the RG to failed rg list.
				add_op_to_failed_list(&failed_op_list,
				    rglistp[i]->rgl_ccr->rg_name);
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code =
					    temp_res.err_code;
				(void) transfer_message(&res, &temp_res);
				temp_res.err_code = SCHA_ERR_NOERR;
				//  continue instead of bailing out.
				continue;
			}
		}

	}

	//
	// Loop through all the non failed RGs checking affinities.
	// We can't check affinities in the above loop, because we need to
	// have the complete list of RGs that we are switching.
	// We can't check in the loop
	// below that sets the switching flags, because we don't want to set
	// the switching flags until we're sure we're going to switch.
	//
	// In checking affinities we make the simplifying assumption that
	// all RGs being switched are coming online on the same nodes
	// (mnodeset).  If this routine ever changes to pick a "best" node
	// for each RG separately, this assumption will need to be re-examined.
	//
	for (i = 0; i < numrgs; i++) {
		int numon, numpendon, rgmasters;
		boolean_t alldone;
		// skip failed RGs.
		if (namelist_exists(failed_op_list,
		    rglistp[i]->rgl_ccr->rg_name))
			continue;

		//
		// Call scswitch_examine_rg to get the real list of onnodes
		// and offnodes (not just the new master nodes).
		//

		scswitch_examine_rg(&temp_res, rglistp[i],
		    rglistp[i]->rgl_ccr->rg_name, B_TRUE, mnodeset, onnodes,
		    offnodes, &numon, &numpendon, &rgmasters, &alldone,
		    is_offlining, is_onlining);
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			// Update failed_op_list.
			add_op_to_failed_list(&failed_op_list,
			    rglistp[i]->rgl_ccr->rg_name);
			if (temp_res.err_msg) {
				rgm_sprintf(&res, "%s", temp_res.err_msg);
				free(temp_res.err_msg);
				temp_res.err_msg = NULL;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
			}

			temp_res.err_code = SCHA_ERR_NOERR;
			//  continue instead of bailing out.
			continue;
		}

		temp_res = check_rg_affinities_offlining(rglistp[i],
		    offnodes, rg_names);
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			// Add the RG to failed rg list.
			add_op_to_failed_list(&failed_op_list,
			    rglistp[i]->rgl_ccr->rg_name);
			if (temp_res.err_msg) {
				rgm_sprintf(&res, "%s", temp_res.err_msg);
				free(temp_res.err_msg);
				temp_res.err_msg = NULL;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
			}

			temp_res.err_code = SCHA_ERR_NOERR;
			//  continue instead of bailing out.
			continue;
		} else if (temp_res.err_msg != NULL) {
			rgm_sprintf(&warning_res, "%s", temp_res.err_msg);
			free(temp_res.err_msg);

			if (warning_res.err_code != SCHA_ERR_NOERR) {
				res.err_code = warning_res.err_code;
				goto switch_primaries_end; //lint !e801
			}
		}

		temp_res = check_rg_affinities_onlining(rglistp[i], onnodes,
		    rg_names);
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			// Add the RG to failed rg list.
			add_op_to_failed_list(&failed_op_list,
			    rglistp[i]->rgl_ccr->rg_name);
			if (temp_res.err_msg) {
				rgm_sprintf(&res, "%s", temp_res.err_msg);
				free(temp_res.err_msg);
				temp_res.err_msg = NULL;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
			}

			temp_res.err_code = SCHA_ERR_NOERR;
			//  continue instead of bailing out.
			continue;
		} else if (temp_res.err_msg != NULL) {
			rgm_sprintf(&warning_res, "%s", temp_res.err_msg);
			free(temp_res.err_msg);
			if (warning_res.err_code != SCHA_ERR_NOERR) {
				res.err_code = warning_res.err_code;
				goto switch_primaries_end; //lint !e801
			}
		}
	}

	//
	// Set the rgl_switching flag of non failed RG in rglistp to
	// SW_IN_PROGRESS.
	// Post a primaries_changing event for each RG.
	// Reset each RG's ok_to_start flag if necessary.
	//
	// Also, if the target nodeset is empty, set the
	// rgl_no_triggered_rebalance flag on each of the
	// RGs.  We don't want the RG to get rebalanced automatically because
	// of affinities until after this command returns.
	//
	for (i = 0; i < numrgs; i++) {
		// skip failed Rgs.
		if (namelist_exists(failed_op_list,
		    rglistp[i]->rgl_ccr->rg_name))
			continue;

		rglistp[i]->rgl_switching = SW_IN_PROGRESS;
		//
		// If the target nodeset is empty, this is a node evacuate
		// or an scswitch -z without any hosts.  We don't want
		// do a triggered rebalance on any of the RGs, because
		// they should end up offline.
		//
		if (mnodeset.isEmpty()) {
			rglistp[i]->rgl_no_triggered_rebalance = B_TRUE;
		}

		//
		// Post an event for this RG. Since this is an scswitch
		// triggered event, resource name is NULL.
		// Also, node is NULL because scswitch -z command is not
		// associated with any particular node.
		//
		CL_PANIC(current_nodeset != NULL);
		if (!current_nodeset[i].isEqual(mnodeset)) {
			event_res = post_primaries_changing_event(NULL,
			    rglistp[i]->rgl_ccr->rg_name,
			    CL_REASON_RG_SWITCHOVER, NULL,
			    rglistp[i]->rgl_ccr->rg_desired_primaries,
			    current_nodeset[i], mnodeset);
			if (event_res.err_code == SCHA_ERR_NOMEM) {
				res.err_code = SCHA_ERR_NOMEM;
				goto clear_rgl_switching; //lint !e801
			}
		}

		//
		// Set the ok_to_start flag on the RG if mnodeset is
		// non-empty and is_offlining is false.
		//
		if ((!mnodeset.isEmpty()) && (!is_offlining)) {
			temp_res = set_ok_to_start(rglistp[i]);
			if (temp_res.err_code != SCHA_ERR_NOERR) {
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				(void) transfer_message(&res, &temp_res);
				goto clear_rgl_switching; //lint !e801
			}
		}
	}
	//
	// Call do_rg_switch() to actually carry out the switch
	// Note that this function clears rgl_switching flag on all RGs
	// in rglistp.
	//
	// do_rg_switch is enhanced  to accept failed_op_list.
	do_rg_switch(&temp_res, rglistp, mnodeset, emptyNodeset, is_offlining,
	    is_onlining, failed_op_list);

	// Check for any errors.
	if (temp_res.err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_NOERR)
			res.err_code = temp_res.err_code;
		if (temp_res.err_msg) {
			rgm_sprintf(&res, "%s", temp_res.err_msg);
			free(temp_res.err_msg);
			temp_res.err_msg = NULL;

		}
	}
	//
	// Remove the rgl_no_triggered_rebalance flag on the RGs.
	// Use the namelist to get pointers to all the RGS (because
	// there's a slim chance that the RG could have been deleted out
	// from under us after the switching flag was reset, while we
	// had the lock released).
	//
	// Also call check_pending_online_blocked to check if any
	// RGs are in PENDING_ONLINE_BLOCKED and to generate a warning
	// message if they are.
	//
	for (temp_rg_name = rg_names; temp_rg_name != NULL;
	    temp_rg_name = temp_rg_name->nl_next) {
		// skip failed Rgs.
		if (namelist_exists(failed_op_list,
		    temp_rg_name->nl_name))
			continue;
		rgp = rgname_to_rg(temp_rg_name->nl_name);
		if (rgp != NULL) {
			rgp->rgl_no_triggered_rebalance = B_FALSE;
			check_pending_online_blocked(&res, rgp);
		}
	}

	goto switch_primaries_end; //lint !e801

end_evac_orb_error:
	//
	// We have encountered an ORB error during node evacuation.
	// We don't want to risk going into cond_wait() because we might
	// never get awakened.  Clear the LN evacuation flags that were
	// set above.
	//
	lni = 0;
	while ((lni = evac_nodeset.nextLniSet(lni + 1)) != 0) {
		ln = lnManager->findObject(lni);
		CL_PANIC(ln != NULL);
		ln->clearEvac();
	}

stop_node_evacuate:
	//
	// We are exiting early from a node evacuation due to a NOMEM error.
	// We have not yet released the state lock or executed any
	// rgm_change_mastery calls.
	//
	// Reset evac_nodes based on the saved value.  If the nodeset is
	// then empty (indicating that no other thread is currently
	// evacuating the same node), reset rgl_switching to SW_NONE if
	// it is currently SW_EVACUATING.
	//
	if (rglistp != NULL) {
		for (i = 0; rglistp[i] != NULL; i++) {
			// skip failed Rgs.
			if (namelist_exists(failed_op_list,
			    rglistp[i]->rgl_ccr->rg_name))
				continue;
			rglistp[i]->rgl_evac_nodes = save_evac[rglistp[i]];
			if (rglistp[i]->rgl_evac_nodes.isEmpty() &&
			    rglistp[i]->rgl_switching == SW_EVACUATING) {
				rglistp[i]->rgl_switching = SW_NONE;
			}
		}
	}

	goto switch_primaries_end; //lint !e801

clear_rgl_switching:

	//
	// Clear the rgl_switching flag and rgl_planned_on_nodes nodeset
	// of each RG in rglistp.
	// However, if rgl_switching is SW_EVACUATING, then only
	// reset rgl_switching if rgl_evac_nodes is empty.
	//
	for (i = 0; rglistp[i] != NULL; i++) {
		// skip failed Rgs.
		if (namelist_exists(failed_op_list,
		    rglistp[i]->rgl_ccr->rg_name))
			continue;
		rglistp[i]->rgl_planned_on_nodes.reset();
		if (rglistp[i]->rgl_switching != SW_EVACUATING ||
		    rglistp[i]->rgl_evac_nodes.isEmpty())
			rglistp[i]->rgl_switching = SW_NONE;
	}

switch_primaries_end:

	//
	// if the request was for onlining on some specified nodelist
	// check if the RGs are brought online on the specified
	// nodelist and print the success message.
	//
	if ((is_onlining) && (orig_rglistp != NULL)) {
		boolean_t op_flag = B_TRUE;
		for (i = 0; orig_rglistp[i] != NULL; i++) {
			// skip failed Rgs
			if (namelist_exists(failed_op_list,
			    orig_rglistp[i]->rgl_ccr->rg_name))
				continue;
			lni = LNI_UNDEF;
			while (((lni = onnodes.nextLniSet(lni + 1)) != 0) &&
			    (op_flag)) {
				ln = lnManager->findObject(lni);
				CL_PANIC(ln != NULL);
				rgm::lni_t j = ln->getLni();
				if (!ln->isInCluster()) {
					continue;
				}

				if (!is_rg_online_on_node(orig_rglistp[i], j,
				    B_TRUE)) {
					op_flag = B_FALSE;
				}
			}

			if (verbose_flag) {
				if (op_flag) {
					rgm_format_successmsg(&res,
					    RWM_RG_ONLINED,
					    orig_rglistp[i]->rgl_ccr->rg_name);
				} else {
					rgm_format_errmsg(&res,
					    REM_RG_ONLINING_FAILED,
					    orig_rglistp[i]->rgl_ccr->rg_name);
				}
			}
		}
	} else if ((is_offlining) && (orig_rglistp != NULL)) {

		//
		// if the request was for offlining on some specified nodelist
		// check if the RGs are brought offline on the specified
		// nodelist and print the success message.
		//
		boolean_t op_flag = B_TRUE;
		for (i = 0; orig_rglistp[i] != NULL; i++) {
			// skip failed Rgs
			if (namelist_exists(failed_op_list,
			    orig_rglistp[i]->rgl_ccr->rg_name))
				continue;
			lni = LNI_UNDEF;
			while (((lni = offnodes.nextLniSet(lni + 1)) != 0) &&
			    (op_flag)) {
				ln = lnManager->findObject(lni);
				CL_PANIC(ln != NULL);
				rgm::lni_t j = ln->getLni();
				if (!ln->isInCluster()) {
					continue;
				}

				if (is_rg_online_on_node(orig_rglistp[i], j,
				    B_TRUE)) {
					op_flag = B_FALSE;
				}
			}

			if (verbose_flag) {
				if (op_flag) {
					rgm_format_successmsg(&res,
					    RWM_RG_OFFLINED,
					    orig_rglistp[i]->rgl_ccr->rg_name);
				} else {
					rgm_format_errmsg(&res,
					    REM_RG_OFFLINING_FAILED,
					    orig_rglistp[i]->rgl_ccr->rg_name);
				}
			}
		}
	}
	free(rglistp);
	free(orig_rglistp);
	free(complete_RG_names);
	rgm_free_nlist(rg_names);
	if (failed_op_list)
		rgm_free_nlist(failed_op_list);

	delete [] current_nodeset;

	//
	// If we had a warning, append it to the error message (as
	// long as we don't have a legitimate error).
	//
	if (warning_res.err_msg && res.err_code == SCHA_ERR_NOERR) {
		rgm_sprintf(&res, "%s", warning_res.err_msg);
	}
	free(warning_res.err_msg);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);
	free(temp_res.err_msg);
	result = retval;
	rgm_unlock_state();

	ucmm_print("RGM", NOGET("exiting idl_scswitch_primaries()\n"));
}



/*
 * idl_quiesce_rgs
 * Run on the President. Called by scswitch(1M)
 * Return SCHA_ERR_NOERR(thru out parameter 'result') if everything went as
 * planned. Otherwise return an error code and/or (warning-)message. scq_args
 * contains a list of rg names to be quiesced, a list of resource names whose
 * inflight methods may be killed to spped quiesce operation and a kill flag.
 * Duplicate/invalid rgs generate error. If rglist is empty, all rgs configured
 * need to be quiesced. kill_flag indicates whether fast-quiesce is desired. If
 * kill_flag is not set, resource list is ignored. If resource list is empty and
 * kill flag is set, all resources of rgs specified are considered. resources
 * not belonging to rgs in rglist are ignored for fast quiesce.
 * verbose_flag is used to print the success message.
 */

void
rgm_comm_impl::idl_quiesce_rgs(
	const rgm::idl_quiesce_rgs_args &scq_args,
	rgm::idl_regis_result_t_out scq_res, bool verbose_flag,
	Environment &)
{
	uint_t		i;

	rlist_p_t	*rlistp = NULL;
	rglist_p_t	*rglistp = NULL;
	uint_t		numresources, numrgs;

	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();

	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	warning_res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	errors = {SCHA_ERR_NOERR, NULL};

	boolean_t is_fast_quiesce = (scq_args.flags & rgm::FAST_QUIESCE) ?
	    B_TRUE : B_FALSE;

	ucmm_print("RGM", NOGET("in idl_scquiesce_rgs()\n"));

	// lock state structure before accessing President info
	rgm_lock_state();



	numrgs = (uint_t)scq_args.RG_names.length();

	//
	// build RG ptrs. Note that we don't omit endish RGs here,
	// so that we don't produce unnecessary warnings of resources not
	// belonging to RGs
	//
	if (numrgs > 0) {
		res.err_code = rgnames_to_rglist(scq_args.RG_names, &rglistp,
		    &numrgs, scq_args.nodeid, scq_args.idlstr_zonename,
		    &errors);
	} else {
		res.err_code = get_rglist(&rglistp, &numrgs, scq_args.nodeid,
		    (const char *)scq_args.idlstr_zonename, &warning_res);

	}
	if (res.err_code != SCHA_ERR_NOERR) {
		goto quiesce_rgs_end; //lint !e801
	}

	numresources = scq_args.resource_names.length();

	if (is_fast_quiesce && numresources != 0) {
		res.err_code = SCHA_ERR_INTERNAL;
		goto quiesce_rgs_end; //lint !e801
#if 0
		//
		// CURRENTLY, the code has been ifdefed out since such args
		// are not expected from cli. This can change in later releases
		//

		//
		// build list of resources whose methods are to be killed
		// ignores invalid and duplicate resource names.
		// updates the numresources field.
		// NOTE: currently numresources is always zero. If it changes
		// in future, we need to make changes in rgm_launch_method
		// where method execution is skipped assuming that all and not
		// just a subset of resources can be selected for quiescing, if
		// an rg is being fast quiesced.
		//
		res = rnames_to_rlist(scq_args.resource_names, &rlistp,
		    &numresources, rglistp, numrgs, &warning_res);

		if (res.err_code != SCHA_ERR_NOERR)
			goto quiesce_rgs_end;

#endif
	}

	if (numrgs == 0)
		goto quiesce_rgs_end;	// nothing to be done
	// we don't put this check earlier because, we want to catch warnings
	// for invalid resource names.

	//
	// we now set the name server tag for each non endish RG
	// in the list.
	// sort of atomic fn. either sets for all rgs in the list,
	//  or upon error, clears tags for all rgs set and returns error.
	//
	res = quiesce_set_nameserver_tags(rglistp, numrgs, is_fast_quiesce,
	    &warning_res);
	if (res.err_code != SCHA_ERR_NOERR)
		goto quiesce_rgs_end; //lint !e801

	// kill start methods to speedup quiescing
	if (is_fast_quiesce) {
		rgm::idl_string_seq_t rsnames;
		rptrs_to_strseq(rlistp, rsnames);
		//
		// if rlistp is not NULL, and still rsnames.length() == 0
		// no resources to be fast quiesced
		//
		if (rlistp == NULL || rsnames.length() != 0) {
			for (i = 0; i < numrgs; i++) {
				//
				// skip rgs that are not quiescing.
				// the check is also made again on
				// slave nodes using is_quiesce_running()
				// when idl_kill_methods is called.
				//
				if (!(rglistp[i]->rgl_quiescing))
					continue;
				kill_methods(rglistp[i], rsnames, &warning_res);
			}
		}
	}

	// Wait till all the RGs are quiesced.
	for (i = 0; i < numrgs; i++) {
		LogicalNodeset start_failed_ns, stop_failed_ns;
		while (!is_rg_quiesced(rglistp[i],
		    start_failed_ns, stop_failed_ns)) {
			os::systime to;
			to.setreltime(1000000);
			cond_slavewait.timedwait(&Rgm_state->rgm_mutex, &to);

		}

		if (!stop_failed_ns.isEmpty()) {
			rgm_format_errmsg(&res,
			    RWM_QUIESCE_STOP_FAILED,
			    rglistp[i]->rgl_ccr->rg_name);
		} else {
			if (verbose_flag)
				rgm_format_successmsg(&res,
				    RWM_QUIESCE_RG,
				    rglistp[i]->rgl_ccr->rg_name);
		}
	}

	quiesce_clear_nameserver_tags(rglistp, is_fast_quiesce);
	goto quiesce_rgs_end; //lint !e801


quiesce_rgs_end:

	free(rglistp);
	free(rlistp);
	//
	// If we had a warning, append it to the error message (as
	// long as we don't have a legitimate error).
	//

	if (warning_res.err_msg && res.err_code == SCHA_ERR_NOERR) {
		rgm_sprintf(&res, "%s", warning_res.err_msg);
	}
	free(warning_res.err_msg);

	if (res.err_code == SCHA_ERR_NOERR)
		res.err_code = errors.err_code;
	if (errors.err_msg) {
		rgm_sprintf(&res, "%s", errors.err_msg);
		free(errors.err_msg);
	}

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	scq_res = retval;
	rgm_unlock_state();

	ucmm_print("RGM", NOGET("exiting idl_quiesce_rgs()\n"));
}

static void
rptrs_to_strseq(rlist_p_t *rlistp, rgm::idl_string_seq_t &rsnames)
{

	uint_t i = 0;
	rlist_p_t *p;

	if (rlistp == NULL) {
		rsnames.length(0);
	} else {
		// compute the length of sequence
		i = 0;
		p = rlistp;
		while (*p) {
			i++;
			p++;
		}
		// set the length of the sequence
		rsnames.length(i);
		// fill in the sequence
		i = 0;
		p = rlistp;
		while (*p) {
			rsnames[i] = new_str((*p)->rl_ccrdata->r_name);
			i++;
			p++;
		}
	}
}

static void
kill_methods(rglist_p_t rg_ptr, rgm::idl_string_seq_t &rsnames,
    scha_errmsg_t *warning_res)
{
	ASSERT(rg_ptr != NULL);

	rgm::rgm_comm_var prgm_serv;
	Environment e;

	// This loop is ok, because we want to call the method on
	// the physical nodes
	sol::nodeid_t i = 0;
	LogicalNodeset ns(Rgm_state->current_membership);
	while ((i = ns.nextPhysNodeSet(i + 1)) != 0) {
		//
		// We can optimise the number of idl calls by doing additonal
		// checks if the RG is running on the particular node before
		// we make the idl_kill() call. this optimisation has not been
		// implemented currently and can be done later.
		//
		prgm_serv = rgm_comm_getref(i);
		if (CORBA::is_nil(prgm_serv)) {
			continue;
		}

		prgm_serv->idl_kill_methods(rg_ptr->rgl_ccr->rg_name,
		    rsnames, e);
		if ((except_to_scha_err(e)).err_code == SCHA_ERR_NOERR) {
			// equivalent to except_to_errcode(e) == RGMIDL_OK)
			continue;
		}
		ucmm_print("RGM", NOGET("couldn't communicate successfully"
		    "with slave node for idl_kill_methods.scha_err code:<%d>"),
		    (except_to_scha_err(e)).err_code);
	}
}



//
// claims the RGM lock. check the nameserver to make sure RG is still being
// quiesced. checks the RG's inflight methods list to see if that res is
// currently running a method on this node. If so, calls RPC fe_kill to force
// immediate kill of the method by signal. releases state lock and returns
// If error happens, it is ignored. The quiesce can proceed anyway without
// the fast kill. if better error reporting is required, we can introduce a
// result variable to return the type of error that happened. Right now only
// idl call errors can be reported through Environment variable.
//
// Kills methods running in all zones on the local node.
//
void
rgm_comm_impl::idl_kill_methods(const char *rgname,
    const rgm::idl_string_seq_t &rsnames, Environment &e)
{
	uint_t i, numresources;
	rglist_p_t rg_ptr;

	slave_lock_state();
	if (!is_quiesce_running(rgname)) {
		goto idl_kill_methods_end; //lint !e801

	}

	rg_ptr = rgname_to_rg(rgname);

	// will this case occur if is_quiesce_running is true??
	// if not then replace it with ASSERT or PANIC !!?? not implemented
	if (rg_ptr == NULL) {
		goto idl_kill_methods_end; //lint !e801

	}

	numresources = rsnames.length();

	if (numresources == 0) {
		quiesce_methods(rg_ptr, NULL);
	} else {
		for (i = 0; i < numresources; i++) {
			quiesce_methods(rg_ptr, rsnames[i]);
		}
	}

idl_kill_methods_end:
	slave_unlock_state();
	return;

}


//
// looks for methods in the rg_ptr inflight method list
// and kills them using fe_kill rpc. If rname is not null, does
// kill for only methods belonging to that resource.
//
// Kills methods in all zones on the local node.
//
static void
quiesce_methods(rglist_p_t rg_ptr, const char *rname)
{
	methodinvoc_t *mi;
	method_t mt;
	char *resource_str;
	char *str = NULL;

	fe_cmd	cmd = {0};
	fe_run_result	result = {FE_OKAY, 0, 0, SEC_OK, NULL};

	for (mi = rg_ptr->rgl_methods_in_flight; mi != NULL; mi = mi->mi_next) {
		//
		// Extract method id (int string) and resource name.
		// Ignore zone name -- we kill in all the zones.
		// Copy the mi_tag since parse_fed_tag modifies the string.
		//
		str = strdup(mi->mi_tag);
		parse_fed_tag(str, NULL, NULL, &resource_str, NULL, &mt);

		if (rname != NULL) {
			if (strcmp(rname, resource_str) != 0) {
				free(str);
				continue;
			}
		}
		//
		// we don't kill boot/init/fini methods because
		// they don't have a failure state to go to.
		//
		if (mt == METH_INIT ||
		    mt == METH_BOOT ||
		    mt == METH_FINI ||
		    mt == METH_UPDATE) {
			free(str);
			continue;
		}

		if (fe_kill(mi->mi_tag, &cmd, &result) != 0) {
			ucmm_print("quiesce_methods",
			    NOGET("IPC fe_kill:<%s> failed\n"), mi->mi_tag);
		} else if (result.type != FE_OKAY) {
			ucmm_print("quiesce_methods",
			    NOGET("fe_kill<%s> err:type <%d>,sys_errno <%d>"),
			    mi->mi_tag, result.type, result.sys_errno);
		}

		xdr_free((xdrproc_t)xdr_fe_run_result, (char *)&result);
		free(str);
	}
}


//
// takes idl_string_seq "rnames" and generates res pointers and places it in
// a buffer that it allocates for this purpose and writes the buffer address
// at a location pointed to by the rlistpp. It ignores invalid and duplicate
// resource names in this list, and sets numresources (passed by reference)
// The res name is only searched for in the rglist specified. The function
// can be later upgraded to accept null in rglistp, in which case it will
// search in all the rgs configured on the system.
// returns error in scha_errmsg_t, and warning error messages in warning_res.
// warning is only produced for invalid resources not for duplicate ones.
//

static scha_errmsg_t
rnames_to_rlist(const rgm::idl_string_seq_t rnames, rlist_p_t **rlistpp,
    uint_t *numresourcesp, rglist_p_t *rglistp, uint_t numrgs,
    scha_errmsg_t *warning_res)
{
	rlist_p_t *rlistp, r_ptr;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_err_t	err_code;

	uint_t i, j, k;
	uint_t numresources = *numresourcesp;

	rlistp = (rlist_p_t *)calloc_nocheck((size_t)(numresources + 1),
	    sizeof (rlist_p_t));
	if (rlistp == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}
	*rlistpp = rlistp;

	for (i = 0, k = 0; i < numresources; i++) {
		err_code = SCHA_ERR_NOERR;
		r_ptr = NULL;
		for (j = 0; j < numrgs; j++) {
			// search all rgs specified by rglist
			if ((r_ptr = rname_to_r(rglistp[j],
			    rnames[i])) != NULL)
				break;
		}

		if (r_ptr == NULL) {
			ucmm_print("RGM", NOGET(
			    "scswitch_quiesce_rgs: Rs <%s> does not "
			    "exist in specified RGs"),
			    (const char *)rnames[i]);
			err_code = SCHA_ERR_RSRC;
		} else {
			// check for duplicate res
			for (j = 0; j < k; j++) {
				if (rlistp[j] == r_ptr) {
					ucmm_print("RGM",
					    NOGET("scswitch: duplicate Resource"
					    "<%s>\n"),
					    r_ptr->rl_ccrdata->r_name);
					break;
				}
			}

			if (j >= k) {	// exited of loop w/o error
				rlistp[k++] = r_ptr;
			}
		}

		if (err_code != SCHA_ERR_NOERR) {

			if (err_code == SCHA_ERR_RSRC) {
				rgm_format_errmsg(warning_res, RWM_INVALID_RS,
				    (const char *)rnames[i]);
			}
			if ((*warning_res).err_code != SCHA_ERR_NOERR) {
				res.err_code = (*warning_res).err_code;
				return (res);
			}
		}
	}

	*numresourcesp = k;
	return (res);

}

//
// takes a idl_string_seq "rgnames" and generates rg pointer list, stores
// it in a buffer that it creates for the purpose and stores the address
// of the buffer at location pointed to by "rglistpp"
// In case of invalid, duplicate rgs, etc function stores error in errors
// structure and continued to build list with valids rgs.
// NOTE, unmanaged RGs are also returned in rglist.
// Callee can proceed as long as no error is returned from this function,
// irrespective of the content of errors structure.
// The memory allocated must be freed by the caller function.
//

static scha_err_t
rgnames_to_rglist(const rgm::idl_string_seq_t rgnames, rglist_p_t **rglistpp,
    uint_t *numrgsp, uint_t nodeid, const char *zonename, scha_errmsg_t *errors)
{
	rglist_p_t	*rglistp, rg_ptr;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	uint_t		i, j, numrgs;
	boolean_t	nodelist_contains, globalflag;

	//
	// Note that calloc initializes all elements to NULL;
	// the last element rglistp[numrgs] will remain NULL.
	//
	rglistp = (rglist_p_t *)calloc_nocheck((size_t)(rgnames.length() + 1),
	    sizeof (rglist_p_t));

	if (rglistp == NULL) {
		return (SCHA_ERR_NOMEM);
	}
	*rglistpp = rglistp;

	// Check rgname args and build pointer list rglistp

	ngzone_check(zonename, globalflag);
	for (i = 0, numrgs = 0; i < rgnames.length(); i++) {
		// Check that the RG name exists

		rg_ptr = rgname_to_rg(rgnames[i]);
		if (rg_ptr == NULL) {
			ucmm_print("RGM", NOGET(
			    "scswitch_primaries: RG <%s> does not "
			    "exist"),
			    (const char *)rgnames[i]);
			smart_overlay_err(*errors, SCHA_ERR_RG);
			rgm_format_errmsg(&res, REM_INVALID_RG,
			    (const char *)rgnames[i]);
			continue;

		}
		if (!globalflag) {
			nodelist_check(rg_ptr, nodeid, zonename,
			    nodelist_contains);
			if (!nodelist_contains) {
				smart_overlay_err(*errors, SCHA_ERR_ACCESS);
				rgm_format_errmsg(&res,
				    REM_RG_NODELIST_NGZONE,
				    rg_ptr->rgl_ccr->rg_name, zonename);
				continue;
			}
		}


		// check for duplicate entries
		for (j = 0; j < numrgs; j++) {
			if (rg_ptr == rglistp[j]) {
				break;
			}
		}
		if (j < numrgs) {
			ucmm_print("RGM",
			    NOGET("scswitch: duplicate RG name "
			    "<%s>\n"),
			    (const char *)rg_ptr->rgl_ccr->rg_name);
			smart_overlay_err(*errors, SCHA_ERR_INVAL);
			rgm_format_errmsg(&res, RWM_DUPLICATE_RG_OPERAND,
			    (const char *)rg_ptr->rgl_ccr->rg_name);
			continue;
		}

		rglistp[numrgs++] = rg_ptr;
	}
	if (res.err_msg) {
		rgm_sprintf(errors, "%s", res.err_msg);
		free(res.err_msg);
	}
	*numrgsp = numrgs;
	return (SCHA_ERR_NOERR);
}


/*
 * allocates a buffer and fills it with rg ptrs and stores
 * the buffer address at location pointed to by rglistpp
 * also sets numrgsp with numrgs
 * to list only unmanaged/managed rgs/ all rgs; flag can be used
 */

static scha_err_t
get_rglist(rglist_p_t **rglistpp, uint_t *numrgsp, uint_t nodeid,
    const char *zonename, scha_errmsg_t *warning_res) {

	scha_err_t	err = SCHA_ERR_NOERR;
	uint_t		numrgs;
	rglist_p_t	rg_ptr;
	rglist_p_t	*rglistp;
	boolean_t	globalflag;
	boolean_t	nodelist_contains;

	// should have been cleared after use, if it had to reach here.
	ASSERT(warning_res->err_code == SCHA_ERR_NOERR);

	numrgs = Rgm_state->rgm_total_rgs;
	//
	// extra space allocated.
	// hopefully performance better than using realloc() without
	// significant memory overuse (since only pointers are stored)
	// If we want, we can call realloc to strip the size once the
	// list is formed.
	//
	rglistp = (rglist_p_t *)calloc_nocheck((size_t)(numrgs + 1),
	    sizeof (rglist_p_t));
	if (rglistp == NULL) {
		return (SCHA_ERR_NOMEM);
	}
	ngzone_check(zonename, globalflag);
	if (!globalflag) {
		rgm_format_errmsg(warning_res, RWM_RG_NGZONE, zonename);
		if (warning_res->err_code != SCHA_ERR_NOERR) {
			err = warning_res->err_code;
			warning_res->err_code = SCHA_ERR_NOERR;
			return (err);
		}
	}

	for (rg_ptr = Rgm_state->rgm_rglist, numrgs = 0; rg_ptr != NULL;
	    rg_ptr = rg_ptr->rgl_next) {
		if (!globalflag) {
			nodelist_check(rg_ptr, nodeid, zonename,
			    nodelist_contains);
		}
		if (globalflag || nodelist_contains) {
			rglistp[numrgs++] = rg_ptr;
		}
	}
	*rglistpp = rglistp;
	*numrgsp = numrgs;
	return (err);
}

//
// nodework structure lists RGs to be brought online/offline
//  on each node.
//
struct onofflists {
	namelist_t *onlist;
	namelist_t *offlist;

	// default ctor for use in map
	inline onofflists() { onlist = offlist = NULL; }
};

//
// do_rg_switch
// Run by the President.  Called as a subroutine by idl_scswitch_primaries()
// and scha_control_action().  The rgm state mutex must already be held
// by the caller.  Caller has verified the validity of the arguments.
// Brings the specified RG(s) online on the specified node(s) and
// offline on all other nodes.
// Determines which nodes (including itself) need to take which
// RGs online or offline.  Sends out orders to nodes by calling
// rgm_change_mastery().
// Returns SCHA_ERR_NOERR if everything went as planned.
//
// The rglistp argument is a null-terminated array of pointers to rglist_t
// structures.  Each pointer designates one RG; the rgl_next field of the
// rglist struct is ignored. The caller is responsible to malloc and free
// the structure.
//
// The caller must have already set the rgl_switching flag of each RG to
// a value other than SW_NONE (i.e., to SW_IN_PROGRESS or SW_IN_FAILBACK).
// This function will reset rgl_switching on each RG to SW_NONE as the
// switch is completed.
//
// online_action_flag is set when idl_scswitch_primaries is called with
// RGACTION_ONLINE.
//
// offline_action_flag is set when idl_scswitch_primaries is called with
// RGACTION_OFFLINE.
//
// failed_op_list is a namelist which is used to add the
// list of failed operands.
//
// The algorithm:
// 1. Iterate through the RGs in rglistp.  For each RG:
// 1a. If the RG is in failed_op_list skip it.
// 1b. If the RG is in failback, and the rgl_failback_interrupt flag has
//	been set to 1, it means that a new failback thread is preempting the
//	current one.  We set rgl_failback_interrupt to 2 and (noting that
//	a failback is executed on just one RG) return early.
//	The error code SCHA_ERR_MEMBERCHG is returned for this case only.
// 2. If RG is "busted", discontinue processing on it by removing it from
//	rglistp[].  Go to step 6.
// 3. Call scswitch_examine_rg() to compare current mastery of the RG to the
//	target mastery.  onnodes is set to the set of nodes on which RG
//	needs to be started; offnodes is the set of nodes on which RG needs
//	to be stopped.
// 4. If a STOP method failed on the RG, discontinue processing on it by
//	removing it from rglistp[].  Go to step 6.
// 5. If RG has attained the target mastery, discontinue processing on it by
//	removing it from rglistp[].
// 5.5. If the RG can no longer go online on the target nodeset because of
//    affinities violations, we discontinue processing on it by removing it
//    from rglistp[].
// 6. If rglistp is empty, return.
// 7. If we have not processed all RGs in rglistp, go back to step 2 to
//	process the next RG in rglistp.
// 8. For each node N in onnodes:
//	8.1. If the current number of masters of RG is less than max_primaries,
//		add RG to the "online" work list for node N; else go to step 8.
// 9. If the RG has 1 current master, and max_primaries >1, and RG is
//	pending_online on one or more nodes, go to step 10 [to avoid
//	taking such an RG completely offline from all nodes].
// 10. For each node N in offnodes:
//	10.1. Add RG to the "offline" work list for node N.
// 11. For each node that has a non-empty worklist, call change_mastery()
//	passing the "online" and "offline" worklists computed in steps 7 and 9.
// 12. Do a cond_wait() on the cond_slavewait condition variable.
// 13. If the rglistp list is not empty, go to step 1.
//
// Errors are returned through the res parameter.
//
void
do_rg_switch(
	scha_errmsg_t	*res,		// "out" parameter for errors
	rglist_p_t	*rglistp,	// null-term'ed array of rglist_t ptrs
	LogicalNodeset	&mnodeset,	// nodeset of intended new masters
	LogicalNodeset	&future_ns,	// nodeset of extra future intended
					// new masters
	boolean_t 	offline_action_flg,
	boolean_t 	online_action_flg,
	namelist_t	*failed_op_list)
{
	scha_errmsg_t	temp_res = {SCHA_ERR_NOERR, NULL};
	scha_err_t	busted_err = SCHA_ERR_NOERR;
	scha_err_t	start_err = SCHA_ERR_NOERR;
	scha_err_t	aff_err	= SCHA_ERR_NOERR;

	int		i, numrgs, nrgs;
	// args for scswitch_examine_rg() call:
	boolean_t	firsttime;	// haven't called change_mastery yet?
	LogicalNodeset	onnodes;
				// nodes on which RG needs to be started
	LogicalNodeset	offnodes;
				// nodes on which RG needs to be stopped
	int		numon = 0;	// # of nodes ONLINE
	int		numpendon = 0;	// # of nodes PENDING_ONLINE
	int		rgmasters = 0;	// # of nodes not OFFLINE
	boolean_t	alldone;	// scswitch all done?
	sc_syslog_msg_handle_t handle;
	LogicalNodeset emptyNodeset;

	//
	// No need to initialize nodework elements. The first access
	// of any lni_t index will create the default element.
	//
	std::map<rgm::lni_t, onofflists> nodework;

	ucmm_print("RGM", NOGET("in do_rg_switch()\n"));

	// Caller must pass us a valid array of RG pointers
	CL_PANIC(rglistp != NULL);

	//
	// Count number of RGs and set each RG's planned_on nodeset to
	// the nodes on which we plan to bring the RG online.  When each
	// RG goes OFFLINE on a node as a result of this switch,
	// process_needy_rg will call run_rebalance_on_affinitents to run
	// rebalance on strong negative affinitents of the RG.  That routine
	// will tell rebalance to ignore the nodes in rgl_planned_on_nodes
	// as potential primaries (because the strong negative affinitee is
	// planning to come online there).  If there is a failure on this
	// RG, such that it does not come online on all planned nodes,
	// we clear the planned_on_nodes nodeset and call
	// run_rebalance_on_affinitents directly, to give them a chance to
	// come online on the nodes that were precluded earlier.
	//
	// Also add in the future_ns of future intended new masters.
	//
	numrgs = 0;
	//
	// nrgs counts the total number of RG operands (including those
	// in the failed_op_list)
	//
	nrgs = 0;
	while (rglistp[nrgs] != NULL) {
		// skip failed Rgs
		if (namelist_exists(failed_op_list,
		    rglistp[nrgs]->rgl_ccr->rg_name)) {
			nrgs++;
			continue;
		}
		rglistp[nrgs]->rgl_planned_on_nodes = mnodeset | future_ns;
		numrgs++;
		nrgs++;
	}

	//
	// Caller has checked that RGs are not UNMANAGED; that there
	// are no duplicates in the list; that the request does not cause
	// any RG to exceed its Maximum_primaries setting; and that
	// all elements of mnodeset are potential primaries of the RG.
	// Caller has checked that each RG is in an "end-ish" state,
	// which permits it to be switched.
	// Caller has set the rgl_switching flag of each RG to a value
	// (e.g., SW_IN_PROGRESS) indicating that the RG is busy.
	//

	//
	// firsttime tells scswitch_examine_rg that we haven't called
	// change_mastery() yet, so it's OK for RG to be in ERROR_STOP_FAILED
	// (provided that we are only switching it offline and not online).
	//
	firsttime = B_TRUE;

	//
	// Carry out the switch by issuing orders to slave nodes.
	// Wait for slave actions to complete by sleeping on cond_slavewait
	// condition variable.  [See header comment above for description
	// of the algorithm].
	//
	while (numrgs > 0) {	// rglist is not empty

		//
		// Examine the dynamic state of each RG in rglistp, on
		// every node, to determine offlining and onlining
		// actions.
		//
		for (i = 0; i < nrgs; i++) {
			scha_err_t	busted = SCHA_ERR_NOERR;
			// skip failed Rgs
			if (namelist_exists(failed_op_list,
			    rglistp[i]->rgl_ccr->rg_name))
				continue;

			if (rglistp[i]->rgl_switching == SW_IN_FAILBACK &&
			    rglistp[i]->rgl_failback_interrupt == 1) {
				//
				// This failback has been interrupted by a
				// new failback thread.  We return early.
				//
				rglistp[i]->rgl_failback_interrupt = 2;
				rglistp[i]->rgl_planned_on_nodes.reset();
				rglist_remove_rg(rglistp, i);
				// failback operates on just one RG
				ASSERT(--numrgs <= 0);
				// free any nodework lists left
				/* CSTYLED */
				for (std::map<rgm::lni_t,onofflists>::iterator
				    it = nodework.begin();
				    it != nodework.end(); ++it) {
					rgm_free_nlist(it->second.onlist);
					rgm_free_nlist(it->second.offlist);
				}
				res->err_code = SCHA_ERR_MEMBERCHG;
				cond_slavewait.broadcast();
				return;
			}

			//
			// Discontinue processing this RG if it is "busted".
			// This means that either a planned new master of the
			// RG died while the switch was in progress, or
			// the RG failed to start or stop on a potential master
			// where it was being switched.
			//
			// Either the wake_president() or rgm_reconfig() thread
			// will call rebalance() after busting the RG, so
			// we don't have to.
			//
			busted = map_busted_err(rglistp[i]->rgl_switching);
			if (busted != SCHA_ERR_NOERR) {
				busted_err = busted;
				rglistp[i]->rgl_planned_on_nodes.reset();
				// Append the appropriate error message
				res->err_code = SCHA_ERR_NOERR;
				append_busted_msg(res, rglistp[i], busted_err);
				if (res->err_code != SCHA_ERR_NOERR) {
					// NOMEM or INTERNAL error occurred
					goto rg_switch_end; //lint !e801
				}
				if (rglistp[i]->rgl_switching !=
				    SW_EVACUATING) {
					rglistp[i]->rgl_switching = SW_NONE;

					//
					// Run rebalance on all RGs
					// that have strong negative
					// affinities for it,
					// as they may be allowed to come
					// online because this RG went offline
					// somewhere, and may have
					// been prevented from doing so
					// by the planned_on_nodes.
					// Call with cleanup_stale_affs set
					// to false.
					//
					run_rebalance_on_affinitents(
					    rglistp[i], B_FALSE, B_FALSE);
				}

				// Note, we will remove this entry from
				// rglistp[], below.
			} else {
				//
				// Process this RG in scswitch_examine_rg
				// subroutine.  This call sets
				// onnodes, offnodes, rgmasters, and
				// alldone.  (See scswitch_examine_rg()
				// function header below for details).
				// We pass down res so that the
				// subroutine can append messages to (rather
				// than overwrite) any existing messages.
				//
				scswitch_examine_rg(&temp_res, rglistp[i],
				    rglistp[i]->rgl_ccr->rg_name, firsttime,
				    mnodeset, onnodes, offnodes, &numon,
				    &numpendon, &rgmasters, &alldone,
				    offline_action_flg, online_action_flg);

				//
				// If stop_failed error condition is returned,
				// the RG has encountered (or remains in)
				// a STOP_FAILED condition.
				// res already contains the error message.
				// Stop processing the RG, because the RG
				// needs human intervention.
				//
				if (temp_res.err_code == SCHA_ERR_STOPFAILED) {
					ucmm_print("RGM", NOGET(
					    "RG <%s> is *_STOP_FAILED\n"),
					    rglistp[i]->rgl_ccr->rg_name);
					rglistp[i]->rgl_switching = SW_NONE;
					rglistp[i]->
					    rgl_planned_on_nodes.reset();

					//
					// Run rebalance on all RGs that have
					// strong negative affinities for it,
					// as they may be allowed to come
					// online due to the mastery changes
					// of this switch, and may have been
					// prevented from doing so because
					// of the planned_on_nodes.
					// Call with cleanup_stale_affs set to
					// false.
					//
					run_rebalance_on_affinitents(
					    rglistp[i], B_FALSE, B_FALSE);

					busted_err = SCHA_ERR_STOPFAILED;
					//
					// Reset the error code for now.
					// We will set it back to stopfailed
					// below, if no more serious error
					// occurs.
					//

					temp_res.err_code = SCHA_ERR_NOERR;
				}

				// If examine_rg returned another error, abort
				if (temp_res.err_code != SCHA_ERR_NOERR) {
					if (temp_res.err_msg) {
						rgm_sprintf(res, "%s",
						    temp_res.err_msg);
						free(temp_res.err_msg);
						temp_res.err_msg = NULL;
						if (res->err_code ==
						    SCHA_ERR_NOERR) {
							res->err_code =
							    temp_res.err_code;
						}
					}

					goto rg_switch_end; //lint !e801
				}

				ucmm_print("RGM", NOGET(
				    "after scswitch_examine_rg(), "
				    "alldone = <%d>, masters = <%d>\n"),
				    alldone, rgmasters);

				if ((rglistp[i]->rgl_switching ==
				    SW_IN_PROGRESS ||
				    rglistp[i]->rgl_switching ==
				    SW_IN_FAILBACK) && alldone) {
					//
					// This RG has attained the desired
					// mastery.  Before removing it from
					// the work list, check its resources
					// (on the nodes where the RG is
					// ONLINE) and append an error message
					// for any resource that ended up in
					// START_FAILED or MONITOR_FAILED state.
					//
					// With its third argument false, the
					// only error code returned by
					// check_starterrs (besides NOERR)
					// is STARTFAILED.
					//
					check_starterrs(&temp_res, rglistp[i],
					    B_FALSE);
					if (temp_res.err_code ==
					    SCHA_ERR_STARTFAILED) {
						start_err =
						    SCHA_ERR_STARTFAILED;
						//
						// Reset the error code for now.
						// We will set it back to
						// startfailed below, if no
						// more serious error occurs.
						//
						temp_res.err_code =
						    SCHA_ERR_NOERR;
					}

					//
					// Append any warning message set by
					// check_starters.
					//
					(void) transfer_message(res, &temp_res);

					//
					// Clear rgl_switching so RG will be
					// removed from the work list.
					// Also clear rgl_planned_on_nodes.
					//
					// Note, in the case of an RG restart,
					// this function is called for only
					// one RG at a time.  The caller will
					// reset rgl_switching back to
					// SW_IN_PROGRESS before re-calling
					// this function to bring the RG back
					// online.
					//
					rglistp[i]->rgl_switching = SW_NONE;
					rglistp[i]->
					    rgl_planned_on_nodes.reset();
				}
				//
				// Verify that our affinities are currently
				// valid on all potential new masters.
				// rg_switch_check_affinities will "postpone"
				// onlining on particular nodes by removing
				// the node from onnodes.  It does this if
				// the affinity is violated, but will be
				// fulfilled later by another switching
				// RG (currently in rglistp).
				//
				// rg_switch_check_affinities returns
				// no error as long as there is at least
				// one node that should eventually be
				// available in the onnodes (because this RG
				// has a strong positive affinity for another
				// RG in rglistp that is still switching).
				// If all onnodes are hopeless,
				// rg_switch_check_affinities returns an error,
				// in which case we stop processing this RG,
				// and call rebalance on it.
				//
				temp_res = rg_switch_check_affinities(
				    rglistp[i], onnodes, rglistp, nrgs);
				if (temp_res.err_code != SCHA_ERR_NOERR) {
					//
					// If the error is low memory,
					// we have to bail out of this
					// routine, even though we're not
					// done.
					//
					if (temp_res.err_code ==
					    SCHA_ERR_NOMEM) {
						//
						// Don't try to copy any
						// error message into res,
						// because we're already
						// low on memory.  Just
						// assign the error code.
						//
						res->err_code = SCHA_ERR_NOMEM;
						free(temp_res.err_msg);
						goto rg_switch_end; //lint !e801
					}
					//
					// Otherwise, we don't want to bail
					// out of this routine completely.  We
					// just need to set an error
					// message, remove this
					// RG from the working list, and
					// run rebalance on the RG.
					// We need to run rebalance here
					// because we don't bust the operation
					// anywhere else, and because
					// process_needy_rg will not have
					// run rebalance on the RG when it went
					// OFFLINE (because the rgl_switching
					// flag was set).
					//
					// Setting the switching flag
					// to SW_NONE will cause the
					// RG to be removed from
					// the working list below.
					//
					// We do not set an error code,
					// because some RGs could
					// succeed in switching.
					//

					//
					// Save the error code.
					//
					aff_err = temp_res.err_code;

					rglistp[i]->rgl_switching = SW_NONE;
					rglistp[i]->
					    rgl_planned_on_nodes.reset();

					ucmm_print("RGM",
					    NOGET("do_rg_switch: "
					    "RG <%s> can't come online "
					    "because of affinities; "
					    "running rebalance.\n"),
					    rglistp[i]->rgl_ccr->rg_name);
					rebalance(rglistp[i], emptyNodeset,
					    B_FALSE, B_TRUE);

					//
					// Run rebalance on all RGs that have
					// strong negative affinities for it,
					// as they may be allowed to come
					// online due to the mastery changes
					// of this switch, and might have
					// been prevented from doing so
					// by the planned_on nodeset.
					// Call with cleanup_stale_affs set to
					// false.
					//
					run_rebalance_on_affinitents(
					    rglistp[i], B_FALSE, B_FALSE);
				}

				(void) transfer_message(res, &temp_res);
			}

			// reset the temp_res error code
			temp_res.err_code = SCHA_ERR_NOERR;
			//
			// If we are done with this RG, remove it from rglistp
			// and go on to the next RG.
			//
			if (rglistp[i]->rgl_switching == SW_NONE ||
			    rglistp[i]->rgl_switching == SW_EVACUATING) {
				ucmm_print("RGM",
				    NOGET("do_rg_switch: "
				    "finished processing RG <%s>\n"),
				    rglistp[i]->rgl_ccr->rg_name);
				rglistp[i]->rgl_planned_on_nodes.reset();
				rglist_remove_rg(rglistp, i);
				if (--numrgs <= 0) {
					goto rg_switch_end; //lint !e801
				}
				i--;
				nrgs--;
				continue;
			}

			//
			// Note onnodes and/or offnodes might be empty while
			// RG is still pending-on or pending-off on some nodes
			// due to an earlier iteration of this loop.
			//

			//
			// add onlining actions to nodework lists
			//
			while (rgmasters <
			    rglistp[i]->rgl_ccr->rg_max_primaries &&
			    !onnodes.isEmpty()) {
				// Set lni to the first element of onnodes
				rgm::lni_t lni = onnodes.nextLniSet(1);

				//
				// Append RG to nodework onlist for lni.
				// Use a different result variable (res2) so
				// we don't overwrite existing messages.
				//
				temp_res = namelist_add_name(
				    &(nodework[lni].onlist),
				    rglistp[i]->rgl_ccr->rg_name);
				if (temp_res.err_code != SCHA_ERR_NOERR) {
					res->err_code = temp_res.err_code;
					goto rg_switch_end; //lint !e801
				}
				ucmm_print("RGM", NOGET(
				    "RG <%s> in ONLIST on <%d>\n"),
				    rglistp[i]->rgl_ccr->rg_name, lni);

				// remove element from onnodes
				onnodes.delLni(lni);

				rgmasters++;
			}

			//
			// add offlining actions to nodework lists
			//
			while (!offnodes.isEmpty()) {
				//
				// To maintain availability of RGs that
				// have maximum_primaries > 1, we postpone doing
				// an offline that would take the total number
				// of masters of an RG to zero as long as there
				// are still onlining actions pending for that
				// RG.  Note that the following if-statement
				// test must fail unless maximum_primaries
				// is >1.
				//
				if (numon == 1 && numpendon > 0) {
					break;
				}

				// Set lni to the first element of offnodes
				rgm::lni_t lni = offnodes.nextLniSet(1);

				//
				// Append RG to nodework offlist for lni.
				// Use a different result variable (res2) so
				// we don't overwrite existing messages.
				//
				temp_res = namelist_add_name(
				    &(nodework[lni].offlist),
				    rglistp[i]->rgl_ccr->rg_name);
				if (temp_res.err_code != SCHA_ERR_NOERR) {
					res->err_code = temp_res.err_code;
					goto rg_switch_end; //lint !e801
				}

				ucmm_print("RGM", NOGET(
				    "RG <%s> in OFFLIST on <%d>\n"),
				    rglistp[i]->rgl_ccr->rg_name, lni);

				// remove element from offnodes
				offnodes.delLni(lni);
			}
		}


		//
		// Iterate through all elements in the nodework map.
		// For elements that have a non-NULL onlist or offlist,
		// we call change_mastery.
		//
		/* CSTYLED */
		for (std::map<rgm::lni_t, onofflists>::iterator it =
		    nodework.begin(); it != nodework.end(); ++it) {

			//
			// Since we clear nodework below, it should not
			// contain any onlist==NULL/offlist==NULL elements.
			// However, just to be paranoid, we'll make sure of it.
			//
			if (it->second.onlist == NULL &&
			    it->second.offlist == NULL) {
				ucmm_print("RGM",
				    NOGET("do_rg_sw -- skip NULL/NULL"));
				continue;
			}

			//
			// Note, cluster membership should not have changed
			// since we computed nodework, since we are holding
			// the rgm state lock.  So, if nodework says there's
			// work on a node, we'll go ahead and dispatch it
			// without re-checking the lni for membership.
			//

			ucmm_print("do_rg_switch()",
			    NOGET("going to do change_mastery()\n"));

			//
			// We pass offlist and onlist to the slave node
			// all at once to facilitate the possible later
			// implementation of V1-compatible (once per node)
			// method callbacks.  However we actually make no
			// attempt to implement the V1 semantics at this time.
			//
			switch (rgm_change_mastery(it->first,
			    it->second.onlist, it->second.offlist)) {
			case RGMIDL_OK:
				break;

			case RGMIDL_NODE_DEATH:
			case RGMIDL_RGMD_DEATH:
				//
				// rgm_change_mastery call failed on this node.
				// Do nothing; rgm_reconfig() will "bust"
				// the affected RGs and will broadcast
				// cond_slavewait after updating cluster
				// membership.
				//
				ucmm_print("do_rg_switch",
				    NOGET("node %d died during the switch\n"),
				    it->first);
				break;

			case RGMIDL_NOREF:
			case RGMIDL_UNKNOWN:
			default:
				//
				// If call failed for some reason other
				// than node death, we might get in an
				// infinite cond_wait().  To avoid this, return
				// early from this function with an error.
				//
				LogicalNode *ln = lnManager->findObject(
				    it->first);
				CL_PANIC(ln != NULL);
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "scswitch: rgm_change_mastery() failed "
				    "with NOREF, UNKNOWN, or invalid error on "
				    "node %s", ln->getFullName());
				sc_syslog_msg_done(&handle);
				res->err_code = SCHA_ERR_ORB;
				rgm_format_errmsg(res, REM_INTERNAL);
				goto rg_switch_end; //lint !e801
			}

			// free nodework lists
			rgm_free_nlist(it->second.onlist);
			it->second.onlist = NULL;
			rgm_free_nlist(it->second.offlist);
			it->second.offlist = NULL;
		}

		nodework.clear();
		//
		// Now wait for work to complete.
		// Even if all nodes in the nodework list have died, we
		// have to go to the cond_wait() because some of our RG
		// operands might be in the process of going online or
		// offline on other nodes not in the current nodework.
		//
		// Eventually, we will wake up on the cond_broadcast when we
		// are notified that the dying zones are down, or sooner if
		// any RGs or resources are moved to offline state in the
		// dead node/zone by the slave node's state machine.
		//
		ucmm_print("do_rg_switch", NOGET("going to cond_wait()\n"));
		cond_slavewait.wait(&Rgm_state->rgm_mutex);

		//
		// Clear the state change queue, to make sure that we have
		// the latest state.  It's ok to unlock and relock the state
		// here (we just did the same thing in the cond_wait).  It will
		// just mean that we need to wait a little longer to get the
		// lock back again.
		//
		rgm_unlock_state();
		flush_state_change_queue();
		rgm_lock_state();

		// From now on, ERROR_STOP_FAILED will terminate
		// processing of the RG.
		firsttime = B_FALSE;
	}

rg_switch_end:

	// If we got here via a goto to rg_switch_end, we need to
	// clean-up rgl_switching, and send broadcast to release
	// any threads that are waiting for the condition variable.

	for (i = 0; rglistp[i] != NULL; i++) {

		// Skip failed RGs
		if (namelist_exists(failed_op_list,
		    rglistp[i]->rgl_ccr->rg_name))
			continue;

		//
		// Clear the planned_on_nodes from every RG that's left.
		//
		rglistp[i]->rgl_planned_on_nodes.reset();

		if (rglistp[i]->rgl_switching != SW_EVACUATING &&
		    rglistp[i]->rgl_switching != SW_NONE) {
			rglistp[i]->rgl_switching = SW_NONE;

			//
			// Run rebalance on all RGs that have
			// strong negative affinities for it,
			// as they may be allowed to come
			// online due to the mastery changes
			// of this switch, and might have been prevented
			// from doing so by the planned_on_nodeset.
			//
			// If we have a memory error, don't bother to
			// try to run rebalance on the affinitents.
			//
			if (res->err_code != SCHA_ERR_NOMEM) {
				// call with cleanup_stale_affs set to false.
				run_rebalance_on_affinitents(rglistp[i],
				    B_FALSE, B_FALSE);
			}
		}
	}
	//
	// we may optimise by restraining the broadcast only if numrgs > 0
	// but just to be safe, we do it unconditionally.
	//
	(void) cond_slavewait.broadcast();

	// free any nodework lists left, in case we exited the loop
	// early above
	/* CSTYLED */
	for (std::map<rgm::lni_t, onofflists>::iterator it =
	    nodework.begin(); it != nodework.end(); ++it) {
		rgm_free_nlist(it->second.onlist);
		it->second.onlist = NULL;
		rgm_free_nlist(it->second.offlist);
		it->second.offlist = NULL;
	}

	//
	// If multiple errors occurred, the stderr message will contain
	// the detailed messages, but we can only return one exit code.
	// We will try to return the exit code for the most severe error
	// that occurred:
	// If a fatal error (such as invalid arguments or NOMEM) occurred,
	// we will return the exit code for that.  Otherwise, if a "busted
	// switch" occurred, we will return the exit code for the last one
	// of those that we detected.
	// If neither of the above errors occurred but a "start failure"
	// occurred (i.e., the RG went online but a resource ended up in
	// START_FAILED state) then we will return an exit code for that.
	//
	// If none of the above errors occurred but a "monitor failure"
	// occurred (i.e., the RG and its resources went online but a
	// resource's monitor_start method failed) then we will exit 0
	// but will still write a warning message to stderr.
	//
	// xxxx Currently scswitch ignores the exit code (other than whether
	// it's zero or non-zero) but we will nonetheless try to return
	// the most meaningful code in case it is used in the future.
	//
	if (res->err_code == SCHA_ERR_NOERR) {
		if (busted_err != SCHA_ERR_NOERR) {
			res->err_code = busted_err;
		} else if (start_err != SCHA_ERR_NOERR) {
			res->err_code = start_err;
		} else if (aff_err != SCHA_ERR_NOERR) {
			res->err_code = aff_err;
		}
	}

	ucmm_print("RGM", NOGET("exiting do_rg_switch()\n"));
}

//
// switchback()
// This function implements the rg remastering logic.
// This makes use of is_failback_required as a helper function.
//

static scha_errmsg_t
switchback(const char *rgname, namelist_t *rg_names, bool verbose_flag)
{
	rglist_p_t	rgp = NULL;
	rglist_p_t	rglistp[2];
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	temp_res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	LogicalNodeset	*ns = NULL;
	LogicalNodeset	fromNodeset;
	LogicalNode	*lnNode;
	LogicalNodeset	emptyNodeset;
	char		*ucmm_buffer = NULL;
	rgm::lni_t	lni;
	nodeidlist_t	*nl;
	LogicalNodeset *nset = NULL;
	boolean_t boot_delay;

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");

	ucmm_print("switchback", NOGET("<%d> ==== switchback: rg = <%s>"),
	    os::thread::self(), rgname);
	rgp = rgname_to_rg(rgname);
	if (rgp == NULL) {
		goto finish; //lint !e801
	}
	// Iterate through RG nodelist
	for (nl = rgp->rgl_ccr->rg_nodelist; nl != NULL;
	    nl = nl->nl_next) {
		lni = nl->nl_lni;
		lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);

		switch (rgp->rgl_xstate[lni].rgx_state) {
		case rgm::RG_ONLINE:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
		case rgm::RG_ON_PENDING_R_RESTART:
			// Add this lni to from_nodeset
			// needed for event generation.
			fromNodeset.addLni(lni);
			continue;
		case rgm::RG_PENDING_OFF_STOP_FAILED:
		case rgm::RG_ERROR_STOP_FAILED:
		case rgm::RG_OFF_BOOTED:
		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_OFF_PENDING_METHODS:
		case rgm::RG_ON_PENDING_MON_DISABLED:
		case rgm::RG_OFF_PENDING_BOOT:
		case rgm::RG_PENDING_ONLINE:
		case rgm::RG_PENDING_OFFLINE:
		case rgm::RG_PENDING_OFF_START_FAILED:
		case rgm::RG_PENDING_ON_STARTED:
		case rgm::RG_OFFLINE:
		case rgm::RG_OFFLINE_START_FAILED:
			break;
		}
	}

	//
	// Check whether we still need to do a switchback.
	//

	// Get the nodeset for the rg.
	nset = get_logical_nodeset_from_nodelist(rgp->rgl_ccr->rg_nodelist);
	if (!is_failback_required(rgp, nset, &ns,
	    boot_delay, B_TRUE, &temp_res)) {
		// No need to do switchback.
		ucmm_print("switchback",
		    NOGET("<%d> ==== switchback: rg = <%s> -- "
		    "switchback no longer required"), os::thread::self(),
		    rgname);
		//
		// Mark the rgp flags to show that we are no longer doing
		// switchback.
		//
		rgp->rgl_switching = SW_NONE;
		if (temp_res.err_msg) {
			rgm_sprintf(&res, "%s", temp_res.err_msg);
			free(temp_res.err_msg);
			temp_res.err_msg = NULL;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
		}
		goto finish; //lint !e801
	}

	if (ns->display(ucmm_buffer) == 0) {

		ucmm_print("switchback",
		    NOGET("<%d> ==== switchback: ns = %s\n"),
		    os::thread::self(), ucmm_buffer);
		free(ucmm_buffer);
	}

	// Post an event for this RG
	(void) post_primaries_changing_event(NULL, rgp->rgl_ccr->rg_name,
	    CL_REASON_RG_REBALANCE, NULL, rgp->rgl_ccr->rg_desired_primaries,
	    fromNodeset, *ns);
	//
	// We call do_rg_switch() to bring the RG online on its new nodeset.
	// Notice that the rgl_switching flag will be reset in do_rg_switch()
	// and cond_broadcast() will be called whenever the RG has finished
	// switchback, thereby releasing any other threads that are in
	// cond_wait().
	//

	rglistp[0] = rgp;
	rglistp[1] = NULL;

	//
	// At this point we're calling do_rg_switch, and switchback is
	// conceptually over.  We still need to do the switching, but if
	// do_rg_switch releases the lock, the RGs will be in
	// non-terminal states somewhere, and there will be a switch in
	// progress.
	//
	// Set the offlining flag as well as onlining flag to false.
	// We dont need the last parameter failed_op_list as well.
	//
	do_rg_switch(&res, rglistp, *ns, emptyNodeset, B_FALSE, B_FALSE,
	    NULL);

	// check for errors.
	if (res.err_code != SCHA_ERR_NOERR) {
		ucmm_print("switchback",
		    NOGET("switchback attempt failed on resource group <%s> "
		    "with error <%s>"), rgp->rgl_ccr->rg_name,
		    rgm_error_msg(res.err_code));
		return (res);
	}

	ucmm_print("switchback",
	    NOGET("<%d> ==== Finish switchback of RG <%s>"),
	    os::thread::self(), rgp->rgl_ccr->rg_name);

	if (verbose_flag) {
		//
		// compute the nodenames for the nodes and print
		// error message.
		//
		lni = 0;
		while ((lni = ns->nextLniSet(lni + 1)) != 0) {
			lnNode = lnManager->findObject(lni);
			CL_PANIC(lnNode);
			rgm_format_successmsg(&res,
			    RWM_RG_REMASTERED, rgp->rgl_ccr->rg_name,
			    lnNode->getFullName());
		}
	}
finish:
	delete ns;
	sc_syslog_msg_done(&handle);
	return (res);
}

//
// scswitch_examine_rg
// This function is called from
// do_rg_switch() to check the state of the
// RG while the switch is in-progress and see if it has completed.
// For the RG referenced by rg_ptr and named by rg_name, iterate through
// all its potential masters and perform the following processing:
//
//   If firsttime is false and the RG is ERROR_STOP_FAILED on one or more
//   nodes, exit early with exit code SCHA_ERR_STOPFAILED.
//   If firsttime is true, the RG may be ERROR_STOP_FAILED on some nodes,
//   provided that the RG is only getting switched offline on those nodes.
//   The caller of do_rg_switch() is supposed to have checked this by calling
//   in_endish_rg_state; for robustness, we do a CL_PANIC assertion of it.
//   If the RG is PENDING_OFF_STOP_FAILED then we exit early with exit code
//   SCHA_ERR_STOPFAILED.
//
//   Set *p_offnodes to the set of nodes on which RG is to be switched offline,
//	i.e., nodes on which RG is currently ONLINE that are not in opnodes.
//	If firsttime is true, also include in p_offnodes the nodes on which
//	the RG is ERROR_STOP_FAILED since we will attempt to switch it offline
//	on these nodes.
//   Set *p_onnodes to the set of nodes on which RG is to be switched online,
//	i.e., nodes on which RG is currently OFFLINE that are in opnodes.
//   Set *p_numon to the number of nodes on which RG is currently ONLINE.
//   Set *p_numpendon to the number of nodes on which RG is currently
//	PENDING_ONLINE.
//   Set *p_rgmasters to the number of nodes on which the RG is in any state
//	other than OFFLINE.
//   If the RG is ONLINE on all nodes in opnodes and OFFLINE or
//	ERROR_STOP_FAILED on all
//	other nodes, set *p_alldone to B_TRUE; otherwise set it to B_FALSE.
//	However, if firsttime is true and the RG is ERROR_STOP_FAILED on
//	any node, set *p_alldone to B_FALSE so that the initial change_mastery
//	calls will be made to attempt to stop the RG on these nodes.
// Note that ON_PENDING_R_RESTART is considered equivalent to ONLINE for
// purposes of this routine.
//
// if the is_offlining flag is set then  *p_onnodes is set to NULL and
// *p_offnodes is set to nodeset intersection of opnodes (proposed
// operational nodes. Operation here is offlining) and the nodes on
// which the given RG is online.
//
// if the is_onlining flag is set then  *p_offnodes is set to NULL and
// *p_onnodes is set to nodeset intersection of opnodes (proposed
// operational nodes. Operation here is onlining) and the nodes on
// which the given RG is offline.
//
// If an error is encountered that is fatal to the scswitch, return the
// appropriate non-zero error code.
//
static void
scswitch_examine_rg(
	scha_errmsg_t		*res,		// existing errmsg
	const rglist_p_t	rg_ptr,		// an RG
	const char		*rg_name,	// name of the RG
	boolean_t		firsttime,	// haven't called change_mastery
	LogicalNodeset		&opnodes,	// proposed operational nodes
	LogicalNodeset		&p_onnodes,	// switch RG ONLINE on these
	LogicalNodeset		&p_offnodes,	// switch RG OFFLINE on these
	int			*p_numon,	// # of nodes ONLINE
	int			*p_numpendon,	// # of nodes PENDING_ONLINE
	int			*p_rgmasters,	// # of nodes not OFFLINE
	boolean_t		*p_alldone,
	boolean_t 		is_offlining,
	boolean_t 		is_onlining)
{
	nodeidlist_t	*nodelist;
	boolean_t	alldone = B_TRUE;
	LogicalNodeset	onnodes;
	LogicalNodeset	offnodes;
	int		rgmasters = 0;
	int		numon = 0;
	int		numpendon = 0;
	sc_syslog_msg_handle_t handle;

	// Iterate thru potential primaries of the RG
	for (nodelist = rg_ptr->rgl_ccr->rg_nodelist; nodelist != NULL;
	    nodelist = nodelist->nl_next) {
		rgm::lni_t lni = nodelist->nl_lni;
		LogicalNode *lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);
		//
		// We can skip examining physical nodes that are down; however,
		// we must examine the RG state on non-global zones even if
		// they are down, because it is possible for the RG to
		// be stopping on a dead zone if it contains resources
		// that have Global_zone=TRUE (thus which are executing
		// their methods in the global zone).
		//
		if (!lnNode->isPhysNodeUp()) {
			continue;
		}

		switch (rg_ptr->rgl_xstate[lni].rgx_state) {
		case rgm::RG_ONLINE:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
		case rgm::RG_ON_PENDING_R_RESTART:
			onnodes.addLni(lni);
			rgmasters++;
			numon++;
			break;

		case rgm::RG_OFFLINE_START_FAILED:
			// It is legal to attempt to switch the RG onto a node
			// where it failed to start.  change_mastery() should
			// change the RG state to PENDING_ONLINE on that node.
			// Note that if the RG transitions to
			// OFFLINE_START_FAILED on any node *during* the
			// switch, then wake_president will bust the switch.
		case rgm::RG_OFFLINE:
		case rgm::RG_OFF_BOOTED:
			offnodes.addLni(lni);
			break;

		case rgm::RG_OFF_PENDING_BOOT:
			//
			// Node has just joined cluster & is still running BOOT
			// methods.  The caller has already certified that
			// we are not attempting to switch RGs online on
			// this node.
			//

			// RG cannot be going online on this node.
			CL_PANIC(is_offlining ||
			    !opnodes.containLni(lni));
			offnodes.addLni(lni);
			break;

		case rgm::RG_ERROR_STOP_FAILED:
			//
			// The first time thru the outer loop in do_rg_switch,
			// we treat an ERROR_STOP_FAILED node as though it
			// were ONLINE (i.e. it is valid to stop the RG on
			// this node).  The call to change_mastery will
			// move the RG to PENDING_OFFLINE on such a node.
			// In subsequent iterations, ERROR_STOP_FAILED will
			// terminate further processing on this RG.
			// Actually, ERROR_STOP_FAILED should bust the
			// switch in the wake_president() thread, but we
			// effectively do the same thing here by returning
			// SCHA_ERR_STOPFAILED, in case some race condition
			// (or bug) puts us here first.
			//
			if (firsttime) {
				// RG cannot be going online on this node.
				CL_PANIC(is_offlining ||!opnodes.containLni(
				    lni));
				onnodes.addLni(lni);
				alldone = B_FALSE;
				rgmasters++;
				break;
			} else {
				// A STOP method failed while attempting this
				// switch; bail out.
				res->err_code = SCHA_ERR_STOPFAILED;
				// Append an error message to res for this RG
				rgm_format_errmsg(res, REM_RG_TO_STOPFAILED,
				    rg_name);
				goto finished; //lint !e801
			}

		case rgm::RG_PENDING_OFF_STOP_FAILED:
			// A STOP method failed while attempting this
			// switch; bail out.
			res->err_code = SCHA_ERR_STOPFAILED;
			// Append an error message to res for this RG
			rgm_format_errmsg(res, REM_RG_TO_STOPFAILED, rg_name);
			goto finished; //lint !e801

		case rgm::RG_PENDING_ONLINE:
		case rgm::RG_PENDING_ON_STARTED:
			numpendon++;
			/* FALL-THRU */
		case rgm::RG_PENDING_OFFLINE:
		case rgm::RG_PENDING_OFF_START_FAILED:
			// pending-online or pending-offline
			alldone = B_FALSE;
			rgmasters++;
			break;

		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_OFF_PENDING_METHODS:
		case rgm::RG_ON_PENDING_MON_DISABLED:
		default:	// error, illegal RG state
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// While attempting to execute an operator-requested
			// switch of the primaries of a resource group, the
			// rgmd has discovered the indicated resource group to
			// be in an invalid state. The switch action will
			// fail.
			// @user_action
			// This may indicate an internal error or bug in the
			// rgmd. Contact your authorized Sun service provider
			// for assistance in diagnosing and correcting the
			// problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "scswitch: internal error: bad state <%s> (<%d>) "
			    "for resource group <%s>",
			    pr_rgstate(rg_ptr->rgl_xstate[lni].rgx_state),
			    rg_ptr->rgl_xstate[lni].rgx_state, rg_name);
			sc_syslog_msg_done(&handle);
			res->err_code = SCHA_ERR_RGSTATE;
			rgm_format_errmsg(res, REM_INTERNAL);
			goto finished; //lint !e801
		}
	}

	//
	// If alldone is true when we reach this point, RG is in a terminal
	// state (either ONLINE or OFFLINE) on every node.
	// (If stop failed, we bailed out).
	// We check to see if we've actually achieved the target mastery.
	// If not, keep going.
	//
	if (is_offlining) {
		if (alldone) {
			LogicalNodeset desired_offnodes;
			desired_offnodes =
			    (offnodes & opnodes);
			if (!opnodes.isEqual(desired_offnodes))
				alldone = B_FALSE;
		}
	} else if (is_onlining) {
		if (alldone) {
			LogicalNodeset desired_offnodes;
			desired_offnodes =
			    (onnodes & opnodes);
			if (!opnodes.isEqual(desired_offnodes))
				alldone = B_FALSE;
		}
	} else {
		if (alldone) {
			if (!onnodes.isEqual(opnodes)) {
				alldone = B_FALSE;
			}
		}
	}
finished:
	if (is_onlining) {
		p_onnodes = offnodes & opnodes;
	} else if (is_offlining) {
		p_offnodes = onnodes & opnodes;
	} else {
		p_onnodes = offnodes & opnodes;
		p_offnodes = onnodes & ~opnodes;
	}

	*p_numon = numon;
	*p_numpendon = numpendon;
	*p_rgmasters = rgmasters;
	*p_alldone = alldone;
}

//
// check_rg_affinities_offlining
// ----------------------------
// For each node in the offlist, makes sure that all
// RGs related by strong positive affinities to rgp (the sp transitive closure)
// are offline or are in the list rg_names.  If any RG is STOP_FAILED, returns
// an error.  If any RG is switching or updating, returns an error.  If there
// are any RGs that are in the sp_transitive_closure, but are ok to switch,
// generates a warning message.
//
static scha_errmsg_t
check_rg_affinities_offlining(rglist_p_t rgp, LogicalNodeset &offlist,
    namelist_t *rg_names)
{
	rgm::lni_t lni;
	namelist_t *violations = NULL, *temp, *msg_violations = NULL;
	int violations_count = 0;
	scha_errmsg_t ret = {SCHA_ERR_NOERR, NULL};
	rglist_p_t rg_aff, rg_viol;
	scha_err_t err = SCHA_ERR_NOERR;

	ucmm_print("RGM", NOGET("check_rg_affinities_offlining: %s\n"),
	    rgp->rgl_ccr->rg_name);
	//
	// Iterate through each node on which we want to bring the RG
	// offline.
	//
	lni = 0;
	while ((lni = offlist.nextLniSet(lni + 1)) != 0) {
		//
		// Get the list of all the RGs that must be dragged off with
		// this one.
		//
		//
		// First, verify that all our strong, positive affinitents
		// are not in stop failed.
		//
		for (temp = rgp->rgl_affinitents.ap_strong_pos; temp != NULL;
		    temp = temp->nl_next) {
#if SOL_VERSION >= __s10
			if (is_enhanced_naming_format_used(temp->nl_name))  {
				if (!allow_inter_cluster()) {
					ucmm_print("RGM", NOGET(
					    "check_rg_affinities_offlining "
					    "Running at too low a version:RG "
					    "<%s> has affinity for RG <%s> "
					    "which is in not in local "
					    "cluster\n"),
					    rgp->rgl_ccr->rg_name,
					    rg_names->nl_name);
					continue;
				}
				if (get_remote_rg_status(temp->nl_name, lni,
					RG_ERROR_STOP_FAILED_STATUS)) {
					ucmm_print("RGM", NOGET(
					    "check_rg_affinities_offlining: "
					    "%s found "
					    "rg %s in STOP_FAILED\n"),
					    rgp->rgl_ccr->rg_name,
					    temp->nl_name);

					// one of them is in stop failed
					ret.err_code = SCHA_ERR_STOPFAILED;
					rgm_format_errmsg(&ret,
					    REM_RG_AFF_STOPFAILED,
					    temp->nl_name);
					return (ret);
				}
			}
#endif
			if (!is_enhanced_naming_format_used(temp->nl_name)) {
				rg_aff = rgname_to_rg(temp->nl_name);
				CL_PANIC(rg_aff != NULL);
				if (rg_aff->rgl_xstate[lni].rgx_state ==
				    rgm::RG_PENDING_OFF_STOP_FAILED ||
				    rg_aff->rgl_xstate[lni].rgx_state ==
				    rgm::RG_ERROR_STOP_FAILED) {

					ucmm_print("RGM", NOGET(
					    "check_rg_affinities_offlining: "
					    "%s found "
					    "rg %s in STOP_FAILED\n"),
					    rgp->rgl_ccr->rg_name,
					    rg_aff->rgl_ccr->rg_name);

					// one of them is in stop failed
					ret.err_code = SCHA_ERR_STOPFAILED;
					rgm_format_errmsg(&ret,
					    REM_RG_AFF_STOPFAILED,
					    rg_aff->rgl_ccr->rg_name);
					return (ret);
				}
			}
		}
		//
		// Now get the complete list
		//
		if (!compute_sp_transitive_closure(err, rgp, lni,
		    rgp->rgl_affinitents.ap_strong_pos, &violations,
		    violations_count, B_TRUE, B_TRUE)) {

			if (err != SCHA_ERR_NOERR) {
				ret.err_code = SCHA_ERR_NOMEM;
				return (ret);
			}

			// one of them is in stop failed
			ucmm_print("RGM", NOGET(
			    "check_rg_affinities_offlining: %s found "
			    "rg in STOP_FAILED\n"),
			    rgp->rgl_ccr->rg_name);
			ret.err_code = SCHA_ERR_STOPFAILED;
			rgm_format_errmsg(&ret, REM_RG_AFF_STOPFAILED,
			    rgp->rgl_ccr->rg_name);
			return (ret);
		}
		//
		// Iterate through the list, checking if any RGs are currently
		// switching or updating.
		//
		for (temp = violations; temp != NULL; temp = temp->nl_next) {
			//
			// Check if the RG that needs to be dragged off is
			// already part of the list of RGs that are being
			// switched.  If so, we can assume that it's being
			// switched off this node too (since the onnodes are
			// the same for all RGs being switched).
			//
			// If it's not there, we need to check if there
			// are any switches or updates in progress, in which
			// case we can't continue with this switch.
			//
			// Note we shouldn't have to check for SW_EVACUATING,
			// because we already determined that rgp is not
			// evacuating, therefore its SP affinitents cannot
			// be either.  However, it costs little to do so
			// and we can preclude any unwanted interactions.
			//
			if (namelist_exists(rg_names, temp->nl_name)) {
				continue;
			}

#if SOL_VERSION >= __s10
			if (is_enhanced_naming_format_used(temp->nl_name)) {
				if (!allow_inter_cluster()) {
					ucmm_print("RGM", NOGET(
					    "check_rg_affinities_offlining "
					    "Running at too low a version:RG "
					    "<%s> has affinity for RG <%s> "
					    "which is in not in local "
					    "cluster\n"),
					    rgp->rgl_ccr->rg_name,
					    rg_names->nl_name);
					continue;
				}
				if (get_remote_rg_status(temp->nl_name, NULL,
					RG_SWITCH_STATUS)) {
					ucmm_print("RGM", NOGET(
					    "check_rg_affinities_onlining: %s"
					    " found rg <%s> that is "
					    "switching.\n"),
					    rgp->rgl_ccr->rg_name,
					    temp->nl_name);

					ret.err_code = SCHA_ERR_RGRECONF;
					rgm_format_errmsg(&ret, REM_RG_AFF_BUSY,
					    rgp->rgl_ccr->rg_name,
					    temp->nl_name);
					goto check_offline_affs_end;
				}
				continue;
			}
#endif
			rg_viol = rgname_to_rg(temp->nl_name);
			CL_PANIC(rg_viol != NULL);
			if (rg_viol->rgl_switching == SW_IN_PROGRESS ||
			    rg_viol->rgl_switching == SW_IN_FAILBACK ||
			    rg_viol->rgl_switching == SW_UPDATING ||
			    rg_viol->rgl_switching == SW_EVACUATING) {

				ucmm_print("RGM", NOGET(
				    "check_rg_affinities_offlining: %s found "
				    "rg %s busy\n"),
				    rgp->rgl_ccr->rg_name,
				    rg_viol->rgl_ccr->rg_name);

				ret.err_code = SCHA_ERR_RGRECONF;
				rgm_format_errmsg(&ret, REM_RG_AFF_BUSY,
				    rgp->rgl_ccr->rg_name, temp->nl_name);
				goto check_offline_affs_end; //lint !e801
			}
			//
			// This RG is not busy, so we can carry out our switch
			// and drag this RG with us.  However, we must save
			// its name and node name to issue a warning message.
			//
			// With physical node affinities we might be taking
			// the affinitents off of different zones than rgp,
			// so we put the physical nodename in the warning
			// message.
			//
			char buff[2* MAXRGMOBJNAMELEN + 10];

			LogicalNode *lnNode = lnManager->findObject(lni);
			CL_PANIC(lnNode != NULL);
			(void) sprintf(buff, "%s on node %s", temp->nl_name,
			    use_logicalnode_affinities() ?
			    lnNode->getFullName() :
			    lnNode->getNodename());

			if (namelist_add_name(&msg_violations, buff).
			    err_code != SCHA_ERR_NOERR) {
				ret.err_code = SCHA_ERR_NOMEM;
				goto check_offline_affs_end; //lint !e801
			}
		}
		rgm_free_nlist(violations);
		violations = NULL;
	}

	//
	// If we had some violations, we need to generate a warning message.
	//
	if (msg_violations != NULL) {
		char *names = namelist_to_str(msg_violations);

		ucmm_print("RGM", NOGET(
		    "check_rg_affinities_offlining: %s found "
		    "rgs <%s> that will need to be dragged offline\n"),
		    rgp->rgl_ccr->rg_name, names);

		rgm_format_errmsg(&ret, RWM_AFFS_OFFLINE, names,
		    rgp->rgl_ccr->rg_name);
		free(names);
	}

check_offline_affs_end:
	rgm_free_nlist(msg_violations);
	rgm_free_nlist(violations);
	return (ret);
}

//
// check_rg_affinities_onlining
// ----------------------------
// For each node in the onlist, makes sure that rgp's strong positive and
// strong negative affinities are satisfied on the node (taking into account
// the list rg_names of rgs that will be going online simultaneously).
//
// Also verify that the strong negative affinitents of rgp, as well as RGs
// related by strong positive affinities to the strong negative affinitents
// (the sp transitive closure) are offline.  If any RG in the closure list is
// STOP_FAILED, returns an error.  If any RG is switching or updating,
// returns an error.  If any RG is in the rg_names list,  returns an error.
// If there are any RGs that are in the sp_transitive_closure, but are ok to
// switch, generates a warning message.
//
static scha_errmsg_t
check_rg_affinities_onlining(rglist_p_t rgp, LogicalNodeset &onlist,
    namelist_t *rg_names)
{
	namelist_t *violations = NULL, *temp, *msg_violations = NULL;
	int violations_count = 0;
	scha_errmsg_t ret = {SCHA_ERR_NOERR, NULL};
	rglist_p_t rg_viol;
	char *viol_str;
	scha_err_t err = SCHA_ERR_NOERR;

	ucmm_print("RGM", NOGET("check_rg_affinities_onlining: %s\n"),
	    rgp->rgl_ccr->rg_name);
	//
	// Iterate through each node on which we want to bring the RG
	// online.
	//
	rgm::lni_t lni = 0;
	while ((lni = onlist.nextLniSet(lni + 1)) != 0) {
		LogicalNode *ln = lnManager->findObject(lni);
		CL_PANIC(ln != NULL);

		//
		// Make sure all our strong positive affinities are satisfied
		// on the node.  The function will take into account the rgs
		// in rg_names that will go online concurrently with rgp.
		//
		if (!sp_affinities_satisfied(err, rgp, lni, rg_names,
		    &violations)) {
			if (err != SCHA_ERR_NOERR) {
				// low memory
				ret.err_code = SCHA_ERR_NOMEM;
				goto check_online_affs_end; //lint !e801
			}
			viol_str = namelist_to_str(violations);

			ucmm_print("RGM", NOGET(
			    "check_rg_affinities_onlining: %s found rgs "
			    "<%s> that are not online.\n"),
			    rgp->rgl_ccr->rg_name, viol_str);

			ret.err_code = SCHA_ERR_NOMASTER;
			rgm_format_errmsg(&ret, REM_RG_STRONG_POS,
			    rgp->rgl_ccr->rg_name, ln->getFullName(), viol_str);
			free(viol_str);
			goto check_online_affs_end; //lint !e801
		}

		rgm_free_nlist(violations);
		violations = NULL;

		//
		// Make sure our strong negative affinities are satisfied.
		// The function will take into account the rgs in rg_names
		// that will go online concurrently with rgp.
		//
		if (!sn_affinities_satisfied(err, rgp, lni, rg_names,
		    &violations)) {
			if (err != SCHA_ERR_NOERR) {
				// low memory
				ret.err_code = SCHA_ERR_NOMEM;
				goto check_online_affs_end; //lint !e801
			}

			viol_str = namelist_to_str(violations);

			ucmm_print("RGM", NOGET(
			    "check_rg_affinities_onlining: %s found rgs "
			    "<%s> that are online.\n"),
			    rgp->rgl_ccr->rg_name, viol_str);

			ret.err_code = SCHA_ERR_NOMASTER;
			rgm_format_errmsg(&ret, REM_RG_STRONG_NEG,
			    rgp->rgl_ccr->rg_name, ln->getFullName(), viol_str);
			free(viol_str);
			goto check_online_affs_end; //lint !e801
		}
		rgm_free_nlist(violations);
		violations = NULL;

		//
		// Check if we're kicking off any RGs.  If so, make sure
		// none of them are stop failed or busy.
		//
		if (!compute_sn_violations(err, rgp, lni, &violations,
		    violations_count, B_FALSE)) {

			if (err != SCHA_ERR_NOERR) {
				// low memory
				ret.err_code = SCHA_ERR_NOMEM;
				goto check_online_affs_end; //lint !e801
			}

			// one of them is in stop failed

			ucmm_print("RGM", NOGET(
			    "check_rg_affinities_onlining: %s found rg "
			    "in STOP_FAILED.\n"),
			    rgp->rgl_ccr->rg_name);

			ret.err_code = SCHA_ERR_STOPFAILED;
			rgm_format_errmsg(&ret, REM_RG_AFF_STOPFAILED,
			    rgp->rgl_ccr->rg_name);
			goto check_online_affs_end; //lint !e801
		}
		//
		// Now get the complete list
		//
		if (!compute_sp_transitive_closure(err, rgp, lni,
		    violations, &temp, violations_count, B_TRUE, B_FALSE)) {

			if (err != SCHA_ERR_NOERR) {
				// low memory
				ret.err_code = SCHA_ERR_NOMEM;
				goto check_online_affs_end; //lint !e801
			}

			// one of them is in stop failed

			ucmm_print("RGM", NOGET(
			    "check_rg_affinities_onlining: %s found rg "
			    "in STOP_FAILED.\n"),
			    rgp->rgl_ccr->rg_name);

			ret.err_code = SCHA_ERR_STOPFAILED;
			rgm_format_errmsg(&ret, REM_RG_AFF_STOPFAILED,
			    rgp->rgl_ccr->rg_name);
			goto check_online_affs_end; //lint !e801
		}
		rgm_free_nlist(violations);
		violations = temp;

		//
		// Iterate through the list, checking if any RGs are currently
		// switching or updating.
		//
		for (temp = violations; temp != NULL; temp = temp->nl_next) {
			//
			// Check if the RG that needs to be dragged off is
			// part of the list of RGs that are being
			// switched.  If so, we can assume that it's being
			// switched on to this node too (since the onnodes are
			// the same for all RGs being switched), so we must
			// reject this switch.
			//
			// If it's not there, we need to check if there
			// are any switches or updates in progress, in which
			// case we can't continue with this switch.
			//
			if (namelist_exists(rg_names, temp->nl_name)) {
				ucmm_print("RGM", NOGET(
				    "check_rg_affinities_onlining: %s found "
				    "rg <%s> that is switching.\n"),
				    rgp->rgl_ccr->rg_name, temp->nl_name);

				ret.err_code = SCHA_ERR_RGRECONF;
				rgm_format_errmsg(&ret, REM_RG_AFF_SAME,
				    rgp->rgl_ccr->rg_name, ln->getFullName(),
				    temp->nl_name);
				goto check_online_affs_end; //lint !e801
			}

#if SOL_VERSION >= __s10
			if (is_enhanced_naming_format_used(temp->nl_name)) {
				if (!allow_inter_cluster()) {
					ucmm_print("RGM", NOGET(
					    "check_rg_affinities_onlining "
					    "Running at too low a version: "
					    "RG <%s> has affinity for RG "
					    "<%s> which is not in local "
					    "cluster\n"),
					    rgp->rgl_ccr->rg_name,
					    rg_names->nl_name);
					continue;
				}
				if (get_remote_rg_status(temp->nl_name, NULL,
					RG_SWITCH_STATUS)) {
					ucmm_print("RGM", NOGET(
					    "check_rg_affinities_onlining: %s "
					    " found rg <%s> that is "
					    "switching.\n"),
					    rgp->rgl_ccr->rg_name,
					    temp->nl_name);

					ret.err_code = SCHA_ERR_RGRECONF;
					rgm_format_errmsg(&ret, REM_RG_AFF_BUSY,
					    rgp->rgl_ccr->rg_name,
					    temp->nl_name);
					goto check_online_affs_end; //lint !e801
				}
				continue;
			}
#endif

			rg_viol = rgname_to_rg(temp->nl_name);
			CL_PANIC(rg_viol != NULL);
			//
			// Note we don't have to check for SW_EVACUATING,
			// because we already determined that rgp is not
			// evacuating.  A SN affinitent might be evacuating
			// from a node not in rgp's Nodelist, however it can
			// only be going offline at this point, so
			// should not preclude rgp from coming online.
			//
			// Later when the SN affinitent is rebalanced, it
			// will exclude nodes on which rgp is going online.
			//
			if (rg_viol->rgl_switching == SW_IN_PROGRESS ||
			    rg_viol->rgl_switching == SW_IN_FAILBACK ||
			    rg_viol->rgl_switching == SW_UPDATING) {

				ucmm_print("RGM", NOGET(
				    "check_rg_affinities_onlining: %s found "
				    "rg <%s> that is switching.\n"),
				    rgp->rgl_ccr->rg_name, temp->nl_name);

				ret.err_code = SCHA_ERR_RGRECONF;
				rgm_format_errmsg(&ret, REM_RG_AFF_BUSY,
				    rgp->rgl_ccr->rg_name, temp->nl_name);
				goto check_online_affs_end; //lint !e801
			}
			//
			// This RG is not busy, so we can carry out our switch
			// and drag this RG with us.  However, we must save
			// its name and node name to issue a warning message.
			//
			// With physical node affinities we might be taking
			// the affinitents off of different zones than rgp,
			// so we put the physical nodename in the warning
			// message.
			//
			char buff[2* MAXRGMOBJNAMELEN + 10];

			(void) sprintf(buff, "%s on node %s", temp->nl_name,
			    use_logicalnode_affinities() ? ln->getFullName() :
			    ln->getNodename());

			if (namelist_add_name(&msg_violations, buff).
			    err_code != SCHA_ERR_NOERR) {
				ret.err_code = SCHA_ERR_NOMEM;
				goto check_online_affs_end; //lint !e801
			}
		}
		rgm_free_nlist(violations);
		violations = NULL;
	}

	//
	// If we had some violations, we need to generate a warning message.
	//
	if (msg_violations != NULL) {
		char *names = namelist_to_str(msg_violations);

		ucmm_print("RGM", NOGET(
		    "check_rg_affinities_onlining: %s found "
		    "rgs <%s> that will be forced offline.\n"),
		    rgp->rgl_ccr->rg_name, names);

		rgm_format_errmsg(&ret, RWM_AFFS_OFFLINE, names,
		    rgp->rgl_ccr->rg_name);
		free(names);
	}

check_online_affs_end:
	rgm_free_nlist(msg_violations);
	rgm_free_nlist(violations);
	return (ret);
}


//
// rg_switch_check_affinities
// ----------------------------
// For each node in the onlist, makes sure that rgp's strong positive and
// strong negative affinities are satisfied on the node.  If the sp
// affinities are not satisifed, but the RG needed is in the list of
// switching rgs, we "postpone" onlining this RG by removing that node from
// the onlist.  However, if the RG we need is not in the list of
// switching rgs, we remove it from the onlist, and "give up" by adding
// it to the giving_up_list.
//
// Also verify that the strong negative affinitents of rgp, as well as RGs
// related by strong positive affinities to the strong negative affinitents
// (the sp transitive closure) are offline.  If any RG in the closure list is
// STOP_FAILED, we remove the node from onlist and add it to giving_up_list.
// If any RG is switching or updating, we leave the node in onlist.
//
// After going through all the nodes, if we've given up on all of them,
// return an error.  If we have at least one node left, return no error.
// Note that we may have a node left (that we haven't given up on), but
// onlist could be empty (if we're postponing starting on that node).
//
static scha_errmsg_t
rg_switch_check_affinities(rglist_p_t rgp, LogicalNodeset &onlist,
    rglist_p_t *rgs, int num_rgs)
{
	namelist_t *violations = NULL, *temp;
	int violations_count = 0;
	scha_errmsg_t ret = {SCHA_ERR_NOERR, NULL};
	char *viol_str;
	scha_err_t err = SCHA_ERR_NOERR;
	LogicalNodeset original_onlist, giving_up_list;

	ucmm_print("RGM", NOGET("rg_switch_check_affinities: %s\n"),
	    rgp->rgl_ccr->rg_name);

	original_onlist = onlist;

	//
	// Iterate through each node on which we want to bring the RG
	// online.
	//
	rgm::lni_t lni = 0;
	while ((lni = onlist.nextLniSet(lni + 1)) != 0) {
		LogicalNode *ln = lnManager->findObject(lni);
		CL_PANIC(ln != NULL);

		//
		// Check if all our strong positive affinities are satisfied
		// on the node.
		//
		if (!sp_affinities_satisfied(err, rgp, lni, NULL,
		    &violations)) {
			if (err != SCHA_ERR_NOERR) {
				// low memory
				ret.err_code = SCHA_ERR_NOMEM;
				rgm_free_nlist(violations);
				return (ret);
			}
			viol_str = namelist_to_str(violations);

			ucmm_print("RGM", NOGET(
			    "rg_switch_check_affinities: %s found rgs "
			    "<%s> that are not online.\n"),
			    rgp->rgl_ccr->rg_name, viol_str);

			//
			// Now check if the violations are all
			// RGs that are still in our list.  If so,
			// we just postpone starting on this lni.
			// Otherwise, we give up on this lni.  Either
			// way, we remove it from the onlist.
			//
			// If we give up on the lni, format an error
			// message in case we will need to give up on
			// this RG entirely.
			//
			if (!rg_names_in_list(violations, rgs, num_rgs)) {
				giving_up_list.addLni(lni);
				ret.err_code = SCHA_ERR_NOMASTER;
				rgm_format_errmsg(&ret, REM_RG_STRONG_POS,
				    rgp->rgl_ccr->rg_name, ln->getFullName(),
				    viol_str);
			}
			onlist.delLni(lni);
			rgm_free_nlist(violations);
			violations = NULL;
			continue;
		}

		rgm_free_nlist(violations);
		violations = NULL;

		//
		// Make sure our strong negative affinities are satisfied.
		// Theoretically, another RG could have been switched
		// onto our target LNI since last time we checked.
		//
		if (!sn_affinities_satisfied(err, rgp, lni, NULL,
		    &violations)) {
			if (err != SCHA_ERR_NOERR) {
				// low memory
				ret.err_code = SCHA_ERR_NOMEM;
				rgm_free_nlist(violations);
				violations = NULL;
				return (ret);
			}

			viol_str = namelist_to_str(violations);

			ucmm_print("RGM", NOGET(
			    "rg_switch_check_affinities: %s found rgs "
			    "<%s> that are online.\n"),
			    rgp->rgl_ccr->rg_name, viol_str);

			//
			// We need to give up on this LNI, but we might
			// not give up on all LNIs.  Store an error message
			// in case we give up on all LNIs.
			//
			ret.err_code = SCHA_ERR_NOMASTER;
			rgm_format_errmsg(&ret, REM_RG_STRONG_NEG,
			    rgp->rgl_ccr->rg_name, ln->getFullName(),
			    viol_str);
			giving_up_list.addLni(lni);
			onlist.delLni(lni);
			rgm_free_nlist(violations);
			violations = NULL;
			continue;
		}
		rgm_free_nlist(violations);
		violations = NULL;

		//
		// Check if we're kicking off any RGs.  If so, make sure
		// none of them are stop failed or busy.  Theoretically,
		// a new RG could have been switched onto this LNI,
		// or an RG could have gone stop_failed.
		//
		if (!compute_sn_violations(err, rgp, lni, &violations,
		    violations_count, B_FALSE)) {

			if (err != SCHA_ERR_NOERR) {
				// low memory
				ret.err_code = SCHA_ERR_NOMEM;
				rgm_free_nlist(violations);
				return (ret);
			}

			// one of them is in stop failed

			ucmm_print("RGM", NOGET(
			    "rg_switch_check_affinities: %s found rg "
			    "in STOP_FAILED.\n"),
			    rgp->rgl_ccr->rg_name);

			//
			// We need to give up on this LNI, but we might
			// not give up on all LNIs.  Store an error message
			// in case we give up on all LNIs.
			//
			ret.err_code = SCHA_ERR_STOPFAILED;
			rgm_format_errmsg(&ret, REM_RG_AFF_STOPFAILED,
			    rgp->rgl_ccr->rg_name);
			giving_up_list.addLni(lni);
			onlist.delLni(lni);
			rgm_free_nlist(violations);
			violations = NULL;
			continue;
		}
		//
		// Now get the complete list
		//
		if (!compute_sp_transitive_closure(err, rgp, lni,
		    violations, &temp, violations_count, B_TRUE, B_FALSE)) {

			if (err != SCHA_ERR_NOERR) {
				// low memory
				ret.err_code = SCHA_ERR_NOMEM;
				rgm_free_nlist(violations);
				rgm_free_nlist(temp);
				return (ret);
			}

			// one of them is in stop failed

			ucmm_print("RGM", NOGET(
			    "rg_switch_check_affinities: %s found rg "
			    "in STOP_FAILED.\n"),
			    rgp->rgl_ccr->rg_name);

			//
			// We need to give up on this LNI, but we might
			// not give up on all LNIs.  Store an error message
			// in case we give up on all LNIs.
			//
			ret.err_code = SCHA_ERR_STOPFAILED;
			rgm_format_errmsg(&ret, REM_RG_AFF_STOPFAILED,
			    rgp->rgl_ccr->rg_name);
			giving_up_list.addLni(lni);
			onlist.delLni(lni);
			rgm_free_nlist(violations);
			violations = NULL;
			rgm_free_nlist(temp);
			temp = NULL;
			continue;
		}
		//
		// If nothing is stop_failed in the closure list,
		// we carry on.  If an RG in the closure list is busy, we
		// continue.  That RG will get forced offline by the slave
		// when this RG tries to come online, and rebalanced by
		// do_rg_switch.
		//
		rgm_free_nlist(violations);
		violations = NULL;
		rgm_free_nlist(temp);
		temp = NULL;
	}

	//
	// Compare the giving up nodelist to the original list of nodes.
	// If we're giving up on all the nodes, do_rg_switch needs to
	// give up on this RG.  We have already set ret with the appropriate
	// error code and messages, so just return it now.
	//
	if (giving_up_list.isEqual(original_onlist)) {

		ucmm_print("RGM", NOGET(
		    "rg_switch_check_affinities: giving up on all nodes "
		    "for rg <%s>.\n"),
		    rgp->rgl_ccr->rg_name);
		return (ret);
	}

	//
	// We have at least one LNI on which we're not giving up.
	// We've removed the other LNIs from onlist, so do_rg_switch
	// will try to bring RG online only on this LNI.  Don't return
	// an error.  Later, after the RG comes online on the LNIs it
	// can, this function will return an error.
	//
	ucmm_print("RGM", NOGET(
	    "rg_switch_check_affinities: NOT giving up on all nodes "
	    "for rg <%s>.\n"),
	    rgp->rgl_ccr->rg_name);
	ret.err_code = SCHA_ERR_NOERR;
	free(ret.err_msg);
	ret.err_msg = NULL;
	return (ret);
}


//
// rg_names_in_list
// ----------------
// Returns B_TRUE if every name in violations namelist is an rg in
// the list of rgs.  Otherwise returns B_FALSE;
//
boolean_t
rg_names_in_list(namelist_t *violations, rglist_p_t *rgs, int num_rgs)
{
	namelist_t *temp;
	int i;
	boolean_t found = B_FALSE;

	for (temp = violations; temp != NULL;
	    temp = temp->nl_next) {
		found = B_FALSE;
		for (i = 0; i < num_rgs; i++) {
			if (strcmp(rgs[i]->rgl_ccr->rg_name, temp->nl_name)
			    == 0) {
				found = B_TRUE;
				break;
			}
		}
		if (!found) {
			ucmm_print("RGM", NOGET(
			    "rg_names_in_list: could not find rg %s.\n"),
			    temp->nl_name);
			return (B_FALSE);
		}
	}
	ucmm_print("RGM", NOGET(
	    "rg_names_in_list: found all names.\n"));
	return (B_TRUE);
}

//
// scswitch -o <list of RG names>
// Called by rgm_scswitch_change_rg_state in the
// admin library linked in by scswitch(1M).
// This function is executed on the President.
//
// locale is not used yet: don't provide the name in the args list
// to avoid spurious warnings.
//
// If the verbose_flag is set the success message is printed.
//
void
rgm_comm_impl::idl_unmanaged_to_offline(const rgm::idl_string_seq_t &rg_names,
    const char *, rgm::idl_regis_result_t_out result, bool verbose_flag,
    sol::nodeid_t nodeid, const char *idlstr_zonename, Environment &)
{
	uint_t rg_index, rg_seq_len;
	rglist_p_t rgp;
	boolean_t need_cond_wait = B_FALSE;
	// SC syslog msg API.
	sc_syslog_msg_handle_t handle;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	LogicalNodeset	*ns = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t temp_res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t *rglistp = NULL;
	uint_t numrgs = 0;
	uint_t i;
	LogicalNodeset emptyNodeset;
	boolean_t	globalflag;
	boolean_t	nodelist_contains;

	//
	// This namelist will be used during handling of multiple operands and
	// continue with the other operands in case of failure of any of the
	// operand.
	// failed_op_list namelist will contain the
	// names of RGs which have failed during processing.
	//
	namelist_t *failed_op_list = NULL;
	rgm::idl_string_seq_t rgnames_seq;

	ucmm_print("idl_unmanaged_to_offline", NOGET("enter\n"));

	rgm_lock_state();

	// check for invalid or duplicate RGs.
	temp_res = prune_operand_list(rg_names, rgnames_seq, B_FALSE);

	if (temp_res.err_code != SCHA_ERR_NOERR) {
		smart_overlay(res, temp_res);
		temp_res.err_code = SCHA_ERR_NOERR;
	}

	(void) transfer_message(&res, &temp_res);

	rg_seq_len = rgnames_seq.length();

	//
	// Note: it is possible that we have not run FINI on some nodes,
	// for some resources in the RG.  This is OK, because boot/init/fini
	// methods are idempotent.  If a fini method didn't run on a node,
	// running the init method (again) cannot hurt anything.
	//

	//
	// Note: in this implementation, there are no guarantees that init
	// methods are run according to RG dependencies.  This is OK; we
	// will document this.
	//

	//
	// An attempt to move an RG out of UNMANAGED state will fail with
	// no side-effects if the RG that is attempting to become managed
	// has an RG_dependency on some other RG that remains UNMANAGED.
	//

	// if there are no valid rgs then dont process further.
	if (rg_seq_len == 0)
		goto return_early; //lint !e801

	ngzone_check(idlstr_zonename, globalflag);
	//
	// Verify that each RG in the pruned rglist is indeed unmanaged,
	// and is not being switched or updated by another thread.
	// Build a NULL-terminated array (rglistp) of pointers to the
	// RG structures on which we will operate.
	//
	for (rg_index = 0; rg_index < rg_seq_len; rg_index++) {
		rgp = rgname_to_rg(rgnames_seq[rg_index]);
		if (!globalflag) {
			nodelist_check(rgp, nodeid, idlstr_zonename,
			    nodelist_contains);
			if (!nodelist_contains) {
				temp_res.err_code = SCHA_ERR_ACCESS;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res,
				    REM_RG_NODELIST_NGZONE,
				    rgp->rgl_ccr->rg_name,
				    (const char *)idlstr_zonename);
				temp_res.err_code = SCHA_ERR_NOERR;
				continue;
			}
		}

		if (rgp->rgl_switching != SW_NONE) {
			// Some other thread is trying to switch or update
			// this RG.
			ucmm_print("RGM", NOGET("idl_unmanaged_to_offline: "
			    "RG is being switched or updated\n"));
			temp_res.err_code = SCHA_ERR_RGRECONF;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;

			rgm_format_errmsg(&res, REM_RG_BUSY,
			    (const char*)rgnames_seq[rg_index]);

			//  continue instead of bailing out.
			temp_res.err_code = SCHA_ERR_NOERR;
			continue;
		}

		if (!rgp->rgl_ccr->rg_unmanaged) {
			//
			// RG must be unmanaged to go to managed via
			// scswitch -o command
			// For idempotency, if RG already managed,
			// just skip over it.
			//
			ucmm_print("RGM", NOGET("idl_unmanaged_to_offline: "
			    "RG <%s> already managed; skip it"),
			    (const char *)rgnames_seq[rg_index]);
			continue;
		}

		//
		// "system" property checks
		//
		//	Can't manage a "system" RG
		//
		if (rgp->rgl_ccr->rg_system) {
			temp_res.err_code = SCHA_ERR_ACCESS;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;

			rgm_format_errmsg(&res, REM_SYSTEM_RG,
				(const char *)rgnames_seq[rg_index]);
			ucmm_print("RGM", NOGET("idl_unmanaged_to_offline: "
			    "RG: <%s> RG_SYSTEM property is TRUE\n"),
			    (const char *)rgnames_seq[rg_index]);

			temp_res.err_code = SCHA_ERR_NOERR;

			//  continue instead of bailing out.
			continue;
		}

		//
		// Add rgp to rglistp
		//
		temp_res = rglist_add_rg(rglistp, rgp, numrgs++);
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			// This error occurs because of low memory
			// Stop processing.
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			goto return_early; //lint !e801
		}
	}

	//
	// If all the RGs in the list are already managed, return early.
	//
	if (numrgs > 0) {
		ucmm_print("RGM",
		    NOGET("-o at least one RG needs processing\n"));
	} else {
		goto return_early; //lint !e801
	}

	CL_PANIC(rglistp != NULL);

	//
	// Verify that no RG in the list has a dependency on another
	// unmanaged RG which is not also in the list.
	//
	for (i = 0; i < numrgs; i++) {
		rgp = rglistp[i];

		// Do generic RG validation on the RG
		if ((temp_res = rg_sanity_checks(rgp->rgl_ccr,
		    &(rgp->rgl_affinities), B_TRUE, rglistp, NULL)).err_code !=
		    SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			if (temp_res.err_msg) {
				rgm_sprintf(&res, "%s", temp_res.err_msg);
				free(temp_res.err_msg);
				temp_res.err_msg = NULL;
				if (res.err_code != SCHA_ERR_NOERR) {
					goto return_early; //lint !e801
				}
			}

			// Add the failed RG to failed_op_list
			// so that we dont process this RG further.

			add_op_to_failed_list(
			    &failed_op_list, rgp->rgl_ccr->rg_name);
			temp_res.err_code = SCHA_ERR_NOERR;
			continue;
		}
	}

	//
	// Set the switching flag of each RG to update-in-progress.
	//
	for (i = 0; i < numrgs; i++) {
		// Skip the RGs which have failed earlier.
		if (namelist_exists(failed_op_list,
		    rglistp[i]->rgl_ccr->rg_name))
			continue;

		rglistp[i]->rgl_switching = SW_UPDATING;
	}


	//
	// Sort the RG list by strong affinities and rg_dependencies, so that
	// we will manage the least-dependent RG first.
	//
	temp_res = switch_order_rgarray(rglistp);
	if (temp_res.err_code != SCHA_ERR_NOERR) {
		// This error occurs because of low memory.
		// Stop processing.
		if (res.err_code == SCHA_ERR_NOERR)
			res.err_code = temp_res.err_code;
		goto return_early; //lint !e801
	}

	//
	// Now process each RG in the sorted array. If we run into errors such
	// as a CCR update error, stop further processing. When we bail out
	// with an error, it is possible that some RGs may have already been
	// processed. After completing the processing loop, we should still
	// wait for *all* RGs in the list to reach an endish state. This is
	// OK since the RGs we do not process must already be in endish states.
	//
	for (i = 0; i < numrgs; i++) {
		// Skip the RGs which have failed earlier.
		if (namelist_exists(failed_op_list,
		    rglistp[i]->rgl_ccr->rg_name))
			continue;

		rgp = rglistp[i];

		// rglistp only contains RGs that are unmanaged.

		// Latch the intention on all slaves
		(void) latch_intention(rgm::RGM_ENT_RG, rgm::RGM_OP_MANAGE,
		    rgp->rgl_ccr->rg_name, &emptyNodeset,
		    rgm::INTENT_MANAGE_RG);

		// set RG managed in CCR

		if ((temp_res = rgmcnfg_set_rgunmanaged(rgp->rgl_ccr->rg_name,
		    B_FALSE, ZONE)).err_code != SCHA_ERR_NOERR) {

			//
			// CCR update failed. Stop further processing
			// and return early.
			//
			ucmm_print("RGM", NOGET(
			    "CCR update failed when changing the state of "
			    "resource group %s to managed.\n"),
			    rgp->rgl_ccr->rg_name);
			(void) unlatch_intention();
			res.err_code = SCHA_ERR_INTERNAL;
			rgm_format_errmsg(&res, REM_INTERNAL_RG_UP,
			    rgp->rgl_ccr->rg_name);
			// This is a fatal error and it is not
			// recommonded to continue with other operands.
			// bailing out.
			goto done_RG_processing; //lint !e801
		}

		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RG_TAG,
		    rgp->rgl_ccr->rg_name);
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that a resource
		// group's state has changed. This message can be used by
		// system monitoring tools.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, STATE_CHANGED,
		    SYSTEXT("resource group %s state change to managed."),
		    rgp->rgl_ccr->rg_name);

		// post an event
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE, ESC_CLUSTER_RG_CONFIG_CHANGE,
			CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_RG_NAME, SE_DATA_TYPE_STRING,
			rgp->rgl_ccr->rg_name,
			CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			CL_EVENT_CONFIG_MANAGED, NULL);
#else
		(void) sc_publish_event(ESC_CLUSTER_RG_CONFIG_CHANGE,
			CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_RG_NAME, SE_DATA_TYPE_STRING,
			rgp->rgl_ccr->rg_name,
			CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			CL_EVENT_CONFIG_MANAGED, NULL);
#endif
		sc_syslog_msg_done(&handle);

		// Print a success message that RG state has been changed.
		if (verbose_flag)
			rgm_format_successmsg(&res, RWM_RG_UNMANAGED_TO_OFFLINE,
				rgp->rgl_ccr->rg_name);


		//
		// We set rgp->rgl_ccr->rg_unmanaged to FALSE
		// in process_intention().
		//

		//
		// Notify all nodes that the RG has been
		// moved from unmanaged to offline.
		//
		(void) process_intention();

		//
		// Unlatch the intention on all slaves;
		// work has been launched on slaves.  A new President
		// will not need to worry about recovery.
		//
		(void) unlatch_intention();

		need_cond_wait = B_TRUE;

		//
		// publish an rg_state_change event as well.
		// This event must be published after the intention is
		// processed, so the changes have been propogated to
		// the in-memory structure.
		//
		(void) post_event_rg_state(rgp->rgl_ccr->rg_name);
	}

done_RG_processing:

	//
	// Signal the rgfreeze() thread to recompute the freeze state of
	// all managed RGs.  This will pick up any of our newly-managed
	// RGs that might be frozen.  We don't have to worry about the
	// president dying before reaching this point, because a new
	// president always updates freeze state on all slaves when it
	// is starting up.
	//
	cond_rgfreeze.signal();

	//
	// Flush the state change queue before entering the cond_wait loop.
	// There may be no reason to wait; all RGs in the list already
	// could be OFFLINE.  On the other hand, as a result of this call
	// we may find that some RGs have their states set to
	// rgm::RG_OFF_PENDING_METHODS.
	//
	if (need_cond_wait) {

		rgm_unlock_state();
		flush_state_change_queue();
		rgm_lock_state();

		//
		// Sleep on slavewait condition variable until all RGs in
		// the list have moved from OFF_PENDING_METHODS to OFFLINE.
		// XXX While any R is still PENDING_INIT/INITING, RG stays in
		// XXX OFF_PENDING_METHODS.  We need to fix the state machine
		// XXX to properly handle failed init methods. (bugids 4215631,
		// XXX 4215776)
		// XXX We should check FYI state here per node and return a
		// XXX meaningful error code.
		//
		for (i = 0; i < numrgs; i++) {
			// Skip the RGs which have failed earlier.
			if (namelist_exists(failed_op_list,
			    rglistp[i]->rgl_ccr->rg_name))
				continue;

			rgp = rglistp[i];
			//
			// The RG will be OFFLINE on those nodes
			// not currently in the cluster, so skip those.
			//

			for (LogicalNodeManager::iter it = lnManager->begin();
			    it != lnManager->end(); ++it) {
				LogicalNode *lnNode =
				    lnManager->findObject(it->second);
				CL_PANIC(lnNode != NULL);

				rgm::lni_t node = lnNode->getLni();

				if (!lnNode->isInCluster()) {
					continue;
				}

				while (rgp->rgl_xstate[node].rgx_state ==
				    rgm::RG_OFF_PENDING_METHODS) {
					cond_slavewait.wait(
					    &Rgm_state->rgm_mutex);
				}
			}
		}
	}

	// XXX bug 4215776: Implement FYI states in rgmd:
	// XXX Return error code indicating complete success, partial success,
	// XXX complete failure, depending on FYI state.

	// for each RG, reset its switching flag to none
	for (i = 0; i < numrgs; i++) {
		// Skip the RGs which have failed earlier.
		if (namelist_exists(failed_op_list,
		    rglistp[i]->rgl_ccr->rg_name))
			continue;

		rgp = rglistp[i];

		//
		// It should not be possible for this update to be "busted",
		// because the RG is initially unmanaged and therefore
		// should not have been able to lose a current master when
		// a node died.
		//
		// However, it is possible that this RG was affected by
		// a node evacuation when we released the state lock, above.
		//
		if (rgp->rgl_switching == SW_UPDATE_BUSTED) {
			ucmm_print("idl_unmanaged_to_offline",
			    NOGET("INTERNAL ERROR: RG <%s> is busted"),
			    (const char *)rgp->rgl_ccr->rg_name);
			temp_res.err_code = SCHA_ERR_INTERNAL;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_UNMANAGED_BUSTED,
			    (const char *)rgp->rgl_ccr->rg_name);
		}

		if (rgp->rgl_switching != SW_EVACUATING)
			rgp->rgl_switching = SW_NONE;
	}


return_early:

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);
	free(temp_res.err_msg);
	free(rglistp);

	delete (ns);
	if (failed_op_list)
		rgm_free_nlist(failed_op_list);
	result = retval;
	rgm_unlock_state();
}


//
// scswitch -u <list of RG names>
// Called by rgm_scswitch_change_rg_state in the
// admin library linked in by scswitch(1M).
// This function is executed on the President.
//
// locale is not used yet: don't provide the name in the args list
// to avoid spurious warnings.
//
// If the verbose_flag is set the success message is printed.
//
void
rgm_comm_impl::idl_offline_to_unmanaged(const rgm::idl_string_seq_t &rg_names,
    const char *, rgm::idl_regis_result_t_out result, bool verbose_flag,
    sol::nodeid_t nodeid, const char *idlstr_zonename, Environment &)
{
	uint_t			rg_index;
	uint_t			rg_seq_len;
	rglist_p_t		rgp;
	rlist_p_t		r;
	rgm::lni_t		lni;
	boolean_t		need_cond_wait = B_FALSE;
	// SC syslog msg API.
	sc_syslog_msg_handle_t	handle;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t		temp_res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t		*rglistp = NULL;
	uint_t			numrgs = 0;
	uint_t			i;
	LogicalNodeset		emptyNodeset;
	boolean_t		globalflag;
	boolean_t		nodelist_contains;
	//
	// This namelist will be used during handling of multiple operands and
	// continue with the other operands in case of failure of any of the
	// operand.
	// failed_op_list namelist will contain the
	// names of RGs which have failed during processing.
	//
	namelist_t *failed_op_list = NULL;
	boolean_t skip_this_rg = B_FALSE;

	rgm::idl_string_seq_t rgnames_seq;
	ucmm_print("RGM", NOGET("in idl_offline_to_unmanaged\n"));

	rgm_lock_state();

	// check for invalid or duplicate RGs.
	temp_res = prune_operand_list(rg_names, rgnames_seq, B_FALSE);

	if (temp_res.err_code != SCHA_ERR_NOERR) {
		smart_overlay(res, temp_res);
		temp_res.err_code = SCHA_ERR_NOERR;
	}

	(void) transfer_message(&res, &temp_res);

	rg_seq_len = rgnames_seq.length();


	//
	// Note: in this implementation, there are no guarantees that fini
	// methods are run according to RG dependencies.  This is OK; we
	// will document this.
	//


	// if there are no valid rgs then dont process further.
	if (rg_seq_len == 0)
		goto return_early; //lint !e801

	ngzone_check(idlstr_zonename, globalflag);

	//
	// Verify that RGs in the pruned rglist are not being switched or
	// updated by another thread, are offline on all nodes and that
	// their resources have onoff_switch disabled.  Skip RGs that are
	// already unmanaged.
	// Build a NULL-terminated array (rglistp) of pointers to the
	// RG structures on which we will operate.
	//
	for (rg_index = 0; rg_index < rg_seq_len; rg_index++) {
		skip_this_rg = B_FALSE;
		rgp = rgname_to_rg(rgnames_seq[rg_index]);
		if (!globalflag) {
			nodelist_check(rgp, nodeid, idlstr_zonename,
			    nodelist_contains);
			if (!nodelist_contains) {
				temp_res.err_code = SCHA_ERR_ACCESS;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res,
				    REM_RG_NODELIST_NGZONE,
				    rgp->rgl_ccr->rg_name,
				    (const char *)idlstr_zonename);
				temp_res.err_code = SCHA_ERR_NOERR;
				continue;
			}
		}



		// No need to check for endish state; we check below that RG
		// is offline on all nodes.
		if (rgp->rgl_switching != SW_NONE) {
			// Some other thread is trying to switch or update
			// this RG.
			ucmm_print("RGM", NOGET("idl_offline_to_unmanaged: "
			    "RG is being switched or updated\n"));
			temp_res.err_code = SCHA_ERR_RGRECONF;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_RG_BUSY,
			    (const char *)rgp->rgl_ccr->rg_name);
			// continue instead of bailing out.
			temp_res.err_code = SCHA_ERR_NOERR;
			continue;
		}

		//
		// Idempotency: if already unmanaged, just skip it.
		// We'll take care not to do a cond_wait if all RGs
		// are already unmanaged.
		//
		if (rgp->rgl_ccr->rg_unmanaged) {
			ucmm_print("RGM", NOGET("idl_offline_to_unmanaged: "
			    "RG <%s> already unmanaged; skip it"),
			    rgp->rgl_ccr->rg_name);
			continue;
		}

		//
		// RG must be offline on all nodes to go to unmanaged via
		// scswitch -u command
		//
		for (LogicalNodeManager::iter it = lnManager->begin();
		    it != lnManager->end(); ++it) {
			LogicalNode *lnNode = lnManager->findObject(it->second);
			CL_PANIC(lnNode != NULL);

			lni = lnNode->getLni();

			if (rgp->rgl_xstate[lni].rgx_state !=
			    rgm::RG_OFFLINE &&
			    rgp->rgl_xstate[lni].rgx_state !=
			    rgm::RG_OFF_BOOTED &&
			    rgp->rgl_xstate[lni].rgx_state !=
			    rgm::RG_OFFLINE_START_FAILED) {
				ucmm_print("RGM",
				    NOGET("idl_offline_to_unmanaged: "
				    "RG <%s> is not offline on node <%d>, "
				    "ret <%d>\n"),
				    rgp->rgl_ccr->rg_name, lni,
				    SCHA_ERR_RGSTATE);
				temp_res.err_code = SCHA_ERR_RGSTATE;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res, REM_RG_UNMAN_NOT_OFF,
				    (const char *)rgp->rgl_ccr->rg_name);
				skip_this_rg = B_TRUE;
				temp_res.err_code = SCHA_ERR_NOERR;
				break;
			}
		}

		if (skip_this_rg)
			continue;

		//
		// process the rg if skip_this_rg is not set.
		// verify that each R in the RG has its onoff_switch
		// disabled
		//
		for (r = rgp->rgl_resources; r != NULL;
		    r = r->rl_next) {
			for (LogicalNodeManager::iter it = lnManager->begin();
			    it != lnManager->end(); ++it) {
				LogicalNode *lnNode =
				    lnManager->findObject(it->second);
				CL_PANIC(lnNode != NULL);

				lni = lnNode->getLni();
				if (((rgm_switch_t *)
				    r->rl_ccrdata->r_onoff_switch)->
				    r_switch[lni] != SCHA_SWITCH_DISABLED) {
					ucmm_print("RGM", NOGET(
					    "idl_offline_to_unmanaged: "
					    "RG <%s> R <%s> onoff is "
					    "not disabled, ret <%d>\n"),
					    rgp->rgl_ccr->rg_name,
					    r->rl_ccrdata->r_name,
					    SCHA_ERR_RGSTATE);
					temp_res.err_code = SCHA_ERR_RGSTATE;
					if (res.err_code == SCHA_ERR_NOERR)
						res.err_code =
						    temp_res.err_code;
					rgm_format_errmsg(&res,
					    REM_RG_UNMAN_RS_NOT_DIS,
					    r->rl_ccrdata->r_name,
					    (const char *)
					    rgp->rgl_ccr->rg_name);
					skip_this_rg = B_TRUE;
					temp_res.err_code = SCHA_ERR_NOERR;
					break;
				}
			}
		}

		//
		// skip the rg if any of its resources were found enabled
		// in the above loop.
		//
		if (skip_this_rg)
			continue;
		//
		// "system" property checks:
		//
		// Can't unmanage a "system" RG
		//
		if (rgp->rgl_ccr->rg_system) {
			temp_res.err_code = SCHA_ERR_ACCESS;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_SYSTEM_RG,
			    (const char *)rgp->rgl_ccr->rg_name);
			ucmm_print("RGM", NOGET("idl_offline_to_unmanaged: "
			    "RG <%s> RG_SYSTEM property is TRUE\n"),
			    (const char *)rgp->rgl_ccr->rg_name);

			// continue instead of bailing out.
			temp_res.err_code = SCHA_ERR_NOERR;
			continue;
		}

		//
		// Add rgp to rglistp
		//

		temp_res = rglist_add_rg(rglistp, rgp, numrgs++);
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			goto return_early; //lint !e801
		}
	}
	//
	// If all the RGs in the list are already unmanaged, return early.
	//
	if (numrgs > 0) {
		ucmm_print("RGM",
		    NOGET("-u at least one RG needs processing\n"));
	} else {
		goto return_early; //lint !e801
	}

	CL_PANIC(rglistp != NULL);

	//
	// Verify that no RG in the list has a managed dependent RG which
	// is not also in the list.  Also check for managed positive
	// affinitents.
	//
	for (i = 0; i < numrgs; i++) {
		rgp = rglistp[i];

		//
		// An attempt to move an RG to UNMANAGED state will fail with no
		// side-effects if some other managed RG which is not itself
		// becoming unmanaged has an RG dependency on
		// the RG that is attempting to become UNMANAGED.
		//
		bool has_man_dep = has_man_dependents(&temp_res, rgp, rglistp,
		    failed_op_list);
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			// error is NOMEM or internal -- fatal
			res.err_code = temp_res.err_code;
			goto return_early; //lint !e801
		}
		if (has_man_dep) {
			ucmm_print("RGM",
			    NOGET("idl_offline_to_unmanaged: "
			    "RG <%s> has managed dependent(s)\n"),
			    rgp->rgl_ccr->rg_name);
			temp_res.err_code = SCHA_ERR_DEPEND;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_RG_UNMAN_DEPEND,
			    (const char *)rgp->rgl_ccr->rg_name);
			// Add the failed RG to failed_op_list
			// so that we dont process this RG further.
			add_op_to_failed_list(
			    &failed_op_list, rgp->rgl_ccr->rg_name);
			// continue instead of bailing out.
			temp_res.err_code = SCHA_ERR_NOERR;
			continue;
		}

		temp_res = check_pos_affinity_managed(rgp->rgl_ccr,
		    &rgp->rgl_affinitents, B_FALSE, rglistp, failed_op_list);
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			ucmm_print("RGM", NOGET("idl_offline_to_unmanaged: "
			    "RG <%s> has managed positive affinitent(s)\n"),
			    rgp->rgl_ccr->rg_name);
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;

			if (temp_res.err_msg) {
				rgm_sprintf(&res, "%s", temp_res.err_msg);
				free(temp_res.err_msg);
				temp_res.err_msg = NULL;
				if (res.err_code != SCHA_ERR_NOERR) {
					goto return_early; //lint !e801
				}
			}
			temp_res.err_code = SCHA_ERR_NOERR;
			// Add the failed RG to failed_op_list
			// so that we dont process this RG further.
			add_op_to_failed_list(
			    &failed_op_list, rgp->rgl_ccr->rg_name);

			// continue instead of bailing out.
			continue;
		}
	}

	//
	// Set the switching flag of each RG to update-in-progress.
	//
	for (i = 0; i < numrgs; i++) {
		// Skip the RGs which have failed earlier.
		if (namelist_exists(failed_op_list,
		    rglistp[i]->rgl_ccr->rg_name))
			continue;
		rglistp[i]->rgl_switching = SW_UPDATING;
	}

	//
	// Sort the RG list by strong affinities and rg_dependencies, then
	// reverse the list so that we will unmanage the most-dependent RG
	// first.
	//
	temp_res = switch_order_rgarray(rglistp);
	if (temp_res.err_code != SCHA_ERR_NOERR) {
		// This error occurs because of low memory.
		// Stop processing.
		if (res.err_code == SCHA_ERR_NOERR)
			res.err_code = temp_res.err_code;
		goto return_early; //lint !e801
	}
	reverse_switch_order_array((void **)rglistp);

	// Now process each RG in the sequence. If we run into errors such
	// as a CCR update error, stop further processing. When we bail out
	// with an error, it is possible that some RGs may have already been
	// processed. After completing the processing loop, we should still
	// wait for *all* RGs in the list to reach an endish state. This is
	// OK since the RGs we do not process must already be in endish states.

	for (i = 0; i < numrgs; i++) {
		// Skip the RGs which have failed earlier.
		if (namelist_exists(failed_op_list,
		    rglistp[i]->rgl_ccr->rg_name))
			continue;
		rgp = rglistp[i];

		// rglistp only contains RGs that are unmanaged.

		//
		// Prepare to latch the intention on all slaves.
		// Note that all slaves will need to update the RG's
		// state to Unmanaged, but it could be that not all
		// slaves will need to launch fini methods.
		//
		(void) latch_intention(rgm::RGM_ENT_RG, rgm::RGM_OP_UNMANAGE,
		    rgp->rgl_ccr->rg_name, &emptyNodeset, 0);

		//
		// set RG unmanaged in CCR after latching the intention
		// We set rgp->rgl_ccr->rg_unmanaged to TRUE
		// in process_intention().
		//
		if ((temp_res = rgmcnfg_set_rgunmanaged(rgp->rgl_ccr->rg_name,
		    B_TRUE, ZONE)).err_code != SCHA_ERR_NOERR) {

			// CCR update failed. Stop further processing.

			ucmm_print("RGM", NOGET(
			    "CCR update failed when changing the state of "
			    "resource group %s to unmanaged.\n"),
			    rgp->rgl_ccr->rg_name);
			(void) unlatch_intention();
			temp_res.err_code = SCHA_ERR_INTERNAL;
			res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_INTERNAL_RG_UP,
			    rgp->rgl_ccr->rg_name);
			goto done_RG_processing; //lint !e801
		}

		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RG_TAG,
		    rgp->rgl_ccr->rg_name);
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that a resource
		// group's state has changed. This message can be used by
		// system monitoring tools.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, STATE_CHANGED,
		    SYSTEXT("resource group %s state change to unmanaged."),
		    rgp->rgl_ccr->rg_name);

		// post an event
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE, ESC_CLUSTER_RG_CONFIG_CHANGE,
			CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_RG_NAME, SE_DATA_TYPE_STRING,
			rgp->rgl_ccr->rg_name,
			CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			CL_EVENT_CONFIG_UNMANAGED,
			NULL);
#else
		(void) sc_publish_event(ESC_CLUSTER_RG_CONFIG_CHANGE,
			CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
			CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			CL_RG_NAME, SE_DATA_TYPE_STRING,
			rgp->rgl_ccr->rg_name,
			CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			CL_EVENT_CONFIG_UNMANAGED,
			NULL);
#endif
		sc_syslog_msg_done(&handle);

		// Print a success message that RG state has been changed.

		if (verbose_flag)
			rgm_format_successmsg(&res, RWM_RG_OFFLINE_TO_UNMANAGED,
			    rgp->rgl_ccr->rg_name);

		//
		// Notify all nodes that the RG has been
		// moved from offline to unmanaged.
		//
		(void) process_intention();

		// Unlatch the intention on all slaves
		(void) unlatch_intention();

		need_cond_wait = B_TRUE;

		//
		// publish an rg_state_change event as well.
		// This event must be published after the intention is
		// processed, so the changes have been propogated to
		// the in-memory structure.
		//
		(void) post_event_rg_state(rgp->rgl_ccr->rg_name);

	}

done_RG_processing:

	//
	// Flush the state change queue before entering the cond_wait loop.
	// There may be no reason to wait; all RGs in the list already
	// could be Unmanaged (need_cond_wait == FALSE).
	// On the other hand, as a result of this call
	// we may find that some RGs have their states set to
	// rgm::RG_OFF_PENDING_METHODS.
	//
	if (need_cond_wait) {

		rgm_unlock_state();
		flush_state_change_queue();
		rgm_lock_state();

		//
		// Sleep on slavewait condition variable until all RGs in
		// the list have moved from OFF_PENDING_METHODS back to
		// OFFLINE.
		//
		// If the RG is in OFF_PENDING_METHODS state on the slave, the
		// slave's state machine is guaranteed to wake the president
		// when the RG moves back to OFFLINE.  So it is safe to do
		// the cond_wait.
		//
		for (i = 0; i < numrgs; i++) {
			// Skip the RGs which have failed earlier.
			if (namelist_exists(failed_op_list,
			    rglistp[i]->rgl_ccr->rg_name))
				continue;
			rgp = rglistp[i];

			//
			// The RG will be OFFLINE on those nodes
			// not currently in the cluster, so skip those.
			//

			for (LogicalNodeManager::iter it = lnManager->begin();
			    it != lnManager->end(); ++it) {
				LogicalNode *lnNode =
				    lnManager->findObject(it->second);
				CL_PANIC(lnNode != NULL);

				rgm::lni_t node = lnNode->getLni();

				if (!lnNode->isInCluster()) {
					continue;
				}

				while (rgp->rgl_xstate[node].rgx_state ==
				    rgm::RG_OFF_PENDING_METHODS) {
					(void) cond_slavewait.wait(
					    &Rgm_state->rgm_mutex);
				}

			} // for each LN
		} // for each RG
	} // if need_cond_wait

	//
	// If one or more CCR updates failed above, res has been set to contain
	// all of the error messages plus the error code of the last failure.
	//

	// XXX Bugid 4215776 (Implement FYI states)
	// XXX Return error code indicating complete success, partial success,
	// XXX complete failure, depending on FYI state.

	// for each RG, reset its switching flag to none
	for (i = 0; i < numrgs; i++) {
		// Skip the RGs which have failed earlier.
		if (namelist_exists(failed_op_list,
		    rglistp[i]->rgl_ccr->rg_name))
			continue;
		rgp = rglistp[i];

		//
		// It should not be possible for this update to be "busted",
		// because the RG is initially offline on all nodes and
		// therefore should not have been able to lose a current
		// master when a node died.
		//
		if (rgp->rgl_switching == SW_UPDATE_BUSTED) {
			ucmm_print("idl_offline_to_unmanaged",
			    NOGET("INTERNAL ERROR: RG <%s> is busted"),
			    (const char *)rgp->rgl_ccr->rg_name);
			temp_res.err_code = SCHA_ERR_INTERNAL;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_OFFLINE_BUSTED,
			    (const char *)rgp->rgl_ccr->rg_name);
		}

		//
		// Since this RG is now unmanaged, it is not subject to
		// a node evacuation so we don't have to check here for
		// SW_EVACUATING.
		//
		rgp->rgl_switching = SW_NONE;
	}

return_early:

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);
	free(temp_res.err_msg);
	free(rglistp);
	if (failed_op_list)
		rgm_free_nlist(failed_op_list);
	result = retval;
	rgm_unlock_state();
}

//
// is_r_monitorable
// Returns B_TRUE if the resource can have monitoring enabled.
// This is the case if the resource has a MONITOR_START or MONITOR_CHECK
// method, or if the resource is scalable (since the ssm wrapper defines
// implicit monitoring methods for all scalable resources).
//
// Otherwise, returns B_FALSE.
//
static boolean_t
is_r_monitorable(const rlist_p_t r_ptr)
{
	if (is_r_scalable(r_ptr->rl_ccrdata) ||
	    is_method_reg(r_ptr->rl_ccrtype, METH_MONITOR_START, NULL) ||
	    is_method_reg(r_ptr->rl_ccrtype, METH_MONITOR_CHECK, NULL)) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}

//
// This function is called during the recursive disable option. Based on the
// deps flag the given dependency list will be scanned. If any dependent
// resource is in enabled state and not yet present in rnames then that
// resource will be added to rnames and outlist.
//
static scha_err_t
check_disable_dependents(rdeplist_t *deps,
    namelist_t **rnames, rgm::idl_string_seq_t &outlist)
{
	rlist_p_t	rp;
	rdeplist_t	*p;
	boolean_t flag = B_FALSE;
	std::map<rgm::lni_t, scha_switch_t>::iterator it;

	for (p = deps; p != NULL; p = p->rl_next) {
		// Skip if remote cluster dependencies.
		if (is_enhanced_naming_format_used(p->rl_name))
			continue;
		rp = rname_to_r(NULL, p->rl_name);
		CL_PANIC(rp != NULL);
		for (it = ((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->
		    r_switch.begin(); it != ((rgm_switch_t *)rp->rl_ccrdata->
		    r_onoff_switch)->r_switch.end(); ++it) {
			if (it->second == SCHA_SWITCH_ENABLED) {
				flag = B_TRUE;
				break;
			}
		}
		// Check for enabled dependents.
		if ((flag) &&
		    !namelist_exists(*rnames, rp->rl_ccrdata->r_name)) {
			if (namelist_add_name(rnames, rp->rl_ccrdata->r_name).
			    err_code == SCHA_ERR_NOMEM) {
				return (SCHA_ERR_NOMEM);
			}
			outlist.length(outlist.length() + 1);
			//
			// idl string assignment from a (const char *)
			// allocates memory & copies the string.
			//
			outlist[outlist.length() - 1] =
			    (const char *)rp->rl_ccrdata->r_name;
		}
	}
	return (SCHA_ERR_NOERR);
}

//
// This function is called during the recursive enable option. Based on the
// deps flag the given dependency list will be scanned and if any dependee
// resource is in disabled state and not yet present in the rnames then that
// resource will be added to rnames and outlist.
//
static scha_err_t
check_enable_dependees(rdeplist_t *deps, namelist_t **rnames,
    rgm::idl_string_seq_t &outlist)
{
	rlist_p_t	rp;
	rdeplist_t	*p;
	boolean_t flag = B_FALSE;
	std::map<rgm::lni_t, scha_switch_t>::iterator it;

	for (p = deps; p != NULL; p = p->rl_next) {
		// Skip if remote cluster dependencies.
		if (is_enhanced_naming_format_used(p->rl_name))
			continue;
		rp = rname_to_r(NULL, p->rl_name);
		CL_PANIC(rp != NULL);
		for (it = ((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->
		    r_switch.begin(); it != ((rgm_switch_t *)rp->rl_ccrdata->
		    r_onoff_switch)->r_switch.end(); ++it) {
			if (it->second != SCHA_SWITCH_ENABLED) {
				flag = B_TRUE;
				break;
			}
		}
		// Check for disabled dependee
		if ((flag) &&
		    !namelist_exists(*rnames, rp->rl_ccrdata->r_name)) {
			if (namelist_add_name(rnames, rp->rl_ccrdata->r_name).
			    err_code == SCHA_ERR_NOMEM) {
				return (SCHA_ERR_NOMEM);
			}
			outlist.length(outlist.length() + 1);
			//
			// idl string assignment from a (const char *)
			// allocates memory & copies the string.
			//
			outlist[outlist.length() - 1] =
			    (const char *)rp->rl_ccrdata->r_name;
		}
	}
	return (SCHA_ERR_NOERR);
}

//
// This is the function which is called by rgm_scswitch_set_resource_switch()
// to implement scswitch -e|-n [-M] [-h].
// This function performs action on president node to enable/disable R
// or enable/disable monitor_switch on a set of nodes or on all nodes.
//
// This function is enhanced to handle the case of recursively disabling the
// dependents if the depndee resource is disabled and recursively
// enabling the dependee in case of enabling any dependent resource.
//
// If the verbose_flag is set the success message is printed.
//
void
rgm_comm_impl::idl_scswitch_onoff(
	const rgm::idl_scswitch_onoff_args	&scswoo_args,
	rgm::idl_regis_result_t_out		result,
	bool verbose_flag,
	Environment	&)
{
	uint_t			i;
	uint_t			nummasters = 0;
	char			*r_name = NULL;
	char			*rg_name = NULL;
	char			*nodename = NULL;

	rlist_p_t		r_ptr = NULL;
	rlist_p_t		*my_rss = NULL;
	namelist_t		*rnames = NULL;
	rglist_p_t		rg_ptr = NULL;
	namelist_t		*rgnames = NULL;	// a "work list" of the
							// affected RGs
	namelist_t		*rgnamep;

	rgm::rgm_operation	opcode;
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t		temp_res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t	handle;		// SC syslog msg API.
	LogicalNodeset		pending_boot_ns;
	LogicalNodeset		stop_failed_ns;
	LogicalNodeset		r_restart_ns;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	rgm::lni_t		lni;
	scha_err_t		busted_err = SCHA_ERR_NOERR;
	scha_err_t		start_err = SCHA_ERR_NOERR;
	boolean_t		do_check_for_startfailed = B_FALSE;
	boolean_t		flag = B_FALSE;
	LogicalNodeset		emptyNodeset;
	LogicalNodeset		ns, operand_ns, *rg_ns;
	LogicalNode		*ln = NULL;

	// get the recursive flag.
	boolean_t is_recursive = B_FALSE;
	rgm::idl_string_seq_t  outlist, rslist;
	boolean_t	nodelist_contains;
	boolean_t	globalflag;

	ucmm_print("RGM", NOGET("enter the idl_scswitch_onoff()\n"));

	if (scswoo_args.R_names.length() == 0) {
		// Should never happen; we'll treat as a no-op.
		retval->ret_code = SCHA_ERR_NOERR;
		result = retval;
		return;
	}
	if (scswoo_args.flags & 0x1) {
		is_recursive = B_TRUE;
	}
	// idl_scswitch_onoff is called at top level so it should claim
	// the lock.
	rgm_lock_state();

	nummasters = scswoo_args.N_names.length();
	for (i = 0; i < nummasters; i++) {
		//
		// have to strdup; otherwise get
		// "Non-const function CORBA::String_field::operator char*()
		// called for const object"
		//
		nodename = strdup_nocheck(scswoo_args.N_names[i]);
		if (nodename == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto finish; //lint !e801
		}
		ln = get_ln(nodename, &temp_res.err_code);

		if (temp_res.err_code == SCHA_ERR_NODE) {
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_INVALID_NODE, nodename);
			temp_res.err_code = SCHA_ERR_NOERR;
			free(nodename);
			nodename = NULL;
			continue;
		} else if (temp_res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			goto finish; //lint !e801
		}
		lni = ln->getLni();

		if (ns.containLni(lni)) {
			ucmm_print("idl_scswitch_onoff : ", NOGET(
			    "duplicate node <%d>\n"), lni);
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_DUPNODE, nodename);
			free(nodename);
			nodename = NULL;
			// continue instead of bailing out
			continue;
		}
		ns.addLni(lni);
		free(nodename);
		nodename = NULL;
	}

	//
	// check for invalid node entry. If ns is empty and res.err_code is
	// set it means that user has entered invalid node(s). None of the
	// nodes specified is valid.
	//
	if ((ns.isEmpty()) && (res.err_code != SCHA_ERR_NOERR)) {
		goto finish; //lint !e801
	}


	opcode = scswoo_args.action;

	// check for invalid or duplicate Rs.
	temp_res = prune_operand_list(scswoo_args.R_names, outlist, B_TRUE);

	if (temp_res.err_code != SCHA_ERR_NOERR) {
		smart_overlay(res, temp_res);
		temp_res.err_code = SCHA_ERR_NOERR;
	}

	(void) transfer_message(&res, &temp_res);

	rnames = convert_value_string_array(outlist);

	ngzone_check(scswoo_args.idlstr_zonename,
	    globalflag);

	for (i = 0; i < outlist.length(); i++) {

		r_ptr = rname_to_r((rglist_t *)NULL, outlist[i]);
		rg_ptr = r_ptr->rl_rg;
		CL_PANIC(rg_ptr != NULL);
		rg_name = rg_ptr->rgl_ccr->rg_name;
		CL_PANIC(rg_name != NULL);
		r_name = r_ptr->rl_ccrdata->r_name;
		rg_ns = get_logical_nodeset_from_nodelist(
		    rg_ptr->rgl_ccr->rg_nodelist);
		flag = B_FALSE;

		if (!globalflag) {
			nodelist_check(rg_ptr, scswoo_args.nodeid,
			    scswoo_args.idlstr_zonename, nodelist_contains);

			if (!nodelist_contains) {
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = SCHA_ERR_ACCESS;
				rgm_format_errmsg(&res,
				    REM_R_NODELIST_NGZONE,
				    (const char *)outlist[i],
				    (const char *)scswoo_args.idlstr_zonename,
				    rg_ptr->rgl_ccr->rg_name);
				namelist_delete_name(&rnames, r_name);
				continue;
			}
		}

		if (ns.isEmpty()) {
			operand_ns = *rg_ns;
		} else {
			operand_ns = ns & *rg_ns;
			if (operand_ns.isEmpty()) {
				ucmm_print("idl_scswitch_onoff", NOGET("None "
				    "of the specified nodes are in the "
				    "resource group's nodelist\n"));
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = SCHA_ERR_NODE;
				rgm_format_errmsg(&res,
				    REM_NODES_NOT_IN_NODELIST, r_name,
				    rg_name);
				namelist_delete_name(&rnames, r_name);
				continue;
			}
		}

		//
		// Skip the R whose relevant flag has already been set.
		// If flag is B_FALSE, we continue to the next resource.
		//
		switch (opcode) {
		case rgm::RGM_OP_ENABLE_R:
			lni = 0;
			while ((lni = operand_ns.nextLniSet(lni + 1)) != 0) {
				if (((rgm_switch_t *)r_ptr->rl_ccrdata->
				    r_onoff_switch)->r_switch[lni] !=
				    SCHA_SWITCH_ENABLED) {
					flag = B_TRUE;
					break;
				}
			}
			if (flag)
				break;
			ucmm_print("idl_scswitch_onoff", NOGET("Resource's "
			    "onoff_switch has already been ENABLED\n"));
			namelist_delete_name(&rnames, r_name);
			continue;
		case rgm::RGM_OP_DISABLE_R:
			lni = 0;
			while ((lni = operand_ns.nextLniSet(lni + 1)) != 0) {
				if (((rgm_switch_t *)r_ptr->rl_ccrdata->
				    r_onoff_switch)->r_switch[lni] !=
				    SCHA_SWITCH_DISABLED) {
					flag = B_TRUE;
					break;
				}
			}
			if (flag)
				break;
			ucmm_print("idl_scswitch_onoff", NOGET("Resource's "
			    "onoff_switch has already been DISABLED\n"));
			namelist_delete_name(&rnames, r_name);
			continue;
		case rgm::RGM_OP_ENABLE_M_R:
			//
			// If R is not monitorable, skip it.
			//
			if (!is_r_monitorable(r_ptr)) {
				ucmm_print("idl_scswitch_onoff", NOGET(
				    "monitoring methods not defined for the "
				    "resource, while trying to enable "
				    "monitoring."));
				//
				// Warning message only; do not set error code
				//
				rgm_format_errmsg(&res, RWM_MON_NOT_DEF,
				    r_name);
				namelist_delete_name(&rnames, r_name);
				continue;
			}

			lni = 0;
			while ((lni = operand_ns.nextLniSet(lni + 1)) != 0) {
				if (((rgm_switch_t *)r_ptr->rl_ccrdata->
				    r_monitored_switch)->r_switch[lni] !=
				    SCHA_SWITCH_ENABLED) {
					flag = B_TRUE;
					break;
				}
			}
			if (flag)
				break;
			ucmm_print("idl_scswitch_onoff", NOGET("Resource's "
			    "monitored_switch has already been ENABLED\n"));
			rgm_format_errmsg(&res, RWM_MON_RESTART_DEF,
				r_name, r_name, r_name);
			namelist_delete_name(&rnames, r_name);
			continue;
		case rgm::RGM_OP_DISABLE_M_R:
			lni = 0;
			while ((lni = operand_ns.nextLniSet(lni + 1)) != 0) {
				if (((rgm_switch_t *)r_ptr->rl_ccrdata->
				    r_monitored_switch)->r_switch[lni] !=
				    SCHA_SWITCH_DISABLED) {
					flag = B_TRUE;
					break;
				}
			}
			if (flag)
				break;
			ucmm_print("idl_scswitch_onoff", NOGET("Resource's "
			    "monitored_switch has already been DISABLED\n"));
			namelist_delete_name(&rnames, r_name);
			continue;
		case rgm::RGM_OP_CREATE:
		case rgm::RGM_OP_DELETE:
		case rgm::RGM_OP_MANAGE:
		case rgm::RGM_OP_UNMANAGE:
		case rgm::RGM_OP_UPDATE:
		case rgm::RGM_OP_SYNCUP:
		case rgm::RGM_OP_FORCE_ENABLE_R:
		case rgm::RGM_OP_FORCE_DISABLE_R:
			ucmm_print("idl_scswitch_onoff", NOGET("Invalid "
			    "opcode <%d>\n"), opcode);
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INTERNAL);
			goto finish; //lint !e801
		}

		//
		// Check whether the corresponding RG is busy. If the RG is
		// running BOOT methods or in ERROR_STOP_FAILED, then the
		// enable/disable is not permitted.
		//
		// We also check whether the RG is ON_PENDING_R_RESTART and we
		// do not permit an enable/disable to be initiated. Note,
		// however, that it is possible for a resource restart to be
		// triggered by a restart-dependency in a resource group that
		// is ON_PENDING_DISABLED or ON_PENDING_MON_DISABLED.  In that
		// case, the restarts and disables proceed simultaneously in
		// the state machine. See process_resource and rgm_run_state.
		//
		// xxxx If we implement force-disable (bugids 4274541 and
		// xxxx 4407447) then we will have to permit the force-disable
		// xxxx to proceed in a wider variety of RG states, including
		// xxxx some busy states (for example, PENDING_ONLINE or
		// xxxx ON_PENDING_R_RESTART).  If the forced disable is
		// xxxx performed on a resource in a group that is
		// xxxx ON_PENDING_R_RESTART, the RG state should be changed
		// xxxx to ON_PENDING_DISABLED (which also allows restarts
		// xxxx to proceed in the state machine).
		// xxxx
		// xxxx The state machine will probably require additional
		// xxxx modifications to deal with forced disables in RGs that
		// xxxx are PENDING_ONLINE, PENDING_ON_STARTED, PENDING_OFFLINE,
		// xxxx etc.
		//
		if (!in_endish_rg_state(rg_ptr, &pending_boot_ns,
		    &stop_failed_ns, NULL, &r_restart_ns)) {
			ucmm_print("idl_scswitch_onoff", NOGET("resource group"
			    "<%s> is not in endish state."), rg_name);
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = SCHA_ERR_RGRECONF;
			rgm_format_errmsg(&res, REM_RS_IN_BUSY_RG, r_name,
			    rg_name);
			// continue instead of bailing out.
			namelist_delete_name(&rnames, r_name);
			continue;
		}

		//
		// If the RG is BUSY or STOP_FAILED on operand_ns, we continue
		// to the next resource.
		//
		pending_boot_ns &= operand_ns;
		r_restart_ns &= operand_ns;
		stop_failed_ns &= operand_ns;
		if (!pending_boot_ns.isEmpty() || !r_restart_ns.isEmpty()) {
			ucmm_print("idl_scswitch_onoff", NOGET("resource group"
			    "<%s> is not in endish state."), rg_name);
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = SCHA_ERR_RGRECONF;
			rgm_format_errmsg(&res, REM_RS_IN_BUSY_RG, r_name,
			    rg_name);
			// continue instead of bailing out.
			namelist_delete_name(&rnames, r_name);
			continue;
		} else if (!stop_failed_ns.isEmpty()) {
			(void) sc_syslog_msg_initialize(&handle,
				SC_SYSLOG_RGM_RG_TAG, rg_name);
			(void) sc_syslog_msg_log(handle, LOG_ERR,
				MESSAGE, "Resource group <%s> requires"
				" operator attention due to STOP "
				"failure", rg_name);
			sc_syslog_msg_done(&handle);
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = SCHA_ERR_STOPFAILED;
			rgm_format_errmsg(&res,
				REM_RS_IN_STOPFAILED_RG, r_name, rg_name);
			// continue instead of bailing out.
			namelist_delete_name(&rnames, r_name);
			continue;
		}

		//
		// "system" property checks:
		//
		//	Resources and associated monitors contained in a
		//	"system" RG can't be enabled or disabled (no need
		//	to check opcode for specifics).
		//
		if (rg_ptr->rgl_ccr->rg_system) {
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = SCHA_ERR_ACCESS;
			rgm_format_errmsg(&res, REM_RS_IN_SYSTEM_RG, r_name,
			    rg_name);
			ucmm_print("RGM", NOGET("idl_scswitch_onoff: R <%s> "
			    "is contained in RG <%s> whose RG_SYSTEM property "
			    "is TRUE\n"), r_ptr->rl_ccrdata->r_name, rg_name);
			// continue instead of bailing out.
			namelist_delete_name(&rnames, r_name);
			continue;
		}

		if (is_recursive) {
			switch (opcode) {
			case rgm::RGM_OP_ENABLE_R :
				//
				// If the recursive flag is set, enable the
				// dependee Rs. These resources get added to
				// rnames and length is incremented.
				//
				if (check_enable_dependees(r_ptr->
				    rl_ccrdata->r_dependencies.dp_strong,
				    &rnames, outlist) == SCHA_ERR_NOMEM ||
				    check_enable_dependees(r_ptr->
				    rl_ccrdata->r_dependencies.dp_restart,
				    &rnames, outlist) == SCHA_ERR_NOMEM ||
				    check_enable_dependees(r_ptr->
				    rl_ccrdata->r_dependencies.dp_weak,
				    &rnames, outlist) == SCHA_ERR_NOMEM ||
				    check_enable_dependees(r_ptr->
				    rl_ccrdata->r_dependencies.
				    dp_offline_restart, &rnames, outlist) ==
				    SCHA_ERR_NOMEM) {
					res.err_code = SCHA_ERR_NOMEM;
					goto finish;
				}
				break;

			case rgm::RGM_OP_DISABLE_R :
				//
				// If the recursive flag is set, disable the
				// dependent Rs. These resources get added to
				// rnames and length is incremented.
				//
				if (check_disable_dependents(r_ptr->
				    rl_dependents.dp_strong, &rnames,
				    outlist) == SCHA_ERR_NOMEM ||
				    check_disable_dependents(r_ptr->
				    rl_dependents.dp_restart, &rnames,
				    outlist) == SCHA_ERR_NOMEM ||
				    check_disable_dependents(r_ptr->
				    rl_dependents.dp_weak, &rnames, outlist) ==
				    SCHA_ERR_NOMEM ||
				    check_disable_dependents(r_ptr->
				    rl_dependents.dp_offline_restart, &rnames,
				    outlist) == SCHA_ERR_NOMEM) {
					res.err_code = SCHA_ERR_NOMEM;
					goto finish;
				}
				break;

			case rgm::RGM_OP_ENABLE_M_R :
			case rgm::RGM_OP_DISABLE_M_R :
			case rgm::RGM_OP_CREATE:
			case rgm::RGM_OP_DELETE:
			case rgm::RGM_OP_MANAGE:
			case rgm::RGM_OP_UNMANAGE:
			case rgm::RGM_OP_UPDATE:
			case rgm::RGM_OP_SYNCUP:
			case rgm::RGM_OP_FORCE_ENABLE_R:
			case rgm::RGM_OP_FORCE_DISABLE_R:
				ucmm_print("idl_scswitch_onoff", NOGET(
					"Invalid opcode <%d>\n"), opcode);
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_INTERNAL);
				goto finish; //lint !e801
			}
		}
	}

	// Order the resource array based on dependency
	namelist_to_seq(rnames, rslist);
	my_rss = convert_seq_to_rlistp(rslist);
	switch_order_rsarray(my_rss);
	if (opcode == rgm::RGM_OP_DISABLE_R) {
		reverse_switch_order_array((void **)my_rss);
	}

	for (i = 0; i < rslist.length(); i++) {
		//
		// When processing enable/disable function, we will process
		// Rs one by one. So the intention will not meet partial failure
		// when president dies.
		//

		r_ptr = my_rss[i];
		rg_ptr = r_ptr->rl_rg;
		CL_PANIC(rg_ptr != NULL);
		rg_name = rg_ptr->rgl_ccr->rg_name;
		CL_PANIC(rg_name != NULL);
		r_name = r_ptr->rl_ccrdata->r_name;
		rg_ns = get_logical_nodeset_from_nodelist(
		    rg_ptr->rgl_ccr->rg_nodelist);

		if (ns.isEmpty()) {
			operand_ns = *rg_ns;
		} else {
			operand_ns = ns & *rg_ns;
			if (operand_ns.isEmpty())
				continue;
		}

#ifdef DEBUG
		//
		// The following "sanity check" could probably be gotten
		// rid of by now.  It hasn't been seen to fail anytime.
		// If we some day implement a force-disable then the following
		// check is too stringent because we might find resources that
		// are disabled but still switching offline.

		//
		// Perform a sanity check. If the R is disabled but is in a
		// state other than OFFLINE or UNINITED on specified node, we
		// will return error.
		//

		lni = 0;
		while ((lni = operand_ns.nextLniSet(lni + 1)) != 0) {
			if ((((rgm_switch_t *)r_ptr->rl_ccrdata->
			    r_onoff_switch)->r_switch[lni] ==
			    SCHA_SWITCH_DISABLED) && r_ptr->rl_xstate[lni].
			    rx_state != rgm::R_OFFLINE && r_ptr->
			    rl_xstate[lni].rx_state != rgm::R_UNINITED) {
				(void) sc_syslog_msg_initialize(&handle,
					SC_SYSLOG_RGM_RS_TAG, r_name);
				//
				// SCMSGS
				// @explanation
				// While attempting to execute an
				// operator-requested enable or disable of a
				// resource, the rgmd has found the indicated
				// resource to have its Onoff_switch property
				// set to DISABLED, yet the resource is not
				// offline. This suggests corruption of the
				// RGM's internal data and will cause the
				// enable or disable action to fail.
				// @user_action
				// This may indicate an internal error or bug
				// in the rgmd. Contact your authorized Sun
				// service provider for assistance in
				// diagnosing and correcting the problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
					MESSAGE, "resource <%s> is disabled "
					"but not offline", r_name);
				sc_syslog_msg_done(&handle);
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = SCHA_ERR_INTERNAL;
				rgm_format_errmsg(&res, REM_INTERNAL);
				goto finish; //lint !e801
			}
		}
#endif	// DEBUG

		switch (opcode) {
		case rgm::RGM_OP_ENABLE_R:

			(void) latch_intention(rgm::RGM_ENT_RS, opcode,
			    r_name, &emptyNodeset, 0);

			// Set flag into CCR.
			if ((temp_res = rgmcnfg_set_onoffswitch(r_name,
			    rg_name, SCHA_SWITCH_ENABLED,
			    &operand_ns)).err_code != SCHA_ERR_NOERR) {

				res.err_code = temp_res.err_code;
				// CCR update failed. Stop further processing
				// and return early.
				ucmm_print("idl_scswitch_onoff", NOGET(
				    "CCR update of on/off flag failed "
				    "for resource %s.\n"), r_name);
				(void) unlatch_intention();
				goto finish; //lint !e801
			}

			// Log resource PROPERTY_CHANGED event for SC Manager.
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RS_TAG, r_name);
			//
			// SCMSGS
			// @explanation
			// This is a notification from the rgmd that the
			// operator has enabled a resource. This message can
			// be used by system monitoring tools.
			// @user_action
			// This is an informational message; no user action is
			// needed.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    PROPERTY_CHANGED, SYSTEXT("resource %s enabled."),
			    r_name);

			// post an event
#if SOL_VERSION >= __s10
			(void) sc_publish_zc_event(ZONE,
				ESC_CLUSTER_R_CONFIG_CHANGE,
				CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
				CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				CL_RG_NAME, SE_DATA_TYPE_STRING,
				rg_name, CL_R_NAME, SE_DATA_TYPE_STRING,
				r_name, CL_CONFIG_ACTION,
				SE_DATA_TYPE_UINT32,
				CL_EVENT_CONFIG_ENABLED, NULL);
#else
			(void) sc_publish_event(ESC_CLUSTER_R_CONFIG_CHANGE,
				CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
				CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				CL_RG_NAME, SE_DATA_TYPE_STRING,
				rg_name, CL_R_NAME, SE_DATA_TYPE_STRING,
				r_name, CL_CONFIG_ACTION,
				SE_DATA_TYPE_UINT32,
				CL_EVENT_CONFIG_ENABLED, NULL);
#endif
			sc_syslog_msg_done(&handle);
			//
			// Print a success message that R
			// state has been changed.
			//
			if (verbose_flag)
				rgm_format_successmsg(&res, RWM_R_ENABLED,
				    r_name);
			break;

		case rgm::RGM_OP_DISABLE_R:
			temp_res = is_strong_dependent_online(r_ptr,
			    &operand_ns);
			if (temp_res.err_code != SCHA_ERR_NOERR) {
				res.err_code = temp_res.err_code;
				ucmm_print("idl_scswitch_onoff", NOGET("goto "
				    "finish: is_strong_dependent_online "
				    "failed"));
				goto finish; //lint !e801
			}
			if (transfer_message(&res, &temp_res) != SCHA_ERR_NOERR)
				goto finish; //lint !e801

			(void) latch_intention(rgm::RGM_ENT_RS, opcode,
			    r_name, &emptyNodeset, 0);

			// Set flag into CCR.
			if ((temp_res = rgmcnfg_set_onoffswitch(r_name,
			    rg_name, SCHA_SWITCH_DISABLED,
			    &operand_ns)).err_code != SCHA_ERR_NOERR) {

				res.err_code = temp_res.err_code;
				// CCR update failed. Stop further processing
				// and return early.
				ucmm_print("idl_scswitch_onoff", NOGET(
				    "CCR update of on/off flag failed "
				    "for resource %s.\n"), r_name);
				(void) unlatch_intention();
				goto finish; //lint !e801

			}

			// Log resource PROPERTY_CHANGED event for SC Manager.
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RS_TAG, r_name);
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    PROPERTY_CHANGED, SYSTEXT("resource %s disabled."),
			    r_name);

			// post an event
#if SOL_VERSION >= __s10
			(void) sc_publish_zc_event(ZONE,
				ESC_CLUSTER_R_CONFIG_CHANGE,
				CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
				CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
				CL_R_NAME, SE_DATA_TYPE_STRING, r_name,
				CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
				CL_EVENT_CONFIG_DISABLED, NULL);
#else
			(void) sc_publish_event(ESC_CLUSTER_R_CONFIG_CHANGE,
				CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
				CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
				CL_R_NAME, SE_DATA_TYPE_STRING, r_name,
				CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
				CL_EVENT_CONFIG_DISABLED, NULL);
#endif
			sc_syslog_msg_done(&handle);
			//
			// Print a success message that R state
			// has been changed.
			//
			if (verbose_flag)
				rgm_format_successmsg(&res, RWM_R_DISABLED,
				    r_name);

			break;

		case rgm::RGM_OP_ENABLE_M_R:

			(void) latch_intention(rgm::RGM_ENT_RS, opcode,
			    r_name, &emptyNodeset, 0);

			// Set flag into CCR.
			if ((temp_res = rgmcnfg_set_monitoredswitch(r_name,
			    rg_name, SCHA_SWITCH_ENABLED,
			    &operand_ns)).err_code != SCHA_ERR_NOERR) {

				res.err_code = temp_res.err_code;
				// CCR update failed. Stop further processing
				// and return early.
				ucmm_print("idl_scswitch_onoff", NOGET(
				    "CCR update of monitored switch failed "
				    "for resource %s.\n"), r_name);
				(void) unlatch_intention();
				goto finish; //lint !e801
			}

			// Log resource PROPERTY_CHANGED event for SC Manager.
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RS_TAG, r_name);
			//
			// SCMSGS
			// @explanation
			// This is a notification from the rgmd that the
			// operator has enabled monitoring on a resource. This
			// message can be used by system monitoring tools.
			// @user_action
			// This is an informational message; no user action is
			// needed.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    PROPERTY_CHANGED,
			    SYSTEXT("resource %s monitor enabled."), r_name);
			// post an event
#if SOL_VERSION >= __s10
			(void) sc_publish_zc_event(ZONE,
				ESC_CLUSTER_R_CONFIG_CHANGE,
				CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
				CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
				CL_R_NAME, SE_DATA_TYPE_STRING, r_name,
				CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
				CL_EVENT_CONFIG_MON_ENABLED, NULL);
#else
			(void) sc_publish_event(ESC_CLUSTER_R_CONFIG_CHANGE,
				CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
				CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
				CL_R_NAME, SE_DATA_TYPE_STRING, r_name,
				CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
				CL_EVENT_CONFIG_MON_ENABLED, NULL);
#endif
			sc_syslog_msg_done(&handle);

			if (verbose_flag)
				rgm_format_successmsg(&res, RWM_R_MON_ENABLED,
				    r_name);
			break;

		case rgm::RGM_OP_DISABLE_M_R:

			(void) latch_intention(rgm::RGM_ENT_RS, opcode,
			    r_name, &emptyNodeset, 0);

			// Set flag into CCR.
			if ((temp_res = rgmcnfg_set_monitoredswitch(r_name,
			    rg_name, SCHA_SWITCH_DISABLED,
			    &operand_ns)).err_code != SCHA_ERR_NOERR) {

				// CCR update failed. Stop further processing
				// and return early.
				res.err_code = temp_res.err_code;
				ucmm_print("idl_scswitch_onoff", NOGET(
				    "CCR update of monitored switch failed "
				    "for resource %s.\n"), r_name);
				(void) unlatch_intention();
				goto finish; //lint !e801
			}

			// Log resource PROPERTY_CHANGED event for SC Manager.
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RS_TAG, r_name);
			//
			// SCMSGS
			// @explanation
			// This is a notification from the rgmd that the
			// operator has disabled monitoring on a resource.
			// This message can be used by system monitoring
			// tools.
			// @user_action
			// This is an informational message; no user action is
			// needed.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    PROPERTY_CHANGED,
			    SYSTEXT("resource %s monitor disabled."), r_name);
			// post an event
#if SOL_VERSION >= __s10
			(void) sc_publish_zc_event(ZONE,
				ESC_CLUSTER_R_CONFIG_CHANGE,
				CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
				CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				CL_RG_NAME, SE_DATA_TYPE_STRING,
				rg_name, CL_R_NAME, SE_DATA_TYPE_STRING,
				r_name, CL_CONFIG_ACTION,
				SE_DATA_TYPE_UINT32,
				CL_EVENT_CONFIG_MON_DISABLED, NULL);
#else
			(void) sc_publish_event(ESC_CLUSTER_R_CONFIG_CHANGE,
				CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
				CL_EVENT_INIT_SYSTEM, 0, 0, 0,
				CL_RG_NAME, SE_DATA_TYPE_STRING,
				rg_name, CL_R_NAME, SE_DATA_TYPE_STRING,
				r_name, CL_CONFIG_ACTION,
				SE_DATA_TYPE_UINT32,
				CL_EVENT_CONFIG_MON_DISABLED, NULL);
#endif
			sc_syslog_msg_done(&handle);

			if (verbose_flag)
				rgm_format_successmsg(&res, RWM_R_MON_DISABLED,
				    r_name);
			break;

		case rgm::RGM_OP_CREATE:
		case rgm::RGM_OP_DELETE:
		case rgm::RGM_OP_MANAGE:
		case rgm::RGM_OP_UNMANAGE:
		case rgm::RGM_OP_UPDATE:
		case rgm::RGM_OP_SYNCUP:
		case rgm::RGM_OP_FORCE_ENABLE_R:
		case rgm::RGM_OP_FORCE_DISABLE_R:
			ucmm_print("idl_scswitch_onoff",
			    NOGET("Invalid opcode <%d>\n"), opcode);
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INTERNAL);
			goto finish; //lint !e801
		}

		// Notify all nodes to update R and run state machine if needed.
		(void) process_intention();
		//
		// processing the intention causes the resource to be
		// reloaded into memory from the CCR, so r_name is no
		// longer valid. r_ptr is still valid, because only the
		// ccrdata portion of the resource is changed.
		//
		r_name = r_ptr->rl_ccrdata->r_name;

		// Unlatch the intention.
		(void) unlatch_intention();

		//
		// rgnames is a "work list" of all the RGs that are affected
		// by this enable/disable.
		//

		// Add this RG to rgnames and set its rgl_switching flag.
		// First see if RG is already in the list
		if (namelist_exists(rgnames, rg_name)) {
			// RG is already in list-- go on to the next R
			continue;
		}
		// Add the RG to the list
		temp_res = namelist_add_name(&rgnames, rg_name);
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			goto finish; //lint !e801
		}

		// Set the rgl_switching flag on the RG.  Setting it to
		// SW_IN_PROGRESS permits the switch to be "busted"
		rg_ptr->rgl_switching = SW_IN_PROGRESS;
	} // for loop

	// If there is any change to the enable/disable property, we
	// should block the program to wait for the state machine to finish
	// running all related methods.
	if (rgnames != NULL) {

		// flush the latest state changes
		rgm_unlock_state();
		flush_state_change_queue();
		rgm_lock_state();

		//
		// Note that an RG might already be "busted" at this point.
		// This is checked for below.
		//

		// Sleep on slavewait condition variable until all RGs in
		// the list have moved out of relevant intermediate state
		// if the state machine has been triggered.

		// If no methods needed to be launched in
		// process_intention, then the RG state will not be
		// the corresponding intermediate state and there will
		// be no block.
		for (rgnamep = rgnames; rgnamep != NULL;
		    rgnamep = rgnamep->nl_next) {
			scha_err_t busted = SCHA_ERR_NOERR;


			// We will wait for the state machine to finish
			// running all required methods for corresponding
			// action.

			// rgname has been check above, just assert rg_ptr
			// isn't NULL;
			rg_ptr = rgname_to_rg(rgnamep->nl_name);
			CL_PANIC(rg_ptr);

			// Make sure this RG is not already "busted."
			busted = map_busted_err(rg_ptr->rgl_switching);
			if (busted == SCHA_ERR_NOERR) {
				// Wait for switch to complete on slaves
				switch (opcode) {
				case rgm::RGM_OP_ENABLE_R:
				case rgm::RGM_OP_ENABLE_M_R:
					busted = wait_for(rg_ptr,
					    rgnamep->nl_name,
					    rgm::RG_PENDING_ONLINE);
					do_check_for_startfailed = B_TRUE;
					break;
				case rgm::RGM_OP_DISABLE_R:
					busted = wait_for(rg_ptr,
					    rgnamep->nl_name,
					    rgm::RG_ON_PENDING_DISABLED);
					break;
				case rgm::RGM_OP_DISABLE_M_R:
					busted = wait_for(rg_ptr,
					    rgnamep->nl_name,
					    rgm::RG_ON_PENDING_MON_DISABLED);
					break;
				case rgm::RGM_OP_CREATE:
				case rgm::RGM_OP_DELETE:
				case rgm::RGM_OP_MANAGE:
				case rgm::RGM_OP_UNMANAGE:
				case rgm::RGM_OP_UPDATE:
				case rgm::RGM_OP_SYNCUP:
				case rgm::RGM_OP_FORCE_ENABLE_R:
				case rgm::RGM_OP_FORCE_DISABLE_R:
					// These were already ruled out above!
					// Panic the node.
					(void) sc_syslog_msg_initialize(&handle,
					    SC_SYSLOG_RGM_RG_TAG,
					    rgnamep->nl_name);
					//
					// SCMSGS
					// @explanation
					// While attempting to execute an
					// operator-requested enable or
					// disable of a resource, the rgmd has
					// encountered an internal error. This
					// error should not occur. The rgmd
					// will produce a core file and will
					// force the node to halt or reboot to
					// avoid the possibility of data
					// corruption.
					// @user_action
					// Save a copy of the
					// /var/adm/messages files on all
					// nodes, and of the rgmd core file.
					// Contact your authorized Sun service
					// provider for assistance in
					// diagnosing the problem.
					//
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "fatal: scswitch_onoff: invalid "
					    "opcode <%d>", opcode);
					sc_syslog_msg_done(&handle);
					abort();
					// NOTREACHABLE
				}
			}

			if (busted != SCHA_ERR_NOERR) {
				busted_err = busted;
				// Append the appropriate error message
				res.err_code = SCHA_ERR_NOERR;
				append_busted_msg(&res, rg_ptr, busted_err);
				if (res.err_code != SCHA_ERR_NOERR) {
					// NOMEM or INTERNAL error occurred
					goto finish; //lint !e801
				}
				// We will set res.err_code below
			}

			//
			// If operation was "enable", check RG for resources
			// that ended up in START_FAILED or MONITOR_FAILED
			// With its third argument false, the only error code
			// returned by check_starterrs() (besides NOERR)
			// is STARTFAILED.
			//
			if (do_check_for_startfailed) {
				check_starterrs(&temp_res, rg_ptr, B_FALSE);
				if (temp_res.err_code == SCHA_ERR_STARTFAILED) {
					start_err = temp_res.err_code;
					//
					// Reset the error code for now.
					// We will set it back to
					// startfailed below, if no
					// more serious error occurs.
					//
					temp_res.err_code = SCHA_ERR_NOERR;
				}
			}
		} // for each RG
	} // if (rgnames != NULL)

finish:
	//
	// For each RG's switching flag that was changed, reset it to SW_NONE.
	// However, we do not reset SW_EVACUATING.
	//
	for (rgnamep = rgnames; rgnamep != NULL; rgnamep = rgnamep->nl_next) {


		// rgname has been check above, just assert rg_ptr isn't NULL;

		rg_ptr = rgname_to_rg(rgnamep->nl_name);
		CL_PANIC(rg_ptr != NULL);

		//
		// Check if any rgs are in PENDING_ONLINE_BLOCKED.
		// We need to do this at the end to get the most accurate
		// information possible.
		//
		if (do_check_for_startfailed) {
			check_pending_online_blocked(&res, rg_ptr);
		}
		if (rg_ptr->rgl_switching != SW_EVACUATING)
			rg_ptr->rgl_switching = SW_NONE;
	}

	//
	// If multiple errors occurred, the stderr message will contain
	// the detailed messages, but we can only return one exit code.
	// We will try to return the exit code for the most severe error
	// that occurred:
	// If a fatal error (such as invalid arguments or NOMEM) occurred,
	// we will return the exit code for that.  Otherwise, if a "busted
	// switch" occurred, we will return the exit code for the last one
	// of those that we detected.
	// If neither of the above errors occurred but a "start failure"
	// occurred (i.e., a resource ended up in START_FAILED state) then
	// we will return an exit code for that.
	//
	// If none of the above errors occurred but a "monitor failure"
	// occurred (i.e., the enabled resources went online but a
	// resource's monitor_start method failed) then we will exit 0
	// but will still write a warning message to stderr.
	//
	// xxxx Currently scswitch ignores the exit code (other than whether
	// it's zero or non-zero) but we will nonetheless try to return
	// the most meaningful code in case it is used in the future.
	//
	if (res.err_code == SCHA_ERR_NOERR) {
		if (busted_err != SCHA_ERR_NOERR) {
			res.err_code = busted_err;
		} else if (start_err != SCHA_ERR_NOERR) {
			res.err_code = start_err;
		}
	}

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);
	free(temp_res.err_msg);
	if (my_rss)
		free(my_rss);
	rgm_free_nlist(rgnames);
	rgm_free_nlist(rnames);
	result = retval;
	rgm_unlock_state();

}

//
// the function is called when disabling a resource to generate warnings if any
// strong dependent resource is online. If the dependency type is of LOCAL_NODE
// type, Warning is produced for dependent resources only if that resource is
// online on any node in ns where dependee resource is being disabled.
//
static scha_errmsg_t
is_strong_dependent_online(rlist_p_t r_ptr, LogicalNodeset *ns)
{
	std::list<char *> rnamelist;
	std::list<char *>:: iterator it;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *rnamestring = NULL;
	uint_t len = 0;

	//
	// If this resource is a shared adddress resource,
	// do not check for any dependents since they all
	// will be brought offline anywa.
	//
	if (r_ptr->rl_ccrtype->rt_sysdeftype ==
	    SYST_SHARED_ADDRESS) {
		//
		// simply return.
		//
		return (res);
	}

	is_dependent_online(r_ptr, ns,
	    r_ptr->rl_dependents.dp_strong, &rnamelist, DEPS_STRONG);
	is_dependent_online(r_ptr, ns,
	    r_ptr->rl_dependents.dp_restart, &rnamelist, DEPS_RESTART);

	is_dependent_online(r_ptr, ns,
	    r_ptr->rl_ccrdata->r_inter_cluster_dependents.dp_strong,
	    &rnamelist, DEPS_STRONG);
	is_dependent_online(r_ptr, ns,
	    r_ptr->rl_ccrdata->r_inter_cluster_dependents.dp_restart,
	    &rnamelist, DEPS_RESTART);

	for (len = 0, it = rnamelist.begin(); it != rnamelist.end(); it++) {
		len = len + strlen(*it) + 1;
	}

	if (len == 0)
		return (res);

	rnamestring = (char *)malloc_nocheck(len + 1);
	if (rnamestring == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}
	rnamestring[0] = '\0';
	for (it = rnamelist.begin(); it != rnamelist.end(); it++) {
		strcat(rnamestring, *it);
		strcat(rnamestring, ",");
	}
	rnamestring[strlen(rnamestring) - 1] = '\0'; // wipe out last comma
	rgm_format_errmsg(&res, RWM_DISABLE_DEP_ONLINE,
	    r_ptr->rl_ccrdata->r_name, rnamestring);
	free(rnamestring);
	return (res);
}


//
// the function is called by is_strong_dependent_online() to look at all
// resources in a dependency list, and add its name to the rnamelist if the
// resource is online and is not specified as one of the resource to be
// disabled in the clrs disable / scswitch -nj command list. The dependent
// resources which are being disabled by the same command, would already be
// disabled at this point in the code -- due to the calls to
// switch_order_rsarray() and reverse_switch_order_array().
// The function uses the nodeset and the dependency type to filter which
// which resources should be added to the list.
// the resource name pointers added point to CCR structure, and hence needn't
// be freed. Also this means that they are validn only under RGM lock.
//
static void
is_dependent_online(rlist_p_t r_ptr, LogicalNodeset *ns,
    rdeplist_t *depl, std::list<char *> *rnamelist, deptype_t deps_kind) {
	rgm::lni_t lni;
	rlist_p_t dep_r_ptr;
	boolean_t online;
	nodeidlist_t *nl;
	boolean_t is_local_node = B_FALSE;
	LogicalNodeset	emptyNodeset;
	for (; depl != NULL; depl = depl->rl_next) {

		// Check the status of remote resource.
		if (is_enhanced_naming_format_used(depl->rl_name)) {
#if SOL_VERSION >= __s10
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "is_dependent_online "
				    "Running at too low a version:R <%s> "
				    "has dependency on R <%s> "
				    "which is in not in local cluster\n"),
				    r_ptr->rl_ccrdata->r_name, depl->rl_name);
				continue;
			}
			if (get_remote_r_state_helper(r_ptr, depl->rl_name,
			    depl->locality_type, deps_kind, emptyNodeset,
			    B_TRUE, NULL)) {
				rnamelist->push_back(depl->rl_name);
			}
#endif
		} else {
			CL_PANIC((dep_r_ptr = rname_to_r(NULL,
			    depl->rl_name)) != NULL);
			check_dep_local(r_ptr, dep_r_ptr, depl->locality_type,
			    &is_local_node, NULL);
			online = B_FALSE;
			for (nl = dep_r_ptr->rl_rg->rgl_ccr->rg_nodelist;
			    nl != NULL; nl = nl->nl_next) {
				lni = nl->nl_lni;
				if ((is_r_online(dep_r_ptr->rl_xstate[lni].
				    rx_state) || is_r_starting(
				    dep_r_ptr->rl_xstate[lni].rx_state)) &&
				    (((rgm_switch_t *)dep_r_ptr->rl_ccrdata
				    ->r_onoff_switch)->r_switch[lni] ==
				    SCHA_SWITCH_ENABLED)) {
					online = B_TRUE;
					if (ns->containLni(lni))
						break;
				}
			}
			if (nl != NULL || (online && !is_local_node))
				rnamelist->push_back(
				    dep_r_ptr->rl_ccrdata->r_name);
		}
	}
}

#endif // !EUROPA_FARM

//
// Extract various components from a fed tag.
// Caller doesn't need to free any of the pointers, however str gets modified.
// str is a fed_tag of the form:
// "[zonename.]rg_name.rs_name.method_id" ([%s.]%s.%s.%d).
//
void parse_fed_tag(char *str, char **z_str, char **rg_str, char **rs_str,
    char **meth_str, method_t *mt) {
	char *method_str, *resource_str, *resourcegroup_str, *zone_str;

	method_str = strrchr(str, '.');
	*method_str = '\0';
	method_str++;

	resource_str = strrchr(str, '.');
	*resource_str = '\0';
	resource_str++;
	resourcegroup_str = strrchr(str, '.');
	if (resourcegroup_str == NULL) {
		resourcegroup_str = str;
		zone_str = NULL;
	} else {
		*resourcegroup_str = '\0';
		resourcegroup_str++;
		zone_str = str;
	}
	if (meth_str != NULL)
		*meth_str = method_str;
	if (mt != NULL)
		*mt = (method_t)atoi(method_str);
	if (rs_str != NULL)
		*rs_str = resource_str;
	if (rg_str != NULL)
		*rg_str = resourcegroup_str;
	if (z_str != NULL)
		*z_str = zone_str;
}

//
// Following two functions perform dependency checking for resource.
// They check all three explicit dependencies (which are defined in
// r->r_dependencies.dp_strong, r->r_dependencies.dp_weak,
// r->r_dependencies.dp_restart and r->r_dependencies.dp_offline_restart)
// and implicit dependencies (of non-network-address Rs on network address Rs).
//

//
// depends_on_r(rlist_p_t r_ptr)
//
// This functions checks whether the resource r_ptr has a dependency (either
// explicit or implicit) on any dependee resource with net-relative methods.
//
// Returns B_TRUE if the resource has a dependency (either explicit or implicit)
// on a resource in the same resource group with a registered Prenet_start
// or Postnet_stop method; otherwise returns B_FALSE.
//
// Calls subroutine check_dependees() to check each explicit dependency list
//
boolean_t
depends_on_r(rlist_p_t r_ptr)
{
	rlist_p_t	rp;
	rglist_p_t	rgp;

	// Check explicit strong dependencies first.
	if (check_dependees(r_ptr, r_ptr->rl_ccrdata->r_dependencies.dp_strong,
	    B_FALSE)) {
		return (B_TRUE);
	}

	// Check explicit weak dependencies.
	if (check_dependees(r_ptr, r_ptr->rl_ccrdata->r_dependencies.dp_weak,
	    B_TRUE)) {
		return (B_TRUE);
	}

	// Now check explicit restart dependencies.
	if (check_dependees(r_ptr, r_ptr->rl_ccrdata->r_dependencies.dp_restart,
	    B_FALSE)) {
		return (B_TRUE);
	}
	// Check for explicit offline-restart dependencies.
	if (check_dependees(r_ptr, r_ptr->rl_ccrdata->
	    r_dependencies.dp_offline_restart,
	    B_FALSE)) {
		return (B_TRUE);
	}


	//
	// If the RG has rg_impl_net_depend set FALSE, or if
	// R is loghost/sharedaddr type R, we don't need to do following
	// implicit dependencies check.
	//
	rgp = r_ptr->rl_rg;
	if (!rgp->rgl_ccr->rg_impl_net_depend ||
	    (r_ptr->rl_ccrtype->rt_sysdeftype == SYST_LOGICAL_HOSTNAME) ||
	    (r_ptr->rl_ccrtype->rt_sysdeftype == SYST_SHARED_ADDRESS)) {
		return (B_FALSE);
	}
	//
	// Do implicit checking.  In a RG with rg_impl_net_depend set,
	// non-network-address resources implicitly depend on network
	// address resources. Check whether there is any network address
	// R which meets the condition.
	//
	for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		if (rp->rl_ccrtype->rt_sysdeftype != SYST_LOGICAL_HOSTNAME &&
		    rp->rl_ccrtype->rt_sysdeftype != SYST_SHARED_ADDRESS) {
			continue;
		}

		// rp is a network address resource, so it has
		// net-relative methods (Prenet_start, Postnet_stop)
		return (B_TRUE);
	}
	return (B_FALSE);
}

//
// check_dependees
//
// This is a subroutine for depends_on_r.  It traverses the list of explicit
// dependee resources 'deps' and returns B_TRUE if any of them meets the
// following condition:
//  - check for dependees in the same RG as the resource 'r_ptr'
//    that have net-relative methods.
// If none of the dependees meets the above condition, return B_FALSE.
//
static boolean_t
check_dependees(rlist_p_t r_ptr, rdeplist_t *deps, boolean_t is_weak_deps)
{
	rlist_p_t	rp;
	rdeplist_t	*p;
//	rgm::lni_t lni;

	for (p = deps; p != NULL; p = p->rl_next) {
		if (is_enhanced_naming_format_used(p->rl_name))
			continue;
		rp = rname_to_r(NULL, p->rl_name);
		CL_PANIC(rp != NULL);
		//
		// dependee must be in the same RG as the dependent.
		//
		if (r_ptr->rl_rg != rp->rl_rg)
			continue;

		if (has_net_relative_methods(rp)) {
			ucmm_print("RGM", NOGET(
			    "R <%s> depends on net-relative R <%s>\n"),
			    r_ptr->rl_ccrdata->r_name,
			    rp->rl_ccrdata->r_name);
			return (B_TRUE);
		}
	}

	return (B_FALSE);
}

#ifndef EUROPA_FARM

//
// has_man_dependents
//
// Returns TRUE if 'rg' has any managed dependents which do not appear
// in 'rglistp'.
// Otherwise returns FALSE.
// Searches the non failed RG list.
// Called by idl_offline_to_unmanaged() when determining
// whether it's OK to move an RG from managed to unmanaged.
// It's not OK to move an RG to unmanaged if it has managed dependents.
//
// Sets the error code of the 'scha_err' argument.
// Caller should check this error code before using the returned Boolean value.
//
static boolean_t
has_man_dependents(scha_errmsg_t *scha_err, rglist_p_t rg, rglist_p_t *rglistp,
    namelist_t *failed_op_list)
{
	rglist_p_t rgp;

	scha_err->err_code = SCHA_ERR_NOERR;
	for (rgp = Rgm_state->rgm_rglist; rgp != NULL; rgp = rgp->rgl_next) {
		// skip failed Rgs
		if (namelist_exists(failed_op_list, rgp->rgl_ccr->rg_name)) {
			continue;
		}

		// if 'rgp' is managed and depends on 'rg', return true
		boolean_t is_dep = is_rg1_dep_on_rg2(scha_err, rgp->rgl_ccr,
		    rg->rgl_ccr);
		if (scha_err->err_code != SCHA_ERR_NOERR) {
			// error is NOMEM or internal
			break;
		}
		if (is_dep && !rgp->rgl_ccr->rg_unmanaged &&
		    !is_rg_in_list(rgp->rgl_ccr->rg_name, rglistp))
			return (B_TRUE);
	}

	return (B_FALSE);
}

//
// check_online_dependents
//
// Check if rglistp[i] has any dependent that is online on one or more nodes
// and does not itself appear in rglistp.
// The RG dependencies are ignored if the dependee is a "single node" RG
// (i.e., only has one node defined in its nodelist) and rglistp[i] is
// being brought offline on a different node than the dependent.
// Searches the entire RG list.
// Called by idl_scswitch_primaries() when determining
// whether it's OK to switch an RG offline on all nodes.
// It's not OK if it has an online dependent that is not also being
// switched offline.
//
// If any online dependents are found then any error/warning message that
// was previously set in res->err_msg before and during the execution of
// this function, will be cleared, to make way for the REM_RG_DEP_ONLINE
// error message.
//
// Must be run on the president. Caller holds the rgm state lock.
//
static void
check_online_dependents(scha_errmsg_t *res, rglist_p_t *rglistp, int i)
{
	rglist_p_t rgp;
	rgm::lni_t lni = 0;
	char *dep_rg_name = NULL;


	//
	// If rglistp[i] is a "single node" RG, obtain that single lni
	//
	if (rglistp[i]->rgl_ccr->rg_nodelist->nl_next == NULL) {
		lni = rglistp[i]->rgl_ccr->rg_nodelist->nl_lni;
	}

	for (rgp = Rgm_state->rgm_rglist; rgp != NULL; rgp = rgp->rgl_next) {
		// Does rgp depend on rglistp[i]?
		boolean_t is_dep = is_rg1_dep_on_rg2(res, rgp->rgl_ccr,
		    rglistp[i]->rgl_ccr);
		if (res->err_code != SCHA_ERR_NOERR) {
			goto alldone; //lint !e801
		}
		if (!is_dep) {
			continue;
		}

		// Is rgp itself being switched offline?
		if (is_rg_in_list(rgp->rgl_ccr->rg_name, rglistp)) {
			continue;
		}

		// Is rgp an online dependent of rglistp[i]?
		if (is_rg_online(rgp)) {
			//
			// Is rglistp[i] is a "single node" RG ?
			// If not, add rgp name to dependent list.
			// else, make sure rgp is not ONLINE on the same
			// node where rglistp[i] is to be brought offline.
			//
			if (rglistp[i]->rgl_ccr->rg_nodelist->nl_next != NULL) {
				// rg is not "single node"
				add_string_to_list(res,
				    rgp->rgl_ccr->rg_name, &dep_rg_name);
				if (res->err_code != SCHA_ERR_NOERR) {
					goto alldone; //lint !e801
				}
			} else {
				// rg is "single node"
				//
				// Check to see if rgp is ONLINE on the same
				// node as rglistp[i].
				// If so we cannot offline it, otherwise print a
				// notice that there is a dependency but it's
				// not being enforced.
				//

				// We set lni from the nodelist, above
				CL_PANIC(lni != 0);
				if (is_rg_online_on_node(rgp, lni, B_TRUE)) {
					add_string_to_list(res,
					    rgp->rgl_ccr->rg_name,
					    &dep_rg_name);
					if (res->err_code != SCHA_ERR_NOERR) {
						goto alldone; //lint !e801
					}
				} else {
					rgm_format_errmsg(res,
					    RWM_RG_DEP_ONLINE,
					    rgp->rgl_ccr->rg_name,
					    rglistp[i]->rgl_ccr->rg_name);
				}
			}
		}
	}

	if (dep_rg_name != NULL) {
		//
		// As dep_rg_name is not NULL, the switch will fail
		// because of the RG Dependency.
		// Clear the RWM_RG_DEP_OFFLINE Warning Message as a
		// dependent has been found, after RWM_RG_DEP_OFFLINE
		// was set.
		//
		free(res->err_msg);
		res->err_msg = NULL;

		res->err_code = SCHA_ERR_DEPEND;
		rgm_format_errmsg(res, REM_RG_DEP_ONLINE,
		    rglistp[i]->rgl_ccr->rg_name, dep_rg_name);
	}

alldone:
	free(dep_rg_name);
}

//
// find_online_resources
//
// resources is a list of resources to check. Generates in onlines
// a list of all resources in resources that are online anywhere.
//
static void
find_online_resources(scha_errmsg_t *res, rdeplist_t *resource_names,
    rglist_p_t *rglistp, char **onlines)
{
	rlist_p_t rp;
	rdeplist_t *name;

	for (name = resource_names; name != NULL;
	    name = name->rl_next) {
		if (is_enhanced_naming_format_used(name->rl_name)) {
			continue;
#if 0
			if (get_remote_r_state_helper(rp, curr_name->rl_name,
			    curr_name->locality_type, deps_kind, nset,
			    B_TRUE)) {
				add_string_to_list(&res,
				    rp->pl_rg->rgl_ccr->rg_name,
				    onlines);
				if (res.err_code != SCHA_ERR_NOERR) {
					return;
				}
			}
			continue;
#endif
		}

		rp = rname_to_r(NULL, name->rl_name);
		CL_PANIC(rp != NULL);

		// If the RG of this resource is in the rglist,
		// we can skip the rest of the checks, because we
		// know it's coming offline too.
		if (is_rg_in_list(rp->rl_rg->rgl_ccr->rg_name, rglistp)) {
			continue;
		}
		if (is_r_online_anywhere(rp)) {
			add_string_to_list(res,
			    rp->rl_rg->rgl_ccr->rg_name,
			    onlines);
			if (res->err_code != SCHA_ERR_NOERR) {
				return;
			}
		}
	}
}

//
// check_online_resource_dependents
//
// Check if rgp has any resources that have dependents that are online on one
// or more nodes and are not in RGs in rglistp (which are also going offline
// at this time).
//
// If any online dependents are found then any error/warning message that
// was previously set in res->err_msg before and during the execution of
// this function, will be cleared, to make way for the REM_R_DEP_ONLINE
// error message.
//
// We don't need to worry about whether the dependency is local-node or
// not because the RG is going offline entirely (so either type of
// dependency would be violated).
//
// Must be run on the president. Caller holds the rgm state lock.
//
static void
check_online_resource_dependents(scha_errmsg_t *res, rglist_p_t rgp,
    rglist_p_t *rglistp, namelist_t *failed_op_list)
{
	rlist_p_t rp;
	char *dep_r_name = NULL;

	for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {

		// skip failed Rgs
		if (namelist_exists(failed_op_list,
		    rgp->rgl_ccr->rg_name)) {
			continue;
		}

		find_online_resources(res, rp->rl_dependents.dp_strong,
		    rglistp, &dep_r_name);
		if (res->err_code != SCHA_ERR_NOERR) {
			goto alldone; //lint !e801
		}
		find_online_resources(res, rp->rl_dependents.dp_restart,
		    rglistp, &dep_r_name);
		if (res->err_code != SCHA_ERR_NOERR) {
			goto alldone; //lint !e801
		}

		find_online_resources(res,
		    rp->rl_ccrdata->r_inter_cluster_dependents.dp_strong,
		    rglistp, &dep_r_name);
		if (res->err_code != SCHA_ERR_NOERR) {
			goto alldone;
		}
		find_online_resources(res,
		    rp->rl_ccrdata->r_inter_cluster_dependents.dp_restart,
		    rglistp, &dep_r_name);
		if (res->err_code != SCHA_ERR_NOERR) {
			goto alldone;
		}


	}
	if (dep_r_name != NULL) {
		/*
		 * As dep_r_name is not NULL, the switch will fail
		 * because of the Resource Dependency.
		 */
		free(res->err_msg);
		res->err_msg = NULL;

		res->err_code = SCHA_ERR_DEPEND;
		rgm_format_errmsg(res, REM_R_DEP_ONLINE,
		    rgp->rgl_ccr->rg_name, dep_r_name);
	}

alldone:
	free(dep_r_name);
}

//
// check_offline_dependees
//
// Check if rgdepp has an RG_dependency on an RG that is offline on all
// nodes and does not itself appear in rglistp.
// The RG dependencies are ignored for the "single node" case (i.e.
// "single node" RG only has one node defined in its nodelist) only if the
// dependent RG is going online on a different node from the "single-node"
// dependee RG. If the dependency is ignored because of the above case, a
// warning message is displayed.
//
// If the caller does not yet know the nodes on which the RG intends to
// come online, it should pass EMPTY_NODESET as the online_nodeset.
// If any dependencies on a single-node offline RG are encountered, and the
// nodeset is empty, the dependencies are ignored. They will be checked
// in rebalance itself.
//
// Called by idl_scswitch_primaries() when determining
// whether it's OK to switch an RG online.
// It's not OK if it has an offline dependendee that is not also being
// switched online.
//
// If any offline dependee(s) are found then any error/warning message that
// was previously set in res->err_msg before and during the execution of
// this function, will be cleared, to make way for the REM_RG_DEP_OFFLINE
// error message.
//
// Handles scalable services as a special case.  If rgdepp contains a
// scalable service resource, then rgdepp is deemed to have an implicit
// "RG dependency" on the RG of the dependee SharedAddress resource,
// for purposes of this function.
//
// Must be run on the president. Caller holds the rgm state lock.
//
void
check_offline_dependees(scha_errmsg_t *res, rglist_p_t rgdepp,
    rglist_p_t *rglistp, LogicalNodeset &online_nodeset)
{
	namelist_t *nl;
	namelist_t *dependee_rgs = NULL;
	rglist_p_t rgp;
	sc_syslog_msg_handle_t	handle;
	rgm::lni_t lni;
	char *dependee_rg_name = NULL;

	ucmm_print("RGM", NOGET("check_offline_dependees: RG <%s>"),
	    rgdepp->rgl_ccr->rg_name);

	//
	// Compute the list of dependee RGs including RG_dependencies
	// plus "implict" dependencies of scalable service RG upon
	// SharedAddress RG.
	//
	dependee_rgs = add_sa_dependee_rgs_to_list(res, rgdepp, NULL);
	if (res->err_code != SCHA_ERR_NOERR) {
		goto alldone; //lint !e801
	}

	//
	// Iterate over the list of dependee RGs
	//
	for (nl = dependee_rgs; nl != NULL; nl = nl->nl_next) {
		// Is the RG "nl" itself being switched online?
		if (is_rg_in_list(nl->nl_name, rglistp)) {
			continue;
		}

		rgp = rgname_to_rg(nl->nl_name);

		// Is nl a dangling RG reference?
		// XXX This should never occur.
		// XXX We could do:	CL_PANIC(rgp);
		if (rgp == NULL) {
			ucmm_print("RGM", NOGET(
			    "check_offline_dependees: RG <%s> in "
			    "RG_dependencies list of RG <%s> does not exist"),
			    nl->nl_name, rgdepp->rgl_ccr->rg_name);
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RG_TAG, rgdepp->rgl_ccr->rg_name);
			//
			// SCMSGS
			// @explanation
			// A non-existent resource group is listed in the
			// RG_dependencies of the indicated resource group.
			// This should not occur and may indicate an internal
			// logic error in the rgmd.
			// @user_action
			// Look for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "ERROR: resource group <%s> has RG_dependency on "
			    "non-existent resource group <%s>",
			    rgdepp->rgl_ccr->rg_name, nl->nl_name);
			sc_syslog_msg_done(&handle);
			continue;
		}

		// Is "nl" an offline dependee of rgdepp?
		if (!is_rg_online(rgp)) {
			ucmm_print("RGM",
			    NOGET("check_offline_dependees: RG <%s> depends "
				"on RG <%s>, which is not online."),
			    rgdepp->rgl_ccr->rg_name, rgp->rgl_ccr->rg_name);
			/*
			 * Is this a "single node" RG?
			 * If so, check that the offline "single node" RG is
			 * not on a node where rgdepp wants to be brought
			 * online. If not, display a warning message.
			 */
			if (rgp->rgl_ccr->rg_nodelist->nl_next == NULL) {
				lni = rgp->rgl_ccr->rg_nodelist->nl_lni;
				if (online_nodeset.containLni(lni)) {
					add_string_to_list(res,
					    rgp->rgl_ccr->rg_name,
					    &dependee_rg_name);
					if (res->err_code != SCHA_ERR_NOERR) {
						goto alldone; //lint !e801
					}
				} else if (!online_nodeset.isEmpty()) {
					//
					// If online_nodeset is empty,
					// we don't bother issuing a warning
					// message.
					//
					rgm_format_errmsg(res,
					    RWM_RG_DEP_OFFLINE,
					    rgdepp->rgl_ccr->rg_name,
					    rgp->rgl_ccr->rg_name);
				}
			} else {
				add_string_to_list(res, rgp->rgl_ccr->rg_name,
				    &dependee_rg_name);
				if (res->err_code != SCHA_ERR_NOERR) {
					goto alldone; //lint !e801
				}
			}
		} else {
			ucmm_print("RGM",
			    NOGET("check_offline_dependees: RG <%s> depends "
				"on RG <%s>, which IS online."),
			    rgdepp->rgl_ccr->rg_name, rgp->rgl_ccr->rg_name);
		}
	}

	if (dependee_rg_name != NULL && !online_nodeset.isEmpty()) {
		/*
		 * As dependee_rg_name is not NULL, the switch will fail
		 * because of the RG Dependency.
		 * Clear the RWM_RG_DEP_OFFLINE Warning Message as a
		 * dependee has been found, after RWM_RG_DEP_OFFLINE
		 * was set.
		 */
		free(res->err_msg);
		res->err_msg = NULL;

		res->err_code = SCHA_ERR_DEPEND;
		rgm_format_errmsg(res, REM_RG_DEP_OFFLINE,
		    rgdepp->rgl_ccr->rg_name, dependee_rg_name);
	}

alldone:
	rgm_free_nlist(dependee_rgs);
	free(dependee_rg_name);
}

//
// add_string_to_list
//
// Appends new_string to commalist using "," as a seperator.
//
static void
add_string_to_list(scha_errmsg_t *res, char *new_string, char **commalist)
{
	uint_t len;
	boolean_t newlist = B_FALSE;
	char *tptr;

	if (*commalist == NULL) {
		newlist = B_TRUE;
		/* Increase len by 1 (terminating NULL) */
		len = strlen(new_string) + 1;
	} else {
		/* Increase len by 2 (terminating NULL and comma) */
		len = strlen(new_string) + strlen(*commalist) + 2;
	}

	if ((tptr = (char *)realloc(*commalist, (size_t)len)) == NULL) {
		res->err_code = SCHA_ERR_NOMEM;
		if (*commalist) {
			free(*commalist);
			*commalist = NULL;
		}
		return;
	}
	*commalist = tptr;

	if (newlist) {
		(void) strcpy(*commalist, new_string);
	} else {
		(void) strcat(*commalist, ",");
		(void) strcat(*commalist, new_string);
	}
}



//
// is_rg_in_list
//
// If the RG named by rgname appears in the null-terminated array rglistp,
// return B_TRUE.  Otherwise, return B_FALSE.
//
boolean_t
is_rg_in_list(const char *rgname, const rglist_p_t *rglistp)
{
	int	i;

	if (rglistp != NULL) {
		for (i = 0; rglistp[i] != NULL; i++) {
			if (strcmp(rgname, rglistp[i]->rgl_ccr->rg_name) == 0) {
				return (B_TRUE);
			}
		}
	}

	return (B_FALSE);
}


//
// is_rg_online
//
// If the given RG is online (or in an online-equivalent state) on any node,
// return B_TRUE.
// Otherwise, return B_FALSE.
// Must be run on the president.
//
static boolean_t
is_rg_online(rglist_p_t rg)
{
	// If RG is unmanaged we can return early
	if (rg->rgl_ccr->rg_unmanaged) {
		return (B_FALSE);
	}

	rgm::lni_t n;

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);

		n = lnNode->getLni();

		if (is_rg_online_on_node(rg, n, B_TRUE)) {
			return (B_TRUE);
		}
	}

	return (B_FALSE);
}

//
// is_r_online_anywhere
//
// If the given R is online (or in an online-equivalent state) on any node,
// return B_TRUE.
// Otherwise, return B_FALSE.
// Must be run on the president.
//
static bool
is_r_online_anywhere(rlist_p_t rp)
{
	// If containing RG is unmanaged we can return early
	if (rp->rl_rg->rgl_ccr->rg_unmanaged) {
		return (B_FALSE);
	}

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);
		if (((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->
		    r_switch[it->second] == SCHA_SWITCH_DISABLED) {
			continue;
		}

		rgm::lni_t n = lnNode->getLni();

		if (is_r_online(rp->rl_xstate[n].rx_state) ||
		    is_r_starting(rp->rl_xstate[n].rx_state)) {
			return (B_TRUE);
		}
	}

	return (B_FALSE);
}

//
// idl_scswitch_clear
//
// This is the function which is called by the rgm_scswitch_clear().
// this function performs action on president node to clear the given flag
// scswitch -c -f
// XXXX Only the STOP_FAILED flag clearing is implemented yet.
// XXXX The implementation for clearing other flags such as UPDATE_FAILED,
// XXXX INIT_FAILED and FINI_FAILED still needs to be completed.
// XXXX bugid 4215776
//
// No restrictions for "system" RGs
//
void
rgm_comm_impl::idl_scswitch_clear(
	const rgm::idl_scswitch_clear_args	&scsw_clr_args,
	rgm::idl_regis_result_t_out		scsw_clr_res,
	Environment &e)
{
	uint_t		i, no_nodes, n_res;
	LogicalNodeset	mnodeset;
	rgm::lni_t	lni;
	rgm::rgm_comm_var prgm_serv = NULL;
	char		*node_name = NULL;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	result = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	temp_res = {SCHA_ERR_NOERR, NULL};
	rlist_p_t	r_p;
	rglist_p_t	rg_p;
	rglist_p_t	*rglistp = NULL;	// array of RG pointers
	uint_t		num_rgs = 0;	// number of RGs being operated on
	namelist_t	*rgnames = NULL;
	boolean_t skip_flag, rg_busy_flag;
	rgm::idl_string_seq_t  outlist;
	boolean_t globalflag, nodelist_contains;
	//
	// This namelist will be used during handling of multiple operands and
	// continue with the other operands in case of failure of any of the
	// operand.
	// failed_op_list namelist will contain the
	// names of RGs which have failed during processing.
	//
	sc_syslog_msg_handle_t	handle;
	idlretval_t	idlretval;
	rgm::idl_string_seq_t  rnames_seq;

	ucmm_print("RGM", NOGET("in idl_scswitch_clear()\n"));

	// lock state structure before accessing President info
	rgm_lock_state();

	//
	// The only flag currently implemented is STOP_FAILED.
	//
	if (scsw_clr_args.flag != rgm::STOP_FAILED) {
		res.err_code = SCHA_ERR_INTERNAL;
		goto switch_clear_end; //lint !e801
	}

	//
	// Check that the nodenames of the nodes in nodelist are valid
	// cluster members and add their LNIs to mnodeset.
	//
	no_nodes = (uint_t)scsw_clr_args.node_names.length();
	ucmm_print("RGM", NOGET("no_nodes = %d \n"), no_nodes);
	LogicalNode *lnNode;

	for (i = 0; i < no_nodes; i++) {
		node_name = strdup(scsw_clr_args.node_names[i]);
		lnNode = get_ln(node_name, &(temp_res.err_code));
		if (temp_res.err_code == SCHA_ERR_NODE) {
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_INVALID_NODE, node_name);
			temp_res.err_code = SCHA_ERR_NOERR;
			free(node_name);
			node_name = NULL;
			continue;
		} else if (temp_res.err_code != SCHA_ERR_NOERR) {
			// NOMEM or internal error -- bail out
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			goto switch_clear_end; //lint !e801
		}

		lni = lnNode->getLni();

		if (mnodeset.containLni(lni)) {
			ucmm_print("RGM",
			    NOGET("idl_scswitch_clear: "
			    "d=uplicate lni <%d>\n"), lni);
			temp_res.err_code = SCHA_ERR_INVAL;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_DUPNODE, node_name);
			temp_res.err_code = SCHA_ERR_NOERR;
			free(node_name);
			node_name = NULL;
			// continue instead of bailing out
			continue;
		}

		//
		// Test membership of physical node in cluster.
		// Even if the zone is down, it might contain an RG in
		// ERROR_STOP_FAILED state if it contains a Global_zone=TRUE
		// resource.
		//
		if (lnNode->isPhysNodeUp()) {
			mnodeset.addLni(lni);
			ucmm_print("idl_scswitch_clear",
			    NOGET("nodename = %s, lni = %d\n"),
			    node_name, lni);
		} else {
			ucmm_print("RGM",
			    NOGET("idl_scswitch_clear: lni %d not in "
			    "cluster\n"), lni);
		}

		if (node_name) {
			free(node_name);
			node_name = NULL;
		}
	}

	// If mnodeset is empty, return early with success
	if (mnodeset.isEmpty()) {
		goto switch_clear_end; //lint !e801
	}

	// check for invalid or duplicate Rs.
	temp_res = prune_operand_list(scsw_clr_args.R_names,
	    rnames_seq, B_TRUE);

	if (temp_res.err_code != SCHA_ERR_NOERR) {
		smart_overlay(res, temp_res);
		temp_res.err_code = SCHA_ERR_NOERR;
	}

	(void) transfer_message(&res, &temp_res);
	n_res = (uint_t)rnames_seq.length();
	ucmm_print("RGM", NOGET("n_res = %d"), n_res);

	ngzone_check(scsw_clr_args.idlstr_zonename,
	    globalflag);
	//
	// for each of the valid resources in the pruned resource list
	// if the flag is STOP_FAILED, make sure that the resource is
	// in STOP_FAILED state on at least one of the given LNIs.
	// The RG must be in ERROR_STOP_FAILED on any node where the
	// resource is STOP_FAILED; if not, the RG is in a busy
	// state and we skip that resource operand.
	//
	for (i = 0; i < n_res; i++) {
		//
		// Note that the elements of scsw_clr_args.R_names cannot be
		// NULL, since they were derived from a NULL-terminated array
		// of char *
		//
		// Note strdup will abort the rgmd if out of swap
		//
		r_p = rname_to_r(NULL, (const char*)rnames_seq[i]);
		if (!globalflag) {
			nodelist_check(r_p->rl_rg, scsw_clr_args.nodeid,
			    scsw_clr_args.idlstr_zonename, nodelist_contains);
			if (!nodelist_contains) {
				temp_res.err_code = SCHA_ERR_ACCESS;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res,
				    REM_R_NODELIST_NGZONE,
				    (const char *)rnames_seq[i],
				    (const char *)scsw_clr_args.idlstr_zonename,
				    r_p->rl_rg->rgl_ccr->rg_name);
				temp_res.err_code = SCHA_ERR_NOERR;
				// continue instead of bailing out.
				continue;
			}
		}

		//
		// The skip flag is set to true initially. In the while loop
		// the state of resource is checked on each of
		// the specified nodes. If the resource is in R_STOP_FAILED
		// on any of the specified nodes then skip_flag is set to
		// false.
		//
		// We check all nodes in the list, because we also need to
		// verify that the RG state is ERROR_STOP_FAILED on
		// every node where the resource is in STOP_FAILED state.
		// If the RG is not ERROR_STOP_FAILED on any of those nodes,
		// it must be in a busy state such as PENDING_OFF_STOP_FAILED,
		// PENDING_OFFLINE, ON_PENDING_R_RESTART, etc.  So, if the
		// RG is in any other state besides ERROR_STOP_FAILED,
		// we set rg_busy_flag and skip this resource with an
		// "RG busy" error.
		//
		skip_flag = B_TRUE;
		rg_busy_flag = B_FALSE;
		lni = 0;
		rg_p = r_p->rl_rg;
		while ((lni = mnodeset.nextLniSet(lni + 1)) != 0) {
			if (r_p->rl_xstate[lni].rx_state !=
			    rgm::R_STOP_FAILED) {
				// the resource is not stop_failed on this
				// node; continue to the next node.
				//
				continue;
			}
			skip_flag = B_FALSE;
			if (rg_p->rgl_xstate[lni].rgx_state !=
			    rgm::RG_ERROR_STOP_FAILED) {
				// continue instead of bailing out.
				rg_busy_flag = B_TRUE;
				break;
			}
		}

		//
		// If the RG is busy, skip the operand with an error.
		//
		// If the resource is not in STOP_FAILED on any of the
		// specified nodes, skip the operand and print an
		// informational message.
		//
		if (rg_busy_flag) {
			temp_res.err_code = SCHA_ERR_RGRECONF;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_RS_IN_BUSY_RG,
			    r_p->rl_ccrdata->r_name,
			    rg_p->rgl_ccr->rg_name);
			temp_res.err_code = SCHA_ERR_NOERR;
			continue;
		} else if (skip_flag) {
			rgm_format_errmsg(&res, RWM_R_NOT_IN_STOP_FAIL,
			    r_p->rl_ccrdata->r_name);
			continue;
		}

		//
		// The flag to be cleared is STOP_FAILED.

		//
		// rglistp will hold a list of all the RGs containing
		// resources that are being cleared.
		//
		// If this RG is already in the list, continue on to the next
		// resource.
		//
		if (is_rg_in_list(rg_p->rgl_ccr->rg_name, rglistp)) {
			//
			// Add the valid Rs to the outlist.
			// This will handle the case of more than one Rs
			// operand in the same RG.
			//
			outlist.length(outlist.length() + 1);
			//
			// idl string assignment from a (const char *)
			// allocates memory & copies the string.
			//
			outlist[outlist.length() - 1] =
			    (const char *)rnames_seq[i];
			continue;
		}

		//
		// Add this resource's RG to rglistp
		//
		temp_res = rglist_add_rg(rglistp, rg_p, num_rgs++);
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			goto switch_clear_end; //lint !e801
		}

		//
		// Add the RG name to the rgnames list also.
		//
		if (!namelist_exists(rgnames, rg_p->rgl_ccr->rg_name)) {
			temp_res = namelist_add_name(&rgnames,
			    rg_p->rgl_ccr->rg_name);
			if (temp_res.err_code != SCHA_ERR_NOERR) {
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				goto switch_clear_end; //lint !e801
			}
		}
		// add the valid Rs in the outlist.
		outlist.length(outlist.length() + 1);
		//
		// idl string assignment from a (const char *)
		// allocates memory & copies the string.
		//
		outlist[outlist.length() - 1] =
		    (const char *)rnames_seq[i];
	}

	//
	// At this point we have the list of nodes on which to call the
	// rgm_clear_flag in mnodeset.
	//
	// We now iterate over the list of zones in mnodeset and, after
	// getting the CORBA reference to the RGM on that node, call the IDL
	// function rgm_clear_flag (which calls rgm_clear_util) on that node.
	// The state of the resource need be cleared only on the slave,
	// because clearing the
	// STOP_FAILED flag consists only of state changes which are
	// pushed from the slave to the pres (there
	// is no separate flag in memory to be cleared).  If we
	// ever implement other flags that could need clearning on the
	// president side, this logic might need to change.
	//
	lni = 0;
	while ((lni = mnodeset.nextLniSet(lni + 1)) != 0) {
		bool is_farmnode;

		lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);
		// Get the physical nodeid of lni
		sol::nodeid_t nodeid = lnNode->getNodeid();

		ucmm_print("RGM",
		    NOGET("idl_switch_clear: for lni %d nodeid %d\n"),
		    lni, nodeid);

		rgm::idl_regis_result_t_var clr_flg_res_v;

		if ((rgmx_hook_call(rgmx_hook_clear_flag, nodeid,
			lni, lnNode->getIncarnation(), &outlist,
			scsw_clr_args.flag, &clr_flg_res_v, &idlretval,
			&is_farmnode) == RGMX_HOOK_ENABLED) &&
			(is_farmnode)) {
			if (idlretval != RGMIDL_OK) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "rgmx_clear_flag(): failed");
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_MEMBERCHG;
				// continue instead of bailing out.
				continue;
			}
		} else {
			prgm_serv = rgm_comm_getref(nodeid);
			if (CORBA::is_nil(prgm_serv)) {
				ucmm_print("RGM",
				    NOGET("CORBA reference for nodeid "
				    "%d returned NULL. "
				    "Hence returning ...\n"), nodeid);
				temp_res.err_code = SCHA_ERR_MEMBERCHG;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res, REM_CLRECONF);
				temp_res.err_code = SCHA_ERR_NOERR;
				// continue instead of bailing out.
				continue;
			}

			// clear flag on the slave
			prgm_serv->rgm_clear_flag(
			    orb_conf::local_nodeid(),
			    Rgm_state->node_incarnations.members[
			    orb_conf::local_nodeid()],
			    lni,
			    lnNode->getIncarnation(),
			    outlist,
			    scsw_clr_args.flag,
			    clr_flg_res_v,
			    e);

			if ((result = except_to_scha_err(e)).err_code !=
			    SCHA_ERR_NOERR) {
				ucmm_print("RGM",
				    NOGET("idl_scswitch_clear(): IDL exception "
				    "when communicating to slave node %d\n"),
				    nodeid);
				res.err_code = result.err_code;
				// Currently, except_to_scha_err doesn't return
				// a message, just an error code.
				goto switch_clear_end; //lint !e801
			}
		}

		temp_res.err_code = (scha_err_t)clr_flg_res_v->ret_code;
		if (res.err_code == SCHA_ERR_NOERR)
			res.err_code = temp_res.err_code;
		temp_res.err_code = SCHA_ERR_NOERR;

		if ((const char *)clr_flg_res_v->idlstr_err_msg !=
		    NULL) {
			rgm_sprintf(&res, "%s",
			    (const char *)clr_flg_res_v->
			    idlstr_err_msg);
		}

		//
		// A nonzero error code from rgm_clear_flag() indicates
		// an internal error, so we return early.
		//
		if (res.err_code != SCHA_ERR_NOERR) {
			ucmm_print("RGM",
			    NOGET("ret_code is not NOERR.\n"));
			// continue instead of bailing out.
			goto switch_clear_end; //lint !e801
		}
	}

	//
	// If rglistp is not null [i.e., flag == STOP_FAILED] do the
	// following check:
	// Iterate through the RGs.  If any remain ERROR_STOP_FAILED, append
	// error messages advising the user how to clear the condition.
	//
	//
	// Before we make the calls, ensure that we have the latest state
	// on the president.
	//
	rgm_unlock_state();
	flush_state_change_queue();
	rgm_lock_state();

	//
	// We can't re-use the array of pointers, because we released the
	// lock and grabbed it again.  Re-lookup the pointers from the
	// names list.
	//
	for (namelist_t *temp_name = rgnames; temp_name != NULL;
	    temp_name = temp_name->nl_next) {
		rg_p = rgname_to_rg(temp_name->nl_name);
		if (rg_p != NULL) {
			check_clear_stopfail(&res, rg_p, mnodeset);
		}
	}

switch_clear_end:

	free(node_name);

	//
	// Suppress lint Warning(644) [c:21]:
	//	rglistp (line 3071) may not have been initialized
	// This is an apparent lint bug.  The variable is initialized when
	// declared.
	//
	free(rglistp);	//lint !e644
	rgm_free_nlist(rgnames);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	scsw_clr_res = retval;
	rgm_unlock_state();

	ucmm_print("RGM", NOGET("exiting idl_scswitch_clear()\n"));
}
#endif // !EUROPA_FARM


//
// rgm_clear_flag
//
// This is the Slave side of the idl_scswitch_clear IDL function.
// It clears the given flag for all the Resources on the given zone,
// on the local (slave node).  The local node may be the president acting
// as slave.
// The bulk of the work is done in the static utility routine rgm_clear_util.
//
// The president should flush the state change queue
// after calling this function on a (non-president) slave.
//
#ifdef EUROPA_FARM
bool_t
rgmx_clear_flag_1_svc(
    rgmx_clear_flag_args *rpc_args, rgmx_regis_result_t *rpc_scsw_clr_ret,
    struct svc_req *)
#else
void
rgm_comm_impl::rgm_clear_flag(
    sol::nodeid_t president,
    sol::incarnation_num incarnation,
    rgm::lni_t lni,
    rgm::ln_incarnation_t ln_incarnation,
    const rgm::idl_string_seq_t	&scsw_clr_rs,
    rgm::scswitch_clear_flags	flag,
    rgm::idl_regis_result_t_out	scsw_clr_ret,
    Environment	&env)
#endif
{
#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	rgm::lni_t lni = rpc_args->lni;
	rgm::ln_incarnation_t ln_incarnation = rpc_args->ln_incarnation;
	rgm::idl_string_seq_t	scsw_clr_rs;
	rgm::scswitch_clear_flags flag;
	rgm::idl_regis_result_t *scsw_clr_ret;

	bzero(rpc_scsw_clr_ret, sizeof (rgmx_regis_result_t));
	array_to_strseq_2(rpc_args->r_list, scsw_clr_rs);
	flag = (rgm::scswitch_clear_flags) rpc_args->flag;

	rpc_scsw_clr_ret->retval = RGMRPC_OK;
#endif
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	rgm::idl_regis_result_t_var clr_utl_res_v;

	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	ucmm_print("RGM",
	    NOGET("rgm_switch_clear: executing on lni %d.\n"), lni);

#ifndef EUROPA_FARM
	FAULT_POINT_FOR_ORPHAN_ORB_CALL("rgm_clear_flag", president,
		incarnation);
#endif

	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore the call.
		//
		goto clear_end; //lint !e801
	}

	//
	// Do not return early with IDL exception if zone died.
	// The RG might contain Global_zone=TRUE resources which could
	// be in ERROR_STOP_FAILED state even though the zone is down.
	//

	// XXX this function needs to be implemented for flags other than
	// STOP_FAILED.  (However, no such flags exist yet).
	// XXXX bugid 4215776
	rgm_clear_util(lni, scsw_clr_rs, flag, clr_utl_res_v);
	res.err_code = (scha_err_t)clr_utl_res_v->ret_code;
	if ((const char *)clr_utl_res_v->idlstr_err_msg)
		res.err_msg = strdup(clr_utl_res_v->idlstr_err_msg);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	scsw_clr_ret = retval;
#ifdef EUROPA_FARM
	rpc_scsw_clr_ret->ret_code = scsw_clr_ret->ret_code;
	if ((char *)scsw_clr_ret->idlstr_err_msg == NULL)
		rpc_scsw_clr_ret->idlstr_err_msg = NULL;
	else
		rpc_scsw_clr_ret->idlstr_err_msg = new_str(
			scsw_clr_ret->idlstr_err_msg);
#endif
clear_end:
	slave_unlock_state();
#ifdef EUROPA_FARM
	return (TRUE);
#endif
}


//
// rgm_clear_util
//
// This is the static utility routine which clears the given flag, flag,
// for the given set of Resources, scsw_clr_rs, corresponding to the given
// zone, lni, on the local node.
//
static void
rgm_clear_util(
	rgm::lni_t			lni,
	const rgm::idl_string_seq_t	&scsw_clr_rs,
	rgm::scswitch_clear_flags	flag,
	rgm::idl_regis_result_t_out	scsw_clr_ret)
{
	char *r_name = NULL;
	sc_syslog_msg_handle_t	handle;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	rglist_p_t rg_p;
	rlist_p_t r_p;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	for (uint_t i = 0; i < scsw_clr_rs.length(); i++) {

		r_name = strdup(scsw_clr_rs[i]);

		ucmm_print("RGM",
		    NOGET("rgm_switch_clear: for lni %d  r name = %s\n"),
		    lni, r_name);

		//
		// idl_scswitch_clear() on the president already checked the
		// validity of the R names, and holds the rgm state lock,
		// so assert that the names must still be valid.
		//
		r_p = rname_to_r(NULL, scsw_clr_rs[i]);
		CL_PANIC(r_p);

		rg_p = r_p->rl_rg;

		ucmm_print("RGM",
		    NOGET("rgm_switch_clear: The RG's state for the r "
		    "name = %s was %s, with numeric value %d.\n"),
		    r_name, pr_rgstate(rg_p->rgl_xstate[lni].rgx_state),
		    rg_p->rgl_xstate[lni].rgx_state);

		switch (flag) {
		case rgm::STOP_FAILED:
			ucmm_print("RGM",
			    NOGET("rgm_clear_util called with STOP_FAILED"));

			ucmm_print("RGM",
			    NOGET("rgm_switch_clear: The state for the resource"
			    " name = %s was %s, with numeric value %d.\n"),
			    r_name, pr_rstate(r_p->rl_xstate[lni].rx_state),
			    r_p->rl_xstate[lni].rx_state);

			// If resource is not stop_failed, go on to next
			// resource with no side effects
			if (r_p->rl_xstate[lni].rx_state !=
			    rgm::R_STOP_FAILED) {
				continue;
			}

			//
			// Note that the president has already made sure that
			// the RG is ERROR_STOP_FAILED on lni.
			//

			// Set the R state to offline.
			set_rx_state(lni, r_p, rgm::R_OFFLINE);
			reset_fm_status(lni, r_p, rgm::R_FM_OFFLINE,
			    RGM_DIR_NULL);

			//
			// Examine the state of resources within the RG, and
			// reset the RG to ONLINE or OFFLINE if appropriate.
			// This function operates on the slave-side only.
			//
			rg_clear_stopfailed(rg_p, lni);

			ucmm_print("RGM",
			    NOGET("rgm_clear_util: The R's new state for the "
			    "r name = %s is now %s, with numeric value %d.\n"),
			    r_name, pr_rstate(r_p->rl_xstate[lni].rx_state),
			    r_p->rl_xstate[lni].rx_state);

			break;

		case rgm::UPDATE_FAILED:
		case rgm::BOOT_FAILED:
		case rgm::INIT_FAILED:
		case rgm::FINI_FAILED:
			// Not yet implemented
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RS_TAG, r_name);
			//
			// SCMSGS
			// @explanation
			// An internal rgmd error has occurred while
			// attempting to carry out an operator request to
			// clear an error flag on a resource. The attempted
			// clear action will fail.
			// @user_action
			// Since this problem might indicate an internal logic
			// error in the rgmd, save a copy of the
			// /var/adm/messages files on all nodes, and the
			// output of clresourcetype show -v, clresourcegroup
			// show -v +, and clresourcegroup status +. Report the
			// problem to your authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "rgm_clear_util called on resource <%s> "
			    "with incorrect flag <%d>", r_name, flag);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INVALID_FLAG);
			goto rgm_clear_util_end; //lint !e801

		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RS_TAG, r_name);
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "rgm_clear_util called on resource <%s> "
			    "with incorrect flag <%d>", r_name, flag);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_INTERNAL;
			rgm_format_errmsg(&res, REM_INTERNAL);
			goto rgm_clear_util_end; //lint !e801
		}

		if (r_name) {
			free(r_name);
			r_name = NULL;
		}
	}


rgm_clear_util_end:

	free(r_name);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	scsw_clr_ret = retval;
}

#ifndef EUROPA_FARM
//
// wait_for
//
// wait_for will call cond_wait() while the RG remains in the state rg_st.
// The check-and-wait will be done till RG has moved out of rg_st on all
// cluster nodes.
//
// The state rgm::RG_PENDING_ONLINE is special_cased: we have to wait for
// the RG to move out of PENDING_ONLINE *and* PENDING_ON_STARTED *and*
// PENDING_OFF_START_FAILED.  We need to wait while the RG is
// PENDING_OFF_START_FAILED because switches are not busted until the RG is
// OFFLINE_START_FAILED.  If we stop waiting while the RG is
// PENDING_OFF_START_FAILED, we might miss the busted status of the RG.
// The subroutine is_rg_in_state() handles this check.
//
// Caller has checked that RG is not initially "busted".
// If RG is "busted" after a cond_wait, return the corresponding error code.
//
// This function is called only on the president.  The caller holds the
// RGM state lock.
//
scha_err_t
wait_for(rglist_p_t rgp, char *rgname, rgm::rgm_rg_state rg_st)
{
	rgm::lni_t lni;
	scha_err_t err = SCHA_ERR_NOERR;

	CL_PANIC(rgp != NULL && rgname != NULL);

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);

		lni = lnNode->getLni();
		if (!lnNode->isInCluster())
			continue;

		while (is_rg_in_state(rgp, rg_st, lni)) {

			cond_slavewait.wait(&Rgm_state->rgm_mutex);

			// After cond_wait, look up the RG again by name
			// (just as a sanity check against internal errors)
			rgp = rgname_to_rg(rgname);
			CL_PANIC(rgp != NULL);

			if ((err = map_busted_err(rgp->rgl_switching)) !=
			    SCHA_ERR_NOERR)
				return (err);
		}
	}

	return (err);
}


//
// is_rg_in_state
//
// This is called as a subroutine by wait_for().  Returns B_TRUE if the RG
// is in state rg_st on node lni.  The state rgm::RG_PENDING_ONLINE is
// special_cased: we return B_TRUE if the RG is either PENDING_ONLINE
// *or* PENDING_ON_STARTED *or* PENDING_OFF_START_FAILED.  We need to wait
// while the RG is PENDING_OFF_START_FAILED because switches are not busted
// until the RG is OFFLINE_START_FAILED.  If we stop waiting while the
// RG is PENDING_OFF_START_FAILED, we might miss the busted status of the
// RG.
//
// Note that the same problem does not apply for PENDING_OFF_STOP_FAILED,
// because switches are busted as soon as the RG goes PENDING_OFF_STOP_FAILED
// (the busting doesn't wait until the RG is ERROR_STOP_FAILED).
//
// This function is called only on the president.  The caller holds the
// RGM state lock.
//
boolean_t
is_rg_in_state(
	rglist_p_t rgp,			// Is this RG ...
	rgm::rgm_rg_state rg_st,	// in this state ...
	rgm::lni_t lni)			// on this node?
{
	CL_PANIC(rgp != NULL);

	if (rg_st == rgm::RG_PENDING_ONLINE) {
		if (rgp->rgl_xstate[lni].rgx_state == rg_st ||
		    rgp->rgl_xstate[lni].rgx_state ==
		    rgm::RG_PENDING_ON_STARTED ||
		    rgp->rgl_xstate[lni].rgx_state ==
		    rgm::RG_PENDING_OFF_START_FAILED) {
			return (B_TRUE);
		} else {
			return (B_FALSE);
		}
	} else {
		if (rgp->rgl_xstate[lni].rgx_state == rg_st) {
			return (B_TRUE);
		} else {
			return (B_FALSE);
		}
	}
}


//
// idl_scswitch_restart_rg()
// It stops RGs on its current master which is specified by
// scswp_args.node_names and re-start them on the same node again.
// The preliminary sanity check will make sure the RG is currently
// mastered by the scswp_args.node_names. Otherwise the code will
// return early with error.
//
// If there is any problem when stoping/starting a RG within a list
// of RGs, the error code will be recorded but the code will go on
// till all RGs has been restarted. It's user's responsibility to
// find out which RG actually fails to be restarted, by restarting
// RG one by one.
//
// newcli allows empty nodelist to be specified.In this case
// the nodelist is calculated.The nodes on which the RG is
// online forms the nodelist.
//
// If the verbose_flag is set the success message is printed.
//
void
rgm_comm_impl::idl_scswitch_restart_rg(
	const rgm::idl_scswitch_primaries_args &scswp_args,
	rgm::idl_regis_result_t_out result,
	bool verbose_flag,
	Environment &)
{
	scha_err_t	err_code = SCHA_ERR_NOERR;
	uint_t		rg_index, node_index, rg_num = 0;
	char		*nodename = NULL;
	LogicalNode	*ln;
	rglist_p_t	rglistp[2];
	rglist_p_t	rgp = NULL;	// null-term-ed array of rglist_t ptrs
	LogicalNodeset	*master_ns;	// nodeset of master of interest.
	LogicalNodeset	cl_to_nodeset;
	LogicalNodeset	stopfailed_ns, offline_ns, pending_boot_ns, restart_ns;
	LogicalNodeset	*off_ns_set = NULL, *on_ns_set = NULL;
	rgm::lni_t	lni;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	temp_res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	event_res;
	int		min_flag_index = 0, max_flag_index = -1;
	LogicalNodeset	emptyNodeset;
	boolean_t globalflag, nodelist_contains;
	rgm::idl_string_seq_t  rgnames_seq;

	//
	// This namelist will be used during handling of multiple operands and
	// continue with the other operands in case of failure of any of the
	// operand.
	// failed_op_list namelist will contain the
	// names of RGs which have failed during processing.
	//
	namelist_t *failed_op_list = NULL;

	ucmm_print("RGM", NOGET("in idl_scswitch_restart_rg()\n"));

	CL_PANIC(scswp_args.RG_names.length() != 0);

	// lock state structure before accessing President info
	rgm_lock_state();

	// Computing the restarting node set (restart_ns). It is same
	// for all RGs we are going to process.

	if (scswp_args.node_names.length() > 0) {
		for (node_index = 0;
		    node_index < scswp_args.node_names.length(); node_index++) {
			nodename =
			    strdup_nocheck(scswp_args.node_names[node_index]);
			if (nodename == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto restart_end; //lint !e801
			}
			ln = get_ln(nodename, &temp_res.err_code);
			if (temp_res.err_code == SCHA_ERR_NODE) {
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res, REM_INVALID_NODE,
				    nodename);
				temp_res.err_code = SCHA_ERR_NOERR;
				free(nodename);
				nodename = NULL;
				// continue instead of bailing out
				continue;
			} else if (temp_res.err_code != SCHA_ERR_NOERR) {
				// NOMEM or internal error -- bail out
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				goto restart_end; //lint !e801
			}
			lni = ln->getLni();

			// test membership in cluster
			if (!ln->isInCluster()) {
				ucmm_print("RGM", NOGET(
				    "idl_scswitch_restart_rg: node %d not in "
				    "cluster\n"), lni);
				temp_res.err_code = SCHA_ERR_MEMBER;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res, REM_NODE_NOT_MEMBER,
				    nodename);
				temp_res.err_code = SCHA_ERR_NOERR;
				free(nodename);
				nodename = NULL;
				//  continue instead of bailing out.
				continue;
			}
			restart_ns.addLni(lni);
			free(nodename);
			nodename = NULL;
		}
	}

	//
	// Implies that the computed nodelist is NULL but the user specified
	// list is not NULL. Thus all nodes that user specfied are INVALID.
	// --bailout.
	//
	if (restart_ns.isEmpty() && (scswp_args.node_names.length() > 0)) {
		smart_overlay(res, temp_res);
		goto restart_end; //lint !e801
	}

	// check for invalid or duplicate RGs.
	temp_res = prune_operand_list(scswp_args.RG_names,
	    rgnames_seq, B_FALSE);

	if (temp_res.err_code != SCHA_ERR_NOERR) {
		smart_overlay(res, temp_res);
		temp_res.err_code = SCHA_ERR_NOERR;
	}

	(void) transfer_message(&res, &temp_res);
	// Iterate through RG list for sanity checks and build
	// nodeset array used in bringing RG offline and online (off_ns_set,
	// on_ns_set)
	//
	// The current master nodeset for each RG is saved as an entry in
	// on_ns_set, with the same index for the RG in scswp.RG_names.
	// The off_ns_set contains the current master nodeset of the RG
	// with specified nodename to be taken out. This nodeset will be
	// used to bring RG offline on specified node (before restart it).

	rg_num = rgnames_seq.length();
	off_ns_set = new LogicalNodeset[rg_num];
	if (off_ns_set == NULL) {
		ucmm_print("RGM", NOGET(
		    "idl_scswitch_restart_rg: calloc failed"));
		//
		// directly use the res variable itself as this is critical
		// error.
		res.err_code = SCHA_ERR_NOMEM;
		goto restart_end; //lint !e801
	}
	on_ns_set = new LogicalNodeset[rg_num];
	if (on_ns_set == NULL) {
		ucmm_print("RGM", NOGET(
		    "idl_scswitch_restart_rg calloc() failed"));
		//
		// directly use the res variable itself as this is critical
		// error.
		//
		res.err_code = SCHA_ERR_NOMEM;
		goto restart_end; //lint !e801
	}

	ngzone_check(scswp_args.idlstr_zonename, globalflag);
	// We are building on/off nodeset for each RG in the following loop.
	for (rg_index = 0; rg_index < rg_num; rg_index++) {
		rgp = rgname_to_rg(rgnames_seq[rg_index]);
		if (!globalflag) {
			nodelist_check(rgp, scswp_args.nodeid,
			    scswp_args.idlstr_zonename, nodelist_contains);
			if (!nodelist_contains) {
				temp_res.err_code = SCHA_ERR_ACCESS;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				rgm_format_errmsg(&res,
				    REM_RG_NODELIST_NGZONE,
				    rgp->rgl_ccr->rg_name,
				    (const char *)scswp_args.idlstr_zonename);
				//
				// Add the failed RG to failed_op_list
				// so that we dont process this RG further.
				//
				add_op_to_failed_list(
				    &failed_op_list,
				    rgnames_seq[rg_index]);
				temp_res.err_code = SCHA_ERR_NOERR;
				//  continue instead of bailing out.
				continue;
			}
		}

		// First make sure the RG is not unmanaged. Since the
		// unmanaged RG will sneak through in_endish_rg_state().
		// It will cause hang up in do_rg_switch().
		if (rgp->rgl_ccr->rg_unmanaged) {
			ucmm_print("RGM", NOGET(
			    "idl_scswitch_restart_rg: RG <%s> is unmanaged"),
			    (const char *)rgnames_seq[rg_index]);
			temp_res.err_code = SCHA_ERR_RGSTATE;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_RG_UNMANAGED,
			    (const char *)rgnames_seq[rg_index]);
			//
			// Add the failed RG to failed_op_list
			// so that we dont process this RG further.
			//
			add_op_to_failed_list(
			    &failed_op_list, rgnames_seq[rg_index]);
			temp_res.err_code = SCHA_ERR_NOERR;
			//  continue instead of bailing out.
			continue;
		}
		//
		// We are making sure RG is in endish state,
		// then in the following code we can directly compute
		// the RG nodeset without scanning through the real RG state.
		//
		// We don't check for nodes running resource_restart because
		// the state machine handles RG restart in that case,
		// even if the RG is restarted on a node where a resource
		// within it is already restarting.
		//
		if (!in_endish_rg_state(rgp, &pending_boot_ns,
		    &stopfailed_ns, &offline_ns, NULL)) {
			ucmm_print("RGM", NOGET(
			    "idl_scswitch_restart_rg: RG <%s> isn't in "
			    "endish state"),
			    (const char *)rgnames_seq[rg_index]);
			temp_res.err_code = SCHA_ERR_RGRECONF;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_RG_BUSY,
			    (const char *)rgnames_seq[rg_index]);
			//
			// Add the failed RG to failed_op_list
			// so that we dont process this RG further.
			//
			add_op_to_failed_list(
			    &failed_op_list, rgnames_seq[rg_index]);
			temp_res.err_code = SCHA_ERR_NOERR;
			//  continue instead of bailing out.
			continue;
		}
		if (!stopfailed_ns.isEmpty()) {
			temp_res.err_code = SCHA_ERR_STOPFAILED;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_RG_STOPFAILED,
			    (const char *)rgnames_seq[rg_index]);
			//
			// Add the failed RG to failed_op_list
			// so that we dont process this RG further.
			//
			add_op_to_failed_list(
			    &failed_op_list, rgnames_seq[rg_index]);
			temp_res.err_code = SCHA_ERR_NOERR;
			//  continue instead of bailing out.
			continue;
		}

		//
		// If a RG is in endish state, it falls into one of
		// the following categries:
		//	ONLINE (including ON_PENDING_R_RESTART)
		//	PENDING_ONLINE_BLOCKED
		//	OFF_PENDING_BOOT
		//	STOPFAILED
		//	OFFLINE
		// Since we 've checked stopfailed_ns = 0, then the current
		// cluster membership subtracting pending_boot_ns and
		// offline_ns will be the nodeset on which RG is online.
		//
		master_ns = lnManager->get_all_logical_nodes(rgm::LN_UP);
		*master_ns -= offline_ns;
		*master_ns -= pending_boot_ns;
		on_ns_set[rg_index] = *master_ns;

		//
		// If nodelist operand is empty, restart the RG on all of its
		// current masters.
		//
		if (scswp_args.node_names.length() == 0) {
			restart_ns = *master_ns;
		}

		// If the restart nodeset is empty, skip this RG.
		if (restart_ns.isEmpty()) {
			temp_res.err_code = SCHA_ERR_STATE;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			rgm_format_errmsg(&res, REM_RG_RST_FAILED,
			    (const char *)rgnames_seq[rg_index]);
			//
			// Add the failed RG to failed_op_list
			// so that we dont process this RG further.
			//
			add_op_to_failed_list(
			    &failed_op_list, rgnames_seq[rg_index]);
			temp_res.err_code = SCHA_ERR_NOERR;
			//  continue instead of bailing out.
			continue;
		}
		//
		// The RG is restarted only on nodes on which it is currently
		// online.  We check whether the RG is
		// online on restart_ns by checking whether restart_ns is
		// subset of master_ns.
		//
		LogicalNodeset subtract1 = restart_ns - *master_ns;
		if (!subtract1.isEmpty()) {
			//
			// One or more nodes specified aren't the master of RG.
			// Return error.
			//
			char *ucmm_buffer = NULL;
			if (subtract1.display(ucmm_buffer) == 0) {
				ucmm_print("RGM", NOGET(
				    "idl_scswitch_restart_rg: RG <%s> "
				    "isn't online on nodeset %s\n"),
				    (const char *)rgnames_seq[rg_index],
				    ucmm_buffer);
				free(ucmm_buffer);
			}

			temp_res.err_code = SCHA_ERR_STATE;
			if (res.err_code == SCHA_ERR_NOERR)
				res.err_code = temp_res.err_code;
			temp_res.err_code = SCHA_ERR_NOERR;

			//
			// compute the nodenames for the nodes and print
			// error message.
			//
			lni = 0;
			while ((lni = subtract1.nextLniSet(lni + 1)) != 0) {
				ln = lnManager->findObject(lni);
				CL_PANIC(ln);
				rgm_format_errmsg(&res,
				    REM_RG_RST_FAILED_ON_NODE,
				    (const char *)rgnames_seq[rg_index],
				    ln->getFullName());
			}

			//
			// check if there are any nodes on which restart
			// can be performed.
			//
			restart_ns -= subtract1;
			if (restart_ns.isEmpty()) {
				//
				// Add the failed RG to failed_op_list
				// so that we dont process this RG further.
				//
				add_op_to_failed_list(
				    &failed_op_list, rgnames_seq[rg_index]);
				//  continue instead of bailing out.
				continue;
			}
		}

		//
		// Add restart_ns to this RG's planned_on_nodes.  Then if
		// any of those nodes dies before we begin to execute the
		// restart of this RG (which might occur if we are restarting
		// multiple RGs), process_rg will bust the restart of this RG.
		//
		rgp->rgl_planned_on_nodes = restart_ns;

		// Subtract restart_ns from current master nodeset, we are
		// getting the nodeset which is used to bring RG offline
		// of specific nodes.
		off_ns_set[rg_index] = *master_ns - restart_ns;
		delete master_ns;

		//
		// Set rgl_switching flag to make this RG "busy" for the
		// duration of the scswitch -R command.  Also track the
		// highest index RG on which we have set the flag.
		//
		rgp->rgl_switching = SW_IN_PROGRESS;
		max_flag_index = (int)rg_index;
	}

	//
	// In the following we are going to bring RG offline and restart
	// RG using do_rg_switch().  We have already set the rgl_switching
	// flag on each RG operand.  We restart the RGs one at a time.
	//
	// do_rg_switch() will call cond_wait() and clear rgl_switching
	// flag. But it does cond_wait() first and when it clears
	// rgl_switching, the lock is already reclaimed. So if we
	// bring RG offline and restart it one by one there is no
	// risk for other code to sneak in and mess up the restart
	// action to the RG.
	//
	for (rg_index = 0; rg_index < rg_num; rg_index++) {
		char *opname = strdup(rgnames_seq[rg_index]);
		// Skip the RGs which have failed earlier.
		if (namelist_exists(failed_op_list,
		    opname)) {
			free(opname);
			continue;
		}
		free(opname);
		// RG name has been checked above, just assert here
		// rgp isn't NULL.
		rgp = rgname_to_rg(rgnames_seq[rg_index]);
		CL_PANIC(rgp);

		//
		// Note, if the switch on this RG has been "busted" (for
		// example, by the death of a node on which the RG is to
		// be restarted), this will be caught by the call to
		// do_rg_switch() below; that function will also reset
		// rgl_switching.
		//

		// Get the nodeset where the Restart is taking place
		cl_to_nodeset = on_ns_set[rg_index] - off_ns_set[rg_index];

		// Post an event for this RG
		event_res = post_primaries_changing_event(NULL,
		    (const char *)rgnames_seq[rg_index],
		    CL_REASON_RG_RESTART, NULL,
		    rgp->rgl_ccr->rg_desired_primaries,
		    on_ns_set[rg_index], cl_to_nodeset);
		if (event_res.err_code == SCHA_ERR_NOMEM) {
			res.err_code = SCHA_ERR_NOMEM;
			goto restart_end; //lint !e801
		}
		rglistp[0] = rgp;
		rglistp[1] = NULL;
		//
		// Pass the restart_ns as the future_on nodeset, so
		// that do_rg_switch adds those nodes to the planned_on
		// nodeset to prevent process_needy_rg from bringing
		// strong negative affinitents online on this node
		// in between the two calls to do_rg_switch.
		//
		do_rg_switch(&temp_res, rglistp, off_ns_set[rg_index],
		    restart_ns, B_FALSE, B_FALSE, failed_op_list);
		if (temp_res.err_code != SCHA_ERR_NOERR) {
			// Error message (if any) was appended to temp_res.
			// Save & reset the error code.
			err_code = temp_res.err_code;
			temp_res.err_code = SCHA_ERR_NOERR;
			ucmm_print("RGM", NOGET(
			    "idl_scswitch_restart_rg: do_rg_switch() failed "
			    "on RG <%s>"),
			    (const char *)rgnames_seq[rg_index]);
			//
			// we don't want to do the next switch to bring this
			// RG online, because it has failed to go
			// offline properly.  Just continue on to the next
			// RG in the list.
			//
		} else {
			//
			// There was no error from the do_rg_switch to bring
			// the RG offline, so we now need to bring it back
			// online.
			//
			// Reset the rgl_switching flag since it is cleared by
			// previous do_rg_switch().
			// Since rglistp which passed into do_rg_switch() is
			// changed during the function call, we have to
			// re-set it.
			rgp->rgl_switching = SW_IN_PROGRESS;
			rglistp[0] = rgp;
			rglistp[1] = NULL;
			do_rg_switch(&temp_res, rglistp, on_ns_set[rg_index],
			    emptyNodeset, B_FALSE, B_FALSE, failed_op_list);
			if (temp_res.err_code != SCHA_ERR_NOERR) {
				// Error message (if any) was appended to res.
				// Save & reset the error code.
				err_code = temp_res.err_code;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
				temp_res.err_code = SCHA_ERR_NOERR;

				ucmm_print("RGM", NOGET(
				    "idl_scswitch_restart_rg: do_rg_switch() "
				    "failed on RG <%s>"), (const char *)
				    rgnames_seq[rg_index]);
			} else {
				if (verbose_flag)
					rgm_format_successmsg(&res,
					    RWM_RG_RESTARTED,
					    (const char *)
					    rgnames_seq[rg_index]);
			}
		}

		//
		// Copy any do_rg_switch messages from temp_res to res.
		// An error return means NOMEM which is fatal.
		//
		if (transfer_message(&res, &temp_res) != SCHA_ERR_NOERR) {
			goto restart_end; //lint !e801
		}

		// Track the lowest index of the RG for which we need to
		// reset the switching flag.  Any RG on which we call
		// do_rg_switch already has the flag reset.
		//
		min_flag_index = (int)rg_index + 1;
	}
	// err_code contains the code of the last error that occurred (if any)
	if (res.err_code == SCHA_ERR_NOERR)
		res.err_code = err_code;
	// res.err_msg already contains the error message(s), if any

	//
	// Check if any of the RGs are in PENDING_ONLINE_BLOCKED, and
	// set the warning message if they are. We do the check here at the
	// end to get maximum accuracy.
	//
	for (rg_index = 0; rg_index < rg_num; rg_index++) {
		char *opname = strdup(rgnames_seq[rg_index]);
		// Skip the RGs which have failed earlier.
		if (namelist_exists(failed_op_list,
		    opname)) {
			free(opname);
			continue;
		}
		free(opname);
		// RG name has been checked above, just assert here
		// rgp isn't NULL.
		rgp = rgname_to_rg(rgnames_seq[rg_index]);
		CL_PANIC(rgp);
		// As error code is not updated res can be used.
		check_pending_online_blocked(&res, rgp);
	}


restart_end:
	//
	// In case of error return, reset any rgl_switching flags that we
	// set above.  If we encounter an error right at the beginning of
	// the function, we won't need to reset any RGs because we never
	// set the rgl_switching flag.  If we don't encounter an error we
	// don't need to reset any RGs because we have called do_rg_switch on
	// all of them.  Only reset the rgl_switching_flag on those RGs
	// that we have set the flag on but haven't called do_rg_switch on.
	// min_flag_index and max_flag_index contain the lower and upper
	// bounds respectively.
	//
	for (int i = min_flag_index; i <= max_flag_index; i++) {
		char *opname = strdup(rgnames_seq[i]);
		// Skip the RGs which have failed earlier.
		if (namelist_exists(failed_op_list,
		    opname)) {
			free(opname);
			continue;
		}
		free(opname);
		//
		// RG name has been checked above, just assert here
		// rgp isn't NULL.
		//
		rgp = rgname_to_rg(rgnames_seq[(uint_t)i]);
		CL_PANIC(rgp);
		rgp->rgl_planned_on_nodes.reset();
		if (rgp->rgl_switching == SW_IN_PROGRESS) {
			rgp->rgl_switching = SW_NONE;
		}
	}

	delete [] (off_ns_set);
	delete [] (on_ns_set);
	rgm_unlock_state();
	ucmm_print("RGM", NOGET("idl_scswitch_restart_rg: exiting"));

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);
	free(res.err_msg);
	free(temp_res.err_msg);
	if (failed_op_list)
		rgm_free_nlist(failed_op_list);
	result = retval;

}
#endif // !EUROPA_FARM


//
// rg_clear_stopfailed
// Check the state of all Rs in a given RG to see if the RG needs to change
// state on the given node from ERROR_STOP_FAILED state to a non-errored state.
// If so, set the RG on zone 'lni' to the new state.
//
// This function is called on a slave node which might be the president.
// A non-president slave node will push the state change back to the president.
//
// The caller certifies that the RG is in ERROR_STOP_FAILED.
// The caller holds the rgm state mutex.
//
// The caller has just cleared the STOP_FAILED state of a resource in the
// given RG, on the given lni.
//
// This function iterates through all the resources in the RG.
// If any resource in the RG is still STOP_FAILED, we return early, leaving
// the RG in ERROR_STOP_FAILED.
//
// Having eliminated resources that are STOP_FAILED or pending-deletion, we
// assert (using CL_PANIC) that any disabled resource must be OFFLINE.
//
// If all enabled Rs are online (or in an online-equivalent state) then we
// move the RG to ONLINE.  If any resources are restarting we move the RG
// to ON_PENDING_R_RESTART instead.  If any resources are OK_TO_START we
// move to PENDING_ONLINE, and if any are STARTING_DEPEND we move to
// PENDING_ONLINE_BLOCKED.
//
// If all enabled Rs are offline then we move the RG to OFFLINE.
//
// If neither of the preceding two conditions are true, then some enabled
// resources are offline and others are not offline; we leave the RG in
// ERROR_STOP_FAILED.
//
// We use set_rgx_state() to set the new RG state.  We do this for the event
// logging, not for the call to process_needy_rg.  Thus, we set the
// no_rebalance flag on the RG before calling set_rgx_state.
//
static void
rg_clear_stopfailed(
	rglist_p_t	rg_p,	// The RG to be processed
	rgm::lni_t	lni)	// The zone on which a R in RG was cleared
{
	boolean_t any_enabled_offline = B_FALSE;	// any enabled Rs
							// offline on lni
	boolean_t any_enabled_not_offline = B_FALSE;	// any enabled Rs not
							// offline on lni
	boolean_t any_restarting = B_FALSE;	// any Rs currently restarting
	boolean_t any_ok_start = B_FALSE;	// any in OK_TO_START
	boolean_t any_starting_depend = B_FALSE;	// any in
							// STARTING_DEPEND
	rlist_p_t r;

	// The RG must be ERROR_STOP_FAILED-- caller has checked this
	CL_PANIC(rg_p->rgl_xstate[lni].rgx_state ==
	    rgm::RG_ERROR_STOP_FAILED);

	// examine each R state
	for (r = rg_p->rgl_resources; r != NULL; r = r->rl_next) {
		//
		// If any r is still stop_failed, leave RG in error_stop_failed
		// state by returning early
		//
		if (r->rl_xstate[lni].rx_state == rgm::R_STOP_FAILED) {
			return;
		}

		if (((rgm_switch_t *)r->rl_ccrdata->r_onoff_switch)->
		    r_switch[lni] == SCHA_SWITCH_ENABLED) {
			// R is enabled on this node; set the appropriate flag.
			if (r->rl_xstate[lni].rx_state == rgm::R_OFFLINE) {
				any_enabled_offline = B_TRUE;
			} else {
				any_enabled_not_offline = B_TRUE;
			}

			// Check if R is restarting
			if (r->rl_xstate[lni].rx_restart_flg != rgm::RR_NONE) {
				any_restarting = B_TRUE;
			}

			// check starting_depend and ok_to_start
			if (r->rl_xstate[lni].rx_state ==
			    rgm::R_OK_TO_START) {
				any_ok_start = B_TRUE;
			} else if (r->rl_xstate[lni].rx_state ==
			    rgm::R_STARTING_DEPEND) {
				any_starting_depend = B_TRUE;
			}
		} else {
			//
			// R is disabled.  We have already ruled out R being
			// STOP_FAILED, therefore it must be OFFLINE.
			//
			CL_PANIC(r->rl_xstate[lni].rx_state ==
			    rgm::R_OFFLINE);
		}
	}

	//
	// If all enabled Rs were found to be offline (and noting that all
	// disabled Rs were asserted to be offline), set RG offline & return.
	// We also take this path if there are no enabled Rs.
	//
	if (!any_enabled_not_offline) {
		//
		// We don't need to set the rgl_no_rebalance flag
		// because process_needy_rg will skip rebalance on an
		// OFFLINE RG if the previous state is ERROR_STOP_FAILED.
		//
		set_rgx_state(lni, rg_p, rgm::RG_OFFLINE);
		return;
	}

	//
	// If all enabled Rs were found to be not-offline (therefore in an
	// online-equivalent endish state such as ONLINE, ONLINE_UNMON,
	// MONITOR_FAILED, or START_FAILED), reset the state of the RG to
	// ONLINE [or if any resources are OK_TO_START to PENDING_ONLINE,
	// or if any are restarting, to ON_PENDING_R_RESTART,
	// or if any are STARTING_DEPEND to PENDING_ONLINE_BLOCKED].
	// Note that this can occur only if the resource that was just cleared
	// was disabled, since clearing it will have set it to OFFLINE.
	//
	// Note that, if we change to PENDING_ONLINE or PENDING_R_RESTART,
	// we need to run the state machine.
	//
	if (!any_enabled_offline) {
		if (any_ok_start) {
			set_rgx_state(lni, rg_p,
			    rgm::RG_PENDING_ONLINE);
			run_state_machine(lni, "rg_clear_stopfailed");
		} else if (any_restarting) {
			set_rgx_state(lni, rg_p,
			    rgm::RG_ON_PENDING_R_RESTART);
			run_state_machine(lni, "rg_clear_stopfailed");
		} else if (any_starting_depend) {
			set_rgx_state(lni, rg_p,
			    rgm::RG_PENDING_ONLINE_BLOCKED);
		} else {
			set_rgx_state(lni, rg_p, rgm::RG_ONLINE);
		}
	}

	//
	// If both of the preceding if-expressions were false, then some
	// enabled resources are offline and others are online.  Therefore
	// we leave the RG in ERROR_STOP_FAILED state, requiring the
	// operator to manually switch it offline.
	//
}


#ifndef EUROPA_FARM
//
// check_starterrs
// The caller has just completed an scswitch -z, -Z, -e, or -R on the given RG.
// This function checks the state of the RG's resources on all nodes where
// the RG is ONLINE (or ON_PENDING_R_RESTART) to find out if any resources
// landed in START_FAILED or
// MONITOR_FAILED.  If so, return error messages in *res.
//
// If a resource is MONITOR_FAILED, append a warning message to res but
// leave res->err_code as-is.
// If a resource is START_FAILED, append an error message to res and
// set res->err_code to SCHA_ERR_STARTFAILED.
//
// If the 'check_offline' parameter is true, do an additional check
// [this case is called only by the "rebalance RGs" flavors of scswitch]:
// 	- if the RG is offline on all nodes, append a REM_RG_NOMASTER
//	  error message to res and set res->err_code to SCHA_ERR_NOMASTER.
//
// Note that we do not check for other "offline equivalent" states.
// OFFLINE_START_FAILED or ERROR_STOP_FAILED would have already "busted"
// the switch and generated
// an error message in rgs_are_rebalancing(). OFF_BOOTED or OFF_PENDING_BOOT
// will lead to another rebalance() call, so it does not accurately represent
// the situation where the RG "remains offline".  OFF_PENDING_METHODS cannot
// occur because rgl_switching was set in idl_scswitch_primaries() right
// after the call to rebalance() or prior to the beginning of node evacuation,
// and would have prevented any editing actions on this rg.
//
static void
check_starterrs(scha_errmsg_t *res, const rglist_p_t rgp,
    boolean_t check_offline)
{
	rlist_p_t	r;
	rgm::lni_t	lni;
	const char	*nodename;
	boolean_t	all_offline = B_TRUE;

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);

		lni = lnNode->getLni();

		if (check_offline &&
		    rgp->rgl_xstate[lni].rgx_state != rgm::RG_OFFLINE) {
			all_offline = B_FALSE;
		}
		if (!rg_on_or_r_restart(rgp, lni)) {
			continue;
		}
		// RG is online on this node; check state of resources
		nodename = lnNode->getFullName();
		for (r = rgp->rgl_resources; r != NULL; r = r->rl_next) {
			if (r->rl_xstate[lni].rx_state ==
			    rgm::R_START_FAILED) {
				// START failed.  Append error message and set
				// error code.
				res->err_code = SCHA_ERR_STARTFAILED;
				rgm_format_errmsg(res, REM_START_FAILED,
				    nodename, rgp->rgl_ccr->rg_name,
				    r->rl_ccrdata->r_name);
			} else if (r->rl_xstate[lni].rx_state ==
			    rgm::R_MON_FAILED) {
				// MONITOR_START failed.  Append error message.
				rgm_format_errmsg(res, RWM_MON_START_FAILED,
				    nodename, rgp->rgl_ccr->rg_name,
				    r->rl_ccrdata->r_name);
			}
		} // for each resource in the RG
	} // for each lni

	if (check_offline && all_offline) {
		res->err_code = SCHA_ERR_NOMASTER;
		rgm_format_errmsg(res, REM_RG_NOMASTER, rgp->rgl_ccr->rg_name);
	}
}


// check_pending_online_blocked
// -----------------------------
// If the given RG is in PENDING_ONLINE_BLOCKED on any node and at least one
// of its resources is in STARTING_DEPEND on the same node, append a warning
// message.
//
static void
check_pending_online_blocked(scha_errmsg_t *res, const rglist_p_t rgp)
{

	rgm::lni_t	lni;

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);
		lni = lnNode->getLni();

		if (!rg_on_or_r_restart(rgp, lni)) {
			continue;
		}
		//
		// If the RG is in PENDING_ONLINE_BLOCKED we need to check
		// also if any resources in the RG are in STARTING_DEPEND.
		// If not, we've already moved out of PENDING_ONLINE_BLOCKED
		// but the president doesn't know it yet.
		//
		if (rgp->rgl_xstate[lni].rgx_state ==
		    rgm::RG_PENDING_ONLINE_BLOCKED &&
		    any_res_starting_depend(rgp, lni)) {
			//
			// Do not change the error code.
			// Just append a warning message.
			//
			rgm_format_errmsg(res, RWM_PENDING_ONLINE_BLOCKED,
			    lnNode->getFullName(), rgp->rgl_ccr->rg_name);
		}
	}
}



//
// check_clear_stopfail
// The caller has just completed an scswitch -c -f STOP_FAILED on one or more
// resources in the given RG, on all nodes in the given nodeset.
// This function checks the state of the RG and its resources on each node
// in the given nodeset.  If the RG remains ERROR_STOP_FAILED on a node,
// an appropriate error message is appended to *errmsg.
//
// The error code in *errmsg is not modified.
//
static void
check_clear_stopfail(
	scha_errmsg_t *errmsg,
	const rglist_p_t rgp,
	LogicalNodeset mnodeset)
{
	rlist_p_t	r;
	rgm::lni_t	lni;

	lni = 0;
	while ((lni = mnodeset.nextLniSet(lni + 1)) != 0) {
		LogicalNode *lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);

		if (rgp->rgl_xstate[lni].rgx_state !=
		    rgm::RG_ERROR_STOP_FAILED) {
			// RG state is clear on this node
			continue;
		}

		//
		// The RG is ERROR_STOP_FAILED on lni.  Append one of two
		// possible error messages, depending on whether any resource
		// in RG remains STOP_FAILED on lni.
		//

		const char *nodename = lnNode->getFullName();

		for (r = rgp->rgl_resources; r != NULL; r = r->rl_next) {
			if (r->rl_xstate[lni].rx_state ==
			    rgm::R_STOP_FAILED) {
				// resource r remains STOP_FAILED
				rgm_format_errmsg(errmsg, RWM_STOP_FAILED,
				    rgp->rgl_ccr->rg_name, nodename,
				    r->rl_ccrdata->r_name);
				break;
			}
		}

		if (r == NULL) {
			// No resource is STOP_FAILED; therefore, some enabled
			// resources are ONLINE while others are OFFLINE.
			rgm_format_errmsg(errmsg, RWM_SOME_RS_ONLINE,
			    rgp->rgl_ccr->rg_name, nodename);
		}
	}
}


//
// switch_order_rsarray()
//
// The function orders the items in the given array of resource
// pointers by dependency;  that is, no resource in the ordered
// list will depend on any resource which follows it in the list.
// The input array is NULL terminated and must contain only
// valid resource pointers.
//
void
switch_order_rsarray(rlist_p_t rss[])
{
	rlist_p_t *rsp1, *rsp2;
	int i, j;
	rdeplist_t *nl;

	if (rss == NULL || *rss == NULL)
		return;

	// First, shift all network resources to the top of the list.
	switch_shift_netrs(rss);

	//
	// Second, for any resource in the list, compare its dependency list to
	// the resources which follow it in the array.  As soon as a match is
	// found, remove the resource from its old location and insert it just
	// after its dependency.
	//

	i = 0;
	rsp1 = rss;
	while (*rsp1) {
rs_moved_forward:
		//
		// Compare each dependency to each resource
		// remaining in the array.
		//
		for (nl = (*rsp1)->rl_ccrdata->r_dependencies.dp_restart;
		    nl; nl = nl->rl_next) {
			// Start with next element in array
			j = i + 1;
			rsp2 = rsp1 + 1;

			// Compare to each remaining array element
			while (*rsp2) {
				if (strcmp(nl->rl_name,
				    (*rsp2)->rl_ccrdata->r_name) == 0) {
					// Found a match - move it
					switch_move_element_forward(i, j,
					    (void **)rss);
					goto rs_moved_forward; //lint !e801
				}
				++j;
				++rsp2;
			}
		}
		for (nl = (*rsp1)->rl_ccrdata->
		    r_dependencies.dp_offline_restart;
		    nl; nl = nl->rl_next) {
			// Start with next element in array
			j = i + 1;
			rsp2 = rsp1 + 1;

			// Compare to each remaining array element
			while (*rsp2) {
				if (strcmp(nl->rl_name,
				    (*rsp2)->rl_ccrdata->r_name) == 0) {
					// Found a match - move it
					switch_move_element_forward(i, j,
					    (void **)rss);
					goto rs_moved_forward; //lint !e801
				}
				++j;
				++rsp2;
			}
		}

		for (nl = (*rsp1)->rl_ccrdata->r_dependencies.dp_strong;
		    nl; nl = nl->rl_next) {
			// Start with next element in array
			j = i + 1;
			rsp2 = rsp1 + 1;

			// Compare to each remaining array element
			while (*rsp2) {
				if (strcmp(nl->rl_name,
				    (*rsp2)->rl_ccrdata->r_name) == 0) {
					// Found a match - move it
					switch_move_element_forward(i, j,
					    (void **)rss);
					goto rs_moved_forward; //lint !e801
				}
				++j;
				++rsp2;
			}
		}

		for (nl = (*rsp1)->rl_ccrdata->r_dependencies.dp_weak;
		    nl; nl = nl->rl_next) {
			// Start with next element in array
			j = i + 1;
			rsp2 = rsp1 + 1;

			// Compare to each remaining array element
			while (*rsp2) {
				if (strcmp(nl->rl_name,
				    (*rsp2)->rl_ccrdata->r_name) == 0) {
					// Found a match - move it
					switch_move_element_forward(i, j,
					    (void **)rss);
					goto rs_moved_forward; //lint !e801
				}
				++j;
				++rsp2;
			}
		}
		++i;
		++rsp1;
	}
}


//
// switch_order_rgarray()
//
// This function is equivalent to switch_order_rsarray(), however
// the input array contains RG pointers; this function orders the
// array by RG_dependencies and strong affinities; that is, no resource
// group in the ordered list will have an RG_dependency or strong
// affinity on any resource group which follows it in the list.
//
// When considering RG dependencies, we include "implicit" dependencies
// of a scalable service RG upon its shared address RG.
//
// The input array is NULL terminated and must contain only
// valid RG pointers.
//
// Caller should check for returned SCHA_ERR_NOMEM error.
//
scha_errmsg_t
switch_order_rgarray(rglist_p_t rgs[])
{
	rglist_p_t *rgp1, *rgp2;
	int i, j;
	namelist_t *nl;
	namelist_t *dependee_rgs = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (rgs == NULL || *rgs == NULL)
		return (res);

	//
	// For each RG in the array, compare its RG dependencies and
	// strong affinities to the RGs which follow it in the array.
	// If a match is found, remove the resource group from its old
	// location and insert it just after its dependency.
	//

	i = 0;
	rgp1 = rgs;
	while (*rgp1) {

rg_moved_forward:
		if (dependee_rgs) {
			rgm_free_nlist(dependee_rgs);
			dependee_rgs = NULL;
		}

		//
		// Compute the list of dependee RGs including RG_dependencies
		// plus "implict" dependencies of scalable service RG upon
		// SharedAddress RG.
		//
		dependee_rgs = add_sa_dependee_rgs_to_list(&res, *rgp1, NULL);
		if (res.err_code != SCHA_ERR_NOERR) {
			goto alldone; //lint !e801
		}

		//
		// Compare each RG dependency and strong affinity to each
		// resource group remaining in the array.
		//

		for (nl = dependee_rgs; nl; nl = nl->nl_next) {
			// Start with next element in array
			j = i + 1;
			rgp2 = rgp1 + 1;

			// Compare to each remaining array element
			while (*rgp2) {
				if (strcmp(nl->nl_name,
				    (*rgp2)->rgl_ccr->rg_name) == 0) {
					// Found a match - move it
					switch_move_element_forward(i, j,
					    (void **)rgs);
					goto rg_moved_forward; //lint !e801
				}
				++j;
				++rgp2;
			}
		}

		for (nl = (*rgp1)->rgl_affinities.ap_strong_pos; nl;
		    nl = nl->nl_next) {
			// Start with next element in array
			j = i + 1;
			rgp2 = rgp1 + 1;

			// Compare to each remaining array element
			while (*rgp2) {
				if (strcmp(nl->nl_name,
				    (*rgp2)->rgl_ccr->rg_name) == 0) {
					// Found a match - move it
					switch_move_element_forward(i, j,
					    (void **)rgs);
					goto rg_moved_forward; //lint !e801
				}
				++j;
				++rgp2;
			}
		}

		for (nl = (*rgp1)->rgl_affinities.ap_strong_neg; nl;
		    nl = nl->nl_next) {
			// Start with next element in array
			j = i + 1;
			rgp2 = rgp1 + 1;

			// Compare to each remaining array element
			while (*rgp2) {
				if (strcmp(nl->nl_name,
				    (*rgp2)->rgl_ccr->rg_name) == 0) {
					// Found a match - move it
					switch_move_element_forward(i, j,
					    (void **)rgs);
					goto rg_moved_forward; //lint !e801
				}
				++j;
				++rgp2;
			}
		}

		++i;
		++rgp1;
	}
alldone:
	if (dependee_rgs) {
		rgm_free_nlist(dependee_rgs);
	}
	return (res);
}


//
// reverse_switch_order_array
//
// This function reverses the order of the input list
// by switching the first and last elements, second and second-to-last, etc.
//
void
reverse_switch_order_array(void **array)
{
	void	**ptr, *rp;
	int	i, count = 0;

	for (ptr = array; *ptr; ptr++)
		count++;

	for (i = 0; i < count / 2; i++) {
		rp = array[i];
		array[i] = array[count - i - 1];
		array[count - i - 1] = rp;
	}
}

//
// switch_shift_netrs
//
// Input: NULL terminated array of rlist_p_t pointers.
// Action: examine each resource to see if it's a
//	network resource (rt->rt_sysdeftype ==
//		SYST_LOGICAL_HOSTNAME | SYST_SHARED_ADDRESS)
//	and if it is, shift it to up array[0]
// Result: all network resources will be at top of array
//
void
switch_shift_netrs(rlist_p_t rss[])
{
	rlist_p_t	*rsp;
	rlist_p_t	ptr;
	int		idx, i;

	if (rss == NULL || *rss == NULL)
		return;

	//
	// for elts 1 and greater in rss[]
	// if elt is netrs
	//	shift to [0]
	//

	rsp = rss;
	// no need to check elt 0
	for (idx = 1, ++rsp; *rsp; ++idx, ++rsp) {
		// is network rt ?
		if ((*rsp)->rl_ccrtype->rt_sysdeftype ==
		    SYST_LOGICAL_HOSTNAME ||
		    (*rsp)->rl_ccrtype->rt_sysdeftype ==
		    SYST_SHARED_ADDRESS) {
			// Move element to the top
			// Save the target element for later reinsertion
			i = idx;
			ptr = rss[i];

			// run the downshift
			for (; i > 0; i--) {
				rss[i] = rss[i - 1];
			}

			// reinsert the target element
			rss[0] = ptr;
		}
	}
}


//
// switch_move_element_forward
//
// This function moves a pointer found at "src_index"
// to a position immediately following the "dst_index" in the given
// array of pointers.
//
void
switch_move_element_forward(int src_index, int dst_index, void *array[])
{
	int i;
	void *ptr;

	// Check arguments
	if (src_index < 0 || src_index >= dst_index)
		return;

	// Save the original item for later insertion
	ptr = array[src_index];

	// Begin the shift
	for (i = src_index; i < dst_index; i++) {
		array[i] = array[i + 1];
	}

	// Insert
	array[dst_index] = ptr;
}


//
// rgs_are_stopping
//
// Run on president, while holding rgm state lock.
//
// For each RG in the NULL-teminated array rglistp, check its state on all
// LNIs in evac_nodeset.
//
// If the RG is no longer pending offline (or PENDING_OFF_STOP_FAILED) on
// any node in the nodeset, remove those nodes from the RG's rgl_evac_nodes
// nodeset.
//
// If any of the RGs in rglistp are still pending offline, return B_TRUE.
// Otherwise, return B_FALSE.
//
// When returning B_FALSE:
//   If any RG was found to be in ERROR_STOP_FAILED state, set a
//   SCHA_ERR_STOPFAILED error code and REM_RG_STOPFAILED message in the
//   out parameter res.
//
//   else if any RG was found to be in a state other than OFFLINE, return a
//   SCHA_ERR_RECONF error code and REM_EVAC_FAILED message in res.
//
// If swap space is exhausted, return B_FALSE with res->err_code set to
// SCHA_ERR_NOMEM.
//
static boolean_t
rgs_are_stopping(scha_errmsg_t *res, const rglist_p_t *rglistp,
    const LogicalNodeset &evac_nodeset, bool verbose_flag)
{
	uint_t i;
	rgm::rgm_rg_state rgx_state;
	boolean_t any_stop_failed = B_FALSE;	// some rg is error_stop_failed
	boolean_t any_reconfig = B_FALSE;	// some rg is "online-ish"
	char *rgname;
	char *stop_failed_rgs = NULL;
	char *tmp_s = NULL;
	size_t rgs_len = 1;
	boolean_t retval = B_FALSE;
	rgm::lni_t lni = 0;

	// Sanity check on rglistp
	if (rglistp == NULL)
		return (retval);

	for (i = 0; rglistp[i] != NULL; i++) {
		rgname = rglistp[i]->rgl_ccr->rg_name;
		CL_PANIC(rgname != NULL);

		for (lni = 0; (lni = evac_nodeset.nextLniSet(lni + 1)) != 0; ) {
			//
			// If set true, clear_evac indicates the rg is no longer
			// evacuating from lni and its evac flags need to
			// be reset
			//
			boolean_t clear_evac = B_FALSE;

			rgx_state = rglistp[i]->rgl_xstate[lni].rgx_state;

			switch (rgx_state) {

			case rgm::RG_PENDING_OFFLINE:
			case rgm::RG_PENDING_OFF_STOP_FAILED:
			case rgm::RG_PENDING_OFF_START_FAILED:
			case rgm::RG_OFF_PENDING_METHODS:
			case rgm::RG_OFF_PENDING_BOOT:
			case rgm::RG_ON_PENDING_METHODS:
				//
				// Methods are still running; evacuation is not
				// finished yet.  Exit early with B_TRUE.
				//
				// In the case of ON_PENDING_METHODS, this is a
				// postponed stop; the RG state will move to
				// PENDING_OFFLINE after the methods complete.
				// [If for some reason the RG state moves to
				// ONLINE instead, we will terminate with an
				// error below.]
				//
				retval = B_TRUE;
				goto bailout; //lint !e801

			case rgm::RG_ERROR_STOP_FAILED:
				//
				// Append rg name to comma separated list of
				// rgnames that are ERROR_STOP_FAILED on lni.
				// If we get a NOMEM error we won't be able to
				// produce a useful error message string, but
				// we continue in order to properly clear the
				// evacuation flags.
				//
				rgs_len += strlen(rgname);
				if (stop_failed_rgs == NULL) {
					stop_failed_rgs =
					    strdup_nocheck(rgname);
					if (stop_failed_rgs == NULL) {
						res->err_code = SCHA_ERR_NOMEM;
					}
				} else {
					// 2 extra bytes are for ", "
					rgs_len += 2;
					tmp_s = (char *)realloc(stop_failed_rgs,
					    rgs_len);
					if (tmp_s == NULL) {
						res->err_code = SCHA_ERR_NOMEM;
					} else {
						stop_failed_rgs = tmp_s;
						(void) strcat(stop_failed_rgs,
						    ", ");
						(void) strcat(stop_failed_rgs,
						    rgname);
					}
				}
				any_stop_failed = B_TRUE;
				clear_evac = B_TRUE;
				break;

			case rgm::RG_OFFLINE:
			case rgm::RG_OFF_BOOTED:
			case rgm::RG_OFFLINE_START_FAILED:
				clear_evac = B_TRUE;
				//
				// No need to call rebalance here, because it
				// has already been called by process_needy_rg()
				// when set_rgx_state() set the RG state to
				// offline.
				// rebalance() will have excluded the evacuating
				// node from candidacy.
				//
				break;

			//
			// The following states indicate a failure of the
			// evacuation, presumably caused by a failure elsewhere
			// in the cluster that caused RG to fail over to
			// lni.
			//
			case rgm::RG_ONLINE:
			case rgm::RG_PENDING_ONLINE:
			case rgm::RG_PENDING_ON_STARTED:
			case rgm::RG_ON_PENDING_DISABLED:
			case rgm::RG_ON_PENDING_MON_DISABLED:
			case rgm::RG_ON_PENDING_R_RESTART:
			case rgm::RG_PENDING_ONLINE_BLOCKED:
				ucmm_print("RGM",
				    NOGET("rgs_are_stopping: RG %s state %s"),
				    rgname, pr_rgstate(rgx_state));
				any_reconfig = B_TRUE;
				clear_evac = B_TRUE;
				LogicalNode *ln = lnManager->findObject(lni);
				CL_PANIC(ln);
				res->err_code = SCHA_ERR_RECONF;
				rgm_format_errmsg(res, REM_EVAC_FAILED,
				    ln->getFullName());
			}

			//
			// If this RG is done evacuating on this lni, reset the
			// rgl_evac_nodes bit.
			//
			if (clear_evac) {
				rglistp[i]->rgl_evac_nodes.delLni(lni);
			}
		}	// for each LNI
		if (verbose_flag) {
			LogicalNodeset online_ns, test_ns;
			get_rg_online_nodeset(rglistp[i], &online_ns);
			test_ns = (online_ns & evac_nodeset);
			if (test_ns.isEmpty()) {
				rgm_format_successmsg(res, RWM_RG_STOPPED,
				    rgname);
			}
		}

	}	// for each RG

	//
	// Now all RGs are done evacuating.  If any RGs
	// are STOP_FAILED, return an error.  REM_EVAC_FAILED errors
	// were already appended to res above.
	//
	// Note, STOPFAILED error will overwrite RECONF error.
	//
	if (any_stop_failed) {
		res->err_code = SCHA_ERR_STOPFAILED;
		if (stop_failed_rgs != NULL) {
			rgm_format_errmsg(res, REM_RG_STOPFAILED,
			    stop_failed_rgs);
		}
	}

bailout:
	//
	// Suppress the following bogus lint warning:
	//	Warning(644) [c:29]: stop_failed_rgs (line 6159) may not
	//	have been initialized
	//
	free(stop_failed_rgs);	//lint !e644
	return (retval);
}


//
// Returns TRUE if rg is quiesced; else returns FALSE.
// It doesn't take care of removing
// rgs from list, nor does it clear the nameserver tag if the
// rgs are quiesced. A separate call to clear_nameserver_tags()
// is required.
//
static boolean_t
is_rg_quiesced(const rglist_p_t rg_ptr,
	LogicalNodeset &start_failed_nodes,
	LogicalNodeset &stop_failed_nodes)
{
	rgm::rgm_rg_state rgx_state;
	rg_switch_t	rg_sw;
	LogicalNodeset	start_failed_ns;
	LogicalNodeset	stop_failed_ns;


	rg_sw = rg_ptr->rgl_switching;

	// Lets check switching, if going on, lets
	// go to the next RG.
	if (rg_sw == SW_IN_PROGRESS ||
	    rg_sw == SW_IN_FAILBACK ||
	    rg_sw == SW_EVACUATING) {
		return (B_FALSE);
	}

	//
	// Iterate through the rgx_state map.
	//
	/* CSTYLED */
	for (std::map<rgm::lni_t, rg_xstate>::iterator it =
	    rg_ptr->rgl_xstate.begin(); it != rg_ptr->rgl_xstate.end(); ++it) {

		rgm::lni_t node = it->first;

		if (!in_current_logical_membership(node)) {
			continue;
		}

		rgx_state = it->second.rgx_state;
		switch (rgx_state) {
		case rgm::RG_PENDING_ONLINE:
		case rgm::RG_PENDING_OFFLINE:
		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_BOOT:
		case rgm::RG_PENDING_OFF_STOP_FAILED:
		case rgm::RG_PENDING_OFF_START_FAILED:
		case rgm::RG_PENDING_ON_STARTED:
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_ON_PENDING_MON_DISABLED:
		case rgm::RG_ON_PENDING_R_RESTART:
			return (B_FALSE);
		case rgm::RG_OFFLINE_START_FAILED:
			start_failed_ns.addLni(node);
			break;
		case rgm::RG_ERROR_STOP_FAILED:
			stop_failed_ns.addLni(node);
			break;
		case rgm::RG_ONLINE:
		case rgm::RG_OFFLINE:
		case rgm::RG_OFF_BOOTED:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
			break;
		default:
			ucmm_print("RGM", NOGET("Unknown RG state:%d\n"),
			    rgx_state);
			CL_PANIC(B_FALSE);
		}
	}
	start_failed_nodes = start_failed_ns;
	stop_failed_nodes = stop_failed_ns;
	return (B_TRUE);
}

//
// rgs_are_rebalancing
//
// Run on president, while holding rgm state lock.
//
// The NULL-terminated array rglistp contains pointers to a set of RGs
// that have just been rebalanced, either by user command or following a
// node evacuation.  If any RG in the list appears to be still in the
// process of rebalancing, return B_TRUE; otherwise return B_FALSE.
// For each RG that is no longer rebalancing, clear the rgl_switching flag
// on that RG (unless it is still evacuating) and delete it from rglistp.
// If the switch on an RG is "busted", or if a resource ended up in
// start_failed or mon_failed state, append an error message for that RG
// and set an error code in the 'res' argument (like do_rg_switch does).
//
// An RG that is PENDING_ONLINE_BLOCKED on some node (but not PENDING_ONLINE
// on any node) is considered to still be rebalancing, as long as at least
// one other RG in the list is pending online.  The blocked RG may become
// unblocked in the course of bringing other RGs online.
//
// If swap space is exhausted, return B_FALSE with res->err_code set to
// SCHA_ERR_NOMEM.
//
// The caller guarantees that rglistp is non-NULL and that rglistp[] is
// NULL-terminated.
//
// This function modifies the array pointed to by rglistp, deleting each
// RG pointer from the list as that RG has finished rebalancing.
// "Deleting" here means re-writing the pointers in the array, but not
// changing the memory allocation of the array itself.
//
// When the function returns false (indicating that no more RGs in rglistp
// are rebalancing), only PENDING_ONLINE_BLOCKED RGs (if any) remain in
// rglistp.
//
// verbose_flag is used to print the success message.If the switch_back
// flag is true (rg remastering option) the success message for rebalance
// is not printed.
//
static boolean_t
rgs_are_rebalancing(scha_errmsg_t *res, rglist_p_t *rglistp,
    namelist_t *failed_op_list, bool skip_suspended, bool verbose_flag,
    boolean_t switch_back)
{
	rgm::rgm_rg_state rgx_state;
	nodeidlist_t *nlist;
	boolean_t any_rg_rebalancing = B_FALSE;
	scha_err_t start_err = SCHA_ERR_NOERR;
	int numrgs = 0;
	int i, j;

	//
	// Iterate through all of the rgs in the list
	//
	for (i = 0; rglistp[i] != NULL; i++) {
		boolean_t is_starting = B_FALSE;
		scha_err_t tmp_err;

		// Skip failed RG
		if (namelist_exists(failed_op_list,
		    rglistp[i]->rgl_ccr->rg_name))
			continue;


		numrgs++;

		//
		// Check for the switch on this RG being "busted".  If so,
		// we need to append an error message for this RG to res
		// (this is done just after the below for loop); and
		// clear the rgl_switching flag; the rg will be deleted from
		// rglistp below.
		//
		tmp_err = map_busted_err(rglistp[i]->rgl_switching);

		// See if RG is starting on any node in its Nodelist.
		for (nlist = rglistp[i]->rgl_ccr->rg_nodelist; nlist != NULL;
		    nlist = nlist->nl_next) {
			LogicalNode *ln = lnManager->findObject(nlist->nl_lni);

			// Skip nodes not in membership
			if (ln == NULL || !ln->isInCluster()) {
				continue;
			}

			// Skip node if it is evacuating this RG
			if (node_is_evacuating(nlist->nl_lni, rglistp[i])) {
				continue;
			}

			rgx_state = rglistp[i]->rgl_xstate[nlist->nl_lni].
			    rgx_state;

			//
			// We must include RG_PENDING_OFF_START_FAILED in our
			// list of starting/rebalancing states because the
			// switch has not yet been busted at the time this state
			// is set.This state will always be followed soon by
			// OFFLINE_START_FAILED, at which point the switch will
			// be properly busted
			//
			if (rgx_state == rgm::RG_PENDING_ONLINE ||
			    rgx_state == rgm::RG_PENDING_OFF_START_FAILED ||
			    rgx_state == rgm::RG_PENDING_ON_STARTED) {
				is_starting = B_TRUE;
				any_rg_rebalancing = B_TRUE;
				break;
			}
			if (rgx_state == rgm::RG_PENDING_ONLINE_BLOCKED) {
				//
				// Don't delete this RG -- it might become
				// unblocked.  Continue checking its state on
				// other nodes.
				//
				is_starting = B_TRUE;
				continue;
			}
		}

		if (!is_starting) {
			//
			// This RG is no longer rebalancing -- we will
			// remove it from rglistp after resetting
			// rgl_switching, resetting rgl_planned_on_nodes,
			// and checking for start errors.
			//
			if (rglistp[i]->rgl_switching != SW_EVACUATING ||
			    rglistp[i]->rgl_evac_nodes.isEmpty()) {
				rglistp[i]->rgl_switching = SW_NONE;
			}
			rglistp[i]->rgl_planned_on_nodes.reset();

			if (tmp_err != SCHA_ERR_NOERR) {
				res->err_code = SCHA_ERR_NOERR;
				append_busted_msg(res, rglistp[i], tmp_err);
				if (res->err_code != SCHA_ERR_NOERR) {
					// NOMEM or INTERNAL error occured.
					goto bailout; //lint !e801
				}
			}

			//
			// Check for start_failed, mon_failed, or "RG
			// remains offline" errors.
			// Note, check_starterrs can crash the node if we run
			// out of swap space.  So in some cases, we might not
			// return a NOMEM error.  This is not very serious,
			// because depletion of swap space is usually fatal
			// to the node anyway.
			//
			check_starterrs(res, rglistp[i], B_TRUE);
			if (res->err_code == SCHA_ERR_STARTFAILED ||
			    res->err_code == SCHA_ERR_NOMASTER) {
				start_err = res->err_code;
				//
				// Reset the error code for now.  We will
				// set it back to start_err below, if no
				// more serious error occurs.
				//
				res->err_code = SCHA_ERR_NOERR;
			} else if (res->err_code != SCHA_ERR_NOERR) {
				// a NOMEM or INTERNAL error was caught
				goto bailout; //lint !e801
			}

			//
			// RG is no longer rebalancing -- remove it from the
			// list by shifting higher array elements down.
			//
			ucmm_print("RGM", NOGET("rgs_are_rebalancing: "
			    "finished processing RG <%s>\n"),
			    rglistp[i]->rgl_ccr->rg_name);

			if ((!switch_back) && (verbose_flag))
				rgm_format_successmsg(res, RWM_RG_REBALANCED,
				    rglistp[i]->rgl_ccr->rg_name);
			for (j = i; rglistp[j] != NULL; j++) {
				rglistp[j] = rglistp[j + 1];
			}
			//
			// Decrement i so that we do not skip the next element
			// which has replaced the current element at position i.
			//
			i--;
		}
	}

	//
	// If any_rg_rebalancing is false this means that only
	// PENDING_ONLINE_BLOCKED RGs remain in rglistp.
	// We return B_FALSE.  The caller will clear the rgl_switching flags
	// and rgl_planned_on_nodes nodeset of these remaining RGs.
	//
	if (!any_rg_rebalancing) {
		for (i = 0; rglistp[i] != NULL; i++) {
			ucmm_print("RGM", NOGET("rgs_are_rebalancing: "
			    "finished processing blocked RG <%s>\n"),
			    rglistp[i]->rgl_ccr->rg_name);
		}
	}

bailout:
	//
	// If no more serious error occurred, set start error in res
	//
	if (res->err_code == SCHA_ERR_NOERR) {
		if (start_err != SCHA_ERR_NOERR) {
			res->err_code = start_err;
		}
	}
	return (any_rg_rebalancing);
}


//
// rglist_add_rg
//
// Add the rglist_p_t pointer rg_ptr to the NULL-terminated array rglistp
// which currently contains 'numrgs' elements (not counting the terminating
// NULL).  rglistp may be NULL, in which case numrgs must be zero.
//
// Returns {SCHA_ERR_NOMEM} if swap space is exhausted, otherwise returns
// {SCHA_ERR_NOERR}.
//
// Allocates (or reallocates) memory for the new entry.  The reference
// argument rglistp is modified if SCHA_ERR_NOERR is returned.
// The caller is responsible for freeing rglistp.
//
// May be called on president or slave.  If rg_ptr is part of the rgm
// in-memory state, then the state lock must be held by the caller.
//
scha_errmsg_t
rglist_add_rg(rglist_p_t *&rglistp, const rglist_p_t rg_ptr, uint_t numrgs)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t *rglp_temp;

	//
	// If rglistp is NULL, we don't call realloc() since it would
	// use the hijacked version of malloc.  We want to call the
	// non-hijacked version (malloc_nocheck) so that we can
	// check for and return a NOMEM error rather than aborting.
	//
	if (rglistp == NULL) {
		CL_PANIC(numrgs == 0);
		rglp_temp = (rglist_p_t *)malloc_nocheck(
		    2 * sizeof (rglist_p_t));
	} else {
		rglp_temp = (rglist_p_t *)realloc(rglistp,
		    (2 + numrgs) * sizeof (rglist_p_t));
	}
	if (rglp_temp == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}
	rglistp = rglp_temp;
	rglistp[numrgs] = rg_ptr;
	rglistp[numrgs + 1] = NULL;

	return (res);
}


//
// set_ok_to_start
//
// If the current rgl_ok_to_start=FALSE on the given RG, add Ok_To_Start
// in ccr table and latch an intention to reset the in-memory
// flag rgl_ok_to_start to TRUE on all slaves to restore
// the normal failover behavior.
//
// Called only on the president, while holding the state lock
//
static scha_errmsg_t
set_ok_to_start(rglist_p_t rgp)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	LogicalNodeset emptyNodeset;

	// Check if flag is already set
	if (rgp->rgl_ok_to_start)
		return (res);

	//
	// Latch the intention on all slaves to reset
	// rgl_ok_to_start in memory.
	//
	ucmm_print("RGM", NOGET("set_ok_to_start(): RG <%s> Latching\n"),
	    rgp->rgl_ccr->rg_name);

	(void) latch_intention(rgm::RGM_ENT_RG, rgm::RGM_OP_UPDATE,
	    rgp->rgl_ccr->rg_name, &emptyNodeset, rgm::INTENT_OK_FLAG);

	res = rgmcnfg_set_ok_start_rg(rgp->rgl_ccr->rg_name, B_TRUE, ZONE);
	if (res.err_code != SCHA_ERR_NOERR)
		ucmm_print("RGM", NOGET("set_ok_to_start: "
		    "RG <%s> CCR Failed\n"), rgp->rgl_ccr->rg_name);
	else {
		ucmm_print("RGM", NOGET(
		    "set_ok_to_start: RG <%s> Processing\n"),
		    rgp->rgl_ccr->rg_name);
		(void) process_intention();
	}

	//
	// Unlatch the intention on all slaves
	//
	(void) unlatch_intention();

	return (res);
}


//
// quiesce_set_nameserver_tags
// takes a list of rglist pointers, number of rgs and sets nameserver tag
// for all non endish rgs.  Note that in_endish_rg_state also checks
// rgl_switching flag.
// This tag is checked during start and stop failures. While this tag is
// set, rebalance calls on the RGs are suppressed, so failovers are suppressed
// and the RG might end up offline at the end of the quiesce operation.
// IF QUIESCE fails for a particular RG, it clears the nameserver tag for all
// the RGs and returns error. UNMANAGED RGs if passed, may or maynot be
// endish; and hence quiesce tag may be set.
//
static scha_errmsg_t
quiesce_set_nameserver_tags(rglist_p_t *rglistp, uint_t numrgs,
    boolean_t is_fast_quiesce,
    scha_errmsg_t *warning_res)
{

	uint_t i;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	warning_res->err_code = SCHA_ERR_NOERR;
	for (i = 0; i < numrgs; i++) {
		LogicalNodeset pending_boot_ns;
		LogicalNodeset r_restart_ns;

		if (in_endish_rg_state(rglistp[i], &pending_boot_ns,
		    NULL, NULL, &r_restart_ns) && (pending_boot_ns |
			r_restart_ns).isEmpty()) {
			ucmm_print("quiesce_set_nameserver_tags", NOGET(
			    "quiescing RG <%s> is not busy\n"),
			    rglistp[i]->rgl_ccr->rg_name);
			continue;
		}

		if (rglistp[i]->rgl_ccr->rg_system) {
			rgm_format_errmsg(warning_res, RWM_SYSTEM_RG_QUIESCE,
			    rglistp[i]->rgl_ccr->rg_name);
		}
		//
		// if we want to issue messages for all rgs passed irrespective
		// of the fact that tag is set or not, then we should move the
		// above piece of code before endish state check.

		if (warning_res->err_code != SCHA_ERR_NOERR) {
			res.err_code = warning_res->err_code;
		} else {
			res = quiesce_set_nameserver(rglistp[i],
			    is_fast_quiesce);
		}
		if (res.err_code != SCHA_ERR_NOERR) {
			for (int j = i; j >= 0; j--) {
				quiesce_clear_nameserver(rglistp[j],
				    is_fast_quiesce);
			}
			return (res);
		}
	}

	return (res);
}

//
// quiesce_set_nameserver helper function.
//
// Set the nameserver tag for the specified
// RG.
//
static scha_errmsg_t
quiesce_set_nameserver(rglist_p_t rgp, boolean_t is_fast_quiesce)
{
	char rgtag[MAXRGMOBJNAMELEN + 11];
	scha_errmsg_t scha_error = {SCHA_ERR_NOERR, NULL };
	int r_val = 0;

	(void) memset(rgtag, 0, MAXRGMOBJNAMELEN + 11);
	if (is_fast_quiesce) {
		(void) sprintf(rgtag, "fastforce_%s", rgp->rgl_ccr->rg_name);
	} else {
		(void) sprintf(rgtag, "slowforce_%s", rgp->rgl_ccr->rg_name);
	}

	r_val = cl_rebind(rgtag, "TRUE");

	if (r_val != 0) {
		scha_error.err_code = SCHA_ERR_INTERNAL;
		rgm_format_errmsg(&scha_error, REM_RG_NAMESERVER,
		    rgp->rgl_ccr->rg_name);
	} else {
		rgp->rgl_quiescing = B_TRUE;
	}
	return (scha_error);

}

//
// quiesce_clear_nameserver_tags
// for a list of rgs
//
static void
quiesce_clear_nameserver_tags(rglist_p_t *rglistp, boolean_t is_fast_quiesce)
{
	uint_t i;
	for (i = 0; rglistp[i] != NULL; i++) {
		quiesce_clear_nameserver(rglistp[i], is_fast_quiesce);
	}
}

//
// Clear the nameserver tag for the specified RG.
// Only called on president.
//
void
quiesce_clear_nameserver(rglist_p_t rgp, boolean_t is_fast_quiesce)
{
	char rgtag[MAXRGMOBJNAMELEN + 11];

	(void) memset(rgtag, 0, MAXRGMOBJNAMELEN + 11);
	if (is_fast_quiesce) {
		(void) sprintf(rgtag, "fastforce_%s", rgp->rgl_ccr->rg_name);
	} else {
		(void) sprintf(rgtag, "slowforce_%s", rgp->rgl_ccr->rg_name);
	}
	(void) cl_unbind(rgtag);
	rgp->rgl_quiescing = B_FALSE;
}
#endif // !EUROPA_FARM


// queries name server for the given tag
static boolean_t
tag_exists_in_nameserver(char *tag)
{
	char *forcestop = NULL; // Forcestop nameserver result tag
	int cl_err;
#ifdef EUROPA_FARM
	//
	// Unable to access directly to nameserver
	// from a farm node.
	// Send a remote query to a server node nameserver.
	//
	cl_err = rgmcnfg_cl_resolve(tag, &forcestop);
#else
	cl_err = cl_resolve(tag, &forcestop);
#endif

	if (cl_err != 0)
		return (B_FALSE);

	if (forcestop == NULL) {
		return (B_FALSE);
	}

	if (strcmp(forcestop, "TRUE") != 0) {
		free(forcestop);
		return (B_FALSE);
	}
	// Its a forcestop

	free(forcestop);
	return (B_TRUE);
}

//
// The following functions check if quiesce is running
// for the given rg. We rely on the nameserver because
// the rgl_quiescing flag is set only on the president.
//

boolean_t
is_slow_quiesce_running(const char *rgname)
{
	char tag[MAXRGMOBJNAMELEN + 11];
	memset(tag, 0, MAXRGMOBJNAMELEN + 11);
	sprintf(tag, "slowforce_%s", rgname);
	return (tag_exists_in_nameserver(tag));
}

boolean_t
is_fast_quiesce_running(const char *rgname)
{
	char tag[MAXRGMOBJNAMELEN + 11];
	memset(tag, 0, MAXRGMOBJNAMELEN + 11);
	sprintf(tag, "fastforce_%s", rgname);
	return (tag_exists_in_nameserver(tag));
}

//
// rglist_remove_rg
//
// "Remove" element i from null-terminated pointer list rglistp by shifting
// higher-numbered elements down into its position.
//
void
rglist_remove_rg(rglist_p_t *rglistp, uint_t i)
{
	for (uint_t j = i; rglistp[j] != NULL; j++) {
	rglistp[j] = rglistp[j + 1];
	}
}


//
// add_sa_dependee_rgs_to_list
//
// For each scalable service resource in the group rg_p, find all
// SharedAddress dependees of the scalable service.  Add the RG
// names of such SharedAddresses to a fresh copy of rg_p's rg_dependencies
// list, eliminating duplicates, and return the expanded list.
//
// The caller is responsible for freeing the memory allocated for the
// returned value.
//
// Non-zero error codes returned in 'res' are fatal.  Caller should check
// the error code before using the return value.
//
// This function is also called during resource property update(from
// rs_sanity_checks function). In that case, this function computes the
// new implied RG dependencies based on the shared address dependencies
// of the proposed scalable resource rs.
//
// The RGM state mutex must be held by the caller.
//
namelist_t *
add_sa_dependee_rgs_to_list(scha_errmsg_t *res, const rglist_p_t rg_p,
    rgm_resource_t *rs)
{
	namelist_t *nl;
	rlist_p_t r, r_dep;
	namelist_t *SA_deps = NULL;
	namelist_t *dep_list = NULL;


	//
	// Make a deep copy of rg_p's rg_dependencies if it is non-null
	//
	if ((nl = rg_p->rgl_ccr->rg_dependencies) != NULL) {
		dep_list = namelist_copy(nl);
		//
		// librgmserver version of namelist_copy() uses
		// interposed malloc, so we shouldn't have to check
		//  for NULL return. Do so anyway for robustness.
		//
		if (dep_list == NULL) {
			res->err_code = SCHA_ERR_NOMEM;
			return (NULL);
		}
	}

	if (rs != NULL && is_r_scalable(rs)) {
		// Get a list of rs's SharedAddress network resource dependees
		SA_deps = NULL;
		compute_NRUlst_frm_deplist(&rs->r_dependencies,
		    &SA_deps, B_TRUE);

		// Iterate through the SharedAddress dependees, adding their
		// RG's names to dep_list
		for (nl = SA_deps; nl != NULL; nl = nl->nl_next) {
			r_dep = rname_to_r(NULL, nl->nl_name);
			if (r_dep == NULL) {
				// Should never happen
				continue;
			}
			if (!namelist_exists(dep_list,
			    r_dep->rl_ccrdata->r_rgname)) {
				*res = namelist_add_name(&dep_list,
				    r_dep->rl_ccrdata->r_rgname);
				if (res->err_code != SCHA_ERR_NOERR) {
					// NOMEM or internal error -- fatal
					goto bailout; //lint !e801
				}
			}
		}
	} else if (rs == NULL) {
		//
		// Iterate through rg_p's resources to find scalable services.
		// For each one found, add the RG of its dependee SharedAddress
		// resource(s) to dep_list.
		//
		for (r = rg_p->rgl_resources; r != NULL; r = r->rl_next) {
			if (!is_r_scalable(r->rl_ccrdata)) {
				continue;
			}
			//
			// Get a list of r's SharedAddress network resource
			// dependees
			//
			SA_deps = NULL;
			compute_NRUlst_frm_deplist(
			    &r->rl_ccrdata->r_dependencies, &SA_deps, B_TRUE);

			// Iterate through the SharedAddress dependees, adding
			// their RG's names to dep_list
			for (nl = SA_deps; nl != NULL; nl = nl->nl_next) {
				r_dep = rname_to_r(NULL, nl->nl_name);
				if (r_dep == NULL) {
					// Should never happen
					continue;
				}
				if (!namelist_exists(dep_list,
				    r_dep->rl_ccrdata->r_rgname)) {
					*res = namelist_add_name(&dep_list,
					    r_dep->rl_ccrdata->r_rgname);
					if (res->err_code != SCHA_ERR_NOERR) {
						// NOMEM or internal error
						// -- fatal
						goto bailout; //lint !e801
					}
				}
			}
		}
	}
bailout:
	rgm_free_nlist(SA_deps);
	return (dep_list);
}

//
// This function checks for duplicate operands in the input operand
// list and prunes them to create a valid list. The checking_r flag is
// set to true if the caller is passing a resource list (in case of
// idl_scswitch_onoff, idl_scswitch_clear); it is set to false
// if the caller is passing a rg list (idl_scswitch_primaries,
// idl_scswitch_restart_rg, idl_unmanaged_to_offline,
// idl_offline_to_unmanaged).
//
scha_errmsg_t
prune_operand_list(const rgm::idl_string_seq_t inlist,
    rgm::idl_string_seq_t &outlist, boolean_t checking_r)
{
	int		i, k = 0;
	rlist_p_t	r_ptr = NULL;
	rglist_p_t	rg_ptr = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t	*rglistp = NULL; // null-term-ed array of rglist_t ptrs
	rlist_p_t *rlistp = NULL; // null-term-ed array of rlist_t ptrs
	boolean_t skip_this_op;

	if (checking_r) {
		rlistp = (rlist_p_t *)calloc_nocheck((size_t)
		    (inlist.length() + 1), sizeof (rlist_p_t));
	} else {
		rglistp = (rglist_p_t *)calloc_nocheck((size_t)
		    (inlist.length() + 1), sizeof (rglist_p_t));
	}

	if ((rglistp == NULL) && (rlistp == NULL)) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	for (i = 0; i < inlist.length(); i++) {
		uint_t j;
		skip_this_op = B_FALSE;
		// check if rlist or rglist.
		if (checking_r) {
			r_ptr = rname_to_r((rglist_t *)NULL,
			    (const char *)inlist[i]);
			if (r_ptr == NULL) {
				// R name is not correct.
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = SCHA_ERR_RSRC;
				rgm_format_errmsg(&res, REM_INVALID_RS,
				    (const char *)inlist[i]);
				//  continue instead of bailing out.
				continue;
			}
		} else {
			rg_ptr = rgname_to_rg(
			    (const char *)inlist[i]);
			if (rg_ptr == NULL) {
				// RG name is not correct.
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = SCHA_ERR_RG;
				rgm_format_errmsg(&res, REM_INVALID_RG,
				    (const char *)inlist[i]);
				//  continue instead of bailing out.
				continue;
			}
		}

		// Check for duplicate operand name in argument list
		for (j = 0; j < k; j++) {
			if (checking_r) {
				if (rlistp[j] == r_ptr) {
					ucmm_print("prune_operand_list",
					    NOGET("scswitch: duplicate R name "
					    "<%s>\n"),
					    r_ptr->rl_ccrdata->r_name);
					rgm_format_errmsg(&res,
					    RWM_DUPLICATE_R_OPERAND,
					    (const char *)inlist[i]);
					// continue to next operand
					skip_this_op = B_TRUE;
					break;
				}
			} else {
				if (rglistp[j] == rg_ptr) {
					ucmm_print("prune_operand_list",
					    NOGET("scswitch: duplicate RG name "
					    "<%s>\n"),
					    rg_ptr->rgl_ccr->rg_name);
					rgm_format_errmsg(&res,
					    RWM_DUPLICATE_RG_OPERAND,
					    (const char *)inlist[i]);
					// continue to next operand
					skip_this_op = B_TRUE;
					break;
				}
			}
		}

		// continue with the next operand if flag is set.
		if (skip_this_op)
			continue;

		//
		// Add the r_ptr/rg_ptr to rlistp/rglistp based on
		// resource or resource group operands.
		//

		if (checking_r) {
			rlistp[k++] = r_ptr;
		} else {
			rglistp[k++] = rg_ptr;
		}

		// add the valid operands to the outlist.
		outlist.length(outlist.length() + 1);
		//
		// idl string assignment from a (const char *)
		// allocates memory & copies the string.
		//
		outlist[outlist.length() - 1] = (const char *)inlist[i];

	}
	free(rglistp);
	free(rlistp);
	return (res);
}
