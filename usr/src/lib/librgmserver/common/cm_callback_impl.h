/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_CM_CALLBACK_IMPL_H
#define	_CM_CALLBACK_IMPL_H

#pragma ident	"@(#)cm_callback_impl.h	1.17	08/05/20 SMI"

#include <h/cmm.h>
#include <orb/object/adapter.h>

/*CSTYLED*/
class cm_callback_impl : public McServerof<cmm::callback> {
public:
	cm_callback_impl();
	~cm_callback_impl();

	// Note that we cannot have a reference to this, since it
	// is only allocated after some ORB components have
	// been initialized. We use a pointer instead.
	static cm_callback_impl*	thep();

	//
	// Methods that implement the IDL interface
	//
	void	start_trans(Environment& _environment);
	void	set_reconf_data(cmm::seqnum_t sn,
	    const cmm::membership_t &membership,
		    Environment &_environment);
	void	step_trans(uint32_t step_number,
		    cmm::timeout_t tmout, Environment& _environment);
	void	return_trans(cmm::timeout_t tmout, Environment& _environment);
	void	stop_trans(cmm::timeout_t tmout, Environment& _environment);
	void	abort_trans(cmm::timeout_t tmout, Environment& _environment);
	void	terminate_step(Environment &);
	void	terminate_step_this_seqnum(cmm::seqnum_t, Environment &);

	void	_unreferenced(unref_t cookie)
		{ (void) _last_unref(cookie); } // Must fix for clean shutdown.

private:
	static cm_callback_impl	*the_cm_callback_implp;
};

#endif	/* !_CM_CALLBACK_IMPL_H */
