//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef	_RGM_LOGICAL_NODE_H
#define	_RGM_LOGICAL_NODE_H

#pragma ident	"@(#)rgm_logical_node.h	1.12	08/10/20 SMI"

#include <h/rgm.h>
#include <rgm/rgm_common.h>
#include <rgm/rgm_util.h>

#ifndef EUROPA_FARM
#include <orb/infrastructure/orb_conf.h>
#endif

#include <list>

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#else
#define	GLOBAL_ZONENAME	"global"
#define	ZONENAME_MAX    0
#endif

//
// orb_conf::local_nodeid replaced by get_local_nodeid()
// to work on both Server and Farm RGMs.
// get_local_nodeid() is defined in rgm_util.cc
//

#ifdef	__cplusplus
extern "C" {
sol::nodeid_t get_local_nodeid();
}
#else
extern sol::nodeid_t get_local_nodeid();
#endif

//
// The LogicalNode class represents one "Logical Node:"
// either a physical node (global zone) or a non-global zone on a physical
// node.
//
// Every logical node has the following characteristics:
// nodeid
// nodename
// zonename (NULL in physical nodes)
// Logical NodeID
// Logical node incarnation number
// Node status: either UNKNOWN, UNCONFIGURED, DOWN, or UP (in the cluster).
// List of zones on that node (physical nodes only)
// Node evacuation flag and timeout -- used on the president only.
// Pointer to slave_intention structure (from latch_intention) -- used on
//	the slave side only.
//
class LogicalNode
{

public:

	// give the mdb print function access to our private members
	friend void rgm_print_logical_node(LogicalNode *ln);

	//
	// Allocate memory and set nodename, zonename.
	//
	LogicalNode(sol::nodeid_t node, const char *zonename_in,
	    rgm::rgm_ln_state status_in);
	LogicalNode(sol::nodeid_t node, const char *zonename_in,
	    const char *nodename_in, rgm::rgm_ln_state status_in);
	virtual ~LogicalNode();

	void setLni(rgm::lni_t l) { lni = l; }
	rgm::lni_t getLni() const { return (lni); }
	rgm::ln_incarnation_t getIncarnation() const;
	void setIncarnation(rgm::ln_incarnation_t incarn_in)
	    { incarnation = incarn_in; }

	bool isInCluster() const { return (status >= rgm::LN_SHUTTING_DOWN ||
	    lni == get_local_nodeid()); }
	bool isInZoneCluster() const { return (status >=
	    rgm::LN_SHUTTING_DOWN); }
	bool isConfigured() const { return (status >= rgm::LN_DOWN); }
	//
	// Function to check whether the node is up or not.
	// For zones, this method will check the status of the zone
	// when this method is called. Note that this method does not
	// depend on the getStatus() method. The return value from
	// getStatus() is heavily dependent on the process membership
	// callbacks and the zone state change callbacks. isUp() will
	// use the zone_get_state() function to determine the current
	// status of a zone.
	//
	bool isUp() const;

	//
	// Function to check whether the physical node is up.
	// For zones, this method might return true even if the LN itself
	// is currently down.
	//
	bool isPhysNodeUp() const;

	//
	// Returns true if the lni is a base-cluster global zone; returns
	// false otherwise.  Must not be called before lni has been set.
	//
	bool isGlobalZone() const;

	//
	// Returns true if the lni is a base-cluster global zone, or a
	// ZC zone.  Returns false otherwise.
	// Assert that isZcOrGlobalZone is never called before lni has been set.
	//
	bool isZcOrGlobalZone() const
	    { CL_PANIC(lni != 0); return (lni == nodeid); }

	void setStatus(rgm::rgm_ln_state status_in);
	rgm::rgm_ln_state getStatus();
	void enterCluster();

	// Marks all local zones DOWN as well
	void leaveCluster();

	void configure();
	void unconfigure();

	// The names are valid only while holding the lock
	const char *getNodename() const { return (nodename); }
	const char *getZonename() const { return (zonename); }
	sol::nodeid_t getNodeid() const { return (nodeid); }

	const char *getFullName() const { return (fullname); }

	void insertLocalZone(LogicalNode *nextNode);
	void removeLocalZone(LogicalNode *node);
	const std::list<LogicalNode *> &getLocalZones() const
	    {return (local_zones); }
	void debug_print();
	void updateCachedNames();

	//
	// Node evacuation methods, used on the president only.
	// When a sticky evacuation begins, call beginEvac().
	// When the evacuation is complete, call endEvac() with a timeout.
	// isEvacuating() will return true during the evacuation and for
	// the timeout period afterward.
	//
	void beginEvac() { evacuating = true; }
	void endEvac(time_t timeout);
	bool isEvacuating() const;
	void clearEvac();
	void clearEvacZone();

	//
	// Intention caching methods, used on the slave only (including the
	// slave side of the president node).
	// The caller is responsible for both allocating and freeing memory
	// for the slave_intention structure.  The LN object only holds
	// a pointer to the slave_intention struct.
	//
	void setIntention(struct slave_intention *i) { intention = i; }
	struct slave_intention *getIntention() { return (intention); }

	//
	// If this zone is UP, not a physical node,
	// and its physical node is the cluster node on which
	// this code is running, and this node is already
	// "booted", run boot methods in this zone.
	//
	void check_run_boot_meths();

	void saveState();
	void revertState();

private:
	char		*nodename;
	char		*zonename;
	char		*fullname;
	rgm::lni_t	lni;
	rgm::ln_incarnation_t	incarnation;
	rgm::rgm_ln_state	status;
	rgm::rgm_ln_state old_status;
	// used to distinguish new lns from those on which we've saved
	// an old_status
	bool status_saved;
	sol::nodeid_t	nodeid;
	bool		evacuating;
	time_t		evac_expires;
	struct slave_intention *intention;

	//
	// Keep the list of local zones on this cluster node
	// Only used if the object represents a node, not a zone
	//
	std::list<LogicalNode *> local_zones;

	// helper method used by ctor
	void initialize_names(const char *zonename_in,
	    const char *nodename_in);

	void generate_state_change_event();

	// declare a private copy ctor and op= to prevent
	// assignment and pass-by-reference
	LogicalNode(LogicalNode&);
	LogicalNode& operator=(LogicalNode&);
};

#endif /* _RGM_LOGICAL_NODE_H */
