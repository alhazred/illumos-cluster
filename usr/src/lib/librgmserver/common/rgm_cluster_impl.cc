//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_cluster_impl.cc	1.14	08/05/20 SMI"

//
// rgm_cluster_impl.cc
//
// The functions in this file implement the rgmd side of the
// scha_cluster_get API calls
//

#include <sys/os.h>
#include <rgm_proto.h>
#include <scha_err.h>
#include <scha.h>

#include <sys/sc_syslog_msg.h>
#include <scadmin/scconf.h>
#include <rgm_api.h>
#include <rgm/rgm_comm_impl.h>
#include <rgm/rgm_util.h>

//
// get the cluster node state
//
void
rgm_comm_impl::idl_scha_get_node_state(
    const rgm::idl_cluster_get_args &getcl_args,
    rgm::idl_get_result_t_out getcl_res, Environment &)
{

	char			*node_name = NULL;
	scha_node_state_t	state;
	rgm::lni_t		lni;
	rgm::idl_get_result_t	*retval = new rgm::idl_get_result_t();

	rgm_lock_state();

	node_name = strdup_nocheck(getcl_args.idlstr_node_name);
	if (node_name == NULL) {
		retval->ret_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	if ((lni = rgm_get_lni(node_name)) == LNI_UNDEF) {
		// no such node
		retval->ret_code = SCHA_ERR_NODE;
		goto finished; //lint !e801
	}

	//
	// if target node is not a current member, this node is in DOWN state
	//
	if (!in_current_logical_membership(lni)) {
		state = SCHA_NODE_DOWN;
		ucmm_print("idl_scha_get_node_state()",
		    NOGET("node <%s> is not in cluster\n"), node_name);
	} else {
		// Note, SHUTTING_DOWN is currently treated as UP
		state = SCHA_NODE_UP;
		ucmm_print("idl_scha_get_node_state()",
		    NOGET("node <%s> is in cluster\n"), node_name);
	}

	retval->ret_code = SCHA_ERR_NOERR;
	retval->value_list = rgm::idl_string_seq_t(1);  // One value
	//
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via rgm_itoa_cpp).
	//
	retval->value_list[0] = rgm_itoa_cpp(state);
	retval->value_list.length(1);

finished:

	rgm_unlock_state();

	if (node_name)
		free(node_name);

	getcl_res = retval;
}
