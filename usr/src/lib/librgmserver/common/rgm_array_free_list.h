//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _RGM_ARRAY_FREE_LIST_H
#define	_RGM_ARRAY_FREE_LIST_H

#pragma ident	"@(#)rgm_array_free_list.h	1.3	08/05/20 SMI"

// system includes
#include <sys/types.h>

// local includes
#include "rgm_logical_nodeset.h"

#define	NO_RESERVED_INDEX (uint_t)0xffffffff

//
// This class manages the allocation of the unique identifier
// for the various 'element' (R, RG, LN), and provides the array
// needed to store these elements
//
// There will be one such class for each 'Element' being managed,
// i.e RG, R, LN
//
template<class OBJ> class ArrayFreeList {
private:
	uint_t	nbEntries;
	uint_t	curEntries;
	OBJ**	objs;

	// Use a LogicalNodeset as the actual free list
	LogicalNodeset *free_list;

	// To prevent pass by value and copying.
	ArrayFreeList(ArrayFreeList&) {};
	ArrayFreeList& operator = (ArrayFreeList&) {};

protected:
	void initialize(uint_t entries);
	void grow(uint_t n);

public:
	// for mdb module
	friend void rgm_print_logical_nodes();

	ArrayFreeList();
	~ArrayFreeList();

	//
	// Allocate unique identifier and insert object in the array.
	//
	uint_t insertObject(OBJ *obj, uint_t reservedIndex = NO_RESERVED_INDEX,
	    uint_t ngrow = 0);

	// Return object pointer and free unique identifier
	OBJ* removeObject(uint_t idx);

	// Return a pointer to the object
	OBJ* getObject(uint_t idx);

	uint_t getNbEntries();
	uint_t getCurEntries();

};


template <class OBJ>
inline
ArrayFreeList<OBJ>::~ArrayFreeList()
{
	delete [] objs;
}

template<class OBJ>
inline uint_t
ArrayFreeList<OBJ>::getNbEntries()
{
	return (nbEntries);
}

template<class OBJ>
inline uint_t
ArrayFreeList<OBJ>::getCurEntries()
{
	return (curEntries);
}

// include the rest of the template definitions
#include "rgm_array_free_list.cc"

#endif /* _RGM_ARRAY_FREE_LIST_H */
