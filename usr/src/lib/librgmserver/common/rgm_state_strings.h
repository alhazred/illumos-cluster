/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_STATE_STRINGS_H
#define	_RGM_STATE_STRINGS_H

#pragma ident	"@(#)rgm_state_strings.h	1.10	08/05/20 SMI"

#include <rgm_state.h>
#include <h/rgm.h>
#include <scha.h>
#include <rgm_proto.h>
#include <rgm/scha_strings.h>

#ifdef __cplusplus
extern "C" {
#endif

//
// This code is shared between the rgmd server library and the MDB module
// for the rgmd to avoid having to maintain copies of the same code in
// two different places.
//
// All variable declarations should use the static qualifier so that this
// file can be included in more than one object file that will be linked
// together (else duplicate symbols will result).
//


//
// For error conditions for unknown R and RG states
//
#define	STATE_NOT_FOUND "state not found"

//
// defines RG state
//

static rgstatename_t rgstatenames[] = {
{rgm::RG_ONLINE, "RG_ONLINE", SCHA_ONLINE, SCHA_RGSTATE_ONLINE},
{rgm::RG_OFFLINE, "RG_OFFLINE", SCHA_OFFLINE, SCHA_RGSTATE_OFFLINE},
{rgm::RG_PENDING_ONLINE, "RG_PENDING_ONLINE", SCHA_PENDING_ONLINE,
						SCHA_RGSTATE_PENDING_ONLINE},
{rgm::RG_PENDING_OFFLINE, "RG_PENDING_OFFLINE", SCHA_PENDING_OFFLINE,
						SCHA_RGSTATE_PENDING_OFFLINE},
{rgm::RG_PENDING_ONLINE_BLOCKED, "RG_PENDING_ONLINE_BLOCKED",
    SCHA_PENDING_ONLINE_BLOCKED, SCHA_RGSTATE_PENDING_ONLINE_BLOCKED},
{rgm::RG_ON_PENDING_METHODS, "RG_ON_PENDING_METHODS", SCHA_ONLINE,
						SCHA_RGSTATE_ONLINE},
{rgm::RG_OFF_PENDING_METHODS, "RG_OFF_PENDING_METHODS", SCHA_OFFLINE,
						SCHA_RGSTATE_OFFLINE},
{rgm::RG_OFF_PENDING_BOOT, "RG_OFF_PENDING_BOOT", SCHA_OFFLINE,
						SCHA_RGSTATE_OFFLINE},
{rgm::RG_OFF_BOOTED, "RG_OFF_BOOTED", SCHA_OFFLINE,
						SCHA_RGSTATE_OFFLINE},
{rgm::RG_ERROR_STOP_FAILED,	"RG_ERROR_STOP_FAILED",
		SCHA_ERROR_STOP_FAILED, SCHA_RGSTATE_ERROR_STOP_FAILED},
{rgm::RG_PENDING_OFF_STOP_FAILED,	"RG_PENDING_OFF_STOP_FAILED",
		SCHA_PENDING_OFFLINE, SCHA_RGSTATE_PENDING_OFFLINE},
{rgm::RG_PENDING_OFF_START_FAILED,	"RG_PENDING_OFF_START_FAILED",
		SCHA_PENDING_OFFLINE, SCHA_RGSTATE_PENDING_OFFLINE},
{rgm::RG_OFFLINE_START_FAILED,		"RG_OFFLINE_START_FAILED",
		SCHA_OFFLINE, SCHA_RGSTATE_OFFLINE},
{rgm::RG_PENDING_ON_STARTED,		"RG_PENDING_ON_STARTED",
		SCHA_PENDING_ONLINE, SCHA_RGSTATE_PENDING_ONLINE},
{rgm::RG_ON_PENDING_DISABLED,	"RG_ON_PENDING_DISABLED",
		SCHA_ONLINE, SCHA_RGSTATE_ONLINE},
{rgm::RG_ON_PENDING_MON_DISABLED,	"RG_ON_PENDING_MON_DISABLED",
		SCHA_ONLINE, SCHA_RGSTATE_ONLINE},
{rgm::RG_ON_PENDING_R_RESTART,	"RG_ON_PENDING_R_RESTART",
		SCHA_ONLINE, SCHA_RGSTATE_ONLINE},
};

#define	RG_ENTRIES	(sizeof (rgstatenames) / sizeof (rgstatenames[0]))

static rgstatename_t rgfaultstates =
{rgm::RG_ONLINE, "RG_ONLINE", SCHA_ONLINE_FAULTED,
    SCHA_RGSTATE_ONLINE_FAULTED};


//
// defines R states
//

static rstatename_t rstatenames[] = {
{rgm::R_MON_FAILED,	"R_MON_FAILED",	SCHA_MONITOR_FAILED,
			SCHA_RSSTATE_MONITOR_FAILED},
{rgm::R_OFFLINE,	"R_OFFLINE",	SCHA_OFFLINE,
			SCHA_RSSTATE_OFFLINE},
{rgm::R_ONLINE,	"R_ONLINE",	SCHA_ONLINE,
			SCHA_RSSTATE_ONLINE},
{rgm::R_ONLINE_STANDBY,	"R_ONLINE_STANDBY",	SCHA_OFFLINE,
				SCHA_RSSTATE_OFFLINE},
{rgm::R_ONLINE_UNMON,	"R_ONLINE_UNMON",	SCHA_ONLINE_NOT_MONITORED,
				SCHA_RSSTATE_ONLINE_NOT_MONITORED},
{rgm::R_STOPPED,	"R_STOPPED",	SCHA_STOPPING,
			SCHA_RSSTATE_STOPPING},
{rgm::R_PRENET_STARTED,	"R_PRENET_STARTED",	SCHA_STARTING,
			SCHA_RSSTATE_STARTING},
{rgm::R_START_FAILED,	"R_START_FAILED",	SCHA_START_FAILED,
			SCHA_RSSTATE_START_FAILED},
{rgm::R_PENDING_UPDATE,		"R_PENDING_UPDATE",
			SCHA_ONLINE,	SCHA_RSSTATE_ONLINE},
{rgm::R_ON_PENDING_UPDATE,	"R_ON_PENDING_UPDATE",
			SCHA_ONLINE,    SCHA_RSSTATE_ONLINE},
{rgm::R_ONUNMON_PENDING_UPDATE,	"R_ONUNMON_PENDING_UPDATE",
			SCHA_ONLINE,	SCHA_RSSTATE_ONLINE},
{rgm::R_MONFLD_PENDING_UPDATE,	"R_MONFLD_PENDING_UPDATE",
			SCHA_ONLINE,	SCHA_RSSTATE_ONLINE},
{rgm::R_STOP_FAILED,	"R_STOP_FAILED",	SCHA_STOP_FAILED,
			SCHA_RSSTATE_STOP_FAILED},
{rgm::R_PENDING_INIT,	"R_PENDING_INIT",	SCHA_OFFLINE,
			SCHA_RSSTATE_OFFLINE},
{rgm::R_PENDING_FINI,	"R_PENDING_FINI",	SCHA_OFFLINE,
			SCHA_RSSTATE_OFFLINE},
{rgm::R_UNINITED,	"R_UNINITED",	SCHA_OFFLINE,
			SCHA_RSSTATE_OFFLINE},
{rgm::R_PENDING_BOOT,	"R_PENDING_BOOT",	SCHA_OFFLINE,
			SCHA_RSSTATE_OFFLINE},
{rgm::R_UPDATING,	"R_UPDATING",	SCHA_ONLINE,
    SCHA_RSSTATE_ONLINE},
{rgm::R_JUST_STARTED,	"R_JUST_STARTED",	SCHA_ONLINE_NOT_MONITORED,
    SCHA_RSSTATE_ONLINE_NOT_MONITORED},
{rgm::R_STARTING_DEPEND,	"R_STARTING_DEPEND",	SCHA_OFFLINE,
    SCHA_RSSTATE_OFFLINE},
{rgm::R_STOPPING_DEPEND,	"R_STOPPING_DEPEND",
    SCHA_ONLINE_NOT_MONITORED,  SCHA_RSSTATE_ONLINE_NOT_MONITORED},
{rgm::R_OK_TO_START,	"R_OK_TO_START",	SCHA_OFFLINE,
    SCHA_RSSTATE_OFFLINE},
{rgm::R_OK_TO_STOP,	"R_OK_TO_STOP",	SCHA_ONLINE_NOT_MONITORED,
    SCHA_RSSTATE_ONLINE_NOT_MONITORED},
{rgm::R_INITING,	"R_INITING",	SCHA_OFFLINE,
			SCHA_RSSTATE_OFFLINE},
{rgm::R_FINIING,	"R_FINIING",	SCHA_OFFLINE,
			SCHA_RSSTATE_OFFLINE},
{rgm::R_BOOTING,	"R_BOOTING",	SCHA_OFFLINE,
			SCHA_RSSTATE_OFFLINE},
{rgm::R_MON_STARTING,	"R_MON_STARTING",
	SCHA_ONLINE_NOT_MONITORED,	SCHA_RSSTATE_ONLINE_NOT_MONITORED},
{rgm::R_MON_STOPPING,	"R_MON_STOPPING",	SCHA_ONLINE,
			SCHA_RSSTATE_ONLINE},
{rgm::R_POSTNET_STOPPING,	"R_POSTNET_STOPPING",	SCHA_STOPPING,
			SCHA_RSSTATE_STOPPING},
{rgm::R_PRENET_STARTING,	"R_PRENET_STARTING",	SCHA_STARTING,
			SCHA_RSSTATE_STARTING},
{rgm::R_STARTING,	"R_STARTING",	SCHA_STARTING,
			SCHA_RSSTATE_STARTING},
{rgm::R_STOPPING,	"R_STOPPING",	SCHA_STOPPING,
			SCHA_RSSTATE_STOPPING},
};

#define	R_ENTRIES	(sizeof (rstatenames) / sizeof (rstatenames[0]))


//
// defines R FM status
//

#define	FM_STATUS_NOT_FOUND	"FM status not found"

static rfmstatusname_t rfmstatusnames[] = {
	{rgm::R_FM_ONLINE,	"R_FM_ONLINE",		SCHA_RSSTATUS_OK},
	{rgm::R_FM_OFFLINE,	"R_FM_OFFLINE",		SCHA_RSSTATUS_OFFLINE},
	{rgm::R_FM_FAULTED,	"R_FM_FAULTED",		SCHA_RSSTATUS_FAULTED},
	{rgm::R_FM_DEGRADED,	"R_FM_DEGRADED",	SCHA_RSSTATUS_DEGRADED},
	{rgm::R_FM_UNKNOWN,	"R_FM_UNKNOWN",		SCHA_RSSTATUS_UNKNOWN},
};

#define	R_FM_ENTRIES	(sizeof (rfmstatusnames) / sizeof (rfmstatusnames[0]))

#ifdef __cplusplus
}
#endif

#endif	/* !_RGM_STATE_STRINGS_H */
