/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_recep_cluster.cc	1.31	08/06/20 SMI"

/*
 * rgm_recep_cluster.cc is the receptionist component of rgmd that
 *	handles the requests from "scha_cluster_get" LIBSCHA API functions
 */

#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <errno.h>
#include <rpc/rpc.h>
#include <netdb.h>

#include <sys/os.h>
#include <rgm/rgm_common.h>
#ifdef EUROPA_FARM
#include <cmm/ucmm_api.h>
#include <rgm/rpc/rgmx_comm_pres.h>
#include <rgmx_proto.h>
#endif
#include <rgm/rgm_cnfg.h>
#include <rgm_state.h>
#include <rgm/rgm_call_pres.h>
#include <rgm_api.h>
#include <rgm_proto.h>
#include <scadmin/scconf.h>
#include <rgm/security.h>

#include <rgm/rgm_recep.h>

#ifndef EUROPA_FARM
/*
 * cluster_open() -
 *
 *      calls scconf_get_incarnation to get cluster incarnation number
 */
void
cluster_open(open_result_t *result)
{

	uint_t	incr_num = 0;

	result->ret_code = SCHA_ERR_NOERR;

	incr_num = scconf_get_incarnation(NULL);
	if (incr_num == uint_t(0)) {
		result->ret_code = SCHA_ERR_INTERNAL;
		ucmm_print("RGM", NOGET("cluster_open(): could not get "
		    "cluster incarnation number\n"));
	} else {
		result->seq_id = rgm_itoa((int)incr_num);
	}
}

#endif /* !EUROPA_FARM */

/*
 * cluster_get_rts() -
 *
 *	calls librgm library 'rgm_get_rtlist' to get all registered
 *	RTs in the cluster
 */
void
cluster_get_rts(get_result_t *result)
{

	namelist_t	*rts = NULL;
	strarr_list	*ret_list = NULL;

	result->ret_code = SCHA_ERR_NOERR;

	//
	// call rgmcnfg_get_rtlist to retrieve all registered RTs
	//
	result->ret_code = rgmcnfg_get_rtlist(&rts, ZONE).err_code;
	if (result->ret_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		NOGET("ret code from rgmcnfg_get_rtlist is %d: returning\n"),
			result->ret_code);
		return;
	}
	std::map<rgm::lni_t, char *> tmp;
	result->ret_code = store_prop_val(SCHA_PTYPE_STRINGARRAY, rts,
				tmp, B_FALSE, NULL, &ret_list, NULL,
				NULL).err_code;
	result->value_list = ret_list;

	if (rts)
		rgm_free_nlist(rts);

}

/*
 * cluster_get_rgs() -
 *
 *	Loop through the internal state structure to retrieve all RG names
 */
void
cluster_get_rgs(get_result_t *result)
{

	rglist_p_t	rg_ptr;
	strarr_list	*ret_list;
	int		num_rgs;
	int		i;

	result->ret_code = SCHA_ERR_NOERR;

	rgm_lock_state();

	//
	// If this node has just joined the cluster, there is a window of time
	// in which the president has not yet ordered this node to read the
	// CCR.  If this is the case, sleep until the CCR has been read.
	//
	wait_until_ccr_is_read();

	rg_ptr = Rgm_state->rgm_rglist;
	if (rg_ptr && Rgm_state->rgm_total_rgs != 0) {
		/*
		 * caculate num_rgs
		 */
		for (num_rgs = 0; rg_ptr != NULL; rg_ptr = rg_ptr->rgl_next)
			num_rgs++;

		/*
		 * num_rgs must match with the number of rgs in
		 * the internal state structure.
		 */
		CL_PANIC(num_rgs == Rgm_state->rgm_total_rgs);

		if ((ret_list = (strarr_list *)calloc_nocheck(
		    1, sizeof (strarr_list))) == NULL) {
			ucmm_print("RGM",
				NOGET("couldn't calloc\n"));
			result->ret_code = SCHA_ERR_NOMEM;
			goto finished;
		}
		if ((ret_list->strarr_list_val = (char **)calloc_nocheck(
		    (size_t)num_rgs, sizeof (char *))) == NULL) {
			ucmm_print("RGM", NOGET("couldn't calloc\n"));
			result->ret_code = SCHA_ERR_NOMEM;
			goto finished;
		}
		rg_ptr = Rgm_state->rgm_rglist;
		i = 0;
		for (rg_ptr = Rgm_state->rgm_rglist; rg_ptr != NULL;
		    rg_ptr = rg_ptr->rgl_next) {
			ret_list->strarr_list_val[i] =
			    strdup(rg_ptr->rgl_ccr->rg_name);
			ucmm_print("RGM",
			    NOGET("SCHA_ALL_RGS: rg = %s\n"),
			    ret_list->strarr_list_val[i]);
			i++;
		}
		ret_list->strarr_list_len = (uint_t)num_rgs;
		result->value_list = ret_list;
	} else
		ucmm_print("RGM", NOGET("cluster_get_rgs: No RG\n"));

finished:
	rgm_unlock_state();
}

/*
 *	get state on local node or specific node
 */
void
cluster_get_node_state(const cluster_get_args *gets_args,
    get_result_t *result)
{
#ifdef EUROPA_FARM
	rgmx_cluster_get_args arg;
	rgmx_get_result_t result_v;
	CLIENT *clnt = NULL;
	enum clnt_stat rpc_res;
#else
	Environment	e;
	rgm::rgm_comm_var	prgm_pres_v;
	rgm::idl_cluster_get_args	arg;
	rgm::idl_get_result_t_var	result_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
#endif
	const char *zonename;

	result->ret_code = SCHA_ERR_NOERR;

	if (gets_args->node_name == NULL ||
	    gets_args->node_name[0] == '\0') {
		// Caller didn't provide nodename, so use our own.
		if (gets_args->zonename == NULL ||
		    gets_args->zonename[0] == '\0') {
			zonename = NULL;
		} else {
			zonename = gets_args->zonename;
		}
		arg.idlstr_node_name = lnManager->computeLn(
		    get_local_nodeid(), zonename);
	} else
		arg.idlstr_node_name = new_str(gets_args->node_name);

#ifndef EUROPA_FARM
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
#endif
	while (1) {
#ifdef EUROPA_FARM
		clnt = rgmx_comm_getpres(ZONE);
		if (clnt != NULL) {
			// In case of RPC timeout, we assume that the president
			// died and we retry the call to the new president.
			bzero(&result_v, sizeof (rgmx_get_result_t));
			rpc_res = rgmx_scha_get_node_state_1(&arg,
			    &result_v, clnt);
			delete [] arg.idlstr_node_name;

			if ((rpc_res != RPC_SUCCESS) &&
			    (rpc_res != RPC_TIMEDOUT)) {
				// some unexpected RPC error
				result->ret_code = SCHA_ERR_ORB;
				xdr_free((xdrproc_t)xdr_rgmx_get_result_t,
				    (char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return;
			}
			if (rpc_res == RPC_SUCCESS) {
				if (result_v.ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM", NOGET(
					    "cluster_get_node_state(): fail to "
					    "get node state: error = %d\n"),
					    result_v.ret_code);
					result->ret_code =
					    (scha_err_t)result_v.ret_code;
				} else {
					copy_string_array(result_v.value_list,
					    &result->value_list);
				}
				xdr_free((xdrproc_t)xdr_rgmx_get_result_t,
				    (char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return;
			}
			xdr_free((xdrproc_t)xdr_rgmx_get_result_t,
			    (char *)&result_v);
			rgmx_comm_freecnt(clnt);
		}
#else
		if (!CORBA::is_nil(prgm_pres_v)) {
			prgm_pres_v->idl_scha_get_node_state(arg, result_v, e);
			res = except_to_scha_err(e);

			// If president has not died, proceed.
			if (res.err_code != SCHA_ERR_CLRECONF) {
				if (res.err_code != SCHA_ERR_NOERR) {
					// some unexpected ORB error
					result->ret_code = res.err_code;
					return;
				}
				if (result_v->ret_code != SCHA_ERR_NOERR) {
					ucmm_print("RGM", NOGET(
					    "cluster_get_node_state(): fail to "
					    "get node state: error = %d\n"),
					    result_v->ret_code);
					result->ret_code =
					    (scha_err_t)result_v->ret_code;
					return;
				}
				(void) strseq_to_array(result_v->value_list,
				    &result->value_list);
				return;
			}
		}

		//
		// President is not yet registered, or has died.  In either
		// case, a new president will be elected soon.
		// Sleep until new president is registered in name service,
		// then loop back and retry.
		//
		// Note that rgm_comm_waitforpres() is guaranteed to return
		// a new president object pointer, different from the current
		// value of prgm_pres_v.
		//
		prgm_pres_v = rgm_comm_waitforpres(prgm_pres_v);
#endif // EUROPA_FARM
	} // while (1)

	// NOTREACHED
}
