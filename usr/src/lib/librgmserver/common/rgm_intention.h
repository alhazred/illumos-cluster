/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_INTENTION_H
#define	_RGM_INTENTION_H

#pragma ident	"@(#)rgm_intention.h	1.10	08/05/20 SMI"

/*
 * This file contains communication related utilities defenitions
 * and wrappers around IDL interfaces, mostly implemented in
 * rgm_comm.cc
 * XXX this is mostly the intention stuff
 */

#include <rgm_state.h>
#include <rgm/rgm_call_pres.h>

#include <rgm/rgm_recep.h>

#include <rgm_logical_nodeset.h>

#include <vector>
using std::vector;
using std::pair;

typedef vector<pair<LogicalNodeset*, rgm::intention_flag_t> > PerNodeFlagsT;

struct slave_intention {
	int			have_processed;		// For idempotency
	rgm::rgm_intention *intention; // The intention
};

//
// Latch and unlatch entity operations on all nodes in the cluster
//


//
// This version should be called directly only if there are per-node flags.
// Otherwise, call latch_intention().
//
int
latch_intention_pernodeflags(rgm::rgm_entity entity, rgm::rgm_operation op,
    const char *name, rgm::intention_flag_t all_node_flags,
    const PerNodeFlagsT &per_node_flags);


int
latch_intention(rgm::rgm_entity entity, rgm::rgm_operation op,
    const char *name, LogicalNodeset *ns, rgm::intention_flag_t flags);

int
process_intention();

int
unlatch_intention();

// Process the latch structure at the new president
int	complete_intention();

#endif	/* !_RGM_INTENTION_H */
