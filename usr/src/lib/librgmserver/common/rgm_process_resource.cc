//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_process_resource.cc	1.144	09/01/05 SMI"

#include <thread.h>
#include <rgm_state.h>
#include <rgm/rgm_cnfg.h>
#include <rgm_proto.h>
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm_threads.h>
#include <sys/sol_version.h>
static scha_errmsg_t set_startargs(rlist_p_t r, launchargs_t *largs);
static scha_errmsg_t set_stopargs(rlist_p_t r, launchargs_t *largs);
static boolean_t postpone_start_r(rlist_p_t r, rgm::lni_t lni,
    method_t meth, boolean_t &is_inter_rg_dep, boolean_t &is_boot_dep_pending);
static boolean_t postpone_stop_r(rlist_p_t r, rgm::lni_t lni,
    method_t meth, boolean_t &is_inter_rg_dep, boolean_t *restart_triggered);
static boolean_t starting_iter_deps_list(rlist_p_t r, deptype_t depsflag,
    rgm::lni_t lni, method_t meth, boolean_t &is_inter_rg_dep,
    boolean_t dep_r_start_flag);
static boolean_t stopping_iter_deps_list(rlist_p_t r, deptype_t depsflag,
    rgm::lni_t lni, method_t meth, boolean_t &is_inter_rg_dep,
    boolean_t *restart_triggered);
static boolean_t r_state_at_least(rlist_p_t r, rgm::rgm_r_state state,
    rgm::lni_t lni, deptype_t depsflag, boolean_t dep_r_start_flag);
static boolean_t has_net_relative_dependencies(rlist_p_t r);
static boolean_t are_dependee_boot_init_methods_pending(const
    rgm_dependencies_t *r_dep_list_ptr, rgm::lni_t lni);

//
// process_resource - evaluate Resource state; launch method if needed
//
// Called by rgm_run_state().  The thread that called rgm_run_state()
// (which calls this function) is holding the mutex lock on the
// rgm_state structure.
//
// For an explanation of the use of the all_* flags (all_on, all_off, etc.)
// see the source code comments for rgm_run_state().
//
// Exit codes:
//	SCHA_ERR_NOERR = 0	Success
//	SCHA_ERR_RSTATE		The resource's state has an illegal value.
//	SCHA_ERR_RT		The Resource has no starting method or it
//				has no stopping method.
//	SCHA_ERR_INTERNAL	Should not occur (like an assertion).
//

scha_errmsg_t
process_resource(
	rgm::lni_t	lni,		// The zone in which to process
	rlist_p_t	r,		// The Resource to process
	rglist_p_t	rg,		// The Resource Group
	boolean_t	*r_state_changed,
					// set TRUE if method launched or
					// R state was changed
	boolean_t	*all_off,	// set FALSE if R not yet stopped
	boolean_t	*all_on,	// set FALSE if R not yet online
	boolean_t	*all_mon,	// set this FALSE if R not yet online
					// & monitored
	boolean_t	*all_methods,	// set this FALSE if init/fini/boot
					// still pending (for pending_methods);
					// or some methods still in-flight (for
					// pending_on and
					// pending_off_stop_failed).
	boolean_t	*any_restart,	// set TRUE if R is restarting
	boolean_t	*any_ok_start,	// set TRUE if R is in OK_TO_START
	boolean_t	*any_starting_depend,	// set TRUE if any in
						// STARTING_DEPEND
	boolean_t	*any_prenet_started,	// set TRUE if any in
						// PRENET_STARTED
	boolean_t	*all_dis_off,	// set FALSE if R is disabled or mon
					// disabled, but R/mon not yet stopped
	boolean_t	*any_stop_failed
					// set TRUE if R is stop_failed
)
{
	boolean_t	do_launch = B_FALSE;
	boolean_t	restart_triggered = B_FALSE;

	launchargs_t	*launch_args = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	name_t		rt_meth_name = NULL;
	sc_syslog_msg_handle_t	handle;
	rgm::rgm_r_state rx_state;
	rgm::rgm_rg_state rgx_state;
	boolean_t is_inter_rg = B_FALSE;
	boolean_t is_boot_dep = B_FALSE;

	rx_state = r->rl_xstate[lni].rx_state;
	rgx_state = rg->rgl_xstate[lni].rgx_state;

	ucmm_print("RGM",
	    NOGET("process: R <%s> state <%s>; RG <%s> state <%s>\n"),
	    r->rl_ccrdata->r_name,
	    pr_rstate(rx_state),
	    rg->rgl_ccr->rg_name,
	    pr_rgstate(rgx_state));
	ucmm_print("RGM",
	    NOGET("process: R FM <%s> status <%s>\n"),
	    r->rl_ccrdata->r_name,
	    pr_rfmstatus(r->rl_xstate[lni].rx_fm_status));

	//
	// Zone death -
	//
	// If the zone is down, the process_zone_death() function will have
	// set the RG state to RG_PENDING_OFFLINE if the RG was not already
	// OFFLINE.  We must run stopping methods for "non-well-behaved"
	// resource types such as LogicalHostname, SharedAddress, or
	// HAStoragePlus, which execute their methods in the global zone.
	//
	// We take one of the following actions:
	//
	// - If the resource is one of the "non-well-behaved" types and
	// the RG state is PENDING_OFFLINE,
	// set rx_restart_flg to RR_STOPPING, i.e., pretend that the
	// resource is "restarting".  This has the effect of forcing the
	// resource to go all the way offline before it is allowed to
	// go back online, in the event that the zone reboots and the RG
	// changes direction from Pending_offline to Pending_online.
	//
	// - If the resource is of a "well-behaved" type that executes its
	// methods in the local zone, set the resource state/status directly
	// to OFFLINE.  Also clear the restarting flag.
	//
	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
#if (SOL_VERSION >= __s10)
	if (strcmp(ZONE, GLOBAL_ZONENAME) == 0) {
#endif
		if (!lnNode->isInCluster()) {
			//
			// Assert that the RG is not going online in a
			// dead zone.
			//
			CL_PANIC(!is_rg_online_not_errored(rg, lni, B_FALSE,
			    B_TRUE, NULL));
			if (is_r_globalzone(r)) {
				if (rgx_state == rgm::RG_PENDING_OFFLINE) {
					r->rl_xstate[lni].rx_restart_flg =
					    rgm::RR_STOPPING;
				}
			} else {
				set_rx_state(lni, r, rgm::R_OFFLINE);
				reset_fm_status(lni, r, rgm::R_FM_OFFLINE,
				    RGM_DIR_NULL);
				r->rl_xstate[lni].rx_restart_flg = rgm::RR_NONE;
				return (res);
			}
		}
#if (SOL_VERSION >= __s10)
	} else {
		if (!lnNode->isInZoneCluster()) {
			//
			// Assert that the RG is not going online in
			// a dead zone.
			//
			CL_PANIC(!is_rg_online_not_errored(rg, lni, B_FALSE,
			    B_TRUE, NULL));
			if (r->rl_ccrtype->rt_globalzone) {
				if (rgx_state == rgm::RG_PENDING_OFFLINE) {
					r->rl_xstate[lni].rx_restart_flg =
					    rgm::RR_STOPPING;
				}
			} else {
				set_rx_state(lni, r, rgm::R_OFFLINE);
				reset_fm_status(lni, r, rgm::R_FM_OFFLINE,
				    RGM_DIR_NULL);
				r->rl_xstate[lni].rx_restart_flg = rgm::RR_NONE;
				return (res);
			}
		}
	}
#endif
	//
	// Allow processing of those disabled Rs whose RGs are
	// OFF_PENDING_METHODS or OFF_PENDING_BOOT or ON_PENDING_DISABLED
	// or PENDING_OFF_STOP_FAILED because we may need to run fini,
	// boot, or stopping methods on the disabled R.
	// Also, as a sanity check, allow processing of any disabled R in
	// STOP_FAILED state; in this case the RG should be ERROR_STOP_FAILED
	// (or PENDING_OFF_STOP_FAILED) and we'll make sure that it goes there
	// even if (due to a bug) it isn't already.
	//
	// Otherwise if the R is disabled, just return.
	//
	// Note at this point that rgx_state is the actual RG state; we haven't
	// yet modified it.
	//
	// xxx bugids 4274541 and 4407447:
	// xxx When force-disable is implemented, we will have to process a
	// xxx disabled resource in a wider range of RG states.
	//
	if (((rgm_switch_t *)r->rl_ccrdata->r_onoff_switch)->r_switch[lni]
	    == SCHA_SWITCH_DISABLED) {
		// R is disabled; clear the restart flag
		r->rl_xstate[lni].rx_restart_flg = rgm::RR_NONE;

		if ((rgx_state != rgm::RG_OFF_PENDING_METHODS) &&
		    (rgx_state != rgm::RG_ON_PENDING_DISABLED) &&
		    (rgx_state != rgm::RG_OFF_PENDING_BOOT) &&
		    (rgx_state != rgm::RG_ON_PENDING_METHODS) &&
		    (rgx_state != rgm::RG_PENDING_OFF_STOP_FAILED) &&
		    (rx_state != rgm::R_STOP_FAILED)) {
			ucmm_print("RGM", NOGET(
			    "process: returning NOERR because DISABLED "
			    "R <%s> is not pending_methods\n"),
			    r->rl_ccrdata->r_name);
			res.err_code = SCHA_ERR_NOERR;
			return (res);
		}
	}

	//
	// launch_method() frees this memory.  If we return early from this
	// function without launching anything, we need to free this memory.
	// We do this by setting res and going to cleanup.
	// (res is set to SCHA_ERR_NOERR by default)
	//
	launch_args = (launchargs_t *)malloc(sizeof (launchargs_t));
	launch_args->l_rname = strdup(r->rl_ccrdata->r_name);
	launch_args->l_rgname = strdup(rg->rgl_ccr->rg_name);
	launch_args->l_is_scalable = is_r_scalable(r->rl_ccrdata);
	launch_args->l_rt_meth_name = NULL;
	// launch_method gets the failover mode directly from the resource


	//
	// Handle a net-relative resource in a PENDING_OFFLINE RG.
	//
	// Background:
	// scha_control RESOURCE_RESTART is willing to operate on a resource
	// type that has Start, Stop, Prenet_start and Postnet_stop methods.
	// The resource-restart will only apply Monitor_stop, Stop, Start, and
	// Monitor_start on such a resource.  Of course, this violates the
	// semantics of the net-relative methods (Prenet_start and
	// Postnet_stop), however it matches the previous behavior of DSDL
	// and is exploited by some data services for which it makes sense to
	// apply Stop followed by Start, without running Postnet_stop and
	// without stopping any of the network address resources in the RG.
	// This issue is documented in the scha_control and DSDL man pages.
	//
	// A PENDING_OFFLINE resource group may change direction to
	// PENDING_ONLINE before it has entirely gone offline.  The problem
	// here is that the group might contain data service resources with
	// net-relative methods, such that if we just change direction and
	// start all of the resources, we are going to have some violations
	// of the net-relative method ordering.  This is described in
	// bugid 4639035.  This problem may occur whether or not resources
	// were being restarted prior to the group going PENDING_OFFLINE.
	//
	// To address this problem, we do the following:
	// If a resource group is PENDING_OFFLINE and contains any resource
	// with net-relative methods and non-net-relative methods that has a
	// dependency (implicit or explicit) on some other resource with
	// net-relative methods, then all resources in the group will be
	// flagged as "restarting" by setting their rx_restart_flg to
	// RR_STOPPING.  As long as the RG
	// remains PENDING_OFFLINE, this has no effect -- all resources are
	// stopped and the rx_restart_flg flags are cleared.  However, if the
	// RG should change direction to PENDING_ONLINE, all resources will
	// be forced to go OFFLINE before being allowed to start up again
	// (this is enforced by the state machine for restarting resources).
	//
	// This is an "edge" case because it isn't so common for
	// an RG to go PENDING_OFFLINE and then reverse to PENDING_ONLINE.
	// But it does occur sometimes as indicated by bugid 4639035.  In
	// such a case, we are being conservative and potentially
	// sacrificing some availability (because it might not be necessary
	// to force all of the resources in the RG to go offline) in order
	// to maintain the "correctness" of net-relative data services
	// (because we can't tell which services might get broken if we
	// don't do this).
	//
	// We assume that this approach is not needed for groups that contain
	// only non-net-relative data services (application resource types that
	// do not declare Prenet_start or Postnet_stop methods); i.e., that it's
	// always OK for a non-net-relative application resource to stop and
	// then restart, without having to also stop and restart any network
	// address resources in the same group that the application resource
	// might depend on.
	//
	// We skip resources that are disabled, in anticipation of the force-
	// disable feature.
	//
	// Note that if the resource is OFFLINE, the switch stmt below will set
	// rx_restart_flg back to RR_NONE before the main switch statement
	// is entered.  If the resource's state is START_FAILED or
	// STOP_FAILED, rx_restart_flg is reset to RR_NONE in the big switch
	// statement.
	//
	// Note at this point that rgx_state is the actual RG state; we haven't
	// yet modified it.
	//
	if (rgx_state == rgm::RG_PENDING_OFFLINE &&
	    ((rgm_switch_t *)r->rl_ccrdata->r_onoff_switch)->r_switch[lni]
	    != SCHA_SWITCH_DISABLED) {
		if (rg->rgl_xstate[lni].rgx_net_relative_restart) {
			r->rl_xstate[lni].rx_restart_flg =
			    rgm::RR_STOPPING;
		} else if (has_net_relative_dependencies(r)) {
			rg->rgl_xstate[lni].rgx_net_relative_restart = B_TRUE;
			//
			// The following line guarantees that rgm_run_state()
			// will take another iteration before releasing the
			// state lock, thus setting the restart flags of all
			// resources in the RG:
			//
			*r_state_changed = B_TRUE;
		}
	}


	//
	// Handle a restarting resource based on the RG state.
	//
	// If the RG is ON_PENDING_R_RESTART, then there are one or more
	// resources restarting within it.  However, there are no disabled
	// resources being stopped.  In the latter case, the RG state moves
	// to ON_PENDING_DISABLED, in which disabled resources can be
	// stopping and other resources can be restarting at the same time.
	// To the extent possible, we honor dependencies between stopping
	// (disabled) and restarting resources.  There can also be restarting
	// resources in an RG that is ON_PENDING_MON_DISABLED.
	//
	// The following code checks if a restart is changing direction (from
	// the "stopping" phase to the "starting" phase) or has completed.
	//
	// The logic for changing direction depends on the RG state:
	//
	// - If the RG is ON_PENDING_R_RESTART or ON_PENDING_DISABLED or
	//   ON_PENDING_MON_DISABLED, a restarting resource changes direction
	//   when the resource reaches either STOPPED or OFFLINE state --
	//   we do *not* run Postnet_stop or Prenet_start on it if there
	//   are Start and Stop methods declared. If there are only
	//   Prenet_start and Postnet_stop, we run those methods to restart
	//   it.
	//
	// - If the RG is in a pending online or pending offline state,
	//   then a restarting resource is made to go all the way OFFLINE
	//   before restarting it, i.e. we *do* run the Postnet_stop and
	//   Prenet_start methods on it in all cases.  Note that in an RG with
	//   net-relative dependencies, all resources are forced to restart in
	//   this manner (see "Handle a net-relative resource in a
	//   PENDING_OFFLINE RG" block comment above).
	//
	// See further comments within the switch statement below.
	//
	// If the RG is ON_PENDING_R_RESTART or ON_PENDING_DISABLED or
	// ON_PENDING_MON_DISABLED, we set rgx_state to PENDING_ONLINE or
	// PENDING_OFFLINE to fake the state machine into running starting
	// or stopping methods on the resource, depending on whether the
	// resource is in the "starting" or "stopping" phase of restarting,
	// respectively.  Note that if we change rgx_state, this modifies
	// the flow in the "big switch statement" below, but does *not*
	// actually change the state of the RG in memory.
	//
	// We permit the RG to move to a PENDING_OFFLINE state from
	// ON_PENDING_R_RESTART, even while resources within it are
	// restarting.  In a PENDING_OFFLINE RG, if a resource is in the
	// "stopping" phase of restarting, we will allow it to stop and
	// then (if the RG is still pending_offline) we clear the restart
	// flag.  If the resource is in the "starting" phase of restarting
	// when the RG moves to PENDING_OFFLINE, we clear the restart flag
	// so that the resource will be brought offline along with the RG.
	//
	// It is possible that the RG can change direction from PENDING_OFFLINE
	// to PENDING_ONLINE; in that case, if the resource is still in the
	// "stopping" phase of restart, we continue to stop it; when it
	// goes offline and if the RG is still PENDING_ONLINE, we clear the
	// restart flag and just allow the resource to start as part of the
	// RG starting.  If the resource is in the "starting" phase of restart
	// and the RG is PENDING_ONLINE, we clear the restart flag; the
	// resource will come online along with the RG.
	//
	// If the RG enters PENDING_OFF_STOP_FAILED or
	// PENDING_OFF_START_FAILED, it is impossible for it to reverse
	// direction to PENDING_ONLINE before all the resources reach
	// terminal states.  Thus, we can safely reset the restarting flag
	// on the resource if the RG is in one of those states.
	// We also reset the restarting flag if the RG is in ERROR_STOP_FAILED,
	// because it can enter that state from PENDING_OFF_STOP_FAILED without
	// process_resource having been called on all resources.  This situation
	// can occur if the resource that goes stop failed has a
	// dependency on another resource, and they both have net-relative
	// methods.
	//
	// We also clear the restart flag for the endish states
	// PENDING_ONLINE_BLOCKED and ONLINE.  This may occur when a
	// restarting resource is stuck offline due to a strong r-dependency
	// on a resource that is also restarting and has gone START_FAILED.
	// In that case, we cannot finish restarting the "stuck" resource,
	// and since the RG is moving to a terminal state we clear the
	// restart flag.
	//
	// Resource_restart is initiated either by a scha_control call
	// or is triggered asynchronously by a
	// restart-dependency (which might occur while an enable, disable,
	// update, pending-online, or pending-offline action is in progress
	// in the same RG).
	//
	// As noted above, resource disables (or monitor disables) occur
	// concurrently with resource restarts in the same RG; the RG state
	// in this case will be ON_PENDING_DISABLED or ON_PENDING_MON_DISABLED,
	// respectively.  When the disable or monitor-disable actions have
	// completed, if there are any resources still restarting, the RG
	// reverts to ON_PENDING_R_RESTART.
	//
	// For the case of resources being updated, added, deleted, etc., when
	// the resource restart is triggered, the restart will be "pending" and
	// will not be executed until the update has completed and the RG would
	// revert to ONLINE state; at that point, rgm_run_state moves the RG
	// into ON_PENDING_R_RESTART and we proceed to do the resource
	// restart(s).
	//
	// Note at this point that rgx_state is still the actual RG state; we
	// haven't yet modified it, but may do so within this switch statement.
	//
	// xxx
	// If forced-disable (bugids 4274541 and 4407447) is implemented, then
	// we will have to deal with the possibility of resources being
	// disabled while other resources might be starting and/or stopping
	// in the same RG at the same time, because a forced disable might
	// be done while the RG is PENDING_ONLINE or PENDING_OFFLINE or
	// ON_PENDING_R_RESTART.
	// xxx
	//
	switch (rgx_state) {
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_OFFLINE:
		//
		// A resource may be restarting in a pending online or
		// pending offline RG.  In these RG states, we take the
		// resource all the way offline before allowing it to restart.
		// See block comments above for further explanation.
		//
		// If the R is in "starting" phase of restart, it must have
		// already completed the stopping phase.  Clear the restart
		// flag and allow the RG to go online or offline normally.
		//
		// If R is in the "stopping" phase of restart and has reached
		// any OFFLINE-equivalent state, treat it the same as a
		// "starting" resource -- clear the restart flag.  This case
		// also occurs when a restart dependency is triggered on a
		// resource that is offline.
		//
		// If the R is in the "stopping" phase of restart and has not
		// stopped yet, process it as though the RG were
		// PENDING_OFFLINE.  Set *all_on
		// and *all_mon to false so rgm_run_state knows (for the
		// pending_online case) that not all the resources are online
		// yet, nor have all of their monitors been started.
		//
		if ((r->rl_xstate[lni].rx_restart_flg ==
		    rgm::RR_STOPPING && is_offline_r_state(rx_state)) ||
		    r->rl_xstate[lni].rx_restart_flg == rgm::RR_STARTING) {
			r->rl_xstate[lni].rx_restart_flg = rgm::RR_NONE;
			ucmm_print("RGM",
			    NOGET("process: clearing restart_flg on R <%s>"),
			    r->rl_ccrdata->r_name);
		} else if (r->rl_xstate[lni].rx_restart_flg ==
		    rgm::RR_STOPPING) {
			// The following 3 lines have no effect (but do no harm)
			// if the RG is already in a "pending offline" state.
			rgx_state = rgm::RG_PENDING_OFFLINE;
			*all_on = B_FALSE;
			*all_mon = B_FALSE;
			ucmm_print("RGM", NOGET(
			    "process: R <%s> is in stopping phase of restart"),
			    r->rl_ccrdata->r_name);
		}
		break;

	case rgm::RG_PENDING_OFF_START_FAILED:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_ONLINE:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
		//
		// If RG is in any of the above states, we
		// unconditionally set the restart
		// flag on the resource to RR_NONE.  See block comments
		// above the switch statement for justification.
		//
		if (r->rl_xstate[lni].rx_restart_flg == rgm::RR_STOPPING ||
		    r->rl_xstate[lni].rx_restart_flg == rgm::RR_STARTING) {
			r->rl_xstate[lni].rx_restart_flg = rgm::RR_NONE;
			ucmm_print("RGM",
			    NOGET("process: clearing restart_flg on R <%s>"),
			    r->rl_ccrdata->r_name);
		}
		break;

	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
		//
		// These RG states allow restarting resources, triggered by
		// restart dependencies.  The disables and restarts are
		// handled concurrently.  Wherever possible, we honor
		// dependencies between resources that are being disabled
		// and those that are restarting.  The special logic for this
		// is in postpone_stop_r() and its subroutines, below.
		//
		// If all the disable actions are completed but one or more
		// resources are still restarting, then the RG state reverts
		// to ON_PENDING_R_RESTART instead of ONLINE.
		//

		// FALLTHRU

	case rgm::RG_ON_PENDING_R_RESTART:
		//
		// If R is in "stopping" phase of restart and has reached
		// STOPPED or an offline-ish state, and has both STOP and
		// START methods declared, it changes direction to
		// "starting".  This means that the Postnet_stop method
		// (if any) and Prenet_start method (if any) are *not* run on
		// the restarting resource.  If the offlinish-state that we
		// reach is STOPPED or OFFLINE, and if there is a Prenet_start
		// method registered, set the resource state to Prenet_started;
		// if we reach STOPPED or OFFLINE and
		// there is no Prenet_start method registered, set the resource
		// state to Offline.  (The latter will be a no-op if the
		// resource is already Offline).  Either way, the method that
		// gets launched on this resource will be Start.
		//
		// If we are in a different offline-ish state, we don't need
		// to reset the state to anything.  The rx_restart_flg will
		// get reset below.
		//
		// If the resource is in the stopping phase and has reached
		// offline and does not have a START method declared,
		// leave the resource in offline state so that PRENET_START
		// is run.
		//
		if (r->rl_xstate[lni].rx_restart_flg == rgm::RR_STOPPING &&
		    (rx_state == rgm::R_STOPPED ||
		    is_offline_r_state(rx_state))) {
			// Change direction from "stopping" to "starting"
			r->rl_xstate[lni].rx_restart_flg = rgm::RR_STARTING;

			//
			// We need to set all_methods to false and
			// r_state_changed to true to ensure that the
			// run_state loop processes this resource one
			// more time.  The problem is that the resource
			// starting could be postponed because of intra-rg
			// dependencies.  In that case, we don't set
			// all_methods or r_state_changed.  So we need to
			// set it here to ensure that we get processed
			// properly
			//
			*all_methods = B_FALSE;
			*r_state_changed = B_TRUE;

			ucmm_print("RGM",
			    NOGET("process: flipping restart_flg on R <%s> from"
			    " stopping to starting"), r->rl_ccrdata->r_name);

			//
			// If we're turning around without going completely
			// offline, and the STOP method did not set its own
			// status, reset status to UNKNOWN/"Starting"
			//
			if (rx_state == rgm::R_STOPPED &&
			    is_fm_status_unchanged(lni, r, RGM_DIR_STOPPING)) {
				reset_fm_status(lni, r, rgm::R_FM_UNKNOWN,
				    RGM_DIR_STARTING);
			}

			if (rx_state == rgm::R_STOPPED ||
			    rx_state == rgm::R_OFFLINE) {
				//
				// Skip prenet_start only if we
				// have both prenet_start and start
				// registered
				//
				if (is_method_reg(r->rl_ccrtype,
				    METH_START, NULL) &&
				    is_method_reg(r->rl_ccrtype,
				    METH_PRENET_START,
				    NULL) && (!r->rl_ccrtype->rt_proxy)) {
					r->rl_xstate[lni].rx_state =
					    rgm::R_PRENET_STARTED;
				} else {
					r->rl_xstate[lni].rx_state =
					    rgm::R_OFFLINE;
				}
				rx_state = r->rl_xstate[lni].rx_state;
			}
		}
		if (r->rl_xstate[lni].rx_restart_flg == rgm::RR_STARTING &&
		    (is_r_started(r, rx_state, lni) || rx_state ==
			rgm::R_STARTING_DEPEND)) {
			//
			// A restarting resource that is in the 'starting'
			// phase and has gone online (or if unmonitored,
			// online_unmon) has completed its restart, so we
			// clear the restart flag.  If the restarting resource
			// becomes STARTING_DEPEND, we must also clear the
			// restarting flag, because the RG may move to
			// PENDING_ONLINE_BLOCKED.
			//
			r->rl_xstate[lni].rx_restart_flg = rgm::RR_NONE;
			ucmm_print("RGM", NOGET(
			    "process: R <%s> restart completed"),
			    r->rl_ccrdata->r_name);
		}

		// If R is in "starting" phase of restart, process it
		// as though the RG were PENDING_ONLINE.
		if (r->rl_xstate[lni].rx_restart_flg == rgm::RR_STARTING) {
			rgx_state = rgm::RG_PENDING_ONLINE;
			ucmm_print("RGM", NOGET(
			    "process: R <%s> is in starting phase of restart"),
			    r->rl_ccrdata->r_name);

		// If R is in "stopping" phase of restart, process it
		// as though the RG were PENDING_OFFLINE.
		} else if (r->rl_xstate[lni].rx_restart_flg ==
		    rgm::RR_STOPPING) {
			rgx_state = rgm::RG_PENDING_OFFLINE;
			ucmm_print("RGM", NOGET(
			    "process: R <%s> is in stopping phase of restart"),
			    r->rl_ccrdata->r_name);
		} else {
			//
			// This R is not restarting, but might be pending
			// disabled or pending mon_disabled, unless RG
			// state is ON_PENDING_R_RESTART.
			// xxx Note, even with forced disable this would be
			// xxx true because we'd change the state to
			// xxx on_pending_disabled.
			//
			if (rg->rgl_xstate[lni].rgx_state ==
			    rgm::RG_ON_PENDING_R_RESTART) {
				ucmm_print("RGM",
				    NOGET("process: R <%s> is not restarting"),
				    r->rl_ccrdata->r_name);

				//
				// We need to set any_ok_start or
				// any_starting_depend (if required) so that
				// after we finish restarting the resource(s)
				// that are restarting, we will move the RG to
				// the appropriate state (PENDING_ONLINE or
				// PENDING_ONLINE_BLOCKED) instead of just
				// ONLINE.
				//
				if (rx_state == rgm::R_OK_TO_START) {
					*any_ok_start = B_TRUE;
				} else if (rx_state == rgm::R_STARTING_DEPEND) {
					*any_starting_depend = B_TRUE;
				}
				goto cleanup; //lint !e801
			}
		}
		break;

	case rgm::RG_ON_PENDING_METHODS:
		//
		// This RG state allows restarting resources, triggered by
		// restart dependencies.  The restarts are postponed until the
		// pending init, fini, or update methods have completed.
		// Then, instead of the RG state reverting to online it
		// goes to ON_PENDING_R_RESTART and the resource restart(s)
		// are processed.
		//

		//
		// The value of rx_restart_flg must either be RR_STOPPING
		// (indicating that the resource is pending restart), or
		// RR_NONE (indicating that the resource is not restarting).
		//
		CL_PANIC(r->rl_xstate[lni].rx_restart_flg != rgm::RR_STARTING);

		//
		// This resource might be pending init, fini, or update, so
		// we drop through to process it in the big switch
		// statement.  If this resource is pending restarting, the
		// code below this switch statement will set *any_restart to
		// true, thereby telling rgm_run_state() that the RG contains
		// at least one restarting resource.
		//

		break;

	case rgm::RG_OFFLINE_START_FAILED:
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_OFFLINE:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_BOOT:
		//
		// These states do not permit a restarting R because the
		// RG is offline.  Even if the resource was restarting, we
		// assert that process_resource cleared the restarting flag
		// while the RG was in the predecessor state (e.g.,
		// PENDING_OFF_START_FAILED or PENDING_OFFLINE).
		// xxx maybe we should just set the flag to RR_NONE instead of
		// xxx asserting that it is already set?  For now, leave it
		// xxx this way for debugging.
		//
		CL_PANIC(r->rl_xstate[lni].rx_restart_flg == rgm::RR_NONE);
		break;

	default:
		// This should not occur
		CL_PANIC(0);
	}

	//
	// If the resource is restarting but in an error state that
	// should terminate the restart, this is handled within the big
	// switch statement below.
	//


	//
	// Since the Proxy resource cannot have monitoring methods
	// set it to R_ONLINE_UNMON when its state is R_ONLINE so that
	// it falls through to R_ONLINE_UNMON case.
	//
	if (r->rl_ccrtype->rt_proxy && (rx_state == rgm::R_ONLINE)) {
		rx_state = rgm::R_ONLINE_UNMON;
	}

	//
	// Following is the "big switch statement" to implement the state
	// machine.
	// Note that rgx_state might not be the actual RG state for resources
	// that are restarting; instead, it represents the "direction" that
	// the restarting resource is currently going.
	//

	switch (rgx_state) {

	//
	// We do not need to differentiate between these two
	// states at the R processing level.  Note that restarting resources
	// that are in the 'starting' phase also drop into this case.
	//
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_PENDING_ONLINE:
		//
		// Caller will check to see whether
		// all_on and all_mon values are still TRUE
		//
		switch (rx_state) {
		case rgm::R_OFFLINE:
			res = set_startargs(r, launch_args);
			if (res.err_code != 0) {
				goto cleanup; //lint !e801
			}
			*all_on = B_FALSE;
			*all_mon = B_FALSE;

			//
			// Check boot dependencies,implicit strong loghost
			// R dependencies and explicit strong and weak R
			// dependencies before starting; postpone R start
			// if necessary.
			//
			if (postpone_start_r(r, lni,
			    launch_args->l_method, is_inter_rg, is_boot_dep)) {

				//
				// postpone starting this R
				// Set all_methods to B_FALSE so that
				// the RG remains in RG_PENDING_ONLINE state
				// until boot dependencies are satisfied.
				//

				// Check for boot dependencies.
				if (is_boot_dep) {
					ucmm_print("RGM",
					    NOGET("postponing start of R <%s> "
					    "because of boot dep\n"),
					    r->rl_ccrdata->r_name);
					*all_methods = B_FALSE;

				//
				// if we are blocking due to an inter-rg
				// dependency we change state to
				// R_STARTING_DEPEND.  In this state, the
				// resource can't do anything (it is blocked).
				// Calling set_rx_state will notify the pres
				// of our predicament, who will check if
				// our dependencies are satisfied.  When they
				// become satisfied, the president will notify
				// us, we will move to R_OK_TO_START, and
				// will finish starting.
				//
				// Note that postpone_start_r checks intra-rg
				// dependencies first, so we know that we have
				// already fulfilled them.  Thus, when we
				// find that our inter-RG deps are satisfied,
				// we do not need to re-check dependencies.
				//
				// Note also that we do not set all_methods to
				// B_FALSE.  The state machine will move
				// the RG to PENDING_ONLINE_BLOCKED, which
				// is a terminal state.
				//
				} else if (is_inter_rg) {
					ucmm_print("RGM",
					    NOGET("postponing start of R <%s> "
					    "because of inter-rg dep\n"),
					    r->rl_ccrdata->r_name);
					set_rx_state(lni, r,
					    rgm::R_STARTING_DEPEND);
					//
					// If we're the president,
					// the call to set_rx_state could
					// have caused the state to be
					// set to R_OK_TO_START immediately.
					//
					if (r->rl_xstate[lni].rx_state ==
					    rgm::R_OK_TO_START) {
						//
						// set all_methods
						// to B_FALSE to make
						// the state machine
						// refrain from moving
						// to a terminal state.
						// Otherwise, it might
						// move to
						// PENDING_ONLINE_BLOCKED, and
						// never call process_resource
						// for this r again.
						//
						*all_methods = B_FALSE;
						*any_ok_start = B_TRUE;
					} else {
						*any_starting_depend = B_TRUE;
					}
					*r_state_changed = B_TRUE;

				//
				// If we have intra-rg deps, we have not
				// necesarily checked inter-rg deps yet.
				// We will call postpone_start_r again when
				// we process this resource again, which will
				// do the necessary checks.
				//
				} else {
					ucmm_print("RGM",
					    NOGET("postponing start of R <%s> "
					    "because of intra-rg dep\n"),
					    r->rl_ccrdata->r_name);
				}
				goto cleanup; //lint !e801
			}
			*all_methods = B_FALSE;
			do_launch = B_TRUE;

			//
			// Reset status/status message to UNKNOWN/"Starting".
			//
			reset_fm_status(lni, r, rgm::R_FM_UNKNOWN,
			    RGM_DIR_STARTING);
			break;

		case rgm::R_OK_TO_START:
			//
			// OK_TO_START is like OFFLINE, except that we
			// don't need to check dependencies again.
			//
			res = set_startargs(r, launch_args);
			if (res.err_code != 0) {
				goto cleanup; //lint !e801
			}
			*all_on = B_FALSE;
			*all_mon = B_FALSE;
			*all_methods = B_FALSE;
			*any_ok_start = B_TRUE;
			do_launch = B_TRUE;


			// Check if current message status is OFFLINE/NULL
			// and reset status message to UNKNOWN/"Starting"
			if ((r->rl_xstate[lni].rx_fm_status
			    == rgm::R_FM_OFFLINE) &&
			    (r->rl_xstate[lni].rx_fm_status_msg == NULL)) {
				reset_fm_status(lni, r, rgm::R_FM_UNKNOWN,
				    RGM_DIR_STARTING);
			}
			break;

		case rgm::R_STOPPED:
			//
			// We changed direction from pending-off to pending-on.
			// Take the state to OFFLINE before starting resource.
			// That is, if the RT has both a STOP and POSTNET_STOP
			// method, we do not require the START method to be
			// able to run immediately after STOP; instead we will
			// apply the POSTNET_STOP method before calling START.
			//
			*all_on = B_FALSE;
			*all_mon = B_FALSE;
			if (!is_method_reg(r->rl_ccrtype, METH_POSTNET_STOP,
			    &rt_meth_name)) {
				// This is an internal error; advance the state
				// to OFFLINE so we don't get caught in a loop.
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "INTERNAL ERROR: "
				    "POSTNET_STOP method is not registered "
				    "for resource <%s>", r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				set_rx_state(lni, r, rgm::R_OFFLINE);
				//
				// Don't reset the R status here; we will
				// reset it in the R_OFFLINE case above
				// on the next iteration of rgm_run_state()
				//

				*all_methods = B_FALSE;
				*r_state_changed = B_TRUE;
				goto cleanup; //lint !e801
			}
			ucmm_print("RGM",
			    NOGET("R_STOPPED: set meth_name to <%s>\n"),
			    rt_meth_name);

			//
			// We've changed direction from pending-off to
			// pending-on, but this resource has just completed
			// STOP and is about to run POSTNET_STOP.  There
			// might be other resources in the same group that
			// are also stopping (either because they are disabled
			// or are restarting).  We call postpone_stop_r
			// to enforce stopping dependencies between this
			// resource and those others that are also stopping.
			//
			// postpone_stop_r() and its subroutines have special-
			// case code to deal with RG states such as
			// ON_PENDING_DISABLED, ON_PENDING_R_RESTART, or
			// PENDING_ONLINE in which not all Rs are stopping.
			//

			//
			// Do not trigger an intra-RG restart in the
			// R_STOPPED case since this implies that the
			// dependee has already run Stop and is about
			// to run Postnet_stop. The way we achieve this
			// is by passing NULL instead of &restart_triggered.
			//
			if (postpone_stop_r(r, lni, METH_POSTNET_STOP,
			    is_inter_rg, NULL)) {
				//
				// postpone stopping this R
				//
				// We don't want to postpone stopping
				// this resource at this point due
				// to inter-RG dependencies, because we have
				// already run STOP and it is too late
				// to postpone.
				//
				// postpone_stop_r gives intra-RG deps
				// a higher priority, so we know that if
				// is_inter_rg is B_TRUE, there are no
				// intra-RG deps.
				//
				if (!is_inter_rg) {
					ucmm_print("RGM",
					    NOGET("postponing stop of R <%s> "
					    "because of intra-rg deps\n"),
					    r->rl_ccrdata->r_name);
					goto cleanup; //lint !e801
				}
			}

			*all_methods = B_FALSE;
			launch_args->l_rt_meth_name = rt_meth_name;
			launch_args->l_method = METH_POSTNET_STOP;
			launch_args->l_succstate = rgm::R_OFFLINE;
			launch_args->l_failstate = rgm::R_STOP_FAILED;
			launch_args->l_workstate = rgm::R_POSTNET_STOPPING;
			do_launch = B_TRUE;
			break;

		case rgm::R_PRENET_STARTED:
			*all_mon = B_FALSE;
			// Note scalable services use ssm_wrapper for START
			if (!is_method_reg(r->rl_ccrtype, METH_START,
			    &rt_meth_name) && !launch_args->l_is_scalable) {
				// This is an internal error; change the state
				// anyway so we don't get caught in a loop.
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "INTERNAL ERROR: START method is "
				    "not registered for resource <%s>",
				    r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				if (!r->rl_ccrtype->rt_proxy) {
					set_rx_state(lni, r,
					    rgm::R_JUST_STARTED);
					//
					// Set all_methods to B_FALSE
					// unconditionally, so that the
					// RG won't move to ONLINE until
					// the pres has acked JUST_STARTED, and
					// we've had a chance to start
					// monitoring.
					//
					*all_methods = B_FALSE;
				} else {
					set_rx_state(lni, r,
					    rgm::R_ONLINE_STANDBY);
				}

				//
				// Set status to ONLINE/NULL only if method
				// didn't set it
				if (is_fm_status_unchanged(lni, r,
				    RGM_DIR_STARTING)) {
					reset_fm_status(lni, r,
					    rgm::R_FM_ONLINE, RGM_DIR_NULL);
				}
				*r_state_changed = B_TRUE;
				goto cleanup; //lint !e801
			}
			*all_on = B_FALSE;

			if (r->rl_xstate[lni].rx_ok_to_start) {
				ucmm_print("process_resource", NOGET(
				    "resource %s is in %s state"),
				    r->rl_ccrdata->r_name, SCHA_STARTING);
				r->rl_xstate[lni].rx_ok_to_start = B_FALSE;
			} else if (postpone_start_r(r, lni, METH_START,
			    is_inter_rg, is_boot_dep)) {
				//
				// postpone starting this R
				//
				// We don't want to postpone starting
				// this resource at this point due
				// to boot dependencies or
				// inter-RG dependencies, because
				// we have already run PRENET_START and it
				// is too late to postpone.
				//
				// postpone_start_r gives intra-RG deps
				// a higher priority, so we know that if
				// if_inter_rg is B_TRUE, there are no
				// intra-RG deps.
				//
				if ((!is_inter_rg) && (!is_boot_dep)) {
					ucmm_print("RGM",
					    NOGET("postponing start of "
					    "R <%s>\n"),
					    r->rl_ccrdata->r_name);
					goto cleanup; //lint !e801
				}
			}
			*all_methods = B_FALSE;

			launch_args->l_rt_meth_name = rt_meth_name;
			launch_args->l_method = METH_START;
			launch_args->l_succstate = rgm::R_JUST_STARTED;
			launch_args->l_failstate = rgm::R_START_FAILED;
			launch_args->l_workstate = rgm::R_STARTING;
			do_launch = B_TRUE;
			break;

		case rgm::R_STOPPING_DEPEND:
		case rgm::R_OK_TO_STOP:
			//
			// we changed direction from a form of PENDING_OFFLINE
			// to a form of PENDING_ONLINE. Move back to
			// ONLINE_UNMON and fall through to that case.
			//
			if (r->rl_ccrtype->rt_proxy) {
				//
				// TBD Don't know whether to put it in
				// ONLINE_STANDBY or ONLINE
				//
				set_rx_state(lni, r, rgm::R_ONLINE_STANDBY);
				*r_state_changed = B_TRUE;
				goto cleanup; //lint !e801
			} else {
				set_rx_state(lni, r, rgm::R_ONLINE_UNMON);
				*r_state_changed = B_TRUE;
			}
			// fall through...

		case rgm::R_ONLINE_UNMON:
			//
			// all_mon remains TRUE if there is no
			// monitor_start method to launch
			//
			// Note scalable services use ssm_wrapper for
			// MONITOR_START
			//
			if (!is_r_monitored(r, &rt_meth_name, lni)) {
				// The resource is not monitored;
				// leave the resource in R_ONLINE_UNMON state
				goto cleanup; //lint !e801
			}
			// The resource is monitored
			launch_args->l_rt_meth_name = rt_meth_name;
			launch_args->l_method = METH_MONITOR_START;
			launch_args->l_succstate = rgm::R_ONLINE;
			launch_args->l_workstate = rgm::R_MON_STARTING;
			launch_args->l_failstate = rgm::R_MON_FAILED;
			*all_mon = B_FALSE;
			*all_methods = B_FALSE;
			do_launch = B_TRUE;
			break;

		case rgm::R_JUST_STARTED:
			//
			// We count JUST_STARTED as ONLINE_UNMON, except
			// that we can't run the monitor yet (because we need
			// to wait for the pres to ack the JUST_STARTED state).
			//
			// However, we don't want the RG processing to think
			// this R is done, whether or not we have a monitor
			// start registered, because we need the president
			// to ack the JUST_STARTED state first.
			//
			// Set all_mon and all_methods to B_FALSE
			// unconditionally (without checking for monitoring)
			// so that the state machine will not move the RG
			// to ONLINE until the president has acked the
			// JUST_STARTED state.
			//
			*all_mon = B_FALSE;
			*all_methods = B_FALSE;
			ucmm_print("RGM", NOGET("<%s> is in "
			    "JUST_STARTED, so we can't monitor "
			    "it yet\n"), r->rl_ccrdata->r_name);
			break;


		case rgm::R_STARTING_DEPEND:
			//
			// set any_starting_depend.
			// We can't do anything else, as we're waiting for
			// the pres to move us to OK_TO_START.
			//
			*any_starting_depend = B_TRUE;
			break;

		case rgm::R_PRENET_STARTING:
		case rgm::R_STARTING:
			*all_on = B_FALSE;
			*all_mon = B_FALSE;
			*all_methods = B_FALSE;
			goto cleanup; //lint !e801

		case rgm::R_POSTNET_STOPPING:
		case rgm::R_STOPPING:
			*all_on = B_FALSE;
			// FALLTHRU
		case rgm::R_MON_STOPPING:
		case rgm::R_MON_STARTING:
			*all_mon = B_FALSE;
			*all_methods = B_FALSE;
			goto cleanup; //lint !e801

		case rgm::R_MON_FAILED:
			*all_mon = B_FALSE;
			// FALLTHRU

		case rgm::R_ONLINE_STANDBY:
			// Fall thru

		case rgm::R_START_FAILED:
			//
			// R_START_FAILED here because Failover mode == NONE
			// or RESTART_ONLY or LOG_ONLY.
			// If Failover mode were HARD or SOFT, we wouldn't
			// get here (see launch_method()).
			//

			// If r was restarting, it isn't any more
			r->rl_xstate[lni].rx_restart_flg = rgm::RR_NONE;

			*all_on = B_FALSE;
			// leave *all_methods true so that run_state terminates
			goto cleanup; //lint !e801

		case rgm::R_ONLINE:
			goto cleanup; //lint !e801

		case rgm::R_BOOTING:
			// internal error-- should not be taking RG
			// online if its Rs are still booting.
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd is attempting to bring a resource group
			// online on a node where BOOT methods are still being
			// run on its resources. This should not occur and may
			// indicate an internal logic error in the rgmd.
			// @user_action
			// Look for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "INTERNAL ERROR: process_resource: Resource <%s> "
			    "is R_BOOTING in PENDING_ONLINE resource group",
			    r->rl_ccrdata->r_name);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_INTERNAL;
			goto cleanup; //lint !e801

		case rgm::R_STOP_FAILED:
			//
			// A STOP or MONITOR_STOP method has failed on r.
			//
			// This code path can be taken only in the case that
			// rebalance() has changed the RG direction from
			// pending offline to pending online while stop methods
			// were still running, and a monitor_stop or stop
			// method fails.
			//
			// This code path is not taken for resources that are
			// in the stopping phase of restarting or [when
			// implemented] force-disabled, because for those
			// cases rgx_state will be set to RG_PENDING_OFFLINE.
			//
			// We do not change the RG state to
			// PENDING_OFF_STOP_FAILED, because the stop-failure
			// of this resource should not cause the stopping of
			// other resources in the same RG that do not depend
			// on this one.  Even though the RG ends up in
			// ERROR_STOP_FAILED state, those non-dependent
			// resources can start and provide service.
			//
			// As a precaution, we clear the restart flag even
			// though it should not have been set at this point.
			// Also we set *any_stop_failed, and leave the R and
			// RG states unchanged.  rgm_run_state() will move
			// the RG to ERROR_STOP_FAILED state when all resources
			// that can be started have been started.
			//
			*any_stop_failed = B_TRUE;
			r->rl_xstate[lni].rx_restart_flg = rgm::RR_NONE;
			goto cleanup; //lint !e801

		case rgm::R_UNINITED:
			// Should occur only in UNMANAGED or PENDING_METHODS RGs
		case rgm::R_INITING:
		case rgm::R_FINIING:
		case rgm::R_UPDATING:
		case rgm::R_PENDING_INIT:
		case rgm::R_PENDING_FINI:
		case rgm::R_PENDING_BOOT:
		case rgm::R_PENDING_UPDATE:
		case rgm::R_ON_PENDING_UPDATE:
		case rgm::R_ONUNMON_PENDING_UPDATE:
		case rgm::R_MONFLD_PENDING_UPDATE:
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd state machine has discovered a resource in
			// an unexpected state on the local node. This should
			// not occur and may indicate an internal logic error
			// in the rgmd.
			// @user_action
			// Look for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "PENDING_ONLINE: bad resource state <%s> "
			    "(%d) for resource <%s>",
			    pr_rstate(rx_state), rx_state,
			    r->rl_ccrdata->r_name);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_RSTATE;
			goto cleanup; //lint !e801
		}

		break;

	case rgm::RG_ON_PENDING_R_RESTART:
		//
		// If the R is restarting, it should have been redirected to
		// the PENDING_OFFLINE case or PENDING_ONLINE case by setting
		// rgx_state (the local variable, not the actual RG state)
		// above.  If R is not restarting, we should have exited early.
		// Therefore, ON_PENDING_R_RESTART case should not be reached.
		//
		CL_PANIC(strlen(
		    "restarting resource should have set rgx_state") == 0);
		break;

	case rgm::RG_ON_PENDING_DISABLED:
		//
		// The RG contains one or more disabled Rs that have to be
		// taken offline.  It may also contain one or more restarting
		// Rs.
		//
		// To take a disabled R offline, we will fall through to the
		// case for RG_PENDING_OFFLINE.  If this resource is not
		// disabled but is restarting, we caught this above and set
		// rgx_state (the local variable, not the in-memory RG state)
		// to PENDING_ONLINE or PENDING_OFFLINE, depending on whether
		// the resource is in the starting or stopping phase of
		// restart, respectively.  So a restarting resource will go
		// directly to one of those cases in the switch statement.
		//
		// If we reach this point, the resource is either disabled
		// and needs to be taken offline; or is enabled and remaining
		// online.  In the latter case we break out of the switch
		// statement and return after performing some checks -- see
		// comment below.
		//
		if (((rgm_switch_t *)r->rl_ccrdata->
		    r_onoff_switch)->r_switch[lni]
		    != SCHA_SWITCH_DISABLED) {
			//
			// We need to set any_ok_start or any_starting_depend
			// (if required) so that after we finish offlining
			// the disabled resource(s), we will move the RG
			// to the appropriate state (PENDING_ONLINE or
			// PENDING_ONLINE_BLOCKED).
			//
			// We also need to set any_prenet_started to true if
			// the resource
			// is blocked in PRENET_STARTED.  This is to catch
			// the case where r1 depends on r2 in the same RG
			// and r3 in a different RG (and r1 and r2 have
			// PRENET_START registered).  r2 might have reached
			// PRENET_START, then blocked waiting for r1 to reach
			// PRENET_START.  However, r1 depends on r3, which
			// might not be online, so it will be in
			// STARTING_DEPEND.  When r1 is disabled, r2 should
			// be allowed to finish starting.
			//
			if (rx_state == rgm::R_OK_TO_START) {
				*any_ok_start = B_TRUE;
			} else if (rx_state == rgm::R_STARTING_DEPEND) {
				*any_starting_depend = B_TRUE;
			} else if (rx_state == rgm::R_PRENET_STARTED) {
				*any_prenet_started = B_TRUE;
			}
			break;
		}

		//
		// While the RG remains in ON_PENDING_DISABLED, we will set the
		// all_dis_off flag to false if any disabled resource has not
		// yet reached the R_OFFLINE state.  If all_dis_off and
		// any_restart are both true, then we have finished stopping
		// all the disabled resources but some resources are still
		// restarting, so rgm_run_state() moves the RG state to
		// ON_PENDING_R_RESTART.
		//
		// If all_dis_off is true and any_restart is false, then
		// we have finished stopping all disabled resources, and all
		// restarting resources have either finished restarting or have
		// gone to an errored state (STOP_FAILED, START_FAILED, or
		// MON_FAILED).  In this case, rgm_run_state() checks the
		// any_stop_failed flag.  If it is true, rgm_run_state() moves
		// the RG state from ON_PENDING_DISABLED to ERROR_STOP_FAILED.
		// If any_stop_failed is false, rgm_run_state moves the RG
		// state to ONLINE or PENDING_ONLINE or PENDING_ONLINE_BLOCKED
		// (based upon satisfaction of inter-RG dependencies).
		//

		// Fall through to RG_PENDING_OFFLINE to process the disabled R.
		// FALLTHRU

	//
	// Handle PENDING_OFF_STOP_FAILED like PENDING_OFFLINE.
	// We continue to stop other resources in the RG
	// as dependencies permit.
	//
	// When we've stopped everything we can, the RG transitions to
	// ERROR_STOP_FAILED and we wake the president.
	// rgm_run_state() uses the all_methods && all_off variables to
	// decide when it is time to call wake_president().  For
	// PENDING_OFF_STOP_FAILED, all_off means that we've stopped all Rs
	// and the RG is offline.  (all_methods && !all_off) means that some
	// Rs couldn't be stopped and some Rs remain in STOP_FAILED state,
	// but no more work can be done.
	//
	case rgm::RG_PENDING_OFF_STOP_FAILED:
		// FALLTHRU
	//
	// We do not need to differentiate between the following two
	// states at the R processing level.
	//
	case rgm::RG_PENDING_OFF_START_FAILED:
	case rgm::RG_PENDING_OFFLINE:

		switch (rx_state) {

		case rgm::R_ON_PENDING_UPDATE:
		case rgm::R_MONFLD_PENDING_UPDATE:
			//
			// These two R states can occur for a "non well behaved"
			// resource type which was pending update (in the
			// global zone) when the local zone in which the R
			// was actually configured died.  We just treat the
			// resource as ONLINE and run the Monitor_stop method.
			// However if the resource in this state is not of a
			// "non well behaved" type, this is an internal error.
			//
			if (!is_r_globalzone(r)) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The rgmd state machine has discovered a
				// resource in an unexpected state on the
				// local node. This should not occur and may
				// indicate an internal logic error in the
				// rgmd.
				// @user_action
				// Look for other syslog error messages on the
				// same node. Save a copy of the
				// /var/adm/messages files on all nodes, and
				// report the problem to your authorized Sun
				// service provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "PENDING_OFFLINE: bad resource state <%s> "
				    "(%d) for resource <%s>",
				    pr_rstate(rx_state), rx_state,
				    r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_INTERNAL;
				goto cleanup; //lint !e801
			}
			// FALLTHRU
		case rgm::R_MON_FAILED:
			//
			// With the RG pending offline (or the R in the stopping
			// phase of a restart), the MON_FAILED resource state
			// is equivalent to ONLINE.
			//
		case rgm::R_ONLINE:
			//
			// Being in any of the above four states implies that
			// monitoring was enabled on the resource (otherwise
			// it would not have gotten past R_ONLINE_UNMON).
			// We will run MONITOR_STOP on the resource.
			//
			if (!is_method_reg(r->rl_ccrtype,
			    METH_MONITOR_STOP, &rt_meth_name) &&
			    !launch_args->l_is_scalable) {
				//
				// This is an internal error; change the
				// state anyway so the state machine
				// doesn't get in a loop with this R
				// stuck in current state.
				//
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle,
				    LOG_ERR, MESSAGE,
				    "INTERNAL ERROR: monitoring is "
				    "enabled, but MONITOR_STOP method "
				    "is not registered for resource "
				    "<%s>", r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				set_rx_state(lni, r,
				    rgm::R_ONLINE_UNMON);
				//
				// Don't reset status,
				// it will be done in fall-
				// thru case below.
				//
				*r_state_changed = B_TRUE;
				// Fall-thru to R_ONLINE_UNMON case
			} else {
				if (((rgm_switch_t *)r->rl_ccrdata->
				    r_onoff_switch)->r_switch[lni] ==
				    SCHA_SWITCH_DISABLED) {
					*all_dis_off = B_FALSE;
				}
				*all_off = B_FALSE;
				*all_methods = B_FALSE;
				ucmm_print("RGM",
				    NOGET("R_ONLINE: set meth_name "
				    "to <%s>\n"),
				    rt_meth_name ? rt_meth_name :
				    "NULL");
				launch_args->l_rt_meth_name =
				    rt_meth_name;
				launch_args->l_method =
				    METH_MONITOR_STOP;
				launch_args->l_succstate =
				    rgm::R_ONLINE_UNMON;
				launch_args->l_failstate =
				    rgm::R_STOP_FAILED;
				launch_args->l_workstate =
				    rgm::R_MON_STOPPING;

				do_launch = B_TRUE;
				break;
			}
			// FALLTHRU

		case rgm::R_PRENET_STARTED:
			//
			// Case where RG was pending online but reversed
			// direction to pending offline.  In this case, we
			// treat the r as if it were in R_ONLINE_UNMON state,
			// i.e. we will next apply the Stop and/or Postnet_stop
			// methods to it.
			//
		case rgm::R_START_FAILED:
			//
			// With the RG pending offline (or the R in the
			// stopping phase of a restart), the START_FAILED
			// resource state is equivalent to ONLINE_UNMON.
			//
		case rgm::R_ONLINE_STANDBY:
			//
			// Fall thru
			//
		case rgm::R_ONUNMON_PENDING_UPDATE:
			//
			// This R state can occur for a "non well behaved"
			// resource type which was pending update (in the
			// global zone) when the local zone in which the R
			// was actually configured died.  We just treat the
			// resource as ONLINE_UNMON and run the Stop method.
			// However if the resource in this state is not of a
			// "non well behaved" type, this is an internal error.
			//
			if (rx_state == rgm::R_ONUNMON_PENDING_UPDATE &&
			    !is_r_globalzone(r)) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "PENDING_OFFLINE: bad resource state <%s> "
				    "(%d) for resource <%s>",
				    pr_rstate(rx_state), rx_state,
				    r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_INTERNAL;
				goto cleanup; //lint !e801
			}
			// FALLTHRU

		//
		// A Proxy resource is set to R_ONLINE_UNMON before the big
		// switch statement since it cannot have monitoring methods.
		//
		case rgm::R_ONLINE_UNMON:
			if (((rgm_switch_t *)r->rl_ccrdata->
			    r_onoff_switch)->r_switch[lni] ==
			    SCHA_SWITCH_DISABLED) {
				*all_dis_off = B_FALSE;
			}
			*all_off = B_FALSE;
			res = set_stopargs(r, launch_args);
			if (res.err_code != 0) {
				goto cleanup; //lint !e801
			}

			//
			// Check implicit strong loghost R dependencies
			// and explicit strong, weak, and restart R
			// dependencies before stopping;
			// postpone R stop if necessary.
			//
			if (postpone_stop_r(r, lni, launch_args->l_method,
			    is_inter_rg, &restart_triggered)) {

				// postpone stopping this R

				//
				// if we are blocking due to an inter-rg
				// dependency we change state to
				// R_STOPPING_DEPEND.  In this state, the
				// resource can't do anything (it is blocked).
				// Calling set_rx_state will notify the pres
				// of our predicament, who will check if
				// our dependencies are satisfied.  When they
				// become satisfied, the president will notify
				// us, we will move to R_OK_TO_STOP, and
				// will finish stopping.
				//
				// Note that postpone_stop_r checks intra-rg
				// dependencies first, so we know that we have
				// already fulfilled them.  Thus, when we
				// find that our inter-RG deps are satisfied,
				// we do not need to re-check dependencies.
				//
				// Note also that we set all_methods to
				// B_FALSE, so that the state machine won't
				// move the RG to a terminal state until
				// this resource has a chance to go offline.
				//
				// Note that if r is disabled, then we have
				// guaranteed elsewhere that all of its
				// dependents are already disabled (although
				// they might be in the process of switching
				// offline on one or more nodes).  Therefore,
				// it is OK to postpone stopping r because its
				// stopping dependencies should soon be
				// satisfied.
				//
				if (is_inter_rg) {
					ucmm_print("RGM",
					    NOGET("postponing stop of R <%s> "
					    "because of inter-rg dep\n"),
					    r->rl_ccrdata->r_name);
					set_rx_state(lni, r,
					    rgm::R_STOPPING_DEPEND);
					*all_methods = B_FALSE;
					*r_state_changed = B_TRUE;

				//
				// If we have intra-rg deps, we have not
				// necesarily checked inter-rg deps yet.
				// We will call postpone_stop_r again when
				// we process this resource again, which will
				// do the necessary checks.
				//
				// Note: we do not need to set all_methods
				// to false, because it is fine for the state
				// machine to exit while methods are postponed.
				// However, if we have triggered a restart of
				// a dependent resource, we have to set
				// all_methods false to make sure that we
				// take another state machine iteration to
				// launch stopping methods on the dependent.
				//
				} else {
					ucmm_print("RGM",
					    NOGET("postponing stop of R <%s> "
					    "because of intra-rg dep\n"),
					    r->rl_ccrdata->r_name);
					if (restart_triggered) {
						*r_state_changed = B_TRUE;
						*all_methods = B_FALSE;
					}
				}
				goto cleanup; //lint !e801
			}
			*all_methods = B_FALSE;
			do_launch = B_TRUE;

			// Reset status message to UNKNOWN/"Stopping"
			reset_fm_status(lni, r, rgm::R_FM_UNKNOWN,
			    RGM_DIR_STOPPING);
			break;

		case rgm::R_OK_TO_STOP:
			//
			// OK_TO_STOP is treated like ONLINE_UNMON,
			// except that we don't need to check dependencies
			// again.
			//
			if (((rgm_switch_t *)r->rl_ccrdata->
			    r_onoff_switch)->r_switch[lni] ==
			    SCHA_SWITCH_DISABLED) {
				*all_dis_off = B_FALSE;
			}
			*all_off = B_FALSE;
			res = set_stopargs(r, launch_args);
			if (res.err_code != 0) {
				goto cleanup; //lint !e801
			}

			*all_methods = B_FALSE;
			do_launch = B_TRUE;

			// Reset status message to UNKNOWN/"Stopping"
			reset_fm_status(lni, r, rgm::R_FM_UNKNOWN,
			    RGM_DIR_STOPPING);
			break;

		case rgm::R_STOPPING_DEPEND:
			//
			// We can't do anything in R_STOPPING_DEPEND.
			// Just set all_off to B_FALSE, and all_methods
			// to B_FALSE (see comments in ONLINE_UNMON
			// case above for explanation).
			//
			if (((rgm_switch_t *)r->rl_ccrdata->
			    r_onoff_switch)->r_switch[lni] ==
			    SCHA_SWITCH_DISABLED) {
				*all_dis_off = B_FALSE;
			}
			*all_off = B_FALSE;
			*all_methods = B_FALSE;
			break;

		case rgm::R_JUST_STARTED:
			//
			// Just started is like ONLINE_UNMON, except that
			// we can't stop the resource yet.  We need to wait
			// for the pres to ack the JUST_STARTED state.
			//
			// However, we want the RG to know that we haven't
			// stopped yet, so set all_off to FALSE.  Also need
			// to set all_methods to FALSE, so the run_state
			// processing doesn't think we failed to stop.
			//
			*all_off = B_FALSE;
			*all_methods = B_FALSE;
			ucmm_print("RGM", NOGET("<%s> is in JUST_STARTED, "
			    "so we can't stop it yet\n"),
			    r->rl_ccrdata->r_name);
			break;


		case rgm::R_STOPPED:
			if (!is_method_reg(r->rl_ccrtype, METH_POSTNET_STOP,
			    &rt_meth_name)) {
				// This is an internal error; change the state
				// anyway so we don't get caught in a loop.
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "INTERNAL ERROR: "
				    "POSTNET_STOP method is not registered for "
				    "resource <%s>", r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				set_rx_state(lni, r, rgm::R_OFFLINE);
				// Set status to OFFLINE/NULL only if method
				// didn't set it
				if (is_fm_status_unchanged(lni, r,
				    RGM_DIR_STOPPING)) {
					reset_fm_status(lni, r,
					    rgm::R_FM_OFFLINE, RGM_DIR_NULL);
				}
				*r_state_changed = B_TRUE;
				goto cleanup; //lint !e801
			}
			if (((rgm_switch_t *)r->rl_ccrdata->
			    r_onoff_switch)->r_switch[lni] ==
			    SCHA_SWITCH_DISABLED) {
				*all_dis_off = B_FALSE;
			}
			*all_off = B_FALSE;
			ucmm_print("RGM",
			    NOGET("R_STOPPED: set meth_name to <%s>\n"),
			    rt_meth_name);

			//
			// Check implicit strong loghost R dependencies
			// and explicit strong, weak, and restart R
			// dependencies before stopping;
			// postpone R stop if necessary.
			//
			if (postpone_stop_r(r, lni, METH_POSTNET_STOP,
			    is_inter_rg, NULL)) {
				//
				// postpone stopping this R
				//
				// We don't want to postpone stopping
				// this resource at this point due
				// to inter-RG dependencies, because we have
				// already run STOP and it is too late
				// to postpone.
				//
				// postpone_stop_r gives intra-RG deps
				// a higher priority, so we know that if
				// is_inter_rg is B_TRUE, there are no
				// intra-RG deps.
				//
				if (!is_inter_rg) {
					ucmm_print("RGM",
					    NOGET("postponing stop of R <%s> "
					    "because of intra-rg deps\n"),
					    r->rl_ccrdata->r_name);
					goto cleanup; //lint !e801
				}
			}
			*all_methods = B_FALSE;

			launch_args->l_rt_meth_name = rt_meth_name;
			launch_args->l_method = METH_POSTNET_STOP;
			launch_args->l_succstate = rgm::R_OFFLINE;
			launch_args->l_failstate = rgm::R_STOP_FAILED;
			launch_args->l_workstate = rgm::R_POSTNET_STOPPING;
			do_launch = B_TRUE;
			break;

		case rgm::R_STOP_FAILED:
			//
			// Do not set *all_dis_off to false; this r is as
			// "disabled" as it's ever going to get.
			//

			*all_off = B_FALSE;

			//
			// A STOP or MONITOR_STOP method has failed.
			// Set *any_stop_failed for use by rgm_run_state().
			//
			*any_stop_failed = B_TRUE;

			// If r was restarting, it isn't any more
			r->rl_xstate[lni].rx_restart_flg = rgm::RR_NONE;

			//
			// If the real RG state is a pending offline state
			// (RG_PENDING_OFFLINE or RG_PENDING_OFF_START_FAILED),
			// then we move it to PENDING_OFF_STOP_FAILED state.
			// We then
			// continue to stop Rs in this RG as permitted by
			// R-dependencies, until all_methods remains true; at
			// that point, rgm_run_state will move the RG state
			// to ERROR_STOP_FAILED and wake the president.
			//
			// If the real RG state is not a pending offline state,
			// we do not move it to PENDING_OFF_STOP_FAILED,
			// because other resources might be online and
			// continuing to provide service.  This can occur for
			// example when we are stopping monitoring on a
			// resource, disabling a resource, or restarting a
			// resource.  We leave the RG in its current state and
			// continue to stop, start, and/or restart resources
			// as permitted by R-dependencies, until all_methods
			// remains true; at that point, rgm_run_state will
			// move the RG state to ERROR_STOP_FAILED and wake
			// the president.
			//
			if (rg->rgl_xstate[lni].rgx_state ==
			    rgm::RG_PENDING_OFF_START_FAILED ||
			    rg->rgl_xstate[lni].rgx_state ==
			    rgm::RG_PENDING_OFFLINE) {
				set_rgx_state(lni, rg,
				    rgm::RG_PENDING_OFF_STOP_FAILED);
				*r_state_changed = B_TRUE;
			}
			goto cleanup; //lint !e801

		//
		// In the following R states, a method is running.
		// We will keep the RG in pending state.
		// When the method completes, the launch_method thread
		// will re-invoke the state machine again.
		//

		case rgm::R_BOOTING:
		case rgm::R_INITING:
		case rgm::R_FINIING:
		case rgm::R_UPDATING:
			//
			// The above four states may occur in the case that a
			// "non well behaved" resource type is running a Boot,
			// Init, Fini, or Update method in the global
			// zone when the non-global zone (in which the
			// resource is actually configured) dies.
			// We have set the RG state to PENDING_OFFLINE,
			// which puts us into this code path.
			// We will exit the state machine with the RG still
			// in PENDING_OFFLINE state; when the method completes,
			// launch_method will re-invoke the state machine
			// so the RG can move to OFFLINE.
			//
			// If the resource is not a "non well behaved" resource
			// type, this is an internal error.
			//
			if (!is_r_globalzone(r)) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "PENDING_OFFLINE: bad resource state <%s> "
				    "(%d) for resource <%s>",
				    pr_rstate(rx_state), rx_state,
				    r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_INTERNAL;
				goto cleanup; //lint !e801
			}
			// FALLTHRU
		case rgm::R_STOPPING:
		case rgm::R_MON_STOPPING:
		case rgm::R_POSTNET_STOPPING:
		case rgm::R_PRENET_STARTING:
		case rgm::R_STARTING:
		case rgm::R_MON_STARTING:
			if (((rgm_switch_t *)r->rl_ccrdata->
			    r_onoff_switch)->r_switch[lni] ==
			    SCHA_SWITCH_DISABLED) {
				*all_dis_off = B_FALSE;
			}
			*all_off = B_FALSE;
			*all_methods = B_FALSE;
			// FALLTHRU

		case rgm::R_OFFLINE:
			goto cleanup; //lint !e801

		case rgm::R_OK_TO_START:
		case rgm::R_STARTING_DEPEND:
			//
			// we changed direction from a form of PENDING_ONLINE
			// to a form of PENDING_OFFLINE.  Reset our state
			// to R_OFFLINE, as we have no need to come online
			// now.
			//
			set_rx_state(lni, r, rgm::R_OFFLINE);
			*r_state_changed = B_TRUE;
			goto cleanup; //lint !e801

		case rgm::R_UNINITED:
		case rgm::R_PENDING_INIT:
		case rgm::R_PENDING_FINI:
		case rgm::R_PENDING_BOOT:
			//
			// The above four states may occur in the case that a
			// "non well behaved" resource type runs a Boot,
			// Init, or Fini method in the global zone, while the
			// non-global zone (in which the resource is actually
			// configured) dies.  We have set the RG state to
			// PENDING_OFFLINE, which puts us into this code path.
			// Since the above states are equivalent to R_OFFLINE,
			// we just set the resource offline.
			//
			// If the resource is not a "non well behaved" resource
			// type, this is an internal error -- fallthru to
			// write the syslog message.
			//
			if (is_r_globalzone(r)) {
				set_rx_state(lni, r, rgm::R_OFFLINE);
				reset_fm_status(lni, r, rgm::R_FM_OFFLINE,
				    RGM_DIR_NULL);
				goto cleanup; //lint !e801
			}
			// FALLTHRU
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "PENDING_OFFLINE: bad resource state <%s> "
			    "(%d) for resource <%s>",
			    pr_rstate(rx_state), rx_state,
			    r->rl_ccrdata->r_name);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_INTERNAL;
			goto cleanup; //lint !e801
		}

		break;


	//
	// We do not need to differentiate between these two
	// states at the R processing level.
	//
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_ON_PENDING_METHODS:

		// launch methods in those offline or online RGs
		// whose Rs are pending init, fini, or update

		switch (rx_state) {
		case rgm::R_PENDING_INIT:
			// Check if dependee boot/init methods have already run.
			if (are_dependee_boot_init_methods_pending(
			    &(r->rl_ccrdata->r_dependencies), lni)) {
				*all_methods = B_FALSE;
				ucmm_print("process_resource()",
				    NOGET("postponing init of R <%s> "
				    "because of boot/init dependencies\n"),
				    r->rl_ccrdata->r_name);
				break;
			}
			// Note scalable services use ssm_wrapper for INIT
			if (!is_method_reg(r->rl_ccrtype, METH_INIT,
			    &rt_meth_name) && !launch_args->l_is_scalable) {
				// This is an internal error; advance the state
				// to OFFLINE so we don't get caught in a loop.
				set_rx_state(lni, r, rgm::R_OFFLINE);

				//
				// Don't reset status; the resource started
				// out offline and remains offline.
				//

				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "ERROR: process_resource: resource"
				    " <%s> is pending_init but no INIT method "
				    "is registered", r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				*r_state_changed = B_TRUE;
				goto cleanup; //lint !e801
			}
			ucmm_print("RGM", NOGET("proc_r R_PENDING_INIT\n"));
			*all_methods = B_FALSE;
			launch_args->l_rt_meth_name = rt_meth_name;
			launch_args->l_method = METH_INIT;
			launch_args->l_workstate = rgm::R_INITING;
			launch_args->l_failstate = rgm::R_OFFLINE;
			launch_args->l_succstate = rgm::R_OFFLINE;
			do_launch = B_TRUE;
			break;

		case rgm::R_PENDING_UPDATE:
		case rgm::R_ON_PENDING_UPDATE:
		case rgm::R_ONUNMON_PENDING_UPDATE:
		case rgm::R_MONFLD_PENDING_UPDATE:
			// Note scalable services use ssm_wrapper for UPDATE
			if (!is_method_reg(r->rl_ccrtype, METH_UPDATE,
			    &rt_meth_name) && !launch_args->l_is_scalable) {
				// This is an internal error; advance the state
				// so we don't get caught in a loop.
				switch (rx_state) {
				case rgm::R_PENDING_UPDATE:
					set_rx_state(lni, r,
					    rgm::R_ONLINE_STANDBY);
					break;
				case rgm::R_ON_PENDING_UPDATE:
					set_rx_state(lni, r,
					    rgm::R_ONLINE);
					break;
				case rgm::R_ONUNMON_PENDING_UPDATE:
					set_rx_state(lni, r,
					    rgm::R_ONLINE_UNMON);
					break;
				case rgm::R_MONFLD_PENDING_UPDATE:
					set_rx_state(lni, r,
					    rgm::R_MON_FAILED);
					break;
				default:
					ASSERT(0);
				}
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "ERROR: process_resource: resource"
				    " <%s> is pending_update but no UPDATE "
				    "method is registered",
				    r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				*r_state_changed = B_TRUE;
				goto cleanup; //lint !e801
			}
			*all_methods = B_FALSE;
			launch_args->l_rt_meth_name = rt_meth_name;
			launch_args->l_method = METH_UPDATE;
			// We have a common workstate for both the R states.
			launch_args->l_workstate = rgm::R_UPDATING;

			ucmm_print("RGM", NOGET("proc_r %s\n"),
			    pr_rstate(r->rl_xstate[lni].rx_state));

			do_launch = B_TRUE;
			//
			// set the succstate and failstate according
			// to the current state of the R
			// r->succ_state and r->fail_state are for
			// proxy resources and it can be dynamically
			// changed by scha_control CHANGE_STATE_ONLINE
			// and CHANGE_STATE_OFFLINE calls.
			//
			if (r->rl_xstate[lni].rx_state ==
			    rgm::R_PENDING_UPDATE) {
				launch_args->l_succstate =
				    rgm::R_ONLINE_STANDBY;
				launch_args->l_failstate =
				    rgm::R_ONLINE_STANDBY;
				r->succ_state =
				    rgm::R_ONLINE_STANDBY;
				r->fail_state =
				    rgm::R_ONLINE_STANDBY;
			} else if (r->rl_xstate[lni].rx_state ==
			    rgm::R_ON_PENDING_UPDATE) {
				launch_args->l_succstate =
				    rgm::R_ONLINE;
				launch_args->l_failstate =
				    rgm::R_ONLINE;
				r->succ_state =
				    rgm::R_ONLINE;
				r->fail_state =
				    rgm::R_ONLINE;
			} else if (r->rl_xstate[lni].rx_state ==
			    rgm::R_ONUNMON_PENDING_UPDATE) {
				launch_args->l_succstate =
				    rgm::R_ONLINE_UNMON;
				launch_args->l_failstate =
				    rgm::R_ONLINE_UNMON;
			} else {
				launch_args->l_succstate =
				    rgm::R_MON_FAILED;
				launch_args->l_failstate =
				    rgm::R_MON_FAILED;
			}

			break;

		case rgm::R_UPDATING:
		case rgm::R_INITING:
		case rgm::R_FINIING:
			ucmm_print("RGM", NOGET("R <%s> still <%s>\n"),
			    r->rl_ccrdata->r_name, pr_rstate(rx_state));
			*all_methods = B_FALSE;
			goto cleanup; //lint !e801

		case rgm::R_STOP_FAILED:
			//
			// Currently this code path cannot occur because
			// the RG with a stop_failed resource would end up
			// in ERROR_STOP_FAILED state and operations that
			// would run INIT, FINI, or UPDATE are not permitted.
			// Even if we implement force-disable, we would
			// set rgx_state to PENDING_OFFLINE for the resource
			// being disabled and would not take this code path.
			// So being here would be an internal error.
			//
			// Even in the unlikely event that we get here, we
			// do not move the RG to PENDING_OFF_STOP_FAILED state
			// because other resources may remain online providing
			// service.  Instead, we set *any_stop_failed and
			// let rgm_run_state() move the RG into
			// ERROR_STOP_FAILED state when it is done with the
			// update.
			//
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// During a resource creation, deletion, or update,
			// the rgmd has discovered a resource in STOP_FAILED
			// state. This may indicate an internal logic error in
			// the rgmd, since updates are not permitted on the
			// resource group until the STOP_FAILED error
			// condition is cleared.
			// @user_action
			// Look for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "INTERNAL ERROR: process_resource: resource group "
			    "<%s> is pending_methods but contains resource <%s>"
			    " in STOP_FAILED state",
			    rg->rgl_ccr->rg_name, r->rl_ccrdata->r_name);
			sc_syslog_msg_done(&handle);
			*any_stop_failed = B_TRUE;
			goto cleanup; //lint !e801

		case rgm::R_OFFLINE:
		case rgm::R_ONLINE:
		case rgm::R_STOPPED:
		case rgm::R_PRENET_STARTED:
		case rgm::R_START_FAILED:
		case rgm::R_UNINITED:
		case rgm::R_MON_FAILED:
		case rgm::R_ONLINE_UNMON:
		case rgm::R_JUST_STARTED:
		case rgm::R_ONLINE_STANDBY:
			// *all_methods remains true
			ucmm_print("RGM",
			NOGET("PENDING_METHODS: R <%s> is <%s>, skipping\n"),
			    r->rl_ccrdata->r_name, pr_rstate(rx_state));
			goto cleanup; //lint !e801

		case rgm::R_PENDING_FINI:
			// Note scalable services use ssm_wrapper for FINI
			if (!is_method_reg(r->rl_ccrtype, METH_FINI,
			    &rt_meth_name) && !launch_args->l_is_scalable) {
				// This is an internal error; advance the state
				// to UNINITED so we don't get caught in a loop.
				set_rx_state(lni, r, rgm::R_UNINITED);

				//
				// Don't reset status; the resource started
				// out offline and remains (uninited) offline.
				//

				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "ERROR: process_resource: resource"
				    " <%s> is pending_fini but no FINI method "
				    "is registered", r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				*r_state_changed = B_TRUE;
				goto cleanup; //lint !e801
			}
			ucmm_print("RGM",
			    NOGET("proc_r R_PENDING_FINI\n"));
			*all_methods = B_FALSE;
			launch_args->l_rt_meth_name = rt_meth_name;
			launch_args->l_method = METH_FINI;
			launch_args->l_workstate = rgm::R_FINIING;
			launch_args->l_failstate = rgm::R_UNINITED;
			launch_args->l_succstate = rgm::R_UNINITED;
			do_launch = B_TRUE;
			break;

		case rgm::R_STARTING_DEPEND:
			//
			// We need to note if we are starting_depend,
			// so the RG will be moved to PENDING_ONLINE_BLOCKED
			// after the methods complete.
			//
			*any_starting_depend = B_TRUE;
			break;

		case rgm::R_OK_TO_START:
			//
			// We need to note if we are ok_to_start, so the
			// RG will be moved to PENDING_ONLINE after the
			// methods complete.
			//
			*any_ok_start = B_TRUE;
			break;

		// Shouldn't be in one of these transitory states
		case rgm::R_STOPPING_DEPEND:
		case rgm::R_OK_TO_STOP:
		case rgm::R_MON_STARTING:
		case rgm::R_MON_STOPPING:
		case rgm::R_POSTNET_STOPPING:
		case rgm::R_PRENET_STARTING:
		case rgm::R_STARTING:
		case rgm::R_STOPPING:
		case rgm::R_PENDING_BOOT:
		case rgm::R_BOOTING:
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd state machine has discovered a resource in
			// an unexpected state on the local node. This should
			// not occur and may indicate an internal logic error
			// in the rgmd.
			// @user_action
			// Look for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "PENDING_METHODS: bad resource state "
			    "<%s> (%d) for resource <%s>",
			    pr_rstate(rx_state), rx_state,
			    r->rl_ccrdata->r_name);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_RSTATE;
			goto cleanup; //lint !e801
		}

		break;

	case rgm::RG_OFF_PENDING_BOOT:

		// launch methods in those RGs whose Rs are pending boot
		switch (rx_state) {
		case rgm::R_PENDING_BOOT:
			// Check if dependee boot methods have already run.
			if (are_dependee_boot_init_methods_pending(
			    &(r->rl_ccrdata->r_dependencies), lni)) {
				*all_methods = B_FALSE;
				ucmm_print("process_resource()",
				    NOGET("postponing boot of R <%s> "
				    "because of boot/init dependencies\n"),
				    r->rl_ccrdata->r_name);
				break;
			}

			// Note scalable services use ssm_wrapper for BOOT
			if (!is_method_reg(r->rl_ccrtype, METH_BOOT,
			    &rt_meth_name) && !launch_args->l_is_scalable) {
				// This is an internal error; advance the state
				// to OFFLINE so we don't get caught in a loop.
				set_rx_state(lni, r, rgm::R_OFFLINE);

				//
				// Don't reset status; the resource started
				// out offline and remains offline.
				//

				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "ERROR: process_resource: resource"
				    " <%s> is offline pending boot, but no BOOT"
				    " method is registered",
				    r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				*r_state_changed = B_TRUE;
				goto cleanup; //lint !e801
			}
			ucmm_print("RGM",
			    NOGET("proc_r R_PENDING_BOOT\n"));
			*all_methods = B_FALSE;
			launch_args->l_rt_meth_name = rt_meth_name;
			launch_args->l_method = METH_BOOT;
			launch_args->l_workstate = rgm::R_BOOTING;
			// BOOT failure is FYI only
			launch_args->l_failstate = rgm::R_OFFLINE;
			launch_args->l_succstate = rgm::R_OFFLINE;
			do_launch = B_TRUE;
			break;

		case rgm::R_BOOTING:
			ucmm_print("RGM", NOGET("R <%s> still booting\n"),
			    r->rl_ccrdata->r_name);
			*all_methods = B_FALSE;
			res.err_code = SCHA_ERR_NOERR;
			goto cleanup; //lint !e801

		case rgm::R_OFFLINE:
			break;

		case rgm::R_MON_FAILED:
		case rgm::R_ONLINE:
		case rgm::R_ONLINE_STANDBY:
		case rgm::R_ONLINE_UNMON:
		case rgm::R_STOPPED:
		case rgm::R_PRENET_STARTED:
		case rgm::R_START_FAILED:
		case rgm::R_STOP_FAILED:
		case rgm::R_PENDING_INIT:
		case rgm::R_PENDING_FINI:
		case rgm::R_PENDING_UPDATE:
		case rgm::R_ON_PENDING_UPDATE:
		case rgm::R_ONUNMON_PENDING_UPDATE:
		case rgm::R_MONFLD_PENDING_UPDATE:
		case rgm::R_UNINITED:
		case rgm::R_INITING:
		case rgm::R_FINIING:
		case rgm::R_UPDATING:
		case rgm::R_MON_STARTING:
		case rgm::R_MON_STOPPING:
		case rgm::R_POSTNET_STOPPING:
		case rgm::R_PRENET_STARTING:
		case rgm::R_STARTING:
		case rgm::R_STOPPING:
		case rgm::R_JUST_STARTED:
		case rgm::R_STOPPING_DEPEND:
		case rgm::R_STARTING_DEPEND:
		case rgm::R_OK_TO_START:
		case rgm::R_OK_TO_STOP:

			// all of the above "should not happen" in an RG that
			// is off_pending_boot
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd state machine has discovered a resource in
			// an unexpected state on the local node. This should
			// not occur and may indicate an internal logic error
			// in the rgmd.
			// @user_action
			// Look for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "OFF_PENDING_BOOT: bad resource state <%s> "
			    "(%d) for resource <%s>",
			    pr_rstate(rx_state), rx_state,
			    r->rl_ccrdata->r_name);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_RSTATE;
			goto cleanup; //lint !e801
		}

		break;

	case rgm::RG_ON_PENDING_MON_DISABLED:
		//
		// When disabling monitor_switch on an online R,
		// RG falls into this state.  Resources may also be
		// restarting when the RG is in this state.  If r
		// is restarting (whether or not it has monitoring
		// disabled), the code above will have set rgx_state
		// (the local variable, not the RG state) to either
		// RG_PENDING_ONLINE or RG_PENDING_OFFLINE so we will not
		// enter this case.  If we have reached this point, either
		// r has monitoring disabled and needs to have its monitor
		// stopped, or r has monitoring enabled and we exit early.
		//
		if (is_local_mon_enabled(r, lni)) {
			//
			// If we meet an R with monitoring enabled,
			// it is not the R we are going to process.
			// Skip it.
			//
			break;
		}

		//
		// Similar to the RG_ON_PENDING_DISABLED case, we use
		// all_dis_off instead of all_off to indicate whether
		// monitoring has been stopped on all resources for which
		// monitoring was disabled.  (This is because restarting
		// resources will fall into the RG_PENDING_OFFLINE case
		// and may clobber all_off in there).
		//

		switch (rx_state) {
		case rgm::R_MON_FAILED:
		case rgm::R_ONLINE:
			//
			// If no monitor_stop method is registed for this R,
			// it should not have reached ONLINE state.  This
			// should not occur.  Set r to ONLINE_UNMON.
			//
			if (!is_method_reg(r->rl_ccrtype,
			    METH_MONITOR_STOP, &rt_meth_name) &&
			    launch_args->l_is_scalable) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle,
				    LOG_ERR, MESSAGE, SYSTEXT(
				    "ERROR: MONITOR_STOP method "
				    "is not registered for ONLINE "
				    "resource <%s>"),
				    r->rl_ccrdata->r_name);
				sc_syslog_msg_done(&handle);
				set_rx_state(lni, r,
				    rgm::R_ONLINE_UNMON);

				//
				// Don't reset status; resource
				// remains online (though unmonitored)
				// scstat will reveal that it is
				// unmonitored by showing
				// the R state as
				// "Online not monitored".
				//

				*r_state_changed = B_TRUE;
				goto cleanup; //lint !e801
			}

			//
			// We have found a non-restarting R which is
			// online and
			//	1. has its monitor_switch turned off and
			//	2. has its monitor_stop method
			//	   registered.
			// We will launch monitor_stop method on this R.
			//
			*all_dis_off = B_FALSE;
			*all_methods = B_FALSE;
			ucmm_print("RGM",
			    NOGET("R_ONLINE: set meth_name to <%s>\n"),
			    rt_meth_name ? rt_meth_name : "NULL");
			launch_args->l_rt_meth_name = rt_meth_name;
			launch_args->l_method = METH_MONITOR_STOP;
			launch_args->l_succstate = rgm::R_ONLINE_UNMON;
			launch_args->l_failstate = rgm::R_STOP_FAILED;
			launch_args->l_workstate = rgm::R_MON_STOPPING;

			do_launch = B_TRUE;
			break;

		case rgm::R_MON_STOPPING:
			*all_dis_off = B_FALSE;
			*all_methods = B_FALSE;
			goto cleanup; //lint !e801

		case rgm::R_UNINITED:
			// Should occur only in UNMANAGED or PENDING_METHODS
			// RGs
		case rgm::R_ONLINE_UNMON:
		case rgm::R_JUST_STARTED:
		case rgm::R_OFFLINE:
		case rgm::R_ONLINE_STANDBY:
		case rgm::R_STOPPED:
		case rgm::R_PRENET_STARTED:
		case rgm::R_START_FAILED:
			break;

		case rgm::R_STOP_FAILED:
			//
			// The RG in ON_PENDING_MON_DISABLED state contains at
			// least one resource for which monitoring has been
			// disabled and for which we need to stop the monitor.
			// It may also contain resources that are restarting.
			//
			// A STOP, POSTNET_STOP, or MONITOR_STOP method has
			// failed on r.
			//
			// We set any_stop_failed to B_TRUE; continue to
			// stop monitoring on Rs that have monitoring disabled;
			// and we continue to restart Rs, as applicable.
			// When no further progress can be made, as indicated
			// by (all_dis_off && !any_restart), rgm_run_state()
			// will note that any_stop_failed is true and move
			// the RG state to ERROR_STOP_FAILED.
			//
			*any_stop_failed = B_TRUE;
			goto cleanup; //lint !e801

		case rgm::R_STARTING_DEPEND:
			//
			// We need to note if we are starting_depend,
			// so the RG will be moved to PENDING_ONLINE_BLOCKED
			// after the monitor disabling completes.
			//
			*any_starting_depend = B_TRUE;
			break;

		case rgm::R_OK_TO_START:
			//
			// We need to note if we are ok_to_start, so the
			// RG will be moved to PENDING_ONLINE after the
			// monitor disabling completes.
			//
			*any_ok_start = B_TRUE;
			break;

		//
		// We should not be stopping or starting the resource,
		// or running INIT, FINI, BOOT, or UPDATE,
		// only (possibly) stopping the monitor.
		//
		case rgm::R_STOPPING_DEPEND:
		case rgm::R_OK_TO_STOP:
		case rgm::R_PENDING_INIT:
		case rgm::R_PENDING_FINI:
		case rgm::R_PENDING_BOOT:
		case rgm::R_PENDING_UPDATE:
		case rgm::R_ON_PENDING_UPDATE:
		case rgm::R_ONUNMON_PENDING_UPDATE:
		case rgm::R_MONFLD_PENDING_UPDATE:
		case rgm::R_INITING:
		case rgm::R_FINIING:
		case rgm::R_UPDATING:
		case rgm::R_BOOTING:
		case rgm::R_MON_STARTING:
		case rgm::R_POSTNET_STOPPING:
		case rgm::R_PRENET_STARTING:
		case rgm::R_STARTING:
		case rgm::R_STOPPING:
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd state machine has discovered a resource in
			// an unexpected state on the local node. This should
			// not occur and may indicate an internal logic error
			// in the rgmd.
			// @user_action
			// Look for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "ON_PENDING_MON_DISABLED: bad resource state "
			    "<%s> (%d) for resource <%s>",
			    pr_rstate(rx_state), rx_state,
			    r->rl_ccrdata->r_name);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_RSTATE;
			goto cleanup; //lint !e801
		}
		break;

	case rgm::RG_OFFLINE_START_FAILED:
	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_OFFLINE:
	case rgm::RG_ONLINE:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
	default:
		ucmm_print("RGM",
		    NOGET("process_resource: terminal RG "
		    "state <%s> (%d), RG <%s>, R <%s>\n"),
		    pr_rgstate(rg->rgl_xstate[lni].rgx_state),
		    rg->rgl_xstate[lni].rgx_state,
		    rg->rgl_ccr->rg_name,
		    r->rl_ccrdata->r_name);
		break;
	}

	//
	// Queue a threadpool task to launch the method while we continue
	// executing the state machine.
	//
	if (do_launch) {
		//
		// Set the local resource state now to indicate work in
		// progress.  When that work has completed, launch_method()
		// will grab the lock, update the local resource state, and
		// run the state machine.
		//
		ucmm_print("RGM", NOGET("process: call launch_method\n"));
		set_rx_state(lni, r, launch_args->l_workstate);

		//
		// Defer the launching of the callback method to the
		// threadpool that does callbacks.
		//
		launch_method_task *launch_task =
		    new launch_method_task(launch_args, lni);

		launch_method_threadpool::the().defer_processing(launch_task);

		//
		// launch_task and launchargs will be deleted by the thread
		// that processes them.
		//

		ucmm_print("RGM", NOGET("process: state change TRUE\n"));
		*r_state_changed = B_TRUE;
		ucmm_print("RGM",
		    NOGET("process: return from process_resource\n"));
		res.err_code = SCHA_ERR_NOERR;

		// If this R is restarting, set the out parameter any_restart
		if (r->rl_xstate[lni].rx_restart_flg != rgm::RR_NONE) {
			*any_restart = B_TRUE;
		}

		return (res);
	}

cleanup:
	ucmm_print("RGM", NOGET("process: return2 from process_resource\n"));
	//
	// we can arrive here with rt_
	// meth_name = launch_args->l_rt_meth_name
	// or not, so we need to take care of both cases.
	// If most cases launch_args->l_rt_meth_name
	// will be NULL except if
	// the state machine calls set_startargs and then
	// doesn't launch a method (i.e: waiting for dependencies)
	//

	//
	// At this point launch_args should be non-NULL, but we check it
	// here as a hedge against possible future code changes.
	//
	if (launch_args) {
		if (launch_args->l_rt_meth_name != rt_meth_name) {
			free(launch_args->l_rt_meth_name);
		}
		free(launch_args->l_rname);
		free(launch_args->l_rgname);
		free(launch_args);
	}
	free(rt_meth_name);

	// If this R is restarting, set the out parameter any_restart
	if (r->rl_xstate[lni].rx_restart_flg != rgm::RR_NONE) {
		*any_restart = B_TRUE;
	}

	return (res);
}


//
// Set up arguments for launching starting method (prenet_start or start).
// If resource is scalable and has a prenet_start method registered, we
// set l_succstate to PRENET_STARTED whether or not there is a START
// method registered, because in either case, launch_method() will launch
// the ssm_wrapper in lieu of START.
//
scha_errmsg_t
set_startargs(
	rlist_p_t	r,
	launchargs_t	*largs)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	name_t		rt_meth_name = NULL;

	ucmm_print("RGM", NOGET("process: set_startargs\n"));

	if (is_method_reg(r->rl_ccrtype, METH_PRENET_START, &rt_meth_name)) {

		largs->l_method = METH_PRENET_START;

		//
		// Ask whether the START method is registered but
		// don't return the method name string.
		// Scalable services always use ssm_wrapper for START.
		//
		if (r->rl_ccrtype->rt_proxy) {
			if (is_method_reg(r->rl_ccrtype, METH_START, NULL)) {
				ucmm_print("RGM", NOGET("process_resource: "
				    "proxy R <%s> has Start method registered"),
				    r->rl_ccrdata->r_name);
				res.err_code = SCHA_ERR_RT;
				free(rt_meth_name);
				return (res);
			}
			largs->l_succstate = rgm::R_ONLINE_STANDBY;
			r->succ_state = rgm::R_ONLINE_STANDBY;
			r->fail_state = rgm::R_START_FAILED;
		} else if (largs->l_is_scalable ||
		    is_method_reg(r->rl_ccrtype, METH_START, NULL)) {
			largs->l_succstate = rgm::R_PRENET_STARTED;
		} else {
			largs->l_succstate = rgm::R_JUST_STARTED;
		}
		largs->l_workstate = rgm::R_PRENET_STARTING;
	} else if (is_method_reg(r->rl_ccrtype, METH_START, &rt_meth_name) &&
	    (!r->rl_ccrtype->rt_proxy)) {
		largs->l_method = METH_START;
		largs->l_workstate = rgm::R_STARTING;
		largs->l_succstate = rgm::R_JUST_STARTED;
	} else {
		// r must register PRENET_START or START
		ucmm_print("RGM", NOGET(
		    "process_resource: R <%s> lacks a starting method\n"),
		    r->rl_ccrdata->r_name);
		res.err_code = SCHA_ERR_RT;
		free(rt_meth_name);
		return (res);
	}
	ucmm_print("RGM",
	    NOGET("process: set_startargs set meth name to <%s>\n"),
	    rt_meth_name);

	largs->l_rt_meth_name = rt_meth_name;
	largs->l_failstate = rgm::R_START_FAILED;
	res.err_code = SCHA_ERR_NOERR;
	return (res);
}


//
// Set up arguments for launching stopping method (stop or postnet_stop)
// Note, ssm_api does not have a STOP call.
//
scha_errmsg_t
set_stopargs(
	rlist_p_t	r,
	launchargs_t	*largs)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	name_t rt_meth_name;

	ucmm_print("RGM", NOGET("process: set_stopargs\n"));

	if (is_method_reg(r->rl_ccrtype, METH_STOP, &rt_meth_name)) {
		if (r->rl_ccrtype->rt_proxy) {
			ucmm_print("RGM", NOGET("process_resource: "
			    "proxy resource "
			    "R <%s> has Stop method registered\n"),
			    r->rl_ccrdata->r_name);
			res.err_code = SCHA_ERR_RT;
			free(rt_meth_name);
			return (res);
		}
		largs->l_method = METH_STOP;
		if (is_method_reg(r->rl_ccrtype, METH_POSTNET_STOP, NULL)) {
			largs->l_succstate = rgm::R_STOPPED;
		} else {
			largs->l_succstate = rgm::R_OFFLINE;
		}
		largs->l_workstate = rgm::R_STOPPING;
	} else if (is_method_reg(r->rl_ccrtype, METH_POSTNET_STOP,
	    &rt_meth_name)) {
		largs->l_method = METH_POSTNET_STOP;
		largs->l_succstate = rgm::R_OFFLINE;
		largs->l_workstate = rgm::R_POSTNET_STOPPING;
		r->succ_state = rgm::R_OFFLINE;
		r->fail_state = rgm::R_STOP_FAILED;
	} else {
		// r must register STOP or POSTNET_STOP
		ucmm_print("RGM", NOGET(
		    "process_resource: R <%s> lacks a stopping method\n"),
		    r->rl_ccrdata->r_name);
		res.err_code = SCHA_ERR_RT;
		return (res);
	}

	largs->l_rt_meth_name = rt_meth_name;
	largs->l_failstate = rgm::R_STOP_FAILED;
	res.err_code = SCHA_ERR_NOERR;
	return (res);
}


//
// postpone_start_r
//
// Returns true if we need to postpone running this R's {start, prenet
// start} method because we have strong/weak/restart dependencies on other Rs
// in the same RG whose {boot/init, start, prenet start} methods have not yet
// run, or we have a strong/weak/restart dependency on an R in a different RG
// (in which case we need to get permission from the president to start
// this R), or we are a network address trying to run START and other
// resources in our group have not yet run PRENET_START, or we are not a
// network address and we're trying to run PRENET_START or START and network
// address resources in our group have not yet run PRENET_START or START
// respectively.
//
// Returns false if it's OK to {start, prenet start} this R now.
//
// Checks are done in this order: boot/init dependency check,
// implicit, then explicit.  Implicit checks
// are done only for resources in the same group.
//
// First we check if all the dependees have executed their boot/init methods
// locally. Call are_dependee_boot_init_methods_pending for all the dependee
// resources in the strong/weak/restart dependency list.
//
// The out parameter is_boot_dep_pending will be set to B_TRUE if the
// dependency that causes us to postpone starting is a boot/init dependency.
// Normally it will be a boot dependency, because it would be unlikely
// for the system administrator to be managing the dependee and starting
// the dependent at the same time.  Nonetheless, we handle BOOT and INIT
// as equivalent.
//
// Second, if we are not a network address resource, and we're trying to
// run PRENET_START or START, make sure that all network address resources in
// our group have run PRENET_START or START respectively.
//
// Third, if this R is a network address resource trying to run START,
// checks that all non-network address resources in the same R have already
// run PRENET_START.
//
// Fourth, call starting_iter_deps_list for each of the three types of
// dependencies: strong/weak/restart/offline-restart.  This subroutine checks
// explicit dependencies.
//
// The out parameter is_inter_rg_dep will be set to B_TRUE if the dependency
// that causes us to postpone starting is an inter-RG dependency, and will
// be set to B_FALSE if the dependency is an intra-RG dependency.
//
// Note that The intra-rg check is done first.  Only if there are no
// intra-rg deps which require postponement of starting are the inter-rg
// dependencies considered.
//
static boolean_t
postpone_start_r(rlist_p_t r, rgm::lni_t lni, method_t meth,
    boolean_t &is_inter_rg_dep, boolean_t &is_boot_dep_pending)
{
	rlist_p_t rp;
	sc_syslog_msg_handle_t handle;
	// dep_r_start_flag is passed to R_STATE_AT_least
	boolean_t dep_r_start_flag = B_FALSE;
	// Get the state of the resource.
	rgm::rgm_r_state	dep_r_state = r->rl_xstate[lni].rx_state;

	ucmm_print("RGM", NOGET("In postpone_start_r: res=<%s>\n"),
	    r->rl_ccrdata->r_name);

	is_inter_rg_dep = B_FALSE;
	is_boot_dep_pending = B_FALSE;

	// Set the dep_r_start_flag to TRUE only if the
	// state of the resource is R_PRENET_STARTED.
	if (dep_r_state == rgm::R_PRENET_STARTED) {
		dep_r_start_flag = B_TRUE;
	}

	//
	// First we check boot/init dependencies.
	// If 'r' is dependent on some resource and the dependee is currently
	// executing a boot or init method which has not yet finished
	// executing, we need to postpone the start of 'r'
	//
	if (are_dependee_boot_init_methods_pending(
	    &(r->rl_ccrdata->r_dependencies), lni)) {
		ucmm_print("RGM", NOGET("postponing start of R <%s> because "
		    "of boot/init dependency\n"), r->rl_ccrdata->r_name);
		is_boot_dep_pending = B_TRUE;
		return (B_TRUE);
	}

	//
	// We check implicit strong dependencies.
	// If 'r' is a non-network address then we have to make sure before
	// it runs relevent starting method, all network address R should be
	// in the right started state.
	if (r->rl_rg->rgl_ccr->rg_impl_net_depend &&
	    (r->rl_ccrtype->rt_sysdeftype != SYST_LOGICAL_HOSTNAME &&
	    r->rl_ccrtype->rt_sysdeftype != SYST_SHARED_ADDRESS)) {

		// 'r' is not a network address, then we are going to
		// check the state of network address Rs since they
		// are implicitely depended by all other type Rs in the RG.

		for (rp = r->rl_rg->rgl_resources; rp != NULL;
		    rp = rp->rl_next) {

			// in the list of Rs, skip 'r'
			if (rp == r)
				continue;

			// skip non-network address Rs
			if (rp->rl_ccrtype->rt_sysdeftype !=
			    SYST_LOGICAL_HOSTNAME &&
			    rp->rl_ccrtype->rt_sysdeftype !=
			    SYST_SHARED_ADDRESS)
				continue;

			if (((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->
			    r_switch[lni] == SCHA_SWITCH_DISABLED) {
				ucmm_print("RGM", NOGET(
				    "postpone_start_r: <%s> implicitly depends "
				    "on <%s> which is a network resource and "
				    "in disabled state"), r->rl_ccrdata->r_name,
				    rp->rl_ccrdata->r_name);
				return (B_TRUE);
			}

			switch (meth) {
			case METH_PRENET_START:
				if (r_state_at_least(rp, rgm::R_PRENET_STARTED,
				    lni, DEPS_STRONG, dep_r_start_flag))
					continue;
				ucmm_print("RGM", NOGET(
				    "postpone_start_r: implicit <%s> "
				    "is <%s>\n"), rp->rl_ccrdata->r_name,
				    pr_rstate(
				    rp->rl_xstate[lni].rx_state));
				break;

			case METH_START:
				if (r_state_at_least(rp, rgm::R_JUST_STARTED,
				    lni, DEPS_STRONG, dep_r_start_flag))
					continue;

				ucmm_print("RGM", NOGET(
				    "postpone_start_r: implicit "
				    "<%s> is <%s>\n"), rp->rl_ccrdata->r_name,
				    pr_rstate(
				    rp->rl_xstate[lni].rx_state));
				break;

			case METH_STOP:
			case METH_VALIDATE:
			case METH_UPDATE:
			case METH_INIT:
			case METH_FINI:
			case METH_BOOT:
			case METH_MONITOR_START:
			case METH_MONITOR_STOP:
			case METH_MONITOR_CHECK:
			case METH_POSTNET_STOP:
			default:
				// programming error
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "INTERNAL ERROR: postpone_start_r: "
				    "meth type <%d>", meth);
				sc_syslog_msg_done(&handle);
				return (B_FALSE);
			}

			// Found a network address R on which 'r'
			// implicitly depends, and which is not yet
			// prenet started/online/online unmon.
			// We must postpone starting 'r'.
			return (B_TRUE);
		}
	} else if (r->rl_rg->rgl_ccr->rg_impl_net_depend &&
	    meth == METH_START) {

		// 'r' is a network address R and going to run start method,
		// we have to make sure all non-network address Rs have
		// reached PRENET_STARTED state.
		//
		// In following check, we are only interested in non-network
		// address Rs. Skip any network address Rs and disabled Rs.
		for (rp = r->rl_rg->rgl_resources; rp != NULL;
		    rp = rp->rl_next) {

			// Skip myself.
			if (rp == r)
				continue;

			// skip network address Rs. We are only checking
			// non-network-address Rs.
			if (rp->rl_ccrtype->rt_sysdeftype ==
			    SYST_LOGICAL_HOSTNAME ||
			    rp->rl_ccrtype->rt_sysdeftype ==
			    SYST_SHARED_ADDRESS)
				continue;

			// skip disabled non-network-address Rs
			if (((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->
			    r_switch[lni] == SCHA_SWITCH_DISABLED)
				continue;

			// r_state_at_least will check for the non-network
			// address R having no prenet_start method registered

			// Make sure the non-network address R has reached
			// PRENET_STARTED state. Otherwise return true.
			if (!r_state_at_least(rp, rgm::R_PRENET_STARTED,
			    lni, DEPS_STRONG, dep_r_start_flag))
				return (B_TRUE);
		}
	}

	boolean_t is_inter_rg_dep_strong = B_FALSE;
	boolean_t is_inter_rg_dep_restart = B_FALSE;
	boolean_t is_inter_rg_dep_restart_offline = B_FALSE;
	//
	// Pass the dep_r_start_flag as r_state_at_least will
	// be called in starting_iter_deps_list.
	//
	if (starting_iter_deps_list(r, DEPS_STRONG, lni, meth,
	    is_inter_rg_dep_strong, dep_r_start_flag)) {
		//
		// If the dependency we found is an inter-rg dependency,
		// we need to check RESTART and WEAK dependencies for
		// intra-rg dependencies before returning B_TRUE.  That is
		// because intra-RG dependencies have a higher priority than
		// inter-RG dependencies for postponing.
		//
		if (!is_inter_rg_dep_strong) {
			return (B_TRUE);
		}
	}

	//
	// Pass the dep_r_start_flag as r_state_at_least will
	// be called in starting_iter_deps_list.
	//
	if (starting_iter_deps_list(r, DEPS_RESTART, lni, meth,
	    is_inter_rg_dep_restart, dep_r_start_flag)) {
		//
		// If the dependency we found is an inter-rg dependency,
		// we need to check WEAK dependencies for
		// intra-rg dependencies before returning B_TRUE.  That is
		// because intra-RG dependencies have a higher priority than
		// inter-RG dependencies for postponing.
		//
		if (!is_inter_rg_dep_restart) {
			return (B_TRUE);
		}
	}

	//
	// Pass the dep_r_start_flag as r_state_at_least will
	// be called in starting_iter_deps_list.
	//
	if (starting_iter_deps_list(r, DEPS_OFFLINE_RESTART, lni, meth,
	    is_inter_rg_dep_restart_offline, dep_r_start_flag)) {
		//
		// If the dependency we found is an inter-rg dependency,
		// we need to check WEAK dependencies for
		// intra-rg dependencies before returning B_TRUE.  That is
		// because intra-RG dependencies have a higher priority than
		// inter-RG dependencies for postponing.
		//
		if (!is_inter_rg_dep_restart_offline) {
			return (B_TRUE);
		}
	}

	//
	// Pass the dep_r_start_flag as r_state_at_least will
	// be called in starting_iter_deps_list.
	//
	if (starting_iter_deps_list(r, DEPS_WEAK, lni, meth,
	    is_inter_rg_dep, dep_r_start_flag)) {
		//
		// Return true whether it was inter-rg or intra-rg deps.
		// There are no more to check.
		//
		return (B_TRUE);
	}

	//
	// If we found an inter-RG dependency earlier, note that fact, and
	// return B_TRUE to specify that we should postpone.
	//
	if (is_inter_rg_dep_strong || is_inter_rg_dep_restart ||
	    is_inter_rg_dep_restart_offline) {
		is_inter_rg_dep = B_TRUE;
		return (B_TRUE);
	} else {
		// OK to call method for 'r' now.
		return (B_FALSE);
	}
}


//
// postpone_stop_r
//
// Returns true if we need to postpone running this R's {stop, postnet
// stop} method because other Rs in the same RG whose {stop, postnet stop}
// methods have not yet run have strong/weak/restart dependencies on
// this R, or an R in a different RG has a strong/weak/restart dependency on
// this R (in which case we need to get permission from the president to stop
// this R), or we are a network address and other resources in our group
// have not yet run STOP or POSTNET_STOP (depending on whether we are running
// STOP or POSTNET_STOP), or we are not a network address and we're trying
// to run POSTNET_STOP and network address resources in our group have not
// yet run STOP.
//
// Returns false if it's OK to {stop, postnet stop} this R now.
//
// Checks are done in this order: implicit, then explicit.  Implicit checks
// are done only for resources in the same group.
//
// First, if this R is a network address resource, checks that all non-network
// address resources in the same R have already stopped to the appropriate
// level (run STOP if we're trying to run STOP, run POSTNET_STOP if we're
// trying to run POSTNET_STOP).
//
// Second, if we are not a network address resource, and we're trying to
// run POSTNET_STOP, make sure that all network address resources in our
// group have run STOP.
//
// Third, call stopping_iter_deps_list for each of the three types of
// dependencies: strong/weak/restart.  This subroutine checks explicit
// dependencies.
//
// The out parameter is_inter_rg_dep will be set to B_TRUE if the dependency
// that causes us to postpone stopping is an inter-RG dependency, and will
// be set to B_FALSE if the dependency is an intra-RG dependency.
//
// Note that The intra-rg check is done first.  Only if there are no
// intra-rg deps which require postponement of stopping are the inter-rg
// dependencies considered.
//
static boolean_t
postpone_stop_r(rlist_p_t r, sol::nodeid_t lni, method_t meth,
    boolean_t &is_inter_rg_dep, boolean_t *restart_triggered)
{
	rlist_p_t rp;
	sc_syslog_msg_handle_t handle;


	ucmm_print("RGM", NOGET("In postpone_stop_r: res=<%s>\n"),
	    r->rl_ccrdata->r_name);

	is_inter_rg_dep = B_FALSE;

	// If 'r' is a network address R within a RG with
	// rg_impl_net_depend set, it should make sure all other
	// non-network address Rs within the RG have already run the
	// relevent stopping method.
	if (r->rl_rg->rgl_ccr->rg_impl_net_depend &&
	    (r->rl_ccrtype->rt_sysdeftype == SYST_LOGICAL_HOSTNAME ||
	    r->rl_ccrtype->rt_sysdeftype == SYST_SHARED_ADDRESS)) {

		// 'r' is network address.
		for (rp = r->rl_rg->rgl_resources; rp != NULL;
		    rp = rp->rl_next) {

			// Skip myself.
			if (rp == r)
				continue;

			//
			// We do *not* skip disabled resources.  Since we
			// support disabling multiple resources at once (and
			// possibly also restarting other resources at the same
			// time), we need to enforce stopping dependencies
			// between all resources in the same RG that might be
			// stopping.
			//
			// If a disabled resource is already offline, checking
			// it will be a no-op anyway.
			//
			// r_state_at_least() will filter out starting
			// resources (e.g., restarting resources that are
			// in the "starting" phase), because it is too late
			// to enforce stopping dependencies on them.
			//

			// skip other network address Rs
			if (rp->rl_ccrtype->rt_sysdeftype ==
			    SYST_LOGICAL_HOSTNAME ||
			    rp->rl_ccrtype->rt_sysdeftype ==
			    SYST_SHARED_ADDRESS)
				continue;

			switch (meth) {
			case METH_STOP:
				//
				// dep_r_start_flag does not have any
				// use when the state is R_STOPPED.So
				// B_FALSE is passed.
				//
				if (r_state_at_least(rp, rgm::R_STOPPED,
				    lni, DEPS_STRONG, B_FALSE))
					continue;

				ucmm_print("RGM", NOGET(
				    "postpone_stop_r: implicit <%s> is <%s>\n"),
				    rp->rl_ccrdata->r_name,
				    pr_rstate(
				    rp->rl_xstate[lni].rx_state));
				break;

			case METH_POSTNET_STOP:
				//
				// dep_r_start_flag does not have any
				// use when the state is R_OFFLINE.So
				// B_FALSE is passed.
				//
				if (r_state_at_least(rp, rgm::R_OFFLINE,
				    lni, DEPS_STRONG, B_FALSE))
					continue;

				ucmm_print("RGM", NOGET(
				    "postpone_stop_r: implicit <%s> is <%s>\n"),
				    rp->rl_ccrdata->r_name,
				    pr_rstate(
				    rp->rl_xstate[lni].rx_state));
				break;

			case METH_START:
			case METH_VALIDATE:
			case METH_UPDATE:
			case METH_INIT:
			case METH_FINI:
			case METH_BOOT:
			case METH_MONITOR_START:
			case METH_MONITOR_STOP:
			case METH_MONITOR_CHECK:
			case METH_PRENET_START:
			default:
				// these cases indicate programming error
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "INTERNAL ERROR: postpone_stop_r: "
				    "meth type <%d>", meth);
				sc_syslog_msg_done(&handle);
				return (B_FALSE);
			}

			//
			// Found an R that depends on 'r',
			// and that is not yet stopped/offline.
			// We must postpone stopping 'r'.
			//
			ucmm_print("RGM", NOGET("postpone_stop_r: res=<%s>. "
			    "Found implicit dep of non-network res on us."),
			    r->rl_ccrdata->r_name);

			return (B_TRUE);
		}
	} else if (r->rl_rg->rgl_ccr->rg_impl_net_depend &&
	    meth == METH_POSTNET_STOP) {
		// If 'r' is non-network address within a RG with
		// rg_impl_net_depend flag set, do reverse dependency check.

		// If the 'r' is non-network address R and going to run
		// postnet_stop method, it should make sure that all
		// network address Rs in the RG have finished running
		// stop method.
		for (rp = r->rl_rg->rgl_resources; rp != NULL;
		    rp = rp->rl_next) {
			// 'r' is non-network address R
			// Skip myself.
			if (rp == r)
				continue;

			// Skip non-network address Rs, We only check
			// network address Rs.
			if (rp->rl_ccrtype->rt_sysdeftype !=
			    SYST_LOGICAL_HOSTNAME &&
			    rp->rl_ccrtype->rt_sysdeftype !=
			    SYST_SHARED_ADDRESS)
				continue;

			//
			// We do *not* skip disabled resources, since they
			// might also be stopping.  See comment in the for-loop
			// above for more info.
			//
			// r_state_at_least will check for the network address
			// R having no STOP method registered.  This should
			// not occur, but the check will do no harm.
			//
			// Make sure the network address R has finished
			// stop method. Otherwise return true.
			//
			// dep_r_start_flag does not have any
			// use when the state is R_STOPPED.So
			// B_FALSE is passed.
			//
			if (!r_state_at_least(rp, rgm::R_STOPPED, lni,
			    DEPS_STRONG, B_FALSE)) {
				ucmm_print("RGM", NOGET("postpone_stop_r: res="
				    " <%s>. Found implicit dep on network "
				    "addr"), r->rl_ccrdata->r_name);

				return (B_TRUE);
			}
		}
	}

	boolean_t is_inter_rg_dep_strong = B_FALSE;
	boolean_t is_inter_rg_dep_restart = B_FALSE;
	boolean_t is_inter_rg_off_dep_restart = B_FALSE;

	if (stopping_iter_deps_list(r, DEPS_STRONG, lni, meth,
	    is_inter_rg_dep_strong, restart_triggered)) {
		//
		// If the dependency we found is an inter-rg dependency,
		// we need to check RESTART and WEAK dependencies for
		// intra-rg dependencies before returning B_TRUE.  That is
		// because intra-RG dependencies have a higher priority than
		// inter-RG dependencies for postponing.
		//
		if (!is_inter_rg_dep_strong) {
			return (B_TRUE);
		}
	}

	if (stopping_iter_deps_list(r, DEPS_RESTART, lni, meth,
	    is_inter_rg_dep_restart, restart_triggered)) {
		//
		// If the dependency we found is an inter-rg dependency,
		// we need to check WEAK dependencies for
		// intra-rg dependencies before returning B_TRUE.  That is
		// because intra-RG dependencies have a higher priority than
		// inter-RG dependencies for postponing.
		//
		if (!is_inter_rg_dep_restart) {
			return (B_TRUE);
		}
	}

	if (stopping_iter_deps_list(r, DEPS_OFFLINE_RESTART, lni, meth,
	    is_inter_rg_off_dep_restart, restart_triggered)) {
		//
		// If the dependency we found is an inter-rg dependency,
		// we need to check WEAK dependencies for
		// intra-rg dependencies before returning B_TRUE.  That is
		// because intra-RG dependencies have a higher priority than
		// inter-RG dependencies for postponing.
		//
		if (!is_inter_rg_off_dep_restart) {
			return (B_TRUE);
		}
	}

	// Check if the resource has any strong inter cluster dependents.
	if (r->rl_ccrdata->r_inter_cluster_dependents.dp_strong)
		is_inter_rg_dep_strong = B_TRUE;
	if (r->rl_ccrdata->r_inter_cluster_dependents.dp_restart)
		is_inter_rg_dep_restart = B_TRUE;
	if (r->rl_ccrdata->r_inter_cluster_dependents.dp_offline_restart)
		is_inter_rg_off_dep_restart = B_TRUE;

	if (stopping_iter_deps_list(r, DEPS_WEAK, lni, meth,
	    is_inter_rg_dep, restart_triggered)) {
		return (B_TRUE);
	}

	//
	// If we found an inter-RG dependency earlier, note that fact, and
	// return B_TRUE to specify that we should postpone.
	//
	if (is_inter_rg_dep_strong || is_inter_rg_dep_restart ||
	    is_inter_rg_off_dep_restart) {
		is_inter_rg_dep = B_TRUE;
		return (B_TRUE);
	} else {
		ucmm_print("RGM", NOGET("postpone_stop_r: res=<%s>. "
		    "No dependencies found."), r->rl_ccrdata->r_name);

		// no dependencies were found; OK to call method for 'r' now.
		return (B_FALSE);
	}
}


//
// are_dependee_boot_init_methods_pending
//
// This function returns B_TRUE if currently running dependee boot or init
// methods have not yet completed on lni; otherwise returns B_FALSE.
// This is called by the process_resource function while processing
// for resource in R_PENDING_BOOT (RG in RG_OFF_PENDING_BOOT) or for
// resource in R_PENDING_INIT (RG in RG_OFF_PENDING_METHODS).
// This function is also called in postpone_start_r to delay the
// execution of start method if the boot/init methods of the dependee
// have not yet completed.
// This is a recursive function which traverses the dependency tree of
// any resource given its dependency list pointer.(rgm_dependencies_t*)
//
// If the dependee's rgx_posponed_boot flag is set on lni, then return
// B_TRUE, as the postponed boot methods are yet to run.  This will postpone
// running the Boot and starting methods of the dependent until the dependee
// finally goes offline and executes its own Boot methods, if any, in the
// zone.  The enforcement of Boot method dependency ordering is required e.g.
// by scalable services and SharedAddresses when they are starting on the
// same node.
//
static boolean_t
are_dependee_boot_init_methods_pending(const rgm_dependencies_t *r_dep_list_ptr,
    rgm::lni_t lni)
{
	rlist_p_t rp;
	rdeplist_t *nlp;
	int i;
	sc_syslog_msg_handle_t handle;

	ucmm_print("are_dep_boot_init_meths_pending", NOGET("enter"));

	for (i = DEPS_WEAK; i <= DEPS_OFFLINE_RESTART; i++) {
		switch (i) {
		case DEPS_STRONG:
			nlp = r_dep_list_ptr->dp_strong;
			break;
		case DEPS_WEAK:
			nlp = r_dep_list_ptr->dp_weak;
			break;
		case DEPS_RESTART:
			nlp = r_dep_list_ptr->dp_restart;
			break;
		case DEPS_OFFLINE_RESTART:
			nlp = r_dep_list_ptr->dp_offline_restart;
			break;
		default:
			// programming error
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// A non-fatal internal error has occurred in the rgmd
			// state machine.
			// @user_action
			// Since this problem might indicate an internal logic
			// error in the rgmd, save a copy of the
			// /var/adm/messages files on all nodes, and the
			// output of clresourcetype show -v, clresourcegroup
			// show -v +, and clresourcegroup status +. Report the
			// problem to your authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "INTERNAL ERROR: are_dependee_boot_methods_pending"
			    " invalid dependency type <%d>", i);
			sc_syslog_msg_done(&handle);
			return (B_TRUE);
		}

		// Get the resource pointer of the first resource in the list.
		while (nlp) {
#if (SOL_VERSION >= __s10)
			// Check if it is a remote cluster resource.
			if (!is_enhanced_naming_format_used(nlp->rl_name))
				break;
#endif
			nlp = nlp->rl_next;
		}

		rp = (nlp ? rname_to_r(NULL, nlp->rl_name) : NULL);

		while (rp != NULL) {
			//
			// Check if the r's resource group has the
			// postponed_boot flag set on this lni.
			//
			if (rp->rl_rg->rgl_xstate[lni].rgx_postponed_boot) {
				ucmm_print("dep meths pending",
				    NOGET("res %s postponed boot"),
				    rp->rl_ccrdata->r_name);
				return (B_TRUE);
			}
			//
			// Check if the r is in R_PENDING_BOOT or R_BOOTING
			// or R_PENDING_INIT or R_INITING state.
			//
			rgm::rgm_r_state rxstate = rp->rl_xstate[lni].rx_state;
			if (rxstate == rgm::R_PENDING_BOOT ||
			    rxstate == rgm::R_BOOTING ||
			    rxstate == rgm::R_PENDING_INIT ||
			    rxstate == rgm::R_INITING) {
				return (B_TRUE);
			}

			//
			// Call are_dependee_boot_init_methods_pending
			// recursively on the dependee r's dependency
			// lists.
			//
			if (are_dependee_boot_init_methods_pending(
			    &(rp->rl_ccrdata->r_dependencies),  lni)) {
				return (B_TRUE);
			}
			// Get the next resource pointer in the dependency list
			while (nlp) {
#if (SOL_VERSION >= __s10)
				// Check if it is a remote cluster resource.
				if (!is_enhanced_naming_format_used(
				    nlp->rl_name))
					break;
#endif
				nlp = nlp->rl_next;
			}
			rp = (nlp->rl_next ?
			    rname_to_r(NULL, nlp->rl_next->rl_name) : NULL);
			nlp = nlp->rl_next;
		}
	}

	return (B_FALSE);
}

//
// starting_iter_deps_list
//
// Called by postpone_start_r().
//
// Checks strong, weak, or restart explicit dependencies as specified by
// 'depsflag'.
//
// First, checks all resources on which we depend.
// If the dependee resource is in the same resource group as us,
// checks the state of the dependee resource to determine if r needs to
// wait for it or not.  Returns B_TRUE immediately if the dependee resource
// is not in a proper state for r to start.
//
// If the dependee resource is in a different resource group, we note that
// we have found a dependee resource in a different group.  However, we do
// not check the state of that resource (because we do not have access to its
// state on all nodes).  We also do not return B_TRUE
// immediately. Only if there are no resources in the same group for which we
// need to wait do we set is_inter_rg_dep to B_TRUE and return B_TRUE (if
// we have found a resource in another group that we depend on).
//
// To put it another way, we make it a higher priority to check for intra-rg
// dependencies, and only if those are all satisfied, do we block for
// inter-rg dependencies.  That way, once the president tells us that inter-rg
// deps are satisifed, we can proceed with the start, and do not need to check
// local dependencies again.
//
// dep_r_start_flag is set in postpone_start_r and is passed in this function
// because r_state_at_least is called within this function.
//
static boolean_t
starting_iter_deps_list(rlist_p_t r, deptype_t depsflag,
    rgm::lni_t lni, method_t meth, boolean_t &is_inter_rg_dep,
    boolean_t dep_r_start_flag)
{
	rlist_p_t rp;
	rdeplist_t *nlp;
	sc_syslog_msg_handle_t handle;
	boolean_t found_inter_rg_deps = B_FALSE;

	switch (depsflag) {
	case DEPS_STRONG:
		nlp = r->rl_ccrdata->r_dependencies.dp_strong;
		break;
	case DEPS_WEAK:
		nlp = r->rl_ccrdata->r_dependencies.dp_weak;
		break;
	case DEPS_RESTART:
		nlp = r->rl_ccrdata->r_dependencies.dp_restart;
		break;
	case DEPS_OFFLINE_RESTART:
		nlp = r->rl_ccrdata->r_dependencies.dp_offline_restart;
		break;
	default:
		// programming error
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A non-fatal internal error has occurred in the rgmd state
		// machine.
		// @user_action
		// Since this problem might indicate an internal logic error
		// in the rgmd, save a copy of the /var/adm/messages files on
		// all nodes, and the output of clresourcetype show -v,
		// clresourcegroup show -v +, and clresourcegroup status +.
		// Report the problem to your authorized Sun service provider.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR: starting_iter_deps_list: invalid "
		    "dependency type <%d>", depsflag);
		sc_syslog_msg_done(&handle);
		return (B_FALSE);
	}

	// First, we check state of Rs in the dependency list.
	for (; nlp != NULL; nlp = nlp->rl_next) {
#if (SOL_VERSION >= __s10)
		// Check if it is a remote cluster resource.
		if (is_enhanced_naming_format_used(nlp->rl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "starting_iter_deps_list "
				    "Running at too low a version:R <%s> "
				    "has dependency on R <%s> "
				    "which is in not in local cluster\n"),
				    r->rl_ccrdata->r_name, nlp->rl_name);
				continue;
			}
			found_inter_rg_deps = B_TRUE;
			continue;
		}
#endif
		rp = rname_to_r(NULL, nlp->rl_name);
		CL_PANIC(rp != NULL);

		//
		// we treat the inter-rg and intra-rg cases differently.
		// If the resources are in the same resource group, we
		// check the state of the dependee resource, and make sure
		// that we are allowed to start the dependent.
		//
		// If the resources are in different groups, we just note
		// that we have found an inter-rg dependency.  Later, if
		// we have found that we do not need to pospone starting
		// this r because of intra-rg deps, we will postpone starting
		// it because of inter-rg deps.
		//
		if (r->rl_rg == rp->rl_rg) {
			//
			// we are in the same resource group.
			// Check dependencies explicitly.
			//

			ucmm_print("RGM",
			    NOGET("starting_iter_deps_list: found dependee "
			    "<%s> in my RG. State is <%s>\n"),
			    rp->rl_ccrdata->r_name,
			    pr_rstate(rp->rl_xstate[lni].rx_state));

			//
			// Disabled checking is done for a resource
			// which is OFFLINE and about to run either
			// Prenet_start or Start.  However, the check is
			// bypassed for a resource which has achieved
			// PRENET_STARTED state and is about to run Start.
			//
			if (((rgm_switch_t *)rp->rl_ccrdata->
			    r_onoff_switch)->r_switch[lni] ==
			    SCHA_SWITCH_DISABLED) {
				return ((boolean_t)!(dep_r_start_flag));
			}

			switch (meth) {
			case METH_PRENET_START:
				if (r_state_at_least(rp, rgm::R_PRENET_STARTED,
				    lni, depsflag, dep_r_start_flag))
					continue;
				break;

			case METH_START:
				if (r_state_at_least(rp, rgm::R_JUST_STARTED,
				    lni, depsflag, dep_r_start_flag))
					continue;
				break;

			case METH_STOP:
			case METH_VALIDATE:
			case METH_UPDATE:
			case METH_INIT:
			case METH_FINI:
			case METH_BOOT:
			case METH_MONITOR_START:
			case METH_MONITOR_STOP:
			case METH_MONITOR_CHECK:
			case METH_POSTNET_STOP:
			default:
				// these cases indicate programming error
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "INTERNAL ERROR: "
				    "starting_iter_deps_list: meth type <%d>",
				    meth);
				sc_syslog_msg_done(&handle);
				return (B_FALSE);
			}

			//
			// Found an R in the same RG on which 'r' has an
			// explicit dependency and which is not yet prenet
			// started/online/online unmon.  We must postpone
			// starting 'r'.
			//
			ucmm_print("RGM",
			    NOGET("starting_iter_deps_list: <%s> is dependent "
			    "on <%s>, and they are in the same group."),
			    r->rl_ccrdata->r_name,
			    rp->rl_ccrdata->r_name);
			return (B_TRUE);
		} else {
			//
			// We found an inter-rg dependency that will cause
			// us to block (move to R_STARTING_DEPEND) if there
			// are no intra-rg dependencies.
			//
			ucmm_print("RGM",
			    NOGET("starting_iter_deps_list: <%s> is dependent "
			    "on <%s>, and they are in different groups."),
			    r->rl_ccrdata->r_name,
			    rp->rl_ccrdata->r_name);

			found_inter_rg_deps = B_TRUE;
		}
	}

	//
	// If we're running PRENET_START we can exit now.  More checks for
	// START exist below.
	//
	if (meth != METH_START) {
		goto starting_iter_deps_list_end; //lint !e801
	}

	//
	// Secondly, we are checking reverse dependency. If another resource
	// in the same group depends on us, we have to make
	// sure it runs prenet_start before we run start.
	//
	// Note: this test only applies to start method.
	// Note: we do not enforce net-relative ordering for inter-rg deps.
	// Note: We use the rl_dependents list of r to find resources that
	// depend on us.
	//

	//
	// Get the proper dependents list for the type of dependency that
	// we are checking.
	//
	switch (depsflag) {
	case DEPS_STRONG:
		nlp = r->rl_dependents.dp_strong;
		break;
	case DEPS_WEAK:
		nlp = r->rl_dependents.dp_weak;
		break;
	case DEPS_RESTART:
		nlp = r->rl_dependents.dp_restart;
		break;
	case DEPS_OFFLINE_RESTART:
		nlp = r->rl_dependents.dp_offline_restart;
		break;

	default:
		// programming error
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR: starting_iter_deps_list: invalid "
		    "dependency type <%d>", depsflag);
		sc_syslog_msg_done(&handle);
		return (B_FALSE);
	}

	//
	// iterate through our strong/weak/restart dependents list.
	//
	for (; nlp != NULL; nlp = nlp->rl_next) {

#if (SOL_VERSION >= __s10)
		// Check if it is a remote cluster resource.
		if (is_enhanced_naming_format_used(nlp->rl_name))
			continue;
#endif
		// get a pointer to the resource with this name
		rp = rname_to_r(NULL, nlp->rl_name);
		CL_PANIC(rp != NULL);

		// skip resources that are not in our RG
		if (rp->rl_rg != r->rl_rg) {
			continue;
		}

		// skip R if it is disabled on the same node.
		if (((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->
		    r_switch[lni] == SCHA_SWITCH_DISABLED)
			continue;

		// r_state_at_least will check for the resource rp
		// having no PRENET_START method registered.

		if (!r_state_at_least(rp, rgm::R_PRENET_STARTED,
		    lni, depsflag, dep_r_start_flag))
			return (B_TRUE);
	}

starting_iter_deps_list_end:

	//
	// If we reach here, we did not find any intra-RG dependencies that
	// are not fulfilled.  Specify in the return value whether we found
	// inter-RG dependencies.
	//
	if (found_inter_rg_deps) {
		is_inter_rg_dep = B_TRUE;
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}


//
// stopping_iter_deps_list
//
// Called by postpone_stop_r().
//
// Checks strong, weak, or restart explicit dependencies as specified by
// 'depsflag'.
//
// First, uses the rl_dependents field to look for Rs with a dependency on
// 'r'.  If the dependent resource is in the same resource group as r,
// checks the state of the dependent resource to determine if r needs to
// wait for it or not.  Returns B_TRUE immediately if the dependent resource
// is not in a proper state for r to stop.
//
// If the dependent resource is in a different resource group, we note that
// we have found a dependent resource in a different group.  However, we do
// not check the state of that resource (because we do not have access to its
// state on all nodes).  We also do not return B_TRUE
// immediately. Only if there are no resources in the same group for which we
// need to wait do we set is_inter_rg_dep to B_TRUE and return B_TRUE (if
// we have found a resource in another group that depends on us).
//
// To put it another way, we make it a higher priority to check for intra-rg
// dependencies, and only if those are all satisfied, do we block for
// inter-rg dependencies.  That way, once the president tells us that inter-rg
// deps are satisifed, we can proceed with the stop, and do not need to check
// local dependencies again.
//
// Also, if running postnet_stop, we have to check
// dependencies for intra-RG deps. r's postnet_stop method should be run only
// after its dependee 'r' finishes its stop method.
//
static boolean_t
stopping_iter_deps_list(rlist_p_t r, deptype_t depsflag,
    rgm::lni_t lni, method_t meth, boolean_t &is_inter_rg_dep,
    boolean_t *restart_triggered)
{
	rlist_p_t rp_dep;
	rdeplist_t *nlp;
	sc_syslog_msg_handle_t handle;
	boolean_t found_inter_rg_deps = B_FALSE;

	is_inter_rg_dep = B_FALSE;

	//
	// Get the proper dependents list for the type of dependency that
	// we are checking.
	//
	switch (depsflag) {
	case DEPS_STRONG:
		nlp = r->rl_dependents.dp_strong;
		break;
	case DEPS_WEAK:
		nlp = r->rl_dependents.dp_weak;
		break;
	case DEPS_RESTART:
		nlp = r->rl_dependents.dp_restart;
		break;
	case DEPS_OFFLINE_RESTART:
		nlp = r->rl_dependents.dp_offline_restart;
		break;

	default:
		// programming error
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A non-fatal internal error has occurred in the rgmd state
		// machine.
		// @user_action
		// Since this problem might indicate an internal logic error
		// in the rgmd, save a copy of the /var/adm/messages files on
		// all nodes, and the output of clresourcetype show -v,
		// clresourcegroup show -v +, and clresourcegroup status +.
		// Report the problem to your authorized Sun service provider.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR: stopping_iter_deps_list: invalid "
		    "dependency type <%d>", depsflag);
		sc_syslog_msg_done(&handle);
		return (B_FALSE);
	}

	//
	// First, iterate through our strong/weak/restart dependents list.
	//
	// Ensure that Rs depending on 'r' have run the appropriate
	// {stop, postnet stop} methods.
	//
	for (; nlp != NULL; nlp = nlp->rl_next) {
#if (SOL_VERSION >= __s10)
		// Check if it is a remote cluster resource.
		if (is_enhanced_naming_format_used(nlp->rl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "stopping_iter_deps_list "
				    "Running at too low a version:R <%s> "
				    "has dependency on R <%s> "
				    "which is in not in local cluster\n"),
				    r->rl_ccrdata->r_name, nlp->rl_name);
				continue;
			}
			found_inter_rg_deps = B_TRUE;
			continue;
		}
#endif
		//
		// Do not skip disabled dependent resources.  It is possible
		// that both 'r' and its dependent are stopping at the same
		// time, and we want to enforce dependencies between them.
		// If the dependent R is already stopped, there is no harm
		// done.
		//
		// To handle resource restarts (and later, forced disables),
		// r_state_at_least() will filter out those dependents that
		// are not stopping.
		//

		// get a pointer to the resource with this name
		rp_dep = rname_to_r(NULL, nlp->rl_name);

		//
		// we treat the inter-rg and intra-rg cases differently.
		// If the resources are in the same resource group, we
		// check the state of the dependent resource, and make sure
		// that we are allowed to stop the dependee.
		//
		// If the resources are in different groups, we just note
		// that we have found an inter-rg dependency.  Later, if
		// we have found that we do not need to pospone stopping
		// this r because of intra-rg deps, we will postpone stopping
		// it because of inter-rg deps.
		//
		if (r->rl_rg == rp_dep->rl_rg) {
			//
			// we are in the same resource group.
			// Check dependencies explicitly.
			//
			ucmm_print("RGM",
			    NOGET("stopping_iter_deps_list: found dependent "
			    "<%s> in my RG. State is <%s>\n"),
			    rp_dep->rl_ccrdata->r_name,
			    pr_rstate(rp_dep->rl_xstate[lni].rx_state));

			// Make sure rp_dep is stopped enough so we can stop
			// 'r'.

			//
			// First, check if the dependee resource is a shared
			// address that is being disabled.
			// If so, treat all strong dependent resources
			// as if they were offline-restart dependents.
			// This will take the strong dependents offline
			// along with the shared address.
			//
			if ((restart_triggered != NULL) &&
			    (r->rl_ccrtype->rt_sysdeftype ==
			    SYST_SHARED_ADDRESS) &&
			    (((rgm_switch_t *)r->rl_ccrdata->r_onoff_switch)->
			    r_switch[lni] == SCHA_SWITCH_DISABLED) &&
			    ((depsflag == DEPS_STRONG) || (depsflag ==
			    DEPS_RESTART))) {
				depsflag = DEPS_OFFLINE_RESTART;
			}

			//
			// Trigger intra-RG offline-restart dependents.
			// However, we do not trigger any restarts if
			// the RG is in PENDING_OFF_START_FAILED,
			// PENDING_OFF_STOP_FAILED, or ERROR_STOP_FAILED state.
			//
			// Do not trigger the restart if the dependent
			// resource is disabled, or is already restarting.
			//
			rgm::rgm_rg_state rgx_state =
			    r->rl_rg->rgl_xstate[lni].rgx_state;
			scha_switch_t rp_dependent_enabled =
			    ((rgm_switch_t *)rp_dep->rl_ccrdata->
			    r_onoff_switch)->r_switch[lni];

			if (restart_triggered != NULL &&
			    rp_dependent_enabled != SCHA_SWITCH_DISABLED &&
			    (meth == METH_STOP || meth == METH_POSTNET_STOP) &&
			    depsflag == DEPS_OFFLINE_RESTART &&
			    rgx_state != rgm::RG_PENDING_OFF_START_FAILED &&
			    rgx_state != rgm::RG_PENDING_OFF_STOP_FAILED &&
			    rgx_state != rgm::RG_ERROR_STOP_FAILED &&
			    (rp_dep->rl_xstate[lni].rx_restart_flg ==
			    rgm::RR_NONE) &&
			    is_r_online(rp_dep->rl_xstate[lni].rx_state)) {
				rp_dep->rl_xstate[lni].rx_restart_flg =
				    rgm::RR_STOPPING;
				*restart_triggered = B_TRUE;
			}

			switch (meth) {
			case METH_STOP:

				//
				// dep_r_start_flag does not have any
				// use when the state is R_STOPPED.So
				// B_FALSE is passed.
				//
				if (r_state_at_least(rp_dep, rgm::R_STOPPED,
				    lni, depsflag, B_FALSE))
					continue;
				break;

			case METH_POSTNET_STOP:
				//
				// dep_r_start_flag does not have any
				// use when the state is R_OFFLINE.So
				// B_FALSE is passed.
				//
				if (r_state_at_least(rp_dep, rgm::R_OFFLINE,
				    lni, depsflag, B_FALSE))
					continue;
				break;

			case METH_START:
			case METH_VALIDATE:
			case METH_UPDATE:
			case METH_INIT:
			case METH_FINI:
			case METH_BOOT:
			case METH_MONITOR_START:
			case METH_MONITOR_STOP:
			case METH_MONITOR_CHECK:
			case METH_PRENET_START:
			default:
				// these cases indicate programming error
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// A non-fatal internal error has occurred in
				// the rgmd state machine.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "INTERNAL ERROR: stopping_iter_deps_list: "
				    "meth type <%d>", meth);
				sc_syslog_msg_done(&handle);
				return (B_FALSE);
			}

			// Found an R that depends on 'r',
			// and that is not yet stopped/offline.
			// We must postpone stopping 'r'.
			ucmm_print("RGM",
			    NOGET("stopping_iter_deps_list: <%s> is dependent "
			    "on <%s>, and they are in the same group."),
			    rp_dep->rl_ccrdata->r_name,
			    r->rl_ccrdata->r_name);
			return (B_TRUE);
		} else {
			//
			// We found an inter-rg dependency that will cause
			// us to block (move to R_STOPPING_DEPEND) if there
			// are no intra-rg dependencies.
			//
			ucmm_print("RGM",
			    NOGET("stopping_iter_deps_list: <%s> is dependent "
			    "on <%s>, and they are in different groups."),
			    rp_dep->rl_ccrdata->r_name,
			    r->rl_ccrdata->r_name);

			found_inter_rg_deps = B_TRUE;
		}
	}

	//
	// If we're just running STOP, we don't need to check
	// any more dependencies.
	//
	if (meth != METH_POSTNET_STOP) {
		goto stopping_iter_deps_list_end; //lint !e801
	}

	//
	// If we are running POSTNET_STOP, we must check the state of resources
	// within our resource group on whom we have dependencies.
	// We must postpone POSTNET_STOP until all depended-on Rs are at least
	// in the STOPPED state.
	//

	//
	// Get the proper dependencies list for the type of dependency that
	// we are checking.
	//
	switch (depsflag) {
	case DEPS_STRONG:
		nlp = r->rl_ccrdata->r_dependencies.dp_strong;
		break;
	case DEPS_WEAK:
		nlp = r->rl_ccrdata->r_dependencies.dp_weak;
		break;
	case DEPS_RESTART:
		nlp = r->rl_ccrdata->r_dependencies.dp_restart;
		break;
	case DEPS_OFFLINE_RESTART:
		nlp = r->rl_ccrdata->r_dependencies.dp_offline_restart;
		break;

	default:
		// programming error
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR: stopping_iter_deps_list: invalid "
		    "dependency type <%d>", depsflag);
		sc_syslog_msg_done(&handle);
		return (B_FALSE);
	}

	for (; nlp != NULL; nlp = nlp->rl_next) {
#if (SOL_VERSION >= __s10)
		// Check if it is a remote cluster resource.
		if (is_enhanced_naming_format_used(nlp->rl_name))
			continue;
#endif
		rp_dep = rname_to_r(NULL, nlp->rl_name);
		CL_PANIC(rp_dep != NULL);

		// skip resources that are not in our resource group.
		if (r->rl_rg != rp_dep->rl_rg) {
			continue;
		}

		//
		// Do *not* skip disabled resources, since they might also
		// be stopping and we need to enforce dependencies on
		// them.  If the disabled resource is already stopped, no
		// harm is done.  r_state_at_least() will filter out resources
		// that are not stopping.
		//

		// r_state_at_least will check for the resource rp
		// having no STOP method registered.

		//
		// dep_r_start_flag does not have any
		// use when the state is R_STOPPED.So
		// B_FALSE is passed.
		//
		if (!r_state_at_least(rp_dep, rgm::R_STOPPED, lni,
		    depsflag, B_FALSE))
			return (B_TRUE);
	}

stopping_iter_deps_list_end:

	//
	// If we reach here, we did not find any intra-RG dependencies that
	// are not fulfilled.  Specify in the return value whether we found
	// inter-RG dependencies.
	//
	if (found_inter_rg_deps) {
		is_inter_rg_dep = B_TRUE;
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}


//
// r_state_at_least
//
// Returns true if 'r' is "at least" in state 'state' when going online or
// offline.  This function contains special-case code to properly handle
// dependency relationships for restarting resources in an
// RG_ON_PENDING_R_RESTART or RG_ON_PENDING_DISABLED or
// RG_ON_PENDING_MON_DISABLED resource group.  See in-line comments below.
//
// Note that the dependency type DEPS_WEAK is special cased in some places.
// However, DEPS_STRONG and DEPS_RESTART are treated identically.
//
// This function is called while starting or stopping some resource 'r1'.
// The argument 'r' passed to this function is a dependent or dependee of
// r1 (as a consequence of whether r1 is starting or stopping, and whether
// we are checking an ordinary dependency or a "reverse" net-relative
// dependency).
// The dep_r_start_flag argument is set to B_TRUE only if r1 is starting
// and is in R_PRENET_STARTED state.  This flag is used to guarantee that
// r1 does not get stuck in that state in the case that r is in
// START_FAILED state.
//
static boolean_t
r_state_at_least(rlist_p_t r, rgm::rgm_r_state state, rgm::lni_t lni,
    deptype_t depsflag, boolean_t dep_r_start_flag)
{
	sc_syslog_msg_handle_t handle;
	rgm::rgm_rg_state rgx_state = r->rl_rg->rgl_xstate[lni].rgx_state;
	boolean_t rg_state_allows_rrestart, rg_is_pending_offline, start_flg;

	//
	// If the RG state is ON_PENDING_R_RESTART or RG_ON_PENDING_DISABLED or
	// RG_ON_PENDING_MON_DISABLED, the RG may contain resources that
	// are restarting or being disabled.  We want to allow a resource to
	// be restarted or disabled even if it has a dependent (the resource
	// 'r') which remains online and is neither being disabled nor
	// restarted.  This applies when 'state' indicates that the resource
	// is stopping.
	//
	// However, in the starting phase (where the dependent resource
	// has gone offline and is about to go back online), we must
	// enforce a strong dependency on resource 'r'.  Therefore, we do
	// not exit early with B_TRUE in that case.
	//

	rg_state_allows_rrestart = (rgx_state == rgm::RG_ON_PENDING_R_RESTART ||
	    rgx_state == rgm::RG_ON_PENDING_DISABLED ||
	    rgx_state == rgm::RG_ON_PENDING_MON_DISABLED) ? B_TRUE : B_FALSE;

	//
	// Starting case and non-weak dependency.
	//
	start_flg = ((state == rgm::R_PRENET_STARTED ||
	    state == rgm::R_JUST_STARTED) &&
	    (depsflag != DEPS_WEAK)) ? B_TRUE : B_FALSE;

	if (!(r->rl_ccrtype->rt_proxy) && rg_state_allows_rrestart &&
	    r->rl_xstate[lni].rx_restart_flg == rgm::RR_NONE && !(start_flg) &&
	    ((rgm_switch_t *)r->rl_ccrdata->r_onoff_switch)->r_switch[lni]
	    != SCHA_SWITCH_DISABLED) {
		return (B_TRUE);
	}

	//
	// If the RG state is not pending offline, but we are going in the
	// OFFLINE direction, then it must be the case that the resource that
	// is being stopped is either disabled or in the stopping phase of
	// restarting.  We will only check dependencies against other resources
	// that are disabled or in the stopping phase of restarting.
	//
	// We capture the RG states of interest in a Boolean here for use
	// in the "going OFFLINE" cases below:
	//
	rg_is_pending_offline = (rgx_state == rgm::RG_PENDING_OFFLINE ||
	    rgx_state == rgm::RG_PENDING_OFF_START_FAILED ||
	    rgx_state == rgm::RG_PENDING_OFF_STOP_FAILED) ? B_TRUE : B_FALSE;

	switch (state) {

	//
	// going ONLINE direction
	//

	case rgm::R_PRENET_STARTED:

		//
		// If r is restarting and is in the 'stopping' phase, return
		// FALSE; we will not return TRUE until after r has stopped
		// and is in the 'starting' phase.
		//
		if (r->rl_xstate[lni].rx_restart_flg == rgm::RR_STOPPING) {
			return (B_FALSE);
		}

		// Skip r if it has no PRENET_START method
		if (!is_method_reg(r->rl_ccrtype, METH_PRENET_START, NULL))
			return (B_TRUE);

		if (depsflag == DEPS_WEAK &&
		    (r->rl_xstate[lni].rx_state == rgm::R_ONLINE_STANDBY))
			return (B_TRUE);

		//
		// If the r is in R_START_FAILED and depsflag is
		// DEPS_WEAK return true.If depsflag is not DEPS_WEAK
		// return true if the related resource (implicit dependee
		// or dependent in case of reverse dependency) is in
		// R_PRENET_STARTED else return false.
		// Note that dep_r_start_flag will be true if the
		// implicit dependee/dependent resource is in
		// R_PRENET_STARTED.
		//
		if (r->rl_xstate[lni].rx_state == rgm::R_START_FAILED) {
			if (depsflag == DEPS_WEAK)
				return (B_TRUE);
			else
				return (dep_r_start_flag);
		}

		if (r->rl_xstate[lni].rx_state == rgm::R_PRENET_STARTED ||
		    r->rl_xstate[lni].rx_state == rgm::R_JUST_STARTED ||
		    r->rl_xstate[lni].rx_state == rgm::R_ONLINE_UNMON ||
		    r->rl_xstate[lni].rx_state == rgm::R_MON_FAILED ||
		    r->rl_xstate[lni].rx_state == rgm::R_STARTING ||
		    r->rl_xstate[lni].rx_state == rgm::R_MON_STARTING ||
		    r->rl_xstate[lni].rx_state == rgm::R_ONLINE)
			return (B_TRUE);

		return (B_FALSE);

	case rgm::R_JUST_STARTED:

		//
		// If r is restarting and is in the 'stopping' phase, return
		// FALSE; we will not return TRUE until after r has stopped
		// and is in the 'starting' phase.
		//
		if (r->rl_xstate[lni].rx_restart_flg == rgm::RR_STOPPING) {
			return (B_FALSE);
		}

		if (depsflag == DEPS_WEAK &&
		    (r->rl_xstate[lni].rx_state == rgm::R_ONLINE_STANDBY))
			return (B_TRUE);

		//
		// If the r is in R_START_FAILED and depsflag is
		// DEPS_WEAK return true.If depsflag is not DEPS_WEAK
		// return true if the related resource (implicit dependee
		// or dependent in case of reverse dependency) is in
		// R_PRENET_STARTED else return false.
		// Note that dep_r_start_flag will be true if the
		// implicit dependee/dependent resource is in
		// R_PRENET_STARTED.
		//
		if (r->rl_xstate[lni].rx_state == rgm::R_START_FAILED) {
			if (depsflag == DEPS_WEAK)
				return (B_TRUE);
			else
				return (dep_r_start_flag);
		}
		if (r->rl_xstate[lni].rx_state == rgm::R_JUST_STARTED ||
		    r->rl_xstate[lni].rx_state == rgm::R_ONLINE_UNMON ||
		    r->rl_xstate[lni].rx_state == rgm::R_MON_FAILED ||
		    r->rl_xstate[lni].rx_state == rgm::R_MON_STARTING ||
		    r->rl_xstate[lni].rx_state == rgm::R_ONLINE)
			return (B_TRUE);

		return (B_FALSE);

	//
	// going OFFLINE direction
	//

	case rgm::R_STOPPED:

		//
		// This is the scenario that we are trying to postpone an r
		// from stopping until its dependents (or for reverse
		// dependency, its dependees) have stopped.  If the
		// dependent/dependee r is already in the 'starting' phase of
		// restarting, then it is too late for it to play a role in
		// postponing the current r from stopping.  In other words,
		// the restarting dependent is as "stopped" as it's ever going
		// to get.  So we must return TRUE for that case.
		//
		// We check that the RG state is ON_PENDING_R_RESTART or
		// ON_PENDING_DISABLED or ON_PENDING_MON_DISABLED, because
		// only in those states can a resource remain in the
		// RR_STARTING phase.  In any other RG state, the RR_STARTING
		// flag will be cleared.  The relevant states were computed
		// above and saved in the Boolean variable
		// rg_state_allows_rrestart.
		//
		if (r->rl_xstate[lni].rx_restart_flg == rgm::RR_STARTING &&
		    rg_state_allows_rrestart) {
			return (B_TRUE);
		}

		//
		// As noted above, if the RG is not pending offline, then the
		// only resources that can be stopping are those which are
		// either disabled or in the stopping phase of restarting.
		// We will only check dependencies between such resources.
		// If a resource does not meet those criteria then we return
		// TRUE to skip the dependency check.
		//
		if (!rg_is_pending_offline &&
		    ((rgm_switch_t *)r->rl_ccrdata->r_onoff_switch)->
		    r_switch[lni] != SCHA_SWITCH_DISABLED &&
		    r->rl_xstate[lni].rx_restart_flg != rgm::RR_STOPPING) {
			return (B_TRUE);
		}

		// Skip r if it has no STOP method
		if (!is_method_reg(r->rl_ccrtype, METH_STOP, NULL))
			return (B_TRUE);

		if (r->rl_xstate[lni].rx_state == rgm::R_STOP_FAILED)
			return (B_TRUE);

		if (r->rl_xstate[lni].rx_state == rgm::R_STOPPED ||
		    r->rl_xstate[lni].rx_state ==
		    rgm::R_POSTNET_STOPPING ||
		    r->rl_xstate[lni].rx_state == rgm::R_OFFLINE)
			return (B_TRUE);

		return (B_FALSE);

	case rgm::R_OFFLINE:

		//
		// Exclude resources that are starting or remaining online
		// in an online resource group; see comments above under the
		// R_STOPPED case:
		//
		if (r->rl_xstate[lni].rx_restart_flg == rgm::RR_STARTING &&
		    rg_state_allows_rrestart) {
			return (B_TRUE);
		}
		if (!rg_is_pending_offline &&
		    ((rgm_switch_t *)r->rl_ccrdata->r_onoff_switch)->
		    r_switch[lni] != SCHA_SWITCH_DISABLED &&
		    r->rl_xstate[lni].rx_restart_flg != rgm::RR_STOPPING) {
			return (B_TRUE);
		}

		if (r->rl_xstate[lni].rx_state == rgm::R_STOP_FAILED)
			return (B_TRUE);

		if (r->rl_xstate[lni].rx_state == rgm::R_OFFLINE)
			return (B_TRUE);

		return (B_FALSE);

	//
	// Programming error if we hit these:
	//

	case rgm::R_MON_FAILED:
	case rgm::R_ONLINE_STANDBY:
	case rgm::R_ONLINE:

	// R_ONLINE_UNMON case no longer used.  R_JUST_STARTED used instead.
	case rgm::R_ONLINE_UNMON:
	case rgm::R_START_FAILED:
	case rgm::R_STOP_FAILED:
	case rgm::R_PENDING_INIT:
	case rgm::R_PENDING_FINI:
	case rgm::R_ON_PENDING_UPDATE:
	case rgm::R_ONUNMON_PENDING_UPDATE:
	case rgm::R_MONFLD_PENDING_UPDATE:
	case rgm::R_UNINITED:
	case rgm::R_PENDING_BOOT:
	case rgm::R_INITING:
	case rgm::R_FINIING:
	case rgm::R_BOOTING:
	case rgm::R_UPDATING:
	case rgm::R_MON_STARTING:
	case rgm::R_MON_STOPPING:
	case rgm::R_POSTNET_STOPPING:
	case rgm::R_PRENET_STARTING:
	case rgm::R_STARTING:
	case rgm::R_STOPPING:
	case rgm::R_STOPPING_DEPEND:
	case rgm::R_STARTING_DEPEND:
	case rgm::R_OK_TO_START:
	case rgm::R_OK_TO_STOP:

	default:
		// these cases indicate programming error
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A non-fatal internal error has occurred in the rgmd state
		// machine.
		// @user_action
		// Since this problem might indicate an internal logic error
		// in the rgmd, save a copy of the /var/adm/messages files on
		// all nodes, and the output of clresourcetype show -v,
		// clresourcegroup show -v +, and clresourcegroup status +.
		// Report the problem to your authorized Sun service provider.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR: r_state_at_least: state <%s> (%d)",
		    pr_rstate(state), state);
		sc_syslog_msg_done(&handle);
		return (B_TRUE);
	}
}


//
// has_net_relative_methods
//
// Returns B_TRUE if the resource r has a Prenet_start or Postnet_stop
// method.  Returns B_FALSE otherwise.
//
boolean_t
has_net_relative_methods(rlist_p_t r)
{
	if (is_method_reg(r->rl_ccrtype, METH_PRENET_START, NULL) ||
	    is_method_reg(r->rl_ccrtype, METH_POSTNET_STOP, NULL)) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}


//
// has_net_relative_dependencies
//
// Returns B_TRUE if the resource r has a Prenet_start or Postnet_stop
// method as well as Start or Stop and has a dependency on another resource
// in the same group which also has a Prenet_start and/or Postnet_stop method.
// The dependency may be implicit or explicit; weak, strong, or restart.
//
// Uses the functions has_net_relative_methods() and depends_on_r() as
// subroutines.
//
static boolean_t
has_net_relative_dependencies(rlist_p_t r)
{
	if (has_net_relative_methods(r) && (is_method_reg(r->rl_ccrtype,
	    METH_START, NULL) ||
		is_method_reg(r->rl_ccrtype, METH_STOP, NULL))) {
		//
		// r has net-relative methods.  See if it has a dependency on
		// another resource in the same RG which also has net-relative
		// methods.
		//
		return (depends_on_r(r));
	} else {
		// r does not have net-relative methods
		return (B_FALSE);
	}
}


//
// is_offline_r_state
//
// Returns B_TRUE if the resource state r_state is R_OFFLINE, or any
// other state equivalent to R_OFFLINE (in the sense that the resource
// has not started).
//
// Otherwise, returns B_FALSE.
//
// This function determines whether we have to clear an RR_STOPPING
// restart_flag on a resource.
//
boolean_t
is_offline_r_state(rgm::rgm_r_state r_state)
{
	//
	// We use a switch statement so that lint will fail in the event
	// that any new R states are introduced.  This forces the implementor
	// to consider whether the restart flag has to be cleared for such
	// new states.
	//
	switch (r_state) {
	case rgm::R_OFFLINE:
	case rgm::R_STARTING_DEPEND:
	case rgm::R_OK_TO_START:
	//
	// Note, the following states "cannot occur" in the context of where
	// this function is called, but there is no harm in checking for them.
	// It would be invalid for a resource to be restarting in
	// any of these states.
	//
	case rgm::R_PENDING_INIT:
	case rgm::R_PENDING_FINI:
	case rgm::R_UNINITED:
	case rgm::R_PENDING_BOOT:
	case rgm::R_INITING:
	case rgm::R_FINIING:
	case rgm::R_BOOTING:
		return (B_TRUE);

	case rgm::R_ONLINE_UNMON:
	case rgm::R_ONLINE_STANDBY:
	case rgm::R_MON_FAILED:
	case rgm::R_ONLINE:
	case rgm::R_ON_PENDING_UPDATE:
	case rgm::R_PENDING_UPDATE:
	case rgm::R_ONUNMON_PENDING_UPDATE:
	case rgm::R_MONFLD_PENDING_UPDATE:
	case rgm::R_UPDATING:
	case rgm::R_MON_STARTING:
	case rgm::R_MON_STOPPING:
	case rgm::R_JUST_STARTED:
	case rgm::R_PRENET_STARTED:
	case rgm::R_STOPPED:
	case rgm::R_START_FAILED:
	case rgm::R_STOP_FAILED:
	case rgm::R_POSTNET_STOPPING:
	case rgm::R_PRENET_STARTING:
	case rgm::R_STARTING:
	case rgm::R_STOPPING:
	case rgm::R_STOPPING_DEPEND:
	case rgm::R_OK_TO_STOP:
		return (B_FALSE);

	default:
		// illegal state!
		CL_PANIC(0);
		break;
	}
	return (B_FALSE);
}
