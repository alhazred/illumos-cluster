/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_block.cc	1.4	08/05/20 SMI"

//
// rgm_block.cc
//
// This file implements the code to block RGM from bringing the resource groups
// online till its dependency on the availability of global file system is met.
// During Dual-partition upgrade, we start off RGM on the upgraded nodes before
// the upgrade process. The upgrade process is run before mounting of global
// file systems. The upgrade process makes sure it is safe to access
// shared resources. Only after this happens, we can safely start all the
// services. RGM is started early as an optimization to reduce the downtime.
// It has an opportunity to do its initialization, president election etc. and
// just before it is ready to startup the services, it blocks the resource
// groups from being online to meet its dependency on global file systems.
// This happens once on a node join.
//

#include <rgm_proto.h>
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <h/quantum_leap.h>

//
// The mutex and cond_t for use by the rgm_block thread.
// Suppress the lint warning about "union initialization."
//
static mutex_t rgm_block_mutex = DEFAULTMUTEX; /*lint !e708 */

//
// Global variable used by this module.
//
static bool rgm_block_done = false;

//
// This function will check if the "RGM object" is registered in
// the local name-server. If it is registered, the funtion will block in
// an invocation on the RGM object. The invocation completes after
// global file systems are available. This will ensure that the RGM
// dependency on the availability of global file systems is met.
//
static void
block_rgm()
{
	Environment			env;
	naming::naming_context_var	ctx_v = ns::local_nameserver();
	CORBA::Exception 		*ex;
	sc_syslog_msg_handle_t		handle;
	quantum_leap::ql_rgm_obj_var	ql_rgm_obj_v;

	CL_PANIC(!CORBA::is_nil(ctx_v));

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");

	ucmm_print("RGM", NOGET("In block_rgm\n"));

	//
	// Look for the RGM object in the name server
	//
	CORBA::Object_var obj =
	    ctx_v->resolve(quantum_leap::ql_rgm_obj::QL_RGM_OBJ_KEY, env);

	if ((ex = env.exception()) != NULL) {
		if (naming::not_found::_exnarrow(ex) != NULL) {
			//
			// The RGM object is not registered
			// Return from the function.
			//
			ucmm_print("RGM",
			    NOGET("RGM object not found\n"));
			env.clear();
			sc_syslog_msg_done(&handle);
			return;
		} else {
			env.clear();

			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("encountered an exception during lookup of"
			    " RGM object"));

			sc_syslog_msg_done(&handle);

			abort_node(B_TRUE, B_TRUE, NULL);
		}
	}

	//
	// Narrow the object to a quantum leap object.
	//
	ql_rgm_obj_v = quantum_leap::ql_rgm_obj::_narrow(obj);

	CL_PANIC(!CORBA::is_nil(ql_rgm_obj_v));

	ucmm_print("RGM", NOGET("Blocking in RGM\n"));

	//
	// SCMSGS
	// @explanation
	// The RGM is blocked until all cluster file systems are available.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE, SYSTEXT(
	    "Blocking in RGM"));

	ql_rgm_obj_v->block_rgm(env);

	if ((ex = env.exception()) != NULL) {

		//
		// SCMSGS
		// @explanation
		// The RGM returned from the blocking invocation with an
		// exception. The process that hosts the server object might
		// have terminated. As a result of this, RGM might try to
		// bring the resources online that need global file systems
		// data. The data might not be available yet, which might
		// result in failure to start the resource groups.
		// @user_action
		// Save a copy of /var/adm/messages,
		// /var/cluster/logs/ql_server_debug.log and
		// /var/cluster/logs/install/ql_upgrade_debug.log on all the
		// nodes. Report the problem to your authorized Sun service
		// provider.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "RGM unblocked, encountered an exception"));

		env.clear();

		sc_syslog_msg_done(&handle);
		abort_node(B_TRUE, B_TRUE, NULL);
	}

	//
	// SCMSGS
	// @explanation
	// The RGM has been unblocked. cluster file systems are available.
	// @user_action
	// This is an informational message, no user action is needed.
	//
	(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE, SYSTEXT(
	    "RGM unblocked, returned from blocked invocation."));

	ucmm_print("RGM", NOGET("RGM unblocked, returned from blocked"
	    " invocation."));

	sc_syslog_msg_done(&handle);
}

//
// The RGM block thread will wait on a condition variable till it is
// woken up when RGM state machine is run the first time. It will
// then block on an invocation to the "RGM object". It returns from
// this blocked invocation after the global file systems are available.
// This thread goes away after doing this.
//
void *
rgm_block_thread_start(void *arg)
{
	int	retval;

	ucmm_print("RGM", NOGET("Starting RGM block thread\n"));

	block_rgm();

	retval = mutex_lock(&rgm_block_mutex);
	CL_PANIC(retval == 0);

	rgm_block_done = true;

	retval = mutex_unlock(&rgm_block_mutex);
	CL_PANIC(retval == 0);

	//
	// Initiate the state machine activity since tasks may have
	// piled up.
	//
	run_state_machine((rgm::lni_t)0, "RGM_BLOCK_THREAD");

	return (arg);
}

//
// Returns true is RGM has not yet blocked. This means we are
// in the RGM state machine for the first time.
//
bool
is_rgm_block_complete()
{
	int	err;
	bool	retval;

	err = mutex_lock(&rgm_block_mutex);
	CL_PANIC(err == 0);

	retval =  rgm_block_done;

	err = mutex_unlock(&rgm_block_mutex);
	CL_PANIC(err == 0);

	ucmm_print("RGM", NOGET("is_rgm_block_complete(): %s\n"),
	    retval ? "true" : "false");

	return (retval);
}
