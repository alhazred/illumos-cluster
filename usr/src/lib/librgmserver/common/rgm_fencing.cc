/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_fencing.cc	1.10	08/05/20 SMI"

//
// rgm_fencing.cc
//
// This file contains the code for handling disk fencing at the rgm level.
// Following a node death, the rgm must not start bringing an RG online on a
// node until fencing has completed on that node (to fence out the old node).
// The rgm knows if fencing has completed or not by calling the utility
// function check_for_fencing in libdcs. Note that only RGs that have
// global_resources_used set to ALL need to honor the fencing wait time.
// If RGs don't use global resources, they can start immediately.
//
// The algorithm works as follows: the state machine checks if fencing is
// complete (via the helper function is_fencing_complete()).
// If fencing is not complete, and the RG depends on global_resources,
// it is not brought online yet.
//
// In order to know when to bring the RG online (that was ignored by the
// state machine because fencing was still in progress) we need another
// thread that waits for fencing to complete and immediately sets a
// fencing_complete variable and runs the state machine again. At that point
// the state machine will find that fencing is complete, and will be able to
// bring the RG in question online.
//
// There are no callbacks to declare that fencing is complete, so the
// fencing thread must wait in a synchronous call to check_for_fencing.
// The fencing thread is started at rgm startup, but spends most of its time
// waiting on a condition var. Only at rgm reconfiguration is the thread
// told to wake up (by rgm_reconfig) and call check_for_fencing.
//
// Another complication is that fencing may never complete (due to errors).
// In that case we don't want to wait forever to bring an RG online on this
// node. We use the default timeout in check_for_fencing(), which waits for
// the CMM timeout. If the timeout expires, then we can go ahead and bring the
// RGs online anyway. The rationale is that the node that is being fenced
// will have killed itself by the time the CMM timeout expires, so it doesn't
// need to be fenced any longer. This situation is communicated between
// threads in the rgmd through the fencing_complete variable and
// is_fencing_complete() function.
//

#include <dc/libdcs/libdcs.h>
#include <thread.h>
#include <synch.h>
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#include <rgm_proto.h>

/*
 * The mutex and cond_t for use by the fencing_thread.
 *
 * Suppress the lint warning about "union initialization."
 */
static mutex_t fencing_mutex = DEFAULTMUTEX; /*lint !e708 */
static cond_t fencing_cond_var = DEFAULTCV;

/*
 * Global variables used by this module. We start out not doing
 * fencing, so need_check_fencing is false and fencing_complete is true.
 */
static bool need_check_fencing = false;
static bool fencing_complete = true;

static const int FENCING_RETRY = 1;

static bool all_rs_offline(rglist_p_t rglp, sol::nodeid_t mynodeid);
static int check_for_fencing_wrapper(int timeout);

/*
 * The fencing thread just waits on a condition variable until it finds
 * that the need_check_fencing flag is true.
 *
 * Then it calls check_for_fencing_wrapper(), after which it queues a state
 * machine task.
 *
 * We need to release the lock while in the check_for_fencing call so that
 * threads trying to tell us to check for fencing by setting
 * need_check_fencing don't block.
 *
 * Note on error handling: the synchronization objects should only return
 * errors if the rgmd is corrupted in some way. Thus, we just CL_PANIC
 * if they do.
 */
void *
fencing_thread_start(void *arg)
{
	int ret;

	ucmm_print("RGM", NOGET("Starting fencing thread\n"));

	while (true) {
		ret = mutex_lock(&fencing_mutex);
		CL_PANIC(ret == 0);

		//
		// Wait until need_check_fencing is true.
		//
		while (!need_check_fencing) {
			ret = cond_wait(&fencing_cond_var, &fencing_mutex);
			CL_PANIC(ret == 0);
		}

		// reset, so we don't check more than we have to
		need_check_fencing = false;

		//
		// Unlock the mutex so we don't hold it through the
		// call. Note that the need_check_fencing flag could
		// get reset while we are in check_for_fencing (if two
		// reconfigs happen in a row, for example). That's fine.
		// That means we'll immediately call it again after we
		// return. Of course, that call will probably return
		// immediately, because the first call should have handled it.
		// However, there's a chance that the flag will get reset
		// after we return from check_for_fencing(), but before
		// we grab the lock, in which case the subsequent call to
		// check_for_fencing() will not return immediately.
		//
		ret = mutex_unlock(&fencing_mutex);
		CL_PANIC(ret == 0);

		ucmm_print("RGM", NOGET("Fencing thread: about to check for "
		    "fencing"));

		ret = check_for_fencing_wrapper(-2);

		//
		// check_for_fencing_wrapper checks for negative return values.
		// The only other possibilities are success or timeout, which
		// we treat identically. The default timeout is the same as the
		// CMM timeout. The node in question would have killed itself
		// within the CMM timeout, so there is no longer any need to
		// fence it. Set the fencing_complete variable back to true.
		// Grab the lock to prevent another thread from looking at
		// the variable while we're trying to change it.
		//

		ret = mutex_lock(&fencing_mutex);
		CL_PANIC(ret == 0);

		//
		// Only reset fencing_complete if we don't need to check
		// fencing again (if there was another reconfiguration)
		//
		if (!need_check_fencing) {
			ucmm_print("RGM", NOGET("Fencing thread: "
			    "fencing complete"));
			fencing_complete = true;
			//
			// Now that fencing is complete, queue
			// a state machine task to bring online any RGs
			// that were waiting for fencing to complete.
			//
			// lni == 0 means run state machine on all zones
			// on the local node.
			//
			run_state_machine((rgm::lni_t)0, "FENCING_THREAD");

		} else {
			//
			// Another reconfiguration happened, so we
			// need to check fencing again. Leave
			// fencing_complete false and don't bother to
			// run the state machine.
			//
			ucmm_print("RGM", NOGET("Fencing thread: "
			    "fencing complete, but we need to check again"));
		}

		ret = mutex_unlock(&fencing_mutex);
		CL_PANIC(ret == 0);


	}

	/* NOTREACHED */
	return (arg);
}

//
// trigger_wait_for_fencing
// ------------------------
// Just sets need_check_fencing to true and signals the fencing_cond_var
// condition variable.
//
void
trigger_wait_for_fencing()
{
	int ret;

	ucmm_print("RGM", NOGET("trigger_wait_for_fencing()\n"));

	ret = mutex_lock(&fencing_mutex);
	CL_PANIC(ret == 0);

	need_check_fencing = true;

	// Set fencing_complete to false until we finish checking
	fencing_complete = false;

	ret = cond_signal(&fencing_cond_var);
	CL_PANIC(ret == 0);
	ret = mutex_unlock(&fencing_mutex);
	CL_PANIC(ret == 0);
}


//
// check_for_fencing_wrapper
// --------------------------
// Wrapper for check_for_fencing that aborts the node if it returns
// an error.
//
static int
check_for_fencing_wrapper(int timeout)
{
	sc_syslog_msg_handle_t handle;
	int ret;

	ucmm_print("RGM", NOGET("check_for_fencing_wrapper: about to call "
	    "check_for_fencing with timeout %d\n"), timeout);
	ret = check_for_fencing(COARSE_GRAIN_FENCING_LOCK, timeout,
	    FENCING_RETRY);
	if (ret < 0) {
		//
		// internal error: syslog a message and abort the node
		//
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The RGM was unable to detect whether or not a node that
		// left the cluster was properly fenced. This is an
		// unrecoverable error so the RGM will abort the node.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: unable to check for fencing completion."));
		sc_syslog_msg_done(&handle);
		abort_node(B_TRUE, B_FALSE, NULL);
	}
	ucmm_print("RGM", NOGET("check_for_fencing_wrapper: ret=%d\n"), ret);
	return (ret);
}

//
// need_wait_fencing
// ------------------
// Returns true if the rg in question has global_resources_used set to the
// ALL value and none of the resources have started yet. If any of the
// resources have already started, this RG started going online before
// fencing occurred, and so we shouldn't stop it now.
//
bool
need_wait_fencing(rglist_p_t rglp, rgm::lni_t mynodeid)
{
	bool ret;

	//
	// If the RG uses global resources and none of its resources
	// have yet started, we need to wait for fencing.
	//
	ret = rglp->rgl_ccr->rg_glb_rsrcused.is_ALL_value &&
	    all_rs_offline(rglp, mynodeid);

	ucmm_print("RGM", NOGET("need_wait_fencing() for RG %s: %s\n"),
	    rglp->rgl_ccr->rg_name, ret ? "true" : "false");

	return (ret);
}

//
// all_rs_offline
// ---------------
// Assumes the RG is PENDING_ONLINE.
//
// While checking for offline we can ignore any offline-ish states that
// aren't actually R_OFFLINE. In all other offline-ish states, we wouldn't
// have an RG in PENDING_ONLINE.
//
static bool
all_rs_offline(rglist_p_t rglp, sol::nodeid_t mynodeid)
{
	rlist_p_t res;

	for (res = rglp->rgl_resources; res != NULL; res = res->rl_next) {
		if (res->rl_xstate[mynodeid].rx_state != rgm::R_OFFLINE) {
			ucmm_print("RGM", NOGET("all_rs_offline() for RG %s: "
			    "found r %s not offline\n"),
			    rglp->rgl_ccr->rg_name, res->rl_ccrdata->r_name);
			return (false);
		}
	}
	return (true);
}

//
// Returns true or false depending on the value of the fencing_complete
// variable.
//
bool
is_fencing_complete()
{
	int err;
	bool ret;

	err = mutex_lock(&fencing_mutex);
	CL_PANIC(err == 0);

	ret = fencing_complete;

	err = mutex_unlock(&fencing_mutex);
	CL_PANIC(err == 0);

	ucmm_print("RGM", NOGET("is_fencing_complete(): %s\n"),
	    ret ? "true" : "false");

	return (ret);
}
