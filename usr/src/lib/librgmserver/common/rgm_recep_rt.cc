/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_recep_rt.cc	1.35	08/05/20 SMI"

/*
 * rgm_recep_rt.cc is the receptionist component of rgmd which
 *	handles the requests from LIBSCHA API functions
 */

#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <errno.h>
#include <rpc/rpc.h>
#include <netdb.h>

#include <rgm/rgm_common.h>
#ifdef EUROPA_FARM
#include <cmm/ucmm_api.h>
#endif
#include <rgm/rgm_cnfg.h>
#include <rgm_state.h>
#include <rgm_proto.h>
#include <rgm_api.h>

#include <rgm/rgm_recep.h>

/*
 * open_rt()
 *
 *      get the RT sequence id from RT state structure in memory
 *	If the RT entry is not found in memory, rtname_to_rt will
 *	read it from CCR
 */
void
open_rt(rt_open_args *args, open_result_t *result)
{

	rgm_rt_t	*rt = NULL;

	result->ret_code = SCHA_ERR_NOERR;

	rgm_lock_state();

	//
	// If this node has just joined the cluster, there is a window of time
	// in which the president has not yet ordered this node to read the
	// CCR.  If this is the case, sleep until the CCR has been read.
	//
	wait_until_ccr_is_read();

	if ((rt = rtname_to_rt(args->rt_name)) == NULL) {
		ucmm_print("RGM", NOGET("open_rt: RT doesn't exist.\n"));
		result->ret_code = SCHA_ERR_RT;
		goto finished; //lint !e801
	}

	/*
	 *  get seq number from in-memory RT
	 */
	result->seq_id = strdup_nocheck(rt->rt_stamp);
	if (result->seq_id == NULL) {
		result->ret_code = SCHA_ERR_NOMEM;
		ucmm_print("RGM", NOGET("open_rt: failed on strdup\n"));
	}
finished:

	rgm_unlock_state();
}

/*
 * get_rt()
 *      get the RT sequence id and value of the given RT from
 *      in-memory state structure
 */
void
get_rt(rt_get_args *args, get_result_t *result)
{

	rgm_rt_t	*rt = NULL;
	std::map<rgm::lni_t, char *>	val_str;
	namelist_t	*val_array = NULL;
	namelist_t	*current;
	strarr_list	*ret_list;
	scha_prop_type_t	type;
	boolean_t	need_free = B_FALSE;

	result->ret_code = SCHA_ERR_NOERR;
	result->value_list = NULL;
	char *tmp_val = NULL;

	rgm_lock_state();

	if ((rt = rtname_to_rt(args->rt_name)) == NULL) {
		ucmm_print("RGM", NOGET(
		    "get_rt: RT <%s> does not exist.\n"), args->rt_name);
		result->ret_code = SCHA_ERR_RT;
		goto finished; //lint !e801
	}

	/*
	 * retrieve property value from in-memory RT
	 * If the given property does not specified in RT RTR file,
	 * will get error SCHA_ERR_PROP
	 */
	result->ret_code = get_rt_prop_val(args->rt_prop_name, rt,
	    &type, &tmp_val, &val_array, &need_free).err_code;
	val_str[0] = tmp_val;

	if (result->ret_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM", NOGET("get_rt: failed with err <%d>\n"),
		    result->ret_code);
		goto finished; //lint !e801
	}

	/*
	 *  get sequence id
	 */
	result->seq_id = strdup_nocheck(rt->rt_stamp);
	if (result->seq_id == NULL) {
		result->ret_code = SCHA_ERR_NOMEM;
		ucmm_print("RGM", NOGET("get_rt: failed on strdup\n"));
		goto finished; //lint !e801
	}

	/*
	 * store the value of the given property to the returned RPC buffer
	 */
	if ((result->ret_code = store_prop_val(type,
	    val_array, val_str, B_FALSE, NULL, &ret_list, NULL, NULL).err_code)
	    != SCHA_ERR_NOERR) {
		if (result->seq_id != NULL)
			free(result->seq_id);
		goto finished; //lint !e801
	}

	result->value_list = ret_list;

finished:
	/*
	 * If need_free is TRUE, need to free the buffer 'val_str' or
	 * 'val_array'
	 */
	if (need_free && val_str[0] != NULL) {
		free(val_str[0]);
	}
	if (need_free && val_array != NULL) {
		/*
		 * free the temporary list created for the RESOURCE_LIST
		 * don't call rgm_free_nlist() as we only want to free the
		 * namelist_t structure, not the content it points to
		 */
		current = val_array;
		while (current != NULL) {
			val_array = current;
			current = current->nl_next;
			free(val_array);
		}
	}

	rgm_unlock_state();
}

/*
 * get_ispernode_prop()
 *	Get the per-node flag of the given property.
 */
void
get_ispernode_prop(rt_get_args *args, get_result_t *result)
{
	strarr_list	*ret_list = NULL;
	rgm_rt_t *rt = NULL;
	rgm_param_t	*param = NULL;

	result->ret_code = SCHA_ERR_NOERR;
	result->value_list = NULL;

	rgm_lock_state();

	if ((rt = rtname_to_rt(args->rt_name)) == NULL) {
		result->ret_code = SCHA_ERR_RT;
		ucmm_print("get_ispernode_prop",
		    NOGET("couldn't get RT <%s>\n"), args->rt_name);
		goto finished; //lint !e801
	}

	if ((param = (rgm_param_t *)get_param_prop(args->rt_prop_name,
	    B_TRUE, rt->rt_paramtable)) == NULL) {
		result->ret_code = SCHA_ERR_PROP;
		ucmm_print("get_ispernode_prop",
		    NOGET("couldn't get param table for prop = <%s> in RT"
		    " <%s>\n"), args->rt_prop_name, args->rt_name);
		goto finished; //lint !e801
	}


	if ((ret_list = (strarr_list *) calloc_nocheck(1,
	    sizeof (strarr_list))) == NULL) {
		ucmm_print("get_ispernode_prop", NOGET("couldn't calloc:"
		    " returning\n"));
		result->ret_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	result->seq_id = strdup_nocheck(rt->rt_stamp);
	if (result->seq_id == NULL) {
		result->ret_code = SCHA_ERR_NOMEM;
		if (ret_list != NULL)
			free(ret_list);
		ucmm_print("get_ispernode_prop",
		    NOGET("get_ispernode_prop: failed on strdup\n"));
		goto finished; //lint !e801
	}

	if ((ret_list->strarr_list_val =
	    (char **)calloc_nocheck(1, sizeof (char *))) == NULL) {
		ucmm_print("get_ispernode_prop", NOGET("couldn't calloc:"
		    " returning\n"));
		if (ret_list != NULL)
			free(ret_list);
		if (result->seq_id)
			free(result->seq_id);
		result->ret_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	if ((ret_list->strarr_list_val[0] =
	    rgm_itoa(param->p_per_node)) == NULL) {
		ucmm_print("get_rs_ispernode", NOGET("Could'nt allocate memory"
		    " returning\n"));
		if (ret_list != NULL)
			free(ret_list);
		if (result->seq_id)
			free(result->seq_id);
		result->ret_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}
	ret_list->strarr_list_len = 1;
	result->value_list = ret_list;

finished:
	rgm_unlock_state();
}
