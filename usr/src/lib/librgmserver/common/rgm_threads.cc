//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_threads.cc	1.35	09/01/28 SMI"

#include <unistd.h>
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm_proto.h>
#include <rgm_threads.h>
#ifndef EUROPA_FARM
#include <rgm_global_res_used.h>
#endif
//
// This method is used to run the RGM state machine.
// If lni is 0, run the state machine on all zones on the local node.
// Otherwise, run it on the specified lni.
//
void
state_machine_task::execute()
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;

	ucmm_print("RGM",
	    NOGET("state_machine_task::execute(): starting\n"));

	//
	// Make sure we have the latest status info before running
	// the state machine.  This function call is only necessary on
	// the president, but does no harm on the slave.
	//
	flush_status_change_queue();

	if (lni == 0) {
		// run state machine on global zone and all local zones
		rgm::lni_t myLni = get_local_nodeid();

		rgm_lock_state();
		res = rgm_run_state(myLni);
		rgm_unlock_state();

		if (res.err_code != SCHA_ERR_NOERR) {
			goto bailout;
		}

		LogicalNode *lnNode = lnManager->findObject(myLni);
		CL_PANIC(lnNode != NULL);
		std::list<LogicalNode*>::const_iterator it;
		for (it = lnNode->getLocalZones().begin(); it !=
		    lnNode->getLocalZones().end(); ++it) {
			rgm_lock_state();
			res = rgm_run_state((*it)->getLni());
			rgm_unlock_state();
			if (res.err_code != SCHA_ERR_NOERR) {
				goto bailout;
			}
		}
	} else {
		// run state machine on specific lni
		rgm_lock_state();
		res = rgm_run_state(lni);
		rgm_unlock_state();
	}

bailout:
	if (res.err_code != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("RGM state machine returned error %d"),
		    res.err_code);
		sc_syslog_msg_done(&handle);
	}

	ucmm_print("RGM",
	    NOGET("state_machine_task::execute(): done processing\n"));

	delete this;
}

state_machine_threadpool state_machine_threadpool::the_state_machine_threadpool;

//
// Construct the threadpool with no threads.
//
state_machine_threadpool::state_machine_threadpool() :
	threadpool(false, 0, "rgm state machine threadpool")
{
}

//
// static method called to initialize the threadpool and set the number of
// threads.
//
// static
void
state_machine_threadpool::initialize()
{
	int	threads_desired,
		threads_created;
	sc_syslog_msg_handle_t handle;

	//
	// Only one thread is needed to run the state machine.
	//
	threads_desired = 1;

	//
	// Create the worker threads for this thread pool.
	//
	threads_created = state_machine_threadpool::the().
	    change_num_threads(threads_desired);

	if (threads_created < threads_desired) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to create a thread upon starting
		// up. This is a fatal error. The rgmd will produce a core
		// file and will force the node to halt or reboot to avoid the
		// possibility of data corruption.
		// @user_action
		// Make sure that the hardware configuration meets documented
		// minimum requirements. Save a copy of the /var/adm/messages
		// files on all nodes, and of the rgmd core file. Contact your
		// authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "fatal: cannot create state machine thread"));
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}
}

#ifndef EUROPA_FARM
//
// This method is used by a slave to inform the President of state changes
// on the slave. This method is run only on the President.
//
// wake_president() on the slave calls the president's
// rgm_comm_impl::notify_me_state_change() function which queues up this task
// with change_tag set to NOTIFY_WAKE.  This tells the president to fetch all
// R/RG state from the slave (if we're still running at an old, pre inter-rg
// version).
//
// Note:
// 1. we do NOT already hold the lock when we enter this function
// 2. We call cond_broadcast after getting state
//
// notify_pres_state_change() on the slave calls the President's
// rgm_comm_impl::notify_me_state_change() function which queues up this task
// with change_tag set to NOTIFY_RES or NOTIFY_RG.  This tells the president
// to update the specific R or RG state.
//
void
notify_state_change_task::execute()
{
	rlist_p_t	rp;
	rglist_p_t	rgp;
	LogicalNodeset	emptyNodeset;

	ucmm_print("RGM", NOGET("notify_state_change_task::execute:  node <%d>"
	    "  change_tag <%s>  R_RG_name <%s>  rstate <%s>  rgstate <%s> \n"),
	    node, pr_change_tag(change_tag), R_RG_name ? R_RG_name : "(none)",
	    pr_rstate(rstate), pr_rgstate(rgstate));

	// Claim the rgm state lock, we need it for set_rx_state, set_rgx_state,
	// rname_to_r, and rgname_to_rg
	rgm_lock_state();

	switch (change_tag) {
		case rgm::NOTIFY_WAKE:
			//
			// This is the "wake_president" case.
			// We just call cond_broadcast.
			//
			if (state == NULL) {
				(void) cond_slavewait.broadcast();
				break;
			}

			//
			// We now have the state (pre-fetched or just
			// retrieved), so go ahead and set it in our internal
			// state structure.
			//
			rgm_update_pres_state(node, state);

			//
			// Must call rebalance on all rgs that need it.
			//
			delayed_rebalance(emptyNodeset, B_FALSE);

			//
			// The state info from the slave will be freed in
			// the destructor.
			//

			//
			// Before releasing the mutex, broadcast the condition
			// variable that is shared by all president threads
			// that are waiting for slave nodes to complete some
			// action.
			//
			cond_slavewait.broadcast();

			break;

		case rgm::NOTIFY_RES:
			// Call set_rx_state for R state change
			rp = rname_to_r(NULL, R_RG_name);
			if (rp == NULL) {
				// Resource not found, we assume it has
				// been deleted so we just exit.
				ucmm_print("RGM", NOGET("in notify_state_"
				    "change_task::execute: Resource <%s> "
				    "not found"), R_RG_name);
				goto cleanup;
			}
			set_rx_state(node, rp, rstate);
			break;

		case rgm::NOTIFY_RG:
			// Call set_rgx_state for RG state change
			rgp = rgname_to_rg(R_RG_name);
			if (rgp == NULL) {
				// RG not found, we assume it has been
				// deleted so we just exit.
				ucmm_print("RGM", NOGET("in notify_state_"
				    "change_task::execute: RG <%s> not found"),
				    R_RG_name);
				goto cleanup;
			}
			set_rgx_state(node, rgp, rgstate);
			break;
		default:
			ucmm_print("RGM", NOGET("notify_state_change_task"
			    "::execute: invalid change_tag value <%d> node"
			    " <%d> R_RG_name <%s>  rstate <%s>  rgstate"
			    " <%s> \n"),
			    (int)change_tag, node,
			    R_RG_name ? R_RG_name : "(none)",
			    pr_rstate(rstate), pr_rgstate(rgstate));
			goto cleanup;
	}
	ucmm_print("RGM",
	    NOGET("notify_state_change_task::execute(): node <%d> done\n"),
	    node);

cleanup:
	// Release rgm state lock
	rgm_unlock_state();

	// Ww are responsible for deleting the task object
	delete this;
}

notify_state_change_threadpool
    notify_state_change_threadpool::the_notify_state_change_threadpool;

//
// Construct the threadpool with no threads.
//
notify_state_change_threadpool::notify_state_change_threadpool() :
	threadpool(false, 0, "rgm notify state change threadpool")
{
}

//
// static method called to initialize the threadpool and set the number of
// threads.
//
// static
void
notify_state_change_threadpool::initialize()
{
	int	threads_desired,
		threads_created;
	sc_syslog_msg_handle_t handle;

	//
	// Only one thread is used since this thread must hold the state
	// mutex and we must serialize all updates.
	//
	threads_desired = 1;

	//
	// Create the worker threads for this thread pool.
	//
	threads_created = notify_state_change_threadpool::the().
	    change_num_threads(threads_desired);

	if (threads_created < threads_desired) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to create a thread upon starting
		// up. This is a fatal error. The rgmd will produce a core
		// file and will force the node to halt or reboot to avoid the
		// possibility of data corruption.
		// @user_action
		// Make sure that the hardware configuration meets documented
		// minimum requirements. Save a copy of the /var/adm/messages
		// files on all nodes, and of the rgmd core file. Contact your
		// authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "fatal: cannot create thread to notify President of state"
		    " changes"));
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}
}

//
// This method is used by a slave to inform the President of status changes
// on the slave. This method is run only on the President.
//
void
notify_status_change_task::execute()
{
	ucmm_print("RGM", NOGET("notify_status_change_task::execute:  "
	    "node <%d>  R_name <%s>  RG_name <%s> status_msg <%s> "
	    "status <%s>\n"),
	    node, R_name, RG_name, status_msg ? status_msg : "null",
	    pr_rfmstatus(status));

	//
	// This helper function grabs the lock. Ignore the return code.
	//
	(void) setfmstatus_helper(node, R_name, RG_name, status, status_msg);

	ucmm_print("RGM",
	    NOGET("notify_status_change_task::execute(): node <%d> done\n"),
	    node);

	// We are responsible for deleting the task object
	delete this;
}

notify_status_change_threadpool
    notify_status_change_threadpool::the_notify_status_change_threadpool;

//
// Construct the threadpool with no threads.
//
notify_status_change_threadpool::notify_status_change_threadpool() :
	threadpool(false, 0, "rgm notify status change threadpool")
{
}

//
// static method called to initialize the threadpool and set the number of
// threads.
//
// static
void
notify_status_change_threadpool::initialize()
{
	int	threads_desired,
		threads_created;
	sc_syslog_msg_handle_t handle;

	//
	// Only one thread is used since this thread must hold the state
	// mutex and we must serialize all updates.
	//
	threads_desired = 1;

	//
	// Create the worker threads for this thread pool.
	//
	threads_created = notify_status_change_threadpool::the().
	    change_num_threads(threads_desired);

	if (threads_created < threads_desired) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to create a thread upon starting
		// up. This is a fatal error. The rgmd will produce a core
		// file and will force the node to halt or reboot to avoid the
		// possibility of data corruption.
		// @user_action
		// Make sure that the hardware configuration meets documented
		// minimum requirements. Save a copy of the /var/adm/messages
		// files on all nodes, and of the rgmd core file. Contact your
		// authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "fatal: cannot create thread to notify President of status"
		    " changes"));
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}
}

//
// Constructor for failback_task. Should be called with the rgm
// state lock held so that the RG pointer is valid for the duration
// of this function.
//
failback_task::failback_task(rgm::lni_t _lni, rglist_p_t rgp) : lni(_lni)
{
	rgname = strdup(rgp->rgl_ccr->rg_name);
}

//
// Destructor for failback_task.  Free the memory allocated by
// the constructor.
//
failback_task::~failback_task()
{
	free(rgname);
}

//
// This method is used to handle failback processing for an RG when a
// node boots and joins the cluster. This method is run only on the
// President.
//
void
failback_task::execute()
{
	failback(lni, rgname);

	delete this;
}

failback_threadpool failback_threadpool::the_failback_threadpool;

//
// Construct the threadpool with no threads.
//
failback_threadpool::failback_threadpool() :
	threadpool(false, 0, "rgm failback threadpool")
{
}

//
// static method called to initialize the threadpool and set the number of
// threads.
//
// static
void
failback_threadpool::initialize(int threads_desired)
{
	int	threads_created;
	sc_syslog_msg_handle_t handle;

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");

	//
	// Create the worker threads for this thread pool.
	//
	threads_created = failback_threadpool::the().
	    change_num_threads(threads_desired);

	if (threads_created == 0) {
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to create a sufficient number of
		// threads upon starting up. This is a fatal error. The rgmd
		// will produce a core file and will force the node to halt or
		// reboot to avoid the possibility of data corruption.
		// @user_action
		// Make sure that the hardware configuration meets documented
		// minimum requirements. Save a copy of the /var/adm/messages
		// files on all nodes, and of the rgmd core file. Contact your
		// authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "fatal: cannot create any threads to handle switchback"));
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	} else if (threads_created != threads_desired) {
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to create the desired number of
		// threads upon starting up. This is not a fatal error, but it
		// might cause RGM reconfigurations to take longer because it
		// will limit the number of tasks that the rgmd can perform
		// concurrently.
		// @user_action
		// Make sure that the hardware configuration meets documented
		// minimum requirements. Examine other syslog messages on the
		// same node to see if the cause of the problem can be
		// determined.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "created %d threads to handle resource group switchback; "
		    "desired number = %d"), threads_created, threads_desired);
	} else
		ucmm_print("RGM", NOGET(
		    "created %d threads to handle RG switch back\n"),
		    threads_created);

	sc_syslog_msg_done(&handle);
}
#endif // !EUROPA_FARM


//
// This method is used to launch callback methods of resources. It can
// run on all nodes in the cluster.
//
void
launch_method_task::execute()
{
	ucmm_print("RGM", NOGET("launch_method_task::execute(): starting\n"));

	launch_method(launch_args, lni);

	ucmm_print("RGM", NOGET("launch_method_task::execute(): "
	    "done processing\n"));

	// launch_method() deletes launch_args when done.
	delete this;
}

launch_method_threadpool launch_method_threadpool::the_launch_method_threadpool;

//
// Construct the threadpool with no threads.
//
launch_method_threadpool::launch_method_threadpool() :
	threadpool(false, 0, "rgm launch method threadpool")
{
}

//
// static method called to initialize the threadpool and set the number of
// threads.
//
// static
void
launch_method_threadpool::initialize(int threads_desired)
{
	int	threads_created;
	sc_syslog_msg_handle_t handle;

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");

	//
	// Create the worker threads for this thread pool.
	//
	threads_created = launch_method_threadpool::the().
	    change_num_threads(threads_desired);

	if (threads_created == 0) {
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to create a thread upon starting
		// up. This is a fatal error. The rgmd will produce a core
		// file and will force the node to halt or reboot to avoid the
		// possibility of data corruption.
		// @user_action
		// Make sure that the hardware configuration meets documented
		// minimum requirements. Save a copy of the /var/adm/messages
		// files on all nodes, and of the rgmd core file. Contact your
		// authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "fatal: cannot create any threads to launch callback "
		    "methods"));
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	} else if (threads_created != threads_desired) {
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to create the desired number of
		// threads upon starting up. This is not a fatal error, but it
		// might cause RGM reconfigurations to take longer because it
		// will limit the number of tasks that the rgmd can perform
		// concurrently.
		// @user_action
		// Make sure that the hardware configuration meets documented
		// minimum requirements. Examine other syslog messages on the
		// same node to see if the cause of the problem can be
		// determined.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "created %d threads to launch resource "
		    "callback methods; desired number = %d"),
		    threads_created, threads_desired);
	} else
		ucmm_print("RGM", NOGET(
		    "created %d threads to launch resource "
		    "callback methods\n"), threads_created);

	sc_syslog_msg_done(&handle);
}

//
// This routine creates thread pools used by the RGM for various purposes
// and reserves a large chunk of swap space for the rgmd process so that
// dynamic memory allocations do not fail.
//
// Where appropriate, use the system or RGM configuration to decide
// the number of threads to be created or the amount of swap space to
// preallocate.
//
void
rgm_manage_threads_and_swap(void)
{
	int		number_of_rs;
	rglist_p_t	rgp;
	int		threads_desired;
	int		max_launch_method_threads;
	long long	memory_in_bytes;
	int		memory_in_megabytes;
	int		swap_size;
	void		*buf_for_swap_reservation;
	sc_syslog_msg_handle_t handle;

	//
	// Start the single state machine thread.
	//
	state_machine_threadpool::initialize();

#ifndef EUROPA_FARM
	//
	// Start the thread that notifies the President of state changes.
	// This thread is active only on the President.
	//
	notify_state_change_threadpool::initialize();

	//
	// Start the thread that notifies the President of status changes.
	// This thread is active only on the President.
	//
	notify_status_change_threadpool::initialize();
	//
	// Start up the thread that will process RMA service state callbacks.
	// This thread is active only on the President.
	//
	thr_create_err(NULL, (size_t)0, (void *(*)(void *))rgfreeze,
	    NULL, THR_BOUND, NULL);
#endif

#ifndef EUROPA_FARM
	//
	// Create the RGM block thread. The RGM block thread will be idle
	// till the state machine code is executed for the first time.
	//
	thr_create_err(NULL, 0, rgm_block_thread_start, NULL, THR_BOUND, NULL);

	/*
	 * Create fencing thread.
	 *
	 * The fencing thread will be idle until we signal its cond_var
	 * following a reconfiguration.
	 *
	 * We must create the fencing_thred after we initialize the
	 * state machine threadpool because it tries to queue a state
	 * machine task.
	 *
	 * See the file rgm_fencing.cc for details on the rgm fencing
	 * algorithm.
	 */
	thr_create_err(NULL, 0, fencing_thread_start, NULL, THR_BOUND, NULL);

	//
	// Failback is done by the President for RGs when other nodes boot
	// and join the cluster. The maximum number of failback threads
	// that can be active at a time is approximately the number of RGs,
	// as there is generally one failback thread per RG.  There is
	// a chance for there to be more than one thread per RG if there
	// are multiple different cluster reconfigurations in close succession.
	// Thus, we double the number of RGs to obtain the desired number of
	// threads.
	//
	// Since there may be an essentially unbounded number of RGs,
	// we limit the threads to a static maximum. If the current number
	// of RGs is low, we create a static minimum number of threads since
	// RGs may be added later and we do not change the number of
	// threads later.
	//

	threads_desired = Rgm_state->rgm_total_rgs * 2;
	if (threads_desired < RGM_MIN_FAILBACK_THREADS)
		threads_desired = RGM_MIN_FAILBACK_THREADS;
	else if (threads_desired > RGM_MAX_FAILBACK_THREADS)
		threads_desired = RGM_MAX_FAILBACK_THREADS;

	failback_threadpool::initialize(threads_desired);
#endif

	//
	// Callback methods are launched per resource. The maximum number
	// of launch_method threads that can be active at a time = the number
	// of Rs.
	//
	// Since there may be an essentially unbounded number of Rs,
	// we limit the threads to a static maximum. If the current number
	// of Rs is low, we create a static minimum number of threads since
	// Rs may be added later and we do not change the number of
	// threads later.
	//

	number_of_rs = 0;
	for (rgp = Rgm_state->rgm_rglist; rgp != NULL; rgp = rgp->rgl_next)
		number_of_rs += rgp->rgl_total_rs;

	threads_desired = number_of_rs;

	if (threads_desired < RGM_MIN_LAUNCH_METHOD_THREADS)
		threads_desired = RGM_MIN_LAUNCH_METHOD_THREADS;
	else if (threads_desired > RGM_MAX_LAUNCH_METHOD_THREADS)
		threads_desired = RGM_MAX_LAUNCH_METHOD_THREADS;

	//
	// Limit the number of threads based on the size of the system.
	// We use the amount of memory in the system as a heuristic
	// to size the system. Limit the number of threads to 1 thread per
	// RGM_MEGABYTES_PER_LAUNCH_METHOD_THREAD megabytes of memory.
	//
	memory_in_bytes = (long long)sysconf(_SC_PHYS_PAGES) *
	    (long long)sysconf(_SC_PAGESIZE);
	memory_in_megabytes = (int)(memory_in_bytes / (1024 * 1024));

	max_launch_method_threads = memory_in_megabytes /
	    RGM_MEGABYTES_PER_LAUNCH_METHOD_THREAD;

	if (threads_desired > max_launch_method_threads)
		threads_desired = max_launch_method_threads;

	launch_method_threadpool::initialize(threads_desired);

	//
	// Reserve an additional amount of swap space for the RGMD by
	// mallocing and freeing a large chunk of memory. If the size of
	// the chunk is larger than the largest chunk in the process's free
	// pool, malloc() calls sbrk() to get the new chunk memory mapped
	// with swap space reserved for it. free() simply keeps this chunk
	// in the free pool and allows it to be doled out (wholly or in
	// pieces) when the process does new mallocs. It is not returned
	// to the system.
	//
	// This is a hack, but is a reasonably stable (though unspecified)
	// feature of the C library on Solaris.
	//

	swap_size = number_of_rs * RGM_SWAPSPACE_PER_RESOURCE;
	if (swap_size < RGM_MIN_RESERVED_SWAP_SIZE)
		swap_size = RGM_MIN_RESERVED_SWAP_SIZE;
	else if (swap_size > RGM_MAX_RESERVED_SWAP_SIZE)
		swap_size = RGM_MAX_RESERVED_SWAP_SIZE;

	buf_for_swap_reservation = malloc_nocheck((size_t)swap_size);

	//
	// If the above allocation failed, try to allocate the smallest
	// size that RGMD can live with.
	//
	if ((buf_for_swap_reservation == NULL) &&
	    (swap_size > RGM_MIN_RESERVED_SWAP_SIZE)) {
		swap_size = RGM_MIN_RESERVED_SWAP_SIZE;
		buf_for_swap_reservation = malloc_nocheck((size_t)swap_size);
	}

	if (buf_for_swap_reservation == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to allocate a sufficient amount
		// of memory upon starting up. This is a fatal error. The rgmd
		// will produce a core file and will force the node to halt or
		// reboot to avoid the possibility of data corruption.
		// @user_action
		// Make sure that the hardware configuration meets documented
		// minimum requirements. Save a copy of the /var/adm/messages
		// files on all nodes, and of the rgmd core file. Contact your
		// authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: Unable to reserve %d MBytes "
		    "of swap space; exiting"),
		    swap_size/(1024 * 1024));
		sc_syslog_msg_done(&handle);

		// Use exit() instead of abort() to avoid core dump
		exit(1);
		// NOTREACHABLE
	} else {
		free(buf_for_swap_reservation);
		ucmm_print("RGM", NOGET("System memory = %d MBytes; "
		    "RGM reserved %d MBytes of swap space\n"),
		    memory_in_megabytes, swap_size/(1024 * 1024));
	}
}
