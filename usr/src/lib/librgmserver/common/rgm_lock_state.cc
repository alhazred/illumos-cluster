/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)rgm_lock_state.cc	1.19	08/06/23 SMI"

#include <rgm_state.h>
#include <thread.h>
#include <rgm_proto.h>


void
rgm_lock_state()
{
	Rgm_state->rgm_mutex.lock();
}


void
rgm_unlock_state()
{
	Rgm_state->rgm_mutex.unlock();
}

boolean_t
rgm_state_lock_held()
{
	if (Rgm_state->rgm_mutex.lock_held()) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}

boolean_t
am_i_president(void)
{
#ifndef EUROPA_FARM
	sol::nodeid_t nid = orb_conf::node_number();
	if (nid == Rgm_state->rgm_President)
		return (B_TRUE);
#endif
	return (B_FALSE);
}


//
// When the president makes an IDL call on itself,
// there are times when it should not try to get
// the lock because it already holds the lock.
// In those cases, call slave_lock_state() instead
// of rgm_lock_state().
//

void
slave_lock_state(void)
{
	if (am_i_president())
		return;

	rgm_lock_state();
}


void
slave_unlock_state(void)
{
	if (am_i_president())
		return;

	rgm_unlock_state();
}
