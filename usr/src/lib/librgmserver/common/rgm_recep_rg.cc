/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_recep_rg.cc	1.32	08/05/20 SMI"

/*
 * rgm_recep_rg.cc is the receptionist component of rgmd which
 *	handles the requests from LIBSCHA API functions
 */

#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <errno.h>
#include <rpc/rpc.h>
#include <netdb.h>

#include <rgm/rgm_common.h>
#ifdef EUROPA_FARM
#include <cmm/ucmm_api.h>
#endif
#include <rgm/rgm_cnfg.h>
#include <rgm_state.h>
#include <rgm/rgm_call_pres.h>
#include <rgm_api.h>
#include <rgm_proto.h>

#include <rgm/rgm_recep.h>

/*
 * open_rg()
 *
 *	get the rg sequence id from RG state structure in memory
 *
 * The structure returned by this function contains:
 *	rg sequence id
 *	error code
 */
void
open_rg(rg_open_args *args, open_result_t *result)
{
	rglist_t  *rg = NULL;

	result->ret_code = SCHA_ERR_NOERR;
	rgm_lock_state();
	//
	// If this node has just joined the cluster, there is a window of time
	// in which the president has not yet ordered this node to read the
	// CCR.  If this is the case, sleep until the CCR has been read.
	//
	wait_until_ccr_is_read();
	/*
	 * Get pointer to RG in our in-memory state structure.
	 */
	if ((rg = rgname_to_rg(args->rg_name)) == NULL) {
		ucmm_print("RGM", NOGET("open_rg: RG doesn't exist\n"));
		result->ret_code = SCHA_ERR_RG;
		goto finished;
	}

	/*
	 *  if we got here the rg exists in memory
	 *  get seq number from the rg
	 */
	result->seq_id = strdup_nocheck(rg->rgl_ccr->rg_stamp);
	if (result->seq_id == NULL) {
		result->ret_code = SCHA_ERR_NOMEM;
		ucmm_print("RGM", NOGET(
		    "open_rg: failed on strdup\n"));
		goto finished;
	}
finished:
	rgm_unlock_state();
}

/*
 * get_rg()
 *
 *	get the RG sequence id and value of the given RG from
 * 	in-memory state structure
 *
 * 	The RPC returned buffer is defined in rgm_recep.x file
 */
void
get_rg(rg_get_args *args, get_result_t *result)
{

	rglist_t  *rg = NULL;
	std::map<rgm::lni_t, char *>	val_str;
	namelist_t	*val_array = NULL;
	strarr_list	*ret_list;
	scha_prop_type_t	type;
	boolean_t	need_free = B_FALSE;

	result->ret_code = SCHA_ERR_NOERR;
	result->value_list = NULL;
	char *tmp_val = NULL;

	rgm_lock_state();

	/*
	 * Get pointer to resource group in our in-memory state structure.
	 */
	if ((rg = rgname_to_rg(args->rg_name)) == NULL) {
		ucmm_print("RGM", NOGET(
		    "get_rg: RG <%s> doesn't exist.\n"), args->rg_name);
		result->ret_code = SCHA_ERR_RG;
		goto finished;
	}

	/*
	 * get the value of the given property from in-memory data structure
	 */
	result->ret_code = get_rg_prop_val(args->rg_prop_name, rg,
	    &type, &tmp_val, &val_array, &need_free).err_code;
	val_str[0] = tmp_val;

	if (result->ret_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM", NOGET(
		    "get_rg: failed with err <%d>\n"), result->ret_code);
		goto finished;
	}

	/*
	 *  get RG sequence id
	 */
	result->seq_id = strdup_nocheck(rg->rgl_ccr->rg_stamp);
	if (result->seq_id == NULL) {
		result->ret_code = SCHA_ERR_NOMEM;
		ucmm_print("RGM", NOGET("get_rg: failed on strdup\n"));
		goto finished;
	}

	/*
	 * stored the property value to the RPC returned buffer
	 */

	if ((result->ret_code = store_prop_val(
	    type, val_array, val_str, B_FALSE, NULL,
	    &ret_list, NULL, NULL).err_code) != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("get_rg: cannot store property value\n"));
		if (result->seq_id != NULL)
			free(result->seq_id);
		goto finished;
	}

	result->value_list = ret_list;

finished:

	/*
	 * If need_free is TRUE, need to free the buffer 'val_str' or
	 * val_array (whichever is non-NULL)
	 */
	if (need_free) {
		if (val_str[0] != NULL) {
			free(val_str[0]);
		} else if (val_array != NULL) {
			rgm_free_nlist(val_array);
		}
	}
	rgm_unlock_state();
}

/*
 * get_rg_state()
 *
 *	send a request to the President via ORB RPC call
 *	to get the RG state.
 *
 * 	The RPC returned buffer 'result' is defined (in rgm_recep.x file):
 */
void
get_rg_state(rg_get_state_args *args, get_result_t *result)
{

	result->ret_code = SCHA_ERR_NOERR;

	/*
	 * send a request to the President via ORB RPC call to get
	 * RG state
	 */
	if ((result->ret_code = call_scha_rg_get_state(args, result).err_code)
	    != SCHA_ERR_NOERR) {
		ucmm_print("RGM", NOGET("get_rg_state: "
		    "failed with err <%d>\n"), result->ret_code);
	} else {
		ucmm_print("RGM", NOGET("get_rg_state: RG = %s, seqid = %s\n"),
		    args->rg_name, result->seq_id);
	}
}
