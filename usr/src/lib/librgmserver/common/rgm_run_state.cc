//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_run_state.cc	1.94	09/01/28 SMI"

#include <libintl.h>
#include <rgm_proto.h>
#include <rgm_state.h>
#include <rgm/rgm_msg.h>
#include <rgm/rgm_errmsg.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>

rgm_state_t *Rgm_state;		// ptr to big state data structure

//
// An array of these structures is allocated by rgm_run_state.
// It contains the list of RGs to be brought online or offline
// on this node.  The wk_postpone field is used for determining
// the order in which stopping RGs should be processed.
//
struct workentry {
	rglist_p_t	wk_rg;		// pointer to RG
	boolean_t	wk_postpone;	// postpone stopping this RG?
};


//
// run_state_on_sn_affinitees
// --------------------------
// With physical-node affinities, the affinitee RG might be pending
// online in a different zone than the one in which the affinitent is
// being forced offline.  In this case, the state machine for the affinitee's
// zone might have exited after calling compute_sn_violations() and
// force_offline(), hence the affinitee would never finish going online.
//
// rgm_run_state() checks for this possibility by calling this function
// when an RG goes offline and physical_node affinities are enabled.
//
// 'rgp' is an RG that just went offline on zone 'lni' on the local node.
//
// For each zone 'z' on the local node in which a strong negative affinitee of
// rgp is PENDING_ONLINE, excluding 'lni' itself, we schedule an execution of
// the state machine on zone 'z'.
//
// We don't have to do this for the zone 'lni', because rgm_run_state()
// is already executing on that zone on behalf of the affinitent and will
// process the affinitee as well.
//
static void
run_state_on_sn_affinitees(const struct rglist *rgp, rgm::lni_t lni)
{
	namelist_t *affs;
	rglist_p_t rg_aff;
	LogicalNodeset lnis_found;
	LogicalNodeset nset;

	//
	// If physical-node affinities are not enabled, or if rgp doesn't
	// have any SN affinitees, just return.
	//
	if (use_logicalnode_affinities() ||
	    rgp->rgl_affinities.ap_strong_neg == NULL) {
		return;
	}

	ucmm_print("run_state_on_sn_affinitees", NOGET("lni=%d, rg=<%s>"),
	    lni, rgp->rgl_ccr->rg_name);

	// Set nset to contain all zones on the local node except 'lni' itself.
	get_all_zones_on_phys_node(lni, nset);
	nset.delLni(lni);

	// Iterate over all SN affinitees of rgp, over all zones in nset
	for (affs = rgp->rgl_affinities.ap_strong_neg; affs != NULL;
	    affs = affs->nl_next) {
		rg_aff = rgname_to_rg(affs->nl_name);
		CL_PANIC(rg_aff != NULL);

		rgm::lni_t n = 0;
		while ((n = nset.nextLniSet(n + 1)) != 0) {
			if (rg_aff->rgl_xstate[n].rgx_state ==
			    rgm::RG_PENDING_ONLINE) {
				lnis_found.addLni(n);
			}
		}
	}

	if (!lnis_found.isEmpty()) {
		// queue executions of state machine
		rgm::lni_t n = 0;
		while ((n = lnis_found.nextLniSet(n + 1)) != 0) {
			run_state_machine(n, "run_state_on_sn_affinitees");
		}
	} else {
		//
		// Log a debug message.
		//
		ucmm_print("run_state_on_sn_affinitees", NOGET(
		    "did not call rgm_run_state, lni %d.\n"), lni);
	}
}


//
// is_dep
//
// Returns TRUE if the specified 'rg' depends on some rg in the
// list 'rgplist' whose state on the given lni, 'mynodeid', is 'state'.
// The length of rgplist is 'length'.  We skip over the RG in the
// 'myindex' position, which is the specified 'rg'.
//
// Used for determining the order in which a list of RGs should be
// brought online.  The starting of dependents should be postponed until
// after the RGs on which they depend are started.
//
static boolean_t
is_dep(const struct rglist *rg, const struct workentry *rgplist, int myindex,
    int length, rgm::lni_t mynodeid, rgm::rgm_rg_state state)
{
	rglist_p_t rg_dep;
	namelist_t *dep;
	int j;

	// for all RG names in the dependencies list
	for (dep = rg->rgl_ccr->rg_dependencies; dep != NULL;
	    dep = dep->nl_next) {
		// for all RG names in the RG worklist
		for (j = 0; j < length; j++) {
			if (j == myindex) {
				// skip this RG
				continue;
			}

			rglist_p_t rg1 = rgplist[j].wk_rg;
			ucmm_print("RGM",
			    NOGET("YYYYY compare <%s> and <%s>\n"),
			    rg1->rgl_ccr->rg_name, dep->nl_name);
			if (strcmp(rg1->rgl_ccr->rg_name, dep->nl_name) == 0) {
				// The name have been checked before
				// entering this function. Assert rg_dep
				// isn't NULL.
				rg_dep = rgname_to_rg(dep->nl_name);
				CL_PANIC(rg_dep);
				if (rg_dep->rgl_xstate[mynodeid].rgx_state ==
				    state) {
					ucmm_print("RGM", NOGET(
					    "YYYYY <%s> depends on <%s>\n"),
					    rg->rgl_ccr->rg_name, dep->nl_name);
					return (B_TRUE);
				}
			}
		}
	}
	return (B_FALSE);
}

static boolean_t
update_rx_ok_to_start(const struct rglist *rgp, rgm::lni_t lni)
{
	boolean_t ret = B_FALSE;
	for (rlist_p_t rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		if (rp->rl_xstate[lni].rx_state == rgm::R_PRENET_STARTED) {
			ucmm_print("update_rx_ok_to_start", NOGET("resource %s"
			    " is in %s state"), rp->rl_ccrdata->r_name,
			    SCHA_STARTING);
			rp->rl_xstate[lni].rx_ok_to_start = B_TRUE;
			ret = B_TRUE;
		}
	}
	return (ret);
}

//
// is_rg_pending_offline
//
// Returns TRUE if 'rg' is in a pending_offline state in the given lni
// 'mynodeid'.  Otherwise returns FALSE.
// Called when checking RG_dependencies for RGs that are stopping, so that
// we enforce RG dependencies for RGs in any kind of pending-offline state.
//
static boolean_t
is_rg_pending_offline(rglist_p_t rg, rgm::lni_t mynodeid)
{
	rgm::rgm_rg_state state = rg->rgl_xstate[mynodeid].rgx_state;

	if (state == rgm::RG_PENDING_OFFLINE ||
	    state == rgm::RG_PENDING_OFF_STOP_FAILED ||
	    state == rgm::RG_PENDING_OFF_START_FAILED) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}


//
// has_pending_offline_dependents
//
// Returns TRUE if 'rg' has pending_offline dependents in the list 'rglist',
// in the given lni 'mynodeid'.
// That is, if some RG in 'rglist' is pending offline and depends on 'rg'.
// Otherwise returns FALSE.
// Called when trying to determine the order in which RGs
// should be brought offline.
// Dependents should be brought offline first.
//
static boolean_t
has_pending_offline_dependents(const struct rglist *rg,
    const struct workentry *rgwlist, int length, rgm::lni_t mynodeid)
{
	int j;
	rglist_p_t wrg;

	for (j = 0; j < length; j++) {
		wrg = rgwlist[j].wk_rg;
		// is the RG in the worklist (rgwlist) dependent on
		// the RG (rg) being processed, and is the RG in
		// the worklist in a pending offline state?
		if (is_rg_pending_offline(wrg, mynodeid)) {
			scha_errmsg_t scha_err = {SCHA_ERR_NOERR, NULL};
			boolean_t retval = is_rg1_dep_on_rg2(&scha_err,
			    wrg->rgl_ccr, rg->rgl_ccr);

			if (scha_err.err_code != SCHA_ERR_NOERR) {
				// error is NOMEM or internal -- fatal
				rgm_fatal_reboot(
				    "has_pending_offline_dependents",
				    B_TRUE, B_FALSE, NULL);
			}
			if (retval) {
				return (B_TRUE);
			}
		}
	}

	return (B_FALSE);
}


//
// compute_dependents
//
// 'list' is the list of RGs needing to be brought online or offline.
// 'length' is the length of the list.
// Search 'list' and set wk_postpone = TRUE for those pending_offline
// RGs that have dependencies on other pending_offline RGs in 'list'.
// The state machine will postpone stopping the RGs marked wk_postpone == TRUE
// until all their dependents are no longer pending offline.
//
static void
compute_dependents(struct workentry *wlist, int length, rgm::lni_t mynodeid)
{
	int i;
	rglist_p_t rg;

	// Start over from scratch.  Clear the whole table.
	for (i = 0; i < length; i++)
		wlist[i].wk_postpone = B_FALSE;

	for (i = 0; i < length; i++) {
		rg = wlist[i].wk_rg;
		if (is_rg_pending_offline(rg, mynodeid)) {
			if (has_pending_offline_dependents(rg, wlist, length,
			    mynodeid)) {
				// This pending offline RG has dependents
				// that are also pending offline, so
				// postpone stopping this RG until after
				// the dependents have stopped.
				wlist[i].wk_postpone = B_TRUE;
			}
		}
	}
}

//
// force_offline
// -------------
// For each name in rg_names, makes the RG PENDING_OFFLINE on the node
// indicated by 'lni', if it is not already in some form of OFFLINE or
// PENDING_OFFLINE.
//
// If physical node affinities are in effect, the RG is made pending_offline
// on all zones on the same physical node as lni.  Otherwise it is
// made pending_offline only on lni itself.
//
// If any state changes are made in a zone, calls run_state_machine to
// schedule another running of the state machine for that zone.
// As we iterate over all of the RG operands, we accumulate a set of changed
// zones in the LogicalNodeset 'lnis_changed'.
//
// Assumes the state lock is held.
//
// If it encounters an RG in STOP_FAILED or PENDING_OFF_STOP_FAILED, and
// ignore_stop_failed is B_FALSE, bails out with an error.  Otherwise ignores
// it.
//
scha_err_t
force_offline(const char *rg_name, const namelist_t *rg_names, rgm::lni_t lni,
    boolean_t ignore_stop_failed)
{
	rglist_p_t rglp;
	LogicalNodeset lnis_changed;
	boolean_t is_changed = B_FALSE;
	LogicalNodeset nset;
	scha_err_t err_code = SCHA_ERR_NOERR;

	if (use_logicalnode_affinities()) {
		nset.addLni(lni);
	} else {
		get_all_zones_on_phys_node(lni, nset);
	}

	for (; rg_names != NULL; rg_names = rg_names->nl_next) {
#if (SOL_VERSION >= __s10)
		if (is_enhanced_naming_format_used(rg_names->nl_name)) {
			char *rc = NULL, *rr = NULL;
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "force_offline "
				    "Running at too low a version:RG <%s> "
				    "has affinity for RG <%s> "
				    "which is in not in local cluster\n"),
				    rg_name, rg_names->nl_name);
				continue;
			}

			// call with cleanup_stale_affs set to false.
			(void) handle_inter_cluster_rg_affinities(rg_name,
			    rg_names->nl_name, rgm::FORCE_OFFLINE, &is_changed,
			    lni, ignore_stop_failed, NULL, B_FALSE);
			continue;
		}
#endif

		rglp = rgname_to_rg(rg_names->nl_name);
		CL_PANIC(rglp != NULL);

		rgm::lni_t n = 0;
		while ((n = nset.nextLniSet(n + 1)) != 0) {

			switch (rglp->rgl_xstate[n].rgx_state) {

			case rgm::RG_OFFLINE:
			case rgm::RG_PENDING_OFFLINE:
			case rgm::RG_OFFLINE_START_FAILED:
			case rgm::RG_OFF_BOOTED:
			case rgm::RG_OFF_PENDING_METHODS:
			case rgm::RG_OFF_PENDING_BOOT:
			case rgm::RG_PENDING_OFF_START_FAILED:
				//
				// It's already offline or heading offline.
				// Just log a debug message.
				//
				ucmm_print("force_offline", NOGET(
				    "RG <%s> already offline or pending off "
				    "on lni %d.\n"), rglp->rgl_ccr->rg_name, n);
				break;

			case rgm::RG_ERROR_STOP_FAILED:
			case rgm::RG_PENDING_OFF_STOP_FAILED:
				if (!ignore_stop_failed) {
					//
					// This is an error that should never
					// happen.  In case it does,
					// bail out with an error code.
					// Note that we may have already
					// told some RGs to go offline.  We
					// must jump to the end to call
					// run_state_machine.
					//
					ucmm_print("force_offline", NOGET(
					    "RG <%s> in STOP_FAILED or "
					    "PENDING_OFF_STOP_FAILED on "
					    "lni %d.\n"),
					    rglp->rgl_ccr->rg_name, n);
					err_code = SCHA_ERR_INTERNAL;
					goto force_offline_end;
				}
				break;

			case rgm::RG_ONLINE:
			case rgm::RG_ON_PENDING_R_RESTART:
			case rgm::RG_PENDING_ONLINE:
			case rgm::RG_PENDING_ONLINE_BLOCKED:
			case rgm::RG_PENDING_ON_STARTED:
				//
				// We are allowed to switch directly to
				// PENDING_OFFLINE from a PENDING_DISABLED
				// state.
				//
			case rgm::RG_ON_PENDING_DISABLED:
			case rgm::RG_ON_PENDING_MON_DISABLED:
				//
				// set RG state to PENDING_OFFLINE and
				// note that we've changed something.
				//
				set_rgx_state(n, rglp,
				    rgm::RG_PENDING_OFFLINE);
				lnis_changed.addLni(n);
				break;

			case rgm::RG_ON_PENDING_METHODS:
				//
				// We need to wait for the methods to finish
				// running before we stop the RG.  Set
				// the rgx_postponed_stop flag, and let the
				// state machine handle it.  We have the lock,
				// so we know that the state machine is
				// not currently running.  We also know
				// that when the method in question
				// finishes running, the state machine will
				// be run again.  Thus, we do not need to
				// run the state machine now.  The state
				// machine thread that will handle the
				// method that finishes will be guaranteed
				// to catch the postponed_stop flag.
				//
				rglp->rgl_xstate[n].rgx_postponed_stop = B_TRUE;
				ucmm_print("force_offline", NOGET("set "
				    "postponed_stop for rg <%s> on lni %d.\n"),
				    rglp->rgl_ccr->rg_name, n);
				break;

			}	// switch on rglp state
		}	// while-loop over LNIs
	}	// for-loop over RGs

force_offline_end:
	//
	// Incase there ia a remote cluster rg which has declared strong
	// negatvive affinity on the local cluster rg, and this local
	// cluster rg is switching online on this lni, the inter cluster
	// affinitent rg would have been brought offline on the
	// zone cluster node.
	// The local cluster rg will be in "pending online" state  and
	// there is need to run state machine. The is_changed flag will be set
	// when the strong negative affinitent is brought offline. This
	// will come into picture only for inter cluster rg affinities.
	// For the physical node affinities lnis_changed is used to decide
	// if the state machine has to be run.
	//
	if (is_changed) {
		// queue another execution of state machine
		ucmm_print("RGM", NOGET("force_offline: "
		    "calling run_state_machine for "
		    " inter cluster rg affinities.\n"));
		run_state_machine(lni, "force_offline");
	} else if (!lnis_changed.isEmpty()) {
		// queue executions of state machine
		ucmm_print("force_offline", NOGET(
		    "calling run_state_machine, lni %d.\n"), lni);

		rgm::lni_t n = 0;
		while ((n = lnis_changed.nextLniSet(n + 1)) != 0) {
			run_state_machine(n, "force_offline");
		}
	} else {
		//
		// Log a debug message.
		//
		ucmm_print("force_offline", NOGET(
		    "did not call rgm_run_state, lni %d.\n"), lni);
	}
	return (err_code);
}

static void
affinity_stop_failed_err(char *name)
{
	sc_syslog_msg_handle_t handle;

	(void) sc_syslog_msg_initialize(&handle,
	    SC_SYSLOG_RGM_RGMD_TAG, "");
	//
	// SCMSGS
	// @explanation
	// The rgmd is unable to bring the specified resource group online on
	// the local node, because one or more resource groups related to
	// it by strong negative RG affinities are in STOP_FAILED error state.
	// @user_action
	// Find the errored resource groupss and resources by using
	// "clresourcegroup status" and "clresource status". Clear the
	// STOP_FAILED error states by using "clresource clear".
	// Use ps(1) or application-specific commands to make sure
	// that the resource has stopped completely; kill resource-related
	// processes if necessary.
	//
	(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT("Error: "
	    "unable to bring Resource Group <%s> ONLINE, because one or more "
	    "Resource Groups with a strong negative affinity for it are in "
	    "an error state."), name);
	sc_syslog_msg_done(&handle);
}

//
// rgm_run_state - "run" the rgmd state machine
//
// The thread that calls this function should already hold the rgm_state
// mutex, i.e., it should already have called rgm_lock_state().  After
// this function returns, the calling thread should release the mutex
// by calling rgm_unlock_state().
//
// If this function returns, it always returns 0.  If an error occurs in
// the call to process_resource, it panicks the node.
//
// The 'lni' parameter indicates in which zone to execute the state
// machine.  The lni must indicate a zone on the local node, which may be
// the global zone or a non-global zone.
//
scha_errmsg_t
rgm_run_state(rgm::lni_t lni)
{
	boolean_t r_state_changed = B_TRUE;	// has any R/RG state changed?
	boolean_t need_wake = B_FALSE;		// need to wake president?
	rgm::rgm_rg_state RG_state;		// RG state
	rlist_p_t R;				// ptr to list of r states
	rglist_p_t RG;				// ptr to list of rg states
	struct workentry *work_list;
	rglist_p_t rglp;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	int rgindex;
	int i;
	sc_syslog_msg_handle_t handle;
#ifndef EUROPA_FARM
	bool fencing_complete;
	bool rgm_block_complete;
#endif

	//
	// Create RG work_list:
	//
	// Create an array of pointers to RGs (rglist_t's)
	// that have a "pending" RG_state on the local node.
	//


	work_list = (struct workentry *)
	    malloc(sizeof (struct workentry) *
	    (uint_t)(Rgm_state->rgm_total_rgs));

	for (rgindex = 0; rgindex < Rgm_state->rgm_total_rgs; rgindex++) {
		work_list[rgindex].wk_rg = NULL;
		work_list[rgindex].wk_postpone = B_FALSE;
	}

#ifndef EUROPA_FARM
	rgm_block_complete = is_rgm_block_complete();

	//
	// Find out if fencing is complete.
	//
	fencing_complete = is_fencing_complete();
#endif

	rgindex = 0;
	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {

		// If the RG is unmanaged, only add it to the list if
		// it's offline pending methods (scswitch -u)

		if ((rglp->rgl_ccr->rg_unmanaged == B_TRUE) &&
		    (rglp->rgl_xstate[lni].rgx_state !=
		    rgm::RG_OFF_PENDING_METHODS)) {
			ucmm_print("RGM",
			NOGET("NOT adding UNMANAGED RG <%s> to worklist\n"),
				    rglp->rgl_ccr->rg_name);
			continue;
		}

		switch (rglp->rgl_xstate[lni].rgx_state) {
		case rgm::RG_PENDING_ONLINE:
#ifdef EUROPA_FARM
	/*
	 * Need a fencing mechanism
	 */
#else
			//
			// Check for fencing.
			//
			if (!fencing_complete && need_wait_fencing(rglp, lni)) {
				//
				// We need to wait for fencing.
				//
				ucmm_print("RGM",
				    NOGET("run_state: NOT adding <%s> "
				    "to worklist: waiting for fencing\n"),
				    rglp->rgl_ccr->rg_name);
				break;
			}
#endif
			//
			// FALLTHRU
			//
		case rgm::RG_PENDING_ON_STARTED:
		case rgm::RG_PENDING_OFFLINE:
		case rgm::RG_PENDING_OFF_START_FAILED:
		case rgm::RG_PENDING_OFF_STOP_FAILED:
		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_METHODS:
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_ON_PENDING_MON_DISABLED:
		case rgm::RG_OFF_PENDING_BOOT:
		case rgm::RG_ON_PENDING_R_RESTART:
#ifndef EUROPA_FARM
			//
			// check if RGM block is complete.
			//
			if (!rgm_block_complete) {

				ucmm_print("RGM",
				    NOGET("run_state: NOT adding <%s> "
				    "to worklist: waiting on RGM block\n"),
				    rglp->rgl_ccr->rg_name);
				break;
			}
#endif
			work_list[rgindex++].wk_rg = rglp;
			ucmm_print("RGM",
			    NOGET("run_state: added <%s> <%s> to worklist\n"),
			    pr_rgstate(rglp->rgl_xstate[lni].rgx_state),
			    rglp->rgl_ccr->rg_name);
			break;

		case rgm::RG_OFFLINE_START_FAILED:
		case rgm::RG_OFFLINE:
		case rgm::RG_OFF_BOOTED:
		case rgm::RG_ONLINE:
		case rgm::RG_ERROR_STOP_FAILED:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
			// These are "terminal" states: there is no
			// work for the state machine to do.
			ucmm_print("RGM",
			NOGET("run_state: NOT adding <%s> <%s> to worklist\n"),
			    pr_rgstate(rglp->rgl_xstate[lni].rgx_state),
			    rglp->rgl_ccr->rg_name);
			break;

		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd state machine on this node has discovered
			// that the indicated resource group's state
			// information is corrupted. The state machine will
			// not launch any methods on resources in this
			// resource group. This may indicate an internal logic
			// error in the rgmd.
			// @user_action
			// Other syslog messages occurring before or after
			// this one might provide further evidence of the
			// source of the problem. If not, save a copy of the
			// /var/adm/messages files on all nodes, and (if the
			// rgmd crashes) a copy of the rgmd core file, and
			// contact your authorized Sun service provider for
			// assistance.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "INTERNAL ERROR: rgm_run_state: "
			    "bad state <%d> for resource group <%s>",
			    rglp->rgl_xstate[lni].rgx_state,
			    rglp->rgl_ccr->rg_name);
			sc_syslog_msg_done(&handle);
		}

		// If RG is in any state other than PENDING_OFFLINE, clear
		// the rgx_net_relative_restart flag
		if (rglp->rgl_xstate[lni].rgx_state !=
		    rgm::RG_PENDING_OFFLINE) {
			rglp->rgl_xstate[lni].rgx_net_relative_restart =
			    B_FALSE;
		}
	}

	//
	// The work_list now contains pointers to all RGs that are in
	// "pending" (or non-endish) states.  These RGs need to be processed
	// by the state machine.  There are (rgindex) number of entries in
	// the list.
	//

	//
	// r_state_changed is set to TRUE when a method is launched.
	// We'll continue to iterate through this loop until no more
	// methods are launched and there are no further RG state
	// changes.  If any RG achieved a terminal ("endish") state,
	// then we will wake the president before returning.
	//
	while (r_state_changed && rgindex > 0) {
		// Repeat until there's nothing left to do

		r_state_changed = B_FALSE;

		//
		// mark the PENDING_OFFLINE RGs in the worklist whose
		// offlining must be postponed until some other
		// PENDING_OFFLINE RGs in the list go offline.
		//
		compute_dependents(work_list, rgindex, lni);

		// For each (RG) in (work_list)
		for (i = 0; i < rgindex; i++) {
			rgm::rgm_rg_state	offstate = rgm::RG_OFFLINE;
			namelist_t *sn_violations = NULL;
			int sn_violations_count = 0;
			namelist_t *offenders = NULL;
			char *errmsg_str = NULL;
			scha_err_t err = SCHA_ERR_NOERR;

			RG = work_list[i].wk_rg;
			ucmm_print("RGM", NOGET(
			    "run_state: in inner loop RG <%s>"),
			    RG->rgl_ccr->rg_name);

			//
			// all_off, all_on, all_mon, all_methods, any_restart,
			// any_ok_start, any_starting_depend, all_dis_off,
			// any_stop_failed, any_prenet_started flags:
			// These are passed as "in/out" parameters to
			// process_resource(), which is called on each resource
			// in the RG.  Process_resource() will set the
			// flags to B_FALSE if the desired condition has not
			// yet been achieved (for the all_* flags) or to B_TRUE
			// if the specified condition is met by a resource
			// (for the any_* flags).  For example, if the RG is
			// PENDING_OFFLINE but the resource has not yet stopped,
			// then process_resource() will set all_off to false.
			// If all_off remains true after process_resource()
			// has been called on all resources in the RG, then
			// we are finished taking the RG offline.  We change
			// its state to OFFLINE and call wake_president().
			// The flags are used similarly for other PENDING_
			// states.
			//
			// all_on means that all resources are started
			// but monitors may not be started yet.  This is used
			// to implement RG-dependencies, and takes the RG into
			// the PENDING_ON_STARTED state.  all_mon means that
			// all resources are started and all monitors are
			// started, taking the RG to the ONLINE state.
			//
			// all_methods means that all pending INIT, FINI,
			// UPDATE, or BOOT methods have been run
			// on an RG which is in ON_PENDING_METHODS,
			// OFF_PENDING_METHODS, or OFF_PENDING_BOOT state.
			// The RG reverts to the ONLINE or OFFLINE state as
			// appropriate.
			//
			// all_methods is also used for the RG states
			// ON_PENDING_DISABLED, ON_PENDING_MON_DISABLED,
			// PENDING_ONLINE and PENDING_OFF_STOP_FAILED.
			// If true, all_methods indicates
			// that all Rs have reached "terminal" states.
			// For example, for RG_PENDING_ONLINE, all_methods
			// indicates that no further progress can be made on
			// this RG's resources; the resources might be in any
			// of the following states:
			//	ONLINE
			//	ONLINE_UNMON with no monitoring enabled
			//	OFFLINE or PRENET_STARTED, but unable to start
			//	   due to a strong direct or indirect dependency
			//	   on a START_FAILED resource.
			//	disabled
			// and so forth.
			// If all_methods is true while the desired terminal
			// state has not been reached, the state machine
			// discontinues processing on the RG and moves it to
			// the appropriate terminal state.  Specifically, if
			// the RG is PENDING_ONLINE and all_on is false and
			// all_methods is true, then the RG is moved to ONLINE.
			// Even though all the resources have not gone online,
			// they have gotten as close to online as they are going
			// to get.  Note, if some resources ended up
			// START_FAILED or MON_FAILED, the RG state as reported
			// by the API is ONLINE_FAULTED although the internal
			// state machine RG state is ONLINE.
			//
			// Similarly, if the RG is PENDING_OFF_STOP_FAILED
			// and all_off is false and all_methods is true, we
			// move the RG to ERROR_STOP_FAILED.
			// NOTE that we do not have to test all_methods for
			// an RG that is PENDING_OFFLINE, because if any of
			// its resources enters the STOP_FAILED state then
			// the RG itself moves to PENDING_OFF_STOP_FAILED.
			//
			// all_off is also used with PENDING_OFF_STOP_FAILED,
			// but it indicates that all resources are now
			// OFFLINE so the RG should go to OFFLINE.
			//
			// any_restart is initialized to false; it is set true
			// if any R in the RG is restarting.
			//
			// any_ok_start, any_starting_depend, and
			// any_prenet_started
			// are initialized to B_FALSE, and are set to true
			// if any R in the RG has a state of OK_TO_START,
			// STARTING_DEPEND, or PRENET_STARTED, respectively.
			//
			// all_dis_off and any_stop_failed are used in the
			// ON_PENDING_DISABLED and ON_PENDING_MON_DISABLED
			// rg states.  all_dis_off indicates whether all of
			// the disabled resources have been stopped.
			// any_stop_failed indicates that a resource is
			// stop_failed.  any_stop_failed is also used in other
			// rg states such as ON_PENDING_R_RESTART,
			// PENDING_ONLINE, and ON_PENDING_METHODS.
			//

			boolean_t all_off = B_TRUE;	// all Rs offline?
			boolean_t all_on = B_TRUE;	// all Rs online?
			boolean_t all_mon = B_TRUE;	// all Rs monitored?
			boolean_t all_methods = B_TRUE;	// all init, fini, boot
							// meths run?
							// or (for pending_on or
							// stop_failed RG) all
							// Rs have reached
							// terminal states?
			boolean_t any_restart = B_FALSE;
							// any R restarting?
			boolean_t any_ok_start = B_FALSE;
							// any in OK_TO_START?
			boolean_t any_starting_depend = B_FALSE;
							// any in
							// STARTING_DEPEND?
			boolean_t any_prenet_started = B_FALSE;
							// any in
							// PRENET_STARTED?
			boolean_t all_dis_off = B_TRUE;	// all disabled Rs
							// stopped?
			boolean_t any_stop_failed = B_FALSE;
							// any R stop_failed?

			// RG state
			RG_state = RG->rgl_xstate[lni].rgx_state;

			//
			// (bugid 4327148) Note: We do not postpone bringing an
			// RG online while needed device services (global
			// resources) are frozen.  We just go ahead
			// and start the RG.  We do however extend method
			// timeouts and suspend scha_control giveovers
			// until the device services are unfrozen.
			//
			// The requirement to postpone starting an RG while its
			// global resources are offline is addressed by the
			// "HAStoragePlus" (HASP)resource type and similar
			// types.  The HASP resource won't come online until
			// its corresponding device service is online.  Then
			// the user can put the HASP resource into an
			// RG, and make other resources dependent on it.
			// This provides a way of postponing bringing
			// resources online until the disks are available.
			//

			//
			// If this RG is being brought online on this node
			// at the same time as any listed in
			// RG_dependencies, bring those in the list online
			// first.  And conversely for offline: bring this
			// RG offline first.  Similarly for RGs running
			// BOOT methods.
			//
			switch (RG_state) {
			case rgm::RG_PENDING_ONLINE:
				//
				// First, we check affinity violations.
				//
				// Sanity check that our strong positive
				// affinities are satisfied.
				//
				if (!sp_affinities_satisfied(err, RG, lni,
				    NULL, &offenders)) {
					if (err != SCHA_ERR_NOERR) {
						// low memory; reboot the node
						rgm_fatal_reboot(
						    "rgm_run_state.1",
						    B_TRUE, B_FALSE, NULL);
					}

					errmsg_str = namelist_to_str(offenders);
					(void) sc_syslog_msg_initialize(&handle,
					    SC_SYSLOG_RGM_RGMD_TAG, "");
					//
					// SCMSGS
					// @explanation
					// The rgmd is enforcing the strong
					// positive affinities of the resource
					// groups. This behavior is normal and
					// expected.
					// @user_action
					// No action required. If desired, use
					// clresourcegroup to change the
					// resource group affinities.
					//
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE, SYSTEXT(
					    "Error: unable to bring Resource "
					    "Group <%s> ONLINE, because the "
					    "Resource Groups <%s> for "
					    "which it has a strong positive "
					    "affinity are not online."),
					    RG->rgl_ccr->rg_name, errmsg_str);
					sc_syslog_msg_done(&handle);
					free(errmsg_str);
					rgm_free_nlist(offenders);
					offenders = NULL;
					errmsg_str = NULL;
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_OFFLINE);
					r_state_changed = B_TRUE;
					continue;
				}

				//
				// Sanity check that our strong negative
				// affinities are satisfied.
				//
				if (!sn_affinities_satisfied(err, RG, lni,
				    NULL, &offenders)) {
					if (err != SCHA_ERR_NOERR) {
						// low memory; reboot the node.
						rgm_fatal_reboot(
						    "rgm_run_state.2",
						    B_TRUE, B_FALSE, NULL);
					}

					errmsg_str = namelist_to_str(offenders);
					(void) sc_syslog_msg_initialize(&handle,
					    SC_SYSLOG_RGM_RGMD_TAG, "");
					//
					// SCMSGS
					// @explanation
					// The rgmd is enforcing the strong
					// negative affinities of the resource
					// groups. This behavior is normal and
					// expected.
					// @user_action
					// No action required. If desired, use
					// clresourcegroup to change the
					// resource group affinities.
					//
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE, SYSTEXT(
					    "Error: unable to bring Resource "
					    "Group <%s> ONLINE, because the "
					    "Resource Groups <%s> for "
					    "which it has a strong negative "
					    "affinity are online."),
					    RG->rgl_ccr->rg_name, errmsg_str);
					sc_syslog_msg_done(&handle);
					free(errmsg_str);
					rgm_free_nlist(offenders);
					offenders = NULL;
					errmsg_str = NULL;
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_OFFLINE);
					r_state_changed = B_TRUE;
					continue;
				}

				//
				// Now check the state on this node of all
				// RGs with strong negative affinities
				// for us.
				//
				// If compute_sn_violations returns B_FALSE,
				// it means that one or more of the RGs
				// with strong negative affinities for us
				// is in STOP_FAILED or
				// PENDING_OFF_STOP_FAILED on this node.
				// In that case, we must not bring this RG
				// online, and instead we must move it
				// to PENDING_OFFLINE.
				//
				if (!compute_sn_violations(err, RG, lni,
				    &sn_violations, sn_violations_count,
				    B_TRUE)) {
					if (err != SCHA_ERR_NOERR) {
						// low memory; reboot the node.
						rgm_fatal_reboot(
						    "rgm_run_state.3",
						    B_TRUE, B_FALSE, NULL);
					}
					affinity_stop_failed_err(
					    RG->rgl_ccr->rg_name);
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_OFFLINE);
					r_state_changed = B_TRUE;
					continue;
				}

				//
				// If there are any strong negative violations,
				// we need to instruct those RGs to go offline,
				// and we need to wait for them to do so.  We
				// don't need to compute the entire
				// transitive closure, because anything with
				// a positive affinity for one of the sn
				// violations will get forced offline in
				// subsequent iterations of the state machine
				// (when its affinitent goes PENDING_OFFLINE).
				//
				if (sn_violations_count != 0) {
					if (force_offline(
					    RG->rgl_ccr->rg_name, sn_violations,
					    lni, B_FALSE) != SCHA_ERR_NOERR) {
						//
						// The only error that we
						// can get from force_offline
						// is that one of the RGs
						// was in stop_failed.
						//
						affinity_stop_failed_err(
						    RG->rgl_ccr->rg_name);
						set_rgx_state(lni, RG,
						    rgm::RG_PENDING_OFFLINE);
						r_state_changed = B_TRUE;
						continue;
					}
					ucmm_print("RGM", NOGET(
					    "postponing starting <%s>: "
					    "waiting for SN affinitents to go "
					    "offline\n"),
					    RG->rgl_ccr->rg_name);
					continue;
				}

				//
				// If we reach this point, we know that all
				// RGs with strong negative affinities for
				// the current RG are safely offline on this
				// node, and that all RGs for which we have
				// strong positive affinities are safely
				// online or pending_online.
				//

				//
				// If an RG in the RG_dependencies list is
				// PENDING_ONLINE on this node, postpone
				// starting the current RG.  The
				// depended-on RG must be at least
				// RG_PENDING_ON_STARTED before we start
				// this one.
				//
				if (is_dep(RG, work_list, i, rgindex, lni,
				    rgm::RG_PENDING_ONLINE)) {
					ucmm_print("RGM", NOGET(
					    "YYYYYY postpone starting <%s>\n"),
					    RG->rgl_ccr->rg_name);
					continue;
				}
				break;

			case rgm::RG_PENDING_OFF_STOP_FAILED:
				//
				// RG started out pending offline, then entered
				// this state because of failure of a
				// MONITOR_STOP, STOP, or POSTNET_STOP method.
				// We continue to stop as many resources as
				// possible in this RG, dependencies permitting,
				// before waking the president.
				//
			case rgm::RG_PENDING_OFF_START_FAILED:
				// RG failed to start and is failing over
			case rgm::RG_PENDING_OFFLINE:
				//
				// Check strong positive affinitents.
				// Any RG with a strong positive affinity
				// for us that is not already offline or
				// pending offline on this node must
				// be forced offline.
				//
				// We don't care about the return code here
				// because we don't care if the RGs are
				// in an errored state (we will go offline
				// anyway).
				//
				(void) force_offline(RG->rgl_ccr->rg_name,
				    RG->rgl_affinitents.ap_strong_pos,
				    lni, B_TRUE);

				//
				// If this RG is stopping and RG(s) in the
				// RG_dependencies list are also stopping,
				// be sure to stop this one first.  Don't
				// stop the others until this one is offline.
				//
				if (work_list[i].wk_postpone == B_TRUE) {
					ucmm_print("RGM",
					    NOGET("YYYYYY postpone "
					    "stopping <%s>\n"),
					    RG->rgl_ccr->rg_name);
					// process this one later
					continue;
				}
				break;

			case rgm::RG_OFF_PENDING_BOOT:
				//
				// If an RG in the RG_dependencies list is
				// also OFF_PENDING_BOOT on this node, postpone
				// booting the current RG.  The depended-on
				// RG must be OFF_BOOTED or OFFLINE before
				// we start this one.
				//
				if (is_dep(RG, work_list, i, rgindex, lni,
				    rgm::RG_OFF_PENDING_BOOT)) {
					ucmm_print("RGM",
					    NOGET("YYYYYY postpone booting "
					    "<%s>\n"),
					    RG->rgl_ccr->rg_name);
					continue;
				}
				break;

			case rgm::RG_PENDING_ON_STARTED:
			case rgm::RG_ON_PENDING_METHODS:
			case rgm::RG_OFF_PENDING_METHODS:
			case rgm::RG_ON_PENDING_MON_DISABLED:
			case rgm::RG_ON_PENDING_DISABLED:
			case rgm::RG_ON_PENDING_R_RESTART:
			case rgm::RG_OFFLINE_START_FAILED:
			case rgm::RG_OFFLINE:
			case rgm::RG_OFF_BOOTED:
			case rgm::RG_ONLINE:
			case rgm::RG_ERROR_STOP_FAILED:
			case rgm::RG_PENDING_ONLINE_BLOCKED:
			default:
				// These cases do not involve RG_dependencies
				break;
			}


			//
			// process_resource() will create a separate thread to
			// launch any methods.  While the method is executing,
			// the resource's state will indicate work in progress.
			// It will return immediately if onoff switch disabled
			// and the R is not pending_methods.
			//

			// For (each (R) in this RG's Resource_list)
			for (R = RG->rgl_resources; R != NULL; R = R->rl_next) {

				ucmm_print("RGM",
				    NOGET("run_state: call process on <%s>\n"),
				    R->rl_ccrdata->r_name);

				if ((res = process_resource(
				    lni,
				    R,
				    RG,
				    &r_state_changed,
				    &all_off,
				    &all_on,
				    &all_mon,
				    &all_methods,
				    &any_restart,
				    &any_ok_start,
				    &any_starting_depend,
				    &any_prenet_started,
				    &all_dis_off,
				    &any_stop_failed)).err_code != 0) {
					need_wake = B_TRUE;
					ucmm_print("RGM",
					    NOGET("run_state: bailing after "
					    "process_resource on <%s> "
					    "returned <%d>\n"),
					    R->rl_ccrdata->r_name,
					    res.err_code);
					goto finish;
				}
				ucmm_print("RGM",
				    NOGET("run_state: after process "
				    "on <%s> <%d>\n"),
				    R->rl_ccrdata->r_name, r_state_changed);
			}

			//
			// When process_resource() returns, the launch_method
			// thread may continue to run for a while.
			// r_state_changed == TRUE => method is launched
			// or the r or rg changed state.
			//

			//
			// The RG state may have changed inside the switch
			// statement above, or process_resource might have
			// changed it, so capture it again.
			//
			RG_state = RG->rgl_xstate[lni].rgx_state;

			switch (RG_state) {
			case rgm::RG_PENDING_ONLINE:
				ucmm_print("RGM",
				    NOGET("run_state: RG PENDING ONLINE\n"));
				//
				// If the RG was pending_online, but now all
				// its resources are online, update the RG
				// state to pending_on_started.
				//
				if (!all_on) {
					if (!all_methods) {
						break;
					}
					//
					// all_methods is true.  This means that
					// some resources are R_START_FAILED or
					// R_STARTING_DEPEND.  Other resources
					// that have an intra-RG dependency on
					// those might be R_OFFLINE.
					//
					// We don't syslog a message here,
					// because this can be a transitory
					// state in which we are blocked by
					// an inter-RG dependency and then
					// immediately become unblocked.
					//
					// If the situation persists, the RG
					// will end up e.g. in ONLINE_FAULTED
					// or PENDING_ONLINE_BLOCKED state, and
					// the president will make the failure
					// visible to the user.
					//
					// We need to update rx_ok_to_start if
					// a resource is in R_PRENET_STARTED
					// state so as to run Start method.
					//
					if (update_rx_ok_to_start(RG, lni)) {
						r_state_changed = B_TRUE;
						break;
					}
				}

				// (all_on || all_methods) is true

				// Update our local RG state info
				set_rgx_state(lni, RG,
				    rgm::RG_PENDING_ON_STARTED);
				r_state_changed = B_TRUE;

				/* FALLTHRU */

			case rgm::RG_PENDING_ON_STARTED:
				//
				// If the RG was pending_on_started, and now
				// all its monitors are online, update the RG
				// state to online.
				//
				if (!all_mon && !all_methods) {
					break;
				}

				//
				// Note, if (!all_mon && all_methods), some
				// resources were postponed due to strong
				// R-dependency on a start_failed resource; we
				// already issued a warning above.
				//

				// (all_mon || all_methods) is true

				//
				// We've gone as far as we can go right now.
				// Update our local RG state info.
				// If any resource is stop_failed, move RG
				// to ERROR_STOP_FAILED.
				// If we have any resources in
				// starting_depend, move to PENDING_ONLINE
				// BLOCKED.  Otherwise move to ONLINE.
				//
				if (any_stop_failed) {
					set_rgx_state(lni, RG,
					    rgm::RG_ERROR_STOP_FAILED);
				} else if (any_starting_depend) {
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_ONLINE_BLOCKED);
				} else {
					set_rgx_state(lni, RG, rgm::RG_ONLINE);
				}
				r_state_changed = B_TRUE;
				need_wake = B_TRUE;
				break;

			case rgm::RG_PENDING_OFF_START_FAILED:
				// This is equivalent to PENDING_OFFLINE; the
				// only difference is that when we're done,
				// the state is set to OFFLINE_START_FAILED.
				//
				offstate = rgm::RG_OFFLINE_START_FAILED;
				/* FALLTHRU */

			case rgm::RG_PENDING_OFFLINE:
				//
				// If the RG was pending_offline{_start_failed},
				// and now all its resources are offline, update
				// the RG state to offline{_start_failed}.
				//
				if (all_off) {
					// Update our local RG state info
					set_rgx_state(lni, RG, offstate);
					run_state_on_sn_affinitees(RG, lni);
					r_state_changed = B_TRUE;
					need_wake = B_TRUE;
				} else {
					ucmm_print("RGM",
					    NOGET("YYYYYYY all_off not "
					    "set!!\n"));
				}
				break;

			case rgm::RG_PENDING_OFF_STOP_FAILED:
				//
				// RG started out pending offline, then entered
				// this state because of failure of a
				// MONITOR_STOP, STOP, or POSTNET_STOP method.
				// We continue to stop as many resources as
				// possible in this RG, dependencies permitting,
				// before waking the president.
				//
				if (all_off) {
					// We succeeded in stopping all Rs
					set_rgx_state(lni, RG,
					    rgm::RG_OFFLINE);
				} else if (all_methods) {
					// We cannot stop all Rs, some remain
					// STOP_FAILED
					set_rgx_state(lni, RG,
					    rgm::RG_ERROR_STOP_FAILED);
				} else {
					break;
				}
				// RG is OFFLINE or ERROR_STOP_FAILED
				r_state_changed = B_TRUE;
				need_wake = B_TRUE;
				//
				// Even if we are in ERROR_STOP_FAILED,
				// we have to run the state machine on an
				// SN affinitent that is waiting for RG
				// to go offline.
				// rgm_run_state() will discover the error
				// state when it calls compute_sn_violations().
				//
				run_state_on_sn_affinitees(RG, lni);
				break;

			case rgm::RG_ON_PENDING_METHODS:
				//
				// The RG is online, but has init, fini, or
				// update methods pending.  Also, some
				// resources might have triggered restarts
				// pending; these are deferred until the
				// init/fini/update methods have completed.
				//
				if (!all_methods) {
					ucmm_print("RGM",
					    NOGET("YYYYYYY on NOT "
					    "all_methods\n"));
					break;
				}

				//
				// At this point all init, fini, update methods
				// have completed.  The RG must change state.
				//

				//
				// check our postponed_stop flag.
				// If it's set, go ahead and jump
				// directly to PENDING_OFFLINE.
				//
				if (RG->rgl_xstate[lni].
				    rgx_postponed_stop) {
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_OFFLINE);
					RG->rgl_xstate[lni].
					    rgx_postponed_stop = B_FALSE;
					ucmm_print("RGM", NOGET(
					    "Jumping from "
					    "ON_PENDING_METHODS to "
					    "PENDING_OFFLINE.\n"));

				//
				// The pres could have snuck in and
				// told us that one of our resources
				// is ok_to_start.  We have delayed bringing
				// the resource online in that case until we
				// finished running methods, so we have to
				// move to PENDING_ONLINE to process
				// it now.  It's OK if there are restarting
				// resources -- those are handled properly
				// in pending_online rg state.
				//
				} else if (any_ok_start) {
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_ONLINE);

				// check for resource restarts
				} else if (any_restart) {
					set_rgx_state(lni, RG,
					    rgm::RG_ON_PENDING_R_RESTART);

				//
				// Check for error_stop_failed case.
				// This should not normally occur, but
				// there is a code path for it in
				// process_resource() so we have to
				// check for it here before moving to a
				// terminal state.
				//
				} else if (any_stop_failed) {
					set_rgx_state(lni, RG,
					    rgm::RG_ERROR_STOP_FAILED);
					need_wake = B_TRUE;
				} else if (any_starting_depend) {
					//
					// In this case we have one
					// or more resources blocked
					// waiting for deps to be
					// fulfilled, so we move to
					// PENDING_ONLINE_BLOCKED.
					//
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_ONLINE_BLOCKED);
					need_wake = B_TRUE;
				} else {
					//
					// The "normal" case: no
					// resources waiting for restart
					// or dependencies or waiting
					// to come online after filling
					// dependencies.
					//
					set_rgx_state(lni, RG,
					    rgm::RG_ONLINE);
					need_wake = B_TRUE;
				}
				r_state_changed = B_TRUE;
				break;

			case rgm::RG_OFF_PENDING_METHODS:
				//
				// The RG is offline, but has init or fini
				// methods pending.
				//
				if (all_methods) {
					set_rgx_state(lni, RG,
					    rgm::RG_OFFLINE);
					r_state_changed = B_TRUE;
					need_wake = B_TRUE;
				} else {
					ucmm_print("RGM",
					    NOGET("YYYYYYY off NOT "
					    "all_methods\n"));
				}
				break;

			case rgm::RG_OFF_PENDING_BOOT:
				//
				// The RG is offline, but has boot methods
				// pending.
				//
				if (all_methods) {
					set_rgx_state(lni, RG,
					    rgm::RG_OFF_BOOTED);
					r_state_changed = B_TRUE;
					need_wake = B_TRUE;
				} else {
					ucmm_print("RGM",
					    NOGET("YYYYYYY off NOT all "
					    "booted\n"));
				}
				break;

			case rgm::RG_ON_PENDING_MON_DISABLED:
				//
				// The RG is online, but one or more Rs in RG
				// are having their monitor turned off.
				// Resources may also be restarting.  If
				// all_dis_off is true, the monitor_stop method
				// has finished running on all resources
				// with mon disabled.  Note that monitor_stop
				// is never postponed for dependencies.
				// If any_restart is true, we are still
				// restarting some resources.
				//
				if (!all_dis_off) {
					// Some Rs are still stopping monitors

					// for debugging; can delete later:
					CL_PANIC(!all_methods);

					break;
				}

				// all_dis_off is true; RG state must change

				//
				// The pres could have snuck in and told us
				// that one of our resources is ok_to_start.
				// We delay bringing the resource online in
				// that case until we have finished disabling
				// monitoring, so we have to move to
				// PENDING_ONLINE to process it now.
				//
				// Note that restarting resources are handled
				// properly in PENDING_ONLINE rg state.
				//
				if (any_ok_start) {
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_ONLINE);
				} else if (any_restart) {
					set_rgx_state(lni, RG,
					    rgm::RG_ON_PENDING_R_RESTART);

				//
				// Before moving into a terminal state, check
				// if stop failure occurred.
				//
				} else if (any_stop_failed) {
					set_rgx_state(lni, RG,
					    rgm::RG_ERROR_STOP_FAILED);
					need_wake = B_TRUE;
					// for debugging; can delete later:
					CL_PANIC(all_methods);
				} else if (any_starting_depend) {
					//
					// In this case we have one or more
					// resources blocked waiting for deps
					// to be fulfilled, so we move to
					// PENDING_ONLINE_BLOCKED.
					//
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_ONLINE_BLOCKED);
					need_wake = B_TRUE;
					// for debugging; can delete later:
					CL_PANIC(all_methods);
				} else {
					//
					// The "normal" case: no resources
					// waiting for dependencies or waiting
					// to come online after filling
					// dependencies.
					//
					set_rgx_state(lni, RG,
					    rgm::RG_ONLINE);
					need_wake = B_TRUE;
					// for debugging; can delete later:
					CL_PANIC(all_methods);
				}
				r_state_changed = B_TRUE;
				break;

			case rgm::RG_ON_PENDING_DISABLED:
				//
				// The RG is online, but one or more Rs in RG
				// have been disabled and need to be stopped.
				// Resources may also be restarting.  If
				// all_dis_off is true, we have finished
				// stopping all disabled resources.
				// If any_restart is true, we are still
				// restarting some resources.
				//
				if (!all_dis_off) {
					// Some disabled Rs are still stopping
					break;
				}

				// all_dis_off is true; RG state must change

				//
				// The pres could have snuck in and told us
				// that one of our resources is ok_to_start.
				// We delay bringing the resource online in
				// that case until we have finished disabling
				// monitoring, so we have to move to
				// PENDING_ONLINE to process it now.
				//
				// Also, if any_prenet_started is set to TRUE,
				// it means that one or more resources in the
				// RG was stuck in PRENET_STARTED waiting for
				// net-relative dependencies.  We move to
				// PENDING_ONLINE to see if any of those deps
				// were resolved by the disabling of one or
				// more resources.
				//
				// Note that restarting resources are handled
				// properly in PENDING_ONLINE rg state.
				//
				if (any_ok_start || any_prenet_started) {
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_ONLINE);
				} else if (any_restart) {
					set_rgx_state(lni, RG,
					    rgm::RG_ON_PENDING_R_RESTART);

				//
				// Before moving into a terminal state, check
				// if stop failure occurred.
				//
				} else if (any_stop_failed) {
					set_rgx_state(lni, RG,
					    rgm::RG_ERROR_STOP_FAILED);
					need_wake = B_TRUE;
					// for debugging; can delete later:
					CL_PANIC(all_methods);
				} else if (any_starting_depend) {
					//
					// In this case we have one or more
					// resources blocked waiting for deps
					// to be fulfilled, so we move to
					// PENDING_ONLINE_BLOCKED.
					//
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_ONLINE_BLOCKED);
					need_wake = B_TRUE;
					// for debugging; can delete later:
					CL_PANIC(all_methods);
				} else {
					//
					// The "normal" case: no resources
					// waiting for dependencies or waiting
					// to come online after filling
					// dependencies.
					//
					set_rgx_state(lni, RG,
					    rgm::RG_ONLINE);
					need_wake = B_TRUE;
					// for debugging; can delete later:
					CL_PANIC(all_methods);
				}
				r_state_changed = B_TRUE;
				break;

			case rgm::RG_ON_PENDING_R_RESTART:
				ucmm_print("RGM", NOGET(
				    "run_state: RG ON PENDING R RESTART"));

				//
				// The pres could have snuck in and
				// told us that one of our resources
				// is ok_to_start.  We can immediately move
				// to PENDING_ONLINE state because restarting
				// resources are handled properly in that rg
				// state.
				//
				if (any_ok_start) {
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_ONLINE);
					r_state_changed = B_TRUE;
					break;
				}

				if (any_restart) {
					// Some R is still restarting
					if (!all_methods) {
						// methods are still in flight
						break;
					}
					//
					// Some R is still restarting, but no
					// methods are in flight.  This implies
					// that a restarting resource is stuck
					// offline because it has a strong
					// dependency on a start_failed R.
					//
					// We don't syslog a message here,
					// because this can be a transitory
					// state in which we are blocked by
					// an inter-RG dependency and then
					// immediately become unblocked.
					//
					// If the situation persists, the RG
					// will end up e.g. in ONLINE_FAULTED
					// or PENDING_ONLINE_BLOCKED state, and
					// the president will make the failure
					// visible to the user.
					//
				}

				//
				// (!any_restart || all_methods) is true.
				// All Rs are finished restarting or can make
				// no further progress.  RG state must change.
				//

				//
				// Before moving into a terminal state, check
				// if stop failure occurred.
				//
				if (any_stop_failed) {
					set_rgx_state(lni, RG,
					    rgm::RG_ERROR_STOP_FAILED);

				} else if (any_starting_depend) {
					//
					// In this case we have one
					// or more resources blocked
					// waiting for deps to be
					// fulfilled, so we move to
					// PENDING_ONLINE_BLOCKED.
					//
					set_rgx_state(lni, RG,
					    rgm::RG_PENDING_ONLINE_BLOCKED);
				} else {
					//
					// The "normal" case: no
					// resources waiting for
					// dependencies or waiting
					// to come online after filling
					// dependencies.
					//
					set_rgx_state(lni, RG,
					    rgm::RG_ONLINE);
				}
				need_wake = B_TRUE;
				r_state_changed = B_TRUE;
				break;

			case rgm::RG_OFFLINE_START_FAILED:
			case rgm::RG_ERROR_STOP_FAILED:
			case rgm::RG_OFFLINE:
			case rgm::RG_OFF_BOOTED:
			case rgm::RG_ONLINE:
			case rgm::RG_PENDING_ONLINE_BLOCKED:
				ucmm_print("RGM", NOGET(
				    "rgm_run_state: resource group <%s> "
				    "achieved endish state <%s>\n"),
				    RG->rgl_ccr->rg_name,
				    pr_rgstate(
				    RG->rgl_xstate[lni].rgx_state));
				//
				// xxx Possible optimization:
				// xxx at this point, we could remove RG from
				// xxx work_list (by memmove-ing higher entries
				// xxx down) and decrement rgindex.
				//
				break;

			default:
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT("fatal: %s: internal error"
				    ": bad state <%d> for resource group <%s>"),
				    "rgm_run_state",
				    rglp->rgl_xstate[lni].rgx_state,
				    rglp->rgl_ccr->rg_name);
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_INTERNAL;
				need_wake = B_TRUE;
				goto finish;
			} // end switch (rg state)
		} // end for (each RG in work list)
	} // end while r_state_changed
	// r_state_changed is now FALSE; there is no more work to do
finish:
	if (need_wake) {
		ucmm_print("RGM",
		    NOGET("rgm_run_state:  finished, waking president\n"));
		(void) wake_president(lni);
	} else {
		ucmm_print("RGM", NOGET(
		    "rgm_run_state:  finished, no need to wake president\n"));
	}
	// bad RG state or an error in process_resource() is fatal...
	CL_PANIC(res.err_code == SCHA_ERR_NOERR);
	free(work_list);
	return (res);
}


//
// process_zone_death
//
// This function is called on the slave side, when a zone has died while
// the slave physical node itself remains up and in the cluster membership.
//
// The caller of this function (on the slave) must hold the rgm state lock.
// The following actions must be taken while holding the state lock:
//
//	- The caller must set the zone status to DOWN *before* calling
//	  this function.
//
//	- The caller (if not the president) must notify the president of
//	  the zone being down.  It should not matter if this is done before
//	  or after calling this function.
//
// When a zone dies while its physical node stays up, we must execute
// stopping methods for resources that were online in the zone and which
// are of a type (such as LogicalHostname or HAStoragePlus) that executes
// its methods in the global zone.
//
// We also have to set all RGs (whether or not their resources' methods execute
// in the global zone) to OFFLINE state so that they can be rebalanced
// onto other nodes.
//
// We accomplish both objectives by setting all RGs that were not offline
// in the zone to PENDING_OFFLINE and running the state machine.  For
// resources whose methods actually execute in the local zone, the state
// machine will note that the zone is down and immediately set the
// resource state/status to OFFLINE.  We do this because the zone
// being down implies all processes have exited, which is considered to be
// equivalent to the resource and its monitor having "stopped".
//
// For resources whose methods execute in the global zone, the state machine
// will actually execute the methods.
//
// When the RG goes OFFLINE, the president will call rebalance on the RG.
//
// If one of these global-zone non-well-behaved stopping methods fails,
// we treat it the same as a method failure in the global zone.  I.e., the
// resource will move to STOP_FAILED state even though the local zone is down.
// If Failover_mode is SOFT, the RG will move to ERROR_STOP_FAILED.
// If Failover_mode is HARD, we cannot reboot the zone -- it's already dead.
// We reboot the physical node in that case.
// These stopping methods (LogicalHostname, HAStoragePlus, etc.) need to be
// coded as robustly as possible to avoid failures of these methods.
//
void
process_zone_death(rgm::lni_t lniDeadZone)
{
	rglist_t *rglp;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	bool need_to_run_state_machine = false;

	ucmm_print("RGM",
	    NOGET("process_zone_death: lni %d"), lniDeadZone);

	// Iterate over all resource groups
	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		//
		// Skip RGs that are already OFFLINE on the zone, but reset
		// their per-zone state.  This also skips RGs that are not
		// configured to run in the zone.
		//
		if (rglp->rgl_xstate[lniDeadZone].rgx_state ==
		    rgm::RG_OFFLINE) {
			reset_rg_per_zone_state(rglp, lniDeadZone);
			continue;
		}
		need_to_run_state_machine = true;

		//
		// Set the RG to PENDING_OFFLINE on the zone.
		// process_resource() has been modified to correctly
		// take the RG offline, regardless of what its prior
		// state was.
		//
		set_rgx_state(lniDeadZone, rglp, rgm::RG_PENDING_OFFLINE);
	}

	//
	// If any RGs are not offline yet in the dead zone, run the state
	// machine on that zone, in the current thread.  This allows
	// us to set well-behaved resources offline and begin
	// running global zone stopping methods on non-well-behaved
	// resources, before the state lock is released.
	//
	if (need_to_run_state_machine) {
		ucmm_print("RGM",
		    NOGET("process_zone_death running state machine on lni %d"),
		    lniDeadZone);
		res = rgm_run_state(lniDeadZone);
		if (res.err_code != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// An error has occurred on this node while attempting
			// to execute the rgmd state machine.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing the
			// problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("RGM state machine returned error %d"),
			    res.err_code);
			sc_syslog_msg_done(&handle);
		}
	}
}


//
// reset_rg_per_zone_state
//
// When a zone has died and after the RG has gone offline on that zone, we
// reset the per-zone state (rgl_xstate) of that RG and its resources
// (rl_xstate), on the dead LNI.  This is called on the slave side;
// the slave might also be the president.
//
// The caller must hold the RGM state lock.
//
void
reset_rg_per_zone_state(rglist_p_t rgp, rgm::lni_t lniDeadZone)
{
	// Reset per-zone state for the RG.
	if (rgp->rgl_xstate.count(lniDeadZone) > 0) {
		//
		// Note, rgx_state should already be OFFLINE, but we set
		// it again just to be sure.
		//
		// We do not reset rgx_start_fail -- it is used by rebalance
		// only on the president side, is not used on the slave,
		// and is supposed to persist through slave reboots.
		//
		// We reset rgx_postponed_boot -- if the zone is dead, then
		// there is no longer a need to run Boot methods until it
		// comes up again.
		//
		rgp->rgl_xstate[lniDeadZone].rgx_state = rgm::RG_OFFLINE;
		rgp->rgl_xstate[lniDeadZone].rgx_net_relative_restart = B_FALSE;
		rgp->rgl_xstate[lniDeadZone].rgx_postponed_stop = B_FALSE;
		rgp->rgl_xstate[lniDeadZone].rgx_postponed_boot = B_FALSE;
	}

	//
	// Reset per-zone state for each resource in RG.
	// There is currently no per-zone resource state that has to be kept
	// across node reboots, so we just erase the map element.
	// The ctor will reinitialize it the next time it is accessed.
	//
	for (rlist_p_t rp = rgp->rgl_resources; rp != NULL;
	    rp = rp->rl_next) {
		if (rp->rl_xstate.count(lniDeadZone) > 0) {
			rp->rl_xstate.erase(lniDeadZone);
		}
	}
}
