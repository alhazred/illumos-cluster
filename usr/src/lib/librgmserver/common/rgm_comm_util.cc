/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_comm_util.cc	1.30	08/10/29 SMI"


#include <h/naming.h>
#include <nslib/ns.h>
#include <rgm/rgm_comm_impl.h>
#include <rgm_proto.h>
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <ucontext.h>
#include <cmm/cmm_ns.h>
#include <h/membership.h>
#include <cmm/membership_client.h>


//
// Object references for cmm rgm_comm objects are
// kept in this array
// MAXNODES plus one because nodeids go from 1 to MAXNODES,
// not 0 to MAXNODES-1
//
static rgm::rgm_comm_ptr rgm_comm_list[MAXNODES+1];

// This mutex protects the above array from being messed with by more then
// one thread of control at any time
static os::mutex_t rgm_comm_lock;

// The rgm comm object for this rgmd
rgm_comm_impl *p_rgm_impl;

// Get a reference to the rgm_comm object on a given node
rgm::rgm_comm_ptr
rgm_comm_getref(sol::nodeid_t nid)
{
	sc_syslog_msg_handle_t handle;

	rgm::rgm_comm_ptr remote_rgm;

	remote_rgm = rgm::rgm_comm::_nil();

	if ((nid < 1) || (nid > MAXNODES)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The low-level cluster machinery has encountered an error.
		// @user_action
		// Look for other syslog messages occurring just before or
		// after this one on the same node; they may provide a
		// description of the error.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("node id <%d> is out of range"),
		    nid);
		sc_syslog_msg_done(&handle);
		return (remote_rgm); // NULL
	}

	rgm_comm_lock.lock();
	if (!CORBA::is_nil(rgm_comm_list[nid])) {
		//
		// Since we are giving out the reference to the caller,
		// we must call _duplicate() to keep our reference around
		// after returning from this call.
		//
		remote_rgm = rgm::rgm_comm::_duplicate(rgm_comm_list[nid]);
		rgm_comm_lock.unlock();
		return (remote_rgm);
	}
	rgm_comm_lock.unlock();

	//
	// We don't have a reference for the remote node. It may
	// have died after the caller last checked for membership.
	//
	return (remote_rgm);
}

//
// Instantiate the rgm_comm object and bind it to the nameserver.
// Called once from startrgmd() during initialization.
//

void
rgm_comm_init()
{
	sc_syslog_msg_handle_t handle;

	sol::nodeid_t i, nid;
	// char nametag[32]; // The name of the object in name server
	char nametag[MAX_COMM_NAMETAG];

	p_rgm_impl = new rgm_comm_impl(); // Create a new object

	//
	// The use of rgm_comm_var rather than rgm_comm_ptr eliminates
	// the need to call CORBA::release when we're finished.
	// This is simply a matter of style preference.
	//
	rgm::rgm_comm_var rgm_comm_server_obj_var = p_rgm_impl->get_objref();

	naming::naming_context_var ctxp;
	Environment e;

	nid = orb_conf::local_nodeid();

	// Generate the name that will be bound in nameserver
	(void) sprintf(nametag, "RGM_COMM_SERVER_%d", nid);
	// Get a pointer to the global nameserver object
#if (SOL_VERSION >= __s10)
	if (process_membership_support_available()) {
		ctxp = ns::root_nameserver_cz(ZONE);
	} else {
		ctxp = ns::root_nameserver();
	}
#else
	ctxp = ns::root_nameserver();
#endif
	if (CORBA::is_nil(ctxp)) {
		//
		// Need to decide what to do for
		// release code. For debug code, assert
		//
		ASSERT(0);
	}

	// bind the new object with the nametag
	ctxp->rebind(nametag, rgm_comm_server_obj_var, e);
	if (e.exception() != NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The low-level cluster machinery has encountered a fatal
		// error. The rgmd will produce a core file and will cause the
		// node to halt or reboot to avoid the possibility of data
		// corruption.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes,
		// and of the rgmd core file. Contact your authorized Sun
		// service provider for assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: Unable to bind to nameserver"));
		sc_syslog_msg_done(&handle);
		abort();
	}

	rgm_comm_lock.lock();

	// Initialize our list of objrefs with NULLs
	for (i = 1; i <= MAXNODES; i++) {
		rgm_comm_list[i] = rgm::rgm_comm::_nil();
	}
	// Record our own reference.
	rgm_comm_list[nid] = rgm::rgm_comm::_duplicate(rgm_comm_server_obj_var);

	rgm_comm_lock.unlock();
}

//
// void rgm_comm_update(nodeset_t newset): Updates the communication setup
// with the list of nodes. The object references for nodes that have left
// the cluster are cleaned up and nodes that have joined the cluster are
// resolved from the nameserver and kept in a global array
// (rgm_comm_list[]).
//
// Called from cmm callback rgm_reconfig() in rgm_president.cc
// whenever there is a change in the cmm membership.
//
// It is possible that the death and reincarnation of a node may be
// reported by the UCMM in the same reconfiguration. This can happen if
// the node reboots very quickly or if the ORB is slow is delivering the
// UCMM's reconfiguration messages. Due to this, this function gets
// fresh references for the remote nodes each time. This is unnecessary
// if we can tell that the node has not reincarnated.
//

#ifndef EUROPA_FARM
#include <nslib/ns_interface.h>
#endif
void
rgm_comm_update(nodeset_t newset)
{
	sol::nodeid_t i;
	char nametag[32];
	rgm::rgm_comm_ptr prgm_serv;
	naming::naming_context_var ctxp;
	Environment e;
	CORBA::Object_ptr obj_p = rgm::rgm_comm::_nil();
	sc_syslog_msg_handle_t handle;
	nodeset_t cluster_nodeset;

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");

#if (SOL_VERSION >= __s10)
	LogicalNodeset mem_nodeset;
	if (process_membership_support_available()) {
		// Query physical cluster membership.
		CORBA::Exception *exp;
		cmm::membership_t membership;
		cmm::seqnum_t seqnum;

		membership::api_var membership_api_v =
		    cmm_ns::get_membership_api_ref();
		if (CORBA::is_nil(membership_api_v)) {
			abort();
		}

		membership_api_v->get_cluster_membership(
		    membership, seqnum, GLOBAL_ZONENAME, e);
		exp = e.exception();
		if (exp != NULL) {
			abort();
		}
		for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
			if (membership.members[nid] != INCN_UNKNOWN) {
				mem_nodeset.addLni(nid);
			}
		}
	}

	// If this rgmd is running for a zone cluster, then we have
	// to update the comm structures for all the rgmds in the
	// physical cluster membership (since we support proxy rgmds
	// on nodes where the ZCis not configured). If this rgmd is
	// running for the global zone, then we need to update the
	// comm structures for rgmds wchich are in the membership.
	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		cluster_nodeset = mem_nodeset.getNodeset();
	} else {
		cluster_nodeset = newset;
	}
#else
	cluster_nodeset = newset;
#endif

	// Get a reference to the root nameserver
#if SOL_VERSION >= __s10
	boolean_t multiple_rgmd = process_membership_support_available();
	if (multiple_rgmd) {
		ctxp = ns::root_nameserver_cz(ZONE);
	} else {
		ctxp = ns::root_nameserver();
	}
#else
	ctxp = ns::root_nameserver();
#endif
	rgm_comm_lock.lock();
	for (i = 1; i <= MAXNODES; i++) {
		if (i == orb_conf::local_nodeid())
			continue; // no need to look up local node

		if (!CORBA::is_nil(rgm_comm_list[i])) {
			CORBA::release(rgm_comm_list[i]);
			rgm_comm_list[i] = rgm::rgm_comm::_nil();
		}
		if (nodeset_contains(cluster_nodeset, i)) {
			//
			// The node is a cluster member. It should have
			// registered its name (RGM_COMM_SERVER_nid) with
			// the root name server before beginning
			// communication with other nodes.
			//
			(void) sprintf(nametag, "RGM_COMM_SERVER_%d", i);
			obj_p = ctxp->resolve(nametag, e);
			if (e.exception() != NULL) {
				//
				// The value of obj_p is junk because there
				// was an exception. Set it to a valid
				// value.
				//
				obj_p = rgm::rgm_comm::_nil();

				// If this rgmd is running for a zone cluster,
				// it is highly possible that we might hit
				// this exception for the first time when
				// the node boots up. This is because for the
				// first rgmd which comes up, the rest of the
				// rgmds on other nodes might still be coming
				// or the proxy rgmds might not have started
				// yet. So, we will ignore this exception if
				// this rgmd was running for a zone cluster
				// and the peer rgmd is not present in the
				// current membership.
				if ((strcmp(ZONE, GLOBAL_ZONENAME) != 0) &&
					(!nodeset_contains(newset, i))) {
					continue;
				}

				// For a zone cluster, it is possible that
				// the peer rgmd has binded itself with the
				// nameserver, but the resolve for that rgmd
				// is failing on remote nodes. Please see
				// CR 6730227 for more information how this
				// condition might occur. Hence, for a zone
				// cluster rgmd, we will wait for sometime
				// and then resolve for the peer rgmd comm
				// if the peer rgmd is in the cluster
				// membership. We will do this until we get
				// a reference to the peer rgmd. Note that
				// this might be a dangerous thing to do,
				// but not having this fix causes a lot of
				// other failure scenarios.
				if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
					// If we are here, it means this
					// rgmd is running for a zone clu-
					// ster and that the peer rgmd is
					// in current cluster membership.
					while (CORBA::is_nil(obj_p)) {
						// Do a look-up till we
						// find a reference.
						e.clear();
						obj_p = ctxp->resolve(nametag,
									e);
						if (e.exception() != NULL) {
							ucmm_print(
							    "rgm_comm_update",
							    NOGET("did not "
							    "resolve for %s"
							    " in %s"), nametag,
							    ZONE);
							//
							// The value of obj_p
							// is junk because
							// there was an
							// exception. Set it
							// to a valid value.
							//
							obj_p = rgm::rgm_comm\
							    ::_nil();

							// do the look-up
							// after one second
							sleep(1);
						} else {
							ASSERT(!CORBA::is_nil(
							    obj_p));
							ucmm_print(
							    "rgm_comm_update",
							    NOGET("got refer"
							    "ence for %s in"
							    "%s"), nametag,
							    ZONE);
						}
					}
					// If we are here, it means we
					// were able to lokk-up the peer
					// rgmd.
					goto peer_resolved;
				}

				//
				// SCMSGS
				// @explanation
				// The low-level cluster machinery has
				// encountered a fatal error. The rgmd will
				// produce a core file and will cause the node
				// to halt or reboot to avoid the possibility
				// of data corruption.
				// @user_action
				// Save a copy of the /var/adm/messages files
				// on all nodes, and of the rgmd core file.
				// Contact your authorized Sun service
				// provider for assistance in diagnosing the
				// problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT("fatal: Unable to "
				    "resolve %s from nameserver"), nametag);
				sc_syslog_msg_done(&handle);
				abort();
				// NOTREACHABLE
			}
peer_resolved:
			// Check if the object type is the one we expect.
			prgm_serv = rgm::rgm_comm::_narrow(obj_p);
			CORBA::release(obj_p);
			obj_p = rgm::rgm_comm::_nil();
			if (CORBA::is_nil(prgm_serv)) {
				//
				// Some other object is bound to the
				// RGM_COMM_SERVER_nid tag.
				//

				//
				// SCMSGS
				// @explanation
				// The low-level cluster machinery has
				// encountered a fatal error. The rgmd will
				// produce a core file and will cause the node
				// to halt or reboot to avoid the possibility
				// of data corruption.
				// @user_action
				// Save a copy of the /var/adm/messages files
				// on all nodes, and of the rgmd core file.
				// Contact your authorized Sun service
				// provider for assistance in diagnosing the
				// problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT(
				    "fatal: Unknown object type "
				    "bound to %s"), nametag);
				sc_syslog_msg_done(&handle);
				abort();
				// NOTREACHABLE
			}
			// Store it in our reference list.
			rgm_comm_list[i] = rgm::rgm_comm::_duplicate(
			    prgm_serv);
			CORBA::release(prgm_serv);
		}
	}
	rgm_comm_lock.unlock();
	sc_syslog_msg_done(&handle);
}


//
// rgm_comm_waitforpres
// This function is called when either
//	1) the current president pres_p is equal to Corba NIL
//	   (indicating that the cluster recently booted and the
//	   president has not yet registered),
// or
//	2) the current president pres_p has died.
//
// Sleep in a loop until the new president has registered, and return a
// reference to the new president.
// Caller must *not* hold the RGM state mutex.
//
rgm::rgm_comm_ptr
rgm_comm_waitforpres(rgm::rgm_comm_ptr pres_p)
{
	rgm::rgm_comm_ptr newpres_p;

	while (1) {

		if (allow_inter_cluster())
			newpres_p = rgm_comm_getpres_zc(ZONE);
		else
			newpres_p = rgm_comm_getpres();
		if (!CORBA::is_nil(newpres_p)) {
			if (CORBA::is_nil(pres_p) ||
			    !newpres_p->_equiv(pres_p)) {
				// newpres_p is a reference to a newly-elected
				// president -- we're done
				return (newpres_p);
			}
			// newpres_p == pres_p, so release it
			CORBA::release(newpres_p);
		}

		//
		// Either newpres_p is nil, or newpres_p was equal to pres_p.
		// In either case, we have not yet elected the new president.
		// Sleep and try again.
		//
		(void) sleep(1);
	}
}
