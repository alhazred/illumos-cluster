//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

//
// rgm_global_res_used.cc - functions to implement Global_resources_used
//	Includes implementation of service_state_callback_impl class.
//

#pragma ident	"@(#)rgm_global_res_used.cc	1.30	08/11/02 SMI"

#include <sys/os.h>
#include <rgm_global_res_used.h>
#include <nslib/ns.h>
#include <rgm_state.h>
#include <rgm_api.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm/rgm_msg.h>
#include <rgm_proto.h>
#include <rgm/fecl.h>
#include <scha_types.h>
#include <unistd.h>

// Use this to check that we create only one object of this class
service_state_callback_impl
	*service_state_callback_impl::the_service_state_callback_implp = NULL;

// Pointer to a list of names of services that are currently in recovery.
rm_service_list_t service_state_callback_impl::frozen_services = {NULL};

// The number of elements in frozen_services
uint_t service_state_callback_impl::num_frozen = 0;

// XXX RFE 4248997 : also track unfrozen services.

//
// Condition variable signalled by RMA callback functions and by
// president code that edits RGs or switches RGs out of the UNMANAGED state.
// Wakes rgfreeze() to update RG freeze states.
//
os::condvar_t cond_rgfreeze;


service_state_callback_impl::service_state_callback_impl()
{
	CL_PANIC(the_service_state_callback_implp == NULL);
	the_service_state_callback_implp = this;
	frozen_services = NULL;
	num_frozen = 0;
}


//
// namearray_add
// If aname is not in *nlist, add it (allocating memory) and
// increment *numelem.  This function always "succeeds", i.e.
// if aname is already in *nlist, log a message and return.
// The rgm state lock is held by the caller.
//
static void
namearray_add(
	rm_service_list_t *nlist,	// ptr to null-terminated array of
					//  (char *)
	uint_t *numelem,		// # of elements in *nlist
	const char *aname)		// the name to add
{
	rm_service_list_t newlist;
	uint_t i;

	ucmm_print("namearray_add", NOGET("adding <%s>\n"), aname);

	// Sanity check *nlist and *numelem; allocate or reallocate memory
	if (*nlist != NULL) {
		// *nlist has already been malloc'd
		for (i = 0; (*nlist)[i] != NULL; i++) {
			if (strcmp((*nlist)[i], aname) == 0) {
				// aname already in list
				ucmm_print("namearray_add",
				    NOGET("<%s> already in list\n"), aname);
				return;
			}
		}
		CL_PANIC(i == *numelem);

		// allocate space for new entry
		newlist = (rm_service_list_t)realloc_err(*nlist,
		    (i + 2) * sizeof (char *));
		*nlist = newlist;
	} else {
		// *nlist is NULL, i.e., has not been malloc'd yet
		CL_PANIC(*numelem == 0);
		*nlist = (rm_service_list_t)calloc(2, sizeof (char *));
		i = 0;
	}

	// Store copy of aname as i'th entry of *nlist
	(*nlist)[i] = strdup(aname);
	(*nlist)[i + 1] = NULL;
	(*numelem)++;
}


//
// namearray_delete
// If dname is in *nlist, delete it and decrement *numelem.
// Don't reallocate memory but shift entries in the array.
// If dname is not in *nlist, log a message and return.
// If dname appears more than once in *nlist, log an error message and
//  delete all instances of it.
// The rgm state lock is held by the caller.
//
static void
namearray_delete(
	rm_service_list_t *nlist,	// ptr to null-terminated array of
					//  (char *)
	uint_t *numelem,		// # of elements in *nlist
	const char *dname)		// The name to delete
{
	uint_t i;
	uint_t numdel = 0;	// number of elements deleted

	ucmm_print("namearray_delete", NOGET("deleting <%s>\n"), dname);

	if (*nlist == NULL || dname == NULL) {
		ucmm_print("namearray_delete",
		    NOGET("*nlist or dname is null\n"));
		return;
	}

	for (i = 0; (*nlist)[i] != NULL; i++) {
		if (strcmp((*nlist)[i], dname) == 0) {
			// found dname in list-- free it, and delete it by
			// shifting all the following entries back by one.
			free((*nlist)[i]);
			for (uint_t j = i; (*nlist)[j] != NULL; j++) {
				(*nlist)[j] = (*nlist)[j + 1];
			}
			(*numelem)--;
			i--;
			numdel++;
		}
		// Note, if there are multiple entries containing dname
		// (though this should never happen), we'll delete all of them.
	}

	// Perform sanity checks before returning
	CL_PANIC(i == *numelem);
	if (numdel == 0) {
		ucmm_print("namearray_delete",
		    NOGET("<%s> was not in list\n"),
		    dname);
	} else if (numdel > 1) {
		ucmm_print("namearray_delete", NOGET(
		    "<%s> appeared %d times in list\n"),
		    dname, numdel);
	}
}


//
// do_svc_state_change
// Add or delete service s_name from the frozen list depending on
// its state s_state.
// Called by the two callback functions (init_services and state_changed).
// For a zone cluster rgmd, this method might be called on the current
// president and on a slave rgmd who was president at some point in time.
// We have to ignore the callback if we are not the current president.
//
void
service_state_callback_impl::do_svc_state_change(
	const char			*s_name,
	const replica::service_state	s_state)
{
	static const char *devsvc = DEVSVC;
	static const char *pxfssvc = PXFSSVC;
	sol::nodeid_t mynodeid;

	mynodeid = orb_conf::local_nodeid();
	// If I am not the rgm president, I should not process this callback
	if (Rgm_state->rgm_President != mynodeid) {
		return;
	}

	// First, filter out any service that is neither a device service nor
	// a pxfs file system service.
	if (strncmp(s_name, devsvc, strlen(devsvc)) != 0 &&
	    strncmp(s_name, pxfssvc, strlen(pxfssvc)) != 0) {
		ucmm_print("do_svc_state_change", NOGET("skipping <%s>\n"),
		    s_name);
		return;
	}

	switch (s_state) {
	case replica::S_UP:
		// Service is up-- make sure it's not in freeze list.
		namearray_delete(&frozen_services, &num_frozen, s_name);

		//
		// XXX RFE 4248997: also maintain unfrozen_services list
		//
		break;

	case replica::S_DOWN:
		// service is down (being deleted)
	case replica::S_UNINIT:
		// service is uninitialized (registered but not functional yet)

		// Treat these services as non-existent; make sure they're
		// not in the freeze list.
		namearray_delete(&frozen_services, &num_frozen, s_name);

		//
		// XXX RFE 4248997: report an error (but do not
		// XXX freeze the RG) if an RG depends on a
		// XXX non-existent service.
		//
		break;

	case replica::S_UNSTABLE:
		// The service is unstable (i.e., frozen).
		// Add it to freeze list.
		namearray_add(&frozen_services, &num_frozen, s_name);
		break;

	default:
		ucmm_print("do_svc_state_change",
		    NOGET("ERROR: invalid s_state <%d>\n"),
		    s_state);
		break;
	}
}


//
// service_state_callback_impl::init_services()
// This callback from RMA to rgmd occurs once, soon after the rgmd first
// registers for callbacks.  This provides the current state of all services
// to the rgmd.
//
// Services that are currently frozen are placed on the static list
// service_state_callback_impl::frozen_services.  Others are ignored.
//
// Signal the cond_rgfreeze condition variable so that RG freeze states
// will be updated.
//
// If any error is encountered, syslog it but keep going.
//
// This function should only be called back once by the RMA.  But if
// it is called more than once for some reason, just go ahead and
// process the service state information again.
//
void
service_state_callback_impl::init_services(
	const replica::service_state_info_list& ssi_list,
	Environment &)
{
	uint_t len = ssi_list.length();
	boolean_t any_frozen_now;
	sol::nodeid_t mynodeid;

	ucmm_print("init_services", NOGET("RMA callback received\n"));
	mynodeid = orb_conf::local_nodeid();
	// If I am not the rgm president, I should not process this callback
	if (Rgm_state->rgm_President != mynodeid) {
		return;
	}

	// If there are no services in ssi_list, we have no work to do
	if (len == 0) {
		return;
	}

	rgm_lock_state();

	any_frozen_now = is_any_service_frozen();

	for (uint_t i = 0; i < len; i ++) {
		ucmm_print("init_services",
		    NOGET("svc <%s> state <%d>\n"),
		    (const char *)ssi_list[i].service_name,
		    ssi_list[i].s_state);
		do_svc_state_change((const char *)ssi_list[i].service_name,
		    ssi_list[i].s_state);
	}

	// Wake rgfreeze to update RG freeze states
	//
	// XXX RFE 4248997 : do the cond_signal unconditionally.
	if (any_frozen_now != is_any_service_frozen()) {
		cond_rgfreeze.signal();
	}

	rgm_unlock_state();
}


//
// service_state_callback_impl::state_changed()
// This callback from RMA tells rgmd that a given service has changed state.
//
// If the service has become frozen, add it to the frozen_services list.
// If the service has un-frozen, remove it from the frozen_services list.
//
// If any error is encountered, syslog it but keep going.
//
void
service_state_callback_impl::state_changed(
	const replica::service_state_info& ssi,
	Environment &)
{
	boolean_t any_frozen_now;
	sol::nodeid_t mynodeid;

	mynodeid = orb_conf::local_nodeid();
	// If I am not the rgm president, I should not process this callback
	if (Rgm_state->rgm_President != mynodeid) {
		return;
	}

	ucmm_print("state_changed", NOGET(
	    "RMA callback received, svc <%s> state <%d>\n"),
	    (const char *)ssi.service_name, ssi.s_state);

	rgm_lock_state();

	any_frozen_now = is_any_service_frozen();

	do_svc_state_change((const char *)ssi.service_name, ssi.s_state);

	// Wake rgfreeze to update RG freeze states
	// XXX RFE 4248997 : do the cond_signal unconditionally.
	if (any_frozen_now != is_any_service_frozen()) {
		cond_rgfreeze.signal();
	}

	rgm_unlock_state();
}


//
// service_state_callback_impl::is_any_service_frozen()
// Returns TRUE iff any service is in recovery.
// The caller holds the rgm state mutex.
//
boolean_t
service_state_callback_impl::is_any_service_frozen()
{
	return ((num_frozen > 0) ? B_TRUE : B_FALSE);
}


//
// init_service_state_callback()
// Register with the RMA for callbacks.  Called once by rgfreeze().
// The caller holds the rgm state mutex.
// Return 0 for success; 1 if we fail to get reference to the RMA;
// 2 if we fail to register for callbacks with the RMA.
//
int
init_service_state_callback(void)
{
	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		// If we are running for a zone cluster, it is poaaible
		// that the same president is elected multiple times
		// without the rgmd daemon going down. So, it is possible
		// that init_service_state_callback() is called multiple
		// times. But we should register for with RMA multiple
		// times for the same callback. We need to have that for
		// zone cluster rgmds here.
		if (service_state_callback_impl::
			the_service_state_callback_implp != NULL) {
			// No need to register again as this rgmd has
			// already registered with RMA for callbacks
			return (0);
		}
	}
	service_state_callback_impl *ssci = new service_state_callback_impl();

	// Get a reference to the rma.
	Environment e;
	CORBA::Object_ptr rma_obj_p = ns::wait_resolve_local("rma_admin", e);
	if (e.exception()) {
		e.clear();
		ucmm_print("init_service_state_callback", NOGET(
		    "RMA exception\n"));
		return (1);
	}
	if (CORBA::is_nil(rma_obj_p)) {
		ucmm_print("init_service_state_callback", NOGET(
		    "RMA reference error\n"));
		return (1);
	}
	replica::rma_public_var the_rma_v = replica::rma_public::_narrow(
	    rma_obj_p);
	CORBA::release(rma_obj_p);

	// Register with the RMA for callbacks
	the_rma_v->register_callback(ssci->get_objref(), e);
	if (e.exception()) {
		e.clear();
		ucmm_print("init_service_state_callback", NOGET(
		    "register_callback error\n"));
		return (2);
	}

	return (0);
}

//
// compute_freeze_opcode
// Called on president node only.
// If the RG is unmanaged, do nothing and return RGMFR_NONE.
// Examine the given RG and return an opcode for its freeze state:
//	RGMFR_NONE if the RG freeze state should remain unchanged;
//	RGMFR_UNFREEZE if the RG should change from frozen to unfrozen;
//	RGMFR_FREEZE if the RG should change from unfrozen to frozen.
// The caller holds the rgm state mutex.
//
fr_opcode
compute_freeze_opcode(rglist_p_t rglp)
{
	fr_opcode		retval;
	sc_syslog_msg_handle_t	handle;

	// If the RG is unmanaged, skip it.
	if (rglp->rgl_ccr->rg_unmanaged) {
		ucmm_print("compute_freeze_opcode",
		    NOGET("ignore UNMANAGED RG <%s>\n"),
		    rglp->rgl_ccr->rg_name);
		return (RGMFR_NONE);
	}

	//
	// In SC 3.0, we only support wildcard or null values
	// for Global_resources_used.  If Global_resources_used
	// is set to null, then RG remains unfrozen.
	//
	// XXX RFE 4248997: the following test will have to be elaborated:
	// XXX freeze the RG if it uses any RM service that
	// XXX is frozen; otherwise, unfreeze the RG.
	//

	// if RG's Global_resources_used == nil, make sure RG is unfrozen
	if (!rglp->rgl_ccr->rg_glb_rsrcused.is_ALL_value &&
	    rglp->rgl_ccr->rg_glb_rsrcused.names == NULL) {
		retval = RGMFR_UNFREEZE;
	} else {
		// RG's Global_resources_used != nil, it must be wildcard.
		// Give warning if not an explicit ALL value.
		if (!rglp->rgl_ccr->rg_glb_rsrcused.is_ALL_value) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RG_TAG, rglp->rgl_ccr->rg_name);
			//
			// SCMSGS
			// @explanation
			// The Global_resources_used property of the resource
			// group was set to a specific non-null string. The
			// only supported settings of this property in the
			// current release are null ("") or wildcard
			// ("&star;").
			// @user_action
			// No user action is required; the rgmd will interpret
			// this value as wildcard. This means that method
			// timeouts for this resource group will be suspended
			// while any device group temporarily goes offline
			// during a switchover or failover. This is usually
			// the desired setting, except when a resource group
			// has no dependency on any global device service or
			// pxfs file system.
			//
			(void) sc_syslog_msg_log(handle, LOG_WARNING, MESSAGE,
			    SYSTEXT("WARNING: Global_resources_used property "
			    "of resource group <%s>"
			    " is set to non-null string, assuming wildcard"),
			    rglp->rgl_ccr->rg_name);
			sc_syslog_msg_done(&handle);
		}

		// Freeze RG if any disk service is frozen; otherwise unfreeze
		if (service_state_callback_impl::is_any_service_frozen()) {
			retval = RGMFR_FREEZE;
		} else {
			retval = RGMFR_UNFREEZE;
		}
	}

	// If RG is already in desired freeze state, reset retval to RGMFR_NONE
	if (retval == RGMFR_FREEZE && rglp->rgl_is_frozen) {
			retval = RGMFR_NONE;
	} else if (retval == RGMFR_UNFREEZE && !rglp->rgl_is_frozen) {
			retval = RGMFR_NONE;
	}

	return (retval);
}


//
// compute_frozen_rgs
// This runs on the president.  Caller holds the rgm state mutex.
// Examines all RGs against current service freeze states; returns
// through the pointer argument frozen_rgs_p a list (in namelist_t
// format) of all RGs that need to be frozen or to remain frozen.
// Returns the number of RGs that need to change from their current
// freeze state.  *frozen_rgs_p is set to point to the constructed list
// of frozen RGs.  The caller is responsible for freeing this list.
//
uint_t
compute_frozen_rgs(namelist_t **frozen_rgs_p)
{
	namelist_t	*nl;			// temp variable
	uint_t		num_changed = 0;	// how many RGs changed
						// their freeze state

	// For each RG, see if it has to be frozen or unfrozen
	for (rglist_p_t rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {

		fr_opcode frop = compute_freeze_opcode(rglp);

		if (frop != RGMFR_NONE) {
			(void) num_changed++;
		}

		// If RG is being frozen or remains frozen, add it
		// to *frozen_rgs_p.
		if (frop == RGMFR_FREEZE ||
		    (rglp->rgl_is_frozen && frop == RGMFR_NONE)) {
			nl = (namelist_t *)malloc(sizeof (namelist_t));
			nl->nl_name = strdup(rglp->rgl_ccr->rg_name);

			// add nl to *frozen_rgs_p
			if (*frozen_rgs_p == NULL) {
				nl->nl_next = NULL;
			} else {
				nl->nl_next = *frozen_rgs_p;
			}
			*frozen_rgs_p = nl;
		}
	}

	return (num_changed);
}


//
// rgfreeze
//
// This function runs in a separate thread on each node, but has work to do
// only on the President.  The President will block until an action occurs that
// may have changed the freeze state of an RG (ie, upon receiving an RMA
// callback, when managing an RG, etc.)  The cond_signal of the cond_rgfreeze
// variable occurs only on the President node.  Thus, a slave node will forever
// block at the cond_wait in the loop, unless the slave node becomes President.
//
// Obtain the rgm state mutex.
// On the President, compute the initial RG freeze state.
// Then, in an infinite loop:
//	Wait on the cond_rgfreeze condition variable.
//	Notify all nodes of changes in rg freeze states.
//
void
rgfreeze(void *)
{
	namelist_t	*frozen_rgs;	// frozen RGs

	rgm_lock_state();

	// If this node is the President node, then do an initial
	// computation of the frozen RGs to account for the fact that the
	// RMA init_services() callback (and cond_signal of the
	// cond_rgfreeze variable) may have happened while this thread
	// was blocked waiting for the rgm state mutex above.
	// If the RMA callback hasn't happened, this will be a no-op
	// because no device services will be frozen.
	if (am_i_president()) {
		frozen_rgs = NULL;

		// Services' freeze states may have changed-- update RGs
		if (compute_frozen_rgs(&frozen_rgs) > 0) {
			// Notify all slave nodes to update RG freeze states
			rgm_change_freeze(frozen_rgs);
		}
		rgm_free_nlist(frozen_rgs);
	}

	for (;;) {	// loop forever
		frozen_rgs = NULL;

		cond_rgfreeze.wait(&Rgm_state->rgm_mutex);

		// Services' freeze states have changed-- update RGs
		if (compute_frozen_rgs(&frozen_rgs) > 0) {
			// Notify all slave nodes to update RG freeze states
			rgm_change_freeze(frozen_rgs);
		}
		rgm_free_nlist(frozen_rgs);
	}

	// NOTREACHED

	// This function never exits-- it runs until the rgmd (& node) dies
}


//
// rgm_change_freeze
// President-side function that orders slaves to change freeze states of RGs.
// RGs in freeze_list are to be (or remain) frozen, and all other RGs are
// to be (or remain) unfrozen.
// Orders are sent to all cluster members, including the president itself.
// Caller holds rgm state lock.
// Caller frees freeze_list.
//
void
rgm_change_freeze(namelist_t *freeze_list)
{
	rgm::rgm_comm_ptr prgm_serv;
	Environment e;
	sol::nodeid_t i;
	rgm::idl_string_seq_t freezelist;
	sol::nodeid_t president;
	sol::incarnation_num incarn;
	LogicalNodeset nset;

	// convert namelists to sequences of strings
	namelist_to_seq(freeze_list, freezelist);

	president = orb_conf::local_nodeid();
	incarn = Rgm_state->node_incarnations.members[orb_conf::local_nodeid()];

	nset = Rgm_state->current_membership;
	i = 0;
	while ((i = nset.nextServerNodeSet(i + 1)) != 0) {

		prgm_serv = rgm_comm_getref(i);
		if (CORBA::is_nil(prgm_serv)) {
			continue;	// This node is not in the cluster
		}

		prgm_serv->rgm_chg_freeze(president, incarn, freezelist, e);
		CORBA::release(prgm_serv);
		// We don't handle any IDL errors, just log them
		(void) except_to_errcode(e, i);
	}
	// freezelist allocated on stack, so no need to delete
}


//
// freeze_adjust_timeouts
// Executed on the slave node (which may be the president).
// If the frozen flag of the rg is true, suspend timeouts on any
// in-flight methods on resources of this RG.
// If the frozen flag of the rg is false, reinstate timeouts on any
// in-flight methods on resources of this RG.
// The caller holds the rgm state mutex.
void
freeze_adjust_timeouts(rglist_p_t rglp)
{
	methodinvoc_t   *mip;
	// It appears that the fe_cmd arg is a no-op for suspend & resume
	fe_cmd		fed_cmd = {0};
	fe_run_result	fed_result = {FE_OKAY, 0, 0, SEC_OK, NULL};
	int		err;
	sc_syslog_msg_handle_t	handle;

	ucmm_print("freeze_adjust_timeouts()", NOGET("enter RG <%s>\n"),
	    rglp->rgl_ccr->rg_name);

	// Iterate through all in-flight method invocations on RG's resources
	for (mip = rglp->rgl_methods_in_flight; mip != NULL;
	    mip = mip->mi_next) {
		//
		// If the mi_completed flag is set, this means that the method
		// has finished running and the methodinvoc_t entry is just
		// about to be deleted, so we will skip it.
		//
		while (!mip->mi_completed) {
			// If the RG is frozen, suspend the timeout on the
			// method; otherwise, reinstate the timeout.
			if (rglp->rgl_is_frozen) {
				ucmm_print("freeze_adjust_timeouts()", NOGET(
				    "calling fe_suspend on tag <%s>\n"),
				    mip->mi_tag);
				err = fe_suspend(mip->mi_tag, &fed_cmd,
				    &fed_result);
			} else {
				ucmm_print("freeze_adjust_timeouts()", NOGET(
				    "calling fe_resume on tag <%s>\n"),
				    mip->mi_tag);
				err = fe_resume(mip->mi_tag, &fed_cmd,
				    &fed_result);
			}
			if (err != 0 ||
			    (fed_result.type != FE_OKAY && fed_result.type !=
			    FE_NOTAG)) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RG_TAG,
				    rglp->rgl_ccr->rg_name);
				//
				// SCMSGS
				// @explanation
				// The rgmd failed in its attempt to suspend
				// timeouts on an executing method during
				// temporary unavailability of a global device
				// group. This could cause the resource method
				// to time-out. Depending on which method was
				// being invoked and the Failover_mode setting
				// on the resource, this might cause the
				// resource group to fail over or move to an
				// error state.
				// @user_action
				// No action is required if the resource
				// method execution succeeds. If the problem
				// recurs, rebooting this node might cure it.
				// Save a copy of the /var/adm/messages files
				// on all nodes and contact your authorized
				// Sun service provider for assistance in
				// diagnosing the problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "freeze_adjust_timeouts: call to rpc.fed "
				    "failed, tag <%s> err <%d> result <%d>",
				    mip->mi_tag, err, fed_result.type);
				sc_syslog_msg_done(&handle);
				/* free the xdr resources */
				xdr_free((xdrproc_t)xdr_fe_run_result,
					(char *)&fed_result);
				fed_result.output = NULL;
				break;
			}

			// fed rpc succeeded, result was OKAY or NOTAG
			if (fed_result.type == FE_OKAY) {
				xdr_free((xdrproc_t)xdr_fe_run_result,
					(char *)&fed_result);
				fed_result.output = NULL;
			//
			// SCMSGS
			// @explanation
			// The timeout monitoring on the running method is being
			// suspended or resumed because a device group
			// switchover has started or finished respectively.
			// @user_action
			// This is just an informational message.
			//
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RG_TAG,
				    rglp->rgl_ccr->rg_name);
				    (void) sc_syslog_msg_log(handle, LOG_NOTICE,
				    MESSAGE, "Timeout monitoring on method tag "
				    "<%s> has been %s.", mip->mi_tag,
				    (rglp->rgl_is_frozen) ?
				    "suspended" : "resumed");
				sc_syslog_msg_done(&handle);
				break;
			}

			// The tag was unknown to the fed.  This can happen
			// in only two cases:
			// (1) The tag entry was just created and fe_run() has
			//	not run yet.
			// or
			// (2) fe_run() has completed and the methodinvoc_t
			//	entry has not been deleted yet.
			//
			// In case (1), fe_run should run immediately and make
			// the tag valid.  In case (2), the launch_method()
			// thread should immediately set the mi_completed flag
			// in the methodinvoc_t structure to true.
			// To handle either case, we will sleep for 1 second.
			// Then we should either find that the completed flag
			// has been set or the tag is valid.
			ucmm_print("freeze_adjust_timeouts()", NOGET(
			    "tag <%s> for RG <%s> was unknown to the fed; "
			    "sleeping for 1 second\n"),
			    mip->mi_tag, rglp->rgl_ccr->rg_name);
			(void) sleep(1);
		}
	}
}
