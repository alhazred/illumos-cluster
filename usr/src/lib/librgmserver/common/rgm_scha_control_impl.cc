//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_scha_control_impl.cc	1.146	09/04/22 SMI"

//
// rgm_scha_control_impl.cc
//
// The functions in this file implement the server side of
// scha_control operation.
//

#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <libintl.h>
#include <nslib/ns.h>
#ifndef EUROPA_FARM
#include <rgm/rgm_comm_impl.h>
#include <rgm_global_res_used.h>
#endif
#include <rgm_state.h>
#include <rgm_proto.h>
#include <rgm/rgm_util.h>
#include <rgm/scha_priv.h>
#include <thread.h>
#include <fcntl.h>
#include <unistd.h>
#include <libgen.h>
#include <utime.h>
#include <wait.h>
#include <rgm_intention.h>
#include <sys/os.h>
#include <scha_err.h>
#include <scha.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>
#include <rgm_state.h>
#include <scadmin/scconf.h>
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <sys/cl_events.h>

// Zone support
#include <rgm_logical_nodeset.h>
#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_farm.h>
#endif

#include <rgmx_hook.h>

// Interval in seconds that defines "recent" in determining whether the
// RG was frozen (due to Global_resources_used) recently.
// XXX This could become a configurable R or RG property in a future release.
//
#define	GRU_FREEZE_RECENT	10

static scha_errmsg_t sanity_check(const char *r_name, rgm::lni_t lni);
static scha_errmsg_t monitor_check(rlist_p_t rp, rgm::lni_t lni);
static scha_errmsg_t pingpong_check(const char *rg_name, int ppinterval,
    rgm::lni_t lni);

#ifndef EUROPA_FARM
static scha_errmsg_t pres_local_sanity_check(rglist_p_t rgp);
#endif
static boolean_t call_real_monchk(rglist_p_t rgp, char *r_name,
    rgm::lni_t lni);
static void compute_timestamp_fname(const char *r_name, rgm::lni_t lni,
    char *pfilename, char *pdirname);
#ifndef EUROPA_FARM
static boolean_t is_resource_in_group(rlist_p_t rp, rglist_p_t rgp);
static boolean_t r_is_restarting(rlist_p_t rp, rgm::lni_t slave,
    scha_err_t *retcode);
static boolean_t rg_state_permits_restart(rglist_p_t rgp, rgm::lni_t lni,
    boolean_t resource_restart);

//
// idl_scha_control_giveover()
// This function is run by the President.  Called by rgm_scha_control.cc.
// It looks for a healthy candidate zone for specific RG, and fails over the
// RG from its current master to the candidate zone.
//
// The candidate selected will be the zone that best satisfies RG affinities
// and appears closest to the front
// of the nodelist (ie. the most preferred zone) that meets the following
// criteria:
//	1.) in current membership.
//	2.) in RG nodelist.
// 	3.) cannot be the zone which issued scha_control.
//	4.) RG is not already ONLINE on this zone.
//	5.) Passed sanity check and monitor check.
//	6.) Passed ping-pong check.
//
// If there is no candidate zone that fulfills the above criteria, and the
// RG is online on at least one other zone (and is thus a scalable service),
// the RG will be taken offline on the requested zone without being brought
// online elsewhere.
//
// This function is also used for the scha_control CHECK_GIVEOVER (sanity
// checks only) sub-command.  In this case, we perform all the validity checks
// and return error status, but do not actually fail over or take down the RG.
//
// This function also implements a GETOFF form of the request, which is
// identical to GIVEOVER, except that a scalable service RG will not be taken
// offline on the requested zone without bringing it online elsewhere unless
// the RG is online on another zone _and_ passes monitor_check.  The
// difference is subtle: in GIVEOVER, a scalable service RG can be taken
// offline on a zone without being brought online elsewhere as long as the RG
// is online an at least one other zone (without any other requirements).  In
// GETOFF, the RG must be online on a zone and must pass monitor_check.
//
// To give user a clear idea of what is going on for each checking
// method on specific zone, an entry 'idlstr_syslog_msg' (char *) is used
// in the Orb RPC return structure to enable the checking method to pass
// back bundled message string. These messages will be logged on the node
// where the 'scha_control' command is initiated.
//
// scha_control_action() on the slave checks to see if the setting of
// failover_mode allows the operation to be done, before sending the request
// to this function on the president.
//
void
rgm_comm_impl::idl_scha_control_giveover(
	const rgm::idl_scha_control_args &sc_args,
	rgm::idl_scha_control_result_out sc_res,
	Environment &)
{
	boolean_t	cand_pass_check, master_pass_check, err_cont;
	boolean_t	checkonly;
	boolean_t	pingpong_failed = B_FALSE;
	boolean_t	monitorcheck_failed = B_FALSE;
	char 		*resource_name, *initial_rg_name = NULL;
	rglist_p_t	rgp;
	rlist_p_t	rp;
	sol::nodeid_t	nid = 0;	// node where request originated
	LogicalNodeset	master_ns;
	rglist_p_t	*rgp_array = NULL;
	nodeidlist_t	*p = NULL;
	namelist_t	*delegate;
	char		*delegate_r_name;
	scha_err_t	ret;
	scha_errmsg_t	syslog_res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	event_res;
	Environment	e;
	rgm::rgm_comm_ptr		prgm_serv;
	rgm::idl_regis_result_t_var	ti_res_v;
	rgm::idl_scha_control_result_var	chk_res_v;
	cl_event_reason_code_t cl_event_reason;
	cl_event_fail_code_t cl_failure_reason = CL_FR_NO_MASTER;
	LogicalNodeset pending_boot_ns;
	LogicalNodeset stop_failed_ns;
	LogicalNodeset current_nodeset;
	LogicalNodeset *temp_ns;
	sc_syslog_msg_handle_t handle;
	rgm::idl_scha_control_checkall_args	check_args;
	rgm::idl_scha_control_result *retval =
	    new rgm::idl_scha_control_result();
	size_t ncand = 0;		// Number of candidates
	candidate_t *c_nodes = NULL;
	int num_online = 0, num_primaries = 0;
	uint_t i;
	LogicalNodeset emptyNodeset;
	LogicalNodeset toRemove_ns; // Set of lnis to be removed from master_ns
	LogicalNode *ttln;
	char *nodename = NULL, *phys_nodename = NULL;
	LogicalNode *lnNode = NULL;
	idlretval_t idlretval;
	bool is_farmnode;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	//
	// 'syslog_res' is declared as scha_errmsg_t so we can re-use
	// rgm_sprintf() call to bundle log messages together. These
	// messages will be passed back to the caller. Note that in this
	// case the 'err_code' of 'syslog_res' isn't touched.
	//

	retval->ret_code = SCHA_ERR_NOERR;

	// CHECK option-- perform sanity/validity checks only
	checkonly = (sc_args.flags & rgm::SCHACTL_CHECKONLY) ? B_TRUE : B_FALSE;

	rgm_lock_state();
	lnNode = lnManager->findObject(sc_args.lni);
	if (lnNode == NULL) {
		retval->ret_code = SCHA_ERR_NODE;
		goto giveover_end_skip_event; //lint !e801
	}
	nid = lnNode->getNodeid();
	nodename = strdup(lnNode->getFullName());

	// Get the resource name for event generation.
	resource_name = new_str(sc_args.idlstr_R_name);

	rgp = rgname_to_rg(sc_args.idlstr_rg_name);
	if (rgp == NULL) {
		ucmm_print("scha_control", NOGET(
		    "The RG passed in scha_control() is invalid"));
		retval->ret_code = SCHA_ERR_RG;
		goto giveover_end_skip_event; //lint !e801
	}

	initial_rg_name = rgp->rgl_ccr->rg_name;

	rp = rname_to_r(NULL, sc_args.idlstr_R_name);
	if (rp == NULL) {
		ucmm_print("scha_control", NOGET(
		    "The R passed in scha_control() is invalid"));
		retval->ret_code = SCHA_ERR_RSRC;
		goto giveover_end_skip_event; //lint !e801
	}

	//
	// In case there is no failover delegation, we will use the requesting
	// resource for the checks below.
	//
	delegate_r_name = rp->rl_ccrdata->r_name;

	if (!is_resource_in_group(rp, rgp)) {
		// Note, is_resource_in_group() generates a syslog message
		retval->ret_code = SCHA_ERR_RG;
		goto giveover_end_skip_event; //lint !e801
	}

	// Make sure the RG is ONLINE, PENDING_ONLINE_BLOCKED, or
	// ON_PENDING_R_RESTART on the node that issued scha_control().
	if (!rg_on_or_r_restart(rgp, sc_args.lni)) {
		ucmm_print("scha_control", NOGET(
		    "RG <%s> is not online on the scha_control node"),
		    rgp->rgl_ccr->rg_name);
		retval->ret_code = SCHA_ERR_STATE;
		cl_failure_reason = CL_FR_BUSY;
		goto giveover_end; //lint !e801
	}

	//
	// Check if the RG delegates the failover request to another RG.
	// The support for inter cluster failover delagation is not
	// yet implemented.
	//
	// toRemove_ns is set to contain the zone(s) from which the
	// delegate RG must fail over.
	//
	// If there is no delegate, we set toRemove_ns to contain only the
	// zone from which rgp is requesting a giveover.
	//
	delegate = rgp->rgl_affinities.ap_failover_delegation;
	if ((delegate != NULL) && (!is_enhanced_naming_format_used(
	    delegate->nl_name))) {
		rlist_p_t delegate_r;
		rgp = get_deepest_delegate(delegate->nl_name, sc_args.lni,
		    &toRemove_ns, &delegate_r);
		if (rgp == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// A non-fatal internal error was detected by the
			// rgmd. The target resource group for a strong
			// positive affinity with failover delegation (+++
			// affinity) is invalid.  The currently attempted
			// failover is deferred and might be retried.
			// @user_action
			// Since this problem might indicate an internal logic
			// error in the rgmd, save a copy of the
			// /var/adm/messages files on all nodes, and the
			// output of clresourcetype show -v, clresourcegroup
			// show -v +, and clresourcegroup status +. Report the
			// problem to your authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "INTERNAL ERROR: invalid "
			    "failover delegate  <%s>", delegate->nl_name);
			sc_syslog_msg_done(&handle);
			retval->ret_code = SCHA_ERR_INTERNAL;
			goto giveover_end_skip_event; //lint !e801
		}

		delegate_r_name = delegate_r ? delegate_r->rl_ccrdata->r_name :
		    NULL;

		//
		// SCMSGS
		// @explanation
		// A resource from a resource group that declares a strong
		// positive affinity with failover delegation (+++ affinity)
		// attempted a giveover request via the scha_control command.
		// The giveover request was forwarded to the
		// delegate resource group.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		rgm_sprintf(&syslog_res, SC_SYSLOG_MESSAGE("Delegating "
		    "giveover request to resource group %s, due to a +++ "
		    "affinity of resource group %s (possibly transitively)."),
		    rgp->rgl_ccr->rg_name, initial_rg_name);
	} else {
		toRemove_ns.addLni(sc_args.lni);
	}

	//
	// If there is giveover delegation and physical-node affinities
	// are in use, set "phys_nodename" to the physical nodename only,
	// because the delegate RG might be failing over from a different
	// zone than the originating RG/resource.
	// Otherwise, set phys_nodename to the full node:zone name.
	// phys_nodename will be used in some syslog messages below.
	//
	if (delegate && !use_logicalnode_affinities()) {
		phys_nodename = strdup(nodename);
	} else {
		phys_nodename = strdup(lnNode->getNodename());
	}

	//
	// Make sure the RG is not busy.  Note, if the RG has resource(s)
	// restarting (even on node 'nodeid'), we permit the giveover;
	// the state machine handles this case.
	// Note: If the RG is running BOOT methods on other node(s), we can
	// proceed with the scha_control(), but will not use a BOOTing
	// node as a new master.
	//
	// If the RG is evacuating from some node(s), then we proceed with
	// the giveover because sort_candidate_nodes() will exclude any
	// evacuating nodes from candidacy.
	//
	if (!in_endish_rg_state(rgp, &pending_boot_ns, &stop_failed_ns, NULL,
	    NULL)) {
		ucmm_print("RGM",
		    NOGET("scha_control_giveover: RG <%s> is busy; exiting"),
		    rgp->rgl_ccr->rg_name);
		retval->ret_code = SCHA_ERR_RGRECONF;
		cl_failure_reason = CL_FR_BUSY;
		goto giveover_end; //lint !e801
	}
	// ERROR_STOP_FAILED is an endish state but precludes scha_control
	if (!stop_failed_ns.isEmpty()) {
		// The RG is ERROR_STOP_FAILED on some node
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "Resource group <%s> requires operator attention due to "
		    "STOP failure"), rgp->rgl_ccr->rg_name);
		sc_syslog_msg_done(&handle);

		// For event generation we havent defined any failure reason
		// for deferring giveover of STOP_FAILED RG's. Setting it to
		// CL_FR_BUSY instead.
		cl_failure_reason = CL_FR_BUSY;

		retval->ret_code = SCHA_ERR_STOPFAILED;
		goto giveover_end; //lint !e801
	}


	// Perform president-local sanity checks; currently this only checks
	// for RG "frozen" on global_resources_used.
	retval->ret_code = pres_local_sanity_check(rgp).err_code;
	if (retval->ret_code != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A scha_control call has failed with a SCHA_ERR_CHECKS error
		// because the resource group has a non-null
		// Global_resources_used property, and a global device group
		// was failing over within the indicated recent time interval.
		// The resource fault probe is presumed to have failed because
		// of the temporary unavailability of the device group. A
		// properly-written resource monitor, upon getting the
		// SCHA_ERR_CHECKS error code from a scha_control call, should
		// sleep for awhile and restart its probes.
		// @user_action
		// No user action is required. Either the resource should
		// become healthy again after the device group is back online,
		// or a subsequent scha_control call should succeed in failing
		// over the resource group to a new master.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "scha_control: resource group <%s> was frozen on "
		    "Global_resources_used within the past %d seconds; "
		    "exiting"),
		    rgp->rgl_ccr->rg_name, GRU_FREEZE_RECENT);
		cl_failure_reason = CL_FR_FROZEN;
		sc_syslog_msg_done(&handle);
		goto giveover_end; //lint !e801
	}

	//
	// Verify that the starting dependencies are resolved for this
	// resource on some node.  Otherwise, we don't want to do the
	// giveover, because the resource would just end up in STARTING_DEPEND.
	// We return SCHA_ERR_CHECKS to make the caller wait until the
	// resource's dependencies are resolved.
	//
	// This check is done on the requesting resource rather than on the
	// delegate.
	//
	// We need a temporary nodeset because
	// are_starting_dependencies_resolved modifies the nodeset.
	//
	// Note that the get_all_logical_nodes() call will exclude
	// zones which are in SHUTTING_DOWN state.  This is OK because
	// they are not candidates for a giveover.
	//
	temp_ns = lnManager->get_all_logical_nodes(rgm::LN_UP);
	if (!are_starting_dependencies_resolved(rp,
	    rp->rl_ccrdata->r_dependencies.dp_strong, DEPS_STRONG, *temp_ns) ||
	    !are_starting_dependencies_resolved(rp,
	    rp->rl_ccrdata->r_dependencies.dp_restart, DEPS_RESTART,
	    *temp_ns) || !are_starting_dependencies_resolved(rp,
	    rp->rl_ccrdata->r_dependencies.dp_offline_restart,
	    DEPS_OFFLINE_RESTART, *temp_ns)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A scha_control(1HA,3HA) GIVEOVER attempt failed on all
		// potential masters, because the resource requesting the
		// GIVEOVER has unsatisfied dependencies. A properly-written
		// resource monitor, upon getting the SCHA_ERR_CHECKS error
		// code from a scha_control call, should sleep for awhile and
		// restart its probes.
		// @user_action
		// Usually no user action is required, because the dependee
		// resource is switching or failing over and will come back
		// online automatically. At that point, either the probes will
		// start to succeed again, or the next GIVEOVER attempt will
		// succeed. If that does not appear to be happening, you can
		// use clresource to determine the resources on which the
		// specified resource depends that are not online, and bring
		// them online.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, SYSTEXT(
		    "RGM is deferring the relocation of resource group <%s> "
		    "from node <%s>, because resource %s depends on another "
		    "resource which is currently offline."),
		    initial_rg_name, nodename, resource_name);
		sc_syslog_msg_done(&handle);
		retval->ret_code = SCHA_ERR_CHECKS;
		cl_failure_reason = CL_FR_NO_MASTER;
		goto giveover_end; //lint !e801
	}

	//
	// Setting up flag here so it's safe for scha_control
	// to release global lock when doing sanity checking and
	// monitor checking.
	//
	rgp->rgl_switching = SW_IN_PROGRESS;

	//
	// Make a call to sort_candidate_nodes to get an ordered list
	// of candidate nodes for the giveover that pass the affinities
	// and other checks (see check_node_candidacy in rgm_process_rgs.cc).
	//
	// We tell sort_candidate_nodes to skip the zones from which rgp
	// is to fail over.  We also remove those zones from master_ns.
	//
	res = sort_candidate_nodes(emptyNodeset, rgp, c_nodes, ncand,
	    toRemove_ns, num_online, master_ns, num_primaries, B_FALSE, B_TRUE,
	    B_FALSE, B_FALSE, B_TRUE);
	if (res.err_code != SCHA_ERR_NOERR) {
		// sort_candidate_nodes has logged a syslog message
		retval->ret_code = res.err_code;
		goto giveover_end; //lint !e801
	}
	//
	// Remove all those lnis from master_ns, that were added to the
	// "toRemove_ns" above.
	//
	// Save the current set of masters as current_nodeset.
	// It is used later for posting an event.
	//
	current_nodeset = master_ns;
	master_ns -= toRemove_ns;

	//
	// If there is no candidate node available and there are no
	// other current masters, veto the giveover.
	//
	if (ncand == 0 && master_ns.isEmpty()) {
		ucmm_print("RGM", NOGET(
		    "scha_control: There is no giveover candidate available"));
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A scha_control(1HA,3HA) GIVEOVER attempt failed because no
		// candidate node was healthy enough to host the resource
		// group, and the resource group was not currently mastered by
		// any other node.
		// @user_action
		// Examine other syslog messages on all cluster members that
		// occurred about the same time as this message, to see why
		// other candidate nodes were not healthy enough to master the
		// resource group.
		// Note:
		// A properly-implemented resource monitor, upon
		// encountering the failure of a scha_control GIVEOVER
		// call, will sleep for awhile and restart its
		// probes.  If the original error condition is temporary,
		// later probes will succeed and the resource can
		// continue providing service on its current master.
		// If probe failures continue to occur on this node, the
		// scha_control GIVEOVER will be re-attempted and might then
		// succeed in relocating the resource group to a new master.
		// In these cases, no user action is required.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, SYSTEXT(
		    "RGM isn't failing resource group <%s> off of node <%s>, "
		    "because there are no other current or potential masters"),
		    rgp->rgl_ccr->rg_name, phys_nodename);
		sc_syslog_msg_done(&handle);
		rgp->rgl_switching = SW_NONE;
		retval->ret_code = SCHA_ERR_NOMASTER;
		cl_failure_reason = CL_FR_NO_MASTER;
		goto giveover_end; //lint !e801
	}

	//
	// Set pingpong timestamp of R on the node where scha_control was
	// issued. The timestamp will be used in future pingpong tests.
	// However, if we are in CHECK mode, do not set the timestamp.
	//
	// If we are going to run getoff instead of giveover, we still
	// need to setup time stamp.
	//
	// If the giveover is delegated, the timestamp is set for the delegate
	// resource, on each lni from which it will fail over.
	//
	if (!checkonly && (delegate_r_name != NULL)) {
		rgm::lni_t n = 0;
		while ((n = toRemove_ns.nextLniSet(n + 1)) != 0) {
			if ((rgmx_hook_call(rgmx_hook_set_timestamp, nid, n,
			    delegate_r_name, &ti_res_v, &idlretval,
			    &is_farmnode) == RGMX_HOOK_ENABLED) &&
			    (is_farmnode)) {
				if (idlretval != RGMIDL_OK) {
					(void) sc_syslog_msg_initialize(&handle,
					    SC_SYSLOG_RGM_RGMD_TAG, "");
					//
					// SCMSGS
					// @explanation
					// The rgmd has encountered an error
					// that prevents the scha_control
					// function from successfully setting a
					// ping-pong time stamp on a farm node,
					// probably because of a node death
					// or network failure.
					// @user_action
					// Usually, no action is required.
					// If another node is available,
					// services will be restarted
					// automatically.
					//
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,
					    "rgmx_set_timestamp(): failed");
					sc_syslog_msg_done(&handle);
					rgp->rgl_switching = SW_NONE;
					retval->ret_code = SCHA_ERR_CLRECONF;
					cl_failure_reason = CL_FR_BUSY;
					goto giveover_end; //lint !e801
				}
			} else {
				prgm_serv = rgm_comm_getref(nid);
				if (CORBA::is_nil(prgm_serv)) {
					ucmm_print("scha_control", NOGET(
					    "Can not set ORB pointer to node "
					    " <%d>"), nid);
					rgp->rgl_switching = SW_NONE;
					// error is probably due to node death
					retval->ret_code = SCHA_ERR_CLRECONF;
					cl_failure_reason = CL_FR_BUSY;
					goto giveover_end; //lint !e801
				}
				prgm_serv->idl_set_timestamp(delegate_r_name,
				    n, ti_res_v, e);

				CORBA::release(prgm_serv);
				if (e.exception() != NULL) {
					(void) sc_syslog_msg_initialize(&handle,
					    SC_SYSLOG_RGM_RGMD_TAG, "");
					//
					// SCMSGS
					// @explanation
					// The rgmd has encountered an error
					// that prevents the scha_control
					// function from successfully setting a
					// ping-pong time stamp, probably
					// because of a node death or network
					// failure.
					// @user_action
					// Usually, no action is required.
					// If another node is available,
					// services will be restarted
					// automatically.
					//
					(void) sc_syslog_msg_log(handle,
					    LOG_NOTICE, MESSAGE,
					    "idl_set_timestamp(): IDL "
					    "Exception");
					sc_syslog_msg_done(&handle);
					e.clear();
					rgp->rgl_switching = SW_NONE;
					retval->ret_code = SCHA_ERR_CLRECONF;
					cl_failure_reason = CL_FR_BUSY;
					goto giveover_end; //lint !e801
				}

				if (ti_res_v->ret_code != SCHA_ERR_NOERR) {
					ucmm_print("scha_control", NOGET(
					    "idl_set_timestamp() failed"));
					rgp->rgl_switching = SW_NONE;
					retval->ret_code = ti_res_v->ret_code;
					//lint -save -e801
					goto giveover_end_skip_event;
					//lint -restore
				}
			}
		}
	}

	//
	// Iterate through the list of candidate zones, trying to find
	// one that passes the checks.
	//
	// If the giveover was delegated, checks are performed on the
	// delegate resource.  If the delegate RG has no resources
	// (indicated by delegate_r_name == NULL) then skip the checks.
	//
	cand_pass_check = master_pass_check = B_FALSE;
	for (i = 0; i < ncand; i++) {
		err_cont = B_FALSE;
		check_args.flags = 0;
		check_args.lni = c_nodes[i].lni;
		if (delegate_r_name == NULL) {
			cand_pass_check = B_TRUE;
			break;
		}
		check_args.idlstr_R_name = new_str(delegate_r_name);

		//
		// Before we call idl_scha_control_checkall(), we
		// are going to release the lock so other modules can
		// still proceed.
		//

		ttln = lnManager->findObject(check_args.lni);
		CL_PANIC(ttln != NULL);

		rgm_unlock_state();

		if ((rgmx_hook_call(rgmx_hook_scha_control_checkall,
			ttln->getNodeid(),
			ttln->getIncarnation(),
			&check_args, &chk_res_v, &idlretval,
			&is_farmnode) == RGMX_HOOK_ENABLED) &&
			(is_farmnode)) {
			if (idlretval == RGMIDL_NOREF) {
				rgm_lock_state();
				continue;
			}
			if (idlretval != RGMIDL_OK) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "rgmx_scha_control_checkall(): failed");
				sc_syslog_msg_done(&handle);
				err_cont = B_TRUE;
				chk_res_v = NULL;
			}
		} else {
			prgm_serv = rgm_comm_getref(ttln->getNodeid());
			if (CORBA::is_nil(prgm_serv)) {
				// This node is not in the name server.
				ucmm_print("scha_control", NOGET(
				    "Failed to get ORB ref to node <%d>"),
				    ttln->getNodeid());
				rgm_lock_state();
				continue;
			}

			// Run checkall() routine on all candidate node.
			prgm_serv->idl_scha_control_checkall(
			    orb_conf::local_nodeid(),
			    Rgm_state->node_incarnations.members[
			    orb_conf::local_nodeid()],
			    ttln->getIncarnation(), check_args, chk_res_v, e);
			CORBA::release(prgm_serv);
		}
		if (chk_res_v == NULL || e.exception() != NULL) {
			//
			// If the ORB call failed, then the node has died;
			// continue to next node.
			//
#ifdef DEBUG
			//
			// In non-debug build, suppress the following syslog
			// message for the case that the node (or zone) died.
			// It is just cluttering the syslog and is
			// incomprehensible to most users.
			// We should consider eliminating this message
			// entirely or at least change the wording.
			// If we keep it, it should output fullname instead
			// of nodeid.
			//
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// During a failover attempt, the scha_control
			// function was unable to check the health of the
			// indicated node, because of an error in inter-node
			// communication. This was probably caused by the
			// death of the indicated node during scha_control
			// execution. The RGM will still attempt to master the
			// resource group on another node, if available.
			// @user_action
			// No action is required; the rgmd should recover
			// automatically. Identify what caused the node to die
			// by examining syslog output. The syslog output might
			// indicate further remedial actions.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "idl_scha_control_checkall(): "
			    "IDL Exception on node <%d>", ttln->getNodeid());
			sc_syslog_msg_done(&handle);
#endif	// DEBUG
			if (e.exception() != NULL) {
				e.clear();
			}
			err_cont = B_TRUE;
		} else if ((const char *)chk_res_v->idlstr_syslog_msg != NULL) {
			//
			// We re-use rgm_sprintf() to encapsulate the
			// log messages returned from checkall()
			// and put them into syslog_res. These msgs will
			// be eventually passed back to the caller.
			//
			rgm_sprintf(&syslog_res, "%s",
			    (const char *)chk_res_v->idlstr_syslog_msg);
		}
		check_args.idlstr_R_name = (const char *)NULL;

		// Ok to reclaim lock after running checkall().
		rgm_lock_state();

		if ((ret = map_busted_err(rgp->rgl_switching)) !=
		    SCHA_ERR_NOERR) {
			// The RG has been busted, do early return;
			ucmm_print("scha_control", NOGET(
			    "RG <%s> is busted"), rgp->rgl_ccr->rg_name);
			if (rgp->rgl_switching != SW_EVACUATING) {
				rgp->rgl_switching = SW_NONE;
			}
			cl_failure_reason = CL_FR_BUSY;
			retval->ret_code = (int32_t)ret;
			goto giveover_end; //lint !e801
		}

		if (err_cont) {
			continue;
		}

		// The following two boolean variables are used
		// for event generation. This new ERR code
		// SCHA_ERR_PINGPONG is introduced for event
		// generation. This ERR code is defined
		// in scha_priv.h, the private header file.
		if (chk_res_v->ret_code == SCHA_ERR_PINGPONG)
			pingpong_failed = B_TRUE;
		else
			monitorcheck_failed = B_TRUE;

		if (chk_res_v->ret_code != SCHA_ERR_NOERR) {
			// This node is not healthy enough to
			// pass the test. Try next node.
			ucmm_print("scha_control", NOGET("node <%d> is not a "
			    "healthy potential master.\n"), check_args.lni);
			retval->ret_code = chk_res_v->ret_code;
			continue;
		}

		ucmm_print("scha_control", NOGET("node <%d> is a healthy "
		    "potential master.\n"), check_args.lni);

		//
		// We found a healthy candidate which could take over
		// the RG.  check_args.lni stores the lni of the node that
		// passed.
		//
		cand_pass_check = B_TRUE;
		break;
	}


	//
	// If we didn't find a candidate node that passed the checks,
	// try to find a current master node, other than the nodes we're
	// trying to get off of, that passes the checks (but only
	// if we're running GETOFF).
	//
	// The 'idlstr_syslog_msg' of 'chk_res_v' contains the log message
	// for each checking method on specific node if the check fails.
	// Remeber there is no need to free idlstr_syslog_msg each time
	// after 'chk_res_v' comes back from ORB RPC call, since overloaded
	// operator "=" for type 'String_field' will clean up old string
	// at the time the new message is loaded.
	//
	// If the giveover was delegated, checks are performed on the
	// delegate resource.  If the delegate RG has no resources
	// (indicated by delegate_r_name == NULL) then skip the checks.
	//
	if ((sc_args.flags & rgm::SCHACTL_GETOFF) != 0 && !cand_pass_check) {
		for (p = rgp->rgl_ccr->rg_nodelist; p != NULL;
		    p = p->nl_next) {

			err_cont = B_FALSE;

			if (!master_ns.containLni(p->nl_lni)) {
				continue;
			}

			check_args.lni = p->nl_lni;
			check_args.flags = rgm::SCHACTL_GETOFF;
			if (delegate_r_name == NULL) {
				master_pass_check = B_TRUE;
				break;
			}
			check_args.idlstr_R_name = new_str(delegate_r_name);

			//
			// Before we call idl_scha_control_checkall(), we
			// are going to release the lock so other modules can
			// still proceed.
			//

			ttln = lnManager->findObject(p->nl_lni);
			CL_PANIC(ttln != NULL);

			rgm_unlock_state();

			if ((rgmx_hook_call(rgmx_hook_scha_control_checkall,
				ttln->getNodeid(),
				ttln->getIncarnation(),
				&check_args, &chk_res_v, &idlretval,
				&is_farmnode) == RGMX_HOOK_ENABLED) &&
				(is_farmnode)) {
				if (idlretval == RGMIDL_NOREF) {
					continue;
				}
				if (idlretval != RGMIDL_OK) {
					(void) sc_syslog_msg_initialize(&handle,
					    SC_SYSLOG_RGM_RGMD_TAG, "");
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR,
					    MESSAGE,
					    "rgmx_scha_control_checkall(): "
					    "failed");
					sc_syslog_msg_done(&handle);
					err_cont = B_TRUE;
					chk_res_v = NULL;
				}
			} else {
				prgm_serv = rgm_comm_getref(ttln->getNodeid());
				if (CORBA::is_nil(prgm_serv)) {
					// This node is not in the name server.
					ucmm_print("scha_control", NOGET(
					    "Failed to get ORB ref to node "
					    "<%d>"),
					    ttln->getNodeid());
					continue;
				}

				// Run checkall() routine on all candidate node.

				prgm_serv->idl_scha_control_checkall(
				    orb_conf::local_nodeid(),
				    Rgm_state->node_incarnations.members[
				    orb_conf::local_nodeid()],
				    ttln->getIncarnation(), check_args,
				    chk_res_v, e);

				CORBA::release(prgm_serv);
			}
			if (chk_res_v == NULL || e.exception() != NULL) {
				// If the ORB call failed, then clean the scene
				// and return early.
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "idl_scha_control_checkall(): "
				    "IDL Exception on node <%d>",
				    ttln->getNodeid());
				sc_syslog_msg_done(&handle);
				if (e.exception() != NULL) {
					e.clear();
				}
				err_cont = B_TRUE;
			} else if ((const char *)
			    chk_res_v->idlstr_syslog_msg != NULL) {
				//
				// We re-use rgm_sprintf() to encapsulate the
				// log messages returned from checkall()
				// and put them into syslog_res. These msgs
				// will be eventually passed back to the
				// caller.
				//
				rgm_sprintf(&syslog_res, "%s", (const char *)
				    chk_res_v->idlstr_syslog_msg);
			}

			// Ok to reclaim lock after running checkall().
			rgm_lock_state();

			if ((ret = map_busted_err(rgp->rgl_switching)) !=
			    SCHA_ERR_NOERR) {
				// The RG has been busted, do early return;
				ucmm_print("scha_control", NOGET(
				    "RG <%s> is busted"),
				    rgp->rgl_ccr->rg_name);
				if (rgp->rgl_switching != SW_EVACUATING) {
					rgp->rgl_switching = SW_NONE;
				}
				cl_failure_reason = CL_FR_BUSY;
				retval->ret_code = (int32_t)ret;
				goto giveover_end; //lint !e801
			}

			if (err_cont) {
				continue;
			}

			// The following two boolean variables are used
			// for event generation. This new ERR code
			// SCHA_ERR_PINGPONG is introduced for event
			// generation. This ERR code is defined
			// in scha_priv.h, the private header file.
			if (chk_res_v->ret_code == SCHA_ERR_PINGPONG)
				pingpong_failed = B_TRUE;
			else
				monitorcheck_failed = B_TRUE;

			if (chk_res_v->ret_code != SCHA_ERR_NOERR) {
				// This node is not healthy enough to
				// pass the test. Try next node.
				retval->ret_code = chk_res_v->ret_code;
				continue;
			}

			//
			// We found a master that passes the checks.
			//
			master_pass_check = B_TRUE;
			break;
		}
	}

	//
	// Now we come to see whether we are eligible to do giveover/getoff
	// due to the test results.
	//
	// If there is a candidate that passed the healthy test we will give
	// the RG to it whether or not the GETOFF flag is set.  So we
	// add this new master to the master_ns and call do_rg_switch.
	//
	// If there are no candidate nodes that passed the test, we can
	// still continue as long as two checks pass.
	// First we make sure we have the RG online on at least one other
	// node (check that master_ns is not empty).
	// Secondly, GETOFF is slightly more stringent, so if we are executing
	// GETOFF, check that at least one of the masters passed the monitor
	// check (signified by master_pass_check being true).
	// If either of the conditions fail, bail out and return an error code.
	//
	// If they both pass, we can go ahead with the do_rg_switch to
	// bring the RG offline on the requested node without bringing
	// it online elsewhere.
	//
	if (cand_pass_check) {
		// We find a healthy candidate, so we can just failover
		// RG to him.
		master_ns.addLni(check_args.lni);

		// For posting an event set the reason code
		// to giveover.

		cl_event_reason = CL_REASON_RG_GIVEOVER;

	} else if (master_ns.isEmpty()) {
		// If there are no candidate nodes and no other current masters,
		// we cannot complete the request.
		// We log an error and veto the giveover.
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, SYSTEXT(
		    "RGM isn't failing resource group <%s> off of node <%s>, "
		    "because there are no other current or potential masters"),
		    rgp->rgl_ccr->rg_name, phys_nodename);
		sc_syslog_msg_done(&handle);
		rgp->rgl_switching = SW_NONE;
		retval->ret_code = SCHA_ERR_CHECKS;

		//
		// For event generation:
		// To set the failure_reason, we give high priority
		// to pingpong failure. When we are here, by default
		// the failure_reason is NO_MASTER. We then check if
		// monitorcheck_failed on any candidate nodeset. If
		// this is true we set the failure rason to
		// MON_CHECK_FAILED. If pingpong check also failed
		// on any master then we set the failure to PINGPONG
		// failure . This gives the pingpong failure high
		// priority over other failures while posting an
		// event.
		//

		cl_failure_reason = CL_FR_NO_MASTER;

		if (monitorcheck_failed)
			cl_failure_reason = CL_FR_MON_CHECK_FAILED;
		if (pingpong_failed)
			cl_failure_reason = CL_FR_PINGPONG;

		goto giveover_end; //lint !e801
	} else if (!master_pass_check &&
	    (sc_args.flags & rgm::SCHACTL_GETOFF) != 0) {
		//
		// If we are executing GETOFF and there are no
		// candidate nodes and no masters that passed the check
		// we cannot complete the request.
		// We log an error and veto the getoff.
		//
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A scha_control(1HA,3HA) GIVEOVER attempt failed on all
		// potential masters, because no candidate node was healthy
		// enough to host the resource group.
		// @user_action
		// Examine other syslog messages on all cluster members that
		// occurred about the same time as this message, to see if the
		// problem that caused the MONITOR_CHECK failure can be
		// identified. Repair the condition that is preventing any
		// potential master from hosting the resource.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, SYSTEXT(
		    "RGM isn't failing resource group <%s> off of "
		    "node <%s>, because no current or potential master "
		    "is healthy enough"), rgp->rgl_ccr->rg_name, phys_nodename);
		sc_syslog_msg_done(&handle);
		rgp->rgl_switching = SW_NONE;
		retval->ret_code = SCHA_ERR_CHECKS;
		cl_failure_reason = CL_FR_MON_CHECK_FAILED;
		goto giveover_end; //lint !e801
	} else if ((sc_args.flags & rgm::SCHACTL_GETOFF) == 0) {
		//
		// If we reach here we are not executing the GET_OFF
		// variant, have not found any new potential masters,
		// but have found at least one other current master.
		// Thus, we can continue to take down the service from
		// the node requested without bringing it online elsewhere.
		//
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_NOTICE,
		    MESSAGE, SYSTEXT(
		    "Resource %s has failed on node %s.  Resource group %s"
		    "is being switched offline by the RGM on this node; the "
		    "resource group remains online on other nodes."),
		    resource_name, phys_nodename, rgp->rgl_ccr->rg_name);
		sc_syslog_msg_done(&handle);

		cl_event_reason = CL_REASON_RG_GIVEOVER;

	} else {
		// we _are_ executing GET_OFF, have not found any new
		// potential masters, but have found at least one other
		// current healthy master.
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The resource group was brought OFFLINE on the node
		// specified, probably because of a public network failure on
		// that node. Although there is no spare node on which to
		// relocate this instance of the resource group, the resource
		// group is currently mastered by at least one other healthy
		// node.
		// @user_action
		// No action required. If desired, examine other syslog
		// messages on the node in question to determine the cause of
		// the network failure.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE,
		    MESSAGE, SYSTEXT(
		    "Resource %s has failed on node %s.  Resource group %s"
		    "is being switched offline by the RGM on this node; the "
		    "resource group remains online on other nodes."),
		    resource_name, nodename, rgp->rgl_ccr->rg_name);
		sc_syslog_msg_done(&handle);
		cl_event_reason = CL_REASON_RG_GETOFF;
	}

	if (!checkonly) {
		// Post an event for this RG

		event_res = post_primaries_changing_event(resource_name,
		    rgp->rgl_ccr->rg_name,
		    cl_event_reason, nodename,
		    rgp->rgl_ccr->rg_desired_primaries,
		    current_nodeset, master_ns);
		if (event_res.err_code == SCHA_ERR_NOMEM) {
			retval->ret_code = SCHA_ERR_NOMEM;
			rgp->rgl_switching = SW_NONE;
			goto giveover_end_skip_event; //lint !e801
		}
	}

	//
	// When we are here we fell into one of the following cases:
	// 1. Found a healthy candidate node to which we can fail over the RG.
	// 2. Executing GIVEOVER, and found at least one other node on which the
	//    RG is mastered.
	// 3. Executing GETOFF, and found at least on other node on which the RG
	//    is mastered that passed monitor_check.
	//
	retval->ret_code = SCHA_ERR_NOERR;
	if (checkonly) {
		rgp->rgl_switching = SW_NONE;
	} else {
		// Note when do_rg_switch() is called here, only one RG
		// will be switched by a call to scha_control.
		// The rgp_array passed to do_rg_switch() contains RG name
		// is NULL terminated.
		//
		// calloc() will set each entries to NULL.

		rgp_array = (rglist_p_t *)calloc_nocheck(2,
		    sizeof (rglist_p_t));
		if (rgp_array == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// A scha_control call failed because the system has
			// run out of swap space. The system is likely to halt
			// or reboot if swap space continues to be depleted.
			// @user_action
			// Investigate the cause of swap space depletion and
			// correct the problem, if possible.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, SYSTEXT(
			    "Failed to alloc memory"));
			sc_syslog_msg_done(&handle);
			rgp->rgl_switching = SW_NONE;
			retval->ret_code = SCHA_ERR_NOMEM;
			goto giveover_end_skip_event; //lint !e801
		}

		rgp_array[0] = rgp;

		//
		// We should have rgl_switching flag set when we enter
		// do_rg_switch() and it will have the flag cleaned
		// when returns.
		//
		ucmm_print("scha_control", NOGET(
		    "Going to call do_rg_switch()"));

		//
		// append do_rg_switch error messages (if any) onto syslog_res
		// so they will be passed back to the caller & syslogged
		//
		// Set the offlining flag as well as onlining flag to false.
		// We dont need the last parameter failed_op_list as well.
		//
		do_rg_switch(&syslog_res, rgp_array, master_ns, emptyNodeset,
		    B_FALSE, B_FALSE, NULL);
		retval->ret_code = (int32_t)syslog_res.err_code;
		// Note do_rg_switch clears rgp->rgl_switching flag
		free(rgp_array);

		//
		// If do_rg_switch failed, it usually indicates a start or
		// stop method failure or a node death.  Currently we do
		// not generate a failure event for these.
		//
		if (retval->ret_code != SCHA_ERR_NOERR) {
			ucmm_print("scha_control", NOGET(
			    "do_rg_switch() returns error code <%d>"),
			    retval->ret_code);
			goto giveover_end_skip_event; //lint !e801
		}
	}

giveover_end:

	// if the giveover failed, post a failure event
	if (!checkonly && retval->ret_code != SCHA_ERR_NOERR)
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE,
		    ESC_CLUSTER_RG_GIVEOVER_DEFERRED,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
		    CL_EVENT_INIT_AGENT, 0, 0, 0,
		    CL_RG_NAME, SE_DATA_TYPE_STRING,
		    rgp->rgl_ccr->rg_name,
		    CL_FAILURE_REASON, SE_DATA_TYPE_UINT32,
		    cl_failure_reason, NULL);
#else
		(void) sc_publish_event(ESC_CLUSTER_RG_GIVEOVER_DEFERRED,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
		    CL_EVENT_INIT_AGENT, 0, 0, 0,
		    CL_RG_NAME, SE_DATA_TYPE_STRING,
		    rgp->rgl_ccr->rg_name,
		    CL_FAILURE_REASON, SE_DATA_TYPE_UINT32,
		    cl_failure_reason, NULL);
#endif
giveover_end_skip_event:

	//
	// Note: cast syslog_res.err_msg to (const char *) ensures
	// the message to be copied to 'retval->idlstr_syslog_msg'
	// instead of doing reference passing. For details please refer:
	//	src/orb/corba.cc
	// The message will be freed by the caller when destructor of
	// 'idl_scha_control_result' is called.
	//
	retval->idlstr_syslog_msg = (const char *)syslog_res.err_msg;
	free(syslog_res.err_msg);
	if (resource_name != NULL)
		delete [] resource_name;
	check_args.idlstr_R_name = (const char *)NULL;

	//
	// Free the candidate list.  If we didn't get far enough to call
	// sort_candidate_nodes, ncand will be 0, so this call will be a
	// no-op.
	//
	free_candidate_list(c_nodes, ncand);
	free(c_nodes);

	// Do not free delegate_r_name; it points into rgmd in-memory state

	//
	// When we reach here, the rgp->rgl_switching flag is already reset,
	// and return code is set.
	//
	sc_res = retval;
	free(nodename);
	free(phys_nodename);
	rgm_unlock_state();
}


//
// idl_scha_control_restart()
// Accomplish the function of scha_control -O RESTART, which stops and
// restarts the RG on the same node; or of scha_control -O RESOURCE_RESTART,
// which stops and starts the resource on the same node; or of scha_control
// RESOURCE_IS_RESTARTED, which increments the RESOURCE_RESTART counter
// for the given resource on the same node, without actually restarting
// the resource.  RESOURCE_IS_RESTARTED assumes that the caller has
// restarted the resource by some other means than calling scha_control.
//
// If the RG restart
// fails on this node, process_needy_rg() will call rebalance()
// to failover the RG to other candidate(s).
//
// This function is also used for the scha_control CHECK_RESTART (sanity
// checks only) sub-command.  In this case, we perform all the validity checks
// and return error status, but do not actually restart the RG.
//
// Checks to see if the setting of Failover_mode allows the operation
// are done on the slave, before sending the request to the president.
//
void
rgm_comm_impl::idl_scha_control_restart(
	const rgm::idl_scha_control_args &sc_args,
	rgm::idl_regis_result_t_out sc_res,
	Environment &)
{
	namelist_t	rg = {NULL, NULL};
	rglist_p_t	rgp = NULL;
	rlist_p_t	rp;
	boolean_t	rg_is_stopped = B_FALSE;
	boolean_t	rg_is_started = B_FALSE;
	scha_err_t	ret;
	boolean_t	checkonly = B_FALSE;
	cl_event_fail_code_t	cl_failure_reason = CL_FR_BUSY;
	boolean_t	post_failure_event = B_FALSE;
	boolean_t	resource_restart = B_FALSE;
	boolean_t	resource_is_restarted = B_FALSE;
	LogicalNodeset	stop_failed_ns;
	LogicalNodeset	temp_ns;
	sc_syslog_msg_handle_t handle;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	scha_errmsg_t	event_res;
	LogicalNodeset tmpNs;
	const char	*r_name = (const char *)sc_args.idlstr_R_name;
	time_t		retry_interval;
	scha_errmsg_t	retry_interval_res = {SCHA_ERR_NOERR, NULL};


	//
	// We need the following nodesets to compute the set of nodes
	// where the RG is currently ONLINE.  The current ONLINE nodeset is
	// needed to post events.
	//
	LogicalNodeset	pending_boot_ns;
	LogicalNodeset	offline_ns;
	LogicalNodeset	current_online_ns;
	LogicalNodeset	restart_ns;
	LogicalNode	*tln;
	rgm::lni_t	lni = sc_args.lni;
	tmpNs.addLni(lni);


	retval->ret_code = SCHA_ERR_NOERR;

	if (sc_args.flags & rgm::SCHACTL_CHECKONLY) {
		// CHECK option-- perform sanity/validity checks only
		checkonly = B_TRUE;
	} else if (sc_args.flags & rgm::SCHACTL_R_RESTART) {
		// RESOURCE_RESTART option
		resource_restart = B_TRUE;
	} else if (sc_args.flags & rgm::SCHACTL_R_IS_RESTARTED) {
		// RESOURCE_IS_RESTARTED option
		resource_is_restarted = B_TRUE;
	}

	rgm_lock_state();
	rgp = rgname_to_rg(sc_args.idlstr_rg_name);
	if (rgp == NULL) {
		ucmm_print("scha_control", NOGET("RG is not valid"));
		retval->ret_code = SCHA_ERR_RG;
		goto restart_end; //lint !e801
	}

	rp = rname_to_r(NULL, r_name);
	if (rp == NULL) {
		ucmm_print("scha_control", NOGET("R is not valid"));
		retval->ret_code = SCHA_ERR_RSRC;
		goto restart_end; //lint !e801
	}

	if (!is_resource_in_group(rp, rgp)) {
		// Note, is_resource_in_group() generates a syslog message
		retval->ret_code = SCHA_ERR_RG;
		goto restart_end; //lint !e801
	}

	// Make sure resource is enabled
	if (((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->r_switch[lni]
	    != SCHA_SWITCH_ENABLED) {
		ucmm_print("RGM",
		    NOGET("scha_control_restart: R <%s> is disabled; exiting"),
		    rgp->rgl_ccr->rg_name);
		retval->ret_code = SCHA_ERR_STATE;
		goto restart_end; //lint !e801
	}

	//
	// If RESOURCE_RESTART request, check if resources of this type are
	// allowed to restart.
	//
	if (resource_restart && !rt_allowed_restart(rp->rl_ccrtype)) {
		CL_PANIC(rp->rl_ccrtype->rt_name != NULL);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A resource monitor (or some other program) is attempting to
		// restart the indicated resource by calling
		// scha_control(1ha),(3ha). This request is rejected
		// and represents a bug in the calling program, because the
		// resource_restart feature can only be
		// applied to resources that have both STOP and START methods
		// or both PRENET_START and POSTNET_STOP methods without
		// START and STOP declared.
		// Instead of attempting to restart the individual resource,
		// the programmer may use scha_control(RESTART) to restart the
		// resource group.
		// @user_action
		// The resource group may be restarted manually on the same
		// node or switched to another node by using clresourcegroup
		// or the equivalent GUI command. Contact the author of the
		// data service (or of whatever program is attempting to call
		// scha_control) and report the error.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "scha_control: resource <%s> restart request is rejected "
		    "because the resource type <%s> must have both START and "
		    "STOP methods or both PRENET_START and POSTNET_STOP "
		    "methods without START and STOP declared."), r_name,
		    rp->rl_ccrtype->rt_name);
		sc_syslog_msg_done(&handle);
		retval->ret_code = SCHA_ERR_RT;
		goto restart_end; //lint !e801
	}

	//
	// (except for RESOURCE_IS_RESTARTED, which is permitted while RG
	// is in any state)
	// Make sure the RG is in an appropriate state on the given node
	// to permit the restart.
	//
	if (!resource_is_restarted) {
		if (!rg_state_permits_restart(rgp, lni, resource_restart)) {
			ucmm_print("RGM", NOGET(
			    "scha_control: RG <%s> in non-restartable state on "
			    "node <%d>"),
			    rgp->rgl_ccr->rg_name, lni);
			retval->ret_code = SCHA_ERR_STATE;
			goto restart_end; //lint !e801
		}
	}

	//
	// (except for RESOURCE_RESTART or RESOURCE_IS_RESTARTED, which are
	// permitted while RG is busy)
	// Make sure the RG is not busy.  Note, if the RG has resource(s)
	// restarting (even on node 'nodeid'), we permit the restart;
	// the state machine handles this case.
	// Note: if the RG is
	// running BOOT methods on some joining node(s), that does not
	// prevent us from restarting the RG on a current master.
	//
	if (!resource_restart && !resource_is_restarted) {
		if (!in_endish_rg_state(rgp, &pending_boot_ns, &stop_failed_ns,
		    &offline_ns, NULL)) {
			ucmm_print("RGM", NOGET(
			    "scha_control_restart: RG <%s> is busy; exiting"),
			    rgp->rgl_ccr->rg_name);
			retval->ret_code = SCHA_ERR_RGRECONF;
			cl_failure_reason = CL_FR_BUSY;
			post_failure_event = B_TRUE;
			goto restart_end; //lint !e801
		}
		//
		// ERROR_STOP_FAILED is an endish state but precludes
		// scha_control restart or giveover
		//
		if (!stop_failed_ns.isEmpty()) {
			// The RG is ERROR_STOP_FAILED on some node
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("Resource group <%s> requires operator "
			    "attention due to STOP failure"),
			    rgp->rgl_ccr->rg_name);
			sc_syslog_msg_done(&handle);
			retval->ret_code = SCHA_ERR_STOPFAILED;
			cl_failure_reason = CL_FR_BUSY;
			post_failure_event = B_TRUE;
			goto restart_end; //lint !e801
		}

		//
		// Now that we know on which nodes the RG is in
		// pending boot, stop failed and offline get the
		// nodeset on which the RG is online.
		// This is needed for posting an event.
		//
		LogicalNodeset *zone_membership =
		    lnManager->get_all_logical_nodes(rgm::LN_UP);
		current_online_ns = *zone_membership - offline_ns;
		current_online_ns -= pending_boot_ns;
		delete (zone_membership);
	}

	//
	// Perform president-local sanity checks; currently this only checks
	// for RG "frozen" on global_resources_used.  This check is not
	// performed for RESOURCE_IS_RESTARTED.
	//
	if (!resource_is_restarted) {
		retval->ret_code = pres_local_sanity_check(rgp).err_code;
		if (retval->ret_code != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "scha_control: resource group <%s> was frozen on "
			    "Global_resources_used within the past %d seconds; "
			    "exiting"),
			    rgp->rgl_ccr->rg_name, GRU_FREEZE_RECENT);
			sc_syslog_msg_done(&handle);
			cl_failure_reason = CL_FR_FROZEN;
			post_failure_event = B_TRUE;
			goto restart_end; //lint !e801
		}
	}


	//
	// If not a resource_is_restarted call, verify that the starting
	// dependencies are resolved for the calling resource on this node.
	// Otherwise, we don't want to do the restart, because the resource
	// would just end up in STARTING_DEPEND. We return SCHA_ERR_CHECKS to
	// make the caller wait until the resource's dependencies are resolved.
	//
	// Note that the assumption is that a scha_control RESTART (to
	// restart the entire RG) is for the benefit of the calling resource.
	// If the calling resource can't restart, then we don't want to
	// restart the RG.
	//
	temp_ns.addLni(lni);
	if (!resource_is_restarted) {
		if (!are_starting_dependencies_resolved(rp,
		    rp->rl_ccrdata->r_dependencies.dp_strong, DEPS_STRONG,
		    temp_ns) || !are_starting_dependencies_resolved(rp,
		    rp->rl_ccrdata->r_dependencies.dp_restart, DEPS_RESTART,
		    temp_ns) || !are_starting_dependencies_resolved(rp,
		    rp->rl_ccrdata->r_dependencies.dp_restart,
		    DEPS_OFFLINE_RESTART, temp_ns)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// A scha_control call has failed with a
			// SCHA_ERR_CHECKS error because the specified
			// resource has a resource dependency that is
			// unsatisfied on the specified node. A
			// properly-written resource monitor, upon getting the
			// SCHA_ERR_CHECKS error code from a scha_control
			// call, should sleep for awhile and restart its
			// probes.
			// @user_action
			// Usually no user action is required, because the
			// dependee resource is switching or failing over and
			// will come back online automatically. At that point,
			// either the probes will start to succeed again, or
			// the next scha_control attempt will succeed. If that
			// does not appear to be happening, you can use
			// clresource to determine the resources on which the
			// specified resource depends that are not online, and
			// bring them online.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, SYSTEXT(
			    "RGM isn't restarting resource group <%s> or "
			    "resource <%s> on node <%s> because that node "
			    "does not satisfy the strong or restart resource "
			    "dependencies of the resource."),
			    rgp->rgl_ccr->rg_name, rp->rl_ccrdata->r_name,
			    (lnManager->findObject(lni))->getFullName());
			sc_syslog_msg_done(&handle);
			retval->ret_code = SCHA_ERR_CHECKS;
			cl_failure_reason = CL_FR_NO_MASTER;
			post_failure_event = B_TRUE;
			goto restart_end; //lint !e801
		}
	}


	//
	// Check for the resource having a Retry_interval property.
	// If not, then NUM_*_RESTARTS queries are not permitted on this
	// resource, therefore we do not need to update its restart history
	// on the slave.
	//
	retry_interval_res = get_retry_interval(rp, &retry_interval);

	//
	// scha_control RESOURCE_IS_RESTARTED
	//
	// Trigger restarts of dependent resources and increment the
	// resource_restart count on the slave but don't actually restart the
	// resource.
	//
	if (resource_is_restarted) {
		ucmm_print("scha_control", NOGET(
		    "Enter resource_is_restarted: R = <%s>"), r_name);

		//
		// call trigger_restart_dependencies to trigger restarts
		// on all of this resource's restart dependents.
		//
		trigger_restart_dependencies(rp, tmpNs, B_FALSE);
		trigger_restart_dependencies(rp, tmpNs, B_TRUE);

		//
		// If the resource does not have a Retry_interval,
		// we do not need to call rgm_resource_restart, because
		// we are not tracking the number of restarts.  Log a
		// warning message, because the calling program may be
		// expecting the number of restarts to be tracked.  Then
		// just return early, with no error.
		//
		if (retry_interval_res.err_code != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// A resource monitor (or some other program) is
			// attempting to notify the RGM that the indicated
			// resource has been restarted, by calling
			// scha_control(1ha),(3ha) with the
			// RESOURCE_IS_RESTARTED option. This request will
			// trigger restart dependencies, but will not
			// increment the retry_count for the resource, because
			// the resource type does not declare the
			// Retry_interval property for its resources. To
			// enable the NUM_RESOURCE_RESTARTS query, the
			// resource type registration (RTR) file must declare
			// the Retry_interval property.
			// @user_action
			// Contact the author of the data service (or of
			// whatever program is attempting to call
			// scha_control) and report the warning.
			//
			(void) sc_syslog_msg_log(handle, LOG_WARNING, MESSAGE,
			    SYSTEXT(
			    "scha_control: warning, RESOURCE_IS_RESTARTED call"
			    " on resource <%s> will not increment the "
			    "resource_restart count because the resource "
			    "does not have a Retry_interval property"),
			    r_name);
			sc_syslog_msg_done(&handle);
			retval->ret_code = SCHA_ERR_NOERR;
			goto restart_end; //lint !e801
		}

		//
		// Note: We do not post an event for scha_control
		// RESOURCE_IS_RESTARTED.  For all Sun-supplied resource
		// types and those using DSDL, there will be PMF events
		// generated for any resource restart that is not done
		// using scha_control RESOURCE_RESTART; any additional
		// event generated here would be redundant.
		//
		// For resource types that do not use DSDL, we do not know
		// enough about how a resource might be restarted to draw
		// any conclusions from the fact that there was a
		// RESOURCE_IS_RESTARTED call.
		//

		//
		// Calling rgm_resource_restart with RGM_RRF_RIR tells the
		// slave that this is a "RESOURCE_IS_RESTARTED" call.  This
		// just updates the resource_restart history list for this
		// resource on the slave.
		//
		switch (rgm_resource_restart(lni, rgm::RGM_RRF_RIR,
		    r_name)) {
		case RGMIDL_OK:
			break;
		case RGMIDL_NODE_DEATH:
		case RGMIDL_RGMD_DEATH:
		case RGMIDL_NOREF:
			// The slave node died or rgmd died; return
			// success to the caller (since a node death effectively
			// overrides a restart request)
			ucmm_print("scha_control", NOGET(
			    "rgm_resource_restart() failed with NODE_DEATH, "
			    "RGMD_DEATH, or NOREF err"));
			goto restart_end; //lint !e801
		case RGMIDL_UNKNOWN:
		default:
			// Something went wrong on rgmd or this node;
			// return SCHA_ERR_INTERNAL to the caller
			ucmm_print("scha_control", NOGET(
			    "rgm_resource_restart() failed with UNKNOWN or "
			    "invalid err"));
			retval->ret_code = SCHA_ERR_INTERNAL;
			goto restart_end; //lint !e801
		}

		goto restart_end; //lint !e801
	}


	//
	// scha_control RESOURCE_RESTART
	//
	// Attempt to restart the resource on the slave.  This will also
	// stop and restart the monitor on the slave.
	//
	// Do not set rgl_switching because the restart of one resource
	// does not preclude scswitch or scha_control actions upon the RG.
	// Edit, enable, and disable actions are still precluded because
	// they check specifically for the ON_PENDING_R_RESTART RG state.
	//
	if (resource_restart) {
		ucmm_print("scha_control", NOGET(
		    "Enter resource restart: R = <%s>"), r_name);

		rdeplist 	*dep;
		rlist_p_t	rp_dep;

		//
		// Trigger restarts of intra-rg offline-restart dependents of
		// rp. The restart is carried out is local_node as the intra-RG
		// dependencies are forced to be local_node
		//
		for (dep = rp->rl_dependents.dp_offline_restart; dep != NULL;
		    dep = dep->rl_next) {
			rp_dep = rname_to_r(NULL, dep->rl_name);
			if (strcmp(rgp->rgl_ccr->rg_name,
			    rp_dep->rl_rg->rgl_ccr->rg_name) == 0) {
				trigger_restart(rp_dep, tmpNs);
			}
		}

		//
		// Set the "beginning to restart" flag for this resource
		// in the president's memory.  This flag enables the
		// r_is_restarting() function to differentiate a
		// resource that is just beginning to restart from one
		// that is finished restarting.
		//
		// This flag is cleared as soon as the president
		// uploads a state change for this resource from the slave.
		//
		rp->rl_xstate[lni].rx_restarting = B_TRUE;

		// Post an event for the resource restart
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE, ESC_CLUSTER_FM_R_RESTARTING,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO, CL_EVENT_INIT_AGENT,
		    0, 0, 0,
		    CL_R_NAME, SE_DATA_TYPE_STRING, r_name,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rgp->rgl_ccr->rg_name,
		    CL_NODE_NAME, SE_DATA_TYPE_STRING,
		    (lnManager->findObject(lni))->getFullName(), NULL);
#else
		(void) sc_publish_event(ESC_CLUSTER_FM_R_RESTARTING,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO, CL_EVENT_INIT_AGENT,
		    0, 0, 0,
		    CL_R_NAME, SE_DATA_TYPE_STRING, r_name,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rgp->rgl_ccr->rg_name,
		    CL_NODE_NAME, SE_DATA_TYPE_STRING,
		    (lnManager->findObject(lni))->getFullName(), NULL);
#endif
		//
		// Calling rgm_resource_restart with RGM_RRF_R means that this
		// is a resource restart.  The slave state machine will do the
		// work of restarting the resource.
		//
		switch (rgm_resource_restart(lni, rgm::RGM_RRF_R,
		    r_name)) {
		case RGMIDL_OK:

			//
			// Now flush the state change queue to make
			// sure we have the latest updates from the slave.
			//
			rgm_unlock_state();
			flush_state_change_queue();
			rgm_lock_state();
			break;
		case RGMIDL_NODE_DEATH:
		case RGMIDL_RGMD_DEATH:
		case RGMIDL_NOREF:
			// The slave node died or rgmd died; return
			// success to the caller (since a node death effectively
			// overrides the restart request)
			ucmm_print("scha_control", NOGET(
			    "rgm_resource_restart() failed with NODE_DEATH, "
			    "RGMD_DEATH, or NOREF err"));
			goto restart_end; //lint !e801
		case RGMIDL_UNKNOWN:
		default:
			// Something went wrong on rgmd or this node;
			// return SCHA_ERR_INTERNAL to the caller
			ucmm_print("scha_control", NOGET(
			    "rgm_resource_restart() failed with UNKNOWN or "
			    "invalid err"));
			retval->ret_code = SCHA_ERR_INTERNAL;
			goto restart_end; //lint !e801
		}

		// Wait for R to finish restarting on slave
		while (r_is_restarting(rp, lni, &retval->ret_code)) {
			cond_slavewait.wait(&Rgm_state->rgm_mutex);
		}

		// The call to r_is_restarting already set the error code

		goto restart_end; //lint !e801
	}

	//
	// scha_control RESTART (restarting the RG)
	//

	ucmm_print("scha_control", NOGET(
	    "Enter idl_scha_control_restart(): R = <%s>"), r_name);

	// If we are only doing sanity checks, we are done.
	if (checkonly) {
		goto restart_end; //lint !e801
	}

	// Setting the switching flag so during the restart
	// period, no other scswitch or scha_control operation
	// could be performed.
	rgp->rgl_switching = SW_IN_PROGRESS;

	//
	// Calling rgm_resource_restart with RGM_RRF_RG tells the slave that
	// this is an RG restart.  This call just updates the rg_restart
	// history list for this resource on the slave.  We do the remainder
	// of the restarting work below by calling rgm_change_mastery().
	//
	// If the resource's type does not declare the Retry_interval property,
	// then we need not update the restart history because NUM_RG_RESTARTS
	// queries are illegal on such a resource.
	//
	if (retry_interval_res.err_code == SCHA_ERR_NOERR) {
		switch (rgm_resource_restart(lni, rgm::RGM_RRF_RG, r_name)) {
		case RGMIDL_OK:
			break;
		case RGMIDL_NODE_DEATH:
		case RGMIDL_RGMD_DEATH:
		case RGMIDL_NOREF:
			// The slave node died or rgmd died; return
			// success to the caller (since a node death effectively
			// overrides the restart request)
			ucmm_print("scha_control", NOGET(
			    "rgm_resource_restart() failed with NODE_DEATH, "
			    "RGMD_DEATH, or NOREF err"));
			goto alldone; //lint !e801
		case RGMIDL_UNKNOWN:
		default:
			// Something went wrong on rgmd or this node;
			// return SCHA_ERR_INTERNAL to the caller
			ucmm_print("scha_control", NOGET(
			    "rgm_resource_restart() failed with UNKNOWN or "
			    "invalid err"));
			retval->ret_code = SCHA_ERR_INTERNAL;
			goto alldone; //lint !e801
		}
	}

	// Create the nodeset on which Restart is done
	restart_ns.addLni(lni);

	// Post an event for this RG
	event_res = post_primaries_changing_event(r_name,
	    rgp->rgl_ccr->rg_name, CL_REASON_RG_SC_RESTART,
	    (lnManager->findObject(lni))->getFullName(),
	    rgp->rgl_ccr->rg_desired_primaries,
	    current_online_ns, restart_ns);
	if (event_res.err_code == SCHA_ERR_NOMEM) {
		retval->ret_code = SCHA_ERR_NOMEM;
		goto alldone; //lint !e801
	}

	rg.nl_name = new_str(sc_args.idlstr_rg_name);
	rg.nl_next = NULL;

	//
	// Before taking the RG offline, add all nodes where the RG is
	// currently online to the RG's planned_on nodeset so that if one
	// of those nodes dies, this command will be busted and the RG will
	// be rebalanced.
	//
	// The planned_on nodeset includes the node on which the RG is
	// being restarted, so process_needy_rg will ignore that node as
	// a potential primary for strong negative affinitents of the RG.
	//
	// The planned_on_nodes will be reset below, before exiting.
	//
	rgp->rgl_planned_on_nodes = current_online_ns;

	// Turn the RG off on the node first.
	switch (rgm_change_mastery(lni, NULL, &rg)) {
	case RGMIDL_OK:
		//
		// Now flush the state change queue to make
		// sure we have the latest updates from the slave.
		//
		rgm_unlock_state();
		flush_state_change_queue();
		rgm_lock_state();
		break;
	case RGMIDL_NODE_DEATH:
	case RGMIDL_RGMD_DEATH:
	case RGMIDL_NOREF:
		// For these errors, this node might have died or rgmd died,
		// return SCHA_ERR_MEMBERCHG to the caller
		ucmm_print("scha_control", NOGET(
		    "rgm_change_mastery() failed with NODE_DEATH, RGMD_DEATH "
		    "or NOREF err"));
		retval->ret_code = SCHA_ERR_MEMBERCHG;
		goto alldone; //lint !e801
	case RGMIDL_UNKNOWN:
	default:
		// For this error, something went wrong on rgmd or this node,
		// return SCHA_ERR_INTERNAL to the caller
		ucmm_print("scha_control", NOGET(
		    "rgm_change_mastery() failed with UNKNOWN or invalid err"));
		retval->ret_code = SCHA_ERR_INTERNAL;
		goto alldone; //lint !e801
	}

	// If stop RG failed on this node, exit with error.
	while (!rg_is_stopped) {
		cond_slavewait.wait(&Rgm_state->rgm_mutex);

		// Check if switch is busted; this could be result of
		// stop-failed or node death.
		if ((ret = map_busted_err(rgp->rgl_switching)) !=
		    SCHA_ERR_NOERR) {
			ucmm_print("scha_control",
			    NOGET("switch was busted\n"));
			retval->ret_code = (int32_t)ret;
			goto alldone; //lint !e801
		}

		switch (rgp->rgl_xstate[lni].rgx_state) {
		case rgm::RG_OFFLINE:
			rg_is_stopped = B_TRUE;
			break;

		case rgm::RG_PENDING_OFF_STOP_FAILED:
		case rgm::RG_ERROR_STOP_FAILED:
			//
			// If RG stop failed on this node, the set_rgx_state
			// code should bust the switch, causing us to exit
			// above.
			// Just to be safe (or paranoid), we'll bust it
			// ourself and exit here.
			//
			ucmm_print("scha_control", NOGET(
			    "scha_control_restart: stop RG <%s>"
			    " failed on node <%d>"),
			    rgp->rgl_ccr->rg_name, lni);
			retval->ret_code = SCHA_ERR_STOPFAILED;
			goto alldone; //lint !e801

		case rgm::RG_ONLINE:
		case rgm::RG_ON_PENDING_R_RESTART:
		case rgm::RG_PENDING_ONLINE:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_BOOT:
		case rgm::RG_OFF_BOOTED:
		case rgm::RG_PENDING_ON_STARTED:
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_ON_PENDING_MON_DISABLED:
		case rgm::RG_PENDING_OFF_START_FAILED:
		case rgm::RG_OFFLINE_START_FAILED:
			ucmm_print("scha_control", NOGET(
			    "RG <%s> falls into incorrect state <%s>"),
			    rgp->rgl_ccr->rg_name, pr_rgstate(
			    rgp->rgl_xstate[lni].rgx_state));
			retval->ret_code = SCHA_ERR_INTERNAL;
			goto alldone; //lint !e801

		case rgm::RG_PENDING_OFFLINE:
			break;

		default:
			ucmm_print("scha_control", NOGET(
			    "RG <%s> falls into illegal state"),
			    rgp->rgl_ccr->rg_name);
			retval->ret_code = SCHA_ERR_INTERNAL;
			goto alldone; //lint !e801
		}
		ucmm_print("scha_control", NOGET(
		    "**** after cond_wait(), flag = <%d>\n"),
		    rg_is_stopped);
	}

	// Bring RG up again on the same node.
	switch (rgm_change_mastery(lni, &rg, NULL)) {
	case RGMIDL_OK:
		//
		// Now flush the state change queue to make
		// sure we have the latest updates from the slave.
		//
		rgm_unlock_state();
		flush_state_change_queue();
		rgm_lock_state();
		break;
	case RGMIDL_NODE_DEATH:
	case RGMIDL_RGMD_DEATH:
	case RGMIDL_NOREF:
		// For these errors, node lni might have died or rgmd died,
		// return SCHA_ERR_MEMBERCHG to the caller
		ucmm_print("scha_control", NOGET(
		    "rgm_change_mastery() failed with NODE_DEATH, RGMD_DEATH "
		    "or NOREF err"));
		retval->ret_code = SCHA_ERR_MEMBERCHG;
		goto alldone; //lint !e801
	case RGMIDL_UNKNOWN:
	default:
		// For this error, something went wrong on rgmd or this node,
		// return SCHA_ERR_INTERNAL to the caller
		ucmm_print("scha_control", NOGET(
		    "rgm_change_mastery() failed with UNKNOWN or invalid err"));
		retval->ret_code = SCHA_ERR_INTERNAL;
		goto alldone; //lint !e801
	}

	while (!rg_is_started) {
		cond_slavewait.wait(&Rgm_state->rgm_mutex);

		//
		// Check if switch is busted; this could be result of
		// start-failed or node death.  The set_rg_state thread will
		// have called rebalance() after busting the switch.
		//
		if ((ret = map_busted_err(rgp->rgl_switching)) !=
		    SCHA_ERR_NOERR) {
			ucmm_print("scha_control",
			    NOGET("switch was busted\n"));
			retval->ret_code = (int32_t)ret;
			goto alldone; //lint !e801
		}

		switch (rgp->rgl_xstate[lni].rgx_state) {
		case rgm::RG_ONLINE:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
			// PENDING_ONLINE_BLOCKED is as started as it's going
			// to get for now.
		case rgm::RG_ON_PENDING_R_RESTART:
			rg_is_started = B_TRUE;
			break;

		case rgm::RG_PENDING_OFF_STOP_FAILED:
		case rgm::RG_ERROR_STOP_FAILED:
			//
			// If RG stop failed on this node, the rg_set_state
			// code should bust the switch, causing us to exit
			// above.
			// Just to be safe (or paranoid), we'll bust it
			// ourself and exit here.
			//
			ucmm_print("scha_control", NOGET(
			    "scha_control_restart: stop RG <%s>"
			    " failed on node <%d>"),
			    rgp->rgl_ccr->rg_name, lni);
			retval->ret_code = SCHA_ERR_STOPFAILED;
			goto alldone; //lint !e801
		case rgm::RG_OFFLINE_START_FAILED:
			// If RG start failed on this node, the rg_set_state
			// code will bust the switch, reset the RG state to
			// OFFLINE, and call rebalance.
			// Just in case there is a legitimate way to get here
			// first, we will bust the switch ourself and exit.
			//
			// A single start-failure is not sufficient to
			// prevent another start attempt on this node,
			// so we do not call touch_file.  Worst case: we
			// will fail to start again.  If rebalance() makes
			// more than RG_START_RETRY_MAX failed attempts to
			// start the RG within Pingpong_interval, it
			// ultimately will disqualify the node.
			//
			ucmm_print("scha_control", NOGET(
			    "scha_control_restart:"
			    "start RG <%s> failed on node <%d>\n"),
			    rgp->rgl_ccr->rg_name, lni);
			retval->ret_code = SCHA_ERR_STARTFAILED;
			goto alldone; //lint !e801
		case rgm::RG_OFFLINE:
		case rgm::RG_PENDING_OFFLINE:
		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_BOOT:
		case rgm::RG_OFF_BOOTED:
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_ON_PENDING_MON_DISABLED:
			ucmm_print("scha_control", NOGET(
			    "RG <%s> falls into incorrect state <%s>"),
			    rgp->rgl_ccr->rg_name, pr_rgstate(
			    rgp->rgl_xstate[lni].rgx_state));
			retval->ret_code = SCHA_ERR_INTERNAL;
			goto alldone; //lint !e801

		case rgm::RG_PENDING_ONLINE:
		case rgm::RG_PENDING_ON_STARTED:
		case rgm::RG_PENDING_OFF_START_FAILED:
			break;

		default:
			ucmm_print("scha_control", NOGET(
			    "RG <%s> falls into illegal state"),
			    rgp->rgl_ccr->rg_name);
			retval->ret_code = SCHA_ERR_INTERNAL;
			goto alldone; //lint !e801
		}
		ucmm_print("scha_control", NOGET(
		    "**** after cond_wait(), flag = <%d>\n"),
		    rg_is_started);
	}

alldone:
	//
	// We only get here if we did an RG restart.  resource_restart
	// jumps straight to restart_end.
	//
	// reset the planned_on nodeset and, if the RG did not start properly,
	// call run_rebalance_on_affinitents to give strong negative
	// affinitents a chance to start on the node.
	//
	rgp->rgl_planned_on_nodes.reset();
	if (!rg_is_started) {
		// call with cleanup_stale_affs set to false.
		run_rebalance_on_affinitents(rgp, B_FALSE, B_FALSE);
	}
	if (rgp->rgl_switching != SW_EVACUATING) {
		rgp->rgl_switching = SW_NONE;
	}
restart_end:
	// if there was an error, post a failure event
	if (!checkonly && post_failure_event)
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE,
		    ESC_CLUSTER_RG_GIVEOVER_DEFERRED,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
		    CL_EVENT_INIT_AGENT, 0, 0, 0,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rgp->rgl_ccr->rg_name,
		    CL_FAILURE_REASON, SE_DATA_TYPE_UINT32,
		    cl_failure_reason, NULL);
#else
		(void) sc_publish_event(ESC_CLUSTER_RG_GIVEOVER_DEFERRED,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
		    CL_EVENT_INIT_AGENT, 0, 0, 0,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rgp->rgl_ccr->rg_name,
		    CL_FAILURE_REASON, SE_DATA_TYPE_UINT32,
		    cl_failure_reason, NULL);
#endif
	if (rg.nl_name)
		delete [] rg.nl_name;
	sc_res = retval;
	rgm_unlock_state();
}

void
rgm_comm_impl::idl_scha_control_disable(
	const rgm::idl_scha_control_args &sc_args,
	rgm::idl_regis_result_t_out sc_res,
	Environment &)
{
	rglist_p_t	rgp = NULL;
	rlist_p_t	rp = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_err_t busted = SCHA_ERR_NOERR;
	sc_syslog_msg_handle_t handle;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	char *r_name = strdup(sc_args.idlstr_R_name);
	char *rg_name = strdup(sc_args.idlstr_rg_name);
	LogicalNodeset r_restart_ns;
	LogicalNodeset pending_boot_ns;
	LogicalNodeset stop_failed_ns;
	rgm::lni_t lni = sc_args.lni;
	LogicalNodeset emptyNodeset;
	LogicalNodeset ns;

	ucmm_print("RGM", NOGET("enter the idl_scha_control_disable()\n"));
	retval->ret_code = SCHA_ERR_NOERR;

	rgm_lock_state();

	rgp = rgname_to_rg(rg_name);
	if (rgp == NULL) {
		ucmm_print("scha_control", NOGET("RG is not valid"));
		retval->ret_code = SCHA_ERR_RG;
		goto disable_end; //lint !e801
	}
	rg_name = rgp->rgl_ccr->rg_name;

	rp = rname_to_r(NULL, r_name);
	if (rp == NULL) {
		ucmm_print("scha_control", NOGET("R is not valid"));
		retval->ret_code = SCHA_ERR_RSRC;
		goto disable_end; //lint !e801
	}
	r_name = rp->rl_ccrdata->r_name;

	if (!is_resource_in_group(rp, rgp)) {
		//
		// Note, is_resource_in_group() generates a syslog message.
		//
		retval->ret_code = SCHA_ERR_RG;
		goto disable_end;
	}

	if (!nodeidlist_contains_lni(rgp->rgl_ccr->rg_nodelist, lni)) {
		ucmm_print("idl_scha_control_disable", NOGET("Node is not in "
		    "resource group's nodelist"));
		retval->ret_code = SCHA_ERR_NODE;
		goto disable_end; //lint !e801
	}

	if (!in_endish_rg_state(rgp, &pending_boot_ns,
	    &stop_failed_ns, NULL, &r_restart_ns) ||
	    !r_restart_ns.isEmpty() || !pending_boot_ns.isEmpty()) {
		ucmm_print("idl_scha_control_disable", NOGET(
		    "RG <%s> is not in endish state\n"),
		    rg_name);
		retval->ret_code = SCHA_ERR_RGRECONF;
		goto disable_end; //lint !e801
	}
	if (!stop_failed_ns.isEmpty()) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RG_TAG, rgp->rgl_ccr->rg_name);
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Resource group <%s> requires operator attention "
		    "due to STOP failure", rgp->rgl_ccr->rg_name);
		sc_syslog_msg_done(&handle);
		retval->ret_code = SCHA_ERR_STOPFAILED;
		goto disable_end; //lint !e801
	}

	if (rgp->rgl_ccr->rg_system) {
		ucmm_print("idl_scha_control_disable", NOGET("R <%s> is "
		    "contained in RG <%s> whose RG_SYSTEM property "
		    "is TRUE\n"), rp->rl_ccrdata->r_name, rg_name);
		retval->ret_code = SCHA_ERR_ACCESS;
		goto disable_end; //lint !e801
	}

	if (((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->r_switch[lni] !=
	    SCHA_SWITCH_ENABLED) {
		ucmm_print("idl_scha_control_disable", NOGET("R <%s> is "
		    "already disabled on this node <%s>."), r_name,
		    rgm_lni_to_nodename(lni));
		retval->ret_code = SCHA_ERR_NOERR;
		goto disable_end; //lint !e801
	}


	ns.addLni(lni);
	(void) latch_intention(rgm::RGM_ENT_RS, rgm::RGM_OP_DISABLE_R,
	    r_name, &emptyNodeset, 0);

	if ((res = rgmcnfg_set_onoffswitch(r_name, rg_name,
	    SCHA_SWITCH_DISABLED, &ns)).err_code != SCHA_ERR_NOERR) {

		// CCR update failed. Stop further processing and return early.
		ucmm_print("idl_scha_control_disable", NOGET("CCR update of "
		    "on/off flag failed for resource %s on node %s.\n"),
		    r_name, rgm_lni_to_nodename(lni));
		(void) unlatch_intention();
		goto disable_end; //lint !e801
	}

	// Log resource PROPERTY_CHANGED event for SC Manager.
	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RS_TAG, r_name);
	//
	// SCMSGS
	// @explanation
	// This is a notification from the rgmd that the operator has disabled
	// a resource. This message can be used by system monitoring tools.
	// @user_action
	// This is an informational message; no user action is needed.
	//
	(void) sc_syslog_msg_log(handle, LOG_NOTICE, PROPERTY_CHANGED,
	    SYSTEXT("resource %s disabled."), r_name);

	// post an event
#if SOL_VERSION >= __s10
	(void) sc_publish_zc_event(ZONE, ESC_CLUSTER_R_CONFIG_CHANGE,
	    CL_EVENT_PUB_RGM,
	    CL_EVENT_SEV_INFO, CL_EVENT_INIT_SYSTEM, 0, 0, 0, CL_RG_NAME,
	    SE_DATA_TYPE_STRING, rg_name, CL_R_NAME, SE_DATA_TYPE_STRING,
	    r_name, CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
	    CL_EVENT_CONFIG_DISABLED, NULL);
#else
	(void) sc_publish_event(ESC_CLUSTER_R_CONFIG_CHANGE, CL_EVENT_PUB_RGM,
	    CL_EVENT_SEV_INFO, CL_EVENT_INIT_SYSTEM, 0, 0, 0, CL_RG_NAME,
	    SE_DATA_TYPE_STRING, rg_name, CL_R_NAME, SE_DATA_TYPE_STRING,
	    r_name, CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
	    CL_EVENT_CONFIG_DISABLED, NULL);
#endif
	sc_syslog_msg_done(&handle);

	// Notify all nodes to update R and run state machine if needed.
	(void) process_intention();

	// processing the intention causes the resource to be reloaded into
	// memory from the CCR, so r_name is no longer valid. r_ptr is still
	// valid because only the ccrdata portion of the resource is changed.

	r_name = rp->rl_ccrdata->r_name;

	// Unlatch the intention.
	(void) unlatch_intention();

	// Set the rgl_switching flag on the RG.  Setting it to
	// SW_IN_PROGRESS permits the switch to be "busted"
	rgp->rgl_switching = SW_IN_PROGRESS;

	rgm_unlock_state();
	flush_state_change_queue();
	rgm_lock_state();

	busted = map_busted_err(rgp->rgl_switching);
	if (busted == SCHA_ERR_NOERR) {
		busted = wait_for(rgp, rg_name, rgm::RG_ON_PENDING_DISABLED);
	}

	res.err_code = SCHA_ERR_NOERR;
	append_busted_msg(&res, rgp, busted);
	// We will set res.err_code below

	if (rgp->rgl_switching != SW_EVACUATING)
		rgp->rgl_switching = SW_NONE;

	if (res.err_code == SCHA_ERR_NOERR && busted != SCHA_ERR_NOERR)
		res.err_code = busted;
	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);
	free(res.err_msg);

disable_end :
	sc_res = retval;
	rgm_unlock_state();
}
#endif // !EUROPA_FARM

//
// idl_scha_control_checkall()
// This function is running on each candidate node to check
// whether the node is healthy enough to take over RG. The
// checking is by checking:
//	a. sanity check
//	b. monitor check
//	c. pingpong check
// Only the node which passes all three tests could be considered
// as healthy node.
//
// This function passes back message for each checking method on specific node.
// And to simplify coding, rgm_sprintf() is re-used to bundle log
// message into string.
//
//
#ifdef EUROPA_FARM
bool_t
rgmx_scha_control_checkall_1_svc(rgmx_scha_control_checkall_args *rpc_args,
    rgmx_scha_control_result *rpc_res, struct svc_req *)
#else
void
rgm_comm_impl::idl_scha_control_checkall(
	sol::nodeid_t president,
	sol::incarnation_num incarnation,
	rgm::ln_incarnation_t ln_incarnation,
	const rgm::idl_scha_control_checkall_args &args,
	rgm::idl_scha_control_result_out chk_res,
	Environment &env)
#endif
{
	rlist_p_t	rp;
	rglist_p_t	rgp;
	scha_err_t	ret;
	rgm::idl_scha_control_result *retval =
	    new rgm::idl_scha_control_result();
	scha_errmsg_t	syslog_res = {SCHA_ERR_NOERR, NULL};
	LogicalNode *ln = NULL;

#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	rgm::ln_incarnation_t ln_incarnation = rpc_args->ln_incarnation;
	rgm::idl_scha_control_checkall_args args;
	rgm::idl_scha_control_result *chk_res;

	bzero(rpc_res, sizeof (rgmx_scha_control_result));

	args.lni = rpc_args->lni;
	args.idlstr_R_name = strdup_nocheck(rpc_args->idlstr_R_name);
	args.flags = rpc_args->flags;

	rpc_res->retval = RGMRPC_OK;
#endif
	retval->ret_code = SCHA_ERR_NOERR;

	ucmm_print("check", NOGET(
	    "enter idl_scha_control_checkall()"));

	rgm_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		// The return code shouldn't matter, as the caller is dead
		//
		retval->ret_code = SCHA_ERR_NODE;
		goto checkall_end; //lint !e801
	}

	// Return early with IDL exception if zone died.
#ifndef EUROPA_FARM
	if (zone_died(args.lni, ln_incarnation, env)) {
		goto checkall_end; //lint !e801
	}
#else
	if (zone_died(args.lni, ln_incarnation, &rpc_res->retval)) {
		goto checkall_end; //lint !e801
	}
#endif

	ln = lnManager->findObject(args.lni);
	CL_PANIC(ln != NULL);

	CL_PANIC((rp = rname_to_r(NULL, (const char *)args.idlstr_R_name))
	    != NULL);

	rgp = rp->rl_rg;

	if (am_i_president() &&
	    (ret = map_busted_err(rgp->rgl_switching)) != SCHA_ERR_NOERR) {
		// Check switching flag. If the RG is already busted
		// on the node (it should be the president), then don't
		// do anything but early return.
		ucmm_print("scha_control", NOGET(
		    "RG <%s> is busted"), rgp->rgl_ccr->rg_name);
		retval->ret_code = (int32_t)ret;
		goto checkall_end; //lint !e801
	}

	//
	// Run pingpong test first, we can avoid unnecessary
	// check on the node which would fail pingpong check.
	//
	// If GETOFF flag is set, skip pingpong check. This will
	// only happened when running on master node.
	//
	if ((args.flags & rgm::SCHACTL_GETOFF) == 0) {
		if ((ret = pingpong_check(args.idlstr_R_name,
		    rgp->rgl_ccr->rg_ppinterval, args.lni).err_code) !=
		    SCHA_ERR_NOERR) {
			ucmm_print("check", NOGET(
			    "Not pass pingpong check on node <%d>\n"),
			    args.lni);
			rgm_sprintf(&syslog_res,
			    //
			    // SCMSGS
			    // @explanation
			    // A scha_control(1HA,3HA) call has rejected the
			    // indicated node as a new master for the resource
			    // group, because the indicated
			    // resource has recently initiated a failover off
			    // of that node by a previous scha_control call.
			    // In this context, "recently" means within the
			    // past Pingpong_interval seconds, where
			    // Pingpong_interval is a user-configurable
			    // property of the resource group. The default
			    // value of Pingpong_interval is 3600 seconds.
			    // For more information, see 'Pingpong_interval'
			    // in the rg_properties(5) man page.
			    // This check is performed to avoid the situation
			    // where a resource group repeatedly "ping-pongs"
			    // or moves back and forth between two or more
			    // nodes, which might occur if some external
			    // problem prevents the resource group from
			    // running successfuly on &star;any&star; node.
			    // @user_action
			    // No action is required.
			    // Sun Cluster will continue to check other
			    // eligible nodes to see if they are healthy
			    // enough to master the resource group.
			    // If the system
			    // administrator wishes to permit failovers to be
			    // attempted even at the risk of ping-pong
			    // behavior, the Pingpong_interval property of the
			    // resource group can be set to a smaller value.
			    //
			    SC_SYSLOG_MESSAGE(
			    "Resource <%s> of Resource Group <%s> failed "
			    "pingpong check on node <%s>.  The resource group "
			    "will not be mastered by that node."),
			    rp->rl_ccrdata->r_name, rgp->rgl_ccr->rg_name,
			    ln->getFullName());
			retval->idlstr_syslog_msg =
			    (const char *)syslog_res.err_msg;
			free(syslog_res.err_msg);
			retval->ret_code = SCHA_ERR_PINGPONG;
			goto checkall_end; //lint !e801
		}
	}
	if ((ret = sanity_check(args.idlstr_R_name, args.lni).err_code) !=
	    SCHA_ERR_NOERR) {
		ucmm_print("check", NOGET(
		    "Not pass sanitry_check on node <%d>"),
		    args.lni);
		rgm_sprintf(&syslog_res,
		    //
		    // SCMSGS
		    // @explanation
		    // Message logged for failed scha_control sanity check
		    // methods on specific node.
		    // @user_action
		    // No user action required.
		    //
		    SC_SYSLOG_MESSAGE("Resource <%s> of Resource Group <%s> "
		    "failed sanity check on node <%s>\n"),
		    rp->rl_ccrdata->r_name, rgp->rgl_ccr->rg_name,
		    ln->getFullName());
		retval->idlstr_syslog_msg =
		    (const char *)syslog_res.err_msg;
		free(syslog_res.err_msg);
		retval->ret_code = (int32_t)ret;
		goto checkall_end; //lint !e801
	}

	// Run monitor checking on the node.
	if ((ret = monitor_check(rp, args.lni).err_code) !=
	    SCHA_ERR_NOERR) {
		ucmm_print("check", NOGET(
		    "Not pass monitor check on node <%d>\n"),
		    args.lni);
		rgm_sprintf(&syslog_res,
		    //
		    // SCMSGS
		    // @explanation
		    // Message logged for failed scha_control monitor check
		    // methods on specific node.
		    // @user_action
		    // No user action required.
		    //
		    SC_SYSLOG_MESSAGE("Resource <%s> of Resource Group <%s> "
		    "failed monitor check on node <%s>\n"),
		    rp->rl_ccrdata->r_name, rgp->rgl_ccr->rg_name,
		    ln->getFullName());
		retval->idlstr_syslog_msg =
		    (const char *)syslog_res.err_msg;
		free(syslog_res.err_msg);
		retval->ret_code = (int32_t)ret;
		goto checkall_end; //lint !e801
	}

checkall_end:
	chk_res = retval;
	rgm_unlock_state();
#ifdef EUROPA_FARM
	rpc_res->ret_code = chk_res->ret_code;
	if ((char *)chk_res->idlstr_err_msg == NULL)
		rpc_res->idlstr_err_msg = NULL;
	else
		rpc_res->idlstr_err_msg = strdup_nocheck(
		    chk_res->idlstr_err_msg);
	if ((char *)chk_res->idlstr_syslog_msg == NULL)
		rpc_res->idlstr_syslog_msg = NULL;
	else
		rpc_res->idlstr_syslog_msg = strdup_nocheck(
		    chk_res->idlstr_syslog_msg);
	return (TRUE);
#endif
}


//
// idl_set_timestamp()
// This function is called by the president to set timestamp of R on the node
// from which scha_control is issued.
//
#ifdef EUROPA_FARM
bool_t
rgmx_set_timestamp_1_svc(rgmx_set_timestamp_args *rpc_args,
    rgmx_regis_result_t *rpc_ti_res,
    struct svc_req *)
#else
void
rgm_comm_impl::idl_set_timestamp(const char *R_name, rgm::lni_t lni,
	rgm::idl_regis_result_t_out ti_res, Environment &)
#endif
{
#ifdef EUROPA_FARM
	char *R_name = rpc_args->R_name;
	rgm::lni_t lni = rpc_args->lni;
	rgm::idl_regis_result_t *ti_res;

	bzero(rpc_ti_res, sizeof (rgmx_regis_result_t));
	R_name = rpc_args->R_name;
#endif
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();

	retval->ret_code = SCHA_ERR_NOERR;

	slave_lock_state(); // since touch_file needs state lock
	if (touch_file(R_name, lni)) {
		retval->ret_code = SCHA_ERR_NOERR;
	} else {
		// Failed to create timestamp file on the
		// local node. Return with error.
		retval->ret_code = SCHA_ERR_TIMESTAMP;
	}
	slave_unlock_state();
	ti_res = retval;
#ifdef EUROPA_FARM
	rpc_ti_res->ret_code = ti_res->ret_code;
	if ((char *)ti_res->idlstr_err_msg == NULL)
		rpc_ti_res->idlstr_err_msg = NULL;
	else
		rpc_ti_res->idlstr_err_msg = strdup_nocheck(
			ti_res->idlstr_err_msg);

	return (TRUE);
#endif
}


//
// get_timestamp()
// This function get a particular R's timestamp from
// its timestamp file on the local node.
// If the file does not exist, assume the event which would have created the
// timestamp file (e.g., a scha_control Giveover call or a stop_failed
// reboot) has not occurred for this R on the given zone.
// In this case, 0 is returned.
//
// Otherwise, return the timestamp.
//
// This function does not create the timestamp file -- see touch_file().
//
// r_name is a resource name, optionally followed by a single suffix character.
// '#' is used as the suffix character by launch_method() for a stop_failed
// reboot timestamp file.
//
scha_errmsg_t
get_timestamp(const char *r_name, time_t *ti, rgm::lni_t lni)
{
	char fname[sizeof (PP_PATH) + ZONENAME_MAX + MAXRGMOBJNAMELEN
	    + 3];	// "${PP_PATH}/zonename/resourcename#"
	struct stat r_stat;
	sc_syslog_msg_handle_t handle;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	int my_errno;

	// store the filename into fname
	compute_timestamp_fname(r_name, lni, fname, NULL);

	if (stat(fname, &r_stat) == 0) {
		ucmm_print("get_timestamp", NOGET(
		    "Timestampe for R <%s> is <%d>"), r_name,
		    r_stat.st_mtime);
		*ti = r_stat.st_mtime;
		res.err_code = SCHA_ERR_NOERR;
		return (res);
	} else {
		my_errno = errno;
		char *s = strerror(my_errno);
		if (my_errno == ENOENT) {
			// If the file not exist.
			*ti = 0;
			res.err_code = SCHA_ERR_NOERR;
			return (res);
		} else {
			// If due to other reasons we cannot
			// access the file, exit with error.
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd has failed in an attempt to stat(2) a file
			// used for the anti-"pingpong" feature. This may
			// prevent the anti-pingpong feature from working,
			// which may permit a resource group to fail over
			// repeatedly between two or more nodes. The failure
			// to access the file might indicate a more serious
			// problem on the node.
			// @user_action
			// Examine other syslog messages occurring around the
			// same time on the same node, to see if the source of
			// the problem can be identified.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("Cannot access file <%s>, err = <%s>"),
			    fname, (s ? s : "(unknown error number)"));
			*ti = 0;
			res.err_code = SCHA_ERR_TIMESTAMP;
			return (res);
		}
	}
}


//
// remove_pingpong_history()
// This function removes the pingpong history files of a deleted resource
// to avoid a newly created resource with the same name inheriting
// the old pingpong history.
//
// The pingpong timestamp filename is the resourcename with optional trailing
// '#'.  If the trailing '#' is not present, it is a scha_control GIVEOVER
// timestamp.  If the trailing '#' is present, it is a HARD stop-failed
// reboot timestamp.  Note that a resource name cannot contain the '#'
// character.
//
// The timestamp files are stored in $PP_PATH for global zone,
// or in ${PP_PATH}/zonename for a non-global zone.
//
// This function is called once per existing zone.
//
void
remove_pingpong_history(const char *r_name, rgm::lni_t lni)
{
	// "${PP_PATH}/zonename/resourcename#"
	char fname[sizeof (PP_PATH) + ZONENAME_MAX + MAXRGMOBJNAMELEN + 3];

	compute_timestamp_fname(r_name, lni, fname, NULL);
	(void) unlink(fname);
	(void) strcat(fname, "#");
	(void) unlink(fname);
}


//
// remove_zone_pingpong_dir()
// This function removes the pingpong history directory on the local node
// for a given non-global zone which has been deconfigured.
// The directory pathname is "${PP_PATH}/zonename".
//
void
remove_zone_pingpong_dir(rgm::lni_t lni)
{
	DIR *dirp;
	char dent[sizeof (struct dirent) + 1 + MAXNAMELEN];
	struct dirent *dentp = (struct dirent *)dent;
	struct dirent *resultp;
	char filename[MAXPATHLEN + 1];

	// enough room for "${PP_PATH}/zonename"
	char dirname[sizeof (PP_PATH) + ZONENAME_MAX + 2];

	compute_timestamp_fname(NULL, lni, NULL, dirname);

	// Make best effort to remove all files in the directory
	if ((dirp = opendir(dirname)) != NULL) {
		while ((readdir_r(dirp, dentp, &resultp) == 0) &&
		    resultp != NULL) {
			if (strcmp(dentp->d_name, ".") == 0 ||
			    strcmp(dentp->d_name, "..") == 0)
				continue;
			(void) sprintf(filename, "%s/%s",
			    dirname, dentp->d_name);
			(void) remove(filename);
		}
		(void) closedir(dirp);
	}
	// Make best effort to remove the directory
	(void) remove(dirname);
}


//
// Compute the timestamp file pathname from the r name and lni.
// See comments for remove_pingpong_history() above.
//
// If pfilename is non-NULL, write the timestamp file pathname into the memory
// area pointed to by pfilename.
//
// If pdirname is non-NULL, write the dirname portion of
// the timestamp file pathname into the memory area pointed to by
// pdirname, i.e., the leading substring but without the "/r_name" at the end.
//
// The caller must guarantee that lni is a valid LNI, and that pfilename and/or
// pdirname point to sufficiently large extents of memory to hold the
// longest possible pathname.  Currently, these are:
//	char pfilename[sizeof (PP_PATH) + ZONENAME_MAX + MAXRGMOBJNAMELEN + 3];
//	char pdirname[sizeof (PP_PATH) + ZONENAME_MAX + 1];
//
// r_name is a resource name, optionally followed by a single suffix character.
// '#' is used as the suffix character by launch_method() for a stop_failed
// reboot timestamp file.
// If pfilename is NULL, then the value of r_name is ignored.
//
// Caller must hold the state lock since this function calls findObject().
//
static void
compute_timestamp_fname(const char *r_name, rgm::lni_t lni, char *pfilename,
    char *pdirname)
{
	// "${PP_PATH}/zonename/resourcename#"
	char fname[sizeof (PP_PATH) + ZONENAME_MAX + MAXRGMOBJNAMELEN + 3];
	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	const char *zonenm = lnNode->getZonename();

	(void) strcpy(fname, PP_PATH);
	if (zonenm != NULL) {
		(void) strcat(fname, "/");
		(void) strlcat(fname, zonenm, ZONENAME_MAX);
	}
	if (pdirname != NULL) {
		(void) strcpy(pdirname, fname);
	}
	if (pfilename != NULL) {
		(void) strcpy(pfilename, fname);
		(void) strcat(pfilename, "/");
		(void) strlcat(pfilename, r_name, MAXRGMOBJNAMELEN + 1);
	}
}


//
// pingpong_check()
// This function performs checking for R to make sure the
// same RG (which the R is residing) is not being scha_control
// back to the node which scha_control has failed recently.
//
// The time stamp for each R on the node is the time which
// the scha_control is invoked against R on that node.
//
// The time_stamp for each R is set up as a touch file in dir
// 	/var/cluster/rgm/pingpong_log/
// with filename as R name and time of file as time_stamp.
//
// On candidate nodes, the testing is done effectively
// in following way:
//	Assume the RG which contain R try to failover from node
//	A to B. We can set up following standard.
//	if (current_time > R(B).time_stamp + Pingpong_interval) then
//		The node B has not invoked scha_control
//		against R for a long enough time.
//		Pingpong check passed.
//	else
//		node B has invoked scha_control() against R
//		recently, we can not failover R to node B.
//		Pingpong check failed.
//
// Due to R name space is unique, there is no name duplication.
//
//
scha_errmsg_t
pingpong_check(const char *r_name, int ppinterval, rgm::lni_t lni)
{
	time_t cand_ti, curr_ti;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (get_timestamp(r_name, &cand_ti, lni).err_code != SCHA_ERR_NOERR) {
		ucmm_print("pingpong_check", NOGET(
		    "Failed to get timestamp for R"));
		res.err_code = SCHA_ERR_CHECKS;
		return (res);
	}
	curr_ti = time(NULL);
	if (curr_ti <= cand_ti + ppinterval) {
		// The R has invoked scha_control recently on this node.
		// This node is not the right candidate to fail RG over.
		ucmm_print("pingpong_check", NOGET(
		    "timestamp check failed"));
		res.err_code = SCHA_ERR_CHECKS;
		return (res);
	} else {
		// The R hasn't invoke scha_control on this node for a long
		// enough time. Pass pingpong test.
		ucmm_print("pingpong_check", NOGET(
		    "pingpong_check passed"));
		res.err_code = SCHA_ERR_NOERR;
		return (res);
	}
}


//
// Sanity_check()
// This function performs sanity_check which will include
// network checking and system check.
//
// If the sanity check may take a long time, we don't want
// to keep other modules waiting for the state_lock we
// hold. The lock should be released and reclaimed back later after
// the check.  See monitor_check() below for an example.
// The R/RG static data (CCR and in-memory) cannot change out from under
// us while the lock is released.  See the header comment of
// idl_scha_control_giveover() for an explanation.
//
scha_errmsg_t
sanity_check(const char *r_name, rgm::lni_t lni)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	ucmm_print("check", NOGET(
	    "Do sanity check on lni <%d>"), lni);
	r_name = r_name;
	lni = lni;
#ifdef FUTURE
	XXXX bug 4184127 hactl/scha_control sanity checks should be rigorous.
	system_check();
#endif
	res.err_code = SCHA_ERR_NOERR;
	return (res);
}


//
// Monitor_check()
// This function run the monitor_check method for those
// Rs with monitor_switch set on.
//
// As with sanity check, we are holding the rgm state lock when we
// enter monitor_check().  It is released during the method execution
// and reclaimed before returning.
// The R/RG static data (CCR and in-memory) cannot change out from under
// us while the lock is released.  See the header comment of
// idl_scha_control_giveover() for an explanation.
//
scha_errmsg_t
monitor_check(rlist_p_t rp, rgm::lni_t lni)
{
	boolean_t		valid;
	boolean_t		is_scalable;
	rlist_p_t		r_ptr;
	char			*caller_r_name = NULL;
	char			*r_name = NULL;
	char			*rg_name = NULL;
	rglist_p_t		rgp;
	char			fed_opts_tag[MAXFEDTAGLEN +1];
							// rg_name.r_name.meth
	hrtime_t		method_start_time, method_end_time;
	hrtime_t		method_run_time = 0;
	int			timeoutpc;
	char			*method_name = strdup(SCHA_MONITOR_CHECK);
	char			*fed_opts_cmd_path = NULL;
	char			*real_method_name = NULL;
	char			*fed_opts_argv[18];
	char 			fed_argv0[MAXPATHLEN + 32]; // for argv[0]
	name_t			rt_meth_name = NULL;
	fe_cmd			fed_cmd = {0};
	fe_run_result		fed_result = {FE_OKAY, 0, 0, SEC_OK, NULL};
	char			**fed_opts_env;
	int			rc = 0;
	uint_t			mrc = 0;
	char			*err_str;
	uint_t			i;
	sc_syslog_msg_handle_t	handle;
	cl_event_result_t	cl_result_code = CL_RC_OK;
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};
	LogicalNode		*lnNode = lnManager->findObject(lni);
	char			*nodename, *zonename = NULL;

	CL_PANIC(lnNode != NULL);
	if (lnNode->getZonename() != NULL) {
		zonename = strdup(lnNode->getZonename());
	} else if ((ZONE != NULL) && (strcmp(ZONE, GLOBAL_ZONENAME) != 0)) {
		zonename = strdup(ZONE);
	}
	nodename = strdup(lnNode->getFullName());
	CL_PANIC(rp != NULL);
	rgp = rp->rl_rg;
	CL_PANIC(rgp != NULL);
	caller_r_name = strdup_nocheck(rp->rl_ccrdata->r_name);
	rg_name = strdup_nocheck(rgp->rgl_ccr->rg_name);
	if (caller_r_name == NULL || rg_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	// Run all monitor check on this node. For Rs of
	// the same RT, the monitor check could run only once.
	for (r_ptr = rgp->rgl_resources; r_ptr != NULL;
	    r_ptr = r_ptr->rl_next) {

		r_name = strdup_nocheck(r_ptr->rl_ccrdata->r_name);
		if (r_name == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}
		valid = B_TRUE;
		// Skip R that is disabled or whose monitor is disabled.
		if (!is_r_monitor_checked(r_ptr, lni)) {
			ucmm_print("monitor_check", NOGET(
			    "R <%s> is disabled or mon disabled"), r_name);
			continue;
		}

		// No need to test for deleted R, since a pending-delete R
		// is always disabled (and we already checked for that).

		//
		// Skip R on which monitor_check method is not registered.
		// A scalable R will call ssm_wrapper whether or not
		// monitor_check is registered.
		//
		is_scalable = is_r_scalable(r_ptr->rl_ccrdata);
		if (!is_method_reg(r_ptr->rl_ccrtype, METH_MONITOR_CHECK,
		    &rt_meth_name) && !is_scalable) {
			ucmm_print("monitor_check", NOGET(
			    "monitor_check: no monitor check method is "
			    "registered for non-scalable resource <%s>"),
			    r_name);
			continue;
		}

		//
		// If rt_meth_name is non-null, it hold the registered
		// method name.  If there is no method registered by the
		// scalable service, rt_meth_name is NULL.
		//
		// If resource is scalable, invoke the ssm wrapper in lieu of
		// the real method.  Set up fed_argv0 string as a synthetic
		// "program name" which will be assigned to argv[0] when the
		// fed executes the method (or the ssm_wrapper).  The value of
		// argv[0] is shown by ps(1), etc.  Append the string
		// "(scalable svc)" to indicate that the scalable service
		// wrapper program is being invoked.  Otherwise, fed_argv0 is
		// just the method program name, as usual.
		//
		if (is_scalable) {
			if (rt_meth_name != NULL &&
			    call_real_monchk(rgp, caller_r_name, lni)) {
				(void) strcpy(fed_argv0, rt_meth_name);
			} else {
				(void) strcpy(fed_argv0, SCHA_MONITOR_CHECK);
			}

			(void) strcat(fed_argv0, " (scalable svc)");
		} else {
			(void) strcpy(fed_argv0, rt_meth_name);
		}

		fed_cmd.host = getlocalhostname();
		if (fed_cmd.host == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// While attempting to process a scha_control(1HA,3HA)
			// call, the rgmd failed in an attempt to obtain the
			// hostname of the local node. This is considered a
			// MONITOR_CHECK method failure. This in turn will
			// prevent the attempted failover of the resource
			// group from its current master to a new master.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("monitor_check: getlocalhostname() failed "
			    "for resource <%s>, resource group <%s>, "
			    "node <%s>"),
			    r_name, rg_name, nodename);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_INTERNAL;
			goto finished; //lint !e801
		}

		//
		// If there is no MONITOR_CHECK method (which can occur only
		// for a scalable resource) then set timeout to default ssm
		// timeout
		//
		if (rt_meth_name == NULL) {
			CL_PANIC(is_scalable);
			fed_cmd.timeout = SSM_WRAPPER_TIMEOUT;
		} else if ((res = rt_method_timeout((const rlist_p_t)r_ptr,
		    rt_meth_name, METH_MONITOR_CHECK,
		    &fed_cmd.timeout)).err_code != SCHA_ERR_NOERR) {
			ucmm_print("scha_control", NOGET(
			    "failed to get method <%s> timeout"),
			    rt_meth_name);
			goto finished; //lint !e801
		}

		fed_cmd.security = SEC_UNIX_STRONG;
		fed_cmd.flags = 0;

		fed_cmd.zonename = NULL;
#if SOL_VERSION >= __s10
		//
		// some methods run in global zone since they need more
		// privileges
		//
		if (!is_r_globalzone(r_ptr) && !is_scalable) {
			fed_cmd.zonename = zonename;
		}
#endif

		if (zonename == NULL) { // hosted on global zone
			(void) sprintf(fed_opts_tag, "%s.%s.%d", rg_name,
			    r_name, METH_MONITOR_CHECK);
		} else {
			(void) sprintf(fed_opts_tag, "%s.%s.%s.%d", zonename,
			    rg_name, r_name, METH_MONITOR_CHECK);
		}
		set_fed_core_props(fed_cmd, fed_opts_tag, zonename,
		    is_r_globalzone(r_ptr));


		// assemble name of full path
		fed_opts_cmd_path = fe_method_full_name(
		    r_ptr->rl_ccrtype->rt_basedir, rt_meth_name);
		if (fed_opts_cmd_path == NULL && !is_scalable) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// During execution of a scha_control(1HA,3HA)
			// function, the rgmd daemon was unable to assemble
			// the full method pathname for the MONITOR_CHECK
			// method. This is considered a MONITOR_CHECK method
			// failure. This in turn will prevent the attempted
			// failover of the resource group from its current
			// master to a new master.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("monitor_check: fe_method_full_name() "
			    "failed for resource <%s>, resource group <%s>, "
			    "node <%s>"),
			    r_name, rg_name, nodename);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_METHODNAME;
			goto finished; //lint !e801
		}

		fed_cmd.project_name = get_project_name(r_ptr->rl_ccrdata,
		    rgp->rgl_ccr);

		// Keep copy of string before release the lock.
		i = 0;
		fed_opts_argv[i++] = fed_argv0;
		fed_opts_argv[i++] = "-R";
		fed_opts_argv[i++] = r_name;
		fed_opts_argv[i++] = "-T";
		fed_opts_argv[i++] = r_ptr->rl_ccrdata->r_type;
		fed_opts_argv[i++] = "-G";
		fed_opts_argv[i++] = rg_name;
#if SOL_VERSION >= __s10
		//
		// add only if hosted in local zone, while method runs in
		// global zone
		//
		if (zonename && is_r_globalzone(r_ptr)) {
			fed_opts_argv[i++] = "-Z";
			fed_opts_argv[i++] = zonename;
		}
#endif

		//
		// If is_scalable is true, then the resource is scalable;
		// we will invoke the ssm_wrapper, passing it the real
		// MONITOR_CHECK method pathname (if any) as a command
		// line argument.  However, if the RG is ONLINE on this node,
		// do not call the real MONITOR_CHECK method, but only call
		// the ssm_wrapper.
		//
		// Append additional args for ssm_wrapper.
		//
		if (is_scalable) {
			fed_opts_argv[i++] = "-m";
			fed_opts_argv[i++] = SCHA_MONITOR_CHECK;
			real_method_name = fed_opts_cmd_path;
			fed_opts_cmd_path = strdup(SSM_WRAPPER_PATH);
			if (real_method_name != NULL &&
			    call_real_monchk(rgp, caller_r_name, lni)) {
				fed_opts_argv[i++] = "-p";
				fed_opts_argv[i++] = real_method_name;
			}
			if (zonename && !is_r_globalzone(r_ptr)) {
				fed_opts_argv[i++] = "-z";
				fed_opts_argv[i++] = zonename;

#ifdef SC_SRM
				// Set the projectname.
				fed_opts_argv[i++] = "-P";
				fed_opts_argv[i++] = fed_cmd.project_name;
				// SC SLM addon start
				fed_cmd.ssm_wrapper_zone = zonename;
				// SC SLM addon end
#endif
			}

		}

		fed_opts_argv[i] = NULL;

		//
		// set environment for method execution
		//
		if ((fed_opts_env = fe_set_env_vars()) == NULL) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// During execution of a scha_control(1HA,3HA)
			// function, the rgmd daemon was unable to set up
			// environment variables for method execution, causing
			// a MONITOR_CHECK method invocation to fail. This in
			// turn will prevent the attempted failover of the
			// resource group from its current master to a new
			// master.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("monitor_check: set_env_vars() failed for "
			    "resource <%s>, resource group <%s>, node <%s>"),
			    r_name, rg_name, nodename);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_CHECKS;
			goto finished; //lint !e801
		}

// SC SLM addon start
#ifdef SC_SRM
		fed_cmd.r_name = r_name;
		fed_cmd.rg_name = rg_name;
		fed_cmd.slm_flags = METH_MONITOR_CHECK;
#if SOL_VERSION >= __s10
		// some methods run in global zone since they
		// need more privileges
		// Bypass fed SLM handling for such methods
		if (rp->rl_ccrtype != NULL && is_r_globalzone(rp) &&
		    zonename != NULL) {
			fed_cmd.slm_flags = METH_FEDSLM_BYPASS;
		}
#endif
		if ((rgp != NULL) && (rgp->rgl_ccr != NULL)) {
			fed_cmd.rg_slm_type = rgp->rgl_ccr->rg_slm_type;
			fed_cmd.rg_slm_pset_type =
			    rgp->rgl_ccr->rg_slm_pset_type;
			fed_cmd.rg_slm_cpu = rgp->rgl_ccr->rg_slm_cpu;
			fed_cmd.rg_slm_cpu_min = rgp->rgl_ccr->rg_slm_cpu_min;
		}
#endif
// SC SLM addon end

		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
		    SYSTEXT("launching method <%s> for resource <%s>, "
		    "resource group <%s>, node <%s>, timeout <%d> seconds"),
		    fed_argv0, r_name, rg_name, nodename,
		    fed_cmd.timeout);
		sc_syslog_msg_done(&handle);

		// Release lock on state structure while we run method.
		rgm_unlock_state();

		method_start_time = os::gethrtime();

		rc = fe_run(fed_opts_tag, &fed_cmd, fed_opts_cmd_path,
		    fed_opts_argv, fed_opts_env, &fed_result);

		method_end_time = os::gethrtime();
		method_run_time = method_end_time - method_start_time;

		if (fed_cmd.timeout != 0) {
			timeoutpc = (int)(((float)method_run_time / 10000000) /
				fed_cmd.timeout);
		} else {
			timeoutpc = 0;
		}

		if (rc != 0) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// A scha_control(1HA,3HA) GIVEOVER attempt failed,
			// due to a failure of the rgmd to communicate with
			// the rpc.fed daemon. If the rpc.fed process died,
			// this might lead to a subsequent reboot of the node.
			// Otherwise, this will prevent a resource group on
			// the local node from failing over to an alternate
			// primary node
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified and if it recurs. Save a copy of the
			// /var/adm/messages files on all nodes and contact
			// your authorized Sun service provider for
			// assistance.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "monitor_check: call to rpc.fed failed for "
			    "resource <%s>, resource group <%s>, node <%s>, "
			    "method <%s>"),
			    r_name, rg_name, nodename, fed_opts_cmd_path);
			sc_syslog_msg_done(&handle);
			valid = B_FALSE;
		}

		// Ok to reclaim back the lock after method finishes.
		rgm_lock_state();

		//
		// We've released and reclaimed the lock, but the R and RG
		// cannot have been edited out from under us.  See the
		// header comment of idl_scha_control_giveover() for an
		// explanation.
		//
		// Just to be paranoid, we'll make sure that the pointers
		// r_ptr, rp and rgp, which point directly into in-memory state,
		// are still valid.  Note that we are testing for pointer
		// equality-- the in-memory locations should not have changed.
		//
		if (rname_to_r(NULL, caller_r_name) != rp || rgp != rp->rl_rg ||
		    rname_to_r(NULL, r_name) != r_ptr) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// An internal error has occurred in the locking logic
			// of the rgmd, such that a resource group was
			// erroneously allowed to be edited while a failover
			// was pending on it, causing the scha_control call to
			// return early with an error. This in turn will
			// prevent the attempted failover of the resource
			// group from its current master to a new master. This
			// should not occur and may indicate an internal logic
			// error in the rgmd.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing and
			// correcting the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("monitor_check: resource "
			    "group <%s> changed while running MONITOR_CHECK "
			    "methods"), r_name, rg_name);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_INTERNAL;
			goto finished; //lint !e801
		}

		if (am_i_president() &&
		    (res.err_code = map_busted_err(rgp->rgl_switching)) !=
		    SCHA_ERR_NOERR) {
			// If the RG is already busted on the
			// node (it should be the president),
			// then don't do any thing but early return.
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// A scha_control(1HA,3HA) GIVEOVER attempt failed,
			// due to the error listed.
			// @user_action
			// Examine other syslog messages on all cluster
			// members that occurred about the same time as this
			// message, to see if the problem that caused the
			// error can be identified and repaired.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("monitor_check: the failover"
			    " requested by scha_control for"
			    " resource <%s>, resource group <%s>, node <%s>"
			    " was not completed because of error: <%s>"),
			    r_name, rg_name, nodename,
			    rgm_error_msg(res.err_code));
			sc_syslog_msg_done(&handle);

			cl_result_code = CL_RC_FAILED;
			goto finished; //lint !e801
		}

		if (rc != 0) {
			cl_result_code = CL_RC_FAILED;
			res.err_code = SCHA_ERR_CHECKS;
			goto finished; //lint !e801
		}

		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		switch (fed_result.type) {
		case FE_OKAY:
			//
			// method_retcode now is the unprocessed return
			// from wait, which needs to be processed by client;
			//

			mrc = fed_result.method_retcode;
			if (!WIFEXITED(mrc)) {
				if (WIFSIGNALED(mrc)) {
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE, SYSTEXT(
					    "Method <%s> on resource "
					    "<%s>, node <%s> terminated due to "
					    "receipt of signal <%d>"),
					    fed_argv0, r_name, nodename,
					    WTERMSIG(mrc));
				} else if (WIFSTOPPED(mrc)) {
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,  SYSTEXT(
					    "Method <%s> on resource <%s>, "
					    "node <%s> stopped due to receipt "
					    "of signal <%d>"),
					    fed_argv0, r_name, nodename,
					    WSTOPSIG(mrc));
				} else {
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE, SYSTEXT(
					    "Method <%s> on resource <%s>, "
					    "node <%s> terminated abnormally"),
					    fed_argv0, r_name, nodename);
				}
				cl_result_code = CL_RC_FAILED;
				valid = B_FALSE;
				break;
			}

			// fe_run succeeds.
			// Check return value of monitor_checking method.
			// 	0:	success
			// 	other: 	failed
			mrc = WEXITSTATUS(fed_result.method_retcode);
			switch (mrc) {
			case EMETH_SUCCESS:
				// all went well
				(void) sc_syslog_msg_log(handle,
				    LOG_NOTICE, MESSAGE, SYSTEXT(
				    "method <%s> completed "
				    "successfully for resource <%s>, "
				    "resource group <%s>, node <%s>, time used:"
				    " %d%% of timeout <%d seconds>"), fed_argv0,
				    r_name, rg_name, nodename, timeoutpc,
				    fed_cmd.timeout);
				cl_result_code = CL_RC_OK;
				break;

			// Currently, all nonzero exit codes are equivalent.
			case EMETH_FAILURE:
			default:
				//
				// SCMSGS
				// @explanation
				// In scha_control, monitor_check method of
				// the resource failed on specific node.
				// @user_action
				// No action is required, this is normal
				// phenomenon of scha_control, which launches
				// the corresponding monitor_check method of
				// the resource on all candidate nodes and
				// looks for a healthy node which passes the
				// test. If a healthy node is found,
				// scha_control will let the node take over
				// the resource group. Otherwise, scha_control
				// will just exit early.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT(
				    "monitor_check: method <%s> failed "
				    "on resource <%s> in "
				    "resource group <%s> on node <%s>, "
				    "exit code <%d>, time used: %d%% of "
				    "timeout <%d seconds>"),
				    fed_argv0, r_name, rg_name,
				    nodename, mrc, timeoutpc,
				    fed_cmd.timeout);
				cl_result_code = CL_RC_FAILED;
				valid = B_FALSE;
				break;
			}
			break;
// SC SLM addon start
		case FE_SLM_ERROR:
			//
			// SCMSGS
			// @explanation
			// Resource group is under SC SLM control and an SLM
			// error occurred. Some errors might be configuration
			// errors. Check fed SLM errors for more details.
			// @user_action
			// Move RG_SLM_type to manual and restart the resource
			// group.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE,
			    SYSTEXT("Method <%s> failed to execute "
			    "on resource <%s> in resource group <%s>, "
			    "SLM error: <%d>"),
			    fed_argv0, r_name, rg_name,
			    fed_result.sys_errno);
			cl_result_code = CL_RC_FAILED;
			valid = B_FALSE;
			break;
// SC SLM addon end

		case FE_SYSERRNO:
			if (strerror(fed_result.sys_errno)) {
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT(
				    "Method <%s> failed to execute on "
				    "resource <%s> in resource group <%s>, "
				    "on node <%s>, error: <%s>"),
				    fed_argv0, r_name, rg_name, nodename,
				    strerror(fed_result.sys_errno));
			} else {
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    SYSTEXT("Method <%s> failed to execute "
				    "on resource <%s> in resource group <%s>, "
				    "on node <%s>, error: <%d>"),
				    fed_argv0, r_name, rg_name,
				    nodename, fed_result.sys_errno);
			}
			cl_result_code = CL_RC_FAILED;
			valid = B_FALSE;
			break;

		case FE_DUP:
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Error: duplicate method <%s> launched on resource"
			    " <%s> in resource group <%s> on node <%s>"),
			    fed_argv0, r_name, rg_name, nodename);
			cl_result_code = CL_RC_FAILED;
			valid = B_FALSE;
			break;

		case FE_UNKNC:
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s>, node <%s>: unknown "
			    "command."), fed_argv0, r_name, nodename);
			cl_result_code = CL_RC_FAILED;
			valid = B_FALSE;
			break;

		case FE_ACCESS:
			err_str = security_get_errormsg(fed_result.sec_errno);
			if (err_str) {
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    SYSTEXT("Method <%s> on resource <%s>, node"
				    " <%s>: authorization error: %s."),
				    fed_argv0, r_name, nodename, err_str);
				free(err_str);
			} else {
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    SYSTEXT("Method <%s> on resource <%s>, node"
				    " <%s>: authorization error: %d."),
				    fed_argv0, r_name, nodename,
				    fed_result.sec_errno);
			}
			cl_result_code = CL_RC_FAILED;
			valid = B_FALSE;
			break;

		case FE_CONNECT:
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s>, node <%s>: RPC "
			    "connection error."),
			    fed_argv0, r_name, nodename);
			if (fed_result.sys_errno == RPC_PROGNOTREGISTERED)
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    SYSTEXT("rpc.fed: program not registered "
				    "or server not started"));
			cl_result_code = CL_RC_FAILED;
			valid = B_FALSE;
			break;

		case FE_TIMEDOUT:
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s>, resource group <%s>,"
			    " node <%s>: Timeout."),
			    fed_argv0, r_name, rg_name,
			    nodename);
			cl_result_code = CL_RC_TIMEOUT;
			valid = B_FALSE;
			break;

		case FE_QUIESCED:
			//
			// SCMSGS
			// @explanation
			// The system administrator has requested a fast quiesce
			// of the reosurce group. The specified callback method
			// for the given resource was killed to speed its
			// completion so that the resource group can be
			// quiesced.
			// @user_action
			// This is just an informational message generated by a
			// quiesce operation initiated by the user.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s>, node <%s> killed to "
			    "achieve user-initiated fast quiesce of the "
			    "resource group <%s>."), fed_argv0, r_name,
			    nodename, rg_name);
			// needs to be changed to quiesce
			cl_result_code = CL_RC_TIMEOUT;
			valid = B_FALSE;
			break;

		case FE_UNKILLABLE:
			/*
			 * This is a serious case.  We must reboot the
			 * node immediately.
			 */
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, SYSTEXT(
			    "fatal: Aborting this node because "
			    "method <%s> on resource <%s> for node <%s>"
			    " is unkillable"),
			    fed_argv0, r_name, nodename);
			sc_syslog_msg_done(&handle);

			// publish a event
#if SOL_VERSION >= __s10
			(void) sc_publish_zc_event(ZONE,
			    ESC_CLUSTER_RG_NODE_REBOOTED,
			    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_R_NAME, SE_DATA_TYPE_STRING,
			    r_name, CL_RG_NAME, SE_DATA_TYPE_STRING,
			    rg_name, CL_NODE_NAME, SE_DATA_TYPE_STRING,
			    fed_cmd.host, NULL);
#else
			(void) sc_publish_event(
			    ESC_CLUSTER_RG_NODE_REBOOTED,
			    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_R_NAME, SE_DATA_TYPE_STRING,
			    r_name, CL_RG_NAME, SE_DATA_TYPE_STRING,
			    rg_name, CL_NODE_NAME, SE_DATA_TYPE_STRING,
			    fed_cmd.host, NULL);
#endif

			//
			// reboot the physical node -- see block comment
			// under FE_UNKILLABLE case in launch_method().
			//
			abort_node(B_TRUE, System_dumpflag, NULL);

			// NOTREACHABLE
			break;

		case FE_NOTAG:
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s> node <%s>: Execution "
			    "failed: no such method tag."),
			    fed_argv0, r_name, nodename);
			cl_result_code = CL_RC_FAILED;
			valid = B_FALSE;
			break;

		case FE_PROJECT_NAME_INVALID:
			//
			// We should never get this error because we do
			// not request fed to validate the project name.
			//

			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("Invalid project name: %s"),
			    fed_cmd.project_name);
			cl_result_code = CL_RC_FAILED;
			valid = B_FALSE;
			break;

		default:
			// log message and run abort transition
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("fatal: Method <%s> on resource <%s>, "
			    "node <%s>: Received unexpected"
			    " result <%d> from rpc.fed, aborting node"),
			    fed_argv0, r_name, nodename,
			    fed_result.type);
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHABLE
		}
		sc_syslog_msg_done(&handle);

		// publish an event for the Monitor_check method launch
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE, ESC_CLUSTER_R_METHOD_COMPLETED,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_R_NAME, SE_DATA_TYPE_STRING, r_name,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
		    CL_METHOD_NAME, SE_DATA_TYPE_STRING, method_name,
		    CL_METHOD_DURATION, SE_DATA_TYPE_TIME, method_run_time,
		    CL_METHOD_TIMEOUT, SE_DATA_TYPE_UINT32, fed_cmd.timeout,
		    CL_METHOD_PATH, SE_DATA_TYPE_STRING, fed_opts_cmd_path,
		    CL_RESULT_CODE, DATA_TYPE_UINT32, cl_result_code, NULL);
#else
		(void) sc_publish_event(ESC_CLUSTER_R_METHOD_COMPLETED,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_R_NAME, SE_DATA_TYPE_STRING, r_name,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
		    CL_METHOD_NAME, SE_DATA_TYPE_STRING, method_name,
		    CL_METHOD_DURATION, SE_DATA_TYPE_TIME, method_run_time,
		    CL_METHOD_TIMEOUT, SE_DATA_TYPE_UINT32, fed_cmd.timeout,
		    CL_METHOD_PATH, SE_DATA_TYPE_STRING, fed_opts_cmd_path,
		    CL_RESULT_CODE, DATA_TYPE_UINT32, cl_result_code, NULL);
#endif

		// No need to syslog here that the method failed,
		// a detailed message for each failure case
		// was sysloged in every case above.
		if (!valid) {
			res.err_code = SCHA_ERR_CHECKS;
			goto finished; //lint !e801
		}

		free(r_name);
		free(fed_cmd.corepath);
		free(fed_cmd.project_name);
		free(rt_meth_name);
		free(fed_opts_cmd_path);
		free(real_method_name);

		r_name = NULL;
		fed_cmd.corepath = NULL;
		fed_cmd.project_name = NULL;
		rt_meth_name = NULL;
		fed_opts_cmd_path = NULL;
		real_method_name = NULL;
	}
	res.err_code = SCHA_ERR_NOERR;

finished:
	/* free the xdr resources */
	xdr_free((xdrproc_t)xdr_fe_run_result, (char *)&fed_result);

	free(r_name);
	free(fed_cmd.corepath);
	free(fed_cmd.project_name);
	free(rt_meth_name);
	free(fed_opts_cmd_path);
	free(real_method_name);

	free(method_name);
	free(caller_r_name);
	free(rg_name);
	free(nodename);
	free(zonename);
	return (res);
}


//
// touch_file()
// This function is used to create timestamp file for resource R on local
// node for the given zone. Return true if succeed and false if failed.
//
// r_name is a resource name, optionally followed by a single suffix character.
// '#' is used as the suffix character by launch_method() for a stop_failed
// reboot timestamp file.
// For additional details on the pingpong filename, see header comments
// for function remove_pingpong_history().
//
// the function assumes that callee holds the state lock mutex
//
boolean_t
touch_file(const char *r_name, rgm::lni_t lni)
{
	char zone_pp_path[sizeof (PP_PATH) + ZONENAME_MAX + 1];
	char fname[sizeof (PP_PATH) + ZONENAME_MAX + MAXRGMOBJNAMELEN
	    + 3];	// "${PP_PATH}/zonename/resourcename#"
	struct stat	statbuf;
	int		fd;
	sc_syslog_msg_handle_t handle;

	const char *nodename = (lnManager->findObject(lni))->getFullName();

	compute_timestamp_fname(r_name, lni, fname, zone_pp_path);

	// Check whether pingpong file exists already
	if (stat(fname, &statbuf) == 0) {
		// File has already been created.  Update mtime.
		if (utime(fname, NULL) < 0) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd failed in a call to utime(2) on the local
			// node. This may prevent the anti-"pingpong" feature
			// from working, which may permit a resource group to
			// fail over repeatedly between two or more nodes. The
			// failure of the utime call might indicate a more
			// serious problem on the node.
			// @user_action
			// Examine other syslog messages occurring around the
			// same time on the same node, to see if the source of
			// the problem can be identified.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, SYSTEXT(
			    "Error: Unable to update scha_control "
			    "timestamp file <%s> for resource <%s> on "
			    "node <%s>"), fname, r_name, nodename);
			sc_syslog_msg_done(&handle);
			return (B_FALSE);
		}
		return (B_TRUE);
	}

	// If the zone PP directory doesn't exist, try to create it
	if (stat(zone_pp_path, &statbuf) != 0) {
		if (os::mkdirp(zone_pp_path, 0755) < 0 && errno != EEXIST) {
			// Failed to create directory
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd is unable to access the directory used for
			// the anti-"pingpong" feature, and cannot create the
			// directory (which should already exist). This may
			// prevent the anti-pingpong feature from working,
			// which may permit a resource group to fail over
			// repeatedly between two or more nodes. The failure
			// to access or create the directory might indicate a
			// more serious problem on the node.
			// @user_action
			// Examine other syslog messages occurring around the
			// same time on the same node, to see if the source of
			// the problem can be identified.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Error: Unable to create directory <%s> for "
			    "scha_control timestamp file"), zone_pp_path);
			sc_syslog_msg_done(&handle);
			return (B_FALSE);
		}
	}

	// Try to create the file
	if ((fd = creat(fname, 0644)) < 0) {
		// Failed to create file
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd has failed in an attempt to create a file used for
		// the anti-"pingpong" feature. This may prevent the
		// anti-pingpong feature from working, which may permit a
		// resource group to fail over repeatedly between two or more
		// nodes. The failure to create the file might indicate a more
		// serious problem on the node.
		// @user_action
		// Examine other syslog messages occurring around the same
		// time on the same node, to see if the source of the problem
		// can be identified.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "Error: Unable to create scha_control timestamp file <%s> "
		    "for resource <%s> node <%s>"), fname, r_name, nodename);
		sc_syslog_msg_done(&handle);
		return (B_TRUE);
	}
	(void) close(fd);
	return (B_TRUE);
}


// map_busted_err
// Return a scha_errmsg_t exit code corresponding to the flavor of "busted"
// of the argument:
//  SCHA_ERR_SWRECONF if arg is SW_BUSTED_RECONF (switch busted by node death)
//	or SW_EVACUATING (switch or update busted by node evacuation).
//  SCHA_ERR_CLRECONF if arg is SW_UPDATE_BUSTED (update busted by node death)
//  SCHA_ERR_STARTFAILED if arg is SW_BUSTED_START (switch busted by start
//	failure)
//  SCHA_ERR_STOPFAILED if arg is SW_BUSTED_STOP (switch busted by stop failure,
//	requires operator intervention)
//  SCHA_ERR_NOERR if arg does not indicate a "busted" state.
scha_err_t
map_busted_err(rg_switch_t arg)
{
	scha_err_t err = SCHA_ERR_NOERR;

	if (arg == SW_BUSTED_RECONF || arg == SW_EVACUATING) {
		err = SCHA_ERR_SWRECONF;
		return (err);
	} else if (arg == SW_UPDATE_BUSTED) {
		err = SCHA_ERR_CLRECONF;
		return (err);
	} else if (arg == SW_BUSTED_START) {
		err = SCHA_ERR_STARTFAILED;
		return (err);
	} else if (arg == SW_BUSTED_STOP) {
		err = SCHA_ERR_STOPFAILED;
		return (err);
	}

	return (err);
}


#ifndef EUROPA_FARM
//
// pres_local_sanity_check
// Performs a validity check on a requested giveover or restart.
// The check is local to the president.
// Currently, the only check performed is that the RG is not currently
// frozen (due to Global_resources_used dependency on a frozen service)
// and has not been frozen within the recent past, where "recent" is
// currently defined to mean within the past (GRU_FREEZE_RECENT) seconds.
//
// If the check vetoes the giveover or restart, we return a non-zero
// scha_errmsg_t value.
//
// Caller holds the rgm state mutex lock.
//
static scha_errmsg_t
pres_local_sanity_check(rglist_p_t rgp)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	// Is RG currently frozen?
	if (rgp->rgl_is_frozen) {
		res.err_code = SCHA_ERR_CHECKS;
		return (res);
	}

	// Determine current clock time
	time_t	curr_time = time(NULL);
	// If time() call failed, abort
	CL_PANIC(curr_time >= (time_t)0);

	if (curr_time - rgp->rgl_last_frozen < GRU_FREEZE_RECENT) {
		res.err_code = SCHA_ERR_CHECKS;
		return (res);
	} else {
		res.err_code = SCHA_ERR_NOERR;
		return (res);
	}
}
#endif


//
// call_real_monchk
//
// Return B_TRUE if the RT's real MONITOR_CHECK method should be called.
// Return B_FALSE if the RT's real MONITOR_CHECK method should not be called.
//
// This function is needed because a scalable service can call scha_control
// "GETOFF" to take the RG offline on a node due to networking problems on
// that node.  As a sanity check, the RGM will call the ssm_wrapper's
// MONITOR_CHECK method to make sure that at least one other node in the
// cluster has healthy network connections.  Because this is a scalable
// service, the RG can be ONLINE where this network check is being done.
// If the RG is ONLINE, we do not want to call the real RT's
// MONITOR_CHECK method because it may execute code that is disruptive to
// an ONLINE instance.  If the RG is OFFLINE on this node, then we do want to
// run the RT's real MONITOR_CHECK method because this node might take over
// the RG.
//
// If the RG is in state RG_OFFLINE, RG_OFF_BOOTED, or RG_OFFLINE_START_FAILED,
// on this node, then this node might be a candidate to take over this RG.
// Therefore, we do want to run the real MONITOR_CHECK method on this node
// to verify that the resource could run on this node if all other sanity
// checks pass.
//
// We explicitly do not want to run the MONITOR_CHECK method for the resource
// if its containing RG is in the state RG_ONLINE (or RG_ON_PENDING_R_RESTART)
// on this node.
//
// If the RG is in RG_ERROR_STOP_FAILED state on any node, the scha_control
// request will exit early with error.  If the RG is in RG_OFF_PENDING_BOOT
// state on a node, that node will not be added to either the master nodeset
// or the candidate nodeset in idl_scha_control_giveover().  Therefore, this
// routine shouldn't be called when the RG state on this node is
// RG_ERROR_STOP_FAILED or RG_OFF_PENDING_BOOT.
//
// The other RG states are non-endish.  There is a call to in_endish_rg_state()
// from idl_scha_control_giveover() that causes the scha_control request to
// exit early with error if the RG is in a non-endish state.  Therefore, this
// routine shouldn't be called when the RG is in a non-endish state.
//
// This function is called during the slave side processing of scha_control,
// though it may also be run on the president node during that processing.
//
// This function is called while holding the rgm state lock.
//
static boolean_t
call_real_monchk(rglist_p_t rgp, char *r_name, sol::nodeid_t mynodeid)
{
	CL_PANIC(rgp != NULL);
	CL_PANIC(r_name != NULL);

	switch (rgp->rgl_xstate[mynodeid].rgx_state) {
	case rgm::RG_OFFLINE:
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_OFFLINE_START_FAILED:
		ucmm_print("call_real_monchk", NOGET(
		    "will call real RT monchk method for R <%s> "
		    "in RG <%s>"), r_name, rgp->rgl_ccr->rg_name);
		return (B_TRUE);

	case rgm::RG_PENDING_ONLINE_BLOCKED:
	case rgm::RG_ONLINE:
	case rgm::RG_ON_PENDING_R_RESTART:
	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_OFF_PENDING_BOOT:
	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_ON_PENDING_METHODS:
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_PENDING_OFF_START_FAILED:
	case rgm::RG_PENDING_ON_STARTED:
	default:
		ucmm_print("call_real_monchk", NOGET(
		    "will not call real RT monchk method for R <%s> "
		    "in RG <%s>"), r_name, rgp->rgl_ccr->rg_name);
		return (B_FALSE);
	}
}


//
// is_resource_in_group
// Checks that the given resource belongs to the given group.
// If so, returns B_TRUE.  If not, syslogs an error message and
// returns B_FALSE.
//
static boolean_t
is_resource_in_group(rlist_p_t rp, rglist_p_t rgp)
{
	sc_syslog_msg_handle_t handle;

	if (rp->rl_rg != rgp) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A resource monitor (or some other program) is attempting to
		// initiate a restart or failover on the indicated resource
		// and group by calling scha_control(1ha),(3ha). However, the
		// indicated resource group does not contain the indicated
		// resource, so the request is rejected. This represents a bug
		// in the calling program.
		// @user_action
		// The resource group may be restarted manually on the same
		// node or switched to another node by using clresourcegroup
		// or the equivalent GUI command. Contact the author of the
		// data service (or of whatever program is attempting to call
		// scha_control) and report the error.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "scha_control: request failed because the given resource "
		    "group <%s> does not contain the given resource <%s>"),
		    rgp->rgl_ccr->rg_name, rp->rl_ccrdata->r_name);
		sc_syslog_msg_done(&handle);
		return (B_FALSE);
	} else {
		return (B_TRUE);
	}
}

#ifndef EUROPA_FARM
//
// r_is_restarting
// Returns B_TRUE if the resource is still in the process of restarting on
// the zone 'slave'; otherwise returns B_FALSE.  Passes an error code back
// through the out parameter 'retcode' if it is non-NULL.
//
// This function must be called on the president, while holding the
// RGM state lock.
//
static boolean_t
r_is_restarting(rlist_p_t rp, rgm::lni_t slave, scha_err_t *retcode)
{
	rgm::rgm_r_state rx_state = rp->rl_xstate[slave].rx_state;
	scha_err_t	errcode = SCHA_ERR_NOERR;
	boolean_t	retval = B_FALSE;

	// If the slave has died, R is no longer restarting
	if (!in_current_logical_membership(slave)) {
		errcode = SCHA_ERR_CLRECONF;
		goto rr_end; //lint !e801
	}

	// If the rx_restarting flag is set, then the restart is just
	// beginning; return true.
	if (rp->rl_xstate[slave].rx_restarting) {
		retval = B_TRUE;
		goto rr_end; //lint !e801
	}

	//
	// We set specific error codes and return B_FALSE if the resource
	// that we were trying to restart is found to be disabled, or in an
	// error state on the slave.
	//
	if (((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->r_switch[slave]
	    != SCHA_SWITCH_ENABLED) {
		errcode = SCHA_ERR_RSTATE;
		goto rr_end; //lint !e801
	} else if (rx_state == rgm::R_START_FAILED ||
	    rx_state == rgm::R_MON_FAILED) {
		errcode = SCHA_ERR_STARTFAILED;
		goto rr_end; //lint !e801
	} else if (rx_state == rgm::R_STOP_FAILED) {
		errcode = SCHA_ERR_STOPFAILED;
		goto rr_end; //lint !e801
	}

	//
	// The slave might not be the president, so we cannot just
	// check the restart_flg of the resource (which is local to the slave).
	// We may need to make an idl call to the slave to fetch the value
	// of the restarting flag, but first we rule out the possibility in
	// other ways.
	//
	// We deduce that the resource is no longer restarting if one of
	// the following is true:
	//
	// - the RG is in a terminal state on the slave
	// - the RG is OFF_PENDING_METHODS, OFF_PENDING_BOOT, or
	//   PENDING_OFF_STOP_FAILED on the slave
	// - the RG is ON_PENDING_METHODS, ON_PENDING_DISABLED, or
	//   ON_PENDING_MON_DISABLED on the slave [however, these RG states
	//   might contain a restarting R when we implement restart
	//   dependencies]
	//

	switch (rp->rl_rg->rgl_xstate[slave].rgx_state) {
	//
	// The R cannot still be restarting in the following RG states, which
	// imply that an RG reconfiguration occurred:
	//
	case rgm::RG_OFFLINE:
	case rgm::RG_OFFLINE_START_FAILED:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_OFF_PENDING_BOOT:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_PENDING_OFF_START_FAILED:
		errcode = SCHA_ERR_RGRECONF;
		goto rr_end; //lint !e801

	// The R restarted successfully or is in R_STARTING_DEPEND
	case rgm::RG_ONLINE:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
		break;

	// For the rest of the states, we need to check the value
	// of the flag on the slave
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_ON_PENDING_R_RESTART:
	case rgm::RG_ON_PENDING_METHODS:
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
	default:
		rgm::r_restart_t restart_flag;
		if (call_fetch_restarting_flag(slave, rp->rl_ccrdata->r_name,
		    restart_flag) != RGMIDL_OK) {
			// if we get an idl error, assume the slave is down
			// and the R isn't restarting anymore.
			retval = B_FALSE;
			goto rr_end; //lint !e801
		}
		if (restart_flag != rgm::RR_NONE) {
			retval = B_TRUE;
		}
		break;
	}
rr_end:
	if (retcode != NULL) {
		*retcode = errcode;
	}
	return (retval);
}


//
// rg_state_permits_restart
//
// Returns B_TRUE if the RG state on the given node permits a restart.
// If 'resource_restart' is true, this is a resource_restart; otherwise
// it is an RG restart.
//
// For RG restarts, the RG must be in an "online" state on the given node.
// For resource restarts, the RG may be in other states such as
// pending online or pending methods.
//
// This function must be called on the president, while holding the
// RGM state lock.  rgp must be non-null and nodeid must be a valid node id.
//
static boolean_t
rg_state_permits_restart(rglist_p_t rgp, rgm::lni_t lni,
    boolean_t resource_restart)
{
	boolean_t rg_is_online = rg_on_or_r_restart(rgp, lni);
	rgm::rgm_rg_state rgx_state = rgp->rgl_xstate[lni].rgx_state;

	if (!resource_restart) {
		// this is an rg restart or check_restart
		return (rg_is_online);
	}

	//
	// This is a resource restart.  All of the following states are
	// permitted.
	//
	// We allow PENDING_OFFLINE because it is theoretically possible
	// for the RG to change direction to PENDING_ONLINE without having
	// stopped all of its resources.  If the RG ends up going offline,
	// the restart won't occur and the resource (and its monitor) will
	// end up being stopped.
	//
	// For ON_PENDING_METHODS, the slave state machine will postpone
	// the resource restart until after the update is completed, and
	// then will execute the restart.
	//
	if (rg_is_online ||
	    rgx_state == rgm::RG_PENDING_ON_STARTED ||
	    rgx_state == rgm::RG_PENDING_ONLINE ||
	    rgx_state == rgm::RG_PENDING_OFFLINE ||
	    rgx_state == rgm::RG_ON_PENDING_DISABLED ||
	    rgx_state == rgm::RG_ON_PENDING_MON_DISABLED ||
	    rgx_state == rgm::RG_ON_PENDING_METHODS) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}
#endif
