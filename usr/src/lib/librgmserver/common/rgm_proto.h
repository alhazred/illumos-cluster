/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * rgm_proto.h
 * function prototypes and global variable declarations for rgm module
 * external interfaces
 */

#ifndef	_RGM_PROTO_H
#define	_RGM_PROTO_H

#pragma ident	"@(#)rgm_proto.h	1.190	09/03/02 SMI"

#include <syslog.h>

#include <orb/infrastructure/orb_conf.h>
#include <cmm/ucmm_api.h>
#include <rgm_state.h>
#include <rgm/rgm_cmn_proto.h>
#include <rgmd_util.h>
#include <rgm_comm.h>

#include <rgm/security.h>
#include <rgm/fecl.h>

#include <rgm/rgm_recep.h>	/* needed for strarr_list defintion */

#include <sys/cl_events.h>
#include <rgm/rgm_cpp.h>
#include <scadmin/scconf.h>

#include <cmm/membership_client.h>

/* Zones support */
#include <rgm_logical_node_manager.h>
#ifdef EUROPA_FARM
#include <rgm/rpc/rgmx_comm.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if (SOL_VERSION >= __s10)
char PP_PATH[ZONENAME_MAX + 31];
#else
#define	DIR_PATH_LEN	64
char PP_PATH[DIR_PATH_LEN + 31];
#endif


/* common constants */

#define	RGMD_BINARY	"/usr/cluster/lib/sc/rgmd"
#define	RGMD_DEBUG_BINARY	"/usr/cluster/lib/sc/rgmd -d"
#define	RGMD_PROC_NAME	"rgmd"

#define	CLUSTER_NAME_DELIMITER		':'
#define	CLUSTER_NAME_DELIMITER_STR	":"

#if (SOL_VERSION >= __s10)
extern os::mutex_t rgm_reconfig_lock;
extern os::condvar_t rgm_reconfig_cv;
struct rgm_reconfig_membership_callback_data_t {
	cmm::membership_t	membership;
	cmm::seqnum_t		seqnum;
};
extern rgm_reconfig_membership_callback_data_t rgm_reconfig_membership_cb_data;
extern boolean_t rgm_reconfig_membership_changed;
extern os::mutex_t peer_rgm_state_lock;
extern os::condvar_t peer_rgm_state_cv;
struct peer_rgm_state_t {
	uint32_t step_num;
	cmm::seqnum_t seqnum;
};
extern peer_rgm_state_t peer_rgm_state[NODEID_MAX+1];
extern uint32_t lowest_nid_step_num;
extern cmm::seqnum_t lowest_nid_seqnum;
extern rgm::sync_result_t lowest_nid_sync_result;
void rgm_process_membership_callback(
	const char *cluster_namep, const char *membership_namep,
	const cmm::membership_t membership, const cmm::seqnum_t seqnum);
void rgm_reconfig_process_membership_after_upgrade();
void rgm_process_membership_callback_internal(
    const cmm::membership_t membership, const cmm::seqnum_t seqnum);
#endif

#define	MAX_COMM_NAMETAG 100

#define	RGM_DBG_BUFSIZE	(64 * 1024) /* 64KB ring buffer for tracing */
//
// Assuming 32 bit integers.
//
#define	MAX_LNI_STR 10;

extern dbg_print_buf *rgm_dbg_buf;

/* global variable declarations */

extern char *ZONE;
char *getlocalhostname();
extern char *progname;
extern char rgm_user_name[69];
extern const char *rgm_product_str;
extern const char *syslog_tag;
extern boolean_t Debugflag;
/* if system dump to be taken when node aborts due to failed method */
extern boolean_t System_dumpflag;
/* if method coredump to be taken when it times out. */
extern boolean_t Method_dumpflag;
extern char *rgmd_sched_class;
extern pri_t rgmd_sched_priority;
extern cmm::callback_info cbinfo;

/* function prototypes */
void register_for_failfast(char *failfast_string);
void failfast_disarm();
void update_onoff_monitored_switch(rgm_resource_t *, nodeidlist *);
boolean_t in_current_logical_membership(rgm::lni_t node);
boolean_t is_zone_up(rgm::lni_t node);
boolean_t in_current_physical_membership(sol::nodeid_t node);
boolean_t in_endish_rg_state(rglist_p_t rg_ptr,
    LogicalNodeset *pending_boot_nodes, LogicalNodeset *stop_failed_nodes,
    LogicalNodeset *offline_nodes, LogicalNodeset *r_restart_nodes);
boolean_t is_global_zone(rgm::lni_t lni);
boolean_t is_rg_online_on_node(rglist_p_t rgp, sol::nodeid_t nodeid,
    boolean_t logical_node);
boolean_t is_r_offline(rgm::rgm_r_state r_state);
boolean_t is_r_stopping(rgm::rgm_r_state r_state);
boolean_t is_rg_online_not_errored(rglist_p_t rgp, rgm::lni_t lni,
    boolean_t count_pending_offline, boolean_t logical_node = B_TRUE,
    rgm::lni_t *error_node = NULL);
boolean_t is_local_mon_enabled(rlist_p_t r, rgm::lni_t lni);
boolean_t is_r_monitored(rlist_p_t r, name_t *rt_meth_name, rgm::lni_t lni);
boolean_t is_r_globalzone(rlist_p_t r);
boolean_t was_rg_available(rglist_p_t rgp, rgm::lni_t n);
boolean_t is_any_rs_faulted(rglist_p_t rgp, rgm::lni_t lni);
boolean_t is_r_monitor_checked(rlist_p_t r, rgm::lni_t lni);
boolean_t is_method_reg(rgm_rt_t *rt, method_t m, name_t *rt_meth_name);
boolean_t is_r_scalable(rgm_resource_t *r);
boolean_t is_rg1_dep_on_rg2(scha_errmsg_t *res, rgm_rg_t *rg1, rgm_rg_t *rg2);
boolean_t namelist_contains(const namelist_t *nl, const char *what);
boolean_t rdeplist_contains(const rdeplist_t *nl, const char *what);
boolean_t nodeidlist_contains(const nodeidlist_t *nl, const char *what);
boolean_t nodeidlist_contains_lni(const nodeidlist_t *nl, rgm::lni_t lni);
namelist_t *namelist_copy(namelist_t *curr_namelist);
void namelist_append(namelist_t *curr_namelist, namelist_t **res_list);
namelist_t *add_sa_dependee_rgs_to_list(scha_errmsg_t *res,
    const rglist_p_t rg_p, rgm_resource_t *rs);
boolean_t stale_idl_call(sol::nodeid_t, sol::incarnation_num);
boolean_t is_fm_status_unchanged(sol::nodeid_t nodeid, rlist_p_t r,
    rgm_direction_t direction);
scha_err_t copy_default_value(const char *prop_name, rlist_p_t r,
    char **value);
boolean_t rg_on_or_r_restart(rglist_p_t rgp, sol::nodeid_t nodeid);
boolean_t is_r_started(rlist_p_t r, rgm::rgm_r_state rx_state, rgm::lni_t lni);
boolean_t has_net_relative_methods(rlist_p_t r);
boolean_t depends_on_r(rlist_p_t r_ptr);
boolean_t node_is_evacuating(const rgm::lni_t nodeid, const rglist_p_t rgp);
boolean_t is_rg_in_list(const char *rgname, const rglist_p_t *rglistp);
scha_err_t wait_for(rglist_p_t, char *, rgm::rgm_rg_state);
boolean_t is_rg_in_state(rglist_p_t rgp, rgm::rgm_rg_state rg_st,
    rgm::lni_t lni);
const rgm_param_t *get_param_prop(const char *prop_name,
    boolean_t is_extension, rgm_param_t **paramtable);
boolean_t is_fast_quiesce_running(const char *rgname);
boolean_t is_slow_quiesce_running(const char *rgname);
#define	is_quiesce_running(rg)	(is_fast_quiesce_running(rg) ||	\
    is_slow_quiesce_running(rg))
void quiesce_clear_nameserver(rglist_p_t rgp, boolean_t is_fast_quiesce);

void ngzone_nodelist_check(const rglist_p_t rg_ptr, uint_t nodeid, const
    char *zonename, boolean_t &globalzone, boolean_t &nodelist_contains);
void nodelist_check(const rglist_p_t rg_ptr, uint_t nodeid, const
    char *zonename,  boolean_t &nodelist_contains);
void ngzone_check(const char *zonename, boolean_t &globalzone);
scha_errmsg_t prune_operand_list(const rgm::idl_string_seq_t inlist,
    rgm::idl_string_seq_t &outlist, boolean_t checking_r);
#if SOL_VERSION >= __s10
scha_err_t authenticate_client_cred(char *rg_name);
#endif

void set_fed_core_props(fe_cmd &fed_cmd, const char *fed_tag,
    char *zonename, boolean_t globalzoneflag);

void parse_fed_tag(char *str, char **z_str, char **rg_str, char **rs_str,
    char **meth_str, method_t *mt);

/* Function prototype to get the syslog priority for R/RG state transitions */
int get_log_level_for_res_state(rgm::rgm_r_state r_state);

/*
 * rgmcnfg_set_onoffswitch(), rgmcnfg_set_monitoredswitch()
 *	These functions are used to change the OnoffSwitch and
 *	MonitoredSwitch of a resource on a specified set of nodes.
 */
scha_errmsg_t rgmcnfg_set_onoffswitch(
    char *rgname, char *rs_name, scha_switch_t sw, LogicalNodeset *ns);
scha_errmsg_t rgmcnfg_set_monitoredswitch(
    char *rgname, char *rs_name, scha_switch_t sw, LogicalNodeset *ns);

/*
 * The difference between get_prop_value and convert_prop_value is that
 * get_prop_value takes an rgm::idl_nameval_seq_t, searches through the
 * entire list to find the property with name "property", then calls
 * convert_prop_value to do the actual conversion on the rgm:idl_nameval.
 */
scha_errmsg_t get_prop_value(const char *property,
    const rgm::idl_nameval_seq_t &nv_seq, char **val,
    scha_prop_type_t type);
scha_errmsg_t get_prop_value_ext(const char *property,
    const rgm::idl_nameval_node_seq_t &nv_seq,
    std::map<rgm::lni_t, char *> &val,
    rgm_property_list_t *ptr, LogicalNodeset *ns,
    LogicalNodeset e_ns, scha_prop_type_t type, rgm_tune_t tuneable);

scha_errmsg_t convert_prop_value(const char *property,
    const rgm::idl_nameval &nv, char **val, scha_prop_type_t type);

char *pr_rfmstatus(rgm::rgm_r_fm_status val);
char *pr_rgstate(rgm::rgm_rg_state val);
char *pr_rstate(rgm::rgm_r_state val);

char *rgm_get_nodename_cache(sol::nodeid_t);
char *rgm_get_nodename(sol::nodeid_t);
int rgm_get_nodeid(char *nodename, scconf_nodeid_t *nodeidp);
rgm::lni_t rgm_get_lni(const char *nodename);
char *strseq_to_str(const rgm::idl_string_seq_t &nodeseq);

const char *get_local_nodename(void);
sol::nodeid_t get_local_nodeid(void);
const char *rt_method_string(method_t meth);
const char *pr_change_tag(rgm::notify_change_tag ct);

const rgm::idl_string_seq_t &get_value_list(const char *property,
    const rgm::idl_nameval_seq_t &nv_seq);
void update_all_nodes_val(const rgm::idl_nameval_node_seq_t &str_seq,
    rgm_property_list_t *rs_conf, rgm_property_list_t *rs,
    rgm::idl_string_seq_t &str_seq_all_nodes,
    LogicalNodeset *ns);
std::map<rgm::lni_t, char *> find_ext_prop(rgm_property_list_t *rp, char *key);
const rgm::idl_string_seq_t &get_value_list_ext(const char *property,
    const rgm::idl_nameval_node_seq_t &nv_seq);



LogicalNodeset *compute_meth_nodes(rgm_rt_t *rt, rgm_rg_t *rgl_ccr,
    method_t meth, boolean_t is_scalable);
#ifdef FUTURE
nodeset_t get_static_nodeset(void);
#endif	/* FUTURE */

void register_ccr_callbacks(void);
int create_cache(boolean_t file_only);
void update_cluster_state(
    boolean_t init_europa_nodes, boolean_t use_membership_api);

// SC SLM addon start
void scslm_get_scconf(unsigned int *, unsigned int *);
void scslm_set_scconf(unsigned int, unsigned int);
// SC SLM addon end
void reset_fm_status(sol::nodeid_t nodeid, rlist_p_t r,
    rgm::rgm_r_fm_status newstatus, rgm_direction_t direction);
void append_busted_msg(scha_errmsg_t *res, rglist_p_t rgp,
    scha_err_t errcode);
void reset_rg_offline(rglist_p_t rgp, rgm::lni_t n,
    boolean_t trigger_offline_restart_deps);

boolean_t am_i_president(void);
scha_errmsg_t get_bool_value(const char *property,
    const rgm::idl_nameval_seq_t &nv_seq, boolean_t *retval);
scha_errmsg_t get_int_value(const char *property,
    const rgm::idl_nameval_seq_t &nv_seq, int *retval);
void r_to_strseq(rgm_resource_t *r, rgm::idl_string_seq_t &props,
    rgm::idl_string_seq_t &ext_props, rgm::idl_string_seq_t &all_node_ext,
    LogicalNodeset *ns);
void rg_to_strseq(rgm_rg_t *rg, rgm::idl_string_seq_t &props);
void update_rg_to_strseq(rgm_rg_t *rg, rgm::idl_string_seq_t &props,
    changed_rg_prop *changed);
uint_t nvseq_to_strseq(const rgm::idl_nameval_seq_t &nv_seq,
    rgm::idl_string_seq_t &str_seq, rgm_resource_t *rs);
void nvseq_copy(const rgm::idl_nameval_seq_t &nv_seq,
    rgm::idl_nameval_seq_t &nv_out_seq);
uint_t nvseq_to_strseq_ext(const rgm::idl_nameval_node_seq_t &nv_seq,
    rgm::idl_string_seq_t &str_seq, rgm_resource_t *rs);

void update_all_nodes_value(std::map<rgm::lni_t, char *> &, name_t key,
    rgm::idl_string_seq_t &all_nodes_ext, int *index,
    LogicalNodeset  *ns);
boolean_t is_ext_prop_updated(name_t key, const
    rgm::idl_nameval_node_seq_t &nv_seq);
scha_errmsg_t
convert_prop_value_ext(const char *property, const rgm::idl_nameval_node &nv,
    std::map<rgm::lni_t, char *> &, rgm_property_list_t *ptr,
    boolean_t is_first, LogicalNodeset *ns, LogicalNodeset e_ns,
    scha_prop_type_t type, rgm_tune_t tuneable);
const char *rgm_lni_to_nodename(rgm::lni_t lni);

void run_state_machine(rgm::lni_t lni, const char *msg);
void run_r_rg_cleanup(const char *r_rg_name, const char *dep_aff_name,
	boolean_t rg_flag, deptype dep);
void wait_until_ccr_is_read(void);

scha_errmsg_t rgm_init_ccr(void);
scha_errmsg_t rgm_update_rts(void);
scha_errmsg_t rgm_add_rg(char *rg_name, rglist_t **new_rg);
scha_errmsg_t rgm_add_r(char *r_name, char *rg_name, rlist_t **new_r);
scha_errmsg_t rgm_add_rt(char *rt_name, rgm_rt_t **new_rt);
scha_errmsg_t rgm_delete_rg(char *rgname);
scha_errmsg_t rgm_delete_r(char *r_name, char *rg_name);
scha_errmsg_t rgm_delete_rt(char *rtname);
scha_errmsg_t rgm_update_rg(char *rg_name);
scha_errmsg_t rgm_update_rt(char *rt_name);
scha_errmsg_t rgm_update_r(char *r_name, char *rg_name);
scha_errmsg_t rglist_add_rg(rglist_p_t *&rglistp,
    const rglist_p_t rg_ptr, uint_t numrgs);

uint_t strseq_to_array(const rgm::idl_string_seq_t &str_seq,
    strarr_list **str_list);
scha_err_t scha_translate_error(scconf_errno_t scconf_err);

nodeidlist_t *get_static_nodelist(void);
LogicalNodeset *get_logical_nodeset_from_nodelist(nodeidlist_t *);
LogicalNodeset *get_logical_nodeset_from_nodelist(nodeidlist_t *nodelist);
scha_err_t nodeidlist_to_namelist(nodeidlist_t *nidlst, namelist_t ** nlst);
scha_errmsg_t get_value_nodeid_array(char *rg_name, const char *property,
    const rgm::idl_nameval_seq_t &nv_seq, nodeidlist_t **nodelist);
scha_errmsg_t parse_logical_nodename(const char *ln, sol::nodeid_t &nodeid,
    char *&zonenamep);
LogicalNode *get_ln(const char *lnName, scha_err_t *err_out = NULL);
char *get_string_from_nodeset(LogicalNodeset &nset);

// MDB helper functions.
scha_errmsg_t update_mdb_extback(mdb_rxback_t& rx_back,
    rgm_property_list_t *ext_prop);
scha_errmsg_t update_mdb_stdback(mdb_rxback_t& rx_back,
    rgm_property_list_t *std_prop);
char *switchmap_to_string(std::map<rgm_lni_t, scha_switch_t> swtch);
void free_extbacks(mdb_rxback_t& rx_back);
void free_stdbacks(mdb_rxback_t& rx_back);


/*
 * The difference between get_value_string_array and convert_value_string_array
 * is that get_value_string_array takes an rgm::idl_nameval_seq_t, searches
 * through the entire list to find the property with name "property", then
 * calls convert_value_string_array to do the actual conversion on the
 * rgm:idl_nameval.
 */
namelist_t *get_value_string_array(const char *property,
    const rgm::idl_nameval_seq_t &nv_seq);
namelist_t *convert_value_string_array(
    const rgm::idl_string_seq_t &valseq);
scha_errmsg_t convert_value_nodeid_array(char *rg_name,
    const rgm::idl_string_seq_t &valseq, nodeidlist_t **nodelist);

rlist_p_t *convert_seq_to_rlistp(const rgm::idl_string_seq_t &valseq);
namelist_all_t get_value_string_all_list(const char *property,
    const rgm::idl_nameval_seq_t &nv_seq, boolean_t update_op);
namelist_t *get_value_string_array_ext(const char *property,
    const rgm::idl_nameval_node_seq_t &nv_seq);

/*
 * The get_value_string_array, convert_value_string_array variants
 * returning rdeplist_t
 */
rdeplist_t *get_value_string_array_dep(const char *property,
    const rgm::idl_nameval_seq_t &nv_seq, scha_errmsg_t &res);
rdeplist_t *convert_value_string_array_dep(
    const rgm::idl_string_seq_t &valseq, scha_errmsg_t &res);


name_t rt_method_name(rgm_rt_t *prt, method_t method);
scha_errmsg_t rt_method_timeout(rlist_p_t r, name_t methname,
    method_t method, int *timeout);

rfmstatusname_t *get_rfmstatus(rgm::rgm_r_fm_status rgm_val,
    char *statusname, scha_rsstatus_t api_val);

rglist_p_t rgname_to_rg(const char *rgname);

rgm::rgm_comm_ptr rgm_comm_getref(sol::nodeid_t nid);
rgm::rgm_comm_ptr rgm_comm_waitforpres(rgm::rgm_comm_ptr p);

rlist_p_t rname_to_r(rglist_p_t rgp, const char *rname);
rgm_rt_t *find_rt_in_memory(const char *rtname);
rgm_rt_t *rtname_to_rt(const char *rtname, scha_err_t *errcode = NULL);

rgstatename_t *get_rgstate(rgm::rgm_rg_state rgm_val, char *statename,
    scha_rgstate_t api_val, rglist_p_t rgp, rgm::lni_t lni);
rstatename_t *get_rstate(rgm::rgm_r_state rgm_val, char *statename,
    scha_rsstate_t api_val);

void do_rg_switch(scha_errmsg_t *res, rglist_p_t *rglistp,
    LogicalNodeset &mnodeset, LogicalNodeset &future_nodeset,
    boolean_t offline_action_flg, boolean_t online_action_flg,
    namelist_t *oplist);
scha_errmsg_t switch_order_rgarray(rglist_p_t rgs[]);
void reverse_switch_order_array(void **array);
scha_err_t map_busted_err(rg_switch_t arg);
scha_errmsg_t process_resource(rgm::lni_t lni, rlist_p_t r, rglist_p_t rg,
    boolean_t *r_state_changed, boolean_t *all_off, boolean_t *all_on,
    boolean_t *all_mon, boolean_t *all_methods, boolean_t *any_restart,
    boolean_t *any_ok_start, boolean_t *any_starting_depend,
    boolean_t *any_prenet_started, boolean_t *all_dis_off,
    boolean_t *any_stop_failed);
scha_errmsg_t rgm_run_state(rgm::lni_t lni);
scha_errmsg_t get_fo_mode(rgm_resource_t *r,
    scha_failover_mode_t *fo_mode);
scha_errmsg_t get_retry_interval(rlist_p_t rp, time_t *retry_int);
scha_errmsg_t get_retry_count(rlist_p_t rp, int *count);
scha_errmsg_t rgm_comm_setfmstatus(char *rs_name, char *rg_name,
    char *node_name, char *status, char *status_msg);
scha_errmsg_t rgm_comm_getfmstatus(char *rs_name, char *rg_name,
    char *node_name, scha_rsstatus_t *status, char **status_msg,
    char **seq_id);
scha_errmsg_t rgm_comm_getrsswitch(char *rs_name, char *rg_name,
    char *node_name, scha_switch_t *swtch, bool on_off, char **seq_id);
scha_errmsg_t rgm_comm_getext(char *rs_name, char *rg_name,
    char *node_name, char *prop_name, char **value, char **seq_id);
scha_errmsg_t rgm_comm_getrsstate(char *rs_name, char *rg_name,
    char *node_name, scha_rsstate_t *state, char **seq_id);
scha_errmsg_t rgm_comm_getfailedstatus(char *rs_name, char *rg_name,
    char *node_name, char *method_name, boolean_t *failstatus,
    char **seq_id);
void update_pn_default_value(rgm_property_list_t *ptr, nodeidlist_t *nl);
scha_errmsg_t rgm_comm_getrgstate(char *rg_name, char *node_name,
    scha_rgstate_t *rgstate, char **seq_id);
scha_errmsg_t update_rg_seq_id(rglist_t *rg_ptr);

idlretval_t fetch_slave_state(sol::nodeid_t nodeid);
void flush_state_change_queue(boolean_t use_qempty = B_FALSE);
void launch_method(launchargs_t *l, rgm::lni_t lni);
scha_errmsg_t launch_validate_method(launchvalargs_t *l, rgm::lni_t lni);
void namelist_to_seq(namelist_t *inlist, rgm::idl_string_seq_t &outlist);
void process_needy_rg(sol::nodeid_t, rglist_p_t,
    rgm::rgm_rg_state old_rg_state);
void process_rgs_on_death(boolean_t i_am_new_president,
    LogicalNodeset &join_ns, boolean_t zone_death_only);
void process_rg(rglist_p_t rglp, boolean_t i_am_new_president,
    const LogicalNodeset &join_ns, boolean_t zone_death_only);
void delayed_rebalance(LogicalNodeset &joiners, boolean_t is_new_president);
void rebalance_rgs(rglist_p_t *rglistp, boolean_t check_for_failures,
    LogicalNodeset &joiners, boolean_t is_new_president,
    boolean_t check_for_rg_suspended, boolean_t set_planned_on_nodes = B_FALSE);
void rebalance(rglist_p_t rglp, LogicalNodeset &join_ns, boolean_t new_pres,
    boolean_t check_for_failures = B_TRUE,
    boolean_t check_for_rg_suspended = B_TRUE,
    boolean_t set_planned_on_nodes = B_FALSE);
void register_president(void);
void rgm_add_nodename(sol::nodeid_t nodeid, char *name);
void rgm_comm_init(void);
void rgm_comm_update(nodeset_t newset);
void rgm_free_timelist(rgm_timelist_t **timelistp);
void rgm_lock_state(void);
boolean_t rgm_state_lock_held();
void member_change(const char *, const cmm::membership_t membership,
    const cmm::seqnum_t seqnum);

#if SOL_VERSION >= __s10
void rgm_zone_reconfig(const char *cluster, const char *membership_namep,
    const cmm::membership_t membership, const cmm::seqnum_t seqnum);
#endif

void rgm_reconfig(LogicalNodeset &ns, boolean_t isucmm);
void rgm_reconfig_helper(LogicalNodeset &ns, boolean_t isucmm);
void rgm_setup(void);
void rgm_unlock_state(void);
void rg_to_rgstate(rglist_p_t in_rg, rgm::rgm_rg_state currstate,
    rgm::rg_state *out_rgstate);
void recep_thread_start(void);
void *sync_thread_start(void *arg);
void *rgm_block_thread_start(void *arg);
bool is_rgm_block_complete(void);
void *fencing_thread_start(void *arg);
void trigger_wait_for_fencing(void);
bool need_wait_fencing(rglist_p_t rglp, rgm::lni_t mynodeid);
bool is_fencing_complete(void);
void rgm_update_pres_state(sol::nodeid_t nodeid, rgm::node_state *state);
void slave_lock_state(void);
void slave_unlock_state(void);

boolean_t namelist_exists(namelist_t *curr_name_list, name_t name);
boolean_t rdeplist_exists(rdeplist_t *curr_name_list, name_t name);

void namelist_delete_name(namelist_t **curr_namelist, const char *name);
void rdeplist_delete_name(rdeplist_t **curr_rdeplist, const name_t name);
scha_errmsg_t rdeplist_add_name(rdeplist_t **curr_rdeplist, name_t name,
    rdep_locality_t dep_type);

void check_dep_local(rlist_p_t rp, rlist_p_t rp_dep, rdep_locality_t type,
    boolean_t *is_local_node, boolean_t *is_local_node_weak);

scha_errmsg_t detect_rg_dependencies_cycles(rgm_rg_t *proposed_rg,
    rgm_affinities_t *affinities, namelist_t *);
scha_errmsg_t detect_r_dependencies_cycles(rgm_resource_t *proposed_r,
    boolean_t skip_dep_restart, boolean_t skip_off_restart,
    boolean_t skip_dep_strong, boolean_t skip_dep_weak,
    boolean_t implicit_dep_check, boolean_t check_icrd_flag);
scha_errmsg_t detect_combined_dependencies_cycles(rglist_p_t rgp,
    rgm_rg_t *proposed_rg, rgm_resource_t *extra_r);
void unlink_dependents_for_resource(rlist_p_t dep_rp);
scha_errmsg_t set_rl_dependents_for_resource(rlist_p_t dep_rp);
scha_errmsg_t update_rl_dependents_for_resource(rlist_p_t dep_rp);
scha_errmsg_t compute_rl_dependents(void);
boolean_t compare_rgm_dependencies(rgm_dependencies_t *rgm_dep1,
    rgm_dependencies_t *rgm_dep2);
void free_rgm_dependencies(rgm_dependencies_t *rgm_dep);

/* Helper functions for rg affinities */
scha_errmsg_t convert_affinities_list(const char *rg_name,
    namelist_t *raw_list, rgm_affinities_t *affinities);
void free_rgm_affinities(rgm_affinities_t *affinities);
scha_errmsg_t set_affinitents_for_resource_group(rglist_p_t dep_rgp);
scha_errmsg_t compute_all_affinities(void);
void unlink_affinitents_for_resource_group(rglist_p_t rgp);
boolean_t namelist_compare(namelist_t *namelist_1,
    namelist_t *namelist_2);
boolean_t rdeplist_compare(rdeplist_t *rdeplist_1,
	rdeplist_t *rdeplist_2);
boolean_t use_logicalnode_affinities(void);
void get_all_zones_on_phys_node(rgm::lni_t lni, LogicalNodeset &nset);

scha_errmsg_t update_affinitents_for_resource_group(rglist_p_t rgp);
void debug_print_affinities(void);
scha_errmsg_t check_pos_affinity_managed(const rgm_rg_t *prg,
    const rgm_affinities_t *affinities, boolean_t managed_rg,
    const rglist_p_t *rglistp, namelist_t *failed_rglist);
void run_rebalance_on_affinitents(rglist_p_t rgp,
    boolean_t positive_affs, boolean_t cleanup_stale_affs);

char *namelist_to_str(namelist_t *l);
char *rdeplist_to_str(rdeplist_t *l);
char *rdeplistQ_to_str(rdeplist_t *l);

boolean_t sn_affinities_satisfied(scha_err_t &err, rglist_p_t rgp,
    sol::nodeid_t nodeid, namelist_t *extras_online, namelist_t **offenders);
boolean_t sp_affinities_satisfied(scha_err_t &err, rglist_p_t rgp,
    sol::nodeid_t nodeid, namelist_t *extras_online, namelist_t **offenders);
void check_offline_dependees(scha_errmsg_t *res, rglist_p_t rgdepp,
    rglist_p_t *rglistp, LogicalNodeset &online_nodeset);
boolean_t compute_sn_violations(scha_err_t &err, rglist_p_t rgp,
    sol::nodeid_t nodeid, namelist_t **violations, int &count,
    boolean_t count_pending_offline);
boolean_t compute_sp_transitive_closure(scha_err_t &err, rglist_p_t rgp,
    sol::nodeid_t nodeid, namelist_t *start_list, namelist_t **tclist,
    int &count, boolean_t check_stop_failed, boolean_t check_start_list);
uint_t compute_enabled_resources(rglist_p_t rgp, rgm::lni_t lni);
int compute_outgoing_weak_affinities(rglist_p_t rgp,
    sol::nodeid_t nodeid, namelist_t *offlist);
int compute_incoming_weak_affinities(rglist_p_t rgp,
    sol::nodeid_t nodeid, namelist_t *offlist);
scha_errmsg_t sort_candidate_nodes(const LogicalNodeset &to_check_nodes,
    rglist_p_t rglp, candidate_t *&cands, size_t &num_cands,
    const LogicalNodeset &ignore_ns,
    int &num_online, LogicalNodeset &from_nodeset,
    int &num_primaries, boolean_t new_pres, boolean_t check_state,
    boolean_t check_zone_bootdelay, boolean_t switch_back,
    boolean_t check_for_failures = B_TRUE);
void free_candidate_list(candidate_t cands[], uint_t num_cands);

boolean_t is_r_starting(rgm::rgm_r_state r_state);
boolean_t is_r_online(rgm::rgm_r_state r_state);

scha_errmsg_t check_nw_res_used(rglist_p_t rgp, rgm_resource_t *src_res,
    namelist_t *nw_res_used);
scha_errmsg_t check_nw_res_used_enabled(name_t r_name,
    namelist_t *nw_res_used, namelist_t *rnames);
scha_errmsg_t rg_sanity_checks(rgm_rg_t *prg, rgm_affinities_t *affinities,
    boolean_t managed_rg, rglist_p_t *rglistp,
    const rgm::idl_string_seq_t &rglist);
scha_errmsg_t rgnodelist_subset_rtnodelist(char *rg_name,
    nodeidlist_t *nodelist, rgm_rt_t *rt);
const char *get_dup_name(namelist_t *list);
const char *get_dup_rdepname(rdeplist_t *list);
rgm::lni_t get_dup_lni(nodeidlist_t *nl);

void FAULT_POINT_FOR_ORPHAN_ORB_CALL(char *function,
    sol::nodeid_t node, sol::incarnation_num incarnation);
#ifdef DEBUG
void FAULT_POINT_FOR_UNREGISTERED_PRES(void);
#endif /* DEBUG */

void failback(sol::nodeid_t nid, const char *rgname);
boolean_t is_failback_required(rglist_p_t rgp, LogicalNodeset* joiners,
    LogicalNodeset **nspp, boolean_t &need_bootdelay, boolean_t switch_back,
    scha_errmsg_t *res);
void rgm_manage_threads_and_swap(void);

void close_on_exec(int fd);

void log_error(int syslog_priority, char *messageID, char *msg, ...);
char *rgm_msgid(const char *msgid);
void ucmm_print(const char *msgid, const char *msg, ...);
void ducmm_print(const char *msgid, const char *msg, ...);

boolean_t	touch_file(const char *fname, rgm::lni_t lni);
scha_errmsg_t	get_timestamp(const char *fname, time_t *ti, rgm::lni_t lni);

void sched_init(void);
void sched_reset_child(void);

void thr_create_err(void *stack_base, size_t stack_size,
    void *(*startfunc)(void *), void *arg, long flags, thread_t *new_thread);
void *calloc_nocheck(size_t num, size_t size);
scha_errmsg_t set_bool_value(char *old_value,
    std::map<rgm::lni_t, char *> &new_value, char *prop_name);

scha_errmsg_t set_bool_value_ext(std::map<rgm::lni_t, char *> &old_value,
    std::map<rgm::lni_t, char *> &, char *prop_name, boolean_t is_per_node,
    LogicalNodeset *ns, LogicalNodeset e_ns, rgm_tune_t tuneable);

void syslog_fmstatus_msg(const char *rs_name, const char *nodename,
    rgm::rgm_r_fm_status old_fm_status, rgm::rgm_r_fm_status new_fm_status,
    const char *old_fm_msg, const char *new_fm_msg);
void remove_pingpong_history(const char *rs_name, rgm::lni_t lni);

char *get_project_name(const rgm_resource_t *rs, const rgm_rg_t *rg);
    scha_errmsg_t validate_project(char *proj_name, const char *prop);

void update_all_nodes_val_add(rgm_resource_t *rs,
    rgm::idl_string_seq_t &all_nodes_ext);
void update_local_node_value_add(rgm_resource_t *rs,
    rgm::idl_string_seq_t &ext_p, rgm::lni_t lni, int len);

void update_local_node_value(rgm::idl_string_seq_t &str_seq,
    rgm_property_list_t *ptr_ext,
    const rgm::idl_nameval_node_seq_t &nv_seq,
    rgm_resource_t *rsrc, rgm::lni_t lni, uint_t len);

void set_rx_state(rgm::lni_t lni, rlist_p_t rp, rgm::rgm_r_state rx_state,
    boolean_t force_trigger_offline_restart_deps = B_FALSE,
    boolean_t mutex_held = B_TRUE);
void set_rgx_state(rgm::lni_t lni, rglist_p_t rgp, rgm::rgm_rg_state rgx_state);

void get_cluster_info(void);

scha_errmsg_t post_event_membership(LogicalNodeset &prev_ns,
	LogicalNodeset &current_ns);
scha_errmsg_t post_event_rg_state(const char *rg_name);
scha_errmsg_t post_event_rs_state(const char *rs_name);
scha_errmsg_t post_primaries_changing_event(const char *rs_name,
    const char *rg_name, cl_event_reason_code_t reason, const char *node_name,
    int rg_desired_primaries, const LogicalNodeset &current_nodeset,
    const LogicalNodeset &new_nodeset);
scha_errmsg_t setfmstatus_helper(sol::nodeid_t nodeid, const char *rs_name,
    const char *rg_name, rgm::rgm_r_fm_status status, const char *status_msg);
void flush_status_change_queue(void);
rglist_p_t get_deepest_delegate(const char *requesting_rg, rgm::lni_t lni = 0,
    LogicalNodeset *toRemove = NULL, rlist_p_t *delegate_r = NULL);
void compute_NRUlst_frm_deplist(rgm_dependencies_t *r_dep_list_ptr,
    namelist_t **nru_list, boolean_t SA_only);
/*
 * rgm_timelist_t utility functions.
 */
int rgm_append_to_timelist(rgm_timelist_t **rgtp);
uint_t rgm_count_timelist(rgm_timelist_t *rgtimep);
void rgm_prune_timelist(rgm_timelist_t **rgtp, time_t prune_tm);

void rgm_fatal_reboot(const char *what, boolean_t reboot_node,
    boolean_t take_dump, const char *zonename);
void abort_node(boolean_t reboot_node, boolean_t take_dump,
    const char *zonename);

/*
 * helpers for inter-rg and restart dependency checks
 */
void check_all_dependencies(void);
void check_blocked_dependencies(rlist_p_t rp, boolean_t check_starting,
    boolean_t check_stopping);
void trigger_restart_dependencies(rlist_p_t rp, LogicalNodeset &ns,
    boolean_t is_off_restart);
void trigger_restart(rlist_p_t rp, LogicalNodeset &ns);
boolean_t is_offline_r_state(rgm::rgm_r_state r_state);
boolean_t are_starting_dependencies_resolved(rlist_p_t rp,
    rdeplist_t *deps_list, deptype_t deps_kind, LogicalNodeset &ns);
boolean_t any_res_starting_depend(const rglist_p_t rgp,
    sol::nodeid_t nodeid);
void get_rg_online_nodeset(rglist_p_t rg_ptr, LogicalNodeset *online_nodes);

// Error message copy utility function.
scha_err_t transfer_message(scha_errmsg_t *res, scha_errmsg_t *temp_res);
/*
 * Zones support
 */
void create_lni_mappings_from_nodeidlist(nodeidlist_t *nodeidlist,
    boolean_t propagate = B_FALSE);
void create_sequence_lni_mappings(sol::nodeid_t nodeid,
    rgm::lni_mappings_seq_t **lni_mappings_out);
#ifndef EUROPA_FARM
void cache_local_nodeid(void);
bool zone_died(rgm::lni_t lni, rgm::ln_incarnation_t ln_incarnation,
    Environment &env);
#else
void cache_local_nodename_nodeid(void);
bool zone_died(rgm::lni_t lni, rgm::ln_incarnation_t ln_incarnation,
    rpcidlretvals *retval);
#endif
void set_booting_state(rgm::lni_t lni);
void run_boot_meths_on_logical_node(rgm::lni_t lni);
bool check_boot_meths_on_rg(rglist_t *rglp, rgm::lni_t lni);
void process_zone_death(rgm::lni_t lniDeadZone);
void reset_rg_per_zone_state(rglist_p_t rgp, rgm::lni_t lniDeadZone);
void remove_zone_pingpong_dir(rgm::lni_t lni);

extern LogicalNodeManager *lnManager;

/*
 * Other utility functions
 */
boolean_t rt_allowed_restart(rgm_rt_t *rt);

/*
 * Utility Functions for inter-cluster dependency and inter cluster rg
 * affinities support
 */
#if SOL_VERSION >= __s10
enum status_type {
	RG_ONLINE_STATUS,
	RG_ERROR_STOP_FAILED_STATUS,
	RG_SWITCH_STATUS
};
scha_errmsg_t get_intercluster_rdep_lists(rgm_dependencies_t r_deps,
    namelist_t **strong, namelist_t **weak, namelist_t **restart,
    namelist_t **offline_restart);
scha_errmsg_t get_remote_resource_name(const char *res_name,  char **rr);
scha_errmsg_t get_remote_cluster_name(const char *res_name, char **rc);
scha_errmsg_t get_remote_cluster_and_rorrg_name(const char *res_name,
    char **rc, char **rr);
boolean_t is_this_local_cluster_r_or_rg(const char *rname, boolean_t rgflag);
boolean_t is_rg_local_cluster_rg(const char *rname);
boolean_t get_remote_r_state_helper(rlist_p_t rp, name_t r_name,
    rdep_locality_t loc_type, deptype_t deps_kind, LogicalNodeset &nset,
    boolean_t start_dep_check, boolean_t *is_invalid_rs);
static scha_errmsg_t configure_icrd(rdeplist_t *dep_list,
    rgm_resource_t *resource, deptype_t dep);
static scha_errmsg_t update_icrd(rgm_dependencies_t dep_list,
    rgm_resource_t *resource, deptype_t dep);
scha_errmsg_t process_intercluster_resource_lists(namelist_t **orig_list,
    namelist_t **new_list, rgm_resource_t *resource, deptype_t dep);
scha_errmsg_t update_dependent_info(char *r_name, boolean_t dep_set_flag,
    char *rname, deptype_t dep);
void nodeidlist_to_nodeidseq(nodeidlist_t *nidlst, rgm::lni_seq_t &nodeidseq);
scha_errmsg_t get_aff_list_helper(namelist *aff_list, namelist **res_list);
scha_errmsg_t get_intercluster_rg_aff_lists(rgm_affinities_t aff,
    namelist_t **spd, namelist_t **sp, namelist_t **wp, namelist_t **sn,
    namelist_t **wn);
static scha_errmsg_t process_intercluster_rg_lists(namelist_t **orig_list,
    namelist_t **new_list, rgm_rg_t *rg,  boolean_t rg_create,
    boolean_t affs_changed, boolean_t des_prims_changed,
    boolean_t nodelist_changed, rgm::rg_aff_flag_t aff_flag);
boolean_t get_remote_rg_status(char *rg_name,  sol::nodeid_t nodeid,
    status_type status);
void cleanup_r_rg(const char *r_rg_name, const char *dep_aff_name,
    boolean_t rg_flag, deptype_t deps);
scha_err_t handle_inter_cluster_rg_affinities(const char *rg_name,
    name_t target_rg_name, rgm::remote_rg_status_flag_t status,
    boolean_t *is_changed, rgm::lni_t mynodeid,
    boolean_t ignore_stop_failed, boolean_t *cleanup_done_flag,
    boolean_t cleanup_stale_affs);

void initialize_zone_callbacks(void);
boolean_t dep_tag_exists_in_nameserver();
void cyclic_depchk_clear_nameserver();
boolean_t dep_tag_created_by_this_zc();
void cleanup_dep_info(const char *r_rg_name, const char *dep_aff_name,
    deptype_t deps, rgm::idl_nameval_seq_t  prop_val);
void cleanup_aff_info(const char *r_rg_name, const char *dep_aff_name,
    rgm::idl_nameval_seq_t  prop_val);
#endif

scha_errmsg_t get_rdeplist_helper(rdeplist_t *deps_list, namelist **res_list);
boolean_t is_rg_offlining(rgm::rgm_rg_state rg_state);
rgm::r_restart_t get_r_restarting(name_t r_name,
    rgm::rgm_rg_state rg_state, rgm::lni_t lni);
boolean_t allow_inter_cluster(void);
scha_err_t force_offline(const char *rg_name, const namelist_t *rg_names,
    rgm::lni_t mynodeid, boolean_t ignore_stop_failed);
boolean_t is_enhanced_naming_format_used(const char *rname);

/*
 * rgmd registers for CCR infrastructure table callbacks.
 * When CCR delivers callback to rgmd on infrastructure table changes,
 * rgmd services this callback in a dedicated thread (infr_table_cb_thread).
 * This function starts this dedicated thread (infr_table_cb_thread).
 */
void start_infr_table_cb_thread();

/*
 * Reconfiguration steps for rgmd (using rgm reconfig thread in process
 * membership support)
 */
#if (SOL_VERSION >= __s10)
bool rgm_reconfiguration_step1(
    const cmm::membership_t membership, const cmm::seqnum_t seqnum);
bool rgm_reconfiguration_step2(
    const cmm::membership_t membership, const cmm::seqnum_t seqnum);
#endif

/*
 * Rolling upgrade support
 */
extern boolean_t rgm_ru_transition_in_progress;
boolean_t is_rgm_zc_version(void);
boolean_t process_membership_support_available(void);
boolean_t use_ucmm_membership(void);
void setup_membership(bool setup_process_membership);
#if (SOL_VERSION >= __s10)
boolean_t membership_api_available_for_rgmd_starter(void);
void rgm_initialize_versioning(void);
void rgm_starter_initialize_versioning(void);
/* Callback function prototype invoked by the upgrade callback */
typedef void (*upgrade_callback_funcp_t)(const char *ucc_name,
    const version_manager::vp_version_t &new_version);
void rgm_starter_upgrade(void);
#endif

scha_errmsg_t
check_rg_affinity_type(const rglist_p_t affinity_rg, const char *affinitent_rg,
    const char *affinitent_cluster, const affinitytype_t aff_type,
    boolean_t *affinity_exists);
scha_errmsg_t
rgm_add_cluster_scope_to_obj_name(const char *objname,
    const char *clustername, char **fullname);
scha_errmsg_t
rgm_set_ok_to_start_for_all_rg();

#ifdef __cplusplus
}
#endif

#endif /* _RGM_PROTO_H */
