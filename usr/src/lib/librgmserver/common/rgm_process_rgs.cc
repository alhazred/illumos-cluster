//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_process_rgs.cc	1.101	09/02/19 SMI"

//
// This module contains routines executed by the President
// during reconfiguration to process RGs.
//

#include <rgm_state.h>
#include <rgm_proto.h>
#include <scadmin/scconf.h>
#include <rgm/rgm_msg.h>
#include <rgm/rgm_errmsg.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <sys/cl_events.h>
#include <syslog.h>

static boolean_t dead_or_joining(rgm::lni_t lni, const LogicalNodeset &join_ns);

static scha_errmsg_t check_node_candidacy(candidate_t &cand, rglist_p_t rglp,
    rgm::lni_t nodeid, boolean_t check_for_failures,  boolean_t switch_back);
static boolean_t bust_switches(namelist_t *rgs);
static boolean_t was_rg_onish(rglist_p_t rgp, rgm::lni_t lni);


//
// dead_or_joining
//
// Must be called with the state lock held.
//
// Only called by OLD President, who would know on which dead nodes
// an RG had been previously mastered.
//
// Returns true if the given lni is not in the current cluster membership;
// or if its physical node is joining, as indicated by it being
// contained in join_ns.
//
// Note, we do not have to distinguish nodes which remained dead from
// nodes which just died.  The president will check the RG state on
// the node, and will only rebalance if the state is (i.e., was) online
// and the node is dead or joining.
//
static boolean_t
dead_or_joining(rgm::lni_t lni, const LogicalNodeset &join_ns)
{
	if (!in_current_logical_membership(lni)) {
		return (B_TRUE);
	}

	//
	// It's possible that the lni could be a non-global zone in one of
	// the joiners. Check the physical node in the join_ns.
	// Above call to in_current_logical_membership() already checked
	// for validity of ln.
	//
	LogicalNode *ln = lnManager->findObject(lni);

	if (join_ns.containLni(ln->getNodeid())) {
		return (B_TRUE);
	} else {
		return (B_FALSE);
	}
}


//
// was_rg_onish
//
// Returns TRUE if the state of the specified RG on the specified node had
// been in any of the "on-ish" states; otherwise returns FALSE.
// The lock is already held.
// Called by an old President for a dead or dead and quickly rejoined node
// in order to determine whether the RG needs to be rebalanced.
//
// If the RG was PENDING_OFF_STOP_FAILED or ERROR_STOP_FAILED on the node
// that died, we consider this
// an "on-ish" state. The death of the node effectively clears the
// stop-failed condition, which may now allow the RG to be brought online.
// Therefore, we do want to run rebalance in that case.
//
static boolean_t
was_rg_onish(rglist_p_t rgp, rgm::lni_t n)
{
	//
	// This static function was superseded by an external function,
	// except that we need to count RG_PENDING_OFFLINE as well.
	//
	return ((boolean_t)(is_rg_online_on_node(rgp, n, B_TRUE) ||
	    rgp->rgl_xstate[n].rgx_state == rgm::RG_PENDING_OFFLINE));
}


//
// Return true if RG is avaliable or will be available
// to clients. This function is different from the
// was_rg_onish() function in that, we dont consider certain
// states where the RG state is going offline or in
// a failed state.
//
boolean_t
was_rg_available(rglist_p_t rgp, rgm::lni_t n)
{
	switch (rgp->rgl_xstate[n].rgx_state) {

	// "on-ish" states
	case rgm::RG_ONLINE:
	case rgm::RG_ON_PENDING_R_RESTART:
	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
	case rgm::RG_ON_PENDING_METHODS:
		return (B_TRUE);

	// "off-ish" states
	case rgm::RG_OFFLINE:
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_BOOT:
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_OFFLINE_START_FAILED:
	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_PENDING_OFF_START_FAILED:
	default:
		return (B_FALSE);
	}
}


//
// reset_rg_offline
//
// Resets the president's in-memory state for an RG on a node or zone that died,
// or died and quickly rejoined.  Sets state of RG and all R's to OFFLINE.
// The lock is already held.
//
// Removes zone n from the RG's rgl_evac_nodes nodeset, because once the
// zone has died we are no longer evacuating from it.  Also resets
// the RG's rgl_switching flag if it is no longer evacuating from any node.
//
// Wrap the set_rgx_state call by setting/clearing the no_rebalance flag
// on the RG.  That is because we don't want to run rebalance on this RG
// that we are just resetting to OFFLINE.  Rebalance will get run later
// by the caller, if needed.  Or, failback might get run.  In either case,
// we don't need to run rebalance now.
//
// If trigger_offline_restart_deps is true, triggers offline-restart
// dependencies for all resources that are being set offline.  Normally, this
// triggering would be done by set_rx_state() when the resource transitions
// directly from online to offline.  However, a newly elected president,
// taking over when the previous president dies, does not know if the RG
// was previously online on the dead node or not; the resource state in the
// new president's memory most likely is already offline when this function is
// called.  It is OK to trigger offline-restart dependencies even if the
// resource wasn't online on the node that died, because the triggering of
// offline-restart dependencies is idempotent; it will have no effect on
// dependent resources that are already offline, or whose offline-restart
// dependency is still satisfied on another node.
//
// xxxx Bugid 4215776 (Implement FYI states in rgmd):
// Even if we do implement FYI states, this function need not set
// method-failed FYI states if any init, fini, boot, or update methods were
// pending on the node n that died.  The reasoning is as follows:  If INIT or
// BOOT was going to run when the node died, that will be taken care of by
// BOOT when the node reboots.  If UPDATE was going to run when the node
// dies, we need not remember that because UPDATE is only applicable to
// resources that are online.  If FINI was going to run when the node died,
// that is already treated as FINI failure by the code that deals with
// unmanaging the RG or deleting a resource.
// So, we would only have to set "method failed" FYI states for the case that
// the method actually times out or exits non-zero.
//
void
reset_rg_offline(rglist_p_t rgp, rgm::lni_t n,
    boolean_t trigger_offline_restart_deps)
{
	rlist_p_t	rp;

	//
	// First set the resources in the RG to OFFLINE.  We need to do this
	// first, because set_rgx_state() might call reset_rg_per_zone_state()
	// (if the zone or node has died), which erases the resource state
	// without calling set_rx_state(), hence without triggering the
	// desired side effects of set_rx_state: for example, generating syslog
	// messages and events for resources that go offline; and triggering
	// offline-restart dependencies.
	//
	for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		// Reset the resource state and status
		set_rx_state(n, rp, rgm::R_OFFLINE,
		    trigger_offline_restart_deps);
		reset_fm_status(n, rp, rgm::R_FM_OFFLINE, RGM_DIR_NULL);
	}

	// Reset the RG state to OFFLINE
	rgp->rgl_no_rebalance = B_TRUE;
	set_rgx_state(n, rgp, rgm::RG_OFFLINE);
	rgp->rgl_no_rebalance = B_FALSE;
	rgp->rgl_delayed_rebalance = B_FALSE;

	//
	// Clear the node evacuation flag bit for the RG on node n;
	// if necessary, reset the rgl_switching flag.
	//
	rgp->rgl_evac_nodes.delLni(n);
	if (rgp->rgl_evac_nodes.isEmpty() &&
	    rgp->rgl_switching == SW_EVACUATING) {
		rgp->rgl_switching = SW_NONE;
	}
}


//
// candidate_compare
// -------------------
// Used as qsort comparison function by sort_candidate_nodes.
// Compares two candidate nodes to see which one is more preferred as
// a potential new master of the RG.
//
// The criteria used, in priority order, is:
//    E, greater is better
//    OW, greater is better
//    V, smaller is better
//    start-failure count, smaller is better
//    Recent start-failures, earlier is better
//    IW, greater is better
//    ordinal position of node in Nodelist, smaller is better
//
static int
candidate_compare(candidate_t *c1, candidate_t *c2)
{
	//
	// If c1 has a higher E, it comes first.
	//
	if (c1->E > c2->E) {
		return (-1);
	}
	if (c1->E < c2->E) {
		return (1);
	}

	//
	// If c1 has a higher OW, it comes first.
	//
	if (c1->OW > c2->OW) {
		return (-1);
	}
	if (c1->OW < c2->OW) {
		return (1);
	}

	//
	// Outgoing weak affinities are equal.
	// We want fewer strong violations.
	//
	if (c1->V > c2->V) {
		return (1);
	}
	if (c1->V < c2->V) {
		return (-1);
	}

	//
	// Outgoing weak affinities and strong violations are equal.
	// We want fewer recent start failures.
	//
	if (c1->start_fail > c2->start_fail) {
		return (1);
	}
	if (c1->start_fail < c2->start_fail) {
		return (-1);
	}

	//
	// If the number of recent start failures are same then we
	// prefer the one with the earliest start failure.
	//
	if (c1->recent_fail > c2->recent_fail) {
		return (1);
	}
	if (c1->recent_fail < c2->recent_fail) {
		return (-1);
	}

	//
	// If c1 has a higher IW, it comes first.
	//
	if (c1->IW > c2->IW) {
		return (-1);
	}
	if (c1->IW < c2->IW) {
		return (1);
	}

	//
	// Everything else is equal: go in nodelist order.
	//
	if (c1->pref > c2->pref) {
		return (1);
	}
	if (c1->pref < c2->pref) {
		return (-1);
	}

	//
	// Sanity check: We should not get here because the nodes must occupy
	// different positions in the RG's nodelist.
	//
	ucmm_print("candidate_compare",
	    NOGET("ERROR: LNIs <%d> and <%d> both "
	    "occupy position <%d> in nodelist.\n"),
	    c1->lni, c2->lni, c1->pref);

	return (0);
}

void
free_candidate_list(candidate_t cands[], uint_t num_cands)
{
	uint_t i;
	for (i = 0; i < num_cands; i++) {
		rgm_free_nlist(cands[i].tclist);
		cands[i].tclist = NULL;
	}
}


//
// process_rg
//
// A new President rebalances all RGs because it has no knowledge of the
// switching state of RGs on other nodes, and because it refrained from
// calling rebalance as it fetched the state from all the slaves.
//
// An old President determines whether the specified RG was affected by a
// node or zone death, i.e., if the RG was online (or switching online)
// on the dead node or zone.  For a physical node death, the old President
// resets the state of the RG and its resources (in president's memory) on
// the dead node and all of its zones.  However if only a non-global zone
// died while its physical node remained up (as indicated by the
// zone_death_only parameter), then do not reset RG state --
// this is handled by process_zone_death() on the slave side.
//
// For node or zone death, the old president busts any switchovers, updates
// or failbacks in progress. For a detailed explanation of "busting", see
// the header comment for process_needy_rg().
//
// In case the RG needs to be brought online somewhere, this function sets
// the rgl_delayed_rebalance flag on it. It can't call rebalance directly,
// because the state of the other RGs on the zone(s) that died have not
// yet been updated.  For more info about rebalance after a zone death,
// see the header comment of process_rgs_on_death().
//
// This is called by the President during ucmm or zone reconfiguration.
// The state machine mutex is already held.
//
// This function must be called after the membership status is updated for
// nodes or zones that died or joined.
//
// join_ns is a nodeset of physical nodes which are joining the cluster
// membership.
//
// If zone_death_only is true, then we are processing the death of a zone
// on a physical node which is still in the cluster membership.
//
void
process_rg(rglist_p_t rglp, boolean_t is_new_president,
    const LogicalNodeset &join_ns, boolean_t zone_death_only)
{
	rgm::lni_t n;
	boolean_t rg_needs_rebalance = B_FALSE;
	boolean_t was_rg_onish_flag;
	LogicalNodeset current_online_ns;
	LogicalNodeset temp_nodeset;
	LogicalNodeset to_nodeset;
	LogicalNodeset *rg_nodeset = NULL;

	ucmm_print("process_rg()", NOGET("<%s>\n"), rglp->rgl_ccr->rg_name);
	// Note: we aren't called for unmanaged RGs, so we don't check here

	//
	// If we are a new president, just rebalance the RG.
	// New president has already reset RG
	// states to offline on dead nodes; and since new president cannot be
	// running any switches or updates, none need to be busted.
	//
	if (is_new_president) {
		//
		// Set the rgl_delayed_rebalance flag instead
		// of actually calling rebalance, as described above.
		//
		ucmm_print("process_rg()", NOGET(
		    "new pres:set delayed_rebal\n"));
		rglp->rgl_delayed_rebalance = B_TRUE;
		return;
	}

	//
	// We are an old president.
	//

	//
	// First translate the RG's nodelist to a nodeset. We do this here
	// for efficiency instead of each time we are searching for a node
	// in the loops below.
	//
	rg_nodeset = get_logical_nodeset_from_nodelist(
		rglp->rgl_ccr->rg_nodelist);

	//
	// The following for loop is used exclusively to find the
	// current online nodeset for the RG. We cannot use the next for loop
	// because the RG state is reset for every node and that
	// generates events even before we can post the node death
	// event. Here we get a set of LNIs where the RG was/is ONLINE.
	// We need the current online set to post the event.
	//
	n = 0;
	while ((n = rg_nodeset->nextLniSet(n + 1)) != 0) {
		//
		// check if the RG was available on this node
		// We need this to post the event.
		//
		if (was_rg_available(rglp, n)) {
			// Add this node to the current masters
			// nodeset.
			current_online_ns.addLni(n);
		}
	}

	//
	// Iterate through all zones in the nodelist to process node deaths.
	//
	n = 0;
	while ((n = rg_nodeset->nextLniSet(n + 1)) != 0) {
		//
		// The old President knows whether the RG had
		// been mastered on a zone that just died.
		// If the zone didn't die, skip to the next zone.
		//
		if (!dead_or_joining(n, join_ns)) {
			continue;
		}

		//
		// This node or zone is dead or is joining.
		//
		// If a switch is in progress on the RG, check if zone
		// that died is contained in the planned_on nodeset of
		// the RG. If so, bust the switch and set the
		// delayed_rebalance flag on the RG.  If not, then we
		// take no action, because the do_rg_switch() should be
		// able to complete its work on the planned_on nodes
		// which are still cluster members.
		//
		// Determine whether the RG had been "on-ish" on the zone.
		// Note that if the RG was "on-ish" on a joining node or zone,
		// this implies that the node or zone died and quickly rejoined
		// the cluster, so we treat it the same as a node death.
		//
		// If the RG had been in an "on-ish" state on that zone, the
		// following actions are taken:
		//  - if an update is in-progress on the RG and it was in
		//	"on-ish" state, then it has lost a master.  Bust the
		//	update, but do not call rebalance(); the updating thread
		//	will call rebalance() after the update has finished or
		//	aborted.
		//  - if neither a switchover nor an update is in progress on
		//	the RG and it is not already busted, set the
		//	delayed_rebalance flag on the RG.
		//
		// Setting the delayed_rebalance flag on the RG will cause
		// rebalance() to be called later (after all RGs are processed)
		// to assign new master(s) to the RG.
		//

		// Create a nodeset of nodes that are dead or joining.
		temp_nodeset.addLni(n);

		was_rg_onish_flag = was_rg_onish(rglp, n);
		//
		// If we are processing a cmm reconfiguration (and not just
		// a zone death), reset the state of the RG and its
		// resources on this node or zone to offline (in president's
		// memory).
		//
		if (!zone_death_only) {
			reset_rg_offline(rglp, n, B_FALSE);
		}

		//
		// We bust a switchover or failback in progress
		// if the current zone is a member of the planned_on
		// nodeset of the RG.  Then we will rebalance the RG.
		// This is done irrespective of whether or not the RG
		// was previously online on that node.
		//
		// Otherwise, we take no action and allow the switchover
		// to proceed.
		//
		// Note that node/zone evacuations (scswitch -S) are
		// not busted.
		//
		if ((rglp->rgl_switching == SW_IN_PROGRESS ||
		    rglp->rgl_switching == SW_IN_FAILBACK) &&
		    rglp->rgl_planned_on_nodes.containLni(n)) {
			rglp->rgl_switching = SW_BUSTED_RECONF;
			ucmm_print("process_rg()", "switch BUSTED!!\n");
			rg_needs_rebalance = B_TRUE;
			// continue looping to process all zone deaths
			continue;
		}

		if (was_rg_onish_flag) {
			//
			// If update is in progress, bust it but do not
			// rebalance the RG.  The update thread will rebalance.
			//
			if (rglp->rgl_switching == SW_UPDATING) {
				ucmm_print("process_rg()", "update BUSTED!!\n");
				// rg_needs_rebalance is left unchanged
				rglp->rgl_switching = SW_UPDATE_BUSTED;
				// continue looping to process all zone deaths
				continue;
			}

			//
			// If no switch or update is in progress,
			// we will rebalance this RG.  If the rg is being
			// evacuated, rebalance itself will exclude
			// evacuating zones, so we do the rebalance.
			//
			if (rglp->rgl_switching == SW_NONE ||
			    rglp->rgl_switching == SW_EVACUATING) {
				rg_needs_rebalance = B_TRUE;
				// continue looping to process all zone deaths
				continue;
			}
		}
	}

	//
	// Get the LogicalNodeset where this RG will not be available.
	// Subtract the zones that died to get the to_nodeset.
	//

	to_nodeset = current_online_ns - temp_nodeset;

	if (!current_online_ns.isEqual(to_nodeset)) {

		// Some zone/zones died. Post an event.

		(void) post_primaries_changing_event(NULL,
		    rglp->rgl_ccr->rg_name,
		    CL_REASON_RG_NODE_DIED, NULL,
		    rglp->rgl_ccr->rg_desired_primaries,
		    current_online_ns, to_nodeset);
	}

	if (rg_needs_rebalance) {
		//
		// Set the rgl_delayed_rebalance flag instead
		// of actually calling rebalance, as described above.
		//
		ucmm_print("process_rg()", "set delayed_rebal\n");
		rglp->rgl_delayed_rebalance = B_TRUE;
	}
	delete (rg_nodeset);
}


static void
dump_candidate_list(char *message, size_t count, candidate_t *list)
{
	size_t i;

	for (i = 0; i < count; i++) {
		ucmm_print("dump_candidate_list",
		    NOGET("%s Candidate <%d>: lni <%d>, OW <%d>, V <%d>, "
		    "start_failed <%d>, IW <%d>, pref <%d>\n"),
		    message, i, list[i].lni, list[i].OW, list[i].V,
		    list[i].start_fail, list[i].IW, list[i].pref);
	}
}

//
// delayed_rebalance
// ------------------
// Calls rebalance on rgs that have their rgl_delayed_rebalance flag set.
// Sorts the RGs by affinities and RG dependencies.
//
void
delayed_rebalance(LogicalNodeset &joiners, boolean_t is_new_president)
{
	rglist_p_t rglp;
	rglist_p_t *rglistp = NULL; // null-terminated array of rglist_t ptrs
	uint_t numrgs = 0;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		ucmm_print("RGM", NOGET("Checking for delayed rebalance on "
		    "<%s>\n"), rglp->rgl_ccr->rg_name);

		// Skip over RGs in unmanaged state

		if (rglp->rgl_ccr->rg_unmanaged) {
			ucmm_print("RGM", NOGET("Skipping UNMANAGED RG <%s>\n"),
			    rglp->rgl_ccr->rg_name);
			continue;
		}

		if (rglp->rgl_delayed_rebalance) {
			rglp->rgl_delayed_rebalance = B_FALSE;

			//
			// add rglp to rglistp
			//
			res = rglist_add_rg(rglistp, rglp, numrgs++);
			if (res.err_code != SCHA_ERR_NOERR) {
				//
				// We ran out of swap space, not having called
				// the interposed malloc.  Reboot the node.
				//
				rgm_fatal_reboot("delayed_rebalance",
				    B_TRUE, B_FALSE, NULL);
			}
		}
	}
	if (rglistp != NULL) {
		ucmm_print("delayed_rebalance", "calling rebalance rgs");
		rebalance_rgs(rglistp, B_TRUE, joiners, is_new_president,
		    B_TRUE);
		free(rglistp);
	}
}


//
// rebalance_rgs
//
// The input argument rglistp is a NULL-terminated array of rglist_p_t.
// The caller guarantees rglistp to be non-NULL.
//
// Sort the elements of the list such that no RG has an RG_dependency or
// stong affinity for any RG that follows it in the list.
// Then call rebalance() on each RG in the sorted list.
// Note that the elements of rglistp are modified (reordered) by this function.
//
// The caller should make sure that all of the RGs in rglistp are managed.
// Though if any were not, rebalance would be a no-op and no real harm
// would be done.
//
// Called on the president, while holding the state lock.
//
// Reboot the node if a NOMEM error is encountered.
//
void
rebalance_rgs(rglist_p_t *rglistp, boolean_t check_for_failures,
    LogicalNodeset &joiners, boolean_t is_new_president,
    boolean_t check_for_rg_suspended, boolean_t set_planned_on_nodes)
{
	uint_t i;

	CL_PANIC(rglistp != NULL);

	// Sort RGs by rg_dependencies & strong affinities
	scha_errmsg_t res = switch_order_rgarray(rglistp);
	if (res.err_code != SCHA_ERR_NOERR) {
		// This error occurs because of low memory.
		// Reboot the node
		rgm_fatal_reboot("rebalance_rgs", B_TRUE, B_FALSE, NULL);
	}

	// rebalance the RGs in the sorted order
	for (i = 0; rglistp[i] != NULL; i++) {
		ucmm_print("rebalance_rgs", "calling rebalance on :%p:",
		    rglistp[i]);

		rebalance(rglistp[i], joiners, is_new_president,
		    check_for_failures, check_for_rg_suspended,
		    set_planned_on_nodes);
	}
}


//
// rebalance
// Bring an RG online to satisfy Desired_primaries without violating
// Max_primaries.  Do not take the RG offline from any nodes even if
// it exceeds Desired_primaries.
//
// The caller supplies a non-empty nodeset (the set of joining nodes)
// if it wants rebalance() to ignore joining nodes.  process_rg() needs
// to ignore joining nodes because node death must be processed before
// node join.
//
// The new_pres parameter is set true only if this node is a new president
// and is calling rebalance from process_rg() from rgm_reconfig().  The policy
// is that since the new_president cannot tell which RGs lost a master when
// the previous president died, it calls rebalance() on *all* RGs.  If
// this parameter is set true, then we might find that the RG is in the
// ON_PENDING_METHODS or OFF_PENDING_METHODS state on a slave because the
// old president was performing an editing operation on the RG when it died.
// In this case, we set the rgl_new_pres_rebalance flag on the RG and return
// without actually rebalancing the RG.  We will be called again each time a
// slave moves the RG out of the PENDING_METHODS state.  When the RG has moved
// out of the pending state on all nodes then we will actually perform the
// rebalance and clear the rgl_new_pres_rebalance flag.
//
// rebalance() will return without side-effects if a switch,
// update or failback is in progress on the RG.
// Note that rebalance can be called with RG in non-endish state if a
// switch was busted or (as noted above) if the former president died.
// Nodes from which the RG is evacuating are excluded from node candidacy.
//
void
rebalance(rglist_p_t rglp, LogicalNodeset &ignore_join_ns, boolean_t new_pres,
    boolean_t check_for_failures, boolean_t check_for_rg_suspended,
    boolean_t set_planned_on_nodes)
{
	int num_online = 0;		// ONLINE or PENDING_ONLINE
	int num_primaries = 0;		// num_online + PENDING_OFFLINE
	sol::nodeid_t nodeid;
	sc_syslog_msg_handle_t handle;
	uint_t i;

	size_t ncand = 0;		// Number of candidates
	candidate_t *c_nodes = NULL;

	namelist_t on;
	LogicalNodeset from_nodeset;
	LogicalNodeset to_nodeset;
	boolean_t need_cond_broadcast = B_FALSE;
	LogicalNodeset emptyNodeset;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	//
	// If the ok_to_start flag is not set, just return. RG will be
	// rebalanced later once it has been manually switched online.
	//
	ucmm_print("rebalance", "entry, rg:%s", rglp->rgl_ccr->rg_name);
	if (!rglp->rgl_ok_to_start) {
		ucmm_print("rebalance", "return because ok_to_start is false");
		return;
	}

	if (check_for_rg_suspended && rglp->rgl_ccr->rg_suspended) {
		ucmm_print("RGM", NOGET("rebalance: RG <%s> is suspended"),
		    rglp->rgl_ccr->rg_name);
		return;
	}

	if (rglp->rgl_ccr->rg_desired_primaries == 0) {
		ucmm_print("RGM", NOGET("rebalance: rg_desired_primaries "
		    "for RG <%s> is set to zero"),
		    rglp->rgl_ccr->rg_name);
		return;
	}

	//
	// If we are quiescing we will return early.
	//
	if (rglp->rgl_quiescing) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The indicated resource was quiesced as a result of
		// clresourcegroup quiesce or scswitch -Q being executed on a
		// node.
		// @user_action
		// Use clresourcegroup status to determine the state of the
		// resource group. If the resource group is in
		// ERROR_STOP_FAILED state on a node, you must manually kill
		// the resource and its monitor, and clear the error
		// condition, before the resource group can be started. Refer
		// to the procedure for clearing the ERROR_STOP_FAILED
		// condition on a resource group in the Sun Cluster
		// Administration Guide.
		//
		// If the resource group is in ONLINE_FAULTED or ONLINE state
		// then you can switch it offline or restart it. If the
		// resource group is OFFLINE on all nodes then you can switch
		// it online.
		//
		// If the resource group is in a non-quiescent state such as
		// PENDING_OFFLINE or PENDING_ONLINE, this indicates that
		// another event, such as the death of a node, occurred during
		// or immediately after execution of the clresourcegroup
		// quiesce command. In this case, you can re-execute the
		// clresourcegroup quiesce command to quiesce the resource
		// group.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "rebalance: resource group <%s> is quiescing."),
		    rglp->rgl_ccr->rg_name);
		sc_syslog_msg_done(&handle);
		return;
	}

	//
	// If everybody is a joiner, and we're ignoring joiners, return.
	// RG will be rebalanced later when we process joiners.
	//
	LogicalNodeset *currMembership = lnManager->get_all_logical_nodes(
		rgm::LN_SHUTTING_DOWN);
	if (ignore_join_ns.isEqual(*currMembership)) {
		// we're processing node death at this time, so return
		return;
	}
	delete currMembership;

	//
	// Make sure the RG is not busy; if so, rebalance() is a no-op.  If the
	// RG is in process of an scswitch -S node evacuation, we do not exit
	// here, but later we will exclude nodes on which the RG is currently
	// being evacuated and nodes that are subject to an unexpired sticky
	// evacuation timeout.
	//
	if (rglp->rgl_switching == SW_IN_PROGRESS ||
	    rglp->rgl_switching == SW_UPDATING ||
	    rglp->rgl_switching == SW_IN_FAILBACK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The indicated resource group has lost a master due to a
		// node death. However, the RGM is unable to switch the
		// resource group to a new master because the resource group
		// is currently in the process of being modified by an
		// operator action, or is currently in the process of "failing
		// back" onto a node that recently joined the cluster.
		// @user_action
		// Use cluster status -t <resourcegroup> to determine the
		// current mastery of the resource group. If necessary, use
		// clresourcegroup switch to switch the resource group online
		// on desired nodes.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "rebalance: resource group <%s> is being switched "
		    "updated or failed back, cannot assign new primaries"),
		    rglp->rgl_ccr->rg_name);
		sc_syslog_msg_done(&handle);
		return;
	}

	//
	// rebalance algorithm:
	//
	// First, call sort_candidate_nodes to count the number of nodes
	// (num_online) on which the RG is either ONLINE or PENDING_ONLINE;
	// to compute num_primaries, which includes num_online plus nodes
	// on which the RG is PENDING_OFFLINE; and to create a sorted list of
	// candidate nodes.
	//
	// If num_online equals or exceeds Desired_primaries, we are done.
	//
	// Next, take one pass thru the sorted candidate list.
	// If the RG is offline, we call change_mastery to take it online
	// and increment both num_online and num_primaries.
	// If the RG is pending_offline, we call change_mastery to take it
	// online (i.e., change direction) and increment only num_online.
	//
	// When num_online is >= Desired_primaries or
	// num_primaries == Max_primaries or we have exhausted the
	// candidate list, then we are done.
	//

	ucmm_print("RGM", NOGET("rebalance <%s>\n"), rglp->rgl_ccr->rg_name);

	res = sort_candidate_nodes(emptyNodeset, rglp, c_nodes, ncand,
	    ignore_join_ns, num_online, from_nodeset, num_primaries,
	    new_pres, B_TRUE, B_FALSE, B_FALSE, check_for_failures);

	if (res.err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_NOMEM) {
			// reboot the node
			rgm_fatal_reboot("rebalance", B_TRUE, B_FALSE, NULL);
		}
		ucmm_print("RGM", NOGET("rebalance <%s>: exiting because "
		    "sort_candidate_nodes returned error code"),
		    rglp->rgl_ccr->rg_name);
		return;
	}

	// save the num_online and num_primaries in temp variables.
	int orig_num_online = num_online;
	int orig_num_primaries = num_primaries;

	//
	// If RG had been in PENDING_METHODS state on any node,
	// sort_candidate_nodes would have returned an error.  So at this
	// point we can safely clear the rgl_new_pres_rebalance flag on the RG.
	//
	// Also clear the rgl_no_rebalance and rgl_no_triggered_rebalance
	// flags.  Since we're actually running rebalance, we know it's safe
	// to do so, and we want to make sure that process_needy_rg would
	// call rebalance later if needed.
	//
	rglp->rgl_new_pres_rebalance = B_FALSE;
	rglp->rgl_no_rebalance = B_FALSE;
	rglp->rgl_no_triggered_rebalance = B_FALSE;

	//
	// sanity checks
	//
	CL_PANIC(rglp->rgl_ccr->rg_desired_primaries <=
	    rglp->rgl_ccr->rg_max_primaries);
	CL_PANIC(num_primaries <= rglp->rgl_ccr->rg_max_primaries);

	if (num_online >= rglp->rgl_ccr->rg_desired_primaries) {
		ucmm_print("RGM",
		    NOGET("have enough primaries for <%s>\n"),
			rglp->rgl_ccr->rg_name);

	} else {

		//
		// from_nodeset contains the nodes where the RG is currently
		// ONLINE. Set to_nodeset to from_nodeset initially. Inside
		// the following for loop if we find a eligible node add that
		// nodeid to the to_nodeset.This loop computes the nodeset
		// for generating the primaries-changing event.
		//
		to_nodeset = from_nodeset;

		for (i = 0; i < ncand; i++) {

			ucmm_print("RGM",
			    NOGET("rebalance num_online <%d>, desired <%d>\n"),
			    num_online, rglp->rgl_ccr->rg_desired_primaries);
			//
			// do not process further if the desired primaries for
			// this RG is satisfied.
			//
			if (num_online >= rglp->rgl_ccr->rg_desired_primaries) {
				break;
			}


			//
			// Note, c_nodes was initialized above
			// (first ncand entries)
			//
			nodeid = c_nodes[i].lni;	/*lint !e644 */

			ucmm_print("rebalance",
			    NOGET("compute to_nodeset for RG <%s>, "
			    "nodeid <%d>, state <%s>\n"),
			    rglp->rgl_ccr->rg_name, nodeid,
			    pr_rgstate(rglp->rgl_xstate[nodeid].rgx_state));

			// Note, we already omitted nodes not in current
			// membership

			switch (rglp->rgl_xstate[nodeid].rgx_state) {

			case rgm::RG_OFFLINE:

				//
				// If the node is currently OFFLINE and we
				// switch it to PENDING_ONLINE, we will incr
				// num_primaries.  So, we have to check that we
				// don't exceed Maximum_primaries for the RG.
				//

				//
				// If num_primaries already equals
				// Maximum_primaries, the RG must be
				// PENDING_OFFLINE on one or more nodes.
				// We can't bring it online
				// on a new node until it's gone completely
				// OFFLINE on the old node, because that would
				// violate maximum_primaries.
				// Hopefully, we will be able to
				// reverse the direction of the RG on the
				// node(s) in question later.
				// But if not, when the RG goes
				// OFFLINE it will trigger another call to
				// rebalance which will then allow us to bring
				// it online on a (possibly) more preferred
				// node.
				//
				if (num_primaries >=
				    rglp->rgl_ccr->rg_max_primaries) {
					continue;
				}

				//
				//  we assume that
				// the RG will go online on the slave.
				// Increment num_primaries because a node on
				// which the RG is OFFLINE will be
				// PENDING_ONLINE.
				//
				ucmm_print("RGM",
				    NOGET("rebalance inc num_prim\n"));
				num_primaries++;
				// FALLTHRU

			case rgm::RG_PENDING_OFFLINE:

				//
				// If the node is PENDING_OFFLINE and we switch
				// it to PENDING_ONLINE, we will *not* incr
				// num_primaries (see definition of
				// num_primaries above).
				// Therefore, we do not have to check
				// whether Maximum_primaries is exceeded.

				// Add this nodeset to the to_nodeset list
				// to be used to generate an event.
				to_nodeset.addLni(nodeid);

				// increment # online/pending_online
				ucmm_print("RGM",
				    NOGET("YYYY inc num_online\n"));
				num_online++;

				break;

			case rgm::RG_OFF_BOOTED:
			case rgm::RG_OFFLINE_START_FAILED:
			case rgm::RG_ONLINE:
			case rgm::RG_PENDING_ONLINE_BLOCKED:
			case rgm::RG_ON_PENDING_R_RESTART:
			case rgm::RG_ON_PENDING_METHODS:
			case rgm::RG_ON_PENDING_DISABLED:
			case rgm::RG_ON_PENDING_MON_DISABLED:
			case rgm::RG_OFF_PENDING_METHODS:
			case rgm::RG_PENDING_OFF_START_FAILED:
			case rgm::RG_PENDING_ONLINE:
			case rgm::RG_PENDING_ON_STARTED:
			case rgm::RG_PENDING_OFF_STOP_FAILED:
			case rgm::RG_ERROR_STOP_FAILED:
			case rgm::RG_OFF_PENDING_BOOT:
				//
				// Internal error-- we already eliminated
				// above states
				//
			default:
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// An internal error has occurred in the rgmd.
				// This may prevent the rgmd from bringing the
				// affected resource group online.
				// @user_action
				// Look for other syslog error messages on the
				// same node. Save a copy of the
				// /var/adm/messages files on all nodes, and
				// report the problem to your authorized Sun
				// service provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "INTERNAL ERROR: bad state <%s> "
				    "(%d) for resource group <%s> in "
				    "rebalance()",
				    pr_rgstate(rglp->rgl_xstate[nodeid].
				    rgx_state),
				    rglp->rgl_xstate[nodeid].rgx_state,
				    rglp->rgl_ccr->rg_name);
				sc_syslog_msg_done(&handle);
				break;
			}


		}

		// Generate primaries change event on the computed nodeset.
		if (!from_nodeset.isEqual(to_nodeset)) {
			// post a primaries changing event
			(void) post_primaries_changing_event(NULL,
			    rglp->rgl_ccr->rg_name,
			    CL_REASON_RG_REBALANCE, NULL,
			    rglp->rgl_ccr->rg_desired_primaries,
			    from_nodeset, to_nodeset);
		}


		// restore the values for num_online, num_primaries
		num_online = orig_num_online;
		num_primaries = orig_num_primaries;

		//
		// This loop computes the nodeset on which the
		// rgm_change_mastery is called to bring the RGs
		// online on the selected candidate nodes.
		//

		for (i = 0; i < ncand; i++) {
			ucmm_print("RGM",
			    NOGET("rebalance num_online <%d>, desired <%d>\n"),
			    num_online, rglp->rgl_ccr->rg_desired_primaries);
			//
			// do not process further if the desired primaries for
			// this RG is satisfied.
			//
			if (num_online >= rglp->rgl_ccr->rg_desired_primaries) {
				break;
			}

			//
			// Note, c_nodes was initialized above
			// (first ncand entries)
			//
			nodeid = c_nodes[i].lni;	/*lint !e644 */

			ucmm_print("RGM",
			    NOGET("rebalance processing for RG <%s>, "
			    "nodeid <%d>, state <%s>\n"),
			    rglp->rgl_ccr->rg_name, nodeid,
			    pr_rgstate(rglp->rgl_xstate[nodeid].rgx_state));

			//
			// Note, we already omitted nodes not in current
			// membership
			//

			switch (rglp->rgl_xstate[nodeid].rgx_state) {

			case rgm::RG_OFFLINE:
				if (num_primaries >=
				    rglp->rgl_ccr->rg_max_primaries) {
					continue;
				}

				//
				// We are about to issue a change_mastery
				// command to the slave node.
				// Whether the call succeeds or fails,
				// we cannot be absolutely certain that the RG
				// will not begin to go online on the slave.
				// Therefore, to be conservative, we assume that
				// the RG will go online on the slave.
				// Increment num_primaries because a node on
				// which the RG is OFFLINE will be
				// PENDING_ONLINE.
				//
				ucmm_print("RGM",
				    NOGET("rebalance inc num_prim\n"));
				num_primaries++;
				// FALLTHRU
			case rgm::RG_PENDING_OFFLINE:


				//
				// Issue IDL command to slave node to
				// change state of RG to PENDING_ONLINE.
				// The slave node may be the president itself.
				//
				// We always treat the call to
				// rgm_change_mastery as having succeeded,
				// in order to avoid any possibility
				// of bringing the RG
				// online on more than max_primaries nodes.
				// There's a slim possibility that could get an
				// rgmd death exception _after_ the
				// RG was brought online on the node.
				// In that case, rgm_change_mastery would fail,
				// but the RG was actually brought online.
				// We don't want to count it as offline on this
				// node and bring it online elsewhere, because
				// of the possibility for data corruption.
				//


				on.nl_name = strdup(rglp->rgl_ccr->rg_name);
				on.nl_next = NULL;

				//
				// Bust any switches in progress in the tclist,
				// because we are going to force them offline.
				//
				// If we bust any switches, we will need to do
				// a cond_broadcast later.
				//
				if (bust_switches(c_nodes[i].tclist)) {
					need_cond_broadcast = B_TRUE;
				}

				//
				// We don't want RGs with affinities for us to
				// run rebalance yet.  Wait until we're done
				// with rebalance, then run rebalance on all
				// those RGs that need it.  We need to set the
				// rgl_no_rebalance flag here, before the call
				// to  rgm_change_mastery, because if we are
				// president node, we will change our state to
				// RG_PENDING_ONLINE directly in this call
				// (not below in the set_rgx_state call).
				//
				rglp->rgl_no_rebalance = B_TRUE;

				(void) rgm_change_mastery(nodeid, &on,
				    c_nodes[i].tclist);
				free(on.nl_name);

				// increment # online/pending_online
				ucmm_print("RGM",
				    NOGET("YYYY inc num_online\n"));
				num_online++;

				//
				// If set_planned_on_nodes flag is true, add
				// this node to rgl_planned_on_nodes.  This
				// allows process_rg() to make sure that this
				// rg gets busted and rebalanced if the node
				// dies while executing a rebalancing
				// command (e.g., 'clrg online').  The flags
				// will be cleared in idl_scswitch_primaries()
				// after the command has completed.
				//
				if (set_planned_on_nodes) {
					rglp->rgl_planned_on_nodes.addLni(
					    nodeid);
				}

				//
				// Need to set our notion of the state as
				// PENDING_ONLINE.  This could be not-quite
				// correct, but if not, it will get corrected
				// by the slave.
				// We need to do this now so that we don't try
				// to bring the RG online on too many primaries.
				//
				set_rgx_state(nodeid, rglp,
				    rgm::RG_PENDING_ONLINE);
				rglp->rgl_no_rebalance = B_FALSE;
				break;

			case rgm::RG_OFF_BOOTED:
			case rgm::RG_OFFLINE_START_FAILED:
			case rgm::RG_ONLINE:
			case rgm::RG_PENDING_ONLINE_BLOCKED:
			case rgm::RG_ON_PENDING_R_RESTART:
			case rgm::RG_ON_PENDING_METHODS:
			case rgm::RG_ON_PENDING_DISABLED:
			case rgm::RG_ON_PENDING_MON_DISABLED:
			case rgm::RG_OFF_PENDING_METHODS:
			case rgm::RG_PENDING_OFF_START_FAILED:
			case rgm::RG_PENDING_ONLINE:
			case rgm::RG_PENDING_ON_STARTED:
			case rgm::RG_PENDING_OFF_STOP_FAILED:
			case rgm::RG_ERROR_STOP_FAILED:
			case rgm::RG_OFF_PENDING_BOOT:
				//
				// Internal error-- we already eliminated
				// above states
				//
			default:
				break;
			}
		}

	}

	//
	// If we busted any switches, we must do a cond_broadcast to wake
	// up any threads waiting for the switch to complete.
	// Most callers of rebalance probably call cond_broadcast themselves,
	// but it doesn't hurt to do it now as well to be on the safe side.
	//
	if (need_cond_broadcast) {
		cond_slavewait.broadcast();
	}

	//
	// free the candidate list
	//
	free_candidate_list(c_nodes, ncand);
	free(c_nodes);

	//
	// If we failed to bring the RG online on at least one node,
	// generate a warning message and event.
	//
	if ((num_primaries <= 0) &&
	    (rglp->rgl_ccr->rg_desired_primaries != 0)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd is currently unable to bring the resource group
		// online because all of its potential masters are down or are
		// ruled out because of strong resource group affinities.
		// @user_action
		// As a cluster reconfiguration is completed, additional nodes
		// become eligible and the resource group may go online
		// without the need for any user action.
		//
		// If the resource group remains offline, repair and reboot
		// broken nodes so they can rejoin the cluster; or use
		// clresourcegroup to edit the Nodelist or RG_affinities
		// property of the resource group, so that the Nodelist
		// contains nodes that are cluster members and on which the
		// resource group's strong affinities can be satisfied.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE, SYSTEXT(
		    "rebalance: no primary node is currently found for "
		    "resource group <%s>."), rglp->rgl_ccr->rg_name);

		//
		// We now have multiple reasons that nodes can be
		// rejected as masters: start failure history and affinites
		// violations.  Different nodes could be rejected for
		// different reasons, so it is impossible to give a definitive
		// reason of either ping-pong failure or affinities failure.
		// Thus, we will always use the generic "no master" reason.
		//
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE,
		    ESC_CLUSTER_RG_REMAINING_OFFLINE,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_RG_NAME, SE_DATA_TYPE_STRING,
		    rglp->rgl_ccr->rg_name,
		    CL_FAILURE_REASON, SE_DATA_TYPE_UINT32,
		    CL_FR_NO_MASTER, NULL);
#else
		(void) sc_publish_event(ESC_CLUSTER_RG_REMAINING_OFFLINE,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_RG_NAME, SE_DATA_TYPE_STRING,
		    rglp->rgl_ccr->rg_name,
		    CL_FAILURE_REASON, SE_DATA_TYPE_UINT32,
		    CL_FR_NO_MASTER, NULL);
#endif
		sc_syslog_msg_done(&handle);
	}

	//
	// Run delayed rebalance on any RGs that were triggered by our
	// rebalance.
	//
	delayed_rebalance(ignore_join_ns, B_FALSE);
}

//
// sort_candidate_nodes
// -------------------------
// We create a list of candidate nodeids.  These are nodes which
// are potential masters of the RG, which are in the cluster, and
// on which the RG is not already online or pending_online.  We exclude
// nodes that do not pass the check_node_candidacy check (which includes a
// check for node evacuation) or which are not found in to_check_nodes.
//
// If to_check_nodes is the EMPTY_NODESET, we check all potential masters.
//
// If check_state is TRUE, we check the state of the RG on
// each node, and only treat the node as a candidate if the RG is in an
// OFFLINEish state.  We also record the num_primaries and num_online.
// If check_state is FALSE, we do not check the state of the RG on each
// node, thus treating every node as a candidate (even if the RG is
// already online).  This version of the function call will not report
// num_online or num_primaries, and should only be called if the caller
// already knows that the RG is in an ok (non-errored) state on each node.
//
// We sort the candidate list as described in the comments for the
// candidate_compare function above.
//
// If check_zone_bootdelay is TRUE, we include in the returned list of
// candidates (cands) any non-global zones which are down although their
// physical node is up.  For such a zone, we set the down_ng_zone flag in
// the candidate_t structure to indicate that this zone is actually down,
// but should be considered by failback() for bootdelay.  However, we
// exclude zones which are in SHUTTING_DOWN state, because we do not
// expect them to reboot soon.
//
// if switch_back is TRUE, the error messages are set in check_node_candidacy.
//
// Returns an error code if there is an error, such as the RG in stop_failed
// state on a node.
//
// Returns SCHA_ERR_NOERR if everything is ok.  Can return SCHA_ERR_NOMEM,
// in which case the caller should take serious action.
//
// Allocate memory for cands. Caller must free memory with free().
//
scha_errmsg_t
sort_candidate_nodes(const LogicalNodeset &to_check_nodes, rglist_p_t rglp,
    candidate_t *&cands, size_t &num_cands, const LogicalNodeset &ignore_ns,
    int &num_online, LogicalNodeset &from_nodeset, int &num_primaries,
    boolean_t new_pres, boolean_t check_state, boolean_t check_zone_bootdelay,
    boolean_t switch_back, boolean_t check_for_failures)
{
	nodeidlist_t *nl;
	LogicalNodeset checked_nodes;
	sc_syslog_msg_handle_t handle;
	uint_t nodelist_order = 0;
	size_t max_nodes;
	LogicalNodeset ignore_nodeset = ignore_ns;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	temp_res = {SCHA_ERR_NOERR, NULL};
	num_cands = 0;
	num_online = num_primaries = 0;

	// Count the number of nodes in the nodelist;
	for (max_nodes = 0, nl = rglp->rgl_ccr->rg_nodelist; nl != NULL;
	    nl = nl->nl_next, max_nodes++);

	// now allocate the candidate node list
	cands = (candidate_t *)calloc(max_nodes, sizeof (candidate_t));

	//
	// Iterate through the RG's nodelist.
	//
	for (nl = rglp->rgl_ccr->rg_nodelist; nl != NULL; nl = nl->nl_next,
	    nodelist_order++) {
		boolean_t in_ignore_ns = B_FALSE;
		LogicalNode *lnNode;

		//
		// Sanity check: make sure nodeid isn't a duplicate.
		//
		if (checked_nodes.containLni(nl->nl_lni)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The same nodename appears twice in the Nodelist of
			// the given resource group. Although non-fatal, this
			// should not occur and may indicate an internal logic
			// error in the rgmd.
			// @user_action
			// Use clresourcegroup show to check the Nodelist of
			// the affected resource group. Save a copy of the
			// /var/adm/messages files on all nodes, and report
			// the problem to your authorized Sun service
			// provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "ERROR: sort_candidate_nodes: duplicate nodeid "
			    "<%d> in Nodelist of resource group <%s>; "
			    "continuing"), nl->nl_lni, rglp->rgl_ccr->rg_name);
			sc_syslog_msg_done(&handle);
			continue;
		}

		//
		// Add the node to our checked_nodes list.
		//
		checked_nodes.addLni(nl->nl_lni);

		//
		// If check_zone_bootdelay is true, and the zone is a
		// non-global zone which is down (not SHUTTING_DOWN
		// or STUCK) but whose
		// physical node is up, then we add this zone to the
		// candidate list with its down_ng_zone flag set to true.
		// This is used only by failback() for implementing zone
		// bootdelay.
		//
		lnNode = lnManager->findObject(nl->nl_lni);
		// lnNode could be NULL if we don't know about the zone yet
		if (lnNode == NULL) {
			continue;
		}
		//
		// Check if lnNode's physical node is up.
		// This might be true even if lnNode itself is down.
		//
		bool isNodeUp = lnNode->isPhysNodeUp();

		if (check_zone_bootdelay && !lnNode->isGlobalZone() &&
		    lnNode->getStatus() < rgm::LN_STUCK && isNodeUp) {
			// Add down zone to candidate list
			cands[num_cands].lni = nl->nl_lni;
			temp_res = check_node_candidacy(cands[num_cands], rglp,
			    nl->nl_lni, check_for_failures,  switch_back);
			if (temp_res.err_code  == SCHA_ERR_NOERR) {
				cands[num_cands].pref = nodelist_order;
				cands[num_cands].down_ng_zone = true;
				num_cands++;
			}
			continue;
		}

		//
		// Skip physical nodes not currently up.  However, if a
		// non-global zone is shutting down or down while its
		// physical node is up, we need to fall through to the
		// code below to check the RG state on the zone, so we can
		// update num_online, num_primaries, etc.  In this case,
		// we add the lni to ignore_nodeset so that we'll skip the
		// call to check_node_candidacy and this node will not be
		// included in the list of candidates.
		//
		if (lnNode->getStatus() != rgm::LN_UP) {
			if (isNodeUp) {
				ignore_nodeset.addLni(nl->nl_lni);
			} else {
				continue;
			}
		}

		//
		// Abort candidate checks if stop failed on any node.
		// Note that we check for stop failed on all nodes in the
		// cluster, not just those that we are considering as
		// candidates.
		//
		if (rglp->rgl_xstate[nl->nl_lni].rgx_state ==
		    rgm::RG_PENDING_OFF_STOP_FAILED ||
		    rglp->rgl_xstate[nl->nl_lni].rgx_state ==
		    rgm::RG_ERROR_STOP_FAILED) {
			// Note this message also syslogged in process_needy_rg,
			// idl_scha_control_giveover, idl_scha_control_restart
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("Resource group <%s> requires operator "
			    "attention due to STOP failure"),
			    rglp->rgl_ccr->rg_name);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_STOPFAILED;
			return (res);
		}


		//
		// skip the node if it's not in our list of nodes to check
		// Note that EMPTY_NODESET means check all nodes in the
		// nodelist.
		//
		if (!to_check_nodes.isEmpty() &&
		    !to_check_nodes.containLni(nl->nl_lni)) {
			continue;
		}

		//
		// Even if we are instructed to ignore this node as a
		// candidate, we need to proceed into the switch statement
		// below to update num_online, num_primaries, and from_nodeset
		// if check_state is true.  We'll do that, but will set
		// in_ignore_ns to true so that we'll skip the call to
		// check_node_candidacy so that this node will not be
		// included in the list of candidates.
		//
		if (ignore_nodeset.containLni(nl->nl_lni)) {
			if (!check_state) {
				continue;
			}
			in_ignore_ns = B_TRUE;
		}

		//
		// If we were not told to check the state of the RG on the
		// node, we check if it's a possible candidate regardless.
		//
		if (!check_state) {
			temp_res = check_node_candidacy(cands[num_cands], rglp,
			    nl->nl_lni, check_for_failures, switch_back);
			if (temp_res.err_code == SCHA_ERR_NOERR) {
				cands[num_cands].pref = nodelist_order;
				num_cands++;
			}
			// check if any error message was set.
			if ((switch_back) && (temp_res.err_msg)) {
				rgm_sprintf(&res, "%s", temp_res.err_msg);
				free(temp_res.err_msg);
				temp_res.err_msg = NULL;
			}
			temp_res.err_code = SCHA_ERR_NOERR;
			continue;
		}

		//
		// If we're here, we're supposed to check the state of the RG
		// on the node.
		//

		// See if this node is a candidate to bring RG online
		switch (rglp->rgl_xstate[nl->nl_lni].rgx_state) {

		case rgm::RG_ONLINE:
		case rgm::RG_ON_PENDING_R_RESTART:
		case rgm::RG_PENDING_ON_STARTED:
		case rgm::RG_PENDING_ONLINE:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
		//
		// The following two cases can occur if an scswitch -n
		// is busted by a node death; RG will revert to ONLINE on the
		// slave.
		//
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_ON_PENDING_MON_DISABLED:
			ucmm_print("RGM", NOGET(
			    "sort_candidate_nodes <%s> online or pending-on "
			    "on node %d\n"), rglp->rgl_ccr->rg_name,
			    nl->nl_lni);
			num_online++;
			num_primaries++;
			// Since the RG is online/onlineish on this node add
			// it to the from_nodeset which is needed to generate
			// events.
			from_nodeset.addLni(nl->nl_lni);

			// Node is not a candidate-- already a primary
			break;

		case rgm::RG_OFF_PENDING_BOOT:
			// This node is still booting; not a candidate.
			ucmm_print("RGM", NOGET(
			    "sort_candidate_nodes <%s> still booting on node "
			    "%d\n"), rglp->rgl_ccr->rg_name, nl->nl_lni);
			break;

		//
		// If we are the old president, the following states are a bug
		// because rgl_switching should have been set to SW_UPDATING.
		//
		// If we are a new president, the following states may occur
		// if an editing operation was in-process when the old
		// president died.  In this case, we set the
		// rgl_new_pres_rebalance flag for the RG to true, and return.
		// We will be called again by set_rgx_state() each
		// time a slave transitions the RG out of a "PENDING_METHODS"
		// state.
		//
		// When the RG has transitioned out of pending states on all
		// nodes, then the actual rebalance will be performed.
		//
		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_METHODS:
			if (new_pres) {
				//
				// Postpone rebalance until INIT/FINI/UPDATE
				// methods initiated by old president have
				// completed.
				//
				rglp->rgl_new_pres_rebalance = B_TRUE;
			} else {
				// Should not occur on old president
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// An internal error has occurred in the
				// locking logic of the rgmd. This error
				// should not occur. It may prevent the rgmd
				// from bringing the indicated resource group
				// online.
				// @user_action
				// Look for other syslog error messages on the
				// same node. Save a copy of the
				// /var/adm/messages files on all nodes, and
				// report the problem to your authorized Sun
				// service provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "ERROR: sort_candidate_nodes: <%s> is "
				    "pending_methods on node <%d>",
				    rglp->rgl_ccr->rg_name, nl->nl_lni);
				sc_syslog_msg_done(&handle);
			}
			res.err_code = SCHA_ERR_RGSTATE;
			return (res);

		//
		// This state "cannot happen" because it was excluded above
		//
		case rgm::RG_PENDING_OFF_STOP_FAILED:
		case rgm::RG_ERROR_STOP_FAILED:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// An internal error has occurred in the rgmd. This
			// may prevent the rgmd from bringing affected
			// resource groups online.
			// @user_action
			// Look for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "INTERNAL ERROR: PENDING_OFF_STOP_FAILED or "
			    "ERROR_STOP_FAILED in sort_candidate_nodes",
			    rglp->rgl_ccr->rg_name, nl->nl_lni);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_STOPFAILED;
			return (res);

		//
		// For the following state, the node is not a candidate, i.e.,
		// we will not switch direction from pending_off_start_failed
		// to pending-on.  Eventually, the RG will go to
		// OFFLINE_START_FAILED on this node.  The slave node will
		// then wake the president and rebalance will be called again.
		// However we still must increment num_primaries.
		//
		case rgm::RG_PENDING_OFF_START_FAILED:
			num_primaries++;
			break;

		//
		// For the remaining cases, the node *is* a potential candidate
		// for bringing this RG online, unless it is a member of
		// ignore_nodeset.
		//

		//
		// Note, OFF_BOOTED and OFFLINE_START_FAILED should have been
		// reset to OFFLINE before calling sort_candidate_nodes;
		// we'll reset them to offline here.
		//
		case rgm::RG_OFF_BOOTED:
		case rgm::RG_OFFLINE_START_FAILED:
			//
			// We don't need to run rebalance again on the RG,
			// because we're already doing so.
			//
			rglp->rgl_no_rebalance = B_TRUE;
			set_rgx_state(nl->nl_lni, rglp, rgm::RG_OFFLINE);
			rglp->rgl_no_rebalance = B_FALSE;
			rglp->rgl_delayed_rebalance = B_FALSE;

			// FALLTHRU

		case rgm::RG_PENDING_OFFLINE:
		case rgm::RG_OFFLINE:
			// If pending-offline, increment # primaries
			if (rglp->rgl_xstate[nl->nl_lni].rgx_state ==
			    rgm::RG_PENDING_OFFLINE) {
				num_primaries++;
			}

			//
			// If node is a member of ignore_nodeset, skip to
			// next node
			//
			if (in_ignore_ns) {
				continue;
			}

			temp_res = check_node_candidacy(cands[num_cands], rglp,
			    nl->nl_lni, check_for_failures,  switch_back);
			if (temp_res.err_code  == SCHA_ERR_NOERR) {
				cands[num_cands].pref = nodelist_order;
				num_cands++;
			} else if (temp_res.err_code == SCHA_ERR_NOMEM) {
				free_candidate_list(cands, num_cands);
				free(cands);
				num_cands = 0;
				return (temp_res);
			}

			if (temp_res.err_msg) {
				rgm_sprintf(&res, "%s", temp_res.err_msg);
				free(temp_res.err_msg);
				temp_res.err_msg = NULL;
				if (res.err_code == SCHA_ERR_NOERR)
					res.err_code = temp_res.err_code;
			}
			temp_res.err_code = SCHA_ERR_NOERR;
		} // [switch on rg state]
	} // [for each node in RG's nodelist]

	// Sort the candidate list
	if (num_cands > 0) {
		dump_candidate_list("***Before qsort:", num_cands, cands);
		qsort((void *)cands, (size_t)num_cands, sizeof (candidate_t),
		    (int (*)(const void *, const void *))candidate_compare);
		dump_candidate_list("***After qsort:", num_cands, cands);
	}
	return (res);
}

//
// check_node_candidacy
// ----------------------
// Input params: pointer to RG list structure, lni.
// Output params: candidate_t structure, which will be filled in if return
// code is 0.
//
// This function performs various checks for bringing rglp online on
// node lni.  If the rg can be brought online, it fills in the candidate_t
// structure with information about the node, and returns SCHA_ERR_NOERR.
// Otherwise, it returns an error code. The error message is set in case the
// switch_back flag is tue. The checks made are:
//
// node evacuation
//    The node is rejected as a candidate if the resource group rglp is
//    currently being evacuated (scswitch -S) from the node, or if the node
//    is currently subject to a "sticky" node evacuation that prevents RGs
//    from moving back onto the node.
//
// strong positive affinities
//    The node is rejected as a candidate if bringing this RG online on the
//    node would violate its strong positive affinities.
//
// strong negative affinities
//    The node is rejected as a candidate if bringing this RG online on the
//    node would violate its strong negative affinities.
//
// RG dependencies
//    The node is rejected as a candidate if bringing this RG online on the
//    node would violate its RG dependencies. Note that RG dependencies
//    are usually independent of the node on which the dependee goes online.
//    However, if the dependee is a single-node RG, then the dependency
//    is local-node only.
//
// Calculates list of RGs that would be forced to move off of the node
// because of this RG coming online.  If any of the pre-empted RGs are
// STOP_FAILED, this node is rejected.
//
// Calculates the weak affinity violations.  These cannot lead to a node being
// rejected, but are used in sorting the nodes.
//
// start_failed history
//
//	First, we prune RG's start-failures that are
//	older than Pingpong_interval.
//
//	If the remaining number of start-failures is greater
//	than RG_START_RETRY_MAX, then the node is not a candidate for
//	becoming a new master of the RG.
//
// This function can return SCHA_ERR_NOMEM, in which case the caller should
// take serious action.
//
scha_errmsg_t
check_node_candidacy(candidate_t &cand, rglist_p_t rglp, rgm::lni_t lni,
    boolean_t check_for_failures, boolean_t switch_back)
{
	rgm_timelist_t	*rgt;
	uint_t i = 0;
	time_t t = 0;
	sc_syslog_msg_handle_t handle;
	namelist_t *offlist = NULL;
	int num_violations = 0;
	namelist_t *templist = NULL;
	char *errmsg_list = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	LogicalNodeset tmpNs;
	const LogicalNode *lnNode = lnManager->findObject(lni);

	CL_PANIC(lnNode != NULL);
	//
	// ignore the start fail history in the case of
	// remaster operation.
	//
	if (switch_back)
		check_for_failures = B_FALSE;

	// skip nodes that are being evacuated
	if (node_is_evacuating(lni, rglp)) {
		ucmm_print("check_node_candidacy ", NOGET(
		    "rg <%s> evacuating node %s"),
		    rglp->rgl_ccr->rg_name, lnNode->getFullName());
		res.err_code = SCHA_ERR_CHECKS;
		return (res);
	}

	//
	// Check that our strong positive affinities are not violated.
	//
	if (!sp_affinities_satisfied(res.err_code, rglp, lni, NULL,
	    &templist)) {
		//
		// If we get an error, return, because it means
		// low memory.
		//
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
		errmsg_list = namelist_to_str(templist);

		//
		// Print an error message if not running a remaster
		// operation (!switch_back). A message is not required
		// for remaster
		//

		if (!switch_back) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd is enforcing the strong positive affinities
			// of the resource groups. This behavior is normal and
			// expected.
			// @user_action
			// No action required. If desired, use clresourcegroup
			// to change the resource group affinities.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    MESSAGE, SYSTEXT("Not attempting "
			    "to start Resource Group <%s> on node <%s>, "
			    "because the Resource Groups <%s> for which it has "
			    "strong positive affinities are not online."),
			    rglp->rgl_ccr->rg_name, lnNode->getFullName(),
			    errmsg_list);
			sc_syslog_msg_done(&handle);
		}
		rgm_free_nlist(templist);
		free(errmsg_list);
		res.err_code = SCHA_ERR_CHECKS;
		return (res);
	}

	//
	// Check that our strong negative affinities are not violated.
	//
	if (!sn_affinities_satisfied(res.err_code, rglp, lni, NULL,
	    &templist)) {
		//
		// If we get an error, return, because it means
		// low memory.
		//
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
		errmsg_list = namelist_to_str(templist);
		if (!switch_back) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd is enforcing the strong negative affinities
			// of the resource groups. This behavior is normal and
			// expected.
			// @user_action
			// No action required. If desired, use clresourcegroup
			// to change the resource group affinities.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    MESSAGE, SYSTEXT("Not attempting "
			    "to start Resource Group <%s> on node <%s>, "
			    "because the Resource Groups <%s> for which it has "
			    "strong negative affinities are online."),
			    rglp->rgl_ccr->rg_name, lnNode->getFullName(),
			    errmsg_list);
			sc_syslog_msg_done(&handle);
		}
		rgm_free_nlist(templist);
		free(errmsg_list);
		res.err_code = SCHA_ERR_CHECKS;
		return (res);
	}

	tmpNs.addLni(lni);
	check_offline_dependees(&res, rglp, NULL, tmpNs);
	if (res.err_code != SCHA_ERR_NOERR) {
		// If we get a low memory error, return.
		if (res.err_code == SCHA_ERR_NOMEM) {
			return (res);
		}
		if (!switch_back) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd is enforcing the resource group
			// dependencies. This behavior is normal and expected.
			// @user_action
			// No action required. If desired, use clresourcegroup
			// to change the resource group dependencies.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    MESSAGE, SYSTEXT("Not attempting "
			    "to start Resource Group <%s> on node <%s>, "
			    "because one or more Resource Groups for which it "
			    "has Resource Group dependencies are not online."),
			    rglp->rgl_ccr->rg_name, lnNode->getFullName());
			sc_syslog_msg_done(&handle);
		}
		res.err_code = SCHA_ERR_CHECKS;
		return (res);
	}

	//
	// Get a list of all RGs on the node that have strong negative
	// affinities for us and are online or pending online, so will have
	// to move off the node.
	//
	if (!compute_sn_violations(res.err_code, rglp, lni, &offlist,
	    num_violations, B_FALSE)) {
		//
		// If we get an error, return, because it means
		// low memory.
		//
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}

		//
		// One of the RGs is in stop_failed, so we have to skip this
		// node.
		//
		if (switch_back) {
			rgm_format_errmsg(&res, REM_RG_AFF_STOPFAILED,
			    rglp->rgl_ccr->rg_name);
		} else {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd is unable to bring the specified resource
			// group online on the specified node because one or
			// more resource groups related to it by strong
			// affinities are in an error state.
			// @user_action
			// Find the errored resource groups(s) by using
			// clresourcegroup status, and clear their error
			// states by using clresourcegroup clear. If desired,
			// use clresourcegroup to change the resource group
			// affinities.
			//
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    MESSAGE, SYSTEXT("Not attempting "
			    "to start resource group <%s> on node <%s> "
			    "because a Resource Group that would be forced "
			    "offline due to affinities is in an error state."),
			    rglp->rgl_ccr->rg_name, lnNode->getFullName());
			sc_syslog_msg_done(&handle);
		}
		res.err_code = SCHA_ERR_CHECKS;
		return (res);
	}

	//
	// Now compute the transitive closure of the offlist.
	//
	if (!compute_sp_transitive_closure(res.err_code, rglp, lni, offlist,
	    &(cand.tclist), cand.V, B_TRUE, B_FALSE)) {
		//
		// If we get an error, return, because it means
		// low memory.
		//
		if (res.err_code != SCHA_ERR_NOERR) {
			rgm_free_nlist(offlist);
			return (res);
		}

		if (switch_back) {
			rgm_format_errmsg(&res, REM_RG_AFF_STOPFAILED,
			    rglp->rgl_ccr->rg_name);
		} else {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_NOTICE,
			    MESSAGE, SYSTEXT("Not attempting "
			    "to start resource group <%s> on node <%s> "
			    "because a Resource Group that would be forced "
			    "offline due to affinities is in an error state."),
			    rglp->rgl_ccr->rg_name, lnNode->getFullName());
			sc_syslog_msg_done(&handle);
		}
		res.err_code = SCHA_ERR_CHECKS;
		rgm_free_nlist(offlist);
		return (res);
	}

	//
	// We don't need the offlist anymore (it's duplicated in tclist).
	//
	rgm_free_nlist(offlist);

	//
	// Compute the weak affinity scores (includes both satisfactions
	// and violations).
	//
	// Do not pass the tclist to compute_outgoing_weak_affinities, because
	// we must count our weak affinities as if we didn't know which groups
	// are being kicked off the node due to our arrival.  That is
	// to satisfy the requirement that outgoing weak affinities take
	// priority over incoming strong negative affinties.
	//
	cand.OW = compute_outgoing_weak_affinities(rglp, lni, NULL);
	cand.IW = compute_incoming_weak_affinities(rglp, lni, cand.tclist);

	//
	// prune expired start_failed timestamp entries for this node
	//
	rgm_timelist_t **rgtimep = &rglp->rgl_xstate[lni].rgx_start_fail;
	rgm_prune_timelist(rgtimep, rglp->rgl_ccr->rg_ppinterval);

	//
	// Count # of recent start failures of RG on lni
	//
	if (check_for_failures) {
		rgt = rglp->rgl_xstate[lni].rgx_start_fail;
		if (rgt != NULL) {
			t = rgt->tl_time;
			i = rgm_count_timelist(rgt);
		}
		if (i > RG_START_RETRY_MAX) {
			//
			// Too many start failures on lni-- skip it.
			//
			if (!switch_back) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The rgmd is preventing "ping-pong" failover
				// of the resource group, i.e., repeated
				// failover of the resource group between two
				// or more nodes.
				// @user_action
				// The time interval in seconds that is
				// mentioned in the message can be adjusted by
				// using clresourcegroup to set the
				// Pingpong_interval property of the resource
				// group.
				//
				(void) sc_syslog_msg_log(handle, LOG_NOTICE,
				    MESSAGE, SYSTEXT("Not attempting"
					" to start resource group <%s> on "
					"node <%s> because this resource group "
					"has already failed to start on this "
					"node %d or more times in the past "
					"%d seconds"),
				    rglp->rgl_ccr->rg_name,
				    lnNode->getFullName(),
				    RG_START_RETRY_MAX + 1,
				    rglp->rgl_ccr->rg_ppinterval);
				sc_syslog_msg_done(&handle);
			}
			rgm_free_nlist(cand.tclist);
			cand.tclist = NULL;
			res.err_code = SCHA_ERR_PINGPONG;
			return (res);
		}
	}

	//
	// Compute the number of resources enabled on lni for rglp
	//
	cand.E = compute_enabled_resources(rglp, lni);

	// lni is a good candidate
	cand.lni = lni;
	cand.start_fail = i;
	cand.recent_fail = t;
	cand.down_ng_zone = false;

	return (res);
}

//
// bust_switches
// ------------
// Busts any switches or failbacks in progress for the RGs in the rgs list.
// If any switches are busted, return B_TRUE, otherwise returns B_FALSE.
//
// Node evacuations (scswitch -S) cannot be busted, however they bust (or
// prevent) other switching actions.
//
boolean_t
bust_switches(namelist_t *rgs)
{
	rglist_p_t rgp;
	namelist_t *temp;
	boolean_t ret = B_FALSE;

	for (temp = rgs; temp != NULL; temp = temp->nl_next) {
#if (SOL_VERSION >= __s10)
		if (is_enhanced_naming_format_used(temp->nl_name))
			continue;
#endif
		rgp = rgname_to_rg(temp->nl_name);
		CL_PANIC(rgp != NULL);
		if (rgp->rgl_switching == SW_IN_PROGRESS ||
		    rgp->rgl_switching == SW_IN_FAILBACK) {
			ucmm_print("RGM", NOGET("Busting switch on rg %s\n"),
			    rgp->rgl_ccr->rg_name);
			rgp->rgl_switching = SW_BUSTED_START;
			ret = B_TRUE;
		}
	}
	return (ret);
}
