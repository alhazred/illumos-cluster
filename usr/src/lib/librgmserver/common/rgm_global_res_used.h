/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * rgm_global_res_used.h - types to implement Global_resources_used
 *	Includes definition of service_state_callback_impl class.
 */

#ifndef	_RGM_GLOBAL_RES_USED_H
#define	_RGM_GLOBAL_RES_USED_H

#pragma ident	"@(#)rgm_global_res_used.h	1.11	08/05/20 SMI"

#include <sys/os.h>
#include <rgm_state.h>
#include <h/replica.h>
#include <orb/object/adapter.h>

// RM service name for disk-related services starts with one of these strings
#define	DEVSVC	"device service "	/* device service */
#define	PXFSSVC	"/dev/"			/* pxfs file system service */

typedef char **rm_service_list_t;	// NULL-terminated array of strings

// return value type for compute_freeze_opcode()
enum fr_opcode {
	RGMFR_NONE,	// RG freeze state should remain unchanged
	RGMFR_UNFREEZE,	// RG freeze state to change from frozen to unfrozen
	RGMFR_FREEZE	// RG freeze state to change from unfrozen to frozen
};

//
// Condition variable signalled by RMA callback functions and by
// president code that edits RGs or switches RGs out of the UNMANAGED state.
// Wakes rgfreeze() to update RG freeze states.
//
extern os::condvar_t	cond_rgfreeze;

extern fr_opcode	compute_freeze_opcode(rglist_p_t rglp);
extern uint_t		compute_frozen_rgs(namelist_t **frozen_rgs_p);
extern void		freeze_adjust_timeouts(rglist_p_t rglp);
extern void		rgfreeze(void *);
extern void		rgm_change_freeze(namelist_t *frozen_rgs);
extern int		init_service_state_callback(void);

/*CSTYLED*/
class service_state_callback_impl :
    public McServerof<replica::service_state_callback> {
public:
	service_state_callback_impl();
	void	_unreferenced(unref_t cookie)
		{ (void) _last_unref(cookie); } // Must fix for clean shutdown.

	// Use this to check that we create only one object of this class
	static service_state_callback_impl *the_service_state_callback_implp;

	//
	// IDL Methods
	//

	// Callback from RMA to rgmd occurs once, soon after the rgmd first
	// registers for callbacks.  This gives the current state of all
	// services.
	void init_services(const replica::service_state_info_list &,
	    Environment &);

	// Callback from RMA tells rgmd that a given service has changed state.
	void state_changed(const replica::service_state_info &,
	    Environment &);

	//
	// local client methods & data
	//

	// TRUE iff any existing service is in recovery
	static boolean_t	is_any_service_frozen();

	// XXX RFE 4248997 : is_one_of_these_services_frozen(svc_list)

private:
	// Holds a list of all services that are currently in recovery.
	static rm_service_list_t	frozen_services;

	// XXX For RFE 4248997 : also maintain list of unfrozen svcs.

	// The number of elements in frozen_services list
	static uint_t		num_frozen;

	// This function processes a single service state change; called by
	//  init_services() and by state_changed().
	void do_svc_state_change(const char *, const replica::service_state);

	// destructor is never called because rgmd president lives as long
	// as the node is a cluster member, and vice-versa.
	~service_state_callback_impl()	{ }
};

#endif	/* !_RGM_GLOBAL_RES_USED_H */
