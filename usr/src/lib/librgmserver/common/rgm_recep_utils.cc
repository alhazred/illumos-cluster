/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_recep_utils.cc	1.91	09/02/27 SMI"

/*
 * rgm_recep_utils.cc is one of the receptionist components of rgmd which
 *	contains the utilities for the  receptionist  functions
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <rpc/rpc.h>
#include <netdb.h>

#include <rgm/rgm_common.h>
#ifdef EUROPA_FARM
#include <cmm/ucmm_api.h>
#endif
#include <rgm/rgm_cnfg.h>
#include <rgm_state.h>
#include <scha.h>
#include <rgm/rgm_msg.h>
#include <rgm_proto.h>
#include <rgm_api.h>
#include <rgm/rgm_util.h>
#include <rgm/rgm_errmsg.h>


#include <rgm/rgm_recep.h>


/* zone support */
#include <rgm_logical_node.h>
#include <rgm_logical_node_manager.h>
#include <rgm_logical_nodeset.h>

using namespace std;


/*
 * get_rs_prop_val()
 *
 *	get the property value from the RS state structure in memory
 *	If the property type is STRING, store the value to returned buffer
 *	val_str.
 *	If the property type is STRING_ARRAY and UINT_ARRAY, store the
 *	value to returned buffer val_strarr.
 *	If the property type is INT, ENUM or BOOLEAN, convert
 *	the value to ASCII using 'rgm_itoa' and store it in the returned
 *	buffer 'val_str'
 *
 * NOTES:
 *	If the returned flag 'need_free' is TRUE, the caller is responsible
 *	for freeing 'val_str' allocated by this routine.
 *
 *	Return SCHA_ERR_PROP error if the property does not exist
 *
 */

scha_errmsg_t
get_rs_prop_val(const char *prop_name, rgm_resource_t *resource,
    scha_prop_type_t *type, std::map<rgm::lni_t, char *> &val_str,
    namelist_t **val_strarr,
    boolean_t *need_free, rdeplist_t **val_array_rdep)

{
	rgm_property_list_t *pp;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	*val_array_rdep = NULL;
	*val_strarr = NULL;
	*type = SCHA_PTYPE_STRING;
	*need_free = B_FALSE;



	if ((strcmp(prop_name, SCHA_TYPE)) == 0) {
		val_str[0] = resource->r_type;
	} else if ((strcmp(prop_name, SCHA_TYPE_VERSION)) == 0) {
		val_str[0] = resource->r_type_version;
	} else if ((strcmp(prop_name, SCHA_RESOURCE_PROJECT_NAME)) == 0) {
		val_str[0] = get_project_name(resource, NULL);
		//
		// if the returned buffer is
		// NULL, return SCHA_ERR_NOMEM
		//
		if (val_str[0] == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			rgm_format_errmsg(&res,
			REM_INTERNAL_MEM);
			return (res);
		} else {
			*need_free = B_TRUE;
		}
	} else if ((strcmp(prop_name, SCHA_RESOURCE_DEPENDENCIES)) == 0) {
		*val_array_rdep = (rdeplist_t *)resource->
		r_dependencies.dp_strong;
		*type = SCHA_PTYPE_STRINGARRAY;
	} else if ((strcmp(prop_name, SCHA_RESOURCE_DEPENDENCIES_WEAK)) == 0) {
		*val_array_rdep = (rdeplist_t *)resource->
		r_dependencies.dp_weak;
		*type = SCHA_PTYPE_STRINGARRAY;
	} else if ((strcmp(prop_name, SCHA_RESOURCE_DEPENDENCIES_RESTART)) ==
	    0) {
		*val_array_rdep = (rdeplist_t *)resource->
		r_dependencies.dp_restart;
		*type = SCHA_PTYPE_STRINGARRAY;
	} else if ((strcmp(prop_name,
	    SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART)) == 0) {
		*val_array_rdep = (rdeplist_t *)
		    resource->r_dependencies.dp_offline_restart;
		*type = SCHA_PTYPE_STRINGARRAY;
	} else if ((strcmp(prop_name, SCHA_R_DESCRIPTION)) == 0) {
		val_str[0] = resource->r_description;
	} else {
		boolean_t prop_found = B_FALSE;
		/*
		 * all optional system-defined property are stored in
		 * resource->r_properties
		 * The value of the property is stored in ASCII form.
		 */
		for (pp = resource->r_properties; pp != NULL;
		    pp = pp->rpl_next) {
			if ((strcmp(pp->rpl_property->rp_key, prop_name))
			    == 0) {
				if ((pp->rpl_property->rp_type ==
				    SCHA_PTYPE_STRINGARRAY ||
				    pp->rpl_property->rp_type ==
				    SCHA_PTYPE_UINTARRAY)) {
					*val_strarr =
					    pp->rpl_property->rp_array_values;
					*type = pp->rpl_property->rp_type;
					//
					// Check if NRU is NULL.If it is NULL
					// then compute the NRU from the STRONG,
					// WEAK, RESTART and OFFLINE_RESTART
					// dependency lists
					//
					if ((strcmp
					    (SCHA_NETWORK_RESOURCES_USED,
					    prop_name) == 0) &&
					    (pp->rpl_property->
					    rp_array_values == NULL)) {
						// call the helper function
						compute_NRUlst_frm_deplist(
						    &(resource->r_dependencies),
						    val_strarr, B_FALSE);
						if (*val_strarr != NULL) {
							*need_free = B_TRUE;
						}
					}
				} else if (pp->rpl_property->rp_type ==
				    SCHA_PTYPE_BOOLEAN) {
					val_str[0] = rgm_itoa((int)str2bool(
					    ((rgm_value_t *)pp->rpl_property->
					    rp_value)->rp_value[0]));
					//
					// if the returned buffer is NULL,
					// return SCHA_ERR_NOMEM
					//
					if (val_str[0] == NULL) {
						res.err_code = SCHA_ERR_NOMEM;
						rgm_format_errmsg(&res,
						REM_INTERNAL_MEM);
						return (res);
					} else {
						*need_free = B_TRUE;
					}
				} else {
					//
					// All other optional sys-defined
					// properties are integer types -- just
					// return the string.
					// If we ever add an enum type property
					// to this list, must convert the enum
					// value to an integer string.
					//
					val_str[0] = ((rgm_value_t *)pp->
					    rpl_property->rp_value)->
					    rp_value[0];
				}
				prop_found = B_TRUE;
				break;
			}
		}
		if (!prop_found) {
			ucmm_print("RGM", NOGET(
			    "get_rs_prop_val - cannot find property <%s>\n"),
			    prop_name);
			res.err_code = SCHA_ERR_PROP;
			return (res);
		}
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}

/*
 * get_rt_resource_list()
 *
 *	walk the list of the RGs, then the list of the resources, looking for
 *	all the resources of the given type.
 *
 *	state lock is held
 */
scha_errmsg_t
get_rt_resource_list(rgm_rt_t *rt, namelist_t **nl)
{
	rglist_p_t	rg;
	rlist_p_t	r;
	namelist_t	*n;
	namelist_t	*current = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};


	*nl = NULL;

	/*
	 * look at the list of the resource groups, then the list of resources
	 * build a temporary name_list whith the resource names with a matching
	 * type. Don't allocate space for the resource names, store_prop_val()
	 * will make the copy
	 */
	for (rg = Rgm_state->rgm_rglist; rg != NULL; rg = rg->rgl_next) {

		for (r = rg->rgl_resources; r != NULL; r = r->rl_next) {

			if (strcmp(r->rl_ccrtype->rt_name, rt->rt_name) == 0) {

				/* build new element */
				n = (namelist_t *)
					malloc_nocheck(sizeof (namelist_t));
				if (n == NULL) {
					goto finished; //lint !e801
				}
				n->nl_name = r->rl_ccrdata->r_name;
				n->nl_next = NULL;

				/* add element to the list */
				if (*nl == NULL) {
					*nl = n;
				}
				if (current != NULL) {
					current->nl_next = n;
				}
				current = n;
			}
		}
	}

	return (res);

finished:
	/*
	 * don't call rgm_free_nlist() as we only want to free the
	 * namelist_t structure, not the content it points to
	 */
	if (*nl != NULL) {
		n = *nl;
		while (n != NULL) {
			current = n;
			n = n->nl_next;
			free(current);
		}
	}

	res.err_code = SCHA_ERR_NOMEM;
	return (res);
}


/*
 * get_rt_prop_val()
 *
 *	Get the property value from the RT in-memory state structure.
 *	If the property type is STRING, store the value to returned buffer
 *	val_str.
 *	If the property type is STRING_ARRAY and UINT_ARRAY, store the
 *	value to returned buffer val_strarr.
 *	If the property type is INT, ENUM or BOOLEAN, convert
 *	the value to ASCII using 'rgm_itoa' and store it in the returned
 *	buffer 'val_str'
 *
 * NOTES:
 *	If the returned flag 'need_free' is TRUE, the caller is responsible
 *	for freeing 'val_str' or 'val_strarr' allocated by this routine.
 *
 *	Return SCHA_ERR_PROP error if the property does not exist
 *
 */
scha_errmsg_t
get_rt_prop_val(char *prop_name, rgm_rt_t *rt, scha_prop_type_t *type,
    name_t *val_str, namelist_t **val_strarr, boolean_t *need_free)
{

	boolean_t is_optional_prop = B_FALSE;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	*val_str = NULL;
	*val_strarr = NULL;
	*type = SCHA_PTYPE_STRING;
	*need_free = B_FALSE;

	if ((strcmp(prop_name, SCHA_RT_BASEDIR)) == 0) {
		is_optional_prop = B_TRUE;
		*val_str = rt->rt_basedir;
	} else if ((strcmp(prop_name, SCHA_SINGLE_INSTANCE)) == 0) {
		*val_str = rgm_itoa(rt->rt_single_inst);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_RT_SYSTEM)) == 0) {
		is_optional_prop = B_TRUE;
		*val_str = rgm_itoa(rt->rt_system);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_INIT_NODES)) == 0) {
		*val_str = rgm_itoa(rt->rt_init_nodes);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_INSTALLED_NODES)) == 0) {
		if (rt->rt_instl_nodes.is_ALL_value) {
			*val_str = strdup_nocheck(SCHA_ALL_SPECIAL_VALUE);
			// note, NOMEM check is done below
			*need_free = B_TRUE;
		} else {
			res.err_code = nodeidlist_to_namelist(
				rt->rt_instl_nodes.nodeids, val_strarr);
			if (SCHA_ERR_NOERR != res.err_code) {
				// note, nodeidlist_to_namelist() frees
				// val_strarr upon error
				return (res);
			}
			// Caller has to free val_strarr
			*need_free = B_TRUE;
			*type = SCHA_PTYPE_STRINGARRAY;
		}
	} else if ((strcmp(prop_name, SCHA_FAILOVER)) == 0) {
		*val_str = rgm_itoa(rt->rt_failover);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_PROXY)) == 0) {
		*val_str = rgm_itoa(rt->rt_proxy);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_API_VERSION)) == 0) {
		*val_str = rgm_itoa((int)rt->rt_api_version);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_PKGLIST)) == 0) {
		is_optional_prop = B_TRUE;
		*val_strarr = (namelist_t *)rt->rt_pkglist;
		*type = SCHA_PTYPE_STRINGARRAY;
	} else if ((strcmp(prop_name, SCHA_RT_VERSION)) == 0) {
		is_optional_prop = B_TRUE;
		*val_str = rt->rt_version;
	} else if (((strcmp(prop_name, SCHA_IS_SHARED_ADDRESS)) == 0) ||
		((strcmp(prop_name, SCHA_IS_LOGICAL_HOSTNAME)) == 0)) {
		is_optional_prop = B_TRUE;
		*val_str = rgm_itoa(rt->rt_sysdeftype);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_RT_DESCRIPTION)) == 0) {
		is_optional_prop = B_TRUE;
		*val_str = rt->rt_description;
	} else if ((strcmp(prop_name, SCHA_RESOURCE_LIST)) == 0) {
		res = get_rt_resource_list(rt, val_strarr);
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
		*type = SCHA_PTYPE_STRINGARRAY;
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_GLOBALZONE)) == 0) {
		*val_str = rgm_itoa(rt->rt_globalzone);
		*need_free = B_TRUE;
	} else {
		is_optional_prop = B_TRUE;
		get_rt_method_pathname(prop_name, rt, val_str);
	}

	/*
	 * if an optional property does not have a value set in
	 * the RT config struct, it was not specified at RT registration,
	 * and so does not exist for the RT.
	 * Return an error.
	 */
	if (is_optional_prop && *val_str == NULL && *val_strarr == NULL) {
		ucmm_print("RGM",
		    NOGET("get_rt_prop_val: cannot find property <%s>\n"),
		    prop_name);
		res.err_code = SCHA_ERR_PROP;
		return (res);
	}

	/*
	 * We did not check the returned buffer from 'rgm_itoa' or
	 * 'strdup_nocheck'.
	 * If the returned buffer is NULL, return SCHA_ERR_NOMEM
	 */
	if (*need_free && *type == SCHA_PTYPE_STRING && val_str == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		*need_free = B_FALSE;
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}

/*
 * get_rg_prop_val()
 *
 *	Get the property value from the RG in-memory state structure.
 *	If the property type is STRING, store the value to returned buffer
 *	val_str.
 *	If the property type is STRING_ARRAY and UINT_ARRAY, store the
 *	value to returned buffer val_strarr.
 *	If the property type is INT, ENUM or BOOLEAN, convert
 *	the value to ASCII using 'rgm_itoa' and store it in the returned
 *	buffer 'val_str'
 *
 * NOTES:
 *	If the returned flag 'need_free' is TRUE, the caller is responsible
 *	for freeing 'val_str' or 'val_strarr' (whichever is non-NULL)
 *	allocated by this routine.
 *
 *	Return SCHA_ERR_PROP error if the property does not exist
 *
 */
scha_errmsg_t
get_rg_prop_val(char *prop_name, rglist_t *rg, scha_prop_type_t *type,
    name_t *val_str, namelist_t **val_strarr, boolean_t *need_free)
{

	rlist_t	*rl;
	namelist_t *nl;
	namelist_t *curr = NULL;
	boolean_t	prop_found = B_TRUE;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	boolean_t	need_free_strarr = B_FALSE;

	*val_str = NULL;
	*val_strarr = NULL;
	*type = SCHA_PTYPE_STRING;
	*need_free = B_FALSE;

	if ((strcmp(prop_name, SCHA_NODELIST)) == 0) {
		res.err_code = nodeidlist_to_namelist(rg->rgl_ccr->rg_nodelist,
		    val_strarr);
		if (SCHA_ERR_NOERR != res.err_code) {
			// note, nodeidlist_to_namelist() frees val_strarr
			// upon error
			return (res);
		}
		// Caller has to free val_strarr
		need_free_strarr = B_TRUE;
		*type = SCHA_PTYPE_STRINGARRAY;
	} else if ((strcmp(prop_name, SCHA_MAXIMUM_PRIMARIES)) == 0) {
		*val_str = rgm_itoa(rg->rgl_ccr->rg_max_primaries);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_DESIRED_PRIMARIES)) == 0) {
		*val_str = rgm_itoa(rg->rgl_ccr->rg_desired_primaries);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_FAILBACK)) == 0) {
		*val_str = rgm_itoa(rg->rgl_ccr->rg_failback);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_RG_SYSTEM)) == 0) {
		*val_str = rgm_itoa(rg->rgl_ccr->rg_system);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_RESOURCE_LIST)) == 0) {
		for (rl = rg->rgl_resources; rl != NULL; rl = rl->rl_next) {
			nl = (namelist_t *)calloc_nocheck
			    (1, sizeof (namelist_t));
			if (nl == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				// Caller has to free val_strarr
				*need_free = B_TRUE;
				return (res);
			}
			nl->nl_name = strdup_nocheck(rl->rl_ccrdata->r_name);
			if (nl->nl_name == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				// Caller has to free val_strarr
				*need_free = B_TRUE;
				return (res);
			}

			if (*val_strarr == NULL)
				*val_strarr = nl;
			else
				curr->nl_next = nl;
			curr = nl;
		}
		// Caller has to free val_strarr
		need_free_strarr = B_TRUE;
		*type = SCHA_PTYPE_STRINGARRAY;
	} else if ((strcmp(prop_name, SCHA_RG_DEPENDENCIES)) == 0) {
		*val_strarr = (namelist_t *)rg->rgl_ccr->rg_dependencies;
		*type = SCHA_PTYPE_STRINGARRAY;
	} else if ((strcmp(prop_name, SCHA_GLOBAL_RESOURCES_USED)) == 0) {
		if (rg->rgl_ccr->rg_glb_rsrcused.is_ALL_value) {
			*val_str = strdup_nocheck(SCHA_ALL_SPECIAL_VALUE);
			// note, NOMEM check is done below
			*need_free = B_TRUE;
		} else {
			*val_strarr =
			    (namelist_t *)rg->rgl_ccr->rg_glb_rsrcused.names;
			*type = SCHA_PTYPE_STRINGARRAY;
		}
	} else if (strcmp(prop_name, SCHA_RG_MODE) == 0) {
		*val_str = rgm_itoa(rg->rgl_ccr->rg_mode);
		*need_free = B_TRUE;
	} else if (strcmp(prop_name, SCHA_IMPL_NET_DEPEND) == 0) {
		*val_str = rgm_itoa(rg->rgl_ccr->rg_impl_net_depend);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_PATHPREFIX)) == 0) {
		*val_str = rg->rgl_ccr->rg_pathprefix;
	} else if ((strcmp(prop_name, SCHA_RG_PROJECT_NAME)) == 0) {
		*val_str = get_project_name(NULL, rg->rgl_ccr);
		*need_free = B_TRUE;
// SC SLM addon start
	} else if ((strcmp(prop_name, SCHA_RG_SLM_TYPE)) == 0) {
		*val_str = rg->rgl_ccr->rg_slm_type;
	} else if ((strcmp(prop_name, SCHA_RG_SLM_PSET_TYPE)) == 0) {
		*val_str = rg->rgl_ccr->rg_slm_pset_type;
	} else if ((strcmp(prop_name, SCHA_RG_SLM_CPU_SHARES)) == 0) {
		*val_str = rgm_itoa(rg->rgl_ccr->rg_slm_cpu);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_RG_SLM_PSET_MIN)) == 0) {
		*val_str = rgm_itoa(rg->rgl_ccr->rg_slm_cpu_min);
		*need_free = B_TRUE;
// SC SLM addon end
	} else if ((strcmp(prop_name, SCHA_RG_AFFINITIES)) == 0) {
		*val_strarr = (namelist_t *)rg->rgl_ccr->rg_affinities;
		*type = SCHA_PTYPE_STRINGARRAY;
	} else if ((strcmp(prop_name, SCHA_RG_AUTO_START)) == 0) {
		*val_str = rgm_itoa(rg->rgl_ccr->rg_auto_start);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_RG_SUSP_AUTO_RECOVERY)) == 0) {
		*val_str = rgm_itoa(rg->rgl_ccr->rg_suspended);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_RG_DESCRIPTION)) == 0) {
		*val_str = rg->rgl_ccr->rg_description;
	} else if ((strcmp(prop_name, SCHA_PINGPONG_INTERVAL)) == 0) {
		*val_str = rgm_itoa(rg->rgl_ccr->rg_ppinterval);
		*need_free = B_TRUE;
	} else if ((strcmp(prop_name, SCHA_RG_IS_FROZEN)) == 0) {
		*val_str = rgm_itoa(rg->rgl_is_frozen ? 1 : 0);
		*need_free = B_TRUE;
	} else
		prop_found = B_FALSE;

	if (!prop_found) {
		ucmm_print("RGM",
		    NOGET("get_rg_prop_val: cannot find property <%s>\n"),
		    prop_name);
		res.err_code = SCHA_ERR_PROP;
		return (res);
	}

	/*
	 * We did not check the returned buffer from 'rgm_itoa' or
	 * 'strdup_nocheck'.
	 * If the returned buffer is NULL, return SCHA_ERR_NOMEM
	 */
	if (*need_free && val_str == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		*need_free = B_FALSE;
		return (res);
	}

	// If we're returning a stringarray that we malloc'd, the caller
	// has to free it.
	if (need_free_strarr) {
		*need_free = B_TRUE;
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}

/*
 * get_rt_method_pathname()
 *
 *	get the pathname of a RT method  from RT in-memory state structure
 *	if the given RT method name does not have a value set in
 *	RT config struct, it was not specified at RT registration,
 *	the returned buffer val_str is set to NULL
 */
void
get_rt_method_pathname(char *method_name, rgm_rt_t *rt, name_t *val_str)
{

	*val_str = NULL;

	if ((strcmp(method_name, SCHA_START)) == 0) {
		*val_str =  rt->rt_methods.m_start;
	} else if ((strcmp(method_name, SCHA_STOP)) == 0) {
		*val_str = rt->rt_methods.m_stop;
	} else if ((strcmp(method_name, SCHA_VALIDATE)) == 0) {
		*val_str = rt->rt_methods.m_validate;
	} else if ((strcmp(method_name, SCHA_UPDATE)) == 0) {
		*val_str = rt->rt_methods.m_update;
	} else if ((strcmp(method_name, SCHA_INIT)) == 0) {
		*val_str = rt->rt_methods.m_init;
	} else if ((strcmp(method_name, SCHA_FINI)) == 0) {
		*val_str = rt->rt_methods.m_fini;
	} else if ((strcmp(method_name, SCHA_BOOT)) == 0) {
		*val_str = rt->rt_methods.m_boot;
	} else if ((strcmp(method_name, SCHA_MONITOR_START)) == 0) {
		*val_str = rt->rt_methods.m_monitor_start;
	} else if ((strcmp(method_name, SCHA_MONITOR_STOP)) == 0) {
		*val_str = rt->rt_methods.m_monitor_stop;
	} else if ((strcmp(method_name, SCHA_MONITOR_CHECK)) == 0) {
		*val_str = rt->rt_methods.m_monitor_check;
	} else if ((strcmp(method_name, SCHA_PRENET_START)) == 0) {
		*val_str = rt->rt_methods.m_prenet_start;
	} else if ((strcmp(method_name, SCHA_POSTNET_STOP)) == 0) {
		*val_str = rt->rt_methods.m_postnet_stop;
	}
}


/*
 * store_prop_val_dep()
 *
 *	based on the type of the property, put the property value which stored
 *	in val_str or val_strarr into a RPC structure starr_list(ret_list).
 *	Input:
 *		If the type of a property is SCHA_PTYPE_STRINGARRAY,
 *		or SCHA_PTYPE_UINTARRAY, the data is stored in val_strarr.
 *	The property being referenced here is one of the four types of
 *	dependencies. The type of property has be SCHA_PTYPE_STRINGARRAY.
 *
 */
scha_errmsg_t
store_prop_val_dep(scha_prop_type_t type, rdeplist_t *val_strarr,
    strarr_list **ret_list, boolean_t qarg)
{

	rdeplist_t	*ap;
	int	i, n;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	char *loc_type = NULL;

	(*ret_list) = NULL;

	if ((type == SCHA_PTYPE_STRINGARRAY) ||
	    (type == SCHA_PTYPE_UINTARRAY)) {
		/*
		 * if the property of STRINGARRAY or UINTARRAY type does
		 * not have data or is NULL, the returned buf is set to NULL
		 */
		if ((val_strarr == NULL) ||
		    (val_strarr->rl_name[0] == '\0')) {
			res.err_code = SCHA_ERR_NOERR;
			return (res);
		}

		/*
		 * allocate the returned buffer
		 */
		if ((*ret_list = (strarr_list *)
		    calloc_nocheck(1, sizeof (strarr_list))) == NULL) {
			goto finished; //lint !e801
		}

		/*
		 * The property has data.
		 * calculate the number of elements in the array
		 */
		i = 0;
		for (ap = val_strarr; ap != NULL; ap = ap->rl_next)
			++i;

		/*
		 * allocate the array
		 */
		if (((*ret_list)->strarr_list_val = (char **)
		    calloc_nocheck((size_t)i, sizeof (char *))) == NULL) {
			goto finished; //lint !e801
		}


		i = 0;
		for (ap = val_strarr; ap != NULL; ap = ap->rl_next) {
			if (qarg) {
				if (ap->locality_type == LOCAL_NODE)
					loc_type = strdup(SCHA_LOCAL_NODE);
				else if (ap->locality_type == ANY_NODE)
					loc_type = strdup(SCHA_ANY_NODE);
			}
			if (loc_type != NULL) {
				(*ret_list)->strarr_list_val[i] =
				    new char[strlen(ap->rl_name) +
				    3 + strlen(loc_type)];
				// (*ret_list)->strarr_list_val[i][0] = '\0';
				(void) strcpy((*ret_list)->strarr_list_val[i],
				    ap->rl_name);
				(void) strcat((*ret_list)->strarr_list_val[i],
				    "{");
				(void) strcat((*ret_list)->strarr_list_val[i],
				    loc_type);
				(void) strcat((*ret_list)->strarr_list_val[i],
				    "}");
				free(loc_type);
				loc_type = NULL;
			} else  if (((*ret_list)->strarr_list_val[i] =
				strdup_nocheck(ap->rl_name)) == NULL) {
				for (n = 0; n < i; n++) {
					if ((*ret_list)->strarr_list_val[n]
					    != NULL) {
						free((*ret_list)->
						    strarr_list_val[n]);
					}
				}
				(*ret_list)->strarr_list_val = NULL;
				goto finished; //lint !e801
			}
			i++;
		}
		(*ret_list)->strarr_list_len = (uint_t)i;
	}
	res.err_code = SCHA_ERR_NOERR;
	return (res);

finished:
	ucmm_print("RGM",
	    NOGET("couldn't strdup; returning\n"));
	if (*ret_list != NULL) {
		if ((*ret_list)->strarr_list_val != NULL)
			free((*ret_list)->strarr_list_val);
		free(*ret_list);
		*ret_list = NULL;
	}
	res.err_code = SCHA_ERR_NOMEM;
	return (res);
}



/*
 * store_prop_val()
 *
 *	based on the type of the property, put the property value which stored
 *	in val_str or val_strarr into a RPC structure starr_list(ret_list).
 *	Input:
 *		If the type of a property is SCHA_PTYPE_STRINGARRAY,
 *		or SCHA_PTYPE_UINTARRAY, the data is stored in val_strarr.
 *		Other types of property, the data is stored in val_str.
 *		prop_name and rl arguments would be sent only in the case
 *		of per-node node properties. This is used to populate the
 *		property default value from the RT param tables in case the
 *		property value is NULL on the requested lni.
 */
scha_errmsg_t
store_prop_val(scha_prop_type_t type, namelist_t *val_strarr,
    std::map<rgm::lni_t, char *> &val_str, boolean_t is_pn,
    name_t node_name, strarr_list **ret_list, char *prop_name,
    rlist_p_t rl)
{

	namelist_t	*ap;
	rgm::lni_t lni;
	int	i, n;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	(*ret_list) = NULL;

	if ((type == SCHA_PTYPE_STRINGARRAY) ||
	    (type == SCHA_PTYPE_UINTARRAY)) {
		/*
		 * if the property of STRINGARRAY or UINTARRAY type does
		 * not have data or is NULL, the returned buf is set to NULL
		 */
		if ((val_strarr == NULL) ||
		    (val_strarr->nl_name[0] == '\0') &&
		    (val_strarr->nl_next == NULL)) {
			res.err_code = SCHA_ERR_NOERR;
			return (res);
		}

		/*
		 * allocate the returned buffer
		 */
		if ((*ret_list = (strarr_list *)
		    calloc_nocheck(1, sizeof (strarr_list))) == NULL) {
			goto finished; //lint !e801
		}

		/*
		 * The property has data.
		 * calculate the number of elements in the array
		 */
		i = 0;
		for (ap = val_strarr; ap != NULL; ap = ap->nl_next)
			++i;

		/*
		 * allocate the array
		 */
		if (((*ret_list)->strarr_list_val = (char **)
		    calloc_nocheck((size_t)i, sizeof (char *))) == NULL) {
			goto finished; //lint !e801
		}


		i = 0;
		for (ap = val_strarr; ap != NULL; ap = ap->nl_next) {
			if (((*ret_list)->strarr_list_val[i] =
			    strdup_nocheck(ap->nl_name)) == NULL) {
				for (n = 0; n < i; n++) {
					if ((*ret_list)->strarr_list_val[n]
					    != NULL) {
						free((*ret_list)->
						    strarr_list_val[n]);
					}
				}
				(*ret_list)->strarr_list_val = NULL;
				goto finished; //lint !e801
			}
			i++;
		}
		(*ret_list)->strarr_list_len = (uint_t)i;
	} else {
		if ((*ret_list = (strarr_list *)calloc_nocheck(1,
		    sizeof (strarr_list))) == NULL) {
			goto finished; //lint !e801
		}
		if (((*ret_list)->strarr_list_val = (char **)calloc_nocheck(
		    1, sizeof (char *))) == NULL) {
			free(*ret_list);
			goto finished; //lint !e801
		}

		if (is_pn) {
			if ((lni = rgm_get_lni(node_name)) == LNI_UNDEF) {
				res.err_code = SCHA_ERR_NODE;
				free((*ret_list)->strarr_list_val);
				free(*ret_list);
				return (res);
			}
			if (val_str[lni] == NULL) {
				if ((res.err_code =
				    copy_default_value(prop_name, rl,
				    &(*ret_list)->strarr_list_val[0]))
				    != SCHA_ERR_NOERR) {
					free((*ret_list)->strarr_list_val);
					free(*ret_list);
					return (res);
				}
			} else {
				if (((*ret_list)->strarr_list_val[0] =
				    strdup_nocheck(val_str[lni])) == NULL) {
					goto finished; //lint !e801
				}
			}
		} else if (val_str[0] == NULL) {
			//
			// Should never reach here.
			//
			res.err_code = SCHA_ERR_INTERNAL;
			return (res);
		} else {
			if (((*ret_list)->strarr_list_val[0] =
			    strdup_nocheck(val_str[0])) == NULL) {
				goto finished; //lint !e801
			}
		}

		(*ret_list)->strarr_list_len = 1;
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);

finished:
	ucmm_print("RGM",
	    NOGET("couldn't strdup; returning\n"));
	if (*ret_list != NULL) {
		if ((*ret_list)->strarr_list_val != NULL)
			free((*ret_list)->strarr_list_val);
		free(*ret_list);
		*ret_list = NULL;
	}
	res.err_code = SCHA_ERR_NOMEM;
	return (res);
}


//
// wait_until_ccr_is_read
// This function sleeps until the CCR has been read.
//
// For receptionist functions that do not call the president (i.e.
// they access the in-memory state of the local rgmd), we have to check to
// make sure that we have read the CCR.  If this node has just joined the
// cluster, there is a window of time in which the president has not yet
// ordered this node to read the CCR.  So in scha_resource*_open, we
// call this function to wait until the CCR has been read, then process
// the request.
//
// For simplicity, this function uses a sleep in a while-loop, rather than
// a cond_wait.
//
// The caller must hold the RGM state lock.  This function will release
// the lock while sleeping and will reclaim it before returning.
//
void
wait_until_ccr_is_read()
{
	while (!Rgm_state->ccr_is_read) {
		rgm_unlock_state();
		(void) sleep(1);
		rgm_lock_state();
	}
}
