//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_logical_node.cc	1.24	09/02/04 SMI"

// unix headers
#include <strings.h>
#include <stdlib.h>

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <libzonecfg.h>
#endif

// Sun Cluster headers
#include <rgm_logical_node.h>
#include <rgm_proto.h>
#include <rgm/scutils.h>
#include <sys/cl_events.h>

// stl headers
using namespace std;

LogicalNode::LogicalNode(sol::nodeid_t node, const char *zonename_in,
    rgm::rgm_ln_state status_in) : nodename(NULL), zonename(NULL),
    lni(0), incarnation(0), status(status_in), status_saved(false),
    nodeid(node), evacuating(false), evac_expires(0), intention(NULL)
{
	initialize_names(zonename_in, NULL);
}

// this ctor takes a nodename that should be used if the nodeid
// conversion to nodename returns NULL.
//
LogicalNode::LogicalNode(sol::nodeid_t node, const char *zonename_in,
    const char *nodename_in, rgm::rgm_ln_state status_in) : nodename(NULL),
    zonename(NULL), lni(0), incarnation(0), status(status_in),
    status_saved(false), nodeid(node), evacuating(false), evac_expires(0),
    intention(NULL)
{
	initialize_names(zonename_in, nodename_in);
	(void) generate_state_change_event();
}

//
// If this zone is UP, not a physical node,
// and its physical node is the cluster node on which
// this code is running, and this node is already
// "booted", run boot methods in this zone.
//
void
LogicalNode::check_run_boot_meths()
{
	sol::nodeid_t mynodeid = get_local_nodeid();
	if (status == rgm::LN_UP && !isZcOrGlobalZone() &&
	    (getNodeid() == mynodeid) && !Rgm_state->is_thisnode_booting) {
		run_boot_meths_on_logical_node(lni);
	}
}

LogicalNode::~LogicalNode()
{
	free(nodename);
	free(zonename);
	free(fullname);
}

//
// Add LN to cluster membership.  Increment incarnation number.
//
void
LogicalNode::enterCluster()
{
	if (status < rgm::LN_UP) {
		incarnation++;
		status = rgm::LN_UP;
		(void) generate_state_change_event();
	}
}

//
// Add a new zones in the list maintained by the
// LogicalNode corresponding to this cluster node.
//
void
LogicalNode::insertLocalZone(LogicalNode *nextNode)
{
	// Assert this is a physical node.
	CL_PANIC(nodeid == lni);

	local_zones.push_back(nextNode);
}

rgm::rgm_ln_state
LogicalNode::getStatus()
{
#if (SOL_VERSION >= __s10)
	// If we are the zone cluster rgmd, then we have a different check
	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		return (status);
	}
#endif
	if (lni == get_local_nodeid())
		return (rgm::LN_UP);
	return (status);
}

//
// Function to check whether the node is up or not.
// For zones, this method will check the status of the zone
// when this method is called. Note that this method does not
// depend on the getStatus() method. The return value from
// getStatus() is heavily dependent on the process membership
// callbacks and the zone state change callbacks. isUp() will
// use the zone_get_state() function to determine the current
// status of a zone. Note that this method should be called
// only on the node where this method is executing.
//
bool
LogicalNode::isUp() const
{
	//
	// Assert that this method is called only on the
	// local node/zone.
	//
	ASSERT(nodeid == get_local_nodeid());
#if (SOL_VERSION < __s10)
	//
	// For S9 and below, use the "status" variable
	//
	return (status >= rgm::LN_UP || lni == get_local_nodeid());
#else
	int err = Z_OK;
	zone_state_t state = ZONE_STATE_CONFIGURED;
	char *local_zone_name = NULL;
	//
	// If this method is called on a physical cluster node,
	// then we will use the "status" variable itself.
	//
	if (isGlobalZone()) {
		return (status >= rgm::LN_UP || lni == get_local_nodeid());
	}

	//
	// If we are here, it means this method was called
	// for a zone. For a zone, we will use zonecfg
	// library calls to determine its state.
	// This zone is configured on the current physical
	// node. We will use zonecfg calls to determine the
	// status of this zone. We have to be careful to pick
	// the zone name properly. For native zones, "zonename"
	// contains the zone name, but for zone clusters, we have
	// to pick the zone name from "ZONE".
	//
	if (strcmp(ZONE, GLOBAL_ZONENAME) == 0) {
		local_zone_name = zonename;
	} else {
		local_zone_name = ZONE;
	}

	err = zone_get_state(local_zone_name, &state);

	if (err != Z_OK) {
		//
		// Unable to determine the zone state.
		// Default to using the "status" varible.
		//
		return (status >= rgm::LN_UP);
	}

	//
	// The zone will be in UP state only if the Solaris zone state is
	// ZONE_STATE_RUNNING. Please note that there are two zone states
	// above ZONE_STATE_RUNNING (one is ZONE_STATE_DOWN and
	// ZONE_STATE_MOUNTED).
	//
	return (state == ZONE_STATE_RUNNING);
#endif
}

//
// Function to check whether the LN's physical node is up.
// We use rgm_comm_getref() so that it will work for ZCs as well as
// base cluster zones.
//
bool
LogicalNode::isPhysNodeUp() const
{
	rgm::rgm_comm_var prgm_serv = rgm_comm_getref(nodeid);
	return (CORBA::is_nil(prgm_serv) ? false : true);
}

//
// Returns the LN's incarnation number.  For non-global zones in the global
// cluster, we return the 'incarnation' member of the LN object, which
// simulates a cmm incarnation number.  For ZC zones or global zones, we ignore
// the value of 'incarnation' and return the actual cmm incarnation number
// cached in Rgm_state.
//
// If the running vp version is less than "zone cluster" level (i.e., the
// rolling upgrade is not yet committed), return the LN incarnation number
// only.
//
rgm::ln_incarnation_t
LogicalNode::getIncarnation() const
{
	return ((is_rgm_zc_version() && isZcOrGlobalZone()) ?
	    (rgm::ln_incarnation_t)(Rgm_state->node_incarnations.
	    members[nodeid]) : incarnation);
}


//
// Returns true if the lni is a base-cluster global zone; returns
// false otherwise.
// Assert that isGlobalZone is never called before lni has been set.
//
bool
LogicalNode::isGlobalZone() const
{
	CL_PANIC(lni != 0);
	return (strcmp(ZONE, GLOBAL_ZONENAME) == 0 && lni == nodeid);
}


//
// Remove a zone from the list.
//
void
LogicalNode::removeLocalZone(LogicalNode *node)
{
	// Check this is a physical node.
	CL_PANIC(nodeid == lni);

	local_zones.remove(node);
}
//
// The nodeid has been set in the ctor.
// Set the zonename, nodename, and fullname fields based on the nodeid
// and zonename.
//
void
LogicalNode::initialize_names(const char *zonename_in, const char *nodename_in)
{
	// first, look up the nodename using the nodeid
	// we set in the ctor initializer list
	//
	// The result must not be NULL, unless the caller passed
	// a nodename to use in case the nodeid: nodename mapping
	// didn't exist.
	//
	nodename = rgm_get_nodename_cache(nodeid);
	if (nodename == NULL) {
		CL_PANIC(nodename_in != NULL);
		nodename = strdup(nodename_in);
	}

	// now assign the zonename
	if (zonename_in) {
		zonename = strdup(zonename_in);
	}

	// now calculate the fullname
	if (zonename == NULL) {
		// no zone
		fullname = strdup(nodename);
	} else {
		// we have a zonename
		if (strcmp(zonename, GLOBAL_ZONENAME) == 0) {
			// zonename is 'global' -- just use the nodename
			fullname = strdup(nodename);
		} else {
			// zonename is non-global -- use nodename and zonename
			fullname = (char *)malloc(strlen(nodename) +
			    strlen(zonename) + 2);
			(void) sprintf(fullname, "%s:%s", nodename, zonename);
		}
	}
}

void LogicalNode::leaveCluster()
{
	if (!isGlobalZone()) {
		clearEvacZone();
	}
	// optimize by checking if we're already in the cluster
	if (status >= rgm::LN_SHUTTING_DOWN) {
		status = rgm::LN_DOWN;
		(void) generate_state_change_event();

#ifndef EUROPA_FARM
		//
		// If I am president, and this zone is non-global, and
		// its physical node remains alive, check for in-progress
		// switchovers or updates of RGs that need to be busted due
		// to the zone death.  Pass arguments to process_rgs_on_death()
		// indicating that this is a non-global zone death only.
		//
		// If the physical or ZC node is dead, this call is unnecessary
		// because rgm_reconfig() calls process_rgs_on_death()
		// directly.  Note that rgm_reconfig() has updated the
		// physical node membership before this method gets called.
		//
		if (!isZcOrGlobalZone() && am_i_president() &&
		    in_current_physical_membership(getNodeid())) {
			LogicalNodeset empty_ns;
			ucmm_print("leaveCluster",
			    NOGET("call process_rgs_on_death(), lni=%d"), lni);
			process_rgs_on_death(B_FALSE, empty_ns, B_TRUE);
		}
#endif

		//
		// If this zone is a non-global zone whose
		// physical node is the cluster node on which
		// this code is running, call process_zone_death()
		//
		sol::nodeid_t mynodeid = get_local_nodeid();
		if (!isGlobalZone() && (getNodeid() == mynodeid)) {
			process_zone_death(lni);
		}

		//
		// Iterate over each local zone in the list, calling
		// the leaveCluster() method on each of them.
		// Use the mem_fun function object adapter to
		// call the member function on each object pointer.
		//
		(void) for_each(local_zones.begin(), local_zones.end(),
		    mem_fun(&LogicalNode::leaveCluster));
	}
}

void LogicalNode::unconfigure()
{
	// optimize by checking if we're already unconfigured
	if (status > rgm::LN_UNCONFIGURED) {
		status = rgm::LN_UNCONFIGURED;
		(void) generate_state_change_event();

		//
		// Iterate over each local zone in the list, calling
		// the unconfigure() method on each of them.
		// Use the mem_fun function object adapter to
		// call the member function on each object pointer.
		//
		(void) for_each(local_zones.begin(), local_zones.end(),
		    mem_fun(&LogicalNode::unconfigure));
	}
}

void
LogicalNode::configure()
{
	if (status < rgm::LN_DOWN) {
		status = rgm::LN_DOWN;
		(void) generate_state_change_event();
	}
}


//
// Sticky node evacuation (scswitch -S) persists for timeout period after
// evacuation is complete.
//
void
LogicalNode::endEvac(time_t timeout)
{
	time_t current_tm, expire_tm;

	ucmm_print("RGM", NOGET("endEvac lni=%d nodeid=%d timeout=%u\n"),
	    lni, nodeid, timeout);

	evacuating = false;
	current_tm = time((time_t *)0);
	// error should never occur -- assert for debugging only
	ASSERT(current_tm > 0);
	expire_tm = current_tm + timeout;
	//
	// Only set evac_expires if expire_time is greater than the current
	// value.  If two or more threads are evacuating the same node,
	// we will use the longest evac_expires time.
	//
	if (expire_tm > evac_expires) {
		evac_expires = expire_tm;
	}
}


bool
LogicalNode::isEvacuating() const
{
	time_t current_tm;
	bool ret;

	if (evacuating) {
		ucmm_print("RGM",
		    NOGET("isEvacuating lni=%d nodeid=%d return=%x\n"),
		    lni, nodeid, true);
		return (true);
	}

	current_tm = time((time_t *)0);
	// error should never occur -- assert for debugging only
	ASSERT(current_tm > 0);
	ret = (current_tm < evac_expires);
	ucmm_print("RGM", NOGET("isEvacuating lni=%d nodeid=%d return=%x\n"),
	    lni, nodeid, ret);
	return (ret);
}


//
// Clear node evacuation of a zone.
// Caller is leaveCluster().
//
void
LogicalNode::clearEvacZone()
{
	evacuating = false;
	evac_expires = (time_t)0;
	ucmm_print("RGM", NOGET("clearEvacZone lni=%d nodeid=%d\n"),
	    lni, nodeid);
}


//
// Clear node evacuation on a node and on all of its zones.
// Used on a node that is down or joining.
// Also used when an RG is explicitly switched onto the evacuating node.
//
void
LogicalNode::clearEvac()
{
	if (status != rgm::LN_UNCONFIGURED) {
		// don't print dummy nodes
		ucmm_print("RGM", NOGET("clearEvac nodeid=%d\n"), nodeid);
	}
	evacuating = false;
	evac_expires = (time_t)0;

	// If this is a global zone, clear the local zones as well
	if (nodeid == lni) {
		// Iterate over each local zone in the list, calling
		// the clearEvac() method on each of them.
		// Use the mem_fun function object adapter to
		// call the member function on each object pointer.
		(void) for_each(local_zones.begin(), local_zones.end(),
		    mem_fun(&LogicalNode::clearEvac));
	}
}

//
// Note: these are replicated in
// usr/src/common/cl/mdbmacros/rgm/mdb_rgm_print.cc
//
static const char *status_strings[] = {"UNKNOWN", "UNCONFIGURED",
	"DOWN", "STUCK", "SHUTTING_DOWN", "UP"};

void
LogicalNode::debug_print()
{
	if (strncmp(fullname, "unconfigured", 12) == 0) {
		// don't print these dummy nodes
		return;
	}
	ucmm_print("RGM", NOGET("    %s <lni=%d, nodeid=%d, status=%s>\n"),
	    fullname, lni, nodeid, status_strings[status]);
}

void
LogicalNode::updateCachedNames()
{
	free(nodename);
	nodename = rgm_get_nodename(nodeid);
	CL_PANIC(nodename != NULL);
	free(fullname);
	if (zonename == NULL) {
		fullname = strdup(nodename);
	} else {
		fullname = (char *)malloc(strlen(nodename) + strlen(zonename)
		    + 2);
		(void) sprintf(fullname, "%s:%s", nodename, zonename);
	}
}

void
LogicalNode::setStatus(rgm::rgm_ln_state status_in)
{
	ucmm_print("RGM", "LogicalNode::setStatus: setting status from %d "
	    "to %d for LN %s\n", status, status_in, fullname);

	//
	// What we call depends almost entirely on the new state, so just
	// switch on the new state.
	//
	// Note that leaveCluster() and enterCluster() take care of
	// calling the appropriate functions to process zone death and
	// zone join
	//
	switch (status_in) {
	case rgm::LN_UNKNOWN:
		// these methods are no-ops if we're already at a low
		// state
		leaveCluster();
		unconfigure();
		status = status_in;
		break;
	case rgm::LN_UNCONFIGURED:
		leaveCluster();
		unconfigure();
		break;
	case rgm::LN_DOWN:
		// figure out which direction we're going
		if (status >= rgm::LN_SHUTTING_DOWN) {
			leaveCluster();
		} else {
			configure();
		}
		break;
	case rgm::LN_UP:
		enterCluster();
		break;
	case rgm::LN_SHUTTING_DOWN:
		//
		// transition to LN_SHUTTING_DOWN should be allowed only
		// from a state greater than or equal to itself.
		//
		if (status >= rgm::LN_SHUTTING_DOWN) {
			status = rgm::LN_SHUTTING_DOWN;
		} else {
			ASSERT(0);
		}
		break;
	case rgm::LN_STUCK:
		// we don't handle it yet
	default:
		ucmm_print("rgm", "LN::setStatus(): bad state %d\n",
		    status_in);
		// do nothing
		break;
	}
}

void
LogicalNode::saveState()
{
	// set the state to the current state
	old_status = status;

	//
	// Set status_saved to distinguish this ln from a new ln that's
	// created after we save state but before we revert membership
	//
	status_saved = true;

	// If this is a global zone, save the local zones as well
	if (nodeid == lni) {
		//
		// Iterate over each local zone in the list, calling
		// the saveState() method on each of them.
		// Use the mem_fun function object adapter to
		// call the member function on each object pointer.
		//
		(void) for_each(local_zones.begin(), local_zones.end(),
		    mem_fun(&LogicalNode::saveState));
	}
}

void
LogicalNode::revertState()
{
	if (status_saved && old_status != status) {
		setStatus(old_status);
	}

	//
	// If this is a global zone, revert the local zones as well
	//
	// If we're reverting a physical node to DOWN instead of UP,
	// then reverting the logical nodes will be (probably) redundant with
	// most of the changes down by set_status (leaveCluster calls itself
	// on all non-global zones).
	//
	if (nodeid == lni) {
		//
		// Iterate over each local zone in the list, calling
		// the clearSavedState() method on each of them.
		// Use the mem_fun function object adapter to
		// call the member function on each object pointer.
		//
		(void) for_each(local_zones.begin(), local_zones.end(),
		    mem_fun(&LogicalNode::revertState));
	}
}

void
LogicalNode::generate_state_change_event()
{
	//
	// The event is logged only of the logical node
	// exists on the same physical node.
	//
	if (nodeid == get_local_nodeid()) {
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE,
		    ESC_CLUSTER_ZONES_STATE,
		    CL_EVENT_PUB_ZONES, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_EVENT_ZONE_NAME, SE_DATA_TYPE_STRING,
		    zonename,
		    CL_EVENT_ZONE_STATE, SE_DATA_TYPE_INT16,
		    status, NULL);
#else
		(void) sc_publish_event(
		    ESC_CLUSTER_ZONES_STATE,
		    CL_EVENT_PUB_ZONES, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_EVENT_ZONE_NAME, SE_DATA_TYPE_STRING,
		    zonename,
		    CL_EVENT_ZONE_STATE, SE_DATA_TYPE_INT16,
		    status, NULL);
#endif
	}
}
