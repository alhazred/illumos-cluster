//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_readccr.cc	1.159	09/02/26 SMI"

//
// rgm_readccr.cc
//
//	Read CCR info into internal state structure
//

#include <h/ccr.h>
#ifndef linux
#include <sys/int_types.h>
#endif
#include <stdio.h>
#include <stdlib.h>

#include <rgm_threads.h>
#include <rgm_state.h>
#include <rgm/rgm_cnfg.h>
#include <rgm_proto.h>
#include <rgm/rgm_util.h>
#include <rgm/rgm_cpp.h>
#include <sys/sol_version.h>
/* Zone support */
#include <rgm_logical_node_manager.h>
#include <rgm_logical_node.h>

// SC SLM addon start
#ifdef SC_SRM
#include <rgm/fecl.h>
#endif
// SC SLM addon end
#include <rgmx_hook.h>


/*
 * We have 10 routines, 9 of which are of the form rgm_XXX_YYY,
 * where XXX could be add, delete and update and YYY could be rg, rt and r.
 * The only remaining routine is rgm_init_ccr, which is called once when
 * the rgmd is coming up and is used to read all the relevant CCR objects,
 * RG/R/RT's into memory.
 *
 * The rgm_add_YYY routines are used to read an object of type YYY into
 * memory and also hook it up in the internal memory structure. Please note
 * that a given rgm_add_YYY routine ONLY reads in objects of the given
 * type. So for example rgm_add_rg, only reads in one RG and not the R's in
 * that RG. It is upto the caller to read in anything else if necessary.
 * Also, only the rgm_add_YYY routines interface with the lower level CCR
 * routines (rgm_cnfg_XXX), hence providing a clean separation among
 * layers.
 *
 * The rgm_delete_YYY routines delete an object of the given type and
 * adjust pointers to maintain consistency.
 * The rgm_update_YYY routines are used to update the CCR portion of an
 * object of the given type. They essentially delete the CCR portion of the
 * RG or R being updated, followed by a read/hookup of the same portion thus
 * preserving the dynamic state and all the back pointers.
 *
 * In a way, we have attempted to implement these routines in an object
 * oriented manner while still using C!
 *
 */


static void check(rgm_state_t *state);
static scha_errmsg_t rgm_get_resources(char *rg_name);
// SC SLM addon start
#ifdef SC_SRM
static int rgm_slm_update_rg(char *, char *, char *, int, int);
static int rgm_slm_update_delete_rg(char *);
static int rgm_slm_update_delete_r(char *, char *);
#endif
// SC SLM addon end


/*
 * This method uses the current total number of resources to adjust
 * the MAX number of RPC threads in the receptionist:
 * - The outstanding number of scha_control() is bound by the number
 *   of resources. We create at least twice as many threads to allow
 *   this maximum to be reached and to also allow other scha_calls to be
 *   performed simultanuously.
 */
static inline void
update_nb_rpc_threads()
{
#ifndef linux
	int cur_max_scha_control;
	int cur_max_rpc_threads;

	if (Rgm_state->rgm_total_rs <= MAX_SCHA_CONTROLS_INIT) {
		cur_max_scha_control = MAX_SCHA_CONTROLS_INIT;
	} else {
		cur_max_scha_control = Rgm_state->rgm_total_rs;
	}

	cur_max_rpc_threads = cur_max_scha_control *
	    RATIO_NB_THREADS_VS_MAX_SCHA_CONTROL;

	if (!rpc_control(RPC_SVC_THRMAX_SET, &cur_max_rpc_threads)) {
		ucmm_print("RGM",
		    NOGET("rpc_control() failed to set the "
			"maximum number of threads"));
	}
#endif
}


/*
 * Helper method for rgm_add_rg/rgm_add_rt
 * Parse the nodelist/instl_nodes and
 * set the lni for each element of the list.
 *
 * All LNIs should exist on the president, and all LNIs for zones on a slave
 * node should exist on the slave, because by the time
 * the read_ccr functions are called, the president has called
 * create_lni_mappings_from_nodeidlist (from rgm_rg_impl.cc or rgm_rt_impl.cc).
 * PANIC the node if this is not true.
 */
static scha_errmsg_t
set_lni_in_nodelist(nodeidlist_t *list)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	nodeidlist_t	*currNodelist = list;
	LogicalNode	*lnNode;
	sol::nodeid_t mynodeid = get_local_nodeid();

	while (currNodelist) {
		lnNode = lnManager->findLogicalNode(currNodelist->nl_nodeid,
		    currNodelist->nl_zonename, NULL);
		if (NULL != lnNode) {
			currNodelist->nl_lni = lnNode->getLni();
		} else {
			// If I'm president, I should already know about
			// all lnis. If I'm not president, but this is a
			// zone on my node, I should already know about its
			// lni
			if (am_i_president() || (currNodelist->nl_nodeid ==
				mynodeid)) {
				CL_PANIC(NULL != lnNode);
			}
			// I'm not pres and this zone is not on this node
			currNodelist->nl_lni = LNI_UNDEF;
		}
		currNodelist = currNodelist->nl_next;
	}
	return (res);
}

/*
 * is_farm_rg -
 *      return true if the RG is configured to run
 *      on Farm nodes, false otherwise.
 */
boolean_t
is_farm_rg(nodeidlist_t *nodeidlst)
{
	nodeidlist_t *list;
	for (list = nodeidlst; list != NULL; list = list->nl_next) {
		if (list->nl_nodeid > MAXNODES)
			return (B_TRUE);
	}
	return (B_FALSE);
}

/*
 * rgm_add_rg
 * read the given RG (rg_name) into memory, initialize its fields
 * and also hook it up in the internal memory structure.
 *
 * The newly read RG is returned in the second argument new_rg, if it
 * is non-null. The caller MUST NOT free the *new_rg, as this is only
 * a pointer to the in-memory struct.
 *
 * This function assumes:
 * that no R's exist in that RG yet.
 * otherwise it's an internal error.
 *
 */
scha_errmsg_t
rgm_add_rg(char *rg_name, rglist_t **new_rg)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm_rg_t	*rg_ccr;    // the CCR portion of the RG
	rglist_t	*rg_mem;    // the in-memory RG structure
	sc_syslog_msg_handle_t handle;

	if (rg_name == NULL) {
		res.err_code = SCHA_ERR_RG;
		return (res);
	}

	ucmm_print("RGM", NOGET("rgm_add_rg(), rg_name = %s\n"), rg_name);

	res = rgmcnfg_read_rgtable(rg_name, &rg_ccr, ZONE);
	ucmm_print("RGM", NOGET("rgm_add_rg(), after readrgtable\n"), rg_name);
	if (res.err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("rgmcnfg_read_rgtable() failed with <%d>\n"),
		    res.err_code);
		return (res);
	}

	//  If we are running in non Europa mode, the RGs configured
	//  on farm nodes are not allowed.
	if (!rgmx_hook_enabled() && is_farm_rg(rg_ccr->rg_nodelist)) {
		ucmm_print("RGM",
		    NOGET("RG %s is configured to run on farm nodes "
		    " - not allowed in the current configuration\n"),
		    rg_name);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: RG %s is configured to run on farm nodes "
		    " - not allowed in the current configuration", rg_name);
		sc_syslog_msg_done(&handle);
		res.err_code = SCHA_ERR_INTERNAL;
		return (res);
	}

	if (am_i_president() && !Rgm_state->ccr_is_read) {
#ifdef EUROPA_FARM
		ASSERT(0);
#else
		//
		// New president in a new cluster creates the LNI
		// mappings when reading the CCR when zones are
		// supported
		//
		// The only time a president reads the CCR is a new president
		// in a new cluster.
		//
		// This function is also called when an RG is added
		// later. In that case, we do not need to create the
		// mappings (we already did it in rgm_rg_impl.cc).
		//
		// Distinguish those two cases via the ccr_is_read flag.
		// It's false when the CCR is first being read -- true
		// otherwise
		//
		create_lni_mappings_from_nodeidlist(
			// Note: lint doesn't understand default arguments
			rg_ccr->rg_nodelist);	/*lint !e118 */
#endif // EUROPA_FARM
	} else {
		//
		// In any other cases we need to set the 'lni' field
		// in the structure nodeidlist_t
		//
		res = set_lni_in_nodelist(rg_ccr->rg_nodelist);
	}
	if (res.err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("rgmcnfg_read_rgtable() failed with <%d>\n"),
		    res.err_code);
		return (res);
	}

	// Allocate the dynamic portion of the RG

	rg_mem = new rglist_t;
	if (rg_mem == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}


	// if new_rg is non-null => the caller is interested in the newly
	// created RG, hence set it
	if (new_rg != NULL)
		*new_rg = rg_mem;

	//
	// No need to initialize the rgl_xstate mapping. The
	// std::map creates a default element with the default
	// ctor for any index accessed that doesn't already
	// contain an element.
	//

	rg_mem->rgl_ccr  = rg_ccr;
	rg_mem->rgl_switching = SW_NONE;
	rg_mem->rgl_failback_running = B_FALSE;
	rg_mem->rgl_failback_interrupt = 0;
	rg_mem->rgl_total_rs = 0;
	rg_mem->rgl_resources = NULL;
	rg_mem->rgl_next = NULL;
	rg_mem->rgl_is_frozen = FROZ_FALSE;
	rg_mem->rgl_last_frozen = (time_t)0;
	rg_mem->rgl_methods_in_flight = NULL;
	rg_mem->rgl_new_pres_rebalance = B_FALSE;
	rg_mem->rgl_delayed_rebalance = B_FALSE;
	rg_mem->rgl_no_rebalance = B_FALSE;

	//
	// Initialize the mdb logging structures.
	//
	rg_mem->log_rgxstate.rgx_state = NULL;
	rg_mem->log_rgxstate.size = 0;

	//
	// Initialize the mdb backup structures.
	//
	rg_mem->mdb_rgxback.rgx_state = NULL;
	rg_mem->mdb_rgxback.size = 0;

	rg_mem->rgl_no_triggered_rebalance = B_FALSE;
	rg_mem->rgl_affinities.ap_strong_pos = NULL;
	rg_mem->rgl_affinities.ap_weak_pos = NULL;
	rg_mem->rgl_affinities.ap_strong_neg = NULL;
	rg_mem->rgl_affinities.ap_weak_neg = NULL;
	rg_mem->rgl_affinities.ap_failover_delegation = NULL;
	rg_mem->rgl_affinitents.ap_strong_pos = NULL;
	rg_mem->rgl_affinitents.ap_weak_pos = NULL;
	rg_mem->rgl_affinitents.ap_strong_neg = NULL;
	rg_mem->rgl_affinitents.ap_weak_neg = NULL;
	rg_mem->rgl_affinitents.ap_failover_delegation = NULL;
	rg_mem->rgl_quiescing = B_FALSE;

#ifndef EUROPA_FARM
	//
	// If the cluster is rebooting, the president adds/removes
	// Ok_To_Start entry in ccr table depend on the setting of property
	// auto_start_on_new_cluster and sync-up the ok_to_start flag.
	// New president on old cluster and slaves set the in-memory
	// ok_to_start flag according to the existence of Ok_To_Start in ccr.
	//
	if (am_i_president() & Rgm_state->is_thisnode_booting) {
		rg_mem->rgl_ok_to_start = rg_mem->rgl_ccr->rg_auto_start;
		ucmm_print("addrg", "ok to start:%d", rg_mem->rgl_ok_to_start);
		res = rgmcnfg_set_ok_start_rg(rg_mem->rgl_ccr->rg_name,
		    rg_mem->rgl_ok_to_start, ZONE);
		if (res.err_code != SCHA_ERR_NOERR) {
			// CCR update failed. Stop further processing
			// and return early.
			ucmm_print("RGM", NOGET(
			    "CCR update of Ok_To_Start flag failed for "
			    "resource group %s.\n"), rg_mem->rgl_ccr->rg_name);
			return (res);
		}
		//
		// Re-read rg_stamp from the CCR.  We have to do this here
		// on the president because we've already read in the rg_stamp
		// in the call to rgmcnfg_read_rgtable() above, and the
		// value is now stale.  The slaves have not yet read the
		// CCR at this point, so they will pick up the correct
		// rg_stamp value.
		//
		res = update_rg_seq_id(rg_mem);
		if (res.err_code != SCHA_ERR_NOERR)
			return (res);
	} else
#endif
		rg_mem->rgl_ok_to_start =
		    rgmcnfg_is_ok_start_rg(rg_mem->rgl_ccr->rg_name, ZONE);

	// stick rg_mem in Rgm_state
	// by adding to the head of Rgm_state->rgm_rglist

	rg_mem->rgl_next = Rgm_state->rgm_rglist;
	Rgm_state->rgm_rglist = rg_mem;

	Rgm_state->rgm_total_rgs++;

	check(Rgm_state);
	return (res);
}

/*
 * rgm_add_r
 * read the given R (r_name) into memory, initialize its fields
 * and also hook it up in the internal memory structure.
 * Read in the new sequence id (CCR generation number)
 * for the RG and update RGM's in-memory copy of this value.
 *
 * The newly read R is returned in the argument new_r, if it
 * is non-null. The caller MUST NOT free the *new_r, as this is only
 * a pointer to the in-memory struct.
 *
 * This function assumes:
 * that the containing RG already exists in the memory,
 * that the RT of this R already exists in the memory
 * (RT's are a special case and read-on-demand by rtname_to_rt)
 * otherwise it's an internal error.
 *
 */

scha_errmsg_t
rgm_add_r(char *r_name, char *rg_name, rlist_t **new_r)
{
	sol::nodeid_t	i;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm_resource_t	*r_ccr = NULL;
	rglist_t	*rg_ptr = NULL;
	rlist_t		*r_mem = NULL;
	rgm_rt_t 	*rt_ptr = NULL;
	char *swtch = NULL;

	if (r_name == NULL || rg_name == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		return (res);
	}

	ucmm_print("RGM", NOGET("rgm_add_r(), r_name = %s, rg_name = %s\n"),
	    r_name, rg_name);

	res = rgmcnfg_get_resource(r_name, rg_name, &r_ccr, ZONE);
	if (res.err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("rgmcnfg_get_resource() failed with <%d>\n"),
		    res.err_code);
		return (res);
	}

	rg_ptr = rgname_to_rg(rg_name);

	if (rg_ptr == NULL) {
		ucmm_print("RGM", NOGET("rg %s not found\n"), rg_name);
		rgm_free_resource(r_ccr);
		res.err_code = SCHA_ERR_INTERNAL;
		return (res);
	}
	update_onoff_monitored_switch(r_ccr,
	    rg_ptr->rgl_ccr->rg_nodelist);
	update_pn_default_value(r_ccr->r_ext_properties,
	    rg_ptr->rgl_ccr->rg_nodelist);

	//
	// Initialize mdb backup information.
	//
	r_ccr->rx_back.mdb_onoffswitch = NULL;
	r_ccr->rx_back.mdb_monswitch = NULL;
	r_ccr->rx_back.mdb_stdprops.value = NULL;
	r_ccr->rx_back.mdb_stdprops.size = 0;
	r_ccr->rx_back.mdb_extprops.value = NULL;
	r_ccr->rx_back.mdb_extprops.size = 0;


	// update the RG CCR table sequence id in memory
	if ((res = update_rg_seq_id(rg_ptr)).err_code != SCHA_ERR_NOERR)
		return (res);

	// Allocate the dynamic portion of the R

	r_mem = new rlist_t;

	// if new_r is non-null => the caller is interested in the newly
	// created R, hence set it
	if (new_r != NULL)
		*new_r = r_mem;

	if ((rt_ptr = rtname_to_rt(r_ccr->r_type)) == NULL) {
		ucmm_print("RGM", NOGET("rt %s not found\n"), r_ccr->r_type);
		rgm_free_resource(r_ccr);
		res.err_code = SCHA_ERR_INTERNAL;
		return (res);
	}


	// Update the mdb backup data.
	if ((res = update_mdb_extback(r_ccr->rx_back,
	    r_ccr->r_ext_properties)).err_code != SCHA_ERR_NOERR) {
		return (res);
	}


	// Update the mdb backup data.

	if ((res = update_mdb_stdback(r_ccr->rx_back,
	    r_ccr->r_properties)).err_code != SCHA_ERR_NOERR) {
		return (res);
	}

	swtch = switchmap_to_string(((rgm_switch_t *)r_ccr->r_onoff_switch)
	    ->r_switch);

	r_ccr->rx_back.mdb_onoffswitch = swtch;

	swtch = switchmap_to_string(((rgm_switch_t *)r_ccr
	    ->r_monitored_switch)->r_switch);

	r_ccr->rx_back.mdb_monswitch = swtch;

	r_mem->rl_ccrtype = rt_ptr;
	r_mem->rl_ccrdata = r_ccr;
	r_mem->rl_rg = rg_ptr;

	//
	// Initialize mdb logging information.
	//
	r_mem->log_rxstate.rx_state = NULL;
	r_mem->log_rxstate.size = 0;

	r_mem->mdb_rxback.rx_state = NULL;
	r_mem->mdb_rxback.size = 0;

	// rl_is_deleted flag is used during resource deletion.
	// It will be set to TRUE before FINI method are run.
	// Later after the fini method is completed, the slave removes
	// resource from memory if rl_is_deleted flag is TRUE.
	r_mem->rl_is_deleted = B_FALSE;

	r_mem->rl_next = NULL;
	r_mem->rl_dependents.dp_restart = NULL;
	r_mem->rl_dependents.dp_strong = NULL;
	r_mem->rl_dependents.dp_weak = NULL;
	r_mem->rl_dependents.dp_offline_restart = NULL;

	//
	//
	// No need to initialize the rl_xstate mapping. The
	// std::map creates a default element with the default
	// ctor for any index accessed that doesn't already
	// contain an element.
	//

	// stick r_mem in Rgm_state
	// by adding it to the head of rg_ptr->rgl_resources list of the
	// corresponding RG

	r_mem->rl_next = rg_ptr->rgl_resources;

	rg_ptr->rgl_resources = r_mem;

	rg_ptr->rgl_total_rs++;

	//
	// Increment total number of resources and increase number
	// of RPC threads if necessary
	//
	Rgm_state->rgm_total_rs++;
	update_nb_rpc_threads();

	check(Rgm_state);
	return (res);
}


/*
 * rgm_add_rt
 *
 * read the given RT (rt_name) into memory, initialize its fields
 * and also hook it up in the internal memory structure.
 *
 * The newly read RT is returned in the second argument new_rt, if it
 * is non-null. The caller MUST NOT free the *new_rt, as this is only
 * a pointer to the in-memory struct.
 *
 * This function assumes:
 * that no R's of this RT exist yet;
 * otherwise it's an internal error.
 *
 */

scha_errmsg_t
rgm_add_rt(char *rt_name, rgm_rt_t **new_rt)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm_rt_t	*rt = NULL;

	if (rt_name == NULL) {
		res.err_code = SCHA_ERR_RT;
		return (res);
	}

	ucmm_print("RGM", NOGET("rgm_add_rt(), rt_name = %s\n"), rt_name);

	res = rgmcnfg_read_rttable(rt_name, &rt, ZONE);
	if (res.err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("rgmcnfg_read_rttable() failed with <%d>\n"),
		    res.err_code);
		return (res);
	}

	if (!(rt->rt_instl_nodes.is_ALL_value)) {
		if (am_i_president()) {
#ifdef EUROPA_FARM
			ASSERT(0);
#else
			//
			// New president in a new cluster creates the LNI
			// mappings when reading the CCR when zones are
			// supported. We don't propagate the mappings
			// in that case. We do it all at once when we
			// tell joiners to read the CCR.
			//
			// This function is also called the first time the
			// president encounters the RT name. Because scrgadm
			// registers the RT name directly, the president
			// doesn't know about it until now. In that
			// case, we need to propagate the mappings to the
			// slaves.
			//
			// If the ccr_is_read flag is set, then we're just
			// adding an RT, and need to propagate the mappings.
			// Otherwise we're reading the whole CCR, and do not
			// need to propagate.
			//
			create_lni_mappings_from_nodeidlist(
			    rt->rt_instl_nodes.nodeids,
			    Rgm_state->ccr_is_read);
#endif // EUROPA_FARM
		} else {
			//
			// If this is executing on the slave, don't assign
			// mappings -- just set them in the nodelist.
			//
			res = set_lni_in_nodelist(rt->rt_instl_nodes.nodeids);

		}
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
	}

	// if new_rt is non-null => the caller is interested in the newly
	// created RT, hence set it
	if (new_rt != NULL)
		*new_rt = rt;

	// Link up this newly found RT at the head of the global RT linked
	// list in Rgm_state.

	rt->rt_next = Rgm_state->rgm_rtlist;
	Rgm_state->rgm_rtlist = rt;

	check(Rgm_state);
	return (res);
}

/*
 * rgm_delete_rg
 * deletes the given RG (rg_name) from memory.
 *
 * Note that this function is not responsible for deleting the RG from CCR.
 * The President does this.
 *
 * This function assumes:
 * The caller already checks that no R's exist in that RG.
 *
 */

scha_errmsg_t
rgm_delete_rg(char *rg_name)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t	rg, rg_prev;

	if (rg_name == NULL) {
		res.err_code = SCHA_ERR_RG;
		return (res);
	}

	ucmm_print("RGM", NOGET("rgm_delete_rg(), rg_name = %s\n"), rg_name);

	// Locate the in-mem RG
	rg = Rgm_state->rgm_rglist;
	rg_prev = rg;	// Previous in the linked list
	while (rg) {
		if (strcmp(rg_name, rg->rgl_ccr->rg_name) == 0)
			break;	// Gotcha
		rg_prev = rg;
		rg = rg->rgl_next;
	}
	if (rg == NULL) {	// This should never happen
		ucmm_print("RGM", "rgm_remove_rg(): RG <%s> Not found\n",
			rg_name);
		res.err_code = SCHA_ERR_RG;
		return (res);
	}

	// Remove this RG from the rgl_affinitents lists of all resource groups
	unlink_affinitents_for_resource_group(rg);

	if (rg == Rgm_state->rgm_rglist)	// First one
		Rgm_state->rgm_rglist = rg->rgl_next;
	else
		rg_prev->rgl_next = rg->rgl_next;

	free_mdbrgxstate(rg->log_rgxstate);
	free_mdbrgxstate(rg->mdb_rgxback);

	// Free the in-CCR portion
	rgm_free_rg(rg->rgl_ccr);

	// Free the list of "start_failed" timestamps from an RG's
	// in-memory state

	/* CSTYLED */
	for (std::map<rgm::lni_t, rg_xstate>::iterator it =
	    rg->rgl_xstate.begin(); it != rg->rgl_xstate.end(); ++it) {
		rgm_free_timelist(&it->second.rgx_start_fail);
	}

	// free the affinities and affinitents
	free_rgm_affinities(&(rg->rgl_affinities));
	free_rgm_affinities(&(rg->rgl_affinitents));

	// Now free the in-mem portion [was created with new, so use delete]
	delete rg;

	Rgm_state->rgm_total_rgs--;
	check(Rgm_state);
	debug_print_affinities();
	// SC SLM addon start
#ifdef SC_SRM
	// Contact the FED to notify it that a resource group is deleted
	(void) rgm_slm_update_delete_rg(rg_name);
#endif
	// SC SLM addon end
	return (res);
}

/*
 * rgm_delete_r
 * deletes the given R (r_name) from memory.
 * It also reads in the new sequence id (CCR generation number) for the RG
 * and updates RGM's in-memory copy of this value.
 *
 * Note that this function is not responsible for deleting the R from CCR.
 * The President does this.
 *
 */

scha_errmsg_t
rgm_delete_r(char *r_name, char *rg_name)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rlist_p_t	r, r_prev;
	rglist_t	*rg_ptr;

	if (r_name == NULL || rg_name == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		return (res);
	}

	ucmm_print("RGM", NOGET("rgm_delete_r(), r_name = %s, rg_name = %s\n"),
	    r_name, rg_name);

	rg_ptr = rgname_to_rg(rg_name);
	if (rg_ptr == NULL) {
		ucmm_print("RGM", NOGET("rg %s not found\n"), rg_name);
		res.err_code = SCHA_ERR_INTERNAL;
		return (res);
	}

	// Locate the in-mem R
	r = rg_ptr->rgl_resources;
	r_prev = r;	// Previous in the linked list
	while (r) {
		if (strcmp(r_name, r->rl_ccrdata->r_name) == 0)
			break;	// Gotcha
		r_prev = r;
		r = r->rl_next;
	}
	if (r == NULL) {	// This should never happen
		ucmm_print("RGM", "rgm_remove_r(): R <%s> Not found\n",
		    r_name);
		res.err_code = SCHA_ERR_RSRC;
		return (res);
	}

	// Remove this R from the 'r_dependents' lists of all resources
	unlink_dependents_for_resource(r);

	// unlink the R from the internal state structure.
	if (r == rg_ptr->rgl_resources)	// First one
		rg_ptr->rgl_resources = r->rl_next;
	else
		r_prev->rl_next = r->rl_next;

	free_mdbrxstate(r->log_rxstate);
	free_mdbrxstate(r->mdb_rxback);

	if (r->rl_ccrdata->rx_back.mdb_onoffswitch)
		free(r->rl_ccrdata->rx_back.mdb_onoffswitch);
	if (r->rl_ccrdata->rx_back.mdb_monswitch)
		free(r->rl_ccrdata->rx_back.mdb_monswitch);
	free_stdbacks(r->rl_ccrdata->rx_back);
	free_extbacks(r->rl_ccrdata->rx_back);

	rgm_free_resource(r->rl_ccrdata);

	// Free the per-zone portion
	/* CSTYLED */
	for (std::map<rgm::lni_t, r_xstate>::iterator it = 
	    r->rl_xstate.begin(); it != r->rl_xstate.end(); ++it) {
		if (it->second.rx_fm_status_msg != NULL) {
			free(it->second.rx_fm_status_msg);
		}
		if (it->second.rx_rg_restarts != NULL) {
			rgm_free_timelist(&it->second.rx_rg_restarts);
		}
		if (it->second.rx_r_restarts != NULL) {
			rgm_free_timelist(&it->second.rx_r_restarts);
		}
		if (it->second.rx_blocked_restart != NULL) {
			rgm_free_timelist(&it->second.rx_blocked_restart);
		}
	}

	// Now free the in-mem portion
	free_rgm_dependencies(&(r->rl_dependents));
	// memory allocated with new must be freed with delete
	delete(r);
	rg_ptr->rgl_total_rs--;

	//
	// Decrement total number of resources and decrease number
	// of RPC threads if necessary
	//
	Rgm_state->rgm_total_rs--;
	update_nb_rpc_threads();

	// update the RG CCR table sequence id in memory
	if ((res = update_rg_seq_id(rg_ptr)).err_code != SCHA_ERR_NOERR)
		return (res);

	check(Rgm_state);
	// SC SLM addon start
#ifdef SC_SRM
	// Contact the FED to notify it that a resource is deleted
	(void) rgm_slm_update_delete_r(rg_name, r_name);
#endif
	// SC SLM addon end
	return (res);
}


/*
 * rgm_delete_rt
 * deletes the given RT (rt_name) from memory.
 *
 * Note that this function is not responsible for deleting the RT from CCR.
 * The President does this.
 *
 * This function assumes:
 * that no R's exist belonging to this RT.
 * otherwise it's an internal error.
 *
 */

scha_errmsg_t
rgm_delete_rt(char *rt_name)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm_rt_t	*rt, *rt_prev;

	if (rt_name == NULL) {
		res.err_code = SCHA_ERR_RT;
		return (res);
	}

	ucmm_print("RGM", NOGET("rgm_delete_rt(), rt_name = %s\n"), rt_name);


	// Locate the in-mem RT
	rt = Rgm_state->rgm_rtlist;
	rt_prev = rt;	// Previous in the linked list
	while (rt) {
		if (strcmp(rt_name, rt->rt_name) == 0)
			break;	// Gotcha
		rt_prev = rt;
		rt = rt->rt_next;
	}

	if (rt == NULL) {
		// since we read RT's into memory "on-demand",
		// this is OK because, maybe we haven't yet read the RT into
		// memory.
		ucmm_print("RGM", "rgm_delete_rt(): RT <%s> Not found\n",
		    rt_name);
		return (res);
	}
	// unlink the RT from the internal state structure
	if (rt == Rgm_state->rgm_rtlist)	// First one
		Rgm_state->rgm_rtlist = rt->rt_next;
	else
		rt_prev->rt_next = rt->rt_next;

	// Free the in-CCR portion
	rgm_free_rt(rt);

	check(Rgm_state);
	return (res);
}

/*
 * rgm_update_rg
 * updates the CCR portion of the given RG (rg_name),
 * retaining the dynamic state as well as all the pointer (and back pointer)
 * information.
 *
 * Note that this function is not responsible for updating the RG in CCR.
 * The President does this.
 *
 */

scha_errmsg_t
rgm_update_rg(char *rg_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rglist_t *rg;
	rgm_rg_t *rg_ccr;    // the CCR portion of the RG
	namelist_t *saved_affinities = NULL;
	namelist_t *saved_affinitents = NULL;

	// SC SLM addon start
#ifdef SC_SRM
	char *saved_slm_pset_type = NULL;
	char *saved_slm_type = NULL;
	int saved_slm_rg_cpu;
	int saved_slm_rg_cpu_min;
#endif
	// SC SLM addon end

	if (rg_name == NULL) {
		res.err_code = SCHA_ERR_RG;
		return (res);
	}

	ucmm_print("RGM", NOGET("rgm_update_rg(), rg_name = %s\n"), rg_name);

	if ((rg = rgname_to_rg(rg_name)) == NULL) {
		res.err_code = SCHA_ERR_RG;
		return (res);
	}

	//
	// Save the affinities for later reference
	//
	saved_affinities = namelist_copy(rg->rgl_ccr->rg_affinities);
	saved_affinitents = namelist_copy(rg->rgl_ccr->rg_affinitents);
	// SC SLM addons start
#ifdef SC_SRM
	saved_slm_type = strdup(rg->rgl_ccr->rg_slm_type);
	if (saved_slm_type == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto rgm_update_rg_end; //lint !e801
	}
	saved_slm_pset_type = strdup(rg->rgl_ccr->rg_slm_pset_type);
	if (saved_slm_pset_type == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto rgm_update_rg_end; //lint !e801
	}
	saved_slm_rg_cpu = rg->rgl_ccr->rg_slm_cpu;
	saved_slm_rg_cpu_min = rg->rgl_ccr->rg_slm_cpu_min;
#endif
	// SC SLM addon end

	// Free the CCR portion of the RG
	rgm_free_rg(rg->rgl_ccr);

	res = rgmcnfg_read_rgtable(rg_name, &rg_ccr, ZONE);
	if (res.err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("rgmcnfg_read_rgtable() failed with <%d>\n"),
		    res.err_code);
		goto rgm_update_rg_end; //lint !e801
	}

	res = set_lni_in_nodelist(rg_ccr->rg_nodelist);
	if (res.err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("rgmcnfg_read_rgtable() failed with <%d>\n"),
		    res.err_code);
		return (res);
	}

	rg->rgl_ccr = rg_ccr;

	//
	// If our affinities have changed, we need to update the in-memory
	// version of them and the inverse pointers from the other RGs.
	// Note that convert_affinities_list frees the old in-memory
	// affinities lists before storing the new ones.
	//
	if (!namelist_compare(saved_affinities, rg->rgl_ccr->rg_affinities)) {
		// convert the affinities to the in-memory form
		res = convert_affinities_list(rg->rgl_ccr->rg_name,
		    rg->rgl_ccr->rg_affinities, &(rg->rgl_affinities));
		if (res.err_code != SCHA_ERR_NOERR) {
			goto rgm_update_rg_end; //lint !e801
		}
		// update the links
		res = update_affinitents_for_resource_group(rg);
		if (res.err_code != SCHA_ERR_NOERR) {
			ucmm_print("RGM", NOGET(
			    "update_affinitents_for_resource_group <%s> "
			    "failed: <%d>\n"), rg_name, res.err_code);
			goto rgm_update_rg_end; //lint !e801
		}
	}


	if (!namelist_compare(saved_affinitents, rg->rgl_ccr->rg_affinitents)) {
		// convert the affinities to the in-memory form
		res = convert_affinities_list(rg->rgl_ccr->rg_name,
		    rg->rgl_ccr->rg_affinitents, &(rg->rgl_affinitents));
		if (res.err_code != SCHA_ERR_NOERR) {
			goto rgm_update_rg_end; //lint !e801
		}
	}


	// SC SLM addon start
#ifdef SC_SRM
	// Contact the FED to notify it that a SC SLM property has changed
	if (strcmp(saved_slm_pset_type, rg->rgl_ccr->rg_slm_pset_type) != 0 ||
	    strcmp(saved_slm_type, rg->rgl_ccr->rg_slm_type) != 0 ||
	    saved_slm_rg_cpu != rg->rgl_ccr->rg_slm_cpu ||
	    saved_slm_rg_cpu_min != rg->rgl_ccr->rg_slm_cpu_min) {
		(void) rgm_slm_update_rg(rg_name,
		    rg->rgl_ccr->rg_slm_type,
		    rg->rgl_ccr->rg_slm_pset_type,
		    rg->rgl_ccr->rg_slm_cpu,
		    rg->rgl_ccr->rg_slm_cpu_min);
	}
#endif
	// SC SLM addon end

	check(Rgm_state);
	debug_print_affinities();

rgm_update_rg_end:
	// SC SLM addon start
#ifdef SC_SRM
	free(saved_slm_pset_type);
	free(saved_slm_type);
#endif
	// SC SLM addon end
	rgm_free_nlist(saved_affinities);
	return (res);
}

/*
 * rgm_update_r
 * updates the given R (r_name) from memory.
 * It also reads in the new sequence id (CCR generation number) for the RG
 * and updates RGM's in-memory copy of this value.
 *
 * Note that this function is not responsible for updating the R in CCR.
 * The President does this.
 *
 * This function takes care to adjust back references from this R to the
 * containing RG and the RT that its belongs to and also to preserve R state.
 *
 * The in-memory rl_dependents needs to be recomputed if r_dependencies field
 * is updated.
 *
 */

scha_errmsg_t
rgm_update_r(char *r_name, char *rg_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rglist_t *rg = NULL;
	rlist_t  *r = NULL;
	rgm_resource_t	*r_ccr = NULL;
	mdb_rxstate_t *rxstate_logs = NULL, *rxstate_back = NULL;
	int size_logs = 0, size_back = 0;
	rgm_dependencies_t	saved_dependencies;
	char *swtch = NULL;

	if (r_name == NULL || rg_name == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		return (res);
	}

	ucmm_print("RGM", NOGET("rgm_update_r(), r_name = %s, rg_name = %s\n"),
	    r_name, rg_name);

	if ((rg = rgname_to_rg(rg_name)) == NULL) {
		res.err_code = SCHA_ERR_RG;
		return (res);
	}

	if ((r = rname_to_r(rg, r_name)) == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		return (res);
	}

	//
	// Copy the original value of 'r_dependencies' for this
	// resource.  We will compare this saved value with the new
	// value read from the ccr to determine if 'rl_dependents' needs
	// to be re-computed.
	//
	saved_dependencies.dp_strong =
	    rdeplist_copy(r->rl_ccrdata->r_dependencies.dp_strong);
	saved_dependencies.dp_weak =
	    rdeplist_copy(r->rl_ccrdata->r_dependencies.dp_weak);
	saved_dependencies.dp_restart =
	    rdeplist_copy(r->rl_ccrdata->r_dependencies.dp_restart);
	saved_dependencies.dp_offline_restart =
	    rdeplist_copy(r->rl_ccrdata->r_dependencies.dp_offline_restart);

	if (r->rl_ccrdata->rx_back.mdb_onoffswitch)
		free(r->rl_ccrdata->rx_back.mdb_onoffswitch);
	if (r->rl_ccrdata->rx_back.mdb_monswitch)
		free(r->rl_ccrdata->rx_back.mdb_monswitch);
	free_stdbacks(r->rl_ccrdata->rx_back);
	free_extbacks(r->rl_ccrdata->rx_back);

	rgm_free_resource(r->rl_ccrdata);

	res = rgmcnfg_get_resource(r_name, rg_name, &r_ccr, ZONE);
	if (res.err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("rgmcnfg_get_resource() failed with <%d>\n"),
		    res.err_code);
		free_rgm_dependencies(&saved_dependencies);
		return (res);
	}
	//
	// Initialize mdb backup structures.
	//
	r_ccr->rx_back.mdb_onoffswitch = NULL;
	r_ccr->rx_back.mdb_monswitch = NULL;
	r_ccr->rx_back.mdb_stdprops.value = NULL;
	r_ccr->rx_back.mdb_stdprops.size = 0;
	r_ccr->rx_back.mdb_extprops.value = NULL;
	r_ccr->rx_back.mdb_extprops.size = 0;

	//
	// We need to populate the r_switch map since we read from CCR which
	// would have populated only the r_switch_node map. Similarly, r_value
	// map needs to be updated from r_value_node map.
	//
	update_onoff_monitored_switch(r_ccr,
	    rg->rgl_ccr->rg_nodelist);
	update_pn_default_value(r_ccr->r_ext_properties,
	    rg->rgl_ccr->rg_nodelist);

	//
	// Update the mdb backup data.
	//
	if ((res = update_mdb_extback(r_ccr->rx_back,
	    r_ccr->r_ext_properties)).err_code != SCHA_ERR_NOERR) {
		return (res);
	}
	//
	// Update the mdb backup data.
	//
	if ((res = update_mdb_stdback(r_ccr->rx_back,
	    r_ccr->r_properties)).err_code != SCHA_ERR_NOERR) {
		return (res);
	}

	swtch = switchmap_to_string(((rgm_switch_t *)r_ccr->r_onoff_switch)
	    ->r_switch);
	r_ccr->rx_back.mdb_onoffswitch = swtch;

	swtch = switchmap_to_string(((rgm_switch_t *)r_ccr
	    ->r_monitored_switch)->r_switch);
	r_ccr->rx_back.mdb_monswitch = swtch;

	r->rl_ccrdata = r_ccr;

	//
	// Update 'rl_dependents' info if necessary
	//
	// If r_dependencies has been changed,
	// we need to update 'rl_dependents' (for all other resources)
	// from the new 'r_dependencies' for this resource.
	//
	if (!compare_rgm_dependencies(&(r->rl_ccrdata->r_dependencies),
	    &saved_dependencies)) {
		res = update_rl_dependents_for_resource(r);
		if (res.err_code != SCHA_ERR_NOERR) {
			ucmm_print("RGM", NOGET(
			    "update_rl_dependents_for_resource <%s> failed: "
			    "<%d>\n"), r_name, res.err_code);
			free_rgm_dependencies(&saved_dependencies);
			return (res);
		}
	}
	free_rgm_dependencies(&saved_dependencies);

	// update the RG CCR table sequence id in memory
	if ((res = update_rg_seq_id(rg)).err_code != SCHA_ERR_NOERR)
		return (res);

	check(Rgm_state);
	return (res);
}

void
free_extbacks(mdb_rxback_t& rx_back)
{
	int i = 0;
	if (rx_back.mdb_extprops.size == 0)
		return;
	while (i < rx_back.mdb_extprops.size) {
		if (rx_back.mdb_extprops.value[i])
			free(rx_back.mdb_extprops.value[i]);
		++i;
	}
	free(rx_back.mdb_extprops.value);
}

void
free_stdbacks(mdb_rxback_t& rx_back)
{
	int i = 0;
	if (rx_back.mdb_stdprops.size == 0)
		return;
	while (i < rx_back.mdb_stdprops.size) {
		if (rx_back.mdb_stdprops.value[i])
			free(rx_back.mdb_stdprops.value[i]);
		++i;
	}
	free(rx_back.mdb_stdprops.value);
}

/*
 * Input: extension property list.
 * Output: mdb_rxback_t is updated with the extension property
 * data. The property value is stored as <prop_name> = <lni>:<value>;..
 */
scha_errmsg_t
update_mdb_extback(mdb_rxback_t& rx_back, rgm_property_list_t *ext_prop)
{
	int cnt = 0, size = 0;
	rgm_property_list_t *t = ext_prop;
	rgm_property_t *prop = NULL;
	char *nv = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	std::map<rgm::lni_t, char *>::iterator it;
	std::map<rgm::lni_t, char *> rp_val;

	ucmm_print("update_mdb_extback", NOGET(
	    "update_mdb_extback: "));

	if (t == NULL)
		return (res);

	while (t != NULL) {
		cnt++;
		t = t->rpl_next;
	}

	rx_back.mdb_extprops.value = (char **)calloc_nocheck(cnt,
	    sizeof (char *));
	if (rx_back.mdb_extprops.value == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	cnt = 0;
	t = ext_prop;
	while (t != NULL) {
		prop = t->rpl_property;
		rp_val = ((rgm_value_t *)prop->rp_value)->rp_value;
		//
		// If the property is not a pernode property, update the
		// rx_back structure with the extension property values.
		// For a pernode property, rx_back structure update is
		// incomplete here. One more loop is run below which
		// updates the <lni>:<value> fields. The property name
		// alone is updated over here. Also, note that the tuple
		// <property>=[lni:]<value> is updated only if there is atleast
		// one non-NULL value (for any LNI)
		//
		if (!prop->is_per_node) {
			if (rp_val[0] != NULL) {
				//
				// size = strlen of rp_val[0] + prop->rp_key +
				//	1 ("=") + 1 (";") + 1 ('\0')
				//
				size = strlen(rp_val[0]) +
				    strlen(prop->rp_key) + 3;
				rx_back.mdb_extprops.value[cnt] =
				    (char *)malloc(size);
				(void) sprintf(
				    rx_back.mdb_extprops.value[cnt],
				    "%s=%s;", prop->rp_key, rp_val[0]);
			}
		} else {
			for (it = rp_val.begin(); it != rp_val.end();
				++it) {
				if (it->second != NULL) {
					//
					// size = strlen (it->second) +
					// 	10 (LNI) + 1 ("=") + 1 (";") +
					// 	1 (":") + 1 ('\0')
					//
					size += strlen(it->second) + 4 +
					    MAX_LNI_STR;
				}
			}
			if (size != 0) {
				size += strlen(prop->rp_key);
				rx_back.mdb_extprops.value[cnt] =
				    (char *)malloc(size);
				(void) sprintf(
				    rx_back.mdb_extprops.value[cnt],
				    "%s=", prop->rp_key);
			}
		}
		cnt++;
		size = 0;
		t = t->rpl_next;
	}

	rx_back.mdb_extprops.size = cnt;
	t = ext_prop;
	cnt = 0;
	while (t != NULL) {
		prop = t->rpl_property;
		nv = rx_back.mdb_extprops.value[cnt];
		if (nv != NULL) {
			rp_val = ((rgm_value_t *)prop->rp_value)->rp_value;
			if (prop->is_per_node) {
				for (it = rp_val.begin(); it != rp_val.end();
					++it) {
					//
					// Assumes the LNI to be at max a 32
					// bit word.
					//
					if (it->second != NULL) {
						(void) sprintf(nv + strlen(nv),
						    "%d:%s;", it->first,
						    it->second);
					}
				}
			}
			nv = NULL;
		}
		cnt++;
		t = t->rpl_next;
	}
	return (res);
}

/*
 * Input: Standard property list.
 * Output: mdb_rxback_t is updated with the standard property
 * data. The property value is stored as <prop_name> = <<value>;..
 */
scha_errmsg_t
update_mdb_stdback(mdb_rxback_t& rx_back, rgm_property_list_t *std_prop)
{
	int cnt = 0, size = 0;
	rgm_property_list_t *t = std_prop;
	rgm_property_t *prop = NULL;
	char *nv = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	std::map<rgm::lni_t, char *>::iterator it;
	std::map<rgm::lni_t, char *> rp_val;

	ucmm_print("update_mdb_stdback", NOGET(
	    "update_mdb_stdback: "));

	if (t == NULL)
		return (res);

	while (t != NULL) {
		cnt++;
		t = t->rpl_next;
	}

	rx_back.mdb_stdprops.value = (char **)calloc_nocheck(cnt,
	    sizeof (char *));
	if (rx_back.mdb_stdprops.value == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	cnt = 0;
	t = std_prop;
	while (t != NULL) {
		prop = t->rpl_property;
		rp_val = ((rgm_value_t *)prop->rp_value)->rp_value;
		if (rp_val[0] != NULL) {
			size = strlen(rp_val[0]) + strlen(prop->rp_key) + 4;
			rx_back.mdb_stdprops.value[cnt] =
			    (char *)malloc(size);
			(void) sprintf(rx_back.mdb_stdprops.value[cnt++],
			    "%s=%s", prop->rp_key, rp_val[0]);
		}
		size = 0;
		t = t->rpl_next;
	}
	rx_back.mdb_stdprops.size = cnt;
	return (res);
}

/*
 * Input: rgm_switch_t structure.
 * Output: mdb_rxback_t is updated with the Onoff_switch/Monitored_switch
 * data. The property value is stored as <prop_name> = <lni>:<value>;..
 * Note that the switch map contain entries pertaining to enabled nodes only.
 */
char *
switchmap_to_string(std::map<rgm_lni_t, scha_switch_t> swtch)
{
	char *nv;
	std::map<rgm::lni_t, scha_switch_t>::iterator it;

	nv = (char *)malloc(sizeof (char));
	*nv = '\0';
	if (swtch.empty())
		return (nv);

	for (it = swtch.begin(); it != swtch.end();
		++it) {
		//
		// Assumes the LNI to be at max a 32 bit word(stored in
		// 4 character string.
		//
		nv = (char *)realloc(nv, strlen(nv) + 4 + 1 + 1);
		(void) sprintf(nv+strlen(nv), "%d,", it->first);
	}
	// Remove the last comma.
	nv[strlen(nv) - 1] = '\0';
	return (nv);
}

/*
 * rgm_update_rt
 *
 * Updates the installed node list, and rt_system
 *  for RT 'rt_name' in memory only.
 *
 * Also reads in the new sequence id for the RT CCR table.
 * The President does the CCR update.
 * We already hold the lock.
 *
 */

scha_errmsg_t
rgm_update_rt(char *rt_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rgm_rt_t *old_rt;
	rgm_stamp_t new_seqnum;

	if (rt_name == NULL) {
		res.err_code = SCHA_ERR_RT;
		return (res);
	}

	//
	// The only updatable RT fields are instnodelist, rt_system
	//
	// If the RT is already in memory, we need to update it with the
	// new instnodelist.  If it's not already in memory, all we need
	// to do is read in the RT.
	//
	if ((old_rt = find_rt_in_memory(rt_name)) != NULL) {
		//
		// Found RT in memory; now update it.
		// Read in the one property after
		// nuking the old instnodelist,
		// then read in rt_system.
		// Also read in the new sequence number.
		//
		name_t value;

		value = NULL;
		if ((res = get_property_value(rt_name, O_RT,
		    SCHA_INSTALLED_NODES, &value, ZONE)).err_code !=
		    SCHA_ERR_NOERR)
			goto update_rt_fin; //lint !e801

		rgm_free_nodeid_list(old_rt->rt_instl_nodes.nodeids);

		if (value != NULL &&
		    strcmp(value, SCHA_ALL_SPECIAL_VALUE) != 0) {
			old_rt->rt_instl_nodes.is_ALL_value = B_FALSE;
			old_rt->rt_instl_nodes.nodeids = str2nodeidlist(value);

			res = set_lni_in_nodelist(
				old_rt->rt_instl_nodes.nodeids);
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nodeid_list(
					old_rt->rt_instl_nodes.nodeids);
				goto update_rt_fin; //lint !e801
			}

		} else {
			old_rt->rt_instl_nodes.is_ALL_value = B_TRUE;
			old_rt->rt_instl_nodes.nodeids = NULL;
		}

		if (value != NULL)
			delete [] value;

		/*
		 * RT_System Property
		 * Default value is FALSE except if sysdeftype is defined
		 */
		value = NULL;
		if ((res = get_property_value(rt_name, O_RT,
		    SCHA_RT_SYSTEM, &value, ZONE)).err_code != SCHA_ERR_NOERR) {
			// Okay if not set
			if (res.err_code == SCHA_ERR_PROP) {
				if (old_rt->rt_sysdeftype != SYST_NONE) {
					old_rt->rt_system = B_TRUE;
				} else {
					old_rt->rt_system = B_FALSE;
				}
				res.err_code = SCHA_ERR_NOERR;
			} else {
				goto update_rt_fin; //lint !e801
			}
		} else {
			old_rt->rt_system = str2bool(value);
		}
		if (value != NULL)
			delete [] value;

		ucmm_print("RGM",
		    NOGET("RT current seq_id is <%s>\n"),
		    old_rt->rt_stamp);

		// Read in the CCR table sequence id for this RT
		if ((res = get_property_value(rt_name, O_RT,
		    RGM_CCRSTAMP, &new_seqnum, ZONE)).err_code !=
		    SCHA_ERR_NOERR) {
			ucmm_print("RGM",
			    NOGET("get_property_value RT STAMP failed <%d>\n"),
			    res.err_code);
			goto update_rt_fin; //lint !e801
		}

		// update the RT sequence id; free the old value first
		free(old_rt->rt_stamp);

		// crash and burn if strdup fails
		// we strdup because rgm_free_rt() uses 'free'
		old_rt->rt_stamp = strdup(new_seqnum);

		delete [] new_seqnum;

		ucmm_print("RGM",
		    NOGET("RT new seq_id is <%s>\n"),
		    old_rt->rt_stamp);
	} else {
		//
		// RT wasn't in memory, just read it in.
		//
		if ((res = rgm_add_rt(rt_name, NULL)).err_code !=
		    SCHA_ERR_NOERR) {
			// failed to read RT from CCR
			goto update_rt_fin; //lint !e801
		}
	}

	check(Rgm_state);

update_rt_fin:

	return (res);
}


//
// update_rg_seq_id
//
// Updates the in-memory copy of the rg CCR table sequence id
// (CCR generation number).  Called by rgm_add_r(), rgm_delete_r(),
// and rgm_update_r().
//
scha_errmsg_t
update_rg_seq_id(rglist_t *rg_ptr)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rgm_stamp_t	new_seqnum = NULL;

	ucmm_print("RGM",
	    NOGET("RG current seq_id is <%s>\n"), rg_ptr->rgl_ccr->rg_stamp);

	// Read in the CCR table sequence id for this RG
	if ((res = get_property_value(rg_ptr->rgl_ccr->rg_name, O_RG,
	    RGM_CCRSTAMP, &new_seqnum, ZONE)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("get_property_value RG STAMP failed with <%d>\n"),
		    res.err_code);
		return (res);
	}

	// update the RG sequence id; free the old value first
	free(rg_ptr->rgl_ccr->rg_stamp);

	// crash and burn if strdup fails
	// we strdup because rgm_free_rg() uses 'free'
	rg_ptr->rgl_ccr->rg_stamp = strdup(new_seqnum);

	delete [] new_seqnum;

	ucmm_print("RGM",
	    NOGET("RG new seq_id is <%s>\n"), rg_ptr->rgl_ccr->rg_stamp);

	return (res);
}

//
// This function is called from rgm_reconfig on the President.
// It is used to update new RTs into memory from the CCR, that were registered
// after the last RGM reconfiguration; and have not been used elsewhere; so
// that they have still not been loaded into memory.
// NOTE: RT registration is done without going through RGM.
//
scha_errmsg_t
rgm_update_rts()
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	namelist_t *rt_list;
	namelist_t *p;

	ucmm_print("rgm_update_rts()", NOGET("entry\n"));

	// retrieve the names of all the RT's in the CCR.
	res = rgmcnfg_get_rtlist(&rt_list, ZONE);
	if (res.err_code != SCHA_ERR_NOERR) {
		// something is wrong in reading
		ucmm_print("rgmcnfg_get_rtlist()", NOGET("returned:%d\n"),
		    res.err_code);
		return (res);
	}
	p = rt_list;
	while (p != NULL) {
		ASSERT(p->nl_name != NULL);
		//
		// this will read all RTs which haven't already been loaded into
		// memory.
		//
		if (rtname_to_rt(p->nl_name, &res.err_code) == NULL) {
			// null indicates error in reading, if rtname is valid
			break;
		}
		p = p->nl_next;
	}
	rgm_free_nlist(rt_list);
	return (res);
}

/*
 * rgm_init_ccr
 * This function is called from rgm_reconfig on the President and
 * from rgm_comm_impl::rgm_read_ccr() on slaves.
 * It reads in the RG/R/RT's into memory from the CCR and hooks them up
 * in the internal state structure.
 */
scha_errmsg_t
rgm_init_ccr()
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	namelist_t *managed_list;
	namelist_t *unmanaged_list;
	namelist_t *p;

	ucmm_print("rgm_init_ccr()", NOGET("entry\n"));

	//
	// Do not add RTs to the Rgm_state at this time.  RTs are added
	// on-demand by the functions rtname_to_rt() or rgm_update_rt().
	// In this manner, a slave avoids trying to read an RT that was
	// registered very recently, such that the president doesn't know
	// about it yet.  Note that RTs are registered into the CCR directly
	// by the admin commands (through librgm), without going through
	// the RGM president.
	//

	res = rgmcnfg_get_rglist(&managed_list, &unmanaged_list,
	    B_FALSE, ZONE);
	ucmm_print("rgm_init_ccr()", "managed rgs:%p unmanaged rgs:%p",
	    managed_list, unmanaged_list);
	if (res.err_code != SCHA_ERR_NOERR) {
		// something is wrong in reading
		ucmm_print("rgm_init_ccr()", NOGET("rgmcnfg_get_rglist()\n"));
		return (res);
	}

	p = managed_list;
	// read the CCR entry for each managed RG in managed_list
	while (p != NULL) {
		if (p->nl_name == NULL) {
			ucmm_print("rgm_init_ccr()",
			    NOGET("RG name shouldn't be NULL\n"));
			res.err_code = SCHA_ERR_RG;
			return (res);
		}

		res = rgm_add_rg(p->nl_name, NULL);
		if (res.err_code != SCHA_ERR_NOERR) {
			ucmm_print("rgm_init_ccr()",
			    NOGET("rgm_add_rg() failed with <%d>\n"),
			    res.err_code);
			res.err_code = SCHA_ERR_RG;
			return (res);
		}

		if ((res = rgm_get_resources(p->nl_name)).err_code
		    != SCHA_ERR_NOERR)
			return (res);

		p = p->nl_next;
	}
	rgm_free_nlist(managed_list);

	p = unmanaged_list;

	// read the CCR entry for each unmanaged RG in unmanaged_list
	while (p != NULL) {
		if (p->nl_name == NULL) {
			ucmm_print("rgm_init_ccr()",
			    NOGET("RG name shouldn't be NULL\n"));
			res.err_code = SCHA_ERR_RG;
			return (res);
		}

		res = rgm_add_rg(p->nl_name, NULL);
		if (res.err_code != SCHA_ERR_NOERR) {
			ucmm_print("rgm_init_ccr()",
			    NOGET("rgm_add_rg() failed with <%d>\n"),
			    res.err_code);
			res.err_code = SCHA_ERR_RG;
			return (res);
		}

		if ((res = rgm_get_resources(p->nl_name)).err_code
		    != SCHA_ERR_NOERR)
			return (res);

		p = p->nl_next;
	}
	rgm_free_nlist(unmanaged_list);

	rgm_manage_threads_and_swap();

	// Setup "rl_dependents" relation from "r_dependencies"
	if ((res = compute_rl_dependents()).err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("compute_rl_dependents failed: <%d>\n"),
		    res.err_code);
		return (res);
	}

	//
	// Setup the in-memory affinities and affinitents for all
	// resource groups
	//
	if ((res = compute_all_affinities()).err_code != SCHA_ERR_NOERR) {
		ucmm_print("rgm_init_ccr()",
		    NOGET("compute_all_affinities failed: <%d>\n"),
		    res.err_code);
		return (res);
	}

	check(Rgm_state);
	debug_print_affinities();
	ucmm_print("rgm_init_ccr()", "exiting");
	return (res);
}


/*
 * rgm_get_resources
 * This is a static function called only from rgm_init_ccr.
 * This is used to localize the functionality related to reading in the
 * R's belonging to the given RG (rg_name).
 */

static scha_errmsg_t
rgm_get_resources(char *rg_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	namelist_t *r_list = 0;		// resource list
	namelist_t *p;

	ucmm_print("RGM", "rgm_get_resources()\n");

	res = rgmcnfg_get_rsrclist(rg_name, &r_list, ZONE);
	if (res.err_code != SCHA_ERR_NOERR)
		return (res);

	for (p = r_list; p != 0; p = p->nl_next)
		if ((res = rgm_add_r(p->nl_name, rg_name, NULL)).err_code !=
		    SCHA_ERR_NOERR)
			return (res);
	rgm_free_nlist(r_list);
	return (res);
}

//
// rgm_free_timelist
// Free a list of timestamps.  Set the memory pointed to by 'timelistp'
// to NULL.
//
void
rgm_free_timelist(rgm_timelist_t **timelistp)
{
	rgm_prune_timelist(timelistp, 0);
}

//
// rgm_setup
// This function is called only once when the rgmd is starting up.
// It initializes some values in the state structure.
// The global RGM mutex has not yet been initialized;
// we're in the main program and haven't forked any threads yet.
//
void
rgm_setup()
{
	CL_PANIC(Rgm_state == NULL);
	Rgm_state = new rgm_state_t;

	//
	// The President's nodeid is set and used on all nodes.
	//
	Rgm_state->rgm_President = PRES_UNKNOWN;

	Rgm_state->rgm_membership_generation = 0;

	//
	// This value is used to determine whether boot methods need to be
	// run; it also represents "I'm joining".  Initially TRUE on all
	// nodes when rgmd starts; set to FALSE on each joiner after
	// kicking off boot methods.
	//
	Rgm_state->is_thisnode_booting = B_TRUE;

	Rgm_state->rgm_rglist = NULL;
	Rgm_state->rgm_rtlist = NULL;

	//
	// The value representing current membership is set on the
	// President right after the President has been elected.  Slaves
	// set this values on command from the President after the
	// President has finished node death/join processing.
	//
	// The bitmask of current cluster members
	// is passed in from UCMM to the big step routine.
	//
	Rgm_state->current_membership.reset();
#if (SOL_VERSION >= __s10)
	Rgm_state->current_zc_membership.reset();
#endif
	Rgm_state->static_membership.reset();

	//
	// Set all node incarnations to INCN_UNKNOWN.
	// The node incarnation array is passed in to the big
	// reconfiguration step by UCMM.
	//
	for (sol::nodeid_t i = 1; i <= MAXNODES; i++)
		Rgm_state->node_incarnations.members[i] = INCN_UNKNOWN;

	//
	// Set to TRUE after CCR has been read by this node.
	//
	Rgm_state->ccr_is_read = B_FALSE;

	Rgm_state->rgm_total_rgs = 0;

	Rgm_state->rgm_total_rs = 0;

	//
	// This flag is normally FALSE.  It is only set to TRUE if we
	// are the president, while undergoing a reconfiguration.
	//
	Rgm_state->pres_reconfig = B_FALSE;

	//
	// This flag is normally FALSE.  It is only set to TRUE if we
	// are running at the new version, and are calling wake_president
	// from rgm_set_pres.
	//
	Rgm_state->suppress_notification = B_FALSE;

	check(Rgm_state);
}

//
// Debug function to print out the specified rgm state.
//
static void
check(rgm_state_t *state)
{
	rglist_t	*rgp = NULL;
	rlist_t		*rp = NULL;
	nodeidlist_t 	*n_ptr = NULL;

	CL_PANIC(state != NULL);

	for (rgp = state->rgm_rglist; rgp != NULL; rgp = rgp->rgl_next) {

		CL_PANIC(rgp->rgl_ccr != NULL);
		CL_PANIC(rgp->rgl_ccr->rg_name != NULL);

		ucmm_print("RGM", NOGET("---------- rg = %s\n"),
		    rgp->rgl_ccr->rg_name);

		for (n_ptr = rgp->rgl_ccr->rg_nodelist; n_ptr != NULL;
		    n_ptr = n_ptr->nl_next) {
			ucmm_print("RGM", NOGET("rg_node = <%d>\n"),
			    n_ptr->nl_nodeid);
		}

		for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {

			CL_PANIC(rp->rl_ccrdata != NULL);
			CL_PANIC(rp->rl_ccrdata->r_name != NULL);
			CL_PANIC(rp->rl_ccrtype->rt_name != NULL);

			ucmm_print("RGM", NOGET("---------- resource = %s\n"),
			    rp->rl_ccrdata->r_name);
			if (rp->rl_ccrtype->rt_basedir != NULL)
				ucmm_print("RGM",
				    NOGET("----------rt_basedir =%s\n"),
				    rp->rl_ccrtype->rt_basedir);
			ucmm_print("RGM",
			    NOGET("---------- resource type = %s\n"),
			    rp->rl_ccrtype->rt_name);
		}
	}
}

void
debug_print_affinities()
{
	rglist_t	*rgp;
	char *strong_pos = NULL, *strong_neg = NULL;
	char *weak_pos = NULL, *weak_neg = NULL;
	char *failover_deleg = NULL;
	char *rg_name;

	ucmm_print("RGM", NOGET("***DEBUG PRINT AFFINTIES***\n"));
	for (rgp = Rgm_state->rgm_rglist; rgp != NULL; rgp = rgp->rgl_next) {
		CL_PANIC(rgp->rgl_ccr != NULL);
		rg_name =  rgp->rgl_ccr->rg_name;
		CL_PANIC(rg_name != NULL);

		strong_pos = namelist_to_str(
		    rgp->rgl_affinities.ap_strong_pos);
		weak_pos = namelist_to_str(
		    rgp->rgl_affinities.ap_weak_pos);
		strong_neg = namelist_to_str(
		    rgp->rgl_affinities.ap_strong_neg);
		weak_neg = namelist_to_str(
		    rgp->rgl_affinities.ap_weak_neg);
		failover_deleg = namelist_to_str(
		    rgp->rgl_affinities.ap_failover_delegation);

		ucmm_print("RGM", NOGET("RG <%s>: AFFINITES: SP <%s>; WP "
		    "<%s>; SN <%s>; WN <%s>; DELEG <%s>"), rg_name, strong_pos,
		    weak_pos, strong_neg, weak_neg, failover_deleg);

		free(strong_pos);
		strong_pos = NULL;

		free(weak_pos);
		weak_pos = NULL;

		free(strong_neg);
		strong_neg = NULL;

		free(weak_neg);
		weak_neg = NULL;

		free(failover_deleg);
		failover_deleg = NULL;

		strong_pos = namelist_to_str(
		    rgp->rgl_affinitents.ap_strong_pos);
		weak_pos = namelist_to_str(
		    rgp->rgl_affinitents.ap_weak_pos);
		strong_neg = namelist_to_str(
		    rgp->rgl_affinitents.ap_strong_neg);
		weak_neg = namelist_to_str(
		    rgp->rgl_affinitents.ap_weak_neg);
		/* failover delegation is not used for affinitent */

		ucmm_print("RGM", NOGET("RG <%s>: INVERSE (AFFINITENTS): SP "
		    "<%s>; WP <%s>; SN <%s>; WN <%s> "), rg_name, strong_pos,
		    weak_pos, strong_neg, weak_neg);

		free(strong_pos);
		strong_pos = NULL;

		free(weak_pos);
		weak_pos = NULL;

		free(strong_neg);
		strong_neg = NULL;

		free(weak_neg);
		weak_neg = NULL;
	}
}


// SC SLM addon start
#ifdef SC_SRM
//
// The method rgm_slm_update is called to contact fed when any of the
// SLM related properties of a RG are modified.
//
static int
rgm_slm_update_rg(char *rg_name, char *rg_slm_type,
    char *rg_slm_pset_type, int rg_slm_cpu, int rg_slm_cpu_min)
{
	fe_cmd args = {0};
	fe_run_result res;
	int err = 0;

	args.host = getlocalhostname();
	if (args.host == NULL) {
		return (-1);
	}

	args.security = SEC_UNIX_STRONG;

	args.slm_flags = FE_SLM_UPDATE_RG_PROP;
	args.rg_name = rg_name;
	args.r_name = "";
	args.rg_slm_type = rg_slm_type;
	args.rg_slm_pset_type = rg_slm_pset_type;
	args.rg_slm_cpu = rg_slm_cpu;
	args.rg_slm_cpu_min = rg_slm_cpu_min;

	err = fe_slm_update("", &args, &res);

	return (err);
}


//
// The method rgm_slm_update_delete_rg is called to contact fed when
// a resource group is deleted
//
static int
rgm_slm_update_delete_rg(char *rg_name)
{
	fe_cmd args = {0};
	fe_run_result res;
	int err = 0;

	args.host = getlocalhostname();
	if (args.host == NULL) {
		return (-1);
	}

	args.security = SEC_UNIX_STRONG;

	args.slm_flags = FE_SLM_UPDATE_DELETE_RG;
	args.rg_name = rg_name;
	args.r_name = "";
	args.rg_slm_type = "";
	args.rg_slm_pset_type = "";
	args.rg_slm_cpu = 0;
	args.rg_slm_cpu_min = 0;

	err = fe_slm_update("", &args, &res);

	return (err);
}


//
// The method rgm_slm_update_delete_r is called to contact fed when
// a resource of a resource group is deleted
//
static int
rgm_slm_update_delete_r(char *rg_name, char *r_name)
{
	fe_cmd args = {0};
	fe_run_result res;
	int err = 0;

	args.host = getlocalhostname();
	if (args.host == NULL) {
		return (-1);
	}

	args.security = SEC_UNIX_STRONG;

	args.slm_flags = FE_SLM_UPDATE_DELETE_R;
	args.rg_name = rg_name;
	args.r_name = r_name;
	args.rg_slm_type = "";
	args.rg_slm_pset_type = "";
	args.rg_slm_cpu = 0;
	args.rg_slm_cpu_min = 0;

	err = fe_slm_update("", &args, &res);

	return (err);
}
#endif
// SC SLM addon end
