//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_comm_impl.cc	1.142	09/03/03 SMI"

//
// rgm_comm_impl.cc
//
// The functions in this file implement the server side of
// internode communication through the ORB.
//

#include <sys/os.h>
#ifndef EUROPA_FARM
#include <nslib/ns.h>
#include <rgm/rgm_comm_impl.h>
#endif
#include <rgm_state.h>
#include <rgm_proto.h>
#ifndef EUROPA_FARM
#include <rgm_global_res_used.h>
#endif
#include <rgm_threads.h>
#include <rgm/rgm_msg.h>
#include <sys/rsrc_tag.h>

#include <scha.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_cnfg.h>
#include <sys/sc_syslog_msg.h>

/* zone support */
#include <rgm_logical_node.h>
#include <rgm_logical_node_manager.h>
#include <syslog.h>
#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_farm.h>
#endif

#include <rgmx_hook.h>

//
// Initialize the join_lock mutex, which protects access to
// Rgm_state->is_thisnode_booting.  When the rgm_mutex also is to be held,
// the join_lock mutex must be grabbed AFTER grabbing the rgm_mutex.  The
// join_lock mutex is used only by server-side IDL calls
// rgm_am_i_joining() and rgm_run_boot_meths() in this file.
//
// Suppress lint error: man page for mutex_init(3THR) recommends this way
// of initialization, and our compiler is supposed to be able to handle
// union initialization.
// Info(708) [c:27]: union initialization
//
static os::mutex_t join_lock;

static void append_restart_timestamp(rgm_timelist_t **rgtp, const char *s,
    const char *rname, const char *rgname, time_t interval);

static void construct_zone_rg_state_seq(rgm::lni_t lni,
    rgm::rg_state_seq_t &rgseq);
static void construct_zone_r_state_seq(rgm::lni_t lni, const rglist_p_t rglp,
    rgm::r_state_seq_t &rseq);

static void rg_offline_helper(rglist_p_t rglp, rgm::lni_t lni,
    boolean_t &is_changed);

#ifndef EUROPA_FARM
rgm_comm_impl::rgm_comm_impl()
{
}


//
// This function should never get called since the reference held by the
// local name server should always be present. If it does get called, that
// is a fatal error since the local rgmd then becomes inaccessible to other
// rgmds. So, we kill the local rgmd (which kills the local node).
//
void
rgm_comm_impl::_unreferenced(unref_t cookie)
{
	sc_syslog_msg_handle_t handle;

	(void) _last_unref(cookie);
	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");
	//
	// SCMSGS
	// @explanation
	// The low-level cluster machinery has encountered a fatal error. The
	// rgmd will produce a core file and will cause the node to halt or
	// reboot to avoid the possibility of data corruption.
	// @user_action
	// Save a copy of the /var/adm/messages files on all nodes, and of the
	// rgmd core file. Contact your authorized Sun service provider for
	// assistance in diagnosing the problem.
	//
	(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
	    "rgm_comm_impl::_unreferenced() called unexpectedly");
		// Doesn't make sense to internationalize this message.
		// It would only confuse the translator and the sys admin.
	sc_syslog_msg_done(&handle);
	abort();
}


#endif
//
// Slave-side function for idl methods that are invoked on a slave
// by the president and which act upon a particular zone on the slave.
// If the zone is down, or if it is up but its incarnation number is
// greater than that passed in by the president (indicating that the zone
// has died and rebooted *after* the president initiated the IDL call), then
// return true; else return false.
//
// The system_exception call simulates a "node down" IDL exception which
// will be returned in the IDL environment 'env' to the president.
//
// The RGM state mutex must be held by the caller.
//
#ifndef EUROPA_FARM
bool
zone_died(rgm::lni_t lni, rgm::ln_incarnation_t ln_incarnation,
    Environment &env)
#else
bool
zone_died(rgm::lni_t lni, rgm::ln_incarnation_t ln_incarnation,
    rpcidlretvals *retval)
#endif
{
	// Get LN of lni.  Assert that the LN exists.
	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	rgm::ln_incarnation_t incarn = lnNode->getIncarnation();
#ifdef EUROPA_FARM
	*retval = RGMRPC_OK;
#endif

	// always return false for a base-cluster global zone
	if (lnNode->isGlobalZone()) {
		return (false);
	}

	//
	// If zone is shutting_down or down or has rebooted, throw a zone_died
	// exception back to caller.
	//
	if (!lnNode->isUp() || ln_incarnation != incarn) {
#ifdef EUROPA_FARM
		//
		// On a farm node, we cannot propagate the
		// rgm::zone_died exception
		// We return RGMRPC_NODE_DEATH to the caller
		// to notify that the zone is dead
		//
		*retval = RGMRPC_ZONE_DEATH;
#else
		rgm::zone_died *zdep = new rgm::zone_died;
		env.exception(zdep);
#endif
		ucmm_print("RGM",
		    NOGET("zone_died: lni=%d, incarn is %d was %d"),
		    lni, incarn, ln_incarnation);
		return (true);
	} else {
		return (false);
	}
}

//
// Server side for idl_retrieve_lni_mappings().
//
// RGM president retrieves the lni mappings from the slaves.
// Called during reconfiguration step.
// -- Executed by the slaves.
//
#ifdef EUROPA_FARM
bool_t
rgmx_retrieve_lni_mappings_1_svc(void *,
	rgmx_retrieve_lni_mappings_result *rpc_res,
	struct svc_req *)
#else
void
rgm_comm_impl::idl_retrieve_lni_mappings(rgm::lni_mappings_seq_t_out lni_map,
    Environment &)
#endif
{
	sol::nodeid_t		mynodeid = get_local_nodeid();
#ifdef EUROPA_FARM
	rgm::lni_mappings_seq_t *lni_map;
#endif
	rgm::lni_mappings_seq_t *lni_map_p = NULL;

	rgm_lock_state();

	create_sequence_lni_mappings(mynodeid, &lni_map_p);
	lni_map = lni_map_p;

	rgm_unlock_state();

#ifdef EUROPA_FARM

	rpc_res->lni_map.lni_mappings_seq_t_len = lni_map->length();
	rpc_res->lni_map.lni_mappings_seq_t_val = (lni_mappings *)malloc(
			rpc_res->lni_map.lni_mappings_seq_t_len *
			sizeof (lni_mappings));
	for (uint_t i = 0; i < rpc_res->lni_map.lni_mappings_seq_t_len; i++) {
		rgm::lni_mappings &currMapping = (*lni_map)[i];
		rpc_res->lni_map.lni_mappings_seq_t_val[i].idlstr_zonename =
			currMapping.idlstr_zonename;
		rpc_res->lni_map.lni_mappings_seq_t_val[i].lni =
			currMapping.lni;
		rpc_res->lni_map.lni_mappings_seq_t_val[i].state =
			(rgm_ln_state) currMapping.state;
		rpc_res->lni_map.lni_mappings_seq_t_val[i].incarn =
			currMapping.incarn;
	}

	return (TRUE);

#endif
}

#ifndef EUROPA_FARM
//
// When the slave receives a notification about a change in zone
// state, it contacts the president. If needed, the president
// assigns a new lni and returns it to the slave.
//
void
rgm_comm_impl::idl_zone_state_change(sol::nodeid_t node,
    const char *zonename, rgm::rgm_ln_state state,
    rgm::ln_incarnation_t incarn, rgm::lni_t &assigned_lni,
    Environment &)
{
	// default to UNCONFIGURED so comparison to UP for new LN is false
	rgm::rgm_ln_state old_state = rgm::LN_UNCONFIGURED;
	char *ln = NULL;

	rgm_lock_state();

	//
	// the only states we should be seeing are up, down, shutting_down,
	// stuck, or unconfigured.
	//
	CL_PANIC(state == rgm::LN_UP || state == rgm::LN_DOWN ||
	    state == rgm::LN_SHUTTING_DOWN || state == rgm::LN_STUCK ||
	    state == rgm::LN_UNCONFIGURED);

	// first see if we already know about the node
	LogicalNode *lnNode = lnManager->findLogicalNode(node,
	    zonename, &ln);

	if (lnNode != NULL) {
		//
		// we already know about it, just set the state and
		// the incarnation number
		//
		ucmm_print("RGM",
		    NOGET("idl_zone_state_change: found LN %d already "
			"here."), lnNode->getLni());

		//
		// We have to set the current incarnation number before setting
		// the state.  If the state transitions from down to up, the
		// incarnation number will be incremented by setStatus().
		//
		lnNode->setIncarnation(incarn);

		//
		// The LogicalNode itself will decide if the state is changed,
		// and what action needs to be taken
		//
		old_state = lnNode->getStatus();
		lnNode->setStatus(state);

		//
		// For rolling upgrade:  the "pre-ZC" version of
		// process_zone_state_helper() assumes that the president has
		// set the incarnation number to zero.
		// This assumption is bogus (see CR 6620504),
		// but we have to play along with it to avoid failure of
		// the zone_died() function.
		//
		if (!is_rgm_zc_version()) {
			lnNode->setIncarnation(0);
		}
	} else {
		ucmm_print("RGM",
		    NOGET("idl_zone_state_change: "
			"Couldn't find ln for %d:%s\n"), node, zonename);
		//
		// Create a new logical node
		//
		// Set the state of the zone to the state the slave
		// told us about
		//
		lnNode = lnManager->createLogicalNode(node,
		    zonename, state, ln);

		//
		// If the zone is being created in the UP state, we need to
		// increment the incarnation number, because we won't be
		// invoking setStatus() [which would have incremented it].
		//
		// However, if we are running at a pre-ZC version, don't
		// increment it, because it won't be incremented on the
		// slave side either.
		//
		if (is_rgm_zc_version()) {
			if (state == rgm::LN_UP) {
				incarn++;
			}
		}

		// set the incarnation number
		lnNode->setIncarnation(incarn);
	}

	//
	// If zone has just joined the cluster membership,
	// set the booting state here on the president also
	// (the slave sets it independently).
	//
	// Also signal failback threads that a zone has joined.
	//
	if (old_state != state && state == rgm::LN_UP) {
		set_booting_state(lnNode->getLni());
		ucmm_print("idl_zone_state_change",
		    NOGET("broadcast cond_zone_bootdelay"));
		cond_zone_bootdelay.broadcast();
	}

	free(ln);

	// return the lni, in case the slave doesn't know it yet
	assigned_lni = lnNode->getLni();

	lnManager->debug_print();

	rgm_unlock_state();
}


//
// Returns a list of zones on the nodeid. If nodeid is 0, returns zones
// on all the nodes. If exclude_global is true, we return only the names
// of base-cluster non-global zones.
//
// Non-global zones that are in LN_UNCONFIGURED state are not returned.
// This includes non-RG capable zones such as brand lx zones; and zones
// that have never been fully booted to the point of starting all
// cluster-related SMF services.
//
// This is called by the slave and executed on the president.  The caller
// must *not* hold the rgm state lock on the slave side.
//
void
rgm_comm_impl::idl_get_logicalnodes(sol::nodeid_t nodeid, bool exclude_global,
    rgm::idl_string_seq_t_out lns, Environment &)
{
	rgm::idl_string_seq_t *lns_p = new rgm::idl_string_seq_t();
	int size = 0;
	int j;
	LogicalNode *lnNode;
	LogicalNodeManager::iter it;

	rgm_lock_state();

	for (size = 0, it = lnManager->begin(); it != lnManager->end(); ++it) {
		lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);
		if (!lnNode->isConfigured())
			continue;
		if (exclude_global && lnNode->isZcOrGlobalZone())
			continue;
		if (nodeid != (sol::nodeid_t)0 && lnNode->getNodeid() != nodeid)
			continue;
		size++;
	}
	lns_p->length(size);

	for (j = 0, it = lnManager->begin(); it != lnManager->end(); ++it) {
		lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);
		if (!lnNode->isConfigured())
			continue;
		if (exclude_global && lnNode->isZcOrGlobalZone())
			continue;
		if (nodeid != (sol::nodeid_t)0 && lnNode->getNodeid() != nodeid)
			continue;
		(*lns_p)[j++] = new_str(lnNode->getFullName());
	}
	rgm_unlock_state();
	lns = lns_p;
}
#endif // !EUROPA_FARM


//
// President sends the new mappings to the slaves
// Slaves processes this call to create the new mappings.
//
#ifdef EUROPA_FARM
bool_t
rgmx_send_lni_mappings_1_svc(rgmx_send_lni_mappings_args *rpc_args,
    void *, struct svc_req *)
#else
void
rgm_comm_impl::idl_send_lni_mappings(sol::nodeid_t president,
    sol::incarnation_num incarnation,
    const rgm::lni_mappings_seq_t &lni_map, Environment &)
#endif
{
	sol::nodeid_t mynodeid;
	LogicalNode *lnNode = NULL;
	char *ln = NULL;
	uint_t i;

#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	rgm::lni_mappings_seq_t lni_map;
#endif

	rgm_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		rgm_unlock_state();
#ifdef EUROPA_FARM
		return (TRUE);
#else
		return;
#endif
	}

#ifdef EUROPA_FARM
	lni_map.length(rpc_args->lni_map.lni_mappings_seq_t_len);
	for (i = 0; i < lni_map.length(); i++) {
		lni_map[i].idlstr_zonename = rpc_args->lni_map.
			lni_mappings_seq_t_val[i].idlstr_zonename;
		lni_map[i].lni = rpc_args->lni_map.
			lni_mappings_seq_t_val[i].lni;
		lni_map[i].state = (rgm::rgm_ln_state) rpc_args->lni_map.
			lni_mappings_seq_t_val[i].state;
		lni_map[i].incarn = rpc_args->lni_map.
			lni_mappings_seq_t_val[i].incarn;
	}
#endif

	mynodeid = get_local_nodeid();
	for (i = 0; i < lni_map.length(); i++) {

		lnNode = lnManager->findLogicalNode(mynodeid,
		    lni_map[i].idlstr_zonename, &ln);
		//
		// There's a chance that this slave just discovered
		// this zone (because it just booted). In that case,
		// the pres just assigned us an lni
		//
		// Note, this code path might be impossible due to
		// locking on the president side.  We leave the check in
		// for now.
		//
		if (lnNode != NULL) {
			CL_PANIC(lnNode->getLni() == lni_map[i].lni);
			ucmm_print("RGM",
			    NOGET("idl_send_lni_mappings: found LN %d already "
			    "here."), lnNode->getLni());
			free(ln);
			continue;
		}

		//
		// Create a new logical node using the LNI
		// the pres told us about.
		//
		// The state is almost certainly UNCONFIGURED.,
		// but read it out of the mapping structure anyway.
		//
		(void) lnManager->createLogicalNodeFromMapping(mynodeid,
		    lni_map[i].idlstr_zonename, lni_map[i].lni,
		    lni_map[i].state, ln);

		free(ln);
	}
	rgm_unlock_state();

#ifdef EUROPA_FARM
	return (TRUE);
#endif
}


//
// Server side code that sends RG and R state back to the President, who
// requested it via a call to rgm_comm_getstate().
// This method will be called only by a new president during rgm_reconfig.
// We grab the lock to ensure mutual exclusion of accessing the state
// structure.
//
// from the rgm.h file:
// virtual void rgm_xmit_state(node_state_out state,
// Environment &_environment);
//
// State is transmitted for all zones on this slave.
// Only "interesting" state -- non-offline R/RG state, non-empty status --
// is transmitted.  The out parameter 'state' can be an empty sequence if
// no zones exist on the slave, or if no zones contain any interesting state.
//
// The memory for the returned sequence should be freed by the caller.
//
#ifdef EUROPA_FARM
bool_t
rgmx_xmit_state_1_svc(void *, node_state *rpc_state, struct svc_req *)
#else
void
rgm_comm_impl::rgm_xmit_state(rgm::node_state_out state, Environment &)
#endif
{
	uint_t lni_index;		// index into lni sequence
	rgm::lni_t myLni = get_local_nodeid();
#ifdef EUROPA_FARM
	rgm::node_state *state;
	zone_state *zs;
	rg_state *rg;
	r_state *res;
	uint_t zone_index = 0, rg_index = 0, res_index = 0;
#endif
	ucmm_print("RGM", NOGET("rgm_comm_impl: in rgm_xmit_state\n"));

	//
	// Grab the RGM state structure mutex.
	// This method is not called if I'm the President,
	// so don't need to worry about President already holding lock.
	//
	rgm_lock_state();

	//
	// Reset the suppression flag.  Now that the president has
	// fetched all the state, we can start sending incremental
	// notifications again after we release the lock.
	//
	Rgm_state->suppress_notification = B_FALSE;

	//
	// Iterate through all zones on this node.
	// Construct the RG state sequence for each zone.
	// If the zone's RG state is non-empty, add it to the sequence
	// to be returned to the president.  Set the length of the returned
	// sequence to the number of zone elements in the sequence (i.e.,
	// the number of zones that had interesting state).
	//
	//

	LogicalNode *lnNode = lnManager->findObject(myLni);
	CL_PANIC(lnNode != NULL);

	//
	// Allocate storage for the out parameter, state.  Allow enough
	// space for all zones on the local node including the global zone.
	// Length will be reset later to the actual sequence length.
	// The ORB will only transmit the non-empty portion of the sequence.
	//
	rgm::node_state *state_tmp = new rgm::node_state;
	state_tmp->seq.length(1 + lnNode->getLocalZones().size());

	// First get RG state sequence for the global zone
	lni_index = 0;
	construct_zone_rg_state_seq(myLni, state_tmp->seq[lni_index].zseq);
	// Include this zone only if the RG state sequence is non-empty
	if (state_tmp->seq[lni_index].zseq.length() > 0) {
		state_tmp->seq[lni_index].lni = myLni;
		++lni_index;
	}

	// Now iterate through the non-global zones
	std::list<LogicalNode*>::const_iterator it;
	for (it = lnNode->getLocalZones().begin();
	    it != lnNode->getLocalZones().end(); ++it) {
		construct_zone_rg_state_seq((*it)->getLni(),
		    state_tmp->seq[lni_index].zseq);
		if (state_tmp->seq[lni_index].zseq.length() > 0) {
			state_tmp->seq[lni_index].lni = (*it)->getLni();
			++lni_index;
		}
	}

	//
	// Set the length of the sequence to the number of zones actually
	// inserted
	//
	state_tmp->seq.length(lni_index);
	state = state_tmp;

	rgm_unlock_state();

#ifdef EUROPA_FARM
	rpc_state->seq.zone_state_seq_t_len = state->seq.length();
	rpc_state->seq.zone_state_seq_t_val = (zone_state *)malloc(
	    rpc_state->seq.zone_state_seq_t_len * sizeof (zone_state));
	for (zone_index = 0, zs = rpc_state->seq.zone_state_seq_t_val;
		zone_index < rpc_state->seq.zone_state_seq_t_len;
		zone_index++, zs++) {
		zs->lni = state->seq[zone_index].lni;
		zs->zseq.rg_state_seq_t_len = state->
			seq[zone_index].zseq.length();
		zs->zseq.rg_state_seq_t_val = (rg_state *)malloc(
			zs->zseq.rg_state_seq_t_len * sizeof (rg_state));
		for (rg_index = 0, rg = zs->zseq.rg_state_seq_t_val;
			rg_index < zs->zseq.rg_state_seq_t_len;
			rg_index++, rg++) {
			rg->idlstr_rgname = state->seq[zone_index].
				zseq[rg_index].idlstr_rgname;
			rg->rgstate = (rgm_rg_state) state->seq[zone_index].
				zseq[rg_index].rgstate;
			rg->rlist.r_state_seq_t_len = state->seq[zone_index].
				zseq[rg_index].rlist.length();
			rg->rlist.r_state_seq_t_val = (r_state *)
				malloc(rg->rlist.r_state_seq_t_len *
					sizeof (r_state));
			for (res_index = 0, res = rg->rlist.r_state_seq_t_val;
				res_index < rg->rlist.r_state_seq_t_len;
				res_index++, res++) {
				res->idlstr_rname = state->seq[zone_index].
					zseq[rg_index].rlist[res_index].
					idlstr_rname;
				res->rstate = (rgm_r_state) state->
					seq[zone_index].zseq[rg_index].
					rlist[res_index].rstate;
				res->fmstatus = (rgm_r_fm_status) state->
					seq[zone_index].zseq[rg_index].
					rlist[res_index].fmstatus;
				res->idlstr_status_msg =  state->
					seq[zone_index].zseq[rg_index].
					rlist[res_index].idlstr_status_msg;
			}
		}

	}

	return (TRUE);
#endif
}


//
// construct_zone_rg_state_seq
// Subroutine for rgm_xmit_state().  For the given lni, constructs an
// rg_state_seq_t containing only those RGs which have "interesting"
// (non-offline) state in that zone.
//
// Caller must hold the rgm state lock.
//
void
construct_zone_rg_state_seq(rgm::lni_t lni, rgm::rg_state_seq_t &rgseq)
{
	rglist_p_t rglp;
	uint_t rg_index;		// index into RG sequence

	//
	// Set the length of the RG state sequence to allow for a maximum
	// length equal to the total number of existing RGs, both managed
	// and unmanaged.  The length of the sequence is later set to the
	// actual count of RGs in the sequence, which might
	// be as few as zero.  In the ORB invocation, only the current
	// length is marshalled.  For efficiency, Europa should do something
	// similar for its rpc call.
	//
	CL_PANIC(Rgm_state->rgm_total_rgs >= 0);
	rgseq.length((uint_t)Rgm_state->rgm_total_rgs);

	rg_index = 0;
	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		//
		// Only store the current RG in the sequence if it has
		// interesting state, or contains at least one resource
		// with interesting state.
		//
		construct_zone_r_state_seq(lni, rglp, rgseq[rg_index].rlist);
		if (rglp->rgl_xstate[lni].rgx_state == rgm::RG_OFFLINE &&
		    rgseq[rg_index].rlist.length() == 0) {
			continue;
		}

		//
		// We new/strcpy rg_name because the destructor (rgm_stub.cc)
		// for rgm::rg_state, which is a part of the rg_state_seq_t
		// type sequence, deletes rgname.  We don't want
		// to delete values in our internal state structure!
		//
		rgseq[rg_index].idlstr_rgname =
		    new_str((const char *)rglp->rgl_ccr->rg_name);
		rgseq[rg_index].rgstate = rglp->rgl_xstate[lni].rgx_state;

		ucmm_print("RGM",
		    NOGET("rgm_xmit_state: state of <%s> RG is <%s>\n"),
		    rglp->rgl_ccr->rg_name,
		    pr_rgstate(rglp->rgl_xstate[lni].rgx_state));

		rg_index++;
	}

	// Set the length of the sequence to the number of RGs actually inserted
	rgseq.length(rg_index);
}


//
// construct_zone_r_state_seq
//
// Subroutine for construct_zone_rg_state_seq().  For the given lni and RG,
// constructs an r_state_seq_t containing only those Rs in the RG which have
// "interesting" (non-offline) state or status in that zone.
//
// Caller must hold the rgm state lock.
//
void
construct_zone_r_state_seq(rgm::lni_t lni, const rglist_p_t rglp,
    rgm::r_state_seq_t &rseq)
{
	rlist_p_t rp;
	uint_t r_index;			// index into R sequence

	ucmm_print("RGM", NOGET("zone_r_state_seq lni %d, rg %s"), lni,
	    rglp->rgl_ccr->rg_name);

	//
	// Set the length of the R state sequence to allow
	// for a maximum length equal to the number of Rs in the RG.
	// The length of the sequence is later set to the actual number
	// of sequence elements.  In the ORB invocation, only the current
	// length is marshalled.  For efficiency, Europa should do something
	// similar for its rpc call.
	//
	rseq.length((uint_t)rglp->rgl_total_rs);

	r_index = 0;
	for (rp = rglp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		//
		// Only store the current r in the sequence if it has
		// interesting state or status
		//
		if (rp->rl_xstate[lni].rx_state == rgm::R_OFFLINE &&
		    rp->rl_xstate[lni].rx_fm_status == rgm::R_FM_OFFLINE &&
		    rp->rl_xstate[lni].rx_fm_status_msg == NULL) {
			continue;
		}
		//
		// We new/strcpy r_name because the destructor
		// (rgm_stub.cc) for rgm::r_state, which is part of the
		// r_state_seq_t type sequence,  deletes rname.
		// We don't want to delete values in our internal
		// state structure!
		//
		rseq[r_index].idlstr_rname =
		    new_str((const char *)rp->rl_ccrdata->r_name);
		rseq[r_index].rstate = rp->rl_xstate[lni].rx_state;
		ucmm_print("RGM",
		    NOGET("rgm_xmit_state: set state of <%s> r to <%s>"),
		    rp->rl_ccrdata->r_name,
		    pr_rstate(rseq[r_index].rstate));

		rseq[r_index].fmstatus = rp->rl_xstate[lni].rx_fm_status;
		if (rp->rl_xstate[lni].rx_fm_status_msg == NULL) {
			rseq[r_index].idlstr_status_msg = (char *)NULL;
		} else {
			rseq[r_index].idlstr_status_msg =
			    new_str((const char *)
			    rp->rl_xstate[lni].rx_fm_status_msg);
		}
		ucmm_print("RGM", NOGET(
		    "rgm_xmit_state: set FM status of r to <%s>\n"),
		    pr_rfmstatus(rseq[r_index].fmstatus));

		r_index++;
	}

	// Set the length of the sequence to the number of Rs actually inserted
	rseq.length(r_index);
}


//
// Server side code called during reconfiguration if there is a President.
// The President has contacted us to let us know who he is.
//
#ifdef EUROPA_FARM
bool_t
rgmx_set_pres_1_svc(rgmx_set_pres_args *rpc_args,
    void *,
    struct svc_req *)
#else
void
rgm_comm_impl::rgm_set_pres(sol::nodeid_t node,
    sol::incarnation_num incarnation, Environment &)
#endif
{
#ifdef EUROPA_FARM
	sol::nodeid_t node = rpc_args->node;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	char *ipaddr = rpc_args->ipaddr;
	sol::incarnation_num old_incarnation;
	boolean_t new_pres = B_FALSE;
#endif
	sol::nodeid_t old_pres;
	sol::incarnation_num old_incarnation;

#ifndef EUROPA_FARM
	FAULT_POINT_FOR_ORPHAN_ORB_CALL("rgm_set_pres", node, incarnation);
#endif
	// acquire lock on state structure before setting our idea
	// of the President node
	rgm_lock_state();

#ifdef EUROPA_FARM
	old_incarnation = Rgm_state->node_incarnations.members[node];
	Rgm_state->node_incarnations.members[node] = incarnation;
	ucmm_print("RGM",
		NOGET("rgm_set_pres: Node %d, incarnation %ld; "
		"current incarnation %ld\n"), node,
		old_incarnation, incarnation);
#endif
	old_incarnation = Rgm_state->node_incarnations.members[node];
	Rgm_state->node_incarnations.members[node] = incarnation;
	ucmm_print("RGM",
		NOGET("rgm_set_pres: Node %d, incarnation %ld; "
		"current incarnation %ld\n"), node,
		old_incarnation, incarnation);
	if (stale_idl_call(node, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		rgm_unlock_state();
#ifdef EUROPA_FARM
		return (TRUE);
#else
		return;
#endif
	}

	//
	// If the new president is different from the old one, and
	// we're not a joining node, we make a wake_president call
	// on the president.
	//
	// Note that we don't need to worry about a new president being
	// the same as the old (but really being new), because that would
	// imply a total cluster reboot, which would mean we are booting too.
	//
	// Note that we need to set the president node first, because
	// wake_president uses it.
	//
	// Note that the president will immediately turn around and call
	// rgm_xmit_state on us.  That's why we need to release the lock
	// before making the call.  In order to prevent out-of-order
	// notifications from being sent to the president, we set the
	// suppress_notification flag, which is reset in rgm_xmit_state,
	// which is guarenteed to be called as a result of the wake_president
	// call, as long as there is no node death.  If the president dies
	// before calling rgm_xmit_state, there will be another reconfiguration
	// with a new president, so rgm_xmit_state is guarenteed to be called
	// eventually; or if we are the new president, rgm_xmit_state will never
	// be called, but the suppress_notification flag is never used from
	// that point onward.
	//
	sol::nodeid_t mynode = get_local_nodeid();
	old_pres = Rgm_state->rgm_President;
	Rgm_state->rgm_President = node;

#ifdef EUROPA_FARM
	//
	// For a Farm node, we have to consider the case where the new
	// president is the same as the old one (but really being new),
	// because we support the Farm being there without any Server
	// node (no president).
	//
	// Therefore, when the Europa Server restarts, the new president
	// node id can be the same as the last known president of the Farm.
	// So the president node id is no more sufficient to determine if
	// a new president is coming. We use the incarnation number to
	// to check if this is a new president.
	//
	// In that case, we need to wake the president, so it will call us
	// back with rgm_xmit_state.
	//
	ucmm_print("RGM",
	    NOGET("rgm_set_pres: Node %d, current president %d\n"),
		node, old_pres);

	if (old_pres != node) {
		new_pres = B_TRUE;
	} else {
		if (old_incarnation != incarnation) {
			new_pres = B_TRUE;
		}
	}

	//
	// Update our reference to the president.
	//
	if (new_pres)
		rgmx_setpres(ipaddr);

	if ((new_pres) &&
	    !Rgm_state->is_thisnode_booting) {
#else
	if (old_pres != node &&
	    !Rgm_state->is_thisnode_booting) {
#endif
		Rgm_state->suppress_notification = B_TRUE;
		rgm_unlock_state();
		(void) wake_president(mynode, B_TRUE);
	} else {
		rgm_unlock_state();
	}
#ifdef EUROPA_FARM
	return (TRUE);
#endif
}


//
// rgm_chg_mastery
// Slave-side of rgm_change_mastery().
// May be called when the slave and president are the same node.
//
// If onlist and offlist are both empty, this is a special case for node
// evacuation, indicating that all RGs should be switched offline in
// the indicated zone.
//
#ifdef EUROPA_FARM
bool_t
rgmx_chg_mastery_1_svc(rgmx_chg_mastery_args *rpc_args,
    rgmx_chg_mastery_result *rpc_res,
    struct svc_req *)
#else
void
rgm_comm_impl::rgm_chg_mastery(sol::nodeid_t president,
    sol::incarnation_num incarnation, rgm::lni_t lni,
    rgm::ln_incarnation_t ln_incarnation,
    const rgm::idl_string_seq_t &onlist,
    const rgm::idl_string_seq_t &offlist, Environment &env)
#endif
{
#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	rgm::lni_t lni = rpc_args->lni;
	rgm::ln_incarnation_t ln_incarnation = rpc_args->ln_incarnation;
	rgm::idl_string_seq_t onlist;
	rgm::idl_string_seq_t offlist;
	idlretval_t retval;

	array_to_strseq_2(rpc_args->onlist, onlist);
	array_to_strseq_2(rpc_args->offlist, offlist);
#endif
	uint_t i;
	uint_t length;
	boolean_t is_changed = B_FALSE;
	rglist_p_t rglp;
	char *c;

	ucmm_print("RGM", NOGET("in rgm_chg_mastery, lni %d"), lni);

#ifndef EUROPA_FARM
	FAULT_POINT_FOR_ORPHAN_ORB_CALL("rgm_chg_mastery", president,
	    incarnation);
#endif
	// error if RG in both on and off lists
	// (caller should have checked for this)

	length = onlist.length();
	ucmm_print("RGM", NOGET("rgm_chg_mastery onlist is length <%d>\n"),
	    length);
	ucmm_print("RGM", NOGET("rgm_chg_mastery offlist is length <%d>\n"),
	    offlist.length());

	// lock the state structure while we access it and update RG state
	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		slave_unlock_state();
#ifdef EUROPA_FARM
		rpc_res->retval = RGMRPC_NOREF;
		return (TRUE);
#else
		return;
#endif
	}

	//
	// If zone died and we are bringing RGs online, return early with IDL
	// exception.
	//
	// However, if we are only bringing RGs offline or evacuating,
	// then we proceed so that the RGs will be able to move to OFFLINE or
	// PENDING_OFFLINE state in the dead/dying zone.  This is especially
	// important to avoid deadlock when zone evacuation is invoked by
	// zc_halt.
	//
	if (length > 0) {
#ifndef EUROPA_FARM
		if (zone_died(lni, ln_incarnation, env)) {
			slave_unlock_state();
			return;
		}
#else
		if (zone_died(lni, ln_incarnation, &(rpc_res->retval))) {
			//
			// Sends back RGMIDL_NODE_DEATH so that the caller
			// can take the appropriate decision
			//
			slave_unlock_state();
			return (TRUE);
		}
#endif
	}

	for (i = 0; i < length; i++) {
		ucmm_print("RGM", NOGET("rgm_chg_mastery ON <%d>\n"), i);

		//
		// We make a copy of this string to satisfy the C++ compiler,
		// which otherwise complains "Formal argument rgname of
		// type char* const  in call to rgname_to_rg(char* const )
		// is being passed const char*."  We need to free the memory
		// when we're finished with it.
		//
		c = strdup((const char *)onlist[i]);
		ucmm_print("RGM", NOGET("chg_mastery ON <%s>\n"), c);
		// Since the validity of the rgname has already been
		// checked before calling this function, we only CL_PANIC
		// here to make sure RG pointer is OK.
		rglp = rgname_to_rg(c);
		CL_PANIC(rglp);
		free(c);

		//
		// Check current RG state.  Even if an error is found which
		// would prevent bringing this RG online, or if it is
		// already online or pending online,
		// we just skip this RG and go on to the next one.
		//
		switch (rglp->rgl_xstate[lni].rgx_state) {
		case rgm::RG_ONLINE:
		case rgm::RG_ON_PENDING_R_RESTART:
		case rgm::RG_PENDING_ONLINE:
		case rgm::RG_PENDING_ONLINE_BLOCKED:
		case rgm::RG_PENDING_ON_STARTED:
		case rgm::RG_PENDING_OFF_START_FAILED:
			//
			// Pres. shouldn't have ordered the RG
			// started if it's already in one of these
			// states.  The RG shouldn't have been able to
			// transition to one of these states from
			// offline or pending_offline on the slave,
			// without the president knowing about it.
			//
			ucmm_print("RGM",
			    NOGET("rgm_chg_mastery(): RG <%s> in"
			    " onlist already has state <%s>\n"),
			    rglp->rgl_ccr->rg_name, pr_rgstate(
			    rglp->rgl_xstate[lni].rgx_state));

			break;

		case rgm::RG_PENDING_OFF_STOP_FAILED:
		case rgm::RG_ERROR_STOP_FAILED:
			//
			// Pres. thought the RG was PENDING_OFFLINE
			// on the slave, but a failure occurred
			// after the order was issued.
			// For slave nodes, the president will deal
			// with this in the wake_president() code path.
			// When the slave is the president itself,
			// the president's state machine will bust the
			// switch and call rebalance() directly when
			// the RG moves into this state.
			//
			ucmm_print("RGM",
			    NOGET("rgm_chg_mastery(): RG <%s> in"
			    " onlist is *_STOP_FAILED\n"),
			    rglp->rgl_ccr->rg_name);
			break;

		case rgm::RG_OFFLINE_START_FAILED:
			//
			// The president has decided to restart the
			// RG even though previous start
			// attempt failed.
			//
			/*FALLTHRU*/
		case rgm::RG_OFFLINE:
		case rgm::RG_OFF_BOOTED:
		case rgm::RG_PENDING_OFFLINE:
			// set RG state to PENDING_ONLINE
			set_rgx_state(lni, rglp,
			    rgm::RG_PENDING_ONLINE);
			is_changed = B_TRUE;
			ucmm_print("RGM", "onlist: is_changed\n");
			break;

		//
		// These are errors.  The RG is busy,
		// so the president shouldn't be issuing
		// orders to slaves to change mastery of it.
		//
		case rgm::RG_ON_PENDING_METHODS:
		case rgm::RG_ON_PENDING_DISABLED:
		case rgm::RG_ON_PENDING_MON_DISABLED:
		case rgm::RG_OFF_PENDING_METHODS:
		case rgm::RG_OFF_PENDING_BOOT:
			ucmm_print("RGM",
			    NOGET("rgm_chg_mastery() called on "
			    "busy RG: state <%s>, RG <%s>\n"),
			    pr_rgstate(rglp->
			    rgl_xstate[lni].rgx_state),
			    rglp->rgl_ccr->rg_name);
			break;
		}
	}

	if (onlist.length() == 0 && offlist.length() == 0) {
		//
		// This means all RGs should be evacuated.
		//
		// Iterate through all RGs, filtering out those which are
		// unmanaged or which don't contain lni in nodelist.
		//
		for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
		    rglp = rglp->rgl_next) {

			if (rglp->rgl_ccr->rg_unmanaged) {
				continue;
			}

			if (!nodeidlist_contains_lni(rglp->rgl_ccr->rg_nodelist,
			    lni)) {
				continue;
			}
			ucmm_print("RGM", "chg_mastery EVAC <%s>\n",
			    rglp->rgl_ccr->rg_name);

			rg_offline_helper(rglp, lni, is_changed);
		}
	} else {
		//
		// Iterate over offlist.
		//
		length = offlist.length();
		for (i = 0; i < length; i++) {
			//
			// We make a copy of this string to satisfy the C++
			// compiler, which otherwise complains "Formal argument
			// rgname of type char* const  in call to
			// rgname_to_rg(char* const ) is being passed const
			// char*."  We need to free the memory
			// when we're finished with it.
			//
			c = strdup((const char *)offlist[i]);
			ucmm_print("RGM", "chg_mastery OFF <%s>\n", c);
			//
			// Since the validity of the rgname has already been
			// checked before calling this function, we only
			// CL_PANIC here to make sure RG pointer is OK.
			//
			rglp = rgname_to_rg(c);
			CL_PANIC(rglp);
			free(c);
			rg_offline_helper(rglp, lni, is_changed);
		}
	}


	if (is_changed) {
		// call run_state in new thread
		ucmm_print("RGM", NOGET("rgm_chg_mastery: run new thread\n"));
		run_state_machine(lni, "rgm_chg_mastery");
	} else {
		//
		// Request failed, or desired mastery already exists, or
		// state was reset from OFFLINE_START_FAILED or OFF_BOOTED
		// to OFFLINE.
		// Log a debug message and wake the president.
		//
		ucmm_print("RGM",
		    NOGET("rgm_chg_mastery: no need to run state machine, "
		    "waking pres\n"));
		(void) wake_president(lni);
	}
	slave_unlock_state();
#ifdef EUROPA_FARM
	rpc_res->retval = RGMRPC_OK;
	return (TRUE);
#endif
}


//
// Helper function to switch an RG offline if it is not already offline.
// RG rglp is switched offline on zone 'lni'.  If the RG's state is changed,
// is_changed is set to B_TRUE.  The caller must then run the state machine.
//
// Called by rgm_chg_mastery.  Caller must hold the state lock.
//
static void
rg_offline_helper(rglist_p_t rglp, rgm::lni_t lni, boolean_t &is_changed)
{
	switch (rglp->rgl_xstate[lni].rgx_state) {

	case rgm::RG_OFFLINE:
	// following cases can occur during node evacuation
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_PENDING_OFF_START_FAILED:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_BOOT:
		//
		// the RG is already offline or pending offline;
		// just skip it.
		//
		ucmm_print("RGM",
		    NOGET("rgm_chg_mastery(): RG <%s> in "
		    "offlist already has state <%s>\n"),
		    rglp->rgl_ccr->rg_name, pr_rgstate(
		    rglp->rgl_xstate[lni].rgx_state));
		break;

	case rgm::RG_OFFLINE_START_FAILED:
	case rgm::RG_OFF_BOOTED:
		//
		// President is resetting the RG to OFFLINE.
		// President will decide when or if to attempt
		// starting the RG on this node.
		//
		set_rgx_state(lni, rglp, rgm::RG_OFFLINE);
		break;

	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
		//
		// User may have cleared the stop-failed
		// condition on resources; there is no harm
		// in trying again.
		//
	case rgm::RG_ONLINE:
	case rgm::RG_ON_PENDING_R_RESTART:
		// state machine knows how to handle this
	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
	case rgm::RG_PENDING_ON_STARTED:
		//
		// We are allowed to switch directly to
		// PENDING_OFFLINE from a PENDING_DISABLED
		// state.  This can occur as a result of
		// affinities dragging RGs off a node.
		//
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
		// set RG state to PENDING_OFFLINE
		set_rgx_state(lni, rglp,
		    rgm::RG_PENDING_OFFLINE);
		is_changed = B_TRUE;
		ucmm_print("RGM", "offlist: is_changed\n");
		break;

	case rgm::RG_ON_PENDING_METHODS:
		//
		// We need to wait for the methods to finish
		// running before we stop the RG.  Set
		// the rgx_postponed_stop flag, and let the
		// state machine handle it.  We have the lock,
		// so we know that the state machine is
		// not currently running.  We also know
		// that when the method in question
		// finishes running, the state machine will
		// be run again.  Thus, we do not need to
		// run the state machine now.  The state
		// machine thread that will handle the
		// method that finishes will be guaranteed
		// to catch the postponed_stop flag.
		//
		rglp->rgl_xstate[lni].rgx_postponed_stop = B_TRUE;
		ucmm_print("RGM", NOGET("rgm_chg_mastery: set"
		    "rgx_postponed_stop flag to B_TRUE.\n"));
		break;
	}
}


//
// rgm_r_restart
// Slave-side of rgm_resource_restart().
// May be called when the slave and president are the same node.
//
// Requests the slave to restart a resource, or notifies the slave that
// the RG is being restarted by this resource.
//
// If rrflag is RGM_RRF_RG, then this is an RG restart.  In this case, the
// president will drive the restart in idl_scha_control_restart() by making
// change_mastery calls.  The president calls this function only to notify
// the slave to update the RG restart history for this resource.
//
// If rrflag is RGM_RRF_R or RGM_RRF_TR, then this is a resource restart
// initiated by a scha_control call or by a triggered restart dependency,
// respectively.  The call to rgm_r_restart tells the slave to restart the
// resource as well as updating the restart history on the slave.
//
// If rrflag is RGM_RRF_RIR, then this is a "RESOURCE_IS_RESTARTING" call.
// Just update the resource_restart history on the slave but don't restart
// the resource.
//
#ifdef EUROPA_FARM
bool_t
rgmx_r_restart_1_svc(rgmx_r_restart_args *rpc_args,
    rgmx_r_restart_result *rpc_res,
    struct svc_req *)
#else
void
rgm_comm_impl::rgm_r_restart(sol::nodeid_t president,
    sol::incarnation_num incarnation, rgm::lni_t lni,
    rgm::ln_incarnation_t ln_incarnation, rgm::rgm_restart_flag rrflag,
    const char *R_name, Environment &env)
#endif
{
#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	rgm::lni_t lni = rpc_args->lni;
	rgm::ln_incarnation_t ln_incarnation = rpc_args->ln_incarnation;
	rgm::rgm_restart_flag rrflag = (rgm::rgm_restart_flag) rpc_args->rrflag;
	char *R_name = rpc_args->R_name;

	rpc_res->retval = RGMRPC_OK;
#endif
	rlist_p_t rp;
	rglist_p_t rgp;
	time_t retry_interval;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;

	ucmm_print("RGM", NOGET("in rgm_r_restart; president %d rrflag %d"
	    " R_name %s incarnation %d lni %d"),
	    president, rrflag, R_name, incarnation, lni);

#ifndef EUROPA_FARM
	FAULT_POINT_FOR_ORPHAN_ORB_CALL("rgm_r_restart", president,
	    incarnation);
#endif

	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		slave_unlock_state();
#ifdef EUROPA_FARM
		rpc_res->retval = RGMRPC_NOREF;
		return (TRUE);
#else
		return;
#endif
	}

	// Return early with IDL exception if zone died.
#ifndef EUROPA_FARM
	if (zone_died(lni, ln_incarnation, env)) {
		slave_unlock_state();
		return;
	}
#else
	if (zone_died(lni, ln_incarnation, &rpc_res->retval)) {
		//
		// Sends back RGMIDL_NODE_DEATH so that the caller
		// can take appropriate decision
		//
		slave_unlock_state();
		return (TRUE);
	}
#endif

	rp = rname_to_r(NULL, R_name);
	CL_PANIC(rp != NULL);
	rgp = rp->rl_rg;
	CL_PANIC(rgp != NULL);

	// Fetch Retry_interval for this resource
	res = get_retry_interval(rp, &retry_interval);

	//
	// The 'num_restarts' queries are valid only if Retry_interval is
	// declared for the resource type.  Update the resource_restart or
	// rg_restart history and truncate it to the Retry_interval.
	//
	if (rrflag == rgm::RGM_RRF_RG) {
		//
		// It's an RG restart-- president will do the actual restart
		// in idl_scha_control_restart() by calling change_mastery().
		// Update the resource's "rg_restarts" history and return.
		//
		// The president has already checked for the presence of
		// the Retry_interval so the following "if" should always
		// succeed.
		//
		if (res.err_code == SCHA_ERR_NOERR) {
			append_restart_timestamp(
			    &rp->rl_xstate[lni].rx_rg_restarts, "RG",
			    R_name, rgp->rgl_ccr->rg_name, retry_interval);
		}
		slave_unlock_state();
#ifdef EUROPA_FARM
		return (TRUE);
#else
		return;
#endif
	} else if (rrflag == rgm::RGM_RRF_RIR) {
		//
		// This is a "RESOURCE_IS_RESTARTING".  Update the resource's
		// "resource_restarts" history and return.
		//
		// The president has already checked for the presence of
		// the Retry_interval so the following "if" should always
		// succeed.
		//
		if (res.err_code == SCHA_ERR_NOERR) {
			append_restart_timestamp(
			    &rp->rl_xstate[lni].rx_r_restarts, "resource",
			    R_name, rgp->rgl_ccr->rg_name, retry_interval);
		}
		slave_unlock_state();
#ifdef EUROPA_FARM
		return (TRUE);
#else
		return;
#endif
	}

	//
	// This is a resource_restart.  If this resource is already
	// in the 'stopping' phase of restarting, just return.
	// (Note, if the resource is in the 'starting' phase of
	// restarting, we will execute the restart, which will stop the
	// resource and start it again.)
	//
	if (rp->rl_xstate[lni].rx_restart_flg == rgm::RR_STOPPING) {
		ucmm_print("RGM", NOGET(
		    "rgm_r_restart: resource %s already restarting"), R_name);
		slave_unlock_state();
#ifdef EUROPA_FARM
		return (TRUE);
#else
		return;
#endif
	}

	//
	// If the resource is already offline, there is no need to restart it.
	//
	if (is_offline_r_state(rp->rl_xstate[lni].rx_state)) {
		ucmm_print("RGM", NOGET(
		    "rgm_r_restart: resource %s is already offline"), R_name);
		slave_unlock_state();
#ifdef EUROPA_FARM
		rpc_res->retval = RGMRPC_OK;
		return (TRUE);
#else
		return;
#endif
	}

	// If this is a scha_control initiated restart, update the
	// resource's resource_restart history.
	if (rrflag == rgm::RGM_RRF_R && res.err_code == SCHA_ERR_NOERR) {
		append_restart_timestamp(&rp->rl_xstate[lni].rx_r_restarts,
		    "resource", R_name, rgp->rgl_ccr->rg_name, retry_interval);
	}

	//
	// Check if the RG is in an "online-ish" state that permits resource
	// restarts:
	//

	switch (rgp->rgl_xstate[lni].rgx_state) {

	//
	// If the RG is ONLINE or PENDING_ONLINE_BLOCKED, change it to
	// ON_PENDING_R_RESTART.
	//
	case rgm::RG_ONLINE:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
		set_rgx_state(lni, rgp, rgm::RG_ON_PENDING_R_RESTART);
		break;

	//
	// The state machine knows how to restart a resource in any of the
	// following RG states.
	//
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
	case rgm::RG_ON_PENDING_R_RESTART:
	case rgm::RG_ON_PENDING_METHODS:
		break;

	// A restart is not permitted in any of the following RG states.
	case rgm::RG_PENDING_OFF_START_FAILED:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_OFFLINE_START_FAILED:
	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_OFFLINE:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_BOOT:
		ucmm_print("RGM", NOGET(
		    "rgm_r_restart: resource <%s> restart rejected because RG "
		    "<%s> is in state <%s>"), R_name, rgp->rgl_ccr->rg_name,
		    pr_rgstate(rgp->rgl_xstate[lni].rgx_state));
		slave_unlock_state();
#ifdef EUROPA_FARM
		rpc_res->retval = RGMRPC_OK;
		return (TRUE);
#else
		return;
#endif

	default:
		// This should not occur
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// An internal error has occurred. The rgmd will produce a
		// core file and will force the node to halt or reboot to
		// avoid the possibility of data corruption.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes,
		// and of the rgmd core file(s). Contact your authorized Sun
		// service provider for assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: %s: internal error: bad state <%d> for "
		    "resource group <%s>", "rgm_r_restart",
		    rgp->rgl_xstate[lni].rgx_state,
		    rgp->rgl_ccr->rg_name);
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}

	// Set restart flag on the resource
	rp->rl_xstate[lni].rx_restart_flg = rgm::RR_STOPPING;

	slave_unlock_state();

	run_state_machine(lni, "rgm_r_restart");
#ifdef EUROPA_FARM
	rpc_res->retval = RGMRPC_OK;
	return (TRUE);
#endif
}


#if (SOL_VERSION >= __s10)
void
rgm_comm_impl::rgm_force_rg_offline(sol::nodeid_t president,
    sol::incarnation_num incarnation, rgm::lni_t lni,
    rgm::ln_incarnation_t ln_incarnation,
    const char *RG_name, Environment &env)
{
	rlist_p_t rp;
	rglist_p_t rgp;
	time_t retry_interval;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;

	ucmm_print("rgm_force_offline", NOGET("in rgm_force_offline; "
	    "president %d RG_name %s incarnation %d lni %d"),
	    president, RG_name, incarnation, lni);
#ifndef EUROPA_FARM
	FAULT_POINT_FOR_ORPHAN_ORB_CALL("rgm_r_restart", president,
	    incarnation);
#endif
	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		slave_unlock_state();
		return;
	}

	// Return early with IDL exception if zone died.
#ifndef EUROPA_FARM
	if (zone_died(lni, ln_incarnation, env)) {
		slave_unlock_state();
		return;
	}
#else
	if (zone_died(lni, ln_incarnation, &rpc_res->retval)) {
		//
		// Sends back RGMIDL_NODE_DEATH so that the caller
		// can take appropriate decision
		//
		slave_unlock_state();
		return (TRUE);
	}
#endif

	rgp = rgname_to_rg(RG_name);
	CL_PANIC(rgp != NULL);

	rgm::rgm_rg_state state = rgp->rgl_xstate[lni].rgx_state;
	if (state == rgm::RG_OFFLINE || state ==
	    rgm::RG_PENDING_OFFLINE) {
		ucmm_print("rgm_force_offline", NOGET(
		    "resource group %s is already offline. "
		    "not calling state machine"),
		    RG_name);
		slave_unlock_state();
		return;
	} else {
		ucmm_print("rgm_force_offline", NOGET(
		    "resource group %s set to pending offline. "
		    "calling state machine"),
		    RG_name);
		set_rgx_state(lni, rgp,
		    rgm::RG_PENDING_OFFLINE);
		slave_unlock_state();
		run_state_machine(lni, "rgm_force_offline");
	}
}
#endif

//
// rgm_ack_start
// -------------
//
// If the current state of the specified resource on the given zone[LNI]
// is R_JUST_STARTED, sets the state to R_ONLINE_UNMON and runs the state
// machine.  Note that the call is idempotent (nothing will happen if
// the state is not R_JUST_STARTED).
// If the resource is a proxy resource, then set the state to R_ONLINE
// instead of R_ONLINE_UNMON and run the state machine.
//
// Grabs the lock to do the tasks, releasing it before returning.
//
#ifdef EUROPA_FARM
bool_t
rgmx_ack_start_1_svc(rgmx_ack_start_args *rpc_args,
    rgmx_ack_start_result *rpc_res,
    struct svc_req *)
#else
void
rgm_comm_impl::rgm_ack_start(sol::nodeid_t president,
    sol::incarnation_num incarnation, rgm::lni_t lni,
    rgm::ln_incarnation_t ln_incarnation, const char *R_name,
    Environment &)
#endif
{
#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	rgm::lni_t lni = rpc_args->lni;
	rgm::ln_incarnation_t ln_incarnation = rpc_args->ln_incarnation;
	char *R_name = rpc_args->R_name;

	rpc_res->retval = RGMRPC_OK;
#endif
	rlist_p_t rp;

	ucmm_print("RGM", NOGET("in rgm_ack_start; president %d"
	    " R_name %s incarnation %d lni %d"),
	    president, R_name, incarnation, lni);

	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		slave_unlock_state();
#ifdef EUROPA_FARM
		rpc_res->retval = RGMRPC_NOREF;
		return (TRUE);
#else
		return;
#endif
	}

	rp = rname_to_r(NULL, R_name);
	CL_PANIC(rp != NULL);

	//
	// verify that the state really is R_JUST_STARTED.  It's possible
	// that a new president is repeating this call.
	//
	if (rp->rl_ccrtype->rt_proxy == B_TRUE) {
		if (rp->rl_xstate[lni].rx_state == rgm::R_JUST_STARTED) {
			set_rx_state(lni, rp, rgm::R_ONLINE);
			run_state_machine(lni, "rgm_ack_start");
		}
	} else {
		if (rp->rl_xstate[lni].rx_state == rgm::R_JUST_STARTED) {
			set_rx_state(lni, rp, rgm::R_ONLINE_UNMON);
			run_state_machine(lni, "rgm_ack_start");
		}
	}
	slave_unlock_state();
#ifdef EUROPA_FARM
	return (TRUE);
#endif
}

//
// rgm_notify_dependencies_resolved
// --------------------------------
// If the state of the specified resource in the given zone is R_STARTING_DEPEND
// or R_STOPPING_DEPEND changes it to R_OK_TO_START or R_OK_TO_STOP
// respectively, and calls run_state_machine.
//
#ifdef EUROPA_FARM
bool_t
rgmx_notify_dependencies_resolved_1_svc(
    rgmx_notify_dependencies_resolved_args *rpc_args,
    rgmx_notify_dependencies_resolved_result *rpc_res,
    struct svc_req *)
#else
void
rgm_comm_impl::rgm_notify_dependencies_resolved(sol::nodeid_t president,
    sol::incarnation_num incarnation, rgm::lni_t lni,
    rgm::ln_incarnation_t ln_incarnation, const char *R_name,
    Environment &env)
#endif
{
#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	rgm::lni_t lni = rpc_args->lni;
	rgm::ln_incarnation_t ln_incarnation = rpc_args->ln_incarnation;
	char *R_name = rpc_args->R_name;

	rpc_res->retval = RGMRPC_OK;
#endif
	rlist_p_t rp;
	rglist_p_t rgp;

	ucmm_print("RGM", NOGET("in rgm_notify_dependencies_resolved; "
	    "president %d R_name %s incarnation %d lni %d"),
	    president, R_name, incarnation, lni);

	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		slave_unlock_state();
#ifdef EUROPA_FARM
		rpc_res->retval = RGMRPC_NOREF;
		return (TRUE);
#else
		return;
#endif
	}

	//
	// For the OK_TO_START notification, if the zone has died,
	// we throw an IDL exception and return early.  However, we
	// do not do this for an OK_TO_STOP notification.  We have to
	// process the notification, because the RG might be stopping
	// on a dead zone if the RG contains resources that have
	// Global_zone=TRUE.
	//

	rp = rname_to_r(NULL, R_name);
	CL_PANIC(rp != NULL);
	rgp = rp->rl_rg;

	if (rp->rl_xstate[lni].rx_state == rgm::R_STARTING_DEPEND) {
		// Return early with IDL exception if zone died.
#ifndef EUROPA_FARM
		if (zone_died(lni, ln_incarnation, env)) {
			slave_unlock_state();
			return;
		}
#else
		if (zone_died(lni, ln_incarnation, &rpc_res->retval)) {
			//
			// Sends back RGMRPC_ZONE_DEATH so that the caller
			// can take the appropriate decision
			//
			slave_unlock_state();
			return (TRUE);
		}
#endif
		set_rx_state(lni, rp, rgm::R_OK_TO_START);

		//
		// If we're in RG_PENDING_ONLINE_BLOCKED, we need to move to
		// RG_PENDING_ONLINE so that the state machine will process
		// this RG and bring the resource online.
		//
		// If the RG is in any other state, this resource will be
		// processed as part of that state, or the RG will transition
		// to RG_PENDING_ONLINE when it finishes processing the
		// other state.
		//
		if (rgp->rgl_xstate[lni].rgx_state ==
		    rgm::RG_PENDING_ONLINE_BLOCKED) {
			set_rgx_state(lni, rgp, rgm::RG_PENDING_ONLINE);
		}
		run_state_machine(lni, "rgm_notify_dependencies_resolved");
	} else if (rp->rl_xstate[lni].rx_state == rgm::R_STOPPING_DEPEND) {
		set_rx_state(lni, rp, rgm::R_OK_TO_STOP);
		run_state_machine(lni, "rgm_notify_dependencies_resolved");
	}

	slave_unlock_state();
#ifdef EUROPA_FARM
	return (TRUE);
#endif
}

//
// fetch_restarting_flag
// ----------------------
// Called by the president to a slave.  Returns the value of the
// rx_restart_flg on the specified resource on the given zone.
//
#ifdef EUROPA_FARM
bool_t
rgmx_fetch_restarting_flag_1_svc(rgmx_fetch_restarting_flag_args *rpc_args,
    rgmx_fetch_restarting_flag_result *rpc_res, struct svc_req *)

#else
//
// ln_incarnation is not used. However, its simpler to keep it to avoid
// RU issues, until we have a major release where RU constraints don't apply
//
void
rgm_comm_impl::rgm_fetch_restarting_flag(sol::nodeid_t president,
    sol::incarnation_num incarnation, rgm::lni_t lni,
    rgm::ln_incarnation_t ln_incarnation, const char *R_name,
    rgm::r_restart_t &restart_flag, Environment &env)
#endif
{
#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	rgm::lni_t lni = rpc_args->lni;
	rgm::ln_incarnation_t ln_incarnation = rpc_args->ln_incarnation;
	char *R_name = rpc_args->R_name;
	rgm::r_restart_t restart_flag;

	rpc_res->retval = RGMRPC_OK;
#endif
	rlist_p_t rp;

	ucmm_print("RGM", NOGET("in rgm_fetch_restarting_flag; "
	    "president %d R_name %s incarnation %d lni %d"),
	    president, R_name, incarnation, lni);

	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		slave_unlock_state();
#ifdef EUROPA_FARM
		rpc_res->retval = RGMRPC_NOREF;
		return (TRUE);
#else
		return;
#endif
	}

	//
	// Do not return an IDL exception if the zone died.
	// We have to process the request, because the RG might be stopping
	// on a dead zone if the RG contains resources that have
	// Global_zone=TRUE.  Instead, if the zone is shutting down
	// or down, return rgm::RR_NONE.
	//
	if (is_zone_up(lni)) {
		// look up the resource
		rp = rname_to_r(NULL, R_name);
		CL_PANIC(rp != NULL);

		// Assign the flag value
		restart_flag = rp->rl_xstate[lni].rx_restart_flg;
	} else {
		restart_flag = rgm::RR_NONE;
	}

	slave_unlock_state();

#ifdef EUROPA_FARM
	rpc_res->restart_flag = (r_restart_t)restart_flag;
	return (TRUE);
#endif
}


#ifndef EUROPA_FARM
//
// notify_me_state_change
// is run on the President in response to a wake_president() or
// notify_pres_state_change() call by a slave.  This function normally
// just queues a task on the notify_state_change threadpool and returns.
//
// However, a NOTIFY_WAKE change_tag (from the wake_president call) is
// processed differently. In that case, if we're
// in the middle of a reconfiguration (as specified by the pres_reconfig flag),
// and if the slave specified "fetch" as the r/rg name,
// we call rgm_comm_getstate directly to get the current state from the
// slave, and call rgm_update_pres_state to update the state.
// In this case, the arguments R_RG_name, rstate, and rgstate are
// ignored; the lni is the physical nodeid; and rgm_comm_getstate() fetches
// state for all zones from the slave.
//
// notify_pres_state_change() sets change_tag to either NOTIFY_RES or
// NOTIFY_RG, indicating notification of a specific state change on the
// given resource or RG, respectively.
//
// The president does not call itself.
//
void
rgm_comm_impl::notify_me_state_change(rgm::lni_t lni,
    rgm::notify_change_tag change_tag, const char *R_RG_name,
    rgm::rgm_r_state rstate, rgm::rgm_rg_state rgstate, Environment &)
{
	notify_state_change_task *notify_task = NULL;

	//
	// If it's a notify_wake call, we're at a high enough version to
	// respond to inter-rg protocol requests, and we are in a
	// reconfiguration or the "fetch" option is set on the wake,
	// we will fetch the state from the slave node now (instead of
	// putting the task on the queue, and fetching state when it
	// comes off the queue).
	//
	if (change_tag == rgm::NOTIFY_WAKE) {
		//
		// If the pres_reconfig flag is set, and the slave specified
		// the "fetch" option, we must immediately set the state.
		// It's ok to do this, because we know that the
		// rgm_reconfig thread is holding the lock, and
		// waiting for us to finish.
		//
		if (Rgm_state->pres_reconfig &&
		    strcmp(R_RG_name, "fetch") == 0) {
			ucmm_print("RGM", NOGET("in "
			    "notify_me_state_change with nodeid <%d> "
			    "change_tag NOTIFY_WAKE: "
			    "pres_reconfig flag is set and fetch is "
			    "specified, so calling rgm_update_pres_state "
			    "instead of queueing a state change task.\n"),
			    lni);
			rgm::node_state *state;
			// Assert that the 'lni' argument is a physical nodeid
			CL_PANIC(is_global_zone(lni));
			if (rgm_comm_getstate(lni, state) == RGMIDL_OK) {
				rgm_update_pres_state(lni, state);
				delete state;
				return;
			}
			//
			// The only reason that rgm_comm_getstate would have
			// failed is if we're about to have a reconfiguration,
			// (because the node we tried to contact died) so
			// there's no need to syslog here.
			//
			ucmm_print("RGM", NOGET("in notify_me_state_change "
			    "with nodeid <%d> change_tag <%s>: unable to get "
			    "state from slave -- not queueing state change "
			    "task.\n"), lni, pr_change_tag(change_tag));
			return;
		}
	}

	//
	// It's either a normal incremental notification, or a NOTIFY_WAKE
	// at an old protocol level, or a NOTIFY_WAKE at a new protocol
	// level (treated differently by the state change processor).
	//
	notify_task = new notify_state_change_task(
	    lni, change_tag, R_RG_name, rstate, rgstate, NULL);

	ucmm_print("RGM", NOGET("in notify_me_state_change with "
	    "lni <%d>  change_tag <%s>  R_RG_name <%s>  rstate "
	    "<%s>  rgstate <%s>\n"), lni, pr_change_tag(change_tag),
	    R_RG_name ? R_RG_name : "(none)", pr_rstate(rstate),
	    pr_rgstate(rgstate));

	notify_state_change_threadpool::the().defer_processing(notify_task);
	// notify_task will be deleted by the thread that processes it.
}


//
// rgm_chg_freeze
// Slave-side of rgm_change_freeze().  Slave may be the president.
// Set frozen bit on specified RGs and clear it on all others.
// Suspend or reinstate timeouts on in-flight method invocations.
//
// When we are called, rgl_is_frozen on each RG is either set to FROZ_TRUE
// (RG is frozen) or FROZ_FALSE (RG is unfrozen).  We iterate through the new
// list of frozen RGs (freezelist); if the RG is currently FROZ_FALSE,
// we set it to FROZ_SET, indicating that the RG has transitioned from
// unfrozen to frozen.  If the RG is currently FROZ_TRUE, we set
// it to FROZ_REMAINS, indicating that the RG remains in the frozen state.
// When we are done, any RG that is FROZ_TRUE is transitioning from
// frozen to unfrozen state, and any RG that is FROZ_FALSE remains in
// the unfrozen state.
//
// This is executed just once on each physical slave node.
// freeze_adjust_timeouts() adjusts timeouts for methods running in
// all zones on the node.
//
void
rgm_comm_impl::rgm_chg_freeze(sol::nodeid_t president,
    sol::incarnation_num incarnation,
    const rgm::idl_string_seq_t &freezelist, Environment &)
{
	uint_t		i;
	uint_t		length;		// # of RGs in sequence
	rglist_p_t	rglp;		// current RG
	char		*s;		// temp copy of RG name
	sc_syslog_msg_handle_t handle;

	ucmm_print("RGM", NOGET("in rgm_chg_freeze\n"));

	FAULT_POINT_FOR_ORPHAN_ORB_CALL("rgm_chg_freeze", president,
		incarnation);

	length = (uint_t)freezelist.length();
	ucmm_print("rgm_chg_freeze()", NOGET("length <%d>\n"), length);

	// lock the state structure while we access it and update RG state
	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		slave_unlock_state();
		return;
	}

	//
	// If we haven't read the CCR yet (which might be the case if we
	// are a newly joining slave and the president gets interrupted
	// during the rgm_reconfig() call before instructing us to read the
	// CCR -- see bug 4419641), we will not have the requisite RG data in
	// memory, and we will run into problems while processing the
	// freeze list below.  It is okay to return early in this case
	// because the president will communicate the complete freeze
	// information to a slave after the slave has been instructed to read
	// the CCR.
	//
	if (!Rgm_state->ccr_is_read) {
		ucmm_print("rgm_chg_freeze()",
		    NOGET("return early, CCR not read\n"));
		slave_unlock_state();
		return;
	}

	// set frozen bit on RGs in freeze list
	for (i = 0; i < length; i++) {
		//
		// We make a copy of this string to satisfy the C++ compiler,
		// which otherwise complains "Formal argument rgname of
		// type char* const  in call to rgname_to_rg(char* const )
		// is being passed const char*."  We need to free the memory
		// when we're finished with it.
		//
		s = strdup((const char *)freezelist[i]);
		ucmm_print("rgm_chg_freeze", NOGET("%d:freeze:<%s>\n"), i, s);
		//
		// Since the validity of the rgname has already been
		// checked before calling this function, we only CL_PANICs
		// here to make sure RG pointer is OK.
		//
		rglp = rgname_to_rg(s);
		CL_PANIC(rglp);
		free(s);

		if (rglp->rgl_is_frozen == FROZ_TRUE) {
			rglp->rgl_is_frozen = FROZ_REMAINS;
		} else if (rglp->rgl_is_frozen == FROZ_FALSE) {
			rglp->rgl_is_frozen = FROZ_SET;
		}
	}

	//
	// Iterate through all RGs, setting their new freeze states.
	// Adjust in-flight method timeouts for those that are currently
	// FROZ_TRUE (transitioning from frozen to unfrozen) or
	// FROZ_SET (transitioning from unfrozen to frozen).
	//
	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		switch (rglp->rgl_is_frozen) {
		case FROZ_FALSE:
			ucmm_print("rgm_chg_freeze()", NOGET(
			    "<%s> remains unfrozen\n"),
			    rglp->rgl_ccr->rg_name);
			break;

		case FROZ_TRUE:
			ucmm_print("rgm_chg_freeze()", NOGET(
			    "<%s> is being unfrozen\n"),
			    rglp->rgl_ccr->rg_name);
			rglp->rgl_is_frozen = FROZ_FALSE;

			// Set last_frozen timestamp to current clock time
			time_t	curr_time;
			curr_time = time(&rglp->rgl_last_frozen);
			// if time() call failed, abort
			CL_PANIC(curr_time >= (time_t)0);




			//
			// SCMSGS
			// @explanation
			// Timeout monitoring for resource methods in the
			// resource group is being reinstated since the
			// device group reconfigurations have completed.
			// @user_action
			// This is just an informational message.
			//
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RG_TAG, rglp->rgl_ccr->rg_name);
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Method timeout monitoring is being resumed for "
			    "Resource group <%s>.", rglp->rgl_ccr->rg_name);
			sc_syslog_msg_done(&handle);
			freeze_adjust_timeouts(rglp);
			break;

		case FROZ_SET:
			ucmm_print("rgm_chg_freeze()", NOGET(
			    "<%s> is being frozen\n"),
			    rglp->rgl_ccr->rg_name);
			rglp->rgl_is_frozen = FROZ_TRUE;
			//
			// SCMSGS
			// @explanation
			// Timeout monitoring for resource methods in the
			// resource group is being suspended while disk device
			// groups are reconfiguring. This prevents unnecessary
			// failovers that might be caused by the temporary
			// unavailability of a device group.
			// @user_action
			// This is just an informational message.
			//
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RG_TAG, rglp->rgl_ccr->rg_name);
			(void) sc_syslog_msg_log(handle, LOG_INFO, MESSAGE,
			    "Method timeouts are being suspended for Resource "
			    "group <%s> until device group switchovers have "
			    "completed.", rglp->rgl_ccr->rg_name);
			sc_syslog_msg_done(&handle);
			freeze_adjust_timeouts(rglp);
			break;

		case FROZ_REMAINS:
			ucmm_print("rgm_chg_freeze()", NOGET(
			    "<%s> remains frozen\n"),
			    rglp->rgl_ccr->rg_name);
			rglp->rgl_is_frozen = FROZ_TRUE;
			break;

		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The in-memory state of the rgmd has been corrupted
			// due to an internal error. The rgmd will produce a
			// core file and will force the node to halt or reboot
			// to avoid the possibility of data corruption.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: rgm_chg_freeze: INTERNAL ERROR: "
			    "invalid value of rgl_is_frozen <%d> for "
			    "resource group <%s>",
			    rglp->rgl_is_frozen, rglp->rgl_ccr->rg_name);
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHABLE
		}
	}

	slave_unlock_state();
}
#endif // ! EUROPA_FARM


//
// rgm_am_i_joining
//
// Server side of IDL are_you_joining() call.
// Sets the OUT boolean parameter 'is_joining' to true if this node is
// just now joining the cluster.  Sets the OUT boolean parameter to false
// if this node was already a member of the cluster before the current
// reconfiguration.
//
// Note: If I died and am rejoining in the same reconfiguration,
// I think I'm joining, so I return true.
//
// This routine must grab the join_lock to protect access to
// Rgm_state->is_thisnode_booting.
//
// However this routine must NOT acquire the rgm_mutex because multiple
// nodes may make the call on each other simultaneously.  Grabbing the
// lock could result in deadlock.
//
// This is executed only per physical node.
//
#ifdef EUROPA_FARM
bool_t
rgmx_am_i_joining_1_svc(void *, bool_t *rpc_is_joining,
    struct svc_req *)
#else
void
rgm_comm_impl::rgm_am_i_joining(bool &is_joining, Environment &)
#endif
{
#ifdef EUROPA_FARM
	bool is_joining;
#endif
	ucmm_print("rgm_am_i_joining", "enter");
	//
	// I'm joining until I get through the first reconfiguration.
	// Slaves reset is_thisnode_booting to FALSE after kicking
	// off boot methods.
	//

	join_lock.lock();

	if (Rgm_state->is_thisnode_booting) {
		ucmm_print("RGM", "RRRRRRRRRRR I am JOINING\n");
		is_joining = true;
	} else {
		ucmm_print("RGM", "RRRRRRRRRRR I'm NOT JOINING\n");
		is_joining = false;
	}

	ucmm_print("rgm am i joining", "is joining:%d", is_joining);
	join_lock.unlock();
#ifdef EUROPA_FARM
	*rpc_is_joining = is_joining;
	return (TRUE);
#endif
}


#ifndef EUROPA_FARM
//
// rgm_slave_record_memb
//
// Server side of IDL slaves_record_membership() call.
//
// The President is calling to instruct us to set the new membership
// value.  When we get this call we know the President has finished
// processing node death.  It's now safe to wipe out the old membership
// value.
//
// Overwriting the previous membership value earlier would be incorrect:
// If the President were to die before having finished processing node
// death, there would be another reconfiguration in which the new
// President would use this incorrect value.  This is important only when
// processing node death.  The President gets its idea of joiners from the
// joiners themselves, not from its previous state.
//
// Here's an example of what would happen if the value were overwritten
// too soon:
//
// Reconfiguration R1
// ------------------
// President node 1's idea of previous membership:	0111
// President node 1's idea of new membership:		1011
// President node 1's idea of death:			0100
// but President node 1 dies before processing dead node 3.
//
// Reconfiguration R2
// ------------------
// New President node 2 is elected, but he overwrote the
// previous membership value too early during R1.
// President node 2's idea of previous membership:	1011
// President node 2's idea of new membership:		1110
// President node 2's idea of death:			0001
// He misses out on the fact that node 3 died.  He should have used the
// previous membership value from R1, 0111, in which case
// died_or_died_and_rejoining() would return true (node 3 would be
// treated as "died and rejoining" in this case).  So an RG that had been
// online on node 3 before its death wouldn't get rebalanced.
//
// This function is executed per physical node.  The updateMembership()
// method removes all zones for nodes that died, but only adds the
// global zone of nodes that joined.  Non-global zones are added in
// a separate code path.
//
void
rgm_comm_impl::rgm_slave_record_memb(sol::nodeid_t president,
    sol::incarnation_num incarnation, nodeset_t curr_ns, Environment &)
{
	slave_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		slave_unlock_state();
		return;
	}

	LogicalNodeset nset = curr_ns;

	//
	// Europa only:
	// Update the physical membership of the cluster, with
	// the Farm nodes membership.
	//
	(void) rgmx_hook_call(rgmx_hook_update_membership, &nset, true);

	Rgm_state->current_membership = nset;

	lnManager->updateMembership(nset);

	slave_unlock_state();
}
#endif // ! EUROPA_FARM


//
// rgm_read_ccr
//
// Server side of joiners_read_ccr().
// The Rgm_state->ccr_is_read flag protects against reading the CCR twice
// in case the President were to die just after issuing this order, and
// were a new President to then issue the same order again.
//
// Previously version modified to support zones.
// The president also sends the list of LogicalNodes that it
// discovered on this node while parsing the RT_instnodes/
// RG_nodelist.
//
#ifdef EUROPA_FARM
bool_t
rgmx_read_ccr_1_svc(rgmx_read_ccr_args *rpc_args,
    void *,
    struct svc_req *)
#else
void
rgm_comm_impl::rgm_read_ccr(sol::nodeid_t president,
    sol::incarnation_num incarnation,
    const rgm::lni_mappings_seq_t &lni_map, Environment &)
#endif
{
#ifndef EUROPA_FARM
	FAULT_POINT_FOR_ORPHAN_ORB_CALL("rgm_read_ccr", president, incarnation);
#endif
	sol::nodeid_t	mynodeid = get_local_nodeid();
	LogicalNode	*lnode;
	uint_t		i;
	char		*ln;
	sc_syslog_msg_handle_t handle;

#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	rgm::lni_mappings_seq_t lni_map;

	lni_map.length(rpc_args->lni_map.lni_mappings_seq_t_len);
	for (i = 0; i < lni_map.length(); i++) {
		lni_map[i].idlstr_zonename = rpc_args->lni_map.
			lni_mappings_seq_t_val[i].idlstr_zonename;
		lni_map[i].lni = rpc_args->lni_map.
			lni_mappings_seq_t_val[i].lni;
		lni_map[i].state = (rgm::rgm_ln_state) rpc_args->lni_map.
			lni_mappings_seq_t_val[i].state;
		lni_map[i].incarn = rpc_args->lni_map.
			lni_mappings_seq_t_val[i].incarn;
	}
#endif

	rgm_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		rgm_unlock_state();
#ifdef EUROPA_FARM
		return (TRUE);
#else
		return;
#endif
	}

	ucmm_print("RGM", NOGET("rgm_comm_impl: in rgm_read_ccr\n"));

	for (i = 0; i < lni_map.length(); i++) {

		lnode = lnManager->findLogicalNode(mynodeid,
		    lni_map[i].idlstr_zonename, &ln);

		//
		// There's a chance that this slave just discovered
		// this zone (because it just booted). In that case,
		// the pres just assigned us an lni
		//
		if (lnode != NULL) {
			CL_PANIC(lnode->getLni() == lni_map[i].lni);
			ucmm_print("RGM",
			    NOGET("rgm_read_ccr: found LN %d already "
			    "here."), lnode->getLni());
			free(ln);
			continue;
		}

		//
		// The state is almost certainly UNCONFIGURED, but read it
		// out of the mapping anyway
		//
		lnode = lnManager->createLogicalNodeFromMapping(mynodeid,
		    lni_map[i].idlstr_zonename, lni_map[i].lni,
		    lni_map[i].state);
		// set the incarnation number
		lnode->setIncarnation(lni_map[i].incarn);
		free(ln);
	}

	if (!Rgm_state->ccr_is_read) {
		if (rgm_init_ccr().err_code != SCHA_ERR_NOERR) {
			// crash and burn
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd is unable to read the cluster
			// configuration repository. This is a fatal error.
			// The rgmd will produce a core file and will force
			// the node to halt or reboot to avoid the possibility
			// of data corruption.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: Failed to read CCR");
			sc_syslog_msg_done(&handle);
			abort();
		}
		Rgm_state->ccr_is_read = B_TRUE;
#if (SOL_VERSION >= __s10)
	} else if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		//
		// Each time a cluster boots, the newly elected
		// president requests all the slaves to read ccr
		// information and store it in Rgm_state. The
		// flag ccr_is_read is used to prevent a slave (and
		// a president as well) from reading the ccr twice.
		// This behavior is required for a cluster consisting
		// of only global zones. However, for zone clusters,
		// the desired behavior is different. Each time a
		// zone cluster is rebooted (without rebooting the
		// physical cluster), the ccr_is_read flag will not be
		// reset to false because all the zc rgmds will continue
		// to run and will be part of intention mechanism. So,
		// each time a zone cluster reboots, the newly elected
		// president's request to slaves to read ccr will not
		// cause an rgm_init_ccr() on the slaves. This is the
		// correct and desired behavior. However, this also
		// results in a condition where any changes to the
		// Ok_to_start flag will not get updated on the slaves.
		// A newly elected president can potentially change this
		// flag and the following code updates Ok_to_start flag
		// on slaves when the president calls joiners_read_ccr().
		// Please see CR 6770386 for more information.
		//
		if (rgm_set_ok_to_start_for_all_rg().err_code
		    != SCHA_ERR_NOERR) {
			// crash and burn
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd is unable to read the cluster
			// configuration repository. This is a fatal error.
			// The rgmd will produce a core file and will force
			// the node to halt or reboot to avoid the possibility
			// of data corruption.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "fatal:Failed to set ok_to_start");
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHABLE
		}
#endif
	}

	rgm_unlock_state();
#ifdef EUROPA_FARM
		return (TRUE);
#endif
}


//
// rgm_run_boot_meths
//
// The President calls joining slaves to run boot methods during
// reconfiguration.  It can call itself.
//
// This function causes boot methods to be run in the global zone and
// all non-global zones that are already up.
//
#ifdef EUROPA_FARM
bool_t
rgmx_run_boot_meths_1_svc(rgmx_run_boot_meths_args *rpc_args,
    void *,
    struct svc_req *)
#else
void
rgm_comm_impl::rgm_run_boot_meths(sol::nodeid_t president,
    sol::incarnation_num incarnation, Environment &)
#endif
{
	rgm::lni_t mynodeid = get_local_nodeid();	// global zone

#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
#endif

	slave_lock_state();

	join_lock.lock();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		//
		join_lock.unlock();
		slave_unlock_state();
#ifdef EUROPA_FARM
		return (TRUE);
#else
		return;
#endif
	}

	// first run them on the global zone
	run_boot_meths_on_logical_node(mynodeid);


	//
	// Now see if any non-global zones are up yet.
	// It not very likely, but it's possible.
	//
	LogicalNode *ln = lnManager->findObject(mynodeid);
	CL_PANIC(ln != NULL);

	// Now iterate through the non-global zones
	std::list<LogicalNode*>::const_iterator it;
	for (it = ln->getLocalZones().begin();
	    it != ln->getLocalZones().end(); ++it) {
		if ((*it)->getStatus() == rgm::LN_UP) {
			run_boot_meths_on_logical_node((*it)->getLni());
		}
	}

	//
	// If there are no boot methods to run, we do not need to
	// wake the President because, if we're at the "new" (inter-rg)
	// version, RG_OFF_BOOTED is an "interesting" RG state, so the
	// president will get notified.  If we're at the old version, the
	// president will fetch state after this call returns.
	//
	Rgm_state->is_thisnode_booting = B_FALSE;

	join_lock.unlock();
	slave_unlock_state();
#ifdef EUROPA_FARM
	return (TRUE);
#endif
}


//
// append_restart_timestamp
//
// Update the resource_restart history or RG_restart history of the
// given resource, using the current clock time.  Also, prune the
// restart history to the number of seconds indicated by the 'interval'
// argument.
//
// If swap space is exhausted, just return.
// If any other error occurs, syslog a message and return.
//
// rgtp points to the timelist to be appended to.
// The string s is either "RG" or "resource", indicating the type
// of restart being performed; to be used in the syslog message.
//
static void
append_restart_timestamp(rgm_timelist_t **rgtp, const char *s,
    const char *rname, const char *rgname, time_t interval)
{
	sc_syslog_msg_handle_t handle;
	int myerrno;

	rgm_prune_timelist(rgtp, interval);
	myerrno = rgm_append_to_timelist(rgtp);
	if (myerrno == ENOMEM) {
		//
		// We're out of swap -- this is a non-fatal error, although
		// it's likely to be followed by a system crash.  Return early.
		//
		return;
	} else if (myerrno != 0) {
		// Inability to store timestamp is non-fatal
		char	*errmesg = strerror(myerrno);

		if (errmesg == NULL) {
			errmesg = "no such errno";
		}
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A time() system call has failed. This prevents updating the
		// history of scha_control restart calls. This could cause the
		// scha_resource_get (NUM_RESOURCE_RESTARTS) or
		// (NUM_RG_RESTARTS) query to return an inaccurate value on
		// this node. This in turn could cause a failing resource to
		// be restarted continually rather than failing over to
		// another node. However, this problem is very unlikely to
		// occur.
		// @user_action
		// If this message is produced and it appears that a resource
		// or resource group is continually restarting without failing
		// over, try switching the resource group to another node.
		// Other syslog error messages occurring on the same node
		// might provide further clues to the root cause of the
		// problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("scha_control: warning: cannot store "
		    "%s restart timestamp for resource group <%s> "
		    "resource <%s>: time() failed, errno <%d> (%s)"),
		    s, rgname, rname, myerrno, errmesg);
		sc_syslog_msg_done(&handle);
	}
}

#if (SOL_VERSION >= __s10)
void
rgm_comm_impl::idl_is_rgm_alive(Environment &)
{
#ifdef EUROPA_FARM
	CL_PANIC(0);
#endif
}

void
rgm_comm_impl::idl_reconfig_ready_to_do_step(
    sol::nodeid_t nid, uint32_t step_num,
    cmm::seqnum_t seqnum, Environment &env)
{
#ifdef EUROPA_FARM
	CL_PANIC(0);
#else	// EUROPA_FARM
	// XXX should we assert that we are the lowest nodeid rgmd?
	peer_rgm_state_lock.lock();
	nodeid_t src_nid = nid;
	peer_rgm_state[src_nid].seqnum = seqnum;
	peer_rgm_state[src_nid].step_num = step_num;
	peer_rgm_state_cv.signal();
	peer_rgm_state_lock.unlock();
#endif	// EUROPA_FARM
}

void
rgm_comm_impl::idl_reconfig_do_step(
    uint32_t step_num, cmm::seqnum_t seqnum,
    rgm::sync_result_t sync_result, Environment &)
{
#ifdef EUROPA_FARM
	CL_PANIC(0);
#else	// EUROPA_FARM
	// XXX should we assert that we are not the lowest nodeid rgmd?

	rgm_reconfig_lock.lock();
	if (rgm_reconfig_membership_cb_data.seqnum > seqnum) {
		//
		// A newer reconfiguration has started.
		// The lowest nodeid rgmd in the newer membership has updated
		// this data. So we will not overwrite it.
		//
		rgm_reconfig_lock.unlock();
		return;
	}
	rgm_reconfig_lock.unlock();

	peer_rgm_state_lock.lock();
	if (lowest_nid_seqnum > seqnum) {
		//
		// A newer reconfiguration has started.
		// The lowest nodeid rgmd in the newer membership has updated
		// this data. So we will not overwrite it.
		//
		peer_rgm_state_lock.unlock();
		return;
	}
	lowest_nid_seqnum = seqnum;
	lowest_nid_step_num = step_num;
	lowest_nid_sync_result = sync_result;
	peer_rgm_state_cv.signal();
	peer_rgm_state_lock.unlock();
#endif	// EUROPA_FARM
}
#endif
