//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#ifndef _RGM_OBJ_MANAGER_H
#define	_RGM_OBJ_MANAGER_H

#pragma ident	"@(#)rgm_obj_manager.h	1.3	08/05/20 SMI"

#include <map>
#include "rgm_array_free_list.h"


//
// There is one 'ObjManager' class per type being managed, i.e:
//
// The following objects may be defined:
// . ObjManager<Resource, uint_t> resourceMgr; // For the RGM resources
// . ObjManager<ResourceGrp, uint_t> resgrpMgr; // For the RGM RG
// . ObjManager<LogicalNode, uint_t> lnMgr ; // For the RGM Logical Node
//
// Currently, only lnManager is defined.
//
template<class OBJ, class T> class ObjManager {
protected:
	ArrayFreeList<OBJ>	array;
	std::map<std::string, T> hash;

public:
	// for mdb module
	friend void rgm_print_logical_nodes();

	//
	// For iterating over all the elements stored in this object
	//
	typedef typename std::map<std::string, T>::iterator iter;
	iter begin() { return hash.begin(); }
	iter end() { return hash.end(); }

	ObjManager();
	virtual ~ObjManager();

	//
	// Lookup element in the hash table
	// (inline wrapper around lookup from hashtable)
	//
	T lookup(const char *name);

	// only call with valid index, else causes panic
	OBJ* findObject(T idx);
	// invalid key will give NULL ptr
	OBJ* findObject(const char *key);

protected:
	// . Allocate an identifier for the 'Element' i.e
	//   LNI, LRGI, LRI
	// . Add 'Element' in the array
	// . Add Element name in the hash table
	//
	virtual T addElement(const char *name, OBJ* obj);


	// . Remove 'Element' name from hash table
	// . Remove 'Element' from array and free
	//   the identifier so it can be reused
	//
	OBJ* removeElement(const char *name);
};

// include the rest of the template definitions
#include "rgm_obj_manager.cc"

#endif /* _RGM_OBJ_MANAGER_H */
