/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_lb.cc	1.21	08/05/20 SMI"

#if !defined(_KERNEL) && defined(_KERNEL_ORB)
#define	UNODE
#else
#undef UNODE
#endif

//
// rgm_lb.cc
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/cladm.h>
#include <orb/invo/common.h>
#include <orb/infrastructure/orb.h>
#include <h/sol.h>
#include <nslib/ns.h>
#include <rgm/rgm_comm_impl.h>
#include <rgm_proto.h>
#include <rgm_api.h>
#include <sys/clconf_int.h>
#include <h/network.h>
#include <clconf/clpdt.h>

static int get_errno(Environment &e);
static network::lbobj_i_ptr get_lbobj(const char *resource);

#ifdef UNODE

// For unode use malloc directly instead of pulling in malloc_nocheck
// that would call malloc directly.  Tell lint it is ok that we're
// defining a macro with the same name as a function.

#define	malloc_nocheck	malloc	//lint !e652

#endif

void
lb_op(scha_ssm_lb_args *arg, scha_ssm_lb_result_t *result)
{
	Environment e;
	unsigned int i;

	bzero(result, sizeof (scha_ssm_lb_result_t));

	// Look up the load balancer by the service/resource
	network::lbobj_i_var lbobj = get_lbobj(arg->rs_name);
	if (CORBA::is_nil(lbobj)) {
		result->ret_code = ESRCH;
		return;
	}


	switch (arg->op_code) {
	case GET_POLICY_TYPE:
		{
		network::lb_policy_t type = lbobj->get_policy(e);
		if (e.exception()) {
			// exception is cleared by get_errno()
			result->ret_code = get_errno(e);
			break;
		}
		result->ret_data.ret_data_len = 1;
		result->ret_data.ret_data_val = (int *)malloc_nocheck(
		    sizeof (int));
		if (result->ret_data.ret_data_val == NULL) {
			result->ret_data.ret_data_len = 0;
			result->ret_code = ENOMEM;
			break;
		}
		result->ret_data.ret_data_val[0] = type;
		}
		break;
	case GET_DISTRIBUTION:
		{
		network::dist_info_t_var dist_infov =
			lbobj->get_distribution(e);
		if (e.exception()) {
			// exception is cleared by get_errno()
			result->ret_code = get_errno(e);
			break;
		}
		result->ret_data.ret_data_len = 2*dist_infov->nodes.length();
		result->ret_data.ret_data_val = (int *)malloc_nocheck(
		    sizeof (int) * result->ret_data.ret_data_len);
		if (result->ret_data.ret_data_val == NULL) {
			result->ret_data.ret_data_len = 0;
			result->ret_code = ENOMEM;
			break;
		}
		for (i = 0; i < dist_infov->nodes.length(); i++) {
			result->ret_data.ret_data_val[2*i] =
				dist_infov->nodes[i];
			result->ret_data.ret_data_val[2*i+1] =
				(int)dist_infov->weights[i];
		}
		break;
		}
	case SET_DISTRIBUTION:
		{
		network::dist_info_t_var weights;

		network::weighted_lbobj_var weighted_lbobj =
			network::weighted_lbobj::_narrow(lbobj);
		if (CORBA::is_nil(weighted_lbobj)) {
			result->ret_code = EINVAL;
			break;
		}

		// Get the current infomation
		weights = lbobj->get_distribution(e);
		if (e.exception()) {
			// exception is cleared by get_errno()
			result->ret_code = get_errno(e);
			break;
		}
		// Plug in the desired weights
		for (i = 0; i < weights->weights.length(); i++) {
			// Set the weights from the input.  If there
			// aren't enough weights in the input, use 0.
			weights->weights[i] =
				(i < (unsigned int)arg->op_data.op_data_len)?
				(unsigned int)arg->op_data.op_data_val[i] : 0;
		}
		// Try setting it.
		weighted_lbobj->set_distribution(*weights, e);
		if (e.exception()) {
			// exception is cleared by get_errno()
			result->ret_code = get_errno(e);
			break;
		}
		if (arg->op_data.op_data_len != weights->nodes.length()) {
			// Number of nodes didn't match what was expected.
			result->ret_code = EREMCHG;
		}
		break;
		}
	case SET_ONE_DISTRIBUTION:
		{
		if (arg->op_data.op_data_len != 2) {
			result->ret_code = EINVAL;
			break;
		}
		int node = arg->op_data.op_data_val[0];
		if (node < 0) {
			result->ret_code = EINVAL;
			break;
		}
		network::weighted_lbobj_var weighted_lbobj =
			network::weighted_lbobj::_narrow(lbobj);
		if (CORBA::is_nil(weighted_lbobj)) {
			result->ret_code = EINVAL;
			break;
		}
		weighted_lbobj->set_weight((unsigned int)node,
				(unsigned int)arg->op_data.op_data_val[1], e);
		if (e.exception()) {
			// exception is cleared by get_errno()
			result->ret_code = get_errno(e);
		}
		break;
		}
	case SET_PERF_MON_STATUS:
		{
		if (arg->op_data.op_data_len != 2) {
			result->ret_code = EINVAL;
			break;
		}
		network::perf_mon_policy_var perf_mon_policy =
			network::perf_mon_policy::_narrow(lbobj);
		if (CORBA::is_nil(perf_mon_policy)) {
			result->ret_code = EINVAL;
			break;
		}
		network::perf_mon_policy::node_metrics metrics;
		metrics.load = (unsigned int)arg->op_data.op_data_val[1];
		perf_mon_policy->set_load_info(
			(unsigned int)arg->op_data.op_data_val[0],
			metrics, e);
		if (e.exception()) {
			// exception is cleared by get_errno()
			result->ret_code = get_errno(e);
		}
		break;
		}
	case SET_PERF_MON_PARAMETERS:
		{
		if (arg->op_data.op_data_len != 6) {
			result->ret_code = EINVAL;
			break;
		}
		network::perf_mon_policy_var perf_mon_policy =
			network::perf_mon_policy::_narrow(lbobj);
		if (CORBA::is_nil(perf_mon_policy)) {
			result->ret_code = EINVAL;
			break;
		}
		network::perf_mon_policy::perf_mon_parameters_t parameters;
		parameters.frequency =
			(unsigned int)arg->op_data.op_data_val[0];
		parameters.scale_rate =
			(unsigned int)arg->op_data.op_data_val[1];
		parameters.min_weight =
			(unsigned int)arg->op_data.op_data_val[2];
		parameters.max_weight =
			(unsigned int)arg->op_data.op_data_val[3];
		parameters.quiet_width =
			(unsigned int)arg->op_data.op_data_val[4];
		parameters.floor = (unsigned int)arg->op_data.op_data_val[5];

		// Set the new policy
		perf_mon_policy->set_parameters(parameters, e);
		if (e.exception()) {
			// exception is cleared by get_errno()
			result->ret_code = get_errno(e);
		}
		break;
		}
	case GET_PERF_MON_PARAMETERS:
		{
		network::perf_mon_policy_var perf_mon_policy =
			network::perf_mon_policy::_narrow(lbobj);
		if (CORBA::is_nil(perf_mon_policy)) {
			result->ret_code = EINVAL;
			break;
		}
		network::perf_mon_policy::perf_mon_parameters_t parameters;
		perf_mon_policy->get_parameters(parameters, e);
		if (e.exception()) {
			// exception is cleared by get_errno()
			result->ret_code = get_errno(e);
			break;
		}

		result->ret_data.ret_data_len = 6;
		result->ret_data.ret_data_val = (int *)malloc_nocheck(
		    sizeof (int) * result->ret_data.ret_data_len);
		if (result->ret_data.ret_data_val == NULL) {
			result->ret_data.ret_data_len = 0;
			result->ret_code = ENOMEM;
			break;
		}
		result->ret_data.ret_data_val[0] = (int)parameters.frequency;
		result->ret_data.ret_data_val[1] = (int)parameters.scale_rate;
		result->ret_data.ret_data_val[2] = (int)parameters.min_weight;
		result->ret_data.ret_data_val[3] = (int)parameters.max_weight;
		result->ret_data.ret_data_val[4] = (int)parameters.quiet_width;
		result->ret_data.ret_data_val[5] = (int)parameters.floor;
		break;
		}
	default:
		{
		result->ret_code = EINVAL;
		break;
		}
	}
}

//
// Convert the environment to an errno.
// Called when there was an exception.
//
static int get_errno(Environment &e)
{
	int err;
	CL_PANIC(e.exception());
	sol::op_e *ex = sol::op_e::_exnarrow(e.exception());
	if (ex != NULL) {
		err = ex->error;
	} else if (network::weighted_lbobj::state_changed::_exnarrow(
			e.exception())) {
		err = EREMCHG;
	} else {
		err = EIO;
	}
	e.clear();
	return (err);
}

// Initialize lbobj and other object references.
// Return NULL if failure
// This function guarantees that lbobj will be valid, but
// weighted_lbobj will only be valid if appropriate.
network::lbobj_i_ptr
get_lbobj(const char *resource)
{
	static int initialized = 0;
	if (!initialized) {
		if (ORB::initialize() != 0) {
			errno = EIO;
			return (NULL);
		}
		initialized = 1;
	}

	// Get the pdt server
	network::PDTServer_var pdts = get_pdt_server();
	if (CORBA::is_nil(pdts)) {
		return (nil);
	}
	Environment e;

	// Get the lbobj
	network::group_t group_n;

	(void) os::strncpy(group_n.resource, resource,
		(size_t)(network::resource_name_len - 1));
	// Service_group is the resource
	group_n.resource[network::resource_name_len - 1] = '\0';

	network::lbobj_i_ptr lbobj = pdts->get_lbobj(group_n, e);
	if (e.exception()) {
		e.clear();
		return (nil);
	}
	if (CORBA::is_nil(lbobj))
		return (nil);
	return (lbobj);
}

#ifdef UNODE

// The rest of this file is code that lets us execute lb_op() in
// unode.  Each of the non-static functions is a unode entry point
// that corresponds to one of the uses of lb_op.  When using unode
// an entry point takes arguments like main() does.  For example,
// if you had a unode cluster named foo and you wanted to call
// lb_op on node 3 to set the weights on a service group named
// dss-res you would run:
//
//	unode_load foo 3 lb_op set_distribution dss-res 1@1,1@2
//
// The set_distribution entry point would convert the strings into
// arguments appropriate for lb_op and then call lb_op with
// SET_DISTRIBUTION.
//
// The form of the entry points is similar.  The number of arguments
// is checked and a usage message printed if incorrect.  The arguments
// are then converted into the appropriate type for lb_op, lb_op is
// called, and failures reported.  Results are printed where appropriate.
// 0 is returned on success and -1 on failure.

#include <unode/output.h>

// unode initialization function, called only once when the module
// is first loaded.  The module stays resident and subsequent unode_load
// runs don't load the module or call unode_init.

int
unode_init()
{
	(void) printf("lb_op loaded\n");

	return (0);
}

// Convert from node_status to a string representation.

static const char *
ns_to_ns_str(network::node_status status)
{
	switch (status) {

	case network::instance_up:
		return ("instance_up");

	case network::instance_down:
		return ("instance_down");

	case network::no_instance:
		return ("no_instance");

	default:
		return ("<bad node status>");
	}
}

int
get_policy_type(int argc, char **argv)
{
	output out(&argc, argv);
	scha_ssm_lb_args arg;
	scha_ssm_lb_result_t result;

	argc--; argv++;

	if (argc != 1) {
		(void) fprintf(out, "usage: get_policy_type service-name\n");
		return (-1);
	}

	arg.rs_name = argv[0];
	arg.op_code = GET_POLICY_TYPE;

	lb_op(&arg, &result);

	if (result.ret_code != 0) {
		(void) fprintf(out, "lb_op GET_POLICY_TYPE \"%s\" failed "
		    "with error %d\n", argv[0], result.ret_code);
		return (-1);
	}

	if (result.ret_data.ret_data_len != 1 ||
	    result.ret_data.ret_data_val == NULL) {
		(void) fprintf(out, "get_policy_type: bad return data\n");
		return (-1);
	}

	switch (result.ret_data.ret_data_val[0]) {

	case network::LB_WEIGHTED:
		(void) fprintf(out, "LB_WEIGHTED\n");
		break;

	case network::LB_PERF:
		(void) fprintf(out, "LB_PERF\n");
		break;

	case network::LB_USER:
		(void) fprintf(out, "LB_USER\n");
		break;

	case network::LB_ST:
		(void) fprintf(out, "LB_ST\n");
		break;

	case network::LB_SW:
		(void) fprintf(out, "LB_SW\n");
		break;

	default:
		(void) fprintf(out, "get_policy_type: unexpected policy: %d\n",
		    result.ret_data.ret_data_val[0]);
		return (-1);
	}

	free(result.ret_data.ret_data_val);

	return (0);
}

int
get_distribution(int argc, char **argv)
{
	output out(&argc, argv);
	scha_ssm_lb_args arg;
	scha_ssm_lb_result_t result;
	uint_t i, dist_len;
	network::node_status nstat;
	uint32_t weight;
	const char *ns_str;

	argc--; argv++;

	if (argc != 1) {
		(void) fprintf(out, "usage: get_distribution service-name\n");
		return (-1);
	}

	arg.rs_name = argv[0];
	arg.op_code = GET_DISTRIBUTION;

	lb_op(&arg, &result);

	if (result.ret_code != 0) {
		(void) fprintf(out, "lb_op GET_DISTRIBUTION \"%s\" failed "
		    "with error %d\n", argv[0], result.ret_code);
		return (-1);
	}

	if (result.ret_data.ret_data_val == NULL) {
		(void) fprintf(out, "get_distribution: bad return data\n");
		return (-1);
	}

	dist_len = result.ret_data.ret_data_len / 2;
	if (dist_len > 0)
		(void) fprintf(out, "unlisted nodes have status no_instance "
		    "and weight 1\n");

	for (i = 0; i < dist_len; i++) {
		nstat = (network::node_status)
		    result.ret_data.ret_data_val[i * 2];
		weight = (uint32_t)result.ret_data.ret_data_val[i * 2 + 1];

		if (nstat != network::no_instance || weight != 1) {
			ns_str = ns_to_ns_str(nstat);
			(void) fprintf(out, "node %u, status: %s, weight: %u\n",
			    i, ns_str, weight);
		}
	}

	free(result.ret_data.ret_data_val);

	return (0);
}

// Parse the weight string for set_distribution.  This probably
// duplicates code else where in the source tree, but sharing
// code would be hassle and this is only unode after all.  This
// code does some error checking, but isn't as sophisticated as
// it could be.

static int
parse_weights(scha_ssm_lb_args *argp, const char *wstr)
{
	int ret, i, *ip;
	char *str, *in, *wan, *last, *w, *last2, *n, *endp;
	int wv, nv;

	argp->op_data.op_data_len = NODEID_MAX + 1;
	ip = (int *)malloc(sizeof (int) * argp->op_data.op_data_len);
	argp->op_data.op_data_val = ip;

	if (ip == NULL)
		return (0);

	for (i = 0; i <= NODEID_MAX; i++)
		ip[i] = 1;

	str = strdup(wstr);
	if (str == NULL) {
		free(ip);
		return (0);
	}

	// Break up the string into comma-separated pieces then
	// @-separated pieces.  Plug the weight value into the
	// spot in the array for the given node.

	ret = 1;
	in = str;
	while ((wan = strtok_r(in, ",", &last)) != NULL) {
		in = NULL;

		if ((w = strtok_r(wan, "@", &last2)) != NULL &&
		    (n = strtok_r(NULL, "@", &last2)) != NULL) {
			wv = (int)strtol(w, &endp, 10);
			if (endp == w) {
				ret = 0;
				break;
			}
			nv = (int)strtol(n, &endp, 10);
			if (endp == n) {
				ret = 0;
				break;
			}

			if (nv < 1 || nv > NODEID_MAX) {
				ret = 0;
				break;
			}

			ip[nv] = wv;
		} else {
			ret = 0;
			break;
		}
	}

	if (wan == NULL && in != NULL)
		ret = 0;

	if (ret == 0)
		free(ip);
	free(str);

	return (ret);
}

int
set_distribution(int argc, char **argv)
{
	output out(&argc, argv);
	char *res;
	const char *wstr;
	scha_ssm_lb_args arg;
	scha_ssm_lb_result_t result;

	argc--; argv++;

	if (argc != 2) {
		(void) fprintf(out, "usage: set_distribution service-name "
		    "weights\n");
		return (-1);
	}

	res = argv[0];
	wstr = argv[1];

	arg.rs_name = res;
	arg.op_code = SET_DISTRIBUTION;
	if (!parse_weights(&arg, wstr)) {
		(void) fprintf(out, "set_distribution: bad weight: %s\n", wstr);
		return (-1);
	}

	lb_op(&arg, &result);

	free(arg.op_data.op_data_val);

	if (result.ret_code != 0) {
		(void) fprintf(out, "lb_op SET_DISTRIBUTION \"%s\" \"%s\" "
		    "failed with error %d\n", res, wstr, result.ret_code);
		return (-1);
	}

	return (0);
}

int
set_one_distribution(int argc, char **argv)
{
	output out(&argc, argv);
	char *res;
	const char *wstr, *node_str;
	scha_ssm_lb_args arg;
	int arg_ints[2];
	scha_ssm_lb_result_t result;

	argc--; argv++;

	if (argc != 3) {
		(void) fprintf(out, "usage: set_one_distribution service-name "
		    "weight node\n");
		return (-1);
	}

	res = argv[0];
	wstr = argv[1];
	node_str = argv[2];

	arg.op_data.op_data_val = arg_ints;
	arg.op_data.op_data_len = 2;

	arg.rs_name = res;
	arg.op_code = SET_ONE_DISTRIBUTION;
	arg.op_data.op_data_val[0] = atoi(wstr);
	arg.op_data.op_data_val[1] = atoi(node_str);

	lb_op(&arg, &result);

	if (result.ret_code != 0) {
		(void) fprintf(out, "lb_op SET_ONE_DISTRIBUTION \"%s\" \"%s\" "
		    "\"%s\" failed with error %d\n", res, wstr, node_str,
		    result.ret_code);
		return (-1);
	}

	return (0);
}

int
set_perf_mon_status(int argc, char **argv)
{
	output out(&argc, argv);
	char *res;
	const char *lstr, *node_str;
	scha_ssm_lb_args arg;
	int arg_ints[2];
	scha_ssm_lb_result_t result;

	argc--; argv++;

	if (argc != 3) {
		(void) fprintf(out, "usage: set_perf_mon_status service-name "
		    "load node\n");
		return (-1);
	}

	res = argv[0];
	lstr = argv[1];
	node_str = argv[2];

	arg.op_data.op_data_val = arg_ints;
	arg.op_data.op_data_len = 2;

	arg.rs_name = res;
	arg.op_code = SET_PERF_MON_STATUS;
	arg.op_data.op_data_val[0] = atoi(lstr);
	arg.op_data.op_data_val[1] = atoi(node_str);

	lb_op(&arg, &result);

	if (result.ret_code != 0) {
		(void) fprintf(out, "lb_op SET_PERF_MON_STATUS \"%s\" \"%s\" "
		    "\"%s\" failed with error %d\n", res, lstr, node_str,
		    result.ret_code);
		return (-1);
	}

	return (0);
}

int
set_perf_mon_parameters(int argc, char **argv)
{
	output out(&argc, argv);
	char *res;
	const char *frs, *scrs, *miws, *maws, *qws, *fls;
	scha_ssm_lb_args arg;
	int arg_ints[6];
	scha_ssm_lb_result_t result;

	argc--; argv++;

	if (argc != 7) {
		(void) fprintf(out, "usage: set_perf_mon_parameters "
		    "service-name frequency scale_rate min_weight max_weight "
		    "quiet_width floor\n");
		return (-1);
	}

	res = argv[0];
	frs = argv[1];
	scrs = argv[2];
	miws = argv[3];
	maws = argv[4];
	qws = argv[5];
	fls = argv[6];

	arg.op_data.op_data_val = arg_ints;
	arg.op_data.op_data_len = 6;

	arg.rs_name = res;
	arg.op_code = SET_PERF_MON_PARAMETERS;
	arg.op_data.op_data_val[0] = atoi(frs);
	arg.op_data.op_data_val[1] = atoi(scrs);
	arg.op_data.op_data_val[2] = atoi(miws);
	arg.op_data.op_data_val[3] = atoi(maws);
	arg.op_data.op_data_val[4] = atoi(qws);
	arg.op_data.op_data_val[5] = atoi(fls);

	lb_op(&arg, &result);

	if (result.ret_code != 0) {
		(void) fprintf(out, "lb_op SET_PERF_MON_PARAMETERS \"%s\" "
		    "\"%s\" \"%s\" \"%s\" \"%s\" \"%s\" failed with error %d\n",
		    frs, scrs, miws, maws, qws, fls,
		    result.ret_code);
		return (-1);
	}

	return (0);
}

int
get_perf_mon_parameters(int argc, char **argv)
{
	output out(&argc, argv);
	scha_ssm_lb_args arg;
	scha_ssm_lb_result_t result;
	int *ap;

	argc--; argv++;

	if (argc != 1) {
		(void) fprintf(out, "usage: get_perf_mon_parameters "
		    "service-name\n");
		return (-1);
	}

	arg.rs_name = argv[0];
	arg.op_code = GET_PERF_MON_PARAMETERS;

	lb_op(&arg, &result);

	if (result.ret_code != 0) {
		(void) fprintf(out, "lb_op GET_PERF_MON_PARAMETERS \"%s\" "
		    "failed with error %d\n", argv[0], result.ret_code);
		return (-1);
	}

	if (result.ret_data.ret_data_val == NULL ||
	    result.ret_data.ret_data_len != 6) {
		(void) fprintf(out, "get_perf_mon_parameters: "
		    "bad return data\n");
		return (-1);
	}

	ap = result.ret_data.ret_data_val;
	(void) fprintf(out, "freq: %d, scale_rate: %d, min_weight: %d, "
	    "max_weight: %d, quiet_width %d, floor: %d\n",
	    ap[0], ap[1], ap[2], ap[3], ap[4], ap[5]);

	free(result.ret_data.ret_data_val);

	return (0);
}

#endif	// UNODE
