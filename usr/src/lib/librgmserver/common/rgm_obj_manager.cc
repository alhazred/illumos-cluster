//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_obj_manager.cc	1.4	08/05/20 SMI"

#include <stdio.h>
#include <string> //lint !e322 !e7

using namespace std;

template<class OBJ, class T>
ObjManager<OBJ, T>::ObjManager()
{
}


template<class OBJ, class T>
ObjManager<OBJ, T>::~ObjManager()
{
}

//
// The hash table implementation will copy the key (name)
// so the called can safely dispose of it.
//
template<class OBJ, class T>
T
ObjManager<OBJ, T>::addElement(const char *name, OBJ *obj)
{
	string key(name);

	// Insert Object and retrieve value
	T value = array.insertObject(obj);

	// Add name/value mapping to hashtable
	hash[key] = value;
	return (value);
}



template<class OBJ, class T>
OBJ*
ObjManager<OBJ, T>::removeElement(const char *name)
{
	string key(name);

	// lookup the value in the hashtable
	T value = lookup(name);
	if (value == T()) {
		return (NULL);
	}
	// Retrieve Object and free entry
	OBJ *obj = array.removeObject(value);
	hash.erase(key);
	return (obj);
}


template<class OBJ, class T>
T
ObjManager<OBJ, T>::lookup(const char *name)
{
	string key(name);

	iter it = hash.find(key);
	if (it != hash.end()) {
		return (it->second);
	}
	T v = T();
	return (v);
}

template<class OBJ, class T>
OBJ*
ObjManager<OBJ, T>::findObject(T idx)
{
	OBJ *obj = array.getObject(idx);
	return (obj);
}

template<class OBJ, class T>
OBJ*
ObjManager<OBJ, T>::findObject(const char *key)
{
	OBJ *obj = NULL;

	T idx = lookup(key);
	if (idx != T()) {
		CL_PANIC(idx >= 1 && idx <= array.getNbEntries());
		obj = array.getObject(idx);
	}
	return (obj);
}
