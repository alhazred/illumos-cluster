//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_rs_impl.cc	1.213	09/04/22 SMI"

//
// rgm_rs_impl.cc
//
// The functions in this file implement the server side of
// internode communication through the ORB.
//

#ifndef EUROPA_FARM
#include <orb/fault/fault_injection.h>
#include <rgm/rgm_comm_impl.h>
#endif
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>
#ifndef EUROPA_FARM
#include <sys/rsrc_tag.h>
#endif
#include <rgm_proto.h>
#ifndef EUROPA_FARM
#include <rgm_intention.h>
#endif
#include <rgm/rgm_cnfg.h>
#include <rgm_api.h>
#include <scadmin/scconf.h>

#include <sys/sc_syslog_msg.h>
#ifndef EUROPA_FARM
#include <sys/rsrc_tag.h>
#endif
#include <rgm/rgm_msg.h>
#ifndef EUROPA_FARM
#include <rgm/rgm_util.h>
#include <rgm/rgm_errmsg.h>
#include <syslog.h>
#endif
#include <sys/cl_events.h>

#ifndef EUROPA_FARM
#include <nslib/ns_interface.h>
#endif
// Zone support
#include <rgm_logical_nodeset.h>

#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_farm.h>
#endif

#include <rgmx_hook.h>
#include <sys/sol_version.h>
#include <sys/vc_int.h>

#ifndef EUROPA_FARM
typedef struct incarnation_list {
	rgm::ln_incarnation_t	incarnation;
	rgm::lni_t		lni;
	struct incarnation_list	*next;
} incarnation_list_t;
#if (SOL_VERSION >= __s10)
#define	DEPTAG	"ICRD_CYCLIC_DEPCHECK"
#define	DEPTAG_LEN	21
void delete_inter_cluster_dependencies(rlist_p_t rp);
static scha_errmsg_t check_ic_dep_local(rlist_p_t rl, char *src_c_name,
    char *src_rs_name, deptype_t deps_kind, boolean_t *is_ic_local_dep);
scha_errmsg_t update_dependent_info(char *r_name, boolean_t dep_set_flag,
    char *local_rname, deptype_t dep);
static scha_errmsg_t
configure_icrd_helper(name_t remote_r_name,
    deptype_t dep, name_t local_r_name, boolean_t dep_set_flag);
static boolean_t
does_r_link_to_inter_cluster_dependents(rgm_resource_t *resource,
    rgm_resource_t *proposed_r, boolean_t recursive_call_flag = B_FALSE);
static scha_errmsg_t
    add_remote_r_dependencies_to_list(rlist_t *rs_ptr,
    rdeplist_t **icrd_dep_list);
static boolean_t any_icr_dependents_for_this_r(rlist_p_t r_ptr);
#endif
static void save_incarnation(LogicalNodeset *ns,
    incarnation_list_t **incar_list);
static void free_incarnation(incarnation_list_t *incar_list);
static boolean_t incarnation_changed(rgm::lni_t, incarnation_list_t *);
static boolean_t is_depended_on(rlist_p_t r_ptr, scha_errmsg_t *res,
    scha_errmsg_t *warn_res);
//
// updatable R system-defined properties
// If the updatable flag is B_TRUE, property value can be updated by
// 'scrgadm -a|-c -y' command.  Otherwise, the update is not permitted.
// The optional system-defined properties(which are not listed in the
// rs_updatable_props) can be updated if:
//	1. the property is defined in RTR file
//	2. Tunable attribute is set to 'ANYTIME' or
//	   it is set to 'WHEN_DISABLED' and R is in disabled state
//	(the rule #2 does not apply to R creation by 'scrgadm -a -y')
//

static updatable_prop_t rs_updatable_props[] = {
	{SCHA_R_DESCRIPTION,		B_TRUE},
	{SCHA_RESOURCE_DEPENDENCIES,	B_TRUE},
	{SCHA_RESOURCE_DEPENDENCIES_WEAK,	B_TRUE},
	{SCHA_RESOURCE_DEPENDENCIES_RESTART,	B_TRUE},
	{SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART,	B_TRUE},
	// The below four are used for inter-cluster dependencies.
	{SCHA_IC_RESOURCE_DEPENDENTS,	B_TRUE},
	{SCHA_IC_RESOURCE_DEPENDENTS_WEAK,	B_TRUE},
	{SCHA_IC_RESOURCE_DEPENDENTS_RESTART,	B_TRUE},
	{SCHA_IC_RESOURCE_DEPENDENTS_OFFLINE_RESTART,	B_TRUE},
	{SCHA_RESOURCE_PROJECT_NAME,	B_TRUE},
	{SCHA_TYPE,			B_FALSE},
	{SCHA_TYPE_VERSION,		B_TRUE},
	{SCHA_ON_OFF_SWITCH,		B_FALSE},
	{SCHA_MONITORED_SWITCH,		B_FALSE},
	{SCHA_RESOURCE_STATE,		B_FALSE},
	{SCHA_UPDATE_FAILED,		B_FALSE},
	{SCHA_INIT_FAILED,		B_FALSE},
	{SCHA_FINI_FAILED,		B_FALSE},
	{SCHA_BOOT_FAILED,		B_FALSE},
	{SCHA_STATUS,			B_FALSE},
};

#define	RS_UPDATABLE_ENTRIES	\
	(sizeof (rs_updatable_props) / sizeof (updatable_prop_t))

#ifndef EUROPA_FARM
#if (SOL_VERSION >= __s10)

//
// Whenever a notification comes for the r check if this r has the caller r
// in its dependency list. If the caller r does not exist then this is an
// invalid notification. In case this function gets called while checking for
// stopping dependencies the stopping_depend_check will be set. For
// stopping dependencies check should be made on the dependent list
// whereas for starting dependencies check should be made on the
// dependencies list.
//
boolean_t validate_remote_r_request(rlist_p_t rp, char *rc, char *rr,
    deptype_t deps, boolean_t stopping_depend_check) {
	char *temp_r_name = (char *)malloc(strlen(rc) + strlen(rr) + 2);
	sprintf(temp_r_name, "%s:%s", rc, rr);


	switch (deps) {
		case DEPS_STRONG:
			if (stopping_depend_check) {
				if (!rdeplist_contains(rp->rl_ccrdata->
				    r_inter_cluster_dependents.dp_strong,
				    temp_r_name)) {
					free(temp_r_name);
					return (B_FALSE);
				}
			} else {
				if (!rdeplist_contains(rp->rl_ccrdata->
				    r_dependencies.dp_strong, temp_r_name)) {
					free(temp_r_name);
					return (B_FALSE);
				}
			}
			break;
		case DEPS_WEAK:
			if (stopping_depend_check) {
				if (!rdeplist_contains(rp->rl_ccrdata->
				    r_inter_cluster_dependents.dp_weak,
				    temp_r_name)) {
					free(temp_r_name);
					return (B_FALSE);
				}
			} else {
				if (!rdeplist_contains(rp->rl_ccrdata->
				    r_dependencies.dp_weak, temp_r_name)) {
					free(temp_r_name);
					return (B_FALSE);
				}
			}
			break;
		case DEPS_RESTART:
			if (stopping_depend_check) {
				if (!rdeplist_contains(rp->rl_ccrdata->
				    r_inter_cluster_dependents.dp_restart,
				    temp_r_name)) {
					free(temp_r_name);
					return (B_FALSE);
				}
			} else {
				if (!rdeplist_contains(rp->rl_ccrdata->
				    r_dependencies.dp_restart, temp_r_name)) {
					free(temp_r_name);
					return (B_FALSE);
				}
			}
			break;
		case DEPS_OFFLINE_RESTART:
			if (stopping_depend_check) {
				if (!rdeplist_contains(rp->rl_ccrdata->
				    r_inter_cluster_dependents.
				    dp_offline_restart, temp_r_name)) {
					free(temp_r_name);
					return (B_FALSE);
				}
			} else {
				if (!rdeplist_contains(rp->rl_ccrdata->
				    r_dependencies.dp_offline_restart,
				    temp_r_name)) {
					free(temp_r_name);
					return (B_FALSE);
				}
			}
			break;

		default:
			break;
	}
	free(temp_r_name);
	return (B_TRUE);
}


//
// Notification recieved from the remote resource about interesting resource
// state changes.
// If the resource is in starting depend and the depended-on resource
// starts then this idl routine will be called with
// STARTING_DEPENDENCIES_SATISFIED
// If the resource is in stopping depend and the dependent resource
// stops then this idl routine will be called with
// STOPPING_DEPENDENCIES_SATISFIED
// If the resource restarts then this idl routine will be called with
// RESTART_DEPENDENCIES_SATISFIED to notify the dependent
// resource to restart.
//
void
rgm_comm_impl::idl_notify_remote_r(const rgm::idl_rs_get_state_args
	&gets_args, rgm::idl_regis_result_t_out gets_res, Environment &)
{
	char *cluster_name = NULL, *rs_name = NULL, *node_name = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rlist_p_t	rl = NULL;
	rgm::idl_regis_result_t	*retval = new rgm::idl_regis_result_t();
	LogicalNodeset	*restart_nsp = NULL, nset, *ns_nodelistp = NULL;

	boolean_t starting_depend_status_flag = B_FALSE;
	boolean_t stopping_depend_status_flag = B_FALSE;
	boolean_t restart_depend_status_flag = B_FALSE;
	boolean_t restart_local_node_flag = B_FALSE;

	char *src_c_name = strdup(gets_args.idlstr_src_cluster_name);
	char *src_rs_name = strdup(gets_args.idlstr_src_rs_name);
	deptype_t deps_kind = (deptype_t)gets_args.depskind;

	//
	// Check if any starting/stopping/restart dependencies were satisfied
	//

	if (gets_args.flags & rgm::STARTING_DEPENDENCIES_SATISFIED)
		starting_depend_status_flag = B_TRUE;
	if (gets_args.flags & rgm::STOPPING_DEPENDENCIES_SATISFIED)
		stopping_depend_status_flag = B_TRUE;
	if (gets_args.flags & rgm::RESTART_DEPENDENCIES_SATISFIED)
		restart_depend_status_flag = B_TRUE;

	rs_name = strdup_nocheck(gets_args.idlstr_rs_name);
	if (rs_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	cluster_name = strdup_nocheck(gets_args.idlstr_cluster_name);
	if (cluster_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	ucmm_print("idl_notify_remote_r()",
	    NOGET("remote cluster = <%s> remote RS = <%s> dep = %d\n"),
	    cluster_name, rs_name, deps_kind);

	if ((rl = rname_to_r(NULL, rs_name)) == NULL) {
		ucmm_print("idl_notify_remote_r()",
		    NOGET("Resource <%s> does not exist!!\n"), rs_name);
		res.err_code = SCHA_ERR_RSRC;
		rgm_format_errmsg(&res, REM_INVALID_RS, rs_name);
		goto finished; //lint !e801
	}

	// Validate if the notification caller r is a valid dependee resource.
	if (!validate_remote_r_request(rl, src_c_name, src_rs_name,
		deps_kind, stopping_depend_status_flag)) {
		ucmm_print("idl_notify_remote_r()",
		    NOGET("invalid dependee for R <%s> !!\n"), rs_name);
		res.err_code = SCHA_ERR_INVALID_REMOTE_DEP;
		goto finished;
	}
	// check if rg is unmanaged.
	if (rl->rl_rg->rgl_ccr->rg_unmanaged) {
		ucmm_print("RGM", NOGET("DEPENDENCIES: "
		"skipping <%s> "
		"as the containing "
		"resource group is unmanaged\n"),
		rl->rl_ccrdata->r_name);
		goto finished; //lint !e801
	}

	// check if the rg is suspended.
	if (rl->rl_rg->rgl_ccr->rg_suspended) {
		ucmm_print("RGM", NOGET("DEPENDENCIES: "
		"skipping <%s> "
		"as the containing "
		"resource group is suspended\n"),
		rl->rl_ccrdata->r_name);
		goto finished; //lint !e801
	}

	// If restart dependency is satisfied, trigger a resource restart.
	if (restart_depend_status_flag) {
		// check for local node dependencies.
		if (check_ic_dep_local(rl, src_c_name, src_rs_name, deps_kind,
		    &restart_local_node_flag).err_code != SCHA_ERR_NOERR)
		    goto finished; //lint !e801

		// get the recieved nodeset sequence.
		for (int i = 0; i < gets_args.nodeset_seq.length(); i++) {
			nset.addLni(gets_args.nodeset_seq[i]);
		}

		// get the rg nodelist for the resource.
		ns_nodelistp = get_logical_nodeset_from_nodelist(
		    rl->rl_rg->rgl_ccr->rg_nodelist);

		if (deps_kind == DEPS_OFFLINE_RESTART) {
			if (restart_local_node_flag) {
				//
				// The type of dependency is local_node.
				// Trigger restart of the dependent on that
				// node only.
				//
				restart_nsp = new LogicalNodeset(nset);
			} else {

				if (gets_args.r_last_inst_stopped) {
					// Rp is the last instance
					// being stopped.
					restart_nsp =
					    get_logical_nodeset_from_nodelist(
					    rl->rl_rg->rgl_ccr->rg_nodelist);
				}
			}
		} else if (deps_kind == DEPS_RESTART) {
			// get the nodelist for restarting.
			restart_nsp =
			    get_logical_nodeset_from_nodelist(
			    rl->rl_rg->rgl_ccr->rg_nodelist);
		}

		if (restart_nsp != NULL) {
			LogicalNodeset	actual_off_restart;
			int i = 0;
			while ((i =
			    restart_nsp->nextLniSet(i + 1)) != 0) {
				//
				// Even though ONLINE_STANDBY
				// doesn't "officially" count
				// as online, we need to
				// trigger the restart in that
				// state.
				//
				if (!is_r_online(rl->rl_xstate[i].
				    rx_state) &&
				    !(rl->rl_xstate[i].rx_state ==
				    rgm::R_ONLINE_STANDBY)) {
					continue;
				}
				actual_off_restart.addLni(i);
			}

			trigger_restart(rl, actual_off_restart);
			delete (restart_nsp);
			restart_nsp = NULL;
		}
	}

	// Check if starting dependencies are resolved.
	if (starting_depend_status_flag)
		check_blocked_dependencies(rl, B_TRUE, B_FALSE);

	// Check if stopping dependencies are resolved.
	if (stopping_depend_status_flag)
		check_blocked_dependencies(rl, B_FALSE, B_TRUE);
	ucmm_print("idl_notify_remote_r()",
	    NOGET("processed remote rs notification: returning\n"));

finished:

	free(rs_name);
	free(node_name);
	free(cluster_name);
	free(src_rs_name);
	free(src_c_name);
	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);
	free(res.err_msg);
	gets_res = retval;
}


//
// Get the state of a remote resource
// The return value is a string of form (define in rgm_util.h).
//
void
rgm_comm_impl::idl_get_remote_r_state(const rgm::idl_rs_get_state_args
	&gets_args, rgm::idl_rs_get_state_result_t_out gets_res, Environment &)
{
	char *cluster_name = NULL, *rs_name = NULL, *node_name = NULL;
	LogicalNodeset satisfied_ns, unsatisfied_ns, moving_on_ns, nset;
	rglist_p_t rgp_dep = NULL;
	rgm::idl_rs_get_state_result_t	*retval =
	    new rgm::idl_rs_get_state_result_t();
	scha_rsstate_t		state;
	char			*seq_id;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rlist_p_t	rl = NULL;
	boolean_t online_status_flag = B_FALSE;
	boolean_t offline_status_flag = B_FALSE;
	boolean_t starting_depend_status_flag = B_FALSE;
	boolean_t stopping_depend_status_flag = B_FALSE;
	boolean_t online_local_node_status_flag = B_FALSE;
	boolean_t offline_local_node_status_flag = B_FALSE;
	boolean_t online_status_dep_unknown_flag = B_FALSE;
	//
	// Check for the state request to be online,offline,
	// starting depend or stopping depend.
	//

	if (gets_args.flags & rgm::ONLINE_STATUS)
		online_status_flag = B_TRUE;
	if (gets_args.flags & rgm::OFFLINE_STATUS)
		offline_status_flag = B_TRUE;
	if (gets_args.flags & rgm::STARTING_DEPENDENCIES_SATISFIED)
		starting_depend_status_flag = B_TRUE;
	if (gets_args.flags & rgm::STOPPING_DEPENDENCIES_SATISFIED)
		stopping_depend_status_flag = B_TRUE;
	if (gets_args.flags & rgm::ONLINE_STATUS_LOCAL_NODE)
		online_local_node_status_flag = B_TRUE;
	if (gets_args.flags & rgm::ONLINE_STATUS_DEP_LOC_TYPE_UNKNOWN) {
		//
		// If this flag was specified, it implies that the
		// caller was unable to determine the dependency locality type
		// for this resource. The dependency locality type could
		// either be "ANY_NODE" or "LOCAL_NODE". Simce, the caller
		// was unable to decide, he has left the decision to this
		// rgmd president.
		//
		online_status_dep_unknown_flag = B_TRUE;
		//
		// Assert that the other flags for dependency locality types
		// were not specified.
		//
		ASSERT(online_status_flag == B_FALSE &&
		    online_local_node_status_flag == B_FALSE);
	}

	// Set the defalt to offline state.
	retval->status = rgm::R_OFFLINE;
	rs_name = strdup_nocheck(gets_args.idlstr_rs_name);
	if (rs_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished;
	}

	cluster_name = strdup_nocheck(gets_args.idlstr_cluster_name);
	if (cluster_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished;
	}

	ucmm_print("idl_get_remote_r_state()",
	    NOGET("remote cluster = <%s> remote RS = <%s>\n"),
	    cluster_name, rs_name);


	if ((rl = rname_to_r(NULL, rs_name)) == NULL) {
		ucmm_print("idl_get_remote_r_state()",
		    NOGET("Resource <%s> does not exist!!\n"), rs_name);
		res.err_code = SCHA_ERR_RSRC;
		rgm_format_errmsg(&res, REM_INVALID_RS, rs_name);
		goto finished;
	}

	if (rl->rl_rg->rgl_ccr->rg_unmanaged) {
		ucmm_print("idl_get_remote_r_state()", NOGET("DEPENDENCIES: "
		"skipping <%s> "
		"as the containing "
		"resource group is unmanaged\n"),
		rl->rl_ccrdata->r_name);
		goto finished;
	}

	if (online_status_dep_unknown_flag) {
		//
		// If the caller did not know the dependency locality type,
		// then this rgmd has to detect it.
		//
		char *src_cluster_name = NULL;
		char *src_rg_name = NULL;
		boolean_t is_src_r_depen = B_FALSE;
		rglist_p_t r_rg;
		boolean_t affinity_exists = B_FALSE;

		src_cluster_name = strdup(gets_args.idlstr_src_cluster_name);
		src_rg_name = strdup(gets_args.idlstr_src_rg_name);

		//
		// It is the caller rgmd's responsibility to check for
		// explicit LOCAL_NODE or ANY_NODE dependency. This rgmd
		// will be able to detect a LOCAL_NODE dependency only
		// if the source RG has an affinity towards the containing
		// RG of this R and the source R has a dependency on this R.
		// All other combinations will be treated as ANY_NODE dependen-
		// cy. This rgmd can assume that the source R has a dependency
		// on this R since the source R's rgmd will set the
		// online_status_dep_unknown_flag only when it has detected an
		// inter-cluster-r-dependency. However, this rgmd cannot assume
		// anything about the inter-cluster-rg-affinities. This has to
		// be verified here.
		// First, we will look for strong positive affinity.
		//
		res = check_rg_affinity_type((const rglist_p_t)rl->rl_rg,
			    src_rg_name, src_cluster_name, AFF_STRONG_POS,
			    &affinity_exists);
		//
		// Check for errors
		//
		if (res.err_code != SCHA_ERR_NOERR) {
			//
			// If we get an error, then we will not be able
			// to determine the dependency type
			//
			free(src_cluster_name);
			free(src_rg_name);
			goto finished;
		}

		if (!affinity_exists) {
			//
			// There was no strong positive affinity.
			// We have to check for weak positive affinity
			// as well.
			//
			res = check_rg_affinity_type(
			    (const rglist_p_t)rl->rl_rg,
			    src_rg_name, src_cluster_name, AFF_WEAK_POS,
			    &affinity_exists);

			if (res.err_code != SCHA_ERR_NOERR) {
				free(src_cluster_name);
				free(src_rg_name);
				goto finished;
			}
		}

		//
		// If either strong positive or weak positive affinity
		// was present, then "affinity_exists" would be true.
		// We have to set the appropriate flag depending
		// on "affinity_exists".
		//
		if (affinity_exists) {
			online_local_node_status_flag = B_TRUE;
		} else {
			online_status_flag = B_TRUE;
		}

		//
		// Free memory
		//
		free(src_cluster_name);
		free(src_rg_name);
	}

	// if the check is for local node dependencies get the nodeset.
	if (online_local_node_status_flag) {
		for (int i = 0; i < gets_args.nodeset_seq.length(); i++) {
			nset.addLni(gets_args.nodeset_seq[i]);
		}
	}

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);
		// skip disabled resource. Anyways default result is offline.
		rgm::lni_t n = lnNode->getLni();

		rg_xstate_t rg_state = rl->rl_rg->rgl_xstate[n];
		r_xstate_t r_state = rl->rl_xstate[n];
		if (((rgm_switch_t *)rl->rl_ccrdata->r_onoff_switch)->
		    r_switch[it->second] ==
		    SCHA_SWITCH_DISABLED) {
			if ((offline_status_flag) && !is_r_offline(
			    r_state.rx_state)) {
				// The resource is disabled and
				// still heading offline on this
				// node, so we should wait for it.

				unsatisfied_ns.addLni(n);
			}
			continue;
		}

		// If request is for online/offline status.
		if (online_status_flag ||online_local_node_status_flag) {
			// Check if the state is online.
			rg_xstate_t rg_state = rl->rl_rg->rgl_xstate[n];
			r_xstate_t r_state = rl->rl_xstate[n];
			if ((is_r_online(rl->rl_xstate[n].rx_state)) &&
			    !is_rg_offlining(rg_state.rgx_state)) {
				if (get_r_restarting(rs_name,
				    rg_state.rgx_state, n) !=
				    rgm::RR_STOPPING) {
					//
					// Add this node to our list
					// of nodes on which the dependency
					// is satisfied.
					//
					satisfied_ns.addLni(n);

					//
					// For non local-node deps,
					// we only need the deps satisfied
					// on one node.  As an optimization,
					// we break out of the loop here.
					//

					if (!online_local_node_status_flag) {
						break;
					}
				}
			}
			//
			// Check if the resource is coming online
			// on the node.
			//
			rgm::r_restart_t rstart_flag = rgm::RR_NONE;
			(void) call_fetch_restarting_flag(n,
			    rs_name, rstart_flag);
			if ((is_r_starting(r_state.rx_state) &&
			    !is_rg_offlining(rg_state.rgx_state)) ||
			    rstart_flag == rgm::RR_STOPPING) {
				moving_on_ns.addLni(n);
			}
		} // dependencies list loop


		if (offline_status_flag) {
			char *src_c_name = strdup(
			    gets_args.idlstr_src_cluster_name);
			char *src_rs_name = strdup(
			    gets_args.idlstr_src_rs_name);
			deptype_t deps_kind = (deptype_t)gets_args.depskind;
			if (check_ic_dep_local(rl, src_c_name, src_rs_name,
			    deps_kind, &offline_local_node_status_flag).
			    err_code != SCHA_ERR_NOERR) {
				free(src_c_name);
				free(src_rs_name);
				goto finished;
			}
			//
			// Check if the resource is coming online
			// on the node.
			//
			rgm::r_restart_t rstart_flag = rgm::RR_NONE;
			if ((call_fetch_restarting_flag(n,
			    rs_name, rstart_flag) != RGMIDL_OK)) {
				//
				// IDL error; we assume that the slave physical
				// node is down, therefore the stopping
				// dependency is satisfied on that node.
				//
				free(src_c_name);
				free(src_rs_name);
				continue;
			}
			if (rstart_flag == rgm::RR_STOPPING &&
			    !is_r_offline(rl->rl_xstate[n].rx_state)) {
				unsatisfied_ns.addLni(n);
				free(src_c_name);
				free(src_rs_name);
				continue;
			}

			//
			// If the RG is offlining, we need to wait
			// for the resource to become offline.
			//
			if (is_rg_offlining(rg_state.rgx_state)) {
				if (!is_r_offline(r_state.rx_state)) {
					unsatisfied_ns.addLni(n);
				}

			// If the RG is not offline, but for some
			// reason the resource is stopping, wait
			// for it to stop.
			} else if (is_r_stopping(r_state.rx_state)) {
				unsatisfied_ns.addLni(n);
			}

			if (offline_local_node_status_flag) {
				nset -= unsatisfied_ns;
				if (nset.isEmpty()) {
					char *ucmm_buffer = NULL;
					if (unsatisfied_ns.display(
					    ucmm_buffer) == 0) {
						ucmm_print("RGM",
						    NOGET("DEPENDENCIES: "
						    "idl_get_remote_r_state() "
						    "local-node stopping "
						    "dependency "
						    "on <%s> not yet resolved "
						    "on any node."
						    "Dependent moving offline"
						    " on nodes %s."),
						    rs_name, ucmm_buffer);
						free(ucmm_buffer);
					}
				}
			} else if (!unsatisfied_ns.isEmpty()) {
				//
				// For non local-node deps, the dependency must
				// be satisfied on all nodes.
				//

				char *ucmm_buffer = NULL;
				if (unsatisfied_ns.display(ucmm_buffer) == 0) {

					ucmm_print("RGM", NOGET("DEPENDENCIES:"
					    " idl_get_remote_r_state() "
					    "stopping dependency of <%s> on "
					    "<%s> not yet resolved. Dependent "
					    "moving offline on nodes %s\n."),
					    rs_name, src_rs_name, ucmm_buffer);
					free(ucmm_buffer);
				}
			}
			free(src_c_name);
			free(src_rs_name);
		}
	}


	if (online_local_node_status_flag) {
		if (gets_args.depskind != DEPS_WEAK) {
			//
			// If we're not checking weak dependencies,
			// remove all nodes from nset on
			// which the dependency is not satisfied.
			//
			nset &= satisfied_ns;
		} else {
			//
			// We are checking weak deps.  Here we care
			// only that the dependee is not moving
			// online on the nodes of interest.  All
			// other nodes (whether the dependee is
			// online or offline) are fine.
			//
			nset -= moving_on_ns;
		}
		//
		// If we have no more nodes, we can't satisfy
		// deps on any node, so return false.  Note that, as long
		// as we have some nodes in nset, we have the possibility of
		// satisfying dependencies on those nodes.
		//
		if (nset.isEmpty()) {
			ucmm_print("RGM", NOGET("DEPENDENCIES: "
			    "idl_get_remote_r_state() "
			    "local-node dependency of on "
			    "<%s> not resolved on any node."),
			    rs_name);
			goto finished;
		}
	} else {

	//
	// Here we're checking non local-node semantics, so the
	// "satisfaction" is all or nothing (satisfied for all nodes or
	// for none).   Note that if we are checking local-node-weak
	// semantics, we want to do the following check.  We have
	// already handled the more stringent requirement for
	// local-node-weak semantics above.
	//
		if (!satisfied_ns.isEmpty() ||
		    (gets_args.depskind == DEPS_WEAK &&
		    moving_on_ns.isEmpty())) {
			//
			// The dependency is satisfied on all nodes.
			//
			ucmm_print("RGM", NOGET("DEPENDENCIES: "
			    "idl_get_remote_r_state() "
			    "non local-node dependency  on "
			    "<%s> resolved."),
			    rs_name);
		} else {
			//
			// Our dependency is not satisfied.
			// We return immediately.  There's no
			// point in checking other dependencies
			// when one of them is not satisfied.
			//
			ucmm_print("RGM", NOGET("DEPENDENCIES: "
			    "idl_get_remote_r_state() "
			    "non local-node dependency  on "
			    "<%s> not resolved yet."),
			    rs_name);
			goto finished;
		}
	}

	// If request is for online/offline status.
	if (online_status_flag ||online_local_node_status_flag) {
		if (retval->status != rgm::R_ONLINE)
			retval->status = rgm::R_ONLINE;
	}


	ucmm_print("idl_get_remote_r_state()",
	    NOGET("got remote rs state: returning\n"));

finished:

	free(rs_name);
	free(cluster_name);
	free(node_name);
	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);
	free(res.err_msg);
	gets_res = retval;

}
#endif

//
// Get the static state of a resource
// The return value is a string of form (define in rgm_util.h).
//
void
rgm_comm_impl::idl_scha_rs_get_state(const rgm::idl_rs_get_state_status_args
	&gets_args, rgm::idl_get_result_t_out gets_res, Environment &)
{
	char *rg_name = NULL, *rs_name = NULL, *node_name = NULL;
	rgm::idl_get_result_t	*retval = new rgm::idl_get_result_t();
	scha_rsstate_t		state;
	char			*seq_id;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	rs_name = strdup_nocheck(gets_args.idlstr_rs_name);
	rg_name = strdup_nocheck(gets_args.idlstr_rg_name);
	node_name = strdup_nocheck(gets_args.idlstr_node_name);
	if (rs_name == NULL || rg_name == NULL || node_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}
	ucmm_print("idl_scha_rs_get_state()",
	    NOGET("RG = <%s> RS = <%s> NODE = <%s>\n"),
	    rg_name, rs_name, node_name);

	if ((res = rgm_comm_getrsstate(rs_name, rg_name, node_name, &state,
	    &seq_id)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scha_rs_get_state()",
		    NOGET("fail to get RS state: returning\n"));
		goto finished; //lint !e801
	}

	retval->idlstr_seq_id = new_str(seq_id);
	retval->value_list = rgm::idl_string_seq_t(1);	// One value
	//
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via rgm_itoa_cpp).
	//
	retval->value_list[0] = rgm_itoa_cpp(state);
	retval->value_list.length(1);
	ucmm_print("idl_scha_rs_get_state()",
	    NOGET("got rs state: returning\n"));

finished:

	free(rs_name);

	free(rg_name);

	free(node_name);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	gets_res = retval;
}

//
// Get the dynamic state of a resource FM (set by fault monitor)
//

void
rgm_comm_impl::idl_scha_rs_get_status(
	const rgm::idl_rs_get_state_status_args &getst_args,
	rgm::idl_get_status_result_t_out getst_res, Environment &)
{

	scha_rsstatus_t status;
	char *rg_name = NULL, *rs_name = NULL, *node_name = NULL;
	char *seq_id = NULL, *status_msg = NULL;
	rgm::idl_get_status_result_t	*retval =
	    new rgm::idl_get_status_result_t();
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	rs_name = strdup_nocheck(getst_args.idlstr_rs_name);
	rg_name = strdup_nocheck(getst_args.idlstr_rg_name);
	node_name = strdup_nocheck(getst_args.idlstr_node_name);

	if (rs_name == NULL || rg_name == NULL || node_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	if ((res = rgm_comm_getfmstatus(rs_name, rg_name, node_name, &status,
	    &status_msg, &seq_id)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scha_rs_get_status()",
		    NOGET("fail to get RS status: returning\n"));
		goto finished; //lint !e801
	}

	retval->idlstr_seq_id = new_str(seq_id);
	//
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via rgm_itoa_cpp).
	//
	retval->idlstr_status = rgm_itoa_cpp(status);
	if (status_msg == NULL) {
		retval->idlstr_status_msg = (char *)NULL;
	} else {
		retval->idlstr_status_msg = new_str(status_msg);
	}

finished:

	free(rs_name);

	free(rg_name);

	free(node_name);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	getst_res = retval;
}

//
// Get the On_off/Monitored switch of a resource on a specified node
//
void
rgm_comm_impl::idl_scha_rs_get_switch(
	const rgm::idl_rs_get_switch_args &getst_args,
	rgm::idl_get_switch_result_t_out getst_res, Environment &)
{

	scha_switch_t swtch;
	bool on_off;
	char *rg_name = NULL, *rs_name = NULL, *node_name = NULL;
	char *seq_id = NULL;
	rgm::idl_get_switch_result_t	*retval =
	    new rgm::idl_get_switch_result_t();
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	rs_name = strdup_nocheck(getst_args.idlstr_rs_name);
	rg_name = strdup_nocheck(getst_args.idlstr_rg_name);
	node_name = strdup_nocheck(getst_args.idlstr_node_name);
	on_off = (bool)getst_args.on_off;

	if (rs_name == NULL || rg_name == NULL || node_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	if ((res = rgm_comm_getrsswitch(rs_name, rg_name, node_name, &swtch,
	    on_off, &seq_id)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scha_rs_get_switch()",
		    NOGET("fail to get RS On_off/Monitored switch: "
		    "returning\n"));
		goto finished; //lint !e801
	}

	retval->idlstr_seq_id = new_str(seq_id);
	//
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via rgm_itoa_cpp).
	//
	retval->idlstr_switch = rgm_itoa_cpp(swtch);

finished:

	free(rs_name);

	free(rg_name);

	free(node_name);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	getst_res = retval;
}

//
// Get the extension property valueon a specified node
//
void
rgm_comm_impl::idl_scha_rs_get_ext(
	const rgm::idl_rs_get_args &rs_args,
	rgm::idl_rs_get_ext_result_t_out rs_res, Environment &)
{

	char *rg_name = NULL, *rs_name = NULL, *node_name = NULL,
	    *prop_name = NULL;
	char *seq_id = NULL;
	char *value = NULL;

	rgm::idl_rs_get_ext_result_t	*retval =
	    new rgm::idl_rs_get_ext_result_t();

	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	rs_name = strdup_nocheck(rs_args.idlstr_rs_name);
	rg_name = strdup_nocheck(rs_args.idlstr_rg_name);
	node_name = strdup_nocheck(rs_args.idlstr_rs_node_name);
	prop_name = strdup_nocheck(rs_args.idlstr_rs_prop_name);


	if (rs_name == NULL || rg_name == NULL || node_name == NULL ||
	    prop_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	if ((res = rgm_comm_getext(rs_name, rg_name, node_name, prop_name,
	    &value, &seq_id)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scha_rs_get_ext()",
		    NOGET("fail to get RS pernode extension property %s:\n "
		    "returning\n"), prop_name);
		goto finished; //lint !e801
	}

	retval->idlstr_seq_id = new_str(seq_id);
	retval->prop_val = value;

finished:

	free(rs_name);

	free(rg_name);

	free(node_name);

	free(prop_name);

	retval->ret_code = res.err_code;

	rs_res = retval;
}


//
// Get the state flag indicating failure of a specified method
// (BOOT, FINI, INIT or UPDATE)
//

void
rgm_comm_impl::idl_scha_rs_get_fail_status(const
	rgm::idl_rs_get_fail_status_args &getf_args,
	rgm::idl_get_result_t_out getf_res, Environment &)
{
	char *seq_id = NULL;
	char *rg_name = NULL, *rs_name = NULL;
	char *node_name = NULL, *meth_name = NULL;
	rgm::idl_get_result_t	*retval = new rgm::idl_get_result_t();
	boolean_t	failstatus;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	rs_name = strdup_nocheck(getf_args.idlstr_rs_name);
	rg_name = strdup_nocheck(getf_args.idlstr_rg_name);
	node_name = strdup_nocheck(getf_args.idlstr_node_name);
	meth_name = strdup_nocheck(getf_args.idlstr_method_name);

	if (rs_name == NULL || rg_name == NULL || node_name == NULL ||
	    meth_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	ucmm_print("idl_scha_rs_get_fail_status()", NOGET(
	    "RG = <%s> RS = <%s> NODE = <%s>  METH = <%s>\n"),
	    rg_name, rs_name, node_name, meth_name);

	if ((res = rgm_comm_getfailedstatus(rs_name, rg_name, node_name,
	    meth_name, &failstatus, &seq_id)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scha_rs_get_fail_status()",
		    NOGET("fail to get RS status: returning\n"));
		goto finished; //lint !e801
	}

	retval->idlstr_seq_id = new_str(seq_id);
	// XXX 4215776 Implement FYI states in rgmd
	// XXX What are we supposed be returning?
	// maybe the FYI states of method failure?
	// AH: Yes, we need to match the method name with
	// the right FYI status flag. The "encoding" of that
	// is TBD. could be an enum or TRUE/FALSE string.

	retval->value_list = rgm::idl_string_seq_t(1);	// One value
	//
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via rgm_itoa_cpp).
	//
	retval->value_list[0] = rgm_itoa_cpp(failstatus);
	retval->value_list.length(1);
	ucmm_print("idl_scha_rs_get_fail_status()",
	    NOGET("got rs status: returning\n"));

finished:

	free(rs_name);

	free(rg_name);

	free(node_name);

	free(meth_name);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	getf_res = retval;

}
#endif /* !EUROPA_FARM */


//
// internal functions for sanity check resource property value in paramtable
//

static scha_errmsg_t rs_sanity_checks(rgm_resource_t *r,
    rgm_param_t **paramtable, boolean_t dep_updated, boolean_t globalflag,
    const char *zonename);
static scha_errmsg_t validate_prop_int(std::map<rgm::lni_t, char *> &value,
    rgm_param_t *param, LogicalNodeset *ns);
static scha_errmsg_t validate_prop_bool(std::map<rgm::lni_t, char *> &value,
    rgm_param_t *param, LogicalNodeset *ns);
static scha_errmsg_t validate_prop_string(std::map<rgm::lni_t, char *> &value,
    rgm_param_t *param, LogicalNodeset *ns);
static scha_errmsg_t validate_prop_strarray(const namelist_t *values,
	rgm_param_t *param);
static scha_errmsg_t validate_prop_enum(std::map<rgm::lni_t, char *> &value,
    rgm_param_t *param, LogicalNodeset *ns);
static scha_errmsg_t validate_rs_prop(const rgm_property_t *prop,
    rgm_param_t *param, LogicalNodeset *ns);


/*
 * helper function to fill in the fields of an rgm_resource_t.
 */
static scha_errmsg_t update_hard_coded_props(
    const rgm::idl_nameval_seq_t &props, rgm_resource_t *rs,
    boolean_t inter_cluster_set_dep,
    boolean_t inter_cluster_unset_dep);
static scha_errmsg_t update_rs_props(const rgm::idl_nameval_seq_t &props,
    const rgm::idl_nameval_node_seq_t &xprops, rgm_resource_t **rs,
    boolean_t rt_upgrade, boolean_t inter_cluster_set_dep,
    boolean_t inter_cluster_unset_dep, LogicalNodeset ns);

/*
 * Helper functions to support cascading dependencies.
 */

static scha_errmsg_t apply_user_props(const rgm::idl_nameval_seq_t &props,
    const rgm::idl_nameval_node_seq_t &xprops, rgm_param_t **paramtable,
    rgm_resource_t **rs, boolean_t rt_upgrade, boolean_t inter_cluster_set_dep,
    boolean_t inter_cluster_unset_dep, LogicalNodeset e_ns);
static scha_errmsg_t create_rgm_property(const rgm::idl_nameval &one_prop,
    const rgm::idl_nameval_node &one_prop_ext, boolean_t is_extension,
    rgm_param_t **paramtable, rgm_property_list_t **prop_struct_p,
    rgm_resource_t **rs, LogicalNodeset e_ns);
static scha_errmsg_t add_r_property(const rgm::idl_nameval &one_prop,
    const rgm::idl_nameval_node &one_prop_ext,
    rgm_property_list_t *prop_struct, rgm_resource_t **rs,
    boolean_t is_ext_prop, LogicalNodeset e_ns);

static scha_errmsg_t check_rs_updatable_props(
    const rgm::idl_nameval_seq_t &props,
    const rgm::idl_nameval_node_seq_t &ext_props, rgm_param_t **paramtable,
    std::map<rgm::lni_t, scha_switch_t> &r_onoff_switch, boolean_t update_op,
    nodeidlist_t *, LogicalNodeset *e_ns);
static scha_errmsg_t check_tunable_flag(const char *prop_name,
    rgm_param_t **paramtable,
    std::map<rgm::lni_t, scha_switch_t> &r_onoff_switch,
    boolean_t update_op, boolean_t extension, LogicalNodeset *e_ns);
static scha_errmsg_t check_rs_defaults(const rgm::idl_nameval_seq_t &props,
    const rgm::idl_nameval_node_seq_t &ext_props, rgm_param_t **paramtable);
static scha_errmsg_t check_scalable_props(const rgm::idl_nameval_seq_t &props,
    boolean_t is_scalable, rgm_property_list_t *r_props,
    scha_errmsg_t *warning_res, const char *rs_name);
static scha_errmsg_t check_client_affinity_props(
    const rgm::idl_nameval_seq_t &props, char *prop_name,
    boolean_t is_scalable, boolean_t is_lbp_sticky, scha_errmsg_t *warning_res,
    const char *rs_name);
static scha_errmsg_t check_rr_prop(const rgm::idl_nameval_seq_t &props,
    char *prop_name, boolean_t is_scalable, boolean_t is_rr_enabled,
    scha_errmsg_t *warning_res, const char *rs_name);
static boolean_t is_sysprop(char *prop_name, rgm_param_t **paramtable);
static boolean_t is_extprop(char *prop_name, rgm_param_t **paramtable);
const rgm_param_t *get_param_prop(const char *prop_name,
    boolean_t is_extension, rgm_param_t **paramtable);
static boolean_t is_upgrade(const rgm::idl_nameval_seq_t &props);
static boolean_t is_dependency_updated(const rgm::idl_nameval_seq_t &props,
	boolean_t *strong_dep, boolean_t *weak_dep, boolean_t *restart_dep,
	boolean_t *off_restart_dep);
static scha_errmsg_t create_upgrade_rtname(const rgm::idl_nameval_seq_t &props,
    name_t current_rt, name_t *upgrade_rt);
static scha_errmsg_t is_upgrade_allowed(rgm_rt_t *upgrade_rt,
    rgm_rt_t *source_rt, rlist_p_t rp, boolean_t *upgrade_allowed);
static void delete_properties(rgm_resource_t *rs_conf,
    rgm_param_t **paramtable);
static boolean_t is_resource_offline(rlist_p_t rp);
static void switch_modify(std::map<rgm::lni_t, scha_switch_t> &, scha_switch_t,
    LogicalNodeset *ns);
static boolean_t is_r_scalable_prop_defined(rgm_resource_t *r);
//
// Add a resource to an RG.
//

void
rgm_comm_impl::idl_scrgadm_rs_add(const rgm::idl_rs_add_args &rs_args,
	rgm::idl_regis_result_t_out rs_res, Environment &)
{
	char			*rg_name = NULL;
	char			*rs_name = NULL;
	char			*rt_name = NULL;
	char			*locale = NULL;
	rglist_p_t		rgp = NULL;
	rlist_p_t		rp = NULL;
	rgm_rt_t		*rt = NULL;
	rgm_resource_t		*new_rs = NULL;
	rgm_resource_t		*new_rs_no_defaults = NULL;
	rgm::idl_validate_args	val_args;
	LogicalNodeset		*ns = NULL;
	uint_t			ext_prop_len = 0;
	LogicalNodeset		e_ns;
	rgm::lni_t		lni;
	rgm::rgm_comm_ptr	prgm_serv;
	Environment		e;
	LogicalNodeset		pending_boot_ns;
	LogicalNodeset		stop_failed_ns;
	LogicalNodeset		*switch_ns = NULL;
	LogicalNodeset		r_restart_ns;
	LogicalNodeset		*nset = NULL;
	scha_err_t		validate_err_code;
	scha_err_t		ret;
	boolean_t		validate_done;
	sc_syslog_msg_handle_t	handle;
	rgm::idl_regis_result_t	*retval = new rgm::idl_regis_result_t();
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t		validate_res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t		warning_res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t		temp_res = {SCHA_ERR_NOERR, NULL};
	boolean_t		dep_updated = B_FALSE;

	// Flags to check if any inter-cluster dependencies are updated.
	boolean_t		dep_strong_updated = B_FALSE;
	boolean_t		dep_weak_updated = B_FALSE;
	boolean_t		dep_restart_updated = B_FALSE;
	boolean_t		dep_off_restart_updated = B_FALSE;

	boolean_t		is_scalable;
	boolean_t		is_scalable_prop_defined;
	boolean_t		tag_to_clear = B_FALSE;
	idlretval_t		idlretval;
	const boolean_t		rt_upgrade = B_FALSE;
	LogicalNodeset		emptyNodeset;
	LogicalNodeset		*ns_nodeset;
	rgm::idl_regis_result_t_var rs_validate_v;
	std::map<rgm::lni_t, scha_switch_t> switch_tmp;
	bool			is_farmnode;
	boolean_t		globalflag;
	boolean_t		nodelist_contains;
	rgm_dependencies_t r_deps;
	rgm_lock_state();

	validate_err_code = SCHA_ERR_NOERR;
	validate_done = B_FALSE;

	rg_name = strdup_nocheck(rs_args.idlstr_rg_name);
	rs_name = strdup_nocheck(rs_args.idlstr_rs_name);
	rt_name = strdup_nocheck(rs_args.idlstr_rt_name);
	locale = strdup_nocheck(rs_args.locale);
	if (rs_name == NULL || rg_name == NULL || rt_name == NULL ||
	    locale == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto rs_add_end; //lint !e801
	}

	ucmm_print("idl_scrgadm_rs_add()",
	    NOGET("RG = <%s> RS <%s> RT <%s>\n"),
	    rg_name, rs_name, rt_name);

	rgp = rgname_to_rg(rg_name);
	if (rgp == NULL) {
		res.err_code = SCHA_ERR_RG;
		rgm_format_errmsg(&res, REM_INVALID_RG, rg_name);
		ucmm_print("idl_scrgadm_rs_add()",
		    NOGET("Invalid RG = <%s>\n"), rg_name);
		goto rs_add_end; //lint !e801
	}
	ngzone_nodelist_check(rgp, rs_args.nodeid, rs_args.idlstr_zonename,
	    globalflag, nodelist_contains);
	if (!globalflag) {
		if (!nodelist_contains) {
			res.err_code = SCHA_ERR_ACCESS;
			rgm_format_errmsg(&res, REM_R_NODELIST_NGZONE,
			    rs_name, (const char *)rs_args.idlstr_zonename,
			    rg_name);
			goto rs_add_end; //lint !e801
		}
	}

	//
	// The resource name should be unique across the cluster.
	// Search in all resource groups to see if the resource name
	// already exists.
	//
	rp = rname_to_r(NULL /* search all RGs */, rs_name);
	if (rp != NULL) {
		ucmm_print("idl_scrgadm_rs_add()",
		    NOGET("R <%s> already exists\n"), rs_name);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RS_EXISTS, rs_name);
		goto rs_add_end; //lint !e801
	}

	// The RG must be in an "endish" state.
	if (!in_endish_rg_state(rgp, &pending_boot_ns, &stop_failed_ns,
	    NULL, &r_restart_ns) || !pending_boot_ns.isEmpty() ||
	    !r_restart_ns.isEmpty()) {
		// The RG is busy or running BOOT methods on some node
		res.err_code = SCHA_ERR_RGRECONF;
		rgm_format_errmsg(&res, REM_RG_BUSY, rg_name);
		ucmm_print("RGM",
		    NOGET("idl_scrgadm_rs_add(): RG <%s> is not in an endish"
		    " state\n"), rg_name);
		goto rs_add_end; //lint !e801
	}
	// ERROR_STOP_FAILED is an endish state but still precludes RG update
	if (!stop_failed_ns.isEmpty()) {
		// The RG is ERROR_STOP_FAILED on some node
		res.err_code = SCHA_ERR_STOPFAILED;
		rgm_format_errmsg(&res, REM_RS_IN_RG_TO_STOPFAILED, rs_name,
		    rg_name);
		ucmm_print("RGM",
		    NOGET("idl_scrgadm_rs_add(): RG <%s> is "
		    "ERROR_STOP_FAILED\n"), rg_name);
		goto rs_add_end; //lint !e801
	}

	//
	// "system" property checks:
	//
	//	Can't add an R to a "system" RG
	//
	if (rgp->rgl_ccr->rg_system) {
		res.err_code = SCHA_ERR_ACCESS;
		rgm_format_errmsg(&res, REM_SYSTEM_RES, rs_name, rg_name);
		ucmm_print("idl_scrgadm_rs_add()",
		    NOGET("Can't add R <%s> to RG <%s> whose RG_SYSTEM "
		    "property is TRUE\n"), rs_name, rg_name);
		goto rs_add_end; //lint !e801
	}

	//
	// Set a flag on rg indicating that it is being updated
	//  This acts as an advisory lock preventing other updates/switches.
	//  Note that this causes "is rg in an end-ish state?" to be false.
	//  [We assert that no other thread on this president can change the
	//  state of this RG or of any of its resources until we clear
	//  this flag.]
	// If there are any failures after this point, we need to
	// go to rs_add_end_clr_sw to reset the switching flag
	// back to none.
	//
	rgp->rgl_switching = SW_UPDATING;

	// Find 'in memory' struct for RT, otherwise add it.
	rt = rtname_to_rt(rt_name);
	if (rt == NULL) {
		ucmm_print("idl_scrgadm_rs_add()",
		    NOGET("Invalid RT = <%s>\n"), rt_name);
		res.err_code = SCHA_ERR_RT;
		goto rs_add_end_clr_sw; //lint !e801
	}

	//
	// It is not permitted to add a "Global_zone=TRUE" resource
	// from within a non-global zone.
	// We will check for this later, in rs_sanity_checks(), so that
	// we can take into account the setting of Global_zone_override.
	//

	//
	// Enforce 'Single_instance' resource type property (Bug 4312914).
	//  If the RT for this resource has 'Single_instance' set (true),
	//  then check for any existing resource of that type in cluster
	//  We consider the types equal if the vendor_id.rt_name portions
	//  match excluding the :version suffix
	//
	if (rt->rt_single_inst != B_FALSE) {
		rglist_p_t rgpx = NULL;
		rlist_p_t rpx = NULL;
		char *dup_r_type = NULL;
		char *dup_r_name = NULL;
		char *version_ptr = NULL;
		size_t len1, len2;
		// Get pointer to version suffix of rt_name if any
		version_ptr = strchr(rt_name, ':');
		// Compute the length excluding the version suffix
		if (NULL != version_ptr) {
			len1 = (size_t)(version_ptr - rt_name);
		} else {
			len1 = strlen(rt_name);
		}
		for (rgpx = Rgm_state->rgm_rglist; rgpx != NULL;
		    rgpx = rgpx->rgl_next) {
			for (rpx = rgpx->rgl_resources; rpx != NULL;
			    rpx = rpx->rl_next) {
				// get the type and name of resource
				dup_r_type = rpx->rl_ccrdata->r_type;
				dup_r_name = rpx->rl_ccrdata->r_name;
				// get pointer to version suffix if any
				version_ptr = strchr(dup_r_type, ':');
				// compute the length excluding version suffix
				if (NULL != version_ptr) {
					len2 = (size_t)
					    (version_ptr - dup_r_type);
				} else {
					len2 = strlen(dup_r_type);
				}
				// Check for duplicates
				if (len2 != len1 ||
				    strncmp(rt_name, dup_r_type, len1) != 0) {
					continue;
				}
				// found existing R of this type,
				//  so can't add new instance
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_SINGLE_INST,
				    rt_name, dup_r_name);
				ucmm_print("RGM",
				    NOGET("idl_scrgadm_rs_add(): RT <%s> "
				    "has Single_instance=TRUE, and "
				    "another R <%s> of this type already "
				    "exists\n"),
				    rt_name, dup_r_name);
				goto rs_add_end_clr_sw; //lint !e801
			}
		}
	}

	//
	// Each node in the RG nodelist must appear in the
	// RT installed nodes list.
	//
	if ((res = rgnodelist_subset_rtnodelist(rg_name,
	    rgp->rgl_ccr->rg_nodelist, rt)).err_code != SCHA_ERR_NOERR) {
		goto rs_add_end_clr_sw; //lint !e801
	}

	//
	// check if system-defined, optional system-defined or extension
	// properties exist in R and can be updated
	//

	switch_tmp[0] = SCHA_SWITCH_DISABLED;
	if ((res = check_rs_updatable_props(rs_args.prop_val,
	    rs_args.ext_prop_val, rt->rt_paramtable, switch_tmp,
	    B_FALSE, rgp->rgl_ccr->rg_nodelist, &e_ns)).
	    err_code != SCHA_ERR_NOERR)
		goto rs_add_end_clr_sw; //lint !e801

	//
	// For the properties that do not have default
	// values, check to see that the default values are set on the
	// scrgadm command line when the resource is created
	//
	if ((res = check_rs_defaults(rs_args.prop_val, rs_args.ext_prop_val,
	    rt->rt_paramtable)).err_code != SCHA_ERR_NOERR)
		goto rs_add_end_clr_sw; //lint !e801

	// call rgm_resource_tmpl() to generate a resource struct in which
	// each property is set to a default value based on the CCR table
	// of rt_name.

	res = rgmcnfg_resource_tmpl((const char *)rs_name,
	    (const char *)rg_name, rt_name, rt, &new_rs);

	// Update the <lni_t, char *> map with the data in the <char *, char *>
	// map.
	update_pn_default_value(new_rs->r_ext_properties,
	    rgp->rgl_ccr->rg_nodelist);
	if (res.err_code != SCHA_ERR_NOERR) {
		goto rs_add_end_clr_sw; //lint !e801
	}

	// Also get a resource template that does not have the defaults.
	// We keep parallel resource templates, so that we can do sanity
	// checks and run validate against the one with defaults, but save
	// the one without defaults to the CCR, in order to support cascading
	// defaults for RT upgrades.

	res = rgmcnfg_resource_tmpl_empty(rs_name,
	    rg_name, rt_name, rt,
	    &new_rs_no_defaults);
	if (res.err_code != SCHA_ERR_NOERR) {
		goto rs_add_end_clr_sw; //lint !e801
	}
	//
	// Fill in user-specified properties to new_rs
	//
	if ((res = update_rs_props(rs_args.prop_val, rs_args.ext_prop_val,
	    &new_rs, rt_upgrade, B_FALSE, B_FALSE, e_ns)).err_code !=
	    SCHA_ERR_NOERR) {
		ucmm_print("idl_scrgadm_rs_add()",
		    NOGET("invalid property or property value "
		    "(from update_rs_props)"));
		goto rs_add_end_clr_sw; //lint !e801
	}

	//
	// Fill in the user-specified properties in the new_rs_no_defaults
	// as well.  Note that we need to call apply_user_props instead
	// of update_rs_props, because they work slightly differently.
	// update_rs_props expects rgm_property_list_t structure for
	// each possible property, while apply_user_props does not.
	//
	if ((res = apply_user_props(rs_args.prop_val, rs_args.ext_prop_val,
	    rt->rt_paramtable, &new_rs_no_defaults, B_FALSE, B_FALSE,
	    B_FALSE, e_ns)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scrgadm_rs_add()",
		    NOGET("invalid property or property value "
		    "(from apply_user_props)"));
		goto rs_add_end_clr_sw; //lint !e801
	}

	//
	// Store warning message if there was any.We add this message
	// if the creation of the resource is succesful.
	//
	(void) transfer_message(&temp_res, &res);
	if (temp_res.err_code == SCHA_ERR_NOMEM)
		// Low memory, bailout
		goto rs_add_end; //lint !e801
	//
	// If the system property list does not contain any of the three
	// resource dependencies or inter-cluster dependencies or
	// network_resources_used, then the dependency related checks in
	// rs_sanity_checks need not be performed.
	//
	dep_updated = is_dependency_updated(rs_args.prop_val,
	    &dep_strong_updated, &dep_weak_updated,
	    &dep_restart_updated, &dep_off_restart_updated);

	// Do generic sanity check on the properties with the new values
	if ((res = rs_sanity_checks(new_rs, rt->rt_paramtable,
	    dep_updated, globalflag, (const char *)rs_args.idlstr_zonename)).
	    err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scrgadm_rs_add()",
		    NOGET("Invalid property or property value "
		    "(from rs_sanity_checks)"));
		goto rs_add_end_clr_sw; //lint !e801
	}

	//
	// Check if there are any RG-node cycles using both rg_dependencies
	// and inter-rg resource dependencies as edges.
	//
	// We don't put this call in rs_sanity_checks because we need the
	// rgp ptr.
	//
	if ((res = detect_combined_dependencies_cycles(rgp, rgp->rgl_ccr,
	    new_rs)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scrgadm_rs_add()",
		    NOGET("Invalid dependency setting "
		    "(from detect_combined_dependencies_cycles)"));
		goto rs_add_end_clr_sw; //lint !e801
	}

	is_scalable = is_r_scalable(new_rs);
	is_scalable_prop_defined = is_r_scalable_prop_defined(new_rs);

	// Check scalable related properties.
	if ((res = check_scalable_props(rs_args.prop_val,
	    is_scalable_prop_defined, new_rs->r_properties, &warning_res,
	    rs_name)).err_code != SCHA_ERR_NOERR)
		goto rs_add_end_clr_sw; //lint !e801

	//
	// If the resource type has registered MONITOR_START or
	// MONITOR_CHECK, set r_monitored_switch to ENABLED.
	//
	// Method names are stored in the RT and method timeout values
	// are stored in the R.  So, we check MONITOR_START_TIMEOUT
	// and MONITOR_CHECK_TIMEOUT on the R rather than MONITOR_START and
	// MONITOR_CHECK.
	//
	// we look for the properties in the system property list
	// of the new_rs (the one with the defaults!).
	//
	// If the resource is scalable, r_monitored_switch is enabled by
	// default even if no monitoring methods were registered, because
	// the ssm_wrapper implements monitor_start/stop/check.
	//
	// Need to set both new_rs and new_rs_no_defaults, because we
	// check against new_rs, but save new_rs_no_defaults to the CCR.
	//
	// Note that this check was moved here from rgmcnfg_resource_tmpl
	// in order to support cascading defaults of properties. We cannot
	// do the check in rgmcnfg_resource_tmpl for two reasons:
	// 1. We need to keep parallel resources (with and without defaults),
	// and both of them need the r_monitored_switch set properly.
	// 2. The code in rgmcnfg_resource_tmpl needs to be more generic
	// than it was previously.
	//
	if (is_scalable || rgm_find_property_in_list(
	    SCHA_MONITOR_START_TIMEOUT, new_rs->r_properties) != NULL ||
	    rgm_find_property_in_list(
	    SCHA_MONITOR_CHECK_TIMEOUT, new_rs->r_properties) != NULL) {
		switch_ns = get_logical_nodeset_from_nodelist(
		    rgp->rgl_ccr->rg_nodelist);
		switch_modify(((rgm_switch_t *)
		    new_rs->r_monitored_switch)->r_switch,
		    SCHA_SWITCH_ENABLED, switch_ns);
		switch_modify(((rgm_switch_t *)
		    new_rs_no_defaults->r_monitored_switch)->r_switch,
		    SCHA_SWITCH_ENABLED, switch_ns);
	}


	// Find the set of nodes on which VALIDATE method is supposed to run
	ns = compute_meth_nodes(rt, rgp->rgl_ccr, METH_VALIDATE, is_scalable);

	//
	// If there is no validate method defined for this R, ns will be empty
	// and there is no need to execute validate methods.
	//
	if (!ns->isEmpty()) {
		//
		// VALIDATE has to be called successfully all nodes in ns that
		// are
		// cluster members, or else we will return early with an error.
		// We will execute VALIDATE on each node in ns and block waiting
		// for completion.  If any of these calls fails, return early
		// with an error.  If any nodes in ns are not in the cluster
		// membership,
		// we will succeed (provided at least one validate call
		// succeeds) but will return a warning message.
		//

		//
		// NOTE:
		// We allocated val_args on the stack.  Since this structure is
		// an IDL IN parameter, we, the caller, are responsible for the
		// storage.  When we leave the scope of this routine, its
		// storage will be automatically freed.  This includes all
		// the storage allocated in r_to_strseq().
		//

		val_args.idlstr_rs_name = new_str(rs_name);
		val_args.idlstr_rt_name = new_str(rt_name);
		val_args.idlstr_rg_name = new_str(rg_name);
		val_args.locale = new_str(locale);
		val_args.opcode = rgm::RGM_OP_CREATE;
		val_args.is_scalable = (bool)is_scalable;

		//
		// extract the all resource properties of new_rs, and
		// store in two string arrays,
		// val_args.rs_props for pre-defined properties
		// val_args.ext_props for extension properties
		// each element is in a format of "<key>=<val,...>"
		//

		ns_nodeset = get_logical_nodeset_from_nodelist(
		    rgp->rgl_ccr->rg_nodelist);
		r_to_strseq(new_rs, val_args.rs_props, val_args.ext_props,
		    val_args.all_nodes_ext_props, ns_nodeset);

		// extract all the rg properties of the rg for new_rs

		rg_to_strseq(rgp->rgl_ccr, val_args.rg_props);

		//
		// All per-node extension properties being updated are
		// passed with a -X flag to the validate method.  These are
		// stored in val_args.all_nodes_ext_props.
		//
		(void) update_all_nodes_val_add(new_rs,
		    val_args.all_nodes_ext_props);

		ext_prop_len = val_args.ext_props.length();

		lni = 0;
		while ((lni = ns->nextLniSet(lni + 1)) != 0) {
			//
			// If the node isn't in the current membership, skip it
			// but set error code SCHA_ERR_RS_VALIDATE in
			// validate_err_code.
			// This error code will be returned
			// if there is no other error in the function.
			//
			LogicalNode *ln = lnManager->findObject(lni);
			CL_PANIC(ln != NULL);
#if (SOL_VERSION >= __s10)
			if (strcmp(ZONE, GLOBAL_ZONENAME) == 0) {
#endif
				if (!ln->isInCluster()) {
					validate_err_code =
					    SCHA_ERR_RS_VALIDATE;
					continue;
				}
#if (SOL_VERSION >= __s10)
			} else {
				if (!ln->isInZoneCluster()) {
					validate_err_code =
					    SCHA_ERR_RS_VALIDATE;
					continue;
				}
			}
#endif
			//
			// Before adding the local-node -x options, reset
			// val_args.ext_props back to its starting length.
			// That way, for each node, we only add the local-node
			// -x options corresponding to that node.
			//
			val_args.ext_props.length(ext_prop_len);

			//
			// The per-node extension property being set on a
			// particular node would be passed to the validate
			// callback as the conventional -x option.
			// Note that this is in addition to the same value
			// being passed with a -X option.
			//
			update_local_node_value_add(new_rs,
			    val_args.ext_props, lni, ext_prop_len);

			//
			// Release the global state machine
			// mutex while running validate
			//
			rgm_unlock_state();
			if ((rgmx_hook_call(rgmx_hook_scrgadm_rs_validate,
				ln->getNodeid(),
				lni, ln->getIncarnation(),
				&val_args, &rs_validate_v, &idlretval,
				&is_farmnode) == RGMX_HOOK_ENABLED) &&
				(is_farmnode)) {
				rgm_lock_state();
				if (idlretval != RGMIDL_OK) {
					validate_err_code ==
					    SCHA_ERR_RS_VALIDATE;
					continue;
				}
			} else {
				prgm_serv = rgm_comm_getref(ln->getNodeid());
				if (CORBA::is_nil(prgm_serv)) {
					validate_err_code =
					    SCHA_ERR_RS_VALIDATE;
					rgm_lock_state();
					// This node is not in the cluster
					continue;
				}

				prgm_serv->idl_scrgadm_rs_validate(
				    orb_conf::local_nodeid(),
				    Rgm_state->node_incarnations.members[
				    orb_conf::local_nodeid()],
				    lni, ln->getIncarnation(),
				    val_args, rs_validate_v, e);
				rgm_lock_state();

				CORBA::release(prgm_serv);
				if (e.exception() != NULL) {
					idlretval = except_to_errcode(e,
					    ln->getNodeid(), lni);
					e.clear();
					if (idlretval == RGMIDL_NODE_DEATH ||
					    idlretval == RGMIDL_RGMD_DEATH) {
						// This node is not in the
						// cluster, or soon won't be.
						validate_err_code =
						    SCHA_ERR_RS_VALIDATE;
						continue;
					}
					// Some unknown/unexpected exception
					// occurred
					ucmm_print("idl_scrgadm_rs_add()",
					    NOGET("VALIDATE failed with "
					    "unknown exception"));
					res.err_code = SCHA_ERR_INTERNAL;
					rgm_format_errmsg(&res, REM_INTERNAL);
					goto rs_add_end_clr_sw; //lint !e801
				}
			}

			//
			// Save the error message from the validate call to
			// return to the user whether or not validate passed
			// (fix for EOU 4219599: Capture stdout/stderr of
			// validate method).
			//
			// rgm_format_errmsg will automatically allocate the
			// necessary memory, concatenating the new message
			// onto previous messages if validate_res.err_msg is
			// non-NULL.
			//
			if ((const char *)rs_validate_v->idlstr_err_msg) {
				//
				// If there's a newline char on the end of the
				// old message, get rid of it, because
				// rgm_format_errmsg adds a new one.
				//
				if (validate_res.err_msg) {
					int last_char = (int)strlen(
					    validate_res.err_msg) - 1;
					if (last_char >= 0 &&
					    validate_res.err_msg[last_char] ==
					    '\n') {
						validate_res.
						    err_msg[last_char] = '\0';
					}
				}
				//
				// We need to make a temporary copy to avoid the
				// compiler warning:
				// A class with a constructor will not reliably
				// work with "..."
				//
				char *temp_str = strdup_nocheck(
				    rs_validate_v->idlstr_err_msg);
				if (temp_str == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto rs_add_end_clr_sw; //lint !e801
				}
				rgm_sprintf(&validate_res, "%s", temp_str);
				free(temp_str);
			}

			//
			// If the VALIDATE call fails (returns non-zero or
			// times out) then exit this function early with an
			// error.
			//
			if (rs_validate_v->ret_code != SCHA_ERR_NOERR) {
				const char *nodename = ln->getFullName();
				ucmm_print("idl_scrgadm_rs_add()",
				    NOGET("VALIDATE failed\n"));
				res.err_code =
				    (scha_err_t)rs_validate_v->ret_code;

				rgm_format_errmsg(&res, REM_VALIDATE_FAILED,
				    rs_name, rg_name, nodename);

				goto rs_add_end_clr_sw; //lint !e801
			}
			// Setting validate_done flag if we have done validation
			// successfully at least once.
			validate_done = B_TRUE;
		}

		// The global state machine mutex is still held.

		//
		// Check if this operation has been "busted".  This means that
		// a current master of the RG has died or is being evacuated.
		// Since we haven't actually updated the CCR yet, we will
		// return early from the attempted update with an error
		// ("cluster is reconfiguring"), without the update having
		// taken effect.
		//
		if ((ret = map_busted_err(rgp->rgl_switching)) !=
		    SCHA_ERR_NOERR) {
			// The RG has been busted, do early return
			ucmm_print("idl_scrgadm_rs_add", NOGET(
			    "RG <%s> is busted with error <%s>"),
			    rg_name, rgm_error_msg(ret));
			res.err_code = ret;
			goto rs_add_end_clr_sw; //lint !e801
		}

		//
		// If validate_done flag is found un-set here, it means none
		// of the validation candidates are in the cluster membership.
		// Return with SCHA_ERR_VALIDATE error.
		//
		if (!validate_done) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RG_TAG, rg_name);
			//
			// SCMSGS
			// @explanation
			// In order to create a resource whose type has a
			// registered VALIDATE method, the rgmd must be able
			// to run VALIDATE on at least one node. However, all
			// of the candidate nodes are down. "Candidate nodes"
			// are either members of the resource
			// group's Nodelist or members of the resource type's
			// Installed_nodes list, depending on the setting of the
			// resource's Init_nodes property.
			// @user_action
			// Boot one of the resource group's potential masters
			// and retry the resource creation operation.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Creation of resource <%s> failed because none of "
			    "the nodes on which VALIDATE would have run are "
			    "currently up", rs_name);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_VALIDATE;
			rgm_format_errmsg(&res, REM_VALIDATE_NO_NODES, rs_name);
			goto rs_add_end_clr_sw; //lint !e801
		}
	} // if (!ns->isEmpty())


	// Check if any inter-cluster dependencies are specified.
	if (dep_updated) {
		r_deps = new_rs_no_defaults->r_dependencies;

#if (SOL_VERSION >= __s10)
		// Check if inter cluster strong dependency is set
		if ((allow_inter_cluster()) && dep_strong_updated) {
			res = configure_icrd(r_deps.dp_strong,
			    new_rs_no_defaults, DEPS_STRONG);
			if (res.err_code !=  SCHA_ERR_NOERR) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RG_TAG, rg_name);
				//
				// SCMSGS
				// @explanation
				// Resource creation failed due to specification
				// of invalid inter cluster dependencies.
				// @user_action
				// Check the remote cluster name or remote
				// resource name and retry the same command.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "Creation of resource <%s> failed because "
				    "the inter-cluster strong "
				    "dependencies could not be set", rs_name);
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_VALIDATE;
				rgm_format_errmsg(&res, REM_ICRD_SET_FAIL,
				    "strong", rs_name);
				goto rs_add_end_clr_sw;
			}
		}

		// Check if inter cluster weak dependency is set.
		if ((allow_inter_cluster()) && dep_weak_updated) {
			res = configure_icrd(r_deps.dp_weak, new_rs_no_defaults,
			    DEPS_WEAK);
			if (res.err_code !=  SCHA_ERR_NOERR) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RG_TAG, rg_name);
				//
				// SCMSGS
				// @explanation
				// Resource creation failed due to specification
				// of invalid inter cluster dependencies.
				// @user_action
				// Check the remote cluster name or remote
				// resource name and retry the same command.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "Creation of resource <%s> failed because "
				    "the inter-cluster weak "
				    "dependencies could not be set", rs_name);
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_VALIDATE;
				rgm_format_errmsg(&res, REM_ICRD_SET_FAIL,
				    "weak", rs_name);
				goto rs_add_end_clr_sw;
			}
		}


		// Check if inter cluster restart dependency is set.
		if ((allow_inter_cluster()) && dep_restart_updated) {
			res = configure_icrd(r_deps.dp_restart,
			    new_rs_no_defaults,
			    DEPS_RESTART);
			if (res.err_code !=  SCHA_ERR_NOERR) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RG_TAG, rg_name);
				//
				// SCMSGS
				// @explanation
				// Resource creation failed due to specification
				// of invalid inter cluster dependencies.
				// @user_action
				// Check the remote cluster name or remote
				// resource name and retry the same command.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "Creation of resource <%s> failed because "
				    "the inter-cluster restart "
				    "dependencies could not be set", rs_name);
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_VALIDATE;
				rgm_format_errmsg(&res, REM_ICRD_SET_FAIL,
				    "restart", rs_name);
				goto rs_add_end_clr_sw;
			}
		}

		// Check if inter cluster offline restart dependency is set.
		if ((allow_inter_cluster()) && dep_off_restart_updated) {
			res = configure_icrd(r_deps.dp_offline_restart,
			    new_rs_no_defaults, DEPS_OFFLINE_RESTART);
			if (res.err_code !=  SCHA_ERR_NOERR) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RG_TAG, rg_name);
				//
				// SCMSGS
				// @explanation
				// Resource creation failed due to specification
				// of invalid inter cluster dependencies.
				// @user_action
				// Check the remote cluster name or remote
				// resource name and retry the same command.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "Creation of resource <%s> failed because "
				    "the inter-cluster offline-restart "
				    "dependencies could not be set", rs_name);
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_VALIDATE;
				rgm_format_errmsg(&res, REM_ICRD_SET_FAIL,
				    "offline-restart", rs_name);
				goto rs_add_end_clr_sw;
			}
		}
#endif
	}

	//
	// Latch the intention to create the new resource r
	// Besides the "opcode" ("create-R"), the intention also contains
	// a node set which identifies the nodes for execution of the INIT
	// method.
	//

	nset = compute_meth_nodes(rt, rgp->rgl_ccr, METH_INIT, is_scalable);

	ucmm_print("RGM_idl_scrgadm_rs_add()", NOGET("RS <%s> Latching\n"),
	    rs_name);
	(void) latch_intention(rgm::RGM_ENT_RS, rgm::RGM_OP_CREATE,
	    rs_name, nset, 0);

	ucmm_print("RGM_idls_crgadm_rs_add()", NOGET("RS <%s> Calling CCR\n"),
	    rs_name);

	//
	// Update the CCR with the new resource r.
	//
	// We save the version of the resource without defaults to the CCR,
	// in order to support the cascading defaults for resource type
	// upgrades.
	//
	if ((res = rgmcnfg_add_resource(new_rs_no_defaults)).err_code
	    != SCHA_ERR_NOERR) {
		ucmm_print("RGM_idl_scrgadm_rs_add()",
		    NOGET("RS <%s> CCR Failed\n"), rs_name);
		(void) unlatch_intention();
		goto rs_add_end_clr_sw; //lint !e801
	}

	delete (ns);
	ns = NULL;


	ucmm_print("RGM_idl_scrgadm_rs_add()",
	    NOGET("RS <%s> Processing\n"), rs_name);

	// Call process_intention().  This notifies all nodes that the
	//  new resource r is created in rg; slaves update their state from
	//  the CCR.
	//  Slave(s) that are supposed to run INIT on r will start their state
	//  machine in a new thread and return immediately.
	(void) process_intention();
	(void) unlatch_intention();

	//
	// post an RS config change event, ignoring the return code.
	// put after process intention, so that clients listening to
	// CCR change events don't get stale values.
	//
	(void) sc_publish_event(ESC_CLUSTER_R_CONFIG_CHANGE,
	    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
	    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
	    CL_R_NAME, SE_DATA_TYPE_STRING, rs_name,
	    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
	    CL_EVENT_CONFIG_ADDED, NULL);

	//
	// We have to post this event after we run process_intention, so
	// that the in-memory structure will have been created.
	//
	(void) post_event_rs_state(rs_name);
	//
	// If nset is empty, no INIT methods are to be run;
	// we're done.
	// Note, in this case the slaves will *not* wake the president so
	// we must not do the cond_wait.
	//
	if (nset->isEmpty()) {
		goto rs_add_end_clr_sw; //lint !e801
	}

	//
	// Flush the state change queue before calling cond_wait()
	//
	// It's ok to unlock and relock the state here (we're about to do the
	// same thing in the cond_wait).  It will just take a little longer
	// (because other threads might get to run first).
	//
	rgm_unlock_state();
	flush_state_change_queue();
	rgm_lock_state();

	// Wait for R to move from PENDING_INIT through INITING to OFFLINE.
	// The R will be OFFLINE on those nodes not currently in the
	// cluster, so just check all nodes.  It would be just as time-
	// consuming to check for and skip over those nodes not in the
	// cluster.
	rp = rname_to_r(rgp, rs_name);

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode =
		    lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);

		rgm::lni_t n = lnNode->getLni();

		// If the R's state is still pending init on any node, wait.
		while (rp->rl_xstate[n].rx_state != rgm::R_OFFLINE) {
			ucmm_print("RGM_idl_scrgadm_rs_add()",
			    NOGET("add_resource wait slavewait\n"));
			cond_slavewait.wait(&Rgm_state->rgm_mutex);
			ucmm_print("RGM_idl_scrgadm_rs_add()",
			    NOGET("after add_resource wait slavewait\n"));
		}
	}

	// XXX 4215776 Implement FYI states in rgmd
	// XXX If INIT didn't complete successfully on all nodes,
	// return a different exit code indicating "qualified success".

rs_add_end_clr_sw:

	if (rgp) {
		//
		// If the operation was "busted", we must run rebalance()
		// because process_rg() did not run it in the RGM
		// reconfiguration step.
		//
		// However, if we were busted by a node evacuation, we do
		// not reset rgl_switching and we do not run rebalance--
		// the evacuating thread will run it while excluding
		// evacuating node(s).
		//
		if (rgp->rgl_switching == SW_UPDATE_BUSTED) {
			ucmm_print("idl_scrgadm_rs_add",
			    NOGET("RG <%s> is busted, running rebalance()"),
			    rg_name);
			rebalance(rgp, emptyNodeset, B_FALSE, B_TRUE);
		}

		if (rgp->rgl_switching != SW_EVACUATING) {
			rgp->rgl_switching = SW_NONE;
		}
	}

rs_add_end:

	//
	// issue a broadcast to wake up failback thread (if any) waiting for
	// this operation.
	//
	(void) cond_slavewait.broadcast();

	// Even if there is no other error encountered in this subroutine,
	// the special case SCHA_ERR_RS_VALIDATE might have occurred
	// due to a node not being present in the cluster.
	if (res.err_code == SCHA_ERR_NOERR &&
	    validate_err_code != SCHA_ERR_NOERR) {
		res.err_code = validate_err_code;
		rgm_format_errmsg(&res, RWM_VALIDATE_NODES);
	}

	free(rg_name);
	free(rs_name);
	free(rt_name);
	free(locale);
	if (new_rs)
		rgm_free_resource(new_rs);
	if (new_rs_no_defaults)
		rgm_free_resource(new_rs_no_defaults);

	retval->ret_code = res.err_code;

	delete (ns);
	delete (nset);
	delete (switch_ns);

	/*
	 * Our final error message consists of the output from all VALIDATE
	 * methods, followed by any other error/warning/notice message.
	 */
	if (res.err_msg)
		rgm_sprintf(&validate_res, "%s", res.err_msg);

	if (warning_res.err_msg)
		rgm_sprintf(&validate_res, "%s", warning_res.err_msg);

	if ((temp_res.err_msg) && (res.err_code == SCHA_ERR_NOERR))
		rgm_sprintf(&validate_res, "%s", temp_res.err_msg);
	/* new_str checks for NULL */
	retval->idlstr_err_msg = new_str(validate_res.err_msg);

	/* free the old strings; free checks for NULL */
	free(res.err_msg);
	free(warning_res.err_msg);
	free(validate_res.err_msg);
	free(temp_res.err_msg);

	rs_res = retval;
	rgm_unlock_state();
}


//
// Update various properties of a resource.
//
//
// We ignore the idlstr_stamp member of the rs_args, because it is
// not relevent.  There is no need to verify the gen number when we're
// ready to write the properties to the CCR, because there is no way that the
// R table can have been updated from under us. We set the rgl_switching flag
// on the rg to SW_UPDATING, which will prevent any other thread from doing an
// update for this RG (and thus for this resource).
//
// We cannot remove the idlstr_stamp member from the structure because of
// rolling upgrade requirements, so we just ignore it.
//
void
rgm_comm_impl::idl_scrgadm_rs_update_prop(
	const rgm::idl_rs_update_prop_args &rs_args,
	rgm::idl_regis_result_t_out rs_res, Environment &)
{
	char 			*rs_name = NULL;
	char			*rg_name = NULL;
	char			*locale = NULL;
	rgm_resource_t 		*rs_conf = NULL;
	rgm_resource_t 		*rs_conf_no_defaults = NULL;
	rgm_resource_t 		*rs_conf_helper = NULL;
	rglist_p_t 		rgp = NULL;
	rlist_p_t 		rp = NULL;
	rgm_rt_t 		*rt = NULL;
	rgm::idl_validate_args 	val_args;
	rgm::rgm_comm_ptr 	prgm_serv;
	LogicalNodeset 		*ns = NULL;
	rgm::lni_t 		lni = 0;
	boolean_t 		need_cond_wait = B_FALSE;
	Environment e;
	LogicalNodeset 		pending_boot_ns;
	LogicalNodeset 		stop_failed_ns;
	LogicalNodeset 		r_restart_ns;
	LogicalNodeset		e_ns;
	LogicalNodeset	*switch_ns;
	scha_err_t		validate_err_code;
	scha_err_t		ret;
	boolean_t		validate_done;
	sc_syslog_msg_handle_t 	handle;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	rgm::idl_regis_result_t_var rs_validate_v;
	scha_errmsg_t 		res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t 		validate_res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t 		warning_res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t 		temp_res = {SCHA_ERR_NOERR, NULL};
	boolean_t		is_scalable;
	boolean_t		is_scalable_prop_defined;
	idlretval_t		idlretval;
	char			*pval;
	boolean_t 		upgrade_allowed = B_FALSE;
	boolean_t 		dep_updated = B_FALSE;

	// Check for inter-cluster dependencies update.
	boolean_t		dep_strong_updated = B_FALSE;
	boolean_t		dep_weak_updated = B_FALSE;
	boolean_t		dep_restart_updated = B_FALSE;
	boolean_t		dep_off_restart_updated = B_FALSE;

	rgm_rt_t 		*upgrade_rt = NULL;
	name_t 			upgrade_rtname = NULL;
	rgm_param_t 		**paramtable;
	LogicalNodeset 		emptyNodeset;
	LogicalNodeset 		*ns_nodeset;
	uint_t			ext_prop_len = 0;
	boolean_t		globalflag;
	boolean_t		nodelist_contains;
	rg_switch_t		save_rgl_switching;
	rgm_dependencies_t r_deps_new;
	rgm_dependencies_t r_deps_orig;
	boolean_t inter_cluster_set_dep = B_FALSE;
	boolean_t inter_cluster_unset_dep = B_FALSE;
	rgm::intention_flag_t flags = 0;

	rgm_lock_state();

	// Check if the inter-cluster dependencies have been set or unset.
	if (allow_inter_cluster()) {
		if (rs_args.flags & rgm::SET_INTER_CLUSTER_DEP)
			inter_cluster_set_dep = B_TRUE;

		if (rs_args.flags & rgm::UNSET_INTER_CLUSTER_DEP)
			inter_cluster_unset_dep = B_TRUE;
	}

	validate_err_code = SCHA_ERR_NOERR;
	validate_done = B_FALSE;

	rs_name = strdup_nocheck(rs_args.idlstr_rs_name);
	locale = strdup_nocheck(rs_args.locale);

	if (rs_name == NULL || locale == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto rs_update_end; //lint !e801
	}

	// If no property is updated, return early with error.
	if ((int)rs_args.prop_val.length() == 0 &&
	    (int)rs_args.ext_prop_val.length() == 0) {
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("No property change for resource <%s>\n"), rs_name);
		res.err_code = SCHA_ERR_INVAL;
		goto rs_update_end; //lint !e801
	}

	if ((rp = rname_to_r(NULL, rs_name)) == NULL) {
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("Resource <%s> does not exist!!\n"), rs_name);
		res.err_code = SCHA_ERR_RSRC;
		rgm_format_errmsg(&res, REM_INVALID_RS, rs_name);
		goto rs_update_end; //lint !e801
	}

	ucmm_print("idl_scrgadm_rs_update_prop()",
	    NOGET("RS <%s>\n"), rs_name);

	rgp = rp->rl_rg;
	rg_name = rgp->rgl_ccr->rg_name;

	ngzone_nodelist_check(rgp, rs_args.nodeid, rs_args.idlstr_zonename,
	    globalflag, nodelist_contains);
	if (!globalflag) {
		if (!nodelist_contains) {
			res.err_code = SCHA_ERR_ACCESS;
			rgm_format_errmsg(&res, REM_R_NODELIST_NGZONE,
			    rs_name, (const char *)rs_args.idlstr_zonename,
			    rgp->rgl_ccr->rg_name);
			goto rs_update_end; //lint !e801
		}
	}

	// The RG must be in an "endish" state.
	if (!in_endish_rg_state(rgp, &pending_boot_ns, &stop_failed_ns,
	    NULL, &r_restart_ns) || !pending_boot_ns.isEmpty() ||
	    !r_restart_ns.isEmpty()) {
		// The RG is busy or running BOOT methods on some node
		res.err_code = SCHA_ERR_RGRECONF;
		rgm_format_errmsg(&res, REM_RG_BUSY, rg_name);
		ucmm_print("RGM",
		    NOGET("idl_scrgadm_rs_update_prop(): RG <%s> is not in an"
		    " endish state\n"), rg_name);
		goto rs_update_end; //lint !e801
	}
	// ERROR_STOP_FAILED is an endish state but still precludes RS update
	if (!stop_failed_ns.isEmpty()) {
		// The RG is ERROR_STOP_FAILED on some node
		res.err_code = SCHA_ERR_STOPFAILED;
		rgm_format_errmsg(&res, REM_RS_IN_RG_TO_STOPFAILED, rs_name,
		    rg_name);
		ucmm_print("RGM",
		    NOGET("idl_scrgadm_rs_update_prop(): RG <%s> is "
		    "ERROR_STOP_FAILED\n"), rg_name);
		goto rs_update_end; //lint !e801
	}

	//
	// "system" property checks
	//
	//	Can't update any property of an R which is contained in
	//	a "system" RG.
	//
	if (rgp->rgl_ccr->rg_system) {
		res.err_code = SCHA_ERR_ACCESS;
		rgm_format_errmsg(&res, REM_SYSTEM_RES, rs_name, rg_name);
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("R <%s> is contained RG <%s> whose RG_SYSTEM "
		    "property is TRUE\n"), rs_name, rg_name);
		goto rs_update_end; //lint !e801
	}

	switch_ns = get_logical_nodeset_from_nodelist(
	    rgp->rgl_ccr->rg_nodelist);

	for (rgm::lni_t nodeid = switch_ns->nextLniSet(1); nodeid != 0;
	    nodeid = switch_ns->nextLniSet(nodeid + 1)) {

		if ((((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)
		    ->r_switch[nodeid] != SCHA_SWITCH_DISABLED)) {
			ucmm_print("idl_scrgadm_rg_update_prop()",
			    NOGET("Dynamic Update in action"));
			need_cond_wait = B_TRUE;
			break;
		}
	}

	rt = rtname_to_rt(rp->rl_ccrdata->r_type);
	if (!rt) {
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("Invalid RT = <%s>\n"), rp->rl_ccrdata->r_type);
		res.err_code = SCHA_ERR_RT;
		goto rs_update_end; //lint !e801
	}

	//
	// first, check to see if the type_version property is among the list
	// of properties being updated. This is done by the is_upgrade(...)
	// routine, which sets the boolean variable upgrade_allowed.
	//
	if (is_upgrade(rs_args.prop_val)) {
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("It's an upgrade."));
		// if it is an rt upgrade, the first thing we do is generate
		// the name of the new RT (to which this resource is being
		// upgraded to).
		if ((res = create_upgrade_rtname(rs_args.prop_val,
		    rp->rl_ccrdata->r_type, &upgrade_rtname)).err_code !=
		    SCHA_ERR_NOERR)
			goto rs_update_end; //lint !e801
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("Upgrade RT Name = <%s>\n"), upgrade_rtname);

		// retrieve the new RT from the CCR and return with an error
		// if it doesn't exist.
		upgrade_rt = rtname_to_rt(upgrade_rtname);
		if (!upgrade_rt) {
			ucmm_print("idl_scrgadm_rs_update_prop()",
			    NOGET("Invalid RT = <%s>\n"), upgrade_rtname);
			res.err_code = SCHA_ERR_RT;
			rgm_format_errmsg(&res, REM_INVALID_RT,
			    upgrade_rtname);
			goto rs_update_end; //lint !e801
		}

		// next, check to see if the upgrade is allowed by the
		// upgrade_from or downgrade_to directives in the destination
		// RT or the source RT, respectively. This is done by
		// the is_upgrade_allowed routine which returns a descriptive
		// error message in a scha_errmsg_t structure.
		if ((res = is_upgrade_allowed(upgrade_rt, rt, rp,
		    &upgrade_allowed)).err_code != SCHA_ERR_NOERR) {
			goto rs_update_end; //lint !e801
		}

		if (upgrade_allowed) {
			ucmm_print("idl_scrgadm_rs_update_prop()",
			    NOGET("Upgrade is allowed by the upgrade-from "
			    "directives."));
		} else {
			ucmm_print("idl_scrgadm_rs_update_prop()",
			    NOGET("Upgrade not allowed due to upgrade-from "
			    "directives."));
		}
	}

	//
	// If the system property list does not contain any of the three
	// resource dependencies or the network_resources_used, then the
	// dependency related checks in rs_sanity_checks need not be
	// performed.
	//
	dep_updated = is_dependency_updated(rs_args.prop_val,
	    &dep_strong_updated, &dep_weak_updated,
	    &dep_restart_updated, &dep_off_restart_updated);

	//
	// Note that we keep two parallel versions of the resource
	// structure.  One has the RT defaults as part of it, applied
	// from the resource type.  The second has only what is stored in
	// the CCR (no RT defaults).
	//
	// Sanity tests and validate should be run on the version with
	// defaults.  The version without defaults should be saved to the
	// CCR.
	//

	//
	// First, we get the version with no defaults.
	//
	if ((res = rgmcnfg_get_resource_no_defaults(rs_name, rg_name,
	    &rs_conf_no_defaults, ZONE)).err_code != SCHA_ERR_NOERR)
		goto rs_update_end;

	update_pn_default_value(rs_conf_no_defaults->r_ext_properties,
	    rgp->rgl_ccr->rg_nodelist);

	update_onoff_monitored_switch(rs_conf_no_defaults,
	    rgp->rgl_ccr->rg_nodelist);

	//
	// Now, we copy that version.
	//

	if ((res = rgmcnfg_copy_resource(rs_conf_no_defaults, &rs_conf)).
	    err_code != SCHA_ERR_NOERR)
		goto rs_update_end; //lint !e801

	//
	// Now, we copy that version into a helper struct.
	// This will be used in case there are inter-cluster r deps.
	//
	if ((res = rgmcnfg_copy_resource(rs_conf, &rs_conf_helper)).err_code !=
		SCHA_ERR_NOERR)
		goto rs_update_end;

	update_onoff_monitored_switch(rs_conf, rgp->rgl_ccr->rg_nodelist);


	//
	// Now we apply the RT defaults to the new copy, so that we have
	// one copy with defaults and one without.
	//
	if ((res = rgmcnfg_apply_rt_defaults(rs_conf,
	    (const char*)ZONE)).err_code !=  SCHA_ERR_NOERR)
		goto rs_update_end; //lint !e801

	update_pn_default_value(rs_conf->r_ext_properties,
	    rgp->rgl_ccr->rg_nodelist);

	if (upgrade_allowed) {
		delete_properties(rs_conf, upgrade_rt->rt_paramtable);

		delete_properties(rs_conf_no_defaults,
		    upgrade_rt->rt_paramtable);

		// if it is an RT upgrade, we use the paramtable from the
		// new RT as that would have new defaults, min, max, arraymin,
		// arraymax etc. for the various properties.
		paramtable = upgrade_rt->rt_paramtable;
	} else
		// if it is not an RT upgrade, we use the paramtable from the
		// old (or current) RT.
		paramtable = rt->rt_paramtable;



	//
	// Now we do all checks on the one with defaults.
	//

	//
	// check if system-defined, optional system-defined or extension
	// properties exist in R and can be updated
	//

	if ((res = check_rs_updatable_props(rs_args.prop_val,
	    rs_args.ext_prop_val, paramtable,
	    ((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)->r_switch,
	    B_TRUE, rgp->rgl_ccr->rg_nodelist, &e_ns)).err_code
	    != SCHA_ERR_NOERR)
		goto rs_update_end; //lint !e801



	//
	// Overwrite the old values with user-specified properties
	// If this is an RT upgrade, as determined by the upgrade_allowed
	// boolean property, update_rs_props routine also modifies the
	// value of the r_type property.
	res = update_rs_props(rs_args.prop_val, rs_args.ext_prop_val,
	    &rs_conf, upgrade_allowed, inter_cluster_set_dep,
	    inter_cluster_unset_dep, e_ns);
	if (res.err_code == SCHA_ERR_CHECKS) {
		goto rs_update_end; //lint !e801
	} else if (res.err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("invalid property or property value "
		    "(from update_rs_props)"));
		goto rs_update_end; //lint !e801
	}

	//
	// Do the same on the version with no defaults.
	// Note that we need to use a different function (apply_user_props
	// instead of update_rs_props), because of different expectations
	// of the two functions.  update_rs_props expects an
	// rgm_property_list_t already in the resource list for every
	// possible property, while apply_user_props does not.
	//
	// The return code of SCHA_ERR_CHECKS indicates
	// that per-node extension property is updated on a node
	// which is enabled and whose TUNABLE flag is set to
	// WHEN_DISABLED.
	//
	res = apply_user_props(rs_args.prop_val, rs_args.ext_prop_val,
	    paramtable, &rs_conf_no_defaults, upgrade_allowed,
	    inter_cluster_set_dep, inter_cluster_unset_dep, e_ns);
	if (res.err_code == SCHA_ERR_CHECKS) {
		goto rs_update_end; //lint !e801
	} else 	if (res.err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("invalid property or property value "
		    "(from apply_user_props)"));
		goto rs_update_end; //lint !e801
	}

	//
	// Store warning message if there was any.We add this message
	// if the creation of the resource is succesful.
	//
	(void) transfer_message(&temp_res, &res);
	if (temp_res.err_code == SCHA_ERR_NOMEM)
		// Low memory, bailout
		goto rs_update_end; //lint !e801

	// The project name can be updated at anytime, but need to print a
	// warning message that it won't take any effect until next time the
	// resource is restarted.
	if ((res = get_prop_value(SCHA_RESOURCE_PROJECT_NAME, rs_args.prop_val,
	    &pval, SCHA_PTYPE_STRING)).err_code == SCHA_ERR_NOERR) {
		rgm_sprintf(&validate_res, "%s", RWM_PROJECT_CHANGED);
	}


	//
	// Skip the validation checks if this is ICRD SET or UNSET operation
	// on the depended on resource.
	//
	if (inter_cluster_set_dep || inter_cluster_unset_dep) {
		rgp->rgl_switching = SW_UPDATING;
		goto rs_write_icd;
	}

	//
	// Do generic sanity check on the properties with the new values
	// Notice that we are using the generic "paramtable" variable which
	// was set further up taking into account whether this is an upgrade
	// or not.
	//
	// Property updates are permitted regardless if the resource has
	// Global_zone set to TRUE or FALSE.  Therefore, we pass B_TRUE for
	// the 'globalflag' parameter to suppress Global_zone checking in
	// rs_sanity_checks.
	//
	if ((res = rs_sanity_checks(rs_conf, paramtable,
	    dep_updated, B_TRUE, NULL)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("Invalid property or property value "
		    "(from rs_sanity_checks)"));
		goto rs_update_end; //lint !e801
	}

	//
	// Check if there are any RG-node cycles using both rg_dependencies
	// and inter-rg resource dependencies as edges.
	//
	// We don't put this call in rs_sanity_checks because we need the
	// rgp ptr.
	//
	if ((res = detect_combined_dependencies_cycles(rgp, rgp->rgl_ccr,
	    rs_conf)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("Invalid dependency setting "
		    "(from detect_combined_dependencies_cycles)"));
		goto rs_update_end; //lint !e801
	}

	is_scalable = is_r_scalable(rp->rl_ccrdata);
	is_scalable_prop_defined = is_r_scalable_prop_defined(
	    rp->rl_ccrdata);

	// Check scalable related properties.
	if ((res = check_scalable_props(rs_args.prop_val,
	    is_scalable_prop_defined, rp->rl_ccrdata->r_properties,
	    &warning_res, rs_name)).err_code != SCHA_ERR_NOERR)
		goto rs_update_end; //lint !e801

	//
	// Set a flag on rg indicating that it is being updated
	// If there are any failures after this point, we need to
	// go to rs_update_end_clr_sw to reset the switching flag
	// back to none.
	//
	rgp->rgl_switching = SW_UPDATING;


	// Find the set of nodes on which VALIDATE method are supposed to run
	ns = compute_meth_nodes(rt, rgp->rgl_ccr, METH_VALIDATE, is_scalable);

	//
	// If there is no validate method defined for this R, ns will be empty,
	// and there is no need to run validate method.
	//
	// Also skip running validate method if the SUPPRESS_VALIDATION option
	// flag is set.
	// This flag is currently used for the purpose of removing
	// resource dependencies upon a resource which is being force-deleted.
	// Since Validate methods are skipped, forced-deletion might leave
	// the remaining resources in an invalid configuration.  This will
	// be documented.
	//
	//
	if (!ns->isEmpty() &&
	    !(rs_args.flags & rgm::SUPPRESS_VALIDATION)) {
		//
		// VALIDATE has to be called successfully on all nodes in ns
		// that are currently in the cluster, or else we will return
		// early with an error.
		// We will execute VALIDATE on each node in ns and block waiting
		// for completion.  If any of these calls fails, return early
		// with an error.  If any nodes in ns are not in the cluster
		// membership, we will succeed (provided at least one validate
		// call succeeds) but will return a warning message.
		//

		//
		// NOTE:
		// We allocated val_args on the stack.  Since this structure is
		// an IDL IN parameter, we, the caller, are responsible for the
		// storage.  When we leave the scope of this routine, its
		// storage will be automatically freed.  This includes all the
		// storage allocated in nvseq_to_strseq().
		//

		val_args.idlstr_rs_name = new_str(rs_name);
		//
		// If this is an RT upgrade we need to run the VALIDATE method
		// of the new RT. To this end we set the name of the new RT in
		// the val_args, which eventually would result in the VALIDATE
		// method of the new RT being run.
		//
		if (upgrade_allowed)
			val_args.idlstr_rt_name = new_str(upgrade_rtname);
		else
			val_args.idlstr_rt_name =
			    new_str(rp->rl_ccrdata->r_type);
		val_args.idlstr_rg_name = new_str(rg_name);
		val_args.locale = new_str(locale);
		val_args.opcode = rgm::RGM_OP_UPDATE;
		val_args.is_scalable = (bool)is_scalable;

		//
		// for resource update, only pass the updated property
		// list of r to idl_scrgadm_rs_validate(),
		// so they in turn can be passed to VALIDATE
		//

		//
		// each element of val_args.rs_props and val_args.rs_props
		// is in a format of "<key>=<val,...>"
		//

		//
		// Added rs_conf as a parameter to this function so as to send
		// the updated NRU for validation when the dependency changes.
		//
		(void) nvseq_to_strseq(rs_args.prop_val, val_args.rs_props,
		    rs_conf);
		(void) nvseq_to_strseq_ext(rs_args.ext_prop_val,
		    val_args.ext_props, rp->rl_ccrdata);

		ns_nodeset = get_logical_nodeset_from_nodelist(
		    rgp->rgl_ccr->rg_nodelist);
		(void) update_all_nodes_val(rs_args.ext_prop_val,
		    rs_conf->r_ext_properties,
		    rp->rl_ccrdata->r_ext_properties,
		    val_args.all_nodes_ext_props, ns_nodeset);

		val_args.rg_props = NULL;
		ext_prop_len = val_args.ext_props.length();

		lni = 0;
		while ((lni = ns->nextLniSet(lni + 1)) != 0) {
			bool is_farmnode;

			//
			// If the node isn't in the current membership, skip
			// it but set error code SCHA_ERR_RS_VALIDATE in
			// validate_err_code.
			// This error code will be returned
			// if there is no other error in the function.
			//
			LogicalNode *ln = lnManager->findObject(lni);
			CL_PANIC(ln != NULL);
#if (SOL_VERSION >= __s10)
			if (strcmp(ZONE, GLOBAL_ZONENAME) == 0) {
#endif
				if (!ln->isInCluster()) {
					validate_err_code =
					    SCHA_ERR_RS_VALIDATE;
					continue;
				}
#if (SOL_VERSION >= __s10)
			} else {
				if (!ln->isInZoneCluster()) {
					validate_err_code =
					    SCHA_ERR_RS_VALIDATE;
					continue;
				}
			}
#endif
			//
			// Before adding the local-node -x options, reset
			// val_args.ext_props back to its starting length.
			// That way, for each node, we only add the local-node
			// -x options corresponding to that node.
			//
			val_args.ext_props.length(ext_prop_len);

			//
			// The per-node extension property being updated on a
			// particular node would be passed to the validate
			// callback as the conventional -x option.
			// Note that this is in addition to the same value
			// being passed with a -X option.
			//
			update_local_node_value(val_args.ext_props,
			    rs_conf->r_ext_properties, rs_args.ext_prop_val,
			    rp->rl_ccrdata, lni, ext_prop_len);

			// Release the global state machine
			// mutex while running validate
			rgm_unlock_state();
			if ((rgmx_hook_call(rgmx_hook_scrgadm_rs_validate,
				ln->getNodeid(),
				lni, ln->getIncarnation(),
				&val_args, &rs_validate_v, &idlretval,
				&is_farmnode) == RGMX_HOOK_ENABLED) &&
				(is_farmnode)) {
				rgm_lock_state();
				if (idlretval != RGMIDL_OK) {
					validate_err_code ==
					    SCHA_ERR_RS_VALIDATE;
					continue;
				}
			} else {
				prgm_serv = rgm_comm_getref(ln->getNodeid());
				if (CORBA::is_nil(prgm_serv)) {
					validate_err_code =
					    SCHA_ERR_RS_VALIDATE;
					rgm_lock_state();
					// This node is not in the cluster
					continue;
				}

				prgm_serv->idl_scrgadm_rs_validate(
				    orb_conf::local_nodeid(),
				    Rgm_state->node_incarnations.members[
				    orb_conf::local_nodeid()], lni,
				    ln->getIncarnation(),
				    val_args, rs_validate_v, e);
				rgm_lock_state();

				CORBA::release(prgm_serv);
				if (e.exception() != NULL) {
					idlretval = except_to_errcode(e,
					    ln->getNodeid(), lni);
					e.clear();
					if (idlretval == RGMIDL_NODE_DEATH ||
					    idlretval == RGMIDL_RGMD_DEATH) {
						// This node is not in the
						// cluster, or soon won't be.
						validate_err_code =
						    SCHA_ERR_RS_VALIDATE;
						continue;
					}
					// Some unknown/unexpected exception
					// occurred
					ucmm_print(
					    "idl_scrgadm_rs_update_prop()",
					    NOGET("VALIDATE failed with unknown"
					    " exception"));
					res.err_code = SCHA_ERR_INTERNAL;
					rgm_format_errmsg(&res, REM_INTERNAL);
					goto rs_update_end_clr_sw; //lint !e801
				}
			}

			//
			// Save the error message from the validate call to
			// return to the user whether or not validate passed
			// (fix for EOU
			// 4219599: Capture stdout/stderr of validate method).
			//
			// rgm_format_errmsg will automatically allocate the
			// necessary memory, concatenating the new message onto
			// previous messages if validate_res.err_msg is
			// non-NULL.
			//
			if ((const char *)rs_validate_v->idlstr_err_msg) {
				//
				// If there's a newline char on the end of the
				// old message, get rid of it, because
				// rgm_format_errmsg adds a new one.
				//
				if (validate_res.err_msg) {
					int last_char = (int)strlen(
					    validate_res.err_msg) - 1;
					if (last_char >= 0 &&
					    validate_res.err_msg[last_char] ==
					    '\n') {
						validate_res.
						    err_msg[last_char] = '\0';
					}
				}
				//
				// We need to make a temporary copy to avoid the
				// compiler warning:
				// A class with a constructor will not reliably
				// work with "..."
				//
				char *temp_str = strdup_nocheck(
				    rs_validate_v->idlstr_err_msg);
				if (temp_str == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto rs_update_end_clr_sw; //lint !e801
				}
				rgm_sprintf(&validate_res, "%s", temp_str);
				free(temp_str);
			}


			//
			// If the VALIDATE call fails (returns non-zero or
			// times out) then exit this function early with an
			// error.
			//
			if (rs_validate_v->ret_code != SCHA_ERR_NOERR) {
				const char *nodename = ln->getFullName();
				ucmm_print("idl_scrgadm_rs_update_prop()",
				    NOGET("VALIDATE failed\n"));
				res.err_code =
				    (scha_err_t)rs_validate_v->ret_code;

				rgm_format_errmsg(&res, REM_VALIDATE_FAILED,
				    rs_name, rg_name, nodename);

				goto rs_update_end_clr_sw; //lint !e801
			}
			// Set validate_done flag if we have run validation
			// successfully at least once.
			validate_done = B_TRUE;
		}

		// The global state machine mutex is still held.

		//
		// Check if this update has been "busted".  This means that a
		// current master of the RG has died or is being evacuated.
		// Since we haven't actually updated the CCR yet, we will return
		// early from the attempted update with an error ("cluster is
		// reconfiguring"), without the update having taken effect.
		// We have to run rebalance() because process_rg() did not
		// run it in the RGM reconfiguration step.
		//
		// Note, if we were busted by a node evacuation, we do not reset
		// rgl_switching and we do not run rebalance-- the evacuating
		// thread will run it while excluding evacuating node(s).
		//
		if ((ret = map_busted_err(rgp->rgl_switching)) !=
		    SCHA_ERR_NOERR) {
			// The RG has been busted, do early return
			ucmm_print("idl_scrgadm_rs_update_prop", NOGET(
			    "RG <%s> is busted with error <%s>"),
			    rg_name, rgm_error_msg(ret));
			res.err_code = ret;
			goto rs_update_end_clr_sw; //lint !e801
		}

		//
		// If validate_done flag is found unset here, it means none
		// of the validation candidates are in the cluster membership.
		// Return with SCHA_ERR_VALIDATE error.
		//
		if (!validate_done) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RG_TAG, rg_name);
			//
			// SCMSGS
			// @explanation
			// In order to change the properties of a resource whose
			// type has a registered VALIDATE method, the rgmd must
			// be able to run VALIDATE on at least one node.
			// However, all of the candidate nodes are down.
			// "Candidate nodes" are either members of the resource
			// group's Nodelist or members of the resource type's
			// Installed_nodes list, depending on the
			// setting of the resource's Init_nodes property.
			// @user_action
			// Boot one of the resource group's potential masters
			// and retry the resource change operation.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "Modification of resource <%s> failed because none "
			    "of the nodes on which VALIDATE would have run are "
			    "currently up", rs_name);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_VALIDATE;
			rgm_format_errmsg(&res, REM_VALIDATE_NO_NODES, rs_name);
			goto rs_update_end_clr_sw; //lint !e801
		}

#if (SOL_VERSION >= __s10)
	// Process inter-cluster strong deps.
	r_deps_new = rs_conf_no_defaults->r_dependencies;
	if (allow_inter_cluster() && dep_strong_updated) {
		res = update_icrd(r_deps_new, rs_conf_helper, DEPS_STRONG);
		if (res.err_code != SCHA_ERR_NOERR)
			goto rs_update_end_clr_sw;
	}

	// Process inter-cluster weak deps.
	if (allow_inter_cluster() && dep_weak_updated) {
		res = update_icrd(r_deps_new, rs_conf_helper, DEPS_WEAK);
		if (res.err_code != SCHA_ERR_NOERR)
			goto rs_update_end_clr_sw;
	}

	// Process inter-cluster restart deps.
	if (allow_inter_cluster() && dep_restart_updated) {
		res = update_icrd(r_deps_new, rs_conf_helper, DEPS_RESTART);
		if (res.err_code != SCHA_ERR_NOERR)
			goto rs_update_end_clr_sw;
	}

	// Process inter-cluster offline-restart deps.
	if (allow_inter_cluster() && dep_off_restart_updated) {
		res = update_icrd(r_deps_new, rs_conf_helper,
		    DEPS_OFFLINE_RESTART);
		if (res.err_code != SCHA_ERR_NOERR)
			goto rs_update_end_clr_sw;
	}
#endif


		//
		// Since the rgm state mutex was released during execution
		// of Validate methods, we have to check again whether the
		// RG is in a non-endish state or ERROR_STOP_FAILED.  This
		// repeats the checks that were done earlier in this function.
		//
		// First, flush the state change queue.  The initial set of
		// state changes on the queue might trigger restart-
		// dependencies on non-president slaves, which would cause
		// RG_ON_PENDING_R_RESTART state transitions to get pushed
		// immediately onto the queue.  So, we pass B_TRUE to
		// flush_state_change_queue() to indicate that it should use
		// the QEMPTY option to keep processing pending state changes
		// until the queue is empty.
		//
		rgm_unlock_state();
		flush_state_change_queue(B_TRUE);
		rgm_lock_state();

		//
		// There is a slight chance that a different president-side
		// thread will grab the state lock after the
		// flush_state_change_queue() call has finished, and trigger
		// a resource restart in our RG, before we have a chance to
		// reclaim the lock.  To avoid this, trigger_restart() checks
		// if rgl_switching == SW_UPDATING, and if the resource restart
		// would occur on a non-president slave.  If so, it releases
		// the lock and does a thr_yield() to return control to this
		// thread.
		//

		//
		// Now repeat the RG busy checks
		// We temporarily reset the rgl_switching flag for the
		// in_endish_rg_state() call.
		//
		save_rgl_switching = rgp->rgl_switching;
		rgp->rgl_switching = SW_NONE;
		if (!in_endish_rg_state(rgp, &pending_boot_ns, &stop_failed_ns,
		    NULL, &r_restart_ns) || !pending_boot_ns.isEmpty() ||
		    !r_restart_ns.isEmpty()) {
			// The RG is busy or running BOOT methods on some node
			res.err_code = SCHA_ERR_RGRECONF;
			rgm_format_errmsg(&res, REM_RG_BUSY, rg_name);
			ucmm_print("idl_scrgadm_rs_update_prop",
			    NOGET("R <%s> RG <%s> not in endish state after"
			    " Validate meths\n"), rs_name, rg_name);
			// restore saved value of rgl_switching
			rgp->rgl_switching = save_rgl_switching;
			goto rs_update_end_clr_sw; //lint !e801
		}
		// restore saved value of rgl_switching
		rgp->rgl_switching = save_rgl_switching;
		//
		// ERROR_STOP_FAILED is an endish state but still precludes
		// RS update
		//
		if (!stop_failed_ns.isEmpty()) {
			// The RG is ERROR_STOP_FAILED on some node
			res.err_code = SCHA_ERR_STOPFAILED;
			rgm_format_errmsg(&res, REM_RS_IN_RG_TO_STOPFAILED,
			    rs_name, rg_name);
			ucmm_print("idl_scrgadm_rs_update_prop",
			    NOGET("R <%s> RG <%s> is ERROR_STOP_FAILED after"
			    " Validate meths\n"), rs_name, rg_name);
			goto rs_update_end_clr_sw; //lint !e801
		}
	} // if (!ns->isEmpty() && !(rs_args.flags & rgm::SUPPRESS_VALIDATION))

rs_write_icd:

	//
	// We have to latch the intention to update the resource
	//

	//
	// We have to set the intention flags before latching one.
	// Each time a resource is updated, its update method will
	// be executed. This is the default behavior. But if an
	// rgm-internal property is being updated, then the update
	// method of the resource should not be executed. For example,
	// the property IC_Resource_dependents is an rgm internal
	// property and when it is updated, we should not run the
	// update method of the resource. This is handled below.
	//
	if (allow_inter_cluster()) {
		//
		// Do not run update method if IC_* properties
		// are being updated.
		//
		if (!inter_cluster_set_dep &&
		    !inter_cluster_unset_dep) {
			flags |= rgm::INTENT_RUN_UPDATE;
		}
	} else {
		flags |= rgm::INTENT_RUN_UPDATE;
	}

	// Latch the intention to update the resource properties

	ucmm_print("idl_scrgadm_rs_update_prop()",
	    NOGET("RS <%s> Latching\n"), rs_name);
	(void) latch_intention(rgm::RGM_ENT_RS, rgm::RGM_OP_UPDATE,
	    rs_name, &emptyNodeset, flags);

	ucmm_print("idl_scrgadm_rs_update_prop()",
	    NOGET("RS <%s> Calling CCR\n"), rs_name);

	//
	// Update the CCR with the new resource properties.
	//
	// Note that we save to CCR the version of the resource without
	// the RT defaults.
	//
	if ((res = rgmcnfg_update_resource(rs_conf_no_defaults,
	    (const char*)ZONE)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("RS <%s> CCR Failed\n"), rs_name);
		(void) unlatch_intention();
		goto rs_update_end_clr_sw; //lint !e801
	}


	ucmm_print("idl_scrgadm_rs_update_prop()",
	    NOGET("RS <%s> Processing\n"), rs_name);

	(void) process_intention();
	(void) unlatch_intention();

	//
	// post an event, ignoring the return code.
	// put after process intention, so that clients listening to
	// CCR change events don't get stale values from RGM.
	//
	(void) sc_publish_event(ESC_CLUSTER_R_CONFIG_CHANGE,
	    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
	    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
	    CL_R_NAME, SE_DATA_TYPE_STRING, rs_name,
	    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
	    CL_EVENT_CONFIG_PROP_CHANGED, NULL);

	if (inter_cluster_set_dep || inter_cluster_unset_dep) {
		if (rgp->rgl_switching != SW_EVACUATING) {
			rgp->rgl_switching = SW_NONE;
		}
		goto rs_update_end;
	}
	//
	// Check dependencies, in case we changed any.  We only need
	// to check starting dependencies, not stopping, because we
	// shouldn't be allowed to update any properties while a
	// resourace is in STOPPING_DEPEND (because that implies the
	// RG is in PENDING_OFFLINE or similar non-terminal state).
	//
	check_blocked_dependencies(rp, B_TRUE, B_FALSE);

	if (need_cond_wait) {
		//
		// Flush the state change queue before calling cond_wait()
		//
		// It's ok to unlock and relock the state here (we're about to
		// do the same thing in the cond_wait).  It will just take a
		// little longer (because other threads might get to run
		// first).
		//
		rgm_unlock_state();
		flush_state_change_queue();
		rgm_lock_state();

		// Sleep on slavewait condition variable until all
		// current masters have run the update method or died.
		// XXX 4215776 Implement FYI states in rgmd
		for (LogicalNodeManager::iter it = lnManager->begin();
		    it != lnManager->end(); ++it) {
			LogicalNode *lnNode = lnManager->findObject(it->second);
			CL_PANIC(lnNode != NULL);

			lni = lnNode->getLni();

			// If the RG's state is still RG_ON_PENDING_METHODS
			// on any node, wait.
			while (rgp->rgl_xstate[lni].rgx_state ==
			    rgm::RG_ON_PENDING_METHODS) {
				ucmm_print("idl_scrgadm_rs_update_prop()",
				    NOGET("update_resource wait slavewait\n"));
				cond_slavewait.wait(&Rgm_state->rgm_mutex);
				ucmm_print("idl_scrgadm_rs_update_prop()",
				    NOGET("after update_resource wait"
				    "slavewait\n"));
			}
		}
	}
	// XXX 4215776 Implement FYI states in rgmd
	// XXX Return "qualified success" if appropriate.

rs_update_end_clr_sw:

	if (rgp) {
		//
		// If the update was "busted", we must run rebalance() because
		// process_rg() did not run it in the RGM reconfiguration step.
		//
		// However, if we were busted by a node evacuation, we do
		// not reset rgl_switching and we do not run rebalance--
		// the evacuating thread will run it while excluding
		// evacuating node(s).
		//
		if (rgp->rgl_switching == SW_UPDATE_BUSTED) {
			ucmm_print("idl_scrgadm_rs_update_prop",
			    NOGET("RG <%s> is busted, running rebalance()"),
			    rg_name);
			rebalance(rgp, emptyNodeset, B_FALSE, B_TRUE);
		}

		if (rgp->rgl_switching != SW_EVACUATING) {
			rgp->rgl_switching = SW_NONE;
		}
	}

rs_update_end:

	//
	// issue a broadcast to wake up failback thread (if any) waiting for
	// this operation.
	//
	(void) cond_slavewait.broadcast();

	// Even if there is no other error encountered in this subroutine,
	// the special case SCHA_ERR_RS_VALIDATE might have occurred
	// due to a node not being present in the cluster.
	//
	if (res.err_code == SCHA_ERR_NOERR &&
	    validate_err_code != SCHA_ERR_NOERR) {
		res.err_code = validate_err_code;
		rgm_format_errmsg(&res, RWM_VALIDATE_NODES);
	}

	free(upgrade_rtname);
	free(rs_name);
	free(locale);
	if (rs_conf)
		rgm_free_resource(rs_conf);
	if (rs_conf_no_defaults)
		rgm_free_resource(rs_conf_no_defaults);
	if (rs_conf_helper)
		rgm_free_resource(rs_conf_helper);

	retval->ret_code = res.err_code;

	if (ns) {
		delete (ns);
	}
	/*
	 * Our final error message consists of the output from all VALIDATE
	 * methods, followed by any other error/waring/notice message.
	 */
	if (res.err_msg)
		rgm_sprintf(&validate_res, "%s", res.err_msg);

	if (warning_res.err_msg)
		rgm_sprintf(&validate_res, "%s", warning_res.err_msg);

	if ((temp_res.err_msg) && (res.err_code == SCHA_ERR_NOERR))
		rgm_sprintf(&validate_res, "%s", temp_res.err_msg);

	/* new_str checks for NULL */
	retval->idlstr_err_msg = new_str(validate_res.err_msg);

	/* free the old strings; free checks for NULL */
	free(res.err_msg);
	free(warning_res.err_msg);
	free(validate_res.err_msg);
	free(temp_res.err_msg);

	rs_res = retval;
	rgm_unlock_state();
}


//
// Remove a resource
//
// This operation may be "busted" by the death of one of the RG's primaries
// while we are doing the cond_wait() (waiting for FINI methods to complete).
// If so, we must run rebalance() on the RG, before returning.
//
// A scalable resource that has no registered FINI method is handled as
// a special case.  For such a resource, the FINI operation (deletion
// of the scalable service group and SAPs from the PDT server) is performed
// by the ssm_wrapper.  This action only needs to be carried out on
// one node, and it need not even be a node on which INIT was run, i.e.,
// it can be any node in the cluster.  Even if all of the nodes implied
// by the Init_nodes property (the potential masters of the RG or the
// Installed_nodes of the RT) are currently down, we can run the FINI
// method on the local node (i.e., the president).
// The verbose flag is added for the NewCli for the success message
// to be printed.
void
rgm_comm_impl::idl_scrgadm_rs_delete(
	const rgm::idl_rs_delete_args &rsdel_args,
	rgm::idl_regis_result_t_out rs_res, bool verbose_flag,
	Environment &)
{
	char			*rs_name = NULL, *rg_name = NULL;
	rglist_p_t		rgp = NULL;
	rlist_p_t		rp = NULL;
	LogicalNodeset		*ns = NULL;
	boolean_t		is_scalable;
	boolean_t		is_scalable_but_no_fini = B_FALSE;
	boolean_t		did_fini_run = B_FALSE;
	LogicalNodeset		pending_boot_ns;
	LogicalNodeset		stop_failed_ns;
	LogicalNodeset		r_restart_ns;
	LogicalNodeset 		*switch_ns;
	sol::nodeid_t		mynodeid = orb_conf::local_nodeid();
	sol::nodeid_t		nodeid = 0;

	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	incarnation_list_t	*incar_list = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	warning_res = {SCHA_ERR_NOERR, NULL};
	LogicalNodeset		emptyNodeset;
	rgm_rt_t		*rt = NULL;
	boolean_t		globalflag;
	boolean_t		nodelist_contains;

	rgm_lock_state();

	rs_name = strdup_nocheck(rsdel_args.idlstr_rs_name);
	if (rs_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto rs_delete_end; //lint !e801
	}

	if ((rp = rname_to_r(NULL, rs_name)) == NULL) {
		ucmm_print("idl_scrgadm_rs_delete()",
		    NOGET("Resource <%s> does not exist!!\n"), rs_name);
		res.err_code = SCHA_ERR_RSRC;
		rgm_format_errmsg(&res, REM_INVALID_RS, rs_name);
		goto rs_delete_end; //lint !e801
	}

	rgp = rp->rl_rg;
	rg_name = rgp->rgl_ccr->rg_name;
	ngzone_nodelist_check(rgp, rsdel_args.nodeid,
	    rsdel_args.idlstr_zonename, globalflag, nodelist_contains);

	if (!globalflag) {
		if (!nodelist_contains) {
			res.err_code = SCHA_ERR_ACCESS;
			rgm_format_errmsg(&res, REM_R_NODELIST_NGZONE,
			    rs_name, (const char *)rsdel_args.idlstr_zonename,
			    rgp->rgl_ccr->rg_name);
			goto rs_delete_end; //lint !e801
		}
	}


	// The RG must be in an "endish" state.
	if (!in_endish_rg_state(rgp, &pending_boot_ns, &stop_failed_ns,
	    NULL, &r_restart_ns) || !pending_boot_ns.isEmpty() ||
	    !r_restart_ns.isEmpty()) {
		// The RG is busy or running BOOT methods on some node
		res.err_code = SCHA_ERR_RGRECONF;
		rgm_format_errmsg(&res, REM_RG_BUSY, rg_name);
		ucmm_print("idl_scrgadm_rs_delete()",
		    NOGET("RG <%s> is not in an"
		    " endish state\n"), rg_name);
		goto rs_delete_end; //lint !e801
	}
	// ERROR_STOP_FAILED is an endish state but still precludes RG update
	if (!stop_failed_ns.isEmpty()) {
		// The RG is ERROR_STOP_FAILED on some node
		res.err_code = SCHA_ERR_STOPFAILED;
		rgm_format_errmsg(&res, REM_RS_IN_RG_TO_STOPFAILED, rs_name,
		    rg_name);
		ucmm_print("idl_scrgadm_rs_delete()",
		    NOGET("RG <%s> is "
		    "ERROR_STOP_FAILED\n"), rg_name);
		goto rs_delete_end; //lint !e801
	}

	switch_ns = get_logical_nodeset_from_nodelist(
	    rgp->rgl_ccr->rg_nodelist);
	for (nodeid = switch_ns->nextLniSet(1); nodeid != 0;
	    nodeid = switch_ns->nextLniSet(nodeid + 1)) {
		if ((((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)
		    ->r_switch[nodeid] != SCHA_SWITCH_DISABLED)) {
			ucmm_print("idl_scrgadm_rs_delete()", NOGET(
			    "Resource <%s> must be disabled on all nodes "
			    "before deletion\n"), rs_name);
			res.err_code = SCHA_ERR_STATE;
			rgm_format_errmsg(&res, REM_RS_DEL_ENABLED, rs_name);
			goto rs_delete_end; //lint !e801
		}
	}

	//
	// check for implicit and explicit restart, weak and strong dependent Rs
	// we don't care whether they are enabled
	//
	if (is_depended_on(rp, &res, &warning_res)) {
		goto rs_delete_end; //lint !e801
	}

	//
	// "system" property checks:
	//
	//	Can't remove an R from a "system" RG
	//
	if (rgp->rgl_ccr->rg_system) {
		res.err_code = SCHA_ERR_ACCESS;
		rgm_format_errmsg(&res, REM_SYSTEM_RES, rs_name, rg_name);
		ucmm_print("idl_scrgadm_rs_delete()",
		    NOGET("R <%s> is contained in RG <%s> whose RG_SYSTEM "
		    "property is TRUE\n"), rs_name, rg_name);
		goto rs_delete_end; //lint !e801
	}

	// Set a flag on rg indicating that it is being updated.
	// This acts as an advisory lock preventing other updates/switches.
	// If there are any failures after this point, we need to
	// go to rs_delete_end_clr_sw to reset the switching flag
	// back to none.
	rgp->rgl_switching = SW_UPDATING;

	is_scalable = is_r_scalable(rp->rl_ccrdata);

	//
	// Find the set of nodes on which FINI must run for r
	//
	// We do not use fininodes; instead, the FINI method
	// is executed on all nodes in Nodelist or Installed_nodes.
	// This might mean that FINI will be re-executed on a node on which
	// it was already executed.
	// This is harmless, due to the required idempotency of fini.
	//

	rt = rtname_to_rt(rp->rl_ccrdata->r_type);
	if (rt == NULL) {
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("Invalid RT = <%s>\n"),
		    rp->rl_ccrdata->r_type);
		res.err_code = SCHA_ERR_RT;
		goto rs_delete_end_clr_sw; //lint !e801
	}
	if (!globalflag && is_r_globalzone(rp)) {
		res.err_code = SCHA_ERR_ACCESS;
		rgm_format_errmsg(&res, REM_R_GLOBALFLAG_NGZONE, rs_name,
		    (const char *)rsdel_args.idlstr_zonename);
		goto rs_delete_end_clr_sw; //lint !e801
	}
	ns = compute_meth_nodes(rt, rgp->rgl_ccr, METH_FINI, is_scalable);

	//
	// If the resource's type is scalable and has no registered FINI method,
	// then we add the local node (i.e., the president) into the fini
	// nodeset because the ssm_wrapper's fini action can run on any node
	// in the cluster.  Even if all the usual fini nodes are down, the
	// fini action can still run on the president.
	//
	if (is_scalable && !is_method_reg(rp->rl_ccrtype, METH_FINI, NULL)) {
		is_scalable_but_no_fini = B_TRUE;
		ns->addLni(mynodeid);
	}

	//
	// Save incarnation of all nodes on which FINI is being executed,
	// so afterward we can find it out if the node died while executing
	// the fini method and rejoined the cluster.
	//
	save_incarnation(ns, &incar_list);

#if (SOL_VERSION >= __s10)
	// handle inter cluster resource dependencies.
	if (allow_inter_cluster()) {
		if (any_icr_dependents_for_this_r(rp)) {
			ucmm_print("RGM",
			    NOGET("cannot delete R with explicit ICR "
			    "dependents\n"));
			res.err_code = SCHA_ERR_DEPEND;
			rgm_format_errmsg(&res, REM_RS_DEL_DEP,
			    rp->rl_ccrdata->r_name);
			goto rs_delete_end;
		}
		delete_inter_cluster_dependencies(rp);
	}
#endif

	ucmm_print("RGM_idl_scrgadm_rs_delete()",
	    NOGET("RS <%s> Latching\n"), rs_name);

	(void) latch_intention(rgm::RGM_ENT_RS, rgm::RGM_OP_DELETE,
	    rs_name, ns, 0);

	// fault injection: kill President here
	FAULTPT_RGM(FAULTNUM_RGM_KILL_PRESIDENT_3, FaultFunctions::generic);

	//
	// First, remove resource from the CCR.
	//
	if ((res = rgmcnfg_remove_resource(rs_name,
	    rg_name, ZONE)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("idl_scrgadm_rs_delete()", NOGET(
		    "ERROR: Failed to remove resource <%s> in RG <%s> "
		    "from ccr, returned <%d>\n"),
		    rs_name, rg_name, res.err_code);
		(void) unlatch_intention();
		goto rs_delete_end_clr_sw; //lint !e801
	}

	// Print success message if the verbose flag is set.
	if (verbose_flag)
		rgm_format_successmsg(&res, RWM_R_DELETED,
		    rs_name);

	// fault injection: kill President here
	FAULTPT_RGM(FAULTNUM_RGM_KILL_PRESIDENT_4, FaultFunctions::generic);

	//
	// process the intention to delete r from all slaves' memory
	// and run fini on the nodes in ns.
	//
	// If ns is not empty, then some of the slave nodes will run
	// FINI method on the resource.  In this case we do a
	// cond_wait after processing the intention.  Slaves that are members
	// of ns will run FINI, and each slave will delete the resource from
	// its memory after FINI runs in all of its zones.
	//
	ucmm_print("idl_scrgadm_rs_delete()",
	    NOGET("RS <%s> Processing\n"), rs_name);
	(void) process_intention();
	(void) unlatch_intention();

	//
	// post an event, ignoring the return code.
	// put after process intention, so that clients listening to
	// CCR change events don't get stale values from RGM.
	//
	(void) sc_publish_event(ESC_CLUSTER_R_CONFIG_CHANGE,
	    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
	    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
	    CL_R_NAME, SE_DATA_TYPE_STRING, rs_name,
	    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
	    CL_EVENT_CONFIG_REMOVED, NULL);

	//
	// Note that if ns is empty, then the resource will have already
	// been deleted from memory by unlatch_intention_helper().
	//
	if (!ns->isEmpty()) {
		//
		// Even if we are not running FINI methods on the local
		// node (the president), we set rl_is_deleted so that we
		// will remove the resource from memory after FINI has
		// finished running on all the slaves.
		//
		rp->rl_is_deleted = B_TRUE;

		//
		// Flush the state change queue before calling cond_wait()
		//
		// It's ok to unlock and relock the state here (we're about to
		// do the same thing in the cond_wait).  It will just take a
		// little longer (because other threads might get to run
		// first).
		//
		rgm_unlock_state();
		flush_state_change_queue();
		rgm_lock_state();

		//
		// At this point it's theoretically possible that the resource
		// pointed to by rp has been deleted from president's memory
		// by set_rx_state(), if all Fini methods finished running
		// while the lock was released.  So, we re-lookup
		// rp by name.  If the resource is deleted, rp
		// will be NULL.
		//
		rp = rname_to_r(rgp, rs_name);

		//
		// Wait for R to move from pending fini to uninited.
		//
		// XXX bugid 4215776: Implement FYI states in rgmd
		// In the following loop, note that if a node dies, the
		// state of all resources on that node will be cleared to
		// OFFLINE.
		// The president's reconfig code that does this should note if
		// the resource was fini-ing on a node that died, and if so, it
		// should set the FINI-FAILED flag for that resource on that
		// node.
		//
		rgm::lni_t n = 0;
		while ((n = ns->nextLniSet(n + 1)) != 0) {
			//
			// If the R's state is still pending fini/finiing
			// on any node, wait.
			//
			while (rp != NULL &&
			    (rp->rl_xstate[n].rx_state == rgm::R_PENDING_FINI ||
			    rp->rl_xstate[n].rx_state == rgm::R_FINIING)) {
				ucmm_print("idl_scrgadm_rs_delete()", NOGET(
				    "scrgadm_rs_delete wait slavewait\n"));
				cond_slavewait.wait(&Rgm_state->rgm_mutex);
				//
				// rp might have been deleted during the
				// cond_wait so we have to lookup again
				//
				rp = rname_to_r(rgp, rs_name);
				ucmm_print("idl_scrgadm_rs_delete()",
				    NOGET("after scrgadm_rs_delete wait "
				    "slavewait\n"));
			}

			//
			// In following, if we find a node in fini nodeset has
			// died or died and rejoined the cluster, we assume
			// that the fini method did not run successfully on
			// that node.
			//

			//
			// Since in above cond_wait the state lock is released,
			// RGM reconfig could happen. Check whether the node
			// is still member of cluster.
			//
			if (!in_current_logical_membership(n))
				continue;

			// If we find the incarnation of the node is changed,
			// it means the node or zone rejoined the cluster.
			if (incarnation_changed(n, incar_list))
				continue;

			//
			// XXX bugid 4215776:
			// XXX we need to check FINI-FAILED flag here;
			// XXX if FINI failed, then do *not* remove node n from
			// XXX the nodeset ns.
			// XXX For now, let's assume that FINI succeeded on
			// XXX all nodes that remain cluster members.
			//

			//
			// At this point, we have successfully run FINI
			// (and/or the ssm_wrapper's FINI action) for this
			// resource on node n.
			//
			// Remove node n from the fini nodeset.
			//
			ns->delLni(n);

			//
			// If the resource's type is scalable and has no
			// registered FINI method, then we need not run the
			// ssm_wrapper's FINI action on any more nodes; one
			// node suffices.  However, we still have to continue
			// looping to wait for FINI methods (launched by the
			// process_intention call above) to complete on all
			// slave nodes.
			// We record the fact that FINI ran on at least one
			// node so that we can set nodeset ns to empty after
			// completing this loop.
			//
			did_fini_run = B_TRUE;
		}
	}

	//
	// When we get here, each lni in 'ns' has finished running the
	// fini action (either successfully or not), or has died or
	// rejoined the cluster.
	//
	// The non-president slaves all have removed the resource from memory
	// in launch_method (after FINI runs) or in unlatch_intention_helper
	// (if FINI didn't run).
	//
	// This node, the president, removes the resource from memory in
	// set_rx_state() after all slaves (including the president itself)
	// are finished running fini methods on it, or in
	// unlatch_intention_helper() if no FINI methods were run.
	//
	// Assert that the resource has been deleted from president's memory
	//
	CL_PANIC(rname_to_r(rgp, rs_name) == NULL);

	//
	// If the resource's type was scalable and it had no registered FINI
	// method, it is only necessary to have run the FINI action on any
	// one node.  If FINI ran on at least one node, set ns to empty.
	//
	if (is_scalable_but_no_fini && did_fini_run) {
		ns->reset();
	}

	// If ns is not empty, some nodes in fininodes did not complete FINI.
	// Return successfully with a warning message
	if (!ns->isEmpty()) {
		ucmm_print("idl_scrgadm_rs_delete()", NOGET(
		    "RS <%s>: FINI didn't run on all nodes\n"), rs_name);
		// XXX bugid 4215776: Implement FYI states in rgmd

		// Get the nodes name from the node set
		char *nodeliststr = get_string_from_nodeset(*ns);
		rgm_format_errmsg(&res, RWM_FINIMETHOD, rs_name, nodeliststr);
		res.err_code = SCHA_ERR_NOERR;
		free(nodeliststr);
	}

rs_delete_end_clr_sw:

	if (rgp) {
		//
		// If the deletion was "busted", we must run rebalance() because
		// process_rg() did not run it in the RGM reconfiguration step.
		//
		// However, if we were busted by a node evacuation, we do
		// not reset rgl_switching and we do not run rebalance--
		// the evacuating thread will run it while excluding
		// evacuating node(s).
		//
		if (rgp->rgl_switching == SW_UPDATE_BUSTED) {
			ucmm_print("idl_scrgadm_rs_delete()",
			    NOGET("RG <%s> is busted, running rebalance()"),
			    rg_name);
			rebalance(rgp, emptyNodeset, B_FALSE, B_TRUE);
		}

		if (rgp->rgl_switching != SW_EVACUATING) {
			rgp->rgl_switching = SW_NONE;
		}
	}

	if (incar_list)
		free_incarnation(incar_list);

rs_delete_end:

	//
	// issue a broadcast to wake up failback thread (if any) waiting for
	// this operation.
	//
	(void) cond_slavewait.broadcast();

	free(rs_name);

	retval->ret_code = res.err_code;

	//
	// If the warning message was set by is_depended_on due to
	// deletion request of implicit network resource the final
	// error message should include that message.
	//
	if ((!res.err_msg) && (warning_res.err_msg))
		rgm_sprintf(&res, "%s", warning_res.err_msg);
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);
	free(warning_res.err_msg);

	delete (ns);

	rs_res = retval;
	rgm_unlock_state();
}
#endif

//
// Validate the new values of a set of resource & RG properties.
// Executed on the slave, called by the president.
//
// This function acquires the rgm state lock, and releases it on exit.
//
#ifdef EUROPA_FARM
bool_t
rgmx_scrgadm_rs_validate_1_svc(rgmx_validate_args *rpc_args,
    rgmx_regis_result_t *rpc_res, struct svc_req *)
#else
void
rgm_comm_impl::idl_scrgadm_rs_validate(
    sol::nodeid_t president,
    sol::incarnation_num incarnation,
    rgm::lni_t lni,
    rgm::ln_incarnation_t ln_incarnation,
    const rgm::idl_validate_args &rs_args,
    rgm::idl_regis_result_t_out rs_res,
    Environment &env)
#endif
{
#ifdef EUROPA_FARM
	sol::nodeid_t president = rpc_args->president;
	sol::incarnation_num incarnation = rpc_args->incarnation;
	rgm::lni_t lni = rpc_args->lni;
	rgm::ln_incarnation_t ln_incarnation = rpc_args->ln_incarnation;
	rgm::idl_validate_args rs_args;
	rgm::idl_regis_result_t *rs_res;

	bzero(rpc_res, sizeof (rgmx_regis_result_t));
	rs_args.idlstr_rs_name = strdup_nocheck(rpc_args->idlstr_rs_name);
	rs_args.idlstr_rt_name = strdup_nocheck(rpc_args->idlstr_rt_name);
	rs_args.idlstr_rg_name = strdup_nocheck(rpc_args->idlstr_rg_name);
	rs_args.locale = strdup_nocheck(rpc_args->locale);
	rs_args.opcode = (rgm::rgm_operation) rpc_args->opcode;
	rs_args.is_scalable = rpc_args->is_scalable;
	array_to_strseq_2(rpc_args->rs_props, rs_args.rs_props);
	array_to_strseq_2(rpc_args->ext_props, rs_args.ext_props);
	array_to_strseq_2(rpc_args->rg_props, rs_args.rg_props);

	rpc_res->retval = RGMRPC_OK;
#endif
	launchvalargs_t	launch_args;
	rgm_rt_t *rt = NULL;
	char *rg_name = NULL, *rs_name = NULL, *rt_name = NULL, *locale = NULL;
	uint_t len, i;
	rgm::idl_string_seq_t seq_p;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	boolean_t lock_is_held = B_TRUE;

	launch_args.lv_rt_meth_name = 0;
	// try initialising all elements to zero above

	// Obtain the global state machine mutex.
	rgm_lock_state();

	if (stale_idl_call(president, incarnation)) {
		//
		// The call is from a dead President. Ignore.
		// The return code shouldn't matter, as the caller is dead
		//
		res.err_code = SCHA_ERR_NODE;
		goto rs_validate_end; //lint !e801
	}

	// Return early with IDL exception if zone died or is shutting down.
#ifndef EUROPA_FARM
	if (zone_died(lni, ln_incarnation, env)) {
#else
	if (zone_died(lni, ln_incarnation, &rpc_res->retval)) {
#endif
		// is this err code OK?
		res.err_code = SCHA_ERR_NODE;
		goto rs_validate_end; //lint !e801
	}

	rg_name = strdup_nocheck(rs_args.idlstr_rg_name);
	rs_name = strdup_nocheck(rs_args.idlstr_rs_name);
	rt_name = strdup_nocheck(rs_args.idlstr_rt_name);
	locale = strdup_nocheck(rs_args.locale);
	if (rs_name == NULL || rg_name == NULL || rt_name == NULL ||
	    locale == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto rs_validate_end; //lint !e801
	}

	ucmm_print("RGM_idl_scrgadm_rs_validate",
	    NOGET("RG = <%s> RS <%s> RT <%s>\n"), rg_name, rs_name, rt_name);

	rt = rtname_to_rt(rt_name);
	if (!rt) {
		ucmm_print("idl_scrgadm_rs_validate()",
		    NOGET("Invalid RT = <%s>\n"), rt_name);
		res.err_code = SCHA_ERR_RT;
		goto rs_validate_end; //lint !e801
	}

	launch_args.lv_rname = rs_name;
	launch_args.lv_rtname = rt_name;
	launch_args.lv_rgname = rg_name;
	launch_args.lv_method = METH_VALIDATE;
	launch_args.lv_opcode = rs_args.opcode;
	launch_args.lv_rt_meth_name = NULL;
	// If resource is scalable, will use SSM wrapper as the method
	launch_args.lv_is_scalable = (boolean_t)rs_args.is_scalable;
	launch_args.locale = locale;

	//
	// The caller (president) has already checked that the VALIDATE method
	// is registered on the RT and applicable to the resource on this node.
	// If the resource type has somehow changed out from under us,
	// such that the VALIDATE method is no longer registered for rt
	// (which ordinarily will not be permitted by the president),
	// we will return failure.
	//
	// Scalable service resources use ssm_wrapper for VALIDATE and don't
	// require the validate method to be registered.
	//
	if (!is_method_reg(rt, METH_VALIDATE, &(launch_args.lv_rt_meth_name)) &&
	    !rs_args.is_scalable) {
		ucmm_print("RGM_idl_scrgadm_rs_validate", NOGET(
		    "VALIDATE not registered for R <%s>, fail validate\n"),
		    rs_name);
		res.err_code = SCHA_ERR_INTERNAL;
		goto rs_validate_end; //lint !e801
	}

	// resource pre-defined properties
	len = rs_args.rs_props.length();
	launch_args.lv_r_props = rgm::idl_string_seq_t(len);
	launch_args.lv_r_props.length(len);
	for (i = 0; i < len; i++)
		launch_args.lv_r_props[i] = rs_args.rs_props[i];

	// resource extension properties
	len = rs_args.ext_props.length();
	launch_args.lv_x_props = rgm::idl_string_seq_t(len);
	launch_args.lv_x_props.length(len);
	for (i = 0; i < len; i++)
		launch_args.lv_x_props[i] = rs_args.ext_props[i];

	// per-node extension property values on all nodes
	len  = rs_args.all_nodes_ext_props.length();
	launch_args.lv_allnodes_x_props = rgm::idl_string_seq_t(len);
	launch_args.lv_allnodes_x_props.length(len);
	for (i = 0; i < len; i++)
		launch_args.lv_allnodes_x_props[i] =
		    rs_args.all_nodes_ext_props[i];

	// rg properties
	len = rs_args.rg_props.length();
	launch_args.lv_g_props = rgm::idl_string_seq_t(len);
	launch_args.lv_g_props.length(len);
	for (i = 0; i < len; i++)
		launch_args.lv_g_props[i] = rs_args.rg_props[i];

	ucmm_print("RGM_idl_scrgadm_rs_validate",
	    NOGET("call launch_validate_method\n"));

	//
	// The protocol between idl_scrgadm_rs_validate and
	// launch_validate_method requires that we hold the state
	// lock before calling launch_validate_method, and that
	// launch_validate_method will release the lock before returning.
	//
	res = launch_validate_method(&launch_args, lni);
	lock_is_held = B_FALSE;

rs_validate_end:

	free(launch_args.lv_rt_meth_name);
	free(rs_name);
	free(rg_name);
	free(rt_name);
	free(locale);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	// Release the global state machine mutex if necessary
	if (lock_is_held)
	    rgm_unlock_state();

	rs_res = retval;
#ifdef EUROPA_FARM
	rpc_res->ret_code = rs_res->ret_code;
	if ((char *)rs_res->idlstr_err_msg == NULL)
		rpc_res->idlstr_err_msg = NULL;
	else
		rpc_res->idlstr_err_msg = strdup_nocheck(
		rs_res->idlstr_err_msg);

	return (TRUE);
#endif
}

//
// get_param_prop
//
// Iterates through all the properties in the paramtable, checking
// if each one has the name prop_name.  If so, it returns the rgm_param_t
// struct for that property.  Otherwise, returns NULL.
//
// Distinguishes between system and extension properties.  The p_extension
// field of the rgm_param_t must match the is_extension parameter.
//
const rgm_param_t *
get_param_prop(const char *prop_name, boolean_t is_extension,
    rgm_param_t **paramtable)
{
	rgm_param_t	**param;

	for (param = paramtable; *param; param++) {
		if (strcasecmp((*param)->p_name, prop_name) == 0 &&
		    (((*param)->p_extension && is_extension) ||
		    (!(*param)->p_extension && !is_extension))) {
			return (*param);
		}
	}
	return (NULL);
}

#ifndef EUROPA_FARM
//
// Set the dynamic status of a resource FM (set by fault monitor)
//
void
rgm_comm_impl::idl_scha_rs_set_status(
	const rgm::idl_rs_set_status_args &setst_args,
	rgm::idl_set_status_result_t_out setst_res, Environment &)
{

	char *rg_name = NULL, *rs_name = NULL, *status_msg = NULL;
	char *node_name = NULL;
	char	*fmstatus;
	rgm::idl_set_status_result_t *retval =
	    new rgm::idl_set_status_result_t();
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	boolean_t nomem = B_FALSE;

	rs_name = strdup_nocheck(setst_args.idlstr_rs_name);
	rg_name = strdup_nocheck(setst_args.idlstr_rg_name);
	node_name = strdup_nocheck(setst_args.idlstr_node_name);
	fmstatus = strdup_nocheck(setst_args.idlstr_status);

	if ((const char *)setst_args.idlstr_status_msg == NULL) {
		status_msg = (char *)NULL;
	} else {
		status_msg = strdup_nocheck(setst_args.idlstr_status_msg);
		if (status_msg == NULL) {
			nomem = B_TRUE;
		}
	}

	if (rs_name == NULL || rg_name == NULL || node_name == NULL ||
	    fmstatus == NULL || nomem) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	//
	// update the R FM status on the specified node
	//
	if ((res = rgm_comm_setfmstatus(rs_name, rg_name, node_name, fmstatus,
	    status_msg)).err_code == SCHA_ERR_NOERR) {
		ucmm_print("idl_scha_rs_set_status()",
		    NOGET("Updating FM status on node <%s> was done \n"),
		    node_name);
	} else
		ucmm_print("idl_scha_rs_set_status()", NOGET(
		    "Updating FM status on node <%s> failed with err : %d"),
		    node_name, res.err_code);

finished:

	free(rs_name);
	free(rg_name);
	free(status_msg);
	free(fmstatus);
	free(node_name);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);
	free(res.err_msg);
	setst_res = retval;
}

#if (SOL_VERSION >= __s10)
void
rgm_comm_impl::idl_get_remote_rsrcs_in_dep_line_of_r(
	const rgm::idl_rs_get_icrd_rsrcs_args &rs_args,
	rgm::idl_rs_get_icrd_rsrcs_result_t_out rs_res, Environment &)
{
	char *rs_name = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rgm::idl_rs_get_icrd_rsrcs_result_t *retval =
	    new rgm::idl_rs_get_icrd_rsrcs_result_t();
	rlist_t *rs_ptr = NULL;
	rdeplist_t *icrd_dep_list = NULL;
	int r_dep_count = 0;
	rdeplist_t *dep_list_itr = NULL;

	rs_name = strdup_nocheck(rs_args.idlstr_rs_name);

	if (rs_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished; //lint !e801
	}

	// First get a pointer to the resource
	rs_ptr = rname_to_r(NULL, rs_name);
	if (rs_ptr == NULL) {
		// Invalid resource name specified
		res.err_code = SCHA_ERR_RSRC;
		goto finished; //lint !e801
	}

	res = add_remote_r_dependencies_to_list(rs_ptr, &icrd_dep_list);
	// Handle any errors
	if (res.err_code != SCHA_ERR_NOERR) {
		goto finished; //lint !e801
	}

	// Now, we have to set the response.
	// First, we have to count the number of dependencies
	dep_list_itr = icrd_dep_list;
	while (dep_list_itr) {
		dep_list_itr = dep_list_itr->rl_next;
		r_dep_count++;
	}

	retval->value_list = rgm::idl_string_seq_t(r_dep_count);

	// Now, we can set the dependencies in the result
	r_dep_count = 0;
	dep_list_itr = icrd_dep_list;
	while (dep_list_itr) {
		retval->value_list[r_dep_count] = strdup(dep_list_itr->rl_name);
		dep_list_itr = dep_list_itr->rl_next;
		r_dep_count++;
	}
	retval->value_list.length(r_dep_count);

finished:

	if (rs_name)
		free(rs_name);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);
	rs_res = retval;
}

static scha_errmsg_t
add_remote_r_dependencies_to_list(rlist_t *rs_ptr,
	rdeplist_t **icrd_dep_list)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rgm_dependencies_t r_dependencies;
	rdeplist_t *r_dp_list;
	rlist_t *dep_r;

	if (rs_ptr == NULL) {
		return (res);
	}

	r_dependencies = rs_ptr->rl_ccrdata->r_dependencies;
	*icrd_dep_list = NULL;

	// We will first check for Strong dependencies
	for (r_dp_list = r_dependencies.dp_strong;
		r_dp_list;
		r_dp_list = r_dp_list->rl_next) {

		// We will check whether this is a remote resource.
		if (is_enhanced_naming_format_used(r_dp_list->rl_name)) {
			// This is a remote resource. So, we will add it
			// to the list
			res = rdeplist_add_name(icrd_dep_list,
						r_dp_list->rl_name,
						r_dp_list->locality_type);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}

			continue;
		}
		// This is a local resource. It is possible that
		// this resource has an inter-cluster-R dependency.
		// We have to look for that dependency as well.
		dep_r = rname_to_r(NULL, r_dp_list->rl_name);
		if (dep_r == NULL) {
			// We should not be here ideally.
			res.err_code = SCHA_ERR_INTERNAL;
			return (res);
		}

		res = add_remote_r_dependencies_to_list(dep_r,
						icrd_dep_list);
		// Handle errors
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
	}
	// Check for Weak dependencies
	for (r_dp_list = r_dependencies.dp_weak;
		r_dp_list;
		r_dp_list = r_dp_list->rl_next) {

		// We will check whether this is a remote resource.
		if (is_enhanced_naming_format_used(r_dp_list->rl_name)) {
			// This is a remote resource. So, we will add it
			// to the list
			res = rdeplist_add_name(icrd_dep_list,
						r_dp_list->rl_name,
						r_dp_list->locality_type);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}

			continue;
		}
		// This is a local resource. It is possible that
		// this resource has an inter-cluster-R dependency.
		// We have to look for that dependency as well.
		dep_r = rname_to_r(NULL, r_dp_list->rl_name);
		if (dep_r == NULL) {
			// We should not be here ideally.
			res.err_code = SCHA_ERR_INTERNAL;
			return (res);
		}

		res = add_remote_r_dependencies_to_list(dep_r,
					icrd_dep_list);
		// Handle errors
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
	}
	// Check for Restart dependencies
	for (r_dp_list = r_dependencies.dp_restart;
		r_dp_list;
		r_dp_list = r_dp_list->rl_next) {

		// We will check whether this is a remote resource.
		if (is_enhanced_naming_format_used(r_dp_list->rl_name)) {
			// This is a remote resource. So, we will add it
			// to the list
			res = rdeplist_add_name(icrd_dep_list,
					r_dp_list->rl_name,
					r_dp_list->locality_type);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}

			continue;
		}
		// This is a local resource. It is possible that
		// this resource has an inter-cluster-R dependency.
		// We have to look for that dependency as well.
		dep_r = rname_to_r(NULL, r_dp_list->rl_name);
		if (dep_r == NULL) {
			// We should not be here ideally.
			res.err_code = SCHA_ERR_INTERNAL;
			return (res);
		}

		res = add_remote_r_dependencies_to_list(dep_r,
						icrd_dep_list);
		// Handle errors
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
	}
	// Check for Offline-Restart dependencies
	for (r_dp_list = r_dependencies.dp_offline_restart;
		r_dp_list;
		r_dp_list = r_dp_list->rl_next) {

		// We will check whether this is a remote resource.
		if (is_enhanced_naming_format_used(r_dp_list->rl_name)) {
			// This is a remote resource. So, we will add it
			// to the list
			res = rdeplist_add_name(icrd_dep_list,
					    r_dp_list->rl_name,
					    r_dp_list->locality_type);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}

			continue;
		}
		// This is a local resource. It is possible that
		// this resource has an inter-cluster-R dependency.
		// We have to look for that dependency as well.
		dep_r = rname_to_r(NULL, r_dp_list->rl_name);
		if (dep_r == NULL) {
			// We should not be here ideally.
			res.err_code = SCHA_ERR_INTERNAL;
			return (res);
		}

		res = add_remote_r_dependencies_to_list(dep_r,
						icrd_dep_list);
		// Handle errors
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
	}

	return (res);
}


// deletes the inter cluster dependent information.
void
delete_dependent_info(name_t r_name, namelist_t *r_list,
    deptype_t dep)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t	handle;
	// Unset the dependent info from the depended-on resource.
	for (; r_list != NULL; r_list = r_list->nl_next) {
		res = update_dependent_info(r_list->nl_name, B_FALSE,
		    r_name, dep);
		if (res.err_code != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RS_TAG, r_name);
			//
			// SCMSGS
			// @explanation
			// Failed to update the information of the depended-on
			// resource.
			// Possible cause could be that the remote zone cluster
			// is down or the containg rg of the depended on
			// resource is switching or the remote depended on
			// resource itself is invalid.
			// @user_action
			// No action.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"Could not update the dependent resource <%s> info on "
			"the inter-cluster depended on resource <%s>",
			r_name, r_list->nl_name);
			sc_syslog_msg_done(&handle);
			continue;
		}
	}
}	

//
// helper function to unset the dependent information from the inter-cluster
// depended on resource. Called when deleting the dependent resource.
//
void delete_inter_cluster_dependencies(rlist_p_t rp) {
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	namelist_t *r_list = NULL;

	//
	// Check if there are any inter-cluster strong dependencies for this
	// resource.
	//
	res = get_intercluster_rdep_lists(
	    rp->rl_ccrdata->r_dependencies, &r_list, NULL, NULL, NULL);
	if (res.err_code != SCHA_ERR_NOERR) {
		goto delete_icd_end;
	}

	if (r_list != NULL) {
		delete_dependent_info(rp->rl_ccrdata->r_name, r_list,
		    DEPS_STRONG);
	}
	if (r_list != NULL) {
		rgm_free_nlist(r_list);
		r_list = NULL;
	}

	//
	// Check if there are any inter-cluster weak dependencies for this
	// resource.
	//

	res = get_intercluster_rdep_lists(rp->rl_ccrdata->r_dependencies, NULL,
	    &r_list, NULL, NULL);
	if (res.err_code != SCHA_ERR_NOERR) {
		goto delete_icd_end;
	}

	if (r_list != NULL) {
		// Unset the dependent info from the depended-on resource.
		delete_dependent_info(rp->rl_ccrdata->r_name, r_list,
		    DEPS_WEAK);
	}
	if (r_list != NULL) {
		rgm_free_nlist(r_list);
		r_list = NULL;
	}

	//
	// Check if there are any inter-cluster restart dependencies for this
	// resource.
	//
	res = get_intercluster_rdep_lists(rp->rl_ccrdata->r_dependencies, NULL,
	    NULL, &r_list, NULL);
	if (res.err_code != SCHA_ERR_NOERR) {
		goto delete_icd_end;
	}

	if (r_list != NULL) {
		// Unset the dependent info from the depended-on resource.
		delete_dependent_info(rp->rl_ccrdata->r_name, r_list,
		    DEPS_RESTART);
	}
	if (r_list != NULL) {
		rgm_free_nlist(r_list);
		r_list = NULL;
	}

	//
	// Check if there are any inter-cluster offline restart dependencies
	// for this resource.
	//
	res = get_intercluster_rdep_lists(rp->rl_ccrdata->r_dependencies, NULL,
	    NULL, NULL, &r_list);
	if (res.err_code != SCHA_ERR_NOERR) {
		goto delete_icd_end;
	}
	if (r_list != NULL) {
		// Unset the dependent info from the depended-on resource.
		delete_dependent_info(rp->rl_ccrdata->r_name, r_list,
		    DEPS_OFFLINE_RESTART);
	}
delete_icd_end :
	if (r_list != NULL) {
		rgm_free_nlist(r_list);
	}
}
#endif


//
// In following validate_prop_*() function, the value passed in
// is the pointer to either default value (if the property has
// default value defined in paramtable and user doesn't overwrite
// it) or the value set by user when creating resource (no matter
// whether the property has default or not). In both case, the
// code should do validation check to the value.
//

//
// Validate if the given integer value is valid in paramtable
//
static scha_errmsg_t
validate_prop_int(std::map<rgm::lni_t, char *> &val, rgm_param_t *param,
    LogicalNodeset *ns)
{
	char *endp;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *value = NULL;

	if (param == NULL) {
		// should not happen
		res.err_code = SCHA_ERR_INTERNAL;
		rgm_format_errmsg(&res, REM_INTERNAL);
		return (res);
	}

	if (!param->p_per_node) {
		value = (char *)val[0];

		// Following check enables the validation to check
		// either the default value or the value defined by
		// user at resource creation.
		if (value == NULL) {
			if (param->p_default_isset == B_TRUE) {
				// default is already set in the rtr file.
				// Return with no error.
				return (res);
			} else {
				// The default is required but it hasn't been
				// set.  Return error.
				ucmm_print("validate_prop_int()",
				    NOGET("ERROR: Missing value for property "
				    "<%s>\n"),
				    param->p_name);
				res.err_code = SCHA_ERR_PINT;
				rgm_format_errmsg(&res,
				    REM_PROP_NO_DEFAULT,
				    param->p_name);
				return (res);
			}
		}

		// When value is set to empty, return error.
		if (value[0] == '\0') {
			ucmm_print("validate_prop_int()",
			    NOGET("value is blank"));
			res.err_code = SCHA_ERR_PINT;
			rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
			    param->p_name);
			return (res);
		}

		long vl = strtol(value, &endp, 10);

		// check if value is an integer
		if (*endp != '\0' || (vl == 0 && strcmp(value, "0") != 0)) {
			ucmm_print("validate_prop_int()",
			    NOGET("ERROR: property value <%s> should be an "
			    "integer\n"),
			    value);
			res.err_code = SCHA_ERR_PINT;
			rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
			    param->p_name);
			return (res);
		}

		if (param->p_min_isset == B_TRUE && vl < param->p_min) {
			ucmm_print("validate_prop_int()", NOGET(
			    "ERROR: property value <%d> should be greater than "
			    "or equal to <%d>\n"),
			    vl, param->p_min);
			res.err_code = SCHA_ERR_PINT;
			rgm_format_errmsg(&res, REM_PROP_PINT_MIN,
			    param->p_name);
			return (res);
		}

		if (param->p_max_isset == B_TRUE && vl > param->p_max) {
			ucmm_print("validate_prop_int()", NOGET(
			    "ERROR: property value <%d> should be smaller than "
			    "or equal to <%d>\n"),
			    vl, param->p_max);
			res.err_code = SCHA_ERR_PINT;
			rgm_format_errmsg(&res, REM_PROP_PINT_MAX,
			    param->p_name);
			return (res);
		}
	} else {
		for (rgm::lni_t nodeid = ns->nextLniSet(1); nodeid != 0;
		    nodeid = ns->nextLniSet(nodeid + 1)) {

			value = (char *)val[nodeid];

			// Following check enables the validation to check
			// either the default value or the value defined by
			// user at resource creation.
			if (value == NULL) {
				if (param->p_default_isset == B_TRUE) {
					// default is already set
					// in the rtr file.
					// Return with no error.
					return (res);
				} else {
					// The default is required but it
					// hasn't been
					// set.  Return error.
					ucmm_print("validate_prop_int()",
					    NOGET("ERROR: Missing value for "
					    "property "
					    "<%s> on node <%d>\n"),
					    param->p_name,
					    nodeid);
					res.err_code = SCHA_ERR_PINT;
					rgm_format_errmsg(&res,
					    REM_PROP_NO_DEFAULT,
					    param->p_name);
					return (res);
				}
			}

			// When value is set to empty, return error.
			if (value[0] == '\0') {
				ucmm_print("validate_prop_int()",
				    NOGET("value is a blank\n"));
				res.err_code = SCHA_ERR_PINT;
				rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
				    param->p_name);
				return (res);
			}

			long vl = strtol(value, &endp, 10);

			// check if value is an integer
			if (*endp != '\0' || (vl == 0 &&
			    strcmp(value, "0") != 0)) {
				ucmm_print("validate_prop_int()",
				    NOGET("ERROR: property value <%s> should "
				    "be an "
				    "integer on node <%d>\n"), value, nodeid);
				res.err_code = SCHA_ERR_PINT;
				rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
				    param->p_name);
				return (res);
			}

			if (param->p_min_isset == B_TRUE && vl < param->p_min) {
				ucmm_print("validate_prop_int()", NOGET(
				    "ERROR: property value <%d> should be "
				    "greater "
				    "than or equal to <%d> on node <%d>\n"),
				    vl, param->p_min, nodeid);
				res.err_code = SCHA_ERR_PINT;
				rgm_format_errmsg(&res, REM_PROP_PINT_MIN,
				    param->p_name);
				return (res);
			}

			if (param->p_max_isset == B_TRUE && vl >
			    param->p_max) {
				ucmm_print("validate_prop_int()", NOGET(
				    "ERROR: property value <%d> should be "
				    "smaller "
				    "than or equal to <%d> on node <%d>\n"),
				    vl, param->p_max, nodeid);
				res.err_code = SCHA_ERR_PINT;
				rgm_format_errmsg(&res, REM_PROP_PINT_MAX,
				    param->p_name);
				return (res);
			}
		}
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}


//
// validate if the given string is a boolean string
//	for example, "TRUE" or "FALSE"
//
static scha_errmsg_t
validate_prop_bool(std::map<rgm::lni_t, char *> &val, rgm_param_t *param,
    LogicalNodeset *ns)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *value = NULL;
	uint_t nodeid;


	if (param == NULL) {
		// should not happen
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}
	if (!param->p_per_node) {
		value = (char *)val[0];
		// cannot set the boolean type property to NULL
		if (value == NULL) {
			if (param->p_default_isset == B_TRUE) {
				// No value is set, and default already set
				// Return with no error.
				return (res);
			} else {
				// The default is required but it hasn't been
				// set.  Return error.
				ucmm_print("validate_prop_bool()",
				    NOGET("ERROR: Missing value for property "
				    "<%s>\n"),
				    param->p_name);
				res.err_code = SCHA_ERR_PBOOL;
				rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
				    param->p_name);
				return (res);
			}
		}
		if (strcasecmp(value, SCHA_TRUE) != 0 &&
		    strcasecmp(value, SCHA_FALSE) != 0) {
			ucmm_print("validate_prop_bool()", NOGET("ERROR: "
			    "property value <%s> should be either <%s> or "
			    " <%s>\n"),
			    value, SCHA_TRUE, SCHA_FALSE);
			res.err_code = SCHA_ERR_PBOOL;
			rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
			    param->p_name);
			return (res);
		} else {
			res.err_code = SCHA_ERR_NOERR;
			return (res);
		}
	} else {
		for (nodeid = ns->nextLniSet(1); nodeid != 0;
		    nodeid = ns->nextLniSet(nodeid + 1)) {

			value = (char *)val[nodeid];
			// cannot set the boolean type property to NULL
			if (value == NULL) {
				if (param->p_default_isset == B_TRUE) {
					// No value is set, and
					// default already set
					// Return with no error.
					return (res);
				} else {
					// The default is required but
					// it hasn't been
					// set.  Return error.
					ucmm_print("validate_prop_bool()",
					    NOGET("ERROR: Missing value for "
					    "property <%s> "
					    "on node <%d>\n"),
					    param->p_name, nodeid);
					res.err_code = SCHA_ERR_PBOOL;
					rgm_format_errmsg(&res,
					    REM_INVALID_PROP_VALUES,
					    param->p_name);
					return (res);
				}
			}

			if (strcasecmp(value, SCHA_TRUE) != 0 &&
			    strcasecmp(value, SCHA_FALSE) != 0) {
				ucmm_print("validate_prop_bool()",
				    NOGET("ERROR: "
				    "property value <%s> should be either "
				    "<%s> or <%s> "
				    "on node <%d>\n"), value, SCHA_TRUE,
				    SCHA_FALSE);
				res.err_code = SCHA_ERR_PBOOL;
				rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
				    param->p_name);
				return (res);
			} else {
				res.err_code = SCHA_ERR_NOERR;
				return (res);
			}
		}
	}
}


//
// validate if the given string value is valid in paramtable
//
// Note: In validate_prop_string, the validation of string
// is done by checking the size of the value passed in and the value.
// No p_default_isset flag will be checked since there are some
// string properties which are required to be initialized
// (with min_size > 0) at resource creation but with no default
// value defined.
//
static scha_errmsg_t
validate_prop_string(std::map<rgm::lni_t, char *> &val, rgm_param_t *param,
    LogicalNodeset *ns)
{
	uint_t len, nodeid;
	char *value = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (param == NULL) {
		// should not happen
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if (!param->p_per_node) {
		value = (char *)val[0];


		// check the length of the property value
		if (value == NULL)
			len = 0;
		else
			len = (uint_t)strlen(value);

		if (param->p_min_isset == B_TRUE &&
		    len < (uint_t)param->p_min) {
			if (len == 0)
				ucmm_print("validate_prop_string()",
				    NOGET("Missing value for property <%s>"),
			    param->p_name);
			else
				ucmm_print("validate_prop_string()",
				    NOGET("ERROR: the string length of "
				    "property value"
				    " <%s> should be greater than or "
				    "equal to <%d>\n"),
				    value, param->p_min);
			res.err_code = SCHA_ERR_PSTR;
			rgm_format_errmsg(&res, REM_PROP_PSTR_MINLEN,
			    param->p_name);
			return (res);
		}

		if (param->p_max_isset == B_TRUE &&
		    len > (uint_t)param->p_max) {
			ucmm_print("validate_prop_string()",
			    NOGET("ERROR: the string length of property value"
			    " <%s> should be smaller than or equal to <%d>\n"),
			    value, param->p_max);
			res.err_code = SCHA_ERR_PSTR;
			rgm_format_errmsg(&res, REM_PROP_PSTR_MAXLEN,
			    param->p_name);
			return (res);
		}

		//
		// validate property value
		// 'TRUE' as the 2nd argument to 'is_rgm_value_valid' call
		// indicates comma is a valid character
		//
		if (!is_rgm_value_valid(value, B_TRUE)) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
			    param->p_name);
			return (res);
		}

		res.err_code = SCHA_ERR_NOERR;
		return (res);
	} else {
		for (nodeid = ns->nextLniSet(1); nodeid != 0;
			nodeid = ns->nextLniSet(nodeid + 1)) {

			value = (char *)val[nodeid];

			// check the length of the property value
			if (value == NULL)
				len = 0;
			else
				len = (uint_t)strlen(value);

			if (param->p_min_isset == B_TRUE && len <
			    (uint_t)param->p_min) {
				if (len == 0)
					ucmm_print("validate_prop_string()",
					    NOGET("Missing value for property "
					    "<%s> on node %d"),
					    param->p_name, nodeid);
				else
					ucmm_print("validate_prop_string()",
					    NOGET("ERROR: the string length "
					    "of property value "
					    "<%s> should be greater than "
					    "or equal to <%d> "
					    "on node <%d>\n"),
					    value, param->p_min, nodeid);
				res.err_code = SCHA_ERR_PSTR;
				rgm_format_errmsg(&res, REM_PROP_PSTR_MINLEN,
				    param->p_name);
				return (res);
			}

			if (param->p_max_isset == B_TRUE && len >
			    (uint_t)param->p_max) {
				ucmm_print("validate_prop_string()",
				    NOGET("ERROR: the string length "
				    "of property value <%s> should be "
				    "smaller than or equal to <%d> on node"
				    " <%d>\n"), value, param->p_max);
				res.err_code = SCHA_ERR_PSTR;
				rgm_format_errmsg(&res, REM_PROP_PSTR_MAXLEN,
				    param->p_name);
				return (res);
			}

			//
			// validate property value
			// 'TRUE' as the 2nd argument to
			// 'is_rgm_value_valid' call
			// indicates comma is a valid character
			//
			if (!is_rgm_value_valid(value, B_TRUE)) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
				    param->p_name);
				return (res);
			}
		}
		res.err_code = SCHA_ERR_NOERR;
		return (res);

	}

}


//
// validate if the given string array is valid in paramtable
//
static scha_errmsg_t
validate_prop_strarray(const namelist_t *values, rgm_param_t *param)
{
	uint_t len;
	uint_t count = 0;
	namelist_t *nl;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (param == NULL) {
		// should not happen
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	// Following check enables the validation to check
	// either the default value or the value defined by
	// user at resource creation.

	// When no value is set OR the value is the empty list,
	// if ARRAYMIN or MIN is not 0, return error
	if (values == NULL ||
	    values->nl_name == NULL || values->nl_name[0] == '\0') {
		if (param->p_arraymin_isset && param->p_arraymin >= 1) {
			ucmm_print("validate_prop_strarray()",
			    NOGET("ERROR: not allow empty list when "
			    "the array_minsize is <%d>\n"),
			    param->p_arraymin);
			res.err_code = SCHA_ERR_PSARRAY;
			rgm_format_errmsg(&res, REM_PROP_EMPTYLIST,
			    param->p_name, SCHA_ARRAY_MINSIZE);
			return (res);
		}

		// default value is the empty list and MIN is set to greater
		// than 0, return error
		if (param->p_min_isset && param->p_min >= 1) {
			ucmm_print("validate_prop_strarray()",
			    NOGET("ERROR: the string length of"
			    " property value <%s> should be greater"
			    " than or equal to <%d>\n"), "", param->p_min);
			res.err_code = SCHA_ERR_PSARRAY;
			rgm_format_errmsg(&res, REM_PROP_EMPTYLIST,
			    param->p_name, SCHA_MINLENGTH);
			return (res);
		}

		// Pass the basic checking when value is the empty list.
		// Return with no error.
		return (res);
	}

	// check if the number of element in the array is correct
	// values is set and it is not empty
	// check if the string length of each array element is correct
	// if bounds are set.
	if (param->p_min_isset == B_TRUE || param->p_max_isset == B_TRUE) {
		for (nl = (namelist_t *)values; nl != NULL && nl->nl_name;
		    nl = nl->nl_next) {

			len = (uint_t)strlen(nl->nl_name);
			if (param->p_min_isset && len < (uint_t)param->p_min) {
				ucmm_print("validate_prop_strarray()",
				    NOGET("ERROR: the string length of"
				    " property value <%s> should be greater"
				    " than or equal to <%d>\n"),
				    nl->nl_name, param->p_min);
				res.err_code = SCHA_ERR_PSARRAY;
				rgm_format_errmsg(&res, REM_PROP_PSTR_MINLEN,
				    param->p_name);
				return (res);
			}

			if (param->p_max_isset && len > (uint_t)param->p_max) {
				ucmm_print("validate_prop_strarray()",
				    NOGET("ERROR: the string length"
				    " of property value <%s> should be"
				    " smaller than or equal to <%d>\n"),
				    nl->nl_name, param->p_max);
				res.err_code = SCHA_ERR_PSARRAY;
				rgm_format_errmsg(&res, REM_PROP_PSTR_MAXLEN,
				    param->p_name);
				return (res);
			}
			//
			// validate property value
			// 'FALSE' as the 2nd argument to 'is_rgm_value_valid'
			// call indicates comma is not a valid character
			//
			if (!is_rgm_value_valid(nl->nl_name, B_FALSE)) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    REM_INVALID_PROP_VALUES,
				    param->p_name);
				return (res);
			}

			count++;
		}
	}

	// check if the number of element in the array is correct
	if (param->p_arraymin_isset && count < (uint_t)param->p_arraymin) {
		ucmm_print("validate_prop_strarray()",
		    NOGET("ERROR: the number of array elements"
		    " <%d> should be greater than or equal to <%d>\n"),
		    count, param->p_arraymin);
		res.err_code = SCHA_ERR_PSARRAY;
		rgm_format_errmsg(&res, REM_PROP_PSARRAY_MINSIZE,
		    param->p_name);
		return (res);
	}

	if (param->p_arraymax_isset && count > (uint_t)param->p_arraymax) {
		ucmm_print("validate_prop_strarray()",
		    NOGET("ERROR: the number of array elements <%d>"
		    " should be smaller than or equal to <%d>\n"),
		    count, param->p_arraymax);
		res.err_code = SCHA_ERR_PSARRAY;
		rgm_format_errmsg(&res, REM_PROP_PSARRAY_MAXSIZE,
		    param->p_name);
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}


//
// validate if the given enum value is valid in paramtable
//
static scha_errmsg_t
validate_prop_enum(std::map<rgm::lni_t, char *> &val, rgm_param_t *param,
    LogicalNodeset *ns)
{
	namelist_t *enumlist = param->p_enumlist;
	namelist_t *ptr = NULL;
	char *value = NULL;
	uint_t nodeid;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (param == NULL) {
		// should not happen
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if (!param->p_per_node) {
		value = (char *)val[0];

		// cannot set the ENUM type property to NULL value.
		if (value == NULL) {
			ucmm_print("validate_prop_enum()",
			    NOGET("ERROR: cannot have NULL "
			    "value for property <%s>\n"),
			    param->p_name);
			res.err_code = SCHA_ERR_PENUM;
			rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
			    param->p_name);
			return (res);
		}
		//
		// although failover mode is special cased, and no longer uses
		// the enum model, however the enumlist should still contain
		// the correct list of possible values
		//
		if (strcmp(param->p_name, SCHA_FAILOVER_MODE) == 0) {
			if ((strcmp(value, SCHA_NONE) == 0) ||
			    (strcmp(value, SCHA_HARD) == 0) ||
			    (strcmp(value, SCHA_SOFT) == 0) ||
			    (strcmp(value, SCHA_RESTART_ONLY) == 0) ||
			    (strcmp(value, SCHA_LOG_ONLY) == 0)) {
				res.err_code = SCHA_ERR_NOERR;
				return (res);
			} else {
				goto inval_enum_prop_val; //lint !e801
			}
		}
		while (enumlist) {
			ptr = enumlist->nl_next;
			if (strcmp(value, enumlist->nl_name) == 0) {
				res.err_code = SCHA_ERR_NOERR;
				return (res);
			}
			enumlist = ptr;
		}
	} else {

		for (nodeid = ns->nextLniSet(1); nodeid != 0;
		    nodeid = ns->nextLniSet(nodeid + 1)) {

			value = (char *)val[nodeid];

			// cannot set the ENUM type property to NULL value.
			if (value == NULL) {
				ucmm_print("validate_prop_enum()",
				    NOGET("ERROR: cannot have NULL value "
				    "for property <%s>\n"),
				    param->p_name);
				res.err_code = SCHA_ERR_PENUM;
				rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
				    param->p_name);
				return (res);
			}

			// failover mode can't be per node property
			CL_PANIC(strcmp(param->p_name,
			    SCHA_FAILOVER_MODE) != 0);

			while (enumlist) {
				ptr = enumlist->nl_next;
				if (strcmp(value, enumlist->nl_name) == 0) {
					res.err_code = SCHA_ERR_NOERR;
					return (res);
				}
				enumlist = ptr;
			}
		}
	}


inval_enum_prop_val:
	ucmm_print("validate_prop_enum()",
	    NOGET("ERROR: invalid enum value - <%s>\n"), value);
	res.err_code = SCHA_ERR_PENUM;
	rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
	    param->p_name);
	return (res);
}


//
//  validate resource property based on the definition in paramtable
//
static scha_errmsg_t
validate_rs_prop(const rgm_property_t *p,
    rgm_param_t *param, LogicalNodeset *ns)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	switch (param->p_type) {
	case SCHA_PTYPE_STRING:
		return (validate_prop_string(((rgm_value_t *)p->rp_value)
		    ->rp_value, param, ns));

	case SCHA_PTYPE_INT:
	case SCHA_PTYPE_UINT:
		return (validate_prop_int(((rgm_value_t *)p->rp_value)
		    ->rp_value, param, ns));

	case SCHA_PTYPE_BOOLEAN:
		return (validate_prop_bool(((rgm_value_t *)p->rp_value)
		    ->rp_value, param, ns));

	case SCHA_PTYPE_ENUM:
		return (validate_prop_enum(((rgm_value_t *)p->rp_value)
		    ->rp_value, param, ns));

	case SCHA_PTYPE_STRINGARRAY:
	case SCHA_PTYPE_UINTARRAY:
		return (validate_prop_strarray(p->rp_array_values, param));

	default:
		res.err_code = SCHA_ERR_INTERNAL;
		return (res);
	}
}

//
// check_dup_dep():
// Checks if the dependency list has any duplicate name
//
static scha_errmsg_t
check_dup_dep(rdeplist_t *dep_list, char *rname, char *dep_flavor)
{
	const char 	*res_dup_name;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	// Get the duplicate name, if any, by calling the helper function
	res_dup_name = get_dup_rdepname(dep_list);

	if (res_dup_name != NULL) {
		res.err_code = SCHA_ERR_INVAL;
		ucmm_print("check_dup_dep()",
		    NOGET("R <%s> has duplicate resources in %s list"),
		    rname, dep_flavor);
		rgm_format_errmsg(&res, REM_DUP_RS_DEP, rname,
		    res_dup_name, dep_flavor);
	}

	return (res);
}

//
// For each dependee in the dependency list, check:
// - the dependee is a valid resource
// intra RG dependency of ANY_NODE are not specified.
// In case of inter cluster dependencies the validity of zone
// cluster is cheked.
//
static scha_errmsg_t
sanity_checks_dep_list(rdeplist_t *dep_list, rgm_resource_t *resource,
    boolean_t *dep_is_empty)
{
	rlist_t		*r_deps;
	rglist_t	*rg_ptr;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	// skip the checking if the deps_list is a empty list
	if (dep_list == NULL ||
	    (dep_list->rl_name == NULL || dep_list->rl_name[0] == '\0') &&
	    dep_list->rl_next == NULL) {
		*dep_is_empty = B_TRUE;
		return (res);
	}

	*dep_is_empty = B_FALSE;


	CL_PANIC((rg_ptr = rgname_to_rg(resource->r_rgname)) != NULL);
	for (rdeplist_t *deps = dep_list; deps != NULL; deps = deps->rl_next) {

		//
		// check if the dependency is specified on remote
		// cluster resource.
		//
		if (!is_enhanced_naming_format_used(deps->rl_name)) {
			// All the Rs in the r_dependencies list must
			// be valid Rs
			if ((r_deps = rname_to_r(NULL,
			    deps->rl_name)) == NULL) {
				ucmm_print("sanity_checks_dep_list()",
				    NOGET("R <%s> has dependency on R <%s>"
				    "which does not exist\n"),
				    resource->r_name, deps->rl_name);
				res.err_code = SCHA_ERR_RSRC;
				rgm_format_errmsg(&res, REM_INVALID_RS,
				    deps->rl_name);
				return (res);
			}
			//
			// if R is in the same RG, then locality_type
			// shouldn't be ANY_NODE
			//
			if (rg_ptr == r_deps->rl_rg &&
			    deps->locality_type == ANY_NODE) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_INTRARG_ANYNODE,
				    resource->r_name, deps->rl_name);
				return (res);
			}
		} else {
#if (SOL_VERSION >= __s10)
			if (!allow_inter_cluster()) {
				ucmm_print("sanity_checks_dep_list()",
				    NOGET("R <%s> has dependency on R <%s>"
				    "which does not belong to this local "
				    "cluster\n"),
				    resource->r_name, deps->rl_name);
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_RGM_IC_NOT_ALLOWED);
				return (res);
			}
			if (is_this_local_cluster_r_or_rg(deps->rl_name,
			    B_FALSE)) {
				res.err_code = SCHA_ERR_DEPEND;
				rgm_format_errmsg(&res,
				    REM_UPDATE_ICDEP_LOCAL_CLUSTER,
				    deps->rl_name);
				return (res);
			} else {
				// remote resource.
				char *rc = NULL, *rr = NULL;
				uint32_t clid;
				int err;
				res = get_remote_cluster_and_rorrg_name(
				    deps->rl_name,  &rc, &rr);
				if ((res.err_code == SCHA_ERR_NOERR) &&
				    (rc != NULL) && (rr != NULL)) {
					err = clconf_get_cluster_id(rc, &clid);
					if (((err == 0) && (clid != 0) &&
					    (clid < MIN_CLUSTER_ID)) ||
					    ((err != 0) && (err != EACCES))) {
						// Inside non-clusterized zone
						res.err_code = SCHA_ERR_CLUSTER;
						rgm_format_errmsg(&res,
						    REM_RGM_IC_UNREACH, rc);
						free(rr);
						free(rc);
						return (res);
					} else if (err == EACCES) {
						ASSERT(0); // Cannot happen
					}
					if (rc == NULL) {
						res.err_code = SCHA_ERR_INVAL;
						free(rr);
					}
					if (rr == NULL) {
						rgm_format_errmsg(&res,
						    REM_INVALID_REMOTE_R);
						res.err_code = SCHA_ERR_INVAL;
						free(rc);

					}
				}
			}
#endif
		}
	}
	return (res);
}

#if (SOL_VERSION >= __s10)
//
// checks whether the inter cluster dependency is of type local node or
// any node.
//
static scha_errmsg_t
check_ic_dep_local(rlist_p_t rl, char *src_c_name, char *src_rs_name,
    deptype_t deps_kind, boolean_t *is_ic_local_dep)
{
	rdeplist_t *nlp;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	char *temp_r_name = (char *)malloc(strlen(src_c_name) + strlen(
	    src_rs_name) + 2);
	if (temp_r_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	(void) sprintf(temp_r_name, "%s:%s", src_c_name, src_rs_name);

	switch (deps_kind) {
		case DEPS_STRONG:
			nlp = rl->rl_ccrdata->r_dependencies.dp_strong;
			break;
		case DEPS_WEAK:
			nlp = rl->rl_ccrdata->r_dependencies.dp_weak;
			break;
		case DEPS_RESTART:
			nlp = rl->rl_ccrdata->r_dependencies.dp_restart;
			break;
		case DEPS_OFFLINE_RESTART:
			nlp = rl->rl_ccrdata->r_dependencies.dp_offline_restart;
			break;

	}

	for (; nlp != NULL; nlp = nlp->rl_next) {
		// Check if it is a remote cluster resource.
		if (is_enhanced_naming_format_used(nlp->rl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "check_ic_dep_local "
				    "Running at too low a version:R <%s> "
				    "has dependency on R <%s> "
				    "which is in not in local cluster\n"),
				    rl->rl_ccrdata->r_name, nlp->rl_name);
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_RGM_IC_NOT_ALLOWED);
				return (res);
			}
			if (strcmp(nlp->rl_name, temp_r_name) == 0)
				if (nlp->locality_type == LOCAL_NODE) {
					*is_ic_local_dep = B_TRUE;
					free(temp_r_name);
					return (res);
				}
		}
	}
	free(temp_r_name);
	*is_ic_local_dep = B_FALSE;
	return (res);
}

//
// This function is used to update inter-cluster resource dependencies.
// This is called by idl_scrgadm_rs_update_props
//
static scha_errmsg_t
update_icrd(rgm_dependencies_t dep_list, rgm_resource_t *resource,
    deptype_t dep)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	namelist_t *orig_list = NULL, *new_list = NULL;
	ucmm_print("update_icrd()",
	    NOGET("Entered update_icrd for R <%s>\n"),
	    resource->r_name);

	switch (dep) {
		case DEPS_STRONG :
			// Get the new list.
			res = get_intercluster_rdep_lists(dep_list,
			    &new_list, NULL, NULL, NULL);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;
			// get the original list.
			res = get_intercluster_rdep_lists(
			    resource->r_dependencies, &orig_list, NULL, NULL,
			    NULL);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;
			// Check if the resoure list has changed and process
			// SET/UNSET accordingly
			res = process_intercluster_resource_lists(&orig_list,
			    &new_list, resource, dep);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;
			break;
	case DEPS_WEAK :
			res = get_intercluster_rdep_lists(dep_list, NULL,
			    &new_list, NULL, NULL);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;
			res = get_intercluster_rdep_lists(
			    resource->r_dependencies, NULL, &orig_list,
			    NULL, NULL);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;
			res = process_intercluster_resource_lists(&orig_list,
			    &new_list, resource, dep);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;
			break;

	case DEPS_RESTART:
			res = get_intercluster_rdep_lists(dep_list, NULL,
			    NULL, &new_list, NULL);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;
			res = get_intercluster_rdep_lists(
			    resource->r_dependencies, NULL, NULL, &orig_list,
			    NULL);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;
			res = process_intercluster_resource_lists(&orig_list,
			    &new_list, resource, dep);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;

			break;


	case DEPS_OFFLINE_RESTART:
			res = get_intercluster_rdep_lists(dep_list, NULL,
			    NULL, NULL, &new_list);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;
			res = get_intercluster_rdep_lists(
			    resource->r_dependencies, NULL, NULL, NULL,
			    &orig_list);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;
			res = process_intercluster_resource_lists(&orig_list,
			    &new_list, resource, dep);
			if (res.err_code != SCHA_ERR_NOERR)
				goto update_icrd_end;

		}

update_icrd_end :
	ucmm_print("update_icrd()",
	    NOGET("Returning from update_icrd for R <%s>\n"),
	    resource->r_name);
	rgm_free_nlist(new_list);
	rgm_free_nlist(orig_list);
	return (res);
}

//
// This function is used to process the inter-cluster dependency updates.
// If the original list and updated list is same then do nothing.
// If a resource is in original list and not present in the update list
// then that resource should be removed form the dependency.
// If a resource is not in original list and present in the update list
// then that resource should be added as a dependency.
//

scha_errmsg_t
process_intercluster_resource_lists(namelist_t **orig_list,
    namelist_t **new_list, rgm_resource_t *resource, deptype_t dep)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	ucmm_print("process_intercluster_resource_lists()",
	    NOGET("process list for R <%s>\n"),
	    resource->r_name);

	// Chek if both lists are Non-Null
	if ((*orig_list != NULL) && (*new_list != NULL)) {
		// Copy the original list.
		namelist_t *temp_list = namelist_copy(*orig_list);
		for (; temp_list != NULL; temp_list =
		    (temp_list)->nl_next) {
			if (namelist_contains(*new_list,
			    temp_list->nl_name)) {
				namelist_delete_name(new_list,
				    temp_list->nl_name);
				namelist_delete_name(orig_list,
				    temp_list->nl_name);
			}
		}

		// Both list are the same. Return.
		if ((*orig_list == NULL) && (*new_list == NULL)) {
			ucmm_print("process_intercluster_resource_lists()",
			    NOGET("Lists identical for R <%s>\n"),
			    resource->r_name);
			rgm_free_nlist(temp_list);
			return (res);
		}

		// If the newlist list has resources then these should be
		// SET in the dependents list of the dependee R.
		if (*new_list != NULL) {
			// Call with set
			for (; *new_list != NULL; *new_list =
			    (*new_list)->nl_next) {
				res = update_dependent_info(
				    (*new_list)->nl_name, B_TRUE,
				    resource->r_name, dep);
				if (res.err_code != SCHA_ERR_NOERR) {
					rgm_free_nlist(temp_list);
					return (res);
				}
			}
		}

		// If the original list has resources then these should be
		// UNSET from the dependents list of the dependee R.
		if (*orig_list != NULL) {
			// Call with unset
			for (; *orig_list != NULL; *orig_list =
			    (*orig_list)->nl_next) {
				res = update_dependent_info(
				    (*orig_list)->nl_name, B_FALSE,
				    resource->r_name, dep);
				if (res.err_code != SCHA_ERR_NOERR) {
					rgm_free_nlist(temp_list);
					return (res);
				}
			}
		}

		// If the original list was Null then the Rs in new_list
		// need to be set in the dependee R.
	} else if ((*orig_list == NULL) && (*new_list != NULL)) {
		// Call with set
		for (; *new_list != NULL; *new_list =
		    (*new_list)->nl_next) {
			res = update_dependent_info(
			    (*new_list)->nl_name, B_TRUE,
			    resource->r_name, dep);
			if (res.err_code != SCHA_ERR_NOERR)
				return (res);
		}
	} else {
		// If the new list was Null then the Rs in
		// original list needs to be UNSET in the dependee R.
		// call with unset.
		for (; *orig_list != NULL; *orig_list =
		    (*orig_list)->nl_next) {
			res = update_dependent_info(
			    (*orig_list)->nl_name, B_FALSE,
			    resource->r_name, dep);
			if (res.err_code != SCHA_ERR_NOERR)
				return (res);
			}
	}

	ucmm_print("process_intercluster_resource_lists()",
	    NOGET("process list complete for R <%s>\n"),
	    resource->r_name);
	return (res);
}


// This function is used to update the dependency information for a resource
// dependee. If the dep_set_flag is SET then the dependencies are set, if the
// dep_set_flag is UNSET then the dependencies are reset.
// This is called during resource update or delete operation if the resource
// has inter-cluster dependencies.
scha_errmsg_t
update_dependent_info(char *r_name, boolean_t dep_set_flag,
    char *local_rname, deptype_t dep)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	ucmm_print("update_dependent_info()",
	    NOGET("process for R <%s>\n"),
	    local_rname);
	res = configure_icrd_helper(r_name, dep, local_rname, dep_set_flag);
	return (res);
}

//
// Helper function for configuring inter-cluster dependencies and is called
// during resource creation  and resource update. This function makes
// an idl call to the remote cluster rgmd to update the dependent information
// of the depended on resource
//
static scha_errmsg_t
configure_icrd_helper(name_t remote_r_name,
    deptype_t dep, name_t local_r_name, boolean_t dep_set_flag)
{

	rlist_t		*r_deps;
	rglist_t	*rg_ptr;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	uint32_t clid;
	int err;
	namelist_t *r_names;
	rgm::idl_rs_update_prop_args		arg;
	rgm::idl_regis_result_t_var		result_v;
	rgm::rgm_comm_var prgm_pres_v;
	rgm::idl_nameval_seq_t	prop_val;
	char *cur_cluster_name = NULL;
	char *enhanced_dep_name = NULL;
	Environment e;

	ucmm_print("configure_icrd_helper()",
	    NOGET("process for R <%s>\n"),
	    local_r_name);

	//
	// Check if its local cluster resource but specified
	// as cluster:resource by the user.
	// If so do not process.
	//
	if (is_this_local_cluster_r_or_rg(remote_r_name, B_FALSE)) {
		res.err_code = SCHA_ERR_DEPEND;
		rgm_format_errmsg(&res,
		    REM_CREATE_ICDEP_LOCAL_CLUSTER,
		    remote_r_name, local_r_name);
		return (res);
		// continue;
	} else  {
		// remote resource.
		char *rc = NULL, *rr = NULL;
		res = get_remote_cluster_and_rorrg_name(
		    remote_r_name,  &rc, &rr);
		if ((res.err_code == SCHA_ERR_NOERR) &&
		    (rc != NULL) && (rr != NULL)) {
			err = clconf_get_cluster_id(rc, &clid);
			if (((err == 0) && (clid != 0) && (clid <
			    MIN_CLUSTER_ID)) || (
			    (err != 0) && (err != EACCES))) {
				// Inside non-clusterized zone
				res.err_code = SCHA_ERR_CLUSTER;
				rgm_format_errmsg(&res, REM_RGM_IC_UNREACH, rc);
				free(rr);
				free(rc);
				return (res);
			} else if (err == EACCES) {
				ASSERT(0); // Cannot happen
			}			// Set the msg locale
			arg.locale =
			    new_str(fetch_msg_locale());

			// Set the option flags
			arg.flags = 0;
			arg.flags |= rgm::SUPPRESS_VALIDATION;
			if (dep_set_flag)
				arg.flags |= rgm::SET_INTER_CLUSTER_DEP;
			else
				arg.flags |= rgm::UNSET_INTER_CLUSTER_DEP;
			arg.idlstr_rs_name = new_str(rr);

			// set the length of the sequence
			prop_val.length(1);

			// copy the name of the property
			switch (dep) {
				case DEPS_STRONG :
					prop_val[0].idlstr_nv_name =
// suppress cstyle warning about line > 80 characters.
// CSTYLED
					    new_str(SCHA_IC_RESOURCE_DEPENDENTS);
					break;

				case DEPS_WEAK :
					prop_val[0].idlstr_nv_name =
// suppress cstyle warning about line > 80 characters.
// CSTYLED
					new_str(SCHA_IC_RESOURCE_DEPENDENTS_WEAK);
					break;

				case DEPS_RESTART :
					prop_val[0].idlstr_nv_name =
// suppress cstyle warning about line > 80 characters.
// CSTYLED
					new_str(SCHA_IC_RESOURCE_DEPENDENTS_RESTART);
				break;

				case DEPS_OFFLINE_RESTART :
					prop_val[0].idlstr_nv_name =
					new_str(
// suppress cstyle warning about line > 80 characters.
// CSTYLED
					SCHA_IC_RESOURCE_DEPENDENTS_OFFLINE_RESTART);
				}

			// get the local cluster name.
			cur_cluster_name = strdup(ZONE);
			if (cur_cluster_name == NULL) {
				free(rr);
				free(rc);
				res.err_code = SCHA_ERR_NOMEM;
				return (res);
			}

			// local resource name in cluster:resource format.
			enhanced_dep_name = (char *)malloc(
			    strlen(cur_cluster_name) +
			    strlen(local_r_name) + 2);

			if (enhanced_dep_name == NULL) {
				free(cur_cluster_name);
				free(rr);
				free(rc);
				res.err_code = SCHA_ERR_NOMEM;
				return (res);
			}
			sprintf(enhanced_dep_name, "%s:%s",
			    cur_cluster_name,
			    local_r_name);

			// Fill in the values for the properties.
			prop_val[0].nv_val.length(1);
			prop_val[0].nv_val[0] =
			    new_str(enhanced_dep_name);
			arg.prop_val = prop_val;

			// Make an idl call to the remote cluster president
			// rgmd.
			prgm_pres_v = rgm_comm_getpres_zc(rc);

			if (CORBA::is_nil(prgm_pres_v)) {
				res.err_code =
				    SCHA_ERR_CLRECONF;
				rgm_format_errmsg(&res,
				    REM_RGM_IC_UNREACH, rc);
				free(rr);
				free(rc);
				free(cur_cluster_name);
				free(enhanced_dep_name);
				return (res);
			}
			prgm_pres_v->idl_scrgadm_rs_update_prop(arg,
			    result_v, e);
			if ((res = except_to_scha_err(e)).
			    err_code != SCHA_ERR_NOERR) {
				res.err_code = SCHA_ERR_DEPEND;
				rgm_format_errmsg(&res,
				REM_RGM_CREATE_DEP_INFO_FAILED,
				    enhanced_dep_name,
				    local_r_name);
				free(rr);
				free(rc);
				free(cur_cluster_name);
				free(enhanced_dep_name);
				return (res);
			}
			res.err_code =
			    (scha_err_t)result_v->ret_code;
			if ((const char *)
			    result_v->idlstr_err_msg)
				res.err_msg =
				strdup(
				result_v->idlstr_err_msg);
			free(rr);
			free(rc);
			free(cur_cluster_name);
			free(enhanced_dep_name);
			return (res);
			}
		if (rc == NULL) {
			res.err_code = SCHA_ERR_INVAL;
			free(rr);
			return (res);
		}

		if (rr == NULL) {
			res.err_code = SCHA_ERR_INVAL;
			free(rc);
			return (res);
		}
	}
}

// This function is used to set inter-cluster dependencies and is called
// during resource creation only.
static scha_errmsg_t
configure_icrd(rdeplist_t *dep_list, rgm_resource_t *resource, deptype_t dep)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rglist_t	*rg_ptr;

	CL_PANIC((rg_ptr = rgname_to_rg(resource->r_rgname)) != NULL);
	for (rdeplist_t *deps = dep_list; deps != NULL; deps = deps->rl_next) {
		// check if the dependency is specified on remote cluster
		// resource.
		if (!is_enhanced_naming_format_used(deps->rl_name)) {
			continue;
		} else {
			res = configure_icrd_helper(deps->rl_name, dep,
			    resource->r_name, B_TRUE);
		}
	}
	return (res);
}

static scha_errmsg_t
cyclic_depchk_set_nameserver()
{
	char deptag[DEPTAG_LEN];
	scha_errmsg_t scha_error = {SCHA_ERR_NOERR, NULL };
	int r_val = 0;

	(void) memset(deptag, 0, DEPTAG_LEN);
	(void) sprintf(deptag, DEPTAG);

	r_val = cl_bind(deptag, ZONE);

	if (r_val != 0) {
		scha_error.err_code = SCHA_ERR_INTERNAL;
		rgm_format_errmsg(&scha_error, REM_RG_NAMESERVER,
		    ZONE);
	}
	return (scha_error);

}

void
cyclic_depchk_clear_nameserver()
{
	char deptag[DEPTAG_LEN];

	(void) memset(deptag, 0, DEPTAG_LEN);
	(void) sprintf(deptag, DEPTAG);
	(void) cl_unbind(deptag);
}

// queries name server for the given tag
boolean_t
dep_tag_exists_in_nameserver()
{

	char *deptag = NULL;
	int cl_err;
	cl_err = cl_resolve(DEPTAG, &deptag);

	if (cl_err != 0)
		return (B_FALSE);

	if (deptag == NULL) {
		return (B_FALSE);
	}

	free(deptag);
	return (B_TRUE);
}

// queries name server for the given tag
boolean_t
dep_tag_created_by_this_zc()
{

	char *deptag = NULL;
	int cl_err;
	cl_err = cl_resolve(DEPTAG, &deptag);

	if (cl_err != 0)
		return (B_FALSE);

	if (deptag == NULL) {
		return (B_FALSE);
	}

	if (strcmp(deptag, ZONE) != 0) {
		free(deptag);
		return (B_FALSE);
	}
	free(deptag);
	return (B_TRUE);
}

//
// This function checks if the inter cluster dependnet resources
// are valid. This is called during the resource delete operation.
//
static boolean_t
any_icr_dependents_for_this_r(rlist_p_t r_ptr)
{
	rlist_p_t	rp;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	LogicalNodeset emptynodeset;
	rdeplist_t *depslist = NULL;
	boolean_t is_invalid_rs = B_FALSE;

	depslist = r_ptr->rl_ccrdata->r_inter_cluster_dependents.dp_strong;

	for (rdeplist_t *deps = depslist; deps != NULL; deps = deps->rl_next) {
		(void) get_remote_r_state_helper(r_ptr, deps->rl_name,
		    deps->locality_type, DEPS_STRONG, emptynodeset, B_TRUE,
		    &is_invalid_rs);
		if (!is_invalid_rs)
			return (B_TRUE);
	}

	depslist = r_ptr->rl_ccrdata->r_inter_cluster_dependents.dp_weak;
	for (rdeplist_t *deps = depslist; deps != NULL; deps = deps->rl_next) {
		(void) get_remote_r_state_helper(r_ptr, deps->rl_name,
		    deps->locality_type, DEPS_WEAK, emptynodeset, B_TRUE,
		    &is_invalid_rs);
		if (!is_invalid_rs)
			return (B_TRUE);
	}

	depslist = r_ptr->rl_ccrdata->r_inter_cluster_dependents.dp_restart;
	for (rdeplist_t *deps = depslist; deps != NULL; deps = deps->rl_next) {
		(void) get_remote_r_state_helper(r_ptr, deps->rl_name,
		    deps->locality_type, DEPS_RESTART, emptynodeset, B_TRUE,
		    &is_invalid_rs);
		if (!is_invalid_rs)
			return (B_TRUE);
	}

	depslist = r_ptr->rl_ccrdata->
	    r_inter_cluster_dependents.dp_offline_restart;
	for (rdeplist_t *deps = depslist; deps != NULL; deps = deps->rl_next) {
		(void) get_remote_r_state_helper(r_ptr, deps->rl_name,
		    deps->locality_type, DEPS_OFFLINE_RESTART, emptynodeset,
		    B_TRUE, &is_invalid_rs);
		if (!is_invalid_rs)
			return (B_TRUE);
	}
	return (B_FALSE);
}


//
// This function is used to remove the stale inter cluster resource dependent
// resources if any. The rgm state lock should have been held before this
// function is called.
//
void
cleanup_dep_info(const char *rs_name, const char *dep_aff_name,
    deptype_t deps, rgm::idl_nameval_seq_t  prop_val)
{
	char			*rg_name = NULL;
	rgm_resource_t 		*rs_conf = NULL;
	rglist_p_t 		rgp = NULL;
	rlist_p_t 		rp = NULL;
	rgm_rt_t 		*rt = NULL;
	scha_errmsg_t 		res = {SCHA_ERR_NOERR, NULL};
	LogicalNodeset 		emptyNodeset;
	rdeplist_t *test = NULL;
	boolean_t stale_ic_dep_cleaned_up = B_FALSE;


	if ((rp = rname_to_r(NULL, rs_name)) == NULL) {
		ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("Resource <%s> does not exist!!\n"), rs_name);
		goto rs_cleanup_end; //lint !e801
	}

	ucmm_print("cleanup_dep_r()",
	    NOGET("RS <%s>\n"), rs_name);

	rgp = rp->rl_rg;
	rg_name = rgp->rgl_ccr->rg_name;

	if ((res = rgmcnfg_get_resource_no_defaults((char *)rs_name, rg_name,
	    &rs_conf, ZONE)).err_code != SCHA_ERR_NOERR)
		goto rs_cleanup_end; //lint !e801

	update_pn_default_value(rs_conf->r_ext_properties,
	    rgp->rgl_ccr->rg_nodelist);

	update_onoff_monitored_switch(rs_conf, rgp->rgl_ccr->rg_nodelist);

	switch (deps) {
	case DEPS_WEAK :
		test = get_value_string_array_dep(
		    SCHA_IC_RESOURCE_DEPENDENTS_WEAK, prop_val, res);
		if (res.err_code != SCHA_ERR_NOERR) {
			goto rs_cleanup_end; //lint !e801
		}

		if (test) {
			if (rdeplist_contains(
			    rs_conf->r_inter_cluster_dependents.
			    dp_weak, test->rl_name)) {
				rdeplist_delete_name(
				    &rs_conf->
				    r_inter_cluster_dependents.dp_weak,
				    test->rl_name);
				stale_ic_dep_cleaned_up = B_TRUE;
			}
			rgm_free_rdeplist(test);
			test = NULL;
		}
		break;

	case DEPS_RESTART:
		test = get_value_string_array_dep(
		    SCHA_IC_RESOURCE_DEPENDENTS_RESTART, prop_val, res);
		if (res.err_code != SCHA_ERR_NOERR) {
			goto rs_cleanup_end; //lint !e801
		}

		if (test) {
			if (rdeplist_contains(
			    rs_conf->r_inter_cluster_dependents.
			    dp_restart, test->rl_name)) {
				rdeplist_delete_name(
				    &rs_conf->
				    r_inter_cluster_dependents.dp_restart,
				    test->rl_name);
				stale_ic_dep_cleaned_up = B_TRUE;
			}
			rgm_free_rdeplist(test);
			test = NULL;
		}
		break;

	case DEPS_OFFLINE_RESTART :
		test = get_value_string_array_dep(
		    SCHA_IC_RESOURCE_DEPENDENTS_OFFLINE_RESTART, prop_val,
		    res);

		if (res.err_code != SCHA_ERR_NOERR) {
			goto rs_cleanup_end; //lint !e801
		}
		if (test) {
			if (rdeplist_contains(
			    rs_conf->r_inter_cluster_dependents.
			    dp_offline_restart, test->rl_name)) {
				rdeplist_delete_name(
				    &rs_conf->
				    r_inter_cluster_dependents.
				    dp_offline_restart, test->rl_name);
				stale_ic_dep_cleaned_up = B_TRUE;
			}
			rgm_free_rdeplist(test);
			test = NULL;
		}
		break;

	case DEPS_STRONG:
		test = get_value_string_array_dep(SCHA_IC_RESOURCE_DEPENDENTS,
		    prop_val, res);
		if (res.err_code != SCHA_ERR_NOERR) {
			goto rs_cleanup_end; //lint !e801
		}
		if (test) {
			if (rdeplist_contains(
			    rs_conf->r_inter_cluster_dependents.
			    dp_strong, test->rl_name)) {
				rdeplist_delete_name(
				    &rs_conf->
				    r_inter_cluster_dependents.
				    dp_strong, test->rl_name);
				stale_ic_dep_cleaned_up = B_TRUE;
			}
			rgm_free_rdeplist(test);
			test = NULL;
		}
	}

	if (!stale_ic_dep_cleaned_up)
		// Nothing to do.
		goto rs_cleanup_end; //lint !e801

	// Latch the intention to update the resource properties

	ucmm_print("cleanup_dep_r()",
	    NOGET("RS <%s> Latching\n"), rs_name);
	(void) latch_intention(rgm::RGM_ENT_RS, rgm::RGM_OP_UPDATE,
	    rs_name, &emptyNodeset, 0);

	ucmm_print("cleanup_dep_r()",
	    NOGET("RS <%s> Calling CCR\n"), rs_name);

	//
	// Update the CCR with the new resource properties.
	//
	// Note that we save to CCR the version of the resource without
	// the RT defaults.
	//
	if ((res = rgmcnfg_update_resource(rs_conf,
	    (const char*)ZONE)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("cleanup_dep_r()",
		    NOGET("RS <%s> CCR Failed\n"), rs_name);
		(void) unlatch_intention();
		if (rgp->rgl_switching != SW_EVACUATING) {
			rgp->rgl_switching = SW_NONE;
		}
		goto rs_cleanup_end; //lint !e801
	}


	ucmm_print("cleanup_dep_r()",
	    NOGET("RS <%s> Processing\n"), rs_name);

	(void) process_intention();
	(void) unlatch_intention();

rs_cleanup_end:
	if (rs_conf)
		rgm_free_resource(rs_conf);
	free(res.err_msg);
}
#endif

//
// is_depended_on
//
// Checks for weak/strong/restart/implicit and inter cluster dependents
// of this R.
//
// Returns TRUE upon the first encounter of a dependent satisfying
// the checking criteria.
//
// Returns FALSE at the end if there are no dependents satisfying
// the checking criteria.
//
// Implicit checking applies only to RGs with rg_impl_net_depend flag
// set. We check whether R is loghost/sharedaddr type. If so, we check
// if non-loghost Rs are [enabled] and print a warning message.Note that
// we return FALSE to allow the dletion of a implicit network resource.
//
// If atleast one of the explicit dependency list is not empty, then
// return TRUE.
//
static boolean_t
is_depended_on(rlist_p_t r_ptr, scha_errmsg_t *res, scha_errmsg_t *warn_res)
{
	rglist_p_t	rgp;
	rlist_p_t	rp;
	LogicalNodeset	*ns;

	//
	// If RG doesn't have rg_impl_net_depend flag set, or if R is not
	// loghost/sharedaddr type R, we don't need to check implicit
	// dependency.
	//
	rgp = r_ptr->rl_rg;

	ns = get_logical_nodeset_from_nodelist(rgp->
	    rgl_ccr->rg_nodelist);

	if (rgp->rgl_ccr->rg_impl_net_depend &&
	    (r_ptr->rl_ccrtype->rt_sysdeftype == SYST_LOGICAL_HOSTNAME ||
	    r_ptr->rl_ccrtype->rt_sysdeftype == SYST_SHARED_ADDRESS)) {
		for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
			// Any non-network address R implicitly depends on r_ptr
			if (rp->rl_ccrtype->rt_sysdeftype !=
			    SYST_LOGICAL_HOSTNAME &&
			    rp->rl_ccrtype->rt_sysdeftype !=
			    SYST_SHARED_ADDRESS) {
				//
				// For the R which isn't loghost/sharedaddr
				// type in RG, check whether it is enabled
				// on atleast one node. If so, print a
				// warning message and return B_FALSE.
				//
				for (rgm::lni_t nodeid =
				    ns->nextLniSet(1); nodeid != 0;
				    nodeid = ns->nextLniSet(
				    nodeid + 1)) {
					if ((((rgm_switch_t *)
					    rp->rl_ccrdata->r_onoff_switch)
					    ->r_switch[nodeid] ==
					    SCHA_SWITCH_ENABLED)) {
						ucmm_print("RGM",
						    NOGET("Enabled R <%s> "
						    "depends on "
						    "loghost/sharedaddr "
						    "R <%s>\n"),
						    rp->rl_ccrdata->r_name,
						    r_ptr->rl_ccrdata->r_name);
						rgm_format_errmsg(warn_res,
						    RWM_RS_DEP_ONLINE,
						    rp->rl_ccrdata->r_name,
						    r_ptr->rl_ccrdata->r_name);
						return (B_FALSE);
					}
				}
			}
		}
	}

	if (r_ptr->rl_dependents.dp_restart != NULL ||
	    r_ptr->rl_dependents.dp_strong != NULL ||
	    r_ptr->rl_dependents.dp_weak != NULL ||
	    r_ptr->rl_dependents.dp_offline_restart != NULL) {
		ucmm_print("RGM",
		    NOGET("cannot delete R with explicit dependents\n"));
		res->err_code = SCHA_ERR_DEPEND;
		rgm_format_errmsg(res, REM_RS_DEL_DEP,
		    r_ptr->rl_ccrdata->r_name);
		return (B_TRUE);
	}

	return (B_FALSE);
}


//
// rt_allowed_restart
//
// Returns B_TRUE if the resources of this type are allowed to declare
// restart dependencies or respond to scha_control RESOURCE_RESTART
// according to the following rules:
//
// 1. proxy resource types can restart.
// 2. A resource with both START and STOP methods declared can restart.
// 3. A resource without either START or STOP but with both PRENET_START and
//    POSTNET_STOP can restart.
//
boolean_t
rt_allowed_restart(rgm_rt_t *rt)
{
	boolean_t is_start = is_method_reg(rt, METH_START, NULL);
	boolean_t is_stop = is_method_reg(rt, METH_STOP, NULL);
	boolean_t is_prenet_start = is_method_reg(rt, METH_PRENET_START, NULL);
	boolean_t is_postnet_stop = is_method_reg(rt, METH_POSTNET_STOP, NULL);

	return (boolean_t)(rt->rt_proxy ||
	    (is_start && is_stop) || (is_prenet_start && is_postnet_stop &&
		!is_start && !is_stop));
}

//
// rs_sanity_checks(): Checks  a proposed resource structure for various
// semantics and pre-conditions.
// Returns appropriate error code from scha_err.h on error, SCHA_ERR_NOERR
// if everything looks ok.
// As it traverses the internal state structures, it expects the caller
// to hold the rgm state lock.
// Notes: the project name is not validated here, since the president
// may not be one of the potential primaries of resource's containing RG.
// The checking is done by the slave nodes which is in launch_validate_method().
//
// The 'globalflag' parameter is set true if the command is being executed
// in the global zone; and false if in a non-global zone.  The 'zonename'
// parameter provides the name of the non-global zone.
//
static scha_errmsg_t
rs_sanity_checks(rgm_resource_t *resource, rgm_param_t **paramtable,
	boolean_t dep_updated, boolean_t globalflag, const char *zonename)
{
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};
	boolean_t		found = B_FALSE;
	rgm_property_list_t 	*p;
	rgm_param_t		**param;
	boolean_t		dep_restart_is_empty = B_FALSE;
	boolean_t		dep_strong_is_empty = B_FALSE;
	boolean_t		dep_weak_is_empty = B_FALSE;
	boolean_t		dep_off_restart_is_empty = B_FALSE;
	rgm_rt_t		*rt = NULL;
	rglist_p_t		rgp = NULL;
	rgm_dependencies_t	r_deps;
	LogicalNodeset		*ns;
	namelist_t *dependee_rgs = NULL;
	boolean_t		gz_override = B_TRUE;
	boolean_t		icrd_check_flag = B_FALSE;

	rt = rtname_to_rt(resource->r_type);
	CL_PANIC(rt != NULL);

	rgp = rgname_to_rg(resource->r_rgname);
	CL_PANIC(rgp != NULL);

	// We will not let R of Failover type RT to be added into
	// Scalable RG.
	if (rgp->rgl_ccr->rg_mode == RGMODE_SCALABLE &&
	    rt->rt_failover) {
		// rg_mode flag is inconsistent with rt_failover flag.
		// check fails.
		ucmm_print("rs_sanity_check()", NOGET(
		    "R <%s> 's type <%s> isn't match with Scalable mode "
		    "RG <%s>\n"), resource->r_name, resource->r_type,
		    resource->r_rgname);
		res.err_code = SCHA_ERR_PROP;
		rgm_format_errmsg(&res, REM_RG_R_MISMATCH,
		    resource->r_name, resource->r_rgname);
		return (res);
	}

	//
	// validate value of RS_DESCRIPTION
	// 'TRUE' as the 2nd argument to 'is_rgm_value_valid'
	// call indicates comma is a valid character
	//
	if (!is_rgm_value_valid(resource->r_description, B_TRUE)) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
		    SCHA_R_DESCRIPTION);
		return (res);
	}

	ns = get_logical_nodeset_from_nodelist(rgp->rgl_ccr->rg_nodelist);

	// validate pre-defined properties -- do generic type checking
	for (p = resource->r_properties; p; p = p->rpl_next) {

		if (p->rpl_property->rp_key == NULL)
			break;
		found = B_FALSE;
		for (param = paramtable; *param; param++) {
			if ((*param)->p_name == NULL)
				break;
			if ((*param)->p_extension)
				continue;
			if (strcasecmp(p->rpl_property->rp_key,
			    (*param)->p_name) == 0) {
				res = validate_rs_prop(p->rpl_property,
				    (*param), ns);
				if (res.err_code != SCHA_ERR_NOERR) {
					ucmm_print("rs_sanity_checks()", NOGET(
					    "ERROR: the value of system "
					    "defined property <%s> is "
					    "invalid\n"),
					    p->rpl_property->rp_key);
					return (res);
				}
				found = B_TRUE;

				//
				// Perform the necessary checks for the
				// Network_resources_used property.
				//
				if (strcasecmp(p->rpl_property->rp_key,
				    SCHA_NETWORK_RESOURCES_USED) == 0) {
					res = check_nw_res_used(rgp,
					    resource, p->rpl_property->
					    rp_array_values);
					if (res.err_code != SCHA_ERR_NOERR) {
						return (res);
					}
				}

				//
				// Capture the Global_zone_override property
				// if set on the command line; otherwise it
				// defaults to true.  It is applied only if
				// the RT declares Global_zone=TRUE.
				//
				if (strcasecmp(p->rpl_property->rp_key,
				    SCHA_GLOBAL_ZONE_OVERRIDE) == 0 &&
				    strcasecmp(SCHA_FALSE,
				    ((rgm_value_t *)p->rpl_property->
				    rp_value)->rp_value[0]) == 0) {
					gz_override = B_FALSE;
				}

				break;
			}
		}

		if (found)
			continue;
		else {
			ucmm_print("rs_sanity_checks()", NOGET(
			    "ERROR: Invalid system defined "
			    "property - <%s>\n"), p->rpl_property->rp_key);
			res.err_code = SCHA_ERR_PROP;
			rgm_format_errmsg(&res, REM_PROP_NOT_EXIST,
			    p->rpl_property->rp_key);
			return (res);
		}
	}

	//
	// If we are executing in a non-global zone but the resource has
	// Global_zone=TRUE, we do not permit the resource creation or
	// deletion.  For resource updates, the caller passes B_TRUE for
	// the globalflag parameter so that this check is bypassed.
	//
	if (!globalflag && rt->rt_globalzone && gz_override) {
		res.err_code = SCHA_ERR_ACCESS;
		rgm_format_errmsg(&res, REM_R_GLOBALFLAG_NGZONE,
		    resource->r_name, zonename);
		return (res);
	}

	// validate extension properties -- do generic type checking
	for (p = resource->r_ext_properties; p; p = p->rpl_next) {
		found = B_FALSE;
		for (param = paramtable; param && *param; param++) {
			if (!(*param)->p_extension)
				continue;

			if (strcasecmp(p->rpl_property->rp_key,
			    (*param)->p_name) == 0) {
				res = validate_rs_prop(p->rpl_property,
				    (*param), ns);
				if (res.err_code != SCHA_ERR_NOERR) {
					ucmm_print("rs_sanity_checks()", NOGET(
					    "ERROR: the value of extension "
					    "property <%s> is invalid\n"),
					    p->rpl_property->rp_key);
					return (res);
				}

				found = B_TRUE;
				break;
			}
		}

		if (found)
			continue;
		else {
			ucmm_print("rs_sanity_checks()",
			    NOGET("ERROR: Invalid extension "
			    "property - <%s>\n"), p->rpl_property->rp_key);
			res.err_code = SCHA_ERR_PROP;
			rgm_format_errmsg(&res, REM_PROP_NOT_EXIST,
			    p->rpl_property->rp_key);
			return (res);
		}
	}

	// Further sanity checks are made for the dependency properties.
	// We will not go further if no dependency properties were specified.
	if (!dep_updated) {
		return (res);
	}
	//
	// Dependency related checks are not performed if the property is
	// not one of the three resource dependencies or
	// network_resources_used.
	//
	// Check for duplicates in all resource dependency lists
	r_deps = resource->r_dependencies;

	// First check for duplicate dependencies
	if (((res = check_dup_dep(r_deps.dp_weak, resource->r_name,
	    SCHA_RESOURCE_DEPENDENCIES_WEAK)).err_code !=
	    SCHA_ERR_NOERR) || ((res = check_dup_dep(r_deps.dp_strong,
	    resource->r_name, SCHA_RESOURCE_DEPENDENCIES)).err_code !=
	    SCHA_ERR_NOERR) || ((res = check_dup_dep(r_deps.dp_restart,
	    resource->r_name,
	    SCHA_RESOURCE_DEPENDENCIES_RESTART)).err_code !=
	    SCHA_ERR_NOERR) || ((res =
	    check_dup_dep(r_deps.dp_offline_restart, resource->r_name,
	    SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART)).err_code !=
	    SCHA_ERR_NOERR)) {
		return (res);
	}

	// Now that we know we have no duplicates, do the individual
	// checks on each resource in the dependency lists

	if ((res = sanity_checks_dep_list(r_deps.dp_weak, resource,
	    &dep_weak_is_empty)).err_code != SCHA_ERR_NOERR ||
	    (res = sanity_checks_dep_list(r_deps.dp_strong, resource,
	    &dep_strong_is_empty)).err_code != SCHA_ERR_NOERR ||
	    (res = sanity_checks_dep_list(r_deps.dp_restart, resource,
	    &dep_restart_is_empty)).err_code != SCHA_ERR_NOERR ||
	    (res = sanity_checks_dep_list(r_deps.dp_offline_restart,
	    resource, &dep_off_restart_is_empty)).err_code !=
	    SCHA_ERR_NOERR) {
		return (res);
	}
	//
	// If it's a restart dependency verify that resources
	// of this type are allowed to declare it.
	//
	if ((!dep_restart_is_empty || !dep_off_restart_is_empty) &&
	    !rt_allowed_restart(rt)) {
		ucmm_print("rs_sanity_checks()",
		    NOGET("R <%s> "
		    "can't have restart dependencies\n"),
		    resource->r_name);
		res.err_code = SCHA_ERR_RT;
		rgm_format_errmsg(&res, REM_RS_RDEP_METHOD,
		    resource->r_name);
		return (res);
	}

	//
	// No resource name can appear in two or more dependencies
	// lists of a given resource. The three set of checks ensure
	// all possible combinations are covered.
	//
	for (rdeplist_t *weak_deps = resource->r_dependencies.dp_weak;
	    weak_deps != NULL; weak_deps = weak_deps->rl_next) {

		if (rdeplist_exists(resource->r_dependencies.dp_strong,
		    weak_deps->rl_name) ||
		    rdeplist_exists(resource->r_dependencies.dp_restart,
		    weak_deps->rl_name) ||
		    rdeplist_exists(
		    resource->r_dependencies.dp_offline_restart,
		    weak_deps->rl_name)) {
			ucmm_print("rs_sanity_checks()",
			    NOGET("R <%s> has weak dependency on R <%s>"
			    "which also exists in the restart strong or"
			    " offline restart dependencies\n"),
			    resource->r_name, weak_deps->rl_name);
			res.err_code = SCHA_ERR_DEPEND;
			rgm_format_errmsg(&res, REM_RS_DUP_DEP,
			    weak_deps->rl_name);
			return (res);
		}
	}

	for (rdeplist_t *strong_deps =
	    resource->r_dependencies.dp_strong; strong_deps != NULL;
	    strong_deps = strong_deps->rl_next) {

		if (rdeplist_exists(resource->r_dependencies.dp_restart,
		    strong_deps->rl_name) ||
		    rdeplist_exists(
		    resource->r_dependencies.dp_offline_restart,
		    strong_deps->rl_name)) {
			ucmm_print("rs_sanity_checks()",
			    NOGET("R <%s> has strong dependency on R "
			    "<%s> which also exists in the restart "
			    "dependencies\n"), resource->r_name,
			    strong_deps->rl_name);
			res.err_code = SCHA_ERR_DEPEND;
			rgm_format_errmsg(&res, REM_RS_DUP_DEP,
			    strong_deps->rl_name);
			return (res);
		}
	}

	for (rdeplist_t *restart_deps =
	    resource->r_dependencies.dp_restart; restart_deps != NULL;
	    restart_deps = restart_deps->rl_next) {

		if (rdeplist_exists(
		    resource->r_dependencies.dp_offline_restart,
		    restart_deps->rl_name)) {
			ucmm_print("rs_sanity_checks()",
			    NOGET("R <%s> has restart dependency "
			    "on R <%s> which also exists in the "
			    "offline-restart dependencies\n"),
			    resource->r_name,
			    restart_deps->rl_name);
			res.err_code = SCHA_ERR_DEPEND;
			rgm_format_errmsg(&res, REM_RS_DUP_DEP,
			    restart_deps->rl_name);
			return (res);
		}
	}

	// the new dependencies being
	// introduced by this R must not create a cycle.
	// Note that for the purpose of detecting a cycle we really
	// don't distinguish between strong, weak and restart
	// dependencies; we simply treat them as edges in the combined
	// dependencies graph. Also we have already checked for
	// duplicates between the restart the strong and the weak
	// dependencies list.
	// It is possible to set resource dependencies spanning
	// Hence, it becomes important that we dont allow resource-
	// dependency-cycles to span across clusters. We
	// need to check for inter-cluster-resource-dependency
	// (ICRD) cycles as well.
	// To detect ICRD cycles, we need to fetch dependency information
	// of the dependee resources from other clusters. This will require
	// making idl calls between rgmds of different clusters. There are
	// certain conditions under which ICRD cycles cannot take place. We
	// will try to handle such conditions first before making idl calls
	// to other rgmds. This way, we will be able to avoid some idl calls,
	// thereby achieving a better performance and also avoiding some
	// error handling. We will first do some basic checking for ICRD
	// cycles in does_r_link_to_inter_cluster_dependents(). This function
	// will return a "icrd_check_flag", which will indicate whether an
	// ICRD is possible or not. We will use this flag in
	// "detect_r_dependencies_cycles" to determine whether we should make
	// idl calls or not.
	// One important aspect of setting inter-cluster-resource dependencies
	// is cluster wide locking. In the case where there is no cluster wide
	// locking, it is possible that ICRD for multiples resources are being
	// set across clusters. It is possible in such a case that two
	// simultaneous updates are taking place, each update not causing an
	// ICRD cycle but the overall update resulting in an ICRD cycle. We need
	// to avoid such a situation. So, it is desirable to have a cluster-wide
	// lock, which will ensure that there is only one update to resource-
	// dependencies across clusters. This cluster-wide locking has been
	// implemented using nameserver tags. An rgmd daemon, which attempts to
	// update a resource dependency property, will first have to ensure
	// that the dependency tag has not been set in the nameserver. If a
	// dependency tag has been set in the nameserver, it means that there
	// is a simultaneous resource update happening elsewhere. In such a
	// case this rgmd should report an error about a simultaneuos update. If
	// the dependency tag does not exist in the nameserver, then this rgmd
	// can go ahead and set the tag in the nameserver, thus ensuring that no
	// other rgmd can sumltaneously update the resource dependency property
	// of any other resource. Once, the resource dependency property
	// verification or update is finished, this rgmd should clear the
	// dependency tag from the nameserver.

#if (SOL_VERSION >= __s10)
	if (allow_inter_cluster()) {
		icrd_check_flag = does_r_link_to_inter_cluster_dependents(
		    resource, resource);

		if (icrd_check_flag && dep_tag_exists_in_nameserver()) {
			res.err_code = SCHA_ERR_DEPEND;
			rgm_format_errmsg(&res, REM_RGM_IC_CYCLIC_DEP);
			return (res);
		}

		if (icrd_check_flag)
			cyclic_depchk_set_nameserver();
	}
#endif
	if ((res = detect_r_dependencies_cycles(resource,
	    dep_restart_is_empty, dep_off_restart_is_empty,
	    dep_strong_is_empty, dep_weak_is_empty,
	    B_FALSE, icrd_check_flag)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("rs_sanity_checks()",
		    NOGET("R <%s> has cyclic dependencies\n"),
		    resource->r_name);

#if (SOL_VERSION >= __s10)
		if (allow_inter_cluster() && icrd_check_flag)
			cyclic_depchk_clear_nameserver();
#endif
		return (res);
	}

#if (SOL_VERSION >= __s10)
	if (allow_inter_cluster() && icrd_check_flag)
		cyclic_depchk_clear_nameserver();
#endif
	if (is_r_scalable(resource)) {
		dependee_rgs = add_sa_dependee_rgs_to_list(&res,
		    rgp, resource);
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
		// the new dependencies being introduced should not
		// create a cycle.
		if ((res = detect_rg_dependencies_cycles(rgp->rgl_ccr,
		    &rgp->rgl_affinities, dependee_rgs)).err_code !=
		    SCHA_ERR_NOERR) {
			ucmm_print("rs_sanity_checks()",
			    NOGET("rg_sanity_checks(): newly "
			    "introduced dependencies creates a "
			    "cycle\n"));
			return (res);
		}
	}

	return (res);
}

#if (SOL_VERSION >= __s10)
//
// does_r_link_to_inter_cluster_dependents(): Checks whether there
// is any incoming inter-cluster-R dependency into the current
// cluster and whether "proposed_r" falls in that dependency line.
// This method returns "B_TRUE" if "proposed_r" falls in the line
// of such an inter-cluster-R dependency, else it returns "B_FALSE".
// Please note that this method goes back in the dependency graph, i.e;
// this method mainly looks at the dependents to check for such a
// dependency. This method is primarily used to determine whether we
// should check for inter-cluster-R dependencies cycles or not. If
// "proposed_r" does not fall in the line of an existing inter-cluster-R
// dependency, then there is no way an inter-cluster-R dependency cycle
// can be set. This is true even when an inter-cluster-R dependency is being
// set for this resource.
// One interesting item about this method is the number of parameters it
// takes. At the outset it might seem that one parameter, "resource", alone
// would suffice for its functionality. However, we have to note that this
// method would be called before "detect_r_dependencies_cycles" is called
// and that it is possible that a local resource dependency cycle exists.
// In such a case, this being a recursive function, this method will end up
// calling itself infite number of times and end up "in a loop". To prevent
// such a situation, we pass the "proposed_r" parameter each time this method
// is called. The "recursive_call_flag" is set to "B_TRUE" each time this
// method is called within itself, else it should be called with "B_FALSE"
// by default.

static boolean_t
does_r_link_to_inter_cluster_dependents(rgm_resource_t *resource,
    rgm_resource_t *proposed_r, boolean_t recursive_call_flag)
{

	rlist_t *res_ptr, *dept_res_ptr;
	rgm_dependencies_t r_dependents;
	rdeplist_t *dp_list;

	if ((resource == NULL) || (proposed_r == NULL)) {
		return (B_FALSE);
	}

	if (recursive_call_flag == B_TRUE) {
		// If we are here, it means we are inside a recursive
		// call and that we have to ensure that we are not
		// going in cycles.
		if (resource == proposed_r) {
			// This is a dependency cycle
			return (B_FALSE);
		}

		if (strcmp(resource->r_name, proposed_r->r_name) == 0) {
			// This is a dependency cycle
			return (B_FALSE);
		}
	}

	// Check for strong dependency
	if (resource->r_inter_cluster_dependents.dp_strong != NULL &&
	    resource->r_inter_cluster_dependents.dp_strong->rl_name
	    != NULL) {
		// There was a strong ICRD
		return (B_TRUE);
	}
	// Check for weak dependency
	if (resource->r_inter_cluster_dependents.dp_weak != NULL &&
	    resource->r_inter_cluster_dependents.dp_weak->rl_name
	    != NULL) {
		// There was a weak ICRD
		return (B_TRUE);
	}
	// Check for restart dependency
	if (resource->r_inter_cluster_dependents.dp_restart != NULL &&
	    resource->r_inter_cluster_dependents.dp_restart->rl_name
	    != NULL) {
		// There was a restart ICRD
		return (B_TRUE);
	}
	// Check for offline restart dependency
	if (resource->r_inter_cluster_dependents.dp_offline_restart != NULL &&
	    resource->r_inter_cluster_dependents.dp_offline_restart->rl_name
	    != NULL) {
		// There was an offline restart ICRD
		return (B_TRUE);
	}

	// There are no direct inter-cluster-R-dependents, but this
	// resource might have dependents in the current cluster itself
	// which have inter-cluster-R-dependents. We have to check for
	// them as well.
	res_ptr = rname_to_r(NULL, resource->r_name);

	if (res_ptr == NULL) {
		// Ideally we should not be here.
		return (B_FALSE);
	}

	r_dependents = res_ptr->rl_dependents;

	// Check for dependents having strong dependency
	if (r_dependents.dp_strong != NULL) {

		for (dp_list = r_dependents.dp_strong; dp_list;
		    dp_list = dp_list->rl_next) {
			dept_res_ptr = NULL;
			dept_res_ptr = rname_to_r(NULL, dp_list->rl_name);
			if (dept_res_ptr == NULL) {
				// We should not be here ideally
				continue;
			}

			// Check whether this dependent have
			// inter-cluster-R-dependents.
			if (does_r_link_to_inter_cluster_dependents(
			    dept_res_ptr->rl_ccrdata, proposed_r, B_TRUE)) {
				// This resource has an ICR-dependent.
				// It means that the resource for which
				// this method was called is linked to
				// a ICR-dependent indirectly. So, we will
				// return true.
				return (B_TRUE);
			}
		}
	}

	// Check for dependents having weak dependency
	if (r_dependents.dp_weak != NULL) {

		for (dp_list = r_dependents.dp_weak; dp_list;
		    dp_list = dp_list->rl_next) {
			dept_res_ptr = NULL;
			dept_res_ptr = rname_to_r(NULL, dp_list->rl_name);
			if (dept_res_ptr == NULL) {
				// We should not be here ideally
				continue;
			}

			// Check whether this dependent have
			// inter-cluster-R-dependents.
			if (does_r_link_to_inter_cluster_dependents(
			    dept_res_ptr->rl_ccrdata, proposed_r, B_TRUE)) {
				return (B_TRUE);
			}
		}
	}

	// Check for dependents having restart dependency
	if (r_dependents.dp_restart != NULL) {

		for (dp_list = r_dependents.dp_restart; dp_list;
			dp_list = dp_list->rl_next) {
			dept_res_ptr = NULL;
			dept_res_ptr = rname_to_r(NULL, dp_list->rl_name);
			if (dept_res_ptr == NULL) {
				// We should not be here ideally
				continue;
			}

			// Check whether this dependent have
			// inter-cluster-R-dependents.
			if (does_r_link_to_inter_cluster_dependents(
			    dept_res_ptr->rl_ccrdata, proposed_r, B_TRUE)) {
				return (B_TRUE);
			}
		}
	}

	// Check for dependents having offline restart dependency
	if (r_dependents.dp_offline_restart != NULL) {

		for (dp_list = r_dependents.dp_offline_restart; dp_list;
		    dp_list = dp_list->rl_next) {
			dept_res_ptr = NULL;
			dept_res_ptr = rname_to_r(NULL, dp_list->rl_name);
			if (dept_res_ptr == NULL) {
				// We should not be here ideally
				continue;
			}

			// Check whether this dependent have
			// inter-cluster-R-dependents.
			if (does_r_link_to_inter_cluster_dependents(
			    dept_res_ptr->rl_ccrdata, proposed_r, B_TRUE)) {
				return (B_TRUE);
			}
		}
	}

	// If we are here, it means this resource is not linked to any
	// inter-cluster-resource-dependent. So, we will return false.
	return (B_FALSE);
}
#endif
//
// append_NRU_in_strong_deplist: Adds the network resource/network resources to
// the strong dependency list if it does not exist in any of the three
// dependency lists.
//

static scha_errmsg_t
append_NRU_in_strong_deplist(namelist_t  *nru_deplist, rgm_resource_t** r)
{
	rlist_p_t rp;
	rdep_locality_t locality_type;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	// for each r in the NRU  list
	for (; nru_deplist != NULL; nru_deplist = nru_deplist->nl_next) {
		//
		// Check if this resource is already in any of the dependency
		// list
		//
		if ((rdeplist_contains(
		    (*r)->r_dependencies.dp_strong, nru_deplist->nl_name)) ||
		    (rdeplist_contains((*r)->r_dependencies.dp_weak,
		    nru_deplist->nl_name)) ||
		    (rdeplist_contains((*r)->r_dependencies.dp_offline_restart,
		    nru_deplist->nl_name)) ||
		    (rdeplist_contains((*r)->r_dependencies.dp_restart,
		    nru_deplist->nl_name))) {
			continue;
		}

		// Get the resource pointer.
		rp = rname_to_r(NULL, nru_deplist->nl_name);
		if (rp == NULL)
			continue;

		// Remove any list element which is ""
		rdeplist_delete_name(&(
		    (*r)->r_dependencies.dp_strong), "");

		//
		// The NRU is to be added in the
		// strong dependency list.
		//
		//
		if (strcmp((*r)->r_rgname, rp->rl_ccrdata->r_rgname) == 0) {
			locality_type = LOCAL_NODE;
		} else {
			locality_type = FROM_RG_AFFINITIES;
		}
		res = rdeplist_add_name(&((*r)->r_dependencies.dp_strong),
		    nru_deplist->nl_name, locality_type);

		rgm_format_errmsg(&res, RWM_NRU_ADDED_TO_RDEP,
		    nru_deplist->nl_name);
	}
	return (res);
}

//
// update_rdeplist : Whenever there is change/update in the strong
// dependency list this function recomputes the dependency and
// adds any Netork resource to the strong dependency list.This function calls
// the append_NRU_to_strong_dependency list helper function
//
static scha_errmsg_t
update_rdeplist(rgm_resource_t *rs)
{

	rgm_property_list_t *ptr = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	//
	// Traverse the property list structure.Get the Network Resources used
	// and add those network resources in the strong dependency list.
	//

	ptr = (rs)->r_properties;
	while (ptr) {
		if ((ptr->rpl_property->rp_type == SCHA_PTYPE_STRINGARRAY) ||
		    (ptr->rpl_property->rp_type == SCHA_PTYPE_UINTARRAY)) {
			if (strcasecmp(SCHA_NETWORK_RESOURCES_USED,
			    ptr->rpl_property->rp_key) == 0) {
				if (ptr->rpl_property->rp_array_values) {
					res = append_NRU_in_strong_deplist(
					    ptr->rpl_property->rp_array_values,
					    &rs);
					break;
				}
			}
		}
		ptr = ptr->rpl_next;
	}
	return (res);
}	


//
// update_hard_coded_props
//
// Updates the values for the hard-coded properties in the resource (those
// properties that have actual fields in the structure), based on the
// user-passed props list.
//
// Returns a scha_errmsg_t to specify whether the operation succeeded or
// failed.  Note that the function could allocate some memory for the fields
// in the rgm_resource_t struct, but still fail.  The user is responsible for
// freeing the memory allocated.
//
// Note that this function was previously part of update_rs_props, but
// was factored out to provide re-use in apply_rs_props as well.
//
scha_errmsg_t
update_hard_coded_props(const rgm::idl_nameval_seq_t &props,
    rgm_resource_t *rs, boolean_t inter_cluster_set_dep,
    boolean_t inter_cluster_unset_dep)
{
	void	*test = NULL;
	char *new_pval;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	//
	// Before we update following properties, we have to release the
	// memory first. But we have to check whether the property is
	// going to be updated.
	//
	// If the get_prop_value routine returns SCHA_ERR_PROP.
	// we can ignore the error because the property is not in the
	// updated list provided by scrgadm.
	//
	if ((res = get_prop_value(SCHA_R_DESCRIPTION, props,
	    &new_pval, SCHA_PTYPE_STRING)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			return (res);
	} else {
		if (new_pval) {
			free(rs->r_description);
			rs->r_description = new_pval;
		}
	}

	if ((res = get_prop_value(SCHA_RESOURCE_PROJECT_NAME, props,
	    &new_pval, SCHA_PTYPE_STRING)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			return (res);
	} else if (new_pval) {
		free(rs->r_project_name);
		rs->r_project_name = new_pval;
	}

	if ((res = get_prop_value(SCHA_TYPE_VERSION, props,
	    &new_pval, SCHA_PTYPE_STRING)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			return (res);
	} else {
		if (new_pval) {
			free(rs->r_type_version);
			rs->r_type_version = new_pval;
		}
	}

	test = (void *)get_value_string_array_dep(
	    SCHA_RESOURCE_DEPENDENCIES_WEAK, props, res);
	if (res.err_code != SCHA_ERR_NOERR) {
		return (res);
	}
	if (test) {
		rgm_free_rdeplist(rs->r_dependencies.dp_weak);
		rs->r_dependencies.dp_weak = (rdeplist_t *)test;
		test = NULL;
	}

	test = (void *)get_value_string_array_dep(
	    SCHA_RESOURCE_DEPENDENCIES_RESTART, props, res);

	if (res.err_code != SCHA_ERR_NOERR) {
		return (res);
	}
	if (test) {
		rgm_free_rdeplist(rs->r_dependencies.dp_restart);
		rs->r_dependencies.dp_restart = (rdeplist_t *)test;
		test = NULL;
	}

	test = (void *)get_value_string_array_dep(
	    SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART, props, res);

	if (res.err_code != SCHA_ERR_NOERR) {
		return (res);
	}
	if (test) {
		rgm_free_rdeplist(rs->r_dependencies.dp_offline_restart);
		rs->r_dependencies.dp_offline_restart = (rdeplist_t *)test;
		test = NULL;
	}


	test = (void *)get_value_string_array_dep(SCHA_RESOURCE_DEPENDENCIES,
	    props, res);
	if (res.err_code != SCHA_ERR_NOERR) {
		return (res);
	}
	if (test) {
		rgm_free_rdeplist(rs->r_dependencies.dp_strong);
		rs->r_dependencies.dp_strong = (rdeplist_t *)test;
		test = NULL;
	}

	if (allow_inter_cluster() && (
		inter_cluster_set_dep || inter_cluster_unset_dep)) {
		rdeplist_t *test;

		test = get_value_string_array_dep(
		    SCHA_IC_RESOURCE_DEPENDENTS_WEAK, props, res);
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
		if (test) {
			if (inter_cluster_set_dep) {
				if (!rdeplist_contains(
					rs->r_inter_cluster_dependents.dp_weak,
					    test->rl_name)) {
					(void) rdeplist_add_name(
					    &rs->r_inter_cluster_dependents.
					    dp_weak, test->rl_name,
					    test->locality_type);
				}
			} else if (inter_cluster_unset_dep) {
				if (rdeplist_contains(
					rs->r_inter_cluster_dependents.dp_weak,
					    test->rl_name)) {
					rdeplist_delete_name(
					    &rs->r_inter_cluster_dependents.
						dp_weak,
						test->rl_name);
				}
			}
			test = NULL;
		}

		test = get_value_string_array_dep(
		    SCHA_IC_RESOURCE_DEPENDENTS_RESTART, props, res);
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}

		if (test) {
			if (inter_cluster_set_dep) {
				if (!rdeplist_contains(
					rs->r_inter_cluster_dependents.
					    dp_restart, test->rl_name)) {
					(void) rdeplist_add_name(
					    &rs->r_inter_cluster_dependents.
					    dp_restart,
					    test->rl_name,
					    test->locality_type);
				}
			} else if (inter_cluster_unset_dep) {
				if (rdeplist_contains(
					rs->r_inter_cluster_dependents.
					dp_restart, test->rl_name)) {
					rdeplist_delete_name(
					&rs->r_inter_cluster_dependents.
					    dp_restart,
					    test->rl_name);
				}
			}
			test = NULL;
		}

		test = get_value_string_array_dep(
		    SCHA_IC_RESOURCE_DEPENDENTS_OFFLINE_RESTART, props, res);

		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
		if (test) {
			if (inter_cluster_set_dep) {
				if (!rdeplist_contains(
					rs->r_inter_cluster_dependents.
					dp_offline_restart, test->rl_name)) {
					(void) rdeplist_add_name(
					    &rs->r_inter_cluster_dependents.
					    dp_offline_restart,
					    test->rl_name, test->locality_type);
				}
			} else if (inter_cluster_unset_dep) {
				if (rdeplist_contains(
					rs->r_inter_cluster_dependents.
					dp_offline_restart, test->rl_name)) {
					rdeplist_delete_name(
					    &rs->r_inter_cluster_dependents.
					    dp_offline_restart,
					    test->rl_name);
				}
			}
			test = NULL;
		}


		test = get_value_string_array_dep(SCHA_IC_RESOURCE_DEPENDENTS,
		    props, res);
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
		if (test) {
			if (inter_cluster_set_dep) {
				if (!rdeplist_contains(
					rs->r_inter_cluster_dependents.
					dp_strong, test->rl_name)) {
					(void) rdeplist_add_name(
					    &rs->r_inter_cluster_dependents.
					    dp_strong,
					    test->rl_name, test->locality_type);
				}
			} else if (inter_cluster_unset_dep) {
				if (rdeplist_contains(
					rs->r_inter_cluster_dependents.
					dp_strong, test->rl_name)) {
					rdeplist_delete_name(
					&rs->r_inter_cluster_dependents.
					    dp_strong,
					    test->rl_name);
				}
			}
			test = NULL;
		}
	}

	// Update the strong dependency list with the NRU.
	res = update_rdeplist(rs);
	return (res);
}

//
// update_rs_props - update the user-specified property to rs.  Also
// 		validate the property value
//
scha_errmsg_t
update_rs_props(const rgm::idl_nameval_seq_t &props,
    const rgm::idl_nameval_node_seq_t &xprops, rgm_resource_t **rs,
    boolean_t upgrade_allowed, boolean_t inter_cluster_set_dep,
    boolean_t inter_cluster_unset_dep, LogicalNodeset e_ns)
{
	rgm_property_list_t	*ptr = NULL;
	std::map<rgm::lni_t, char *> new_pval;
	namelist_t		*new_pvals;
	sc_syslog_msg_handle_t	handle;
	rgm_param_t		*param = NULL;
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};
	name_t			upgrade_rt_name = NULL;
	uint_t			i;
	rglist_p_t		rgp = NULL;
	LogicalNodeset		*ns;
	char			*key = NULL;
	namelist_t		*oldNRUlst = NULL;
	char			*tmp = NULL;
	rgm_rt_t		*rt = NULL;

	//
	// Update the hard-coded system properties.
	//
	if ((res = update_hard_coded_props(props, *rs, inter_cluster_set_dep,
		inter_cluster_unset_dep)).err_code !=
	    SCHA_ERR_NOERR) {
		goto finished; //lint !e801
	}

	//
	// Note that this code assumes that if get_value_string_array
	// returns NULL, the property in question is not in the args.
	//
	// This assumption requires that get_value_string_array allocate
	// memory with the interposed malloc call, which will exit the daemon
	// on memory allocation failure.
	//

	ptr = (*rs)->r_properties;
	while (ptr) {
		key = ptr->rpl_property->rp_key;
		switch (ptr->rpl_property->rp_type) {
		case SCHA_PTYPE_STRING:
		case SCHA_PTYPE_ENUM:
		case SCHA_PTYPE_INT:
		case SCHA_PTYPE_UINT:
		case SCHA_PTYPE_BOOLEAN:
			if ((res = get_prop_value(key, props,
			    &tmp, ptr->rpl_property->rp_type)).err_code
			    != SCHA_ERR_NOERR) {
				if (res.err_code == SCHA_ERR_PROP) {
					res.err_code = SCHA_ERR_NOERR;
					break;
				} else {
					goto finished; //lint !e801
				}
			}
			if (tmp != NULL) {
				((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value.clear();
				if (ptr->rpl_property->rp_type ==
				    SCHA_PTYPE_BOOLEAN) {
					res = set_bool_value(tmp,
					    ((rgm_value_t *)ptr->rpl_property->
					    rp_value)->rp_value, key);
					free(tmp);
					tmp = NULL;
					if (res.err_code !=
					    SCHA_ERR_NOERR)
						goto finished; //lint !e801
				} else {
					// make failover mode case-insensitive
					if (strcasecmp(key,
					    SCHA_FAILOVER_MODE) == 0) {
						for (i = 0; tmp[i]; i++) {
							tmp[i] =
							    toupper(tmp[i]);
						}
					}
					((rgm_value_t *)ptr->
					    rpl_property->rp_value)->
					    rp_value[0] = tmp;
				}
			}
			break;

		case SCHA_PTYPE_STRINGARRAY:
		case SCHA_PTYPE_UINTARRAY:
			new_pvals = get_value_string_array(key, props);
			if (new_pvals != NULL) {
				if (strcasecmp(SCHA_NETWORK_RESOURCES_USED,
				    ptr->rpl_property->rp_key)
				    == 0) {
					oldNRUlst = namelist_copy(ptr->
					    rpl_property->rp_array_values);
					for (; oldNRUlst != NULL; oldNRUlst =
					    oldNRUlst->nl_next) {
						rdeplist_delete_name(&(
						    (*rs)->
						    r_dependencies.dp_strong),
						    oldNRUlst->nl_name);
					}
					rgm_free_nlist(oldNRUlst);
				}
				rgm_free_nlist(
				    ptr->rpl_property->rp_array_values);
				ptr->rpl_property->rp_array_values = new_pvals;
			}

			//
			// Add the resource to strong dependency list if it is
			// a network resource
			//

			if (strcasecmp(SCHA_NETWORK_RESOURCES_USED,
			    ptr->rpl_property->rp_key)
			    == 0) {
				if (ptr->rpl_property->rp_array_values) {
					res = append_NRU_in_strong_deplist(
					    ptr->rpl_property->rp_array_values,
					    rs);
					if (res.err_code != SCHA_ERR_NOERR)
						goto finished; //lint !e801
				}
			}
			break;

		default:
			// should not happen; abort node
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// An attempted creation or update of a resource has
			// failed because of invalid resource type data. This
			// may indicate CCR data corruption or an internal
			// logic error in the rgmd. The rgmd will produce a
			// core file and will force the node to halt or
			// reboot.
			// @user_action
			// Use clresource show -v and clresourcetype show -v
			// to examine resource properties. If the resource or
			// resource type properties appear to be corrupted,
			// the CCR might have to be rebuilt. If values appear
			// correct, this may indicate an internal error in the
			// rgmd. Re-try the creation or update operation. If
			// the problem recurs, save a copy of the
			// /var/adm/messages files on all nodes and contact
			// your authorized Sun service provider for
			// assistance.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "INTERNAL ERROR: Invalid resource property "
			    "type <%d> on resource <%s>; aborting node",
			    ptr->rpl_property->rp_type, (*rs)->r_name);
			sc_syslog_msg_done(&handle);
			abort();
			/* NOTREACHED */
		}
		ptr = ptr->rpl_next;
	}


	ptr = (*rs)->r_ext_properties;
	while (ptr) {
		key = ptr->rpl_property->rp_key;
		switch (ptr->rpl_property->rp_type) {
		case SCHA_PTYPE_STRING:
		case SCHA_PTYPE_ENUM:
		case SCHA_PTYPE_INT:
		case SCHA_PTYPE_UINT:
		case SCHA_PTYPE_BOOLEAN:
			rgp = rgname_to_rg((*rs)->r_rgname);
			ns = get_logical_nodeset_from_nodelist(rgp->rgl_ccr->
			    rg_nodelist);
			rt = rtname_to_rt((*rs)->r_type);
			param = (rgm_param_t *)get_param_prop(key, B_TRUE,
			    rt->rt_paramtable);
			res = get_prop_value_ext(key, xprops,
			    new_pval, ptr, ns, e_ns,
			    ptr->rpl_property->rp_type, param->p_tunable);
			if (res.err_code != SCHA_ERR_NOERR) {
				if (res.err_code == SCHA_ERR_PROP) {
					res.err_code = SCHA_ERR_NOERR;
					break;
				} else {
					goto finished; //lint !e801
				}
			} else {
				((rgm_value_t *)ptr->rpl_property->rp_value)
				->rp_value.clear();

				if (ptr->rpl_property->rp_type ==
				    SCHA_PTYPE_BOOLEAN) {
					res = set_bool_value_ext(new_pval,
					((rgm_value_t *)ptr->rpl_property->
					rp_value)->rp_value, key,
					ptr->rpl_property->is_per_node,
					ns, e_ns, param->p_tunable);
					if (ptr->rpl_property->is_per_node ==
					    B_TRUE) {
						for (i = ns->nextLniSet(1);
						    i != 0;
						    i = ns->nextLniSet(i + 1)) {
							free(new_pval[i]);
							new_pval[i] = NULL;
						}
					} else {
						free(new_pval[0]);
						new_pval[0] = NULL;
					}

					if (res.err_code != SCHA_ERR_NOERR)
						goto finished; //lint !e801
				} else {
					((rgm_value_t *)
					    ptr->rpl_property->rp_value)
					    ->rp_value = new_pval;
					new_pval.clear();
				}
			}
			break;

		case SCHA_PTYPE_STRINGARRAY:
		case SCHA_PTYPE_UINTARRAY:
			if (ptr->rpl_property->is_per_node ==
			    B_TRUE) {
				res.err_code = SCHA_ERR_INVAL;
				goto finished; //lint !e801
			}
			new_pvals = get_value_string_array_ext(key, xprops);
			if (new_pvals != NULL) {
				rgm_free_nlist(
				    ptr->rpl_property->rp_array_values);
				ptr->rpl_property->rp_array_values = new_pvals;
			}
			break;
		default:
			// should not happen; abort node
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// An attempted creation or update of a resource has
			// failed because of invalid resource type data. This
			// may indicate CCR data corruption or an internal
			// logic error in the rgmd. The rgmd will produce a
			// core file and will force the node to halt or
			// reboot.
			// @user_action
			// Use clresource show -v and clresourcetype show -v
			// to examine resource properties. If the resource or
			// resource type properties appear to be corrupted,
			// the CCR might have to be rebuilt. If values appear
			// correct, this may indicate an internal error in the
			// rgmd. Re-try the creation or update operation. If
			// the problem recurs, save a copy of the
			// /var/adm/messages files on all nodes and contact
			// your authorized Sun service provider for
			// assistance.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "INTERNAL ERROR: "
			    "Invalid resource extension property type <%d> "
			    "on resource <%s>; aborting node",
			    ptr->rpl_property->rp_type, (*rs)->r_name);
			sc_syslog_msg_done(&handle);
			abort();
			/* NOTREACHED */
		}

		ptr = ptr->rpl_next;
	}

	if (upgrade_allowed) {
		if ((res = create_upgrade_rtname(props, (*rs)->r_type,
		    &upgrade_rt_name)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
		free((*rs)->r_type);
		(*rs)->r_type = upgrade_rt_name;
	}

finished:
	return (res);

}

//
// apply_user_props
//
// Add the user-specified properties to the resource structure.
// For each property in the props and the xprops lists, either overwrites
// an existing property in rs (if it exists), or adds a new rgm_property_t
// structure to the system or extension properties lists.
//
// This function is different from update_rs_props in that it does not
// expect that each property in props and xprops will already have an
// rgm_property_list_t structure in the system or extension property
// lists in the resource.  Thus, it cannot iterate through the already
// existing structures the same way that update_rs_props works.
//
static scha_errmsg_t
apply_user_props(const rgm::idl_nameval_seq_t &props,
    const rgm::idl_nameval_node_seq_t &xprops, rgm_param_t **paramtable,
    rgm_resource_t **rs, boolean_t upgrade_allowed,
    boolean_t inter_cluster_set_dep,
    boolean_t inter_cluster_unset_dep, LogicalNodeset e_ns)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t temp_res = {SCHA_ERR_NOERR, NULL};
	rgm_property_list_t *ptr = NULL;
	rgm::idl_nameval_node one_prop_ext;
	rgm::idl_nameval one_prop;
	uint_t i, len;
	char *prop_name = NULL;
	rgm_property_list_t *temp_p;
	name_t upgrade_rt_name = NULL;

	//
	// Update the hard-coded system properties.
	//
	if ((temp_res = update_hard_coded_props(props, *rs,
	    inter_cluster_set_dep, inter_cluster_unset_dep)).err_code !=
	    SCHA_ERR_NOERR) {
		return (temp_res);
	}

	len = props.length();
	for (i = 0; i < len; i++) {

		//
		// first, look up if we have a structure for the property
		// already.
		//
		if ((prop_name = strdup_nocheck(props[i].idlstr_nv_name))
		    == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}
		// we look for the property in the system property list
		temp_p = rgm_find_property_in_list(prop_name,
		    (*rs)->r_properties);

		if (temp_p != NULL) {
			// We already have a structure.
			// Just change the old value to the new value
			// that the user entered on the command line.
			//
			// Call a helper function to actually add the property
			// value for this property, depending on its type.
			//
			res = add_r_property(props[i], one_prop_ext,
			    temp_p, rs, B_FALSE, e_ns);
			if (res.err_code != SCHA_ERR_NOERR) {
				goto end_apply_user_props; //lint !e801
			}
		} else {

			//
			// We didn't find an existing structure,
			// so go ahead and create a new one.
			//
			// The helper function create_rgm_property
			// fills in the new value, as well as allocating
			// the structure.
			//
			// We pass the name from the paramtable
			//
			ptr = NULL;
			res = create_rgm_property(props[i],
			    one_prop_ext, B_FALSE,
			    paramtable, &ptr, rs, e_ns);
			if (res.err_code != SCHA_ERR_NOERR) {
				goto end_apply_user_props; //lint !e801
			}

			if (ptr != NULL) {
				//
				// add the new property to the head of
				// the system properties list
				//
				ptr->rpl_next = (*rs)->r_properties;
				(*rs)->r_properties = ptr;
			}
		}
	}

	//
	// Loop through the list of extension properties that the user entered.
	//
	len = xprops.length();
	for (i = 0; i < len; i++) {
		//
		// first, look up if we have a structure for the property
		// already.
		//

		if ((prop_name = strdup_nocheck(xprops[i].idlstr_nv_name))
		    == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto end_apply_user_props; //lint !e801
		}

		// we look for the property in the extension property list
		temp_p = rgm_find_property_in_list(
		    prop_name, (*rs)->r_ext_properties);

		if (temp_p != NULL) {
			//
			// We already have a structure.
			// Just change the old value to the new value
			// that the user entered on the command line.
			//
			// Call a helper function to actually add the property
			// value for this property, depending on its type.
			//
			res = add_r_property(one_prop, xprops[i],
			    temp_p, rs, B_TRUE, e_ns);
			if (res.err_code != SCHA_ERR_NOERR) {
				goto end_apply_user_props; //lint !e801
			}
		} else {
			//
			// We didn't find an existing structure,
			// so go ahead and create a new one.
			//
			// The helper function create_rgm_property
			// fills in the new value, as well as allocating
			// the structure.
			//
			ptr = NULL;
			res = create_rgm_property(one_prop, xprops[i], B_TRUE,
			    paramtable, &ptr, rs, e_ns);
			if (res.err_code != SCHA_ERR_NOERR) {
				goto end_apply_user_props; //lint !e801
			}

			if (ptr != NULL) {
				//
				// add the new property to the head of
				// the extension properties list
				//
				ptr->rpl_next = (*rs)->r_ext_properties;
				(*rs)->r_ext_properties = ptr;
			}
		}
	}

	if (upgrade_allowed) {
		if ((res = create_upgrade_rtname(props, (*rs)->r_type,
		    &upgrade_rt_name)).err_code != SCHA_ERR_NOERR)
			return (res);
		free((*rs)->r_type);
		(*rs)->r_type = upgrade_rt_name;
	}

end_apply_user_props:
	free(prop_name);

	// Append notice message to res if there was any.
	(void) transfer_message(&res, &temp_res);
	return (res);
}

//
// create_rgm_property
//
// This function allocate a new rgm_property_list_t, and stores it in
// *prop_struct_p.
//
// The property one_prop's name is looked up in the paramtable to
// find the type of the property.  Based on the type, the property
// value from the one_prop idl_nameval is converted and stored in
// the new rgm_property_list_t.
//
// Returns a scha_errmsg_t with err_code SCHA_ERR_NOERR, unless there is an
// error. If there is an error, no memory is allocated in  *prop_struct_p.
//
// Note: the code in this function was modified from some code in
// rgmcnfg_resource_tmpl.
//
static scha_errmsg_t
create_rgm_property(const rgm::idl_nameval &one_prop,
    const rgm::idl_nameval_node &one_prop_ext,  boolean_t is_extension,
    rgm_param_t **paramtable, rgm_property_list_t **prop_struct_p,
    rgm_resource_t **rs, LogicalNodeset e_ns)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	// initialize the struct to NULL, in case we return early.
	*prop_struct_p = NULL;
	char *name = NULL;


	//
	// look up prop in paramtable, to get the type of the
	// property, and the "official" name of the property.
	// We know that any property the user entered
	// has to be declared in the paramtable (already checked).
	//
	if (is_extension) {
		name = strdup_nocheck(one_prop_ext.idlstr_nv_name);
	} else {
		name = strdup_nocheck(one_prop.idlstr_nv_name);
	}
	const rgm_param_t *param_default = get_param_prop(
	    name, is_extension, paramtable);

	if (param_default != NULL) {
		// we are supposed to add this property (ie.
		// it wasn't a hard-coded system prop
		// that we already added).

		// allocate the memory for this property
		*prop_struct_p = (rgm_property_list_t *)
		    malloc_nocheck(sizeof (rgm_property_list_t));

		if (*prop_struct_p == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}

		// zero it out to make freeing on errors easier.
		(void) memset(*prop_struct_p, '\0',
		    sizeof (rgm_property_list_t));

		(*prop_struct_p)->rpl_property =
		    (rgm_property_t *)malloc_nocheck(sizeof (rgm_property_t));
		if ((*prop_struct_p)->rpl_property == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto err_end_create_rgm_property; //lint !e801
		}

		// zero it out to make freeing on errors easier.
		(void) memset((*prop_struct_p)->rpl_property, '\0',
		    sizeof (rgm_property_t));
		(*prop_struct_p)->rpl_property->rp_value =
		    new rgm_value_t;

		//
		// Use the "official" name of the property, from the
		// paramtable, because some code (for example the
		// scha_resource_get implementation, and the code for
		// launching methods that looks up the timeout values)
		// depends on case-sensitive versions of the property names.
		//
		// Arguably, those code paths shouldn't be case
		// sensitive with respect to property names, but it's easier
		// to use the "official" name here than change them all.
		//
		(*prop_struct_p)->rpl_property->rp_key = strdup(
		    param_default->p_name);
		if ((*prop_struct_p)->rpl_property->rp_key == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto err_end_create_rgm_property; //lint !e801
		}


		(*prop_struct_p)->rpl_property->is_per_node =
		    (boolean_t)one_prop_ext.is_per_node;

		(*prop_struct_p)->rpl_property->rp_array_values = NULL;
		(*prop_struct_p)->rpl_property->rp_description = NULL;

		// set the type based on the paramtable
		(*prop_struct_p)->rpl_property->rp_type =
		    param_default->p_type;
		(*prop_struct_p)->rpl_next = NULL;

		//
		// Call a helper function to actually add the property
		// value for this property, depending on its type.
		//
		res = add_r_property(one_prop, one_prop_ext,
		    *prop_struct_p, rs, is_extension, e_ns);

		if (res.err_code != SCHA_ERR_NOERR) {
			goto err_end_create_rgm_property; //lint !e801
		}
	}
	return (res);

err_end_create_rgm_property:
	//
	// We don't know at what point we jumped to here.  Assume that
	// all allocated memory was zeroed, so we can use NULL checks to
	// figure out what was allocated.
	//
	if (*prop_struct_p) {
		if ((*prop_struct_p)->rpl_property) {
			if ((*prop_struct_p)->rpl_property->rp_key) {
				free ((*prop_struct_p)->rpl_property->rp_key);
			}
			free ((*prop_struct_p)->rpl_property);
		}
		free (*prop_struct_p);
		*prop_struct_p = NULL;
	}
	free(name);
	return (res);
}

//
// add_r_property
//
// Helper function to add a property to this rgm_property_list_t,
// depending on the type specified in the rgm_property_list_t.
//
// There can either be an old property value present (which will be freed),
// or the rgm_property_list_t can be brand new.
//
// If there is an error, no memory is allocated, and an error code is
// returned in the scha_errmsg_t.
//

static scha_errmsg_t
add_r_property(const rgm::idl_nameval &one_prop,
    const rgm::idl_nameval_node &one_prop_ext,
    rgm_property_list_t *prop_struct, rgm_resource_t **rs,
    boolean_t is_extension, LogicalNodeset e_ns)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	std::map<rgm::lni_t, char *> new_pval;
	char *tmp = NULL;
	namelist_t *new_pvals;
	rglist_p_t rgp = NULL;
	uint_t i;
	LogicalNodeset *ns;
	rgm_rt_t *rt = NULL;
	rgm_param_t	*param = NULL;

	//
	// figure out how to parse the property value
	//
	// Note: this switch statement is different from the one
	// in update_rs_props in that we already have the
	// property, so we can call convert_prop_value instead
	// of get_prop_value, and convert_value_string_array
	// instead of get_value_string_array.
	//
	// Without resorting to function pointers, it's hard to
	// combine the two versions of the switch.
	//
	char *key = prop_struct->rpl_property->rp_key;
	switch (prop_struct->rpl_property->rp_type) {
	case SCHA_PTYPE_STRING:
	case SCHA_PTYPE_ENUM:
	case SCHA_PTYPE_INT:
	case SCHA_PTYPE_UINT:
	case SCHA_PTYPE_BOOLEAN:
		//
		// It's a single value, not an array, so call
		// the helper convert_prop_value to get the string
		// version, and to do error checking.
		//
		if (is_extension) {
			//
			// The property being added is an extension property.
			// Call convert_prop_value_ext to marshal the input
			// idl sequence to a string. If the extension property
			// that is being updated is a per node one, the values
			// are populated in an dynamic array (map).
			// Each index in the array represents the value of the
			// property for each potential node in the cluster.
			//
			rgp = rgname_to_rg((*rs)->r_rgname);
			ns = get_logical_nodeset_from_nodelist(rgp->rgl_ccr->
			    rg_nodelist);
			rt = rtname_to_rt((*rs)->r_type);
			param = (rgm_param_t *)get_param_prop(key,
			    B_TRUE, rt->rt_paramtable);

			res = convert_prop_value_ext(key, one_prop_ext,
			    new_pval, prop_struct, B_TRUE, ns, e_ns,
			    prop_struct->rpl_property->rp_type,
			    param->p_tunable);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}

			if (prop_struct->rpl_property->is_per_node) {

				for (i = ns->nextLniSet(1); i != 0;
				    i = ns->nextLniSet(i + 1)) {
					free(((rgm_value_t *)prop_struct->
					    rpl_property->rp_value)->
					    rp_value[i]);
					((rgm_value_t *)prop_struct->
					    rpl_property->
					    rp_value)->rp_value[i] = NULL;
				}
			} else {

				free(((rgm_value_t *)prop_struct->
				    rpl_property->
				    rp_value)->rp_value[0]);
				((rgm_value_t *)prop_struct->rpl_property->
				    rp_value)->rp_value[0] = NULL;
			}
			if (prop_struct->rpl_property->rp_type ==
			    SCHA_PTYPE_BOOLEAN) {
				res = set_bool_value_ext(new_pval,
				    ((rgm_value_t *)prop_struct->rpl_property
				    ->rp_value)->rp_value, key,
				    prop_struct->rpl_property->is_per_node,
				    ns, e_ns, param->p_tunable);
				new_pval.clear();

				if (res.err_code != SCHA_ERR_NOERR)
					return (res);
			} else {
				((rgm_value_t *)prop_struct->rpl_property
				    ->rp_value)->rp_value = new_pval;
			}
		} else {
			if ((res = convert_prop_value(key, one_prop,
			    &tmp, prop_struct->rpl_property->rp_type)).err_code
			    != SCHA_ERR_NOERR) {
				return (res);
			} else {
				((rgm_value_t *)prop_struct->rpl_property
				    ->rp_value)->rp_value.clear();
				if (prop_struct->rpl_property->rp_type ==
				    SCHA_PTYPE_BOOLEAN) {
					res = set_bool_value(tmp,
					    ((rgm_value_t *)prop_struct->
					    rpl_property->
					    rp_value)->rp_value, key);
					free(new_pval[0]);
					free(tmp);
					if (res.err_code != SCHA_ERR_NOERR)
						return (res);
				} else {
					if (strcasecmp(key,
					    SCHA_FAILOVER_MODE) == 0) {
						for (i = 0; tmp[i]; i++) {
							tmp[i] =
							    toupper(tmp[i]);
						}
					}
					((rgm_value_t *)prop_struct->
					    rpl_property->
					    rp_value)->rp_value[0] = tmp;
				}
			}
		}
		break;

	case SCHA_PTYPE_STRINGARRAY:
	case SCHA_PTYPE_UINTARRAY:
		if (is_extension)
			new_pvals = convert_value_string_array(
			    one_prop_ext.nv_val);
		else
			new_pvals = convert_value_string_array(one_prop.nv_val);
		if (new_pvals != NULL) {
			//
			// If NRU is changed or deleted we need to
			// delete it from the strong dependency list.
			//
			if (strcasecmp(SCHA_NETWORK_RESOURCES_USED, key)
			    == 0) {
				namelist_t *oldNRUlst = NULL;
				oldNRUlst = namelist_copy(prop_struct->
				    rpl_property->rp_array_values);
				for (; oldNRUlst != NULL; oldNRUlst =
				    oldNRUlst->nl_next) {
					rdeplist_delete_name(&(
					    (*rs)->r_dependencies.dp_strong),
					    oldNRUlst->nl_name);
				}
				rgm_free_nlist(oldNRUlst);
			}
			rgm_free_nlist(prop_struct->rpl_property->
			    rp_array_values);
			prop_struct->rpl_property->
			    rp_array_values = new_pvals;
		}

		//  Add the NRU list to the strong dependency list.
		if (strcasecmp(SCHA_NETWORK_RESOURCES_USED, key)
		    == 0) {
			if ((prop_struct->rpl_property->rp_array_values) &&
			    (strcmp(namelist_to_str(
			    prop_struct->rpl_property->rp_array_values),
			    "") != 0)) {
				res = append_NRU_in_strong_deplist(
				    prop_struct->rpl_property->rp_array_values,
				    rs);
				if (res.err_code != SCHA_ERR_NOERR)
					return (res);
				rgm_format_errmsg(&res, RWM_NRU_OVERRIDE_DEP);
			}
		}
		break;
	default:
		// should not happen; abort node
		sc_syslog_msg_handle_t handle;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR: "
		    "Invalid resource property type <%d> "
		    "on resource <%s>; aborting node",
		    prop_struct->rpl_property->rp_type, (*rs)->r_name);
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}
	return (res);
}

//
// check_tunable_flag() - check whether the property can be updated
//
//	If update_op is FALSE, this routine is being called on R creation.  If
//	TRUE, it's being called on R update.
//
//	If extension is TRUE, the given property is an extension property.
//
//	RT paramtable contains the tunable flag for optional system-defined and
//	extension properties
//	Get the TUNABLE flag from RT paramtable.
// 		If tunable flag is ANYTIME, update is OK.
//		If tunable flag is NONE, no update is allowed.
//		If tunable flag is AT_CREATION and this is R creation , update
//		is allowed.  If this is R update, no update is allowed.
//		If tunable flag is WHEN_DISABLED and the R is in ENABLE state,
//		no update is allowed.
//

scha_errmsg_t
check_tunable_flag(const char *prop_name, rgm_param_t **paramtable,
    std::map<rgm::lni_t, scha_switch_t> &r_onoff, boolean_t update_op,
    boolean_t extension, LogicalNodeset *enabled_ns)
{
	rgm_param_t	**param;
	boolean_t	found = B_FALSE;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	std::map<rgm::lni_t, scha_switch_t>::iterator iter;


	for (param = paramtable; *param; param++) {
		found = B_FALSE;
		if ((*param)->p_name == NULL)
			break;
		if ((strcasecmp((*param)->p_name, prop_name) == 0) &&
		    ((*param)->p_extension == extension)) {
			found = B_TRUE;

			switch ((*param)->p_tunable) {
			case TUNE_ANYTIME:
				res.err_code = SCHA_ERR_NOERR;
				return (res);

			case TUNE_NONE:
				ucmm_print("RGM", NOGET("check_tunable_flag: "
				    "Cannot update property <%s> when "
				    "its tunable flag is NONE\n"), prop_name);
				rgm_format_errmsg(&res, REM_PROP_TUNE_NONE,
				    prop_name);
				res.err_code = SCHA_ERR_CHECKS;
				return (res);

			case TUNE_AT_CREATION:

				//
				// Only allow to be changed at R creation
				//
				if (!update_op) {
					res.err_code = SCHA_ERR_NOERR;
					return (res);
				}
				//
				// If this is R update, return error
				//
				ucmm_print("RGM", NOGET("check_tunable_flag: "
				    "Cannot update property <%s> when "
				    "tunable flag is AT_CREATION\n"),
				    prop_name);
				rgm_format_errmsg(&res,
				    REM_PROP_TUNE_AT_CREATION, prop_name);
				res.err_code = SCHA_ERR_CHECKS;
				return (res);

			case TUNE_WHEN_DISABLED:

				//
				// Only allowed to be changed when R is disabled
				//
				if (!update_op) {
					res.err_code = SCHA_ERR_NOERR;
					return (res);
				}
				for (iter = r_onoff.begin();
				    iter != r_onoff.end(); iter++) {
					if (r_onoff[iter->first] !=
					    SCHA_SWITCH_DISABLED) {
						if ((*param)->p_per_node) {
							(*enabled_ns).
							addLni(iter->first);
							continue;
						}
						/*
						 * If Per node ext prop,
						 * then check out if the
						 * property is updated on
						 * this node. if yes, only
						 * then the update is not
						 * allowed. We are only
						 * building the enabled
						 * nodeset here. The enabled
						 * nodeset would be checked
						 * by convert_prop_value_ext
						 * or set_bool_value_ext
						 * functions in rgm_util.cc
						 */
						ucmm_print("RGM", NOGET(
						    "check_tunable_flag: "
						    "Cannot update property "
						    "<%s> when tunable "
						    "flag is WHEN_DISABLED\n"),
						    prop_name);
						rgm_format_errmsg(&res,
						    REM_PROP_TUNE_WHEN_DISABLED,
						    prop_name);
						res.err_code = SCHA_ERR_CHECKS;
						return (res);
					}
				}
				res.err_code = SCHA_ERR_NOERR;
				return (res);

			case TUNE_WHEN_OFFLINE:
			case TUNE_WHEN_UNMANAGED:
			case TUNE_WHEN_UNMONITORED:
			default:
				// should not happen; abort node
				sc_syslog_msg_handle_t handle;
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// An internal error occurred in the rgmd
				// while checking whether a resource property
				// could be modified. The rgmd will produce a
				// core file and will force the node to halt
				// or reboot.
				// @user_action
				// Look for other syslog error messages on the
				// same node. Save a copy of the
				// /var/adm/messages files on all nodes, and
				// report the problem to your authorized Sun
				// service provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "INTERNAL ERROR: "
				    "Invalid resource property tunable "
				    "flag <%d> for property <%s>; "
				    "aborting node",
				    (*param)->p_tunable, prop_name);
				sc_syslog_msg_done(&handle);
				abort();
				/* NOTREACHED */

			} // case
		} // if
	} // for loop
	if (!found) {
		rgm_format_errmsg(&res, REM_PROP_NOT_EXIST,
		    prop_name);
		res.err_code = SCHA_ERR_PROP;
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}

//
// check_rs_updatable_props(): 	Checks whether the property can be updated
//
//	If update_op is FALSE, this routine is being called on R creation.  If
//	TRUE, it's being called on R update.
//
//	props contains system-defined and optional system-defined properties.
//	ext_props contains extension properties.
//
//	For the properties in props, first check the system_defined updatable
//	table.  If can not find the property, then it might be an optional
//	system-defined property,  check the RT paramtable.
//	Return error if it can not be updated, it is an extension
//	property or it does not exist.
//
//	For the properties in ext_props, check the RT paramtable.
//	Return error if it can not be updated, it is an system-defined
//	property or it does not exist.
//

scha_errmsg_t
check_rs_updatable_props(const rgm::idl_nameval_seq_t &props,
    const rgm::idl_nameval_node_seq_t &ext_props, rgm_param_t **paramtable,
    std::map<rgm::lni_t, scha_switch_t> &r_onoff_switch, boolean_t update_op,
    nodeidlist *nodelist, LogicalNodeset *e_ns)
{

	uint_t			i, n, len, node_len, j;
	updatable_prop_t	*rs_tbl = NULL;
	char			*prop_name = NULL;
	boolean_t		found = B_FALSE;
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};


	len = props.length();
	//
	// Loop through the list of updated properties in props.
	//
	for (i = 0; i < len; i++) {
		found = B_FALSE;
		if ((prop_name = strdup_nocheck(props[i].idlstr_nv_name))
		    == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}

		if (strcasecmp(prop_name, "Type_version") == 0 &&
		    update_op == B_FALSE) {
			rgm_format_errmsg(&res, REM_UPDATE_TV_AT_CREATTION);
			res.err_code = SCHA_ERR_UPDATE;
			goto finished; //lint !e801
		}

		if (is_sysprop(prop_name, paramtable)) {
			//
			// If it is not allowed to be updated, return error.
			//
			if ((res = check_tunable_flag(prop_name, paramtable,
			    r_onoff_switch, update_op, B_FALSE,
			    e_ns)).err_code != SCHA_ERR_NOERR)
				goto finished; //lint !e801

			if (prop_name != NULL) {
				free(prop_name);
				prop_name = NULL;
			}
			continue;
		}

		//
		// check updatable system-defined property list
		//
		for (n = 0; n < RS_UPDATABLE_ENTRIES; n++) {
			rs_tbl = &rs_updatable_props[n];
			if (strcasecmp(prop_name, rs_tbl->prop_name) == 0) {
				found = B_TRUE;

				//
				// return error if it cannot be updated.
				//
				if (!rs_tbl->updatable) {
					ucmm_print("RGM", NOGET(
					    "check_rs_updatable_props(): "
					    "property <%s> is not permitted "
					    "to be updated\n"), prop_name);
					rgm_format_errmsg(&res,
					    REM_PROP_CANT_UPDATE, prop_name);
					res.err_code = SCHA_ERR_UPDATE;
					goto finished; //lint !e801
				}

				//
				// check the next property in 'props' list
				//
				break;
			}
		} // for

		//
		// found the property in system-defined updatable table
		// check the next property in 'props"
		//
		if (found) {
			if (prop_name != NULL) {
				free(prop_name);
				prop_name = NULL;
			}
			continue;
		}

		//
		// if the user specifies -y for an extension property,
		// return error
		//
		// modify prop_name temporarily so that "prop{node}=val"
		// is sent as "prop\0node}=val". reset it after is_extprop()
		//
		char *tempchar = NULL;
		if ((tempchar = strchr(prop_name, RGM_PN_SEPARATOR_PRE))
		    != NULL) {
			*tempchar = '\0';
		}
		if (is_extprop(prop_name, paramtable)) {
			ucmm_print("check_rs_updatable_props()",
			    NOGET("property <%s> is an extension property\n"),
			    prop_name);
			rgm_format_errmsg(&res, REM_PROP_IS_EXT,
			    prop_name);
			res.err_code = SCHA_ERR_PROP;
			goto finished; //lint !e801
		}
		// restore the temporary modification done above
		if (tempchar != NULL) {
			*tempchar = RGM_PN_SEPARATOR_PRE;
		}

		//
		// if this property does not exist, return error
		//
		ucmm_print("check_rs_updatable_props()",
		    NOGET("property <%s> is not declared "
		    "in rtr file\n"), prop_name);
		rgm_format_errmsg(&res, REM_PROP_NOT_EXIST,
		    prop_name);
		res.err_code = SCHA_ERR_PROP;
		goto finished; //lint !e801
	} // for loop

	//
	// check the extension properties
	//
	len = 0;
	prop_name = NULL;
	len = ext_props.length();

	for (i = 0; i < len; i++) {
		if ((prop_name = strdup_nocheck(ext_props[i].idlstr_nv_name))
		    == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}

		if (ext_props[i].is_per_node) {
			//
			// Make sure that the nodes on which the property value
			// is updated is part of the RG's
			// nodelist that hosts the resource.
			//
			node_len = 0;
			node_len = ext_props[i].nv_nodes.length();
			for (j = 0; j < node_len; j++) {
				if (!nodeidlist_contains(nodelist,
				    (const char *)ext_props[i].nv_nodes[j])) {
					res.err_code = SCHA_ERR_NODE;
					goto finished; //lint !e801
				}
			}
		}

		if (is_extprop(prop_name, paramtable)) {
			//
			// If it is not allowed to be updated, return error.
			//
			if ((res = check_tunable_flag(prop_name, paramtable,
			    r_onoff_switch, update_op, B_TRUE,
			    e_ns)).err_code != SCHA_ERR_NOERR) {
				goto finished; //lint !e801
			}
			if (prop_name != NULL) {
				free(prop_name);
				prop_name = NULL;
			}

			//
			// check the next property name in 'ext_props'
			//
			continue;
		}

		if (is_sysprop(prop_name, paramtable)) {
			// User specified -x for an optional system
			// property.  -x is for extension properties.
			// Tell user to use -y for system properties.
			ucmm_print("check_rs_updatable_props()",
			    NOGET("property <%s> is a system-defined "
			    "property\n"), prop_name);
			rgm_format_errmsg(&res, REM_PROP_IS_SYS,
			    prop_name);
			res.err_code = SCHA_ERR_PROP;
			goto finished; //lint !e801
		}

		for (n = 0; n < RS_UPDATABLE_ENTRIES; n++) {
			rs_tbl = &rs_updatable_props[n];
			if (strcasecmp(prop_name, rs_tbl->prop_name) == 0) {
				// user specify -x option for a system-defined
				// property, return error
				if (!rs_tbl->updatable) {
					// User specified -x for non-updatable
					// system property.  -x is for
					// extension properties.
					// Since this property cannot be
					// updated, don't return
					// 'REM_PROP_IS_SYS' error message.
					// Tell user that property was not
					// declared in RTR file
					ucmm_print("check_rs_updatable_props()",
					    NOGET("property <%s> is not "
					    "declared in rtr file\n"),
					    prop_name);
					rgm_format_errmsg(&res,
					    REM_PROP_NOT_EXIST,
					    prop_name);
					res.err_code = SCHA_ERR_PROP;
					goto finished; //lint !e801
				}

				// User specified -x for a system
				// property.  -x is for extension properties.
				// Tell user to use -y for system properties.
				ucmm_print("check_rs_updatable_props()",
				    NOGET("property <%s> is a system-defined "
				    "property\n"), prop_name);
				rgm_format_errmsg(&res, REM_PROP_IS_SYS,
				    prop_name);
				res.err_code = SCHA_ERR_PROP;
				goto finished; //lint !e801
			}
		}
		//
		// the property does not exist, return error
		//
		ucmm_print("check_rs_updatable_props()",
		    NOGET("property <%s> is not declared in rtr file\n"),
		    prop_name);
		rgm_format_errmsg(&res, REM_PROP_NOT_EXIST,
		    prop_name);
		res.err_code = SCHA_ERR_PROP;
		goto finished; //lint !e801
	}

finished:
	if (prop_name != NULL)
		free(prop_name);
	return (res);
}

//
// check_scalable_props(): Checks if the scalable related properties are used
//	properly.  These properties can only be specified for scalable resource.
//	Affinity_timeout, UDP_affinity and Weak_affinity can only be set when
//	with Load_balancing_policy=LB_STICKY or	LB_STICKY_WILD.
//	The input arg r_props is the list of system-defined properties of rs.
//	It has been updated with the user specified property values in scrgadm
//	(in_props) before this function is called.
//
scha_errmsg_t
check_scalable_props(const rgm::idl_nameval_seq_t &in_props,
    boolean_t is_scalable_defined, rgm_property_list_t *r_props,
    scha_errmsg_t *warning_res, const char *rs_name)
{
	char		*val = NULL;
	namelist_t	*lbws = NULL;
	boolean_t	is_lbp_sticky = B_FALSE;
	boolean_t	is_rr_enabled = B_FALSE;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm_property_list_t	*p;

	if ((res = get_prop_value(SCHA_LOAD_BALANCING_POLICY, in_props,
	    &val, SCHA_PTYPE_STRING)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			// It is ok if not specified
			res.err_code = SCHA_ERR_NOERR;

			// This could be the case that Load_balancing_policy
			// is not provided in scrgadm but defaults to LB_STICKY
			// or LB_STICKY_WILD in RTR file, we can find the
			// default in the complete system-defined property
			// list passed by the caller.
			for (p = r_props; p != NULL; p = p->rpl_next) {
				if (strcasecmp(SCHA_LOAD_BALANCING_POLICY,
				    p->rpl_property->rp_key) == 0 &&
				    (strcasecmp(((rgm_value_t *)p->
				    rpl_property->
				    rp_value)->rp_value[0],
				    "LB_STICKY") == 0 ||
				    strcasecmp(((rgm_value_t *)p->rpl_property->
				    rp_value)->rp_value[0],
				    "LB_STICKY_WILD") == 0)) {
					is_lbp_sticky = B_TRUE;
					break;
				}
			}
		} else
			// Return error
			goto finished; //lint !e801
	} else {
		if (!is_scalable_defined) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_PROP_SCALABLE,
			    SCHA_LOAD_BALANCING_POLICY, rs_name);
			goto finished; //lint !e801
		} else {
			if (strcasecmp(val, "LB_STICKY") == 0 ||
			    strcasecmp(val, "LB_STICKY_WILD") == 0)
				is_lbp_sticky = B_TRUE;
		}
	}

	if ((lbws = get_value_string_array(SCHA_LOAD_BALANCING_WEIGHTS,
	    in_props)) != NULL && !is_scalable_defined) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_PROP_SCALABLE,
		    SCHA_LOAD_BALANCING_WEIGHTS, rs_name);
		goto finished; //lint !e801
	}

	if ((res = check_client_affinity_props(in_props, SCHA_AFFINITY_TIMEOUT,
	    is_scalable_defined, is_lbp_sticky, warning_res,
	    rs_name)).err_code != SCHA_ERR_NOERR)
		// Return error
		goto finished; //lint !e801

	if ((res = check_client_affinity_props(in_props, SCHA_UDP_AFFINITY,
	    is_scalable_defined, is_lbp_sticky, warning_res,
	    rs_name)).err_code != SCHA_ERR_NOERR)
		// Return error
		goto finished; //lint !e801

	if ((res = check_client_affinity_props(in_props, SCHA_WEAK_AFFINITY,
	    is_scalable_defined, is_lbp_sticky, warning_res,
	    rs_name)).err_code != SCHA_ERR_NOERR)
		// Return error
		goto finished; //lint !e801

	if ((res = check_client_affinity_props(in_props, SCHA_GENERIC_AFFINITY,
	    is_scalable_defined, is_lbp_sticky, warning_res,
	    rs_name)).err_code != SCHA_ERR_NOERR)
		// Return error
		goto finished; //lint !e801

	if ((res = get_prop_value(SCHA_ROUND_ROBIN, in_props,
	    &val, SCHA_PTYPE_BOOLEAN)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
		// It is ok if not specified
			res.err_code = SCHA_ERR_NOERR;
			for (p = r_props; p != NULL; p = p->rpl_next) {
				if (strcasecmp(SCHA_ROUND_ROBIN,
				    p->rpl_property->rp_key) == 0 &&
				    str2bool(((rgm_value_t *)
				    p->rpl_property->rp_value)
				    ->rp_value[0]) == B_TRUE) {
					is_rr_enabled =
					    str2bool(((rgm_value_t *)
					    p->rpl_property
					    ->rp_value)->rp_value[0]);
					break;
				}
			}
		} else {
			goto finished; //lint !e801
		}
	} else {
		is_rr_enabled = str2bool(val);
	}

	if ((res = check_rr_prop(in_props, SCHA_CONN_THRESHOLD,
	    is_scalable_defined, is_rr_enabled, warning_res,
	    rs_name)).err_code != SCHA_ERR_NOERR)
		// Return error
		goto finished; //lint !e801
finished:
	if (val != NULL)
		free(val);

	if (lbws != NULL)
		rgm_free_nlist(lbws);

	return (res);
}

//
// Client affinity properties can be set only when Scalable is True
// and Load_balancing_policy is LB_STICKY or LB_STICKY_WILD.
//
scha_errmsg_t
check_client_affinity_props(const rgm::idl_nameval_seq_t &in_props,
    char *prop_name, boolean_t is_scalable_defined, boolean_t is_lbp_sticky,
    scha_errmsg_t *warning_res, const char *rs_name)
{
	char		*val = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	if ((res = get_prop_value(prop_name, in_props, &val,
	    SCHA_PTYPE_BOOLEAN)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP)
			// it is ok if not specified
			res.err_code = SCHA_ERR_NOERR;
		else
			// Return error
			goto finished; //lint !e801
	} else {
		if (!is_scalable_defined) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_PROP_SCALABLE, prop_name,
			    rs_name);
			goto finished; //lint !e801
		}
		if (!is_lbp_sticky) {
			rgm_format_errmsg(warning_res, RWM_PROP_AFFINITY,
			    prop_name);
		}
	}

finished:
	if (val != NULL)
		free(val);

	return (res);
}


scha_errmsg_t
check_rr_prop(const rgm::idl_nameval_seq_t &in_props, char *prop_name,
    boolean_t is_scalable_defined, boolean_t is_rr_enabled,
    scha_errmsg_t *warning_res, const char *rs_name)
{
	char		*val = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	if ((res = get_prop_value(prop_name, in_props, &val,
	    SCHA_PTYPE_INT)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP)
			// it is ok if not specified
			res.err_code = SCHA_ERR_NOERR;
		else
			// Return error
			goto finished; //lint !e801
	} else {
		if (!is_scalable_defined) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_PROP_SCALABLE, prop_name,
			    rs_name);
			goto finished; //lint !e801
		}
		if (!is_rr_enabled) {
			rgm_format_errmsg(warning_res, RWM_PROP_RR, prop_name);
		}
	}

finished:
	if (val != NULL)
		free(val);

	return (res);
}

//
// Save incarnation numbers for all of the LNIs in ns.
// If the LN is a global or ZC zone, save the physical node incarnation number;
// otherwise, save the LN incarnation number.
//
void
save_incarnation(LogicalNodeset *ns, incarnation_list_t **head)
{
	rgm::lni_t	lni;
	incarnation_list_t	*list, *old_list;

	old_list = NULL;
	*head = list = NULL;

	lni = 0;
	while ((lni = ns->nextLniSet(lni + 1)) != 0) {
		LogicalNode *ln = lnManager->findObject(lni);
		CL_PANIC(ln != NULL);
		list = (incarnation_list_t *)
		    malloc(sizeof (incarnation_list_t));
		if (ln->isZcOrGlobalZone()) {
			sol::nodeid_t nodeid = ln->getNodeid();
			list->incarnation = (rgm::ln_incarnation_t)
			    Rgm_state->node_incarnations.members[nodeid];
		} else {
			list->incarnation = ln->getIncarnation();
		}
		list->lni = lni;
		list->next = NULL;
		if (*head == NULL)
			*head = list;
		if (old_list != NULL)
			old_list->next = list;
		old_list = list;
	}
}

//
// Check whether the incarnation of the specified LNI has been changed.
// If it has been, it means the node died and rejoined the cluster.
// The function returns TRUE in this case. Otherwise, return FALSE.
//
boolean_t
incarnation_changed(rgm::lni_t n, incarnation_list_t *incar_list)
{
	incarnation_list_t	*ptr;
	rgm::ln_incarnation_t	incarn;
	LogicalNode *ln = lnManager->findObject(n);
	CL_PANIC(ln != NULL);

	if (ln->isZcOrGlobalZone()) {
		incarn = (rgm::ln_incarnation_t)
		    Rgm_state->node_incarnations.members[ln->getNodeid()];
	} else {
		incarn = ln->getIncarnation();
	}

	for (ptr = incar_list; ptr; ptr = ptr->next) {
		if (n == ptr->lni) {
			if (incarn == ptr->incarnation) {
				return (B_FALSE);
			} else {
				return (B_TRUE);
			}
		}
	}
	return (B_FALSE);
}

// Free incarnation list we saved earlier.
void
free_incarnation(incarnation_list_t *incar_list)
{
	incarnation_list_t	*ptr, *ptr_tmp;

	for (ptr = incar_list; ptr; ptr = ptr_tmp) {
		ptr_tmp = ptr->next;
		free(ptr);
	}
}

//
// check_rs_defaults() - check whether the values of the
//	properties are set on scrgadm command line
//	when the properties do not have defaults in the RT paramtable
//
//	props and ext_props are the properties that specified in
//	scrgadm -a -j -y|-x
//
//	props contains system-defined and optional system-defined properties.
//	ext_props contains extension properties.
//
//	Loop through each property in RT paramtable,  if default_isset
//	flag is set, this property has default values.  Otherwise,
//	check to see whether the property values are set on the scrgadm
//	command line.  If it is not, return error.
//
//	This routine only called by idl_scrgadm_rs_add routine.
//
scha_errmsg_t
check_rs_defaults(const rgm::idl_nameval_seq_t &props,
    const rgm::idl_nameval_node_seq_t &ext_props, rgm_param_t **paramtable)
{

	uint_t		i, len;
	boolean_t	found = B_FALSE;
	rgm_param_t	**param;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	//
	// loop through the properties in the paramtable
	//
	for (param = paramtable; *param; param++) {
		len = 0;
		found = B_FALSE;

		//
		// skip the property which has the defaut values in the
		// RT paramtable
		//
		if ((*param)->p_default_isset == B_TRUE) {
			found = B_TRUE;
			continue;
		}

		//
		// check optional system-defined properties
		//
		if (!(*param)->p_extension) {
			len = props.length();

			//
			// Loop through props
			//
			for (i = 0; i < len; i++) {
				//
				// if found the property name in the props,
				// the values are set on scrgadm
				//
				if (strcasecmp((*param)->p_name,
				    props[i].idlstr_nv_name) == 0) {
					found = B_TRUE;
					break;
				}
			}
		} else {
			//
			// check extension properies
			//
			len = ext_props.length();

			//
			// Loop through the list of updated system-defined
			// properties in props.
			//
			for (i = 0; i < len; i++) {
				//
				// if found the property name in the props
				// the values are set on scrgadm
				//
				if (strcasecmp((*param)->p_name,
				    ext_props[i].idlstr_nv_name) == 0) {
					found = B_TRUE;
					break;
				}
			}
		}
		if (!found) {
			ucmm_print("check_rs_defaults()",
			    NOGET("property <%s> values are missing\n"),
			    (*param)->p_name);
			rgm_format_errmsg(&res, REM_PROP_NO_DEFAULT,
			    (*param)->p_name);
			res.err_code = SCHA_ERR_NODEFAULT;
			goto finished; //lint !e801
		}
	}

finished:

	return (res);
}

//
// is_extprop()
//
// If the specified property is in the paramtable and is an extension
//	property, return B_TRUE.
// Otherwise, return B_FALSE
//
static boolean_t
is_extprop(char *prop_name, rgm_param_t **paramtable)
{
	rgm_param_t	**param;

	for (param = paramtable; *param; param++) {
		//
		// If the property is found in paramtable,
		// check whether the user specify the right option(-x) for this
		// property in scrgadm
		//
		if (strcasecmp((*param)->p_name, prop_name) == 0) {
			if ((*param)->p_extension)
				return (B_TRUE);
		}
	}
	return (B_FALSE);
}

static boolean_t
is_pernode_prop(char *prop_name, rgm_param_t **paramtable)
{
	rgm_param_t	**param;

	for (param = paramtable; *param; param++) {
		//
		// If the property is found in paramtable,
		// check if its a per-node extension property
		//
		if (strcasecmp((*param)->p_name, prop_name) == 0) {
			if ((!(*param)->p_extension) && (*param)->
			    p_per_node) {
				//
				// Should never reach here. Per node properties
				// are restricted to Extension property only.
				//
				ASSERT(0);
			}
			return ((*param)->p_per_node);
		}
	}
}



//
// is_sysprop()
//
// If the specified property is in the paramtable and is a system-define or
//	an optional system-defined property, return B_TRUE
// Otherwise, return B_FALSE
//
static boolean_t
is_sysprop(char *prop_name, rgm_param_t **paramtable)
{
	rgm_param_t	**param;

	for (param = paramtable; *param; param++) {
		//
		// If the property is found in paramtable,
		// check whether the user specify the right option(-y) for this
		// property in scrgadm
		//
		if (strcasecmp((*param)->p_name, prop_name) == 0) {
			if (!(*param)->p_extension)
				return (B_TRUE);
		}
	}
	return (B_FALSE);
}

// is_upgrade(...)
// scans through the list of system properties being updated to look for the
// existence of the "Type_version" property. The existence of this property
// indicates that this update is actually an "RT Upgrade".
//

static boolean_t
is_upgrade(const rgm::idl_nameval_seq_t &props)
{
	uint_t i, len = 0;

	len = props.length();

	ucmm_print("idl_scrgadm_rs_update_prop()",
		    NOGET("Inside is_upgrade."));

	for (i = 0; i < len; i++) {
		if (strcasecmp(props[i].idlstr_nv_name, "Type_version") == 0)
			return (B_TRUE);
	}

	return (B_FALSE);
}

// is_dependency_updated
// scans through the system property list to look for existence of
// any of the 4 resource dependencies namely "Resource_dependencies",
// "Resource_dependencies_weak","Resource_dependencies_restart" and
// "Resource_dependencies_offline_restart" or the
// "Network_resources_used.

static boolean_t
is_dependency_updated(const rgm::idl_nameval_seq_t &props,
	boolean_t *strong_dep, boolean_t *weak_dep, boolean_t *restart_dep,
	boolean_t *off_restart_dep)
{
	uint_t i, len = 0;
	boolean_t NRU_updated = B_FALSE;

	len = props.length();

	for (i = 0; i < len; i++) {
		if (strcasecmp(props[i].idlstr_nv_name,
		    "Resource_dependencies") == 0)
		    *strong_dep = B_TRUE;
		if (strcasecmp(props[i].idlstr_nv_name,
		    "Resource_dependencies_weak") == 0)
		    *weak_dep = B_TRUE;

		if (strcasecmp(props[i].idlstr_nv_name,
		    "Resource_dependencies_restart") == 0)
		    *restart_dep = B_TRUE;

		if (strcasecmp(props[i].idlstr_nv_name,
		    "Resource_dependencies_offline_restart") == 0)
		    *off_restart_dep = B_TRUE;

		if (strcasecmp(props[i].idlstr_nv_name,
		    "Network_resources_used") == 0)
			NRU_updated = B_TRUE;
	}

	return ((boolean_t)(*strong_dep || *weak_dep || *restart_dep ||
		NRU_updated || *off_restart_dep));
}

static boolean_t
is_resource_offline(rlist_p_t rp)
{

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {

		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);

		rgm::lni_t nodeid = lnNode->getLni();

		if (!lnNode->isInCluster()) {
			continue;
		}
		if (rp->rl_xstate[nodeid].rx_state != rgm::R_OFFLINE)
			return (B_FALSE);
	}
	return (B_TRUE);
}

//
// is_r_scalable_prop_defined - checks whether Scalable system-defined
// property is defined for the resource type of this resource.
// Returns true if the Scalable system-defined property exists otherwise,
// returns false.
//
static boolean_t
is_r_scalable_prop_defined(rgm_resource_t *r)
{
	rgm_property_list_t	*pp;

	//
	// system-defined properties are stored in r->r_properties.
	// The value of the property is stored in ASCII form.
	//
	pp = rgm_find_property_in_list(SCHA_SCALABLE, r->r_properties);

	//
	// If cannot find the Scalable property, it means that this
	// property is not defined in the RT of this resource.
	//
	if (pp == NULL) {
		return (B_FALSE);
	} else {
		return (B_TRUE);
	}
}

static void switch_modify(std::map<rgm::lni_t, scha_switch_t> &swtch,
    scha_switch_t flag, LogicalNodeset *ns)
{
	rgm::lni_t lni = 0;
	while ((lni = ns->nextLniSet(lni + 1)) != 0) {
		swtch[lni] = flag;
	}
}

//
// create_upgrade_rtname(...)
// This routine returns the name of the "upgrade RT" by first stripping off
// the version string from the name of the "current RT" and then concatenating
// the value of the "Type_version" property from the "update args".
// This routine assumes that the "Type_version" property is among the
// properties being updated and makes no attempt to re-verify that. This is OK
// since this routine is only called from inside the "if" statement which is
// predicated on the existence of the "Type_version" property in update_args.
//

static scha_errmsg_t
create_upgrade_rtname(const rgm::idl_nameval_seq_t &props,
    name_t current_rt, name_t *upgrade_rt)
{

	uint_t i, len = 0;
	name_t current_rt_dup = NULL;
	name_t upgrade_rt_version = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *lasts = NULL;

	len = props.length();

	for (i = 0; i < len; i++) {
		if (strcasecmp(props[i].idlstr_nv_name, SCHA_TYPE_VERSION)
		    == 0) {
			if ((upgrade_rt_version =
			    strdup_nocheck(props[i].nv_val[0])) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				return (res);
			}
			break;
		}
	}
	CL_PANIC(upgrade_rt_version != NULL);

	if ((current_rt_dup = strdup_nocheck(current_rt)) == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		free(upgrade_rt_version);
		return (res);
	}

	(void) strtok_r(current_rt_dup, ":", &lasts);

	ucmm_print("create_upgrade_rtname()",
	    NOGET("current RT w/o version = %s"), current_rt_dup);

	// The +2 below is for the ":" and the null character.
	if ((*upgrade_rt = (name_t)malloc_nocheck(strlen(current_rt_dup) +
	    strlen(upgrade_rt_version) + 2)) == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		free(upgrade_rt_version);
		free(current_rt_dup);
		return (res);
	}

	if (upgrade_rt_version[0] == '\0') {
		(void) sprintf(*upgrade_rt, "%s", current_rt_dup);
	} else {
		(void) sprintf(*upgrade_rt, "%s:%s", current_rt_dup,
		    upgrade_rt_version);
	}

	free(upgrade_rt_version);
	free(current_rt_dup);

	return (res);
}

//
// is_upgrade_allowed
// determines if the given upgrade is allowed by the "upgrade_from"
// directives in the destination RTR file, or by the "downgrade_to" directives
// in the source RTR file. The destination RT is pointed to by "upgrade_rt";
// the source RT is pointed by "source_rt"; the resource being updated/upgraded
// is pointed to by "rp"; upgarde_allowed acts as the "out" parameter.
//
static scha_errmsg_t
is_upgrade_allowed(rgm_rt_t *upgrade_rt, rgm_rt_t *source_rt, rlist_p_t rp,
    boolean_t *upgrade_allowed)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm_rt_upgrade_t *upgrade_from = NULL;
	rgm_tune_t	tunablity;
	rglist_p_t	rgp = NULL;
	char		*rt_basename = NULL;
	char		*lasts = NULL;
	sol::nodeid_t	nodeid;
	LogicalNodeset *switch_ns = NULL;


	ucmm_print("is_upgrade_allowed()",
	    NOGET("source RT name = %s"), source_rt->rt_name);

	if ((rt_basename = strdup_nocheck(upgrade_rt->rt_name)) == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	(void) strtok_r(rt_basename, ":", &lasts);

	// search through the linked list of rt_upgrade_from structures
	// to see if there exists a directive for the "source RT version",
	// i.e. the RT version being upgraded from.
	for (upgrade_from = upgrade_rt->rt_upgrade_from; upgrade_from != NULL;
	    upgrade_from = upgrade_from->rtu_next) {
		if ((strcasecmp(upgrade_from->rtu_version,
		    rp->rl_ccrdata->r_type_version)) == 0)
			break;
	}

	//
	// Notice that with both upgrade_from and downgrade_from (in
	// different RTR files though) there are 4 possible cases:
	//	   upgrade_from | downgrade_to 	| tunablity
	//	   --------------------------------------
	// case#1  present	| absent	| (from dest RT)
	// 			|		| upgrade_from->rtu_tunability
	// case#2 absent	| present	| (from src RT)
	// 			|		| downgrade_to->rtu_tunability
	// case#3 absent	| absent	| TUNE_WHEN_UNMANAGED
	// case#4 present	| present	| N.A.
	//
	// Notice that case #4 can not occur, i.e. we can not have a matching
	// upgrade_from and downgrade_to directive. We make no specific
	// attempt to verify this; we simply look for the upgrade_from and
	// downgrade_to directives in that order and proceed with the first
	// match found.
	//

	if (upgrade_from == NULL) {
/*
		// No upgrade_from directive found for the "source RT version."
		// Next, search through the linked list of rt_downgrade_to
		// structures in the source_rt to see if there exists a
		// downgrade_to directive for the "upgrade RT version",
		// i.e. the RT version being downgraded to.
		for (downgrade_to = source_rt->rt_downgrade_to;
		    downgrade_to != NULL;
		    downgrade_to = downgrade_to->rtu_next) {
			if ((strcasecmp(downgrade_to->rtu_version,
			    upgrade_rt->rt_version)) == 0)
				break;
		}

		if (downgrade_to == NULL) {
			// case#3 above
			// We found neither a matching "upgrade_from"
			// directive, nor a matching "downgrade_to" directive.
			// Therefore assign the default value of
			// "when unmanaged".
*/
		tunablity = TUNE_WHEN_UNMANAGED;
/*
		} else {
			// case #2 above
			// A matching "upgrade_to" directive was not found,
			// but a matching "downgrade_to" directive WAS found.
			// Therefore, use the tunability criteria from the
			// downgrade_to structure.
			tunablity = downgrade_to->rtu_tunability;
		}
*/
	} else {
		// case #1 above
		// A matching "downgrade_to" directive was found. No need to
		// to search further.
		tunablity = upgrade_from->rtu_tunability;
	}

	// determine whether the upgrade/downgrade is allowed based on the
	// tunability directive and the current state of the
	// resource.
	switch (tunablity) {
	case TUNE_AT_CREATION:
		// TUNE_AT_CREATION means that the upgrade is not allowed.
		*upgrade_allowed = B_FALSE;
		res.err_code = SCHA_ERR_UPDATE;
		rgm_format_errmsg(&res, REM_UPGRADE_AT_CREATION,
		    rp->rl_ccrdata->r_name, upgrade_rt->rt_version,
		    rt_basename);
		break;
	case TUNE_ANYTIME:
		// TUNE_ANYTIME means that the upgrade is always allowed.
		*upgrade_allowed = B_TRUE;
		break;
	case TUNE_WHEN_UNMONITORED:
		// upgrade is allowed if the the R is
		// "unmonitored" on all nodes.
		rgp = rgname_to_rg(rp->rl_ccrdata->r_rgname);
		switch_ns = get_logical_nodeset_from_nodelist
		    (rgp->rgl_ccr->rg_nodelist);
		*upgrade_allowed = B_TRUE;
		if (is_resource_offline(rp))
			break;
		// are we sure the above check is not required for
		// TUNE_WHEN_DISABLED
		for (nodeid = switch_ns->nextLniSet(1); nodeid != 0;
		    nodeid = switch_ns->nextLniSet(nodeid + 1)) {
			if (((rgm_switch_t *)rp->rl_ccrdata->
			    r_monitored_switch)->
			    r_switch[nodeid] == SCHA_SWITCH_ENABLED) {
				*upgrade_allowed = B_FALSE;
				res.err_code = SCHA_ERR_UPDATE;
				rgm_format_errmsg(&res,
				    REM_UPGRADE_WHEN_UNMONITORED,
				    rp->rl_ccrdata->r_name,
				    upgrade_rt->rt_version,
				    rt_basename);
				break;
			}
		}
		break;
	case TUNE_WHEN_UNMANAGED:
		// upgrade is allowed if the RG is "unmanaged".
		rgp = rgname_to_rg(rp->rl_ccrdata->r_rgname);
		if (rgp->rgl_ccr->rg_unmanaged == B_TRUE)
			*upgrade_allowed = B_TRUE;
		else {
			*upgrade_allowed = B_FALSE;
			res.err_code = SCHA_ERR_UPDATE;
			rgm_format_errmsg(&res, REM_UPGRADE_WHEN_UNMANAGED,
			    rp->rl_ccrdata->r_name, upgrade_rt->rt_version,
			    rt_basename, rp->rl_ccrdata->r_rgname);
		}
		break;
	case TUNE_WHEN_OFFLINE:
		// upgrade is allowed if the R is offline on all nodes
		// (since offline is a per node state).
		if (is_resource_offline(rp))
			*upgrade_allowed = B_TRUE;
		else {
			*upgrade_allowed = B_FALSE;
			res.err_code = SCHA_ERR_UPDATE;
			rgm_format_errmsg(&res, REM_UPGRADE_WHEN_OFFLINE,
			    rp->rl_ccrdata->r_name, upgrade_rt->rt_version,
			    rt_basename);
		}
		break;
	case TUNE_WHEN_DISABLED:
		rgp = rgname_to_rg(rp->rl_ccrdata->r_rgname);
		switch_ns = get_logical_nodeset_from_nodelist
		    (rgp->rgl_ccr->rg_nodelist);
		*upgrade_allowed = B_TRUE;
		for (nodeid = switch_ns->nextLniSet(1); nodeid != 0;
		    nodeid = switch_ns->nextLniSet(nodeid + 1)) {
			if (((rgm_switch_t *)rp->rl_ccrdata->r_onoff_switch)
			    ->r_switch[nodeid] == SCHA_SWITCH_ENABLED) {
				*upgrade_allowed = B_FALSE;
				res.err_code = SCHA_ERR_UPDATE;
				rgm_format_errmsg(&res,
				    REM_UPGRADE_WHEN_DISABLED,
				    rp->rl_ccrdata->r_name,
				    upgrade_rt->rt_version,
				    rt_basename);
				break;
			}
		}
		break;

	case TUNE_NONE:
		// TUNE_NONE is actually an unacceptable value for the
		// "upgrade-from" directive. The RTR file parser should
		// have already checked for it and rejected the RTR file.
		// fall through.
	default:
		// should not happen; abort node
		sc_syslog_msg_handle_t handle;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A fatal internal error has occurred in the RGM.
		// @user_action
		// Since this problem might indicate an internal logic error
		// in the rgmd, save a copy of the /var/adm/messages files on
		// all nodes, and the output of clresourcetype show -v,
		// clresourcegroup show -v +, and clresourcegroup status +.
		// Report the problem to your authorized Sun service provider.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR: Invalid upgrade-from tunablity "
		    "flag <%d>; aborting node", tunablity);
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}

	free(rt_basename);

	return (res);
}

//
// delete_properties
// called from idl_scrgadm_rs_update_prop if the update is an RT upgrade.
// This routine deletes the properties from the resource structure which
// are not there in the new paramtable
//
static void
delete_properties(rgm_resource_t *rs_conf, rgm_param_t **paramtable)
{
	rgm_property_list_t *curr_prop;
	rgm_property_list_t *prev_prop;

	curr_prop = rs_conf->r_properties;
	prev_prop = NULL;
	while (curr_prop != NULL) {
		// is_sysprop tells us whether the curr_prop exists in the
		// paramtable or not. Therefore, we call the delete curr_prop
		// when is_sysprop returns negative.
		if (!is_sysprop(curr_prop->rpl_property->rp_key, paramtable)) {
			if (prev_prop == NULL) {
				// The property being deleted is the first
				// property in the list
				rs_conf->r_properties = curr_prop->rpl_next;
				rgm_free_property(curr_prop->rpl_property);
				free(curr_prop);
				curr_prop = rs_conf->r_properties;
				// at this point both rs_conf->r_properties
				// and curr_prop point to the first entry
				// in the list. It is almost like "starting
				// over" with this while loop. In the next
				// iteration things might change.
			} else {
				// The property being deleted is not the first
				// property. Therefore, rs_conf->r_properties
				// (which is the "head" of the list) does not
				// need to change in subsequent iterations of
				// the while loop. prev_prop points to either
				// the first or later entries in the list,
				// while curr_prop points to the second or
				// later entries in the list.
				prev_prop->rpl_next = curr_prop->rpl_next;
				// notice that prev_prop hasn't changed.
				// We have only adjusted the rpl_next pointer
				// to point to the entry after curr_prop,
				// which might very well be NULL.
				rgm_free_property(curr_prop->rpl_property);
				free(curr_prop);
				curr_prop = prev_prop->rpl_next;
				// now that we have deleted the matching
				// entry and updated the pointers, curr_prop
				// and prev_prop once again have a "distance"
				// of one entry between them. Notice that in
				// this iteration, prev_prop didn't move.
				// At this point curr_prop might very well be
				// NULL, which would imply that the entry we
				// just deleted was the last entry in the
				// list and we're done with this while loop.
			}
		} else {
			// No match found. This is easy... all we have to do
			// is update both prev_prop and curr_prop
			prev_prop = curr_prop;
			curr_prop = curr_prop->rpl_next;
		}
	}

	// Now iterate over the extension properties
	curr_prop = rs_conf->r_ext_properties;
	prev_prop = NULL;
	while (curr_prop != NULL) {
		// is_extprop tells us whether the curr_prop exists in the
		// paramtable or not. Therefore, we delete curr_prop
		// when is_extprop returns negative.
		if (!is_extprop(curr_prop->rpl_property->rp_key, paramtable)) {
			if (prev_prop == NULL) {
				// The property being deleted is the first
				// property in the list
				rs_conf->r_ext_properties =
				    curr_prop->rpl_next;
				rgm_free_property(curr_prop->rpl_property);
				free(curr_prop);
				curr_prop = rs_conf->r_ext_properties;
				// at this point both rs_conf->r_ext_properties
				// and curr_prop point to the first entry
				// in the list. It is almost like "starting
				// over" with this while loop. In the next
				// iteration things might change.
			} else {
				// The property being deleted is not the first
				// property.
				// Therefore, rs_conf->r_ext_properties
				// (which is the "head" of the list) does not
				// need to change in subsequent iterations of
				// the while loop. prev_prop points to either
				// the first or later entries in the list,
				// while curr_prop points to the second or
				// later entries in the list.
				prev_prop->rpl_next = curr_prop->rpl_next;
				// notice that prev_prop hasn't changed.
				// We have only adjusted the rpl_next pointer
				// to point to the entry after curr_prop,
				// which might very well be NULL.
				rgm_free_property(curr_prop->rpl_property);
				free(curr_prop);
				curr_prop = prev_prop->rpl_next;
				// now that we have deleted the matching
				// entry and updated the pointers, curr_prop
				// and prev_prop once again have a "distance"
				// of one entry between them. Notice that in
				// this iteration, prev_prop didn't move.
				// At this point curr_prop might very well be
				// NULL, which would imply that the entry we
				// just deleted was the last entry in the
				// list and we're done with this while loop.
			}
		} else {
			// No match found. This is easy... all we have to do
			// is update both prev_prop and curr_prop
			prev_prop = curr_prop;
			curr_prop = curr_prop->rpl_next;
		}
	}
}

void
print_map(rgm_value_t *p)
{
	std::map<rgm::lni_t, char*>::iterator it;
	for (it = p->rp_value.begin(); it != p->rp_value.end();
	    it++) {
		sc_syslog_msg_handle_t handle;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Oh lni map rp_value[%d] = %s: ",
		    it->first, it->second);
		sc_syslog_msg_done(&handle);
	}
	std::map<std::string, char *>::iterator gt;
	for (gt = p->rp_value_node.begin(); gt != p->rp_value_node.end();
	    gt++) {
		sc_syslog_msg_handle_t handle;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "Oh lni map rp_value_node[%s] = %s: ",
		    gt->first.data(), gt->second);
		sc_syslog_msg_done(&handle);
	}
}
#endif
