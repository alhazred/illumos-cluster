/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 *	Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 *	Use is subject to license terms.
 */

/*
 * rgm_confcdb.cc
 *
 */

#pragma	ident	"@(#)rgm_confcdb.cc	1.68	08/05/20 SMI"

#include <sys/utsname.h>
#include <sys/time.h>
#include <stdio.h>

#include <rgm_proto.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <errno.h>
#include <string.h>

static char Hostname[SYS_NMLN];		/* Cache the value here */

/*
 * char *getlocalhostname(void)
 * Returns the physical hostname of the local node.
 * Useful for making local RPC calls
 */
char *
getlocalhostname()
{
	sc_syslog_msg_handle_t handle;
	struct  utsname utsn;

	if (!Hostname[0]) {	/* If not cached */

		// get node name
		if (uname(&utsn) == -1) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// A uname(2) system call failed. The rgmd will
			// produce a core file and will force the node to halt
			// or reboot to avoid the possibility of data
			// corruption.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: uname: %s (UNIX error %d)",
			    strerror(errno), errno);
			sc_syslog_msg_done(&handle);
			abort();
			/* NOTREACHED */
		}
		(void) strcpy(Hostname, utsn.nodename);

	}
	return (Hostname);
}


/*
 * Initialize configuration information structures.
 */
void
get_cluster_info()
{
	/*
	 * The rgmd uses two steps - the first to ensure that all nodes
	 * get the same cluster membership and incarnation numbers and
	 * the second to execute the uninterruptible reconfiguration
	 * function rgm_reconfig().
	 *
	 * RGM does not use any timeouts for the transitions.
	 * (A timeout value of 0 indicates no timeout to UCMM.)
	 * The transitions can take an unbounded amount of time
	 * if the system is low on memory and the RGMD is swapped
	 * out. This can delay failovers, but that is the price to
	 * be paid when systems are run close to maximum load.
	 */
	cbinfo.num_steps = 2;
	cbinfo.step_timeouts.length(cbinfo.num_steps);
	cbinfo.return_timeout = 0;
	cbinfo.stop_timeout = 0;
	cbinfo.abort_timeout = 0;
	for (uint_t i = 1; i <= cbinfo.num_steps; i++)
		cbinfo.step_timeouts[i-1] = 0;

	/* cache local hostname */
	(void) getlocalhostname();
}
