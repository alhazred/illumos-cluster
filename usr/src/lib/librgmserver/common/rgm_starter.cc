/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * rgm_starter.cc
 *
 *	rgm starter functions.
 */


#pragma ident	"@(#)rgm_starter.cc	1.19	08/10/21 SMI"

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/clconf_int.h>

#include <cmm/cmm_ns.h>
#include <h/membership.h>
#include <cmm/membership_client.h>
#include <nslib/ns_interface.h>
#include <nslib/ns.h>


#include <sys/resource.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/procfs.h>
#ifndef linux
#include <sys/int_types.h>
#endif
#include <orb/invo/common.h>
#ifndef EUROPA_FARM
#include <cmm/cmm_ns.h>
#include <cmm/ff_signal.h>
#endif
#include <rgm_proto.h>
#include <libintl.h>
#include <new.h>
#include <rgm/rgm_msg.h>
#ifndef EUROPA_FARM
#include <rgm/rgm_cnfg.h>
#include <cm_callback_impl.h>
#endif
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm/scutils.h>

#ifdef EUROPA_FARM
#include <sys/st_failfast.h>
#endif
#include <syslog.h>
#ifndef EUROPA_FARM
#define	RGM_STARTER_DOOR_FILE_NAME	"/var/cluster/rgm/.rgm_starter"
#endif

/* Zone support */
#include <rgm_logical_node_manager.h>
#include <rgm_logical_node.h>

#include <rgmx_hook.h>

#include <sys/sol_version.h>

#include <h/naming.h>
#include <nslib/ns.h>
#include <rgm/sczones.h>

#define	debuglog syslog
extern int startrgmd();

void cluster_reconfig_for_starter(
    const char *cluster, const char *membership_namep,
    const cmm::membership_t membership, const cmm::seqnum_t seqnum);

extern char RGMD_SYSLOG_TAG[ZONENAME_MAX + 18];

/* global variables */

boolean_t Debugflag = B_FALSE;
boolean_t System_dumpflag = B_FALSE;
boolean_t Method_dumpflag = B_TRUE;


static void parse_cmd_args(int argc, char **argv);
static void usage(void);
static void init_signal_handlers(void);
static void sig_handler(int sig);
static void set_signal_handler(int sig);
static void rgm_register_cluster_callbacks();
pid_t startnewrgmd(int i);
#if (SOL_VERSION >= __s10)
static int update_membership(int i);
static void register_membership(char *zone, mem_callback_funcp_t funcp);
static void unregister_membership(const char *zone);
static void add_cluster(const char *cluster);
static void remove_cluster(const char *cluster);
#endif

static char *last_path_elem(char *path);

#ifndef EUROPA_FARM
static ff::failfast_var		death_ff_v;
#endif


static sc_syslog_msg_handle_t handle;


#if (SOL_VERSION >= __s10)
os::mutex_t rgm_death_lock;
os::condvar_t rgm_death_cv;
pid_t dead_rgmd_pid = 0;
char dead_rgmd_cluster[ZONENAME_MAX] = {'\0'};

#define	PMMD_DOOR_FILE_NAME	"/var/cluster/.pmmd"

// global variable to store the number of zone clusters.
int NUMCZS;

// global variable to store the info of zone clusters.
cluster_info_t *CLUSTERINFO = NULL;

#define	zone_is_up(i)	(CLUSTERINFO[i].node_incarnation != INCN_UNKNOWN)
#define	need_new_rgm(i)	(CLUSTERINFO[i].pid_incarnation == 0 && \
    CLUSTERINFO[i].rgm_incarnation < CLUSTERINFO[i].node_incarnation)
#define	pid_matches(pid, i)	(CLUSTERINFO[i].pid_incarnation == pid)
os::mutex_t ccr_change_lock;
os::condvar_t ccr_change_cv;
enum ccr_change_t {ZONE_CLUSTER_ADDED, ZONE_CLUSTER_REMOVED};
ccr_change_t ccr_change;
char cluster_name[ZONENAME_MAX];

static void *
ccr_change_thread(void *)
{
	ccr_change_lock.lock();
	while (1) {
		ccr_change_cv.wait(&ccr_change_lock);
		if (ccr_change == ZONE_CLUSTER_ADDED) {
			add_cluster(cluster_name);
		} else if (ccr_change == ZONE_CLUSTER_REMOVED) {
			remove_cluster(cluster_name);
		} else {
			// Should not happen
			abort();
		}
	}
	return (NULL);
}

static void
start_ccr_change_thread()
{
	if (thr_create(NULL, 0, ccr_change_thread,
	    NULL, THR_BOUND, NULL) != 0) {
		//
		// SCMSGS
		// @explanation
		// Could not start ccr change thread.
		// This may be due to inadequate memory on the system.
		// @user_action
		// Add more memory to the system. If that does not resolve the
		// problem, contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: could not start ccr change thread"));
		abort();
	}
}
#endif


// Update the membership information for a given cluster.
#if SOL_VERSION >= __s10
static int
update_membership(int i)
{
	ucmm_print("update_membership", "i = %d, cl_name = %s\n",
	    i, CLUSTERINFO[i].clustername);
	static local_nodeid = clconf_get_local_nodeid();
	Environment e;
	CORBA::Exception *exp;

	// request a snapshot of membership and seqnum
	if (membership_api_available_for_rgmd_starter()) {
		cmm::membership_t membership;
		cmm::seqnum_t seqnum;
		membership::api_var membership_api_v =
		    cmm_ns::get_membership_api_ref();
		if (CORBA::is_nil(membership_api_v)) {
			abort();
		}
		membership_api_v->get_cluster_membership(
		    membership, seqnum, CLUSTERINFO[i].clustername, e);
		exp = e.exception();
		if (exp != NULL) {
			abort();
		}
		CLUSTERINFO[i].node_incarnation =
		    membership.members[local_nodeid];
	} else {
		CL_PANIC(os::strcmp(CLUSTERINFO[i].clustername,
		    GLOBAL_ZONENAME) == 0);
		cmm::control_var cmm_ctrl_v = cmm_ns::get_control();
		if (CORBA::is_nil(cmm_ctrl_v)) {
			abort();
		}
		cmm::cluster_state_t cmm_state;
		cmm_ctrl_v->get_cluster_state(cmm_state, e);
		exp = e.exception();
		if (exp != NULL) {
			abort();
		}
		CLUSTERINFO[i].node_incarnation =
		    cmm_state.members.members[local_nodeid];
	}

	ucmm_print("update_membership",
	    "got membership for cluster = %s, local incn %lu\n",
	    CLUSTERINFO[i].clustername, CLUSTERINFO[i].node_incarnation);
	return (0);
}


// initialise the zone cluster info with the global/base cluster.
static void
rgm_init_cluster_info() {
	CLUSTERINFO = (cluster_info_t *)calloc(1, sizeof (cluster_info_t));
	CLUSTERINFO[0].clustername = strdup(GLOBAL_ZONENAME);
	CLUSTERINFO[0].node_incarnation = INCN_UNKNOWN;
	CLUSTERINFO[0].pid_incarnation = 0;
	CLUSTERINFO[0].rgm_incarnation = INCN_UNKNOWN;
	NUMCZS = 1;
	if (membership_api_available_for_rgmd_starter()) {
		register_membership(CLUSTERINFO[0].clustername,
		    cluster_reconfig_for_starter);
	}
}

// helper function for printing the cluster information.
static void
print_cluster_info() {
	int i;
	debuglog(LOG_ERR, "numczs:%d", NUMCZS);
	for (i = 0; i < NUMCZS; i++) {
		debuglog(LOG_ERR, "clustername:%s:",
		    CLUSTERINFO[i].clustername ?
		    CLUSTERINFO[i].clustername : "null");
	}
}
//
// routine to add a cluster. This is called whenever a
// new zone cluster is created.
//
static void
add_cluster(const char *cluster) {
	CL_PANIC(os::strcmp(cluster, GLOBAL_ZONENAME) != 0);
	// debuglog(LOG_ERR, "before");
	// print_cluster_info();
	CLUSTERINFO = (cluster_info_t *)realloc(CLUSTERINFO,
	    (NUMCZS + 1) * sizeof (cluster_info_t));
	memset(&CLUSTERINFO[NUMCZS], '\0', sizeof (cluster_info_t));
	CLUSTERINFO[NUMCZS].clustername = strdup(cluster);
	NUMCZS++;
	// debuglog(LOG_ERR, "after");
	// print_cluster_info();
	register_membership((char *)cluster,
	    cluster_reconfig_for_starter);
}
//
// routine to remove a cluster. This is called whenevr
// a zone cluster is deleted.
//
static void
remove_cluster(const char *cluster) {
	CL_PANIC(os::strcmp(cluster, GLOBAL_ZONENAME) != 0);
	// debuglog(LOG_ERR, "after");
	// print_cluster_info();
	uint_t i;
	for (i = 0; i < NUMCZS; i++) {
		if (strcmp(CLUSTERINFO[i].clustername, cluster) == 0) {
			if (i < (NUMCZS - 1)) { // not last entry
				free(CLUSTERINFO[i].clustername);
				memcpy(&CLUSTERINFO[i],
				    &CLUSTERINFO[NUMCZS - 1],
				    sizeof (cluster_info_t));
			}
			break;

		}
	}
	NUMCZS--;
	CLUSTERINFO = (cluster_info_t *)realloc(
	    CLUSTERINFO, NUMCZS * sizeof (cluster_info_t));
	// debuglog(LOG_ERR, "after");
	// print_cluster_info();
	unregister_membership(cluster);
	//
	// we need to make sure that the rgmd is dead by now.
	// unless all methods have been quiesced, we can't remove the cluster
	// the rgms themselves will need to listen to the callback, and if their
	// cluster is removed, they should end themselves
	//
}

//
// During rolling upgrade version commit, the upgrade callbacks happen.
// And that is when this function is called.
// The only task that rgmd starter has to do here is to register
// for base cluster membership callbacks.
//
void
rgm_starter_upgrade() {
	CL_PANIC(NUMCZS == 1);
	register_membership(GLOBAL_ZONENAME, cluster_reconfig_for_starter);
}


// routine to update cluster info.
static void
rgm_update_cluster_info() {
	// debuglog(LOG_ERR, "\nin update_cluster_info: initial config");
	// print_cluster_info();
	uint_t i, j, numczs;
	char **czlist = clconf_cluster_get_cluster_names();
	for (i = 0; czlist[i] != NULL; i++) {
		// debuglog(LOG_NOTICE, "get_cluster_names_returned:%d:%s", i,
		//    czlist[i]);
	}
	numczs = i + 1; // get_cluster_names doesn't return global cluster
	// debuglog(LOG_ERR, "new number of czs:%d", numczs);
	for (i = 0; i < NUMCZS; i++) {
		if (strcmp(GLOBAL_ZONENAME, CLUSTERINFO[i].clustername) == 0)
			continue;
		for (j = 0; czlist[j] != NULL; j++) {
			if (strcmp(CLUSTERINFO[i].clustername,
			    czlist[j]) == 0) {
				//
				// this is an existing cluster. remove entry
				// from czlist, so that we don't create it again
				//
				czlist[j][0] = '\0';
				break;
			}
		}
		if (czlist[j] == NULL) {
			//
			// this cluster name is not in latest list. This has
			// been deleted. Remove it from our list.
			//
			free(CLUSTERINFO[i].clustername);
			CLUSTERINFO[i].clustername = NULL;
			// unregister membership?
		}
	}
	// now we have invalidated all non-existing clusters. shrink the list
	j = NUMCZS - 1;
	for (i = 0; i < NUMCZS && j > i; i++) {
		if (CLUSTERINFO[i].clustername == NULL) {
			for (; j > i; j--) {
				if (CLUSTERINFO[j].clustername != NULL) {
					// shift it up
					memcpy(&CLUSTERINFO[i], &CLUSTERINFO[j],
					    sizeof (cluster_info_t));
					CLUSTERINFO[j].clustername = NULL;
					break;
				}
			}
		}
	}

	// debuglog(LOG_ERR, "in update_cluster_info: after shrinking");
	// print_cluster_info();

	for (j = 0; j < NUMCZS; j++) {
		if (CLUSTERINFO[j].clustername == NULL)
			break;
	}
	// now j will index to the start of null list
	NUMCZS = numczs;
	CLUSTERINFO = (cluster_info_t *)realloc(
	    CLUSTERINFO, NUMCZS * sizeof (cluster_info_t));
	// should never return NULL

	for (i = 0; czlist[i] != NULL; i++) {
		if (czlist[i][0] == '\0')
			continue;
		memset(&CLUSTERINFO[j], '\0', sizeof (cluster_info_t));
		CLUSTERINFO[j++].clustername = strdup(czlist[i]);
		//
		// If we are running the version that has the membership
		// subsystem active, then we register for cluster membership
		// callbacks. Else, we will register for such callbacks
		// during the upgrade callbacks.
		//
		if (membership_api_available_for_rgmd_starter()) {
			register_membership(CLUSTERINFO[j-1].clustername,
			    cluster_reconfig_for_starter);
		}
	}
	for (i = 0; czlist[i] != NULL; i++) {
		free(czlist[i]);
	}
	delete czlist;
	// debuglog(LOG_ERR, "in update_cluster_info: final config");

	//
	// XXX We are not initializing the node, rgm and pid
	// XXX incarnation members of the cluster_info_t
	// XXX for non-global zone clusters. Is that fine?
	//

	if (!membership_api_available_for_rgmd_starter()) {
		//
		// If we are not running the version where
		// we have membership API support, then
		// only base cluster should be present.
		//
		CL_PANIC(NUMCZS == 1);
	}
	// print_cluster_info();
}

static os::mutex_t starter_mutex;
static void
starter_lock() {
	starter_mutex.lock();
}
static void
starter_unlock() {
	starter_mutex.unlock();
}
#endif

#if (SOL_VERSION >= __s10)
#ifndef EUROPA_FARM
static int
unlink_file(char *fnamep)
{
	int err;
	while (unlink(fnamep) == -1) {
		err = errno;
		if (err == ENOENT) {
			return (0);
		}

		ucmm_print("unlink_file", "file %s errno %d\n",
		    RGM_STARTER_DOOR_FILE_NAME, errno);
		if (err != EINTR && err != EAGAIN) {
			return (err);
		}
	}
	return (0);
}

void
rgm_process_mem_callback(
    const char *cluster, const char *mem_obj_namep,
    const cmm::membership_t membership, const cmm::seqnum_t seqnum)
{
	static nodeid_t local_nodeid = clconf_get_local_nodeid();
	if (membership.members[local_nodeid] != INCN_UNKNOWN) {
		return;
	}
	rgm_death_lock.lock();
	(void) os::strcpy(dead_rgmd_cluster, cluster);
	rgm_death_cv.signal();
	rgm_death_lock.unlock();
}

// Door server
static void
door_server_func(void *cookie, char *arg, size_t arglen, door_desc_t *, uint_t)
{
	// The arg would be of form "<pid>'\0'<zonename>'\0'"
	ASSERT(arg[arglen - 1] == '\0');
	rgm_death_lock.lock();
	uint_t pid_len = os::strlen(arg);
	dead_rgmd_pid = os::atoi(arg);
	arg += (pid_len + 1);
	(void) os::strncpy(dead_rgmd_cluster, arg, arglen - pid_len - 1);
	rgm_death_cv.signal();
	rgm_death_lock.unlock();
}

// Set up the door at which pmmd would notify us of a rgmd death
static void
door_setup()
{
	int door_fd = -1;
	int door_file_fd = -1;
	int err;

	if (unlink_file(RGM_STARTER_DOOR_FILE_NAME) != 0) {
		return;
	}

	if ((door_file_fd =
	    open(RGM_STARTER_DOOR_FILE_NAME, O_RDWR | O_CREAT, 0600)) < 0) {
		err = errno;
		ucmm_print("door_setup", "open file %s errno %d\n",
		    RGM_STARTER_DOOR_FILE_NAME, err);
		return;
	}
	(void) close(door_file_fd);

	if ((door_fd = door_create(door_server_func, NULL, 0)) < 0) {
		err = errno;
		ucmm_print("door_setup", "door_create errno %d\n", err);
		(void) unlink_file(RGM_STARTER_DOOR_FILE_NAME);
		return;
	}

	if (fattach(door_fd, RGM_STARTER_DOOR_FILE_NAME) < 0) {
		err = errno;
		ucmm_print("door_setup", "fattach file %s errno %d\n",
		    RGM_STARTER_DOOR_FILE_NAME, err);
		(void) unlink_file(RGM_STARTER_DOOR_FILE_NAME);
		return;
	}

	ucmm_print("door_setup", "successfully created door at %s\n",
	    RGM_STARTER_DOOR_FILE_NAME);
}
#endif	// EUROPA_FARM
#endif	// SOL_VERSION >= __s10

static void
orb_init()
{
#ifndef EUROPA_FARM
	if (clconf_lib_init() != 0) {
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to initialize its interface to
		// the low-level cluster machinery. This might occur because
		// the operator has attempted to start the rgmd on a node that
		// is booted in non-cluster mode. The rgmd will produce a core
		// file, and in some cases it might cause the node to halt or
		// reboot to avoid data corruption.
		// @user_action
		// If the node is in non-cluster mode, boot it into cluster
		// mode before attempting to start the rgmd. If the node is
		// already in cluster mode, save a copy of the
		// /var/adm/messages files on all nodes, and of the rgmd core
		// file. Contact your authorized Sun service provider for
		// assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: couldn't initialize ORB, possibly because  "
		    "machine is booted in non-cluster mode"));
		abort();
	}
#endif
}

//
// Arm a failfast device to kill the node if the rgmd process
// dies either due to hitting a fatal bug or due to being killed
// inadvertently by an operator.
//
// This needs to be done after initializing the ORB since the ORB
// is needed to lookup the failfast admin object.
//
void
register_for_failfast(char *failfast_string)
{
	Environment e;

	ff::failfast_admin_var ff_admin_ptr = cmm_ns::get_ff_admin();

	death_ff_v = ff_admin_ptr->ff_create(failfast_string,
	    ff::FFM_DEFERRED_PANIC, e);

	if (e.exception() || CORBA::is_nil(death_ff_v)) {
		//
		// SCMSGS
		// @explanation
		// The rgmd program could not enable the failfast mechanism.
		// The failfast mechanism is designed to prevent data
		// corruption by causing the node to be shutdown in the
		// event that the rgmd program dies.
		// @user_action
		// To avoid data corruption, the rgmd will halt or reboot
		// the node. Contact your authorized Sun service provider
		// to determine whether a workaround or patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: could not create death_ff");
		abort();
		// UNREACHABLE
	}
	death_ff_v->arm(ff::FF_INFINITE, e);
	if (e.exception()) {
		//
		// SCMSGS
		// @explanation
		// The rgmd program could not enable the failfast
		// mechanism. The failfast mechanism is designed to
		// prevent data corruption by causing the node to be
		// shutdown in the event that the rgmd program dies.
		// @user_action
		// To avoid data corruption, the rgmd will halt or
		// reboot the node. Contact your authorized Sun service
		// provider to determine whether a workaround or
		// patch is available.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: death_ff->arm failed");
		abort();
	}
	// Trigger the failfast upon receipt of these signals
	ff_register_signal_handler(SIGILL, &death_ff_v);
	ff_register_signal_handler(SIGBUS, &death_ff_v);
	ff_register_signal_handler(SIGSEGV, &death_ff_v);
}

// disarm failfast. rgmd for a zone cluster will have to
// unregister with failfast when the zone cluster is deleted.
void
failfast_disarm() {
	Environment e;

	// this method must be called after register_for_failfast()
	// has been called. Otherwise, it would imply that this
	// function is not being used correctly. Hence, we will abort.
	if (CORBA::is_nil(death_ff_v)) {
		abort();
	}

	death_ff_v->disarm(e);
	if (e.exception()) {
		abort();
	}
}

int local_nodeid = -1;
#if SOL_VERSION >= __s10

void
startspawner()
{
	starter_lock();
	pid_t pid; int i, retval;
	char *failfast_string = "rgmdstarter";

	register_for_failfast(failfast_string);
	rgm_starter_initialize_versioning();
	start_ccr_change_thread();
	rgm_init_cluster_info();
	rgm_register_cluster_callbacks();
	rgm_update_cluster_info();

	local_nodeid = clconf_get_local_nodeid();
	// register for ucmm membership for all physical/logical clusters
	// whenever zone starts up, start rgmd....
	for (i = 0; i < NUMCZS; i++) {
		update_membership(i);
		CLUSTERINFO[i].pid_incarnation = 0;
		if (zone_is_up(i) && need_new_rgm(i)) {
			if (!membership_api_available_for_rgmd_starter()) {
				//
				// Should start base cluster rgmd only
				// if we are not yet running the software
				// version that supports membership API.
				//
				CL_PANIC(os::strcmp(CLUSTERINFO[i].clustername,
				    GLOBAL_ZONENAME) == 0);
			}
			CLUSTERINFO[i].pid_incarnation =
			    startnewrgmd(i);
			CLUSTERINFO[i].rgm_incarnation =
			    CLUSTERINFO[i].node_incarnation;
			ucmm_print("startspawner", "pid created:%d",
			    CLUSTERINFO[i].pid_incarnation);
			if (CLUSTERINFO[i].pid_incarnation <= 0) {
				abort();
			}
		} else {
			// When a node comes up, we have to check whether there
			// are any zone-clusters which are configured and are in
			// cluster mode, but are not configured on the current
			// node. For such cases, we have to start a proxy rgmd
			// for such zone clusters. This is required for CLIs,
			// scha commands and SCM to work.
			if (sc_zone_configured_on_cur_node(
				CLUSTERINFO[i].clustername)) {
				continue;
			}
			// This zone cluster is not configured on this node. If
			// this zone cluster is already in cluster mode, then we
			// will start th proxy rgmd. Otherwise we will wait for
			// membership callbacks to notify of cluster members and
			// will start a proxy rgmd only then.
			CORBA::Exception *exp;
			cmm::membership_t membership;
			cmm::seqnum_t seqnum;
			Environment e;
			boolean_t in_cluster_mode;

			in_cluster_mode = B_FALSE;
			membership::api_var membership_api_v =
			    cmm_ns::get_membership_api_ref();
			if (CORBA::is_nil(membership_api_v)) {
				// ignore the error
				continue;
			}

			membership_api_v->get_cluster_membership(
			    membership, seqnum, CLUSTERINFO[i].clustername, e);
			exp = e.exception();

			if (exp != NULL) {
				// ignore this exception
				continue;
			}

			for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
				if (membership.members[nid] != INCN_UNKNOWN) {
					in_cluster_mode = B_TRUE;
					break;
				}
			}

			if (in_cluster_mode == B_FALSE) {
				// None of the members of this zone cluster
				// are up. No need to start the proxy now.
				continue;
			}

			// We need to start the proxy rgmd now as some of the
			// members of this zone cluster are already up and
			// running.
			// debuglog(LOG_ERR, "starting proxy rgmd for %s",
			//    CLUSTERINFO[i].clustername);
			CLUSTERINFO[i].pid_incarnation = startnewrgmd(i);
			CLUSTERINFO[i].rgm_incarnation =
					CLUSTERINFO[i].node_incarnation;
			if (CLUSTERINFO[i].pid_incarnation <= 0) {
				// Error starting proxy rgmd
				abort();
			}

		}
	}
	starter_unlock();
	rgm_death_lock.lock();
	while (1) {
		ucmm_print("startspawner:", "wait for children\n");
		rgm_death_cv.wait(&rgm_death_lock);
		for (i = 0; i < NUMCZS; i++) {
			if (os::strcmp(dead_rgmd_cluster,
			    CLUSTERINFO[i].clustername) == 0) {
				ucmm_print("startspawner",
				    "rgmd for cluster %s died\n",
				    CLUSTERINFO[i].clustername);
				break;
			}
		}
		// some rgmd process has died. try starting new rgmd for that cz
		starter_lock();

		if (!membership_api_available_for_rgmd_starter()) {
			CL_PANIC(os::strcmp(CLUSTERINFO[i].clustername,
			    GLOBAL_ZONENAME) == 0);
		}

		// local_incarn: zone_state, pid, rgm_incarn, zone_incar
		ucmm_print("startspawner", ":%d:%d\n",
		    i, CLUSTERINFO[i].pid_incarnation);
		ucmm_print("startspawner", "set pid 0 for %d\n", i);
		CLUSTERINFO[i].pid_incarnation = 0;
		if (zone_is_up(i) && need_new_rgm(i)) {
			CLUSTERINFO[i].pid_incarnation =
			    startnewrgmd(i);

			CLUSTERINFO[i].rgm_incarnation =
			    CLUSTERINFO[i].node_incarnation;
			ucmm_print("startspawner", "pid created:%d",
			    CLUSTERINFO[i].pid_incarnation);
			if (CLUSTERINFO[i].pid_incarnation <= 0) {
				rgm_death_lock.unlock();
				abort();
			}
		}
		starter_unlock();
	}
	rgm_death_lock.unlock();
}

void
cluster_reconfig_for_starter(
    const char *cluster, const char *membership_namep,
    const cmm::membership_t membership, const cmm::seqnum_t seqnum)
{
	int i;

	ucmm_print("crfs", "reconfiguration received for cluster:%s", cluster);
	ASSERT(ZONE == NULL);
	starter_lock();
	for (i = 0; i < NUMCZS; i++) {
		if (strcmp(cluster, CLUSTERINFO[i].clustername) == 0) {
			break;
		}
	}
	if (i >= NUMCZS) {
		// we received membership before receiving the configuration
		debuglog(LOG_ERR, "received membership for unknown cluster");
		abort();
	}
	CLUSTERINFO[i].node_incarnation = membership.members[local_nodeid];
	ucmm_print("crfs", ": czname:%s:pid:%d:rgm_incarn:%d:node_incarn:%d\n",
	    cluster, CLUSTERINFO[i].pid_incarnation,
	    CLUSTERINFO[i].rgm_incarnation, CLUSTERINFO[i].node_incarnation);
	if (zone_is_up(i) && need_new_rgm(i)) {
		CLUSTERINFO[i].pid_incarnation =
			    startnewrgmd(i);
			CLUSTERINFO[i].rgm_incarnation =
			    CLUSTERINFO[i].node_incarnation;
			if (CLUSTERINFO[i].pid_incarnation <= 0) {
				abort();
			}
#if (SOL_VERSION >= __s10)
	} else if ((!sc_zone_configured_on_cur_node(cluster)) &&
			(CLUSTERINFO[i].pid_incarnation <= 0)) {
		// We have to check whether a zone by this name exists or
		// not in this physical node. If there is no such zone, it
		// means that we will have to start a proxy rgmd on this
		// node.
		// If we are here, it means that we have to start the proxy
		// rgmd. Before we start the proxy rgmd we have to ensure
		// that it is not already running. Hence, two 'if' checks.
		debuglog(LOG_ERR, "starting proxy rgmd for %s", cluster);
		CLUSTERINFO[i].pid_incarnation = startnewrgmd(i);
		CLUSTERINFO[i].rgm_incarnation =
				CLUSTERINFO[i].node_incarnation;
		if (CLUSTERINFO[i].pid_incarnation <= 0) {
			// Error starting proxy rgmd
			abort();
		}
#endif
	}
	starter_unlock();
}

extern char **environ;
pid_t
startnewrgmd(int i)
{
	int err = 0;
	pid_t pid;
	ucmm_print("startnewrgmd", "attempt to start new rgmd for cz:%s\n",
	    CLUSTERINFO[i].clustername);

	//
	// We send the signal mask of the rgmd starter to pmmd.
	// pmmd will set up this signal mask for the child rgmd that it forks.
	//
	sigset_t blocked_signal_set;
	if (sigprocmask(SIG_BLOCK, NULL, &blocked_signal_set) != 0) {
		err = errno;
		ucmm_print("startnewrgmd", "sigprocmask(2) failed to query "
		    "current signal mask, error %s\n", strerror(err));

		//
		// SCMSGS
		// @explanation
		// rgmd_starter was unable to query its current signal mask
		// because the sigprocmask(2) function failed.
		// The message contains the system error.
		// rgmd_starter cannot launch an rgmd daemon.
		// @user_action
		// Save a copy of the /var/adm/messages files on this node,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigprocmask(2) failed while querying current signal mask: "
		    "error %s", strerror(err));

		// Return 0 as the pid of the process forked; this means error
		return (0);
	}

	//
	// rgmd starter could have set the signal disposition of certain signals
	// to SIG_IGN. We pass on this ignored signal set to pmmd.
	// pmmd will set the signal disposition of the rgmd (that it forks off)
	// for signals in this signal set to SIG_IGN.
	//

	// Initialize a signal set that will be the set of ignored signals
	sigset_t ignored_signal_set;
	if (sigemptyset(&ignored_signal_set) != 0) {
		err = errno;
		ucmm_print("startnewrgmd", "sigemptyset(3C) failed to "
		    "initialize the ignored signal set, error %s\n",
		    strerror(err));

		//
		// SCMSGS
		// @explanation
		// rgmd_starter was unable to initialize a signal set
		// because the sigemptyset(3C) function failed.
		// The message contains the system error.
		// rgmd_starter cannot launch an rgmd daemon.
		// @user_action
		// Save a copy of the /var/adm/messages files on this node,
		// and report the problem to your authorized Sun service
		// provider.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "sigemptyset(3C) failed while initializing "
		    "ignored signal set: error %s", strerror(err));

		// Return 0 as the pid of the process forked; this means error
		return (0);
	}

	//
	// Find out which signals are ignored and
	// add such signals to the ignored signal set.
	//
	for (int sig = 1; sig <= SIGRTMAX; sig++) {
		//
		// Try converting the signal to its name.
		// If the signal number does not correspond to a valid signal,
		// then this conversion will fail; ignore such a signal number.
		// Of course, the assumption here is that
		// sig2str() works correctly.
		//
		char sig_name[SIG2STR_MAX];
		if (sig2str(sig, (char *)sig_name) != 0) {
			continue;
		}

		// Query the sigaction for this signal
		struct sigaction action;
		(void) memset(&action, 0, sizeof (struct sigaction));
		if (sigaction(sig, NULL, &action) != 0) {
			err = errno;
			ucmm_print("startnewrgmd", "sigaction(2) failed "
			    "to query disposition for signal SIG%s, "
			    "error %s\n", sig_name, strerror(err));

			//
			// SCMSGS
			// @explanation
			// rgmd_starter was unable to query
			// the signal disposition for a signal number
			// because the sigaction(2) function failed.
			// The message contains the system error.
			// rgmd_starter cannot launch an rgmd daemon.
			// @user_action
			// Save a copy of the /var/adm/messages files
			// on this node, and report the problem to
			// your authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "sigaction(2) failed while querying "
			    "disposition for signal SIG%s: error %s",
			    sig_name, strerror(err));

			//
			// Return 0 as the pid of the process forked;
			// this means error
			//
			return (0);
		}

		if (action.sa_handler != SIG_IGN) {
			continue;
		}

		//
		// This signal is set to be ignored.
		// Add the signal number to the ignored signal set.
		//
		if (sigaddset(&ignored_signal_set, sig) != 0) {
			err = errno;
			ucmm_print("startnewrgmd", "sigaddset(3C) failed "
			    "to add signal SIG%s to a signal set, error %s\n",
			    sig_name, strerror(err));

			//
			// SCMSGS
			// @explanation
			// rgmd_starter was unable to add a signal number
			// to a signal set, because the sigaddset(3C)
			// function failed.
			// The messages contains the system error.
			// rgmd_starter cannot launch an rgmd daemon.
			// @user_action
			// Save a copy of the /var/adm/messages files
			// on this node, and report the problem to
			// your authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "sigaddset(3C) failed while adding signal SIG%s "
			    "to ignored signal set: error %s",
			    sig_name, strerror(err));

			//
			// Return 0 as the pid of the process forked;
			// this means error
			//
			return (0);
		}
	}

	// Launch rgmd process under pmmd control
	// Setup door args
	uint_t data_len = 0;
	if (Debugflag) {
		data_len =
		    (sizeof (sigset_t) * 2) + // Blocked and ignored signal sets
		    os::strlen(RGMD_PROC_NAME) + 1 +
		    os::strlen(CLUSTERINFO[i].clustername) + 1 +
		    os::strlen(RGMD_DEBUG_BINARY) + 1 +
		    os::strlen("-z") + 1 +
		    os::strlen(CLUSTERINFO[i].clustername) + 1;
	} else {
		data_len =
		    (sizeof (sigset_t) * 2) + // Blocked and ignored signal sets
		    os::strlen(RGMD_PROC_NAME) + 1 +
		    os::strlen(CLUSTERINFO[i].clustername) + 1 +
		    os::strlen(RGMD_BINARY) + 1 +
		    os::strlen("-z") + 1 +
		    os::strlen(CLUSTERINFO[i].clustername) + 1;
	}
	for (uint_t ind = 0; environ[ind] != NULL; ind++) {
		data_len += (os::strlen(environ[ind]) + 1);
	}
	//
	// currently we do a quick workaround of doing an exec
	// with ld_preload so that we can change library without reboot
	// of whole node.
	//
	bool use_alt_library = false;
	char library[ZONENAME_MAX + 19]; // 18 for string librgmserver.so.1.
	strcpy(library, "librgmserver.so.1.");
	strcat(library, CLUSTERINFO[i].clustername);
	if (access(library, R_OK) == 0 &&
	    access("/librgmserver.so.1", X_OK) == 0) {
		data_len += (os::strlen("LD_PRELOAD=/librgmserver.so.1") + 1);
		use_alt_library = true;
			debuglog(LOG_ERR,
			    "use alternate /librgmserver.so.1 for CZ");
	}

	char *datap = new char[data_len];
	char *tmp_datap = datap;
	uint_t len = 0;

	// Blocked signal set
	len = sizeof (sigset_t);
	(void) memcpy((void *)tmp_datap,
	    (const void *)&blocked_signal_set, (size_t)len);
	tmp_datap += len;

	// Ignored signal set
	len = sizeof (sigset_t);
	(void) memcpy((void *)tmp_datap,
	    (const void *)&ignored_signal_set, (size_t)len);
	tmp_datap += len;

	// Process name
	len = os::strlen(RGMD_PROC_NAME);
	(void) os::strncpy(tmp_datap, RGMD_PROC_NAME, len);
	tmp_datap[len] = '\0';
	tmp_datap += (len + 1);

	// Zone cluster name
	len = os::strlen(CLUSTERINFO[i].clustername);
	(void) os::strncpy(tmp_datap, CLUSTERINFO[i].clustername, len);
	tmp_datap[len] = '\0';
	tmp_datap += (len + 1);

	// Rgmd binary path with arguments
	if (Debugflag) {
		len = os::strlen(RGMD_DEBUG_BINARY);
		(void) os::strncpy(tmp_datap, RGMD_DEBUG_BINARY, len);
	} else {
		len = os::strlen(RGMD_BINARY);
		(void) os::strncpy(tmp_datap, RGMD_BINARY, len);
	}
	tmp_datap[len] = ' ';
	tmp_datap += (len + 1);
	len = os::strlen("-z");
	(void) os::strncpy(tmp_datap, "-z", len);
	tmp_datap[len] = ' ';
	tmp_datap += (len + 1);
	len = os::strlen(CLUSTERINFO[i].clustername);
	(void) os::strncpy(tmp_datap, CLUSTERINFO[i].clustername, len);
	tmp_datap[len] = '\0';
	tmp_datap += (len + 1);

	// Environment variables
	for (uint_t ind = 0; environ[ind] != NULL; ind++) {
		len = os::strlen(environ[ind]);
		(void) os::strncpy(tmp_datap, environ[ind], len);
		tmp_datap[len] = '\0';
		tmp_datap += (len + 1);
	}
	if (use_alt_library) {
		len = os::strlen("LD_PRELOAD=/librgmserver.so.1");
		(void) os::strncpy(tmp_datap,
		    "LD_PRELOAD=/librgmserver.so.1", len);
		tmp_datap[len] = '\0';
		tmp_datap += (len + 1);
	}

	// Make a door call
	door_arg_t door_args;
	struct ret_data_t {
		int err;
		pid_t pid;
	};
	struct ret_data_t rbuf;
	int door_fd = open(PMMD_DOOR_FILE_NAME, O_RDONLY);
	if (door_fd < 0) {
		ucmm_print("startnewrgmd",
		    "error in opening door %s, errno %d\n",
		    PMMD_DOOR_FILE_NAME, errno);
		return (0);
	}
	door_args.data_ptr = datap;
	door_args.data_size = data_len;
	door_args.desc_ptr = NULL;
	door_args.desc_num = 0;
	door_args.rbuf = (char *)&rbuf;
	door_args.rsize = sizeof (struct ret_data_t);

	if (door_call(door_fd, &door_args) == -1) {
		err = errno;
		(void) close(door_fd);
		door_fd = -1;
		// should retry in case error is EINTR
		ucmm_print("startnewrgmd",
		    "error in door call to %s errno %d\n",
		    PMMD_DOOR_FILE_NAME, err);
		return (0);
	}
	(void) close(door_fd);
	door_fd = -1;

	rbuf = *((struct ret_data_t *)((void *)door_args.rbuf));
	if (rbuf.err != 0) {
		ucmm_print("startnewrgmd",
		    "return value from door server is %d, pid %d\n",
		    rbuf.err, rbuf.pid);
		return (0);
	}
	return (rbuf.pid);
}
#endif // SOL_VERSION

int
startrgmdstarter(int argc, char **argv)
{

	progname = last_path_elem(argv[0]);
	parse_cmd_args(argc, argv);
#if SOL_VERSION >= __s10
	if (ZONE != NULL) {
		sprintf(RGMD_SYSLOG_TAG, "Cluster.RGM.%s.rgmd", ZONE);
		sprintf(rgm_user_name, "RGM");
	} else {
		sprintf(RGMD_SYSLOG_TAG, "Cluster.RGM.rgmdstarter");
		strcpy(rgm_user_name, "rgm_starter");
	}
#else
	sprintf(RGMD_SYSLOG_TAG, "Cluster.rgmd");
	strcpy(rgm_user_name, "RGM");
#endif
	(void) sc_syslog_msg_set_syslog_tag(RGMD_SYSLOG_TAG);

	//
	// check if its necessary to call get_cluster_info needs to be called
	// before make daemon
	//

	// Allocate ring buffer for tracing.
	rgm_dbg_buf = new dbg_print_buf(RGM_DBG_BUFSIZE);

#if SOL_VERSION >= __s10
	// daemonise only the starter. children get automatically daemonised.
	// even otherwise, if we fork again, then starter won't be able to
	// track the health of this rgmd
	if (ZONE == NULL) {
		make_daemon();
	}
#else
	make_daemon();
#endif

	orb_init();

#if SOL_VERSION >= __s10
	if (ZONE == NULL) {
		// set up the door at which pmmd would tell us when a rgmd dies
#ifndef EUROPA_FARM
		door_setup();
#endif
		// start the rgmd spawner method
		startspawner();
		// NOT REACHABLE
	}
#else
	ZONE = strdup(GLOBAL_ZONENAME);
#endif

	// start rgmd normally. This is only used for debugging
	// ensure this happens only on debug bits, else assert
	// rgm_get_clusterlist(&CZLIST, &NUMCZS);
	// this was needed so that we can identify which are native zones
	// but we can directly use clconf to check zone type. so removed
	// this
	startrgmd();
	// NOT REACHABLE
	return (1);
}

#if SOL_VERSION >= __s10
//
// register with czmm for membership
//
// register with membership subsystem for membership callbacks
// for a zone cluster
//
static void
register_membership(char *zone, mem_callback_funcp_t funcp)
{
	nodeid_t nid;

	ASSERT(zone != NULL);
	//
	// Get a reference to the membership API object
	// on the local node from local nameserver
	//
	membership::api_var membership_api_v = cmm_ns::get_membership_api_ref();
	ASSERT(!CORBA::is_nil(membership_api_v));

	Environment e;
	CORBA::Exception *exp;

	// This callback object should be hosted by the client
	membership::client_ptr client_p =
	    (new membership_client_impl(rgm_user_name,
	    membership::ZONE_CLUSTER, zone, NULL, funcp))->get_objref();
	while (1) {
		membership_api_v->register_for_cluster_membership(
		    client_p, zone, e);
		exp = e.exception();
		if (exp == NULL) {
			// Successfully registered
			break;
		}
		// Exception occurred
		if (membership::no_such_membership::_exnarrow(exp)) {
			//
			// We hear from CCR that a zone cluster has
			// been created. Membership also hears from CCR.
			// Both membership and rgm do their actions
			// asynchronously. So there is a race and hence
			// it is possible that membership subsystem does not
			// know about the new zone cluster when rgm
			// comes to know about it. But we can assume
			// that membership subsystem will eventually
			// start maintaining the membership for the
			// zone cluster. So, we retry here if we get
			// a no_such_membership exception from the
			// membership subsystem.
			//
			e.clear();
			continue;
		} else {
			// Exception not expected
			abort();
		}
	}
}

static void
unregister_membership(const char *zone)
{
	ASSERT(zone != NULL);
	// debuglog(LOG_ERR, "unregister for %s: currently no-op", zone);
	return;
#if 0
	// Get a reference to the CZMM object on the local node from nameserver
	membership_api::registry_var the_membership_registry_v =
	    cmm_ns::get_membership_registry();
	Environment e;
	CORBA::Exception *exp;

	// during shutdown
	the_membership_registry_v->unregister_for_membership(
	    client_ptr, zonenamep, e);
	CORBA::release(client_ptr);
#endif
}
#endif

//
// Parse the arguments from the command line.
// -d specifies whether verbose debugging is to be started or not.
// -p specifies process dump behaviour, -s specifies system dump behaviour.
// Both take ON AND OFF as argument, and accordingly behave to produce dumps
// while rebooting node/ method timing out. See CR 4374293
//
// if no -z flag, this means that we are starting the rgmd starter. otherwise
// it starts for given zone. only used for debugging purposes. all the time,
// it will be started without any zone argument. The starter forks rgmds for
// other zones, which use other logic ot see if they are started in what mode.
static void
parse_cmd_args(int argc, char **argv)
{
	int	c;
	uint_t	ival;

	while ((c = getopt(argc, argv, "dp:s:z:")) != -1)
		switch (c) {
		case 'd':
			Debugflag = B_TRUE;
			break;
		case 'p':
			// by default process dump is ON
			if (strcasecmp(optarg, "ON") == 0) {
				Method_dumpflag = B_TRUE;
			} else if (strcasecmp(optarg, "OFF") == 0) {
				Method_dumpflag = B_FALSE;
			} else {
				usage();
				/* NOTREACHED */
			}
			break;
		case 's':
			// by default system dump is OFF
			if (strcasecmp(optarg, "ON") == 0) {
				System_dumpflag = B_TRUE;
			} else if (strcasecmp(optarg, "OFF") == 0) {
				System_dumpflag = B_FALSE;
			} else {
				usage();
				/* NOTREACHED */
			}
			break;
		case 'z':
			ZONE = strdup(optarg);
			break;
		case '?':
		default:
			usage();
			/* NOTREACHED */
		}
	argc -= optind;
	argv += optind;

	if (argc != 0)
		usage();

}


/*
 * Set up signal handlers and do other signal related initializations.
 */
static void
init_signal_handlers()
{
	set_signal_handler(SIGTERM);
	set_signal_handler(SIGINT);
	set_signal_handler(SIGALRM);
	set_signal_handler(SIGPOLL);
}

/*
 * Set up a signal handler in the SA_RESTART mode.
 */
static void
set_signal_handler(int sig)
{
	struct sigaction act;

	(void) sigemptyset(&act.sa_mask);
	act.sa_handler = (void(*)(int))sig_handler;
	act.sa_flags = SA_RESTART;

	if (sigaction(sig, &act, NULL) == -1) {
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: sigaction: %s (UNIX errno %d)",
		    strerror(errno), errno);
		abort();
	}
}

/*
 * Signal handler for the SIGTERM, SIGPOLL, SIGALRM, and SIGINT signals.
 */
static void
sig_handler(int sig)
{
	ucmm_print("RGM", NOGET("sig_handler: signal %d caught\n"), sig);

	switch (sig) {
	case SIGTERM:
#ifdef EUROPA_FARM
		st_ff_disarm();
#endif
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: received signal %d"), sig);
		_exit(1);
		break;
	case SIGALRM:
		// Fall through
	case SIGPOLL:
		// Fall through
	case SIGINT:
		// Fall through
	default:
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("received signal %d"), sig);
	}
}


/*
 * Print the program usage.
 */
static void
usage()
{
	(void) fprintf(stderr, gettext("Usage:\n"));
	(void) fprintf(stderr, "\t%s [-d] [-p ON|OFF] [-s ON|OFF]\n",
	    progname);
	exit(1);
}

/*
 * Return a pointer to the last element in a file pathname;
 */
static char *
last_path_elem(char *path)
{
	char 	*q;

	for (q = path + strlen(path); q >= path; q--)
		if (*q == '/')
			return (q + 1);

	return (path);
}





#if SOL_VERSION >= __s10
class rgm_cb_impl : public McServerof<ccr::callback> {
public:
	void did_update_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery_zc(const char *, const char *, ccr::ccr_update_type,
	    Environment &);
	void did_update(const char *, ccr::ccr_update_type,
	    Environment &);
	void did_recovery(const char *, ccr::ccr_update_type,
	    Environment &);
	void _unreferenced(unref_t);
};
void
rgm_cb_impl::did_update_zc(const char *, const char *,
    ccr::ccr_update_type type, Environment &)
{
	ucmm_print("did_update_zc", NOGET("called\n"));
}
void
rgm_cb_impl::did_update(const char *cluster, ccr::ccr_update_type type,
    Environment &)
{
	uint_t newnumczs, i;
	char **czlist;
	debuglog(LOG_ERR, "did_update called");

	ccr_change_lock.lock();

	switch (type) {
	case CCR_TABLE_ADDED:
		debuglog(LOG_ERR, "new cluster %s added", cluster);
		ccr_change = ZONE_CLUSTER_ADDED;
		os::strcpy(cluster_name, cluster);
		ccr_change_cv.signal();
		break;
	case CCR_TABLE_REMOVED:
		debuglog(LOG_ERR, "cluster %s removed", cluster);
		ccr_change = ZONE_CLUSTER_REMOVED;
		os::strcpy(cluster_name, cluster);
		ccr_change_cv.signal();
		break;
	case CCR_TABLE_MODIFIED:
		debuglog(LOG_ERR, "cluster %s modified", cluster);
		ccr_change_lock.unlock();
		return;
	CCR_RECOVERED:
	CCR_INV_UPDATE:
		// debuglog(LOG_ERR, "shoudln't happen");
		ASSERT(0);
		ccr_change_lock.unlock();
		return;
	}
	ccr_change_lock.unlock();

	ucmm_print("did_update", NOGET("return.\n"));
}

void
rgm_cb_impl::did_recovery_zc(const char *, const char *,
    ccr::ccr_update_type, Environment &)
{
	ucmm_print("did_recovery_zc", NOGET("called\n"));
}

void
rgm_cb_impl::did_recovery(const char *, ccr::ccr_update_type,
    Environment &)
{
	ucmm_print("did_recovery", NOGET("called.\n"));
}
static void
rgm_cb_impl::_unreferenced(unref_t)
{
	ucmm_print("_unreferenced", NOGET("called.\n"));
	delete this;
}

static void rgm_register_cluster_callbacks()
{
	Environment e;
	rgm_cb_impl *infcb;
	ccr::callback_var cbptr;
	static ccr::directory_var ccr_directory_var = ccr::directory::_nil();
	infcb = new rgm_cb_impl;
	cbptr = infcb->get_objref();
	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	ccr_directory_var = ccr::directory::_narrow(obj);
	CL_PANIC(!CORBA::is_nil(ccr_directory_var));

	ccr_directory_var->register_cluster_callback(cbptr, e);
	if (e.exception()) {
		debuglog(LOG_ERR, "fatal: couldn't register for clusters."
		    " abort");
		abort();
		// UNREACHABLE
	}
}
#endif
