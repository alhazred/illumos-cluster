//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_intention.cc	1.28	08/10/12 SMI"

//
// rgm_intention.cc
//

#include <unistd.h>
#include <rgm/rgm_msg.h>
#include <sys/sc_syslog_msg.h>

#include <rgmx_hook.h>

// local includes
#include "rgm_proto.h"
#include "rgm_intention.h"
#include <sys/sol_version.h>
#include <cmm/cmm_ns.h>
#include <h/membership.h>
#include <cmm/membership_client.h>
using namespace std;

// File whose existence forces physical-node RG affinities
#define	USE_PHYSNODE_FILE	"/var/cluster/rgm/physnode_affinities"

//
// Sends slightly different intentions to different logical nodes.
// The flags field is generated from the all_node_flags (which are sent to
// all nodes) and the per_node_flags, which are sent only to those nodes
// in the nodeset.
//
int
latch_intention_pernodeflags(rgm::rgm_entity entity, rgm::rgm_operation op,
    const char *name, rgm::intention_flag_t all_node_flags,
    const PerNodeFlagsT &per_node_flags)
{
	rgm::rgm_intention intent;
	rgm::rgm_comm_ptr prgm_serv;
	Environment e;
	sol::nodeid_t i;
	int n = 0;
	bool is_farmnode;
	LogicalNodeset ns(Rgm_state->static_membership);
	rgm::lni_t lni;
	boolean_t zc_rgmd = B_FALSE;

	ucmm_print("latch_intention_pernodeflags()", NOGET(
	    "Entity = <%s>, Type = <%d>, Op = <%d>, "
		"all_node_flags=<0x%x>\n"), name, entity, op, all_node_flags);

	intent.entity_type = entity;
	intent.operation_type = op;
	intent.idlstr_entity_name = new_str(name);

#if (SOL_VERSION >= __s10)
	// In case of Zone cluster rgmd still continues to run
	// even after zone death. The intention should pass through
	// these rgmds. Process intention should be called on proxy
	// rgmds as well.

	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		zc_rgmd = B_TRUE;
		CORBA::Exception *exp;
		cmm::membership_t membership;
		cmm::seqnum_t seqnum;
		LogicalNodeset mem_nodeset;

		membership::api_var membership_api_v =
		    cmm_ns::get_membership_api_ref();
		if (CORBA::is_nil(membership_api_v)) {
			abort();
		}

		membership_api_v->get_cluster_membership(
		    membership, seqnum, GLOBAL_ZONENAME, e);
		exp = e.exception();
		if (exp != NULL) {
			abort();
		}
		for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
			if (membership.members[nid] != INCN_UNKNOWN) {
				mem_nodeset.addLni(nid);
			}
		}
		ns = mem_nodeset;
	} else {
		//
		// The use_physnode flag forces physical-node semantics for RG
		// affinities and LOCAL_NODE resource dependencies in the base
		// cluster, even on a single-node cluster.
		// We set the flag to true if the USE_PHYSNODE_FILE exists
		// in the global zone and is readable; or to false otherwise.
		//
		Rgm_state->use_physnode =
		    access(USE_PHYSNODE_FILE, R_OK) == 0 ? B_TRUE : B_FALSE;
	}

#endif
	//
	// Iterate IDL calls over all logical nodes
	//
	// If this turns out to be too inefficient, we can re-architect
	// latch_intention_pernodeflags() so that it makes an idl call to each
	// physical node and passes a sequence of <lni, rgm::rgm_intention>
	// pairs; then the iteration over zones would occur on the
	// slave side.  However, R/RG updates are not highly time-critical,
	// and the overhead of making multiple IDL calls is not very large.
	//
	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);
		if (strcmp(ZONE, GLOBAL_ZONENAME) == 0) {
			if (!lnNode->isInCluster()) {
				// skip this zone
				continue;
			}
			// logical node id
			lni = lnNode->getLni();
		} else {
			// logical node id
			lni = lnNode->getLni();
			if (!ns.containLni(lni))
				continue;
		}
		// physical node id
		i = lnNode->getNodeid();

		if ((rgmx_hook_call(rgmx_hook_latch_intention, i,
			lni, lnNode->getIncarnation(),
			&intent, all_node_flags, &per_node_flags, &n,
			&is_farmnode) == RGMX_HOOK_ENABLED) &&
			(is_farmnode)) {
			continue;
		}

		prgm_serv = rgm_comm_getref(i);
		if (CORBA::is_nil(prgm_serv)) {
			//
			// The physical node is not in the membership.
			// Should never occur, since we already checked for
			// membership of the lni.
			//
			continue;
		}

		intent.flags = all_node_flags;
		for (int j = 0; j < per_node_flags.size(); j++) {

			LogicalNodeset *nset = per_node_flags[j].first;
			//
			// Generate the flags for this node from the
			// per_node_flags  and flags.
			//
			if (nset->containLni(lni)) {
				intent.flags |= per_node_flags[j].second;
			}
		}
		prgm_serv->idl_latch_intention(orb_conf::local_nodeid(),
		    Rgm_state->node_incarnations.members[
		    orb_conf::local_nodeid()], lni,
		    lnNode->getIncarnation(), intent, e);
		CORBA::release(prgm_serv);
		if (e.exception() != NULL) {
			(void) except_to_errcode(e, i, lni);
			continue;
		}
		n++;
	}
	return (n);
}


//
// Latch an entity creation/modification/deletion operation
// on all cluster nodes. Called from the president node while
// processing an admin request.
//
// This function is simply a wrapper around latch_intention_pernodeflags.
// It allows the caller to avoid creating the PerNodeFlags argument.
//
// Return value: Is the number of nodes on which this operation
// succeeded. This could potentially be used by the RGM president
// node to double check if the number of node on which a "latch"
// operation was done is the same (or not) as the number of
// nodes on which the "unlatch/process" operations we
// performed. This is probably not neccessary.
//
int
latch_intention(rgm::rgm_entity entity, rgm::rgm_operation op,
    const char *name, LogicalNodeset *ns, rgm::intention_flag_t flags)
{
	ucmm_print("latch_intention()", NOGET("called\n"));

	//
	// latch_intention_pernodeflags expects a vector of nodeset/flag
	// pairs. We convert the ns to a ns/INTENT_ACTION pair
	// (the default behavior for this intention).
	//

	PerNodeFlagsT node_flags(1, make_pair(ns, rgm::INTENT_ACTION));
	return (latch_intention_pernodeflags(entity, op, name, flags,
	    node_flags));
}


//
// Process a previously latched intention on all nodes. All nodes update
// in-core view of the CCR and possibly take other action like running
// INIT, FINI, or UPDATE methods as a result.
//
// Return value: number of nodes on which the operation
// succeeded.
//
// process_intention doesn't actually send the intention;
// it relies on the slave to use the cached version from latch_intention.
//
int
process_intention()
{
	rgm::rgm_comm_ptr prgm_serv;
	Environment e;
	sol::nodeid_t i;
	int n = 0;
	LogicalNodeset ns(Rgm_state->current_membership);
	bool is_farmnode;

	ucmm_print("process_intention()", NOGET(
	    "Not sending intention info; slave uses cached version\n"));

#if (SOL_VERSION >= __s10)
	// In case of Zone cluster rgmd still continues to run
	// even after zone death. The intention should pass through
	// these rgmds. Process intention should be called on proxy
	// rgmds as well.

	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		CORBA::Exception *exp;
		cmm::membership_t membership;
		cmm::seqnum_t seqnum;
		LogicalNodeset mem_nodeset;

		membership::api_var membership_api_v =
		    cmm_ns::get_membership_api_ref();
		if (CORBA::is_nil(membership_api_v)) {
			abort();
		}

		membership_api_v->get_cluster_membership(
		    membership, seqnum, GLOBAL_ZONENAME, e);
		exp = e.exception();
		if (exp != NULL) {
			abort();
		}
		for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
			if (membership.members[nid] != INCN_UNKNOWN) {
				mem_nodeset.addLni(nid);
			}
		}
		ns = mem_nodeset;
	}
#endif
	//
	// Since we don't pass the intention, we iterate over the
	// physical slave nodes.  The iteration over non-global zones
	// is done on the slave side.
	//
	i = 0;
	while ((i = ns.nextPhysNodeSet(i + 1)) != 0) {
		if ((rgmx_hook_call(rgmx_hook_process_intention, i, &n,
			&is_farmnode) == RGMX_HOOK_ENABLED) &&
			(is_farmnode)) {
			continue;
		}

		prgm_serv = rgm_comm_getref(i);
		if (CORBA::is_nil(prgm_serv)) {
			continue;	// This node is not in the cluster
		}
		prgm_serv->idl_process_intention(orb_conf::local_nodeid(),
		    Rgm_state->node_incarnations.members[
		    orb_conf::local_nodeid()], e);
		CORBA::release(prgm_serv);
		if (e.exception() != NULL) {
			(void) except_to_errcode(e, i);
			continue;
		}
		n++;
	}
	return (n);
}



//
// Unlatch an intention from all nodes.
//
// Return value: Number of nodes on which operation was
// successfully performed.
//
// Doesn't send the intention info. Relies on the slave to use
// its cached version.
//
int
unlatch_intention()
{
	rgm::rgm_comm_ptr prgm_serv;
	Environment e;
	sol::nodeid_t i;
	int n = 0;
	LogicalNodeset ns(Rgm_state->current_membership);
	bool is_farmnode;

	ucmm_print("unlatch_intention()", NOGET(
	    "Not sending intention info; slave uses cached version\n"));

#if (SOL_VERSION >= __s10)
	// In case of Zone cluster rgmd still continues to run
	// even after zone death. The intention should pass through
	// these rgmds. Process intention should be called on proxy
	// rgmds as well.

	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		CORBA::Exception *exp;
		cmm::membership_t membership;
		cmm::seqnum_t seqnum;
		LogicalNodeset mem_nodeset;

		membership::api_var membership_api_v =
		    cmm_ns::get_membership_api_ref();
		if (CORBA::is_nil(membership_api_v)) {
			abort();
		}

		membership_api_v->get_cluster_membership(
		    membership, seqnum, GLOBAL_ZONENAME, e);
		exp = e.exception();
		if (exp != NULL) {
			abort();
		}
		for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
			if (membership.members[nid] != INCN_UNKNOWN) {
				mem_nodeset.addLni(nid);
			}
		}
		ns = mem_nodeset;
	}
#endif
	//
	// Since we don't pass the intention, we iterate over the
	// physical slave nodes.  The iteration over non-global zones
	// is done on the slave side.
	//
	i = 0;
	while ((i = ns.nextPhysNodeSet(i + 1)) != 0) {
		if ((rgmx_hook_call(rgmx_hook_unlatch_intention, i, &n,
			&is_farmnode) == RGMX_HOOK_ENABLED) &&
			(is_farmnode)) {
			continue;
		}

		prgm_serv = rgm_comm_getref(i);
		if (CORBA::is_nil(prgm_serv)) {
			continue;	// This node is not in the cluster
		}
		prgm_serv->idl_unlatch_intention(orb_conf::local_nodeid(),
		    Rgm_state->node_incarnations.members[
		    orb_conf::local_nodeid()], e);
		CORBA::release(prgm_serv);
		if (e.exception() != NULL) {
			(void) except_to_errcode(e, i);
			continue;
		}
		n++;
	}
	return (n);
}
