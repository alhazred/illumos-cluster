//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_president.cc	1.174	09/02/19 SMI"

//
// This module implements the RGM President functions executed
// during reconfiguration.
// The communication functions for sending ORB/rpc requests
// are in rgm_comm.c
//

#include<syslog.h>
#include <orb/fault/fault_injection.h>
#include <rgm_state.h>
#include <rgm_proto.h>
#include <rgm_global_res_used.h>
#include <rgm_intention.h>
#include <time.h>
#include <scha_err.h>
#include <rgm/rgm_common.h>
#include <rgm/rgm_cnfg.h>
#include <rgm/rgm_msg.h>
#include <rgm/rgm_errmsg.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm_threads.h>
#include <scadmin/scconf.h>


#include <orb/infrastructure/orb.h>
#include <cmm/cmm_ns.h>
#include <h/membership.h>
#include <cmm/membership_client.h>
#include <sys/sol_version.h>


// Zone support
#include <rgm_logical_node_manager.h>
#include <rgm_logical_node.h>
#include <rgm_logical_nodeset.h>
#include <rgm/sczones.h>

//
// Maximum time in seconds we wait for non-global zones to boot before
// choosing the most preferred zones for RG failback.
//
#define	ZONE_BOOTDELAY	20
#define	SEPARATOR	"$"

#include <rgmx_hook.h>

static idlretval_t rgm_init_pres_state(const LogicalNodeset &ns);
static idlretval_t elect_president(nodeset_t ns,
    boolean_t *i_am_president, boolean_t *i_am_new_president);
static void rgm_pres_set_all_offline(sol::nodeid_t nodeid);
static void reset_rg_state(rgm::lni_t lni, const rglist_p_t rgp);
static void check_run_failback(rglist_p_t rgp, rgm::lni_t lni);
static void failback_wrapper(sol::nodeid_t nodeid, rglist_p_t rgp);
static void unpack_rg_seq(rgm::lni_t lni, rgm::rg_state rgseq_ref);
static void run_rebalance(rglist_p_t rgp);
os::condvar_t cond_slavewait;
os::condvar_t cond_zone_bootdelay;
static void run_rebalance_on_dependents(const rglist_p_t rgp);

static void update_nlp(nodeidlist_t *nlp);

//
// Zone support.
//
static int retrieve_lni_mappings(const LogicalNodeset &non_joiners);
static void fill_in_lnis(void);


static nodeset_t mem_nodeset = 0;

//
// rgm_reconfig
//
// This function is called either:
// 	via step_trans() in cm_callback_impl.cc.
// 	The UCMM nodeset is passed as an input argument.
// 	The flag isucmm is set to true.
//
// or via FMM thread (fmm_callback_func) in case Europa functionality is
// activated. If the case the flag isucmm is set to false.
//
//
// It is one, big, uninterruptible step.  So if a node dies/joins while we
// are in this code, all nodes will finish the step before being called
// back by UCMM.
//
// We hold the rgm state lock for the duration of this function, so
// we do not need to worry about any wake_president calls interrupting
// us until we have released the lock at the end.
//
// * Grab lock
// * Adjust object references according to the new UCMM membership
// * Elect President
// * I'm finished if I'm not the President
// * I'm President; set membership values in state structure
// * Find out who's joining.
// * If I am NEW President, register with name service, tell slaves.
// * If I'm a new, joining President, read CCR.
// * If new President, register the RMA service state callback.
// * If NEW, non-joining President, complete intention processing and
//   initialize President state from other nodes.  This includes handling
//   of "lost wakes", i.e. calls are made to process_needy_rg() as the
//   new RG states are fetched.
// * Process RGs who had a potential master die or die and quickly rejoin
// * Tell joiners to read CCR.
// * Notify slaves of frozen RGs for global resources used.
// * Tell joiners to run boot methods, then fetch their state
// * Tell slaves to record new membership values
// * Release lock.  Now wake_me calls can do more rebalancing and failback
//   for joiners.
//
// We handle all IDL calls the same way during reconfiguration:  Upon rgmd
// and node death (return value != RGMIDL_OK) we return early.  If we
// cannot get a reference or we get an unknown exception, we crash and
// burn (exit).  There is one exception to this: complete_intention() just
// keeps going if there is a node or rgmd death.
//


void
rgm_reconfig_helper(LogicalNodeset &ns, boolean_t isucmm)
{
	boolean_t i_am_new_president = B_FALSE;
	boolean_t i_am_president = B_FALSE;
	LogicalNodeset join_ns;
	LogicalNodeset prev_ns;
	LogicalNodeset prev_zc_ns;
	LogicalNodeset clear_evac_ns;
	sol::nodeid_t i, mynodeid, oldpresident;
	namelist_t	*frozen_rgs = NULL;	// frozen RGs
	sc_syslog_msg_handle_t handle;
	int rma_reg_err;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	LogicalNode *lnNode;
	LogicalNodeset non_joiners;
	char *ns_buffer;
	int retval;
	LogicalNodeset slave_nodeset;
	boolean_t zc_rgmd = B_FALSE;

	rgm::lni_t lni = 0;
	while ((lni = ns.nextLniSet(lni + 1)) != 0) {
		ucmm_print("rgm_reconfig_helper", "node %d up", lni);
	}
	if (ns.display(ns_buffer) == 0) {
		ucmm_print("RGM",
		    NOGET("rgm_reconfig %s membership ns=0x%s\n"),
		    (isucmm? "UCMM" : "FMM"), ns_buffer);
		free(ns_buffer);
	}

	mynodeid = orb_conf::local_nodeid();


	if (isucmm) {
		//
		// Cache the current cluster state from the cmm automaton.  This
		// includes a nodeset of the "cluster nodes", i.e. all nodes
		// configured statically in the cluster whether or not they are
		// current members (rp_all_nodes) and the highest configured
		// nodeid (rp_nnodes).
		//
		if (process_membership_support_available()) {
			update_cluster_state(B_TRUE, B_TRUE);
		} else {
			update_cluster_state(B_TRUE, B_FALSE);
		}
		ucmm_print("RGM", NOGET("static membership ns=0x%llx; highest "
		    "configured nodeid=%d"), Rgm_state->thecluststate.
		    rp_all_nodes, Rgm_state->thecluststate.rp_nnodes);
	}

	// Europa - Add the configured farm nodes to the static membership.
	if (rgmx_hook_call(rgmx_hook_update_configured_nodes,
	    &Rgm_state->static_membership, &retval) == RGMX_HOOK_ENABLED) {
		if (retval == -1) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "fatal: rgmx_update_configured_nodes() : failed");
			sc_syslog_msg_done(&handle);
			abort();
		}
		Rgm_state->num_static_nodes =
		    Rgm_state->static_membership.count();
	}

	//
	// Europa only:
	// Update the physical membership of the cluster, either with
	// the Server nodes membership or with the Farm nodes membership.
	//
	if (rgmx_hook_call(rgmx_hook_update_membership, &ns, isucmm)
		== RGMX_HOOK_ENABLED) {
		if (ns.isEmpty()) {
			ucmm_print("RGM", "Waiting for UCMM membership\n");
			rgm_unlock_state();
			return;
		}
	}

	//
	// Should be redundant with code from ccr callbacks, but just
	// in case do it here too.
	//
	// Note, this unconfigures zones on nodes that were unconfigured, but
	// does not configure zones on nodes that were newly configured.
	//
	ucmm_print("rgm_reconfig_helper", "call updateConfiguredNodes");
	lnManager->updateConfiguredNodes(Rgm_state->static_membership);

	//
	// If we are in a FMM reconfiguration, check our Server role.
	// If we are not the president, return early. Only the president
	// node is allowed to process the Farm reconfiguration.
	//
	if (!isucmm) {
		if (Rgm_state->rgm_President != mynodeid) {
			ucmm_print("rgm_reconfig_helper",
			    "I'm NOT president and FMM"
			    " reconfiguration, we are done\n");
			rgm_unlock_state();
			return;
		}
	}


	//
	// If this reconfiguration included a node death,
	// there may be fencing in progress. We need
	// to avoid bringing RGs online until fencing is complete.
	//
	// The rgm_run_state logic will postpone starting an RG
	// if fencing is in progress. In that case we need to
	// re-run the state machine once fencing has completed.
	// The fencing thread will do this if we tell it to
	// start checking now.
	//
	// We make the call here, even though we don't know whether
	// or not the reconfig actually caused fencing to start.
	// We need to make the call just in case fencing does start.
	// The fencing call is pretty light-weight, and an extra run_state
	// call is also, so the overhead is not too high.
	//
	// See the comments in rgm_fencing.cc for details.
	//
	trigger_wait_for_fencing();

	//
	// Begin the Presidential election.
	// Pass to this function the new physical node membership bitmask.
	// It will set i_am_president to TRUE if I'm the president,
	// and new_president to TRUE if I'm the NEW president.
	// It does not modify the value of Rgm_state->rgm_President.
	// Makes IDL calls; see block comment at top.
	//
	ucmm_print("rgm_reconfig_helper", "elect president");
#if (SOL_VERSION >= __s10)
	if (!sc_zone_configured_on_cur_node(ZONE)) {
		rgm_unlock_state();
		return;
	}
#endif
	if (isucmm) {
		//
		// President election make sense only if we are in a Server
		// (UCMM) reconfiguration.
		//
		nodeset_t members_ns;
		//
		// elect_president() is called with cluster membership
		// nodeset as the first parameter. Process membership
		// nodeset(ns) contains all the rgmds which are
		// alive and running for a cluster. For the physical
		// cluster, this would translate to all the nodes which
		// are in membership (because rgmd not running on a
		// physical node will trigger a failfast). However, this
		// case is different for a zone cluster. Zone clusters
		// can have proxy rgmds running and proxy rgmds will
		// be part of process membership nodeset. However, we
		// have to be careful to eliminate proxy rgmds from
		// being part of the election. For zone clusters, it
		// would not be possible to entirely depend on zcmm membership
		// to determine the current cluster membership. Zcmm
		// membership (Rgm_state->current_zc_membership) is
		// updated each time a zcmm callback is received (in
		// rgm_zone_reconfig) and it is in rgm_zone_reconfig
		// where process membership is notified about zone up
		// states. The process membership callback (which finally
		// calls rgm_reconfig_helper) is a separate thread. To
		// reiterate, the zcmm callback thread is different from
		// process membership callback thread and it is possible
		// that the two callbacks are slightly out of sync and
		// zcmm nodeset has a node which is not present in "ns".
		// And this can be different on different nodes, which
		// can result in a situation where for the same process
		// membership callback, different nodes have different
		// values of Rgm_state->current_zc_membership. This can
		// result in multiple presidents and consequenses thereof.
		// To avoid such situations, we use an intersection of nodesets
		// of process membership and zcmm below.
		//

#if (SOL_VERSION >= __s10)
		if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {

			nodeset_t pm_ns = ns.getNodeset();
			nodeset_t zcmm_ns =
			    Rgm_state->current_zc_membership.getNodeset();
			char *nodeset_buf;

			members_ns = EMPTY_NODESET;
			if (ns.display(nodeset_buf) == 0) {
				ucmm_print("rgm_reconfig_helper",
				    "process mem nodeset=0x%s\n", nodeset_buf);
				free(nodeset_buf);
			}
			if (Rgm_state->current_zc_membership.display(
			    nodeset_buf) == 0) {
				ucmm_print("rgm_reconfig_helper",
				    "zcmm nodeset=0x%s\n", nodeset_buf);
				free(nodeset_buf);
			}

			members_ns = nodeset_intersect(pm_ns, zcmm_ns);
		} else {
			members_ns = ns.getNodeset();
		}
#else
		members_ns = ns.getNodeset();
#endif

		if (elect_president(members_ns, &i_am_president,
		    &i_am_new_president) != RGMIDL_OK) {
			//
			// return early from reconfiguration step
			//
			rgm_unlock_state();
			return;
		}
	} else {
		//
		// We are in a Farm (FMM) reconfiguration, we are the current
		// president.
		//
		i_am_new_president = B_FALSE;
		i_am_president = B_TRUE;
	}
	ucmm_print("rgm_reconfig_helper", "elected president");

	FAULTPT_RGM(FAULTNUM_RGM_AFTER_ELECT_PRESIDENT,
		FaultFunctions::generic);

	//
	// If I'm not President, I don't do anything now but
	// respond to orders from the President.  Return.
	//
	if (!i_am_president) {
		rgm_unlock_state();
		return;
	}

	//
	// I'm President!!!
	//
	ucmm_print("RGM", "RRRRRRRRRRRR I'm President!\n");

	//
	// Europa only:
	//
	// If this reconfiguration included some farm nodes death/join,
	// there may be fencing operations to process. We need
	// to avoid bringing RGs online until fencing is complete.
	// This operation is done synchronously at this point, and therefore
	// the reconfiguration is blocked until all the outgoing farm nodes are
	// fenced from the shared storage and all the incoming farm nodes are
	// unfenced from the shared storage.
	//
	if (!isucmm) {
		(void) rgmx_hook_call(rgmx_hook_trigger_farm_fencing);
	}

	//
	// Set the rgm_President state flag, but save the nodeid of the
	// old president.  If we are a new president, and need to return
	// early from this routine, we set our notion of who is the president
	// back to the old president, so that we will get treated as a new
	// president again in the next reconfiguration.
	//
	// We need to do this because we might return early, before we've
	// fetched all the state from the slaves and processed all the needy
	// RGs, resource dependencies, etc.  In that case, we need to consider
	// ourselves a new president in the next reconfiguration, so that we
	// fetch state from the slaves and do all the needy RG/R processing
	// at that point.
	//
	// This is ok to do, because the only reason we would return early
	// from this routine is if a node died or joined, causing another
	// imminent reconfiguration.
	//
	// After we have called process_rgs_on_death, we can permanently
	// leave our notion of the president as ourself, because we have
	// read all the state and processed the RGs/Rs.
	//
	// The presidential election can handle our setting the president back
	// to the old president, because joiners in a previous reconfiguration
	// will still be marked as joiners in the new reconfiguration
	// (unless we'd already told joiners to run boot methods, in which
	// case we would also have set our Rgm_state->rgm_president flag to
	// ourself).
	//
	oldpresident = Rgm_state->rgm_President;
	Rgm_state->rgm_President = mynodeid;

	//
	// Only the President advances notion of current membership now.
	// Slaves do it at the request of the President after the
	// President has finished processing node death/join.
	//
	prev_ns = Rgm_state->current_membership;
	Rgm_state->current_membership = ns;
	//
	// Generate a sysevent with the current cluster
	// membership, regardless of whether i am a new
	// president or old.
	//

	res = post_event_membership(prev_ns, ns);
	if (res.err_msg)
		free(res.err_msg);

	//
	// Update LogicalNode objects for each cluster member.
	//
	// This removes zones on non-member nodes from membership;
	// but does not add zones on joining nodes to the membership.
	// It also tells each LN to save its current state, so it can be
	// reverted if we need to exit the reconfiguration early.
	// We can revert to it using revertMembership().
	//
	// Assertions about membership reversion:
	// * We do not release the lock between the call to updateMembership()
	// and revertMemberhip(). Therefore, we cannot find out about
	// zones booting up asynchronosly.
	// * However, we can find out about new LNs from slaves. These LNs
	// may be UP already by the time we find out about them. Therefore,
	// when we revert membership, we must not only revert all lns to
	// saved states, but should mark down any lns in nodes that
	// were temporarily in the membership, but have to be excluded again.
	// * Calling leaveCluster and enterCluster on LogicalNodes is fine
	// because they won't do any of the actions, because this node is
	// the president, and the only time the president does join/death
	// actions on lns is if they are zones on this node. We asserted above
	// that we won't find out about zones on this node.
	//
	lnManager->updateMembership(ns);

	//
	// Find out which physical nodes are joining
	// (who haven't yet been instructed to run boot methods).
	// Makes IDL calls; see block comment at top.
	//
	if (who_is_joining(ns, &join_ns) != RGMIDL_OK) {
		//
		// return early from reconfiguration step
		//
		// Reset the rgm_President flag to the old president.
		// See block comment explanation above.
		//
		Rgm_state->rgm_President = oldpresident;
		Rgm_state->current_membership = prev_ns;
		lnManager->revertMembership();
		rgm_unlock_state();
		return;
	}

	if (join_ns.display(ns_buffer) == 0) {
		ucmm_print("RGM",
			"RRRRRRRRRRRR joining nodeset = %s\n", ns_buffer);
		free(ns_buffer);
	}

#if (SOL_VERSION >= __s10)
	if ((strcmp(ZONE, GLOBAL_ZONENAME) != 0) &&
	    (!join_ns.isEmpty())) {
		//
		// This rgmd is running for a zone cluster. It is possible
		// that the zone boot delay thread for some RG in this zone
		// cluster is waiting for the most preferred zone to come
		// online. It is also possible that the most preferred
		// zone is now online and is a joiner. In such a case, we
		// have to notify the zone boot delay thread and ask it
		// to wake up.
		//
		ucmm_print("RGM", "Notifying the boot delay thread");
		cond_zone_bootdelay.broadcast();
	}
#endif
	//
	// Reset the "node evacuation" bit and timestamp for nodes that
	// are dead or joining, and for all known zones on those nodes.  On a
	// newly booted cluster, this initializes all physical nodes.
	//
	clear_evac_ns = (~ns) | join_ns;
	i = 0;
	while ((i = clear_evac_ns.nextPhysNodeSet(i + 1)) != 0) {
		lnNode = lnManager->findObject(i);
		lnNode->clearEvac();
	}

	//
	// If I'm a NEW President, register with name service
	// under the name RGM_PRESIDENT so that the lower level
	// admin lib can directly call me.
	//
	if (i_am_new_president) {
		ucmm_print("RGM", "RRRRRRRRRRRR I'm NEW President\n");

#ifdef DEBUG
		//
		// If this fault point is enabled, we will sleep for awhile
		// before registering with the name service.  To enable it,
		// create a file /var/tmp/DONT_REGISTER_PRESIDENT, containing
		// the time in seconds that this new president should
		// sleep before registering.  This file is only checked
		// for on the node that is taking over as a new president,
		// but it is harmless to create it on all nodes.
		//
		// Remember to delete the file when you are done testing!!!
		//
		// XXXXXX Replace this with a proper FI framework fault point.
		//
		FAULT_POINT_FOR_UNREGISTERED_PRES();
#endif // DEBUG


		//
		// Before we register the new president we
		// retrieve the existing LNI mappings in the
		// cluster.
		//

		non_joiners = ns - join_ns;

		//
		// Note: there are only mappings to retrieve if
		// we are a new non-joining president.
		// If we are joining, then there are no non-joiners.
		//
		// We should already have mappings in our logical node
		// manager for all physical nodes and all zones known
		// on this node
		//
		if (retrieve_lni_mappings(non_joiners) != 0) {
			//
			// An error just means that a slave node died.
			// Return early from reconfiguration step.
			// sc_syslog message not needed for this case.
			//
			ucmm_print("RGM",
			    "rgm_reconfig: retrieve_lni_mappings() "
			    "failed - assume node died");
			Rgm_state->rgm_President = oldpresident;
			Rgm_state->pres_reconfig = B_FALSE;
			Rgm_state->current_membership = prev_ns;
			lnManager->revertMembership();
			rgm_unlock_state();
			return;
		}

		//
		// Register with the name service.
		//
		register_president();

		FAULTPT_RGM(FAULTNUM_RGM_AFTER_REGISTER_PRESIDENT,
			FaultFunctions::generic);
	}


	// If the physical cluster is booting up, the president has not yet
	// read the CCR. Read it at this time.
	if (!Rgm_state->ccr_is_read) {
		//
		// I'm joining; read the CCR.
		//
		if (rgm_init_ccr().err_code != SCHA_ERR_NOERR) {
			// crash and burn
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "fatal: Failed to read CCR");
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHABLE
		}
		Rgm_state->ccr_is_read = B_TRUE;
	} else {

#if (SOL_VERSION >= __s10)
		if ((strcmp(ZONE, GLOBAL_ZONENAME) != 0) &&
		    i_am_new_president && Rgm_state->is_thisnode_booting) {
			//
			// If this rgmd is running for a zone cluster
			// and if the entire cluster is booting, then
			// this rgmd should make sure that the
			// Ok_to_start flag for all the RGs in the zone
			// cluster have been set appropriately. Please
			// see CR 6770386 for more information. The
			// function rgm_set_ok_to_start_for_all_rg()
			// is called by rgm_init_ccr() also. But
			// for zone clusters, ccr_is_read flag will
			// be true when the zone cluster reboots (it will
			// be false only the first time a zone cluster boots).
			// Hence, for subsequent zone cluster reboots, it
			// is imperative that the new president set the
			// Ok_to_start flag for all RGs accordingly.
			//
			if (rgm_set_ok_to_start_for_all_rg().err_code
			    != SCHA_ERR_NOERR) {
				// crash and burn
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "fatal:Failed to set ok_to_start");
				sc_syslog_msg_done(&handle);
				abort();
				// NOTREACHABLE
			}
		}
#endif

		// load new rts which are not in memory.
		if (rgm_update_rts().err_code != SCHA_ERR_NOERR) {
			// crash and burn
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "fatal: Failed to read CCR");
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHABLE
		}

		//
		// fill in the LNIs of the nodelists and
		// RT installed nodes lists
		//
		// Only needed for new non-joining president. If we are
		// an old pres, we already have all the mappings in the
		// list. If we're a new, joining, pres, lnis are
		// assigned in the call to rgm_init_ccr() above.
		//
		if (i_am_new_president)
			fill_in_lnis();
	}

	//
	// If I'm a NEW President,
	// Start up the thread that processes RMA service state callbacks.
	//
	if (i_am_new_president) {

		// Initialize service state callback from the RMA
		if ((rma_reg_err = init_service_state_callback()) != 0) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RG_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd encountered an error while trying to
			// initialize the Global_resources_used mechanism on
			// this node. This is not considered a fatal error,
			// but probably means that method timeouts will not be
			// suspended while a device service is failing over.
			// This could cause unneeded failovers of resource
			// groups when device groups are switched over.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes. Contact your authorized Sun service provider
			// for assistance in diagnosing the problem. This
			// error might be cleared by rebooting the node.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "ERROR: Failed to initialize callbacks "
			    "for Global_resources_used, error code <%d>",
			    rma_reg_err);
			sc_syslog_msg_done(&handle);
			// Ignore this error.  This failure means that
			// the rgmd won't receive notification of device
			// state changes, but this shouldn't be a fatal error.
		}

		FAULTPT_RGM(FAULTNUM_RGM_AFTER_COND_SIGNAL,
			FaultFunctions::generic);
	}

	//
	// Tell ALL slaves I'm the President, even if I'm an old President.
	// This way joiners also get notified.
	//
	// Note that the rgm_comm_i_am_president call will call rgm_set_pres
	// on the specified slave.  If we are a new president and
	// if the slave is not joining, rgm_set_pres will, in turn, call
	// notify_me_state_change (with the NOTIFY_WAKE flag) on us.
	// Because we set the pres_reconfig flag to
	// B_TRUE, notify_me_state_change will immediately fetch the state
	// from the slave and set it.  Note that if we're joining, all
	// the slaves must be joining too, so none of the above applies.
	//
	// Note that as states are obtained from the slaves,
	// we will call process_needy_rg() on each RG in
	// a "needy" state on the slave.  This is OK because
	// we're only obtaining state from non-joining nodes.
	//
	// However, process_needy_rg will _not_ call
	// rebalance, even if it finds that it is needed,
	// because the pres_reconfig flag is set to B_TRUE.
	// We need to avoid calling rebalance until later for
	// two reasons:
	// 1) We have not yet processed joiners, so we
	// would need to call the ignore_joiners form of
	// rebalance.
	// 2) We do not have the complete state picture on the
	// president yet, so rebalance might not run correctly.
	//
	// A new president will unconditionally call
	// the ignore_joiners form of rebalance on all RGs in
	// the process_rgs_on_death function below.
	//
	// Note also that, because the pres_reconfig flag
	// is set to B_TRUE, check_dependencies_resource
	// will _not_ be called for each resource state change.
	// We will call check_all_dependencies after we have
	// finished updating the entire state.
	//
	Rgm_state->pres_reconfig = B_TRUE;
	slave_nodeset = ns;

#if (SOL_VERSION >= __s10)
	// If this rgmd is running for a zone cluster, then we have to notify
	// all the slaves and all the proxy rgmds as well about the new
	// president
	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		zc_rgmd = B_TRUE;
		CORBA::Exception *exp;
		cmm::membership_t membership;
		cmm::seqnum_t seqnum;
		Environment e;
		LogicalNodeset mem_nodeset;

		membership::api_var membership_api_v =
		    cmm_ns::get_membership_api_ref();
		if (CORBA::is_nil(membership_api_v)) {
			ucmm_print("RGM", "could not get reference "
			    "to membership API object; hence aborting\n");
			abort();
		}

		membership_api_v->get_cluster_membership(
		    membership, seqnum, GLOBAL_ZONENAME, e);
		exp = e.exception();
		if (exp != NULL) {
			abort();
		}

		for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
			if (membership.members[nid] != INCN_UNKNOWN) {
				mem_nodeset.addLni(nid);
			}
		}

		slave_nodeset = mem_nodeset;
	}
#endif

	// This explict loop through physical nodes is correct here
	i = 0;
	while ((i = slave_nodeset.nextPhysNodeSet(i + 1)) != 0) {
		//
		// Skip myself.  It would be harmless but unnecesary to
		// notify ourself.
		//
		if (i == mynodeid)
			continue;

		//
		// Makes IDL calls; see block comment at top.
		//
		// If I am a new non-joining president, this call
		// causes me to fetch state from all non-joining slaves.
		//
		if (rgm_comm_I_am_President(i, mynodeid) != RGMIDL_OK) {
			// If this rgmd is running for a zone cluster, then it
			// is highly likely that we will get this error since
			// the other rgmds might not even have come up. So, we
			// will ignore this error if we are a zone cluster rgmd
			if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
				continue;
			}
			//
			// return early from reconfiguration step
			//
			// Reset the rgm_President flag to the old
			// president.  See block comment explanation
			// above, beginning with
			// "Set the rgm_President state flag" ...
			//
			Rgm_state->rgm_President = oldpresident;
			Rgm_state->pres_reconfig = B_FALSE;
			Rgm_state->current_membership = prev_ns;
			lnManager->revertMembership();
			rgm_unlock_state();
			return;
		}

		//
		// The call to rgm_comm_I_am_president() might have caused
		// set_rx_state() to be called with 'mutex_held == B_FALSE'.
		// Therefore it might be necessary to broadcast the slavewait
		// condition variable.  This will be done by the call to
		// process_rgs_on_death(), below.
		//

		FAULTPT_RGM(FAULTNUM_RGM_AFTER_RGM_COMM_I_AM_PRESIDENT,
			FaultFunctions::generic);
	}

	FAULTPT_RGM(FAULTNUM_RGM_AFTER_LOOP_CALLS_RGM_COMM,
		FaultFunctions::generic);


	// If I'm a NEW, NON-joining President, complete intention processing
	// and check dependencies.
	if (i_am_new_president && !Rgm_state->is_thisnode_booting) {
		//
		// I'm not joining.
		//

		ucmm_print("RGM", "RRRRRRRRRRRR I'm NOT joining\n");

		//
		// It is possible that the old president was running a
		// quiesce command when it died.  The quiescing tags which
		// it added in the nameserver would not get cleaned up.
		// Try to clean all possible tags. This is inefficient, but
		// we can't do better since we don't have any other way out
		// in current implementation.
		//
		rglist_p_t	rg_ptr;
		for (rg_ptr = Rgm_state->rgm_rglist; rg_ptr != NULL;
		    rg_ptr = rg_ptr->rgl_next) {
			quiesce_clear_nameserver(rg_ptr, B_TRUE);
			quiesce_clear_nameserver(rg_ptr, B_FALSE);
		}

#if (SOL_VERSION >= __s10)
		if (allow_inter_cluster()) {
			if (dep_tag_created_by_this_zc())
				cyclic_depchk_clear_nameserver();
		}
#endif
		//
		// Complete intention started by previous President.
		// Cast to void because it doesn't hurt to continue
		// processing intentions in the event of a
		// cluster membership change.
		//
		(void) complete_intention();

		//
		// Set the state on all nodes that are not part of the cluster
		// to OFFLINE.  Also triggers offline-restart dependencies.
		//
		if (rgm_init_pres_state(ns) != RGMIDL_OK) {
			//
			// return early from reconfiguration step
			//
			// Reset the rgm_President flag to the old
			// president.  See block comment explanation
			// above.
			//
			Rgm_state->rgm_President = oldpresident;
			Rgm_state->pres_reconfig = B_FALSE;
			Rgm_state->current_membership = prev_ns;
			lnManager->revertMembership();
			rgm_unlock_state();
			return;
		}

		//
		// Now that we have the complete state picture,
		// we can check if any dependencies are fulfilled,
		// acknowledge the JUST_STARTED state, and trigger
		// restart dependencies.
		//
		// Note that we do this check here, before
		// we have done any rebalancing due to node death
		// or joins.
		//
		check_all_dependencies();
	}

	Rgm_state->pres_reconfig = B_FALSE;

	//
	// Regardless of whether this is a new or old cluster, do
	// rebalancing of RGs affected by node death.  Joiners must be
	// processed after node death is processsed.  We must do this here
	// unconditionally because a new, joining President could die in
	// the middle of reconfiguration.  Depending on the timing, the
	// next President could think this is still a new cluster.
	//
	process_rgs_on_death(i_am_new_president, join_ns, B_FALSE);

	//
	// We have finished processing node deaths.
	// From this point on, we don't need to reset our idea of the pres
	// to the old pres if we return early.
	//
	// Also, we don't need to revert the current membership to the
	// previous membership if we return early.  Joiners will still be
	// identified correctly on the next reconfiguration, because the
	// set of joiners is identified by asking each slave, by calling
	// who_is_joining().  A newly booted slave node continues to consider
	// itself a joiner until it is finished running boot methods.
	//

	FAULTPT_RGM(FAULTNUM_RGM_AFTER_PROCESS_RGS_ON_DEATH,
		FaultFunctions::generic);

	ucmm_print("RGM", "RRRRRRRRRRRR after processing node death\n");

	//
	// Now we can do all aspects of processing joiners.
	//

	//
	// President orders joining nodes (but not himself) to read CCR.
	// Slaves do NOT do this in a separate thread
	// because this operation must be blocking.
	// President cannot issue orders until AFTER slaves
	// have finished reading the CCR.
	// Makes IDL calls; see block comment at top.
	//
	if (joiners_read_ccr(join_ns) != RGMIDL_OK) {

		// return early from reconfiguration step
		rgm_unlock_state();
		return;
	}

	FAULTPT_RGM(FAULTNUM_RGM_AFTER_JOINERS_READ_CCR,
		FaultFunctions::generic);

	//
	// Notify all slaves of current RG freeze states.
	//
	// Some slaves may be joining the cluster; so, we notify the
	// slaves of all frozen RGs, regardless of whether any
	// RG's freeze state has changed.  This will be a no-op on
	// slaves that are already up-to-date.
	// If we're a new president, the init_services callback from the
	// RMA has not yet run because it is waiting for the RGM state
	// mutex.  This means that our RG freeze data is stale, but at
	// least we are getting all slaves in sync with the president.
	// When init_services() runs (after this step releases the rgm
	// mutex), it will update the slaves' RG freeze states again.
	//
	(void) compute_frozen_rgs(&frozen_rgs);
	rgm_change_freeze(frozen_rgs);
	rgm_free_nlist(frozen_rgs);

	//
	// President orders joining nodes (including himself)
	// to run global zone boot methods if they need to.
	// This function also fetches slave state after boot
	// methods are kicked off.
	// Makes IDL calls; see block comment at top.
	//
	// At this point, boot methods on the slave are executed only in
	// the global zone and any non-global zones that have come up
	// already (probably 0).
	//
	// Most non-global zone boot methods are initiated directly by the
	// slave when it gets the UP state transition.
	//
	if (joiners_run_boot_methods(join_ns) != RGMIDL_OK) {

		// return early from reconfiguration step
		rgm_unlock_state();
		return;
	}

	FAULTPT_RGM(FAULTNUM_RGM_AFTER_JOINERS_RUN_BOOT_METHODS,
		FaultFunctions::generic);

	//
	// No need for a new president to call process_needy_rg to do
	// "lost wake" processing here, because it was done above when we
	// ran rebalance in process_rgs_on_death.
	//

	//
	// We've finished processing node death and node join.
	// Tell slaves to record physical node membership now.
	// Makes IDL calls; see block comment at top.
	//
	if (slaves_record_membership(Rgm_state->current_membership.getNodeset())
	    != RGMIDL_OK) {

		// return early from reconfiguration step
		rgm_unlock_state();
		return;
	}

	FAULTPT_RGM(FAULTNUM_RGM_AFTER_SLAVES_RECORD_MEMBERSHIP,
		FaultFunctions::generic);

	ucmm_print("rgm_reconfig", "RRRRRRRRRRRR finished reconfiguration\n");

	rgm_unlock_state();
}

#if (SOL_VERSION >= __s10)
boolean_t reconfig_flag = B_FALSE;

#include <h/naming.h>
#include <nslib/ns.h>
//
// The rgmd for a ZC still continue to run after the zone death/halt.
// When the ZC node boots up the process membership will not do a callback
// routine as it sees the rgmd for the zone cluster still running.
// This is why rgmd registers with zone cluster membership module so
// that the ZCMM does a callback to rgm once the Zone goes up or down.
// When a down call back is recieved the cleanup activities, processing rgs
// on zone death is taken care.
// When an up call back is recieved rgmd notifies the Process membership
// that it is alive. The rebalancing of rgs are handled during this.
//
void rgm_zone_reconfig(const char *cluster, const char *membership_namep,
    const cmm::membership_t membership, const cmm::seqnum_t seqnum)
{
	LogicalNodeset mem_nodeset;
	ucmm_print("rgm_zone_reconfig", "seqnum = %llu\n", seqnum);
	rgm_lock_state();
	if (Rgm_state->zc_membership_generation >= seqnum) {
		//
		// A newer reconfig callback has been serviced
		// while we were not running
		//
		ucmm_print("rgm_zone_reconfig",
		    "not servicing stale seqnum = %llu\n", seqnum);
		rgm_unlock_state();
		return;
	}

	// Get the zc membership
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (membership.members[nid] != INCN_UNKNOWN) {
			ucmm_print("rgm_zone_reconfig", "node %d up, incn %d\n",
			    nid, membership.members[nid]);
			mem_nodeset.addLni(nid);
		} else {
		}
	}
	// Update the comm structures because we might have to run boot
	// methods.
	rgm_comm_update(mem_nodeset.getNodeset());

	if (!mem_nodeset.containLni(orb_conf::local_nodeid())) {
	}

	// Check if this zone has come up using the prev and present
	// membership information
	LogicalNodeset prev_ns = Rgm_state->current_zc_membership;
	Rgm_state->current_zc_membership = mem_nodeset;
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (membership.members[nid] != INCN_UNKNOWN) {
			ucmm_print("rgm_zone_reconfig", "node %d up, incn %d\n",
			    nid, membership.members[nid]);
		}
	}
	nodeid_t nid = orb_conf::local_nodeid();
	if ((!prev_ns.containLni(nid)) &&
	    (membership.members[nid] != INCN_UNKNOWN)) {
		ucmm_print("rgm_zone_reconfig",
		    "telling czmm that nid %d process up\n",
		    nid);

		Rgm_state->is_thisnode_booting = B_TRUE;
		//
		// Notify membership subsystem
		// that process has come up
		//
		uint32_t clid = 0;
		if (clconf_get_cluster_id(ZONE, &clid)) {
			//
			// Must be able to convert
			// cluster name to cluster id
			//
			ASSERT(0);
		}
		membership::engine_var engine_v =
		    cmm_ns::get_membership_engine_ref();
		ASSERT(!CORBA::is_nil(engine_v));
		membership::engine_event_info event_info;
		event_info.mem_manager_info.mem_type =
		    membership::PROCESS_UP_ZONE_DOWN;
		event_info.mem_manager_info.cluster_id = clid;
		//
		// Casting as char* while assigning to
		// string field does not allocate fresh memory
		//
		event_info.mem_manager_info.process_namep =
		    os::strdup(RGMD_PROC_NAME);
		event_info.event = membership::PROCESS_UP;
		Environment e;
		CORBA::Exception *exp = NULL;
		engine_v->deliver_event_to_engine(
		    membership::DELIVER_MEMBERSHIP_CHANGE_EVENT,
		    event_info, e);
		if ((exp = e.exception()) != NULL) {
			e.clear();
			ASSERT(0);
		}
	}
	lnManager->updateMembership(mem_nodeset);
	Rgm_state->zc_membership_generation = seqnum;
	Rgm_state->zc_node_incarnations = membership;

	rgm_unlock_state();
}

//
// RGM receives a process membership callback here.
// We signal the RGM reconfiguration thread here,
// which will drive the reconfiguration.
//
void rgm_process_membership_callback(
    const char *cluster_namep, const char *membership_namep,
    const cmm::membership_t membership, const cmm::seqnum_t seqnum)
{
	nodeset_t mem_nodeset = 0;
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (membership.members[nid] != INCN_UNKNOWN) {
			nodeset_add(mem_nodeset, nid);
		}
	}
	ucmm_print("rgm_process_membership_callback",
	    "nodeset %lld, seqnum %d\n", mem_nodeset, (uint32_t)seqnum);

	rgm_process_membership_callback_internal(membership, seqnum);
}

void rgm_process_membership_callback_internal(
    const cmm::membership_t membership, const cmm::seqnum_t seqnum)
{
	rgm_reconfig_lock.lock();
	if (rgm_reconfig_membership_cb_data.seqnum >= seqnum) {
		// This callback is for a stale seqnum. Ignore this callback.
		ucmm_print("rgm_process_membership_callback_internal",
		    "ignoring stale callback, stale seqnum %d, "
		    "known seqnum %d\n", (uint32_t)seqnum,
		    (uint32_t)rgm_reconfig_membership_cb_data.seqnum);
		rgm_reconfig_lock.unlock();
		return;
	}
	ucmm_print("rgm_process_membership_callback_internal",
	    "updated membership callback data, old seqnum %d, new seqnum %d\n",
	    (uint32_t)rgm_reconfig_membership_cb_data.seqnum,
	    (uint32_t)seqnum);
	rgm_reconfig_membership_cb_data.membership = membership;
	rgm_reconfig_membership_cb_data.seqnum = seqnum;
	rgm_reconfig_membership_changed = B_TRUE;
	rgm_reconfig_cv.signal();
	rgm_reconfig_lock.unlock();

	//
	// Signal the reconfiguration thread that might be waiting
	// for communication from peer reconfig threads.
	//
	peer_rgm_state_lock.lock();
	peer_rgm_state_cv.signal();
	peer_rgm_state_lock.unlock();
}

//
// Step 1 in step synchronized RGM reconfiguration initiated by
// a process membership callback.
// We update local membership data in this step.
// Returns true if step succeeds fine, and returns false on failure.
//
bool rgm_reconfiguration_step1(
    const cmm::membership_t membership, const cmm::seqnum_t seqnum)
{

	nodeset_t mem_nodeset = 0;
	ucmm_print("rgm_reconfiguration_step1",
	    NOGET("seqnum = %d\n"), (uint32_t)seqnum);

	rgm_lock_state();
	if (Rgm_state->using_process_membership &&
	    (Rgm_state->rgm_membership_generation >= seqnum)) {
		//
		// A newer reconfig callback has been serviced
		// while we were not running
		//
		ucmm_print("rgm_reconfiguration_step1",
		    NOGET("not servicing stale seqnum = %d\n"),
		    (uint32_t)seqnum);
		rgm_unlock_state();
		return (false);
	}

	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (membership.members[nid] != INCN_UNKNOWN) {
			ucmm_print("rgm_reconfiguration_step1",
			    NOGET("node %d up, incn %d\n"),
			    nid, membership.members[nid]);
			nodeset_add(mem_nodeset, nid);
		}
	}

	Rgm_state->rgm_membership_generation = seqnum;
	Rgm_state->node_incarnations = membership;
	ucmm_print("rgm_reconfiguration_step1",
	    NOGET("node %d: rgm_reconfig:%d, nodeset:%lld\n"),
	    orb_conf::local_nodeid(), (uint32_t)seqnum, mem_nodeset);

	rgm_unlock_state();
	return (true);
}

//
// Step 2 in step synchronized RGM reconfiguration initiated by
// a process membership callback.
// Returns true if step succeeds fine, and returns false on failure.
// We update the rgm_comm references in this step.
// We do the big, uninterruptible action of RGM reconfiguration in this step.
//
bool rgm_reconfiguration_step2(
    const cmm::membership_t, const cmm::seqnum_t)
{

	nodeset_t mem_nodeset = 0;
	ucmm_print("rgm_reconfiguration_step2", NOGET("seqnum = %d\n"),
	    (uint32_t)Rgm_state->rgm_membership_generation);

	FAULTPT_RGM(FAULTNUM_RGM_BEFORE_RGM_LOCK_STATE,
		FaultFunctions::generic);

	rgm_lock_state();
	cmm::membership_t membership = Rgm_state->node_incarnations;

	// Populate nodeset with the latest known membership
	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		if (membership.members[nid] != INCN_UNKNOWN) {
			nodeset_add(mem_nodeset, nid);
		}
	}
	ucmm_print("rgm_reconfiguration_step2",
	    NOGET("node %d: rgm_reconfig:%d, nodeset:%lld\n"),
	    orb_conf::local_nodeid(),
	    (uint32_t)Rgm_state->rgm_membership_generation,
	    mem_nodeset);

	// Adjust object references according to new membership.
	FAULTPT_RGM(FAULTNUM_RGM_BEFORE_RGM_COMM_UPDATE,
		FaultFunctions::generic);

	// Update rgm comm references
	rgm_comm_update(mem_nodeset);

	FAULTPT_RGM(FAULTNUM_RGM_AFTER_RGM_COMM_UPDATE,
		FaultFunctions::generic);

	// The real RGM big, uninterruptible reconfiguration
	rgm_reconfig_helper((LogicalNodeset)mem_nodeset, B_TRUE);

	return (true);
}
#endif

// Called by ucmm-style automaton membership
void
rgm_reconfig(LogicalNodeset &ns, boolean_t isucmm)
{

	FAULTPT_RGM(FAULTNUM_RGM_BEFORE_RGM_LOCK_STATE,
		FaultFunctions::generic);

	rgm_lock_state();

	// Adjust object references according to new membership.
	FAULTPT_RGM(FAULTNUM_RGM_BEFORE_RGM_COMM_UPDATE,
		FaultFunctions::generic);

	if (isucmm) {
		rgm_comm_update(ns.getNodeset());
	}

	FAULTPT_RGM(FAULTNUM_RGM_AFTER_RGM_COMM_UPDATE,
	    FaultFunctions::generic);

	rgm_reconfig_helper(ns, B_TRUE);
}

//
// elect_president
//
// Sets i_am_president TRUE if I'm the president.
// Sets i_am_new_president TRUE if I'm the NEW president.
// Returns RGMIDL_OK if there were no exceptions during IDL calls.
// Otherwise returns a value informing the caller to return early from
// the big reconfiguration step.
//
// Does not change the value of Rgm_state->rgm_president, even if we
// are a newly elected president.  The caller (rgm_reconfig) will change it
// when it needs to do so.
//
idlretval_t
elect_president(nodeset_t ns,
    boolean_t *i_am_president,
    boolean_t *i_am_new_president)
{
	idlretval_t retval;
	sol::nodeid_t i;
	sol::nodeid_t mynodeid;
	boolean_t is_joining;

	*i_am_new_president = B_FALSE;
	*i_am_president = B_FALSE;

	mynodeid = orb_conf::local_nodeid();

	ucmm_print("elect_president", "point0\n");
	//
	// First thing is to check whether I was the
	// President in last reconfiguration? If yes,
	// and if I am still in current membership,
	// then I will continue to be the president. I
	// should also make sure that I am not booting
	// before I conclude that I am a surviving
	// president. I cannot be a surviving president,
	// if I am booting into cluster mode.
	//
	if (Rgm_state->rgm_President == mynodeid &&
	    nodeset_contains(ns, Rgm_state->rgm_President) &&
	    (!Rgm_state->is_thisnode_booting)) {
		// yes, I'm still the President
		*i_am_president = B_TRUE;
		return (RGMIDL_OK);
	}

	//
	// If I was the last president but I am not in the
	// current membership, then I can no longer be the
	// president. This scenario arises when a zone, part of
	// a zone cluster is halted. In such a case, a new
	// president has to be elected. See bug 6722618 for
	// more information. This case can be generalized. If
	// I am not in the membership, then I should not
	// participate in the election. This case can arise
	// for zone clusters.
	//
	if (!nodeset_contains(ns, mynodeid)) {
		return (RGMIDL_OK);
	}

	//
	// Is the old President still in office?
	//
	if (Rgm_state->rgm_President != PRES_UNKNOWN &&
	    nodeset_contains(ns, Rgm_state->rgm_President)) {
		ucmm_print("elect_president", "point1\n");
		//
		// The guy I thought was President is in the current
		// membership.  Is he really a survivor, or did he
		// die and quickly rejoin?
		//
		if ((retval = are_you_joining(Rgm_state->rgm_President,
		    &is_joining)) != RGMIDL_OK) {
			// return early
			return (retval);
		}

		if (!is_joining) {
			// That guy is still the President, so I'm not
			// going to be elected.
			return (RGMIDL_OK);
		}
		// That guy was a faker.  Keep looking at other candidates.
	}
	ucmm_print("elect_president", "point2");

	//
	// If I'm joining, look for a member with a lower nodeid than mine.
	// If one exists, I'm not the President.
	//
	if (Rgm_state->is_thisnode_booting) {
		ucmm_print("elect_president", "point3\n");
		for (i = 1; i < mynodeid; i++) {
			if (nodeset_contains(ns, i)) {
				// I cannot be the President because I'm
				// joining and there is another lower nodeid
				// in the cluster.
				return (RGMIDL_OK);
			}
		}

		//
		// I'm joining, but there are no nodes with a lower nodeid
		// than mine.  Look for a surviving node with a higher
		// nodeid than mine.
		//
		ucmm_print("elect_president", "point4\n");
		for (i = mynodeid + 1; i <= MAXNODES; i++) {
			if (!nodeset_contains(ns, i))
				continue;

			if ((retval = are_you_joining(i, &is_joining))
			    != RGMIDL_OK) {
				return (retval);
			}

			ucmm_print("elect president",
			    "%d returned joining as%d:"
			    " true is:%d", i, is_joining, B_TRUE);
			if (!is_joining) {
				// there is a higher-numbered surviving
				// node, so I'm not the President
				return (RGMIDL_OK);
			}
		}
		ucmm_print("elect_president", "point5\n");

		//
		// Nobody is surviving, and I'm the lowest numbered node.
		// (Everybody is joining.)  I'm the NEW President.
		//
		*i_am_new_president = B_TRUE;
		*i_am_president = B_TRUE;
		return (RGMIDL_OK);
	}

	//
	// I'm surviving, not joining.
	// Look for a lower-numbered surviving member.
	//
	ucmm_print("elect_president", "point6\n");
	for (i = 1; i < mynodeid; i++) {
		if (!nodeset_contains(ns, i))
			continue;

		if ((retval = are_you_joining(i, &is_joining))
		    != RGMIDL_OK)
			// return early
			return (retval);

		if (!is_joining) {
			// I cannot be the President because there
			// is a lower numbered surviving node
			// in the cluster.
			return (RGMIDL_OK);
		}
	}

	ucmm_print("elect_president", "point7\n");
	//
	// No lower-numbered surviving nodes were found.
	// I'm the new President.
	//
	*i_am_new_president = B_TRUE;
	*i_am_president = B_TRUE;
	return (RGMIDL_OK);
}


//
// rgm_init_pres_state
// Input is current membership nodeset.
// Only a new non-joining President runs this routine during reconfiguration.
// Sets all RG and R state for each
// node that is not part of the cluster to OFFLINE.
// Triggers restart-dependencies for all resources in such RGs.
//
// Note: The implementation of r_xstate and rg_xstate as a C++ map starting
// in SC 3.2 effectively initializes R/RG states to OFFLINE for all zones.
// Therefore, we might not still need to explicitly reset the states.
// However, the new president still needs to call this function so that
// reset_rg_offline() gets called for all RGs for all nodes that died, so
// that offline-restart dependencies are triggered.
// See CRs 6345204 and 6549509 for further information .
//
static idlretval_t
rgm_init_pres_state(const LogicalNodeset &ns)
{
	sol::nodeid_t i;

	i = 0;
	LogicalNodeset conf_ns(Rgm_state->static_membership);
	while ((i = conf_ns.nextPhysNodeSet(i + 1)) != 0) {
		// Skip myself, because I already know my state.
		if (i == orb_conf::local_nodeid()) {
			continue;
		}

		if (!ns.containLni(i)) {
			//
			// Node i is not currently participating in cluster;
			// set all states to offline for this node.
			// We do this now so that when the node rejoins the
			// cluster, state is already properly initialized.
			//
			rgm_pres_set_all_offline(i);
			continue;
		}
	}
	return (RGMIDL_OK);
}


//
// rgm_pres_set_all_offline
// Called only by the new President, only for a node that is dead.
// Sets all RG and R states for the specified node to offline in our
// internal state structure and triggers restart dependencies.
//
// Does *not* clear the start_failed time stamp history for the RG
// on that node; as long as the president stays up, it retains the
// history of START failures for other nodes, even through reboots.
// Caller has lock on state structure.
//
static void
rgm_pres_set_all_offline(sol::nodeid_t nodeid)
{
	rglist_p_t rglp;

	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		reset_rg_offline(rglp, nodeid, B_TRUE);
	}
}


//
// rgm_update_pres_state
//
// Takes as input a nodeid and the sequence of RG state information sent
// by that node to the President via the ORB.  Updates the per-zone state
// of each RG and its R's in our internal state structure.
// Our caller holds the lock on the state structure.
//
// Note that only non-"interesting" (i.e., non offline) states are included
// in the sequence.
//
// The 'nodeid' argument is not really used any more since the node_state
// sequence is now a sequence of <zone_state> and each element contains
// the lni of the particular zone on the slave (including the global zone).
// As a sanity check, we assert that the physical nodeid for each lni is
// equal to 'nodeid'.
//
void
rgm_update_pres_state(sol::nodeid_t nodeid, rgm::node_state *state)
{
	ucmm_print("RGM",
	    "rgm_president.cc: rgm_update_pres_state, nodeid = <%d>, "
	    "len = <%d>\n", nodeid, state->seq.length());

	//
	// Iterate over the zones in the sequence.
	// For each zone, iterate over the RGs in the zone sequence.
	// Call unpack_rg_seq once for each RG in the sequence.
	//
	for (uint_t i = 0; i < state->seq.length(); i++) {
		rgm::zone_state zstate = state->seq[i];

		// the following 3 lines are just a sanity check
		LogicalNode *lnNode = lnManager->findObject(zstate.lni);
		CL_PANIC(lnNode);
		CL_PANIC(lnNode->getNodeid() == nodeid);

		for (uint_t j = 0; j < zstate.zseq.length(); j++) {
			unpack_rg_seq(zstate.lni, zstate.zseq[j]);
		}
	}
	// Caller frees 'state'.
}


//
// syslog_fmstatus_msg
// Called on the President.
// Syslog messages when there is a change in FM status or FM status message
//
void
syslog_fmstatus_msg(const char *rs_name, const char *nodename,
    rgm::rgm_r_fm_status old_fm_status, rgm::rgm_r_fm_status new_fm_status,
    const char *old_fm_msg, const char *new_fm_msg)
{
	sc_syslog_msg_handle_t  handle;
	boolean_t	msg_changed = B_FALSE;

	(void) sc_syslog_msg_initialize(&handle,
	    SC_SYSLOG_RGM_RS_TAG, rs_name);
	if (old_fm_status != new_fm_status) {
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that a resource's
		// fault monitor status has changed.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE,
		    STATE_CHANGED, SYSTEXT(
		    "resource %s status on node %s change to %s"),
		    rs_name, nodename, pr_rfmstatus(new_fm_status));
	}

	//
	// At least one of these values is not NULL.
	// If both are non-NULL, compare them
	//
	if ((old_fm_msg == NULL && new_fm_msg != NULL) ||
	    (old_fm_msg != NULL && new_fm_msg == NULL))
		msg_changed = B_TRUE;

	if ((old_fm_msg != NULL && new_fm_msg != NULL) &&
	    (strcmp(old_fm_msg, new_fm_msg) != 0))
		msg_changed = B_TRUE;

	if (msg_changed) {
		if (new_fm_msg != NULL) {
			//
			// SCMSGS
			// @explanation
			// This is a notification from the rgmd that a
			// resource's fault monitor status message has
			// changed.
			// @user_action
			// This is an informational message; no user action is
			// needed.
			//
			(void) sc_syslog_msg_log(handle,
			    LOG_NOTICE, STATE_CHANGED,
			    SYSTEXT("resource %s status msg "
			    "on node %s change to <%s>"),
			    rs_name, nodename,
			    new_fm_msg);
		} else {
			(void) sc_syslog_msg_log(handle,
			    LOG_NOTICE, STATE_CHANGED,
			    SYSTEXT("resource %s status msg "
			    "on node %s change to <%s>"),
			    rs_name, nodename, "");
		}
	}
	sc_syslog_msg_done(&handle);
}


//
// unpack_rg_seq
//
// Update the president's in-memory state with the RG state fetched from
// a slave node.  Called by rgm_update_pres_state() as a result of a
// wake_president call or during reconfiguration.
//
// Lock is already held on state structure.
//
// Processes status changes by queueing them up on the status change
// threadpool.  We do that to ensure that they are serialized properly
// with the asynchronous status changes that come in.
//
static void
unpack_rg_seq(rgm::lni_t lni, rgm::rg_state rgseq_ref)
{
	rglist_p_t		rgp;
	rlist_p_t		rp;
	sc_syslog_msg_handle_t	handle;
	notify_status_change_task *notify_task = NULL;

	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode);

	if ((rgp = rgname_to_rg(rgseq_ref.idlstr_rgname)) == NULL) {
		char *rgname = rgseq_ref.idlstr_rgname;
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Due to an internal error, the rgmd daemon was unable to
		// find the specified resource group data in memory.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes.
		// Contact your authorized Sun service provider for assistance
		// in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "ERROR: unpack_rg_seq(): rgname_to_rg failed <%s>"),
		    rgname);
		sc_syslog_msg_done(&handle);
		return;
	}


	//
	// Now get the sequence of Rs for this RG.
	//
	for (uint32_t j = 0; j < rgseq_ref.rlist.length(); j++) {
		if ((rp = rname_to_r(rgp, rgseq_ref.rlist[j].idlstr_rname))
		    == NULL) {
			char *rname = rgseq_ref.rlist[j].idlstr_rname;
			//
			// Resource not found; we assume it has been
			// deleted and just continue to the next resource.
			// This might occur if we are a newly-elected president,
			// and the intention to delete the resource is still
			// being processed on slave nodes.
			//
			ucmm_print("unpack_rg_seq", NOGET(
			    "Resource <%s> not found, skipping"), rname);
			continue;
		}


		//
		// Queue a status change task.
		//
		notify_task = new notify_status_change_task(
		    lni, rp->rl_ccrdata->r_name,
		    rp->rl_rg->rgl_ccr->rg_name,
		    rgseq_ref.rlist[j].fmstatus,
		    (const char *)(rgseq_ref.rlist[j].idlstr_status_msg));

		notify_status_change_threadpool::the().defer_processing(
		    notify_task);
		// notify_task will be deleted by the thread that processes it.

		//
		// Pass B_FALSE for the 'mutex_held' argument to set_rx_state().
		// In this code path, we are a new president doing an
		// 'immediate' fetch of rg state from slaves.  A different
		// thread -- rgm_reconfig_helper() -- holds the state mutex.
		//
		set_rx_state(lni, rp, rgseq_ref.rlist[j].rstate, B_FALSE,
		    B_FALSE);
	}
	//
	// Note that we populate the in memory structure with the
	// resource state information first and then proceed with
	// updating the RG state information.
	// Also, set_rgx_state set's the RG state to PENDING_ONLINE
	// in case the RG state is PENDING_ONLINE_BLOCKED and none
	// of the resources in the RG are in STARTING_DEPEND.
	// By setting the RG state before resource states, we could end
	// up in a scenario (CR 6402407) where the RG state is
	// PENDING_ONLINE and one or more resources state in
	// STARTING_DEPEND.
	//

	//
	// Prevent the rebalance while we're unpacking the sequence.  The
	// caller of this function will call rebalance, if needed.
	//
	rgp->rgl_no_rebalance = B_TRUE;
	set_rgx_state(lni, rgp, rgseq_ref.rgstate);
	rgp->rgl_no_rebalance = B_FALSE;
}


//
// process_needy_rg
//
// This function is called by set_rgx_state on the president only, when
// the president is recording an RG state change on a slave zone (which might
// be on the president itself).
// There are two cases in which this function is called: when the president
// processes a state change from a slave; and when the president
// (acting as a slave itself) updates the state of an RG locally.
//
// The following cases are handled by this function:
//
//	1. START failure - bust any in-process switches on the RG; update the
//	   start failure history for the RG on that node.  Rebalance will
//	get called by process_needy_rg when we process the OFFLINE state.
//
//	2. STOP failure - bust any in-process switches on the RG; syslog an
//	   error message.
//
//	3. BOOT method completion - reset the RG state to OFFLINE.  The OFFLINE
//	case will run failback.
//
//	4. RG is ONLINE- clear the president's start failure history for the
//	RG on that node.
//
//	5. RG is OFFLINE.  If there is no switch in progress, we assume that
//	the RG was forced offline due to an affinities violations, so we run
//	rebalance.  If the previous state was OFF_BOOTED, run failback
//	instead.  If the previous state was OFF_PENDING_METHODS or
//	ERROR_STOP_FAILED, do not run rebalance.
//
//	6. RG is PENDING_ONLINE.  Any RGs with strong positive affinities for
//	the RG that went pending online can now start on the node.  So call
//	rebalance on each RG that has a strong positive affinity for the RG.
//
// Following is an explanation of "busting" an in-process switch:
//
// While an scswitch or failback is in progress on an RG
// (as indicated by rgl_switching value of SW_IN_PROGRESS or
// SW_IN_FAILBACK), it may happen that a potential
// master of the RG dies, or a START or STOP method on one of the
// resources in the RG fails. If any of these events occurs, then the logic
// in do_rg_switch() may not be able to achieve the desired "goal state"
// of mastery of the RG. Instead of continuing the do_rg_switch algorithms,
// we "bust" the switch by setting the rgl_switching flag of the RG to one
// of the "busted" values:
//	SW_BUSTED_RECONF -	switch busted by node death
//	SW_BUSTED_START -	switch busted: start method failed
//	SW_BUSTED_STOP -	switch busted: stop method failed
//
// When do_rg_switch() discovers one of these setting, it immediately
// abandons further processing on the RG. The thread that busts the
// switch will also call rebalance() to attemp to bring the RG online
// on the N most-preferred primaries, where N=Desired_primaries;
// however, rebalance() is not called for the stop-failed case,
// since an RG in ERROR_STOP_FAILED state requires operator attention
// before it can be brought online on any new masters.
//
// If an RG is being edited (as indicated by an rgl_switching value
// of SW_UPDATING) and a current master of the RG dies, we "bust" the
// edit by setting rgl_switching to SW_UPDATE_BUSTED.  Note that the editing
// thread releases the RGM state lock only while running methods (VALIDATE
// before the CCR is updated; UPDATE, INIT or FINI after the update is
// committed to the CCR) and the edit can be busted only during one
// of these times that the lock is released.  If the edit is busted during
// VALIDATE, we immediately fail the edit with an error and no side-effects.
// If the edit is busted during UPDATE, INIT or FINI methods, then we let the
// methods run to completion.  In all cases where an edit is busted, the
// editing thread will run rebalance() after the editing action is completed
// or aborted.
//
// The setting of SW_BUSTED_RECONF or SW_UPDATE_BUSTED for node death is
// done in process_rg() which is called by the big reconfiguration step
// (rgm_reconfig()).  The setting of SW_BUSTED_START for start failure
// or SW_BUSTED_STOP for stop failure is done by process_needy_rg()
// whenever the president records a start-failed or stop-failed RG state
// from a slave.
//
// The president-side functions that call either set_rgx_state() or
// process_rg() always call cond_broadcast(&cond_slavewait) afterward, so
// that any threads that are sleeping waiting for an RG state change
// will wake up and take note of the new RG state.
//
void
process_needy_rg(rgm::lni_t lni, rglist_p_t rgp, rgm::rgm_rg_state old_rg_state)
{
	rgm_timelist_t	**rgtp;
	sc_syslog_msg_handle_t handle;
	int	myerrno;

	// See if further action (rebalance, failback, bust, clear start-fail
	// history) is required on the RG
	switch (rgp->rgl_xstate[lni].rgx_state) {
	case rgm::RG_OFF_BOOTED:
		ucmm_print("RGM",
		    "RRRRRRRRRRRR in process_needy_rg <%s> OFF_BOOTED\n",
		    rgp->rgl_ccr->rg_name);
		//
		// The slave has just booted.  If the slave is a potential
		// master of the RG, the President will determine whether
		// it is necessary to bring up RG on joining node.
		//
		// Reset the rg's state on the slave to OFFLINE.
		// We do not run failback yet.  When we get the OFFLINE
		// transition from the slave we will run failback.
		//
		reset_rg_state(lni, rgp);
		break;

	case rgm::RG_OFFLINE_START_FAILED:
		ucmm_print("RGM",
		    "RRRRRRRR in process_needy_rg <%s> OFFLINE_START_FAILED\n",
		    rgp->rgl_ccr->rg_name);
		//
		// An attempt to start the rg on zone <lni> has failed.
		//
		// If a switch (or scha_control() call) or failback is in
		// progress, "bust" it.  The switching thread will stop
		// processing the busted RG as soon as it wakes up from
		// cond_wait().
		//
		// Note: cond_broadcast() gets called after we return from
		// this routine; see block comment above.  The switching
		// thread will see the new (busted) state on the RG in
		// both cases: a) slave is not the President and b) slave
		// is the President.
		//
		// SW_EVACUATING need not be checked here because we do not
		// "bust" node evacuation.
		//
		if (rgp->rgl_switching == SW_IN_PROGRESS ||
		    rgp->rgl_switching == SW_IN_FAILBACK) {
			ucmm_print("RGM",
			    NOGET("process_needy_rg: start of RG <%s> on "
			    "node <%d> failed; busting scswitch or failback "
			    "in progress\n"), rgp->rgl_ccr->rg_name, lni);
			rgp->rgl_switching = SW_BUSTED_START;
		}

		//
		// We don't call rebalance yet. Rebalance will get called
		// when we get the OFFLINE transition from the slave.
		//

		//
		// Append a time stamp containing the current clock time
		// to the RG's start-failed list.
		// rebalance() will prune old entries from the list and
		// implement the start-failure ping-pong algorithm.
		//
		rgtp = &rgp->rgl_xstate[lni].rgx_start_fail;
		myerrno = rgm_append_to_timelist(rgtp);
		// Inability to store timestamp is non-fatal
		if (myerrno == ENOMEM) {
			// We're out of swap, so don't try to syslog a message!
			// Write to console instead
			(void) fprintf(stderr, "RGM: Warning: out of swap "
			    "space; cannot store start-failed timestamp for "
			    "resource group <%s>\n", rgp->rgl_ccr->rg_name);
			break;
		} else if (myerrno != 0) {
			const char *errmesg = strerror(myerrno);

			if (errmesg == NULL) {
				errmesg = "no such errno";
			}
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The specified resource group failed to come online
			// on some node, but this node is unable to record
			// that fact due to the failure of the time(2) system
			// call. The consequence of this is that the resource
			// group may continue to pingpong between nodes for
			// longer than the Pingpong_interval property setting.
			// @user_action
			// Examine other syslog messages occurring around the
			// same time on the same node, to see if the cause of
			// the problem can be identified. If the same error
			// recurs, you might have to reboot the affected node.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "warning: cannot store start_failed timestamp for "
			    "resource group <%s>: time() failed, errno <%d> "
			    "(%s)"),
			    rgp->rgl_ccr->rg_name, myerrno, errmesg);
			sc_syslog_msg_done(&handle);
			break;
		}
#ifdef DEBUG
		char timebuf[32];

		ucmm_print("RGM",
		    NOGET("process_needy_rg: adding start-failed "
		    "timestamp <%s> for RG <%s> on node <%d>\n"),
		    os::tod::ctime_r(&((*rgtp)->tl_time), timebuf, 32),
		    rgp->rgl_ccr->rg_name, lni);
#endif // DEBUG

		//
		// Reset the RG's state on the slave to OFFLINE.
		// If the slave is the president, it will change our
		// state directly.  We run rebalance when the state moves to
		// RG_OFFLINE, so that means that rebalance will get called
		// as a side effect of this call if we're the president.
		//
		reset_rg_state(lni, rgp);

		break;

	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_ERROR_STOP_FAILED:
		ucmm_print("RGM",
		    "RRRRRRRRRRRR in process_needy_rg <%s> STOP_FAILED\n",
		    rgp->rgl_ccr->rg_name);
		//
		// The slave got into this state from PENDING_ONLINE or
		// PENDING_OFFLINE.
		// If scswitch or RG failback is in progress, bust it.
		// No need to call rebalance() since it is illegal to bring
		// the RG online on any node until the stop-failed condition
		// is cleared.
		//
		// SW_EVACUATING need not be checked here because we do not
		// "bust" node evacuation.
		//
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that a resource group
		// has had a STOP method failure or timeout on one of its
		// resources. The resource group is in ERROR_STOP_FAILED
		// state. This may cause another operation such as clresource
		// or scha_control(1HA,3HA) to fail with a SCHA_ERR_STOPFAILED
		// error.
		// @user_action
		// Refer to the procedure for clearing the ERROR_STOP_FAILED
		// condition on a resource group in the Sun Cluster
		// Administration Guide.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "Resource group <%s> requires operator attention due "
		    "to STOP failure"), rgp->rgl_ccr->rg_name);
		sc_syslog_msg_done(&handle);

		if (rgp->rgl_switching == SW_IN_PROGRESS ||
		    rgp->rgl_switching == SW_IN_FAILBACK) {
			ucmm_print("RGM",
			    NOGET("process_needy_rg: stop of RG <%s> on "
			    "node <%d> failed; busting scswitch/failback in"
			    "progress\n"), rgp->rgl_ccr->rg_name, lni);
			rgp->rgl_switching = SW_BUSTED_STOP;
		}
		break;

	case rgm::RG_ONLINE:
	case rgm::RG_ON_PENDING_R_RESTART:
		ucmm_print("RGM",
		    "RRRRRRRRRRRR in process_needy_rg <%s> RG_ONLINE\n",
		    rgp->rgl_ccr->rg_name);
		//
		// The RG has started successfully on lni; clear the
		// start-failure history for all nodes.
		//
		/* CSTYLED */
		for (std::map<rgm::lni_t, rg_xstate>::iterator it =
		    rgp->rgl_xstate.begin();
		    it != rgp->rgl_xstate.end(); ++it) {

			rgm_free_timelist(&it->second.rgx_start_fail);
		}
		break;

	case rgm::RG_OFFLINE:

		ucmm_print("RGM",
		    NOGET("process_needy_rg <%s> OFFLINE\n"),
		    rgp->rgl_ccr->rg_name);

		//
		// If the old state was OFF_BOOTED, we need to check
		// if we should run failback.  Otherwise we run rebalance.
		//
		if (old_rg_state == rgm::RG_OFF_BOOTED) {
			check_run_failback(rgp, lni);
		} else if (old_rg_state != rgm::RG_OFF_PENDING_METHODS &&
		    old_rg_state != rgm::RG_ERROR_STOP_FAILED) {

			//
			// The RG has gone offline.  We don't know a priori
			// whether or not this state is intended.  If there
			// is a switch or failback in progress, we assume that
			// it was intended, so we do not run rebalance.
			//
			// If there is no switch in progress (or if it is
			// busted), we need to run rebalance on the RG.
			//
			// If the RG is evacuating, we run rebalance() which
			// will exclude the evacuating node(s) from candidacy.
			//
			// If the RG is switching or in failback we still need
			// to run rebalance on the strong negative affinitents.
			// do_rg_switch will have set the pending_on_nodes to
			// the nodes on which the RG is planning to come online
			// as a result of the switch.
			// run_rebalance_on_affinitents will pass these nodes
			// to rebalance, so that rebalance will not try to
			// bring the strong-negative affinitents online on
			// those nodes.
			//
			if (rgp->rgl_switching == SW_IN_PROGRESS ||
			    rgp->rgl_switching == SW_IN_FAILBACK) {
				//
				// Run rebalance on all RGs that have strong
				// negative affinities for RG, as they may be
				// allowed to come online.
				// Set the cleanup_stale_affs to true.
				//
				run_rebalance_on_affinitents(rgp, B_FALSE,
				    B_TRUE);

				//
				// We don't need to rebalance this rg itself,
				// because it is still switching, so break
				// out of this case.
				//
				ucmm_print("RGM",
				    NOGET("process_needy_rg: RG is switching, "
				    "so not running rebalance\n"));
				break;
			}

			//
			// If we're here, the RG is not switching or in
			// failback.  Call rebalance() on the RG.
			//
			run_rebalance(rgp);

			//
			// Now that we have run rebalance on the RG,
			// run rebalance on all RGs that have strong
			// negative affinities for it, as they may be
			// allowed to come online.
			// Set the cleanup_stale_affs to true.
			//
			run_rebalance_on_affinitents(rgp, B_FALSE, B_TRUE);
		} else {
			ucmm_print("RGM",
			    NOGET("process_needy_rg: RG was "
			    "OFF_PENDING_METHODS or ERROR_STOP_FAILED, "
			    "so not running rebalance.\n"));
		}

		break;

	case rgm::RG_PENDING_ONLINE:
		//
		// Moving this RG online may allow other RGs that have an
		// affinity or RG dependency for it to come online. So call
		// rebalance on each RG that has a strong positive affinity
		// or RG dependency for this one.
		//
		if (old_rg_state == rgm::RG_OFFLINE)
			// Set the cleanup_stale_affs to true.
			run_rebalance_on_affinitents(rgp, B_TRUE, B_TRUE);
		else
			// Set the cleanup_stale_affs to false.
			run_rebalance_on_affinitents(rgp, B_TRUE, B_FALSE);
		run_rebalance_on_dependents(rgp);
		break;

	// The remaining cases don't require further processing:
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_ON_PENDING_METHODS:
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_BOOT:
	case rgm::RG_PENDING_OFF_START_FAILED:
		//
		// We don't clear start-failure history for an RG in
		// PENDING_ONLINE_BLOCKED.  Wait for it to do online.
		//
	case rgm::RG_PENDING_ONLINE_BLOCKED:
		break;
	}
}

//
// run_rebalance
// -------------
// Checks the rgl_no_rebalance flag on rgp.  If the flag is set, sets
// the rgl_delayed_rebalance flag. Otherwise, calls rebalance directly.
//
static void
run_rebalance(rglist_p_t rgp)
{
	LogicalNodeset emptyNodeset;

	if (rgp->rgl_no_rebalance) {
		ucmm_print("RGM", NOGET("In process_needy_rg: need to "
		    "call rebalance on <%s>, but no_rebalance is "
		    "set on the RG."), rgp->rgl_ccr->rg_name);
		rgp->rgl_delayed_rebalance = B_TRUE;
	} else {
		ucmm_print("RGM", NOGET(
			"in process_needy_rg call rebal on <%s>\n"),
		    rgp->rgl_ccr->rg_name);
		//
		// Instruct rebalance NOT to ignore joiners by passing
		// it an empty nodeset.  The third arg is FALSE because
		// in the above switch stmt, we skip RGs that are in
		// ON_PENDING_METHODS or OFF_PENDING_METHODS state,
		// therefore this argument is not applicable.  The
		// third arg is set TRUE only when a new president
		// calls rebalance() from process_rg(), during the
		// reconfiguration step.
		//
		rebalance(rgp, emptyNodeset, B_FALSE, B_TRUE);
	}
}

//
// run_rebalance_on_affinitents
// ----------------------------
// Checks eligibility for, and possibly runs rebalance on, all
// RGs with strong (positive or negative) affinities for rgp.
//
// An RG is eligible to run rebalance if it is managed, the
// rgl_no_rebalance flag is not set on the affinitee (in which case we set
// the rgl_delayed_rebalance flag on the affinitent instead), the
// rgl_no_triggered_rebalance is not set on the affinitent,
// and rgl_ok_to_start flag is set.
//
// Note that we do not need to bust switches on an RG
// before calling rebalance on it.  If the RG is busy,
// rebalance will be a no-op, and the switching code will
// make sure the RG gets online on the proper nodes.
//
// However, if the RG is updating, then we need to bust
// the update so that rebalance will be run.  The thread
// doing the update will run rebalance after it finds that
// the update was busted.
//
// The stale inter cluster affinitents if any should be cleaned
// up only when the rg is moving from RG_OFFLINE state to RG_ONLINE
// state. cleanup_stale_affs will be set to B_TRUE if the rg is
// is moving from RG_OFFLINE state to RG_ONLINE state.
//
void
run_rebalance_on_affinitents(rglist_p_t rgp, boolean_t positive_affs,
    boolean_t cleanup_stale_affs)
{
	namelist_t *affs = NULL;
	namelist_t *nlist = NULL;
	rglist_p_t rg_aff = NULL;
	const char *aff_type = NULL, *state_string = NULL;
	LogicalNodeset emptyNodeset;
	boolean_t cleanup_flag = B_FALSE;

	ucmm_print("RGM", NOGET(
	    "run_rebalance_on_affinitients on RG <%s>\n"),
	    rgp->rgl_ccr->rg_name);

	//
	// Figure out whether we're checking strong positive or strong
	// negative affinities, and set the namelist and debugging strings
	// appropriately.
	//
	if (positive_affs) {
		nlist = rgp->rgl_affinitents.ap_strong_pos;
		aff_type = "positive";
		state_string = "RG_PENDING_ONLINE";
	} else {
		nlist = rgp->rgl_affinitents.ap_strong_neg;
		aff_type = "negative";
		state_string = "RG_OFFLINE";
	}

	for (affs = nlist; affs != NULL && !cleanup_flag;
	    affs = affs->nl_next) {
#if (SOL_VERSION >= __s10)
		scha_err_t err = SCHA_ERR_NOERR;
		if (is_enhanced_naming_format_used(affs->nl_name)) {
			if (!allow_inter_cluster()) {
				ucmm_print("RGM", NOGET(
				    "run_rebalance_on_affinitents "
				    "Running at too low a version:RG <%s> "
				    "has affinity for RG <%s> "
				    "which is in not in local cluster\n"),
				    rgp->rgl_ccr->rg_name, affs->nl_name);
				continue;
			}
			err = handle_inter_cluster_rg_affinities(
			    rgp->rgl_ccr->rg_name, affs->nl_name,
			    rgm::REBALANCE_TARGET_AFFINITY_RG, NULL, NULL,
			    positive_affs, &cleanup_flag, cleanup_stale_affs);
			continue;
		}
#endif
		rg_aff = rgname_to_rg(affs->nl_name);
		CL_PANIC(rg_aff != NULL);
		//
		// Skip the RG if it's not managed.
		//
		if (rg_aff->rgl_ccr->rg_unmanaged) {
			continue;
		}
		//
		// Note that we check the flag on rgp, not on rg_aff.
		// That is intentional, because unpack_rg_seq sets
		// the flags on the RGs one at a time, so rg_aff
		// might not have its flag set yet.
		//
		if (rgp->rgl_no_rebalance) {
			//
			// we're not supposed to run rebalance yet.
			// Set a flag on the RG so the caller knows
			// to run it later.
			//
			rg_aff->rgl_delayed_rebalance = B_TRUE;
			ucmm_print("RGM", NOGET("process_needy_rg: "
			    "Not running rebalance on RG <%s>, even "
			    "though it has a strong %s affinity "
			    "for RG <%s> which is %s, because the latter has "
			    "the rgl_no_rebalance flag set.\n"),
			    rg_aff->rgl_ccr->rg_name, aff_type,
			    rgp->rgl_ccr->rg_name, state_string);
		} else if (rg_aff->rgl_no_triggered_rebalance) {
			//
			// We check the no_triggered_rebalance flag on
			// the rg_aff, not rgp.  This flag is only set when
			// do_rg_switch is switching an RG. In that case, we
			// don't want to rebalance the RG due to strong
			// affinities.  However, we do want to rebalance it
			// if it goes start_failed (or something like that),
			// so we can't just use the no_rebalance flag.
			//
			ucmm_print("RGM", NOGET("process_needy_rg: "
			    "Not running rebalance on RG <%s>, even "
			    "though it has a strong %s affinity "
			    "for RG <%s> which is %s, because the former has "
			    "the rgl_no_triggered_rebalance flag set.\n"),
			    rg_aff->rgl_ccr->rg_name, aff_type,
			    rgp->rgl_ccr->rg_name, state_string);
		} else if (!rg_aff->rgl_ok_to_start) {
			ucmm_print("RGM", NOGET("process_needy_rg: "
			    "Not running rebalance on RG <%s>, even "
			    "though it has a strong %s affinity for RG <%s> "
			    "which is %s, because the former is not "
			    "ok_to_start.\n"), rg_aff->rgl_ccr->rg_name,
			    aff_type, rgp->rgl_ccr->rg_name, state_string);
		} else if (rg_aff->rgl_switching == SW_UPDATING) {
				ucmm_print("RGM", NOGET(
				    "process_needy_rg: busting update "
				    "on RG <%s> because it has a strong %s "
				    "affinity for RG <%s> which is %s.\n"),
				    rg_aff->rgl_ccr->rg_name, aff_type,
				    rgp->rgl_ccr->rg_name, state_string);
				rg_aff->rgl_switching = SW_UPDATE_BUSTED;
				//
				// Don't need to do a cond_broadcast,
				// because we didn't change the
				// condition on which a thread waiting
				// for an update to finish is waiting.
				//
		} else {
			ucmm_print("RGM", NOGET(
			    "process_needy_rg: calling "
			    "rebalance on RG <%s> because it has a "
			    "strong %s affinity for RG <%s> which is "
			    "%s.\n"), rg_aff->rgl_ccr->rg_name,
			    aff_type, rgp->rgl_ccr->rg_name, state_string);
			//
			// If we're processing negative affinities, pass the
			// planned_on nodeset for rebalance to ignore.
			//
			// For positive affinities, we don't want to ignore
			// the planned_on_nodes.
			//
			if (!positive_affs) {
				rebalance(rg_aff, rgp->rgl_planned_on_nodes,
				    B_FALSE, B_TRUE);
			} else {
				rebalance(rg_aff, emptyNodeset, B_FALSE,
				    B_TRUE);
			}
		}
	}
}

//
// run_rebalance_on_dependents
// ----------------------------
// Checks eligibility for, and possibly runs rebalance on, all
// RGs with RG dependencies for rgp.
//
// An RG is eligible to run rebalance if it is managed, the
// rgl_no_rebalance flag is not set on the dependee (in which case we set
// the rgl_delayed_rebalance flag on the dependent instead), the
// rgl_no_triggered_rebalance is not set on the dependent,
// and rgl_ok_to_start flag is set.
//
// Note that we do not need to bust switches on an RG
// before calling rebalance on it.  If the RG is busy,
// rebalance will be a no-op, and the switching code will
// make sure the RG gets online on the proper nodes.
//
// However, if the RG is updating, then we need to bust
// the update so that rebalance will be run.  The thread
// doing the update will run rebalance after it finds that
// the update was busted.
//
void
run_rebalance_on_dependents(const rglist_p_t rgp)
{
	rglist_p_t rg_dep = NULL;
	LogicalNodeset emptyNodeset;

	ucmm_print("RGM", NOGET(
	    "run_rebalance_on_dependents on RG <%s>\n"),
	    rgp->rgl_ccr->rg_name);

	// Iterate through all registered RGs
	for (rg_dep = Rgm_state->rgm_rglist; rg_dep != NULL;
	    rg_dep = rg_dep->rgl_next) {

		//
		// Skip the RG if it's not managed.
		//
		if (rg_dep->rgl_ccr->rg_unmanaged) {
			continue;
		}

		// Skip the RG if it's not dependent on rgp
		scha_errmsg_t scha_err = {SCHA_ERR_NOERR, NULL};
		boolean_t is_dep = is_rg1_dep_on_rg2(&scha_err,
		    rg_dep->rgl_ccr, rgp->rgl_ccr);

		if (scha_err.err_code != SCHA_ERR_NOERR) {
			// error is NOMEM or internal -- fatal
			rgm_fatal_reboot("run_rebalance_on_dependents",
			    B_TRUE, B_FALSE, NULL);
		}
		if (!is_dep) {
			continue;
		}

		//
		// Note that we check the flag on rgp, not on rg_dep.
		// That is intentional, because unpack_rg_seq sets
		// the flags on the RGs one at a time, so rg_dep
		// might not have its flag set yet.
		//
		if (rgp->rgl_no_rebalance) {
			//
			// we're not supposed to run rebalance yet.
			// Set a flag on the RG so the caller knows
			// to run it later.
			//
			rg_dep->rgl_delayed_rebalance = B_TRUE;
			ucmm_print("RGM", NOGET("process_needy_rg: "
			    "Not running rebalance on RG <%s>, even "
			    "though it has an RG dependency "
			    "for RG <%s> which is PENDING_ONLINE, "
			    "because the latter has "
			    "the rgl_no_rebalance flag set.\n"),
			    rg_dep->rgl_ccr->rg_name,
			    rgp->rgl_ccr->rg_name);
		} else if (rg_dep->rgl_no_triggered_rebalance) {
			//
			// We check the no_triggered_rebalance flag on
			// the rg_dep, not rgp.  This flag is only set when
			// do_rg_switch is switching an RG. In that case, we
			// don't want to rebalance the RG due to dependencies.
			// However, we do want to rebalance it
			// if it goes start_failed (or something like that),
			// so we can't just use the no_rebalance flag.
			//
			ucmm_print("RGM", NOGET("process_needy_rg: "
			    "Not running rebalance on RG <%s>, even "
			    "though it has an RG dependency "
			    "for RG <%s> which is PENDING_ONLINE, "
			    "because the former has "
			    "the rgl_no_triggered_rebalance flag set.\n"),
			    rg_dep->rgl_ccr->rg_name,
			    rgp->rgl_ccr->rg_name);
		} else if (!rg_dep->rgl_ok_to_start) {
			ucmm_print("RGM", NOGET("process_needy_rg: "
			    "Not running rebalance on RG <%s>, even "
			    "though it has an RG dependency for RG <%s> which "
			    "is PENDING_ONLINE, because the former is not "
			    "ok_to_start.\n"), rg_dep->rgl_ccr->rg_name,
			    rgp->rgl_ccr->rg_name);
		} else if (rg_dep->rgl_switching == SW_UPDATING) {
				ucmm_print("RGM", NOGET(
				    "process_needy_rg: busting update "
				    "on RG <%s> because it has an RG "
				    "dependency for RG <%s> which is "
				    "PENDING_ONLINE\n"),
				    rg_dep->rgl_ccr->rg_name,
				    rgp->rgl_ccr->rg_name);
				rg_dep->rgl_switching = SW_UPDATE_BUSTED;
				//
				// Don't need to do a cond_broadcast,
				// because we didn't change the
				// condition on which a thread waiting
				// for an update to finish is waiting.
				//
		} else {
			ucmm_print("RGM", NOGET(
			    "process_needy_rg: calling "
			    "rebalance on RG <%s> because it has an "
			    "RG dependency for RG <%s> which is "
			    "PENDING_ONLINE.\n"), rg_dep->rgl_ccr->rg_name,
			    rgp->rgl_ccr->rg_name);
			rebalance(rg_dep, emptyNodeset, B_FALSE,
			    B_TRUE);
		}
	}
}


//
// reset_rg_state
// Called on the president, while holding the RGM state mutex.
// Set the RG state for lni to RG_OFFLINE on the slave by
// using rgm_change_mastery().  If the rgm_change_mastery() call fails
// (presumably due to node death), log a debug message but return normally.
//
// Note that we call rgm_change_mastery even if the nodeid is ourself (the
// president).  That is so that the state will get reset even on the president.
//
static void
reset_rg_state(rgm::lni_t lni, const rglist_p_t rgp)
{
	sc_syslog_msg_handle_t handle;

	ucmm_print("RGM",
	    NOGET("resetting state of RG <%s> to OFFLINE on node <%d>\n"),
	    rgp->rgl_ccr->rg_name, lni);

	// We allocate nl_this_rg on the stack because
	// rgm_change_mastery() does not modify or create pointers to
	// its arguments.
	namelist_t	nl_this_rg = {NULL, NULL};

	// Set namelist nl_this_rg to contain just this RG name.
	// XXX bug 4225833:string handling investigation
	// XXX probably don't have to use strdup because rgm_change_mastery()
	// XXX does not modify or create pointers to its arguments.
	nl_this_rg.nl_name = strdup(rgp->rgl_ccr->rg_name);

	// We're not currently differentiating among the various types
	// of rgm_change_mastery failure.
	if (rgm_change_mastery(lni, NULL, &nl_this_rg) != RGMIDL_OK) {
		//
		// rgm_change_mastery call failed.
		// Assume failure is caused by death of the node;
		// just log it, since a cluster reconfiguration will
		// soon follow.
		//
		LogicalNode *lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to reset the state of the
		// specified resource group to offline on the specified node
		// or zone, presumably because the node or zone died.
		// @user_action
		// Examine syslog output on the specified node to determine
		// the cause of node death. The syslog output might indicate
		// further remedial actions.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("reset_rg_state: unable to change state of resource"
		    " group <%s> on node <%s>; assuming that node died"),
		    nl_this_rg.nl_name, lnNode->getFullName());
		sc_syslog_msg_done(&handle);
	}

	free(nl_this_rg.nl_name);
}


//
// process_rgs_on_death
//
// Run by old and new Presidents in old and new clusters.
// Goes through the whole list of RGs and determines
// which RGs need to be rebalanced due to node or zone death.
// Busts switches/updates and resets R/RG state as needed.
// Sends out orders to nodes (including itself) via the ORB.
// Caller has lock on state structure.
//
// If zone_death_only is true, it indicates that we are processing
// the death of a zone on a node which remains alive.  In this case,
// process_rg() busts switches and updates affected by the zone death,
// and does set the delayed_rebalance flag, but does not reset RG state.
// RG state resetting for that case is handled by process_zone_death() on the
// slave side.
//
// We rebalance on zone death to handle the case where the RG has
// already gone offline before the president has been notified of the
// zone death.  In the case of non-well-behaved resource types
// (with Global_zone=TRUE), the RG might be PENDING_OFFLINE for awhile
// and the rebalance will be a no-op.  In that case, when the RG finally
// goes OFFLINE, the president will rebalance it in process_needy_rg().
//
void
process_rgs_on_death(boolean_t i_am_new_president, LogicalNodeset &join_ns,
    boolean_t zone_death_only)
{
	rglist_p_t rglp;
	rgm::lni_t n;
	LogicalNodeset *rg_nodeset = NULL;

	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		ucmm_print("RGM", NOGET("computing work for <%s>\n"),
		    rglp->rgl_ccr->rg_name);

		// Skip over RGs in unmanaged state

		if (rglp->rgl_ccr->rg_unmanaged) {
			ucmm_print("RGM", NOGET("Skipping UNMANAGED RG <%s>\n"),
			    rglp->rgl_ccr->rg_name);
			continue;
		}

		process_rg(rglp, i_am_new_president, join_ns, zone_death_only);
	}

	//
	// process_rg doesn't actually call rebalance. It just
	// sets the rgl_delayed_rebalance flag on the RG.
	// Call delayed_rebalance, telling it to ignore joiners
	// because we need to finish processing node death first.
	//
	delayed_rebalance(join_ns, i_am_new_president);

	//
	// Broadcast the condition variable in case a node died
	// since the last reconfiguration.
	// Unconditionally do the broadcast;
	// don't bother to check for node death.
	// Extra calls to cond_broadcast are harmless.
	//
	// We call this because the calls to process_rg() may have busted
	// in-progress switches on RGs.  We have to broadcast the
	// condition variable so that the threads running busted switches
	// will wake up and recognize that they are busted.
	//
	cond_slavewait.broadcast();
}

//
// in_current_physical_membership
//
// return TRUE if the specified node is in the current physical membership
//
boolean_t
in_current_physical_membership(sol::nodeid_t node)
{
	if (Rgm_state->current_membership.containLni(node))
		return (B_TRUE);

	return (B_FALSE);
}


//
// in_current_logical_membership
//
// return TRUE if the specified node is in the current logical membership
// The caller must hold the rgm state lock.
//
boolean_t
in_current_logical_membership(rgm::lni_t node)
{
	LogicalNode *lnNode = lnManager->findObject(node);
#if (SOL_VERSION >= __s10)
	// If we are the zone cluster rgmd, then we have a different check
	if (strcmp(ZONE, GLOBAL_ZONENAME) != 0) {
		if ((lnNode != NULL) && (lnNode->getStatus() == rgm::LN_DOWN)) {
			return (B_FALSE);
		}
	}
#endif
	// lnNode could be NULL if we don't know about the zone yet
	return ((lnNode != NULL && lnNode->isInCluster()) ? B_TRUE : B_FALSE);
}


//
// is_global_zone
//
// return TRUE if the specified lni designates a physical node
// (i.e., a global zone) or a ZC node.  This function does *not* assume that
// LNIs for physical nodes fall within any particular range.
//
// The caller must hold the rgm state lock.
//
boolean_t
is_global_zone(rgm::lni_t lni)
{
	LogicalNode *lnNode = lnManager->findObject(lni);
	// lnNode could be NULL if we don't know about the zone yet
	return ((lnNode != NULL && lnNode->isZcOrGlobalZone()) ?
	    B_TRUE : B_FALSE);
}


#ifdef DEAD_CODE
//
// This function is no longer used and is awaiting code cleanup.
// See CR 6419313.
//

//
// fetch_slave_state
//
// Fetch the state for all Rs and RGs from slave 'nodeid', and create a new
// entry on the state task queue for later processing.
//
// The caller of this function should call flush_state_change_queue to
// process this new state.
//
// Caller must pass a physical nodeid only -- not a non-global zone lni.
//
idlretval_t
fetch_slave_state(sol::nodeid_t nodeid)
{
	rgm::node_state *state = NULL;
	idlretval_t retval;

	// Make sure arg is a physical nodeid
	CL_PANIC(is_global_zone(nodeid));

	if (nodeid == orb_conf::local_nodeid()) {
		//
		// No need to fetch state from myself; I already have it.
		//
		return (RGMIDL_OK);
	}

	if (!in_current_physical_membership(nodeid))
		// no need to contact nodes not currently in the cluster
		return (RGMIDL_OK);

	//
	// When this function returns, 'state' will point to the
	// slave node's entire state, contained within a sequence.
	//
	if ((retval = rgm_comm_getstate(nodeid, state)) == RGMIDL_OK) {
		//
		// Create a NOTIFY_WAKE task, and add it to the queue.
		//
		notify_state_change_task *notify_task = new
		    notify_state_change_task(nodeid, rgm::NOTIFY_WAKE, NULL,
		    rgm::R_OFFLINE, rgm::RG_OFFLINE, state);

		ucmm_print("RGM", NOGET("in fetch_slave_state, added state "
		    "for nodeid <%d> to queue.\n"), nodeid);

		notify_state_change_threadpool::the().defer_processing(
			notify_task);
		// notify_task will be deleted by the thread that processes it.
	}

	// bubble up error to caller who cares
	return (retval);
}
#endif	// DEAD_CODE


//
// flush_state_change_queue
// ------------------------
// Caller should not hold the lock.  Caller must release the lock before
// calling this function, otherwise this function will deadlock.
//
// All this function does is call the quiesce method on the state change
// threadpool.  If use_qempty is false (the default), the QFENCE option of
// quiesce() is used. This will process everything currently in the queue,
// will allow new tasks to be queued, but will not process those new tasks.
//
// If use_qempty is true, the QEMPTY option of quiesce() is used.  This will
// continue processing state changes until the queue is empty.  This option is
// currently used only during r/rg updates, after validate methods have
// executed and before the update intention is processed.  It is needed
// to make sure that the president is aware of all triggered resource
// restarts on slaves; Update methods cannot be executed in an rg in
// ON_PENDING_R_RESTART state.
//
// xxxx Consider using QEMPTY for all flush_state_change_queue() calls.
//
void
flush_state_change_queue(boolean_t use_qempty)
{
	ucmm_print("flush_state_change_queue",
	    NOGET("Begin, use_qempty = %d"), use_qempty);
	notify_state_change_threadpool::the().quiesce(
	    use_qempty ? threadpool::QEMPTY : threadpool::QFENCE);
	ucmm_print("flush_state_change_queue", NOGET("End"));
}



//
// check_run_failback
// ------------------
// Checks if failback is required for the rg, and, if so, runs it.
//
static void
check_run_failback(rglist_p_t rgp, rgm::lni_t lni)
{
	LogicalNodeset emptyNodeset;
	boolean_t need_bootdelay;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	//
	// Check if failback is required. We don't want to
	// waste our time if the RG is already mastered by
	// enough primaries, and the Failback property is
	// not set on this RG.
	//
	// We ignore the return value of need_bootdelay here; failback()
	// will call is_failback_required() again and if necessary, will
	// apply the bootdelay.
	//
	if (is_failback_required(rgp, &emptyNodeset, NULL, need_bootdelay,
	    B_FALSE, &res)) {
		ucmm_print("failback",
		    NOGET("process_needy_rg: lni <%d> "
		    "joined, and failback is required for "
		    "rg <%s>"), lni, rgp->rgl_ccr->rg_name);

		//
		// Add this lni to the set of joining nodes
		// for this rg.  We store the set of joining
		// nodes on a per-RG basis so that failback
		// can process them (failback runs on a per-RG
		// basis).
		//
		// We don't need to add the joining node to the
		// nodeset if failback isn't required (meaning
		// the failback flag is false, and the RG is
		// mastered by enough nodes).  Even if the
		// failback thread is still running, we don't
		// want to add this joining node, because the
		// failback thread won't be changing
		// anything in this case.
		//
		rgp->rgl_joining_nodes.addLni(lni);

		//
		// We have only one or two failback threads running per
		// RG at a time.  While do_rg_switch() is running in the
		// preceding failback invocation, we may start another
		// failback thread which waits for the preceding one to
		// finish.  But this overlap will occur for only a limited
		// period of time, and there will be no more than two
		// failback threads running at one time.
		//
		// So, first check whether a failback thread is already
		// running in the pre-do_rg_switch phase; and if not, call
		// failback_wrapper, which will asynchronously
		// start up a failback thread from the threadpool.
		//
		// If rgl_failback_running is true, it indicates that a
		// failback thread is already running and has not yet
		// executed do_rg_switch() [i.e., it is still in an
		// initial cond_wait loop].  The running failback thread
		// will notice the new joining node that we added to the
		// nodeset, so we don't have to do anything more.
		//
		// Note that we pass the lni to failback_wrapper,
		// even though failback doesn't technically
		// need it anymore.  It is useful for debugging.
		//
		if (!rgp->rgl_failback_running) {
			rgp->rgl_failback_running = B_TRUE;
			failback_wrapper(lni, rgp);
		} else {
			ucmm_print("RGM",
			    NOGET("process_needy_rg: not "
			    "calling failback wrapper for "
			    "node <%d>, rg <%s>, because "
			    "failback is running."),
			    lni, rgp->rgl_ccr->rg_name);
		}
	}
}


//
// Wrapper for queueing failback work. Should be called when holding
// the rgm state lock so that the RG pointer is valid throughout the
// function.
//
void
failback_wrapper(rgm::lni_t lni, rglist_p_t rgp)
{
	failback_task *fb_task = new failback_task(lni, rgp);

	ucmm_print("RGM", NOGET("failback_wrapper(): lni = <%d>, rg = <%s>"),
	    lni, rgp->rgl_ccr->rg_name);

	failback_threadpool::the().defer_processing(fb_task);

	// fb_task will be deleted by the thread that processes it.
}

//
// failback()
//
// For any single cluster membership change, we start only one failback
// thread running for each RG (the old implementation had one thread per
// each RG per each node that joined).
// If two or more membership changes occur close together in time, then we
// might start a second failback thread for an RG while the previous failback
// thread is still executing do_rg_switch().  The second invocation will
// idle in cond_wait() in the "while (need_wait)" loop until the preceding
// invocation has finished switching the RG.
// The use of the rgl_failback_running flag assures that there will be
// no more than two failback threads running for a given RG.
//
// The failback thread uses the rgl_joining_nodes field in the rglist_t
// structure to switch the RG to more preferred nodes (if the failback flag
// is set for the RG), or to bring the RG online on desired_primaries nodes
// (if it is not online on enough nodes) or both.
//
// Notice that lni_in is not used.  It is a parameter for historical reasons.
//
void
failback(rgm::lni_t lni_in, const char *rgname)
{
	rglist_p_t	rgp = NULL;
	rglist_p_t	rglistp[2];
	boolean_t	need_wait = B_TRUE;
	boolean_t	need_bootdelay = B_TRUE;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	LogicalNodeset	*ns = NULL;
	LogicalNodeset	fromNodeset;
	LogicalNode	*lnNode;
	LogicalNodeset	emptyNodeset;
	char		*ucmm_buffer = NULL;
	rgm::lni_t	lni;
	nodeidlist_t	*nl;
	os::systime	to;

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");

	ucmm_print("failback", NOGET("<%d> ==== rg = <%s>, lni = <%d>"),
	    os::thread::self(), rgname, lni_in);

	rgm_lock_state();

	//
	// If a "zone bootdelay" is needed, delay for the required interval
	// before checking the RG state.
	//
	// If in this loop we find that failback is not required, exit early.
	//

	// Set the bootdelay timeout
	to.setreltime(ZONE_BOOTDELAY * MICROSEC);

	while (need_bootdelay) {
		//
		// Because of the cond_wait, the state_lock could be
		// taken by other jobs (e.g. update, switching).
		// So each time we go through the check we have to make
		// sure the RG hasn't been removed from internal struct,
		// which could be possible in this scenario.
		//
		rgp = rgname_to_rg(rgname);
		if (rgp == NULL) {
			//
			// RG has just been removed. We will bail out.
			// Can't reset the failback_running flag
			// or the joining_nodes nodeset because we don't
			// have an RG pointer anymore.
			//
			goto finish; //lint !e801
		}
		//
		// Check if zone bootdelay is (still) required.
		// set the switch_back option to B_FALSE.
		//
		boolean_t need_failback = is_failback_required(rgp,
		    &rgp->rgl_joining_nodes, &ns, need_bootdelay, B_FALSE,
		    &res);
		//
		// At this point, we're not yet ready to use ns.
		// It will be recomputed below.
		//
		delete ns;
		ns = NULL;
		//
		// If need_bootdelay is true, it means that there
		// is a more-preferred non-global zone which might
		// be joining the cluster membership, so we call
		// timedwait() to implement the bootdelay.
		//
		if (need_bootdelay) {
			os::condvar_t::wait_result err;
			err = cond_zone_bootdelay.timedwait(
			    &Rgm_state->rgm_mutex, &to);
			if (err == os::condvar_t::TIMEDOUT) {
				// zone bootdelay has timed out
				need_bootdelay = B_FALSE;
			}
		} else if (!need_failback) {
			//
			// Zone bootdelay is not required, and no need to
			// do failback.  We can exit early.
			//
			ucmm_print("failback",
			    NOGET("<%d> ==== rg = <%s> -- "
			    "failback no longer required"), thr_self(), rgname);
			//
			// Mark the rgp flags to show that we are no
			// longer doing failback.  Also reset the
			// joining nodes, as we don't need them for
			// this RG.
			//
			rgp->rgl_joining_nodes.reset();
			rgp->rgl_failback_running = B_FALSE;
			goto finish; //lint !e801
		}
	}


	//
	// The next while-loop calls cond_wait() until the RG falls into
	// a quiescent state.
	//
	// If in this loop we find that failback is not required, exit early.
	//
	while (need_wait) {
		fromNodeset.reset();
		need_wait = B_FALSE;
		//
		// Because of the cond_wait, the state_lock could be
		// taken by other jobs (e.g. update, switching).
		// So each time we go through the check we have to make
		// sure the RG hasn't been removed from internal struct,
		// which could be possible in this scenario.
		//
		rgp = rgname_to_rg(rgname);
		if (rgp == NULL) {
			//
			// RG has just been removed. We will bail out.
			// Can't reset the failback_running flag
			// or the joining_nodes nodeset because we don't
			// have an RG pointer anymore.
			//
			goto finish; //lint !e801
		}

		//
		// If the other failback thread is running on this rg, request
		// it to return early by setting rgl_failback_interrupt to 1.
		// The other thread will set rgl_failback_interrupt to 2
		// and exit early without an error.
		//
		if (rgp->rgl_switching == SW_IN_FAILBACK &&
		    rgp->rgl_failback_interrupt == 0) {
			rgp->rgl_failback_interrupt = 1;
			ucmm_print("failback", NOGET(
			    "<%d> ==== setting rgl_failback_interrupt "
			    "to 1"), os::thread::self());
		}

		if (rgp->rgl_switching != SW_NONE &&
		    (rgp->rgl_switching != SW_IN_FAILBACK ||
		    rgp->rgl_failback_interrupt != 2)) {
			//
			// We have to wait for a preceding invocation of
			// switch/enable/disable/update on the same RG to
			// complete.
			//
			ucmm_print("failback", NOGET(
			    "<%d> ==== rgl_switching = %d, rgl_fb_int = %d,"
			    " need to wait"), os::thread::self(),
			    rgp->rgl_switching, rgp->rgl_failback_interrupt);
			need_wait = B_TRUE;

		//
		// At this point, either rgl_switching is SW_NONE; or
		// rgl_switching is SW_IN_FAILBACK and rgl_failback_interrupt
		// equals 2.
		// Iterate through RG nodelist.
		//
		} else for (nl = rgp->rgl_ccr->rg_nodelist; nl != NULL;
		    nl = nl->nl_next) {
			lni = nl->nl_lni;
			lnNode = lnManager->findObject(lni);
			CL_PANIC(lnNode != NULL);

			switch (rgp->rgl_xstate[lni].rgx_state) {

			// If the RG is ERROR_STOP_FAILED, exit early.
			case rgm::RG_PENDING_OFF_STOP_FAILED:
			case rgm::RG_ERROR_STOP_FAILED:
				ucmm_print("failback", NOGET(
				    "<%d> ==== Bail out because RG <%s> is "
				    "STOP_FAILED"),
				    os::thread::self(), rgp->rgl_ccr->rg_name);
				//
				// We are in (or entering) STOP_FAILED on a
				// node.  Reset the failback flag and
				// joining nodes nodeset for the RG, and
				// bail out.
				//
				rgp->rgl_joining_nodes.reset();
				rgp->rgl_failback_running = B_FALSE;
				rgp->rgl_failback_interrupt = 0;
				goto finish; //lint !e801

			//
			// The RG cannot be in the following four states,
			// because we already checked that rgl_switching is
			// SW_NONE or SW_IN_FAILBACK.
			//
			case rgm::RG_ON_PENDING_METHODS:
			case rgm::RG_ON_PENDING_DISABLED:
			case rgm::RG_OFF_PENDING_METHODS:
			case rgm::RG_ON_PENDING_MON_DISABLED:
				//
				// SCMSGS
				// @explanation
				// The rgmd has discovered that the indicated
				// resource group's state information appears
				// to be incorrect. This may prevent any
				// administrative actions from being performed
				// on the resource group.
				// @user_action
				// Since this problem might indicate an
				// internal logic error in the rgmd, save a
				// copy of the /var/adm/messages files on all
				// nodes, and the output of clresourcetype
				// show -v, clresourcegroup show -v +, and
				// clresourcegroup status +. Report the
				// problem to your authorized Sun service
				// provider.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "INTERNAL ERROR: resource group "
				    "<%s> state <%s> node <%s> has invalid "
				    "rgl_switching flag <%d>",
				    rgp->rgl_ccr->rg_name,
				    pr_rgstate(rgp->rgl_xstate[lni].rgx_state),
				    lnNode->getFullName(), rgp->rgl_switching);
				rgp->rgl_joining_nodes.reset();
				rgp->rgl_failback_running = B_FALSE;
				rgp->rgl_failback_interrupt = 0;
				goto finish; //lint !e801

			// No need to wait for the following states:
			case rgm::RG_ONLINE:
			case rgm::RG_PENDING_ONLINE_BLOCKED:
			case rgm::RG_ON_PENDING_R_RESTART:
			case rgm::RG_PENDING_OFFLINE:
			case rgm::RG_PENDING_OFF_START_FAILED:
				// Add this lni to from_nodeset
				// needed for event generation.
				fromNodeset.addLni(lni);
				// FALLTHRU
			case rgm::RG_OFFLINE:
			case rgm::RG_OFFLINE_START_FAILED:
			case rgm::RG_OFF_BOOTED:
			case rgm::RG_OFF_PENDING_BOOT:
			case rgm::RG_PENDING_ONLINE:
			case rgm::RG_PENDING_ON_STARTED:
				break;
			}
		}
		if (need_wait) {
			cond_slavewait.wait(&Rgm_state->rgm_mutex);
		}
	}

	rgp->rgl_failback_interrupt = 0;

	//
	// The only way to exit the preceding cond_wait loop is if
	// rgl_switching is SW_NONE or SW_IN_FAILBACK.
	//
	if (rgp->rgl_switching == SW_NONE) {
		rgp->rgl_switching = SW_IN_FAILBACK;
	}
	CL_PANIC(rgp->rgl_switching = SW_IN_FAILBACK);

	//
	// Check whether we still need to do a failback.
	// At this point, we've already applied the bootdelay (if it was
	// needed), so we ignore the returned value of need_bootdelay.
	//
	if (!is_failback_required(rgp, &rgp->rgl_joining_nodes, &ns,
	    need_bootdelay, B_FALSE, &res)) {
		// No need to do failback.
		ucmm_print("failback", NOGET("<%d> ==== rg = <%s> -- "
		    "no longer required"), os::thread::self(), rgname);
		//
		// Mark the rgp flags to show that we are no longer doing
		// failback.  Also reset the joining nodes, as we don't
		// need them for this RG.
		//
		rgp->rgl_switching = SW_NONE;
		rgp->rgl_joining_nodes.reset();
		rgp->rgl_failback_running = B_FALSE;

		goto finish; //lint !e801
	}

	if (ns->display(ucmm_buffer) == 0) {

		ucmm_print("failback", NOGET("<%d> ==== ns = %s\n"),
		    os::thread::self(), ucmm_buffer);
		free(ucmm_buffer);
	}

	// Post an event for this RG
	(void) post_primaries_changing_event(NULL, rgp->rgl_ccr->rg_name,
	    CL_REASON_RG_FAILBACK, NULL, rgp->rgl_ccr->rg_desired_primaries,
	    fromNodeset, *ns);

	//
	// We call do_rg_switch() to bring the RG online on its new nodeset.
	// Notice that the rgl_switching flag will be reset in do_rg_switch()
	// and cond_broadcast() will be called whenever the RG has finished
	// failback, thereby releasing any other threads that are in
	// cond_wait().
	//

	rglistp[0] = rgp;
	rglistp[1] = NULL;

	//
	// At this point, we allow a second failback() thread to run on the
	// same RG.
	//
	rgp->rgl_failback_running = B_FALSE;

	//
	// Call do_rg_switch to execute the failback.
	// When do_rg_switch releases the lock, the RGs will
	// be in non-terminal states somewhere, and there will be a switch in
	// progress.  Failback has done all it can for this nodelist.
	//
	// If do_rg_switch fails with any other exit code, the switch will
	// get busted, and rebalance will take care of it.
	//

	//
	// Set the offlining flag as well as onlining flag to false.
	// We dont need the last parameter failed_op_list as well.
	//
	do_rg_switch(&res, rglistp, *ns, emptyNodeset, B_FALSE, B_FALSE, NULL);

	//
	// If do_rg_switch() returns SCHA_ERR_MEMBERCHG, it means that a
	// new joining node has started its own failback thread, causing
	// do_rg_switch() to return early.  In this case, do_rg_switch
	// leaves rgl_switching set to SW_IN_FAILBACK and sets
	// rgl_failback_interrupt to 2.  We leave rgl_joining_nodes intact,
	// and let the new failback thread execute its own do_rg_switch().
	//
	if (res.err_code == SCHA_ERR_MEMBERCHG) {
		ucmm_print("failback", NOGET("preempted by other fb thread"));
		res.err_code = SCHA_ERR_NOERR;
	} else {
		rgp->rgl_joining_nodes.reset();
	}

	if (res.err_code != SCHA_ERR_NOERR) {
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to failback the specified
		// resource group to a more preferred node. The additional
		// error information in the message explains why.
		// @user_action
		// Examine other syslog messages occurring around the same
		// time on the same node. These messages may indicate further
		// action.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "failback attempt failed on resource group <%s> "
		    "with error <%s>"), rgp->rgl_ccr->rg_name,
		    rgm_error_msg(res.err_code));
	}

	ucmm_print("failback", NOGET("<%d> ==== Finish RG <%s>"),
	    os::thread::self(), rgp->rgl_ccr->rg_name);

finish:
	free(res.err_msg);
	delete ns;
	rgm_unlock_state();
	sc_syslog_msg_done(&handle);
}

//
// is_failback_required() decides whether failback or switchback
// needs to be done based on the switch_back flag.
// It returns TRUE if failback or switchback is needed, otherwise
// return FALSE.
// If it returns TRUE and nspp isn't NULL, *nspp will contain the
// the target nodeset which is used for do_rg_switch().
//
// If nspp is not NULL, *joiners must contain the nodeset of nodes that
// have just joined the cluster, and on which this RG's resources have
// completed their boot methods.
//
// If one of the RG's most-preferred masters
// (on which it would have been brought online) is a non-global zone which is
// down but whose physical node is up, then need_bootdelay is set to
// B_TRUE; otherwise it is set to B_FALSE.
// This allows failback to wait a short while for all candidate
// zones to come up, so that the RG can be started on the most-preferred
// zones -- see CR# 6329429 .
//
// If the caller is checking for zone bootdelay, it must check the returned
// value of need_bootdelay *before* checking the return value of this function,
// as this function might return B_FALSE even while setting need_bootdelay to
// B_TRUE.  In that case, the caller should take the zone bootdelay rather than
// assuming that failback is not required.  However, if the caller is not
// checking for zone_bootdelay (i.e., if the bootdelay has already been
// taken), then the caller should use the value returned by this function
// to decide whether to proceed with the failback.
//
// The res variable gets updated with any error messages when called with the
// switchback option. No error messages are written into res when switch_back
// is false.
//
// This function expects the state lock to be held.
//
boolean_t
is_failback_required(rglist_p_t rgp, LogicalNodeset* joiners,
    LogicalNodeset **nspp, boolean_t &need_bootdelay, boolean_t switch_back,
    scha_errmsg_t *res)
{
	uint_t		num_online = 0;
	LogicalNodeset	on_ns;
	size_t		ncand = 0;		// Number of candidates
	candidate_t	*c_nodes = NULL;
	uint_t		i, j;
	LogicalNodeset	emptyNodeset;
	int		dummy_num_online = 0;
	int		dummy_num_primaries = 0;
	nodeidlist_t	*nl;

	need_bootdelay = B_FALSE;

	// don't failback if rg suspended
	if (rgp->rgl_ccr->rg_suspended) {
		ucmm_print("is_failback_reqd", NOGET("RG <%s> "
		    "is suspended."), rgp->rgl_ccr->rg_name);
		return (B_FALSE);
	}

	// don't failback if RG has ok_to_start flag set to FALSE
	if (!rgp->rgl_ok_to_start) {
		ucmm_print("is_failback_reqd", NOGET(
		    "RG <%s> has ok_to_start=FALSE"),
		    rgp->rgl_ccr->rg_name);
		return (B_FALSE);
	}

	// Iterate through RG nodelist
	for (nl = rgp->rgl_ccr->rg_nodelist; nl != NULL; nl = nl->nl_next) {
		rgm::lni_t lni = nl->nl_lni;
		LogicalNode *lnNode = lnManager->findObject(lni);
		CL_PANIC(lnNode != NULL);

		if (lnNode->isInCluster()) {
			//
			// We count the number of zones on which RG is online
			// or going to be online. We will also record
			// the nodeset on which it is ONLINE.
			//
			switch (rgp->rgl_xstate[lni].rgx_state) {
			case rgm::RG_ONLINE:
			case rgm::RG_PENDING_ONLINE_BLOCKED:
				// PENDING_ONLINE_BLOCKED counts as online
			case rgm::RG_ON_PENDING_R_RESTART:
			case rgm::RG_PENDING_ONLINE:
			case rgm::RG_PENDING_ON_STARTED:
			case rgm::RG_ON_PENDING_DISABLED:
			case rgm::RG_ON_PENDING_MON_DISABLED:
			case rgm::RG_ON_PENDING_METHODS:
				on_ns.addLni(lni);
				num_online++;
				break;
			case rgm::RG_OFFLINE:
			case rgm::RG_PENDING_OFFLINE:
			case rgm::RG_OFF_PENDING_METHODS:
			case rgm::RG_OFF_PENDING_BOOT:
			case rgm::RG_OFF_BOOTED:
			case rgm::RG_ERROR_STOP_FAILED:
			case rgm::RG_PENDING_OFF_STOP_FAILED:
			case rgm::RG_PENDING_OFF_START_FAILED:
			case rgm::RG_OFFLINE_START_FAILED:
				break;
			}
		}
	}

	ucmm_print("is_failback_reqd", NOGET("RG <%s> is online on "
	    "%d zones.\n"), rgp->rgl_ccr->rg_name, num_online);

	//
	// Check if the failback flag is set.  If so, that takes precedence.
	//
	if (rgp->rgl_ccr->rg_failback || switch_back) {
		ucmm_print("is_failback_reqd", NOGET("<%s>: "
		    "rg_failback is set.\n"), rgp->rgl_ccr->rg_name);

		//
		// If nspp is not NULL, we must calculate the new nodeset
		// for the RG.
		//
		if (nspp != NULL) {
			// First we allocate the LogicalNodeset.
			LogicalNodeset *nsp = new LogicalNodeset;

			//
			// Call sort_candidate_nodes, passing a nodeset to
			// check which is the union of the current online nodes
			// and the joining nodes.  sort_candidate_nodes will
			// give us back an ordered list of those nodes in
			// the check nodeset that are acceptable primaries.
			// We will then specify the target nodeset for
			// failback as the first rg_desired_primaries nodes
			// of the candidate nodes.
			//
			// Note that, because the failback flag is set,
			// we are allowed to switch to the rg_desired_primaries
			// most preferred nodes of the joiners and the current
			// masters.
			//
			// We call sort_candidate_nodes with the
			// check_zone_bootdelay flag set to B_TRUE.  As a
			// result, the returned list 'c_nodes' will include
			// down non-global zones whose physical node is
			// up.  These are indicated by the down_ng_zone flag
			// being set true.  If such a zone exists and would
			// have been a candidate if it were up, we set
			// need_bootdelay to true.  We do not add that zone
			// to nsp, since it is not actually a candidate.
			//
			LogicalNodeset union_ns = *joiners | on_ns;
			*res = sort_candidate_nodes(union_ns, rgp,
			    c_nodes, ncand, emptyNodeset, dummy_num_online,
			    emptyNodeset, dummy_num_primaries, B_FALSE,
			    B_FALSE, B_TRUE, switch_back, B_TRUE);
			if (res->err_code != SCHA_ERR_NOERR) {
				if (res->err_code == SCHA_ERR_NOMEM) {
					// reboot this node
					rgm_fatal_reboot("is_failback_req.1",
					    B_TRUE, B_FALSE, NULL);
				}

				ucmm_print("is_failback_reqd", NOGET(
				    "<%s>: returning B_FALSE because "
				    "sort_candidate_nodes returned error "
				    "code %d"), rgp->rgl_ccr->rg_name,
				    res->err_code);
				return (B_FALSE);
			}

			//
			// Add, to our nodeset, enough joining nodes
			// to get up to num_primaries.
			// Assume that current masters will always pass the
			// candidate check.
			//
			for (i = 0, j = 0; i < ncand && (int)j <
			    rgp->rgl_ccr->rg_desired_primaries; i++) {
				// Check for down NG zone for bootdelay
				if (c_nodes[i].down_ng_zone) {
					ucmm_print("is_failback_reqd",
					    NOGET("<%s>: bootdelay for zone "
					    "%d"), rgp->rgl_ccr->rg_name,
					    c_nodes[i].lni);
					need_bootdelay = B_TRUE;
					continue;
				}
				++j;
				nsp->addLni(c_nodes[i].lni);
				ucmm_print("is_failback_reqd", NOGET(
				    "<%s>: adding node <%d> to nodeset.\n"),
				    rgp->rgl_ccr->rg_name, c_nodes[i].lni);
			}

			//
			// free the candidate list
			//
			free_candidate_list(c_nodes, ncand);
			free(c_nodes);

			ucmm_print("is_failback_reqd", NOGET(
			    "<%s>: added %d nodes to nodeset\n"),
			    rgp->rgl_ccr->rg_name, j);

			//
			// Make sure the new nodeset is different from the old.
			// It could be that none of the joining nodes are
			// more preferred than the current nodes, in which
			// case we don't need to do failback.
			//
			if (on_ns.isEqual(*nsp)) {
				return (B_FALSE);
			}
			*nspp = nsp;
		}
		return (B_TRUE);

	//
	// Now check the case where the failback flag is not set, but we
	// don't have the RG mastered by enough primaries.
	//
	} else if (num_online < (uint_t)rgp->rgl_ccr->rg_desired_primaries) {

		ucmm_print("is_failback_reqd", NOGET("<%s>: "
		    "rg_failback is not set.\n"), rgp->rgl_ccr->rg_name);

		//
		// Need to bring up RG on one or more of the newly
		// joined nodes.
		//
		// If nsp is not NULL, we must calculate the new nodeset
		// for the RG.
		//
		if (nspp != NULL) {
			LogicalNodeset *nsp = new LogicalNodeset(on_ns);
			//
			// Call sort_candidate_nodes, passing a nodeset to
			// check which is only the joining nodes that are
			// not already members of *nsp, since the RG is already
			// online or pending online on the members of *nsp.
			// sort_candidate_nodes will give us back an ordered
			// list of those nodes in the check nodeset that are
			// acceptable primaries.
			//
			// We will then specify the target nodeset for
			// failback as the current nodes on which the RG is
			// online or pending online, plus the first
			// (rg_desired_primaries - num_online) nodes of the
			// candidate nodes.
			//
			// We call sort_candidate_nodes with the
			// check_zone_bootdelay flag set to B_TRUE.  As a
			// result, the returned list 'c_nodes' will include
			// down non-global zones whose physical node is
			// up.  These are indicated by the down_ng_zone flag
			// being set true.  If such a zone exists and would
			// have been a candidate if it were up, we set
			// need_bootdelay to true.  We do not add that zone
			// to nsp, since it is not actually a candidate.
			//
			*res = sort_candidate_nodes((*joiners) - (*nsp), rgp,
			    c_nodes, ncand, emptyNodeset, dummy_num_online,
			    emptyNodeset, dummy_num_primaries, B_FALSE,
			    B_FALSE, B_TRUE, B_FALSE, B_TRUE);
			if (res->err_code != SCHA_ERR_NOERR) {
				if (res->err_code == SCHA_ERR_NOMEM) {
					// reboot the node
					rgm_fatal_reboot("is_failback_req.2",
					    B_TRUE, B_FALSE, NULL);
				}
				ucmm_print("is_failback_reqd", NOGET(
				    "<%s>: returning B_FALSE because "
				    "sort_candidate_nodes returned error "
				    "code"), rgp->rgl_ccr->rg_name);
				return (B_FALSE);
			}

			//
			// Add, to our nodeset, enough joining nodes
			// to get up to num_primaries.
			//
			for (i = 0, j = 0; i < ncand && (j + num_online) <
			    (uint_t)rgp->rgl_ccr->rg_desired_primaries; i++) {
				// Check for down NG zone for bootdelay
				if (c_nodes[i].down_ng_zone) {
					ucmm_print("is_failback_reqd",
					    NOGET("<%s>: bootdelay for zone "
					    "%d"), rgp->rgl_ccr->rg_name,
					    c_nodes[i].lni);
					need_bootdelay = B_TRUE;
					continue;
				}
				++j;
				nsp->addLni(c_nodes[i].lni);
				ucmm_print("is_failback_reqd", NOGET(
				    "<%s>: adding node <%d> to nodeset.\n"),
				    rgp->rgl_ccr->rg_name, c_nodes[i].lni);
			}

			//
			// free the candidate list
			//
			free_candidate_list(c_nodes, ncand);
			free(c_nodes);

			ucmm_print("is_failback_reqd", NOGET(
			    "<%s>: added %d nodes to nodeset.  Started with "
			    "%d nodes.\n"),
			    rgp->rgl_ccr->rg_name, j, num_online);

			//
			// Make sure the new nodeset is different from the old.
			// It could be that none of the joining nodes are
			// in the nodelist for this RG, so we won't be able
			// to fullfill the desired primaries.
			//
			if (on_ns.isEqual(*nsp)) {
				return (B_FALSE);
			}
			*nspp = nsp;
		}
		return (B_TRUE);
	}

	//
	// We don't have the failback flag set, and we are mastered by
	// enough primaries.  We don't need to run failback.
	//
	return (B_FALSE);
}

static int
retrieve_lni_mappings(const LogicalNodeset &non_joiners)
{
	LogicalNode		*lnNode = NULL;
	rgm::lni_mappings_seq_t	*lni_mapping;

	rgm::rgm_comm_var	prgm_serv;
	Environment		e;
	idlretval_t		retval = RGMIDL_OK;
	uint_t			i, j;
	sol::nodeid_t mynodeid = orb_conf::local_nodeid();
	bool is_farmnode;

	// Retrieve existing mappings on cluster nodes.
	if (non_joiners.isEmpty()) {
		ucmm_print("RGM",
		    NOGET("No non-joiners: not retrieving mappings\n"));
		return (0);
	}

	ucmm_print("RGM", "retrieve_lni_mappings\n");

	i = 0;
	while ((i = non_joiners.nextPhysNodeSet(i + 1)) != 0) {
		if (mynodeid == i) {
			continue;
		}

		if ((rgmx_hook_call(rgmx_hook_retrieve_lni_mappings,
		    i,
		    &lni_mapping, &retval,
		    &is_farmnode) == RGMX_HOOK_DISABLED) ||
		    (!is_farmnode)) {

			prgm_serv = rgm_comm_getref(i);
			//
			// Ensure that the object reference is not null.
			// If it is null, it means that this node died
			// while we are processing this reconfiguration.
			//
			if (!CORBA::is_nil(prgm_serv)) {
				prgm_serv->idl_retrieve_lni_mappings(
				    lni_mapping, e);
				retval = except_to_errcode(e, i);
			} else {
				//
				// The object reference for this node is
				// null. Set the appropriate error.
				//
				retval = RGMIDL_NODE_DEATH;
			}
		}

		if (retval != RGMIDL_OK) {
			//
			// This means that node i has died.
			// Exit early with an error, so caller (rgm_reconfig)
			// can exit early.
			//
			return (-1);
		}


		for (j = 0; j < lni_mapping->length(); j++) {

			rgm::lni_mappings &currMapping = (*lni_mapping)[j];

			LogicalNode *node = lnManager->
			    createLogicalNodeFromMapping(i,
			    (char *)currMapping.idlstr_zonename,
			    currMapping.lni, currMapping.state);

			// set the incarnation number
			node->setIncarnation(currMapping.incarn);
		}
		delete(lni_mapping);
	}

	lnManager->debug_print();

	return (0);
}

//
// This function should be called only in the new non-joining president case,
// and should not be called until all mappings have been retrieved from slaves.
// It's possible that zones existing on a node not currently in the cluster
// are referenced in a nodelist or rt installed nodes list. In that case, we
// won't yet have mappings, so we must assign a new one.
//
void
fill_in_lnis()
{
	ucmm_print("fill_in_lnis", "entry");

	//
	// Disable reclaims while the nodelists are being processed.
	//
	lnManager->reclaimOff();

	rglist_p_t rgptr = NULL;
	for (rgptr = Rgm_state->rgm_rglist; rgptr != NULL;
	    rgptr = rgptr->rgl_next) {
		nodeidlist_t *nlp = NULL;
		rlist_p_t rp = NULL;
		rgm_resource_t *r_ccr = NULL;
		char *swtch = NULL;

		for (nlp = rgptr->rgl_ccr->rg_nodelist; nlp != NULL;
		    nlp = nlp->nl_next) {
			update_nlp(nlp);
		}

		//
		// New non-joining president needs to be updated with
		// the ENABLED status of the resources on the new updated
		// LNI's.
		//
		for (rp = rgptr->rgl_resources; rp != NULL;
			rp = rp->rl_next) {

			r_ccr = rp->rl_ccrdata;
			//
			// Repopulate onoff/monitored/extension properties
			// from the r_switch_node/rp_value_node maps.
			// This is done to retrieve the switch/value status
			// on freshly filled in LNI's.
			//
			update_onoff_monitored_switch(r_ccr,
			    rgptr->rgl_ccr->rg_nodelist);
			update_pn_default_value(r_ccr->r_ext_properties,
			    rgptr->rgl_ccr->rg_nodelist);

			free_extbacks(rp->rl_ccrdata->rx_back);

			if (rp->rl_ccrdata->rx_back.mdb_onoffswitch)
				free(rp->rl_ccrdata->rx_back.mdb_onoffswitch);
			if (rp->rl_ccrdata->rx_back.mdb_monswitch)
				free(rp->rl_ccrdata->rx_back.mdb_monswitch);
			//
			// Update the mdb backup data.
			//
			if ((update_mdb_extback(r_ccr->rx_back,
			    r_ccr->r_ext_properties)).err_code !=
			    SCHA_ERR_NOERR) {
				ucmm_print("fill_in_lnis()",
				    "MDB ext property backup failed");
			}

			swtch = switchmap_to_string(((rgm_switch_t *)r_ccr
			    ->r_onoff_switch)->r_switch);
			r_ccr->rx_back.mdb_onoffswitch = swtch;

			swtch = switchmap_to_string(((rgm_switch_t *)r_ccr
			    ->r_monitored_switch)->r_switch);
			r_ccr->rx_back.mdb_monswitch = swtch;
		}

	}

	rgm_rt_t *rtptr;
	for (rtptr = Rgm_state->rgm_rtlist; rtptr != NULL;
	    rtptr = rtptr->rt_next) {
		if (rtptr->rt_instl_nodes.is_ALL_value) {
			continue;
		}
		nodeidlist_t *nlp;
		for (nlp = rtptr->rt_instl_nodes.nodeids; nlp != NULL;
		    nlp = nlp->nl_next) {
			update_nlp(nlp);
		}
	}

	lnManager->reclaimOn();
}

// updates the lni, if it contains invalid value. panics if inconsistency found
static void
update_nlp(nodeidlist_t *nlp)
{
	if (nlp->nl_lni == LNI_UNDEF) {
		// Should never happen for a physical node
		CL_PANIC(nlp->nl_zonename != NULL);
		LogicalNode *ln = lnManager->findLogicalNode(
		    nlp->nl_nodeid, nlp->nl_zonename);
		if (ln == NULL) {
			// create a new mapping
			ln = lnManager->createLogicalNode(
			    nlp->nl_nodeid,
			    nlp->nl_zonename,
			    rgm::LN_UNCONFIGURED);
		}
		nlp->nl_lni = ln->getLni();
	}
}
