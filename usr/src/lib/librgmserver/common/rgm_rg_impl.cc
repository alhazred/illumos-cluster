//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_rg_impl.cc	1.153	09/01/28 SMI"

//
// rgm_rg_impl.cc
//
// The functions in this file implement the server (i.e., president) side of
// RG-related functions.
//

#include <orb/fault/fault_injection.h>
#include <rgm/rgm_comm_impl.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rgm/rgm_msg.h>
#include <rgm_global_res_used.h>
#include <rgm_proto.h>
#include <rgm_intention.h>
#include <rgm/rgm_cnfg.h>
#include <rgm_api.h>
#include <scadmin/scconf.h>
#include <rgm/rgm_errmsg.h>
#include <rgm/rgm_util.h>
#include <sys/cl_events.h>
#include <vector>
#include <syslog.h>
#include <sys/vc_int.h>
using namespace std;

#include <rgmx_hook.h>

// SC SLM addon start
#ifndef FSS_MAXSHARES
#define	FSS_MAXSHARES   65535 /* XXX from sys/fss.h */
#endif
// SC SLM addon end

//
// internal function to check a list of properties for any prop. which
// may not be updated in a "system" RG
//
static scha_errmsg_t check_system_rg_updatable_props(rglist_p_t rg_ptr,
    const rgm::idl_nameval_seq_t &props, scha_errmsg_t *warning_res);

static scha_errmsg_t check_ngzone_rg_updatable_props(const rglist_p_t rg_ptr,
    const rgm::idl_rg_update_prop_args_v1 &rgup_args);

//
// internal function for sanity check resource group property
//
static scha_errmsg_t check_rg_updatable_props(
    const rgm::idl_nameval_seq_t &props, rgm::caller_flag_t);

static scha_errmsg_t rg_property_check(rgm_rg_t *rgp,
    rgm_affinities_t *affinities, boolean_t rg_create,
    boolean_t affs_changed, boolean_t des_prims_changed,
    boolean_t nodelist_changed, boolean_t auto_start_changed,
    const rgm::idl_string_seq_t &rg_list);
static void run_init_method(rglist_p_t rgp, bool &need_run_init);
static scha_errmsg_t rg_affinities_check_nodelist(rgm_rg_t *rgp,
    const rgm_affinities_t *affinities);
static scha_errmsg_t check_sp_affinities_nodelist(rgm_rg_t *rgp_affinitent,
    rgm_rg_t *rgp_affinitee);
static scha_errmsg_t check_wp_affinities_nodelist(rgm_rg_t *rgp_affinitent,
    rgm_rg_t *rgp_affinitee);
static scha_errmsg_t rg_affinities_validate(rgm_rg_t *rgp,
    rgm_affinities_t *affinities, boolean_t rg_create,
    boolean_t affs_changed, boolean_t des_prims_changed,
    boolean_t nodelist_changed, boolean_t auto_start_changed,
    const rgm::idl_string_seq_t &rg_list);
static scha_errmsg_t only_prop_updated(const rgm::idl_nameval_seq_t &props,
    boolean_t &only_susp_resume_updated, boolean_t &only_rg_system_updated);
static scha_errmsg_t implicit_dependencies_sanity_check(rglist_p_t rgp);
static bool is_rg_in_update_list(const char *rg_name,
    const rgm::idl_string_seq_t &rg_list);
static uint_t count_intersect_nlist(const rgm_rg_t *rgp, const rgm_rg_t *rgp2,
    boolean_t logical_nodes);
static LogicalNodeset *get_phys_nodeset_from_nodelist(nodeidlist_t *nodelist);

#if SOL_VERSION >= __s10
static scha_errmsg_t
rg_ext_affinities_validate(rgm_rg_t *rgp, rgm_affinities_t *affinities,
    boolean_t rg_create, boolean_t affs_changed, boolean_t des_prims_changed,
    boolean_t nodelist_changed);
#endif
static int
count_intersect_lniseq(rgm::lni_seq_t ls1, rgm::lni_seq_t ls2);
// New functions for supporting rolling upgrade.
static void scrgadm_rg_update_prop_helper(
    const rgm::idl_rg_update_prop_args_v1 &rgup_args,
    rgm::idl_regis_result_t_out rgup_res);
static void rg_update_prop_v0_to_v1(
    rgm::idl_rg_update_prop_args_v1 &rgup_v1_args,
    const rgm::idl_rg_update_prop_args &rgup_args);

// SC SLM addon start
// helper function to check RG_SLM_CPU_SHARES and RG_SLM_PSET_MIN values
static const char *check_slm_cpu(int);
static const char *check_slm_cpu_shares(int);
// helper function to check RG name length for SLM
static const char *check_slm_rgname_length(const rgm_rg_t *);
// helper function to check RG names clash for SLM
// see details in slm_rgm_name_strcmp() comments
static const char *check_slm_rgname_clash(const rgm_rg_t *);
// helper function to check SLM properties
static scha_errmsg_t rg_slm_sanity_checks(const rgm_rg_t *,
    const rgm::idl_string_seq_t &rg_list);
// string compare where '-' and '_' are equivalent
// see details in slm_rgm_name_strcmp() comments
static int slm_rgm_name_strcmp(const char *, const char *);
// SC SLM addon end

// helper function for validating inter cluster rg affinities.
static scha_errmsg_t
rg_ext_affinities_validate_helper(rgm_rg_t *rgp, namelist_t *aff,
    boolean_t rg_create, boolean_t affs_changed, boolean_t des_prims_changed,
    boolean_t nodelist_changed, rgm::rg_aff_flag_t aff_flag,
    boolean_t rg_set_aff);

//
// updatable rg properties
// If the updatable flag is B_TRUE, property value can be updated by
// 'scrgadm -a|-c -y' command
//

static updatable_prop_t rg_updatable_props[] = {
	{SCHA_RG_DESCRIPTION,		B_TRUE},
	{SCHA_PINGPONG_INTERVAL,	B_TRUE},
	{SCHA_NODELIST,			B_TRUE},
	{SCHA_MAXIMUM_PRIMARIES,	B_TRUE},
	{SCHA_DESIRED_PRIMARIES,	B_TRUE},
	{SCHA_FAILBACK,			B_TRUE},
	{SCHA_RESOURCE_LIST,		B_FALSE},
	{SCHA_RG_DEPENDENCIES,		B_TRUE},
	{SCHA_GLOBAL_RESOURCES_USED,	B_TRUE},
	{SCHA_IMPL_NET_DEPEND,		B_TRUE},
	{SCHA_RG_PROJECT_NAME,		B_TRUE},
	// SC SLM addon start
	{SCHA_RG_SLM_TYPE,		B_TRUE},
	{SCHA_RG_SLM_PSET_TYPE,		B_TRUE},
	{SCHA_RG_SLM_CPU_SHARES,	B_TRUE},
	{SCHA_RG_SLM_PSET_MIN,		B_TRUE},
	// SC SLM addon end
	{SCHA_RG_AFFINITIES,		B_TRUE},
	{SCHA_IC_RG_AFFINITY_SOURCES,	B_TRUE},
	{SCHA_RG_AUTO_START,		B_TRUE},
	//
	// The rg_mode property needs to be updated in rg create.
	// rg update is not allowed to change this property.
	// A specific check is done in rg update code.
	//
	{SCHA_RG_MODE,			B_TRUE},
	{SCHA_PATHPREFIX,		B_TRUE},
	{SCHA_RG_STATE,			B_FALSE},
	{SCHA_RG_SYSTEM,		B_TRUE},
	//
	// scrgadm is not allowed to update this property, it can
	// only be updated by scswitch via the -s suspend|resume command
	// code checks are made which will prevent scrgadm to update it
	//
	{SCHA_RG_SUSP_AUTO_RECOVERY,	B_TRUE}
};

#define	RG_UPDATABLE_ENTRIES	\
	(sizeof (rg_updatable_props) / sizeof (updatable_prop_t))
//
// is_rg_in_update_list - does rg name appear in list of operands
//
static bool
is_rg_in_update_list(const char *rg_name, const rgm::idl_string_seq_t &rg_list)
{
	for (uint_t i = 0; i < rg_list.length(); i++) {
		if (strcmp(rg_list[i], rg_name) == 0) {
			return (true);
		}
	}
	return (false);
}


//
// Fetch RG state (on local node or given node) from the president.
// Implements scha_resourcegroup_get RG_STATE and RG_STATE_NODE optags.
//
void
rgm_comm_impl::idl_scha_rg_get_state(
	const rgm::idl_rg_get_state_args &rggs_args,
	rgm::idl_get_result_t_out rggs_res, Environment &)
{
	char *rg_name = NULL, *node_name = NULL;
	rgm::idl_get_result_t	*retval = new rgm::idl_get_result_t();
	scha_rgstate_t		state;
	char			*seq_id = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	rg_name = strdup_nocheck(rggs_args.idlstr_rg_name);
	node_name = strdup_nocheck(rggs_args.idlstr_node_name);
	if (rg_name == NULL || node_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		goto finished; //lint !e801
	}

	if ((res = rgm_comm_getrgstate(rg_name, node_name, &state, &seq_id)).
	    err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM",
		    NOGET("idl_scha_rg_get_state(): failed to get RG state:"
		    " returning\n"));
		goto finished; //lint !e801
	}

	retval->idlstr_seq_id = new_str(seq_id);
	if ((char *)retval->idlstr_seq_id == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		goto finished; //lint !e801
	}
	retval->value_list = rgm::idl_string_seq_t(1);	// One value
	//
	// The sequence frees its elements with delete[], so we must
	// allocate them with new[] (via rgm_itoa_cpp).
	//
	retval->value_list[0] = rgm_itoa_cpp(state);
	retval->value_list.length(1);

	ucmm_print("RGM",
	    NOGET("idl_scha_rg_get_state(): got rg state: returning\n"));

finished:
	free(rg_name);
	free(node_name);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	rggs_res = retval;
}


//
// rg_sanity_checks(): Checks a proposed RG structure for various
// semantics and pre-conditions.
// If 'managed_rg' is B_TRUE, check that all RGs in the rg_dependencies
// list are in managed state; however, RGs in the list 'rglistp' (which
// are also being managed at the same time) are excluded from this check.
// If 'managed_rg' is B_FALSE, skip this check.
// Returns appropiate error code from scha_err.h on error, SCHA_ERR_NOERR
// if everything looks ok.
// As it traverses the internal state structures, it expects the caller
// to hold the rgm state lock.
// Notes: this routine is being called on RG creation(unmanaged), RG
//	update(unmanaged or managed) and RG state change from unmanaged to
//	offline(managed).
// 	This routine doesn't check rg_project_name, since the local host
//	may not be one of nodes in rg_nodelist.  If the given project name
//	is not valid, resource creation/update will catch this problem later.
//

scha_errmsg_t
rg_sanity_checks(rgm_rg_t *prg, rgm_affinities_t *affinities,
    boolean_t managed_rg, rglist_p_t *rglistp,
    const rgm::idl_string_seq_t &rg_list)
{
	nodeidlist_t	*nl;
	rglist_t	*rg;
	rglist_p_t	rg_ptr;
	LogicalNodeset	new_node_list;
	int		numnode = 0;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	namelist_t	*deps;
	namelist_t	*dependee_rgs = NULL;

	// Desired_primaries <= Maximum_primaries.
	if (prg->rg_desired_primaries > prg->rg_max_primaries) {
		ucmm_print("RGM", NOGET("rg_sanity_checks(): "
		    "RG <%s> Desired is greater than Max Primaries\n"),
		    prg->rg_name);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RG_DP_GT_MP, prg->rg_name);
		return (res);
	}

	// Get a list of all the lnis in the new nodelist
	// Assume that lnis have already been assigned to this list
	nl = prg->rg_nodelist;
	while (nl) {
		new_node_list.addLni(nl->nl_lni);
		numnode++;
		nl = nl->nl_next;
	}

	if (numnode == 0) {
		// The nodelist of the RG cannot be empty.
		ucmm_print("RGM", NOGET("rg_sanity_checks(): The nodelist of "
		    "the RG <%s> cannot be empty\n"), prg->rg_name);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_EMPTY_NODELIST, prg->rg_name);
		return (res);
	}

	// Desired_primaries <= Total no. of nodes in the nodelist
	if (prg->rg_desired_primaries > numnode) {
		ucmm_print("RGM", NOGET("rg_sanity_checks(): "
		    "RG <%s> Desired Primaries <%d> Numnodes <%d>\n"),
		    prg->rg_name, prg->rg_desired_primaries, numnode);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RG_DP_GT_NODES, prg->rg_name);
		return (res);
	}

	numnode = 0;
	if ((rg = rgname_to_rg(prg->rg_name)) != NULL) {
		// The RG has to be OFFLINE on the nodes being deleted
		// from nodelist <=> all the nodes that RG is currently
		// online must be in the nodelist

		// iterate through all known logical nodes, checking
		// RG state on all of them.
		for (LogicalNodeManager::iter it = lnManager->begin();
		    it != lnManager->end(); ++it) {
			LogicalNode *lnNode = lnManager->findObject(it->second);
			CL_PANIC(lnNode != NULL);

			rgm::lni_t lni = lnNode->getLni();

			if (rg->rgl_xstate[lni].rgx_state ==
			    rgm::RG_ONLINE ||
			    rg->rgl_xstate[lni].rgx_state ==
			    rgm::RG_PENDING_ONLINE_BLOCKED) {
				// RG is online, so new nodeidlist must
				// contain this ln as well
				if (!new_node_list.containLni(lni)) {
					ucmm_print("RGM",
					    NOGET("rg_sanity_checks(): "
					    "RG <%s> is ONLINE on node %d "
					    "which is not in the nodelist\n"),
					    prg->rg_name, lni);
					res.err_code = SCHA_ERR_RGSTATE;

					rgm_format_errmsg(&res,
					    REM_RG_ON_NODE,
					    prg->rg_name,
					    lnNode->getFullName());
					return (res);
				}
				// numnode is used to count num of current
				// master which RG is ONLINE.
				numnode++;
			}
		}
	}

	// Maximum_primaries cann't be less than the number of
	// current master of RG since there is CL_PANIC assertion
	// in rgm_process_rgs.cc to guarantee it.
	if (numnode > prg->rg_max_primaries) {
		ucmm_print("rg_sanity_check()", NOGET(
		    "The updated Maximum_primaries is less than number of "
		    "the current master of RG <%s>\n"), prg->rg_name);
		res.err_code = SCHA_ERR_PROP;
		rgm_format_errmsg(&res, REM_RG_MP_LT_CURRMASTER, prg->rg_name);
		return (res);
	}

	//
	// In considering RG_dependencies, we add "implicit" dependees for
	// scalable services.  If this RG contains a scalable service
	// resource, then this RG is considered to have an implicit RG
	// dependency (for purposes of this check) upon an RG containing
	// a SharedAddress dependee of the scalable service resource.
	//
	if (rg != NULL) {
		dependee_rgs = add_sa_dependee_rgs_to_list(&res, rg, NULL);
		if (res.err_code != SCHA_ERR_NOERR) {
			goto bailout; //lint !e801
		}
	}

	//
	// Iterate over the list of dependee RGs
	//
	for (deps = dependee_rgs; deps != NULL; deps = deps->nl_next) {
		rglist_t *rg_deps = NULL;

		// skip the checking if the rg_dependencies is a empty list
		if ((deps->nl_name == NULL || deps->nl_name[0] == '\0') &&
		    deps->nl_next == NULL) {
			break;
		}

		// All the RGs in the RG_dependencies list must be valid RGs
		if ((rg_deps = rgname_to_rg(deps->nl_name)) == NULL) {
			ucmm_print("RGM", NOGET("rg_sanity_checks(): "
			    "RG <%s> has dependency on RG <%s>"
			    "which does not exist\n"),
			    prg->rg_name, deps->nl_name);
			res.err_code = SCHA_ERR_RG;
			rgm_format_errmsg(&res, REM_RG_DEP_NON_RG,
			    prg->rg_name, deps->nl_name);
			goto bailout; //lint !e801
		}

		//
		// All the RGs in the RG_dependencies list must be managed
		// only when the rg is managed.  Exclude RGs in rglistp.
		//
		if (managed_rg && rg_deps->rgl_ccr->rg_unmanaged &&
		    !is_rg_in_list(deps->nl_name, rglistp)) {
			ucmm_print("RGM", NOGET("rg_sanity_checks(): "
			    "RG <%s> has dependency on RG <%s>"
			    "which is unmanaged\n"),
			    prg->rg_name, deps->nl_name);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RG_DEP_UNMAN_RG,
			    prg->rg_name, deps->nl_name);
			goto bailout; //lint !e801
		}

		// If this RG has Desired_primaries > 0, then all the RGs in the
		// RG_dependencies list must also have Desired_primaries > 0
		if (prg->rg_desired_primaries > 0 &&
		    rg_deps->rgl_ccr->rg_desired_primaries == 0) {
			res.err_code = SCHA_ERR_DEPEND;
			rgm_format_errmsg(&res, REM_RG_DEP_DESIRED_PRIMARIES,
			    prg->rg_name, rg_deps->rgl_ccr->rg_name);
			goto bailout; //lint !e801
		}

		// If this RG has Auto_start_on_new_cluster = TRUE, then all
		// the RGs in the RG_dependencies list must also have
		// Auto_start_on_new_cluster = TRUE.
		if (prg->rg_auto_start && !rg_deps->rgl_ccr->rg_auto_start) {
			res.err_code = SCHA_ERR_DEPEND;
			rgm_format_errmsg(&res, REM_RG_DEP_AUTO_START,
			    prg->rg_name, rg_deps->rgl_ccr->rg_name);
			goto bailout; //lint !e801
		}
	}

	for (rg_ptr = Rgm_state->rgm_rglist; rg_ptr != NULL;
	    rg_ptr = rg_ptr->rgl_next) {
		boolean_t retval = is_rg1_dep_on_rg2(&res, rg_ptr->rgl_ccr,
		    prg);
		if (res.err_code != SCHA_ERR_NOERR) {
			// NOMEM or internal -- fatal
			goto bailout; //lint !e801
		}
		if (!retval) {
			continue;
		}

		// rg_ptr is dependent on prg

		// If this RG has Desired_primaries = 0, then all dependent
		// RGs must also have Desired_primaries = 0
		if (prg->rg_desired_primaries == 0 &&
		    rg_ptr->rgl_ccr->rg_desired_primaries > 0) {
			res.err_code = SCHA_ERR_DEPEND;
			rgm_format_errmsg(&res, REM_RG_DEP_DESIRED_PRIMARIES,
			    rg_ptr->rgl_ccr->rg_name, prg->rg_name);
			goto bailout; //lint !e801
		}

		// If this RG has Auto_start_on_new_cluster = FALSE,
		// then all dependent RGs must also have
		// Auto_start_on_new_cluster = FALSE
		if (!prg->rg_auto_start && rg_ptr->rgl_ccr->rg_auto_start) {
			res.err_code = SCHA_ERR_DEPEND;
			rgm_format_errmsg(&res, REM_RG_DEP_AUTO_START,
			    rg_ptr->rgl_ccr->rg_name, prg->rg_name);
			goto bailout; //lint !e801
		}
	}

	if (prg->rg_ppinterval < 0) {
		ucmm_print("rg_sanity_checks()",
		    NOGET("RG <%s> pingpong value is less than zero (%d)\n"),
		    prg->rg_name, prg->rg_ppinterval);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_PP_IV_LT_0, prg->rg_name);
		goto bailout; //lint !e801
	}

	// the new dependencies and strong affinities being introduced by
	// this RG must not create a cycle.
	if ((res = detect_rg_dependencies_cycles(prg, affinities,
	    dependee_rgs)).err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM", NOGET("rg_sanity_checks(): "
		    "RG <%s> has cyclic dependencies/affinities\n"),
		    prg->rg_name);
		goto bailout; //lint !e801
	}

	//
	// For argument 'affinities', a list of affinitees is always passed to
	// this routine by the caller.  However check_pos_affinity_managed()
	// will assume the 'affinities' is a list of affinitents when
	// managed_rg is FALSE.   Therefore, we skip this check here if
	// managed_rg=FALSE.
	//
	if (managed_rg) {
		//
		// The last arg is supposed to be failed_op_list which
		// should be NULL in this case.
		//
		res = check_pos_affinity_managed(prg, affinities, B_TRUE,
		    rglistp, NULL);
		if (res.err_code != SCHA_ERR_NOERR) {
			goto bailout; //lint !e801
		}
	}

	// SC SLM addon start
	//
	// Check SLM properties.
	//
	if ((res = rg_slm_sanity_checks(prg, rg_list)).err_code !=
	    SCHA_ERR_NOERR) {
		goto bailout; //lint !e801
	}
	// SC SLM addon end

bailout:
	rgm_free_nlist(dependee_rgs);
	return (res);
}


//
// rs_validate(): Checks a proposed RG structure after it has been
// changed: runs validate method on resource rp on all nodes to
// check that the RG change did not affect the resource.
// Returns appropriate error code from scha_err.h on error, SCHA_ERR_NOERR
// if everything looks ok.
// Returns in the err_msg field of the return value the output of all validate
// methods run.
//
// Because this function traverses the internal state structures, it expects
// the caller to hold the rgm state lock.  This function releases the rgm state
// lock prior to calling VALIDATE on the slave nodes, then reclaims the
// lock after all the VALIDATE calls have completed.
//
// Argument rgl_ccr is the "proposed" (updated) RG, while rg_ptr points to
// the actual current rg structure.  If the update is "busted" due to a node
// death or evacuation, rg_ptr will reflect the "busted" state.
//
// Note: Since we release the rgm lock before running the validate methods,
// the RG state must be locked (by setting the RG's rgl_switching flag to
// SW_UPDATING) before this function is called. This prevents concurrent
// updates to the RG or any of its resources.
//
static scha_errmsg_t
rs_validate(char *rg_name, rgm_rg_t *rgl_ccr, changed_rg_prop *changed,
    rlist_t *rp, rglist_p_t rg_ptr, char *locale)
{
	rgm::idl_validate_args val_args;
	LogicalNodeset *ns = NULL;
	rgm::lni_t lni;
	rgm::rgm_comm_ptr prgm_serv;
	Environment e;
	rgm_resource_t	*rs = rp->rl_ccrdata;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rgm::idl_regis_result_t_var rs_validate_v;
	scha_err_t	validate_err_code = SCHA_ERR_NOERR;
	scha_err_t	ret;
	boolean_t	validate_done = B_FALSE;
	boolean_t	is_scalable;
	sc_syslog_msg_handle_t	handle;
	idlretval_t	idlretval;
	bool is_farmnode;

	// Find the set of nodes on which VALIDATE method are supposed to run
	is_scalable = is_r_scalable(rs);
	ns = compute_meth_nodes(rp->rl_ccrtype, rgl_ccr, METH_VALIDATE,
	    is_scalable);

	// If there is no validate method, return early
	if (ns->isEmpty()) {
		ucmm_print("rs_validate()",
		    NOGET("no need to run VALIDATE on any node\n"));
		delete (ns);
		return (res);
	}

	// VALIDATE has to be called successfully on all nodes in ns currently
	// in the cluster, or else we will return early with an error.
	// We will execute VALIDATE on each node in ns and block waiting
	// for completion.  If any of these calls fails, return early with
	// an error.  If any nodes in ns are not in the cluster membership,
	// we will succeed (provided at least one validate call succeeds)
	// but will return a warning message.

	val_args.idlstr_rs_name = new_str(rs->r_name);
	val_args.idlstr_rt_name = new_str(rs->r_type);
	val_args.idlstr_rg_name = new_str(rg_name);
	val_args.opcode = rgm::RGM_OP_UPDATE;
	val_args.is_scalable = (bool)is_scalable;

	// no change to resource props
	val_args.rs_props = NULL;
	val_args.ext_props = NULL;
	val_args.locale = new_str(locale);

	// extract all the rg properties of the PROPOSED rg
	update_rg_to_strseq(rgl_ccr, val_args.rg_props, changed);

	lni = 0;
	while ((lni = ns->nextLniSet(lni + 1)) != 0) {
		//
		// If the node isn't in current cluster, skip it but
		// set error code SCHA_ERR_RS_VALIDATE in validate_err_code.
		// rgm state mutex is still held.
		//
		LogicalNode *ln = lnManager->findObject(lni);
		CL_PANIC(ln != NULL);
		if (!ln->isInCluster()) {
			validate_err_code = SCHA_ERR_RS_VALIDATE;
			continue;
		}

		//
		// Release the global state machine
		// mutex while executing
		// validate method on the slave.  Then reclaim it.
		//
		rgm_unlock_state();
		if ((rgmx_hook_call(rgmx_hook_scrgadm_rs_validate,
			ln->getNodeid(),
			lni, ln->getIncarnation(),
			&val_args, &rs_validate_v, &idlretval,
			&is_farmnode) == RGMX_HOOK_ENABLED) &&
			(is_farmnode)) {
			rgm_lock_state();
			if (idlretval != RGMIDL_OK) {
				validate_err_code == SCHA_ERR_RS_VALIDATE;
				continue;
			}
		} else {
			prgm_serv = rgm_comm_getref(ln->getNodeid());
			if (CORBA::is_nil(prgm_serv)) {
				validate_err_code = SCHA_ERR_RS_VALIDATE;
				rgm_lock_state();
				continue; // This node is not in the cluster
			}

			prgm_serv->idl_scrgadm_rs_validate
				(orb_conf::local_nodeid(),
			    Rgm_state->node_incarnations.members[
			    orb_conf::local_nodeid()], lni,
				ln->getIncarnation(),
			    val_args, rs_validate_v, e);
			rgm_lock_state();

			CORBA::release(prgm_serv);
			if (e.exception() != NULL) {
				idlretval = except_to_errcode
					(e, ln->getNodeid(), lni);
				e.clear();
				if (idlretval == RGMIDL_NODE_DEATH ||
				    idlretval == RGMIDL_RGMD_DEATH) {
					// This node is not in the cluster,
					// or soon // won't be.
					validate_err_code =
						SCHA_ERR_RS_VALIDATE;
					continue;
				}
				// Some unknown/unexpected exception occurred
				ucmm_print("rs_validate()",
				    NOGET("VALIDATE failed "
				    "with unknown exception"));
				res.err_code = SCHA_ERR_INTERNAL;
				rgm_format_errmsg(&res, REM_INTERNAL);
				delete (ns);
				return (res);
			}
		}

		/*
		 * Save the error message from the validate call to return to
		 * the user whether or not validate passed (fix for EOU
		 * 4219599: Capture stdout/stderr of validate method).
		 *
		 * rgm_sprintf will automatically allocate the necessary
		 * memory, concatenating the new message onto previous messages
		 * if res.err_msg is non-NULL.
		 */
		if ((const char *)rs_validate_v->idlstr_err_msg) {
			/*
			 * If there's a newline char on the end of the old
			 * message, get rid of it, because rgm_sprintf
			 * adds a new one.
			 */
			if (res.err_msg) {
				int last_char = (int)strlen(res.err_msg) - 1;
				if (last_char >= 0 &&
				    res.err_msg[last_char] == '\n') {
					res.err_msg[last_char] = '\0';
				}
			}
			/*
			 * We need to make a temporary copy to avoid the
			 * compiler warning:
			 * A class with a constructor will not reliably work
			 * with "..."
			 */
			char *temp_str = strdup_nocheck(
			    rs_validate_v->idlstr_err_msg);
			if (temp_str == NULL) {
				/* must return with the lock held */
				res.err_code = SCHA_ERR_NOMEM;
				delete (ns);
				return (res);
			}
			rgm_sprintf(&res, "%s", temp_str);
			free(temp_str);
		}


		// If the VALIDATE call fails (returns non-zero or times out)
		// then exit this function early with an error.
		if (rs_validate_v->ret_code != SCHA_ERR_NOERR) {
			const char *nodename = ln->getFullName();
			ucmm_print("rs_validate()",
			    NOGET("VALIDATE failed\n"));
			res.err_code = (scha_err_t)rs_validate_v->ret_code;
			rgm_format_errmsg(&res, REM_VALIDATE_FAILED,
			    rs->r_name, rg_name, nodename);
			delete (ns);
			return (res);
		}
		// Set validate_done flag if we have run validation
		// successfully at least once.
		validate_done = B_TRUE;
	}

	// The global state machine mutex is still held.

	//
	// Check if this update has been "busted".  This means that a current
	// master of the RG has died.  Since we haven't actually updated the
	// CCR yet, we will return early from the attempted update with an
	// error ("cluster is reconfiguring") and no side-effects.
	//
	if ((ret = map_busted_err(rg_ptr->rgl_switching)) != SCHA_ERR_NOERR) {
		// The RG has been busted. Do early return; the caller will
		// call rebalance() and reset the rgl_switching flag.
		ucmm_print("rs_validate",
		    NOGET("RG <%s> is busted with error <%s>"),
		    rg_name, rgm_error_msg(ret));
		res.err_code = ret;
		delete (ns);
		return (res);
	}

	//
	// If validate_done flag is found unset here, it means none
	// of the validation candidates are in the cluster membership.
	// Return with SCHA_ERR_VALIDATE error.
	//
	if (!validate_done) {
		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RG_TAG,
		    rg_name);
		//
		// SCMSGS
		// @explanation
		// Before it will permit the properties of a resource group to
		// be edited, the rgmd runs the VALIDATE method on each
		// resource in the group for which a VALIDATE method is
		// registered. For each such resource, the rgmd must be able
		// to run VALIDATE on at least one node. However, all of the
		// candidate nodes are down. "Candidate nodes" are either
		// members of the resource group's Nodelist or members of the
		// resource type's Installed_nodes list, depending on the
		// setting of the resource's Init_nodes property.
		// @user_action
		// Boot one of the resource group's potential masters and
		// retry the resource creation operation.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "Modification of resource group <%s> failed because none "
		    "of the nodes on which VALIDATE would have run for "
		    "resource <%s> are currently up"), rg_name, rs->r_name);
		sc_syslog_msg_done(&handle);
		res.err_code = SCHA_ERR_VALIDATE;
		delete (ns);
		return (res);
	}

	//
	// Even if there is no other error encountered in this subroutine,
	// the special case SCHA_ERR_RS_VALIDATE might have occurred
	// due to a node not being present in the cluster.  This
	// permits the update to succeed but generates a warning.
	//
	if (res.err_code == SCHA_ERR_NOERR) {
		res.err_code = validate_err_code;
	}
	delete (ns);
	return (res);
}

//
// Create a new Resource Group
// RGs are always initially unmanaged.
//
void
rgm_comm_impl::idl_scrgadm_rg_create(const rgm::idl_rg_create_args &rgcr_args,
		rgm::idl_regis_result_t_out rtui_res, Environment &e)
{
	char			*s = NULL;
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t		check_res = {SCHA_ERR_NOERR, NULL};
	rglist_t		*new_rg = NULL;
	rgm_rg_t		*rg_ccr = NULL;
	char			*rg_name;
	rgm::idl_regis_result_t	*retval = new rgm::idl_regis_result_t();
	rgm_affinities_t 	affinities = {NULL, NULL, NULL, NULL, NULL};
	LogicalNodeset		emptyNodeset;
	const rgm::idl_string_seq_t	empty_rg_list;
	// SC SLM addon start
	int			rg_slm = 0;
	boolean_t ext_affinities = B_FALSE;
	// SC SLM addon end

	rgm_lock_state();

	// Validation checks... Should be unique name
	rg_name = strdup_nocheck(rgcr_args.idlstr_rg_name);
	if (rg_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		goto finished; //lint !e801
	}

	new_rg = rgname_to_rg(rg_name);
	if (new_rg != NULL) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RG_EXISTS, rg_name);
		goto finished; //lint !e801
	}

	//
	// check whether updated property can be updated and it is a valid
	// rg property name
	//
	if ((res = check_rg_updatable_props(rgcr_args.prop_val,
	    rgm::RGM_FROM_SCRGADM)).err_code != SCHA_ERR_NOERR)

		goto finished; //lint !e801

	rg_ccr = (rgm_rg_t *)malloc_nocheck(sizeof (rgm_rg_t));
	if (rg_ccr == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		goto finished; //lint !e801
	}
	(void) memset((char *)rg_ccr, 0, sizeof (rgm_rg_t));
	ucmm_print("RGM", NOGET("idl_scrgadm_rg_create(): RG <%s>\n"), rg_name);

	//
	// Now prepare the static "in-CCR" portion of the RG
	//
	// First convert the name value pairs into a neat little
	// struct we will be writing to the CCR via the low-level
	// CCR interface.
	//
	// We assume that the front end library has done all preliminary
	// verifications. Thus we need not worry about things like, RG name
	// not set, and other such stuff.
	// Check to see if some of these properties are not filled in,
	// if so, Do fill in default values...
	// If get_prop_value routine returns SCHA_ERR_PROP.
	// we can ignor this err because the property is not in the
	// updated list provided by scrgadm.

	rg_ccr->rg_name = strdup(rg_name);

	// Is RG mode defined?
	// If not, the value will be set by checking the value of
	// max_primaries.
	if ((res = get_prop_value(SCHA_RG_MODE, rgcr_args.prop_val,
	    &s, SCHA_PTYPE_ENUM)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_mode = RGMODE_NONE;
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	} else {
		if (s != NULL && s[0] != '\0') {
			rg_ccr->rg_mode = prop_rgmode(s);
			free(s);
			if (rg_ccr->rg_mode == RGMODE_NONE) {
				// Wrong value passed in.
				res.err_code = SCHA_ERR_INVAL;
				ucmm_print("idl_scrgadm_rg_create()", NOGET(
				    "Wrong mode for RG <%s> passed in\n"),
				    rg_name);
				rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
				    SCHA_RG_MODE);
				goto finished; //lint !e801
			}
		} else {
			rg_ccr->rg_mode = RGMODE_NONE;
		}
	}

	if ((res = get_bool_value(SCHA_IMPL_NET_DEPEND, rgcr_args.prop_val,
	    &rg_ccr->rg_impl_net_depend)).err_code
	    != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			// If rg_impl_net_depend isn't set, we will
			// set the default value here.
			rg_ccr->rg_impl_net_depend = B_TRUE;
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	}

	// List of nodes on which this RG could be instantiated.
	// Default value is the current static nodelist of nodeids.
	res = get_value_nodeid_array(rg_name, SCHA_NODELIST,
	    rgcr_args.prop_val, &rg_ccr->rg_nodelist);
	if (SCHA_ERR_NOERR != res.err_code) {
		goto finished; //lint !e801
	}
	if (rg_ccr->rg_nodelist == NULL) {
		//
		// Nodelist is defaulted to the cluster nodes.
		// (We already have all the LNIs)
		//
		// Note: Defaulting empty nodelist to all global zones is
		// questionable on a system that has non-global zones.
		// See CR 6326003 for further information.
		//
		rg_ccr->rg_nodelist = get_static_nodelist();
		//
	} else {
		//
		// The nodelist is not defaulted to the cluster nodes for
		// which we already have an LNI mapping.  Create mapping
		// for missing LNIs.
		//
		// Ideally, we would wait until we know we're going
		// to accept this RG creation (eg after sanity checks,
		// etc) to create mappings and send them to slave.
		// However, the sanity checks, etc. use the mappings,
		// so we have to do it here.
		//
		create_lni_mappings_from_nodeidlist(rg_ccr->rg_nodelist,
		    B_TRUE);
	}

	// Maximum primaries: Default is 1 if not specified.
	if ((res = get_int_value(SCHA_MAXIMUM_PRIMARIES, rgcr_args.prop_val,
	    &rg_ccr->rg_max_primaries)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_max_primaries = 1;
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	}

	// For rg_mode, if it isn't set yet (is RGMODE_NONE),
	// check max_primaries and using its value to decide what rg_mode
	// it will be.
	// So for RG creation, it's legal to pass in RG property with
	// rg_mode set to RGMODE_NONE.
	// After RG is created, rg_mode should be either Failover or
	// Scalable, but can't be RGMODE_NONE.

	if (rg_ccr->rg_mode == RGMODE_NONE) {
		if (rg_ccr->rg_max_primaries > 1)
			rg_ccr->rg_mode = RGMODE_SCALABLE;
		else
			rg_ccr->rg_mode = RGMODE_FAILOVER;
	}

	// Desired primaries: Default is 1 if not specified
	if ((res = get_int_value(SCHA_DESIRED_PRIMARIES, rgcr_args.prop_val,
	    &rg_ccr->rg_desired_primaries)).err_code
	    != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_desired_primaries = 1;
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	}

	// Failback flag: Default is B_FALSE
	if ((res = get_bool_value(SCHA_FAILBACK, rgcr_args.prop_val,
	    &rg_ccr->rg_failback)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_failback = B_FALSE;
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	}

	// Project name: Default is NULL
	if ((res = get_prop_value(SCHA_RG_PROJECT_NAME, rgcr_args.prop_val,
	    &rg_ccr->rg_project_name, SCHA_PTYPE_STRING)).err_code
	    != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_project_name = strdup("");
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	}

	// SC SLM addon start
	// SLM Type: default value is SCHA_SLM_TYPE_MANUAL
	if ((res = get_prop_value(SCHA_RG_SLM_TYPE, rgcr_args.prop_val,
	    &rg_ccr->rg_slm_type, SCHA_PTYPE_STRING)).err_code !=
	    SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_slm_type = strdup(SCHA_SLM_TYPE_MANUAL);
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	} else {
		if (strcmp(rg_ccr->rg_slm_type, SCHA_SLM_TYPE_AUTOMATED) == 0) {
			rg_slm = 1;
		}
	}

	// SLM pset type: default value is SCHA_SLM_PSET_TYPE_DEFAULT
	if ((res = get_prop_value(SCHA_RG_SLM_PSET_TYPE, rgcr_args.prop_val,
	    &rg_ccr->rg_slm_pset_type, SCHA_PTYPE_STRING)).err_code !=
	    SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_slm_pset_type =
			    strdup(SCHA_SLM_PSET_TYPE_DEFAULT);
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	} else {
		if (rg_slm == 0) {
			res.err_code = SCHA_ERR_PROP;
			rgm_format_errmsg(&res, REM_SLM_NOT_AUTOMATED,
			    rg_ccr->rg_name, SCHA_RG_SLM_PSET_TYPE);
			goto finished; //lint !e801
		}
	}

	// RG_SLM_CPU_SHARES default value is SCHA_SLM_RG_CPU_SHARES_DEFAULT
	if ((res = get_int_value(SCHA_RG_SLM_CPU_SHARES, rgcr_args.prop_val,
	    &rg_ccr->rg_slm_cpu)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_slm_cpu = SCHA_SLM_RG_CPU_SHARES_DEFAULT;
			res.err_code = SCHA_ERR_NOERR;
		} else {
			goto finished; //lint !e801
		}
	} else {
		if (rg_slm == 0) {
			res.err_code = SCHA_ERR_PROP;
			rgm_format_errmsg(&res, REM_SLM_NOT_AUTOMATED,
			    rg_ccr->rg_name, SCHA_RG_SLM_CPU_SHARES);
			goto finished; //lint !e801
		}
	}

	// RG_SLM_PSET_MIN default value is SCHA_SLM_RG_PSET_MIN_DEFAULT
	if ((res = get_int_value(SCHA_RG_SLM_PSET_MIN, rgcr_args.prop_val,
	    &rg_ccr->rg_slm_cpu_min)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_slm_cpu_min = SCHA_SLM_RG_PSET_MIN_DEFAULT;
			res.err_code = SCHA_ERR_NOERR;
		} else {
			goto finished;
		}
	} else {
		if (rg_slm == 0) {
			res.err_code = SCHA_ERR_PROP;
			rgm_format_errmsg(&res, REM_SLM_NOT_AUTOMATED,
			    rg_ccr->rg_name, SCHA_RG_SLM_PSET_MIN);
			goto finished; //lint !e801
		}
	}
	// SC SLM addon end

	// Affinities: Default is NULL
	rg_ccr->rg_affinities =
		get_value_string_array(SCHA_RG_AFFINITIES, rgcr_args.prop_val);

	//
	// convert the affinities to a usable in-memory form so we can
	// check for cycles, etc. in the property and sanity checks routines.
	//
	// Note that this conversion will be done again on this node in
	// process_intention, if this resource group is accepted.  We can't
	// save this converted version for later because we don't have our
	// rglist_t in-memory structure yet.
	//
	if ((res = convert_affinities_list(rg_name, rg_ccr->rg_affinities,
	    &affinities)).err_code != SCHA_ERR_NOERR) {
		goto finished; //lint !e801
	}

	// RG auto startup on new cluster flag : Default is B_TRUE
	if ((res = get_bool_value(SCHA_RG_AUTO_START, rgcr_args.prop_val,
	    &rg_ccr->rg_auto_start)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_auto_start = B_TRUE;
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	}

	// Global resources used: Default is wildcard(*)
	rg_ccr->rg_glb_rsrcused =
	    get_value_string_all_list(SCHA_GLOBAL_RESOURCES_USED,
	    rgcr_args.prop_val, B_FALSE);

	rg_ccr->rg_unmanaged = B_TRUE;

	// Dependencies are actually other RGnames.
	rg_ccr->rg_dependencies =
	    get_value_string_array(SCHA_RG_DEPENDENCIES, rgcr_args.prop_val);

	// Pathprefix
	if ((res = get_prop_value(SCHA_PATHPREFIX, rgcr_args.prop_val,
	    &rg_ccr->rg_pathprefix, SCHA_PTYPE_STRING)).err_code
	    != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_pathprefix = strdup("");
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	}
	// A free form description of the RG
	if ((res = get_prop_value(SCHA_RG_DESCRIPTION, rgcr_args.prop_val,
		&rg_ccr->rg_description, SCHA_PTYPE_STRING)).err_code
		!= SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		//
		// validate description value
		// 'TRUE' as the 2nd argument to 'is_rgm_value_valid' call
		// indicates comma is a valid character
		//
		if (!is_rgm_value_valid(rg_ccr->rg_description, B_TRUE)) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
			    SCHA_RG_DESCRIPTION);
			goto finished; //lint !e801
		}
	}
	if (rg_ccr->rg_description == NULL)
		rg_ccr->rg_description = strdup("");

	// Time stamp: Essentially the sequence number assigned by CCR
	rg_ccr->rg_stamp = NULL;

	// Pingpong interval: Default value is 3600 seconds (1 hour)
	if ((res = get_int_value(SCHA_PINGPONG_INTERVAL, rgcr_args.prop_val,
	    &rg_ccr->rg_ppinterval)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_ppinterval = 3600;
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	}

	//
	// RG_System flag: Default is B_FALSE
	//
	if ((res = get_bool_value(SCHA_RG_SYSTEM, rgcr_args.prop_val,
	    &rg_ccr->rg_system)).err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			rg_ccr->rg_system = B_FALSE;
			res.err_code = SCHA_ERR_NOERR;
		} else
			goto finished; //lint !e801
	}

	//
	// check whether the value of each RG property is properly set.
	//
	if ((res = rg_property_check(rg_ccr, &affinities, B_TRUE, B_TRUE,
	    B_TRUE, B_TRUE, B_TRUE, empty_rg_list)).err_code != SCHA_ERR_NOERR)
		goto finished; //lint !e801

	// save the warning message
	if (res.err_msg) {
		rgm_sprintf(&check_res, "%s", res.err_msg);
		free(res.err_msg);
		/* set it to NULL, so we don't try to use it again */
		res.err_msg = NULL;
	}

	if ((res = rg_sanity_checks(rg_ccr, &affinities, B_FALSE, NULL, NULL)).
	    err_code != SCHA_ERR_NOERR)
		goto finished;

	// save the warning message
	if (res.err_msg) {
		rgm_sprintf(&check_res, "%s", res.err_msg);
		free(res.err_msg);
		/* set it to NULL, so we don't try to use it again */
		res.err_msg = NULL;
	}

#if SOL_VERSION >= __s10
	// Validate rg_affinities property for inter cluster rg affinities.
	if (allow_inter_cluster()) {
		if ((res = rg_ext_affinities_validate(rg_ccr, &affinities,
		    B_TRUE, B_TRUE, B_TRUE, B_TRUE)).err_code !=
		    SCHA_ERR_NOERR)
			goto finished; //lint !e801
	}

	// save the warning message
	if (res.err_msg) {
		rgm_sprintf(&check_res, "%s", res.err_msg);
		free(res.err_msg);
		/* set it to NULL, so we don't try to use it again */
		res.err_msg = NULL;
	}
#endif

	ucmm_print("RGM", NOGET("idl_scrgadm_rg_create(): "
	    "RG <%s> max = %d des = %d\n"), rg_name, rg_ccr->rg_max_primaries,
	    rg_ccr->rg_desired_primaries);

	// Latch the intention on all slaves

	//
	// We cannot use the nset field for the set of nodes to run the
	// INIT method since the set of nodes could be different for
	// each resource in the RG. Therefore, the value is left as
	// EMPTY_NODESET.
	//
	ucmm_print("RGM", NOGET("idl_scrgadm_rg_create(): RG <%s> Latching\n"),
	    rg_name);
	(void) latch_intention(rgm::RGM_ENT_RG, rgm::RGM_OP_CREATE,
	    rg_ccr->rg_name, &emptyNodeset, 0);

	ucmm_print("RGM",
	    NOGET("idl_scrgadm_rg_create(): RG <%s> Calling CCR\n"), rg_name);

	res = rgmcnfg_create_rgtable(rg_ccr, ZONE);
	if (res.err_code != SCHA_ERR_NOERR)
		ucmm_print("RGM", NOGET("idl_scrgadm_rg_create(): "
		    "RG <%s> CCR Failed\n"), rg_name);
	else {
		ucmm_print("RGM",
		    NOGET("idl_scrgadm_rg_create(): RG <%s> Processing\n"),
		    rg_name);

		(void) process_intention();

		// Publish an event.  Ignore the return code.  If the
		// rest of the operation succeeds, but this one fails,
		// we still want to count it as a success.
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE, ESC_CLUSTER_RG_CONFIG_CHANGE,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
		    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
		    CL_EVENT_CONFIG_ADDED, NULL);
#else
		(void) sc_publish_event((char *)ESC_CLUSTER_RG_CONFIG_CHANGE,
		    (char *)CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
		    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
		    CL_EVENT_CONFIG_ADDED, NULL);
#endif

		// Publish an rg_state_change event as well.
		// Ignore the return code.
		(void) post_event_rg_state(rg_name);
	}

	// Unlatch the intention on all slaves

	(void) unlatch_intention();

finished:

	free_rgm_affinities(&affinities);

	free(rg_name);

	if (rg_ccr)
		rgm_free_rg(rg_ccr);

	retval->ret_code = res.err_code;

	//
	// Our final error message consists of the warning messages from all
	// checks, followed by any other error message.
	//
	if (res.err_msg)
		rgm_sprintf(&check_res, "%s", res.err_msg);

	retval->idlstr_err_msg = new_str(check_res.err_msg);

	free(res.err_msg);
	free(check_res.err_msg);

	rtui_res = retval;
	// All done... Release lock and return
	rgm_unlock_state();

}

#if (SOL_VERSION >= __s10)
// This function deletes the source affinity rg info by contacting the
// remote cluster
static void
delete_source_affinity_info(name_t rg_name, namelist_t *ext_aff_list,
    char *aff_type)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t	handle;
	namelist_t *affp;

	for (affp = ext_aff_list; affp; affp = affp->nl_next) {
		char *enhanced_aff_name = NULL;
		uint32_t clid;
		int err;
		rgm::idl_rg_update_prop_args_v1 args;
		rgm::idl_regis_result_t_var	result_v;
		rgm::rgm_comm_var prgm_pres_v;
		rgm::idl_nameval_seq_t	prop_val;
		Environment e;
		if (!is_enhanced_naming_format_used(ext_aff_list->nl_name))
			continue;

		// Check if its local cluster rg but specified
		// as cluster:rg by the user.
		char *rc = NULL, *rr = NULL;
		res = get_remote_cluster_and_rorrg_name(ext_aff_list->nl_name,
			&rc, &rr);
		if ((res.err_code == SCHA_ERR_NOERR) && (rc != NULL) &&
		    (rr != NULL)) {
			err = clconf_get_cluster_id(rc, &clid);
			if (((err == 0) && (clid != 0) && (
			    clid < MIN_CLUSTER_ID)) || (
			    (err != 0) && (err != EACCES))) {
				// Inside non-clusterized zone
				res.err_code = SCHA_ERR_CLUSTER;
				rgm_format_errmsg(&res, REM_RGM_IC_UNREACH, rc);
				free(rr);
				free(rc);
				continue;
			} else if (err == EACCES) {
				ASSERT(0); // Cannot happen
			}

			prgm_pres_v = rgm_comm_getpres_zc(rc);
			if (CORBA::is_nil(prgm_pres_v)) {
				res.err_code = SCHA_ERR_CLRECONF;
				rgm_format_errmsg(&res,  REM_RGM_IC_UNREACH,
				    rc);
				free(rr);
				free(rc);
				continue;
			}

			enhanced_aff_name = (char *)malloc(strlen(rg_name) +
			    strlen(ZONE) + 5);


			if (enhanced_aff_name == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				free(rr);
				free(rc);
				return;
			}

			sprintf(enhanced_aff_name, "%s%s:%s", aff_type,
			    ZONE, rg_name);

			/* Set the msg locale */
			args.locale = new_str(fetch_msg_locale());
			/* Set the option flags */
			args.flags = 0;
			args.caller_flag = rgm::RGM_FROM_SCRGADM;
			args.flags |= rgm::SUPPRESS_RG_VALIDATION;
			args.flags |= rgm::INTER_CLUSTER_RG_AFF_UNSET;
			args.flags |= rgm::RG_DELETE_CALL;

			args.idlstr_rg_name = new_str(rr);

			prop_val.length(1);
			prop_val[0].idlstr_nv_name = new_str(
			    SCHA_IC_RG_AFFINITY_SOURCES);

			prop_val[0].nv_val.length(1);
			prop_val[0].nv_val[0] = new_str(enhanced_aff_name);
			args.prop_val = prop_val;

			prgm_pres_v->idl_scrgadm_rg_update_prop_v1(
				args, result_v, e);

			if ((res = except_to_scha_err(e)).err_code !=
			    SCHA_ERR_NOERR) {
				(void) sc_syslog_msg_initialize(&handle,
				SC_SYSLOG_RGM_RS_TAG, rg_name);
				//
				// SCMSGS
				// @explanation
				//
				// The indicated source resource group on the
				// local cluster is being deleted. This resource
				// group had the indicated inter-cluster target
				// RG affinity.  Sun Cluster was unable to
				// remove the affinity information on the
				// target cluster.
				// Possible cause could be that the remote
				// zone cluster is down, or the target
				// affinity rg is switching,
				// or the remote target affinity rg itself
				// is invalid
				// @user_action
				// No action.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "Could not delete inter-cluster resource "
				    "group affinity information on the target"
				    "cluster; "
				    "source RG = <%s>, target RG_affinity = "
				    "<%s>",
				    rg_name, affp->nl_name);
				sc_syslog_msg_done(&handle);
				free(rr);
				free(rc);
				free(enhanced_aff_name);
				continue;
			}

			res.err_code = (scha_err_t)result_v->ret_code;
			if ((const char *)result_v->idlstr_err_msg)
			    res.err_msg = strdup(
				result_v->idlstr_err_msg);
		}
		free(rr);
		free(rc);
		free(enhanced_aff_name);
	}
}

//
// helper function to unset the dependent information from the inter-cluster
// target affinity rg. Called when deleting the source affinity rg.
//
static void
delete_inter_cluster_affinities(rglist_p_t rg_ptr)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	namelist_t *ext_aff_list = NULL;

	// The support for inter-cluster strong positive affinities
	// with failover delegation is not implemented.

	//
	// Check if there are any inter-cluster strong positive affinities
	// for this rg.
	//
	res = get_intercluster_rg_aff_lists(rg_ptr->rgl_affinities,
	    NULL, &ext_aff_list, NULL, NULL, NULL);
	if (res.err_code != SCHA_ERR_NOERR) {
		rgm_free_nlist(ext_aff_list);
		return;
	}

	delete_source_affinity_info(rg_ptr->rgl_ccr->rg_name, ext_aff_list,
	    SP);
	rgm_free_nlist(ext_aff_list);
	ext_aff_list = NULL;

	//
	// Check if there are any inter-cluster weak positive affinities for
	// this rg.
	//
	res = get_intercluster_rg_aff_lists(rg_ptr->rgl_affinities,
	    NULL, NULL, &ext_aff_list, NULL, NULL);
	if (res.err_code != SCHA_ERR_NOERR) {
		rgm_free_nlist(ext_aff_list);
		return;
	}

	delete_source_affinity_info(rg_ptr->rgl_ccr->rg_name, ext_aff_list,
	    WP);
	rgm_free_nlist(ext_aff_list);
	ext_aff_list = NULL;

	//
	// Check if there are any inter-cluster strong negetive affinities
	// for this rg.
	//
	res = get_intercluster_rg_aff_lists(rg_ptr->rgl_affinities,
	    NULL, NULL, NULL, &ext_aff_list, NULL);
	if (res.err_code != SCHA_ERR_NOERR) {
		rgm_free_nlist(ext_aff_list);
		return;
	}

	delete_source_affinity_info(rg_ptr->rgl_ccr->rg_name, ext_aff_list,
	    SN);
	rgm_free_nlist(ext_aff_list);
	ext_aff_list = NULL;

	//
	// Check if there are any inter-cluster weak negetive affinities
	// for this rg
	//
	res = get_intercluster_rg_aff_lists(rg_ptr->rgl_affinities,
	    NULL, NULL, NULL, NULL, &ext_aff_list);
	if (res.err_code != SCHA_ERR_NOERR) {
		rgm_free_nlist(ext_aff_list);
		return;
	}

	delete_source_affinity_info(rg_ptr->rgl_ccr->rg_name, ext_aff_list,
	    WN);
	rgm_free_nlist(ext_aff_list);
}	

//
// This function is used to remove the stale inter cluster source affinity
// rgs if any. The rgm state lock should have been held before this
// function is called.
//

void
cleanup_aff_info(const char *rg_name, const char *dep_aff_name,
    rgm::idl_nameval_seq_t  prop_val)
{
	rgm_rg_t	*rg_ccr = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t	rg_ptr = NULL;
	namelist_t	*raw_affinitents = NULL;
	rgm::intention_flag_t flags = 0;
	LogicalNodeset emptyNodeset;

	if (prop_val.length() == 0) {
		// If there is no property to be updated,
		// return with error.
		ucmm_print("cleanup_aff_info()", NOGET(
		    "No property is updated for RG <%s>\n"), rg_name);
		goto finished; //lint !e801
	}

	ucmm_print("RGM", NOGET("cleanup_aff_info(): "
	    "RG = <%s>\n"), rg_name);

	rg_ptr = rgname_to_rg(rg_name);
	if (rg_ptr == NULL) {
		ucmm_print("RGM", NOGET("cleanup_aff_info(): "
		    "Invalid RG = <%s>\n"), rg_name);
		goto finished; //lint !e801
	}

	rg_ccr = (rgm_rg_t *)malloc_nocheck(sizeof (rgm_rg_t));
	if (rg_ccr == NULL) {
		ucmm_print("RGM", NOGET("cleanup_aff_info(): "
		    "No memory"));
		goto finished; //lint !e801
	}

	// Copy the CCR stuff into the "proposed"
	// rg_ccr. Note that we don't do a "deep" copy
	// here. If some members of the struct are pointers,
	// we just pick up the same values. If THAT property
	// is actually being updated, we will create a new
	// pointer and allocate memory...
	//
	// Beware that if rgm_free_rg() is called on the
	// proposed RG struct, it will free EVERYTHING...

	*rg_ccr = *(rg_ptr->rgl_ccr);

	raw_affinitents = get_value_string_array(
	    SCHA_IC_RG_AFFINITY_SOURCES, prop_val);
	if (raw_affinitents) {
		rg_ccr->rg_affinitents = raw_affinitents;
	} else {
		// Nothing more to do. Return early.
		goto finished; //lint !e801
	}

	(void) latch_intention(rgm::RGM_ENT_RG, rgm::RGM_OP_UPDATE,
	    rg_name, &emptyNodeset, 0);

	ucmm_print("RGM",
	    NOGET("cleanup_aff_info(): RG <%s> Calling CCR\n"),
	    rg_name);

	res = rgmcnfg_update_rgtable(rg_ccr, (const char*)ZONE,
	    B_TRUE);
	if (res.err_code != SCHA_ERR_NOERR)
		ucmm_print("RGM", NOGET("cleanup_aff_info(): "
		    "RG <%s> CCR Failed\n"), rg_name);
	else {
		ucmm_print("RGM", NOGET("cleanup_aff_info(): "
		    "RG <%s> Processing\n"), rg_name);

		(void) process_intention();
	}

	// Unlatch the intention on all slaves

	(void) unlatch_intention();
finished:
	if (rg_ccr) {
		if (raw_affinitents) {
			rgm_free_nlist(raw_affinitents);
		}
		free(rg_ccr);
	}
	free(res.err_msg);
}
#endif

//
// Update various properties of an RG.
//
// We set the rgl_switching flag on the rg to SW_UPDATING, which will prevent
// any other thread from doing an update for this RG.
//
static void
scrgadm_rg_update_prop_helper(
	const rgm::idl_rg_update_prop_args_v1 &rgup_args,
	rgm::idl_regis_result_t_out rgup_res)
{
	char		*s = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	warning_res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	validate_res = {SCHA_ERR_NOERR, NULL};
	char *rg_name = NULL, *locale = NULL;
	rgm_rg_t	*rg_ccr = NULL;
	rglist_p_t	rg_ptr = NULL;
	nodeidlist_t	*nodelist = NULL;
	namelist_t	*deplist = NULL;
	name_t		desc = NULL;
	name_t		pathprefix = NULL;
	name_t		proj_name = NULL;
	// SC SLM addon start
	name_t		slm_type = NULL;
	name_t		slm_pset_type = NULL;
	int		rg_slm = 0;
	// SC SLM addon end
	namelist_t	*raw_affinities = NULL;
	namelist_t	*raw_affinitents = NULL;
	namelist_all_t	globrsrc;

	LogicalNodeset	*new_ns = NULL;
	LogicalNodeset	*old_ns = NULL;
	LogicalNodeset	removed_ns;
	LogicalNodeset	added_ns;

	LogicalNodeset pending_boot_ns;
	LogicalNodeset stop_failed_ns;
	LogicalNodeset r_restart_ns;
	LogicalNodeset *switch_ns;

	bool need_run_init = false;
	bool ng_zone_removed = false;
	rlist_t		*rsrc;
	rgm_affinities_t affinities = {NULL, NULL, NULL, NULL, NULL};
	rgm_affinities_t affinitents = {NULL, NULL, NULL, NULL, NULL};
	rgm::intention_flag_t flags = 0;
	LogicalNodeset emptyNodeset;
	char *ucmm_buffer1 = NULL;
	char *ucmm_buffer2 = NULL;
	boolean_t ext_affinities = B_FALSE;

	changed_rg_prop	changed = {
	    0,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    // SC SLM addon start
	    B_FALSE,
	    B_FALSE,
	    B_FALSE,
	    B_FALSE
	    // SC SLM addon end
	};
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	scha_err_t	validate_err_code = SCHA_ERR_NOERR;
	boolean_t	need_cond_wait = B_FALSE;
	sol::nodeid_t	nodeid;
	boolean_t	only_susp_resume_updated = B_FALSE;
	boolean_t	only_rg_system_updated = B_FALSE;
	rg_switch_t	save_rgl_switching;

	globrsrc.is_ALL_value = B_FALSE;
	globrsrc.names = NULL;
	PerNodeFlagsT nodeflags;
	char *ucmm_buffer = NULL;
	unsigned long aff_set_flag = 0;
	unsigned long aff_unset_flag = 0;
	unsigned long aff_rg_delete_flag = 0;

	// Check if the source affinity rg prop is set or unset.
	if (allow_inter_cluster()) {
		aff_set_flag = rgup_args.flags & rgm::INTER_CLUSTER_RG_AFF_SET;
		aff_unset_flag = rgup_args.flags &
		    rgm::INTER_CLUSTER_RG_AFF_UNSET;

	// To remove the source affinity rg information as that rg is
	// being deleted.
		aff_rg_delete_flag = rgup_args.flags & rgm::RG_DELETE_CALL;
	}
	//
	// This function gets called in three scenarios.
	//
	// called while a RG property is updated during which
	// we need to get the lock.
	//
	// called from delete_source_affinity_info. Even
	// in this case we need to acquire the lock. The aff_rg_delete_flag
	// will be set in this case.
	//
	// called from idl_ext_rg_validate_prop, the lock would have been
	// already taken in idl_ext_rg_validate_prop
	// for if the source affinity rg property is to be updated.
	// The aff_set_flag or aff_unset_flag will be set during this.
	//
	if ((!aff_set_flag && !aff_unset_flag) || aff_rg_delete_flag)
		rgm_lock_state();
	rg_name = strdup_nocheck(rgup_args.idlstr_rg_name);

	/* Get the locale */
	locale = strdup_nocheck(rgup_args.locale);

	if (rg_name == NULL || locale == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		goto finished; //lint !e801
	}

	if (rgup_args.prop_val.length() == 0) {
		// If there is no property to be updated,
		// return with error.
		res.err_code = SCHA_ERR_INVAL;
		ucmm_print("scrgadm_rg_update_prop_helper()", NOGET(
		    "No property is updated for RG <%s>\n"), rg_name);
		goto finished; //lint !e801
	}

	ucmm_print("RGM", NOGET("scrgadm_rg_update_prop_helper(): "
	    "RG = <%s>\n"), rg_name);

	rg_ptr = rgname_to_rg(rg_name);
	if (rg_ptr == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_INVALID_RG, rg_name);
		ucmm_print("RGM", NOGET("scrgadm_rg_update_prop_helper(): "
		    "Invalid RG = <%s>\n"), rg_name);
		goto finished; //lint !e801
	}

	/*
	 * Check if Suspend/Resume is the only property being updated,
	 * if yes then skip the in_endish check for this updating it.
	 * This will be allowed only if Suspend/Resume is the one and
	 * only property being updated, not in any other case.
	 *
	 * This call also checks if rg_system is the only property being
	 * updated.  If so, then we will skip execution of Validate and Update
	 * methods on resources in the RG.
	 */
	res = only_prop_updated(rgup_args.prop_val, only_susp_resume_updated,
	    only_rg_system_updated);
	if (res.err_code != SCHA_ERR_NOERR) {
		goto finished; //lint !e801
	}

	if (!only_susp_resume_updated) {
		// The RG must be in an "endish" state.
		if (!in_endish_rg_state(rg_ptr, &pending_boot_ns,
		    &stop_failed_ns, NULL, &r_restart_ns) ||
		    !pending_boot_ns.isEmpty() ||
		    !r_restart_ns.isEmpty()) {
			// RG is busy or running BOOT methods
			// on some node
			res.err_code = SCHA_ERR_RGRECONF;
			rgm_format_errmsg(&res, REM_RG_BUSY, rg_name);
			ucmm_print("scrgadm_rg_update_prop_helper",
			    NOGET("RG <%s> is not in a stable state\n"),
			    rg_name);
			goto finished; //lint !e801
		}
	}

	if (only_susp_resume_updated && ((rg_ptr->rgl_switching ==
	    SW_UPDATING) || (rg_ptr->rgl_switching == SW_UPDATE_BUSTED))) {
		res.err_code = SCHA_ERR_RGRECONF;
		rgm_format_errmsg(&res, REM_RG_BUSY, rg_name);
		ucmm_print("scrgadm_rg_update_prop_helper",
		    NOGET(" RG <%s> switching flag is SW_SW_UPDATE_BUSTED "
		    "or SW_UPDATING. Not proceeding with update of suspend "
		    "automatic recovery flag\n"), rg_name);
		goto finished; //lint !e801
	}

	//
	// ERROR_STOP_FAILED is an endish state but still precludes RG update.
	// According to the RGM architecture, no update is permitted on the
	// RG until the administrator clears the error state.
	//
	if (!stop_failed_ns.isEmpty()) {
		// The RG is ERROR_STOP_FAILED on some node
		res.err_code = SCHA_ERR_STOPFAILED;
		rgm_format_errmsg(&res, REM_RG_STOPFAILED, rg_name);
		ucmm_print("RGM",
		    NOGET("scrgadm_rg_update_prop_helper: RG <%s> is "
		    "ERROR_STOP_FAILED\n"), rg_name);
		goto finished; //lint !e801
	}

	//
	// check whether updated property can be changed and it is a valid
	// rg property name
	//

	if ((res = check_rg_updatable_props(rgup_args.prop_val,
	    rgup_args.caller_flag)).err_code != SCHA_ERR_NOERR)
		goto finished; //lint !e801


	//
	// "system" property checks:
	// The only property which can be updated on a "system" RG is the system
	// property and suspend_automatic_recovery property.
	// For suspend_automatic_recovery property, a warning is generated.
	//
	if (rg_ptr->rgl_ccr->rg_system) {
		if ((res = check_system_rg_updatable_props(rg_ptr,
		    rgup_args.prop_val,
		    &warning_res)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
	}

	//
	// non-global zone check
	//
	res = check_ngzone_rg_updatable_props(rg_ptr, rgup_args);
	if (res.err_code != SCHA_ERR_NOERR)
		goto finished; //lint !e801

	rg_ccr = (rgm_rg_t *)malloc_nocheck(sizeof (rgm_rg_t));
	if (rg_ccr == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		goto finished; //lint !e801
	}

	// Copy the CCR stuff into the "proposed"
	// rg_ccr. Note that we don't do a "deep" copy
	// here. If some members of the struct are pointers,
	// we just pick up the same values. If THAT property
	// is actually being updated, we will create a new
	// pointer and allocate memory...
	//
	// Beware that if rgm_free_rg() is called on the
	// proposed RG struct, it will free EVERYTHING...

	*rg_ccr = *(rg_ptr->rgl_ccr);

	rg_ccr->rg_name = strdup(rg_name);


	if (aff_set_flag || aff_unset_flag) {
		raw_affinitents = get_value_string_array(
		    SCHA_IC_RG_AFFINITY_SOURCES, rgup_args.prop_val);
		if (raw_affinitents) {
			rg_ccr->rg_affinitents = raw_affinitents;
			changed.rg_affinitents = B_TRUE;
			++changed.len;
		}
		goto icrga_intention_routine;
	}
	// Now get all the properties which are being updated. Note
	// that we cannot validate each property one at a time
	// because there are interdependencies. Example: max_primaries
	// must be less then or equal to the number of nodes in
	// the nodelist. So if BOTH are being updated simualtaneously,
	// we have a problem.
	// The approach we use here is that we make a copy of the
	// existing RG and update the values which are being
	// changed and then validate the proposed RG in one shot.
	// The code to do generic RG validation is in rg_sanity_checks().
	//
	// If get_prop_value routine returns SCHA_ERR_PROP.
	// we can ignor this error because the property is not in the
	// updated list provided by scrgadm.
	// If the user specifiey nothing,
	// Example:
	//	scrgadm -c -g -y <propertyname>=
	// we will not update the property value if the property type is
	// BOOLEAN, ENUM or INT because these properties have to give
	// a value.
	//
	if ((res = get_prop_value(SCHA_RG_MODE, rgup_args.prop_val,
	    &s, SCHA_PTYPE_ENUM)).err_code == SCHA_ERR_PROP) {
		// should get SCHA_ERR_PROP because
		// rg_mode is not updatable.
		res.err_code = SCHA_ERR_NOERR;
	} else {
		// rg_mode is not updatable.
		res.err_code = SCHA_ERR_UPDATE;
		rgm_format_errmsg(&res, REM_UPDATE_RGMODE, rg_name);
		ucmm_print("scrgadm_rg_update_prop_helper()", NOGET(
		    "Incorrectly try to update rg_mode of RG <%s>\n"), rg_name);
		goto finished; //lint !e801
	}

	if ((res = get_bool_value(SCHA_IMPL_NET_DEPEND, rgup_args.prop_val,
	    &rg_ccr->rg_impl_net_depend)).err_code
	    != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		changed.rg_impl_net_depend = B_TRUE;
		++changed.len;
	}

	// The NODELIST can be changed.
	res = get_value_nodeid_array(rg_name, SCHA_NODELIST,
	    rgup_args.prop_val, &nodelist);
	if (SCHA_ERR_NOERR != res.err_code) {
		goto finished; //lint !e801
	}

	if (nodelist) {
		//
		// Create LNI mappings and send them to the slaves
		//
		// Ideally, we would do this only when we're sure
		// that the update will be accepted. However, the sanity
		// checks, etc. depend on the lni mappings being there
		// already, so we have to do it now.
		//
		create_lni_mappings_from_nodeidlist(nodelist, B_TRUE);

		// The nodes in the nodelist must be in the RT installed
		// nodes list for each resource.
		for (rsrc = rg_ptr->rgl_resources; rsrc != NULL;
		    rsrc = rsrc->rl_next) {
			if ((res = rgnodelist_subset_rtnodelist(rg_name,
			    nodelist, rsrc->rl_ccrtype)).err_code !=
			    SCHA_ERR_NOERR) {
				goto finished; //lint !e801
			}
		}

		//
		// Find out which nodenames are newly added. some resources in
		// this rg might need to run init on those nodes.
		//
		// Note: at version 0, we did not run FINI on the deleted
		// nodes now since scalable service RT FINI de-configured
		// the resource. However, at version 1, we do run FINI.
		// We updated the scalable service RT to distinguish between
		// local-node cleanup and global cleanup.
		//
		old_ns = get_logical_nodeset_from_nodelist(
		    rg_ccr->rg_nodelist);
		new_ns = get_logical_nodeset_from_nodelist(nodelist);
		added_ns = *new_ns - *old_ns;
		removed_ns = *old_ns - *new_ns;

		delete(new_ns);
		delete(old_ns);


		if ((added_ns.display(ucmm_buffer1) == 0) &&
		    (removed_ns.display(ucmm_buffer2)) == 0) {
			ucmm_print("scrgadm_rg_update_prop_helper()",
			    NOGET("added_ns is %s\n; removed_ns is %s\n"),
			    ucmm_buffer1, ucmm_buffer2);
		}
		free(ucmm_buffer1);
		free(ucmm_buffer2);

		rg_ccr->rg_nodelist = nodelist;
		changed.rg_nodelist = B_TRUE;
		++changed.len;
	}

	// Pathprefix
	if ((res = get_prop_value(SCHA_PATHPREFIX, rgup_args.prop_val,
	    &pathprefix, SCHA_PTYPE_STRING)).err_code
	    != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		if (pathprefix) {
			rg_ccr->rg_pathprefix = pathprefix;
			changed.rg_pathprefix = B_TRUE;
			++changed.len;
		}
	}

	// Project name
	if ((res = get_prop_value(SCHA_RG_PROJECT_NAME, rgup_args.prop_val,
	    &proj_name, SCHA_PTYPE_STRING)).err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else if (proj_name) {
		rg_ccr->rg_project_name = proj_name;
		changed.rg_project_name = B_TRUE;
		++changed.len;
	}

	// SC SLM addon start
	// SLM type
	if ((res = get_prop_value(SCHA_RG_SLM_TYPE, rgup_args.prop_val,
	    &slm_type, SCHA_PTYPE_STRING)).err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		if (slm_type) {
			rg_ccr->rg_slm_type = slm_type;
			changed.rg_slm_type = B_TRUE;
			++changed.len;
		}
	}
	if (strcmp(rg_ccr->rg_slm_type, SCHA_SLM_TYPE_AUTOMATED) == 0) {
		rg_slm = 1;
	}

	// SLM pset type
	if ((res = get_prop_value(SCHA_RG_SLM_PSET_TYPE, rgup_args.prop_val,
	    &slm_pset_type, SCHA_PTYPE_STRING)).err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		if (slm_pset_type) {
			if (rg_slm == 0) {
				res.err_code = SCHA_ERR_PROP;
				rgm_format_errmsg(&res, REM_SLM_NOT_AUTOMATED,
				    rg_ccr->rg_name, SCHA_RG_SLM_PSET_TYPE);
				goto finished; //lint !e801
			}
			rg_ccr->rg_slm_pset_type = slm_pset_type;
			changed.rg_slm_pset_type = B_TRUE;
			++changed.len;
		}
	}

	// RG_SLM_CPU_SHARES
	if ((res = get_int_value(SCHA_RG_SLM_CPU_SHARES, rgup_args.prop_val,
	    &rg_ccr->rg_slm_cpu)).err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		if (rg_slm == 0) {
			res.err_code = SCHA_ERR_PROP;
			rgm_format_errmsg(&res, REM_SLM_NOT_AUTOMATED,
			    rg_ccr->rg_name, SCHA_RG_SLM_CPU_SHARES);
			goto finished; //lint !e801
		}
		changed.rg_slm_cpu = B_TRUE;
		++changed.len;
	}

	// RG_SLM_PSET_MIN
	if ((res = get_int_value(SCHA_RG_SLM_PSET_MIN, rgup_args.prop_val,
	    &rg_ccr->rg_slm_cpu_min)).err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		if (rg_slm == 0) {
			res.err_code = SCHA_ERR_PROP;
			rgm_format_errmsg(&res, REM_SLM_NOT_AUTOMATED,
			    rg_ccr->rg_name, SCHA_RG_SLM_PSET_MIN);
			goto finished; //lint !e801
		}
		changed.rg_slm_cpu_min = B_TRUE;
		++changed.len;
	}
	// SC SLM addon end

	// Affinities
	raw_affinities = get_value_string_array(SCHA_RG_AFFINITIES,
	    rgup_args.prop_val);
	if (raw_affinities) {
		rg_ccr->rg_affinities = raw_affinities;
		changed.rg_affinities = B_TRUE;
		++changed.len;
	}

	//
	// convert the affinities so we can check for cycles, etc. in the
	// property and sanity checks routines.
	//
	// Note that this conversion will be done again on this node in
	// process_intention, if this resource group is accepted.  We can't
	// save this converted version for later because we don't want to
	// update our rglist_t in-memory structure yet (in case the RG is
	// not accepted).
	//
	if ((res = convert_affinities_list(rg_name, rg_ccr->rg_affinities,
	    &affinities)).err_code != SCHA_ERR_NOERR) {
		goto finished; //lint !e801
	}

	// RG SUSP_AUTO_RECOVERY
	if ((res = get_bool_value(SCHA_RG_SUSP_AUTO_RECOVERY,
		rgup_args.prop_val, &rg_ccr->rg_suspended))
		    .err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP) {
			res.err_code = SCHA_ERR_NOERR;
		} else {
			goto finished; //lint !e801
		}
	} else {
		changed.rg_suspended = B_TRUE;
		++changed.len;
	}


	// RG auto start on new cluster
	if ((res = get_bool_value(SCHA_RG_AUTO_START, rgup_args.prop_val,
	    &rg_ccr->rg_auto_start)).err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		changed.rg_auto_start = B_TRUE;
		++changed.len;
	}

	// get_<type>_value() routines DO NOT overwrite the passed in
	// pointer if the value is absent in the property list or property
	// value is blank.
	// That is exactly what we want, use the one in the existing
	// RG, if not being updated.
	if ((res = get_int_value(SCHA_MAXIMUM_PRIMARIES, rgup_args.prop_val,
	    &rg_ccr->rg_max_primaries)).err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		changed.rg_max_primaries = B_TRUE;
		++changed.len;
	}

	if ((res = get_int_value(SCHA_DESIRED_PRIMARIES, rgup_args.prop_val,
	    &rg_ccr->rg_desired_primaries)).err_code
	    != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		changed.rg_desired_primaries = B_TRUE;
		++changed.len;
	}

	if ((res = get_bool_value(SCHA_FAILBACK, rgup_args.prop_val,
	    &rg_ccr->rg_failback)).err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		changed.rg_failback = B_TRUE;
		++changed.len;
	}

	//
	// RG_System Property
	//
	if ((res = get_bool_value(SCHA_RG_SYSTEM, rgup_args.prop_val,
	    &rg_ccr->rg_system)).err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		changed.rg_system = B_TRUE;
		++changed.len;
	}

	if ((res = get_prop_value(SCHA_RESOURCE_LIST, rgup_args.prop_val,
	    &s, SCHA_PTYPE_STRINGARRAY)).err_code == SCHA_ERR_PROP) {
		// it is OK if this property is not in the updated list
		// because Resource_list is not allowed to be changed with
		// this interface.
		res.err_code = SCHA_ERR_NOERR;
	} else {
		ucmm_print("RGM", NOGET("Resource_list can't be changed\n"));
		res.err_code = SCHA_ERR_UPDATE;
		rgm_format_errmsg(&res, REM_UPDATE_RS_LIST, rg_name);
		goto finished; //lint !e801
	}

	deplist = get_value_string_array(SCHA_RG_DEPENDENCIES,
	    rgup_args.prop_val);
	if (deplist) {
		rg_ccr->rg_dependencies = deplist;
		changed.rg_dependencies = B_TRUE;
		++changed.len;
	}
	if ((res = get_prop_value(SCHA_RG_DESCRIPTION, rgup_args.prop_val,
	    &desc, SCHA_PTYPE_STRING)).err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		//
		// validate description value
		// 'TRUE' as the 2nd argument to 'is_rgm_value_valid' call
		// indicates comma is a valid character
		//
		if (!is_rgm_value_valid(desc, B_TRUE)) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
			    SCHA_RG_DESCRIPTION);
			goto finished; //lint !e801
		}
		if (desc) {
			rg_ccr->rg_description = desc;
			changed.rg_description = B_TRUE;
			++changed.len;
		}
	}

	//
	// if GLOBAL_RESOURCES_USED is changed,
	// globrsrc.is_ALL_value is TRUE when "*" is specified or
	// globrsrc.names is not NULL when global resources are specified.
	// Otherwise, globrsrc.is_ALL_value is FALSE and globrsrc.names is NULL
	//
	globrsrc = get_value_string_all_list(SCHA_GLOBAL_RESOURCES_USED,
	    rgup_args.prop_val, B_TRUE);
	if (globrsrc.is_ALL_value || globrsrc.names != NULL) {
		rg_ccr->rg_glb_rsrcused = globrsrc;
		changed.rg_glb_rsrcused = B_TRUE;
		++changed.len;

		// If RG freeze state changed due to edit, signal the
		// rgfreeze() thread to update RG freeze states.
		if (compute_freeze_opcode(rg_ptr) != RGMFR_NONE) {
			cond_rgfreeze.signal();
		}
	}

	if ((res = get_int_value(SCHA_PINGPONG_INTERVAL, rgup_args.prop_val,
	    &rg_ccr->rg_ppinterval)).err_code != SCHA_ERR_NOERR) {
		// it is OK if this property is not in the updated list
		if (res.err_code == SCHA_ERR_PROP)
			res.err_code = SCHA_ERR_NOERR;
		else
			goto finished; //lint !e801
	} else {
		changed.rg_ppinterval =  B_TRUE;
		++changed.len;
	}

	//
	// check whether the value of each RG property is properly set.
	//
	if ((res = rg_property_check(rg_ccr, &affinities, B_FALSE,
	    changed.rg_affinities, changed.rg_desired_primaries,
	    changed.rg_nodelist, changed.rg_auto_start,
	    rgup_args.rglist)).err_code != SCHA_ERR_NOERR)
		goto finished; //lint !e801

	// save the warning message
	if (res.err_msg) {
		rgm_sprintf(&validate_res, "%s", res.err_msg);
		free(res.err_msg);
		/* set it to NULL, so we don't try to use it again */
		res.err_msg = NULL;
	}

	// Do generic RG validation on the RG.
	// If rg is in managed state, all the RGs in the RG_dependencies
	// list must be managed.  Otherwise, it is OK if they are not in
	// managed state.
	if (rg_ccr->rg_unmanaged)
		res = rg_sanity_checks(rg_ccr, &affinities, B_FALSE,
		    NULL, rgup_args.rglist);
	else
		res = rg_sanity_checks(rg_ccr, &affinities, B_TRUE,
		    NULL, rgup_args.rglist);
	if (res.err_code != SCHA_ERR_NOERR)
		goto finished; //lint !e801

	//
	// check if updating or reenabling the
	// implicit_network_dependencies creates cyclic dependencies.
	//
	if (rg_ccr->rg_impl_net_depend) {
		if ((res =
		    implicit_dependencies_sanity_check(
		    rg_ptr)).err_code != SCHA_ERR_NOERR)
			goto finished; //lint !e801
	}

	//
	// Check if there are any RG-node cycles using both
	// rg_dependencies and inter-rg resource dependencies as edges.
	//
	// We don't put this call in rg_sanity_checks because we don't
	// need to call it on RG creation.
	//
	// Note that we must call it _after_ some code (namely
	// rg_sanity_check) has verified that all the rg names in the
	// rg_dependencies_list are valid rg names.
	//
	res = detect_combined_dependencies_cycles(rg_ptr, rg_ccr, NULL);
	if (res.err_code != SCHA_ERR_NOERR) {
		goto finished; //lint !e801
	}

#if SOL_VERSION >= __s10
	// Do the same if we have any remote cluster affinities.
	if ((allow_inter_cluster()) && (changed.rg_affinities ||
	    changed.rg_desired_primaries ||changed.rg_nodelist)) {
		if ((res = rg_ext_affinities_validate(rg_ccr, &affinities,
				B_FALSE, changed.rg_affinities,
				changed.rg_desired_primaries,
				changed.rg_nodelist)).err_code !=
				SCHA_ERR_NOERR)
			goto finished; //lint !e801
	}


	// save the warning message
	if (res.err_msg) {
		rgm_sprintf(&validate_res, "%s", res.err_msg);
		free(res.err_msg);
		/* set it to NULL, so we don't try to use it again */
		res.err_msg = NULL;
	}
#endif

	//
	// Since we release the rgm lock before running the resource
	// Validate, Update, Init or Fini methods, we need to lock the state
	// of the RG.
	// Set a flag on the RG indicating that it is being updated.
	// This acts as an advisory lock preventing other updates/switches.
	// Note that this causes "is rg in an end-ish state?" to be false.
	// [We assert that no other thread on this president can change the
	// state of this RG or of any of its resources until we clear
	// this flag.]
	// If there are any failures after this point, we need to
	// go to rg_update_end_clr_sw to reset the switching flag
	// back to none.
	//
	//
	// We don't need to set/reset the rgl_switching flag when
	// suspend automatic recovery property alone is updated as
	// no validate method is invoked and the rgm state lock is not
	// released.
	//
	if (!only_susp_resume_updated)
		rg_ptr->rgl_switching = SW_UPDATING;

	//
	// Run validate method on all Rs in RG to check that the change
	// does not affect the resources.
	//
	// If Suspend/Resume or RG_system is the one and only property being
	// changed, or if the SUPPRESS_RG_VALIDATION option flag is set, then
	// we skip the validation.
	//
	// The SUPPRESS_RG_VALIDATION flag is currently used for the purpose
	// of removing RG_dependencies or RG_affinities upon an RG which is
	// being force-deleted.  Since Validate methods are skipped,
	// forced-deletion might leave the remaining resources
	// or RGs in an invalid configuration.  This will be documented.
	//


	if (!only_susp_resume_updated && !only_rg_system_updated &&
	    !(rgup_args.flags & rgm::SUPPRESS_RG_VALIDATION)) {
		//
		// Before the validate methods are called we
		// make sure that no lni's will be reclaimed
		// during the period of validation.
		//
		lnManager->reclaimOff();
		for (rsrc = rg_ptr->rgl_resources; rsrc != NULL;
		    rsrc = rsrc->rl_next) {
			res = rs_validate(rg_name, rg_ccr, &changed, rsrc,
			    rg_ptr, locale);

			/*
			 * Save the error message from the validate call to
			 * return to the user whether or not validate passed
			 * (fix for EOU 4219599: Capture stdout/stderr of
			 * validate method).
			 *
			 * rgm_sprintf will automatically allocate the necessary
			 * memory, concatenating the new message onto previous
			 * messages if validate_res.err_msg is non-NULL.
			 */
			if (res.err_msg) {
				/*
				 * If there's a newline char on the end of the
				 * old message, get rid of it, because
				 * rgm_format_errmsg adds a new one.
				 */
				if (validate_res.err_msg) {
					int last_char = (int)strlen(
					    validate_res.err_msg) - 1;
					if (last_char >= 0 &&
					    validate_res.err_msg[last_char] ==
					    '\n') {
						validate_res.err_msg[last_char]
						    = '\0';
					}
				}

				rgm_sprintf(&validate_res, "%s", res.err_msg);
				free(res.err_msg);
				// set it to NULL, so we don't try
				// to use it again
				res.err_msg = NULL;
			}

			// SCHA_ERR_RS_VALIDATE will generate a warning only if
			// no other error occurs.
			if (res.err_code == SCHA_ERR_RS_VALIDATE) {
				validate_err_code = SCHA_ERR_RS_VALIDATE;
			} else if (res.err_code != SCHA_ERR_NOERR) {
				retval->ret_code = res.err_code;
				goto rg_update_end_clr_sw; //lint !e801
			}
			//
			// If one resource is enabled on at least one node, we
			// will execute the cond_wait loop after processing the
			// intention.
			//
			rgm::lni_t lni = 0;
			switch_ns = get_logical_nodeset_from_nodelist(
			    rg_ccr->rg_nodelist);
			while ((lni = switch_ns->nextLniSet(lni + 1))
			    != 0) {
				if (((rgm_switch_t *)rsrc->rl_ccrdata->
				    r_onoff_switch)->r_switch[lni] !=
				    SCHA_SWITCH_DISABLED) {
					ucmm_print(
					    "scrgadm_rg_update_prop_helper()",
					    NOGET("Dynamic Update in action"));
					need_cond_wait = B_TRUE;
					break;
				}
			}
		}

		//
		// Since the rgm state mutex was released during execution
		// of Validate methods, we have to check again whether the
		// RG is in a non-endish state or ERROR_STOP_FAILED.  This
		// repeats the checks that were done earlier in this function.
		//
		// First, flush the state change queue.  The initial set of
		// state changes on the queue might trigger restart-
		// dependencies on non-president slaves, which would cause
		// RG_ON_PENDING_R_RESTART state transitions to get pushed
		// immediately onto the queue.  So, we pass B_TRUE to
		// flush_state_change_queue() to indicate that it should use
		// the QEMPTY option to keep processing pending state changes
		// until the queue is empty.
		//
		rgm_unlock_state();
		flush_state_change_queue(B_TRUE);
		rgm_lock_state();

		//
		// There is a slight chance that a different president-side
		// thread will grab the state lock after the
		// flush_state_change_queue() call has finished, and trigger
		// a resource restart in our RG, before we have a chance to
		// reclaim the lock.  To avoid this, trigger_restart() checks
		// if rgl_switching == SW_UPDATING, and if the resource restart
		// would occur on a non-president slave.  If so, it releases
		// the lock and does a thr_yield() to return control to this
		// thread.
		//

		lnManager->reclaimOn(); // re-enable lni reclaim

		//
		// Now repeat the RG busy checks
		// We temporarily reset the rgl_switching flag for the
		// in_endish_rg_state() call.
		//
		save_rgl_switching = rg_ptr->rgl_switching;
		rg_ptr->rgl_switching = SW_NONE;
		if (!in_endish_rg_state(rg_ptr, &pending_boot_ns,
		    &stop_failed_ns, NULL, &r_restart_ns) ||
		    !pending_boot_ns.isEmpty() || !r_restart_ns.isEmpty()) {
			// RG is busy or running BOOT methods
			// on some node
			res.err_code = SCHA_ERR_RGRECONF;
			rgm_format_errmsg(&res, REM_RG_BUSY, rg_name);
			ucmm_print("scrgadm_rg_update_prop_helper",
			    NOGET("RG <%s> is not in a stable state after"
			    " Validate meths\n"), rg_name);
			// restore saved value of rgl_switching
			rg_ptr->rgl_switching = save_rgl_switching;
			goto rg_update_end_clr_sw; //lint !e801
		}
		// restore saved value of rgl_switching
		rg_ptr->rgl_switching = save_rgl_switching;
		if (!stop_failed_ns.isEmpty()) {
			// The RG is ERROR_STOP_FAILED on some node
			res.err_code = SCHA_ERR_STOPFAILED;
			rgm_format_errmsg(&res, REM_RG_STOPFAILED, rg_name);
			ucmm_print("RGM",
			    NOGET("scrgadm_rg_update_prop_helper: RG <%s> is "
			    "ERROR_STOP_FAILED after Validate meths\n"),
			    rg_name);
			goto rg_update_end_clr_sw; //lint !e801
		}
	}

	//
	// If nodes have been added to the nodelist, the resources in this RG
	// need to run INIT on the new nodes.  The RG UPDATE intention runs
	// both the INIT and UPDATE methods as needed.
	//
	// The function run_init_method called here
	// just figures out which nodes should run INIT, but doesn't actually
	// run them yet.
	//
	if (changed.rg_nodelist && !rg_ccr->rg_unmanaged &&
	    !added_ns.isEmpty()) {
		run_init_method(rg_ptr, need_run_init);
	}


intention_routine:
	// Latch the intention on all slaves to update RG properties

	if (changed.rg_auto_start && rg_ccr->rg_auto_start) {
		//
		// Setting INTENT_OK_FLAG is to inform slaves that
		// the property auto_start_on_cluster is changed to TRUE
		// and in-memory ok_to_start flag needs to be updated.
		// If rg_auto_start is changed to FALSE, no need to request
		// slaves to update ok_to_start flag since the change
		// won't take effect until next cluster reboot.
		//
		flags |= rgm::INTENT_OK_FLAG;
	}

	ucmm_print("RGM",
	    NOGET("scrgadm_rg_update_prop_helper(): RG <%s> Latching\n"),
	    rg_name);

	//
	// Need to send a flag specifying that we're supposed
	// to run update methods on all nodes
	// except when only suspend resume prop is being updated,
	// or only rg_system prop is being updated.
	//
	if (!only_susp_resume_updated && !only_rg_system_updated) {
		flags |= rgm::INTENT_RUN_UPDATE;
	}

	//
	// Set up the vector of per-node flags
	//

	//
	// If we're supposed to run init methods, we must set
	// the INTENT_ACTION flag on nodes in added_ns.
	//

	if (need_run_init) {
		nodeflags.push_back(make_pair(&added_ns,
		    rgm::INTENT_ACTION));

		if (added_ns.display(ucmm_buffer) == 0) {
			ucmm_print("scrgadm_rg_update_prop_helper()",
			    NOGET("Specifying INIT on nodes %s\n"),
			    ucmm_buffer);
			free(ucmm_buffer);
		}
	}

	//
	// We also set the INTENT_RUN_FINI flag on nodes in
	// removed_ns.
	//
	nodeflags.push_back(make_pair(&removed_ns, rgm::INTENT_RUN_FINI));

	ucmm_buffer = NULL;
	if (removed_ns.display(ucmm_buffer) == 0) {
		ucmm_print("scrgadm_rg_update_prop_helper()",
		    NOGET("Specifying FINI on nodes %s\n"),
		    ucmm_buffer);
		free(ucmm_buffer);
	}

	//
	// If the nodelist is changed, then we need to update the in-memory
	// resource structure so as to retrieve the correct per-node values
	// for extension properties and onoff_switch/monitored_switch.
	//
	if (!added_ns.isEmpty() || !removed_ns.isEmpty())
		flags |= rgm::INTENT_READ_RS;
icrga_intention_routine:
	(void) latch_intention_pernodeflags(rgm::RGM_ENT_RG,
		    rgm::RGM_OP_UPDATE, rg_name, flags, nodeflags);

	//
	// If running Init, Fini, or Update, need to call cond_wait()
	//
	if ((!aff_set_flag && !aff_unset_flag) &&
	    !only_susp_resume_updated && !only_rg_system_updated)
		need_cond_wait = B_TRUE;

	ucmm_print("RGM",
	    NOGET("scrgadm_rg_update_prop_helper(): RG <%s> Calling CCR\n"),
	    rg_name);

	res = rgmcnfg_update_rgtable(rg_ccr, (const char*)ZONE,
	    boolean_t(aff_unset_flag));
	if (res.err_code != SCHA_ERR_NOERR)
		ucmm_print("RGM", NOGET("scrgadm_rg_update_prop_helper(): "
		    "RG <%s> CCR Failed\n"), rg_name);
	else {
		ucmm_print("RGM", NOGET("scrgadm_rg_update_prop_helper(): "
		    "RG <%s> Processing\n"), rg_name);

		(void) process_intention();

		// publish an event if any non-susp-resume prop updated.
		if (!only_susp_resume_updated) {
#if SOL_VERSION >= __s10
			(void) sc_publish_zc_event(ZONE,
			    ESC_CLUSTER_RG_CONFIG_CHANGE,
			    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
			    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			    CL_EVENT_CONFIG_PROP_CHANGED, NULL);
#else
			(void) sc_publish_event(
			    (char *)ESC_CLUSTER_RG_CONFIG_CHANGE,
			    (char *)CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
			    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			    CL_EVENT_CONFIG_PROP_CHANGED, NULL);
#endif
		}
		// susp-resume property change generates a special event.
		if (changed.rg_suspended) {
#if SOL_VERSION >= __s10
			(void) sc_publish_zc_event(ZONE,
			    ESC_CLUSTER_RG_CONFIG_CHANGE,
			    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
			    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			    rg_ccr->rg_suspended ? CL_EVENT_CONFIG_SUSPENDED :
			    CL_EVENT_CONFIG_RESUMED, NULL);
#else
			(void) sc_publish_event(
			    (char *)ESC_CLUSTER_RG_CONFIG_CHANGE,
			    (char *)CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
			    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
			    rg_ccr->rg_suspended ? CL_EVENT_CONFIG_SUSPENDED :
			    CL_EVENT_CONFIG_RESUMED, NULL);
#endif
		}

	}

	// Unlatch the intention on all slaves

	(void) unlatch_intention();

	if (aff_set_flag || aff_unset_flag) {
		if (rg_ptr->rgl_switching != SW_EVACUATING) {
			rg_ptr->rgl_switching = SW_NONE;
		}
		goto finished;
	}
	//
	// Wait for all slaves to finish running UPDATE, INIT, or FINI methods
	// on the resources in this RG.  In other words, wait until the RG is
	// out of ON_PENDING_METHODS or OFF_PENDING_METHODS on all the slaves.
	// We have to wait, because if we clear the rgl_switching flag while
	// methods are still being run, then this could permit another
	// update or switch (for example, disabling of a resource) to begin
	// while UPDATE, INIT, or FINI methods are still running, which could
	// cause state machine confusion and could crash the rgmd on the slave.
	//
	if (need_cond_wait) {
		// Flush the state change queue before calling cond_wait()
		rgm_unlock_state();
		flush_state_change_queue();
		rgm_lock_state();

		//
		// We don't need to retrieve the rg_ptr again (the old value
		// is still valid), because this RG could not have been
		// deleted, because it's either updating or busted.
		//

		//
		// Sleep on slavewait condition variable until all
		// current masters have run the update or init methods or died.
		// XXX 4215776 Implement FYI states in rgmd
		//
		// Also, take note of whether any base-cluster non-global zone
		// was removed from the nodelist.  Set 'ng_zone_removed'
		// accordingly.
		//
		for (LogicalNodeManager::iter it = lnManager->begin();
		    it != lnManager->end(); ++it) {
			bool lni_was_removed;
			LogicalNode *lnNode = lnManager->findObject(it->second);
			CL_PANIC(lnNode != NULL);

			nodeid = lnNode->getLni();
			lni_was_removed = removed_ns.containLni(nodeid);

			rgm::rgm_rg_state wait_state;

			if (lni_was_removed && !lnNode->isZcOrGlobalZone()) {
				ng_zone_removed = true;
			}

			if (added_ns.containLni(nodeid) || lni_was_removed) {
				//
				// The slave should be running INIT or FINI.
				// Wait for the RG to leave
				// RG_OFF_PENDING_METHODS
				//
				wait_state = rgm::RG_OFF_PENDING_METHODS;
			} else {
				//
				// The slave should be running UPDATE.
				// wait for the RG to leave
				// RG_ON_PENDING_METHODS
				//
				wait_state = rgm::RG_ON_PENDING_METHODS;
			}
			while (rg_ptr->rgl_xstate[nodeid].rgx_state ==
			    wait_state) {
				ucmm_print("scrgadm_rg_update_prop_helper()",
				    NOGET("update_resource wait slavewait"));
				cond_slavewait.wait(&Rgm_state->rgm_mutex);
				ucmm_print("scrgadm_rg_update_prop_helper()",
				    NOGET("after update_resource wait"
				    "slavewait"));
			}
		}
	}

	//
	// If any non-global zones were removed from the nodelist,
	// reclaim any unused LNIs.
	//
	if (ng_zone_removed) {
		lnManager->reclaimLnis();
	}

	// XXX 4215776 Implement FYI states in rgmd
	// XXX Return "qualified success" if appropriate.

rg_update_end_clr_sw:

	//
	// If the update was "busted", we must run rebalance() because
	// process_rg() did not run it in the RGM reconfiguration step.
	// However if we were busted by a node evacuation, we do not run
	// rebalance because the evacuating thread will run it such that
	// evacuating node(s) are excluded.
	// When suspend automatic resume property alone is updated,
	// we do not set/reset the rgl_switching flag.
	//
	if (!only_susp_resume_updated &&
	    rg_ptr->rgl_switching == SW_UPDATE_BUSTED) {
		ucmm_print("scrgadm_rg_update_prop_helper",
		    NOGET("RG <%s> is busted, running rebalance()"), rg_name);
		rebalance(rg_ptr, emptyNodeset, B_FALSE, B_TRUE);
	}

	if (rg_ptr->rgl_switching != SW_EVACUATING &&
	    !only_susp_resume_updated) {
		// Reset the rgl_switching field to indicate that the RG
		// update is complete.
		rg_ptr->rgl_switching = SW_NONE;
	}
	//
	// to ensure availability, we run rebalance as soon as the RG has
	// automatic recovery resumed
	//
	if (changed.rg_suspended && !rg_ccr->rg_suspended &&
	    !rg_ccr->rg_unmanaged) {
		ucmm_print("scrgadm_rg_update_prop_helper",
		    NOGET("RG <%s> resumed. running rebalance()"), rg_name);
		rebalance(rg_ptr, emptyNodeset, B_FALSE, B_TRUE, B_TRUE);
	}


finished:

	//
	// issue a broadcast to wake up failback thread (if any) waiting for
	// this operation.
	//
	cond_slavewait.broadcast();

	//
	// How to free the "proposed" RG struct?
	// Without being overly gross?
	// if (rg_ccr)
	//	rgm_free_rg(rg_ccr);	Can't do this
	// Because some of the pointer members could be a
	// copy of the existing RG struct (which could have
	// ALREADY gotten free()ed during the process_intention()
	// step). If the operation has actually failed due
	// to some validity checks, we are worse off because
	// now we will be free()ing stuff which is actually in
	// use by a RG.
	//


	if (rg_ccr) {

		// To free the allocated memory, we
		// rely on that fact that rgm_free_rg()
		// will not touch a NULL pointer. Thus
		// We set the pointer to NULL for
		// properties which have NOT been updated.
		// That way we only free() what was actually
		// allocated in this routine.

		if (changed.rg_nodelist == B_FALSE)
			rg_ccr->rg_nodelist = NULL;
		if (changed.rg_dependencies == B_FALSE)
			rg_ccr->rg_dependencies = NULL;
		if (changed.rg_affinities == B_FALSE)
			rg_ccr->rg_affinities = NULL;
		if (changed.rg_affinitents == B_FALSE)
			rg_ccr->rg_affinitents = NULL;
		if (changed.rg_description == B_FALSE)
			rg_ccr->rg_description = NULL;
		if (changed.rg_glb_rsrcused == B_FALSE)
			rg_ccr->rg_glb_rsrcused.names = NULL;
		if (changed.rg_pathprefix == B_FALSE)
			rg_ccr->rg_pathprefix = NULL;
		if (changed.rg_project_name == B_FALSE)
			rg_ccr->rg_project_name = NULL;
		// SC SLM addon start
		if (changed.rg_slm_type == B_FALSE)
			rg_ccr->rg_slm_type = NULL;
		if (changed.rg_slm_pset_type == B_FALSE)
			rg_ccr->rg_slm_pset_type = NULL;
		// SC SLM addon end
		if (changed.rg_affinities == B_FALSE)
			rg_ccr->rg_affinities = NULL;
		rg_ccr->rg_stamp = NULL;

		rgm_free_rg(rg_ccr);
	}

	free_rgm_affinities(&affinities);
	free_rgm_affinities(&affinitents);
	free(rg_name);
	free(locale);

	//
	// Even if there is no other error encountered in this subroutine,
	// the special case SCHA_ERR_RS_VALIDATE might have occurred
	// due to a node not being present in the cluster.  This
	// permits the update to succeed but generates a warning.
	//
	if (res.err_code == SCHA_ERR_NOERR) {
		if (validate_err_code != SCHA_ERR_NOERR) {
			res.err_code = validate_err_code;
			rgm_format_errmsg(&res, RWM_VALIDATE_NODES);
		} else if (changed.rg_project_name &&
		    !rg_ptr->rgl_ccr->rg_unmanaged) {
			rgm_format_errmsg(&res, RWM_PROJECT_CHANGED);
		}
		if (warning_res.err_msg) {
			rgm_sprintf(&res, "%s", warning_res.err_msg);
		}

	}
	free(warning_res.err_msg);

	retval->ret_code = res.err_code;

	/*
	 * Our final error message consists of the output from all VALIDATE
	 * methods, followed by any other error message.
	 */
	if (res.err_msg)
		rgm_sprintf(&validate_res, "%s", res.err_msg);

	/* new_str checks for NULL */
	retval->idlstr_err_msg = new_str(validate_res.err_msg);

	/* free the old strings; free checks for NULL */
	free(res.err_msg);
	free(validate_res.err_msg);

	rgup_res = retval;

	//
	// This function gets called in three scenarios.
	//
	// called while a RG property is updated during which
	// we need to get the lock.
	//
	// called from delete_source_affinity_info. Even
	// in this case we need to acquire the lock. The aff_rg_delete_flag
	// will be set in this case.
	//
	// called from idl_ext_rg_validate_prop, the lock would have been
	// already taken in idl_ext_rg_validate_prop
	// for if the source affinity rg property is to be updated.
	// The aff_set_flag or aff_unset_flag will be set during this.
	//
	if ((!aff_set_flag && !aff_unset_flag) || aff_rg_delete_flag)
		rgm_unlock_state();
}

//
// Remove an RG
//
// Preconditions:
// The RG must be empty of resources and in an endish state.
// No other RG can explicitly depend on this one.
//
// The RG need not be unmanaged, and it does not matter if it is
// online or offline on any nodes.
//
// The verbose_flag is used by the newcli for the verbose mode.
//
void
rgm_comm_impl::idl_scrgadm_rg_remove(
	const rgm::idl_rg_remove_args &rgrm_args,
	rgm::idl_regis_result_t_out rgrm_res, bool verbose_flag, Environment &e)
{
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};
	char			*rg_name = NULL;
	rglist_p_t		 rg_ptr, dep_rg;
	namelist_t		*rg_dep_list;
	LogicalNodeset		pending_boot_ns;
	LogicalNodeset		stop_failed_ns;
	LogicalNodeset		r_restart_ns;
	sc_syslog_msg_handle_t	handle;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	LogicalNodeset		emptyNodeset;


	rgm_lock_state();

	rg_name = strdup_nocheck(rgrm_args.idlstr_rg_name);
	if (rg_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		goto finished; //lint !e801
	}

	ucmm_print("RGM", NOGET("idl_scrgadm_rg_remove(): RG = <%s>\n"),
	    rg_name);

	// RG name must be a valid one
	rg_ptr = rgname_to_rg(rg_name);
	if (rg_ptr == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_INVALID_RG, rg_name);
		goto finished; //lint !e801
	}

	// RG should not have any resources
	if (rg_ptr->rgl_total_rs != 0) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RG_DEL_RS, rg_name);
		ucmm_print("RGM", NOGET("idl_scrgadm_rg_remove(): "
		    "RG = <%s> total_rs = <%d>\n"), rg_name,
		    rg_ptr->rgl_total_rs);
		goto finished; //lint !e801
	}

	//
	// Make sure RG is not busy.  Although it has no resources, another
	// thread might be adding a resource to it; the other thread would
	// temporarily release the mutex while running VALIDATE methods.
	//
	if (!in_endish_rg_state(rg_ptr, &pending_boot_ns, &stop_failed_ns,
	    NULL, &r_restart_ns)) {
		res.err_code = SCHA_ERR_RGRECONF;
		rgm_format_errmsg(&res, REM_RG_BUSY, rg_name);
		goto finished; //lint !e801
	}
	//
	// Since the RG is empty of resources, it should be impossible for
	// it to be ERROR_STOP_FAILED or PENDING_BOOT or ON_PENDING_R_RESTART
	// on any node, however,
	// we will check for those cases as a sanity check.
	//
	if (!pending_boot_ns.isEmpty() ||
	    !stop_failed_ns.isEmpty() ||
	    !r_restart_ns.isEmpty()) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RG_TAG, rg_ptr->rgl_ccr->rg_name);
		//
		// SCMSGS
		// @explanation
		// The operator is attempting to delete the indicated resource
		// group. Although the group is empty of resources, it was
		// found to be in an unexpected state. This will cause the
		// resource group deletion to fail.
		// @user_action
		// Use clresourcegroup offline to switch the resource group
		// offline on all nodes, then retry the deletion operation.
		// Since this problem might indicate an internal logic error
		// in the rgmd, save a copy of the /var/adm/messages files on
		// all nodes, and report the problem to your authorized Sun
		// service provider.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "INTERNAL ERROR: resource group <%s> is PENDING_BOOT or "
		    "ERROR_STOP_FAILED or ON_PENDING_R_RESTART, but contains "
		    "no resources", rg_ptr->rgl_ccr->rg_name);
		sc_syslog_msg_done(&handle);
		res.err_code = SCHA_ERR_INTERNAL;
		rgm_format_errmsg(&res, REM_INTERNAL);
		goto finished; //lint !e801
	}

	// Note, RG state (online/offline) doesn't matter-- it has no resources.

	//
	// No other RG should be depending upon this one; we don't want to
	// leave a dangling dependency in the CCR.
	// It doesn't matter if this RG depends upon some other RG;
	// when we delete this RG, the dependency will go away too.
	//
	dep_rg = Rgm_state->rgm_rglist;
	while (dep_rg) {
		// Skip myself
		if (strcmp(rg_name, dep_rg->rgl_ccr->rg_name) == 0) {
			dep_rg = dep_rg->rgl_next;
			continue;
		}
		rg_dep_list = dep_rg->rgl_ccr->rg_dependencies;
		while (rg_dep_list) {
			if (strcmp(rg_name, rg_dep_list->nl_name) == 0) {
				res.err_code = SCHA_ERR_DEPEND;
				rgm_format_errmsg(&res, REM_RG_DEL_DEP,
				    rg_name, dep_rg->rgl_ccr->rg_name);
				goto finished; //lint !e801
			}
			rg_dep_list = rg_dep_list->nl_next;
		}
		dep_rg = dep_rg->rgl_next;
	}

	//
	// "system" property checks:
	//
	//	Can't remove a "system" RG
	//
	if (rg_ptr->rgl_ccr->rg_system) {
		res.err_code = SCHA_ERR_ACCESS;
		rgm_format_errmsg(&res, REM_SYSTEM_RG, rg_name);
		goto finished; //lint !e801
	}

	//
	// Make sure that no other RG has declared an affinity
	// for this rg
	//
	if (rg_ptr->rgl_affinitents.ap_strong_pos != NULL ||
	    rg_ptr->rgl_affinitents.ap_weak_pos != NULL ||
	    rg_ptr->rgl_affinitents.ap_strong_neg != NULL ||
	    rg_ptr->rgl_affinitents.ap_weak_neg != NULL) {
		res.err_code = SCHA_ERR_DEPEND;
		rgm_format_errmsg(&res, REM_RG_DEL_AFF, rg_name);
		goto finished; //lint !e801
	}

	if (allow_inter_cluster()) {
#if SOL_VERSION >= __s10
		// handle inter cluster rg affinities.
		delete_inter_cluster_affinities(rg_ptr);
#endif
	}
	// Everything looks fine. Nuke this RG from CCR

	// Latch the intention on all slaves
	//
	// We cannot use the nset field for the set of nodes to run the
	// FINI method since the set of nodes could be different for
	// each resource in the RG. Therefore, the value is left as
	// EMPTY_NODESET.

	(void) latch_intention(rgm::RGM_ENT_RG, rgm::RGM_OP_DELETE,
	    rg_name, &emptyNodeset, 0);

	ucmm_print("RGM", NOGET("idl_scrgadm_rg_remove(): "
	    "RG <%s> Calling CCR\n"), rg_name);

	res = rgmcnfg_remove_rgtable(rg_name, ZONE);
	if (res.err_code != SCHA_ERR_NOERR)
		ucmm_print("RGM", NOGET("idl_scrgadm_rg_remove(): "
		    "RG <%s> CCR Failed\n"), rg_name);
	else {
		ucmm_print("RGM", NOGET("idl_scrgadm_rg_remove(): "
		    "RG <%s> Processing\n"), rg_name);

		(void) process_intention();

		// publish an event
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE, ESC_CLUSTER_RG_CONFIG_CHANGE,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
		    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
		    CL_EVENT_CONFIG_REMOVED, NULL);
#else
		(void) sc_publish_event((char *)ESC_CLUSTER_RG_CONFIG_CHANGE,
		    (char *)CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rg_name,
		    CL_CONFIG_ACTION, SE_DATA_TYPE_UINT32,
		    CL_EVENT_CONFIG_REMOVED, NULL);
#endif

		// Print the success message.
		if (verbose_flag)
			rgm_format_successmsg(&res, RWM_RG_DELETED,
			    rg_name);
	}

	// Unlatch the intention on all slaves
	(void) unlatch_intention();

	// reclaim LNIs that might have been freed by RG removal
	lnManager->reclaimLnis();

finished:

	free(rg_name);

	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);

	free(res.err_msg);

	rgrm_res = retval;
	rgm_unlock_state();

}


//
// if global zone, return success. else
// if nodelist change, return failure, else
// return success if nodelist contains request origin
//
static scha_errmsg_t
check_ngzone_rg_updatable_props(const rglist_p_t rg_ptr,
    const rgm::idl_rg_update_prop_args_v1 &rgup_args) {

	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	boolean_t globalflag, nodelist_contains;

	ngzone_check(rgup_args.idlstr_zonename, globalflag);
	if (globalflag)	// globalzone is allowed all action
		return (res);

	for (uint_t i = 0; i < rgup_args.prop_val.length(); i++) {
		if (strcasecmp(rgup_args.prop_val[i].idlstr_nv_name,
		    SCHA_NODELIST) == 0) {
			res.err_code = SCHA_ERR_ACCESS;
			rgm_format_errmsg(&res, REM_PROP_NGZONE,
			    SCHA_NODELIST);
			return (res); // ng zones can't modify nodelist
		}
	}
	nodelist_check(rg_ptr, rgup_args.nodeid, rgup_args.idlstr_zonename,
	    nodelist_contains);

	if (!nodelist_contains) {// non-nodelist zones cant modify rg
		res.err_code = SCHA_ERR_ACCESS;
		rgm_format_errmsg(&res, REM_RG_NODELIST_NGZONE,
		    rg_ptr->rgl_ccr->rg_name,
		    (const char *)rgup_args.idlstr_zonename);
		return (res);
	}

	return (res);
}

//
// check_rg_updated_prop() - check to see if the user-specified prop can be
//		 updated or it is a valid RG property name
//
// According to the RGM architecture, no update is permitted on certain
// RG properties -- see rg_updatable_props, above.
//
// If the property is not allowed to update, return SCHA_ERR_UPDATE.
// It it is not a RGM defined the RG property, return
// SCHA_ERR_PROP.  otherwise, return SCHA_ERR_NOERR..
//
scha_errmsg_t
check_rg_updatable_props(const rgm::idl_nameval_seq_t &props,
    rgm::caller_flag_t caller_flag)
{

	uint_t		i, n, len;
	const char		*prop_name;
	updatable_prop_t	*rg_tbl = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_err_t err_code;

	len = props.length();
	//
	// Loop through the list of user-specified properties.
	// If the property in the list is non-updatable,
	// return with 'SCHA_ERR_UPDATE' error immediately.
	//
	for (i = 0; i < len; i++) {
		err_code = SCHA_ERR_PROP;

		prop_name = props[i].idlstr_nv_name;
		for (n = 0; n < RG_UPDATABLE_ENTRIES; n++) {
			rg_tbl = &rg_updatable_props[n];
			if (strcasecmp(prop_name, rg_tbl->prop_name) == 0) {
				if (rg_tbl->updatable) {
					err_code = SCHA_ERR_NOERR;
				} else {
					err_code = SCHA_ERR_UPDATE;
				}

				break;
			}
		}		 // for loop

		if (strcasecmp(prop_name, SCHA_RG_SUSP_AUTO_RECOVERY) == 0) {
			// override SCHA_ERR_NOERR and UPDATE
			if (caller_flag == rgm::RGM_FROM_SCRGADM) {
				// property updated only from scswitch
				err_code = SCHA_ERR_UPDATE;
			} else {
				// prop in list, with updateable = true
				ASSERT(err_code == SCHA_ERR_NOERR);
			}

		}
		if (err_code == SCHA_ERR_NOERR) {
			continue;
		}
		res.err_code = err_code;
		if (err_code == SCHA_ERR_PROP) {
			ucmm_print("RGM", NOGET("check_rg_updatable_props(): "
			    "invalid RG property name <%s>\n"), prop_name);
			rgm_format_errmsg(&res, REM_PROP_NOT_FOUND, prop_name);

		} else if (err_code == SCHA_ERR_UPDATE) {
			ucmm_print("RGM", NOGET("check_rg_updatable_props(): "
			    "property <%s> is not permitted "
			    "to be updated\n"), prop_name);
			rgm_format_errmsg(&res, REM_PROP_CANT_UPDATE,
			    prop_name);
		}
		break;

	} // for loop
	return (res);

}

//
// only_prop_updated, is the function that will be called to check
// that suspend/resume, or rg_system, is the one and only property that is
// being updated.
// If suspend/resume is the only property that is being updated, then the
// in_endish check can be skipped and the property can be updated even if
// the RG is busy.
// If rg_system is the only property that is being updated, then we skip
// execution of Validate and Update methods on resources in the RG.
//
scha_errmsg_t
only_prop_updated(const rgm::idl_nameval_seq_t &props,
    boolean_t &only_susp_resume_updated, boolean_t &only_rg_system_updated)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	char		*prop_name = NULL;

	only_susp_resume_updated = B_FALSE;
	only_rg_system_updated = B_FALSE;

	//
	// if more than one property is being updated then return false
	//
	if (props.length() > 1) {
		goto finished; //lint !e801
	}
	//
	// Set Boolean out-parameter based on which property is being updated
	//
	if ((prop_name = strdup_nocheck(props[0].idlstr_nv_name)) == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		goto finished; //lint !e801
	}
	if (strcasecmp(prop_name, SCHA_RG_SUSP_AUTO_RECOVERY) == 0) {
		only_susp_resume_updated = B_TRUE;
	} else if (strcasecmp(prop_name, SCHA_RG_SYSTEM) == 0) {
		only_rg_system_updated = B_TRUE;
	}

finished:
	if (prop_name != NULL)
		free(prop_name);
	return (res);
}


//
// rg_affinities_validate() -
//
// Perform the following checks on the given affinities
// - no resource group occurs more than once in the affinities lists
// - the given rg cannot appear in its own affinities list.
// - post a warning message if the given rg declares strong affinities for
//   more than one rgs.
// - Check that the rg doesn't declare a strong positive affinity with
//   failover delegation for more than one resource group
// - When RG_affinities are edited, all of rg's strong affinities
//   (positive and negative) must be satisfied by the current online/offline
//   state of rg and the RGs for which it declares affinity.
// - Calls rg_affinities_check_nodelist to do the affinities/nodelist
//   interaction checks.
//
// Note: no need to check if the rg name in the affinities list is a valid rg
// since it has been checked in convert_affinities_list() previously.
//
// The warning checks are performed only if the relevent property has
// actually been changed (affinities, desired primaries, or nodelist).
//
static scha_errmsg_t
rg_affinities_validate(rgm_rg_t *rgp, rgm_affinities_t *affinities,
    boolean_t rg_create, boolean_t affs_changed, boolean_t des_prims_changed,
    boolean_t nodelist_changed, boolean_t auto_start_changed,
    const rgm::idl_string_seq_t &rg_list)
{
	namelist_t	*affinities_list = NULL;
	namelist_t	*affp;
	const char	*rg_dup_name;
	int		count = 0;
	rglist_p_t	my_rgp, aff_rgp;
	sol::nodeid_t	nid;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	res2 = {SCHA_ERR_NOERR, NULL};

	// Note: a strong positive affinity with failover delegation (+++),
	// if any, is part of list affinities->ap_failover_delegation AND the
	// list affinities->ap_strong_pos. Therefore, most of the validation
	// is done on ap_strong_pos, and ap_failover_delegation is mostly
	// ignored, except to make sure there is only one delegate in the
	// list.

	// First check rg duplication in RG_affinities property.

	affinities_list = namelist_copy(affinities->ap_strong_pos);
	namelist_append(affinities->ap_weak_pos, &affinities_list);
	namelist_append(affinities->ap_strong_neg, &affinities_list);
	namelist_append(affinities->ap_weak_neg, &affinities_list);

	if (affinities_list == NULL) {
		// no affinity; skip checking affinity
		// for rg update case, we continue to check the rgs that have
		// affinities for this rg
		if (rg_create) {
			return (res);
		} else {
			my_rgp = rgname_to_rg(rgp->rg_name);
			goto rg_update_check_affinitents; //lint !e801
		}
	}

	rg_dup_name = get_dup_name(affinities_list);

	// If we have a duplicate name, it's an error
	if (rg_dup_name != NULL) {
		ucmm_print("RGM", NOGET("rg_affinities_validate(): "
		    "Duplicated rg name <%s> in <%s>\n"),
		    rg_dup_name, SCHA_RG_AFFINITIES);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_DUP_RG_DEP, rgp->rg_name,
		    rg_dup_name, SCHA_RG_AFFINITIES);
		goto finished; //lint !e801
	}

	// Check if the given rg appear in its own affinities list
	if (namelist_exists(affinities_list, rgp->rg_name)) {
		ucmm_print("RGM", NOGET("rg_affinities_validate(): "
		    "the specified rg name in <%s>\n"), SCHA_RG_AFFINITIES);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RG_AFF_SELF, rgp->rg_name,
		    rg_dup_name, SCHA_RG_AFFINITIES);
		goto finished; //lint !e801
	}

	//
	// Check that the list of RG to delegate failover requests to
	// has only one member.
	//
	count = 0;
	for (affp = affinities->ap_failover_delegation; affp;
	    affp = affp->nl_next) {
		count++;
	}
	if (count > 1) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RG_AFF_DELEGATE_MANY,
		    rgp->rg_name);
		goto finished; //lint !e801
	}

	//
	// We only do the following check if the affinities have changed,
	// because we only want to issue the warning message once.
	//
	count = 0;
	if (affs_changed) {
		// Check if rg has strong affinities for two or more other RGs
		for (affp = affinities->ap_strong_pos; affp;
		    affp = affp->nl_next) {
			count++;
		}
		for (affp = affinities->ap_strong_neg; affp;
		    affp = affp->nl_next) {
			count++;
		}
		if (count > 1) {
			rgm_format_errmsg(&res, RWM_AFF_STRONG_MULTI_RG,
			    rgp->rg_name);
			ucmm_print("RGM", NOGET("rg_affinities_validate: RG "
			    "<%s> has strong affinities for more than one "
			    "rgs\n"), rgp->rg_name);
		}
	}

	//
	// If the given RG has a strong positive affinity for another
	// RG that has Desired_primaries less than the Desired_primaries
	// of the given RG, issue a warning message.
	//
	// If this RG has Auto_start_on_new_cluster = TRUE, then all
	// the RGs for which this RG has strong positive affinities must
	// also have Auto_start_on_new_cluster = TRUE.
	//
	// If Desired_primaries or Auto_start_on_new_cluster changed, exclude
	// checking other RGs that are being updated at the same time, because
	// they will be getting the same property update.
	//
	for (affp = affinities->ap_strong_pos; affp; affp = affp->nl_next) {

		if (is_enhanced_naming_format_used(affp->nl_name))
			continue;

		bool affp_updated = is_rg_in_update_list(affp->nl_name,
		    rg_list);
		aff_rgp = rgname_to_rg(affp->nl_name);
		CL_PANIC(aff_rgp != NULL);

		//
		// Only do the following check if affinities or
		// desired primaries have changed, because we want to
		// issue the warning only once.
		//
		// Skip the check if desired primaries is changed
		// on both this RG and on the affinity RG.
		//
		if ((affs_changed && !des_prims_changed) ||
		    (des_prims_changed && !affp_updated)) {
			if (aff_rgp->rgl_ccr->rg_desired_primaries <
			    rgp->rg_desired_primaries) {
				rgm_format_errmsg(&res, RWM_AFF_LESS_DESIRED_P,
				    rgp->rg_name, aff_rgp->rgl_ccr->rg_name,
				    rgp->rg_name);
			}
		}

		//
		// Only do the following check if affinities or
		// Auto_start_on_new_cluster has changed, because we want to
		// issue the warning only once.
		//
		// Skip the check if auto_start is changed
		// on both this RG and the affinity RG.
		//
		if ((affs_changed && !auto_start_changed) ||
		    (auto_start_changed && !affp_updated)) {
			if (rgp->rg_auto_start &&
			    !aff_rgp->rgl_ccr->rg_auto_start) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res, REM_RG_AFF_AUTO_START,
				    rgp->rg_name, aff_rgp->rgl_ccr->rg_name);
				goto finished; //lint !e801
			}
		}
	}

	//
	// Run the nodelist/affinities interaction checks only if
	// we have changed the affinities, desired_primaries, or nodelist.
	//
	if (affs_changed || des_prims_changed || nodelist_changed) {
		res2 = rg_affinities_check_nodelist(rgp, affinities);
		if (res2.err_msg != NULL) {
			rgm_sprintf(&res, "%s", res2.err_msg);
			free(res2.err_msg);
		}
		if (res2.err_code != SCHA_ERR_NOERR) {
			res.err_code = res2.err_code;
			goto finished; //lint !e801
		}
	}

	if (rg_create)
		goto finished; //lint !e801

	//
	// The following checks are only performed when editing RG_affinities
	//
	// Check if all of rg's affinities are satisfied by
	// the current online/offline state of rg.
	//

	//
	// Need to get the rgp pointer before jumping to the
	// affinitents checks.
	//
	my_rgp = rgname_to_rg(rgp->rg_name);

	if (!affs_changed) {
		goto rg_update_check_affinitents; //lint !e801
	}

	for (LogicalNodeManager::iter it = lnManager->begin();
	    it != lnManager->end(); ++it) {
		LogicalNode *lnNode = lnManager->findObject(it->second);
		CL_PANIC(lnNode != NULL);
		if (!lnNode->isInCluster())
			continue;

		rgm::lni_t nid = lnNode->getLni();

		//
		// For each strong positive affinity rg, make sure it is
		// online on the same node where the specified rg is online.
		//
		// If physical node affinities apply, then the affinity RG
		// may be online on any zone on the same physical node as nid.
		//
		for (affp = affinities->ap_strong_pos; affp;
		    affp = affp->nl_next) {
			if (is_enhanced_naming_format_used(affp->nl_name))
				continue;
			aff_rgp = rgname_to_rg(affp->nl_name);
			CL_PANIC(aff_rgp != NULL);
			if (is_rg_online_not_errored(my_rgp, nid, B_FALSE,
			    B_TRUE, NULL) &&
			    !is_rg_online_not_errored(aff_rgp, nid, B_FALSE,
			    use_logicalnode_affinities(), NULL)) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    REM_RG_AFF_STATE, rgp->rg_name,
				    "positive", affp->nl_name);
				goto finished; //lint !e801
			}
		}

		//
		// For each strong negative affinity rg, make sure it is NOT
		// online on the same node where the specified rg is online.
		//
		for (affp = affinities->ap_strong_neg; affp;
		    affp = affp->nl_next) {
			if (is_enhanced_naming_format_used(affp->nl_name))
				continue;
			aff_rgp = rgname_to_rg(affp->nl_name);
			CL_PANIC(aff_rgp != NULL);
			if (is_rg_online_not_errored(my_rgp, nid, B_FALSE,
			    B_TRUE, NULL) &&
			    is_rg_online_not_errored(aff_rgp, nid, B_FALSE,
			    use_logicalnode_affinities(), NULL)) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    REM_RG_AFF_STATE, rgp->rg_name,
				    "negative", affp->nl_name);
				goto finished; //lint !e801
			}
		}

		//
		// For weak affinities.
		// If the current online state of rg1 and other RGs violates
		// any weak affinities (positive or negative) that are declared
		// for rg1, the edit will succeed with a warning msg.
		//
		for (affp = affinities->ap_weak_pos; affp;
		    affp = affp->nl_next) {
			if (is_enhanced_naming_format_used(affp->nl_name))
				continue;
			aff_rgp = rgname_to_rg(affp->nl_name);
			CL_PANIC(aff_rgp != NULL);
			if (is_rg_online_not_errored(my_rgp, nid, B_FALSE,
			    B_TRUE, NULL) &&
			    !is_rg_online_not_errored(aff_rgp, nid, B_FALSE,
			    use_logicalnode_affinities(), NULL)) {
				rgm_format_errmsg(&res,
				    RWM_RG_AFF_STATE, rgp->rg_name,
				    "positive", affp->nl_name);
			}
		}

		for (affp = affinities->ap_weak_neg; affp;
		    affp = affp->nl_next) {
			if (is_enhanced_naming_format_used(affp->nl_name))
				continue;
			aff_rgp = rgname_to_rg(affp->nl_name);
			CL_PANIC(aff_rgp != NULL);
			if (is_rg_online_not_errored(my_rgp, nid, B_FALSE,
			    B_TRUE, NULL) &&
			    is_rg_online_not_errored(aff_rgp, nid, B_FALSE,
			    use_logicalnode_affinities(), NULL)) {
				rgm_format_errmsg(&res,
				    RWM_RG_AFF_STATE, rgp->rg_name,
				    "negative", affp->nl_name);
			}
		}
	}

rg_update_check_affinitents:

	// If there is an RG which has a strong positive affinity for
	// the given RG and its Desired_primaries is less than the
	// Desired_primaries of the given RG, issue a warning message.
	//
	// If this RG has Auto_start_on_new_cluster = FALSE, then all
	// the RGs that have strong affinities for the specified RG
	// must also have Auto_start_on_new_cluster = FALSE.
	//
	// We also need to run the nodelist checks on all rgs that have
	// strong positive affinities for the given RG.
	//
	// Exclude checking other RGs that are being updated at the same time,
	// because they will be getting the same desired_primaries or
	// auto_start property update. Note that it is OK to skip the nodelist
	// check in this case, because the same check will be called
	// redundantly on the affinitent.
	//
	for (affp = my_rgp->rgl_affinitents.ap_strong_pos;
	    affp; affp = affp->nl_next) {

		if (is_enhanced_naming_format_used(affp->nl_name))
			continue;

		if (is_rg_in_update_list(affp->nl_name, rg_list)) {
			continue;
		}
		aff_rgp = rgname_to_rg(affp->nl_name);
		CL_PANIC(aff_rgp != NULL);

		//
		// We only do the following check if we are changing
		// desired_primaries.
		//
		if (des_prims_changed) {
			if (rgp->rg_desired_primaries <
			    aff_rgp->rgl_ccr->rg_desired_primaries) {
				rgm_format_errmsg(&res, RWM_AFF_LESS_DESIRED_P,
				    aff_rgp->rgl_ccr->rg_name, rgp->rg_name,
				    aff_rgp->rgl_ccr->rg_name);
			}
		}

		//
		// We only do the following check if we are changing
		// Auto_start_on_new_cluster.
		//
		if (auto_start_changed && !rgp->rg_auto_start &&
		    aff_rgp->rgl_ccr->rg_auto_start) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RG_AFF_AUTO_START,
			    aff_rgp->rgl_ccr->rg_name, rgp->rg_name);
			goto finished; //lint !e801
		}

		//
		// Do the nodelist/affinities interaction checks only if
		// we have changed the nodelist or desired_primaries.
		//
		// We can't call rg_affinities_check_nodelist, because
		// my_rgp isn't stored in memory yet.
		//
		if (nodelist_changed || des_prims_changed) {
			res2 = check_sp_affinities_nodelist(aff_rgp->rgl_ccr,
			    rgp);
			if (res2.err_msg != NULL) {
				rgm_sprintf(&res, "%s", res2.err_msg);
				free(res2.err_msg);
			}
			if (res2.err_code != SCHA_ERR_NOERR) {
				res.err_code = res2.err_code;
				goto finished; //lint !e801
			}
		}
	}

	//
	// If there is an RG which has a weak positive affinity for
	// the given RG, we need to run the nodelist checks.
	//
	// However, if the affinitent is being updated at the same time, skip
	// this check, because it is redundant with the same check that will
	// be made on the affinitent.
	//
	for (affp = my_rgp->rgl_affinitents.ap_weak_pos;
	    affp; affp = affp->nl_next) {


		if (is_enhanced_naming_format_used(affp->nl_name))
			continue;

		if (is_rg_in_update_list(affp->nl_name, rg_list)) {
			continue;
		}
		aff_rgp = rgname_to_rg(affp->nl_name);
		CL_PANIC(aff_rgp != NULL);

		//
		// Do the nodelist/affinities interaction checks only
		// if we have changed the nodelist.
		//
		// We can't call rg_affinities_check_nodelist, because
		// my_rgp isn't stored in memory yet.
		//
		if (nodelist_changed) {
			res2 = check_wp_affinities_nodelist(aff_rgp->rgl_ccr,
			    rgp);
			if (res2.err_msg != NULL) {
				rgm_sprintf(&res, "%s", res2.err_msg);
				free(res2.err_msg);
			}
			if (res2.err_code != SCHA_ERR_NOERR) {
				res.err_code = res2.err_code;
				goto finished; //lint !e801
			}
		}
	}


finished:
	if (affinities_list)
		rgm_free_nlist(affinities_list);

	return (res);
}

#if (SOL_VERSION >= __s10)
//
// Helper function to check the original affinity list and the
// update affinity list before calling the validate idl call.
//
static scha_errmsg_t
process_intercluster_rg_lists(namelist_t **orig_list,
    namelist_t **new_list, rgm_rg_t *rg,  boolean_t rg_create,
    boolean_t affs_changed, boolean_t des_prims_changed,
    boolean_t nodelist_changed, rgm::rg_aff_flag_t aff_flag)
{

	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	ucmm_print("process_intercluster_rg_lists()",
	    NOGET("process list for RG <%s>\n"), rg->rg_name);

	// Chek if both lists are Non-Null
	if ((*orig_list != NULL) && (*new_list != NULL)) {
		// Copy the original list.
		namelist_t *temp_list = namelist_copy(*orig_list);
		for (; temp_list != NULL; temp_list = (temp_list)->nl_next) {
			if (namelist_contains(*new_list, temp_list->nl_name)) {
				namelist_delete_name(new_list,
				    temp_list->nl_name);
				namelist_delete_name(orig_list,
				    temp_list->nl_name);
			}
		}

		// Both list are the same. Return.
		if ((*orig_list == NULL) && (*new_list == NULL)) {
			ucmm_print("process_intercluster_rg_lists()",
			    NOGET("Lists identical for RG <%s>\n"),
			    rg->rg_name);
			rgm_free_nlist(temp_list);
			return (res);
		}

		// If the newlist list has rgs then these should be
		// SET in the source affinity list of the target affinity RG.
		if (*new_list != NULL) {
			res = rg_ext_affinities_validate_helper(rg, *new_list,
			    rg_create, affs_changed, des_prims_changed,
			    nodelist_changed, aff_flag, B_TRUE);
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(temp_list);
				return (res);
			}
		}


		// If the original list has rgs then these should be
		// UNSET from the source affinity list of the target affinity
		// RG.
		if (*orig_list != NULL) {
			res = rg_ext_affinities_validate_helper(rg, *orig_list,
			    rg_create, affs_changed, des_prims_changed,
			    nodelist_changed, aff_flag, B_FALSE);
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(temp_list);
				return (res);
			}
		}

		// If the original list was Null then the RGs in new_list
		// need to be set in the target affinity RG.
	} else if ((*orig_list == NULL) && (*new_list != NULL)) {
			// Call with set
			res = rg_ext_affinities_validate_helper(rg, *new_list,
			    rg_create, affs_changed, des_prims_changed,
			    nodelist_changed, aff_flag, B_TRUE);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}
	} else {
			// If the new list was Null then the RGs in
			// original list needs to be UNSET in the
			// target affinity RG.
			res = rg_ext_affinities_validate_helper(rg, *orig_list,
			    rg_create, affs_changed, des_prims_changed,
			    nodelist_changed, aff_flag, B_FALSE);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}
	}

	ucmm_print("process_intercluster_rg_lists()",
	    NOGET("process list complete for RG <%s>\n"),
	    rg->rg_name);
	return (res);
}

//
// rg_ext_affinities_validate() -
//
// The same checks which are done for locla cluster affinities
// need to be done for the inter cluster rg affinities.
// For validating the affinities we need to ''push" the relavant
// information needed for validation to the remote cluster rgmd
// presidnet and do the validation on the remote cluster as well.
// This function takes care of the the case when affinity is set
// during resource creation as well as when the affinities
// are updated.
//
static scha_errmsg_t
rg_ext_affinities_validate(rgm_rg_t *rgp, rgm_affinities_t *affinities,
    boolean_t rg_create, boolean_t affs_changed, boolean_t des_prims_changed,
    boolean_t nodelist_changed)
{
	namelist_t	*affinities_list = NULL;
	namelist_t	*affp;
	const char	*rg_dup_name;
	int		count = 0;
	rglist_p_t	my_rgp, aff_rgp;
	sol::nodeid_t	nid;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	res2 = {SCHA_ERR_NOERR, NULL};
	rglist_t *rg;
	rgm_rg_t *rg_ccr;    // the CCR portion of the RG
	namelist_t *saved_affinities = NULL;
	namelist_t *saved_affinitents = NULL;
	boolean_t ext_aff;
	namelist_t *orig_list = NULL;
	namelist_t *new_list = NULL;


	affinities_list = namelist_copy(affinities->ap_strong_pos);
	namelist_append(affinities->ap_weak_pos, &affinities_list);
	namelist_append(affinities->ap_strong_neg, &affinities_list);
	namelist_append(affinities->ap_weak_neg, &affinities_list);

	if (affinities_list == NULL) {
		// no affinity; skip checking affinity
		// for rg update case, we continue to check the rgs that have
		// affinities for this rg
		if (rg_create) {
			return (res);
		} else {
			my_rgp = rgname_to_rg(rgp->rg_name);
		}
	}


	// this is an update scenario
	if (!rg_create)  {
		if ((rg = rgname_to_rg(rgp->rg_name)) == NULL) {
			res.err_code = SCHA_ERR_RG;
			return (res);
		}

		//
		// Save the affinities for later reference
		//
		saved_affinities = namelist_copy(rgp->rg_affinities);

		res = rgmcnfg_read_rgtable(rgp->rg_name, &rg_ccr, ZONE);
		if (res.err_code != SCHA_ERR_NOERR) {
			ucmm_print("RGM",
			    NOGET("rgmcnfg_read_rgtable() failed with <%d>\n"),
			    res.err_code);
			rgm_free_nlist(saved_affinities);
			return (res);
		}

		if (!namelist_compare(saved_affinities,
		    rg_ccr->rg_affinities)) {
			// convert the affinities to the in-memory form
			res = convert_affinities_list(rg_ccr->rg_name,
			    rg_ccr->rg_affinities, &(rg->rgl_affinities));
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			// Process strong positive affinities.

			res = get_intercluster_rg_aff_lists(
			    *(affinities), NULL, &new_list, NULL, NULL, NULL);

			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			res = get_intercluster_rg_aff_lists(
			    rg->rgl_affinities, NULL, &orig_list, NULL, NULL,
			    NULL);
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			res = process_intercluster_rg_lists(
			    &orig_list, &new_list, rgp, rg_create, affs_changed,
			    des_prims_changed, nodelist_changed,
			    rgm::STRONG_POS);
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			rgm_free_nlist(orig_list);
			rgm_free_nlist(new_list);
			orig_list = NULL;
			new_list = NULL;

			// Process weak positive affinities.
			res = get_intercluster_rg_aff_lists(
			    *(affinities), NULL, NULL, &new_list, NULL, NULL);

			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			res = get_intercluster_rg_aff_lists(
			    rg->rgl_affinities, NULL, NULL, &orig_list, NULL,
			    NULL);
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			res = process_intercluster_rg_lists(
			    &orig_list, &new_list, rgp, rg_create, affs_changed,
			    des_prims_changed, nodelist_changed, rgm::WEAK_POS);
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			rgm_free_nlist(orig_list);
			rgm_free_nlist(new_list);
			orig_list = NULL;
			new_list = NULL;

			// Process Strong negative affinities.
			res = get_intercluster_rg_aff_lists(
			    *(affinities), NULL, NULL, NULL, &new_list, NULL);

			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			res = get_intercluster_rg_aff_lists(
			    rg->rgl_affinities, NULL, NULL, NULL, &orig_list,
			    NULL);
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			res = process_intercluster_rg_lists(
			    &orig_list, &new_list, rgp, rg_create, affs_changed,
			    des_prims_changed, nodelist_changed,
			    rgm::STRONG_NEG);
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			rgm_free_nlist(orig_list);
			rgm_free_nlist(new_list);
			orig_list = NULL;
			new_list = NULL;

			// Process Weak negetive affinities
			res = get_intercluster_rg_aff_lists(
			    *(affinities), NULL, NULL, NULL, NULL, &new_list);

			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			res = get_intercluster_rg_aff_lists(
			    rg->rgl_affinities, NULL, NULL, NULL, NULL,
			    &orig_list);
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			// update the links
			res = process_intercluster_rg_lists(
			    &orig_list, &new_list, rgp, rg_create, affs_changed,
			    des_prims_changed, nodelist_changed, rgm::WEAK_NEG);
			if (res.err_code != SCHA_ERR_NOERR) {
				rgm_free_nlist(saved_affinities);
				rgm_free_rg(rg_ccr);
				return (res);
			}

			rgm_free_nlist(orig_list);
			rgm_free_nlist(new_list);
			orig_list = NULL;
			new_list = NULL;
		}
	}


	// This is the case of rg creation. Validate rg_affinities property
	res = rg_ext_affinities_validate_helper(rgp, affinities->ap_strong_pos,
	    rg_create, affs_changed, des_prims_changed, nodelist_changed,
	    rgm::STRONG_POS, B_TRUE);
	if (res.err_code != SCHA_ERR_NOERR) {
		rgm_free_nlist(saved_affinities);
		rgm_free_rg(rg_ccr);
		return (res);
	}


	res = rg_ext_affinities_validate_helper(rgp, affinities->ap_weak_pos,
	    rg_create, affs_changed, des_prims_changed, nodelist_changed,
	    rgm::WEAK_POS, B_TRUE);
	if (res.err_code != SCHA_ERR_NOERR) {
		rgm_free_nlist(saved_affinities);
		rgm_free_rg(rg_ccr);
		return (res);
	}


	res = rg_ext_affinities_validate_helper(rgp, affinities->ap_strong_neg,
	    rg_create, affs_changed, des_prims_changed, nodelist_changed,
	    rgm::STRONG_NEG, B_TRUE);
	if (res.err_code != SCHA_ERR_NOERR) {
		rgm_free_nlist(saved_affinities);
		rgm_free_rg(rg_ccr);
		return (res);
	}

	res = rg_ext_affinities_validate_helper(rgp, affinities->ap_weak_neg,
	    rg_create, affs_changed, des_prims_changed, nodelist_changed,
	    rgm::WEAK_NEG, B_TRUE);
	if (res.err_code != SCHA_ERR_NOERR) {
		rgm_free_nlist(saved_affinities);
		rgm_free_rg(rg_ccr);
		return (res);
	}
	return (res);
}

//
// This is a helper function called during the rg affinities validation.
// this function makes the idl clal to the remote cluster for the
// validation of the rg properties during the rg affinity configuration.
//

static scha_errmsg_t
rg_ext_affinities_validate_helper(rgm_rg_t *rgp, namelist_t *aff,
    boolean_t rg_create, boolean_t affs_changed, boolean_t des_prims_changed,
    boolean_t nodelist_changed, rgm::rg_aff_flag_t aff_flag,
    boolean_t rg_set_aff) {

	namelist_t	*affinities_list = NULL;
	namelist_t	*affp;
	const char	*rg_dup_name;
	int		count = 0;
	rglist_p_t	my_rgp, aff_rgp;
	sol::nodeid_t	nid;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	res2 = {SCHA_ERR_NOERR, NULL};

	// rg update case.
	if (!rg_create)
		my_rgp = rgname_to_rg(rgp->rg_name);

	for (affp = aff; affp; affp = affp->nl_next) {
		if (!is_enhanced_naming_format_used(affp->nl_name))
			continue;
		uint32_t clid;
		int err;
		// Check if its local cluster resource but specified
		// as cluster:resource by the user.
		// If so do not process.
		if (is_this_local_cluster_r_or_rg(affp->nl_name, B_TRUE)) {
			res.err_code = SCHA_ERR_RG;
			rgm_format_errmsg(&res,
			    REM_CREATE_IC_AFF_LOCAL_CLUSTER,
			    affp->nl_name, rgp->rg_name);
			return (res);
		} else  {
			// remote rg.
			char *rc = NULL, *rr = NULL;
			res = get_remote_cluster_and_rorrg_name(affp->nl_name,
			    &rc, &rr);
			if ((res.err_code == SCHA_ERR_NOERR) && (rc != NULL) &&
			    (rr != NULL)) {
				err = clconf_get_cluster_id(rc, &clid);
				if (((err == 0) && (clid != 0) &&
				    (clid < MIN_CLUSTER_ID)) || (
				    (err != 0) && (err != EACCES))) {
					// Inside non-clusterized zone
					res.err_code = SCHA_ERR_CLUSTER;
					rgm_format_errmsg(&res,
					    REM_RGM_IC_UNREACH, rc);
					free(rr);
					free(rc);
					return (res);
				} else if (err == EACCES) {
					ASSERT(0); // Cannot happen
				}
				// Set the msg locale
				// Idl flags for affs_changed,
				// des_prims_changed and
				// nodelist_changed. Create idl
				// structure to store
				// the desired primaries, nodelist
				// sequence, rg_auto_start flag as well
				// as online node id sequence.

				// check the validity with the remote
				// rg. if error goto finished.

				rgm::idl_rg_validate_prop_args arg;
				rgm::idl_regis_result_t_var result_v;
				rgm::rgm_comm_var prgm_pres_v;
				Environment e;
				rgm::idl_nameval_seq_t	prop_val;

				arg.flags = 0;
				if (affs_changed)
					arg.flags |=
					    rgm::AFFINITY_CHANGED;
				if (des_prims_changed)
					arg.flags |=
					rgm::DESIRED_PRIMARIES_CHANGED;
				if (nodelist_changed)
					arg.flags |=
					    rgm::NODELIST_CHANGED;

				arg.idlstr_rg_name = new_str(rr);
				arg.rg_managed_state =
				    !rgp->rg_unmanaged;
				arg.desired_primaries =
				    rgp->rg_desired_primaries;
				arg.auto_start = rgp->rg_auto_start;
				arg.rg_create_flag = rg_create;
				arg.set_aff_for_rg = rg_set_aff;
				nodeidlist_to_nodeidseq(
				    rgp->rg_nodelist,
				    arg.nodelist_val);
				if (!rg_create) {
					LogicalNodeset *online_ns =
					    new LogicalNodeset;
					get_rg_online_nodeset(my_rgp,
					    online_ns);
					rgm::lni_t	lni = 0;
					int i = 0;
					arg.online_nodelist.length(
					    online_ns->count());
					while ((lni = online_ns->
					    nextLniSet(lni +1)) != 0) {
						arg.online_nodelist[i]
						    = lni;
						i++;
					}
				}

				arg.aff_flag = 0;
				arg.aff_flag |= aff_flag;

				// fill in the prop value.
				prop_val.length(1);
				prop_val[0].idlstr_nv_name = new_str(
					SCHA_IC_RG_AFFINITY_SOURCES);

				char *cur_cluster_name = strdup(ZONE);
				if (cur_cluster_name == NULL) {
					free(rr);
					free(rc);
					res.err_code = SCHA_ERR_NOMEM;
					return (res);
				}
				//
				// preparte the full rg name string
				// in cluster:rgname
				//
				char *enhanced_dep_name = (char *)
				    malloc(
				    strlen(cur_cluster_name) +
				    strlen(rgp->rg_name) + 2);
				if (enhanced_dep_name == NULL) {
					free(cur_cluster_name);
					free(rr);
					free(rc);
					res.err_code = SCHA_ERR_NOMEM;
					return (res);
				}

				sprintf(enhanced_dep_name, "%s:%s",
				    cur_cluster_name, rgp->rg_name);
				arg.idlstr_src_cluster_name = strdup(
				    cur_cluster_name);
				arg.idlstr_src_rg_name = strdup(
				    rgp->rg_name);
				prop_val[0].nv_val.length(1);
				prop_val[0].nv_val[0] = new_str(
				    enhanced_dep_name);
				arg.prop_val = prop_val;

				//
				// get the reference of the remote
				// rgmd president.
				//
				prgm_pres_v = rgm_comm_getpres_zc(rc);
				if (CORBA::is_nil(prgm_pres_v)) {
					res.err_code =
					    SCHA_ERR_CLRECONF;
					rgm_format_errmsg(&res,
					    REM_RGM_IC_UNREACH, rc);
					free(rr);
					free(rc);
					free(cur_cluster_name);
					free(enhanced_dep_name);
					return (res);
				}
				//
				// Make the idl call to remote rgmd
				// president.
				//
				prgm_pres_v->idl_ext_rg_validate_prop(
				    arg, result_v, e);
				if ((res = except_to_scha_err(e)).
				    err_code != SCHA_ERR_NOERR) {
					res.err_code = SCHA_ERR_DEPEND;
					rgm_format_errmsg(&res,
					REM_RGM_CREATE_AFF_INFO_FAILED,
					    enhanced_dep_name,
					    affp->nl_name);
					free(rr);
					free(rc);
					free(cur_cluster_name);
					free(enhanced_dep_name);
					return (res);
				}

				res.err_code = (scha_err_t)
				    result_v->ret_code;
				if ((const char *)result_v->
				    idlstr_err_msg) res.err_msg =
				    strdup(result_v->idlstr_err_msg);
				free(rr);
				free(rc);
				free(cur_cluster_name);
				free(enhanced_dep_name);
				return (res);
			}

			if (rc == NULL) {
				res.err_code = SCHA_ERR_INVAL;
				free(rr);
				return (res);
			}

			if (rr == NULL) {
				res.err_code = SCHA_ERR_INVAL;
				free(rc);
				return (res);
			}
		}
	}
	return (res);
}

//
// This idl function is used to handle the remote rgm president call
// requesting for the rg online status or rg error stop failed status for
// a given node. This is called from the compute_sp_transitive_closure
// on the cluster which has the source affinity rg.
//

void
rgm_comm_impl::idl_check_remote_rg_sp_aff(
	const rgm::idl_remote_rg_sp_aff_check_args &gets_args,
	rgm::idl_remote_rg_sp_aff_check_result_t_out gets_res,
	Environment &)
{
	char *cluster_name = NULL, *rg_name = NULL, *node_name = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t	rgp = NULL;
	rgm::idl_remote_rg_sp_aff_check_result_t *retval =
	    new rgm::idl_remote_rg_sp_aff_check_result_t();
	unsigned long rebalance_flag = 0;
	unsigned long force_offline_flag = 0;
	unsigned long force_online_flag = 0;
	char *aff_type = NULL;
	char *state_string = NULL;
	LogicalNodeset emptyNodeset;
	LogicalNodeset ignoreNodeset;
	namelist_t *affs, *temp_list = NULL;
	char *full_name = NULL;
	rgm::idl_string_seq_t rg_list;

	rg_name = strdup_nocheck(gets_args.idlstr_rg_name);
	if (rg_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished;
	}

	cluster_name = strdup_nocheck(gets_args.idlstr_cluster_name);
	if (cluster_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished;
	}

	ucmm_print("idl_check_remote_rg_sp_aff()",
	    NOGET("remote cluster = <%s> remote RG = <%s>\n"),
	    cluster_name, rg_name);


	if ((rgp = rgname_to_rg(rg_name)) == NULL) {
		ucmm_print("idl_check_remote_rg_sp_aff()",
		    NOGET("RG <%s> does not exist!!\n"), rg_name);
		res.err_code = SCHA_ERR_RSRC;
		rgm_format_errmsg(&res, REM_INVALID_RG, rg_name);
		goto finished;
	}


	for (affs = rgp->rgl_affinitents.ap_strong_pos;
	    affs != NULL; affs = affs->nl_next) {
		// Check for any inter cluster target rg affinities.
		if (is_enhanced_naming_format_used(affs->nl_name)) {
			// get the online status of the remote rg.
			if (get_remote_rg_status(affs->nl_name, gets_args.nid,
				RG_ONLINE_STATUS)) {
				//
				// The RG is online on the node.  Add it to the
				// namelist.
				//
				ucmm_print("idl_check_remote_rg_sp_aff",
				    NOGET("RG <%s> is online on node <%d>\n"),
				    affs->nl_name, gets_args.nid);
				if (namelist_exists(temp_list,
				    affs->nl_name)) {
					if (namelist_add_name(
					    &temp_list, affs->nl_name).
					    err_code != SCHA_ERR_NOERR) {
						res.err_code = SCHA_ERR_NOMEM;
						goto finished; //lint !e801
					}
				}
			// Check if the rg is in stop failed status.
			} else if (get_remote_rg_status(affs->nl_name,
			    gets_args.nid, RG_ERROR_STOP_FAILED_STATUS)) {
				//
				// We found an error state.
				//
				ucmm_print("idl_check_remote_rg_sp_aff",
				    NOGET("RG <%s> is errored on node <%d>\n"),
				    affs->nl_name, gets_args.nid);
				goto finished; //lint !e801
			}

			continue;
		}

		// Handle local cluster target affinity RGs
		rgp = rgname_to_rg(affs->nl_name);
		CL_PANIC(rgp != NULL);
		if (is_rg_online_not_errored(rgp, gets_args.nid,
		    B_FALSE)) {
			//
			// The RG is online on the node.  Add it to the
			// namelist.
			//
			ucmm_print("idl_check_remote_rg_sp_aff",
			    NOGET("RG <%s> is online on node <%d>\n"),
			    rgp->rgl_ccr->rg_name, gets_args.nid);
			if (!namelist_exists(temp_list,
			    affs->nl_name)) {
				full_name = (char *)malloc(strlen
				    (ZONE) + strlen(affs->nl_name) + 2);
				sprintf(full_name, "%s:%s",
					ZONE, affs->nl_name);
				if (namelist_add_name(&temp_list,
				    full_name).err_code !=
				    SCHA_ERR_NOERR) {
					res.err_code = SCHA_ERR_NOMEM;
					goto finished; //lint !e801
				}
				full_name = NULL;
				free(full_name);
			}
		} else if (gets_args.chk_stop_failed_flag &&
		    (rgp->rgl_xstate[gets_args.nid].rgx_state ==
		    rgm::RG_PENDING_OFF_STOP_FAILED ||
		    rgp->rgl_xstate[gets_args.nid].rgx_state ==
		    rgm::RG_ERROR_STOP_FAILED)) {
			//
			// We found an error state.
			//
			ucmm_print("idl_check_remote_rg_sp_aff",
			    NOGET("RG <%s> is errored on node <%d>\n"),
			    rgp->rgl_ccr->rg_name, gets_args.nid);
			goto finished; //lint !e801
		}

	}
	namelist_to_seq(temp_list,  retval->rg_list);
	ucmm_print("idl_check_remote_rg_sp_aff()",
	    NOGET("processed remote rs notification: returning\n"));

finished:

	free(rg_name);
	free(cluster_name);
	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);
	free(res.err_msg);
	gets_res = retval;
}

//
// Whenever a notification comes for the rg check if this rg has the caller rg
// in its affinity list. If the caller rg does not exist then this is an
// invalid notification.
//
boolean_t validate_remote_rg_request(rglist_p_t rgp, char *rc, char *rrg,
    boolean_t positive_aff) {
	char *temp_rg_name = (char *)malloc(strlen(rc) + strlen(rrg) + 2);
	sprintf(temp_rg_name, "%s:%s", rc, rrg);

	// Check for positive affinities list.
	if (positive_aff) {
		if (!namelist_exists(rgp->rgl_affinities.ap_strong_pos,
		    temp_rg_name) &&
		    (!namelist_exists(rgp->rgl_affinities.ap_weak_pos,
		    temp_rg_name))) {
			free(temp_rg_name);
			return (B_FALSE);
		}
	} else {
		// negetive affinities list.
		if (!namelist_exists(rgp->rgl_affinities.ap_strong_neg,
		    temp_rg_name) &&
		    (!namelist_exists(rgp->rgl_affinities.ap_weak_neg,
		    temp_rg_name))) {
			free(temp_rg_name);
			return (B_FALSE);
		}
	}
	free(temp_rg_name);
	return (B_TRUE);
}

//
// Notify the remote rg to bring the RG offline or rebalance
//
void
rgm_comm_impl::idl_notify_remote_rg(const rgm::idl_notify_remote_rg_args
	&gets_args, rgm::idl_notify_remote_rg_result_t_out gets_res,
	Environment &)
{
	char *cluster_name = NULL, *rg_name = NULL, *node_name = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t	rgp = NULL;
	rgm::idl_notify_remote_rg_result_t *retval =
		new rgm::idl_notify_remote_rg_result_t();
	boolean_t rebalance_flag = B_FALSE;
	boolean_t force_offline_flag = B_FALSE;
	char *aff_type = NULL;
	char *state_string = NULL;
	LogicalNodeset emptyNodeset;
	LogicalNodeset ignoreNodeset;
	char *src_c_name = NULL;
	char *src_rg_name = NULL;
	sc_syslog_msg_handle_t	handle;

	// Check if the noticication was for rebalancing or offlining.
	if (gets_args.flag & rgm::REBALANCE_TARGET_AFFINITY_RG)
		rebalance_flag = B_TRUE;

	if (gets_args.flag & rgm::FORCE_OFFLINE)
		force_offline_flag = B_TRUE;

	rg_name = strdup_nocheck(gets_args.idlstr_rg_name);
	if (rg_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished;
	}


	cluster_name = strdup_nocheck(gets_args.idlstr_cluster_name);
	if (cluster_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished;
	}

	ucmm_print("idl_notify_remote_rg()",
	    NOGET("remote cluster = <%s> remote RG = <%s>\n"),
	    cluster_name, rg_name);


	if ((rgp = rgname_to_rg(rg_name)) == NULL) {
		ucmm_print("idl_notify_remote_rg()",
		    NOGET("RG <%s> does not exist!!\n"), rg_name);
		res.err_code = SCHA_ERR_RG;
		rgm_format_errmsg(&res, REM_INVALID_RG, rg_name);
		goto finished;
	}

	src_c_name = strdup(gets_args.idlstr_src_cluster_name);
	src_rg_name = strdup(gets_args.idlstr_src_rg_name);

	// Validate if the notification caller rg is a valid affinity.
	if (!validate_remote_rg_request(rgp, src_c_name, src_rg_name,
		((gets_args.positive_aff)?B_TRUE:B_FALSE))) {
		ucmm_print("idl_notify_remote_rg()",
		    NOGET("invalid affinity for RG <%s> !!\n"), rg_name);
		res.err_code = SCHA_ERR_INVALID_REMOTE_AFF;
		goto finished;
	}

	// Handle rebalancing of the target affinity rg.
	if (rebalance_flag) {
		ucmm_print("idl_notify_remote_rg()",
		    NOGET("rebalnce flag set  RG <%s> \n"), rg_name);
		if (rgp->rgl_ccr->rg_unmanaged)
			goto finished;

		//
		// Note that we check the flag on rgp, not on rg_aff.
		// That is intentional, because unpack_rg_seq sets
		// the flags on the RGs one at a time, so rg_aff
		// might not have its flag set yet.
		//

		if (gets_args.positive_aff) {
			aff_type = "positive";
			state_string = "RG_PENDING_ONLINE";
		} else {
			aff_type = "negative";
			state_string = "RG_OFFLINE";
		}

		if (rgp->rgl_no_rebalance) {
			//
			// we're not supposed to run rebalance yet.
			// Set a flag on the RG so the caller knows
			// to run it later.
			//
			rgp->rgl_delayed_rebalance = B_TRUE;
			ucmm_print("RGM", NOGET("process_needy_rg: "
			    "Not running rebalance on RG <%s>, even "
			    "though it has a strong %s affinity "
			    "for RG <%s:%s> which is %s, because the "
			    "latter has the rgl_no_rebalance flag set.\n"),
			    rgp->rgl_ccr->rg_name, aff_type,
			    src_c_name, src_rg_name, state_string);
		} else if (rgp->rgl_no_triggered_rebalance) {
			//
			// We check the no_triggered_rebalance flag on
			// the rg_aff, not rgp.  This flag is only set when
			// do_rg_switch is switching an RG. In that case, we
			// don't want to rebalance the RG due to strong
			// affinities.  However, we do want to rebalance it
			// if it goes start_failed (or something like that),
			// so we can't just use the no_rebalance flag.
			//
			ucmm_print("RGM", NOGET("process_needy_rg: "
			    "Not running rebalance on RG <%s>, even "
			    "though it has a strong %s affinity "
			    "for RG <%s:%s> which is %s, because the "
			    "former has the rgl_no_triggered_rebalance "
			    "flag set.\n"),
			    rgp->rgl_ccr->rg_name, aff_type,
			    src_c_name, src_rg_name, state_string);
		} else if (!rgp->rgl_ok_to_start) {
			ucmm_print("RGM", NOGET("process_needy_rg: "
			    "Not running rebalance on RG <%s>, even "
			    "though it has a strong %s affinity for RG <%s:%s> "
			    "which is %s, because the former is not "
			    "ok_to_start.\n"), rgp->rgl_ccr->rg_name,
			    aff_type, src_c_name, src_rg_name, state_string);
		} else if (rgp->rgl_switching == SW_UPDATING) {
				ucmm_print("RGM", NOGET(
				    "process_needy_rg: busting update "
				    "on RG <%s> because it has a strong %s "
				    "affinity for RG <%s:%s> which is %s.\n"),
				    rgp->rgl_ccr->rg_name, aff_type,
				    src_c_name, src_rg_name, state_string);
				rgp->rgl_switching = SW_UPDATE_BUSTED;
				//
				// Don't need to do a cond_broadcast,
				// because we didn't change the
				// condition on which a thread waiting
				// for an update to finish is waiting.
				//
		} else {
			ucmm_print("RGM", NOGET(
			    "process_needy_rg: calling "
			    "rebalance on RG <%s> because it has a "
			    "strong %s affinity for RG <%s> which is "
			    "%s.\n"), rgp->rgl_ccr->rg_name,
			    aff_type, src_c_name, src_rg_name, state_string);
			//
			// If we're processing negative affinities, pass the
			// planned_on nodeset for rebalance to ignore.
			//
			// For positive affinities, we don't want to ignore
			// the planned_on_nodes.
			//

			if (!gets_args.positive_aff) {
				for (int i = 0; i < gets_args.ignore_nodelist.
				    length(); i++) {
					ignoreNodeset.addLni(
					    gets_args.ignore_nodelist[i]);
				}
				rebalance(rgp, ignoreNodeset, B_FALSE, B_TRUE);
			} else {
				rebalance(rgp, emptyNodeset, B_FALSE, B_TRUE);
			}
		}
	}

	// Handle offlining of the rg.
	if (force_offline_flag) {
		namelist_t *nl = NULL;
		rgm::rgm_comm_var prgm_serv;
		Environment e;

		LogicalNode *lnNode = lnManager->findObject(gets_args.nid);
		CL_PANIC(lnNode != NULL);
		// get the node for where the rg is to be forced offline.
		sol::nodeid_t slave = lnNode->getNodeid();


		ucmm_print("idl_notify_remote_rg()",
			NOGET("force offline flag set <%s> \n"),
			rgp->rgl_ccr->rg_name);
		namelist_add_name(&nl, rgp->rgl_ccr->rg_name);

		// make an idl call to the slave node.
		prgm_serv = rgm_comm_getref(slave);
		if (CORBA::is_nil(prgm_serv)) {
			ucmm_print("idl_notify_remote_rg()",
			    NOGET("unable to get the reference of "
			    "slave node"));
			return;
		}

		prgm_serv->rgm_force_rg_offline(orb_conf::local_nodeid(),
		    Rgm_state->node_incarnations.members[
		    orb_conf::local_nodeid()], gets_args.nid,
		    lnNode->getIncarnation(), rgp->rgl_ccr->rg_name, e);
		if ((res = except_to_scha_err(e)).err_code !=
		    SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RS_TAG, rg_name);
			//
			// SCMSGS
			// @explanation
			// Failed to bring the inter-cluster
			// source RG affinity offline.
			// Possible cause could be that
			// the remote zone cluster is down.
			// @user_action
			// No action.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE,
			    "Could not bring the source affinity "
			    "resource group %s offline",
			    rgp->rgl_ccr->rg_name);
			sc_syslog_msg_done(&handle);
		}
		rgm_free_nlist(nl);
	}

	ucmm_print("idl_notify_remote_rg()",
	    NOGET("processed remote rg notification: returning\n"));

finished:

	free(rg_name);
	free(cluster_name);
	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);
	free(res.err_msg);
	gets_res = retval;
}


//
// Get the state of a remote resource group.
// The return value is a string of form (define in rgm_util.h).
//
void
rgm_comm_impl::idl_get_remote_rg_state(const rgm::idl_remote_rg_get_state_args
	&gets_args, rgm::idl_remote_rg_get_state_result_t_out gets_res,
	Environment &)
{
	char *cluster_name = NULL, *rg_name = NULL, *node_name = NULL;

	rgm::idl_remote_rg_get_state_result_t	*retval =
	    new rgm::idl_remote_rg_get_state_result_t();
	scha_rsstate_t		state;
	char			*seq_id;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rglist_p_t	rl = NULL;
	boolean_t  online_flag = B_FALSE;
	boolean_t  err_stp_flag = B_FALSE;
	boolean_t  switch_flag = B_FALSE;


	if (gets_args.flag & rgm::ONLINE_STATUS)
		online_flag = B_TRUE;

	if (gets_args.flag & rgm::RG_ERR_STOP_FAILED_STATUS)
		err_stp_flag = B_TRUE;

	if (gets_args.flag & rgm::RG_SWITCH_STATUS)
		switch_flag = B_TRUE;


	// Set the defalt to offline state.
	retval->status = rgm::RG_OFFLINE;
	rg_name = strdup_nocheck(gets_args.idlstr_rg_name);
	if (rg_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished;
	}

	cluster_name = strdup_nocheck(gets_args.idlstr_cluster_name);
	if (cluster_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished;
	}

	ucmm_print("idl_get_remote_rg_state()",
	    NOGET("remote cluster = <%s> remote RG = <%s>\n"),
	    cluster_name, rg_name);


	if ((rl = rgname_to_rg(rg_name)) == NULL) {
		ucmm_print("idl_get_remote_rg_state()",
		    NOGET("RG <%s> does not exist!!\n"), rg_name);
		res.err_code = SCHA_ERR_RG;
		rgm_format_errmsg(&res, REM_INVALID_RG, rg_name);
		goto finished;
	}


	// recheck for the validity of the rg again.
	if ((rl = rgname_to_rg(rg_name)) == NULL) {
		ucmm_print("idl_get_remote_rg_state()",
		    NOGET("RG <%s> does not exist!!\n"), rg_name);
		res.err_code = SCHA_ERR_RG;
		rgm_format_errmsg(&res, REM_INVALID_RG, rg_name);
		goto finished;
	}

	if (switch_flag) {
		if (rl->rgl_switching == SW_IN_PROGRESS ||
			rl->rgl_switching == SW_IN_FAILBACK ||
			rl->rgl_switching == SW_UPDATING) {
			retval->switch_flag = 1;
		} else {
			retval->switch_flag = 0;
		}
		goto finished;
	}

	switch (rl->rgl_xstate[gets_args.nid].rgx_state) {

	// "on-ish" states
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_ERROR_STOP_FAILED:
		if (err_stp_flag)
			retval->status = rgm::RG_ERROR_STOP_FAILED;
		break;
	case rgm::RG_ONLINE:
	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_ON_STARTED:
	case rgm::RG_PENDING_OFF_START_FAILED:
	case rgm::RG_ON_PENDING_DISABLED:
	case rgm::RG_ON_PENDING_MON_DISABLED:
	case rgm::RG_ON_PENDING_METHODS:
	case rgm::RG_ON_PENDING_R_RESTART:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
		if (online_flag)
			retval->status = rgm::RG_ONLINE;
		break;
	// "off-ish" states
	case rgm::RG_OFFLINE:
	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_BOOT:
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_OFFLINE_START_FAILED:
	default:
		retval->status = rgm::RG_OFFLINE;
	}

	ucmm_print("idl_get_remote_rg_state()",
	    NOGET("got remote rs state: returning\n"));

finished:

	free(rg_name);
	free(cluster_name);
	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);
	free(res.err_msg);
	gets_res = retval;
}


//
// Update various properties of an RG.
//
// We set the rgl_switching flag on the rg to SW_UPDATING, which will prevent
// any other thread from doing an update for this RG.
//
void
rgm_comm_impl::idl_ext_rg_validate_prop(const rgm::idl_rg_validate_prop_args
	&rgup_args, rgm::idl_regis_result_t_out rs_res, Environment &e) {
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	char *rg_name = NULL, *src_rg_name = NULL, *src_cluster_name = NULL;
	char *full_src_rg_name = NULL;
	rgm_rg_t	*rg_ccr = NULL;
	rglist_p_t	rg_ptr = NULL;
	nodeidlist_t	*nodelist = NULL;
	rgm::idl_regis_result_t *retval = new rgm::idl_regis_result_t();
	char *affinitent = NULL;
	rgm::lni_seq_t lnilist = NULL;
	rgm::idl_rg_update_prop_args_v1 args;
	rgm::idl_regis_result_t_var	result_v;
	rgm::rgm_comm_var prgm_pres_v;
	int i, j;
	rgm::idl_nameval_seq_t	prop_val;
	char *enhanced_aff_name = NULL;
	rgm_affinities_t 	affinities = {NULL, NULL, NULL, NULL, NULL};
	char *aff_type = NULL;
	boolean_t affs_changed = B_FALSE;
	boolean_t  des_prims_changed = B_FALSE;
	boolean_t  nodelist_changed = B_FALSE;
	boolean_t  sp = B_FALSE;
	boolean_t  sn = B_FALSE;
	boolean_t  wp = B_FALSE;
	boolean_t  wn = B_FALSE;

	if (rgup_args.flags & rgm::AFFINITY_CHANGED)
		affs_changed = B_TRUE;
	if (rgup_args.flags & rgm::DESIRED_PRIMARIES_CHANGED)
		des_prims_changed = B_TRUE;
	if (rgup_args.flags & rgm::NODELIST_CHANGED)
		nodelist_changed = B_TRUE;
	if (rgup_args.aff_flag & rgm::STRONG_POS)
		sp = B_TRUE;
	if (rgup_args.aff_flag & rgm::STRONG_NEG)
		sn = B_TRUE;
	if (rgup_args.aff_flag & rgm::WEAK_POS)
		wp = B_TRUE;
	if (rgup_args.aff_flag & rgm::WEAK_NEG)
		wn = B_TRUE;

	rgm_lock_state();

	prgm_pres_v = rgm_comm_getpres_zc(ZONE);

	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		goto finished;
	}

	if (sp)
		aff_type = strdup(SP);
	if (sn)
		aff_type = strdup(SN);
	if (wp)
		aff_type = strdup(WP);
	if (wn)
		aff_type = strdup(WN);

	rg_name = strdup_nocheck(rgup_args.idlstr_rg_name);
	src_rg_name = strdup_nocheck(rgup_args.idlstr_src_rg_name);
	src_cluster_name = strdup_nocheck(rgup_args.idlstr_src_cluster_name);

	if ((rg_name == NULL) || (src_rg_name == NULL) ||
	    (src_cluster_name == NULL)) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		goto finished; //lint !e801
	}

	enhanced_aff_name = (char *)malloc(
	    strlen(src_cluster_name) +
	    strlen(src_rg_name) + 5);

	if (enhanced_aff_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished;
	}

	sprintf(enhanced_aff_name, "%s%s:%s",
	    aff_type, src_cluster_name,
	    src_rg_name);

	full_src_rg_name = (char *)malloc(
	    strlen(src_cluster_name) +
	    strlen(src_rg_name) + 2);

	if (full_src_rg_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto finished;
	}

	sprintf(full_src_rg_name, "%s:%s",
	    src_cluster_name,
	    src_rg_name);

	if ((rg_name == NULL) || (src_rg_name == NULL) ||
	    (src_cluster_name == NULL)) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_format_errmsg(&res, REM_INTERNAL_MEM);
		goto finished; //lint !e801
	}

	if (rgup_args.prop_val.length() == 0) {
		// If there is no property to be updated,
		// return with error.
		res.err_code = SCHA_ERR_INVAL;
		ucmm_print("idl_ext_rg_validate_prop()", NOGET(
		    "No property is updated for RG <%s>\n"), rg_name);
		goto finished; //lint !e801
	}

	ucmm_print("RGM", NOGET("idl_ext_rg_validate_prop(): "
	    "RG = <%s>\n"), rg_name);

	rg_ptr = rgname_to_rg(rg_name);

	if (rg_ptr == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_INVALID_RG, rg_name);
		ucmm_print("RGM", NOGET("idl_ext_rg_validate_prop(): "
		    "Invalid RG = <%s>\n"), rg_name);
		goto finished; //lint !e801
	}
	/* Set the msg locale */
	args.locale = new_str(fetch_msg_locale());
	/* Set the option flags */
	args.flags = 0;
	args.caller_flag = rgm::RGM_FROM_SCRGADM;
	args.flags |= rgm::SUPPRESS_RG_VALIDATION;
	if (rgup_args.set_aff_for_rg)
		args.flags |= rgm::INTER_CLUSTER_RG_AFF_SET;
	else
		args.flags |= rgm::INTER_CLUSTER_RG_AFF_UNSET;
	args.idlstr_rg_name = new_str(rg_ptr->rgl_ccr->rg_name);

	prop_val.length(1);
	prop_val[0].idlstr_nv_name =
		new_str(SCHA_IC_RG_AFFINITY_SOURCES);

	prop_val[0].nv_val.length(1);
	prop_val[0].nv_val[0] =
	    new_str(enhanced_aff_name);
	args.prop_val = prop_val;

	//
	// Only do the following check if affinities or
	// desired primaries have changed, because we want to
	// issue the warning only once.
	//
	if (sp && (affs_changed || des_prims_changed)) {
		if (rg_ptr->rgl_ccr->rg_desired_primaries <
		    rgup_args.desired_primaries) {
			rgm_format_errmsg(&res, RWM_AFF_LESS_DESIRED_P,
			    full_src_rg_name, rg_ptr->rgl_ccr->rg_name,
			    full_src_rg_name);
		}
	}

	if (sp && rgup_args.auto_start && !rg_ptr->rgl_ccr->rg_auto_start) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RG_AFF_AUTO_START,
		    full_src_rg_name, rg_ptr->rgl_ccr->rg_name);
		goto finished;
	}


	nodeidlist_to_nodeidseq(rg_ptr->rgl_ccr->rg_nodelist, lnilist);

	if (affs_changed || des_prims_changed || nodelist_changed) {

		int  i = count_intersect_lniseq(rgup_args.nodelist_val,
		    lnilist);

		if (i == 0) {
			ucmm_print("RGM", NOGET(
				"rg_affinities_check_nodelist(): "
				"rg <%s> has strong positive affinities "
				"for rg <%s>; but the Nodelists of these "
				"two rg do not have any nodes in common"),
				rg_ptr->rgl_ccr->rg_name,
				rg_ptr->rgl_ccr->rg_name);
				rgm_format_errmsg(&res, REM_NODELIST_NO_COMMON,
				full_src_rg_name,
				rg_ptr->rgl_ccr->rg_name);
			res.err_code = SCHA_ERR_INVAL;
			goto finished;		}

		if (sp && i < rgup_args.desired_primaries) {
			// just issue a warning message
			ucmm_print("RGM", NOGET(
			    "rg_affinities_check_nodelist(): "
			    "the number of nodes in the intersection of "
			    "rg <%s> and rg <%s> is less than the "
			    "Desired_primaries of rg <%s>"),
			    rg_ptr->rgl_ccr->rg_name, rg_ptr->rgl_ccr->rg_name,
			    rg_ptr->rgl_ccr->rg_name);

			rgm_format_errmsg(&res, RWM_AFF_DESIRED_PRIM,
			    full_src_rg_name, rg_ptr->rgl_ccr->rg_name,
			    full_src_rg_name);
		}
	}


	// aff_rg is an affinitee of prg

	if (rgup_args.rg_managed_state && rg_ptr->rgl_ccr->rg_unmanaged) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RG_AFF_MANAGED,
		    full_src_rg_name, rg_ptr->rgl_ccr->rg_name);
		goto finished;
	}

	if (rgup_args.rg_create_flag)
		goto update_rg;


	if (sp) {
		for (i = 0; i < rgup_args.online_nodelist.length(); i++) {
			if (!is_rg_online_not_errored(rg_ptr,
				rgup_args.online_nodelist[i], B_FALSE)) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    REM_RG_AFF_STATE, full_src_rg_name,
				    "positive", rg_ptr->rgl_ccr->rg_name);
				goto finished;
			}
		}
	}

	if (sn) {
		for (i = 0; i < rgup_args.online_nodelist.length(); i++) {
			if (is_rg_online_not_errored(rg_ptr,
				rgup_args.online_nodelist[i], B_FALSE)) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    REM_RG_AFF_STATE, full_src_rg_name,
				    "negetive", rg_ptr->rgl_ccr->rg_name);
				goto finished;
			}
		}
	}

	if (wp) {
		for (i = 0; i < rgup_args.online_nodelist.length(); i++) {
			if (!is_rg_online_not_errored(rg_ptr,
				rgup_args.online_nodelist[i], B_FALSE)) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    RWM_RG_AFF_STATE, full_src_rg_name,
				    "positive", rg_ptr->rgl_ccr->rg_name);
			}
		}
	}


	if (wn) {
		for (i = 0; i < rgup_args.online_nodelist.length(); i++) {
			if (is_rg_online_not_errored(rg_ptr,
				rgup_args.online_nodelist[i], B_FALSE)) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    RWM_RG_AFF_STATE, full_src_rg_name,
				    "negetive", rg_ptr->rgl_ccr->rg_name);
			}
		}
	}

update_rg :
	prgm_pres_v->idl_scrgadm_rg_update_prop_v1(args, result_v, e);

	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
		goto finished;

	res.err_code = (scha_err_t)result_v->ret_code;
	if ((const char *)result_v->idlstr_err_msg)
		res.err_msg = strdup(result_v->idlstr_err_msg);

finished:
	free(src_cluster_name);
	free(src_rg_name);
	free_rgm_affinities(&affinities);
	free(rg_name);
	free(full_src_rg_name);
	retval->ret_code = res.err_code;
	retval->idlstr_err_msg = new_str(res.err_msg);
	free(res.err_msg);
	rs_res = retval;
	rgm_unlock_state();
}
#endif

//
// Return the number of nodes in the intersection of two given rgs'
// node lists.
//
// If 'logical_nodes' is true, count logical nodes (i.e. zones); otherwise
// count physical nodes.
//
static uint_t
count_intersect_nlist(const rgm_rg_t *rgp, const rgm_rg_t *rgp2,
    boolean_t logical_nodes)
{
	LogicalNodeset *ns1 = NULL;
	LogicalNodeset *ns2 = NULL;

	if (logical_nodes) {
		ns1 = get_logical_nodeset_from_nodelist(rgp->rg_nodelist);
		ns2 = get_logical_nodeset_from_nodelist(rgp2->rg_nodelist);
	} else {
		ns1 = get_phys_nodeset_from_nodelist(rgp->rg_nodelist);
		ns2 = get_phys_nodeset_from_nodelist(rgp2->rg_nodelist);
	}

	LogicalNodeset nsIntersect = *ns1 & *ns2;

	delete(ns1);
	delete(ns2);

	return (nsIntersect.count());
}

static int
count_intersect_lniseq(rgm::lni_seq_t ls1, rgm::lni_seq_t ls2)
{
	uint_t i, j, count = 0;
	for (i = 0; i < ls1.length(); i++) {
		for (j = 0; j < ls2.length(); j++) {
			if (ls1[i] == ls2[j])
				count ++;
		}
	}
	return (count);
}

//
// Similar to get_logical_nodeset_from_nodelist(), but the returned
// LogicalNodeset contains only the physical nodes represented in the
// specified nodeidlist.
//
// Unlike get_logical_nodeset_from_nodelist(), this function assumes that
// it is called only on the president, so the specified nodeidlist must
// contain only valid nodeids.
//
// Allocates memory for the returned LogicalNodeset, which must be deleted
// by the caller.
//
static LogicalNodeset*
get_phys_nodeset_from_nodelist(nodeidlist_t *nodelist)
{
	LogicalNodeset *nset = new LogicalNodeset;
	nodeidlist_t *nl;
	CL_PANIC(nset != NULL);

	for (nl = nodelist; nl != NULL; nl = nl->nl_next) {
		LogicalNode *lnode = lnManager->findObject(nl->nl_lni);
		CL_PANIC(lnode != NULL);
		nset->addLni(lnode->getNodeid());
	}
	return (nset);
}


//
// rg_affinities_check_nodelist -
//
// Assume rg1 is the specified rg.
// If rg1 has a (strong or weak) positive affinity for rg2, then we
// count the number of nodes in the intersection of rg1's Nodelist and
// rg2's Nodelist; call this number 'i'.  The following checks are made:
//
// . If i is zero, the edit is vetoed if rg1 has strong positive
//   affinity for rg2; or a warning is issued if rg1 has a weak positive
//   affinity for rg2.
//
// . If i is less than rg1's Desired_primaries and rg1's affinity for
//   rg2 is strong, issue a warning message.
//
//
// Call helper routines check_sp_affinities_nodelist and
// check_wp_affinities_nodelist to do the actual work.
//
// XXX The following check seems to be a "best practice" but is not
// XXX implemented yet.
// . The nodes that are in common between rg1's Nodelist and rg2's
//   Nodelist should appear in the same order in both Nodelists.  If
//   not, a warning is issued, something like this:  "Warning:
//   resource group <rg1> with positive affinity for resource group
//   <rg2> has a different ordering of nodes in Nodelist."
//
// This function is called when RG_affinities, Desired_primaries,
// or Nodelist is edited.
//
scha_errmsg_t
rg_affinities_check_nodelist(rgm_rg_t *rgp, const rgm_affinities_t *affinities)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	scha_errmsg_t	res2 = {SCHA_ERR_NOERR, NULL};
	namelist_t	*affp;
	rglist_p_t rgp_aff;

	for (affp = affinities->ap_strong_pos; affp != NULL;
	    affp = affp->nl_next) {
		if (is_enhanced_naming_format_used(affp->nl_name))
			continue;
		rgp_aff = rgname_to_rg(affp->nl_name);
		CL_PANIC(rgp_aff != NULL);
		res2 = check_sp_affinities_nodelist(rgp, rgp_aff->rgl_ccr);
		if (res2.err_code != SCHA_ERR_NOERR) {
			return (res2);
		} else if (res2.err_msg != NULL) {
			// save the warning msg, if any
			rgm_sprintf(&res, "%s", res2.err_msg);
			free(res2.err_msg);
		}
	}

	for (affp = affinities->ap_weak_pos; affp != NULL;
	    affp = affp->nl_next) {
		if (is_enhanced_naming_format_used(affp->nl_name))
			continue;
		rgp_aff = rgname_to_rg(affp->nl_name);
		CL_PANIC(rgp_aff != NULL);

		res2 = check_wp_affinities_nodelist(rgp, rgp_aff->rgl_ccr);
		if (res2.err_code != SCHA_ERR_NOERR) {
			return (res2);
		} else if (res2.err_msg != NULL) {
			// save the warning msg, if any
			rgm_sprintf(&res, "%s", res2.err_msg);
			free(res2.err_msg);
		}
	}

	return (res);
}

//
// check_sp_affinities_nodelist
// ---------------------------
// Performs basic nodelist checks for two rgs related by strong positive
// affinities.  If the two rgs have no nodes in common, return an error.
//
// If the two rgs have fewer nodes in common than the affinitent's
// desired_primaries, return a warning.
//
scha_errmsg_t
check_sp_affinities_nodelist(rgm_rg_t *rgp_affinitent, rgm_rg_t *rgp_affinitee)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	uint_t i;

	//
	// Count the the number of nodes in the intersection of
	// both rgs' nodelists.  Count physical or logical nodes, according
	// to whether physical node affinities are applicable.
	//
	i = count_intersect_nlist(rgp_affinitent, rgp_affinitee,
	    use_logicalnode_affinities());

	if (i == 0) {
		ucmm_print("RGM", NOGET(
		    "check_sp_affinities_nodelist(): "
		    "rg <%s> has strong positive affinities for rg <%s>; "
		    "but the Nodelists of these two rg do not have "
		    "any nodes in common"), rgp_affinitent->rg_name,
		    rgp_affinitee->rg_name);
		rgm_format_errmsg(&res, REM_NODELIST_NO_COMMON,
		    rgp_affinitent->rg_name, rgp_affinitee->rg_name);
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if (i < rgp_affinitent->rg_desired_primaries) {
		// just issue a warning message
		ucmm_print("RGM", NOGET(
		    "check_sp_affinities_nodelist(): "
		    "the number of nodes in the intersection of "
		    "rg <%s> and rg <%s> is less than the "
		    "Desired_primaries of rg <%s>"),
		    rgp_affinitent->rg_name, rgp_affinitee->rg_name,
		    rgp_affinitent->rg_name);

		rgm_format_errmsg(&res, RWM_AFF_DESIRED_PRIM,
		    rgp_affinitent->rg_name, rgp_affinitee->rg_name,
		    rgp_affinitent->rg_name);
	}
	return (res);
}

//
// check_wp_affinities_nodelist
// ---------------------------
// Performs basic nodelist checks for two rgs related by weak positive
// affinities.  If the two rgs have no nodes in common, return an warning.
//
scha_errmsg_t
check_wp_affinities_nodelist(rgm_rg_t *rgp_affinitent, rgm_rg_t *rgp_affinitee)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	uint_t i;

	// count the the number of nodes in the intersection of
	// both rg's
	i = count_intersect_nlist(rgp_affinitent, rgp_affinitee,
	    use_logicalnode_affinities());

	if (i == 0) {
		ucmm_print("RGM", NOGET(
		    "check_wp_affinities_nodelist(): "
		    "rg <%s> has weak positive affinities for rg <%s>; "
		    "but the Nodelists of these two rg do not have "
		    "any nodes in common"), rgp_affinitent->rg_name,
		    rgp_affinitee->rg_name);
		rgm_format_errmsg(&res, RWM_NODELIST_NO_COMMON,
		    rgp_affinitent->rg_name, rgp_affinitee->rg_name);
	}
	return (res);
}

//
// check_system_rg_updatable_prop
// The only properties updatable on a "system" RG are the system prop
// and suspend/resume property. Set *prop_updatable_p to B_TRUE if this list has
// only these properties and B_FALSE otherwise.
// store error/warning as appropriate in scha_errmsg_t.
//

scha_errmsg_t
check_system_rg_updatable_props(const rglist_p_t rg_ptr,
    const rgm::idl_nameval_seq_t &props, scha_errmsg_t *warning_res)
{
	const char *prop_name;
	uint_t i, len;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	len = props.length();
	for (i = 0; i < len; i++) {
		prop_name = props[i].idlstr_nv_name;
		if (strcasecmp(prop_name, SCHA_RG_SUSP_AUTO_RECOVERY)
		    == 0) {
			rgm_format_errmsg(warning_res, RWM_SYSTEM_RG_SUSPEND,
			    rg_ptr->rgl_ccr->rg_name);
			if (res.err_code != SCHA_ERR_NOERR) {
				return (res);
			}
			continue;
		}
		if (strcasecmp(prop_name, SCHA_RG_SYSTEM) != 0) {
			res.err_code = SCHA_ERR_ACCESS;
			rgm_format_errmsg(&res, REM_SYSTEM_RG,
			    rg_ptr->rgl_ccr->rg_name);
			ucmm_print("check_system_rg_updatable_props()",
			    NOGET("RG <%s> is a system RG\n"),
			    rg_ptr->rgl_ccr->rg_name);
			return (res);
		}

	}
	return (res);
}

// Perform basic sanity check of the values in RG's property.

scha_errmsg_t
rg_property_check(rgm_rg_t *rgp, rgm_affinities_t *affinities,
    boolean_t rg_create, boolean_t affs_changed, boolean_t des_prims_changed,
    boolean_t nodelist_changed, boolean_t auto_start_changed,
    const rgm::idl_string_seq_t &rg_list)
{
	nodeidlist_t	*rglst;
	namelist_t	*nlist;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	const char 	*rg_dup_name;
	// SC SLM addon start
	const char	*slm_err;
	// SC SLM addon end

	// check node duplication in RG nodelist.
	rglst = rgp->rg_nodelist;
	if (rglst != NULL) {
		if (get_dup_lni(rglst) != 0) {
			ucmm_print("RGM", NOGET(
			    "Duplicated name in <%s>\n"), SCHA_NODELIST);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_DUPNODE_IN_NODELIST);
			return (res);
		}
	}

	// Check node duplication in RG_dependencies property.
	nlist = rgp->rg_dependencies;
	if (nlist != NULL) {
		// Use the helper function from rgm_util.cc to check
		// for duplicate names
		rg_dup_name = get_dup_name(nlist);

		// If we have a duplicate name, it's an error
		if (rg_dup_name != NULL) {
			ucmm_print("RGM", NOGET("Duplicated name in <%s>\n"),
			    SCHA_RG_DEPENDENCIES);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_DUP_RG_DEP, rgp->rg_name,
			    rg_dup_name, SCHA_RG_DEPENDENCIES);
			return (res);
		}
	}

	if (rgp->rg_max_primaries == 0) {
		ucmm_print("rg_property_check()", NOGET(
		    "RG <%s> must have Max Primaries set greater than zero\n"),
		    rgp->rg_name);
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	// maximum primaries can not be negative value.
	if (rgp->rg_max_primaries < 0) {
		// Wrong value passed in.
		res.err_code = SCHA_ERR_INVAL;
		ucmm_print("rg_property_check()", NOGET(
		    "Negative max_primaries for RG <%s> is passed in\n"),
		    rgp->rg_name);
		return (res);
	}

	// desired primaries can not be negative value.
	if (rgp->rg_desired_primaries < 0) {
		// Wrong value passed in.
		res.err_code = SCHA_ERR_INVAL;
		ucmm_print("rg_property_check()", NOGET(
		    "Negative desired_primaries for RG <%s> is passed in\n"),
		    rgp->rg_name);
		return (res);
	}

	// Do sanity check for RG mode and rg_max_primaries.
	// At this point, the RG mode must be either "Failover" or
	// "Scalable".  Otherwise, there is severe problem.
	CL_PANIC(rgp->rg_mode != RGMODE_NONE);

	if (rgp->rg_mode == RGMODE_FAILOVER) {
		if (rgp->rg_max_primaries != 1) {
			ucmm_print("rg_property_check()", NOGET(
			    "RG <%s> Max Primaries must be 1 for "
			    "Failover mode RG\n"), rgp->rg_name);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RG_FO_MP_GT_1,
			    rgp->rg_name);
			return (res);
		}
		if (rgp->rg_desired_primaries > 1) {
			ucmm_print("rg_property_check()", NOGET(
			    "RG <%s> Desired Primaries must be no bigger than "
			    "1 for Failover mode RG\n"), rgp->rg_name);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RG_FO_DP_GT_1,
			    rgp->rg_name);
			return (res);
		}
	}

	// SC SLM addon start
	//
	// RG_SLM_TYPE has two possible values.
	//
	if (strcmp(rgp->rg_slm_type, SCHA_SLM_TYPE_MANUAL) != 0 &&
	    strcmp(rgp->rg_slm_type, SCHA_SLM_TYPE_AUTOMATED) != 0) {
		ucmm_print("rg_property_check()", NOGET(
		    "RG <%s> %s must be either %s or %s"),
		    rgp->rg_name, SCHA_RG_SLM_TYPE, SCHA_SLM_TYPE_MANUAL,
		    SCHA_SLM_TYPE_AUTOMATED);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RG_SLM_TYPE_WR,
		    rgp->rg_name, SCHA_SLM_TYPE_MANUAL,
		    SCHA_SLM_TYPE_AUTOMATED);
		return (res);
	}

	//
	// RG_SLM_TYPE=automated is only accepted after
	// checking the RG name length.
	//
	if (strcmp(rgp->rg_slm_type, SCHA_SLM_TYPE_AUTOMATED) == 0) {
		slm_err = check_slm_rgname_length(rgp);
		if (slm_err != NULL) {
			ucmm_print("rg_property_check()", (char *)slm_err,
			    rgp->rg_name, SCSLM_RG_NAME_MAX_LENGTH);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, slm_err,
			    rgp->rg_name, SCSLM_RG_NAME_MAX_LENGTH);
			return (res);
		}
	}

	//
	// RG_SLM_TYPE=automated is only accepted after
	// checking that other RG names with RG_SLM_TYPE=automated do not
	// differ only about '-' or '_'
	// see details in slm_rgm_name_strcmp() comments
	//
	if (strcmp(rgp->rg_slm_type, SCHA_SLM_TYPE_AUTOMATED) == 0) {
		slm_err = check_slm_rgname_clash(rgp);
		if (slm_err != NULL) {
			ucmm_print("rg_property_check()", (char *)slm_err,
			    rgp->rg_name, REM_RG_SLM_NAME_CLASH);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, slm_err,
			    rgp->rg_name, REM_RG_SLM_NAME_CLASH);
			return (res);
		}
	}

	//
	// RG_SLM_PSET_TYPE has two possible values.
	//
	if (strcmp(rgp->rg_slm_pset_type, SCHA_SLM_PSET_TYPE_DEFAULT) != 0 &&
	    strcmp(rgp->rg_slm_pset_type,
		SCHA_SLM_PSET_TYPE_DEDICATED_WEAK) != 0 &&
	    strcmp(rgp->rg_slm_pset_type,
		SCHA_SLM_PSET_TYPE_DEDICATED_STRONG) != 0) {
		ucmm_print("rg_property_check()", NOGET(
		    "RG <%s> %s must be either %s or %s or %s"),
		    rgp->rg_name, SCHA_RG_SLM_PSET_TYPE,
		    SCHA_SLM_PSET_TYPE_DEFAULT,
		    SCHA_SLM_PSET_TYPE_DEDICATED_WEAK,
		    SCHA_SLM_PSET_TYPE_DEDICATED_STRONG);
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RG_SLM_PSET_TYPE_WR,
		    rgp->rg_name, SCHA_SLM_PSET_TYPE_DEFAULT,
		    SCHA_SLM_PSET_TYPE_DEDICATED_WEAK,
		    SCHA_SLM_PSET_TYPE_DEDICATED_STRONG);
		return (res);
	}

	//
	// RG_SLM_CPU_SHARES value check
	//
	if (rgp->rg_slm_cpu != SCHA_SLM_RG_CPU_SHARES_DEFAULT) {
		slm_err = check_slm_cpu_shares(rgp->rg_slm_cpu);
		if (slm_err != NULL) {
			ucmm_print("rg_property_check()", (char *)slm_err,
			    rgp->rg_name, SCHA_RG_SLM_CPU_SHARES);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, slm_err,
			    rgp->rg_name, SCHA_RG_SLM_CPU_SHARES);
			return (res);
		}
	}

	//
	// RG_SLM_PSET_MIN has a specific syntax
	//
	if (rgp->rg_slm_cpu_min != SCHA_SLM_RG_PSET_MIN_DEFAULT) {
		slm_err = check_slm_cpu(rgp->rg_slm_cpu_min);
		if (slm_err != NULL) {
			ucmm_print("rg_property_check()", (char *)slm_err,
			    rgp->rg_name, SCHA_RG_SLM_PSET_MIN);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, slm_err,
			    rgp->rg_name, SCHA_RG_SLM_PSET_MIN);
			return (res);
		}
		// Check RG_SLM_PSET_MIN <= (RG_SLM_CPU_SHARES / 100)
		// When RG_SLM_PSET_TYPE is strong or weak,
		// 100 RG_SLM_CPU_SHARES is pset.max size of 1
		if (rgp->rg_slm_cpu_min > (rgp->rg_slm_cpu / 100)) {
			slm_err = REM_RG_SLM_PSET_MIN_TOO_BIG;
			ucmm_print("rg_property_check()", (char *)slm_err,
			    rgp->rg_name, SCHA_RG_SLM_PSET_MIN);
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, slm_err,
			    rgp->rg_name, SCHA_RG_SLM_PSET_MIN);
			return (res);
		}
	}
	// SC SLM addon end

	// Validate rg_affinities property
	res = rg_affinities_validate(rgp, affinities, rg_create, affs_changed,
	    des_prims_changed, nodelist_changed, auto_start_changed, rg_list);

	//
	// If nodelist property has not changed. We need not
	// verify it again because it might lead to extraneous
	// warning messages confusing the user.
	//
	if (!nodelist_changed)
		return (res);

	for (nodeidlist_t *nl = rgp->rg_nodelist; nl != NULL;
	    nl = nl->nl_next) {
		if (nl->nl_zonename == NULL)
			continue;	// global zone. skip
		LogicalNode *ln = lnManager->findLogicalNode(nl->nl_nodeid,
		    nl->nl_zonename);
		ASSERT(ln != NULL);

		if (!ln->isConfigured()) {
			rgm_format_errmsg(&res,
			    RWM_UNCONFIGURED_ZONES);
			break;
		}
	}


	return (res);
}


//
// implicit_dependencies_sanity_check
//
// This function calls detect_r_dependencies_cycles
// to check if any of the loghost/sharedaddr R is dependent
// on non-loghost/sharedaddr R (within the same rg).
//
static scha_errmsg_t
implicit_dependencies_sanity_check(const rglist_p_t rgp) {
	rlist_t		*rsrc;
	rgm_resource_t	*r;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	//
	// Essentially this should be called only for
	// Rs which are loghost/sharedaddr. We can skip
	// that check here as this will be checked in
	// function detect_r_dependencies_cycles
	//
	for (rsrc = rgp->rgl_resources; rsrc != NULL;
	    rsrc = rsrc->rl_next) {
		r = rsrc->rl_ccrdata;
		// set the implicit_dep_check flag to true.
		// The other skip flags can be set to false.
		if ((res = detect_r_dependencies_cycles(r,
		    B_FALSE, B_FALSE, B_FALSE, B_FALSE,
		    B_TRUE, B_TRUE)).err_code != SCHA_ERR_NOERR)
			return (res);
	}
	return (res);
}

//
// run_init_method -
//
// The name of this function is historical -- it doesn't actually
// run init. All it does is detect whether or not init needs to be run.
//
// Note that, as an optimization, we don't process the intention unless at
// least one resource in the RG registers an INIT method.
//
// Assume that this function is called under the following conditions:
// - caller holds the rgm state lock
// - rgl_switching of the specified rg is set to SW_UPDATING
// - rg is in managed state
//
static void
run_init_method(const rglist_p_t rgp, bool &need_run_init)
{
	rlist_p_t	rp;
	need_run_init = false;

	ucmm_print("RGM", NOGET("in run_init_method\n"));

	CL_PANIC(rgp != NULL);

	ucmm_print("run_init_method()",
	    NOGET("RG <%s> Processing\n"), rgp->rgl_ccr->rg_name);

	//
	// Iterate through each resource in the RG, checking if any registers
	// an INIT method (so that we know whether or not to process the
	// intention).
	//
	for (rp = rgp->rgl_resources; rp != NULL; rp = rp->rl_next) {
		//
		// Find out whether this resource needs to run INIT.
		//
		if (is_method_reg(rp->rl_ccrtype, METH_INIT, NULL)) {
			need_run_init = true;
		}
	}
}


// check_pos_affinity_managed() -
//
// If the given RG is managed and has a positive affinity for other rg that is
// unmanaged and which does not appear in the list 'rglistp', the edit should
// be vetoed (if the affinity is strong) or
// a warning message issued (if the affinity is weak).  This function is
// called when RG_affinities of a managed RG are edited, or
// when an RG becomes managed or unmanaged.
// When unmanaging an RG, the second argument is the affinitents, otherwise,
// the affinities is passed.
// The last arg is a namelist which contains the failed RGs.

scha_errmsg_t
check_pos_affinity_managed(const rgm_rg_t *prg,
    const rgm_affinities_t *affinities,
    boolean_t managed_rg, const rglist_p_t *rglistp, namelist_t *failed_rglist)
{
	namelist_t *affp;
	rglist_p_t aff_rg;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	for (affp = affinities->ap_strong_pos; affp; affp = affp->nl_next) {
		if (is_enhanced_naming_format_used(affp->nl_name))
			continue;
		// Skip failed RGs.
		if (namelist_exists(failed_rglist,
		    affp->nl_name)) {
			continue;
		}

		if (is_rg_in_list(affp->nl_name, rglistp))
			continue;
		aff_rg = rgname_to_rg(affp->nl_name);

		// aff_rg is an affinitee of prg
		if (managed_rg && aff_rg->rgl_ccr->rg_unmanaged) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RG_AFF_MANAGED,
			    prg->rg_name, affp->nl_name);
			return (res);
		}

		// aff_rg is an affinitent of prg
		if (!managed_rg && !aff_rg->rgl_ccr->rg_unmanaged) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RG_AFF_MANAGED,
			    affp->nl_name, prg->rg_name);
			return (res);
		}
	}

	for (affp = affinities->ap_weak_pos; affp; affp = affp->nl_next) {
		if (is_enhanced_naming_format_used(affp->nl_name))
			continue;
		if (is_rg_in_list(affp->nl_name, rglistp))
			continue;
		aff_rg = rgname_to_rg(affp->nl_name);

		// aff_rg is an affinitee of prg
		if (managed_rg && aff_rg->rgl_ccr->rg_unmanaged) {
			rgm_format_errmsg(&res, RWM_RG_AFF_MANAGED,
			    prg->rg_name, affp->nl_name);
			ucmm_print("RGM", NOGET("check_pos_affinity_managed(): "
			    "WARNING: RG <%s> has a weak positive affinity for "
			    "unmanged RG <%s>\n"),
			    prg->rg_name, affp->nl_name);
		}

		// aff_rg is an affinitent of prg
		if (!managed_rg && !aff_rg->rgl_ccr->rg_unmanaged) {
			rgm_format_errmsg(&res, RWM_RG_AFF_MANAGED,
			    affp->nl_name, prg->rg_name);
			ucmm_print("RGM", NOGET("check_pos_affinity_managed(): "
			    "WARNING: managed RG <%s> has a weak positive "
			    "affinity for RG <%s>\n"),
			    affp->nl_name, prg->rg_name);
		}
	}

	return (res);
}


// SC SLM addon start

//
// RG_SLM_PSET_MIN check.
// An integer, can be 0, max value is (FSS_MAXSHARES / 100)
//
static const char *
check_slm_cpu(int x)
{

	if (x > (FSS_MAXSHARES / 100)) {
		return (REM_RG_SLM_CPU_TOO_BIG);
	}
	if (x < 0) {
		return (REM_RG_SLM_CPU_GARBAGE);
	}
	return (NULL);
}


//
// RG_SLM_CPU_SHARES check.
// An integer, cannot be greater than FSS_MAXSHARES
// 0 is refused.
// Return value is error, NULL is no error
//
static const char *
check_slm_cpu_shares(int x)
{
	if (x > FSS_MAXSHARES) {
		// Solaris max shares is 65535
		return (REM_RG_SLM_CPU_TOO_BIG);
	}
	if (x < 0) {
		return (REM_RG_SLM_CPU_GARBAGE);
	}
	if (x == 0) {
		// 0 refused
		return (REM_RG_SLM_CPU_ZERO);
	}
	return (NULL);
}


//
// SCSLM generated project are based on RG name.
// Refuse to set RG_SLM_TYPE=automated if SCSLM generated
// name is to long, Solaris projects have a name length limit.
//
static const char *
check_slm_rgname_length(const rgm_rg_t *rgp)
{
	char		*s = rgp->rg_name;

	if (strlen(s) > SCSLM_RG_NAME_MAX_LENGTH) {
		return (REM_RG_SLM_NAME_LENGTH);
	}
	return (NULL);
}


//
// RG_SLM_TYPE=automated is only accepted after
// checking that other RG names with RG_SLM_TYPE=automated do not
// differ only about '-' or '_'
// see details in slm_rgm_name_strcmp() comments
//
static const char *
check_slm_rgname_clash(const rgm_rg_t *rgp)
{
	rglist_p_t	rglp;

	//
	// for each SLM automated RG, check for names conflicts about
	// '-' and '_'. See details in slm_rgm_name_strcmp comments.
	//
	for (rglp = Rgm_state->rgm_rglist; rglp != NULL;
	    rglp = rglp->rgl_next) {
		// Skip ourselves
		if (strcmp(rglp->rgl_ccr->rg_name, rgp->rg_name) == 0) {
			continue;
		}
		if (strcmp(rglp->rgl_ccr->rg_slm_type,
		    SCHA_SLM_TYPE_AUTOMATED) == 0) {
			if (slm_rgm_name_strcmp(rglp->rgl_ccr->rg_name,
			    rgp->rg_name) == 0) {
				return (REM_RG_SLM_NAME_CLASH);
			}
		}
	}
	return (NULL);
}


// rg_slm_sanity_checks() :
// 1- verify that the nodelist contains only zones if RG_SLM_PSET_TYPE
// is set to weak or strong.  global zones are
// not yet supported.
// 2- verify that all the other RGs running in the same zone are
// configured with the same value for RG_SLM_TYPE and RG_SLM_PSET_TYPE
// than the one configured for this particular RG.
static scha_errmsg_t
rg_slm_sanity_checks(const rgm_rg_t *cur_rgp,
    const rgm::idl_string_seq_t &rg_list)
{
	scha_errmsg_t ret = {SCHA_ERR_NOERR, NULL};
	char			*rg_slm_type;

	if (cur_rgp->rg_slm_type == NULL ||
	    cur_rgp->rg_slm_pset_type == NULL) {
		// no ckeck needed
		return (ret);
	}

#if SOL_VERSION < __s10
	if (strcmp(cur_rgp->rg_slm_type, SCHA_SLM_TYPE_MANUAL) == 0) {
		// no ckeck needed
		return (ret);
	}
	if (strcmp(cur_rgp->rg_slm_type,
	    SCHA_SLM_TYPE_AUTOMATED) == 0 &&
	    strcmp(cur_rgp->rg_slm_pset_type,
	    SCHA_SLM_PSET_TYPE_DEFAULT) != 0) {
		ret.err_code = SCHA_ERR_PROP;
		rgm_format_errmsg(&ret, REM_SLM_NO_DEDICATED,
		    cur_rgp->rg_name);
	}
	if (strcmp(cur_rgp->rg_slm_type,
	    SCHA_SLM_TYPE_AUTOMATED) == 0 &&
	    cur_rgp->rg_slm_cpu_min != SCHA_SLM_RG_PSET_MIN_DEFAULT) {
		ret.err_code = SCHA_ERR_PROP;
		rgm_format_errmsg(&ret, REM_SLM_NO_PSET_MIN,
		    cur_rgp->rg_name);
	}
	return (ret);
#else
	/*
	 * browse the nodelist to ensure that the nodelist
	 * only contains zones if slm_pset_type is set to
	 * weak or strong
	 */
	if (strcmp(cur_rgp->rg_slm_pset_type,
	    SCHA_SLM_PSET_TYPE_DEFAULT) != 0) {
		for (nodeidlist_t *cur_nlp = cur_rgp->rg_nodelist;
			cur_nlp != NULL;
			cur_nlp = cur_nlp->nl_next) {

			if (cur_nlp->nl_zonename == NULL) {
				ret.err_code = SCHA_ERR_PROP;
				rgm_format_errmsg(&ret, REM_SLM_PSET_AND_ZONE,
				    cur_rgp->rg_name);
				goto finished; //lint !e801
			}
		}
	}

	/*
	 * browse the nodelist of each RG and if an RG uses
	 * the same lni than the current RG, verify that the
	 * RG_SLM_PSET_TYPE values are compatible.
	 * compatibilty means that weak does not run
	 * in the same zone as strong.
	 * Verify that in a same local zone, RG_SLM_type is
	 * the same value for all resource group, mixing RG_SLM_type
	 * 'automated' and 'manual' is not allowed in a same local zone.
	 */

	rg_slm_type = cur_rgp->rg_slm_type;

	// for each RG
	for (rglist_p_t rglp = Rgm_state->rgm_rglist;
	    rglp != NULL; rglp = rglp->rgl_next) {
		// Skip if the rg was provided as part of input operands.
		if (is_rg_in_update_list(rglp->rgl_ccr->rg_name, rg_list)) {
			continue;
		}

		// for each LNI
		for (nodeidlist_t *nlp = rglp->rgl_ccr->rg_nodelist;
		    nlp != NULL; nlp = nlp->nl_next) {
			// Skip ourselves
			if (strcmp(rglp->rgl_ccr->rg_name,
			    cur_rgp->rg_name) == 0) {
				continue;
			}
			// Is it in the current RG LNI list?
			for (nodeidlist_t *cur_nlp = cur_rgp->rg_nodelist;
			    cur_nlp != NULL; cur_nlp = cur_nlp->nl_next) {
				if (nlp->nl_lni == cur_nlp->nl_lni) {
					if (strcmp(
					    rglp->rgl_ccr->rg_slm_pset_type,
					    SCHA_SLM_PSET_TYPE_DEFAULT) != 0 &&
					    strcmp(cur_rgp->rg_slm_pset_type,
						SCHA_SLM_PSET_TYPE_DEFAULT)
						!= 0 &&
					    strcmp(
						rglp->rgl_ccr->rg_slm_pset_type,
						cur_rgp->rg_slm_pset_type)
						!= 0) {
						ret.err_code = SCHA_ERR_PROP;
						rgm_format_errmsg(&ret,
					REM_SLM_PSET_TYPE_IDENTICAL_IN_ZONE,
						    cur_rgp->rg_name,
						    rglp->rgl_ccr->rg_name,
						    (nlp->nl_zonename == NULL ?
						    "global" :
						    nlp->nl_zonename));
						goto finished; //lint !e801
					}
					if (nlp->nl_zonename != NULL &&
					    strcmp(rglp->rgl_ccr->rg_slm_type,
					    rg_slm_type) != 0) {
						ret.err_code = SCHA_ERR_PROP;
						rgm_format_errmsg(&ret,
						    REM_SLM_TYPE_DIFF_IN_ZONE,
						    cur_rgp->rg_name,
						    rglp->rgl_ccr->rg_name,
						    nlp->nl_zonename);
						goto finished; //lint !e801
					}
				}
			}
		}
	}

finished:

	return (ret);

#endif
}


// slm_rgm_name_strcmp():
// like strcmp, but '-' and '_' are equivalent.
// Solaris 10 does not accepts to create project names with '-'.
// As SLM generates project for RG under SLM control (RG_SLM_type=automated)
// based on project name, this is solved by generating:
// for RG_XX: SCSLM_RG_XX
// for RG-YY: SCSLM_RG_YY
// but seting RG_SLM_TYPE to automated for both RG_XX and RG-XX is not
// allowed, as resulting SCSLM generated project name would be the same.
// In this case, refuse to change the RG_SLM_TYPE property to automated.
static int
slm_rgm_name_strcmp(const char *s1, const char *s2)
{
	if (s1 == NULL || s2 == NULL) {
		return (1);
	}
	if (strlen(s1) != strlen(s2)) {
		return (1);
	}
	while (*s1 != '\0') {
		if ((*s1 == '-') || (*s1 == '_')) {
			if ((*s2 != '-') && (*s2 != '_')) {
				return (1);
			}
		} else {
			if (*s1 != *s2) {
				return (1);
			}
		}
		s1++;
		s2++;
	}
	if (*s1 != *s2) {
		return (1);
	}
	return (0);
}
// SC SLM addon end

//
// idl_scrgadm_rg_update_prop
//
// Simply a wrapper for scrgadm_rg_update_prop_helper.
// Converts the rg_update_prop_args structure to a version 1
// structure and calls scrgadm_rg_update_prop_helper.
//
void
rgm_comm_impl::idl_scrgadm_rg_update_prop(
	const rgm::idl_rg_update_prop_args &rgup_args,
	rgm::idl_regis_result_t_out rgup_res, Environment &)
{
	//
	// Construct a v1 version of the rg_update_props
	//
	rgm::idl_rg_update_prop_args_v1 rgup_args_v1;
	rg_update_prop_v0_to_v1(rgup_args_v1, rgup_args);
	scrgadm_rg_update_prop_helper(rgup_args_v1,
		rgup_res);
}

//
// New version of the function for supporting the new
// rg_update_prop_args struct. Simply a wrapper for
// scrgadm_rg_update_prop_helper.
//
void
rgm_comm_impl::idl_scrgadm_rg_update_prop_v1(
	const rgm::idl_rg_update_prop_args_v1 &rgup_args,
	rgm::idl_regis_result_t_out rgup_res, Environment &)
{
	scrgadm_rg_update_prop_helper(rgup_args,
		rgup_res);
}


//
// rg_update_prop_v0_to_v1
//
// Converts a version 0 idl_rg_update_prop_args structure to a version 1
// idl_rg_update_prop_args structure.
//
static void
rg_update_prop_v0_to_v1(rgm::idl_rg_update_prop_args_v1
	&rgup_v1_args,  const rgm::idl_rg_update_prop_args &rgup_args)
{

	//
	// OK to assign here because the string (really _string_field)
	// operator= performs a deep copy.
	//
	rgup_v1_args.idlstr_rg_name = rgup_args.idlstr_rg_name;
	rgup_v1_args.locale = rgup_args.locale;
	rgup_v1_args.idlstr_zonename = rgup_args.idlstr_zonename;
	rgup_v1_args.nodeid = rgup_args.nodeid;
	rgup_v1_args.caller_flag = rgup_args.caller_flag;
	rgup_v1_args.flags = rgup_args.flags;

	// copy the rg property list
	nvseq_copy(rgup_args.prop_val, rgup_v1_args.prop_val);

	// initialize the rglist to NULL
	rgup_v1_args.rglist = NULL;
}
