/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_rg.cc	1.39	08/06/20 SMI"

//
// rgm_rg.cc
//
// The call_xxx() functions for RGs. Called by the receptionist thread to the
// president node. The caller provides storage for both arguments.
// IDL errors or other communication errors are returned as the return
// value from this function. The IDL invocation on the president node
// fills in the result structure.
//
// Important Note: Assignment of const char * to an IDL String_field
// (idlstr_xxx is our RGM naming convention) makes a copy of the string,
// which is deleted by the [IDL compiler-generated] destructor upon
// leaving the function.  Assignment of char * to idlstr does NOT
// automatically make a copy of the string, but the destructor still does
// a delete upon leaving the function.  To simplify coding and avoid
// coding errors, we ALWAYS make a copy of the string via new_str()
// regardless of whether the right hand side is a char * or a const char *.
// The right thing will happen.  Upon leaving the function, the
// destructor will delete the new-ed memory and there will be no memory
// leaks.
//
// Rule: Subroutine MUST make copies (new_str)of input strings before
// assigning them to String_fields.  However, the subroutine doesn't need
// to explicitly delete them, because the [IDL compiler-generated]
// destructor frees them upon leaving the subroutine.
//

#include <rgm_proto.h>
#include <rgm_state.h>
#include <rgm/rgm_call_pres.h>

#include <rgm/rgm_recep.h>

#ifdef EUROPA_FARM
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_pres.h>
#endif

// List of functions needed for public API

scha_errmsg_t
call_scha_rg_get_state(const rg_get_state_args *rggs_args,
    get_result_t *rggs_res)
{
#ifdef EUROPA_FARM
	rgmx_rg_get_state_args arg;
	rgmx_get_result_t result_v;
	CLIENT *clnt = NULL;
	enum clnt_stat rpc_res;
#else
	rgm::idl_rg_get_state_args arg;
	rgm::idl_get_result_t_var result_v;
	rgm::rgm_comm_var prgm_pres_v;

	Environment e;
#endif
	const char *zonename;

	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};


	arg.idlstr_rg_name = new_str(rggs_args->rg_name);
	if (rggs_args->node_name == NULL || rggs_args->node_name[0] == '\0') {

		if (rggs_args->zonename == NULL ||
		    rggs_args->zonename[0] == '\0')
			zonename = NULL;
		else
			zonename = rggs_args->zonename;
		// The caller didn't pass in the node name,
		// so use the current node.
		arg.idlstr_node_name = lnManager->computeLn(
		    get_local_nodeid(), zonename);
	} else
		arg.idlstr_node_name = new_str(rggs_args->node_name);

#ifndef EUROPA_FARM
	if (allow_inter_cluster())
		prgm_pres_v = rgm_comm_getpres_zc(ZONE);
	else
		prgm_pres_v = rgm_comm_getpres();
#endif
	while (1) {
#ifdef EUROPA_FARM
		clnt = rgmx_comm_getpres(ZONE);
		if (clnt != NULL) {
			// In case of RPC timeout, we assume that the president
			// died and we retry the call to the new president.
			bzero(&result_v, sizeof (rgmx_get_result_t));
			rpc_res = rgmx_scha_rg_get_state_1(&arg,
				&result_v, clnt);
			delete [] arg.idlstr_rg_name;
			delete [] arg.idlstr_node_name;
			if ((rpc_res != RPC_SUCCESS) &&
			    (rpc_res != RPC_TIMEDOUT)) {
				// some unexpected RPC error
				res.err_code = SCHA_ERR_ORB;
				xdr_free((xdrproc_t)xdr_rgmx_get_result_t,
					(char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			if (rpc_res == RPC_SUCCESS) {
				if ((rggs_res->ret_code = result_v.ret_code) ==
				    SCHA_ERR_NOERR) {
					rggs_res->seq_id = strdup_nocheck(
					    result_v.idlstr_seq_id);
					if (rggs_res->seq_id == NULL) {
						res.err_code = SCHA_ERR_NOMEM;
					} else {
						copy_string_array(
						    result_v.value_list,
						    &rggs_res->value_list);
						res.err_code = SCHA_ERR_NOERR;
					}
				} else {
					res.err_code = (scha_err_t)
						result_v.ret_code;
				}
				if ((const char *)result_v.idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v.idlstr_err_msg);
				}
				xdr_free((xdrproc_t)xdr_rgmx_get_result_t,
					(char *)&result_v);
				rgmx_comm_freecnt(clnt);
				return (res);
			}
			xdr_free((xdrproc_t)xdr_rgmx_get_result_t,
				(char *)&result_v);
			rgmx_comm_freecnt(clnt);
		}
#else
		if (!CORBA::is_nil(prgm_pres_v)) {
			prgm_pres_v->idl_scha_rg_get_state(arg, result_v, e);
			res = except_to_scha_err(e);

			// If president has not died, proceed.
			if (res.err_code != SCHA_ERR_CLRECONF) {
				if (res.err_code != SCHA_ERR_NOERR) {
					// some unexpected ORB error
					return (res);
				}

				if ((rggs_res->ret_code = result_v->ret_code) ==
				    SCHA_ERR_NOERR) {
					rggs_res->seq_id = strdup_nocheck(
					    result_v->idlstr_seq_id);
					if (rggs_res->seq_id == NULL) {
						res.err_code = SCHA_ERR_NOMEM;
						return (res);
					}
					(void) strseq_to_array(
					    result_v->value_list,
					    &rggs_res->value_list);
				}

				res.err_code = (scha_err_t)result_v->ret_code;
				if ((const char *)result_v->idlstr_err_msg) {
					res.err_msg =
					    strdup(result_v->idlstr_err_msg);
				}
				return (res);
			}
		}
		// President is not yet registered, or has died.  In either
		// case, a new president will be elected soon.
		// Sleep until new president is registered in name service,
		// then loop back and retry.
		//
		// Note that rgm_comm_waitforpres() is guaranteed to return
		// a new president object pointer, different from the current
		// value of prgm_pres_v.
		//
		prgm_pres_v = rgm_comm_waitforpres(prgm_pres_v);
#endif // EUROPA_FARM
	} // while (1)

	// NOTREACHED
}
