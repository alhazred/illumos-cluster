//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_array_free_list.cc	1.5	08/05/20 SMI"

#include <string.h>
#include <sys/types.h>

//
// This class is an array of pointers to objects. We maintain a free list
// of available spots in the array in a LogicalNodeset of the same size.
// The bit in the nodeset is set if the spot in the array is unused, and
// is unset when the spot in the array is in use.
//

template <class OBJ>
ArrayFreeList<OBJ>::ArrayFreeList() : nbEntries(0), curEntries(0), objs(NULL)
{
	initialize(LogicalNodeset::maxLni());
}

template <class OBJ>
void
ArrayFreeList<OBJ>::initialize(uint_t entries)
{
	// Allocate memory for array of pointers & free list
	objs = new OBJ* [entries + 1];
	free_list = new LogicalNodeset();

	// set all ptrs to NULL
	memset(objs, 0, sizeof (OBJ *) * (entries + 1));

	// Set every bit in the LogicalNodeset
	// (Can't use ~= because we didn't define it)
	*free_list = ~(*free_list);

	nbEntries = entries;
}


//
// Add n to the current size of the ArrayFreeList.
// n should be a multiple of BITSPERWORD (currently 32).
//
template <class OBJ>
void
ArrayFreeList<OBJ>::grow(uint_t n)
{
	size_t oldsize = (nbEntries + 1) * sizeof (OBJ *);
	size_t newsize = (nbEntries + n + 1) * sizeof (OBJ *);
	OBJ** new_objs = new OBJ* [nbEntries + n + 1];

	// initialize new array to NULL
	(void) memset(new_objs, 0, newsize);

	// copy old array into new
	(void) memcpy(new_objs, objs, oldsize);

	delete [] objs;
	objs = new_objs;

	//
	// Save the negation of the current free list.  This will create
	// a LogicalNodeset of the current (small) size, which we can use
	// to update the new (larger) free_list.
	//
	LogicalNodeset notFree = ~(*free_list);

	//
	// Create new free list, specifying the new larger size in bits.
	// This also increases the global value of maxLni().
	//
	LogicalNodeset *new_free_list = new LogicalNodeset(
	    LogicalNodeset::maxLni() + n);
	//
	// Set every bit in the new LogicalNodeset
	// (Can't use ~= because we didn't define it)
	//
	*new_free_list = ~(*new_free_list);
	// subtract out all the non-free elements from the old free_list
	*new_free_list -= notFree;

	delete free_list;
	free_list = new_free_list;
	nbEntries += n;
}


//
// Insert an object in the array
//
// . If an index 'reservedIndex' is specified:
//   Directly use the index.
//   Allocate additional LNIs, if necessary, to accommodate the index.
//   Value of ngrow parameter is ignored.
//
// . If reserved index is not provided:
//   Pick first entry in the free list.
//   If the free list is empty and the 'ngrow' parameter is zero, return zero.
//   (Caller may do garbage collection of LNIs and retry).
//   If the free list is empty and ngrow is positive, allocate 'ngrow'
//   additional LNIs.  ngrow should be a multiple of BITSPERWORD (currently 32).
//
template <class OBJ>
uint_t
ArrayFreeList<OBJ>::insertObject(OBJ *obj, uint_t reservedIndex, uint_t ngrow)
{
	uint_t idx;

	if (reservedIndex == NO_RESERVED_INDEX) {
		idx = free_list->nextLniSet(1);
		if (idx == 0 && ngrow > 0) {
			grow(ngrow);
			idx = free_list->nextLniSet(1);
			// assert for debugging
			ASSERT(idx > 0);
		}
		if (idx == 0) {
			return (0);
		}
	} else {
		uint_t curr_max = LogicalNodeset::maxLni();
		if (reservedIndex > curr_max) {
			// Need to grow LNIs to accommodate index
			uint_t new_max = (uint_t)NBWORDS(reservedIndex) *
			    BITSPERWORD;
			grow(new_max - curr_max);
		}

		CL_PANIC((reservedIndex > 0) && (reservedIndex <= nbEntries) &&
		    free_list->containLni(reservedIndex));
		idx = reservedIndex;
	}

	objs[idx] = obj;
	free_list->delLni(idx);

	curEntries++;
	return (idx);
}

//
// Remove an object from the array and add it back to the free list.
// Return a pointer to the object that was removed.
//
template <class OBJ>
OBJ*
ArrayFreeList<OBJ>::removeObject(uint_t idx)
{
	OBJ* obj = NULL;

	// assert that we actually have this object
	CL_PANIC(!free_list->containLni(idx));

	obj = objs[idx];
	objs[idx] = NULL;

	// add this spot back to the free list
	free_list->addLni(idx);

	curEntries--;
	return (obj);
}

template <class OBJ>
OBJ *
ArrayFreeList<OBJ>::getObject(uint_t idx)
{
	//
	// Note, it is OK for idx to be in free list; objs[idx] will be
	// NULL for that case.
	//
	return (objs[idx]);
}
