//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//


#pragma ident	"@(#)rgm_launch_method.cc	1.156	09/04/22 SMI"

#include <sys/os.h>
#include <rgm_state.h>
#include <rgm_proto.h>
#include <rgm_api.h>
#include <wait.h>
#include <rgm/rgm_cnfg.h>
#include <rgm/rgm_msg.h>
#include <rgm/rgm_errmsg.h>
#include <rgm/rgm_util.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>
#include <rpc/rpc.h>
#include <sys/param.h>
#include <sys/cl_events.h>
#include <stdlib.h>
#include <rgm_logical_node.h>
#ifdef SC_SRM
#include <project.h>
#endif

//
// is_method_ssm
//
// Return true if the given method has a corresponding ssm function call;
// otherwise return false.
//
static boolean_t
is_method_ssm(method_t m)
{
	boolean_t	result;

	switch (m) {
	case METH_START:
	case METH_VALIDATE:
	case METH_UPDATE:
	case METH_INIT:
	case METH_FINI:
	case METH_BOOT:
	case METH_MONITOR_START:
	case METH_MONITOR_STOP:
	case METH_MONITOR_CHECK:
		result = B_TRUE;
		break;

	case METH_STOP:
	case METH_PRENET_START:
	case METH_POSTNET_STOP:
	default:
		result = B_FALSE;
		break;
	}

	return (result);
}

//
// get_method_timeout_tag
//
// Given the resource type and method name,
// returns the method timeout tag for passing to get_rs_prop_val().
//
static const char *
get_meth_timeout_tag(method_t method_id)
{
	switch (method_id) {
	case METH_START:
		return (SCHA_START_TIMEOUT);
	case METH_STOP:
		return (SCHA_STOP_TIMEOUT);
	case METH_VALIDATE:
		return (SCHA_VALIDATE_TIMEOUT);
	case METH_UPDATE:
		return (SCHA_UPDATE_TIMEOUT);
	case METH_INIT:
		return (SCHA_INIT_TIMEOUT);
	case METH_FINI:
		return (SCHA_FINI_TIMEOUT);
	case METH_BOOT:
		return (SCHA_BOOT_TIMEOUT);
	case METH_MONITOR_START:
		return (SCHA_MONITOR_START_TIMEOUT);
	case METH_MONITOR_STOP:
		return (SCHA_MONITOR_STOP_TIMEOUT);
	case METH_MONITOR_CHECK:
		return (SCHA_MONITOR_CHECK_TIMEOUT);
	case METH_PRENET_START:
		return (SCHA_PRENET_START_TIMEOUT);
	case METH_POSTNET_STOP:
		return (SCHA_POSTNET_STOP_TIMEOUT);
	default:
		break;
	}

	return (NULL);
}

//
// rt_method_timeout
//
// Given a resource type and the method name, returns the timeout
// value for that method.
// The method timeout value is getting from R
//
scha_errmsg_t
rt_method_timeout(rlist_p_t r, name_t methname, method_t method_id,
    int *timeout)
{
	scha_prop_type_t	prop_type;
	namelist_t		*val_array = NULL;
	const char			*tag;
	std::map<rgm::lni_t, name_t> val_str;
	rdeplist_t		*val_array_rdep = NULL;
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	boolean_t		need_free = B_FALSE;

	// first get the method timeout tag for this method type
	if ((tag = get_meth_timeout_tag(method_id)) == NULL) {
		ucmm_print("RGM", NOGET("rt_method_timeout: "
		    "cannot get method <%s> tag for resource <%s>\n"),
		    methname, r->rl_ccrdata->r_name);
		res.err_code = SCHA_ERR_METHODNAME;
		goto finished; //lint !e801
	}

	if (get_rs_prop_val(tag, r->rl_ccrdata,
	    &prop_type, val_str, &val_array, &need_free,
	    &val_array_rdep).err_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM", NOGET("rt_method_timeout: "
		    "cannot get method <%s> timeout for resource <%s>\n"),
		    methname, r->rl_ccrdata->r_name);
		res.err_code = SCHA_ERR_METHODNAME;
	} else if (sscanf(val_str[0], "%d", timeout) != 1) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The indicated resource method timeout, as stored in the
		// CCR, is not an integer value. This might indicate
		// corruption of CCR data or rgmd in-memory state. The method
		// invocation will fail; depending on which method was being
		// invoked and the Failover_mode setting on the resource, this
		// might cause the resource group to fail over or move to an
		// error state.
		// @user_action
		// Use clresource show -v to examine resource properties. If
		// the values appear corrupted, the CCR might have to be
		// rebuilt. If values appear correct, this might indicate an
		// internal error in the rgmd. Contact your authorized Sun
		// service provider for assistance in diagnosing and
		// correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "ERROR: method <%s> timeout for resource <%s> is "
		    "not an integer"),
		    methname, r->rl_ccrdata->r_name);
		sc_syslog_msg_done(&handle);
		res.err_code = SCHA_ERR_INTERNAL;
	}
finished:
	/*
	 * If need_free is TRUE, need to free the buffer 'val_str'
	 */
	if (need_free && val_str[0] != NULL) {
		free(val_str[0]);
	}
	return (res);
}


//
// is_r_globalzone
//
// Return a boolean indicating if the given resource's methods are to be
// executed in the global zone even if the RG is configured to run in a
// non-global zone.
//
// Returns B_TRUE if the RTR file declares Global_zone=TRUE, or if the RT
// is a network address RT (which defaults to Global_zone=TRUE).  However,
// if the Global_zone_override resource property exists and is set false,
// this overrides the Global_zone=TRUE setting.
//
boolean_t
is_r_globalzone(rlist_p_t r)
{
	rgm_property_list_t	*pp;

	//
	// Lookup the RT's Global_zone property.  If it is false, we're done.
	//
	if (!r->rl_ccrtype->rt_globalzone) {
		return (B_FALSE);
	}

	//
	// Global_zone is TRUE for the resource type.
	// Lookup the resource's Global_zone_override property.
	// If the property is undeclared or set to true, there is no override.
	// If the property is set to false, then it overrides the
	// Global_zone=TRUE setting of the RT.
	//
	pp = rgm_find_property_in_list(SCHA_GLOBAL_ZONE_OVERRIDE,
	    r->rl_ccrdata->r_properties);

	if (pp == NULL) {
		return (B_TRUE);
	} else if (((rgm_value_t *)pp->rpl_property->rp_value)->rp_value[0] ==
	    NULL) {
		return (B_TRUE);
	} else {
		return (str2bool(((rgm_value_t *)pp->rpl_property->rp_value)->
		    rp_value[0]));
	}
}

//
// this function checks if the rg that the resource method corresponds to, is
// undergoing fast quiesce, and the method is one of those which can be killed.
//
static boolean_t
quiesce_early_return(char *fed_tag)
{
	char tag[MAXFEDTAGLEN + 1];
	char *rgname = NULL;
	method_t mt = METH_START;
	(void) strcpy(tag, fed_tag);
	parse_fed_tag(tag, NULL, &rgname, NULL, NULL, &mt);
	//
	// we don't kill boot/init/fini methods anyway because
	// they don't have a failure state to go to.
	//
	if (mt == METH_INIT ||
	    mt == METH_BOOT ||
	    mt == METH_FINI ||
	    mt == METH_UPDATE) {
		ucmm_print("quiesce_early_return", NOGET("%s: fast quiesce not "
		    "allowed. return false."), fed_tag);
		return (B_FALSE);
	}
	//
	// currently blanket check on rg works because resource specific quiesce
	// is not supported. Only fast_quiesce prevents method from completing.
	//
	if (!is_fast_quiesce_running(rgname)) {
		ucmm_print("quiesce_early_return", NOGET("%s: fast quiesce "
		    "not running on %s. return false."), fed_tag, rgname);
		return (B_FALSE);
	}
	ucmm_print("quiesce_early_return", "return true");
	return (B_TRUE);
}

//
// launch_method(launchargs_t *l)
// This is run in a threadpool thread which is queued in process_resource().
// The method_launch threadpool allows multiple concurrent method executions.
// The state structure lock is not held upon entering this routine.
// Grabs the lock to lookup the resource and rg structures by name, and
// to create an entry in the global list of in-flight method tags.  Releases
// the lock again before the rpc call.
//
// Makes an rpc call to the fork execution daemon to execute
// the appropriate resource method for the specified resource.
// When the rpc call returns, grabs the lock on the state structure and
// updates the resource's state (see details below).  Then releases the lock.
//
void
launch_method(launchargs_t *l, rgm::lni_t lni)
{
	char 		fed_opts_tag[ MAXFEDTAGLEN + 1 ] = { 0 };
	char 		*fed_opts_cmd_path = NULL;	// method pathname
	char 		*real_method_name = NULL;	// for scalable res
	char		*method_string = NULL;		// "START", etc.
	char		*method_name = NULL;		// "START", etc .
	// If additional command-line arguments are ever defined for methods,
	// fed_opts_argv will have to be made correspondingly larger:
	char 		*fed_opts_argv[18];
	char 		fed_argv0[MAXPATHLEN + 32];	// string for argv[0]
	fe_cmd		fed_cmd = {0};
	fe_run_result	fed_result = {FE_OKAY, 0, 0, SEC_OK, NULL};
	char 		**fed_opts_env;
	rlist_p_t	r;
	rglist_p_t	rg;
	rgm::rgm_r_state	rstate = l->l_succstate;
	uint_t		rc = 0;
	char		*err_str;
	boolean_t	method_failed = B_TRUE;
	hrtime_t	method_start_time, method_end_time, method_run_time;
	int		timeoutpc;
	cl_event_result_t	cl_result_code = CL_RC_OK;
	methodinvoc_t	*mip = NULL;
	uint_t		i;
	rgm_direction_t	rg_direction = RGM_DIR_NULL;	// RG's "direction"
							// (starting, stopping)
	sc_syslog_msg_handle_t handle;
	char		*extra_msg;
	rgm::rgm_rg_state rgx_state;
	char		*nodename = NULL;
	char		*zonename = NULL;
	// If the method has no ssm function call, set scalable back to false
	if (!is_method_ssm(l->l_method)) {
		l->l_is_scalable = B_FALSE;
	}
	//
	// If resource is scalable, invoke the ssm wrapper in lieu of the real
	// method.  Set up fed_argv0 string as a synthetic "program name"
	// which will be assigned to argv[0] when the fed executes the
	// method (or the ssm_wrapper).  The value of argv[0] is
	// shown by ps(1), etc.  Append the string "(scalable svc)" to
	// indicate that the scalable service wrapper program is being
	// invoked.  Otherwise, fed_argv0 is just the method program name,
	// as usual.
	//
	// If l->l_rt_meth_name is non-null, it holds the registered method
	// name.  If there is no method registered by the scalable service,
	// this parameter is NULL.
	//
	if (l->l_is_scalable) {
		method_string = strdup(rt_method_string(l->l_method));
		(void) strcpy(fed_argv0,
		    l->l_rt_meth_name ? l->l_rt_meth_name : method_string);
		(void) strcat(fed_argv0, " (scalable svc)");
	} else {
		(void) strcpy(fed_argv0, l->l_rt_meth_name);
	}

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");

	//
	// Claim the rgm state lock
	//
	rgm_lock_state();

	// lookup lni while holding the lock
	LogicalNode *lnNode = lnManager->findObject(lni);
	CL_PANIC(lnNode != NULL);
	if (lnNode->getZonename() != NULL) {
		zonename = strdup(lnNode->getZonename());
	} else if ((ZONE != NULL) && (strcmp(ZONE, GLOBAL_ZONENAME) != 0)) {
#if SOL_VERSION >= __s10
		zonename = strdup(ZONE);
#endif
	}
	nodename = strdup(lnNode->getFullName());

	//
	// Because the rg and r structures could have changed while we
	// were not holding the lock, we must lookup them up again via the
	// name.
	//
	rg = rgname_to_rg(l->l_rgname);
	CL_PANIC(rg != NULL);

	r = rname_to_r(rg, l->l_rname);

	//
	// The resource might have been deleted while the lock was released,
	// and the resource pointer will be invalid, in which
	// case we need to exit early.  Just to be cautious, we will still
	// run the state machine.
	//
	if (r == NULL) {
		goto run_state_machine; //lint !e801
	}

#if SOL_VERSION >= __s10
	//
	// When a nonglobal zone dies, the function process_zone_death()
	// sets the state of all RGs which were not OFFLINE on that zone to
	// PENDING_OFFLINE and calls the state machine without releasing
	// the lock.  The state machine detects that the zone is dead and
	// will only launch methods that are to execute in the global zone.
	//
	// If we find the zone dead now and the method is to execute in the
	// dead zone, it means that the zone died between the time the
	// state machine released the lock and this function claimed it.
	// In that case, we just rerun the state machine without actually
	// launching the method.
	//
	// This logic depends on the fact that whenever a slave detects a
	// zone death and marks the zone status from UP to DOWN, it also
	// calls process_zone_death() while holding the state mutex.
	//
	if (!lnNode->isInCluster() && !is_r_globalzone(r)) {
		// If the zone is dead it must be non-global.
		CL_PANIC(!lnNode->isGlobalZone());
		goto run_state_machine; //lint !e801
	}
#endif

	fed_cmd.host = getlocalhostname();	// doesn't need to be freed
	if (fed_cmd.host == NULL) {
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to obtain the name of the local
		// host, causing a method invocation to fail. Depending on
		// which method is being invoked and the Failover_mode setting
		// on the resource, this might cause the resource group to
		// fail over or move to an error state.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("getlocalhostname() failed for resource <%s>, "
		    "resource group <%s>, method <%s>"),
		    l->l_rname, l->l_rgname, fed_argv0);
		goto wrapup; //lint !e801
	}

	//
	// Get the method timeout from the resource.
	//
	// If there is no method (which may occur only for a scalable RT)
	// then set the timeout to default ssm timeout.
	//
	if (l->l_rt_meth_name == NULL) {
		CL_PANIC(l->l_is_scalable);
		fed_cmd.timeout = SSM_WRAPPER_TIMEOUT;
	} else if (rt_method_timeout(r, l->l_rt_meth_name, l->l_method,
	    &fed_cmd.timeout).err_code != SCHA_ERR_NOERR) {
		//
		// SCMSGS
		// @explanation
		// Due to an internal error, the rgmd daemon was unable to
		// obtain the method timeout for the indicated resource. This
		// is considered a method failure. Depending on which method
		// was being invoked and the Failover_mode setting on the
		// resource, this might cause the resource group to fail over
		// or move to an error state.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("rgm_launch_method: failed to get "
		    "method <%s> timeout value from resource <%s>"),
		    l->l_rt_meth_name, l->l_rname);
		goto wrapup; //lint !e801
	}

	fed_cmd.security = SEC_UNIX_STRONG;
	fed_cmd.flags = 0;

#if SOL_VERSION >= __s10
	// some methods run in global zone since they need more privileges
	if (!is_r_globalzone(r) && !l->l_is_scalable) {
		fed_cmd.zonename = zonename;
	}
#endif
	//
	// construct the unique fed tag identifier from zonename, RG name, R
	// name and method enum (expressed as integer).
	// RGM should never try to launch the same method for the same resource
	// on the same zone simultaneously causing FE_DUP error (see below).
	//
	if (zonename == NULL) {
	// hosted on global zone
		(void) sprintf(fed_opts_tag, "%s.%s.%d", l->l_rgname,
		    l->l_rname, l->l_method);
	} else {
		(void) sprintf(fed_opts_tag, "%s.%s.%s.%d", zonename,
		    l->l_rgname, l->l_rname, l->l_method);
	}

	set_fed_core_props(fed_cmd, fed_opts_tag, zonename, is_r_globalzone(r));

	// assemble name of full path
	fed_opts_cmd_path = fe_method_full_name(r->rl_ccrtype->rt_basedir,
		l->l_rt_meth_name);
	if ((fed_opts_cmd_path == NULL) && !l->l_is_scalable) {
		//
		// SCMSGS
		// @explanation
		// Due to an internal error, the rgmd daemon was unable to
		// assemble the full method pathname. This is considered a
		// method failure. Depending on which method was being invoked
		// and the Failover_mode setting on the resource, this might
		// cause the resource group to fail over or move to an error
		// state.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fe_method_full_name() failed for resource <%s>, "
		    "resource group <%s>, method <%s>"),
		    l->l_rname, l->l_rgname, l->l_rt_meth_name);
		goto wrapup; //lint !e801
	}

	i = 0;
	fed_opts_argv[i++] = fed_argv0;
	fed_opts_argv[i++] = "-R";
	fed_opts_argv[i++] = l->l_rname;
	fed_opts_argv[i++] = "-T";
	fed_opts_argv[i++] = r->rl_ccrdata->r_type;
	fed_opts_argv[i++] = "-G";
	fed_opts_argv[i++] = l->l_rgname;

#if SOL_VERSION >= __s10
	// add -Z only if hosted in local zone, while method runs in global zone
	if (zonename && is_r_globalzone(r)) {
		fed_opts_argv[i++] = "-Z";
		fed_opts_argv[i++] = zonename;
	}
#endif

#ifdef SC_SRM
	// project name
	fed_cmd.project_name = get_project_name(r->rl_ccrdata, rg->rgl_ccr);
#endif
	//
	// If l->l_is_scalable is true, then the resource is scalable;
	// we will invoke the ssm_wrapper, passing it the real method
	// pathname (if any) as a command line argument.
	//
	// Append additional args for ssm_wrapper
	//
	if (l->l_is_scalable) {
		fed_opts_argv[i++] = "-m";
		fed_opts_argv[i++] = method_string;
		real_method_name = fed_opts_cmd_path;
		fed_opts_cmd_path = strdup(SSM_WRAPPER_PATH);
		if (real_method_name != NULL) {
			fed_opts_argv[i++] = "-p";
			fed_opts_argv[i++] = real_method_name;
		}
		if (zonename && !is_r_globalzone(r)) {
			fed_opts_argv[i++] = "-z";
			fed_opts_argv[i++] = zonename;
#ifdef SC_SRM
			// Set the project_name.
			fed_opts_argv[i++] = "-P";
			fed_opts_argv[i++] = fed_cmd.project_name;
			// SC SLM addon start
			fed_cmd.ssm_wrapper_zone = zonename;
			// SC SLM addon end
#endif
		}
	}

	fed_opts_argv[i] = NULL;

	//
	// set environment for method execution
	//
	if ((fed_opts_env = fe_set_env_vars()) == NULL) {
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to set up environment variables
		// for a method execution, causing the method invocation to
		// fail. Depending on which method was being invoked and the
		// Failover_mode setting on the resource, this might cause the
		// resource group to fail over or move to an error state.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing and correcting the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fe_set_env_vars() failed for "
		    "Resource <%s>, resource group <%s>, method <%s>"),
		    l->l_rname, l->l_rgname, fed_argv0);
		goto wrapup; //lint !e801
	}

	//
	// Before executing the method, create a new entry for this method
	// invocation in the global list of in-flight methods for this RG.
	// While holding the mutex, we'll also check if the RG is currently
	// frozen.  If so, we set the flag for fe_run indicating that timeouts
	// should be suspended immediately.
	//
	// We release the mutex before executing the method.
	//
	// After executing the method, we set the mi_completed field of our
	// entry in the in-flight method list to true before claiming the
	// rgm state mutex.  We must not hold the lock at this point, because
	// another thread running rgm_chg_freeze() might be holding the
	// lock and waiting for the mi_completed field to be set true.
	// For further details, see freeze_adjust_timeouts() in
	// rgm_global_res_used.cc.
	//
	// Then we grab the lock on the state structure, deal with method
	// failures, update the state, and re-run the state machine.
	//

	//
	// Update the global list of in-flight methods.
	//
	mip = (methodinvoc_t *)malloc(sizeof (methodinvoc_t));
	mip->mi_tag = strdup(fed_opts_tag);
	mip->mi_completed = B_FALSE;
	// insert new methodinvoc record at head of list for this RG
	mip->mi_next = rg->rgl_methods_in_flight;
	rg->rgl_methods_in_flight = mip;

	//
	// If the RG is currently frozen, set flag for fe_run indicating that
	// timeouts should be suspended immediately upon launch.
	//
	if (rg->rgl_is_frozen) {
		fed_cmd.flags |= FE_OPT_INITSUSPEND;
	}


#ifdef SC_SRM
	// SC SLM addon start
	fed_cmd.r_name = l->l_rname;
	fed_cmd.rg_name = l->l_rgname;
	fed_cmd.slm_flags = l->l_method;
#if SOL_VERSION >= __s10
	// some methods run in global zone since they need more privileges
	// Bypass fed SLM handling for such methods
	if (r->rl_ccrtype != NULL && is_r_globalzone(r) &&
	    zonename != NULL) {
		fed_cmd.slm_flags = METH_FEDSLM_BYPASS;
	}
#endif
	if (rg->rgl_ccr != NULL) {
		fed_cmd.rg_slm_type = rg->rgl_ccr->rg_slm_type;
		fed_cmd.rg_slm_pset_type = rg->rgl_ccr->rg_slm_pset_type;
		fed_cmd.rg_slm_cpu = rg->rgl_ccr->rg_slm_cpu;
		fed_cmd.rg_slm_cpu_min = rg->rgl_ccr->rg_slm_cpu_min;
	}
	// SC SLM addon end
#endif

	// Release the rgm state lock before launching the method.
	rgm_unlock_state();

	//
	// SCMSGS
	// @explanation
	// RGM has invoked a callback method for the named resource, as a
	// result of a cluster reconfiguration, scha_control GIVEOVER, or
	// clresourcegroup switch.
	// @user_action
	// This is an informational message; no user action is needed.
	//
	(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
	    SYSTEXT("launching method <%s> for resource <%s>, "
	    "resource group <%s>, node <%s>, timeout <%d> seconds"),
	    fed_argv0, l->l_rname, l->l_rgname, nodename,
	    fed_cmd.timeout);

	method_start_time = os::gethrtime();
	if (quiesce_early_return(fed_opts_tag)) {
		fed_result.type = FE_QUIESCED;
		//
		// SCMSGS
		// @explanation
		// The system administrator has requested a fast quiesce of the
		// reosurce group indicating that the running callback methods
		// if any, should be killed to speed its completion. Though the
		// method had already been scheduled to be launched when the
		// quiesce command was given, it hadn't actually been launched.
		// Hence RGM doesn't launch this method and instead assumes the
		// method to have failed.
		// @user_action
		// This is just an informational message generated by a
		// quiesce operation initiated by the user.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT(
		    "Execution of method <%s> on resource <%s>, node <%s> "
		    "skipped to achieve user-initiated fast quiesce of the "
		    "resource group <%s>."), fed_argv0, l->l_rname,
		    nodename, l->l_rgname);
		cl_result_code = CL_RC_TIMEOUT;
	} else if (fe_run(fed_opts_tag, &fed_cmd,
	    fed_opts_cmd_path, fed_opts_argv, fed_opts_env,
	    &fed_result) != 0) {
		//
		// SCMSGS
		// @explanation
		// The rgmd failed in an attempt to execute a method, due to a
		// failure to communicate with the rpc.fed daemon. Depending
		// on which method was being invoked and the Failover_mode
		// setting on the resource, this might cause the resource
		// group to fail over or move to an error state. If the
		// rpc.fed process died, this might lead to a subsequent
		// reboot of the node.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Save a copy
		// of the /var/adm/messages files on all nodes and contact
		// your authorized Sun service provider for assistance in
		// diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "call to rpc.fed failed for "
		    "resource <%s>, resource group <%s>, method <%s>",
		    l->l_rname, l->l_rgname, fed_argv0);
	} else {

		method_end_time = os::gethrtime();
		method_run_time = method_end_time - method_start_time;

		if (fed_cmd.timeout != 0) {
			timeoutpc = (int)(((float)method_run_time / 10000000) /
				fed_cmd.timeout);
		} else {
			timeoutpc = 0;
		}

		switch (fed_result.type) {

		// If all went well, set state of resource to success state

		case FE_OKAY:
			//
			// method_retcode now is the unprocessed return
			// from wait, which needs to be processed by client;
			//

			// rc is used here and re-used below
			rc = fed_result.method_retcode;
			if (!WIFEXITED(rc)) {
				if (WIFSIGNALED(rc)) {
					//
					// SCMSGS
					// @explanation
					// A resource method was terminated by
					// a signal, most likely resulting
					// from an operator-issued kill(1).
					// The method is considered to have
					// failed.
					// @user_action
					// No action is required. The operator
					// can choose to issue a
					// clresourcegroup command to bring
					// resource groups onto desired
					// primaries, or re-try the
					// administrative action that was
					// interrupted by the method failure.
					//
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE, SYSTEXT(
					    "Method <%s> on resource "
					    "<%s> terminated due to receipt "
					    "of signal <%d>"), fed_argv0,
					    l->l_rname, WTERMSIG(rc));
				} else if (WIFSTOPPED(rc)) {
					//
					// SCMSGS
					// @explanation
					// A resource method was stopped by a
					// signal, most likely resulting from
					// an operator-issued kill(1). The
					// method is considered to have
					// failed.
					// @user_action
					// The operator must kill the stopped
					// method. The operator can then
					// choose to issue a clresourcegroup
					// command to bring resource groups
					// onto desired primaries, or re-try
					// the administrative action that was
					// interrupted by the method failure.
					//
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE,  SYSTEXT(
					    "Method <%s> on resource <%s>, "
					    "node <%s> stopped due to receipt "
					    "of signal <%d>"), fed_argv0,
					    l->l_rname, nodename,
					    WSTOPSIG(rc));
				} else {
					//
					// SCMSGS
					// @explanation
					// A resource method terminated
					// without using an exit(2) call. The
					// rgmd treats this as a method
					// failure.
					// @user_action
					// Consult resource type
					// documentation, or contact the
					// resource type developer for further
					// information.
					//
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE, SYSTEXT(
					    "Method <%s> on resource <%s>, "
					    "node <%s> terminated abnormally"),
					    fed_argv0, l->l_rname, nodename);
				}
				cl_result_code = CL_RC_FAILED;
				break;
			}

			// rc is used above and re-used here
			rc = WEXITSTATUS(fed_result.method_retcode);
			switch (rc) {
			case EMETH_SUCCESS:
				// all went well

				//
				// SCMSGS
				// @explanation
				// RGM invoked a callback method for the named
				// resource, as a result of a cluster
				// reconfiguration, scha_control GIVEOVER, or
				// clresourcegroup switch. The method
				// completed successfully.
				// @user_action
				// This is an informational message; no user
				// action is needed.
				//
				(void) sc_syslog_msg_log(handle,
				    LOG_NOTICE, MESSAGE,
				    SYSTEXT("method <%s> completed "
				    "successfully for resource <%s>, "
				    "resource group <%s>, node <%s>, time used:"
				    " %d%% of timeout <%d seconds>"),
				    fed_argv0, l->l_rname, l->l_rgname,
				    nodename, timeoutpc, fed_cmd.timeout);
				method_failed = B_FALSE;
				cl_result_code = CL_RC_OK;
				break;

			// Currently, all nonzero exit codes are equivalent.
			case EMETH_FAILURE:
			default:
				//
				// Construct our "extra message" to print
				// for the failures of certain methods.
				//
				switch (l->l_method) {
				case METH_INIT:
					extra_msg =
					    ": this resource might need to be "
					    "initialized manually.";
					break;
				case METH_BOOT:
					extra_msg =
					    ": this resource might need to be "
					    "initialized manually on this "
					    "node.";
					break;
				case METH_FINI:
					extra_msg =
					    ": this resource might need to be "
					    "de-configured manually.";
					break;
				case METH_UPDATE:
					extra_msg =
					    ": the resource might not take "
					    "note of the update until it "
					    "is restarted.";
					break;
				case METH_START:
				case METH_STOP:
				case METH_VALIDATE:
				case METH_MONITOR_START:
				case METH_MONITOR_STOP:
				case METH_MONITOR_CHECK:
				case METH_PRENET_START:
				case METH_POSTNET_STOP:
				default:
					extra_msg = "";
					break;
				}

				//
				// SCMSGS
				// @explanation
				// A resource method exited with a non-zero
				// exit code; this is considered a method
				// failure. Depending on which method is being
				// invoked and the Failover_mode setting on
				// the resource, this might cause the resource
				// group to fail over or move to an error
				// state. Note that failures of FINI, INIT,
				// BOOT, and UPDATE methods do not cause the
				// associated administrative actions (if any)
				// to fail.
				// @user_action
				// Consult resource type documentation to
				// diagnose the cause of the method failure.
				// Other syslog messages occurring just before
				// this one might indicate the reason for the
				// failure. If the failed method was one of
				// START, PRENET_START, MONITOR_START, STOP,
				// POSTNET_STOP, or MONITOR_STOP, you can
				// issue a clresourcegroup command to bring
				// resource groups onto desired primaries
				// after fixing the problem that caused the
				// method to fail. If the failed method was
				// UPDATE, note that the resource might not
				// take note of the update until it is
				// restarted. If the failed method was FINI,
				// INIT, or BOOT, you may need to initialize
				// or de-configure the resource manually as
				// approprite on the affected node.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT(
				    "Method <%s> failed on resource <%s> in "
				    "resource group <%s> [exit code <%d>, "
				    "time used: %d%% of timeout <%d seconds>] "
				    "%s"), fed_argv0, l->l_rname, l->l_rgname,
				    rc, timeoutpc, fed_cmd.timeout, extra_msg);
				cl_result_code = CL_RC_FAILED;
				break;
			}
			break;

		/* SC SLM addon start */
		case FE_SLM_ERROR:
			//
			// SCMSGS
			// @explanation
			// Resource group is under SC SLM control and an SLM
			// error occurred. Some errors might be configuration
			// errors. Check fed SLM errors for more details.
			// @user_action
			// Move RG_SLM_type to manual and restart the resource
			// group.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s>: Execution "
			    "failed: SLM error"),
			    fed_argv0, l->l_rname);
			cl_result_code = CL_RC_FAILED;
			break;
		/* SC SLM addon end */

		case FE_SYSERRNO:
			if (strerror(fed_result.sys_errno)) {
				//
				// SCMSGS
				// @explanation
				// A resource method failed to execute, due to
				// a system error described in the message.
				// For an explanation of the error message,
				// consult intro(2). This is considered a
				// method failure. Depending on which method
				// was being invoked and the Failover_mode
				// setting on the resource, this might cause
				// the resource group to fail over or move to
				// an error state, or it might cause an
				// attempted edit of a resource group or its
				// resources to fail.
				// @user_action
				// If the error message is not
				// self-explanatory, other syslog messages
				// occurring at about the same time might
				// provide evidence of the source of the
				// problem. If not, save a copy of the
				// /var/adm/messages files on all nodes, and
				// (if the rgmd did crash) a copy of the rgmd
				// core file, and contact your authorized Sun
				// service provider for assistance.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT(
				    "Method <%s> failed to execute on "
				    "resource <%s> in resource group <%s>, "
				    "on node <%s>, error: <%s>"),
				    fed_argv0, l->l_rname, l->l_rgname,
				    nodename,
				    strerror(fed_result.sys_errno));
			} else {
				//
				// SCMSGS
				// @explanation
				// A resource method failed to execute, due to
				// a system error number identified in the
				// message. The indicated error number appears
				// not to match any of the known errno values
				// described in intro(2). This is considered a
				// method failure. Depending on which method
				// is being invoked and the Failover_mode
				// setting on the resource, this might cause
				// the resource group to fail over or move to
				// an error state, or it might cause an
				// attempted edit of a resource group or its
				// resources to fail.
				// @user_action
				// Other syslog messages occurring at about
				// the same time might provide evidence of the
				// source of the problem. If not, save a copy
				// of the /var/adm/messages files on all
				// nodes, and (if the rgmd did crash) a copy
				// of the rgmd core file, and contact your
				// authorized Sun service provider for
				// assistance.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    SYSTEXT("Method <%s> failed to execute "
				    "on resource <%s> in resource group <%s>, "
				    "on node <%s>, error: <%d>"),
				    fed_argv0, l->l_rname, l->l_rgname,
				    nodename, fed_result.sys_errno);
			}
			cl_result_code = CL_RC_FAILED;
			break;

		case FE_DUP:
			//
			// SCMSGS
			// @explanation
			// Due to an internal error, the rgmd state machine
			// has attempted to launch two different methods on
			// the same resource on the same node, simultaneously.
			// The rgmd will reject the second attempt and treat
			// it as a method failure.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes. Contact your authorized Sun service provider
			// for assistance in diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Error: duplicate method <%s> launched on resource"
			    " <%s> in resource group <%s> on node <%s>"),
			    fed_argv0, l->l_rname, l->l_rgname, nodename);
			cl_result_code = CL_RC_FAILED;
			break;

		case FE_UNKNC:
			//
			// SCMSGS
			// @explanation
			// An internal logic error in the rgmd has prevented
			// it from successfully executing a resource method.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes. Contact your authorized Sun service provider
			// for assistance in diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s>, node <%s>: unknown "
			    "command."), fed_argv0, l->l_rname, nodename);
			cl_result_code = CL_RC_FAILED;
			break;

		case FE_ACCESS:
			err_str = security_get_errormsg(fed_result.sec_errno);
			if (err_str) {
				//
				// SCMSGS
				// @explanation
				// An attempted method execution failed,
				// apparently due to a security violation;
				// this error should not occur. The last
				// portion of the message describes the error.
				// This failure is considered a method
				// failure. Depending on which method was
				// being invoked and the Failover_mode setting
				// on the resource, this might cause the
				// resource group to fail over or move to an
				// error state.
				// @user_action
				// Correct the problem identified in the error
				// message. If necessary, examine other syslog
				// messages occurring at about the same time
				// to see if the problem can be diagnosed.
				// Save a copy of the /var/adm/messages files
				// on all nodes and contact your authorized
				// Sun service provider for assistance in
				// diagnosing the problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    SYSTEXT("Method <%s> on resource <%s>, "
				    "node <%s>: authorization error: %s."),
				    fed_argv0, l->l_rname,
				    nodename, err_str);
				free(err_str);
			} else {
				//
				// SCMSGS
				// @explanation
				// An attempted method execution failed,
				// apparently due to a security violation;
				// this error should not occur. This failure
				// is considered a method failure. Depending
				// on which method was being invoked and the
				// Failover_mode setting on the resource, this
				// might cause the resource group to fail over
				// or move to an error state.
				// @user_action
				// Examine other syslog messages occurring at
				// about the same time to see if the problem
				// can be diagnosed. Save a copy of the
				// /var/adm/messages files on all nodes and
				// contact your authorized Sun service
				// provider for assistance in diagnosing the
				// problem.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    SYSTEXT("Method <%s> on resource <%s>, "
				    "node <%s>: authorization error: %d."),
				    fed_argv0, l->l_rname, nodename,
				    fed_result.sec_errno);
			}
			cl_result_code = CL_RC_FAILED;
			break;

		case FE_CONNECT:
			//
			// SCMSGS
			// @explanation
			// An attempted method execution failed, due to an RPC
			// connection problem. This failure is considered a
			// method failure. Depending on which method was being
			// invoked and the Failover_mode setting on the
			// resource, this might cause the resource group to
			// fail over or move to an error state; or it might
			// cause an attempted edit of a resource group or its
			// resources to fail.
			// @user_action
			// Examine other syslog messages occurring around the
			// same time on the same node, to see if the cause of
			// the problem can be identified. If the same error
			// recurs, you might have to reboot the affected node.
			// After the problem is corrected, the operator can
			// choose to issue a clresourcegroup command to bring
			// resource groups onto desired primaries, or re-try
			// the resource group update operation.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s>, node <%s>: RPC "
			    "connection error."),
			    fed_argv0, l->l_rname, nodename);
			if (fed_result.sys_errno == RPC_PROGNOTREGISTERED)
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    SYSTEXT("rpc.fed: program not registered "
				    "or server not started"));
			cl_result_code = CL_RC_FAILED;
			break;
		//
		// SCMSGS
		// @explanation
		// A method execution has exceeded its configured timeout and
		// was killed by the rgmd. Depending on which method was being
		// invoked and the Failover_mode setting on the resource, this
		// might cause the resource group to fail over or move to an
		// error state.
		// @user_action
		// Consult data service documentation to diagnose the cause of
		// the method failure.  Other syslog messages occurring just
		// before this one might indicate the reasone for the failure.
		// After correcting the problem that caused the method to fail,
		// the operator can retry the operation that failed.
		//
		case FE_TIMEDOUT:
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s>, resource group <%s>,"
			    " node <%s>: Timeout."),
			    fed_argv0, l->l_rname, l->l_rgname,
			    nodename);
			cl_result_code = CL_RC_TIMEOUT;
			break;

		// explanation, user action is in file rgm_scha_control_impl.cc
		case FE_QUIESCED:
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s>, node <%s> killed "
			    "to achieve user-initiated fast quiesce of the "
			    "resource group <%s>."), fed_argv0, l->l_rname,
			    nodename, l->l_rgname);

			//
			// in future, we should define and use a new result code
			// for quiesce instead of the timeout code. CR 6446023
			//
			cl_result_code = CL_RC_TIMEOUT;
			break;

		case FE_UNKILLABLE:
			/*
			 * This is a serious case.  We must reboot the
			 * node immediately.
			 */

			//
			// Theoretically, we could attempt to reboot
			// the zone only.  However, this scenario
			// (unkillable process) indicates a kernel hang
			// which will most likely prevent the zone from
			// rebooting successfully.  There is an RFE to
			// check the Failover_mode of the resource, and
			// only if it is "HARD" would the whole node be
			// rebooted.  Note, however, that we currently make
			// no such check of failover_mode in the non-zones
			// case of an unkillable process - the node is rebooted
			// regardless of the failover mode of the resource.
			//
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The specified callback method for the specified
			// resource became stuck in the kernel, and could not
			// be killed with a SIGKILL. The RGM reboots the node
			// to force the data service to fail over to a
			// different node, and to avoid data corruption.
			// @user_action
			// No action is required. This is normal behavior of
			// the RGM. Other syslog messages that occurred just
			// before this one might indicate the cause of the
			// method failure.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, SYSTEXT(
			    "fatal: Aborting this node because "
			    "method <%s> on resource <%s> for node <%s> "
			    "is unkillable"),
			    fed_argv0, l->l_rname, nodename);
			sc_syslog_msg_done(&handle);

			// publish an event
#if SOL_VERSION >= __s10

			(void) sc_publish_zc_event(ZONE,
			    ESC_CLUSTER_RG_NODE_REBOOTED,
			    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_R_NAME, SE_DATA_TYPE_STRING,
			    l->l_rname, CL_RG_NAME, SE_DATA_TYPE_STRING,
			    l->l_rgname, CL_NODE_NAME, SE_DATA_TYPE_STRING,
			    fed_cmd.host, NULL);
#else
			(void) sc_publish_event(
			    ESC_CLUSTER_RG_NODE_REBOOTED,
			    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
			    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
			    CL_R_NAME, SE_DATA_TYPE_STRING,
			    l->l_rname, CL_RG_NAME, SE_DATA_TYPE_STRING,
			    l->l_rgname, CL_NODE_NAME, SE_DATA_TYPE_STRING,
			    fed_cmd.host, NULL);
#endif
			// reboot the physical node -- see block comment above
			abort_node(B_TRUE, System_dumpflag, NULL);

			// NOTREACHABLE
			break;

		case FE_NOTAG:
			//
			// SCMSGS
			// @explanation
			// An internal error has occurred in the rpc.fed
			// daemon which prevents method execution. This is
			// considered a method failure. Depending on which
			// method was being invoked and the Failover_mode
			// setting on the resource, this might cause the
			// resource group to fail over or move to an error
			// state, or it might cause an attempted edit of a
			// resource group or its resources to fail.
			// @user_action
			// Examine other syslog messages occurring at about
			// the same time to see if the problem can be
			// identified. Save a copy of the /var/adm/messages
			// files on all nodes and contact your authorized Sun
			// service provider for assistance in diagnosing the
			// problem. Re-try the edit operation.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s> node <%s>: Execution "
			    "failed: no such method tag."),
			    fed_argv0, l->l_rname, nodename);
			cl_result_code = CL_RC_FAILED;
			break;

		case FE_PROJECT_NAME_INVALID:
			//
			// We should never get this error because we do
			// not request fed to validate the project name.
			//

			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("Invalid project name: %s"),
			    fed_cmd.project_name);
			cl_result_code = CL_RC_FAILED;
			break;

		default:
			// log message and abort node

			//
			// SCMSGS
			// @explanation
			// A serious error has occurred in the communication
			// between rgmd and rpc.fed. The rgmd will produce a
			// core file and will force the node to halt or reboot
			// to avoid the possibility of data corruption.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("fatal: Method <%s> on resource <%s>, node "
			    "<%s>: Received unexpected"
			    " result <%d> from rpc.fed, aborting node"),
			    fed_argv0, l->l_rname, nodename,
			    fed_result.type);
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHABLE
		}
		method_name = strdup(rt_method_string(l->l_method));

		// publish a event
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE, ESC_CLUSTER_R_METHOD_COMPLETED,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_R_NAME, SE_DATA_TYPE_STRING, l->l_rname,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, l->l_rgname,
		    CL_METHOD_NAME, SE_DATA_TYPE_STRING, method_name,
		    CL_METHOD_DURATION, SE_DATA_TYPE_TIME, method_run_time,
		    CL_METHOD_TIMEOUT, SE_DATA_TYPE_UINT32, fed_cmd.timeout,
		    CL_METHOD_PATH, SE_DATA_TYPE_STRING, fed_opts_cmd_path,
		    CL_RESULT_CODE, DATA_TYPE_UINT32, cl_result_code, NULL);
#else
		(void) sc_publish_event(ESC_CLUSTER_R_METHOD_COMPLETED,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_R_NAME, SE_DATA_TYPE_STRING, l->l_rname,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, l->l_rgname,
		    CL_METHOD_NAME, SE_DATA_TYPE_STRING, method_name,
		    CL_METHOD_DURATION, SE_DATA_TYPE_TIME, method_run_time,
		    CL_METHOD_TIMEOUT, SE_DATA_TYPE_UINT32, fed_cmd.timeout,
		    CL_METHOD_PATH, SE_DATA_TYPE_STRING, fed_opts_cmd_path,
		    CL_RESULT_CODE, DATA_TYPE_UINT32, cl_result_code, NULL);
#endif
		/* free the xdr resources */
		xdr_free((xdrproc_t)xdr_fe_run_result, (char *)&fed_result);
		if (method_name)
			free(method_name);
	}

	//
	// We assert that mip still points to our method-in-flight record,
	// created above.  No other thread modifies it or frees it.
	//
	// Set mi_completed flag to true *before* reclaiming rgm state lock.
	// This is necessary because an rgm_chg_freeze() thread might be
	// holding the state lock.  If this other thread finds that the
	// method tag (in the global list of in-flight method tags) is
	// unknown to the fed, this implies that either the method is
	// just about to be launched, or it has just completed.  To identify
	// the latter case, the other thread will poll for the
	// mi_completed flag to be set true while still holding the lock.
	// Therefore, we have to set the flag true before claiming the lock.
	//
	mip->mi_completed = B_TRUE;

	//
	// Get the latest status changes processed before grabbing the lock
	// (will be a no-op if we're not the president).
	//
	flush_status_change_queue();
	rgm_lock_state();

	//
	// Note: the following code assumes that the LogicalNode and its
	// zonename cannot have changed while the lock was released.
	//

	//
	// We have to look up the RG and R again by names in case any
	// pointers changed while we were not holding the lock.
	//
	rg = rgname_to_rg(l->l_rgname);
	CL_PANIC(rg != NULL);

	r = rname_to_r(rg, l->l_rname);

wrapup:

	//
	// At this point, we hold the lock, either from grabbing it directly
	// above, or from holding it before jumping here.
	//

	//
	// Delete the method_in_flight record from the global state structure,
	// if it was allocated above.
	//
	if (mip != NULL) {
		// Look up our record again in the global list
		methodinvoc_t	*previous_mip, *current_mip;
		previous_mip = NULL;
		for (current_mip = rg->rgl_methods_in_flight;
		    current_mip != NULL; previous_mip = current_mip,
		    current_mip = current_mip->mi_next) {
			if (current_mip == mip) {
				break;
			}
		}
		if (current_mip == NULL) {
			// We're not in the list

			//
			// SCMSGS
			// @explanation
			// An internal error has occurred. The rgmd will
			// produce a core file and will force the node to halt
			// or reboot to avoid the possibility of data
			// corruption.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "INTERNAL ERROR: launch_method: method tag <%s> "
			    "not  found in method invocation list "
			    "for resource group <%s>"),
			    fed_opts_tag, l->l_rgname);
		} else {
			// Found our record-- delete it
			if (previous_mip == NULL) {
				rg->rgl_methods_in_flight =
				    current_mip->mi_next;
			} else {
				previous_mip->mi_next = current_mip->mi_next;
			}
			free(current_mip->mi_tag);
			free(current_mip);
		}
	}

	//
	// The resource might have been deleted while the lock was released,
	// and the resource pointer will be invalid, in which
	// case we need to exit early.  Just to be cautious, we will still
	// run the state machine.
	//
	if (r == NULL) {
		goto run_state_machine; //lint !e801
	}

	//
	// Set the RG's "direction" (Starting, Stopping, neither).
	// This is used for resetting the resource's fm status below.
	//
	// If the resource is restarting (as indicated by its rx_restart_flg)
	// then we set the direction based upon rx_restart_flg rather than
	// upon the state of the resource group.  For example, a
	// restarting resource might be 'stopping' in an RG that is
	// PENDING_ONLINE, or 'starting' in an RG that is ON_PENDING_DISABLED.
	//

	rgx_state = rg->rgl_xstate[lni].rgx_state;

	if (r->rl_xstate[lni].rx_restart_flg == rgm::RR_STARTING) {
		rg_direction = RGM_DIR_STARTING;
	} else if (r->rl_xstate[lni].rx_restart_flg == rgm::RR_STOPPING) {
		rg_direction = RGM_DIR_STOPPING;
	} else switch (rgx_state) {
	case rgm::RG_PENDING_ONLINE:
	case rgm::RG_PENDING_ON_STARTED:
		rg_direction = RGM_DIR_STARTING;
		break;

	case rgm::RG_PENDING_OFFLINE:
	case rgm::RG_PENDING_OFF_STOP_FAILED:
	case rgm::RG_PENDING_OFF_START_FAILED:
	case rgm::RG_ON_PENDING_DISABLED:
		rg_direction = RGM_DIR_STOPPING;
		break;

	//
	// As noted below, some of the following cases should never occur;
	// but we're not checking for that error here.
	//
	case rgm::RG_ONLINE:
	case rgm::RG_PENDING_ONLINE_BLOCKED:
	case rgm::RG_OFF_BOOTED:
	case rgm::RG_ERROR_STOP_FAILED:
	case rgm::RG_OFFLINE_START_FAILED:
	case rgm::RG_OFFLINE:
		//
		// above states are all endish; no method should have
		// been running
		//
	case rgm::RG_ON_PENDING_R_RESTART:
		//
		// We already eliminated restarting resources in if-statement
		// above.  No other method should have been running.
		//
	case rgm::RG_ON_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_METHODS:
	case rgm::RG_OFF_PENDING_BOOT:
	case rgm::RG_ON_PENDING_MON_DISABLED:
		//
		// These states are possible, but resource is neither
		// starting nor stopping.
		//
	default:
		// rg_direction was already initialized to RGM_DIR_NULL above.
		break;
	}

	// Take appropriate action for method failure
	if (method_failed) {
		scha_failover_mode_t	fo_mode;
		scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};

		// new R state will be failstate instead of succstate
		if ((r->rl_ccrtype->rt_proxy == B_TRUE) &&
		    ((l->l_method == METH_PRENET_START) ||
		    (l->l_method == METH_POSTNET_STOP) ||
		    (l->l_method == METH_UPDATE)))
			rstate = r->fail_state;
		else
			rstate = l->l_failstate;
		//
		// Set the local state of the resource to the failure
		// state
		//
		set_rx_state(lni, r, rstate);

		//
		// At this point, if we ran Fini and if we are the president,
		// the resource might already have been deleted from memory
		// on this node by set_rx_state().  Therefore we re-lookup r.
		// If it is now NULL, then skip the following steps involving
		// r and go directly to running the state machine.
		//
		r = rname_to_r(rg, l->l_rname);
		if (r == NULL) {
			goto run_state_machine; //lint !e801
		}

#if SOL_VERSION >= __s10
		//
		// If method was executed in a nonglobal zone which is now
		// dead: Since the zone is shutting down or down already, we
		// leave the resource in the failure state, but skip method
		// failure recovery actions such as failover or zone reboot.
		//
		if (!lnNode->isUp() && !is_r_globalzone(r)) {
			ucmm_print("launch_method", NOGET(
			    "Node %d is down. Skipping method failure "
			    "handling"), lnNode->getLni());
			goto skip_method_failure_handling; //lint !e801
		}
#endif

		// get Failover_mode of the resource
		res = get_fo_mode(r->rl_ccrdata, &fo_mode);
		if (res.err_code != SCHA_ERR_NOERR) {
			//
			// SCMSGS
			// @explanation
			// A method execution has failed or timed out. For
			// some reason, the rgmd is unable to obtain the
			// Failover_mode property of the resource. The rgmd
			// assumes a setting of NONE for this property,
			// therefore avoiding the outcome of rebooting the
			// node (for STOP method failure) or failing over the
			// resource group (for START method failure). For
			// these cases, the resource is placed into a
			// STOP_FAILED or START_FAILED state, respectively.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and contact your authorized Sun service
			// provider for assistance in diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "ERROR: launch_method: cannot get Failover_mode"
			    " for resource <%s>, assuming NONE."),
			    l->l_rname);
			fo_mode = SCHA_FOMODE_NONE;
		}

		//
		// get the SCHA_RG_SUSP_AUTO_RECOVERY property value for the
		// RG and if the RG is suspended then override the failover
		// mode and set it to Log Only.
		//
		if (rg->rgl_ccr->rg_suspended) {
			fo_mode = SCHA_FOMODE_LOG_ONLY;
		}

		// Take action for the particular method failure
		switch (l->l_method) {
		case METH_PRENET_START:
		case METH_START:
			//
			// start failed; initiate failover only under the
			// following conditions:
			// - fo_mode is SOFT or HARD
			// - resource method did not execute scha_control
			//    IGNORE_FAILED_START
			// - RG is not being forcestopped (scswitch -Q)
			//
			if ((fo_mode == SCHA_FOMODE_SOFT ||
			    fo_mode == SCHA_FOMODE_HARD) &&
			    !r->rl_xstate[lni].rx_ignore_failed_start &&
			    !is_quiesce_running(l->l_rgname)) {
				set_rgx_state(lni, rg,
				    rgm::RG_PENDING_OFF_START_FAILED);
			}
			break;

		case METH_STOP:
		case METH_POSTNET_STOP:
		case METH_MONITOR_STOP:
			//
			// Stop failed; abort node/zone if fo_mode is HARD
			//
			// If the RT's fo_mode is HARD, and if the
			// resource's start and stop methods both fail,
			// the cluster could enter into a pingpong
			// rebooting syndrome like:
			//
			//  1) attempt to bring RG online on a node1 (the
			//	president).
			//  2) START fails on node1, causing attempted
			//	stoping of RG.
			//  3) STOP fails on node1, causing node1 to reboot.
			//  4) Node2 takes over as president.
			//  5) Node2 attempts to bring up RG as part of
			//	reconfiguration.
			//  6) START and STOP fail (like steps 2 and 3).
			//  7) Node2 reboots.
			//  8) Node1 take over...
			//
			// This syndrome could go on forever and the
			// continuous rebooting will leave no spare time
			// for customer to perform any useful recovery but
			// to stop the clustering service, boot into
			// non-clustering mode and manually update CCR.
			//
			// To prevent the pingpong rebooting,
			// rgmd touches a timestamp file
			// each time before rgmd aborts the node due to
			// failed stop method. The file can survive
			// the system reboot and will be useful clue for
			// the future pingpong check. So next time when the
			// rgmd tries to abort the node because of the failure
			// of stop method of the same resource, if a
			// timestamp file is found to be touched recently
			// within RG's pingpong interval time, it can assume
			// the cluster falls into pingpong rebooting syndrome
			// and will stop rebooting the node and let RG move
			// into STOP_FAILED state.
			//

			if (fo_mode == SCHA_FOMODE_HARD &&
			    !is_quiesce_running(l->l_rgname)) {
				char	fname[MAXPATHLEN];
				time_t	curr_ti, old_ti;

				//
				// For the stop_failed reboot timestamp file,
				// we append a '#' character after the resource
				// name.
				//
				(void) sprintf(fname, "%s#", l->l_rname);
				(void) get_timestamp(fname, &old_ti, lni);
				//
				// Get the last rebooting time cause by the
				// same R from timestamp file by calling
				// get_timestamp().
				//
				// It doesn't matter if get_timestamp()
				// doesn't return successfully. Either the
				// timestamp file doesn't exist, which means
				// the node isn't aborted by the same resource's
				// stop method failure before; or if there is
				// problem to get the state of timestamp file,
				// 'old_ti' will be set to "0". In both
				// cases, it is ok to just let the node
				// reboot.
				//
				if (old_ti == 0) {
					//
					// If 'old_ti' is "0", means either
					// the timestamp file doesn't exist or
					// its state can't be checked correctly.
					// In either case we fake the
					// 'curr_ti' to pass the time_out check
					// and let the node abort.
					//
					ucmm_print("launch_method", NOGET(
					    "Failed to get timestamp for "
					    "R <%s>"), l->l_rname);
					curr_ti = 100 +
					    rg->rgl_ccr->rg_ppinterval;
				} else {
					curr_ti = time(NULL);
				}
				if (curr_ti <= old_ti +
				    rg->rgl_ccr->rg_ppinterval) {
					//
					// Failed the timeout check.
					// Cluster falls into pingpong booting
					// syndrome. Stop bringing down this
					// node but do syslog to let user know.
					//

					//
					// SCMSGS
					// @explanation
					// A stop method is failed and
					// Failover_mode is set to HARD, but
					// the RGM has detected this resource
					// group falling into pingpong
					// behavior and will not abort the
					// node on which the resource's stop
					// method failed. This is most likely
					// due to the failure of both start
					// and stop methods.
					// @user_action
					// Save a copy of /var/adm/messages,
					// check for both failed start and
					// stop methods of the failing
					// resource, and make sure to have the
					// failure corrected. To restart the
					// resource group, refer to the
					// procedure for clearing the
					// ERROR_STOP_FAILED condition on a
					// resource group in the Sun Cluster
					// Administration Guide.
					//
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE, SYSTEXT(
					    "Cluster appears to be pingpong "
					    "rebooting because of failure of "
					    "method <%s> on resource <%s>. "
					    "RGM is not aborting node %s."),
					    fed_argv0, l->l_rname,
					    lnNode->getFullName());
					//
					// We do not change the RG state.
					// The state machine deals with the
					// resource in STOP_FAILED state and
					// will move the RG to ERROR_STOP_FAILED
					// at the appropriate time.
					//
					break;
				}
				//
				// The only reason we wouldn't abort the
				// node or zone at this point is if the
				// zone is already in SHUTTING_DOWN state
				// (or lower)
				//
				bool abort_node_or_zone = true;
#ifndef EUROPA_FARM
				//
				// Even though zone reboot eventually sets the
				// zone state to LN_SHUTTING_DOWN, we do it
				// here to prevent more methods from being
				// launched on the shutting down zone.
				//
				if (!lnNode->isGlobalZone()) {
					if (lnNode->isUp()) {
						lnNode->setStatus(
						    rgm::LN_SHUTTING_DOWN);
					} else {
						abort_node_or_zone = false;
					}
				}
#endif // EUROPA_FARM
				if (abort_node_or_zone) {
					//
					// No pingpong rebooting clue is found,
					// log timestamp file and abort the
					// zone/node.
					//
					// If the timestamp file can't be
					// touched we have no way but hoping
					// the other node in cluster is
					// healthy enough to touch the
					// file and, eventually, stop the
					// pingpong rebooting syndrome. So the
					// return code of touch_file() doesn't
					// matter.
					//
					(void) touch_file(fname, lni);
					//
					// SCMSGS
					// @explanation
					// A STOP method has failed or timed
					// out on a resource, and the
					// Failover_mode property of
					// that resource is set to HARD.
					// @user_action
					// No action is required. This is
					// normal behavior of the RGM. With
					// Failover_mode set to HARD, the rgmd
					// reboots the indicated node or zone
					// to force the resource group
					// offline so that it can be switched
					// onto another node. Other syslog
					// messages that occurred just before
					// this one might indicate the cause
					// of the STOP method failure.
					//
					(void) sc_syslog_msg_log(handle,
					    LOG_ERR, MESSAGE, SYSTEXT(
					    "fatal: Aborting node %s because "
					    "method <%s> failed on resource "
					    "<%s> and Failover_mode is set to "
					    "HARD"), lnNode->getFullName(),
					    fed_argv0, l->l_rname);
					sc_syslog_msg_done(&handle);

					//
					// publish a node reboot event
					//
					// Note: this might refer to zone
					// reboot rather than node reboot.
					// We use the same event type, putting
					// the zone fullname rather than
					// the nodename into the event.
					//
#if SOL_VERSION >= __s10
					(void) sc_publish_zc_event(ZONE,
					    ESC_CLUSTER_RG_NODE_REBOOTED,
					    CL_EVENT_PUB_RGM,
					    CL_EVENT_SEV_CRITICAL,
					    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
					    CL_R_NAME, SE_DATA_TYPE_STRING,
					    l->l_rname,
					    CL_RG_NAME, SE_DATA_TYPE_STRING,
					    l->l_rgname,
					    CL_NODE_NAME, SE_DATA_TYPE_STRING,
					    lnNode->getFullName(), NULL);
#else

					(void) sc_publish_event(
					    ESC_CLUSTER_RG_NODE_REBOOTED,
					    CL_EVENT_PUB_RGM,
					    CL_EVENT_SEV_CRITICAL,
					    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
					    CL_R_NAME, SE_DATA_TYPE_STRING,
					    l->l_rname,
					    CL_RG_NAME, SE_DATA_TYPE_STRING,
					    l->l_rgname,
					    CL_NODE_NAME, SE_DATA_TYPE_STRING,
					    lnNode->getFullName(), NULL);
#endif
					//
					// For a non-global zone, since we
					// launch the zone reboot in background,
					// we can continue to hold the state
					// lock.
					// For global zone reboot, the lock
					// is kept to prevent anything else
					// from happening until rgmd itself
					// aborts.
					//

					abort_node(B_TRUE, System_dumpflag,
					    zonename);

					//
					// Reaching this point means that we
					// are rebooting a nonglobal zone.
					// Reboot of a non-global zone proceeds
					// in background.
					//
					// rgmd will eventually be notified of
					// the zone death and will process it.
					//
					CL_PANIC(zonename != NULL);
				} else {
					ucmm_print("launch_method", NOGET(
					    "Not rebooting zone %s because "
					    "it's already down or "
					    "shutting_down"), zonename);
				}

				//
				// The resource state remains in the failure
				// state, so that a currently executing
				// command (e.g., clrg online) will terminate.
				//
			}

			//
			// We do not change the RG state.  The state machine
			// deals with the resource in STOP_FAILED state and
			// will move the RG to ERROR_STOP_FAILED state
			// at the appropriate time.
			//

			break;

		case METH_UPDATE:
		case METH_INIT:
		case METH_FINI:
		case METH_BOOT:
			// Set FYI-state for method failure
			// XXX (bugid 4215776) FYI states not yet implemented
			break;

		case METH_MONITOR_START:
			// No other action required.
			break;

		case METH_VALIDATE:
			// State machine should not launch validate
		case METH_MONITOR_CHECK:
			// State machine should not launch monitor_check
		default:
			// Should never happen

			//
			// SCMSGS
			// @explanation
			// An internal error occurred when the rgmd attempted
			// to launch an invalid method for the named resource.
			// The rgmd will produce a core file and will force
			// the node to halt or reboot.
			// @user_action
			// Look for other syslog error messages on the same
			// node. Save a copy of the /var/adm/messages files on
			// all nodes, and report the problem to your
			// authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "INTERNAL ERROR: launch_method: state machine "
			    "attempted to launch invalid method <%s> "
			    "(method <%d>) for resource <%s>; aborting node",
			    fed_argv0, l->l_method, l->l_rname);
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHABLE
		}
	} else {
		//
		// Set the local state of the resource to the
		// success state.
		//
		if ((r->rl_ccrtype->rt_proxy == B_TRUE) &&
		    ((l->l_method == METH_PRENET_START) ||
		    (l->l_method == METH_POSTNET_STOP) ||
		    (l->l_method == METH_UPDATE)))
			rstate = r->succ_state;
		else
			rstate = l->l_succstate;

		set_rx_state(lni, r, rstate);
		//
		// At this point, if we ran Fini and if we are the president,
		// the resource might already have been deleted from memory
		// on this node by set_rx_state().  Therefore we re-lookup r.
		// If it is now NULL, then skip the following steps involving
		// r and go directly to running the state machine.
		//
		r = rname_to_r(rg, l->l_rname);
		if (r == NULL) {
			goto run_state_machine; //lint !e801
		}
	}

skip_method_failure_handling:
	// Reset rx_ignore_failed_start whether or not method succeeded.
	r->rl_xstate[lni].rx_ignore_failed_start = B_FALSE;

	// reset the resource's status and call the state machine

	//
	// Reset fm_status and fm_status_msg based on the resource state and
	// the RG's pending "direction".
	//
	switch (rg_direction) {
	case RGM_DIR_STARTING:
		// RG's direction is "Starting".
		// If rstate is JUST_STARTED and the methods did not set the
		// status, we reset it to ONLINE/NULL.
		//
		// If rstate is START_FAILED and the methods did not set the
		// status, we reset it to FAULTED/NULL.
		//
		if (rstate == rgm::R_JUST_STARTED &&
		    is_fm_status_unchanged(lni, r, RGM_DIR_STARTING)) {
			reset_fm_status(lni, r, rgm::R_FM_ONLINE,
			    RGM_DIR_NULL);
		} else if (rstate == rgm::R_START_FAILED &&
		    is_fm_status_unchanged(lni, r, RGM_DIR_STARTING)) {
			reset_fm_status(lni, r, rgm::R_FM_FAULTED,
			    RGM_DIR_NULL);
		} else if (rstate == rgm::R_ONLINE_STANDBY &&
		    is_fm_status_unchanged(lni, r,
		    RGM_DIR_STARTING)) {
			reset_fm_status(lni, r, rgm::R_FM_UNKNOWN,
			    RGM_DIR_NULL);
		}

		break;

	case RGM_DIR_STOPPING:
		// RG's direction is "Stopping".
		// If rstate is OFFLINE and the methods did not set the status,
		// we reset it to OFFLINE/NULL.
		//
		// If rstate is STOP_FAILED and the methods did not set the
		// status, we reset it to FAULTED/NULL.
		//
		if (rstate == rgm::R_OFFLINE &&
		    is_fm_status_unchanged(lni, r, RGM_DIR_STOPPING)) {
			reset_fm_status(lni, r, rgm::R_FM_OFFLINE,
			    RGM_DIR_NULL);
		} else if (rstate == rgm::R_STOP_FAILED &&
		    is_fm_status_unchanged(lni, r, RGM_DIR_STOPPING)) {
			reset_fm_status(lni, r, rgm::R_FM_FAULTED,
			    RGM_DIR_NULL);
		}

		break;

	case RGM_DIR_NULL:
	default:
		// RG's direction is neither starting nor stopping.  We might
		// be running INIT, FINI, UPDATE, BOOT, or enabling/disabling
		// monitoring.  We do not reset the R status in these cases.
		break;
	}

	//
	// Remove r from memory if deleted flag is set and this R is no
	// longer PENDING_FINI or FINIING on any zone in the local node.
	//
	// We don't remove r here if the local node is the president, since
	// other nodes might still be running Fini methods on this resource,
	// so the president needs to still know about it.
	// The president will remove the resource from its memory in
	// set_rx_state(), after all nodes have finished running fini
	// methods on it.
	//
	if (l->l_method == METH_FINI && r->rl_is_deleted && !am_i_president()) {
		bool fini_completed = true;
		// get lni and ln of the physical node
		rgm::lni_t myLni = lnNode->getNodeid();
		LogicalNode *lnSlave = lnManager->findObject(myLni);
		CL_PANIC(lnSlave != NULL);

		//
		// check the global zone first
		//
		if (r->rl_xstate[myLni].rx_state == rgm::R_PENDING_FINI ||
		    r->rl_xstate[myLni].rx_state == rgm::R_FINIING) {
			fini_completed = false;
		} else {

			//
			// iterate through all local zones on this node,
			// check if any fini methods are still running on r
			//
			std::list<LogicalNode*>::const_iterator it;
			for (it = lnSlave->getLocalZones().begin(); it !=
			    lnSlave->getLocalZones().end(); ++it) {
				rgm::lni_t zoneLni = (*it)->getLni();
				rgm::rgm_r_state rx_state =
				    r->rl_xstate[zoneLni].rx_state;
				if (rx_state == rgm::R_PENDING_FINI ||
				    rx_state == rgm::R_FINIING) {
					fini_completed = false;
					break;
				}
			}
		}

		if (fini_completed) {
			ucmm_print("launch_method()",
			    NOGET("Destroy RS <%s> From Memory"), l->l_rname);
			CL_PANIC(rgm_delete_r(l->l_rname, l->l_rgname).err_code
			    == SCHA_ERR_NOERR);
		} else {
			ucmm_print("launch_method()",
			    NOGET("Not destroying RS <%s> From Memory because "
			    " fini is not completed on all zones"), l->l_rname);
		}
	}

run_state_machine:
	//
	// NOTE: At this point, the resource might have been deleted and
	// r might be NULL, so we must not access r any more.
	//
	// Release the RGM state mutex before running the state machine.
	// When the president is the slave, releasing the mutex allows
	// other threads (such as idl_scha_control_restart) to respond to
	// resource failure states (such as START_FAILED or MON_FAILED)
	// on the president before the state machine moves the resource
	// into a different state.  The call to set_rx_state() above will
	// have done a cond_broadcast after setting these error states.
	// Releasing the lock here permits other threads to unblock from
	// a cond_wait and recognize the error; thereby enabling them to
	// return an appropriate error code to their caller.
	//
	rgm_unlock_state();

	//
	// Execute my state machine using the threadpool.
	// This will reclaim the state mutex.  run_state_machine will
	// syslog an error message in the unlikely event that
	// rgm_run_state() returns an error.
	//
	// We don't need to execute the state machine if we have moved to
	// the JUST_STARTED state, because we can't go anywhere from that
	// state.  As soon as the president acks the state, the state machine
	// will run again.
	//

	if (rstate != rgm::R_JUST_STARTED) {
		ucmm_print("RGM",
		    NOGET("launch: call state machine, R <%s>, meth <%s>\n"),
		    l->l_rname, fed_argv0);
		run_state_machine(lni, "LAUNCH_METHOD");
	} else {
		ucmm_print("RGM",
		    NOGET("launch: NOT calling state machine, R <%s>, meth "
		    "<%s>, because the new state is R_JUST_STARTED\n"),
		    l->l_rname, fed_argv0);
	}

	ucmm_print("RGM",
	    NOGET("launch: exit launch_method, R <%s>, meth <%s>\n"),
	    l->l_rname, fed_argv0);

	free(nodename);
	free(zonename);
	// free the method name(s) strdup-ed by the caller or by this function
	free(l->l_rt_meth_name);
	free(real_method_name);
	free(method_string);
	// free the launchargs structure malloc-ed in process_resource
	free(l->l_rname);
	free(l->l_rgname);
	free(l);
	// free the fed_opts_cmd_path string malloc-ed in fe_method_full_name
	free(fed_opts_cmd_path);

	// free project name strdup-ed by function get_project_name
	if (fed_cmd.project_name)
		free(fed_cmd.project_name);

	if (fed_cmd.corepath)
		free(fed_cmd.corepath);
	sc_syslog_msg_done(&handle);
}


//
// launch_validate_method(launchvalargs_t, rgm::lni_t)
//
// This function is called on the slave node, outside of the state machine.
// VALIDATE method is only called when resource is created or updated through
// adminstrative action.  It is called before the creation or update is
// applied, and a failure return code from the method on any node causes
// the creation or update to be canceled.
//
// This function is called only by idl_scrgadm_rs_validate(). The protocol
// with idl_srcgadm_validate requires that idl_scrgadm_rs_validate holds
// the state lock before calling launch_validate_method, and that
// launch_validate_method release the lock before returning.
//
// The syntax of arguments for VALIDATE method is:
//
//	<method> { -c | -u } -R <resource_name> -T <rt_name> -G <rg_name>
//	    [ -Z zone_name ]
//	    [ -r <resource_property_name>=<value> ] ...
//	    [ -x <extension_property_name>=<value> ] ...
//	    [ -g <rg_property_name>=<value> ] ...
//	    [ -P project_name ]
//	    [ -m VALIDATE [ -p path_to_real_method ]
//	    [ -z zone_to_launch_method_into ]]
//
// The -m and -p arguments are used for scalable resources, when the
// ssm_wrapper is invoked instead of the real validate method (if any).
//
// For resource creation, the property list will include all resource and
// RG properties.  On update, it will include only those properties set
// in the update opertion.
//
// XXX RFE 4257230: create methodinvoc_t entry (as in launch_method above).
//
// This function validates the project name.  The president doesn't validate
// project name because the president may not be one of the potential masters
// of the rg, that is, the resource may not be able to be brought online on the
// president node.  The project name is only validated on the nodes that
// VALIDATE method will be run.
//

scha_errmsg_t
launch_validate_method(launchvalargs_t *l, rgm::lni_t lni)
{
	char 		fed_opts_tag[ MAXFEDTAGLEN + 1 ] = { 0 };
	char 		*fed_opts_cmd_path = NULL;
	char 		*real_method_name = NULL;
	char 		fed_argv0[MAXPATHLEN + 32];	// string for argv[0]
	char 		**fed_opts_argv = NULL;
	size_t		argv_size;	// # entries allocated for fed_opts_argv
	fe_cmd		fed_cmd = {0};
	fe_run_result	fed_result = {FE_OKAY, 0, 0, SEC_OK, NULL};
	char 		**fed_opts_env;
	rgm_rt_t	*rt = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	uint_t		i, j, r_len, x_len, g_len, env_i, pn_ext_len;
	uint_t		rc = 0;
	char		*err_str;
	hrtime_t	method_start_time, method_end_time, method_run_time = 0;
	int		timeoutpc;
	char		*method_name = NULL;		// "VALIDATE"
	sc_syslog_msg_handle_t handle;
	cl_event_result_t	cl_result_code = CL_RC_OK;
	char *zonename = NULL;
	LogicalNode *lnNode = lnManager->findObject(lni);

	CL_PANIC(lnNode != NULL);
	if (lnNode->getZonename() != NULL) {
		zonename = strdup(lnNode->getZonename());
	} else if ((ZONE != NULL) && (strcmp(ZONE, GLOBAL_ZONENAME) != 0)) {
#if SOL_VERSION >= __s10
		zonename = strdup(ZONE);
#endif
	}
	char *nodename = strdup(lnNode->getFullName());
	// To create the name/value environemnt variable string.
	// The biggest locale should be MAXNAMELEN.  Multiply by 2 to
	// give (more than enough) room for the name (like LC_MESSAGES).
	char env_buf[MAXNAMELEN * 2];

	// For our local copy of the env vars.  An extra space because the
	// list must be NULL terminated.
	char *val_env_vars[NUM_ENV_VARS + 1];
	// SCSLM addon start
	rglist_p_t rg = NULL;
	// SCSLM addon end

	/*
	 * Make sure the val_env_vars is initialized.
	 * Needs to be here, before there's any chance we can get to
	 * the finished tag, because we free the strings there.
	 */
	(void) memset(val_env_vars, '\0', sizeof (char *) *
	    (NUM_ENV_VARS + 1));

	//
	// If resource is scalable, invoke the ssm wrapper in lieu of the real
	// validate method.  Set up fed_argv0 string as a synthetic "program
	// name" which will be assigned to argv[0] when the fed executes the
	// method (or the ssm_wrapper).  The value of argv[0] is
	// shown by ps(1), etc.  Append the string "(scalable svc)" to
	// indicate that the scalable service wrapper program is being
	// invoked.  Otherwise, fed_argv0 is just the method program name,
	// as usual.
	//
	// If l->lv_rt_meth_name is non-null, it holds the registered method
	// name.  If there is no VALIDATE method registered by the scalable
	// service, this parameter is NULL.
	//
	if (l->lv_is_scalable) {
		(void) strcpy(fed_argv0,
		    l->lv_rt_meth_name ? l->lv_rt_meth_name : SCHA_VALIDATE);
		(void) strcat(fed_argv0, " (scalable svc)");
	} else {
		(void) strcpy(fed_argv0, l->lv_rt_meth_name);
	}

	fed_cmd.host = getlocalhostname();	// doesn't need to be freed
	if (fed_cmd.host == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to obtain the name of the local
		// host, causing a VALIDATE method invocation to fail. This in
		// turn will cause the failure of a creation or update
		// operation on a resource or resource group.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Re-try the
		// creation or update operation. If the problem recurs, save a
		// copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for
		// assistance.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "launch_validate_method: getlocalhostname() failed "
		    "for resource <%s>, resource group <%s>, method <%s>"),
		    l->lv_rname, l->lv_rgname, fed_argv0);
		sc_syslog_msg_done(&handle);

		// Caller expects us to release state lock before returning
		rgm_unlock_state();

		res.err_code = SCHA_ERR_VALIDATE;
		free(nodename);
		free(zonename);
		return (res);
	}

	fed_cmd.security = SEC_UNIX_STRONG;
	fed_cmd.flags = 0;

	//
	// call rtname_to_rt to get RT pointer from memory.
	// If RT is not in memory, rtname_to_rt will get it from ccr and
	// copy it in memory
	//
	rt = rtname_to_rt(l->lv_rtname);
	if (rt == NULL) {
		rgm_unlock_state();
		res.err_code = SCHA_ERR_RT;
		free(nodename);
		free(zonename);
		return (res);
	}

	//
	// Capture the RT's "Global_zone" property.  We might override this
	// later if the resource sets Global_zone_override;
	//
	boolean_t globalzone = rt->rt_globalzone;

	/*
	 * Tell the fed to capture the output
	 * Don't syslog it, for now.
	 */
	fed_cmd.flags |= FE_CAPTURE_OUTPUT;

	if (zonename == NULL) {	// hosted on global zone
		(void) sprintf(fed_opts_tag, "%s.%s.%d", l->lv_rgname,
		    l->lv_rname, l->lv_method);
	} else {
		(void) sprintf(fed_opts_tag, "%s.%s.%s.%d", zonename,
		    l->lv_rgname, l->lv_rname, l->lv_method);
	}

	// assemble name of full path
	fed_opts_cmd_path = fe_method_full_name(rt->rt_basedir,
		l->lv_rt_meth_name);
	if ((fed_opts_cmd_path == NULL) && !l->lv_is_scalable) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Due to an internal error, the rgmd daemon was unable to
		// assemble the full method pathname for the VALIDATE method.
		// This is considered a VALIDATE method failure. This in turn
		// will cause the failure of a creation or update operation on
		// a resource or resource group.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Re-try the
		// creation or update operation. If the problem recurs, save a
		// copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for
		// assistance.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("launch_validate: fe_method_full_name() failed "
		    "for resource <%s>, resource group <%s>, method <%s>"),
		    l->lv_rname, l->lv_rgname,
		    l->lv_rt_meth_name ? l->lv_rt_meth_name : "");
		sc_syslog_msg_done(&handle);
		rgm_unlock_state();
		res.err_code = SCHA_ERR_VALIDATE;
		free(nodename);
		free(zonename);
		return (res);
	}

	// SCSLM addon start
	rg = rgname_to_rg(l->lv_rgname);
	// SCSLM addon end

	//
	// obtain the R, if this is an UPDATE
	// IMPORTANT NOTE: if this is a create, the r
	// will be NULL, and cannot be referenced anywhere in this
	// function!
	//
	rlist_p_t r = rname_to_r(NULL, l->lv_rname);
	if (l->lv_opcode == rgm::RGM_OP_UPDATE) {
		if (r == NULL) {
			ucmm_print("RGM", NOGET("launch_validate_method: "
			    "failed to get R <%s>\n"), l->lv_rname);
			rgm_unlock_state();
			res.err_code = SCHA_ERR_RSRC;
			goto finished; //lint !e801
		} else {
			// recompute globalzone based on the resource props
			globalzone = is_r_globalzone(r);
		}
	}

	//
	// Obtain the existing timeout for the validate method in case a
	// new one has not been provided.  If a new timeout has been
	// specified as part of the update, it will be set later.
	//
	// If there is no VALIDATE method (which may occur only for a scalable
	// resource) then set the timeout to default ssm timeout.
	//
	if (l->lv_rt_meth_name == NULL) {
		CL_PANIC(l->lv_is_scalable);
		fed_cmd.timeout = SSM_WRAPPER_TIMEOUT;
	} else if (l->lv_opcode == rgm::RGM_OP_UPDATE) {
		int	timeout;
		if ((res = rt_method_timeout(r, l->lv_rt_meth_name,
		    l->lv_method, &timeout)).err_code != SCHA_ERR_NOERR) {
			ucmm_print("RGM", NOGET("launch_validate_method: "
			    "failed to get method <%s> timeout value from "
			    "resource <%s>\n"),
			    l->lv_rt_meth_name, l->lv_rname);
			rgm_unlock_state();
			goto finished; //lint !e801
		}
		fed_cmd.timeout = timeout;
	}

	// Don't release state mutex until we're done accessing R/RG properties

	//
	// If l->lv_is_scalable is true, then the resource is scalable;
	// we will invoke the ssm_wrapper, passing it the real VALIDATE method
	// pathname (if any) as a command line argument.
	//
	if (l->lv_is_scalable) {
		real_method_name = fed_opts_cmd_path;
		fed_opts_cmd_path = strdup(SSM_WRAPPER_PATH);
	}

	//
	// Allocate memory for fed_opts_argv.  14 elements (plus terminating
	// NULL) will hold the fixed arguments, including possible scalable
	// services args -m and -p and possible zone argument -Z.
	// More memory will be allocated below for
	// the property args (-r, -x, -g) by calling realloc_err().
	// Note: calloc (and also, later calls to realloc_err) will exit the
	// rgmd if out of swap space.  So the return value is guaranteed to
	// be non-NULL.
	//
	argv_size = 19;
	fed_opts_argv = (char **)calloc(argv_size, sizeof (char *));
	i = 0;
	fed_opts_argv[i++] = fed_argv0;

	if (l->lv_opcode == rgm::RGM_OP_CREATE)
		fed_opts_argv[i++] = "-c";
	else if (l->lv_opcode == rgm::RGM_OP_UPDATE)
		fed_opts_argv[i++] = "-u";
	else {
		ucmm_print("RGM",
		    NOGET("launch_validate: invalid opcode -- <%d>\n"),
		    l->lv_opcode);
		res.err_code = SCHA_ERR_INTERNAL;
		rgm_unlock_state();
		goto finished; //lint !e801
	}

	fed_opts_argv[i++] = "-R";
	fed_opts_argv[i++] = l->lv_rname;
	fed_opts_argv[i++] = "-T";
	fed_opts_argv[i++] = l->lv_rtname;
	fed_opts_argv[i++] = "-G";
	fed_opts_argv[i++] = l->lv_rgname;

	//
	// On resource creation,
	// the validate arguments in l->lv_r_props contain *all* the
	// RT and R properties, including the VALIDATE method timeout.
	// So in the following for-loop, this value will be extracted and
	// assigned to fed_cmd.timeout.
	//
	// If we are doing an R update instead of creating a new R,
	// and a new VALIDATE method timeout has been specified,
	// overwrite the existing value set above with the new one.
	//
	// In this loop, we will also extract the Resource_project_name
	// and Global_zone_override properties from the lv_r_props.
	//
	// Following the same convention used for Validate timeout and
	// Resource_project_name, if we are doing an UPDATE and setting
	// a new value for Global_zone_override, the new value will apply
	// to the execution of the Validate method itself.
	//

	int	val_timeout;	// used in sscanf call below
	// for resource predefined properties
	r_len = l->lv_r_props.length();
	//
	// Make room for two more elements in fed_opts_argv.
	//
	// Suppress the following lint warning:
	// Warning(672) [c:46]: Creation of memory leak in assignment
	// to 'fed_opts_argv'
	// There is no leak because realloc_err calls realloc(), which
	// frees its first argument if necessary.
	//
	argv_size += 2 * r_len;
	fed_opts_argv = (char **)realloc_err(fed_opts_argv,
	    argv_size * sizeof (char *));	//lint !e672
	fed_cmd.project_name = NULL;

	for (j = 0; j < r_len; j++) {
		fed_opts_argv[i++] = "-r";
		fed_opts_argv[i++] = l->lv_r_props[j];
		// set fed_cmd.timeout to the timeout of validate method
		if (strncasecmp(l->lv_r_props[j], SCHA_VALIDATE_TIMEOUT,
		    strlen(SCHA_VALIDATE_TIMEOUT)) == 0) {
			if (sscanf((strstr(l->lv_r_props[j], "=") + 1),
			    "%d", &val_timeout) == 1) {
				fed_cmd.timeout = val_timeout;
			} else {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// The indicated resource's VALIDATE method
				// timeout, as stored in the CCR, is not an
				// integer value. This might indicate
				// corruption of CCR data or rgmd in-memory
				// state; the VALIDATE method invocation will
				// fail. This in turn will cause the failure
				// of a creation or update operation on a
				// resource or resource group.
				// @user_action
				// Use clresource show -v to examine resource
				// properties. If the VALIDATE method timeout
				// or other property values appear corrupted,
				// the CCR might have to be rebuilt. If values
				// appear correct, this may indicate an
				// internal error in the rgmd. Re-try the
				// creation or update operation. If the
				// problem recurs, save a copy of the
				// /var/adm/messages files on all nodes and
				// contact your authorized Sun service
				// provider for assistance.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT(
				    "ERROR: VALIDATE method timeout property "
				    "of resource <%s> is not an integer"),
				    l->lv_rname);
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_VALIDATE;
				rgm_unlock_state();
				goto finished; //lint !e801
			}
		}
#ifdef SC_SRM
		//
		// Get the resource project name specified in the command line.
		// If resource_project_name is set to empty string on resource
		// creation, do nothing; later will check the setting of
		// rg_project_name in rg properties list.
		// Note: For R/RG update, only those properties specified in the
		// command line are included in the arg list.
		// On resource creation, all r and rg properties are included in
		// the arg list.
		//
		if (fed_cmd.project_name == NULL &&
		    strncasecmp(l->lv_r_props[j], SCHA_RESOURCE_PROJECT_NAME,
		    strlen(SCHA_RESOURCE_PROJECT_NAME)) == 0) {
			// "Resource_project_name=<projname>" is found in
			// argument list
			char proj[MAXRGMOBJNAMELEN];
			proj[0] = '\0';
			if (sscanf((strstr((const char *)l->lv_r_props[j],
			    "=") + 1),
			    "%s", proj) == 1 && proj[0] != '\0')
				fed_cmd.project_name = strdup(proj);
			else
				// resource_project_name is set to ""
				// If this is for updating resource,
				// Use the project name of the containing rg
				if (l->lv_opcode == rgm::RGM_OP_UPDATE) {
					rglist_p_t rgp = rgname_to_rg(
					    l->lv_rgname);
					CL_PANIC(rgp != NULL);
					fed_cmd.project_name = get_project_name(
					    NULL, rgp->rgl_ccr);
				}
		}

#endif
#if SOL_VERSION >= __s10
		//
		// Get Global_zone_override if specified in the command line.
		// This property is applicable only if the RT's globalzone
		// property is set to true; it can override it to false.
		// If set on the command line, (and the operation is UPDATE),
		// this setting will take precedence over the resource's
		// existing Global_zone_override property value.
		//
		if (rt->rt_globalzone) {
			if (strncasecmp(l->lv_r_props[j],
			    SCHA_GLOBAL_ZONE_OVERRIDE,
			    strlen(SCHA_GLOBAL_ZONE_OVERRIDE)) == 0) {
				// "Global_zone_override=<bool>" is found in
				// argument list
				if (strcasecmp("=" SCHA_FALSE,
				    (const char *)l->lv_r_props[j] +
				    strlen(SCHA_GLOBAL_ZONE_OVERRIDE)) == 0) {
					globalzone = B_FALSE;
				} else {
					globalzone = B_TRUE;
				}
			}
		}
#endif
	}

#if SOL_VERSION >= __s10
	// some methods run in global zone since they need more privileges
	if (!globalzone && !l->lv_is_scalable) {
		fed_cmd.zonename = zonename;
	}

	// add -Z only if hosted in local zone, while method runs in global zone
	if (zonename && globalzone) {
		fed_opts_argv[i++] = "-Z";
		fed_opts_argv[i++] = zonename;
	}
#endif

	// Now that we have globalzone, we can set fed core props
	set_fed_core_props(fed_cmd, fed_opts_tag, zonename, globalzone);

	// for resource extention properties
	x_len = l->lv_x_props.length();
	//
	// Make room for two more elements in fed_opts_argv
	//
	// Suppress the following lint warning:
	// Warning(672) [c:46]: Possible memory leak in assignment
	// to 'fed_opts_argv'
	// There is no leak because realloc_err calls realloc(), which
	// frees its first argument if necessary.  If the realloc()
	// call returns NULL, realloc_err exits the rgmd process.
	//
	argv_size += 2 * x_len;
	fed_opts_argv = (char **)realloc_err(fed_opts_argv,
	    argv_size * sizeof (char *));	//lint !e672

	for (j = 0; j < x_len; j++) {
		fed_opts_argv[i++] = "-x";
		fed_opts_argv[i++] = l->lv_x_props[j];
	}

	// for per-node resource extention properties
	pn_ext_len = l->lv_allnodes_x_props.length();
	//
	// Make room for two more elements in fed_opts_argv
	//
	// Suppress the following lint warning:
	// Warning(672) [c:46]: Possible memory leak in assignment
	// to 'fed_opts_argv'
	// There is no leak because realloc_err calls realloc(), which
	// frees its first argument if necessary.  If the realloc()
	// call returns NULL, realloc_err exits the rgmd process.
	//
	argv_size += 2 * pn_ext_len;
	fed_opts_argv = (char **)realloc_err(fed_opts_argv,
	    argv_size * sizeof (char *));	//lint !e672

	for (j = 0; j < pn_ext_len; j++) {
		fed_opts_argv[i++] = "-X";
		fed_opts_argv[i++] = l->lv_allnodes_x_props[j];
	}


	// for rg properties
	g_len = l->lv_g_props.length();
	//
	// Make room for two more elements in fed_opts_argv
	//
	// Suppress the following lint warning:
	// Warning(672) [c:46]: Possible memory leak in assignment
	// to 'fed_opts_argv'
	// There is no leak because realloc_err calls realloc(), which
	// frees its first argument if necessary.  If the realloc()
	// call returns NULL, realloc_err exits the rgmd process.
	//
	argv_size += 2 * g_len;
	fed_opts_argv = (char **)realloc_err(fed_opts_argv,
	    argv_size * sizeof (char *));	//lint !e672

	for (j = 0; j < g_len; j++) {
		fed_opts_argv[i++] = "-g";
		fed_opts_argv[i++] = l->lv_g_props[j];

#ifdef SC_SRM
		// Get the RG project name specified in the command line.
		if (fed_cmd.project_name == NULL &&
		    strncasecmp(l->lv_g_props[j], SCHA_RG_PROJECT_NAME,
		    strlen(SCHA_RG_PROJECT_NAME)) == 0) {
			// "RG_project_name=<projname>" is found in
			// argument list
			char proj[MAXRGMOBJNAMELEN];
			proj[0] = '\0';
			if (sscanf((strstr((const char *)l->lv_g_props[j],
			    "=") + 1),
			    "%s", proj) == 1 && proj[0] != '\0')
				fed_cmd.project_name = strdup(proj);
			else {
				// RG_project_name is changed to ""
				// Use "default" project name
				fed_cmd.project_name = get_project_name(NULL,
				    NULL);
			}
		}
#endif
	}


#ifdef SC_SRM

	// If fed_cmd.project_name is still NULL, that means both
	// resource_project_name and rg_project_name are not set in the
	// argument list or set to "".  There are two cases for it:
	// Case 1: on resource update, resource_project_name is not specified.
	// Case 2: on RG update, RG_project_name is not in arg list.
	// For both cases, use in-memory copy of resource project name.
	//
	// We only do this in the UPDATE case -- in the CREATE case, there
	// is no resource in-memory to look at
	//
	if (fed_cmd.project_name == NULL &&
	    l->lv_opcode == rgm::RGM_OP_UPDATE) {
		fed_cmd.project_name = get_project_name(r->rl_ccrdata, NULL);
	}

	if (fed_cmd.project_name != NULL) {
		//
		// Ask fed to validate the project name
		//
		fed_cmd.flags |= FE_VALIDATE_PROJ_NAME;
	}
#endif

	// Now we're done accessing R/RG properties -- can unlock the mutex
	rgm_unlock_state();

	//
	// Note: the following code assumes that the LogicalNode and its
	// zonename cannot change after the lock is released.
	//

	// If the resource is scalable, append additional args for ssm_wrapper
	if (l->lv_is_scalable) {
		//
		// Note, the calloc() and realloc() calls above already
		// allocated enough space for these four additional elements
		// of fed_opts_argv.
		//
		// Suppress these lint Info messages:
		// Info(794) [c:41]: Conceivable use of null pointer
		//	(fed_opts_argv) in left argument to operator '['
		// Info(794) [c:37]: Conceivable use of null pointer
		//	(fed_opts_argv) in left argument to operator '['
		// Info(794) [c:49]: Conceivable use of null pointer
		//	(fed_opts_argv) in left argument to operator '['
		// Info(794) [c:61]: Conceivable use of null pointer
		//	(fed_opts_argv) in left argument to operator '['
		//
		// in the four following assignments to fed_opts_argv[i++].
		// The above call to calloc() cannot return NULL.
		//
		fed_opts_argv[i++] = "-m";			//lint !e794
		fed_opts_argv[i++] = SCHA_VALIDATE;		//lint !e794
		if (real_method_name != NULL) {
			fed_opts_argv[i++] = "-p";		//lint !e794
			fed_opts_argv[i++] = real_method_name;	//lint !e794
		}
		//
		// We have already captured the Global_zone setting either
		// from the rt, the resource, or the command line.
		//
		if (zonename && !globalzone) {
			fed_opts_argv[i++] = "-z";
			fed_opts_argv[i++] = zonename;
#ifdef SC_SRM
			// Set the projectname.
			fed_opts_argv[i++] = "-P";
			fed_opts_argv[i++] = fed_cmd.project_name;
			// SC SLM addon start
			fed_cmd.ssm_wrapper_zone = zonename;
			// SC SLM addon end
#endif
		}

	}

	//
	// Suppress these lint Info messages:
	// Info(797) [c:27]: Conceivable creation of out-of-bounds pointer
	// (4 beyond end of data) by operator '['
	// Info(794) [c:27]: Conceivable use of null pointer (fed_opts_argv)
	// in left argument to operator '['
	// Info(796) [c:27]: Conceivable access of out-of-bounds pointer (4
	// beyond end of data) by operator '['
	//
	// The above calls to realloc_err allocate the necessary storage and
	// cannot return NULL.
	//
	fed_opts_argv[i] = NULL;	//lint !e797 !e794 !e796

#ifdef	DEBUG
	// Suppress lint Info messages.  See comment above.
	ucmm_print("RGM", NOGET("fed_cmd.timeout=%d\n"), fed_cmd.timeout);
	for (i = 0; fed_opts_argv[i] != NULL; i++) {	//lint !e794
		ucmm_print("RGM", NOGET("launch_validate: arg[%d] = <%s>\n"),
		    i, fed_opts_argv[i]);		//lint !e794
	}
#endif	// DEBUG

	//
	// set environment for method execution
	//
	// Suppress this lint error (which seems to come from a bug in lint
	// itself):
	// Info(794) [c:8]: Conceivable use of null pointer (fed_opts_argv) in
	//	left argument to operator '['
	//
	if ((fed_opts_env = fe_set_env_vars()) == NULL) {	//lint !e794
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to set up environment variables
		// for method execution, causing a VALIDATE method invocation
		// to fail. This in turn will cause the failure of a creation
		// or update operation on a resource or resource group.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Re-try the
		// creation or update operation. If the problem recurs, save a
		// copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for
		// assistance.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("launch_validate: fe_set_env_vars() failed "
		    "for resource <%s>, resource group <%s>, method <%s>"),
		    l->lv_rname, l->lv_rgname, fed_argv0);
		sc_syslog_msg_done(&handle);
		res.err_code = SCHA_ERR_VALIDATE;
		goto finished; //lint !e801
	}

	/*
	 * Make a local copy of the env vars so we can add our LC_MESSAGES
	 * Copy over all except the last one (LC_MESSAGES).
	 */
	for (env_i = 0; env_i < NUM_ENV_VARS - 1; env_i++) {
		/*
		 * Note: strdup will crash the rgm if it cannot
		 * allocate the required memory, so there is no need
		 * to check the result.
		 */
		val_env_vars[env_i] = strdup(fed_opts_env[env_i]);
	}

	/*
	 * Set the locale that we were given.
	 * Cap the whole string at 2 * MAXNAMELEN, which it should never
	 * exceed in any case.
	 */
	(void) snprintf(env_buf, (size_t)(2 * MAXNAMELEN),
	    "LC_MESSAGES=%s", l->locale);

	/*
	 * Note: strdup will crash the rgm if it cannot
	 * allocate the required memory, so there is no need
	 * to check the result.
	 */
	val_env_vars[NUM_ENV_VARS - 1] = strdup(env_buf);

	ucmm_print("RGM",
	    NOGET("launch_validate: fe_run R <%s>, RG <%s>, meth <%s>, "
	    "timeout <%d>, locale <%s>\n"),
	    l->lv_rname, l->lv_rgname, fed_argv0, fed_cmd.timeout,
	    val_env_vars[NUM_ENV_VARS - 1]);

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");
	(void) sc_syslog_msg_log(handle, LOG_NOTICE, MESSAGE,
	    SYSTEXT("launching method <%s> for resource <%s>, "
	    "resource group <%s>, node <%s>, timeout <%d> seconds"),
	    fed_argv0, l->lv_rname, l->lv_rgname, nodename,
	    fed_cmd.timeout);
	sc_syslog_msg_done(&handle);

	// SC SLM addon start
#ifdef SC_SRM
	fed_cmd.r_name = l->lv_rname;
	fed_cmd.rg_name = l->lv_rgname;
	fed_cmd.slm_flags = l->lv_method;
#if SOL_VERSION >= __s10
	// some methods run in global zone since they need more privileges
	// Bypass SLM handling for such methods
	if (rt != NULL && globalzone && zonename != NULL) {
		fed_cmd.slm_flags = METH_FEDSLM_BYPASS;
	}
#endif
	if ((rg != NULL) && (rg->rgl_ccr != NULL)) {
		fed_cmd.rg_slm_type = rg->rgl_ccr->rg_slm_type;
		fed_cmd.rg_slm_pset_type = rg->rgl_ccr->rg_slm_pset_type;
		fed_cmd.rg_slm_cpu = rg->rgl_ccr->rg_slm_cpu;
		fed_cmd.rg_slm_cpu_min = rg->rgl_ccr->rg_slm_cpu_min;
	}
#endif
	// SC SLM addon end

	method_start_time = os::gethrtime();

	// Call the fork execution daemon to run the method
	if (fe_run(fed_opts_tag, &fed_cmd, fed_opts_cmd_path,
	    fed_opts_argv, val_env_vars, &fed_result) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd failed in an attempt to execute a VALIDATE method,
		// due to a failure to communicate with the rpc.fed daemon. If
		// the rpc.fed process died, this might lead to a subsequent
		// reboot of the node. Otherwise, this will cause the failure
		// of a creation or update operation on a resource or resource
		// group.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the problem can be identified. Re-try the
		// creation or update operation. If the problem recurs, save a
		// copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for
		// assistance.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("launch_validate: call to rpc.fed failed "
		    "for resource <%s>, method <%s>"),
		    l->lv_rname, fed_argv0);
		sc_syslog_msg_done(&handle);

		res.err_code = SCHA_ERR_VALIDATE;
		rgm_format_errmsg(&res, REM_FED_RPC, l->lv_rname, fed_argv0);

		goto finished; //lint !e801
	}

	switch (fed_result.type) {
	case FE_OKAY:

		method_end_time = os::gethrtime();
		method_run_time = method_end_time - method_start_time;

		if (fed_cmd.timeout != 0) {
			timeoutpc = (int)(((float)method_run_time / 10000000) /
				fed_cmd.timeout);
		} else {
			timeoutpc = 0;
		}

		/*
		 * We executed the method, so we have some output.
		 * For some reason rpc calls appear to translate NULL strings
		 * to empty strings, so check for empty string as well as
		 * NULL.
		 */
		if (fed_result.output && strlen(fed_result.output) != 0) {
			rgm_sprintf(&res, "%s", fed_result.output);
		}

		//
		// method_retcode now is the unprocessed return
		// from wait, which needs to be processed by client;
		//

		// rc is used here and re-used below
		rc = fed_result.method_retcode;
		if (!WIFEXITED(rc)) {
			if (WIFSIGNALED(rc)) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT(
				    "Method <%s> on resource <%s>, node <%s> "
				    "terminated due to receipt of signal <%d>"),
				    fed_argv0, l->lv_rname, nodename,
				    WTERMSIG(rc));
				sc_syslog_msg_done(&handle);

				rgm_format_errmsg(&res, REM_FED_SIGNAL,
				    fed_argv0, l->lv_rname, WTERMSIG(rc));

			} else if (WIFSTOPPED(rc)) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT(
				    "Method <%s> on resource <%s>, node <%s> "
				    "stopped due to receipt of signal <%d>"),
				    fed_argv0, l->lv_rname, nodename,
				    WSTOPSIG(rc));
				sc_syslog_msg_done(&handle);

				rgm_format_errmsg(&res, REM_FED_SIGNAL,
				    fed_argv0, l->lv_rname, WSTOPSIG(rc));
			} else {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, SYSTEXT(
				    "Method <%s> on resource <%s>, node <%s> "
				    "terminated abnormally"),
				    fed_argv0,
				    l->lv_rname, nodename);
				sc_syslog_msg_done(&handle);

				rgm_format_errmsg(&res, REM_FED_ABNORM,
				    fed_argv0, l->lv_rname);

			}
			cl_result_code = CL_RC_FAILED;
			res.err_code = SCHA_ERR_VALIDATE;
			break;
		}

		// rc is used above and re-used here
		rc = WEXITSTATUS(fed_result.method_retcode);
		switch (rc) {
		case EMETH_SUCCESS:
			// all went well
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle,
			    LOG_NOTICE, MESSAGE,
			    SYSTEXT("method <%s> completed "
			    "successfully for resource <%s>, "
			    "resource group <%s>, node <%s>, time used: %d%% of"
			    " timeout <%d seconds>"),
			    fed_argv0, l->lv_rname, l->lv_rgname,
			    nodename, timeoutpc,
			    fed_cmd.timeout);
			sc_syslog_msg_done(&handle);
			res.err_code = SCHA_ERR_NOERR;
			cl_result_code = CL_RC_OK;
			/* Don't send this message back to the user */
			break;

		case EMETH_FAILURE:
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The resource's VALIDATE method exited with a
			// non-zero exit code. This indicates that an
			// attempted update of a resource or resource group is
			// invalid.
			// @user_action
			// Examine syslog messages occurring just before this
			// one to determine the cause of validation failure.
			// Re-try the update.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("VALIDATE failed on resource <%s>, "
			    "resource group <%s>, time used: %d%% of "
			    "timeout <%d, seconds>"),
			    l->lv_rname, l->lv_rgname, timeoutpc,
			    fed_cmd.timeout);
			sc_syslog_msg_done(&handle);
			cl_result_code = CL_RC_FAILED;
			res.err_code = SCHA_ERR_VALIDATE;
			rgm_format_errmsg(&res, REM_FED_VALIDATE,
			    l->lv_rname, l->lv_rgname);
			break;
		}
		break;

	/* SC SLM addon start */
	case FE_SLM_ERROR:
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "Method <%s> on resource <%s>: "
		    "Execution failed: SLM error"),
		    fed_argv0, l->lv_rname);
		sc_syslog_msg_done(&handle);
		res.err_code = SCHA_ERR_VALIDATE;
		cl_result_code = CL_RC_FAILED;

		rgm_format_errmsg(&res, REM_FED_SLM_ERROR,
		    fed_argv0, l->lv_rname);

		break;
	/* SC SLM addon end */

	case FE_SYSERRNO:
		if (strerror(fed_result.sys_errno)) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> failed to execute on resource <%s> "
			    "in resource group <%s>, on node <%s>, error: "
			    "<%s>"), fed_argv0, l->lv_rname, l->lv_rgname,
			    nodename, strerror(fed_result.sys_errno));
			sc_syslog_msg_done(&handle);

		} else {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> failed to execute on resource <%s> "
			    "in resource group <%s>, on node <%s>, error: "
			    "<%d>"), fed_argv0, l->lv_rname, l->lv_rgname,
			    nodename, fed_result.sys_errno);
			sc_syslog_msg_done(&handle);
		}
		cl_result_code = CL_RC_FAILED;
		res.err_code = SCHA_ERR_VALIDATE;
		rgm_format_errmsg(&res, REM_FED_FAILED_EXEC,
		    fed_argv0, l->lv_rname, l->lv_rgname,
		    strerror(fed_result.sys_errno) ?
		    strerror(fed_result.sys_errno) : "");

		break;

	case FE_DUP:
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "Error: duplicate method <%s> launched on "
		    "resource <%s> in resource group <%s> on node <%s>"),
		    fed_argv0, l->lv_rname, l->lv_rgname, nodename);
		sc_syslog_msg_done(&handle);
		cl_result_code = CL_RC_FAILED;
		res.err_code = SCHA_ERR_VALIDATE;
		rgm_format_errmsg(&res, REM_FED_DUP,
		    fed_argv0, l->lv_rname, l->lv_rgname);
		break;

	case FE_UNKNC:
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// An internal error has occurred in the interface between the
		// rgmd and fed daemons. This in turn will cause a method
		// invocation to fail. This should not occur and may indicate
		// an internal logic error in the rgmd.
		// @user_action
		// Look for other syslog error messages on the same node. Save
		// a copy of the /var/adm/messages files on all nodes, and
		// report the problem to your authorized Sun service provider.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Method <%s>: unknown command."),
		    fed_argv0);
		sc_syslog_msg_done(&handle);

		cl_result_code = CL_RC_FAILED;
		res.err_code = SCHA_ERR_VALIDATE;
		rgm_format_errmsg(&res, REM_FED_UNKNC,
		    fed_argv0);
		break;

	case FE_ACCESS:
		err_str = security_get_errormsg(fed_result.sec_errno);
		if (err_str) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s>, node <%s>: "
			    "authorization error: %s."),
			    fed_argv0, l->lv_rname, nodename,
			    err_str);
			sc_syslog_msg_done(&handle);

			/* create an error message */
			rgm_format_errmsg(&res, REM_FED_AUTH,
			    fed_argv0, l->lv_rname, err_str);
			free(err_str);
		} else {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "Method <%s> on resource <%s>, node <%s>: "
			    "authorization error: %d."),
			    fed_argv0, l->lv_rname, nodename,
			    fed_result.sec_errno);
			sc_syslog_msg_done(&handle);

			/* create an error message */
			rgm_format_errmsg(&res, REM_FED_AUTH,
			    fed_argv0, l->lv_rname, "");
		}
		cl_result_code = CL_RC_FAILED;
		res.err_code = SCHA_ERR_VALIDATE;
		break;

	case FE_CONNECT:
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "Method <%s> on resource <%s>, node <%s>: RPC connection "
		    "error."), fed_argv0, l->lv_rname, nodename);
		sc_syslog_msg_done(&handle);

		rgm_format_errmsg(&res, REM_FED_CONNECT,
		    fed_argv0, l->lv_rname);

		if (fed_result.sys_errno == RPC_PROGNOTREGISTERED) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT(
			    "rpc.fed: program not registered "
			    "or server not started"));
			sc_syslog_msg_done(&handle);
		}
		cl_result_code = CL_RC_FAILED;
		res.err_code = SCHA_ERR_VALIDATE;
		break;

	//
	// SCMSGS
	// @explanation
	// A VALIDATE method execution has exceeded its configured
	// timeout and was killed by the rgmd. This in turn will cause
	// the failure of a creation or property update operation on a
	// resource or resource group.
	// @user_action
	// Consult data service documentation to diagnose the cause of the
	// method failure. Other syslog messages occurring just before this one
	// might indicate the reason for the failure. After correcting the
	// problem that caused the method to fail, the operator can retry
	// the operation that failed.
	//
	case FE_TIMEDOUT:
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "Method <%s> on resource <%s>, resource group <%s>, node "
		    "<%s>: Timed out."),
		    fed_argv0, l->lv_rname, l->lv_rgname, nodename);
		sc_syslog_msg_done(&handle);
		res.err_code = SCHA_ERR_VALIDATE;
		cl_result_code = CL_RC_TIMEOUT;

		rgm_format_errmsg(&res, REM_FED_TIMEDOUT,
		    fed_argv0, l->lv_rname);

		break;

	// explanation, user action is in file rgm_scha_control_impl.cc
	case FE_QUIESCED:
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "Method <%s> on resource <%s>, node <%s> killed to achieve "
		    "user-initiated fast quiesce of the resource group <%s>."),
		    fed_argv0, l->lv_rname, nodename, l->lv_rgname);
		sc_syslog_msg_done(&handle);
		res.err_code = SCHA_ERR_VALIDATE;
		// needs to be changed to quiesce
		cl_result_code = CL_RC_TIMEOUT;

		rgm_format_errmsg(&res, REM_FED_QUIESCED,
		    fed_argv0, l->lv_rname);

		break;

	case FE_UNKILLABLE:
		/*
		 * This is a serious case.  We must reboot the
		 * node immediately.
		 */

		//
		// See block comment under "FE_UNKILLABLE" case in
		// launch_method().  Same issues apply here.
		//
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, SYSTEXT("fatal: Aborting this node because "
		    "method <%s> on resource <%s> for node <%s> is unkillable"),
		    fed_argv0, l->lv_rname, nodename);
		sc_syslog_msg_done(&handle);

		// publish a event
#if SOL_VERSION >= __s10
		(void) sc_publish_zc_event(ZONE, ESC_CLUSTER_RG_NODE_REBOOTED,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_R_NAME, SE_DATA_TYPE_STRING,
		    l->lv_rname,
		    CL_RG_NAME, SE_DATA_TYPE_STRING,
		    l->lv_rgname,
		    CL_NODE_NAME, SE_DATA_TYPE_STRING,
		    fed_cmd.host, NULL);
#else
		(void) sc_publish_event(ESC_CLUSTER_RG_NODE_REBOOTED,
		    CL_EVENT_PUB_RGM, CL_EVENT_SEV_CRITICAL,
		    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
		    CL_R_NAME, SE_DATA_TYPE_STRING,
		    l->lv_rname,
		    CL_RG_NAME, SE_DATA_TYPE_STRING,
		    l->lv_rgname,
		    CL_NODE_NAME, SE_DATA_TYPE_STRING,
		    fed_cmd.host, NULL);
#endif
		// reboot the physical node
		abort_node(B_TRUE, System_dumpflag, NULL);

		// NOTREACHABLE
		break;

	case FE_NOTAG:
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE, SYSTEXT(
		    "Method <%s> on resource <%s> node <%s>: "
		    "Execution failed: no such method tag."),
		    fed_argv0, l->lv_rname, nodename);
		sc_syslog_msg_done(&handle);
		res.err_code = SCHA_ERR_VALIDATE;
		cl_result_code = CL_RC_FAILED;

		rgm_format_errmsg(&res, REM_FED_NOTAG,
		    fed_argv0, l->lv_rname);

		break;

	case FE_PROJECT_NAME_INVALID:
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Either the given project name doesn't exist, or
		// root is not a valid user of the given project.
		// @user_action
		// Check if the project name is valid and root is a
		// valid user of that project.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("Invalid project name: %s"),
		    fed_cmd.project_name);
		sc_syslog_msg_done(&handle);
		cl_result_code = CL_RC_FAILED;
		res.err_code = SCHA_ERR_VALIDATE;
		rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
		    SCHA_RESOURCE_PROJECT_NAME);

		break;

	default:
		// log message and abort node
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// A serious error has occurred in the communication between
		// rgmd and rpc.fed while attempting to execute a VALIDATE
		// method. The rgmd will produce a core file and will force
		// the node to halt or reboot to avoid the possibility of data
		// corruption.
		// @user_action
		// Save a copy of the /var/adm/messages files on all nodes,
		// and of the rgmd core file. Contact your authorized Sun
		// service provider for assistance in diagnosing the problem.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: Received unexpected result "
		    "<%d> from rpc.fed, aborting node"),
		    fed_result.type);
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}

	// Get the method name for posting the event
	method_name = strdup(rt_method_string(METH_VALIDATE));

	// publish an event
#if SOL_VERSION >= __s10
	(void) sc_publish_zc_event(ZONE, ESC_CLUSTER_R_METHOD_COMPLETED,
	    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
	    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_R_NAME, SE_DATA_TYPE_STRING,
	    l->lv_rname,
	    CL_RG_NAME, SE_DATA_TYPE_STRING,
	    l->lv_rgname,
	    CL_METHOD_NAME, SE_DATA_TYPE_STRING, method_name,
	    CL_METHOD_DURATION, SE_DATA_TYPE_TIME, method_run_time,
	    CL_METHOD_TIMEOUT, SE_DATA_TYPE_UINT32, fed_cmd.timeout,
	    CL_METHOD_PATH, SE_DATA_TYPE_STRING, fed_opts_cmd_path,
	    CL_RESULT_CODE, DATA_TYPE_UINT32, cl_result_code, NULL);
#else

	(void) sc_publish_event(ESC_CLUSTER_R_METHOD_COMPLETED,
	    CL_EVENT_PUB_RGM, CL_EVENT_SEV_INFO,
	    CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_R_NAME, SE_DATA_TYPE_STRING,
	    l->lv_rname,
	    CL_RG_NAME, SE_DATA_TYPE_STRING,
	    l->lv_rgname,
	    CL_METHOD_NAME, SE_DATA_TYPE_STRING, method_name,
	    CL_METHOD_DURATION, SE_DATA_TYPE_TIME, method_run_time,
	    CL_METHOD_TIMEOUT, SE_DATA_TYPE_UINT32, fed_cmd.timeout,
	    CL_METHOD_PATH, SE_DATA_TYPE_STRING, fed_opts_cmd_path,
	    CL_RESULT_CODE, DATA_TYPE_UINT32, cl_result_code, NULL);
#endif

finished:
	ucmm_print("RGM",
	    NOGET("launch_validate: exit launch_validate_method, "
	    "R <%s>, meth <%s>\n"),
	    l->lv_rname, fed_argv0);

	/* free the xdr resources */
	xdr_free((xdrproc_t)xdr_fe_run_result, (char *)&fed_result);

	free(fed_opts_cmd_path);
	free(fed_opts_argv);
	free(real_method_name);
	free(method_name);
	// free project name strdup-ed by function get_project_name
	if (fed_cmd.project_name)
		free(fed_cmd.project_name);
	if (fed_cmd.corepath)
		free(fed_cmd.corepath);

	/* Free our environment variables */
	for (i = 0; i < NUM_ENV_VARS; i++) {
		free(val_env_vars[i]);
	}
	free(nodename);
	free(zonename);
	return (res);
}
