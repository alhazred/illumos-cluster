/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_recep_start.cc	1.34	08/07/21 SMI"


/*
 * Main for the recep daemon.
 */

#include <stdio.h>
#include <stdlib.h> /* getenv, exit */
#include <unistd.h>

#ifdef linux
#include <rgm/rpc_services.h>
#endif
#include <errno.h>
#include <rgm_proto.h>
#include <rgm/security.h>

#include <rgm/rgm_recep.h>

/*
 * to store the door id returned after door_create,
 * will be used for attaching other non-global zones to
 * the door server.
 */
#if DOOR_IMPL
int door_id;
#endif

#include <rgm/rgm_msg.h>
#include <rgm_threads.h>
#include <sys/sc_syslog_msg.h>
#include <sys/rsrc_tag.h>

#if !DOOR_IMPL
extern "C" {
void scha_program_1(struct svc_req *rqstp, register SVCXPRT *transp);
}



// the following variable is used by the rgm_recep_svc.cc file (which
// generate by rpcgen.  If this variable is set to 1, the rpc output will be
// send to syslogd, which depending on the configuration of /etc/syslog.conf.
// Otherwise, it will be placed on strm.
int _rpcpmstart = 0;
#endif

extern char RGMD_RECEPTIONIST_NAME[];

/*
 * receptionist thread processes the request from the rgmd
 */
void
recep_thread_start()
{
	sc_syslog_msg_handle_t handle;
#if !DOOR_IMPL
#ifdef linux
	/*
	 * The default stack size on Solaris is about 1m while it is
	 * about 10m on Linux.  On Linux, with that much default stack
	 * size, we are not able to create couple hundred threads (e.g. 512).
	 * So, we need to change the default stack size.
	 * PTHREAD_STACK_MIN(16k) * 64 = 1MB.
	 */
	int stacksz = PTHREAD_STACK_MIN * 64;
#endif
	int cur_rpc_threads = MAX_SCHA_CONTROLS_INIT *
	    RATIO_NB_THREADS_VS_MAX_SCHA_CONTROL;

#ifndef linux
	int mode = RPC_SVC_MT_AUTO;

	/*
	 * set MT mode
	 */
	if (!rpc_control(RPC_SVC_MTMODE_SET, &mode)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd failed in a call to rpc_control(3N). This error
		// should never occur. If it did, it would cause the failure
		// of subsequent invocations of scha_cmds(1HA) and
		// scha_calls(3HA). This would most likely lead to resource
		// method failures and prevent RGM reconfigurations from
		// occurring. The rgmd will produce a core file and will force
		// the node to halt or reboot.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the source of the problem can be identified.
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing the problem. Reboot the node to restart the
		// clustering daemons.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: rpc_control() failed to set "
		    "automatic MT mode; aborting node"));
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}

	/*
	 * Initialize default for maximum RPC threads.
	 */
	if (!rpc_control(RPC_SVC_THRMAX_SET, &cur_rpc_threads)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd failed in a call to rpc_control(3N). This error
		// should never occur. If it did, it prevent to dynamically
		// adjust the maximum number of threads that the RPC library
		// can create, and as a result could prevent a maximum number
		// of some scha_call to be handled at the same time.
		// @user_action
		// Examine other syslog messages occurring at about the same
		// time to see if the source of the problem can be identified.
		// Save a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for assistance
		// in diagnosing the problem. Reboot the node to restart the
		// clustering daemons.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: rpc_control() failed to set "
		    "the maximum number of threads"));
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}
#endif /* !linux */

#ifdef linux
	if (rpc_init_rpc_mgr(
	    cur_rpc_threads, Debugflag, stacksz) != RPC_SERVICES_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: rpc_init_rpc_mgr() failed"));
		sc_syslog_msg_done(&handle);
		abort();
	}
#endif

	/*
	 * Initialize loopbackset array and rpc service.
	 */
	if (security_svc_reg(scha_program_1, SCHA_PROGRAM_NAME,
	    SCHA_PROGRAM, SCHA_VERSION, 0) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to start up successfully because
		// it failed to register an RPC service. It will produce a
		// core file and will force the node to halt or reboot.
		// @user_action
		// If rebooting the node doesn't fix the problem, examine
		// other syslog messages occurring at about the same time to
		// see if the problem can be identified and if it recurs. Save
		// a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for
		// assistance.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: unable to register RPC service; "
		    "aborting node"));
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}
#else
	door_id = security_svc_reg(&recep_door_server,
	    RGM_SCHA_PROGRAM_CODE, 0, ZONE);

	if (door_id < 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd daemon was unable to start up successfully because
		// it failed to register a door service. It will produce a
		// core file and will force the node to halt or reboot.
		// @user_action
		// If rebooting the node doesn't fix the problem, examine
		// other syslog messages occurring at about the same time to
		// see if the problem can be identified and if it recurs. Save
		// a copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for
		// assistance.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: unable to register door service; "
		    "aborting node"));
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}
#endif
}
