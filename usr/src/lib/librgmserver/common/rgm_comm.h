/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_COMM_H
#define	_RGM_COMM_H

#pragma ident	"@(#)rgm_comm.h	1.14	08/05/20 SMI"

#include <rgm_state.h>
#include <h/rgm.h>
#include <rgm/rgm_util.h>

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

//
// Design note:
//
// Before making an IDL call we need to get an object reference for the
// rgm_comm object on the node we're calling.  If we fail to get a
// reference, we do not make the call, and we return RGMIDL_NOREF.
//
// In IDL calls we expect to get exceptions COMM_FAILURE and INV_OBJREF.
// The former maps to node death; the latter to rgmd death.  We neither
// expect nor handle any other type of exception.  This "shouldn't
// happen", but if it does, we do log an error message and return
// RGMIDL_UNKNOWN.  See except_to_errcode().
//
// The callers of these IDL functions need to avoid any "split brain"
// windows that might occur in between the time rgmd dies and the node
// aborts.  In some cases the calling node knows it is going to be called
// back by UCMM due to a change in membership, and may choose to ignore
// the error based on that fact.  The important thing is to avoid starting
// up an RG prematurely on another node before the node with a dead rgmd
// actually aborts.  The RG could still be running on the node with the
// dead rgmd, and data corruption could result.
//
typedef enum idlretvals {
	RGMIDL_OK,
	RGMIDL_NOREF,		// name service returned nil
	RGMIDL_RGMD_DEATH,	// if we get this, we'd get node death next
	RGMIDL_NODE_DEATH,
	RGMIDL_UNKNOWN
} idlretval_t;

extern idlretval_t except_to_errcode(Environment &e, sol::nodeid_t nodeid,
    rgm::lni_t = LNI_UNDEF);

#ifndef EUROPA_FARM
extern idlretval_t rgm_change_mastery(sol::nodeid_t i, namelist_t *on_list,
    namelist_t *off_list);
extern idlretval_t rgm_comm_I_am_President(sol::nodeid_t i, sol::nodeid_t pres);
extern idlretval_t rgm_comm_getstate(sol::nodeid_t i, rgm::node_state *&state);
#endif
extern idlretval_t rgm_resource_restart(sol::nodeid_t slave,
    rgm::rgm_restart_flag rrflag, const char *r_name);
extern idlretval_t wake_president(const rgm::lni_t lni,
    boolean_t fetch_immediate = B_FALSE);
#ifndef EUROPA_FARM
extern idlretval_t who_is_joining(LogicalNodeset &curr_ns,
		LogicalNodeset *out_joining_ns);
extern idlretval_t joiners_read_ccr(LogicalNodeset& join_ns);
extern idlretval_t joiners_run_boot_methods(LogicalNodeset& join_ns);
extern idlretval_t slaves_record_membership(nodeset_t curr_ns);
extern idlretval_t are_you_joining(sol::nodeid_t n, boolean_t *n_is_joining);
#endif
extern void notify_pres_state_change(sol::nodeid_t, rgm::notify_change_tag,
    const char *, rgm::rgm_r_state, rgm::rgm_rg_state);

#ifndef EUROPA_FARM
// Helper functions for the inter-rg dependencies idl calls
extern idlretval_t call_ack_start(rgm::lni_t lni, const char *r_name);
extern idlretval_t call_notify_dependencies_resolved(rgm::lni_t lni,
    const char *r_name);
extern idlretval_t call_fetch_restarting_flag(rgm::lni_t lni,
    const char *r_name, rgm::r_restart_t &restart_flag);
#endif

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* !_RGM_COMM_H */
