/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)scsymon_cluster.c	1.7	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <scadmin/scsymon_srv.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <scsymon_private.h>

/*
 * scsymon_get_cluster_config
 *
 * get the following properties of a cluster:
 *	cluster name
 *	install mode
 *	private net
 *	private netmask
 *	add node auth type
 *	add node auth list
 *
 * Upon success, an object of scsymon_cluster_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_cluster_config().
 *
 * INPUT: pcluster_config
 * OUTPUT:ppcluster
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_ECLUSTERRECONFIG	- cluster is reconfiguring
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_cluster_config(scconf_cfg_cluster_t *pcluster_config,
    scsymon_cluster_config_t **ppcluster)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scsymon_cluster_config_t *pcluster = NULL;

	if (pcluster_config == NULL)
		return (scsymon_error);

	if (ppcluster == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	pcluster = (scsymon_cluster_config_t *)calloc(1,
	    sizeof (scsymon_cluster_config_t));
	if (pcluster == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}

	/* fill in cluster properties */
	pcluster->pname =
	    strdup((pcluster_config->scconf_cluster_clustername == NULL) ?
	    SCSYMON_NULL : pcluster_config->scconf_cluster_clustername);
	if (pcluster->pname == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}

	pcluster->pinstall_mode =
	    strdup((pcluster_config->scconf_cluster_installmode == NULL) ?
	    SCSYMON_NULL : pcluster_config->scconf_cluster_installmode);
	if (pcluster->pinstall_mode == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}

	pcluster->pprivnetaddr =
	    strdup((pcluster_config->scconf_cluster_privnetaddr == NULL) ?
	    SCSYMON_NULL : pcluster_config->scconf_cluster_privnetaddr);
	if (pcluster->pprivnetaddr == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}

	pcluster->pprivnetmask =
	    strdup((pcluster_config->scconf_cluster_privnetmask == NULL) ?
	    SCSYMON_NULL : pcluster_config->scconf_cluster_privnetmask);
	if (pcluster->pprivnetmask == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}

	if (pcluster_config->scconf_cluster_authtype == SCCONF_AUTH_SYS)
		pcluster->pauthtype = strdup(SCSYMON_AUTH_SYS);
	else
		pcluster->pauthtype = strdup(SCSYMON_AUTH_DH);
	if (pcluster->pauthtype == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}

	if (pcluster_config->scconf_cluster_authlist == NULL) {
		pcluster->pauthlist = strdup(SCSYMON_NULL);
		if (pcluster->pauthlist == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
	} else {
		scsymon_error = symon_get_scconf_namelist(\
		    pcluster_config->scconf_cluster_authlist,
		    &pcluster->pauthlist);
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;
	}

	*ppcluster = pcluster;

	pcluster = NULL; /* so we don't accidentally delete it */

cleanup:
	scsymon_free_cluster_config(pcluster);

	return (scsymon_error);
}

/*
 * scsymon_free_cluster_config
 *
 * free cluster properties
 */
void
scsymon_free_cluster_config(scsymon_cluster_config_t *pcluster)
{
	if (pcluster != NULL) {
		if (pcluster->pname != NULL)
			free(pcluster->pname);
		if (pcluster->pinstall_mode != NULL)
			free(pcluster->pinstall_mode);
		if (pcluster->pprivnetaddr != NULL)
			free(pcluster->pprivnetaddr);
		if (pcluster->pprivnetmask != NULL)
			free(pcluster->pprivnetmask);
		if (pcluster->pauthtype != NULL)
			free(pcluster->pauthtype);
		if (pcluster->pauthlist != NULL)
			free(pcluster->pauthlist);

		free(pcluster);
	}
}

/*
 * scsymon_get_cluster_status
 *
 * get the following properties of a cluster:
 *	cluster name
 *	minimum votes required
 *	current votes
 *
 * Upon success, an object of scsymon_cluster_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_cluster_status().
 *
 * INPUT: pcluster_status
 * OUTPUT:ppcluster
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_ECLUSTERRECONFIG	- cluster is reconfiguring
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_cluster_status(scstat_cluster_t *pcluster_status,
    scsymon_cluster_status_t **ppcluster)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scsymon_cluster_status_t *pcluster = NULL;

	if (pcluster_status == NULL)
		return (scsymon_error);

	if (ppcluster == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	pcluster = (scsymon_cluster_status_t *)calloc(1,
	    sizeof (scsymon_cluster_status_t));
	if (pcluster == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}

	/* fill in cluster properties */
	pcluster->pname =
	    strdup((pcluster_status->scstat_cluster_name == NULL) ?
	    SCSYMON_NULL : pcluster_status->scstat_cluster_name);
	if (pcluster->pname == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}

	/* fill in cluster quorum */
	pcluster->current_vote =
	    pcluster_status->scstat_quorum->scstat_vote;
	pcluster->min_vote =
	    pcluster_status->scstat_quorum->scstat_vote_needed;

	*ppcluster = pcluster;

	pcluster = NULL; /* so we don't accidentally delete it */

cleanup:
	scsymon_free_cluster_status(pcluster);

	return (scsymon_error);
}

/*
 * scsymon_free_cluster_status
 *
 * free cluster status
 */
void
scsymon_free_cluster_status(scsymon_cluster_status_t *pcluster)
{
	if (pcluster != NULL) {
		if (pcluster->pname != NULL)
			free(pcluster->pname);

		free(pcluster);
	}
}
