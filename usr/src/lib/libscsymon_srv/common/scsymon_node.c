/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scsymon_node.c	1.17	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <scadmin/scsymon_srv.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <scsymon_private.h>

/*
 * scsymon_get_node_configs
 *
 * get the following properties of all nodes configured for a cluster
 *	node name
 *	votes configured
 *	private net hostname
 *	transport adapter list
 *
 * Upon success, a list of objects of scsymon_node_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_node_configs().
 *
 * INPUT: pcluster_config
 * OUTPUT:pplnodes
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_node_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_node_config_t **pplnodes)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scconf_cfg_node_t *pnode_config = NULL;
	char *node_name = NULL;
	scsymon_node_config_t *pnode_current = NULL;
	scsymon_node_config_t *pnode = NULL;

	if (pcluster_config == NULL)
		return (scsymon_error);

	if (pplnodes == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* node info */
	pnode_config = pcluster_config->scconf_cluster_nodelist;

	while (pnode_config != NULL) {
		node_name = pnode_config->scconf_node_nodename;
		if (node_name == NULL) {
			scsymon_error = SCSYMON_EUNEXPECTED;
			goto cleanup;
		}

		/* fill in property and state for each node */
		pnode = (scsymon_node_config_t *)calloc(1,
		    sizeof (scsymon_node_config_t));
		if (pnode == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* node name */
		pnode->pname = strdup((node_name == NULL) ? SCSYMON_NULL :
		    node_name);
		if (pnode->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
		pnode->default_vote =
		    (unsigned int)pnode_config->scconf_node_qvotes;

		/* node configuration */
		pnode->pprivnethostname =
		    strdup((pnode_config->scconf_node_privatehostname == NULL) ?
		    SCSYMON_NULL :
		    pnode_config->scconf_node_privatehostname);
		if (pnode->pprivnethostname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		scsymon_error = symon_get_scconf_adapterlist(
		    pnode_config->scconf_node_adapterlist,
		    &pnode->padapterlist);
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		/* add to the list */
		if (pnode_current == NULL) { /* list empty */
			*pplnodes = pnode;
			pnode_current = pnode;
		} else {
			pnode_current->pnext = pnode;
			pnode_current = pnode_current->pnext;
		}

		pnode = NULL; /* so we don't accidentally delete it */

		pnode_config = pnode_config->scconf_node_next;
	}

cleanup:
	if (pnode != NULL)
		scsymon_free_node_configs(pnode);

	return (scsymon_error);
}

/*
 * scsymon_free_node_configs
 *
 * free cluster node properties
 */
void
scsymon_free_node_configs(scsymon_node_config_t *plnodes)
{
	scsymon_node_config_t *pnode = plnodes;

	while (pnode != NULL) {
		scsymon_node_config_t *pnext;

		if (pnode->pname != NULL)
			free(pnode->pname);
		if (pnode->pprivnethostname != NULL)
			free(pnode->pprivnethostname);
		if (pnode->padapterlist != NULL)
			free(pnode->padapterlist);

		pnext = pnode->pnext;
		free(pnode);
		pnode = pnext;
	}
}

/*
 * scsymon_get_node_device_configs
 *
 * get the following properties of devices associated with a node.
 *	node name
 *	quorum device list
 *	possible mastered device group list
 *	possible mastered RG group list
 *
 * Upon success, a list of objects of scsymon_node_device_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_node_device_configs().
 *
 * INPUT: pcluster_config
 * OUTPUT:pplnodedevs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_node_device_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_node_device_config_t **pplnodedevs)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_node_t *pnode_config = NULL;
	scconf_cfg_qdev_t *pqdev_config = NULL;
	scconf_cfg_qdev_t *ptr_qdev_config = NULL;
	scconf_cfg_qdevport_t *pqdevport = NULL;
	char *qdev_name = NULL;
	scconf_cfg_ds_t *pdevgrp_config = NULL;
	scconf_cfg_ds_t *ptr_ds_config = NULL;
	char *devgrp_name = NULL;
	scconf_nodeid_t *pnodeid = NULL;
	scsymon_rgm_rg_rs_t *plrgm_rgs = NULL;
	scsymon_rgm_rg_rs_t *ptr_rgm_rg = NULL;
	rgm_rg_t *prgm_rg = NULL;
	char *rg_name;
	scsymon_node_device_config_t *pnodedev = NULL;
	scsymon_node_device_config_t *pnodedev_current = NULL;
	char *pnodename = NULL;
	char *node_name = NULL;
	nodeidlist_t *pnode = NULL;
	char *buf = NULL;
	unsigned int len;
	unsigned int old_len = 0;
	unsigned int buf_size;
	char *delimiter = NULL;

	if (pcluster_config == NULL)
		return (scsymon_error);

	if (pplnodedevs == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* node info */
	pnode_config = pcluster_config->scconf_cluster_nodelist;

	/* quorum device config */
	pqdev_config = pcluster_config->scconf_cluster_qdevlist;

	/* get devive group config */
	scconf_error = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG,
	    &pdevgrp_config);
	if (scconf_error != SCCONF_NOERR) {
		scsymon_error =
		    scsymon_convert_scconf_error_code(scconf_error);

		goto cleanup;
	}

	/* get rg config */
	scsymon_error = scsymon_get_rgm_rg_rss(&plrgm_rgs);
	if (scsymon_error != SCSYMON_ENOERR)
		goto cleanup;
	while (pnode_config != NULL) {
		node_name = pnode_config->scconf_node_nodename;
		if (node_name == NULL) {
			scsymon_error = SCSYMON_EUNEXPECTED;
			goto cleanup;
		}

		ptr_qdev_config = pqdev_config;
		ptr_ds_config = pdevgrp_config;
		ptr_rgm_rg = plrgm_rgs;

		/* fill in property and state for each node */
		pnodedev = (scsymon_node_device_config_t *)calloc(1,
		    sizeof (scsymon_node_device_config_t));
		if (pnodedev == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* node name */
		pnodedev->pname = strdup((node_name == NULL) ? SCSYMON_NULL :
		    node_name);
		if (pnodedev->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		len = 0;
		buf_size = SCSYMON_MAX_STRING_LEN;
		buf = (char *)calloc(buf_size, sizeof (char));
		if (buf == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
		(void) memset(buf, NULL, buf_size * sizeof (char));

		/* quorum device accessible from the node */
		while (ptr_qdev_config != NULL) {
			qdev_name = ptr_qdev_config->scconf_qdev_name;
			pqdevport = ptr_qdev_config->scconf_qdev_ports;

			/*
			 * concatenate names of every port of the quorum device
			 * and place the result into buf
			 */
			while (pqdevport != NULL) {
				if (pqdevport->scconf_qdevport_nodename ==
				    NULL || node_name == NULL) {
					scsymon_error = SCSYMON_EUNEXPECTED;
					goto cleanup;
				}

				if (strcmp(pqdevport->scconf_qdevport_nodename,
				    node_name) == 0) {
					delimiter = (len == 0) ?
					    SCSYMON_LIST_HEAD :
					    SCSYMON_LIST_DELIMITER;

					(void) snprintf(buf + len,
					    buf_size - len, "%s%s",
					    delimiter, qdev_name);

					old_len = len;
					len = strlen(buf);

					break;
				}
				pqdevport = pqdevport->scconf_qdevport_next;
			}

			if (len >= buf_size - 1) {
				scsymon_error =
				    scsymon_double_buffer_size(\
				    buf_size,
				    buf,
				    &buf_size,
				    &buf);

				if (scsymon_error != SCSYMON_ENOERR)
					goto cleanup;

				len = old_len;
				continue;
			}

			ptr_qdev_config =
			    ptr_qdev_config->scconf_qdev_next;
		}

		pnodedev->pqdevlist = buf;
		buf = NULL;

		len = 0;
		buf_size = SCSYMON_MAX_STRING_LEN;
		buf = (char *)calloc(buf_size, sizeof (char));
		if (buf == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
		(void) memset(buf, NULL, buf_size * sizeof (char));

		/*
		 * device group which the node is potential primary
		 */
		while (ptr_ds_config != NULL) {
			devgrp_name = ptr_ds_config->scconf_ds_name;
			pnodeid = ptr_ds_config->scconf_ds_nodelist;

			if (len >= SCSYMON_MAX_STRING_LEN - 1)
				break;

			/*
			 * concatenate names of every node which is the
			 * potential primary of the device group and place
			 * the result into buf
			 */
			while ((pnodeid != NULL) && (*pnodeid != NULL)) {
				scconf_error = scconf_get_cached_nodename(
				    pcluster_config, *pnodeid, &pnodename);
				if (scconf_error != SCCONF_NOERR) {
					scsymon_error =
					    scsymon_convert_scconf_error_code(
					    scconf_error);

					goto cleanup;
				}
				if (pnodename == NULL) {
					scsymon_error = SCSYMON_EINVAL;
					goto cleanup;
				}
				if (node_name == NULL) {
					scsymon_error = SCSYMON_EUNEXPECTED;
					goto cleanup;
				}
				if (strcmp(pnodename, node_name) == 0) {
					delimiter = (len == 0) ?
					    SCSYMON_LIST_HEAD :
					    SCSYMON_LIST_DELIMITER;

					(void) snprintf(buf + len,
					    buf_size - len, "%s%s",
					    delimiter, devgrp_name);

					if (pnodename != NULL) {
						free(pnodename);
						pnodename = NULL;
					}

					old_len = len;
					len = strlen(buf);

					break;
				}

				if (pnodename != NULL) {
					free(pnodename);
					pnodename = NULL;
				}
				pnodeid++;
			}

			/* double the buffer if necessary */
			if (len >= buf_size - 1) {
				scsymon_error =
				    scsymon_double_buffer_size(\
				    buf_size,
				    buf,
				    &buf_size,
				    &buf);

				if (scsymon_error != SCSYMON_ENOERR)
					goto cleanup;

				len = old_len;
				continue;
			}

			ptr_ds_config =
			    ptr_ds_config->scconf_ds_next;
		}

		pnodedev->pdevgrplist = buf;
		buf = NULL;

		len = 0;
		buf = (char *)calloc(buf_size, sizeof (char));
		if (buf == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
		(void) memset(buf, NULL, buf_size * sizeof (char));

		/*
		 * resource group which the node is potential primary
		 */
		while (ptr_rgm_rg != NULL) {
			prgm_rg = ptr_rgm_rg->prgm_rg;
			rg_name = prgm_rg->rg_name;
			pnode = prgm_rg->rg_nodelist;

			/*
			 * concatenate names of every node which is the
			 * potentail primary of the RG and place the result
			 * into buf
			 */

			if (node_name == NULL) {
				scsymon_error = SCSYMON_EUNEXPECTED;
				goto cleanup;
			}

			while (pnode != NULL) {

				if (pnode->nl_nodeid ==
				    pnode_config->scconf_node_nodeid) {
					delimiter = (len == 0) ?
					    SCSYMON_LIST_HEAD :
					    SCSYMON_LIST_DELIMITER;

					(void) snprintf(buf + len,
					    buf_size - len, "%s%s",
					    delimiter, rg_name);

					old_len = len;
					len = strlen(buf);

					break;
				}
				pnode = pnode->nl_next;
			}

			if (len >= buf_size - 1) {
				scsymon_error =
				    scsymon_double_buffer_size(\
				    buf_size,
				    buf,
				    &buf_size,
				    &buf);

				if (scsymon_error != SCSYMON_ENOERR)
					goto cleanup;

				len = old_len;
				continue;
			}

			ptr_rgm_rg =
			    ptr_rgm_rg->pnext;
		}

		pnodedev->prglist = buf;
		buf = NULL;

		/* add to the list */
		if (pnodedev_current == NULL) { /* list empty */
			*pplnodedevs = pnodedev;
			pnodedev_current = pnodedev;
		} else {
			pnodedev_current->pnext = pnodedev;
			pnodedev_current = pnodedev_current->pnext;
		}

		pnodedev = NULL; /* so we don't accidentally delete it */

		pnode_config = pnode_config->scconf_node_next;
	}

cleanup:
	if (buf != NULL)
		free(buf);

	if (pdevgrp_config != NULL)
		scconf_free_ds_config(pdevgrp_config);

	if (plrgm_rgs != NULL)
		scsymon_free_rgm_rg_rss(plrgm_rgs);

	if (pnodedev != NULL)
		scsymon_free_node_device_configs(pnodedev);

	if (pnodename != NULL)
		free(pnodename);

	return (scsymon_error);
}

/*
 * scsymon_free_node_device_configs
 *
 * free cluster node device properties
 */
void
scsymon_free_node_device_configs(scsymon_node_device_config_t *plnodedevs)
{
	scsymon_node_device_config_t *pnodedev = plnodedevs;

	while (pnodedev != NULL) {
		scsymon_node_device_config_t *pnext;

		if (pnodedev->pname != NULL)
			free(pnodedev->pname);
		if (pnodedev->pqdevlist != NULL)
			free(pnodedev->pqdevlist);
		if (pnodedev->pdevgrplist != NULL)
			free(pnodedev->pdevgrplist);
		if (pnodedev->prglist != NULL)
			free(pnodedev->prglist);
		pnext = pnodedev->pnext;
		free(pnodedev);
		pnodedev = pnext;
	}
}

/*
 * scsymon_get_node_statuss
 *
 * get the status of all nodes configured for a cluster
 *	node name
 *	node state
 *	current votes
 *
 * Upon success, a list of objects of scsymon_node_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_node_statuss().
 *
 * INPUT: pcluster_status
 * OUTPUT:pplnodes
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_node_statuss(scstat_cluster_t *pcluster_status,
    scsymon_node_status_t **pplnodes)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scstat_node_t *pnode_status = NULL;
	char *node_name = NULL;
	scstat_node_quorum_t *ptr_node_quorum = NULL;
	scstat_node_quorum_t *pnode_quorum = NULL;
	scsymon_node_status_t *pnode_current = NULL;
	scsymon_node_status_t *pnode = NULL;

	if (pcluster_status == NULL)
		return (scsymon_error);

	if (pplnodes == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* node info */
	pnode_status = pcluster_status->scstat_node_list;
	pnode_quorum = pcluster_status->scstat_quorum->scstat_node_quorum_list;

	while (pnode_status != NULL) {
		node_name = pnode_status->scstat_node_name;
		if (node_name == NULL) {
			scsymon_error = SCSYMON_EUNEXPECTED;
			goto cleanup;
		}

		ptr_node_quorum = pnode_quorum;

		/* fill in property and state for each node */
		pnode = (scsymon_node_status_t *)calloc(1,
		    sizeof (scsymon_node_status_t));
		if (pnode == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* node name */
		pnode->pname = strdup((node_name == NULL) ? SCSYMON_NULL :
		    node_name);
		if (pnode->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		pnode->pstate = strdup((pnode_status->scstat_node_statstr
		    == NULL) ? SCSYMON_NULL :
		    pnode_status->scstat_node_statstr);
		if (pnode->pstate == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* node quorum */
		while (ptr_node_quorum != NULL) {
			if (node_name == NULL ||
			    ptr_node_quorum->scstat_node_name == NULL) {
				scsymon_error = SCSYMON_EUNEXPECTED;
				goto cleanup;
			}
			if (strcmp(ptr_node_quorum->scstat_node_name,
			    node_name) == 0) {
				pnode->current_vote =
				    ptr_node_quorum->scstat_vote_contributed;
				break;
			}
			ptr_node_quorum =
			    ptr_node_quorum->scstat_node_quorum_next;
		}

		/* add to the list */
		if (pnode_current == NULL) { /* list empty */
			*pplnodes = pnode;
			pnode_current = pnode;
		} else {
			pnode_current->pnext = pnode;
			pnode_current = pnode_current->pnext;
		}

		pnode = NULL; /* so we don't accidentally delete it */

		pnode_status = pnode_status->scstat_node_next;
	}

cleanup:
	if (pnode != NULL)
		scsymon_free_node_statuss(pnode);

	return (scsymon_error);
}

/*
 * scsymon_free_node_statuss
 *
 * free cluster node status
 */
void
scsymon_free_node_statuss(scsymon_node_status_t *plnodes)
{
	scsymon_node_status_t *pnode = plnodes;

	while (pnode != NULL) {
		scsymon_node_status_t *pnext;

		if (pnode->pname != NULL)
			free(pnode->pname);
		if (pnode->pstate != NULL)
			free(pnode->pstate);

		pnext = pnode->pnext;
		free(pnode);
		pnode = pnext;
	}
}

/*
 * scsymon_get_node_device_statuss
 *
 * get the status of devices associated with a node.
 *	node name
 *	mastered device group list
 *	mastered RG list
 *
 * Upon success, a list of objects of scsymon_node_device_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_node_device_statuss().
 *
 * INPUT: pcluster_status
 * OUTPUT:pplnodedevs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_node_device_statuss(scstat_cluster_t *pcluster_status,
    scsymon_node_device_status_t **pplnodedevs)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scstat_node_t *pnode_status = NULL;
	scstat_ds_t *pdevgrp_status = NULL;
	scstat_ds_t *ptr_ds_status = NULL;
	char *devgrp_name;
	scstat_ds_node_state_t *preplica;
	scstat_rg_t *prg_status = NULL;
	scstat_rg_t *ptr_rg_status = NULL;
	scstat_rg_status_t *prg_status_by_node = NULL;
	char *rg_name = NULL;
	scsymon_node_device_status_t *pnodedev = NULL;
	scsymon_node_device_status_t *pnodedev_current = NULL;
	char *node_name = NULL;
	char *buf = NULL;
	unsigned int len;
	unsigned int old_len = 0;
	unsigned int buf_size;
	char *delimiter = NULL;

	if (pcluster_status == NULL)
		return (scsymon_error);

	if (pplnodedevs == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* node info */
	pnode_status = pcluster_status->scstat_node_list;
	pdevgrp_status = pcluster_status->scstat_ds_list;
	prg_status = pcluster_status->scstat_rg_list;

	while (pnode_status != NULL) {
		node_name = pnode_status->scstat_node_name;
		if (node_name == NULL) {
			scsymon_error = SCSYMON_EUNEXPECTED;
			goto cleanup;
		}

		ptr_ds_status = pdevgrp_status;
		ptr_rg_status = prg_status;

		/* fill in property and state for each node */
		pnodedev = (scsymon_node_device_status_t *)calloc(1,
		    sizeof (scsymon_node_device_status_t));
		if (pnodedev == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* node name */
		pnodedev->pname = strdup((node_name == NULL) ? SCSYMON_NULL :
		    node_name);
		if (pnodedev->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		len = 0;
		buf_size = SCSYMON_MAX_STRING_LEN;
		buf = (char *)calloc(buf_size, sizeof (char));
		if (buf == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
		(void) memset(buf, NULL, buf_size * sizeof (char));

		/*
		 * device group which the node is the primary
		 */
		while (ptr_ds_status != NULL) {
			devgrp_name = ptr_ds_status->scstat_ds_name;
			preplica = ptr_ds_status->scstat_node_state_list;

			/*
			 * concatenate names of every node which is the primary
			 * of the device group and place the result into buf
			 */
			while (preplica != NULL) {
				if (preplica->scstat_node_name == NULL ||
				    node_name == NULL) {
					scsymon_error = SCSYMON_EUNEXPECTED;
					goto cleanup;
				}

				if (strcmp(preplica->scstat_node_name,
				    node_name) == 0) {
					if (preplica->scstat_node_state ==
					    SCSTAT_PRIMARY) {
						delimiter = (len == 0) ?
						    SCSYMON_LIST_HEAD :
						    SCSYMON_LIST_DELIMITER;

						(void) snprintf(buf + len,
						    buf_size - len, "%s%s",
						    delimiter, devgrp_name);
					}

					old_len = len;
					len = strlen(buf);

					break;
				}
				preplica = preplica->scstat_node_next;
			}

			/* double the buffer if necessary */
			if (len >= buf_size - 1) {
				scsymon_error =
				    scsymon_double_buffer_size(\
				    buf_size,
				    buf,
				    &buf_size,
				    &buf);

				if (scsymon_error != SCSYMON_ENOERR)
					goto cleanup;

				len = old_len;
				continue;
			}

			ptr_ds_status =
			    ptr_ds_status->scstat_ds_next;
		}

		pnodedev->pdevgrplist = buf;
		buf = NULL;

		len = 0;
		buf_size = SCSYMON_MAX_STRING_LEN;
		buf = (char *)calloc(buf_size, sizeof (char));
		if (buf == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
		(void) memset(buf, NULL, buf_size * sizeof (char));

		/*
		 * resource group which the node is the primary
		 */
		while (ptr_rg_status != NULL) {
			rg_name = ptr_rg_status->scstat_rg_name;
			prg_status_by_node =
			    ptr_rg_status->scstat_rg_status_list;

			/*
			 * concatenate names of every node which is the primary
			 * of the resource group and place the result into buf
			 */
			while (prg_status_by_node != NULL) {
				if (prg_status_by_node->scstat_node_name
				    == NULL || node_name == NULL) {
					scsymon_error = SCSYMON_EUNEXPECTED;
					goto cleanup;
				}

				if (strcmp(prg_status_by_node->
				    scstat_node_name, node_name) == 0) {
					if (prg_status_by_node->
					    scstat_rg_status_by_node
					    == SC_STATE_ONLINE) {
						delimiter = (len == 0) ?
						    SCSYMON_LIST_HEAD :
						    SCSYMON_LIST_DELIMITER;

						(void) snprintf(buf + len,
						    buf_size - len, "%s%s",
						    delimiter, rg_name);
					}

					old_len = len;
					len = strlen(buf);

					break;
				}
				prg_status_by_node = prg_status_by_node->\
				    scstat_rg_status_by_node_next;
			}

			/* double the buffer if necessary */
			if (len >= buf_size - 1) {
				scsymon_error =
				    scsymon_double_buffer_size(\
				    buf_size,
				    buf,
				    &buf_size,
				    &buf);

				if (scsymon_error != SCSYMON_ENOERR)
					goto cleanup;

				len = old_len;
				continue;
			}

			ptr_rg_status = ptr_rg_status->scstat_rg_next;
		}

		pnodedev->prglist = buf;
		buf = NULL;

		/* add to the list */
		if (pnodedev_current == NULL) { /* list empty */
			*pplnodedevs = pnodedev;
			pnodedev_current = pnodedev;
		} else {
			pnodedev_current->pnext = pnodedev;
			pnodedev_current = pnodedev_current->pnext;
		}

		pnodedev = NULL; /* so we don't accidentally delete it */

		pnode_status = pnode_status->scstat_node_next;
	}

cleanup:
	if (buf != NULL)
		free(buf);

	if (pnodedev != NULL)
		scsymon_free_node_device_statuss(pnodedev);

	return (scsymon_error);
}

/*
 * scsymon_free_node_device_statuss
 *
 * free cluster node device status
 */
void
scsymon_free_node_device_statuss(scsymon_node_device_status_t *plnodedevs)
{
	scsymon_node_device_status_t *pnodedev = plnodedevs;

	while (pnodedev != NULL) {
		scsymon_node_device_status_t *pnext;

		if (pnodedev->pname != NULL)
			free(pnodedev->pname);
		if (pnodedev->pdevgrplist != NULL)
			free(pnodedev->pdevgrplist);
		if (pnodedev->prglist != NULL)
			free(pnodedev->prglist);

		pnext = pnodedev->pnext;
		free(pnodedev);
		pnodedev = pnext;
	}
}
