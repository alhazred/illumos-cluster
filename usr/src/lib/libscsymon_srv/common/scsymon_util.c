/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scsymon_util.c	1.34	08/07/24 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <locale.h>
#include <libintl.h>
#include <scadmin/scsymon_srv.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <scsymon_private.h>
#include <syslog.h>

/*
 * scsymon_strerr
 *
 * Map scsymon_errno_t to a string.
 *
 * The supplied "text" should be of at least SCSYMON_MAX_STRING_LEN
 * in length.
 */
void
scsymon_strerr(scsymon_errno_t err, char *text)
{
	/* Check arguments */
	if (text == NULL)
		return;

	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Make sure that the buffer is terminated */
	text[SCSYMON_MAX_STRING_LEN - 1] = '\0';

	/* Find the string */
	switch (err) {
	case SCSYMON_ENOERR:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_ENOERR_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EPERM:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EPERM_TEXT), SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_ENOTCLUSTER:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_ENOTCLUSTER_TEXT), SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_ENOTCONFIGURED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_ENOTCONFIGURED_TEXT), SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_ENOMEM:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_ENOMEM_TEXT), SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EINVAL:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EINVAL_TEXT), SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_ESERVICENAME:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_ESERVICENAME_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_ECLUSTERRECONFIG:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_ECLUSTERRECONFIG_TEXT), SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_ERGRECONFIG:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_ERGRECONFIG_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EOBSOLETE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EOBSOLETE_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EDOOROPEN:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EDOOROPEN_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EDOORCALL:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EDOORCALL_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EDOORCREATE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EDOORCREATE_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EDOORBIND:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EDOORBIND_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EORBINIT:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EORBINIT_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EMKDOORDIR:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EMKDOORDIR_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_ECREATEDOORFILE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_ECREATEDOORFILE_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EFORK:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EFORK_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EOPENDEVCONSOLE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EOPENDEVCONSOLE_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_ESETSID:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_ESETSID_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	case SCSYMON_EUNEXPECTED:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EUNEXPECTED_TEXT),
		    SCSYMON_MAX_STRING_LEN - 1);
		break;
	default:
		(void) strncpy(text, dgettext(TEXT_DOMAIN,
		    SCSYMON_EUNKNOWN_TEXT), SCSYMON_MAX_STRING_LEN - 1);
		break;
	}
}

/*
 * scsymon_dbg_print
 *
 * If DEBUG is defined, print the arguments in specified format to stdout.
 * Otherwise, do nothing.
 */
/* ARGSUSED */
void
scsymon_dbg_print(char *format, ...)
{
	va_list parg; /* points to each unnamed arg in turn */
	char *pformat, *sval;
	int ival;
	double dval;

	if (getenv("DEBUG_SCSYMON") == NULL)
		return;

	if (format == NULL)
		return;
	/* lint -save -e40 -e746 -e1055 lint complain about__builtin_va_alist */
	va_start(parg, format);

	for (pformat = format; *pformat; pformat++) {
		if (*pformat != '%') {
			(void) putchar(*pformat);
			continue;
		}

		switch (*++pformat) {
		case 'd':
			ival = va_arg(parg, int); /* lint !e718 */
			(void) printf("%d", ival);
			break;
		case 'f':
			dval = va_arg(parg, double);
			(void) printf("%f", dval);
			break;
		case 's':
			for (sval = va_arg(parg, char *); *sval; sval++)
				(void) putchar(*sval);
			break;
		default:
			(void) putchar(*pformat);
			break;
		}
	}

	va_end(parg);
	/* lint -restore */
}

/*
 * scsymon_convert_scconf_error_code
 *
 * convert a scconf error code to scsymon error code
 *
 * Possible return values:
 *
 *	SCSYMON_NOERR		- success
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_convert_scconf_error_code(const scconf_errno_t scconf_error)
{
	scsymon_errno_t error = SCSYMON_EUNEXPECTED;

	switch (scconf_error) {
	case SCCONF_NOERR:
		error = SCSYMON_ENOERR;
		break;
	case SCCONF_EPERM:
		error = SCSYMON_EPERM;
		break;
	case SCCONF_ENOCLUSTER:
		error = SCSYMON_ENOTCLUSTER;
		break;
	case SCCONF_ENOMEM:
		error = SCSYMON_ENOMEM;
		break;
	case SCCONF_EINVAL:
		error = SCSYMON_EINVAL;
		break;
	default:
		break;
	}
	return (error);
}

/*
 * scsymon_convert_scstat_error_code
 *
 * convert a scstat error code to scsymon error code
 *
 * Possible return values:
 *
 *      SCSYMON_ENOMEM           - not enough memory
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ESERVICENAME	- invalid device group name
 *	SCSYMON_EINVAL		- scconf: invalid argument
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ECLUSTERRECONFIG	- cluster is reconfiguring
 *	SCSYMON_ERGRECONFIG	- RG is reconfiguring
 *	SCSYMON_EOBSOLETE	- Resource/RG has been updated
 *      SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_convert_scstat_error_code(const scstat_errno_t scstat_error)
{
	scsymon_errno_t error = SCSYMON_EUNEXPECTED;

	switch (scstat_error) {
	case SCSTAT_ENOERR:
		error = SCSYMON_ENOERR;
		break;
	case SCSTAT_ENOMEM:
		error = SCSYMON_ENOMEM;
		break;
	case SCSTAT_ENOTCLUSTER:
		error = SCSYMON_ENOTCLUSTER;
		break;
	case SCSTAT_ENOTCONFIGURED:
		error = SCSYMON_ENOTCONFIGURED;
		break;
	case SCSTAT_ESERVICENAME:
		error = SCSYMON_ESERVICENAME;
		break;
	case SCSTAT_EINVAL:
		error = SCSYMON_EINVAL;
		break;
	case SCSTAT_EPERM:
		error = SCSYMON_EPERM;
		break;
	case SCSTAT_ECLUSTERRECONFIG:
		error = SCSYMON_ECLUSTERRECONFIG;
		break;
	case SCSTAT_ERGRECONFIG:
		error = SCSYMON_ERGRECONFIG;
		break;
	case SCSTAT_EOBSOLETE:
		error = SCSYMON_EOBSOLETE;
		break;
	default:
		break;
	}

	return (error);
}

/*
 * symon_convert_scha_error_code
 *
 * convert a scha error code to scsymon error code
 *
 * Possible return values:
 *
 *      SCSYMON_ENOMEM           - not enough memory
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_EINVAL		- scconf: invalid argument
 *	SCSYMON_ECLUSTERRECONFIG	- cluster is reconfiguring
 *	SCSYMON_ERGRECONFIG	- RG is reconfiguring
 *	SCSYMON_EOBSOLETE	- Resource/RG has been updated
 *      SCSYMON_EUNEXPECTED      - internal or unexpected error
 */

/* lint -save -e1746 Lint report a C++ error */
scsymon_errno_t
symon_convert_scha_error_code(const scha_errmsg_t scha_error)
{
	scsymon_errno_t error = SCSYMON_EUNEXPECTED;

	switch (scha_error.err_code) {
	case SCHA_ERR_NOERR:
	case SCHA_ERR_SEQID:
		error = SCSYMON_ENOERR;
		break;
	case SCHA_ERR_NOMEM:
		error = SCSYMON_ENOMEM;
		break;
	case SCHA_ERR_RT:
	case SCHA_ERR_RG:
	case SCHA_ERR_RSRC:
		error = SCSYMON_EOBSOLETE;
		break;
	case SCHA_ERR_RECONF:
	case SCHA_ERR_CLRECONF:
		error = SCSYMON_ECLUSTERRECONFIG;
		break;
	case SCHA_ERR_RGRECONF:
		error = SCSYMON_ERGRECONFIG;
		break;
	case SCHA_ERR_NODE:
	case SCHA_ERR_MEMBER:
		error = SCSYMON_ENOTCLUSTER;
		break;
	case SCHA_ERR_INVAL:
		error = SCSYMON_EINVAL;
		break;
	case SCHA_ERR_CCR:
	case SCHA_ERR_ACCESS:
	case SCHA_ERR_PROP:
	case SCHA_ERR_TAG:
	case SCHA_ERR_HANDLE:
	case SCHA_ERR_ORB:
	case SCHA_ERR_INTERNAL:
	case SCHA_ERR_DEPEND:
	case SCHA_ERR_STATE:
	case SCHA_ERR_METHOD:
	case SCHA_ERR_CHECKS:
	case SCHA_ERR_RSTATUS:
	default:
		break;
	}

	return (error);
}
/* lint -restore */

/*
 * scsymon_delete_newline
 *
 * Delete all newline characters from the string buffer. It returns the
 * same buffer with the newline charactors removed from it
 *
 */
const char *
scsymon_delete_newline(const char *pbuf)
{
	char *pchar;

	pchar =	strrchr(pbuf, '\n');
	while (pchar != NULL) {
		(void) strcpy(pchar, pchar + 1);
		pchar =	strrchr(pbuf, '\n');
	}

	return (pbuf);
}

/*
 * scsymon_double_buffer_size
 *
 * double the size of the buffer passed in.
 *
 * Upon success, a buf objects of scsymon_rt_method_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_rt_method_configs().
 *
 * INPUT:	buf_size - input buffer size
 *		pbuf - input buffer address
 * OUTPUT:	psize_new - pointer to output buffer size
 *		ppbuf_new - pointer to output buffer address
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *
 */
scsymon_errno_t
scsymon_double_buffer_size(const unsigned int buf_size, char *pbuf,
    unsigned int *psize_new, char **ppbuf_new)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	unsigned int new_size;

	if (buf_size == 0 || pbuf == NULL || psize_new == NULL ||
	    ppbuf_new == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* double the buffer size */
	new_size = buf_size << 1;
	*ppbuf_new = (char *)realloc(pbuf, new_size);
	if (*ppbuf_new == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}
	*psize_new = new_size;

cleanup:
	return (scsymon_error);
}

/*
 * symon_get_scconf_namelist
 *
 * concatenate names from a name list and separate them with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
scsymon_errno_t
symon_get_scconf_namelist(scconf_namelist_t *pname_list, char **pbuf)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	unsigned int len = 0;
	unsigned int old_len;
	char *buf = NULL;
	unsigned int buf_size;
	char *delimiter = NULL;

	if (pbuf == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	len = 0;
	buf_size = SCSYMON_MAX_STRING_LEN;
	buf = (char *)calloc(buf_size, sizeof (char));
	if (buf == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}
	(void) memset(buf, NULL, buf_size * sizeof (char));

	/* concatenate names from a name list and separate them with "," */
	while (pname_list != NULL) {
		delimiter = (len == 0) ? SCSYMON_LIST_HEAD :
		    SCSYMON_LIST_DELIMITER;

		(void) snprintf(buf + len, buf_size - len, "%s%s",
		    delimiter, pname_list->scconf_namelist_name);

		old_len = len;
		len = strlen(buf);

		/* double the buffer if necessary */
		if (len >= buf_size - 1) {
			scsymon_error =
			    scsymon_double_buffer_size(buf_size, buf, &buf_size,
			    &buf);

			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;

			len = old_len;
			continue;
		}

		pname_list = pname_list->scconf_namelist_next;
	}

	*pbuf = buf;
	buf = NULL;

cleanup:
	if (buf != NULL)
		free(buf);

	return (scsymon_error);
}

/*
 * symon_get_scconf_adapterlist
 *
 * concatenate names from a scconf adapter list and separate them with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
scsymon_errno_t
symon_get_scconf_adapterlist(scconf_cfg_cltr_adap_t *padapter_list,
    char **pbuf)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	unsigned int len = 0;
	unsigned int old_len;
	char *buf = NULL;
	unsigned int buf_size;
	char *delimiter = NULL;

	if (pbuf == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	len = 0;
	buf_size = SCSYMON_MAX_STRING_LEN;
	buf = (char *)calloc(buf_size, sizeof (char));
	if (buf == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}
	(void) memset(buf, NULL, buf_size * sizeof (char));

	/*
	 * concatenate names from a scconf adapter list and separate them
	 * with ","
	 */
	while (padapter_list != NULL) {
		delimiter = (len == 0) ? SCSYMON_LIST_HEAD :
		    SCSYMON_LIST_DELIMITER;

		(void) snprintf(buf + len, buf_size - len, "%s%s",
		    delimiter, padapter_list->scconf_adap_adaptername);

		old_len = len;
		len = strlen(buf);

		/* double the buffer if necessary */
		if (len >= buf_size - 1) {
			scsymon_error =
			    scsymon_double_buffer_size(buf_size, buf, &buf_size,
			    &buf);

			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;

			len = old_len;
			continue;
		}

		padapter_list = padapter_list->scconf_adap_next;
	}

	*pbuf = buf;
	buf = NULL;

cleanup:
	if (buf != NULL)
		free(buf);

	return (scsymon_error);
}

/*
 * symon_get_scconf_qdevportlist
 *
 * concatenate names from a scconf quorum device port list and separate them
 * with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
scsymon_errno_t
symon_get_scconf_qdevportlist(scconf_cfg_qdevport_t *pport_list,
    char **pbuf)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	unsigned int len = 0;
	unsigned int old_len;
	char *buf = NULL;
	unsigned int buf_size;
	char *delimiter = NULL;

	if (pbuf == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	len = 0;
	buf_size = SCSYMON_MAX_STRING_LEN;
	buf = (char *)calloc(buf_size, sizeof (char));
	if (buf == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}
	(void) memset(buf, NULL, buf_size * sizeof (char));

	/*
	 * concatenate names from a scconf quorum device port list and
	 * separate them with ","
	 */
	while (pport_list != NULL) {
		delimiter = (len == 0) ? SCSYMON_LIST_HEAD :
		    SCSYMON_LIST_DELIMITER;
		(void) snprintf(buf + len, buf_size - len, "%s%s",
		    delimiter, pport_list->scconf_qdevport_nodename);

		old_len = len;
		len = strlen(buf);

		/* double the buffer if necessary */
		if (len >= buf_size - 1) {
			scsymon_error =
			    scsymon_double_buffer_size(buf_size, buf, &buf_size,
			    &buf);

			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;

			len = old_len;
			continue;
		}

		pport_list = pport_list->scconf_qdevport_next;
	}

	*pbuf = buf;
	buf = NULL;

cleanup:
	if (buf != NULL)
		free(buf);

	return (scsymon_error);

}

/*
 * symon_get_scconf_portlist
 *
 * concatenate names from a scconf port list and separate them
 * with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
scsymon_errno_t
symon_get_scconf_portlist(scconf_cfg_port_t *pport_list,
    char **pbuf)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	unsigned int len = 0;
	unsigned int old_len;
	char *buf = NULL;
	unsigned int buf_size;
	char *delimiter = NULL;

	if (pbuf == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	len = 0;
	buf_size = SCSYMON_MAX_STRING_LEN;
	buf = (char *)calloc(buf_size, sizeof (char));
	if (buf == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}
	(void) memset(buf, NULL, buf_size * sizeof (char));

	/*
	 * concatenate names from a scconf port list and separate them
	 * with ","
	 */
	while (pport_list != NULL) {
		delimiter = (len == 0) ? SCSYMON_LIST_HEAD :
		    SCSYMON_LIST_DELIMITER;

		(void) snprintf(buf + len, buf_size - len, "%s%s",
		    delimiter, pport_list->scconf_port_portname);

		old_len = len;
		len = strlen(buf);

		/* double the buffer if necessary */
		if (len >= buf_size - 1) {
			scsymon_error =
			    scsymon_double_buffer_size(buf_size, buf, &buf_size,
			    &buf);

			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;

			len = old_len;
			continue;
		}
		pport_list = pport_list->scconf_port_next;
	}

	*pbuf = buf;
	buf = NULL;

cleanup:
	if (buf != NULL)
		free(buf);

	return (scsymon_error);

}

/*
 * symon_get_cached_scconf_nodelist
 *
 * concatenate names from a scconf node id list and separate them
 * with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
scsymon_errno_t
symon_get_cached_scconf_nodelist(scconf_cfg_cluster_t *pcluster_config,
    scconf_nodeid_t *pnodeid_array, char **pbuf)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scconf_errno_t scconf_error = SCCONF_NOERR;
	unsigned int len = 0;
	unsigned int old_len;
	char *buf = NULL;
	unsigned int buf_size;
	char *delimiter = NULL;
	char *pname;
	scconf_nodeid_t *pnodeid;

	if (pbuf == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	len = 0;
	buf_size = SCSYMON_MAX_STRING_LEN;
	buf = (char *)calloc(buf_size, sizeof (char));
	if (buf == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}
	(void) memset(buf, NULL, buf_size * sizeof (char));

	pnodeid = pnodeid_array;
	pname = NULL;

	/*
	 * concatenate names from a scconf node id list and separate them
	 * with ","
	 */
	while ((pnodeid != NULL) && (*pnodeid != NULL)) {
		scconf_error = scconf_get_cached_nodename(pcluster_config,
		    *pnodeid, &pname);
		delimiter = (len == 0) ? SCSYMON_LIST_HEAD :
		    SCSYMON_LIST_DELIMITER;

		if (scconf_error != SCCONF_NOERR) {
			/*
			 * Could not get the node name!  Print
			 * out the nodeid instead.
			 */
			(void) snprintf(buf + len, buf_size - len,
			    "%s%d", delimiter, *pnodeid);
		} else {
			(void) snprintf(buf + len, buf_size - len,
			    "%s%s", delimiter, pname);
		}
		free(pname);

		pname = NULL;

		old_len = len;
		len = strlen(buf);

		/* double the buffer if necessary */
		if (len >= buf_size - 1) {
			scsymon_error =
			    scsymon_double_buffer_size(buf_size, buf, &buf_size,
			    &buf);

			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;

			len = old_len;
			continue;
		}

		pnodeid++;
	}

	*pbuf = buf;
	buf = NULL;

cleanup:
	if (buf != NULL)
		free(buf);

	return (scsymon_error);
}

/*
 * symon_get_scstat_primarylist
 *
 * concatenate primary names from a scstat_ds_node_state_t list and separate
 * them with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
scsymon_errno_t
symon_get_scstat_primarylist(scstat_ds_node_state_t *preplica_list,
    char **pbuf)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	unsigned int len = 0;
	unsigned int old_len;
	char *buf = NULL;
	unsigned int buf_size;
	char *delimiter = NULL;

	if (pbuf == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	len = 0;
	buf_size = SCSYMON_MAX_STRING_LEN;
	buf = (char *)calloc(buf_size, sizeof (char));
	if (buf == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}
	(void) memset(buf, NULL, buf_size * sizeof (char));

	/*
	 * concatenate primary names from a scstat_ds_node_state_t list
	 * and separate them with ","
	 */
	while (preplica_list != NULL) {
		if (preplica_list->scstat_node_state == SCSTAT_PRIMARY) {
			delimiter = (len == 0) ? SCSYMON_LIST_HEAD :
			    SCSYMON_LIST_DELIMITER;

			(void) snprintf(buf + len, buf_size - len, "%s%s",
			    delimiter, preplica_list->scstat_node_name);

			old_len = len;
			len = strlen(buf);

			/* double the buffer if necessary */
			if (len >= buf_size - 1) {
				scsymon_error =
				    scsymon_double_buffer_size(buf_size, buf,
				    &buf_size, &buf);

				if (scsymon_error != SCSYMON_ENOERR)
					goto cleanup;

				len = old_len;
				continue;
			}
		}

		preplica_list = preplica_list->scstat_node_next;
	}

	*pbuf = buf;
	buf = NULL;

cleanup:
	if (buf != NULL)
		free(buf);

	return (scsymon_error);
}

/*
 * symon_get_rgm_namelist
 *
 * concatenate names from a name list and separate them with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
scsymon_errno_t
symon_get_rgm_namelist(namelist_t *pname_list, char **pbuf)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	unsigned int len = 0;
	unsigned int old_len;
	char *buf = NULL;
	unsigned int buf_size;
	char *delimiter = NULL;

	if (pbuf == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	len = 0;
	buf_size = SCSYMON_MAX_STRING_LEN;
	buf = (char *)calloc(buf_size, sizeof (char));
	if (buf == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}
	(void) memset(buf, NULL, buf_size * sizeof (char));

	/* concatenate names from a name list and separate them with "," */
	while (pname_list != NULL) {
		delimiter = (len == 0) ? SCSYMON_LIST_HEAD :
		    SCSYMON_LIST_DELIMITER;

		(void) snprintf(buf + len, buf_size - len, "%s%s",
		    delimiter, pname_list->nl_name);

		old_len = len;
		len = strlen(buf);

		/* double the buffer if necessary */
		if (len >= buf_size - 1) {
			scsymon_error =
			    scsymon_double_buffer_size(buf_size, buf, &buf_size,
			    &buf);

			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;

			len = old_len;
			continue;
		}

		pname_list = pname_list->nl_next;
	}

	*pbuf = buf;
	buf = NULL;

cleanup:
	if (buf != NULL)
		free(buf);

	return (scsymon_error);
}

/*
 * symon_get_rgm_rdeplist
 *
 * concatenate names from a name list and separate them with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
scsymon_errno_t
symon_get_rgm_rdeplist(rdeplist_t *pname_list, char **pbuf)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	unsigned int len = 0;
	unsigned int old_len;
	char *buf = NULL;
	unsigned int buf_size;
	char *delimiter = NULL;

	if (pbuf == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	len = 0;
	buf_size = SCSYMON_MAX_STRING_LEN;
	buf = (char *)calloc(buf_size, sizeof (char));
	if (buf == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}
	(void) memset(buf, NULL, buf_size * sizeof (char));

	/* concatenate names from a name list and separate them with "," */
	while (pname_list != NULL) {
		delimiter = (len == 0) ? SCSYMON_LIST_HEAD :
		    SCSYMON_LIST_DELIMITER;

		(void) snprintf(buf + len, buf_size - len, "%s%s",
		    delimiter, pname_list->rl_name);

		old_len = len;
		len = strlen(buf);

		/* double the buffer if necessary */
		if (len >= buf_size - 1) {
			scsymon_error =
			    scsymon_double_buffer_size(buf_size, buf, &buf_size,
			    &buf);

			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;

			len = old_len;
			continue;
		}

		pname_list = pname_list->rl_next;
	}

	*pbuf = buf;
	buf = NULL;

cleanup:
	if (buf != NULL)
		free(buf);

	return (scsymon_error);
}

/*
 * symon_get_rgm_nodeidlist
 *
 * concatenate names from a nodeid list and separate them with ","
 * (convert the nodeid inn the nodeidlist_t into nodenames)
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
scsymon_errno_t
symon_get_rgm_nodeidlist(nodeidlist_t *pnodeid_list, char **pbuf)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	unsigned int len = 0;
	unsigned int old_len;
	char *buf = NULL;
	unsigned int buf_size;
	char *delimiter = NULL;
	char *nodename = NULL;
	scconf_errno_t sc_errno;

	if (pbuf == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	len = 0;
	buf_size = SCSYMON_MAX_STRING_LEN;
	buf = (char *)calloc(buf_size, sizeof (char));
	if (buf == NULL) {
		scsymon_error = SCSYMON_ENOMEM;
		goto cleanup;
	}
	(void) memset(buf, NULL, buf_size * sizeof (char));

	/* concatenate names from a name list and separate them with "," */
	while (pnodeid_list != NULL) {
		delimiter = (len == 0) ? SCSYMON_LIST_HEAD :
		    SCSYMON_LIST_DELIMITER;

		sc_errno = scconf_get_nodename(pnodeid_list->nl_nodeid,
		    &nodename);
		if (SCCONF_NOERR != sc_errno) {
			scsymon_error =
			    scsymon_convert_scconf_error_code(sc_errno);
			goto cleanup;
		}

		(void) snprintf(buf + len, buf_size - len, "%s%s",
		    delimiter, nodename);

		old_len = len;
		len = strlen(buf);

		/* double the buffer if necessary */
		if (len >= buf_size - 1) {
			scsymon_error =
			    scsymon_double_buffer_size(buf_size,
				buf, &buf_size, &buf);

			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;

			len = old_len;
			free(nodename);
			nodename = NULL;
			continue;
		}

		pnodeid_list = pnodeid_list->nl_next;
		free(nodename);
		nodename = NULL;
	}

	*pbuf = buf;
	buf = NULL;

cleanup:
	if (buf != NULL)
		free(buf);

	if (nodename != NULL) {
		free(nodename);
		nodename = NULL;
	}
	return (scsymon_error);
}

/*
 * scsymon_nodeidzone_to_nodename
 *
 * converts the given nodeid and zone name to
 * logical nodename.
 *
 * This function is duplicate of
 * scrgadm_nodeidzone_to_nodename
 *
 * A pointer to the logical nodename is returned.
 * The caller is responsible for freeing the nodename.
 */

char *
scsymon_nodeidzone_to_nodename(uint_t nodeid, char *zonename) {
	char *nodename = NULL;
	char *tmp = NULL;
	scconf_errno_t scconferr;

	scconferr = scconf_get_nodename(nodeid, &nodename);
	if (scconferr != SCCONF_NOERR) {
		free(nodename);
		return (NULL);
	}
	if (zonename == NULL) {
		return (nodename);
	}
	tmp = realloc(nodename, strlen(nodename) + strlen(zonename) + 2);
	if (tmp == NULL) {
		/* nodename is initialized to NULL so suppress lint warning */
		free(nodename); /* lint !e644 */
		return (NULL);
	}
	nodename = tmp;
	(void) strcat(nodename,  ":");
	(void) strcat(nodename, zonename);
	return (nodename);
}

/*
 * scsymon_free_name_array
 *
 * Free a char array.
 *
 */
void
scsymon_free_name_array(char **pplnames)
{
	char *pname;
	char **ppname;

	if (pplnames == NULL)
		return;

	ppname = pplnames;
	pname = *ppname;
	while (pname != NULL) {
		free(pname);
		ppname++;
		pname = *ppname;
	}

	free(pplnames);
}

/*
 * scsymon_get_rg_type
 *
 * Get RG type of the specified RG object..
 * Input: prgm_rg
 * Output: ptype
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 *
 */
scsymon_errno_t
scsymon_get_rg_type(rgm_rg_t *prgm_rg, scsymon_rg_type_t *ptype)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;

	if (prgm_rg == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	*ptype = SCSYMON_SCALABLE_RG;

	/* check if failover RG */
	if (prgm_rg->rg_mode == RGMODE_FAILOVER)
		*ptype = SCSYMON_FAILOVER_RG;
	else
		*ptype = SCSYMON_SCALABLE_RG;

cleanup:
	return (scsymon_error);
}

/*
 * scsymon_get_rgm_rts
 *
 * Call rgm_scrgadm interface to get properties for all Resource Types.
 *
 * Upon success, a list of objects of scsymon_rgm_rt_t is returned.
 * The caller is responsible for freeing the array using
 * scsymon_free_rgm_rts().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rgm_rts(scsymon_rgm_rt_t **pplrts)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char **pplrt_names = NULL;
	char **pplrt_names_current = NULL;
	char *pname = NULL;
	rgm_rt_t *prgm_rt = NULL;
	scsymon_rgm_rt_t *prt = NULL;
	scsymon_rgm_rt_t *prt_current = NULL;

	if (pplrts == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* get all rts */
	scsymon_error = scsymon_get_rt_names(&pplrt_names);
	if (scsymon_error != SCSYMON_ENOERR)
		goto cleanup;

	if (pplrt_names == NULL)
		goto cleanup;
	else
		pplrt_names_current = pplrt_names;

	while (pplrt_names_current != NULL) {
		pname = *pplrt_names_current;

		if (pname == NULL)
			break;

		/* get configuration for each rt */
		rgm_status = rgm_scrgadm_getrtconf(pname, &prgm_rt, NULL);
		scsymon_error = symon_convert_scha_error_code(rgm_status);
		if (scsymon_error == SCSYMON_EOBSOLETE) { /* skip */
			if (prgm_rt != NULL) {
				rgm_free_rt(prgm_rt);
				prgm_rt = NULL;
			}

			scsymon_error = SCSYMON_ENOERR;
			pplrt_names_current++;
			continue;
		}
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		prt = (scsymon_rgm_rt_t *)calloc(1,
		    sizeof (scsymon_rgm_rt_t));
		if (prt == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* fill in RT properties */
		prt->prgm_rt = prgm_rt;
		prgm_rt = NULL;

		/* add to the list */
		if (prt_current == NULL) { /* list empty */
			*pplrts = prt;
			prt_current = prt;
		} else {
			prt_current->pnext = prt;
			prt_current = prt_current->pnext;
		}
		prt = NULL;

		pplrt_names_current++;
	}

cleanup:
	if (pplrt_names != NULL)
		scsymon_free_name_array(pplrt_names);

	if (prgm_rt != NULL)
		rgm_free_rt(prgm_rt);

	if (prt != NULL)
		scsymon_free_rgm_rts(prt);

	return (scsymon_error);
}

/*
 * scsymon_free_rgm_rts
 *
 * free Resource Type properties
 */
void
scsymon_free_rgm_rts(scsymon_rgm_rt_t *plrts)
{
	scsymon_rgm_rt_t *prt = plrts;
	rgm_rt_t *prgm_rt;
	scsymon_rgm_rt_t *pnext;

	while (prt != NULL) {
		prgm_rt = prt->prgm_rt;
		if (prgm_rt != NULL)
			rgm_free_rt(prgm_rt);

		pnext = prt->pnext;
		free(prt);
		prt = pnext;
	}
}

/*
 * scsymon_get_rgm_rg_rss
 *
 * Call rgm_scrgadm interface to get properties for all Resource Groups
 * as well as the Resources for every Resource Group.
 *
 * Upon success, a list of objects of scsymon_rgm_rg_rs_t is returned.
 * The caller is responsible for freeing the array using
 * scsymon_free_rgm_rg_rss().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rgm_rg_rss(scsymon_rgm_rg_rs_t **pplrgs)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char **pplrg_names = NULL;
	char **pplrg_names_current = NULL;
	char *pname = NULL;
	rgm_rg_t *prgm_rg = NULL;
	scsymon_rgm_rs_t *plrss = NULL;
	scsymon_rgm_rg_rs_t *prg = NULL;
	scsymon_rgm_rg_rs_t *prg_current = NULL;

	if (pplrgs == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* get all rgs */
	scsymon_error = scsymon_get_rg_names(&pplrg_names);
	if (scsymon_error != SCSYMON_ENOERR)
		goto cleanup;

	if (pplrg_names == NULL)
		goto cleanup;
	else
		pplrg_names_current = pplrg_names;

	while (pplrg_names_current != NULL) {
		pname = *pplrg_names_current;

		if (pname == NULL)
			break;

		/* get configuration for each rg */
		rgm_status = rgm_scrgadm_getrgconf(pname, &prgm_rg, NULL);
		scsymon_error = symon_convert_scha_error_code(rgm_status);
		if (scsymon_error == SCSYMON_EOBSOLETE) { /* skip */
			if (prgm_rg != NULL) {
				rgm_free_rg(prgm_rg);
				prgm_rg = NULL;
			}

			scsymon_error = SCSYMON_ENOERR;
			pplrg_names_current++;
			continue;
		}
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		prg = (scsymon_rgm_rg_rs_t *)calloc(1,
		    sizeof (scsymon_rgm_rg_rs_t));
		if (prg == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* fill in RG properties */
		prg->prgm_rg = prgm_rg;
		prgm_rg = NULL;

		scsymon_error = scsymon_get_rgm_rss(pname, &plrss);
		if (scsymon_error == SCSYMON_EOBSOLETE) { /* skip */
			scsymon_free_rgm_rss(plrss);
			plrss = NULL;

			scsymon_error = SCSYMON_ENOERR;
		}

		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		prg->plrss = plrss;
		plrss = NULL;

		/* add to the list */
		if (prg_current == NULL) { /* list empty */
			*pplrgs = prg;
			prg_current = prg;
		} else {
			prg_current->pnext = prg;
			prg_current = prg_current->pnext;
		}
		prg = NULL;

		pplrg_names_current++;
	}

cleanup:
	if (pplrg_names != NULL)
		scsymon_free_name_array(pplrg_names);

	if (prgm_rg != NULL)
		rgm_free_rg(prgm_rg);

	if (plrss != NULL)
		scsymon_free_rgm_rss(plrss);

	if (prg != NULL)
		scsymon_free_rgm_rg_rss(prg);

	return (scsymon_error);
}

/*
 * scsymon_free_rgm_rg_rss
 *
 * free properties for Resource Group as well the Resources in each
 * Resource Group.
 */
void
scsymon_free_rgm_rg_rss(scsymon_rgm_rg_rs_t *plrgs)
{
	scsymon_rgm_rg_rs_t *prg = plrgs;
	scsymon_rgm_rs_t *prs;
	rgm_rg_t *prgm_rg;
	scsymon_rgm_rg_rs_t *pnext;

	while (prg != NULL) {
		prgm_rg = prg->prgm_rg;
		if (prgm_rg != NULL)
			rgm_free_rg(prgm_rg);

		prs = prg->plrss;
		scsymon_free_rgm_rss(prs);

		pnext = prg->pnext;
		free(prg);
		prg = pnext;
	}
}

/*
 * scsymon_get_rgm_rss
 *
 * Call rgm_scrgadm interface to get properties for all Resources
 * for a particular Resource Group.
 *
 * Upon success, a list of objects of scsymon_rgm_rs_t is returned.
 * The caller is responsible for freeing the array using
 * scsymon_free_rgm_rss().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rgm_rss(char *rg_name, scsymon_rgm_rs_t **pplrss)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	char **pplrs_names = NULL;
	char **pplrs_names_current = NULL;
	char *pname = NULL;
	rgm_resource_t *prgm_rs = NULL;
	scsymon_rgm_rs_t *prs = NULL;
	scsymon_rgm_rs_t *prs_current = NULL;

	if (rg_name == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	if (pplrss == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* get all resources */
	scsymon_error = scsymon_get_rs_names(rg_name, &pplrs_names);
	if (scsymon_error != SCSYMON_ENOERR)
		goto cleanup;

	if (pplrs_names == NULL)
		goto cleanup;
	else
		pplrs_names_current = pplrs_names;

	while (pplrs_names_current != NULL) {
		pname = *pplrs_names_current;

		if (pname == NULL)
			break;

		/* get configuration for each resource */
		rgm_status = rgm_scrgadm_getrsrcconf(pname, &prgm_rs, NULL);
		scsymon_error = symon_convert_scha_error_code(rgm_status);
		if (scsymon_error == SCSYMON_EOBSOLETE) { /* skip */
			if (prgm_rs != NULL) {
				rgm_free_resource(prgm_rs);
				prgm_rs = NULL;
			}

			scsymon_error = SCSYMON_ENOERR;
			pplrs_names_current++;
			continue;
		}
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		prs = (scsymon_rgm_rs_t *)calloc(1,
		    sizeof (scsymon_rgm_rs_t));
		if (prs == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* fill in RG properties */
		prs->prgm_rs = prgm_rs;
		prgm_rs = NULL;

		/* add to the list */
		if (prs_current == NULL) { /* list empty */
			*pplrss = prs;
			prs_current = prs;
		} else {
			prs_current->pnext = prs;
			prs_current = prs_current->pnext;
		}
		prs = NULL;

		pplrs_names_current++;
	}

cleanup:
	if (pplrs_names != NULL)
		scsymon_free_name_array(pplrs_names);

	if (prgm_rs != NULL)
		rgm_free_resource(prgm_rs);

	if (prs != NULL)
		scsymon_free_rgm_rss(prs);

	return (scsymon_error);
}

/*
 * scsymon_free_rgm_rss
 *
 * free properties for Resources.
 */
void
scsymon_free_rgm_rss(scsymon_rgm_rs_t *plrss)
{
	scsymon_rgm_rs_t *prs = plrss;
	rgm_resource_t *prgm_rs;
	scsymon_rgm_rs_t *pnext;

	while (prs != NULL) {
		prgm_rs = prs->prgm_rs;
		if (prgm_rs != NULL)
			rgm_free_resource(prgm_rs);

		pnext = prs->pnext;
		free(prs);
		prs = pnext;
	}
}

/*
 * scsymon_get_node_status
 *
 * Call scha interface to get node status.  The supplied "pstatus" should
 * be of at least SCSTAT_MAX_STRING_LEN.
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_node_status(char *node_name, char *pstatus)
{
	scsymon_errno_t error = SCSYMON_ENOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scha_cluster_t handle = NULL;
	int free_handle = 0;
	scha_node_state_t node_state;
	scstat_state_code_t state_code;

	scha_status.err_code = scha_cluster_open(&handle);
	error = symon_convert_scha_error_code(scha_status);
	if (error != SCSYMON_ENOERR)
		goto cleanup;

	free_handle = 1;
	/* get status of the node */
	scha_status.err_code =
	    scha_cluster_get(handle, SCHA_NODESTATE_NODE, node_name,
	    &node_state);
	error = symon_convert_scha_error_code(scha_status);
	if (error != SCSYMON_ENOERR)
		goto cleanup;

	if (node_state == SCHA_NODE_UP)
		state_code = SCSTAT_ONLINE;
	else
		state_code = SCSTAT_OFFLINE;
	scstat_get_default_status_string(state_code, pstatus);

cleanup:
	if (free_handle == 1)
		(void) scha_cluster_close(handle);

	return (error);
}

/*
 * scsymon_get_rg_type_by_name
 *
 * Call scha interface to get RG type.
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rg_type_by_name(char *rg_name, scsymon_rg_type_t *ptype)
{
	scsymon_errno_t error = SCSYMON_ENOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scha_resourcegroup_t handle = NULL;
	int free_handle = 0;
	scha_rgmode_t rg_mode = RGMODE_NONE;

	scha_status.err_code = scha_resourcegroup_open(rg_name, &handle);
	error = symon_convert_scha_error_code(scha_status);
	if (error != SCSYMON_ENOERR)
		goto cleanup;

	free_handle = 1;
	/* get rg type */
	scha_status.err_code =
	    scha_resourcegroup_get(handle, SCHA_RG_MODE, &rg_mode);
	error = symon_convert_scha_error_code(scha_status);
	if (error != SCSYMON_ENOERR)
		goto cleanup;

	if (rg_mode == RGMODE_FAILOVER)
		*ptype = SCSYMON_FAILOVER_RG;
	else
		*ptype = SCSYMON_SCALABLE_RG;

cleanup:
	if (free_handle == 1)
		(void) scha_resourcegroup_close(handle);

	return (error);
}

/*
 * scsymon_get_rt_names
 *
 * Get names of all RTs registered for the cluster. The result name array
 * must be freed using scsymon_free_name_array().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *
 */
scsymon_errno_t
scsymon_get_rt_names(char ***ppplnames)
{
	scsymon_errno_t error = SCSYMON_ENOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scha_cluster_t handle = NULL;
	int free_handle = 0;
	char **pplnames = NULL;
	char *pname;
	scha_str_array_t *plrt_names = NULL;
	unsigned int i;

	scha_status.err_code = scha_cluster_open(&handle);
	error = symon_convert_scha_error_code(scha_status);
	if (error != SCSYMON_ENOERR)
		goto cleanup;

	free_handle = 1;
	/* get rg names */
	scha_status.err_code =
	    scha_cluster_get(handle, SCHA_ALL_RESOURCETYPES, &plrt_names);
	error = symon_convert_scha_error_code(scha_status);
	if (error != SCSYMON_ENOERR)
		goto cleanup;

	if (plrt_names == NULL)
		goto cleanup;

	/* allocate a buffer to hold elements of the rt name array */
	pplnames = (char **)calloc(plrt_names->array_cnt + 1, sizeof (char *));
	if (pplnames == NULL) {
		error = SCSYMON_ENOMEM;
		goto cleanup;
	}

	*ppplnames = pplnames;

	/* fill the new array with elements from the rt name array */
	for (i = 0; i < plrt_names->array_cnt; i++) {
		pname = plrt_names->str_array[i];
		if (pname == NULL)
			break;

		pplnames[i] = strdup(pname);
		if (pplnames[i] == NULL) {
			error = SCSYMON_ENOMEM;
			goto cleanup;
		}
	}

cleanup:
	if (free_handle == 1) {
		/* free memory pointed to by plrt_names */
		(void) scha_cluster_close(handle);
	}

	return (error);
}

/*
 * scsymon_get_rg_names
 *
 * Get names of all RGs configured for the cluster. The result name array
 * must be freed using scsymon_free_name_array().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *
 */
scsymon_errno_t
scsymon_get_rg_names(char ***ppplnames)
{
	scsymon_errno_t error = SCSYMON_ENOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scha_cluster_t handle = NULL;
	int free_handle = 0;
	char **pplnames = NULL;
	char *pname;
	scha_str_array_t *plrg_names;
	unsigned int i;

	scha_status.err_code = scha_cluster_open(&handle);
	error = symon_convert_scha_error_code(scha_status);
	if (error != SCSYMON_ENOERR)
		goto cleanup;

	free_handle = 1;
	/* get rg names */
	scha_status.err_code =
	    scha_cluster_get(handle, SCHA_ALL_RESOURCEGROUPS, &plrg_names);
	error = symon_convert_scha_error_code(scha_status);
	if (error != SCSYMON_ENOERR)
		goto cleanup;

	if (plrg_names == NULL)
		goto cleanup;

	/* allocate a buffer to hold elements of the rg name array */
	pplnames = (char **)calloc(plrg_names->array_cnt + 1, sizeof (char *));
	if (pplnames == NULL) {
		error = SCSYMON_ENOMEM;
		goto cleanup;
	}

	*ppplnames = pplnames;

	/* fill the new array with elements from the rg name array */
	for (i = 0; i < plrg_names->array_cnt; i++) {
		pname = plrg_names->str_array[i];
		if (pname == NULL)
			break;

		pplnames[i] = strdup(pname);
		if (pplnames[i] == NULL) {
			error = SCSYMON_ENOMEM;
			goto cleanup;
		}
	}

cleanup:
	if (free_handle == 1) {
		/* free memory pointed to by plrg_names */
		(void) scha_cluster_close(handle);
	}

	return (error);
}

/*
 * scsymon_get_rs_names
 *
 * Get names of all RSs configured for the RG. The result name array
 * must be freed using scsymon_free_name_array().
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *
 */
scsymon_errno_t
scsymon_get_rs_names(char *rg_name, char ***ppplnames)
{
	scsymon_errno_t error = SCSYMON_ENOERR;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	scha_cluster_t handle = NULL;
	int free_handle = 0;
	char **pplnames = NULL;
	char *pname;
	scha_str_array_t *plrs_names;
	unsigned int i;

	scha_status.err_code = scha_resourcegroup_open(rg_name, &handle);
	error = symon_convert_scha_error_code(scha_status);
	if (error != SCSYMON_ENOERR)
		goto cleanup;

	free_handle = 1;

	/* get rs names */
	scha_status.err_code =
	    scha_resourcegroup_get(handle, SCHA_RESOURCE_LIST, &plrs_names);
	error = symon_convert_scha_error_code(scha_status);
	if (error != SCSYMON_ENOERR)
		goto cleanup;

	if (plrs_names == NULL)
		goto cleanup;

	/* allocate a buffer to hold elements of the rs name array */
	pplnames = (char **)calloc(plrs_names->array_cnt + 1, sizeof (char *));
	if (pplnames == NULL) {
		error = SCSYMON_ENOMEM;
		goto cleanup;
	}

	*ppplnames = pplnames;

	/* fill the new array with elements from the rs name array */
	for (i = 0; i < plrs_names->array_cnt; i++) {
		pname = plrs_names->str_array[i];
		if (pname == NULL)
			break;

		pplnames[i] = strdup(pname);
		if (pplnames[i] == NULL) {
			error = SCSYMON_ENOMEM;
			goto cleanup;
		}
	}

cleanup:
	if (free_handle == 1)
		(void) scha_resourcegroup_close(handle);

	return (error);
}

/*
 * Device groups utility functions
 */

/*
 * scsymon_is_autogen_dg_by_name
 *
 *	Return 1 if the disk group is autogenerated.
 *      A group is autogenerated if
 *	SCCONF_DS_PROP_AUTOGENERATED property is set.
 *
 *	Otherwise, return zero.
 *
 *      Input : device group name
 *              link list of device groups config info
 */
int
scsymon_is_autogen_dg_by_name(char *dgname, scconf_cfg_ds_t *dgconfig)
{
	scconf_cfg_ds_t *dgp;

	/* Check arguemnts */
	if (dgname == NULL || dgconfig == NULL)
		return (0);

	/* Find the device group in the list */
	for (dgp = dgconfig;  dgp;  dgp = dgp->scconf_ds_next) {
		if (dgp->scconf_ds_name &&
		    strcmp(dgname, dgp->scconf_ds_name) == 0)
			break;
	}

	/* If we cannot find the group, just return zero */
	if (dgp == NULL)
	    return (0);

	return (scsymon_is_autogen_dg((const scconf_cfg_ds_t *)dgp));
}

/*
 * scsymon_is_autogen_dg
 *
 *	Return 1 if the disk group is autogenerated.
 *      A group is autogenerated if
 *	SCCONF_DS_PROP_AUTOGENERATED property is set.
 *
 *	Otherwise, return zero.
 *
 *      Input : device gourp config info
 */
int
scsymon_is_autogen_dg(const scconf_cfg_ds_t *dgp)
{
	scconf_cfg_prop_t *propp;

	/* Check arguemnts */
	if (dgp == NULL)
		return (0);

	/* Check to see if the "autogenerated" property is set */
	for (propp = dgp->scconf_ds_propertylist;  propp;
	    propp = propp->scconf_prop_next) {
		if (propp->scconf_prop_key &&
		    strcmp(SCCONF_DS_PROP_AUTOGENERATED,
		    propp->scconf_prop_key) == 0)
			return (1);
	}
	return (0);
}
