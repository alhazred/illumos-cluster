/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)scsymon_qdev.c	1.8	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <scadmin/scsymon_srv.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <scsymon_private.h>

/*
 * scsymon_get_qdev_configs
 *
 * get the following properties of all quorum devices configured for a cluster:
 *	quorum device name
 *	device path
 *	enabled
 *	votes configured
 *	port list
 *
 * Upon success, a list of objects of scsymon_qdev_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_qdev_configs().
 *
 * INPUT: pcluster_config
 * OUTPUT:pplqdevs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_qdev_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_qdev_config_t **pplqdevs)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scconf_cfg_qdev_t *pqdev_config = NULL;
	scsymon_qdev_config_t *pqdev_current = NULL;
	scsymon_qdev_config_t *pqdev = NULL;

	if (pcluster_config == NULL)
		return (scsymon_error);

	if (pplqdevs == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* quorum device info */
	pqdev_config = pcluster_config->scconf_cluster_qdevlist;

	while (pqdev_config != NULL) {
		char *qdev_name = pqdev_config->scconf_qdev_name;

		/* fill in property and state for each qdev */
		pqdev = (scsymon_qdev_config_t *)calloc(1,
		    sizeof (scsymon_qdev_config_t));
		if (pqdev == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* qdev name */
		pqdev->pname = strdup((qdev_name == NULL) ? SCSYMON_NULL :
		    qdev_name);
		if (pqdev->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* qurorum device configuration */
		pqdev->ppath =
		    strdup((pqdev_config->scconf_qdev_device == NULL) ?
		    SCSYMON_NULL : pqdev_config->scconf_qdev_device);
		if (pqdev->ppath == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		pqdev->enabled =
		    (pqdev_config->scconf_qdev_state == SCCONF_STATE_ENABLED) ?
		    1 : 0;

		pqdev->default_vote = pqdev_config->scconf_qdev_votes;

		/* quorum device ports */
		scsymon_error = symon_get_scconf_qdevportlist(
		    pqdev_config->scconf_qdev_ports, &pqdev->pportlist);
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		/* add to the list */
		if (pqdev_current == NULL) { /* list empty */
			*pplqdevs = pqdev;
			pqdev_current = pqdev;
		} else {
			pqdev_current->pnext = pqdev;
			pqdev_current = pqdev_current->pnext;
		}

		pqdev = NULL; /* so we don't accidentally delete it */

		pqdev_config = pqdev_config->scconf_qdev_next;
	}

cleanup:
	if (pqdev != NULL)
		scsymon_free_qdev_configs(pqdev);

	return (scsymon_error);
}

/*
 * scsymon_free_qdev_configs
 *
 * free quorum device properties
 */
void
scsymon_free_qdev_configs(scsymon_qdev_config_t *plqdevs)
{
	scsymon_qdev_config_t *pqdev = plqdevs;

	while (pqdev != NULL) {
		scsymon_qdev_config_t *pnext;

		if (pqdev->pname != NULL)
			free(pqdev->pname);
		if (pqdev->ppath != NULL)
			free(pqdev->ppath);
		if (pqdev->pportlist != NULL)
			free(pqdev->pportlist);

		pnext = pqdev->pnext;
		free(pqdev);
		pqdev = pnext;
	}
}

/*
 * scsymon_get_qdevport_configs
 *
 * get the following properties of hosts of all quorum devices configured
 * for a cluster:
 *	quorum device name
 *	quorum device host name
 *	enabled
 *
 * Upon success, a list of objects of scsymon_qdevport_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_qdevport_config_s().
 *
 * INPUT: pcluster_config
 * OUTPUT:pplqdevports
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_qdevport_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_qdevport_config_t **pplqdevports)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scconf_cfg_qdev_t *pqdev_config = NULL;
	scconf_cfg_qdevport_t *pqdev_port = NULL;
	scsymon_qdevport_config_t *pqdevport_current = NULL;
	scsymon_qdevport_config_t *pqdevport = NULL;

	if (pcluster_config == NULL)
		return (scsymon_error);

	if (pplqdevports == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* quorum device info */
	pqdev_config = pcluster_config->scconf_cluster_qdevlist;

	while (pqdev_config != NULL) {
		char *qdev_name = pqdev_config->scconf_qdev_name;

		pqdev_port = pqdev_config->scconf_qdev_ports;
		while (pqdev_port != NULL) {
			/* fill in property and state for each qdev */
			pqdevport = (scsymon_qdevport_config_t *)calloc(1,
			    sizeof (scsymon_qdevport_config_t));
			if (pqdevport == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* qdev name */
			pqdevport->pname =
			    strdup((qdev_name == NULL) ? SCSYMON_NULL :
			    qdev_name);
			if (pqdevport->pname == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			pqdevport->pnode =
			    strdup((pqdev_port->scconf_qdevport_nodename ==
			    NULL) ? SCSYMON_NULL :
			    pqdev_port->scconf_qdevport_nodename);
			if (pqdevport->pnode == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			pqdevport->enabled =
			    (pqdev_port->scconf_qdevport_state ==
			    SCCONF_STATE_ENABLED) ? 1 : 0;

			/* add to the list */
			if (pqdevport_current == NULL) { /* list empty */
				*pplqdevports = pqdevport;
				pqdevport_current = pqdevport;
			} else {
				pqdevport_current->pnext = pqdevport;
				pqdevport_current = pqdevport_current->pnext;
			}

			pqdevport = NULL;

			pqdev_port = pqdev_port->scconf_qdevport_next;
		}

		pqdev_config = pqdev_config->scconf_qdev_next;
	}

cleanup:
	if (pqdevport != NULL)
		scsymon_free_qdevport_configs(pqdevport);

	return (scsymon_error);
}

/*
 * scsymon_free_qdevport_configs
 *
 * free quorum device host properties
 */
void
scsymon_free_qdevport_configs(scsymon_qdevport_config_t *plqdevports)
{
	scsymon_qdevport_config_t *pqdevport = plqdevports;

	while (pqdevport != NULL) {
		scsymon_qdevport_config_t *pnext;

		if (pqdevport->pname != NULL)
			free(pqdevport->pname);
		if (pqdevport->pnode != NULL)
			free(pqdevport->pnode);

		pnext = pqdevport->pnext;
		free(pqdevport);
		pqdevport = pnext;
	}
}

/*
 * scsymon_get_qdev_statuss
 *
 * get the status of all quorum devices configured for a cluster:
 *	quorum device name
 *	status
 *	current votes
 *
 * Upon success, a list of objects of scsymon_qdev_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_qdev_statuss().
 *
 * INPUT: pcluster_status
 * OUTPUT:pplqdevs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_qdev_statuss(scstat_cluster_t *pcluster_status,
    scsymon_qdev_status_t **pplqdevs)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scstat_quorumdev_t *pqdev_status = NULL;
	scsymon_qdev_status_t *pqdev_current = NULL;
	scsymon_qdev_status_t *pqdev = NULL;

	if (pcluster_status == NULL)
		return (scsymon_error);

	if (pplqdevs == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* quorum device status */
	pqdev_status = pcluster_status->scstat_quorum->scstat_quorumdev_list;

	while (pqdev_status != NULL) {
		/* fill in property and state for each qdev */
		pqdev = (scsymon_qdev_status_t *)calloc(1,
		    sizeof (scsymon_qdev_status_t));
		if (pqdev == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* qdev name */
		pqdev->pname = strdup((pqdev_status->scstat_quorumdev_name
		    == NULL) ? SCSYMON_NULL :
		    pqdev_status->scstat_quorumdev_name);
		if (pqdev->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		pqdev->pstate = strdup((pqdev_status->scstat_quorumdev_statstr
		    == NULL) ? SCSYMON_NULL :
		    pqdev_status->scstat_quorumdev_statstr);
		if (pqdev->pstate == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		pqdev->current_vote = pqdev_status->scstat_vote_contributed;

		/* add to the list */
		if (pqdev_current == NULL) { /* list empty */
			*pplqdevs = pqdev;
			pqdev_current = pqdev;
		} else {
			pqdev_current->pnext = pqdev;
			pqdev_current = pqdev_current->pnext;
		}

		pqdev = NULL; /* so we don't accidentally delete it */

		pqdev_status = pqdev_status->scstat_quorumdev_next;
	}

cleanup:
	if (pqdev != NULL)
		scsymon_free_qdev_statuss(pqdev);

	return (scsymon_error);
}

/*
 * scsymon_free_qdev_statuss
 *
 * free quorum device status
 */
void
scsymon_free_qdev_statuss(scsymon_qdev_status_t *plqdevs)
{
	scsymon_qdev_status_t *pqdev = plqdevs;

	while (pqdev != NULL) {
		scsymon_qdev_status_t *pnext;

		if (pqdev->pname != NULL)
			free(pqdev->pname);
		if (pqdev->pstate != NULL)
			free(pqdev->pstate);

		pnext = pqdev->pnext;
		free(pqdev);
		pqdev = pnext;
	}
}
