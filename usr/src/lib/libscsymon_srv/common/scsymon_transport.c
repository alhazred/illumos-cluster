/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)scsymon_transport.c	1.9	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <scadmin/scsymon_srv.h>
#include <scadmin/scstat.h>
#include <scsymon_private.h>

/*
 * scsymon_get_path_statuss
 *
 * get the status of all paths configured for a cluster
 *	source adapter
 *	destination adapter
 *	status
 *
 * Upon success, a list of objects of scsymon_path_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_path_statuss().
 *
 * INPUT: ptransport_status
 * OUTPUT: pplpaths
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_path_statuss(scconf_cfg_cluster_t *pcluster_config,
    scstat_transport_t *ptransport_status,
    scsymon_path_status_t **pplpaths)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scstat_errno_t scstat_error = SCSTAT_ENOERR;
	scstat_path_t *ppath_status = NULL;
	scsymon_path_status_t *ppath_current = NULL;
	scsymon_path_status_t *ppath = NULL;
	char *padapter1 = NULL, *padapter2 = NULL;
	char endpoint[SCSTAT_MAX_STRING_LEN];

	if (ptransport_status == NULL)
		return (scsymon_error);

	if (pplpaths == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* path info */
	ppath_status = ptransport_status->scstat_path_list;

	while (ppath_status != NULL) {
		/* fill in property and state for each path */
		ppath = (scsymon_path_status_t *)calloc(1,
		    sizeof (scsymon_path_status_t));
		if (ppath == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* get endpoints */
		scstat_error = scstat_get_path_endpoints(
		    ppath_status->scstat_path_name,
		    &padapter1, &padapter2);
		if (scstat_error != SCSTAT_ENOERR) {
			scsymon_error =
			    scsymon_convert_scstat_error_code(scstat_error);
			goto cleanup;
		}

		if (padapter1 == NULL || padapter2 == NULL) {
			scsymon_error = SCSYMON_EINVAL;
			goto cleanup;
		}

		scstat_error = scstat_get_cached_transport_adapter_name(
		    pcluster_config, padapter1, endpoint);
		if (scstat_error != SCSTAT_ENOERR) {
			scsymon_error =
			    scsymon_convert_scstat_error_code(scstat_error);
			goto cleanup;
		}
		ppath->padapter1 = strdup(endpoint);
		if (ppath->padapter1 == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
		free(padapter1);
		padapter1 = NULL;

		scstat_error = scstat_get_cached_transport_adapter_name(
		    pcluster_config, padapter2, endpoint);
		if (scstat_error != SCSTAT_ENOERR) {
			scsymon_error =
			    scsymon_convert_scstat_error_code(scstat_error);
			goto cleanup;
		}
		ppath->padapter2 = strdup(endpoint);
		if (ppath->padapter2 == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
		free(padapter2);
		padapter2 = NULL;

		ppath->pstate = strdup((ppath_status->scstat_path_statstr
		    == NULL) ? SCSYMON_NULL :
		    ppath_status->scstat_path_statstr);
		if (ppath->pstate == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* add to the list */
		if (ppath_current == NULL) { /* list empty */
			*pplpaths = ppath;
			ppath_current = ppath;
		} else {
			ppath_current->pnext = ppath;
			ppath_current = ppath_current->pnext;
		}

		ppath = NULL; /* so we don't accidentally delete it */

		ppath_status = ppath_status->scstat_path_next;
	}

cleanup:
	if (ppath != NULL)
		scsymon_free_path_statuss(ppath);

	if (padapter1 != NULL)
		free(padapter1);

	if (padapter2 != NULL)
		free(padapter2);

	return (scsymon_error);
}

/*
 * scsymon_free_path_statuss
 *
 * free cluster path status
 */
void
scsymon_free_path_statuss(scsymon_path_status_t *plpaths)
{
	scsymon_path_status_t *ppath = plpaths;

	while (ppath != NULL) {
		scsymon_path_status_t *pnext;

		if (ppath->padapter1 != NULL)
			free(ppath->padapter1);
		if (ppath->padapter2 != NULL)
			free(ppath->padapter2);
		if (ppath->pstate != NULL)
			free(ppath->pstate);

		pnext = ppath->pnext;
		free(ppath);
		ppath = pnext;
	}
}

/*
 * scsymon_get_transport_adapter_configs
 *
 * get the following properties of all transport adapters configured for a
 * cluster
 *	node name
 *	adapter name
 *	type
 *	enabled or not
 *
 * Upon success, a list of objects of scsymon_transport_adapter_config_t is
 * returned. The caller is responsible for freeing the space using
 * scsymon_free_adapter_configs().
 *
 * INPUT: pcluster_config
 * OUTPUT:ppladapters
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_transport_adapter_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_transport_adapter_config_t **ppladapters)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scconf_cfg_node_t *pnode_config = NULL;
	scconf_cfg_cltr_adap_t *padapter_config = NULL;
	scsymon_transport_adapter_config_t *padapter_current = NULL;
	scsymon_transport_adapter_config_t *padapter = NULL;

	if (pcluster_config == NULL)
		return (scsymon_error);

	if (ppladapters == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* node list */
	pnode_config = pcluster_config->scconf_cluster_nodelist;

	while (pnode_config != NULL) {
		/* get adapter list */
		padapter_config = pnode_config->scconf_node_adapterlist;

		while (padapter_config != NULL) {
			padapter =
			    (scsymon_transport_adapter_config_t *)calloc(1,
			    sizeof (scsymon_transport_adapter_config_t));
			if (padapter == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* get adapter properties */
			padapter->pnode = strdup((pnode_config->\
			    scconf_node_nodename == NULL) ?
			    SCSYMON_NULL :
			    pnode_config->scconf_node_nodename);
			if (padapter->pnode == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			padapter->padapter = strdup((padapter_config->\
			    scconf_adap_adaptername == NULL) ? SCSYMON_NULL :
			    padapter_config->scconf_adap_adaptername);
			if (padapter->padapter == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			padapter->ptype = strdup((padapter_config->\
			    scconf_adap_cltrtype == NULL) ? SCSYMON_NULL :
			    padapter_config->scconf_adap_cltrtype);
			if (padapter->ptype == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			padapter->enabled = (padapter_config->\
			    scconf_adap_adapterstate == SCCONF_STATE_ENABLED) ?
			    1 : 0;

			/* add to the list */
			if (padapter_current == NULL) { /* list empty */
				*ppladapters = padapter;
				padapter_current = padapter;
			} else {
				padapter_current->pnext = padapter;
				padapter_current = padapter_current->pnext;
			}

			padapter = NULL;

			padapter_config = padapter_config->scconf_adap_next;
		}
		pnode_config = pnode_config->scconf_node_next;
	}

cleanup:
	if (padapter != NULL)
		scsymon_free_transport_adapter_configs(padapter);

	return (scsymon_error);
}

/*
 * scsymon_free_transport_adapter_configs
 *
 * free transport adapter properties
 */
void
scsymon_free_transport_adapter_configs(
    scsymon_transport_adapter_config_t *pladapters)
{
	scsymon_transport_adapter_config_t *padapter = pladapters;

	while (padapter != NULL) {
		scsymon_transport_adapter_config_t *pnext;

		if (padapter->pnode != NULL)
			free(padapter->pnode);
		if (padapter->padapter != NULL)
			free(padapter->padapter);
		if (padapter->ptype != NULL)
			free(padapter->ptype);

		pnext = padapter->pnext;
		free(padapter);
		padapter = pnext;
	}
}

/*
 * scsymon_get_junction_configs
 *
 * get the following properties of all junctions configured for a cluster
 *	junction name
 *	junction type
 *	enabled or not
 *	configured port list
 *
 * Upon success, a list of objects of scsymon_junction_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_junction_configs().
 *
 * INPUT: pcluster_config
 * OUTPUT:ppljunctions
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_junction_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_junction_config_t **ppljunctions)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scconf_cfg_cpoint_t *pjunction_config = NULL;
	scsymon_junction_config_t *pjunction_current = NULL;
	scsymon_junction_config_t *pjunction = NULL;

	if (pcluster_config == NULL)
		return (scsymon_error);

	if (ppljunctions == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* junction list */
	pjunction_config = pcluster_config->scconf_cluster_cpointlist;

	while (pjunction_config != NULL) {
		pjunction = (scsymon_junction_config_t *)calloc(1,
		    sizeof (scsymon_junction_config_t));
		if (pjunction == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* get junction properties */
		pjunction->pname = strdup((pjunction_config->\
		    scconf_cpoint_cpointname == NULL) ?
		    SCSYMON_NULL :
		    pjunction_config->scconf_cpoint_cpointname);
		if (pjunction->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		pjunction->ptype = strdup((pjunction_config->\
		    scconf_cpoint_type == NULL) ? SCSYMON_NULL :
		    pjunction_config->scconf_cpoint_type);
		if (pjunction->ptype == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		pjunction->enabled = (pjunction_config->\
		    scconf_cpoint_cpointstate == SCCONF_STATE_ENABLED) ?
		    1 : 0;

		/* junction configured ports */
		scsymon_error = symon_get_scconf_portlist(
		    pjunction_config->scconf_cpoint_portlist,
		    &pjunction->pportlist);
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		/* add to the list */
		if (pjunction_current == NULL) { /* list empty */
			*ppljunctions = pjunction;
			pjunction_current = pjunction;
		} else {
			pjunction_current->pnext = pjunction;
			pjunction_current = pjunction_current->pnext;
		}

		pjunction = NULL; /* so we don't accidentally delete it */

		pjunction_config = pjunction_config->scconf_cpoint_next;
	}

cleanup:
	if (pjunction != NULL)
		scsymon_free_junction_configs(pjunction);

	return (scsymon_error);
}

/*
 * scsymon_free_junction_configs
 *
 * free junction properties
 */
void
scsymon_free_junction_configs(scsymon_junction_config_t *pljunctions)
{
	scsymon_junction_config_t *pjunction = pljunctions;

	while (pjunction != NULL) {
		scsymon_junction_config_t *pnext;

		if (pjunction->pname != NULL)
			free(pjunction->pname);
		if (pjunction->ptype != NULL)
			free(pjunction->ptype);
		if (pjunction->pportlist != NULL)
			free(pjunction->pportlist);

		pnext = pjunction->pnext;
		free(pjunction);
		pjunction = pnext;
	}
}

/*
 * scsymon_get_cable_configs
 *
 * get the following properties of all cables configured for a cluster
 *	endpoint1 type
 *	endpoint1
 *	endpoint1 port
 *	endpoint2 type
 *	endpoint2
 *	endpoint2 port
 *	enabled or not
 *
 * Upon success, a list of objects of scsymon_cable_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_cable_configs().
 *
 * INPUT: pcluster_config
 * OUTPUT:pplcables
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_cable_configs(scconf_cfg_cluster_t *pcluster_config,
    scsymon_cable_config_t **pplcables)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scconf_cfg_cable_t *pcable_config = NULL;
	scsymon_cable_config_t *pcable_current = NULL;
	scsymon_cable_config_t *pcable = NULL;
	char endpoint_name[SCSYMON_MAX_STRING_LEN];

	if (pcluster_config == NULL)
		return (scsymon_error);

	if (pplcables == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* cable list */
	pcable_config = pcluster_config->scconf_cluster_cablelist;

	while (pcable_config != NULL) {
		scconf_cltr_epoint_t endpoint1 =
		    pcable_config->scconf_cable_epoint1;
		scconf_cltr_epoint_t endpoint2 =
		    pcable_config->scconf_cable_epoint2;

		pcable = (scsymon_cable_config_t *)calloc(1,
		    sizeof (scsymon_cable_config_t));
		if (pcable == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* get cable properties */
		pcable->pend_type1 =
		    strdup((endpoint1.scconf_cltr_epoint_type
		    == SCCONF_CLTR_EPOINT_TYPE_ADAPTER) ?
		    "ADAPTER" : "JUNCTION");
		if (pcable->pend_type1 == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		if (endpoint1.scconf_cltr_epoint_type ==
		    SCCONF_CLTR_EPOINT_TYPE_ADAPTER) {
			(void) snprintf(endpoint_name,
			    SCSYMON_MAX_STRING_LEN,
			    "%s%s%s@%s",
			    (endpoint1.scconf_cltr_epoint_nodename == NULL) ?
			    SCSYMON_NULL :
			    endpoint1.scconf_cltr_epoint_nodename,
			    ":",
			    (endpoint1.scconf_cltr_epoint_devicename == NULL) ?
			    SCSYMON_NULL :
			    endpoint1.scconf_cltr_epoint_devicename,
			    (endpoint1.scconf_cltr_epoint_portname == NULL) ?
			    SCSYMON_NULL :
			    endpoint1.scconf_cltr_epoint_portname);
		} else {
			(void) snprintf(endpoint_name,
			    SCSYMON_MAX_STRING_LEN,
			    "%s@%s",
			    (endpoint1.scconf_cltr_epoint_devicename == NULL) ?
			    SCSYMON_NULL :
			    endpoint1.scconf_cltr_epoint_devicename,
			    (endpoint1.scconf_cltr_epoint_portname == NULL) ?
			    SCSYMON_NULL :
			    endpoint1.scconf_cltr_epoint_portname);
		}

		pcable->pend1 = strdup((endpoint_name == NULL) ? SCSYMON_NULL :
		    endpoint_name);
		if (pcable->pend1 == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		pcable->pend_type2 =
		    strdup((endpoint2.scconf_cltr_epoint_type
		    == SCCONF_CLTR_EPOINT_TYPE_ADAPTER) ?
		    "ADAPTER" : "JUNCTION");
		if (pcable->pend_type2 == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		if (endpoint2.scconf_cltr_epoint_type ==
		    SCCONF_CLTR_EPOINT_TYPE_ADAPTER) {
			(void) snprintf(endpoint_name,
			    SCSYMON_MAX_STRING_LEN,
			    "%s%s%s@%s",
			    (endpoint2.scconf_cltr_epoint_nodename == NULL) ?
			    SCSYMON_NULL :
			    endpoint2.scconf_cltr_epoint_nodename,
			    ":",
			    (endpoint2.scconf_cltr_epoint_devicename == NULL) ?
			    SCSYMON_NULL :
			    endpoint2.scconf_cltr_epoint_devicename,
			    (endpoint2.scconf_cltr_epoint_portname == NULL) ?
			    SCSYMON_NULL :
			    endpoint2.scconf_cltr_epoint_portname);
		} else {
			(void) snprintf(endpoint_name,
			    SCSYMON_MAX_STRING_LEN,
			    "%s@%s",
			    (endpoint2.scconf_cltr_epoint_devicename == NULL) ?
			    SCSYMON_NULL :
			    endpoint2.scconf_cltr_epoint_devicename,
			    (endpoint2.scconf_cltr_epoint_portname == NULL) ?
			    SCSYMON_NULL :
			    endpoint2.scconf_cltr_epoint_portname);
		}

		pcable->pend2 = strdup((endpoint_name == NULL) ? SCSYMON_NULL :
		    endpoint_name);
		if (pcable->pend2 == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		pcable->enabled = (pcable_config->\
		    scconf_cable_cablestate == SCCONF_STATE_ENABLED) ?
		    1 : 0;

		/* add to the list */
		if (pcable_current == NULL) { /* list empty */
			*pplcables = pcable;
			pcable_current = pcable;
		} else {
			pcable_current->pnext = pcable;
			pcable_current = pcable_current->pnext;
		}

		pcable = NULL; /* so we don't accidentally delete it */

		pcable_config = pcable_config->scconf_cable_next;
	}

cleanup:
	if (pcable != NULL)
		scsymon_free_cable_configs(pcable);

	return (scsymon_error);
}

/*
 * scsymon_free_cable_configs
 *
 * free cable properties
 */
void
scsymon_free_cable_configs(scsymon_cable_config_t *plcables)
{
	scsymon_cable_config_t *pcable = plcables;

	while (pcable != NULL) {
		scsymon_cable_config_t *pnext;

		if (pcable->pend_type1 != NULL)
			free(pcable->pend_type1);
		if (pcable->pend1 != NULL)
			free(pcable->pend1);
		if (pcable->pend_type2 != NULL)
			free(pcable->pend_type2);
		if (pcable->pend2 != NULL)
			free(pcable->pend2);

		pnext = pcable->pnext;
		free(pcable);
		pcable = pnext;
	}
}
