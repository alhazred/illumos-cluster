/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scsymon_rg.c	1.21	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <scadmin/scsymon_srv.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <scsymon_private.h>

/*
 * scsymon_get_rg_configs
 *
 * get the following properties of a Resource Group:
 *	RG name
 *	primary list
 *	description
 *	maximum primaries
 *	desired primaries
 *	failback or not
 *	implicit network dependencies
 *	RG dependencies
 *	global resources used
 *	path prefix
 *
 * Upon success, a list of objects of scsymon_rg_config_t of the specified type
 * is returned. The caller is responsible for freeing the space using
 * scsymon_free_rg_configs().
 *
 * Possible RG type:
 *	SCSYMON_FAILOVER_RG
 *	SCSYMON_SCALABLE_RG
 *
 * INPUT: plrgm_rgs, rg_type_filter
 * OUTPUT:pplrgs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rg_configs(scsymon_rgm_rg_rs_t *plrgm_rgs,
    scsymon_rg_type_t rg_type_filter,
    scsymon_rg_config_t **pplrgs)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scsymon_rgm_rg_rs_t *ptr_rgm_rg;
	rgm_rg_t *prgm_rg = NULL;
	char **pplrs_names = NULL;
	char **pprs_name;
	char *prs_name;
	scsymon_rg_type_t rg_type;
	scsymon_rg_config_t *prg = NULL;
	scsymon_rg_config_t *prg_current = NULL;
	char *buf = NULL;
	unsigned int len = 0;
	unsigned int old_len;
	unsigned int buf_size;
	char *delimiter = NULL;

	if (plrgm_rgs == NULL)
		return (scsymon_error);

	if (pplrgs == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* get properties of every RG */
	ptr_rgm_rg = plrgm_rgs;
	while (ptr_rgm_rg != NULL) {
		prgm_rg = ptr_rgm_rg->prgm_rg;
		scsymon_error = scsymon_get_rg_type(prgm_rg, &rg_type);
		if (scsymon_error != SCSYMON_ENOERR)
			return (scsymon_error);

		/* if RG type not match, skip the one */
		if ((rg_type & rg_type_filter) == 0) {
			ptr_rgm_rg = ptr_rgm_rg->pnext;
			continue;
		}

		/* if RG is updating, skip */
		pplrs_names = NULL;
		scsymon_error = scsymon_get_rs_names(prgm_rg->rg_name,
		    &pplrs_names);
		if (scsymon_error == SCSYMON_EOBSOLETE) { /* skip */
			if (pplrs_names != NULL) {
				scsymon_free_name_array(pplrs_names);
				pplrs_names = NULL;
			}
			scsymon_error = SCSYMON_ENOERR;
			ptr_rgm_rg = ptr_rgm_rg->pnext;
			continue;
		}
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		prg = (scsymon_rg_config_t *)calloc(1,
		    sizeof (scsymon_rg_config_t));
		if (prg == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* fill in rg properties */
		prg->pname = strdup((prgm_rg->rg_name == NULL) ?
		    SCSYMON_NULL : prgm_rg->rg_name);
		if (prg->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		scsymon_error = symon_get_rgm_nodeidlist(prgm_rg->rg_nodelist,
		    &prg->pprimaries);
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		prg->pdesc = strdup((prgm_rg->rg_description == NULL) ?
		    SCSYMON_NULL : prgm_rg->rg_description);
		if (prg->pdesc == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prg->maxPrim = prgm_rg->rg_max_primaries;

		prg->desPrim = prgm_rg->rg_desired_primaries;

		prg->failback = prgm_rg->rg_failback;

		prg->net_depend = prgm_rg->rg_impl_net_depend;

		scsymon_error = symon_get_rgm_namelist(prgm_rg->rg_dependencies,
		    &prg->pRG_depend);
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		if (prgm_rg->rg_glb_rsrcused.is_ALL_value == B_TRUE) {
			prg->pglobal_res_used = strdup(SCSYMON_ALL);
			if (prg->pglobal_res_used == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}
		} else {
			scsymon_error =
			    symon_get_rgm_namelist(
				(prgm_rg->rg_glb_rsrcused).names,
				&prg->pglobal_res_used);
			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;
		}

		prg->ppath_prefix = strdup(
		    (prgm_rg->rg_pathprefix == NULL) ?
		    SCSYMON_NULL : prgm_rg->rg_pathprefix);
		if (prg->ppath_prefix == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		len = 0;
		buf_size = SCSYMON_MAX_STRING_LEN;
		buf = (char *)calloc(buf_size, sizeof (char));
		if (buf == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
		(void) memset(buf, NULL, buf_size * sizeof (char));

		/*
		 * concatenate names of resource in the RG and
		 * place the result into buf
		 */
		pprs_name = pplrs_names;
		while (pprs_name != NULL) {
			prs_name = *pprs_name;
			if (prs_name != NULL) {
				delimiter = (len == 0) ? SCSYMON_LIST_HEAD :
				    SCSYMON_LIST_DELIMITER;

				(void) snprintf(buf + len, buf_size - len,
				    "%s%s", delimiter, prs_name);

				old_len = len;
				len = strlen(buf);

				/* double the buffer if necessary */
				if (len >= buf_size - 1) {
					scsymon_error =
					    scsymon_double_buffer_size(\
					    buf_size,
					    buf,
					    &buf_size,
					    &buf);

					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;

					len = old_len;
					continue;
				}
			} else
				break;

			pprs_name++;
		}
		scsymon_free_name_array(pplrs_names);
		pplrs_names = NULL;

		prg->prslist = buf;
		buf = NULL;

		/* add to the list */
		if (prg_current == NULL) { /* list empty */
			*pplrgs = prg;
			prg_current = prg;
		} else {
			prg_current->pnext = prg;
			prg_current = prg_current->pnext;
		}
		prg = NULL;

		ptr_rgm_rg = ptr_rgm_rg->pnext;
	} /* end while */

cleanup:
	if (buf != NULL)
		free(buf);

	if (prg != NULL)
		scsymon_free_rg_configs(prg);

	if (pplrs_names != NULL)
		scsymon_free_name_array(pplrs_names);

	return (scsymon_error);
}

/*
 * scsymon_free_rg_configs
 *
 * free Resource Group properties
 */
void
scsymon_free_rg_configs(scsymon_rg_config_t *plrgs)
{
	scsymon_rg_config_t *prg = plrgs;

	while (prg != NULL) {
		scsymon_rg_config_t *pnext;

		if (prg->pname != NULL)
			free(prg->pname);
		if (prg->pprimaries != NULL)
			free(prg->pprimaries);
		if (prg->pdesc != NULL)
			free(prg->pdesc);
		if (prg->pRG_depend != NULL)
			free(prg->pRG_depend);
		if (prg->pglobal_res_used != NULL)
			free(prg->pglobal_res_used);
		if (prg->ppath_prefix != NULL)
			free(prg->ppath_prefix);
		if (prg->prslist != NULL)
			free(prg->prslist);

		pnext = prg->pnext;
		free(prg);
		prg = pnext;
	}
}

/*
 * scsymon_get_rg_statuss
 *
 * get Resource Group states:
 *	RG name
 *	primary node name
 *	primary state
 *	RG state on the node
 *
 * Upon success, a list of objects of scsymon_rg_status_t of the specified type
 * is returned. The caller is responsible for freeing the space using
 * scsymon_free_rg_statuss().
 *
 * Possible RG type:
 *	SCSYMON_FAILOVER_RG
 *	SCSYMON_SCALABLE_RG
 *
 * INPUT: pcluster_status, rg_type_filter
 * OUTPUT:pplrgs
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_ECLUSTERRECONFIG	- cluster is reconfiguring
 *	SCSYMON_ERGRECONFIG	- RG is reconfiguring
 *	SCSYMON_EOBSOLETE	- Resource/RG has been updated
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rg_status(scstat_cluster_t *pcluster_status,
    scsymon_rg_type_t rg_type_filter,
    scsymon_rg_status_t **pplrgs)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scstat_rg_t *plrg_statuss;
	scstat_rg_t *prg_status;
	scstat_rg_status_t *plrg_status_by_nodes;
	scstat_rg_status_t *prg_status_by_node;
	char *rg_name;
	scsymon_rg_type_t rg_type;
	scsymon_rg_status_t *prg = NULL;
	scsymon_rg_status_t *prg_current = NULL;
	char text[SCSYMON_MAX_STRING_LEN];

	if (pcluster_status == NULL)
		return (scsymon_error);

	if (pplrgs == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	plrg_statuss = pcluster_status->scstat_rg_list;

	/* get status of every RG */
	prg_status = plrg_statuss;
	while (prg_status != NULL) {
		rg_name = prg_status->scstat_rg_name;
		scsymon_error = scsymon_get_rg_type_by_name(rg_name, &rg_type);
		if (scsymon_error == SCSYMON_EOBSOLETE) { /* skip */
			scsymon_error = SCSYMON_ENOERR;
			prg_status = prg_status->scstat_rg_next;
			continue;
		}
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		/* if RG type not match, skip the one */
		if ((rg_type & rg_type_filter) == 0) {
			prg_status = prg_status->scstat_rg_next;
			continue;
		}

		plrg_status_by_nodes = prg_status->scstat_rg_status_list;

		prg_status_by_node = plrg_status_by_nodes;
		while (prg_status_by_node != NULL) {
			prg = (scsymon_rg_status_t *)calloc(1,
			    sizeof (scsymon_rg_status_t));
			if (prg == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prg->pname = strdup((prg_status->scstat_rg_name == NULL)
			    ? SCSYMON_NULL : prg_status->scstat_rg_name);
			if (prg->pname == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prg->pprimary =
			    strdup((prg_status_by_node->scstat_node_name
			    == NULL) ?
			    SCSYMON_NULL :
			    prg_status_by_node->scstat_node_name);
			if (prg->pprimary == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			scsymon_error = scsymon_get_node_status(\
			    prg->pprimary, text);
			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;
			prg->pprimary_state = strdup(text);
			if (prg->pprimary_state == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prg->prg_state =
			    strdup(prg_status_by_node->scstat_rg_statstr);
			if (prg->prg_state == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* add to the list */
			if (prg_current == NULL) { /* list empty */
				*pplrgs = prg;
				prg_current = prg;
			} else {
				prg_current->pnext = prg;
				prg_current = prg_current->pnext;
			}
			prg = NULL;

			prg_status_by_node =
			    prg_status_by_node->scstat_rg_status_by_node_next;
		} /* end for */

		prg_status = prg_status->scstat_rg_next;
	} /* while (prg_status != NULL) */

cleanup:
	if (prg != NULL)
		scsymon_free_rg_statuss(prg);

	return (scsymon_error);
}

/*
 * scsymon_free_rg_statuss
 *
 * free Resource Group state structures
 */
void
scsymon_free_rg_statuss(scsymon_rg_status_t *plrgs)
{
	scsymon_rg_status_t *prg = plrgs;

	while (prg != NULL) {
		scsymon_rg_status_t *pnext;

		if (prg->pname != NULL)
			free(prg->pname);
		if (prg->pprimary != NULL)
			free(prg->pprimary);
		if (prg->pprimary_state != NULL)
			free(prg->pprimary_state);
		if (prg->prg_state != NULL)
			free(prg->prg_state);

		pnext = prg->pnext;
		free(prg);
		prg = pnext;
	}
}

/*
 * scsymon_get_rs_configs
 *
 * get Resource properties:
 *	RG name
 *	Resource name
 *	RT name
 *	On/Off switch
 *	Monitored switch
 *	logical hostnames used
 *	Resource dependencies
 *
 * Upon success, a list of objects of scsymon_rs_config_t of the specified type
 * is returned. The caller is responsible for freeing the space using
 * scsymon_free_rs_configs().
 *
 * Possible RG type:
 *	SCSYMON_FAILOVER_RG
 *	SCSYMON_SCALABLE_RG
 *
 * INPUT: plrgm_rgs, rg_type_filter
 * OUTPUT: pplrss
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rs_configs(scsymon_rgm_rg_rs_t *plrgm_rgs,
    scsymon_rg_type_t rg_type_filter,
    scsymon_rs_config_t **pplrss)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scsymon_rgm_rg_rs_t *ptr_rgm_rg;
	scsymon_rgm_rs_t *plrgm_rss;
	scsymon_rgm_rs_t *ptr_rgm_rs;
	rgm_rg_t *prgm_rg = NULL;

	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	nodeidlist_t *nl = NULL;
	char *nodename = NULL;
	scha_switch_t swtch;
	uint_t on_off_switch = 0;
	uint_t monitored_switch = 0;

	scsymon_rg_type_t rg_type;
	rgm_resource_t *prgm_rs = NULL;
	scsymon_rs_config_t *prs = NULL;
	scsymon_rs_config_t *prs_current = NULL;

	if (plrgm_rgs == NULL)
		return (scsymon_error);

	if (pplrss == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* get basic properties of every resource in every RG */
	ptr_rgm_rg = plrgm_rgs;
	while (ptr_rgm_rg) {
		prgm_rg = ptr_rgm_rg->prgm_rg;
		scsymon_error = scsymon_get_rg_type(prgm_rg, &rg_type);
		if (scsymon_error != SCSYMON_ENOERR)
			return (scsymon_error);

		/* if RG type not match, skip the one */
		if ((rg_type & rg_type_filter) == 0) {
			ptr_rgm_rg = ptr_rgm_rg->pnext;
			continue;
		}

		plrgm_rss = ptr_rgm_rg->plrss;
		ptr_rgm_rs = plrgm_rss;
		while (ptr_rgm_rs) {
			prgm_rs = ptr_rgm_rs->prgm_rs;

			prs = (scsymon_rs_config_t *)calloc(1,
			    sizeof (scsymon_rs_config_t));
			if (prs == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* fill in resource property */
			prs->prg_name = strdup((prgm_rs->r_rgname ==
			    NULL) ? SCSYMON_NULL : prgm_rs->r_rgname);
			if (prs->prg_name == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prs->prs_name = strdup((prgm_rs->r_name ==
			    NULL) ? SCSYMON_NULL : prgm_rs->r_name);
			if (prs->prs_name == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prs->ptype = strdup((prgm_rs->r_type == NULL) ?
			    SCSYMON_NULL : prgm_rs->r_type);
			if (prs->ptype == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* Copy the RG nodelist. */
			nl = prgm_rg->rg_nodelist;
			while (nl) {
				/*
				 * Get the logical nodename for the
				 * nodeid and zonename
				 */
				nodename = scsymon_nodeidzone_to_nodename(
				    nl->nl_nodeid, nl->nl_zonename);
				if (nodename == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					goto cleanup;
				}

				/*
				 * Get the Resource Enabled switch
				 * for the logical nodename
				 */
				rgm_status = get_swtch(prgm_rs->r_onoff_switch,
				    nodename, &swtch);
				if (rgm_status.err_code != SCHA_ERR_NOERR) {
					scsymon_error =
					    symon_convert_scha_error_code(
					    rgm_status);
					goto cleanup;
				}

				if (swtch == SCHA_SWITCH_ENABLED) {
					on_off_switch = 1;
				} else {
					on_off_switch = 0;
				}
				if (prs->on_off_switch) {
					prs->on_off_switch = (char *)
					    realloc(
					    prs->on_off_switch,
					    strlen(prs->on_off_switch) +
					    strlen(nodename) + 5);

					if (prs->on_off_switch == NULL) {
						scsymon_error = SCSYMON_ENOMEM;
						goto cleanup;
					}
					(void) sprintf(prs->on_off_switch,
					    "%s,%d{%s}", prs->on_off_switch,
					    on_off_switch, nodename);
				} else {
					prs->on_off_switch = (char *)
					    calloc(1, strlen(nodename) + 4);
					if (prs->on_off_switch == NULL) {
						scsymon_error = SCSYMON_ENOMEM;
						goto cleanup;
					}
					(void) sprintf(prs->on_off_switch,
					    "%d{%s}", on_off_switch,
					    nodename);
				}

				/*
				 * Get the Resource Monitored switch for
				 * the logical nodename
				 */
				rgm_status = get_swtch(
				    prgm_rs->r_monitored_switch,
				    nodename, &swtch);
				if (rgm_status.err_code != SCHA_ERR_NOERR) {
					scsymon_error =
					    symon_convert_scha_error_code(
					    rgm_status);
					goto cleanup;
				}
				if (swtch == SCHA_SWITCH_ENABLED) {
					monitored_switch = 1;
				} else {
					monitored_switch = 0;
				}

				if (prs->monitored_switch) {
					prs->monitored_switch = (char *)
					    realloc(
					    prs->monitored_switch,
					    strlen(prs->monitored_switch) +
					    strlen(nodename) + 5);

					if (prs->monitored_switch == NULL) {
						scsymon_error = SCSYMON_ENOMEM;
						goto cleanup;
					}
					(void) sprintf(prs->monitored_switch,
					    "%s,%d{%s}", prs->monitored_switch,
					    monitored_switch, nodename);
				} else {
					prs->monitored_switch = (char *)
					    calloc(1, strlen(nodename) + 4);
					if (prs->monitored_switch == NULL) {
						scsymon_error = SCSYMON_ENOMEM;
						goto cleanup;
					}
					(void) sprintf(prs->monitored_switch,
					    "%d{%s}", monitored_switch,
					    nodename);
				}
				/* free the nodename */
				if (nodename) {
					free(nodename);
					nodename = NULL;
				}
				nl = nl->nl_next;
			}
			scsymon_error = symon_get_rgm_rdeplist(
			    prgm_rs->r_dependencies.dp_strong,
			    &prs->pstrong_depend);
			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;

			scsymon_error = symon_get_rgm_rdeplist(
			    prgm_rs->r_dependencies.dp_weak,
			    &prs->pweak_depend);
			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;

			/* add to the list */
			if (prs_current == NULL) { /* list empty */
				*pplrss = prs;
				prs_current = prs;
			} else {
				prs_current->pnext = prs;
				prs_current = prs_current->pnext;
			}
			prs = NULL;

			ptr_rgm_rs = ptr_rgm_rs->pnext;
		} /* end while (ptr_rgm_rs != NULL) */

		ptr_rgm_rg = ptr_rgm_rg->pnext;
	} /* end while (ptr_rgm_rg != NULL) */

cleanup:
	if (nodename)
		free(nodename);
	if (prs != NULL)
		scsymon_free_rs_configs(prs);

	return (scsymon_error);
}

/*
 * scsymon_free_rs_configs
 *
 * free Resource properties
 */
void
scsymon_free_rs_configs(scsymon_rs_config_t *plrss)
{
	scsymon_rs_config_t *prs = plrss;

	while (prs != NULL) {
		scsymon_rs_config_t *pnext;

		if (prs->prg_name != NULL)
			free(prs->prg_name);
		if (prs->prs_name != NULL)
			free(prs->prs_name);
		if (prs->ptype != NULL)
			free(prs->ptype);
		if (prs->on_off_switch != NULL)
			free(prs->on_off_switch);
		if (prs->monitored_switch != NULL)
			free(prs->monitored_switch);
		if (prs->pstrong_depend != NULL)
			free(prs->pstrong_depend);
		if (prs->pweak_depend != NULL)
			free(prs->pweak_depend);

		pnext = prs->pnext;
		free(prs);
		prs = pnext;
	}
}

/*
 * scsymon_get_rs_statuss
 *
 * get Resource state information:
 *	RG name
 *	Resource name
 *	primary node name
 *	primary state
 *	FM reported state
 *	RGM reported state
 *
 * Upon success, a list of objects of scsymon_rs_status_t of the specified type
 * is returned. The caller is responsible for freeing the space using
 * scsymon_free_rs_statuss().
 *
 * Possible RG + RS type:
 *	SCSYMON_FAILOVER_RG
 *	SCSYMON_SCALABLE_RG
 *
 * INPUT: pcluster_status, rg_type_filter
 * OUTPUT: pplrss
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_ECLUSTERRECONFIG - cluster is reconfiguring
 *	SCSYMON_ERGRECONFIG	- RG is reconfiguring
 *	SCSYMON_EOBSOLETE	- Resource/RG has been updated
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rs_statuss(scstat_cluster_t *pcluster_status,
    scsymon_rg_type_t rg_type_filter,
    scsymon_rs_status_t **pplrss)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scstat_rg_t *prg_status = NULL;
	char *rg_name;
	scsymon_rg_type_t rg_type;
	scstat_rs_t *plrs_statuss = NULL;
	scstat_rs_t *prs_status = NULL;
	scstat_rs_status_t *plrs_status_by_nodes = NULL;
	scstat_rs_status_t *prs_status_by_node = NULL;
	scsymon_rs_status_t *prs = NULL;
	scsymon_rs_status_t *prs_current = NULL;
	char text[SCSYMON_MAX_STRING_LEN];

	if (pcluster_status == NULL)
		return (scsymon_error);

	if (pplrss == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* get status of resources in every RG */
	prg_status = pcluster_status->scstat_rg_list;
	while (prg_status != NULL) {
		rg_name = prg_status->scstat_rg_name;
		scsymon_error = scsymon_get_rg_type_by_name(rg_name, &rg_type);
		if (scsymon_error == SCSYMON_EOBSOLETE) {
			scsymon_error = SCSYMON_ENOERR;
			prg_status = prg_status->scstat_rg_next;
			continue;
		}
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		/* if RG type not match, skip the one */
		if ((rg_type & rg_type_filter) == 0) {
			prg_status = prg_status->scstat_rg_next;
			continue;
		}

		plrs_statuss = prg_status->scstat_rs_list;
		prs_status = plrs_statuss;
		while (prs_status != NULL) {
			plrs_status_by_nodes =
			    prs_status->scstat_rs_status_list;
			prs_status_by_node = plrs_status_by_nodes;

			while (prs_status_by_node != NULL) {
				prs = (scsymon_rs_status_t *)calloc(1,
				    sizeof (scsymon_rs_status_t));
				if (prs == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					goto cleanup;
				}

				/* fill in resource state */
				prs->prg_name =
				    strdup(prg_status->scstat_rg_name);
				if (prs->prg_name == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					goto cleanup;
				}

				prs->prs_name = strdup(
				    (prs_status->scstat_rs_name == NULL) ?
				    SCSYMON_NULL : prs_status->scstat_rs_name);
				if (prs->prs_name == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					goto cleanup;
				}

				prs->pprimary = strdup(
				    (prs_status_by_node->scstat_node_name
				    == NULL) ?
				    SCSYMON_NULL :
				    prs_status_by_node->scstat_node_name);
				if (prs->pprimary == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					goto cleanup;
				}

				scsymon_error = scsymon_get_node_status(\
				    prs->pprimary, text);
				if (scsymon_error != SCSYMON_ENOERR)
					goto cleanup;
				prs->pprimary_state = strdup(text);
				if (prs->pprimary_state == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					goto cleanup;
				}

				prs->pfm_state =
				    strdup((prs_status_by_node->\
				    scstat_rs_statstr == NULL) ?
				    SCSYMON_NULL :
				    prs_status_by_node->scstat_rs_statstr);
				if (prs->pfm_state == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					goto cleanup;
				}

				prs->prgm_state =
				    strdup((prs_status_by_node->\
				    scstat_rs_state_str == NULL) ?
				    SCSYMON_NULL :
				    prs_status_by_node->scstat_rs_state_str);
				if (prs->prgm_state == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					goto cleanup;
				}

				/* add to the list */
				if (prs_current == NULL) {
				    /* list empty */
					*pplrss = prs;
					prs_current = prs;
				} else {
					prs_current->pnext = prs;
					prs_current = prs_current->pnext;
				}
				prs = NULL;

				prs_status_by_node = prs_status_by_node->\
				    scstat_rs_status_by_node_next;
			} /* end while (prs_status_by_node != NULL) */

			prs_status = prs_status->scstat_rs_next;
		} /* end while (prs_status != NULL) */

		prg_status = prg_status->scstat_rg_next;
	} /* end while (prg_status != NULL) */

cleanup:
	if (prs != NULL)
		scsymon_free_rs_statuss(prs);

	return (scsymon_error);
}

/*
 * scsymon_free_rs_statuss
 *
 * free Resource state structures
 */
void
scsymon_free_rs_statuss(scsymon_rs_status_t *plrss)
{
	scsymon_rs_status_t *prs = plrss;

	while (prs != NULL) {
		scsymon_rs_status_t *pnext;

		if (prs->prg_name != NULL)
			free(prs->prg_name);
		if (prs->prs_name != NULL)
			free(prs->prs_name);
		if (prs->pprimary != NULL)
			free(prs->pprimary);
		if (prs->pprimary_state != NULL)
			free(prs->pprimary_state);
		if (prs->pfm_state != NULL)
			free(prs->pfm_state);
		if (prs->prgm_state != NULL)
			free(prs->prgm_state);

		pnext = prs->pnext;
		free(prs);
		prs = pnext;
	}
}

/*
 * scsymon_get_rs_prop_configs
 *
 * get properties of all Resources:
 *	RG name
 *	Resource name
 *	property name
 *	property value
 *	property description
 *
 * Upon success, a list of objects of scsymon_rs_prop_config_t of the
 * specified type is returned. The caller is responsible for freeing the space
 * using scsymon_free_rs_prop_configs().
 *
 * Possible RG type:
 *	SCSYMON_FAILOVER_RG
 *	SCSYMON_SCALABLE_SERVICE_RG
 *
 * Possible RS property type type:
 *	SCSYMON_RS_COM_PROP
 *	SCSYMON_RS_EXT_PROP
 *	SCSYMON_RS_TIMEOUT_PROP
 *
 * INPUT: plrgm_rgs, rg_type_filter, rs_prop_type_filter
 * OUTPUT: pplrss
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rs_prop_configs(scsymon_rgm_rg_rs_t *plrgm_rgs,
    scsymon_rg_type_t rg_type_filter,
    scsymon_rs_prop_type_t rs_prop_type_filter,
    scsymon_rs_prop_config_t **pplrss)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scsymon_rgm_rg_rs_t *ptr_rgm_rg;
	scsymon_rgm_rs_t *plrgm_rss;
	scsymon_rgm_rs_t *ptr_rgm_rs;
	rgm_rg_t *prgm_rg = NULL;
	scsymon_rg_type_t rg_type;
	rgm_resource_t *prgm_rs = NULL;
	rgm_property_list_t *plproperties = NULL;
	rgm_property_list_t *pproperty = NULL;
	rgm_property_t *pprop = NULL;
	scsymon_rs_prop_config_t *prs = NULL;
	scsymon_rs_prop_config_t *prs_current = NULL;
	nodeidlist_t *nl = NULL;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	name_t tmp_value = NULL;
	name_t nodename = NULL;

	if (plrgm_rgs == NULL)
		return (scsymon_error);

	if (pplrss == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* get properties of every resource in every RG */
	ptr_rgm_rg = plrgm_rgs;
	while (ptr_rgm_rg != NULL) {
		prgm_rg = ptr_rgm_rg->prgm_rg;
		scsymon_error = scsymon_get_rg_type(prgm_rg, &rg_type);
		if (scsymon_error != SCSYMON_ENOERR)
			return (scsymon_error);

		/* if RG type not match, skip the one */
		if ((rg_type & rg_type_filter) == 0) {
			ptr_rgm_rg = ptr_rgm_rg->pnext;
			continue;
		}

		plrgm_rss = ptr_rgm_rg->plrss;
		ptr_rgm_rs = plrgm_rss;
		while (ptr_rgm_rs != NULL) {
			prgm_rs = ptr_rgm_rs->prgm_rs;

			if (rs_prop_type_filter == SCSYMON_RS_EXT_PROP)
				plproperties = prgm_rs->r_ext_properties;
			else
				plproperties = prgm_rs->r_properties;
			pproperty = plproperties;

			while (pproperty != NULL) {
				name_t rp_key = NULL;
				name_t rp_value = NULL;
				scha_prop_type_t rp_type;
				namelist_t *rp_array_values = NULL;
				name_t rp_description = NULL;
				boolean_t is_per_node_prop = B_FALSE;

				pprop = pproperty->rpl_property;

				if (pprop == NULL) {
					pproperty = pproperty->rpl_next;
					continue;
				}

				rp_key = pprop->rp_key;
				if (rp_key == NULL) {
					pproperty = pproperty->rpl_next;
					continue;
				}

				if (rs_prop_type_filter ==
				    SCSYMON_RS_COM_PROP) {
					/* looking for standard property */
					if (strcasecmp(rp_key + strlen(rp_key)
					    - strlen(SCSYMON_TIMEOUT_SUFFIX),
					    SCSYMON_TIMEOUT_SUFFIX) == 0) {
						/* timeout property, skip */
						pproperty = pproperty->rpl_next;
						continue;
					}
				} else {
					/*
					 * Set the per-node property flag if the
					 * extension property is per-node
					 * extn. prop.
					 */
					if (pprop->is_per_node == B_TRUE)
						is_per_node_prop = B_TRUE;
				}

				if (rs_prop_type_filter ==
				    SCSYMON_RS_TIMEOUT_PROP) {
					/* looking for timeout property */
					if (strcasecmp(rp_key + strlen(rp_key)
					    - strlen(SCSYMON_TIMEOUT_SUFFIX),
					    SCSYMON_TIMEOUT_SUFFIX) != 0) {
						/* non-timeout property, skip */
						pproperty = pproperty->rpl_next;
						continue;
					}
				}
				if (is_per_node_prop == B_FALSE) {
					get_value(pprop->rp_value, NULL,
					    &rp_value,
					    B_FALSE);
				} else {
					/* Copy the RG nodelist. */
					nl = prgm_rg->rg_nodelist;
					while (nl) {
						/*
						 * Get the logical nodename
						 * for the nodeid and zonename
						 */
						nodename =
						scsymon_nodeidzone_to_nodename(
						    nl->nl_nodeid,
						    nl->nl_zonename);
						if (nodename == NULL) {
							scsymon_error
							    = SCSYMON_ENOMEM;
							goto cleanup;
						}
						scha_status = get_value(
						    pprop->rp_value,
						    nodename,
						    &tmp_value,
						    B_TRUE);

						if (scha_status.err_code !=
						    SCHA_ERR_NOERR) {
						scsymon_error =
						symon_convert_scha_error_code(
						    scha_status);
						goto cleanup;
						}
						if (rp_value) {
							rp_value = (char *)
							    realloc(rp_value,
							    strlen(rp_value) +
							    strlen(tmp_value) +
							    strlen(nodename) +
							    4);
							if (rp_value == NULL) {
								scsymon_error =
								SCSYMON_ENOMEM;
								goto cleanup;
							}
							(void) sprintf(rp_value,
							    "%s,%s{%s}",
							    rp_value,
							    tmp_value,
							    nodename);
						} else {
							rp_value = (char *)
							    calloc(1,
							    strlen(tmp_value) +
							    strlen(nodename) +
							    3);
							if (rp_value == NULL) {
								scsymon_error =
								SCSYMON_ENOMEM;
								goto cleanup;
							}
							(void) sprintf(rp_value,
							    "%s{%s}",
							    tmp_value,
							    nodename);
						}
						/* Free the temporary value. */
						if (tmp_value) {
							free(tmp_value);
							tmp_value = NULL;
						}
						/* Free the nodename. */
						if (nodename) {
							free(nodename);
							nodename = NULL;
						}
						nl = nl->nl_next;
					}
				}
				rp_type = pprop->rp_type;
				rp_array_values = pprop->rp_array_values;
				rp_description = pprop->rp_description;

				prs = (scsymon_rs_prop_config_t *)
				    calloc(1,
				    sizeof (scsymon_rs_prop_config_t));
				if (prs == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					goto cleanup;
				}

				/* fill in property */
				prs->prg_name = strdup(
				    (prgm_rs->r_rgname == NULL) ?
				    SCSYMON_NULL : prgm_rs->r_rgname);
				if (prs->prg_name == NULL) {
					scsymon_error =
					    SCSYMON_ENOMEM;
					goto cleanup;
				}

				prs->prs_name = strdup(
				    (prgm_rs->r_name == NULL) ?
				    SCSYMON_NULL : prgm_rs->r_name);
				if (prs->prs_name == NULL) {
					scsymon_error =
					    SCSYMON_ENOMEM;
					goto cleanup;
				}

				prs->pprop_name = strdup((rp_key == NULL) ?
				    SCSYMON_NULL : rp_key);
				if (prs->pprop_name == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					goto cleanup;
				}

				if (rp_type == SCHA_PTYPE_STRINGARRAY ||
				    rp_type == SCHA_PTYPE_UINTARRAY) {
					scsymon_error = symon_get_rgm_namelist(
					    rp_array_values, &prs->pprop_value);
					if (scsymon_error != SCSYMON_ENOERR)
						goto cleanup;
				} else {
					prs->pprop_value =
					    strdup((rp_value == NULL) ?
					    SCSYMON_NULL : rp_value);
					if (prs->pprop_value == NULL) {
						scsymon_error = SCSYMON_ENOMEM;
						goto cleanup;
					}
				}

				prs->pprop_desc =
				    strdup((rp_description == NULL) ?
				    SCSYMON_NULL : rp_description);
				if (prs->pprop_desc == NULL) {
					scsymon_error = SCSYMON_ENOMEM;
					goto cleanup;
				}

				/* add to the list */
				if (prs_current == NULL) {
					/* list empty */
					*pplrss = prs;
					prs_current = prs;
				} else {
					prs_current->pnext = prs;
					prs_current = prs_current->pnext;
				}
				prs = NULL;

				pproperty = pproperty->rpl_next;
			} /* end while (pproperty != NULL) */

			ptr_rgm_rs = ptr_rgm_rs->pnext;
		} /* end while (ptr_rgm_rs != NULL) */

		ptr_rgm_rg = ptr_rgm_rg->pnext;
	} /* end while (ptr_rgm_rg != NULL) */

cleanup:
	if (tmp_value != NULL)
		free(tmp_value);

	if (nodename != NULL)
		free(nodename);

	if (prs != NULL)
		scsymon_free_rs_prop_configs(prs);

	return (scsymon_error);
}

/*
 * scsymon_free_rs_prop_configs
 *
 * free Resource properties
 */
void
scsymon_free_rs_prop_configs(scsymon_rs_prop_config_t *plrss)
{
	scsymon_rs_prop_config_t *prs = plrss;

	while (prs != NULL) {
		scsymon_rs_prop_config_t *pnext;

		if (prs->prg_name != NULL)
			free(prs->prg_name);
		if (prs->prs_name != NULL)
			free(prs->prs_name);
		if (prs->pprop_name != NULL)
			free(prs->pprop_name);
		if (prs->pprop_value != NULL)
			free(prs->pprop_value);
		if (prs->pprop_desc != NULL)
			free(prs->pprop_desc);

		pnext = prs->pnext;
		free(prs);
		prs = pnext;
	}
}
