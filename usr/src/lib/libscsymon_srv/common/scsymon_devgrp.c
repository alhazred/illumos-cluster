/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
#pragma ident	"@(#)scsymon_devgrp.c	1.12	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <scadmin/scsymon_srv.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <scsymon_private.h>

/*
 * scsymon_get_devgrp_configs
 *
 * get the following properties of all device groups configured for a cluster
 *	device group name
 *	service type
 *	failback enabled
 *	node list
 *	node order
 *	device list
 *
 * Upon success, a list of objects of scsymon_devgrp_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_devgrp_configs().
 *
 * INPUT: pldevgrp_configs
 * OUTPUT:ppldevgrps
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_devgrp_configs(scconf_cfg_cluster_t *pcluster_config,
    scconf_cfg_ds_t *pldevgrp_configs,
    scsymon_devgrp_config_t **ppldevgrps)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scconf_cfg_ds_t *pdevgrp_config = NULL;
	scsymon_devgrp_config_t *pdevgrp_current = NULL;
	scsymon_devgrp_config_t *pdevgrp = NULL;

	if (pldevgrp_configs == NULL)
		return (scsymon_error);

	if (ppldevgrps == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	pdevgrp_config = pldevgrp_configs;
	while (pdevgrp_config != NULL) {
		/* hide autogen dgs */
		if (scsymon_is_autogen_dg(
		    (const scconf_cfg_ds_t *)pdevgrp_config)) {
			pdevgrp_config = pdevgrp_config->scconf_ds_next;
			continue;
		}

		/* fill in property and state for each device group */
		pdevgrp = (scsymon_devgrp_config_t *)calloc(1,
		    sizeof (scsymon_devgrp_config_t));
		if (pdevgrp == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* device group name */
		pdevgrp->pname = strdup((pdevgrp_config->scconf_ds_name == NULL)
		    ? SCSYMON_NULL : pdevgrp_config->scconf_ds_name);
		if (pdevgrp->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* device group type */
		pdevgrp->pservice_type =
		    strdup((pdevgrp_config->scconf_ds_type == NULL)
		    ? SCSYMON_NULL : pdevgrp_config->scconf_ds_type);
		if (pdevgrp->pservice_type == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* device group failback enabled */
		pdevgrp->failback_enabled =
		    (pdevgrp_config->scconf_ds_failback == SCCONF_STATE_ENABLED)
		    ? 1 : 0;

		scsymon_error = symon_get_cached_scconf_nodelist(
		    pcluster_config, pdevgrp_config->scconf_ds_nodelist,
		    &pdevgrp->pnodelist);
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		/* device group node order enabled */
		pdevgrp->node_order =
		    (pdevgrp_config->scconf_ds_preference ==
		    SCCONF_STATE_ENABLED) ? 1 : 0;

		/* device group device name list */
		if (pdevgrp_config->scconf_ds_devvalues == NULL) {
			pdevgrp->pdevicelist = strdup(SCSYMON_NULL);
			if (pdevgrp->pdevicelist == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}
		} else {
			scsymon_error = symon_get_scconf_namelist(\
			    pdevgrp_config->scconf_ds_devvalues,
			    &pdevgrp->pdevicelist);
			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;
		}

		/* add to the list */
		if (pdevgrp_current == NULL) { /* list empty */
			*ppldevgrps = pdevgrp;
			pdevgrp_current = pdevgrp;
		} else {
			pdevgrp_current->pnext = pdevgrp;
			pdevgrp_current = pdevgrp_current->pnext;
		}

		pdevgrp = NULL; /* so we don't accidentally delete it */

		pdevgrp_config = pdevgrp_config->scconf_ds_next;
	}

cleanup:
	if (pdevgrp != NULL)
		scsymon_free_devgrp_configs(pdevgrp);

	return (scsymon_error);
}

/*
 * scsymon_free_devgrp_configs
 *
 * free device group properties
 */
void
scsymon_free_devgrp_configs(scsymon_devgrp_config_t *pldevgrps)
{
	scsymon_devgrp_config_t *pdevgrp = pldevgrps;

	while (pdevgrp != NULL) {
		scsymon_devgrp_config_t *pnext;

		if (pdevgrp->pname != NULL)
			free(pdevgrp->pname);
		if (pdevgrp->pservice_type != NULL)
			free(pdevgrp->pservice_type);
		if (pdevgrp->pnodelist != NULL)
			free(pdevgrp->pnodelist);
		if (pdevgrp->pdevicelist != NULL)
			free(pdevgrp->pdevicelist);

		pnext = pdevgrp->pnext;
		free(pdevgrp);
		pdevgrp = pnext;
	}
}

/*
 * scsymon_get_devgrp_statuss
 *
 * get the status of all device groups configured for a cluster
 *	device group name
 *	status
 *	primary list
 *
 * Upon success, a list of objects of scsymon_devgrp_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_devgrp_statuss().
 *
 * INPUT: pldevgrp_statuss
 * OUTPUT:ppldevgrps
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_devgrp_statuss(scstat_ds_t *pldevgrp_statuss,
    scsymon_devgrp_status_t **ppldevgrps)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scstat_ds_t *pdevgrp_status;
	scsymon_devgrp_status_t *pdevgrp_current = NULL;
	scsymon_devgrp_status_t *pdevgrp = NULL;

	/*
	 * devgrp config info is needed to filter out
	 * autogenerated groups
	 */

	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_ds_t *pldevgrp_configs = NULL;

	if (pldevgrp_statuss == NULL)
		return (scsymon_error);

	if (ppldevgrps == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	scconf_error = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG,
	    &pldevgrp_configs);
	if (scconf_error != SCCONF_NOERR) {
		scsymon_error = scsymon_convert_scconf_error_code(scconf_error);
		goto cleanup;
	}

	pdevgrp_status = pldevgrp_statuss;
	while (pdevgrp_status != NULL) {

		/* hide autogen dgs */
		if (scsymon_is_autogen_dg_by_name(
		    pdevgrp_status->scstat_ds_name, pldevgrp_configs)) {
			pdevgrp_status = pdevgrp_status->scstat_ds_next;
			continue;
		}

		/* don't display multiowner device groups */
		if (scconf_is_local_device_service(
		    pdevgrp_status->scstat_ds_name)) {
			pdevgrp_status = pdevgrp_status->scstat_ds_next;
			continue;
		}

		/* fill in property and state for each device group */
		pdevgrp = (scsymon_devgrp_status_t *)calloc(1,
		    sizeof (scsymon_devgrp_status_t));
		if (pdevgrp == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* device group name */
		pdevgrp->pname = strdup((pdevgrp_status->scstat_ds_name == NULL)
		    ? SCSYMON_NULL : pdevgrp_status->scstat_ds_name);
		if (pdevgrp->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* device group state */
		pdevgrp->pstate = strdup((pdevgrp_status->\
		    scstat_ds_statstr == NULL) ?
		    SCSYMON_NULL : pdevgrp_status->scstat_ds_statstr);

		scsymon_error = symon_get_scstat_primarylist(
		    pdevgrp_status->scstat_node_state_list,
		    &pdevgrp->pprimarylist);
		if (scsymon_error != SCSYMON_ENOERR)
			goto cleanup;

		/* add to the list */
		if (pdevgrp_current == NULL) { /* list empty */
			*ppldevgrps = pdevgrp;
			pdevgrp_current = pdevgrp;
		} else {
			pdevgrp_current->pnext = pdevgrp;
			pdevgrp_current = pdevgrp_current->pnext;
		}

		pdevgrp = NULL; /* so we don't accidentally delete it */

		pdevgrp_status = pdevgrp_status->scstat_ds_next;
	}

cleanup:
	if (pldevgrp_configs != NULL)
		scconf_free_ds_config(pldevgrp_configs);
	if (pdevgrp != NULL)
		scsymon_free_devgrp_statuss(pdevgrp);

	return (scsymon_error);
}

/*
 * scsymon_free_devgrp_statuss
 *
 * free device group status
 */
void
scsymon_free_devgrp_statuss(scsymon_devgrp_status_t *pldevgrps)
{
	scsymon_devgrp_status_t *pdevgrp = pldevgrps;

	while (pdevgrp != NULL) {
		scsymon_devgrp_status_t *pnext;

		if (pdevgrp->pname != NULL)
			free(pdevgrp->pname);
		if (pdevgrp->pstate != NULL)
			free(pdevgrp->pstate);
		if (pdevgrp->pprimarylist != NULL)
			free(pdevgrp->pprimarylist);

		pnext = pdevgrp->pnext;
		free(pdevgrp);
		pdevgrp = pnext;
	}
}

/*
 * scsymon_get_replica_statuss
 *
 * get the status of all replicas configured for a device group
 *	device group name
 *	node name
 *	replica state
 *
 * Upon success, a list of objects of scsymon_replica_status_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_replica_statuss().
 *
 * INPUT: pldevgrp_statuss
 * OUTPUT:pplreplicas
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_EPERM		- not root
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_replica_statuss(scstat_ds_t *pldevgrp_statuss,
    scsymon_replica_status_t **pplreplicas)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scstat_ds_t *pdevgrp_status;
	scsymon_replica_status_t *preplica_current = NULL;
	scsymon_replica_status_t *preplica = NULL;

	/*
	 * devgrp config info is needed to filter out
	 * autogenerated groups
	 */

	scconf_errno_t scconf_error = SCCONF_NOERR;
	scconf_cfg_ds_t *pldevgrp_configs = NULL;

	if (pldevgrp_statuss == NULL)
		return (scsymon_error);

	if (pplreplicas == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	scconf_error = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG,
	    &pldevgrp_configs);
	if (scconf_error != SCCONF_NOERR) {
		scsymon_error = scsymon_convert_scconf_error_code(scconf_error);
		goto cleanup;
	}

	pdevgrp_status = pldevgrp_statuss;
	while (pdevgrp_status != NULL) {

		scstat_ds_node_state_t *preplica_status =
		    pdevgrp_status->scstat_node_state_list;

		/* hide autogen dgs */
		if (scsymon_is_autogen_dg_by_name(
		    pdevgrp_status->scstat_ds_name, pldevgrp_configs)) {
			pdevgrp_status = pdevgrp_status->scstat_ds_next;
			continue;
		}

		/* fill state for each replica */
		while (preplica_status != NULL) {
			preplica = (scsymon_replica_status_t *)calloc(1,
			    sizeof (scsymon_replica_status_t));
			if (preplica == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* device group name */
			preplica->pdevgrp_name =
			    strdup((pdevgrp_status->scstat_ds_name == NULL)
			    ? SCSYMON_NULL : pdevgrp_status->scstat_ds_name);
			if (preplica->pdevgrp_name == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* node name */
			preplica->pnode_name =
			    strdup((preplica_status->scstat_node_name == NULL)
			    ? SCSYMON_NULL : preplica_status->scstat_node_name);
			if (preplica->pnode_name == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* replica status */
			preplica->pstate = (char *)calloc(1,
			    SCSYMON_MAX_STRING_LEN);
			if (preplica->pstate == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}
			scstat_get_preference_level_string(
			    preplica_status->scstat_node_state,
			    preplica->pstate);

			/* add to the list */
			if (preplica_current == NULL) { /* list empty */
				*pplreplicas = preplica;
				preplica_current = preplica;
			} else {
				preplica_current->pnext = preplica;
				preplica_current = preplica_current->pnext;
			}

			preplica = NULL;

			preplica_status = preplica_status->scstat_node_next;
		}
		pdevgrp_status = pdevgrp_status->scstat_ds_next;
	}

cleanup:
	if (pldevgrp_configs != NULL)
		scconf_free_ds_config(pldevgrp_configs);
	if (preplica != NULL)
		scsymon_free_replica_statuss(preplica);

	return (scsymon_error);
}

/*
 * scsymon_free_replica_statuss
 *
 * free device group replica status
 */
void
scsymon_free_replica_statuss(scsymon_replica_status_t *plreplicas)
{
	scsymon_replica_status_t *preplica = plreplicas;

	while (preplica != NULL) {
		scsymon_replica_status_t *pnext;

		if (preplica->pdevgrp_name != NULL)
			free(preplica->pdevgrp_name);
		if (preplica->pnode_name != NULL)
			free(preplica->pnode_name);
		if (preplica->pstate != NULL)
			free(preplica->pstate);

		pnext = preplica->pnext;
		free(preplica);
		preplica = pnext;
	}
}
