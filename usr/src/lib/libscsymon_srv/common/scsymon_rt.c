/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scsymon_rt.c	1.14	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <scadmin/scsymon_srv.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <rgm/rgm_scrgadm.h>
#include <scsymon_private.h>

/*
 * scsymon_get_rt_configs
 *
 * get the following properties of all Resource Types
 *	RT name
 *	installed node list
 *	description
 *	RT base directory
 *	single instance or not
 *	init node list
 *	faiover or not
 *	logical hostname, shared address or none
 *	RT dependencies
 *	api version
 *	RT version
 *	packages comprise the RT
 *
 * Upon success, a list of objects of scsymon_rt_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_rt_configs().
 *
 * INPUT: plrgm_rts
 * OUTPUT:pplrts
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rt_configs(scsymon_rgm_rt_t *plrgm_rts,
    scsymon_rt_config_t **pplrts)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scsymon_rgm_rt_t *ptr_rgm_rt;
	rgm_rt_t *prgm_rt = NULL;
	scsymon_rt_config_t *prt = NULL;
	scsymon_rt_config_t *prt_current = NULL;

	if (plrgm_rts == NULL)
		return (scsymon_error);

	if (pplrts == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* get properties of every RT */
	ptr_rgm_rt = plrgm_rts;
	while (ptr_rgm_rt != NULL) {
		prgm_rt = ptr_rgm_rt->prgm_rt;

		prt = (scsymon_rt_config_t *)calloc(1,
		    sizeof (scsymon_rt_config_t));
		if (prt == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* fill in RT properties */
		prt->pname = strdup((prgm_rt->rt_name == NULL) ?
		    SCSYMON_NULL : prgm_rt->rt_name);
		if (prt->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}


		if (prgm_rt->rt_instl_nodes.is_ALL_value == B_TRUE) {
			prt->pinstalled_nodes = strdup(SCSYMON_ALL);
			if (prt->pinstalled_nodes == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}
		} else {
			scsymon_error = symon_get_rgm_nodeidlist(\
			    (prgm_rt->rt_instl_nodes).nodeids,
			    &prt->pinstalled_nodes);
			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;
		}

		prt->pdesc = strdup((prgm_rt->rt_description == NULL) ?
		    SCSYMON_NULL : prgm_rt->rt_description);
		if (prt->pdesc == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pbasedir = strdup((prgm_rt->rt_basedir == NULL) ?
		    SCSYMON_NULL : prgm_rt->rt_basedir);
		if (prt->pbasedir == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->single_inst = prgm_rt->rt_single_inst;

		prt->pinit_nodes = (char *)calloc(1, SCSYMON_MAX_STRING_LEN);
		if (prt->pinit_nodes == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
		switch (prgm_rt->rt_init_nodes) {
		case SCHA_INFLAG_RG_PRIMARIES:
			(void) sprintf(prt->pinit_nodes, "%s",
			    SCSYMON_ALL_POTENTIAL_MASTERS);
			break;
		case SCHA_INFLAG_RT_INSTALLED_NODES:
			(void) sprintf(prt->pinit_nodes, "%s", SCSYMON_ALL);
			break;
		default:
			(void) sprintf(prt->pinit_nodes, "%s", SCSYMON_UNKNOWN);
			break;
		} /* switch (rt->rt_init_nodes) */

		prt->failover = prgm_rt->rt_failover;

		prt->psysdeftype = (char *)calloc(1, SCSYMON_MAX_STRING_LEN);
		if (prt->psysdeftype == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}
		switch (prgm_rt->rt_sysdeftype) {
		case SYST_NONE:
			(void) sprintf(prt->psysdeftype, "%s", SCSYMON_GENERIC);
			break;
		case SYST_LOGICAL_HOSTNAME:
			(void) sprintf(prt->psysdeftype, "%s",
			    SCSYMON_LOGICAL_HOSTNAME);
			break;
		case SYST_SHARED_ADDRESS:
			(void) sprintf(prt->psysdeftype, "%s",
			    SCSYMON_SHARED_ADDRESS);
			break;
		default:
			(void) sprintf(prt->psysdeftype, "%s", SCSYMON_UNKNOWN);
			break;
		} /* switch (rt->rt_sysdeftype) */

		if (prgm_rt->rt_dependencies == NULL) {
			prt->pdepend = strdup(SCSYMON_NULL);
			if (prt->pdepend == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}
		} else {
			scsymon_error = symon_get_rgm_namelist(\
			    prgm_rt->rt_dependencies, &prt->pdepend);
			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;
		}

		prt->api_version = prgm_rt->rt_api_version;

		prt->pversion = strdup((prgm_rt->rt_version == NULL) ?
		    SCSYMON_NULL : prgm_rt->rt_version);
		if (prt->pversion == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		if (prgm_rt->rt_pkglist == NULL) {
			prt->ppkglist = strdup(SCSYMON_NULL);
			if (prt->ppkglist == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}
		} else {
			scsymon_error = symon_get_rgm_namelist(\
			    prgm_rt->rt_pkglist, &prt->ppkglist);
			if (scsymon_error != SCSYMON_ENOERR)
				goto cleanup;
		}

		/* add to the list */
		if (prt_current == NULL) { /* list empty */
			*pplrts = prt;
			prt_current = prt;
		} else {
			prt_current->pnext = prt;
			prt_current = prt_current->pnext;
		}
		prt = NULL;

		ptr_rgm_rt = ptr_rgm_rt->pnext;
	}

cleanup:
	if (prt != NULL)
		scsymon_free_rt_configs(prt);

	return (scsymon_error);
}

/*
 * scsymon_free_rt_configs
 *
 * free Resource Type properties
 */
void
scsymon_free_rt_configs(scsymon_rt_config_t *plrts)
{
	scsymon_rt_config_t *prt = plrts;

	while (prt != NULL) {
		scsymon_rt_config_t *pnext;

		if (prt->pname != NULL)
			free(prt->pname);
		if (prt->pinstalled_nodes != NULL)
			free(prt->pinstalled_nodes);
		if (prt->pdesc != NULL)
			free(prt->pdesc);
		if (prt->pbasedir != NULL)
			free(prt->pbasedir);
		if (prt->pinit_nodes != NULL)
			free(prt->pinit_nodes);
		if (prt->psysdeftype != NULL)
			free(prt->psysdeftype);
		if (prt->pdepend != NULL)
			free(prt->pdepend);
		if (prt->pversion != NULL)
			free(prt->pversion);
		if (prt->ppkglist != NULL)
			free(prt->ppkglist);

		pnext = prt->pnext;
		free(prt);
		prt = pnext;
	}
}

/*
 * scsymon_get_rt_method_configs
 *
 * get the following method pathnames of all Resource Types:
 *	RT name
 *	start method pathname
 *	stop method pathname
 *	primary_change method pathname
 *	validate method pathname
 *	update method pathname
 *	init method pathname
 *	fini method pathname
 *	boot method pathname
 *	monitor_init method pathname
 *	monitor_start method pathname
 *	monitor_stop method pathname
 *	monitor_check method pathname
 *	pre_net_start method pathname
 *	post_net_stop method pathname
 *
 * Upon success, a list of objects of scsymon_rt_method_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_rt_method_configs().
 *
 * INPUT: plrgm_rts
 * OUTPUT:pplrts
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rt_method_configs(scsymon_rgm_rt_t *plrgm_rts,
    scsymon_rt_method_config_t **pplrts)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scsymon_rgm_rt_t *ptr_rgm_rt;
	rgm_rt_t *prgm_rt = NULL;
	rgm_methods_t rgm_rt_methods;
	scsymon_rt_method_config_t *prt = NULL;
	scsymon_rt_method_config_t *prt_current = NULL;

	if (plrgm_rts == NULL)
		return (scsymon_error);

	if (pplrts == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* get method pathnames of every RT */
	ptr_rgm_rt = plrgm_rts;
	while (ptr_rgm_rt != NULL) {
		prgm_rt = ptr_rgm_rt->prgm_rt;

		rgm_rt_methods = prgm_rt->rt_methods;

		prt = (scsymon_rt_method_config_t *)calloc(1,
		    sizeof (scsymon_rt_method_config_t));
		if (prt == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* fill in method pathnames */
		prt->pname = strdup((prgm_rt->rt_name == NULL) ?
		    SCSYMON_NULL : prgm_rt->rt_name);
		if (prt->pname == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pstart = strdup((rgm_rt_methods.m_start == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_start);
		if (prt->pstart == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pstop = strdup((rgm_rt_methods.m_stop == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_stop);
		if (prt->pstop == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pprim_change = strdup(SCSYMON_NULL);
		if (prt->pprim_change == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pvalidate = strdup((rgm_rt_methods.m_validate == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_validate);
		if (prt->pvalidate == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pupdate = strdup((rgm_rt_methods.m_update == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_update);
		if (prt->pupdate == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pinit = strdup((rgm_rt_methods.m_init == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_init);
		if (prt->pinit == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pfini = strdup((rgm_rt_methods.m_fini == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_fini);
		if (prt->pfini == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pboot = strdup((rgm_rt_methods.m_boot == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_boot);
		if (prt->pboot == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pmon_init = strdup(SCSYMON_NULL);
		if (prt->pmon_init == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pmon_start = strdup(
		    (rgm_rt_methods.m_monitor_start == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_monitor_start);
		if (prt->pmon_start == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pmon_stop = strdup(
		    (rgm_rt_methods.m_monitor_stop == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_monitor_stop);
		if (prt->pmon_stop == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->pmon_check = strdup(
		    (rgm_rt_methods.m_monitor_check == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_monitor_check);
		if (prt->pmon_check == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->ppre_net_start = strdup(
		    (rgm_rt_methods.m_prenet_start == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_prenet_start);
		if (prt->ppre_net_start == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		prt->ppost_net_stop = strdup(
		    (rgm_rt_methods.m_postnet_stop == NULL) ?
		    SCSYMON_NULL : rgm_rt_methods.m_postnet_stop);
		if (prt->ppost_net_stop == NULL) {
			scsymon_error = SCSYMON_ENOMEM;
			goto cleanup;
		}

		/* add to the list */
		if (prt_current == NULL) { /* list empty */
			*pplrts = prt;
			prt_current = prt;
		} else {
			prt_current->pnext = prt;
			prt_current = prt_current->pnext;
		}
		prt = NULL;

		ptr_rgm_rt = ptr_rgm_rt->pnext;
	}
cleanup:
	if (prt != NULL)
		scsymon_free_rt_method_configs(prt);

	return (scsymon_error);
}

/*
 * scsymon_free_rt_method_configs
 *
 * free Resource Type method pathnames
 */
void
scsymon_free_rt_method_configs(scsymon_rt_method_config_t *plrts)
{
	scsymon_rt_method_config_t *prt = plrts;

	while (prt != NULL) {
		scsymon_rt_method_config_t *pnext;

		if (prt->pname != NULL)
			free(prt->pname);
		if (prt->pstart != NULL)
			free(prt->pstart);
		if (prt->pstop != NULL)
			free(prt->pstop);
		if (prt->pprim_change != NULL)
			free(prt->pprim_change);
		if (prt->pvalidate != NULL)
			free(prt->pvalidate);
		if (prt->pupdate != NULL)
			free(prt->pupdate);
		if (prt->pinit != NULL)
			free(prt->pinit);
		if (prt->pfini != NULL)
			free(prt->pfini);
		if (prt->pboot != NULL)
			free(prt->pboot);
		if (prt->pmon_init != NULL)
			free(prt->pmon_init);
		if (prt->pmon_start != NULL)
			free(prt->pmon_start);
		if (prt->pmon_stop != NULL)
			free(prt->pmon_stop);
		if (prt->pmon_check != NULL)
			free(prt->pmon_check);
		if (prt->ppre_net_start != NULL)
			free(prt->ppre_net_start);
		if (prt->ppost_net_stop != NULL)
			free(prt->ppost_net_stop);
		pnext = prt->pnext;
		free(prt);
		prt = pnext;
	}
}

/*
 * scsymon_get_rt_param_configs
 *
 * get parameters of all Resource Types
 *	RT name
 *	property name
 *	extenstion or not
 *	tunable
 *	type
 *	default value
 *	min INT type, minlen for STRING and STRINGARRY
 *	max INT type, maxlen for STRING and STRINGARRY
 *	minimum array size for STRINGARRAY type
 *	maximum array size for STRINGARRAY type
 *	description
 *
 * Upon success, a list of objects of scsymon_rt_param_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_rt_param_configs().
 *
 * INPUT: plrgm_rts
 * OUTPUT:pplrts
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EINVAL		- invalid argument
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rt_param_configs(scsymon_rgm_rt_t *plrgm_rts,
    scsymon_rt_param_config_t **pplrts)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	scsymon_rgm_rt_t *ptr_rgm_rt;
	rgm_rt_t *prgm_rt = NULL;
	rgm_param_t **pprgm_rt_params = NULL;
	rgm_param_t *prgm_rt_param = NULL;
	scsymon_rt_param_config_t *prt = NULL;
	scsymon_rt_param_config_t *prt_current = NULL;
	unsigned int i;
	char *ptype = NULL;
	char *pdefault = NULL;

	if (plrgm_rts == NULL)
		return (scsymon_error);

	if (pplrts == NULL) {
		scsymon_error = SCSYMON_EINVAL;
		goto cleanup;
	}

	/* get paramtable of every RT */
	ptr_rgm_rt = plrgm_rts;
	while (ptr_rgm_rt != NULL) {
		prgm_rt = ptr_rgm_rt->prgm_rt;

		pprgm_rt_params = prgm_rt->rt_paramtable;

		if (pprgm_rt_params == NULL)
			continue;

		i = 0;
		while ((prgm_rt_param = pprgm_rt_params[i++]) != NULL) {

			prt = (scsymon_rt_param_config_t *)calloc(1,
			    sizeof (scsymon_rt_param_config_t));
			if (prt == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* fill in parameter property */
			prt->prt_name = strdup((prgm_rt->rt_name == NULL) ?
			    SCSYMON_NULL : prgm_rt->rt_name);
			if (prt->prt_name == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prt->pname = strdup((prgm_rt_param->p_name == NULL) ?
			    SCSYMON_NULL : prgm_rt_param->p_name);
			if (prt->pname == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prt->ext = prgm_rt_param->p_extension;
			prt->per_node = prgm_rt_param->p_per_node;

			prt->ptunable = (char *)calloc(1,
			    SCSYMON_MAX_STRING_LEN);
			if (prt->ptunable == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}
			/* get tunable as string */
			switch (prgm_rt_param->p_tunable) {
			case TUNE_NONE:
				(void) sprintf(prt->ptunable, "%s",
				    SCSYMON_NOT_TUNABLE);
				break;
			case TUNE_AT_CREATION:
				(void) sprintf(prt->ptunable, "%s",
				    SCSYMON_AT_CREATION);
				break;
			case TUNE_ANYTIME:
				(void) sprintf(prt->ptunable, "%s",
				    SCSYMON_ANYTIME);
				break;
			case TUNE_WHEN_DISABLED:
				(void) sprintf(prt->ptunable, "%s",
				    SCSYMON_WHEN_DISABLED);
				break;
			case TUNE_WHEN_OFFLINE:
			case TUNE_WHEN_UNMANAGED:
			case TUNE_WHEN_UNMONITORED:
			default:
				(void) sprintf(prt->ptunable, "%s",
				    SCSYMON_UNKNOWN);
				break;
			} /* switch (prgm_rt_param->p_tunable) */

			prt->ptype = (char *)calloc(1, SCSYMON_MAX_STRING_LEN);
			if (prt->ptype == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}
			prt->pdefault = (char *)calloc(1,
			    SCSYMON_MAX_STRING_LEN);
			if (prt->pdefault == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prt->pmin = (char *)calloc(1, SCSYMON_MAX_STRING_LEN);
			if (prt->pmin == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prt->pmax = (char *)calloc(1, SCSYMON_MAX_STRING_LEN);
			if (prt->pmax == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prt->parraymin = (char *)calloc(1,
			    SCSYMON_MAX_STRING_LEN);
			if (prt->parraymin == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prt->parraymax = (char *)calloc(1,
			    SCSYMON_MAX_STRING_LEN);
			if (prt->parraymax == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* get type, default value and range as string */
			/* lint -save -e788 */
			switch (prgm_rt_param->p_type) {
			case SCHA_PTYPE_STRING:
				(void) sprintf(prt->ptype, "%s",
				    SCSYMON_STRING);
				(void) sprintf(prt->pdefault, "%s",
				    (prgm_rt_param->p_defaultstr == NULL) ?
				    SCSYMON_NULL : prgm_rt_param->p_defaultstr);
				(void) sprintf(prt->pmin, "%d",
				    prgm_rt_param->p_min);
				(void) sprintf(prt->pmax, "%d",
				    prgm_rt_param->p_max);

				break;
			case SCHA_PTYPE_INT:
				(void) sprintf(prt->ptype, "%s", SCSYMON_INT);
				(void) sprintf(prt->pdefault, "%d",
				    prgm_rt_param->p_defaultint);
				(void) sprintf(prt->pmin, "%d",
				    prgm_rt_param->p_min);
				(void) sprintf(prt->pmax, "%d",
				    prgm_rt_param->p_max);

				break;
			case SCHA_PTYPE_BOOLEAN:
				(void) sprintf(prt->ptype, "%s",
				    SCSYMON_BOOLEAN);
				if (prgm_rt_param->p_defaultbool == B_TRUE)
					(void) strcpy(prt->pdefault,
					    SCSYMON_TRUE);
				else
					(void) strcpy(prt->pdefault,
					    SCSYMON_FALSE);

				break;
			case SCHA_PTYPE_ENUM:
				/*
				 * concatenate enum with the comma separated
				 * enum list and store it as the type
				 */
				(void) sprintf(prt->ptype, "%s - ",
				    SCSYMON_ENUM);
				scsymon_error = symon_get_rgm_namelist(
				    prgm_rt_param->p_enumlist,
				    &ptype);
				if (scsymon_error != SCSYMON_ENOERR)
					goto cleanup;

				if (ptype != NULL) {
					(void) snprintf(prt->ptype +
					    strlen(prt->ptype),
					    SCSYMON_MAX_STRING_LEN
					    - strlen(prt->ptype) - 3, "%s",
					    ptype);
					free(ptype);
					ptype = NULL;
				}

				(void) sprintf(prt->pdefault, "%s",
				    (prgm_rt_param->p_defaultstr == NULL) ?
				    SCSYMON_NULL : prgm_rt_param->p_defaultstr);

				break;
			case SCHA_PTYPE_STRINGARRAY:
				(void) sprintf(prt->ptype, "%s",
				    SCSYMON_STRING_ARRAY);
				scsymon_error = symon_get_rgm_namelist(
				    prgm_rt_param->p_defaultarray,
				    &pdefault);
				if (scsymon_error != SCSYMON_ENOERR)
					goto cleanup;

				if (pdefault != NULL) {
					(void) snprintf(prt->pdefault,
					    SCSYMON_MAX_STRING_LEN, "%s",
					    pdefault);
					free(pdefault);
					pdefault = NULL;
				}

				(void) sprintf(prt->pmin, "%d",
				    prgm_rt_param->p_min);
				(void) sprintf(prt->pmax, "%d",
				    prgm_rt_param->p_max);
				(void) sprintf(prt->parraymin, "%d",
				    prgm_rt_param->p_arraymin);
				(void) sprintf(prt->parraymax, "%d",
				    prgm_rt_param->p_arraymax);

				break;
			default:
				(void) sprintf(prt->ptype, "%s",
				    SCSYMON_UNKNOWN);

				break;
			} /* switch (prgm_rt_param->p_type) */
			/* lint -restore */

			/* if not set, reset it to N/A */
			if (prgm_rt_param->p_default_isset == B_FALSE)
				(void) strcpy(prt->pdefault,
				    SCSYMON_NOT_APPLIED);
			if (prgm_rt_param->p_min_isset == B_FALSE)
				(void) strcpy(prt->pmin, SCSYMON_NOT_APPLIED);
			if (prgm_rt_param->p_max_isset == B_FALSE)
				(void) strcpy(prt->pmax, SCSYMON_NOT_APPLIED);
			if (prgm_rt_param->p_arraymin_isset == B_FALSE)
				(void) strcpy(prt->parraymin,
				    SCSYMON_NOT_APPLIED);
			if (prgm_rt_param->p_arraymax_isset == B_FALSE)
				(void) strcpy(prt->parraymax,
				    SCSYMON_NOT_APPLIED);

			prt->pdesc = strdup((prgm_rt_param->p_description ==
			    NULL) ? SCSYMON_NULL :
			    prgm_rt_param->p_description);
			if (prt->pdesc == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* add to the list */
			if (prt_current == NULL) { /* list empty */
				*pplrts = prt;
				prt_current = prt;
			} else {
				prt_current->pnext = prt;
				prt_current = prt_current->pnext;
			}
			prt = NULL;
		}

		ptr_rgm_rt = ptr_rgm_rt->pnext;
	}

cleanup:
	if (prt != NULL)
		scsymon_free_rt_param_configs(prt);

	return (scsymon_error);
}

/*
 * scsymon_free_rt_param_configs
 *
 * free Resource Type paramtables
 */
void
scsymon_free_rt_param_configs(scsymon_rt_param_config_t *plrts)
{
	scsymon_rt_param_config_t *prt = plrts;

	while (prt != NULL) {
		scsymon_rt_param_config_t *pnext;

		if (prt->prt_name != NULL)
			free(prt->prt_name);
		if (prt->pname != NULL)
			free(prt->pname);
		if (prt->ptunable != NULL)
			free(prt->ptunable);
		if (prt->ptype != NULL)
			free(prt->ptype);
		if (prt->pdefault != NULL)
			free(prt->pdefault);
		if (prt->pmin != NULL)
			free(prt->pmin);
		if (prt->pmax != NULL)
			free(prt->pmax);
		if (prt->parraymin != NULL)
			free(prt->parraymin);
		if (prt->parraymax != NULL)
			free(prt->parraymax);
		if (prt->pdesc != NULL)
			free(prt->pdesc);

		pnext = prt->pnext;
		free(prt);
		prt = pnext;
	}
}

/*
 * scsymon_get_rt_rs_configs
 *
 * get the list of Resources of all Resource Types
 *	RT name
 *	RG name
 *	Resource list
 *
 * Upon success, a list of objects of scsymon_rt_rs_config_t is returned.
 * The caller is responsible for freeing the space using
 * scsymon_free_rt_rs_configs().
 *
 * INPUT: plrgm_rts
 * OUTPUT:pplrts
 *
 * Possible return values:
 *	SCSYMON_ENOERR		- no error
 *	SCSYMON_ENOMEM		- not enough memory
 *	SCSYMON_EUNEXPECTED	- internal or unexpected error
 */
scsymon_errno_t
scsymon_get_rt_rs_configs(scsymon_rgm_rt_t *plrgm_rts,
    scsymon_rt_rs_config_t **pplrts)
{
	scsymon_errno_t scsymon_error = SCSYMON_ENOERR;
	char *rt_name;
	scsymon_rgm_rt_t *ptr_rgm_rt;
	rgm_rt_t *prgm_rt;
	scsymon_rgm_rg_rs_t *plrgm_rgs = NULL;
	scsymon_rgm_rg_rs_t *ptr_rgm_rg = NULL;
	rgm_rg_t *prgm_rg;
	char *rg_name;
	scsymon_rgm_rs_t *ptr_rgm_rs = NULL;
	rgm_resource_t *prgm_rs;
	scsymon_rt_rs_config_t *prt = NULL;
	scsymon_rt_rs_config_t *prt_current = NULL;
	char *buf = NULL;
	unsigned int len = 0;
	unsigned int old_len;
	unsigned int buf_size;
	char *delimiter = NULL;

	if (plrgm_rts == NULL)
		return (scsymon_error);

	if (pplrts == NULL)
		return (SCSYMON_EINVAL);

	scsymon_error = scsymon_get_rgm_rg_rss(&plrgm_rgs);
	if (scsymon_error != SCSYMON_ENOERR)
		goto cleanup;

	/* get names of all resources of every resource type */
	ptr_rgm_rt = plrgm_rts;
	while (ptr_rgm_rt != NULL) {
		prgm_rt = ptr_rgm_rt->prgm_rt;
		rt_name = prgm_rt->rt_name;
		ptr_rgm_rg = plrgm_rgs;
		while (ptr_rgm_rg != NULL) {
			len = 0;
			buf_size = SCSYMON_MAX_STRING_LEN;
			buf = (char *)calloc(buf_size, sizeof (char));
			if (buf == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}
			(void) memset(buf, NULL, buf_size * sizeof (char));

			prgm_rg = ptr_rgm_rg->prgm_rg;
			rg_name = prgm_rg->rg_name;

			/*
			 * concatenate names of every resource of a certain
			 * resource type and place the result into buf
			 */
			ptr_rgm_rs = ptr_rgm_rg->plrss;
			while (ptr_rgm_rs != NULL) {
				prgm_rs = ptr_rgm_rs->prgm_rs;
				if (strcmp(prgm_rs->r_type, rt_name) == NULL) {
					/* type match */
					delimiter = (len == 0) ?
					    SCSYMON_LIST_HEAD :
					    SCSYMON_LIST_DELIMITER;

					(void) snprintf(buf + len,
					    buf_size - len, "%s%s",
					    delimiter, prgm_rs->r_name);

					old_len = len;
					len = strlen(buf);

					/* double the buffer if necessary */
					if (len >= buf_size - 1) {
						scsymon_error =
						    scsymon_double_buffer_size(\
						    buf_size,
						    buf,
						    &buf_size,
						    &buf);

						if (scsymon_error !=
						    SCSYMON_ENOERR)
							goto cleanup;

						len = old_len;
						continue;
					}
				}

				ptr_rgm_rs = ptr_rgm_rs->pnext;
			} /* while (ptr_rgm_rs != NULL) */

			prt = (scsymon_rt_rs_config_t *)calloc(1,
			    sizeof (scsymon_rt_rs_config_t));
			if (prt == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			/* fill in rt_rs_config properties */
			prt->prt_name = strdup((rt_name == NULL) ?
			    SCSYMON_NULL : rt_name);
			if (prt->prt_name == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prt->prg_name = strdup((rg_name == NULL) ?
			    SCSYMON_NULL : rg_name);
			if (prt->prg_name == NULL) {
				scsymon_error = SCSYMON_ENOMEM;
				goto cleanup;
			}

			prt->prslist = buf;
			buf = NULL;

			/* add to the list */
			if (prt_current == NULL) { /* list empty */
				*pplrts = prt;
				prt_current = prt;
			} else {
				prt_current->pnext = prt;
				prt_current = prt_current->pnext;
			}
			prt = NULL;

			ptr_rgm_rg = ptr_rgm_rg->pnext;
		} /* while (ptr_rgm_rg != NULL) */

		ptr_rgm_rt = ptr_rgm_rt->pnext;
	} /* while (ptr_rgm_rt != NULL) */

cleanup:
	if (buf != NULL)
		free(buf);

	if (plrgm_rgs != NULL)
		scsymon_free_rgm_rg_rss(plrgm_rgs);

	if (prt != NULL)
		scsymon_free_rt_rs_configs(prt);

	return (scsymon_error);
}

/*
 * scsymon_free_rt_rs_configs
 *
 * free Resource Type Resource list properties
 */
void
scsymon_free_rt_rs_configs(scsymon_rt_rs_config_t *plrts)
{
	scsymon_rt_rs_config_t *prt = plrts;

	while (prt != NULL) {
		scsymon_rt_rs_config_t *pnext;

		if (prt->prt_name != NULL)
			free(prt->prt_name);
		if (prt->prg_name != NULL)
			free(prt->prg_name);
		if (prt->prslist != NULL)
			free(prt->prslist);

		pnext = prt->pnext;
		free(prt);
		prt = pnext;
	}
}
