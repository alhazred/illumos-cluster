#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.27	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libscsymon_srv/Makefile.com
#
include $(SRC)/Makefile.master

LIBRARYCCC= libscsymon_srv.a
VERS=.1

OBJECTS= \
	scsymon_cluster.o	\
	scsymon_node.o	\
	scsymon_devgrp.o \
	scsymon_qdev.o \
	scsymon_transport.o \
	scsymon_rt.o	\
	scsymon_rg.o	\
	scsymon_util.o

# include library definitions
include $(SRC)/lib/Makefile.lib

TEXT_DOMAIN	= SUNW_SC_SALLIB
POFILE=         $(LIBRARYCCC:.a=.po)

MAPFILE	=	../common/mapfile-vers
PMAP	=	-M $(MAPFILE)
SRCS	=		$(OBJECTS:%.o=../common/%.c)

LIBS =		$(DYNLIBCCC)

CLOBBERFILES += $(PIFILES)

MTFLAG	=	-mt
CFLAGS +=	-v
CFLAGS64 +=	-v
CPPFLAGS +=	-I..
LINTFLAGS +=	-I..
LINTFILES += 	$(OBJECTS:%.o=%.ln)

LDLIBS +=       -lclcomm -lclconf \
                -lrgm -lscha -lscconf -lscstat \
                -lclos -lnsl -lrt -lgen -lc -ldl

.KEEP_STATE:

$(DYNLIBCCC):	$(MAPFILE)

FRC:

# include library targets
include $(SRC)/lib/Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
