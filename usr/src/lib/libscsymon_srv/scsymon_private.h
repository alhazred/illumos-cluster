/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#ifndef _SCSYMON_PRIVATE_H
#define	_SCSYMON_PRIVATE_H

#pragma ident	"@(#)scsymon_private.h	1.10	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * libscsymon private functions
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <rgm/rgm_scrgadm.h>
#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <scadmin/scsymon_srv.h>

/* scsymon_util.c */
/*
 * symon_convert_scha_error_code
 *
 * convert a scha error code to scsymon error code
 *
 * Possible return values:
 *
 *      SCSYMON_ENOMEM           - not enough memory
 *	SCSYMON_ENOTCLUSTER	- not a cluster node
 *	SCSYMON_EINVAL		- scconf: invalid argument
 *	SCSYMON_ECLUSTERRECONFIG	- cluster is reconfiguring
 *	SCSYMON_ERGRECONFIG	- RG is reconfiguring
 *	SCSYMON_EOBSOLETE	- Resource/RG has been updated
 *      SCSYMON_EUNEXPECTED      - internal or unexpected error
 */
extern scsymon_errno_t
symon_convert_scha_error_code(const scha_errmsg_t scha_error);

/*
 * symon_get_scconf_namelist
 *
 * concatenate names from a name list and separate them with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
extern scsymon_errno_t symon_get_scconf_namelist(scconf_namelist_t *pname_list,
    char **pbuf);

/*
 * symon_get_scconf_adapterlist
 *
 * concatenate names from a scconf adapter list and separate them with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
extern scsymon_errno_t symon_get_scconf_adapterlist(\
    scconf_cfg_cltr_adap_t *padapter_list, char **pbuf);

/*
 * symon_get_scconf_qdevportlist
 *
 * concatenate names from a scconf quorum device port list and separate them
 * with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
extern scsymon_errno_t symon_get_scconf_qdevportlist(\
    scconf_cfg_qdevport_t *pport_list, char **pbuf);

/*
 * symon_get_scconf_portlist
 *
 * concatenate names from a scconf port list and separate them
 * with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
extern scsymon_errno_t symon_get_scconf_portlist(\
    scconf_cfg_port_t *pport_list, char **pbuf);

/*
 * symon_get_cached_scconf_nodelist
 *
 * concatenate names from a scconf node id list and separate them
 * with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
extern scsymon_errno_t symon_get_cached_scconf_nodelist(\
    scconf_cfg_cluster_t *pcluster_cfg, scconf_nodeid_t *pnodeid_array,
    char **pbuf);

/*
 * symon_get_scstat_primarylist
 *
 * concatenate primary names from a scstat_ds_node_state_t list and separate
 * them with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
extern scsymon_errno_t symon_get_scstat_primarylist(\
    scstat_ds_node_state_t *preplica_list, char **pbuf);

/*
 * symon_get_rgm_namelist
 *
 * concatenate names from a name list and separate them with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
extern scsymon_errno_t symon_get_rgm_namelist(\
    namelist_t *pname_list, char **pbuf);

/*
 * symon_get_rgm_rdeplist
 *
 * concatenate names from a name list and separate them with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
extern scsymon_errno_t symon_get_rgm_rdeplist(\
    rdeplist_t *pname_list, char **pbuf);

/*
 * symon_get_rgm_nodeidlist
 *
 * concatenate names from a nodeidlist struct and separate them with ","
 *
 * A pointer to a buffer containing the concatenated names is returned.
 * The caller is responsible for freeing the space using free().
 */
extern scsymon_errno_t symon_get_rgm_nodeidlist(\
    nodeidlist_t *pnodeid_list, char **pbuf);

/*
 * scsymon_nodeidzone_to_nodename
 *
 * converts the given nodeid and zone name to
 * logical nodename.
 *
 * A pointer to the logical nodename is returned.
 * The caller is responsible for freeing the space
 */
extern char *scsymon_nodeidzone_to_nodename(uint_t nodeid,
    char *zonename);

/*
 * scsymon_is_autogen_dg_by_name
 *
 *	Return 1 if the disk group is autogenerated.
 *      A group is autogenerated if
 *	SCCONF_DS_PROP_AUTOGENERATED property is set.
 *
 *	Otherwise, return zero.
 *
 *      Input : device group name
 *              link list of device groups config info
 */

extern int scsymon_is_autogen_dg_by_name(\
    char *dgname, scconf_cfg_ds_t *dgconfig);

/*
 * scsymon_is_autogen_dg
 *
 *	Return 1 if the disk group is autogenerated.
 *      A group is autogenerated if
 *	SCCONF_DS_PROP_AUTOGENERATED property is set.
 *
 *	Otherwise, return zero.
 *
 *      Input : device gourp config info
 */
extern int scsymon_is_autogen_dg(const scconf_cfg_ds_t *dgp);


#ifdef __cplusplus
}
#endif

#endif	/* _SCSYMON_PRIVATE_H */
