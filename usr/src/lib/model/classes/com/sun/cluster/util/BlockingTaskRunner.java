/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)BlockingTaskRunner.java 1.6     08/05/20 SMI"
 */

package com.sun.cluster.util;

/**
 * Class is a helper class for TimeoutController. This class is responsible for
 * running a task in a new thread.
 */

public class BlockingTaskRunner extends Thread {

    private BlockingTask bTask = null;
    private boolean interrupted = false;
    private Exception exception = null;

    public BlockingTaskRunner(BlockingTask blockTask) {
        bTask = blockTask;
    }

    // The thread starts here.
    public void run() {

        if (bTask == null) {
            return;
        }

        try {
            /*
             * Execute the actual task from here
             */

            bTask.executeTask();
        } catch (Exception exc) {
            exception = exc;
        }

        if (interrupted) {

            /*
             * If this task was interrupted because this task timed out, then
             * we have to undo this task, just in case there is a clean up
             * to be made.
             */
            undoTask();
        }
    }

    /**
     * This method should be called in case the controller of this class decides
     * to interrupt this thread.
     */
    public void undoTaskExecution() {
        interrupted = true;
    }

    /**
     * Used to get the exception that executeTask() caused.
     *
     * @return  Exception that executeTask() caused
     */
    public Exception getException() {
        return exception;
    }

    /**
     * This method will be used to do the cleanup of the task which was
     * attempted.
     */
    private void undoTask() {

        if (bTask == null) {
            return;
        }

        try {
            bTask.undoTask();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}
