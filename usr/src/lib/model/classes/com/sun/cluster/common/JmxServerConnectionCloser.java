/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)JmxServerConnectionCloser.java 1.7     08/05/20 SMI"
 */

package com.sun.cluster.common;

// J2SE
import java.util.Timer;
import java.util.TimerTask;

// JMX
import javax.management.remote.JMXConnector;


/**
 * Helper class that managers a timer queue to close connections - closing can
 * take a long time since there may need to be a TCP timeout.
 */
class JmxServerConnectionCloser extends TimerTask {

    private static Timer myTimer = new Timer(true);

    private JMXConnector jmxc;

    /**
     * Class to schedule background tasks for closing connections
     *
     * @param  jmxc  a <code>JMXConnector</code> value
     */
    private JmxServerConnectionCloser(JMXConnector jmxc) {
        this.jmxc = jmxc;
    }

    /**
     * public method to close a given connection in the background
     *
     * @param  jmxc  a <code>JMXConnector</code> value
     */
    public static void close(JMXConnector jmxc) {

        myTimer.schedule(new JmxServerConnectionCloser(jmxc), 0);
    }

    /**
     * Closes the given connection, or quietly fails trying to do so
     */
    public void run() {

        try {
            jmxc.close();
        } catch (Throwable e) {
            // Expected in some error cases, can ignore
        }
    }
}
