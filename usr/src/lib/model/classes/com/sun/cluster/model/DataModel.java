/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)DataModel.java	1.15	08/07/14 SMI"
 */

package com.sun.cluster.model;

import java.util.HashMap;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import com.iplanet.jato.RequestContext;

import com.sun.cluster.agent.node.NodeData;
import java.util.Collections;
import java.util.Map;

/**
 * DataModel - Data model class.
 *
 * This is the top-level access class for *all* SCM data.
 * DataModel itself doesn't return any data directly, but only
 * returns handles to the sub-data models.
 *
 * DataModel and all sub-data model classes must extend from ModelBase
 */
public class DataModel extends ModelBase {

    static private final String DATAMODEL_HASHMAP = "dataModelHashMap";

    static public  final String CLUSTER_KEY = "cluster";
    static public  final String LOCALHOST = "localhost";

    // This syncObject has to be static because it is used in a static context.
    // It overrides (hides) the one in ModelBase.
    static private Object syncObject = new Object();

    // for no-session case only
    static private DataModel dataModel = null;

    //
    // Component model handles
    //
    private RbacModel rbacModel = null;
    private SessionModel sessionModel = null;


    private NodeModel nodeModel = null;
    private ZoneClusterModel zoneClusterModel = null;
    private ResourceGroupModel resourceGroupModel = null;
    private ResourceModel resourceModel = null;
    private ResourceTypeModel resourceTypeModel = null;

    static public DataModel getDataModel(RequestContext rq) {

	if (rq == null) {
            // no session case
	    if (dataModel == null) {
		dataModel = new DataModel();

                // disable the caching mechanism
                dataModel.setCacheEnabled(false);
	    }
	    return dataModel;
	}

        HttpServletRequest httpRequest = rq.getRequest();
        String clusterName =  getClusterParameter(httpRequest);
	return getDataModelFromSession(httpRequest.getSession(), clusterName);
    }

    static public DataModel getDataModelFromSession(HttpSession session,
            String clusterName) {

        Map map = (Map)session.getAttribute(DATAMODEL_HASHMAP);
        if (map == null) {
            synchronized (syncObject) {
                /**
                 * This is the first call to this method for this session
                 * hence, create the hashMap.
                 * By using the sync wrapper, we don't have to worry about
                 * manually synchronizing it from now on.
                 */
                map = Collections.synchronizedMap(new HashMap());
                session.setAttribute(DATAMODEL_HASHMAP, map);
            }
	}

        if (clusterName == null || clusterName.length() == 0) {
	    clusterName = LOCALHOST;
	}

	DataModel dm = (DataModel)map.get(clusterName);
	if (dm == null) {
	    synchronized (syncObject) {
		if (dm == null) {	// must check again under synchronized
		    dm = new DataModel();
                    map.put(clusterName, dm);
                    session.setAttribute(DATAMODEL_HASHMAP, map);
		}
	    }
	}

        if (!clusterName.equals(LOCALHOST)) {
            // disable the cache mechanism for all remote clusters
            dm.setCacheEnabled(false);
        }

	return dm;
    }

    // private Constructor to one DataModel per cluster
    private DataModel() {
    }

    // return NodeModel
    public NodeModel getNodeModel() {
	if (nodeModel == null) {
	    synchronized (syncObject) {
		if (nodeModel == null) {	// must check again under sync
		    nodeModel = new NodeModel();
		}
	    }
	}
	return nodeModel;
    }

    // return RbacModel
    public RbacModel getRbacModel() {
	if (rbacModel == null) {
	    synchronized (syncObject) {
		if (rbacModel == null) {	// must check again under sync
		    rbacModel = new RbacModel();
		}
	    }
	}
	return rbacModel;
    }

    // return SessionModel
    public SessionModel getSessionModel() {
	if (sessionModel == null) {
	    synchronized (syncObject) {
		if (sessionModel == null) {	// must check again under sync
		    sessionModel = new SessionModel();
		}
	    }
	}
	return sessionModel;
    }

    /**
     * Returns the name of the endpoint used to access the cluster.
     *
     * <p>For now this returns 'localhost' since we don't have a floating IP
     * address and we presume that the SPM is running on the cluster.
     *
     * <p>NOTE : This method might return <code>null</code> depending when you
     * call it. Be aware that in order to be able to retrieve the cluster name,
     * the <code>RequestContext</code> must be available.  This means that this
     * method should not be called in your View constructor, for example.
     *
     * @return  a <code>String</code> value
     */
    static public String getClusterEndpoint() {
	return LOCALHOST;
    }

    static public String getClusterEndpoint(RequestContext rq) {

        if (rq != null && rq.getRequest() != null) {
            String endPoint =  getClusterParameter(rq.getRequest());
            if (endPoint != null && endPoint.length() != 0) {
                return endPoint;
            }
        }
        return getClusterEndpoint();
    }

    /**
     * Returns the name of the endpoint used to access a give node
     *
     * <p>We cannot use the node name as an endpoint since it does not provide a
     * fully qualified name.
     *
     * <p>NOTE : This method might return <code>null</code> depending when you
     * call it. Be aware that in order to be able to retrieve the endpoint, the
     * <code>RequestContext</code> must be available.  This means that this
     * method should not be called in your View constructor, for example.
     *
     * @return  a <code>String</code> value
     */
    static public synchronized String getNodeEndpoint(RequestContext rq,
	String nodeName) {
	NodeData nodeData = null;
	try {
	    DataModel dm = DataModel.getDataModel(rq);
	    nodeData = dm.getNodeModel().getNodeData(rq, nodeName);
            if (nodeData != null) {
                return nodeData.publicInetAddress;
            } else {
                return nodeName;
            }
	} catch (Exception e) {
	    System.out.println("Exception in getNodeEndpoint");
	    return nodeName;
	}
    }

    /**
     * Get cluster prarameter from httpRequest or request attribute
     */
    static public String getClusterParameter(HttpServletRequest request) {

        // first try to get the parameter from httpRequest
        String clusterParam = request.getParameter(CLUSTER_KEY);

        if (clusterParam == null || clusterParam.length() == 0) {
            // not found in the request parameter, try to get it from attribute
            clusterParam = (String)request.getAttribute(CLUSTER_KEY);
        }
        return clusterParam;
    }
    // return ZoneClusterModel
    public ZoneClusterModel getZoneClusterModel() {
	if (zoneClusterModel == null) {
            synchronized (syncObject) {
                if (zoneClusterModel == null) { // must check again under sync
                    zoneClusterModel = new ZoneClusterModel();
                }
            }
        }
	return zoneClusterModel;
    }

    // return ResourceGroupModel
    public ResourceGroupModel getResourceGroupModel() {
        if (resourceGroupModel == null) {
            synchronized (syncObject) {
                if (resourceGroupModel == null) {// must check again under sync
                    resourceGroupModel = new ResourceGroupModel();
                }
            }
        }
	return resourceGroupModel;
    }

    // return ResourceModel
    public ResourceModel getResourceModel() {
        if (resourceModel == null) {
            synchronized (syncObject) {
                if (resourceModel == null) {    // must check again under sync
                    resourceModel = new ResourceModel();
                }
            }
        }
	return resourceModel;
    }

    // return ResourceModel
    public ResourceTypeModel getResourceTypeModel() {
        if (resourceTypeModel == null) {
            synchronized (syncObject) {
                if (resourceTypeModel == null) { // must check again under sync
                    resourceTypeModel = new ResourceTypeModel();
                }
            }
        }
	return resourceTypeModel;
    }
}
