/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ZoneClusterModel.java	1.10	08/09/02 SMI"
 */
package com.sun.cluster.model;

import com.iplanet.jato.RequestContext;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.agent.zonecluster.ZoneClusterGroupMBean;
import com.sun.cluster.agent.zonecluster.ZoneClusterData;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.zonecluster.ZoneClusterStatusEnum;
import com.sun.cluster.common.TrustedMBeanModel;

import com.sun.cluster.common.MBeanModel;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Data model class for Zone Cluster data
 */
public class ZoneClusterModel extends ModelBase {

    public static final String ALL = "ALL";

    boolean zcDataValid = false;
    HashMap zcDataMap;
    Set zcNameSet;
    String[] zoneClusterList = null;

    // Key used to put/retrieve the zonecluster name in the session
    public static final String ZC_NAME = "zc_name";
    // Test Data
    private HashMap testZCDataMap = new HashMap();

    public ZoneClusterModel() {
        // super();
        if (isTestMode) {
            zcDataMap = testZCDataMap;
        }
    }

    private void zoneClusterInit(RequestContext context) throws IOException {
        synchronized (syncObject) {
            if (!zcDataValid) {
                if (!isTestMode) {
                    ZoneClusterGroupMBean mbean =
                            getZoneClusterGroupMBean(context);
                    zcDataMap = mbean.readData();
                }

                // Get the name set for better search
                zcNameSet = zcDataMap.keySet();
		zoneClusterList = (String []) zcNameSet.toArray(new String[0]);

                zcDataValid = true;
            }
        }
    }

    /**
     * Utility functiion to return an array of ZoneClusterData Objects.
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @return Array of ZoneClusterData Objects
     * @throws java.io.IOException
     */
    public ZoneClusterData[] getZoneClusterData(RequestContext rq)
            throws IOException {
        if (!zcDataValid) {
            zoneClusterInit(rq);
        }

        // Return an array of zone cluster data objects
        ZoneClusterData[] zcDataAr = new ZoneClusterData[zcDataMap.size()];
        Iterator iter = zcNameSet.iterator();
        int count = 0;
        while (iter.hasNext()) {
            zcDataAr[count++] = (ZoneClusterData) zcDataMap.get(iter.next());
        }

        return zcDataAr;
    }

    /**
     * Utility function to retrieve ZoneCluster data corresponding to the given
     * zonecluster name.
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @param zoneClusterName ZoneClusterName for which the data is required
     * @return ZoneClusterData object
     * @throws java.io.IOException
     */
    public ZoneClusterData getZoneClusterData(RequestContext rq,
            String zoneClusterName)
            throws IOException {

        if (!zcDataValid) {
            zoneClusterInit(rq);
        }
        return (ZoneClusterData) zcDataMap.get(zoneClusterName);
    }

    /**
     * Utility function to retrieve names of ZoneClusters
     * @param rq RequestContext
     * @return Set of ZoneCluster names
     * @throws java.io.IOException
     */
    public Set getZoneClusterNames(RequestContext rq) throws IOException {
        if (!zcDataValid) {
            zoneClusterInit(rq);
        }
        return zcNameSet;
    }

    /**
     * Utility function to retrieve names of ZoneClusters as an array
     * @param rq RequestContext
     * @return array of zone cluster names
     * @throws java.io.IOException
     */
    public String[] getZoneClusterList(RequestContext rq) throws IOException {
        if (!zcDataValid) {
            zoneClusterInit(rq);
        }

	return zoneClusterList;
    }

    /**
     * Utility function to retrieve node name on which given cluster branded
     * zone identified by the hostname is hosted.
     * @param rq
     * @param zoneClusterName
     * @param hostName
     * @return Name of the node on which this cluster branded zone is hosted
     * @throws java.io.IOException
     */
    public String getZoneNodeName(RequestContext rq,
            String zoneClusterName, String hostName) throws IOException {
        ZoneClusterData zcData = getZoneClusterData(rq, zoneClusterName);
        Map hostNodeMap = zcData.getHostNodeMap();
        return (String)hostNodeMap.get(hostName);
    }

    /**
     * Utility function to retrieve status of given cluster branded zone
     * identified by the given zonecluster name and hostname.
     * @param rq
     * @param zoneClusterName
     * @param hostName
     * @return status of the cluster branded zone
     * @throws java.io.IOException
     */
    public ZoneClusterStatusEnum getZoneHostStatus(RequestContext rq,
            String zoneClusterName, String hostName) throws IOException {
        ZoneClusterData zcData = getZoneClusterData(rq, zoneClusterName);
        Map hostStatusMap = zcData.getHostStatusMap();
        return (ZoneClusterStatusEnum)hostStatusMap.get(hostName);
    }

    /**
     * Utility function to query for running state of a cluster branded zone
     * @param rq
     * @param zoneClusterName
     * @param hostName
     * @return true of the zone is running else false
     * @throws java.io.IOException
     */
    public boolean isZoneRunning(RequestContext rq,
            String zoneClusterName, String hostName) throws IOException {
            return getZoneHostStatus(rq, zoneClusterName, hostName).equals(
                                                ZoneClusterStatusEnum.ONLINE);
    }


    public boolean isZoneClusterRunning(RequestContext rq,
            String zoneClusterName) throws IOException {
	boolean retVal = false;

	List hostList = getZoneClusterData(rq, zoneClusterName).
						    getHostNameList();
	Iterator iterList = hostList.iterator();
	while (iterList.hasNext()) {
	    // If any one of the zone host inside a zonecluster is running
	    // return true.
	    if (isZoneRunning(rq, zoneClusterName, (String)iterList.next())) {
		retVal = true;
		break;
	    }
	}

	return retVal;
    }



    /**
     * Utility function to retrieve all the offline zone hosts, present in
     * the given zonecluster name
     * @param rq RequestContext Object
     * @param zoneClusterName The zonecluster where this query is to be run.
     * If the zonecluster name is ZoneClusterModel.ALL, then this query would
     * search all the clusters.
     * @return
     * @throws java.io.IOException
     */
    public Set getOfflineZoneHost(RequestContext rq, String zoneClusterName)
            throws IOException {
        Set retSet = new HashSet();
        ZoneClusterData[] zcDataAr = new ZoneClusterData[] {};
        if (zoneClusterName != null && zoneClusterName.equals(ALL)) {
            zcDataAr = getZoneClusterData(rq);
        } else {
            zcDataAr = new ZoneClusterData[] {
                getZoneClusterData(rq, zoneClusterName)};
        }

        for (int i = 0; i < zcDataAr.length; i++) {
            List hostNames = zcDataAr[i].getHostNameList();
            Map hostStatusMap = zcDataAr[i].getHostStatusMap();
            Iterator iterHostNames = hostNames.iterator();
            while (iterHostNames.hasNext()) {
                String hostName = (String) iterHostNames.next();
                if (!
                    ((ZoneClusterStatusEnum)hostStatusMap.get(hostName)).equals(
                        ZoneClusterStatusEnum.ONLINE)) {
                    retSet.add(hostName);
                }
            }
        }

        return retSet;
    }


    /**
     * Utility function to retrieve scheduling class for the cluster zone
     * present on this node.
     * @param rq RequestContext Object
     * @param zoneClusterName
     * @return
     * @throws com.sun.cluster.agent.auth.CommandExecutionException
     * @throws java.io.IOException
     */
    public String obtainZoneSchedulingClass(RequestContext rq,
            String zoneClusterName)
            throws CommandExecutionException, IOException {
        String outputString = "";

        ExitStatus status[] = getZoneClusterGroupMBean(rq).
                            obtainZoneSchedulingClass(zoneClusterName);

        for (int j = 0; j < status.length; j++) {
            List output = status[j].getOutStrings();
            Iterator iterator = output.iterator();
            while (iterator.hasNext()) {
                outputString += (String) iterator.next();
            }
        }

        return outputString;
    }


    public void zoneClusterDataChanged(SysEventNotification event) {
        synchronized (syncObject) {
            zcDataValid = false;
        }
        return;
    }

    // This utility method would go in ModelBase class once it gets created in
    // the main gate.
    public static String convertList(List inputList, String delimiter) {

        StringBuffer tmpStr = new StringBuffer();

        Iterator itr = inputList.iterator();
        while (itr.hasNext()) {
            tmpStr.append(itr.next());
            if (itr.hasNext()) {
                tmpStr.append(delimiter);
            }
        }

        return tmpStr.toString();
    }

    private ZoneClusterGroupMBean getZoneClusterGroupMBean(RequestContext rq)
            throws IOException {
        ZoneClusterGroupMBean mbean = null;
        if (rq != null) {
            mbean = (ZoneClusterGroupMBean) MBeanModel.getMBeanProxy(
                    rq,
                    DataModel.getClusterEndpoint(),
                    ZoneClusterGroupMBean.class,
                    null);
        } else {
            // Get ZoneClusterGroupMBean using TrustedMBean Model
            mbean = (ZoneClusterGroupMBean) TrustedMBeanModel.getMBeanProxy(
                    DataModel.getClusterEndpoint(),
                    ZoneClusterGroupMBean.class,
                    null);
        }
        return mbean;
    }
}
