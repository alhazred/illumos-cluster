/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ModelBase.java	1.5	08/05/20 SMI"
 */

package com.sun.cluster.model;

/**
 * ModelBase - base class for all data model classes
 */
public class ModelBase {

    // Object to be used in synchronized {} blocks to
    // protect critical regions to be MT-safe
    // Usage:
    //	    synchronized (syncObject) {
    //		// critical code
    //	    }
    protected Object syncObject = new Object();

    // Caching is enabled by default; to disable caching,
    // use: setCacheEnabled(false)
    protected boolean cacheEnabled = true;

    protected boolean isTestMode = false;

    public boolean isCacheEnabled() {
	return cacheEnabled;
    }

    public void setCacheEnabled(boolean enabled) {
	cacheEnabled = enabled;
    }

    public void test() {
    }
}
