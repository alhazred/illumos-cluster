/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)NodeModel.java	1.11	08/07/14 SMI"
 */

package com.sun.cluster.model;

import com.sun.cluster.common.MBeanModel;

import com.iplanet.jato.RequestContext;

// Cacao
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.agent.node.NodeGroupMBean;
import com.sun.cluster.agent.node.NodeData;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.dataservices.utils.Util;

// CMASS
import com.sun.cluster.common.TrustedMBeanModel;

// JAVA
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.io.IOException;

/**
 * NodeModel - data model for cluster node data
 */
public class NodeModel extends ModelBase {

    /*
     * String Constants which can be used to determine the
     * node type
     */

    public static final String CLUSTER_NODE_TYPE_ID = "1";
    public static final String FARM_NODE_TYPE_ID = "2";

    // TBD: add access auth field to every data structure returned here

    private boolean nodeDataValid = false;
    private NodeData[] nodeDataArray;
    private NodeData[] farmNodeDataArray;

    //
    // Constructor
    //
    public NodeModel() {
    }

    //
    // Nodes methods
    //

    private void nodeInit(RequestContext context)
	throws IOException {
	synchronized (syncObject) {
	    if (!nodeDataValid) {
		NodeGroupMBean mbean = getNodeGroupMBean(context);
		NodeData[] allNodes = mbean.readData();

		ArrayList regNodes = new ArrayList();
		ArrayList farmNodes = new ArrayList();
		for (int i = 0; i < allNodes.length; i++) {
		    if (allNodes[i].nodeType.equals(
			FARM_NODE_TYPE_ID)) {
			farmNodes.add(allNodes[i]);
		    } else {
			regNodes.add(allNodes[i]);
		    }
		}
		nodeDataArray = (NodeData[])regNodes.toArray(new NodeData[0]);
		farmNodeDataArray = (NodeData[])farmNodes.toArray(
		    new NodeData[0]);

		if (isCacheEnabled()) {
		    nodeDataValid = true;
		}
	    }
	}
    }


    // get only regular nodes *excluding* Farm nodes
    public NodeData[] getNodesData(RequestContext rq)
	throws IOException {
	if (!nodeDataValid) {
	    nodeInit(rq);
	}

	return nodeDataArray;
    }

    public NodeData getNodeData(RequestContext rq, String name)
	throws IOException {

	if (!nodeDataValid) {
	    nodeInit(rq);
	}

	// TBH: make MBeanModel cache all proxies per session;

	NodeData nodeData = null;
	for (int i = 0; i < nodeDataArray.length; i++) {
	    NodeData zData = nodeDataArray[i];
	    if (zData.name.equals(name) ||
		zData.publicInetAddress.equals(name)) {
		nodeData = zData;
		break;
	    }
	}

	return nodeData;
    }

    public Set getNodesByStatus(RequestContext rq, boolean online)
	throws IOException {

	if (!nodeDataValid) {
	    nodeInit(rq);
	}

        HashSet s = new HashSet();
	for (int i = 0; i < nodeDataArray.length; i++) {

	    NodeData node = nodeDataArray[i];

	    if (node.isOnline == online)
		s.add(node);
	}

	return s;
    }

    public Set getNodeNames(RequestContext rq) throws IOException {
	if (!nodeDataValid) {
	    nodeInit(rq);
	}

        HashSet s = new HashSet();
	for (int i = 0; i < nodeDataArray.length; i++) {
	    s.add(nodeDataArray[i].name);
	}

	return s;
    }

    public void nodeDataChanged(SysEventNotification event) {
	synchronized (syncObject) {
	    nodeDataValid = false;
	}
	return;
    }

    private NodeGroupMBean getNodeGroupMBean(RequestContext rq)
	throws IOException {
	NodeGroupMBean mbean = null;
	if (rq != null) {
	    mbean = (NodeGroupMBean) MBeanModel.getMBeanProxy(
		    rq,
		    DataModel.getClusterEndpoint(rq),
		    NodeGroupMBean.class,
		    null);
	} else {
	    // Get NodeGroupMBean using TrustedMBean Model
	    mbean = (NodeGroupMBean) TrustedMBeanModel.getMBeanProxy(
		    DataModel.getClusterEndpoint(rq),
		    NodeGroupMBean.class,
		    null);
	}
	return mbean;
    }

}
