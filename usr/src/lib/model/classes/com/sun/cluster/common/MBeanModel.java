/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)MBeanModel.java	1.17	08/05/20 SMI"
 */

package com.sun.cluster.common;


// JATO/Lockhart
import com.iplanet.jato.RequestContext;

// Cacao
import com.sun.cacao.ObjectNameFactory;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.util.OperationTimedOutException;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

// JMX
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.QueryExp;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;


/**
 * Class that is responsible for providing access to mbean proxies for a given
 * type of mbean
 */
public class MBeanModel {

    /**
     * Obtain a proxy handle for a specific MBean on a given agent
     *
     * <p>The methods Object.toString(), Object.hashCode(), and
     * Object.equals(Object), when invoked on a proxy are forwarded to the MBean
     * server as methods on the proxied MBean. This will only work if the MBean
     * declares those methods in its management interface.
     *
     * @param  rq  callers <code>RequestContext</code> value
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  instanceName  instance key for the mbean type in question
     *
     * @return  an mbean proxy
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    public static Object getMBeanProxy(RequestContext rq, String agentLocation,
        Class mbeanInterface, String instanceName) throws java.io.IOException {

        return getMBeanProxy(rq, agentLocation, mbeanInterface, instanceName,
                false);
    }

    /**
     * Obtain a snapshotProxy handle for a specific MBean on a given agent
     *
     * <p>The methods Object.toString(), Object.hashCode(), and
     * Object.equals(Object), when invoked on a proxy are forwarded to the MBean
     * server as methods on the proxied MBean. This will only work if the MBean
     * declares those methods in its management interface.
     *
     * @param  rq  callers <code>RequestContext</code> value
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  instanceName  instance key for the mbean type in question
     *
     * @return  a <code>MBeanMBean</code> value
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    public static Object getMBeanSnapshotProxy(RequestContext rq,
        String agentLocation, Class mbeanInterface, String instanceName)
        throws java.io.IOException {

        return getMBeanProxy(rq, agentLocation, mbeanInterface, instanceName,
                true);
    }

    /**
     * Obtain a proxy handle for a specific MBean on a given agent
     *
     * <p>The methods Object.toString(), Object.hashCode(), and
     * Object.equals(Object), when invoked on a proxy are forwarded to the MBean
     * server as methods on the proxied MBean. This will only work if the MBean
     * declares those methods in its management interface.
     *
     * @param  rq  callers <code>RequestContext</code> value
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  instanceName  instance key for the mbean type in question
     * @param  snapshotProxy  return a snapshot proxy
     *
     * @return  an mbean proxy
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    public static Object getMBeanProxy(RequestContext rq, String agentLocation,
        Class mbeanInterface, String instanceName, boolean snapshotProxy)
        throws java.io.IOException {

        ObjectNameFactory onf = new ObjectNameFactory(mbeanInterface
                .getPackage().getName());

        ObjectName name = onf.getObjectName(mbeanInterface, instanceName);

        return RetryMBeanServerInvocationHandler.newProxyInstance(rq,
                agentLocation, name, mbeanInterface, snapshotProxy);
    }

    /**
     * Obtain a proxy handle for a specific MBean on a given agent in a given
     * domain.
     *
     * <p>The methods Object.toString(), Object.hashCode(), and
     * Object.equals(Object), when invoked on a proxy are forwarded to the MBean
     * server as methods on the proxied MBean. This will only work if the MBean
     * declares those methods in its management interface.
     *
     * @param  rq  callers <code>RequestContext</code> value
     * @param  domain  containing MBean
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  instanceName  instance key for the mbean type in question
     * @param  snapshotProxy  return a snapshot proxy
     *
     * @return  an mbean proxy
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    public static Object getMBeanProxy(RequestContext rq, String domain,
        String agentLocation, Class mbeanInterface, String instanceName,
        boolean snapshotProxy) throws java.io.IOException {
        ObjectNameFactory onf = new ObjectNameFactory(domain);

        ObjectName name = onf.getObjectName(mbeanInterface, instanceName);

        return RetryMBeanServerInvocationHandler.newProxyInstance(rq,
                agentLocation, name, mbeanInterface, snapshotProxy);
    }


    /**
     * Obtain the mbean names for all instances of this type in the agent
     *
     * @param  mbsc  a valid mbean server connection for this query
     * @param  rq  callers <code>RequestContext</code> value
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  queryExp  optional query expression used for filtering
     *
     * @return  a <code>Set</code> of <code>ObjectName</code> values
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    private static Set getMBeanObjectNames(MBeanServerConnection mbsc,
        RequestContext rq, String agentLocation, Class mbeanInterface,
        QueryExp queryExp) throws java.io.IOException {

        ObjectNameFactory onf = new ObjectNameFactory(mbeanInterface
                .getPackage().getName());

        ObjectName pattern = onf.getObjectNamePattern(mbeanInterface);

        return mbsc.queryNames(pattern, queryExp);
    }

    /**
     * Obtain the names of all instances of this object at the given agent
     *
     * @param  rq  callers <code>RequestContext</code> value
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  queryExp  optional query expression used for filtering
     *
     * @return  a <code>List</code> of all instance names as strings
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    public static Set getInstanceNames(RequestContext rq, String agentLocation,
        Class mbeanInterface, QueryExp queryExp) throws java.io.IOException {

        MBeanServerConnection mbsc = null;

        try {
            mbsc = JmxServerConnection.getInstance().getConnection(rq,
                    agentLocation);
        } catch (OperationTimedOutException oprtnExc) {

            /**
             * If we are here then it means than the connection timed out.
             * There is no point attempting to re-connect. Hence, throwing
             * an IOException from here.
             */
            throw (new IOException(oprtnExc.getMessage()));
        }

        Set mbeanNames = getMBeanObjectNames(mbsc, rq, agentLocation,
                mbeanInterface, queryExp);
        Set instanceNames = new java.util.TreeSet();

        java.util.Iterator i = mbeanNames.iterator();
        ObjectNameFactory onf = new ObjectNameFactory(mbeanInterface
                .getPackage().getName());

        while (i.hasNext()) {
            ObjectName objectName = (ObjectName) i.next();
            instanceNames.add(onf.getInstanceName(objectName));
        }

        return instanceNames;
    }

    /**
     * Obtain the attribute values of this object for the specified instances at
     * the given agent
     *
     * @param  rq  callers <code>RequestContext</code> value
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  attributeName  a <code>String</code> value representing the
     * attribute name
     * @param  instanceName  a <code>String</code> value representing the
     * instance key for the mbean type in question or null for all instances
     * @param  queryExp  optional query expression used for filtering when no
     * instance key has been specified
     *
     * @return  a <code>SortedMap</code> where keys are the instance names and
     * values are the attribute value objects
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     * @exception  AttributeNotFoundException  if an error occurs
     * @exception  InstanceNotFoundException  if an error occurs
     * @exception  MBeanException  if an error occurs
     * @exception  ReflectionException  if an error occurs
     */
    public static Map getAttributeValues(RequestContext rq,
        String agentLocation, Class mbeanInterface, String attributeName,
        String instanceName, QueryExp queryExp) throws java.io.IOException,
        AttributeNotFoundException, InstanceNotFoundException, MBeanException,
        ReflectionException {

        // Get all instance names if no instance specified
        Set instanceNames = null;

        if (instanceName == null) {
            instanceNames = getInstanceNames(rq, agentLocation, mbeanInterface,
                    queryExp);
        } else
            instanceNames.add(instanceName);

        // Connect to the MBean Server
        MBeanServerConnection mbsc = null;

        try {
            mbsc = JmxServerConnection.getInstance().getConnection(rq,
                    agentLocation);
        } catch (OperationTimedOutException oprtnExc) {

            /**
             * If we are here then it means than the connection timed out.
             * There is no point attempting to re-connect. Hence, throwing
             * an IOException from here.
             */
            throw (new IOException(oprtnExc.getMessage()));
        }

        // Build the Set to be returned by iterating on instances
        TreeMap attributeMap = new TreeMap();
        Iterator iterator = instanceNames.iterator();
        ObjectNameFactory onf = new ObjectNameFactory(mbeanInterface
                .getPackage().getName());

        while (iterator.hasNext()) {

            // Build the ObjectName for this instance of MBean
            String instance = (String) iterator.next();
            ObjectName objectName = onf.getObjectName(mbeanInterface, instance);

            // Add the attribute object to the Set to be returned
            attributeMap.put(instance,
                mbsc.getAttribute(objectName, attributeName));
        }

        return attributeMap;
    }

    /**
     * Obtain a List of proxies for all instances of a given type on the given
     * agent. We can't put proxies in a Set since they don't have a valid
     * hashcode.
     *
     * <p>The methods Object.toString(), Object.hashCode(), and
     * Object.equals(Object), when invoked on a proxy are forwarded to the MBean
     * server as methods on the proxied MBean. This will only work if the MBean
     * declares those methods in its management interface.
     *
     * @param  rq  callers <code>RequestContext</code> value
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  snapshotProxies  if true, proxies returned are snapshot proxies
     * @param  queryExp  optional query expression used for filtering
     *
     * @return  a <code>List</code> of proxy objects
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    public static List getMBeanProxies(RequestContext rq, String agentLocation,
        Class mbeanInterface, boolean snapshotProxies, QueryExp queryExp)
        throws java.io.IOException {

        Set instanceNames = getInstanceNames(rq, agentLocation, mbeanInterface,
                queryExp);

        List proxyMBeans = new ArrayList(instanceNames.size());

        java.util.Iterator i = instanceNames.iterator();

        while (i.hasNext()) {
            String instanceName = (String) i.next();
            Object mbean = getMBeanProxy(rq, agentLocation, mbeanInterface,
                    instanceName, snapshotProxies);
            proxyMBeans.add(mbean);
        }

        return proxyMBeans;
    }

    /**
     * Obtain a List of proxies for all instances of a given type on the given
     * agent in a given domain. We can't put proxies in a Set since they don't
     * have a valid hashcode.
     *
     * <p>The methods Object.toString(), Object.hashCode(), and
     * Object.equals(Object), when invoked on a proxy are forwarded to the MBean
     * server as methods on the proxied MBean. This will only work if the MBean
     * declares those methods in its management interface.
     *
     * @param  rq  callers <code>RequestContext</code> value
     * @param  domain  containing MBean
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  snapshotProxies  if true, proxies returned are snapshot proxies
     * @param  queryExp  optional query expression used for filtering
     *
     * @return  a <code>List</code> of proxy objects
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    public static List getMBeanProxies(RequestContext rq, String domain,
        String agentLocation, Class mbeanInterface, boolean snapshotProxies,
        QueryExp queryExp) throws java.io.IOException {

        Set instanceNames = getInstanceNames(rq, agentLocation, mbeanInterface,
                queryExp);

        List proxyMBeans = new ArrayList(instanceNames.size());

        java.util.Iterator i = instanceNames.iterator();

        while (i.hasNext()) {
            String instanceName = (String) i.next();
            Object mbean = getMBeanProxy(rq, domain, agentLocation,
                    mbeanInterface, instanceName, snapshotProxies);
            proxyMBeans.add(mbean);
        }

        return proxyMBeans;
    }

    /**
     * Obtain a proxy handle for a specific MBean on a given agent
     *
     * <p>The methods Object.toString(), Object.hashCode(), and
     * Object.equals(Object), when invoked on a proxy are forwarded to the MBean
     * server as methods on the proxied MBean. This will only work if the MBean
     * declares those methods in its management interface.
     *
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  instanceName  instance key for the mbean type in question
     *
     * @return  an mbean proxy
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    public static Object getMBeanProxy(String agentLocation,
        Class mbeanInterface, String instanceName) throws java.io.IOException,
        SecurityException {

        return getMBeanProxy(agentLocation, mbeanInterface, instanceName,
                false);
    }

    /**
     * Obtain a snapshotProxy handle for a specific MBean on a given agent
     *
     * <p>The methods Object.toString(), Object.hashCode(), and
     * Object.equals(Object), when invoked on a proxy are forwarded to the MBean
     * server as methods on the proxied MBean. This will only work if the MBean
     * declares those methods in its management interface.
     *
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  instanceName  instance key for the mbean type in question
     *
     * @return  a <code>MBeanMBean</code> value
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    public static Object getMBeanSnapshotProxy(String agentLocation,
        Class mbeanInterface, String instanceName) throws java.io.IOException,
        SecurityException {

        return getMBeanProxy(agentLocation, mbeanInterface, instanceName, true);
    }

    /**
     * Gets the proxy of the MBean using WellKnownConnection This method is to
     * be used by CLI Wizards.
     *
     * @param  agentLocation  Location of the Agent
     * @param  mbeanInterface  Interface Class of the MBean
     * @param  instanceName  instance key for the mbean type in question
     * @param  snapshotProxy  return a snapshot proxy
     */
    public static Object getMBeanProxy(String agentLocation,
        Class mbeanInterface, String instanceName, boolean snapshotProxy)
        throws IOException, SecurityException {

        JMXConnector connector = TrustedMBeanModel.getWellKnownConnector(
                agentLocation);

        // Now get a reference to the mbean server connection
        MBeanServerConnection mbsc = connector.getMBeanServerConnection();

        // Get the ObjectNameFactory
        ObjectNameFactory dsOnf = new ObjectNameFactory(mbeanInterface
                .getPackage().getName());

        ObjectName objName = dsOnf.getObjectName(mbeanInterface, instanceName);

        // Look up the CacaoMBean instance
        Object mBean = MBeanServerInvocationHandler.newProxyInstance(mbsc,
                objName, mbeanInterface, snapshotProxy);

        return mBean;
    }

    /**
     * Obtain the mbean names for all instances of this type in the agent
     *
     * @param  mbsc  a valid mbean server connection for this query
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  queryExp  optional query expression used for filtering
     *
     * @return  a <code>Set</code> of <code>ObjectName</code> values
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    private static Set getMBeanObjectNames(MBeanServerConnection mbsc,
        String agentLocation, Class mbeanInterface, QueryExp queryExp)
        throws java.io.IOException, SecurityException {

        ObjectNameFactory onf = new ObjectNameFactory(mbeanInterface
                .getPackage().getName());

        ObjectName pattern = onf.getObjectNamePattern(mbeanInterface);

        return mbsc.queryNames(pattern, queryExp);
    }

    /**
     * Obtain the names of all instances of this object at the given agent
     *
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  queryExp  optional query expression used for filtering
     *
     * @return  a <code>List</code> of all instance names as strings
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    public static Set getInstanceNames(String agentLocation,
        Class mbeanInterface, QueryExp queryExp) throws java.io.IOException,
        SecurityException {

        JMXConnector connector = TrustedMBeanModel.getWellKnownConnector(
                agentLocation);

        // Now get a reference to the mbean server connection
        MBeanServerConnection mbsc = connector.getMBeanServerConnection();

        Set mbeanNames = getMBeanObjectNames(mbsc, agentLocation,
                mbeanInterface, queryExp);
        Set instanceNames = new java.util.TreeSet();

        java.util.Iterator i = mbeanNames.iterator();
        ObjectNameFactory onf = new ObjectNameFactory(mbeanInterface
                .getPackage().getName());

        while (i.hasNext()) {
            ObjectName objectName = (ObjectName) i.next();
            instanceNames.add(onf.getInstanceName(objectName));
        }

        return instanceNames;
    }

    /*
     * Obtain the attribute values of this object for the specified instances
     * at the given agent
     *
     * @param agentLocation cluster or node name used to locate agent
     * @param mbeanInterface class of the mbean interface
     * @param attributeName a <code>String</code> value representing the
     * attribute name
     * @param queryExp optional query expression used for filtering when
     * no instance key has been specified
     * @return a <code>SortedMap</code> where keys are the instance names and
     * values are the attribute value objects
     * @exception java.io.IOException if a communication or authorization
     * error occurs
     * @exception AttributeNotFoundException if an error occurs
     * @exception InstanceNotFoundException if an error occurs
     * @exception MBeanException if an error occurs
     * @exception ReflectionException if an error occurs
     */
    public static Map getAttributeValues(String agentLocation,
        Class mbeanInterface, String attributeName, String instanceName,
        QueryExp queryExp) throws java.io.IOException,
        AttributeNotFoundException, InstanceNotFoundException, MBeanException,
        ReflectionException {

        // Get all instance names if no instance specified
        Set instanceNames = null;

        if (instanceName == null) {
            instanceNames = getInstanceNames(agentLocation, mbeanInterface,
                    queryExp);
        } else
            instanceNames.add(instanceName);


        JMXConnector connector = TrustedMBeanModel.getWellKnownConnector(
                agentLocation);

        // Now get a reference to the mbean server connection
        MBeanServerConnection mbsc = connector.getMBeanServerConnection();

        // Build the Set to be returned by iterating on instances
        TreeMap attributeMap = new TreeMap();
        Iterator iterator = instanceNames.iterator();
        ObjectNameFactory onf = new ObjectNameFactory(mbeanInterface
                .getPackage().getName());

        while (iterator.hasNext()) {

            // Build the ObjectName for this instance of MBean
            String instance = (String) iterator.next();
            ObjectName objectName = onf.getObjectName(mbeanInterface, instance);

            // Add the attribute object to the Set to be returned
            attributeMap.put(instance,
                mbsc.getAttribute(objectName, attributeName));
        }

        return attributeMap;
    }

    /**
     * Obtain a List of proxies for all instances of a given type on the given
     * agent. We can't put proxies in a Set since they don't have a valid
     * hashcode.
     *
     * <p>The methods Object.toString(), Object.hashCode(), and
     * Object.equals(Object), when invoked on a proxy are forwarded to the MBean
     * server as methods on the proxied MBean. This will only work if the MBean
     * declares those methods in its management interface.
     *
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  mbeanInterface  class of the mbean interface
     * @param  snapshotProxies  if true, proxies returned are snapshot proxies
     * @param  queryExp  optional query expression used for filtering
     *
     * @return  a <code>List</code> of proxy objects
     *
     * @exception  java.io.IOException  if a communication or authorization
     * error occurs
     */
    public static List getMBeanProxies(String agentLocation,
        Class mbeanInterface, boolean snapshotProxies, QueryExp queryExp)
        throws java.io.IOException {

        Set instanceNames = getInstanceNames(agentLocation, mbeanInterface,
                queryExp);

        List proxyMBeans = new ArrayList(instanceNames.size());

        java.util.Iterator i = instanceNames.iterator();

        while (i.hasNext()) {
            String instanceName = (String) i.next();
            Object mbean = getMBeanProxy(agentLocation, mbeanInterface,
                    instanceName, snapshotProxies);
            proxyMBeans.add(mbean);
        }

        return proxyMBeans;
    }

}
