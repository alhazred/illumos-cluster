/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */


/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceModel.java	1.12	09/01/05 SMI"
 */

package com.sun.cluster.model;

// JATO
import com.iplanet.jato.RequestContext;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.rgm.RGroupMBean;
import com.sun.cluster.agent.rgm.ResourceData;
import com.sun.cluster.agent.rgm.RgmManagerMBean;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyBoolean;
import com.sun.cluster.agent.rgm.property.ResourcePropertyEnum;
import com.sun.cluster.agent.rgm.property.ResourcePropertyInteger;
import com.sun.cluster.agent.rgm.property.ResourcePropertyString;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;

import com.sun.cluster.common.MBeanModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 * Data model class for Resource data
 */
public class ResourceModel extends ModelBase {

    HashMap rDataValid = new HashMap();

    /*
     * HashMap to hold Resource Data corresponding to physical cluster
     * OR zoneclusters. ZoneCluster name would be null in case of physical
     * cluster (OR the cluster in which this code is running) in the following
     * structure.
     *
     * This HashMap is a key value pair of ZoneClusterName and a HashMap as
     * follows :
     *  KEY                           VALUE
     *  ResourceGroupName             HashMap object with following structure
     *                                KEY                     VALUE
     *                                Resource name           ResourceData
     *                                                        Object
     */
    HashMap zc_rg_r_data_map = new HashMap();


    // Test Data - should be of type zc_rg_r_data_map
    private HashMap testRDataMap = new HashMap();

    public ResourceModel() {
        // super();
        if (isTestMode) {
            zc_rg_r_data_map = testRDataMap;
        }
    }

    private void resourceInit(
            RequestContext context,
            String zoneClusterName)
            throws IOException {
        synchronized (syncObject) {
            if (!isDataValid(zoneClusterName)) {
                HashMap rg_r_data_map;
                if (!isTestMode) {
                    RGroupMBean mbean = getRGroupMBean(context);
                    rg_r_data_map = mbean.readData(zoneClusterName, null);

                    // Store the retrieved map
                    zc_rg_r_data_map.put(zoneClusterName, rg_r_data_map);
                }

                setDataValid(zoneClusterName, true);
            }
        }
    }

    /**
     * Utility function to retrieve an array of ResourceData Objects for
     * the given zoneClusterName and ResourceGroup name combination.
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @param zoneClusterName If this is null the method would return all the
     *                        resources present in the cluster it is run.
     * @param resourceGroupName If this null the data returned would be for
     *                          given zonecluster name.
     * @return Array of ResourceData Objects
     * @throws java.io.IOException
     */
    public ResourceData[] getResourceData(
            RequestContext rq,
            String zoneClusterName,
            String resourceGroupName)
            throws IOException {
        if (!isDataValid(zoneClusterName)) {
            resourceInit(rq, zoneClusterName);
        }

        HashMap r_data_map;
	ArrayList rDataList = new ArrayList();

        // Retrieve the rg_r hashmap for the corresponding zonecluster (OR the
        // the cluster in which this code is running)
        HashMap rg_r_data_map = (HashMap) zc_rg_r_data_map.get(zoneClusterName);

        if (resourceGroupName != null) {
            // Retrieve all the resources for the given resource group name
            r_data_map = (HashMap) rg_r_data_map.get(resourceGroupName);
            Iterator iter = r_data_map.keySet().iterator();
            while (iter.hasNext()) {
		rDataList.add((ResourceData) r_data_map.get(iter.next()));
            }
        } else {
            // Retrieve all the resources for the given zonecluster (OR the
            // cluster in which this code is running)
            // Fill the values in the array
            Iterator iter_rg_r = rg_r_data_map.keySet().iterator();
            while (iter_rg_r.hasNext()) {
                r_data_map = (HashMap)rg_r_data_map.get(iter_rg_r.next());
                Iterator iter_r = r_data_map.keySet().iterator();
                while (iter_r.hasNext()) {
		    rDataList.add((ResourceData) r_data_map.get(iter_r.next()));
                }
            }
        }

	return (ResourceData[]) rDataList.toArray(new ResourceData[0]);
    }

    /**
     * Utility function to retrieve data corresponding to given resource name.
     * The method would search the resource name in the given zonecluster and
     * resource group name combination.
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @param zoneClusterName If this is null the resource search would be in
     *                        physical cluster or in the cluster in which this
     *                        method is run.
     * @param resourceGroupName If this is null the resource search would be
     *                          among all the resources of the cluster in which
     *                          this method is run.
     * @param resourceName
     * @return ResourceData object
     * @throws java.io.IOException
     */
    public ResourceData getResourceData(
            RequestContext rq,
            String zoneClusterName,
            String resourceGroupName,
            String resourceName)
            throws IOException {

        if (!isDataValid(zoneClusterName)) {
            resourceInit(rq, zoneClusterName);
        }

        HashMap r_data_map;
        ResourceData rData = null;

        // Retrieve the rg_r hashmap for the corresponding zonecluster (OR the
        // the cluster in which this code is running)
        HashMap rg_r_data_map = (HashMap) zc_rg_r_data_map.get(zoneClusterName);

        if (resourceGroupName != null) {
            // Retrieve all the resources for the given resource group name
            r_data_map = (HashMap) rg_r_data_map.get(resourceGroupName);
            rData = (ResourceData) r_data_map.get(resourceName);
        } else {
            // Retrieve all the resources for the given zonecluster (OR the
            // cluster in which this code is running)

            Iterator iter_rg_r = rg_r_data_map.keySet().iterator();

            while (iter_rg_r.hasNext()) {
                r_data_map = (HashMap)rg_r_data_map.get(iter_rg_r.next());
                Object tmpObj = r_data_map.get(resourceName);
                if (tmpObj != null) {
                    rData = (ResourceData)tmpObj;
                    break;
                }
            }
        }

        return rData;
    }

    /**
     * Utility function to retrieve data corresponding to given resource of the
     * given type and with a particular property name and value.
     * The method would search the resource type in the given zonecluster.
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @param zoneClusterName If this is null the resource search would be in
     *                        physical cluster or in the cluster in which this
     *                        method is run.
     * @param rgList list of resource groups
     * @param resourceType the resource type to search for
     * @param propertyName the property name to search for
     * @param propertyValue the property value to search for
     * @return list of resource names or ResourceData objects
     * @throws java.io.IOException
     */
    public List getResourceList(
	RequestContext rq,
	String zoneClusterName,
	List rgList,
	String resourceType,
	String propertyName,
	Object propertyValue) throws IOException {
	return getResourceList(rq, zoneClusterName, rgList, resourceType,
		propertyName, propertyValue, false);
    }
 
    /**
     * Utility function to retrieve data corresponding to given resource of the
     * given type and with a particular property name and value.
     * The method would search the resource type in the given zonecluster.
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @param zoneClusterName If this is null the resource search would be in
     *                        physical cluster or in the cluster in which this
     *                        method is run.
     * @param rgList list of resource groups
     * @param resourceType the resource type to search for
     * @param propertyName the property name to search for
     * @param propertyValue the property value to search for
     * @param bAllData if true return resourceData else return resource names
     * @return list of resource names or ResourceData objects
     * @throws java.io.IOException
     */
    public List getResourceList(
	RequestContext rq,
	String zoneClusterName,
	List rgList,
	String resourceType,
	String propertyName,
	Object propertyValue,
	boolean bAllData) throws IOException {

	if (!isDataValid(zoneClusterName)) {
	    resourceInit(rq, zoneClusterName);
	}

	HashMap r_data_map;
	ResourceData rData = null;
	ArrayList resourceList = null;

	// Retrieve the rg_r hashmap for the corresponding ZC or base cluster
	HashMap rg_r_data_map = (HashMap) zc_rg_r_data_map.get(
	    zoneClusterName);

	if (rgList != null) {
	    int size = rgList.size();
	    for (int i = 0; i < size; i++) {
		// Retrieve all the resources for the given resource group name
		String rgName = (String) rgList.get(i);
		r_data_map = (HashMap) rg_r_data_map.get(rgName);

		Iterator iter_r = r_data_map.keySet().iterator();
		while (iter_r.hasNext()) {
		    Object tmpObj = r_data_map.get(iter_r.next());
		    if (tmpObj != null) {
			rData = (ResourceData)tmpObj;

			String type = rData.getType();
			if (type.startsWith(resourceType)) {
			    if (propertyName != null && propertyValue != null) {
				if (matchingPropertyFound(rData,
				    propertyName, propertyValue)) {
				    if (resourceList == null) {
					resourceList = new ArrayList();
				    }

				    if (bAllData) {
					resourceList.add(rData);
				    } else {
					resourceList.add(rData.getName());
				    }
				}
			    } else {
				// just look for matching type
				if (resourceList == null) {
				    resourceList = new ArrayList();
				}
				resourceList.add(rData);
			    }
			}
		    }
		}
            }
	} else {

	    // Retrieve all the resources
	    Iterator iter_rg_r = rg_r_data_map.keySet().iterator();
	    int count = 0;
	    while (iter_rg_r.hasNext()) {
		r_data_map = (HashMap)rg_r_data_map.get(iter_rg_r.next());

		Iterator iter_r = r_data_map.keySet().iterator();
		while (iter_r.hasNext()) {
		    Object tmpObj = r_data_map.get(iter_r.next());
		    if (tmpObj != null) {
			rData = (ResourceData)tmpObj;

			String type = rData.getType();

			if (type.startsWith(resourceType)) {
			    if (propertyName != null && propertyValue != null) {
				if (matchingPropertyFound(rData,
				    propertyName, propertyValue)) {
				    if (resourceList == null) {
					resourceList = new ArrayList();
				    }
				    if (bAllData) {
					resourceList.add(rData);
				    } else {
					resourceList.add(rData.getName());	
				    }
				}
			    } else {
				// just look for matching type
				if (resourceList == null) {
				    resourceList = new ArrayList();
				}
				resourceList.add(rData);
			    }
			}
		    }
		}
	    }
	}

	return resourceList;
    }

    /**
     *  Utility function to check if a matching property name and value
     * is found.
     */
    private boolean matchingPropertyFound(ResourceData rsData,
	String propertyName, Object propertyValue) {

	// find a matching propertyName and value
	boolean found = false;

	// Find the property name in Extension Properties
	List extProps = rsData.getExtProperties();
	Iterator extPropIter = extProps.iterator();

	while (extPropIter.hasNext() && !found) {
	    ResourceProperty rp = (ResourceProperty) extPropIter.next();

	    if (isPropertyValueEqual(rp, propertyName, propertyValue)) {
		found = true;
	    }
	}

	// If not found, find the property name in System Properties
	if (!found) {
	    List sysProps = rsData.getSystemProperties();
	    Iterator sysPropIter = sysProps.iterator();

	    while (sysPropIter.hasNext() && !found) {
		ResourceProperty rp = (ResourceProperty) sysPropIter.next();

		if (isPropertyValueEqual(rp, propertyName, propertyValue)) {
		    found = true;
		}
	    }
	}
	return found;
    }

    /**
     * Checks for a Property Name and Value against a ResourceProperty Object
     *
     * @param  rp  ResourcePropertyObject
     * @param  propertyName  PropertyName to check
     * @param  propertyValue  PropertyValue to check
     */
    public boolean isPropertyValueEqual(ResourceProperty rp,
        String propertyName, Object propertyValue) {


        String entry = rp.getName().trim();

        if (entry.equals(propertyName)) {

            // If Boolean Property
            if (rp instanceof ResourcePropertyBoolean) {
                ResourcePropertyBoolean rpBool = (ResourcePropertyBoolean) rp;
                Boolean value = (Boolean) propertyValue;

                if (value.equals(rpBool.getValue())) {
                    return true;
                }
            }

            // If Integer Property
            if (rp instanceof ResourcePropertyInteger) {
                ResourcePropertyInteger rpInt = (ResourcePropertyInteger) rp;
                Integer value = (Integer) propertyValue;

                if (value.equals(rpInt.getValue())) {
                    return true;
                }
            }
            // If Enum Property
            if (rp instanceof ResourcePropertyEnum) {
                ResourcePropertyEnum rpEnum = (ResourcePropertyEnum) rp;

                if (rpEnum.getEnumList().containsAll((List) propertyValue)) {
                    return true;
                }
            }

            // If String Property
            if (rp instanceof ResourcePropertyString) {
                ResourcePropertyString rpStr = (ResourcePropertyString) rp;
                String value = (String) propertyValue;

                if (value.equals(rpStr.getValue())) {
                    return true;
                }
            }

            // If String[] Property
            if (rp instanceof ResourcePropertyStringArray) {
                ResourcePropertyStringArray rpStrArr =
                    (ResourcePropertyStringArray) rp;
                String valueArr[] = (String[]) propertyValue;
                String matchArr[] = rpStrArr.getValue();

                for (int i = 0; i < matchArr.length; i++) {
                    String match = matchArr[i];

                    for (int j = 0; j < valueArr.length; j++) {
                        String value = valueArr[j];

                        if (value.equals(match)) {
                            return true;
                        }
                    }
                }
            }
        }
	return false;
    }

    /**
     * Utility function to retrieve data corresponding to given resource of the
     * given type.
     * The method would search the resource type in the given zonecluster.
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @param zoneClusterName If this is null the resource search would be in
     *                        physical cluster or in the cluster in which this
     *                        method is run.
     * @param resourceType the resource type to search for
     * @return ResourceData object
     * @throws java.io.IOException
     */
    public ResourceData getResourceBasedOnType(
            RequestContext rq,
            String zoneClusterName,
            String resourceType) throws IOException {

        if (!isDataValid(zoneClusterName)) {
            resourceInit(rq, zoneClusterName);
        }

        HashMap r_data_map;
        ResourceData rData = null;

        // Retrieve the rg_r hashmap for the corresponding zonecluster (OR the
        // the cluster in which this code is running)
        HashMap rg_r_data_map = (HashMap) zc_rg_r_data_map.get(zoneClusterName);

	// Retrieve all the resources for the given zonecluster (OR the
	// cluster in which this code is running)

	Iterator iter_rg_r = rg_r_data_map.keySet().iterator();

	boolean found = false;
	while (iter_rg_r.hasNext() && !found) {
	    r_data_map = (HashMap)rg_r_data_map.get(iter_rg_r.next());

	    Iterator iter_r = r_data_map.keySet().iterator();
	    while (iter_r.hasNext() && !found) {
		Object tmpObj = r_data_map.get(iter_r.next());
                if (tmpObj != null) {
                    rData = (ResourceData)tmpObj;
		    String type = rData.getType();

		    if (type.startsWith(resourceType)) {
			found = true;
		    }
                }
            }
        }
	if (found) {
	    return rData;
	} else {
	    return null;
	}
    }


    /**
     * Get the property value corresponding to given resource name and extension
     * property name.
     * @param rq
     * @param zoneClusterName
     * @param rsName
     * @param propName
     * @return
     * @throws java.io.IOException
     */
    public Object getResourcePropertyValue(RequestContext rq,
	String zoneClusterName, String rsName, String propName)
	throws IOException {

	Object retObj = null;

	ResourceData rData = getResourceData(
	    rq, zoneClusterName, null, rsName);

	if (rData != null) {
	    List rExtProps = rData.getExtProperties();

	    Iterator iterExtProps = rExtProps.iterator();
	    boolean found = false;

	    while (iterExtProps.hasNext() && !found) {
		ResourceProperty property = (ResourceProperty) iterExtProps
		    .next();
		if (property.getName().equals(propName)) {
		    retObj = getResourcePropertyValue(property, false);
		    found = true;
		}
	    }
	}

	return retObj;
    }


    /**
     * Retrieve the value from a ResourceProperty Object
     *
     * @param  resourceProperty  ResourceProperty whose value is needed
     * @param  def  Whether the default value is needed
     *
     * @return  value of the ResourceProperty, default value if default is set
     * to true.
     */
    public Object getResourcePropertyValue(ResourceProperty rp,
	boolean def) {

	// If Boolean Property
	if (rp instanceof ResourcePropertyBoolean) {
	    ResourcePropertyBoolean rpBool = (ResourcePropertyBoolean) rp;

	    if (def) {
		return rpBool.getDefaultValue();
	    } else {
		return rpBool.getValue();
	    }
	}

	// If Integer Property
	if (rp instanceof ResourcePropertyInteger) {
	    ResourcePropertyInteger rpInt = (ResourcePropertyInteger) rp;
	    if (def) {
		return rpInt.getDefaultValue();
	    } else {
		return rpInt.getValue();
	    }
	}

	// If Enum Property
	if (rp instanceof ResourcePropertyEnum) {
	    ResourcePropertyEnum rpEnum = (ResourcePropertyEnum) rp;

	    if (def) {
		return rpEnum.getDefaultValue();
	    } else {
		return rpEnum.getValue();
	    }
	}

	// If String Property
	if (rp instanceof ResourcePropertyString) {
	    ResourcePropertyString rpStr = (ResourcePropertyString) rp;

	    if (def) {
		return rpStr.getDefaultValue();
	    } else {
		return rpStr.getValue();
	    }
	}

	// If String[] Property
	if (rp instanceof ResourcePropertyStringArray) {
	    ResourcePropertyStringArray rpStrArr = (ResourcePropertyStringArray)
		rp;

	    if (def) {
		return rpStrArr.getDefaultValue();
	    } else {
		return rpStrArr.getValue();
	    }
	}

	return null;
    }


    /**
     *
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @param zoneClusterName
     * @param rgName
     * @return
     * @throws java.io.IOException
     */
    public Set getResourceNames(
            RequestContext rq,
            String zoneClusterName,
            String rgName) throws IOException {
        if (!isDataValid(zoneClusterName)) {
            resourceInit(rq, zoneClusterName);
        }

        HashMap r_data_map;
        Set resNames = new HashSet();

        // Retrieve the rg_r hashmap for the corresponding zonecluster (OR the
        // the cluster in which this code is running)
        HashMap rg_r_data_map = (HashMap) zc_rg_r_data_map.get(zoneClusterName);

        if (rgName != null) {
            // Retrieve all the resources for the given resource group name
            r_data_map = (HashMap) rg_r_data_map.get(rgName);
            resNames = r_data_map.keySet();
        } else {
            // Retrieve all the resources for the given zonecluster (OR the
            // cluster in which this code is running)

            Iterator iter_rg_r = rg_r_data_map.keySet().iterator();
            int count = 0;
            while (iter_rg_r.hasNext()) {
                r_data_map = (HashMap)rg_r_data_map.get(iter_rg_r.next());
                if (count == 0) {
                    resNames = r_data_map.keySet();
                    count += 1;
                } else {
                    resNames.addAll(r_data_map.keySet());
                }
            }
        }
        return resNames;
    }


    /**
     * Enable resources specified by comma separated string rs, present in
     * zonecluster specified by zoneClusterName.
     * @param rq RequestContext Object
     * @param zoneClusterName
     * @param rs
     * @return
     * @throws CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] enableRs(RequestContext rq,
            String zoneClusterName, String rs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "enable", zoneClusterName, rs);
    }

    /**
     * Disable resources specified by comma separated string rs, present in
     * zonecluster specified by zoneClusterName.
     * @param rq RequestContext Object
     * @param zoneClusterName
     * @param rs
     * @return
     * @throws CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] disableRs(RequestContext rq,
            String zoneClusterName, String rs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "disable", zoneClusterName, rs);
    }

    /**
     * Start Monitor resources specified by comma separated string rs, present
     * in zonecluster specified by zoneClusterName.
     * @param rq RequestContext Object
     * @param zoneClusterName
     * @param rs
     * @return
     * @throws CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] startMonitorRs(RequestContext rq,
            String zoneClusterName, String rs)
        throws CommandExecutionException, IOException {
        return executeOperation(rq, "enableMonitor", zoneClusterName, rs);
    }

    /**
     * Stop Monitor resources specified by comma separated string rs, present
     * in zonecluster specified by zoneClusterName.
     * @param rq RequestContext Object
     * @param zoneClusterName
     * @param rs
     * @return
     * @throws CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] stopMonitorRs(RequestContext rq,
            String zoneClusterName, String rs)
        throws CommandExecutionException, IOException {
        return executeOperation(rq, "disableMonitor", zoneClusterName, rs);
    }

    /**
     * Clear Stop Failed flag for the specified resource , present in
     * zonecluster specified by zoneClusterName.
     * @param rq RequestContext Object
     * @param zoneClusterName
     * @param res
     * @return
     * @throws CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] clearStopFailedFlag(RequestContext rq,
            String zoneClusterName, String res)
        throws CommandExecutionException, IOException {
        return executeOperation(rq, "clearStopFailedFlag",
                                                zoneClusterName, res);
    }


    /**
     * Enable resource on a per-node basis.
     * @param rq RequestContext Object
     * @param zoneClusterName
     * @param rsName
     * @param nodeList
     * @return
     * @throws com.sun.cluster.agent.auth.CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] pernodeEnableRs(RequestContext rq,
            String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException, IOException {
        return executeOperation(rq, "pernodeEnable", zoneClusterName,
                rsName, nodeList);
    }

    /**
     * Disable resource on a per-node basis.
     * @param rq RequestContext Object
     * @param zoneClusterName
     * @param rsName
     * @param nodeList
     * @return
     * @throws com.sun.cluster.agent.auth.CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] pernodeDisableRs(RequestContext rq,
            String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException, IOException {
        return executeOperation(rq, "pernodeDisable", zoneClusterName,
                rsName, nodeList);
    }

    /**
     * Start Monitors for resource on a node basis.
     * @param rq RequestContext Object
     * @param zoneClusterName
     * @param rsName
     * @param nodeList
     * @return
     * @throws com.sun.cluster.agent.auth.CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] pernodeStartMonitorRs(RequestContext rq,
            String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException, IOException {
        return executeOperation(rq, "pernodeStartMonitor", zoneClusterName,
                rsName, nodeList);
    }

    /**
     * Stop Monitors for resource on a per-node basis.
     * @param rq RequestContext Object
     * @param zoneClusterName
     * @param rsName
     * @param nodeList
     * @return
     * @throws com.sun.cluster.agent.auth.CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] pernodeStopMonitorRs(RequestContext rq,
            String zoneClusterName,
            String rsName, String nodeList)
        throws CommandExecutionException, IOException {
        return executeOperation(rq, "pernodeStopMonitor", zoneClusterName,
                rsName, nodeList);
    }

    /**
     * This method creates a new instance of a Resource with its default system
     * values.
     * @param rq RequestContext Object
     * @param name the name of the resource. This name should be unique and
     * can not be modified after the creation of the Resource.
     * @param rgName the name of the resource group owning this new resource.
     * @param type the resource type used for the creation of this resource.
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @param systemProperties the system properties associated to this
     * resource (might be null).
     * @param extProperties the extended properties associated to this
     * resource (might be null).
     * @param commit
     * @return
     * @throws com.sun.cluster.agent.auth.CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] addResource(RequestContext rq, String name,
            String rgName, String type, String zoneClusterName,
            List systemProperties, List extProperties, boolean commit)
            throws CommandExecutionException, IOException {
        invalidateCache(commit);
        return getRGroupMBean(rq).addResource(
                name, rgName, type, zoneClusterName, systemProperties,
                extProperties, commit);
    }

    /**
     * Utility method to delete resource
     * @param rq RequestContext Object
     * @param name the name of the resource
     * @param zoneClusterName name of the zone cluster or if null current
     * cluster scope.
     * @return
     * @throws com.sun.cluster.agent.auth.CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] deleteResource(RequestContext rq, String name,
            String zoneClusterName)
            throws CommandExecutionException, IOException {
        invalidateCache(true);
        return getRGroupMBean(rq).remove(name, zoneClusterName);
    }


    /**
     * Change some of the resource's system properties.
     *
     * @param rq RequestContext Object
     * @param name the name of the resource
     * @param  newSystemProperties  a <code>List</code> value containing keys
     * with instances of
     * {@link com.sun.cluster.agent.rgm.property.ResourceProperty}.
     * @param zoneClusterName name of the zone cluster or null for base cluster
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     * @throws java.io.IOException
     */
    public ExitStatus[] changeSystemProperties(RequestContext rq,
            String name, List newSystemProperties,
	String zoneClusterName, boolean commit)
        throws CommandExecutionException, IOException {
        invalidateCache(commit);
        return getRGroupMBean(rq).changeSystemProperties(
                name, newSystemProperties, zoneClusterName, commit);
    }

    /**
     * Change some of the resource's extend properties.
     *
     * @param rq RequestContext Object
     * @param name the name of the resource
     * @param  newExtProperties  a <code>List</code> value containing keys with
     * instances of {@link com.sun.cluster.agent.rgm.property.ResourceProperty}.
     * @param zoneClusterName the name of the zone cluster or null for base
     * cluster
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     * @throws java.io.IOException
     */
    public ExitStatus[] changeExtProperties(RequestContext rq,
            String name, List newExtProperties,
	String zoneClusterName, boolean commit)
        throws CommandExecutionException, IOException {
        invalidateCache(commit);
        return getRGroupMBean(rq).changeExtProperties(
                name, newExtProperties, zoneClusterName, commit);
    }

    /**
     * Any Resource event would lead to complete Data Map refresh.
     * We could handle this by maintaining a hashmap of zoneclustername and
     * individual 'dataValid' parameters.
     *
     * @param event
     */
    public void resourceDataChanged(SysEventNotification event) {
        synchronized (syncObject) {
            // Need to update all the data, since there can be inter-cluster
            // resource dependencies
            Boolean tmpVal = new Boolean(false);
            Set names = rDataValid.keySet();
            Iterator iter = names.iterator();
            while (iter.hasNext()) {
                rDataValid.put(iter.next(), tmpVal);
            }
        }
        return;
    }


    private void invalidateCache(boolean commit) {
        synchronized (syncObject) {
            if (commit) {
                Boolean tmpVal = new Boolean(false);
                Set names = rDataValid.keySet();
                Iterator iter = names.iterator();
                while (iter.hasNext()) {
                    rDataValid.put(iter.next(), tmpVal);
                }
            }
        }
    }


    private ExitStatus[] executeOperation(RequestContext rq,
            String operationName, String zoneClusterName, String RList)
        throws CommandExecutionException, IOException {
        ExitStatus[] status = null;
        RgmManagerMBean rgmMB = getRgmManagerMBean(rq);
        invalidateCache(true);
        if (operationName.equals("clearStopFailedFlag")) {
            status = getRGroupMBean(rq).clearStopFailedFlag(
                                                RList, zoneClusterName);
        } else if (operationName.equals("enable")) {
            status = rgmMB.enableRs(zoneClusterName, RList);
        } else if (operationName.equals("disable")) {
            status = rgmMB.disableRs(zoneClusterName, RList);
        } else if (operationName.equals("enableMonitor")) {
            status = rgmMB.startMonitorRs(zoneClusterName, RList);
        } else if (operationName.equals("disableMonitor")) {
            status = rgmMB.stopMonitorRs(zoneClusterName, RList);
        }
        return status;
    }


    private ExitStatus[] executeOperation(RequestContext rq,
            String operationName, String zoneClusterName, String RList,
            String nodeList)
        throws CommandExecutionException, IOException {
        ExitStatus[] status = null;
        RgmManagerMBean rgmMB = getRgmManagerMBean(rq);
        invalidateCache(true);
        if (operationName.equals("pernodeEnable")) {
            status = rgmMB.pernodeEnableRs(zoneClusterName, RList, nodeList);
        } else if (operationName.equals("pernodeDisable")) {
            status = rgmMB.pernodeDisableRs(zoneClusterName, RList, nodeList);
        } else if (operationName.equals("pernodeStartMonitor")) {
            status = rgmMB.pernodeStartMonitorRs(zoneClusterName,
                    RList, nodeList);
        } else if (operationName.equals("pernodeStopMonitor")) {
            status = rgmMB.pernodeStopMonitorRs(zoneClusterName,
                    RList, nodeList);
        }
        return status;
    }


    private RGroupMBean getRGroupMBean(RequestContext rq)
            throws IOException {
        RGroupMBean mbean = null;
        if (rq != null) {
            mbean = (RGroupMBean) MBeanModel.getMBeanProxy(
                    rq,
                    DataModel.getClusterEndpoint(rq),
                    RGroupMBean.class,
                    null);
        } else {
            // Get RgGroupMBean using TrustedMBean Model
            mbean = (RGroupMBean) TrustedMBeanModel.getMBeanProxy(
                    DataModel.getClusterEndpoint(rq),
                    RGroupMBean.class,
                    null);
        }
        return mbean;
    }


    private RgmManagerMBean getRgmManagerMBean(RequestContext rq)
            throws IOException {
        RgmManagerMBean mbean = null;
        if (rq != null) {
            mbean = (RgmManagerMBean) MBeanModel.getMBeanProxy(
                    rq,
                    DataModel.getClusterEndpoint(rq),
                    RgmManagerMBean.class,
                    null);
        } else {
            // Get RgGroupMBean using TrustedMBean Model
            mbean = (RgmManagerMBean) TrustedMBeanModel.getMBeanProxy(
                    DataModel.getClusterEndpoint(rq),
                    RgmManagerMBean.class,
                    null);
        }
        return mbean;
    }


    private boolean isDataValid(String zoneClusterName) {
        Boolean val = new Boolean(false);
        Object testObj = rDataValid.get(zoneClusterName);
        if (testObj != null) {
            val = (Boolean) testObj;
        }
        return val.booleanValue();
    }

    private void setDataValid(String zoneClusterName, boolean val) {
        rDataValid.put(zoneClusterName, new Boolean(val));
    }
}
