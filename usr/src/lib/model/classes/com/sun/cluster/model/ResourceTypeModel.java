/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceTypeModel.java	1.2	08/07/10 SMI"
 */

package com.sun.cluster.model;

// JATO
import com.iplanet.jato.RequestContext;

// CMASS
import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.rgm.ResourceTypeData;
import com.sun.cluster.agent.rgm.RtGroupMBean;
import com.sun.cluster.common.TrustedMBeanModel;

import com.sun.cluster.common.MBeanModel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import javax.management.Attribute;


/**
 * Data model class for Resource Type data
 */
public class ResourceTypeModel extends ModelBase {

    /**
     * We cannot cache data here which is unlike other model classes, because
     * resourcetype data would always have to be fetched owing to unavailability
     * corresponding events.
     */

    public ResourceTypeModel() {
    }


    /**
     * Utility method to retrieve data corresponding to ResourceTypes.
     *
     * @param rq RequestContext Object
     * @param zoneClusterName for which the query is to be done. If this is
     *        null, the method would return all the ResourceGroups present in
     *        the cluster it is run.
     * @param qAttribute If this is not null, return rtnames having an attribute
     * matching this one.
     * @return HashMap with following structure
     *                  KEY                     VALUE
     *                  ResourceTypeName       ResourceTypeData Object
     * @throws java.io.IOException
     */
    public HashMap getRTData(RequestContext rq,
            String zoneClusterName, Attribute qAttribute)
            throws IOException {
        return getRtGroupMBean(rq).readData(zoneClusterName, qAttribute);
    }

    /**
     * Utility method to retrieve data corresponding to given rt name and
     * zoneclustername combination.
     * @param rq
     * @param rtName
     * @param zoneClusterName
     * @return ResourceTypeData object
     * @throws java.io.IOException
     */
    public ResourceTypeData getRTData(RequestContext rq, String rtName,
            String zoneClusterName) throws IOException {
        Attribute tmpAttr = new Attribute("Name", rtName);
        HashMap retMap = getRTData(rq, zoneClusterName, tmpAttr);
        return (ResourceTypeData)retMap.get(rtName);
    }


    /**
     * Utility method to retrieve all RT names corresponding to given attribute
     * and zoneclustername combination.
     * @param rq RequestContext Object
     * @param zoneClusterName
     * @param qAttribute javax.management.Attribute to help in search
     * @return Set of rt names
     * @throws java.io.IOException
     */
    public Set getRTNames(RequestContext rq, String zoneClusterName,
            Attribute qAttribute) throws IOException {
        return getRTData(rq, zoneClusterName, qAttribute).keySet();
    }


    /**
     * This method register a new resource type so that it can be used by the
     * user.
     *
     * @param rq RequestContext rq
     * @param rType the name of the resource type to register.
     * @param zoneClusterName the name of the zone cluster
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     * @throws java.io.IOException
     */
    public ExitStatus[] registerResourceType(RequestContext rq, String rType,
            String zoneClusterName)
	throws CommandExecutionException, IOException {
        return getRtGroupMBean(rq).registerResourceType(rType, zoneClusterName);
    }

    private RtGroupMBean getRtGroupMBean(RequestContext rq)
            throws IOException {
        RtGroupMBean mbean = null;
        if (rq != null) {
            mbean = (RtGroupMBean) MBeanModel.getMBeanProxy(
                    rq,
                    DataModel.getClusterEndpoint(rq),
                    RtGroupMBean.class,
                    null);
        } else {
            // Get RtGroupMBean using TrustedMBean Model
            mbean = (RtGroupMBean) TrustedMBeanModel.getMBeanProxy(
                    DataModel.getClusterEndpoint(rq),
                    RtGroupMBean.class,
                    null);
        }
        return mbean;
    }
}
