/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)JmxServerConnection.java	1.25	08/07/14 SMI"
 */
package com.sun.cluster.common;


// JATO/Lockhart
import com.iplanet.jato.RequestContext;
import com.iplanet.jato.RequestManager;

import com.iplanet.sso.SSOException;

// Cacao
import com.sun.cacao.agent.JmxClient;
import com.sun.cacao.ObjectNameFactory;

import com.sun.cluster.model.MBeanNotificationListener;
import com.sun.cluster.model.DataModel;
import com.sun.cluster.util.BlockingTask;
import com.sun.cluster.util.OperationTimedOutException;
import com.sun.cluster.util.TimeoutController;

import com.sun.management.services.authentication.UserRoleCredential;
import com.sun.management.services.authentication.UserRolePrincipal;
import com.sun.management.services.logging.ConsoleLogService;

// JDK
import java.io.IOException;
import java.net.InetAddress;

import java.util.HashMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;
import javax.security.auth.Subject;

// JMX
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;

import javax.management.remote.JMXConnectionNotification;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

// CMAS
import com.sun.cluster.agent.event.SysEventNotifierMBean;


/**
 * Class that manages connections to JMX servers (ie. agents/services) It hides
 * the details of the connection from the caller, and can optionally cache
 * connections for performance reasons. Connections are always made using the
 * caller's security credentials.
 */
public final class JmxServerConnection implements NotificationListener,
    BlockingTask {

    /**
     * prefix used in HttpSession attributes for JMX connections (suffix is
     * endpoint
     */
    public static final String JMX_CONNECTION_PREFIX = "JMX_CONNECTION : ";

    /** myself will contain the only instance of this class */
    private static JmxServerConnection myself;
    private static Logger consoleLogger;

    /**
     * Performing initializations at the time of loading this class
     */
    static {

        /**
         * Initialize myself here itself. This is done here so that we do not
         * have to worry about synchronizing getInstance() (which is the only
         * way to obtain an instance of this class). Setting the static variable
         *
         * here itself ensures that we have only one instance of this class.
         * This
         * is set by the jvm at the time of loading this class. Hence,we do not
         * have to synchronize getInstance().
         */
        myself = new JmxServerConnection();
        consoleLogger = ConsoleLogService.getConsoleLogger();
    }

    /**
     * These variables are required for storing the information regarding the
     * connection we were attempting to make. The thread which calls
     * getConnection() is different from the thread which called
     * getJmxConnector(). We need these variables so that we can store
     * information between these threads.
     */
    private JMXConnector currentJMXConn;
    private Subject currentSub;
    private String currentAgentLoc;
    private RequestContext currentContext;
    private String remAgentLoc;
    private RequestContext remContext;
    private MBeanNotificationListener nl;

    /**
     * Making the constructor private. This helps us in ensuring that this is a
     * Singleton. An instance of this class can be created only from within this
     * class. People outside this class can obtain an instance of this class
     * using the static method getInstance().
     */
    private JmxServerConnection() {

    }

    /**
     * Obtain an MBean Server connection
     *
     * @param  requestContext  required to obtain credentials for connection
     * @param  agentLocation  cluster or node name used to locate agent
     *
     * @return  a <code>MBeanServerConnection</code> value
     *
     * @exception  IOException  if a communication or authorization error occurs
     */
    public final synchronized MBeanServerConnection getConnection(
        com.iplanet.jato.RequestContext requestContext, String agentLocation)
        throws IOException, OperationTimedOutException {
        HttpSession session = requestContext.getRequest().getSession();

	// The following code creates a single hashkey for the nodename
	// and its ipaddress. This reduces the number of the JMX
	// Connections stored in the session by half.
	String connectionKey = agentLocation;
	if (!agentLocation.equals(
                DataModel.getClusterEndpoint(requestContext))) {
	    connectionKey = DataModel.getNodeEndpoint(requestContext,
		agentLocation);
	}

        // Try and get a live connection from our session
        JMXConnector jmxc = (JMXConnector)session.getAttribute(
                JMX_CONNECTION_PREFIX + connectionKey);

	MBeanServerConnection mbsc = null;
        if (jmxc != null) {
            try {
                mbsc = jmxc.getMBeanServerConnection();
                return mbsc;
            } catch (IOException e) {
                session.removeAttribute(JMX_CONNECTION_PREFIX + connectionKey);
            }
        }

        // create a connection and add it to the session
        try {

            if (!connectionKey.equals(
                    InetAddress.getLocalHost().getHostAddress()) &&
                    !agentLocation.equals(DataModel.LOCALHOST)) {
                /**
                 * This is a remote cacao. make sure the local cacao
                 * connection is available.
                 */
                jmxc = (JMXConnector)session.getAttribute(
                    JMX_CONNECTION_PREFIX + DataModel.LOCALHOST);
                if (jmxc == null) {
                    // no local connection, create local cacao connection first
                    createConnection(DataModel.LOCALHOST, session,
                                                    requestContext);
                }
            }

            jmxc = createConnection(connectionKey, session, requestContext);
	    mbsc = jmxc.getMBeanServerConnection();

	    // XXX: This is a temporary hack to avoid putting listeners
	    // on more than one connection.
	    if (connectionKey.equals(DataModel.LOCALHOST)) {

                nl = new MBeanNotificationListener(connectionKey);
                ObjectNameFactory onf = new ObjectNameFactory(
                    SysEventNotifierMBean.class.getPackage().getName());
                ObjectName name = onf.getObjectName(
                    SysEventNotifierMBean.class, null);

		mbsc.addNotificationListener(name, nl, null, session);
	    }
        } catch (OperationTimedOutException oprtnExc) {
            consoleLogger.warning("The connection timed out while " +
                "connecting to " + currentAgentLoc);
            remAgentLoc = currentAgentLoc;
            remContext = currentContext;
            oprtnExc.printStackTrace();
            throw oprtnExc;
        } catch (IOException ioExc) {
            consoleLogger.warning("There was an IOException connecting to " +
                currentAgentLoc);
            ioExc.printStackTrace();
            throw ioExc;
        } catch (SSOException ssoExc) {

            /**
             * If we are here then there was an exception while creating
             * the tokens. Essentially we were not able to establish a
             * connection to the remove node. This is an exception which
             * is of importance only to this class, hence suppressing it
             * here itself by throwing an IOException.
             */
            consoleLogger.warning("There was an exception creating tokens");
            ssoExc.printStackTrace();
            throw new IOException(ssoExc.getMessage());
        } catch (Exception e) {

            /**
             * If we are here we are not sure what caused the exception.
             * Throwing IOException when we are not sure about this.
             */
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
        return mbsc;
    }

    /**
     * Create a new jmx connection.
     *
     * @param  agentLocation  String which contains the location of the agent
     * (node1, node2, etc)
     * @param  session  HttpSession object where the connection object will be
     * stored so that we do not create more than one connection per user.
     * @param  requestContext  RequestContext object describing the current
     * request.
     *
     * @exception  OperationTimedOutException  in case this method was unable to
     * connect to the specified agent within a span of 5 seconds.
     * @exception  IOException  if there are exceptions in IO
     * @exception  SSOException  in case there is an exception related to tokens
     * @exception  any  other exception (eg; related to threads)
     */

    private synchronized JMXConnector createConnection(String agentLocation,
        HttpSession session, RequestContext requestContext) throws IOException,
        OperationTimedOutException, SSOException, Exception {

        JMXConnector jmxc;
        com.iplanet.sso.SSOTokenManager tm;
        com.iplanet.sso.SSOToken token;
        TimeoutController timeoutCntrl;

        tm = com.iplanet.sso.SSOTokenManager.getInstance();
        token = tm.createSSOToken(requestContext.getRequest());
        currentSub = token.getSubject();
        currentAgentLoc = agentLocation;
        currentContext = requestContext;

        /**
         * getJmxConnector takes a long time (5-10 minutes) in case the
         * specified agent is down or is not responding. We are attempting
         * to execute getJmxConnector for a maximum of 5 seconds. In case,
         * getJmxConnector does not return within five seconds we will get
         * an OperationTimedOutException. We are using the utility provided
         * by TimeoutController here to achieve this. getJmxConnector is called
         * from executeTask()
         */

        timeoutCntrl = new TimeoutController(this);
        timeoutCntrl.doControlledExecution();
        jmxc = currentJMXConn;
        currentSub = null;
        currentJMXConn = null;
        currentAgentLoc = null;
        jmxc.addConnectionNotificationListener(this, null,
            JMX_CONNECTION_PREFIX + agentLocation);

        if (jmxc != null) {
            session.setAttribute(JMX_CONNECTION_PREFIX + agentLocation, jmxc);
        }

        return jmxc;
    }

    /**
     * Close a connection to a given target
     *
     * @param  requestContext  required to obtain credentials for connection
     * @param  agentLocation  cluster or node name used to locate agent
     *
     * @exception  IOException  if a communication or authorization error occurs
     */
    public final void closeConnection(
        com.iplanet.jato.RequestContext requestContext, String agentLocation)
        throws IOException {
        HttpSession session = requestContext.getRequest().getSession(false);
	if (session != null) {
	    // Try and get a live connection from our session
	    String attrName = JMX_CONNECTION_PREFIX + agentLocation;
	    JMXConnector jmxc = (JMXConnector) session.getAttribute(attrName);

	    if (jmxc != null) {

		// Closing a connection can take time, do in a background thread
		JmxServerConnectionCloser.close(jmxc);
		session.removeAttribute(attrName);
	    }
	}
    }

    /**
     * Obtain a JMX server connection
     *
     * @param  subject  used to obtain the caller's credentials
     * @param  agentLocation  cluster or node name used to locate agent
     * @param  requestContext  RequestContext object describing the current
     * request.
     *
     * @return  a <code>MBeanServerConnection</code> value
     *
     * @exception  IOException  if a communication or authorization error occurs
     */
    private final void getJmxConnector(Subject subject, String agentLocation)
            throws IOException {

        String userName = null;
        String userPass = null;
        String roleName = null;
        String rolePass = null;
        JMXServiceURL serviceURL = null;
        JMXConnector jmxConnector = null;

        Object ups[] = subject.getPrincipals().toArray();
        Object pcs[] = subject.getPrivateCredentials().toArray();

        if ((ups.length < 1) || (pcs.length < 1)) {
            throw new IOException("No signon information available");
        }

        Object upobj = ups[0];

        if (upobj instanceof UserRolePrincipal) {

            // User + role
            UserRolePrincipal urp = (UserRolePrincipal) upobj;
            UserRoleCredential rc = (UserRoleCredential) pcs[0];

            if (urp == null) {
                throw new IOException("No signon UserRolePricipal " +
                    "defined");
            }

            if (rc == null) {
                throw new IOException("No signon UserRoleCredential " +
                    "defined");
            }

            if ((urp.getUserName() != null) && (rc.getUserPassword() != null)) {
                userName = urp.getUserName();
                userPass = rc.getUserPassword();
            }

            if ((urp.getRoleName() != null) && (rc.getRolePassword() != null)) {
                roleName = urp.getRoleName();
                rolePass = rc.getRolePassword();
            }
        } else {
            throw new IOException("Unexpected authorization type");
        }

        String base = JmxClient.BASE_MAP_KEY;
        HashMap envProps = new HashMap();

        // set parameters for local cluster connection(may not be local node)
        envProps.put(base + JmxClient.WELLKNOWN_KEY, "false");
        envProps.put(base + JmxClient.TRUSTSERVER_KEY, "true");
        envProps.put(base + JmxClient.USERNAME_KEY, userName);
        envProps.put(base + JmxClient.USERPASS_KEY, userPass);
        envProps.put(base + JmxClient.ROLENAME_KEY, roleName);
        envProps.put(base + JmxClient.ROLEPASS_KEY, rolePass);

        serviceURL = new JMXServiceURL("service:jmx:" + JmxClient.RMI_PROTOCOL +
                "://" + agentLocation);
        jmxConnector = JMXConnectorFactory.newJMXConnector(serviceURL,
                envProps);
        jmxConnector.connect();
        consoleLogger.info("Connected with RMI protocol successfully");
        currentJMXConn = jmxConnector;
    }

    /**
     * Obtain an instance of this class. This is the only way to obtain an
     * instance of this class.
     *
     * @return  JmxServerConnection. The only such object that exists at any
     * point in time.
     */
    public static JmxServerConnection getInstance() {

        // Just return the already initialized static variable
        return myself;
    }

    /**
     * Implement Notification interface - callback made when there's a
     * notification about one of our connections. remove the connection from our
     * cache if closed/failed
     *
     * @param  notification  a <code>Notification</code> value
     * @param  handback  an <code>Object</code> value
     */
    public void handleNotification(Notification notification, Object handback) {
        HttpSession session = RequestManager.getRequestContext().getRequest()
            .getSession(false);
	if (session == null) {
	    return;
	}

        if (notification instanceof JMXConnectionNotification) {
            String type = notification.getType();

            if ((type == JMXConnectionNotification.CLOSED) ||
                    (type == JMXConnectionNotification.FAILED)) {

                // Refresh all datamodels to avoid display of any stale data
                if (nl != null) {
                    nl.refreshAll(handback);
                }

                try {

                    // Closing a connection can take time, do in a
                    // background thread
                    JMXConnector jmxc = (JMXConnector) session.getAttribute(
                            (String) handback);

                    if (jmxc != null) {
                        JmxServerConnectionCloser.close(jmxc);
                    }

                    session.removeAttribute((String) handback);
                } catch (Exception e) {
                    consoleLogger.fine("cleanup caught exception " +
                        e.getMessage());
                }
            }

            if (type == JMXConnectionNotification.NOTIFS_LOST) {
                // Refresh all datamodels to avoid display of any stale data
                if (nl != null) {
                    nl.refreshAll(handback);
                }

                consoleLogger.fine("JMXConnection received " +
                    "notifs_lost notification");
            }
        }
    }

    /**
     * Implement BlockingTask's executeTask(). Essentially call
     * getJmxConnector() which takes time when the remote node is down.
     */
    public void executeTask() throws IOException {
        getJmxConnector(currentSub, currentAgentLoc);
    }

    /**
     * Implement BlockingTask's undoTask(). Close the jmxconnection in case
     * getJmxConnector was interrupted at the time of creation.
     */
    public void undoTask() throws IOException {
        closeConnection(currentContext, currentAgentLoc);
    }
}
