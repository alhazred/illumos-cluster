/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)MBeanNotificationListener.java	1.9	08/07/14 SMI"
 */

package com.sun.cluster.model;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.servlet.http.HttpSession;

import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.event.ClEventDefs;

public class MBeanNotificationListener
    implements NotificationListener {

    private String clusterName = null;

    public MBeanNotificationListener(String name) {
        clusterName = name;
    }

    public void handleNotification(Notification notification,
	Object handback) {
	HttpSession session = (HttpSession)handback;
	if (session == null) {
	    return;
	}
	DataModel dm = DataModel.getDataModelFromSession(session, clusterName);

	SysEventNotification clEvent = (SysEventNotification)notification;
        String subClass = clEvent.getSubclass();

	if (subClass.equals(ClEventDefs.ESC_CLUSTER_NODE_STATE_CHANGE) ||
		subClass.equals(ClEventDefs.ESC_ZC_STATE_CHANGE) ||
                subClass.equals(ClEventDefs.ESC_CLUSTER_NODE_CONFIG_CHANGE)) {
	   dm.getNodeModel().nodeDataChanged(clEvent);
           dm.getZoneClusterModel().zoneClusterDataChanged(clEvent);
           dm.getResourceGroupModel().resourceGroupDataChanged(clEvent);
           dm.getResourceModel().resourceDataChanged(clEvent);
	} else if (subClass.equals(ClEventDefs.ESC_CLUSTER_RG_CONFIG_CHANGE) ||
                    subClass.equals(ClEventDefs.ESC_CLUSTER_RG_STATE)) {
	   dm.getResourceGroupModel().resourceGroupDataChanged(clEvent);
           dm.getResourceModel().resourceDataChanged(clEvent);
	} else if (subClass.equals(ClEventDefs.ESC_CLUSTER_R_CONFIG_CHANGE) ||
                        subClass.equals(ClEventDefs.ESC_CLUSTER_R_STATE)) {
	   dm.getResourceModel().resourceDataChanged(clEvent);
	}

        // Add event to the session model so that EventMonitorDataViewBean
        // can read it to refresh SCM.
        // The event must only be added after refreshing all model classes to
        // avoid scm refresh before model data refresh.
        dm.getSessionModel().addEvent(clEvent);
    }

    public void refreshAll(Object handback) {

        HttpSession session = (HttpSession)handback;
	if (session == null) {
	    return;
	}
        DataModel dm = DataModel.getDataModelFromSession(session, clusterName);


        // Refresh all datamodels
        dm.getNodeModel().nodeDataChanged(null);
        dm.getZoneClusterModel().zoneClusterDataChanged(null);
        dm.getResourceGroupModel().resourceGroupDataChanged(null);
        dm.getResourceModel().resourceDataChanged(null);
        // Create an empty event with subclass as "*", seeing which applet would
        // refresh the page always.
        long l = 0;
        SysEventNotification clEvent = new SysEventNotification("", null, l,
                "", "*", ClEventDefs.ClEventSeverityEnum.CL_EVENT_SEV_CRITICAL,
                l, "", l, null);

        // Put this event in session model for scm to be refreshed
        dm.getSessionModel().addEvent(clEvent);
    }
}
