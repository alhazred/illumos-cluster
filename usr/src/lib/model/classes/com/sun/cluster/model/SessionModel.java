/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)SessionModel.java	1.6	08/07/14 SMI"
 */

package com.sun.cluster.model;

import com.sun.cluster.agent.event.SysEventNotification;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Session model for cluster
 */
public class SessionModel extends ModelBase {

    private static final int MAX_EVENTS = 1000;
    LinkedList events;
    int eventHistoryPeriod = 30000; // Default to 30 seconds

    public SessionModel() {
        events = new LinkedList();
    }


    public List getEventHistory() {
        List retList;
        synchronized (syncObject) {
            eventHistoryPurgeOldEvents();
            retList = (List)events.clone();
        }
        return retList;
    }

    public void addEvent(SysEventNotification notif) {
        synchronized (syncObject) {
            events.addLast(notif);
            if (events.size() > MAX_EVENTS) {
                events.removeFirst();
            }
        }
    }

    /**
     * Helper method, delete all events that are too old
     */
    private void eventHistoryPurgeOldEvents() {

        // Time before which all records should be deleted
        long time = new Date().getTime() - eventHistoryPeriod;

        try {

            while (true) {
                SysEventNotification notif = (SysEventNotification)
                        events.getFirst();

                if ((notif.getTimeStamp() - time) >= 0) {
                    break;
                }

                events.removeFirst();
            }
        } catch (NoSuchElementException e) {
            // Expected when no elements in list
        }
    }
}
