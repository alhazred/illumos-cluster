/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */


/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RACModel.java	1.17	09/04/06 SMI"
 */

package com.sun.cluster.model;

import javax.servlet.http.HttpSession;
import javax.management.remote.JMXConnector;

import com.iplanet.jato.RequestContext;

import com.sun.cluster.common.MBeanModel;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.common.TrustedMBeanModel;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.utils.DataServicesUtil;
import com.sun.cluster.agent.dataservices.utils.Util;
import com.sun.cluster.agent.dataservices.utils.ClusterDiskGroupInfo;
import com.sun.cluster.agent.dataservices.utils.DiskGroupInfo;
import com.sun.cluster.agent.dataservices.oraclerac.OracleRACMBean;
import com.sun.cluster.agent.rgm.ResourceData;
import com.sun.cluster.agent.rgm.ResourceGroupData;
import com.sun.cluster.agent.rgm.ResourceTypeData;
import com.sun.cluster.agent.rgm.property.ResourceProperty;
import com.sun.cluster.agent.rgm.property.ResourcePropertyStringArray;

import java.io.IOException;
import java.io.File;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * RACModel - RAC model class.
 *
 */
public class RACModel extends ModelBase {

    static private final String RACMODEL = "racModel";
    private static String DG_RG_PREFIX = "dg";
    private static String FS_RG_PREFIX = "mnt";
    private static String MDS_RG_PREFIX = "mds";
    private static String QFS = "qfs";
    private static String WAIT_FOR_ZC_BOOT = "waitzcboot-";
    private static final String JMX_CONNECTION_PREFIX = "JMX_CONNECTION : ";

    // This syncObject has to be static because it is used in a static context.
    // It overrides (hides) the one in ModelBase.
    static private Object syncObject = new Object();

    static private RACModel racModel = null;
    private OracleRACMBean racMBean = null;
    static private HashMap connectionMap = new HashMap();

    static public RACModel getRACModel(RequestContext rq) {
	if (rq == null) {
	    if (racModel == null) {
		racModel = new RACModel();
	    }

	    return racModel;
	}
	return getRACModelFromSession(rq.getRequest().getSession());
    }

    static public RACModel getRACModelFromSession(HttpSession session) {
	RACModel racModel = (RACModel)session.getAttribute(RACMODEL);

	if (racModel == null) {
	    synchronized (syncObject) {
		if (racModel == null) {	// must check again under synchronized
		    racModel = new RACModel();
		    session.setAttribute(RACMODEL, racModel);
		}
	    }
	}

	return racModel;
    }

    // private Constructor to ensure singleton object
    private RACModel() {

    }

    private JMXConnector getConnection(String agentLocation) {
        String split_str[] = agentLocation.split(Util.COLON);
        agentLocation = split_str[0];

        JMXConnector connector = (JMXConnector) connectionMap.get(
                JMX_CONNECTION_PREFIX + agentLocation);

        if (connector == null) {

            try {
                connector = TrustedMBeanModel.getWellKnownConnector(
                        agentLocation);
            } catch (Exception ex) {
                System.out.println("Failed to get connection to node " +
                    agentLocation);
                System.exit(1);
            }

            setConnection(agentLocation, connector);
        }

        return connector;
    }

    private void setConnection(String agentLocation, JMXConnector connector) {
        connectionMap.put(JMX_CONNECTION_PREFIX + agentLocation,
            connector);
    }

    public void closeConnections(String nodeList[]) {
        JMXConnector localConnector = (JMXConnector) connectionMap.get(
                JMX_CONNECTION_PREFIX + Util.getClusterEndpoint());

        if (localConnector != null) {

            // If nodelist is null, close the local connection alone
            if (nodeList != null) {

                for (int i = 0; i < nodeList.length; i++) {
                    String nodeName = nodeList[i];

                    // extract the nodename incase this is a logical nodename
                    String split_str[] = nodeName.split(Util.COLON);
                    String nodeEndPoint = Util.getNodeEndpoint(localConnector,
                            split_str[0]);
                    JMXConnector connector = (JMXConnector) connectionMap
                        .get(JMX_CONNECTION_PREFIX +
                            nodeEndPoint);

                    if (connector != null) {

                        try {
                            connector.close();
                            connectionMap.put(
                                JMX_CONNECTION_PREFIX + nodeEndPoint,
                                null);
                        } catch (IOException ioe) {
                        }
                    }
                }
            }

            try {
                localConnector.close();
                connectionMap.put(JMX_CONNECTION_PREFIX +
                    Util.getClusterEndpoint(), null);
            } catch (IOException ioe) {
            }
        }
    }

    private OracleRACMBean getMBean(RequestContext rq, String nodeToConnect)
	throws IOException {
	if (racMBean == null) {
	    racMBean = getOracleRACMBean(rq, nodeToConnect);
	}
	return racMBean;
    }

    private OracleRACMBean getOracleRACMBean(RequestContext rq,
	    String nodeToConnect)
	throws IOException {
	OracleRACMBean mbean = null;

	if (nodeToConnect == null) {
	    nodeToConnect = Util.getClusterEndpoint();
	}

	if (rq != null) {
	    mbean = (OracleRACMBean) MBeanModel.getMBeanProxy(
			rq,
			Util.DOMAIN,
			nodeToConnect,
			OracleRACMBean.class,
			null,
			false);
	} else {
	    JMXConnector connector = getConnection(nodeToConnect);
	    mbean = (OracleRACMBean) TrustedMBeanModel.getMBeanProxy(
			Util.DOMAIN,
			connector,
			OracleRACMBean.class,
			null,
			false);
	}

	return mbean;
    }

    public List generateCommands(RequestContext rq, String nodeToConnect,
	Map configurationInformation) throws CommandExecutionException {
	try {
	    return getMBean(rq, nodeToConnect).generateCommands(
					    configurationInformation);
	} catch (IOException e) {
	    return null;
	}
    }

    private HashMap fetchRACResourceGroupNodes(RequestContext rq,
	String nodeToConnect, Object helperData) {

	HashMap retMap = new HashMap();
	String rgNodeNames[] = null;
	String racFrRgName = null;
	String racFrRsName = null;
	String racSVMRes = null;
	String racCVMRes = null;
	String udlmRes = null;
	try {
	    DataModel dm = DataModel.getDataModel(rq);
	    ResourceModel rm = dm.getResourceModel();

	    if (rq == null) {
		rm.setCacheEnabled(false);
	    }

	    String zoneClusterName = null;
	    if (helperData != null && helperData instanceof HashMap) {
		zoneClusterName = (String) ((HashMap) helperData).get(
							Util.SEL_ZONE_CLUSTER);
	    }

	    // Search for a resource of type Util.RAC_FRAMEWORK_RTNAME in the
	    // given cluster
	    ResourceData rsData = rm.getResourceBasedOnType(
		rq, zoneClusterName, Util.RAC_FRAMEWORK_RTNAME);

	    if (rsData != null) {
		racFrRsName = rsData.getName();
		racFrRgName = rsData.getResourceGroupName();

		// get RAC RG nodes
		ResourceGroupModel rgm = dm.getResourceGroupModel();
		if (rq == null) {
		    rgm.setCacheEnabled(false);
		}

		ResourceGroupData rgData = rgm.getResourceGroupData(
		    rq, zoneClusterName, racFrRgName);

		rgNodeNames = rgData.getNodeNames();
	    }

	    // Search for a resource of type SUNW.rac_svm in the given cluster
	    rsData = rm.getResourceBasedOnType(rq, zoneClusterName,
							"SUNW.rac_svm");
	    if (rsData != null) {
		racSVMRes = rsData.getName();
	    }

	    // Search for a resource of type SUNW.rac_cvm
	    rsData = rm.getResourceBasedOnType(rq, zoneClusterName,
							"SUNW.rac_cvm");
	    if (rsData != null) {
		racCVMRes = rsData.getName();
	    }

	    // Search for a resource of type SUNW.rac_udlm
	    rsData = rm.getResourceBasedOnType(rq, zoneClusterName,
							"SUNW.rac_udlm");
	    if (rsData != null) {
		udlmRes = rsData.getName();
	    }
	} catch (Exception ioe) {
	    // Do not do anything here and handle return value of null in
	    // the
	    // wizard
	    // ioe.printStackTrace();
	    System.out.println(ioe.getMessage());
	}

	retMap.put(Util.RAC_RG_NODES, rgNodeNames);
	retMap.put(Util.RAC_FRAMEWORK_RSNAME, racFrRsName);
	retMap.put(Util.RAC_FRAMEWORK_RGNAME, racFrRgName);
	retMap.put(Util.ORF_UDLM_RID, udlmRes);
	retMap.put(Util.ORF_SVM_RID, racSVMRes);
	retMap.put(Util.ORF_VXVM_RID, racCVMRes);

	return retMap;
    }

    private HashMap generateRGName(RequestContext rq, String nodeToConnect,
	Object helperData) {

	HashMap retMap = new HashMap();
	DataModel dm = DataModel.getDataModel(rq);
	ResourceGroupModel rgm = dm.getResourceGroupModel();

	if (rq == null) {
	    rgm.setCacheEnabled(false);
	}

	String rgPrefix = (String) ((HashMap)helperData).get(
						    Util.RG_PREFIX_KEY);
	String rgName = rgPrefix + Util.RG_POSTFIX;
	Object obj = ((HashMap)helperData).get(Util.SEL_ZONE_CLUSTER);
	String zoneClusterName = null;
	if (obj != null && obj instanceof String) {
	    zoneClusterName = (String)obj;
	}

	ResourceGroupData rgData = null;
	try {
	    rgData = rgm.getResourceGroupData(rq, zoneClusterName, rgName);

	    int count = 1;
	    while (rgData != null) {
		// Check if the RG Name already exists
		rgName = new String(rgPrefix + count + Util.RG_POSTFIX);
		rgData = rgm.getResourceGroupData(rq, zoneClusterName, rgName);
		count++;
	    }
	    retMap.put(Util.RG_NAME, rgName);
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	return retMap;
    }

    private HashMap generateRGNameWithFilter(RequestContext rq,
	String nodeToConnect, Object helperData) {

	HashMap retMap = new HashMap();

	DataModel dm = DataModel.getDataModel(rq);
	ResourceGroupModel rgm = dm.getResourceGroupModel();

	if (rq == null) {
	    rgm.setCacheEnabled(false);
	}

	HashMap passedData = (HashMap) helperData;
	String zstrPrefix = (String) passedData.get(Util.PREFIX);
	String rgName = zstrPrefix + Util.RG_POSTFIX;
	ArrayList resExceptions = (ArrayList) passedData.get(Util.EXEMPT);
	Object obj = passedData.get(Util.SEL_ZONE_CLUSTER);
	String zoneClusterName = null;
	if (obj != null && obj instanceof String) {
	    zoneClusterName = (String)obj;
	}

	ResourceGroupData rgData = null;

	try {
	    rgData = rgm.getResourceGroupData(rq, zoneClusterName, rgName);
	    int count = 1;
	    while (rgData != null || resExceptions.contains(rgName)) {
		rgName = new String(zstrPrefix + count + Util.RG_POSTFIX);
		rgData = rgm.getResourceGroupData(rq, zoneClusterName, rgName);
		count++;
	    }

	    retMap.put(Util.RG_NAME_W_EXCEPTIONS, rgName);
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	return retMap;
    }

    private HashMap generateRSName(RequestContext rq, String nodeToConnect,
	Object helperData) {

	HashMap retMap = new HashMap();

	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();

	if (rq == null) {
	    rm.setCacheEnabled(false);
	}

	HashMap passedData = (HashMap) helperData;

	String rsPrefix = (String) passedData.get(Util.RS_PREFIX_KEY);
	String rName = rsPrefix + Util.RS_POSTFIX;
	Object obj = passedData.get(Util.SEL_ZONE_CLUSTER);
	String zoneClusterName = null;
	if (obj != null && obj instanceof String) {
	    zoneClusterName = (String)obj;
	}

	// Check if the resName already exists
	ResourceData rData = null;
	try {
	    rData = rm.getResourceData(rq,  zoneClusterName, null, rName);
	    int count = 1;
	    while (rData != null) {
		rName = rsPrefix + count + Util.RS_POSTFIX;
		rData = rm.getResourceData(rq,  zoneClusterName, null, rName);
		count++;
	    }

	    retMap.put(Util.RS_NAME, rName);
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	return retMap;
    }

    private HashMap generateRSNameWithFilter(RequestContext rq,
	String nodeToConnect, Object helperData) {
	HashMap retMap = new HashMap();

	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();

	if (rq == null) {
	    rm.setCacheEnabled(false);
	}

	HashMap passedData = (HashMap) helperData;
	String zstrPrefix = (String) passedData.get(Util.PREFIX);
	String rName = zstrPrefix + Util.RS_POSTFIX;
	ArrayList resExceptions = (ArrayList) passedData.get(Util.EXEMPT);
	Object obj = passedData.get(Util.SEL_ZONE_CLUSTER);
	String zoneClusterName = null;
	if (obj != null && obj instanceof String) {
	    zoneClusterName = (String)obj;
	}

	// Check if the resName already exists
	ResourceData rData = null;
	try {
	    rData = rm.getResourceData(rq,  zoneClusterName, null, rName);
	    int count = 1;
	    while (rData != null || resExceptions.contains(rName)) {
		rName = zstrPrefix + count + Util.RS_POSTFIX;
		rData = rm.getResourceData(rq,  zoneClusterName, null, rName);
		count++;
	    }

	    retMap.put(Util.RS_NAME_W_EXCEPTIONS, rName);
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	return retMap;
    }

    private HashMap generateNameForGDDResource(RequestContext rq,
	String nodeToConnect, Object helperData) {

	HashMap retMap = new HashMap();

	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();

	if (rq == null) {
	    rm.setCacheEnabled(false);
	}
	// Attribute could be either a mountpoint or a diskset
	HashMap passedData = (HashMap) helperData;
	String attribute = (String) passedData.get(Util.PREFIX);
	Object obj = passedData.get(Util.SEL_ZONE_CLUSTER);
	String zoneClusterName = null;
	if (obj != null && obj instanceof String) {
	    zoneClusterName = (String)obj;
	}
	// String attribute = (String) helperData;
	attribute = attribute.replace('/', '-');

	String rName;

	// Generated Name scal-<attr>-rs-<count>
	rName = Util.SCAL + attribute + Util.RS_POSTFIX;

	// Check if the name already exists
	ResourceData rData = null;
	try {
	    rData = rm.getResourceData(rq, zoneClusterName, null, rName);
	    int count = 1;
	    while (rData != null) {
		rName = new String(
			Util.SCAL + attribute + count + Util.RS_POSTFIX);
		rData = rm.getResourceData(rq, zoneClusterName, null, rName);
		count++;
	    }

	    retMap.put(Util.STORAGERESOURCE, rName);
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	return retMap;
    }

    private HashMap generateNameForGDDResourceGroup(RequestContext rq,
	String nodeToConnect, Object helperData) {

	HashMap retMap = new HashMap();

	HashMap passedData = (HashMap) helperData;
	String resourceName = (String) passedData.get(Util.RS_NAME);
	Object obj = passedData.get(Util.SEL_ZONE_CLUSTER);
	String zoneClusterName = null;
	if (obj != null && obj instanceof String) {
	    zoneClusterName = (String)obj;
	}

	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();

	if (rq == null) {
	    rm.setCacheEnabled(false);
	}

	ResourceData rData = null;
	try {
	    rData = rm.getResourceData(rq, zoneClusterName, null, resourceName);

	    if (rData != null) {
		retMap.put(Util.STORAGEGROUP, rData.getResourceGroupName());
	    } else {
		// New Resource
		// Generate Unique Name
		String rgName = null;
		rgName = resourceName.substring(0,
		    resourceName.indexOf(Util.RS_POSTFIX));

		// QFS Resource command generation creates
		// name that starts with QFS. All QFS resource
		// that gets created are now put in the same RG and
		// hence making the RG name generation independent
		// of the specific mount point
		if (rgName.startsWith(QFS)) {
		    rgName = QFS + MDS_RG_PREFIX;
		} else {

		    // This code assumes that the dg does not have
		    // have a dash in it. This will be true in most of
		    // the cases. The user can always change the
		    // name that is generated.
		    // This assumption is required to differentiate
		    // between the scal dg resource group and the
		    // scal mount point resource group. This assumption
		    // helps makes minimum changes to the code for
		    // consolidating all scal dg resource in one RG and
		    // similar changes for scal mount point resource.
		    // This alogrithm must be improved when there are
		    // cycles for more qualification.
		    if ((rgName.split(Util.DASH)).length < 3) {
			rgName = Util.SCAL + DG_RG_PREFIX;
		    } else {
			rgName = Util.SCAL + FS_RG_PREFIX;
		    }
		}

		ResourceGroupModel rgm = dm.getResourceGroupModel();

		if (rq == null) {
		    rgm.setCacheEnabled(false);
		}

		String newRGName = new String(rgName + Util.RG_POSTFIX);

		ResourceGroupData rgData = rgm.getResourceGroupData(
		    rq, zoneClusterName, newRGName);

		int count = 1;

		while (rgData != null) {
		    newRGName = new String(rgName + "-" + count +
						    Util.RG_POSTFIX);
		    rgData = rgm.getResourceGroupData(
			rq, zoneClusterName, newRGName);
		    count++;
		}

		retMap.put(Util.STORAGEGROUP, newRGName);
	    }
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}
	return retMap;
    }

    private HashMap getQFSInfo(RequestContext rq, String nodeToConnect,
	Object helperData) {

	HashMap retMap = new HashMap();
	Map infoMap = new HashMap();
	HashMap map = null;
	String[] fsInfo = null;
	String qfsFSName = null;
	String qfsMountpoint = null;
	String absZoneRootPath = null;

	if (helperData != null && helperData instanceof HashMap) {
	    map = (HashMap) helperData;
	    fsInfo = (String []) map.get(Util.QFS_INFO);
	    qfsFSName = fsInfo[0];
	    qfsMountpoint = fsInfo[1];

	    String zoneClusterName = (String) map.get(Util.SEL_ZONE_CLUSTER);
	    String zoneClusterPath = null;
	    StringBuffer zoneRootPath = null;

	    if (zoneClusterName != null &&
		zoneClusterName.trim().length() > 0) {

		ArrayList zoneClusterPaths = (ArrayList)
					    map.get(Util.ZONE_PATH);
		    if (zoneClusterPaths != null &&
			zoneClusterPaths.size() == 1) {
		    zoneClusterPath = (String) zoneClusterPaths.get(0);
		}

		if (zoneClusterPath != null) {
		    zoneRootPath =  new StringBuffer(zoneClusterPath);
		    zoneRootPath.append(Util.ROOT_PATH);

		    File file = new File(zoneRootPath.toString());
		    absZoneRootPath = file.getAbsolutePath();
		}
	    }
	}
	try {
	    retMap.put(Util.METASERVER_RESOURCE,
		getMetaserverResource(rq, qfsMountpoint, absZoneRootPath));

	    retMap.putAll(
		getMBean(rq, nodeToConnect).discoverPossibilities(
		    new String[] { Util.METASERVER_NODES }, qfsFSName));

	    retMap.putAll(
		getMBean(rq, nodeToConnect).discoverPossibilities(
		new String[] { Util.OBAN_UNDER_QFS}, qfsFSName));
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	return retMap;
    }

    private HashMap getVxVMDeviceGroupInfo(String[] propertyNames,
	RequestContext rq, String nodeToConnect, Object helperData) {

	HashMap retMap = new HashMap();
	String racNodes[] = (String[]) helperData;
	StringBuffer racNodeList = new StringBuffer();
	Map scalVxVMDisksets = null;
	Map otherVxVMDisksets = null;

	for (int j = 0; j < racNodes.length; j++) {
	    racNodeList.append(racNodes[j]);

	    if (j < (racNodes.length - 1)) {
		racNodeList.append(Util.COMMA);
	    }
	}

	HashMap cvmDGMap = null;
	try {
	    cvmDGMap = (HashMap) getMBean(rq, nodeToConnect).
		discoverPossibilities(propertyNames, null);

	    ArrayList cvmDGs = (ArrayList) cvmDGMap.get(Util.VxVMDEVICEGROUP);

	    DataModel dm = DataModel.getDataModel(rq);
	    ResourceModel rm = dm.getResourceModel();
	    if (rq == null) {
		rm.setCacheEnabled(false);
	    }

	    for (Iterator i = cvmDGs.iterator(); i.hasNext();) {
		String deviceGroupName = (String) i.next();

		// Check whether this deviceGroup is under any resource of
		// type SUNW.Scal_DeviceGroup
		List dgResources = rm.getResourceList(rq, null,
		    null, "SUNW.ScalDeviceGroup", Util.DISKGROUP_NAME,
						    deviceGroupName);

		if (dgResources == null) {
		    if (otherVxVMDisksets == null) {
			otherVxVMDisksets = new HashMap();
		    }

		    otherVxVMDisksets.put(deviceGroupName,
			racNodeList.toString());
		} else {

		    // Add the DiskGroup in the "RGM Monitored" Map
		    // with Key as Resource Name
		    if (scalVxVMDisksets == null) {
			scalVxVMDisksets = new HashMap();
		    }

		    scalVxVMDisksets.put((String) dgResources.get(0),
			deviceGroupName);
		}
	    }

	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	retMap.put(Util.RGM_VxVMDEVICEGROUP, scalVxVMDisksets);
	retMap.put(Util.NONRGM_VxVMDEVICEGROUP, otherVxVMDisksets);
	return retMap;
    }

    private HashMap getScalableDeviceGroupRSList(RequestContext rq,
	String nodeToConnect, Object helperData) {

	HashMap retMap = new HashMap();
	String nodeList[] = (String[]) ((HashMap) helperData).get(
	    Util.RG_NODELIST);

	String zoneClusterName = (String) ((HashMap) helperData).get(
	    Util.SEL_ZONE_CLUSTER);

	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();
	ResourceGroupModel rgm = dm.getResourceGroupModel();

	if (rq == null) {
	    rm.setCacheEnabled(false);
	    rgm.setCacheEnabled(false);
	}

	List scalDevGroupRS = null;

	// get a list of SUNW.ScalDeviceGroup resources in the given cluster
	// with nodelist set to nodeList
	try {
	    List rgNames = rgm.getResourceGroupNames(rq, zoneClusterName,
						    nodeList);

	    // Output modified to return list of resourceData objects
	    scalDevGroupRS = rm.getResourceList(
		rq, zoneClusterName, rgNames, Util.SCAL_DEVGROUP_RTNAME, null,
									null);

	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	retMap.put(Util.SCAL_DEVGROUP_RSLIST, scalDevGroupRS);
	return retMap;
    }

    private HashMap getScalableMountPointRSList(RequestContext rq,
	String nodeToConnect, Object helperData) {

	HashMap retMap = new HashMap();

	String nodeList[] = (String[]) ((HashMap) helperData).get(
	    Util.RG_NODELIST);

	String zoneClusterName = (String) ((HashMap) helperData).get(
	    Util.SEL_ZONE_CLUSTER);

	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();
	ResourceGroupModel rgm = dm.getResourceGroupModel();

	if (rq == null) {
	    rm.setCacheEnabled(false);
	    rgm.setCacheEnabled(false);
	}
	List scalMountPointRS = null;

	// get a list of SUNW.ScalMountPoint resources in the given cluster
	// with nodelist set to nodeList
	try {
	    List rgNames = rgm.getResourceGroupNames(rq, zoneClusterName,
								    nodeList);

	    // get a list of SUNW.ScalMountPoint resources
	    // Output modified to return list of resourceData objects
	    scalMountPointRS = rm.getResourceList(
		rq, zoneClusterName, rgNames, Util.SCAL_MOUNTPOINT_RTNAME,
		null, null);

	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	retMap.put(Util.SCAL_MOUNTPOINT_RSLIST, scalMountPointRS);
	return retMap;
    }

    private HashMap getCRSFrameworkInfo(RequestContext rq,
	String nodeToConnect, Object helperData) {

	HashMap retMap = new HashMap();

	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();

	if (rq == null) {
	    rm.setCacheEnabled(false);
	}

	String zoneClusterName = null;

	if (helperData != null && helperData instanceof HashMap) {
	    zoneClusterName = (String) ((HashMap) helperData).get(
		Util.SEL_ZONE_CLUSTER);
	}

	List rsList = null;
	try {
	    rsList = rm.getResourceList(
			    rq, zoneClusterName, null,
			    Util.CRS_FRAMEWORK_RTNAME, null, null);
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	if ((rsList != null) && (rsList.size() > 0)) {
	    Iterator i = rsList.iterator();

	    if (i.hasNext()) {
		ResourceData rsData = (ResourceData) i.next();
		retMap.put(Util.CRS_FRAMEWORK_PRESENT, new Boolean(true));
		retMap.put(Util.CRS_FRAMEWORK_RSNAME, rsData.getName());
	    }
	} else {
	    retMap.put(Util.CRS_FRAMEWORK_PRESENT, new Boolean(false));
	    retMap.put(Util.CRS_FRAMEWORK_RSNAME, null);
	}

	return retMap;
    }

    private HashMap getCRSFrameworkRSName(RequestContext rq,
	String nodeToConnect, Object helperData) {

	HashMap retMap = new HashMap();

	String zoneClusterName = null;
	if (helperData != null && helperData instanceof HashMap) {
	    zoneClusterName = (String) ((HashMap) helperData).get(
		Util.SEL_ZONE_CLUSTER);
	}
	// generate resource name for CRS Framework resource
	String rtName = Util.CRS_FRAMEWORK_RTNAME;
	int index = rtName.indexOf(Util.DOT);
	StringBuffer rs_prefix = new StringBuffer(rtName.substring(
		index + 1));
	StringBuffer rsname;
	String rsnamestr = null;
	int count = 0;
	ResourceData rData = null;
	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();
	if (rq == null) {
	    rm.setCacheEnabled(false);
	}
	try {

	    do {
		rsname = rs_prefix;

		if (count > 0) {
		    rsname.append(Util.UNDERSCORE);
		    rsname.append(count);
		}

		rsname.append(Util.RS_POSTFIX);
		count++;

		rsnamestr = rsname.toString();

		// Check if the resName already exists
		rData = rm.getResourceData(
			rq, zoneClusterName, null, rsnamestr);

	    } while (rData != null);

	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}
	retMap.put(Util.CRS_FRAMEWORK_RSNAME, rsnamestr);
	return retMap;
    }

    private HashMap getRACProxyRSName(RequestContext rq, String nodeToConnect,
	Object helperData) {

	HashMap retMap = new HashMap();
	String zoneClusterName = null;
	if (helperData != null && helperData instanceof HashMap) {
	    zoneClusterName = (String) ((HashMap) helperData).get(
		Util.SEL_ZONE_CLUSTER);
	}
	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();
	if (rq == null) {
	    rm.setCacheEnabled(false);
	}
	String rtName = Util.RAC_SERVER_PROXY_RTNAME;
	ResourceData rData = null;

	// find index of first UNDERSCORE
	int index = rtName.indexOf(Util.UNDERSCORE);
	StringBuffer rs_prefix = new StringBuffer(rtName.substring(
	    index + 1));
	StringBuffer rsname;
	String rsnamestr = null;
	int count = 0;

	try {
	    do {
		rsname = rs_prefix;

		if (count > 0) {
		    rsname.append(Util.UNDERSCORE);
		    rsname.append(count);
		}

		rsname.append(Util.RS_POSTFIX);
		count++;
		rsnamestr = rsname.toString();
		rData = rm.getResourceData(
			rq, zoneClusterName, null, rsnamestr);
	    } while (rData != null);

	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}
	retMap.put(Util.RAC_PROXY_RSNAME, rsnamestr);
	return retMap;
    }

    private HashMap getRACProxyRGName(RequestContext rq, String nodeToConnect,
	Object helperData) {

	HashMap retMap = new HashMap();
	String zoneClusterName = null;
	if (helperData != null && helperData instanceof HashMap) {
	    zoneClusterName = (String) ((HashMap) helperData).get(
		Util.SEL_ZONE_CLUSTER);
	}
	DataModel dm = DataModel.getDataModel(rq);
	ResourceGroupModel rgm = dm.getResourceGroupModel();
	if (rq == null) {
	    rgm.setCacheEnabled(false);
	}

	String rtName = Util.RAC_SERVER_PROXY_RTNAME;

	// find index of first UNDERSCORE
	int index = rtName.indexOf(Util.UNDERSCORE);
	StringBuffer rg_prefix = new StringBuffer(rtName.substring(
	    index + 1));
	StringBuffer rgname;
	String rgnamestr = null;
	int count = 0;
	ResourceGroupData rgData = null;

	try {
	    do {
		rgname = rg_prefix;

		if (count > 0) {
		    rgname.append(Util.UNDERSCORE);
		    rgname.append(count);
		}

		rgname.append(Util.RG_POSTFIX);
		count++;
		rgnamestr = rgname.toString();
		rgData = rgm.getResourceGroupData(
			    rq, zoneClusterName, rgnamestr);
	    } while (rgData != null);
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	retMap.put(Util.RAC_PROXY_RGNAME, rgnamestr);
	return retMap;
    }

    private HashMap getHASPRSList(RequestContext rq, String nodeToConnect,
	Object helperData) {

	HashMap retMap = new HashMap();
	String zoneClusterName = null;
	String[] nodeList = null;
	if (helperData != null && helperData instanceof HashMap) {
	    zoneClusterName = (String) ((HashMap) helperData).get(
		Util.SEL_ZONE_CLUSTER);

	    nodeList = (String[]) ((HashMap) helperData).get(
		Util.RG_NODELIST);
	}
	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();
	ResourceGroupModel rgm = dm.getResourceGroupModel();
	if (rq == null) {
	    rm.setCacheEnabled(false);
	    rgm.setCacheEnabled(false);
	}

	// get a list of HAStoragePlus resources
	// with nodelist set to nodeList
	List haspRSList = null;
	try {
	    List rgNames = rgm.getResourceGroupNames(
				rq, zoneClusterName, nodeList);

	    // Output modified to return list of resourceData objects
	    haspRSList = rm.getResourceList(
		rq, zoneClusterName, rgNames,
		Util.HASP_RTNAME, null, null);

	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}
	retMap.put(Util.HASP_RSLIST, haspRSList);
	return retMap;
    }

    private HashMap getLHRSList(RequestContext rq, String nodeToConnect,
	Object helperData) {

	HashMap retMap = new HashMap();
	String zoneClusterName = null;
	String[] nodeList = null;
	if (helperData != null && helperData instanceof HashMap) {
	    zoneClusterName = (String) ((HashMap) helperData).get(
		Util.SEL_ZONE_CLUSTER);

	    nodeList = (String[]) ((HashMap) helperData).get(
		Util.RG_NODELIST);
	}
	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();
	ResourceGroupModel rgm = dm.getResourceGroupModel();
	if (rq == null) {
	    rm.setCacheEnabled(false);
	    rgm.setCacheEnabled(false);
	}

	// get a list of Logical Host resources hosted on the specific set of
	// nodes or LH resources in a specified RG.
	List resources = null;
	try {

	    if (nodeList == null) {
		String rgName = (String) ((HashMap) helperData).get(
		    Util.RG_NAME);

		List rgList = new ArrayList();
		rgList.add(rgName);

		resources = rm.getResourceList(
		    rq, zoneClusterName, rgList, Util.LH_RTNAME, null, null);
	    } else {
		// get a list of LH resources with nodelist set to nodeList
		List rgNames = rgm.getResourceGroupNames(
				    rq, zoneClusterName, nodeList);

		// Output modified to return list of resourceData objects
		resources = rm.getResourceList(
		    rq, zoneClusterName, rgNames, Util.LH_RTNAME, null, null);
	    }
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	retMap.put(Util.LH_RSLIST, resources);

	return retMap;
    }

    private HashMap getRACProxyRTName(RequestContext rq, String nodeToConnect,
	Object helperData) {

	HashMap retMap = new HashMap();

	String propName = Util.RAC_SERVER_PROXY_RTNAME;
	ResourceProperty property = null;

	String zoneClusterName = null;
	if (helperData != null && helperData instanceof HashMap) {
	    zoneClusterName = (String) ((HashMap) helperData).get(
		Util.SEL_ZONE_CLUSTER);
	}
	DataModel dm = DataModel.getDataModel(rq);
	ResourceTypeModel rtm = dm.getResourceTypeModel();
	if (rq == null) {
	    rtm.setCacheEnabled(false);
	}
	ResourceTypeData rtData =  null;
	try {
	    rtData = rtm.getRTData(rq, propName, zoneClusterName);
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	if (rtData != null) {
	    String name = rtData.getName();
	    List extProps = rtData.getExtProperties();
	    List sysProps = rtData.getSystemProperties();
	    boolean found = false;
	    ResourceProperty rp = null;

	    Iterator sysIter = null;
	    Iterator extIter = null;

	    if (extProps != null) {
		extIter = extProps.iterator();
	    }
	    if (sysProps != null) {
		sysIter = sysProps.iterator();
	    }
	    // Search for our property in extension Properties
	    while (extIter.hasNext() && !found) {
		rp = (ResourceProperty) extIter.next();
		if (rp.getName().equals(name)) {
		    found = true;
		}
	    }

	    if (!found) {
		// Search for our property in System Properties
		while (sysIter.hasNext() && !found) {
		    rp = (ResourceProperty) sysIter.next();
		    if (rp.getName().equals(name)) {
			found = true;
		    }
		}
	    }

	    if (found) {
		// Omit the Discovered Properties
		if (!retMap.containsKey(name)) {
		    retMap.put(name, rp);
		}
	    }
	}

	return retMap;
    }

    public HashMap discoverPossibilities(RequestContext rq,
	String nodeToConnect, String propertyNames[], Object helperData) {
	HashMap resultMap = new HashMap();
	try {
	    List propList = Arrays.asList(propertyNames);

	    boolean bPropFound = false;

	    if (propList.contains(Util.RAC_RG_NODES)) {
		bPropFound = true;
		resultMap.putAll(fetchRACResourceGroupNodes(rq,
		    nodeToConnect, helperData));

	    }
	    if (propList.contains(Util.RG_NAME)) {
		bPropFound = true;
		resultMap.putAll(generateRGName(rq, nodeToConnect, helperData));

	    }
	    if (propList.contains(Util.RG_NAME_W_EXCEPTIONS)) {
		bPropFound = true;
		resultMap.putAll(generateRGNameWithFilter(
		    rq, nodeToConnect, helperData));

	    }
	    if (propList.contains(Util.RS_NAME)) {
		bPropFound = true;
		resultMap.putAll(generateRSName(rq, nodeToConnect, helperData));

	    }
	    if (propList.contains(Util.RS_NAME_W_EXCEPTIONS)) {
		bPropFound = true;
		resultMap.putAll(generateRSNameWithFilter(
		    rq, nodeToConnect, helperData));

	    }
	    if (propList.contains(Util.STORAGERESOURCE)) {
		bPropFound = true;
		/*
		 * Generate an unique name for a Scalable GDD Resource based on
		 * the Mountpoint or diskGroupName
		 */
		resultMap.putAll(generateNameForGDDResource(rq,
		    nodeToConnect, helperData));

	    }
	    if (propList.contains(Util.STORAGEGROUP)) {
		bPropFound = true;
		/*
		 * Discover the ResourceGroup for a Scalable GDD resource
		 * If the resource is new a new RG name is constructed using the
		 * resource name
		 */

		resultMap.putAll(generateNameForGDDResourceGroup(rq,
		    nodeToConnect, helperData));

	    }
	    if (propList.contains(Util.METASERVER_RESOURCE)) {
		bPropFound = true;
		/**
		 * Discover the Metaserver Resource for a QFS Mountpoint
		 * The resultMap would have the Metaserver Resource if any or
		 * NULL
		 */
		if (helperData != null && helperData instanceof HashMap) {
		    HashMap passedData = (HashMap) helperData;
		    String qfsMountpoint = (String)
					passedData.get(Util.QFS_INFO);
		    String zoneRootPath = (String)
					passedData.get(Util.ZONE_PATH);
		    resultMap.put(Util.METASERVER_RESOURCE,
			getMetaserverResource(rq, qfsMountpoint, zoneRootPath));
		}
	    }
	    if (propList.contains(Util.QFS_INFO)) {
		bPropFound = true;
		/**
		 * Discover Metaserver, Underlying volume manager (if any) for a
		 * S-QFS Filesystem and Mountpoint
		 * helperData has qfsFilesystem Name and Mountpoint
		 * The resultMap would be of the following structure
		 *   Util.QFS_INFO(key) infoMap(HashMap)
		 *    where, infoMap is of structure
		 *         MetaserverResource(key) - Name of Metaserver Resource
		 *         MetaserverNodes(key) - List of Metaserver Nodes
		 *         ObanDiskGroup(key) - Name of obanDiskGroup
		 */

		resultMap.putAll(getQFSInfo(rq, nodeToConnect, helperData));

	    }
	    if (propList.contains(Util.WAIT_ZC_BOOT_INFO)) {
		bPropFound = true;
		if (helperData != null && helperData instanceof HashMap) {
		    String zoneClusterName = (String)((HashMap)helperData).get(Util.SEL_ZONE_CLUSTER);
		    if (zoneClusterName != null && zoneClusterName.trim().length() > 0) {
			//configuring for a zone cluster
			resultMap.putAll(getWaitForZCBootInfo(rq, nodeToConnect, zoneClusterName));
		    }
		}
	    }

	    if (propList.contains(Util.VxVMDEVICEGROUP)) {
		bPropFound = true;
		resultMap.putAll(getVxVMDeviceGroupInfo(propertyNames,
		    rq, nodeToConnect, helperData));

	    }
	    if (propList.contains(Util.SCAL_DEVGROUP_RSLIST)) {
		bPropFound = true;
		// Should be able to use this same function for
		// Util.SCAL_DEVGROUPS fetch all resources of type
		// SUNW.ScalDeviceGroup
		resultMap.putAll(getScalableDeviceGroupRSList(rq,
		    nodeToConnect, helperData));

	    }
	    if (propList.contains(Util.SCAL_MOUNTPOINT_RSLIST)) {
		bPropFound = true;
		// Should be able to use the same discovery for
		// Util.SCAL_MOUNTPOINTS fetch all resources of type
		// SUNW.ScalMountPoint
		resultMap.putAll(getScalableMountPointRSList(rq,
		    nodeToConnect, helperData));

	    }
	    if (propList.contains(Util.CRS_FRAMEWORK_PRESENT)) {
		bPropFound = true;
		resultMap.putAll(getCRSFrameworkInfo(rq, nodeToConnect,
		    helperData));

	    }
	    if (propList.contains(Util.CRS_FRAMEWORK_RSNAME)) {
		bPropFound = true;
		resultMap.putAll(getCRSFrameworkRSName(rq, nodeToConnect,
		    helperData));

	    }
	    if (propList.contains(Util.RAC_PROXY_RSNAME)) {
		bPropFound = true;
		resultMap.putAll(getRACProxyRSName(rq, nodeToConnect,
		    helperData));

	    }
	    if (propList.contains(Util.RAC_PROXY_RGNAME)) {
		bPropFound = true;
		resultMap.putAll(getRACProxyRGName(rq, nodeToConnect,
		    helperData));

	    }
	    if (propList.contains(Util.HASP_RSLIST)) {
		bPropFound = true;
		resultMap.putAll(getHASPRSList(rq, nodeToConnect,
		    helperData));

	    }
	    if (propList.contains(Util.LH_RSLIST)) {
		bPropFound = true;
		resultMap.putAll(getLHRSList(rq, nodeToConnect,
		    helperData));

	    }
	    if (propList.contains(Util.RAC_SERVER_PROXY_RTNAME)) {
		bPropFound = true;
		resultMap.putAll(getRACProxyRTName(rq, nodeToConnect,
		    helperData));

	    }
	    if (!bPropFound) {
		resultMap = getMBean(rq, nodeToConnect).
		    discoverPossibilities(propertyNames, helperData);
	    }
	} catch (IOException e) {
	    System.out.println(e.getMessage());
	}

	return resultMap;
    }

    /**
     * Checks if a given mountpoint for a filesystem of type either "s-qfs" or
     * "nas" is under RGM, monitored by a ScalMountPoint resource
     *
     * @param rq RequestContext
     * @param zoneClusterName name of the zone cluster or null for base cluster
     * @param  fsType  Filesystem Type
     * @param  mountPointDir  Mountpoint DIr
     *
     * @return  ScalMountPoint Resource if found or null
     */
    private String getScalResourceForMountPoint(
	RequestContext rq, String zoneClusterName, String fsType,
	String qfsMountPoint)
	throws IOException {

	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();
	if (rq == null) {
	    rm.setCacheEnabled(false);
	}

	// Get all the ScalMountPoint Resources monitoring the fsType
	List scalQFSResources = rm.getResourceList(
	    rq, zoneClusterName, null, "SUNW.ScalMountPoint", "FileSystemType",
	    fsType);

	// Check if any of the found resources monitor the
	// mountpoint dir
	if (scalQFSResources != null) {
	    for (Iterator j = scalQFSResources.iterator(); j.hasNext();) {
		String scalMountPointResource = (String) j.next();
		String mountPointDir = (String) rm.getResourcePropertyValue(
		    rq, zoneClusterName, scalMountPointResource,
		    "MountPointDir");

		if (mountPointDir.equals(qfsMountPoint)) {
		    return scalMountPointResource;
		}
	    }
	}

	return null;
    }

    /**
     * Gets the metaserver resource for a qfs mountpoint If it doesnt exists
     * generate a unique name and return it.
     *
     * @param  qfsMountpoint
     * @param  zoneRootPath
     *
     * @return  Existing Metaserver Resource or auto-generate Name
     */
    private String getMetaserverResource(RequestContext rq,
	    String qfsMountPoint, String zoneRootPath)
	throws IOException {
	StringBuffer mountPoint = new StringBuffer();

	if (zoneRootPath == null) {
	    // user is configuring on a base cluster
	    mountPoint.append(qfsMountPoint);
	} else {
	    // append mount point with zone root path for zone clusters
	    // The user is configuring on a zone cluster
	    mountPoint.append(zoneRootPath);
	    mountPoint.append(qfsMountPoint);
	}

	String qfsFilesystemArr[] = new String[] { mountPoint.toString() };

	// get Resources of type SUNW.qfs with property QFSFileSystem equals to
	// qfsFilesystemArr
	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();
	if (rq == null) {
	    rm.setCacheEnabled(false);
	}

	List scalQFSResources = rm.getResourceList(
	    rq, null, null, "SUNW.qfs", "QFSFileSystem", qfsFilesystemArr);

        // If there exists a metaserver resource monitoring the mountpoint
        // return the same
        if ((scalQFSResources != null) && (scalQFSResources.size() > 0)) {

            // The first metaserver resource is returned.
            // It is highly unlikely that multiple metaserver resources would
            // be created for a single qfsmount point
            return (String) scalQFSResources.get(0);
        } else {

            // Generate an unique Name for the Metaserver Resource
            String rName;

            // Generated Name scal-<attr>-rs-<count>
            rName = QFS + qfsMountPoint.replace('/', '-') + "-rs";

            // Check if the name already exists

	    ResourceData rData = rm.getResourceData(
		rq, null, null, rName);
	    if (rData != null) {
                int count = 1;

                while (rm.getResourceData(
		    rq, null, null, rName + "-" + count) != null) {
                    count++;
                }

                rName = rName + "-" + count;
            }

            return rName;
        }
    }

    /**
     * Gets a map containing the waitForZCBoot RS and RG name if they exist. If the RS and RG does
     * not exist, generates unique names for them
     *
     * @param rq
     * @parem nodeToConnect
     * @param zoneClusterName
     *
     * @return map containing the RS and RG name
     */
    private Map getWaitForZCBootInfo(RequestContext rq, String nodeToConnect, String zoneClusterName) {
	HashMap retMap = new HashMap();

	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();
	if (rq == null) {
	    rm.setCacheEnabled(false);
	}

	List rsList = null;
	try {
	    // Look for the resource in the base cluster
	    rsList = rm.getResourceList(rq, null,
		null, Util.WAIT_ZC_BOOT_RTNAME, Util.ZCNAME_PROP,
		zoneClusterName, true);
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	}

	if (rsList != null && rsList.size() > 0) {
	    // get the first resource
	    ResourceData rsData = (ResourceData)rsList.get(0);

	    if (rsData != null) {
		retMap.put(Util.WAIT_ZC_BOOT_RESOURCE, rsData.getName());
		retMap.put(Util.WAIT_ZC_BOOT_GROUP,
		    rsData.getResourceGroupName());
	    }
	} else {
	    // a resource was not found. Generate a unique name.

	    StringBuffer prefix = new StringBuffer();
	    prefix.append(WAIT_FOR_ZC_BOOT);
	    prefix.append(zoneClusterName);

	    HashMap helperData = new HashMap();
	    helperData.put(Util.SEL_ZONE_CLUSTER, null);
	    helperData.put(Util.RS_PREFIX_KEY, prefix.toString());

	    HashMap rsMap = generateRSName(rq, nodeToConnect, (Object)helperData);
	    retMap.put(Util.WAIT_ZC_BOOT_RESOURCE, rsMap.get(Util.RS_NAME));

	    // generate a unique RG
	    helperData.put(Util.RG_PREFIX_KEY, prefix.toString());
	    HashMap rgMap = generateRGName(rq, nodeToConnect, (Object)helperData);
	    retMap.put(Util.WAIT_ZC_BOOT_GROUP, rgMap.get(Util.RG_NAME));
        }
	return retMap;
    }

    public HashMap discoverPossibilities(RequestContext rq,
	String nodeToConnect, String nodeList[], String propertyNames[],
	Object helperData) {

	HashMap resultMap = new HashMap();

        try {
            List propList = Arrays.asList(propertyNames);

	    if (propList.contains(Util.NAS)) {
		 // Key ("exported FS Name") Value ("ScalMountRes,MountPoint")
		Map scalNas = null;

		// Key ("exported FS Name") Value ("MountPoint")
		Map otherNas = null;

		HashMap mountPointMap = getMBean(rq, nodeToConnect).
		    discoverPossibilities(nodeList, propertyNames, helperData);


		String zoneClusterName = null;
		if (helperData != null && helperData instanceof HashMap) {
		    zoneClusterName = (String) ((HashMap) helperData).get(
			Util.SEL_ZONE_CLUSTER);
		}

		if (mountPointMap != null) {
		    ArrayList mountPoints = (ArrayList)
					    mountPointMap.get(Util.NAS);

		    if (mountPoints != null) {
			int size = mountPoints.size();

			for (int i = 0; i < size; i++) {
			    String mountPoint = (String) mountPoints.get(i);

			    String[] splitStr = mountPoint.split(Util.COMMA);
			    String nasMountPoint = splitStr[0];
			    String filesystem = splitStr[1];

			    // Find if this mountpoint is monitored by an
			    // ScalMountPoint resource of type nas
			    String scalMountPointResource =
				    getScalResourceForMountPoint(
				rq,
				zoneClusterName,
				Util.NAS.toLowerCase(),
				nasMountPoint);

			    if (scalMountPointResource != null) {
				if (scalNas == null) {
				    scalNas = new HashMap();
				}

				scalNas.put(filesystem,
				    scalMountPointResource + "," +
				    nasMountPoint);
			    } else {
				if (otherNas == null) {
				    otherNas = new HashMap();
				}

				otherNas.put(filesystem, nasMountPoint);
			    }
			}
                    }
                }

		resultMap.put(Util.RGM_NAS, scalNas);
		resultMap.put(Util.NONRGM_NAS, otherNas);

	    } else if (propList.contains(Util.SQFS)) {
		HashMap infoMap = getMBean(rq, nodeToConnect).
		    discoverPossibilities(nodeList, propertyNames, helperData);

		// Key ("s-qfs Name") Value ("ScalMountRes,MountPoint")
		Map scalQfs = null;

		// Key ("s-qfs Name") Value ("MountPoint")
		Map otherQfs = null;

		String zoneClusterName = null;
		if (helperData != null && helperData instanceof HashMap) {
		    zoneClusterName = (String) ((HashMap) helperData).get(
			Util.SEL_ZONE_CLUSTER);
		}

		if (infoMap != null) {
		    List selQFSFilesystems = (ArrayList) infoMap.get(Util.SQFS);

		    /*
		     * Check whether each of the QFS filesystem is monitored in
		     * RGM by a ScalMountPoint resource. Accordingly populate
		     * the rgm and non-rgm qfs Maps
		     */

		    if (selQFSFilesystems != null) {
			for (Iterator i = selQFSFilesystems.iterator();
							    i.hasNext();) {
			    String fileSystem = (String) i.next();
			    String[] splitStr = fileSystem.split(Util.COMMA);

			    if (splitStr != null && splitStr.length == 2) {
				String qfsName = splitStr[0];
				String mountPoint = splitStr[1];

				if (mountPoint != null &&
				    mountPoint.trim().length() > 0) {
				    // Find if this mountpoint is monitored by
				    // an ScalMountPoint resource of type s-qfs

				    String scalMountPointResource =
					    getScalResourceForMountPoint(
							rq, zoneClusterName,
							Util.SQFS, mountPoint);

				    if (scalMountPointResource != null) {
					if (scalQfs == null) {
					    scalQfs = new HashMap();
					}

					scalQfs.put(qfsName,
						scalMountPointResource + "," +
						mountPoint);
				    } else {
					if (otherQfs == null) {
					    otherQfs = new HashMap();
					}

					otherQfs.put(qfsName, mountPoint);
				    }
				}
			    }
			}
		    }
		}

		resultMap.put(Util.RGM_SQFS, scalQfs);
		resultMap.put(Util.NONRGM_SQFS, otherQfs);
	    } else if (propList.contains(Util.SVMDEVICEGROUP)) {
		HashMap infoMap = getMBean(rq, nodeToConnect).
		    discoverPossibilities(nodeList, propertyNames, helperData);

		String zoneClusterName = null;
		if (helperData != null && helperData instanceof HashMap) {
		    zoneClusterName = (String) ((HashMap) helperData).get(
			Util.SEL_ZONE_CLUSTER);
		}

		ClusterDiskGroupInfo clusterDiskGroupInfo =
			(ClusterDiskGroupInfo)
		    infoMap.get(Util.SVMDEVICEGROUP);
		fillScalDGMap(rq, zoneClusterName, clusterDiskGroupInfo);
		resultMap.put(Util.SVMDEVICEGROUP, clusterDiskGroupInfo);
	    } else {
		resultMap = getMBean(rq, nodeToConnect).
		    discoverPossibilities(nodeList, propertyNames, helperData);
	    }
	} catch (IOException e) {
	    e.printStackTrace();
	}

	return resultMap;
    }

    private void fillScalDGMap(RequestContext rq, String zoneClusterName,
	ClusterDiskGroupInfo clusterDiskGroupInfo) throws IOException {

	HashMap scalDGMap = null;
	HashMap rsSVMMap = null;
	HashMap rsQfsSVMMap = null;
	List clusterDiskGroupInfoList = null;

	DataModel dm = DataModel.getDataModel(rq);
	ResourceModel rm = dm.getResourceModel();
	if (rq == null) {
	    rm.setCacheEnabled(false);
	}

	if (clusterDiskGroupInfo != null) {
	    clusterDiskGroupInfoList =
		    clusterDiskGroupInfo.getDiskGroupInfoList();
	}

	if (clusterDiskGroupInfoList != null) {
	    int size = clusterDiskGroupInfoList.size();
	    for (int i = 0; i < size; i++) {
		DiskGroupInfo diskGroupInfo = (DiskGroupInfo)
		    clusterDiskGroupInfoList.get(i);

		if (diskGroupInfo != null) {
		    // for just SVM, scalable disk group is in the ZC.
		    List rsListInZC = rm.getResourceList(
			rq, zoneClusterName, null,
			Util.SCAL_DEVGROUP_RTNAME,
			null, null);

		    // list of resources in the zone cluster
		    // for shared QFS with SVM, scalable disk group
		    // is always in the base cluster.
		    List rsListInBC = rm.getResourceList(
			rq, null, null,
			Util.SCAL_DEVGROUP_RTNAME,
			null, null);

		    String diskGroupName = diskGroupInfo.getDiskGroupName();
		    HashMap availSVMVolMap = diskGroupInfo.getAvailSVMVolMap();
		    if (availSVMVolMap != null) {
			Set volumesKeySet = availSVMVolMap.keySet();

			Iterator volumeIterator = volumesKeySet.iterator();
			while (volumeIterator.hasNext()) {

			    String volume = (String) volumeIterator.next();
			    String[] volumeArr = new String[] { volume };

			    String rsName =  getResourceName(
				rm, rsListInZC, diskGroupName, volumeArr);

			    if (rsName != null && rsName.trim().length() > 0) {
				// Found a resource managing the volume.
				// add resource name against the volume

				String allDisks = null;

				String[] splitStr = rsName.split(Util.COLON);
				if (splitStr != null && splitStr.length == 2) {
				    rsName = splitStr[0];
				    allDisks = splitStr[1];
				}
				
				availSVMVolMap.put(volume, rsName);

				rsSVMMap = diskGroupInfo.getSVMResourceMap();

				if (rsSVMMap == null) {
				    rsSVMMap = new HashMap();
				}

				if (allDisks == null) {
				    List volList = (List) rsSVMMap.get(rsName);
				    if (volList == null) {
					volList  = new ArrayList();
				    }
				    if (!volList.contains(volume)) {
					volList.add(volume);
				    }
				    rsSVMMap.put(rsName, volList);
				} else {
				    List volList = new ArrayList();
				    volList.add(allDisks);
				    rsSVMMap.put(rsName, volList);
				}
				diskGroupInfo.setSVMResourceMap(rsSVMMap);
			    }
			} // volume iterator
		    } // availSVMVolMap != null

		    HashMap availQfsSVMVolMap =
			    diskGroupInfo.getAvailQfsSVMVolMap();
		    if (availQfsSVMVolMap != null) {
			Set volumesKeySet = availQfsSVMVolMap.keySet();

			Iterator volumeIterator = volumesKeySet.iterator();
			while (volumeIterator.hasNext()) {

			    boolean found = false;
			    String volume = (String) volumeIterator.next();
			    String[] volumeArr = new String[] { volume };

			    String rsName =  getResourceName(
				rm, rsListInBC, diskGroupName, volumeArr);

			    if (rsName != null && rsName.trim().length() > 0) {
				// Found a resource managing the volume in the
				// base cluster.
				// add resource name to the volume

				String allDisks = null;

				String[] splitStr = rsName.split(Util.COLON);
				if (splitStr != null && splitStr.length == 2) {
				    rsName = splitStr[0];
				    allDisks = splitStr[1];
				}
				availQfsSVMVolMap.put(volume, rsName);

				rsQfsSVMMap =
					diskGroupInfo.getQfsSVMResourceMap();

				if (rsQfsSVMMap == null) {
				    rsQfsSVMMap = new HashMap();
				}

				if (allDisks == null) {
				    List volList = (List) rsQfsSVMMap.get(rsName);
				    if (volList == null) {
					volList  = new ArrayList();
				    }
				    if (!volList.contains(volume)) {
					volList.add(volume);
				    }
				    rsQfsSVMMap.put(rsName, volList);
				} else {
				    List volList = new ArrayList();
				    volList.add(allDisks);
				    rsQfsSVMMap.put(rsName, volList);
				}

				diskGroupInfo.setQfsSVMResourceMap(rsQfsSVMMap);
			    }
			} // volume iterator
		    } // availQfsSVMVolMap != null
		} // diskGroupInfo != null
	    }// for loop
	} // if clusterDiskGroupInfoList != null
    }

    private String getResourceName(ResourceModel rm, List rsList,
	    String diskGroupName, String[] volumeArr) {

	String rsName = null;
	boolean found = false;
	String allDisks = null;

	if (rsList != null) {
	    int size1 = rsList.size();
	    int j = 0;
	    while (j < size1 && !found) {
		ResourceData rsData = (ResourceData) rsList.get(j);

		rsName = rsData.getName();
		// Find the property name in Extension Properties
		List extProps = rsData.getExtProperties();
		Iterator extPropIter1 = extProps.iterator();

		while (extPropIter1.hasNext() && !found) {
		    ResourceProperty rp1 = (ResourceProperty)
						    extPropIter1.next();
		    if (rm.isPropertyValueEqual(
			rp1, Util.DISKGROUP_NAME, diskGroupName)) {

			// An entry is found with a matching disk group name
			// Now check the logical device list

			Iterator extPropIter2 = extProps.iterator();
			while (extPropIter2.hasNext() && !found) {
			    ResourceProperty rp2 = (ResourceProperty)
							extPropIter2.next();

			    if (rp2.getName().equals(Util.LOGICAL_DEVLIST)) {
				Object resValue = rm.getResourcePropertyValue(
								    rp2, false);


				String[] strArray = null;
				
				if (rp2 instanceof ResourcePropertyStringArray) {
				    if (resValue != null) {
					strArray = (String[])resValue;
				    }
				}
				if (strArray == null ||
				    strArray.length == 0) {
				    found = true;
				    allDisks = Util.ALL;
				} else if (rm.isPropertyValueEqual(rp2,
					Util.LOGICAL_DEVLIST, volumeArr)) {
				    found = true;
				}
			    }
			}
		    }
		}
		j++;
	    }
	}

	if (found) {
	    if (allDisks != null) {
		return rsName + Util.COLON + allDisks;
	    }
	    return rsName;
	} else {
	    return null;
	}
    }

    public ErrorValue validateInput(RequestContext rq,
	String nodeToConnect, String propertyNames[], HashMap userInputs,
	Object helperData) {

        ErrorValue err = new ErrorValue();
        err.setReturnVal(Boolean.TRUE);

	try {
	    List propList = Arrays.asList(propertyNames);

	    // Validate ResourceGroup Name

	    if (propList.contains(Util.RESOURCEGROUPNAME)) {
		String rgName = (String) userInputs.get(Util.RESOURCEGROUPNAME);
		String zoneClusterName = null;
		if (helperData != null && helperData instanceof HashMap) {
		    zoneClusterName = (String) ((HashMap) helperData).get(
						    Util.SEL_ZONE_CLUSTER);
		}

		DataModel dm = DataModel.getDataModel(rq);
		ResourceGroupModel rgm = dm.getResourceGroupModel();
		if (rq == null) {
		    rgm.setCacheEnabled(false);
		}

		ResourceGroupData rgData = rgm.getResourceGroupData(
		    rq, zoneClusterName, rgName);

		if (rgData != null) {
		    // RG Name is in use
		    err.setReturnVal(Boolean.FALSE);
		}
	    } else if (propList.contains(Util.RESOURCENAME)) {
		String rName = (String) userInputs.get(Util.RESOURCENAME);

		// Make sure rsName is unique in the zonecluster
		String zoneClusterName = null;
		if (helperData != null && helperData instanceof HashMap) {
		    zoneClusterName = (String) ((HashMap) helperData).get(
							Util.SEL_ZONE_CLUSTER);
		}

		DataModel dm = DataModel.getDataModel(rq);
		ResourceModel rm = dm.getResourceModel();
		if (rq == null) {
		    rm.setCacheEnabled(false);
		}

		ResourceData rData = rm.getResourceData(
		    rq, zoneClusterName, null, rName);

		if (rData != null) {
		    err.setReturnVal(Boolean.FALSE);
		}
	    } else if (propList.contains(Util.HOSTNAMELIST)) {
		String hostNamesArr[] = (String[])userInputs.get(
		    Util.HOSTNAMELIST);
		String zoneClusterName = null;
		HashMap helperMap = null;

		if (helperData != null && helperData instanceof HashMap) {
		    helperMap = (HashMap)helperData;
		    zoneClusterName = (String)helperMap.get(
					    Util.SEL_ZONE_CLUSTER);
		}

		// Validate for Valid HostName
		for (int i = 0; i < hostNamesArr.length; i++) {
		    if (!DataServicesUtil.isHostNameValid(hostNamesArr[i])) {
			err.setReturnVal(Boolean.FALSE);
			err.setErrorString(hostNamesArr[i] +
					    " is an invalid hostname");
			return err;
		    }
		    // Validate whether the HostName is already used by Another
		    // LH Resource in the Cluster
		    DataModel dm = DataModel.getDataModel(rq);
		    ResourceModel rm = dm.getResourceModel();
		    if (rq == null) {
			rm.setCacheEnabled(false);
		    }

		    List resourceList = rm.getResourceList(rq, zoneClusterName,
			null, Util.LH_RTNAME,
			Util.HOSTNAMELIST, new String[] { hostNamesArr[i] });

		    if ((resourceList != null) && (resourceList.size() > 0)) {
			err.setReturnVal(Boolean.FALSE);
			err.setErrorString(
			    hostNamesArr[i] + " is being used by the another " +
			    "resource " +
			    resourceList.iterator().next().toString());
			return err;
		    }
		    // Validate whether the HostName is already used by SH
		    // Resource in the Cluster
		    resourceList = rm.getResourceList(rq, zoneClusterName,
			null, Util.SH_RTNAME,
			Util.HOSTNAMELIST, new String[] { hostNamesArr[i] });

		    if ((resourceList != null) && (resourceList.size() > 0)) {
			err.setReturnVal(Boolean.FALSE);
			err.setErrorString(
			    hostNamesArr[i] + " is being used by the another " +
			    "resource " +
			    resourceList.iterator().next().toString());
			return err;
		    }
		}
	    } else {
		err = getMBean(rq, nodeToConnect).
		    validateInput(propertyNames, userInputs, helperData);
	    }
	} catch (IOException e) {
	    return null;
	}

	return err;
    }

    public ErrorValue applicationConfiguration(RequestContext rq,
	String nodeToConnect, Object helperData) {
	try {
	    return getMBean(rq, nodeToConnect).applicationConfiguration(
								helperData);
	} catch (IOException e) {
	    return null;
	}
    }

    public List getCommandList(RequestContext rq,
	String nodeToConnect) {
	try {
	    return getMBean(rq, nodeToConnect).getCommandList();
	} catch (IOException e) {
	    return null;
	}
    }

    /**
     * Bring Resources Online for the RAC Database 9i wizard
     *
     * @param  configurationInformation  Has the configuration information
     *
     * @return  CommandList
     */
    public List bringOnline9IDatabase(RequestContext rq, String nodeToConnect,
	Map configurationInformation) throws CommandExecutionException {
	try {
	    return getMBean(rq, nodeToConnect).bringOnline9IDatabase(
						configurationInformation);
	} catch (IOException e) {
	    return null;
	}
    }

    /**
     * Bring Resources Online for the RAC Database 9i wizard
     *
     * @param  configurationInformation  Has the configuration information
     *
     * @return  CommandList
     */
    public List rollBack9ICreation(RequestContext rq, String nodeToConnect,
	Map configurationInformation) throws CommandExecutionException {
	try {
	    return getMBean(rq, nodeToConnect).rollBack9ICreation(
						configurationInformation);
	} catch (IOException e) {
	    return null;
	}
    }
}
