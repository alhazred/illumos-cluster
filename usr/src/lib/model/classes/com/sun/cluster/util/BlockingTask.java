/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)BlockingTask.java 1.6     08/05/20 SMI"
 */

package com.sun.cluster.util;

/**
 * Interface which can be used along with TimeoutController to perform a desired
 * operation in a time controlled manner. The class which has a method which
 * needs to be executed in a time controlled fashion has to perform that
 * operation from the executeTask() method. Any clean-up the class has to
 * perform in case the task execution failed can be implemented in undoTask()
 */
public interface BlockingTask {

    /**
     * Perform the operations which take time in this method.
     *
     * @exception  Exception  which the desired operation may throw.
     */
    public void executeTask() throws Exception;

    /**
     * Perform any clean up in case the desired operation failed.
     *
     * @exception  Exception  which the clean up may throw.
     */
    public void undoTask() throws Exception;
}
