/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)TimeoutController.java 1.6     08/05/20 SMI"
 */

package com.sun.cluster.util;

/**
 * Class that can be used to execute functions which take a long time in a time
 * controlled fashion. If there is a piece of code which executes(blocks) for a
 * long time and has to be executed within a maximum number of seconds then this
 * class can be used to execute that piece of code. One typical scenario where
 * this can be used in cases where we attempt to obtain a connection to some
 * remote node. There are cases where the method which connects to the remote
 * node takes long time to timeout when the remote node is down. This class can
 * be used in such a case. For example, JmxServerConnection uses this class. The
 * class which needs to use this utility has to implement the interface
 * BlockingTask and override the functions executeTask() and undoTask(). This
 * class will attempt to execute the function executeTask() in a separate thread
 * (T2)which is different from the thread(T1) which invokes this class. This
 * class waits for a maximum of (specified) timeout seconds. In case the
 * executeTask() (thread T2) returns before this specified timeout then T1
 * returns immediately. On the other hand, if executeTask() is taking longer
 * than the specified timeout then this class halts executing the method and
 * returns and also executes undoTask() in the background. The default timeout
 * is 5 seconds. Example: Assuming there is a class A which has a method
 * doThis() which takes a long time to execute under certain conditions and we
 * would like to time-control this method. Then class A should implement
 * BlockingTask and call this class's doControlledExecution() as follows: class
 * A implements BlockingTask { private void doThis(){ ...... } public void
 * performSomething() { // this is where we would have otherwise called doThis()
 * TimeoutController tCont = new TimeoutController(this);
 * tCont.doControlledExecution(); } public void executeTask(){ doThis(); }
 * public void undoTask(){ undoThis(); } } JmxServerConnection uses this class
 * to connect to a remote node in a controlled manner.
 */

public class TimeoutController {

    public static long TIME_OUT = 5000;

    private BlockingTask bTask = null;
    private long timeOutMillis = TIME_OUT;

    public TimeoutController(BlockingTask blockTask) {
        bTask = blockTask;
    }

    public TimeoutController(BlockingTask blockTask, long timeOut) {
        bTask = blockTask;
        timeOutMillis = timeOut;
    }

    /**
     * Perform an operation in a time controlled manner. This method will invoke
     * BlockingTask's executeTask() method. If executeTask() does not complete
     * in the specified timeout, it interrupts the process and returns while
     * executing BlockingTask's undoTask() method in the background. If
     * executeTask() returns within the timeout specified this method returns
     * immediately.
     *
     * @exception  OperationTimedOutException  if the specified task could not
     * be executed within the specified timeout.
     * @exception  Exception  any exception which executeTask() throws.
     */

    public void doControlledExecution() throws OperationTimedOutException,
        Exception {

        /**
         * This method will not synchronize anything. It is upto the
         * caller to check synchronization related issues.
         */

        BlockingTaskRunner bRunner = new BlockingTaskRunner(bTask);
        Exception taskExc;

        bRunner.start();
        bRunner.join(timeOutMillis);

        if (bRunner.isAlive()) {
            bRunner.undoTaskExecution();
            bRunner.interrupt();
            throw (new OperationTimedOutException());
        }

        taskExc = bRunner.getException();

        if (taskExc != null) {
            throw (taskExc);
        }
    }
}
