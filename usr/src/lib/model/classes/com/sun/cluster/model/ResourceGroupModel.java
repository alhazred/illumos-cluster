/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)ResourceGroupModel.java	1.12	08/09/22 SMI"
 */

package com.sun.cluster.model;

import com.iplanet.jato.RequestContext;

import com.sun.cluster.agent.auth.CommandExecutionException;
import com.sun.cluster.agent.auth.ExitStatus;
import com.sun.cluster.agent.event.SysEventNotification;
import com.sun.cluster.agent.rgm.ResourceGroupData;
import com.sun.cluster.agent.rgm.ResourceGroupModeEnum;
import com.sun.cluster.agent.rgm.RgGroupMBean;
import com.sun.cluster.agent.rgm.RgmManagerMBean;
import com.sun.cluster.common.TrustedMBeanModel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.sun.cluster.common.MBeanModel;
import java.util.Map;

/**
 * Data model class for ResourceGroup data
 */
public class ResourceGroupModel extends ModelBase {

    HashMap rgDataValid = new HashMap();

    /**
     * HashMap to hold ResourceGroup Data corresponding to physical cluster
     * OR zoneclusters. ZoneCluster name would be null in case of physical
     * cluster (OR the cluster in which this code is running) in the following
     * structure.
     * The structure of this HashMap is as follows
     *  KEY                           VALUE
     *  ZoneClusterName OR null       HashMap object with following structure
     *                                KEY                     VALUE
     *                                ResourceGroup name      ResourceGroupData
     *                                                        Object
     */
    HashMap rgDataMap = new HashMap();


    /**
     * HashMap to hold names of ResourceGroups for better search. The structure
     * of this HashMap is as follows. ZoneCluster name would be null in case of
     * physical cluster (OR the cluster in which this code is running) in the
     * following structure.
     *  KEY                           VALUE
     *  ZoneClusterName or null       Set object with the names of
     *                                ResourceGroups.
     */

    HashMap rgNameMap = new HashMap();


    // Test Data - should be of type rgDataMap and rgNameMap
    private HashMap testRGDataMap = new HashMap();
    private HashMap testRGNameMap = new HashMap();

    public ResourceGroupModel() {
        // super();
        if (isTestMode) {
            rgDataMap = testRGDataMap;
            rgNameMap = testRGNameMap;
        }
    }

    private void resourceGroupInit(
            RequestContext context, String zoneClusterName)
            throws IOException {
        synchronized (syncObject) {
            if (!isDataValid(zoneClusterName)) {
                HashMap tmpMap;
                if (!isTestMode) {
                    RgGroupMBean mbean = getRgGroupMBean(context);

                    tmpMap = mbean.readData(zoneClusterName);

                    // Store the retrieved map
                    rgDataMap.put(zoneClusterName, tmpMap);

                    // Store the names of ResourceGroups for better search
                    rgNameMap.put(zoneClusterName, tmpMap.keySet());
                }
                setDataValid(zoneClusterName, true);
            }
        }
    }

    /**
     * Utility function to retrieve an array of ResourceGroupData Objects for
     * the given zoneClusterName.
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @param zoneClusterName If this is null the data returned would be for the
     *                        physical cluster (OR the cluster in which this
     *                        code is running)
     * @return Array of ResourceGroupData Objects
     * @throws java.io.IOException
     */
    public ResourceGroupData[] getResourceGroupData(
            RequestContext rq, String zoneClusterName)
            throws IOException {
        if (!isDataValid(zoneClusterName)) {
            resourceGroupInit(rq, zoneClusterName);
        }

        // Retrieve the corresponding data
        HashMap tmpRgMap = (HashMap)rgDataMap.get(zoneClusterName);
        Set tmpRgNameSet = (Set)rgNameMap.get(zoneClusterName);

        // Return an array of ResourceGroupData objects
        ResourceGroupData[] rgDataAr = new ResourceGroupData[tmpRgMap.size()];
        Iterator iter = tmpRgNameSet.iterator();
        int count = 0;
        while (iter.hasNext()) {
            rgDataAr[count++] = (ResourceGroupData) tmpRgMap.get(iter.next());
        }

        return rgDataAr;
    }



    /**
     * Utility function to retrieve Resource Group data corresponding to
     * the given resource group name and zonecluster name.
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @param zoneClusterName If this is null the data returned would be for the
     *                        physical cluster (OR the cluster in which this
     *                        code is running)
     * @param rgName ResourceGroupName for which the data is required
     * @return ResourceGroupData object
     * @throws java.io.IOException
     */
    public ResourceGroupData getResourceGroupData(
            RequestContext rq, String zoneClusterName, String rgName)
            throws IOException {

        if (!isDataValid(zoneClusterName)) {
            resourceGroupInit(rq, zoneClusterName);
        }

        // Retrieve the corresponding data and return
        HashMap tmpRgMap = (HashMap)rgDataMap.get(zoneClusterName);
	Object obj = tmpRgMap.get(rgName);
	if (obj != null) {
	    return (ResourceGroupData)obj;
	}
	return null;
    }


    /**
     * Utility function to retrieve names of Resource Groups corresponding to
     * the given zonecluster name.
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @param zoneClusterName If this is null the data returned would be for the
     *                        physical cluster (OR the cluster in which this
     *                        code is running)
     * @return Set of ResourceGroup names
     * @throws java.io.IOException
     */
    public Set getResourceGroupNames(
            RequestContext rq, String zoneClusterName) throws IOException {
        if (!isDataValid(zoneClusterName)) {
            resourceGroupInit(rq, zoneClusterName);
        }
        // Retrieve the corresponding data and return
        return (Set)rgNameMap.get(zoneClusterName);
    }

    /**
     * Utility function to retrieve names of Resource Groups corresponding to
     * the given zonecluster name and the given nodeList.
     * @param rq RequestContext object. If this is null the method would use
     *                         TrustedMBean model to get the connection.
     * @param zoneClusterName If this is null the data returned would be for the
     *                        physical cluster (OR the cluster in which this
     *                        code is running)
     * @param nodeList return RGs hosted on the given nodes
     * @return Set of ResourceGroup names
     * @throws java.io.IOException
     */
    public List getResourceGroupNames(
	RequestContext rq, String zoneClusterName, String[] nodeList)
	throws IOException {

	ArrayList rgNames = null;
        if (!isDataValid(zoneClusterName)) {
            resourceGroupInit(rq, zoneClusterName);
        }
        // Retrieve the corresponding data and return

	ResourceGroupData[] rgDataArray =
                            getResourceGroupData(rq, zoneClusterName);
	if (rgDataArray != null) {
	    for (int i = 0; i < rgDataArray.length; i++) {
		ResourceGroupData rgData = rgDataArray[i];

		List rgNodes = Arrays.asList(rgData.getNodeNames());
		List nodes = Arrays.asList(nodeList);

		if (rgNodes.containsAll(nodes) &&
		    nodes.containsAll(rgNodes)) {
		    // Get resource model
		    if (rgNames == null) {
			rgNames = new ArrayList();
		    }
		    rgNames.add(rgData.getName());
		}
	    }
	}

        return rgNames;
    }



    /**
     * This method creates a new instance of a Resource Group with its default
     * system values
     *
     * @param  name  the name of the resource group. This name should be unique
     * and can not be modified after the creation of the Resource Group.
     * @param zoneClusterName If this is null the resource group would be added
     *                        in physical cluster or in the cluster in which
     *                        this method is run.
     * @param  properties  the properties associated to this resource group
     * (might be null).
     * @param  commit  if set to <code>true</code>, commit the operation and
     * made the underline changes in the cluster; else just return the
     * constructed command.
     *
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     */
    public ExitStatus[] addResourceGroup(RequestContext rq,
            String name, String zoneClusterName,
            Map properties, boolean commit)
            throws CommandExecutionException, IOException {

        // Verify that the resource group name is not already used by another
        // resource group.
        if (getResourceGroupNames(rq, zoneClusterName).contains(name)) {
            throw new IllegalArgumentException("Resource Group " + name +
                    " already registered.");
        }

        invalidateCache(commit);
        return getRgGroupMBean(rq).addResourceGroup(name, properties,
                zoneClusterName, commit);
    }



    /**
     * Suspend resourcegroups (rgs) present in zonecluster (zoneClusterName).
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgs
     * @return
     * @throws CommandExecutionException
     */
    public ExitStatus[] suspendRGs(RequestContext rq,
            String zoneClusterName, String rgs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "suspend", zoneClusterName, rgs, null);
    }

    /**
     * Fast Suspend resourcegroups (rgs) present in
     * zonecluster (zoneClusterName).
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgs
     * @return
     * @throws CommandExecutionException
     */
    public ExitStatus[] fastsuspendRGs(RequestContext rq,
            String zoneClusterName, String rgs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "fastsuspend", zoneClusterName, rgs, null);
    }

    /**
     * Resume resourcegroups (rgs) present in zonecluster (zoneClusterName).
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgs
     * @return
     * @throws CommandExecutionException
     */
    public ExitStatus[] resumeRGs(RequestContext rq,
            String zoneClusterName, String rgs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "resume", zoneClusterName, rgs, null);
    }

    /**
     * Remaster resourcegroups (rgs) present in zonecluster (zoneClusterName).
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgs
     * @return
     * @throws CommandExecutionException
     */
    public ExitStatus[] remasterRGs(RequestContext rq,
            String zoneClusterName, String rgs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "remaster", zoneClusterName, rgs, null);
    }

    /**
     * Restart resourcegroups (rgs) present in zonecluster (zoneClusterName).
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgs
     * @return
     * @throws CommandExecutionException
     */
    public ExitStatus[] restartRGs(RequestContext rq,
            String zoneClusterName, String rgs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "restart", zoneClusterName, rgs, null);
    }

    /**
     * Restart resource on a per-node basis.
     *
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgNames
     * @param nodeList
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     * @throws java.io.IOException
     */
    public ExitStatus[] pernodeRestartRGs(RequestContext rq,
	    String zoneClusterName,
            String rgNames, String nodeList)
        throws CommandExecutionException, IOException {
	return executeOperation(
		rq, "restartPerNode", zoneClusterName, rgNames, nodeList);
    }


    /**
     * Force Delete resourcegroups (rgs) present in
     * zonecluster (zoneClusterName).
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgs
     * @return
     * @throws CommandExecutionException
     */
    public ExitStatus[] forceDeleteRGs(RequestContext rq,
            String zoneClusterName, String rgs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "forceDelete", zoneClusterName, rgs, null);
    }

    /**
     * Bring resourcegroups (rgs) online present in
     * zonecluster (zoneClusterName).
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgs
     * @return
     * @throws CommandExecutionException
     */
    public ExitStatus[] bringOnlineRGs(RequestContext rq,
            String zoneClusterName, String rgs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "bringOnline", zoneClusterName, rgs, null);
    }

    /**
     * Bring Online resource on a per-node basis.
     *
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgNames
     * @param nodeList
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     * @throws java.io.IOException
     */
    public ExitStatus[] pernodeBringOnlineRGs(RequestContext rq,
	    String zoneClusterName,
            String rgNames, String nodeList)
        throws CommandExecutionException, IOException {
	return executeOperation(
		rq, "bringOnlinePerNode", zoneClusterName, rgNames, nodeList);
    }


    /**
     * Bring resourcegroups (rgs) offline present in
     * zonecluster (zoneClusterName).
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgs
     * @return
     * @throws CommandExecutionException
     */
    public ExitStatus[] bringOfflineRGs(RequestContext rq,
            String zoneClusterName, String rgs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "bringOffline", zoneClusterName, rgs, null);
    }

    /**
     * Bring Offline resource on a per-node basis.
     *
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgNames
     * @param nodeList
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     * @throws java.io.IOException
     */
    public ExitStatus[] pernodeBringOfflineRGs(RequestContext rq,
	    String zoneClusterName,
            String rgNames, String nodeList)
        throws CommandExecutionException, IOException {
	return executeOperation(
		rq, "bringOfflinePerNode", zoneClusterName, rgNames, nodeList);
    }

    /**
     * Manage resourcegroups (rgs) present in zonecluster (zoneClusterName).
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgs
     * @return
     * @throws CommandExecutionException
     */
    public ExitStatus[] manageRGs(RequestContext rq,
            String zoneClusterName, String rgs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "manage", zoneClusterName, rgs, null);
    }

    /**
     * UnManage resourcegroups (rgs) present in zonecluster (zoneClusterName).
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgs
     * @return
     * @throws CommandExecutionException
     */
    public ExitStatus[] unmanageRGs(RequestContext rq,
            String zoneClusterName, String rgs)
            throws CommandExecutionException, IOException {
        return executeOperation(rq, "unmanage", zoneClusterName, rgs, null);
    }


    /**
     * Change properties for the given zoneclustername and rgname combination.
     * @param rq RequestContext
     * @param zoneClusterName
     * @param rgName
     * @param newProperties
     * @param commit
     * @return
     * @throws CommandExecutionException
     * @throws java.io.IOException
     */
    public ExitStatus[] changeProperties(RequestContext rq,
            String zoneClusterName, String rgName, Map newProperties,
            boolean commit) throws CommandExecutionException, IOException {
        invalidateCache(commit);
        return getRgGroupMBean(rq).changeProperties(
                rgName, newProperties, zoneClusterName, commit);
    }



    /**
     * Switch the primaries of this resource group.
     *
     * @param rq RequestContext Object
     * @param name the name of the resource group
     * @param  newPrimaries  an array of node names.
     * @param zoneClusterName name of the zone cluster or null for current
     * cluster scope.
     * @return  an {@link ExitStatus[]} object.
     *
     * @throws  CommandExecutionException  if an underlying CLI error occured.
     * @throws java.io.IOException
     */
    public ExitStatus[] switchPrimaries(RequestContext rq, String name,
            String[] newPrimaries, String zoneClusterName)
            throws CommandExecutionException, IOException {
        invalidateCache(true);
        return getRgGroupMBean(rq).switchPrimaries(name, newPrimaries,
                zoneClusterName);
    }



    /**
     * Any RG event would lead to complete rgDataMap refresh.
     * We could handle this by maintaining a hashmap of zoneclustername and
     * individual 'dataValid' parameters.
     *
     * @param event
     */
    public void resourceGroupDataChanged(SysEventNotification event) {
        synchronized (syncObject) {
            // Need to update all the data, as there can be inter-cluster
            // rg affinities.
            Boolean tmpVal = new Boolean(false);
            Set names = rgDataValid.keySet();
            Iterator iter = names.iterator();
            while (iter.hasNext()) {
                rgDataValid.put(iter.next(), tmpVal);
            }
        }
        return;
    }


    private ExitStatus[] executeOperation(RequestContext rq,
            String operationName, String zoneClusterName, String RGList,
	    String nodeList)
            throws CommandExecutionException, IOException {
        ExitStatus[] status = null;
        RgmManagerMBean rgmMB = getRgmManagerMBean(rq);
        invalidateCache(true);
        if (operationName.equals("suspend")) {
            status = rgmMB.suspendRGs(zoneClusterName, RGList);
        } else if (operationName.equals("fastsuspend")) {
            status = rgmMB.fastsuspendRGs(zoneClusterName, RGList);
        } else if (operationName.equals("resume")) {
            status = rgmMB.resumeRGs(zoneClusterName, RGList);
        } else if (operationName.equals("remaster")) {
            status = rgmMB.remasterRGs(zoneClusterName, RGList);
        } else if (operationName.equals("restart")) {
            status = rgmMB.restartRGs(zoneClusterName, RGList);
        } else if (operationName.equals("bringOnline")) {
            status = rgmMB.bringOnlineRGs(zoneClusterName, RGList);
        } else if (operationName.equals("bringOffline")) {
            status = rgmMB.bringOfflineRGs(zoneClusterName, RGList);
        } else if (operationName.equals("manage")) {
            status = rgmMB.manageRGs(zoneClusterName, RGList);
        } else if (operationName.equals("unmanage")) {
            status = rgmMB.unmanageRGs(zoneClusterName, RGList);
        } else if (operationName.equals("forceDelete")) {
            status = rgmMB.forceDeleteRGs(zoneClusterName, RGList);
        } else if (operationName.equals("bringOfflinePerNode")) {
            status = rgmMB.pernodeBringOfflineRGs(zoneClusterName, RGList,
		    nodeList);
        } else if (operationName.equals("bringOnlinePerNode")) {
            status = rgmMB.pernodeBringOnlineRGs(zoneClusterName, RGList,
		    nodeList);
        } else if (operationName.equals("restartPerNode")) {
            status = rgmMB.pernodeRestartRGs(zoneClusterName, RGList,
		    nodeList);
        }
        return status;
    }


    private void invalidateCache(boolean commit) {
        synchronized (syncObject) {
            if (commit) {
                Boolean tmpVal = new Boolean(false);
                Set names = rgDataValid.keySet();
                Iterator iter = names.iterator();
                while (iter.hasNext()) {
                    rgDataValid.put(iter.next(), tmpVal);
                }
            }
        }
    }


    private RgGroupMBean getRgGroupMBean(RequestContext rq)
            throws IOException {
        RgGroupMBean mbean = null;
        if (rq != null) {
            mbean = (RgGroupMBean) MBeanModel.getMBeanProxy(
                    rq,
                    DataModel.getClusterEndpoint(rq),
                    RgGroupMBean.class,
                    null);
        } else {
            // Get RgGroupMBean using TrustedMBean Model
            mbean = (RgGroupMBean) TrustedMBeanModel.getMBeanProxy(
                    DataModel.getClusterEndpoint(rq),
                    RgGroupMBean.class,
                    null);
        }
        return mbean;
    }

    private RgmManagerMBean getRgmManagerMBean(RequestContext rq)
            throws IOException {
        RgmManagerMBean mbean = null;
        if (rq != null) {
            mbean = (RgmManagerMBean) MBeanModel.getMBeanProxy(
                    rq,
                    DataModel.getClusterEndpoint(rq),
                    RgmManagerMBean.class,
                    null);
        } else {
            // Get RgGroupMBean using TrustedMBean Model
            mbean = (RgmManagerMBean) TrustedMBeanModel.getMBeanProxy(
                    DataModel.getClusterEndpoint(rq),
                    RgmManagerMBean.class,
                    null);
        }
        return mbean;
    }

    private boolean isDataValid(String zoneClusterName) {
        Boolean val = new Boolean(false);
        Object testObj = rgDataValid.get(zoneClusterName);
        if (testObj != null) {
            val = (Boolean) testObj;
        }
        return val.booleanValue();
    }

    private void setDataValid(String zoneClusterName, boolean val) {
        rgDataValid.put(zoneClusterName, new Boolean(val));
    }

}
