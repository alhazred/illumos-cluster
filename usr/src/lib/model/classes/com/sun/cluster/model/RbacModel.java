/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006-2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)RbacModel.java	1.4	08/05/20 SMI"
 */

package com.sun.cluster.model;

import com.sun.cluster.common.MBeanModel;

import java.io.IOException;
import java.util.Set;
import java.util.HashMap;

import com.iplanet.jato.RequestContext;

import com.sun.cacao.rbac.RbacAuthorizationMBean;

/**
 * RbacModel - model for RBAC (Role-Based Access Control) data
 */
public class RbacModel extends ModelBase {

    private HashMap auths = new HashMap();

    private void rbacInit(RequestContext rq, String endpoint)
	throws IOException {
	synchronized (syncObject) {
	    if (auths.get(endpoint) == null) {
                RbacAuthorizationMBean mbean;
		mbean = (RbacAuthorizationMBean)
		    MBeanModel.getMBeanProxy(rq,
		    endpoint, RbacAuthorizationMBean.class, null);

		Set s = (Set)mbean.getAuths();
		auths.put(endpoint, s);
	    }
	}
    }

    // Look up the RBAC authorizations this user has
    public Set getAuths(RequestContext rq, String endpoint)
	throws IOException {
	Set authSet = (Set)auths.get(endpoint);
	if (authSet == null) {
	    rbacInit(rq, endpoint);
	    authSet = (Set)auths.get(endpoint);
	}

	return authSet;
    }

    public void test() {
    }
}
