/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)st_ff_client_linux.c	1.3	08/05/20 SMI"

/*
 * This file contains the implementation of the clst library targeted to be used
 * by Europa Linux farm nodes.
 *
 * These two routines, st_ff_arm() and st_ff_disarm(), provide an interface
 * to the failfast service implemented as a kernel module.
 *
 */
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <errno.h>
#include <sys/sc_syslog_msg.h>
#include <libintl.h>

#include "if_failfast.h"
#include <scxcfg/scxcfg.h>

int did = -1;
#define	SC_SYSLOG_FFMOD_TAG "Cluster.FailFast"

void st_ff_init(void) __attribute__((constructor));

int failfast_disabled = 0;

void
st_ff_init(void)
{
	scxcfg cfg;
	scxcfg_error error;
	char prop[FCFG_MAX_LEN];

	if (scxcfg_open(&cfg, &error) != FCFG_OK)
		return;

	if (scxcfg_getproperty_value(&cfg, NULL,
		"farm.properties.failfast_disabled", prop, &error) == FCFG_OK) {
		if (strcmp(prop, "yes") == 0)
			failfast_disabled = 1;
	}
	scxcfg_close(&cfg);
}

static int is_failfast_disabled()
{
	return (failfast_disabled);
}

/*
 * DESCRIPTION:
 *    This method creates and arms a failfast unit.
 *
 * INPUT:
 *    pname: name of the failfast unit.
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    0       : Succesful completion.
 *    EINVAL  : Invalid argument
 *    EACCES  : Enable to access failfast driver ...
 *
 */

int
st_ff_arm(char *pname)
{
	ffconf_t config;
	int millisecs = FF_TIMEOUT_INFINITE;
	sc_syslog_msg_handle_t handle;
	int cr = -1;

	if (is_failfast_disabled()) {
		return (0);
	}

	did = open("/dev/ffmodd", O_RDWR);
	if (did == -1) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_FFMOD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    gettext("could not open the failfast driver: %s"),
		    strerror(errno));
		    sc_syslog_msg_done(&handle);
		    goto finished;
	} else {

		strcpy(config.name, pname);
		config.mode = FFM_REBOOT;

		cr = ioctl(did, FFIOCSCONF, (void *)&config);
		if (cr == -1) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_FFMOD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    gettext("could not configure the"
			    " failfast driver: %s"),
			    strerror(errno));
			sc_syslog_msg_done(&handle);
			goto finished;
		}

		cr = ioctl(did, FFIOCARM, &millisecs);
		if (cr == -1) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_FFMOD_TAG, "");
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    gettext("could not arm the failfast driver: %s"),
			    strerror(errno));
			sc_syslog_msg_done(&handle);
			goto finished;
		}
	}
finished:
	return (cr);
}

/*
 * DESCRIPTION:
 *    This method disarms a failfast unit.
 *
 * INPUT:
 *    None
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    0 Success.
 *    Otherwise error
 *
 */

int
st_ff_disarm()
{
	int cr = 0;

	if (is_failfast_disabled()) {
		return (0);
	}

	if (did != -1) {
		cr = ioctl(did, FFIOCDISABLE, NULL);
	}

	return (cr);
}
