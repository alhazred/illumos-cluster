/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)st_ff_client.c	1.5	08/05/20 SMI"

/*
 * This file contains the implementation of the clst library targeted to be used
 * by Europa Solaris farm nodes.
 *
 * These two routines, st_ff_arm() and st_ff_disarm(), provide an interface
 * to the failfast service implemented as a kernel module.
 *
 */
#include <sys/os.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stropts.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/sc_syslog_msg.h>
#include <libintl.h>
#include "ffmod_ctl.h"
#include <scxcfg/scxcfg.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#include <rgm/sczones.h>

#define	SC_SYSLOG_FFMOD_TAG "Cluster.FailFast"
int did = -1;
minor_t	minor;

void st_ff_init(void);
#pragma init(st_ff_init)

int failfast_disabled = 0;

void
st_ff_init(void)
{
	scxcfg cfg;
	scxcfg_error error;
	char prop[FCFG_MAX_LEN];

	if (scxcfg_open(&cfg, &error) != FCFG_OK) {
		return;
	}

	if (scxcfg_getproperty_value(&cfg, NULL,
		"farm.properties.failfast_disabled", prop, &error) == FCFG_OK) {
		if (strcmp(prop, "yes") == 0) {
			failfast_disabled = 1;
		}
	}
	(void) scxcfg_close(&cfg);
}

static int is_failfast_disabled()
{
	return (failfast_disabled);
}

/*
 * DESCRIPTION:
 *    This method creates and arms a failfast unit.
 *
 * INPUT:
 *    pname: name of the failfast unit.
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    0       : Succesful completion.
 *    EINVAL  : Invalid argument
 *    EACCES  : Enable to access failfast driver ...
 *
 */

int
st_ff_arm(char *pname)
{
	failure_function_type failure_type;
	sc_syslog_msg_handle_t handle;
	int rc = 0;
	int dctl = -1;
	struct ffmod_ioctl ioc;
	char dev[80];
	static unsigned int sleeptime = 2; /* nb of secs to sleep btw stat's */
	static unsigned int maxsleep = 120; /* maximum nb of seconds to sleep */
	struct stat64 buf;
	unsigned int	cursleep;
#if SOL_VERSION >= __s10
	zoneid_t zoneid;
	char zone_name[ZONENAME_MAX + 1];
#endif

	if (is_failfast_disabled())
		return (0);

retry:
	dctl = open("/dev/ffmodctl", O_RDWR);
	if (dctl == -1) {
		if (errno == EBUSY) { /*lint !e746 */
			(void) sleep(1);
			goto retry;
		}
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_FFMOD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    gettext("Could not open the failfast control "
			"driver: %s"), strerror(errno));
		sc_syslog_msg_done(&handle);
		goto error;
	}

	if (strlen(pname) > MAXNAMELEN) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_FFMOD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    gettext("Failfast name too long"));
		sc_syslog_msg_done(&handle);
		goto error;
	}

	(void) strcpy(ioc.ffmod_name, pname);
#if SOL_VERSION >= __s10
	/* Append zone name to avoid name conflict */
	zoneid = getzoneid();
	(void) strcat(ioc.ffmod_name, "_");
	if (getzonenamebyid(zoneid, zone_name,
	    (size_t)(ZONENAME_MAX + 1)) != -1) {
		(void) strcat(ioc.ffmod_name, zone_name);
	} else {
		(void) sprintf(zone_name, "%d", zoneid);
	}
#endif
	ioc.ffmod_minor = 0;
	rc = ioctl(dctl, FFMOD_IOC_CREATE, &ioc);
	if (rc != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_FFMOD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    gettext("Could not create the failfast "
			"instance: %s"), strerror(errno));
		sc_syslog_msg_done(&handle);
		goto error;
	}
	(void) close(dctl);
	dctl = -1;

	minor = ioc.ffmod_minor;
	(void) sprintf(dev, "/dev/ffmod%d", ioc.ffmod_minor);

	/*
	 * Wait until dev creation
	 */
	cursleep = 0;
	while (cursleep < maxsleep) {
		if (stat64(dev, &buf) == -1) {
			(void) sleep(sleeptime);
			cursleep += sleeptime;
			continue;
		}
		break;
	}

	did = open(dev, O_RDWR);

	if (did == -1) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_FFMOD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    gettext("Could not open the failfast instance "
			"for minor %d : %s"),
		    minor, strerror(errno));
		sc_syslog_msg_done(&handle);
		goto error;
	}

	failure_type = SEND_MESSAGE_AND_PANIC;

	rc = ioctl(did, FFMOD_IOC_SCONF,
	    (void *)&failure_type);
	if (rc != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_FFMOD_TAG, "");
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    gettext("Could not configure the failfast instance"),
		    pname);
		sc_syslog_msg_done(&handle);
		goto error;
	}

	rc = ioctl(did, FFMOD_IOC_ARM);
	if (rc != 0)
		goto error;

	return (0);
error:
	if (dctl != -1)
		(void) close(dctl);
	if (did != -1)
		(void) close(did);
	(void) sc_syslog_msg_initialize(&handle,
	    SC_SYSLOG_FFMOD_TAG, "");
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
	    gettext("Could not arm the failfast driver for %s"),
	    pname);
	sc_syslog_msg_done(&handle);
	return (EACCES);
}

/*
 * DESCRIPTION:
 *    This method disarms a failfast unit.
 *
 * INPUT:
 *    None
 *
 * OUTPUT:
 *    None
 *
 * RETURN:
 *    0 : Succesful completion.
 *    Otherwise error
 */

int
st_ff_disarm()
{
	int rc = 0;
	struct ffmod_ioctl ioc;
	int dctl = -1;

	if (is_failfast_disabled())
		return (0);

	if (did != -1) {
		rc = ioctl(did, FFMOD_IOC_DISARM);
		if (rc != 0) {
			goto error;
		}
		(void) close(did);

		dctl = open("/dev/ffmodctl", O_RDWR);
		if (dctl == -1) {
			goto error;
		}

		ioc.ffmod_minor = minor;
		rc = ioctl(dctl, FFMOD_IOC_DELETE, &ioc);
		if (rc != 0) {
			goto error;
		}
		(void) close(dctl);
	}
	return (0);
error:
	return (rc);
}
