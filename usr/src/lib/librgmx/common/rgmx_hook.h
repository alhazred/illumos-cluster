/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * rgmx_hook.h
 *
 *	Europa hooks to implement RGM extension for Farm management.
 */

#ifndef	_RGMX_HOOK_H
#define	_RGMX_HOOK_H

#pragma ident	"@(#)rgmx_hook.h	1.3	08/05/20 SMI"

#include <sys/types.h>
#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef uint_t hook_mode_t;

#define	RGMX_HOOK_DISABLED	0
#define	RGMX_HOOK_ENABLED	1

/*
 * ATTENTION: this enum MUST always be in sync with the two arrays
 * defined in rgmx_hook.cc (rgmx_hook[] and rgmx_hook_name[]).
 */
typedef enum hook_id {
	rgmx_hook_cfg_init = 0,
	rgmx_hook_set_maxnodes,
	rgmx_hook_startup,
	rgmx_hook_rpc_start
#ifndef EUROPA_FARM
, rgmx_hook_update_configured_nodes,
	rgmx_hook_update_membership,
	rgmx_hook_trigger_farm_fencing,
	rgmx_hook_get_nodename,
	rgmx_hook_get_nodeid,
	rgmx_hook_scha_farm_get,
	rgmx_hook_scrgadm_rs_validate,
	rgmx_hook_scha_control_checkall,
	rgmx_hook_set_timestamp,
	rgmx_hook_latch_intention,
	rgmx_hook_process_intention,
	rgmx_hook_unlatch_intention,
	rgmx_hook_xmit_state,
	rgmx_hook_set_pres,
	rgmx_hook_am_i_joining,
	rgmx_hook_read_ccr,
	rgmx_hook_run_boot_meths,
	rgmx_hook_chg_mastery,
	rgmx_hook_r_restart,
	rgmx_hook_clear_flag,
	rgmx_hook_ack_start,
	rgmx_hook_notify_dependencies_resolved,
	rgmx_hook_fetch_restarting_flag,
	rgmx_hook_retrieve_lni_mappings,
	rgmx_hook_send_lni_mappings,
	rgmx_hook_reclaim_lnis
#endif
} hook_id_t;

typedef void (*rgmx_hook_function_t)(va_list ap);

/*
 * RGMX hook functions prototypes.
 */
void rgmx_cfg_init(va_list ap);
void rgmx_set_maxnodes(va_list ap);
void rgmx_startup(va_list ap);
void rgmx_rpc_start(va_list ap);
#ifndef EUROPA_FARM
void rgmx_update_configured_nodes(va_list ap);
void rgmx_update_membership(va_list ap);
void rgmx_trigger_farm_fencing(va_list ap);
void rgmx_get_nodename(va_list ap);
void rgmx_get_nodeid(va_list ap);
void rgmx_scha_farm_get(va_list ap);
void rgmx_scrgadm_rs_validate(va_list ap);
void rgmx_scha_control_checkall(va_list ap);
void rgmx_set_timestamp(va_list ap);
void rgmx_latch_intention(va_list ap);
void rgmx_process_intention(va_list ap);
void rgmx_unlatch_intention(va_list ap);
void rgmx_xmit_state(va_list ap);
void rgmx_set_pres(va_list ap);
void rgmx_am_i_joining(va_list ap);
void rgmx_read_ccr(va_list ap);
void rgmx_run_boot_meths(va_list ap);
void rgmx_chg_mastery(va_list ap);
void rgmx_r_restart(va_list ap);
void rgmx_clear_flag(va_list ap);
void rgmx_ack_start(va_list ap);
void rgmx_notify_dependencies_resolved(va_list ap);
void rgmx_fetch_restarting_flag(va_list ap);
void rgmx_retrieve_lni_mappings(va_list ap);
void rgmx_send_lni_mappings(va_list ap);
void rgmx_reclaim_lnis(va_list ap);
#endif

void rgmx_hook_init();
bool rgmx_hook_enabled();
hook_mode_t rgmx_hook_call(hook_id_t hook_id, ...);

#ifdef EUROPA_FARM
inline boolean_t
clconf_is_farm_mgt_enabled()
{
	return (B_TRUE);
}
#endif

#ifdef __cplusplus
}
#endif

#endif /* _RGMX_HOOK_H */
