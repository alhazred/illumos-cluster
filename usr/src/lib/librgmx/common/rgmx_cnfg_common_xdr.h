/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * rgmx_cnfg_common_xdr.h
 *
 * EUROPA specific file.
 *
 * function prototypes and global variable declarations for rgm module
 * external interfaces
 *
 */

#ifndef	_RGMX_CNFG_COMMON_XDR_H
#define	_RGMX_CNFG_COMMON_XDR_H

#pragma ident	"@(#)rgmx_cnfg_common_xdr.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * XDR prototypes
 */
bool_t xdr_rgm_rt_t(register XDR *xdrs, rgm_rt_t *objp);

bool_t xdr_rgm_rg_t(register XDR *xdrs, rgm_rg_t *objp);

bool_t xdr_rgm_resource_t(register XDR *xdrs, rgm_resource_t *objp);

bool_t xdr_namelist_t(register XDR *xdrs, namelist_t *objp);

bool_t xdr_rgm_obj_type_t(register XDR *xdrs, rgm_obj_type_t *objp);

#ifdef __cplusplus
}
#endif


#endif /* _RGMX_CNFG_COMMON_XDR_H */
