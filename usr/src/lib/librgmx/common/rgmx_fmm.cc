//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgmx_fmm.cc	1.5	08/07/21 SMI"

#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <errno.h>
#include <rpc/rpc.h>
#include <netdb.h>
#include <poll.h>

#include <saf_clm.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>
#include <rgm_state.h>
#include <rgm_threads.h>

#include <rgm_logical_node_manager.h>

#include <dlfmm.h>

static void *instance;
static safclm_api_t api;
static LogicalNodeset *farm_nodeset;

//
// FCMM callback function
//
void
fmm_callback_func(SaClmClusterNotificationT *notificationBuffer,
    SaUint32T numberOfItems, SaUint32T numberOfMembers, SaUint64T viewNumber,
    SaErrorT error)
{
	unsigned int i = 0;

	ucmm_print("RGM", "fmm thread: in callback with view number %llu, "
	    "numberOfMembers %d\n", viewNumber, numberOfMembers);
	if (error != SA_OK)
		return;

	farm_nodeset->reset();

	rgm_lock_state();
	for (i = 0; i < numberOfItems; i++) {
		if ((notificationBuffer[i].clusterNode.nodeId != 0) &&
		    (notificationBuffer[i].clusterNode.member)) {
			farm_nodeset->addLni(
				notificationBuffer[i].clusterNode.nodeId);
		    }
	}

	rgm_unlock_state();

	rgm_reconfig_helper(*farm_nodeset, B_FALSE);
} /*lint !e1746 */

void
clusterNodeGetCallback(SaInvocationT, SaClmClusterNodeT *,
	SaErrorT)
{
}


//
// FCMM thread
//
void *
fmm_thread_start(void *arg)
{
	struct pollfd poll_fd;
	sc_syslog_msg_handle_t handle;
	boolean_t fmmup = B_FALSE;
	SaClmCallbacksT callbacks;
	SaVersionT version;
	SaSelectionObjectT selectionObject;
	SaClmClusterNotificationT notificationBuffer[MAXNODES];
	SaUint32T numberOfItems = MAXNODES;
	SaClmHandleT clm_handle;
	SaErrorT result = SA_OK;
	unsigned int count = 0;

	farm_nodeset = new LogicalNodeset();

	//
	// First of all, load saf Clm (libfmm) dynamic library.
	//
	if (safclm_load_module(&instance, &api) == -1) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
			MESSAGE, "fatal: safclm_load_module() failed\n");
		sc_syslog_msg_done(&handle);
		abort();
	}

	callbacks.saClmClusterNodeGetCallback = clusterNodeGetCallback;
	callbacks.saClmClusterTrackCallback = fmm_callback_func;
	version.releaseCode = 'A';
	version.major = 0x01;
	version.minor = 0xff;

	//
	// We try to get the FMM memberhsip immediatly,
	// do not wait for UCMM to have triggered a rgm_reconfig
	// and elected the president.
	// rgm_reconfig() will take care to start real stuff only
	// when it has received membership from both FMM and UCMM.
	//

	// Connect to FCMM
	do {
		result = api.saClmInitialize(&clm_handle, &callbacks, &version);
		if (result != SA_OK) {
			count++;
			(void) sleep(1);
		} else {
			fmmup = B_TRUE;
		}
	} while ((!fmmup) && (count < 30));
	if (!fmmup) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, "fatal: saClmInitialize error %d", result);
		sc_syslog_msg_done(&handle);
		abort();
	}

	ucmm_print("RGM", "starting fmm thread\n");

	// Get file descriptor to poll
	result = api.saClmSelectionObjectGet(clm_handle, &selectionObject);
	if (result != SA_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, "fatal: saClmSelectionObjectGet error %d", result);
		sc_syslog_msg_done(&handle);
		abort();
	}

	farm_nodeset->reset();

	bzero(notificationBuffer, numberOfItems *
	    sizeof (SaClmClusterNotificationT));
	result = api.saClmClusterTrackStart(clm_handle, SA_TRACK_CURRENT,
	    notificationBuffer, numberOfItems);

	if (result != SA_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, "fatal: saClmClusterTrackStart error %d", result);
		sc_syslog_msg_done(&handle);
		abort();
	}

	ucmm_print("RGM",
	    "fmm thread: reconfig for initial membership done, polling\n");

	result = api.saClmClusterTrackStart(clm_handle, SA_TRACK_CHANGES,
	    notificationBuffer, numberOfItems);
	if (result != SA_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR,
		    MESSAGE, "fatal: saClmClusterTrackStart error %d", result);
		sc_syslog_msg_done(&handle);
		abort();
	}

	while (1) {

		//
		// Wait for change notification
		//

		poll_fd.fd = (int)selectionObject;
		poll_fd.events = POLLIN;

		switch (poll((struct pollfd *)&poll_fd, 1, -1)) {

		case 0:
			break;
		case 1:
			if ((!(poll_fd.revents & POLLIN)) ||
			    (poll_fd.revents & POLLHUP) ||
			    (poll_fd.revents & POLLERR) ||
			    (poll_fd.revents & POLLNVAL)) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) sc_syslog_msg_log(handle,
				    LOG_ERR, MESSAGE,
				    "fatal: fmm polling error");
				sc_syslog_msg_done(&handle);
				return (arg);
			}

			// Got something
			result = api.saClmDispatch(clm_handle, SA_DISPATCH_ALL);
			if (result != SA_OK) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE, "fatal: saClmDispatch error %d",
				    result);
				sc_syslog_msg_done(&handle);
				return (arg);
			}

			break;

		case -1:
		default:
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, "fatal: fmm polling error");
			sc_syslog_msg_done(&handle);
			return (arg);
			break;

		}
	}

	/* NOTREACHED */
	return (arg);
}
