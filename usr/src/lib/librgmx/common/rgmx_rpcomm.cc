/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgmx_rpcomm.cc	1.3	08/05/20 SMI"

/*
 * rgmx_rpcomm_solaris.c
 *
 * Set of functions to create a client handle before making a RPC call or
 * create a server handle before receiving calls. Used by Europa RPC
 * servers to manage the communication between Server/Farm RGMs.
 *
 * Solaris specific version based on standard Sun RPC and TLI interfaces.
 * Linux specific version based on standard Sun RPC and socket interfaces.
 */

#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define	PORTMAP
#include <rpc/rpc.h>
#include <sys/sc_syslog_msg.h>
#include <rgmx_proto.h>

#define	RGMX_PORT	6500

#define	MS	1L
#define	SECONDS (1000 * MS)
#define	MINUTES (60 * SECONDS)
#define	HOURS	(60 * MINUTES)
#define	DAYS	(24 * HOURS)

#ifdef Solaris

int _rpcpmstart = 0;

/*
 * Create a RPC client handle for a tcp/ip connection.
 * A Transport endpoint is created and is connected to remote_addr/RGMX_PORT.
 * If keepalive is TRUE, then the TCP keepalive option is carried
 * to the TCP connection. The TCP keepalive will be sent periodically
 * (solaris/tcp_keepalive_interval). If for more than (tcp_keepalive_interval
 * + tcp_ip_abort_interval) we have not heard anything, the connection is
 * killed.
 */
CLIENT *
rgmx_clnt_create(char *remote_addr,		/* remote server address */
    char *program_name,		/* program name */
    rpcprog_t program_num,	/* program number */
    rpcvers_t version,		/* version */
    bool_t keepalive)		/* keepalive option */
{
	int fd;
	CLIENT *clnt;
	struct sockaddr_in raddr;
	struct timeval timeout;
	struct netconfig *nconf;
	struct netbuf svcaddr;
	struct t_optmgmt oreq, ores;
	struct opthdr *topt;
	char buf[128];
	int *ip;
	sc_syslog_msg_handle_t handle;

	/*
	 * Get the network information for TCP transport.
	 */
	nconf = getnetconfigent("tcp");
	if (nconf == (struct netconfig *)NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_clnt_create (%s/%s): getnetconfigent "
			"failed - error %s",
			program_name, remote_addr, strerror(errno));
		sc_syslog_msg_done(&handle);
		return (NULL);
	}

	/*
	 * Set the keepalive option if requested by the caller.
	 * Used to handle the unresponsiveness of a remote RPC server.
	 */
	fd = t_open(nconf->nc_device, O_RDWR, NULL);
	if (fd == -1) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_clnt_create (%s/%s): t_open "
			"failed - error %s",
			program_name, remote_addr, t_strerror(t_errno));
		sc_syslog_msg_done(&handle);

		freenetconfigent(nconf);
		return (NULL);
	}

	ip = (int *)(buf + sizeof (struct opthdr));
	*ip = 1;
	topt = (struct opthdr *)buf;
	topt->level =  SOL_SOCKET;
	topt->name = SO_KEEPALIVE;
	topt->len = sizeof (int);
	oreq.flags = T_NEGOTIATE;
	oreq.opt.len = sizeof (struct opthdr) + sizeof (int);
	oreq.opt.buf = buf;

	ores.flags = 0;
	ores.opt.buf = buf;
	ores.opt.maxlen = 128;
	if (t_optmgmt(fd, &oreq, &ores) < 0 ||
	    ores.flags != T_SUCCESS) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_clnt_create (%s/%s): t_optmgmt "
			"(keepalive) failed - error %s",
			program_name, remote_addr, t_strerror(t_errno));
		sc_syslog_msg_done(&handle);

		freenetconfigent(nconf);
		t_close(fd);
		return (NULL);
	}

	/*
	 * Create now a RPC client handle / TCP connection.
	 * Use the default send and receive buffer sizes.
	 */
	raddr.sin_family = AF_INET;
	raddr.sin_addr.s_addr = inet_addr(remote_addr);
	raddr.sin_port = htons(RGMX_PORT);

	(void) memset(&svcaddr, 0, sizeof (svcaddr));
	svcaddr.len = svcaddr.maxlen = sizeof (raddr);
	svcaddr.buf = (char *)malloc(svcaddr.len);
	memcpy(svcaddr.buf, (char *)&raddr, svcaddr.len);

	clnt = (CLIENT *)clnt_tli_create(fd, nconf, &svcaddr, program_num,
				version, 0, 0);
	if (clnt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_clnt_create (%s/%s): clnt_tli_create "
			"failed - error %s",
			program_name, remote_addr, clnt_spcreateerror(""));
		sc_syslog_msg_done(&handle);

		freenetconfigent(nconf);
		t_close(fd);
		return (NULL);
	}

	/*
	 * Set a large RPC timeout (1 full day).
	 * When keepalive is used, unresponsiveness cases are managed
	 * by the keepalive probe. This is typically the case when a Server
	 * node sends a request to a unresponsive Farm node. The connection
	 * is killed by the TCP stack and the Server fences the unresponsive
	 * node from the cluster.
	 *
	 * When keepalive is not used, wait for a while. This is typically
	 * the case when a Farm node sends a request to a unresponsive Server
	 * node. The Farm node waits for a new Server node to come up. The
	 * new Server node is in charge of resetting all the pending
	 * connections.
	 */
	timeout.tv_usec = 0;
	timeout.tv_sec = 1 * DAYS;
	if (clnt_control(clnt, CLSET_TIMEOUT, (caddr_t)&timeout) == FALSE) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_clnt_create (%s/%s): clnt_control "
			" (CLSET_TIMEOUT) failed - error %s",
			program_name, remote_addr, strerror(errno));
		sc_syslog_msg_done(&handle);

		freenetconfigent(nconf);
		t_close(fd);
		return (NULL);
	}

	return (clnt);
}

/*
 * Creates, registers, and returns a (rpc) tcp based transporter.
 * A transport endpoint is created, and bound to the local_addr/RGMX_PORT port.
 * The routine then starts a tcp listener on the endpoint's associated port.
 */
SVCXPRT *
rgmx_svc_create(char *local_addr)	/* local server address */
{
	int fd;
	SVCXPRT *transp;
	struct sockaddr_in laddr;
	struct netconfig *nconf;
	struct t_bind tbind;
	struct t_optmgmt oreq, ores;
	struct opthdr *topt;
	char buf[128];
	int *ip;
	sc_syslog_msg_handle_t handle;

	/*
	 * Get the network information for TCP transport.
	 */
	nconf = getnetconfigent("tcp");
	if (nconf == (struct netconfig *)NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_svc_create (%s): getnetconfigent "
			"failed - error %s",
			local_addr, strerror(errno));
		sc_syslog_msg_done(&handle);
		return (NULL);
	}
	fd = t_open(nconf->nc_device, O_RDWR, NULL);
	if (fd == -1) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_svc_create (%s/%s): t_open "
			"failed - error %s",
			local_addr, t_strerror(t_errno));
		sc_syslog_msg_done(&handle);

		freenetconfigent(nconf);
		return (NULL);
	}

	/*
	 * Set SO_REUSEADDR.
	 * tells the kernel that even if this port is busy (in
	 * the TIME_WAIT state), go ahead and reuse it anyway.
	 * It is useful if the server has been shut down, and then
	 * restarted right away while sockets are still active on its port.
	 */
	ip = (int *)(buf + sizeof (struct opthdr));
	*ip = 1;
	topt = (struct opthdr *)buf;
	topt->level =  SOL_SOCKET;
	topt->name = SO_REUSEADDR;
	topt->len = sizeof (int);
	oreq.flags = T_NEGOTIATE;
	oreq.opt.len = sizeof (struct opthdr) + sizeof (int);
	oreq.opt.buf = buf;

	ores.flags = 0;
	ores.opt.buf = buf;
	ores.opt.maxlen = 128;
	if (t_optmgmt(fd, &oreq, &ores) < 0 ||
	    ores.flags != T_SUCCESS) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_WARNING, MESSAGE,
			"rgmx_svc_create (%s): t_optmgmt "
			"(reuseaddr) failed - error %s",
			local_addr, t_strerror(t_errno));
		sc_syslog_msg_done(&handle);
	}

	/*
	 * Create an RPC server handle on TCP transport.
	 * Bind the transport endpoint to the local address.
	 */
	laddr.sin_family = AF_INET;
	laddr.sin_addr.s_addr = inet_addr(local_addr);
	laddr.sin_port = htons(RGMX_PORT);

	(void) memset(&(tbind.addr), 0, sizeof ((tbind.addr)));
	(tbind.addr).len = (tbind.addr).maxlen = sizeof (laddr);
	(tbind.addr).buf = (char *)malloc((tbind.addr).len);
	memcpy((tbind.addr).buf, (char *)&laddr, (tbind.addr).len);

	if ((transp = (SVCXPRT *)svc_tli_create(fd,
				nconf, &tbind, 0, 0)) == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_svc_create (%s): svc_tli_create "
			"failed - error %s",
			local_addr, strerror(errno));
		sc_syslog_msg_done(&handle);

		freenetconfigent(nconf);
		free((tbind.addr).buf);
		return (NULL);
	}

	freenetconfigent(nconf);
	free((tbind.addr).buf);

	return (transp);
}

/*
 * Associate the program with the dispatch  procedure.
 * Protocol is set to 0, as we don't need to register
 * the service with  the  portmap  service.
 */
int
rgmx_svc_register(SVCXPRT *transp,
    svc_program_t svc_program,
    char *program_name,		/* program name */
    rpcprog_t program_num,	/* program number */
    rpcvers_t version)		/* version */
{
	sc_syslog_msg_handle_t handle;

	if (!svc_register(transp, program_num, version,
			svc_program, 0)) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_svc_register (%s): svc_register "
			"failed - error %s",
			program_name, strerror(errno));
		sc_syslog_msg_done(&handle);

		return (-1);
	}

	return (0);
}

/*
 * Release RPC/TCP connection.
 */
void
rgmx_clnt_destroy(CLIENT *clnt)
{
	/*
	 * Destroy client handle and close the according transport endpoint
	 * file descriptor.
	 * The endpoint file descriptor has been stored in the cl_private
	 * area or the client handle (first integer).
	 */
	int fd = *(int *)(clnt->cl_private);
	t_close(fd);
	clnt_destroy(clnt);
}
#endif // Solaris

#ifdef linux
/*
 * Create a RPC client handle for a tcp/ip connection.
 * A Transport endpoint is created and is connected to remote_addr/RGMX_PORT.
 * If keepalive is TRUE, then the TCP keepalive option is carried
 * to the TCP connection. The TCP keepalive will be sent periodically
 * (solaris/tcp_keepalive_interval). If for more than (tcp_keepalive_interval
 * + tcp_ip_abort_interval) we have not heard anything, the connection is
 * killed.
 */
CLIENT *
rgmx_clnt_create(char *remote_addr,		/* remote server address */
    char *program_name,		/* program name */
    rpcprog_t program_num,	/* program number */
    rpcvers_t version,		/* version */
    bool_t keepalive)		/* keepalive option */
{
	int sockfd;
	CLIENT *clnt;
	struct sockaddr_in raddr;
	struct timeval timeout;
	int on = 1;
	sc_syslog_msg_handle_t handle;

	/*
	 * Set the keepalive option if requested by the caller.
	 * Used to handle the unresponsiveness of a remote RPC server.
	 */
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (socket < 0) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_clnt_create (%s/%s): socket "
			"failed - error %s",
			program_name, remote_addr, strerror(errno));
		sc_syslog_msg_done(&handle);
	}

	if (keepalive == TRUE) {
		if (setsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, &on,
		    sizeof (on)) < 0) {
			(void) sc_syslog_msg_initialize(&handle,
				SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
				"rgmx_clnt_create (%s/%s): setsockopt "
				"(keepalive) failed - error %s",
				program_name, remote_addr, strerror(errno));
			sc_syslog_msg_done(&handle);
			return (NULL);
		}
	}

	/*
	 * Open a TCP connection with the remote RPC server.
	 */
	raddr.sin_family = AF_INET;
	raddr.sin_addr.s_addr = inet_addr(remote_addr);
	raddr.sin_port = htons(RGMX_PORT);
	if (connect(sockfd, (struct sockaddr*)&raddr, sizeof (raddr)) == -1) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_clnt_create (%s/%s): connect "
			"failed - error %s",
			program_name, remote_addr, strerror(errno));
		sc_syslog_msg_done(&handle);
		return (NULL);
	}

	/*
	 * Create now a RPC client handle for this TCP connection.
	 * Use the default send and receive buffer sizes.
	 */
	clnt = (CLIENT *)clnttcp_create(&raddr, program_num, version,
				&sockfd, 0, 0);
	if (clnt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_clnt_create (%s/%s): clnttcp_create "
			"failed - error %s",
			program_name, remote_addr, clnt_spcreateerror(""));
		sc_syslog_msg_done(&handle);
		return (NULL);
	}

	/*
	 * Set a large RPC timeout (1 full day).
	 * When keepalive is used, unresponsiveness cases are managed
	 * by the keepalive probe. This is typically the case when a Server
	 * node sends a request to a unresponsive Farm node. The connection
	 * is killed by the TCP stack and the Server fences the unresponsive
	 * node from the cluster.
	 *
	 * When keepalive is not used, wait for a while. This is typically
	 * the case when a Farm node sends a request to a unresponsive Server
	 * node. The Farm node waits for a new Server node to come up. The
	 * new Server node is in charge of resetting all the pending
	 * connections.
	 */
	timeout.tv_usec = 0;
	timeout.tv_sec = 1 * DAYS;
	if (clnt_control(clnt, CLSET_TIMEOUT, (caddr_t)&timeout) == FALSE) {
		return (NULL);
	}

	return (clnt);
}

/*
 * Creates, registers, and returns a (rpc) tcp based transporter.
 * A transport endpoint is created, and bound to the local_addr/RGMX_PORT port.
 * The routine then starts a tcp listener on the endpoint's associated port.
 */
SVCXPRT *
rgmx_svc_create(char *local_addr)	/* local server address */
{
	int sockfd;
	SVCXPRT *transp;
	struct sockaddr_in laddr;
	int on = 1;
	sc_syslog_msg_handle_t handle;

	/*
	 * Bind the socket to the local RPC server address.
	 */
	laddr.sin_family = AF_INET;
	laddr.sin_addr.s_addr = inet_addr(local_addr);
	laddr.sin_port = htons(RGMX_PORT);

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_svc_create (%s): socket "
			"failed - error %s",
			local_addr, strerror(errno));
		sc_syslog_msg_done(&handle);
		return (NULL);
	}

	/*
	 * Set SO_REUSEADDR.
	 * tells the kernel that even if this port is busy (in
	 * the TIME_WAIT state), go ahead and reuse it anyway.
	 * It is useful if the server has been shut down, and then
	 * restarted right away while sockets are still active on its port.
	 */
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &on,
	    sizeof (on)) < 0) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_WARNING, MESSAGE,
			"rgmx_svc_create (%s/%s): setsockopt "
			"(reuseaddr) failed - error %s",
			local_addr, strerror(errno));
		sc_syslog_msg_done(&handle);
	}

	if (bind(sockfd, (struct sockaddr*)&laddr, sizeof (laddr)) < 0) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_svc_create (%s): bind "
			"failed - error %s",
			local_addr, strerror(errno));
		sc_syslog_msg_done(&handle);
		return (NULL);
	}
	if ((transp = (SVCXPRT *)svctcp_create(sockfd, 0, 0)) == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_svc_create (%s): svctcp_create "
			"failed - error %s",
			local_addr, clnt_spcreateerror(""));
		sc_syslog_msg_done(&handle);
		return (NULL);
	}

	return (transp);
}

/*
 * Associate the program with the dispatch  procedure.
 * Protocol is set to 0, as we don't need to register
 * the service with  the  portmap  service.
 */
int
rgmx_svc_register(SVCXPRT *transp,
    void (*svc_program)(struct svc_req *, SVCXPRT *),
    char *program_name,		/* program name */
    rpcprog_t program_num,	/* program number */
    rpcvers_t version)		/* version */
{
	sc_syslog_msg_handle_t handle;

	if (!svc_register(transp, program_num, version,
			svc_program, 0)) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_svc_register (%s): svc_register "
			"failed - error %s",
			program_name, strerror(errno));
		sc_syslog_msg_done(&handle);

		return (-1);
	}

	return (0);
}

/*
 * Release RPC/TCP connection.
 */
void
rgmx_clnt_destroy(CLIENT *clnt)
{
	/*
	 * Destroy client handle and close the according transport endpoint
	 * file descriptor.
	 * The endpoint file descriptor has been stored in the cl_private
	 * area or the client handle (first integer).
	 */
	int fd = *(clnt->cl_private);
	close(fd);
	clnt_destroy(clnt);
}
#endif // linux
