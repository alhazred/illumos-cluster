/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * rgmx_util.cc
 *
 *	Europa specific rgmd utility functions
 */

#pragma ident	"@(#)rgmx_util.cc	1.4	08/05/20 SMI"

#include <rgm/rgm_msg.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>

#include <rgm_logical_nodeset.h>
#include <rgm_logical_node_manager.h>

#ifndef EUROPA_FARM
LogicalNodeset	*current_farm_membership;
LogicalNodeset	*current_server_membership;
LogicalNodeset  *farm_configured_nodes;
LogicalNodeset  *last_farm_membership;

#endif

//
// Set the maximum number of physical nodes
// to maximum number of server nodes (MAXNODES) +
// maximum number of farm nodes.
//
void
rgmx_set_maxnodes(va_list ap)
{
	sc_syslog_msg_handle_t handle;
	uint_t maxfarmnodes;
	uint_t initialmaxlni;

	// Set maxnode & maxlni LogicalNodeset static members.
	maxfarmnodes = rgmx_cfg_getmaxnodes();
	LogicalNodeset::setMaxNode(MAXNODES + maxfarmnodes);

	initialmaxlni = MAXNODES + maxfarmnodes + 32;
	LogicalNodeset::setMaxLni(initialmaxlni);
	ucmm_print("RGM", NOGET("Max Server nodes = %d, "
				"Max Farm nodes   = %d, "
				"Initial Max lnis = %d\n"),
				MAXNODES, maxfarmnodes, initialmaxlni);
}

#ifndef EUROPA_FARM
//
// Update set of configured nodes.
// Add all the configured Farm nodes to Rgm_state->static_membership.
// Return 0 in case of success, -1 otherwise.
//
void
rgmx_update_configured_nodes(va_list ap)
{
	LogicalNodeset *conf_ns;
	int *retval;
	int nb_farm_nodes;
	f_property_t *farm_node_list = NULL;
	f_property_t *farm_node;
	rgm::lni_t lni;
	char *ns_buffer;

	conf_ns = va_arg(ap, LogicalNodeset *);
	retval = va_arg(ap, int *);

	*retval = 0;

	/*
	 * Get all farm nodes.
	 */
	if (rgmx_cfg_getfarmnodes(&farm_node_list,
		&nb_farm_nodes) == -1) {
		*retval = -1;
	} else {
		if (!farm_configured_nodes) {
			farm_configured_nodes = new LogicalNodeset();
			last_farm_membership = new LogicalNodeset();
			last_farm_membership->reset();
		}
		if (nb_farm_nodes != 0) {
			/*
			 * Reset previous list.
			 */
			farm_configured_nodes->reset();
			for (farm_node = farm_node_list;
				farm_node != NULL;
				farm_node = farm_node->next) {
				lni = (rgm::lni_t)atoi(farm_node->value);
				farm_configured_nodes->addLni(lni);

				LogicalNode *ln = lnManager->findLogicalNode(
				    (sol::nodeid_t)lni, NULL);
				if (ln) ln->updateCachedNames();
			}
			*conf_ns |= *farm_configured_nodes;
		}
	}

	if (conf_ns->display(ns_buffer) == 0) {
		ucmm_print("RGM", NOGET("static membership ns=%s\n"),
			ns_buffer);
		free(ns_buffer);
	}

	if (farm_node_list != NULL) {
		rgmx_cfg_freelist(farm_node_list);
	}

}

void
rgmx_init_membership()
{
	current_server_membership = new LogicalNodeset();
	current_farm_membership = new LogicalNodeset();

}

//
// Merge Server & Farm nodesets.
//
void
rgmx_update_membership(va_list ap)
{
	boolean_t isucmm;
	LogicalNodeset *ns;
	char *ns_buffer;

	ns = va_arg(ap, LogicalNodeset *);
	isucmm = va_arg(ap, boolean_t);

	if (isucmm) {
		//
		// Server membership.
		//
		*current_server_membership = *ns;
		*ns |= *current_farm_membership;
	} else {
		//
		// Farm membership.
		//
		*current_farm_membership = *ns;
		*ns |= *current_server_membership;
	}

	if (ns->display(ns_buffer) == 0) {
		ucmm_print("RGM", NOGET("unified membership ns=%s\n"),
			ns_buffer);
		free(ns_buffer);
	}

	//
	// Don't do the first real rgm_reconfig
	// until we have received the membership from UCMM.
	//
	if (current_server_membership->isEmpty()) {
		ns->reset();
	}
}

//
// in_current_farm_membership
//
// return TRUE if the specified node is in the current Farm membership
//
boolean_t
in_current_farm_membership(sol::nodeid_t node)
{
	if (current_farm_membership->containLni((rgm::lni_t)node)) {
		return (B_TRUE);
	}

	return (B_FALSE);
}

//
// Trigger fencing operation on the Farm membership.
//
void
rgmx_trigger_farm_fencing(va_list ap)
{
	(void) rgmx_update_farm_fencing(*current_farm_membership);
}

//
// rgmx_get_nodename
//
// Returns the nodename string of the given farm node id.
// Caller must free allocated memory.
//
void
rgmx_get_nodename(va_list ap)
{
	sol::nodeid_t n;
	char **nodenamep;

	n = va_arg(ap, sol::nodeid_t);
	nodenamep = va_arg(ap, char **);
	rgmx_cfg_getnodename((scxcfg_nodeid_t)n, nodenamep);
}

//
// rgmx_get_nodeid
//
// Returns the node id of a given nodename.
//
// If the "nodename" is numeric, it is assumed to actually be a nodeid.
// And, if the ID is found in the configuration, it is returned in
// "nodeidp" upon success.
//
void
rgmx_get_nodeid(va_list ap)
{
	char *nodename;
	char **nodenamep;
	scxcfg_nodeid_t nodeid = 0;
	scxcfg_nodeid_t *nodeidp;
	int rc;
	char *s;

	nodenamep = va_arg(ap, char **);
	nodeidp = va_arg(ap, scxcfg_nodeid_t *);

	// Check to see if nodename is a nodeid (number > 0)
	for (s = *nodenamep;  *s;  ++s)
		if (!isdigit(*s))
			break;
	if (*s == '\0') {
		nodeid = (scxcfg_nodeid_t)atoi(*nodenamep);
		if (nodeid < 1) {
			*nodeidp = 0;
			return;
		}
	}

	if (nodeid != 0) {
		if (rgmx_cfg_getnodename(nodeid, &nodename) != 0)
			*nodeidp = 0;
		else {
			free(nodename);
			*nodeidp = nodeid;
		}
	} else {
		if (rgmx_cfg_getnodeid(*nodenamep, nodeidp) != 0)
			*nodeidp = 0;
	}

}
#endif // !EUROPA_FARM

#ifdef EUROPA_FARM
//
// RPC style array of chars to string sequence
//
void
array_to_strseq_2(strarr_list *str_list, rgm::idl_string_seq_t &str_seq)
{
	uint_t i = 0;
	char **p;

	// Treat NULL as an empty list
	if (str_list == NULL) {
		str_seq.length(0);
		return;
	}

	// Set the length of the sequence
	i = str_list->strarr_list_len;
	str_seq.length(i);

	// Fill in the sequence
	i = 0;
	p = str_list->strarr_list_val;

	while (i < str_list->strarr_list_len) {
		str_seq[i] = new_str(*p);
		i++;
		p++;
	}
}

//
// Copy RPC style array of chars
//
void
copy_string_array(strarr_list *in_list, strarr_list **out_list)
{
	uint_t		i, l;
	strarr_list	*p;

	p = (strarr_list *) malloc(sizeof (strarr_list));
	l = in_list->strarr_list_len;
	p->strarr_list_len = l;
	p->strarr_list_val = (char **)malloc(l * sizeof (char *));
	for (i = 0; i < l; i++) {
		p->strarr_list_val[i] = strdup(in_list->strarr_list_val[i]);
	}
	*out_list = p;
}

#endif // EUROPA_FARM
