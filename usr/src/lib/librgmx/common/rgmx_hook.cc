/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * rgmx_hook.cc
 *
 *	Europa hooks to implement RGM extension for Farm management.
 */

#pragma ident	"@(#)rgmx_hook.cc	1.3	08/05/20 SMI"

#include <sys/clconf_int.h>
#include <rgm_proto.h>
#include <rgmx_hook.h>

static hook_mode_t hook_mode = RGMX_HOOK_DISABLED;

static char *hook_mode_string[] =
{
	"RGMX_HOOK_DISABLED",
	"RGMX_HOOK_ENABLED"
};

/*
 * RGM hook functions table.
 * ATTENTION: the two follwing arrays MUST always be in sync with
 * the hook_id enum defined in rgmx_hook.h
 */
rgmx_hook_function_t rgmx_hook[] = {
	rgmx_cfg_init,
	rgmx_set_maxnodes,
	rgmx_startup,
	rgmx_rpc_start
#ifndef EUROPA_FARM
, rgmx_update_configured_nodes,
	rgmx_update_membership,
	rgmx_trigger_farm_fencing,
	rgmx_get_nodename,
	rgmx_get_nodeid,
	rgmx_scha_farm_get,
	rgmx_scrgadm_rs_validate,
	rgmx_scha_control_checkall,
	rgmx_set_timestamp,
	rgmx_latch_intention,
	rgmx_process_intention,
	rgmx_unlatch_intention,
	rgmx_xmit_state,
	rgmx_set_pres,
	rgmx_am_i_joining,
	rgmx_read_ccr,
	rgmx_run_boot_meths,
	rgmx_chg_mastery,
	rgmx_r_restart,
	rgmx_clear_flag,
	rgmx_ack_start,
	rgmx_notify_dependencies_resolved,
	rgmx_fetch_restarting_flag,
	rgmx_retrieve_lni_mappings,
	rgmx_send_lni_mappings,
	rgmx_reclaim_lnis
#endif
};

static char *rgmx_hook_name[] = {
	"rgmx_cfg_init",
	"rgmx_set_maxnodes",
	"rgmx_startup",
	"rgmx_rpc_start"
#ifndef EUROPA_FARM
, "rgmx_update_configured_nodes",
	"rgmx_update_membership",
	"rgmx_trigger_farm_fencing",
	"rgmx_get_nodename",
	"rgmx_get_nodeid",
	"rgmx_scha_farm_get",
	"rgmx_scrgadm_rs_validate",
	"rgmx_scha_control_checkall",
	"rgmx_set_timestamp",
	"rgmx_latch_intention",
	"rgmx_process_intention",
	"rgmx_unlatch_intention",
	"rgmx_xmit_state",
	"rgmx_set_pres",
	"rgmx_am_i_joining",
	"rgmx_read_ccr",
	"rgmx_run_boot_meths",
	"rgmx_chg_mastery",
	"rgmx_r_restart",
	"rgmx_clear_flag",
	"rgmx_ack_start",
	"rgmx_notify_dependencies_resolved",
	"rgmx_fetch_restarting_flag",
	"rgmx_retrieve_lni_mappings",
	"rgmx_send_lni_mappings",
	"rgmx_reclaim_lnis"
#endif
};

void
rgmx_hook_init()
{
	hook_mode = clconf_is_farm_mgt_enabled() == B_TRUE ?
		RGMX_HOOK_ENABLED : RGMX_HOOK_DISABLED;

	ucmm_print("RGM", "HOOK mode = %s\n", hook_mode_string[hook_mode]);
}

bool
rgmx_hook_enabled()
{
	return (hook_mode == RGMX_HOOK_ENABLED ? true : false);
}

hook_mode_t
rgmx_hook_call(hook_id_t hook_id, ...)
{
	va_list ap;

	if (hook_mode == RGMX_HOOK_ENABLED) {
		va_start(ap, hook_id);
		ucmm_print("RGM", "RGMX: hook calling %s\n",
			rgmx_hook_name[hook_id]);
		rgmx_hook[hook_id](ap);
		va_end(ap);
	}

	return (hook_mode);
}
