/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgmx_comm_farm_client.cc	1.7	08/05/20 SMI"

//
// rgmx_comm_farm_client.cc
//
// The functions in this file implement the client side of the internode
// Server to Farm communication through the RPC.
//

#include <sys/sc_syslog_msg.h>
#include <rgm/rgm_msg.h>
#include <rgm_intention.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_farm.h>

//
// Fake an IDL return code
//
static idlretval_t
rpc2idlretval(rpcidlretvals retval) {

	switch (retval) {
	case RGMRPC_OK:
		return (RGMIDL_OK);
	case RGMRPC_NOREF:
		return (RGMIDL_NOREF);
	case RGMRPC_RGMD_DEATH:
		return (RGMIDL_RGMD_DEATH);
	case RGMRPC_NODE_DEATH:
		/*FALLTHRU*/
	case RGMRPC_ZONE_DEATH:
		return (RGMIDL_NODE_DEATH);
	case RGMRPC_UNKNOWN:
		/*FALLTHRU*/
	default:
		return (RGMIDL_UNKNOWN);
	}
}

//
// Free RPC style array of chars
//
static void
free_string_array(strarr_list *str_list)
{
	uint_t		i;

	if (str_list == NULL)
		return;
	for (i = 0; i < str_list->strarr_list_len; i++) {
		free(str_list->strarr_list_val[i]);
	}
}

//
// Free RPC style lni mappings sequence
//
static void
free_lni_mappings(lni_mappings_seq_t *lni_map)
{
	uint_t		i;

	if (lni_map == NULL)
		return;
	for (uint_t i = 0; i < lni_map->lni_mappings_seq_t_len; i++) {
		free(lni_map->lni_mappings_seq_t_val[i].idlstr_zonename);
	}

	free(lni_map->lni_mappings_seq_t_val);
}

//
// TCP keepalive tunable is set on the Server to reset the connection
// if the Farm node recipient is unresponsive.
// When keepalive is used, unresponsiveness cases are managed
// by the keepalive probe. This is typically the case when a Server
// node sends a request to a unresponsive Farm node. The connection
// is killed by the TCP stack and the Server fences the unresponsive
// node from the cluster.
//

// Get a reference to the rgm_comm_farm server on a given node.
CLIENT *
rgmx_comm_getref(sol::nodeid_t nodeid)
{
	CLIENT *clnt = NULL;
	char *remote_addr;
	sc_syslog_msg_handle_t handle;

	remote_addr = rgmx_cfg_getaddr(nodeid);
	if (remote_addr == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_WARNING, MESSAGE,
			"rgmx_comm_getref(farm node %d) failed",
			nodeid);
		sc_syslog_msg_done(&handle);
		free(remote_addr);
		return (NULL);
	}

	clnt = rgmx_clnt_create(remote_addr,
		RGMX_COMM_FARM_PROGRAM_NAME,
		RGMX_COMM_FARM_PROGRAM, RGMX_COMM_FARM_VERSION,
		TRUE); /* Keepalive sent in the way Server->Farm */
	if (clnt == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_WARNING, MESSAGE,
			"rgmx_comm_getref(farm node %d) failed",
			nodeid);
		sc_syslog_msg_done(&handle);
	}

	free(remote_addr);
	return (clnt);
}

// Free the reference to the rgm_comm_farm server.
void
rgmx_comm_freecnt(CLIENT *clnt)
{
	if (clnt != NULL) {
		rgmx_clnt_destroy(clnt);
	}
}

//
// The following functions implement the client side of the internode
// Server to Farm communication through the RPC (RGM_COMM protocol part).
//

void
rgmx_latch_intention(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_t lni;
	rgm::ln_incarnation_t ln_incarnation;
	rgm::rgm_intention *intent;
	rgm::intention_flag_t all_node_flags;
	PerNodeFlagsT *per_node_flags;
	int *n;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni = va_arg(ap, rgm::lni_t);
	ln_incarnation = va_arg(ap, rgm::ln_incarnation_t);
	intent = va_arg(ap, rgm::rgm_intention *);
	all_node_flags = va_arg(ap, rgm::intention_flag_t);
	per_node_flags = va_arg(ap, PerNodeFlagsT *);
	n = va_arg(ap, int *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		sc_syslog_msg_handle_t handle;
		rgmx_latch_intention_args rpc_args;
		rgmx_latch_intention_result rpc_res;
		CLIENT *clnt;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt != NULL) {
			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation =
			    Rgm_state->node_incarnations.members[
			    orb_conf::local_nodeid()];
			rpc_args.lni = lni;
			rpc_args.ln_incarnation = ln_incarnation;
			rpc_args.intent.entity_type =
				(rgm_entity)intent->entity_type;
			rpc_args.intent.operation_type =
				(rgm_operation)intent->operation_type;
			rpc_args.intent.idlstr_entity_name = strdup(
				intent->idlstr_entity_name);
			rpc_args.intent.flags = all_node_flags;
			for (int j = 0; j < per_node_flags->size(); j++) {
				LogicalNodeset *nset =
					(*per_node_flags)[j].first;
				//
				// Generate the flags for this node from the
				// per_node_flags nodesets and flags.
				//
				if (nset->containLni(lni)) {
					rpc_args.intent.flags
					    |= (*per_node_flags)[j].second;
				}
			}

			if (rgmx_latch_intention_1(&rpc_args, &rpc_res, clnt)
			    != RPC_SUCCESS) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "latch_intention(): IDL exception "
				    "when communicating to node %d", nodeid);
				sc_syslog_msg_done(&handle);
			} else {
				if (rpc_res.retval == RGMRPC_OK) {
					(*n)++;
				}
			}
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_process_intention(va_list ap)
{
	sol::nodeid_t nodeid;
	int *n;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	n = va_arg(ap, int *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		sc_syslog_msg_handle_t handle;
		rgmx_process_intention_args rpc_args;
		CLIENT *clnt;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt != NULL) {
			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation =
			    Rgm_state->node_incarnations.members[
			    orb_conf::local_nodeid()];

			if (rgmx_process_intention_1(&rpc_args, NULL, clnt)
			    != RPC_SUCCESS) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "process_intention(): IDL exception "
				    "when communicating to node %d", nodeid);
				sc_syslog_msg_done(&handle);
			} else {
				(*n)++;
			}
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_unlatch_intention(va_list ap)
{
	sol::nodeid_t nodeid;
	int *n;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	n = va_arg(ap, int *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		sc_syslog_msg_handle_t handle;
		rgmx_process_intention_args rpc_args;
		CLIENT *clnt;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt != NULL) {
			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation =
			    Rgm_state->node_incarnations.members[
			    orb_conf::local_nodeid()];

			if (rgmx_unlatch_intention_1(&rpc_args, NULL, clnt)
			    != RPC_SUCCESS) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				//
				// SCMSGS
				// @explanation
				// Need explanation of this message!
				// @user_action
				// Need a user action for this message.
				//
				(void) sc_syslog_msg_log(handle, LOG_ERR,
				    MESSAGE,
				    "unlatch_intention(): IDL exception "
				    "when communicating to node %d", nodeid);
				sc_syslog_msg_done(&handle);
			} else {
				(*n)++;
			}
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_xmit_state(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::node_state **state;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	state = va_arg(ap, rgm::node_state **);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT * clnt;
		rgm::node_state *tmp_state = new rgm::node_state;
		node_state rpc_state = {0};
		rg_state *rg;
		zone_state *zs;
		r_state *res;
		uint_t zone_index = 0, rg_index = 0, res_index = 0;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
			return;
		}
		if (rgmx_xmit_state_1(NULL, &rpc_state, clnt)
		    != RPC_SUCCESS) {
			*retval = RGMIDL_NODE_DEATH;
			xdr_free((xdrproc_t)xdr_node_state,
			    (char *)&rpc_state);
			rgmx_comm_freecnt(clnt);
			return;
		}

		tmp_state->seq.length(rpc_state.seq.zone_state_seq_t_len);
		for (zs = rpc_state.seq.zone_state_seq_t_val, zone_index = 0;
		    zone_index < rpc_state.seq.zone_state_seq_t_len;
		    zs++) {
			tmp_state->seq[zone_index].lni = zs->lni;
			tmp_state->seq[zone_index].zseq.length(
				zs->zseq.rg_state_seq_t_len);

			for (rg = zs->zseq.rg_state_seq_t_val, rg_index = 0;
			    rg_index < zs->zseq.rg_state_seq_t_len;
			    rg++) {
				tmp_state->seq[zone_index].
				    zseq[rg_index].idlstr_rgname =
				    strdup_nocheck(rg->idlstr_rgname);
				tmp_state->seq[zone_index].
				    zseq[rg_index].rgstate =
				    (rgm::rgm_rg_state) rg->rgstate;
				tmp_state->seq[zone_index].
				    zseq[rg_index].rlist.
				    length(rg->rlist.r_state_seq_t_len);
				for (res = rg->rlist.r_state_seq_t_val,
				    res_index = 0;
				    res_index < rg->rlist.r_state_seq_t_len;
				    res++) {
					tmp_state->seq[zone_index].
					    zseq[rg_index].rlist[res_index].
					    idlstr_rname = strdup_nocheck(
					    res->idlstr_rname);
					tmp_state->seq[zone_index].
					    zseq[rg_index].rlist[res_index].
					    rstate =
					    (rgm::rgm_r_state) res->rstate;
					tmp_state->seq[zone_index].
					    zseq[rg_index].rlist[res_index].
					    fmstatus =
					    (rgm::rgm_r_fm_status)res->fmstatus;
					tmp_state->seq[zone_index].
					    zseq[rg_index].rlist[res_index].
					    idlstr_status_msg = strdup_nocheck(
					    res->idlstr_status_msg);
					res_index++;
				}
				rg_index++;
			}
			zone_index++;
		}

		xdr_free((xdrproc_t)xdr_node_state, (char *)&rpc_state);
		rgmx_comm_freecnt(clnt);
		*state = tmp_state;

		*retval = RGMIDL_OK;
	}
}


void
rgmx_set_pres(va_list ap)
{
	sol::nodeid_t nodeid;
	sol::nodeid_t pres;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	pres =  va_arg(ap, sol::nodeid_t);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT * clnt;
		rgmx_set_pres_args rpc_args;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.node = pres;
			rpc_args.incarnation =
				Rgm_state->node_incarnations.members
			    [orb_conf::local_nodeid()];
			rpc_args.ipaddr = (char *)rgmx_cfg_getaddr(pres);

			*retval = RGMIDL_OK;
			if (rgmx_set_pres_1(&rpc_args, NULL, clnt)
				!= RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			}

			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_chg_mastery(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_t lni;
	rgm::ln_incarnation_t ln_incarnation;
	namelist_t *on_list;
	namelist_t *off_list;
	rgm::idl_string_seq_t onlist;
	rgm::idl_string_seq_t offlist;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni = va_arg(ap, rgm::lni_t);
	ln_incarnation = va_arg(ap, rgm::ln_incarnation_t);
	on_list = va_arg(ap, namelist_t *);
	off_list = va_arg(ap,  namelist_t *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_chg_mastery_args rpc_args;
		rgmx_chg_mastery_result rpc_res;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation =
				Rgm_state->node_incarnations.members[
				orb_conf::local_nodeid()];
			rpc_args.lni = lni;
			rpc_args.ln_incarnation = ln_incarnation;

			// convert namelist to sequence of strings
			namelist_to_seq(on_list, onlist);
			namelist_to_seq(off_list, offlist);
			(void) strseq_to_array(onlist, &rpc_args.onlist);
			(void) strseq_to_array(offlist, &rpc_args.offlist);

			if (rgmx_chg_mastery_1(&rpc_args, &rpc_res, clnt)
			    != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			}
			*retval = rpc2idlretval(rpc_res.retval);

			free_string_array(rpc_args.onlist);
			free_string_array(rpc_args.offlist);
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_r_restart(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_t lni;
	rgm::ln_incarnation_t ln_incarnation;
	rgm::rgm_restart_flag rrflag;
	char *r_name;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni = va_arg(ap, rgm::lni_t);
	ln_incarnation = va_arg(ap, rgm::ln_incarnation_t);
	rrflag = va_arg(ap, rgm::rgm_restart_flag);
	r_name = va_arg(ap, char *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_r_restart_args rpc_args;
		rgmx_r_restart_result rpc_res;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation =
				Rgm_state->node_incarnations.members[
				orb_conf::local_nodeid()];
			rpc_args.lni = lni;
			rpc_args.ln_incarnation = ln_incarnation;
			rpc_args.rrflag = (rgm_restart_flag)rrflag;
			rpc_args.R_name = strdup(r_name);

			*retval = RGMIDL_OK;
			if (rgmx_r_restart_1(&rpc_args, &rpc_res, clnt)
			    != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			}

			*retval = rpc2idlretval(rpc_res.retval);

			free(rpc_args.R_name);
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_am_i_joining(va_list ap)
{
	sol::nodeid_t nodeid;
	bool *is_joining;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	is_joining = va_arg(ap, bool *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		bool_t rpc_is_joining;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			if (rgmx_am_i_joining_1(NULL, &rpc_is_joining, clnt)
			    != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				*is_joining = rpc_is_joining ? true : false;
				*retval = RGMIDL_OK;
			}
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_read_ccr(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_mappings_seq_t *lni_map;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni_map = va_arg(ap, rgm::lni_mappings_seq_t *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_read_ccr_args rpc_args;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rgmx_read_ccr_args rpc_args;

			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation = Rgm_state->
			    node_incarnations.members[
				    orb_conf::local_nodeid()];
			rpc_args.lni_map.lni_mappings_seq_t_len =
				lni_map->length();
			rpc_args.lni_map.lni_mappings_seq_t_val =
			    (lni_mappings *)malloc(
			    rpc_args.lni_map.lni_mappings_seq_t_len *
			    sizeof (lni_mappings));

			for (uint_t i = 0; i < lni_map->length(); i++) {
				rgm::lni_mappings &currMapping = (*lni_map)[i];
				rpc_args.lni_map.
				    lni_mappings_seq_t_val[i]
				    .idlstr_zonename = strdup(
				    currMapping.idlstr_zonename);
				rpc_args.lni_map.
				    lni_mappings_seq_t_val[i].lni =
				    currMapping.lni;
				rpc_args.lni_map.
				    lni_mappings_seq_t_val[i].state =
				    (rgm_ln_state) currMapping.state;
				rpc_args.lni_map.
				    lni_mappings_seq_t_val[i].incarn =
				    currMapping.incarn;
			}

			if (rgmx_read_ccr_1(&rpc_args, NULL, clnt)
			    != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				*retval = RGMIDL_OK;
			}
			free_lni_mappings(&rpc_args.lni_map);
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_run_boot_meths(va_list ap)
{
	sol::nodeid_t nodeid;
	idlretval_t *retval;
	sol::nodeid_t mynodeid = orb_conf::node_number();
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_run_boot_meths_args rpc_args;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.president = mynodeid;
			rpc_args.incarnation = Rgm_state->
			    node_incarnations.members[mynodeid];
			if (rgmx_run_boot_meths_1(&rpc_args, NULL,
			    clnt)
			    != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				*retval = RGMIDL_OK;
			}

			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_ack_start(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_t lni;
	rgm::ln_incarnation_t ln_incarnation;
	char *r_name;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni = va_arg(ap, rgm::lni_t);
	ln_incarnation  = va_arg(ap, rgm::ln_incarnation_t);
	r_name = va_arg(ap, char *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_ack_start_args rpc_args;
		rgmx_ack_start_result rpc_res;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation =
				Rgm_state->node_incarnations.members[
			    orb_conf::local_nodeid()];
			rpc_args.lni = lni;
			rpc_args.ln_incarnation = ln_incarnation;
			rpc_args.R_name = strdup(r_name);

			if (rgmx_ack_start_1(&rpc_args, &rpc_res, clnt)
			    != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				*retval = rpc2idlretval(rpc_res.retval);
			}

			free(rpc_args.R_name);
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_notify_dependencies_resolved(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_t lni;
	rgm::ln_incarnation_t ln_incarnation;
	char *r_name;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni = va_arg(ap, rgm::lni_t);
	ln_incarnation = va_arg(ap, rgm::ln_incarnation_t);
	r_name = va_arg(ap, char *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_notify_dependencies_resolved_args rpc_args;
		rgmx_notify_dependencies_resolved_result rpc_res;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation =
				Rgm_state->node_incarnations.members[
				orb_conf::local_nodeid()];
			rpc_args.lni = lni;
			rpc_args.ln_incarnation = ln_incarnation;
			rpc_args.R_name = strdup(r_name);

			if (rgmx_notify_dependencies_resolved_1(&rpc_args,
				&rpc_res, clnt) != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				 *retval =  rpc2idlretval(rpc_res.retval);
			}

			free(rpc_args.R_name);
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_fetch_restarting_flag(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_t lni;
	rgm::ln_incarnation_t ln_incarnation;
	char *r_name;
	rgm::r_restart_t *flag_param;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni = va_arg(ap, rgm::lni_t);
	ln_incarnation = va_arg(ap, rgm::ln_incarnation_t);
	r_name = va_arg(ap, char *);
	flag_param = va_arg(ap, rgm::r_restart_t *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_fetch_restarting_flag_args rpc_args;
		rgmx_fetch_restarting_flag_result rpc_res;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation =
				Rgm_state->node_incarnations.members[
				orb_conf::local_nodeid()];
			rpc_args.lni = lni;
			rpc_args.ln_incarnation = ln_incarnation;
			rpc_args.R_name = strdup(r_name);

			if (rgmx_fetch_restarting_flag_1(&rpc_args,
				&rpc_res, clnt) != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				*flag_param = (rgm::r_restart_t)
					rpc_res.restart_flag;
				*retval = rpc2idlretval(rpc_res.retval);
			}

			xdr_free((xdrproc_t)
			    xdr_rgmx_fetch_restarting_flag_result,
			    (char *)&rpc_res);
			free(rpc_args.R_name);
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_scrgadm_rs_validate(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_t lni;
	rgm::ln_incarnation_t ln_incarnation;
	rgm::idl_validate_args *val_args;
	rgm::idl_regis_result_t_var *rs_validate_v;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni = va_arg(ap, rgm::lni_t);
	ln_incarnation =  va_arg(ap, rgm::ln_incarnation_t);
	val_args = va_arg(ap, rgm::idl_validate_args *);
	rs_validate_v  = va_arg(ap, rgm::idl_regis_result_t_var *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_regis_result_t rpc_rs_validate_v = {0};
		rgmx_validate_args rpc_val_args;
		rgm::idl_regis_result_t *rs_validate
		    = new rgm::idl_regis_result_t();

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_val_args.president = orb_conf::local_nodeid();
			rpc_val_args.incarnation =
			    Rgm_state->node_incarnations.members[
			    orb_conf::local_nodeid()];
			rpc_val_args.lni = lni;
			rpc_val_args.ln_incarnation = ln_incarnation;
			rpc_val_args.idlstr_rs_name = val_args->idlstr_rs_name;
			rpc_val_args.idlstr_rt_name = val_args->idlstr_rt_name;
			rpc_val_args.idlstr_rg_name = val_args->idlstr_rg_name;
			rpc_val_args.locale = val_args->locale;
			rpc_val_args.opcode = (rgm_operation)val_args->opcode;
			rpc_val_args.is_scalable = val_args->is_scalable;
			(void) strseq_to_array(val_args->rs_props,
			    &rpc_val_args.rs_props);
			(void) strseq_to_array(val_args->ext_props,
			    &rpc_val_args.ext_props);
			(void) strseq_to_array(val_args->rg_props,
			    &rpc_val_args.rg_props);

			if (rgmx_scrgadm_rs_validate_1(&rpc_val_args,
			    &rpc_rs_validate_v, clnt) != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				*retval = rpc2idlretval(
				    rpc_rs_validate_v.retval);

				if (*retval == RGMIDL_OK) {
					rs_validate->ret_code =
					    rpc_rs_validate_v.ret_code;
					if ((rpc_rs_validate_v.idlstr_err_msg
					    != NULL) &&
					    (rpc_rs_validate_v.idlstr_err_msg[0]
					    != '\0')) {
						rs_validate->idlstr_err_msg =
						    strdup(rpc_rs_validate_v.
						    idlstr_err_msg);
					}

					*rs_validate_v = rs_validate;
				}
			}
			xdr_free((xdrproc_t)xdr_rgmx_regis_result_t,
			    (char *)&rpc_rs_validate_v);
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_set_timestamp(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_t lni;
	char *resource_name;
	rgm::idl_regis_result_t_var *ti_res_v;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni = va_arg(ap, rgm::lni_t);
	resource_name = va_arg(ap, char *);
	ti_res_v = va_arg(ap, rgm::idl_regis_result_t_var *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_set_timestamp_args rpc_args;
		rgmx_regis_result_t rpc_ti_res_v = {0};
		rgm::idl_regis_result_t *ti_res
		    = new rgm::idl_regis_result_t();

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.lni =  lni;
			rpc_args.R_name = resource_name;
			if (rgmx_set_timestamp_1(&rpc_args,
			    &rpc_ti_res_v, clnt) != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				ti_res->ret_code = rpc_ti_res_v.ret_code;
				if ((rpc_ti_res_v.idlstr_err_msg != NULL) &&
				    (rpc_ti_res_v.idlstr_err_msg[0] != '\0'))
					ti_res->idlstr_err_msg = strdup(
					    rpc_ti_res_v.idlstr_err_msg);

				*ti_res_v = ti_res;
				*retval = RGMIDL_OK;
			}
			xdr_free((xdrproc_t)xdr_rgmx_regis_result_t,
			    (char *)&rpc_ti_res_v);
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_scha_control_checkall(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::ln_incarnation_t ln_incarnation;
	rgm::idl_scha_control_checkall_args *check_args;
	rgm::idl_scha_control_result_var *chk_res_v;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	ln_incarnation = va_arg(ap, rgm::ln_incarnation_t);
	check_args = va_arg(ap, rgm::idl_scha_control_checkall_args *);
	chk_res_v  = va_arg(ap, rgm::rgm::idl_scha_control_result_var *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_scha_control_checkall_args rpc_args;
		rgmx_scha_control_result rpc_chk_res_v = {0};
		rgm::idl_scha_control_result *chk_res =
		    new rgm::idl_scha_control_result();

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation =
			    Rgm_state->node_incarnations.members[
			    orb_conf::local_nodeid()];
			rpc_args.ln_incarnation = ln_incarnation;
			rpc_args.lni = check_args->lni;
			rpc_args.idlstr_R_name = strdup(
				check_args->idlstr_R_name);
			rpc_args.flags = check_args->flags;

			if (rgmx_scha_control_checkall_1(&rpc_args,
				&rpc_chk_res_v, clnt) != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				*retval = rpc2idlretval(rpc_chk_res_v.retval);
				if (*retval == RGMIDL_OK) {
					chk_res->ret_code =
					    rpc_chk_res_v.ret_code;
					if ((rpc_chk_res_v.idlstr_err_msg
					    != NULL) &&
					    (rpc_chk_res_v.idlstr_err_msg[0]
					    != '\0'))
						chk_res->idlstr_err_msg =
						    strdup(rpc_chk_res_v.
						    idlstr_err_msg);
					if ((rpc_chk_res_v.idlstr_syslog_msg
					    != NULL) &&
					    (rpc_chk_res_v.idlstr_syslog_msg[0]
					    != '\0'))
						chk_res->idlstr_syslog_msg =
						    strdup(rpc_chk_res_v.
						    idlstr_syslog_msg);

					*chk_res_v = chk_res;
				}
			}

			xdr_free((xdrproc_t)xdr_rgmx_scha_control_result,
			    (char *)&rpc_chk_res_v);
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_clear_flag(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_t lni;
	rgm::ln_incarnation_t ln_incarnation;
	rgm::idl_string_seq_t *rseq;
	rgm::scswitch_clear_flags flags;
	rgm::idl_regis_result_t_var *clr_flg_res_v;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni = va_arg(ap, rgm::lni_t);
	ln_incarnation = va_arg(ap, rgm::ln_incarnation_t);
	rseq = va_arg(ap, rgm::idl_string_seq_t *);
	flags = va_arg(ap, rgm::scswitch_clear_flags);
	clr_flg_res_v  = va_arg(ap, rgm::idl_regis_result_t_var *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_clear_flag_args rpc_args;
		rgm::idl_regis_result_t *tmp_res =
		    new rgm::idl_regis_result_t();

		rgmx_regis_result_t rpc_clr_flg_res_v = {0};

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation =
			    Rgm_state->node_incarnations.members[
				    orb_conf::local_nodeid()];
			rpc_args.lni = lni;
			rpc_args.ln_incarnation = ln_incarnation;
			(void) strseq_to_array(*rseq,
			    &rpc_args.r_list);
			rpc_args.flag =
			    (scswitch_clear_flags) flags;

			if (rgmx_clear_flag_1(&rpc_args,
			    &rpc_clr_flg_res_v, clnt) != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				*retval = rpc2idlretval(
				    rpc_clr_flg_res_v.retval);
				if (*retval == RGMIDL_OK) {
					tmp_res->ret_code =
					    rpc_clr_flg_res_v.ret_code;
					if ((rpc_clr_flg_res_v.
					    idlstr_err_msg != NULL) &&
					    (rpc_clr_flg_res_v.
					    idlstr_err_msg[0] != '\0'))
						tmp_res->idlstr_err_msg =
						    strdup(rpc_clr_flg_res_v.
						    idlstr_err_msg);
					free_string_array(rpc_args.r_list);

					*clr_flg_res_v = tmp_res;
				}

			}
			xdr_free((xdrproc_t)xdr_rgmx_regis_result_t,
			    (char *)&rpc_clr_flg_res_v);
			rgmx_comm_freecnt(clnt);
		}
	}
}


void
rgmx_retrieve_lni_mappings(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_mappings_seq_t **lni_map;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni_map = va_arg(ap, rgm::lni_mappings_seq_t **);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgm::lni_mappings_seq_t *tmp_lni_map =
		    new rgm::lni_mappings_seq_t();
		rgmx_retrieve_lni_mappings_result rpc_res = {0};

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
			return;
		}
		if (rgmx_retrieve_lni_mappings_1(NULL, &rpc_res,
		    clnt) != RPC_SUCCESS) {
			*retval = RGMIDL_NODE_DEATH;
			xdr_free((xdrproc_t)
			    xdr_rgmx_retrieve_lni_mappings_result,
			    (char *)&rpc_res);
			rgmx_comm_freecnt(clnt);
			return;
		}

		tmp_lni_map->length(rpc_res.lni_map.lni_mappings_seq_t_len);
		for (uint_t i = 0;
		    i < rpc_res.lni_map.lni_mappings_seq_t_len;
		    i++) {
			(*tmp_lni_map)[i].idlstr_zonename =
			    strdup(rpc_res.lni_map.
				lni_mappings_seq_t_val[i].
				idlstr_zonename);
			(*tmp_lni_map)[i].lni =
			    rpc_res.lni_map.lni_mappings_seq_t_val[i].lni;
			(*tmp_lni_map)[i].state = (rgm::rgm_ln_state)
			    rpc_res.lni_map.lni_mappings_seq_t_val[i].state;
			(*tmp_lni_map)[i].incarn =
			    rpc_res.lni_map.lni_mappings_seq_t_val[i].incarn;
		}

		xdr_free((xdrproc_t)
		    xdr_rgmx_retrieve_lni_mappings_result,
		    (char *)&rpc_res);
		rgmx_comm_freecnt(clnt);
		*lni_map = tmp_lni_map;

		*retval = RGMIDL_OK;
	}
}


void
rgmx_send_lni_mappings(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_mappings_seq_t *lni_map;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni_map = va_arg(ap, rgm::lni_mappings_seq_t *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_send_lni_mappings_args rpc_args;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.president = orb_conf::local_nodeid();
			rpc_args.incarnation = Rgm_state->node_incarnations.
			    members[orb_conf::local_nodeid()];
			rpc_args.lni_map.lni_mappings_seq_t_len =
			    lni_map->length();
			rpc_args.lni_map.lni_mappings_seq_t_val =
			    (lni_mappings *)malloc(
			    rpc_args.lni_map.lni_mappings_seq_t_len *
			    sizeof (lni_mappings));

			for (uint_t i = 0; i < lni_map->length(); i++) {
				rgm::lni_mappings &currMapping = (*lni_map)[i];
				rpc_args.lni_map.
				    lni_mappings_seq_t_val[i]
				    .idlstr_zonename = strdup(
				    currMapping.idlstr_zonename);
				rpc_args.lni_map.
				    lni_mappings_seq_t_val[i].lni =
				    currMapping.lni;
				rpc_args.lni_map.
				    lni_mappings_seq_t_val[i].state =
				    (rgm_ln_state)currMapping.state;
				rpc_args.lni_map.
				    lni_mappings_seq_t_val[i].incarn =
				    currMapping.incarn;
			}

			if (rgmx_send_lni_mappings_1(&rpc_args, NULL, clnt)
			    != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				*retval = RGMIDL_OK;
			}
			free_lni_mappings(&rpc_args.lni_map);
			rgmx_comm_freecnt(clnt);
		}
	}
}

void
rgmx_reclaim_lnis(va_list ap)
{
	sol::nodeid_t nodeid;
	rgm::lni_seq_t *lni_seq;
	rgm::ln_incarnation_t ln_incarnation;
	idlretval_t *retval;
	bool *is_farmnode;

	nodeid = va_arg(ap, sol::nodeid_t);
	lni_seq = va_arg(ap, rgm::lni_seq_t *);
	retval = va_arg(ap, idlretval_t *);
	is_farmnode = va_arg(ap, bool *);

	*is_farmnode = false;
	if (in_current_farm_membership(nodeid)) {
		CLIENT *clnt;
		rgmx_reclaim_lnis_args rpc_args;
		rgmx_reclaim_lnis_result rpc_res;

		*is_farmnode = true;
		clnt = rgmx_comm_getref(nodeid);
		if (clnt == NULL) {
			*retval = RGMIDL_NOREF;
		} else {
			rpc_args.node = orb_conf::local_nodeid();
			rpc_args.incarnation =
			    Rgm_state->node_incarnations.members[
			    orb_conf::local_nodeid()];
			rpc_args.lni_seq.lni_seq_t_len = lni_seq->length();
			rpc_args.lni_seq.lni_seq_t_val =
			    (lni_t *)malloc(
			    rpc_args.lni_seq.lni_seq_t_len *
			    sizeof (lni_t));
			for (uint_t i = 0; i < lni_seq->length(); i++) {
				rpc_args.lni_seq.lni_seq_t_val[i] =
				    (*lni_seq)[i];
			}

			if (rgmx_reclaim_lnis_1(&rpc_args, &rpc_res,
			    clnt)
			    != RPC_SUCCESS) {
				*retval = RGMIDL_NODE_DEATH;
			} else {
				*retval = RGMIDL_OK;
				lni_seq->length(rpc_res.lni_seq.lni_seq_t_len);
				for (uint_t i = 0;
				    i < lni_seq->length(); i++) {
					(*lni_seq)[i] =
					    rpc_res.lni_seq.lni_seq_t_val[i];
				}
			}
			free(rpc_args.lni_seq.lni_seq_t_val);
			xdr_free((xdrproc_t)xdr_rgmx_reclaim_lnis_result,
			    (char *)&rpc_res);
			rgmx_comm_freecnt(clnt);
		}
	}
}
