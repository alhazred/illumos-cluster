/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgmx_receptionist.cc	1.6	08/07/21 SMI"

/*
 * rgmx_receptionist.cc is part of receptionist component of rgmd
 * On server node: the scha_farm_get is defined and used by both server and
 * farm nodes to get farm nodes information.
 *
 * On farm nodes: scha_cluster_get retrieved information form local side if
 * possible, otherwise it calls the president to get the appropriate
 * information.
 *
 * Note that europa is leveraging the receptionist/doors implementation (see
 * 1334 project) to support zones on solaris farm nodes. Linux farm nodes are
 * using receptionist/RPC implementation (i.e DOOR_IMPL always false).
 *
 */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <rgm/rgm_msg.h>
#include <rgm_state.h>
#include <rgm_api.h>
#include <rgm/security.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>
#ifdef EUROPA_FARM
#include <rgm/rpc/rgmx_comm_pres.h>
#endif


#ifndef EUROPA_FARM
/*
 * scha_farm_get() -
 *
 * 	This function is called by both Server nodes and Farm nodes.
 * 	It is always called after the scha_cluster_get_1_svc function that
 * 	retrieved inforation for Server nodes. Therefore result parameter could
 * 	always be filled with Server information.
 *	This function gets the Farm configuration according to the cluster
 *	get tag given in argument and make the union (if needed) of the Server
 *	and Farm results.
 *
 * 	On Server nodes: called directly by scha_cluster_get_1_svc
 * 	(rgm_receptionist.cc)
 * 	On Farm nodes: called by rgmx_scha_cluster_get_1_svc (rgmx_comm_pres.cc)
 *
 * 	Only the following tags are processed in this function:
 *
 * 	SCHA_NODEID_NODENAME
 * 	SCHA_NODENAME_NODEID
 * 	SCHA_PRIVATELINK_HOSTNAME_NODE
 * 	SCHA_ALL_NODENAMES
 * 	SCHA_ALL_NODEIDS
 * 	SCHA_ALL_PRIVATELINK_HOSTNAMES
 *
 * 	Input parameters:
 * 		arg: cluster tag + either nodeid/nodename
 * 		result: for SCHA_ALL_* ret_code is significant.
 */
bool_t
scha_farm_get(cluster_get_args *arg, get_result_t *result)
{
	int isfarmnode;
	int nb_logical_server_nodes;
	int nb_server_nodes;
	int nb_farm_nodes;
	f_property_t *server_node_list = NULL;
	f_property_t *server_node;
	f_property_t *farm_node_list = NULL;
	f_property_t *farm_node;
	uint_t incr_num = 0;

	/*
	 * If the tag doesn't concern any information of Farm configuration
	 * we're done.
	 */
	if ((strcmp(arg->cluster_tag, SCHA_NODEID_NODENAME) != 0) &&
	    (strcmp(arg->cluster_tag, SCHA_NODENAME_NODEID) != 0) &&
	    (strcmp(arg->cluster_tag, SCHA_PRIVATELINK_HOSTNAME_NODE) != 0) &&
	    (strcmp(arg->cluster_tag, SCHA_ALL_NODENAMES) != 0) &&
	    (strcmp(arg->cluster_tag, SCHA_ALL_NODEIDS) != 0) &&
	    (strcmp(arg->cluster_tag, SCHA_ALL_PRIVATELINK_HOSTNAMES) != 0)) {
		return (TRUE);
	}
	/*
	 * If the nodename/nodeid given in argument is not a Farm node
	 * we're done.
	 */
	if ((strcmp(arg->cluster_tag, SCHA_NODENAME_NODEID) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_NODEID_NODENAME) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_PRIVATELINK_HOSTNAME_NODE) == 0)) {
		isfarmnode = rgmx_cfg_isfarmnode(arg->node_id, arg->node_name);
		if (!isfarmnode) {
			return (TRUE);
		}
	}

	/*
	 * An invalid result here means that we need to get the
	 * cluster incarnation number.
	 */
	if (result->ret_code != SCHA_ERR_NOERR) {
		incr_num = scconf_get_incarnation(NULL);
		if (incr_num == (uint_t)0) {
			result->ret_code = SCHA_ERR_INTERNAL;
			goto cleanup;
		}
	}
	result->ret_code = SCHA_ERR_NOERR;
	ucmm_print("RGM",
		NOGET("SCHA_FARM_GET(): seq_id = %d\n"),
		incr_num);

	/*
	 * If the request is to get something for a nodeid/nodename,
	 * we need to know the type of the node. If the node is a
	 * server node, we're done.
	 * otherwise (farm node), get information requested from the
	 * Farm configuration.
	 */
	if (strcmp(arg->cluster_tag, SCHA_NODENAME_NODEID) == 0) {
		char *nodenamep;

		if (rgmx_cfg_getnodename(arg->node_id,
			&nodenamep) == -1) {
			result->ret_code = SCHA_ERR_NODE;
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
				result->ret_code);
			goto cleanup;
		}

		ucmm_print("RGM", NOGET("SCHA_NODENAME_NODEID, "
			    "id = %d, name = %s\n"), arg->node_id, nodenamep);

		result->value_list =
			(strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
			(strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));
		(result->value_list->strarr_list_val)[0] = strdup(nodenamep);

		free(nodenamep);
		goto cleanup;
	}

	if (strcmp(arg->cluster_tag, SCHA_NODEID_NODENAME) == 0) {
		scxcfg_nodeid_t nodeid;
		char nodeid_str[10];

		if (arg->node_name == NULL ||
			arg->node_name[0] == '\0') {
			result->ret_code = SCHA_ERR_NODE;
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
				result->ret_code);
			goto cleanup;
		}

		if (rgmx_cfg_getnodeid(arg->node_name, &nodeid) == -1) {
			result->ret_code = SCHA_ERR_NODE;
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
				result->ret_code);
			goto cleanup;
		}

		(void) sprintf(nodeid_str, "%d", nodeid);

		result->value_list =
			(strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
			(strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));

		ucmm_print("RGM",
			NOGET("SCHA_NODEID_NODENAME, "
				" name = %s, id = %s\n"),
			arg->node_name, nodeid_str);

		(result->value_list->strarr_list_val)[0] = strdup(nodeid_str);

		goto cleanup;
	}

	if (strcmp(arg->cluster_tag, SCHA_PRIVATELINK_HOSTNAME_NODE) == 0) {
		scxcfg_nodeid_t nodeid;
		char *private_hostname;

		if (arg->node_name == NULL ||
			arg->node_name[0] == '\0') {
			result->ret_code = SCHA_ERR_NODE;
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
				result->ret_code);
			goto cleanup;
		}

		if (rgmx_cfg_getnodeid(arg->node_name,
				&nodeid) == -1) {
			result->ret_code = SCHA_ERR_NODE;
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
				result->ret_code);
			goto cleanup;
		}

		if (rgmx_cfg_getprivatehostname(nodeid,
			&private_hostname) == -1) {
			result->ret_code = SCHA_ERR_NODE;
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
				result->ret_code);
			goto cleanup;
		}

		result->value_list =
			(strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
			(strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));

		ucmm_print("RGM",
			NOGET("SCHA_PRIVATELINK_HOSTNAME_NODE, "
				" nodeid = %d, is = %s\n"),
			nodeid, private_hostname);

		(result->value_list->strarr_list_val)[0] =
			strdup(private_hostname);

		free(private_hostname);
		goto cleanup;
	}

	/*
	 * If the request is something like get ALL_*, need to get info from
	 * both server and farm side and return the union of results.
	 */
	if ((strcmp(arg->cluster_tag, SCHA_ALL_NODENAMES) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_ALL_NODEIDS) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_ALL_PRIVATELINK_HOSTNAMES) == 0)) {
		int nb_nodes = 0;

		// At this point, result contains Server information.
		nb_logical_server_nodes = 0;
		if (result->value_list != NULL) {
			nb_logical_server_nodes =
			    result->value_list->strarr_list_len;
		}

		/*
		 * Get all farm nodes.
		 */
		if (rgmx_cfg_getfarmnodes(&farm_node_list,
				&nb_farm_nodes) == -1) {
			result->ret_code = SCHA_ERR_NODE;
			ucmm_print("RGM", NOGET("scha_error=%d\n"),
				result->ret_code);
			goto cleanup;
		}
		if (nb_farm_nodes == 0) {
			goto cleanup;
		}

		nb_nodes += nb_farm_nodes;

		// For SCHA_ALL_PRIVATELINK_HOSTNAMES tag, we need to
		// realloc space for both server+farm nodes.
		if (strcmp(arg->cluster_tag,
		    SCHA_ALL_PRIVATELINK_HOSTNAMES) == 0) {
			// Get all server nodes.
			if (rgmx_cfg_getservernodes(&server_node_list,
					&nb_server_nodes) == -1) {
				result->ret_code = SCHA_ERR_NODE;
				ucmm_print("RGM", NOGET("scha_error=%d\n"),
					result->ret_code);
				goto cleanup;
			}
			nb_nodes += nb_server_nodes;
		}

		if (result->value_list == NULL) {
			result->value_list = (strarr_list *)
				malloc(sizeof (strarr_list));
			result->value_list->strarr_list_val =
				(strarr *) malloc(sizeof (strarr) *
				    nb_nodes);
			result->value_list->strarr_list_len = nb_nodes;
		} else {
			result->value_list->strarr_list_len += nb_nodes;
			result->value_list->strarr_list_val =
				(strarr *) realloc(
					result->value_list->strarr_list_val,
					sizeof (strarr) *
					result->value_list->strarr_list_len);
		}
	}

	if (strcmp(arg->cluster_tag, SCHA_ALL_NODEIDS) == 0) {
		scxcfg_nodeid_t nodeid;
		char nodeid_str[10];
		int i;

		ucmm_print("RGM",
			NOGET("SCHA_ALL_NODEIDS, nb_farm_nodes = %d\n"),
			nb_farm_nodes);

		for (farm_node = farm_node_list, i = nb_logical_server_nodes;
			farm_node != NULL;
			farm_node = farm_node->next, i++) {
			nodeid = atoi(farm_node->value);
			ucmm_print("RGM", NOGET(
				"SCHA_ALL_NODEIDS, node %d is %d\n"),
				i, nodeid);

			(void) sprintf(nodeid_str, "%d", nodeid);
			(result->value_list->strarr_list_val)[i] =
				strdup(nodeid_str);
		}

		goto cleanup;
	}

	if (strcmp(arg->cluster_tag, SCHA_ALL_NODENAMES) == 0) {
		scxcfg_nodeid_t nodeid;
		char *nodenamep;
		int i;

		ucmm_print("RGM",
			NOGET("SCHA_ALL_NODENAMES, nb_farm_nodes = %d\n"),
			nb_farm_nodes);

		for (farm_node = farm_node_list, i = nb_logical_server_nodes;
			farm_node != NULL;
			farm_node = farm_node->next, i++) {
			nodeid = atoi(farm_node->value);

			(void) rgmx_cfg_getnodename(nodeid, &nodenamep);

			ucmm_print("RGM", NOGET(
				"SCHA_ALL_NODENAMES, node %d is %s\n"),
				i, nodenamep);

			(result->value_list->strarr_list_val)[i] =
				strdup(nodenamep);
			free(nodenamep);
		}

		goto cleanup;
	}

	if (strcmp(arg->cluster_tag, SCHA_ALL_PRIVATELINK_HOSTNAMES) == 0) {
		scxcfg_nodeid_t nodeid;
		char *private_hostname;
		int i;

		ucmm_print("RGM",
		    NOGET("SCHA_ALL_PRIVATELINK_HOSTNAMES, "
		    "nb_server_nodes = %d, nb_farm_nodes = %d\n"),
		    nb_server_nodes, nb_farm_nodes);

		for (server_node = server_node_list,
			i = nb_logical_server_nodes;
			server_node != NULL;
			server_node = server_node->next, i++) {
			nodeid = atoi(server_node->value);

			(void) rgmx_cfg_getprivatehostname(nodeid,
				&private_hostname);

			ucmm_print("RGM", NOGET(
				"SCHA_ALL_PRIVATELINK_HOSTNAMES, "
				"server node %d is %s\n"),
				i, private_hostname);

			(result->value_list->strarr_list_val)[i] =
				strdup(private_hostname);
			free(private_hostname);
		}
		rgmx_cfg_freelist(server_node_list);

		for (farm_node = farm_node_list;
			farm_node != NULL;
			farm_node = farm_node->next, i++) {
			nodeid = atoi(farm_node->value);

			(void) rgmx_cfg_getprivatehostname(nodeid,
				&private_hostname);

			ucmm_print("RGM", NOGET(
				"SCHA_ALL_PRIVATELINK_HOSTNAMES, "
				"farm node %d is %s\n"),
				i, private_hostname);

			(result->value_list->strarr_list_val)[i] =
				strdup(private_hostname);
			free(private_hostname);
		}

		goto cleanup;
	}

cleanup:
	if (result->ret_code != SCHA_ERR_NOERR) {
		ucmm_print("RGM", NOGET("scha_farm_get failed\n"));
	} else {
		if (incr_num != 0) {
			result->seq_id = rgm_itoa((int)incr_num);
		}
		ucmm_print("RGM",
		    NOGET("result->seq_id = %s\n"), result->seq_id);
		if (result->seq_id == NULL) {
			result->ret_code = SCHA_ERR_NOMEM;
			ucmm_print("RGM", NOGET("scha_farm_get failed\n"));
		}
	}

	if (farm_node_list) {
		rgmx_cfg_freelist(farm_node_list);
	}

	return (TRUE);
}

void
rgmx_scha_farm_get(va_list ap)
{
	cluster_get_args *arg;
	get_result_t *result;
	bool_t *retval;

	arg = va_arg(ap, cluster_get_args *);
	result = va_arg(ap, get_result_t *);
	retval = va_arg(ap, bool_t *);

	*retval = scha_farm_get(arg, result);
}
#endif

#ifdef EUROPA_FARM

#if DOOR_IMPL
bool_t
scha_ssm_ip_1_svc(ssm_ip_args *arg, ssm_result_t *result)
#else
bool_t
scha_ssm_ip_1_svc(ssm_ip_args *arg, ssm_result_t *result,
    struct svc_req *rqstp)
#endif
{
	result->ret_code = SCHA_ERR_INTERNAL;
	return (TRUE);
}

/*
 * scha_lb_1_svc() -
 *
 *	Do a load balancer op.
 */
#if DOOR_IMPL
bool_t
scha_lb_1_svc(scha_ssm_lb_args *arg, scha_ssm_lb_result_t *result)
#else
bool_t
scha_lb_1_svc(scha_ssm_lb_args *arg, scha_ssm_lb_result_t *result,
    struct svc_req *rqstp)
#endif
{
	result->ret_code = SCHA_ERR_INTERNAL;
	return (TRUE);
}

/*
 * scha_cluster_open_1_svc() -
 *
 *	this function calls scha_cluster_open service on the president
 *	to get cluster incarnation number
 *
 */
#if DOOR_IMPL
bool_t
scha_cluster_open_1_svc(void *, open_result_t *result)
#else
bool_t
scha_cluster_open_1_svc(void *, open_result_t *result, struct svc_req *rqstp)
#endif
{
	CLIENT *clnt;

	/*
	 * Check security:
	 * SEC_UNIX_STRONG means that we're checking if
	 * the call came from the loopback transport,
	 * and that the userid matches the one requested - in this
	 * case root. The last arg value indicates that the caller
	 * doesn't have to be root.
	 */
#if DOOR_IMPL
	door_cred_t	door_cred;
	if (security_svc_authenticate(&door_cred, FALSE) != SEC_OK) {
#else
	struct authunix_parms credential;
	if (security_svc_authenticate(rqstp, &credential,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		return (FALSE);
	}

	bzero(result, sizeof (open_result_t));

	/*
	 * Connect to the President receptionist to get the cluster
	 * incarnation number
	 */
	clnt = rgmx_comm_getpres(ZONE);
	if (clnt == NULL) {
		result->ret_code = SCHA_ERR_INTERNAL;
		ucmm_print("RGM",
		    NOGET("SCHA_CLUSTER_OPEN, FAILED\n"));
		return (TRUE);
	}

	clnt->cl_auth = authunix_create_default();

	if (rgmx_scha_cluster_open_1(NULL, result, clnt) != RPC_SUCCESS) {
		result->ret_code = SCHA_ERR_INTERNAL;
		ucmm_print("RGM",
		    NOGET("SCHA_CLUSTER_OPEN, FAILED\n"));
	}

	rgmx_comm_freecnt(clnt);

	return (TRUE);
}

/*
 * scha_cluster_get_1_svc() -
 *
 *	this function retrieves various cluster information. If the information
 *	requested is not available locally, the call is redirected to the
 *	president.
 *
 */
#if DOOR_IMPL
bool_t
scha_cluster_get_1_svc(cluster_get_args *arg, get_result_t *result)
#else
bool_t
scha_cluster_get_1_svc(cluster_get_args *arg, get_result_t *result,
    struct svc_req *rqstp)
#endif
{
	bool_t retval = TRUE;
#if DOOR_IMPL
	door_cred_t	door_cred;
#else
	struct authunix_parms credential;
#endif

	char *nodenamep = NULL;
	CLIENT *clnt;

	bzero(result, sizeof (get_result_t));
	result->ret_code = SCHA_ERR_NOERR;

	/*
	 * Check security.
	 */
#if DOOR_IMPL
	if (security_svc_authenticate(&door_cred, FALSE) != SEC_OK) {
#else
	if (security_svc_authenticate(rqstp, &credential,
	    SEC_UNIX_STRONG, FALSE) != SEC_OK) {
#endif
		result->ret_code = SCHA_ERR_ACCESS;
		retval = FALSE;
		goto cleanup;
	}

	if ((strcmp(arg->cluster_tag, SCHA_ALL_RESOURCEGROUPS)) == 0) {
		cluster_get_rgs(result);
	}

	if ((strcmp(arg->cluster_tag, SCHA_ALL_RESOURCETYPES)) == 0) {
		cluster_get_rts(result);
	}

	if ((strcmp(arg->cluster_tag, SCHA_NODESTATE_LOCAL)) == 0 ||
	    (strcmp(arg->cluster_tag, SCHA_NODESTATE_NODE)) == 0) {
		cluster_get_node_state(arg, result);
		return (TRUE);
	}

	if (strcmp(arg->cluster_tag, SCHA_NODENAME_LOCAL) == 0) {

		scconf_nodeid_t nodeid;
		char *nodenamep = NULL;
		(void) rgm_get_nodeid(NULL, &nodeid);
		nodenamep = rgm_get_nodename(nodeid);

		ucmm_print("RGM",
		    NOGET("SCHA_NODENAME_LOCAL, localnodename = %s\n"),
		    nodenamep);

		result->value_list =
		    (strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));
		(result->value_list->strarr_list_val)[0] = strdup(nodenamep);

		free(nodenamep);
		goto cleanup;
	}

	if (strcmp(arg->cluster_tag, SCHA_NODEID_LOCAL) == 0) {

		scconf_nodeid_t nodeid;
		char nodeid_str[10];

		(void) rgm_get_nodeid(NULL, &nodeid);

		ucmm_print("RGM",
		    NOGET("SCHA_NODEID_LOCAL, localnodeid = %d\n"),
		    nodeid);

		(void) sprintf(nodeid_str, "%d", nodeid);

		result->value_list =
		    (strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));
		(result->value_list->strarr_list_val)[0] = strdup(nodeid_str);

		goto cleanup;
	}

	if (strcmp(arg->cluster_tag, SCHA_PRIVATELINK_HOSTNAME_LOCAL) == 0) {

		scconf_nodeid_t nodeid;
		char *private_hostname;

		(void) rgm_get_nodeid(NULL, &nodeid);
		if (rgmx_cfg_getprivatehostname(nodeid,
				&private_hostname) == -1) {
			result->ret_code = SCHA_ERR_NODE;
			goto cleanup;
		}

		ucmm_print("RGM",
		    NOGET("SCHA_PRIVATELINK_HOSTNAME_LOCAL, nodeid = %d "
			    "is %s\n"), nodeid, private_hostname);

		result->value_list =
		    (strarr_list *) malloc(sizeof (strarr_list));
		result->value_list->strarr_list_len = 1;
		result->value_list->strarr_list_val =
		    (strarr *) malloc(sizeof (strarr) *
			(result->value_list->strarr_list_len));

		(result->value_list->strarr_list_val)[0] =
			private_hostname == NULL ? NULL :
			strdup(private_hostname);

		free(private_hostname);
		goto cleanup;
	}

	if ((strcmp(arg->cluster_tag, SCHA_CLUSTERNAME) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_NODEID_NODENAME) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_NODENAME_NODEID) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_PRIVATELINK_HOSTNAME_NODE) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_ALL_NODENAMES) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_ALL_NODEIDS) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_ALL_PRIVATELINK_HOSTNAMES) ||
	    (strcmp(arg->cluster_tag, SCHA_ALL_ZONES) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_ALL_ZONES_NODEID) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_ALL_NONGLOBAL_ZONES_NODEID) == 0) ||
	    (strcmp(arg->cluster_tag, SCHA_ALL_NONGLOBAL_ZONES) == 0) == 0)) {

		clnt = rgmx_comm_getpres(ZONE);
		if (clnt == NULL) {
			result->ret_code = SCHA_ERR_INTERNAL;
			ucmm_print("RGM",
			    NOGET("SCHA_CLUSTER_GET, FAILED\n"));
			return (TRUE);
		}

		clnt->cl_auth = authunix_create_default();

		if (rgmx_scha_cluster_get_1(arg, result, clnt) != RPC_SUCCESS) {
			result->ret_code = SCHA_ERR_INTERNAL;
			ucmm_print("RGM",
			    NOGET("SCHA_CLUSTER_GET, FAILED\n"));
		}

		rgmx_comm_freecnt(clnt);
	}

cleanup:
	if (result->seq_id == NULL) {
		// means farm local processing.
		// we are not aware of cluster incarnation number.
		result->seq_id = rgm_itoa(0);
	}
	if (result->ret_code == SCHA_ERR_NOERR) {
		ucmm_print("RGM", NOGET("SCHA_CLUSTER_GET: "
		    "seq_id = %s\n"), result->seq_id);
	} else
		ucmm_print("RGM",
		    NOGET("scha_cluster_get_1_svc: failed\n"));

	return (retval);
}
#endif /* EUROPA_FARM */
