/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgmx_cnfg_common_xdr.cc	1.4	08/05/20 SMI"

/*
 * File: rgm_cnfg_xdr.cc: RPC interface to allow a farm node to access
 * remote CCR/Name server information (on server node).
 *
 * ATTENTION - THE DEFINITIONS IN THIS FILE MUST BE IN SYNC WITH
 * ./head/rgm/rgm_common.h ./head/rgm/rgm_cpp.h
 *
 */
#include <rpc/rpc.h>
#include <strings.h>
#include <stdlib.h>
#include <rgm/rgm_cpp.h>
#include <rgm/rgm_util.h>
#include <rgm_proto.h>
#include <rgmx_cnfg_common_xdr.h>
#include <string>
#include <map>

#define	NULL_STRING	(int)0xFFFFFFFF

/*
 * In Linux, xdr_string is defined as linux_xdr_string
 * and the implementation of linux_xdr_string is mapped to Solaris implem.
 * In this case, Solaris implem does not work:
 * null strings are encoded as empty strings ("" instead of NULL)
 * and this creates problems in rgmd, for instance with zone names.
 * (When looking for a node in logicalNodeManager, if the zone name is not
 * null, we build a string nodeid:zonename. Otherwise we simply use nodeid.
 * So if zonename is "", the string is for instance "65:" instead of "65")
 * So we define xdr_string as xdr_string_null, meaning that a NULL string
 * is encoded in such a way that it can de decoded as a NULL string
 * and not an empty string.
 * The definition must be the same for linux and solaris.
 */
#ifdef linux
#undef xdr_string
#endif

#define	xdr_string(a, b, c) xdr_string_null(a, b, c)

bool_t
xdr_string_null(register XDR *xdrs, char **cpp, uint_t maxsize)
{
	char *sp = *cpp;	/* sp is the actual string pointer */
	int size;
	uint_t nodesize;
	bool_t res;

	/*
	 * first deal with the length since xdr strings are counted-strings
	 */
	switch (xdrs->x_op) {
	case XDR_FREE:
		if (sp == NULL) {
			return (TRUE);		/* already free */
		}
		/* fall through... */
	case XDR_ENCODE:
		size = (sp != NULL) ? strlen(sp) : NULL_STRING;
		break;
	case XDR_DECODE:
		break;
	}
	if (!xdr_u_int(xdrs, (uint_t *)&size)) {
		return (FALSE);
	}
	if (size > maxsize) {
		return (FALSE);
	}

	nodesize = (size != NULL_STRING) ? size + 1 : 0;

	/*
	 * now deal with the actual bytes
	 */
	switch (xdrs->x_op) {
	case XDR_DECODE:
		if (nodesize == 0) {
			*cpp = NULL;
			return (TRUE);
		}
		if (sp == NULL)
			*cpp = sp = (char *)mem_alloc(nodesize);
		if (sp == NULL) {
			(void) fprintf(stderr, "xdr_string: out of memory\n");
			return (FALSE);
		}
		sp[size] = 0;
		/* fall into ... */

	case XDR_ENCODE:
		if (nodesize == 0)
			return (TRUE);
		else
			return (xdr_opaque(xdrs, sp, (uint_t)size));

	case XDR_FREE:
		mem_free(sp, nodesize);
		*cpp = NULL;
		return (TRUE);
	}
	return (FALSE);
}

bool_t
xdr_rgm_methods_t(register XDR *xdrs, rgm_methods_t *objp)
{

	if (!xdr_string(xdrs, (char **)&objp->m_start, ~0))
			return (FALSE);
	if (!xdr_string(xdrs, (char **)&objp->m_stop, ~0))
			return (FALSE);
	if (!xdr_string(xdrs, (char **)&objp->m_validate, ~0))
			return (FALSE);
	if (!xdr_string(xdrs, (char **)&objp->m_update, ~0))
			return (FALSE);
	if (!xdr_string(xdrs, (char **)&objp->m_init, ~0))
			return (FALSE);
	if (!xdr_string(xdrs, (char **)&objp->m_fini, ~0))
		return (FALSE);
	if (!xdr_string(xdrs, (char **)&objp->m_boot, ~0))
		return (FALSE);
	if (!xdr_string(xdrs, (char **)&objp->m_monitor_start, ~0))
		return (FALSE);
	if (!xdr_string(xdrs, (char **)&objp->m_monitor_stop, ~0))
		return (FALSE);
	if (!xdr_string(xdrs, (char **)&objp->m_monitor_check, ~0))
		return (FALSE);
	if (!xdr_string(xdrs, (char **)&objp->m_prenet_start, ~0))
		return (FALSE);
	if (!xdr_string(xdrs, (char **)&objp->m_postnet_stop, ~0))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_scha_initnodes_flag_t(register XDR *xdrs, scha_initnodes_flag_t *objp)
{

	if (!xdr_enum(xdrs, (enum_t *)objp))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rgm_tune_t(register XDR *xdrs, rgm_tune_t *objp)
{

	if (!xdr_enum(xdrs, (enum_t *)objp))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rgm_sysdeftype_t(register XDR *xdrs, rgm_sysdeftype_t *objp)
{

	if (!xdr_enum(xdrs, (enum_t *)objp))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rgm_lni_t(register XDR *xdrs, rgm_lni_t *objp)
{

	if (!xdr_uint32_t(xdrs, (uint32_t *)objp))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rgm_stamp_t(register XDR *xdrs, rgm_stamp_t *objp)
{

	if (!xdr_string(xdrs, objp, ~0))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_name_t(register XDR *xdrs, name_t *objp)
{

	if (!xdr_string(xdrs, objp, ~0))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_namelist_t(register XDR *xdrs, namelist_t *objp)
{

	if (!xdr_name_t(xdrs, &objp->nl_name))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->nl_next,
	    sizeof (namelist_t), (xdrproc_t)xdr_namelist_t))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_namelist_all_t(register XDR *xdrs, namelist_all_t *objp)
{

	if (!xdr_bool(xdrs, (bool_t *)&objp->is_ALL_value))
			return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->names,
	    sizeof (namelist_t), (xdrproc_t)xdr_namelist_t))
			return (FALSE);
	return (TRUE);
}

bool_t
xdr_nodeidlist_t(register XDR *xdrs, nodeidlist_t *objp)
{

	if (!xdr_u_int(xdrs, &objp->nl_nodeid))
		return (FALSE);
	if (!xdr_name_t(xdrs, &objp->nl_zonename))
		return (FALSE);
	if (!xdr_rgm_lni_t(xdrs, &objp->nl_lni))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->nl_next,
	    sizeof (nodeidlist_t), (xdrproc_t)xdr_nodeidlist_t))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_nodeidlist_all_t(register XDR *xdrs, nodeidlist_all_t *objp)
{

	if (!xdr_bool(xdrs, (bool_t *)&objp->is_ALL_value))
			return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->nodeids,
	    sizeof (nodeidlist_t), (xdrproc_t)xdr_nodeidlist_t))
			return (FALSE);
	return (TRUE);
}

bool_t
xdr_scha_rgmode_t(register XDR *xdrs, scha_rgmode_t *objp)
{

	if (!xdr_enum(xdrs, (enum_t *)objp))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_scha_switch_t(register XDR *xdrs, scha_switch_t *objp)
{

	if (!xdr_enum(xdrs, (enum_t *)objp))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_scha_prop_type_t(register XDR *xdrs, scha_prop_type_t *objp)
{

	if (!xdr_enum(xdrs, (enum_t *)objp))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rdep_locality_t(register XDR *xdrs, rdep_locality_t *objp)
{

	if (!xdr_enum(xdrs, (enum_t *)objp))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rdeplist_t(register XDR *xdrs, rdeplist_t *objp)
{

	if (!xdr_name_t(xdrs, &objp->rl_name))
		return (FALSE);
	if (!xdr_rdep_locality_t(xdrs, &objp->locality_type))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rl_next,
	    sizeof (rdeplist_t), (xdrproc_t)xdr_rdeplist_t))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rgm_dependencies_t(register XDR *xdrs, rgm_dependencies_t *objp)
{

	if (!xdr_pointer(xdrs, (char **)&objp->dp_weak,
	    sizeof (rdeplist_t), (xdrproc_t)xdr_rdeplist_t))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->dp_strong,
	    sizeof (rdeplist_t), (xdrproc_t)xdr_rdeplist_t))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->dp_restart,
	    sizeof (rdeplist_t), (xdrproc_t)xdr_rdeplist_t))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->dp_offline_restart,
	    sizeof (rdeplist_t), (xdrproc_t)xdr_rdeplist_t))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_value_t(register XDR *xdrs, value_t *objp)
{
	std::map<rgm_lni_t, char *>::iterator it;
	std::map<std::string, name_t>::iterator ite;
	char *rp_value = NULL;
	char *node = NULL;
	rgm_lni_t lni;
	rgm_value_t *rgmv;
	uint_t count;
	char *s = NULL;
	char *rp_value_node  = NULL;

	switch (xdrs->x_op) {
	case XDR_ENCODE:
		for (count = 0, it = ((rgm_value_t *)*objp)->rp_value.begin();
		    it != ((rgm_value_t *)*objp)->rp_value.end(); ++it,
		    count++);
		break;
	case XDR_DECODE:
		break;
	}

	if (xdrs->x_op != XDR_FREE)
		if (!xdr_u_int(xdrs, &count))
			return (FALSE);

	switch (xdrs->x_op) {
	case XDR_ENCODE:
	case XDR_FREE:
		for (it = ((rgm_value_t *)*objp)->rp_value.begin();
		    it != ((rgm_value_t *)*objp)->rp_value.end(); ++it) {
			if (!xdr_rgm_lni_t(xdrs, (rgm_lni_t *)&it->first))
				return (FALSE);
			if (!xdr_string(xdrs, &it->second, ~0))
				return (FALSE);
		}
		break;
	case XDR_DECODE:
		rgmv = new rgm_value_t;
		if (rgmv == NULL)
			return (FALSE);
		for (; count > 0; count--) {
			if (!xdr_rgm_lni_t(xdrs, &lni))
				return (FALSE);
			rp_value = NULL;
			if (!xdr_string(xdrs, &rp_value, ~0))
				return (FALSE);
			rgmv->rp_value[lni] = rp_value;
		}
		break;
	}

	switch (xdrs->x_op) {
	case XDR_ENCODE:
		for (count = 0,
		    ite = ((rgm_value_t *)*objp)->rp_value_node.begin();
		    ite != ((rgm_value_t *)*objp)->rp_value_node.end(); ++ite,
		    count++);
		break;
	case XDR_DECODE:
		break;
	}

	if (xdrs->x_op != XDR_FREE)
		if (!xdr_u_int(xdrs, &count))
			return (FALSE);

	switch (xdrs->x_op) {
	case XDR_ENCODE:
	case XDR_FREE:
		for (ite = ((rgm_value_t *)*objp)->rp_value_node.begin();
		    ite != ((rgm_value_t *)*objp)->rp_value_node.end(); ++ite) {
			if (!xdr_string(xdrs, (char **)&ite->first, ~0))
				return (FALSE);
			if (!xdr_string(xdrs, &ite->second, ~0))
				return (FALSE);
		}
		break;
	case XDR_DECODE:
		for (; count > 0; count--) {
			s = NULL;
			if (!xdr_string(xdrs, &s, ~0))
				return (FALSE);
			rp_value_node = NULL;
			if (!xdr_string(xdrs, &rp_value_node, ~0))
				return (FALSE);
			rgmv->rp_value_node[s] = rp_value_node;
		}

		*objp = (value_t)(rgmv);
		break;
	}

	return (TRUE);
}

bool_t
xdr_rgm_property_t(register XDR *xdrs, rgm_property_t *objp)
{

	if (!xdr_string(xdrs, &objp->rp_key, ~0))
		return (FALSE);
	if (!xdr_value_t(xdrs, &objp->rp_value))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rp_array_values,
	    sizeof (namelist_t), (xdrproc_t)xdr_namelist_t))
		return (FALSE);
	if (!xdr_scha_prop_type_t(xdrs, &objp->rp_type))
		return (FALSE);
	if (!xdr_string(xdrs, &objp->rp_description, ~0))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->is_per_node))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rgm_property_list_t(register XDR *xdrs, rgm_property_list_t *objp)
{

	if (!xdr_pointer(xdrs, (char **)&objp->rpl_property,
	    sizeof (rgm_property_t), (xdrproc_t)xdr_rgm_property_t))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rpl_next,
		sizeof (rgm_property_list_t),
		(xdrproc_t)xdr_rgm_property_list_t))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_switch_t(register XDR *xdrs, switch_t *objp)
{
	std::map<rgm_lni_t, scha_switch_t>::iterator it;
	std::map<std::string, scha_switch_t>::iterator ite;
	scha_switch_t schaswitch;
	char *r_switch_node = NULL;
	rgm_switch_t *rgms = NULL;
	rgm_lni_t lni;

	uint_t count;

	/* If we are encoding, get the number of elements in r_switch */
	switch (xdrs->x_op) {
	case XDR_ENCODE:
		for (count = 0, it = ((rgm_switch_t *)*objp)->r_switch.begin();
		    it != ((rgm_switch_t *)*objp)->r_switch.end(); ++it,
		    count++);
		break;
	case XDR_DECODE:
		break;
	}

	if (xdrs->x_op != XDR_FREE)
		if (!xdr_u_int(xdrs, &count))
			return (FALSE);

	switch (xdrs->x_op) {
	case XDR_ENCODE:
	case XDR_FREE:
		for (it = ((rgm_switch_t *)*objp)->r_switch.begin();
		    it != ((rgm_switch_t *)*objp)->r_switch.end(); ++it) {
			if (!xdr_rgm_lni_t(xdrs, (rgm_lni_t *)&it->first))
				return (FALSE);
			if (!xdr_scha_switch_t(xdrs, &it->second))
				return (FALSE);
		}
		break;
	case XDR_DECODE:
		rgms = new rgm_switch_t;
		if (rgms == NULL)
			return (FALSE);

		for (; count > 0; count--) {
			if (!xdr_rgm_lni_t(xdrs, &lni))
				return (FALSE);
			if (!xdr_scha_switch_t(xdrs, &schaswitch))
				return (FALSE);
			rgms->r_switch[lni] = schaswitch;
		}
		break;
	}

	/* If we are encoding, get the number of elements in r_switch_node */
	switch (xdrs->x_op) {
	case XDR_ENCODE:
		for (count = 0,
		    ite = ((rgm_switch_t *)*objp)->r_switch_node.begin();
		    ite != ((rgm_switch_t *)*objp)->r_switch_node.end(); ++ite,
		    count++);
		break;
	case XDR_DECODE:
		break;
	}

	if (xdrs->x_op != XDR_FREE)
		if (!xdr_u_int(xdrs, &count))
			return (FALSE);

	switch (xdrs->x_op) {
	case XDR_ENCODE:
	case XDR_FREE:
		char *tmpstr;
		for (ite = ((rgm_switch_t *)*objp)->r_switch_node.begin();
		    ite != ((rgm_switch_t *)*objp)->r_switch_node.end();
		    ++ite) {
			tmpstr = (char *)ite->first.data();
			if (!xdr_string(xdrs, &tmpstr, ~0))
				return (FALSE);
			if (!xdr_scha_switch_t(xdrs, &ite->second))
				return (FALSE);
		}
		break;
	case XDR_DECODE:
		for (; count > 0; count--) {
			r_switch_node = NULL;
			if (!xdr_string(xdrs, &r_switch_node, ~0))
				return (FALSE);
			if (!xdr_scha_switch_t(xdrs, &schaswitch))
				return (FALSE);
			rgms->r_switch_node[r_switch_node] = schaswitch;
		}
		*objp = (void *) rgms;
		break;
	}

	return (TRUE);
}

bool_t
xdr_rgm_resource_t(register XDR *xdrs, rgm_resource_t *objp)
{

	if (!xdr_string(xdrs, &objp->r_name, ~0))
		return (FALSE);
	if (!xdr_string(xdrs, &objp->r_type, ~0))
		return (FALSE);
	if (!xdr_string(xdrs, &objp->r_rgname, ~0))
		return (FALSE);
	if (!xdr_switch_t(xdrs, &objp->r_onoff_switch))
		return (FALSE);
	if (!xdr_switch_t(xdrs, &objp->r_monitored_switch))
		return (FALSE);
	if (!xdr_rgm_dependencies_t(xdrs, &objp->r_dependencies))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->r_properties,
	    sizeof (rgm_property_list_t), (xdrproc_t)xdr_rgm_property_list_t))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->r_ext_properties,
	    sizeof (rgm_property_list_t), (xdrproc_t)xdr_rgm_property_list_t))
		return (FALSE);
	if (!xdr_string(xdrs, &objp->r_description, ~0))
		return (FALSE);
	if (!xdr_string(xdrs, &objp->r_project_name, ~0))
		return (FALSE);
	if (!xdr_string(xdrs, &objp->r_type_version, ~0))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rgm_obj_type_t(register XDR *xdrs, rgm_obj_type_t *objp)
{

	if (!xdr_enum(xdrs, (enum_t *)objp))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rgm_param_t(register XDR *xdrs, rgm_param_t *objp)
{

	if (!xdr_string(xdrs, &objp->p_name, ~0))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->p_extension))
		return (FALSE);
	if (!xdr_rgm_tune_t(xdrs, &objp->p_tunable))
		return (FALSE);
	if (!xdr_scha_prop_type_t(xdrs, &objp->p_type))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->p_defaultint))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->p_defaultbool))
		return (FALSE);
	if (!xdr_string(xdrs, &objp->p_defaultstr, ~0))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->p_defaultarray,
	    sizeof (namelist_t), (xdrproc_t)xdr_namelist_t))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->p_min))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->p_max))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->p_arraymin))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->p_arraymax))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->p_enumlist,
	    sizeof (namelist_t), (xdrproc_t)xdr_namelist_t))
		return (FALSE);
	if (!xdr_string(xdrs, &objp->p_description, ~0))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->p_default_isset))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->p_min_isset))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->p_max_isset))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->p_arraymin_isset))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->p_arraymax_isset))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->p_per_node))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rgm_paramtable(register XDR *xdrs, rgm_param_t ***objp)
{

	rgm_param_t **param;
	uint_t count;

	switch (xdrs->x_op) {
	case XDR_ENCODE:
		for (count = 0, param = *objp; *param; param++, count++);
		break;
	case XDR_DECODE:
		break;
	}

	if (xdrs->x_op != XDR_FREE)
		if (!xdr_u_int(xdrs, &count))
			return (FALSE);

	switch (xdrs->x_op) {
	case XDR_ENCODE:
	case XDR_FREE:
		for (param = *objp; *param; param++)
			if (!xdr_pointer(xdrs, (char **)param,
			    sizeof (rgm_param_t), (xdrproc_t)xdr_rgm_param_t))
				return (FALSE);
		break;
	case XDR_DECODE:
		*objp = (rgm_param_t **)mem_alloc((count + 2) *
		    sizeof (rgm_param_t *));
		if (*objp == NULL)
			return (FALSE);
		bzero(*objp, (count + 2) * sizeof (rgm_param_t *));
		for (param = *objp; count > 0; count--, param++)
			if (!xdr_pointer(xdrs, (char **)param,
			    sizeof (rgm_param_t), (xdrproc_t)xdr_rgm_param_t))
				return (FALSE);
		*param = 0;
		break;
	}

	return (TRUE);
}

bool_t
xdr_rgm_rt_upgrade_t(register XDR *xdrs, rgm_rt_upgrade_t *objp)
{

	if (!xdr_pointer(xdrs, (char **)&objp->rtu_version,
	    sizeof (char), (xdrproc_t)xdr_char))
		return (FALSE);
	if (!xdr_rgm_tune_t(xdrs, &objp->rtu_tunability))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rtu_next,
	    sizeof (rgm_rt_upgrade_t), (xdrproc_t)xdr_rgm_rt_upgrade_t))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rgm_rt_t(register XDR *xdrs, rgm_rt_t *objp)
{

	if (!xdr_name_t(xdrs, &objp->rt_name))
		return (FALSE);
	if (!xdr_name_t(xdrs, &objp->rt_basedir))
		return (FALSE);
	if (!xdr_rgm_methods_t(xdrs, &objp->rt_methods))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rt_single_inst))
		return (FALSE);
	if (!xdr_scha_initnodes_flag_t(xdrs, &objp->rt_init_nodes))
		return (FALSE);
	if (!xdr_nodeidlist_all_t(xdrs, &objp->rt_instl_nodes))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rt_failover))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rt_globalzone))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rt_proxy))
		return (FALSE);
	if (!xdr_rgm_sysdeftype_t(xdrs, &objp->rt_sysdeftype))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rt_dependencies,
	    sizeof (namelist_t), (xdrproc_t)xdr_namelist_t))
		return (FALSE);
	if (!xdr_int(xdrs, (int *)&objp->rt_api_version))
		return (FALSE);
	if (!xdr_name_t(xdrs, &objp->rt_version))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rt_pkglist, sizeof (namelist_t),
	    (xdrproc_t)xdr_namelist_t))
		return (FALSE);
	if (!xdr_rgm_paramtable(xdrs, (rgm_param_t ***)&objp->rt_paramtable))
		return (FALSE);
	if (!xdr_string(xdrs, &objp->rt_description, ~0))
		return (FALSE);
	if (!xdr_rgm_stamp_t(xdrs, &objp->rt_stamp))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rt_sc30))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rt_upgrade_from,
	    sizeof (rgm_rt_upgrade_t), (xdrproc_t)xdr_rgm_rt_upgrade_t))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rt_downgrade_to,
	    sizeof (rgm_rt_upgrade_t), (xdrproc_t)xdr_rgm_rt_upgrade_t))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rt_system))
		return (FALSE);
	return (TRUE);
}

bool_t
xdr_rgm_rg_t(register XDR *xdrs, rgm_rg_t *objp)
{

	if (!xdr_name_t(xdrs, &objp->rg_name))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rg_unmanaged))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rg_nodelist,
	    sizeof (nodeidlist_t), (xdrproc_t)xdr_nodeidlist_t))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->rg_max_primaries))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->rg_desired_primaries))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rg_failback))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rg_dependencies,
	    sizeof (namelist_t), (xdrproc_t)xdr_namelist_t))
		return (FALSE);
	if (!xdr_scha_rgmode_t(xdrs, &objp->rg_mode))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rg_impl_net_depend))
		return (FALSE);
	if (!xdr_namelist_all_t(xdrs, &objp->rg_glb_rsrcused))
		return (FALSE);
	if (!xdr_name_t(xdrs, &objp->rg_pathprefix))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rg_description,
	    sizeof (char), (xdrproc_t)xdr_char))
		return (FALSE);
	if (!xdr_rgm_stamp_t(xdrs, &objp->rg_stamp))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->rg_ppinterval))
		return (FALSE);
	if (!xdr_pointer(xdrs, (char **)&objp->rg_affinities,
	    sizeof (namelist_t), (xdrproc_t)xdr_namelist_t))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rg_auto_start))
		return (FALSE);
	if (!xdr_name_t(xdrs, &objp->rg_project_name))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rg_system))
		return (FALSE);
	if (!xdr_bool(xdrs, (bool_t *)&objp->rg_suspended))
		return (FALSE);
	if (!xdr_name_t(xdrs, &objp->rg_slm_type))
		return (FALSE);
	if (!xdr_name_t(xdrs, &objp->rg_slm_pset_type))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->rg_slm_cpu))
		return (FALSE);
	if (!xdr_int(xdrs, &objp->rg_slm_cpu_min))
		return (FALSE);
	return (TRUE);
}
