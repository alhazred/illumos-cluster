//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgmx_comm_pres_server.cc	1.6	08/05/20 SMI"

//
// rgmx_comm_pres_server.cc
//
// The functions in this file implement the server side of the internode
// Farm to Server communication through the RPC.
//
// RGM_COMM rgm protocol.
//

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <sys/sc_syslog_msg.h>
#include <rgm/rgm_msg.h>
#include <rgm_state.h>
#include <rgm_api.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>
#include <rgm/rpc/rgmx_comm_pres.h>

//
// RPC server handle on TCP network.
// Bound to local private Europa network address / port.
// Used also in rgmx_cnfg_server.cc (rgmx_cnfg_init)
//
SVCXPRT *svc_transp = NULL;

//
// rgmx_comm_pres disptacher
// Generated by rpcgen (rgmx_comm_pres_svc.c)
//
extern "C" {
void rgmx_comm_pres_program_1(struct svc_req *rqstp, register SVCXPRT *transp);
}

//
// Start the rgmx_comm_pres server and bind it to the local
// europa private address.
//
void
rgmx_comm_init()
{
	sol::nodeid_t nodeid;
	char *local_addr;
	sc_syslog_msg_handle_t handle;

	nodeid = orb_conf::local_nodeid();
	local_addr = rgmx_cfg_getaddr(nodeid);
	if (local_addr == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_comm_init: unable to get network private "
			"address; aborting node");
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}

	// Create the rcp/tcp server handle.
	// This is done once for both rgmx_comm_pres and
	// rgmx_cnfg rpc programs.
	if ((svc_transp = rgmx_svc_create(local_addr)) == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("rgmx_comm_init: unable to create "
		    "rpc/tcp server handle; aborting node"));
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}

	if (rgmx_svc_register(svc_transp,
		rgmx_comm_pres_program_1,
		RGMX_COMM_PRES_PROGRAM_NAME,
		RGMX_COMM_PRES_PROGRAM, RGMX_COMM_PRES_VERSION) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("rgmx_comm_init: unable to register "
		    "rgmx_comm_pres rpc service; aborting node"));
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}

	ucmm_print("RGM", NOGET("rgmx_comm_init: rgmx_comm_pres service created"
			" local address is %s\n"), local_addr);
	free(local_addr);
}

//
// Free RPC results.
// Called by rgmx_comm_pres_program_1 (dispatcher) after having
// sent the reply to the client.
//
int
rgmx_comm_pres_program_1_freeresult(SVCXPRT *, xdrproc_t xdr_result,
	caddr_t res)
{
	/*
	 * Insert additional freeing code here, if needed
	 */
	xdr_free(xdr_result, res);

	return (1);
}

//
// The following functions implement the server side of the internode
// Farm to Server communication through the RPC (RGM_COMM protocol part).
//

//
// rgmx_scha_rs_get_state_1_svc() -
//
//	Get the static state of a resource
//	wrapper to IDL call idl_scha_rs_get_state (rgm_rs_impl.cc)
//
bool_t
rgmx_scha_rs_get_state_1_svc(rgmx_rs_get_state_status_args *rpc_args,
    rgmx_get_result_t *rpc_res, struct svc_req *)
{
	rgm::idl_rs_get_state_status_args idl_args;
	rgm::idl_get_result_t_var idl_res;
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_get_result_t));
	idl_args.idlstr_rs_name = new_str(rpc_args->idlstr_rs_name);
	idl_args.idlstr_rg_name = new_str(rpc_args->idlstr_rg_name);
	idl_args.idlstr_node_name = new_str(rpc_args->idlstr_node_name);

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}
	prgm_serv->idl_scha_rs_get_state(idl_args, idl_res, e);
	if (e.exception() != NULL) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}
	rpc_res->ret_code = idl_res->ret_code;
	rpc_res->idlstr_seq_id = (idl_res->idlstr_seq_id != NULL) ?
		strdup(idl_res->idlstr_seq_id) :  NULL;
	rpc_res->idlstr_err_msg =  (idl_res->idlstr_err_msg != NULL) ?
		strdup(idl_res->idlstr_err_msg) : NULL;
	(void) strseq_to_array(idl_res->value_list, &rpc_res->value_list);
	return (TRUE);
}

//
// rgmx_scha_rs_get_status_1_svc() -
//
//	Get the dynamic state of a resource FM (set by fault monitor)
//	wrapper to IDL call idl_scha_rs_get_status (rgm_rs_impl.cc)
//
bool_t
rgmx_scha_rs_get_status_1_svc(rgmx_rs_get_state_status_args *rpc_args,
    rgmx_get_status_result_t *rpc_res, struct svc_req *)
{
	rgm::idl_rs_get_state_status_args idl_args;
	rgm::idl_get_status_result_t_var idl_res;
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_get_status_result_t));
	idl_args.idlstr_rs_name = new_str(rpc_args->idlstr_rs_name);
	idl_args.idlstr_rg_name = new_str(rpc_args->idlstr_rg_name);
	idl_args.idlstr_node_name = new_str(rpc_args->idlstr_node_name);

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}
	prgm_serv->idl_scha_rs_get_status(idl_args, idl_res, e);
	if (e.exception() != NULL) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}
	rpc_res->ret_code = idl_res->ret_code;

	rpc_res->idlstr_status = (idl_res->idlstr_status != NULL) ?
		strdup(idl_res->idlstr_status) :  NULL;
	rpc_res->idlstr_status_msg =  (idl_res->idlstr_status_msg != NULL) ?
		strdup(idl_res->idlstr_status_msg) : NULL;
	rpc_res->idlstr_seq_id = (idl_res->idlstr_seq_id != NULL) ?
		strdup(idl_res->idlstr_seq_id) :  NULL;
	rpc_res->idlstr_err_msg =  (idl_res->idlstr_err_msg != NULL) ?
		strdup(idl_res->idlstr_err_msg) : NULL;

	return (TRUE);
}

//
// rgmx_scha_rs_get_switch_1_svc() -
//
//	Get the on_off switch of a resource
//	wrapper to IDL call idl_scha_rs_get_switch (rgm_rs_impl.cc)
//
bool_t
rgmx_scha_rs_get_switch_1_svc(rgmx_rs_get_switch_args *rpc_args,
    rgmx_rs_get_switch_result_t *rpc_res, struct svc_req *)
{
	rgm::idl_rs_get_switch_args idl_args;
	rgm::idl_get_switch_result_t_var idl_res;
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_rs_get_switch_result_t));
	idl_args.idlstr_rs_name = new_str(rpc_args->idlstr_rs_name);
	idl_args.idlstr_rg_name = new_str(rpc_args->idlstr_rg_name);
	idl_args.idlstr_node_name = new_str(rpc_args->idlstr_node_name);
	idl_args.on_off = rpc_args->on_off;

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}
	prgm_serv->idl_scha_rs_get_switch(idl_args, idl_res, e);
	if (e.exception() != NULL) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}
	rpc_res->ret_code = idl_res->ret_code;

	rpc_res->idlstr_switch = (idl_res->idlstr_switch != NULL) ?
	    strdup(idl_res->idlstr_switch) : NULL;
	rpc_res->idlstr_seq_id = (idl_res->idlstr_seq_id != NULL) ?
		strdup(idl_res->idlstr_seq_id) :  NULL;
	rpc_res->idlstr_err_msg =  (idl_res->idlstr_err_msg != NULL) ?
		strdup(idl_res->idlstr_err_msg) : NULL;

	return (TRUE);
}

//
// rgmx_scha_rs_get_ext_1_svc() -
//
//	Get the Per-node extension property of a resource
//	wrapper to IDL call idl_scha_rs_get_ext (rgm_rs_impl.cc)
//
bool_t
rgmx_scha_rs_get_ext_1_svc(rgmx_rs_get_args *rpc_args,
    rgmx_rs_get_ext_result_t *rpc_res, struct svc_req *)
{
	rgm::idl_rs_get_args idl_args;
	rgm::idl_rs_get_ext_result_t_var idl_res;
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_rs_get_switch_result_t));
	idl_args.idlstr_rs_name = new_str(rpc_args->idlstr_rs_name);
	idl_args.idlstr_rg_name = new_str(rpc_args->idlstr_rg_name);
	idl_args.idlstr_rs_node_name = new_str(rpc_args->idlstr_rs_node_name);
	idl_args.idlstr_rs_prop_name = new_str(rpc_args->idlstr_rs_prop_name);

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}
	prgm_serv->idl_scha_rs_get_ext(idl_args, idl_res, e);
	if (e.exception() != NULL) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}

	rpc_res->ret_code = idl_res->ret_code;
	rpc_res->prop_type = idl_res->prop_type;
	rpc_res->prop_val = idl_res->prop_val;
	rpc_res->idlstr_seq_id = (idl_res->idlstr_seq_id != NULL) ?
		strdup(idl_res->idlstr_seq_id) :  NULL;

	return (TRUE);
}

//
// rgmx_scha_rs_get_fail_status_1_svc() -
//
//	Get the state flag indicating failure of a specified method
//	(BOOT, FINI, INIT or UPDATE)
//	wrapper to IDL call idl_scha_rs_get_fail_status (rgm_rs_impl.cc)
//
bool_t
rgmx_scha_rs_get_fail_status_1_svc(rgmx_rs_get_fail_status_args *rpc_args,
    rgmx_get_result_t *rpc_res, struct svc_req *)
{
	rgm::idl_rs_get_fail_status_args idl_args;
	rgm::idl_get_result_t_var idl_res;
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_get_result_t));
	idl_args.idlstr_rs_name = new_str(rpc_args->idlstr_rs_name);
	idl_args.idlstr_rg_name = new_str(rpc_args->idlstr_rg_name);
	idl_args.idlstr_node_name = new_str(rpc_args->idlstr_node_name);
	idl_args.idlstr_method_name = new_str(rpc_args->idlstr_method_name);

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}
	prgm_serv->idl_scha_rs_get_fail_status(idl_args, idl_res, e);
	if (e.exception() != NULL) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}
	rpc_res->ret_code = idl_res->ret_code;
	rpc_res->idlstr_seq_id = (idl_res->idlstr_seq_id != NULL) ?
		strdup(idl_res->idlstr_seq_id) :  NULL;
	rpc_res->idlstr_err_msg =  (idl_res->idlstr_err_msg != NULL) ?
		strdup(idl_res->idlstr_err_msg) : NULL;
	(void) strseq_to_array(idl_res->value_list, &rpc_res->value_list);
	return (TRUE);
}

//
// rgmx_scha_rs_set_status_1_svc() -
//
//	Set the dynamic status of a resource FM (set by fault monitor)
//	wrapper to idl call idl_scha_rs_set_status (rgm_rs_impl.cc)
//
bool_t
rgmx_scha_rs_set_status_1_svc(rgmx_rs_set_status_args *rpc_args,
    rgmx_set_status_result_t *rpc_res, struct svc_req *)
{
	rgm::idl_rs_set_status_args idl_args;
	rgm::idl_set_status_result_t_var idl_res;
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_set_status_result_t));
	idl_args.idlstr_rs_name = new_str(rpc_args->idlstr_rs_name);
	idl_args.idlstr_rg_name = new_str(rpc_args->idlstr_rg_name);
	idl_args.idlstr_node_name = new_str(rpc_args->idlstr_node_name);
	idl_args.idlstr_status = new_str(rpc_args->idlstr_status);
	idl_args.idlstr_status_msg = new_str(rpc_args->idlstr_status_msg);

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}

	prgm_serv->idl_scha_rs_set_status(idl_args, idl_res, e);
	if (e.exception() != NULL) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}

	rpc_res->ret_code = idl_res->ret_code;
	rpc_res->idlstr_err_msg =  (idl_res->idlstr_err_msg != NULL) ?
		strdup(idl_res->idlstr_err_msg) : NULL;

	return (TRUE);
}

//
// rgmx_scha_rg_get_state_1_svc() -
//
//	Fetch RG state (on local node or given node) from the president.
//	Implements scha_resourcegroup_get RG_STATE and RG_STATE_NODE optags.
//	wrapper to idl call idl_scha_rg_get_state (rgm_rg_impl.cc)
//
bool_t
rgmx_scha_rg_get_state_1_svc(rgmx_rg_get_state_args *rpc_args,
    rgmx_get_result_t *rpc_res, struct svc_req *)
{
	rgm::idl_rg_get_state_args idl_args;
	rgm::idl_get_result_t_var idl_res;
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_get_result_t));
	idl_args.idlstr_rg_name = new_str(rpc_args->idlstr_rg_name);
	idl_args.idlstr_node_name = new_str(rpc_args->idlstr_node_name);

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}
	prgm_serv->idl_scha_rg_get_state(idl_args, idl_res, e);
	if (e.exception() != NULL) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}
	rpc_res->ret_code = idl_res->ret_code;
	rpc_res->idlstr_seq_id = (idl_res->idlstr_seq_id != NULL) ?
		strdup(idl_res->idlstr_seq_id) :  NULL;
	rpc_res->idlstr_err_msg =  (idl_res->idlstr_err_msg != NULL) ?
		strdup(idl_res->idlstr_err_msg) : NULL;
	(void) strseq_to_array(idl_res->value_list, &rpc_res->value_list);

	return (TRUE);
}

//
// rgmx_scha_get_node_state_1_svc() -
//
//	Get the cluster node state
//	wrapper to idl call idl_scha_get_node_state
//
bool_t
rgmx_scha_get_node_state_1_svc(rgmx_cluster_get_args *rpc_args,
    rgmx_get_result_t *rpc_res, struct svc_req *)
{
	rgm::idl_cluster_get_args idl_args;
	rgm::idl_get_result_t_var idl_res;
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_get_result_t));
	idl_args.idlstr_node_name = new_str(rpc_args->idlstr_node_name);

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}

	prgm_serv->idl_scha_get_node_state(idl_args, idl_res, e);
	if (e.exception() != NULL) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}

	rpc_res->ret_code = idl_res->ret_code;
	rpc_res->idlstr_seq_id = (idl_res->idlstr_seq_id != NULL) ?
		strdup(idl_res->idlstr_seq_id) :  NULL;
	rpc_res->idlstr_err_msg =  (idl_res->idlstr_err_msg != NULL) ?
		strdup(idl_res->idlstr_err_msg) : NULL;
	(void) strseq_to_array(idl_res->value_list, &rpc_res->value_list);

	return (TRUE);
}

//
// rgmx_scha_control_giveover_1_svc() -
//
//	wrapper to idl call idl_scha_control_giveover (rgm_scha_control_impl.cc)
//
bool_t
rgmx_scha_control_giveover_1_svc(rgmx_scha_control_args *rpc_args,
    rgmx_scha_control_result *rpc_res, struct svc_req *)
{
	rgm::idl_scha_control_args idl_args;
	rgm::idl_scha_control_result_var idl_res;
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_scha_control_result));
	idl_args.lni =  rpc_args->lni;
	idl_args.idlstr_R_name = new_str(rpc_args->idlstr_R_name);
	idl_args.idlstr_rg_name = new_str(rpc_args->idlstr_rg_name);
	idl_args.flags = rpc_args->flags;

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}

	prgm_serv->idl_scha_control_giveover(idl_args, idl_res, e);
	if (e.exception() != NULL) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}

	rpc_res->ret_code = idl_res->ret_code;
	rpc_res->idlstr_err_msg =  (idl_res->idlstr_err_msg != NULL) ?
		strdup(idl_res->idlstr_err_msg) : NULL;
	rpc_res->idlstr_syslog_msg =  (idl_res->idlstr_syslog_msg != NULL) ?
		strdup(idl_res->idlstr_syslog_msg) : NULL;

	return (TRUE);
}

//
// rgmx_scha_control_restart_1_svc() -
//
//	wrapper to idl call idl_scha_control_restart (rgm_scha_control_impl.cc)
//
bool_t
rgmx_scha_control_restart_1_svc(rgmx_scha_control_args *rpc_args,
    rgmx_regis_result_t *rpc_res, struct svc_req *)
{
	rgm::idl_scha_control_args idl_args;
	rgm::idl_regis_result_t_var idl_res;
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_regis_result_t));
	idl_args.lni = rpc_args->lni;
	idl_args.idlstr_R_name = new_str(rpc_args->idlstr_R_name);
	idl_args.idlstr_rg_name = new_str(rpc_args->idlstr_rg_name);
	idl_args.flags = rpc_args->flags;

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}

	prgm_serv->idl_scha_control_restart(idl_args, idl_res, e);
	if (e.exception() != NULL) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}

	rpc_res->ret_code = idl_res->ret_code;
	rpc_res->idlstr_err_msg =  (idl_res->idlstr_err_msg != NULL) ?
		strdup(idl_res->idlstr_err_msg) : NULL;

	return (TRUE);
}

//
// rgmx_scha_control_restart_1_svc() -
//
//	wrapper to idl call idl_scha_control_restart (rgm_scha_control_impl.cc)
//
bool_t
rgmx_scha_control_disable_1_svc(rgmx_scha_control_args *rpc_args,
    rgmx_regis_result_t *rpc_res, struct svc_req *)
{
	rgm::idl_scha_control_args idl_args;
	rgm::idl_regis_result_t_var idl_res;
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_regis_result_t));
	idl_args.lni = rpc_args->lni;
	idl_args.idlstr_R_name = new_str(rpc_args->idlstr_R_name);
	idl_args.idlstr_rg_name = new_str(rpc_args->idlstr_rg_name);
	idl_args.flags = rpc_args->flags;

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}

	prgm_serv->idl_scha_control_disable(idl_args, idl_res, e);
	if (e.exception() != NULL) {
		rpc_res->ret_code = SCHA_ERR_ORB;
		return (TRUE);
	}

	rpc_res->ret_code = idl_res->ret_code;
	rpc_res->idlstr_err_msg =  (idl_res->idlstr_err_msg != NULL) ?
		strdup(idl_res->idlstr_err_msg) : NULL;

	return (TRUE);
}

//
// rgmx_notify_me_state_change_1_svc() -
//
//	wrapper to idl call notify_me_state_change (rgm_comm_impl.cc)
//
bool_t
rgmx_notify_me_state_change_1_svc(rgmx_notify_me_state_change_args *rpc_args,
    void *,
    struct svc_req *)
{
	rgm::rgm_comm_var prgm_serv;
	Environment e;

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		return (FALSE);
	}

	prgm_serv->notify_me_state_change(rpc_args->lni,
	    (rgm::notify_change_tag)rpc_args->change_tag,
	    rpc_args->R_RG_name, (rgm::rgm_r_state)rpc_args->rstate,
	    (rgm::rgm_rg_state)rpc_args->rgstate, e);

	return (TRUE);
}

//
// rgmx_zone_state_change_1_svc() -
//
//	wrapper to idl call idl_zone_state_change (rgm_comm_impl.cc)
//
bool_t
rgmx_zone_state_change_1_svc(rgmx_zone_state_change_args *rpc_args,
    rgmx_zone_state_change_result *rpc_res, struct svc_req *)
{
	rgm::rgm_comm_var prgm_serv;
	rgm::lni_t assigned_lni = LNI_UNDEF;
	Environment e;

	bzero(rpc_res, sizeof (rgmx_zone_state_change_result));

	prgm_serv = rgm_comm_getref(orb_conf::local_nodeid());
	if (CORBA::is_nil(prgm_serv)) {
		return (FALSE);
	}

	prgm_serv->idl_zone_state_change(rpc_args->node,
	    rpc_args->zonename, (rgm::rgm_ln_state) rpc_args->state,
	    rpc_args->incarn, assigned_lni, e);

	rpc_res->assigned_lni = assigned_lni;
	return (TRUE);
}

//
// rgmx_scha_cluster_open_1_svc() -
//
//	this function calls scha_cluster_open to get
//	cluster incarnation number
//
bool_t
rgmx_scha_cluster_open_1_svc(void *, open_result_t *result,
	struct svc_req *rqstp)
{
#if DOOR_IMPL
	return (scha_cluster_open_1_svc(NULL, result));
#else
	return (scha_cluster_open_1_svc(NULL, result, rqstp));
#endif
}

//
// rgmx_scha_cluster_get_1_svc() -
//
//	this function calls both scha_cluster_get and scha_farm_get routines
//	to retrieve the cluster information (server + farm).
//
bool_t
rgmx_scha_cluster_get_1_svc(cluster_get_args *arg, get_result_t *result,
    struct svc_req *rqstp)
{
#if DOOR_IMPL
	return (scha_cluster_get_1_svc(arg, result));
#else
	return (scha_cluster_get_1_svc(arg, result, rqstp));
#endif
}
