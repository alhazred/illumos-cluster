/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * rgmx_main.cc
 *
 *	Europa specific rgmd startup functions
 */

#pragma ident	"@(#)rgmx_main.cc	1.4	08/05/20 SMI"

#include <cmm/ucmm_api.h>
#include <sys/sc_syslog_msg.h>
#include <scxcfg/scxcfg.h>
#include <rgm/rgm_msg.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>

// Zones support
#include <rgm_logical_node_manager.h>

#ifdef EUROPA_FARM
#include <signal.h>
#include <sys/st_failfast.h>
#endif

// Global variables

#ifndef EUROPA_FARM

void
rgmx_startup(va_list ap)
{
	sc_syslog_msg_handle_t handle;
	scconf_nodeid_t nodeid;
	char *nodename;

	// Reset Server+Farm memberships ...
	rgmx_init_membership();

	// Create FMM thread and listen to Farm membership changes.
	thr_create_err(NULL, 0, fmm_thread_start, NULL, THR_BOUND, NULL);

	// Start rgmx_comm_pres RPC server.
	// Farm to Server communication through the RPC:
	// RGM_COMM protocol.
	rgmx_comm_init();

	// Start rgmx_cnfg RPC server.
	// Farm to Server communication through the RPC:
	// Request CCR/name Server information to the Server President.
	rgmx_cnfg_init();

	nodename = rgm_get_nodename(orb_conf::local_nodeid());
	if (nodename == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_startup(): Cannot get nodename");
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHED
	}

	ucmm_print("RGM", NOGET("Starting Server RGM id = %d, name = %s\n"),
		orb_conf::local_nodeid(), nodename);
	free(nodename);
}

#else // !EUROPA_FARM

void
rgmx_startup(va_list ap)
{
	sc_syslog_msg_handle_t handle;
	scxcfg_nodeid_t nodeid;
	char *nodename;

	if (rgm_get_nodeid(NULL, &nodeid) != 0) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_startup(): Cannot get nodeid");
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHED
	}
	nodename = rgm_get_nodename(nodeid);
	if (nodename == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_startup(): Cannot get nodename");
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHED
	}

	// Creating LogicalNode for myself (farm node).
	// lni = nodeid.
	// The initial state might need to be rgm::LN_UP.
	// If we mark the farm node as DOWN, we need to find a place to
	// mark it as UP (for slaves, this is done in slave_record_memb).
	(void) lnManager->createLogicalNodeFromMapping((sol::nodeid_t)nodeid,
		    NULL, (rgm::lni_t)nodeid, rgm::LN_UP);


	// Start rgmx_comm_farm RPC server.
	// Server to Farm communication through the RPC:
	// RGM_COMM protocol.
	rgmx_comm_init();

	ucmm_print("RGM", NOGET("Starting Farm RGM id = %d, name = %s\n"),
		nodeid, nodename);
	free(nodename);
}

#endif // !EUROPA_FARM

//
// Read Europa configuration  and initialize fencing mechanism.
// This function is called during RGM startup (startrgmd).
// The configuration is stored in a memory three and will
// get accessed through rgmx_cfg API (rgmx_cfg.cc).
//
void
rgmx_cfg_init(va_list ap)
{
	sc_syslog_msg_handle_t handle;
	dbg_print_buf *old_rgm_dbg_buf;

	// Read Europa cluster configuration.
	if (rgmx_cfg_open() == -1) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_set_maxnodes(): Cannot read configuration");
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHED
	}

#ifndef EUROPA_FARM
	//
	// Init farm fencing functions.
	//
	ucmm_print("RGM", NOGET("Calling  rgmx_fence_open\n"));
	if (rgmx_farm_fence_open(&Cfg) == -1) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_startup(): Cannot start fencing functions ");
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHED
	}
#endif

	// Set size of ring buffer for tracing.
	old_rgm_dbg_buf = rgm_dbg_buf;
	rgm_dbg_buf = new dbg_print_buf(rgmx_cfg_getbufsize());
	delete old_rgm_dbg_buf;
}

//
// Note that the RPC server is configured in MT mode
// with the maximum number of threads set to 64.
// For linux, this is done in recep_thread_start().
//

#define	MAX_RPC_THREADS		64
void
rgmx_rpc_start(va_list ap)
{
#ifndef linux
	sc_syslog_msg_handle_t handle;
	int mode = RPC_SVC_MT_AUTO;
	int cur_rpc_threads = MAX_RPC_THREADS;

	// set MT mode
	if (!rpc_control(RPC_SVC_MTMODE_SET, &mode)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("rgmx_rpc_start: rpc_control() failed to set "
		    "automatic MT mode; aborting node"));
		sc_syslog_msg_done(&handle);
		abort();
		/* NOTREACHED */
	}

	// Initialize default for maximum RPC threads.
	if (!rpc_control(RPC_SVC_THRMAX_SET, &cur_rpc_threads)) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("rgmx_rpc_start: rpc_control() failed to set "
		    "the maximum number of threads"));
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHED
	}
#endif
	svc_run();
}
