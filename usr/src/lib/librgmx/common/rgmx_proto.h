/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * rgmx_proto.h
 *
 * EUROPA specific file.
 *
 * function prototypes and global variable declarations for rgm module
 * external interfaces
 *
 */

#ifndef	_RGMX_PROTO_H
#define	_RGMX_PROTO_H

#pragma ident	"@(#)rgmx_proto.h	1.5	08/07/21 SMI"

#include <rgm_state.h>
#include <rgm/rgm_recep.h>	/* needed for strarr_list defintion */
#include <rpc/rpc.h>
#include <rgmx_hook.h>
#include <scxcfg/scxcfg.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Function prototypes
 */

/*
 * Common prototypes to RGM server and RGM delegate.
 */

boolean_t in_current_farm_membership(sol::nodeid_t node);

/*
 * RPC Client handle / Transport creation functions (based on TCP/socket).
 * Used for Europa private communication between Server/Farm RGMs.
 */
CLIENT *
rgmx_clnt_create(char *remote_addr,		/* remote server address */
		    char *program_name,		/* program name */
		    rpcprog_t program_num,	/* program number */
		    rpcvers_t version,		/* version */
		    bool_t keepalive);		/* keepalive option */

SVCXPRT *
rgmx_svc_create(char *local_addr);	/* local server address */

/*
 * Associate the program with the dispatch  procedure.
 * Protocol is set to 0, as we don't need to register
 * the service with  the  portmap  service.
 */
typedef void (*svc_program_t)(struct svc_req *, SVCXPRT *);

int
rgmx_svc_register(SVCXPRT *transp,
    svc_program_t svc_program,
    char *program_name,		/* program name */
    rpcprog_t program_num,	/* program number */
    rpcvers_t version);		/* version */

void
rgmx_clnt_destroy(CLIENT *clnt);

/*
 * Client side of the internode Server/Farm - Farm/Server communication
 * through the RPC.
 */
void rgmx_comm_freecnt(CLIENT *);

/*
 * Server side of the internode Farm to Server communication through the RPC.
 */
void rgmx_comm_init();

/*
 * Configuration access functions.
 */
int rgmx_cfg_open();
uint_t rgmx_cfg_getmaxnodes();
char *rgmx_cfg_getaddr(scxcfg_nodeid_t nodeid);
int rgmx_cfg_getbufsize();
int rgmx_cfg_getprivatehostname(scxcfg_nodeid_t nodeid, char **privhostnamep);
void rgmx_cfg_freelist(f_property_t *fp);
extern scxcfg Cfg;

#ifndef EUROPA_FARM

/*
 * Prototypes specific to RGM server (rgmd)
 */

/*
 * Membership management.
 */
void *fmm_thread_start(void *arg);
void rgmx_init_membership();

/*
 * Fencing.
 */
int rgmx_farm_fence_open(scxcfg_t cfg);
void rgmx_update_farm_fencing(LogicalNodeset &curr);

/*
 * Client side of the internode Server to Farm communication through the RPC.
 */
CLIENT *rgmx_comm_getref(sol::nodeid_t nid);

/*
 * Server side of the internode Farm to Server communication through the RPC.
 */
void rgmx_cnfg_init();

/*
 * Configuration access functions.
 */
int rgmx_cfg_isfarmnode(scxcfg_nodeid_t nodeid, char *nodename);
int rgmx_cfg_getnodeid(char *nodename, scxcfg_nodeid_t *nodeidp);
int rgmx_cfg_getnodename(scxcfg_nodeid_t nodeid, char **nodenamep);
int rgmx_cfg_getservernodes(f_property_t **farm_node_list, int *num);
int rgmx_cfg_getfarmnodes(f_property_t **farm_node_list, int *num);

/*
 * Extension to receptionist "scha_cluster_get" to handle farm nodes.
 */
bool_t
scha_farm_get(cluster_get_args *arg, get_result_t *result,
    struct svc_req *rqstp);

#else // EUROPA_FARM

/*
 * Prototypes specific to RGM delegate (frgmd).
 */

boolean_t nodename_in_nodelist(const char *nodename, namelist_t *nl);
boolean_t nodename_in_compute_meth_nodes(rgm_rt *rt, rgm_rg_t *rgl_ccr,
    method_t method, boolean_t is_scalable, const char *nodename);

/*
 * Used for the RPC2IDL mapping of parameters.
 */
void array_to_strseq_2(strarr_list *str_list, rgm::idl_string_seq_t &str_seq);
void copy_string_array(strarr_list *in_list, strarr_list **out_list);

/*
 * Used by librgmfarm/common/rgmx_cnfg.cc:
 *
 * namelist_copy & property_list_copy
 * Helper functions for copying namelists and property lists, which are
 * list structures used in an rgm_resource_t structure.
 * Defined in lib/librgm/common/rgm_cnfg.cc
 */
rgm_property_list_t *property_list_copy(rgm_property_list_t *src);
scha_errmsg_t rgmcnfg_copy_resource(const rgm_resource_t *src,
	rgm_resource_t **dest_p);

/*
 * Client side of the internode Farm to Server communication through the RPC.
 */
void rgmx_clnt_list_add(CLIENT *clnt);
bool_t rgmx_clnt_list_remove(CLIENT *clnt);
void rgmx_clnt_list_abort();
char *rgmx_getpres(char *cluster_name);
void rgmx_setpres(char *pres_addr);
CLIENT *rgmx_comm_getpres(char *cluster_name);
CLIENT *rgmx_cnfg_getpres(char *cluster_name);
void rgmx_cnfg_freecnt(CLIENT *);

/*
 * Configuration access functions.
 * Get nodeid/nodename from local configuration cache.
 */
int rgmx_cfg_getnode(scxcfg_nodeid_t *nodeid,  char **nodename);

#endif // !EUROPA_FARM

#ifdef __cplusplus
}
#endif

#endif /* _RGMX_PROTO_H */
