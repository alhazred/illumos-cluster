/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgmx_farm_fencing.cc	1.4	08/05/20 SMI"

//
// rgmx_farm_fencing
//
// Support for the fencing activities performed by the RGM in
// "Europa" mode.
//

#include <sys/os.h>
#include <sys/sc_syslog_msg.h>
#include <nslib/ns.h>
#include <dc/sys/dc_lib.h>
#include <dc/libdcs/libdcs.h>
#include <scxcfg/scxcfg.h>
#include <dlfence.h>
#include <fence.h>
#include <rgm/rgm_msg.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>

//
// rgmx_farm_fencing
//
// Support of fencing functions that are used to fence
// farm nodes which are not seen.
//

//
// libfence API, dynamically loaded.
//
static void *instance;
static fence_api_t api;

//
// Globals needed from elsewhere.
//
extern LogicalNodeset *farm_configured_nodes;
extern LogicalNodeset *last_farm_membership;


//
// rgmx_fence_open()
//
// Load the libfence library support and initialize it.
// This is called once at RGM startup time.
//
int
rgmx_farm_fence_open(scxcfg_t cfg)
{
	sc_syslog_msg_handle_t handle;
	//
	// First of all, load fence dynamic library.
	//
	if (fence_load_module(&instance, &api) == -1) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
			MESSAGE, "rgmx_fence_open: fence_load_module failed\n");
		sc_syslog_msg_done(&handle);
		return (-1);
	}
	//
	// Init fencing functions.
	//
	if (api.fenceInitialize(cfg) != FENCE_OK) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_fence_open: fenceInitialize failed");
		sc_syslog_msg_done(&handle);
		return (-1);
	}
	return (0);
}

//
// rgmx_fence_nodes()
//
void
rgmx_fence_farm_nodes(LogicalNodeset &lns)
{
	sc_syslog_msg_handle_t handle;
	nodeList_t nl;
	Node *n;
	char *nodeName;
	int i, rc;
	//
	// Build node list.
	//
	ucmm_print("RGM", NOGET("fence farm nodes:"));
	i = lns.firstFarmNodeSet() - 1;
	while ((i = lns.nextFarmNodeSet(i + 1)) != 0) {
		if (rgmx_cfg_getnodename(i, &nodeName)) {
			// error retrieving nodename
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			(void) sc_syslog_msg_log(handle, LOG_WARNING,
			    MESSAGE, "rgmx_fence_nodes: getnodename "
			    "error for node %d\n", i);
			sc_syslog_msg_done(&handle);
			continue;
		}
		ucmm_print("RGM", NOGET("%s "), nodeName);
		n = new Node(nodeName);
		free(nodeName);
		if (!n) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			(void) sc_syslog_msg_log(handle, LOG_WARNING,
			    MESSAGE, "rgmx_fence_nodes: no memory ");
			sc_syslog_msg_done(&handle);
			return;
		}
		nl.push_back(n);
	}
	ucmm_print("RGM", NOGET("\n"));
	//
	// Fence the nodes contained in node list.
	//
	rc = api.fenceNodes(nl);
	if (rc != FENCE_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_WARNING,
		    MESSAGE, "api.fenceNodes Failed");
		sc_syslog_msg_done(&handle);
		return;
	}
}

//
// rgmx_unfence_nodes()
//
void
rgmx_unfence_farm_nodes(LogicalNodeset &lns)
{
	sc_syslog_msg_handle_t handle;
	nodeList_t nl;
	Node *n;
	char *nodeName;
	int i, rc;
	//
	// Build node list.
	//
	ucmm_print("RGM", NOGET("UNfence farm nodes:"));
	i = lns.firstFarmNodeSet() - 1;
	while ((i = lns.nextFarmNodeSet(i + 1)) != 0) {
		if (rgmx_cfg_getnodename(i, &nodeName)) {
			// error retrieving nodename
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			(void) sc_syslog_msg_log(handle, LOG_WARNING,
			    MESSAGE, "rgmx_unfence_nodes: getnodename "
			    "error for node %d\n", i);
			sc_syslog_msg_done(&handle);
			continue;
		}
		ucmm_print("RGM", NOGET("%s "), nodeName);
		n = new Node(nodeName);
		if (!n) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_WARNING,
			    MESSAGE, "rgmx_fence_nodes: no memory ");
			sc_syslog_msg_done(&handle);
			return;
		}
		nl.push_back(n);
	}
	ucmm_print("RGM", NOGET("\n"));
	//
	// Unfence the nodes contained in node list.
	//
	rc = api.unFenceNodes(nl);
	if (rc != FENCE_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_WARNING,
		    MESSAGE, "api.unFenceNodes Failed");
		sc_syslog_msg_done(&handle);
		return;
	}
}

//
// rgmx_update_farm_fencing()
//
// Update the fencing status of farm nodes according to the current
// view of the RGM president.
//
void
rgmx_update_farm_fencing(LogicalNodeset &currFarmNS)
{
	LogicalNodeset nodes;
	char *ns_buffer;

	if (farm_configured_nodes->display(ns_buffer) == 0) {
		ucmm_print("RGM", NOGET("rgmx_update_farm_fencing: "
		    "farm configured nodes ns=%s\n"), ns_buffer);
		free(ns_buffer);
	}
	if (currFarmNS.display(ns_buffer) == 0) {
		ucmm_print("RGM", NOGET("rgmx_update_farm_fencing: "
		    "current farm membership ns=%s\n"), ns_buffer);
		free(ns_buffer);
	}

	//
	// rgmx_update_farm_fencing() is called  during rgm_reconfig
	// by rgm_trigger_farm_fencing().
	//
	if (last_farm_membership->display(ns_buffer) == 0) {
		ucmm_print("RGM", NOGET("rgmx_update_farm_fencing: "
		    "last farm membership ns=%s\n"), ns_buffer);
		free(ns_buffer);
	}

	//
	// Unfence only nodes that were not in previous membership.
	//
	nodes = currFarmNS - *last_farm_membership;

	if (!nodes.isEmpty())
		rgmx_unfence_farm_nodes(nodes);

	//
	// Remember current farm membership for next time.
	//
	*last_farm_membership = currFarmNS;

	//
	// Fence nodes that are configured and not in current membership,
	// unconditionnaly. There is no optimization to try to
	// fence only the nodes that were not already fenced.
	// This takes into account any new node that would have
	// been added in the configuration.
	//
	nodes = *farm_configured_nodes - currFarmNS;

	if (!nodes.isEmpty())
		rgmx_fence_farm_nodes(nodes);
}

//
// rgmx_farm_check_for_fencing()
//
// Verify if there is a fencing lock pending for farm nodes.
//
rgmx_farm_check_for_fencing(const char *lock_name, int timeout,
    unsigned int retry_interval)
{
	unsigned int i;
	int rt;
	Environment e;
	CORBA::Exception *ex;
	char *tempname;
	char *fence_lock;
	bool lock_cleared;
	f_property_t *farm_node_list = NULL;
	int nb_farm_nodes = 0;

	if (lock_name == NULL)
		return (-2);

	if (timeout < -2)
		return (-2);

	if (retry_interval < 1)
		return (-2);

	// setup default timeout if specified
	// XXX
	if (timeout == -2)
		timeout = 2;
	//
	// Get all farm nodes.
	//
	if (rgmx_cfg_getfarmnodes(&farm_node_list, &nb_farm_nodes) == -1) {
		ucmm_print("RGM", NOGET("rgmx_cfg_getfarmnodes error\n"));
		return (-1);
	}

	// setup infinite loop for synchronous call
	// disable retries for immediate return call
	if ((timeout == -1) || (timeout == 0))
		rt = timeout;
	else
		rt = timeout / (int)retry_interval;

	// Contact the nameserver
	naming::naming_context_var ctxv = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctxv));

	fence_lock = new char [strlen(lock_name) + CL_MAX_LEN];
	if (fence_lock == NULL)
		return (-1);

	f_property_t *v;
	for (i = 0, v = farm_node_list;
	    i < nb_farm_nodes && v;
	    i++, v = v->next) {
		uint32_t n;

		n = atoi(v->value);
		if (rgmx_cfg_getnodename(n, &tempname) == -1)
			continue;
		if (tempname == NULL)
			continue;
		(void) sprintf(fence_lock, "%s.%s", lock_name, tempname);

		(void) ctxv->resolve(fence_lock, e);
		ex = e.exception();
		if (ex != NULL) {
			// looking for a not_found exception
			if (naming::not_found::_exnarrow(ex) != NULL) {
				e.clear();
				// XXX
				continue;
			}
			e.clear();
		}
		//
		// Found, will sleep.
		//
		lock_cleared = false;
		while (rt--) {
			// Fence lock is still held wait
			(void) sleep(retry_interval);
			(void) ctxv->resolve(fence_lock, e);
			ex = e.exception();
			if (ex != NULL) {
				// looking for a not_found exception
				if (naming::not_found::_exnarrow(ex) != NULL) {
					e.clear();
					lock_cleared = true;
					break;
				}
				e.clear();
			}
		}
		if (lock_cleared)
			continue;

		// XXX
		if (farm_node_list)
			rgmx_cfg_freelist(farm_node_list);
		delete [] fence_lock;
		return (1);
	}
	if (farm_node_list)
		rgmx_cfg_freelist(farm_node_list);
	delete [] fence_lock;
	return (0);
}

//
//
// Utility function to create the fencing lock for a farm node in the
// global context nameserver.
//
farm_mark_fenced_nodes(const char *node)
{
	Environment e;
	CORBA::Exception *ex;
	char *fence_call, *fence_lock;
	unsigned int i;

	//
	// Contact the global nameserver.
	//
	naming::naming_context_var ctxv = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctxv));

	// Store the fencing lock name in the new context
	fence_lock = new char[strlen("FENCE_LOCK.") + strlen(node) + 1];
	ASSERT(fence_lock != NULL);

	(void) sprintf(fence_lock, "FENCE_LOCK.%s", node);

#define	FENCE_LOCK_RETRIES 2

	for (i = 0; i < FENCE_LOCK_RETRIES; i++) {
		(void) ctxv->bind_new_context(fence_lock, e);
		if ((ex = e.exception()) != NULL) {
			if (naming::already_bound::_exnarrow(ex) != NULL) {
				// XX
				printf("fence_nodes(%s): lock already held.\n",
				    node);
				e.clear();
				break;
			} else {
				// XX
				printf("fence_nodes(): exception '%s' while "
				    "calling bind_new_context() for node "
					"'%s'.\n", ex->_name(), node);
				e.clear();
			}
		} else
			// succeeded in binding
			break;
	}
	delete fence_lock;
	return (0);
}
