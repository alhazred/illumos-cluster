/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgmx_cfg.cc	1.4	08/05/20 SMI"

/*
 * rgmx_cfg.cc
 *
 */

#include <sys/os.h>
#include <scxcfg/scxcfg.h>
#include <sys/sc_syslog_msg.h>
#include <rgm_proto.h>
#include <rgmx_proto.h>
#include <dlscxcfg.h>

/*
 * scxcfg API.
 */
static void *instance;
static scxcfg_api_t api;

/*
 * Properties used by the RGM.
 */
#define	PROP_RGM_MAX_FARM_NODES    "farm.properties.rgm.max_farm_nodes"
#define	PROP_RGM_DBG_BUFSIZE	   "farm.properties.rgm.dbg_bufsize"
#define	PROP_NODE_PRIVATE_HOSTNAME "farm.nodes.%d.properties.private_hostname"
#define	PROP_SERVER_NODES	   "farm.servers"
#define	PROP_FARM_NODES		   "farm.nodes"

/*
 * Europa configuration handle.
 */
scxcfg Cfg;

/*
 * Create the Europa configuration cache in memory.
 * This is called once at RGM startup time. The cache is automatically
 * refreshed by the libscxcfg itselves. Each time we call the library to
 * get a property value, the cache is updated in case if configuration changes.
 */
int
rgmx_cfg_open()
{
	scxcfg_error error;
	sc_syslog_msg_handle_t handle;
	char prop[FCFG_MAX_LEN];

	//
	// First of all, load scxcfg dynamic library.
	//
	if (scxcfg_load_module(&instance, &api) == -1) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR,
			MESSAGE, "rgmx_cfg_open: scxcfg_load_module failed\n");
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	if (api.scxcfg_open(&Cfg, &error) != FCFG_OK) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_cfg_open: scxcfg_open failed");

		sc_syslog_msg_done(&handle);
		return (-1);
	}

	return (0);
}

#define	RGM_MAX_FARMNODES	512
uint_t
rgmx_cfg_getmaxnodes()
{
	char property[FCFG_MAX_LEN];
	char prop[FCFG_MAX_LEN];
	scxcfg_error error;
	uint_t maxfarmnodes;
	sc_syslog_msg_handle_t handle;

	strcpy(property, PROP_RGM_MAX_FARM_NODES);
	if (api.scxcfg_getproperty_value(&Cfg, NULL,
			property, prop, &error) != FCFG_OK) {
		return (RGM_MAX_FARMNODES);
	}

	if (sscanf(prop, "%d", &maxfarmnodes) != 1) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_cfg_getmaxnodes: invalid property value");
		sc_syslog_msg_done(&handle);
		return (RGM_MAX_FARMNODES);
	}

	return (maxfarmnodes);
}

/*
 * Get address of a given node (Server or farm) on the private europa
 * network.
 * Don't forget that address must be freed by the caller.
 */
char *
rgmx_cfg_getaddr(scxcfg_nodeid_t nodeid)
{
	char *ipaddr;
	scxcfg_error error;
	sc_syslog_msg_handle_t handle;

	ipaddr = (char *)malloc(FCFG_MAX_LEN);
	if (ipaddr == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_cfg_getaddr(%d) malloc failed", nodeid);
		sc_syslog_msg_done(&handle);
		return (NULL);
	}

	if (api.scxcfg_get_ipaddress(&Cfg, nodeid, ipaddr, &error) != FCFG_OK) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_cfg_getaddr(%d): scxcfg_get_ipaddress failed",
			nodeid);
		sc_syslog_msg_done(&handle);
		return (NULL);
	}

	return (ipaddr);
}

#define	RGMX_DBG_BUFSIZE	(128 * 1024) /* 128KB ring buffer tracing */
int
rgmx_cfg_getbufsize()
{
	char property[FCFG_MAX_LEN];
	char prop[FCFG_MAX_LEN];
	scxcfg_error error;
	int size;
	sc_syslog_msg_handle_t handle;

	strcpy(property, PROP_RGM_DBG_BUFSIZE);
	if (api.scxcfg_getproperty_value(&Cfg, NULL,
			property, prop, &error) != FCFG_OK) {
		return (RGMX_DBG_BUFSIZE);
	}

	if (sscanf(prop, "%d", &size) != 1) {
		(void) sc_syslog_msg_initialize(&handle,
			SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			"rgmx_cfg_getbufsize: invalid property value");
		sc_syslog_msg_done(&handle);
		return (RGMX_DBG_BUFSIZE);
	}

	/* Convert size from KB to bytes */
	return (size * 1024);
}

/*
 * Get private hostname for a given nodeid.
 * Don't forget that private hostname must be freed by the caller.
 */
int
rgmx_cfg_getprivatehostname(scxcfg_nodeid_t nodeid, char **privhostnamep)
{
	scxcfg_error error;
	sc_syslog_msg_handle_t handle;
	char property[FCFG_MAX_LEN];
	char prop[FCFG_MAX_LEN];

	/*
	 * We return the private address of the node on the
	 * Europa private interconnect instead of the private
	 * hostname. This code should be changed to return the
	 * private hostname of the farm node as soon as a name
	 * service backend will be implemented.
	 * Something like nss_farm.so.1
	 */
	*privhostnamep = rgmx_cfg_getaddr(nodeid);
	return (privhostnamep == NULL ? -1 : 0);

	*privhostnamep = (char *)malloc(FCFG_MAX_LEN);
	if (*privhostnamep == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_cfg_getprivatehostname(%d) malloc failed", nodeid);
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	(void) sprintf(property, PROP_NODE_PRIVATE_HOSTNAME, nodeid);
	if (api.scxcfg_getproperty_value(&Cfg, NULL,
		property, prop, &error) != FCFG_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_cfg_getprivatehostname(%d): "
		    "scxcfg_getproperty_value failed");
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	return (0);
}

/*
 * Utility: free the result list of scxcfg_getlistpropert_value.
 */
void
rgmx_cfg_freelist(f_property_t *fp)
{
	if (!fp)
		return;
	(void) api.scxcfg_freelist(fp);
}

#ifndef EUROPA_FARM
/*
 * Get the node category giving either a nodeid or a nodename
 * in argument. Possible values are server or farm.
 */
int
rgmx_cfg_isfarmnode(scxcfg_nodeid_t nodeid, char *nodename)
{
	scxcfg_error error;
	sc_syslog_msg_handle_t handle;
	scxcfg_nodeid_t nid;
	int rc;
	/*
	 * Test according to nodename
	 */
	if (nodeid == 0) {
		rc = api.scxcfg_get_nodeid(&Cfg, nodename, &nid, &error);
		if (rc == FCFG_ENOTFOUND) {
			/*
			 * Not found in farm configuration, it's a server
			 */
			return (0);
		} else if (rc != FCFG_OK) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "rgmx_cfg_isfarmnode: "
			    "scxcfg_get_nodeid(%s) failed",
			    nodename);
			sc_syslog_msg_done(&handle);
			return (0);
		} else {
			/*
			 * scxcfg_get_nodeid has succeded,
			 * it must be a farm node.
			 */
			return (1);
		}
	}
	/*
	 * Test according to nodeid
	 */
	return (api.scxcfg_isfarm_nodeid(nodeid));
}

/*
 * Get nodeid for a given farm nodename.
 */
int
rgmx_cfg_getnodeid(char *nodename, scxcfg_nodeid_t *nodeidp)
{
	scxcfg_error error;
	sc_syslog_msg_handle_t handle;

	/*
	 * A NULL nodename means the caller wants the local
	 * node ID.
	 */
	if (nodename == NULL) {
		if (api.scxcfg_get_local_nodeid(nodeidp, &error) != FCFG_OK) {
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// Need explanation of this message!
			// @user_action
			// Need a user action for this message.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    "rgmx_cfg_getnodeid: "
			    "scxcfg_get_local_nodeid failed");
			sc_syslog_msg_done(&handle);
			return (-1);
		}
		return (0);
	}

	/*
	 * Then, try in Farm configuration.
	 */
	if (api.scxcfg_get_nodeid(&Cfg, nodename, nodeidp,
			&error) != FCFG_OK) {
		ucmm_print("RGM", "rgmx_cfg_getnodeid(%s) "
			"scxcfg_get_nodename failed", nodename);
		return (-1);
	}
	return (0);
}

/*
 * Get nodename for a given farm nodeid.
 * Don't forget that nodename must be freed by the caller.
 */
int
rgmx_cfg_getnodename(scxcfg_nodeid_t nodeid, char **nodenamep)
{
	scxcfg_error error;
	sc_syslog_msg_handle_t handle;

	*nodenamep = (char *)malloc(FCFG_MAX_LEN);
	if (*nodenamep == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_cfg_getnodename(%d) malloc failed", nodeid);
		sc_syslog_msg_done(&handle);
		return (-1);
	}
	if (api.scxcfg_get_nodename(&Cfg, nodeid, *nodenamep,
			&error) != FCFG_OK) {
				ucmm_print("RGM", "rgmx_cfg_getnodename(%d) "
					"scxcfg_get_nodename failed", nodeid);

		free(*nodenamep);
		*nodenamep = NULL;
		return (-1);
	}

	return (0);

}

/*
 * Get the Server nodes list.
 */
int
rgmx_cfg_getservernodes(f_property_t **server_node_list, int *num)
{
	scxcfg_error error;
	sc_syslog_msg_handle_t handle;

	if (api.scxcfg_getlistproperty_value(&Cfg, NULL, PROP_SERVER_NODES,
		server_node_list, num, &error) != FCFG_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_cfg_getservernodes: "
		    "scxcfg_getlistproperty_value failed");
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	return (0);
}

/*
 * Get the Farm nodes list.
 */
int
rgmx_cfg_getfarmnodes(f_property_t **farm_node_list, int *num)
{
	scxcfg_error error;
	sc_syslog_msg_handle_t handle;

	if (api.scxcfg_getlistproperty_value(&Cfg, NULL, PROP_FARM_NODES,
		farm_node_list, num, &error) != FCFG_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_cfg_getfarmnodes: "
		    "scxcfg_getlistproperty_value failed");
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	return (0);
}

#endif // !EUROPA_FARM

#ifdef EUROPA_FARM
/*
 * Get local nodeid and nodename (Farm) from the configuration cache.
 */
int
rgmx_cfg_getnode(scxcfg_nodeid_t *nodeidp,  char **nodename)
{
	scxcfg_error error;
	sc_syslog_msg_handle_t handle;

	if (api.scxcfg_get_local_nodeid(nodeidp, &error) != FCFG_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_cfg_getnode: scxcfg_get_local_nodeid failed");
		sc_syslog_msg_done(&handle);
		return (-1);
	}
	*nodename = (char *)malloc(FCFG_MAX_LEN);
	if (*nodename == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_cfg_getnode() malloc failed");
		sc_syslog_msg_done(&handle);
		return (-1);
	}
	if (api.scxcfg_get_local_nodename(&Cfg, *nodename, &error) != FCFG_OK) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// Need explanation of this message!
		// @user_action
		// Need a user action for this message.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "rgmx_cfg_getnode() scxcfg_get_local_nodename failed");
		sc_syslog_msg_done(&handle);
		return (-1);
	}

	return (0);
}
#endif // EUROPA_FARM
