#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.4	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/librgmx/Makefile.com
#

LIBRARY = librgmx.a
VERS = .1

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

RGMXSERVER_OBJS += rgmx_hook.o
RGMXSERVER_OBJS += rgmx_main.o
RGMXSERVER_OBJS += rgmx_fmm.o
RGMXSERVER_OBJS += rgmx_cnfg_server.o
RGMXSERVER_OBJS += rgmx_comm_farm_client.o rgmx_comm_pres_server.o
RGMXSERVER_OBJS += rgmx_cfg.o rgmx_rpcomm.o
RGMXSERVER_OBJS += rgmx_receptionist.o
RGMXSERVER_OBJS += rgmx_util.o rgmx_cnfg_common_xdr.o
RGMXSERVER_OBJS += rgmx_farm_fencing.o

RGMXSERVER_RPC_OBJS = rgmx_cnfg_xdr.o
RGMXSERVER_RPC_OBJS	+= rgmx_cnfg_svc.o

RGMXSERVER_RPC_OBJS += rgmx_comm_xdr.o
RGMXSERVER_RPC_OBJS	+= rgmx_comm_pres_svc.o

RGMXSERVER_RPC_OBJS += rgmx_comm_farm_clnt.o

OBJECTS += $(RGMXSERVER_OBJS) $(RGMXSERVER_RPC_OBJS)

include $(SRC)/common/cl/Makefile.files

OBJS_DIR = objs

CPPFLAGS += $(CL_CPPFLAGS)

PMAP =
SRCS =		$(OBJECTS:%.o=../common/%.c)

LIBS = $(LIBRARY)

CLEANFILES =

CHECKHDRS = rgmx_hook.h rgmx_proto.h
CHECK_FILES = $(RGMXSERVER_OBJS:%.o=%.c_check) \
	$(CHECKHDRS:%.h=%.h_check)

# definitions for lint
LINTFILES += $(RGMXSERVER_OBJS:%.o=%.ln)
LINTS_DIR = .

CC_PICFLAGS = -KPIC
C_PICFLAGS = -KPIC

MTFLAG = -mt

CPPFLAGS += $(MTFLAG)
CFLAGS += $(C_PICFLAGS)
CFLAGS64 += $(C_PICFLAGS)
CCFLAGS += $(CC_PICFLAGS)
CCFLAGS64 += $(CC_PICFLAGS)
CPPFLAGS += -DPIC -D_TS_ERRNO
CPPFLAGS += -I../common -I$(SRC)/common/cl/interfaces/$(CLASS)
CPPFLAGS += -I$(SRC)/common/cl
CPPFLAGS += -I$(ROOTCLUSTINC)/fmm
CPPFLAGS += -I$(SRC)/lib/librgmserver/common
CPPFLAGS += -I$(SRC)/lib/libfence/common
CPPFLAGS += -I$(SRC)/lib/dlscxcfg/common
CPPFLAGS += -I$(SRC)/lib/dlfmm/common
CPPFLAGS += -I$(SRC)/lib/dlfence/common

CPPFLAGS += -DSolaris

CPPFLAGS += -DSC_SRM

RPCGENFLAGS	= $(RPCGENMT) -C -K -1

RPCFILE2_XDR = $(RPCFILE2:$(SRC)/head/rgm/rpc/%.x=../common/%_xdr.c)
RPCFILE2_SVC = $(RPCFILE2:$(SRC)/head/rgm/rpc/%.x=../common/%_svc.c)
RPCFILE2 = $(SRC)/head/rgm/rpc/rgmx_cnfg.x

RPCFILE3_XDR = $(RPCFILE3:$(SRC)/head/rgm/rpc/%.x=../common/%_xdr.c)
RPCFILE3 = $(SRC)/head/rgm/rpc/rgmx_comm.x

RPCFILE4_CLNT = $(RPCFILE4:$(SRC)/head/rgm/rpc/%.x=../common/%_clnt.c)
RPCFILE4 = $(SRC)/head/rgm/rpc/rgmx_comm_farm.x

RPCFILE5_SVC = $(RPCFILE5:$(SRC)/head/rgm/rpc/%.x=../common/%_svc.c)
RPCFILE5 = $(SRC)/head/rgm/rpc/rgmx_comm_pres.x

CLOBBERFILES += $(RPCFILE_XDR) $(RPCFILE_SVC)
CLOBBERFILES += $(RPCFILE2_XDR) $(RPCFILE2_SVC)
CLOBBERFILES += $(RPCFILE3_XDR)
CLOBBERFILES += $(RPCFILE4_CLNT)
CLOBBERFILES += $(RPCFILE5_SVC)

.KEEP_STATE:

all:  $(LIBS)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

$(RPCFILE2_XDR): $(RPCFILE2)
	@$(RM) -f $@
	$(RPCGEN) $(RPCGENFLAGS) -c -o $@ $(RPCFILE2)

$(RPCFILE2_SVC): $(RPCFILE2)
	@$(RM) -f $@
	$(RPCGEN) $(RPCGENFLAGS) -m -o $@ $(RPCFILE2)

$(RPCFILE3_XDR): $(RPCFILE3)
	@$(RM) -f $@
	$(RPCGEN) $(RPCGENFLAGS) -c -o $@ $(RPCFILE3)

$(RPCFILE4_CLNT): $(RPCFILE4)
	@$(RM) -f $@
	$(RPCGEN) $(RPCGENFLAGS) -l -o $@ $(RPCFILE4)

$(RPCFILE5_SVC): $(RPCFILE5)
	@$(RM) -f $@
	$(RPCGEN) $(RPCGENFLAGS) -m -o $@ $(RPCFILE5)
