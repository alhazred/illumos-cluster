/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslm_sensor.cc	1.9	08/06/23 SMI"


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libintl.h>		// for gettext
#include <assert.h>		// for assert

#include <scslm_sensor.h>
#include <scslmadm_config.h>	// for units l10n
/*
 * Sensor management functions
 */


char *key_type_strings[] = {
	"unknown",
	"name",
	"file",
	"type",
	"name",
	"unit",
	"enabled",
	"motype",
	"divisor"
};

#define	SAFE_FREE(X) { if (X) { free(X); X = NULL; } }

/*
 * ********************************************************************
 * Returns the string corresponding to a field number (int)
 *
 * Params:
 *   o key: the field index
 *
 * Returned Value:
 *   o a string corresponding to the parameter
 *
 * ********************************************************************
 */
const char *
key_type_t2string(key_type_t key)
{
	return (key_type_strings[key]);
}

/*
 * ********************************************************************
 * Build the CCR key corresponding to a field of a sensor
 *
 * Params:
 *   o sensor_id: The id of the sensor
 *   o ki_id: the KI id of the sensor
 *   o type: the type of the field to store in CCR
 *
 * Returned Value:
 *   o the CCR key string.
 * ********************************************************************
 */
const char *
sensor_build_key(int sensor_id, int ki_id, key_type_t type)
{
	char *res = (char *)malloc(256);

	if (res == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		return (NULL);
	}
	if (ki_id == 0) {
		(void) snprintf(res, 256, "sensor.%d.%s",
		    sensor_id, key_type_t2string(type));
	} else {
		(void) snprintf(res, 256, "sensor.%d.KI.%d.%s",
		    sensor_id, ki_id, key_type_t2string(type));
	}

	return (res);
}


/*
 * ********************************************************************
 * Break a CCR key into pieces. This is the opposite work than
 * sensor_build_key
 *
 * Param:
 *   o key: key string to break.
 *   o [OUTPUT] sensor_id: The id of the sensor
 *   o [OUTPUT] ki_id: the KI id of the sensor
 *   o [OUTPUT] type: the type of the field to store in CCR
 *
 * Returned Value:
 *   o status
 * ********************************************************************
 */
scslm_error_t
sensor_parse_key(
    const char *P_key,
    int *P_sensor_id, int *P_ki_id, key_type_t *P_type)
{

	enum {
		PARSE_SENSOR,
		PARSE_SENSOR_ID,
		PARSE_SENSOR_KEY_CODE,
		PARSE_KI_ID,
		PARSE_KI_KEY_CODE,
		PARSE_END
	}  state = PARSE_SENSOR;

	char *L_token;
	char *L_copy = strdup(P_key);

	if (L_copy == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		goto end;
	}

	L_token = strtok(L_copy, ".");
	while (L_token != NULL) {
		switch (state) {
		case PARSE_SENSOR:
			if (strcmp(L_token, "sensor") == 0) {
				state = PARSE_SENSOR_ID;
			} else {
				goto fail;
			}
			break;

		case PARSE_SENSOR_ID:
			*P_sensor_id = atoi(L_token);
			state = PARSE_SENSOR_KEY_CODE;
			break;

		case PARSE_KI_ID:
			*P_ki_id = atoi(L_token);
			state = PARSE_KI_KEY_CODE;
			break;

		case PARSE_SENSOR_KEY_CODE:
			if (strcmp(L_token, "KI") == 0) {
				state = PARSE_KI_ID;
				break;
			} else if (strcmp(L_token, "name") == 0) {
				*P_type = KEY_SENSOR_NAME;
			} else if (strcmp(L_token, "file") == 0) {
				*P_type = KEY_SENSOR_FILE;
			} else if (strcmp(L_token, "type") == 0) {
				*P_type = KEY_SENSOR_TYPE;
			} else {
				goto end;
			}
			state = PARSE_END;
			break;

		case PARSE_KI_KEY_CODE:
			if (strcmp(L_token, "name") == 0) {
				*P_type = KEY_KI_NAME;
			} else if (strcmp(L_token, "unit") == 0) {
				*P_type = KEY_KI_UNIT;
			} else if (strcmp(L_token, "motype") == 0) {
				*P_type = KEY_KI_MOTYPE;
			} else if (strcmp(L_token, "divisor") == 0) {
				*P_type = KEY_KI_DIVISOR;
			} else if (strcmp(L_token, "enabled") == 0) {
				*P_type = KEY_KI_ENABLED;
			} else {
				goto fail;
			}
			state = PARSE_END;
			break;

		case PARSE_END:
			/*
			 * if there is still toke to parse whereas we
			 * are in this state, it means the string is
			 * erroneous
			 */
		default:
			goto fail;

		}
		L_token = strtok(NULL, ".");
	}

	end:
	free(L_copy);
	return (scslm_errno);
	fail:
	free(L_copy);
	return scslm_seterror(SCSLM_EPROP,
	    gettext("Invalid CCR entry: \"%s\".\n"),
	    P_key);

}

/*
 * ********************************************************************
 * find a sensor in the given list. The sensor is selected on the id
 *
 * Params:
 *   o list: list of sensors to search in
 *   o sensor_id: id looked for
 *
 * Returned Value:
 *   o the found sensor
 *   o NULL otherwise
 * ********************************************************************
 */
sensor_t *
get_sensor(sensor_t **list, int sensor_id)
{
	sensor_t *ptr = *list;

	/*
	 * search sensor id. if it is present return the sensor
	 */
	while (ptr != NULL) {
		if (ptr->id != sensor_id) {
			ptr = ptr->next;
		} else {
			return (ptr);
		}
	}

	/*
	 * if the sensor is not found, allocate a new one
	 */
	ptr = (sensor_t*) calloc(1, sizeof (sensor_t));
	if (ptr == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		return (NULL);
	}

	/* head insertion */
	ptr->next = *list;
	*list = ptr;
	ptr->id = sensor_id;

	return (ptr);
}

/*
 * ********************************************************************
 * find a ki in the given list. The ki is selected on the id. If the
 * ki is not found, it is created and inserted
 *
 * Params:
 *   o list: list of ki to search in
 *   o sensor_id: id looked for
 *
 * Returned Value:
 *   o the found/created ki
 *   o NULL is case of error (The error is set)
 * ********************************************************************
 */
ki_t *
get_ki(ki_t **ki, int ki_id)
{
	ki_t *ptr = *ki;

	/* search for the ki in list */
	while (ptr != NULL) {
		if (ptr->id != ki_id) {
			ptr = ptr->next;
		} else {
			return (ptr);
		}
	}

	/* if not found, create a nmew one */
	ptr = (ki_t*) calloc(1, sizeof (ki_t));
	if (ptr == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		return (NULL);
	}

	ptr->next = *ki;
	*ki = ptr;
	ptr->id = ki_id;

	return (ptr);

}

/*
 * ********************************************************************
 * Set the property of a sensor
 *
 * Params:
 *   o sensor: the sensor to modify
 *   o type: the field to change
 *   o data: the new value of the field
 *
 * Returned Value:
 *   o status
 *
 * ********************************************************************
 */
scslm_error_t
set_sensor_property(sensor_t *sensor, key_type_t type, const char *data)
{
	char **L_ptr;

	assert(sensor != NULL);

	switch (type) {
	case KEY_SENSOR_NAME:
		L_ptr = &(sensor->name);
		break;
	case KEY_SENSOR_FILE:
		L_ptr = &(sensor->file);
		break;
	case KEY_SENSOR_TYPE:
		L_ptr = &(sensor->type);
		break;
	default:
		return (SCSLM_EINTERNAL);
	}

	*L_ptr = strdup(data);

	if (*L_ptr ==  NULL) {
		return (scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n")));
	}
	return (SCSLM_NOERR);
}

/*
 * ********************************************************************
 * Set the property of a ki
 *
 * Params:
 *   o sensor: the ki to modify
 *   o type: the field to change
 *   o data: the new value of the field
 *
 * Returned Value:
 *   o status
 *
 * ********************************************************************
 */
scslm_error_t
set_ki_property(ki_t *ki, const key_type_t type, const char *data)
{
	char **L_ptr;
	switch (type) {
	case KEY_KI_NAME:
		L_ptr = &(ki->name);
		break;
	case KEY_KI_UNIT:
		L_ptr = &(ki->unit);
		break;
	case KEY_KI_ENABLED:
		L_ptr = &(ki->enabled);
		break;
	case KEY_KI_MOTYPE:
		L_ptr = &(ki->motype);
		break;
	case KEY_KI_DIVISOR:
		L_ptr = &(ki->divisor);
		break;
	default:
		return (SCSLM_EPROP);
	}

	if (*L_ptr) {
		free(*L_ptr);
	}
	*L_ptr = strdup(data);

	if (*L_ptr == NULL) {
		return (scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n")));
	}

	return (SCSLM_NOERR);
}

/*
 * ********************************************************************
 * Scan a sensor_list to find a ki with the correct name and type
 * if found, returns it or NULL otherwise
 *
 * Params:
 *   o P_sensor_list: The list to scan
 *   o P_name: the kiname to find
 *   o P_Type: the type of the ki looked for
 *
 * Returned Value:
 *   o the found ki
 *   o NULL otherwise
 *
 * ********************************************************************
 */
ki_t *
fetch_ki_from_sensor_list(
    const sensor_t *P_sensor_list,
    const char *P_name,
    const char *P_type)
{
	ki_t *ki_ptr;

	while (P_sensor_list != NULL) {
		ki_ptr = P_sensor_list->ki_list;
		while (ki_ptr != NULL) {
			if (strcmp(ki_ptr->name, P_name) == 0 &&
			    strcmp(ki_ptr->motype, P_type) == 0)
				return (ki_ptr);
			ki_ptr = ki_ptr->next;
		}
		P_sensor_list = P_sensor_list->next;
	}
	return (NULL);
}

/*
 * ********************************************************************
 * Scan a sensor_list to find a ki with the correct name and type
 * if found, returns it or NULL otherwise
 *
 * Params:
 *   o P_sensor_list: The list to scan
 *   o P_name: the kiname to find
 *   o P_Type: the type of the ki looked for
 *
 * Returned Value:
 *   o the found ki
 *   o NULL otherwise
 *
 * ********************************************************************
 */
scslm_error_t
l10n_sensor_list(
    const sensor_t *P_sensor_list)
{
	ki_t *ki_ptr;
	char *L_new_unit = NULL;

	while (P_sensor_list != NULL) {
		ki_ptr = P_sensor_list->ki_list;
		while (ki_ptr != NULL) {
			if (strcmp(ki_ptr->unit,
				SCSLM_UNIT_KBYTES_PER_SEC) == 0)
				L_new_unit = strdup(
				    gettext(SCSLM_UNIT_KBYTES_PER_SEC));
			else if (strcmp(ki_ptr->unit,
				SCSLM_UNIT_MBITS_PER_SEC) == 0)
				L_new_unit = strdup(
				    gettext(SCSLM_UNIT_MBITS_PER_SEC));
			else if (strcmp(ki_ptr->unit,
				SCSLM_UNIT_WRITES_PER_SEC) == 0)
				L_new_unit = strdup(
				    gettext(SCSLM_UNIT_WRITES_PER_SEC));
			else if (strcmp(ki_ptr->unit,
				SCSLM_UNIT_READS_PER_SEC) == 0)
				L_new_unit = strdup(
				    gettext(SCSLM_UNIT_READS_PER_SEC));
			else if (strcmp(ki_ptr->unit,
				SCSLM_UNIT_BLOCKS_USED) == 0)
				L_new_unit = strdup(
				    gettext(SCSLM_UNIT_BLOCKS_USED));
			else if (strcmp(ki_ptr->unit,
				SCSLM_UNIT_INODES_USED) == 0)
				L_new_unit = strdup(
				    gettext(SCSLM_UNIT_INODES_USED));
			else if (strcmp(ki_ptr->unit,
				SCSLM_UNIT_PACKETS_PER_SEC) == 0)
				L_new_unit = strdup(
				    gettext(SCSLM_UNIT_PACKETS_PER_SEC));
			else if (strcmp(ki_ptr->unit,
				SCSLM_UNIT_MBYTES) == 0)
				L_new_unit = strdup(
				    gettext(SCSLM_UNIT_MBYTES));
			else if (strcmp(ki_ptr->unit,
				SCSLM_UNIT_CPUS) == 0)
				L_new_unit = strdup(
				    gettext(SCSLM_UNIT_CPUS));
			else if ((ki_ptr->unit)[0] == '\0')
				L_new_unit = strdup("");
			else {
				assert(L_new_unit != NULL);
				L_new_unit = strdup(ki_ptr->unit);
			}

			if (L_new_unit == NULL) {
				(void) scslm_seterror(SCSLM_ENOMEM,
				    gettext("Unable to allocate memory.\n"));
				goto end;
			}
			free(ki_ptr->unit);
			ki_ptr->unit = L_new_unit;

			ki_ptr = ki_ptr->next;
		}
		P_sensor_list = P_sensor_list->next;
	}
end:
	return (scslm_errno);
}


/*
 * ********************************************************************
 * get a string read from CCR and apply it to the list (extra_param)
 * This function is a call-back executed for each line in the CCR
 *
 * Params:
 *    o CCR_Key: the line read from CCR (key part)
 *    o P_value: the value read from CCR
 *    o extra_param: the list of sensors that receives the modification
 *
 * Returned Value:
 *    o The status
 *
 * ********************************************************************
 */
scslm_error_t
store_in_sensor_list(const char *CCR_key, char *P_value, void *extra_param)
{

	sensor_t **list = (sensor_t**)extra_param;

	int sensor_id = 0;
	int ki_id = 0;
	key_type_t type = KEY_UNKNOWN;
	sensor_t *sensor = NULL;

	if (sensor_parse_key(CCR_key, &sensor_id, &ki_id, &type)
	    != SCSLM_NOERR)
		return (scslm_errno);

	sensor = get_sensor(list, sensor_id);
	if (sensor == NULL) {
		return (scslm_errno);
	}

	if (ki_id != 0) {
		ki_t *ki = get_ki(&(sensor->ki_list), ki_id);
		if (ki == NULL) {
			return (scslm_errno);
		}
		if (set_ki_property(ki, type, P_value) != SCSLM_NOERR) {
			return (scslm_errno);
		}
	} else {
		if (set_sensor_property(sensor, type, P_value) != SCSLM_NOERR)
			return (scslm_errno);
	}

	return (SCSLM_NOERR);
}

/*
 * ********************************************************************
 * Returns the number of KIs managed by a sensor list
 *
 * Params:
 *   o sensor_list: the list of sensors scanned
 *
 * Returned Value:
 *   o the count.
 * ********************************************************************
 */
unsigned int
get_nb_ki_in_sensor_list(const sensor_t *sensor_list)
{
	const sensor_t *ptr = sensor_list;
	ki_t *ki_ptr;
	unsigned int count = 0;

	while (ptr != NULL) {
		ki_ptr = ptr->ki_list;
		while (ki_ptr != NULL) {
			count++;
			ki_ptr = ki_ptr->next;
		}
		ptr = ptr->next;
	}

	return (count);
}

/*
 * ********************************************************************
 * Free a sensor and returns a pointer to the next one, if any.
 *
 * Params:
 *   o P_sensor_to_free: The sensor to free
 *
 * Returned Value:
 *   o the next sensor in the list (ease the deletion of a list)
 *
 * ********************************************************************
 */
sensor_t *
free_sensor(sensor_t *P_sensor_to_free)
{
	sensor_t *L_result = NULL;
	ki_t *ki_ptr;


	if (P_sensor_to_free == NULL)
		return (NULL);

	L_result = P_sensor_to_free->next;
	ki_ptr = P_sensor_to_free->ki_list;
	while (ki_ptr != NULL) {
		SAFE_FREE(ki_ptr->name);
		SAFE_FREE(ki_ptr->unit);
		SAFE_FREE(ki_ptr->motype);
		SAFE_FREE(ki_ptr->enabled);
		SAFE_FREE(ki_ptr->divisor);

		ki_t *tmp = ki_ptr;
		ki_ptr = ki_ptr->next;
		SAFE_FREE(tmp);
	}

	SAFE_FREE(P_sensor_to_free->name);
	SAFE_FREE(P_sensor_to_free->file);
	SAFE_FREE(P_sensor_to_free->type);
	SAFE_FREE(P_sensor_to_free);
	return (L_result);
}

/*
 * ********************************************************************
 * Free a whole sensor_list
 *
 * Params:
 *   o P_sensor_list: pointer to sensor list to free
 *
 * Returned Value:
 *   N/A
 *
 * ********************************************************************
 */
void
free_sensor_list(sensor_t **P_sensor_list)
{
	sensor_t *ptr = *P_sensor_list;

	while (ptr != NULL)
		ptr = free_sensor(ptr);
	*P_sensor_list = NULL;
}
