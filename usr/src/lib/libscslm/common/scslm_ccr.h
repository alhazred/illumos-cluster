/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCSLM_CCR_H
#define	_SCSLM_CCR_H

#pragma ident	"@(#)scslm_ccr.h	1.7	08/06/23 SMI"

#include <scslm_sensor.h>
#include <scslm_threshold.h>

extern scslm_error_t
scslm_delete_ccr(
    const char *table);

extern scslm_error_t
scslm_read_table_ccr(const char *P_table,
    scslm_error_t (*callback)(const char *, char *, void *),
    const void *P_cookie, int P_keep_alive);


extern scslm_error_t
scslmadm_create_default_ccr(
    const char *table);

extern scslm_error_t
scslmadm_update_table_ccr(
    const char *table,
    const sensor_t *sensor_list);


extern scslm_error_t
scslmthresh_update_table_ccr(
    const char *table,
    const threshold_list_t *P_list,
    int create);

extern void
scslm_abort_update_table_ccr(const char *P_table);

#endif	/* _SCSLM_CCR_H */
