/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslm_threshold.cc	1.8	08/05/20 SMI"

#include <assert.h>
#include <stdlib.h>		// for malloc, atoi
#include <stdio.h>		// for snprintf
#include <string.h>		// for strdup, memset
#include <libintl.h>		// for gettext

#include <scslm_threshold.h>
#include <scslm_cli.h>		// for STR_MONAME, ...


const char *ccr_keys[THRESH_NB_FIELDS] =
{
	STR_MONAME,
	STR_MOTYPE,
	STR_NODENAME,
	STR_KINAME,
	"rising_warning_valuelimit",
	"rising_warning_resetvalue",
	"rising_fatal_valuelimit",
	"rising_fatal_resetvalue",
	"falling_warning_valuelimit",
	"falling_warning_resetvalue",
	"falling_fatal_valuelimit",
	"falling_fatal_resetvalue",
};

#define	FREE(c) { assert(c != NULL); free(c); c = NULL; }


/* some string for error messages */
#define	STR_RF	"\"" STR_RISING  "-" STR_FATAL   "\""
#define	STR_RW	"\"" STR_RISING  "-" STR_WARNING "\""
#define	STR_FW	"\"" STR_FALLING "-" STR_WARNING "\""
#define	STR_FF	"\"" STR_FALLING "-" STR_FATAL   "\""



static char STR_NULL[] = "<NULL>";

static const char *safe(const char *P_str)
{
	return ((P_str == NULL)?STR_NULL:P_str);
}


/*
 * **********************************************************************
 * return -1 if string is empty or unset, 0 otherwise
 *
 * **********************************************************************
 */
static int
is_empty(const char *P_str)
{
	if (P_str == NULL)
		return (-1);

	if (*P_str == '\0')
		return (-1);

	return (0);
}

/*
 * **********************************************************************
 * ALlocate a new threshold (return NULL in case of prblem)
 * **********************************************************************
 */
threshold_t *
threshold_allocate()
{
	threshold_t *L_new;

	L_new = (threshold_t *)malloc(sizeof (threshold_t));
	if (L_new == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		goto end;
	}

	(void) memset(L_new, 0, sizeof (threshold_t));
end:
	return (L_new);
}

/*
 * **********************************************************************
 * free a threshold correctly
 * **********************************************************************
 */
void
threshold_free(threshold_t **P_thresh)
{
	assert(P_thresh != NULL);
	assert(*P_thresh != NULL);

	for (int ii = 0; ii < THRESH_NB_FIELDS; ii++)
		if ((**P_thresh)[ii] != NULL)
			free ((**P_thresh)[ii]);

	*P_thresh = NULL;
}


/*
 * **********************************************************************
 * set the value of a field of a threshold
 * **********************************************************************
 */
scslm_error_t
threshold_set_field(
    threshold_t *P_thresh,
    unsigned int P_field,
    const char *P_value)
{
	assert(P_thresh != NULL);
	assert(P_field < THRESH_NB_FIELDS);

	if ((P_value == NULL) ||
	    (strcmp(P_value, STR_NULL) == 0)) {
		free((*P_thresh)[P_field]);
		(*P_thresh)[P_field] = NULL;
	} else {
		/* first free zone if necessary */
		if ((*P_thresh)[P_field] != NULL)
			free((*P_thresh)[P_field]);

		/* then copy string */
		(*P_thresh)[P_field] = strdup(P_value);

		if ((*P_thresh)[P_field] == NULL) {
			return (scslm_seterror(SCSLM_ENOMEM,
				gettext("Unable to allocate memory.\n")));
		}
	}
	return (SCSLM_OK);
}


/*
 * **********************************************************************
 * get the value of a field of a threshold
 * **********************************************************************
 */
const char *
threshold_get_field(
    const threshold_t *P_thresh,
    unsigned int P_field)
{
	assert(P_thresh != NULL);
	assert(P_field < THRESH_NB_FIELDS);

	return (safe((*P_thresh)[P_field]));
}


/*
 * *********************************************************************
 * Construct a key for CCR. The key is valid for scslmthresh and has the
 * following shape: <STR_THRESHOLD>.<id>.<string fro field>
 *
 * Param:
 *   o P_id: id f the current group of entries
 *   o P_field: index of the field to be stored
 *
 * Result:
 *   o an allocated buffer containing the key
 *
 * **********************************************************************
 */
char *
threshold_build_ccr_key(unsigned int P_id, unsigned int P_field)
{
	char *result;

	assert(P_field < THRESH_NB_FIELDS);

	result = (char *)malloc(255 * sizeof (char));
	if (result != NULL)
		(void) snprintf(result, 255, "%s.%d.%s",
		    STR_THRESHOLD,
		    P_id,
		    ccr_keys[P_field]); //lint !e661
	return (result);
}


/*
 * **********************************************************************
 * parse one key read from CCR and fill the id of the read threshold and
 * the id of the field.
 *
 * returned value indicates the occurence of error
 * **********************************************************************
 */
scslm_error_t
threshold_parse_ccr_key(
    const char *P_line,
    unsigned int *P_id,
    unsigned int *P_field)
{
	char *L_str_to_parse;
	scslm_error_t err;
	char *L_tok;
	unsigned int   ii;

	assert(P_line != NULL);
	assert(P_id != NULL);
	assert(P_field != NULL);

	L_str_to_parse = strdup(P_line);
	if (L_str_to_parse == NULL) {
		err = scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		goto end;
	}

	if ((L_tok = strtok(L_str_to_parse, ".")) == NULL) {
		err = scslm_seterror(SCSLM_EPROP,
		    gettext("Incorrect CCR entry: \"%s\".\n"),
		    P_line);
		goto end;
	}
	// we must have the prefix....check it
	assert(strcmp(STR_THRESHOLD, L_tok) == 0);

	if ((L_tok = strtok(NULL, ".")) == NULL) {
		err = scslm_seterror(SCSLM_EPROP,
		    gettext("Incorrect CCR entry: \"%s\".\n"),
		    P_line);
		goto end;
	}


	// This is the id
	*P_id = (unsigned int) atoi(L_tok);

	if ((L_tok = strtok(NULL, ".")) == NULL) {
		err = scslm_seterror(SCSLM_EPROP,
		    gettext("Incorrect CCR entry: \"%s\".\n"),
		    P_line);
		goto end;
	}


	*P_field = VOID_FIELD_ID;

	for (ii = 0; ii < THRESH_NB_FIELDS; ii++) {
		if (strcmp(L_tok, ccr_keys[ii]) == 0) {
			*P_field = ii;
			err = SCSLM_NOERR;
			break;
		}
	}

	if (*P_field == VOID_FIELD_ID)	{
		err = scslm_seterror(SCSLM_EPROP,
		    gettext("Incorrect CCR entry: \"%s\".\n"),
		    P_line);
		goto end;
	}

	err = SCSLM_NOERR;
end:
	/* Can't use FREE here, as string can really be NULL */
	if (L_str_to_parse != NULL)
		free(L_str_to_parse);

	return (err);
}



/*
 * **********************************************************************
 * return -1 of the threshold is enabled, 0 otherwise
 *
 * **********************************************************************
 */
int
threshold_is_enabled(const threshold_t *P_thresh)
{
	if (!is_empty((*P_thresh)[THRESH_RISING_WARNING_VALUE]) ||
	    !is_empty((*P_thresh)[THRESH_RISING_FATAL_VALUE]) ||
	    !is_empty((*P_thresh)[THRESH_FALLING_WARNING_VALUE]) ||
	    !is_empty((*P_thresh)[THRESH_FALLING_FATAL_VALUE]))
		return (-1);
	else
		return (0);
}

/*
 * **********************************************************************
 * disable a threshold. After having called this function
 * threshold_is_enabled returns FALSE
 *
 * **********************************************************************
 */
scslm_error_t
threshold_disable(threshold_t *P_thresh)
{
	scslm_error_t err;

	err = threshold_set_field(P_thresh,
	    THRESH_RISING_WARNING_VALUE, NULL);

	if (err != SCSLM_OK)
		return (err);

	err = threshold_set_field(P_thresh,
	    THRESH_RISING_FATAL_VALUE, NULL);

	if (err != SCSLM_OK)
		return (err);

	err = threshold_set_field(P_thresh,
	    THRESH_FALLING_WARNING_VALUE, NULL);

	if (err != SCSLM_OK)
		return (err);

	err = threshold_set_field(P_thresh,
	    THRESH_FALLING_FATAL_VALUE, NULL);

	if (err != SCSLM_OK)
		return (err);

	err = threshold_set_field(P_thresh,
	    THRESH_RISING_WARNING_REARM, NULL);

	if (err != SCSLM_OK)
		return (err);

	err = threshold_set_field(P_thresh,
	    THRESH_RISING_FATAL_REARM, NULL);

	if (err != SCSLM_OK)
		return (err);

	err = threshold_set_field(P_thresh,
	    THRESH_FALLING_WARNING_REARM, NULL);

	if (err != SCSLM_OK)
		return (err);

	err = threshold_set_field(P_thresh,
	    THRESH_FALLING_FATAL_REARM, NULL);

	if (err != SCSLM_OK)
		return (err);

	return (SCSLM_OK);
}

/*
 * **********************************************************************
 * fill one value with the content of the string.
 *
 * the string must contain a double >= 0
 *
 * Param:
 *   o P_strToRead: String to read
 *   o P_doubleToWrite: pointer to a double where the parsed value is stored
 *                      if the string is empty, -1 is stored
 * Result:
 *   o SCSLM_NOERR: content is valid
 *   o SCSLM_EINVAL: value is incorrect
 *
 * **********************************************************************
 */
static scslm_error_t
parse_one_float(const char *P_strToRead, double *P_doubleToWrite)
{
	char *L_strEndOfDouble;

	if (is_empty(P_strToRead)) {
		*P_doubleToWrite = -1;
		return (SCSLM_NOERR);
	}

	*P_doubleToWrite = strtod(P_strToRead, &L_strEndOfDouble);

	if (*L_strEndOfDouble != '\0')
		return (scslm_seterror(SCSLM_EINVAL,
			gettext("Invalid value \"%s\" - not a number.\n"),
			P_strToRead));

	if (*P_doubleToWrite < 0)
		return (scslm_seterror(SCSLM_EINVAL,
			gettext(
			    "Invalid value \"%s\" - not a positive number.\n"),
			P_strToRead));

	return (SCSLM_NOERR);
}

/*
 * **********************************************************************
 * Check if the values of a threshold are correct
 *
 * Param:
 *   o P_thresh: threshold to be tested
 *   o P_buf: allocated zone that will receive the error msg (if any)
 *
 * Result:
 *   o occurence of error
 *
 * **********************************************************************
 */
scslm_error_t
threshold_is_ok(const threshold_t *P_thresh)
{
	/*
	 * These two arrays are holding the different severities, the
	 * first index is holding the highest defined, the last the
	 * lowest. The order is fatal/rising first and fatal/falling
	 * last.
	 */
	enum {
		RF = 0 /* rising fatal */,
		RW = 1 /* rising warning */,
		FW = 2 /* falling Warning */,
		FF = 3 /* falling fatal */
	};

	//lint -e786 String concatenation within initializer
	const char *L_severity_string[4] = {STR_RF, STR_RW, STR_FW, STR_FF};
	//lint +e776

	/*
	 * the following array is used to simplify comparison between
	 * rising and falling. this is a factor parameter
	 */
	int    L_direction[4] = { 1, 1, -1, -1};

	// Array of threshold values
	double L_value[4];
	// Array of rearm values
	double L_rearm[4];

	int    ii, jj;

	/* first check that all value and rearm are floats */
	if (parse_one_float((*P_thresh)[THRESH_RISING_FATAL_VALUE],
		&(L_value[RF])) != SCSLM_NOERR)
		goto end;
	if (parse_one_float((*P_thresh)[THRESH_RISING_WARNING_VALUE],
		&(L_value[RW])) != SCSLM_NOERR)
		goto end;
	if (parse_one_float((*P_thresh)[THRESH_FALLING_WARNING_VALUE],
		&(L_value[FW])) != SCSLM_NOERR)
		goto end;
	if (parse_one_float((*P_thresh)[THRESH_FALLING_FATAL_VALUE],
		&(L_value[FF])) != SCSLM_NOERR)
		goto end;

	if (parse_one_float((*P_thresh)[THRESH_RISING_FATAL_REARM],
		&(L_rearm[RF])) != SCSLM_NOERR)
		goto end;
	if (parse_one_float((*P_thresh)[THRESH_RISING_WARNING_REARM],
		&(L_rearm[RW])) != SCSLM_NOERR)
		goto end;
	if (parse_one_float((*P_thresh)[THRESH_FALLING_WARNING_REARM],
		&(L_rearm[FW])) != SCSLM_NOERR)
		goto end;
	if (parse_one_float((*P_thresh)[THRESH_FALLING_FATAL_REARM],
		&(L_rearm[FF])) != SCSLM_NOERR)
		goto end;

	for (ii = RF; ii <= FF; ii++) {
		if (L_rearm[ii] < 0)
			continue;

		/* check that every rearm has a value */
		if (L_value[ii] < 0) {
			(void) scslm_seterror(SCSLM_EPROP,
			    gettext("No \"%s\" are "
				"configured for %s.\n"),
			    STR_VALUELIMIT,
			    L_severity_string[ii]);
			goto end;
		}



		/* check that every rearm is in the right order */
		if (L_rearm[ii] * L_direction[ii]
		    > L_value[ii] * L_direction[ii]) {
			(void) scslm_seterror(SCSLM_EPROP,
			    gettext("Invalid order between \"%s\" "
				"and \"%s\" for %s.\n"),
			    STR_RESETVALUE,
			    STR_VALUELIMIT,
			    L_severity_string[ii]);
			goto end;
		}

		/*
		 * check that the rearm is below the value
		 * of higher severities
		 */
		for (jj = RF; jj < ii; jj++) {
			if (L_value[jj] < 0)
				continue;

			if (L_rearm[ii] <= L_value[jj])
				continue;

			(void) scslm_seterror(SCSLM_EPROP,
			    gettext("Invalid order between "
				"\"%s\" of %s and "
				"\"%s\" of %s.\n"),
			    STR_RESETVALUE, L_severity_string[ii],
			    STR_VALUELIMIT, L_severity_string[jj]);
			goto end;
		}

		/*
		 * check that the rearm is above the value
		 * of lower severities
		 */
		for (jj = ii + 1; jj <= FF; jj++) {
			if (L_value[jj] < 0)
				continue;

			if (L_rearm[ii] >= L_value[jj])
				continue;

			(void) scslm_seterror(SCSLM_EPROP,
			    gettext("Invalid order between "
				"\"%s\" of %s and "
				"\"%s\" of %s.\n"),
			    STR_RESETVALUE, L_severity_string[ii],
			    STR_VALUELIMIT, L_severity_string[jj]);
			goto end;
		}
	}

	/*
	 * check that the values of all the severitites are ordered
	 * The method is: comparing all values with the ones below
	 */
	for (ii = RF; ii <= FF; ii++) {
		if (L_value[ii] < 0)
			continue;
		for (jj = ii + 1; jj <= FF; jj++) {
			if (L_value[jj] < 0)
				continue;

			if (L_value[ii] >= L_value[jj])
				continue;

			(void) scslm_seterror(SCSLM_EPROP,
			    gettext("Invalid order between "
				"\"%s\" of %s and "
				"\"%s\" of %s.\n"),
			    STR_RESETVALUE, L_severity_string[ii],
			    STR_VALUELIMIT, L_severity_string[jj]);
			goto end;
		}
	}
end:
	return (scslm_errno);
}


/*
 * **********************************************************************
 * Creation  of a full list (the list does not contain an threshold)
 * **********************************************************************
 */
threshold_list_t *
threshold_list_allocate()
{
	threshold_list_t *L_new;

	L_new = (threshold_list_t *)malloc(sizeof (threshold_list_t));
	if (L_new == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		return (NULL);
	}
	L_new->threshold = NULL;
	L_new->id_in_ccr = INVALID_THRESH_ID;
	L_new->next = NULL;
	L_new->is_head = 1;

	return (L_new);
}


/*
 * **********************************************************************
 *  destruction of a full list
 * **********************************************************************
 */
scslm_error_t
threshold_list_free(threshold_list_t **P_list)
{
	threshold_list_t *L_ptr;
	threshold_list_t *L_prev = NULL;

	assert(P_list != NULL);

	if (*P_list != NULL)
		assert((*P_list)->is_head == 1);

	L_ptr = *P_list;
	while (L_ptr) {
		if (L_ptr->threshold != NULL)
			threshold_free(&(L_ptr->threshold));
		L_ptr->id_in_ccr = INVALID_THRESH_ID;
		L_prev = L_ptr;
		L_ptr = L_ptr->next;
		FREE(L_prev);
	}
	return (SCSLM_NOERR);
}


/*
 * **********************************************************************
 * return a threshold having the id in the list
 * **********************************************************************
 */
threshold_t *
threshold_list_extract(
    const threshold_list_t *P_list,
    unsigned int P_id)
{
	if (P_list != NULL)
		assert(P_list->is_head == 1);

	while (P_list != NULL) {
		if (P_list->id_in_ccr == P_id)
			return (P_list->threshold);
		P_list = P_list->next;
	}
	return (NULL);
}


/*
 * **********************************************************************
 * insert s threshold in the list with a given id.
 * Check the id is not already used
 * **********************************************************************
 */
scslm_error_t
threshold_list_insert(
    threshold_list_t **P_list,
    unsigned int P_id_in_ccr,
    threshold_t *P_thresh)
{
	threshold_list_t *L_new_elem;
	assert(P_list != NULL);


	if (*P_list != NULL) {
		assert((*P_list)->is_head == 1);
		/* check if the threshold is not already in */
		assert(threshold_list_extract(*P_list, P_id_in_ccr) == NULL);

		(*P_list)->is_head = 0;
	}

	L_new_elem = threshold_list_allocate();
	if (L_new_elem == NULL)
		return (scslm_seterror(SCSLM_ENOMEM,
			gettext("Unable to allocate memory.\n")));

	L_new_elem->next = (*P_list);
	L_new_elem->threshold = P_thresh;
	L_new_elem->id_in_ccr = P_id_in_ccr;
	(*P_list) = L_new_elem;
	return (SCSLM_OK);
}

/*
 * **********************************************************************
 * get an id that is not contained in the list
 * **********************************************************************
 */
unsigned int
threshold_list_get_new_id(const threshold_list_t *P_list)
{
	const threshold_list_t *L_ptr = P_list;
	unsigned int L_max = 0;

	if (P_list == NULL)
		return (0);

	assert(P_list->is_head == 1);

	while (L_ptr) {
		if (L_ptr->id_in_ccr > L_max)
			L_max = L_ptr->id_in_ccr;
		L_ptr = L_ptr->next;
	}
	return (L_max + 1);
}

/*
 * **********************************************************************
 * specify if a threshold is matching a pattern
 *
 * Param:
 *   o P_thresh: threshold to be tested
 *   o P_pattern: patern to apply
 *
 * Result:
 *   o 0 (zero) if threshold is *not* matching the pattern
 *   o -1 if it is.
 *
 * **********************************************************************
 */
int
threshold_is_matching(const threshold_t *P_thresh, const threshold_t *P_pattern)
{
	if (!is_empty((*P_pattern)[THRESH_MONAME]) &&
	    (strcmp((*P_thresh)[THRESH_MONAME],
		(*P_pattern)[THRESH_MONAME]) != 0))
		return (0);

	if (!is_empty((*P_pattern)[THRESH_MOTYPE]) &&
	    (strcmp((*P_thresh)[THRESH_MOTYPE],
		(*P_pattern)[THRESH_MOTYPE]) != 0))
		return (0);


	if (!is_empty((*P_pattern)[THRESH_KINAME]) &&
	    (strcmp((*P_thresh)[THRESH_KINAME],
		(*P_pattern)[THRESH_KINAME]) != 0))
		return (0);

	/*
	 * After having tested the parameter, we test the scope.
	 * This test is specific, if filter or found threshold has
	 * a cluster-wiode scope, the two thresholds match.
	 * and if both are set and equals, thresholds match also
	 */
	if ((!is_empty((*P_thresh)[THRESH_NODENAME])) &&
	    (!is_empty((*P_pattern)[THRESH_NODENAME])) &&
	    (strcmp((*P_thresh)[THRESH_NODENAME],
		(*P_pattern)[THRESH_NODENAME]) != 0))
			return (0);

	return (-1);
}

/*
 * **********************************************************************
 * given a list and a threshold. returns the id of a threshold
 * matching the given
 * one.
 *
 * Param:
 *   o P_list: the list of threshold to scan
 *   o P_thresh: matching criteria
 *
 * Result:
 *   o Id of the first threshold (should be unique) matching the
 *     criteria
 *   o INVALID_THRESH_ID if none are found
 *
 * **********************************************************************
 */
unsigned int
threshold_list_get_matching_id(
    const threshold_list_t *P_list,
    const threshold_t *P_thresh)
{
	const threshold_list_t *L_ptr = P_list;
	const threshold_t *L_thresh;
	unsigned int found_id = INVALID_THRESH_ID;

	if (P_list == NULL)
		goto end;

	assert(P_thresh != NULL);
	assert(P_list != NULL);
	assert(P_list->is_head == 1);

	for (L_ptr = P_list; L_ptr; L_ptr = L_ptr->next) {
		L_thresh =  L_ptr->threshold;

		if (threshold_is_matching(L_thresh, P_thresh)) {
			found_id = L_ptr->id_in_ccr;
			break;
		}
	}
end:
	return (found_id);
}

/*
 * **********************************************************************
 * print the thresholds matching a criterion (specified by a threshold
 * partially filled)
 *
 * Param:
 *   o P_list: the list of threshold to scan
 *   o P_filter: matching criteria
 *
 * Result:
 *   o NONE
 *
 * **********************************************************************
 */

void
threshold_list_print(
    const threshold_list_t *P_list,
    const threshold_t *P_filter)
{
	const threshold_list_t *L_ptr = P_list;
	const threshold_t *L_thresh;

	if (P_list == NULL)
		goto end;	// only solution ok for check and lint


	assert(P_list->is_head == 1);

	for (L_ptr = P_list; L_ptr; L_ptr = L_ptr->next) {
		L_thresh = L_ptr->threshold;

		if (P_filter != NULL)
			if (!threshold_is_matching(L_thresh, P_filter))
				continue;

		const char *L_mot = make_short_mot((*L_thresh)[THRESH_MOTYPE]);
		const char *L_ki = make_short_ki((*L_thresh)[THRESH_KINAME]);

		(void) printf("----------------------------------------\n");
		(void) fprintf(stdout, "%s : %s\n", STR_MOTYPE,
		    (L_mot != NULL)?L_mot:(*L_thresh)[THRESH_MOTYPE]);
		(void) fprintf(stdout, "%s : %s\n", STR_KINAME,
		    (L_ki != NULL)?L_ki:(*L_thresh)[THRESH_KINAME]);
		(void) fprintf(stdout, "%s : %s\n", STR_MONAME,
		    safe((*L_thresh)[THRESH_MONAME]));
		(void) fprintf(stdout, "%s : %s\n", STR_NODENAME,
		    safe((*L_thresh)[THRESH_NODENAME]));

		if (!is_empty((*L_thresh)[THRESH_RISING_FATAL_VALUE]))
			(void) fprintf(stdout,
			    "%s - %s: %s = %s %s = %s\n",
			    STR_RISING,
			    STR_FATAL,
			    STR_VALUELIMIT,
			    (*L_thresh)[THRESH_RISING_FATAL_VALUE],
			    STR_RESETVALUE,
			    safe((*L_thresh)[THRESH_RISING_FATAL_REARM]));

		if (!is_empty((*L_thresh)[THRESH_RISING_WARNING_VALUE]))
			(void) fprintf(stdout,
			    "%s - %s: %s = %s %s = %s\n",
			    STR_RISING,
			    STR_WARNING,
			    STR_VALUELIMIT,
			    (*L_thresh)[THRESH_RISING_WARNING_VALUE],
			    STR_RESETVALUE,
			    safe((*L_thresh)[THRESH_RISING_WARNING_REARM]));

		if (!is_empty((*L_thresh)[THRESH_FALLING_WARNING_VALUE]))
			(void) fprintf(stdout,
			    "%s - %s: %s = %s %s = %s\n",
			    STR_FALLING,
			    STR_WARNING,
			    STR_VALUELIMIT,
			    (*L_thresh)[THRESH_FALLING_WARNING_VALUE],
			    STR_RESETVALUE,
			    safe((*L_thresh)[THRESH_FALLING_WARNING_REARM]));

		if (!is_empty((*L_thresh)[THRESH_FALLING_FATAL_VALUE]))
			(void) fprintf(stdout,
			    "%s - %s: %s = %s %s = %s\n",
			    STR_FALLING,
			    STR_FATAL,
			    STR_VALUELIMIT,
			    (*L_thresh)[THRESH_FALLING_FATAL_VALUE],
			    STR_RESETVALUE,
			    safe((*L_thresh)[THRESH_FALLING_FATAL_REARM]));

	}
end:
	;
}
