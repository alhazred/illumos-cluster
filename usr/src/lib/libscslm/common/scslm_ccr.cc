/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslm_ccr.cc	1.13	08/06/24 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <libintl.h>		// for gettext

#include <h/ccr.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <orb/object/adapter.h>
#include <orb/infrastructure/orb.h>

#include <scslmadm_config.h>
#include <scslm_ccr.h>
#include <scslm_sensor.h>
#include <scslm_threshold.h>
#include <scslm.h>		// for SCSLM_EXXXXX
#include <sys/sol_version.h>	/* SOL_VERSION */

static ccr::directory_var ccr_dir;

struct {
	char *name;
	ccr::updatable_table_var list;
	int presence;
} opened_tables[] = {
	{SCSLMADM_TABLE, nil, 0}, //lint !e651
	{SCSLMTHRESH_TABLE, nil, 0 },
	{NULL, NULL, 0}
};

#define	INVALID_TABLE_ID ((unsigned int) -1)

/*
 * CR 6223425 makes ki.sunw.opacket.rate not work on Solaris 9.
 */

/*
 * mot.sunw.node and mot.sunw.cluster.resourcegroup are defined in two
 * different places, in this file and in common/scslm_const.h. This must be
 * changed some time. See RFE 6346092.
 */

/*
 * The sensor names are hard-coded in the code of the Ganymede CMASS agent
 * (agent_ganymede). Therefore, do not change the sensor names here without
 * reflecting the changes in the SensorBase class of agent_ganymede.
 */


static char *default_config[][2] = {

	/* Disk Device Sensor */
	{"sensor.1.file",		"libdiskdevsensor.so.1"},
	{"sensor.1.name",		"Disk Device Sensor"},
	{"sensor.1.type",		"lib"},

	{"sensor.1.KI.1.name",		"ki.sunw.rbyte.rate"},
	{"sensor.1.KI.1.unit",		SCSLM_UNIT_KBYTES_PER_SEC},
	{"sensor.1.KI.1.motype",	"mot.sunw.disk"},
	{"sensor.1.KI.1.divisor",	"1"},
	{"sensor.1.KI.1.enabled",	"true"},

	{"sensor.1.KI.2.name",		"ki.sunw.wbyte.rate"},
	{"sensor.1.KI.2.unit",		SCSLM_UNIT_KBYTES_PER_SEC},
	{"sensor.1.KI.2.motype",	"mot.sunw.disk"},
	{"sensor.1.KI.2.divisor",	"1"},
	{"sensor.1.KI.2.enabled",	"true"},

	{"sensor.1.KI.3.name",		"ki.sunw.write.rate"},
	{"sensor.1.KI.3.unit",		SCSLM_UNIT_WRITES_PER_SEC},
	{"sensor.1.KI.3.motype",	"mot.sunw.disk"},
	{"sensor.1.KI.3.divisor",	"1"},
	{"sensor.1.KI.3.enabled",	"true"},

	{"sensor.1.KI.4.name",		"ki.sunw.read.rate"},
	{"sensor.1.KI.4.unit",		SCSLM_UNIT_READS_PER_SEC},
	{"sensor.1.KI.4.motype",	"mot.sunw.disk"},
	{"sensor.1.KI.4.divisor",	"1"},
	{"sensor.1.KI.4.enabled",	"true"},

	/* Memory/CPU process sensor */
	{"sensor.2.file",		"procsensor"},
	{"sensor.2.name",		"Memory/CPU Process Sensor"},
	{"sensor.2.type",		"cmd"},

	{"sensor.2.KI.1.name",		"ki.sunw.mem.used"},
	{"sensor.2.KI.1.unit",		SCSLM_UNIT_MBYTES},
	{"sensor.2.KI.1.motype",	"mot.sunw.cluster.resourcegroup"},
	{"sensor.2.KI.1.divisor",	"1024"}, /* KBytes -> MBytes */
	{"sensor.2.KI.1.enabled",	"true"},

	{"sensor.2.KI.2.name",		"ki.sunw.swap.used"},
	{"sensor.2.KI.2.unit",		SCSLM_UNIT_MBYTES},
	{"sensor.2.KI.2.motype",	"mot.sunw.cluster.resourcegroup"},
	{"sensor.2.KI.2.divisor",	"1024"}, /* KBytes -> MBytes */
	{"sensor.2.KI.2.enabled",	"false"},

	{"sensor.2.KI.3.name",		"ki.sunw.cpu.used"},
	{"sensor.2.KI.3.unit",		SCSLM_UNIT_CPUS},
	{"sensor.2.KI.3.motype",	"mot.sunw.cluster.resourcegroup"},
	{"sensor.2.KI.3.divisor",	"100"},
	{"sensor.2.KI.3.enabled",	"true"},

	/* CPU Device Sensor */
	{"sensor.3.file",		"libcpudevsensor.so.1"},
	{"sensor.3.name",		"CPU Device Sensor"},
	{"sensor.3.type",		"lib"},

	{"sensor.3.KI.1.name",		"ki.sunw.cpu.idle"},
	{"sensor.3.KI.1.unit",		SCSLM_UNIT_CPUS},
	{"sensor.3.KI.1.motype",	"mot.sunw.node"},
	{"sensor.3.KI.1.divisor",	"100"},
	{"sensor.3.KI.1.enabled",	"false"},

	{"sensor.3.KI.2.name",		"ki.sunw.cpu.iowait"},
	{"sensor.3.KI.2.unit",		SCSLM_UNIT_CPUS},
	{"sensor.3.KI.2.motype",	"mot.sunw.node"},
	{"sensor.3.KI.2.divisor",	"100"},
	{"sensor.3.KI.2.enabled",	"false"},

	{"sensor.3.KI.3.name",		"ki.sunw.cpu.used"},
	{"sensor.3.KI.3.unit",		SCSLM_UNIT_CPUS},
	{"sensor.3.KI.3.motype",	"mot.sunw.node"},
	{"sensor.3.KI.3.divisor",	"100"},
	{"sensor.3.KI.3.enabled",	"true"},

	{"sensor.3.KI.4.name",		"ki.sunw.cpu.loadavg.1mn"},
	{"sensor.3.KI.4.unit",		""},
	{"sensor.3.KI.4.motype",	"mot.sunw.node"},
	{"sensor.3.KI.4.divisor",	"1000"},
	{"sensor.3.KI.4.enabled",	"false"},

	{"sensor.3.KI.5.name",		"ki.sunw.cpu.loadavg.5mn"},
	{"sensor.3.KI.5.unit",		""},
	{"sensor.3.KI.5.motype",	"mot.sunw.node"},
	{"sensor.3.KI.5.divisor",	"1000"},
	{"sensor.3.KI.5.enabled",	"true"},

	{"sensor.3.KI.6.name",		"ki.sunw.cpu.loadavg.15mn"},
	{"sensor.3.KI.6.unit",		""},
	{"sensor.3.KI.6.motype",	"mot.sunw.node"},
	{"sensor.3.KI.6.divisor",	"1000"},
	{"sensor.3.KI.6.enabled",	"false"},

	/* Memory Device Sensor */
	{"sensor.4.file",		"libmemdevsensor.so.1"},
	{"sensor.4.name",		"Memory Device Sensor"},
	{"sensor.4.type",		"lib"},

	{"sensor.4.KI.1.name",		"ki.sunw.mem.used"},
	{"sensor.4.KI.1.unit",		SCSLM_UNIT_MBYTES},
	{"sensor.4.KI.1.motype",	"mot.sunw.node"},
	{"sensor.4.KI.1.divisor",	"1024"}, /* KBytes -> MBytes */
	{"sensor.4.KI.1.enabled",	"true"},

	{"sensor.4.KI.2.name",		"ki.sunw.mem.free"},
	{"sensor.4.KI.2.unit",		SCSLM_UNIT_MBYTES},
	{"sensor.4.KI.2.motype",	"mot.sunw.node"},
	{"sensor.4.KI.2.divisor",	"1024"}, /* KBytes -> MBytes */
	{"sensor.4.KI.2.enabled",	"false"},

	{"sensor.4.KI.3.name",		"ki.sunw.swap.used"},
	{"sensor.4.KI.3.unit",		SCSLM_UNIT_MBYTES},
	{"sensor.4.KI.3.motype",	"mot.sunw.node"},
	{"sensor.4.KI.3.divisor",	"1024"}, /* KBytes -> MBytes */
	{"sensor.4.KI.3.enabled",	"true"},

	{"sensor.4.KI.4.name",		"ki.sunw.swap.free"},
	{"sensor.4.KI.4.unit",		SCSLM_UNIT_MBYTES},
	{"sensor.4.KI.4.motype",	"mot.sunw.node"},
	{"sensor.4.KI.4.divisor",	"1024"}, /* KBytes -> MBytes */
	{"sensor.4.KI.4.enabled",	"false"},

	/* Network Device Sensor */
	{"sensor.5.file",		"libnetdevsensor.so.1"},
	{"sensor.5.name",		"Network Device Sensor"},
	{"sensor.5.type",		"lib"},

	{"sensor.5.KI.1.name",		"ki.sunw.wbyte.rate"},
	{"sensor.5.KI.1.unit",		SCSLM_UNIT_MBITS_PER_SEC},
	{"sensor.5.KI.1.motype",	"mot.sunw.netif"},
	{"sensor.5.KI.1.divisor",	"125000"}, /* B/s * 8 / 1e6 = Mb/s */
	{"sensor.5.KI.1.enabled",	"true"},

	{"sensor.5.KI.2.name",		"ki.sunw.rbyte.rate"},
	{"sensor.5.KI.2.unit",		SCSLM_UNIT_MBITS_PER_SEC},
	{"sensor.5.KI.2.motype",	"mot.sunw.netif"},
	{"sensor.5.KI.2.divisor",	"125000"}, /* B/s * 8 / 1e6 = Mb/s */
	{"sensor.5.KI.2.enabled",	"true"},

	{"sensor.5.KI.3.name",		"ki.sunw.ipacket.rate"},
	{"sensor.5.KI.3.unit",		SCSLM_UNIT_PACKETS_PER_SEC},
	{"sensor.5.KI.3.motype",	"mot.sunw.netif"},
	{"sensor.5.KI.3.divisor",	"1"},
	{"sensor.5.KI.3.enabled",	"true"},

	{"sensor.5.KI.4.name",		"ki.sunw.opacket.rate"},
	{"sensor.5.KI.4.unit",		SCSLM_UNIT_PACKETS_PER_SEC},
	{"sensor.5.KI.4.motype",	"mot.sunw.netif"},
	{"sensor.5.KI.4.divisor",	"1"},
	{"sensor.5.KI.4.enabled",	"true"},

#if SOL_VERSION >= __s10
	/* Zone Device Sensor */
	{"sensor.6.file",		"libzonedevsensor.so.1"},
	{"sensor.6.name",		"Zone Device Sensor"},
	{"sensor.6.type",		"lib"},

	{"sensor.6.KI.1.name",		"ki.sunw.cpu.idle"},
	{"sensor.6.KI.1.unit",		SCSLM_UNIT_CPUS},
	{"sensor.6.KI.1.motype",	"mot.sunw.solaris.zone"},
	{"sensor.6.KI.1.divisor",	"100"},
	{"sensor.6.KI.1.enabled",	"true"},
#endif
	{NULL, NULL}
};

/*
 * **********************************************************************
 * get an index for the used table....desing is not nice..
 * this index is used for opened_table_list and presence
 * **********************************************************************
 */
static unsigned int
get_index_by_tablename(const char *P_name)
{
	unsigned int ii = 0;
	while (opened_tables[ii].name != NULL) {
		if (strcmp(opened_tables[ii].name, P_name) == 0)
			return (ii);
		ii++;
	}
	return (INVALID_TABLE_ID);
}


/*
 * **********************************************************************
 * initialize connection to CCR
 * **********************************************************************
 */
static scslm_error_t
scslm_initialize()
{

	static int initialized = 0;

	if (initialized == 0) {
		Environment e;

		if (ORB::initialize() != 0) {
			return (scslm_seterror(SCSLM_EINTERNAL,
				gettext("Failed to initialize ORB.\n")));
		}

		// look up ccr directory in name server
		naming::naming_context_var ctxp = ns::local_nameserver();
		CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
		if (e.exception()) {
			e.clear();
			return (scslm_seterror(SCSLM_EINTERNAL,
				gettext("ccr_directory not found "
				    "in name server.\n")));
		}

		ccr_dir = ccr::directory::_narrow(obj);

		initialized = 1;
	}

	return (SCSLM_NOERR);
}


/*
 * **********************************************************************
 * generic function to modify a ccr table
 * **********************************************************************
 */
static scslm_error_t
scslm_update_table_ccr(
    const char *P_table,
    const ccr::element_seq *P_elems,
    const int P_create) {
	unsigned int L_index;
	Environment e;
	ccr::updatable_table_var tabptr;

	if (P_create == 1) {
		/* create table */
		ccr_dir->create_table(P_table, e);
		if (e.exception()) {
			// ignore error if table has been created concurently
			e.clear();

			Environment e2;
			(void) ccr_dir->lookup(P_table, e2);
			if (e2.exception()) {
				e2.clear();

				return (scslm_seterror(SCSLM_EINTERNAL,
					gettext("error creating table "
					    "\"%s\".\n"),
					P_table));
			}
		}
	}

	L_index = get_index_by_tablename(P_table);
	if (L_index == INVALID_TABLE_ID) {
		(void) scslm_seterror(SCSLM_EINTERNAL,
		    gettext("Unknown table \"%s\".\n"),
		    P_table);
		return (SCSLM_EINTERNAL);
	}

	if (opened_tables[L_index].presence == 1) {
		tabptr = opened_tables[L_index].list;
	} else {
		tabptr = ccr_dir->begin_transaction(P_table, e);
		if (e.exception()) {
			tabptr._ptr = nil;
			if (ccr::table_modified::_exnarrow(e.exception())) {
				e.clear();
				return (scslm_seterror(SCSLM_EBUSY,
					gettext("Table \"%s\" has been updated"
					    " concurently.\n"), P_table));
			}

			e.clear();
			return (scslm_seterror(SCSLM_EINTERNAL,
				gettext("failed to update table \"%s\".\n"),
				P_table));
		}
	}


	tabptr->remove_all_elements(e);
	if (e.exception()) {
		if (ccr::table_modified::_exnarrow(e.exception())) {
			e.clear();
			tabptr->abort_transaction(e);
			e.clear();
			tabptr._ptr = nil;

			return (scslm_seterror(SCSLM_EBUSY,
				gettext("Table \"%s\" has been updated "
				    "concurently.\n"),
				P_table));
		}

		e.clear();
		tabptr->abort_transaction(e);
		e.clear();	// clear any exceptions that occurred
		tabptr._ptr = nil;
		return (scslm_seterror(SCSLM_EINTERNAL,
			gettext("Error deleting the old file entries.\n")));
	}

	tabptr->add_elements(*P_elems, e);
	if (e.exception()) {
		e.clear();
		tabptr->abort_transaction(e);
		if (e.exception()) {
			e.clear();
			tabptr._ptr = nil;
			return (scslm_seterror(SCSLM_EINTERNAL,
				gettext("Unable to abort transaction"
				    " for table \"%s\".\n"),
				P_table));
		}
		tabptr._ptr = nil;
		return (scslm_seterror(SCSLM_EINTERNAL,
			gettext("Unable to write in table \"%s\".\n"),
			P_table));
	}


	tabptr->commit_transaction(e);
	if (e.exception()) {
		e.clear();
		tabptr->abort_transaction(e);
		tabptr._ptr = nil;
		if (e.exception()) {
			e.clear();
			return (scslm_seterror(SCSLM_EINTERNAL,
				gettext("Unable to abort transaction"
				    " for table \"%s\".\n"),
				P_table));
		}
		return (scslm_seterror(SCSLM_EINTERNAL,
			gettext("Unable to commit modification on "
			    "table \"%s\".\n"),
			P_table));
	}

	tabptr._ptr = nil;

	return (SCSLM_NOERR);

}

/*
 * **********************************************************************
 * create a default scslmadm table (i.e. /usr/cluster/cc/scslmki)
 * **********************************************************************
 */
scslm_error_t
scslmadm_create_default_ccr(const char *P_table)
{
	unsigned int L_index = 0;
	Environment e;
	ccr::updatable_table_var tabptr;
	ccr::element_seq *elems = NULL;
	scslm_error_t err;

	if ((err = scslm_initialize()) != SCSLM_NOERR)
		goto end;

	/* first delete table */
	(void) scslm_delete_ccr(P_table);

	/* ignore returned error */
	scslm_clearerror();

	/* create table */
	ccr_dir->create_table(P_table, e);
	if (e.exception()) {
		e.clear();
		err = scslm_seterror(SCSLM_EINTERNAL,
		    gettext("Unable to create table \"%s\".\n"), P_table);
		goto end;
	}

	/* open table */

	tabptr = ccr_dir->begin_transaction(P_table, e);
	if (e.exception()) {
		e.clear();
		err = scslm_seterror(SCSLM_EINTERNAL,
		    gettext("unable to create table \"%s\".\n"), P_table);
		goto end;
	}

	/* write default values */
	while (default_config[L_index][0] != NULL) {
		L_index++;
	}

	elems = new ccr::element_seq(L_index, L_index);
	if (elems == NULL) {
		err = scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		goto end;
	}

	for (unsigned int ii = 0; ii < L_index; ii++) {
		(*elems)[ii].key = strdup(default_config[ii][0]);
		(*elems)[ii].data = strdup(default_config[ii][1]);
	}

	tabptr->add_elements(*elems, e);

	// delete elems;
	if (e.exception()) {
		e.exception()->print_exception("commit_transaction");
		e.clear();
		tabptr->abort_transaction(e);
		if (e.exception()) {
			e.clear();
			err = scslm_seterror(SCSLM_EINTERNAL,
			    gettext("Unable to abort transaction"
				" for table \"%s\".\n"), P_table);
			goto end;
		}
		err = scslm_seterror(SCSLM_EINTERNAL,
		    gettext("Unable to write in table \"%s\".\n"),
		    P_table);
		goto end;
	}


	tabptr->commit_transaction(e);

	if (e.exception()) {
		e.clear();
		tabptr->abort_transaction(e);
		if (e.exception()) {
			e.clear();
			err = scslm_seterror(SCSLM_EINTERNAL,
			    gettext("Unable to abort transaction"
				" for table \"%s\".\n"), P_table);
			goto end;
		}
		err = scslm_seterror(SCSLM_EINTERNAL,
		    gettext("Unable to commit modification on table "
			"\"%s\".\n"),
		    P_table);
		goto end;
	}

	err = SCSLM_NOERR;
end:
	if (elems) {
		delete(elems);
	}
	tabptr._ptr = nil;
	return (err);
}



/*
 * **********************************************************************
 * delete a CCR table - in debug only
 * **********************************************************************
 */
scslm_error_t
scslm_delete_ccr(const char *P_table)
{
	Environment e;
	scslm_error_t err;

	err = scslm_initialize();

	if (err != SCSLM_NOERR)
		goto end;

	ccr_dir->remove_table(P_table, e);
	if (e.exception()) {
		if (ccr::no_such_table::_exnarrow(e.exception()) != NULL) {
			err = scslm_seterror(SCSLM_ENOENT,
		    gettext("Unable to delete table \"%s\""), P_table);
		} else {
			err = scslm_seterror(SCSLM_EACCESS,
			    gettext("Unable to delete table \"%s\""), P_table);
		}
		e.clear();
		goto end;
	}
	err = SCSLM_NOERR;
end:
	return (err);
}



/*
 * **********************************************************************
 * read a table and call a callback for all elem in the table
 * **********************************************************************
 */
scslm_error_t
scslm_read_table_ccr(const char *P_table,
    scslm_error_t (*P_callback)(const char *, char *, void *),
    const void *P_cookie, int P_keep_alive)
{
	Environment e;
	CORBA::Exception *ex;
	ccr::readonly_table_ptr rtabptr;
	scslm_error_t err;

	ccr::element_seq_var L_elems;
	int count = 0;

	if ((err = scslm_initialize()) != SCSLM_NOERR)
		goto end;

	// We open the table in update mode if we want to write into it later
	// It ensure that nobody will update it in our back

	if (P_keep_alive == 1) {
		unsigned int L_index = get_index_by_tablename(P_table);
		if (L_index == INVALID_TABLE_ID) {
			err = scslm_seterror(SCSLM_ENOENT,
			    gettext("Service Level Manageability "
			    "is not configured.\n"));
			goto end;
		}

		opened_tables[L_index].list
		    = ccr_dir->begin_transaction(P_table, e);
		if (e.exception()) {
			e.clear();
			err = scslm_seterror(SCSLM_EINTERNAL,
			    gettext("Impossible to begin transaction "
				"on table \"%s\".\n"),
			    P_table);
			goto end;
		}
		opened_tables[L_index].presence = 1;
	}

	/* open table */
	rtabptr = ccr_dir->lookup(P_table, e);
	if (e.exception()) {
		e.clear();
		err = scslm_seterror(SCSLM_ENOENT,
		    gettext(
		    "Service Level Manageability is not configured.\n"));
		goto end;
	}

	retry:

	count = rtabptr->get_num_elements(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex)) {
			err = scslm_seterror(SCSLM_EACCESS,
			    gettext("Unable to access table \"%s\".\n"),
			    P_table);
			e.clear();
			goto end;
		} else {
			// We have a stale handle to the table - try again.
			e.clear();
			goto retry;

		}
	}

	if (count > 0) {
		rtabptr->atfirst(e);

		if ((ex = e.exception()) == NULL) {
			rtabptr->next_n_elements((uint32_t)count, L_elems, e);
		}

		if ((ex = e.exception()) != NULL) {
			if (ccr::table_modified::_exnarrow(ex)) {
				err = scslm_seterror(SCSLM_EACCESS,
				    gettext("Unable to access table"
					" \"%s\".\n"),
				    P_table);
				e.clear();
				goto end;
			} else {
				// We have a stale handle to the table
				// - try again.
				e.clear();
				goto retry;
			}
		}

		/* Let's call the callback for each elem */
		for (unsigned int ii = 0; ii < (*L_elems)->length(); ii++) {
			err = P_callback(
			    L_elems[ii].key,
			    L_elems[ii].data,
			    (void *) P_cookie);

			if (err != SCSLM_NOERR)
				goto end;

		}
	}

	err = SCSLM_NOERR;
end:

	return (err);
}

void
scslm_abort_update_table_ccr(const char *P_table)
{
	Environment e;
	ccr::updatable_table_var tabptr;
	unsigned int L_index;

	// Check inputs
	if (!P_table) {
		return;
	}

	// Get the table index
	L_index = get_index_by_tablename(P_table);
	if (L_index == INVALID_TABLE_ID) {
		return;
	}

	if (opened_tables[L_index].presence == 1) {
		tabptr = opened_tables[L_index].list;
		// Abort the transaction
		e.clear();
		tabptr->abort_transaction(e);
		e.clear();
	}
}

/*
 * **********************************************************************
 * Modification of SCSLMADM CCR table
 * **********************************************************************
 */
scslm_error_t
scslmadm_update_table_ccr(const char *P_table, const sensor_t *P_sensor_list)
{
	ccr::element_seq *elems = NULL;
	unsigned int L_index = 0;
	unsigned int table_size = 0;
	scslm_error_t err;
	const sensor_t *ptr = P_sensor_list;
	ki_t *ki_ptr = NULL;

	if ((err = scslm_initialize()) != SCSLM_NOERR) {
		return (err);
	}

	// compute table size
	while (ptr != NULL) {
		ki_ptr = ptr->ki_list;
		while (ki_ptr != NULL) {
			table_size += CCR_FIELDS_PER_KI;
			ki_ptr = ki_ptr->next;
		}
		table_size += CCR_FIELDS_PER_SENSOR;
		ptr = ptr->next;
	}


	/* build new table */

	elems = new ccr::element_seq(table_size);

	if (elems == NULL) {
		err = scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		return (err);
	}
	// reinit pointers
	ptr = P_sensor_list;
	ki_ptr = NULL;

	while (ptr != NULL) {
		(*elems)[L_index].key = sensor_build_key(ptr->id,
		    0, KEY_SENSOR_NAME);
		(*elems)[L_index].data = strdup(ptr->name);
		L_index++;

		(*elems)[L_index].key = sensor_build_key(ptr->id,
		    0, KEY_SENSOR_FILE);
		(*elems)[L_index].data = strdup(ptr->file);
		L_index++;

		(*elems)[L_index].key = sensor_build_key(ptr->id,
		    0, KEY_SENSOR_TYPE);
		(*elems)[L_index].data = strdup(ptr->type);
		L_index++;

		ki_ptr = ptr->ki_list;
		while (ki_ptr != NULL) {
			(*elems)[L_index].key = sensor_build_key(ptr->id,
			    ki_ptr->id, KEY_KI_NAME);
			(*elems)[L_index].data = strdup(ki_ptr->name);
			L_index++;

			(*elems)[L_index].key = sensor_build_key(ptr->id,
			    ki_ptr->id, KEY_KI_UNIT);
			(*elems)[L_index].data = strdup(ki_ptr->unit);
			L_index++;

			(*elems)[L_index].key = sensor_build_key(ptr->id,
			    ki_ptr->id, KEY_KI_ENABLED);
			(*elems)[L_index].data = strdup(ki_ptr->enabled);
			L_index++;

			(*elems)[L_index].key = sensor_build_key(ptr->id,
			    ki_ptr->id, KEY_KI_MOTYPE);
			(*elems)[L_index].data = strdup(ki_ptr->motype);
			L_index++;

			(*elems)[L_index].key = sensor_build_key(ptr->id,
			    ki_ptr->id, KEY_KI_DIVISOR);
			(*elems)[L_index].data = strdup(ki_ptr->divisor);
			L_index++;

			ki_ptr = ki_ptr->next;
		}
		ptr = ptr->next;
	}
	elems->length(L_index);

	err = scslm_update_table_ccr(P_table, elems, 0 /* do not create*/);

	delete elems;

	return (err);
}



/*
 * **********************************************************************
 * Modification of SCSLMTHRESH CCR table
 * **********************************************************************
 */

scslm_error_t
scslmthresh_update_table_ccr(
    const char *P_table,
    const threshold_list_t *P_list,
    int P_create)
{
	ccr::element_seq *L_elems = NULL;
	unsigned int L_one_line;   // in ccr table
	unsigned int L_one_thresh; // in thresh table P_list
	unsigned int ii;	// within a thresh
	unsigned int L_nb_entries = 0;
	scslm_error_t err = SCSLM_NOERR;

	const threshold_list_t *ptr;

	if ((err = scslm_initialize()) != SCSLM_NOERR) {
		goto end;
	}

	// compute table size
	ptr = P_list;
	while (ptr != NULL) {
		if (threshold_is_enabled(ptr->threshold) == 0) {
			ptr = ptr->next;
			continue;
		}

		L_nb_entries += THRESH_NB_FIELDS;
		ptr = ptr->next;
	}

	/* build new table */
	L_elems = new ccr::element_seq(L_nb_entries);

	if (L_elems == NULL) {
		err = scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		goto end;
	}

	// reinit pointers
	L_one_thresh = 1;	// start ar 1 in CCR
	L_one_line = 0;
	ptr = P_list;
	while (ptr != NULL) {

		// shunt disabled thresholds
		if (threshold_is_enabled(ptr->threshold) == 0) {
			ptr = ptr->next;
			continue;
		}

		for (ii = 0; ii < THRESH_NB_FIELDS; ii++) {
			(*L_elems)[L_one_line].key =
			    threshold_build_ccr_key(L_one_thresh, ii);
			(*L_elems)[L_one_line].data =
			    strdup(threshold_get_field(ptr->threshold, ii));
			L_one_line++;
		}
		ptr = ptr->next;
		L_one_thresh++;
	}
	L_elems->length(L_one_line);

	err = scslm_update_table_ccr(P_table, L_elems, P_create);
end:
	if (L_elems) {
		delete L_elems;
	}
	return (err);
}
