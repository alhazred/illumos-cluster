/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef __SCLMADM_CONFIG_H
#define	__SCLMADM_CONFIG_H

#pragma ident	"@(#)scslmadm_config.h	1.6	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * WARNING!!! All the following units must be used at least once in a
 * gettext. to be trapped by "dmake _msg"
 * A fake call is make in scslmadm.cc. If you add one unit, please
 * update also the scslmadm.cc file
 */
#define	SCSLM_UNIT_KBYTES_PER_SEC	"KBytes/s"
#define	SCSLM_UNIT_MBITS_PER_SEC	"Mbits/s"
#define	SCSLM_UNIT_WRITES_PER_SEC	"writes/s"
#define	SCSLM_UNIT_READS_PER_SEC	"read/s"
#define	SCSLM_UNIT_BLOCKS_USED		"%age blocks used"
#define	SCSLM_UNIT_INODES_USED		"%age inodes used"
#define	SCSLM_UNIT_PACKETS_PER_SEC	"packets/s"
#define	SCSLM_UNIT_MBYTES		"MBytes"
#define	SCSLM_UNIT_CPUS			"CPUs"

#ifdef __cplusplus
}
#endif

#endif /* __SCLMADM_CONFIG_H */
