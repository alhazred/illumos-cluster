/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCSLM_CACAO_H
#define	_SCSLM_CACAO_H

#pragma ident	"@(#)scslm_cacao.h	1.4	08/05/20 SMI"

#include <scslm.h>

#define	CACAOCSC_PATH	"/usr/lib/cacao/lib/tools/cacaocsc"

#define	CACAOADM_PATH	"/usr/lib/cacao/bin/cacaoadm"

/* _CACAOCSC_ARGS is private to this file */
#define	_CACAOCSC_ARGS	" -h localhost" \
	" -d /etc/cacao/instances/default/security/nss/wellknown" \
	" -f /etc/cacao/instances/default/security/password" \
	" -c 'com.sun.cluster.agent.ganymede:"

#define	CACAOCSC_SCSLMADM_CMD _CACAOCSC_ARGS"scslmadmCmdStream"
#define	CACAOCSC_SCSLMTHRESH_CMD _CACAOCSC_ARGS"scslmthreshCmdStream"

#define	CACAOCSC_ARGS_END "'"

scslm_error_t
scslmadm_executable_file_exist(const char *P_file);

const char *
get_cacaocsc_path();

scslm_error_t
scslmadm_get_command_stream_adapter_port(int *P_port);


#endif /* _SCSLM_CACAO_H */
