/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCSLM_CLI_H
#define	_SCSLM_CLI_H

#pragma ident	"@(#)scslm_cli.h	1.8	08/05/20 SMI"

// THIS FILE IS CONTAINING STRING FOR CLI
#include <scslm.h>

#define	STR_MOTYPE	"object-type"
#define	STR_MONAME	"object-instance"
#define	STR_NODENAME	"node"
#define	STR_KINAME	"telemetry-attribute"
#define	STR_VALUELIMIT	"value"
#define	STR_RESETVALUE	"rearm"
#define	STR_DIRECTION	"direction"
#define	STR_RISING	"rising"
#define	STR_FALLING	"falling"
#define	STR_SEVERITY	"severity"
#define	STR_WARNING	"warning"
#define	STR_FATAL	"fatal"
#define	STR_UNKNOWN	"unknown"
#define	STR_THRESHOLD	"threshold"
#define	STR_STATUS	"status"
#define	STR_CURRENTVALUE "value"

#define	STR_DISABLED	"disabled"
#define	STR_ENABLED	"enabled"


#define	max(a, b) ((a) > (b) ? (a) : (b))

// The below short options are not exposed to the customers, thus are
// not supported and customers should not use them
#define	STR_MOTYPE_SHORT "ot"
#define	STR_KINAME_SHORT "ta"


#define	RULE "--------------------------------------------------"

/*
 * The Managed Object Types mot.sunw.host and mot.sunw.cluster.resourcegroup are
 * particular in the sense that no host must be specified when creating
 * thresholds for them. Having them hardcoded here isn't appropriate as they
 * exist in two different header file - common/scslm_const.h and
 * common/scslmadm_config.h. This must be changed some time, see RFE 6346092.
 * TODO
 */

#define	MOT_HOST "mot.sunw.node"
#define	MOT_RESOURCEGROUP "mot.sunw.cluster.resourcegroup"


/* TEMPORARY - TO BE REMOVED */
extern scslm_error_t glossary_initialize();
extern const char *make_short_mot(const char *long_mot);
extern const char *make_long_mot(const char *short_mot);
extern const char *make_short_ki(const char *long_ki);
extern const char *make_long_ki(const char *short_ki);
extern scslm_error_t make_long_ki_list(const char *P_input, char **P_output);
extern scslm_error_t convert_date(const char *P_date, long *P_time1,
    long *P_time2);

#endif /* _SCSLM_CLI_H */
