/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslm_cli.cc	1.8	08/06/23 SMI"

#include <scslm.h>		// for error management
#include <scslm_cli.h>
#include <scslm_ccr.h>		// for scslm_read_table
#include <scslm_sensor.h>	// to get ki and mot

#include <assert.h>
#include <stdlib.h>		// for NULL, malloc, ...
#include <string.h>		// for strcmp
#include <stdio.h>		// for BUFSIZ
#include <libintl.h>		// for gettext
#include <sys/types.h>		// for uint_t
#include <strings.h>		// for index

/*
 * This file contains what is necessary to translate objects from
 * internal world to external one. The main task is translating long
 * names (internal) to short names (external).
 *
 * for In -> Out: a canonical form exists depending on the object
 *                MO: last suffix, KI: removing ki.sunw
 * for Out -> In: all discriminant suffixes are accepted
 */

/* Is the module initialized */
static int initialized = 0;

/* when allocating a glossary, make allocation by bundles */
#define	BUNDLE_OF_ENTRIES 10

/* Empty string avoids NULL pointers */
#define	EMPTY_STRING ""

/* What is an entry in the glossary? */
typedef struct {
	char *key;		// a unique key
	char *value;
	// int   hash;
	// TODO: create an hash function
} glossary_entry_t;

/* What is a void entry */
#define	VOID_ENTRY_T { NULL, NULL, /* 0 */}

/* data structure for a full glossary */
typedef struct {
	// char *name;
	glossary_entry_t *glossary;
	uint_t nb_entries;
	uint_t nb_allocated_entries;
} glossary_t;

/*
 * s2l means "short to long"
 * l2s means "long to short"
 */

static glossary_t  G_l2s_KIs = { /* "long to short KIs", */   NULL, 0, 0 };
static glossary_t  G_l2s_MOTs = { /* "long to short MOTs", */ NULL, 0, 0 };
static glossary_t  G_s2l_KIs = { /* "short to long KIs", */   NULL, 0, 0 };
static glossary_t  G_s2l_MOTs = { /* "short to long MOTs", */ NULL, 0, 0 };

#define	FREE(c) { assert(c != NULL); free(c); c = NULL; }

/*
 * ********************************************************************
 * return value from a key or NULL if key is not present
 *
 * TODO: use a HASH function to avoid strcmp
 * ********************************************************************
 */
static const char *glossary_get_value(
    const glossary_t &P_glos,
    const char *P_str)
{
	for (uint_t ii = 0; ii < P_glos.nb_entries; ii++) {
		if (strcmp(P_glos.glossary[ii].key, P_str) == 0)
			return (P_glos.glossary[ii].value);
	}
	return (NULL);
}

/*
 * ********************************************************************
 * Construct the long motype from the short one
 *
 * NOTE: the returned string must *NOT* be modified  by caller
 * ********************************************************************
 */
const char *
make_long_mot(const char *P_short)
{
	if (glossary_initialize() != SCSLM_OK)
		return (NULL);
	if (P_short == NULL)
		return (NULL);
	return (glossary_get_value(G_s2l_MOTs, P_short));
}

/*
 * ********************************************************************
 * Construct the short motype from the long one
 *
 * NOTE: the returned string must *NOT* be modified  by caller
 * ********************************************************************
 */
const char *
make_short_mot(const char *P_long)
{
	if (glossary_initialize() != SCSLM_OK)
		return (NULL);
	if (P_long == NULL)
		return (NULL);
	return (glossary_get_value(G_l2s_MOTs, P_long));
}



/*
 * ********************************************************************
 * Construct the long ki from the short one
 *
 * NOTE: the returned string must *NOT* be modified  by caller
 * ********************************************************************
 */
const char *
make_long_ki(const char *P_short)
{
	if (glossary_initialize() != SCSLM_OK)
		return (NULL);
	if (P_short == NULL)
		return (NULL);
	return (glossary_get_value(G_s2l_KIs, P_short));
}

/*
 * ********************************************************************
 * Construct the short ki from the long one
 *
 * NOTE: the returned string must *NOT* be modified  by caller
 * ********************************************************************
 */
const char *
make_short_ki(const char *P_long)
{
	if (glossary_initialize() != SCSLM_OK)
		return (NULL);
	if (P_long == NULL)
		return (NULL);
	return (glossary_get_value(G_l2s_KIs, P_long));
}


/*
 * ********************************************************************
 * construct a list of long KI name from a coma separated list of short
 * kis. the second parameter will receive the new string and must point
 * to NULL. the returned value is the constructed buffer
 *
 * if an error is detected (return code is SCSLM_ERR) P_output contains
 * the invalid KI
 * ********************************************************************
 */
scslm_error_t
make_long_ki_list(const char *P_input, char **P_output)
{
	scslm_error_t err;
	unsigned int nb_kis = 0;
	unsigned int L_final_length = 0;
	char *token;
	char *L_output;
	char *L_input;

	assert(*P_output == NULL);
	assert(*P_input != NULL);

	/* copy to a new place */
	L_input = strdup(P_input);
	if (L_input == NULL) {
		err = scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		goto end;
	}

	/* first check the string and compute the final length */
	token = strtok(L_input, ",");
	while (token != NULL) {
		const char *L_one_long_KI;

		L_one_long_KI = make_long_ki(token);

		/* THIS TOKEN IS INVALID */
		if (L_one_long_KI == NULL) {
			/* copy the invalid token */
			*P_output = strdup(token);
			err = SCSLM_EPROP;
			goto end;

		}

		nb_kis++;
		L_final_length += strlen(L_one_long_KI);

		token = strtok(NULL, ",");
		/* restore the "," as a second strtok will be done */
		if (token != NULL)
			*(token - 1) = ',';
	}

	/*
	 * We can now allocate the right size that is the total length
	 * of all the long representation of kis, plus, the commas
	 * separating them plus the final 0.
	 */
	*P_output = (char *)malloc(L_final_length + (nb_kis - 1) + 1);
	if (*P_output == NULL) {
		err = scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		goto end;
	}

	/* temp pointer to construct string */
	L_output = *P_output;
	L_output[0] = '\0';

	token = strtok(L_input, ",");
	while (token != NULL) {
		/* make_long_ki cannot be null - as it has been tested */
		L_output = strcat(L_output,  make_long_ki(token));

		token = strtok(NULL, ",");

		/* restore the "," as it is nicer and introduce the right "," */
		if (token != NULL) {
			L_output = strcat(L_output, ",");
			*(token-1) = ',';
		}
	}
	err = SCSLM_NOERR;
end:
	if (L_input)
		FREE(L_input);
	return (err);
}


/*
 * ********************************************************************
 * make suffix longer according to a key
 *
 * return 1 if it has been changed, 0 otherwise
 * (return 0 iff the key is equal to the value)
 *
 * The algorithm consists in inverting the string because it is easier
 * to search prefixes than suffixes. Thus easier to suffix a prefix than
 * prefix a suffix (== enlarge).
 * ********************************************************************
 */
static scslm_error_t enlarge_suffix(glossary_entry_t *P_entry)
{
	scslm_error_t err;
	unsigned int L_str_len;
	unsigned int L_suffix_len;
	char *L_new_prefix = NULL;
	char L_one_char;
	char *L_pos;

	L_str_len = strlen(P_entry->key);
	L_suffix_len = strlen(P_entry->value);

	if (L_suffix_len == L_str_len) {
		err = SCSLM_NOERR;
		goto end;
	}

	/* it's easier to invert the string */
	L_new_prefix = strdup(P_entry->key);
	if (L_new_prefix == NULL) {
		err = scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		goto end;
	}

	for (uint_t ii = 0, jj = L_str_len-1; ii < jj; ii++, jj--) {
		L_one_char = L_new_prefix[ii];
		L_new_prefix[ii] = L_new_prefix[jj];
		L_new_prefix[jj] = L_one_char;
	}

	// the '.' is the separator
	L_pos = strchr(L_new_prefix + L_suffix_len + 1, '.');

	FREE(P_entry->value);
	if (L_pos == NULL) {
		P_entry->value = strdup(P_entry->key);
		if (P_entry->value == NULL) {
			err = scslm_seterror(SCSLM_ENOMEM,
			    gettext("Unable to allocate memory.\n"));
			goto end;
		}
	} else {
		*L_pos = '\0';
		L_str_len = strlen(L_new_prefix);

		/* reinvert */
		uint_t ii = 0;
		uint_t jj = L_str_len - 1;
		for (; ii < jj; ii++, jj--) {
			L_one_char = L_new_prefix[ii];
			L_new_prefix[ii] = L_new_prefix[jj];
			L_new_prefix[jj] = L_one_char;
		}
		P_entry->value = strdup(L_new_prefix);
		if (P_entry->value == NULL) {
			err = scslm_seterror(SCSLM_ENOMEM,
			    gettext("Unable to allocate memory.\n"));
			goto end;
		}
	}
	err = SCSLM_NOERR;
end:

	if (L_new_prefix)
		FREE(L_new_prefix);
	return (err);
}


/*
 * ********************************************************************
 * add en entry in glossary (no checks are made to know if the entry is
 * already in or not.
 *
 * this function is allocating all necessary information. the given entry
 * is duplicated in memory
 *
 * ********************************************************************
 */
static scslm_error_t dup_in_glossary(
    glossary_t *P_glos,
    const glossary_entry_t &P_entry)
{
	glossary_entry_t *L_new_entry;

	if (P_glos->nb_entries == P_glos->nb_allocated_entries) {
		glossary_entry_t *L_tmp;

		P_glos->nb_allocated_entries += BUNDLE_OF_ENTRIES;

		L_tmp = (glossary_entry_t *)
		    realloc((void *) P_glos->glossary,
			P_glos->nb_allocated_entries *
			sizeof (glossary_entry_t));
		if (L_tmp == NULL) {
			return (scslm_seterror(SCSLM_ENOMEM,
				gettext("Unable to allocate memory.\n")));
		}
		P_glos->glossary = L_tmp;
	}

	L_new_entry = &(P_glos->glossary[P_glos->nb_entries++]);

	/* make the copy */
	assert(P_entry.key);
	assert(P_entry.value);

	L_new_entry->key = strdup(P_entry.key);
	if (L_new_entry->key == NULL) {
		return (scslm_seterror(SCSLM_ENOMEM,
			gettext("Unable to allocate memory.\n")));
	}

	L_new_entry->value = strdup(P_entry.value);
	if (L_new_entry->value == NULL) {
		return (scslm_seterror(SCSLM_ENOMEM,
			gettext("Unable to allocate memory.\n")));
	}

	return (SCSLM_NOERR);
}

/*
 * ********************************************************************
 * create a unique value glossary from the keys. the algo consists
 * in enlarging a suffix when it creates conflicts.
 *
 * Warning a conflict can be between more than two keys. Then all
 * conflicting keys must be enlarged.
 *
 * the P_min_size create a minimum size for suffix
 *
 * ********************************************************************
 */
static scslm_error_t
create_glossary_unique_value(glossary_t &P_glos, uint_t P_min_size)
{
	int conflict;
	int at_least_one_conflict;
	scslm_error_t err;

	uint_t ii;
	uint_t jj;

	for (ii = 0; ii < P_glos.nb_entries; ii++) {
		for (jj = 0; jj < P_min_size; jj++) {
			/* We don't care about result here */
			err = enlarge_suffix(&(P_glos.glossary[ii]));

			if (err != SCSLM_OK)
				goto end;
		}
	}


	at_least_one_conflict = 1;
	while (at_least_one_conflict) {
		at_least_one_conflict = 0;
		for (ii = 0; ii < P_glos.nb_entries; ii++) {
			conflict = 0;

			// Enlarge all conflicting entries
			for (jj = 0; jj < P_glos.nb_entries; jj++) {
				if (ii == jj)
					continue;

				if (strcmp(P_glos.glossary[ii].value,
					P_glos.glossary[jj].value) == 0) {
					conflict = 1;
					/* We don't care about result here */
					err = enlarge_suffix(
					    &(P_glos.glossary[jj]));
					if (err != SCSLM_OK)
						return (err);
				}
			}

			if (conflict == 0)
				continue;
			at_least_one_conflict = 1;
			/*
			 * if a conflict has been found; enlarge also reference
			 * We don't care about result here
			 */
			(void) enlarge_suffix(&(P_glos.glossary[ii]));
		}
	}
	err = SCSLM_OK;
end:

	return (err);
}

/*
 * ********************************************************************
 * from long to short, create a short to long glossary where short are
 * every possible usable suffixes giving the long entry
 *
 * ********************************************************************
 */
static scslm_error_t
invert_glossary(const glossary_t &P_glos_in, glossary_t *P_glos_out)
{
	glossary_entry_t L_new_entry = VOID_ENTRY_T;
	glossary_entry_t L_swap_entry = VOID_ENTRY_T;
	scslm_error_t err;

	for (uint_t ii = 0; ii < P_glos_in.nb_entries; ii++) {
		L_new_entry.key = strdup(P_glos_in.glossary[ii].key);
		if (L_new_entry.key == NULL) {
			err = scslm_seterror(SCSLM_ENOMEM,
			    gettext("Unable to allocate memory.\n"));
			goto end;
		}

		L_new_entry.value = strdup(P_glos_in.glossary[ii].value);
		if (L_new_entry.value == NULL) {
			err = scslm_seterror(SCSLM_ENOMEM,
			    gettext("Unable to allocate memory.\n"));
			goto end;
		}

		/* copy (key, value) -> (value, key) */
		L_swap_entry.key = L_new_entry.value;
		L_swap_entry.value = L_new_entry.key;

		err = dup_in_glossary(P_glos_out, L_swap_entry);
		if (err != SCSLM_NOERR)
			goto end;

		/*
		 * create all keys longer than the found one. as the
		 * found one is unique, longer keys are also unique */
		while (strlen(L_new_entry.key) != strlen(L_new_entry.value)) {
			err = enlarge_suffix(&L_new_entry);
			if (err != SCSLM_NOERR)
				goto end;

			L_swap_entry.key = L_new_entry.value;
			L_swap_entry.value = L_new_entry.key;
			err = dup_in_glossary(P_glos_out, L_swap_entry);
			if (err != SCSLM_NOERR)
				goto end;
		}
		FREE(L_new_entry.key);
		FREE(L_new_entry.value);
	}
	err = SCSLM_NOERR;
end:
	return (err);
}



/*
 * ********************************************************************
 * returns 0 is the string is *not* in the glossary (as SHORT or LONG
 * according to P_type). otherwise return -1
 *
 * ********************************************************************
 */
static int is_in_glossary(
    const glossary_t &P_glos,
    const char *P_str)
{
	for (uint_t ii = 0; ii < P_glos.nb_entries; ii++) {
		assert(P_glos.glossary[ii].key != NULL);
		if (strcmp(P_glos.glossary[ii].key, P_str) == 0)
			return (-1);
	}
	return (0);
}

/*
 * ********************************************************************
 * destroy totally a glossary
 * ********************************************************************
 */
static void
free_glossary(glossary_t *P_glos)
{
	for (uint_t ii = 0; ii < P_glos->nb_entries; ii++) {
		FREE(P_glos->glossary[ii].key);
		FREE(P_glos->glossary[ii].value);
	}
}

/*
 * ********************************************************************
 * This function is filling the glossary from long to short names
 *
 * ********************************************************************
 */
scslm_error_t
glossary_initialize()
{
	sensor_t *L_sensor_list = NULL;
	sensor_t *L_sensor_ptr = NULL;
	scslm_error_t  err;
	glossary_entry_t L_entry;
	char *L_empty_string = EMPTY_STRING;

	/*
	 * these two glossary have no constraints and they do not
	 * satisfy output there are here to build the inverse glossary
	 * (better for input)
	 */
	glossary_t L_full_l2s_KIs  = { /* "TEMP KI", */ NULL, 0, 0  };

	ki_t *L_ki_ptr = NULL;

	if (initialized == 1) {
		err = SCSLM_NOERR;
		goto end;
	}

	err = scslm_read_table_ccr(SCSLMADM_TABLE,
	    store_in_sensor_list,
	    (void *)&L_sensor_list,
	    0 /* keep_alive */);

	if (err != SCSLM_NOERR) {
		goto end;
	}

	/* fill the glossaries with the read entries */
	L_entry.key  = NULL;
	L_sensor_ptr = L_sensor_list;
	while (L_sensor_ptr != NULL) {
		L_ki_ptr = L_sensor_ptr->ki_list;
		while (L_ki_ptr != NULL) {
			if (!is_in_glossary(G_l2s_KIs, L_ki_ptr->name)) {
				L_entry.key = L_ki_ptr->name;
				/*
				 * default SHORT ki names is just
				 * removing the prefix ki.sunw */
				L_entry.value = strdup(L_entry.key + 8);
				if (L_entry.value == NULL) {
					err = scslm_seterror(SCSLM_ENOMEM,
					    gettext("Unable to "
						"allocate memory.\n"));
					goto end;
				}

				err = dup_in_glossary(&G_l2s_KIs, L_entry);

				if (err != SCSLM_OK)
					goto end;

				FREE(L_entry.value);

				L_entry.value = L_empty_string;
				if (L_entry.value == NULL) {
					err = scslm_seterror(SCSLM_ENOMEM,
					    gettext("Unable to "
						"allocate memory.\n"));
					goto end;
				}
				err = dup_in_glossary(&L_full_l2s_KIs,
				    L_entry);

				if (err != SCSLM_OK)
					goto end;
				L_entry.value = NULL; /* avoid bad free */
			}
			if (!is_in_glossary(G_l2s_MOTs, L_ki_ptr->motype)) {
				L_entry.key = L_ki_ptr->motype;
				L_entry.value = strdup(EMPTY_STRING);
				if (L_entry.value == NULL) {
					err = scslm_seterror(SCSLM_ENOMEM,
					    gettext("Unable to "
						"allocate memory.\n"));
					goto end;
				}
				err = dup_in_glossary(&G_l2s_MOTs, L_entry);
				if (err != SCSLM_OK) {
					FREE(L_entry.value);
					goto end;
				}
				FREE(L_entry.value);
			}

			L_ki_ptr = L_ki_ptr->next;
		}
		L_sensor_ptr = L_sensor_ptr->next;
	}

	/*
	 * compute short notation for MOT....by default th eshort
	 * notation has at least one (i.e parameter 2 equals 1) word
	 * (this is an easy optimization)
	 */
	err = create_glossary_unique_value(G_l2s_MOTs, 1);

	if (err != SCSLM_OK)
		goto end;

	/*
	 * build the input glossary for MOT from the output one
	 */
	err = invert_glossary(G_l2s_MOTs, &G_s2l_MOTs);

	if (err != SCSLM_OK)
		goto end;


	/*
	 * this glossary is filled by ki where short notation is the
	 * long without "mot.sunw". there should be no
	 * conflict... let's do it in case
	 */
	err = create_glossary_unique_value(G_l2s_KIs, 0 /* already ok */);

	if (err != SCSLM_OK)
		goto end;

	/*
	 * build the input glossary for KI from the unconstraint output one
	 * (i.e even "1mn" is accepted)
	 */
	err = create_glossary_unique_value(L_full_l2s_KIs, 1);

	if (err != SCSLM_OK)
		goto end;

	err = invert_glossary(L_full_l2s_KIs, &G_s2l_KIs);

	if (err != SCSLM_OK)
		goto end;


	free_glossary(&L_full_l2s_KIs);
	free(L_full_l2s_KIs.glossary);

	err = SCSLM_NOERR;
end:
	if (err != SCSLM_NOERR) {
		free_glossary(&G_l2s_MOTs);
		free(G_l2s_MOTs.glossary);
		free_glossary(&G_s2l_MOTs);
		free(G_s2l_MOTs.glossary);
		free_glossary(&G_l2s_KIs);
		free(G_l2s_KIs.glossary);
		free_glossary(&G_s2l_KIs);
		free(G_s2l_KIs.glossary);
	}
	else
		initialized = 1;

	if (L_sensor_list) {
		free_sensor_list(&L_sensor_list);
	}
	return (err);
}

/*
 * ********************************************************************
 * convernt the string given in -y (i.e date range)
 * int two integers. The accepted date formats are derived from ISO
 * standard (i.e: www.w3.org/TR/NOTE-datetime).
 *
 *        format 1: YYYY-MM-DD
 *        format 2: YYYY-MM-DD"T"HH:MM
 *        format 3: YYYY-MM-DD"T"HH:MM:SS
 *
 * Params:
 *    o P_date: strong to parse
 *    o P_is_start: is the parsed date a start or an end of interval?
 *                  This change the way to interpret a date with now hour
 *                  o start => no hour = 00:00:00
 *                  o end   => no hour = 23:59:59
 *                  o end   => no sec  = HH:MM:59
 *
 *    o P_time: Struct receiving the result of the parsed string
 *
 * Returned code:
 *    o status
 *
 * ********************************************************************
 */
static scslm_error_t
convert_one_date(
    char *P_date,
    boolean_t P_is_start,
    long *P_time)
{
	struct tm L_tm;
	char *L_at;
	int L_offset = 0;

	(void) memset(&L_tm, 0, sizeof (L_tm));
	L_at = strptime(P_date, "%Y-%m-%d", &L_tm);

	if (L_at == NULL) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("Invalid date: %s.\n"), P_date);
		goto end;
	}

	if (*L_at == 'T') {
		L_at = strptime(P_date, "%Y-%m-%dT%H:%M:%S", &L_tm);

		if (L_at == NULL) {
			L_offset = 59;
			L_at = strptime(P_date, "%Y-%m-%dT%H:%M", &L_tm);
		}

		if (L_at == NULL) {
			(void) scslm_seterror(SCSLM_EINVAL,
			    gettext("Invalid date: %s.\n"), P_date);
			goto end;
		}
	} else
		L_offset = 60 * 60 * 24 - 1;

	L_tm.tm_isdst = -1;
	*P_time = mktime(&L_tm);
	if (P_is_start == B_FALSE)
		*P_time += L_offset;

	/* the parsed string has characters behind? */
	if ((L_at != NULL) && (*L_at != '\0'))
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("Invalid date: %s.\n"), P_date);

end:
	return (scslm_errno);
}

/*
 * ********************************************************************
 * convert a string (from Command line) to interval dates.
 * The string can have the following format:
 *
 *     t1,t2 or t1+ or t2-
 *
 * Params:
 *    o P_date: string to parse
 *    o P_time1: lower bound (i.e. first date)
 *    o P_time2: upper bound (i.e. second date)
 *
 * Returned code:
 *    o status
 *
 * ********************************************************************
 */
scslm_error_t
convert_date(const char *P_date, long *P_time1, long *P_time2)
{
	char *L_temp;
	char *L_option;
	char *L_last_char;
	assert(P_date != NULL);
	assert(P_time1 != NULL);
	assert(P_time2 != NULL);

	*P_time1 = 0;
	*P_time2 = 0;

	L_temp = strdup(P_date);
	if (L_temp == NULL) {
		(void) scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		goto end;
	}

	if (strlen(L_temp) == 0) {
		(void) scslm_seterror(SCSLM_EINVAL,
		    gettext("Invalid date: %s.\n"), P_date);
		goto end;
	}


	L_last_char = L_temp + strlen(L_temp) - 1;

	/* Does the string contain a comma */

	if ((L_option = index(L_temp, ',')) != NULL) {
		*L_option = '\0';

		if (convert_one_date(L_temp, B_TRUE, P_time1) != SCSLM_NOERR)
			goto end;

		if (convert_one_date(L_option + 1, B_FALSE, P_time2)
		    != SCSLM_NOERR)
			goto end;

		goto end;
	}

	/* Does the string contain a '+' */
	if (*L_last_char == '+') {
		*L_last_char = '\0';

		/* we cast to void as the return code is in scslm_errno */
		(void) convert_one_date(L_temp, B_TRUE, P_time1);
		goto end;
	}

	/* Does the string contain a '-' */
	if (*L_last_char == '-') {
		*L_last_char = '\0';

		/* we cast to void as the return code is in scslm_errno */
		(void) convert_one_date(L_temp, B_FALSE, P_time2);
		goto end;
	}

	(void) scslm_seterror(SCSLM_EINVAL,
	    gettext("Invalid date: %s.\n"), P_date);


end:
	if (L_temp != NULL) {
		free(L_temp); L_temp = NULL;
	}
	return (scslm_errno);
}
