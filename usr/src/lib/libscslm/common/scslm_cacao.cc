/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslm_cacao.cc	1.3	08/05/20 SMI"

#include <unistd.h>
#include <libgen.h>
#include <strings.h>
#include <scslm_cacao.h>
#include <locale.h>
#include <stdlib.h>

/*
 * ********************************************************************
 *
 * Verify that the file provided in input exists and is executable
 *
 * ********************************************************************
 */

scslm_error_t
scslmadm_executable_file_exist(const char *P_file)
{
	scslm_error_t err = SCSLM_NOERR;
	if (access(P_file, F_OK | X_OK) != 0) {
		err = scslm_seterror(SCSLM_EACCESS,
		    gettext("Unable to access \"%s\".\n"),
		    P_file);
	}
	return (err);
}

static const char *
get_cacaoadm_path(void)
{
	const char *path = CACAOADM_PATH;

	if (scslmadm_executable_file_exist(path) != SCSLM_NOERR) {
		path = NULL;
	}

	return (path);
}

const char *
get_cacaocsc_path(void)
{
	const char *path = CACAOCSC_PATH;

	if (scslmadm_executable_file_exist(path) != SCSLM_NOERR) {
		path = NULL;
	}

	return (path);
}

/*
 * ********************************************************************
 * get the port from the cacao command adapter. This information is
 * known by calling "cacaoadm"
 *
 * Params:
 *    o P_port [output]: the port.
 *
 * Returned value:
 *    o status
 *
 * ********************************************************************
 */
scslm_error_t
scslmadm_get_command_stream_adapter_port(int *P_port)
{
	const char *cacaoadm_path = get_cacaoadm_path();
	char *cacaoadm_options = "get-param -v commandstream-adaptor-port";
	scslm_error_t err;
	char buffer[BUFSIZ] = "";
	FILE *ptr;

	*P_port = -1;
	char *command = (char *)malloc(strlen(cacaoadm_path) +
	    1 /* space */ +
	    strlen(cacaoadm_options) +
	    1 /* \0 */);
	if (command == NULL) {
		err = scslm_seterror(SCSLM_ENOMEM,
		    gettext("Unable to allocate memory.\n"));
		goto end;
	}

	(void) sprintf(command, "%s %s", cacaoadm_path, cacaoadm_options);


	if ((ptr = popen(command, "r")) == NULL) {
		err = scslm_seterror(SCSLM_EINTERNAL,
		    gettext("Unable to open a pipe to \"%s\".\n"),
		    command);
		goto end;
	}

	(void) fgets(buffer, BUFSIZ, ptr);

	if (strlen(buffer) == 0) {
		err = scslm_seterror(SCSLM_EIO,
		    gettext("Void answer from \"%s\".\n"), command);
		goto end;
	}


	(void) pclose(ptr);
	*P_port = atoi(buffer);
	err = SCSLM_NOERR;
end:
	if (command != NULL)
		free(command);
	return (err);
}
