/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCSLM_SENSOR_H
#define	_SCSLM_SENSOR_H

#pragma ident	"@(#)scslm_sensor.h	1.5	08/05/20 SMI"

#include <scslm.h>

#define	SCSLMADM_TABLE	"scslmki"

#define	SCSLMADM_TRUE	"true"
#define	SCSLMADM_FALSE	"false"



typedef struct ki_internal {
	int id;
	char *name;
	char *unit;
	char *enabled;
	char *motype;
	char *divisor;
	struct ki_internal *next;
} ki_t;

typedef struct sensor_internal {
	int id;
	char *name;
	char *file;
	char *type;
	ki_t *ki_list;
	struct sensor_internal *next;
} sensor_t;

typedef enum {
	KEY_UNKNOWN = 0,
	KEY_SENSOR_NAME,
	KEY_SENSOR_FILE,
	KEY_SENSOR_TYPE,
	KEY_KI_NAME,
	KEY_KI_UNIT,
	KEY_KI_ENABLED,
	KEY_KI_MOTYPE,
	KEY_KI_DIVISOR
} key_type_t;

// number of fields ki related (#KEY_KI*)
#define	CCR_FIELDS_PER_KI 5

// number of fields ki related (#KEY_SENSOR*)
#define	CCR_FIELDS_PER_SENSOR 3


extern const char *
key_type_t2string(key_type_t key);

extern const char *
sensor_build_key(int sensor_id, int ki_id, key_type_t type);

extern scslm_error_t
sensor_parse_key(const char *key, int *sensor_id, int *ki_id, key_type_t *type);

extern sensor_t *
get_sensor(sensor_t **list, int sensor_id);

extern ki_t *
get_ki(ki_t **ki, int ki_id);

extern scslm_error_t
set_sensor_property(sensor_t *sensor, key_type_t type, const char *data);

extern scslm_error_t
set_ki_property(ki_t *ki, const key_type_t type, const char *data);

extern scslm_error_t
store_in_sensor_list(const char *key, char *data, void *list);

extern unsigned int
get_nb_ki_in_sensor_list(const sensor_t *sensor_list);

extern scslm_error_t
l10n_sensor_list(const sensor_t *);

extern sensor_t *
free_sensor(sensor_t *l);

extern void
free_sensor_list(sensor_t **l);

extern ki_t *
fetch_ki_from_sensor_list(
    const sensor_t *P_sensor_list,	/* list to scan */
    const char *P_name,		/* name to find */
    const char *P_type);		/* type to find */

#endif /* _SCSLM_SENSOR_H */
