/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCSLM_THRESHOLD_H
#define	_SCSLM_THRESHOLD_H

#pragma ident	"@(#)scslm_threshold.h	1.2	08/05/20 SMI"

#include <scslm.h>	// for scslm_error_t

#define	SCSLMTHRESH_TABLE	"scslmthresh"

// Invalid Id is an Id that will never be found in CCR
#define	INVALID_THRESH_ID	10000001
#define	VOID_FIELD_ID		10000001

enum {
    THRESH_MONAME,
    THRESH_MOTYPE,
    THRESH_NODENAME,
    THRESH_KINAME,
    THRESH_RISING_WARNING_VALUE,
    THRESH_RISING_WARNING_REARM,
    THRESH_RISING_FATAL_VALUE,
    THRESH_RISING_FATAL_REARM,
    THRESH_FALLING_WARNING_VALUE,
    THRESH_FALLING_WARNING_REARM,
    THRESH_FALLING_FATAL_VALUE,
    THRESH_FALLING_FATAL_REARM,
    THRESH_NB_FIELDS
};

typedef char *threshold_t[THRESH_NB_FIELDS];

/* functions managing allocation, deallocation, getter and setter */
extern threshold_t *
threshold_allocate();
extern void
threshold_free(threshold_t **P_thresh);
extern scslm_error_t
threshold_set_field(threshold_t *P_thresh,
    unsigned int P_field,
    const char *P_value);
extern const  char *
threshold_get_field(const threshold_t *P_thresh,
    unsigned int P_field);


/* functions managing enability */
extern int
threshold_is_enabled(const threshold_t *P_thresh);
extern scslm_error_t
threshold_disable(threshold_t *P_thresh);

/* check the value/rearm are ok */
extern scslm_error_t
threshold_is_ok(const threshold_t *P_thresh);

/* functions related to the read/write of ccr keys */
extern char *threshold_build_ccr_key(
    unsigned int P_id,
    unsigned int P_field);

extern scslm_error_t threshold_parse_ccr_key(
    const char *P_line,
    unsigned int *P_id,
    unsigned int *P_field);


/* function managing a list of thresholds */
typedef struct threshold_list_t
{
	threshold_t *threshold;
	unsigned int id_in_ccr;		/* only used at read */
	struct threshold_list_t *next;
	/* == 1: when the elem is the head of a list, 0 otherwise */
	int is_head;
} threshold_list_t;



extern threshold_list_t *threshold_list_allocate();

extern scslm_error_t threshold_list_free(
    threshold_list_t **P_list);

extern threshold_t *threshold_list_extract(
    const threshold_list_t *P_list,
    unsigned int P_id);

extern scslm_error_t threshold_list_insert(
    threshold_list_t **P_list,
    unsigned int P_id_in_ccr,
    threshold_t *P_thresh);

extern unsigned int threshold_list_get_new_id(
    const threshold_list_t *P_list);

extern unsigned int threshold_list_get_matching_id(
    const threshold_list_t *P_list,
    const threshold_t *P_thresh);

extern void threshold_list_print(
    const threshold_list_t *P_list,
    const threshold_t *P_filter);




#endif /* _SCSLM_THRESHOLD_H */
