/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scslm.cc	1.2	08/05/20 SMI"

#include <scslm.h>
#include <stdio.h>		// for puts, fprintf, stderr, ...

static char *error_strings[] = {
	"no error",
	"Allocation Error",
	"Invalid Parameter",
	"Permission Denied",
	"Object in Wrong State",
	"Invalid Method",
	"Invalid Property",
	"Internal Error",
	"I/O Error",
	"Object Does Not Exist",
	"Disallowed Operation",
	"Device Busy",
	"Object Exists",
	"Unknown Type"
};



scslm_error_t scslm_errno = SCSLM_OK;

#define	ERROR_BUFFER_SIZE	256
static char scslm_error[ERROR_BUFFER_SIZE] = "";

/*
 * ********************************************************************
 *
 * set an error and fill the bugger with an explanation text
 *
 * ********************************************************************
 */
scslm_error_t
scslm_seterror(
    scslm_error_t P_error,
    char *P_fmt, ...)
{
	va_list ap;
	/*lint -save -e50 -e10 -e26 -e40 */
	/* Undeclared identifier (__builtin_va_alist) */
	va_start(ap, P_fmt);
	/*lint -restore */

	(void) vsnprintf(scslm_error, ERROR_BUFFER_SIZE, P_fmt, ap);
	scslm_errno = P_error;
	va_end(ap);
	return (P_error);
}


/*
 * ********************************************************************
 *
 * clear a error to ignore it.
 *
 * ********************************************************************
 */
void
scslm_clearerror()
{
	scslm_errno = SCSLM_NOERR;
}

/*
 * ********************************************************************
 *
 * print the error message
 *
 * ********************************************************************
 */
void
scslm_perror(char *P_prefix)
{
	if (scslm_errno == SCSLM_OK)
		return;

	(void) fprintf(stderr, "%s: (%s) %s",
	    ((P_prefix != NULL) ? P_prefix : "ERROR"),
	    error_strings[scslm_errno],
	    ((scslm_error != NULL) ? scslm_error : ""));
}
