/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SCSLM_H
#define	_SCSLM_H

#pragma ident	"@(#)scslm.h	1.2	08/05/20 SMI"

#include <stdarg.h>

typedef enum {
	SCSLM_NOERR = 0,
	SCSLM_OK = 0,		/* CL_NOERROR */
	SCSLM_ENOMEM,		/* not enough swap */
	SCSLM_EINVAL,		/* invalipd input param */
	SCSLM_EACCESS,		/* permission denied */
	SCSLM_ESTATE,		/* object in wrong state */
	SCSLM_EMETHOD,		/* invalid method */
	SCSLM_EPROP,		/* invalid property */
	SCSLM_EINTERNAL,	/* internal error */
	SCSLM_EIO,		/* IO error */
	SCSLM_ENOENT,		/* object does not exist */
	SCSLM_EOP,		/* dissallowed operation */
	SCSLM_EBUSY,		/* device busy */
	SCSLM_EEXIST,		/* object exists */
	SCSLM_ETYPE		/* unknown type */
} scslm_error_t;

extern void scslm_perror(char *P_prefix);

extern scslm_error_t scslm_seterror(scslm_error_t error, char *P_fmt, ...);

extern void scslm_clearerror();

extern scslm_error_t scslm_errno;


#endif /* _SCSLM_H */
