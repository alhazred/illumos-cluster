#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.17	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libclconf/Makefile.com
#

LIBRARYCCC= libclconf.a
VERS= .1

include $(SRC)/common/cl/Makefile.files

OBJECTS += $(CLCONF_UOBJS) $(TM_UOBJS) $(CL_LIBDCS_UOBJS)

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib
CC_PICFLAGS = -KPIC

CLOBBERFILES +=	$(RPCFILES)

SRCS =		$(OBJECTS:%.o=../common/%.c)

MTFLAG= -mt

CPPFLAGS += $(MTFLAG)
CPPFLAGS += $(CL_CPPFLAGS)

CPPFLAGS += -I$(SRC)/common/cl -I$(SRC)/common/cl/interfaces/$(CLASS)
CPPFLAGS += -I$(SRC)/common/cl/interfaces/$(CLASS)/h -I$(SRC)/common/cl/dc/sys
CPPFLAGS += -I$(SRC)/common/cl/sys -I$(SRC)/common/cl/clconf
CPPFLAGS += -I$(SRC)/common/cl/dc/libdcs
CCFLAGS += -noex -xildoff  +p +w $(CCERRWARN)
CCFLAGS64 += -noex -xildoff  +p +w $(CCERRWARN)

LIBS = $(DYNLIBCCC) $(LIBRARYCCC)

# Warning: new dependencies here should be marked in cmd/scrconf/Makefile also
# to build the static command
LDLIBS += -lclcomm -lclos -lnsl -lsocket -ldoor -lthread -lc -lmd5

CLEANFILES =
LINTFILES += $(OBJECTS:%.o=%.ln)
CHECKHDRS = $(CLCONF_HG) $(CLCONF_HGIN)
CHECKHDRS += $(TM_HU) $(TM_HUIN)
CHECKHDRS += $(CL_LIBDCS_HG)

PMAP =

OBJS_DIR = objs
OBJS_DIR += pics

.KEEP_STATE:

.NO_PARALLEL:

all: $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules
