/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)hbrsimu.c 1.3	08/05/20 SMI"

#include <stdio.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>

#include <fmm/hbrsimu.h>
#include <fmm/hbr.h>

static int fifoid = -1;
static pthread_t recvThreadId = (pthread_t)-1;
hb_callback *callback = NULL;

hb_error_t
hbr_init(hb_callback *cb)
{
	char fifo_name[PATH_MAX];
	mode_t oldMask;

	oldMask = umask(0);
	(void) sprintf(fifo_name, "%s/%s", HBSIMU_TMP_DIR, HBSIMU_FIFO);

	if (mkfifo(fifo_name, (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1) {
		if (errno == EEXIST) { /*lint !e746 */
			(void) remove(fifo_name);
			if (mkfifo(fifo_name,
			    (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1) {
				(void) umask(oldMask);
				return (HB_EINVAL);
			}
		} else {
			(void) umask(oldMask);
			return (HB_EINVAL);
		}
	}

	(void) umask(oldMask);

	callback = cb;
	return (HB_OK);
}


/*ARGSUSED*/
static void *
recvThread(void *param)
{
	ssize_t nbytes = 0;
	boolean_t go_on = B_TRUE;
	hbsimu_msg_t message;
	char fifo_name[PATH_MAX];

	(void) sprintf(fifo_name, "%s/%s", HBSIMU_TMP_DIR, HBSIMU_FIFO);

new_writer:
	/* open gets blocked until there is a writer process */
	fifoid = open(fifo_name, O_RDONLY);

	if (fifoid == -1) {
		(void) remove(fifo_name);
		return (NULL);
	}

	while (go_on) {
		nbytes = read(fifoid, &message, sizeof (hbsimu_msg_t));
		switch (nbytes) {
		case 0:
			/* no writer process */
			/* close the pipe and retry to open */
			(void) close(fifoid);
			goto new_writer;
		case -1:
			go_on = B_FALSE;
			break;
		case (sizeof (hbsimu_msg_t)):
			/* call the callback */
			if (callback)
				callback(message.nodeid, message.event,
				    message.incarnation);
			break;
		default:
			break;
		}
	}
	return (NULL);
}

hb_error_t
hbr_start(void)
{
	pthread_attr_t attr;

	(void) pthread_attr_init(&attr);
	if (pthread_create(&recvThreadId, & attr, recvThread, NULL) != 0) {
		recvThreadId = (pthread_t)-1;
		return (HB_EINVAL);
	}
	(void) pthread_attr_destroy(&attr);

	return (HB_OK);
}

hb_error_t
hbr_stop(void)
{
	return (HB_OK);
}

hb_error_t
hbr_reset(void)
{
	return (HB_OK);
}

/*ARGSUSED*/
hb_error_t
hbr_delete_node(hb_nodeid_t nodeid)
{
	return (HB_OK);
}
