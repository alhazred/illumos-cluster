#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.6	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libscprivip/Makefile.com
#

LIBRARY= libscprivip.a
VERS=.1

include $(SRC)/common/cl/Makefile.files

OBJECTS = $(PRIVIP_UOBJS)

# include library definitions
include ../../Makefile.lib

MAPFILE=	../common/mapfile-vers

LIBS =		$(DYNLIB) $(LIBRARY)

LINTFLAGS +=	-I..
LINTFILES +=	$(OBJECTS:%.o=%.ln)

MTFLAG	=	-mt
CPPFLAGS += $(CL_CPPFLAGS)
CPPFLAGS += -I$(SRC)/common/cl -I$(SRC)/common/cl/interfaces/$(CLASS)
CPPFLAGS += -I$(SRC)/common/cl/sys -I$(SRC)/common/cl/clconf
CPPFLAGS += -I$(SRC)/common/cl/privip
PMAP = -M $(MAPFILE)

LDLIBS += -lc $(REF_USRLIB:%=%/libCrun.so.1) -lthread -ldl -lclcomm
LDLIBS += -lclconf -lsocket -lnsl -lclos

CHECKHDRS = $(PRIVIP_HGIN) $(PRIVIP_HG)

$(DYNLIB):	$(MAPFILE)

all: $(LIBS)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules
