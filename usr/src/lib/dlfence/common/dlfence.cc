/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * dlfence.c
 *
 *	fence API through dynamic linking ...
 */

#pragma ident	"@(#)dlfence.cc	1.3	08/05/20 SMI"

#include <dlfcn.h>
#include "dlfence.h"

static char *funcnames[] = {
	"fenceInitialize",
	"fenceNodes",
	"unFenceNodes",
	"fenceFinalize"
};

static char path[] = FENCE_LIB_PATH;

/*
 * Load fence dynamic library. Returns instance of opened library,
 * and populates the api argument with the functions addresses exported
 * by the API.
 * Return 0 in case of success, -1 otherwise.
 */
int
fence_load_module(void **instance, fence_api_t *api)
{
	int i;
	*instance = dlopen(path, RTLD_LAZY);
	if (*instance == NULL) {
		return (-1);
	}

	if ((((fenceInitialize_function_t *)api)[0] =
	    (fenceInitialize_function_t)dlsym(*instance,
	    funcnames[0])) == NULL) {
		(void) fence_unload_module(instance, api);
		return (-1);
	}

	if ((((fenceNodes_function_t *)api)[1] =
	    (fenceNodes_function_t)dlsym(*instance,
	    funcnames[1])) == NULL) {
		(void) fence_unload_module(instance, api);
		return (-1);
	}

	if ((((unFenceNodes_function_t *)api)[2] =
	    (unFenceNodes_function_t)dlsym(*instance,
	    funcnames[2])) == NULL) {
		(void) fence_unload_module(instance, api);
		return (-1);
	}

	if ((((fenceFinalize_function_t *)api)[3] =
	    (fenceFinalize_function_t)dlsym(*instance,
	    funcnames[3])) == NULL) {
		(void) fence_unload_module(instance, api);
		return (-1);
	}

	return (0);
}

/*
 * Unload a fence library instance.
 */
int
fence_unload_module(void **instance, fence_api_t *api)
{
	static fence_api_t null_api;

	if (dlclose(*instance) != 0) {
		return (-1);
	}
	*instance = NULL;
	*api = null_api;
	return (0);
}
