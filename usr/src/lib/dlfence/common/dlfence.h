/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

/*
 * fence.h
 *
 *	fence API through dynamic linking ...
 */

#ifndef	_DLFENCE_H
#define	_DLFENCE_H

#pragma ident	"@(#)dlfence.h	1.3	08/05/20 SMI"

#include <fence.h>

#ifdef __cplusplus
extern "C" {
#endif

#define	FENCE_LIB_PATH	"/usr/cluster/lib/libfence.so.1"

typedef int (*fenceInitialize_function_t)(scxcfg_t cfg);
typedef int (*fenceNodes_function_t)(nodeList_t &nl);
typedef int (*unFenceNodes_function_t)(nodeList_t &nl);
typedef int (*fenceFinalize_function_t)();

typedef struct {
	fenceInitialize_function_t fenceInitialize;
	fenceNodes_function_t fenceNodes;
	unFenceNodes_function_t unFenceNodes;
	fenceFinalize_function_t fenceFinalize;
} fence_api_t;

int fence_load_module(void **instance, fence_api_t *api);
int fence_unload_module(void **instance, fence_api_t *api);

#ifdef __cplusplus
}
#endif

#endif /* _DLFENCE_H */
