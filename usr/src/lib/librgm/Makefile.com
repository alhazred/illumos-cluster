#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)Makefile.com	1.22	08/05/20 SMI"
#
# lib/librgm/Makefile.com
#

LIBRARYCCC = librgm.a
VERS = .1

#
# This library contains
#
#	code common to rgmd and rgmd clients:
#
#		rgm_common.o
#		rgm_ccrget.o
#		rgm_ccrput.o
#		rgm_datatype.o
#		rgm_cnfg.o
#		rgm_free.o
#		rgm_errmsg.o
#
#	code needed only by clients of rgmd:
#
#		rgm_rt.o
#		rgm_scrgadm.o
#		rgm_scswitch.o
#
#	rgmd server interposes its own fail_ccr_read_write() routine;
#	the one in rgm_client_util.o is used only by rgmd clients
#
#		rgm_client_util.o
#

OBJECTS = rgm_common.o rgm_ccrget.o rgm_ccrput.o rgm_datatype.o rgm_cnfg.o
OBJECTS += rgm_free.o rgm_rt.o rgm_scrgadm.o rgm_scswitch.o rgm_client_util.o
OBJECTS += rgm_errmsg.o rgm_event.o rgm_scconf.o

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

# include the macros for doors or rpc implementation
include $(SRC)/Makefile.ipc

MAPFILE=	../common/mapfile-vers
PMAP =
SRCS =		$(OBJECTS:%.o=../common/%.c)

LIBS = $(DYNLIBCCC) $(LIBRARYCCC)

CLEANFILES =

# definitions for lint
LINTFILES += $(OBJECTS:%.o=%.ln)

MTFLAG	= -mt

CPPFLAGS += $(CL_CPPFLAGS)
CPPFLAGS += -I$(SRC)/common/cl/interfaces/$(CLASS) -I$(SRC)/common/cl
CPPFLAGS += -I$(SRC)/lib/libclcomm/common
CPPFLAGS += -I$(SRC)/head/scadmin/rpc
CPPFLAGS += -I$(SRC)/lib/dlscxcfg/common

$(S9_BUILD)LDLIBS += $(SPRO_LIBS)/libCstd.a $(SPRO_LIBS)/libCrun.a
CPPFLAGS += -DSC_SRM

# Warning: new dependencies here should be marked in cmd/scrconf/Makefile also
# to build the static command
LDLIBS += -lc -ldl -lclcomm -lclconf -lclos -lscha -ldlscxcfg -lsczones

.KEEP_STATE:

all:  $(LIBS)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<
