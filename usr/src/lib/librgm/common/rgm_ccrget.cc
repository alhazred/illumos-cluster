//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_ccrget.cc	1.62	08/06/20 SMI"

#include <map>
#include <h/ccr.h>
#include <rgm/rgm_cmn_proto.h>
#include <rgm/rgm_cnfg.h>
#include <rgm/rgm_private.h>
#include "rgm_prop_descriptions.h"	// For System property descriptions

using namespace std;
//
// This file contains several functions (named ccr_get_xxx) to read
// specific fields from CCR tables.
//
// Each ccr_get_xxx function is passed a read-only pointer to a CCR table
// and the name of a field to read. The caller obtains the table pointer by
// calling lookup() with the table name. While one process is trying to
// read the table, an admin tool can update or delete the table. (The
// update or delete operation is done via the rgmd President.) If the CCR
// table has been modified or deleted after the table pointer is obtained,
// the CCR returns a table_modified exception when the pointer is used to
// read the table. When this happens, the CCR table reader should simply
// reopen the table and retry the operation.
//
// Since the ccr_get_xxx function is given only the table pointer and
// not the table name, it cannot reopen the table. Instead, it returns the
// error SCHA_ERR_SEQID. The caller should then reopen the table and call
// ccr_get_xxx again with the new pointer.
//

static scha_err_t set_default_client_aff_props(rgm_param_t **paramtable[]);
static scha_err_t set_default_rr_props(rgm_param_t **paramtable[]);

/*
 * The SYS_PROP_DESCRIPTIONS contains the descriptions of all
 * system-defined properties. The structure description_mappings
 * maps the properties with their descriptions.
 */
static struct
description_mappings sys_prop_descriptions[] = SYS_PROP_DESCRIPTIONS;


scha_errmsg_t
ccr_get_string(const ccr::readonly_table_var &tabptr, const char *key,
    char **data)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	Environment e;
	CORBA::Exception *ex;
	name_t value;

	*data = 0;
	value = tabptr->query_element(key, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex))
			res.err_code = SCHA_ERR_PROP;
		else if (ccr::table_modified::_exnarrow(ex))
			res.err_code = SCHA_ERR_SEQID;
		else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}
	*data = strdup(value);
	delete [] value;

	res.err_code = SCHA_ERR_NOERR;

	return (res);
}


scha_errmsg_t
ccr_get_int(const ccr::readonly_table_var &tabptr, const char *key, int *data)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	Environment e;
	CORBA::Exception *ex;
	name_t value;
	char *endp = NULL;

	value = tabptr->query_element(key, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex))
			res.err_code = SCHA_ERR_PROP;
		else if (ccr::table_modified::_exnarrow(ex))
			res.err_code = SCHA_ERR_SEQID;
		else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}

	*data = strtol(value, &endp, 0);
	if (*endp != '\0') {
		delete [] value;
		res.err_code = SCHA_ERR_INTERNAL;
		return (res);
	}

	delete [] value;

	res.err_code = SCHA_ERR_NOERR;

	return (res);
}


scha_errmsg_t
ccr_get_bool(const ccr::readonly_table_var &tabptr, const char *key,
    boolean_t *data)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	Environment e;
	CORBA::Exception *ex;
	name_t value;

	value = tabptr->query_element(key, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex))
			res.err_code = SCHA_ERR_PROP;
		else if (ccr::table_modified::_exnarrow(ex))
			res.err_code = SCHA_ERR_SEQID;
		else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}
	*data = str2bool(value);
	delete [] value;
	res.err_code = SCHA_ERR_NOERR;

	return (res);
}


scha_errmsg_t
ccr_get_nstring(const ccr::readonly_table_var &tabptr,
    const char *key, namelist_t **data)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	Environment e;
	CORBA::Exception *ex;
	name_t	value;

	*data = 0;
	value = tabptr->query_element(key, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex))
			res.err_code = SCHA_ERR_PROP;
		else if (ccr::table_modified::_exnarrow(ex))
			res.err_code = SCHA_ERR_SEQID;
		else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}
	// Since my_strtok() inside of str2nstr() will change ptr passed in,
	// save a copy for later free.
	*data = str2nstr(value);
	delete [] value;

	res.err_code = SCHA_ERR_NOERR;

	return (res);
}

scha_errmsg_t
ccr_get_nodeidlist(const ccr::readonly_table_var &tabptr,
    const char *key, nodeidlist_t **data)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	Environment e;
	CORBA::Exception *ex;
	char    *value;

	*data = 0;
	value = tabptr->query_element(key, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex))
			res.err_code = SCHA_ERR_PROP;
		else if (ccr::table_modified::_exnarrow(ex))
			res.err_code = SCHA_ERR_SEQID;
		else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}
	*data = str2nodeidlist(value);
	delete [] value;

	res.err_code = SCHA_ERR_NOERR;

	return (res);
}

scha_errmsg_t
ccr_get_nstring_all(const ccr::readonly_table_var &tabptr,
    const char *key, namelist_all_t *data)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	Environment e;
	CORBA::Exception *ex;
	name_t value;

	data->is_ALL_value = B_FALSE;
	data->names = NULL;
	value = tabptr->query_element(key, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex))
			res.err_code = SCHA_ERR_PROP;
		else if (ccr::table_modified::_exnarrow(ex))
			res.err_code = SCHA_ERR_SEQID;
		else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}

	if (strcmp(value, SCHA_ALL_SPECIAL_VALUE) == 0) {
		data->is_ALL_value = B_TRUE;
		data->names = NULL;
	} else {
		data->is_ALL_value = B_FALSE;
		data->names = str2nstr(value);
	}
	delete [] value;

	res.err_code = SCHA_ERR_NOERR;

	return (res);
}


scha_errmsg_t
ccr_get_nodeidlist_all(const ccr::readonly_table_var &tabptr,
    const char *key, nodeidlist_all_t *data)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	Environment e;
	CORBA::Exception *ex;
	char *value;

	data->is_ALL_value = B_FALSE;
	data->nodeids = NULL;
	value = tabptr->query_element(key, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex))
			res.err_code = SCHA_ERR_PROP;
		else if (ccr::table_modified::_exnarrow(ex))
			res.err_code = SCHA_ERR_SEQID;
		else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}

	if (strcmp(value, SCHA_ALL_SPECIAL_VALUE) == 0) {
		data->is_ALL_value = B_TRUE;
		data->nodeids = NULL;
	} else {
		data->is_ALL_value = B_FALSE;
		data->nodeids = str2nodeidlist(value);
	}
	delete [] value;

	res.err_code = SCHA_ERR_NOERR;

	return (res);
}


scha_prop_type_t
get_property_type(const rgm_rt_t *rt, const char *pname,
	boolean_t is_extension_prop)
{
	rgm_param_t **param;
	for (param = rt->rt_paramtable; *param; ++param) {
		if ((*param)->p_extension != is_extension_prop)
			continue;

		if (strcmp(pname, (*param)->p_name) == 0)
			return ((*param)->p_type);
	}

	return (SCHA_PTYPE_STRING);
}


char *
get_property_description(const rgm_rt_t *rt, const char *pname,
	boolean_t is_extension_prop)
{
	rgm_param_t **param;
	char *desc = NULL;

	for (param = rt->rt_paramtable; *param; ++param) {
		if ((*param)->p_extension != is_extension_prop)
			continue;

		if (strcmp(pname, (*param)->p_name) == 0) {
			if ((*param)->p_description != NULL) {
				desc = strdup((*param)->p_description);
				return (desc);
			}
		}
	}

	return (NULL);
}


/*
 * scan_kv is a utility for the parsing of CCR table entries.
 * NOTE: the input string s is destroyed during the parse.
 *
 * The input string s is assumed to contain a sequence of
 * enteries in the format or "%s%c" "%s=%s%c", with the first
 * string in the format being the key and the second, if present
 * a value.
 * The input string has '\0' written over the first "=" and ";"
 * in the input string to form key and value strings, and
 * these are returned in the out parameters.
 *
 * The value returned from scan_kv is
 * NULL if the input is at the end of the string,
 * or a pointer to the next character following the tokenized segment,
 * which presumably is the start of the next key or the end of the string.
 */
static char *
scan_kv(char *s, char **key, char **val, int separator)
{

	if (s == NULL || *s == '\0') {
		*key = NULL;
		*val = NULL;
		return (NULL);
	}
	*key = s;
	*val = NULL;
	while ((*s != '=') && (*s != separator) && (*s != '\0')) {
		s++;
	}
	if (*s == '\0') {
		return (s);
	}
	if (*s == separator) {
		*s = '\0';
		return (s+1);
	}
	*s = '\0';
	s++;
	*val = s;
	while ((*s != separator) && (*s != '\0')) {
		s++;
	}
	if (*s == '\0') {
		return (s);
	}
	*s = '\0';

	return (s+1);
}


scha_errmsg_t
ccr_get_resource(const ccr::readonly_table_var &tabptr, const char *key,
	rgm_resource_t **rsrc, const char *zc_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	Environment e;
	CORBA::Exception *ex;
	char *s, *original_s, *pn_key = NULL, *old_key = NULL,
	    *pn_nodeid = NULL;
	char	mod_key[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RS)];
	uint_t	pname_len;
	rgm_property_list_t *ptr, *curr = NULL;
	boolean_t is_value = B_FALSE;

	// Flags are for the client affinity properties:
	//	Affinity_timeout, UDP_affinity, Weak_affinity
	// These properties do not exist in SC30.  If the resource
	// is created in sc30, it doesn't have these properties in
	// its system-defined property list.  For this case, we
	// add them with default value in the list if the resource
	// is scalable.
	boolean_t	affinity_timeout_found = B_FALSE;
	boolean_t	udp_affinity_found = B_FALSE;
	boolean_t	weak_affinity_found = B_FALSE;
	boolean_t	generic_affinity_found = B_FALSE;
	boolean_t	is_scalable = B_FALSE;
	boolean_t	is_new_prop = B_FALSE;
	std::map<std::string, char *> value;

	//  Flags for Round Robin

	boolean_t	rr_enable = B_FALSE;
	boolean_t	conn_threshold_found = B_FALSE;

	// Since we prefixed RGM_PREFIX_RS to the resource name before
	// adding it to the CCR, we add it back to the resource name,
	// before retrieving it.
	// Note nothing else needs to be done to the resource "value"
	// retrieved.
	(void) sprintf(mod_key, "%s%s", RGM_PREFIX_RS, key);

	original_s = tabptr->query_element(mod_key, e);
	s = original_s;

	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex))
			res.err_code = SCHA_ERR_RSRC;
		else if (ccr::table_modified::_exnarrow(ex))
			res.err_code = SCHA_ERR_SEQID;
		else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}

	char *p, *k, *v;
	char *r;

	p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
	(void) my_strtok(&p, '=');
	(*rsrc)->r_type = strdup(p);

	// get the RT info to obtain the paramtable
	rgm_rt_t *rt;
	res = rgmcnfg_read_rttable((*rsrc)->r_type, &rt, zc_name);
	if (res.err_code != SCHA_ERR_NOERR) {
		delete [] original_s;
		return (res);
	}

	// Type_version property introduced in SC3.1.
	// For backward compatibility, determine whether this property
	// even appears in the CCR resource entry.  If not, set to
	// the RT_version value of resource's RT if it exists; otherwise
	// set it to "".
	pname_len = strlen(SCHA_TYPE_VERSION);
	if (strncmp(SCHA_TYPE_VERSION, s, pname_len) == 0 &&
	    *(s + pname_len) == '=') {
		p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
		(void) my_strtok(&p, '=');
		(*rsrc)->r_type_version = strdup(p);
	} else {
		// Type_version doesn't exist in CCR
		if (rt->rt_version != NULL)
			(*rsrc)->r_type_version = strdup(rt->rt_version);
		else
			(*rsrc)->r_type_version = strdup("");
	}

	p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
	(void) my_strtok(&p, RGM_VALUE_ASSIGN);
	(*rsrc)->r_description = strdup(p);

	p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
	(void) my_strtok(&p, RGM_VALUE_ASSIGN);

	while (p != NULL) {
		r = my_strtok(&p, RGM_VALUE_SEPARATOR);

		//
		// In CCR, onoff_switch and monitored_switch are
		// stored as <nodeid>,<nodeid:zonename>,... etc
		//
		if (r[0] != '\0') {
			((rgm_switch_t *)(*rsrc)->r_onoff_switch)->
			    r_switch_node[r] = SCHA_SWITCH_ENABLED;
		}
	}

	p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
	(void) my_strtok(&p, RGM_VALUE_ASSIGN);
	while (p != NULL) {
		r = my_strtok(&p, RGM_VALUE_SEPARATOR);

		if (r[0] != '\0') {
			((rgm_switch_t *)(*rsrc)->r_monitored_switch)->
			    r_switch_node[r] = SCHA_SWITCH_ENABLED;
		}
	}

	// Resource_project_name and Resource_dependency_restart properties
	// do not exist before sc31 release.
	// For backward compatibility, we check if these property names
	// appear in the certain position of ccr entry of resource.  If not,
	// we just assign r_project_name to an empty string and
	// dependencies.dp_restart to NULL, and contine parsing s for
	// the rest properties.

	// Check the next token and see if it is Resource_project_name
	pname_len = strlen(SCHA_RESOURCE_PROJECT_NAME);
	if (strncmp(SCHA_RESOURCE_PROJECT_NAME, s, pname_len) == 0 &&
	    *(s + pname_len) == '=') {
		p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
		(void) my_strtok(&p, '=');
		(*rsrc)->r_project_name = strdup(p);
	} else {
		// project name not there; set it to empty string
		(*rsrc)->r_project_name = strdup("");
	}

	// current property is resource_dependencies_strong
	p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
	(void) my_strtok(&p, '=');
	(*rsrc)->r_dependencies.dp_strong = str2rdepstr(p);

	p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
	(void) my_strtok(&p, '=');
	(*rsrc)->r_dependencies.dp_weak = str2rdepstr(p);

	// Check the next token and see if it is Resource_dependency_restart
	pname_len = strlen(SCHA_RESOURCE_DEPENDENCIES_RESTART);
	if (strncmp(SCHA_RESOURCE_DEPENDENCIES_RESTART, s, pname_len) == 0 &&
	    *(s + pname_len) == '=') {
		p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
		(void) my_strtok(&p, '=');
		(*rsrc)->r_dependencies.dp_restart = str2rdepstr(p);
	} else {
		// Resource dependency restart not there; set it to empty string
		(*rsrc)->r_dependencies.dp_restart = NULL;
	}
	//
	// Check the next token and see if it is
	//  Resource_dependency_offline_restart
	pname_len = strlen(SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART);
	if (strncmp(SCHA_RESOURCE_DEPENDENCIES_OFFLINE_RESTART, s,
	    pname_len) == 0 && *(s + pname_len) == '=') {
		p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
		(void) my_strtok(&p, '=');
		(*rsrc)->r_dependencies.dp_offline_restart = str2rdepstr(p);
	} else {
		//
		// Resource dependency offline restart not there;
		//  set it to empty string
		//
		(*rsrc)->r_dependencies.dp_offline_restart = NULL;
	}

	//
	// Check the next token and see if it is Inter Cluster Strong
	// dependents
	//
	pname_len = strlen(SCHA_IC_RESOURCE_DEPENDENTS);
	if (strncmp(SCHA_IC_RESOURCE_DEPENDENTS, s, pname_len) == 0 &&
	    *(s + pname_len) == '=') {
		p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
		(void) my_strtok(&p, '=');
		(*rsrc)->r_inter_cluster_dependents.dp_strong = str2rdepstr(p);
	} else {
		(*rsrc)->r_inter_cluster_dependents.dp_strong = NULL;
	}

	//
	// Check the next token and see if it is Inter Cluster weak
	// dependents
	//
	pname_len = strlen(SCHA_IC_RESOURCE_DEPENDENTS_WEAK);
	if (strncmp(SCHA_IC_RESOURCE_DEPENDENTS_WEAK, s, pname_len) == 0 &&
	    *(s + pname_len) == '=') {
		p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
		(void) my_strtok(&p, '=');
		(*rsrc)->r_inter_cluster_dependents.dp_weak = str2rdepstr(p);
	} else {
		(*rsrc)->r_inter_cluster_dependents.dp_weak = NULL;
	}


	//
	// Check the next token and see if it is Inter Cluster restart
	// dependents
	//
	pname_len = strlen(SCHA_IC_RESOURCE_DEPENDENTS_RESTART);
	if (strncmp(SCHA_IC_RESOURCE_DEPENDENTS_RESTART, s, pname_len) == 0 &&
	    *(s + pname_len) == '=') {
		p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
		(void) my_strtok(&p, '=');
		(*rsrc)->r_inter_cluster_dependents.dp_restart = str2rdepstr(p);
	} else {
		(*rsrc)->r_inter_cluster_dependents.dp_restart = NULL;
	}

	//
	// Check the next token and see if it is Inter Cluster offline
	// restart dependents
	//
	pname_len = strlen(SCHA_IC_RESOURCE_DEPENDENTS_OFFLINE_RESTART);
	if (strncmp(SCHA_IC_RESOURCE_DEPENDENTS_OFFLINE_RESTART, s,
	    pname_len) == 0 && *(s + pname_len) == '=') {
		p = (char *)my_strtok(&s, RGM_PROP_SEPARATOR);
		(void) my_strtok(&p, '=');
		(*rsrc)->r_inter_cluster_dependents.dp_offline_restart
		    = str2rdepstr(p);
	} else {
		(*rsrc)->r_inter_cluster_dependents.dp_offline_restart = NULL;
	}

	// Pre-defined properties list
	(*rsrc)->r_properties = 0;
	p = s;
	while ((p = scan_kv(p, &k, &v, RGM_PROP_SEPARATOR)) != NULL) {

		if (strcmp(k, SCHA_EXTENSION) == 0)
			break;

		ptr = (rgm_property_list_t *)
		    malloc_nocheck(sizeof (rgm_property_list_t));
		if (ptr == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}


		ptr->rpl_next = 0;

		ptr->rpl_property =
		    (rgm_property_t *)malloc_nocheck(sizeof (rgm_property_t));
		if (ptr->rpl_property == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}
		bzero((char *)ptr->rpl_property, sizeof (rgm_property_t));

		ptr->rpl_property->rp_value = new(nothrow) rgm_value_t;
		if (ptr->rpl_property->rp_value == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}
		ptr->rpl_property->rp_key = strdup(k);
		ptr->rpl_property->rp_type = get_property_type(rt, k, B_FALSE);

		if (ptr->rpl_property->rp_type == SCHA_PTYPE_STRINGARRAY) {
			ptr->rpl_property->rp_array_values = str2nstr(v);
		} else {
			if (v != NULL) {
				((rgm_value_t *)ptr->rpl_property->rp_value)->
				    rp_value[0] = strdup(v);
			} else
				((rgm_value_t *)ptr->rpl_property->rp_value)->
				    rp_value[0] = NULL;
			ptr->rpl_property->rp_array_values = NULL;
		}

		ptr->rpl_property->rp_description =
			get_property_description(rt, k, B_FALSE);

		if ((*rsrc)->r_properties == 0)
			(*rsrc)->r_properties = ptr;
		else
			curr->rpl_next = ptr;
		curr = ptr;

		if (strcasecmp(k, SCHA_SCALABLE) == 0)
			is_scalable = B_TRUE;
		else if (strcasecmp(k, SCHA_AFFINITY_TIMEOUT) == 0)
			affinity_timeout_found = B_TRUE;
		else if (strcasecmp(k, SCHA_UDP_AFFINITY) == 0)
			udp_affinity_found = B_TRUE;
		else if (strcasecmp(k, SCHA_WEAK_AFFINITY) == 0)
			weak_affinity_found = B_TRUE;
		else if (strcasecmp(k, SCHA_GENERIC_AFFINITY) == 0)
			generic_affinity_found = B_TRUE;
		else if (strcasecmp(k, SCHA_ROUND_ROBIN) == 0)
			rr_enable = B_TRUE;
		else if (strcasecmp(k, SCHA_CONN_THRESHOLD) == 0)
			conn_threshold_found = B_TRUE;
	}

	// if resource is scalable and the client affinity properties are
	// missing, add them into the system-defined property list.
	if (is_scalable) {
		if (curr == NULL)
			abort();

		// Affinity_timeout: default is 0 and type is INT
		if (!affinity_timeout_found) {
			if ((ptr = (rgm_property_list_t *)malloc_nocheck(
			    sizeof (rgm_property_list_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			ptr->rpl_next = NULL;
			curr->rpl_next = ptr;
			curr = ptr;

			if ((ptr->rpl_property = (rgm_property_t *)
			    malloc_nocheck(sizeof (rgm_property_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			bzero((char *)ptr->rpl_property,
			    sizeof (rgm_property_t));
			ptr->rpl_property->rp_key =
			    strdup(SCHA_AFFINITY_TIMEOUT);

			//
			// Initialize the rp_value structure. Typecast
			// the void * to rgm_value_t *
			//
			ptr->rpl_property->rp_value = new(nothrow) rgm_value_t;
			if (ptr->rpl_property->rp_value == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			((rgm_value_t *)ptr->rpl_property->rp_value)->
			    rp_value[0] = strdup("0");
			ptr->rpl_property->rp_type = SCHA_PTYPE_INT;
		}

		// UDP_Affinity: default is FALSE and type is BOOLEAN
		if (!udp_affinity_found) {
			if ((ptr = (rgm_property_list_t *)malloc_nocheck(
			    sizeof (rgm_property_list_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			ptr->rpl_next = NULL;
			curr->rpl_next = ptr;
			curr = ptr;

			if ((ptr->rpl_property = (rgm_property_t *)
			    malloc_nocheck(sizeof (rgm_property_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			bzero((char *)ptr->rpl_property,
			    sizeof (rgm_property_t));

			ptr->rpl_property->rp_value = new(nothrow) rgm_value_t;
			if (ptr->rpl_property->rp_value == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			ptr->rpl_property->rp_key = strdup(SCHA_UDP_AFFINITY);

			((rgm_value_t *)ptr->rpl_property->rp_value)->
			    rp_value[0] = strdup("FALSE");
			ptr->rpl_property->rp_type = SCHA_PTYPE_BOOLEAN;
		}

		// WEAK_Affinity: default is FALSE and type is BOOLEAN
		if (!weak_affinity_found) {
			if ((ptr = (rgm_property_list_t *)malloc_nocheck(
			    sizeof (rgm_property_list_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			ptr->rpl_next = NULL;
			curr->rpl_next = ptr;
			curr = ptr;

			if ((ptr->rpl_property = (rgm_property_t *)
			    malloc_nocheck(sizeof (rgm_property_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			bzero((char *)ptr->rpl_property,
			    sizeof (rgm_property_t));

			ptr->rpl_property->rp_value = new(nothrow) rgm_value_t;
			if (ptr->rpl_property->rp_value == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			ptr->rpl_property->rp_key = strdup(SCHA_WEAK_AFFINITY);

			((rgm_value_t *)ptr->rpl_property->rp_value)->
			    rp_value[0] = strdup("FALSE");
			ptr->rpl_property->rp_type = SCHA_PTYPE_BOOLEAN;
		}

		if (!generic_affinity_found) {
			if ((ptr = (rgm_property_list_t *)malloc_nocheck(
			    sizeof (rgm_property_list_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			ptr->rpl_next = NULL;
			curr->rpl_next = ptr;
			curr = ptr;

			if ((ptr->rpl_property = (rgm_property_t *)
			    malloc_nocheck(sizeof (rgm_property_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			bzero((char *)ptr->rpl_property,
			    sizeof (rgm_property_t));

			ptr->rpl_property->rp_value = new(nothrow) rgm_value_t;
			if (ptr->rpl_property->rp_value == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			ptr->rpl_property->rp_key =
			    strdup(SCHA_GENERIC_AFFINITY);

			((rgm_value_t *)ptr->rpl_property->rp_value)->
			    rp_value[0] = strdup("FALSE");
			ptr->rpl_property->rp_type = SCHA_PTYPE_BOOLEAN;
		}

		// RR_Enable: default is FALSE and type is BOOLEAN
		if (!rr_enable) {
			if ((ptr = (rgm_property_list_t *)malloc_nocheck(
			    sizeof (rgm_property_list_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			ptr->rpl_next = NULL;
			curr->rpl_next = ptr;
			curr = ptr;

			if ((ptr->rpl_property = (rgm_property_t *)
			    malloc_nocheck(sizeof (rgm_property_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			bzero((char *)ptr->rpl_property,
			    sizeof (rgm_property_t));
			ptr->rpl_property->rp_value = new(nothrow) rgm_value_t;
			if (ptr->rpl_property->rp_value == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}
			ptr->rpl_property->rp_key = strdup(SCHA_ROUND_ROBIN);
			((rgm_value_t *)ptr->rpl_property->rp_value)->
			    rp_value[0] = strdup("FALSE");
			ptr->rpl_property->rp_type = SCHA_PTYPE_BOOLEAN;
		}
		// Connection Threshold: default is 100 and type is INT
		if (!conn_threshold_found) {
			if ((ptr = (rgm_property_list_t *)malloc_nocheck(
			    sizeof (rgm_property_list_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			ptr->rpl_next = NULL;
			curr->rpl_next = ptr;
			curr = ptr;

			if ((ptr->rpl_property = (rgm_property_t *)
			    malloc_nocheck(sizeof (rgm_property_t))) == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			bzero((char *)ptr->rpl_property,
			    sizeof (rgm_property_t));
			ptr->rpl_property->rp_value = new(nothrow) rgm_value_t;
			if (ptr->rpl_property->rp_value == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}
			ptr->rpl_property->rp_key =
			    strdup(SCHA_CONN_THRESHOLD);
			((rgm_value_t *)ptr->rpl_property->rp_value)->
			    rp_value[0] = strdup("100");
			ptr->rpl_property->rp_type = SCHA_PTYPE_INT;
		}
	}

	// extension properties list
	(*rsrc)->r_ext_properties = 0;
	curr = 0;
	old_key = NULL;
	while ((p = scan_kv(p, &k, &v, RGM_PROP_SEPARATOR)) != NULL) {
		//
		// Per node extension property is stored in the form
		// <prop_name>{<nodeid>}=<val>;<prop_name2>{<nodeid2>}=<val2>;..
		//

		pn_key = my_strtok(&k, RGM_PN_SEPARATOR_PRE);

		/*
		 * In CCR, per-node properties are stored together as
		 * <prop_name>{<node1>}=<val1>;<prop_name>{<node2>}=<val2>;...
		 * is_new_prop variable below signifies that all
		 * the per-node values for a particular property
		 * has been amassed. The old_key value keeps track
		 * of the previously retrieved property name from
		 * the CCR. The pn_key tracks the current retrieved property
		 * name from CCR. In CCR, the property per-node property
		 * values are stored sequentially. Thus, if the old key is
		 * different from pn_key,it implies that we have a
		 * different property and the values pertaining to the
		 * current per-node property have all been processed.
		 */
		if ((old_key == NULL) && (pn_key != NULL))
			is_new_prop = B_TRUE;
		else if ((old_key != NULL) && (pn_key == NULL))
			is_new_prop = B_TRUE;
		else if ((old_key == NULL) && (pn_key == NULL))
			is_new_prop = B_FALSE;
		else if (strcmp(old_key, pn_key) == 0)
			is_new_prop = B_FALSE;
		else is_new_prop = B_TRUE;

		/*
		 * is_value will be true iff the previous property which we
		 * finished scanning was a per-node property.
		 */
		if (is_new_prop && is_value)  {
			/*
			 * We have got a new property. Since the per-node
			 * property values are always stored contiguously in
			 * the CCR, we have amassed all the values for
			 * the property old_key. We now create a property
			 * strucuture for the same.
			 */
			ptr = (rgm_property_list_t *)malloc_nocheck(
			    sizeof (rgm_property_list_t));
			if (ptr == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}
			ptr->rpl_next = NULL;

			ptr->rpl_property =
			    (rgm_property_t *)malloc_nocheck(
			    sizeof (rgm_property_t));
			if (ptr->rpl_property == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}
			bzero((char *)ptr->rpl_property,
			    sizeof (rgm_property_t));

			ptr->rpl_property->rp_value = new(nothrow) rgm_value_t;
			if (ptr->rpl_property->rp_value == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}

			ptr->rpl_property->is_per_node = B_TRUE;
			ptr->rpl_property->rp_key = strdup(old_key);
			ptr->rpl_property->rp_type = get_property_type(rt,
			    old_key, B_TRUE);
			((rgm_value_t *)ptr->rpl_property->rp_value)->
			    rp_value_node = value;
			is_value = B_FALSE;
			value.clear();
			ptr->rpl_property->rp_description =
			    get_property_description(rt, old_key, B_TRUE);

			if ((*rsrc)->r_ext_properties == 0)
				(*rsrc)->r_ext_properties = ptr;
			else
				curr->rpl_next = ptr;
			curr = ptr;
		}

		if (k != NULL) {
			//
			// The property being retrieved is a per-node
			// extension property. The per-node extension
			// property in CCR is presumed to be consistent.
			// Hence, there has to be a "}"
			// after "{".
			//
			pn_nodeid = my_strtok(&k, RGM_PN_SEPARATOR_POST);
			if (v != NULL)
				value[pn_nodeid] = strdup(v);
			else
				value[pn_nodeid] = NULL;
			is_value = B_TRUE;
			if (*p == '\0') {
				/*
				 * This is the last property in the CCR.
				 * Create a property structure
				 * for the last property pn_key
				 */
				ptr = (rgm_property_list_t *)malloc_nocheck(
				    sizeof (rgm_property_list_t));
				if (ptr == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto finished; //lint !e801
				}
				ptr->rpl_next = 0;

				ptr->rpl_property =
				    (rgm_property_t *)malloc_nocheck(
				    sizeof (rgm_property_t));
				if (ptr->rpl_property == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto finished; //lint !e801
				}
				bzero((char *)ptr->rpl_property,
				    sizeof (rgm_property_t));

				ptr->rpl_property->rp_value =
				    new(nothrow) rgm_value_t;
				if (ptr->rpl_property->rp_value == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto finished; //lint !e801
				}
				ptr->rpl_property->is_per_node = B_TRUE;
				ptr->rpl_property->rp_key = strdup(pn_key);
				ptr->rpl_property->rp_type = get_property_type(
				    rt, pn_key, B_TRUE);
				((rgm_value_t *)ptr->rpl_property->rp_value)->
				    rp_value_node = value;
				value.clear();
				ptr->rpl_property->rp_description =
				    get_property_description(rt, pn_key,
				    B_TRUE);

				if ((*rsrc)->r_ext_properties == 0)
					(*rsrc)->r_ext_properties = ptr;
				else
					curr->rpl_next = ptr;
				curr = ptr;
			}

			old_key = pn_key;
		} else {

			ptr = (rgm_property_list_t *)
			malloc_nocheck(sizeof (rgm_property_list_t));
			if (ptr == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}
			ptr->rpl_next = NULL;
			ptr->rpl_property =
			(rgm_property_t *)malloc_nocheck
			    (sizeof (rgm_property_t));
			if (ptr->rpl_property == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}
			bzero((char *)ptr->rpl_property,
			    sizeof (rgm_property_t));
			ptr->rpl_property->rp_value = new(nothrow)
			    rgm_value_t;
			if (ptr->rpl_property->rp_value == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto finished; //lint !e801
			}
			ptr->rpl_property->is_per_node = B_FALSE;
			ptr->rpl_property->rp_key = strdup(pn_key);

			ptr->rpl_property->rp_type = get_property_type(rt,
			    pn_key, B_TRUE);

			if (ptr->rpl_property->rp_type ==
			    SCHA_PTYPE_STRINGARRAY) {
				ptr->rpl_property->rp_array_values =
				    str2nstr(v);
			} else {
				if (v != NULL)
					((rgm_value_t *)ptr->rpl_property
					->rp_value)->rp_value[0] = strdup(v);
				ptr->rpl_property->rp_array_values = NULL;
			}
			ptr->rpl_property->rp_description =
				get_property_description(rt,
				    pn_key, B_TRUE);

			if ((*rsrc)->r_ext_properties == 0)
				(*rsrc)->r_ext_properties = ptr;
			else
				curr->rpl_next = ptr;
			curr = ptr;
		}
		res.err_code = SCHA_ERR_NOERR;
	}

finished:
	rgm_free_rt(rt);
	delete [] original_s;

	return (res);
} //lint !e818

static rgm_param_t *
new_param()
{
	rgm_param_t *p = (rgm_param_t *)malloc(sizeof (rgm_param_t));

	if (p == NULL)
		return (NULL);

	bzero((char *)p, sizeof (rgm_param_t));

	p->p_name = NULL;
	p->p_extension = B_FALSE;
	p->p_per_node = B_FALSE;
	p->p_tunable = TUNE_NONE;
	p->p_type = SCHA_PTYPE_STRING;
	p->p_defaultint = 0;
	p->p_defaultbool = B_FALSE;
	p->p_defaultstr = NULL;
	p->p_defaultarray = NULL;
	p->p_min = 0;
	p->p_max = 0;
	p->p_arraymin = 0;
	p->p_arraymax = 0;
	p->p_enumlist = NULL;
	p->p_description = NULL;
	p->p_default_isset = B_FALSE;
	p->p_min_isset = B_FALSE;
	p->p_arraymin_isset = B_FALSE;
	p->p_arraymax_isset = B_FALSE;

	return (p);
}



//
// ccr_get_rt_upgrade -
//	This function first checks if RT_UPGRADE flag is in CCR. If it isn't,
//	that means this RT is created from a sc30 version of rtr file.
//	SC30 rt doesn't have upgrade_from or downgrade_to stuff.  In
//	this case, just return SCHA_ERR_OK.  Otherwise, this function
//	reads ccr entry sequentially and looks for Upgrade_from
//	and Downgrade_to.
//	It returns SCHA_ERR_OK if those entries do not appear in the ccr,
//	since those entries are optional.
//
scha_errmsg_t
ccr_get_rt_upgrade(const ccr::readonly_table_var &tabptr, boolean_t *sc30_rt,
    rgm_rt_upgrade_t **upgrade_from_list, rgm_rt_upgrade_t **downgrade_to_list)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	Environment e;
	CORBA::Exception *ex;
	ccr::table_element *element = NULL;
	rgm_rt_upgrade_t *curr_up = NULL, *curr_down = NULL;
	rgm_rt_upgrade_t *upg_p;
	boolean_t	upgrade_from_found = B_FALSE;
	const char	*tag;
	char	buff[MAXRGMOBJNAMELEN];
	name_t junk;

	*sc30_rt = B_TRUE;
	*upgrade_from_list = NULL;
	*downgrade_to_list = NULL;

	junk = tabptr->query_element(SCHA_UPGRADE_HEADER, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {
			// Upgrade header is not in CCR.
			// This RT was created from a sc30 rtr file.
			// There is no upgrade_from or downgrade_to stuff.
			// Just return.
			e.clear();
			return (res);
		}

		if (ccr::table_modified::_exnarrow(ex))
			res.err_code = SCHA_ERR_SEQID;
		else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}

	delete [] junk;

	// Upgrade header exists in CCR, this rt is created
	// from a sc31 or post-sc31 rtr file.
	*sc30_rt = B_FALSE;

	tabptr->atfirst(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex))
			res.err_code = SCHA_ERR_SEQID;
		else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}

	//
	// tabptr->next_element() returns ccr::NoMoreElements exception if
	// there is no element to be returned from the table
	//
	for (;;) {
		tabptr->next_element(element, e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::NoMoreElements::_exnarrow(ex)) {
				// End of the table
				e.clear();
				break;
			} else {
				if (ccr::table_modified::_exnarrow(ex)) {
					res.err_code = SCHA_ERR_SEQID;
				} else {
					res.err_code = SCHA_ERR_CCR;
				}
				e.clear();
				goto finished; //lint !e801
			}
		}

		// we got an element and there was no exception

		upgrade_from_found = B_FALSE;

		if (strncmp((char *)element->key, SCHA_UPGRADE_FROM,
		    strlen(SCHA_UPGRADE_FROM)) == 0) {
			upgrade_from_found = B_TRUE;
			tag = SCHA_UPGRADE_FROM;
		} else if (strncmp((char *)element->key, SCHA_DOWNGRADE_TO,
		    strlen(SCHA_DOWNGRADE_TO)) == 0) {
			tag = SCHA_DOWNGRADE_TO;
		} else {
			// skip this ccr entry
			delete element;		// re-use element
			element = NULL;
			continue;
		}

		upg_p = (rgm_rt_upgrade_t *)malloc(sizeof (rgm_rt_upgrade_t));
		if (upg_p == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}

		// The key for CCR entry should be either
		// "Upgrade_from.<rt_version>" or "Downgrade_to.<rt_version>".
		// If <rt_version> is NULL, set rtu_version to an empty string.
		if (strlen((char *)element->key) == strlen(tag) + 1)
			upg_p->rtu_version = strdup_nocheck("");
		else {
			if (upgrade_from_found)
				(void) sscanf((char *)element->key,
				    SCHA_UPGRADE_FROM".%s", buff);
			else
				(void) sscanf((char *)element->key,
				    SCHA_DOWNGRADE_TO".%s", buff);
			upg_p->rtu_version = strdup_nocheck(buff);
		}

		if (upg_p->rtu_version == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto finished; //lint !e801
		}

		upg_p->rtu_tunability = param_tune((char *)element->data);
		upg_p->rtu_next = NULL;

		if (upgrade_from_found) {
			if (*upgrade_from_list == NULL)
				*upgrade_from_list = upg_p;
			else
				curr_up->rtu_next = upg_p;
			curr_up = upg_p;
		} else {
			if (*downgrade_to_list == NULL)
				*downgrade_to_list = upg_p;
			else
				curr_down->rtu_next = upg_p;
			curr_down = upg_p;
		}

		delete element;
		element = NULL;
	}

finished:

	if (element != NULL)
		delete element;

	if (res.err_code != SCHA_ERR_NOERR) {
		if (*upgrade_from_list)
			rgm_free_upglist(*upgrade_from_list);
		if (*downgrade_to_list)
			rgm_free_upglist(*downgrade_to_list);
	}

	return (res);
}


scha_errmsg_t
ccr_get_paramtable(const ccr::readonly_table_var &tabptr,
	rgm_param_t **paramtable[])
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	Environment e;
	CORBA::Exception *ex;
	ccr::table_element *element = NULL;
	rgm_param_t **params_ptr = NULL;
	rgm_param_t *param = NULL;
	boolean_t is_extension, is_pn_extension;
	boolean_t is_Scalable_in_rtr = B_FALSE;
	uint_t i = 0;
	char	*endp;

	tabptr->atfirst(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex))
			res.err_code = SCHA_ERR_SEQID;
		else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}

	*paramtable = 0;

	//
	// tabptr->next_element() returns ccr::NoMoreElements exception if
	// there is no element to be returned from the table
	//
	for (;;) {
		tabptr->next_element(element, e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::NoMoreElements::_exnarrow(ex)) {
				// End of the table
				e.clear();
				break;
			} else {
				rgm_free_paramtable(*paramtable);
				if (ccr::table_modified::_exnarrow(ex)) {
					res.err_code = SCHA_ERR_SEQID;
				} else {
					res.err_code = SCHA_ERR_CCR;
				}
				e.clear();
				return (res);
			}
		}

		// we got an element and there was no exception

		if (strncmp((char *)element->key, RGM_PREFIX_PROP,
		    strlen(RGM_PREFIX_PROP)) == 0) {
			// found a pre-defined property definition
			is_extension = B_FALSE;
			is_pn_extension = B_FALSE;
		} else if (strncmp((char *)element->key, RGM_PREFIX_PN_EXT,
		    strlen(RGM_PREFIX_PN_EXT)) == 0) {
			// found a per-node extension property
			is_pn_extension = B_TRUE;
			is_extension = B_TRUE;
		} else if (strncmp((char *)element->key, RGM_PREFIX_EXT,
		    strlen(RGM_PREFIX_EXT)) == 0) {
			// found an extension property definition
			is_extension = B_TRUE;
			is_pn_extension = B_FALSE;
		} else {
			delete element;
			element = NULL;
			continue;
		}

		params_ptr = (rgm_param_t **)
		    realloc(*paramtable, (i + 2) * (sizeof (rgm_param_t *)));
		if (params_ptr == NULL) {
			rgm_free_paramtable(*paramtable);
			res.err_code = SCHA_ERR_NOMEM;
			delete element;
			return (res);
		}

		//
		// Upon early exit from this routine, rgm_free_paramtable()
		// frees the memory allocated by realloc(), above, and
		// new_param(), below.
		//
		param = new_param();
		*paramtable = params_ptr;
		(*paramtable)[i] = param;
		(*paramtable)[i + 1] = (rgm_param_t *)0;

		param->p_extension = is_extension;
		param->p_per_node = is_pn_extension;
		if (is_extension == B_FALSE) {
			param->p_name =
			    strdup((char *)element->key
			    + strlen(RGM_PREFIX_PROP));
			//
			// If property Scalable is declared in RTR file,
			// we need to make sure all other scalable properties
			// are also available in paramtable.
			//
			if (strcasecmp(param->p_name, SCHA_SCALABLE) == 0)
				is_Scalable_in_rtr = B_TRUE;
		} else {
			if (is_pn_extension == B_TRUE) {
				param->p_name = strdup((char *)
				    element->key + strlen(RGM_PREFIX_PN_EXT));
			} else
				param->p_name =
				    strdup((char *)element->key
				    + strlen(RGM_PREFIX_EXT));
		}

		/*
		 * Copy the element data into a scratch string
		 * which is going to be written into as it is parsed.
		 */
		char *s = strdup((char *)element->data);

		char *default_value = NULL;
		char *p, *k, *v;

		p = s;
		while ((p = scan_kv(p, &k, &v, RGM_PROP_SEPARATOR)) != NULL) {
			if (k == NULL)
				continue;
			if (strcmp(k, SCHA_TYPE) == 0) {
				param->p_type = param_type(v);
			} else if (strcmp(k, SCHA_TUNABLE) == 0) {
				param->p_tunable = param_tune(v);
			} if (strcmp(k, SCHA_DEFAULT) == 0) {
				param->p_default_isset = B_TRUE;
				default_value = v;
			} else if (strcmp(k, SCHA_MIN) == 0) {
				param->p_min = strtol(v, &endp, 0);
				if (*endp != '\0') {
					rgm_free_paramtable(*paramtable);
					res.err_code = SCHA_ERR_INTERNAL;
					delete element;
					free(s);
					return (res);
				}
				param->p_min_isset = B_TRUE;
			} else if (strcmp(k, SCHA_MAX) == 0) {
				param->p_max = strtol(v, &endp, 0);
				if (*endp != '\0') {
					rgm_free_paramtable(*paramtable);
					res.err_code = SCHA_ERR_INTERNAL;
					delete element;
					free(s);
					return (res);
				}
				param->p_max_isset = B_TRUE;
			} else if (strcmp(k, SCHA_ARRAY_MINSIZE) == 0) {
				param->p_arraymin = strtol(v, &endp, 0);
				if (*endp != '\0') {
					rgm_free_paramtable(*paramtable);
					res.err_code = SCHA_ERR_INTERNAL;
					delete element;
					free(s);
					return (res);
				}
				param->p_arraymin_isset = B_TRUE;
			} else if (strcmp(k, SCHA_ARRAY_MAXSIZE) == 0) {
				param->p_arraymax = strtol(v, &endp, 0);
				if (*endp != '\0') {
					rgm_free_paramtable(*paramtable);
					res.err_code = SCHA_ERR_INTERNAL;
					delete element;
					free(s);
					return (res);
				}
				param->p_arraymax_isset = B_TRUE;
			} else if (strcmp(k, SCHA_ENUMLIST) == 0) {
				param->p_enumlist = str2nstr(v);
			} else if (strcmp(k, SCHA_DESCRIPTION) == 0) {
				// Description is contain anything
				// between two double quotes.
				if (strlen(v) != 2) {
					// get rid of double quotes at front.
					char *d = strdup(v+1);
					// get rid of double quote at end.
					*(d + strlen(d) - 1) = '\0';
					param->p_description = d;
				}
			} else {
				/* Ignore unknown key */
				continue;
			}
		}

		/*
		 * If the property is system-defined, then the corresponding
		 * description is assigned to it.
		 */
		if (param->p_extension == B_FALSE) {
			for (uint_t j = 0; j < NUM_SYS_PARAMS; j++) {
				if (strcmp(sys_prop_descriptions[j].name,
				    param->p_name) == 0)
					param->p_description = strdup(
					    sys_prop_descriptions[j].desc);
			}
		}

		if (default_value != NULL) {
			switch (param->p_type) {
			case SCHA_PTYPE_INT:
			case SCHA_PTYPE_UINT:
				param->p_defaultint =
				    strtol(default_value, &endp, 0);
				if (*endp != '\0') {
					rgm_free_paramtable(*paramtable);
					res.err_code = SCHA_ERR_INTERNAL;
					delete element;
					free(s);
					return (res);
				}
				break;
			case SCHA_PTYPE_BOOLEAN:
				param->p_defaultbool = str2bool(default_value);
				break;
			case SCHA_PTYPE_STRING:
			case SCHA_PTYPE_ENUM:
				param->p_defaultstr = strdup(default_value);
				break;
			case SCHA_PTYPE_STRINGARRAY:
			case SCHA_PTYPE_UINTARRAY:
				param->p_defaultarray = str2nstr(default_value);
				break;
			}
		}

		i++;
		free(s);
		delete element;
		element = NULL;
	}

	res.err_code = SCHA_ERR_NOERR;
	delete element;

	if (is_Scalable_in_rtr) {
		res.err_code = set_default_client_aff_props(paramtable);
		if (res.err_code == SCHA_ERR_NOERR) {
			res.err_code = set_default_rr_props(paramtable);
		}

	}

	return (res);
}


//
// set_default_client_aff_props() -
//	The function is called by ccr_get_paramtable() after parsing
//	the RTR file.  Properties Affinity_timeout, UDP_affinity and
//	Weak_affinity are newly added in SC3.1.  If a scalable RT that
//	is registered in SC3.0 cluster, these new properties do not
//	exist in ccr table of the RT.  For backward compatibility,
//	we add these missing properties with default values in to the
//	in-memory paramtable for resource creation.
// 	Added Generic_Affinity in SC3.2.
//
scha_err_t
set_default_client_aff_props(rgm_param_t **paramtable[])
{

	boolean_t	affinity_timeout_found = B_FALSE;
	boolean_t	udp_affinity_found = B_FALSE;
	boolean_t	weak_affinity_found = B_FALSE;
	boolean_t	generic_affinity_found = B_FALSE;
	uint_t		num_props_curr = 0;
	uint_t		num_new_props = 4;

	rgm_param_t	**ptr;
	rgm_param_t	*param;

	// First count the number of properties in paramtable in RTR.
	// Also check if the properties for client affinity exist in
	// paramtable.
	for (ptr = *paramtable; *ptr; ptr++) {
		num_props_curr++;

		if (strcmp((*ptr)->p_name, SCHA_AFFINITY_TIMEOUT) == 0) {
			affinity_timeout_found = B_TRUE;
			num_new_props--;
		} else if (strcmp((*ptr)->p_name, SCHA_UDP_AFFINITY) == 0) {
			udp_affinity_found = B_TRUE;
			num_new_props--;
		} else if (strcmp((*ptr)->p_name, SCHA_WEAK_AFFINITY) == 0) {
			weak_affinity_found = B_TRUE;
			num_new_props--;
		} else if (strcmp((*ptr)->p_name, SCHA_GENERIC_AFFINITY) == 0) {
			generic_affinity_found = B_TRUE;
			num_new_props--;
		}
	}

	if (num_new_props == 0) {
		// All client affinity properties are already in paramtable
		return (SCHA_ERR_NOERR);
	}

	// Add the missing client affinity properties into paramtable
	ptr = (rgm_param_t **)realloc(*paramtable,
	    (num_props_curr + num_new_props + 1) * (sizeof (rgm_param_t *)));
	if (ptr == NULL)
		goto return_err; //lint !e801

	*paramtable = ptr;

	// Affinity_timeout: default is 0 and tunable is ANYTIME
	if (!affinity_timeout_found) {
		if ((param = new_param()) == NULL)
			goto return_err; //lint !e801

		(*paramtable)[num_props_curr++] = param;

		param->p_name = strdup(SCHA_AFFINITY_TIMEOUT);
		param->p_type = SCHA_PTYPE_INT;
		param->p_tunable = TUNE_ANYTIME;
		param->p_defaultint = 0;
		param->p_default_isset = B_TRUE;
		param->p_min = 0;
		param->p_min_isset = B_TRUE;
		param->p_description = strdup(sys_prop_descriptions[18].desc);
	}

	// For UDP_Affinity: default is FALSE and tunable is WHEN_DISABLED
	if (!udp_affinity_found) {
		if ((param = new_param()) == NULL)
			goto return_err; //lint !e801

		(*paramtable)[num_props_curr++] = param;

		param->p_name = strdup(SCHA_UDP_AFFINITY);
		param->p_type = SCHA_PTYPE_BOOLEAN;
		param->p_tunable = TUNE_WHEN_DISABLED;
		param->p_defaultbool = B_FALSE;
		param->p_default_isset = B_TRUE;
		param->p_description = strdup(sys_prop_descriptions[19].desc);
	}

	// For Weak_Affinity: default is FALSE and tunable is WHEN_DISABLED
	if (!weak_affinity_found) {
		if ((param = new_param()) == NULL)
			goto return_err; //lint !e801

		(*paramtable)[num_props_curr++] = param;

		param->p_name = strdup(SCHA_WEAK_AFFINITY);
		param->p_type = SCHA_PTYPE_BOOLEAN;
		param->p_tunable = TUNE_WHEN_DISABLED;
		param->p_defaultbool = B_FALSE;
		param->p_default_isset = B_TRUE;
		param->p_description = strdup(sys_prop_descriptions[20].desc);
	}

	// For Generic_Affinity: default is FALSE and tunable is WHEN_DISABLED
	if (!generic_affinity_found) {
		if ((param = new_param()) == NULL)
			goto return_err; //lint !e801

		(*paramtable)[num_props_curr++] = param;

		param->p_name = strdup(SCHA_GENERIC_AFFINITY);
		param->p_type = SCHA_PTYPE_BOOLEAN;
		param->p_tunable = TUNE_WHEN_DISABLED;
		param->p_defaultbool = B_FALSE;
		param->p_default_isset = B_TRUE;
		param->p_description = strdup(sys_prop_descriptions[27].desc);
	}
	(*paramtable)[num_props_curr] = (rgm_param_t *)NULL;
	return (SCHA_ERR_NOERR);

return_err:

	rgm_free_paramtable(*paramtable);
	return (SCHA_ERR_NOMEM);
}

scha_err_t
set_default_rr_props(rgm_param_t **paramtable[])
{
	boolean_t	conn_threshold_found = B_FALSE;
	boolean_t	rr_enabled = B_FALSE;
	uint_t		num_props_curr = 0;
	uint_t		num_new_props = 2;

	rgm_param_t	**ptr;
	rgm_param_t	*param;

	// First count the number of properties in paramtable in RTR.
	// Also check if the properties for RR exist in paramtable.
	for (ptr = *paramtable; *ptr; ptr++) {
		num_props_curr++;

		if (strcmp((*ptr)->p_name, SCHA_ROUND_ROBIN) == 0) {
			rr_enabled = B_TRUE;
			num_new_props--;
		} else if (strcmp((*ptr)->p_name, SCHA_CONN_THRESHOLD) == 0) {
			conn_threshold_found = B_TRUE;
			num_new_props--;
		}
	}

	if (num_new_props == 0) {
		// All RR properties are already in paramtable
		return (SCHA_ERR_NOERR);
	}

	// Add the missing RR properties into paramtable
	ptr = (rgm_param_t **)realloc(*paramtable,
	    (num_props_curr + num_new_props + 1) * (sizeof (rgm_param_t *)));
	if (ptr == NULL)
		goto return_err; //lint !e801

	*paramtable = ptr;

	// RR : default is FALSE and tunable is At Disable
	if (!rr_enabled) {
		if ((param = new_param()) == NULL)
			goto return_err; //lint !e801

		(*paramtable)[num_props_curr++] = param;

		param->p_name = strdup(SCHA_ROUND_ROBIN);
		param->p_type = SCHA_PTYPE_BOOLEAN;
		param->p_defaultbool = B_FALSE;
		param->p_default_isset = B_TRUE;
		param->p_tunable = TUNE_WHEN_DISABLED;
		param->p_description = strdup(sys_prop_descriptions[25].desc);
	}

	// For Conn threshold: default is 100 and tunable is at Disable
	if (!conn_threshold_found) {
		if ((param = new_param()) == NULL)
			goto return_err; //lint !e801

		(*paramtable)[num_props_curr++] = param;

		param->p_name = strdup(SCHA_CONN_THRESHOLD);
		param->p_type = SCHA_PTYPE_INT;
		param->p_tunable = TUNE_WHEN_DISABLED;
		param->p_defaultint = 100;
		param->p_default_isset = B_TRUE;
		param->p_description = strdup(sys_prop_descriptions[26].desc);
	}

	(*paramtable)[num_props_curr] = (rgm_param_t *)NULL;
	return (SCHA_ERR_NOERR);

return_err:

	rgm_free_paramtable(*paramtable);

	return (SCHA_ERR_NOMEM);
}
