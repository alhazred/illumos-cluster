//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_ccrput.cc	1.22	08/05/20 SMI"


#include <h/ccr.h>
#include <rgm/rgm_common.h>
#include <rgm/rgm_private.h>
#include <rgm/scha_priv.h>

scha_errmsg_t
ccr_put_string(const ccr::updatable_table_var &transptr, const char *key,
    const char *value)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	CORBA::Exception *ex;

	if (value == NULL)
		transptr->add_element(key, "", e);
	else
		transptr->add_element(key, value, e);

	if ((ex = e.exception()) != NULL) {
		if (ccr::key_exists::_exnarrow(ex)) {
			// duplicate property
			res.err_code = SCHA_ERR_PROP;
		} else {
			// ccr add element fails
			res.err_code = SCHA_ERR_CCR;
		}
		e.clear();
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}

void
ccr_pack_string(ccr::element_seq &seq, const char *key, const char *value)
{
	unsigned int i = seq.length();

	seq.length(i + 1);
	seq[i].key = os::strdup(key);
	if (value == NULL) {
		seq[i].data = os::strdup("");
	} else {
		seq[i].data = os::strdup(value);
	}
}


scha_errmsg_t
ccr_put_int(const ccr::updatable_table_var &transptr, const char *key,
    int value)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	CORBA::Exception *ex;
	char *s = rgm_itoa(value);

	transptr->add_element(key, s, e);
	free(s);
	if ((ex = e.exception()) != NULL) {
		if (ccr::key_exists::_exnarrow(ex)) {
			// duplicate property
			res.err_code = SCHA_ERR_PROP;
		} else {
			// ccr add element fails
			res.err_code = SCHA_ERR_CCR;
		}
		e.clear();
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}

void
ccr_pack_int(ccr::element_seq &seq, const char *key, int value)
{
	unsigned int i = seq.length();

	seq.length(i + 1);
	seq[i].key = os::strdup(key);
	seq[i].data = rgm_itoa_cpp(value);
	//
	// memory allocated by rgm_itoa will be freed when the sequence
	// is deleted
	//
}

scha_errmsg_t
ccr_put_bool(const ccr::updatable_table_var &transptr,
    const char *key, boolean_t value)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	CORBA::Exception *ex;

	transptr->add_element(key, bool2str(value), e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::key_exists::_exnarrow(ex)) {
			// duplicate property
			res.err_code = SCHA_ERR_PROP;
		} else {
			// ccr add element fails
			res.err_code = SCHA_ERR_CCR;
		}
		e.clear();
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}


void
ccr_pack_bool(ccr::element_seq &seq, const char *key, boolean_t value)
{
	unsigned int i = seq.length();

	seq.length(i + 1);
	seq[i].key = os::strdup(key);
	seq[i].data = os::strdup(bool2str(value));
}

scha_errmsg_t
ccr_put_nstring(const ccr::updatable_table_var &transptr,
	const char *key, namelist_t *value)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	CORBA::Exception *ex;

	if (value == NULL)
		transptr->add_element(key, "", e);
	else {
		char *s = nstr2str(value);
		transptr->add_element(key, s, e);
		free(s);
	}

	if ((ex = e.exception()) != NULL) {
		if (ccr::key_exists::_exnarrow(ex)) {
			// duplicate property
			res.err_code = SCHA_ERR_PROP;
		} else {
			// ccr add element fails
			res.err_code = SCHA_ERR_CCR;
		}
		e.clear();
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}

void
ccr_pack_nstring(ccr::element_seq &seq, const char *key, namelist_t *value)
{
	unsigned int i = seq.length();

	seq.length(i + 1);
	seq[i].key = os::strdup(key);
	if (value == NULL) {
		seq[i].data = os::strdup("");
	} else {
		seq[i].data = nstr2str_cpp(value);
	}
}


void
ccr_pack_nodeidlist(ccr::element_seq &seq, const char *key, nodeidlist_t *value)
{
	unsigned int i = seq.length();

	seq.length(i + 1);
	seq[i].key = os::strdup(key);
	if (value == NULL) {
		seq[i].data = os::strdup("");
	} else {
		seq[i].data = nodeidlist2str_cpp(value);
	}
}


scha_errmsg_t
ccr_put_nstring_all(const ccr::updatable_table_var &transptr,
	const char *key, namelist_all_t *value)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	CORBA::Exception *ex;

	if (value == NULL)
		transptr->add_element(key, "", e);
	else {
		char *s = nstr_all2str(value);
		transptr->add_element(key, s, e);
		free(s);
	}

	if ((ex = e.exception()) != NULL) {
		if (ccr::key_exists::_exnarrow(ex)) {
			// duplicate property
			res.err_code = SCHA_ERR_PROP;
		} else {
			// ccr add element fails
			res.err_code = SCHA_ERR_CCR;
		}
		e.clear();
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
} //lint !e818

void
ccr_pack_nstring_all(ccr::element_seq &seq, const char *key,
    namelist_all_t *value)
{
	unsigned int i = seq.length();

	seq.length(i + 1);
	seq[i].key = os::strdup(key);
	if (value == NULL) {
		seq[i].data = os::strdup("");
	} else {
		seq[i].data = nstr_all2str_cpp(value);
	}
} //lint !e818

void
ccr_pack_nodeidlist_all(ccr::element_seq &seq,
    const char *key, nodeidlist_all_t *value)
{
	unsigned int i = seq.length();

	seq.length(i + 1);
	seq[i].key = os::strdup(key);
	if (value == NULL) {
		seq[i].data = os::strdup("");
	} else {
		seq[i].data = nodeidlist_all2str_cpp(value);
	}
} //lint !e818

void
ccr_pack_paramtable(ccr::element_seq &seq, rgm_param_t **paramtable)
{
	rgm_param_t **prop;
	char key[MAXNAMELEN];
	char *s = NULL;
	unsigned int i = seq.length();

	if (paramtable == NULL) {
		return;
	}

	for (prop = paramtable; *prop; ++prop) {
		if ((*prop)->p_per_node == B_TRUE) {
			(void) sprintf(key, "n.%s",
			    (*prop)->p_name);
		} else if ((*prop)->p_extension == B_TRUE) {
			(void) sprintf(key, "x.%s", (*prop)->p_name);
		} else {
			(void) sprintf(key, "p.%s", (*prop)->p_name);
		}

		s = params2str_cpp(*prop);

		seq.length(i + 1);
		seq[i].key = os::strdup(key);
		if (s == NULL) {
			seq[i].data = os::strdup("");
		} else {
			seq[i].data = s;
		}
		i++;
	}
}

scha_errmsg_t
ccr_put_resource_group(const ccr::updatable_table_var &transptr,
    const ccr::element_seq &seq)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;

	transptr->add_elements(seq, e);
	if ((e.exception()) != NULL) {
		// ccr add elements fails
		res.err_code = SCHA_ERR_CCR;
		e.clear();
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}
