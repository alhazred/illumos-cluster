//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// This file is used both to build the librgm library linked with the main
// RGMD daemon running on server nodes (standard Sun Cluster), and to build
// the ligthweight FRGMD daemon running on Europa farm nodes.
//
// Only a subset of the utility functions are needed for the frgmd daemon and
// therefore all the functions that are used only by the rgmd running on a
// server node will be #ifndef'ed with "EUROPA_FARM" compilation flag.
//
// List of common functions between rgmd and frgmd:
//
// + new_str
// + malloc_nocheck
// + realloc_err
// + rgm_rsrc_on_which_rg
// + rgm_error_msg
// + rgm_vsprintf
// + rgm_format_errmsg
// + rgm_sprintf
// + strdup_nocheck
// + rgm_find_property_in_list
//


#pragma ident	"@(#)rgm_common.cc	1.57	08/11/02 SMI"

#include <rgm/rgm_cmn_proto.h>
#ifdef EUROPA_FARM
#include <cmm/ucmm_api.h>
#endif
#include <rgm/rgm_cnfg.h>
#include <dlfcn.h>
#include <sys/sc_syslog_msg.h>
#include <nslib/ns.h>
#include <locale.h>
#include <pthread.h>
#include <rgm/scha_strings.h>
#include <orb/infrastructure/orb.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_util.h>
#include <clcomm_cluster_vm.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#include <sys/vc_int.h>
#else
#define	GLOBAL_ZONENAME	"global"
#endif

//
// Utility functions common to rgmd and its clients.
//
#ifndef EUROPA_FARM
/*
 * fetch_msg_locale
 * ------------
 * Returns the locale for the LC_MESSAGES variable.
 * Guarenteed to return a valid string.  If none is found by setlocale,
 * will return "C".
 *
 * Note that the string returned should be copied to new memory, because
 * setlocale can overwrite it next time it is called.
 */
const char *
fetch_msg_locale(void)
{
	const char *locale = (const char *)(setlocale(LC_MESSAGES, NULL));
	if (locale == NULL) {
		return ("C");
	}
	return (locale);
}
#endif /* EUROPA_FARM */

//
// just like strdup(), but with space allocated with 'new'
// and freeable with delete[].  We use this to make duplicates
// of strings when we don't want our original strings to later
// be destroyed by destructors of structures that 'delete' the strings.
// We use this instead of 'strdup' because we like to use 'new' with
// 'delete'.  'strdup' uses 'malloc'.  With 'malloc' we like to use 'free'.
//
char *
new_str(const char *str)
{
	char *ret;
	sc_syslog_msg_handle_t handle;

	// If str is NULL, just return NULL.
	if (str == NULL)
		return (NULL);

	ret = new char[strlen(str) + 1];	/*CSTYLED*/
	if (ret != NULL) {
		(void) strcpy(ret, str);
	} else {
		int myerrno = errno;
		char *s = strerror(myerrno);
		// new won't return NULL; it will raise exception.
		// We call set_new_handler() upon rgmd startup.
		// crash and burn
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd failed to allocate memory, most likely because the
		// system has run out of swap space. The rgmd will produce a
		// core file and will force the node to halt or reboot to
		// avoid the possibility of data corruption.
		// @user_action
		// The problem is probably cured by rebooting. If the problem
		// recurs, you might need to increase swap space by
		// configuring additional swap devices. See swap(1M) for more
		// information.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: new_str strcpy: %s (UNIX error %d)",
		    (s ? s : "unknown error number"), myerrno);
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}
	return (ret);
}

//
// malloc_direct
//
// This function simply calls malloc(). This way we allow
// the interposition of any kind of memory allocators.
//
// Note that this function will be used by all clients of
// librgm.so except rgmd.
//
void *
malloc_direct(size_t n)
{
	return (malloc(n));
}


//
// malloc_nocheck definition is moved to rgmd.
// Any calls to malloc_nocheck() ( other than from
// rgmd ) will now go to malloc_direct() due to
// the following pragma directive.
//
#pragma weak malloc_nocheck = malloc_direct

//
// Unlike malloc() and the functions calloc() and strdup() that call
// malloc(), realloc() does not exit the process on a memory allocation
// failure (unless ptr is NULL in which case malloc() is called by
// realloc()).
//
// This function provides the equivalent error checking. It calls
// realloc() and exits the process on error.
//
void *
realloc_err(void *ptr, size_t size)
{
	void	*ptr2;
	sc_syslog_msg_handle_t handle;

	if ((ptr2 = realloc(ptr, size)) == NULL) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		//
		// SCMSGS
		// @explanation
		// The rgmd failed to allocate memory, most likely because the
		// system has run out of swap space. The rgmd will produce a
		// core file and will force the node to halt or reboot to
		// avoid the possibility of data corruption.
		// @user_action
		// The problem was probably cured by rebooting. If the problem
		// recurs, you might need to increase swap space by
		// configuring additional swap devices. See swap(1M) for more
		// information.
		//
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    "fatal: realloc: %s (UNIX error %d)",
		    strerror(errno), errno);
		sc_syslog_msg_done(&handle);
		abort();
		// NOTREACHABLE
	}
	return (ptr2);
}


#ifndef EUROPA_FARM
//
// except_to_scha_err
//
// Map exceptions to scha error codes.
// We only handle COMM_FAILURE, INV_OBJREF, and zone_death
// (a user exception);
// we don't expect anything else and don't handle
// anything else.
//
// IMPORTANT: Keep this function in sync with except_to_errcode().
//
scha_errmsg_t
except_to_scha_err(Environment &e)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (e.exception() == NULL) {
		res.err_code = SCHA_ERR_NOERR;
		return (res);
	}

	if (CORBA::COMM_FAILURE::_exnarrow(e.exception()) ||	// node death
	    CORBA::INV_OBJREF::_exnarrow(e.exception()) ||	// rgmd death
	    rgm::zone_died::_exnarrow(e.exception())) {		// zone death
		res.err_code = SCHA_ERR_CLRECONF;
	} else {
		// unknown, unexpected
		res.err_code = SCHA_ERR_ORB;
	}

	e.clear();

	return (res);
}
#endif /* EUROPA_FARM */


/*
 * rgm_rsrc_on_which_rg() - find the RG that the given resource is configured.
 * rgname is the out parameter, comtaining the name of the RG which contains
 * the Resource rname. The memory for rgname is allocated by
 * rgm_rsrc_on_which_rg, so the caller doesn't have to. The caller however
 * is responsible for freeing it.
 */
scha_errmsg_t
rgm_rsrc_on_which_rg(char *rname, char **rgname, char *cluster)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	namelist_t *managed_rgs = NULL;
	namelist_t *unmanaged_rgs = NULL;
	// the boolean to record the fact that one of the CCR accesses failed
	// while trying to read the RG tables successively.
	boolean_t ccr_access_failed = B_FALSE;

	*rgname = NULL;
	if (rname == NULL) {
		return (res);
	}
	res = rgmcnfg_get_rglist(
	    &managed_rgs, &unmanaged_rgs, B_FALSE, cluster);

	if (res.err_code != SCHA_ERR_NOERR) {
		return (res);
	}

	namelist_t *np = managed_rgs;
	while (np != NULL) {
		res = rgmcnfg_rs_exist(rname, np->nl_name, cluster);
		// here res.err_code could be one of the 4 things:
		// 1) SCHA_ERR_NOERR : Resource found. Set the RG name and
		// return.
		// 2&3) SCHA_ERR_RG & SCHA_ERR_RSRC: The RG not found or the
		// Resource not found in the current RG. That's Ok, continue
		// looking at the next RG in the list. Better luck next time!
		// 4) SCHA_ERR_CCR: CCR open/read of the table of the current
		// RG failed. This is more complicated, because we can't be
		// sure if the R we are looking for was in that table itself,
		// or lies somewhere ahead. We note this fact (through
		// ccr_access_failed) and move on. If at the end of the list
		// we still don't find the R in the system, we return
		// SCHA_ERR_CCR to the calling routine, which, in this case,
		// means "don't know" or "maybe."
		if (res.err_code == SCHA_ERR_NOERR) {
			*rgname = strdup(np->nl_name);
			goto cleanup; //lint !e801
		} else if (res.err_code == SCHA_ERR_CCR)
			ccr_access_failed = B_TRUE;

		np = np->nl_next;
	}
	np = unmanaged_rgs;
	while (np != NULL) {
		res = rgmcnfg_rs_exist(rname, np->nl_name, cluster);
		// see the comment above for explanation of the res.err_code's
		// and their handling.
		if (res.err_code == SCHA_ERR_NOERR) {
			*rgname = strdup(np->nl_name);
			goto cleanup; //lint !e801
		} else if (res.err_code == SCHA_ERR_CCR)
			ccr_access_failed = B_TRUE;

		np = np->nl_next;
	}


	// if we got this far, it means we didn't find the R in any of the
	// RGs. This could mean two things:
	// 1) if the reads of all the RG tables succeeded
	// (i.e. ccr_access_failed == B_FALSE), then the R doesn't exist in
	// the system anymore.  In this case, reset  res.err_code to
	// SCHA_ERR_RSRC and bubble it to the calling routine.
	// 2) if the reads of one of the RG tables failed
	// (i.e. ccr_access_failed == B_TRUE), then we can't be certain
	// if the R exists in the system or not. In this case, reset
	// res.err_code to SCHA_ERR_CCR and bubble it to the calling routine.

	if (ccr_access_failed)
		res.err_code = SCHA_ERR_CCR;
	else
		res.err_code = SCHA_ERR_RSRC;

cleanup:
	rgm_free_nlist(managed_rgs);
	rgm_free_nlist(unmanaged_rgs);
	return (res);
}


#ifndef EUROPA_FARM
//
// rgm_orbinit
//
// ORB initialize
//
scha_errmsg_t
rgm_orbinit()
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (ORB::initialize() != 0) {
		res.err_code = SCHA_ERR_INTERNAL;
		return (res);
	}
	res.err_code = SCHA_ERR_NOERR;
	return (res);
}

// the return string needs to be freed.
char *
get_clustername_from_zone(const char *zone_name, scha_errmsg_t *err)
{
#if SOL_VERSION >= __s10
	char zonename[ZONENAME_MAX];
	scha_err_t errcode;
	uint_t clusterid;

	err->err_code = SCHA_ERR_NOERR;

	if (clconf_lib_init() != 0) {
		err->err_code = SCHA_ERR_INTERNAL;
		return (NULL);
	}

	if (zone_name == NULL) {
		// get the local zonename.
		(void) getzonenamebyid(getzoneid(), zonename, ZONENAME_MAX);
		zone_name = zonename;
	}
	// now zone contains some non nil string

	// GLOBAL ZONE
	if (strcmp(zone_name, GLOBAL_ZONENAME) == 0)
		return (strdup(zone_name));

	errcode = clconf_get_cluster_id((char *)zone_name, &clusterid);
	if (errcode == 0) {
		if (clusterid >= MIN_CLUSTER_ID) {
			// its a valid zone cluster
			return (strdup(zone_name));
		} else {
			// its a native non-global zone
			ASSERT(clusterid == BASE_NONGLOBAL_ID);
			return (strdup(GLOBAL_ZONENAME));
		}
	}
	err->err_code = SCHA_ERR_INVAL;
	return (NULL);
#else
	return (strdup(GLOBAL_ZONENAME));
#endif
}


//
// rgm_comm_getpres
//
// Get a reference to the RGM_PRESIDENT node
// The caller is supposed to release it after use.
//
rgm::rgm_comm_ptr
rgm_comm_getpres(void)
{
	rgm::rgm_comm_ptr pres_rgm_p = rgm::rgm_comm::_nil();
	naming::naming_context_var ctx_v;
	Environment e;
	CORBA::Object_ptr obj_p = CORBA::Object::_nil();

	if (!ORB::is_initialized()) {
		if (rgm_orbinit().err_code != SCHA_ERR_NOERR) {
			return (pres_rgm_p);	// nil
		}
	}

	ctx_v = ns::root_nameserver();
	ASSERT(!CORBA::is_nil(ctx_v));

	obj_p = ctx_v->resolve("RGM_PRESIDENT", e);
	if ((e.exception() != NULL) || (CORBA::is_nil(obj_p))) {
		e.clear();
		return (pres_rgm_p);	// nil
	} else {
		pres_rgm_p = rgm::rgm_comm::_narrow(obj_p);
		CORBA::release(obj_p);
	}
	ASSERT(!CORBA::is_nil(pres_rgm_p));
	// Success
	return (pres_rgm_p);
}

//
// rgm_comm_getpres_zc
//
// Get a reference to the RGM_PRESIDENT node
// of the ZC. The caller is supposed to release it after use.
//
rgm::rgm_comm_ptr
rgm_comm_getpres_zc(char *cluster_name)
{
	rgm::rgm_comm_ptr pres_rgm_p = rgm::rgm_comm::_nil();
	naming::naming_context_var ctx_v;
	Environment e;
	CORBA::Object_ptr obj_p = CORBA::Object::_nil();

	if (cluster_name == NULL) {
		return (pres_rgm_p);
	}

	if (!ORB::is_initialized()) {
		if (rgm_orbinit().err_code != SCHA_ERR_NOERR) {
			return (pres_rgm_p);	// nil
		}
	}
#if SOL_VERSION >= __s10
	zoneid_t zoneid = getzoneid();
	if (zoneid < 0) {
		return (pres_rgm_p);
	}
	if (zoneid != GLOBAL_ZONEID) {
		//
		// for commands from local zones, we can only access default
		// cluster
		//
		ctx_v = ns::root_nameserver();
	} else {
		ctx_v = ns::root_nameserver_cz(cluster_name);
	}

#else
	ctx_v = ns::root_nameserver();
#endif
	if (CORBA::is_nil(ctx_v)) {
		//
		// Need to decide what to do for
		// release code. For debug code, assert
		//
		ASSERT(0);
	}

	obj_p = ctx_v->resolve("RGM_PRESIDENT", e);
	if ((e.exception() != NULL) || (CORBA::is_nil(obj_p))) {
		e.clear();
		return (pres_rgm_p);	// nil
	} else {
		// safe typecast by going down inheritance tree
		pres_rgm_p = rgm::rgm_comm::_narrow(obj_p);
		CORBA::release(obj_p);
	}
	ASSERT(!CORBA::is_nil(pres_rgm_p));
	return (pres_rgm_p);
}


//
// Some routines for manipulating sequences.
//

//
// array_to_strseq
//
// Convert a NULL terminated (argv style) list of char * into
// an idl_string_seq_t.
// An [empty] sequence was created by the caller.
//
void
array_to_strseq(const char **listp, rgm::idl_string_seq_t &str_seq)
{
	uint_t		i = 0;
	const char	**p;

	// Treat NULL listp as an empty list
	if (listp == NULL) {
		str_seq.length(0);
		return;
	}

	// Compute the length of the sequence
	p = listp;
	while (*p) {
		i++;
		p++;
	}

	// set the length of the sequence
	str_seq.length(i);

	// Fill in the sequence.
	i = 0;
	p = listp;
	while (*p) {
		str_seq[i] = new_str(*p);
		i++;
		p++;
	}
} //lint !e818


//
// get_namelist_len
//
// Get length of a namelist_t.
//
static uint_t
get_namelist_len(const namelist_t  *n)
{
	uint_t		i = 0;
	const namelist_t	*x;

	x = n;
	while (x) {
		i++;
		x = x->nl_next;
	}
	return (i);
}


//
// get_plist_len
//
// Get length of a NULL-terminated array of scha_property_t's.
//
static uint_t
get_plist_len(const scha_property_t **pp)
{
	const scha_property_t		*q = *pp;
	uint_t		i = 0;

	while (q->sp_name) {
		i++;
		q++;
	}
	return (i);
} //lint !e818


//
// property_to_nameval
//
// Marshal a property namelist into an idl_nameval.
//
static void
property_to_nameval(const scha_property_t *p, rgm::idl_nameval & nv)
{
	namelist_t	*q;
	uint_t		i;

	// compute the length of the property namelist
	i = get_namelist_len(p->sp_value);

	// set the length of the sequence
	nv.nv_val.length(i);

	// copy the name of the property
	nv.idlstr_nv_name = new_str(p->sp_name);

	// Fill in the sequence
	q = p->sp_value;
	i = 0;
	while (q) {

		nv.nv_val[i++] = new_str(q->nl_name);
		q = q->nl_next;
	}
}
//
// property_to_namevalnode
//
// Marshal a property namevalnode list into an idl_nameval.
//
static void
property_to_namevalnode(const scha_property_t *p, rgm::idl_nameval_node & nv)
{
	namelist_t	*q;
	uint_t		i;

	// compute the length of the property namelist
	i = get_namelist_len(p->sp_value);

	// set the length of the sequence
	nv.nv_val.length(i);

	// copy the name of the property
	nv.idlstr_nv_name = new_str(p->sp_name);

	// Fill in the sequence
	q = p->sp_value;
	i = 0;
	while (q) {

		nv.nv_val[i++] = new_str(q->nl_name);
		q = q->nl_next;
	}
	nv.is_per_node = (bool)p->is_per_node;

	// compute the length of the property namelist
	i = get_namelist_len(p->sp_nodes);

	// set the length of the sequence
	nv.nv_nodes.length(i);

	if (p->is_per_node) {
		i = 0;
		// Fill in the sequence
		q = p->sp_nodes;
		while (q) {

			nv.nv_nodes[i++] = new_str(q->nl_name);
			q = q->nl_next;
		}
	}
}

//
// plist_to_nv_seq_ext
//
// Marshal array of scha_property_t's into a sequence of idl_nameval_node's.
// Each idl_nameval_node also contains a sequence.
//
void
plist_to_nv_seq_ext(const scha_property_t **pl, rgm::idl_nameval_node_seq_t
	&nv_seq)
{
	uint_t		i;
	const scha_property_t		*p;
	uint_t		n;

	// compute the length of the property list
	n = get_plist_len((const scha_property_t **)pl);

	// set the length of the sequence
	nv_seq.length(n);

	// Fill in the sequence
	p = *pl;
	i = 0;

	while (p->sp_name) {
		// Marshal property list into nameval sequence
		property_to_namevalnode(p, nv_seq[i]);
		i++;
		p++;
	}
} //lint !e818


//
// plist_to_nv_seq
//
// Marshal array of scha_property_t's into a sequence of idl_nameval's.
// Each idl_nameval also contains a sequence.
//
void
plist_to_nv_seq(const scha_property_t **pl, rgm::idl_nameval_seq_t &nv_seq)
{
	uint_t		i;
	const scha_property_t		*p;
	uint_t		n;

	// compute the length of the property list
	n = get_plist_len(pl);

	// set the length of the sequence
	nv_seq.length(n);

	// Fill in the sequence
	p = *pl;
	i = 0;
	while (p->sp_name) {
		// Marshal property list into nameval sequence
		property_to_nameval(p, nv_seq[i]);
		i++;
		p++;
	}
} //lint !e818
#endif /* EUROPA_FARM */


char *
rgm_error_msg(scha_err_t err_code)
{
	//
	// The function invokes the scha_strerror_i18n function in
	// the libscha library to retrieve the internationalized error
	// string corresponding to the error code err_code.
	//
	return (scha_strerror_i18n(err_code));
}

//
// rgm_vsprintf:
//
// This routine has a vsprintf like variable argument interface.
// It takes a format string, fmt, and the actual parameters to be
// substituted in the format string (in the va_list parameter),
// and fills in the resultant error string in res->err_msg.
//
// The routine figures out the size of the final error message,
// allocates a buffer of that size and uses vsprintf to do the text
// formatting and appends the output to res->err_msg. Please note that it's
// the caller's responsibility to free the memory allocated by this routine.
// Typically, however, the place to free this memory would be where the
// error message is "consumed".
//
// Here are the gory details:
// The second argument to the rgm_vsprintf routine is the format string,
// similar to the second argument of sprintf.
//
// The first argument serves as an in/out parameter. rgm_vsprintf reallocs
// the appropriate sized buffer and appends the formatted error message to the
// end of res->err_msg. It also inserts a newline character between the old
// error message and the new error message being appended. No newline character
// is inserted if res->err_msg was NULL to begin with, which means this was the
// first error message. Typically the fmt string will be the output of the
// gettext on the appropriate error message, so that the string being placed
// in res->err_msg is localized as well. In case rgm_vsprintf itself
// encounters an error (like out of memory or vsprintf failure [highly
// unlikely]) rgm_vsprintf overwrites the err_code and the err_msg in
// res (after freeing err_msg) and inserts an error code and message of its
// own.
//
static void
rgm_vsprintf(scha_errmsg_t *res, const char *fmt, va_list ap)
{
	int errmsg_size = 0;
	FILE *devnull = NULL;
	char *err_msg = NULL;


	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	// find out the size of the final formatted error string by
	// doing a dummy fprintf to /dev/null
	if ((devnull = fopen("/dev/null", "w")) != NULL) {
		errmsg_size = vfprintf(devnull, fmt, ap);
		(void) fclose(devnull);
	} else {
		// fopen of /dev/null failed; internal error.
		res->err_code = SCHA_ERR_INTERNAL;
		free(res->err_msg);
		res->err_msg = strdup_nocheck(dgettext(TEXT_DOMAIN,
		    "Internal error."));
		return;
	}

	if (errmsg_size < 0) {
		// vfprintf failed; internal error.
		res->err_code = SCHA_ERR_INTERNAL;
		free(res->err_msg);
		res->err_msg = strdup_nocheck(dgettext(TEXT_DOMAIN,
		    "Internal error."));
		return;
	}

	// allocate a local buffer of the appropriate size

	if ((err_msg = (char *)malloc_nocheck((size_t)(errmsg_size + 1))) ==
	    NULL) {
		res->err_code = SCHA_ERR_NOMEM;
		free(res->err_msg);
		res->err_msg =
		    strdup_nocheck(dgettext(TEXT_DOMAIN,
			"Not enough swap space."));
		return;
	}

	// do a sprintf to the local buffer using the format string returned
	// by gettext and using all the input parameters passed "as is"
	if (vsprintf(err_msg, fmt, ap) < 0) {
		// vsprintf failed; internal error.
		res->err_code = SCHA_ERR_INTERNAL;
		free(res->err_msg);
		res->err_msg = strdup_nocheck(dgettext(TEXT_DOMAIN,
		    "Internal error."));
		return;
	}

	// we have to take care of two cases here:
	// 1) when res->err_msg is non-null, i.e., the new error
	// message has to be concatenated to the previous one
	// 2) when res->err_msg is null in which case the local buffer
	// can simply be assigned to res->err-msg
	if (res->err_msg) {
		char *tmpbuf;

		uint_t orig_buf_size = (uint_t)strlen(res->err_msg);
		// res->err_msg is non-null, hence this is a concatenation...
		// realloc a new buffer of the appropriate size and then
		// strcat the local buffer to it.
		if ((tmpbuf = (char *)realloc(res->err_msg,
		    orig_buf_size + 1 + strlen(err_msg) + 1)) == NULL) {
			res->err_code = SCHA_ERR_NOMEM;
			free(res->err_msg);
			res->err_msg = strdup_nocheck(dgettext(TEXT_DOMAIN,
			    "Not enough swap space."));
			return;
		} else
			res->err_msg = tmpbuf;
		// add a newline char before appending the next error message.
		res->err_msg[orig_buf_size] = '\n';
		res->err_msg[orig_buf_size + 1] = '\0';
		(void) strcat(res->err_msg, err_msg);
		free(err_msg);
	} else {
		// res->err_msg is null; simply assign the local buffer to
		// res->err_msg
		res->err_msg = err_msg;
	}
}

//
// rgm_format_errmsg:
//
// This function takes a scha_errmsg_t, a non-internationalized string (fmt),
// and the variable number of parameters to be substituted in it.  It looks
// up the fmt string in the message catalog using dgettext, then calls
// rgm_vsprintf (which provides the same functionality as rgm_sprintf)
// to substitute the parameters into the newly localized string, and copy it
// into res->err_msg field.
//
// This function uses a lock to provide mutual exclusion in its execution.
// This locking is necessary because dgettext is not entirely MT-safe.
//
// Note that this function should only be called if the string fmt is
// intended to be internationalized. The rgm_sprintf function provides the
// same functionality as rgm_format_errmsg, but without internationalizing
// the string.  Note that strings intended for syslog should not be
// internationalized, and should not have their formatting parameters
// substituted before the actual syslog call is made, so they should not
// make use of this function.
//
// This function is enhanced to generate message ids.This message id
// appended in the message.
//
// Declare and initialize the mutual exclusion lock here.
// We suppress the  lint error about "union initialization", because
// it is bogus here.
//
static pthread_mutex_t i18n_lock = PTHREAD_MUTEX_INITIALIZER; /*lint !e708 */

void
rgm_format_errmsg(scha_errmsg_t *res, const char *fmt, ...)
{
	va_list ap;
	const char *i18n_str;
	char *idd_str;
	unsigned int id;
	char msgid[20];

	/* Flexelint doesn't understand varargs */
	va_start(ap, fmt);	/*lint !e26 !e50 !e10 */

	// acquire the entry lock
	(void) pthread_mutex_lock(&i18n_lock);
	//
	// First, bind the textdomain.
	// The librgm library uses a different text domain from the commands
	// that call into the library.  So even if the process in which we
	// are executing (rgmd, scrgadm, etc.) called bindtextdomain, it
	// called it for its value of TEXT_DOMAIN, which is different from
	// the value of TEXT_DOMAIN here in librgm.
	//
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	// lookup the string in the catalog.
	i18n_str = dgettext(TEXT_DOMAIN, fmt);
	idd_str = (char *)calloc(1, strlen(i18n_str) + 30);
	if (idd_str == NULL) {
		res->err_code = SCHA_ERR_NOMEM;
		free(res->err_msg);
		res->err_msg = strdup_nocheck(dgettext(TEXT_DOMAIN,
		    "Not enough swap space."));
		return;
	}
	STRLOG_MAKE_MSGID(fmt, id);
	(void) sprintf(msgid, "(C%6.6d) ", id);

	(void) strcat(idd_str, msgid);
	(void) strcat(idd_str, i18n_str);
	// format the string
	rgm_vsprintf(res, idd_str, ap);
	// release the lock
	(void) pthread_mutex_unlock(&i18n_lock);
	va_end(ap);
	free(idd_str);
}

//
// rgm_format_successmsg:
//
// This function is a clone of rgm_format_errmsg.
// The only difference with this is it doesent generate msgids.
//

void
rgm_format_successmsg(scha_errmsg_t *res, const char *fmt, ...)
{
	va_list ap;
	const char *i18n_str;

	va_start(ap, fmt);	/*lint !e40 */

	// acquire the entry lock
	(void) pthread_mutex_lock(&i18n_lock);
	//
	// First, bind the textdomain.
	// The librgm library uses a different text domain from the commands
	// that call into the library.  So even if the process in which we
	// are executing (rgmd, scrgadm, etc.) called bindtextdomain, it
	// called it for its value of TEXT_DOMAIN, which is different from
	// the value of TEXT_DOMAIN here in librgm.
	//
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	// lookup the string in the catalog.
	i18n_str = dgettext(TEXT_DOMAIN, fmt);
	// format the string
	rgm_vsprintf(res, i18n_str, ap);
	// release the lock
	(void) pthread_mutex_unlock(&i18n_lock);
	va_end(ap);
}
//
// rgm_sprintf:
//
// Just a wrapper for rgm_vsprintf with var-arg parameter format.
// See comments on that function above.
//
// Note that this function should be called instead of rgm_format_errmsg
// when the string fmt is not intended to be internationalized.
// If the string fmt should be internationalized, rgm_format_errmsg should
// be used.
//
// Note that strings intended for syslog should not have their formatting
// parameters substituted before the actual syslog call is made, so they
// should not make use of this function.
//
void
rgm_sprintf(scha_errmsg_t *res, const char *fmt, ...)
{
	va_list ap;

	/* Flexelint doesn't understand varargs */
	va_start(ap, fmt);	/*lint !e26 !e50 !e10 */
	rgm_vsprintf(res, fmt, ap);
	va_end(ap);
}

//
// This function is provided for callers that can handle strdup() failing
// to allocate memory (returning NULL). RGMD callers who cannot handle
// memory allocation failures should call strdup() which calls malloc() to
// allocate memory. In the rgmd process, malloc() will exit() if it
// cannot allocate memory and will not return NULL.
//
// The code is cloned from libc with malloc() replaced by malloc_nocheck().
//
char *
strdup_nocheck(const char *s1)
{
	char *s2 = (char *)malloc_nocheck(strlen(s1) + 1);

	if (s2)
		(void) strcpy(s2, s1);
	return (s2);
}


/*
 * check whether zonename contains only valid characters as specified in solaris
 * zones guide. null strings are passed (signifying globalzones), but 0 length
 * strings are rejected. on pre-sol10, non null zonenames are rejected.
 */
boolean_t
is_valid_zonename(const char *ptr)
{
	int i;

	if (ptr == NULL)
		return (B_TRUE);

#if SOL_VERSION < __s10
	return (B_FALSE);
#else
	// check for 0 length strings
	if (ptr[0] == '\0')
		return (B_FALSE);
	for (i = 0; ptr[i] != '\0'; i++) {
		// break early if invalid character
		if (ptr[i] != '_' && ptr[i] != '-' &&
		    ptr[i] != '.' &&
		    !(ptr[i] >= 'a' && ptr[i] <= 'z') &&
		    !(ptr[i] >= 'A' && ptr[i] <= 'Z') &&
		    !(ptr[i] >= '0' && ptr[i] <= '9'))
			return (B_FALSE);
	}
	return (B_TRUE);
#endif
}


#ifndef EUROPA_FARM
//
// Check if the given RGM name is a valid name.
// Those RGM names include : RG name, RT name, RS name,
// enumeration literal names, vendor id.
// The rules for RGM names are:
//	- Must start with a letter
//	- Can contain letters (Upper or lower case), digits, plus the two
//	  additional characters dash ('-') and underbar ('_').
//	- the maximum length of the identifier is 255 characters,
//	  unless we're creating an RG or RT CCR table (is_ccr_table == TRUE),
//	  in which case the maximum length is less.
//
boolean_t
is_rgm_objname_valid(const char *objname, boolean_t is_ccr_table)
{
	const char *tmp = objname;
	size_t len;

	if (objname == NULL)
		return (B_FALSE);

	len = strlen(objname);
	if (len < 1)
		return (B_FALSE);

	// There are restrictions on the length of RT and RT table
	// names in the CCR; MAXRGMOBJNAMELEN > MAXCCRTBLNMLEN.
	if (is_ccr_table) {
		if (len > MAXCCRTBLNMLEN)
			return (B_FALSE);
	} else {
		if (len > MAXRGMOBJNAMELEN)
			return (B_FALSE);
	}

	// the objname must start with a letter.  If not, return error
	if (!isalpha(tmp[0])) //lint !e1055 !e746
		return (B_FALSE);
	tmp += mblen(tmp, MB_CUR_MAX);

	while (*tmp) {
		if (isalpha(*tmp) || isdigit(*tmp)) { //lint !e1055 !e746
			// a - z, A - Z or 0 - 9
			tmp += mblen(tmp, MB_CUR_MAX);
			continue;
		}

		// if it is not the first character, '-' and '_' are allowed
		if (*tmp == '-' || *tmp == '_')
			tmp += mblen(tmp, MB_CUR_MAX);
		else
			return (B_FALSE);
	}

	return (B_TRUE);
}


//
// Slash, asterisk, question mark, left square bracket, right square bracket,
// backslash, blank, tab, semi-colon, comma
// /, *, ?, [,  ], \, , , \t, ;, and ,
// are not valid characters for RT_version string in post-SC30 release.
// In SC30, only ; and , are disallowed and RT version can be NULL.
// Returns TRUE if the version string contains illegal characters;
// otherwise FALSE.
//
boolean_t
illegal_rt_version_chars(const char *rtversion, boolean_t sc30rt)
{
	if (rtversion == NULL)
		return (B_FALSE);

	while (*rtversion) {
		if (*rtversion == ';' || *rtversion == ',')
			return (B_TRUE);

		if (!sc30rt &&
		    (*rtversion == '/' || *rtversion == '*' ||
		    *rtversion == '?' || *rtversion == '[' ||
		    *rtversion == ']' || *rtversion == '\\' ||
		    *rtversion == ' ' || *rtversion == '\t'))
			return (B_TRUE);

		rtversion += mblen(rtversion, MB_CUR_MAX);
	}
	return (B_FALSE);
}


// Check if the given RGM value is  INT value.
// If no value is pass in, return B_TRUE;
// If the value is not in between '0' and '9', return B_FALSE
//
boolean_t
is_an_int_value(const char *value)
{
	const char *tmp = value;

	if (value == NULL)
		return (B_TRUE);

	while (*tmp) {
		if (isdigit(*tmp))		// 0 - 9
			tmp += mblen(tmp, MB_CUR_MAX);
		else
			return (B_FALSE);
	}
	return (B_TRUE);
}


//
// Check if the given RGM value is valid value.
// Those RGM values include :
//	- Property value (including RG property, RT property, R property
//		both system and extension)
//	- Description (including RT description, RG description and
//		R description)
//
// The rules for RGM values are:
//	- Must be in ascii.
//	- Cannot contain characters NUL, newline and semicolon.
//	- the maximum length of the value is MAXRGMVALUESLEN(4MB - 1).
//
// A boolean flag - allow_comma is used to ignore the checking for comma
//		in the string value.
//
boolean_t
is_rgm_value_valid(const char *value, boolean_t allow_comma)
{
	const char *tmp = value;

	// if the value is not specified, it is OK
	if (value == NULL || value[0] == '\0') {
		return (B_TRUE);
	}

	if (strlen(value) > MAXRGMVALUESLEN)
		return (B_FALSE);

	while (*tmp) {
		if (*tmp == '\n' ||	// newline
		    *tmp == ';')	// semicolon
			return (B_FALSE);

		if (!allow_comma && *tmp == ',')	// comma
			return (B_FALSE);
		tmp += mblen(tmp, MB_CUR_MAX);
	}

	return (B_TRUE);
}
#endif /* EUROPA_FARM */

//
// rgm_find_property_in_list
//
// Iterates through the list of which prop_list is the head, comparing
// each key with name.
//
// If a match is found, returns that list element.  Otherwise returns NULL.
//
rgm_property_list_t *
rgm_find_property_in_list(char *name, rgm_property_list_t *prop_list)
{
	rgm_property_list_t *temp_p;

	for (temp_p = prop_list; temp_p != NULL; temp_p = temp_p->rpl_next) {
		if (strcasecmp(name, temp_p->rpl_property->rp_key) == 0) {
			return (temp_p);
		}
	}
	return (NULL);
} //lint !e818

//
// Adds the given name to the end of the curr_namelist by converting it
// into a namelist_t structure.
// It makes local copy of the name passed in.
// This routine does not check for duplicates. If the duplicate check is
// needed, it can be done using the previous routine -- namelist_exists
//
scha_errmsg_t
namelist_add_name(namelist_t **curr_namelist, name_t name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	namelist_t *new_node = NULL;
	if ((new_node = (namelist_t *)
		malloc_nocheck(sizeof (namelist_t))) == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	if ((new_node->nl_name = strdup_nocheck(name)) == NULL) {
		free(new_node);
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	new_node->nl_next = NULL;

	if (*curr_namelist == NULL)
		// first element in the list hence set curr_namelist
		*curr_namelist = new_node;
	else {
		namelist_t *curr_node = *curr_namelist;
		// append the new node to the end of the list
		while (curr_node->nl_next != NULL)
			curr_node = curr_node->nl_next;
		curr_node->nl_next = new_node;
	}
	res.err_code = SCHA_ERR_NOERR;
	return (res);
} //lint !e818

#ifndef EUROPA_FARM
//
// compute_NRUlist_frm_deplist
//
// Computes the Network Resources used list from the dependency
// lists.The information of resource dependencies is read from the
// CCR.
//
void
compute_NRUlist_frm_deplist(
    rgm_resource_t *rs, namelist_t **nl, char *zonename) {
	rdeplist_t *nlp = NULL;
	rgm_rt_t *rt = (rgm_rt_t*)0;
	rgm_resource_t *res = (rgm_resource_t *)0;
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	int i;

	for (i = DEPS_WEAK; i <= DEPS_OFFLINE_RESTART; i++) {
		switch (i) {
			case DEPS_STRONG:
				nlp = rs->r_dependencies.dp_strong;
				break;
			case DEPS_WEAK:
				nlp = rs->r_dependencies.dp_weak;
				break;
			case DEPS_RESTART:
				nlp = rs->r_dependencies.dp_restart;
				break;
			case DEPS_OFFLINE_RESTART:
				nlp = rs->r_dependencies.dp_offline_restart;
				break;
			default:
				// programming error
				break;
		}

		/* Scan each dependency list */
		for (; nlp != NULL; nlp = nlp->rl_next) {
			/* Get the resource configuration */

			(void) rgm_scrgadm_getrsrcconf(nlp->rl_name,
			    &res, zonename);
			if (res == NULL)
				continue;

			/*
			 * Get the resource type
			 * configuration
			*/
			(void) rgm_scrgadm_getrtconf(res->r_type, &rt,
							zonename);
			if (rt == NULL)
				continue;
			/*
			 * If the r property is of system
			 * type "Logical Hostname" or
			 * "Shared Address"
			 * then print those resources
			*/
			else if (rt->rt_sysdeftype == SYST_LOGICAL_HOSTNAME ||
			    rt->rt_sysdeftype == SYST_SHARED_ADDRESS) {
				rgm_status =
				    namelist_add_name(nl, nlp->rl_name);
				if (rgm_status.err_code != SCHA_ERR_NOERR) {
					rgm_format_errmsg(&rgm_status,
					    "Internal error: out of memory.");
					return;
				}
			}
		}
	}
} //lint !e818


// ------------------------- nodeidlist namelist conversion functions

//
// Convert a namelist(which maye be of form node:zone), to a nodeidlist. Caller
// needs to free allocated buffer. initializes the lni as LNI_UNDEF (unless the
// logical node refers to physical node in which case lni = nodeid)
//
scha_errmsg_t
convert_namelist_to_nodeidlist(namelist_t *namelist_in,
    nodeidlist_t **nodeidlist_out)
{
	nodeidlist_t *nidlist;
	nodeidlist_t *nidlist_res = NULL;
	nodeidlist_t *nidlist_last = NULL;
	sol::nodeid_t nodeid = 0;
	char *zonename = NULL;
	scha_errmsg_t scha_err = {SCHA_ERR_NOERR, NULL};

	while (namelist_in) {
		scha_err = logicalnode_to_nodeidzone(namelist_in->nl_name,
		    &nodeid, &zonename);
		if (scha_err.err_code != SCHA_ERR_NOERR) {
			rgm_free_nodeid_list(nidlist_res);
			return (scha_err);
		}


		nidlist = (nodeidlist_t *)calloc(1, sizeof (nodeidlist_t));
		if (NULL == nidlist) {
			rgm_free_nodeid_list(nidlist_res);
			scha_err.err_code = SCHA_ERR_NOMEM;
			return (scha_err);
		}
		nidlist->nl_nodeid = nodeid;
		nidlist->nl_zonename = zonename;
		nidlist->nl_lni = (zonename == NULL) ? nodeid : LNI_UNDEF;
		nidlist->nl_next = NULL;

		if (NULL == nidlist_res) {
			// only the first time through the loop
			nidlist_res = nidlist;
		} else {
			ASSERT(nidlist_last != NULL);
			nidlist_last->nl_next = nidlist;
		}
		nidlist_last = nidlist;

		namelist_in = namelist_in->nl_next;
	}
	*nodeidlist_out = nidlist_res;

	return (scha_err);
} //lint !e818

//
// the function takes a logical node, and converts it to nodeid and zonename
// and stores it in the pointers provided. returns scha_errmsg_t to indicate
// success or failure. Caller needs to free zonename pointer. if zonename is
// holding some allocated buffer, this may cause memory leak.
// There is a similar function parse_logical_nodename in librgmserver. We can
// at a later point of time integrate these if feasible.
//
scha_errmsg_t
logicalnode_to_nodeidzone(const char *name, uint_t *nodeid, char **zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *ptr = NULL;
	char nodename[MAX_LN_LEN];

	CL_PANIC(name != NULL && zonename != NULL && nodeid != NULL);

	*zonename = NULL;
	if (strlcpy(nodename, name, MAX_LN_LEN) >= MAX_LN_LEN) {
		res.err_code = SCHA_ERR_NODE;
		return (res);
	}

	ptr = strchr(nodename, LN_DELIMITER);
	if (ptr != NULL) {
		*ptr++ = '\0';
		if (!is_valid_zonename(ptr)) {
			res.err_code = SCHA_ERR_NODE;
			return (res);
		}
	}

	if (get_nodeid(nodename, nodeid) != 0) {
		res.err_code = SCHA_ERR_NODE;
		return (res);
	}
	if (ptr != NULL && strcasecmp(ptr, GLOBAL_ZONENAME) != 0) {
		*zonename = strdup_nocheck(ptr);
		if (*zonename == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
		}
	}
	return (res);
}

#if SOL_VERSION >= __s10
//
// Convert a namelist of a zonecluster to a nodeidlist. Caller
// needs to free allocated buffer. initializes the lni as LNI_UNDEF (unless the
// logical node refers to physical node in which case lni = nodeid)
//
scha_errmsg_t
convert_zc_namelist_to_nodeidlist(namelist_t *namelist_in,
    nodeidlist_t **nodeidlist_out, char *zcname)
{
	nodeidlist_t *nidlist;
	nodeidlist_t *nidlist_res = NULL;
	nodeidlist_t *nidlist_last = NULL;
	sol::nodeid_t nodeid = 0;
	char *zonename = NULL;
	scha_errmsg_t scha_err = {SCHA_ERR_NOERR, NULL};
	clconf_cluster_t *cl_handle;

	// If the zone cluster name was not specified or if it is "global",
	// then we will fall back on the default method.
	if ((zcname == NULL) ||
		(strcmp(zcname, GLOBAL_ZONENAME) == 0)) {
		return (convert_namelist_to_nodeidlist(namelist_in,
				nodeidlist_out));
	}

	// If we are here, then zone cluster was specified. We have to
	// verify whether this is a valid zone cluster or not.
	cl_handle = clconf_cluster_get_vc_current(zcname);
	CL_PANIC(cl_handle != NULL);

	while (namelist_in) {
		// Fetch the nodeid for this nodename
		nodeid = clconf_zc_get_nodeid_by_nodename(zcname,
					namelist_in->nl_name);
		if (nodeid == NODEID_UNKNOWN) {
			rgm_free_nodeid_list(nidlist_res);
			scha_err.err_code = SCHA_ERR_NODE;
			return (scha_err);
		}

		nidlist = (nodeidlist_t *)calloc(1, sizeof (nodeidlist_t));
		if (NULL == nidlist) {
			rgm_free_nodeid_list(nidlist_res);
			scha_err.err_code = SCHA_ERR_NOMEM;
			return (scha_err);
		}
		nidlist->nl_nodeid = nodeid;
		nidlist->nl_zonename = zonename;
		nidlist->nl_lni = (zonename == NULL) ? nodeid : LNI_UNDEF;
		nidlist->nl_next = NULL;

		if (NULL == nidlist_res) {
			// only the first time through the loop
			nidlist_res = nidlist;
		} else {
			ASSERT(nidlist_last != NULL);
			nidlist_last->nl_next = nidlist;
		}
		nidlist_last = nidlist;

		namelist_in = namelist_in->nl_next;
	}
	*nodeidlist_out = nidlist_res;

	return (scha_err);
} //lint !e818


//
// The function populates node,zone information for passing to rgmd,
// for restricting some of these commands depending on nodelist and/or other
// characteristics.
// returns nodeid using orb_conf. returns null for global zone.
// the function should only be called post-S9.
//
scha_errmsg_t
populate_zone_cred(uint_t *nodeid, char **zonename) {

	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	char		*err_msg = NULL;
	char		local_zone[ZONENAME_MAX + 1 ];
	zoneid_t	zoneid;

	*zonename = NULL;
	*nodeid = orb_conf::local_nodeid();

	if ((zoneid = getzoneid()) < 0) {
		// we don't have errno to scha_err_t mapping
		res.err_code = SCHA_ERR_INTERNAL;
		err_msg = strerror(errno);
		if (err_msg != NULL)
			rgm_format_errmsg(&res, err_msg);
		free(err_msg);
		return (res);
	}
	if (zoneid == 0)
		return (res);

	if (getzonenamebyid(zoneid, local_zone, ZONENAME_MAX) < 0) {
		// we don't have errno to scha_err_t mapping
		res.err_code = SCHA_ERR_INTERNAL;
		res.err_msg = strerror(errno);
		if (err_msg != NULL)
			rgm_format_errmsg(&res, err_msg);
		free(err_msg);
		return (res);
	}
	*zonename = strdup_nocheck(local_zone);
	if (zonename == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}
	return (res);
}
#endif

#endif

/*
 * var_strcat
 * -------------
 * This function concatenates the string represented by all the
 * parameters except the first onto the string stored in *ptr.
 * The format and args are written to a temporary buffer, then
 * *ptr is realloced to be large enough to store the new string
 * as well as its current string.  Finally, the new string is concatenated
 * onto the end of the old.
 *
 * This function handles the case of a NULL *ptr.
 * This function also properly avoids overflowing the temporary buffer.
 */
void
var_strcat(char **ptr, const char *format, /* args */ ...)
{
	va_list pvar;
	char buff[BUFSIZ];
	int size;
	char *temp_ptr = NULL, *buff_ptr = NULL;

	/*
	 * Write the string with args to our buffer.
	 * Use the n form of sprintf to avoid overwriting the
	 * buffer.  vsnprintf (and snprintf) has a neat feature, in that
	 * it returns the number of bytes it should have written, even
	 * if it's more than the number it actually did write, because
	 * it was limited by the buffer size.  Note that the number
	 * returned does not include the \0, which is always written.
	 */

	/* Lint doesn't understand va_start, so suppress the error */
	va_start(pvar, format);	/*lint !e40 */
	size = vsnprintf(buff, BUFSIZ, format, pvar);
	va_end(pvar);

	/*
	 * Make sure we don't need a larger buffer, by
	 * comparing the size returned from vsnprintf with the
	 * size of the buffer.  If the size returned is greater, it
	 * means vsnprintf wanted to write more.
	 *
	 * Note that we must use >=, because snprintf always writes a \0,
	 * but does not include it in the total size it wanted to write.
	 *
	 * We assign the ptr to our buffer (whether it's on the stack
	 * or the heap) to the buff_ptr pointer,  to unify the code below.
	 */
	if (size >= BUFSIZ) {
		/*
		 * In this case, dynamically allocate some space
		 * It should be a rare case, so we can take the
		 * performance hit.
		 */
		temp_ptr = (char *)realloc_err(temp_ptr, (size_t)(size + 1));

		/* Lint doesn't understand va_start, so suppress the error */
		va_start(pvar, format);	/*lint !e40 */
		(void) vsprintf(temp_ptr, format, pvar);
		va_end(pvar);
		buff_ptr = temp_ptr;
	} else {
		buff_ptr = buff;
	}

	/*
	 * If the old string is empty, we allocate space for the new
	 * stuff plus one for the NULL char.
	 *
	 * If we have something in the old string, we allocate space
	 * for the new stuff and the old stuff (remembering the NULL char).
	 */
	if (*ptr == NULL) {
		*ptr = (char *)realloc_err(*ptr, (size_t)(size + 1));
		(*ptr)[0] = '\0';		// initialize buffer
	} else {
		*ptr = (char *)realloc_err(*ptr, strlen(*ptr) +
		    (size_t)(size + 1));
	}
	/* now do the actual concatenation */
	(void) strcat(*ptr, buff_ptr);

	/*
	 * Always free the temp_ptr, even though most of the time
	 * it's NULL. Freeing a NULL pointer is a no-op.
	 */
	free(temp_ptr);
}

// Check whether VM can return new version before upgrade
// callback is complete.
boolean_t
allow_zc(void)
{
#if SOL_VERSION >= __s10
	clcomm_vp_version_t rgm_version;
	if (clcomm_get_running_version("rgm", &rgm_version)
	    == 0) {
		if ((rgm_version.major_num > 2) ||
		    ((rgm_version.major_num == 2) &&
		    (rgm_version.minor_num >= 6)))
			return (B_TRUE);
		else
			return (B_FALSE);
	} else {
		return (B_FALSE);
	}
#else
	return (B_FALSE);
#endif
}
