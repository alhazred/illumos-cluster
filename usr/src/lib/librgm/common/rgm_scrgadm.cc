/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */


#pragma ident	"@(#)rgm_scrgadm.cc	1.125	09/01/09 SMI"



#include <rgm/rgm_msg.h>
#include <rgm/rgm_cmn_proto.h>
#include <rgm/rgm_cnfg.h>
#include <rgm/rgm_util.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/rgm_call_pres.h>
#include <sys/sc_syslog_msg.h>
#include <rgm/rgm_errmsg.h>
#include <clcomm_cluster_vm.h>

#include <rgm/sczones.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

extern scha_errmsg_t check_installmode(void);

static scha_errmsg_t construct_paramtable(const rgm_rt_t *rt, int paramtblsz,
    RtrParam_T **paramt, rgm_param_t ***rgm_params_result);
static scha_errmsg_t rt_registered(char *vendor_id, char *RT_name,
    const char *new_version, boolean_t *retval, const char *zonename);
static scha_errmsg_t build_rt_upgrade_list(RtrUpg_T **rtr_array,
    int arraysz, rgm_rt_upgrade_t **rtulist, const char *tag);
static scha_errmsg_t rgm_scrgadm_update_resource_property_helper(char *rname,
    const scha_resource_properties_t *rpl, boolean_t suppress_validate,
    const char *zone);
static scha_errmsg_t rgm_scrgadm_update_rg_prop_helper(char *rgname,
    scha_property_t **pl, uint_t flg, boolean_t suppress_validate,
    const char **rgnames, const char *zone);
static scha_errmsg_t call_scrgadm_rs_add(const char *rname, const char *rtname,
    const char *rgname, scha_resource_properties_t *rpl, const char *zone);

//
// Called by scrgadm.c.
//
scha_errmsg_t
rgm_scrgadm_getrtconf(char *rtname, rgm_rt_t **rt, const char *zone_cluster)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	// We don't need to call rgmcnfg_get_rtrealname()
	// here because scrgadm used the list of RTs returned
	// by rgmcnfg_get_rtlist().

	if (rtname == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	return (rgmcnfg_read_rttable(rtname, rt, zone_cluster));
}

/*
 * Creates a cannonical name of the form nodeid:zonename
 * Callers responsibility to free the allocated memory.
 */
char *
get_canonical_name(const char *nname)
{
	char *fullname = NULL, *node_name = NULL, *zname = NULL;
	uint_t nodeid;

	node_name = strdup(nname);
	zname = strchr(node_name, ':');
	if (zname)
		*zname++ = '\0';

	if (get_nodeid(node_name, &nodeid) != 0) {
		free(node_name);
		return (NULL);
	}
	if (zname) {
		fullname = new char[strlen(zname) + 2 + 2];
	} else {
		fullname = new char [2 + 1];
	}
	(void) sprintf(fullname, "%d", nodeid);
	if (zname) {
		(void) strcat(fullname, ":");
		(void) strcat(fullname, zname);
	}
	free(node_name);
	return (fullname);
}

/*
 * This function would be called by scrgadm.c to
 * retirieve the "switch" value of both r_onoff_switch
 * and r_monitored_switch on a particular node.
 */

scha_errmsg_t
get_swtch(switch_t r_onoff_switch, char *name, scha_switch_t *swith)
{
	rgm_switch_t *t = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *node_name = NULL;

	node_name = get_canonical_name(name);
	if (node_name == NULL) {
		res.err_code = SCHA_ERR_NODE;
		return (res);
	}
	t = (rgm_switch_t *)r_onoff_switch;
	*swith = t->r_switch_node[node_name];
	free(node_name);
	return (res);
} //lint !e818

#if (SOL_VERSION >= __s10)
/*
 * This function is mainly called by the new-cli commands to
 * retirieve the "switch" value of both r_onoff_switch
 * and r_monitored_switch on a particular node for a resource.
 * If "zcname", zone-cluster name, is not specified, this method
 * will invoke "get_swtch". This method will check the validity
 * of the specified node in the zone cluster "zcname".
 */

scha_errmsg_t
get_swtch_zc(switch_t r_onoff_switch, char *name, scha_switch_t *swith,
    char *zcname)
{
	rgm_switch_t *t = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *node_name = NULL;
	clconf_cluster_t *cl_handle;
	nodeid_t nodeid;

	if (zcname == NULL) {
		return (get_swtch(r_onoff_switch, name, swith));
	}

	// Verify whether this is a valid zone cluster.
	cl_handle = clconf_cluster_get_vc_current(zcname);

	if (cl_handle == NULL) {
		// Invalid zone cluster
		res.err_code = SCHA_ERR_ZONE_CLUSTER;
		return (res);
	}

	nodeid = clconf_zc_get_nodeid_by_nodename(zcname, name);
	if (nodeid == NODEID_UNKNOWN) {
		res.err_code = SCHA_ERR_NODE;
		return (res);
	}

	node_name = new char [2 + 1];
	(void) sprintf(node_name, "%d", nodeid);
	t = (rgm_switch_t *)r_onoff_switch;
	*swith = t->r_switch_node[node_name];
	free(node_name);
	return (res);
} //lint !e818
#endif

/*
 * This function would be called by scrgadm.c to
 * retrieve the rp_value for a particular nodename.
 * Its the callers responsibility to free the memory.
 */
scha_errmsg_t
get_value(value_t rp_value, char *name, char **value,
    boolean_t is_per_node)
{
	rgm_value_t *t = NULL;
	char *node_name = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

//	*value = NULL;
	if (name != NULL) {
		node_name = get_canonical_name(name);
		if (node_name == NULL) {
			res.err_code = SCHA_ERR_NODE;
			return (res);
		}
	}

	t = (rgm_value_t *)rp_value;
	if (is_per_node) {
		if (node_name == NULL) {
			res.err_code = SCHA_ERR_NODE;
			return (res);
		}
		if (t->rp_value_node[node_name] != NULL)
			*value = strdup(t->rp_value_node[node_name]);
		else
			/*
			 * The default value for a per-node extension
			 * property was stored at the index "0"
			 */
			if (t->rp_value_node["0"])
				*value = strdup(t->rp_value_node["0"]);
	} else {
		if (t->rp_value[0])
			*value = strdup(t->rp_value[0]);
	}
	free(node_name);
	return (res);
} //lint !e818

//
// Called by scrgadm.c.
//
scha_errmsg_t
rgm_scrgadm_getrtlist(char **rts[], const char *zonename)
{
	namelist_t *rt_list = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	*rts = NULL;

	// Read the CCR to get a list of all RTs
	res = rgmcnfg_get_rtlist(&rt_list, zonename);

	if (res.err_code != SCHA_ERR_NOERR || rt_list == NULL)
		return (res);

	/* convert rt_list to rts */
	*rts = nstr2strarray(rt_list);

	rgm_free_nlist(rt_list);

	if (*rts == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}


/*
 * memstr()2str - Convert the data from RtrString type to string type.
 *    If the data is unset return NULL,
 *    else if the data is NULL return " ",
 *    else return the string
 */

static char *
memstr2str(const RtrString &s)
{
	char *p;

	if (s.value == RTR_NONE) {
		return (NULL);
	}
	if (s.value == RTR_NULL) {
		p = strdup("");
		return (p);
	}

	p = (char *)malloc_nocheck((size_t)(s.len + 1));
	if (p == NULL)
		return (NULL);

	(void) strncpy(p, s.start, (size_t)(s.len));
	*(p + s.len) = 0;
	return (p);
}


/*
 * memstrs2namelist()
 */
static namelist_t *
memstrs2namelist(RtrString *ns, int count)
{
	namelist_t *l = NULL;
	namelist_t *curr = NULL;

	if (ns == NULL || count == 0)
		return ((namelist_t *)NULL);
	for (int i = 0; i < count; i++) {
		namelist_t *ptr = (namelist_t *)malloc(sizeof (namelist_t));
		if (ptr == NULL)
			return (NULL);
		ptr->nl_name = memstr2str(ns[i]);
		ptr->nl_next = NULL;

		if (l == NULL)
			l = ptr;
		else
			curr->nl_next = ptr;
		curr = ptr;
	}

	return (l);
} //lint !e818


//
// Called by scrgadm and clresourcetype for RT registration
//
scha_errmsg_t
rgm_scrgadm_parse2rt(RtrFileResult *rtreg_res, rgm_rt_t **rt_p,
    const char *zonename)
{
	Rtr_T *rtr_p;
	char *compound_name = NULL;
	char *vendor_id = NULL;
	char *RT_name = NULL;
	char *rt_version = NULL;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	rgm_rt_t	*rt = NULL;
	size_t		len = 0;
	boolean_t	exists = B_FALSE;
	rgm_param_t	**rgm_params_result;

	*rt_p = NULL;
	if (rtreg_res == NULL || rtreg_res->paramtabsz == 0) {
		res.err_code = SCHA_ERR_INTERNAL;
		rgm_format_errmsg(&res, REM_INTERNAL);
		return (res);
	}
	rtr_p = rtreg_res->rt_registration;

	/*
	 * RT registration information
	 */

	/*
	 * RT name is stored as vendor_id.RT_name:rt_version
	 * if #$upgrade directive is present; otherwise it is
	 * stored as vendor_id.RT_name.
	 */

	if ((RT_name = memstr2str(rtr_p->rtname)) == NULL) {
		// This cannot happen; parser will catch it first.
		// However memstr2str could return NULL if out of memory.
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	// validate the rt name before concatenating the prefix and suffix;
	// B_TRUE says check object name length for CCR table creation
	if (!is_rgm_objname_valid(RT_name, B_TRUE)) {
		/* rtname is invalid */
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RT_NAME_INVALID, RT_name);
		goto cleanup_and_return; //lint !e801
	}

	vendor_id = memstr2str(rtr_p->vendor_id);
	if (vendor_id != NULL && vendor_id[0] != '\0') {
		// validate the vendor id;
		// B_TRUE says check object name length for CCR table creation.
		if (!is_rgm_objname_valid(vendor_id, B_TRUE)) {
			/* vendor id is invalid */
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_VENDOR_ID_INVALID,
			    vendor_id);
			goto cleanup_and_return; //lint !e801
		}
		/* length of vendor_id plus dot */
		len += strlen(vendor_id) + 1;
	}

	rt_version = memstr2str(rtr_p->version);

	if (!rtreg_res->sc30_rtr) {
		// This is a 3.1 (or later) RTR file

		if (rt_version == NULL || rt_version[0] == '\0') {
			// No RT version was specified in RTR file
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_RT_NO_VERSION);
			goto cleanup_and_return; //lint !e801
		}

		// length of version string plus colon character
		len += strlen(rt_version) + 1;
	}

	// Check for illegal version characters
	if (illegal_rt_version_chars(rt_version, rtreg_res->sc30_rtr)) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_ILLEGAL_RT_VERS_CHARS,
		    "RT_version string");
		goto cleanup_and_return; //lint !e801
	}

	//
	// Make sure there isn't already an RT registered with same
	// name and version (but possibly different CCR table name).
	// Involves reading the CCR.
	//
	res = rt_registered(vendor_id, RT_name, rt_version, &exists, zonename);
	if (res.err_code != SCHA_ERR_NOERR) {
		goto cleanup_and_return; //lint !e801
	}
	if (exists) {
		res.err_code = SCHA_ERR_RT;
		rgm_format_errmsg(&res, REM_RT_EXISTS, RT_name);
		goto cleanup_and_return; //lint !e801
	}

	len += strlen(RT_name);

	// allocate a buf to contain compound RT name;
	// add 1 for the terminating NUL character
	// <vendor_id>.<resource_type>:<version>
	if ((compound_name = (char *)malloc_nocheck(len+1)) == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto cleanup_and_return; //lint !e801
	}

	if (!rtreg_res->sc30_rtr) {
		// 3.1 (or later) RTR file

		if (vendor_id == NULL || vendor_id[0] == '\0')
			// no vendor_id was specified
			(void) sprintf(compound_name, "%s:%s", RT_name,
			    rt_version);
		else
			// vendor_id was specified
			(void) sprintf(compound_name, "%s.%s:%s",
			    vendor_id, RT_name, rt_version);
	} else {
		// 3.0-style RTR file
		if (vendor_id == NULL || vendor_id[0] == '\0')
			// no vendor_id was specified
			(void) sprintf(compound_name, "%s", RT_name);
		else
			// vendor_id was specified
			(void) sprintf(compound_name, "%s.%s",
			    vendor_id, RT_name);
	}

	if (len > MAXCCRTBLNMLEN) {
		// compound rtname is too long; CCR table creation would fail
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_RT_NAME_INVALID, compound_name);
		goto cleanup_and_return; //lint !e801
	}

	rt = (rgm_rt_t *)malloc(sizeof (rgm_rt_t));
	if (rt == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto cleanup_and_return; //lint !e801
	}

	bzero((char *)rt, sizeof (rgm_rt_t));

	rt->rt_sc30 = rtreg_res->sc30_rtr;

	// make a copy of compound_name to make cleanup at bottom cleaner
	// (no double-freeing via rgm_free_rt(rt))
	if ((rt->rt_name = strdup_nocheck(compound_name)) == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto cleanup_and_return; //lint !e801
	}

	rt->rt_basedir = memstr2str(rtr_p->basedir);
	rt->rt_methods.m_start = memstr2str(rtr_p->start);
	rt->rt_methods.m_stop = memstr2str(rtr_p->stop);
	rt->rt_methods.m_validate = memstr2str(rtr_p->validate);
	rt->rt_methods.m_update = memstr2str(rtr_p->update);
	rt->rt_methods.m_init = memstr2str(rtr_p->init);
	rt->rt_methods.m_fini = memstr2str(rtr_p->fini);
	rt->rt_methods.m_boot = memstr2str(rtr_p->boot);
	rt->rt_methods.m_monitor_start = memstr2str(rtr_p->monitor_start);
	rt->rt_methods.m_monitor_stop = memstr2str(rtr_p->monitor_stop);
	rt->rt_methods.m_monitor_check = memstr2str(rtr_p->monitor_check);
	rt->rt_methods.m_prenet_start = memstr2str(rtr_p->prenet_start);
	rt->rt_methods.m_postnet_stop = memstr2str(rtr_p->postnet_stop);

	// default value of single_instance = B_FALSE
	rt->rt_single_inst = (rtr_p->single_instance == RTR_TRUE) ?
		B_TRUE : B_FALSE;


	// default value of init_nodes = SCHA_RG_PRIMARIES
	switch (rtr_p->init_nodes) {
	case RTR_RT_INSTALLED_NODES:
		rt->rt_init_nodes = SCHA_INFLAG_RT_INSTALLED_NODES;
		break;
	case RTR_NONE:
	case RTR_RG_PRIMARIES:
		rt->rt_init_nodes = SCHA_INFLAG_RG_PRIMARIES;
		break;
	case RTR_FALSE:
	case RTR_WHEN_UNMONITORED:
	case RTR_SHARED_ADDRESS:
	case RTR_NULL:
	case RTR_LOGICAL_HOSTNAME:
	case RTR_WHEN_UNMANAGED:
	case RTR_WHEN_OFFLINE:
	case RTR_FALSE_UNSET:
	case RTR_STRINGARRAY:
	case RTR_ENUM:
	case RTR_STRING:
	case RTR_BOOLEAN:
	case RTR_INT:
	case RTR_AT_CREATION:
	case RTR_TRUE:
	case RTR_ANYTIME:
	case RTR_WHEN_DISABLED:
	default:
		ASSERT(0);
	}


	// default value of rt_proxy = B_FALSE
	rt->rt_proxy = (rtr_p->proxy == RTR_TRUE) ?
		B_TRUE : B_FALSE;

	// default value of rt_failover = B_FALSE
	rt->rt_failover = (rtr_p->failover == RTR_TRUE) ?
		B_TRUE : B_FALSE;

	//
	// Default value of rt_globalzone = B_FALSE except for network
	// address resource types.
	//
	rt->rt_globalzone = (rtr_p->globalzone == RTR_TRUE) ?
		B_TRUE : B_FALSE;

	// default value of rt->rt_sysdeftype = SYST_NONE
	switch (rtr_p->sysdefined_type) {
	case RTR_SHARED_ADDRESS:
		rt->rt_sysdeftype = SYST_SHARED_ADDRESS;
		rt->rt_globalzone = B_TRUE;
		break;
	case RTR_LOGICAL_HOSTNAME:
		rt->rt_sysdeftype = SYST_LOGICAL_HOSTNAME;
		rt->rt_globalzone = B_TRUE;
		break;
	case RTR_NONE:
		rt->rt_sysdeftype = SYST_NONE;
		break;
	case RTR_NULL:
	case RTR_TRUE:
	case RTR_FALSE:
	case RTR_AT_CREATION:
	case RTR_ANYTIME:
	case RTR_WHEN_DISABLED:
	case RTR_WHEN_UNMANAGED:
	case RTR_WHEN_UNMONITORED:
	case RTR_WHEN_OFFLINE:
	case RTR_RG_PRIMARIES:
	case RTR_FALSE_UNSET:
	case RTR_STRINGARRAY:
	case RTR_ENUM:
	case RTR_STRING:
	case RTR_BOOLEAN:
	case RTR_INT:
	case RTR_RT_INSTALLED_NODES:
	default:
		ASSERT(0);
	}
	//
	// RT_System property
	// Default value is FALSE except if sysdeftype is defined
	//
	switch (rtr_p->rt_system) {
	case RTR_TRUE:
		rt->rt_system = B_TRUE;
		break;
	case RTR_FALSE:
		rt->rt_system = B_FALSE;
		break;
	case RTR_NONE:
	case RTR_AT_CREATION:
	case RTR_ANYTIME:
	case RTR_WHEN_DISABLED:
	case RTR_WHEN_UNMANAGED:
	case RTR_WHEN_UNMONITORED:
	case RTR_WHEN_OFFLINE:
	case RTR_RG_PRIMARIES:
	case RTR_FALSE_UNSET:
	case RTR_STRINGARRAY:
	case RTR_ENUM:
	case RTR_STRING:
	case RTR_BOOLEAN:
	case RTR_INT:
	case RTR_RT_INSTALLED_NODES:
	case RTR_NULL:
	case RTR_SHARED_ADDRESS:
	case RTR_LOGICAL_HOSTNAME:
	default:
		rt->rt_system = (rt->rt_sysdeftype != SYST_NONE) ?
			B_TRUE : B_FALSE;
	}
	// If apiversion is not set then set it with the
	// current API version

	if (rtr_p->apiversion_isset == 1)
		rt->rt_api_version = (uint_t)rtr_p->apiversion;
	else
		rt->rt_api_version = CURRENT_SCHA_API_VERSION;

	// make a copy of rt_version to make cleanup at bottom cleaner
	// (no double-freeing via rgm_free_rt(rt))
	if (rt_version) {
		if ((rt->rt_version = strdup_nocheck(rt_version)) == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto cleanup_and_return; //lint !e801
		}
	}

	rt->rt_pkglist = memstrs2namelist(rtr_p->pkglist, rtr_p->pkglistsz);

	rt->rt_description = memstr2str(rtr_p->rt_description);
	//
	// validate rt description value
	// 'TRUE' as the 2nd argument to 'is_rgm_value_valid' call
	// indicates comma is a valid character
	//
	if (!is_rgm_value_valid(rt->rt_description, B_TRUE)) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES,
		    SCHA_RT_DESCRIPTION);
		goto cleanup_and_return; //lint !e801
	}

	//
	// RT upgrade lists
	//

	if (rt->rt_sc30) {
		// 3.0 RTR file
		rt->rt_upgrade_from = NULL;
		rt->rt_downgrade_to = NULL;
	} else {
		// 3.1 (or later) RTR file

		res = build_rt_upgrade_list(rtreg_res->upgradearray,
		    rtreg_res->upgradesz, &(rt->rt_upgrade_from),
		    SCHA_UPGRADE_FROM);
		if (res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_CHECKS) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    REM_ILLEGAL_RT_VERS_CHARS,
				    "Upgrade_from RT version");
			}
			goto cleanup_and_return; //lint !e801
		}

		res = build_rt_upgrade_list(
		    rtreg_res->downgradearray,
		    rtreg_res->downgradesz, &(rt->rt_downgrade_to),
		    SCHA_DOWNGRADE_TO);
		if (res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_CHECKS) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    REM_ILLEGAL_RT_VERS_CHARS,
				    "Downgrade_to RT version");
			}
			goto cleanup_and_return; //lint !e801
		}
	}

	//
	// ParamTable
	//
	res = construct_paramtable(rt, rtreg_res->paramtabsz,
	    rtreg_res->paramtab, &rgm_params_result);
	if (res.err_code != SCHA_ERR_NOERR)
		goto cleanup_and_return; //lint !e801

	rt->rt_paramtable = rgm_params_result;

	*rt_p = rt;

cleanup_and_return:

	free(vendor_id);
	free(RT_name);
	free(rt_version);
	free(compound_name);

	if (res.err_code != SCHA_ERR_NOERR) {
		// only free this on failure
		rgm_free_rt(rt);
		return (res);
	}

	return (res);
} //lint !e818


/*
 * build_rt_upgrade_list -
 *	Build a list for upgrade_from or downgrade_to directives from the
 *	RTR parser result.
 */
static scha_errmsg_t
build_rt_upgrade_list(RtrUpg_T **rtr_array, int arraysz,
    rgm_rt_upgrade_t **rtulist, const char *tag)
{
	rgm_rt_upgrade_t	*upg_p, *curr = NULL;
	scha_errmsg_t		res = {SCHA_ERR_NOERR, NULL};
	uint_t		i;

	*rtulist = NULL;
	for (i = 0; i < (uint_t)arraysz; i++) {
		upg_p = (rgm_rt_upgrade_t *)malloc(sizeof (rgm_rt_upgrade_t));
		if (upg_p == NULL) {
			rgm_free_upglist(*rtulist);
			*rtulist = NULL;
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}

		upg_p->rtu_next = NULL;
		upg_p->rtu_version = memstr2str(rtr_array[i]->upg_version);
		if (upg_p->rtu_version == NULL) {
			rgm_free_upglist(*rtulist);
			*rtulist = NULL;
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}

		if (illegal_rt_version_chars(upg_p->rtu_version, B_FALSE)) {
			res.err_code = SCHA_ERR_CHECKS;
			rgm_free_upglist(*rtulist);
			*rtulist = NULL;
			return (res);
		}

		// tunability for rt upgrade
		switch (rtr_array[i]->upg_tunability) {
		case RTR_ANYTIME:
			upg_p->rtu_tunability = TUNE_ANYTIME;
			break;
		case RTR_AT_CREATION:
			upg_p->rtu_tunability = TUNE_AT_CREATION;
			break;
		case RTR_WHEN_DISABLED:
			upg_p->rtu_tunability = TUNE_WHEN_DISABLED;
			break;
		case RTR_WHEN_OFFLINE:
			upg_p->rtu_tunability = TUNE_WHEN_OFFLINE;
			break;
		case RTR_WHEN_UNMANAGED:
			upg_p->rtu_tunability = TUNE_WHEN_UNMANAGED;
			break;
		case RTR_WHEN_UNMONITORED:
			upg_p->rtu_tunability = TUNE_WHEN_UNMONITORED;
			break;
		case RTR_NONE:
		case RTR_NULL:
		case RTR_TRUE:
		case RTR_FALSE:
		case RTR_RG_PRIMARIES:
		case RTR_RT_INSTALLED_NODES:
		case RTR_INT:
		case RTR_BOOLEAN:
		case RTR_STRING:
		case RTR_ENUM:
		case RTR_STRINGARRAY:
		case RTR_LOGICAL_HOSTNAME:
		case RTR_SHARED_ADDRESS:
		case RTR_FALSE_UNSET:
		default:
			rgm_free_upglist(*rtulist);
			*rtulist = NULL;
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INVALID_PROP_VALUES, tag);
			return (res);
		}

		if (*rtulist == NULL)
			*rtulist = upg_p;
		else
			curr->rtu_next = upg_p;
		curr = upg_p;
	}

	return (res);
} //lint !e818


//
// construct_paramtable
//
// Called by rgm_scrgadm_parse2rt() to construct the paramtable
// of resource properties.
//
scha_errmsg_t
construct_paramtable(const rgm_rt_t *rt, int paramtblsz, RtrParam_T **paramt,
    rgm_param_t ***rgm_params_result)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	namelist_t	*nl;
	rgm_param_t	**rgm_params;

	//
	// ParamTable
	//
	rgm_params = (rgm_param_t **)
	    calloc((size_t)(paramtblsz + 1), sizeof (rgm_param_t *));
	if (rgm_params == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	for (int i = 0; i < paramtblsz; i++) {
		const RtrString pname = paramt[i]->propname;
		//
		// If the Global_zone_override property is declared in the
		// paramtable, make sure that Global_zone=TRUE for this
		// resource type.  If not, return an error.
		//
		if (strcasecmp(pname.start, SCHA_GLOBAL_ZONE_OVERRIDE) == 0 &&
		    !rt->rt_globalzone) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INVALID_RTR_PROP,
			    SCHA_GLOBAL_ZONE_OVERRIDE);
			return (res);
		}

		rgm_params[i] = (rgm_param_t *)malloc(sizeof (rgm_param_t));
		if (rgm_params[i] == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}
		bzero((char *)rgm_params[i], sizeof (rgm_param_t));

		rgm_params[i]->p_name = memstr2str(pname);

		//
		// validate property name
		// B_FALSE says this won't be CCR table
		//
		if (!is_rgm_objname_valid(rgm_params[i]->p_name, B_FALSE)) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INVALID_PROP_NAME,
			    rgm_params[i]->p_name);
			return (res);
		}

		// Set any unrecognized value to p_extension = B_FALSE
		rgm_params[i]->p_extension =
		    (paramt[i]->extension == RTR_TRUE) ? B_TRUE : B_FALSE;
		rgm_params[i]->p_per_node =
		    (paramt[i]->per_node == RTR_TRUE) ? B_TRUE : B_FALSE;

		// explicit and default setting of attribute has been
		// handled at a higher level.
		// Set any unrecognized value to no tunability
		switch (paramt[i]->tunable) {
		case RTR_AT_CREATION:
			rgm_params[i]->p_tunable = TUNE_AT_CREATION;
			break;
		case RTR_TRUE:
		case RTR_ANYTIME:
			rgm_params[i]->p_tunable = TUNE_ANYTIME;
			break;
		case RTR_WHEN_DISABLED:
			rgm_params[i]->p_tunable = TUNE_WHEN_DISABLED;
			break;
		case RTR_FALSE:
		case RTR_NONE:
		case RTR_SHARED_ADDRESS:
		case RTR_WHEN_UNMONITORED:
		case RTR_LOGICAL_HOSTNAME:
		case RTR_NULL:
		case RTR_WHEN_UNMANAGED:
		case RTR_WHEN_OFFLINE:
		case RTR_RG_PRIMARIES:
		case RTR_FALSE_UNSET:
		case RTR_STRINGARRAY:
		case RTR_ENUM:
		case RTR_STRING:
		case RTR_BOOLEAN:
		case RTR_INT:
		case RTR_RT_INSTALLED_NODES:
		default:
			rgm_params[i]->p_tunable = TUNE_NONE;
			break;
		}
		//
		// All types have the default value represented in
		// the RtrFileResult as a string or string-array,
		// as well as having the string converted to other
		// types for convenience.
		// Use param->defaultstr and defaultint_isset as indicating
		// whether default INT type is set.
		// Use param->defaultstr as indicating whether default STRING
		// or ENUM type is set.
		// Use param->defaultarray and param->defaultarraysz as
		// indicating whether default STRINGARRAY type is set.
		// Use param->defaultboolean and param->defaultstr as indicating
		// whether default BOOLEAN type is set.
		//
		// Assume that the RTR file processing did all the right
		// conversions and checks, so just copy the type-specific
		// default values when setting the type.
		//
		//
		rgm_params[i]->p_default_isset = B_FALSE;

		switch (paramt[i]->type) {
		default:
			// shouldn't happen that type isn't set
		case RTR_SHARED_ADDRESS:
		case RTR_LOGICAL_HOSTNAME:
		case RTR_NULL:
		case RTR_TRUE:
		case RTR_FALSE:
		case RTR_AT_CREATION:
		case RTR_ANYTIME:
		case RTR_WHEN_DISABLED:
		case RTR_WHEN_UNMANAGED:
		case RTR_WHEN_UNMONITORED:
		case RTR_WHEN_OFFLINE:
		case RTR_RG_PRIMARIES:
		case RTR_FALSE_UNSET:
		case RTR_RT_INSTALLED_NODES:
		case RTR_NONE:
		case RTR_STRING:
			//
			// When DEFAULT = ; and DEFAULT = "", defaultstr.value
			// is set to RTR_NULL and defaultstr.start is NULL.
			// When no DEFAULT, defaultstr.value is set to RTR_NONE.
			// When DEFAULT = <value>, defaultstr.value is set to
			// RTR_STRING and defaultstr.start is not NULL.
			//
			if (paramt[i]->defaultstr.value != RTR_NONE)
				rgm_params[i]->p_default_isset = B_TRUE;

			rgm_params[i]->p_type = SCHA_PTYPE_STRING;
			rgm_params[i]->p_defaultstr =
			    memstr2str(paramt[i]->defaultstr);

			//
			// validate property default values
			// 'TRUE' as the 2nd argument to 'is_rgm_value_valid'
			// call indicates comma is a valid character.
			//
			if (!is_rgm_value_valid(rgm_params[i]->p_defaultstr,
			    B_TRUE)) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    REM_INVALID_PROP_VALUES,
				    rgm_params[i]->p_name);
				return (res);
			}
			break;

		case RTR_INT:
			//
			// When DEFAULT = <value>, defaultint is set and
			// defaultint_isset is B_TRUE.
			// When DEFAULT =; or DEFAULT = "", defaultint is
			// set to 0 and defaultint_isset is B_FALSE.
			// When no DEFAULT, defaultint_isset is set to B_FALSE.
			//
			if (paramt[i]->defaultint_isset)
				rgm_params[i]->p_default_isset = B_TRUE;

			rgm_params[i]->p_type = SCHA_PTYPE_INT;
			rgm_params[i]->p_defaultint = paramt[i]->defaultint;
			break;

		case RTR_BOOLEAN:
			//
			// When DEFAULT = ;, defaultboolean and
			// defaultstr.value are both set to RTR_NULL.
			// When DEFAULT = "", defaultboolean is set to RTR_NONE
			// and defaultstr.value is set to RTR_NULL.
			// When no DEFAULT, defaultboolean and defaultstr.value
			// are both set to RTR_NONE.
			// When DEFAULT = TRUE|FALSE, defaultboolean is set to
			// RTR_TRUE|RTR_FALSE and defaultstr.value is set to
			// RTR_STRING and defaultstr.start is not NULL.
			//
			if (paramt[i]->defaultboolean != RTR_NONE &&
			    paramt[i]->defaultstr.value != RTR_NONE)
				rgm_params[i]->p_default_isset = B_TRUE;

			rgm_params[i]->p_type = SCHA_PTYPE_BOOLEAN;
			rgm_params[i]->p_defaultbool =
			    (paramt[i]->defaultboolean == RTR_TRUE) ?
			    B_TRUE : B_FALSE;
			break;

		case RTR_ENUM:
			//
			// When DEFAULT = ; and DEFAULT = "", they are not
			// allowed for ENUM type.
			// When no DEFAULT, defaultstr.value is set to RTR_NONE.
			// When DEFAULT = <value>, defaultstr.value is set to
			// RTR_STRING and defaultstr.start is not NULL.
			//
			if (paramt[i]->defaultstr.value != RTR_NONE &&
			    paramt[i]->defaultstr.value != RTR_NULL &&
			    paramt[i]->defaultstr.start != NULL)
				rgm_params[i]->p_default_isset = B_TRUE;

			rgm_params[i]->p_type = SCHA_PTYPE_ENUM;
			rgm_params[i]->p_enumlist =
			    memstrs2namelist(paramt[i]->enumlist,
			    paramt[i]->enumlistsz);
			//
			// validate the enumeration literal name
			//
			for (nl = rgm_params[i]->p_enumlist; nl != NULL;
			    nl = nl->nl_next) {
				// B_FALSE says this won't be CCR table
				if (!is_rgm_objname_valid(nl->nl_name,
				    B_FALSE)) {
					res.err_code = SCHA_ERR_INVAL;
					rgm_format_errmsg(&res,
					    REM_INVALID_ENUM_LIST,
					    rgm_params[i]->p_name);
					return (res);
				}
			}
			rgm_params[i]->p_defaultstr =
			    memstr2str(paramt[i]->defaultstr);
			//
			// validate enum list
			// 'FALSE' as the 2nd argument to 'is_rgm_value_valid'
			// call indicates comma is not a valid character.
			//
			if (!is_rgm_value_valid(rgm_params[i]->p_defaultstr,
			    B_FALSE)) {
				res.err_code = SCHA_ERR_INVAL;
				rgm_format_errmsg(&res,
				    REM_INVALID_PROP_VALUES,
				    rgm_params[i]->p_name);
				return (res);
			}

			break;

		case RTR_STRINGARRAY:
			//
			// When DEFAULT = ; and DEFAULT = "", defaultarraysz
			// is 1 and defaultarray.value is set to RTR_NULL.
			// When no DEFAULT, defaultarraysz is 0.
			// When DEFAULT = <value>, defaultarray.value is set to
			// RTR_STRING and defaultarray.start is not NULL and
			// defaultarraysz is not 0.
			//
			if (paramt[i]->defaultarraysz > 0)
				rgm_params[i]->p_default_isset = B_TRUE;

			rgm_params[i]->p_type = SCHA_PTYPE_STRINGARRAY;
			rgm_params[i]->p_defaultarray =
			    memstrs2namelist(paramt[i]->defaultarray,
			    paramt[i]->defaultarraysz);

			//
			// validate the property values
			// 'FALSE' as the 2nd argument to 'is_rgm_value_valid'
			// call indicates comma is not a valid character.
			//
			for (nl = rgm_params[i]->p_defaultarray; nl != NULL;
			    nl = nl->nl_next) {
				if (!is_rgm_value_valid(nl->nl_name, B_FALSE)) {
					res.err_code = SCHA_ERR_INVAL;
					rgm_format_errmsg(&res,
					    REM_INVALID_PROP_VALUES,
					    rgm_params[i]->p_name);
					return (res);
				}
			}
			break;
		}
		// check for the is_set flags for the following
		// If they are unset fill them with the default
		// values.

		if (paramt[i]->rtr_min_isset == 1) {
			rgm_params[i]->p_min = paramt[i]->rtr_min;
			rgm_params[i]->p_min_isset = B_TRUE;
		} else {
			rgm_params[i]->p_min_isset = B_FALSE;
		}

		if (paramt[i]->rtr_max_isset == 1) {
			rgm_params[i]->p_max = paramt[i]->rtr_max;
			rgm_params[i]->p_max_isset = B_TRUE;
		} else {
			rgm_params[i]->p_max_isset = B_FALSE;
		}
		if (paramt[i]->arraymin_isset == 1) {
			rgm_params[i]->p_arraymin = paramt[i]->arraymin;
			rgm_params[i]->p_arraymin_isset = B_TRUE;
		} else {
			rgm_params[i]->p_arraymin_isset = B_FALSE;
		}

		if (paramt[i]->arraymax_isset == 1) {
			rgm_params[i]->p_arraymax = paramt[i]->arraymax;
			rgm_params[i]->p_arraymax_isset = B_TRUE;
		} else {
			rgm_params[i]->p_arraymax_isset = B_FALSE;
		}

		rgm_params[i]->p_description =
		    memstr2str(paramt[i]->description);

		//
		// validate extension property description
		// 'TRUE' as the 2nd argument to 'is_rgm_value_valid' call
		// indicates comma is a valid character
		//
		if (!is_rgm_value_valid(rgm_params[i]->p_description,
		    B_TRUE)) {
			res.err_code = SCHA_ERR_INVAL;
			rgm_format_errmsg(&res, REM_INVALID_DESC_VALUES,
			    rgm_params[i]->p_name);
			return (res);
		}
	}
	rgm_params[paramtblsz] = (rgm_param_t *)NULL;

	*rgm_params_result = rgm_params;

	return (res);
} //lint !e818

//
// rgm_scrgadm_register_rt
//
// Add a new RT to the RGM configuration.
// Called by scrgadm.c.
//
scha_errmsg_t
rgm_scrgadm_register_rt(rgm_rt_t *rt, boolean_t bypass_install_mode,
    const char *cluster_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;

	// check if the cluster is in install mode
	if (!bypass_install_mode) {
		if ((res = check_installmode()).err_code != SCHA_ERR_NOERR)
			return (res);
	}

	if (rt == NULL || rt->rt_name == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if ((res  = rgm_rt_sanity_check((const rgm_rt_t *)rt)).err_code !=
	    SCHA_ERR_NOERR)
		return (res);

	res = rgmcnfg_create_rttable(rt, cluster_name);
	// Log resource type ADDED event for SC Manager.
	if (res.err_code == SCHA_ERR_NOERR) {

		// No need to contact slaves because slaves will read
		// in new RT on demand via rtname_to_rt().

		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RT_TAG,
		    rt->rt_name);
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that the operator has
		// created a new resource type. This message can be used by
		// system monitoring tools.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, ADDED,
		    SYSTEXT("resource type %s added."), rt->rt_name);
		sc_syslog_msg_done(&handle);
	}

	return (res);
}


//
// Called by scrgadm.c.
//
scha_errmsg_t
rgm_scrgadm_update_rt_instnodes(
	char *rtname, char **inst_nodes, const char *zone_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	char *r_rtname = NULL;
	char *c_name;

	// check if the cluster is in install mode
	if ((res = check_installmode()).err_code != SCHA_ERR_NOERR) {
		return (res);
	}
	if (rtname == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if ((c_name = get_clustername_from_zone(zone_name,
	    &res)) == NULL) {
		return (res);
	}



	// Get the real name for this RT
	res = rgmcnfg_get_rtrealname(rtname, &r_rtname, c_name);

	if (res.err_code != SCHA_ERR_NOERR) {
		free(c_name);
		return (res);
	}

	// ask the President to update the RT installed nodes list
	res = call_scrgadm_update_rt_instnodes((const char *)r_rtname,
	    (const char **)inst_nodes, c_name);

	// Log resource type PROPERTY_CHANGED event for SC Manager.
	if (res.err_code == SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RT_TAG,
		    r_rtname);
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that the operator has
		// edited a property of a resource type. This message can be
		// used by system monitoring tools.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, PROPERTY_CHANGED,
		    SYSTEXT("resource type %s updated."), r_rtname);
		sc_syslog_msg_done(&handle);
	}

	if (r_rtname)
		free(r_rtname);
	free(c_name);
	return (res);
}


//
// Called by scrgadm.c.
//
scha_errmsg_t
rgm_scrgadm_unregister_rt(char *rtname, const char *zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *r_rtname = NULL;
	char *cluster_name;

	// check if the cluster is in install mode
	if ((res = check_installmode()).err_code != SCHA_ERR_NOERR)
		return (res);

	if (rtname == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}
	if ((cluster_name = get_clustername_from_zone(
	    zonename, &res)) == NULL) {
		return (res);
	}

	// Get the real name for this RT
	if ((res = rgmcnfg_get_rtrealname(rtname,
	    &r_rtname, cluster_name)).err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}

	// Ask President to order slaves to remove RT.
	res = call_scrgadm_rt_unregister(r_rtname, cluster_name);

	if (r_rtname)
		free(r_rtname);
	free(cluster_name);
	return (res);
}


scha_errmsg_t
rgm_scrgadm_getrgconf(char *rgname, rgm_rg_t **rg,
    const char *zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *cluster_name;

	if (rgname == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if ((cluster_name = get_clustername_from_zone(zonename,
	    &res)) == NULL)
		return (res);

	res = rgmcnfg_read_rgtable(rgname, rg, cluster_name);
	// Free memory
	if (cluster_name)
		free(cluster_name);

	return (res);
}


scha_errmsg_t
rgm_scrgadm_getrglist(char **managed_rgs[], char **unmanaged_rgs[],
    boolean_t only_local_rgs, const char *zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	namelist_t *m_rglst, *um_rglst;

	*managed_rgs = NULL;
	*unmanaged_rgs = NULL;

	res = rgmcnfg_get_rglist(&m_rglst, &um_rglst, only_local_rgs, zonename);
	if (res.err_code == SCHA_ERR_NOERR) {
		if (m_rglst != NULL) {
			*managed_rgs = nstr2strarray(m_rglst);
			rgm_free_nlist(m_rglst);
		}
		if (um_rglst != NULL) {
			*unmanaged_rgs = nstr2strarray(um_rglst);
			rgm_free_nlist(um_rglst);
		}
	}

	return (res);
}


scha_errmsg_t
rgm_scrgadm_create_rg(char *rgname, scha_property_t **pl,
    boolean_t bypass_install_mode, const char *zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	char *cluster_name = NULL;
	// check if the cluster is in install mode
	if (!bypass_install_mode) {
		if ((res = check_installmode()).err_code != SCHA_ERR_NOERR)
			return (res);
	}

	if (rgname == NULL || pl == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	// B_TRUE says check object name length for CCR table creation
	if (!is_rgm_objname_valid(rgname, B_TRUE)) {
		res.err_code = SCHA_ERR_RG;
		rgm_format_errmsg(&res, REM_RG_NAME_INVALID, rgname);
		return (res);
	}


	if ((cluster_name = get_clustername_from_zone(zonename,
	    &res)) == NULL) {
		return (res);
	}

	res = call_scrgadm_rg_create(rgname, pl, cluster_name);

	// Log resource group ADDED event for SC Manager.
	if (res.err_code == SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RG_TAG,
		    rgname);
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that a new resource
		// group has been added. This message can be used by system
		// monitoring tools.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, ADDED,
		    SYSTEXT("resource group %s added."), rgname);
		sc_syslog_msg_done(&handle);
	}
	free(cluster_name);
	return (res);
}


scha_errmsg_t
rgm_scrgadm_update_rg_property(char *rgname, scha_property_t **pl, uint_t flg,
    const char **rgnames, const char *zone_name)
{
	return (rgm_scrgadm_update_rg_prop_helper(rgname, pl, flg, B_FALSE,
	    rgnames, zone_name));
}

scha_errmsg_t
rgm_scrgadm_update_rg_prop_no_validate(char *rgname, scha_property_t **pl,
    uint_t flg, const char *zone_name)
{
	return (rgm_scrgadm_update_rg_prop_helper(rgname, pl, flg, B_TRUE,
	    NULL, zone_name));
}

static scha_errmsg_t
rgm_scrgadm_update_rg_prop_helper(char *rgname, scha_property_t **pl,
    uint_t flg, boolean_t suppress_validate, const char **rgnames,
    const char *zone_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	char *cluster_name = NULL;

	// check if the cluster is in install mode
	if ((res = check_installmode()).err_code != SCHA_ERR_NOERR)
		return (res);

	if (rgname == NULL || pl == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if ((cluster_name = get_clustername_from_zone(
	    zone_name, &res)) == NULL) {
		return (res);
	}
	res = call_scrgadm_rg_update_prop((const char *)rgname,
	    (const scha_property_t **)pl, flg, suppress_validate, rgnames,
	    cluster_name);

	// Log resource group PROPERTY_CHANGED event for SC Manager.
	if (res.err_code == SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RG_TAG,
		    rgname);
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that the operator has
		// edited a property of a resource group. This message can be
		// used by system monitoring tools.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, PROPERTY_CHANGED,
		    SYSTEXT("resource group %s property changed."), rgname);
		sc_syslog_msg_done(&handle);
	}
	free(cluster_name);
	return (res);
}


scha_errmsg_t
rgm_scrgadm_remove_rg(char *rgname, boolean_t v_flag,
    const char *zone_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	char *cluster_name = NULL;

	// check if the cluster is in install mode
	if ((res = check_installmode()).err_code != SCHA_ERR_NOERR)
		return (res);

	if (rgname == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}
	if ((cluster_name = get_clustername_from_zone(zone_name,
	    &res)) == NULL) {
		return (res);
	}

	res = call_scrgadm_rg_remove(rgname, v_flag, cluster_name);

	// Log resource group REMOVED event for SC Manager.
	if (res.err_code == SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RG_TAG,
		    rgname);
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that the operator has
		// deleted a resource group. This message can be used by
		// system monitoring tools.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, REMOVED,
		    SYSTEXT("resource group %s removed."), rgname);
		sc_syslog_msg_done(&handle);
	}
	free(cluster_name);
	return (res);
}


scha_errmsg_t
rgm_scrgadm_getrsrcconf(char *resourcename, rgm_resource_t **resource,
    const char *zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *rgname;

	if (resourcename == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	res = rgm_rsrc_on_which_rg(resourcename, &rgname, (char *)zonename);
	if (rgname == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		return (res);
	}

	res = rgmcnfg_get_resource(resourcename, rgname, resource, zonename);
	if (rgname)
		free(rgname);
	return (res);
}


//
// rgm_scrgadm_getrsrclist()
// copy list of resourcenames as a string array into rss. If rgname is
// specified, all resources of the specified rg are listed, otherwise all rgs
// are taken into account.
//
scha_errmsg_t
rgm_scrgadm_getrsrclist(char *rgname, char **rss[],
    const char *zonename)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};
	namelist_t	*rgs[2];
	uint_t		rsnames_size = 1; // for '\0'
	char		*rsnames = (char *)calloc(1, rsnames_size);
	boolean_t	need_free = B_FALSE;
	namelist_t	rglist;

	if (rsnames == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}
	rgs[0] = rgs[1] = NULL;
	rsnames[0] = '\0';

	if (rgname != NULL) {
		// create a single element list for the rgname passed.

		rglist.nl_name = rgname;
		rglist.nl_next = NULL;

		rgs[0] = &rglist;
	} else {
		need_free = B_TRUE;
		// consider all rgs, if none is specified.
		res = rgmcnfg_get_rglist(&rgs[0], &rgs[1], B_FALSE, zonename);
		if (res.err_code != SCHA_ERR_NOERR) {
			goto error_out;
		}
	}

	// the rg lists are stored in rgs[0], and rgs[1]
	for (int i = 0; i <= 1; i++) {
		namelist_t *ptr;
		char *data = NULL;
		char *tmp_rsnames;
		for (ptr = rgs[i]; ptr != NULL; ptr = ptr->nl_next) {
			res = get_property_value(ptr->nl_name, O_RG,
				SCHA_RESOURCE_LIST, &data, zonename);
			if (res.err_code != SCHA_ERR_NOERR) {
				res.err_code = SCHA_ERR_RG;
				goto error_out;
			}
			if (data == NULL || *data == '\0') {
				delete [] data;
				data = NULL;
				continue;
			}
			rsnames_size += strlen(data) + 1;
			tmp_rsnames = (char *)realloc(rsnames, rsnames_size);

			if (tmp_rsnames == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				delete [] data;
				data = NULL;
				goto error_out;
			}
			rsnames = tmp_rsnames;
			if (rsnames[0] != '\0') {
				// already contains some entry
				(void) strcat(rsnames, ",");
			}
			(void) strcat(rsnames, data);
			delete [] data;
			data = NULL;
		}
	}

	// str2strarray() calls my_strtok() which can changes the string
	*rss = str2strarray(rsnames);

error_out:
	free(rsnames);
	if (need_free) {
		rgm_free_nlist(rgs[0]);
		rgm_free_nlist(rgs[1]);
	}
	return (res);
}


#ifdef SC_SRM
scha_errmsg_t
rgm_scrgadm_get_default_proj(char *rg_name, char **proj_name,
const char *zonename)
{
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	if (rg_name == NULL) {
		*proj_name = strdup_nocheck(RGM_DEFAULT_PROJ);
		if (*proj_name == NULL)
			res.err_code = SCHA_ERR_NOMEM;

		return (res);
	}

	res = get_property_value(rg_name, O_RG, SCHA_RG_PROJECT_NAME,
	    proj_name, zonename);
	if (res.err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			// RG was created in Solaris 8.
			// RG_project doesn't exist in CCR.
			// Return the system default project "default"
			res.err_code = SCHA_ERR_NOERR;
			*proj_name = strdup_nocheck(RGM_DEFAULT_PROJ);
			if (*proj_name == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				return (res);
			}
		}
		return (res);
	}

	if (*proj_name == NULL || **proj_name == '\0') {
		// RG project name is not set in CCR
		// Return the system default project "default"
		*proj_name = strdup_nocheck(RGM_DEFAULT_PROJ);
		if (*proj_name == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}
	}

	return (res);
}
#endif


// SC SLM addon start
scha_errmsg_t
rgm_scrgadm_get_rg_slm_type(char *rg_name, char **slm_type,
const char *zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (rg_name == NULL) {
		res.err_code = SCHA_ERR_RG;
		return (res);
	}

	res = get_property_value(rg_name, O_RG, SCHA_RG_SLM_TYPE,
	    slm_type, (char *)zonename);
	if (res.err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			// RG was created before SLM existed in SC.
			// RG_SLM_type doesn't exist in CCR.
			// Return the system default : "manual"
			res.err_code = SCHA_ERR_NOERR;
			*slm_type = strdup_nocheck(SCHA_RG_SLM_TYPE);
			if (*slm_type == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				return (res);
			}
		}
		return (res);
	}

	if (*slm_type == NULL || **slm_type == '\0') {
		// RG slm type is not set in CCR
		// Return the system default : "manual"
		*slm_type = strdup_nocheck(SCHA_RG_SLM_TYPE);
		if (*slm_type == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}
	}

	return (res);
}


scha_errmsg_t
rgm_scrgadm_get_rg_slm_pset_type(char *rg_name, char **slm_pset_type,
    const char *zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (rg_name == NULL) {
		res.err_code = SCHA_ERR_RG;
		return (res);
	}

	res = get_property_value(rg_name, O_RG,
	    SCHA_RG_SLM_PSET_TYPE, slm_pset_type, (char *)zonename);
	if (res.err_code != SCHA_ERR_NOERR) {
		if (res.err_code == SCHA_ERR_PROP) {
			// RG was created before SLM existed in SC.
			// RG_SLM_pset_type doesn't exist in CCR.
			// Return the system default : "default"
			res.err_code = SCHA_ERR_NOERR;
			*slm_pset_type = strdup_nocheck(SCHA_RG_SLM_PSET_TYPE);
			if (*slm_pset_type == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				return (res);
			}
		}
		return (res);
	}

	if (*slm_pset_type == NULL || **slm_pset_type == '\0') {
		// RG slm type is not set in CCR
		// Return the system default : "default"
		*slm_pset_type = strdup_nocheck(SCHA_RG_SLM_PSET_TYPE);
		if (*slm_pset_type == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			return (res);
		}
	}

	return (res);
}
// SC SLM addon end


//
// rgm_scrgadm_check_resource_prop() - will be implemented later
//
scha_errmsg_t
rgm_scrgadm_check_resource_prop(const rgm_rt_t *rt, const char *prop_name,
	const  char *value)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (rt == NULL || prop_name == NULL || value == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	res.err_code = SCHA_ERR_NOERR;
	return (res);
}

scha_errmsg_t
rgm_scrgadm_add_resource(char *rname, char *rtname,
	char *rgname, scha_resource_properties_t *rpl,
	boolean_t bypass_install_mode, const char *zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	char *r_rtname = NULL;
	char *cluster_name;

	// check if the cluster is in install mode
	if (!bypass_install_mode) {
		if ((res = check_installmode()).err_code != SCHA_ERR_NOERR)
			return (res);
	}

	if (rname == NULL || rtname == NULL || rgname == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	// B_FALSE says this won't be CCR table
	if (!is_rgm_objname_valid(rname, B_FALSE)) {
		res.err_code = SCHA_ERR_RSRC;
		rgm_format_errmsg(&res, REM_RS_NAME_INVALID, rname);
		return (res);
	}
	if ((cluster_name = get_clustername_from_zone(zonename, &res))
	    == NULL) {
		return (res);
	}

	// Get the real name for this RT
	res = rgmcnfg_get_rtrealname(rtname, &r_rtname, cluster_name);

	if (res.err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}

	res = call_scrgadm_rs_add((const char *)rname, (const char *)r_rtname,
	    (const char *)rgname, rpl, cluster_name);
	if (r_rtname)
		free(r_rtname);

	// Log resource ADDED event for SC Manager.
	if (res.err_code == SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RS_TAG,
		    rname);
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that the operator has
		// created a new resource. This message can be used by system
		// monitoring tools.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, ADDED,
		    SYSTEXT("resource %s added."), rname);
		sc_syslog_msg_done(&handle);
	}

	free(cluster_name);
	return (res);
}


scha_errmsg_t
rgm_scrgadm_update_resource_property(char *rname,
    scha_resource_properties_t *rpl, const char *zonename) //lint !e818
{
	return (rgm_scrgadm_update_resource_property_helper(rname,
	    (const scha_resource_properties_t *)rpl,
	    B_FALSE, zonename));
} //lint !e818


scha_errmsg_t
rgm_scrgadm_update_rs_prop_no_validate(char *rname,
    scha_resource_properties_t *rpl, const char *zonename) //lint !e818
{
	return (rgm_scrgadm_update_resource_property_helper(rname,
	    (const scha_resource_properties_t *)rpl,
	    B_TRUE, zonename));
} //lint !e818


static scha_errmsg_t
rgm_scrgadm_update_resource_property_helper(char *rname,
    const scha_resource_properties_t *rpl, boolean_t suppress_validate,
    const char *zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;

	// check if the cluster is in install mode
	if ((res = check_installmode()).err_code != SCHA_ERR_NOERR)
		return (res);

	if (rname == NULL || rpl == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if (rpl == NULL ||
	    (rpl->srp_predefined == NULL && rpl->srp_extension == NULL)) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	char *cluster_name = NULL;
	if ((cluster_name = get_clustername_from_zone(zonename,
	    &res)) == NULL) {
		return (res);
	}
	res = call_scrgadm_rs_update_prop((const char *)rname,
	    (const scha_resource_properties_t *)rpl, suppress_validate,
	    cluster_name);

	// Log resource PROPERTY_CHANGED event for SC Manager.
	if (res.err_code == SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RS_TAG,
		    rname);
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that a resource's
		// property has been edited by the cluster administrator. This
		// message can be used by system monitoring tools.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, PROPERTY_CHANGED,
		    SYSTEXT("resource %s property changed."), rname);
		sc_syslog_msg_done(&handle);
	}
	free(cluster_name);
	return (res);
}


scha_errmsg_t
rgm_scrgadm_delete_resource(char *resourcename, boolean_t verbose_flag,
    const char *zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	sc_syslog_msg_handle_t handle;
	char *cluster_name;

	// check if the cluster is in install mode
	if ((res = check_installmode()).err_code != SCHA_ERR_NOERR)
		return (res);

	if (resourcename == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if ((cluster_name = get_clustername_from_zone(zonename,
	    &res)) == NULL) {
		return (res);
	}
	res = call_scrgadm_rs_delete(resourcename, verbose_flag,
	    cluster_name);

	// Log resource REMOVED event for SC Manager.
	if (res.err_code == SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RS_TAG,
		    resourcename);
		//
		// SCMSGS
		// @explanation
		// This is a notification from the rgmd that the operator has
		// deleted a resource. This message can be used by system
		// monitoring tools.
		// @user_action
		// This is an informational message; no user action is needed.
		//
		(void) sc_syslog_msg_log(handle, LOG_NOTICE, REMOVED,
		    SYSTEXT("resource %s removed."), resourcename);
		sc_syslog_msg_done(&handle);
	}
	free(cluster_name);
	return (res);
}


scha_errmsg_t
call_scrgadm_rg_create(const char *rg_name, scha_property_t **pl,
    const char *cluster_name)
{
	rgm::idl_rg_create_args		arg;
	rgm::idl_regis_result_t_var	result_v;
	rgm::rgm_comm_var prgm_pres_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;


#if SOL_VERSION >= __s10
	if (strcmp(cluster_name, GLOBAL_ZONENAME) == 0) {
		// this check is only for global zone cluster which
		// can host native type zones.
		if (sc_nv_zonescheck() != 0) {
			res.err_code = SCHA_ERR_ACCESS;
			rgm_format_errmsg(&res, REM_RG_NGZONE);
			return (res);
		}
	}
#endif
	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc((char *)cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		return (res);
	}

	arg.idlstr_rg_name = new_str(rg_name);

	plist_to_nv_seq((const scha_property_t **)pl, arg.prop_val);

	/* set the msg locale */
	arg.locale = new_str(fetch_msg_locale());
	prgm_pres_v->idl_scrgadm_rg_create(arg, result_v, e);
	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
		return (res);

	res.err_code = (scha_err_t)result_v->ret_code;
	if ((const char *)result_v->idlstr_err_msg)
		res.err_msg = strdup(result_v->idlstr_err_msg);
	return (res);
}

scha_errmsg_t
rgm_rg_update_props_v1(const char *rgname,
    const scha_property_t **pl, uint_t flg, boolean_t suppress_validate,
    const char **rgnames, const char *cluster_name) {

	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rgm::idl_regis_result_t_var	result_v;
	rgm::rgm_comm_var prgm_pres_v;

	Environment e;
	char *zname;
	rgm::idl_string_seq_t rg_names_seq;

	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc((char *)cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		return (res);
	}
	/*
	 * idl_rg_update_prop takes a flag (caller_flag) which
	 * tells the rgmd where did the command originate (it was
	 * scswitch or scrgadm), so that it can decide whether to
	 * allow the command to proceed or not.
	 */
	rgm::idl_rg_update_prop_args_v1 args;

#if SOL_VERSION >= __s10
	res = populate_zone_cred(&(args.nodeid), &zname);
	if (res.err_code != SCHA_ERR_NOERR)
		return (res);
	args.idlstr_zonename = zname;
#endif

	if (flg == FROM_SCRGADM)
		args.caller_flag = rgm::RGM_FROM_SCRGADM;
	else
		args.caller_flag = rgm::RGM_FROM_SCSWITCH;
	args.idlstr_rg_name = new_str(rgname);
	plist_to_nv_seq((const scha_property_t **)pl, args.prop_val);
	/* Set the msg locale */
	args.locale = new_str(fetch_msg_locale());
	/* Set the option flags */
	args.flags = 0;
	if (suppress_validate) {
		args.flags |= rgm::SUPPRESS_RG_VALIDATION;
	}

	array_to_strseq(rgnames, args.rglist);

	prgm_pres_v->idl_scrgadm_rg_update_prop_v1(args, result_v, e);
	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
		return (res);

	res.err_code = (scha_err_t)result_v->ret_code;
	if ((const char *)result_v->idlstr_err_msg)
		res.err_msg = strdup(result_v->idlstr_err_msg);


	return (res);
}

scha_errmsg_t
rgm_rg_update_props_v0(const char *rgname,
    const scha_property_t **pl, uint_t flg, boolean_t suppress_validate,
    const char *cluster_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rgm::idl_regis_result_t_var	result_v;
	rgm::rgm_comm_var prgm_pres_v;

	Environment e;
	char *zname;

	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc((char *)cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		return (res);
	}

	/*
	 * idl_rg_update_prop takes a flag (caller_flag) which
	 * tells the rgmd where did the command originate (it was
	 * scswitch or scrgadm), so that it can decide whether to
	 * allow the command to proceed or not.
	 */

	rgm::idl_rg_update_prop_args args;

#if SOL_VERSION >= __s10
	res = populate_zone_cred(&(args.nodeid), &zname);
	if (res.err_code != SCHA_ERR_NOERR)
		return (res);
	args.idlstr_zonename = zname;
#endif

	if (flg == FROM_SCRGADM)
		args.caller_flag = rgm::RGM_FROM_SCRGADM;
	else
		args.caller_flag = rgm::RGM_FROM_SCSWITCH;
	args.idlstr_rg_name = new_str(rgname);
	plist_to_nv_seq((const scha_property_t **)pl, args.prop_val);
	/* Set the msg locale */
	args.locale = new_str(fetch_msg_locale());
	/* Set the option flags */
	args.flags = 0;
	if (suppress_validate) {
		args.flags |= rgm::SUPPRESS_RG_VALIDATION;
	}

	prgm_pres_v->idl_scrgadm_rg_update_prop(args, result_v, e);
	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
		return (res);

	res.err_code = (scha_err_t)result_v->ret_code;
	if ((const char *)result_v->idlstr_err_msg)
		res.err_msg = strdup(result_v->idlstr_err_msg);

	return (res);
}

scha_errmsg_t
call_scrgadm_rg_update_prop(const char *rgname,
    const scha_property_t **pl, uint_t flg, boolean_t suppress_validate,
    const char **rgnames, const char *cluster_name)
{
	clcomm_vp_version_t rgm_version;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (clcomm_get_running_version("rgm", &rgm_version)
	    == 0) {
		if (rgm_version.major_num >= 2)
			res = rgm_rg_update_props_v1(rgname, pl, flg,
			    suppress_validate, rgnames, cluster_name);
		else
			res = rgm_rg_update_props_v0(rgname, pl, flg,
			    suppress_validate, cluster_name);
	} else {
		res = rgm_rg_update_props_v0(rgname, pl, flg,
		    suppress_validate, cluster_name);
	}
	return (res);
}


scha_errmsg_t
call_scrgadm_rg_remove(const char *rgname, boolean_t v_flag,
    const char *cluster_name)
{
	rgm::idl_rg_remove_args		arg;
	rgm::idl_regis_result_t_var	result_v;
	rgm::rgm_comm_var prgm_pres_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;

#if SOL_VERSION >= __s10
	if (strcmp(cluster_name, GLOBAL_ZONENAME) == 0) {
		// this check is only for global zone cluster which
		// can host native type zones.
		if (sc_nv_zonescheck() != 0) {
			res.err_code = SCHA_ERR_ACCESS;
			rgm_format_errmsg(&res, REM_RG_NGZONE);
			return (res);
		}
	}
#endif

	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc((char *)cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		return (res);
	}

	arg.idlstr_rg_name = new_str(rgname);

	/* Set the msg locale */
	arg.locale = new_str(fetch_msg_locale());

	prgm_pres_v->idl_scrgadm_rg_remove(arg, result_v, (bool)v_flag, e);
	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
		return (res);

	res.err_code = (scha_err_t)result_v->ret_code;
	if ((const char *)result_v->idlstr_err_msg)
		res.err_msg = strdup(result_v->idlstr_err_msg);
	return (res);
}


static scha_errmsg_t
call_scrgadm_rs_add(const char *rname, const char *rtname,
    const char *rgname, scha_resource_properties_t *rpl,
    const char *cluster_name)
{
	rgm::idl_rs_add_args		arg;
	rgm::idl_regis_result_t_var	result_v;
	rgm::rgm_comm_var prgm_pres_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	char *zname;
	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc((char *)cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		return (res);
	}

	arg.idlstr_rg_name = new_str(rgname);
	arg.idlstr_rs_name = new_str(rname);
	arg.idlstr_rt_name = new_str(rtname);

#if SOL_VERSION >= __s10
	res = populate_zone_cred(&(arg.nodeid), &zname);
	if (res.err_code != SCHA_ERR_NOERR)
		return (res);
	arg.idlstr_zonename = zname;
#endif


	if (rpl == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if (rpl->srp_predefined != NULL) {
		// Marshal property list into arg.prop_val
		plist_to_nv_seq(
		    (const scha_property_t **)(rpl->srp_predefined),
		    arg.prop_val);
	}

	if (rpl->srp_extension != NULL) {
		// Marshal extension property list into arg.ext_prop_val
		plist_to_nv_seq_ext(
		    (const scha_property_t **)(rpl->srp_extension),
		    arg.ext_prop_val);
	}

	/* Set the msg locale */
	arg.locale = new_str(fetch_msg_locale());

	prgm_pres_v->idl_scrgadm_rs_add(arg, result_v, e);
	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
		return (res);

	res.err_code = (scha_err_t)result_v->ret_code;
	if ((const char *)result_v->idlstr_err_msg)
		res.err_msg = strdup(result_v->idlstr_err_msg);
	return (res);
} //lint !e818


scha_errmsg_t
call_scrgadm_rs_update_prop(const char *rname, const
    scha_resource_properties_t *rpl,
    boolean_t suppress_validate, const char *cluster_name)
{
	rgm::idl_rs_update_prop_args		arg;
	rgm::idl_regis_result_t_var		result_v;
	rgm::rgm_comm_var prgm_pres_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	char *zname;

	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc((char *)cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		return (res);
	}

#if SOL_VERSION >= __s10
	res = populate_zone_cred(&(arg.nodeid), &zname);
	if (res.err_code != SCHA_ERR_NOERR)
		return (res);
	arg.idlstr_zonename = zname;
#endif
	arg.idlstr_rs_name = new_str(rname);

	if (rpl == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if (rpl->srp_predefined != NULL) {
		plist_to_nv_seq(
		    (const scha_property_t **)(rpl->srp_predefined),
		    arg.prop_val);
	}

	if (rpl->srp_extension != NULL) {
		plist_to_nv_seq_ext(
		    (const scha_property_t **)(rpl->srp_extension),
		    arg.ext_prop_val);
	}

	/* Set the msg locale */
	arg.locale = new_str(fetch_msg_locale());

	/* Set the option flags */
	arg.flags = 0;
	if (suppress_validate) {
		arg.flags |= rgm::SUPPRESS_VALIDATION;
	}

	prgm_pres_v->idl_scrgadm_rs_update_prop(arg, result_v, e);
	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
		return (res);

	res.err_code = (scha_err_t)result_v->ret_code;
	if ((const char *)result_v->idlstr_err_msg)
		res.err_msg = strdup(result_v->idlstr_err_msg);
	return (res);
}


scha_errmsg_t
call_scrgadm_rs_delete(const char *rname, boolean_t verbose_flag,
    const char *cluster_name)
{
	rgm::idl_rs_delete_args		arg;
	rgm::idl_regis_result_t_var	result_v;
	rgm::rgm_comm_var prgm_pres_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	char *zname;

	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc((char *)cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		return (res);
	}

	arg.idlstr_rs_name = new_str(rname);

	/* Set the msg locale */
	arg.locale = new_str(fetch_msg_locale());

#if SOL_VERSION >= __s10
	res = populate_zone_cred(&(arg.nodeid), &zname);
	if (res.err_code != SCHA_ERR_NOERR)
		return (res);
	arg.idlstr_zonename = zname;
#endif
	prgm_pres_v->idl_scrgadm_rs_delete(arg, result_v,
	    (bool)verbose_flag, e);
	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
		return (res);

	res.err_code = (scha_err_t)result_v->ret_code;
	if ((const char *)result_v->idlstr_err_msg)
		res.err_msg = strdup(result_v->idlstr_err_msg);
	return (res);
}


scha_errmsg_t
rgm_rt_sanity_check(const rgm_rt_t *rt)
{
	nodeidlist_t	*ptr0, *ptr1;
	sc_syslog_msg_handle_t handle;
	scha_errmsg_t	res = {SCHA_ERR_NOERR, NULL};

	if (rt->rt_api_version < MIN_SCHA_API_VERSION) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_API_VERSION_INVALID,
		    rt->rt_api_version);
		return (res);
	}
	if (!IS_SUPPORTED_SCHA_API_VERSION(rt->rt_api_version)) {
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_API_VERSION_LOW,
		    rt->rt_api_version);
		return (res);
	}

	// so far only the name dup check is done for installed nodelist in RT
	if (!rt->rt_instl_nodes.is_ALL_value) {
		boolean_t do_strcmp;
		// If is_ALL_value turns on, the names isn't meaningful.
		for (ptr0 = rt->rt_instl_nodes.nodeids; ptr0;
		    ptr0 = ptr0->nl_next) {
			for (ptr1 = ptr0->nl_next; ptr1;
			    ptr1 = ptr1->nl_next) {
				do_strcmp = B_TRUE;
				if (ptr0->nl_nodeid != ptr1->nl_nodeid)
					continue;
				if (ptr0->nl_zonename == NULL ||
				    ptr1->nl_zonename == NULL) {
					/* atleast one zonename is NULL */
					do_strcmp = B_FALSE;
					if (ptr0->nl_zonename != NULL ||
					    ptr1->nl_zonename != NULL)
						/* one null and one nonnull */
						continue;
				}
				/* either both null or both non null */
				if (do_strcmp && strcmp(ptr0->nl_zonename,
				    ptr1->nl_zonename) != 0)
					continue;

				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RT_TAG, rt->rt_name);
				// syslog: nodename -> LN

				//
				// SCMSGS
				// @explanation
				// User has defined duplicated installed node
				// name when creating resource type.
				// @user_action
				// Recheck the installed nodename list and
				// make sure there is no nodename duplication.
				//
				(void) sc_syslog_msg_log(handle,
				    LOG_NOTICE, ADDED, SYSTEXT(
				    "Duplicated installed nodename "
				    "when Resource Type <%s> is "
				    "added."), rt->rt_name);
				sc_syslog_msg_done(&handle);
				res.err_code = SCHA_ERR_DUPNODE;
				return (res);

			}
		}
	}
	return (res);
}


//
// rt_registered
//
// Returns TRUE if there is already an RT with the same vendor id, name
// and version registered.
//
// We need this routine to prevent the second registration in each of
// these scenarios from succeeding:
// A.
// SUNW.phoney with version 2.0 which was registered in 3.0 and
// SUNW.phoney:2.0 whose RTR file is 3.1-style and which is being
// registered in 3.1 (should fail)
//
// B.
// SUNW.phoney:2.0 which was registered in 3.1 and
// SUNW.phoney which is being registered also in 3.1 and whose version
// is also 2.0 but whose RTR file is 3.0-style (should fail)
//
scha_errmsg_t
rt_registered(char *vendor_id, char *RT_name,
    const char *new_version, boolean_t *retval, const char *zonename)
{
	scha_errmsg_t status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = NULL;
	char **rt_namep;
	char *newname_novers;
	rgm_rt_t *rt = NULL;
	size_t len, len2;

	*retval = B_FALSE;

	/* Get the list of all rt names */
	status = rgm_scrgadm_getrtlist(&rt_names, zonename);
	if (status.err_code != SCHA_ERR_NOERR) {
		status.err_code = SCHA_ERR_INTERNAL;
		rgm_format_errmsg(&status, REM_INTERNAL);
		return (status);
	}
	if (rt_names == NULL)
		// there are no registered RTs at this time
		return (status);

	len = strlen(RT_name);
	if (vendor_id)
		// + 1 for the dot
		len += strlen(vendor_id) + 1;
	if ((newname_novers = (char *)malloc_nocheck(len+1)) == NULL) {
		status.err_code = SCHA_ERR_NOMEM;
		return (status);
	}

	if (vendor_id == NULL || vendor_id[0] == '\0') {
		// no vendor_id was specified

		// the name as it might appear in the CCR w/o version
		(void) sprintf(newname_novers, "%s", RT_name);
	} else {
		// vendor_id was specified

		// the name as it might appear in the CCR w/o version
		(void) sprintf(newname_novers, "%s.%s", vendor_id, RT_name);
	}

	//
	// Look through the list of CCR tables for a table name containing
	// the components <vendor_id>.<RT_name>, and optionally a version
	// suffix.
	//
	// If we find one, check the proposed version
	// with the stored version.  If they match, return TRUE.
	//
	for (rt_namep = rt_names; rt_namep && *rt_namep; rt_namep++) {

		//
		// Compare just the portion of the name BEFORE the
		// version suffix.  (There may not be a version suffix.)
		// SUNW.phone must not match SUNW.phoney, so check the
		// length as well!
		//
		if ((len2 = strcspn(*rt_namep, (const char *)":")) == 0)
			// no version suffix; use length of whole table name
			len2 = strlen(*rt_namep);

		if (len != len2)
			// no match
			continue;

		// The portion before the version suffix is the same length,
		// so they might match.
		if (strncmp(newname_novers, *rt_namep, len) != 0)
			continue;
			// no match
		// We found a CCR table containing the components
		// <vendor_id>.<RT_name>
		// Read the existing table and check the RT_version property.
		status = rgmcnfg_read_rttable(*rt_namep, &rt, zonename);
		if (status.err_code != SCHA_ERR_NOERR) {
			// rt doesn't need to be freed in this case
			rgm_free_strarray(rt_names);
			free(newname_novers);
			status.err_code = SCHA_ERR_INTERNAL;
			rgm_format_errmsg(&status, REM_INTERNAL);
			return (status);
		}

		if ((new_version == NULL || new_version[0] == '\0') &&
		    (rt->rt_version == NULL || rt->rt_version[0] == '\0')) {
			// Both versions are NULL/EMPTY.
			*retval = B_TRUE;
			break;
		}

		if (new_version && rt->rt_version) {
			if (strcmp(new_version, rt->rt_version) == 0) {
				// Version in table matches that in RTR file.
				// Fail the registration.
				*retval = B_TRUE;
				break;
			}
		}
		// we found an RT with the same name, but the version
		// number didn't match so it's OK to register this RT
		break;
	}

	free(newname_novers);
	rgm_free_strarray(rt_names);
	rgm_free_rt(rt);

	return (status);
}

scha_errmsg_t
rgm_scrgadm_convert_namelist_to_nodeidlist(namelist_t *namelist_in,
    nodeidlist_t **nodeidlist_out)
{
	return (convert_namelist_to_nodeidlist(namelist_in, nodeidlist_out));
}

#if SOL_VERSION >= __s10
scha_errmsg_t
rgm_scrgadm_convert_zc_namelist_to_nodeidlist(namelist_t *namelist_in,
    nodeidlist_t **nodeidlist_out, char *zc_name)
{
	return (convert_zc_namelist_to_nodeidlist(namelist_in,
			nodeidlist_out, zc_name));
}
#endif
