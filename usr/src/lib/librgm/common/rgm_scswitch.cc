/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_scswitch.cc	1.60	08/06/20 SMI"


//
// rgm_scswitch.cc
//
// This file contains functions called by scswitch(1M) to
// perform scswitch -z, -u, -o, -e, -n, -c -f.
//

#include <orb/fault/fault_injection.h>
#include <rgm/rgm_comm_impl.h>
#include <rgm/rgm_cmn_proto.h>
#include <rgm/rgm_scswitch.h>
#include <rgm/rgm_errmsg.h>
#include <rgm/scha_priv.h>


//
// rgm_scswitch_switch_rg
//
// If rg_action is RGACTION_NONE switches over RG(s) to the specified nodes.
//
// 	scswitch -z -h <node> -g <RG>
//
// If rg_action is RGACTION_EVACUATE, then newmaster_list contains a node to be
// evacuated rather than the new masters.  In this case, rg_list is ignored.
// Resource groups are prevented from failing over onto the evacuating node
// during the evacuation and for a period of n seconds after the evacuation
// is complete, where 'n' is specified by the evac_timeout parameter.
// Currently, evac_timeout is a ushort_t thus is limited to a value between
// zero and USHRT_MAX (65535). For any other rg_action, evac_timeout is
// ignored.
//
// If rg_action is RGACTION_REBALANCE, then rg_list contains a list of RGs that
// are to be rebalanced, i.e. brought online on their most-preferred masters
// while attempting to satisfy Desired_primaries and RG affinities.  In
// this case, newmaster_list is ignored.  If rg_action is RGACTION_REBALANCE
// and rg_list is empty, then all managed resource groups are rebalanced.
//
// If rg_action is RGACTION_ONLINE, then rg_list contains a list of RGs that
// are to be brought online on the nodelist specified without
// impacting the state of RG on the nodes not specified in the nodelist.
// If the maximum primaries for the RG would be exceeded this command
// will fail.
//
// If rg_action is RGACTION_OFFLINE, then rg_list contains a list of RGs that
// are to be brought offline on the nodelist specified without
// impacting the state of RG on the nodes not specified in the nodelist.
//
// If rg_action is RGACTION_SWITCHBACK, then rg_list contains a list of RGs
// that are to be remastered on their most preferred primaries.
//
// v_flag is used by the newcli for the verbose print.
//
scha_errmsg_t
rgm_scswitch_switch_rg(const char **newmaster_list, const char **rg_list,
    switch_rg_action_t rg_action, ushort_t evac_timeout,
    boolean_t v_flag, const char *zonename)
{
	rgm::rgm_comm_var prgm_pres_v;
	Environment e;
	rgm::idl_scswitch_primaries_args scswp_args;
	rgm::idl_regis_result_t_var scswp_res_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *zname = NULL;
	char *cluster_name = NULL;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);
	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc(cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	// If getpres returns nil, no RGM president has yet registered with
	// the naming service.
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		free(cluster_name);
		return (res);
	}

	//
	// Marshal node names into scswp_args.
	// A NULL newmaster_list value is accepted as indicating an empty list.
	//
	array_to_strseq(newmaster_list, scswp_args.node_names);

	//
	// Marshal RG names into scswp_args.
	// A NULL rg_list value is accepted as indicating an empty list.
	//
	array_to_strseq(rg_list, scswp_args.RG_names);

	/* Get the msg locale */
	scswp_args.locale = new_str(fetch_msg_locale());

#if SOL_VERSION >= __s10
	res = populate_zone_cred(&(scswp_args.nodeid), &zname);
	if (res.err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}
	scswp_args.idlstr_zonename = zname;
#endif
	/* Set the flags */
	scswp_args.flags = 0;
	if (rg_action == RGACTION_EVACUATE) {
		scswp_args.flags |= rgm::NODE_EVACUATE;
		scswp_args.flags |= SET_EVAC_STICKY_TIMEOUT(evac_timeout);
	} else if (rg_action == RGACTION_REBALANCE) {
		scswp_args.flags |= rgm::REBALANCE;
	} else if (rg_action == RGACTION_CLUSTER_SHUTDOWN) {
		scswp_args.flags |= rgm::SHUTDOWN;
	} else if (rg_action == RGACTION_ONLINE) {
		scswp_args.flags |= rgm::ONLINE;
	} else if (rg_action == RGACTION_OFFLINE) {
		scswp_args.flags |= rgm::OFFLINE;
	} else if (rg_action == RGACTION_SWITCH_BACK) {
		scswp_args.flags |= rgm::SWITCH_BACK;
	}
	prgm_pres_v->idl_scswitch_primaries(scswp_args, scswp_res_v,
	    (bool)v_flag, e);

	// scswp_args was allocated on stack, so do not need to free

	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}
	res.err_code = (scha_err_t)scswp_res_v->ret_code;
	if ((const char *)scswp_res_v->idlstr_err_msg)
		res.err_msg = strdup(scswp_res_v->idlstr_err_msg);
	free(cluster_name);
	return (res);
}

/*
 * scswitch -v -Q
 * calls idl_scswitch_quiesce_rgs
 * verbose_flag is used by the newcli for the verbose print.
 */

scha_errmsg_t
rgm_scswitch_quiesce_rgs(const char **rg_list,
	const char **resource_list, uint_t kill_flg,
	boolean_t verbose_flag, const char *zonename)
{
	rgm::rgm_comm_var prgm_pres_v;
	Environment e;
	rgm::idl_quiesce_rgs_args scq_args;
	rgm::idl_regis_result_t_var scq_res_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *zname = NULL;
	char *cluster_name = NULL;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);
	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc(cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();

	// If getpres returns nil, no RGM president has yet registered with
	// the naming service.
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		free(cluster_name);
		return (res);
	}

	//
	// Marshal resource names into scq_args.
	// Currently the list is always NULL. if kill_flg is set, all the
	// resources (of the resourcegroups specified) will be fast-quiesced.
	//
	array_to_strseq(resource_list, scq_args.resource_names);

	//
	// Marshal RG names into scq_args.
	// A NULL list means all rgs configured on the system will
	// be quiesced
	//
	array_to_strseq(rg_list, scq_args.RG_names);

	/* Get the msg locale */
	scq_args.locale = new_str(fetch_msg_locale());

#if SOL_VERSION >= __s10
	res = populate_zone_cred(&(scq_args.nodeid), &zname);
	if (res.err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}
	scq_args.idlstr_zonename = zname;
#endif

	/* Set the flags */
	scq_args.flags = 0;
	if (kill_flg)
		scq_args.flags |= rgm::FAST_QUIESCE;

	prgm_pres_v->idl_quiesce_rgs(scq_args, scq_res_v,
	    (bool)verbose_flag, e);

	// scq_args was allocated on stack, so do not need to free

	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}
	res.err_code = (scha_err_t)scq_res_v->ret_code;
	if ((const char *)scq_res_v->idlstr_err_msg)
		res.err_msg = strdup(scq_res_v->idlstr_err_msg);
	free(cluster_name);
	return (res);
}






//
// rgm_scswitch_change_rg_state
//
// scswitch -v -o | -u
// SCSWITCH_OFFLINE (-o) Takes a list of RGs from unmanaged to offline.
// SCSWITCH_UNMANAGED (-u) Takes a list of RGs from offline to unmanaged.
// scswitch talks directly to the President node,
// which latches intentions on slave nodes.
//
// verbose_flag is used by the newcli for the verbose print.
//
scha_errmsg_t
rgm_scswitch_change_rg_state(const char **rg_list,
    const scha_scswitch_obj_state_t state, boolean_t verbose_flag,
    const char *zone_name)
{
	rgm::rgm_comm_var prgm_pres_v;
	Environment e;
	rgm::idl_regis_result_t_var result_v;
	rgm::idl_string_seq_t rg_names_seq;
	sol::nodeid_t nodeid;
	char *zonename = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *cluster_name = NULL;

	// construct a sequence of RG names for the President's use
	if (rg_list == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if ((cluster_name = get_clustername_from_zone(
	    zone_name, &res)) == NULL)
		return (res);

	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc(cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	// If getpres returns nil, no RGM president has yet registered with
	// the naming service.
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		free(cluster_name);
		return (res);
	}
#if SOL_VERSION >= __s10
	res = populate_zone_cred(&(nodeid), &zonename);
	if (res.err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}
#endif

	array_to_strseq(rg_list, rg_names_seq);

	switch (state) {

	case SCSWITCH_UNMANAGED:
		prgm_pres_v->idl_offline_to_unmanaged(rg_names_seq,
		    fetch_msg_locale(), result_v, (bool)verbose_flag, nodeid,
		    zonename, e);
		break;

	case SCSWITCH_OFFLINE:
		prgm_pres_v->idl_unmanaged_to_offline(rg_names_seq,
		    fetch_msg_locale(), result_v, (bool)verbose_flag, nodeid,
		    zonename, e);
		break;

	default:
		// rg_names_seq was allocated on stack, so do not need to free
		res.err_code = SCHA_ERR_INTERNAL;
		rgm_format_errmsg(&res, REM_INTERNAL);
		free(cluster_name);
		return (res);
	}

	// rg_names_seq was allocated on stack, so do not need to free

	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}
	res.err_code = (scha_err_t)result_v->ret_code;
	if ((const char *)result_v->idlstr_err_msg)
		res.err_msg = strdup(result_v->idlstr_err_msg);
	free(cluster_name);
	return (res);
}

//
// rgm_scswitch_set_resource_switch
//
// The function accomplishes the function for
// scswitch  -e | -n (-M) (-h)
// It will either set r_onoff_switch (without -M)
// or set r_monitored_switch (with -M) for specific R(s).
// on a specified set of nodes (with -h) or on all nodes (without -h).
//
// recursive_flg is used for recursive enabling/disabling of
// dependee/dependent resources.
// verbose_flag is used by the newcli for the verbose print.
//
scha_errmsg_t
rgm_scswitch_set_resource_switch(
	const char **r_list,
	const scha_switch_t flag,
	const boolean_t	monitor,
	const boolean_t	recursive_flg, const char **n_list,
	boolean_t verbose_flag, const char *zone_name)
{
	Environment	e;
	rgm::rgm_comm_var	prgm_pres_v;
	rgm::idl_regis_result_t_var	scswoo_res_v;
	rgm::idl_scswitch_onoff_args	scswoo_args;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *zname = NULL;
	char *cluster_name = NULL;

	// Marshal R names into scswoo_args.
	if (r_list == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	if ((cluster_name = get_clustername_from_zone(
	    zone_name, &res)) == NULL)
		return (res);

	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc(cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	// If getpres returns nil, no RGM president has yet registered with
	// the naming service.
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		free(cluster_name);
		return (res);
	}

	scswoo_args.flags = 0;

	array_to_strseq(r_list, scswoo_args.R_names);
	// Marshal N names into scswoo_args.
	array_to_strseq(n_list, scswoo_args.N_names);

#if SOL_VERSION >= __s10
	res = populate_zone_cred(&(scswoo_args.nodeid), &zname);
	if (res.err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}
	scswoo_args.idlstr_zonename = zname;
#endif

	if (recursive_flg) {
		scswoo_args.flags |= 0x1;
	}
	switch (flag) {
	case SCHA_SWITCH_ENABLED:
		if (monitor)
			scswoo_args.action = rgm::RGM_OP_ENABLE_M_R;
		else
			scswoo_args.action = rgm::RGM_OP_ENABLE_R;
		break;
	case SCHA_SWITCH_DISABLED:
		if (monitor)
			scswoo_args.action = rgm::RGM_OP_DISABLE_M_R;
		else
			scswoo_args.action = rgm::RGM_OP_DISABLE_R;
		break;
	default:
		// scswoo_args was allocated on stack, so do not need to free
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_INTERNAL);
		free(cluster_name);
		return (res);
	}

	/* Get the msg locale */
	scswoo_args.locale = new_str(fetch_msg_locale());

	prgm_pres_v->idl_scswitch_onoff(
	    (const rgm::idl_scswitch_onoff_args) scswoo_args, scswoo_res_v,
	    (bool)verbose_flag, e);

	// scswoo_args was allocated on stack, so do not need to free

	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}
	res.err_code = (scha_err_t)scswoo_res_v->ret_code;
	if ((const char *)scswoo_res_v->idlstr_err_msg)
		res.err_msg = strdup(scswoo_res_v->idlstr_err_msg);
	free(cluster_name);
	return (res);
}

// This function implements the scswitch -c -f functionality.


scha_errmsg_t
rgm_scswitch_clear(const char **node_list,
    const char **resource_list,
    scswitch_error_flag_t flag, const char *zone_name)
{
	rgm::idl_scswitch_clear_args scsw_clr_args;
	rgm::idl_regis_result_t_var  scsw_clr_res_v;
	Environment	e;
	rgm::rgm_comm_ptr	prgm_pres_p;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *zname = NULL;
	char *cluster_name = NULL;

	// Translate the input parameters to this function into what would
	// be accepted by the IDL function.

	// Marshal node names into scsw_clr_args
	if (node_list == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		goto finish_clear; //lint !e801
	}
	array_to_strseq(node_list, scsw_clr_args.node_names);

	if (resource_list == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		goto finish_clear; //lint !e801
	}
	array_to_strseq(resource_list, scsw_clr_args.R_names);

#if SOL_VERSION >= __s10
	res = populate_zone_cred(&(scsw_clr_args.nodeid), &zname);
	if (res.err_code != SCHA_ERR_NOERR)
		goto finish_clear; //lint !e801
	scsw_clr_args.idlstr_zonename = zname;
#endif

	// Translate the scswitch flag to be cleared

	switch (flag) {
	case UPDATE_FAILED :
		scsw_clr_args.flag = rgm::UPDATE_FAILED;
		break;
	case INIT_FAILED :
		scsw_clr_args.flag = rgm::INIT_FAILED;
		break;
	case FINI_FAILED :
		scsw_clr_args.flag = rgm::FINI_FAILED;
		break;
	case STOP_FAILED :
		FAULTPT_RGM(FAULTNUM_RGM_AFTER_CLEARING_STOP_FAILED,
			FaultFunctions::generic);
		scsw_clr_args.flag = rgm::STOP_FAILED;
		break;
	default :
		res.err_code = SCHA_ERR_INTERNAL;
		rgm_format_errmsg(&res, REM_INTERNAL);
		goto finish_clear; //lint !e801
	}

	if ((cluster_name = get_clustername_from_zone(
	    zone_name, &res)) == NULL)
		return (res);

	// get a pointer to the President node

	if (allow_zc())
		prgm_pres_p = rgm_comm_getpres_zc(cluster_name);
	else
		prgm_pres_p = rgm_comm_getpres();
	// If getpres returns nil, no RGM president has yet registered with
	// the naming service.
	if (CORBA::is_nil(prgm_pres_p)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		goto finish_clear; //lint !e801
	}

	/* Get the msg locale */
	scsw_clr_args.locale = new_str(fetch_msg_locale());

	// call the IDL method on the President
	prgm_pres_p->idl_scswitch_clear(
	    (const rgm::idl_scswitch_clear_args) scsw_clr_args,
	    scsw_clr_res_v, e);

	CORBA::release(prgm_pres_p);

	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
		goto finish_clear; //lint !e801

	res.err_code = (scha_err_t)scsw_clr_res_v->ret_code;
	if ((const char *)scsw_clr_res_v->idlstr_err_msg)
		res.err_msg = strdup(scsw_clr_res_v->idlstr_err_msg);

finish_clear:
	if (cluster_name) {
		free(cluster_name);
	}

	return (res);
}

scha_errmsg_t
rgm_scswitch_restart_rg(
    const char **newmaster_list, const char **rg_list,
    boolean_t verbose_flag, const char *zone_name)
{
	rgm::rgm_comm_var	prgm_pres_v;
	Environment		e;
	rgm::idl_scswitch_primaries_args	scrst_args;
	rgm::idl_regis_result_t_var		scrst_res_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *zname = NULL;
	char *cluster_name = NULL;

	//
	// The newcli can accept Nil nodelist in which rgm will
	// calculate the nodelist on which all nodes the RG is
	// online.
	//

	if (newmaster_list != NULL) {
		array_to_strseq(newmaster_list, scrst_args.node_names);
	}

	// Marshal RG names into scrst_args
	if (rg_list == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}
	array_to_strseq(rg_list, scrst_args.RG_names);

	/* Get msg locale */
	scrst_args.locale = new_str(fetch_msg_locale());

#if SOL_VERSION >= __s10
	res = populate_zone_cred(&(scrst_args.nodeid), &zname);
	if (res.err_code != SCHA_ERR_NOERR)
		return (res);
	scrst_args.idlstr_zonename = zname;
#endif
	/* set the flags */
	scrst_args.flags = 0;

	if ((cluster_name = get_clustername_from_zone(
	    zone_name, &res)) == NULL)
		return (res);

	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc(cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	// If getpres returns nil, no RGM president has yet registered with
	// the naming service.
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		free(cluster_name);
		return (res);
	}


	prgm_pres_v->idl_scswitch_restart_rg(scrst_args, scrst_res_v,
	    (bool)verbose_flag, e);

	// scrst_args was allocated on stack, so do not need to free

	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}

	res.err_code = (scha_err_t)scrst_res_v->ret_code;
	if ((const char *)scrst_res_v->idlstr_err_msg)
		res.err_msg = strdup(scrst_res_v->idlstr_err_msg);
	free(cluster_name);
	return (res);
}
