//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_event.cc	1.8	08/06/20 SMI"

//
// rgm_event.cc
//
// Client side helper functions to make IDL calls
// to idl_event_gen_XXX() set of functions on
// the RGM president.
//


#include <rgm/rgm_cmn_proto.h>
#include <rgm/rgm_call_pres.h>
#include <rgm/rgm_errmsg.h>

//
// call_event_gen_membership()
// send the 'generate cluster membership event
// request to President
//
scha_errmsg_t
call_event_gen_membership(const char *zonename)
{
	rgm::rgm_comm_var prgm_pres_v;
	rgm::idl_regis_result_t_var ev_res_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	char *cluster_name;

	if (allow_zc()) {
		if ((cluster_name = get_clustername_from_zone(zonename,
		    &res)) == NULL)
			return (res);

		prgm_pres_v = rgm_comm_getpres_zc(cluster_name);
		if (CORBA::is_nil(prgm_pres_v)) {
			res.err_code = SCHA_ERR_CLRECONF;
			rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
			free(cluster_name);
			return (res);
		}
	} else {
		prgm_pres_v = rgm_comm_getpres();
		if (CORBA::is_nil(prgm_pres_v)) {
			res.err_code = SCHA_ERR_CLRECONF;
			rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
			free(cluster_name);
			return (res);
		}
	}

	prgm_pres_v->idl_event_gen_membership(ev_res_v, e);

	res = except_to_scha_err(e);

	if (res.err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}
	res.err_code = (scha_err_t)ev_res_v->ret_code;
	if ((const char *)ev_res_v->idlstr_err_msg)
		res.err_msg = strdup(ev_res_v->idlstr_err_msg);
	free(cluster_name);
	return (res);
}

//
// call_event_gen_rg_state()
//	send the 'generate rg state event' request to President
//
scha_errmsg_t
call_event_gen_rg_state(const char *rg_name, const char *zonename)
{
	rgm::rgm_comm_var prgm_pres_v;
	rgm::idl_regis_result_t_var ev_res_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	char *cluster_name;

	if (allow_zc()) {
		if ((cluster_name = get_clustername_from_zone(zonename,
		    &res)) == NULL)
			return (res);

		prgm_pres_v = rgm_comm_getpres_zc(cluster_name);
		if (CORBA::is_nil(prgm_pres_v)) {
			res.err_code = SCHA_ERR_CLRECONF;
			rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
			free(cluster_name);
			return (res);
		}
	} else {
		prgm_pres_v = rgm_comm_getpres();
		if (CORBA::is_nil(prgm_pres_v)) {
			res.err_code = SCHA_ERR_CLRECONF;
			rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
			free(cluster_name);
			return (res);
		}
	}

	prgm_pres_v->idl_event_gen_rg_state(rg_name,
		ev_res_v, e);

	res = except_to_scha_err(e);

	if (res.err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}
	res.err_code = (scha_err_t)ev_res_v->ret_code;
	if ((const char *)ev_res_v->idlstr_err_msg)
		res.err_msg = strdup(ev_res_v->idlstr_err_msg);
	free(cluster_name);
	return (res);
}

//
// call_event_gen_rs_state()
//	send the 'generate rg state event' request to President
//
scha_errmsg_t
call_event_gen_rs_state(const char *rs_name, const char *zonename)
{
	rgm::rgm_comm_var prgm_pres_v;
	rgm::idl_regis_result_t_var ev_res_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	char *cluster_name;

	if (allow_zc()) {
		if ((cluster_name = get_clustername_from_zone(zonename,
		    &res)) == NULL)
			return (res);

		prgm_pres_v = rgm_comm_getpres_zc(cluster_name);
		if (CORBA::is_nil(prgm_pres_v)) {
			res.err_code = SCHA_ERR_CLRECONF;
			rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
			free(cluster_name);
			return (res);
		}
	} else {
		prgm_pres_v = rgm_comm_getpres();
		if (CORBA::is_nil(prgm_pres_v)) {
			res.err_code = SCHA_ERR_CLRECONF;
			rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
			free(cluster_name);
			return (res);
		}
	}
	prgm_pres_v->idl_event_gen_rs_state(rs_name,
		ev_res_v, e);
	res = except_to_scha_err(e);

	if (res.err_code != SCHA_ERR_NOERR) {
		free(cluster_name);
		return (res);
	}
	res.err_code = (scha_err_t)ev_res_v->ret_code;
	if ((const char *)ev_res_v->idlstr_err_msg)
		res.err_msg = strdup(ev_res_v->idlstr_err_msg);
	free(cluster_name);
	return (res);
}
