//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)rgm_datatype.cc	1.57	08/05/20 SMI"

#include <rgm/rgm_cmn_proto.h>
#include <rgm/rgm_util.h>
/* #include <map> */
/*
 * values for scha_initnodes_flag_t
 */
static struct {
	const char *string;
	scha_initnodes_flag_t value;
} RGM_INITNODES[] = {
	{SCHA_RG_PRIMARIES,
		SCHA_INFLAG_RG_PRIMARIES},
	{SCHA_RT_INSTALLED_NODES,
		SCHA_INFLAG_RT_INSTALLED_NODES},
	{NULL,	SCHA_INFLAG_RG_PRIMARIES}
};

/*
 * values for scha_prop_type_t
 */
static struct {
	const char *string;
	scha_prop_type_t value;
} RGM_TYPE[] = {
	{SCHA_STRING,	SCHA_PTYPE_STRING},
	{SCHA_INT,	SCHA_PTYPE_INT},
	{SCHA_BOOLEAN,	SCHA_PTYPE_BOOLEAN},
	{SCHA_ENUM,	SCHA_PTYPE_ENUM},
	{SCHA_STRINGARRAY,
			SCHA_PTYPE_STRINGARRAY},
	{SCHA_UINTARRAY,
			SCHA_PTYPE_UINTARRAY},
	{SCHA_UINT,	SCHA_PTYPE_UINT},
	{NULL,		SCHA_PTYPE_STRING}
};

/*
 * values for rgm_tune_t
 */
static struct {
	const char *string;
	rgm_tune_t value;
} RGM_TUNE[] = {
	{SCHA_NONE,	TUNE_NONE},
	{SCHA_AT_CREATION,
			TUNE_AT_CREATION},
	{SCHA_ANYTIME,	TUNE_ANYTIME},
	{SCHA_WHEN_DISABLED,
			TUNE_WHEN_DISABLED},
	{SCHA_WHEN_OFFLINE,
			TUNE_WHEN_OFFLINE},
	{SCHA_WHEN_UNMANAGED,
			TUNE_WHEN_UNMANAGED},
	{SCHA_WHEN_UNMONITORED,
			TUNE_WHEN_UNMONITORED},
	{NULL,		TUNE_NONE}
};

/*
 * values for rgm_sysdeftype_t
 */
static struct {
	const char *string;
	rgm_sysdeftype_t value;
} RGM_SYSDEF_TYPE[] = {
	{SCHA_NONE,	SYST_NONE},
	{SCHA_LOGICAL_HOSTNAME,
			SYST_LOGICAL_HOSTNAME},
	{SCHA_SHARED_ADDRESS,
			SYST_SHARED_ADDRESS},
	{NULL,		SYST_NONE}
};

/*
 * values for scha_rgmode_t
 */
static struct {
	const char		*string;
	scha_rgmode_t	value;
} RGM_RGMODE[] = {
	{SCHA_FAILOVER, RGMODE_FAILOVER},
	{SCHA_SCALABLE, RGMODE_SCALABLE},
	{NULL,		RGMODE_NONE}
};

/*
 * Caller is trusted to pass non-NULL 's'.
 */
scha_prop_type_t
param_type(const char *s)
{
	int i = 0;
	while (RGM_TYPE[i].string != NULL) {
		if (strcmp(s, RGM_TYPE[i].string) == 0)
			return (RGM_TYPE[i].value);
		i++;
	}

	return (SCHA_PTYPE_STRINGARRAY);	// return default value
}

const char *
type2str(scha_prop_type_t value)
{
	int i = 0;
	while (RGM_TYPE[i].string != NULL) {
		if (RGM_TYPE[i].value == value)
			return (RGM_TYPE[i].string);
		i++;
	}

	return (SCHA_STRINGARRAY);	// return default value
}


/*
 * Caller is trusted to pass non-NULL 's'.
 */
rgm_tune_t
param_tune(const char *s)
{
	int i = 0;
	while (RGM_TUNE[i].string != NULL) {
		if (strcmp(s, RGM_TUNE[i].string) == 0)
			return (RGM_TUNE[i].value);
		i++;
	}

	return (TUNE_WHEN_DISABLED);	// return default value
}


const char *
tune2str(rgm_tune_t value)
{
	int i = 0;
	while (RGM_TUNE[i].string != NULL) {
		if (RGM_TUNE[i].value == value)
			return (RGM_TUNE[i].string);
		i++;
	}

	return (SCHA_WHEN_DISABLED);	// return default value
}


/*
 * Caller is trusted to pass non-NULL 's'.
 */
scha_initnodes_flag_t
prop_initnodes(const char *s)
{
	int i = 0;
	while (RGM_INITNODES[i].string != NULL) {
		if (strcmp(s, RGM_INITNODES[i].string) == 0)
			return (RGM_INITNODES[i].value);
		i++;
	}

	return (SCHA_INFLAG_RT_INSTALLED_NODES);	// return default value
}


const char *
initnodes2str(scha_initnodes_flag_t value)
{
	int i = 0;
	while (RGM_INITNODES[i].string != NULL) {
		if (RGM_INITNODES[i].value == value)
			return (RGM_INITNODES[i].string);
		i++;
	}
	return (NULL);
}


/*
 * Convert a string to enum of rgm_sysdeftype_t
 * Caller is trusted to pass non-NULL 's'.
 */
rgm_sysdeftype_t
prop_sysdeftype(const char *s)
{
	int i = 0;
	while (RGM_SYSDEF_TYPE[i].string != NULL) {
		if (strcmp(s, RGM_SYSDEF_TYPE[i].string) == 0)
			return (RGM_SYSDEF_TYPE[i].value);
		i++;
	}

	return (SYST_NONE);	// return default value
}

/*
 * Convert a string to enum of scha_rgmode_t.
 * Caller is trusted to pass in non-NULL 's'.
 * if the string is not "scalable' or "failover", return RGMODE_NONE.
 */
scha_rgmode_t
prop_rgmode(const char *s)
{
	int i = 0;
	while (RGM_RGMODE[i].string != NULL) {
		if (strcasecmp(s, RGM_RGMODE[i].string) == 0)
			return (RGM_RGMODE[i].value);
		i++;
	}

	return (RGMODE_NONE);	// return RGMODE_NONE
}

const char *
sysdeftype2str(rgm_sysdeftype_t value)
{
	int i = 0;
	while (RGM_SYSDEF_TYPE[i].string != NULL) {
		if (RGM_SYSDEF_TYPE[i].value  ==  value)
			return (RGM_SYSDEF_TYPE[i].string);
		i++;
	}

	return (SCHA_NONE);	// return default value
}

const char *
rgmode2str(scha_rgmode_t value)
{
	int i = 0;
	while (RGM_RGMODE[i].string != NULL) {
		if (RGM_RGMODE[i].value  ==  value)
			return (RGM_RGMODE[i].string);
		i++;
	}

	return (SCHA_NONE);	// return default value
}


/*
 * Convert boolean_t to string
 */
char *
bool2str(boolean_t b)
{
	return ((b == B_TRUE) ? SCHA_TRUE : SCHA_FALSE);
}


/*
 * Convert string to boolean_t
 * Caller is trusted to pass non-NULL 's'.
 */
boolean_t
str2bool(const char *s)
{
	return ((strcasecmp(s, SCHA_TRUE) == 0) ? B_TRUE : B_FALSE);
}


/*
 * Convert integer to ascii
 * NOTE: caller needs to free memory
 */
char *
rgm_itoa(int i)
{
	// Use MAXNAMELEN to be on the safe side
	char	s[MAXNAMELEN];
	char	*ptr;

	(void) snprintf(s, MAXNAMELEN, "%d", i);
	ptr = strdup(s);

	return (ptr);
}

/*
 * Convert integer to ascii. Use new to allocate memory.
 * NOTE: caller needs to free memory
 */
char *
rgm_itoa_cpp(int i)
{
	// Use MAXNAMELEN to be on the safe side
	char	s[MAXNAMELEN];
	char	*ptr;

	(void) snprintf(s, MAXNAMELEN, "%d", i);
	ptr = os::strdup(s);

	return (ptr);
}

/*
 *
 * utility function called by nstr2str and nstr2str_cpp
 * Can allocate memory with either malloc or new, based on "cpp" argument
 *
 * Convert a linked list of structure namelist_t or rdeplist_t to a string and
 * each value is separated by ','
 *
 */
static char *
sub_nstr2str(void *l, boolean_t cpp, boolean_t is_rdeplist)
{
	char		*ptr;
	namelist_t	*curr, *nl = NULL;
	uint_t		strsize = 0;
	rdeplist_t	*curr_dep, *rl = NULL;
	int		num_elements = 0;

	if (l == NULL) {
		if (cpp) {
			ptr = os::strdup("");
		} else {
			ptr = strdup("");
		}
		return (ptr);
	}

	if (is_rdeplist)
		rl = (rdeplist_t *)l;
	else
		nl = (namelist_t *)l;

	/*
	 * At this point, either nl is NULL or rl is NULL.
	 */
	if (nl != NULL) {
		for (curr = nl; curr != NULL; curr = curr->nl_next) {
			if (curr->nl_name != NULL) {
				// add 1 for the separator
				strsize += strlen(curr->nl_name) + 1;
				// count the number of elements in the list
				num_elements++;
			}
		}
	} else {
		for (curr_dep = rl; curr_dep != NULL;
		    curr_dep = curr_dep->rl_next) {
			if (curr_dep->rl_name != NULL) {
				switch (curr_dep->locality_type) {
				// In the CCR, the resource dependency would be
				// stored as <resource_name>{LOCAL_NODE/
				// ANY_NODE}. For dependencies, following the
				// default semantics (FROM_RG_AFFINITES), the
				// storage allocated is just for the resrc name
				// and the separator.
				case LOCAL_NODE:
					strsize += strlen(curr_dep->rl_name) +
					    strlen(SCHA_LOCAL_NODE) + 3;
					break;
				case ANY_NODE:
					strsize += strlen(curr_dep->rl_name) +
					    strlen(SCHA_ANY_NODE) + 3;
					break;
				case FROM_RG_AFFINITIES:
					strsize += strlen(curr_dep->rl_name)
					    + 1;
					break;
				default:
					// Should'nt reach here..
					ASSERT(0);
				}
				num_elements++;
			}
		}
	}

	if (cpp) {
		ptr = new char[strsize];
	} else {
		ptr = (char *)malloc(strsize);
	}
	if (ptr == NULL) {
		return (NULL);
	}

	ptr[0] = '\0';

	if (nl != NULL) {
		for (curr = nl; curr != NULL; curr = curr->nl_next) {
			if (curr->nl_name != NULL) {
				(void) strcat(ptr, curr->nl_name);
				num_elements--;
				// no separator following the last element
				if (num_elements > 0)
					(void) strcat(ptr,
					    RGM_VALUE_SEPARATOR_STR);
			}
		}
	} else {
		for (curr_dep = rl; curr_dep != NULL;
		    curr_dep = curr_dep->rl_next) {
			if (curr_dep->rl_name != NULL) {
				(void) strcat(ptr, curr_dep->rl_name);
				switch (curr_dep->locality_type) {
				case LOCAL_NODE:
					(void) strcat(ptr, "{");
					(void) strcat(ptr, SCHA_LOCAL_NODE);
					(void) strcat(ptr, "}");
					break;
				case ANY_NODE:
					(void) strcat(ptr, "{");
					(void) strcat(ptr, SCHA_ANY_NODE);
					(void) strcat(ptr, "}");
					break;
				case FROM_RG_AFFINITIES:
					break;
				default:
					// Should'nt reach here.
					ASSERT(0);
				}
				num_elements--;
				// no separator following the last element
				if (num_elements > 0)
					(void) strcat(ptr,
					    RGM_VALUE_SEPARATOR_STR);
			}
		}
	}
	return (ptr);
}


/*
 * Convert a linked list of structure namelist_t to a string and
 * each value is separated by ','
 * Note: caller needs to free memory
 */
char *
nstr2str(namelist_t *l)
{
	return (sub_nstr2str(l, B_FALSE, B_FALSE));
}


/*
 * Convert a linked list of structure rdeplist_t to a string and
 * each value is separated by ','
 * Note: caller needs to free memory
 */

char *
nstr2str_dep(rdeplist_t *l)
{
	return (sub_nstr2str(l, B_FALSE, B_TRUE));
}

/*
 * Convert a linked list of structure namelist_t to a string and
 * each value is separated by ','
 * Note: caller needs to free memory
 */
char *
nstr2str_cpp(namelist_t *l)
{
	return (sub_nstr2str(l, B_TRUE, B_FALSE));
}


/*
 * Convert a linked list of structure namelist_all_t to a string,
 * handling the special case of an "*" or "all" value.
 * If the value is not all, a regular conversion to a ',' separated
 * list is done.
 * Note: caller needs to free memory
 */
char *
nstr_all2str(const namelist_all_t *l)
{

	char *ptr = (l->is_ALL_value == B_TRUE) ?
	    strdup(SCHA_ALL_SPECIAL_VALUE) :
	    nstr2str(l->names);
	return (ptr);
}


/*
 *
 * utility function called by nodeidlist2str and nodeidlist2str_cpp
 * Can allocate memory with either malloc or new, based on "cpp" argument
 *
 * Convert a linked list of structure nodeidlist_t to a string and
 * each value is separated by ','
 *
 */
static char*
sub_nodeidlist2str(nodeidlist_t *l, boolean_t cpp)
{
	char		s[MAXNAMELEN];
	nodeidlist_t	*curr;
	char		*ptr;
	uint_t		strsize = 0;
	int		num_elements = 0;

	// Determine length of the string to allocate
	for (curr = l; curr != NULL; curr = curr->nl_next) {
		bzero(s, MAXNAMELEN);
		(void) snprintf(s, MAXNAMELEN, "%d", curr->nl_nodeid);
		if (curr->nl_zonename) {
			(void) strcat(s, LN_DELIMITER_STR);
			(void) strcat(s, curr->nl_zonename);
		}
		// add 1 for the separator and 1 for the \0
		strsize += strlen(s) + 2;
		// count the number of elements in the list
		num_elements++;
	}

	// Allocate memory
	if (cpp) {
		ptr = new char[strsize];
	} else {
		ptr = (char *)malloc(strsize);
	}
	if (ptr == NULL) {
		return (NULL);
	}

	// Copy the int nodeid into the string
	ptr[0] = '\0';
	for (curr = l; curr != NULL; curr = curr->nl_next) {
		bzero(s, MAXNAMELEN);
		(void) snprintf(s, MAXNAMELEN, "%d", curr->nl_nodeid);
		if (curr->nl_zonename) {
			(void) strcat(s, LN_DELIMITER_STR);
			(void) strcat(s, curr->nl_zonename);
		}
		(void) strcat(ptr, s);
		num_elements--;
		// no separator following the last element
		if (num_elements > 0) {
			(void) strcat(ptr, RGM_VALUE_SEPARATOR_STR);
		}
	}
	return (ptr);
}

/*
 * Convert a linked list of structure nodeidlist_t to a string and
 * each value is separated by ','
 * Note: caller needs to free memory
 */
char*
nodeidlist2str(nodeidlist_t *l)
{
	return (sub_nodeidlist2str(l, B_FALSE));
}

/*
 * Convert a linked list of structure nodeidlist_t to a string and
 * each value is separated by ','
 * Note: caller needs to free memory
 */
char *
nodeidlist2str_cpp(nodeidlist_t *l)
{
	return (sub_nodeidlist2str(l, B_TRUE));
}

/*
 * Convert a linked list of structure namelist_all_t to a string,
 * handling the special case of an "*" or "all" value.
 * If the value is not all, a regular conversion to a ',' separated
 * list is done.
 * Note: caller needs to free memory
 * use C++ allocator
 */
char *
nstr_all2str_cpp(const namelist_all_t *l)
{

	char *ptr = (l->is_ALL_value == B_TRUE) ?
	    os::strdup(SCHA_ALL_SPECIAL_VALUE) :
	    nstr2str_cpp(l->names);
	return (ptr);
}

/*
 *
 */
char *
nodeidlist_all2str_cpp(const nodeidlist_all_t *l)
{

	char *ptr = (l->is_ALL_value == B_TRUE) ?
	    os::strdup(SCHA_ALL_SPECIAL_VALUE) :
	    nodeidlist2str_cpp(l->nodeids);
	return (ptr);
}



/*
 * Convert a string to a linked list of structure namelist_t.
 * Values in the string are delimited by ','.
 * Notes: (1) Caller needs to free the returned memory.
 *        (2) This function cannot return a memory allocation error since it
 *            is hard to distinguish it from a case where the correct result
 *            is NULL. Therefore, this function will cause rgmd (and the node)
 *            to die in case of memory allocation failures.
 *        (3) Caller is trusted to pass non-NULL 's'.
 */
namelist_t *
str2nstr(char *s)
{
	namelist_t *namelist_head = NULL;
	namelist_t *ptr, *curr = NULL;
	char *nm;

	if (s == NULL || s[0] == '\0')
		return (NULL);

	/*
	 * For each token in 's', allocate memory for a namelist_t struct,
	 * and link the new namelist_t struct to the end of the list
	 * headed by 'namelist_head'.
	 */
	while ((nm = (char *)my_strtok(&s, RGM_VALUE_SEPARATOR)) != NULL) {
		ptr = (namelist_t *)malloc(sizeof (namelist_t));
		ptr->nl_name = strdup(nm);
		ptr->nl_next = NULL;

		if (namelist_head == NULL)
			namelist_head = ptr;
		else
			curr->nl_next = ptr;

		curr = ptr;
	}

	return (namelist_head);
}


/*
 * Convert a string to a linked list of structure nodeidlist_t.
 * Values in the string are delimited by ','.
 * Notes: (1) Caller needs to free the returned memory.
 *        (2) This function cannot return a memory allocation error since it
 *            is hard to distinguish it from a case where the correct result
 *            is NULL. Therefore, this function will cause rgmd (and the node)
 *            to die in case of memory allocation failures.
 *        (3) Caller is trusted to pass non-NULL 's'.
 */
nodeidlist_t *
str2nodeidlist(char *s)
{
	nodeidlist_t *nodeidlist_head = NULL;
	nodeidlist_t *ptr, *curr = NULL;
	char *nm;
	char *nodeid;
	char *zonename;

	if (s == NULL || s[0] == '\0')
		return (NULL);

	/*
	 * For each token in 's', allocate memory for a nodeidlist_t struct,
	 * and link the new nodeidlist_t struct to the end of the list
	 * headed by 'nodeidlist_head'.
	 */
	while ((nm = (char *)my_strtok(&s, RGM_VALUE_SEPARATOR)) != NULL) {
		ptr = (nodeidlist_t *)malloc(sizeof (nodeidlist_t));

		nodeid = (char *)my_strtok(&nm, LN_DELIMITER);
		zonename = nm;

		ptr->nl_nodeid = (unsigned int)atoi(nodeid);
		ptr->nl_zonename = (zonename) ? strdup(zonename) : NULL;
		ptr->nl_lni = LNI_UNDEF;
		ptr->nl_next = NULL;

		if (nodeidlist_head == NULL)
			nodeidlist_head = ptr;
		else
			curr->nl_next = ptr;

		curr = ptr;
	}

	return (nodeidlist_head);
}


/*
 * Convert a string to a linked list of structure rdeplist_t. A string is of the
 * form <resource_name>{<locality_type>}. locality_type has to be one of the
 * following: LOCAL_NODE, ANY_NODE or FROM_RG_AFFINITIES
 *
 * Notes: (1) Caller needs to free the returned memory.
 *        (2) This function cannot return a memory allocation error since it
 *            is hard to distinguish it from a case where the correct result
 *            is NULL. Therefore, this function will cause rgmd (and the node)
 *            to die in case of memory allocation failures.
 *        (3) Caller is trusted to pass non-NULL 's'.
 */
rdeplist_t *
str2rdepstr(char *s)
{
	rdeplist_t *rdeplist_head = NULL;
	rdeplist_t *ptr, *curr = NULL;
	char *nm, *name;
	char *ltype;

	if (s == NULL || s[0] == '\0')
		return (NULL);

	/*
	 * For each token in 's', allocate memory for a rdeplist_t struct,
	 * and link the new rdeplist_t struct to the end of the list
	 * headed by 'rdeplist_head'.
	 */
	while ((nm = (char *)my_strtok(&s, RGM_VALUE_SEPARATOR)) != NULL) {
		ptr = (rdeplist_t *)malloc(sizeof (rdeplist_t));
		name = my_strtok(&nm, RGM_DEP_SEPARATOR_PRE);
		ptr->rl_name = strdup(name);
		if (nm == NULL) {
			// Delimiter "{" not found
			ptr->locality_type = FROM_RG_AFFINITIES;
		} else {
			ltype = my_strtok(&nm, RGM_DEP_SEPARATOR_POST);
			if (strcasecmp(ltype, SCHA_LOCAL_NODE) == 0)
				ptr->locality_type = LOCAL_NODE;
			else if (strcasecmp(ltype, SCHA_ANY_NODE) == 0)
				ptr->locality_type = ANY_NODE;
			else if (strcasecmp(ltype, SCHA_FROM_RG_AFFINITIES)
			    == 0)
				ptr->locality_type = FROM_RG_AFFINITIES;
		}
		ptr->rl_next = NULL;

		if (rdeplist_head == NULL)
			rdeplist_head = ptr;
		else {
			curr->rl_next = ptr;
		}

		curr = ptr;
	}

	return (rdeplist_head);
}







/*
 * Convert an array of strings to a string in which elements are separated by ,
 * Note: caller needs to free memory.
 */
char *
strarray2str(char **sa)
{
	char	*ptr;
	char	 **s;
	uint_t	strsize = 1; // start with 1 for the \0
	int	num_elements = 0;

	if (sa == NULL) {
		ptr = strdup("");
		return (ptr);
	}

	for (s = sa; *s; s++) {
		// add 1 for the separator
		strsize += strlen(*s) + 1;
		// count the number of elements in the list
		num_elements++;
	}
	if ((ptr = (char *)malloc(strsize)) == NULL)
		return (NULL);
	ptr[0] = '\0';

	for (s = sa; *s; s++) {
		(void) strcat(ptr, *s);
		num_elements--;
		// no separator following the last element
		if (num_elements > 0)
			(void) strcat(ptr,
			    RGM_VALUE_SEPARATOR_STR); //lint !e803
	}

	return (ptr);
}


/*
 * Convert a list of names which are separated by ',' to a array of strings
 * Notes: (1) Caller needs to free the returned memory.
 *        (2) This function cannot return a memory allocation error since it
 *            is hard to distinguish it from a case where the correct result
 *            is NULL. Therefore, this function will cause rgmd (and the node)
 *            to die in case of memory allocation failures.
 *        (3) Caller is trusted to pass non-NULL 's'.
 */
char **
str2strarray(char *s)
{
	char **sa = NULL;
	uint_t i = 0;
	char *nm;

	if (s == NULL || s[0] == '\0')
		return (NULL);

	/*
	 * Keep appending to dynamically reallocated 'sa' the next token
	 * from 's' until we reach the end of 's'.
	 */
	while ((nm = (char *)my_strtok(&s, RGM_VALUE_SEPARATOR)) != NULL) {
		/*
		 * 2 = 1 + 1
		 * 1 (because we index from zero)
		 * plus 1 (for the terminating NULL entry)
		 */
		sa = (char **)realloc_err(sa, (i + 2) * sizeof (char *));
		sa[i] = strdup(nm);
		i++;
	}
	if (sa)
		sa[i] = NULL;

	return (sa);
}


/*
 * Convert a linked list of structure namelist_t to array of string
 * Notes: (1) Caller needs to free the returned memory.
 *        (2) This function cannot return a memory allocation error since it
 *            is hard to distinguish it from a case where the correct result
 *            is NULL. Therefore, this function will cause rgmd (and the node)
 *            to die in case of memory allocation failures.
 */
char **
nstr2strarray(namelist_t *nlist)
{
	char **sa = NULL;
	namelist_t *lp;
	uint_t i = 0;

	if (nlist == (namelist_t *)0)
		return (NULL);

	for (lp = nlist; lp != NULL; lp = lp->nl_next) {
		/*
		 * 2 = 1 + 1
		 * 1 (because we index from zero)
		 * plus 1 (for the terminating NULL entry)
		 */
		sa = (char **)realloc_err(sa, (i + 2) * (sizeof (char *)));
		sa[i] = strdup(lp->nl_name);
		i++;
	}
	if (sa)
		sa[i] = NULL;

	return (sa);
}


/*
 * Convert an array of strings to a linked list of structure namelist_t
 * Notes: (1) Caller needs to free the returned memory.
 *        (2) This function cannot return a memory allocation error since it
 *            is hard to distinguish it from a case where the correct result
 *            is NULL. Therefore, this function will cause rgmd (and the node)
 *            to die in case of memory allocation failures.
 */
namelist_t *
strarray2nstr(char **sa)
{
	namelist_t *namelist_head = NULL;
	namelist_t *ptr, *curr = NULL;
	char **s;

	if (sa == NULL)
		return (NULL);

	for (s = sa; *s; s++) {
		ptr = (namelist_t *)malloc(sizeof (namelist_t));
		ptr->nl_name = strdup(*s);
		ptr->nl_next = NULL;
		if (namelist_head == NULL)
			namelist_head = ptr;
		else
			curr->nl_next = ptr;
		curr = ptr;
	}

	return (namelist_head);
}

/*
 * cat_buf_to_s is a utility specifically for params2str.
 * It concatenates a buffer to a malloc'd string and returns a
 * pointer to the reallocated string.
 * The size of the original string must be passed as an in/out
 * parameter, and this parameter is set to the size of
 * the new string.
 * If reallocation fails, the input string is freed, NULL is returned,
 * and 0 is returned as the size of string.
 */
static char *
cat_buff_to_s(char *s, uint_t *s_size, const char *buff)
{
	uint_t  new_s_size = *s_size + strlen(buff);
	char *old_s = s;
	char *new_s = (char *)realloc_err(s, new_s_size + 1);
	if (new_s == NULL) {
		free(old_s);
		*s_size = 0;
		return (NULL);
	}
	(void) strcpy(new_s + *s_size, buff);
	*s_size = new_s_size;
	return (new_s);
}

/*
 * Convert the info stored in struct rgm_param_t into a string.
 * Definitions are separated by RGM_PROP_SEPARATOR.
 * Note: caller needs to free memory
 *
 */
char *
params2str(const rgm_param_t *p)
{
	char *sptr = NULL;
	char	buff[BUFSIZ];	/* used for smallish strings */
	uint_t sptr_size = 0;
	uint_t new_sptr_size = 0;
	scha_prop_type_t ptype;

	const char str_fmt[] = "%s=%s"RGM_PROP_SEPARATOR_STR;
	const char qstr_fmt[] = "%s=\"%s\""RGM_PROP_SEPARATOR_STR;
	const char int_fmt[] = "%s=%d"RGM_PROP_SEPARATOR_STR;

	/*
	 * The following constants stand for the number of extra characters
	 * needed to store strings in the appropriate format. This includes
	 * the equal sign separating the parameter name from its value, the
	 * separator character and, in the case of qstr_fmt_len, the two quote
	 * characters. It does not include the terminating NULL character.
	 *
	 * To get the size of the actual <name/value> pair, one needs to
	 * add the lengths of the name string and the value string.
	 */
	const int str_fmt_len = 2;
	const int qstr_fmt_len = 4;

	if (p == NULL) {
		sptr = strdup("");
		return (sptr);
	}

	ptype = p->p_type;

	/*
	 * Routines that convert enum values to strings
	 * return static constants that do not need
	 * to be free'd.
	 */
	(void) sprintf(buff,
	    "%s=%s"RGM_PROP_SEPARATOR_STR"%s=%s"RGM_PROP_SEPARATOR_STR,
	    SCHA_TUNABLE, tune2str(p->p_tunable),
	    SCHA_TYPE, type2str(ptype));
	/*
	 * Why buff[BUFSIZ] is not in danger of being overrun:
	 * Adding up the sizes we have approximately
	 * SCHA_TUNABLE (7) + SCHA_TYPE (4) +
	 * tunable attribute string (13) + Type attribute string (11) +
	 * =;=; (4) + '\0' (1) = 40 which is less than BUFSIZ.
	 * Similarly, the subsequent uses of buff have on the order
	 * of only few dozen bytes.
	 */
	sptr_size = strlen(buff);
	if ((sptr = (char *)malloc(sptr_size + 1)) == NULL)
		return (NULL);
	(void) strcpy(sptr, buff);

	if (ptype == SCHA_PTYPE_ENUM) {
		char *new_sptr = NULL;

		/* Append default string if any. */

		if (p->p_defaultstr) {
			new_sptr = (char *)malloc(str_fmt_len +
			    strlen(SCHA_DEFAULT) + strlen(p->p_defaultstr) + 1);
			if (new_sptr == NULL) {
				free(sptr);
				return (NULL);
			}
			(void) sprintf(new_sptr, str_fmt, SCHA_DEFAULT,
			    p->p_defaultstr);
			sptr = cat_buff_to_s(sptr, &sptr_size, new_sptr);
			free(new_sptr);
			if (sptr == NULL) {
				return (NULL);
			}
		}

		/* Append Enumlist. */

		char *tmp = nstr2str(p->p_enumlist);

		if (tmp == NULL) {
			free(sptr);
			return (NULL);
		}

		new_sptr = (char *)malloc(str_fmt_len + strlen(SCHA_ENUMLIST) +
		    strlen(tmp) + 1);
		if (new_sptr == NULL) {
			free(tmp);
			free(sptr);
			return (NULL);
		}
		(void) sprintf(new_sptr, str_fmt, SCHA_ENUMLIST, tmp);
		free(tmp);
		sptr = cat_buff_to_s(sptr, &sptr_size, new_sptr);
		free(new_sptr);
		if (sptr == NULL) {
			return (NULL);
		}
	}
	if (p->p_default_isset) {
		switch (ptype) {
		case SCHA_PTYPE_STRING:
		case SCHA_PTYPE_STRINGARRAY:
		case SCHA_PTYPE_UINTARRAY:
		{
			char *tmp, *tmp1;
			boolean_t free_tmp = B_FALSE;
			if (ptype == SCHA_PTYPE_STRINGARRAY ||
			    ptype == SCHA_PTYPE_UINTARRAY) {
				tmp = nstr2str(p->p_defaultarray);
				if (tmp == NULL) {
					free(sptr);
					return (NULL);
				}
				free_tmp = B_TRUE;
			} else {
				tmp = p->p_defaultstr;
				if (tmp == NULL)
					break;
			}
			tmp1 = (char *)malloc(str_fmt_len +
			    strlen(SCHA_DEFAULT) + strlen(tmp) + 1);
			if (tmp1 == NULL) {
				free(sptr);
				return (NULL);
			}
			(void) sprintf(tmp1, str_fmt, SCHA_DEFAULT, tmp);
			sptr = cat_buff_to_s(sptr, &sptr_size, tmp1);
			if (free_tmp == B_TRUE) {
				free(tmp);
			}
			free(tmp1);
			if (sptr ==  NULL) {
				return (NULL);
			}
			break;
		}
		case SCHA_PTYPE_INT:
		case SCHA_PTYPE_UINT:
		case SCHA_PTYPE_BOOLEAN:
			if (ptype == SCHA_PTYPE_BOOLEAN) {
				/*
				 * size of constructed string < BUFSIZ:
				 * str_fmt (6) + SCHA_DEFAULT (7) +
				 * "FALSE" (5) + '\0' (1) = 19
				 */
				(void) sprintf(buff,
				    str_fmt,
				    SCHA_DEFAULT,
				    bool2str(p->p_defaultbool));
			} else {
				(void) sprintf(buff,
				    int_fmt,
				    SCHA_DEFAULT,
				    p->p_defaultint);
			}
			sptr = cat_buff_to_s(sptr, &sptr_size, buff);
			if (sptr == NULL) {
				return (NULL);
			}
			break;
		case SCHA_PTYPE_ENUM:
			break;
		}
	}

	if (p->p_min_isset) {
		(void) sprintf(buff, int_fmt, SCHA_MIN, p->p_min);
		sptr = cat_buff_to_s(sptr, &sptr_size, buff);
		if (sptr == NULL) {
			return (NULL);
		}
	}

	if (p->p_max_isset) {
		(void) sprintf(buff, int_fmt, SCHA_MAX, p->p_max);
		sptr = cat_buff_to_s(sptr, &sptr_size, buff);
		if (sptr == NULL) {
			return (NULL);
		}
	}
	if (p->p_arraymin_isset) {
		(void) sprintf(buff, int_fmt,
		    SCHA_ARRAY_MINSIZE, p->p_arraymin);
		sptr = cat_buff_to_s(sptr, &sptr_size, buff);
		if (sptr == NULL) {
			return (NULL);
		}
	}
	if (p->p_arraymax_isset) {
		(void) sprintf(buff, int_fmt,
		    SCHA_ARRAY_MAXSIZE, p->p_arraymax);
		sptr = cat_buff_to_s(sptr, &sptr_size, buff);
		if (sptr == NULL) {
			return (NULL);
		}
	}
	if (p->p_description) {
		char *new_sptr;

		new_sptr_size = qstr_fmt_len + strlen(SCHA_DESCRIPTION) +
		    strlen(p->p_description);
		new_sptr = (char *)malloc(new_sptr_size + 1);
		if (new_sptr == NULL) {
			free(sptr);
			return (NULL);
		}
		(void) sprintf(new_sptr, qstr_fmt, SCHA_DESCRIPTION,
		    p->p_description);

		sptr = cat_buff_to_s(sptr, &sptr_size, new_sptr);
		free(new_sptr);
		if (sptr == NULL) {
			return (NULL);
		}
	}

	return (sptr);
}

/*
 *
 * wrapper around params2str to use C++ allocator
 *
 * params2str is too complicated to rewrite entirely with C++ allocators
 * instead, call params2str and make a copy. Inefficient, but
 * params2str already makes so many realloc...
 */
char *
params2str_cpp(rgm_param_t *p)
{
	unsigned int len;
	char *res, *str;

	str = params2str(p);

	if (str != NULL) {
		len = strlen(str) + 1;
		res = new char[len];
		if (res != NULL) {
			(void) strncpy(res, str, len);
		}
		free(str);
	} else {
		res = str;
	}
	return (res);
} //lint !e818

/*
 * Return the first token in the input str as delimited
 * by the delimiter or the end of the string (which
 * ever comes first.
 * The delimiter is replaced with a terminating '\0'.
 *
 * The input str is also modified to point to
 * the first character after to delimiter, i.e.
 * str is adjusted to point to the start of
 * the next token after the one just returned.
 *
 * Note that the original input string is destroyed
 * in the course of breaking-off tokens.
 */
char *
my_strtok(char **str, char delimiter)
{
	char *temp, *temp1;
	if (*str == NULL)
		return (NULL);

	temp = (char *)strchr(*str, delimiter);
	if (temp == NULL) {
		temp = *str;
		*str = NULL;
	} else {
		temp1 = temp + sizeof (char);
		*temp = '\0';
		temp = *str;
		*str = temp1;

	}
	return (temp);
}


/*
 * Remove element from a list in which the elements are separated by ','.
 * Return a pointer to the new list.
 *
 * Note: This function cannot return memory allocation errors since that
 *       cannot be distinguished from a case where the resulting list is
 *       NULL (either because a NULL list is passed in or the single name
 *       in the list got deleted). Therefore, this function will cause rgmd
 *       (and the node) to die in case of memory allocation failures.
 */
char *
name_delete(const char *name, char *s)
{
	char *nm, *ptr;
	char *tmp_s = NULL;

	if (s == NULL) {
		ptr = strdup("");
		return (ptr);
	}

	//
	// Copy the old string, minus the token 'name', to a new chunk of
	// memory.
	//
	while ((nm = (char *)my_strtok(&s, RGM_VALUE_SEPARATOR)) != NULL) {
		if (strcmp(name, nm) == 0)
			/* this is the name we're deleting; skip over it */
			continue;

		if (tmp_s == NULL) {
			tmp_s = strdup(nm);
			continue;
		}

		// We need to add two bytes: one for the RGM_VALUE_SEPARATOR,
		// one for the terminating null character
		if ((ptr = (char *)malloc(strlen(tmp_s) + strlen(nm) + 2))
		    == NULL)
			return (NULL);

		(void) sprintf(ptr, "%s"RGM_VALUE_SEPARATOR_STR"%s", tmp_s, nm);

		// free the old chunk of memory
		free(tmp_s);

		// point to the new chunk of memory
		tmp_s = ptr;
	}

	if (tmp_s == NULL) {
		ptr = strdup("");
		return (ptr);
	} else
		return (tmp_s);
}
