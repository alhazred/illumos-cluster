//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// rgm_sconf.cc - Duplicate some methods from libscconf.
//		  We cannot call libscconf because this
//		  would introduce a circular dependency
//		  with librgm.
//

#pragma ident	"@(#)rgm_scconf.cc	1.5	08/05/20 SMI"

#include <sys/cladm_int.h>
#include <sys/clconf_int.h>
#include <rgm/scha_priv.h>
#include <rgm/rgm_common.h>
#include <dlscxcfg.h>

//
// Check installmode property using libclconf
//
scha_errmsg_t
check_installmode()
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	const char *propval;

	if (clconf_lib_init() != 0) {
		res.err_code = SCHA_ERR_INTERNAL;
		return (res);
	}

	if ((clconf = clconf_cluster_get_current()) == NULL) {
		res.err_code = SCHA_ERR_INTERNAL;
		return (res);
	}

	// Get the installmode property
	propval = clconf_obj_get_property((clconf_obj_t *)clconf,
	    "installmode");

	// If the property is not set, default is "disabled"
	if (propval && strcmp(propval, "enabled") == 0) {
		res.err_code = SCHA_ERR_INSTLMODE;
		return (res);
	}

	return (res);
}

//
// Retrieve nodeid using libclconf
// (Imported from libscconf)
//
// This function returns int, to be consistent with rgm_get_nodeid()
// and scconf_get_nodeid().
//
int
get_nodeid(char *nodename, uint_t *nodeidp)
{
	clconf_cluster_t *clconf = NULL;
	int res = SCHA_ERR_NOERR;
	uint_t nodeid = 0;
	uint_t id;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	const char *name;
	char *s;

	/* Must be root */
	if (getuid() != 0) {
		res = SCHA_ERR_ACCESS;
		goto cleanup; //lint !e801
	}

	/* Initialize libclconf */
	if (clconf_lib_init() != 0) {
		res = SCHA_ERR_INTERNAL;
		goto cleanup; //lint !e801
	}

	/* Check arguments. */
	if (nodeidp == NULL) {
		res = SCHA_ERR_INTERNAL;
		goto cleanup; //lint !e801
	}


	/* If nodename is NULL, use _cladm() sys call to get our nodeid */
	if (nodename == NULL) {
		if (cladm(CL_CONFIG, CL_NODEID, nodeidp) != 0) {
			res = SCHA_ERR_INTERNAL;
			goto cleanup; //lint !e801
		}
		res = SCHA_ERR_NOERR;
		goto cleanup; //lint !e801

	}

	/* Get the clconf */
	clconf = clconf_cluster_get_current();
	if (clconf == NULL) {
		res = SCHA_ERR_INTERNAL;
		goto cleanup; //lint !e801
	}

	/* Check to see if nodename is a nodeid (number > 0) */
	for (s = nodename;  *s;  ++s)
		if (!isdigit(*s)) //lint !e1055 !e746
			break;
	if (*s == '\0') {
		nodeid = (uint_t)atoi(nodename);
		if (nodeid < 1) {
			res = SCHA_ERR_INVAL;
			goto cleanup; //lint !e801
		}
	}

	/* Iterate through the list of configured nodes */
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {

		id = (uint_t)clconf_obj_get_id(node);
		if (id == (uint_t)-1) {
			res = SCHA_ERR_INTERNAL;
			goto cleanup; //lint !e801
		}

		name = clconf_obj_get_name(node);

		/* If nodeid is set, search for configured nodeid */
		if (nodeid > (uint_t)0) {
			if (nodeid == id) {
				*nodeidp = nodeid;
				res = SCHA_ERR_NOERR;
				goto cleanup; //lint !e801
			}

			/* Otherwise, search for a nodename match */
		} else if (name != NULL && strcmp(nodename, name) == 0) {
			*nodeidp = id;
			res = SCHA_ERR_NOERR;
			goto cleanup; //lint !e801
		}
		clconf_iter_advance(currnodesi);
	}

	res = SCHA_ERR_NODE;

	/* If europa is enabled, we also need to look for this nodename */
	/* in the farm nodes list */

	if (clconf_is_farm_mgt_enabled()) {
		void *instance;
		scxcfg_api_t api;
		scxcfg_error error;
		int res2;
		scxcfg cfg;

		if (scxcfg_load_module(&instance, &api) == -1) {
			res = SCHA_ERR_INTERNAL;
			goto cleanup_europa; //lint !e801
		}

		res2 = api.scxcfg_open(&cfg, &error);
		if (res2 != FCFG_OK && res2 != FCFG_EEXIST) {
			res = SCHA_ERR_INTERNAL;
			goto cleanup_europa; //lint !e801
		}

		switch (api.scxcfg_get_nodeid(&cfg, nodename, nodeidp,
		    &error)) {
		case FCFG_OK:
			res = SCHA_ERR_NOERR;
			break;
		case FCFG_ENOTFOUND:
			res = SCHA_ERR_NODE;
			break;
		default:
			res = SCHA_ERR_INTERNAL;
			break;
		}

cleanup_europa:
		(void) api.scxcfg_close(&cfg);
	}

cleanup:
	/* Release the clconf */
	if (clconf != NULL) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	return (res);
}
