//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident   "@(#)rgm_client_util.cc 1.8     08/05/20 SMI"

#include <sys/types.h>
#include <rgm/rgm_cmn_proto.h>

//
// Utility functions used by clients of rgmd.
//


//
// fail_ccr_read_write
//
// called by FAULT_POINT_CCR_READ_WRITE macro for rgmd clients
//
// Function to simulate CCR read/update failures. This is a no-op
// (returns B_FALSE) unless compiled with the DEBUG flag.
//
// It checks if there exists a file /tmp/rgm_debug_ccr. If so, it returns
// SCHA_ERR_CCR for every Nth CCR read/write call. The number N can be
// specified in the file and defaults to 3.
//
boolean_t
fail_ccr_read_write(void)
{
#ifdef DEBUG
	static int callnum = 0;
	FILE *fd;
	int callnum_to_fail = 0;

	if ((fd = fopen("/tmp/rgm_debug_ccr", "r")) != NULL) {
		(void) fscanf(fd, "%d", &callnum_to_fail);
		(void) fclose(fd);
		if (callnum_to_fail <= 0)
			callnum_to_fail = 3 /* default */;
		callnum++;
		if (callnum == callnum_to_fail) {
			// Reset callnum and cause the call to fail.
			callnum = 0;
			return (B_TRUE);
		}
	}

#endif
	return (B_FALSE);
}
