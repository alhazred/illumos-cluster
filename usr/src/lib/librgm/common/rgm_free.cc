/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * rgm_free.cc - free functions
 */


#pragma ident	"@(#)rgm_free.cc	1.30	09/01/28 SMI"

#include <stdlib.h>
#include <rgm/rgm_cpp.h>
#include <string>
#include <map>


void
rgm_free_rg(rgm_rg_t *rg)
{
	if (rg) {
		if (rg->rg_name)
			free(rg->rg_name);
		if (rg->rg_nodelist)
			rgm_free_nodeid_list(rg->rg_nodelist);
		if (rg->rg_dependencies)
			rgm_free_nlist(rg->rg_dependencies);
		if (rg->rg_glb_rsrcused.names)
			rgm_free_nlist(rg->rg_glb_rsrcused.names);
		if (rg->rg_pathprefix)
			free(rg->rg_pathprefix);
		if (rg->rg_project_name)
			free(rg->rg_project_name);
		if (rg->rg_affinities)
			rgm_free_nlist(rg->rg_affinities);
		if (rg->rg_affinitents)
			rgm_free_nlist(rg->rg_affinitents);
		if (rg->rg_description)
			free(rg->rg_description);
		if (rg->rg_stamp)
			free(rg->rg_stamp);
// SC SLM addon start
		if (rg->rg_slm_type)
			free(rg->rg_slm_type);
		if (rg->rg_slm_pset_type)
			free(rg->rg_slm_pset_type);
// SC SLM addon end

		free(rg);
	}
}

void
rgm_free_rt(rgm_rt_t *rt)
{
	if (rt) {
		if (rt->rt_name)
			free(rt->rt_name);
		if (rt->rt_basedir)
			free(rt->rt_basedir);
		if (rt->rt_methods.m_start)
			free(rt->rt_methods.m_start);
		if (rt->rt_methods.m_stop)
			free(rt->rt_methods.m_stop);
		if (rt->rt_methods.m_validate)
			free(rt->rt_methods.m_validate);
		if (rt->rt_methods.m_update)
			free(rt->rt_methods.m_update);
		if (rt->rt_methods.m_init)
			free(rt->rt_methods.m_init);
		if (rt->rt_methods.m_fini)
			free(rt->rt_methods.m_fini);
		if (rt->rt_methods.m_boot)
			free(rt->rt_methods.m_boot);
		if (rt->rt_methods.m_monitor_start)
			free(rt->rt_methods.m_monitor_start);
		if (rt->rt_methods.m_monitor_stop)
			free(rt->rt_methods.m_monitor_stop);
		if (rt->rt_methods.m_monitor_check)
			free(rt->rt_methods.m_monitor_check);
		if (rt->rt_methods.m_prenet_start)
			free(rt->rt_methods.m_prenet_start);
		if (rt->rt_methods.m_postnet_stop)
			free(rt->rt_methods.m_postnet_stop);
		if (rt->rt_instl_nodes.nodeids)
			rgm_free_nodeid_list(rt->rt_instl_nodes.nodeids);
		if (rt->rt_dependencies)
			rgm_free_nlist(rt->rt_dependencies);
		if (rt->rt_version)
			free(rt->rt_version);
		if (rt->rt_upgrade_from)
			rgm_free_upglist(rt->rt_upgrade_from);
		if (rt->rt_downgrade_to)
			rgm_free_upglist(rt->rt_downgrade_to);
		if (rt->rt_pkglist)
			rgm_free_nlist(rt->rt_pkglist);
		if (rt->rt_paramtable)
			rgm_free_paramtable(rt->rt_paramtable);
		if (rt->rt_description)
			free(rt->rt_description);
		if (rt->rt_stamp)
			free(rt->rt_stamp);

		free(rt);
	}
}

void
rgm_free_upglist(rgm_rt_upgrade_t *upg)
{
	rgm_rt_upgrade_t *nxp;

	while (upg) {
		nxp = upg->rtu_next;
		if (upg->rtu_version)
			free(upg->rtu_version);
		free(upg);
		upg = nxp;
	}
}

void
rgm_free_property(rgm_property_t *p)
{

	if (p == NULL)
		return;

	if (p->rp_key)
		free(p->rp_key);

	if (p->rp_description)
		free(p->rp_description);

	if (p->rp_type == SCHA_PTYPE_STRINGARRAY)
		rgm_free_nlist(p->rp_array_values);
	else {
		if (p->is_per_node == B_TRUE) {
			std::map<rgm_lni_t, char *>::const_iterator it;
			for (it = ((rgm_value_t *)p->rp_value)
			    ->rp_value.begin(); it != ((rgm_value_t *)p
			    ->rp_value)->rp_value.end(); ++it) {
				if (it->second != NULL)
					free(it->second);
			}
			std::map<std::string, char *>::const_iterator jt;
			for (jt = ((rgm_value_t *)p->rp_value)
			    ->rp_value_node.begin(); jt != ((rgm_value_t *)p
			    ->rp_value)->rp_value_node.end(); ++jt) {
				if (jt->second != NULL)
					free(jt->second);
			}
		} else {
			if (((rgm_value_t *)p->rp_value)->rp_value[0] != NULL)
				free(((rgm_value_t *)p->rp_value)
				    ->rp_value[0]);
		}
		delete ((rgm_value_t *)p->rp_value);
	}
	free(p);
}

void
rgm_free_proplist(rgm_property_list_t *pl)
{
	rgm_property_list_t *nx_pl;

	while (pl) {
		nx_pl = pl->rpl_next;
		rgm_free_property(pl->rpl_property);
		free(pl);
		pl = nx_pl;
	}
}

void
rgm_free_resource(rgm_resource_t *r)
{

	if (r) {

		if (r->r_name)
			free(r->r_name);
		if (r->r_type)
			free(r->r_type);
		if (r->r_type_version)
			free(r->r_type_version);
		if (r->r_rgname)
			free(r->r_rgname);
		if (r->r_project_name)
			free(r->r_project_name);

		if (r->r_dependencies.dp_restart)
			rgm_free_rdeplist(r->r_dependencies.dp_restart);
		if (r->r_dependencies.dp_weak)
			rgm_free_rdeplist(r->r_dependencies.dp_weak);
		if (r->r_dependencies.dp_strong)
			rgm_free_rdeplist(r->r_dependencies.dp_strong);
		if (r->r_dependencies.dp_offline_restart)
			rgm_free_rdeplist(r->r_dependencies.dp_offline_restart);
		// free system-defined and extenston properties
		rgm_free_proplist(r->r_properties);
		rgm_free_proplist(r->r_ext_properties);


		if (r->r_description)
			free(r->r_description);
		delete ((rgm_switch_t *)r->r_onoff_switch);
		delete ((rgm_switch_t *)r->r_monitored_switch);
		free(r);
	}

}

void
rgm_free_nlist(namelist_t *l)
{
	namelist_t *nx_l;

	while (l) {
		nx_l = l->nl_next;

		if (l->nl_name)
			free(l->nl_name);
		free(l);
		l = nx_l;
	}
}


void
rgm_free_rdeplist(rdeplist_t *l)
{
	rdeplist_t *nx_l;

	while (l) {
		nx_l = l->rl_next;

		if (l->rl_name)
			free(l->rl_name);
		free(l);
		l = nx_l;
	}
}

void
rgm_free_nodeid_list(nodeidlist_t *l)
{
	nodeidlist_t *nx_l;

	while (l) {
		nx_l = l->nl_next;

		if (l->nl_zonename) {
			free(l->nl_zonename);
		}

		free(l);
		l = nx_l;
	}
}


void
rgm_free_nlist_all(namelist_all_t *l)
{
	if (l) {
		rgm_free_nlist(l->names);
		free(l);
	}
}

void
rgm_free_paramtable(rgm_param_t **paramtable)
{
	rgm_param_t **param;

	if (paramtable == NULL || *paramtable == NULL)
		return;

	for (param = paramtable; *param; param++) {
		if ((*param)->p_name)
			free((*param)->p_name);

		if ((*param)->p_defaultstr)
			free((*param)->p_defaultstr);

		if ((*param)->p_defaultarray)
			rgm_free_nlist((*param)->p_defaultarray);

		if ((*param)->p_enumlist)
			rgm_free_nlist((*param)->p_enumlist);

		if ((*param)->p_description)
			free((*param)->p_description);

		free(*param);
	}

	free(paramtable);
}

void
rgm_free_stamp(rgm_stamp_t stmp)
{
	if (stmp)
		free(stmp);
}

void
rgm_free_strarray(char **sa)
{
	char **ptr;

	if (sa == NULL)
		return;

	for (ptr = sa; *ptr; ptr++)
		free(*ptr);

	free(sa);
}
