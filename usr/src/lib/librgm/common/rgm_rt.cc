/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_rt.cc	1.32	08/06/20 SMI"

//
// rgm_rt.cc
//
// The call_scrgadm_xxx() functions for RTs are called directly by scrgadm
// to the president node.  The caller provides storage for both arguments.
// The IDL invocation on the president node fills in the result structure.
//

#include <rgm/rgm_cmn_proto.h>
#include <rgm/rgm_call_pres.h>
#include <rgm/rgm_errmsg.h>
#include <rgm/sczones.h>
#if SOL_VERSION >= __s10
#include <sys/clconf_int.h>
#endif


//
// call_scrgadm_update_rt_instnodes
//
// Calls on the President to update the installed nodes list of an RT.
//
scha_errmsg_t
call_scrgadm_update_rt_instnodes(const char *rt_name,
	const char **inst_nodes, const char *cluster_name)
{
	rgm::idl_rt_update_instnode_args	arg;
	rgm::idl_regis_result_t_var		result_v;
	rgm::rgm_comm_var prgm_pres_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;

	//
	// This call is allowed only from two contexts. One is from the
	// global zone and another is from a zone cluster context.
	//
#if SOL_VERSION >= __s10
	if (sc_nv_zonescheck() != 0) {
		//
		// We are inside a zone. We have to check whether we
		// are inside a zone cluster or not
		//
		if (clconf_inside_virtual_cluster() == B_FALSE) {
			res.err_code = SCHA_ERR_ACCESS;
			rgm_format_errmsg(&res, REM_ONLY_GZ_ZC_ALLOWED);
			return (res);
		}
	}
#endif

	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc((char *)cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		return (res);
	}

	arg.idlstr_rt_name = new_str(rt_name);

	// Marshal node names from inst_nodes into arg
	if (inst_nodes == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}
	array_to_strseq(inst_nodes, arg.node_names);

	/* Set the msg locale */
	arg.locale = new_str(fetch_msg_locale());

	// Make the IDL call
	prgm_pres_v->idl_scrgadm_rt_update_instnodes(arg, result_v, e);

	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
		return (res);

	res.err_code = (scha_err_t)result_v->ret_code;
	if ((const char *)result_v->idlstr_err_msg)
		res.err_msg = strdup(result_v->idlstr_err_msg);
	return (res);
}


//
// call_scrgadm_rt_unregister
//
// Calls on the President to remove an RT from the RGM configuration.
//
scha_errmsg_t
call_scrgadm_rt_unregister(const char *rt_name, const char *cluster_name)
{
	rgm::idl_regis_result_t_var result_v;
	rgm::rgm_comm_var prgm_pres_v;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;

	//
	// This call is allowed only from two contexts. One is from the
	// global zone and another is from a zone cluster context.
	//
#if SOL_VERSION >= __s10
	if (sc_nv_zonescheck() != 0) {
		//
		// We are inside a zone. We have to check whether we
		// are inside a zone cluster or not
		//
		if (clconf_inside_virtual_cluster() == B_FALSE) {
			res.err_code = SCHA_ERR_ACCESS;
			rgm_format_errmsg(&res, REM_ONLY_GZ_ZC_ALLOWED);
			return (res);
		}
	}
#endif

	if (allow_zc())
		prgm_pres_v = rgm_comm_getpres_zc((char *)cluster_name);
	else
		prgm_pres_v = rgm_comm_getpres();
	if (CORBA::is_nil(prgm_pres_v)) {
		res.err_code = SCHA_ERR_CLRECONF;
		rgm_format_errmsg(&res, REM_RGM_NOT_STARTED);
		return (res);
	}

	// Ask the President to remove this RT from the RGM configuration.
	prgm_pres_v->idl_scrgadm_rt_delete(rt_name,
	    fetch_msg_locale(), result_v, e);

	if ((res = except_to_scha_err(e)).err_code != SCHA_ERR_NOERR)
		return (res);

	res.err_code = (scha_err_t)result_v->ret_code;
	if ((const char *)result_v->idlstr_err_msg)
		res.err_msg = strdup(result_v->idlstr_err_msg);
	return (res);
}
