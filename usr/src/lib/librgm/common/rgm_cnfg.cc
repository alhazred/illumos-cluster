//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//
// rgm_cnfg.cc - API for storing/retrieving RGM configuration in CCR
//
// This file is used both to build the librgm library linked with the main
// RGMD daemon running on server nodes (Sun Cluster or Europa), and to build
// the ligthweight FRGMD daemon running on Europa farm nodes.
//
// Only a subset of the utility functions are needed for the frgmd daemon and
// therefore all the functions that are used only by the rgmd running on a
// server node will be #ifndef'ed with "EUROPA_FARM" compilation flag.
//
// On Server side: the librgm.so.1 shared object is generated.
// It contains the // full RGM/CCR API plus all the utility functions.
// On Farm side: the librgm.so.1 object does not exist. The rgm_cnfg.o file
// containing a subset of the utility functions needed by the RGM/CCR API
// is directly linked with librgmfarm.a, and therefore with frgmd.
//
// The API for storing/retrieving RGM configuration in CCR is implemented for
// Europa farm RGM server in ./lib/librgmserver/common/rgmx_cnfg.cc.
//
// List of common functions between rgmd and frgmd:
//
// + namelist_copy
// + property_list_copy
// + rgmcnfg_rs_exist
// + rgmcnfg_is_ok_start_rg
//

#pragma ident	"@(#)rgm_cnfg.cc	1.125	08/07/28 SMI"

using namespace std;
#include <string> //lint !e322
#include <map> //lint !e322
#include <syslog.h>
#include <sys/os.h>

#ifndef EUROPA_FARM
#include <orb/fault/fault_injection.h>
#endif

#include <h/ccr.h>
#include <h/version_manager.h>
#include <nslib/ns.h>
#ifdef EUROPA_FARM
#include <cmm/ucmm_api.h>
#endif
#include <rgm/rgm_cnfg.h>
#include <rgm/rgm_cmn_proto.h>

#ifndef EUROPA_FARM
#include <rgm/rgm_msg.h>
#endif

#include <rgm/rgm_private.h>
#include <sys/sc_syslog_msg.h>

#ifndef EUROPA_FARM
#include <rgm/rgm_errmsg.h>
#include <scadmin/scconf.h>
#endif

#include <rgm/sczones.h>
#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <zone.h>
#endif

#ifndef EUROPA_FARM
static void FAULT_POINT_CCR_TABLE(const char *tablename, const char *operation);
static boolean_t is_ccr_table_empty(const char *tablename, const char *zc_name);
static scha_errmsg_t open_ccr_table(char *objname, rgm_obj_type_t objtype,
    ccr::readonly_table_var &tabptr, const char *zc_name);

//
// rgmcnfg_new_property_from_param
//
// Helper function for creating resource property structures.
// See details in header comments of function definition.
//
static scha_errmsg_t rgmcnfg_new_property_from_param(
    rgm_param_t **prop, rgm_property_list_t **ptr_p);
#endif

//
// namelist_copy & property_list_copy
// ----------------------------------
// Helper functions for copying namelists and property lists, which are
// list structures used in an rgm_resource_t structure.
//
namelist_t *namelist_copy(const namelist_t *src);
rgm_property_list_t *property_list_copy(const rgm_property_list_t *src);

//
// switch_copy & switch_create
// ---------------------------
// Helper function for copying scha_switch_t array of On_off_switch
// and Monitored_switch in an rgm_resource_t structure and also creating
// an scha_switch_t array of SCHA_SWITCH_DISABLED for resource creation.
//
void switch_copy(const rgm_resource_t *src,
    const rgm_resource_t *des);
#ifndef EUROPA_FARM

//
// Look up the CCR directory in the local name server. Once looked up, the
// reference is cached. This function should never return a NULL pointer.
//
static ccr::directory_var
lookup_ccrdir()
{
	static ccr::directory_var ccr_directory_var = ccr::directory::_nil();
	static os::mutex_t ccr_mutex;

	if (CORBA::is_nil(ccr_directory_var)) {

		ccr_mutex.lock();

		//
		// With the lock held, check again if the pointer is NULL.
		// If it is still NULL, look up the reference from the name
		// server. If not, just return the pointer.
		//
		if (!CORBA::is_nil(ccr_directory_var)) {
			ccr_mutex.unlock();
			return (ccr_directory_var);
		}

		naming::naming_context_var ctxp = ns::local_nameserver();
		Environment e;
		CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
		if (e.exception()) {
			sc_syslog_msg_handle_t handle;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// The rgmd daemon was unable to open the cluster
			// configuration repository (CCR). The rgmd will
			// produce a core file and will force the node to halt
			// or reboot to avoid the possibility of data
			// corruption.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("fatal: Unable to open CCR"));
			sc_syslog_msg_done(&handle);
			abort();
			// NOTREACHABLE
		} else {
			ccr_directory_var = ccr::directory::_narrow(obj);
			CL_PANIC(!CORBA::is_nil(ccr_directory_var));
		}

		ccr_mutex.unlock();
	}

	return (ccr_directory_var);
}

//
// get_rsrc_cnfg -
//	call ccr_get_resource function to get the resource configuration data
//	ccr_get_resource function will read the resource data from CCR
//
static scha_errmsg_t
get_rsrc_cnfg(const ccr::readonly_table_var &tabptr, name_t resourcename,
	const char *rgname, rgm_resource_t **resource, const char *zc_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	*resource = (rgm_resource_t *)malloc_nocheck(sizeof (rgm_resource_t));
	if (*resource == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}
	bzero((char *)(*resource), sizeof (rgm_resource_t));
	(*resource)->r_onoff_switch = new(nothrow) rgm_switch_t;
	if ((*resource)->r_onoff_switch == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_free_resource(*resource);
		*resource = NULL;
		return (res);
	}

	(*resource)->r_monitored_switch = new(nothrow) rgm_switch_t;
	if ((*resource)->r_monitored_switch == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_free_resource(*resource);
		*resource = NULL;
		return (res);
	}
	(*resource)->r_name = strdup_nocheck(resourcename);
	if ((*resource)->r_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_free_resource(*resource);
		*resource = NULL;
		return (res);
	}
	(*resource)->r_rgname = strdup_nocheck(rgname);
	if ((*resource)->r_rgname == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_free_resource(*resource);
		*resource = NULL;
		return (res);
	}

	// Query the keyword with resource name
	res = ccr_get_resource(tabptr, (const char *)resourcename, resource,
	    zc_name);
	if (res.err_code != SCHA_ERR_NOERR) {
		rgm_free_resource(*resource);
		*resource = NULL;
	}

	return (res);
} //lint !e818

int
debug_print(const rgm_value_t *rp_value)
{
	std::map<std::string, char *>::iterator it;
	std::map<std::string, char *> p;
	p = rp_value->rp_value_node;
	for (it = p.begin(); it != p.end(); ++it)
		(void) printf(" %s %s\n", it->first.data(), it->second);
	(void) printf("%p\n", p["0"]);
	return (0);
}

//
// apply_rt_defaults
//
// Loads the resource type info specified by resource->r_type.
// Adds to the resource a property structure for every property (with a
// default in the paramtable of the resource type) that is not already
// in the property list of the resource.  Does this for both system and
// extension properties.
//
// Does not overwrite properties already in the resource with the defaults.
// It is expected that the resource will contain properties read from the CCR
// (those that the user explicitly entered).  We are trying to fill in the
// defaults for the properties for which the user has not entered values.
//
scha_errmsg_t
rgmcnfg_apply_rt_defaults(rgm_resource_t *resource, const char *zc_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rgm_rt_t *rt = NULL;
	boolean_t prop_found;
	rgm_property_list_t *ptr = NULL;
	rgm_param_t **prop = NULL;

	//
	// get the resource type information.
	//
	res = rgmcnfg_read_rttable(resource->r_type, &rt, zc_name);
	if (res.err_code != SCHA_ERR_NOERR) {
		return (res);
	}
	prop = rt->rt_paramtable;

	//
	// Iterate through the paramtable of the RT
	//
	for (; prop && *prop; ++prop) {

		// First, check if we already have this property.
		// If so, we don't want to override it with the defaults.

		prop_found = B_FALSE;
		if ((*prop)->p_extension) {
			if ((ptr = rgm_find_property_in_list((*prop)->p_name,
			    resource->r_ext_properties)) != NULL) {
				prop_found = B_TRUE;
			}
		} else {
			if (rgm_find_property_in_list((*prop)->p_name,
			    resource->r_properties) != NULL) {
				prop_found = B_TRUE;
			}
		}

		//
		// If we didn't find the property, then we need to add
		// in the default value from the paramtable.  If we found
		// the property and the property is not pernode,
		// skip that part, and continue to the next property.
		// In case of per-node extension property, the default value is
		// stored in "0" the index of the map rp_value_node.
		//
		if (prop_found) {
			if (!(*prop)->p_per_node)
				continue;
			if ((*prop)->p_default_isset == B_TRUE) {
				switch ((*prop)->p_type) {
				case SCHA_PTYPE_STRING:
				case SCHA_PTYPE_ENUM:
					if ((*prop)->p_defaultstr == NULL)
						continue;
					((rgm_value_t *)ptr->rpl_property->
					    rp_value)->rp_value_node["0"] =
					    strdup_nocheck((*prop)->
					    p_defaultstr);
					if (((rgm_value_t *)ptr->rpl_property->
					    rp_value)
					    ->rp_value_node["0"] == NULL) {
						res.err_code = SCHA_ERR_NOMEM;
						return (res);
					}
					break;
				case SCHA_PTYPE_UINT:
				case SCHA_PTYPE_INT:
					((rgm_value_t *)ptr->rpl_property->
					    rp_value)->rp_value_node["0"] =
					    rgm_itoa((*prop)->p_defaultint);
					if (((rgm_value_t *)ptr->rpl_property->
					    rp_value)
					    ->rp_value_node["0"] == NULL) {
						res.err_code = SCHA_ERR_NOMEM;
						return (res);
					}
					break;
				case SCHA_PTYPE_BOOLEAN:
					((rgm_value_t *)ptr->rpl_property->
					    rp_value)->rp_value_node["0"] =
					    strdup_nocheck(bool2str(
					    (*prop)->p_defaultbool));
					if (((rgm_value_t *)ptr->rpl_property->
					    rp_value)
					    ->rp_value_node["0"] == NULL) {
						res.err_code = SCHA_ERR_NOMEM;
						return (res);
					}
					break;
				case SCHA_PTYPE_STRINGARRAY:
				case SCHA_PTYPE_UINTARRAY:
				default:
					ASSERT(0);
				}
			}
			continue;
		}

		//
		// See if we have a default.  If we don't, it's
		// an internal error, because the user should have
		// entered one on creation, and we should have saved
		// it to the CCR at that time.
		//
		if (!(*prop)->p_default_isset) {
			sc_syslog_msg_handle_t handle;
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			//
			// SCMSGS
			// @explanation
			// A non-fatal internal error has occurred in the RGM.
			// @user_action
			// Since this problem might indicate an internal logic
			// error in the rgmd, save a copy of the
			// /var/adm/messages files on all nodes, and the
			// output of clresourcetype show -v, clresourcegroup
			// show -v +, and clresourcegroup status +. Report the
			// problem to your authorized Sun service provider.
			//
			(void) sc_syslog_msg_log(handle, LOG_ERR,
			    MESSAGE, SYSTEXT("Internal error: default "
			    "value missing for resource property"));
			sc_syslog_msg_done(&handle);

			res.err_code = SCHA_ERR_INTERNAL;
			goto finished; //lint !e801
		}

		//
		// Call a helper function to actually create the
		// rgm_resource_list_t.
		//
		res = rgmcnfg_new_property_from_param(prop, &ptr);
		if (res.err_code != SCHA_ERR_NOERR) {
			goto finished; //lint !e801
		}

		//
		// Add the new property to the head of its respective
		// list
		//
		if ((*prop)->p_extension == B_FALSE) {
			ptr->rpl_next = resource->r_properties;
			resource->r_properties = ptr;
		} else {
			ptr->rpl_next = resource->r_ext_properties;
			resource->r_ext_properties = ptr;
		}
	}
finished:
	rgm_free_rt(rt);
	return (res);
}

//
// rgmcnfg_get_resource
//
// A wrapper function that calls rgmcnfg_get_resource_no_defaults to
// read the actual stuff that's stored in the CCR, then calls apply_rt_defaults
// to add in the resource type defaults to the resource in memory.
//
scha_errmsg_t
rgmcnfg_get_resource(name_t resourcename, name_t rgname,
    rgm_resource_t **resource, const char *zc_name)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	// read the resource from the CCR
	res = rgmcnfg_get_resource_no_defaults(resourcename, rgname, resource,
	    zc_name);
	if (res.err_code != SCHA_ERR_NOERR) {
		return (res);
	}

	// apply the resource type defaults
	res = rgmcnfg_apply_rt_defaults(*resource, zc_name);
	return (res);
} //lint !e818

//
// switch_copy
// -----------
//
// Copies src to des if either is not NULL
//
void switch_copy(const rgm_resource_t *src,
    const rgm_resource_t *des)
{
	std::map<std::string, scha_switch_t>::const_iterator jt;
	for (jt = ((rgm_switch_t *)src->r_onoff_switch)->r_switch_node.begin();
	    jt !=
	    ((rgm_switch_t *)src->r_onoff_switch)->r_switch_node.end(); ++jt) {
		((rgm_switch_t *)des->r_onoff_switch)
		    ->r_switch_node[jt->first] = jt->second;
	}
	for (jt =
	    ((rgm_switch_t *)src->r_monitored_switch)->r_switch_node.begin(); jt
	    != ((rgm_switch_t *)src->r_monitored_switch)->r_switch_node.end();
	    ++jt) {
		((rgm_switch_t *)des->r_monitored_switch)->
		    r_switch_node[jt->first] = jt->second;
	}


	std::map<rgm_lni_t, scha_switch_t>::const_iterator it;
	for (it = ((rgm_switch_t *)src->r_onoff_switch)->r_switch.begin(); it !=
	    ((rgm_switch_t *)src->r_onoff_switch)->r_switch.end(); ++it) {
		((rgm_switch_t *)des->r_onoff_switch)->r_switch[it->first] =
		    it->second;
	}
	for (it = ((rgm_switch_t *)src->r_monitored_switch)->r_switch.begin();
	    it !=
	    ((rgm_switch_t *)src->r_monitored_switch)->r_switch.end(); ++it) {
		((rgm_switch_t *)des->r_monitored_switch)->r_switch[it->first] =
		    it->second;
	}

}

//
// rgmcnfg_get_resource_no_defaults -
//	open the RG table that the R belongs to in CCR and call
//	get_rsrc_cnfg function to get the resource data
//
scha_errmsg_t
rgmcnfg_get_resource_no_defaults(name_t resourcename, name_t rgname,
	rgm_resource_t **resource, const char *zone_name)
{
	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_1,
		FaultFunctions::generic);

	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	ccr::readonly_table_var tabptr;
	char *cluster_name = NULL;

	if ((cluster_name = get_clustername_from_zone(zone_name, &res)) == NULL)
		return (res);
	*resource = NULL;

	while (1) {
		res = open_ccr_table(rgname, O_RG, tabptr, cluster_name);
		if (res.err_code != SCHA_ERR_NOERR) {
			// Free memory
			if (cluster_name)
				free(cluster_name);
			return (res);
		}
		res = get_rsrc_cnfg(tabptr, resourcename,
		    (const char *)rgname, resource, zone_name);
		if (res.err_code == SCHA_ERR_SEQID) {
			//
			// The CCR table has been modified after it was
			// opened; retry after a small delay. See block
			// comments at the head of rgm_ccrget.cc for an
			// explanation of the SCHA_ERR_SEQID error returned
			// when the CCR is concurrently updated or deleted.
			//
			free(res.err_msg);
			(void) sleep(1 /* second */);
			continue;
		}
		// Free memory before returning
		if (cluster_name) {
			free(cluster_name);
			cluster_name = NULL;
		}

		return (res);
	}
}

#endif /* !EUROPA_FARM */

//
// namelist_copy
// -------------
//
// Iterates through the namelist of which src is the head, copying each
// element of the list, and creating a new list identical to the src list.
//
// Returns a pointer to the head of the new list, or NULL if there is a
// memory error.
//
namelist_t *
namelist_copy(const namelist_t *src)
{
	namelist_t *base = NULL, *new_cur = NULL, *next = NULL;

	for (; src != NULL; src = src->nl_next) {
		new_cur = (namelist_t *)malloc_nocheck(sizeof (namelist_t));
		if (new_cur == NULL) {
			rgm_free_nlist(base);
			return (NULL);
		}
		new_cur->nl_name = strdup_nocheck(src->nl_name);
		if (new_cur->nl_name == NULL) {
			rgm_free_nlist(base);
			free(new_cur);
			return (NULL);
		}
		//
		// Add the new namelist_t to the end of the list, so
		// we maintain the same list ordering as in the src list.
		//
		new_cur->nl_next = NULL;
		if (base == NULL) {
			base = new_cur;
		} else {
			next->nl_next = new_cur;
		}
		next = new_cur;
	}

	return (base);
}


//
// rdeplist_copy
// -------------
//
// Iterates through the rdeplist of which src is the head, copying each
// element of the list, and creating a new list identical to the src list.
//
// Returns a pointer to the head of the new list, or NULL if there is a
// memory error.
//
rdeplist_t *
rdeplist_copy(const rdeplist_t *src)
{
	rdeplist_t *base = NULL, *new_cur = NULL, *next = NULL;

	for (; src != NULL; src = src->rl_next) {
		new_cur = (rdeplist_t *)malloc_nocheck(sizeof (rdeplist_t));
		if (new_cur == NULL) {
			rgm_free_rdeplist(base);
			return (NULL);
		}
		new_cur->rl_name = strdup_nocheck(src->rl_name);
		if (new_cur->rl_name == NULL) {
			rgm_free_rdeplist(base);
			free(new_cur);
			return (NULL);
		}
		new_cur->locality_type = src->locality_type;
		//
		// Add the new rdeplist_t to the end of the list, so
		// we maintain the same list ordering as in the src list.
		//
		new_cur->rl_next = NULL;
		if (base == NULL) {
			base = new_cur;
		} else {
			next->rl_next = new_cur;
		}
		next = new_cur;
	}

	return (base);
}

//
// property_list_copy
// -------------
//
// Iterates through the rgm_property_list of which src is the head, copying
// each element of the list, and creating a new list identical to the src list.
//
// Returns a pointer to the head of the new list, or NULL if there is a
// memory error.
//
rgm_property_list_t *
property_list_copy(const rgm_property_list_t *src)
{
	rgm_property_list_t *base = NULL, *new_cur = NULL, *next = NULL;
	std::map<rgm::lni_t, name_t>  t;
	std::map<rgm::lni_t, char *>::iterator jt;

	for (; src != NULL; src = src->rpl_next) {
		new_cur = (rgm_property_list_t *)malloc_nocheck(
		    sizeof (rgm_property_list_t));
		if (new_cur == NULL) {
			rgm_free_proplist(base);
			return (NULL);
		}
		new_cur->rpl_property = (rgm_property_t *)malloc_nocheck(
		    sizeof (rgm_property_t));
		if (new_cur->rpl_property == NULL) {
			rgm_free_proplist(base);
			free(new_cur);
			return (NULL);
		}
		new_cur->rpl_property->rp_value = new(nothrow) rgm_value_t;
		if (new_cur->rpl_property->rp_value == NULL) {
			rgm_free_proplist(base);
			free(new_cur);
			return (NULL);
		}


		//
		// copy all the fields of the structure
		//
		new_cur->rpl_property->rp_key = strdup_nocheck(src->
		    rpl_property->rp_key);
		if (new_cur->rpl_property->rp_key == NULL) {
			rgm_free_proplist(base);
			free(new_cur->rpl_property);
			free(new_cur);
			return (NULL);
		}
		if (src->rpl_property->is_per_node) {
			new_cur->rpl_property->is_per_node = B_TRUE;
			std::map<rgm::lni_t, name_t>::iterator it;
			for (it = ((rgm_value_t *)src->rpl_property->rp_value)->
			    rp_value.begin();
			    it != ((rgm_value_t *)src->rpl_property->rp_value)->
			    rp_value.end(); ++it) {
				if (it->second != NULL) {
					t[it->first] = strdup_nocheck(
					    it->second);
					if (t[it->first] == NULL) {
						rgm_free_proplist(base);
						free(new_cur->rpl_property->
						    rp_key);
						free(new_cur->rpl_property);
						for (jt = t.begin(); jt !=
						    t.end(); ++jt)
							free(t[jt->first]);
						free(new_cur);
						return (NULL);
					}
				}
			}
			((rgm_value_t *)new_cur->rpl_property->rp_value)
			    ->rp_value = t;
		} else {
			new_cur->rpl_property->is_per_node = B_FALSE;
			if (((rgm_value_t *)src->rpl_property->rp_value)
			    ->rp_value[0] != NULL) {
				((rgm_value_t *)new_cur->rpl_property->rp_value)
				    ->rp_value[0] =
				    strdup_nocheck((const char*)((rgm_value_t *)
				    src->rpl_property->rp_value)->rp_value[0]);
				if (((rgm_value_t *)new_cur->rpl_property->
				    rp_value)->rp_value[0] == NULL) {
					rgm_free_proplist(base);
					free(new_cur->rpl_property->rp_key);
					free(new_cur->rpl_property);
					free(new_cur);
					return (NULL);
				}
			}
		}

		if (src->rpl_property->rp_description != NULL) {
			new_cur->rpl_property->rp_description = strdup_nocheck(
			    src->rpl_property->rp_description);
			if (new_cur->rpl_property->rp_description == NULL) {
				rgm_free_proplist(base);
				free(new_cur->rpl_property->rp_key);
				free(new_cur->rpl_property);
				free(new_cur);
				return (NULL);
			}
		} else {
			new_cur->rpl_property->rp_description = NULL;
		}

		if (src->rpl_property->rp_array_values != NULL) {
			new_cur->rpl_property->rp_array_values = namelist_copy(
			    src->rpl_property->rp_array_values);
			if (new_cur->rpl_property->rp_array_values == NULL) {
				rgm_free_proplist(base);
				free(new_cur->rpl_property->rp_description);
				//
				// Free'ing of the rp_value structure would be
				// done inside rgm_free_proplist.
				//
				free(new_cur->rpl_property->rp_key);
				free(new_cur->rpl_property);
				free(new_cur);
				return (NULL);
			}
		} else {
			new_cur->rpl_property->rp_array_values = NULL;
		}
		new_cur->rpl_property->rp_type = src->rpl_property->rp_type;

		//
		// Add the new rgm_property_list_t to the end of the list, so
		// we maintain the same list ordering as in the src list.
		//
		new_cur->rpl_next = NULL;
		if (base == NULL) {
			base = new_cur;
		} else {
			next->rpl_next = new_cur;
		}
		next = new_cur;
	}
	return (base);
}

#ifndef EUROPA_FARM
//
// rgmcnfg_copy_resource
// ---------------------
// Copies all fields of the rgm_resource_t, allocating new memory for
// everything.
//
scha_errmsg_t
rgmcnfg_copy_resource(const rgm_resource_t *src, rgm_resource_t **dest_p)
{
	*dest_p = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	rgm_resource_t *dest = (rgm_resource_t *)malloc_nocheck(
	    sizeof (rgm_resource_t));
	if (dest == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	(void) memset(dest, '\0', sizeof (rgm_resource_t));

	dest->r_onoff_switch = new(nothrow) rgm_switch_t;
	if (dest->r_onoff_switch == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	dest->r_monitored_switch = new(nothrow) rgm_switch_t;

	if (dest->r_monitored_switch == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}


	// copy the structure fields
	dest->r_name = strdup_nocheck(src->r_name);
	dest->r_type = strdup_nocheck(src->r_type);
	dest->r_type_version = strdup_nocheck(src->r_type_version);
	dest->r_rgname = strdup_nocheck(src->r_rgname);
	switch_copy(src, dest);
	dest->r_description = strdup_nocheck(src->r_description);
	dest->r_project_name = strdup_nocheck(src->r_project_name);

	//
	// check for NULL returns from strdup
	//
	if (dest->r_project_name == NULL || dest->r_description == NULL ||
	    dest->r_rgname == NULL || dest->r_type_version == NULL ||
	    dest->r_type == NULL || dest->r_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		rgm_free_resource(dest);
		return (res);
	}

	//
	// Copy the dependencies, checking for memory allocation failures.
	//
	if (src->r_dependencies.dp_restart != NULL) {
		dest->r_dependencies.dp_restart = rdeplist_copy(
		    src->r_dependencies.dp_restart);
		if (dest->r_dependencies.dp_restart == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			rgm_free_resource(dest);
			return (res);
		}
	} else {
		dest->r_dependencies.dp_restart = NULL;
	}

	if (src->r_dependencies.dp_offline_restart != NULL) {
		dest->r_dependencies.dp_offline_restart = rdeplist_copy(
		    src->r_dependencies.dp_offline_restart);
		if (dest->r_dependencies.dp_offline_restart == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			rgm_free_resource(dest);
			return (res);
		}
	} else {
		dest->r_dependencies.dp_offline_restart = NULL;
	}

	if (src->r_dependencies.dp_weak != NULL) {
		dest->r_dependencies.dp_weak = rdeplist_copy(
		    src->r_dependencies.dp_weak);
		if (dest->r_dependencies.dp_weak == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			rgm_free_resource(dest);
			return (res);
		}
	} else {
		dest->r_dependencies.dp_weak = NULL;
	}

	if (src->r_dependencies.dp_strong != NULL) {
		dest->r_dependencies.dp_strong = rdeplist_copy(
		    src->r_dependencies.dp_strong);
		if (dest->r_dependencies.dp_strong == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			rgm_free_resource(dest);
			return (res);
		}
	} else {
		dest->r_dependencies.dp_strong = NULL;
	}

	//
	// Copy the Inter cluster resource dependents, checking for
	// memory allocation failures.
	//
	if (src->r_inter_cluster_dependents.dp_restart != NULL) {
		dest->r_inter_cluster_dependents.dp_restart = rdeplist_copy(
		    src->r_inter_cluster_dependents.dp_restart);
		if (dest->r_inter_cluster_dependents.dp_restart == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			rgm_free_resource(dest);
			return (res);
		}
	} else {
		dest->r_inter_cluster_dependents.dp_restart = NULL;
	}

	if (src->r_inter_cluster_dependents.dp_offline_restart != NULL) {
		dest->r_inter_cluster_dependents.dp_offline_restart =
		    rdeplist_copy(
		    src->r_inter_cluster_dependents.dp_offline_restart);
		if (dest->r_inter_cluster_dependents.dp_offline_restart
		    == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			rgm_free_resource(dest);
			return (res);
		}
	} else {
		dest->r_inter_cluster_dependents.dp_offline_restart = NULL;
	}

	if (src->r_inter_cluster_dependents.dp_weak != NULL) {
		dest->r_inter_cluster_dependents.dp_weak = rdeplist_copy(
		    src->r_inter_cluster_dependents.dp_weak);
		if (dest->r_inter_cluster_dependents.dp_weak == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			rgm_free_resource(dest);
			return (res);
		}
	} else {
		dest->r_inter_cluster_dependents.dp_weak = NULL;
	}

	if (src->r_inter_cluster_dependents.dp_strong != NULL) {
		dest->r_inter_cluster_dependents.dp_strong = rdeplist_copy(
		    src->r_inter_cluster_dependents.dp_strong);
		if (dest->r_inter_cluster_dependents.dp_strong == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			rgm_free_resource(dest);
			return (res);
		}
	} else {
		dest->r_inter_cluster_dependents.dp_strong = NULL;
	}

	//
	// Copy the properties and extension properties,
	// checking for memory allocation failures.
	//
	if (src->r_properties != NULL) {
		dest->r_properties = property_list_copy(
		    (const rgm_property_list*)src->r_properties);
		if (dest->r_properties == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			rgm_free_resource(dest);
			return (res);
		}
	} else {
		dest->r_properties = NULL;
	}

	if (src->r_ext_properties != NULL) {
		dest->r_ext_properties = property_list_copy(
		    src->r_ext_properties);
		if (dest->r_ext_properties == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			rgm_free_resource(dest);
			return (res);
		}
	} else {
		dest->r_ext_properties = NULL;
	}

	*dest_p = dest;
	return (res);
}

//
// rgmcnfg_get_rsrclist -
// Get a list of all the resource names in a given RG. This function
// allocates the memory for the resource name list. The caller must
// de-allocate the memory.
// null rg name doesn't cause error. returns a null resource list
//
scha_errmsg_t
rgmcnfg_get_rsrclist(name_t rgname, namelist_t **rnames,
    const char *zonename)
{
	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_2,
		FaultFunctions::generic);


	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *cluster_name;
	ccr::readonly_table_var tabptr;

	*rnames = NULL;
	if (rgname == NULL) {
		return (res);
	}
	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);
	while (1) {
		res = open_ccr_table(rgname, O_RG, tabptr, cluster_name);
		if (res.err_code != SCHA_ERR_NOERR) {
			free(cluster_name);
			return (res);
		}
		res = ccr_get_nstring(tabptr, SCHA_RESOURCE_LIST, rnames);
		if (res.err_code == SCHA_ERR_SEQID) {
			//
			// The CCR table has been modified after it was
			// opened; retry after a small delay. See block
			// comments at the head of rgm_ccrget.cc for an
			// explanation of the SCHA_ERR_SEQID error returned
			// when the CCR is concurrently updated or deleted.
			//
			free(res.err_msg);
			(void) sleep(1 /* second */);
			continue;
		}
		free(cluster_name);
		return (res);
	}
}


//
// rgmcnfg_read_rgtable -
//	open the RG CCR table and query each RG property
//	Each property is stored in CCR table as follows:
//	<property name>	<property value>
//
scha_errmsg_t
rgmcnfg_read_rgtable(name_t rgname, rgm_rg_t **rg,
    const char *cluster_name)
{
	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_3,
		FaultFunctions::generic);
	char	*s = NULL;
	ccr::readonly_table_var tabptr;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	rgm_rg_t *rgp = NULL;
	*rg = NULL;

	while (1) {
		if (res.err_code != SCHA_ERR_NOERR) {
			rgm_free_rg(rgp);
			free(res.err_msg);
			if (res.err_code != SCHA_ERR_SEQID) {
				return (res);
			}
			//
			// The CCR table has been modified after it was
			// opened; retry after a small delay. See block
			// comments at the head of rgm_ccrget.cc for an
			// explanation of the SCHA_ERR_SEQID error returned
			// when the CCR is concurrently updated or deleted.
			//
			(void) sleep(1 /* second */);
		}

		res = open_ccr_table(rgname, O_RG, tabptr, cluster_name);
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}
		rgp = (rgm_rg_t *)malloc(sizeof (rgm_rg_t));
		(void) memset((char *)(rgp), 0, sizeof (rgm_rg_t));

		FAULT_POINT_CCR_TABLE(rgname, "read");

		// Query each RG property
		// Assume that the RG table has no error

		res = ccr_get_string(tabptr, RGM_CCRSTAMP, &(rgp->rg_stamp));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_bool(tabptr, RGM_UNMANAGED, &(rgp->rg_unmanaged));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_nodeidlist(tabptr, SCHA_NODELIST,
		    &(rgp->rg_nodelist));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_int(tabptr, SCHA_MAXIMUM_PRIMARIES,
		    &(rgp->rg_max_primaries));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_int(tabptr, SCHA_DESIRED_PRIMARIES,
		    &(rgp->rg_desired_primaries));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		// For backward compatibility, we don't return error
		// if properties RG_project_name, RG_affinities,
		// RG_SLM_type, RG_SLM_pset_type, RG_SLM_CPU_SHARES,
		// RG_SLM_PSET_MIN and RG_auto_start are not in ccr.
		res = ccr_get_string(tabptr, SCHA_RG_PROJECT_NAME,
		    &(rgp->rg_project_name));
		if (res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_PROP)
				rgp->rg_project_name = strdup("");
			else
				continue;
		}

// SC SLM addon start
		res = ccr_get_string(tabptr, SCHA_RG_SLM_TYPE,
		    &(rgp->rg_slm_type));
		if (res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_PROP)
				rgp->rg_slm_type =
				    strdup(SCHA_SLM_TYPE_MANUAL);
			else
				continue;
		}
		res = ccr_get_string(tabptr, SCHA_RG_SLM_PSET_TYPE,
		    &(rgp->rg_slm_pset_type));
		if (res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_PROP)
				rgp->rg_slm_pset_type =
				    strdup(SCHA_SLM_PSET_TYPE_DEFAULT);
			else
				continue;
		}
		res = ccr_get_int(tabptr, SCHA_RG_SLM_CPU_SHARES,
		    &(rgp->rg_slm_cpu));
		if (res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_PROP) {
				rgp->rg_slm_cpu =
				    SCHA_SLM_RG_CPU_SHARES_DEFAULT;
			} else {
				continue;
			}
		}
		res = ccr_get_int(tabptr, SCHA_RG_SLM_PSET_MIN,
		    &(rgp->rg_slm_cpu_min));
		if (res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_PROP) {
				rgp->rg_slm_cpu_min =
				    SCHA_SLM_RG_PSET_MIN_DEFAULT;
			} else {
				continue;
			}
		}
// SC SLM addon end

		res = ccr_get_nstring(tabptr, SCHA_RG_AFFINITIES,
		    &(rgp->rg_affinities));
		if (res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_PROP)
				rgp->rg_affinities = NULL;
			else
				continue;
		}

		res = ccr_get_nstring(tabptr, SCHA_IC_RG_AFFINITY_SOURCES,
		    &(rgp->rg_affinitents));
		if (res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_PROP)
				rgp->rg_affinitents = NULL;
			else
				continue;
		}

		res = ccr_get_bool(tabptr, SCHA_RG_AUTO_START,
		    &(rgp->rg_auto_start));
		if (res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_PROP)
				rgp->rg_auto_start = B_TRUE;
			else
				continue;
		}

		res = ccr_get_bool(tabptr, SCHA_RG_SUSP_AUTO_RECOVERY,
		    &(rgp->rg_suspended));
		if (res.err_code != SCHA_ERR_NOERR) {
			if (res.err_code == SCHA_ERR_PROP)
				rgp->rg_suspended = B_FALSE;
			else
				continue;
		}

		res = ccr_get_bool(tabptr, SCHA_FAILBACK, &(rgp->rg_failback));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		//
		// RG_System Property
		//
		res = ccr_get_bool(tabptr, SCHA_RG_SYSTEM, &(rgp->rg_system));
		if (res.err_code == SCHA_ERR_PROP)
			rgp->rg_system = B_FALSE;
		else if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_nstring(tabptr, SCHA_RG_DEPENDENCIES,
		    &(rgp->rg_dependencies));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_nstring_all(tabptr, SCHA_GLOBAL_RESOURCES_USED,
		    &(rgp->rg_glb_rsrcused));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_PATHPREFIX,
		    &(rgp->rg_pathprefix));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_RG_DESCRIPTION,
		    &(rgp->rg_description));
		if (res.err_code != SCHA_ERR_NOERR) {
			continue;
		}

		res = ccr_get_int(tabptr, SCHA_PINGPONG_INTERVAL,
		    &(rgp->rg_ppinterval));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_RG_MODE, &s);
		if (res.err_code != SCHA_ERR_NOERR)
			// There is no need to free "s" in this branch.
			continue;
		rgp->rg_mode = prop_rgmode(s);
		free(s);

		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_bool(tabptr, SCHA_IMPL_NET_DEPEND,
		    &(rgp->rg_impl_net_depend));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		rgp->rg_name = strdup(rgname);
		*rg = rgp;
		return (res);
	} // end while
}

//
// rgmcnfg_get_rtlist -
//	Read the CCR to get a list of all RTs in the cluster
//	Return NULL if no RT in the cluster
//
scha_errmsg_t
rgmcnfg_get_rtlist(namelist_t **rts, const char *zonename)
{
	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_4,
		FaultFunctions::generic);

	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;
	*rts = NULL;
	char *cluster_name;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);

	//
	// get a list of all table names
	//
	CORBA::StringSeq *tnames = NULL;
	ccr::directory_var ccrdir = lookup_ccrdir();
	ccrdir->get_table_names_zc(cluster_name, tnames, e);
	if (e.exception()) {
		e.clear();
		// ccr returned unexpected (out_of_service?) exception
		res.err_code = SCHA_ERR_CCR;
		rgm_format_errmsg(&res, REM_INTERNAL);
		free(cluster_name);
		return (res);
	}

	uint_t num_tables = tnames->length();

	//
	// find all the ccr table names that start with the RGM_PREFIX_RT
	// prefix
	//
	namelist_t *curr = 0, *ptr;
	for (uint_t i = 0; i < num_tables; i++) {

		char *tname = (char *)(*tnames)[i];

		if (strncmp(tname, RGM_PREFIX_RT,
		    strlen(RGM_PREFIX_RT)) != 0)
			continue;

		char rtname[MAXRGMOBJNAMELEN];

		(void) strcpy(rtname, tname +  strlen(RGM_PREFIX_RT));

		//
		// Make sure that the RT table can be opened and is not
		// empty (left over from an aborted creation attempt).
		//
		ccr::readonly_table_var tabptr;

		//
		// Just continue when we get an error back;
		// there is nothing we can do to handle the error.
		// SCHA_ERR_RT indicates no table or empty table
		// left behind from an aborted creation attempt.
		// SCHA_ERR_CCR indicates a CCR access failure.
		//
		if ((open_ccr_table(rtname, O_RT, tabptr,
		    cluster_name)).err_code != SCHA_ERR_NOERR) {
			continue;
		}

		ptr = (namelist_t *)malloc(sizeof (namelist_t));
		ptr->nl_name = strdup(rtname);
		ptr->nl_next = 0;

		if (*rts == 0)
			*rts = ptr;
		else
			curr->nl_next = ptr;

		curr = ptr;
	}

	delete tnames;
	free(cluster_name);
	return (res);
}

//
// rgmcnfg_get_rglist():
//
// get all managed and unmanaged RGs in the cluster from CCR. if only_local_rgs
// is set as TRUE, get only those rgs whose nodelist contains the zone from
// where the call is made.
// only_local_rgs should always be FALSE for preS10
//
scha_errmsg_t
rgmcnfg_get_rglist(namelist_t **managed_rgs, namelist_t **unmanaged_rgs,
    boolean_t only_local_rgs, const char *zone_name)
{
	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_5,
		FaultFunctions::generic);

	Environment e;
	ccr::readonly_table_var tabptr = NULL;
	uint_t i;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	nodeidlist_t *nodelist = NULL;
	nodeidlist_t *nidlist = NULL;
	uint_t nodeid;
	char *zonename = NULL;
	boolean_t unman;
	char *cluster_name = NULL;

	if ((cluster_name = get_clustername_from_zone(zone_name, &res)) == NULL)
		return (res);
	//
	// get table names
	//
	CORBA::StringSeq *tnames = NULL;
	ccr::directory_var ccrdir = lookup_ccrdir();
	ccrdir->get_table_names_zc(cluster_name, tnames, e);
	if (e.exception()) {
		e.clear();
		// ccr returned unexpected (out_of_service?) exception
		res.err_code = SCHA_ERR_CCR;
		rgm_format_errmsg(&res, REM_INTERNAL);
		// Free memory
		if (cluster_name) {
			free(cluster_name);
		}

		return (res);
	}

	uint_t num_tables = tnames->length();

	//
	// find all the ccr table names which start with rgm_rg_ prefix
	//
	*managed_rgs = 0;
	*unmanaged_rgs = 0;
	namelist_t *m_curr = NULL, *u_curr = NULL, *ptr = NULL;
	for (i = 0; i < num_tables; i++) {
		char *tname = (char *)(*tnames)[i];

		if (strncmp(tname, RGM_PREFIX_RG,
		    strlen(RGM_PREFIX_RG)) != 0)
			continue;

		char rgname[MAXRGMOBJNAMELEN];

		(void) strcpy(rgname, tname + strlen(RGM_PREFIX_RG));

		if (open_ccr_table(rgname, O_RG, tabptr, cluster_name).err_code
		    != SCHA_ERR_NOERR)
			continue;


		scha_errmsg_t r = ccr_get_bool(tabptr, RGM_UNMANAGED, &unman);
		if (r.err_code != SCHA_ERR_NOERR)
			continue;

#if SOL_VERSION >= __s10
		// only_local_rgs doesn't have meaning on preS10, so ignored
		if (only_local_rgs) {
			r = ccr_get_nodeidlist(tabptr,
			    SCHA_NODELIST, &nodelist);

			if (r.err_code != SCHA_ERR_NOERR)
				continue;
			r = populate_zone_cred(&nodeid, &zonename);
			if (r.err_code != SCHA_ERR_NOERR)
				continue;
			for (nidlist = nodelist; nidlist != NULL;
			    nidlist = nidlist->nl_next) {
				if (nodeid != nidlist->nl_nodeid)
					continue; // nodeid different
				if (zonename == NULL ||
				    nidlist->nl_zonename == NULL) {
					if (zonename == nidlist->nl_zonename)
						break;
				} else if (strcmp(zonename,
				    nidlist->nl_zonename) == 0) {
					break;
				}
			}

			if (nidlist == NULL) // not found
				continue;
		}
#else
		ASSERT(!only_local_rgs);
#endif

		ptr = (namelist_t *)malloc(sizeof (namelist_t));
		ptr->nl_name = strdup(rgname);
		ptr->nl_next = 0;

		if (unman == B_FALSE) {
			if (*managed_rgs == 0)
				*managed_rgs = ptr;
			else
				m_curr->nl_next = ptr;
			m_curr = ptr;
		} else {
			if (*unmanaged_rgs == 0)
				*unmanaged_rgs = ptr;
			else
				u_curr->nl_next = ptr;
			u_curr = ptr;
		}
	}

	delete tnames;
	rgm_free_nodeid_list(nodelist);
	if (cluster_name)
		free(cluster_name);

	return (res);
}

//
// rgmcnfg_get_invalid_tables -
//
// Read the CCR to get a list of all RGM tables in the cluster that
// are invalid.
//
//
scha_errmsg_t
rgmcnfg_get_invalid_tables(namelist_t **table_names, const char *zonename)
{

	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	Environment e;

	*table_names = NULL;
	const char *prefix_rt = RGM_PREFIX_RT;
	const char *prefix_rg = RGM_PREFIX_RG;
	char *cluster_name;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);


	//
	// get a list of all invalid table names
	// get_invalid_table_names cannot return an exception.
	//
	CORBA::StringSeq *tnames = NULL;
	ccr::directory_var ccrdir = lookup_ccrdir();
	ccrdir->get_invalid_table_names_zc(cluster_name, tnames, e);
	ASSERT(!e.exception());

	uint_t num_tables = tnames->length();

	//
	// find all the ccr table names that start with rgm_rt_ prefix or
	// rgm_rg_ prefix.  If other prefixes are added, they should be
	// checked here as well.
	//
	namelist_t *curr = NULL, *ptr;
	for (uint_t i = 0; i < num_tables; i++) {

		char *tname = (char *)(*tnames)[i];

		if ((strncmp(tname, prefix_rt, strlen(prefix_rt))
		    != 0) && (strncmp(tname, prefix_rg,
		    strlen(prefix_rg)) != 0))
			continue;

		ptr = (namelist_t *)malloc(sizeof (namelist_t));
		ptr->nl_name = strdup(tname);
		ptr->nl_next = NULL;

		if (*table_names == NULL)
			*table_names = ptr;
		else
			curr->nl_next = ptr;

		curr = ptr;
	}

	delete tnames;
	free(cluster_name);
	return (res);
}


//
// vid_n_version_process
//
// Return in 'r_rtname' the "real" RT name stored in the CCR.
// rgmcnfg_get_rtrealname calls this function if 'rtname'
// has both a Vendor_ID prefix and a version suffix.
//
// Returns success only if an exact match is found in the list.
//
static scha_errmsg_t
vid_n_version_process(const char *rtname, namelist_t *rt_list,
    char **r_rtname)
{
	namelist_t *ptr = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	for (ptr = rt_list; ptr; ptr = ptr->nl_next) {

		// we know rtname is not NULL, so OK to strcmp
		if (strcmp(rtname, ptr->nl_name) == 0) {

			// exact match; we're finished
			*r_rtname = ptr->nl_name;
			break;
		}
	}

	if (ptr) {				// Found match
		res.err_code = SCHA_ERR_NOERR;
		return (res);
	} else {				// No match
		res.err_code = SCHA_ERR_RT;
		rgm_format_errmsg(&res, REM_INVALID_RT, rtname);
		return (res);
	}
}


//
// vid_process
//
// Return in 'r_rtname' the "real" RT name stored in the CCR.
// rgmcnfg_get_rtrealname calls this function if 'rtname'
// has a Vendor_ID prefix but no version specified.
//
// For example, if "SUNW.abc" is supplied,
//
// SUNW.abc			=> exact match
// SUNW.abc SUNW.abc:2.0	=> exact match
// SUNW.abc SUNW.abc:1.0	=> exact match
// SUNW.abc:1.0			=> match
// SUNW.abc:1.0 SUNW.abc:2.0	=> no match
// abc				=> no match
// abc:1.0			=> no match
//
static scha_errmsg_t
vid_process(name_t rtname, namelist_t *rt_list, char **r_rtname)
{
	namelist_t *ptr = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *match = NULL;
	char *rtversion = NULL;
	uint_t num_rts = 0;
	boolean_t found = B_FALSE;
	size_t novers_len = 0;

	for (ptr = rt_list; ptr; ptr = ptr->nl_next) {

		// we know rtname is not NULL, so OK to strcmp
		if (strcmp(rtname, ptr->nl_name) == 0) {

			// exact match; we're finished
			match = ptr->nl_name;
			found = B_TRUE;
			break;
		}

		// If we get here it wasn't an exact match.

		// Once we've found multiple matches
		// without the version suffix (version suffix is wildcard)
		// we don't need to come in here any more.
		// However we do still check for exact matches.
		if (num_rts > 1)
			continue;

		// Check the list for entries with a version suffix.
		// SUNW.abc will match SUNW.abc:<version>
		// as long as there is only one version
		// registered for abc.

		rtversion = strchr(ptr->nl_name, ':');
		if (rtversion == NULL)
			// This entry doesn't have a version suffix;
			// RT must have been registered in 3.0.
			// Skip it because we know the vid prefix
			// doesn't match, or it would have matched
			// exactly, above.
			continue;
		novers_len = strlen(ptr->nl_name) - strlen(rtversion);

		// Compare the supplied rtname with
		// the entry minus the entry's version suffix.
		if (strlen(rtname) == novers_len &&
		    strncmp(rtname, ptr->nl_name, novers_len) == 0) {
			num_rts++;
			if (num_rts == 1) {
				// This might be the one, but we need to get
				// through the whole list before deciding.
				match = ptr->nl_name;
				found = B_TRUE;
				continue;
			}
			// num_rts > 1
			// error; we found multiple RTs.
			// But we still might find an
			// exact match, so keep going.
			found = B_FALSE;
		}
	}

	if (found) {				// Found match
		*r_rtname = match;
		res.err_code = SCHA_ERR_NOERR;
		return (res);
	} else if (num_rts > 1) {		// error: more than one match
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_MULT_RT_MATCHES, rtname);
		return (res);
	} else {				// No match
		res.err_code = SCHA_ERR_RT;
		rgm_format_errmsg(&res, REM_INVALID_RT, rtname);
		return (res);
	}
}


//
// get_rtname_separators
//
// Dots and colons are not allowed in simple RT names.
// The presence of a colon in the string 'rtname' indicates the caller
// supplied a version suffix.  The presence of a dot could indicate the
// caller supplied a vendor id prefix, or it could be part of the version
// id supplied by the caller.
//
// If there is a version suffix in the string 'rtname', this function sets
// 'colon' to the colon separating the rtname from the suffix.  If there
// is a vendor id prefix in the string 'rtname', this function sets 'dot'
// to the dot separating the prefix from the rtname.
//
static void
get_rtname_separators(name_t rtname, char **dot, char **colon)
{
	*colon = strchr(rtname, ':');
	*dot = strchr(rtname, '.');
	if (*colon) {
		// There is a colon, so there's a version suffix.
		// If there is a dot, it could be
		// a) part of the version id or
		// b) the separator between the vendor id and the rtname.
		// If the first dot comes before the colon in the string,
		// then there's a vendor id prefix.
		if (*dot && (*dot > *colon))
			// The dot is part of the version id.
			*dot = NULL;
	}
	// Else there is no colon, so there is no version suffix.
	// Therefore if there's a dot it's the separator
	// between the vendor id and the rtname.
}


//
// version_process
//
// Return in 'r_rtname' the "real" RT name stored in the CCR.
// rgmcnfg_get_rtrealname calls this function if 'rtname'
// has a version specified but no Vendor_ID prefix.
//
// For example, if "abc:1.0" is supplied,
//
// abc:1.0			=> exact match
// abc:1.0 SUNW.abc:1.0		=> exact match
// abc abc:1.0			=> exact match
// SUNW.abc:1.0			=> match
// SUNW.abc:1.0 XYZ.abc:1.0	=> no match
// abc				=> no match
// SUNW.abc			=> no match
//
static scha_errmsg_t
version_process(name_t rtname, namelist_t *rt_list, char **r_rtname)
{
	namelist_t *ptr = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *match = NULL;
	char *rtname_novid = NULL;
	char *colon = NULL;
	uint_t num_rts = 0;
	boolean_t found = B_FALSE;

	for (ptr = rt_list; ptr; ptr = ptr->nl_next) {

		// we know rtname is not NULL, so OK to strcmp
		if (strcmp(rtname, ptr->nl_name) == 0) {

			// exact match; we're finished
			match = ptr->nl_name;
			found = B_TRUE;
			break;
		}

		// If we get here it wasn't an exact match.

		// Once we've found multiple matches
		// without the vendor id prefix (vid prefix is wildcard)
		// we don't need to come in here any more.
		// However we do still check for exact matches.
		if (num_rts > 1)
			continue;

		// This entry didn't match exactly, so
		// check for entries with a prefixed vendor id.
		// abc:version will match <vid>.abc:version
		// as long as there is only one vendor registered
		// for abc.

		get_rtname_separators(ptr->nl_name, &rtname_novid,
		    &colon);
		if (!rtname_novid) {
			// no dot -> no vendor id prefix for this entry;
			// skip it
			continue;
		}

		// Compare the entry without its vendor id prefix.
		rtname_novid++;
		if (strcmp(rtname, rtname_novid) == 0) {
			num_rts++;
			if (num_rts == 1) {
				// This might be the one, but we need to get
				// through the whole list before deciding.
				match = ptr->nl_name;
				found = B_TRUE;
				continue;
			}
			// num_rts > 1
			// error; we found multiple RTs.
			// But we still might find an
			// exact match, so keep going.
			found = B_FALSE;
		}
	}

	if (found) {				// Found match
		*r_rtname = match;
		res.err_code = SCHA_ERR_NOERR;
		return (res);
	} else if (num_rts > 1) {		// error: more than one match
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_MULT_RT_MATCHES, rtname);
		return (res);
	} else {				// No match
		res.err_code = SCHA_ERR_RT;
		rgm_format_errmsg(&res, REM_INVALID_RT, rtname);
		return (res);
	}
}


//
// shorthand_process
//
// Return in 'r_rtname' the "real" RT name stored in the CCR.
// rgmcnfg_get_rtrealname calls this function if passed in 'rtname'
// lacks a Vendor_ID prefix and a version suffix.
//
// For example, if "abc" is supplied,
//
// abc				=> exact match
// abc SUNW.abc			=> exact match
// abc SUNW.abc:1.0		=> exact match
// SUNW.abc			=> match
// abc:1.0			=> match
// XYZ.abc abc:2.0		=> no match
// SUNW.abc XYZ.abc		=> no match
// abc:1.0 abc:2.0		=> no match
// SUNW.abc abc:1.0		=> no match
//
static scha_errmsg_t
shorthand_process(name_t rtname, namelist_t *rt_list, char **r_rtname)
{
	namelist_t *ptr = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *match = NULL;
	char *colon = NULL;
	char *dot = NULL;
	uint_t num_rts = 0;
	boolean_t found = B_FALSE;
	size_t novers_len = 0;

	for (ptr = rt_list; ptr; ptr = ptr->nl_next) {

		// we know rtname is not NULL, so OK to strcmp
		if (strcmp(rtname, ptr->nl_name) == 0) {

			// exact match
			match = ptr->nl_name;
			found = B_TRUE;
			break;
		}

		// Once we've found multiple matches
		// (wildcard vendor id prefix and version suffix)
		// we don't need to come in here any more.
		// However we do still check for exact matches.
		if (num_rts > 1) {
			continue;
		}

		get_rtname_separators(ptr->nl_name, &dot, &colon);

		if (dot && colon) {
			// The current entry has both a vendor id prefix
			// and a version suffix.
			// Compare 'rtname' against the entry without
			// the prefix and suffix.
			dot++;
			novers_len = strlen(dot) - strlen(colon);
			// compare just the rtname portion between the
			// dot and the colon
			if (strlen(rtname) == novers_len &&
			    strncmp(rtname, dot, novers_len) == 0) {
				num_rts++;
				if (num_rts > 1) {
					found = B_FALSE;
					// keep looking for exact match
					continue;
				}
				match = ptr->nl_name;
				found = B_TRUE;
				continue;
			}
		} else if (dot) {
			// The current entry has a vendor id prefix only.
			// Compare 'rtname' against the entry without
			// the prefix.
			dot++;
			if (strcmp(rtname, dot) == 0) {
				num_rts++;
				if (num_rts > 1) {
					found = B_FALSE;
					// keep looking for exact match
					continue;
				}
				match = ptr->nl_name;
				found = B_TRUE;
				continue;
			}
		} else if (colon) {
			// The current entry has a version suffix only.
			// Compare 'rtname' against the entry without
			// the suffix.
			novers_len = strlen(ptr->nl_name) - strlen(colon);
			if (strlen(rtname) == novers_len &&
			    strncmp(rtname, ptr->nl_name, novers_len) == 0) {
				num_rts++;
				if (num_rts > 1) {
					found = B_FALSE;
					// keep looking for exact match
					continue;
				}
				match = ptr->nl_name;
				found = B_TRUE;
				continue;
			}
		} else {
			// This entry cannot match because it
			// has neither vendor id nor version.
			// It would have to be an exact match.
			continue;
		}
	}

	if (found) {			// Match Found
		*r_rtname = match;
		res.err_code = SCHA_ERR_NOERR;
		return (res);
	} else if (num_rts > 1) {	// Error: More than one match
		res.err_code = SCHA_ERR_INVAL;
		rgm_format_errmsg(&res, REM_MULT_RT_MATCHES, rtname);
		return (res);
	} else {			// Error : Invalid RT name
		res.err_code = SCHA_ERR_RT;
		rgm_format_errmsg(&res, REM_INVALID_RT, rtname);
		return (res);
	}
}


//
// rgmcnfg_get_rtrealname -
//
// Returns the real RT name as stored in the CCR for the given 'rtname'.
// 'rtname' can be specified with or without a vendor id prefix and
// version suffix.
//
scha_errmsg_t
rgmcnfg_get_rtrealname(name_t rtname, char **rt_realname,
    const char *cluster_name)
{
	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_6,
		FaultFunctions::generic);

	namelist_t *rt_list = NULL;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *colon = NULL;
	char *dot = NULL;
	char *realresult = NULL;

	if (rtname == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	*rt_realname = NULL;

	// Read the CCR to get a list of all RTs
	res = rgmcnfg_get_rtlist(&rt_list, cluster_name);

	if (res.err_code != SCHA_ERR_NOERR)
		return (res);

	if (rt_list == NULL) {	// RT not registered yet
		res.err_code = SCHA_ERR_RT;
		rgm_format_errmsg(&res, REM_INVALID_RT, rtname);
		return (res);
	}

	get_rtname_separators(rtname, &dot, &colon);

	if (dot && colon)
		// both vendor id prefix and version suffix were specified
		res = vid_n_version_process(rtname,
		    rt_list, &realresult);
	else if (dot)
		// only vendor id prefix was specified
		res = vid_process(rtname, rt_list, &realresult);
	else if (colon)
		// only version suffix was specified
		res = version_process(rtname, rt_list, &realresult);
	else
		// neither vendor id nor version suffix was specified
		res = shorthand_process(rtname, rt_list, &realresult);

	// dup the name because we're about to free the list of RTs
	if (realresult)
		*rt_realname = strdup(realresult);

	rgm_free_nlist(rt_list);

	return (res);
}

//
// rgmcnfg_read_rttable -
//	get all properties of a given RT from CCR
//	Each property is stored in CCR table as follows:
//	   For system defined property:
//		<property name> <property value>
//	   For optional system defined property:
//		p.<property name> Tunable=<value>;Type=<value>;
//			Default=<value>;Min=<value>; Max=<value>;
//	   For extension property:
//		x.<property name>  Tunable=<value>;Type=<value>;
//			Default=<value>Max=<value>;Description=<value>"
scha_errmsg_t
rgmcnfg_read_rttable(name_t rtname, rgm_rt_t **rt,
    const char *zonename)
{
	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_7,
		FaultFunctions::generic);

	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	ccr::readonly_table_var tabptr;
	*rt = NULL;
	char *cluster_name = NULL;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);

	while (1) {
		if (res.err_code != SCHA_ERR_NOERR) {
			rgm_free_rt(*rt);
			free(res.err_msg); // ??
			if (res.err_code != SCHA_ERR_SEQID) {
				// Free memory
				if (cluster_name)
					free(cluster_name);
				return (res);
			}
			//
			// The CCR table has been modified after it was
			// opened; retry after a small delay. See block
			// comments at the head of rgm_ccrget.cc for an
			// explanation of the SCHA_ERR_SEQID error returned
			// when the CCR is concurrently updated or deleted.
			//
			(void) sleep(1 /* second */);
		}

		res = open_ccr_table(rtname, O_RT, tabptr, cluster_name);
		if (res.err_code != SCHA_ERR_NOERR) {
			// Free memory
			if (cluster_name)
				free(cluster_name);
			return (res);
		}

		// Query each RT property
		// Optional properties may not be present in the CCR table.
		// Check the CCR access of optional properties for
		// SCHA_ERR_PROP return status indicating that property is
		// not found.  Leave unset optional properties as NULL.

		*rt = (rgm_rt_t *)malloc(sizeof (rgm_rt_t));
		bzero((char *)(*rt), sizeof (rgm_rt_t));

		res = ccr_get_string(tabptr, RGM_CCRSTAMP, &((*rt)->rt_stamp));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_RT_BASEDIR,
		    &((*rt)->rt_basedir));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_RT_DESCRIPTION,
		    &((*rt)->rt_description));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_START,
		    &((*rt)->rt_methods.m_start));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_STOP,
		    &((*rt)->rt_methods.m_stop));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_VALIDATE,
		    &((*rt)->rt_methods.m_validate));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_UPDATE,
		    &((*rt)->rt_methods.m_update));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_INIT,
		    &((*rt)->rt_methods.m_init));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_FINI,
		    &((*rt)->rt_methods.m_fini));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_BOOT,
		    &((*rt)->rt_methods.m_boot));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_MONITOR_START,
		    &((*rt)->rt_methods.m_monitor_start));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_MONITOR_STOP,
		    &((*rt)->rt_methods.m_monitor_stop));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_MONITOR_CHECK,
		    &((*rt)->rt_methods.m_monitor_check));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_PRENET_START,
		    &((*rt)->rt_methods.m_prenet_start));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_POSTNET_STOP,
		    &((*rt)->rt_methods.m_postnet_stop));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_bool(tabptr, SCHA_SINGLE_INSTANCE,
		    &((*rt)->rt_single_inst));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		char *s;
		res = ccr_get_string(tabptr, SCHA_INIT_NODES, &s);
		if (res.err_code != SCHA_ERR_NOERR)
			continue;
		(*rt)->rt_init_nodes = prop_initnodes(s);
		free(s);

		res = ccr_get_string(tabptr, RT_SYSDEF_TYPE, &s);
		if (res.err_code == SCHA_ERR_PROP) {
			(*rt)->rt_sysdeftype = SYST_NONE;
		} else if (res.err_code == SCHA_ERR_NOERR) {
			(*rt)->rt_sysdeftype = prop_sysdeftype(s);
			free(s);
		} else {
			continue;
		}

		res = ccr_get_nodeidlist_all(tabptr, SCHA_INSTALLED_NODES,
		    &((*rt)->rt_instl_nodes));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		//
		// RT_System Property
		//
		res = ccr_get_bool(tabptr, SCHA_RT_SYSTEM, &((*rt)->rt_system));
		if (res.err_code == SCHA_ERR_PROP) {
			if ((*rt)->rt_sysdeftype != SYST_NONE) {
				(*rt)->rt_system = B_TRUE;
			} else {
				(*rt)->rt_system = B_FALSE;
			}
		} else if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_bool(tabptr, SCHA_FAILOVER,
		    &((*rt)->rt_failover));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_bool(tabptr, SCHA_PROXY, &((*rt)->rt_proxy));
		if (res.err_code == SCHA_ERR_PROP) {
			(*rt)->rt_proxy = B_FALSE;
		} else if (res.err_code != SCHA_ERR_NOERR)
			continue;


		res = ccr_get_bool(tabptr, SCHA_GLOBALZONE,
		    &((*rt)->rt_globalzone));
		if (res.err_code == SCHA_ERR_PROP) {
			//
			// LH and SA res types run in global zone. So default
			// to true. if property exists in CCR, it will always be
			// TRUE because of code in creat_table
			//
			if ((*rt)->rt_sysdeftype == SYST_LOGICAL_HOSTNAME ||
			    (*rt)->rt_sysdeftype == SYST_SHARED_ADDRESS)
				(*rt)->rt_globalzone = B_TRUE;
			else
				(*rt)->rt_globalzone = B_FALSE;
		} else if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_int(tabptr, SCHA_API_VERSION,
		    (int *)&((*rt)->rt_api_version));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_string(tabptr, SCHA_RT_VERSION,
		    &((*rt)->rt_version));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_nstring(tabptr, SCHA_PKGLIST,
		    &((*rt)->rt_pkglist));
		if (res.err_code != SCHA_ERR_PROP && res.err_code !=
		    SCHA_ERR_NOERR)
			continue;

		res = ccr_get_rt_upgrade(tabptr, &((*rt)->rt_sc30),
		    &((*rt)->rt_upgrade_from), &((*rt)->rt_downgrade_to));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		res = ccr_get_paramtable(tabptr, &((*rt)->rt_paramtable));
		if (res.err_code != SCHA_ERR_NOERR)
			continue;

		(*rt)->rt_name = strdup(rtname);
		(*rt)->rt_dependencies = NULL;

		// Free memory
		if (cluster_name)
			free(cluster_name);

		return (res);
	} // end while
}

//
// get_property_value -
//	Get the value of specified property
//	Note: only for RT or RG properties.
//
// Return codes:
// SCHA_ERR_NOERR: No Error,
// SCHA_ERR_RT: The RT not found (only applicable if objtype == O_RT),
// SCHA_ERR_RG: The RG not found (only applicable if objtype == O_RG),
// SCHA_ERR_CCR: CCR Open Table or Read Table (of RG or RT) failed,
// SCHA_ERR_PROP: The given key not found in the RG/RT table.
//
scha_errmsg_t
get_property_value(name_t objname, rgm_obj_type_t objtype,
    const char *key, char **value, const char *zonename)
{
	Environment e;
	CORBA::Exception *ex;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	ccr::readonly_table_var tabptr;
	char *cluster_name = NULL;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);

	*value = 0;

	// This function cannot be used to get a resource property.
	CL_PANIC(objtype != O_RSRC);

	while (1) {
		res = open_ccr_table(objname, objtype, tabptr, cluster_name);
		if (res.err_code != SCHA_ERR_NOERR) {
			// Free memory before returning
			if (cluster_name) {
				free(cluster_name);
			}

			return (res);
		}
		*value = tabptr->query_element(key, e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::no_such_key::_exnarrow(ex)) {
				e.clear();
				res.err_code = SCHA_ERR_PROP;
			} else if (ccr::table_modified::_exnarrow(ex)) {
				//
				// The CCR table has been modified after it was
				// opened; retry after a small delay.
				//
				e.clear();
				(void) sleep(1 /* second */);
				continue;
			} else {
				e.clear();
				res.err_code = SCHA_ERR_CCR;
			}
		}

		// Free any memory before returning
		if (cluster_name) {
			free(cluster_name);
		}
		return (res);
	}
}

//
// check if the specified RG exists in CCR
//
boolean_t
rgmcnfg_rg_exist(name_t rgname, scha_errmsg_t *err,
    const char *cluster_name)
{
	ccr::readonly_table_var tabptr;
	err->err_code = SCHA_ERR_NOERR;
	err->err_msg = NULL;

	ASSERT(cluster_name != NULL);

	if (rgname == NULL)
		return (B_FALSE);

	*err = open_ccr_table(rgname, O_RG, tabptr, cluster_name);

	// If there's no error, the table exists
	if (err->err_code == SCHA_ERR_NOERR) {
		return (B_TRUE);
	} else if (err->err_code == SCHA_ERR_RG) {
		// The table doesn't exist. The caller doesn't want an error
		// code in this case.
		err->err_code = SCHA_ERR_NOERR;
		return (B_FALSE);
	} else {
		// The caller will want to know if there was an error
		// other than the table not existing.
		return (B_FALSE);
	}
}


//
// rgmcnfg_rt_exist -
//	check if the specified RT exists in CCR
//
boolean_t
rgmcnfg_rt_exist(name_t rtname, scha_errmsg_t *err,
    const char *cluster_name)
{
	ccr::readonly_table_var tabptr;
	err->err_code = SCHA_ERR_NOERR;
	err->err_msg = NULL;

	ASSERT(cluster_name != NULL);

	if (rtname == NULL)
		return (B_FALSE);

	*err = open_ccr_table(rtname, O_RT, tabptr, cluster_name);

	// If there's no error, the table exists
	if (err->err_code == SCHA_ERR_NOERR) {
		return (B_TRUE);
	} else if (err->err_code == SCHA_ERR_RT) {
		// The table doesn't exist. The caller doesn't want an error
		// code in this case.
		err->err_code = SCHA_ERR_NOERR;
		return (B_FALSE);
	} else {
		// The caller will want to know if there was an error
		// other than the table not existing.
		return (B_FALSE);
	}
}
#endif /* !EUROPA_FARM */


//
// rgmcnfg_rs_exist -
//	check if the specified Resource exists in CCR table of the given RG
//
scha_errmsg_t
rgmcnfg_rs_exist(name_t resourcename, name_t rgname, const char *zonename)
{
	char *data;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char	mod_key[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RS)];

	if (resourcename == NULL) {
		res.err_code = SCHA_ERR_RSRC;
		return (res);
	}

	if (rgname == NULL) {
		res.err_code = SCHA_ERR_RG;
		return (res);
	}

	// Note that we prefix the string RGM_PREFIX_RS to the resource
	// name before storing it in the CCR. Hence, we need to prefix
	// it here too before doing a query in the CCR.
	(void) sprintf(mod_key, "%s%s", RGM_PREFIX_RS, resourcename);
	res = get_property_value(rgname, O_RG, mod_key, &data,
	    zonename);
	// If the error returned by get_property_value is SCHA_ERR_PROP,
	// remap it to SCHA_ERR_RSRC. Because that's really what makes
	// sense for the routines at this level and higher up.
	if (res.err_code == SCHA_ERR_PROP)
		res.err_code = SCHA_ERR_RSRC;
	delete [] data;

	return (res);
}



#ifndef EUROPA_FARM
//
// rgmcnfg_create_rgtable -
//
// Function for creating an RG table in the CCR and adding all RG properties
// in it.
//
scha_errmsg_t
rgmcnfg_create_rgtable(rgm_rg_t *rg, const char *zonename)
{
	Environment e;
	CORBA::Exception *ex;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *cluster_name;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);

	// The new prefix is longer than the old, so use the new in
	// sizing the array so it could be used for either.
	char tablename[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RG)];
	ccr::element_seq seq;
	ccr::updatable_table_var transptr = NULL;
	ccr::directory_var ccrdir = lookup_ccrdir();

	bzero(tablename, MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RG));
	(void) sprintf(tablename, "%s%s", RGM_PREFIX_RG, rg->rg_name);

	ccrdir->create_table_zc(cluster_name, tablename, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_exists::_exnarrow(ex)) {

			// RG table exists already; it is OK if it is
			// empty (from a previous creation attempt that
			// failed before the table was filled). We will
			// check for that later holding the transaction lock
			// for the table.

			e.clear();
		} else {
			e.clear();
			// ccr create table failed
			res.err_code = SCHA_ERR_CCR;
			free(cluster_name);
			return (res);
		}
	}

	transptr = ccrdir->begin_transaction_zc(cluster_name, tablename, e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
		free(cluster_name);
		return (res);
	}

	FAULT_POINT_CCR_TABLE(tablename, "creation");

	// Make sure that the RG table is empty. We check this *after* starting
	// the update transaction because that gives us an update lock on
	// the table preventing concurrent updates.

	if (!is_ccr_table_empty(tablename, cluster_name)) {
		res.err_code = SCHA_ERR_RG;
		rgm_format_errmsg(&res, REM_RG_EXISTS, rg->rg_name);
		free(cluster_name);
		goto abort_create_rg; //lint !e801
	}

	ccr_pack_bool(seq, RGM_UNMANAGED, B_TRUE);

	ccr_pack_nodeidlist(seq, SCHA_NODELIST, rg->rg_nodelist);

	ccr_pack_int(seq, SCHA_MAXIMUM_PRIMARIES, rg->rg_max_primaries);
	ccr_pack_int(seq, SCHA_DESIRED_PRIMARIES, rg->rg_desired_primaries);
	ccr_pack_bool(seq, SCHA_FAILBACK, rg->rg_failback);
	ccr_pack_bool(seq, SCHA_RG_SYSTEM, rg->rg_system);
	ccr_pack_string(seq, SCHA_RESOURCE_LIST, NULL);
	ccr_pack_nstring(seq, SCHA_RG_DEPENDENCIES, rg->rg_dependencies);
	ccr_pack_nstring_all(seq, SCHA_GLOBAL_RESOURCES_USED,
	    &rg->rg_glb_rsrcused);
	ccr_pack_string(seq, SCHA_RG_MODE, rgmode2str(rg->rg_mode));
	ccr_pack_bool(seq, SCHA_IMPL_NET_DEPEND, rg->rg_impl_net_depend);
	ccr_pack_string(seq, SCHA_PATHPREFIX, rg->rg_pathprefix);
	ccr_pack_string(seq, SCHA_RG_DESCRIPTION, rg->rg_description);
	ccr_pack_int(seq, SCHA_PINGPONG_INTERVAL, rg->rg_ppinterval);
	ccr_pack_string(seq, SCHA_RG_PROJECT_NAME, rg->rg_project_name);
// SC SLM addon start
	ccr_pack_string(seq, SCHA_RG_SLM_TYPE, rg->rg_slm_type);
	ccr_pack_string(seq, SCHA_RG_SLM_PSET_TYPE, rg->rg_slm_pset_type);
	ccr_pack_int(seq, SCHA_RG_SLM_CPU_SHARES, rg->rg_slm_cpu);
	ccr_pack_int(seq, SCHA_RG_SLM_PSET_MIN, rg->rg_slm_cpu_min);
// SC SLM addon end
	ccr_pack_nstring(seq, SCHA_RG_AFFINITIES, rg->rg_affinities);
	ccr_pack_nstring(seq, SCHA_IC_RG_AFFINITY_SOURCES, rg->rg_affinitents);
	ccr_pack_bool(seq, SCHA_RG_AUTO_START, rg->rg_auto_start);
	ccr_pack_bool(seq, SCHA_RG_SUSP_AUTO_RECOVERY, rg->rg_suspended);

	if (rg->rg_auto_start) {
		ccr_pack_string(seq, RG_OK_TO_START, "");
	}

	res = ccr_put_resource_group(transptr, seq);

	if (res.err_code != SCHA_ERR_NOERR)
		goto abort_create_rg; //lint !e801


	transptr->commit_transaction(e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
		free(cluster_name);
		return (res);
	}

	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_8,
		FaultFunctions::generic);
	return (res);

abort_create_rg:

	transptr->abort_transaction(e);

	if (e.exception())
		// this exception is not important
		e.clear();
	free(cluster_name);
	return (res);
}

//
// pack_rt_upgrade_list -
// This function packs the info of all upgrade_from or downgrade_to directives
// declared in RTR file into a sequence of ccr elements
// The input argument tag is either SCHA_UPGRADE_FROM or SCHA_DOWNGRADE_TO.
//
// For each #$upgrade_from in RTR, write an entry into CCR
// in the format of "upgrade_from.<rt_version>   <tunability>"
// For each #$downgrade_to in RTR, write an entry into CCR
// in the format of "downgrade_to.<rt_version>   <tunability>"
//
static void
pack_rt_upgrade_list(ccr::element_seq &seq, rgm_rt_upgrade_t *rt_upgrade_list,
    const char *tag)
{
	rgm_rt_upgrade_t *upgp;
	char buff[MAXRGMOBJNAMELEN];

	for (upgp = rt_upgrade_list; upgp != NULL; upgp = upgp->rtu_next) {

		if (upgp->rtu_version == NULL) {
			// rt version is not specified
			(void) sprintf(buff, "%s.", tag);
		} else {
			(void) sprintf(buff, "%s.%s", tag, upgp->rtu_version);
		}

		ccr_pack_string(seq, buff, tune2str(upgp->rtu_tunability));
	}
}


//
// rgmcnfg_create_rttable -
//	create an RT table in the CCR
//	Each property is stored in CCR table as follows:
//	   For system defined property:
//		<property name> <property value>
//	   For optional system defined property:
//		p.<property name> Tunable=<value>;Type=<value>;
//			Default=<value>;Min=<value>; Max=<value>;
//	   For extension property:
//		x.<property name>  Tunable=<value>;Type=<value>;
//			Default=<value>Max=<value>;Description=<value>"
//
// Note: RT creation is done by scrgadm directly without
// going through the rgm President. Therefore, it is possible to have
// concurrent invocations of this function. It is also possible for scrgadm or
// the node to die after the RT table is created and before the RT
// table is filled. That results in an empty RT table being left
// behind.
//
scha_errmsg_t
rgmcnfg_create_rttable(rgm_rt_t *rt, const char *zonename)
{
	//
	// RT create is anomalous among rgmcnfg functions in that
	// it's called directly by scrgadm instead of going through
	// the RGM president. The reason is that we want to support
	// registering RTR files in local paths on non-president
	// nodes.
	//
	Environment e;
	CORBA::Exception *ex;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *cluster_name;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);
#if SOL_VERSION >= __s10
	if (strcmp(cluster_name, GLOBAL_ZONENAME) == 0) {
		if (sc_nv_zonescheck() != 0) {
			res.err_code = SCHA_ERR_ACCESS;
			rgm_format_errmsg(&res, REM_RT_NGZONE);
			free(cluster_name);
			return (res);
		}
	}
#endif


	char tablename[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RT)];
	ccr::updatable_table_var transptr;
	ccr::element_seq seq;
	ccr::directory_var ccrdir = lookup_ccrdir();

	(void) sprintf(tablename, "%s%s", RGM_PREFIX_RT, rt->rt_name);

	ccrdir->create_table_zc(cluster_name, tablename, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_exists::_exnarrow(ex)) {

			// RT table exists already; it is OK if it is
			// empty (from a previous creation attempt that
			// failed before the table was filled). We will
			// check for that later holding the transaction lock
			// for the table.

			e.clear();
		} else {
			e.clear();
			// ccr create table failed
			res.err_code = SCHA_ERR_CCR;
			free(cluster_name);
			return (res);
		}
	}

	// Start an update transaction on the table. If two concurrently
	// running creations are attempted, the 2nd one to reach
	// begin_transaction will block at the begin_transaction waiting for
	// the first to commit or abort. That is okay: the 2nd one in will
	// find that the RT table is non-empty, and will return early with
	// SCHA_ERR_RT.

	transptr = ccrdir->begin_transaction_zc(cluster_name, tablename, e);
	if ((ex = e.exception()) != NULL) {

		// If the table no longer exists, the President must have
		// concurrently deleted the RT. That RT must have already
		// existed, so it is legit for this routine to return
		// SCHA_ERR_INVAL.

		if (ccr::no_such_table::_exnarrow(ex)) {
			res.err_code = SCHA_ERR_RT;
			rgm_format_errmsg(&res, REM_RT_EXISTS, rt->rt_name);
		} else
			res.err_code = SCHA_ERR_CCR;
		e.clear();
		goto abort_create_rt; //lint !e801
	}

	FAULT_POINT_CCR_TABLE(tablename, "creation");

	// Make sure that the RT table is empty. If it is not, a concurrent
	// creation attempt got ahead of us and filled the table. We check
	// this *after* starting the update transaction because that allows
	// the lookup to see any transactions that committed before our
	// begin_transaction above.

	if (!is_ccr_table_empty(tablename, cluster_name)) {
		res.err_code = SCHA_ERR_RT;
		rgm_format_errmsg(&res, REM_RT_EXISTS, rt->rt_name);
		goto abort_create_rt; //lint !e801
	}

	// Empty the table. The table should be empty, but clean up anyway
	// just to be doubly sure.

	transptr->remove_all_elements(e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
		goto abort_create_rt; //lint !e801
	}

	// All checks on required properties and fill-in of default
	// values will have been done by now.
	// Optional RT properties that have not been set are NULL
	// in the rt structure, and will not be written to the CCR table.

	if (rt->rt_basedir != NULL) {
		ccr_pack_string(seq, SCHA_RT_BASEDIR, rt->rt_basedir);
	}

	if (rt->rt_description != NULL) {
		ccr_pack_string(seq, SCHA_RT_DESCRIPTION,
		    rt->rt_description);
	}

	if (rt->rt_methods.m_start != NULL) {
		ccr_pack_string(seq, SCHA_START, rt->rt_methods.m_start);
	}

	if (rt->rt_methods.m_stop != NULL) {
		ccr_pack_string(seq, SCHA_STOP, rt->rt_methods.m_stop);
	}

	if (rt->rt_methods.m_validate != NULL) {
		ccr_pack_string(seq, SCHA_VALIDATE, rt->rt_methods.m_validate);
	}

	if (rt->rt_methods.m_update != NULL) {
		ccr_pack_string(seq, SCHA_UPDATE, rt->rt_methods.m_update);
	}

	if (rt->rt_methods.m_init != NULL) {
		ccr_pack_string(seq, SCHA_INIT, rt->rt_methods.m_init);
	}

	if (rt->rt_methods.m_fini != NULL) {
		ccr_pack_string(seq, SCHA_FINI, rt->rt_methods.m_fini);
	}

	if (rt->rt_methods.m_boot != NULL) {
		ccr_pack_string(seq, SCHA_BOOT, rt->rt_methods.m_boot);
	}

	if (rt->rt_methods.m_monitor_start != NULL) {
		ccr_pack_string(seq, SCHA_MONITOR_START,
		    rt->rt_methods.m_monitor_start);
	}

	if (rt->rt_methods.m_monitor_stop != NULL) {
		ccr_pack_string(seq, SCHA_MONITOR_STOP,
		    rt->rt_methods.m_monitor_stop);
	}

	if (rt->rt_methods.m_monitor_check != NULL) {
		ccr_pack_string(seq, SCHA_MONITOR_CHECK,
		    rt->rt_methods.m_monitor_check);
	}

	if (rt->rt_methods.m_prenet_start != NULL) {
		ccr_pack_string(seq, SCHA_PRENET_START,
		    rt->rt_methods.m_prenet_start);
	}

	if (rt->rt_methods.m_postnet_stop != NULL) {
		ccr_pack_string(seq, SCHA_POSTNET_STOP,
		    rt->rt_methods.m_postnet_stop);
	}

	ccr_pack_bool(seq, SCHA_SINGLE_INSTANCE, rt->rt_single_inst);

	ccr_pack_string(seq, SCHA_INIT_NODES,
	    initnodes2str(rt->rt_init_nodes));

	if (rt->rt_sysdeftype != SYST_NONE) {
		ccr_pack_string(seq, RT_SYSDEF_TYPE,
		    sysdeftype2str(rt->rt_sysdeftype));
	}

	ccr_pack_nodeidlist_all(seq, SCHA_INSTALLED_NODES,
	    &rt->rt_instl_nodes);

	ccr_pack_bool(seq, SCHA_FAILOVER, rt->rt_failover);

	ccr_pack_bool(seq, SCHA_PROXY, rt->rt_proxy);

	// irrespective of globalzone flag, LH and SA run in global zone
	if (rt->rt_sysdeftype == SYST_LOGICAL_HOSTNAME ||
	    rt->rt_sysdeftype == SYST_SHARED_ADDRESS)
		ccr_pack_bool(seq, SCHA_GLOBALZONE, B_TRUE);
	else
		ccr_pack_bool(seq, SCHA_GLOBALZONE, rt->rt_globalzone);

	//
	// RT_System Property
	//
	ccr_pack_bool(seq, SCHA_RT_SYSTEM, rt->rt_system);

	ccr_pack_int(seq, SCHA_API_VERSION, (int)rt->rt_api_version);

	if (rt->rt_version != NULL) {
		ccr_pack_string(seq, SCHA_RT_VERSION, rt->rt_version);
	}

	if (rt->rt_pkglist != NULL) {
		ccr_pack_nstring(seq, SCHA_PKGLIST, rt->rt_pkglist);
	}

	if (!rt->rt_sc30) {
		ccr_pack_string(seq, SCHA_UPGRADE_HEADER, "");

		pack_rt_upgrade_list(seq, rt->rt_upgrade_from,
		    SCHA_UPGRADE_FROM);

		pack_rt_upgrade_list(seq, rt->rt_downgrade_to,
		    SCHA_DOWNGRADE_TO);
	}

	ccr_pack_paramtable(seq, rt->rt_paramtable);

	transptr->add_elements(seq, e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
		goto abort_create_rt; //lint !e801
	}

	transptr->commit_transaction(e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
		goto abort_create_rt; //lint !e801
	}

	// We are done with transptr. Clear it.
	transptr = NULL;

	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_9,
		FaultFunctions::generic);
	free(cluster_name);
	return (res);

abort_create_rt:

	if (transptr) {
		transptr->abort_transaction(e);
		if (e.exception())
			// this exception is not important
			e.clear();
	}
	free(cluster_name);
	return (res);
}

//
// rgmcnfg_update_rttable
//
//
// updates the only updatable RT propertes -- installed nodes, and RT_System
//
//   "rt_system" is always updated - there is no "don't update" value for
//   rt_system.
//
//   "don't update flag" value for "installed_nodes" is NULL (installed_nodes
//   can't be empty)
//
scha_errmsg_t
rgmcnfg_update_rttable(rgm_rt_t *rt, const char *instnodes,
    const boolean_t rt_system, const char *zonename)
{
	Environment e;
	CORBA::Exception *ex;

	char tablename[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RT)];
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *cluster_name;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);

	ccr::updatable_table_var transptr = NULL;
	ccr::directory_var ccrdir = lookup_ccrdir();

	(void) sprintf(tablename, "%s%s", RGM_PREFIX_RT, rt->rt_name);

	transptr = ccrdir->begin_transaction_zc(cluster_name, tablename, e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
		free(cluster_name);
		return (res);
	}

	if (instnodes != NULL) {
		transptr->update_element(SCHA_INSTALLED_NODES, instnodes, e);
		if (e.exception()) {
			res.err_code = SCHA_ERR_PROP;
			goto abort_update_rt; //lint !e801
		}
	}

	transptr->update_element(SCHA_RT_SYSTEM, bool2str(rt_system), e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_key::_exnarrow(ex)) {

			/* RT_SYSTEM prop is not in the table yet. Add it */

			e.clear();

			res = ccr_put_string(transptr, SCHA_RT_SYSTEM,
			    bool2str(rt_system));
			if (res.err_code != SCHA_ERR_NOERR) {
				res.err_code = SCHA_ERR_CCR;
				goto abort_update_rt; //lint !e801
			}
		} else {
			res.err_code = SCHA_ERR_PROP;
			goto abort_update_rt; //lint !e801
		}
	}

	transptr->commit_transaction(e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
		free(cluster_name);
		return (res);
	}
	free(cluster_name);
	return (res);

abort_update_rt:
	e.clear();
	transptr->abort_transaction(e);
	if (e.exception())
		e.clear();
	free(cluster_name);
	return (res);
} //lint !e818

//
// rgmcnfg_resource_tmpl_empty
//
// Creates an rgm_resource_t struct with the default values for
// the "hard-coded" system properties (those for which there are explicit
// fields in the rgm_resource_t structure), and no values
// (or structures allocated) for the optional system properties or extension
// properties.
//
// Returns a scha_errmsg_t, which will contain an error if this function is
// unable to allocate the rgm_resource_t structure, or if the input parameters
// are invalid.
//
// Returns a pointer to the new rgm_resource_t in *r as an out parameter
// if successful.  Note that the client owns the memory, and is responsible
// for freeing it.
//
scha_errmsg_t
rgmcnfg_resource_tmpl_empty(const char *r_name, const char *rg_name,
    name_t rt_name, rgm_rt_t *rt, rgm_resource_t **r)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	*r = NULL;
	if (r_name == NULL || rg_name == NULL || rt_name == NULL) {
		res.err_code = SCHA_ERR_INTERNAL;
		rgm_format_errmsg(&res, REM_INTERNAL);
		return (res);
	}

	// make sure the rt exists.
	if (rt == NULL) {
		res.err_code = SCHA_ERR_RT;
		rgm_format_errmsg(&res, REM_INVALID_RT, rt_name);
		return (res);
	}

	// allocate the memory
	*r = (rgm_resource_t *)malloc_nocheck(sizeof (rgm_resource_t));
	if (*r == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	(*r)->r_onoff_switch = new(nothrow) rgm_switch_t;
	if ((*r)->r_onoff_switch == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	(*r)->r_monitored_switch = new(nothrow) rgm_switch_t;

	if ((*r)->r_monitored_switch == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}


	//
	// Need to check for NULL returns from strdup, because we might
	// not have the interposed version of malloc from the rgmd.
	//
	(*r)->r_name = strdup_nocheck(r_name);
	(*r)->r_rgname = strdup_nocheck(rg_name);
	(*r)->r_type = strdup_nocheck(rt_name);

	//
	// Type_version property introduced in SC3.1 which has the same
	// value of the RT_version of resource's type.
	// For backward compatibility, determine whether the RT_version
	// even appears in the CCR of resource type.  If not, set
	// Type_version to a value of "" (empty string).
	//
	if (rt->rt_version == NULL)
		(*r)->r_type_version = strdup_nocheck("");
	else
		(*r)->r_type_version = strdup_nocheck(rt->rt_version);
	// On off switch and monitored switch structures not initialized here.
	// All nodes will have a default value of DISABLED.
	(*r)->r_dependencies.dp_weak = NULL;
	(*r)->r_dependencies.dp_strong = NULL;
	(*r)->r_dependencies.dp_restart = NULL;
	(*r)->r_dependencies.dp_offline_restart = NULL;
	(*r)->r_inter_cluster_dependents.dp_strong = NULL;
	(*r)->r_inter_cluster_dependents.dp_weak = NULL;
	(*r)->r_inter_cluster_dependents.dp_restart = NULL;
	(*r)->r_inter_cluster_dependents.dp_offline_restart = NULL;

	(*r)->r_properties = NULL;
	(*r)->r_ext_properties = NULL;
	(*r)->r_description = strdup_nocheck("");
	(*r)->r_project_name = strdup_nocheck("");

	//
	// Check for NULL returns from strdup_nocheck.
	//
	if ((*r)->r_name == NULL || (*r)->r_rgname == NULL || (*r)->r_type
	    == NULL || (*r)->r_type_version == NULL || (*r)->r_description
	    == NULL || (*r)->r_project_name == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}

	return (res);
} //lint !e818


//
// rgmcnfg_resource_tmpl -
//
// Returns a scha_errmsg_t, which will specify whether the function
// succeeded or failed.
//
// If the err_code is SCHA_ERR_NOERR, returns a resource template as an out
// parameter (in *r).
//
// Calls rgmcnfg_resource_tmpl_empty to create the resource structure, and
// to fill in the resource properties that are hard-coded fields in the
// structure.
//
// For each property in the paramtable of this resource's Resource Type,
// creates an rgm_property_list_t structure, and appends it to
// the r_properties or r_ext_properties list.
//
// This function is different from apply_rt_defaults in that it creates
// rgm_property_list_t structures even if there is no default specified in
// the paramtable.
//
// No checks of resource property values are made here.  This function only
// sets the values from the RT paramtable.
//
scha_errmsg_t
rgmcnfg_resource_tmpl(const char *r_name, const char *rg_name,
	name_t rt_name, rgm_rt_t *rt, rgm_resource_t **r)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	/*
	 * Call the helper function rgmcnfg_resource_tmpl_empty to
	 * create an rgm_resource_t structure with no properties.
	 *
	 * This helper function will check the validity of the input
	 * parameters (r_name, rg_name, rt_name, and rt).
	 */
	res = rgmcnfg_resource_tmpl_empty(r_name, rg_name, rt_name,
	    rt, r);
	if (res.err_code != SCHA_ERR_NOERR) {
		return (res);
	}

	rgm_property_list_t *ptr = NULL;
	rgm_param_t **prop = NULL;

	for (prop = rt->rt_paramtable; prop && *prop; ++prop) {
		//
		// Call a helper function to actually create the
		// rgm_resource_list_t.
		//
		res = rgmcnfg_new_property_from_param(prop,
		    &ptr);
		if (res.err_code != SCHA_ERR_NOERR) {
			return (res);
		}

		// add it to the head of the appropriate list
		if ((*prop)->p_extension == B_FALSE) {
			ptr->rpl_next = (*r)->r_properties;
			(*r)->r_properties = ptr;
		} else {
			ptr->rpl_next = (*r)->r_ext_properties;
			(*r)->r_ext_properties = ptr;
		}
	}
	return (res);
}

//
// rgmcnfg_new_property_from_param
//
// Helper function to create an rgm_property_list_t from an rgm_param_t.
//
// This code was previously part of rgmcnfg_resource_tmpl, but was factored
// out (and modified) to be called by multiple functions.
//
// Creates a new rgm_property_list_t structure, and fills in the
// fields.
//
// Switches on the type of the resource (in (*prop)->p_type) to decide
// how to parse the value.
//
// Returns the new rgm_property_list_t in the output ptr_p parameter.
// Returns a scha_errmsg_t as the return value.
//
scha_errmsg_t
rgmcnfg_new_property_from_param(rgm_param_t **prop,
    rgm_property_list_t **ptr_p)
{
	rgm_property_list_t *ptr;
	char *s;
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	*ptr_p = NULL;
	//
	// Need to check return values from malloc_nocheck and strdup_nocheck
	//
	ptr = (rgm_property_list_t *)malloc_nocheck(
	    sizeof (rgm_property_list_t));
	if (ptr == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		return (res);
	}


	(void) memset(ptr, '\0', sizeof (rgm_property_list_t));


	ptr->rpl_property = (rgm_property_t *)malloc_nocheck(
	    sizeof (rgm_property_t));
	if (ptr->rpl_property == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto err_new_property; //lint !e801
	}


	(void) memset(ptr->rpl_property, '\0', sizeof (rgm_property_list_t));

	ptr->rpl_property->rp_value = new(nothrow) rgm_value_t;
	if (ptr->rpl_property->rp_value == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto err_new_property; //lint !e801
	}

	ptr->rpl_property->rp_key = strdup_nocheck((*prop)->p_name);
	if (ptr->rpl_property->rp_key == NULL) {
		res.err_code = SCHA_ERR_NOMEM;
		goto err_new_property; //lint !e801
	}
	ptr->rpl_property->rp_array_values = NULL;

	//
	// We need to set the description of the resource from the
	// paramtable.  The description is set for each property when
	// it is read from the CCR. However, now that the default RT
	// properties are not being stored in the CCR, we need to set
	// the description here when we add the property to the resource
	// structure.
	//
	if ((*prop)->p_description != NULL) {
		ptr->rpl_property->rp_description =
		    strdup_nocheck((*prop)->p_description);
		if (ptr->rpl_property->rp_description == NULL) {
			res.err_code = SCHA_ERR_NOMEM;
			goto err_new_property; //lint !e801
		}
	} else {
		ptr->rpl_property->rp_description = NULL;
	}
	ptr->rpl_property->rp_type = (*prop)->p_type;
	ptr->rpl_next = NULL;
	if ((*prop)->p_per_node) {
		ptr->rpl_property->is_per_node = B_TRUE;
	} else {
		ptr->rpl_property->is_per_node = B_FALSE;
	}

	if ((*prop)->p_default_isset == B_TRUE) {
		switch ((*prop)->p_type) {
		case SCHA_PTYPE_STRING:
		case SCHA_PTYPE_ENUM:
			if ((*prop)->p_defaultstr == NULL)
				goto finished; //lint !e801
			if ((*prop)->p_per_node) {
				/*
				 * Property value being updated is a per-node
				 * property, initialize the default value on
				 * all the nodes.
				 * The default value is updated in the "0" th
				 * index of the map.
				 */

				((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value_node["0"] = strdup_nocheck(
				    (*prop)->p_defaultstr);
				if (((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value_node["0"] == NULL &&
				    (*prop)->p_defaultstr != NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto err_new_property; //lint !e801
				}
			} else {
				ptr->rpl_property->is_per_node = B_FALSE;
				((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value[0] =
				    strdup_nocheck((*prop)->p_defaultstr);
				if (((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value[0] == NULL &&
				    (*prop)->p_defaultstr != NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto err_new_property; //lint !e801
				}
			}

			break;
		case SCHA_PTYPE_UINT:
		case SCHA_PTYPE_INT:
			if ((*prop)->p_per_node == B_TRUE) {
				/*
				 * Property value being updated is a per-node
				 * property, initialize the default value on
				 * all the nodes.
				 * The default value is updated in the "0" th
				 * index of the map.
				 */
				((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value_node["0"] = rgm_itoa
				    ((*prop)->p_defaultint);
				if (((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value_node["0"] == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto err_new_property; //lint !e801
				}
			} else {
				((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value[0] = rgm_itoa(
				    (*prop)->p_defaultint);
				if (((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value[0] == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto err_new_property; //lint !e801
				}
			}
			break;
		case SCHA_PTYPE_BOOLEAN:
			if ((*prop)->p_per_node == B_TRUE) {
				/*
				 * Property value being updated is a per-node
				 * property, initialize the default value on
				 * all the nodes. The default value is updated
				 * in the "0" th index of the map
				 */
				((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value_node["0"] = strdup_nocheck
				    (bool2str((*prop)->p_defaultbool));
				if (((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value_node["0"] == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto err_new_property; //lint !e801
				}

			} else {
				((rgm_value_t *)ptr->rpl_property->rp_value)->
				    rp_value[0] =
				    strdup_nocheck(bool2str((*prop)->
				    p_defaultbool));
				if (((rgm_value_t *)ptr->rpl_property->rp_value)
				    ->rp_value[0] == NULL) {
					res.err_code = SCHA_ERR_NOMEM;
					goto err_new_property; //lint !e801
				}
			}
			break;
		case SCHA_PTYPE_UINTARRAY:
		case SCHA_PTYPE_STRINGARRAY:
			if ((*prop)->p_per_node == B_TRUE) {
				// Should never reach here!
				res.err_code = SCHA_ERR_INVAL;
				return (res);
			}
			ptr->rpl_property->is_per_node = B_FALSE;
			s = nstr2str((*prop)->p_defaultarray);
			ptr->rpl_property->rp_array_values = str2nstr(s);
			free(s);
			if ((*prop)->p_defaultarray != NULL &&
			    ptr->rpl_property->rp_array_values == NULL) {
				res.err_code = SCHA_ERR_NOMEM;
				goto err_new_property; //lint !e801
			}
			break;
		}
	}

finished:
	// assign the newly allocated memory to the output param
	*ptr_p = ptr;
	return (res);

err_new_property:
	if (ptr) {
		if (ptr->rpl_property) {
			free(ptr->rpl_property->rp_key);
			free(ptr->rpl_property->rp_description);
			free(ptr->rpl_property);
		}
		free(ptr);
	}
	return (res);
} //lint !e818

//
// Functions for removing RGM objects
//
static scha_errmsg_t
remove_ccrtable(const char *tablename, const char *zonename)
{
	Environment e;
	ccr::directory_var ccrdir = lookup_ccrdir();
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *cluster_name;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);

	ccrdir->remove_table_zc(cluster_name, tablename, e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
	}

	free(cluster_name);
	return (res);
}

//
// rgmcnfg_remove_rgtable -
//	Remove the table for the given RG from CCR
//
scha_errmsg_t
rgmcnfg_remove_rgtable(name_t rgname, const char *zonename)
{
	char tablename[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RG)];
	scha_errmsg_t errmsg = {SCHA_ERR_NOERR, NULL};
	char *cluster_name;

	if ((cluster_name = get_clustername_from_zone(zonename,
	    &errmsg)) == NULL)
		return (errmsg);

	(void) sprintf(tablename, "%s%s", RGM_PREFIX_RG, rgname);
	errmsg = remove_ccrtable(tablename, cluster_name);

	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_11,
	    FaultFunctions::generic);
	free(cluster_name);
	return (errmsg);
}


//
// rgmcnfg_remove_rttable -
//	Remove the RT table from the CCR
//	'rtname' is the full name as stored in the CCR.
//
scha_errmsg_t
rgmcnfg_remove_rttable(name_t rtname, const char *zonename)
{
	// The new prefix is longer than the old, so use the new in
	// sizing the array so it could be used for either.
	char tablename[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RT)];
	scha_errmsg_t res;
	char *cluster_name;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);

	// remove RT table
	(void) sprintf(tablename, "%s%s", RGM_PREFIX_RT, rtname);
	res = remove_ccrtable(tablename, cluster_name);

	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_12,
		FaultFunctions::generic);
	free(cluster_name);
	return (res);
}

//
// rgmcnfg_remove_resource -
//	Remove the specified R from RG table
//
scha_errmsg_t
rgmcnfg_remove_resource(name_t resourcename, name_t rgname,
    const char *zonename)
{
	FAULTPT_RGM(FAULTNUM_RGM_FAULT_POINT_CCR_READ_WRITE_13,
		FaultFunctions::generic);

	char tablename[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RG)];
	Environment e;
	ccr::updatable_table_var transptr = NULL;
	ccr::directory_var ccrdir = lookup_ccrdir();
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *r_list = NULL;
	char	mod_key[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RS)];
	char *cluster_name;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);

	(void) sprintf(tablename, "%s%s", RGM_PREFIX_RG, rgname);

	transptr = ccrdir->begin_transaction_zc(cluster_name, tablename, e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_RG;
		free(cluster_name);
		return (res);
	}

	// Note that we prefix the string RGM_PREFIX_RS to the resource
	// name before storing it in the CCR. Hence, we need to prefix
	// it here too before removing it from the CCR.
	(void) sprintf(mod_key, "%s%s", RGM_PREFIX_RS, resourcename);
	transptr->remove_element(mod_key, e);
	if (e.exception()) {
		e.clear();
		transptr->abort_transaction(e);
		if (e.exception())
			// this exception is not important
			e.clear();
		res.err_code = SCHA_ERR_RSRC;
		free(cluster_name);
		return (res);
	}

	// update resource_list by removing resourcename
	res = get_property_value(rgname, O_RG, SCHA_RESOURCE_LIST,
	    &r_list, (const char*)cluster_name);
	if (res.err_code != SCHA_ERR_NOERR || r_list == NULL) {
		transptr->abort_transaction(e);
		if (e.exception())
			// this exception is not important
			e.clear();
		free(cluster_name);
		return (res);
	}

	char *new_rl = name_delete((const char *)resourcename, r_list);
	delete [] r_list;
	if (new_rl == NULL || new_rl[0] == '\0')
		transptr->update_element(SCHA_RESOURCE_LIST, "", e);
	else {
		transptr->update_element(SCHA_RESOURCE_LIST, new_rl, e);
	}
	if (new_rl) {
		free(new_rl);
	}
	if (e.exception()) {
		e.clear();
		transptr->abort_transaction(e);
		if (e.exception())
			// this exception is not important
			e.clear();
		res.err_code = SCHA_ERR_CCR;
		free(cluster_name);
		return (res);
	}

	transptr->commit_transaction(e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
		free(cluster_name);
		return (res);
	}
	free(cluster_name);
	return (res);
}

//
// update_property -
//	Update the specified property for RT or RG in CCR
//
scha_errmsg_t
update_property(const char *objname, rgm_obj_type_t objtype, const char *key,
    const char *value, const char *zonename)
{
	Environment e;
	char tablename[MAXRGMOBJNAMELEN +
	    MAX(sizeof (RGM_PREFIX_RT), sizeof (RGM_PREFIX_RG))];
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	char *cluster_name;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);

	ccr::directory_var ccrdir = lookup_ccrdir();
	ccr::updatable_table_var transptr = NULL;

	//
	// This function cannot be used to update a resource property.
	//
	CL_PANIC(objtype != O_RSRC);

	if (objtype == O_RT) {
		(void) sprintf(tablename, "%s%s", RGM_PREFIX_RT, objname);
	} else {
		(void) sprintf(tablename, "%s%s", RGM_PREFIX_RG, objname);
	}

	transptr = ccrdir->begin_transaction_zc(cluster_name, tablename, e);
	if (e.exception()) {
		e.clear();
		if (objtype == O_RT) {
			res.err_code = SCHA_ERR_RT;
			free(cluster_name);
			return (res);
		} else {
			res.err_code = SCHA_ERR_RG;
			free(cluster_name);
			return (res);
		}
	}

	transptr->update_element(key, value, e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_PROP;
		free(cluster_name);
		return (res);
	}

	transptr->commit_transaction(e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
		free(cluster_name);
		return (res);
	}
	free(cluster_name);
	return (res);
}
//
// rgmcnfg_set_rgunmanaged -
//	set the UNMANAGED flag to the specified value(TRUE or FALSE)
//
scha_errmsg_t
rgmcnfg_set_rgunmanaged(name_t rgname, boolean_t unmanaged,
    const char *zonename)
{
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};

	if (rgname == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		return (res);
	}

	res = update_property((const char *)rgname, O_RG, RGM_UNMANAGED,
	    bool2str(unmanaged), zonename);
	FAULTPT_RGM(FAULTNUM_RGM_AFTER_RGMCNFG_SET_RGUNMANAGED,
		FaultFunctions::generic);
	return (res);
}
#endif /* !EUROPA_FARM */

//
// rgmcnfg_is_ok_start_rg -
//	Return TRUE if Ok_To_Start flag is found in RG CCR table.
//	Otherwise return FALSE.
//
boolean_t
rgmcnfg_is_ok_start_rg(name_t rgname, const char *zonename)
{
	char		*junk;

	if (get_property_value(rgname, O_RG, RG_OK_TO_START, &junk,
	    (char *)zonename).err_code
	    != SCHA_ERR_NOERR)
		return (B_FALSE);

	delete [] junk;
	return (B_TRUE);
}

#ifndef EUROPA_FARM
//
// rgmcnfg_set_ok_start_rg -
//	This function is for adding and removing Ok_To_Start in CCR table
//	of the specified RG.
//	If parameter ok_flag is TRUE, set Ok_To_Start by adding an entry
//	'Ok_To_Start' in ccr .
//	If the parameter flag=FALSE, remove "Ok_To_Start" from CCR.
//
scha_errmsg_t
rgmcnfg_set_ok_start_rg(name_t rgname, boolean_t ok_flag,
    const char *zonename)
{
	FAULTPT_RGM(FAULTNUM_RGM_AFTER_RGMCNFG_SET_OK_TO_START,
		FaultFunctions::generic);

	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	boolean_t already_set_in_ccr;
	Environment e;
	char tablename[MAXRGMOBJNAMELEN + sizeof (RGM_PREFIX_RG)];
	ccr::updatable_table_var transptr = NULL;
	ccr::directory_var ccrdir = lookup_ccrdir();
	char *cluster_name;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);


	if (rgname == NULL) {
		res.err_code = SCHA_ERR_INVAL;
		free(cluster_name);
		return (res);
	}

	already_set_in_ccr = rgmcnfg_is_ok_start_rg(rgname, zonename);
	if ((ok_flag && already_set_in_ccr) ||
	    (!ok_flag && !already_set_in_ccr)) {
		// ok; do nothing.
		free(cluster_name);
		return (res);
	}

	(void) sprintf(tablename, "%s%s", RGM_PREFIX_RG, rgname);

	transptr = ccrdir->begin_transaction_zc(cluster_name, tablename, e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_RG;
		return (res);
	}

	if (ok_flag) {
		// The "OK_TO_START" is not in ccr yet.
		// Add OK_TO_START to ccr table
		res = ccr_put_string(transptr, RG_OK_TO_START, "");
		if (res.err_code != SCHA_ERR_NOERR) {
			transptr->abort_transaction(e);
			if (e.exception())
				// this exception is not important
				e.clear();
			free(cluster_name);
			return (res);
		}
	} else {
		// Remove OK_TO_START from ccr table
		transptr->remove_element(RG_OK_TO_START, e);
		if (e.exception()) {
			e.clear();
			transptr->abort_transaction(e);
			if (e.exception())
				// this exception is not important
				e.clear();
			res.err_code = SCHA_ERR_CCR;
			free(cluster_name);
			return (res);
		}
	}

	transptr->commit_transaction(e);
	if (e.exception()) {
		e.clear();
		res.err_code = SCHA_ERR_CCR;
	}
	free(cluster_name);
	return (res);
}


//
// Function to open an RG or RT CCR table for reading. It skips empty tables
// left behind by aborted creation attempts.
//
static scha_errmsg_t
open_ccr_table(char *objname, rgm_obj_type_t objtype,
    ccr::readonly_table_var &tabptr, const char *zonename)
{
	char tablename[MAXRGMOBJNAMELEN +
	    MAX(sizeof (RGM_PREFIX_RT), sizeof (RGM_PREFIX_RG))];
	scha_errmsg_t res = {SCHA_ERR_NOERR, NULL};
	int32_t num_elems = 0;
	Environment e;
	CORBA::Exception *ex;
	const char *prefix_rt = RGM_PREFIX_RT;
	const char *prefix_rg = RGM_PREFIX_RG;
	char *cluster_name = NULL;

	if ((cluster_name = get_clustername_from_zone(zonename, &res)) == NULL)
		return (res);


	ccr::directory_var ccrdir = lookup_ccrdir();
	tabptr = NULL;

	if (objtype == O_RT)
		(void) sprintf(tablename, "%s%s", prefix_rt, objname);
	else if (objtype == O_RG)
		(void) sprintf(tablename, "%s%s", prefix_rg, objname);
	else {
		// CCR tables only exist for RTs & RGs.
		res.err_code = SCHA_ERR_INVAL;
		goto finish_open_ccr_table; //lint !e801
	}

	// set default error code
	if (objtype == O_RT)
		res.err_code = SCHA_ERR_RT;
	else
		res.err_code = SCHA_ERR_RG;

	while (1) {
		tabptr = ccrdir->lookup_zc(cluster_name, tablename, e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::no_such_table::_exnarrow(ex) == NULL) {
				// ccr read error. override error code
				res.err_code = SCHA_ERR_CCR;
			}
			e.clear();
			goto finish_open_ccr_table; //lint !e801
		}

		num_elems = tabptr->get_num_elements(e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::table_modified::_exnarrow(ex)) {
				//
				// The CCR table has been modified after it was
				// opened; retry after a small delay.
				//
				e.clear();
				(void) sleep(1 /* second */);
				continue;
			}
			e.clear();
			res.err_code = SCHA_ERR_CCR;
			goto finish_open_ccr_table; //lint !e801
		}

		if (num_elems == 0) { // invalid table
			goto finish_open_ccr_table; //lint !e801
		}
		res.err_code = SCHA_ERR_NOERR;
		goto finish_open_ccr_table; //lint !e801
	} // while
finish_open_ccr_table:
	if (cluster_name)
		free(cluster_name);

	return (res);
}

//
// Function to check if a given CCR table is empty.
// This function should be called with the update lock held for the table.
// No exceptions are expected from the CCR.
//
static boolean_t
is_ccr_table_empty(const char *tablename, const char *cluster_name)
{
	int32_t num_elems = 0;
	Environment e;

	ccr::directory_var ccrdir = lookup_ccrdir();

	ccr::readonly_table_var tabptr = ccrdir->lookup_zc(
	    cluster_name, tablename, e);
	CL_PANIC(e.exception() == NULL);

	num_elems = tabptr->get_num_elements(e);
	CL_PANIC(e.exception() == NULL);

	if (num_elems == 0)
		return (B_TRUE);
	else
		return (B_FALSE);
}

//
// rgm_check_vp_version
//
// This function checks if the version in CCR is up-to-date.
//
// It also verifies that we are not backing up to an incompatible version.
// If the rgmd is running at lower than 2.0, the version in the CCR cannot be
// 2.0 or higher. If so, abort the rgmd.
//
// If the ccr table doesn't exist, the version in the CCR is lower than
// the running version, or the version in the CCR is backwards compatible
// with the running version, return B_TRUE to tell the caller (rgmd) that it
// needs to create the table or update it with the current version.
//
// B_FALSE will be returned if the CCR table has the current version and
// thus should not be updated.
//
boolean_t
rgm_check_vp_version(const uint16_t rgm_major_num, const uint16_t rgm_minor_num)
{
	Environment e;
	CORBA::Exception *ex;
	int ccr_major, ccr_minor;
	sc_syslog_msg_handle_t handle;

	ccr::directory_var ccrdir = lookup_ccrdir();
	ccr::readonly_table_var tabptr = ccrdir->lookup(RGM_VP_TABLE, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(ex)) {
			e.clear();
			return (B_TRUE);
		} else {
			// invalid ccr table
			//
			// SCMSGS
			// @explanation
			// The rgmd daemon was unable to open the cluster
			// configuration repository (CCR). The rgmd will
			// produce a core file and will force the node to halt
			// or reboot to avoid the possibility of data
			// corruption.
			// @user_action
			// Save a copy of the /var/adm/messages files on all
			// nodes, and of the rgmd core file. Contact your
			// authorized Sun service provider for assistance in
			// diagnosing the problem.
			//
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("CCR: Invalid CCR table : %s."),
			    RGM_VP_TABLE);
			sc_syslog_msg_done(&handle);
			abort();
		}
	}
	e.clear();

	// More sanity checks are done in kernal.  See
	// usr/src/common/cl/vm/vm_internal_custom.cc

	if ((ccr_get_int(tabptr, VP_MAJOR_NUM, &ccr_major)).err_code !=
	    SCHA_ERR_NOERR ||
	    (ccr_get_int(tabptr, VP_MINOR_NUM, &ccr_minor)).err_code !=
	    SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: Failed to read CCR"));
		sc_syslog_msg_done(&handle);
		abort();
	}

	if (ccr_major == rgm_major_num &&
	    ccr_minor == rgm_minor_num) {
		// The version is already up-to-date.
		return (B_FALSE);
	}

	// We can't go from 2.0 or higher to before 2.0.
	if (ccr_major >= 2 && rgm_major_num < 2) {
		//
		// SCMSGS
		// @explanation
		// The cluster software has been rolled back to a version
		// that is incompatible with the current CCR contents.
		// @user_action
		// Complete an upgrade of the cluster software to the
		// higher version at which it was previously running.
		//
		(void) sc_syslog_msg_initialize(&handle,
		    SC_SYSLOG_RGM_RGMD_TAG, "");
		(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
		    SYSTEXT("fatal: The rgm is trying to run at a lower "
			"version than that supported by the CCR contents."));
		sc_syslog_msg_done(&handle);
		abort();
	}

	// The version in CCR will need to be updated.
	return (B_TRUE);
}

//
// rgm_set_vp_version
//
// Create the CCR table with the current vp versioning if it doesn't exist.
// Otherwise, update the numbers in CCR with the current version.
//
void
rgm_set_vp_version(const uint16_t rgm_major_num, const uint16_t rgm_minor_num)
{
	Environment e;
	CORBA::Exception *ex;
	ccr::updatable_table_var transptr;
	sc_syslog_msg_handle_t handle;

	ccr::directory_var ccrdir = lookup_ccrdir();
	transptr = ccrdir->begin_transaction(RGM_VP_TABLE, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(ex)) {
			// CCR table for versioning doesn't exist.
			// Create the table with the input version number
			e.clear();
			ccrdir->create_table(RGM_VP_TABLE, e);
			if ((ex = e.exception()) != NULL) {
				// ccr create table failed
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle,
				    LOG_ERR, MESSAGE,
				    SYSTEXT("CCR: Create table %s failed."),
				    RGM_VP_TABLE);
				sc_syslog_msg_done(&handle);
				goto abort_set_version;
			}

			e.clear();
			transptr = ccrdir->begin_transaction(RGM_VP_TABLE, e);
			if (e.exception()) {
				(void) sc_syslog_msg_initialize(&handle,
				    SC_SYSLOG_RGM_RGMD_TAG, "");
				(void) sc_syslog_msg_log(handle,
				    LOG_ERR, MESSAGE,
				    SYSTEXT("fatal: Unable to open CCR"));
				sc_syslog_msg_done(&handle);
				goto abort_set_version;
			}
		} else {
			// failed to open CCR table
			(void) sc_syslog_msg_initialize(&handle,
			    SC_SYSLOG_RGM_RGMD_TAG, "");
			(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
			    SYSTEXT("fatal: Unable to open CCR"));
			sc_syslog_msg_done(&handle);
			goto abort_set_version;
		}
	} else {
		// There are only two elements in the CCR and we are going
		// to overwrite them with the current version numbers.
		// Empty the table first and then write the numbers back in.
		e.clear();
		transptr->remove_all_elements(e);
		if (e.exception()) {
			goto syslog_update_failed;
		}
	}

	if ((ccr_put_int(transptr, VP_MAJOR_NUM, rgm_major_num)).
	    err_code != SCHA_ERR_NOERR) {
		goto syslog_update_failed;
	}

	if ((ccr_put_int(transptr, VP_MINOR_NUM, rgm_minor_num)).
	    err_code != SCHA_ERR_NOERR) {
		goto syslog_update_failed;
	}

	e.clear();
	transptr->commit_transaction(e);
	if (e.exception()) {
		goto syslog_update_failed;
	}

	e.clear();
	return;

syslog_update_failed:

	(void) sc_syslog_msg_initialize(&handle, SC_SYSLOG_RGM_RGMD_TAG, "");
	(void) sc_syslog_msg_log(handle, LOG_ERR, MESSAGE,
	    SYSTEXT("CCR: Failed to update table %s."), RGM_VP_TABLE);
	sc_syslog_msg_done(&handle);

abort_set_version:

	if (e.exception())
		e.clear();

	if (transptr) {
		transptr->abort_transaction(e);
		if (e.exception())
			// this exception is not important
			e.clear();
	}

	abort();
}

//
// Fault point function to simulate two kinds of scenarios:
//
// (1)  a failure between an empty CCR table creation and the operation
//	of filling the table.
//
// (2)  a concurrent deletion or udpate of a table while the table is being
//	read.
//
// This function is a no-op unless the DEBUG compiler toggle is turned on.
// We cannot use rgmd's global debug flag "Debugflag" since this code is
// invoked by admin tools in addition to rgmd.
//
// It checks if there exists a file /tmp/rgm_debug_ccr_table_<operation>.
// If so, it prints a debug message and sleeps for the amount of time
// (in seconds) specified in the file (with a default of 60 seconds) before
// returning. When the message appears on the console or admin terminal,
// the operator is expected to inject the appropriate error.
//
static void
#ifdef DEBUG
FAULT_POINT_CCR_TABLE(const char *tablename, const char *operation)
#else
FAULT_POINT_CCR_TABLE(const char *, const char *)
#endif
{
#ifdef DEBUG
	FILE *fd;
	char filename[MAXNAMELEN];

	(void) sprintf(filename, "%s%s", "/tmp/rgm_debug_ccr_table_",
	    operation);
	if ((fd = fopen(filename, "r")) != NULL) {
		int seconds_to_sleep = 0;
		(void) fscanf(fd, "%d", &seconds_to_sleep);
		(void) fclose(fd);
		if (seconds_to_sleep <= 0)
			seconds_to_sleep = 60 /* default */;
		(void) printf("FAULT_POINT_CCR_TABLE_%s: "
		    "working on CCR table <%s>; "
		    "sleeping for %d seconds before proceeding.\n",
		    operation, tablename, seconds_to_sleep);
		os::usecsleep(seconds_to_sleep * 1000 * 1000);
	}
#endif
}
#endif /* !EUROPA_FARM */
