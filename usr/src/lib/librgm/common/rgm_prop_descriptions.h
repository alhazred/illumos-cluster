/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_RGM_PROP_DESCRIPTIONS_H
#define	_RGM_PROP_DESCRIPTIONS_H

#pragma ident	"@(#)rgm_prop_descriptions.h	1.7	08/05/20 SMI"

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * This file contains descriptions of all system-defined properties. The
 * descriptions are stored here as they cannot be changed in the RTR file.
 * This is for fixing CR 4313742.
 */

struct description_mappings {
	const char *name;
	const char *desc;
};

/*
 * The function set_default_client_aff_props() relies upon the ordinal
 * position of entries in the following table.  To preserve those
 * ordinal positions, new entries should be added only to the end of the
 * initializer; and deletions should be avoided.
 * If any entries are added, also update the value of NUM_SYS_PARAMS below.
 */

/*lint -e786 String concatenation within initializer */
#define	SYS_PROP_DESCRIPTIONS { \
	{SCHA_SCALABLE, "Indicates whether network load balancing " \
		"features are used."}, \
	{SCHA_START_TIMEOUT, "Maximum execution time allowed for " \
		"Start method."}, \
	{SCHA_STOP_TIMEOUT, "Maximum execution time allowed for " \
		"Stop method."}, \
	{SCHA_VALIDATE_TIMEOUT, "Maximum execution time allowed for " \
		"Validate method."}, \
	{SCHA_UPDATE_TIMEOUT, "Maximum execution time allowed for " \
		"Update method."}, \
	{SCHA_INIT_TIMEOUT, "Maximum execution time allowed for " \
		"Init method."}, \
	{SCHA_FINI_TIMEOUT, "Maximum execution time allowed for " \
		"Fini method."}, \
	{SCHA_BOOT_TIMEOUT, "Maximum execution time allowed for " \
		"Boot method."}, \
	{SCHA_MONITOR_START_TIMEOUT, "Maximum execution time allowed " \
		"for Monitor_Start method."}, \
	{SCHA_MONITOR_STOP_TIMEOUT, "Maximum execution time allowed " \
		"for Monitor_Stop method."}, \
	{SCHA_MONITOR_CHECK_TIMEOUT, "Maximum execution time allowed " \
		"for Monitor_Check method."}, \
	{SCHA_PRENET_START_TIMEOUT, "Maximum execution time allowed " \
		"for Prenet_Start method."}, \
	{SCHA_POSTNET_STOP_TIMEOUT, "Maximum execution time allowed " \
		"for Postnet_stop method."}, \
	{SCHA_FAILOVER_MODE, "Modifies recovery actions taken when the " \
		"resource fails."}, \
	{SCHA_NETWORK_RESOURCES_USED, "A list of logical hostname or shared " \
		"address resource dependencies of this resource."}, \
	{SCHA_PORT_LIST, "List of ports on which the server is listening."}, \
	{SCHA_LOAD_BALANCING_POLICY, "Determines how the load is balanced " \
		"across different nodes."}, \
	{SCHA_LOAD_BALANCING_WEIGHTS, "Indicates the weights taken by " \
		"different nodes for balancing the load."}, \
	{SCHA_AFFINITY_TIMEOUT, "Time during which connections from a client " \
		"IP address are sent to the same server node."}, \
	{SCHA_UDP_AFFINITY, "Indicates whether all UDP traffic from a client " \
		"is sent to same server node."}, \
	{SCHA_WEAK_AFFINITY, "Indicates whether weak form of client " \
		"affinity is enabled."}, \
	{SCHA_CHEAP_PROBE_INTERVAL, "Time between invocations of a quick " \
		"fault probe of the resource."}, \
	{SCHA_THOROUGH_PROBE_INTERVAL, "Time between invocations of a " \
		"high-overhead fault probe of the resource."}, \
	{SCHA_RETRY_COUNT, "Indicates the number of times a monitor restarts " \
		"the resource if it fails."}, \
	{SCHA_RETRY_INTERVAL, "Time in which monitor attempts to restart a " \
		"failed resource Retry_count times."}, \
	{SCHA_ROUND_ROBIN, "Indicates we use round robin " \
		"load balancing scheme"}, \
	{SCHA_CONN_THRESHOLD, "Connections threshold after " \
		"which the cleanup thread is started"}, \
	{SCHA_GENERIC_AFFINITY, "Indicates whether all traffic from a client " \
		"is sent to same server node."}, \
	{SCHA_GLOBAL_ZONE_OVERRIDE, "Overrides the Global_zone property of " \
		"the resource type."} \
} /*lint +e786 String concatenation within initializer */

#define	NUM_SYS_PARAMS 29

#ifdef	__cplusplus
}
#endif

#endif	/* !_RGM_PROP_DESCRIPTIONS_H */
