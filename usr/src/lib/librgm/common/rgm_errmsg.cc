/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)rgm_errmsg.cc	1.114	09/01/06 SMI"

#include <sys/sol_version.h>
// RGM error messages start with the prefix REM_
// RGM warning messages start with the prefix RWM_.
// The warning messages are at the end of the file.

// Error messages

const char *REM_INVALID_NODE = "%s: Invalid node";
const char *REM_INVALID_RG = "%s: Invalid resource group";
const char *REM_DUP_RG = "Duplicate resource groups";
const char *REM_INVALID_RS = "%s: Invalid resource";
const char *REM_INVALID_RT = "Resource type %s is not registered";
const char *REM_RG_EXISTS =  "%s: resource group exists; cannot create";
const char *REM_RS_EXISTS =  "%s: resource exists; cannot create";
const char *REM_RT_EXISTS =  "%s: resource type exists; cannot create";
const char *REM_EMPTY_NODELIST = "Resource group %s cannot have empty "
	"nodelist";
const char *REM_NODES_NOT_IN_NODELIST = "Skipping resource %s because none of "
	"the specified nodes are in the node list of its resource group %s.";
const char *REM_RG_ON_NODE = "Resource group %s is ONLINE on node %s; cannot "
	"remove this node from nodelist";
const char *REM_RG_MP_GT_0 = "The Maximum_primaries of resource group %s must "
	"be greater than zero";
const char *REM_RG_MP_LT_CURRMASTER = "The Maximum_primaries of resource "
	"group %s must be greater than or equal to the number of current "
	"masters of the resource group";
const char *REM_RG_DP_GT_MP = "The Desired_primaries of resource group %s "
	"must be less than or equal to Maximum_primaries for the resource "
	"group";
const char *REM_RG_DP_GT_NODES = "%s: Desired_primaries must be less than "
	"or equal to the number of nodes in the resource group's nodelist";
const char *REM_RG_FO_MP_GT_1 = "%s: Maximum_Primaries must be 1 for a "
	"Failover type resource group";
const char *REM_RG_FO_DP_GT_1 = "%s: Desired_primaries must be no greater "
	"than 1 for a Failover type resource group";
const char *REM_RG_DEP_NON_RG = "Resource group %s cannot have dependency on "
	"resource group %s which does not exist";
const char *REM_RG_AFF_NON_RG = "Resource group %s cannot have an affinity "
	"for resource group %s which does not exist";
const char *REM_RG_AFF_SELF = "Resource group %s cannot have an affinity "
	"for itself";
const char *REM_RG_AFF_MANAGED = "Request failed: managed resource group %s "
	"cannot have a strong positive affinity for unmanaged resource group "
	"%s";
const char *REM_RG_AFF_STATE = "Request failed: resource group %s declares "
	"a strong %s affinity for resource group %s; but this affinity "
	"is not satisfied by the current state of the two resource groups";
const char *REM_RG_AFF_AUTO_START = "Resource group <%s> cannot have "
	"Auto_start_on_new_cluster=TRUE while having a strong positive "
	"affinity for resource group <%s> with Auto_start_on_new_cluster=FALSE";
const char *REM_RG_AFF_DELEGATE_MANY = "Resource group %s declares "
	"more than one target resource group for failover delegation";
const char *REM_NODELIST_NO_COMMON = "Request failed: Resource group %s has "
	"a strong positive affinity for resource group %s; but the Nodelists "
	"of these two resource groups do not have any nodes in common";
const char *REM_RG_DEL_AFF = "Cannot delete resource group %s because there is "
	"another resource group which has an affinity for this resource group";
const char *REM_RG_DEP_UNMAN_RG = "Resource group %s cannot be in managed state"
	" and have a dependency on resource group %s which is unmanaged";
const char *REM_PP_IV_LT_0 = "Pingpong_interval for %s cannot be less than "
	"zero";
const char *REM_PROP_TUNE_AT_CREATION = "Update failed: property %s is "
	"configured to be tunable only at resource creation";
const char *REM_PROP_TUNE_NONE = "Request failed: property %s is configured "
	"to be non-tunable";
const char *REM_PROP_TUNE_WHEN_DISABLED = "Update failed: property %s is "
	"configured to be tunable only on nodes on which the resource "
	"has been disabled";
const char *REM_PROP_NOT_FOUND = "%s: property not found";
const char *REM_PROP_NO_DEFAULT = "Property <%s> that does not have a default "
	"value must be assigned a value in this command";
const char *REM_PROP_EMPTYLIST = "Property <%s> cannot be set to the empty "
	"list because %s is greater than zero";
const char *REM_PROP_SCALABLE = "Property %s cannot be set or updated for "
	"resource %s because Scalable property is not set to TRUE";
const char *REM_RG_R_MISMATCH = "Adding Failover type resource %s into "
	"Scalable mode resource group %s is not allowed";
const char *REM_RG_UNMANAGED = "Skipping resource group %s because it is "
	"unmanaged";
const char *REM_INTERNAL = "Internal system error";
const char *REM_INTERNAL_MEM = "Internal error: out of memory";
const char *REM_INTERNAL_RG_CR = "Internal error: could not create resource "
	"group %s";
const char *REM_INTERNAL_RG_UP = "Internal error: could not update resource "
	"group %s";
const char *REM_INTERNAL_RG_DL = "Internal error: could not delete resource "
	"group %s";
const char *REM_RG_BUSY = "%s: resource group is undergoing a "
	"reconfiguration, try again later";
const char *REM_RS_IN_BUSY_RG = "Skipping resource %s because its resource "
	"group %s is undergoing a reconfiguration.";
const char *REM_RS_IN_SYSTEM_RG = "Cannot enable/disable resource %s because "
	"its resource group has the RG_system property set to TRUE.";
const char *REM_RG_AFF_BUSY = "Cannot switch resource group %s because "
	"resource group %s, which is related to it by strong affinities and "
	"must be switched concurrently, is busy.";
const char *REM_RG_AFF_SAME = "Cannot switch resource group %s online on node "
	"%s because resource group %s, which must be switched off of the node "
	"because of strong affinities, is also being switched online.";
const char *REM_RG_AFF_STOPFAILED = "Cannot switch resource group %s because "
	"one or more resource groups, which are related to it by strong "
	"affinities and must be switched concurrently, are in an errored "
	"state.";
const char *REM_RS_IN_STOPFAILED_RG = "Skipping resource %s because its "
	"resource group %s is in ERROR_STOP_FAILED state.";
const char *REM_RS_IN_RG_TO_STOPFAILED = "Request failed for resource "
	"%s because its resource group %s is in ERROR_STOP_FAILED state.";
const char *REM_RG_STRONG_POS = "Cannot switch resource group %s online on "
	"node %s because it has strong positive affinities for resource "
	"group(s) {%s}, which are not online on that node.";
const char *REM_RG_STRONG_NEG = "Cannot switch resource group %s online on "
	"node %s because it has strong negative affinities for resource "
	"group(s) {%s}, which are online or will be online.";
const char *REM_RG_STOPFAILED = "Skipping resource group %s because it is "
	"in ERROR_STOP_FAILED state";
const char *REM_UPDATE_RS_LIST = "%s: Resource_list cannot be edited directly; "
	"use the clresource(1CL) command to add or remove resources.";
const char *REM_RG_DEL_RS = "%s: resource group contains resources; "
	"cannot delete";
const char *REM_RG_DEL_DEP = "Cannot delete resource group %s because "
	"resource group %s depends on it";
const char *REM_PROP_CANT_UPDATE = "%s: property is not allowed to be updated";
const char *REM_DUPNODE = "Ignoring duplicate node name %s";
const char *REM_DUPNODE_IN_NODELIST = "Request failed: the nodelist contains "
	"duplicate node names";
const char *REM_VALIDATE_NO_NODES = "%s: resource creation failed because "
	"none of the nodes on which VALIDATE would have run are currently up";
const char *REM_VALIDATE_FAILED = "Validation of resource %s in resource "
	"group %s on node %s failed.";
const char *REM_RS_DEL_ENABLED = "%s: resource must be disabled before "
	"deletion";
const char *REM_RS_DEL_DEP = "Cannot delete resource %s because one or "
	"more other resources depend on it";
const char *REM_RS_RDEP_METHOD = "Resource %s cannot have a restart "
	"dependency because it has one of the START or STOP methods without "
	"the other. Resources that declare restart dependencies must declare "
	"both START and STOP, or neither of them.";
const char *REM_RS_DEP_INVALID_STATE = "Request failed: resource %s cannot "
	"have a dependency on resource %s while resource %s is disabled or "
	"in START_FAILED/STOP_FAILED state";
const char *REM_INTRARG_ANYNODE = "Resource %s can't have ANY_NODE "
	"dependency on resource %s because both are in the same resource "
	"group.";
const char *REM_RS_DUP_DEP = "%s: resource cannot exist in more than one "
	"dependency list";
const char *REM_RT_NODELIST = "The nodelist of resource group %s must be a "
	"subset of the installed nodelist of resource type %s; node %s "
	"violates this requirement because it is not in the installed "
	"nodelist of the resource type.";
const char *REM_RT_0_NODELIST = "%s: resource type cannot have an empty "
	"nodelist";
const char *REM_RT_RG_BUSY = "Resource group %s, containing resource %s of "
	"type %s, is undergoing a reconfiguration; update of "
	"Installed_nodes failed";
const char *REM_RT_DEL_RG_BUSY = "%s: resource group %s is currently being "
	"updated; deletion of resource type failed";
const char *REM_RT_DEL_R_EXISTS = "resource %s of resource type %s exists; "
	"deletion of resource type failed";
const char *REM_NODE_NOT_MEMBER = "Cannot use node %s because it is not "
	"currently in the cluster membership.";
const char *REM_ALLNODE_INVAL = "Request failed because none of the specified "
	"nodes are usable.";
const char *REM_RG_MAXPRIMARIES = "Request failed because it would cause the "
	"Maximum_primaries of resource group %s to be exceeded";
const char *REM_RG_MASTERS = "Request failed because node %s is not a "
#if SOL_VERSION >= __s10
	"potential primary for resource group %s. Ensure that when a zone is "
	"intended, it is explicitly specified by using the node:zonename "
	"format.";
#else
	"potential primary for resource group %s";
#endif

const char *REM_RG_TO_STOPFAILED = "Request failed because resource group %s "
	"is in ERROR_STOP_FAILED state and requires operator attention";
const char *REM_RG_UNMAN_DEPEND = "Cannot take resource group %s UNMANAGED "
	"because one or more other resource groups depend on it";
const char *REM_RG_UNMAN_NOT_OFF = "Cannot take resource group %s UNMANAGED "
	"because it is in a non-OFFLINE state on one or more nodes in the "
	"cluster";
const char *REM_RG_UNMAN_RS_NOT_DIS = "Request failed: resource %s which "
	"belongs to resource group %s is not disabled";
const char *REM_R_DEP_DISABLED = "Cannot enable resource %s because it "
	"depends on one or more resources which are either disabled or "
	"in START_FAILED/STOP_FAILED state";
const char *REM_R_DEP_ENABLED = "Cannot disable resource %s because one or "
	"more enabled resources depend upon it and must be disabled first";
const char *REM_CLRECONF = "Cluster is undergoing a reconfiguration";
const char *REM_UPDATE_RGMODE = "%s: the rg_mode property can be set only "
	"when the resource group is created.";
const char *REM_RG_DEP_CYCLIC = "Request failed on resource group %s because "
	"the combination of resource group dependencies and/or strong "
	"affinities would create a cycle";
const char *REM_RG_CYCLIC_COMBINED = "Request failed on resource group %s "
	"because the combination of resource group dependencies and "
	"resource dependencies would create a cycle";
const char *REM_RG_CYCLIC_IMPL_DEP = "Request failed on resource group %s "
	"because setting implicit network dependencies would create a cycle";
const char *REM_RS_CYCLIC_COMBINED = "Request failed on resource %s because "
	"the combination of resource group dependencies and resource "
	"dependencies would create a cycle";
const char *REM_RS_DEP_CYCLIC = "Request failed on resource %s because the "
	"resource dependencies would create a cycle";
const char *REM_RS_DEP_NET_CYCLIC = "Request failed on resource %s because a "
	"combination of net-relative resource dependencies and explicit "
	"resource dependencies creates a cycle";
const char *REM_RS_NOT_NR = "Resource %s specifies a resource %s in its "
	"Network_resources_used property that is not a network resource.";
const char *REM_RG_NAME_INVALID = "%s: Invalid resource group name";
const char *REM_RS_NAME_INVALID = "%s: Invalid resource name";
const char *REM_INVALID_ENUM_LIST = "%s: Invalid enumeration literal name";
const char *REM_INVALID_PROP_NAME = "%s: Invalid property name";
const char *REM_INVALID_PROP_VALUES = "%s: Invalid property value";
const char *REM_INVALID_FLAG = "the error flag specified is incorrect";
const char *REM_RT_NAME_INVALID = "%s: Invalid resource type name";
const char *REM_INVALID_DESC_VALUES = "%s: Invalid description value";
const char *REM_UNMANAGED_BUSTED = "Internal error: unmanaged resource group "
	"%s lost a current master when a node died";
const char *REM_OFFLINE_BUSTED = "Internal error: offline resource group %s "
	"lost a current master when a node died";
const char *REM_SINGLE_INST = "Cannot add resource of type %s with "
	"Single_instance=TRUE because resource %s of this type already exists";
const char *REM_RG_DEP_ONLINE = "Resource group %s cannot be offline while "
	"resource groups {%s} which depend on it are online";
const char *REM_R_DEP_ONLINE = "Resource group %s cannot be offline while "
	"resources {%s} which depend on resources in it are online";
const char *REM_RG_DEP_OFFLINE = "Resource group %s cannot be online while "
	"having a dependency on offline resource groups {%s}";
const char *REM_RG_DEP_DESIRED_PRIMARIES = "Resource group <%s> cannot have "
	"Desired_primaries > 0 while having a dependency on resource "
	"group <%s> with Desired_primaries = 0";
const char *REM_RG_DEP_AUTO_START = "Resource group <%s> cannot have "
	"Auto_start_on_new_cluster=TRUE while having a dependency on "
	"resource group <%s> with Auto_start_on_new_cluster=FALSE";
const char *REM_PROP_PSTR_MINLEN = "%s: The string length of property value "
	"is less than MINLENGTH";
const char *REM_PROP_PSTR_MAXLEN = "%s: The string length of property value "
	"is greater than MAXLENGTH";
const char *REM_PROP_PSARRAY_MINSIZE = "%s: The number of array elements of "
	"property value is less than ARRAY_MINSIZE";
const char *REM_PROP_PSARRAY_MAXSIZE = "%s: The number of array elements of "
	"property value is greater than ARRAY_MAXSIZE";
const char *REM_PROP_PINT_MIN = "%s: Property value is less than MIN";
const char *REM_PROP_PINT_MAX = "%s: Property value is greater than MAX";
const char *REM_RGM_NOT_STARTED = "RGM has not yet started";
const char *REM_PROP_NOT_EXIST = "%s: property is not declared in resource "
	"type registration file";
const char *REM_PROP_IS_EXT = "There is no updatable standard resource "
	"property with the name %s.  If the extension property was intended, "
	"use the -x option instead of -y on the command line.";
const char *REM_PROP_IS_SYS = "The resource type has no extension property "
	"with the name %s.  If the standard property was intended, use the -y "
	"option instead of -x on the command line.";
const char *REM_START_FAILED = "On node %s, resource group %s is online but "
	"resource %s failed to start";
const char *REM_RG_FO_STARTFAIL = "Resource group %s failed to start on "
	"chosen node and might fail over to other node(s)";
const char *REM_RG_NOMASTER = "No primary node could be found for resource "
	"group %s; it remains offline";
const char *REM_RG_SW_BUSTED = "Requested switch operation on resource group "
	"%s was interrupted by a node death and might have to be re-tried";
const char *REM_DUP_RG_DEP = "%s has a duplicate resource group, %s, in its "
	"%s property.";
const char *REM_DUP_RS_DEP = "%s has a duplicate resource, %s, in its %s "
	"property. Use clresource(1CL) to view the current dependency list, "
	"or to assign it a new value.";
const char *REM_DUP_RS_NETRESUSED = "%s has a duplicate network resource, %s, "
	"in its Network_resources_used property. Use clresource(1CL) to view "
	"the current Network_resources_used list, or to assign it a "
	"new value.";
const char *REM_FED_RPC = "Call to rpc.fed failed for resource %s, method %s.";
const char *REM_FED_SIGNAL = "Method %s on resource %s stopped or terminated "
	"due to receipt of signal %d.";
const char *REM_FED_ABNORM = "Method %s on resource %s terminated abnormally.";
const char *REM_FED_VALIDATE = "VALIDATE on resource %s, resource group %s, "
	"exited with non-zero exit status.";
const char *REM_FED_FAILED_EXEC = "Method %s failed to execute on resource %s "
	"in resource group %s, error: %s.";
const char *REM_FED_DUP = "Error: duplicate method %s launched on "
	"resource %s in resource group %s.";
const char *REM_FED_EXEC = "Method %s on resource %s: program file is not "
	"executable.";
const char *REM_FED_STAT = "Method %s on resource %s: stat of program file "
	"failed.";
const char *REM_FED_UNKNC = "Method %s: unknown command.";
const char *REM_FED_AUTH = "Method %s on resource %s: authorization error: "
	"%s.";
const char *REM_FED_CONNECT = "Method %s on resource %s: RPC connection "
	"error.";
const char *REM_FED_TIMEDOUT = "Method %s on resource %s: Method timed out.";
const char *REM_FED_QUIESCED = "Method %s on resource %s: quiesced.";
const char *REM_FED_NOTAG = "Method %s on resource %s: Execution failed: no "
	"such method tag.";
// SC SLM addon start
const char *REM_FED_SLM_ERROR = "Method %s on resource %s: Execution failed: "
	"SLM error";
// SC SLM addon end

const char *REM_RG_NGZONE = "Operation is not allowed from non-global zone.";
const char *REM_RG_NODELIST_NGZONE = "Operation on resource group <%s> is not "
	"allowed in non-global zone <%s> because this zone is not in its "
	"nodelist.";
const char *REM_PROP_NGZONE = "The <%s> property is not allowed to be modified "
	"from a non-global zone.";

const char *REM_RT_NGZONE = REM_RG_NGZONE;

const char *REM_R_NODELIST_NGZONE = "Operation on resource <%s> is not allowed "
	"in non-global zone <%s> because this zone is not in the nodelist of "
	"the resource group <%s>.";
const char *REM_R_GLOBALFLAG_NGZONE = "Operation on resource <%s> is not "
	"allowed in non-global zone <%s> because the resource type has "
	"Global_zone flag set to TRUE.";

const char *REM_EVAC_NGZONE = "A zone (or node) can't be evacuated from another"
	" non-global zone.";
// Non global zone can't evacuate any zone or node other than itself.";

const char *REM_RG_START_FAILED = "Not attempting to start resource group <%s>"
	" on node <%s> because this resource group has already failed to"
	" start on this node %d or more times in the past %d seconds";
const char *REM_WRONG_OS = "Property %s is not supported in Solaris 8.";
const char *REM_MULT_RT_MATCHES = "Resource type name %s matches more than "
	"one registered resource type.  Specify a qualifying vendor id prefix "
	"or version suffix.";
const char *REM_VENDOR_ID_INVALID = "%s: Invalid resource type vendor id name";
const char *REM_RT_NO_VERSION = "The RT_Version property is not set in the "
	"resource type registration file.";
const char *REM_ILLEGAL_RT_VERS_CHARS = "The %s in the "
	"resource type registration file contains illegal characters.";
const char *REM_UPGRADE_AT_CREATION = "Resource <%s> cannot be changed "
	"to version <%s> because its resource type <%s> does not allow "
	"it. You must create a new resource instead.";
const char *REM_UPGRADE_WHEN_UNMONITORED = "Resource <%s> can only be "
	"changed to version <%s> of the resource type <%s> when monitoring "
	"has been disabled on the resource.";
const char *REM_UPGRADE_WHEN_UNMANAGED = "Resource <%s> can only be "
	"changed to version <%s> of the resource type <%s> when its "
	"resource group <%s> has been switched to unmanaged state.";
const char *REM_UPGRADE_WHEN_OFFLINE = "Resource <%s> can only be changed "
	"to version <%s> of the resource type <%s> when it is offline.";
const char *REM_UPGRADE_WHEN_DISABLED = "Resource <%s> can only be changed "
	"to version <%s> of the resource type <%s> when it is disabled.";
const char *REM_UPDATE_TV_AT_CREATTION = "Property <Type_version> cannot "
	"be updated at resource creation.";
const char *REM_SYSTEM_RT = "Operation not allowed on system resource type "
	"<%s>";
const char *REM_SYSTEM_RG = "Operation not allowed on system resource group "
	"<%s>";
const char *REM_SYSTEM_RG_SWPRIM = "Operation not allowed on system resource "
	"group <%s> because it would switch it offline on all nodes";
const char *REM_SYSTEM_RES = "Operation not allowed on resource <%s> which "
	"is contained in system resource group <%s>";
const char *REM_API_VERSION_INVALID = "Invalid API_version <%u>.";
const char *REM_API_VERSION_LOW = "Invalid API_version. This resource type "
	"requires a newer version of Sun Cluster software, with API_version "
	"greater than or equal to <%u>.";
const char *REM_EVAC_FAILED = "Failed to evacuate all resource groups from "
	"cluster node %s";
const char *REM_RG_NAMESERVER = "An internal nameserver error prevented the "
	"RGM from quiescing resource group %s";
// SC SLM addon start
const char *REM_RG_SLM_TYPE_WR = "%s : RG_SLM_TYPE must be either %s or %s";
const char *REM_RG_SLM_PSET_TYPE_WR = "%s : "
	"RG_SLM_PSET_TYPE must be either %s, %s or %s";
const char *REM_RG_SLM_CPU_GARBAGE = "%s : %s value is not a valid number";
const char *REM_RG_SLM_CPU_TOO_BIG = "%s : %s value is too big";
const char *REM_RG_SLM_PSET_MIN_TOO_BIG = "%s : %s value is too big, "
	"must be less than or equal to RG_SLM_CPU_SHARES/100 value";
const char *REM_RG_SLM_CPU_ZERO = "%s : %s value cannot be zero";
const char *REM_RG_SLM_NAME_LENGTH = "%s : resource group name for SLM has"
	" maximum %d characters";
const char *REM_RG_SLM_NAME_CLASH = "%s : resource group names with "
	"RG_SLM_TYPE=automated cannot differ only by '-' or '_'";
const char *REM_SLM_PSET_AND_ZONE = "%s : only non-global zones may be present "
	"in the node list if RG_SLM_PSET_TYPE is set to either 'weak'"
	" or 'strong'";
const char *REM_SLM_PSET_TYPE_IDENTICAL_IN_ZONE = "%s : another resource group "
	"(%s) is configured on non-global zone <%s> with another "
	"RG_SLM_PSET_TYPE property";
const char *REM_SLM_NOT_AUTOMATED = "%s : RG_SLM_TYPE property "
	"'manual' and %s not allowed";
const char *REM_SLM_NO_DEDICATED = "%s : RG_SLM_PSET_TYPE property "
	"'strong' or 'weak' is not supported for "
	"this Solaris version";
const char *REM_SLM_NO_PSET_MIN = "%s : RG_SLM_PSET_MIN property is not "
	"supported for this Solaris version";
const char *REM_SLM_TYPE_DIFF_IN_ZONE = "%s : another resource group (%s) is "
	"configured on non-global zone <%s> with another RG_SLM_TYPE property";
// SC SLM addon end
const char *REM_RG_RST_FAILED_ON_NODE  = "Request to restart resource group "
	"%s on node %s failed as the resource group is offline on that "
	"node.";
const char *REM_RG_RST_FAILED  = "Request failed: resource group %s is in "
	"offline state.";
const char *REM_RG_OFFLINING_FAILED = "Failed to bring resource group %s "
	"offline on specified nodes";
const char *REM_RG_ONLINING_FAILED = "Failed to bring resource group %s "
	"online on specified nodes";
const char *REM_INVALID_RTR_PROP = "Registration failed: Invalid declaration "
	"of <%s> property in resource type registration file";


// Inter cluster dependencies start
const char *REM_RGM_IC_UNREACH = "Failed to communicate to remote cluster %s";
const char *REM_RGM_GET_REMOTE_R_STATE_FAILED = "Failed to get state of"
	" %s:%s";
const char *REM_RGM_NOTIFY_REMOTE_R_STATE_FAILED = "Failed to notify state "
	"change for %s:%s";
const char *REM_RGM_CREATE_DEP_INFO_FAILED = "Failed to create inter-cluster "
	"dependencies from %s to %s";
const char *REM_RGM_UPDATE_DEP_INFO_FAILED = "Failed to update inter-cluster "
	"dependencies of %s to %s";
const char *REM_CREATE_ICDEP_LOCAL_CLUSTER = "Do not specify %s for local "
	"cluster dependency %s";
const char *REM_ICRD_SET_FAIL = "Failed to create %s inter-cluster "
	"dependencies for resource %s";
const char *REM_UPDATE_ICDEP_LOCAL_CLUSTER = "Cannot update dependencies as "
	"one of the local cluster dependencies %s is specified in "
	"cluster:resource format";
const char *REM_INVALID_REMOTE_R = "Failed to create or update dependencies "
	"because you specified an invalid remote resource";
const char *REM_CREATE_IC_AFF_LOCAL_CLUSTER = "Do not specify %s for "
	"local cluster affinities for %s";
const char *REM_RGM_CREATE_AFF_INFO_FAILED = "Failed to create inter-cluster "
	"affinities from %s to %s";
const char *REM_RGM_UPDATE_AFF_INFO_FAILED = "Failed to update inter-cluster "
	"affinities of %s to %s";
const char *REM_UPDATE_ICAFF_LOCAL_CLUSTER = "Cannot update affinities as "
	"one of the local cluster affinity is specified in cluster:resource "
	"format";
const char *REM_IC_RG_DEP_NOT_SUPPORTED = "Inter-cluster RG_dependencies "
	"feature is not supported";
const char *REM_ONLY_GZ_ZC_ALLOWED = "Operation is allowed only from a global"
	" zone or a zone cluster";
const char *REM_RGM_IC_NOT_ALLOWED = "Request failed: the version of the "
	"Resource Group Manager (RGM) that you are running does not allow "
	"inter-cluster resource dependencies or inter-cluster resource group"
	"affinities";
const char *REM_RGM_IC_CYCLIC_DEP = "The dependency operation that you "
	"attempted is not permitted because inter-cluster dependencies are "
	"currently being updated";
const char *REM_ICRD_NAMESERVER = "You cannot create or update "
	"inter-cluster resource dependencies because an internal "
	"name server error occurred";
const char *REM_VALUE_OUT_OF_RANGE = "Error: The given value of"
	" the property %s is out of range. The value"
	" should be within range (0-%d).";

// Inter cluster dependencies end

// Warning messages
const char *RWM_PROP_AFFINITY = "NOTICE: Property <%s> can only be set when "
	"property Load_balancing_policy = LB_STICKY or "
	"Load_balancing_policy = LB_STICKY_WILD";
const char *RWM_PROP_RR = "NOTICE: Property <%s> can only be set when "
	"property Round_robin = True";
const char *RWM_VALIDATE_NODES = "WARNING: operation succeeded, but some of "
	"the nodes on which VALIDATE would have run are currently down";
const char *RWM_MON_START_FAILED = "WARNING: on node %s, resource group %s is "
	"online but the monitor of resource %s failed to start";
const char *RWM_PENDING_ONLINE_BLOCKED = "WARNING: on node %s, resource group "
	"%s is in PENDING_ONLINE_BLOCKED state";
const char *RWM_STOP_FAILED = "NOTICE: Operation succeeded, but resource "
	"group %s remains in ERROR_STOP_FAILED state on node %s because "
	"resource %s is still STOP_FAILED.  Manually kill the resource, then "
	"use 'clresource clear' to clear it.";
const char *RWM_SOME_RS_ONLINE = "NOTICE: Operation succeeded, but resource "
	"group %s remains in ERROR_STOP_FAILED state on node %s because some "
	"resources in the group remain online while others are offline.  "
	"To clear this condition, switch the resource group offline.";
const char *RWM_MON_NOT_DEF = "NOTICE: monitoring not enabled on resource %s "
	"because monitoring methods are not defined for this resource type";
const char *RWM_PROJECT_CHANGED = "WARNING: changing the project name won't "
	"take effect until next time the resource is restarted.";
const char *RWM_FINIMETHOD = "WARNING: Resource %s is deleted, but the FINI "
	"method didn't run on the following nodes - %s.";
const char *RWM_RG_DEP_ONLINE = "NOTICE: Resource group %s remains online "
	"while having a dependency on resource group %s which is now "
	"offline.";
const char *RWM_RG_DEP_OFFLINE = "NOTICE: Resource group %s is now online "
	"while having a dependency on resource group %s which remains "
	"offline.";
const char *RWM_AFF_DESIRED_PRIM = "WARNING: the number of nodes in the "
	"intersection of the Nodelists of resource groups %s and %s is less "
	"than the Desired_primaries of resource group %s.";
const char *RWM_NODELIST_NO_COMMON = "WARNING: Resource group %s has a "
	"weak positive affinity for resource group %s; but the Nodelists "
	"of these two resource groups do not have any nodes in common.";
const char *RWM_AFF_LESS_DESIRED_P = "WARNING: Resource group %s has a strong "
	"positive affinity for resource group %s whose Desired_primaries is "
	"less than the Desired_primaries of resource group %s.";
const char *RWM_RG_AFF_UNMANAGED = "WARNING: Resource group %s has a weak "
	"affinity for unmanaged resource group %s.";
const char *RWM_RG_AFF_MANAGED = "WARNING: managed resource group %s has a "
	"weak positive affinity for unmanaged resource group %s.";
const char *RWM_AFF_STRONG_MULTI_RG = "WARNING: resource group %s has a strong "
	"affinity for more than one resource group; it might be forced to "
	"remain offline if all its strong affinities cannot be satisfied.";
const char *RWM_RG_AFF_STATE = "WARNING: resource group %s declares a weak %s "
	"affinity for resource group %s; but this affinity is not satisfied "
	"by the current state of the two resource groups.";
const char *RWM_AFFS_OFFLINE = "NOTICE: Resource group(s) {%s} were either "
	"switched to different nodes or brought offline as a result of "
	"their strong resource group affinities for resource group %s.";
const char *RWM_RG_NOREBALANCE = "NOTICE: Not switching resource group %s "
	"because it is currently undergoing a reconfiguration.";
const char *RWM_RG_NOREBAL_UNMAN = "NOTICE: Not switching resource group %s "
	"because it is unmanaged.";
const char *RWM_RG_NOREBAL_SUSP = "NOTICE: Not switching resource group %s "
	"because it is currently suspended.";
const char *RWM_AFF_SWITCH_DELEGATE_USED = "NOTICE: Resource group %s was also "
	"included in the request, due to a +++ affinity of resource "
	"group %s (possibly transitively).";
const char *RWM_MON_RESTART_DEF = "WARNING: Cannot enable monitoring on "
	"resource %s because it already has monitoring enabled. To force the "
	"monitor to restart, disable monitoring using 'clresource unmonitor %s'"
	" and re-enable monitoring using 'clresource monitor %s'.";
const char *RWM_INVALID_RS = "WARNING: Invalid resource: %s. Resource ignored";
const char *RWM_QUIESCE_STOP_FAILED = "WARNING: Resource group %s is in "
	"ERROR_STOP_FAILED state on one or more node(s). After manually "
	"killing the processes of any resources in the group that are in "
	"STOP_FAILED state, use 'clresource clear -g <resourcegroupname> +' "
	"to clear the error state.";
const char *RWM_SYSTEM_RG_SUSPEND = "WARNING: You have suspended or resumed "
	"automatic recovery actions on a system resource group <%s>.";
const char *RWM_SYSTEM_RG_QUIESCE = "WARNING: system resource group <%s> being"
	" quiesced.";
const char *RWM_EVAC_SUSPENDED_RG = "Warning: resource groups in suspended"
	" state might be left offline after node evacuation.";
const char *RWM_NRU_ADDED_TO_RDEP = "NOTICE: Resource %s from the Network_"
	"resources_used property was added to the Resource_dependencies "
	"property.";
const char *RWM_NRU_OVERRIDE_DEP = "WARNING: non-empty user specified value "
	"of Network_resources_used will override the default value derived "
	"from resource dependencies.";
const char *RWM_UNCONFIGURED_ZONES = "WARNING: One or more zones in the node "
	"list have never been fully booted in cluster mode. "
	"Verify that the correct zone name was entered.";
const char *RWM_RG_UNMANAGED_TO_OFFLINE = "resource group %s state changed "
	"from unmanaged state to managed offline state.";
const char *RWM_RG_OFFLINE_TO_UNMANAGED = "resource group %s state changed "
	"from offline state to unmanaged state.";
const char *RWM_SKIP_RG_EVAC_THREAD = "Skipping resource group %s because "
	"it is already being evacuated from the specified nodes.";
const char *RWM_SKIP_RG = "Skipping resource group %s because none of "
	"the specified nodes are in its nodelist.";
const char *RWM_RS_DEP_ONLINE = "NOTICE: Resource %s, which remains online "
	"had an implicit dependency on resource %s which is now "
	"deleted.";
const char *RWM_DISABLE_DEP_ONLINE = "WARNING: Resource %s being disabled "
	"while its dependent resources {%s} remain online.";
const char *RWM_DUPLICATE_R_OPERAND = "NOTICE: Ignoring duplicate resource "
	"name %s.";
const char *RWM_DUPLICATE_RG_OPERAND = "NOTICE: Ignoring duplicate resource "
	"group name %s.";
// Success messages
const char *RWM_RG_STOPPED = "resource group %s stopped successfully";
const char *RWM_RG_REBALANCED = "resource group %s rebalanced successfully";
const char *RWM_R_ENABLED = "resource %s marked as enabled";
const char *RWM_R_DISABLED = "resource %s marked as disabled";
const char *RWM_R_MON_ENABLED = "resource %s monitor marked as enabled";
const char *RWM_R_MON_DISABLED = "resource %s monitor marked as disabled";
const char *RWM_RG_RESTARTED = "resource group %s restarted successfully";
const char *RWM_R_DELETED = "resource %s deleted";
const char *RWM_RG_DELETED = "resource group %s deleted";
const char *RWM_QUIESCE_RG = "resource group %s quiesced";
const char *RWM_RG_OFFLINED = "resource group %s was successfully brought "
	"offline";
const char *RWM_RG_ONLINED = "resource group %s was successfully brought "
	"online";
const char *RWM_RG_REMASTERED = "resource group %s was successfully "
	"remastered on %s";
const char *RWM_R_NOT_IN_STOP_FAIL = "Skipping resource %s because it is "
	"not in STOP_FAILED state on any of the specified nodes.";
const char *RWM_RG_NGZONE = "Warning: Resource groups whose nodelist doesn't "
	"contain this zone will be ignored.";
