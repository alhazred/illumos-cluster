/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _SCCONF_PRIVATE_H
#define	_SCCONF_PRIVATE_H

#pragma ident	"@(#)scconf_private.h	1.41	09/01/16 SMI"

/*
 * This is the private header file for libscconf.   Users of libscconf
 * should not include this file.
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <scadmin/scconf.h>
#include <scadmin/scstat.h>
#include <scxcfg/scxcfg.h>
#include <rgm/rgm_scrgadm.h>
#include <rgm/pnm.h>

#include <sys/cladm_int.h>
#include <sys/dditypes.h>
#include <sys/didio.h>
#include <sys/clconf_int.h>
#include <sys/clconf_property.h>
#include <sys/vc_int.h>

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <thread.h>
#include <synch.h>
#include <libintl.h>
#include <dirent.h>
#include <values.h>
#include <dlfcn.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/utsname.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <libdid.h>

#include <fmm/saf_clm.h>
#include <fmm/saf_types.h>

#define	SCCONF_MSG_BUFSIZ	1024
#define	SCCONF_HB_BUFSIZ	128

/* Cluster properties */
#define	PROP_CLUSTER_ID			"cluster_id"
#define	PROP_CLUSTER_NETADDR		"private_net_number"
#define	PROP_CLUSTER_NETMASK		"private_netmask"
#define	PROP_CLUSTER_SUBNETMASK		"private_subnet_netmask"
#define	PROP_CLUSTER_MAXNODES		"private_maxnodes"
#define	PROP_CLUSTER_MAXPRIVATENETS	"private_maxprivnets"
#define	PROP_CLUSTER_ZONECLUSTERS	"zoneclusters"
#define	PROP_CLUSTER_USER_NET_NUM	"private_user_net_number"
#define	PROP_CLUSTER_USER_NETMASK	"private_user_netmask"
#define	PROP_CLUSTER_INSTALLMODE	"installmode"
#define	PROP_CLUSTER_AUTHTYPE		"auth_joinlist_type"
#define	PROP_CLUSTER_AUTHJOINLIST	"auth_joinlist_hostslist"
#define	PROP_CLUSTER_NODEVOTECHANGE	"quorum_max_nodevote_change"
#define	PROP_CLUSTER_HB_TIMEOUT		"transport_heartbeat_timeout"
#define	PROP_CLUSTER_HB_QUANTUM		"transport_heartbeat_quantum"
#define	PROP_CLUSTER_UDP_TIMEOUT	"udp_session_timeout"
#define	PROP_CLUSTER_CLUSTERNETMASK	"cluster_netmask"

/* Node properties */
#define	PROP_NODE_QUORUM_VOTE		"quorum_vote"
#define	PROP_NODE_QUORUM_RESV_KEY	"quorum_resv_key"
#define	PROP_NODE_QUORUM_DEFAULTVOTE	"quorum_defaultvote"
#define	PROP_NODE_PRIVATE_HOSTNAME	"private_hostname"
/* SC SLM addon start */
#define	PROP_NODE_SCSLM_GLOBAL_SHARES	"global_zone_shares"
#define	PROP_NODE_SCSLM_DEFAULT_PSET	"default_pset_min"
/* SC SLM addon end */

/* Adapter properties */
#define	PROP_ADAPTER_DEVICE_NAME	"device_name"
#define	PROP_ADAPTER_DEVICE_INSTANCE	"device_instance"
#define	PROP_ADAPTER_TRANSPORT_TYPE	"transport_type"
#define	PROP_ADAPTER_HEARTBEAT_TIMEOUT	"heartbeat_timeout"
#define	PROP_ADAPTER_HEARTBEAT_QUANTUM	"heartbeat_quantum"
#define	PROP_ADAPTER_VLAN_ID		"vlan_id"

/* Cpoint properties */
#define	PROP_CPOINT_TYPE		"type"

/* Quorum device properties */
#define	PROP_QDEV_QUORUM_VOTE		"votecount"
#define	PROP_QDEV_GDEVNAME		"gdevname"
#define	PROP_QDEV_ACCESSMODE		"access_mode"
#define	PROP_QDEV_TYPE			"type"

#ifdef WEAK_MEMBERSHIP
/* WEAK MEMBERSHIP Properties */
#define	PROP_CLUSTER_PING_TARGETS	"ping_targets"
#define	PROP_CLUSTER_MULTI_PARTITIONS	"multiple_partitions"
#endif // WEAK_MEMBERSHIP

#define	SCCONF_DID_RDSK		"/dev/did/rdsk/"
#define	SCCONF_GLOBAL_RDSK	"/dev/global/rdsk/"

/* quorum device list with connectivity information */
typedef struct conf_conn_qdev {
	char		*conf_conn_qdevname;			/* qdevname */
	boolean_t	conf_conn_ports[SCCONF_NODEID_MAX];	/* ports */
	struct conf_conn_qdev	*conf_conn_qdevnext;		/* next */
} conf_conn_qdev_t;

/* Syslog tag */
#define	SC_SYSLOG_HEARTBEAT_TAG		"Cluster.heartbeat"

/*
 * Return the number of nodes in "numnodesp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_numnodes(clconf_cluster_t *clconf,
    int *numnodesp);

/*
 * Return the "nodeid" in "nodeidp" for the given "nodename"
 *
 * If the "nodename" is numeric, it is assumed to actually be a nodeid.
 * And, if the ID is found in the configuration, it is returned in
 * "nodeidp" upon success.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the nodename (or, id) is not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_nodeid(clconf_cluster_t *clconf,
    char *nodename, scconf_nodeid_t *nodeidp);

/*
 * Return the "nodename" in "nodenamep" for the given "nodeid".
 *
 * If successful, the caller is responsible for freeing memory
 * returned in "nodenamep".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_nodename(clconf_cluster_t *clconf,
    scconf_nodeid_t nodeid, char **nodenamep);

/*
 * Return the object pointer (clconf_obj_t *) for the given
 * "nodeid" in "nodep", using the given "clconf".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_nodeobj(clconf_cluster_t *clconf,
    scconf_nodeid_t nodeid, clconf_obj_t **nodep);

/*
 * Return the object pointer (clconf_obj_t *) for the given
 * "privatehostname" in "nodep", using the given "clconf".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the privatehostname is not found
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_lookup_privatehostname(clconf_cluster_t *clconf,
    char *privatehostname, clconf_obj_t **nodep);

/*
 * Upon success, a disk reservation key is formed from the given
 * cluster and node ID's.  The key is copied into the supplied "key"
 * buffer.
 *
 * The "key" buffer must be large enough to hold a disk reservation string,
 * (SCCONF_RESKEY_BUFSIZ).
 *
 * The reservation key has a format of "0xFFFFFFFFFFFFFFFF".
 * The first 4 "bytes" are formed from the clusterid;
 * the  last 4 "bytes" are formed from the nodeid.
 *
 * The key is formed from the concatenation of the two
 * 32 bit integers stored as an ASCII string of hex digits.
 * The string is introduced by "0x" to indicate to the
 * consumer that a conversion to binary data (8 bytes)
 * is required.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 */
extern scconf_errno_t conf_form_reskey(int clusterid, scconf_nodeid_t nodeid,
    char *key);

/*
 * Get the propery list associated with the given object.
 *
 * The caller is responsible for freeing memory
 * associated with "propslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_proplist(clconf_obj_t *clobj, char *exclude_props[],
    scconf_cfg_prop_t **propslistp);

/*
 * Free all memory associated with the given property "list".
 */
void conf_free_proplist(scconf_cfg_prop_t *list);

/*
 * Set the state of a clconf object.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_set_state(clconf_obj_t *object,
    scconf_state_t state);

/*
 * Upon success, return the cluster ID in "clusteridp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- cluster ID does not exist
 *	SCCONF_EBADVALUE	- bad cluster ID
 *	SCCONF_EINVAL		- invalid argument
 */
extern scconf_errno_t conf_get_clusterid(clconf_cluster_t *clconf,
    int *clusteridp);

/*
 * Upon success, update the given "clconf" with a new cluster ID.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_set_clusterid(clconf_cluster_t *clconf);

/*
 * conf_get_maxprivnets
 *
 * Upon success, return the maxprivnets in "maxp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- maxprivnet value does not exist
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
conf_get_maxprivnets(clconf_cluster_t *clconf, int *maxp);

/*
 * conf_get_maxnodes
 *
 * Upon success, return the maxnodes in "maxp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- maxprivnet value does not exist
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
conf_get_maxnodes(clconf_cluster_t *clconf, int *maxp);

/*
 * conf_check_notinuse_bycable
 *
 * Check to see if the given node, adapter, cpoint, or port object ("clobj")
 * is configured as part of a cable definition.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINUSE		- the object is in use
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_check_notinuse_bycable(
    clconf_cluster_t *clconf, clconf_obj_t *clobj, uint_t statechk);

/*
 * conf_nodecount
 *
 * Return the number of nodes configured in the given "clconf".
 *
 * Possible return values:
 *
 *	-1			- error
 *	> 0			- number of configured nodes
 */
extern int conf_nodecount(clconf_cluster_t *clconf);

/*
 * conf_adaptercount
 *
 * Return the number of adapters configured in the given node.
 *
 * Possible return values:
 *
 *	-1			- error
 *	> 0			- number of configured nodes
 */
extern int conf_adaptercount(clconf_node_t *node);

/*
 * conf_qdevcount
 *
 * Return the number of quorum devices configured in the given "clconf".
 *
 * Possible return values:
 *
 *	-1			- error
 *	>= 0			- number of configured quorum devices
 */
extern int conf_qdevcount(clconf_cluster_t *clconf);

/*
 * conf_qdev_enabled_count
 *
 * Return the number of enabled quorum devices configured in the given
 * "clconf".
 *
 * Possible return values:
 *
 *	-1			- error
 *	>= 0			- number of configured quorum devices
 */
extern int conf_qdev_enabled_count(clconf_cluster_t *clconf);

/*
 * conf_rm_quorum_device
 *
 * Remove a quorum device. Currently this function is common to both
 * scsi and NAS quorum. If there will be some device type specific
 * tasks in the future, this function can be updated and put into
 * the quorum library separately.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
extern scconf_errno_t conf_rm_quorum_device(char *quorumdevice,
	uint32_t force_flg);

/*
 * conf_get_qdevobj
 *
 * Find the quorum object from ccr.
 *
 * Return the object pointer (clconf_obj_t *) for the given
 * share quorum device "name" in "qdevp", using the given "clconf".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the diddisknum is not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_qdevobj(clconf_cluster_t *clconf,
    char *qdevname, clconf_obj_t **qdevp);

/*
 * conf_setstate_quorum_device
 *
 * Set the state of a shared quourm device to either enabled
 * or disabled, as indicated by "state".  This doesn't mean anything
 * to the kernel quorum algorithm.  But, we use it to indicate whether or
 * not the device is in maint state (disabled in maint state).
 *
 * conf_sync_quorum_device() is then called;   it checks the state and
 * adjusts the vote count accordingly (in a stepped fashion).
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
extern scconf_errno_t conf_setstate_quorum_device(char *quorumdevice,
    scconf_state_t state, uint_t force_flg);

/*
 * conf_obj_check
 *
 * Call clconf_obj_check() on the object, and return an scconf_errno_t.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ESETUP		- setup attempt failed
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_obj_check(clconf_obj_t *obj, char **messages);

/*
 * conf_check_isolation
 *
 * Compare the new clconf to the existing clconf.   And, if the new
 * configuration isolates nodes which were not previously isolated,
 * check to see if any such node is an active cluster member.  If active,
 * return SCCONF_EBUSY.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EBUSY		- attempt to isolate busy node
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_check_isolation(clconf_cluster_t *existing_clconf,
    clconf_cluster_t *new_clconf);

/*
 * conf_check_transition
 *
 * Call clconf_cluster_check_transition(), then return scconf_errno_t.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ESETUP		- setup attempt failed
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_check_transition(clconf_cluster_t *existing_clconf,
    clconf_cluster_t *new_clconf, char **messages);

/*
 * conf_cluster_commit
 *
 * Call clconf_cluster_commit(), then return scconf_errno_t.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ESTALE		- stale clconf handle
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ESETUP		- setup attempt failed
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_cluster_commit(clconf_cluster_t *clconf,
    char **messages);

/*
 * Get the list of all properties for a given "clplobj_type" (e.g.,
 * CL_ADAPTER or CL_BLACKBOX) and "clplobj_name" (e.g., "hme" or "switch").
 *
 * Upon success, a pointer to the property list is returned in
 * "propdeslisp".  If there is no property description list, the value
 * pointed to by "propdeslistp" is set to NULL.
 *
 * The caller is responsible for freeing memory associated
 * with the "propdeslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root or permission denied
 *	SCCONF_EINVAL		- invalid argument (or item(s) in clconfig)
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t
conf_get_propdeslist(clconf_objtype_t clplobj_type, const char *clplobj_name,
    scconf_cltr_propdes_t **propdeslistp);

/*
 * Check the given property "name" and "value" against
 * the given propery description list ("propdeslist").
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- bad propterty
 */
extern scconf_errno_t conf_check_property(char *name, char *value,
    scconf_cltr_propdes_t *propdeslist);

/*
 * Lookup the type of admin security currently in use with the given
 * "clconf".  Upon success, the authentication type is set in the location
 * pointed to by "authtypep".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- unexpected or internal error
 */
extern scconf_errno_t conf_get_secure_authtype(clconf_cluster_t *clconf,
    scconf_authtype_t *authtypep);

/*
 * From the given "clconf", get the list of nodes which have permission
 * to add themselves to the cluster.   Upon success, a pointer to the
 * list of names is placed in the location pointed to by "joinlistp".
 * If the list is empty, the pointer is set to NULL.
 *
 * The caller is responsible for freeing memory associated
 * with the "joinlistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
extern scconf_errno_t conf_get_secure_joinlist(clconf_obj_t *clconf,
    scconf_namelist_t **joinlistp);

/*
 * conf_split_adaptername
 *
 * The given adaptername is checked for correct format, then split into
 * its two parts, device_name and device_instance.  The "device_name"
 * and "device_instance" buffers must be large enough to hold the split
 * "adaptername" components.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument or bad adaptername format
 */
extern scconf_errno_t conf_split_adaptername(char *adaptername,
    char *device_name, char *device_instance);

/*
 * conf_cat_newmsg
 *
 * Concatenate the contents of the "msgbuff" to the location pointed to
 * by "messages".
 *
 * Each message in "messages" is always terminated by at least one newline.
 * And, a NULL terminates the end of the buffer.
 *
 * It is the callers responsibility to free memory allocated for "messages".
 */
extern void conf_cat_newmsg(char *msgbuff, char **messages);

/*
 * Get a snapshot of the current cluster config, using the given "clconf".
 *
 * The caller is responsible for freeing memory associated
 * with the "clconfigp".
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_ENOMEM           - not enough memory
 *      SCCONF_EINVAL           - invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_clusterconfig(clconf_cluster_t *clconf,
    scconf_cfg_cluster_t **clconfigp);

/*
 * Get the nodes list from the given "clconf".
 *
 * The caller is responsible for freeing memory
 * associated with "nodeslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_clusternodes(clconf_cluster_t *clconf,
    scconf_cfg_node_t **nodeslistp);

/*
 * conf_free_clusternodes
 *
 * Free all memory associated with an "scconf_cfg_node" structure.
 */
extern void conf_free_clusternodes(scconf_cfg_node_t *nodeslist);

/*
 * Get the cluster transport adapters list for the given "node".
 *
 * The caller is responsible for freeing memory
 * associated with "adapslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_cltr_adapters(clconf_node_t *node,
    scconf_cfg_cltr_adap_t **adapslistp);

/*
 * Free all memory associated with an "scconf_cfg_cltr_adap" structure.
 */
extern void conf_free_cltr_adapters(scconf_cfg_cltr_adap_t *adapslist);

/*
 * Get the cpoints list for the cluster.
 *
 * The caller is responsible for freeing memory
 * associated with "cpointslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_cltr_cpoints(clconf_cluster_t *clconf,
    scconf_cfg_cpoint_t **cpointslistp);

/*
 * Free all memory associated with an "scconf_cfg_cpoint" structure.
 */
extern void conf_free_cltr_cpoints(scconf_cfg_cpoint_t *cpointslist);

/*
 * Get the list associated with the given iterator.
 *
 * The caller is responsible for freeing memory
 * associated with "portslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_cltr_ports(clconf_iter_t *portsi,
    scconf_cfg_port_t **portslistp);

/*
 * Add the "port_obj" information to the given "scconf_cltr_port" structure.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_cltr_port(clconf_obj_t *port_obj,
    scconf_cfg_port_t *port_cfg);

/*
 * Free all memory associated with an "scconf_cfg_port" structure.
 */
extern void conf_free_cltr_ports(scconf_cfg_port_t *portslist);

/*
 * Get the list of cables in the cluster.
 *
 * The caller is responsible for freeing memory
 * associated with "cableslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_cltr_cables(clconf_cluster_t *clconf,
    scconf_cfg_cable_t **cableslistp);

/*
 * Free all memory associated with an "scconf_cfg_cable" structure.
 */
extern void conf_free_cltr_cables(scconf_cfg_cable_t *cableslist);

/*
 * Free all memory associated with the elements of an scconf_cltr_epoint
 * structure, without freeing the scconf_cltr_epoint struct itself.
 */
extern void conf_free_cltr_epoint_elements(scconf_cltr_epoint_t *epoint);

/*
 * Get the list of shared quorum devices in the cluster.
 *
 * The caller is responsible for freeing memory
 * associated with "qdevslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
extern scconf_errno_t conf_get_qdevs(clconf_cluster_t *clconf,
    scconf_cfg_qdev_t **qdevslistp);

/*
 * Free all memory associated with an "scconf_cfg_qdev" structure.
 */
extern void conf_free_qdevs(scconf_cfg_qdev_t *qdevslist);

/*
 * conf_use_adapter_default_properties()
 *
 * Walks through the "propdeslist" list, and for every property that is
 * is not required, but for which a default is supplied, inserts the default
 * value into the property list "proplist".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid arguments
 */
extern scconf_errno_t conf_use_adapter_default_properties(
    scconf_cltr_propdes_t *propdeslist, scconf_cfg_prop_t **proplist);

/*
 * scconf_get_legal_hb
 *
 * When the "set" flag is FALSE, it just checks the input "timeout"
 * and "quantum" against the adapter properties description (in
 * *propdeslist_p) that is obtained from the adapter's clpl file
 * and see if they are legal, i.e., within the range.
 *
 * If the "set" flag is TRUE, it then picks the default "timeout"
 * and "quantum" settings from the adapter properties description.
 *
 * Possible return values:
 *
 *      SCCONF_EINVAL		Invalid parameters
 *      SCCONF_ERANGE		Out of range
 *	SCCONF_EUNEXPECTED	Internal or unexpected error
 *      SCCONF_NOERR		No error
 */
extern scconf_errno_t
scconf_get_legal_hb(char *transport_type,
    scconf_cltr_propdes_t *propdeslist_p, char **timeout,
    boolean_t set_timeout, char **quantum, boolean_t set_quantum);

/*
 * scconf_hb_to_properties
 *
 * Take the global heart beat settings into the "properties" format.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_ENOMEM           - not enough memory
 *      SCCONF_EINVAL           - invalid argument
 *      SCCONF_EUNEXPECTED      - internal or unexpected error
 */
extern scconf_errno_t scconf_hb_to_properties(
    char *timeout, char *quantum, char *properties, char *transport_type);

/*
 * scconf_properties_to_hb
 *
 * Get the heart beat values from the "properties". Upon finished,
 * the heart beats could have the following values and what it means
 * to the caller:
 *
 *	"default"	Needs to get the default global heart beat.
 *	'\0'		No input from the user.
 *	Some value specified by user
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM           - not enough memory
 *	SCCONF_EINVAL           - invalid argument
 *	SCCONF_EUNEXPECTED      - internal or unexpected error
 */
extern scconf_errno_t scconf_properties_to_hb(
    char **timeout, char **quantum, char *properties, char *transport_type);

/*
 * scconf_get_default_status_string
 *
 * Get status string from status code. The supplied "text" should
 * be of at least SCSTAT_MAX_STRING_LEN.
 */
extern void scconf_get_default_status_string(
    const scstat_state_code_t state_code, char *text);

/*
 * scconf_str_to_nodeid
 *
 * Convert string (nodeid) to scconf_nodeid_t (nodeid).
 *
 * Returns B_TRUE, if it is a nodeid.
 * Returns B_FALSE otherwise.
 */
extern boolean_t scconf_str_to_nodeid(char *name,
    scconf_nodeid_t *scconf_nodeid);

#ifdef __cplusplus
}
#endif

#ifdef _KERNEL_ORB
#include <orbtest/trans_tests/unode_trtests.h>
#endif

#endif /* _SCCONF_PRIVATE_H */
