/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scconf_quorum.c	1.68	09/04/20 SMI"

/*
 * libscconf cluster quorum config functions
 *
 * The functions in this file are common to all types of quorum
 * device. There are some functions that are specific to the
 * the type of the device defined in lib/scqd/scqd_common.h,
 * including,
 *
 *	scqd_add_quorum		Add a quorum device
 *	scqd_remove_quorum	Remove a quorum device
 *	scqd_set_qd_properties	Change quorum properties
 *
 */

#include "scconf_private.h"
#include <unistd.h>
#include <regex.h>
#include <dlfcn.h>
#include <dirent.h>

#ifdef WEAK_MEMBERSHIP
#include "wait.h"
#include "scadmin/scconf.h"
#endif


#define	SCCONF_QD_LIB_DIR	"/usr/cluster/lib/qd"
#define	SCCONF_GET_QD_TYPE	"scqd_get_qd_type"
#define	SCCONF_GET_QD_DEVICE	"scqd_get_qd_device"
#define	SCCONF_ADD_QD_DEVICE	"scqd_add_quorum"
#define	SCCONF_RM_QD_DEVICE	"scqd_remove_quorum"
#define	SCCONF_SET_QD_PROP	"scqd_set_qd_properties"

#define	QDLIB_NO_HANDLE		0
#define	QDLIB_HANDLE		1

#ifdef WEAK_MEMBERSHIP
#define	TRUE			"true"
#define	FALSE			"false"
#define	SCRCMD			"/usr/cluster/lib/sc/scrcmd"
#define	PING_CMD		"/usr/sbin/ping"
#endif // WEAK_MEMBERSHIP

typedef scconf_errno_t (*scqd_add_quorum_t)(char *, char **,
    char *, char **);
typedef scconf_errno_t (*scqd_rm_quorum_t)(char *, uint32_t force_flg, char **);
typedef scconf_errno_t (*scqd_set_qd_prop_t)(char *, char *, char **);

static scconf_errno_t conf_sync_quorum_device(char *quorumdevice);
static scconf_errno_t conf_setcount_quorum_node(char *nodename, int targetvote);
static scconf_errno_t conf_set_installmode_state(scconf_state_t state);
static scconf_errno_t conf_check_vote_change(clconf_cluster_t *clconf,
    scconf_nodeid_t, uint_t voteval, uint_t old_vote);
static scconf_errno_t conf_add_optimal_qdev(conf_conn_qdev_t **, char *,
    boolean_t *, scconf_nodeid_t);
static scconf_errno_t conf_ins_optimal_qdev(conf_conn_qdev_t **, char *,
    boolean_t *);
static scconf_errno_t conf_ins_name_qdev(scconf_name_qdev_t **, char *);
static void conf_del_optimal_qdev(conf_conn_qdev_t **, conf_conn_qdev_t *,
    conf_conn_qdev_t *);
static void conf_cmp_conn(boolean_t *, conf_conn_qdev_t *, scconf_nodeid_t,
    int *);
static void conf_free_conn_qdevlist(conf_conn_qdev_t *);

static void *open_qd_lib(char *scconf_name, char *qd_name,
    char *func_name, uint_t c_flg);

#ifdef WEAK_MEMBERSHIP
static scconf_errno_t conf_addto_ping_targets(char *target);

static scconf_errno_t conf_rmfrom_ping_targets(char *target);

static scconf_errno_t conf_clear_ping_targets(boolean_t force);

static scconf_errno_t
scconf_change_multiple_partitions(char *value, char **messages);

static scconf_errno_t
execve_command(char *cli_cmd, char **cli_args, char **cli_envs);
#endif // WEAK_MEMBERSHIP

/*
 * scconf_add_quorum_device
 *
 * For the given "devicetype", find the address of function
 * scqd_add_quorum() and call it to add a quorum device, if it is
 * not already configured as such.  Then, for each node in the
 * the "nodeslist", add a port from the node to the device.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- quorum device is already configured
 *	SCCONF_ENOEXIST		- globaldev not found or not ported to node(s)
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
scconf_errno_t
scconf_add_quorum_device(char *devicename, char *nodeslist[],
    char *devicetype, char *properties, char **messages)
{
	void *dl_handle = NULL;
	scconf_errno_t scconferr = SCCONF_NOERR;
	scqd_add_quorum_t real_add_qd_func;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (devicename == NULL || devicetype == NULL)
		return (SCCONF_EINVAL);

	/* Get the library for this device type */
	dl_handle = open_qd_lib(devicetype, NULL,
	    SCCONF_GET_QD_TYPE, QDLIB_HANDLE);
	if (dl_handle == (void *)0)
		return (SCCONF_EUNKNOWN);

	/* Get the address of the function */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if ((real_add_qd_func = (scqd_add_quorum_t)dlsym(dl_handle,
	    SCCONF_ADD_QD_DEVICE)) !=  NULL) {
		scconferr = (*real_add_qd_func)(devicename,
		    nodeslist, properties, messages);
	} else {
		scconferr = SCCONF_EUNEXPECTED;
	}

	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}

	return (scconferr);
}

/*
 * scconf_rm_quorum_device
 *
 * Remove a quorum device.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
scconf_errno_t
scconf_rm_quorum_device(char *quorumdevice, uint32_t force_flg, char **messages)
{
	void *dl_handle = NULL;
	scconf_errno_t scconferr = SCCONF_NOERR;
	scqd_rm_quorum_t real_rm_qd_func;
	char qdevname[MAXPATHLEN];

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (quorumdevice == NULL)
		return (SCCONF_EINVAL);

	/* Find the library for this quorum device */
	dl_handle = open_qd_lib(quorumdevice, qdevname,
	    SCCONF_GET_QD_DEVICE, QDLIB_HANDLE);

	if ((dl_handle == (void *)0) ||
	    (qdevname == NULL || *qdevname == '\0')) {
		return (SCCONF_ENOEXIST);
	}

	/* Get the address of the function in the library */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if ((real_rm_qd_func = (scqd_rm_quorum_t)dlsym(dl_handle,
	    SCCONF_RM_QD_DEVICE)) !=  NULL) {
		scconferr = (*real_rm_qd_func)(qdevname, force_flg, messages);
	} else {
		scconferr = SCCONF_EUNEXPECTED;
	}

	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}

	return (scconferr);
}

/*
 * scconf_maintstate_quorum_device
 *
 * Put the given "quorumdevice" into maintenance mode.
 *
 * In this mode, the vote count is set to zero, and the state
 * of the device is set to "disabled".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
scconf_errno_t
scconf_maintstate_quorum_device(char *quorumdevice)
{
	scconf_errno_t rstatus;

	rstatus = conf_setstate_quorum_device(quorumdevice,
	    SCCONF_STATE_DISABLED, 0);

	return (rstatus);
}

/*
 * scconf_reset_quorum_device
 *
 * Reset the quorum state of the given "quorumdevice".
 *
 * Set the vote count to "N-1", where "N" is the number of "enabled"
 * ports to the device.  And, the state of the device is set to
 * "enabled".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
scconf_errno_t
scconf_reset_quorum_device(char *quorumdevice)
{
	scconf_errno_t rstatus;

	rstatus = conf_setstate_quorum_device(quorumdevice,
	    SCCONF_STATE_ENABLED, 0);

	return (rstatus);
}

/*
 * scconf_maintstate_quorum_node
 *
 * Put the given "nodename" into quorum maintenance state.
 *
 * In this state, the vote count is set to zero, and all shared quorum
 * devices with enabled ports to this node decremented by 1.
 *
 * It is not legal to put a node into maint state as long as it is
 * active in the cluster;  SCCONF_EBUSY is return.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- node not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 *	SCCONF_EBUSY		- the node is active in the cluster
 *	SCCONF_EINUSE		- opposing command is in progress
 */
scconf_errno_t
scconf_maintstate_quorum_node(char *nodename)
{
	scconf_errno_t rstatus;
	scconf_nodeid_t nodeid;
	uint_t ismember;

	/* Check arguments */
	if (nodename == NULL)
		return (SCCONF_EINVAL);

	/* Get the nodeid */
	rstatus = scconf_get_nodeid(nodename, &nodeid);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);

	/* Make sure that the node is not a member of the cluster */
	rstatus = scconf_ismember(nodeid, &ismember);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);
	if (ismember)
		return (SCCONF_EBUSY);

	/* Set the vote count to zero, and update shared quorum devices */
	return (conf_setcount_quorum_node(nodename, 0));
}

/*
 * scconf_reset_quorum_node
 *
 * Reset the quorum state of the given "nodename".
 *
 * In this state, the vote count is set to the default_votecount, and
 * all shared quorum devices with ports to this node are enabled.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- node not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 *	SCCONF_EINUSE		- opposing command is in progress
 */
scconf_errno_t
scconf_reset_quorum_node(char *nodename)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_cluster_t *clconf;
	clconf_obj_t *node;
	scconf_nodeid_t nodeid;
	const char *propval;
	uint_t defaultvote;

	/* Allocate the cluster config. */
	rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}
	clconf = clconf_cluster_get_current();

	/* Get the nodeid */
	rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Find the node */
	rstatus = conf_get_nodeobj(clconf, nodeid, &node);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Get the current vote count */
	propval = clconf_obj_get_property(node, PROP_NODE_QUORUM_DEFAULTVOTE);
	if (propval == NULL) {
		defaultvote = 1;
	} else {
		defaultvote = (uint_t)atoi(propval);
	}

	rstatus = conf_setcount_quorum_node(nodename, (int)defaultvote);

cleanup:

	/* Release all clconf objects allocated */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_set_node_defaultvote
 *
 * Sets the defaultvote property for the given node. This function
 * takes a vote value (voteval) and first sets the defaultvote
 * property in the ccr. It then calls conf_setcount_quorum_node with
 * the voteval to cause the system to increment the node's actual
 * quorum votes until it reaches the value.
 *
 * If the voteval passed in is one, which is the default for the
 * system, then instead of adding the value to the CCR, it removes
 * any previous values. So, there will only be a value set in the CCR
 * if the value is not equal to one.
 *
 * Possible return vaules:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- node not found
 *	SCCONF_EINVAL		- vote value out of range
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 *	SCCONF_EINUSE		- opposing command is in progress
 */
scconf_errno_t
scconf_set_node_defaultvote(char *nodename, uint_t voteval)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_errnum_t clconf_err;
	int old_vote = 0;
	int max_change = 0;
	uint_t max_value;
	clconf_obj_t *node;
	scconf_nodeid_t nodeid;
	const char *propval;
	char svotecount[BUFSIZ];
	char *endp;

	/*
	 * This is the max_value for any given node. We set this to prevent
	 * the total votes of the cluster from overflowing an int.
	 * See bug 4617509 for a description of the formula used.
	 */
	max_value = (uint_t)(MAXINT / (2 * NODEID_MAX)); /*lint !e504 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Get the nodeid */
		rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Find the node */
		rstatus = conf_get_nodeobj(clconf, nodeid, &node);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Get the current vote count */
		propval = clconf_obj_get_property(node, PROP_NODE_QUORUM_VOTE);
		if (propval == NULL) {
			old_vote = 0;
		} else {
			old_vote = atoi(propval);
		}

		/* Check for vote change by too much */
		propval = NULL;
		propval = clconf_obj_get_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_NODEVOTECHANGE);
		if (propval == NULL) {
			max_change = 16;
		} else {
			max_change = (int)strtol(propval, &endp, 10);
			if (*endp != NULL) {
				max_change = 16;
			}
		}

		if (abs((int)voteval - old_vote) > max_change) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/* Check for vote out of bounds */
		if (voteval >= max_value) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/*
		 * Check if the total change might cause a quorum error.
		 * This will check the vote change for the node and how that
		 * will change the votes of the QDs.
		 */
		rstatus = conf_check_vote_change(clconf, nodeid, voteval,
		    (uint_t)old_vote);
		if (rstatus != SCCONF_NOERR) {
			if (rstatus == SCCONF_ERANGE) {
				/*
				 * A large change or a multiple change is
				 * acceptable, so just ignore it.
				 */
				rstatus = SCCONF_NOERR;
			} else if (rstatus == SCCONF_ESTALE) {
				/*
				 * The conf_check_vote_change call makes a call
				 * to check the transition. If that call used
				 * a stale clconfig, then when this loop later
				 * calls to check the transition, it will also
				 * have a stale config, so we'll let the error
				 * checking catch it there.
				 */
				rstatus = SCCONF_NOERR;
			} else {
				/*
				 * Any other error means that this is an
				 * unacceptable change. Goto cleanup which
				 * will return the error returned from
				 * conf_check_vote_change.
				 */
				goto cleanup;
			}
		}

		/* Pass NULL to delete from CCR if 1, else pass the value. */
		clconf_err = CL_NOERROR;
		if (voteval != 1) {
			(void) sprintf(svotecount, "%d", voteval);
			clconf_err = clconf_obj_set_property(node,
			    PROP_NODE_QUORUM_DEFAULTVOTE, svotecount);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}
		} else {
			clconf_err = clconf_obj_set_property(node,
			    PROP_NODE_QUORUM_DEFAULTVOTE, NULL);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}
		}

		if (clconf_err != CL_NOERROR) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returns okay, we are done. */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}


cleanup:

	/* Release all clconf objects */
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	if (rstatus != SCCONF_NOERR) {
		return (rstatus);
	}

	return (conf_setcount_quorum_node(nodename, (int)voteval));
}


/*
 * scconf_reset_quourm
 *
 * Reset all nodes, all quorum devices, and installmode.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EQUORUM		- minimum quorum requirements not met
 *	SCCONF_EPERM		- not root
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_reset_quorum(void)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_iter_t *currqdevsi, *qdevsi;
	clconf_iter_t *currnodesi, *nodesi;
	clconf_obj_t *node;
	clconf_obj_t *qdev;
	const char *name;
	int nodecount;
	int qdevcount;
#ifdef WEAK_MEMBERSHIP
	boolean_t multiple_partitions;
#endif
	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Allocate config */
	rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/*
	 * Make sure that the minimum quorum device requirements are met.
	 *
	 * That is, if there are fewer than three nodes, at least one
	 * shared quorum device, must be configured.
	 */

	/* Get the nodecount */
	nodecount = conf_nodecount(clconf);

	/* if less than one node, unexpected error */
	if (nodecount < 1) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;

	/* if exactly one node, quorum error */
	} else if (nodecount == 1) {
		rstatus = SCCONF_EQUORUM;
		goto cleanup;

	/* if two nodes, check the qdevcount */
	} else if (nodecount == 2) {
		qdevcount = conf_qdevcount(clconf);
#ifdef WEAK_MEMBERSHIP
		scconf_get_multiple_partitions(clconf,
			&multiple_partitions);

		/*
		 * There are no quorum devices
		 * configured and MULTIPLE_PARTITIONS
		 * is set to FALSE
		 */
		if (qdevcount < 1 && multiple_partitions == B_FALSE) {
#else
		/* There must be at least one quorum device */
		if (qdevcount < 1) {
#endif // WEAK_MEMBERSHIP
			rstatus = SCCONF_EQUORUM;
			goto cleanup;
		}
	}

	/*
	 * Reset quorum for each node.
	 */

	/* Iterate through the list of configured nodes */
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {

		/* get the name */
		name = clconf_obj_get_name(node);

		/* reset quorum for the node */
		(void) scconf_reset_quorum_node((char *)name);

		clconf_iter_advance(currnodesi);
	}

	/* Release the iterator */
	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	/*
	 * Reset quorum for each quorum device.
	 */

	/* Iterate through the list of shared quorum devices */
	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
	while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {

		/* Get the name of the global device */
		name = clconf_obj_get_property(qdev, PROP_QDEV_GDEVNAME);
		if (name == NULL)
			continue;

		/* reset quorum for the device */
		(void) scconf_reset_quorum_device((char *)name);

		clconf_iter_advance(currqdevsi);
	}

	/* Release the iterator */
	if (qdevsi != (clconf_iter_t *)0)
		clconf_iter_release(qdevsi);

	/* Reset installmode */
	(void) conf_set_installmode_state(SCCONF_STATE_DISABLED);

cleanup:

	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_set_quorum_installflag
 *
 * Set installmode to "enabled".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_set_quorum_installflag(void)
{
	return (conf_set_installmode_state(SCCONF_STATE_ENABLED));
}

/*
 * scconf_clear_quorum_installflag
 *
 * Set installmode to "disabled".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_clear_quorum_installflag(void)
{
	return (conf_set_installmode_state(SCCONF_STATE_DISABLED));
}

/*
 * scconf_get_quorum_installflag
 *
 * Get the state of installmode.   If installmode is enabled,
 * the flag pointed to by "flagp" is set to non-zero;   if it is disabled,
 * it is set to zero.
 *
 * If an error is ecounted, the contents of "flagp" remain unchanged.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_get_quorum_installflag(uint_t *flagp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	const char *propval;

	/* Check arguments */
	if (flagp == NULL)
		return (SCCONF_EINVAL);

	/* Allocate the cluster config */
	rstatus = scconf_cltr_openhandle(
	    (scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Get the installmode property */
	propval = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_INSTALLMODE);

	/* If the property is not set, default is "disabled" */
	if (propval == NULL)
		propval = "disabled";

	/* Set the flag */
	if (strcmp(propval, "enabled") == 0)
		*flagp = 1;
	else
		*flagp = 0;

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_get_optimal_quorum_config()
 *
 * Analyse current quorum configuration and current storage connectivity
 * setup and suggest modification to the quorum configuration to reach
 * (one of many) optimal quorum configuration.
 *
 * At present this is supported only on 2 node clusters. Instead of hard
 * coding the 2 node config, this uses a generalised algorithm. The algorithm
 * also works for fully connected and n + 1 configs with any number of
 * nodes. However, more than 2 nodes are not supported at this point
 * because the result from this algorithm can't be claimed as optimal for
 * all possible configurations. In fact, the "optimal" quorum configuration
 * is yet to be defined for many arbitrary configurations.
 *
 *     qdevs_to_add	- quorum devices to add to reach optimal config
 *     qdevs_to_del	- quorum devices to remove to reach optimal config
 *     user_op		- add or change user operation
 *     messages 	- msg buffer for detailed messages
 *
 * Possible return values:
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 */
scconf_errno_t
scconf_get_optimal_quorum_config(scconf_name_qdev_t **qdevs_to_add,
    scconf_name_qdev_t **qdevs_to_del, int user_op, char **messages)
{
	did_device_list_t *did_devlist = NULL, *cur_dev;
	char *did_path = NULL;
	char didname[MAXPATHLEN];
	char qdevname[MAXPATHLEN];
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_nodeid_t nodeid, preset_port_nodeid;
	scconf_errno_t rstatus = SCCONF_NOERR;
	boolean_t qdev_conn[SCCONF_NODEID_MAX];
	conf_conn_qdev_t *optimal_qdevlist = NULL, *curr_optimal;
	int port_count;
	conf_conn_qdev_t *preset_qdevlist = NULL;
	conf_conn_qdev_t *curr_preset_qdev, *seen_preset_qdev;
	conf_conn_qdev_t *curr_optimal_qdev, *prev_optimal_qdev;
	conf_conn_qdev_t *next_optimal_qdev, *saved_curr, *saved_prev;
	scconf_name_qdev_t *qdevs_del = NULL;
	boolean_t preset_conn[SCCONF_NODEID_MAX];
	int cmp_res;
	scconf_nodeid_t max_nodeid = 0;
	scconf_cfg_qdev_t *cfg_qdev_list = NULL, *cfg_qdev = NULL;
	uint_t preset_port_count = 0;
	scconf_cfg_qdevport_t *preset_port;
	did_subpath_t *subpath_ptr;
	char *curr_node;
	char buffer[SCCONF_MAXSTRINGLEN];
	int redundant_quorum = 0;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *nodeobj;
	const char *nodevote;
	int numnodes = 0;
	char *replaced_optimal_qdev = NULL;
	did_repl_t repl_status;
	did_repl_list_t repl_data;

	rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Get current quorum config */
	rstatus = conf_get_qdevs(clconf, &cfg_qdev_list);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Add operation works only if no quorum is configured. */
	if ((user_op == QUORUM_ADD_OP) && cfg_qdev_list) {
		if (messages != (char **)0) {
			scconf_addmessage(dgettext(TEXT_DOMAIN,
			    "One or more quorum devices already "
			    "configured.\n"), messages);
		}
		rstatus = SCCONF_EEXIST;
		goto cleanup;
	}

	/*
	 * Create the list of existing quorum devices.
	 * If we detect any non-default setting in the already existing
	 * quorum configuration, we don't want to mess with the configuration.
	 */
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((nodeobj = clconf_iter_get_current(currnodesi)) != NULL) {
		nodevote = clconf_obj_get_property(nodeobj,
		    PROP_NODE_QUORUM_VOTE);
		if (nodevote == NULL) {
			if (messages != (char **)0) {
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				    "Non-default quorum votes being used, "
				    "aborting.\n"), messages);
			}
			rstatus = SCCONF_EEXIST;
			goto cleanup;
		}
		if (atoi(nodevote) > 1) {
			if (messages != (char **)0) {
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				    "Non-default quorum votes being used, "
				    "aborting.\n"), messages);
			}
			rstatus = SCCONF_EEXIST;
			goto cleanup;
		}
		numnodes += 1;
		clconf_iter_advance(currnodesi);
	}

	if (numnodes > 2) {
		scconf_addmessage(dgettext(TEXT_DOMAIN,
		    "Automatic configuration for optimal quorum devices "
		    "is not supported on clusters with more that 2 nodes.\n"),
		    messages);
		rstatus = SCCONF_EQD_CONFIG;
		goto cleanup;
	}

	for (cfg_qdev = cfg_qdev_list; cfg_qdev; cfg_qdev =
	    cfg_qdev->scconf_qdev_next) {

		preset_port_count = 0;
		(void) memset(preset_conn, B_FALSE,
		    sizeof (boolean_t) * SCCONF_NODEID_MAX);

		for (preset_port = cfg_qdev->scconf_qdev_ports; preset_port;
		    preset_port = preset_port->scconf_qdevport_next) {
			if (preset_port->scconf_qdevport_nodename == NULL) {
				continue;
			}
			rstatus = conf_get_nodeid(clconf,
			    preset_port->scconf_qdevport_nodename,
			    &preset_port_nodeid);
			if (rstatus != SCCONF_NOERR) {
				goto cleanup;
			}
			preset_port_count++;
			preset_conn[preset_port_nodeid] = B_TRUE;
		}
		if (preset_port_count != (cfg_qdev->scconf_qdev_votes + 1)) {
			if (messages != (char **)0) {
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				    "Non-default quorum votes being used, "
				    "aborting.\n"), messages);
			}
			rstatus = SCCONF_EEXIST;
			goto cleanup;
		}
		rstatus = conf_ins_optimal_qdev(&preset_qdevlist,
		    cfg_qdev->scconf_qdev_name, preset_conn);

		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}
	}

	/*
	 * Figure out the connectivity of each disk and attempt to add it
	 * to the list of optimal quorum devices. Don't even try if it has
	 * less than two nodes connected to it.
	 */
	did_devlist = list_all_devices("disk");
	if (did_devlist == NULL) {
		if (messages != (char **)0) {
			scconf_addmessage(dgettext(TEXT_DOMAIN,
			    "Can not obtain any did disk information.\n"),
			    messages);
		}
		rstatus = SCCONF_ENOEXIST;
		goto cleanup;
	}

	for (cur_dev = did_devlist; cur_dev; cur_dev = cur_dev->next) {

		port_count = 0;
		(void) memset(qdev_conn, B_FALSE,
		    sizeof (boolean_t) * SCCONF_NODEID_MAX);

		did_path = did_get_did_path(cur_dev);
		if (did_path == NULL) {
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Can not obtain did information for "
				    "instance \"%d\".\n"),
				    cur_dev->instance);
				scconf_addmessage(buffer, messages);
			}
			rstatus = SCCONF_ENOEXIST;
			goto cleanup;
		}
		(void) sprintf(didname, "/dev/did/%ss2", did_path);
		if (scconf_qdevname(didname, qdevname)) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		if (cur_dev->diskid == NULL) {
			continue;
		}

		subpath_ptr = cur_dev->subpath_listp;

		while (subpath_ptr != NULL) {
			curr_node = strtok(subpath_ptr->device_path, ":");
			if (curr_node == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			rstatus = conf_get_nodeid(clconf, curr_node, &nodeid);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			qdev_conn[nodeid] = B_TRUE;
			if (nodeid > max_nodeid) {
				max_nodeid = nodeid;
			}
			port_count++;
			subpath_ptr = subpath_ptr->next;
		}

		if (port_count < 2) {
			continue;
		}

		/* Ignore replicated devices */
		(void) sprintf(didname, "/dev/did/%s", did_path);
		free(did_path);

		repl_status = check_for_repl_device(didname, &repl_data);
		switch (repl_status) {
		case DID_REPL_TRUE:
			/* Replicated device. Ignore it */
			continue;

		case DID_REPL_FALSE:
			/* Non-replicated device */
			break;

		case DID_REPL_BAD_DEV:

		case DID_REPL_ERROR:
			/* internal error */
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		rstatus = conf_add_optimal_qdev(&optimal_qdevlist, qdevname,
		    qdev_conn, max_nodeid);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}
	}

	/*
	 * Go through all the existing quorum device, if any of the existing
	 * quorum disk is redundant (connected to a subset of nodes that
	 * another existing quorum disk is connected to), add it to the
	 * list of quorum disks to be removed. If it is redundant because
	 * of any disk in the new optimal list, mark it for removal too.
	 * In case an existing quorum disk has the same connectivity as that
	 * of a disk in the new optimal list, remove it from the optimal
	 * list.
	 */
	for (curr_preset_qdev = preset_qdevlist; curr_preset_qdev;
	    curr_preset_qdev = curr_preset_qdev->conf_conn_qdevnext) {

		redundant_quorum = 0;
		for (seen_preset_qdev = preset_qdevlist;
		    seen_preset_qdev && /* Just to satisfy lint */
		    strcmp(seen_preset_qdev->conf_conn_qdevname,
		    curr_preset_qdev->conf_conn_qdevname) != 0;
		    seen_preset_qdev =
		    seen_preset_qdev->conf_conn_qdevnext) {

			conf_cmp_conn(curr_preset_qdev->conf_conn_ports,
			    seen_preset_qdev, max_nodeid, &cmp_res);
			if (cmp_res <= 0) {
				rstatus = conf_ins_name_qdev(&qdevs_del,
				    curr_preset_qdev->conf_conn_qdevname);
				if (rstatus != SCCONF_NOERR) {
					goto cleanup;
				}
				redundant_quorum = 1;
				break;
			}
		}

		if (redundant_quorum) {
			continue;
		}

		for (curr_optimal_qdev = optimal_qdevlist,
		    prev_optimal_qdev = optimal_qdevlist; curr_optimal_qdev;
		    /* Do nothing */) {

			conf_cmp_conn(curr_preset_qdev->conf_conn_ports,
			    curr_optimal_qdev, max_nodeid, &cmp_res);
			if (cmp_res < 0) {
				rstatus = conf_ins_name_qdev(&qdevs_del,
				    curr_preset_qdev->conf_conn_qdevname);
				if (rstatus != SCCONF_NOERR) {
					goto cleanup;
				}
				prev_optimal_qdev = curr_optimal_qdev;
				curr_optimal_qdev =
				    curr_optimal_qdev->conf_conn_qdevnext;
			} else if (cmp_res == 0) {
				next_optimal_qdev =
				    curr_optimal_qdev->conf_conn_qdevnext;
				saved_curr = curr_optimal_qdev;
				saved_prev = prev_optimal_qdev;
				conf_del_optimal_qdev(&optimal_qdevlist,
				    prev_optimal_qdev, curr_optimal_qdev);
				if (saved_prev == saved_curr) {
					curr_optimal_qdev = next_optimal_qdev;
					prev_optimal_qdev = curr_optimal_qdev;
				} else {
					curr_optimal_qdev = next_optimal_qdev;
				}
			} else if (cmp_res == 2) {
				prev_optimal_qdev = curr_optimal_qdev;
				curr_optimal_qdev =
				    curr_optimal_qdev->conf_conn_qdevnext;
			} else {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}
	}

	curr_optimal = optimal_qdevlist;
	while (curr_optimal) {
		rstatus = conf_ins_name_qdev(qdevs_to_add,
		    curr_optimal->conf_conn_qdevname);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}
		curr_optimal = curr_optimal->conf_conn_qdevnext;
	}

	if (qdevs_to_del) {
		*qdevs_to_del = qdevs_del;
	}

cleanup:

	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	if (cfg_qdev_list) {
		conf_free_qdevs(cfg_qdev_list);
	}

	if (preset_qdevlist) {
		conf_free_conn_qdevlist(preset_qdevlist);
	}

	if (optimal_qdevlist) {
		conf_free_conn_qdevlist(optimal_qdevlist);
	}

	if (replaced_optimal_qdev) {
		free(replaced_optimal_qdev);
	}

	if (did_path != NULL) {
		free(did_path);
	}

	return (rstatus);
}

/*
 * scconf_free_name_qdevlist()
 * Free a list of type scconf_name_qdev_t *
 */
void
scconf_free_name_qdevlist(scconf_name_qdev_t *listp)
{
	scconf_name_qdev_t *tmp_qdev, *saved_ptr;

	tmp_qdev = listp;

	while (tmp_qdev) {
		saved_ptr = tmp_qdev;
		tmp_qdev = tmp_qdev->scconf_name_qdevnext;
		free(saved_ptr->scconf_name_qdevname);
		free(saved_ptr);
	}
}

/*
 * conf_free_conn_qdevlist()
 * Free a list of type conf_conn_qdev_t *
 */
static void
conf_free_conn_qdevlist(conf_conn_qdev_t *listp)
{
	conf_conn_qdev_t *tmp_qdev, *saved_ptr;

	tmp_qdev = listp;

	while (tmp_qdev) {
		saved_ptr = tmp_qdev;
		tmp_qdev = tmp_qdev->conf_conn_qdevnext;
		free(saved_ptr->conf_conn_qdevname);
		free(saved_ptr);
	}
}

/*
 * conf_add_optimal_qdev()
 * Evluate if the new device specified by qdevname and qdev_conn should be
 * added to the list qdevlist of optimal quorum disks being prepared. If
 * qdevname qualifies, it is added to qdevlist.
 *
 * Return status:
 * zero		- success (inserted qdevname, or no insertion needed)
 * non-zero	- error
 */
static scconf_errno_t
conf_add_optimal_qdev(conf_conn_qdev_t **qdevlist, char *qdevname,
    boolean_t *qdev_conn, scconf_nodeid_t max_nodeid)
{
	int cmp_res;
	conf_conn_qdev_t *curr_qdev, *prev_qdev, *next_qdev;
	conf_conn_qdev_t *saved_curr, *saved_prev;
	scconf_errno_t rstatus;

	for (curr_qdev = *qdevlist, prev_qdev = *qdevlist; curr_qdev; ) {

		conf_cmp_conn(qdev_conn, curr_qdev, max_nodeid, &cmp_res);

		if (cmp_res <= 0) {

			/*
			 * There is a better or similar disk already in our
			 * list.
			 */
			return (SCCONF_NOERR);

		} else if (cmp_res == 1) {

			/* The new disk can replace one in our list */
			saved_prev = prev_qdev;
			saved_curr = curr_qdev;
			next_qdev = curr_qdev->conf_conn_qdevnext;
			conf_del_optimal_qdev(qdevlist, prev_qdev, curr_qdev);
			if (saved_prev == saved_curr) {
				curr_qdev = next_qdev;
				prev_qdev = curr_qdev;
			} else {
				curr_qdev = prev_qdev->conf_conn_qdevnext;
			}

		} else {
			/*
			 * The new disk doesn't match the current one in our
			 * list, keep looking.
			 */
			prev_qdev = curr_qdev;
			curr_qdev = curr_qdev->conf_conn_qdevnext;
		}
	}

	/*
	 * If we are here, none of the disks in our list makes the new disk
	 * redundant, so we add the new disk to our list.
	 */
	rstatus = conf_ins_optimal_qdev(qdevlist, qdevname, qdev_conn);
	return (rstatus);
}

/*
 * conf_ins_optimal_qdev()
 * Insert quorum device specified by qdevname and qdev_conn into the list
 * of optimal devices.
 */
static scconf_errno_t
conf_ins_optimal_qdev(conf_conn_qdev_t **qdevlist, char *qdevname,
    boolean_t *qdev_conn)
{
	conf_conn_qdev_t *new_qdev;

	new_qdev = calloc(1, sizeof (conf_conn_qdev_t));
	if (new_qdev == (conf_conn_qdev_t *)0) {
		return (SCCONF_ENOMEM);
	}

	new_qdev->conf_conn_qdevname = calloc(1, strlen(qdevname) + 1);
	if (new_qdev->conf_conn_qdevname == (char *)0) {
		return (SCCONF_ENOMEM);
	}

	(void) strcpy(new_qdev->conf_conn_qdevname, qdevname);
	(void) memcpy(new_qdev->conf_conn_ports, qdev_conn,
	    sizeof (boolean_t) * SCCONF_NODEID_MAX);
	new_qdev->conf_conn_qdevnext = *qdevlist;

	*qdevlist = new_qdev;
	return (SCCONF_NOERR);
}

/*
 * conf_ins_name_qdev()
 * Insert quorum device specified by qdevname into the list of device names
 * qdevlist.
 */
static scconf_errno_t
conf_ins_name_qdev(scconf_name_qdev_t **qdevlist, char *qdevname)
{
	scconf_name_qdev_t *new_qdev;

	new_qdev = calloc(1, sizeof (scconf_name_qdev_t));
	if (new_qdev == (scconf_name_qdev_t *)0) {
		return (SCCONF_ENOMEM);
	}
	new_qdev->scconf_name_qdevname = calloc(1, strlen(qdevname) + 1);
	if (new_qdev->scconf_name_qdevname == (char *)0) {
		return (SCCONF_ENOMEM);
	}
	(void) strcpy(new_qdev->scconf_name_qdevname, qdevname);
	new_qdev->scconf_name_qdevnext = *qdevlist;

	*qdevlist = new_qdev;
	return (SCCONF_NOERR);
}

/*
 * conf_del_optimal_qdev()
 * Remove the quorum device specified by curr_qdev from the list of optimal
 * quorum devices qdevlist. prev_qdev is the previous entry in the linked
 * list to optimize things.
 */
static void
conf_del_optimal_qdev(conf_conn_qdev_t **qdevlist,
    conf_conn_qdev_t *prev_qdev, conf_conn_qdev_t *curr_qdev)
{
	if (prev_qdev == curr_qdev) {
		*qdevlist = curr_qdev->conf_conn_qdevnext;
	} else {
		prev_qdev->conf_conn_qdevnext =
		    curr_qdev->conf_conn_qdevnext;
	}
	free(curr_qdev->conf_conn_qdevname);
	free(curr_qdev);
}

/*
 * conf_cmp_conn()
 *
 * Decide whether the disk being evaluated (with connectivity specified in
 * qdev_conn) fits as a quorum device better than the curr_qdev.
 * max_nodeid specifies the highest nodeid seen in our computation so far
 * for any device, this speeds up calculations.
 *
 * The result as returned by cmp_res is
 *     -1	- curr_qdev is more suitable
 *     0	- both have equal connectivity
 *     1	- curr_qdev is less suitable
 *     2	- disjoint connectivity
 */
static void
conf_cmp_conn(boolean_t *qdev_conn, conf_conn_qdev_t *curr_qdev,
    scconf_nodeid_t max_nodeid, int *cmp_res)
{
	scconf_nodeid_t cnt;

	boolean_t conn_subset = B_TRUE;
	boolean_t conn_superset = B_TRUE;

	for (cnt = 0; cnt <= max_nodeid; cnt++) {
		switch (curr_qdev->conf_conn_ports[cnt]) {
		case B_TRUE:
			if (qdev_conn[cnt] == B_FALSE) {
				conn_superset = B_FALSE;
			}
			break;
		case B_FALSE:
			if (qdev_conn[cnt] == B_TRUE) {
				conn_subset = B_FALSE;
			}
			break;
		default:
			break;			/* Just to suppress lint! */
		}
	}

	switch (conn_subset) {
	case B_TRUE:
		switch (conn_superset) {
		case B_TRUE:
			*cmp_res = 0;
			break;
		case B_FALSE:
			*cmp_res = -1;
			break;
		default:
			break;			/* Just to suppress lint */
		}
		break;
	case B_FALSE:
		switch (conn_superset) {
		case B_TRUE:
			*cmp_res = 1;
			break;
		case B_FALSE:
			*cmp_res = 2;
			break;
		default:
			break;			/* Just to suppress lint */
		}
		break;
	default:
		break;				/* Just to suppress lint! */
	}
}

/*
 * Set the specfied properties to the given quorum device.
 *
 * This functions dlopen's the quorum library whose type corresponds
 * to the given quorum device name, and call the
 * scqd_set_qd_properties() routine in the library to set the
 * properties of this quorum device.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOEXIST		- quorum does not exist
 *	SCCONF_EUNKNOWN		- unkown quorum device type
 *	SCCONF_EPERM		- not root
 */
scconf_errno_t
scconf_set_qd_properties(char *devicename, char *properties,
    char **messages)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	char qdevname[MAXPATHLEN];
	void *dl_handle = NULL;
	scqd_set_qd_prop_t real_set_qd_prop_func;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (devicename == NULL || properties == NULL)
		return (SCCONF_EINVAL);

	/* Validate the quorumdevice name */
	dl_handle = open_qd_lib(devicename, qdevname,
	    SCCONF_GET_QD_DEVICE, QDLIB_HANDLE);
	if ((dl_handle == (void *)0) ||
	    (qdevname == NULL || *qdevname == '\0')) {
		return (SCCONF_ENOEXIST);
	}

	/* Get the address of the function in the library */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if ((real_set_qd_prop_func = (scqd_set_qd_prop_t)dlsym(
	    dl_handle, SCCONF_SET_QD_PROP)) !=  NULL) {
		scconferr = (*real_set_qd_prop_func)(qdevname,
		    properties, messages);
	} else {
		scconferr = SCCONF_EUNEXPECTED;
	}

	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}

	return (scconferr);
}

/*
 * scconf_check_quorum_devicetype
 *
 * This functions dlopen's the quorum library which corresponds to
 * the given type. If it can find such a quorum library, the given
 * device type is valid; Otherwise it is not a valid type.
 *
 * Possible return values:
 *
 *      B_TRUE          The type is valid
 *      B_FALSE         Unknown type
 */
boolean_t
scconf_check_quorum_devicetype(char *devicetype)
{
	void *dl_handle = NULL;
	boolean_t check_type = B_FALSE;

	/* Check arguments */
	if (devicetype == NULL)
		return (check_type);

	/* Get the library for this device type */
	dl_handle = open_qd_lib(devicetype, NULL,
	    SCCONF_GET_QD_TYPE, QDLIB_HANDLE);
	if (dl_handle != (void *)0) {
		check_type = B_TRUE;
		(void) dlclose(dl_handle);
	}

	return (check_type);
}

/*
 * Set the specfied pathes to the node for the given quorum device. If
 * no quorum devices are specified, set the node paths to all quorum
 * devices. If the flag is set, add the node path to the qorum devices;
 * If the flag is not set, remove the node path to the qorum devices.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR            - success
 *	SCCONF_ENOMEM           - not enough memory
 *	SCCONF_EINVAL           - invalid argument
 *	SCCONF_ENOEXIST         - node does not exist
 */
scconf_errno_t
scconf_set_qd_common_properties(char *devicename,
    char *nodename, uint_t flag)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_iter_t *currqdevsi, *qdevsi = (clconf_iter_t *)0;
	clconf_obj_t *qdev;
	scconf_nodeid_t nodeid;
	const char *propval;
	int do_commit;
	char pathprop[BUFSIZ];
	const char *qdev_name;

	/* Check arguments */
	if (nodename == NULL) {
		return (SCCONF_EINVAL);
	}

	/*
	 * Change the state for all "paths" for this node.
	 *
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Initialize the do_commit flag */
		do_commit = 0;

		/* Get the nodeid */
		rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Initialize the property name for the "path" */
		(void) sprintf(pathprop, "path_%d", nodeid);

		/* For all quorum devices, check for a path */
		qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
		while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {

			/* Get the name of this quorum */
			if (devicename) {
				qdev_name = clconf_obj_get_name(qdev);
				if (qdev_name &&
				    (strcmp(qdev_name, devicename) != 0)) {
					continue;
				}
			}

			/* Get the path for this node */
			propval = clconf_obj_get_property(qdev, pathprop);

			/* Remove the path? */
			clconf_err = CL_NOERROR;
			if (!flag) {
				if (propval) {
					clconf_err = clconf_obj_set_property(
					    qdev, pathprop, NULL);
					if (clconf_err != CL_NOERROR) {
						rstatus = SCCONF_EUNEXPECTED;
						goto cleanup;
					}
					++do_commit;
				}
			} else {
				clconf_err = clconf_obj_set_property(qdev,
				    pathprop, "enabled");
				if (clconf_err != CL_NOERROR) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}
				++do_commit;
			}

			clconf_iter_advance(currqdevsi);
		}

		/* Release the iterator */
		if (qdevsi != (clconf_iter_t *)0)
			clconf_iter_release(qdevsi);
		qdevsi = (clconf_iter_t *)0;

		/* If do_commit is not set, we are done */
		if (!do_commit) {
			rstatus = SCCONF_NOERR;
			break;
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit return okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdate handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

	/*
	 * Sync all of the quorum device to which we have a path.
	 * Use the clconf image and "pathprop" from the last lookup.
	 */
	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
	while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {

		/* Skip devices to which we do not have a "path" */
		propval = clconf_obj_get_property(qdev, pathprop);
		if (propval == NULL) {
			clconf_iter_advance(currqdevsi);
			continue;
		}

		/* Get the name of the global device */
		propval = clconf_obj_get_property(qdev, PROP_QDEV_GDEVNAME);
		if (propval == NULL)
			continue;

		/* attempt to synch, ignoring errors */
		(void) conf_sync_quorum_device((char *)propval);

		clconf_iter_advance(currqdevsi);
	}

cleanup:
	/* Release the iterator */
	if (qdevsi != (clconf_iter_t *)0)
		clconf_iter_release(qdevsi);

	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_qdevname
 *
 * Using the given "didname", return a copy of name which should
 * be used in the infrastructure CCR table for the quorum device name.
 *
 * "qdevname" must be large enough to hold a copy of the new name.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_qdevname(char *didname, char *qdevname)
{
	char buffer[MAXPATHLEN];
	int disknumber;

	(void) sprintf(buffer, "%sd%%d", SCCONF_DID_RDSK);
	if (sscanf(didname, buffer, &disknumber) != 1)
		return (SCCONF_EINVAL);

	(void) sprintf(qdevname, "d%d", disknumber);

	return (SCCONF_NOERR);
}

/*
 * conf_rm_quorum_device
 *
 * Remove a quorum device. Currently this function is common to both
 * scsi and NAS quorum. If there will be some device type specific
 * tasks in the future, this function can be updated and put into
 * the quorum library separately.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
scconf_errno_t
conf_rm_quorum_device(char *quorumdevice, uint32_t force_flg)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_cfg_prop_t *prop, *propslist = (scconf_cfg_prop_t *)0;
	const char *propval;
	scconf_nodeid_t nodeid;
	clconf_obj_t *qdev;
	int do_commit;
	int installmode;
	int nodecount;
	int qdevcount;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (quorumdevice == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Remove all ports, then the device.
	 *
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Free the propslist */
		if (propslist != (scconf_cfg_prop_t *)0) {
			conf_free_proplist(propslist);
			propslist = (scconf_cfg_prop_t *)0;
		}

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Initialize do_commit flag */
		do_commit = 0;

		/* Get the installmode flag */
		propval = clconf_obj_get_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_INSTALLMODE);
		if (propval && strcmp(propval, "enabled") == 0)
			installmode = 1;
		else
			installmode = 0;

		/* Get the shared quorum device */
		rstatus = conf_get_qdevobj(clconf, quorumdevice, &qdev);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/*
		 * If installmode is not set and quorum is enabled, make sure
		 * that we are not trying to remove the last enabled quorum
		 * device on a two node cluster and disable the quorum device.
		 */
		if ((!force_flg) &&
			(!installmode && (clconf_obj_enabled(qdev)))) {
			/* Get the nodecount */
			nodecount = conf_nodecount(clconf);
			if (nodecount < 1) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* If 2 nodes or fewer, get the number of enabled QD */
			if (nodecount <= 2) {
				qdevcount = conf_qdev_enabled_count(clconf);
				/* There must be at least one quorum device */
				if (qdevcount <= 1) {
					rstatus = SCCONF_EQUORUM;
					goto cleanup;
				}
			}
		}

		if (clconf_obj_enabled(qdev)) {
			/*
			 * Disable the quorum device.
			 * conf_sync_quorum_device() is then called; and reduce
			 * the vote of the quorum device to 0.
			 * Retry the command if there is a SCCONF_STALE error.
			 */
			for (;;) {
				rstatus =
				    conf_setstate_quorum_device(quorumdevice,
					SCCONF_STATE_DISABLED, force_flg);

				/*
				 * if conf_setstate_quorum_device return okay,
				 * we are done.
				 */
				if (rstatus == SCCONF_NOERR) {
					break;

				/* else if outdate handle, continue to retry */
				} else if (rstatus != SCCONF_ESTALE) {
					goto cleanup;
				}
			}
		}

		/* Get all of the properties for the device */
		rstatus = conf_get_proplist(qdev, (char **)0, &propslist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Remove configured "paths" as needed */
		for (prop = propslist;  prop;
		    prop = prop->scconf_prop_next) {

			/* Skip any non-"path" properties */
			if (sscanf(prop->scconf_prop_key,
			    "path_%d", &nodeid) != 1)
				continue;

			/* Remove the "path" property */
			clconf_err = clconf_obj_set_property(qdev,
			    prop->scconf_prop_key, NULL);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}

			/* Set the do_commit flag */
			++do_commit;
		}
		/* If do_commit is not set, we are done removing "paths" */
		if (!do_commit) {
			rstatus = SCCONF_NOERR;
			break;
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);
		/* if commit returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;
		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

	/*
	* If there are no ports remaining to the device, remove it.
	*
	* Loop until the commit succeeds, or until a fatal error
	* is encountered.
	*/
	for (;;) {

		/* Free the propslist */
		if (propslist != (scconf_cfg_prop_t *)0) {
			conf_free_proplist(propslist);
			propslist = (scconf_cfg_prop_t *)0;
		}

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
			(scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get the shared quorum device */
		rstatus = conf_get_qdevobj(clconf, quorumdevice, &qdev);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get all of the properties for the device */
		rstatus = conf_get_proplist(qdev, (char **)0, &propslist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Set the do_commit flag */
		do_commit = 1;

		/* Clear do_commit, if "path" properties remain */
		for (prop = propslist;  prop;
			prop = prop->scconf_prop_next) {
			if (sscanf(prop->scconf_prop_key,
			    "path_%d", &nodeid) == 1)
				do_commit = 0;
		}

		/* If do_commit is not set, we are done */
		if (!do_commit) {
			rstatus = SCCONF_NOERR;
			break;
		}

		/* Verify that the vote count is set to zero */
		propval = clconf_obj_get_property(qdev, PROP_QDEV_QUORUM_VOTE);
		if (propval == NULL) {
			rstatus = SCCONF_EBADVALUE;
			goto cleanup;
		}
		if (strcmp(propval, "0") != 0) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Remove the quorum device */
		clconf_err = clconf_cluster_remove_quorum_device(clconf,
			(clconf_quorum_t *)qdev);

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	/* Free the propslist */
	if (propslist != (scconf_cfg_prop_t *)0) {
		conf_free_proplist(propslist);
		propslist = (scconf_cfg_prop_t *)0;
	}

	return (rstatus);
}

/*
 * conf_get_qdevs
 *
 * Get the qdev list from the given "clconf".
 *
 * The caller is responsible for freeing memory
 * associated with the "qdevslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_qdevs(clconf_cluster_t *clconf, scconf_cfg_qdev_t **qdevslistp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_iter_t *currqdevsi, *qdevsi = (clconf_iter_t *)0;
	scconf_cfg_qdev_t *qdevslist = (scconf_cfg_qdev_t *)0;
	scconf_cfg_prop_t *prop, *propslist = (scconf_cfg_prop_t *)0;
	scconf_cfg_prop_t **nextpropp;
	scconf_cfg_qdev_t **nextp;
	scconf_cfg_qdevport_t **nextportp;
	scconf_qdevstate_t state;
	clconf_obj_t *qdev;
	scconf_nodeid_t nodeid;
	const char *gdevname;
	const char *dev_type;
	const char *name;
	const char *svotecount;
	const char *acc_mode;

	/* Check arguments */
	if (clconf == NULL || qdevslistp == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of shared quorum devices */
	nextp = &qdevslist;
	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
	while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {

		/* Allocate struct for the next qdev */
		*nextp = (scconf_cfg_qdev_t *)calloc(1,
		    sizeof (scconf_cfg_qdev_t));
		if (*nextp == (scconf_cfg_qdev_t *)0) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add the qdev name */
		name = clconf_obj_get_name(qdev);

		/* if no name, use the gdevice name */
		if (name == NULL) {
			name = clconf_obj_get_property(qdev,
			    PROP_QDEV_GDEVNAME);
		}

		/* set the name */
		if (name != NULL) {
			(*nextp)->scconf_qdev_name = strdup(name);
			if ((*nextp)->scconf_qdev_name == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Add the access mode property */
		acc_mode = clconf_obj_get_property(qdev, PROP_QDEV_ACCESSMODE);

		if (acc_mode != NULL && *acc_mode != '\0') {
			(*nextp)->scconf_qdev_accessmode = strdup(acc_mode);
			if ((*nextp)->scconf_qdev_accessmode == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Add qdev votes */
		svotecount = clconf_obj_get_property(qdev,
		    PROP_QDEV_QUORUM_VOTE);
		if (svotecount == NULL) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		errno = 0; /*lint !e746 */
		(*nextp)->scconf_qdev_votes = (uint_t)atoi(svotecount);
		if (errno != 0) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Set the quorum device state */
		state = (clconf_obj_enabled(qdev)) ?
		    SCCONF_STATE_ENABLED : SCCONF_STATE_DISABLED;
		(*nextp)->scconf_qdev_state = state;

		/* Set the quorum device name */
		gdevname = clconf_obj_get_property(qdev, PROP_QDEV_GDEVNAME);
		(*nextp)->scconf_qdev_device = strdup(gdevname);

		/* Set the device type */
		dev_type = clconf_obj_get_property(qdev, PROP_QDEV_TYPE);
		if (dev_type != NULL && *dev_type != '\0') {
			(*nextp)->scconf_qdev_type = strdup(dev_type);
			if ((*nextp)->scconf_qdev_type == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		} else if (acc_mode != NULL && *acc_mode != '\0') {
			(*nextp)->scconf_qdev_type = strdup(acc_mode);
			if ((*nextp)->scconf_qdev_type == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Add the ports */
		nextportp = &(*nextp)->scconf_qdev_ports;
		rstatus = conf_get_proplist(qdev, (char **)0, &propslist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;
		for (prop = propslist;  prop; prop = prop->scconf_prop_next) {

			/* Skip any non-"path" properties */
			if (sscanf(prop->scconf_prop_key,
			    "path_%d", &nodeid) != 1)
				continue;

			/* Allocate struct for the next port name */
			*nextportp = (scconf_cfg_qdevport_t *)calloc(1,
			    sizeof (scconf_cfg_qdevport_t));
			if (*nextportp == (scconf_cfg_qdevport_t *)0) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			/* Get the nodename */
			rstatus = conf_get_nodename(clconf, nodeid,
			    &(*nextportp)->scconf_qdevport_nodename);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

			/* Get the state of the "path" */
			if (prop->scconf_prop_value &&
			    strcmp(prop->scconf_prop_value, "enabled") == 0)
				state = SCCONF_STATE_ENABLED;
			else
				state = SCCONF_STATE_DISABLED;
			(*nextportp)->scconf_qdevport_state = state;

			/* next */
			nextportp = &(*nextportp)->scconf_qdevport_next;
		}

		/* Get other property list */
		nextpropp = &(*nextp)->scconf_qdev_propertylist;
		for (prop = propslist;  prop; prop = prop->scconf_prop_next) {

			/* Skip property with no key/value */
			if (!(prop->scconf_prop_key) ||
			    !(prop->scconf_prop_value))
				continue;

			/* Skip common properties */
			if (sscanf(prop->scconf_prop_key,
			    "path_%d", &nodeid) == 1)
				continue;

			if (strcmp(prop->scconf_prop_key,
			    PROP_QDEV_QUORUM_VOTE) == 0)
				continue;

			if (strcmp(prop->scconf_prop_key,
			    PROP_QDEV_GDEVNAME) == 0)
				continue;

			if (strcmp(prop->scconf_prop_key,
			    PROP_QDEV_ACCESSMODE) == 0)
				continue;

			if (strcmp(prop->scconf_prop_key,
			    PROP_QDEV_TYPE) == 0)
				continue;

			/* Allocate struct for the next property */
			*nextpropp = (scconf_cfg_prop_t *)calloc(1,
			    sizeof (scconf_cfg_prop_t));
			if (*nextpropp == (scconf_cfg_prop_t *)0) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			/* Add the properties */
			(*nextpropp)->scconf_prop_key = strdup(
			    prop->scconf_prop_key);
			if ((*nextpropp)->scconf_prop_key == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			(*nextpropp)->scconf_prop_value = strdup(
			    prop->scconf_prop_value);
			if ((*nextpropp)->scconf_prop_value == NULL) {
				rstatus = SCCONF_ENOMEM;
				if ((*nextpropp)->scconf_prop_key)
					free((*nextpropp)->scconf_prop_key);
				goto cleanup;
			}

			/* next property */
			nextpropp = &(*nextpropp)->scconf_prop_next;
		}

		if (propslist != (scconf_cfg_prop_t *)0) {
			conf_free_proplist(propslist);
			propslist = (scconf_cfg_prop_t *)0;
		}

		/* next */
		nextp = &(*nextp)->scconf_qdev_next;
		clconf_iter_advance(currqdevsi);
	}

	rstatus = SCCONF_NOERR;
	*qdevslistp = qdevslist;

cleanup:
	if (qdevsi != (clconf_iter_t *)0)
		clconf_iter_release(qdevsi);

	if (propslist != (scconf_cfg_prop_t *)0)
		conf_free_proplist(propslist);

	if (rstatus != SCCONF_NOERR && qdevslist)
		conf_free_qdevs(qdevslist);

	return (rstatus);
}

/*
 * conf_free_qdevs
 *
 * Free all memory associated with an "scconf_cfg_qdev" structure.
 */
void
conf_free_qdevs(scconf_cfg_qdev_t *qdevslist)
{
	scconf_cfg_qdev_t *last, *next;
	scconf_cfg_qdevport_t *lastn, *nextn;

	/* Check argument */
	if (qdevslist == NULL)
		return;

	/* Free memory associated with each shared quorum device in the list */
	last = (scconf_cfg_qdev_t *)0;
	next = qdevslist;
	while (next != NULL) {

		/* free the last qdev struct */
		if (last != NULL)
			free(last);

		/* qdev name */
		if (next->scconf_qdev_name)
			free(next->scconf_qdev_name);

		/* device name */
		if (next->scconf_qdev_device)
			free(next->scconf_qdev_device);

		/* device access mode */
		if (next->scconf_qdev_accessmode)
			free(next->scconf_qdev_accessmode);

		/* device type */
		if (next->scconf_qdev_type)
			free(next->scconf_qdev_type);

		/* portlist */
		lastn = (scconf_cfg_qdevport_t *)0;
		nextn = next->scconf_qdev_ports;
		while (nextn != NULL) {

			/* free the last namelist struct */
			if (lastn != NULL)
				free(lastn);

			/* name */
			if (nextn->scconf_qdevport_nodename)
				free(nextn->scconf_qdevport_nodename);

			lastn = nextn;
			nextn = nextn->scconf_qdevport_next;
		}
		if (lastn != NULL)
			free(lastn);

		/* property list */
		if (next->scconf_qdev_propertylist)
			conf_free_proplist(next->scconf_qdev_propertylist);

		last = next;
		next = next->scconf_qdev_next;
	}
	if (last != NULL)
		free(last);
}

/*
 * conf_setstate_quorum_device
 *
 * Set the state of a shared quourm device to either enabled
 * or disabled, as indicated by "state".  This doesn't mean anything
 * to the kernel quorum algorithm.  But, we use it to indicate whether or
 * not the device is in maint state (disabled in maint state).
 *
 * conf_sync_quorum_device() is then called;   it checks the state and
 * adjusts the vote count accordingly (in a stepped fashion).
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
scconf_errno_t
conf_setstate_quorum_device(char *quorumdevice, scconf_state_t state,
	uint32_t force_flg)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_obj_t *qdev;
	char qdevname[MAXPATHLEN];
	int nodecount;
	int qdevcount;
	void *ret_lib = NULL;
	uint_t install_flag;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (quorumdevice == NULL)
		return (SCCONF_EINVAL);

	/* Validate the quorumdevice name */
	ret_lib = open_qd_lib(quorumdevice, qdevname,
	    SCCONF_GET_QD_DEVICE, QDLIB_NO_HANDLE);
	if ((ret_lib == (void *)0) ||
	    (qdevname == NULL || *qdevname == '\0')) {
		return (SCCONF_ENOEXIST);
	}

	rstatus = scconf_get_quorum_installflag(&install_flag);
	if (rstatus != SCCONF_NOERR) {
		return (rstatus);
	}

	/*
	 * Set the state of the quorum device as indicated.
	 * This doesn't mean anything to the kernel quorum
	 * algorithm.  But, we use it to indicate whether or
	 * not the device is in maint state.  conf_sync_quorum_device()
	 * checks for this and adjusts the vote count accordingly.
	 *
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get the shared quorum device */
		rstatus = conf_get_qdevobj(clconf, qdevname, &qdev);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/*
		 * Make sure that we are not trying to disable the last enabled
		 * quorum device on a two node cluster, unless install mode
		 * is set.
		 */
		if (((state == SCCONF_STATE_DISABLED) &&
		    (clconf_obj_enabled(qdev))) && (!force_flg)) {
			/* Get the nodecount */
			nodecount = conf_nodecount(clconf);
			if (nodecount < 1) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/*
			 * If 2 nodes or fewer, and install mode is
			 * not set, then get and check the number of
			 * enabled QD.
			 */
			if ((nodecount <= 2) && (!install_flag)) {
				qdevcount = conf_qdev_enabled_count(clconf);

				/* There must be at least one quorum device */
				if (qdevcount <= 1) {
					rstatus = SCCONF_EQUORUM;
					goto cleanup;
				}
			}
		}

		/* Set the state */
		rstatus = conf_set_state(qdev, state);
		if (rstatus != SCCONF_NOERR)
		goto cleanup;

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);
		/* if commit returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}
	/* sync the quorum device */
	rstatus = conf_sync_quorum_device(quorumdevice);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * conf_sync_quorum_device
 *
 * Adjust the vote count of an "enabled" quorum device to be equal
 * to the number of "enabled" paths minus one.  Or, if there are no
 * enabled paths, or the quorum device itself is not enabled, the
 * vote count is set to zero.
 *
 * The vote count is only ever changed by a unit of one for any commit.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- quorumdev not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
static scconf_errno_t
conf_sync_quorum_device(char *quorumdevice)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	scconf_cfg_prop_t *prop, *propslist = (scconf_cfg_prop_t *)0;
	const char *propval;
	clconf_obj_t *qdev;
	scconf_nodeid_t nodeid;
	clconf_obj_t *node;
	char qdevname[MAXPATHLEN];
	char svotecount[BUFSIZ];
	int votecount, targetvote, nodevote;
	int first;
	void *ret_lib = NULL;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (quorumdevice == NULL)
		return (SCCONF_EINVAL);

	/* Validate the quorumdevice name */
	ret_lib = open_qd_lib(quorumdevice, qdevname,
	    SCCONF_GET_QD_DEVICE, QDLIB_NO_HANDLE);
	if ((ret_lib == (void *)0) ||
	    (qdevname == NULL || *qdevname == '\0')) {
		return (SCCONF_ENOEXIST);
	}

	/*
	 * Loop until we reach the necessary vote count, or until
	 * a fatal error is encountered.
	 */
	first = 1;
	for (;;) {

		/* Free the propslist */
		if (propslist != (scconf_cfg_prop_t *)0) {
			conf_free_proplist(propslist);
			propslist = (scconf_cfg_prop_t *)0;
		}

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get the shared quorum device */
		rstatus = conf_get_qdevobj(clconf, qdevname, &qdev);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get the current vote count */
		propval = clconf_obj_get_property(qdev, PROP_QDEV_QUORUM_VOTE);
		if (propval == NULL) {
			rstatus = SCCONF_EBADVALUE;
			goto cleanup;
		}
		errno = 0;
		votecount = atoi(propval);
		if (errno != 0) {
			rstatus = SCCONF_EBADVALUE;
			goto cleanup;
		}

		/* Start with a target vote count of zero */
		targetvote = 0;

		/* If the device is enabled, consider "enabled" paths */
		if (clconf_obj_enabled(qdev)) {
			rstatus = conf_get_proplist(qdev, (char **)0,
			    &propslist);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			for (prop = propslist;  prop;
			    prop = prop->scconf_prop_next) {
				if (sscanf(prop->scconf_prop_key,
				    "path_%d", &nodeid) == 1 &&
				    strcmp(prop->scconf_prop_value,
				    "enabled") == 0) {

					/* Get the node obj */
					rstatus = conf_get_nodeobj(clconf,
					    nodeid, &node);
					if (rstatus != SCCONF_NOERR)
						goto cleanup;

					/* Get the current vote count */
					propval = clconf_obj_get_property(node,
					    PROP_NODE_QUORUM_VOTE);
					if (propval == NULL) {
						nodevote = 0;
					} else {
						errno = 0;
						nodevote = atoi(propval);
						if (errno != 0) {
							rstatus =
							    SCCONF_EBADVALUE;
							goto cleanup;
						}
					}
					propval = NULL;

					/* Increment QD vote by node vote */
					targetvote += nodevote;
				}
			}

			/* vote count is number of connected votes - 1 */
			if (targetvote)
				--targetvote;
		}

		/* If the vote count is good, we are done */
		if (votecount == targetvote) {
			rstatus = SCCONF_NOERR;
			break;
		}

		/* Otherwise, set the new vote count */
		if (votecount > targetvote)
			--votecount;
		else
			++votecount;

		/*
		 * If this is our first time through and the difference between
		 * the targetvote and the initial votecount is greater than one
		 * unit, make sure that the target is valid.
		 */
		if ((first) && (targetvote != votecount)) {
			/* Only do this once */
			first = 0;

			/* Make a copy of the original */
			existing_clconf =
			    (clconf_cluster_t *)clconf_obj_deep_copy(
			    (clconf_obj_t *)clconf);
			if (existing_clconf == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			/* Update the property with the target vote */
			(void) sprintf(svotecount, "%d", targetvote);
			clconf_err = clconf_obj_set_property(qdev,
			    PROP_QDEV_QUORUM_VOTE, svotecount);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}

			/* Check the transition, SCCONF_ERANGE is expected */
			rstatus = conf_check_transition(existing_clconf, clconf,
			    NULL);
			switch (rstatus) {
			case SCCONF_ERANGE:
				break;
			case SCCONF_ESTALE:
				first = 1;
				continue;
			case SCCONF_NOERR:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			default:
				goto cleanup;
			} /*lint !e788 */
		}

		/* Update the property */
		(void) sprintf(svotecount, "%d", votecount);
		clconf_err = clconf_obj_set_property(qdev,
		    PROP_QDEV_QUORUM_VOTE, svotecount);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);
		if (rstatus != SCCONF_NOERR && rstatus != SCCONF_ESTALE)
			goto cleanup;
		}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	/* Free the propslist */
	if (propslist != (scconf_cfg_prop_t *)0) {
		conf_free_proplist(propslist);
		propslist = (scconf_cfg_prop_t *)0;
	}

	return (rstatus);
}

/*
 * conf_setcount_quorum_node
 *
 * Set the vote count for the node to the defaultvote value from
 * the CCR file (if given) or to one.  This is done in a stepped
 * fashion.   Once the vote count has been set, all quorum devices
 * ports are checked, and their state and quorum vote adjusted
 * to reflect the vote count of the node.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- node not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 *	SCCONF_EINUSE		- opposing command is in progress
 */
static scconf_errno_t
conf_setcount_quorum_node(char *nodename, int targetvote)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	clconf_iter_t *currqdevsi, *qdevsi = (clconf_iter_t *)0;
	clconf_obj_t *qdev;
	clconf_obj_t *node;
	scconf_nodeid_t nodeid;
	const char *propval;
	int do_commit;
	int votecount;
	int last_votecount = 0;
	int first;
	char pathprop[BUFSIZ];
	char svotecount[BUFSIZ];
	char *spathstate;
#ifdef DEBUG
	char *value;
	uint_t seconds;
#endif /* DEBUG */

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodename == NULL)
		return (SCCONF_EINVAL);
	if ((targetvote < 0) ||
	    (targetvote > (MAXINT / NODEID_MAX))) /*lint !e504 */
		return (SCCONF_EINVAL);

	/*
	 * Loop until we reach the necessary vote count, or until
	 * a fatal error is encountered.
	 */
	first = 1;
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get the nodeid */
		rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Find the node */
		rstatus = conf_get_nodeobj(clconf, nodeid, &node);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get the current vote count */
		propval = clconf_obj_get_property(node, PROP_NODE_QUORUM_VOTE);
		if (propval == NULL) {
			votecount = 0;
		} else {
			errno = 0;
			votecount = atoi(propval);
			if (errno != 0) {
				rstatus = SCCONF_EBADVALUE;
				goto cleanup;
			}
		}

		/* If the vote count is good, we are done */
		if (votecount == targetvote && propval != NULL) {
			rstatus = SCCONF_NOERR;
			break;
		}

		/* Set the new vote count */
		if (votecount > targetvote)
			--votecount;
		else if (votecount < targetvote) {
			/* Check if the vote count was decreased */
			if (votecount < last_votecount) {
				rstatus = SCCONF_EINUSE;
				goto cleanup;
			}
			++votecount;
		}

		/*
		 * If this is our first time through and the difference between
		 * the targetvote and the initial votecount is greater than one
		 * unit, make sure that the target is valid.
		 */
		if ((first) && (targetvote != votecount)) {
			/* Only do this once */
			first = 0;

			/* Make a copy of the original */
			existing_clconf =
			    (clconf_cluster_t *)clconf_obj_deep_copy(
			    (clconf_obj_t *)clconf);
			if (existing_clconf == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			/* Update the property with the target vote */
			(void) sprintf(svotecount, "%d", targetvote);
			clconf_err = clconf_obj_set_property(node,
			    PROP_NODE_QUORUM_VOTE, svotecount);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}

			/* Check the transition, SCCONF_ERANGE is expected */
			rstatus = conf_check_transition(existing_clconf, clconf,
			    NULL);
			switch (rstatus) {
			case SCCONF_ERANGE:
				break;
			case SCCONF_ESTALE:
				first = 1;
				continue;
			case SCCONF_NOERR:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			default:
				goto cleanup;
			} /*lint !e788 */
		}

		last_votecount = votecount;

		/* Update the property */
		(void) sprintf(svotecount, "%d", votecount);
		clconf_err = clconf_obj_set_property(node,
		    PROP_NODE_QUORUM_VOTE, svotecount);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);
		if (rstatus != SCCONF_NOERR && rstatus != SCCONF_ESTALE)
			goto cleanup;
#ifdef DEBUG
		/*
		 * If compiled for DEBUG, pause here for SCCONF_VOTE_PAUSE
		 * seconds.
		 *
		 * This allows testing for proper handling of opposing scconf
		 * commands. To test, set SCCONF_VOTE_PAUSE, launch a process
		 * which will increase the votecount, pause about
		 * SCCONF_VOTE_PAUSE/2 seconds and launch a second process which
		 * will decrease the vote. The first process should give up and
		 * let the second process decrease the vote count.
		 */
		if ((value = getenv("SCCONF_VOTE_PAUSE")) != NULL &&
		    (seconds = (uint_t)atoi(value)) > 0) {
			(void) printf("Pausing for %d seconds to allow for "
			    "node votecount testing (count=%d, target=%d)... ",
			    seconds, votecount, targetvote);
			(void) fflush(stdout);
			(void) sleep(seconds);
			(void) printf("done.\n");
		}
#endif /* DEBUG */
	}

	/* The new state for each path */
	spathstate = (targetvote == 0) ? "disabled" : "enabled";

	/*
	 * Change the state for all "paths" for this node.
	 *
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Initialize the do_commit flag */
		do_commit = 0;

		/* Get the nodeid */
		rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Initialize the property name for the "path" */
		(void) sprintf(pathprop, "path_%d", nodeid);

		/* For all quorum devices, check for a path */
		qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
		while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {

			/* Check for an enabled path for this node */
			propval = clconf_obj_get_property(qdev, pathprop);
			if (propval != NULL &&
			    strcmp(propval, spathstate) != 0) {
				clconf_err = clconf_obj_set_property(qdev,
				    pathprop, spathstate);
				if (clconf_err != CL_NOERROR) {
					switch (clconf_err) {
					case CL_BADNAME:
					default:
						rstatus = SCCONF_EUNEXPECTED;
						goto cleanup;
					} /*lint !e788 */
				}
				++do_commit;
			}

			clconf_iter_advance(currqdevsi);
		}

		/* Release the iterator */
		if (qdevsi != (clconf_iter_t *)0)
			clconf_iter_release(qdevsi);
		qdevsi = (clconf_iter_t *)0;

		/* If do_commit is not set, we are done */
		if (!do_commit) {
			rstatus = SCCONF_NOERR;
			break;
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit return okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdate handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

	/*
	 * Sync all of the quorum device to which we have a path.
	 * Use the clconf image and "pathprop" from the last lookup.
	 */
	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
	while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {

		/* Skip devices to which we do not have a "path" */
		propval = clconf_obj_get_property(qdev, pathprop);
		if (propval == NULL) {
			clconf_iter_advance(currqdevsi);
			continue;
		}

		/* Get the name of the global device */
		propval = clconf_obj_get_property(qdev, PROP_QDEV_GDEVNAME);
		if (propval == NULL)
			continue;

		/* attempt to synch, ignoring errors */
		(void) conf_sync_quorum_device((char *)propval);

		clconf_iter_advance(currqdevsi);
	}

cleanup:
	/* Release the iterator */
	if (qdevsi != (clconf_iter_t *)0)
		clconf_iter_release(qdevsi);

	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	return (rstatus);
}

/*
 * conf_set_installmode_state
 *
 * Set the state of the cluster install mode property to "enabled"
 * or "disabled".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
static scconf_errno_t
conf_set_installmode_state(scconf_state_t state)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	char *spropstate;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	switch (state) {
	case SCCONF_STATE_ENABLED:
		spropstate = "enabled";
		break;

	case SCCONF_STATE_DISABLED:
		spropstate = "disabled";
		break;

	case SCCONF_STATE_UNCHANGED:
		return (SCCONF_NOERR);

	default:
		return (SCCONF_EINVAL);
	}

	/*
	 * Loop until the commit succeeds, or until
	 * a fatal error is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Set the property */
		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_INSTALLMODE, spropstate);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit return okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdate handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * conf_get_qdevobj
 *
 * Return the object pointer (clconf_obj_t *) for the given
 * share quorum device "name" in "qdevp", using the given "clconf".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the diddisknum is not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_qdevobj(clconf_cluster_t *clconf, char *qdevname, clconf_obj_t **qdevp)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_iter_t *currqdevsi, *qdevsi = (clconf_iter_t *)0;
	clconf_obj_t *qdev;
	const char *name;

	/* Check arguments */
	if (clconf == NULL || qdevname == NULL || qdevp == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of shared quorum devices */
	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
	while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {
		name = clconf_obj_get_name(qdev);
		if (name != NULL && strcmp(name, qdevname) == 0) {
			*qdevp = qdev;
			rstatus = SCCONF_NOERR;
			goto cleanup;
		}
		clconf_iter_advance(currqdevsi);
	}

	/* Not found */
	rstatus = SCCONF_ENOEXIST;

cleanup:
	if (qdevsi != (clconf_iter_t *)0)
		clconf_iter_release(qdevsi);

	return (rstatus);
}

/*
 * conf_check_vote_change
 *
 * Given the current cluster config passed in, modify it to reflect what
 * would be the final state of the cluster after the change to the node
 * quorum vote was processed. Check to make sure that in the final state, the
 * cluster has quorum. If that is true, then each 1 by 1 vote change along the
 * way should also have quorum, so we won't end up in an in-between state.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ELARGE		- either multiple changes or large change.
 *	SCCONF_EQUORUM		- change could cause loss of quorum.
 *	SCCONF_ESTALE		- clconfig is stale. (Will be retried.)
 *	SCCONF_ENOMEM
 */
static scconf_errno_t conf_check_vote_change(clconf_cluster_t *clconf,
    scconf_nodeid_t nodeid, uint_t voteval, uint_t old_vote)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *modified_clconf = (clconf_cluster_t *)0;
	clconf_iter_t *currqdevsi, *qdevsi = (clconf_iter_t *)0;
	clconf_obj_t *node, *qdev;
	char pathprop[BUFSIZ];
	const char *propval;
	uint_t qd_vote;
	char svotecount[BUFSIZ];
	int qd_effective_vote;

	/* Make a copy of the original clconfig. */
	modified_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
	    (clconf_obj_t *)clconf);
	if (modified_clconf == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Find the node */
	rstatus = conf_get_nodeobj(modified_clconf, nodeid, &node);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/*
	 * Check if the total change could cause loss of quorum.
	 * First, set the vote of the node to the proposed vote.
	 */
	(void) sprintf(svotecount, "%d", voteval);
	clconf_err = CL_NOERROR;
	clconf_err = clconf_obj_set_property(node,
	    PROP_NODE_QUORUM_VOTE, svotecount);
	if (clconf_err != CL_NOERROR) {
		switch (clconf_err) {
		case CL_BADNAME:
		default:
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		} /*lint !e788 */
	}

	/* Initialize the property name for the "path" for QDs. */
	(void) sprintf(pathprop, "path_%d", nodeid);

	/* Iterate through the list of shared quorum devices */
	qdevsi = currqdevsi =
	    clconf_cluster_get_quorum_devices(modified_clconf);
	while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {

		clconf_iter_advance(currqdevsi);

		/* Check to see if this QD is connected to the node. */
		propval = clconf_obj_get_property(qdev, pathprop);
		if (propval == NULL) {
			continue;
		}

		/* Get the current vote of the QD. */
		propval = clconf_obj_get_property(qdev, PROP_QDEV_QUORUM_VOTE);
		if (propval == NULL) {
			qd_vote = 0;
		} else {
			qd_vote = (uint_t)atoi(propval);
		}

		/*
		 * Set the QD vote count to current QD vote - change node vote.
		 * New QD vote = Old QD vote + (New node vote - old node vote).
		 * If the Old QD vote is 0 (offline QD) and we are resetting the
		 * node vote to a smaller number than before, the new QD vote
		 * needs to be 0 and not a negative integer.
		 */
		qd_effective_vote =  (int)(qd_vote + voteval - old_vote);

		if (qd_effective_vote < 0) {
			qd_effective_vote = 0;
		}

		(void) sprintf(svotecount, "%d", qd_effective_vote);

		clconf_err = clconf_obj_set_property(qdev,
		    PROP_QDEV_QUORUM_VOTE, svotecount);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}
	}

	rstatus = conf_check_transition(clconf, modified_clconf, NULL);

	/*
	 * We don't retry here if the config was stale. That will be handled
	 * by the caller.
	 */

cleanup:
	/* Release the iterator */
	if (qdevsi != (clconf_iter_t *)0)
		clconf_iter_release(qdevsi);

	/* Release modified cluster config */
	if (modified_clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)modified_clconf);
		modified_clconf = (clconf_cluster_t *)0;
	}

	return (rstatus);
}

/*
 * open_qd_lib
 *
 * Opens SCCONF_QD_LIB_DIR directory. If the c_flg is set,
 * returns a handle of the right library that contains the given
 * func_name with B_TRUE return code, based on the given
 * parameters; If c_flg is not set, only call the func_name
 * and not return the library handle.
 *
 * NOTE: The first two parameters are parameters for the func_name.
 *	Depends on what function the func_name is, these two
 *	parameters may have different meanings. Here the names
 *	of these two parameters don't mean too much.
 *
 * Possible return values:
 *	equal to zero   - error.
 *	non zero        - success. A library handle is returned.
 */
static void *open_qd_lib(char *scconf_name, char *qd_name,
    char *func_name, uint_t c_flg)
{
	DIR *dirp = NULL;
	char *pattern = "^scconf_.*.so.1$";
	regex_t re;
	struct dirent *dp = NULL;
	char dirbuf[BUFSIZ];
	char path_name[MAXPATHLEN];
	void *dl_handle = NULL;
	boolean_t (*scconf_get_qd_func)(char *, char *);
	boolean_t match_found = B_FALSE;

	/* Open the qd shared library directory */
	if ((dirp = opendir(SCCONF_QD_LIB_DIR)) == NULL)
		goto cleanup;

	/* Compile the pattern */
	if (regcomp(&re, pattern, REG_EXTENDED) != 0)
		goto cleanup;

	/* Open the library */
	while ((dp = readdir(dirp)) != NULL) {
		/* Check if the library opened is in the format required */
		if (regexec(&re, dp->d_name, (size_t)0, NULL, 0) != 0)
			continue;
		if (sscanf(dp->d_name, "scconf_%s", dirbuf) != 1)
			continue;

		(void) sprintf(path_name, "%s/%s", SCCONF_QD_LIB_DIR,
			dp->d_name);
		/*
		* Open the shared library with RTLD_NODELETE so it
		* won't be loaded repeatedly in subsequent calls to
		* this function.
		*/
		if ((dl_handle = dlopen(path_name,
		    RTLD_NOW|RTLD_NODELETE)) == (void *)0)
			goto cleanup;

		/* Find the address of the function */
		if ((scconf_get_qd_func = (boolean_t(*)(char *, char *))
		    dlsym(dl_handle, func_name)) != NULL) {

			/* Call the function */
			match_found = (*scconf_get_qd_func)(scconf_name,
			    qd_name);
			if (match_found) {
				/* Find the right lib. Return. */
				goto cleanup;
			} else {
				if (dl_handle != (void *)0) {
					(void) dlclose(dl_handle);
					dl_handle = NULL;
				}
				continue;
			}
		} else {
			if (dl_handle != (void *)0) {
				(void) dlclose(dl_handle);
				dl_handle = NULL;
				goto cleanup;
			}
		}
	}
cleanup:
	regfree(&re);
	if (dirp != NULL)
		(void) closedir(dirp);

	/* Close the library if c_flg is not set */
	if (!c_flg) {
		if (dl_handle != (void *)0)
			(void) dlclose(dl_handle);
		/* Set it so it won't be 0 which is error code */
		dl_handle = (void *)1;
	}

	return (dl_handle);
}

#ifdef WEAK_MEMBERSHIP
/*
 * scconf_change_global_quorum_prop
 *
 * This function changes the value for the given quorum property name if valid
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBADVALUE	- bad CCR data
 */
scconf_errno_t
scconf_change_global_quorum_prop(char *propname, char *propvalue,
	char **messages) {

    scconf_errno_t scconferr = SCCONF_NOERR;
    char lasts[BUFSIZ];
    char *lasts_p = &lasts[0];
    char *value = NULL;
    char *value_str = NULL;
    char buffer[SCCONF_MAXSTRINGLEN];
    scconf_cltr_handle_t handle = (scconf_cltr_handle_t *)0;
    boolean_t multiple_partitions;

    if (strncasecmp(propname, PROP_CLUSTER_PING_TARGETS,
	strlen(PROP_CLUSTER_PING_TARGETS)) == 0) {
	/*
	 * Check if we are adding or removing or setting the property
	 * value
	 */
	if (strchr(propname, '+') != NULL) {
	    scconferr = conf_addto_ping_targets(propvalue);
	    if (scconferr == SCCONF_EBADVALUE) {
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				"\"%s\" is already configured as \"%s\".\n"),
				propvalue, PROP_CLUSTER_PING_TARGETS);
			scconf_addmessage(buffer, messages);
		}
	    } else if (scconferr != SCCONF_NOERR) {
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				"Failed to add \"%s\" to \"%s\".\n"),
				propvalue, PROP_CLUSTER_PING_TARGETS);
			scconf_addmessage(buffer, messages);
		}
	    }
	} else if (strchr(propname, '-') != NULL) {
	    scconferr = conf_rmfrom_ping_targets(propvalue);
	    if (scconferr != SCCONF_NOERR) {
		/* Release old handle, if necessary */
		if (handle != (scconf_cltr_handle_t)0)
			scconf_cltr_releasehandle(handle);

		handle = (scconf_cltr_handle_t)0;

		/* Get handle */
		(void) scconf_cltr_openhandle(&handle);

		scconf_get_multiple_partitions(handle,
			&multiple_partitions);

		if (multiple_partitions == B_TRUE) {
			if (messages != (char **)0) {
			    (void) sprintf(buffer,
				dgettext(TEXT_DOMAIN,
				    "Failed to remove \"%s\" from \"%s\". "
				    "\"%s\" cannot be empty when \"%s\" "
				    "is set to \"true\".\n"),
				    propvalue, PROP_CLUSTER_PING_TARGETS,
				    PROP_CLUSTER_PING_TARGETS,
				    PROP_CLUSTER_MULTI_PARTITIONS);

				scconf_addmessage(buffer, messages);
			}
		} else {
			if (messages != (char **)0) {
			    (void) sprintf(buffer,
				dgettext(TEXT_DOMAIN,
				    "Failed to remove \"%s\" from \"%s\".\n"),
				    propvalue, PROP_CLUSTER_PING_TARGETS);

				scconf_addmessage(buffer, messages);
			}
		}
	    }
	} else {
		/*
		 * Re set the value of ping_targets with the newly
		 * specified values
		 */
		value_str = strdup(propvalue);
		if (value_str == NULL) {
			return (SCCONF_ENOMEM);
		}

		if (strlen(value_str) == 0) {
		    scconferr = conf_clear_ping_targets(B_FALSE);
		} else {
		    scconferr = conf_clear_ping_targets(B_TRUE);
		}

		if (scconferr != SCCONF_NOERR) {
		    /* Release old handle, if necessary */
		    if (handle != (scconf_cltr_handle_t)0)
			scconf_cltr_releasehandle(handle);

		    handle = (scconf_cltr_handle_t)0;

		    /* Get handle */
		    (void) scconf_cltr_openhandle(&handle);

		    scconf_get_multiple_partitions(handle,
				&multiple_partitions);

		    if (multiple_partitions == B_TRUE &&
			strlen(value_str) == 0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "\"%s\" cannot be empty when \"%s\" "
			    "is set to \"true\".\n"),
			    PROP_CLUSTER_PING_TARGETS,
			    PROP_CLUSTER_MULTI_PARTITIONS);
			scconf_addmessage(buffer, messages);
		    } else {
			if (messages != (char **)0) {
			    (void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				"Unable to set \"%s\" to empty.\n"),
				PROP_CLUSTER_PING_TARGETS);
			    scconf_addmessage(buffer, messages);
			}
		    }

		    return (scconferr);
		}

		value_str = strdup(propvalue);
		lasts_p = &lasts[0];
		value = (char *)strtok_r(value_str, ",", (char **)&lasts_p);

		while (value != NULL) {
		    scconferr = conf_addto_ping_targets(value);
		    value = (char *)strtok_r(NULL,
				",", (char **)&lasts_p);
		}
	    }

	    return (scconferr);
/* CSTYLED */
    } else if (strcasecmp(propname, PROP_CLUSTER_MULTI_PARTITIONS) == 0) {
	    scconferr =
		scconf_change_multiple_partitions(propvalue, messages);
	    return (scconferr);
/* CSTYLED */
    }

	/*
	 * Invalid property name for this quorum type
	 */

    return (SCCONF_EINVAL);
}



/*
 * scconf_change_multiple_partitions
 *
 * Change or set the default multiple_partitions value for
 * the cluster.
 *
 */
scconf_errno_t
scconf_change_multiple_partitions(char *value, char **messages)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clconf_errnum_t clconf_err = CL_NOERROR;
	scconf_cltr_handle_t handle = (scconf_cltr_handle_t *)0;
	int valid_prop_value = 0;

	/* Check arguments */
	if (value == NULL) {
		return (SCCONF_EINVAL);
	}

	if (strcasecmp(value, TRUE) == 0 ||
		strcasecmp(value, FALSE) == 0) {
		valid_prop_value = 1;
	}

	if (!valid_prop_value) {
		return (SCCONF_EINVAL);
	}

	/*
	 * If we generate a handle here, loop until commit succeeds,
	 * or until a fatal error is encountered.
	 */
	for (;;) {
		/* Release old handle, if necessary */
		if (handle != (scconf_cltr_handle_t)0)
			scconf_cltr_releasehandle(handle);
		handle = (scconf_cltr_handle_t)0;

		/* Get handle */
		(void) scconf_cltr_openhandle(&handle);

		/*
		 * Change the multiple partition setting
		 * Need to convert the value being passed here to lower
		 * case to make sure we write to CCR always the lower
		 * case "true" and "false" against the multiple_partitions
		 * property
		 */
		if (strcasecmp(value, "TRUE") == 0) {
		    clconf_err = clconf_obj_set_property(handle,
				PROP_CLUSTER_MULTI_PARTITIONS,
				"true");
		} else {
		    clconf_err = clconf_obj_set_property(handle,
				PROP_CLUSTER_MULTI_PARTITIONS,
				"false");
		}

		if (clconf_err != CL_NOERROR) {
			goto cleanup;
		}

		if (handle != (scconf_cltr_handle_t)0) {
			/* update */
			scconferr = scconf_cltr_updatehandle(handle, messages);

			/* if update returned okay, we are done */
			if (scconferr == SCCONF_NOERR) {
				clconf_cluster_reconfigure();
				break;
				/* if outdated handle, continue to retry */
			} else if (scconferr != SCCONF_ESTALE) {
				goto cleanup;
			}
		} else {
			break;
		}
	}

cleanup:
	/* Release the handle */
	if (handle != (scconf_cltr_handle_t)0)
		scconf_cltr_releasehandle(handle);

	return (scconferr);
}

/*
 * Get the list of weak membership targets configured for the cluster.
 *
 * The caller is responsible for freeing memory
 * associated with "pinglistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_ping_targets(clconf_cluster_t *clconf,
		scconf_namelist_t **pinglistp) {
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *propval = (char *)0;
	const char *value;
	scconf_namelist_t *pinglist = (scconf_namelist_t *)0;
	scconf_namelist_t **namelistp;
	char *laststrtok;
	char *ptr;

	/* Check argument */
	if (clconf == NULL)
		return (SCCONF_EINVAL);

	/* Get the ping_targets property */
	value = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_PING_TARGETS);

	/* Empty list? */
	if (value == NULL) {
		*pinglistp = (scconf_namelist_t *)0;
		return (SCCONF_NOERR);
	}

	/* Dup the list */
	propval = strdup(value);
	if (propval == NULL)
		return (SCCONF_ENOMEM);

	/* Set the ping target list */
	namelistp = &pinglist;
	ptr = (char *)propval;
	ptr = strtok_r(ptr, ",", &laststrtok);
	while (ptr != NULL) {
		*namelistp = (scconf_namelist_t *)calloc(1,
		    sizeof (scconf_namelist_t));
		if (*namelistp == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
		(*namelistp)->scconf_namelist_name = strdup(ptr);
		if ((*namelistp)->scconf_namelist_name == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
		namelistp = &(*namelistp)->scconf_namelist_next;
		ptr = strtok_r(NULL, ",", &laststrtok);
	}

cleanup:
	if (propval)
		free(propval);

	if (rstatus == SCCONF_NOERR) {
		*pinglistp = pinglist;
	} else {
		if (pinglist)
			scconf_free_namelist(pinglist);
	}

	return (rstatus);
}

/*
 * Get the value of multiple_partitions configured for the cluster.
 *
 * The caller is responsible for freeing memory
 * associated with "multiple_partitions".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */

scconf_errno_t
scconf_get_multiple_partitions(clconf_cluster_t *clconf,
	boolean_t *multiple_partitions) {

	const char *value;

	/* Check argument */
	if (clconf == NULL)
		return (SCCONF_EINVAL);

	/* Get the ping_targets property */
	value = clconf_obj_get_property((clconf_obj_t *)clconf,
		PROP_CLUSTER_MULTI_PARTITIONS);

	/* Empty list? */
	if (value == NULL) {
		*multiple_partitions = B_FALSE;
		return (SCCONF_NOERR);
	}

	if (strcasecmp(value, "TRUE") == 0)  {
		*multiple_partitions = B_TRUE;
	} else {
		*multiple_partitions = B_FALSE;
	}

	return (SCCONF_NOERR);
}

/*
 * conf_addto_ping_targets
 *
 * Add the target ip address to the list of targets which may be contacted
 * when a split brain occurs and the cluster is running WEAK membership
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EBADVALUE	- already in the list
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- unexpected or internal error
 */
scconf_errno_t
conf_addto_ping_targets(char *target)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_namelist_t *listptr, *pinglist = (scconf_namelist_t *)0;
	char *pinglistprop = (char *)0;
	clconf_errnum_t clconf_err;
	size_t len;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check argument */
	if (target == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Loop until commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Free old pinglist */
		if (pinglist != NULL) {
			scconf_free_namelist(pinglist);
			pinglist = (scconf_namelist_t *)0;
		}

		/* Free old pinglist property */
		if (pinglistprop != (char *)0) {
			free(pinglistprop);
			pinglistprop = (char *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* otherwise, add the new node to the list */
		/* Get the current weak membership target list */
		rstatus = scconf_get_ping_targets(
		    (clconf_cluster_t *)clconf, &pinglist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Allocate the pinglist property buffer */
		len = 0;
		for (listptr = pinglist;  listptr;
		    listptr = listptr->scconf_namelist_next) {
			len += strlen(listptr->scconf_namelist_name);
			++len;
		}

		len += strlen(target);
		++len;

		pinglistprop = (char *)calloc(1, len);
		if (pinglistprop == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add target to list */
		for (listptr = pinglist;  listptr;
		    listptr = listptr->scconf_namelist_next) {
			if (listptr->scconf_namelist_name == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			if (strcmp(listptr->scconf_namelist_name,
			    target) == 0) {
				rstatus = SCCONF_EBADVALUE;
				goto cleanup;
			}
			if (strcmp(listptr->scconf_namelist_name,
			    ".") == 0)
				continue;
			if (*pinglistprop != '\0')
				(void) strcat(pinglistprop, ",");
			(void) strcat(pinglistprop,
			    listptr->scconf_namelist_name);
		}

		if (*pinglistprop != '\0')
		    (void) strcat(pinglistprop, ",");

		(void) strcat(pinglistprop, target);

		/* Set the property */
		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_PING_TARGETS, pinglistprop);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returns okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	/* Free old pinglist */
	if (pinglist != NULL)
		scconf_free_namelist(pinglist);

	/* Free old pinglist property */
	if (pinglistprop != (char *)0)
		free(pinglistprop);

	return (rstatus);
}

/*
 * conf_rmfrom_ping_targets
 *
 * Remove a ip adress target from the list of ping targets
 *
 * If the list becomes completely empty, any node may add itself to the
 * cluster.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EBADVALUE	- not in the list
 *	SCCONF_ENOMEM		- not enouRgh memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- unexpected or internal error
 */
scconf_errno_t
conf_rmfrom_ping_targets(char *target)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_namelist_t *listptr, *pinglist = (scconf_namelist_t *)0;
	char *pinglistprop = (char *)0;
	clconf_errnum_t clconf_err;
	size_t len;
	int removed;
	boolean_t multiple_partitions;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check argument */
	if (target == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Loop until commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Free old pinglist */
		if (pinglist != NULL) {
			scconf_free_namelist(pinglist);
			pinglist = (scconf_namelist_t *)0;
		}

		/* Free old pinglist property */
		if (pinglistprop != (char *)0) {
			free(pinglistprop);
			pinglistprop = (char *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get the current ping target list */
		rstatus =
		    scconf_get_ping_targets(
			(clconf_cluster_t *)clconf,
					&pinglist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Allocate the pinglist property buffer */
		len = 0;
		for (listptr = pinglist;  listptr;
		    listptr = listptr->scconf_namelist_next) {
			len += strlen(listptr->scconf_namelist_name);
			++len;
		}
		if (len == 0) {
			rstatus = SCCONF_EBADVALUE;
			goto cleanup;
		}
		pinglistprop = (char *)calloc(1, len);
		if (pinglistprop == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Remove ipaddress target from list */
		removed = 0;
		for (listptr = pinglist;  listptr;
		    listptr = listptr->scconf_namelist_next) {
			if (strcmp(listptr->scconf_namelist_name,
			    target) == 0) {
				++removed;
			} else {
				if (*pinglistprop != '\0')
					(void) strcat(pinglistprop, ",");
				(void) strcat(pinglistprop,
				    listptr->scconf_namelist_name);
			}
		}
		if (!removed) {
			rstatus = SCCONF_ENOEXIST;
			goto cleanup;
		}

		/* If the list is empty, set property to NULL */
		if (*pinglistprop == '\0') {
			scconf_get_multiple_partitions(clconf,
					&multiple_partitions);
			if (multiple_partitions == B_TRUE) {
				free(pinglistprop);
				pinglistprop = NULL;
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
		}

		/* Set the property */
		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_PING_TARGETS, pinglistprop);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returns okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	/* Free old pinglist */
	if (pinglist != NULL)
		scconf_free_namelist(pinglist);

	/* Free old pinglist property */
	if (pinglistprop != (char *)0)
		free(pinglistprop);

	return (rstatus);
}

/*
 * conf_clear_ping_targets
 *
 * Clear the list of ping targets
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- unexpected or internal error
 */
scconf_errno_t
conf_clear_ping_targets(boolean_t force)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_errnum_t clconf_err;
	boolean_t multiple_partitions;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/*
	 * Loop until commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {
		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		scconf_get_multiple_partitions(clconf, &multiple_partitions);

		if (multiple_partitions == B_TRUE &&
			force == B_FALSE) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/* Clear the property */
		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_PING_TARGETS, NULL);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returns okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * execute_command
 *
 * Executes the given command with the given arguments.
*/
scconf_errno_t
execve_command(char *cli_cmd, char **cli_args, char **cli_envs) {
	pid_t cli_pid;
	int cli_stat;
	scconf_errno_t scconferr = SCCONF_NOERR;

	/* Check for inputs */
	if (!cli_cmd || (cli_args == (char **)0)) {
		return (SCCONF_EUNEXPECTED);
	}

	cli_pid = fork1();
	if (cli_pid == -1) {
		return (SCCONF_EUNEXPECTED);
	} else if (cli_pid == 0) {
		/* Child */

		if (cli_envs) {
			if (execve(cli_cmd, cli_args, cli_envs) == -1) {
				exit(SCCONF_EUNEXPECTED);
			}
		} else {
			if (execv(cli_cmd, cli_args) == -1) {
				exit(SCCONF_EUNEXPECTED);
			}
		}
	} else {
		/* Wait for command to finish */
		int status = 0;
		while ((status = waitpid(cli_pid, &cli_stat, 0)) != cli_pid) {
			if (status == -1 && errno == EINTR) {
				continue;
			} else {
				status = -1;
				break;
			}
		}
		if (status != cli_pid) {
			scconferr = SCCONF_EUNEXPECTED;
		} else {
			scconferr = WEXITSTATUS(cli_stat);
		}
	}

	return (scconferr);
}

/*
 * scconf_health_check_status
 *
 * Validate if the given ping_target argument is reachable from the given
 * cluster node
 *
 * Possible return values
 *
 *      SCCONF_NOERR - The ping target is reachable from the given nodename
 *      SCCONF_EINVAL - The ping target is not-reachable from the given nodename
 *      Any Other Value - Unknown status
 */

scconf_errno_t
scconf_health_check_status(char *nodename, char *ping_target) {
	char **cli_args = NULL;
	int rstatus = -1;
	char mynodename[BUFSIZ];
	scconf_errno_t scconferr = SCCONF_NOERR;

	cli_args = (char **)malloc(11 * sizeof (char *));

	cli_args[0] = SCRCMD;
	cli_args[1] = "-N";

	if (nodename != NULL) {
		cli_args[2] = strdup(nodename);
	} else  {
		/* Get the local node */
		mynodename[0] = '\0';
		(void) gethostname(mynodename, sizeof (mynodename));

		cli_args[2] = strdup(mynodename);
	}

	cli_args[3] = "scprivipadm";
	cli_args[4] = "runpingcmd";
	cli_args[5] = strdup(ping_target);
	cli_args[6] = "1000";
	cli_args[7] = ">";
	cli_args[8] = "/dev/null";
	cli_args[9] = "2>&1";
	cli_args[10] = NULL;

	rstatus = execve_command(SCRCMD, cli_args, NULL);

	free(cli_args[2]);
	free(cli_args[5]);

	free(cli_args);

	if (rstatus == 0) {
		return (SCCONF_NOERR);
	} else if (rstatus == 1) {
		return (SCCONF_EINVAL);
	}

	return (SCCONF_EUNKNOWN);
}

/*
 * scconf_check_hostname_in_files
 *
 * Validate if the given ping_target argument is having a mapping
 * in either one of the local files /etc/inet/ipnodes or
 * /etc/inet/hosts
 *
 * Possible return values
 *
 *      SCCONF_NOERR - Has a mapping for the given ping_target
 *      SCCONF_EINVAL - No mapping found for the given ping_target
 *      Any Other Value - Unknown status
 */
scconf_errno_t
scconf_check_hostname_in_files(char *nodename, char *hostname) {
	char **cli_args = NULL;
	int rstatus = -1;
	char mynodename[BUFSIZ];
	scconf_errno_t scconferr = SCCONF_NOERR;

	cli_args = (char **)malloc(10 * sizeof (char *));

	cli_args[0] = SCRCMD;
	cli_args[1] = "-N";

	if (nodename != NULL) {
		cli_args[2] = strdup(nodename);
	} else  {
		/* Get the local node */
		mynodename[0] = '\0';
		(void) gethostname(mynodename, sizeof (mynodename));

		cli_args[2] = strdup(mynodename);
	}

	cli_args[3] = "scprivipadm";
	cli_args[4] = "hostnamecheck";
	cli_args[5] = strdup(hostname);
	cli_args[6] = ">";
	cli_args[7] = "/dev/null";
	cli_args[8] = "2>&1";
	cli_args[9] = NULL;

	rstatus = execve_command(SCRCMD, cli_args, NULL);

	free(cli_args[2]);
	free(cli_args[5]);

	free(cli_args);

	if (rstatus != 0) {
		return (SCCONF_EINVAL);
	}

	return (SCCONF_NOERR);
}
#endif // WEAK_MEMBERSHIP
