/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scconf_misc.c	1.46	08/09/17 SMI"

/*
 * libscconf logical host config functions
 */

#include "scconf_private.h"

static scconf_errno_t conf_didname(char *globaldevicename, char *didname,
    uint_t sflag);

#define	LIBSCXCFG			"/usr/cluster/lib/libscxcfg.so.1"
#define	SCXCFG_OPEN			"scxcfg_open"
#define	SCXCFG_CLOSE			"scxcfg_close"
#define	SCXCFG_SETPROP_VALUE		"scxcfg_setproperty_value"
#define	SCXCFG_GETPROP_VALUE		"scxcfg_getproperty_value"
#define	SCXCFG_GETLISTPROP_VALUE	"scxcfg_getlistproperty_value"
#define	SCXCFG_GET_NODEID		"scxcfg_get_nodeid"
#define	SCXCFG_GET_NODENAME		"scxcfg_get_nodename"
#define	SCXCFG_ADD_NODE			"scxcfg_add_node"
#define	SCXCFG_DEL_NODE			"scxcfg_del_node"
#define	SCXCFG_FREELIST			"scxcfg_freelist"
#define	LIBFMM				"/usr/cluster/lib/libfmm.so.1"
#define	SACLMINITIALIZE			"saClmInitialize"
#define	SACLMFINALIZE			"saClmFinalize"
#define	SACLMCLUSTERNODEGET		"saClmClusterNodeGet"

/* Globals for farm (scxcfg, fmm) library */
void *DL_SCX_HANDLE;
uint_t ISLIBSCXCFG;
void *DL_FMM_HANDLE;
uint_t ISLIBFMM;
SaClmHandleT FMM_CLM_HANDLE;
uint_t ISFMMINIT;

/*
 * scconf_ismember
 *
 * Check to see if the given "nodeid" is an active cluster member.
 * Upon success, if the node is in the cluster, the location pointed to
 * by "flagp" is set to non-zero.   Otherwise, if the node is not
 * a cluster member, "flagp" is set to zero.
 *
 * If "nodeid" is set to zero, the current node is checked.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_EPERM            - not root
 *	SCCONF_ENOEXIST		- nodeid not found
 *	SCCONF_ENOCLUSTER	- cannot access cluster
 *      SCCONF_ENOMEM           - not enough memory
 *      SCCONF_EINVAL           - invalid argument
 */
scconf_errno_t
scconf_ismember(scconf_nodeid_t nodeid, uint_t *flagp)
{
	uint_t i;
	int bootflags;
	quorum_status_t *quorum_status;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/*
	 * First make sure that we are in the cluster.
	 *
	 * If the caller wants to know this node, and we are
	 * not in the cluster, then the flag is set to ZERO
	 * and the call returns without error.   But, if the
	 * caller wants to know about some other node, and this
	 * node is not a member of the cluster, SCCONF_ENOCLUSTER
	 * is returned.
	 */
	if ((cladm(CL_INITIALIZE, CL_GET_BOOTFLAG, &bootflags) != 0) ||
	    !(bootflags & CLUSTER_BOOTED)) {
		if (nodeid) {
			return (SCCONF_ENOCLUSTER);
		} else {
			*flagp = 0;
			return (SCCONF_NOERR);
		}
	}

	/* If nodeid was not given, get my nodeid */
	if (nodeid == (scconf_nodeid_t)0)
		if (cladm(CL_CONFIG, CL_NODEID, &nodeid) != 0)
			return (SCCONF_ENOCLUSTER);

	/* Initialize libclconf */
	if (scconf_cltr_openhandle((scconf_cltr_handle_t *)0) != SCCONF_NOERR)
		return (SCCONF_EUNEXPECTED);

	/* Get quorum status */
	if (cluster_get_quorum_status(&quorum_status) != 0)
		return (SCCONF_EINVAL);

	/* Find the nodeid */
	for (i = 0;  i < quorum_status->num_nodes;  ++i) {
		if (quorum_status->nodelist[i].nid == nodeid)
			break;
	}
	if (i == quorum_status->num_nodes) {
		cluster_release_quorum_status(quorum_status);
		return (SCCONF_ENOEXIST);
	}

	/* Set the flag based on whether or not the node is online */
	switch (quorum_status->nodelist[i].state) {
	case QUORUM_STATE_ONLINE:
		*flagp = 1;
		break;

	case QUORUM_STATE_OFFLINE:
		*flagp = 0;
		break;

	default:
		cluster_release_quorum_status(quorum_status);
		return (SCCONF_EUNEXPECTED);
	}

	cluster_release_quorum_status(quorum_status);

	return (SCCONF_NOERR);
}

/*
 * scconf_iseuropa
 *
 * Check if this an Europa cluster.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 */
scconf_errno_t
scconf_iseuropa(boolean_t *iseuropa)
{
	scconf_errno_t scconferr = SCCONF_NOERR;

	*iseuropa = B_FALSE;

	/* Initialize clconf */
	scconferr = clconf_lib_init();
	if (scconferr != SCCONF_NOERR) {
		return (scconferr);
	}

	*iseuropa = clconf_is_farm_mgt_enabled();

	return (scconferr);
}

/*
 * scconf_isfarmmember
 *
 * Check to see if the given "nodeid" is an active farm member.
 * Upon success, if the node is in the farm, the location pointed to
 * by "flagp" is set to non-zero.   Otherwise, if the node is not
 * a farm member, "flagp" is set to zero.
 *
 * scconf_fmm_initialize() must be called before calling this function.
 * Similary, scconf_fmm_finalize() must be called after this function.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_EPERM            - not root
 *	SCCONF_EUNEXPECTED	- Internal error
 *      SCCONF_EINVAL           - invalid argument
 */
scconf_errno_t
scconf_isfarmmember(scconf_nodeid_t nodeid, uint_t *flagp)
{
	scconf_errno_t rstatus;

	*flagp = 0;

	/* Must be root */
	if (getuid() != 0) {
		return (SCCONF_EPERM);
	}

	/* Check for argument */
	if (nodeid < 0) {
		return (SCCONF_EINVAL);
	}

	/* Check for farm membership */
	rstatus = scconf_saClmClusterNodeGet(nodeid);
	if (rstatus == SCCONF_EEXIST) {
		*flagp = 1;
		rstatus = SCCONF_NOERR;
	}

	return (rstatus);
}

/*
 * scconf_isserverfarm_mix
 *
 * Check to see if the given nodelist contains a mix of server and
 * farm nodes. Upon success, if the nodes are of both server and farm
 * types, the location pointed to by "flagp" is set to non-zero.
 * Otherwise, "flagp" is set to zero.
 *
 * This requires an external dlopen/dlclose of scxcfg library.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_EPERM            - not root
 *      SCCONF_ENOMEM		- not enough memory
 *      SCCONF_EUNEXPECTED      - Internal error
 *      SCCONF_ENOEXIST		- node (id/name) doesn't exist
 */
scconf_errno_t
scconf_isserverfarm_mix(char *nodelist, uint_t *flagp)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	scxcfg_error scxerr;
	scxcfg scxhdl;
	char *nodes = NULL;
	char *name = NULL;
	scconf_nodeid_t nodeid;
	char *nodename = NULL;
	boolean_t servertype = B_FALSE;
	boolean_t farmtype = B_FALSE;

	char *lasts[BUFSIZ];
	char **lasts_p = &lasts[0];

	*flagp = 0;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodelist == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	/* Get a copy of nodelist */
	nodes = strdup(nodelist);
	if (nodes == NULL) {
		return (SCCONF_ENOMEM);
	}

	name = strtok_r(nodes, ",", (char **)&lasts_p);
	while (name) {
		/*
		 * If name is a nodeid, try to get the corresponding
		 * nodename from the CCR/FARM configuration. This helps
		 * to confirm that the nodeid is a valid one and belongs
		 * server/farm type.
		 */
		if (scconf_str_to_nodeid(name, &nodeid)) {
			scconferr = scconf_get_nodename(nodeid, &nodename);
			if (scconferr == SCCONF_NOERR) {
				servertype = B_TRUE;
			} else {
				scconferr = scconf_get_farmnodename(nodeid,
				    &nodename);
				if (scconferr == SCCONF_NOERR) {
					farmtype = B_TRUE;
				}
			}
		} else {
			/*
			 * If it is a nodename, try to get the corresponding
			 * nodeid from the CCR/FARM configuration. This helps
			 * to confirm that the nodename is a valid one and
			 * belongs to server/farm type.
			 */
			scconferr = scconf_get_nodeid(name, &nodeid);
			if (scconferr == SCCONF_NOERR) {
				servertype = B_TRUE;
			} else {
				scconferr = scconf_get_farmnodeid(name,
				    &nodeid);
				if (scconferr == SCCONF_NOERR) {
					farmtype = B_TRUE;
				}
			}
		}

		/* Set the flag if nodelist has both server and farm */
		if (servertype && farmtype) {
			*flagp = 1;
			break;
		}

		/* Get next name from the comma list */
		name = strtok_r(NULL, ",", (char **)&lasts_p);
	}

cleanup:
	if (nodename) {
		free(nodename);
	}

	if (nodes) {
		free(nodes);
	}

	if (scconferr == SCCONF_ENOEXIST)
		scconferr = SCCONF_NOERR;

	return (scconferr);
}

/*
 * scconf_isservernode
 *
 * Check to see if the given node is a server node by getting its
 * nodeid from cluster configuration. Upon success, if the nodeid
 * exists for the specified node in CCR, the location pointed to by
 * "flagp" is set to non-zero. Otherwise, "flagp" is set to zero.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_EPERM            - not root
 *	SCCONF_EUNEXPECTED	- Internal error
 *      SCCONF_EINVAL           - invalid argument
 */
scconf_errno_t
scconf_isservernode(char *nodename, uint_t *flagp)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_nodeid_t nodeid;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check for arguments */
	if (nodename == NULL)
		return (SCCONF_EINVAL);

	*flagp = 0;

	/* See if the nodename exists in cluster config */
	scconferr = scconf_get_nodeid(nodename, &nodeid);
	if (scconferr == SCCONF_NOERR)
		*flagp = 1;

	return (scconferr);
}

/*
 * scconf_isfarmnode
 *
 * Check to see if the given node is a farm node by getting its
 * nodeid from farm configuration. Upon success, if the nodeid
 * exists for the specified node in FARM, the location pointed to
 * by "flagp" is set to non-zero. Otherwise, "flagp" is set to zero.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_EPERM            - not root
 *	SCCONF_EUNEXPECTED	- Internal error
 *      SCCONF_EINVAL           - invalid argument
 */
scconf_errno_t
scconf_isfarmnode(char *nodename, uint_t *flagp)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_nodeid_t nodeid;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check for arguments */
	if (nodename == NULL)
		return (SCCONF_EINVAL);

	*flagp = 0;

	/* See if the nodename exists in FARM config */
	scconferr = scconf_get_farmnodeid(nodename, &nodeid);
	if (scconferr == SCCONF_NOERR)
		*flagp = 1;

	return (scconferr);
}

/*
 * scconf_isfullmembership
 *
 * Check to see if all configured nodes, with the exception of "nodeid",
 * are currently members of the cluster.  If "nodeid" is set to zero,
 * all configured nodes must be cluster members.
 *
 * Upon success, if all requisite nodes are cluster members, the
 * location pointed to by "flagp" is set to non-zero.   Otherwise, it is
 * set to zero.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_EPERM            - not root
 *	SCCONF_ENOEXIST		- nodeid not found
 *	SCCONF_ENOCLUSTER	- cannot access cluster
 *      SCCONF_ENOMEM           - not enough memory
 *      SCCONF_EINVAL           - invalid argument
 */
scconf_errno_t
scconf_isfullmembership(scconf_nodeid_t nodeid, uint_t *flagp)
{
	int i;
	uint_t ismember;
	quorum_status_t *quorum_status;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (flagp == NULL)
		return (SCCONF_EINVAL);

	/* Make sure that we are in the cluster */
	(void) scconf_ismember(0, &ismember);
	if (!ismember)
		return (SCCONF_ENOCLUSTER);

	/* Initialize libclconf */
	if (scconf_cltr_openhandle((scconf_cltr_handle_t *)0) != SCCONF_NOERR)
		return (SCCONF_EUNEXPECTED);

	/* Get quorum status */
	if (cluster_get_quorum_status(&quorum_status) != 0)
		return (SCCONF_EINVAL);

	/* See if everyone other than "nodeid" is online */
	*flagp = 1;
	for (i = 0;  i < (int)quorum_status->num_nodes;  ++i) {
		if (quorum_status->nodelist[i].state != QUORUM_STATE_ONLINE &&
		    quorum_status->nodelist[i].nid != nodeid) {
			*flagp = 0;
			break;
		}
	}

	/* Release quorum status */
	cluster_release_quorum_status(quorum_status);

	return (SCCONF_NOERR);
}

/*
 * scconf_membercount
 *
 * Return the number of current members in the cluster.
 *
 * Possible return values:
 *
 *	-1			- error
 *	greater than zero	- number of cluster members
 */
int
scconf_membercount(void)
{
	uint_t i;
	int count;
	quorum_status_t *quorum_status;
	uint_t ismember = 0;

	/* Must be root */
	if (getuid() != 0)
		return (-1);

	/* Make sure that we are in the cluster */
	(void) scconf_ismember(0, &ismember);
	if (!ismember)
		return (-1);

	/* Initialize libclconf */
	if (scconf_cltr_openhandle((scconf_cltr_handle_t *)0) != SCCONF_NOERR)
		return (SCCONF_EUNEXPECTED);

	/* Get quorum status */
	if (cluster_get_quorum_status(&quorum_status) != 0)
		return (-1);

	/* Count the online nodes */
	count = 0;
	for (i = 0;  i < quorum_status->num_nodes;  ++i) {
		if (quorum_status->nodelist[i].state == QUORUM_STATE_ONLINE)
			++count;
	}

	/* Release quorum status */
	cluster_release_quorum_status(quorum_status);

	return (count);
}

/*
 * conf_nodecount
 *
 * Return the number of nodes configured in the given "clconf".
 *
 * Possible return values:
 *
 *	-1			- error
 *	greater than zero	- number of configured nodes
 */
int
conf_nodecount(clconf_cluster_t *clconf)
{
	clconf_iter_t *currnodesi, *nodesi;
	int nodecount;

	/* Must be root */
	if (getuid() != 0)
		return (-1);

	/* Check arguments */
	if (clconf == NULL)
		return (-1);

	/* Count the nodes */
	nodecount = 0;
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while (clconf_iter_get_current(currnodesi) != NULL) {
		++nodecount;
		clconf_iter_advance(currnodesi);
	}

	/* Release the iterator */
	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	return (nodecount);
}

/*
 * conf_qdevcount
 *
 * Return the number of quorum devices configured in the given "clconf".
 *
 * Possible return values:
 *
 *	-1			- error
 *	greater than -1		- number of configured quorum devices
 */
int
conf_qdevcount(clconf_cluster_t *clconf)
{
	clconf_iter_t *currqdevsi, *qdevsi;
	int qdevcount;

	/* Must be root */
	if (getuid() != 0)
		return (-1);

	/* Check arguments */
	if (clconf == NULL)
		return (-1);

	/* Count the quorum devices */
	qdevcount = 0;
	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
	while (clconf_iter_get_current(currqdevsi) != NULL) {
		++qdevcount;
		clconf_iter_advance(currqdevsi);
	}

	/* Release the iterator */
	if (qdevsi != (clconf_iter_t *)0)
		clconf_iter_release(qdevsi);

	return (qdevcount);
}

/*
 * conf_qdev_enabled_count
 *
 * Return the number of enabled (not in maint state) quorum devices
 * configured in the given "clconf".
 *
 * Possible return values:
 *
 *	-1			- error
 *	greater than -1		- number of configured quorum devices
 */
int
conf_qdev_enabled_count(clconf_cluster_t *clconf)
{
	clconf_iter_t *currqdevsi, *qdevsi;
	int qdevcount;
	clconf_obj_t *qdev;

	/* Must be root */
	if (getuid() != 0)
		return (-1);

	/* Check arguments */
	if (clconf == NULL)
		return (-1);

	/* Count the quorum devices */
	qdevcount = 0;
	qdevsi = currqdevsi = clconf_cluster_get_quorum_devices(clconf);
	while ((qdev = clconf_iter_get_current(currqdevsi)) != NULL) {

		if (clconf_obj_enabled(qdev))
			++qdevcount;

		clconf_iter_advance(currqdevsi);
	}

	/* Release the iterator */
	if (qdevsi != (clconf_iter_t *)0)
		clconf_iter_release(qdevsi);

	return (qdevcount);
}

/*
 * scconf_get_adapterlist
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scconf_errno_t
scconf_get_adapterlist(ulong_t netnumber, ulong_t netmask,
    scconf_cfg_adapter_t **adapterlistp)
{
	return (SCCONF_NOERR);
}

/*
 * scconf_strerr
 *
 * Map scconf_errno_t to a string.
 *
 * The supplied "errbuffer" should be of at least SCCONF_MAXSTRINGLEN
 * in length.
 */
void
scconf_strerr(char *errbuffer, scconf_errno_t err)
{
	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Check arguments */
	if (errbuffer == NULL)
		return;

	/* Make sure that the buffer is terminated */
	errbuffer[SCCONF_MAXSTRINGLEN - 1] = '\0';

	/* Find the string */
	switch (err) {
	case SCCONF_NOERR:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN, "no error"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EPERM:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "permission denied"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EEXIST:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "already exists"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_ENOEXIST:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "does not exist"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_ESTALE:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "stale handle"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EUNKNOWN:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "unknown type"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_ENOCLUSTER:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "cluster does not exist"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_ENODEID:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "node ID used in place of name"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EINVAL:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "invalid argument"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EUSAGE:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "usage error"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_ETIMEDOUT:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "timed out"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EINUSE:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "in use"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EBUSY:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "busy"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EINSTALLMODE:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "install mode"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_ENOMEM:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "not enough memory"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_ESETUP:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "setup attempt failed"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EUNEXPECTED:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "unexpected error"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EBADVALUE:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "bad value in configuration database"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EOVERFLOW:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "message buffer overflow"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EQUORUM:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "quorum could be compromised"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EAUTH:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "authentication error, access denied"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EIO:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "IO error"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_TM_EBADOPTS:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "bad transport properties"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_TM_EINVAL:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "invalid transport properties"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_DS_EINVAL:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "invalid state transition"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_DS_ENODEINVAL:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "invalid node"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_ERANGE:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "out of range"), SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_ERECONFIG:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "cluster is reconfiguring, please try again later"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_VP_MISMATCH:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "cluster upgrade is not committed"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_DUP_NASQD:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "one quorum device is already configured for this filer"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_NO_BINARY:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "cannot find the required binary"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EQD_SCRUB:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "cannot scrub the device. Check the device configuration"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_EQD_CONFIG:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "invalid quorum configuration"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;

	case SCCONF_DUP_QSQD:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "one quorum server on this host is already configured"),
		    SCCONF_MAXSTRINGLEN - 1);
		break;
	default:
		(void) strncpy(errbuffer, dgettext(TEXT_DOMAIN,
		    "unknown error"), SCCONF_MAXSTRINGLEN - 1);
		break;
	}
}

/*
 * conf_get_proplist
 *
 * Get the propery list associated with the given object.
 *
 * The caller is responsible for freeing memory
 * associated with "propslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_proplist(clconf_obj_t *clobj, char *exclude_props[],
    scconf_cfg_prop_t **propslistp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_iter_t *currpropsi, *propsi = (clconf_iter_t *)0;
	scconf_cfg_prop_t *propslist = (scconf_cfg_prop_t *)0;
	scconf_cfg_prop_t **nextp;
	clconf_obj_t *prop;
	const char *key, *value;
	char **strp;

	/* Check arguments */
	if (propslistp == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of properties */
	nextp = &propslist;
	propsi = currpropsi = clconf_obj_get_properties(clobj);
	while ((prop = clconf_iter_get_current(currpropsi)) != NULL) {

		/* Get key */
		key = clpl_get_name(prop);

		/* Check for exclusion */
		if (key != NULL && exclude_props != (char **)0) {
			for (strp = exclude_props;  *strp;  ++strp) {
				if (strcmp(*strp, key) == 0)
					break;
			}
			if (*strp) {
				clconf_iter_advance(currpropsi);
				continue;
			}
		}

		/* Add property */
		if (key != NULL) {

			/* Allocate struct for the next property */
			*nextp = (scconf_cfg_prop_t *)calloc(1,
			    sizeof (scconf_cfg_prop_t));
			if (*nextp == (scconf_cfg_prop_t *)0) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			/* key */
			(*nextp)->scconf_prop_key = strdup(key);
			if ((*nextp)->scconf_prop_key == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			/* kalue */
			value = clconf_obj_get_property(clobj, key);
			(*nextp)->scconf_prop_value = strdup(value);
			if ((*nextp)->scconf_prop_value == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			/* next */
			nextp = &(*nextp)->scconf_prop_next;
			clconf_iter_advance(currpropsi);
		}
	}

cleanup:
	/* Release iterator */
	if (propsi != (clconf_iter_t *)0)
		clconf_iter_release(propsi);

	if (rstatus == SCCONF_NOERR) {
		*propslistp = propslist;
	} else {
		conf_free_proplist(propslist);
	}

	return (rstatus);
}

/*
 * conf_propstr_to_proplist
 *
 * Return the list of properties (scconf_prop_t *) for the
 * given "properties" string of comma seperated properties
 * in "listp".
 *
 * If successful, the caller is responsible for freeing memory
 * returned in "listp" by calling conf_free_proplist().
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_propstr_to_proplist(char *properties, scconf_cfg_prop_t **listp)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	scconf_cfg_prop_t *list = (scconf_cfg_prop_t *)0;
	char *prop, *props = NULL;
	char *p, *last;
	scconf_cfg_prop_t **next;

	/* check arguments */
	if (listp == NULL) {
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

	/* if the properties string is empty, set the list to null */
	if (properties == NULL || *properties == '\0') {
		*listp = (scconf_cfg_prop_t *)0;
		return (SCCONF_NOERR);
	}

	/* dup the properties string */
	if ((props = strdup(properties)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* create the list */
	next = &list;
	prop = strtok_r(props, ",", &last);
	while (prop != NULL) {

		/* allocate the new prop struct */
		*next = (scconf_cfg_prop_t *)calloc(1,
		    sizeof (scconf_cfg_prop_t));
		if (*next == (scconf_cfg_prop_t *)0) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* if there is a value, dup it */
		if ((p = strchr(prop, '=')) != NULL) {
			*p++ = '\0';
			if (*p != '\0') {
				(*next)->scconf_prop_value = strdup(p);
				if ((*next)->scconf_prop_value == NULL) {
					rstatus = SCCONF_ENOMEM;
					goto cleanup;
				}
			}
		}

		/* dup the key */
		(*next)->scconf_prop_key = strdup(prop);
		if ((*next)->scconf_prop_key == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* reset next */
		next = &(*next)->scconf_prop_next;

		/* find the next property */
		prop = strtok_r(NULL, ",", &last);
	}

	/* got the list */
	*listp = list;
	rstatus = SCCONF_NOERR;

cleanup:
	/* get rid of props */
	if (props != NULL)
		free(props);

	/* if error, get rid of the list */
	if (rstatus != SCCONF_NOERR && list != (scconf_cfg_prop_t *)0)
		conf_free_proplist(list);

	return (rstatus);
}

/*
 * scconf_properties_to_hb
 *
 * Get the heart beat values from the "properties". Upon finished,
 * the heart beats could have the following values and what it means
 * to the caller:
 *
 * 	"default"	Needs to get the default global heart beat.
 *	'\0'		No input from the user.
 *	Some value specified by user
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_properties_to_hb(char **timeout, char **quantum, char *properties,
    char *transport_type)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *prop, *props = NULL;
	char *p, *last;
	char p_timeout[SCCONF_HB_BUFSIZ], p_quantum[SCCONF_HB_BUFSIZ];
	char *value = (char *)0;

	/* Check arguments */
	if (properties == NULL || *properties == '\0') {
		*timeout = NULL;
		*quantum = NULL;
		return (SCCONF_NOERR);
	}

	/* Create the property strings */
	if (transport_type != NULL && *transport_type != '\0') {
		(void) sprintf(p_timeout, "%s_%s",
		    transport_type, PROP_ADAPTER_HEARTBEAT_TIMEOUT);
		(void) sprintf(p_quantum, "%s_%s",
		    transport_type, PROP_ADAPTER_HEARTBEAT_QUANTUM);
	} else {
		(void) strcpy(p_timeout, PROP_ADAPTER_HEARTBEAT_TIMEOUT);
		(void) strcpy(p_quantum, PROP_ADAPTER_HEARTBEAT_QUANTUM);
	}

	/* Make a copy of the properties */
	if ((props = strdup(properties)) == NULL) {
		return (SCCONF_ENOMEM);
	}

	if ((prop = strtok_r(props, ",", &last)) == NULL) {
		rstatus = SCCONF_TM_EBADOPTS;
		goto cleanup;
	}

	/* Get the properties values */
	while (prop != NULL) {
		if ((p = strchr(prop, '=')) == NULL) {
			rstatus = SCCONF_TM_EBADOPTS;
			goto cleanup;
		}

		/* if there is a value, dup it */
		*p++ = '\0';
		value = (*p != '\0') ? strdup(p) : strdup("default");
		if (value == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Set the values */
		if (strcmp(p_timeout, prop) == 0) {
			if ((*timeout = strdup(value)) == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		} else if (strcmp(p_quantum, prop) == 0) {
			if ((*quantum = strdup(value)) == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* find the next property */
		prop = strtok_r(NULL, ",", &last);
	}

cleanup:
	/* Release props */
	if (props != NULL) {
		free(props);
	}

	if (value != NULL) {
		free(value);
	}

	return (rstatus);
}

/*
 * scconf_hb_to_properties
 *
 * Take the global heart beat settings into the "properties" format.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_hb_to_properties(char *timeout, char *quantum, char *properties,
    char *transport_type)
{
	/* Check arguments */
	if (timeout == NULL || *timeout == '\0') {
		return (SCCONF_EINVAL);
	}
	if (quantum == NULL || *quantum == '\0') {
		return (SCCONF_EINVAL);
	}

	if (transport_type == NULL) {
		return (SCCONF_EINVAL);
	}

	/* Create properties */
	(void) sprintf(properties, "%s_%s=%s,%s_%s=%s",
	    transport_type, PROP_ADAPTER_HEARTBEAT_TIMEOUT, timeout,
	    transport_type, PROP_ADAPTER_HEARTBEAT_QUANTUM, quantum);
	if (properties == NULL) {
		return (SCCONF_EUNEXPECTED);
	} else {
		return (SCCONF_NOERR);
	}
}

/*
 * conf_free_proplist
 *
 * Free all memory associated with the given property "list".
 */
void
conf_free_proplist(scconf_cfg_prop_t *list)
{
	register scconf_cfg_prop_t **listp;
	register scconf_cfg_prop_t *last;

	/* check arg */
	if (list == (scconf_cfg_prop_t *)0)
		return;

	/* free memory for each member of the list */
	listp = &list;
	while (*listp) {
		if ((*listp)->scconf_prop_key)
			free((*listp)->scconf_prop_key);
		if ((*listp)->scconf_prop_value)
			free((*listp)->scconf_prop_value);
		last = *listp;
		*listp = (*listp)->scconf_prop_next;
		free(last);
	}
}

/*
 * conf_set_state
 *
 * Possible return values:
 *
 *      SCCONF_NOERR		- success
 *      SCCONF_EINVAL		- invalid argument
 *      SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_set_state(clconf_obj_t *object, scconf_state_t state)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_errnum_t clconf_err = CL_NOERROR;

	switch (state) {
	case SCCONF_STATE_UNCHANGED:
		break;

	case SCCONF_STATE_ENABLED:
		clconf_err = clconf_obj_enable(object);
		break;

	case SCCONF_STATE_DISABLED:
		clconf_err = clconf_obj_disable(object);
		break;

	default:
		return (SCCONF_EINVAL);
	}
	if (clconf_err != CL_NOERROR)
		rstatus = SCCONF_EUNEXPECTED;

	return (rstatus);
}

/*
 * conf_cat_newmsg
 *
 * Concatenate the contents of the "msgbuff" to the location pointed to
 * by "messages".
 *
 * Each message in "messages" is always terminated by at least one newline.
 * And, a NULL terminates the end of the buffer.
 *
 * It is the callers responsibility to free memory allocated for "messages".
 */
void
conf_cat_newmsg(char *msgbuff, char **messages)
{
	char *ptr;

	/* If there is nothing to copy, just return */
	if (msgbuff == NULL || *msgbuff == '\0' ||
	    messages == NULL)
		return;

	/* Allocate memory, including space for newline and NULL */
	ptr = (char *)malloc((*messages ? strlen(*messages) : 0) +
	    strlen(msgbuff) + 2);
	if (ptr == NULL)
		return;
	*ptr = '\0';
	if (*messages)
		(void) strcat(ptr, *messages);
	*messages = ptr;

	/* Add the new message */
	(void) strcat(*messages, msgbuff);

	/* Add a newline, if not already there */
	if ((*messages)[strlen(*messages) - 1] != '\n')
		(void) strcat(*messages, "\n");
}

/*
 * conf_obj_check
 *
 * Call clconf_obj_check() on the object, then return scconf_errno_t.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR		- success
 *      SCCONF_EINVAL		- invalid argument
 *      SCCONF_ESETUP		- setup attempt failed
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *      SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_obj_check(clconf_obj_t *obj, char **messages)
{
	clconf_errnum_t clconf_err;
	char msgbuff[SCCONF_MSG_BUFSIZ];

	/* Check arguemnts */
	if (obj == NULL)
		return (SCCONF_EINVAL);

	/* Check object */
	*msgbuff = '\0';
	clconf_err = clconf_obj_check(obj, msgbuff, sizeof (msgbuff));
	if (*msgbuff && messages)
		conf_cat_newmsg(msgbuff, messages);
	if (clconf_err != CL_NOERROR) {
		switch (clconf_err) {
		case CL_INVALID_OBJ:
			return (SCCONF_ESETUP);

		case CL_BUF_TOO_SMALL:
			return (SCCONF_EOVERFLOW);

		default:
			return (SCCONF_EUNEXPECTED);
		} /*lint !e788 */
	}

	return (SCCONF_NOERR);
}

/*
 * conf_check_transition
 *
 * Call clconf_cluster_check_transition(), then return scconf_errno_t.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *      SCCONF_EINVAL		- invalid argument
 *      SCCONF_ESETUP		- setup attempt failed
 *	SCCONF_ENOCLUSTER	- cluster does not exist
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_ERANGE		- largest vote change is > 1
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *      SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_check_transition(clconf_cluster_t *existing_clconf,
    clconf_cluster_t *new_clconf, char **messages)
{
	clconf_errnum_t clconf_err;
	char msgbuff[SCCONF_MSG_BUFSIZ];

	/* Check arguments */
	if (existing_clconf == NULL || new_clconf == NULL)
		return (SCCONF_EINVAL);

	/* Check transition */
	*msgbuff = '\0';
	clconf_err = clconf_cluster_check_transition(existing_clconf,
	    new_clconf, msgbuff, sizeof (msgbuff), NULL);
	if (*msgbuff && messages)
		conf_cat_newmsg(msgbuff, messages);
	if (clconf_err != CL_NOERROR) {
		switch (clconf_err) {
		case CL_INVALID_OBJ:
		case CL_INVALID_TREE:
			return (SCCONF_ESETUP);

		case CL_BUF_TOO_SMALL:
			return (SCCONF_EOVERFLOW);

		case CL_NO_CLUSTER:
			return (SCCONF_ENOCLUSTER);

		case CL_QUORUM_CONFIG_WILL_LOSE_QUORUM:
			return (SCCONF_EQUORUM);

		case CL_QUORUM_CONFIG_LARGE_CHANGE:
		case CL_QUORUM_CONFIG_MULTIPLE_CHANGES:
			return (SCCONF_ERANGE);

		case CL_INVALID_CABLE_ENDPOINT:
			return (SCCONF_EINVAL);

		default:
			return (SCCONF_EUNEXPECTED);
		} /*lint !e788 */
	}

	return (SCCONF_NOERR);
}

/*
 * conf_cluster_commit
 *
 * Call clconf_cluster_commit(), then return scconf_errno_t.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ESTALE		- stale clconf handle
 *      SCCONF_EINVAL		- invalid argument
 *      SCCONF_ESETUP		- setup attempt failed
 *	SCCONF_ENOCLUSTER	- cluster does not exist
 *	SCCONF_EQUORUM		- would result in loss of quorum
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *      SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_EBUSY		- would result in node isolation
 */
scconf_errno_t
conf_cluster_commit(clconf_cluster_t *clconf, char **messages)
{
	clconf_errnum_t clconf_err;
	char msgbuff[SCCONF_MSG_BUFSIZ];

	/* Check arguments */
	if (clconf == NULL)
		return (SCCONF_EINVAL);

	/* Commit */
	*msgbuff = '\0';
	clconf_err = clconf_cluster_commit(clconf, msgbuff, sizeof (msgbuff));
	if (*msgbuff && messages)
		conf_cat_newmsg(msgbuff, messages);
	if (clconf_err != CL_NOERROR) {
		switch (clconf_err) {
		case CL_INVALID_OBJ:
		case CL_INVALID_TREE:
			return (SCCONF_ESETUP);

		case CL_NODE_ISOLATED:
			return (SCCONF_EBUSY);

		case CL_BUF_TOO_SMALL:
			return (SCCONF_EOVERFLOW);

		case CL_TREE_OUTDATED:
			return (SCCONF_ESTALE);

		case CL_NO_CLUSTER:
			return (SCCONF_ENOCLUSTER);

		case CL_QUORUM_CONFIG_WILL_LOSE_QUORUM:
			return (SCCONF_EQUORUM);

		case CL_INVALID_CABLE_ENDPOINT:
			return (SCCONF_EINVAL);

		default:
			return (SCCONF_EUNEXPECTED);
		} /*lint !e788 */
	}

	return (SCCONF_NOERR);
}

/*
 * scconf_addmessage
 *
 * Add a message to the "messages" buffer.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 */
void
scconf_addmessage(char *message, char **messages)
{
	char *tmpbuffer;
	size_t oldlen;
	size_t msg_len = 0;

	/* Check arguments */
	if (message == (char *)0 || messages == (char **)0)
		return;

	/*
	 * Attempt to allocate enlarged buffer.
	 * Add two extra bytes, in case trailing NEWLINE(s) are missing.
	 */
	oldlen = (*messages) ? strlen(*messages) : 0;
	msg_len = strlen(message);
	if (oldlen == 0) {
		tmpbuffer = calloc((size_t)msg_len + 2, sizeof (char));
	} else {
		tmpbuffer = (char *)realloc(*messages, oldlen + msg_len + 3);
	}
	if (tmpbuffer == NULL)
		return;
	*messages = tmpbuffer;

	/* NEWLINE on messages */
	if (oldlen != 0 &&
	    ((*messages)[oldlen - 1]) != '\n') {
		(void) strcat(*messages, "\n");
	}

	(void) strcat(*messages, message);

	/* Append NEWLINE if message doesn't end with it */
	if (msg_len != 0 && message[msg_len - 1] != '\n') {
		(void) strcat(*messages, "\n");
	}
}

/*
 * Free all memory associated with an "scconf_namelist" structure.
 */
void
scconf_free_namelist(scconf_namelist_t *namelst)
{
	scconf_namelist_t *last, *next;

	/* Check argument */
	if (namelst == NULL)
		return;

	/* Free memory associated with each name in the list */
	last = (scconf_namelist_t *)0;
	next = namelst;
	while (next != NULL) {

		/* free the last namelist struct */
		if (last != NULL)
			free(last);

		/* namelist name */
		if (next->scconf_namelist_name)
			free(next->scconf_namelist_name);

		last = next;
		next = next->scconf_namelist_next;
	}
	if (last != NULL)
		free(last);
}

/*
 * Free all memory associated with "f_property_t" structure.
 */
void
scconf_free_fpropertylist(f_property_t *namelist)
{
	f_property_t *lastp, *nextp;

	/* Check argument */
	if (namelist == NULL)
		return;

	/* Free memory associated with each name in the list */
	lastp = (f_property_t *)0;
	nextp = namelist;
	while (nextp != NULL) {

		/* free the last namelist struct */
		if (lastp != NULL)
			free(lastp);

		/* namelist name */
		if (nextp->value)
			free((char *)nextp->value);

		lastp = nextp;
		nextp = nextp->next;
	}
	if (lastp != NULL)
		free(lastp);
}

/*
 *
 * scconf_get_incarnation
 *
 * Get the incarnation number associated with the given handle.
 *
 * Possible return values:
 *
 *      incarnation number
 */
uint_t
scconf_get_incarnation(scconf_cltr_handle_t handle)
{

	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	scconf_errno_t	rstatus;
	uint_t	incn_num;

	if (handle == (scconf_cltr_handle_t *)0) {
		rstatus = scconf_cltr_openhandle(NULL);
		if (rstatus != SCCONF_NOERR)
			return ((uint_t)0);
		else {
			/* Get the clconf */
			clconf = clconf_cluster_get_current();
			if (clconf == NULL)
				return ((uint_t)0);
		}
	}

	/* get the incarnation number */
	incn_num = clconf_obj_get_incarnation((clconf_obj_t *)clconf);

	/* Release the clconf */
	clconf_obj_release((clconf_obj_t *)clconf);
	return (incn_num);
}

/*
 * conf_adaptercount
 *
 * Get the number of cluster transport adapters list for the given "node".
 *
 * Possible return values:
 *
 *	-1			- error
 *	greater than zero	- number of configured nodes
 *
 */
int
conf_adaptercount(clconf_node_t *node)
{
	clconf_iter_t *curradaptersi, *adaptersi = (clconf_iter_t *)0;
	int numadp;

	/* Must be root */
	if (getuid() != 0)
		return (-1);

	/* Check arguments */
	if (node == NULL)
		return (-1);

	/* Iterate through the list of cluster transport adapters */
	numadp = 0;
	adaptersi = curradaptersi = clconf_node_get_adapters(node);
	while ((clconf_iter_get_current(curradaptersi)) != NULL) {
		numadp++;
		clconf_iter_advance(curradaptersi);
	}

	if (adaptersi != (clconf_iter_t *)0)
		clconf_iter_release(adaptersi);

	return (numadp);
}

/*
 * scconf_didname
 *
 * Using the given "globaldevicename", return a copy of the full path
 * name of the rdsk device for the corresponding DID device in didname.
 *
 * If /dev/global/rdsk/<devicename> is given for the "globaldevicename",
 * it is converted to /dev/did/rdsk/<devicename>.   Only the last
 * component of the name (i.e., <devicename>) need be given. If slice number
 * is also given, it will be ignored.
 *
 * "didname" must be large enough to hold a copy of the new name.
 *
 * Possible return values:
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- bad devicename format
 *	SCCONF_EUNEXPECTED	- bad call or internal error
 */
scconf_errno_t
scconf_didname(char *globaldevicename, char *didname)
{
	return (conf_didname(globaldevicename, didname, 0));
}

/*
 * scconf_didname_slice
 *
 * Using the given "globaldevicename", return a copy of the full path
 * name of the rdsk device for the corresponding DID device in didname;
 * the didname includes a slice component.
 *
 * If /dev/global/rdsk/<devicename> is given for the "globaldevicename",
 * it is converted to /dev/did/rdsk/<devicename>.   Only the last
 * component of the name (i.e., <devicename>) need be given.   If the
 * slice is missing, slice two is used.
 *
 * "didname" must be large enough to hold a copy of the new name.
 *
 * Possible return values:
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- bad devicename format
 *	SCCONF_EUNEXPECTED	- bad call or internal error
 */
scconf_errno_t
scconf_didname_slice(char *globaldevicename, char *didname)
{
	return (conf_didname(globaldevicename, didname, 1));
}

/*
 * conf_didname
 *
 * Using the given "globaldevicename", return a copy of the full path
 * name of the rdsk device for the corresponding DID device in didname.
 *
 * If /dev/global/rdsk/<devicename> is given for the "globaldevicename",
 * it is converted to /dev/did/rdsk/<devicename>.   Only the last
 * component of the name (i.e., <devicename>) need be given.
 *
 * If sflag is zero, the slice is not included in the name.  If slfag
 * is non-zero, a slice is included;   slice 2 is used, unless a slice
 * number is included with the globaldevicename.
 *
 * "didname" must be large enough to hold a copy of the new name.
 *
 * Possible return values:
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- bad devicename format
 *	SCCONF_EUNEXPECTED	- bad call or internal error
 */
static scconf_errno_t
conf_didname(char *globaldevicename, char *didname, uint_t sflag)
{
	char name[MAXPATHLEN];
	char *ptr;
	int d, s;
	size_t len;

	/* Check arguments */
	if (globaldevicename == NULL || *globaldevicename == '\0' ||
	    didname == NULL)
		return (SCCONF_EUNEXPECTED);

	/* Initialize did device name */
	(void) strcpy(name, SCCONF_DID_RDSK);

	/* Begins with slash? */
	if (*globaldevicename == '/') {
		if (strncmp(globaldevicename, SCCONF_GLOBAL_RDSK,
		    strlen(SCCONF_GLOBAL_RDSK)) == 0) {
			ptr = globaldevicename + strlen(SCCONF_GLOBAL_RDSK);
		} else if (strncmp(globaldevicename, SCCONF_DID_RDSK,
		    strlen(SCCONF_DID_RDSK)) == 0) {
			ptr = globaldevicename + strlen(SCCONF_DID_RDSK);
		} else {
			return (SCCONF_EINVAL);
		}
	} else {
		ptr = globaldevicename;
	}

	/* d<N>s<S> */
	if (sscanf(ptr, "d%ds%d%n", &d, &s, &len) == 2 && len == strlen(ptr)) {
		if (sflag) {
			(void) sprintf(name, "%sd%ds%d", SCCONF_DID_RDSK, d, s);
		} else {
			(void) sprintf(name, "%sd%d", SCCONF_DID_RDSK, d);
		}

	/* d<N> */
	} else if (sscanf(ptr, "d%d%n", &d, &len) == 1 && len == strlen(ptr)) {
		if (sflag) {
			(void) sprintf(name, "%sd%ds2", SCCONF_DID_RDSK, d);
		} else {
			(void) sprintf(name, "%sd%d", SCCONF_DID_RDSK, d);
		}

	/* unexpected format */
	} else {
		return (SCCONF_EINVAL);
	}

	/* okay */
	(void) strcpy(didname, name);
	return (SCCONF_NOERR);
}

/*
 * Convert colon seperated names to list of names.
 */
scconf_errno_t
colonlist_to_namelist(char *list, scconf_namelist_t **namelist)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_namelist_t *nameptr = NULL;
	scconf_namelist_t *listhead = NULL;
	scconf_namelist_t *listtail = NULL;
	char *name = NULL;
	int i, j;
	int listcount = 0;

	char lasts[BUFSIZ];
	char *lasts_p = &lasts[0];

	/* Check for argument */
	if (namelist == NULL)
		return (SCCONF_EINVAL);

	/* Return NULL, if list is empty */
	if (list == NULL) {
		*namelist = NULL;
		return (SCCONF_NOERR);
	}

	/*
	 * For each entry in the list, create a new
	 * scconf_namelist_t struct. Fill in the name
	 * and attach to the list.
	 */
	name = (char *)strtok_r(list, ":", (char **)&lasts_p);
	while (name) {
		/* Allocate memory for scconf_namelist_t struct */
		nameptr = (scconf_namelist_t *)malloc(
		    sizeof (scconf_namelist_t));
		if (nameptr == NULL) {
			scconferr = SCCONF_ENOMEM;
			goto cleanup;
		}
		/* Allocate memory and copy the name */
		nameptr->scconf_namelist_name = strdup(name);
		if (nameptr->scconf_namelist_name == NULL) {
			scconferr = SCCONF_ENOMEM;
			goto cleanup;
		}

		nameptr->scconf_namelist_next = NULL;

		/* Attach to the namelist */
		if (listhead == NULL)
			listhead = nameptr;
		if (listtail != NULL)
			listtail->scconf_namelist_next = nameptr;
		listtail = nameptr;

		/* Get next entry from the list */
		name = (char *)strtok_r(NULL, ":", (char **)&lasts_p);
	}

	*namelist = listhead;

cleanup:
	if (scconferr != SCCONF_NOERR)
		scconf_free_namelist(listhead);

	return (scconferr);
}

/*
 * Convert colon seperated names to list of f_property names.
 */
scconf_errno_t
colonlist_to_fpropertylist(char *list, f_property_t **namelist)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	f_property_t *nameptr = NULL;
	f_property_t *listhead = NULL;
	f_property_t *listtail = NULL;
	char *name = NULL;
	int i, j;
	int listcount = 0;

	char lasts[BUFSIZ];
	char *lasts_p = &lasts[0];

	/* Check for argument */
	if (namelist == NULL)
		return (SCCONF_EINVAL);

	/* Return NULL, if list is empty */
	if (list == NULL) {
		*namelist = NULL;
		return (SCCONF_NOERR);
	}

	/*
	 * For each entry in the list, create a new
	 * f_property_t struct. Fill in the name
	 * and attach to the list.
	 */
	name = (char *)strtok_r(list, ":", (char **)&lasts_p);
	while (name) {
		/* Allocate memory for f_property_t struct */
		nameptr = (f_property_t *)malloc(
		    sizeof (f_property_t));
		if (nameptr == NULL) {
			scconferr = SCCONF_ENOMEM;
			goto cleanup;
		}
		/* Allocate memory and copy the name */
		nameptr->value = strdup(name);
		if (nameptr->value == NULL) {
			scconferr = SCCONF_ENOMEM;
			goto cleanup;
		}

		nameptr->next = NULL;

		/* Attach to the namelist */
		if (listhead == NULL)
			listhead = nameptr;
		if (listtail != NULL)
			listtail->next = nameptr;
		listtail = nameptr;

		/* Get next entry from the list */
		name = (char *)strtok_r(NULL, ":", (char **)&lasts_p);
	}

	*namelist = listhead;

cleanup:
	if (scconferr != SCCONF_NOERR) {
		scconf_free_fpropertylist(listhead);
	}

	return (scconferr);
}

/*
 * FMM library open
 */
scconf_errno_t
scconf_libfmm_open(void)
{
	ISLIBFMM = 0;

	DL_FMM_HANDLE = dlopen(LIBFMM, RTLD_LAZY);
	if (DL_FMM_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	ISLIBFMM = 1;

	return (SCCONF_NOERR);
}

/*
 * FMM library close
 */
void
scconf_libfmm_close(void)
{
	if (ISLIBFMM || DL_FMM_HANDLE != NULL) {
		(void) dlclose(DL_FMM_HANDLE);
	}

	/* Reset islibfmm and dl_fmm_handle */
	ISLIBFMM = 0;
	DL_FMM_HANDLE == NULL;
}

/*
 * Dummy clusterNodeGet callback routine for saClmClusterNodeGet()
 */
static void
clusterNodeGet(SaInvocationT invocation, SaClmClusterNodeT *clusterNode,
    SaErrorT error)
{
}

/*
 * Dummy clusterTrackStart callback routine for saClmClusterNodeGet()
 */
static void
clusterTrackStart(SaClmClusterNotificationT *notificationbuffer,
    SaUint32T numberOfItems,
    SaUint32T numberOfMembers,
    SaUint64T viewNumber,
    SaErrorT error)
{
}

/*
 * scconf wrapper to initialize FMM Clm handle.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 */

scconf_errno_t
scconf_fmm_initialize(void)
{

	SaErrorT result = SA_OK;
	SaClmCallbacksT callbacks;
	SaVersionT version;

	SaErrorT (*fn_saClmInitialize)(SaClmHandleT *handle,
	    SaClmCallbacksT *clmcallbacks, SaVersionT *version);

	ISFMMINIT = 0;

	if (!ISLIBFMM || DL_FMM_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_saClmInitialize = (SaErrorT(*)())dlsym(DL_FMM_HANDLE,
	    SACLMINITIALIZE);
	if (fn_saClmInitialize == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	callbacks.saClmClusterNodeGetCallback = clusterNodeGet;
	callbacks.saClmClusterTrackCallback = clusterTrackStart;
	version.releaseCode = 'A';
	version.major = 0x01;
	version.minor = 0xff;

	/* Initialize fmm library */
	result = (*fn_saClmInitialize)(&FMM_CLM_HANDLE, &callbacks, &version);
	if (result != SA_OK) {
		return (SCCONF_EUNEXPECTED);
	}

	ISFMMINIT = 1;

	return (SCCONF_NOERR);
}

/*
 * scconf wrapper to release the FMM Clm handle.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 */

scconf_errno_t
scconf_fmm_finalize(void)
{
	SaErrorT result = SA_OK;
	SaClmHandleT handle;

	SaErrorT (*fn_saClmFinalize)(SaClmHandleT handle);

	if (!ISLIBFMM || DL_FMM_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_saClmFinalize = (SaErrorT(*)())dlsym(DL_FMM_HANDLE,
	    SACLMFINALIZE);
	if (fn_saClmFinalize == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	/* Free fmm library handle */
	(*fn_saClmFinalize)(FMM_CLM_HANDLE);

	ISFMMINIT = 0;

	return (SCCONF_NOERR);
}

/*
 * scconf wrapper for (farm) FMM API saClmCluterNodeGet() that
 * gets the farm node dynamic status.
 * scconf_fmm_initialize should be called before this method is
 * called so as to initialize the saClmHandle, else this
 * function will return SCCONF_EUNEXPECTED. To free the handle
 * after this function scconf_fmm_finalize should be called.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 *      SCCONF_EEXIST		- node exists
 */

scconf_errno_t
scconf_saClmClusterNodeGet(scconf_nodeid_t nodeid)
{
	SaErrorT result = SA_OK;
	SaClmCallbacksT callbacks;
	SaVersionT version;
	SaClmClusterNodeT clusterNode;
	SaTimeT timeout = 5000000000;

	SaErrorT (*fn_saClmClusterNodeGet)(SaClmHandleT handle,
	    SaClmNodeIdT nodeid, SaTimeT timeout,
	    SaClmClusterNodeT *clusterNode);

	if (!ISLIBFMM || DL_FMM_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	if (!ISFMMINIT) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_saClmClusterNodeGet = (SaErrorT(*)())dlsym(DL_FMM_HANDLE,
	    SACLMCLUSTERNODEGET);
	if (fn_saClmClusterNodeGet == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	callbacks.saClmClusterNodeGetCallback = clusterNodeGet;
	callbacks.saClmClusterTrackCallback = clusterTrackStart;
	version.releaseCode = 'A';
	version.major = 0x01;
	version.minor = 0xff;

	result = (*fn_saClmClusterNodeGet)(FMM_CLM_HANDLE, nodeid, timeout,
	    &clusterNode);

	if (result == SA_OK && clusterNode.member) {
		return (SCCONF_EEXIST);
	}

	return (SCCONF_NOERR);
}

/*
 * Farm (scxcfg) library open
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 */
scconf_errno_t
scconf_libscxcfg_open(void)
{
	ISLIBSCXCFG = 0;

	DL_SCX_HANDLE = dlopen(LIBSCXCFG, RTLD_LAZY);
	if (DL_SCX_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	ISLIBSCXCFG = 1;

	return (SCCONF_NOERR);
}

/*
 * Farm (scxcfg) library close
 */
void
scconf_libscxcfg_close(void)
{
	if (ISLIBSCXCFG || DL_SCX_HANDLE != NULL) {
		(void) dlclose(DL_SCX_HANDLE);
	}

	/* Reset islibscxcfg and dl_scx_handle */
	ISLIBSCXCFG = 0;
	DL_SCX_HANDLE == NULL;
}

/*
 * scconf wrapper for scxcfg_open()
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 */
scconf_errno_t
scconf_scxcfg_open(scxcfg_t scxhdl, scxcfg_error_t error)
{
	int scxret;
	char *name = NULL;

	int (*fn_scxcfg_open)(scxcfg_t scxhdl, scxcfg_error_t error);

	if (!ISLIBSCXCFG || DL_SCX_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_scxcfg_open = (int(*)())dlsym(DL_SCX_HANDLE, SCXCFG_OPEN);
	if (fn_scxcfg_open == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	scxret = (*fn_scxcfg_open)(scxhdl, error);
	if (scxret != FCFG_OK) {
		return (SCCONF_EUNEXPECTED);
	}

	return (SCCONF_NOERR);
}

/*
 * scconf wrapper for scxcfg_close()
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 */
scconf_errno_t
scconf_scxcfg_close(scxcfg_t scxhdl)
{
	int (*fn_scxcfg_close)(scxcfg_t scxhdl);

	if (!ISLIBSCXCFG || DL_SCX_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_scxcfg_close = (int(*)())dlsym(DL_SCX_HANDLE, SCXCFG_CLOSE);
	if (fn_scxcfg_close == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	(*fn_scxcfg_close)(scxhdl);

	return (SCCONF_NOERR);
}

/*
 * scconf wrapper for scxcfg_setproperty_value()
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 */
scconf_errno_t
scconf_scxcfg_setproperty_value(scxcfg_t scxhdl,
    const char *context, const char *property, const char *value,
    scxcfg_error_t error)
{
	int scxret;

	int (*fn_setproperty_value)(scxcfg_t scxhdl, const char *context,
	    const char *property, const char *value, scxcfg_error_t error);

	if (!ISLIBSCXCFG || DL_SCX_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_setproperty_value = (int(*)())dlsym(DL_SCX_HANDLE,
	    SCXCFG_SETPROP_VALUE);
	if (fn_setproperty_value == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	scxret = (*fn_setproperty_value)(scxhdl, NULL, property, value,
	    error);
	if (scxret != FCFG_OK) {
		return (SCCONF_EUNEXPECTED);
	}

	return (SCCONF_NOERR);
}

/*
 * scconf wrapper for scxcfg_getproperty_value()
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 *	SCCONF_ENOEXIST		- property does not exist
 */
scconf_errno_t
scconf_scxcfg_getproperty_value(scxcfg_t scxhdl,
    const char *context, const char *property, const char *value,
    scxcfg_error_t error)
{
	int scxret;

	int (*fn_getproperty_value)(scxcfg_t scxhdl, const char *context,
	    const char *property, const char *value, scxcfg_error_t error);

	if (!ISLIBSCXCFG || DL_SCX_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_getproperty_value = (int(*)())dlsym(DL_SCX_HANDLE,
	    SCXCFG_GETPROP_VALUE);
	if (fn_getproperty_value == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	scxret = (*fn_getproperty_value)(scxhdl, NULL, property, value,
	    error);
	if (scxret != FCFG_OK) {
		if (scxret == FCFG_ENOTFOUND) {
			return (SCCONF_ENOEXIST);
		} else {
			return (SCCONF_EUNEXPECTED);
		}
	}

	return (SCCONF_NOERR);
}

/*
 * scconf wrapper for scxcfg_getlistproperty_value()
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 */
scconf_errno_t
scconf_scxcfg_getlistproperty_value(scxcfg_t scxhdl,
    const char *context, const char *property, f_property_t **listvalue,
    int *num, scxcfg_error_t error)
{
	int scxret;

	int (*fn_getlistproperty_value)(scxcfg_t scxhdl, const char *context,
	    const char *property, f_property_t **listvalue, int *num,
	    scxcfg_error_t error);

	if (!ISLIBSCXCFG || DL_SCX_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_getlistproperty_value = (int(*)())dlsym(DL_SCX_HANDLE,
	    SCXCFG_GETLISTPROP_VALUE);
	if (fn_getlistproperty_value == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	scxret = (*fn_getlistproperty_value)(scxhdl, NULL, property,
	    listvalue, num, error);
	if (scxret != FCFG_OK) {
		return (SCCONF_EUNEXPECTED);
	}

	return (SCCONF_NOERR);
}

/*
 * scconf wrapper for scxcfg_get_nodeid()
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 *      SCCONF_EEXIST		- nodeid found
 *      SCCONF_ENOEXIST		- nodeid not found
 */
scconf_errno_t
scconf_scxcfg_get_nodeid(scxcfg_t scxhdl,
    const char *nodename, scxcfg_nodeid_t *nodeid, scxcfg_error_t error)
{
	scconf_errno_t rstatus;
	int scxret;

	int (*fn_scxcfg_get_nodeid)(scxcfg_t scxhdl, const char *nodename,
	    scxcfg_nodeid_t *nodeid, scxcfg_error_t error);

	if (!ISLIBSCXCFG || DL_SCX_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_scxcfg_get_nodeid = (int(*)())dlsym(DL_SCX_HANDLE,
	    SCXCFG_GET_NODEID);
	if (fn_scxcfg_get_nodeid == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	scxret = (*fn_scxcfg_get_nodeid)(scxhdl, nodename, nodeid, error);
	if (scxret != FCFG_OK) {
		if (scxret == FCFG_ENOTFOUND) {
			return (SCCONF_ENOEXIST);
		} else {
			return (SCCONF_EUNEXPECTED);
		}
	}

	return (SCCONF_NOERR);
}

/*
 * scconf wrapper for scxcfg_get_nodename()
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 *      SCCONF_ENOEXIST		- nodename not found
 */
scconf_errno_t
scconf_scxcfg_get_nodename(scxcfg_t scxhdl,
    const scxcfg_nodeid_t nodeid, const char *nodename, scxcfg_error_t error)
{
	int scxret;

	int (*fn_scxcfg_get_nodename)(scxcfg_t scxhdl,
	    const scxcfg_nodeid_t nodeid, const char *nodename,
	    scxcfg_error_t error);

	if (!ISLIBSCXCFG || DL_SCX_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_scxcfg_get_nodename = (int(*)())dlsym(DL_SCX_HANDLE,
	    SCXCFG_GET_NODENAME);
	if (fn_scxcfg_get_nodename == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	scxret = (*fn_scxcfg_get_nodename)(scxhdl, nodeid, nodename, error);
	if (scxret != FCFG_OK) {
		if (scxret == FCFG_ENOTFOUND) {
			return (SCCONF_ENOEXIST);
		} else {
			return (SCCONF_EUNEXPECTED);
		}
	}

	return (SCCONF_NOERR);
}

/*
 * scconf wrapper for scxcfg_add_node()
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 */
scconf_errno_t
scconf_scxcfg_add_node(scxcfg_t scxhdl, const char *nodename,
    f_property_t *adapterlist, scxcfg_nodeid_t *nodeid, scxcfg_error_t error)
{
	int scxret;

	int (*fn_scxcfg_add_node)(scxcfg_t scxhdl,
	    const char *nodename, f_property_t *adapterlist,
	    scxcfg_nodeid_t *nodeid, scxcfg_error_t error);

	if (!ISLIBSCXCFG || DL_SCX_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_scxcfg_add_node = (int(*)())dlsym(DL_SCX_HANDLE, SCXCFG_ADD_NODE);
	if (fn_scxcfg_add_node == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	scxret = (*fn_scxcfg_add_node)(scxhdl, nodename, adapterlist,
	    nodeid, error);
	if (scxret != FCFG_OK) {
		return (SCCONF_EUNEXPECTED);
	}

	return (SCCONF_NOERR);
}

/*
 * scconf wrapper for scxcfg_del_node()
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 *      SCCONF_EINVAL		- Invalid node
 */
scconf_errno_t
scconf_scxcfg_del_node(scxcfg_t scxhdl,
    const char *nodename, scxcfg_error_t error)
{
	int scxret;

	int (*fn_scxcfg_del_node)(scxcfg_t scxhdl,
	    const char *nodename, scxcfg_error_t error);

	if (!ISLIBSCXCFG || DL_SCX_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_scxcfg_del_node = (int(*)())dlsym(DL_SCX_HANDLE, SCXCFG_DEL_NODE);
	if (fn_scxcfg_del_node == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	scxret = (*fn_scxcfg_del_node)(scxhdl, nodename, error);
	if (scxret != FCFG_OK) {
		if (scxret == FCFG_ENOTFOUND) {
			return (SCCONF_EINVAL);
		} else {
			return (SCCONF_EUNEXPECTED);
		}
	}

	return (SCCONF_NOERR);
}

/*
 * scconf wrapper for scxcfg_freelist()
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED	- Internal error
 */
scconf_errno_t
scconf_scxcfg_freelist(f_property_t *flist)
{
	int (*fn_scxcfg_freelist)(f_property_t *flist);

	if (!ISLIBSCXCFG || DL_SCX_HANDLE == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	fn_scxcfg_freelist = (int(*)())dlsym(DL_SCX_HANDLE, SCXCFG_FREELIST);
	if (fn_scxcfg_freelist == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	(*fn_scxcfg_freelist)(flist);

	return (SCCONF_NOERR);
}

/*
 * Get status string from scstat state code.
 */
void
scconf_get_default_status_string(const scstat_state_code_t state_code,
    char *text)
{
	/* Check arguments */
	if (text == NULL) {
		return;
	}

	/* I18N houskeeping */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Make sure that the buffer is terminated */
	text[SCSTAT_MAX_STRING_LEN - 1] = '\0';

	switch (state_code) {
	case SCSTAT_ONLINE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Online"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_OFFLINE:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Offline"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	case SCSTAT_UNKNOWN:
	default:
		(void) strncpy(text, dgettext(TEXT_DOMAIN, "Unknown"),
		    SCSTAT_MAX_STRING_LEN - 1);
		break;
	}
}

/*
 * scconf_str_to_nodeid()
 *
 * Input: string purporting to represent a nodeid
 *
 * Action: attempt to create a scconf_nodeid from the input
 *
 *  string must be of the form: "<n>" where <n> is an integer
 *
 * Output:
 *      If name can be converted to a scconf_nodeid_t,
 *      it is placed into scconf_nodeid.
 *
 * Possible return values:
 *      B_TRUE
 *      B_FALSE
 */
boolean_t
scconf_str_to_nodeid(char *name, scconf_nodeid_t *scconf_nodeid) {
	char *s;
	boolean_t result = B_TRUE;
	scconf_nodeid_t nodeid = 0;

	for (s = name;  *s;  ++s) {
		if (!isdigit(*s)) {
			result = B_FALSE;
			break;  /* can't be a nodeid */
		}
	}
	if (result == B_TRUE) {
		nodeid = (uint_t)atoi(name);
		if (nodeid < 1) {
			result = B_FALSE;
		} else {
			*scconf_nodeid = nodeid;
		}
	}
	return (result);
}

#if SOL_VERSION >= __s10
/*
 * Check to see if the current zone is a zone cluster.
 * If it is a zone cluster, "flagp" is set to B_TRUE.
 * Otherwise, it is set to B_FALSE.
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_EPERM            - not root
 *      SCCONF_ENOCLUSTER       - cannot access cluster
 *      SCCONF_ENOMEM           - not enough memory
 *      SCCONF_EINVAL           - invalid argument
 *	SCCONF_ENOCLUSTER	- not in cluster mode
 *	SCCONF_EUNEXPECTED	- Internal error
 */
scconf_errno_t
scconf_zone_cluster_check(boolean_t *flagp) {

	uint_t ismember = 0;
	uint_t cl_id = 0;
	uint_t err_no = 0;
	char *cl_name = (char *)0;

	/* Make sure that we are in the cluster */
	(void) scconf_ismember(0, &ismember);
	if (!ismember)
		return (SCCONF_ENOCLUSTER);

	cl_name = clconf_get_clustername();

	if (!cl_name)
		return (SCCONF_EUNEXPECTED);

	err_no = clconf_get_cluster_id(cl_name, &cl_id);

	if (err_no != 0) {
		/* If we are here then we are in a non-CZ */
		*flagp = B_FALSE;
		free(cl_name);
		return (SCCONF_NOERR);
	}

	/* If we are here it means we are either in the global zone or CZ */

	if (cl_id < MIN_CLUSTER_ID) {
		/* We are in the global zone */
		*flagp = B_FALSE;
	} else {
		*flagp = B_TRUE;
	}

	free(cl_name);

	return (SCCONF_NOERR);
}

/*
 * Get the Zone Cluster id of the given zone name.
 * If the given zone name is not a zone cluster, then this
 * method will return SCCONF_ENOEXIST.
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOCLUSTER	- cannot access cluster
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOEXIST		- no such clusterized zone found
 */
scconf_errno_t
scconf_get_zone_cluster_id(char *zonename,
    uint_t *cl_id) {

	uint_t ismember = 0;
	uint_t err_no = 0;

	if (zonename == NULL)
		return (SCCONF_ENOCLUSTER);

	/* Make sure that we are in the cluster */
	(void) scconf_ismember(0, &ismember);
	if (!ismember)
		return (SCCONF_ENOCLUSTER);

	err_no = clconf_get_cluster_id(zonename, cl_id);

	if ((err_no != 0) || (*cl_id < MIN_CLUSTER_ID))
		return (SCCONF_ENOEXIST);

	return (SCCONF_NOERR);
}

/*
 * Get the hostname of a zone-cluster-node given its node-id.
 * If the given zone cluster or the node-id does not exist, this
 * method will return SCCONF_ENOEXIST. This method will allocate
 * memory for "nodename". It is the callers responsibility to
 * free the memory. If "zcname" is NULL, this method will look
 * for the node-id in the current cluster.
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOEXIST		- no such zone cluster/nodeid found
 */
scconf_errno_t
scconf_get_zc_nodename_by_nodeid(char *zcname, uint_t nodeid,
    char **nodename) {

	clconf_cluster_t *cl_handle;
	const char *tmpName = NULL;

	/*
	 * Get the cluster handle
	 */
	if (zcname == NULL) {
		cl_handle = clconf_cluster_get_current();
	} else {
		cl_handle = clconf_cluster_get_vc_current(zcname);
	}

	if (cl_handle == NULL) {
		/*
		 * The zone cluster does not exist
		 */
		return (SCCONF_ENOEXIST);
	}

	/*
	 * Get the nodename
	 */
	tmpName = clconf_cluster_get_nodename_by_nodeid(cl_handle,
							nodeid);

	if (tmpName == NULL) {
		return (SCCONF_ENOEXIST);
	}

	*nodename = strdup(tmpName);

	if (*nodename == NULL) {
		return (SCCONF_ENOMEM);
	}

	return (SCCONF_NOERR);
}

/*
 * This method will check whether the given zone cluster consists
 * a given resource type. If hasrt is set to true, then it would
 * mean that the specified cluster has the RT. If NULL is specified
 * for "zcname", this method will check for the RT name in the
 * current cluster. Please note that the "rtname" specified here
 * must not contain any cluster scoping.
 * Possible return values:
 *
 *	SCCONF_NOERR		- no error
 *	SCCONF_EINVAL		- invalid cluster name or NULL rtname
 */
scconf_errno_t
scconf_does_zc_have_rt(char *zcname, char *rtname,
    boolean_t *hasrt) {

	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_rt_t *rt = (rgm_rt_t *)NULL;

	if (!rtname) {
		return (SCCONF_EINVAL);
	}

	scha_status = rgm_scrgadm_getrtconf(rtname, &rt, zcname);

	if (scha_status.err_code != SCHA_ERR_NOERR) {
		*hasrt = B_FALSE;
	} else {
		*hasrt = B_TRUE;
	}

	if (rt) {
		rgm_free_rt(rt);
	}

	return (SCCONF_NOERR);
}

/*
 * This method will check whether the given zone cluster consists
 * a given resource group. If hasrg is set to true, then it would
 * mean that the specified cluster has the RG. If NULL is specified
 * for "zcname", this method will check for the RG name in the
 * current cluster. Please note that the "rgname" specified here
 * must not contain any cluster scoping.
 * Possible return values:
 *
 *	SCCONF_NOERR		- no error
 *	SCCONF_EINVAL		- invalid cluster name or NULL rgname
 */
scconf_errno_t
scconf_does_zc_have_rg(char *zcname, char *rgname,
    boolean_t *hasrg) {

	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_rg_t *rgconf = (rgm_rg_t *)0;

	if (!rgname) {
		return (SCCONF_EINVAL);
	}

	scha_status = rgm_scrgadm_getrgconf(rgname, &rgconf, zcname);

	if (scha_status.err_code != SCHA_ERR_NOERR) {
		*hasrg = B_FALSE;
	} else {
		*hasrg = B_TRUE;
	}

	if (rgconf) {
		rgm_free_rg(rgconf);
	}

	return (SCCONF_NOERR);
}
/*
 * This method will check whether the given zone cluster hosts
 * a given resource. If hasrs is set to true, then it would
 * mean that the specified cluster has the R. If NULL is specified
 * for "zcname", this method will check for the resource name in the
 * current cluster. Please note that the "rsname" specified here
 * must not contain any cluster scoping.
 * This method will fetch all the RGs in the specified zone cluster
 * and check whether any of the RG has the resource. If at least
 * one of the RG has the resource, this method will set "hasrs"
 * to true, else it will be set to false.
 * Possible return values:
 *
 *	SCCONF_NOERR		- no error
 *	SCCONF_EINVAL		- invalid cluster name or NULL rsname
 *	SCCONF_EUNEXPECTED	- internal error
 */
scconf_errno_t
scconf_does_zc_have_rs(char *zcname, char *rsname,
    boolean_t *hasrs) {

	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	rgm_resource_t *rsconf = (rgm_resource_t *)0;
	char **rg_names = NULL;

	if (!rsname) {
		return (SCCONF_EINVAL);
	}

	scha_status = rgm_scrgadm_getrsrcconf(rsname, &rsconf, zcname);

	if (scha_status.err_code != SCHA_ERR_NOERR) {
		*hasrs = B_FALSE;
	} else {
		*hasrs = B_TRUE;
	}

	/* Free memory */
	if (rsconf) {
		rgm_free_resource(rsconf);
	}

	return (SCCONF_NOERR);
}
#endif
/*
 * Parses the complete object name, which has been scoped with
 * the cluster name (zone cluster name or global zone) and
 * returns the cluster name and the object name. This method
 * is required in a lot of places which take command line
 * arguments in the format "<zone cluster name>:<object name>".
 * This method is used to parse the node name, resource group
 * name, resource name, etc.
 * If there was no scoping in "fullname", this method just
 * duplicates the value in "fullname" and returns it as
 * "objname". It is upto the caller to free the memory.
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_parse_obj_name_from_cluster_scope(char *fullname, char **objname,
    char **clustername) {

	char *tmp_name, *tmp_full_name, *tmp_ptr;
	char errbuff[BUFSIZ];
	if (fullname == NULL) {
		return (SCCONF_EINVAL);
	}

	/* Check whether "fullname" was scoped to a zone cluster. */
	if (!strchr(fullname, ':')) {
		/* There was no scoping. */
		*objname = strdup(fullname);
		*clustername = NULL;
		return (SCCONF_NOERR);
	}

	/* If we are here, then there was scoping */
	tmp_full_name = strdup(fullname);
	tmp_name = tmp_full_name;

	tmp_ptr = strtok(tmp_name, ":");
	if (tmp_ptr != NULL) {
		*clustername = strdup(tmp_ptr);
	} else {
		free(tmp_full_name);
		scconf_strerr(errbuff, SCCONF_EINVAL);
		(void) fprintf(stderr, "%s.\n", errbuff);
		return (SCCONF_EINVAL);
	}

	tmp_ptr = strtok(NULL, ":");
	if (tmp_ptr != NULL) {
		*objname = strdup(tmp_ptr);
	} else {
		free(tmp_full_name);
		scconf_strerr(errbuff, SCCONF_EINVAL);
		(void) fprintf(stderr, "%s.\n", errbuff);
		return (SCCONF_EINVAL);
	}

	free(tmp_full_name);
	return (SCCONF_NOERR);
}
/*
 * Concats "clustername" and "objname" to the format
 * <clustername>:<objname>. If "clustername" is NULL, this method
 * just copies "objname" to "fullname". This method is opposite
 * of "scconf_parse_obj_name_from_cluster_scope" in terms of
 * functionality. It is upto the caller to free the memory.
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_add_cluster_scope_to_obj_name(char *objname, char *clustername,
    char **fullname) {

	char *tmp_name;
	int len_count = 2; /* 1 for the seperator and 1 for NULL */
	if (objname == NULL) {
		return (SCCONF_EINVAL);
	}

	/* Check whether "clustername" was specified. */
	if (clustername == NULL) {
		/* There was no cluster name specified. */
		*fullname = strdup(objname);
		return (SCCONF_NOERR);
	}

	/* If we are here, then scoping is required. */
	len_count += strlen(objname) + strlen(clustername);
	*fullname = (char *)calloc(len_count, 1);

	if (! *fullname) {
		return (SCCONF_ENOMEM);
	}

	strcat(*fullname, clustername);
	strcat(*fullname, ":");
	strcat(*fullname, objname);
	return (SCCONF_NOERR);
}

/*
 * Parses the complete RT name, which has been scoped with
 * the cluster name (zone cluster name or global zone) and
 * returns the cluster name and the RT name. This method
 * is required in a lot of places which take command line
 * arguments in the format "<zone cluster name>:<RT name>".
 * This method is used to parse only the RT name.
 * scconf_parse_obj_name_from_cluster_scope() cannot be used to
 * parse RT name since a ':' is allowed in an RT to specify
 * the version of the RT.
 *
 * Check for any colons in the given "fullname"
 * If there are no colons then we have no zone cluster
 * scoping for the RT and the given fullname is the RT
 * name without any version strings
 *
 * If there is a single colon we give preference to
 * check if the first part of "fullname" is a valid
 * zone cluster name, then we set the "clustername" to
 * the first token and remaining string to "rtname"
 * If the first token is not a valid zone cluster name
 * then we assume the entire "fullname" as an rtname.
 *
 * If there are more than 2 colons then we have a
 * fully qualified RT Name. Hence parse the string in
 * "fullname" and fill "clustername" and "rtname"
 * accordingly
 *
 * If there was no scoping in "fullname", this method just
 * duplicates the value in "fullname" and returns it as
 * "rtname". It is upto the caller to free the memory.
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_parse_rt_name_from_cluster_scope(char *fullname, char **rtname,
    char **clustername) {

	char *tmp_name = NULL, *tmp_full_name = NULL;
	char *tmp_token = NULL;
	int count, str_len;
	boolean_t invalid_vers = B_FALSE;
	char *rt_token[3];
	int colon_count = 0;
	uint_t cl_id = 0;
	uint_t clconf_err = 0;
	int i = 0;

	if (fullname == NULL) {
		return (SCCONF_EINVAL);
	}

	tmp_full_name = strdup(fullname);


	colon_count = 0;
	tmp_token = strtok(tmp_full_name, ":");
	while (tmp_token) {
		rt_token[colon_count] = strdup(tmp_token);
		colon_count++;
		tmp_token = strtok(NULL, ":");
	}

	/*
	 * Adjust the colon count
	 * Since colon count is 1 less than the number of
	 * tokens
	 */
	if (colon_count) {
		colon_count--;
	}

	switch (colon_count) {
		case 0:
			/*
			 * Check for any colons in the given "fullname"
			 * If there are no colons then we have no zone cluster
			 * scoping for the RT and the given fullname is the RT
			 * name without any version strings
			 */
			*rtname = strdup(rt_token[0]);
			*clustername = NULL;
			break;
		case 1:
			/*
			 * If there is a single colon we give preference to
			 * check if the first part of "fullname" is a valid
			 * zone cluster name, then we set the "clustername" to
			 * the first token and remaining string to "rtname"
			 * If the first token is not a valid zone cluster name
			 * then we assume the entire "fullname" as an rtname.
			 */

#if (SOL_VERSION >= __s10)
			clconf_err = clconf_get_cluster_id(rt_token[0], &cl_id);
			if ((clconf_err != 0) || (cl_id < MIN_CLUSTER_ID)) {
				/*
				 * No zone cluster exists in the given name
				 */
				*clustername = NULL;
				if (strncmp(rt_token[0], "global",
					strlen("global")) == 0) {
					*rtname = strdup(rt_token[1]);
				} else {
#endif
					tmp_token = (char *)
						malloc(sizeof (char) *
						(strlen(rt_token[0]) +
						strlen(rt_token[1]) + 2));
					strcpy(tmp_token, rt_token[0]);
					strcat(tmp_token, ":");
					strcat(tmp_token, rt_token[1]);
					*rtname = strdup(tmp_token);
					free(tmp_token);
#if (SOL_VERSION >= __s10)
				}
			} else {
				*clustername = strdup(rt_token[0]);
				*rtname = strdup(rt_token[1]);
			}
#endif
			break;
		case 2:
			/*
			 * If there are more than 2 colons then we have a
			 * fully qualified RT Name. Hence parse the string in
			 * "fullname" and fill "clustername" and "rtname"
			 * accordingly
			 */

			*clustername = strdup(rt_token[0]);
			tmp_token = (char *) malloc(sizeof (char) *
					(strlen(rt_token[1]) +
					strlen(rt_token[2]) + 2));
			strcpy(tmp_token, rt_token[1]);
			strcat(tmp_token, ":");
			strcat(tmp_token, rt_token[2]);
			*rtname = strdup(tmp_token);
			free(tmp_token);
			break;
	}

	/*
	 * Free rt_token
	 */

	for (i = 0; i <= colon_count; i++)
		if (rt_token[i])
			free(rt_token[i]);

	free(tmp_full_name);

	return (SCCONF_NOERR);
}
