/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scconf_transport.c	1.48	08/05/20 SMI"

/*
 * libscconf cluster transport config functions
 */

#include "scconf_private.h"
#include <sys/sc_syslog_msg.h>

#define	PHYS_NAME 0
#define	FULL_NAME 1

static scconf_errno_t conf_get_adapterobj(clconf_obj_t *node,
    char *adaptername, clconf_obj_t **adapterp);
static scconf_errno_t conf_get_cpointobj(clconf_cluster_t *clconf,
    char *cpointname, clconf_obj_t **cpointp);
static scconf_errno_t conf_get_cableobj(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint1, scconf_cltr_epoint_t *epoint2,
    clconf_obj_t **cablep);
static scconf_errno_t conf_create_port(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint, clconf_port_t **portp, char **messages);
static scconf_errno_t conf_create_port_adapter(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint, clconf_port_t **portp, char **messages);
static scconf_errno_t conf_create_port_cpoint(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint, clconf_port_t **portp, char **messages);
static char *conf_get_default_portname(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint);
static char *conf_get_default_portname_adapter(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint);
static char *conf_get_default_portname_cpoint(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint);
static char *conf_generate_default_portname(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint1, scconf_cltr_epoint_t *epoint2);
static int conf_epointcmp(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint1, scconf_cltr_epoint_t *epoint2);
static scconf_errno_t conf_isnode_isolated(clconf_cluster_t *clconf,
    clconf_obj_t *node);
static int conf_iscable_fullyenabled(clconf_cluster_t *clconf,
    clconf_obj_t *cable);
static scconf_errno_t conf_get_or_change_hb(
    scconf_cltr_handle_t *handle, char **timeout, char **quantum,
    char **msgbuffer, boolean_t change_hb);
static scconf_errno_t conf_get_adaptername(clconf_obj_t *, int, char **);

/*
 * scconf_cltr_openhandle
 *
 * Get a handle to use for cluster transport (cltr) update functions.
 *
 * All libscconf functions responsible for updating (add, change, remove)
 * the cluster transport configuration may be passed a handle returned
 * by this function.   When passed a NULL handle, these functions commit
 * before returning success.  Otherwise, changes are only committed to
 * the configuration image represented by the handle;  the handle itself
 * is not committed to the cluster until scconf_cltr_updatehandle() returns
 * with success.  Applications which use cluster transport handles
 * will want to batch all cluster transport related updates together;
 * interleaved updates could cause scconf_cltr_updatehandle() to fail.
 *
 * Not all update functions accept a handle, either because
 * they do not use the libclconf interfaces (e.g., global adapters,
 * dcs, and rgm) or because a functional update can depend on any
 * number of libclonf commits (e.g., quorum device/vote changes) for
 * success.
 *
 * Upon success, the new handle is returned in "handlep".
 *
 * If "handlep" is NULL, libclconf is inititialized without actually
 * creating a handle.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_cltr_openhandle(scconf_cltr_handle_t *handlep)
{
	clconf_cluster_t *current_clconf, *copy_clconf;
#ifdef DEBUG
	char *value;
	uint_t seconds;
#endif /* DEBUG */

	/* Initialize */
	if (clconf_lib_init() != 0)
		return (SCCONF_EUNEXPECTED);

	/* If the handle pointer is NULL, we are done */
	if (handlep == (scconf_cltr_handle_t *)0)
		return (SCCONF_NOERR);

	/* Get a copy of the current config */
	current_clconf = clconf_cluster_get_current();
	if (current_clconf == NULL)
		return (SCCONF_ENOCLUSTER);

	/* And, make a copy of that */
	copy_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
	    (clconf_obj_t *)current_clconf);
	if (copy_clconf == NULL) {
		clconf_obj_release((clconf_obj_t *)current_clconf);
		return (SCCONF_ENOMEM);
	}

	/* Release the original handle */
	clconf_obj_release((clconf_obj_t *)current_clconf);

	/* Set the handle to the copy */
	*handlep = (scconf_cltr_handle_t)copy_clconf;

#ifdef DEBUG
	/*
	 * If compiled for DEBUG, pause here for SCCONF_PAUSE seconds.
	 *
	 * This allows testing for proper handling of CCR database
	 * updates, when more than one process is racing to update
	 * the same CCR table in the same timeframe.   To test,
	 * launch one process with SCCONF_PAUSE set;  launch a second
	 * process during the pause with SCCONF_PAUSE clear.   Check the
	 * resulting CCR table for correctness.
	 */
	if ((value = getenv("SCCONF_PAUSE")) != NULL &&
	    (seconds = (uint_t)atoi(value)) > 0) {
		(void) printf("Pausing for %d seconds to allow for "
		    "update testing ... ", seconds);
		(void) fflush(stdout);
		(void) sleep(seconds);
		(void) printf("done.\n");
	}
#endif /* DEBUG */

	return (SCCONF_NOERR);
}

/*
 * scconf_cltr_updatehandle
 *
 * Commit all data associated with the given cluster transport (cltr)
 * handle.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ESTALE		- handle is stale
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_cltr_updatehandle(scconf_cltr_handle_t handle, char **messages)
{
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Commit */
	return (conf_cluster_commit(clconf, messages));
}

/*
 * scconf_cltr_releasehandle
 *
 * Release the handle cluster transport (cltr) handle.
 */
void
scconf_cltr_releasehandle(scconf_cltr_handle_t handle)
{
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;

	/* Release handle */
	if (handle != NULL)
		clconf_obj_release((clconf_obj_t *)clconf);
}

/*
 * scconf_add_cltr_adapter
 *
 * Create a new cluster transport adapter for a given node.
 *
 * The "nodename" may actually be either a node name or node ID.
 *
 * The "adaptername" takes the form <device-name><physical-unit>
 * (e.g., hme0).
 *
 * The "adaptername" should not be part of any ipmp group on "nodename"
 *
 * The adapter "properties" are given in a string as a comma and/or space
 * separated list of "<keyword>=<value>" pairs.  The "properties"
 * string conforms to getsubopt(3C) semantics.
 *
 * If "handle" is NULL, the change is committed upon successful
 * return of the function.  Otherwise, the change to the configuration
 * does not occur until scconf_cltr_updatehandle() returns with success.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- the given "adaptername" is already used
 *	SCCONF_EUNKNOWN		- the "adapter type" is unknown
 *	SCCONF_EUNKNOWN		- the "transport_type" is unknown
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOEXIST		- the "nodename" is unknown
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_TM_EBADOPTS	- bad "properties" for transport adapter
 *	SCCONF_TM_EINVAL	- other transport TM error
 */
scconf_errno_t
scconf_add_cltr_adapter(scconf_cltr_handle_t handle, uint_t noenable,
    char *nodename, char *transport_type, char *adaptername, int vlanid,
    char *properties, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	clconf_cluster_t *tmp_clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	clconf_obj_t *tmp_adapter, *adapter = (clconf_obj_t *)0;
	scconf_cfg_prop_t *proplist = (scconf_cfg_prop_t *)0;
	scconf_cfg_prop_t **proplistp;
	scconf_cltr_propdes_t *propdeslist = (scconf_cltr_propdes_t *)0;
	scconf_cltr_propdes_t *propdes;
	const char *propval;
	char *device_name = (char *)0;
	char *device_instance = (char *)0;
	char *laststrtok;
	char *ptr;
	char *hb_timeout = (char *)0;
	char *hb_quantum = (char *)0;
	char *prval = (char *)0;
	char p_timeout[SCCONF_HB_BUFSIZ], p_quantum[SCCONF_HB_BUFSIZ];
	clconf_obj_t *node;
	scconf_nodeid_t nodeid;
	size_t len;
	char buffer[SCCONF_MAXSTRINGLEN];
	char svlan[VLAN_STRMAX];
	div_t inst_vlan;
	int instance = 0;
	long tmp_instance = 0;
	int expanded_instance_num = 0;
	char *expanded_adaptername = (char *)0;
	char *endp;
	int vlan_prop_found = 0;
	int vlanid_specified = vlanid;
	int maxprivnets, numadp;
	boolean_t installmode = B_FALSE;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodename == NULL || transport_type == NULL || adaptername == NULL)
		return (SCCONF_EINVAL);

	/* Make sure there is no embedded ':' or '@' in adaptername */
	if (strchr(adaptername, ':') || strchr(adaptername, '@'))
		return (SCCONF_EINVAL);

	/* Split the adaptername into its two components */
	len = strlen(adaptername) + 1;
	device_name = (char *)calloc(1, len);
	device_instance = (char *)calloc(1, len);
	if (device_name == NULL || device_instance == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}
	rstatus = conf_split_adaptername(adaptername, device_name,
	    device_instance);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	if (*device_instance == '\0') {
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				"No device instance specified\n"));
			scconf_addmessage(buffer, messages);
		}
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

	tmp_instance = strtol(device_instance, &endp, 10);
	if (*endp != NULL) {
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "Invalid value \"%.256s\" given for device "
			    "instance.\n"), device_instance);
			scconf_addmessage(buffer, messages);
		}
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

	/* Allocate the cluster config, if necessary */
	if (clconf == (clconf_cluster_t *)0) {
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&tmp_clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;
		clconf = tmp_clconf;
	}

	propval = clconf_obj_get_property((clconf_obj_t *)clconf,
				PROP_CLUSTER_INSTALLMODE);
	if (propval && strcmp(propval, "enabled") == 0)
		installmode = B_TRUE;

	/* Check if this adapter part of an ipmp group */
	/* only if we are not in installmode */
	if (installmode == B_FALSE)
		rstatus = scconf_verify_in_ipmp(adaptername, nodename);
	else
		rstatus = SCCONF_ENOEXIST;

	switch (rstatus) {
	case SCCONF_NOERR:
		/* Exists part of ipmp group */
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "%s cannot be used as transport adapter "
			    "as it is part of an ipmp group on %s.\n"),
			    adaptername, nodename);
			scconf_addmessage(buffer, messages);
		}
		rstatus = SCCONF_EINVAL;
		goto cleanup;

	case SCCONF_ESETUP:
		if (messages != (char **)0) {
			/*
			 * The other node could be down or pnmd did not
			 * respond properly. We can block this
			 * operation. However for flexibility we just
			 * inform the user to check his configuration
			 */
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "Unable to verify if %s is part of any ipmp "
			    "group on node %s.\n"), adaptername, nodename);
			scconf_addmessage(buffer, messages);
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "Make sure %s is not part of any ipmp group.\n"),
			    adaptername);
			scconf_addmessage(buffer, messages);
		}
		break;

	case SCCONF_ENOEXIST:
		/* Does not exist in any ipmp group which is fine */
		rstatus = SCCONF_NOERR;
		break;

	case SCCONF_EUNEXPECTED:
	default:
			goto cleanup;
	}

	if (tmp_instance >
	    (VLAN_ID_MAX * VLAN_MULTIPLIER + VLAN_ZONE_START - 1)) {
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "Device instance \"%.256s\" is out of range.\n"),
			    device_instance);
			scconf_addmessage(buffer, messages);
		}
		rstatus = SCCONF_ERANGE;
		goto cleanup;
	}

	if ((tmp_instance >= VLAN_ZONE_START) && (vlanid != 0)) {
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "The device instance number specifies a VLAN "
			    "device, you cannot specify a separate VLAN ID."
			    "\n"));
			scconf_addmessage(buffer, messages);
		}
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

	if (vlanid != 0) {

		/* Expand the adaptername */
		expanded_adaptername = (char *)calloc(1, len + 6);
		if (expanded_adaptername == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
		(void) strncat(expanded_adaptername, device_name,
		    strlen(device_name));
		expanded_instance_num = vlanid * VLAN_MULTIPLIER +
		    (int)tmp_instance;
		(void) sprintf(expanded_adaptername + strlen(device_name), "%d",
		    expanded_instance_num);
		adaptername = expanded_adaptername;

	} else if (tmp_instance >= VLAN_ZONE_START) {

		/* Implicit vlan device, deduce vlan id and instance. */
		inst_vlan = div((int)tmp_instance, VLAN_MULTIPLIER);
		vlanid = inst_vlan.quot;
		instance = inst_vlan.rem;
		(void) memset(device_instance, 0, strlen(device_instance));
		(void) sprintf(device_instance, "%d", instance);
	}

	(void) memset(svlan, 0, VLAN_STRMAX);
	(void) sprintf(svlan, "%d", vlanid);

	/*
	 * Unless an already established "handle" is provided,
	 * loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)tmp_clconf);
			tmp_clconf = clconf = (clconf_cluster_t *)0;
		}
		if (existing_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)existing_clconf);
			existing_clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config, if necessary */
		if (clconf == (clconf_cluster_t *)0) {
			rstatus = scconf_cltr_openhandle(
			    (scconf_cltr_handle_t *)&tmp_clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			clconf = tmp_clconf;
		}

		/* Make a copy of the original */
		existing_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
		    (clconf_obj_t *)clconf);
		if (existing_clconf == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Get the nodeid */
		rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Find the node */
		rstatus = conf_get_nodeobj(clconf, nodeid, &node);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/*
		 * Get the max number of private networks allowed by
		 * the configuration's IP address range.
		 */
		rstatus = conf_get_maxprivnets(clconf, &maxprivnets);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		numadp = conf_adaptercount((clconf_node_t *)node);
		if (numadp == -1)
			goto cleanup;

		if (numadp + 1 > maxprivnets) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "Insufficient private IP address "
			    "range to add the adapter\n"));
			scconf_addmessage(buffer, messages);
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/* Make sure that the adapter is not already configured */
		rstatus = conf_get_adapterobj(node, adaptername,
		    &tmp_adapter);
		switch (rstatus) {
		case SCCONF_ENOEXIST:
			rstatus = SCCONF_NOERR;
			break;

		case SCCONF_NOERR:
		case SCCONF_EINUSE:
			rstatus = SCCONF_EEXIST;
			goto cleanup;

		default:
			goto cleanup;
		} /*lint !e788 */


		/*
		 * Get the list of adapter property descriptions.
		 * And, make sure the adapter type is known
		 * (scconf_get_cltr_adapter_propdeslist() returns
		 * SCCONF_EUNKNOWN for unknown adapter types).
		 */
		rstatus = scconf_get_cltr_adapter_propdeslist(adaptername,
		    &propdeslist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Create the new adapter */
		adapter = (clconf_obj_t *)clconf_adapter_create();
		if (adapter == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Set the adapter name itself */
		clconf_obj_set_name(adapter, adaptername);

		/* Add the adapter to the node */
		clconf_err = clconf_node_add_adapter(
		    (clconf_node_t *)node, (clconf_adapter_t *)adapter);

		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
				rstatus = SCCONF_EINVAL;
				goto cleanup;

			case CL_INVALID_ID:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Set the device name property */
		clconf_err = clconf_obj_set_property(adapter,
		    PROP_ADAPTER_DEVICE_NAME, device_name);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Set the device instance property */
		clconf_err = clconf_obj_set_property(adapter,
		    PROP_ADAPTER_DEVICE_INSTANCE, device_instance);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/*
		 * Set the vlan_id property if the property is defined for
		 * this adapter. We use the stored adapter properties here
		 * just to maintain the vlan supportability information.
		 */
		vlan_prop_found = 0;
		for (propdes = propdeslist;  propdes;
		    propdes = propdes->scconf_cltr_propdes_next) {
			if (strcmp(propdes->scconf_cltr_propdes_name,
			    PROP_ADAPTER_VLAN_ID) != 0)
				continue;
			vlan_prop_found = 1;
		}

		if ((vlan_prop_found == 0) && (vlanid != 0)) {
			if (messages != (char **)0) {
				if (vlanid_specified != 0) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Specified property \"vlanid\" is "
					    "not supported for the \"%.256s\" "
					    "device.\n"), device_name);
				} else {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "A VLAN device is implied but the "
					    "specified device \"%.256s\" does "
					    "not support VLANs.\n"),
					    device_name);
				}
				scconf_addmessage(buffer, messages);
			}
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		if (vlanid != 0) {
			clconf_err = clconf_obj_set_property(adapter,
			    PROP_ADAPTER_VLAN_ID, svlan);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}
		}

		/*
		 * Set the state
		 */

		if (noenable) {
			/* Set the state to disabled for add */
			rstatus = conf_set_state(adapter,
			    SCCONF_STATE_DISABLED);
		} else {
			/* Set the state to enabled for add */
			rstatus = conf_set_state(adapter, SCCONF_STATE_ENABLED);
		}
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/*
		 * Set both global and adapter type dependent properties
		 */

		/* Make sure transport type is valid for this adapter */
		ptr = NULL;
		for (propdes = propdeslist;  propdes;
		    propdes = propdes->scconf_cltr_propdes_next) {
			if (propdes->scconf_cltr_propdes_name == NULL ||
			    propdes->scconf_cltr_propdes_enumlist == NULL)
				continue;
			if (strcmp(propdes->scconf_cltr_propdes_name,
			    PROP_ADAPTER_TRANSPORT_TYPE) != 0)
				continue;
			ptr = propdes->scconf_cltr_propdes_enumlist;
			ptr = strtok_r(ptr, ":", &laststrtok);
			while (ptr != NULL) {
				if (strcmp(ptr, transport_type) == 0)
					break;
				ptr = strtok_r(NULL, ":", &laststrtok);
			}
		}

		if (ptr == NULL) {
			rstatus = SCCONF_EUNKNOWN;
			goto cleanup;
		}

		/* Set the transport type */
		clconf_err = clconf_obj_set_property(adapter,
		    PROP_ADAPTER_TRANSPORT_TYPE, transport_type);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Get user specified heart beat parameters */
		rstatus = scconf_properties_to_hb(&hb_timeout,
		    &hb_quantum, properties, transport_type);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/*
		 * The user specified heart beat property must have
		 * a value. If no value, above function
		 * scconf_properties_to_hb() set it to "default".
		 */
		if (hb_timeout &&
		    (strcmp(hb_timeout, "default") == 0)) {
			rstatus = SCCONF_TM_EBADOPTS;
			goto cleanup;
		}

		if (hb_quantum &&
		    (strcmp(hb_quantum, "default") == 0)) {
			rstatus = SCCONF_TM_EBADOPTS;
			goto cleanup;
		}

		/* May need to use the CCR heart beat setting */
		if (hb_timeout == NULL || *hb_timeout == '\0') {
			prval = (char *)clconf_obj_get_property(
			    (clconf_obj_t *)clconf,
			    PROP_CLUSTER_HB_TIMEOUT);
			if (prval != NULL) {
				hb_timeout = strdup(prval);
				if (hb_timeout == NULL) {
					rstatus = SCCONF_ENOMEM;
					goto cleanup;
				}
			}
		}

		if (hb_quantum == NULL || *hb_quantum == '\0') {
			prval = (char *)clconf_obj_get_property(
			    (clconf_obj_t *)clconf,
			    PROP_CLUSTER_HB_QUANTUM);
			if (prval != NULL) {
				hb_quantum = strdup(prval);
				if (hb_quantum == NULL) {
					rstatus = SCCONF_ENOMEM;
					goto cleanup;
				}
			}
		}

		/*
		 * Get the default heart beat setting if "hb_timeout",
		 * "hb_quantum" is '\0'; If it has the user specified
		 * settings, check if they are legal settings.
		 */
		rstatus = conf_get_or_change_hb((scconf_cltr_handle_t *)clconf,
		    &hb_timeout, &hb_quantum, messages, B_FALSE);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* get the adapter property list from the properties string */
		rstatus = scconf_propstr_to_proplist(properties, &proplist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		(void) sprintf(p_timeout, "%s_%s",
		    transport_type, PROP_ADAPTER_HEARTBEAT_TIMEOUT);
		(void) sprintf(p_quantum, "%s_%s",
		    transport_type, PROP_ADAPTER_HEARTBEAT_QUANTUM);

		/* Re-set heart beat settings in the properties list */
		for (proplistp = &proplist; *proplistp;
		    proplistp = &(*proplistp)->scconf_prop_next) {
			if ((*proplistp)->scconf_prop_key == NULL)
				continue;

			if (strcmp((*proplistp)->scconf_prop_key,
			    p_timeout) == 0) {
				if ((*proplistp)->scconf_prop_value) {
					free((*proplistp)->scconf_prop_value);
				}

				/* Set the new timeout */
				(*proplistp)->scconf_prop_value =
				    strdup(hb_timeout);
				if ((*proplistp)->scconf_prop_value == NULL) {
					rstatus = SCCONF_ENOMEM;
					goto cleanup;
				}
			}

			if (strcmp((*proplistp)->scconf_prop_key,
			    p_quantum) == 0) {
				if ((*proplistp)->scconf_prop_value) {
					free((*proplistp)->scconf_prop_value);
				}

				/* Set the new quantum */
				(*proplistp)->scconf_prop_value =
				    strdup(hb_quantum);
				if ((*proplistp)->scconf_prop_value == NULL) {
					rstatus = SCCONF_ENOMEM;
					goto cleanup;
				}
			}
		}

		/* make sure all required properties are included */
		for (propdes = propdeslist;  propdes;
		    propdes = propdes->scconf_cltr_propdes_next) {
			if (propdes->scconf_cltr_propdes_name == NULL ||
			    propdes->scconf_cltr_propdes_required == 0)
				continue;
			if (strcmp(propdes->scconf_cltr_propdes_name,
			    PROP_ADAPTER_TRANSPORT_TYPE) == 0)
				continue;

			/* Needs cleanup up as per bug ID 4257425 */
			if (strcmp(propdes->scconf_cltr_propdes_name,
			    PROP_ADAPTER_DEVICE_NAME) == 0)
				continue;

			for (proplistp = &proplist; *proplistp;
			    proplistp = &(*proplistp)->scconf_prop_next) {
				if ((*proplistp)->scconf_prop_key == NULL)
					continue;

				if (strcmp((*proplistp)->scconf_prop_key,
				    propdes->scconf_cltr_propdes_name) == 0) {
					break;
				}
			}
			if (*proplistp == (scconf_cfg_prop_t *)0) {
				rstatus = SCCONF_TM_EBADOPTS;
				goto cleanup;
			}
		}

		/*
		 * If property not required, and there is default, use it.
		 * The default values are entered into proplist.
		 */
		if ((rstatus = conf_use_adapter_default_properties(
		    propdeslist, &proplist)) != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Check, then set, adapter properties */
		for (proplistp = &proplist; *proplistp;
		    proplistp = &(*proplistp)->scconf_prop_next) {
			rstatus = scconf_check_cltr_property(
			    (*proplistp)->scconf_prop_key,
			    (*proplistp)->scconf_prop_value,
			    propdeslist);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

			clconf_err = clconf_obj_set_property(adapter,
			    (*proplistp)->scconf_prop_key,
			    (*proplistp)->scconf_prop_value);

			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
					rstatus = SCCONF_EINVAL;
					goto cleanup;

				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}
		}

		/* Free the property list */
		if (proplist != (scconf_cfg_prop_t *)0) {
			conf_free_proplist(proplist);
			proplist = (scconf_cfg_prop_t *)0;
		}

		/* Free the property description list */
		if (propdeslist != (scconf_cltr_propdes_t *)0) {
			scconf_free_propdeslist(propdeslist);
			propdeslist = (scconf_cltr_propdes_t *)0;
		}

		/* make sure that the adapter object is okay */
		rstatus = conf_obj_check(adapter, messages);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* if tmp_clconf is set, attempt commit here */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			rstatus = conf_cluster_commit(tmp_clconf, messages);

		/* otherwise, just check the transition */
		} else {
			rstatus = conf_check_transition(existing_clconf, clconf,
			    messages);
		}

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release temp cluster config */
	if (tmp_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)tmp_clconf);

	/* Release copy of cluster config */
	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	/* Free the property list */
	if (proplist != (scconf_cfg_prop_t *)0)
		conf_free_proplist(proplist);

	/* Free the property description list */
	if (propdeslist != (scconf_cltr_propdes_t *)0)
		scconf_free_propdeslist(propdeslist);

	/* Free heart beat */
	if (hb_timeout != (char *)0)
		free(hb_timeout);
	if (hb_quantum != (char *)0)
		free(hb_quantum);

	/* Free other temp space */
	if (device_name)
		free(device_name);
	if (device_instance)
		free(device_instance);
	if (expanded_adaptername)
		free(expanded_adaptername);

	return (rstatus);
}

/*
 * scconf_get_cltr_adapter_propdeslist
 *
 * Get the list of legal properties for a given adapter ("adaptername").
 * The "adaptername" takes the form <name><physical-unit> (e.g., hme0).
 *
 * The property list does include transport_type, which is not typically
 * set as a "property", but as a "trtype".
 *
 * Upon success, a pointer to the property list is returned in
 * "propdeslistp".   If there is no property description list, the value
 * pointed to by "propdeslistp" is set to NULL.
 *
 * The caller is responsible for freeing memory associated
 * with the "propdeslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EUNKNOWN		- the "adaptername" is of unknown type
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_get_cltr_adapter_propdeslist(char *adaptername,
    scconf_cltr_propdes_t **propdeslistp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *device_name = (char *)0;
	char *device_instance = (char *)0;
	scconf_cltr_propdes_t *propdeslist = (scconf_cltr_propdes_t *)0;
	scconf_cltr_propdes_t *propdes;
	char *laststrtok;
	char *ptr;
	size_t len;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (adaptername == NULL || propdeslistp == NULL)
		return (SCCONF_EINVAL);

	/* Split the adaptername into its two components */
	len = strlen(adaptername + 1);
	if (len < sizeof ("default"))
		len = sizeof ("default");
	device_name = (char *)calloc(1, len);
	device_instance = (char *)calloc(1, len);
	if (device_name == NULL || device_instance == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}
	rstatus = conf_split_adaptername(adaptername, device_name,
	    device_instance);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* See if it is a known device_name */
	rstatus = conf_get_propdeslist(CL_ADAPTER, "generic", &propdeslist);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;
	ptr = NULL;
	for (propdes = propdeslist;  propdes;
	    propdes = propdes->scconf_cltr_propdes_next) {
		if (propdes->scconf_cltr_propdes_name == NULL ||
		    propdes->scconf_cltr_propdes_enumlist == NULL ||
		    (strcmp(propdes->scconf_cltr_propdes_name,
		    PROP_ADAPTER_DEVICE_NAME) != 0))
			continue;

		ptr = propdes->scconf_cltr_propdes_enumlist;
		ptr = strtok_r(ptr, ":", &laststrtok);
		while (ptr != NULL) {
			if (strcmp(ptr, device_name) == 0)
				break;
			ptr = strtok_r(NULL, ":", &laststrtok);
		}
		if (ptr != NULL)
			break;
	}

	/* If not a known device, use the default */
	if (ptr == NULL) {
		(void) strcpy(device_name, "default");
	}

	/* Free the generic description list */
	if (propdeslist != NULL) {
		scconf_free_propdeslist(propdeslist);
		propdeslist = (scconf_cltr_propdes_t *)0;
	}

	/* Get the properties for the adapter */
	rstatus = conf_get_propdeslist(CL_ADAPTER, device_name, &propdeslist);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* If error, clear the list */
	if (rstatus != SCCONF_NOERR) {
		if (propdeslist != NULL) {
			scconf_free_propdeslist(propdeslist);
			propdeslist = (scconf_cltr_propdes_t *)0;
		}
	}

	/* Free device_name and device_instance */
	if (device_name != NULL)
		free(device_name);
	if (device_instance != NULL)
		free(device_instance);

	*propdeslistp = propdeslist;

	return (rstatus);
}

/*
 * scconf_change_cltr_adapter
 *
 * Change the properties or state associated with
 * a cluster transport adapter on a given node.
 *
 * The "properties" are given in a string as a comma and/or space
 * separated list of "<keyword>=<value>" pairs.  The "properties"
 * string conforms to getsubopt(3C) semantics.
 *
 * If "handle" is NULL, the change is committed upon successful
 * return of the function.  Otherwise, the change to the configuration
 * does not occur until scconf_cltr_updatehandle() returns with success.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- the adapter does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_TM_EBADOPTS	- bad "propterties" for transport type
 *	SCCONF_TM_EINVAL	- other transport TM error
 */
scconf_errno_t
scconf_change_cltr_adapter(scconf_cltr_handle_t handle, char *nodename,
    char *adaptername, int vlanid, char *properties,
    scconf_cltr_adapterstate_t state, uint_t cablechk, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	clconf_cluster_t *tmp_clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	scconf_cfg_prop_t *proplist = (scconf_cfg_prop_t *)0;
	scconf_cfg_prop_t **proplistp;
	scconf_cltr_propdes_t *propdeslist = (scconf_cltr_propdes_t *)0;
	clconf_obj_t *adapter;
	int unknowntype;
	clconf_obj_t *node;
	scconf_nodeid_t nodeid;
	const char *svlan = NULL;
	int stored_vlanid;
	uint_t statechk = 0;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodename == NULL || adaptername == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Unless an already established "handle" is provided,
	 * loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)tmp_clconf);
			tmp_clconf = clconf = (clconf_cluster_t *)0;
		}
		if (existing_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)existing_clconf);
			existing_clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config, if necessary */
		if (clconf == (clconf_cluster_t *)0) {
			rstatus = scconf_cltr_openhandle(
			    (scconf_cltr_handle_t *)&tmp_clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			clconf = tmp_clconf;
		}

		/* Make a copy of the original */
		existing_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
		    (clconf_obj_t *)clconf);
		if (existing_clconf == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Get the nodeid */
		rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Find the node */
		rstatus = conf_get_nodeobj(clconf, nodeid, &node);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Find the adapter */
		rstatus = conf_get_adapterobj(node, adaptername,
		    &adapter);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		svlan = clconf_obj_get_property(adapter, PROP_ADAPTER_VLAN_ID);
		if ((svlan == NULL) && (vlanid != 0)) {
			rstatus = SCCONF_ENOEXIST;
			goto cleanup;
		}

		if ((svlan != NULL) && (vlanid != 0)) {
			stored_vlanid = atoi(svlan);
			if (stored_vlanid != vlanid) {
				rstatus = SCCONF_ENOEXIST;
				goto cleanup;
			}
		}

		/* Make sure that the adapter is not being used by a cable */
		if (cablechk) {
			statechk = 1;
			rstatus = conf_check_notinuse_bycable(clconf, adapter,
			    statechk);
			if (rstatus != SCCONF_NOERR) {
				goto cleanup;
			}
		}

		/* Set the state */
		if (state != SCCONF_STATE_UNCHANGED) {
			rstatus = conf_set_state(adapter, state);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		/* Get the list of adapter property descriptions */
		unknowntype = 0;
		rstatus = scconf_get_cltr_adapter_propdeslist(adaptername,
		    &propdeslist);
		if (rstatus == SCCONF_EUNKNOWN) {
			++unknowntype;
			rstatus = SCCONF_NOERR;
		} else if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* get the adapter property list from the properties string */
		rstatus = scconf_propstr_to_proplist(properties, &proplist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Check, then set, adapter properties */
		for (proplistp = &proplist; *proplistp;
		    proplistp = &(*proplistp)->scconf_prop_next) {
			if (!unknowntype) {
				rstatus = scconf_check_cltr_property(
				    (*proplistp)->scconf_prop_key,
				    (*proplistp)->scconf_prop_value,
				    propdeslist);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
			}
			clconf_err = clconf_obj_set_property(adapter,
			    (*proplistp)->scconf_prop_key,
			    (*proplistp)->scconf_prop_value);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
					rstatus = SCCONF_EINVAL;
					goto cleanup;

				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}
		}

		/* Free the property list */
		if (proplist != (scconf_cfg_prop_t *)0) {
			conf_free_proplist(proplist);
			proplist = (scconf_cfg_prop_t *)0;
		}

		/* Free the property description list */
		if (propdeslist != (scconf_cltr_propdes_t *)0) {
			scconf_free_propdeslist(propdeslist);
			propdeslist = (scconf_cltr_propdes_t *)0;
		}

		/* make sure that the adapter object is okay */
		rstatus = conf_obj_check(adapter, messages);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* If disabled, make sure we haven't isolated a node */
		if (state == SCCONF_STATE_DISABLED) {
			rstatus = conf_check_isolation(existing_clconf, clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		/* if tmp_clconf is set, attempt commit here */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			rstatus = conf_cluster_commit(tmp_clconf, messages);

		/* otherwise, just check the transition */
		} else {
			rstatus = conf_check_transition(existing_clconf, clconf,
			    messages);
		}

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release temp cluster config */
	if (tmp_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)tmp_clconf);

	/* Release copy of cluster config */
	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	/* Free the property list */
	if (proplist != (scconf_cfg_prop_t *)0)
		conf_free_proplist(proplist);

	/* Free the property description list */
	if (propdeslist != (scconf_cltr_propdes_t *)0)
		scconf_free_propdeslist(propdeslist);

	return (rstatus);
}

/*
 * scconf_rm_cltr_adapter
 *
 * Remove a cluster transport adapter from the cluster configuration.
 *
 * If "handle" is NULL, the change is committed upon successful
 * return of the function.  Otherwise, the change to the configuration
 * does not occur until scconf_cltr_updatehandle() returns with success.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EINUSE		- one or more ports on the adapter are in use
 *	SCCONF_ENOEXIST		- the adapter does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_rm_cltr_adapter(scconf_cltr_handle_t handle, char *nodename,
    char *adaptername, int vlanid, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	clconf_cluster_t *tmp_clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	clconf_obj_t *adapter;
	clconf_obj_t *node;
	scconf_nodeid_t nodeid;
	const char *svlan = NULL;
	int stored_vlanid;
	uint_t statechk = 0;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodename == NULL || adaptername == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Unless an already established "handle" is provided,
	 * loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)tmp_clconf);
			tmp_clconf = clconf = (clconf_cluster_t *)0;
		}
		if (existing_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)existing_clconf);
			existing_clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config, if necessary */
		if (clconf == (clconf_cluster_t *)0) {
			rstatus = scconf_cltr_openhandle(
			    (scconf_cltr_handle_t *)&tmp_clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			clconf = tmp_clconf;
		}

		/* Make a copy of the original */
		existing_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
		    (clconf_obj_t *)clconf);
		if (existing_clconf == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Get the nodeid */
		rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Find the node */
		rstatus = conf_get_nodeobj(clconf, nodeid, &node);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Find the adapter */
		rstatus = conf_get_adapterobj(node, adaptername,
		    &adapter);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		svlan = clconf_obj_get_property(adapter, PROP_ADAPTER_VLAN_ID);
		if ((svlan == NULL) && (vlanid != 0)) {
			rstatus = SCCONF_ENOEXIST;
			goto cleanup;
		}

		if ((svlan != NULL) && (vlanid != 0)) {
			stored_vlanid = atoi(svlan);
			if (stored_vlanid != vlanid) {
				rstatus = SCCONF_ENOEXIST;
				goto cleanup;
			}
		}

		/* Make sure that the adapter is not being used by a cable */
		rstatus = conf_check_notinuse_bycable(clconf, adapter,
		    statechk);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Remove the adapter from the node */
		clconf_err = clconf_node_remove_adapter((clconf_node_t *)node,
		    (clconf_adapter_t *)adapter);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_INVALID_TREE:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Make sure we haven't isolated a node */
		rstatus = conf_check_isolation(existing_clconf, clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* if tmp_clconf is set, attempt commit here */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			rstatus = conf_cluster_commit(tmp_clconf, messages);

		/* otherwise, just check the transition */
		} else {
			rstatus = conf_check_transition(existing_clconf, clconf,
			    messages);
		}

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release temp cluster config */
	if (tmp_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)tmp_clconf);

	/* Release copy of cluster config */
	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	return (rstatus);
}

/*
 * scconf_add_cltr_cpoint
 *
 * Create a new off-node cluster transport connection point
 * (blackbox device).
 *
 * The "properties" are given in a string as a comma and/or space
 * separated list of "<keyword>=<value>" pairs.  The "properties"
 * string conforms to getsubopt(3C) semantics.
 *
 * If "handle" is NULL, the change is committed upon successful
 * return of the function.  Otherwise, the change to the configuration
 * does not occur until scconf_cltr_updatehandle() returns with success.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- the given "cpointname" is already used
 *	SCCONF_EUNKNOWN		- the "cpoint_type" is unknown
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_TM_EBADOPTS	- bad "propterties" for the connection type
 *	SCCONF_TM_EINVAL	- other transport TM error
 */
scconf_errno_t
scconf_add_cltr_cpoint(scconf_cltr_handle_t handle, uint_t noenable,
    char *cpoint_type, char *cpointname, char *properties, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	clconf_cluster_t *tmp_clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	clconf_obj_t *tmp_cpoint, *cpoint = (clconf_obj_t *)0;
	scconf_cfg_prop_t *proplist = (scconf_cfg_prop_t *)0;
	scconf_cfg_prop_t **proplistp;
	scconf_cltr_propdes_t *propdeslist = (scconf_cltr_propdes_t *)0;
	scconf_cltr_propdes_t *propdes;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (cpoint_type == NULL || cpointname == NULL)
		return (SCCONF_EINVAL);

	/* Make sure there is no embedded ':' or '@' in cpointname */
	if (strchr(cpointname, ':') || strchr(cpointname, '@'))
		return (SCCONF_EINVAL);

	/*
	 * Unless an already established "handle" is provided,
	 * loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)tmp_clconf);
			tmp_clconf = clconf = (clconf_cluster_t *)0;
		}
		if (existing_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)existing_clconf);
			existing_clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config, if necessary */
		if (clconf == (clconf_cluster_t *)0) {
			rstatus = scconf_cltr_openhandle(
			    (scconf_cltr_handle_t *)&tmp_clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			clconf = tmp_clconf;
		}

		/* Make a copy of the original */
		existing_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
		    (clconf_obj_t *)clconf);
		if (existing_clconf == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Make sure that the cpoint is not already configured */
		rstatus = conf_get_cpointobj(clconf, cpointname,
		    &tmp_cpoint);
		switch (rstatus) {
		case SCCONF_ENOEXIST:
			rstatus = SCCONF_NOERR;
			break;

		case SCCONF_NOERR:
			rstatus = SCCONF_EEXIST;
			goto cleanup;

		default:
			goto cleanup;
		} /*lint !e788 */

		/*
		 * Get the list of cpoint property descriptions.
		 * And, make sure the cpoint type is known
		 * (scconf_get_cltr_cpoint_propdeslist() returns
		 * SCCONF_EUNKNOWN for unknwon cpoint types).
		 */
		rstatus = scconf_get_cltr_cpoint_propdeslist(cpoint_type,
		    &propdeslist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Create the new cpoint */
		cpoint = (clconf_obj_t *)clconf_bb_create();
		if (cpoint == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* add the cpoint to the cluster */
		clconf_err = clconf_cluster_add_bb(
		    clconf, (clconf_bb_t *)cpoint);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
				rstatus = SCCONF_EINVAL;
				goto cleanup;

			case CL_INVALID_ID:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Add the cpoint name */
		clconf_obj_set_name(cpoint, cpointname);

		if (noenable) {
			/* Set the state to disabled for add */
			rstatus = conf_set_state(cpoint,
			    SCCONF_STATE_DISABLED);
		} else {
			/* Set the state to enabled for add */
			rstatus = conf_set_state(cpoint, SCCONF_STATE_ENABLED);
		}
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Set the type */
		clconf_bb_set_type((clconf_bb_t *)cpoint, cpoint_type);

		/* get the cpoint property list from the properties string */
		rstatus = scconf_propstr_to_proplist(properties, &proplist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* make sure all required properties are included */
		for (propdes = propdeslist;  propdes;
		    propdes = propdes->scconf_cltr_propdes_next) {
			if (propdes->scconf_cltr_propdes_name == NULL ||
			    propdes->scconf_cltr_propdes_required == 0)
				continue;
			if (strcmp(propdes->scconf_cltr_propdes_name,
			    PROP_CPOINT_TYPE) == 0)
				continue;
			for (proplistp = &proplist; *proplistp;
			    proplistp = &(*proplistp)->scconf_prop_next) {
				if ((*proplistp)->scconf_prop_key == NULL)
					continue;
				if (strcmp((*proplistp)->scconf_prop_key,
				    propdes->scconf_cltr_propdes_name) == 0)
					break;
			}
			if (*proplistp == (scconf_cfg_prop_t *)0) {
				rstatus = SCCONF_TM_EBADOPTS;
				goto cleanup;
			}
		}

		/* Check, then set, cpoint properties */
		for (proplistp = &proplist; *proplistp;
		    proplistp = &(*proplistp)->scconf_prop_next) {
			rstatus = scconf_check_cltr_property(
			    (*proplistp)->scconf_prop_key,
			    (*proplistp)->scconf_prop_value,
			    propdeslist);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			clconf_err = clconf_obj_set_property(cpoint,
			    (*proplistp)->scconf_prop_key,
			    (*proplistp)->scconf_prop_value);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
					rstatus = SCCONF_EINVAL;
					goto cleanup;

				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}
		}

		/* Free the property list */
		if (proplist != (scconf_cfg_prop_t *)0) {
			conf_free_proplist(proplist);
			proplist = (scconf_cfg_prop_t *)0;
		}

		/* Free the property description list */
		if (propdeslist != (scconf_cltr_propdes_t *)0) {
			scconf_free_propdeslist(propdeslist);
			propdeslist = (scconf_cltr_propdes_t *)0;
		}

		/* make sure that the cpoint object is okay */
		rstatus = conf_obj_check(cpoint, messages);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* if tmp_clconf is set, attempt commit here */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			rstatus = conf_cluster_commit(tmp_clconf, messages);

		/* otherwise, just check the transition */
		} else {
			rstatus = conf_check_transition(existing_clconf, clconf,
			    messages);
		}

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release temp cluster config */
	if (tmp_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)tmp_clconf);

	/* Release copy of cluster config */
	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	/* Free the property list */
	if (proplist != (scconf_cfg_prop_t *)0)
		conf_free_proplist(proplist);

	/* Free the property description list */
	if (propdeslist != (scconf_cltr_propdes_t *)0)
		scconf_free_propdeslist(propdeslist);

	return (rstatus);
}

/*
 * scconf_get_cltr_cpoint_propdeslist
 *
 * Get the list of legal properties for a given "cpoint_type".
 *
 * Upon success, a pointer to the property list is returned in
 * "propdeslistp".   If there is no property description list, the value
 * pointed to by "propdeslistp" is set to NULL.
 *
 * The caller is responsible for freeing memory associated
 * with the "propdeslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EUNKNOWN		- the "cpoint_type" is unknown
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_get_cltr_cpoint_propdeslist(char *cpoint_type,
    scconf_cltr_propdes_t **propdeslistp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_cltr_propdes_t *propdeslist = (scconf_cltr_propdes_t *)0;
	scconf_cltr_propdes_t *propdes;
	char *laststrtok;
	char *ptr;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (cpoint_type == NULL || propdeslistp == NULL)
		return (SCCONF_EINVAL);

	/* See if it is a known switch type */
	rstatus = conf_get_propdeslist(CL_BLACKBOX, "generic", &propdeslist);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;
	ptr = NULL;
	for (propdes = propdeslist;  propdes;
	    propdes = propdes->scconf_cltr_propdes_next) {
		if (propdes->scconf_cltr_propdes_enumlist) {
			ptr = propdes->scconf_cltr_propdes_enumlist;
			ptr = strtok_r(ptr, ":", &laststrtok);
			while (ptr != NULL) {
				if (strcmp(ptr, cpoint_type) == 0)
					break;
				ptr = strtok_r(NULL, ":", &laststrtok);
			}
			if (ptr != NULL)
				break;
		}
	}
	if (ptr == NULL) {
		rstatus = SCCONF_EUNKNOWN;
		goto cleanup;
	}

	/* Free the generic description list */
	if (propdeslist != NULL) {
		scconf_free_propdeslist(propdeslist);
		propdeslist = (scconf_cltr_propdes_t *)0;
	}

	/* Get the properties for the switch type */
	rstatus = conf_get_propdeslist(CL_BLACKBOX, cpoint_type, &propdeslist);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

cleanup:
	/* If error, clear the list */
	if (rstatus != SCCONF_NOERR) {
		if (propdeslist != NULL) {
			scconf_free_propdeslist(propdeslist);
			propdeslist = (scconf_cltr_propdes_t *)0;
		}
	}

	*propdeslistp = propdeslist;

	return (rstatus);
}

/*
 * scconf_change_cltr_cpoint
 *
 * Change the properties or state associated with
 * an off-node cluster transport connection point.
 *
 * The "properties" are given in a string as a comma and/or space
 * separated list of "<keyword>=<value>" pairs.  The "properties"
 * string conforms to getsubopt(3C) semantics.
 *
 * If "handle" is NULL, the change is committed upon successful
 * return of the function.  Otherwise, the change to the configuration
 * does not occur until scconf_cltr_updatehandle() returns with success.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- the cpoint does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_TM_EBADOPTS	- bad "propterties" for transport type
 *	SCCONF_TM_EINVAL	- other transport TM error
 */
scconf_errno_t
scconf_change_cltr_cpoint(scconf_cltr_handle_t handle, char *cpointname,
    char *properties, scconf_cltr_cpointstate_t state, uint_t cablechk,
    char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	clconf_cluster_t *tmp_clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	scconf_cfg_prop_t *proplist = (scconf_cfg_prop_t *)0;
	scconf_cfg_prop_t **proplistp;
	scconf_cltr_propdes_t *propdeslist = (scconf_cltr_propdes_t *)0;
	clconf_obj_t *cpoint;
	char *cpoint_type;
	int unknowntype;
	uint_t statechk = 0;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (cpointname == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Unless an already established "handle" is provided,
	 * loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)tmp_clconf);
			tmp_clconf = clconf = (clconf_cluster_t *)0;
		}
		if (existing_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)existing_clconf);
			existing_clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config, if necessary */
		if (clconf == (clconf_cluster_t *)0) {
			rstatus = scconf_cltr_openhandle(
			    (scconf_cltr_handle_t *)&tmp_clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			clconf = tmp_clconf;
		}

		/* Make a copy of the original */
		existing_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
		    (clconf_obj_t *)clconf);
		if (existing_clconf == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Find the cpoint */
		rstatus = conf_get_cpointobj(clconf, cpointname,
		    &cpoint);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Make sure that the cpoint is not being used by a cable */
		if (cablechk) {
			statechk = 1;
			rstatus = conf_check_notinuse_bycable(clconf, cpoint,
			    statechk);
			if (rstatus != SCCONF_NOERR) {
				goto cleanup;
			}
		}

		/* Set the state */
		if (state != SCCONF_STATE_UNCHANGED) {
			rstatus = conf_set_state(cpoint, state);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		/* Get the type */
		cpoint_type = (char *)clconf_bb_get_type((clconf_bb_t *)cpoint);
		if (cpoint_type == NULL) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Get the list of cpoint property descriptions */
		unknowntype = 0;
		rstatus = scconf_get_cltr_cpoint_propdeslist(cpoint_type,
		    &propdeslist);
		if (rstatus == SCCONF_EUNKNOWN) {
			++unknowntype;
			rstatus = SCCONF_NOERR;
		} else if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* get the cpoint property list from the properties string */
		rstatus = scconf_propstr_to_proplist(properties, &proplist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Check, then set, cpoint properties */
		for (proplistp = &proplist; *proplistp;
		    proplistp = &(*proplistp)->scconf_prop_next) {
			if (!unknowntype) {
				rstatus = scconf_check_cltr_property(
				    (*proplistp)->scconf_prop_key,
				    (*proplistp)->scconf_prop_value,
				    propdeslist);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
			}
			clconf_err = clconf_obj_set_property(cpoint,
			    (*proplistp)->scconf_prop_key,
			    (*proplistp)->scconf_prop_value);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
					rstatus = SCCONF_EINVAL;
					goto cleanup;

				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}
		}

		/* Free the property list */
		if (proplist != (scconf_cfg_prop_t *)0) {
			conf_free_proplist(proplist);
			proplist = (scconf_cfg_prop_t *)0;
		}

		/* Free the property description list */
		if (propdeslist != (scconf_cltr_propdes_t *)0) {
			scconf_free_propdeslist(propdeslist);
			propdeslist = (scconf_cltr_propdes_t *)0;
		}

		/* make sure that the cpoint object is okay */
		rstatus = conf_obj_check(cpoint, messages);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* If disabled, make sure we haven't isolated a node */
		if (state == SCCONF_STATE_DISABLED) {
			rstatus = conf_check_isolation(existing_clconf, clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		/* if tmp_clconf is set, attempt commit here */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			rstatus = conf_cluster_commit(tmp_clconf, messages);

		/* otherwise, just check the transition */
		} else {
			rstatus = conf_check_transition(existing_clconf, clconf,
			    messages);
		}

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release temp cluster config */
	if (tmp_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)tmp_clconf);

	/* Release copy of cluster config */
	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	/* Free the property list */
	if (proplist != (scconf_cfg_prop_t *)0)
		conf_free_proplist(proplist);

	/* Free the property description list */
	if (propdeslist != (scconf_cltr_propdes_t *)0)
		scconf_free_propdeslist(propdeslist);

	return (rstatus);
}

/*
 * scconf_rm_cltr_cpoint
 *
 * Remove an off-node cluster transport connection point from the
 * cluster configuration.
 *
 * If "handle" is NULL, the change is committed upon successful
 * return of the function.  Otherwise, the change to the configuration
 * does not occur until scconf_cltr_updatehandle() returns with success.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EINUSE		- one or more ports on the device are in use
 *	SCCONF_ENOEXIST		- the cpoint does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_rm_cltr_cpoint(scconf_cltr_handle_t handle, char *cpointname,
    char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	clconf_cluster_t *tmp_clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	clconf_obj_t *cpoint;
	uint_t statechk = 0;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (cpointname == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Unless an already established "handle" is provided,
	 * loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)tmp_clconf);
			tmp_clconf = clconf = (clconf_cluster_t *)0;
		}
		if (existing_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)existing_clconf);
			existing_clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config, if necessary */
		if (clconf == (clconf_cluster_t *)0) {
			rstatus = scconf_cltr_openhandle(
			    (scconf_cltr_handle_t *)&tmp_clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			clconf = tmp_clconf;
		}

		/* Make a copy of the original */
		existing_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
		    (clconf_obj_t *)clconf);
		if (existing_clconf == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Find the cpoint */
		rstatus = conf_get_cpointobj(clconf, cpointname,
		    &cpoint);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Make sure that the cpoint is not being used by a cable */
		rstatus = conf_check_notinuse_bycable(clconf, cpoint,
		    statechk);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Remove the cpoint from the cluster */
		clconf_err = clconf_cluster_remove_bb(clconf,
		    (clconf_bb_t *)cpoint);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_INVALID_TREE:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Make sure we haven't isolated a node */
		rstatus = conf_check_isolation(existing_clconf, clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* if tmp_clconf is set, attempt commit here */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			rstatus = conf_cluster_commit(tmp_clconf, messages);

		/* otherwise, just check the transition */
		} else {
			rstatus = conf_check_transition(existing_clconf, clconf,
			    messages);
		}

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release temp cluster config */
	if (tmp_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)tmp_clconf);

	/* Release copy of cluster config */
	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	return (rstatus);
}

/*
 * scconf_add_cltr_cable
 *
 * Create a virtual "cable", or mapping between possible connection points.
 * Connection points may be on-node adapters or off-node connection points
 * (blackbox devices).
 *
 * The scconf_cltr_epoint_t structures contain information regarding
 * type (on-node adapter or off-node connection point), node id (only
 * for on-node adapter), connection point id (namespace depends on type),
 * port name, and port state.
 *
 * The "scconf_cltr_epoint_nodename" field must be NULL for off-node
 * connection points.
 *
 * If the "scconf_cltr_epoint_portname" field is set to NULL, no
 * name is assigned to the port.
 *
 * Unless the "noenable" flag is non-zero, adding the cable
 * enables not only the cable, but also its ports, adapters,
 * and cpoints.   Nodes are always enabled when cabled,
 * regardless of whether or not the "nonable" flag is given.
 *
 * The "scconf_cltr_epoint_state" field is ignored by this function.
 *
 * The order in which the two endpoints are specified in the function
 * call is unimportant.
 *
 * If "handle" is NULL, the change is committed upon successful
 * return of the function.  Otherwise, the change to the configuration
 * does not occur until scconf_cltr_updatehandle() returns with success.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- the given "cable" is already established
 *	SCCONF_ENOEXIST		- one of the endpoints does not exist
 *	SCCONF_EINUSE		- a given port is already in use
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_add_cltr_cable(scconf_cltr_handle_t handle, uint_t noenable,
    scconf_cltr_epoint_t *epoint1, scconf_cltr_epoint_t *epoint2,
    char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	clconf_cluster_t *tmp_clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	clconf_obj_t *cable = (clconf_obj_t *)0;
	scconf_cltr_epoint_t *epoints[2];
	scconf_cltr_epoint_t *epoint;
	clconf_port_t *ports[2];
	clconf_port_t **portp;
	clconf_obj_t *port, *port_parent, *port_grandparent;
	clconf_objtype_t port_parent_type;
	scconf_nodeid_t nodeids[2];
	scconf_nodeid_t *nodeidp;
	char *defaultp1 = NULL;
	char *defaultp2 = NULL;
	int e;
	clconf_obj_t *tmp_adapter = (clconf_obj_t *)0;
	char *stored_adaptername;
	clconf_obj_t *nodeobj = (clconf_obj_t *)0;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (epoint1 == NULL || epoint1->scconf_cltr_epoint_devicename == NULL ||
	    epoint2 == NULL || epoint2->scconf_cltr_epoint_devicename == NULL)
		return (SCCONF_EINVAL);

	/* Initialize endpoints array */
	epoints[0] = epoint1;
	epoints[1] = epoint2;

	/*
	 * Unless an already established "handle" is provided,
	 * loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Initialize ports array */
		for (e = 1;  e <= 2;  ++e)
			ports[e - 1] = (clconf_port_t *)0;

		/* Release copies of cluster config, if they exist */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)tmp_clconf);
			tmp_clconf = clconf = (clconf_cluster_t *)0;
		}
		if (existing_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)existing_clconf);
			existing_clconf = (clconf_cluster_t *)0;
		}

		/* Free default portname memory, if allocated */
		if (defaultp1 != NULL) {
			free(defaultp1);
			defaultp1 = NULL;
			epoint1->scconf_cltr_epoint_portname = NULL;
		}
		if (defaultp2 != NULL) {
			free(defaultp2);
			defaultp2 = NULL;
			epoint2->scconf_cltr_epoint_portname = NULL;
		}

		/* Allocate the cluster config, if necessary */
		if (clconf == (clconf_cluster_t *)0) {
			rstatus = scconf_cltr_openhandle(
			    (scconf_cltr_handle_t *)&tmp_clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			clconf = tmp_clconf;
		}

		/* Make a copy of the original */
		existing_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
		    (clconf_obj_t *)clconf);
		if (existing_clconf == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Make sure this is not switch-to-switch */
		if (epoint1->scconf_cltr_epoint_type ==
		    SCCONF_CLTR_EPOINT_TYPE_CPOINT &&
		    epoint2->scconf_cltr_epoint_type ==
		    SCCONF_CLTR_EPOINT_TYPE_CPOINT) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/* Check for existance of the nodes */
		nodeidp = &nodeids[0];
		if (epoint1->scconf_cltr_epoint_nodename) {
			rstatus = conf_get_nodeid(clconf,
			    epoint1->scconf_cltr_epoint_nodename, nodeidp);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}
		nodeidp = &nodeids[1];
		if (epoint2->scconf_cltr_epoint_nodename) {
			rstatus = conf_get_nodeid(clconf,
			    epoint2->scconf_cltr_epoint_nodename, nodeidp);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		/*
		 * For the adapter endpoint(s), check that the adapter is
		 * defined, and if it is defined as a vlan device, modify the
		 * adapter name for the endpoint.
		 */
		for (e = 1;  e <= 2;  ++e) {
			epoint = epoints[e - 1];
			if (epoint->scconf_cltr_epoint_type !=
			    SCCONF_CLTR_EPOINT_TYPE_ADAPTER)
				continue;

			rstatus = conf_get_nodeobj(clconf, nodeids[e - 1],
			    &nodeobj);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

			rstatus = conf_get_adapterobj(
			    nodeobj,
			    epoint->scconf_cltr_epoint_devicename,
			    &tmp_adapter);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

			stored_adaptername = NULL;
			rstatus = conf_get_adaptername(tmp_adapter, FULL_NAME,
			    &stored_adaptername);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

			if (strcmp(stored_adaptername,
			    epoint->scconf_cltr_epoint_devicename) != 0) {
				epoint->scconf_cltr_epoint_devicename =
				    stored_adaptername;
			} else {
				free(stored_adaptername);
				stored_adaptername = NULL;
			}
		}

		/* Generate default portnames, if necessary */
		if (epoint1->scconf_cltr_epoint_portname == NULL) {
			defaultp1 = conf_generate_default_portname(clconf,
			    epoint1, epoint2);
			if (defaultp1 == NULL) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
			epoint1->scconf_cltr_epoint_portname = defaultp1;
		}
		if (epoint2->scconf_cltr_epoint_portname == NULL) {
			defaultp2 = conf_generate_default_portname(clconf,
			    epoint2, epoint1);
			if (defaultp2 == NULL) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
			epoint2->scconf_cltr_epoint_portname = defaultp2;
		}

		/* Make sure we have different endpoints */
		if (conf_epointcmp(clconf, epoint1, epoint2) == 0) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/* Make sure that the cable is not already configured */
		rstatus = conf_get_cableobj(clconf, epoint1, epoint2, &cable);
		switch (rstatus) {
		case SCCONF_ENOEXIST:
			rstatus = SCCONF_NOERR;
			break;

		case SCCONF_NOERR:
			rstatus = SCCONF_EEXIST;
			goto cleanup;

		default:
			goto cleanup;
		} /*lint !e788 */

		/* Get two unused ports */
		for (e = 1;  e <= 2;  ++e) {
			epoint = epoints[e - 1];
			portp = &ports[e - 1];

			rstatus = conf_create_port(clconf, epoint, portp,
			    messages);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		/* Create the cable */
		cable = (clconf_obj_t *)clconf_cable_create();
		if (cable == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add the cable to the cluster */
		clconf_err = clconf_cluster_add_cable(clconf,
		    (clconf_cable_t *)cable);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
				rstatus = SCCONF_EINVAL;
				goto cleanup;

			case CL_INVALID_ID:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Add ports to the cable */
		for (e = 1;  e <= 2;  ++e) {
			portp = &ports[e - 1];

			clconf_err = clconf_cable_set_endpoint(
			    (clconf_cable_t *)cable, e, *portp);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
					rstatus = SCCONF_EINVAL;
					goto cleanup;

				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}
		}

		/* each node is enabled, regardless of "noenable" flag */
		for (e = 1;  e <= 2;  ++e) {
			port = (clconf_obj_t *)ports[e - 1];
			port_parent = clconf_obj_get_parent(port);
			if (port_parent == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* type of parent of port */
			port_parent_type = clconf_obj_get_objtype(
			    port_parent);
			if (port_parent_type != CL_ADAPTER &&
			    port_parent_type != CL_BLACKBOX) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			if (port_parent_type == CL_ADAPTER) {
				port_grandparent = clconf_obj_get_parent(
				    port_parent);
				if (port_grandparent == NULL) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}
				rstatus = conf_set_state(port_grandparent,
				    SCCONF_STATE_ENABLED);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
			}
		}

		/* Set the state */
		if (noenable) {

			/* each port */
			for (e = 1;  e <= 2;  ++e) {
				port = (clconf_obj_t *)ports[e - 1];

				rstatus = conf_set_state(port,
				    SCCONF_STATE_DISABLED);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
			}

			/* the cable */
			rstatus = conf_set_state(cable,
			    SCCONF_STATE_DISABLED);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

		} else {

			/* the cable */
			rstatus = conf_set_state(cable,
			    SCCONF_STATE_ENABLED);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

			/* each port and parent port */
			for (e = 1;  e <= 2;  ++e) {
				port = (clconf_obj_t *)ports[e - 1];
				port_parent = clconf_obj_get_parent(port);
				if (port_parent == NULL) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}

				/* parent port */
				rstatus = conf_set_state(port_parent,
				    SCCONF_STATE_ENABLED);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;

				/* port */
				rstatus = conf_set_state(port,
				    SCCONF_STATE_ENABLED);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
			}
		}

		/* make sure that the cable object is okay */
		rstatus = conf_obj_check(cable, messages);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* if tmp_clconf is set, attempt commit here */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			rstatus = conf_cluster_commit(tmp_clconf, messages);

		/* otherwise, just check the transition */
		} else {
			rstatus = conf_check_transition(existing_clconf, clconf,
			    messages);
		}

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release temp cluster config */
	if (tmp_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)tmp_clconf);

	/* Release copy of cluster config */
	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	/* Free default portname memory, if allocated */
	if (defaultp1 != NULL) {
		free(defaultp1);
		epoint1->scconf_cltr_epoint_portname = NULL;
	}
	if (defaultp2 != NULL) {
		free(defaultp2);
		epoint2->scconf_cltr_epoint_portname = NULL;
	}

	return (rstatus);
}

/*
 * scconf_change_cltr_cable
 *
 * Change the state of a cable.
 *
 * The order in which the two endpoints are specified in the function
 * call is unimportant.   And, "epoint2" may be given as NULL.
 *
 * If "handle" is NULL, the change is committed upon successful
 * return of the function.  Otherwise, the change to the configuration
 * does not occur until scconf_cltr_updatehandle() returns with success.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- the cpoint does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_change_cltr_cable(scconf_cltr_handle_t handle,
    scconf_cltr_epoint_t *epoint1, scconf_cltr_epoint_t *epoint2,
    scconf_cltr_cablestate_t state, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	clconf_cluster_t *tmp_clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	clconf_obj_t *cable = (clconf_obj_t *)0;
	clconf_obj_t *port, *port_parent;
	char *defaultp1 = NULL;
	char *defaultp2 = NULL;
	int e;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	if (state == SCCONF_STATE_UNCHANGED)
		return (SCCONF_NOERR);

	/* Check arguments */
	if (epoint1 == NULL || epoint1->scconf_cltr_epoint_devicename == NULL ||
	    (epoint2 != NULL &&
	    epoint2->scconf_cltr_epoint_devicename == NULL) ||
	    (state != SCCONF_STATE_DISABLED && state != SCCONF_STATE_ENABLED))
		return (SCCONF_EINVAL);

	/*
	 * Unless an already established "handle" is provided,
	 * loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)tmp_clconf);
			tmp_clconf = clconf = (clconf_cluster_t *)0;
		}
		if (existing_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)existing_clconf);
			existing_clconf = (clconf_cluster_t *)0;
		}

		/* Free default portname memory, if allocated */
		if (defaultp1 != NULL) {
			free(defaultp1);
			defaultp1 = NULL;
			epoint1->scconf_cltr_epoint_portname = NULL;
		}
		if (defaultp2 != NULL) {
			free(defaultp2);
			defaultp2 = NULL;
			epoint2->scconf_cltr_epoint_portname = NULL;
		}

		/* Allocate the cluster config, if necessary */
		if (clconf == (clconf_cluster_t *)0) {
			rstatus = scconf_cltr_openhandle(
			    (scconf_cltr_handle_t *)&tmp_clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			clconf = tmp_clconf;
		}

		/* Make a copy of the original */
		existing_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
		    (clconf_obj_t *)clconf);
		if (existing_clconf == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Port names must be given when there is more than one port */
		if (epoint1->scconf_cltr_epoint_portname == NULL) {
			defaultp1 = conf_get_default_portname(clconf, epoint1);
			if (defaultp1 == NULL) {
				rstatus = SCCONF_ENOEXIST;
				goto cleanup;
			}
			epoint1->scconf_cltr_epoint_portname = defaultp1;
		}
		if (epoint2 != NULL &&
		    epoint2->scconf_cltr_epoint_portname == NULL) {
			defaultp2 = conf_get_default_portname(clconf, epoint2);
			if (defaultp2 == NULL) {
				rstatus = SCCONF_ENOEXIST;
				goto cleanup;
			}
			epoint2->scconf_cltr_epoint_portname = defaultp2;
		}

		/* Make sure we have different endpoints */
		if (conf_epointcmp(clconf, epoint1, epoint2) == 0) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/* Make sure that the cable is configured */
		rstatus = conf_get_cableobj(clconf, epoint1, epoint2, &cable);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Set the state the cable */
		rstatus = conf_set_state(cable, state);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* And, the two ports;  and, on enable, enable port parents */
		for (e = 1;  e <= 2;  ++e) {
			/* Get port */
			port = (clconf_obj_t *)clconf_cable_get_endpoint(
			    (clconf_cable_t *)cable, e);
			if (port == (clconf_obj_t *)0) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			if (state == SCCONF_STATE_ENABLED) {
				/* Get port's parent */
				port_parent = clconf_obj_get_parent(port);
				if (port_parent == NULL) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}

				/* Enable port's parent */
				rstatus = conf_set_state(port_parent,
				    SCCONF_STATE_ENABLED);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
			}

			/* Set port state */
			rstatus = conf_set_state(port, state);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		/* If disabled, make sure we haven't isolated a node */
		if (state == SCCONF_STATE_DISABLED) {
			rstatus = conf_check_isolation(existing_clconf, clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		/* if tmp_clconf is set, attempt commit here */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			rstatus = conf_cluster_commit(tmp_clconf, messages);

		/* otherwise, just check the transition */
		} else {
			rstatus = conf_check_transition(existing_clconf, clconf,
			    messages);
		}

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release temp cluster config */
	if (tmp_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)tmp_clconf);

	/* Release copy of cluster config */
	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	/* Free default portname memory, if allocated */
	if (defaultp1 != NULL) {
		free(defaultp1);
		epoint1->scconf_cltr_epoint_portname = NULL;
	}
	if (defaultp2 != NULL) {
		free(defaultp2);
		epoint2->scconf_cltr_epoint_portname = NULL;
	}

	return (rstatus);
}

/*
 * scconf_rm_cltr_cable
 *
 * Remove the cable with matching endpoints.
 *
 * The order in which the two endpoints are specified in the function
 * call is unimportant.   And, "epoint2" may be given as NULL.
 *
 * If "handle" is NULL, the change is committed upon successful
 * return of the function.  Otherwise, the change to the configuration
 * does not occur until scconf_cltr_updatehandle() returns with success.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- the cable does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_rm_cltr_cable(scconf_cltr_handle_t handle,
    scconf_cltr_epoint_t *epoint1, scconf_cltr_epoint_t *epoint2,
    int chk_state, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	clconf_cluster_t *tmp_clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	clconf_obj_t *cable = (clconf_obj_t *)0;
	clconf_obj_t *ports[2];
	clconf_obj_t *port, *port_parent;
	char *defaultp1 = NULL;
	char *defaultp2 = NULL;
	int e;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (epoint1 == NULL || epoint1->scconf_cltr_epoint_devicename == NULL ||
	    (epoint2 != NULL &&
	    epoint2->scconf_cltr_epoint_devicename == NULL))
		return (SCCONF_EINVAL);

	/*
	 * Unless an already established "handle" is provided,
	 * loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)tmp_clconf);
			tmp_clconf = clconf = (clconf_cluster_t *)0;
		}
		if (existing_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)existing_clconf);
			existing_clconf = (clconf_cluster_t *)0;
		}

		/* Free default portname memory, if allocated */
		if (defaultp1 != NULL) {
			free(defaultp1);
			defaultp1 = NULL;
			epoint1->scconf_cltr_epoint_portname = NULL;
		}
		if (defaultp2 != NULL) {
			free(defaultp2);
			defaultp2 = NULL;
			epoint2->scconf_cltr_epoint_portname = NULL;
		}

		/* Allocate the cluster config, if necessary */
		if (clconf == (clconf_cluster_t *)0) {
			rstatus = scconf_cltr_openhandle(
			    (scconf_cltr_handle_t *)&tmp_clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			clconf = tmp_clconf;
		}

		/* Make a copy of the original */
		existing_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
		    (clconf_obj_t *)clconf);
		if (existing_clconf == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Port names must be given when there is more than one port */
		if (epoint1->scconf_cltr_epoint_portname == NULL) {
			defaultp1 = conf_get_default_portname(clconf, epoint1);
			if (defaultp1 == NULL) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
			epoint1->scconf_cltr_epoint_portname = defaultp1;
		}
		if (epoint2 != NULL &&
		    epoint2->scconf_cltr_epoint_portname == NULL) {
			defaultp2 = conf_get_default_portname(clconf, epoint2);
			if (defaultp2 == NULL) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
			epoint2->scconf_cltr_epoint_portname = defaultp2;
		}

		/* Make sure we have different endpoints */
		if (conf_epointcmp(clconf, epoint1, epoint2) == 0) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/* Make sure that the cable is configured */
		rstatus = conf_get_cableobj(clconf, epoint1, epoint2, &cable);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Make sure that the cable itself is enabled */
		if (chk_state) {
			if (clconf_obj_enabled(cable)) {
				rstatus = SCCONF_EINUSE;
				goto cleanup;
			}
		}

		/* Get the two ports */
		for (e = 1;  e <= 2;  ++e) {
			port = (clconf_obj_t *)clconf_cable_get_endpoint(
			    (clconf_cable_t *)cable, e);
			if (port == (clconf_obj_t *)0) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			ports[e - 1] = port;
		}

		/* Remove the cable */
		clconf_err = clconf_cluster_remove_cable(clconf,
		    (clconf_cable_t *)cable);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_INVALID_TREE:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* And, remove the two ports */
		for (e = 1;  e <= 2;  ++e) {
			port = ports[e - 1];

			port_parent = clconf_obj_get_parent(port);
			if (port_parent == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			switch (clconf_obj_get_objtype(port_parent)) {
			case CL_ADAPTER:
				clconf_err = clconf_adapter_remove_port(
				    (clconf_adapter_t *)port_parent,
				    (clconf_port_t *)port);
				if (clconf_err != CL_NOERROR) {
					switch (clconf_err) {
					case CL_INVALID_TREE:
					default:
						rstatus = SCCONF_EUNEXPECTED;
						goto cleanup;
					} /*lint !e788 */
				}
				break;

			case CL_BLACKBOX:
				clconf_err = clconf_bb_remove_port(
				    (clconf_bb_t *)port_parent,
				    (clconf_port_t *)port);
				if (clconf_err != CL_NOERROR) {
					switch (clconf_err) {
					case CL_INVALID_TREE:
					default:
						rstatus = SCCONF_EUNEXPECTED;
						goto cleanup;
					} /*lint !e788 */
				}
				break;

			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}

		/* Make sure we haven't isolated a node */
		rstatus = conf_check_isolation(existing_clconf, clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* if tmp_clconf is set, attempt commit here */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			rstatus = conf_cluster_commit(tmp_clconf, messages);

		/* otherwise, just check the transition */
		} else {
			rstatus = conf_check_transition(existing_clconf, clconf,
			    messages);
		}

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release temp cluster config */
	if (tmp_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)tmp_clconf);

	/* Release copy of cluster config */
	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	/* Free default portname memory, if allocated */
	if (defaultp1 != NULL) {
		free(defaultp1);
		epoint1->scconf_cltr_epoint_portname = NULL;
	}
	if (defaultp2 != NULL) {
		free(defaultp2);
		epoint2->scconf_cltr_epoint_portname = NULL;
	}

	return (rstatus);
}

/*
 * scconf_set_netaddr
 *
 * Set the network address and netmask for using TCP/IP over the
 * private interconnect.
 *
 * If either the netaddr or netmask are set to zero, the default is used.
 * But, both values are updated in the CCR, without preserving original
 * CCR values.
 *
 * For more information, see comments under scconf_check_netaddr().
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid netaddr or netmask
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_set_netaddr(ulong_t netaddr, ulong_t netmask, char **messages)
{
	scconf_errno_t rstatus;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	char snetaddr[sizeof ("255.255.255.255")];
	char snetmask[sizeof ("255.255.255.255")];
	struct in_addr in;
	char *s;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Set defaults, if needed */
	if (netaddr == 0)
		netaddr = inet_network(SCCONF_DEFAULT_NETNUMBER_STRING);
	if (netmask == 0)
		netmask = inet_network(SCCONF_DEFAULT_NETMASK_STRING);

	/* Check the values */
	rstatus = scconf_check_netaddr(netaddr, netmask);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Convert to string, dot notation */
	in.s_addr = netaddr;
	if ((s = inet_ntoa(in)) == NULL) {
		return (SCCONF_EUNEXPECTED);
	} else {
		(void) strcpy(snetaddr, s);
	}
	in.s_addr = netmask;
	if ((s = inet_ntoa(in)) == NULL) {
		return (SCCONF_EUNEXPECTED);
	} else {
		(void) strcpy(snetmask, s);
	}

	/*
	 * Loop until the commit succeeds, or until
	 * a fatal error is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			return (rstatus);

		/* Set the properties */
		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_NETADDR, snetaddr);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_NETMASK, snetmask);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_SUBNETMASK, SCCONF_DEFAULT_SUBNETMASK_STRING);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_MAXNODES, SCCONF_DEFAULT_MAXNODES_STRING);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
			PROP_CLUSTER_MAXPRIVATENETS,
			SCCONF_DEFAULT_MAXPRIVNETS_STRING);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
			PROP_CLUSTER_USER_NET_NUM,
			SCCONF_DEFAULT_USER_NET_NUM_STRING);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
			PROP_CLUSTER_USER_NETMASK,
			SCCONF_DEFAULT_USER_NETMASK_STRING);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, messages);

		/* If committed okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_check_netaddr
 *
 * This function can be used to verify that the given
 * netaddr/netmask pair is legal for the private interconnect.
 *
 * This function verifies all of the following:
 *
 *	1. There are no holes in the netmask.
 *
 *	2. The netaddr is not bigger than the netmask.
 *
 * If either "netaddr" or "netmask" are zero, the default is used.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid netaddr or netmask
 */
scconf_errno_t
scconf_check_netaddr(ulong_t netaddr, ulong_t netmask)
{
	int i, startflg, bitsize;

	/* If both are default, no need to check anything */
	if (netaddr == 0 && netmask == 0)
		return (SCCONF_NOERR);

	/* Set defaults, if needed */
	if (netaddr == 0)
		netaddr = inet_network(SCCONF_DEFAULT_NETNUMBER_STRING);
	if (netmask == 0)
		netmask = inet_network(SCCONF_DEFAULT_NETMASK_STRING);

	/* Check for holes in the mask */
	startflg = 0;
	bitsize = sizeof (netmask) * 8;
	for (i = 0;  i < bitsize;  ++i) {
		if (!startflg && ((netmask >> i) & 1)) {
			++startflg;
		} else if (startflg && !((netmask >> i) & 1)) {
			return (SCCONF_EINVAL);
		}
	}

	/* Make sure that the netaddr is not bigger than the mask */
	if (netaddr & ~netmask)
		return (SCCONF_EINVAL);

	return (SCCONF_NOERR);
}

/*
 * scconf_check_cltr_property
 *
 * Check the given cluster transport property "name" and "value" against
 * the given propery description list ("propdeslist").
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_TM_EBADOPTS	- bad propterty
 */
scconf_errno_t
scconf_check_cltr_property(char *name, char *value,
    scconf_cltr_propdes_t *propdeslist)
{
	scconf_errno_t rstatus;

	/* Check the property */
	rstatus = conf_check_property(name, value, propdeslist);
	if (rstatus == SCCONF_EINVAL)
		rstatus = SCCONF_TM_EBADOPTS;

	return (rstatus);
}

/*
 * scconf_get_legal_hb
 *
 * When the "set" flag is FALSE, it just checks the input "timeout"
 * and "quantum" against the adapter properties description (in
 * *propdeslist_p) that is obtained from the adapter's clpl file
 * and see if they are legal, i.e., within the range.
 *
 * If the "set" flag is TRUE, it then picks the default "timeout"
 * and "quantum" settings from the adapter properties description.
 *
 * Possible return values:
 *
 * 	SCCONF_EINVAL		Invalid parameters
 *	SCCONF_ERANGE		Out of range
 *	SCCONF_EUNEXPECTED	Internal or unexpected error
 *	SCCONF_NOERR		No error
 */
scconf_errno_t
scconf_get_legal_hb(char *transport_type,
    scconf_cltr_propdes_t *propdeslist_p, char **timeout,
    boolean_t set_timeout, char **quantum, boolean_t set_quantum)
{
	char p_timeout[SCCONF_HB_BUFSIZ], p_quantum[SCCONF_HB_BUFSIZ];
	scconf_cltr_propdes_t *propdes;
	int value = 0;
	boolean_t also_set_quantum = B_FALSE;

	/* Check the parameters */
	if (transport_type == NULL || propdeslist_p == NULL) {
		return (SCCONF_EINVAL);
	}

	/* Get the timeout/quantum property string */
	(void) sprintf(p_timeout, "%s_%s",
		transport_type, PROP_ADAPTER_HEARTBEAT_TIMEOUT);
	(void) sprintf(p_quantum, "%s_%s",
		transport_type, PROP_ADAPTER_HEARTBEAT_QUANTUM);

	/* Iterate the property description list for timeout */
	for (propdes = propdeslist_p;  propdes;
		propdes = propdes->scconf_cltr_propdes_next) {
		if (strcmp(propdes->scconf_cltr_propdes_name,
		    p_timeout) != 0)
			continue;

		/* Set timeout */
		value = 0;
		if (*timeout != NULL)
			value = atoi(*timeout);

		if (set_timeout &&
		    (value < atoi(propdes->scconf_cltr_propdes_default))) {
			*timeout = strdup(
			    propdes->scconf_cltr_propdes_default);
			if (*timeout == NULL) {
				return (SCCONF_ENOMEM);
			}
			value = atoi(*timeout);
			if (set_quantum)
				also_set_quantum = B_TRUE;
		}

		/* Check the range */
		if (propdes->scconf_cltr_propdes_min != 0 &&
		    propdes->scconf_cltr_propdes_max != 0) {
			if ((value < propdes->scconf_cltr_propdes_min) ||
			    (value > propdes->scconf_cltr_propdes_max))
				return (SCCONF_ERANGE);
		}
	}

	/* Iterate the property description list for quantum */
	for (propdes = propdeslist_p;  propdes;
	    propdes = propdes->scconf_cltr_propdes_next) {
		if (strcmp(propdes->scconf_cltr_propdes_name,
		    p_quantum) != 0)
			continue;

		/* Set quantum */
		value = 0;
		if (*quantum != NULL)
			value = atoi(*quantum);

		if (also_set_quantum ||
		    (set_quantum &&
		    (value < atoi(propdes->scconf_cltr_propdes_default)))) {
			*quantum = strdup(
			    propdes->scconf_cltr_propdes_default);
			if (*quantum == NULL) {
				if (*timeout)
					free(timeout);
				return (SCCONF_ENOMEM);
			}
			value = atoi(*quantum);
		}

		/* Check the range */
		if (propdes->scconf_cltr_propdes_min != 0 &&
		    propdes->scconf_cltr_propdes_max != 0) {
			if ((value < propdes->scconf_cltr_propdes_min) ||
			    (value > propdes->scconf_cltr_propdes_max))
				return (SCCONF_ERANGE);
		}
	}

	if (set_timeout && (atoi(*timeout) == 0))
		return (SCCONF_EUNEXPECTED);

	if (set_quantum && (atoi(*quantum) == 0))
		return (SCCONF_EUNEXPECTED);

	return (SCCONF_NOERR);
}

/*
 * scconf_change_heartbeat
 *
 * Change or set the default heart beat settings for the transport.
 *
 */
scconf_errno_t
scconf_change_heartbeat(char *subopts, char **messages)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clconf_errnum_t clconf_err = CL_NOERROR;
	char *hb_timeout = (char *)0;
	char *hb_quantum = (char *)0;
	scconf_cltr_handle_t handle = (scconf_cltr_handle_t *)0;
	sc_syslog_msg_handle_t sys_handle;
	boolean_t log_timeout = B_FALSE;
	boolean_t log_quantum = B_FALSE;

	/* Must be root */
	if (getuid() != 0) {
		return (SCCONF_EPERM);
	}

	/* Check arguments */
	if (subopts == NULL) {
		return (SCCONF_EINVAL);
	}

	scconferr = scconf_properties_to_hb(&hb_timeout,
	    &hb_quantum, subopts, NULL);
	if (scconferr != SCCONF_NOERR) {
		goto cleanup1;
	}

	/*
	 * Check specified heart beat settings. We only allow
	 * one parameter one time (either timeout or quantum).
	 */
	if (hb_timeout && hb_quantum) {
		scconf_addmessage(dgettext(TEXT_DOMAIN,
		    "Specify only one property.\n"),
		    messages);
		scconferr = SCCONF_TM_EBADOPTS;
		goto cleanup1;
	}
	if (hb_timeout) {
		log_timeout = B_TRUE;
	} else if (hb_quantum) {
		log_quantum = B_TRUE;
	} else {
		return (SCCONF_TM_EBADOPTS);
	}

	/*
	 * If we generate a handle here, loop until commit succeeds,
	 * or until a fatal error is encountered.
	 */
	for (;;) {
		/* Release old handle, if necessary */
		if (handle != (scconf_cltr_handle_t)0)
			scconf_cltr_releasehandle(handle);
		handle = (scconf_cltr_handle_t)0;

		/* Get handle */
		(void) scconf_cltr_openhandle(&handle);

		/* May need to use the CCR heart beat setting */
		if (hb_timeout == NULL) {
			hb_timeout = (char *)clconf_obj_get_property(
			    (clconf_obj_t *)handle,
			    PROP_CLUSTER_HB_TIMEOUT);
			if (hb_timeout == NULL) {
				/* Use the default setting */
				if ((hb_timeout = strdup(
				    "default")) == NULL) {
					scconferr = SCCONF_ENOMEM;
					goto cleanup;
				}
			}
		}
		if (hb_quantum == NULL) {
			hb_quantum = (char *)clconf_obj_get_property(
			    (clconf_obj_t *)handle,
			    PROP_CLUSTER_HB_QUANTUM);
			if (hb_quantum == NULL) {
				if ((hb_quantum = strdup(
				    "default")) == NULL) {
					scconferr = SCCONF_ENOMEM;
					goto cleanup;
				}
			}
		}

		/*
		 * Get the default heart beat setting if hb_timeout,
		 * hb_quantum is '\0'; If they are specified by
		 * user input, check if they are legal.
		 */
		if ((scconferr = conf_get_or_change_hb(handle,
		    &hb_timeout, &hb_quantum,
		    messages, B_FALSE)) != SCCONF_NOERR) {
			goto cleanup;
		}

		/* If no heart beat setting, unexpected error */
		if (hb_timeout == NULL || *hb_timeout == '\0') {
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		if (hb_quantum == NULL || *hb_quantum == '\0') {
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Change properties for all apdaters */
		if ((scconferr = conf_get_or_change_hb(handle,
		    &hb_timeout, &hb_quantum,
		    messages, B_TRUE)) != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Change the default transport heart beat setting */
		clconf_err = clconf_obj_set_property(handle,
		    PROP_CLUSTER_HB_TIMEOUT, hb_timeout);
		if (clconf_err != CL_NOERROR) {
			goto cleanup;
		}

		clconf_err = clconf_obj_set_property(handle,
		    PROP_CLUSTER_HB_QUANTUM, hb_quantum);
		if (clconf_err != CL_NOERROR) {
			goto cleanup;
		}

		/* Update handle */
		if (handle != (scconf_cltr_handle_t)0) {
			/* update */
			scconferr = scconf_cltr_updatehandle(handle, messages);

			/* if update returned okay, we are done */
			if (scconferr == SCCONF_NOERR) {
				break;
			/* if outdated handle, continue to retry */
			} else if (scconferr != SCCONF_ESTALE) {
				goto cleanup;
			}
		} else {
			break;
		}
	}

cleanup:
	/* Release the handle */
	if (handle != (scconf_cltr_handle_t)0)
		scconf_cltr_releasehandle(handle);

	/* Hidden command, so do not print usage */

	if (scconferr == SCCONF_NOERR) {
		/* Log the changes into syslog */
		(void) sc_syslog_msg_initialize(&sys_handle,
		    SC_SYSLOG_HEARTBEAT_TAG, "");
		/* Log the change */
		if (log_timeout) {
			/*
			 * SCMSGS
			 * @explanation
			 * The global transport heart beat timeout is changed.
			 * @user_action
			 * None. This is only for information.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_INFO,
			    MESSAGE,
			    "Transport heart beat timeout is "
			    "changed to %s.", hb_timeout);
		} else if (log_quantum) {
			/*
			 * SCMSGS
			 * @explanation
			 * The global transport heart beat quantum is changed.
			 * @user_action
			 * None. This is only for information.
			 */
			(void) sc_syslog_msg_log(sys_handle, LOG_INFO,
			    MESSAGE,
			    "Transport heart beat quantum is "
			    "changed to %s.", hb_quantum);
		}
		/* Close the log */
		sc_syslog_msg_done(&sys_handle);
	}

cleanup1:
	/* Free heart beat */
	if (hb_timeout != (char *)0)
		free(hb_timeout);
	if (hb_quantum != (char *)0)
		free(hb_quantum);

	return (scconferr);
}

/*
 * scconf_get_heartbeat
 *
 * Get the heartbeat property value from ccr. If it is not in CCR,
 * get it from the transport clpl files for the default value.
 *
 * Caller is responsible to free the memory allocated for the values.
 *
 */
scconf_errno_t
scconf_get_heartbeat(char **hb_timeout_val, boolean_t timeout_default,
    char **hb_quantum_val, boolean_t quantum_default)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_cltr_handle_t handle = (scconf_cltr_handle_t *)0;
	char *conf_timeout = (char *)0;
	char *conf_quantum = (char *)0;

	/* Check arguments */
	if ((hb_timeout_val == (char **)0) &&
	    (hb_quantum_val == (char **)0)) {
		return (SCCONF_EINVAL);
	}

	/* Get clconf handle */
	rstatus = scconf_cltr_openhandle(&handle);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	*hb_timeout_val = (char *)0;
	*hb_quantum_val = (char *)0;

	/* If not use the default setting, get the values in ccr */
	if (!timeout_default) {
		conf_timeout = (char *)clconf_obj_get_property(
		    (clconf_obj_t *)handle, PROP_CLUSTER_HB_TIMEOUT);
		if (conf_timeout) {
			*hb_timeout_val = strdup(conf_timeout);
			if (*hb_timeout_val == (char *)0) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}
	}

	if (!quantum_default) {
		conf_quantum = (char *)clconf_obj_get_property(
		    (clconf_obj_t *)handle, PROP_CLUSTER_HB_QUANTUM);
		if (conf_quantum) {
			*hb_quantum_val = strdup(conf_quantum);
			if (*hb_quantum_val == (char *)0) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}
	}

	/* Get default from clpl if not exists in ccr */
	if ((*hb_timeout_val == (char *)0) || (*hb_quantum_val == (char *)0)) {
		rstatus = conf_get_or_change_hb(handle,
		    hb_timeout_val, hb_quantum_val, NULL, B_FALSE);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}
	}

cleanup:
	/* Release the handle */
	if (handle != (scconf_cltr_handle_t)0) {
		scconf_cltr_releasehandle(handle);
	}

	return (rstatus);
}

/*
 * conf_get_adapterobj
 *
 * Return the adapter object in "adapterp" for the given "node" and
 * "adaptername".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the adapter does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ENOMEM 		- Out of memory
 *	SCCONF_EINUSE		- physical device being used already
 */
static scconf_errno_t
conf_get_adapterobj(clconf_obj_t *node, char *adaptername,
    clconf_obj_t **adapterp)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_iter_t *curradaptersi, *adaptersi = (clconf_iter_t *)0;
	clconf_obj_t *adapter;
	const char *svlan = NULL;
	const char *dev_name = NULL;
	const char *dev_instance = NULL;
	int vlan_id = 0;
	int instance_num = 0;
	int composite_num = 0;
	char *phys_devname = NULL;
	char *expanded_adaptername = NULL;
	char *specified_physdev = NULL;
	char *specified_drv = NULL;
	char *specified_ins = NULL;
	div_t inst_vlan;
	size_t len;
	int inst_num;

	/* Check arguments */
	if (node == NULL || adaptername == NULL || adapterp == NULL)
		return (SCCONF_EINVAL);

	/* Split the adaptername into its two components */
	len = strlen(adaptername) + 1;
	specified_drv = (char *)calloc(1, len);
	specified_ins = (char *)calloc(1, len);
	specified_physdev = (char *)calloc(1, len);
	if (specified_drv == NULL || specified_ins == NULL ||
	    specified_physdev == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}
	rstatus = conf_split_adaptername(adaptername, specified_drv,
	    specified_ins);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	inst_num = atoi(specified_ins);
	if (inst_num >= VLAN_ZONE_START) {
		inst_vlan = div(inst_num, VLAN_MULTIPLIER);
		inst_num = inst_vlan.rem;
	}
	(void) strcat(specified_physdev, specified_drv);
	(void) sprintf(specified_physdev + strlen(specified_drv), "%d",
	    inst_num);

	/* Iterate through the list of configured adapters */
	adaptersi = curradaptersi = clconf_node_get_adapters(
	    (clconf_node_t *)node);
	while ((adapter = clconf_iter_get_current(curradaptersi)) != NULL) {

		/*
		 * adaptername must match either the device name or the
		 * full name.
		 */
		dev_name = clconf_obj_get_property(adapter,
		    PROP_ADAPTER_DEVICE_NAME);
		dev_instance = clconf_obj_get_property(adapter,
		    PROP_ADAPTER_DEVICE_INSTANCE);

		phys_devname = (char *)calloc(1, strlen(dev_name) +
		    strlen(dev_instance) + 1);
		if (phys_devname == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		(void) strcat(phys_devname, dev_name);
		(void) strcat(phys_devname, dev_instance);
		if (strcmp(adaptername, phys_devname) == 0) {
			*adapterp = adapter;
			free(phys_devname);
			rstatus = SCCONF_NOERR;
			goto cleanup;
		}

		svlan = clconf_obj_get_property(adapter, PROP_ADAPTER_VLAN_ID);
		if (svlan == NULL) {
			clconf_iter_advance(curradaptersi);
			free(phys_devname);
			continue;
		}

		expanded_adaptername = (char *)calloc(1, strlen(dev_name) +
		    strlen(dev_instance) + strlen(svlan) + 3);
		if (expanded_adaptername == NULL) {
			rstatus = SCCONF_ENOMEM;
			free(phys_devname);
			goto cleanup;
		}

		(void) strcat(expanded_adaptername, dev_name);
		vlan_id = atoi(svlan);
		if (vlan_id == 0) {
			(void) strcat(expanded_adaptername + strlen(dev_name),
			    dev_instance);
		} else {
			instance_num = atoi(dev_instance);
			composite_num = vlan_id * VLAN_MULTIPLIER +
			    instance_num;
			(void) sprintf(expanded_adaptername + strlen(dev_name),
			    "%d", composite_num);
		}

		if (strcmp(adaptername, expanded_adaptername) == 0) {
			*adapterp = adapter;
			free(expanded_adaptername);
			free(phys_devname);
			rstatus = SCCONF_NOERR;
			goto cleanup;
		}

		/*
		 * Could not find the adapter, see if it is being used as
		 * a different VLAN device.
		 */
		if (strcmp(specified_physdev, phys_devname) == 0) {
			free(expanded_adaptername);
			free(phys_devname);
			rstatus = SCCONF_EINUSE;
			goto cleanup;
		}

		free(expanded_adaptername);
		free(phys_devname);
		clconf_iter_advance(curradaptersi);
	}

	/* The adapter was not found */
	rstatus = SCCONF_ENOEXIST;

cleanup:
	/* Release iterator */
	if (adaptersi != (clconf_iter_t *)0)
		clconf_iter_release(adaptersi);

	if (specified_drv)
		free(specified_drv);
	if (specified_ins)
		free(specified_ins);
	if (specified_physdev)
		free(specified_physdev);

	return (rstatus);
}

/*
 * conf_get_adaptername
 *
 * Return the adapter name as derived from its components: device name,
 * instance number and vlan id if existing. If nametype is set to PHYS_NAME
 * then just return the physical part (i.e., ce2), else return the full
 * name (i.e., ce2002 etc).
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM 		- Out of memory
 *	SCCONF_EINVAL		- Invalid parameter
 */
static scconf_errno_t
conf_get_adaptername(clconf_obj_t *adapter, int nametype, char **adaptername)
{
	const char *svlan = NULL;
	const char *dev_name = NULL;
	const char *dev_instance = NULL;
	int vlan_id = 0;
	size_t vlan_strlen = 0;
	int instance_num = 0;
	int composite_num = 0;
	char *expanded_adaptername = NULL;

	/* Check arguments */
	if (adapter == NULL || adaptername == NULL)
		return (SCCONF_EINVAL);

	if (clconf_obj_get_objtype(adapter) != CL_ADAPTER)
		return (SCCONF_EINVAL);

	dev_name = clconf_obj_get_property(adapter, PROP_ADAPTER_DEVICE_NAME);
	dev_instance = clconf_obj_get_property(adapter,
	    PROP_ADAPTER_DEVICE_INSTANCE);
	svlan = clconf_obj_get_property(adapter, PROP_ADAPTER_VLAN_ID);
	if (svlan == NULL) {
		vlan_id = 0;
		vlan_strlen = 0;
	} else {
		vlan_id = atoi(svlan);
		vlan_strlen = strlen(svlan);
	}

	expanded_adaptername = (char *)calloc(1, strlen(dev_name) +
	    strlen(dev_instance) + vlan_strlen + 3);
	if (expanded_adaptername == NULL)
		return (SCCONF_ENOMEM);

	(void) strcat(expanded_adaptername, dev_name);

	if ((vlan_id == 0) || (nametype == PHYS_NAME)) {
		(void) strcat(expanded_adaptername + strlen(dev_name),
		    dev_instance);
	} else {
		instance_num = atoi(dev_instance);
		composite_num = vlan_id * VLAN_MULTIPLIER + instance_num;
		(void) sprintf(expanded_adaptername + strlen(dev_name),
		    "%d", composite_num);
	}

	*adaptername = expanded_adaptername;
	return (SCCONF_NOERR);
}

/*
 * conf_get_cpointobj
 *
 * Return the cpoint object in "cpointp" for the given "cpointname".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the cpoint does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_get_cpointobj(clconf_cluster_t *clconf, char *cpointname,
    clconf_obj_t **cpointp)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_iter_t *currcpointsi, *cpointsi = (clconf_iter_t *)0;
	clconf_obj_t *cpoint;
	const char *name;

	/* Check arguments */
	if (clconf == NULL || cpointname == NULL || cpointp == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of configured connection points */
	cpointsi = currcpointsi = clconf_cluster_get_bbs(clconf);
	while ((cpoint = clconf_iter_get_current(currcpointsi)) != NULL) {
		name = clconf_obj_get_name(cpoint);

		/* Search for configured connection point */
		if (name != NULL && strcmp(cpointname, name) == 0) {
			*cpointp = cpoint;
			rstatus = SCCONF_NOERR;
			goto cleanup;
		}
		clconf_iter_advance(currcpointsi);
	}

	/* The cpoint was not found */
	rstatus = SCCONF_ENOEXIST;

cleanup:
	/* Release iterator */
	if (cpointsi != (clconf_iter_t *)0)
		clconf_iter_release(cpointsi);

	return (rstatus);
}

/*
 * conf_get_cableobj
 *
 * Return the cable object in "cablep" for the endpoint(s) ("epoint<N>")
 * given.  Only one endpoint is required, but must be specified in
 * "epoint1".   The first match is returned.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the cable does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_get_cableobj(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint1, scconf_cltr_epoint_t *epoint2,
    clconf_obj_t **cablep)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_iter_t *currcablesi, *cablesi = (clconf_iter_t *)0;
	clconf_obj_t *cable;
	clconf_obj_t *port_obj, *port_parent_obj, *node_obj;
	const char *port_name, *port_parent_name;
	clconf_objtype_t port_parent_type;
	scconf_cltr_epoint_t *epoints[2];
	scconf_cltr_epoint_t *epoint;
	scconf_nodeid_t nodeids[2];
	scconf_nodeid_t *nodeidp;
	int id;
	int e1, e2, nummatched;
	char *full_devname = NULL;
	char *phys_devname = NULL;

	/* Check arguments */
	if (clconf == NULL || cablep == NULL ||
	    epoint1 == NULL || epoint1->scconf_cltr_epoint_devicename == NULL ||
	    (epoint2 && epoint2->scconf_cltr_epoint_devicename == NULL))
		return (SCCONF_EINVAL);

	/* Make sure we have different endpoints */
	if (conf_epointcmp(clconf, epoint1, epoint2) == 0)
		return (SCCONF_EINVAL);

	/* Initialize endpoints array */
	epoints[0] = epoint1;
	epoints[1] = epoint2;

	/* Verify types and nodes */
	for (e1 = 1;  e1 <= 2;  ++e1) {
		epoint = epoints[e1 - 1];
		nodeidp = &nodeids[e1 - 1];

		/* If epoint is not set, clear the node ID */
		if (epoint == NULL) {
			*nodeidp = 0;
			continue;
		}

		/* Adapter or blackbox */
		switch (epoint->scconf_cltr_epoint_type) {
		case SCCONF_CLTR_EPOINT_TYPE_ADAPTER:
			if (epoint->scconf_cltr_epoint_nodename == NULL) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}

			/* For adapter, get the node ID */
			rstatus = conf_get_nodeid(clconf,
			    epoint->scconf_cltr_epoint_nodename, nodeidp);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			break;

		case SCCONF_CLTR_EPOINT_TYPE_CPOINT:
			*nodeidp = (scconf_nodeid_t)0;
			break;

		default:
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}
	}

	/* Iterate through the list of cables */
	cablesi = currcablesi = clconf_cluster_get_cables(clconf);
	while ((cable = clconf_iter_get_current(currcablesi)) != NULL) {

		/* Check both endpoint arguments ... */
		nummatched = 0;
		for (e1 = 1;  e1 <= 2;  ++e1) {
			epoint = epoints[e1 - 1];
			nodeidp = &nodeids[e1 - 1];

			if (epoint == NULL)
				continue;

			/* Against both cable ports ... */
			for (e2 = 1;  e2 <= 2;  ++e2) {

				/* get the port and name */
				port_obj =
				    (clconf_obj_t *)clconf_cable_get_endpoint(
				    (clconf_cable_t *)cable, e2);
				if (port_obj == NULL) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}
				port_name = clconf_obj_get_name(port_obj);

				/* get the parent of port */
				port_parent_obj = clconf_obj_get_parent(
				    port_obj);
				if (port_parent_obj == NULL) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}

				/* name of parent of port */
				port_parent_name = clconf_obj_get_name(
				    port_parent_obj);
				if (port_parent_name == NULL) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}

				/* type of parent of port */
				port_parent_type = clconf_obj_get_objtype(
				    port_parent_obj);
				if (port_parent_type != CL_ADAPTER &&
				    port_parent_type != CL_BLACKBOX) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}

				/* compare type */
				if ((port_parent_type == CL_ADAPTER &&
				    epoint->scconf_cltr_epoint_type !=
				    SCCONF_CLTR_EPOINT_TYPE_ADAPTER) ||
				    (port_parent_type == CL_BLACKBOX &&
				    epoint->scconf_cltr_epoint_type !=
				    SCCONF_CLTR_EPOINT_TYPE_CPOINT))
					continue;

				/* compare port name, if given */
				if ((!port_name &&
				    epoint->scconf_cltr_epoint_portname))
					continue;
				if (epoint->scconf_cltr_epoint_portname &&
				    port_name &&
				    strcmp(port_name,
				    epoint->scconf_cltr_epoint_portname) != 0)
					continue;

				/*
				 * Compare device name, in case of adapter,
				 * check both physical name and full name.
				 */
				if (port_parent_type == CL_ADAPTER) {
					rstatus = conf_get_adaptername(
					    port_parent_obj, FULL_NAME,
					    &full_devname);
					if (rstatus != SCCONF_NOERR)
						goto cleanup;
					rstatus = conf_get_adaptername(
					    port_parent_obj, PHYS_NAME,
					    &phys_devname);
					if (rstatus != SCCONF_NOERR)
						goto cleanup;

					if ((strcmp(full_devname, epoint->
					    scconf_cltr_epoint_devicename)
					    != 0) && (strcmp(phys_devname,
					    epoint->
					    scconf_cltr_epoint_devicename)
					    != 0)) {
						free(full_devname);
						free(phys_devname);
						full_devname = phys_devname =
						    NULL;
						continue;
					}
					free(full_devname);
					free(phys_devname);
					full_devname = phys_devname = NULL;
				} else {
					if (strcmp(port_parent_name,
					    epoint->
					    scconf_cltr_epoint_devicename) != 0)
						continue;
				}

				/* if adapter, get node and ID */
				if (port_parent_type == CL_ADAPTER) {
					node_obj = clconf_obj_get_parent(
					    port_parent_obj);
					if (node_obj == NULL) {
						rstatus = SCCONF_EUNEXPECTED;
						goto cleanup;
					}
					id = clconf_obj_get_id(node_obj);
					if (id == -1) {
						rstatus = SCCONF_EUNEXPECTED;
						goto cleanup;
					}

					/* compare node IDs */
					if (id != (int)*nodeidp)
						continue;
				}

				/* Match found - exit loop w/ e2 <= 2 */
				break;
			}
			/* Found a match? */
			if (e2 <= 2)
				++nummatched;
		}
		/* If match, set cablep */
		if ((epoint2 == NULL && nummatched == 1) ||
		    (epoint2 != NULL && nummatched == 2)) {
			*cablep = cable;
			rstatus = SCCONF_NOERR;
			goto cleanup;
		}
		clconf_iter_advance(currcablesi);
	}

	/* The cable was not found */
	rstatus = SCCONF_ENOEXIST;

cleanup:
	/* Release iterator */
	if (cablesi != (clconf_iter_t *)0)
		clconf_iter_release(cablesi);

	return (rstatus);
}

/*
 * conf_create_port
 *
 * Return a port object in "portp" for the given "epoint".
 *
 * If the port exists, a check is made to verify that it is not
 * already in use by a cable.  If it is in use, SCCONF_EINUSE is returned.
 * If the port does not exist, it is created and added to the configuration.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINUSE		- the port is already in use
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_create_port(clconf_cluster_t *clconf, scconf_cltr_epoint_t *epoint,
    clconf_port_t **portp, char **messages)
{
	/* Check arguments */
	if (clconf == NULL || epoint == NULL ||
	    epoint->scconf_cltr_epoint_devicename == NULL ||
	    portp == NULL)
		return (SCCONF_EINVAL);

	/* Adapter or cpoint? */
	switch (epoint->scconf_cltr_epoint_type) {
	case SCCONF_CLTR_EPOINT_TYPE_ADAPTER:
		return (conf_create_port_adapter(clconf, epoint, portp,
		    messages));

	case SCCONF_CLTR_EPOINT_TYPE_CPOINT:
		return (conf_create_port_cpoint(clconf, epoint, portp,
		    messages));

	default:
		return (SCCONF_EINVAL);
	}
}

/*
 * conf_create_port_adapter
 *
 * Return an adapter port object in "portp" for the given "epoint".
 *
 * If the port exists, a check is made to verify that it is not
 * already in use by a cable.  If it is in use, SCCONF_EINUSE is returned.
 * If the port does not exist, it is created and added to the configuration.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINUSE		- the port is already in use
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_create_port_adapter(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint, clconf_port_t **portp, char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_errnum_t clconf_err;
	clconf_iter_t *currportsi, *portsi = (clconf_iter_t *)0;
	clconf_obj_t *port = NULL;
	clconf_obj_t *adapter;
	clconf_obj_t *node;
	scconf_nodeid_t nodeid;
	char *portname;
	const char *name;
	uint_t statechk = 0;

	/* Check arguments */
	if (clconf == NULL || epoint == NULL ||
	    epoint->scconf_cltr_epoint_nodename == NULL ||
	    epoint->scconf_cltr_epoint_devicename == NULL ||
	    epoint->scconf_cltr_epoint_portname == NULL ||
	    portp == NULL)
		return (SCCONF_EINVAL);

	/* Get the nodeid */
	rstatus = conf_get_nodeid(clconf,
	    epoint->scconf_cltr_epoint_nodename, &nodeid);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Find the node */
	rstatus = conf_get_nodeobj(clconf, nodeid, &node);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Find the adapter */
	rstatus = conf_get_adapterobj(node,
	    epoint->scconf_cltr_epoint_devicename, &adapter);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* port name */
	portname = epoint->scconf_cltr_epoint_portname;

	/* Iterate through the list of configured adapter ports */
	portsi = currportsi = clconf_adapter_get_ports(
	    (clconf_adapter_t *)adapter);
	while ((port = clconf_iter_get_current(currportsi)) != NULL) {

		/* get the name */
		name = clconf_obj_get_name(port);

		/* Search for port name */
		if (name != NULL && strcmp(portname, name) == 0)
			break;

		clconf_iter_advance(currportsi);
	}

	/*
	 * If we did not find a port, create one and add it
	 * to the adapter.
	 */
	if (port == NULL) {
		port = (clconf_obj_t *)clconf_port_create();
		if (port == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* add the port to the adapter */
		clconf_err = clconf_adapter_add_port(
		    (clconf_adapter_t *)adapter, (clconf_port_t *)port);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
				rstatus = SCCONF_EINVAL;
				goto cleanup;

			case CL_INVALID_ID:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* set the port name */
		clconf_obj_set_name(port, portname);

		/* make sure that the port object is okay */
		rstatus = conf_obj_check(port, messages);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

	/* If we did find a port, make sure that it is not in use */
	} else {
		rstatus = conf_check_notinuse_bycable(clconf, port, statechk);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;
	}

cleanup:
	/* Release iterator */
	if (portsi != (clconf_iter_t *)0)
		clconf_iter_release(portsi);

	/* Set the port */
	if (rstatus == SCCONF_NOERR)
		*portp = (clconf_port_t *)port;

	return (rstatus);
}

/*
 * conf_create_port_cpoint
 *
 * Return a cpoint port object in "portp" for the given "epoint".
 *
 * If the port exists, a check is made to verify that it is not
 * already in use by a cable.  If it is in use, SCCONF_EINUSE is returned.
 * If the port does not exist, it is created and added to the configuration.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINUSE		- the port is already in use
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_create_port_cpoint(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint, clconf_port_t **portp, char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_errnum_t clconf_err;
	clconf_iter_t *currportsi, *portsi = (clconf_iter_t *)0;
	clconf_obj_t *port = NULL;
	clconf_obj_t *cpoint;
	char *portname;
	const char *name;
	uint_t statechk = 0;

	/* Check arguments */
	if (clconf == NULL || epoint == NULL ||
	    epoint->scconf_cltr_epoint_devicename == NULL ||
	    epoint->scconf_cltr_epoint_portname == NULL ||
	    portp == NULL)
		return (SCCONF_EINVAL);

	/* Find the cpoint */
	rstatus = conf_get_cpointobj(clconf,
	    epoint->scconf_cltr_epoint_devicename, &cpoint);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* port name */
	portname = epoint->scconf_cltr_epoint_portname;

	/* Iterate through the list of configured cpoint ports */
	portsi = currportsi = clconf_bb_get_ports((clconf_bb_t *)cpoint);
	while ((port = clconf_iter_get_current(currportsi)) != NULL) {

		/* get the name of the port */
		name = clconf_obj_get_name(port);

		/* Search for port name */
		if (name != NULL && strcmp(portname, name) == 0)
			break;

		clconf_iter_advance(currportsi);
	}

	/*
	 * If we did not find a port, create one and add it
	 * to the cpoint.
	 */
	if (port == NULL) {
		port = (clconf_obj_t *)clconf_port_create();
		if (port == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* add the port to the cpoint */
		clconf_err = clconf_bb_add_port(
		    (clconf_bb_t *)cpoint, (clconf_port_t *)port);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
				rstatus = SCCONF_EINVAL;
				goto cleanup;

			case CL_INVALID_ID:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* set the port name */
		if (portname != NULL)
			clconf_obj_set_name(port, portname);

		/* make sure that the port object is okay */
		rstatus = conf_obj_check(port, messages);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

	/* If we did find a port, make sure that it is not in use */
	} else {
		rstatus = conf_check_notinuse_bycable(clconf, port, statechk);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;
	}

cleanup:
	/* Release iterator */
	if (portsi != (clconf_iter_t *)0)
		clconf_iter_release(portsi);

	/* Set the port */
	if (rstatus == SCCONF_NOERR)
		*portp = (clconf_port_t *)port;

	return (rstatus);
}

/*
 * conf_get_default_portname
 *
 * Return a default portname.
 *
 * A default portname exists only when there is a single named port
 * associated with the device.   Otherwise, when there is more than
 * one named port, there is no default.
 *
 * If it is not possible to find a default, the function returns NULL.
 *
 * It is the responsibility of the caller to free portname memory.
 */
static char *
conf_get_default_portname(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint)
{
	/* Check arguments */
	if (clconf == NULL || epoint == NULL ||
	    epoint->scconf_cltr_epoint_devicename == NULL ||
	    epoint->scconf_cltr_epoint_portname != NULL)
		return (NULL);

	/* Adapter or cpoint? */
	switch (epoint->scconf_cltr_epoint_type) {
	case SCCONF_CLTR_EPOINT_TYPE_ADAPTER:
		return (conf_get_default_portname_adapter(clconf, epoint));

	case SCCONF_CLTR_EPOINT_TYPE_CPOINT:
		return (conf_get_default_portname_cpoint(clconf, epoint));

	default:
		break;
	}

	return (NULL);
}

/*
 * conf_get_default_portname_adapter
 *
 * Return a default portname for an adapter.
 * A default portname exists only when there is a single named port
 * associated with the adapter.   Otherwise, when there is more than
 * one named port, there is no default.
 *
 * If it is not possible to find a default, the function returns NULL.
 *
 * It is the responsibility of the caller to free portname memory.
 */
static char *
conf_get_default_portname_adapter(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint)
{
	scconf_errno_t status;
	clconf_iter_t *currportsi, *portsi = (clconf_iter_t *)0;
	clconf_obj_t *port;
	clconf_obj_t *adapter;
	clconf_obj_t *node;
	scconf_nodeid_t nodeid;
	const char *name, *savename = NULL;
	int portcount;

	/* Check arguments */
	if (clconf == NULL || epoint == NULL ||
	    epoint->scconf_cltr_epoint_nodename == NULL ||
	    epoint->scconf_cltr_epoint_devicename == NULL ||
	    epoint->scconf_cltr_epoint_portname != NULL)
		return (NULL);

	/* Get the nodeid */
	status = conf_get_nodeid(clconf,
	    epoint->scconf_cltr_epoint_nodename, &nodeid);
	if (status != SCCONF_NOERR)
		return (NULL);

	/* Find the node */
	status = conf_get_nodeobj(clconf, nodeid, &node);
	if (status != SCCONF_NOERR)
		return (NULL);

	/* Find the adapter */
	status = conf_get_adapterobj(node,
	    epoint->scconf_cltr_epoint_devicename, &adapter);
	if (status != SCCONF_NOERR)
		return (NULL);

	/* Count the named ports */
	portcount = 0;
	portsi = currportsi = clconf_adapter_get_ports(
	    (clconf_adapter_t *)adapter);
	while ((port = clconf_iter_get_current(currportsi)) != NULL) {

		/* get the name */
		name = clconf_obj_get_name(port);
		if (name != NULL) {
			savename = name;
			++portcount;
		}

		clconf_iter_advance(currportsi);
	}

	/* Release iterator */
	if (portsi != (clconf_iter_t *)0)
		clconf_iter_release(portsi);

	/* If there is just the one name, return a copy */
	if (portcount == 1)
		return (strdup(savename));

	return (NULL);
}

/*
 * conf_get_default_portname_cpoint
 *
 * Return a default portname for a cpoint.
 * A default portname exists only when there is a single named port
 * associated with the cpoint.   Otherwise, when there is more than
 * one named port, there is no default.
 *
 * If it is not possible to find a default, the function returns NULL.
 *
 * It is the responsibility of the caller to free portname memory.
 */
static char *
conf_get_default_portname_cpoint(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint)
{
	scconf_errno_t status;
	clconf_iter_t *currportsi, *portsi = (clconf_iter_t *)0;
	clconf_obj_t *port;
	clconf_obj_t *cpoint;
	const char *name, *savename = NULL;
	int portcount;

	/* Check arguments */
	if (clconf == NULL || epoint == NULL ||
	    epoint->scconf_cltr_epoint_devicename == NULL ||
	    epoint->scconf_cltr_epoint_portname != NULL)
		return (NULL);

	/* Find the cpoint */
	status = conf_get_cpointobj(clconf,
	    epoint->scconf_cltr_epoint_devicename, &cpoint);
	if (status != SCCONF_NOERR)
		return (NULL);

	/* Count the named ports */
	portcount = 0;
	portsi = currportsi = clconf_bb_get_ports((clconf_bb_t *)cpoint);
	while ((port = clconf_iter_get_current(currportsi)) != NULL) {

		/* get the name */
		name = clconf_obj_get_name(port);
		if (name != NULL) {
			savename = name;
			++portcount;
		}

		clconf_iter_advance(currportsi);
	}

	/* Release iterator */
	if (portsi != (clconf_iter_t *)0)
		clconf_iter_release(portsi);

	/* If there is just the one name, return a copy */
	if (portcount == 1)
		return (strdup(savename));

	return (NULL);
}

/*
 * conf_generate_default_portname
 *
 * Generate a default portname for use with an endpoint on a new cable.
 * Two endpoints must be given;  the returned portname is for the
 * first endpoint, "epoint1".
 *
 * When creating a new cable, the default portname for an adapter
 * endpoint is zero.   And, for a blackbox, it is the nodeid on
 * the other end of the cable.   There is no default for
 * blackbox-to-blackbox cables.
 *
 * This function will generate portnames which may already in use by
 * this or another cable.
 *
 * If it is not possible to generate a default, the function returns NULL.
 *
 * It is the responsibility of the caller to free portname memory.
 */
static char *
conf_generate_default_portname(clconf_cluster_t *clconf,
    scconf_cltr_epoint_t *epoint1, scconf_cltr_epoint_t *epoint2)
{
	scconf_errno_t status;
	scconf_nodeid_t nodeid;
	char snodeid[10];

	/* Check arguments */
	if (clconf == NULL || epoint1 == NULL || epoint2 == NULL ||
	    epoint1->scconf_cltr_epoint_devicename == NULL ||
	    epoint2->scconf_cltr_epoint_devicename == NULL ||
	    epoint1->scconf_cltr_epoint_portname != NULL)
		return (NULL);

	/* Adapter or cpoint? */
	switch (epoint1->scconf_cltr_epoint_type) {
	case SCCONF_CLTR_EPOINT_TYPE_ADAPTER:
		return (strdup("0"));

	case SCCONF_CLTR_EPOINT_TYPE_CPOINT:

		/* Make sure the other end is attached to a node */
		if (epoint2->scconf_cltr_epoint_type !=
		    SCCONF_CLTR_EPOINT_TYPE_ADAPTER ||
		    epoint2->scconf_cltr_epoint_nodename == NULL)
			return (NULL);

		/* Get the nodeid of the other end */
		status = conf_get_nodeid(clconf,
		    epoint2->scconf_cltr_epoint_nodename, &nodeid);
		if (status != SCCONF_NOERR)
			return (NULL);

		/* Return the default */
		(void) sprintf(snodeid, "%d", nodeid);
		return (strdup(snodeid));

	default:
		break;
	}

	return (NULL);
}

/*
 * conf_epointcmp
 *
 * Compare endpoints, and return zero if they are the same.
 *
 * Possible return values:
 *
 *	0	the endpoints are the same
 *	1	the endpoints are not the same
 */
static int
conf_epointcmp(clconf_cluster_t *clconf, scconf_cltr_epoint_t *epoint1,
    scconf_cltr_epoint_t *epoint2)
{
	scconf_errno_t nodeid1_status, nodeid2_status;
	scconf_nodeid_t nodeid1, nodeid2;

	/* Neither set, they match */
	if (epoint1 == NULL && epoint2 == NULL)
		return (0);

	/* Only one set, they don't match */
	if (epoint1 == NULL || epoint2 == NULL)
		return (1);

	/* Same type? */
	if (epoint1->scconf_cltr_epoint_type !=
	    epoint2->scconf_cltr_epoint_type)
		return (1);

	/* Same device? */
	if ((epoint1->scconf_cltr_epoint_devicename != NULL &&
	    epoint2->scconf_cltr_epoint_devicename == NULL) ||
	    (epoint1->scconf_cltr_epoint_devicename == NULL &&
	    epoint2->scconf_cltr_epoint_devicename != NULL))
		return (1);
	if (epoint1->scconf_cltr_epoint_devicename != NULL &&
	    strcmp(epoint1->scconf_cltr_epoint_devicename,
	    epoint2->scconf_cltr_epoint_devicename) != 0)
		return (1);

	/* Same port? */
	if ((epoint1->scconf_cltr_epoint_portname != NULL &&
	    epoint2->scconf_cltr_epoint_portname == NULL) ||
	    (epoint1->scconf_cltr_epoint_portname == NULL &&
	    epoint2->scconf_cltr_epoint_portname != NULL))
		return (1);
	if (epoint1->scconf_cltr_epoint_portname != NULL &&
	    strcmp(epoint1->scconf_cltr_epoint_portname,
	    epoint2->scconf_cltr_epoint_portname) != 0)
		return (1);

	/* If adapter, same node? */
	if (epoint1->scconf_cltr_epoint_type ==
	    SCCONF_CLTR_EPOINT_TYPE_ADAPTER) {
		if ((epoint1->scconf_cltr_epoint_nodename != NULL &&
		    epoint2->scconf_cltr_epoint_nodename == NULL) ||
		    (epoint1->scconf_cltr_epoint_nodename == NULL &&
		    epoint2->scconf_cltr_epoint_nodename != NULL))
			return (1);
		nodeid1_status = conf_get_nodeid(clconf,
		    epoint1->scconf_cltr_epoint_nodename, &nodeid1);
		nodeid2_status = conf_get_nodeid(clconf,
		    epoint2->scconf_cltr_epoint_nodename, &nodeid2);
		if (nodeid1_status != nodeid2_status)
			return (1);
		if (nodeid1_status == SCCONF_NOERR) {
			if (nodeid1 != nodeid2)
				return (1);
		} else if (nodeid1_status == SCCONF_ENOEXIST) {
			if (strcmp(epoint1->scconf_cltr_epoint_nodename,
			    epoint2->scconf_cltr_epoint_nodename) != 0)
				return (1);
		}
	}

	return (0);
}

/*
 * conf_check_notinuse_bycable
 *
 * Check to see if the given node, adapter, cpoint, or port object ("clobj")
 * is configured as part of a cable definition.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINUSE		- the object is in use
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_check_notinuse_bycable(clconf_cluster_t *clconf,
    clconf_obj_t *clobj, uint_t statechk)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_obj_t *parent_obj;
	clconf_obj_t *grandparent_obj;
	int id;
	int parent_id = -1;
	int grandparent_id = -1;
	clconf_iter_t *currcablesi, *cablesi = (clconf_iter_t *)0;
	clconf_obj_t *cable;
	int port_id, port_parent_id, node_id = -1;
	clconf_obj_t *port_obj, *port_parent_obj, *node_obj;
	clconf_objtype_t clobj_type, port_parent_type;
	int e;
	const char *state = NULL;

	/* Check arguments */
	if (clconf == NULL || clobj == NULL)
		return (SCCONF_EINVAL);

	/* Make sure the object is a type we can check */
	clobj_type = clconf_obj_get_objtype(clobj);
	if (clobj_type != CL_NODE &&
	    clobj_type != CL_ADAPTER &&
	    clobj_type != CL_BLACKBOX &&
	    clobj_type != CL_PORT)
		return (SCCONF_EINVAL);

	/* Get the id of the object */
	id = clconf_obj_get_id(clobj);
	if (id == -1)
		return (SCCONF_EUNEXPECTED);

	/* If this is an adapter, blackbox or port, get the parent */
	if (clobj_type == CL_ADAPTER || clobj_type == CL_BLACKBOX ||
	    clobj_type == CL_PORT) {
		parent_obj = clconf_obj_get_parent(clobj);
		if (parent_obj == NULL)
			return (SCCONF_EUNEXPECTED);
		parent_id = clconf_obj_get_id(parent_obj);
		if (parent_id == -1)
			return (SCCONF_EUNEXPECTED);

		/* If the parent of the object is an adapter, get its parent */
		if (clconf_obj_get_objtype(parent_obj) == CL_ADAPTER) {
			grandparent_obj = clconf_obj_get_parent(parent_obj);
			if (grandparent_obj == NULL)
				return (SCCONF_EUNEXPECTED);
			grandparent_id = clconf_obj_get_id(grandparent_obj);
			if (grandparent_id == -1)
				return (SCCONF_EUNEXPECTED);
		}
	}

	/* Iterate through the list of cables */
	cablesi = currcablesi = clconf_cluster_get_cables(clconf);
	while ((cable = clconf_iter_get_current(currcablesi)) != NULL) {

		/* Check both endpoints */
		for (e = 1;  e <= 2;  ++e) {

			/* get port and port ID */
			port_obj = (clconf_obj_t *)clconf_cable_get_endpoint(
			    (clconf_cable_t *)cable, e);
			if (port_obj == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			port_id = clconf_obj_get_id(port_obj);
			if (port_id == -1) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* get parent of port and ID of parent of port */
			port_parent_obj = clconf_obj_get_parent(port_obj);
			if (port_parent_obj == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			port_parent_id = clconf_obj_get_id(port_parent_obj);
			if (port_parent_id == -1) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* what type of port is this? */
			port_parent_type = clconf_obj_get_objtype(
			    port_parent_obj);
			if (port_parent_type != CL_ADAPTER &&
			    port_parent_type != CL_BLACKBOX) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* if adapter type, get the node and node ID */
			if (port_parent_type == CL_ADAPTER) {
				node_obj = clconf_obj_get_parent(
				    port_parent_obj);
				if (node_obj == NULL) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}
				node_id = clconf_obj_get_id(node_obj);
				if (node_id == -1) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}
			}

			/* Is our object in use? */
			switch (clobj_type) {
			case CL_NODE:
				if (port_parent_type == CL_ADAPTER &&
				    id == node_id)
					rstatus = SCCONF_EINUSE;
				break;

			case CL_ADAPTER:
				if (port_parent_type == CL_ADAPTER &&
				    id == port_parent_id &&
				    parent_id == node_id)
					rstatus = SCCONF_EINUSE;
				break;

			case CL_BLACKBOX:
				if (port_parent_type == CL_BLACKBOX &&
				    id == port_parent_id)
					rstatus = SCCONF_EINUSE;
				break;

			case CL_PORT:
				if (port_parent_type == CL_ADAPTER) {
					if (id == port_id &&
					    parent_id == port_parent_id &&
					    grandparent_id == node_id)
						rstatus = SCCONF_EINUSE;
				} else if (port_parent_type == CL_BLACKBOX) {
					if (id == port_id &&
					    parent_id == port_parent_id)
						rstatus = SCCONF_EINUSE;
				}
				break;

			default:
				rstatus = SCCONF_EUNEXPECTED;
				break;
			} /*lint !e788 */
			/*
			 * If statechk is set, check for the cable state
			 * and if it is disabled, consider it as not in use.
			 * This allows "clintr disable" to disable the
			 * adapter and switch endpoints, if it is part of
			 * the cable that is in disabled state.
			 */
			if (statechk && rstatus == SCCONF_EINUSE) {
				state = clconf_obj_get_state(cable);
				if (strcmp(state, "disabled") == 0) {
					rstatus = SCCONF_NOERR;
					goto cleanup;
				}
			}
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		clconf_iter_advance(currcablesi);
	}

cleanup:
	/* Release iterator */
	if (cablesi != (clconf_iter_t *)0)
		clconf_iter_release(cablesi);

	return (rstatus);
}

/*
 * conf_check_isolation
 *
 * Compare the new clconf to the existing clconf.   And, if the new
 * configuration isolates nodes which were not previously isolated,
 * check to see if any such node is an active cluster member.  If active,
 * return SCCONF_EBUSY.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EBUSY		- attempt to isolate busy node
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_check_isolation(clconf_cluster_t *existing_clconf,
    clconf_cluster_t *new_clconf)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	int *existing_isolated_ids = (int *)0;
	int *new_isolated_ids = (int *)0;
	int *tmp_nodeids;
	int **nodeidsp;
	size_t i;
	int j, id;
	uint_t ismember;
	clconf_cluster_t *clconf;

	/* Check arguments */
	if (existing_clconf == NULL || new_clconf == NULL)
		return (SCCONF_EINVAL);

	/* If we are the only node left in the cluster, anything goes */
	if (scconf_membercount() == 1)
		return (SCCONF_NOERR);

	/* Get the list of isolated nodeids for both existing and new */
	for (clconf = existing_clconf;  clconf; ) {
		if (clconf == existing_clconf) {
			nodeidsp = &existing_isolated_ids;
		} else {
			nodeidsp = &new_isolated_ids;
		}

		/* Iterate through the list of configured nodes */
		nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
		while ((node = clconf_iter_get_current(currnodesi)) != NULL) {

			rstatus = conf_isnode_isolated(clconf, node);
			if (rstatus == SCCONF_EBUSY) {

				/* Reset return status */
				rstatus = SCCONF_NOERR;

				/* Get the nodeid */
				id = clconf_obj_get_id(node);
				if (id == -1) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}

				/* Add it to the list */
				i = 0;
				if (*nodeidsp)
					while ((*nodeidsp)[i])
						++i;
				i += 2;
				tmp_nodeids = (int *)realloc(*nodeidsp, i);
				if (tmp_nodeids == (int *)0) {
					rstatus = SCCONF_ENOMEM;
					goto cleanup;
				}
				tmp_nodeids[i - 1] = 0; /*lint !e661 */
				tmp_nodeids[i - 2] = id;
				*nodeidsp = tmp_nodeids;

			} else if (rstatus != SCCONF_NOERR) {
				goto cleanup;
			}

			clconf_iter_advance(currnodesi);
		}

		/* Do both clconfs */
		if (clconf == existing_clconf) {
			clconf = new_clconf;
		} else {
			clconf = NULL;
		}
	}

	/* Make sure any newly isolate node IDs are not in the cluster */
	if (new_isolated_ids) {
		for (i = 0;  new_isolated_ids[i];  ++i) {

			/* See if nodeid exists only in the "new" list */
			id = 0;
			if (!existing_isolated_ids) {
				id = new_isolated_ids[i];
			} else {
				for (j = 0;  existing_isolated_ids[j];  ++j) {
					if (new_isolated_ids[i] ==
					    existing_isolated_ids[j]) {
						break;
					}
				}
				if (existing_isolated_ids[j] == 0)
					id = new_isolated_ids[i];
			}

			/* If we found a new nodeid, see if node is active */
			if (id != 0) {
				rstatus = scconf_ismember((scconf_nodeid_t)id,
				    &ismember);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
				if (ismember) {
					rstatus = SCCONF_EBUSY;
					goto cleanup;
				}
			}
		}
	}

	/* If we are here, it is not busy */
	rstatus = SCCONF_NOERR;

cleanup:
	/* Release iterator */
	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	/* Release arrays of nodeids */
	if (existing_isolated_ids)
		free(existing_isolated_ids);
	if (new_isolated_ids)
		free(new_isolated_ids);

	return (rstatus);
}


/*
 * conf_isnode_isolated
 *
 * Return SCCONF_EBUSY, if the indicated "node" is isolated from
 * the rest of the cluster as defined by the given "clconf".
 *
 * Note that complex comparisons are not performed.  Only the
 * states of the following object links are checked:
 *
 *	node 		=>	adapter
 *	adapter 	=>	port
 *	port 		=>	cable
 *	cable		=>	other port
 *	other port	=>	parent of this port (adapter or blackbox)
 *
 *	(blackbox	=>	configed state of any second blackbox port)
 *	(adapter	=>	configed state of the other node)
 *
 * This function may return SCCONF_NOERR even if a node is actually isolated.
 * For example, a blackbox may be entirely isolated through all other pathways
 * from any other cluster nodes, even when we do at least look for a
 * a second enabled port.  And, it is also possible for the user to
 * partition cluster nodes into undesireable configurations.  Also, we
 * are not able to verify that all configured paths are actually healthy
 * and viable.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EBUSY		- attempt to isolate busy node
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_isnode_isolated(clconf_cluster_t *clconf, clconf_obj_t *node)
{
	scconf_errno_t rstatus = SCCONF_EBUSY;
	scconf_errno_t status;
	clconf_iter_t *curradaptersi, *adaptersi = (clconf_iter_t *)0;
	clconf_iter_t *currportsi, *portsi = (clconf_iter_t *)0;
	clconf_obj_t *adapter, *port, *cable;
	scconf_cltr_epoint_t epoint;
	const char *nodename, *portname;
	char *adaptername;
	int i;

	/* Check arguments */
	if (clconf == NULL || node == NULL)
		return (SCCONF_EINVAL);

	/* Initialize the endpoint struct */
	bzero(&epoint, sizeof (epoint));
	epoint.scconf_cltr_epoint_type = SCCONF_CLTR_EPOINT_TYPE_ADAPTER;
	nodename = clconf_obj_get_name(node);
	if (nodename == NULL) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}
	epoint.scconf_cltr_epoint_nodename = strdup(nodename);
	if (epoint.scconf_cltr_epoint_nodename == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Iterate through the list of adapters on this node */
	adaptersi = curradaptersi = clconf_node_get_adapters(
	    (clconf_node_t *)node);
	while ((adapter = clconf_iter_get_current(curradaptersi)) != NULL) {

		/* If not enabled, go to the next */
		if (!clconf_obj_enabled(adapter)) {
			clconf_iter_advance(curradaptersi);
			continue;
		}

		/* Get and set the adapter name */
		rstatus = conf_get_adaptername(adapter, FULL_NAME,
		    &adaptername);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		if (adaptername == NULL) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		if (epoint.scconf_cltr_epoint_devicename)
			free(epoint.scconf_cltr_epoint_devicename);
		epoint.scconf_cltr_epoint_devicename = adaptername;

		/* Iterate through the adapter ports */
		portsi = currportsi = clconf_adapter_get_ports(
		    (clconf_adapter_t *)adapter);
		while ((port = clconf_iter_get_current(currportsi)) != NULL) {

			/* If not enabled, go to the next */
			if (!clconf_obj_enabled(port)) {
				clconf_iter_advance(currportsi);
				continue;
			}

			/* Get and set the port name */
			portname = clconf_obj_get_name(port);
			if (portname == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			if (epoint.scconf_cltr_epoint_portname)
				free(epoint.scconf_cltr_epoint_portname);
			epoint.scconf_cltr_epoint_portname = strdup(portname);
			if (epoint.scconf_cltr_epoint_portname == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			/* Find the matching cable */
			status = conf_get_cableobj(clconf, &epoint, NULL,
			    &cable);

			/* If no such cable, go to the next */
			if (status == SCCONF_ENOEXIST) {
				clconf_iter_advance(currportsi);
				continue;
			}

			/* If cable not enabled, go to the next */
			if (!clconf_obj_enabled(cable)) {
				clconf_iter_advance(currportsi);
				continue;
			}

			/* Make sure that the cable is "fully" enabled */
			i = conf_iscable_fullyenabled(clconf, cable);

			/* Not enabled, keep trying */
			if (i == 0) {
				clconf_iter_advance(currportsi);
				continue;
			}

			/* Unexpected error */
			if (i < 0) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* If we got this far, we must be "fully" enabled */
			rstatus = SCCONF_NOERR;
			goto cleanup;
		}

		/* Advance adapters iterator */
		clconf_iter_advance(curradaptersi);
	}

cleanup:
	/* Release iterators */
	if (portsi != (clconf_iter_t *)0)
		clconf_iter_release(portsi);
	if (adaptersi != (clconf_iter_t *)0)
		clconf_iter_release(adaptersi);

	/* Release memory associated with the port elements */
	conf_free_cltr_epoint_elements(&epoint);

	return (rstatus);
}

/*
 * conf_iscable_fullyenabled
 *
 * Return 1, if the indicated "cable", and both of its endpoints,
 * are "fully" enabled.   Otherwise, return 0 or -1.
 *
 * Possible return values:
 *
 *	1			- cable is "fully" enabled
 *	0			- cable is NOT "fully" enabled
 *	-1			- enexpected error
 */
static int
conf_iscable_fullyenabled(clconf_cluster_t *clconf, clconf_obj_t *cable)
{
	int rstatus = 0;
	clconf_obj_t *node, *other_port2_node;
	scconf_nodeid_t nodeid = (scconf_nodeid_t)-1;
	int other_port2_nodeid;
	clconf_obj_t *other_port2_adapter;
	clconf_obj_t *blackbox;
	const char *blackboxname;
	clconf_iter_t *currportsi, *portsi = (clconf_iter_t *)0;
	clconf_obj_t *other_port, *other_port2;
	clconf_obj_t *other_cable;
	clconf_obj_t *port[2];
	clconf_obj_t *port_parent[2];
	clconf_obj_t *port_grandparent[2];
	int portid, other_portid;
	const char *other_portname;
	scconf_cltr_epoint_t epoint;
	int count, e;
	int adapter_e = 0;
	int bb_e;

	/* Initialize epoint struct */
	bzero(&epoint, sizeof (epoint));

	/* Make sure that the cable itself is enabled */
	if (!clconf_obj_enabled(cable))
		return (0);

	/* Check both endpoints, their ports and parents of ports */
	for (e = 0;  e <= 1;  ++e) {

		/* Get the port */
		port[e] = (clconf_obj_t *)clconf_cable_get_endpoint(
		    (clconf_cable_t *)cable, (e + 1));
		if (port[e] == NULL) {
			rstatus = -1;
			goto cleanup;
		}

		/* Make sure that the port is enabled */
		if (!clconf_obj_enabled(port[e]))
			goto cleanup;

		/* Get the parent of the port */
		port_parent[e] = clconf_obj_get_parent(port[e]);
		if (port_parent[e] == NULL) {
			rstatus = -1;
			goto cleanup;
		}

		/* Make sure that the port's parent is enabled */
		if (!clconf_obj_enabled(port_parent[e]))
			goto cleanup;
	}

	/* Now, check adapters */
	count = 0;
	for (e = 0;  e <= 1;  ++e) {

		/* If not an adapter, try the next one */
		if (clconf_obj_get_objtype(port_parent[e]) != CL_ADAPTER)
			continue;

		/* Add to the adapter count */
		++count;
		adapter_e = e;

		/* Get the node */
		port_grandparent[e] = clconf_obj_get_parent(port_parent[e]);
		if (port_grandparent[e] == NULL) {
			rstatus = -1;
			goto cleanup;
		}

		/* Make sure that the adapter's node is enabled */
		if (!clconf_obj_enabled(port_grandparent[e]))
			goto cleanup;

		/* The port's grandparent is a node */
		node = port_grandparent[e];
		nodeid = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (nodeid == (scconf_nodeid_t)-1) {
			rstatus = -1;
			goto cleanup;
		}
	}

	/* If both endpoints are adapters, we are done */
	if (count == 2) {
		rstatus = 1;
		goto cleanup;
	}

	/* If neither end is an adapter, we have a problem */
	if (count == 0) {
		rstatus = -1;
		goto cleanup;
	}

	/* The one that is not the adapter must be the blackbox */
	bb_e = (adapter_e == 0) ? 1 : 0;
	blackbox = port_parent[bb_e];

	/* Not an adapter and not a blackbox */
	if (clconf_obj_get_objtype(blackbox) != CL_BLACKBOX) {
		rstatus = -1;
		goto cleanup;
	}

	/* Get the port ID of our blackbox port */
	portid = clconf_obj_get_id(port[bb_e]);
	if (portid == -1) {
		rstatus = -1;
		goto cleanup;
	}

	/* Get the blackbox name */
	blackboxname = clconf_obj_get_name(blackbox);
	if (blackbox == NULL) {
		rstatus = -1;
		goto cleanup;
	}

	/* Initialize epoint for blackbox */
	epoint.scconf_cltr_epoint_type = SCCONF_CLTR_EPOINT_TYPE_CPOINT;
	epoint.scconf_cltr_epoint_devicename = strdup(blackboxname);
	if (epoint.scconf_cltr_epoint_devicename == NULL) {
		rstatus = -1;
		goto cleanup;
	}

	/* Make sure there is another port on this blackbox which we can use */
	portsi = currportsi = clconf_bb_get_ports((clconf_bb_t *)blackbox);
	while ((other_port = clconf_iter_get_current(currportsi)) != NULL) {

		/* Get the other port ID */
		other_portid = clconf_obj_get_id(other_port);
		if (other_portid == -1) {
			rstatus = -1;
			goto cleanup;
		}

		/* Skip it, if it is our original port */
		if (portid == other_portid) {
			clconf_iter_advance(currportsi);
			continue;
		}

		/* Skip it, if it is not enabled */
		if (!clconf_obj_enabled(other_port)) {
			clconf_iter_advance(currportsi);
			continue;
		}

		/* Get the other port name */
		other_portname = clconf_obj_get_name(other_port);
		if (other_portname == NULL) {
			rstatus = -1;
			goto cleanup;
		}

		/* From that, get this other cable */
		if (epoint.scconf_cltr_epoint_portname)
			free(epoint.scconf_cltr_epoint_portname);
		epoint.scconf_cltr_epoint_portname = strdup(other_portname);
		if (epoint.scconf_cltr_epoint_portname == NULL) {
			rstatus = -1;
			goto cleanup;
		}
		if (conf_get_cableobj(clconf, &epoint, NULL, &other_cable) !=
		    SCCONF_NOERR) {
			rstatus = -1;
			goto cleanup;
		}

		/* Get the other endpoint */
		for (e = 0;  e <= 1;  ++e) {

			/* Get the port */
			other_port2 = (clconf_obj_t *)
			    clconf_cable_get_endpoint(
			    (clconf_cable_t *)other_cable, (e + 1));
			if (other_port2 == NULL) {
				rstatus = -1;
				goto cleanup;
			}

			/* If it is not the adapter end, keep searching */
			other_port2_adapter =
			    clconf_obj_get_parent(other_port2);
			if (other_port2_adapter == NULL) {
				rstatus = -1;
				goto cleanup;
			}
			if (clconf_obj_get_objtype(other_port2_adapter)
			    != CL_ADAPTER)
				continue;

			/* If it is enabled, we are done */
			if (clconf_obj_enabled(other_port2) &&
			    clconf_obj_enabled(other_port2_adapter))
				break;
		}
		/* Make sure we found the other endpoint */
		if (e >= 1) {
			clconf_iter_advance(currportsi);
			continue;
		}

		/* Get the node */
		other_port2_node = clconf_obj_get_parent(other_port2_adapter);
		if (other_port2_node == NULL) {
			rstatus = -1;
			goto cleanup;
		}

		/* Make sure that it is enabled */
		if (!clconf_obj_enabled(other_port2_node)) {
			clconf_iter_advance(currportsi);
			continue;
		}

		/* Get the node ID */
		other_port2_nodeid = clconf_obj_get_id(other_port2_node);
		if (other_port2_nodeid == -1) {
			rstatus = -1;
			goto cleanup;
		}

		/* If the node IDs are different, we have a way out */
		if (nodeid != (scconf_nodeid_t)other_port2_nodeid) {
			rstatus = 1;
			break;
		}

		/* Advance iterator */
		clconf_iter_advance(currportsi);
	}

cleanup:
	/* Release iterators */
	if (portsi != (clconf_iter_t *)0)
		clconf_iter_release(portsi);

	/* Release memory associated with the port elements */
	conf_free_cltr_epoint_elements(&epoint);

	/* Return */
	return (rstatus);
}

/*
 * conf_get_cltr_adapters
 *
 * Get the cluster transport adapters list for the given "node".
 *
 * The caller is responsible for freeing memory
 * associated with "adapslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_cltr_adapters(clconf_node_t *node,
    scconf_cfg_cltr_adap_t **adapslistp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_iter_t *curradaptersi, *adaptersi = (clconf_iter_t *)0;
	clconf_iter_t *portsi = (clconf_iter_t *)0;
	scconf_cfg_cltr_adap_t *adapslist = (scconf_cfg_cltr_adap_t *)0;
	scconf_cfg_cltr_adap_t **nextp;
	clconf_obj_t *adapter;
	char *iname;
	const char *name, *value;
	char *exclude_props[] = {PROP_ADAPTER_TRANSPORT_TYPE, (char *)0};
	scconf_cltr_adapterid_t id;
	scconf_cltr_adapterstate_t state;

	/* Check arguments */
	if (node == NULL || adapslistp == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of cluster transport adapters */
	nextp = &adapslist;
	adaptersi = curradaptersi = clconf_node_get_adapters(node);
	while ((adapter = clconf_iter_get_current(curradaptersi)) != NULL) {

		/* Allocate struct for the next adapter */
		*nextp = (scconf_cfg_cltr_adap_t *)calloc(1,
		    sizeof (scconf_cfg_cltr_adap_t));
		if (*nextp == (scconf_cfg_cltr_adap_t *)0) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add the adapter name */
		name = clconf_obj_get_name(adapter);
		if (name != NULL) {
			(*nextp)->scconf_adap_adaptername = strdup(name);
			if ((*nextp)->scconf_adap_adaptername == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Add the adapter ID */
		id = (scconf_cltr_adapterid_t)clconf_obj_get_id(adapter);
		if (id == (scconf_cltr_adapterid_t)-1) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		(*nextp)->scconf_adap_adapterid = id;

		/* Set the adapter state */
		state = (clconf_obj_enabled(adapter)) ?
		    SCCONF_STATE_ENABLED : SCCONF_STATE_DISABLED;
		(*nextp)->scconf_adap_adapterstate = state;

		/* Add the adapter transport type */
		value = clconf_obj_get_property(adapter,
		    PROP_ADAPTER_TRANSPORT_TYPE);
		if (value) {
			(*nextp)->scconf_adap_cltrtype = strdup(value);
			if ((*nextp)->scconf_adap_cltrtype == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Add the list of properties for this adapter */
		rstatus = conf_get_proplist(adapter, exclude_props,
		    &(*nextp)->scconf_adap_propertylist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Add the ports list for this adapter */
		if (portsi != (clconf_iter_t *)0)
			clconf_iter_release(portsi);
		portsi = clconf_adapter_get_ports((clconf_adapter_t *)adapter);
		rstatus = conf_get_cltr_ports(portsi,
		    &(*nextp)->scconf_adap_portlist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Add the adapter name */
		iname = clconf_obj_get_int_name(adapter);
		if (iname != NULL)
			(*nextp)->scconf_adap_iname = iname;

		/* next */
		nextp = &(*nextp)->scconf_adap_next;
		clconf_iter_advance(curradaptersi);
	}

cleanup:
	if (adaptersi != (clconf_iter_t *)0)
		clconf_iter_release(adaptersi);
	if (portsi != (clconf_iter_t *)0)
		clconf_iter_release(portsi);

	if (rstatus == SCCONF_NOERR) {
		*adapslistp = adapslist;
	} else {
		conf_free_cltr_adapters(adapslist);
	}

	return (rstatus);
}

/*
 * conf_free_cltr_adapters
 *
 * Free all memory associated with an "scconf_cfg_cltr_adap" structure.
 */
void
conf_free_cltr_adapters(scconf_cfg_cltr_adap_t *adapslist)
{
	scconf_cfg_cltr_adap_t *last, *next;

	/* Check argument */
	if (adapslist == NULL)
		return;

	/* Free memory for each adapter in the list */
	last = (scconf_cfg_cltr_adap_t *)0;
	next = adapslist;
	while (next != NULL) {

		/* free the last adapter struct */
		if (last != NULL)
			free(last);

		/* adapter name */
		if (next->scconf_adap_adaptername)
			free(next->scconf_adap_adaptername);

		/* transport type */
		if (next->scconf_adap_cltrtype)
			free(next->scconf_adap_cltrtype);

		/* property list */
		if (next->scconf_adap_propertylist)
			conf_free_proplist(next->scconf_adap_propertylist);

		/* ports list */
		if (next->scconf_adap_portlist)
			conf_free_cltr_ports(next->scconf_adap_portlist);

		/* adapter internal name */
		if (next->scconf_adap_iname)
			clconf_free_string(next->scconf_adap_iname);

		last = next;
		next = next->scconf_adap_next;
	}
	if (last != NULL)
		free(last);
}

/*
 * conf_get_cltr_cpoints
 *
 * Get the cpoints list for the cluster.
 *
 * The caller is responsible for freeing memory
 * associated with "cpointslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_cltr_cpoints(clconf_cluster_t *clconf,
    scconf_cfg_cpoint_t **cpointslistp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_iter_t *currcpointsi, *cpointsi = (clconf_iter_t *)0;
	clconf_iter_t *portsi = (clconf_iter_t *)0;
	scconf_cfg_cpoint_t *cpointslist = (scconf_cfg_cpoint_t *)0;
	scconf_cfg_cpoint_t **nextp;
	clconf_obj_t *cpoint;
	const char *name, *value;
	char *iname;
	char *exclude_props[] = {PROP_CPOINT_TYPE, (char *)0};
	scconf_cltr_cpointid_t id;
	scconf_cltr_cpointstate_t state;

	/* Check arguments */
	if (clconf == NULL || cpointslistp == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of cpoints */
	nextp = &cpointslist;
	cpointsi = currcpointsi = clconf_cluster_get_bbs(clconf);
	while ((cpoint = clconf_iter_get_current(currcpointsi)) != NULL) {

		/* Allocate struct for the next cpoint */
		*nextp = (scconf_cfg_cpoint_t *)calloc(1,
		    sizeof (scconf_cfg_cpoint_t));
		if (*nextp == (scconf_cfg_cpoint_t *)0) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add the cpoint name */
		name = clconf_obj_get_name(cpoint);
		if (name != NULL) {
			(*nextp)->scconf_cpoint_cpointname = strdup(name);
			if ((*nextp)->scconf_cpoint_cpointname == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Add the cpoint ID */
		id = (scconf_cltr_cpointid_t)clconf_obj_get_id(cpoint);
		if (id == (scconf_cltr_cpointid_t)-1) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		(*nextp)->scconf_cpoint_cpointid = id;

		/* Set the cpoint state */
		state = (clconf_obj_enabled(cpoint)) ?
		    SCCONF_STATE_ENABLED : SCCONF_STATE_DISABLED;
		(*nextp)->scconf_cpoint_cpointstate = state;

		/* Add the cpoint type */
		value = clconf_obj_get_property(cpoint, PROP_CPOINT_TYPE);
		if (value) {
			(*nextp)->scconf_cpoint_type = strdup(value);
			if ((*nextp)->scconf_cpoint_type == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Add the list of properties for this cpoint */
		rstatus = conf_get_proplist(cpoint, exclude_props,
		    &(*nextp)->scconf_cpoint_propertylist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Add the ports list for this cpoint */
		if (portsi != (clconf_iter_t *)0)
			clconf_iter_release(portsi);
		portsi = clconf_bb_get_ports((clconf_bb_t *)cpoint);
		rstatus = conf_get_cltr_ports(portsi,
		    &(*nextp)->scconf_cpoint_portlist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Add the cpoint internal name */
		iname = clconf_obj_get_int_name(cpoint);
		if (iname != NULL)
			(*nextp)->scconf_cpoint_iname = iname;

		/* next */
		nextp = &(*nextp)->scconf_cpoint_next;
		clconf_iter_advance(currcpointsi);
	}

cleanup:
	if (cpointsi != (clconf_iter_t *)0)
		clconf_iter_release(cpointsi);
	if (portsi != (clconf_iter_t *)0)
		clconf_iter_release(portsi);

	if (rstatus == SCCONF_NOERR) {
		*cpointslistp = cpointslist;
	} else {
		conf_free_cltr_cpoints(cpointslist);
	}

	return (rstatus);
}

/*
 * conf_free_cltr_cpoints
 *
 * Free all memory associated with an "scconf_cfg_cpoint" structure.
 */
void
conf_free_cltr_cpoints(scconf_cfg_cpoint_t *cpointslist)
{
	scconf_cfg_cpoint_t *last, *next;

	/* Check argument */
	if (cpointslist == NULL)
		return;

	/* Free memory for each cpoint in the list */
	last = (scconf_cfg_cpoint_t *)0;
	next = cpointslist;
	while (next != NULL) {

		/* free the last cpoint struct */
		if (last != NULL)
			free(last);

		/* cpoint name */
		if (next->scconf_cpoint_cpointname)
			free(next->scconf_cpoint_cpointname);

		/* transport type */
		if (next->scconf_cpoint_type)
			free(next->scconf_cpoint_type);

		/* property list */
		if (next->scconf_cpoint_propertylist)
			conf_free_proplist(next->scconf_cpoint_propertylist);

		/* ports list */
		if (next->scconf_cpoint_portlist)
			conf_free_cltr_ports(next->scconf_cpoint_portlist);

		/* cpoint internal name */
		if (next->scconf_cpoint_iname)
			clconf_free_string(next->scconf_cpoint_iname);

		last = next;
		next = next->scconf_cpoint_next;
	}
	if (last != NULL)
		free(last);
}

/*
 * conf_get_cltr_ports
 *
 * Get the ports list associated with the given iterator.
 *
 * The caller is responsible for freeing memory
 * associated with "portslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_cltr_ports(clconf_iter_t *portsi,
    scconf_cfg_port_t **portslistp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_cfg_port_t *portslist = (scconf_cfg_port_t *)0;
	scconf_cfg_port_t **nextp;
	clconf_obj_t *port;

	/* Check arguments */
	if (portslistp == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of ports */
	nextp = &portslist;
	while ((port = clconf_iter_get_current(portsi)) != NULL) {

		/* Allocate struct for the next port */
		*nextp = (scconf_cfg_port_t *)calloc(1,
		    sizeof (scconf_cfg_port_t));
		if (*nextp == (scconf_cfg_port_t *)0) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Fill out port information */
		rstatus = conf_get_cltr_port(port, *nextp);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* next */
		nextp = &(*nextp)->scconf_port_next;
		clconf_iter_advance(portsi);
	}

cleanup:
	if (rstatus == SCCONF_NOERR) {
		*portslistp = portslist;
	} else {
		conf_free_cltr_ports(portslist);
	}

	return (rstatus);
}

/*
 * conf_get_cltr_port
 *
 * Add the "port_obj" information to the given "scconf_cltr_port" structure.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_cltr_port(clconf_obj_t *port_obj, scconf_cfg_port_t *port_cfg)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *portname = (char *)0;
	char *port_iname = (char *)0;
	char *iname;
	const char *name;
	scconf_cltr_portid_t id = (scconf_cltr_portid_t)-1;
	scconf_cltr_portstate_t state = 0;

	/* Check arguments */
	if (port_obj == NULL || port_cfg == NULL)
		return (SCCONF_EINVAL);

	/* Add the port name */
	name = clconf_obj_get_name(port_obj);
	if (name != NULL) {
		portname = strdup(name);
		if (portname == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the port ID */
	id = (scconf_cltr_portid_t)clconf_obj_get_id(port_obj);
	if (id == (scconf_cltr_portid_t)-1) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Set the cpoint state */
	state = (clconf_obj_enabled(port_obj)) ?
	    SCCONF_STATE_ENABLED : SCCONF_STATE_DISABLED;

	/* Add the port internal name */
	iname = clconf_obj_get_int_name(port_obj);
	if (iname != NULL)
		port_iname = iname;

cleanup:
	if (rstatus == SCCONF_NOERR) {
		port_cfg->scconf_port_portname = portname;
		port_cfg->scconf_port_portid = id;
		port_cfg->scconf_port_portstate = state;
		port_cfg->scconf_port_iname = port_iname;
	} else {
		if (portname)
			free(portname);
		if (port_iname)
			free(port_iname);
	}

	return (rstatus);
}

/*
 * conf_free_cltr_ports
 *
 * Free all memory associated with an "scconf_cfg_port" structure.
 */
void
conf_free_cltr_ports(scconf_cfg_port_t *portslist)
{
	scconf_cfg_port_t *last, *next;

	/* Check argument */
	if (portslist == NULL)
		return;

	/* Free memory for each port in the list */
	last = (scconf_cfg_port_t *)0;
	next = portslist;
	while (next != NULL) {

		/* free the last port struct */
		if (last != NULL)
			free(last);

		/* port name */
		if (next->scconf_port_portname)
			free(next->scconf_port_portname);

		/* port internal name */
		if (next->scconf_port_iname)
			clconf_free_string(next->scconf_port_iname);

		last = next;
		next = next->scconf_port_next;
	}
	if (last != NULL)
		free(last);
}

/*
 * conf_get_or_change_hb
 *
 * If "change_hb" is set:
 * 	It updates the heart beat settings of all adapters.
 *
 * If "change_hb" is not set:
 * 	If "timeout" and/or "quantum" is set to some values, check
 * 	check if they are legal; Otherwise, get the default global
 * 	heart beat setting and set them to "timeout" / "quantum".
 */
static scconf_errno_t
conf_get_or_change_hb(scconf_cltr_handle_t *handle, char **timeout,
    char **quantum, char **msgbuffer, boolean_t change_hb)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	char *node_name = (char *)0;
	clconf_iter_t *curradaptersi, *adaptersi = (clconf_iter_t *)0;
	clconf_obj_t *adapter;
	char *adapter_name = (char *)0;
	char *transport_type = (char *)0;
	scconf_cltr_propdes_t *propdeslist = (scconf_cltr_propdes_t *)0;
	char properties[SCCONF_HB_BUFSIZ];
	boolean_t set_timeout = B_FALSE;
	boolean_t set_quantum = B_FALSE;
	uint_t cablechk = 0;

	/* Check the arguments */
	if (handle == (scconf_cltr_handle_t)0)
		return (SCCONF_EINVAL);

	/* Must have heart beat setting if to change it */
	if ((change_hb) && ((*timeout == NULL) || (*quantum == NULL)))
		return (SCCONF_EINVAL);

	/* Need to get the default heart beat setting and set it */
	if ((*timeout == NULL) || (strcmp(*timeout, "default") == 0))
		set_timeout = B_TRUE;
	if ((*quantum == NULL) || (strcmp(*quantum, "default") == 0))
		set_quantum = B_TRUE;

	/* Iterate through the list of configured nodes */
	nodesi = currnodesi = clconf_cluster_get_nodes(
	    (clconf_cluster_t *)handle);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {

		if (change_hb) {
			/* Get the node name */
			node_name = (char *)clconf_obj_get_name(node);
			if (node_name == NULL) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
		}

		/*  Iterate through this node's adapter list */
		adaptersi = curradaptersi = clconf_node_get_adapters(
		    (clconf_node_t *)node);

		while ((adapter = clconf_iter_get_current(
		    curradaptersi)) != NULL) {
			/* Get adapter name */
			adapter_name = (char *)clconf_obj_get_name(adapter);

			/* Get the list of adapter's property descriptions */
			rstatus = scconf_get_cltr_adapter_propdeslist(
			    (char *)adapter_name, &propdeslist);
			if (rstatus != SCCONF_NOERR) {
				goto cleanup;
			}

			/* Get transport type */
			transport_type = (char *)clconf_obj_get_property(
			    (clconf_obj_t *)adapter,
			    PROP_ADAPTER_TRANSPORT_TYPE);

			if (change_hb) {
				/* Get the properties string */
				*properties = '\0';
				rstatus = scconf_hb_to_properties(*timeout,
				    *quantum, properties,
				    (char *)transport_type);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;

				/* Update adapter properties */
				rstatus = scconf_change_cltr_adapter(
				    handle, (char *)node_name,
				    (char *)adapter_name, 0,
				    properties, SCCONF_STATE_UNCHANGED,
				    cablechk, msgbuffer);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
			} else {
				/* Check for legal heart beat */
				rstatus = scconf_get_legal_hb(
				    (char *)transport_type,
				    propdeslist, timeout, set_timeout,
				    quantum, set_quantum);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
			}

			/* Free the property description list */
			if (propdeslist != (scconf_cltr_propdes_t *)0) {
				scconf_free_propdeslist(propdeslist);
				propdeslist = (scconf_cltr_propdes_t *)0;
			}

			/* Next */
			clconf_iter_advance(curradaptersi);
		}

		/* Next */
		clconf_iter_advance(currnodesi);
	}

cleanup:
	/* Release iterator */
	if (adaptersi != (clconf_iter_t *)0)
		clconf_iter_release(adaptersi);

	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	/* Free the property description list */
	if (propdeslist != (scconf_cltr_propdes_t *)0) {
		scconf_free_propdeslist(propdeslist);
		propdeslist = (scconf_cltr_propdes_t *)0;
	}

	return (rstatus);
}

/*
 * conf_get_cltr_cables
 *
 * Get the list of cables in the cluster.
 *
 * The caller is responsible for freeing memory
 * associated with "cableslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_cltr_cables(clconf_cluster_t *clconf,
    scconf_cfg_cable_t **cableslistp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_iter_t *currcablesi, *cablesi = (clconf_iter_t *)0;
	scconf_cfg_cable_t *cableslist = (scconf_cfg_cable_t *)0;
	scconf_cfg_cable_t **nextp;
	clconf_obj_t *cable;
	clconf_obj_t *port, *port_parent, *port_grandparent;
	clconf_objtype_t port_parent_type;
	scconf_cltr_epoint_t *epoints[2];
	scconf_cltr_epoint_t *epoint;
	scconf_cfg_port_t port_data;
	scconf_cltr_cableid_t id;
	const char *name;
	char *iname;
	int e, state;

	/* Check arguments */
	if (clconf == NULL || cableslistp == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of cables */
	nextp = &cableslist;
	cablesi = currcablesi = clconf_cluster_get_cables(clconf);
	while ((cable = clconf_iter_get_current(currcablesi)) != NULL) {

		/* Allocate struct for the next cable */
		*nextp = (scconf_cfg_cable_t *)calloc(1,
		    sizeof (scconf_cfg_cable_t));
		if (*nextp == (scconf_cfg_cable_t *)0) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add the cable ID */
		id = (scconf_cltr_cableid_t)clconf_obj_get_id(cable);
		if (id == (scconf_cltr_cableid_t)-1) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		(*nextp)->scconf_cable_cableid = id;

		/* Set the cable state */
		state = (clconf_obj_enabled(cable)) ?
		    SCCONF_STATE_ENABLED : SCCONF_STATE_DISABLED;
		(*nextp)->scconf_cable_cablestate = state;

		/* Do each endpoint */
		epoints[0] = &(*nextp)->scconf_cable_epoint1;
		epoints[1] = &(*nextp)->scconf_cable_epoint2;
		for (e = 1;  e <= 2;  ++e) {
			epoint = epoints[e - 1];

			/* Get the port object */
			port = (clconf_obj_t *)clconf_cable_get_endpoint(
			    (clconf_cable_t *)cable, e);
			if (port == (clconf_obj_t *)0) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* Get port data */
			bzero(&port_data, sizeof (port_data));
			rstatus = conf_get_cltr_port(port, &port_data);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

			/* we are not interested in the iname */
			if (port_data.scconf_port_iname) {
				clconf_free_string(port_data.scconf_port_iname);
				port_data.scconf_port_iname = 0;
			}

			/* copy port data to the epoint struct */
			epoint->scconf_cltr_epoint_portname =
			    port_data.scconf_port_portname;
			epoint->scconf_cltr_epoint_portid =
			    port_data.scconf_port_portid;
			epoint->scconf_cltr_epoint_portstate =
			    port_data.scconf_port_portstate;

			/* get the parent of the port */
			port_parent = clconf_obj_get_parent(port);
			if (port_parent == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* get the type of port */
			port_parent_type = clconf_obj_get_objtype(
			    port_parent);
			switch (port_parent_type) {
			case CL_ADAPTER:

				/* Set the endpoint type */
				epoint->scconf_cltr_epoint_type =
				    SCCONF_CLTR_EPOINT_TYPE_ADAPTER;

				/* Get the grandparent node */
				port_grandparent = clconf_obj_get_parent(
				    port_parent);
				if (port_grandparent == NULL) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}

				/* Add the node name */
				name = clconf_obj_get_name(port_grandparent);
				if (name != NULL) {
					epoint->scconf_cltr_epoint_nodename =
					    strdup(name);
					if (epoint->scconf_cltr_epoint_nodename
					    == NULL) {
						rstatus = SCCONF_ENOMEM;
						goto cleanup;
					}
				}
				break;

			case CL_BLACKBOX:

				/* Set the endpoint type */
				epoint->scconf_cltr_epoint_type =
				    SCCONF_CLTR_EPOINT_TYPE_CPOINT;
				break;

			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */

			/* Add the name of the parent device */
			name = clconf_obj_get_name(port_parent);
			if (name != NULL) {
				epoint->scconf_cltr_epoint_devicename =
				    strdup(name);
				if (epoint->scconf_cltr_epoint_devicename
				    == NULL) {
					rstatus = SCCONF_ENOMEM;
					goto cleanup;
				}
			}
		}

		/* Add the cable internal name */
		iname = clconf_obj_get_int_name(cable);
		if (iname != NULL)
			(*nextp)->scconf_cable_iname = iname;

		/* next */
		nextp = &(*nextp)->scconf_cable_next;
		clconf_iter_advance(currcablesi);
	}

cleanup:
	if (cablesi != (clconf_iter_t *)0)
		clconf_iter_release(cablesi);

	if (rstatus == SCCONF_NOERR) {
		*cableslistp = cableslist;
	} else {
		conf_free_cltr_cables(cableslist);
	}

	return (rstatus);
}

/*
 * conf_free_cltr_cables
 *
 * Free all memory associated with an "scconf_cfg_cable" structure.
 */
void
conf_free_cltr_cables(scconf_cfg_cable_t *cableslist)
{
	scconf_cfg_cable_t *last, *next;

	/* Check argument */
	if (cableslist == NULL)
		return;

	/* Free memory for each cable in the list */
	last = (scconf_cfg_cable_t *)0;
	next = cableslist;
	while (next != NULL) {

		/* free the last cable struct */
		if (last != NULL)
			free(last);

		/* Do each endpoint */
		conf_free_cltr_epoint_elements(&next->scconf_cable_epoint1);
		conf_free_cltr_epoint_elements(&next->scconf_cable_epoint2);

		/* cable internal name */
		if (next->scconf_cable_iname)
			clconf_free_string(next->scconf_cable_iname);

		last = next;
		next = next->scconf_cable_next;
	}
	if (last != NULL)
		free(last);
}

/*
 * conf_free_cltr_epoint_elements
 *
 * Free all memory associated with the elements of an scconf_cltr_epoint
 * structure, without freeing the scconf_cltr_epoint struct itself.
 */
void
conf_free_cltr_epoint_elements(scconf_cltr_epoint_t *epoint)
{
	/* node name */
	if (epoint->scconf_cltr_epoint_nodename) {
		free(epoint->scconf_cltr_epoint_nodename);
		epoint->scconf_cltr_epoint_nodename = NULL;
	}

	/* device name */
	if (epoint->scconf_cltr_epoint_devicename) {
		free(epoint->scconf_cltr_epoint_devicename);
		epoint->scconf_cltr_epoint_devicename = NULL;
	}

	/* port name */
	if (epoint->scconf_cltr_epoint_portname) {
		free(epoint->scconf_cltr_epoint_portname);
		epoint->scconf_cltr_epoint_portname = NULL;
	}
}

/*
 * conf_split_adaptername
 *
 * The given adaptername is checked for correct format, then split into
 * its two parts, device_name and device_instance.  The "device_name"
 * and "device_instance" buffers must be large enough to hold the split
 * "adaptername" components.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument or bad adaptername format
 */
scconf_errno_t
conf_split_adaptername(char *adaptername, char *device_name,
    char *device_instance)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *name = NULL;
	char *inst = NULL;
	char *s;

	/* Check arguments */
	if (adaptername == NULL || device_name == NULL ||
	    device_instance == NULL)
		return (SCCONF_EINVAL);

	/* Isolate the device name portion of the adapter name */
	name = strdup(adaptername);
	if (name == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}
	if (isdigit(*name)) {
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}
	s = &name[strlen(name) - 1];
	while (isdigit(*s))
		s--;
	s++;

	/* Isolate the device instance portion of the adapter name */
	inst = strdup(s);
	if (inst == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* terminate device name */
	*s = '\0';

	/* Make sure device instance is all digits */
	for (s = inst;  *s;  ++s)
		if (!isdigit(*s))
			break;
	if (*s != '\0') {
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

cleanup:
	/* If no errors, copy values to supplied buffers */
	if (rstatus == SCCONF_NOERR) {
		if (name != NULL)
			(void) strcpy(device_name, name);
		if (inst != NULL)
			(void) strcpy(device_instance, inst);
	}

	if (name != NULL)
		free(name);
	if (inst != NULL)
		free(inst);

	return (rstatus);
}
/*
 * scconf_verify_in_ipmp
 *
 * The given adapter is searched in ipmp groups of given
 * node and returns success if exists.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR            - success
 *	SCCONF_EUNEXPECTED      - internal error
 *	SCCONF_ESETUP		- node is offline
 *	SCCONF_ENOEXIST         - not found in any ipmp group
 */
scconf_errno_t
scconf_verify_in_ipmp(char *adaptername, char *nodename)
{
	scha_err_t scerr = SCHA_ERR_NOERR;
	scha_node_state_t node_state;
	scha_cluster_t hdl = NULL;
	scconf_errno_t ret_code = SCCONF_NOERR;
	pnm_group_list_t glist = NULL;
	int pnm_ret_status = 0;
	char *gr_str = NULL;
	char *next_gr_str = NULL;
	pnm_grp_status_t grp_status;
	int found = 0;
	uint_t i = 0;

	if (adaptername == NULL || nodename == NULL)
		return (SCCONF_EUNEXPECTED);

	scerr = scha_cluster_open(&hdl);
	if (scerr != SCHA_ERR_NOERR)
		return (SCCONF_EUNEXPECTED);

	/* Is given node currently online */
	scerr = scha_cluster_get(hdl, SCHA_NODESTATE_NODE,
	    nodename, &node_state);
	if (scerr != SCHA_ERR_NOERR) {
		ret_code = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* If node is down, we cannot verify */
	if (node_state == SCHA_NODE_DOWN) {
		ret_code = SCCONF_ESETUP;
		goto cleanup;
	}

	/* Use pnm api and search for this adapter in ipmp groups */
	pnm_ret_status = pnm_init(nodename);
	if (pnm_ret_status != 0) {
		/* Unable to retrieve information */
		ret_code = SCCONF_ESETUP;
		goto cleanup;
	}

	pnm_ret_status = pnm_group_list(&glist);
	if (pnm_ret_status != 0) {
		/* Unable to retrieve information */
		ret_code = SCCONF_ESETUP;
		goto cleanup;
	}
	next_gr_str = glist;

	while ((gr_str = strtok_r(next_gr_str, ":", &next_gr_str)) != NULL) {

		/* Get the adp level status for this group from libpnm */
		pnm_ret_status = pnm_grp_adp_status(gr_str, &grp_status);
		if (pnm_ret_status != 0) {
			/* Unable to retrieve information */
			ret_code = SCCONF_ESETUP;
			goto cleanup;
		}

		for (i = 0; i < grp_status.num_adps; i++) {
			if (grp_status.pnm_adp_stat[i].adp_name &&
				strcmp(grp_status.pnm_adp_stat[i].adp_name,
				    adaptername) == 0) {
				/* Found it */
				found++;
				break;
			}
		}

		/* Free the group status struct */
		pnm_grp_status_free(&grp_status);
		if (found)
			break;
	}

	if (found)
		ret_code = SCCONF_NOERR;
	else
		ret_code = SCCONF_ENOEXIST;

cleanup:
	/* Free the glist struct allocated */
	if (glist)
		free(glist);
	/* Close the connection to the specified node's pnmd */
	pnm_fini();
	(void) scha_cluster_close(hdl);
	return (ret_code);
}
