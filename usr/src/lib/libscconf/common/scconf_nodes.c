/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scconf_nodes.c	1.48	09/04/02 SMI"

/*
 * libscconf cluster nodes config functions
 */

#include <sys/sol_version.h>

#include "scconf_private.h"
#if SOL_VERSION >= __s10
#include <privip_map.h>
#endif
/* SC SLM addon start */
#ifndef FSS_MAXSHARES
#define	BUG_6541238
#define	FSS_MAXSHARES	65535 /* XXX from sys/fss.h */
#endif
/* SC SLM addon end */

#define	STR_ENABLED	"enabled"
#define	STR_SIMPLE	"simple"
#define	STR_REDUNDANT	"redundant"

static scconf_errno_t conf_check_node_notinuse(clconf_cluster_t *clconf,
    clconf_obj_t *node, char **messages);
static scconf_errno_t conf_check_node_notinuse_byrg(char *nodename,
    scconf_nodeid_t nodeid, char **messages);
static scconf_errno_t conf_check_node_notinuse_byrt(char *nodename,
    scconf_nodeid_t nodeid, char **messages);
static scconf_errno_t conf_check_node_notinuse_bydg(char *nodename,
    scconf_nodeid_t nodeid, int skip_auto, char **messages);
static scconf_errno_t conf_check_node_notinuse_byquorum(
    clconf_cluster_t *clconf, char *nodename, char **messages);
static scconf_errno_t conf_get_node_inaddr(clconf_cluster_t *clconf,
    scconf_nodeid_t nodeid, struct in_addr *addr);
static scconf_errno_t conf_remove_cables(clconf_cluster_t *clconf,
    clconf_obj_t *node, scconf_nodeid_t nodeid, char **messages);
static scconf_errno_t conf_check_node_removal(clconf_cluster_t *clconf,
    clconf_obj_t *node, char **messages);

/*
 * scconf_add_node
 *
 * Add a new node to the cluster config database for the given "nodename".
 *
 * If "handle" is NULL, the change is committed upon successful
 * return of the function.  Otherwise, the change to the configuration
 * does not occur until scconf_cltr_closehandle() returns with success.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- the given "nodename" is already configured
 *	SCCONF_ENODEID		- ID used in place of node name
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_add_node(scconf_cltr_handle_t handle, char *nodename, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	scxcfg_error scxerr;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	clconf_cluster_t *tmp_clconf = (clconf_cluster_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;
	clconf_obj_t *node = (clconf_obj_t *)0;
	clconf_obj_t *tmpnode;
	char key[sizeof ("0xFFFFFFFFFFFFFFFF")];
	char privatehostname[SYS_NMLN + sizeof ("-NNN")];
	char buffer[SYS_NMLN];
	char errbuffer[SCCONF_MAXSTRINGLEN];
	scconf_nodeid_t nodeid;
	scxcfg scxhdl;
	int suffix;
	int clusterid;
	char *s;
	int numnodes, maxnodes;
	boolean_t iseuropa = B_FALSE;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodename == NULL)
		return (SCCONF_EINVAL);

	/* Make sure there is no embedded ':' in nodename */
	if (strchr(nodename, ':'))
		return (SCCONF_EINVAL);

	/* Check if farm mgmt enabled */
	rstatus = scconf_iseuropa(&iseuropa);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/*
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)tmp_clconf);
			tmp_clconf = clconf = (clconf_cluster_t *)0;
		}
		if (existing_clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)existing_clconf);
			existing_clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config, if necessary */
		if (clconf == (clconf_cluster_t *)0) {
			rstatus = scconf_cltr_openhandle(
			    (scconf_cltr_handle_t *)&tmp_clconf);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			clconf = tmp_clconf;
		}

		/* Make a copy of the original */
		existing_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
		    (clconf_obj_t *)clconf);
		if (existing_clconf == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		rstatus = conf_get_maxnodes(clconf, &maxnodes);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		numnodes = conf_nodecount(clconf);
		if (numnodes == -1)
			goto cleanup;

		if (numnodes + 1 > maxnodes) {
			(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
			    "Insufficient private IP address range "
			    "to add the node\n"));
			scconf_addmessage(errbuffer, messages);
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/* Make sure that the "nodename" is not an ID (numeric) */
		for (s = nodename;  *s;  ++s)
			if (!isdigit(*s))
				break;
		if (*s == '\0') {
			rstatus = SCCONF_ENODEID;
			goto cleanup;
		}

		/* See if the nodename is already configured */
		rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
		switch (rstatus) {
		case SCCONF_ENOEXIST:
			rstatus = SCCONF_NOERR;
			break;

		case SCCONF_NOERR:
			rstatus = SCCONF_EEXIST;
			goto cleanup;

		default:
			goto cleanup;
		} /*lint !e788 */

		/*
		 * See if the nodename is already
		 * configured in the farm table.
		 */
		if (iseuropa) {
			/* Allocate farm config */
			rstatus = scconf_scxcfg_open(&scxhdl, &scxerr);
			if (rstatus != SCCONF_NOERR) {
				(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
				    "Failed to initialize scxcfg library.\n"));
				scconf_addmessage(errbuffer, messages);
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/*
			 * See if the nodename is already
			 * configured in FARM TABLE.
			 */
			rstatus = scconf_scxcfg_get_nodeid(&scxhdl, nodename,
			    &nodeid, &scxerr);
			switch (rstatus) {
			case SCCONF_NOERR:
				rstatus = SCCONF_EEXIST;
				goto cleanup;

			case SCCONF_ENOEXIST:
				rstatus = SCCONF_NOERR;
				break;

			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}

		/* Create the new node */
		node = (clconf_obj_t *)clconf_node_create();
		if (node == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* add the node to the cluster */
		clconf_err = clconf_cluster_add_node(clconf,
		    (clconf_node_t *)node);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
				rstatus = SCCONF_EINVAL;
				goto cleanup;

			case CL_INVALID_ID:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Add the node name */
		clconf_obj_set_name(node, nodename);

		/* Set the state to disabled for add */
		rstatus = conf_set_state(node, SCCONF_STATE_DISABLED);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Set the quorum vote count to zero */
		clconf_err = clconf_obj_set_property(node,
		    PROP_NODE_QUORUM_VOTE, "0");
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/*
		 * Set the reservation key using the cluster ID
		 */

		/* Get the cluster ID */
		rstatus = conf_get_clusterid(clconf, &clusterid);

		/* If the cluster ID is not set or is bad, set it now */
		if (rstatus != SCCONF_NOERR) {
			switch (rstatus) {
			case SCCONF_ENOEXIST:
			case SCCONF_EBADVALUE:
				rstatus = conf_set_clusterid(clconf);
				if (rstatus != SCCONF_NOERR)
					goto cleanup;
				rstatus = conf_get_clusterid(clconf,
				    &clusterid);
				if (rstatus != SCCONF_NOERR) {
					switch (rstatus) {
					case SCCONF_ENOEXIST:
					case SCCONF_EBADVALUE:
						rstatus = SCCONF_EUNEXPECTED;
						goto cleanup;

					default:
						goto cleanup;
					}
				}
				break;

			default:
				goto cleanup;
			} /*lint !e788 */
		}

		/* get the node id */
		nodeid = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (nodeid == (scconf_nodeid_t)-1) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		rstatus = conf_form_reskey(clusterid, nodeid, key);
		if (rstatus  != SCCONF_NOERR)
			goto cleanup;

		/* Set the quorum reservation key */
		clconf_err = clconf_obj_set_property(node,
		    PROP_NODE_QUORUM_RESV_KEY, key);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Form the default privatehostname */
		rstatus = scconf_form_default_privatehostname(nodeid, buffer);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;
		(void) strcpy(privatehostname, buffer);

		/* Make sure we find an unused one */
		suffix = 1;
		while (conf_lookup_privatehostname(clconf, privatehostname,
		    &tmpnode) == SCCONF_NOERR) {
			(void) sprintf(privatehostname, "%s-%d", buffer,
			    suffix++);
		}

		/* Add the privatehostname node property */
		clconf_err = clconf_obj_set_property(node,
		    PROP_NODE_PRIVATE_HOSTNAME, privatehostname);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* make sure that the node object is okay */
		rstatus = conf_obj_check(node, messages);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* if tmp_clconf is set, attempt commit here */
		if (tmp_clconf != (clconf_cluster_t *)0) {
			rstatus = conf_cluster_commit(tmp_clconf, messages);

		/* otherwise, just check the transition */
		} else {
			rstatus = conf_check_transition(existing_clconf, clconf,
			    messages);
		}

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release the farm config if europa is enabled */
	if (iseuropa)
		scconf_scxcfg_close(&scxhdl);

	/* Release temp cluster config */
	if (tmp_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)tmp_clconf);

	/* Release copy of cluster config */
	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	return (rstatus);
}

/*
 * scconf_add_farmnode
 *
 * Add a new node to the farm config database for the given "nodename".
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- the given "nodename" is already configured
 *	SCCONF_ENODEID		- ID used in place of node name
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_add_farmnode(scconf_cltr_handle_t handle, char *nodename,
    char *adapters, char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scxcfg_error scxerr;
	int scxret;
	char errbuffer[SCCONF_MAXSTRINGLEN];
	char *s;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;
	scconf_nodeid_t nodeid;
	scxcfg scxhdl;
	f_property_t *adapterlist = (f_property_t *)0;
	f_property_t *adapterp, *listhead, *listtail;
	char *adapter1 = NULL;
	char *adapter2 = NULL;

	/* Must be root */
	if (getuid() != 0) {
		return (SCCONF_EPERM);
	}

	/* Check arguments */
	if (nodename == NULL || adapters == NULL) {
		return (SCCONF_EINVAL);
	}

	/* Make sure there is no embedded ':' in nodename */
	if (strchr(nodename, ':')) {
		return (SCCONF_EINVAL);
	}

	/* Make sure that the "nodename" is not an ID (numeric) */
	for (s = nodename;  *s;  ++s) {
		if (!isdigit(*s)) {
			break;
		}
	}
	if (*s == '\0') {
		return (SCCONF_ENODEID);
	}

	/*
	 * Get the adapters from the colon seperated list of adapters
	 * and make sure there are no more than two adapters.
	 */
	adapter1 = strdup(adapters);
	if (adapter1 == NULL) {
		return (SCCONF_ENOMEM);
	}
	adapter2 = strchr(adapter1, ':');
	if (adapter2) {
		*adapter2++ = '\0';
	}
	if (adapter2 && strchr(adapter2, ':')) {
		(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
		    "You cannot specify more than two adapters.\n"));
		scconf_addmessage(errbuffer, messages);
		return (SCCONF_EINVAL);
	}

	rstatus = colonlist_to_fpropertylist(adapters, &adapterlist);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Allocate cluster config */
	rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR) {
		return (rstatus);
	}

	/* See if the nodename is already configured in CCR */
	rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
	switch (rstatus) {
	case SCCONF_ENOEXIST:
		rstatus = SCCONF_NOERR;
		break;

	case SCCONF_NOERR:
		rstatus = SCCONF_EEXIST;
		goto cleanup;

	default:
		goto cleanup;
	} /*lint !e788 */

	/* Allocate farm config */
	rstatus = scconf_scxcfg_open(&scxhdl, &scxerr);
	if (rstatus != SCCONF_NOERR) {
		(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
		    "Failed to initialize scxcfg library.\n"));
		scconf_addmessage(errbuffer, messages);
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* See if the nodename is already configured in FARM TABLE */
	rstatus = scconf_scxcfg_get_nodeid(&scxhdl, nodename,
	    &nodeid, &scxerr);
	switch (rstatus) {
	case SCCONF_NOERR:
		rstatus = SCCONF_EEXIST;
		goto cleanup;

	case SCCONF_ENOEXIST:
		rstatus = SCCONF_NOERR;
		break;

	default:
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Add a farm node */
	rstatus = scconf_scxcfg_add_node(&scxhdl, nodename, adapterlist,
	    &nodeid, &scxerr);
	if (rstatus != SCCONF_NOERR) {
		rstatus = SCCONF_EUNEXPECTED;
	}

cleanup:
	/* Free the adapterlist */
	if (adapterlist) {
		scconf_scxcfg_freelist(adapterlist);
	}

	/* Release the farm config */
	scconf_scxcfg_close(&scxhdl);

	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
	}

	return (rstatus);
}

/*
 * scconf_change_nodename
 *
 * Change the "oldnodename" to "newnodename".
 *
 * If "handle" is NULL, the change is committed upon successful
 * return of the function.  Otherwise, the change to the configuration
 * does not occur until scconf_cltr_closehandle() returns with success.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- the "newnodename" is already configured
 *	SCCONF_ENOEXIST		- the "oldnodename" is not found
 *	SCCONF_ENODEID		- ID used in place of new node name
 *	SCCONF_EBUSY		- the node is active in the cluster
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_change_nodename(char *newnodename, char *oldnodename, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_obj_t *node;
	scconf_nodeid_t nodeid;
	uint_t ismember;
	char *s;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (newnodename == NULL || oldnodename == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get the nodeid for the oldnodename */
		rstatus = conf_get_nodeid(clconf, oldnodename, &nodeid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Make sure the node is not in the cluster */
		rstatus = scconf_ismember(nodeid, &ismember);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;
		if (ismember) {
			rstatus = SCCONF_EBUSY;
			goto cleanup;
		}

		/* Find the node for the oldnodename */
		rstatus = conf_get_nodeobj(clconf, nodeid, &node);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* See if the newnodename is already configured */
		rstatus = conf_get_nodeid(clconf, newnodename, &nodeid);
		if (rstatus != SCCONF_ENOEXIST) {
			if (rstatus == SCCONF_NOERR)
				rstatus = SCCONF_EEXIST;
			goto cleanup;
		}

		/* Make sure that the "newnodename" is not an ID (numeric) */
		for (s = newnodename;  *s;  ++s)
			if (!isdigit(*s))
				break;
		if (*s == '\0') {
			rstatus = SCCONF_ENODEID;
			goto cleanup;
		}

		/* Change the node name */
		clconf_obj_set_name(node, newnodename);

		/* Commit */
		rstatus = conf_cluster_commit(clconf, messages);

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release temp cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_change_farm_monstate
 *
 * Change the farm state (monitoring property) of given farm node(s).
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- the "oldnodename" is not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_change_farm_monstate(char *nodelist, char *state,
    uint_t allnodes, char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scxcfg_error scxerr;
	int scxret;
	char errbuffer[SCCONF_MAXSTRINGLEN];
	scconf_namelist_t *nodes = (scconf_namelist_t *)0;
	scxcfg scxhdl;
	scconf_nodeid_t nodeid;
	f_property_t *farm_nodelist = NULL;
	f_property_t *farmnodes;
	char property[FCFG_MAX_LEN];
	char value[FCFG_MAX_LEN];
	int num = 0;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check for arguments */
	if (!allnodes && nodelist == NULL)
		return (SCCONF_EUNEXPECTED);

	/* Allocate farm config */
	rstatus = scconf_scxcfg_open(&scxhdl, &scxerr);
	if (rstatus != SCCONF_NOERR) {
		(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
		    "Failed to initialize scxcfg library.\n"));
		scconf_addmessage(errbuffer, messages);
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	if (!allnodes) {
		rstatus = colonlist_to_namelist(nodelist, &nodes);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		while (nodes) {
			/* See if the nodename exists in FARM configuration */
			rstatus = scconf_scxcfg_get_nodeid(&scxhdl,
			    nodes->scconf_namelist_name, &nodeid, &scxerr);
			switch (rstatus) {
			case SCCONF_NOERR:
				break;

			case SCCONF_ENOEXIST:
				(void) sprintf(errbuffer, dgettext(
				    TEXT_DOMAIN, "%s does not exist in the "
				    "farm configuration.\n"),
				    nodes->scconf_namelist_name);
				scconf_addmessage(errbuffer, messages);
				goto cleanup;

			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* Set the monitoring property */
			sprintf(property, SCCONF_FARM_NODE_MONITORING_STATE,
			    nodeid);
			rstatus = scconf_scxcfg_setproperty_value(&scxhdl,
					    NULL, property, state, &scxerr);
			if (rstatus != SCCONF_NOERR) {
				(void) sprintf(errbuffer, dgettext(
				    TEXT_DOMAIN, "Failed to set monitoring "
				    "property for %s.\n"),
				    nodes->scconf_namelist_name);
				scconf_addmessage(errbuffer, messages);
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			nodes = nodes->scconf_namelist_next;
		}

		goto cleanup;
	}

	/* If all_nodes, then get the list of N nodeids. */
	rstatus = scconf_scxcfg_getlistproperty_value(&scxhdl, NULL,
		    SCCONF_FARM_NODES, &farm_nodelist, &num, &scxerr);
	if (rstatus != SCCONF_NOERR) {
		(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
		    "Failed to get farm configuration information.\n"));
		scconf_addmessage(errbuffer, messages);
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	if (num == 0 || farm_nodelist == NULL) {
		(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
		    "There are no nodes in the farm configuration.\n"));
		scconf_addmessage(errbuffer, messages);
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

	for (farmnodes = farm_nodelist; farmnodes; farmnodes =
	    farmnodes->next) {

		nodeid = atoi(farmnodes->value);
		if (nodeid < 0) {
			continue;
		}

		sprintf(property, SCCONF_FARM_NODE_MONITORING_STATE, nodeid);

		/* Set the monitoring property */
		rstatus = scconf_scxcfg_setproperty_value(&scxhdl,
				    NULL, property, state, &scxerr);
		if (rstatus != SCCONF_NOERR) {
			(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
			    "Failed to set monitoring property for %s.\n"),
			    nodes->scconf_namelist_name);
			scconf_addmessage(errbuffer, messages);
			rstatus = SCCONF_EUNEXPECTED;
		}
	}

cleanup:
	/* Release the farm config */
	scconf_scxcfg_close(&scxhdl);

	return (rstatus);
}

/*
 * scconf_form_default_privatehostname
 *
 * Using the given "nodeid", form the name of the default private host
 * name for this node, and copy it to "buffer".  The "buffer" must be at
 * least SYS_NMLN in length.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_form_default_privatehostname(scconf_nodeid_t nodeid, char *buffer)
{
	/* Check arguments */
	if (buffer == NULL)
		return (SCCONF_EINVAL);

	/* Form the name */
	(void) sprintf(buffer, "clusternode%d-priv", nodeid);

	return (SCCONF_NOERR);
}

/*
 * scconf_set_privatehostname
 *
 * Add the host name to be used for accessing a cluster node
 * or zone over the private interconnect.  The name is given by
 * the caller in "privatehostname".  If "privatehostname" is NULL,
 * the name is set to "clusternodeN-Z-priv" where N and Z are the
 * node id of the node to which the zone belongs and zone name
 * respectively.  If "privatehostname" is already in use for
 * another node, SCCONF_EINUSE is returned.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EINUSE		- privatehostname is already in use
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_set_privatehostname(char *nodename, char *privatehostname,
    uint_t nochange, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	int res_status = 0;
	clconf_errnum_t clconf_err;
	char *zonename = NULL;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_nodeid_t nodeid, id;
	clconf_obj_t *tmpnode, *node;
	char dflt_privatehostname[SYS_NMLN + sizeof ("-NNN")];
	char buffer[SYS_NMLN];
	int suffix, found;
	char *tmp_nodename = NULL;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodename == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Save the nodename so this function does not change
	 * the passed string.
	 */
	tmp_nodename = strdup(nodename);
	if (!tmp_nodename) {
		return (SCCONF_ENOMEM);
	}

#if SOL_VERSION >= __s10
	/*
	 * Since the nodename passed in is a logical node name
	 * (nodename or nodename:zonename), isolate the zonename
	 * from the saved nodename.
	 */
	zonename = strchr(tmp_nodename, ':');
	if (zonename) {
		*zonename++ = '\0';

		/* Check for zonename */
		if (strlen(zonename) == 0) {
			if (tmp_nodename) {
				free(tmp_nodename);
			}
			return (SCCONF_EINVAL);
		}
	}

	/* Check for required zone privatehostname */
	if (zonename && privatehostname == NULL) {
		if (tmp_nodename) {
			free(tmp_nodename);
		}
		return (SCCONF_EUNEXPECTED);
	}
#endif

	/*
	 * Loop until the commit succeeds, or until
	 * a fatal error is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get the nodeid */
		rstatus = conf_get_nodeid(clconf, tmp_nodename, &nodeid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Find the node */
		rstatus = conf_get_nodeobj(clconf, nodeid, &node);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* If privatehostname is not supplied, use the default */
		if (privatehostname == NULL) {

			/* Form the default privatehostname */
			rstatus = scconf_form_default_privatehostname(
			    nodeid, buffer);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			(void) strcpy(dflt_privatehostname, buffer);

			/*
			 * Make sure the private hostname is unused.
			 * First check against the node private hostnames
			 * then the non-global zone private hostnames
			 * and if not found then use it. If found, choose
			 * a new default private hostname using a new
			 * suffix.
			 */
			suffix = 1;
			found = 1;
			while (found) {
				if (conf_lookup_privatehostname(clconf,
					dflt_privatehostname, &tmpnode) ==
					SCCONF_NOERR) {
					id = (scconf_nodeid_t)
						clconf_obj_get_id(tmpnode);

					if (id == (scconf_nodeid_t)-1) {
						rstatus = SCCONF_EUNEXPECTED;
						goto cleanup;
					}

					/* If already set, we are done */
					if (id == nodeid) {
						rstatus = SCCONF_NOERR;
						goto cleanup;
					}

					/* Else, try another suffix */
					(void) sprintf(dflt_privatehostname,
					    "%s-%d", buffer, suffix++);

#if SOL_VERSION >= __s10
				} else if ((res_status =
				    scprivip_hostname_exists((const char *)
				    dflt_privatehostname)) != SCPRIVIP_NOERR) {

					if (res_status ==
						SCPRIVIP_EUNEXPECTED) {
						rstatus = SCCONF_EUNEXPECTED;
						goto cleanup;
					}

					if (res_status == SCPRIVIP_EINUSE) {
						/* Try another suffix */
						(void) sprintf(
						    dflt_privatehostname,
						    "%s-%d", buffer, suffix++);
					}
#endif
				} else
					found = 0;
			}

			/* Use the default */
			privatehostname = dflt_privatehostname;

		/* privatehostname */
		} else {

			/* Make sure it is unused */
			if (conf_lookup_privatehostname(clconf,
			    privatehostname, &tmpnode) == SCCONF_NOERR) {
				id = (scconf_nodeid_t)clconf_obj_get_id(
				    tmpnode);
				if (id == (scconf_nodeid_t)-1) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}

				/*
				 * While setting a zone privatehostname,
				 * if the privatehostname specified already
				 * belongs to a node, consider it as INUSE.
				 */
				if (zonename && id == nodeid) {
					rstatus = SCCONF_EINUSE;
					goto cleanup;
				}

				/* If already set, we are done */
				if (id == nodeid) {
					rstatus = SCCONF_NOERR;
					goto cleanup;

				/* otherwise, it is an "in use" error */
				} else {
					rstatus = SCCONF_EINUSE;
					goto cleanup;
				}
			}

#if SOL_VERSION >= __s10
			res_status = scprivip_hostname_exists(
				(const char *)privatehostname);
			if (res_status != SCPRIVIP_NOERR) {
				switch (res_status) {
				case SCPRIVIP_EINUSE:
					rstatus = SCCONF_EINUSE;
					break;
				case SCPRIVIP_EUNEXPECTED:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					break;
				}
				goto cleanup;
			} else {
				rstatus = SCCONF_NOERR;
			}
#endif
		}

#if SOL_VERSION >= __s10
		/* Add the privatehostname for a cluster local zone */
		if (zonename) {
			if (nochange) {
				res_status =
				    scprivip_get_address(privatehostname,
				    zonename, nodeid);
			} else {
				res_status = scprivip_change_hostname(
				    privatehostname, zonename, nodeid);
			}

			if (res_status != SCPRIVIP_NOERR) {
				switch (res_status) {
				case SCPRIVIP_EINUSE:
					rstatus = SCCONF_EINUSE;
					break;
				case SCPRIVIP_EEXIST:
					rstatus = SCCONF_EEXIST;
					break;
				case SCPRIVIP_ENOEXIST:
					rstatus = SCCONF_ENOEXIST;
					break;
				case SCPRIVIP_NO_ADDR:
					rstatus = SCCONF_ESETUP;
					break;
				case SCPRIVIP_EUNEXPECTED:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					break;
				}
				goto cleanup;
			} else {
				rstatus = SCCONF_NOERR;
				break;
			}
		}
#endif

		/*
		 * Add the privatehostname node property only if zonename
		 * is NULL. Otherwise the privatehostname node property
		 * needs to remain unchanged.
		 */
		if (zonename == NULL) {
			clconf_err = clconf_obj_set_property(node,
			    PROP_NODE_PRIVATE_HOSTNAME, privatehostname);
			if (clconf_err != CL_NOERROR) {
				switch (clconf_err) {
				case CL_BADNAME:
				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}

			/* Commit */
			rstatus = conf_cluster_commit(clconf, messages);

			/* If committed okay, we are done */
			if (rstatus == SCCONF_NOERR) {
				break;

			/* else if outdated handle, continue to retry */
			} else if (rstatus != SCCONF_ESTALE) {
				goto cleanup;
			}
		}
	}

cleanup:
	if (tmp_nodename) {
		free(tmp_nodename);
	}

	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_clear_privatehostname
 *
 * Remove the hostname used for accessing a cluster local zone
 * over the private interconnect. This routine does not remove
 * the hostname used for accessing a cluster node (global zone).
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_clear_privatehostname(char *nodename)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	int res_status = 0;
	char *zonename = NULL;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_nodeid_t nodeid;
	char *nodename_dup = NULL;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodename == NULL)
		return (SCCONF_EINVAL);

	/* Save the nodename */
	nodename_dup = strdup(nodename);
	if (!nodename_dup) {
		return (SCCONF_ENOMEM);
	}

	/*
	 * Since the nodename passed in is a logical node name
	 * (nodename or nodename:zonename), isolate the zonename
	 * from the nodename.
	 */
	zonename = strchr(nodename_dup, ':');
	*zonename++ = '\0';

	/* Check for zonename */
	if (strlen(zonename) == 0) {
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

	/* Release copies of cluster config, if they exist */
	if (clconf != (clconf_cluster_t *)0) {
		clconf_obj_release((clconf_obj_t *)clconf);
		clconf = (clconf_cluster_t *)0;
	}

	/* Allocate config */
	rstatus = scconf_cltr_openhandle((scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Get the nodeid */
	rstatus = conf_get_nodeid(clconf, nodename_dup, &nodeid);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

#if SOL_VERSION >= __s10
	/* Remove the privatehostname for a cluster local zone */
	res_status = scprivip_free_address(zonename, nodeid);

	if (res_status != SCPRIVIP_NOERR) {
		switch (res_status) {
		case SCPRIVIP_ENOEXIST:
			rstatus = SCCONF_ENOEXIST;
			break;
		case SCPRIVIP_EUNEXPECTED:
		default:
			rstatus = SCCONF_EUNEXPECTED;
			break;
		}
	}
#endif

cleanup:
	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	if (nodename_dup) {
		free(nodename_dup);
	}

	return (rstatus);
}

/*
 * scconf_rm_cables
 *
 * Remove all the cable objects that are attached to a particular node
 * If the node is part of the cluster, then the removal of the cables will
 * fail.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- the node does not exist
 *	SCCONF_EBUSY		- busy, try again later
 *	SCCONF_EINUSE		- node is part of cluster
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_rm_cables(char *nodename, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_nodeid_t nodeid;
	uint_t ismember;
	clconf_obj_t *node;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodename == NULL)
		return (SCCONF_EINVAL);


	/*
	 * Loop until the commit succeeds, or until
	 * a fatal error is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			return (rstatus);

		/* Get the nodeid */
		rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Make sure the node is not in the cluster */
		rstatus = scconf_ismember(nodeid, &ismember);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;
		if (ismember) {
			/* Add message */
			if (messages != (char **)0)
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				"The node is still in cluster mode.\n"),
				messages);
			rstatus = SCCONF_EINUSE;
			goto cleanup;
		}

		/* Find the node */
		rstatus = conf_get_nodeobj(clconf, nodeid, &node);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Remove any cables attached to this node */
		rstatus = conf_remove_cables(clconf, node, nodeid,
			messages);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Commit */
		rstatus = conf_cluster_commit(clconf, messages);

		/* If committed okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_check_node_removal
 *
 * Check to see if the node can be safely removed.  By this, the node
 * cannot be referenced by any quorum disk, resource group, or logical
 * device group (logical device groups are SDS (or SVM) metasets
 * diskgroups.)
 *
 * Physical device groups (DID disks) and cluster transport objects such as
 * cables and interface endpoints will not be checked by this function.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success - node can be safely removed
 *	SCCONF_EINUSE		- failure - node cannot be safely removed
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- the node does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_check_node_removal(char *nodename, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_nodeid_t nodeid;
	clconf_obj_t *node;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodename == NULL)
		return (SCCONF_EINVAL);

	/* Allocate config */
	rstatus = scconf_cltr_openhandle(
	    (scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);

	/* Get the nodeid */
	rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Find the node */
	rstatus = conf_get_nodeobj(clconf, nodeid, &node);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Make sure that the node is not in use */
	rstatus = conf_check_node_removal(clconf, node, messages);

cleanup:
	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_rm_node
 *
 * Remove a node from the cluster configuration.
 *
 * The node must not be referenced by any rgm CCR tables in RG
 * nodelists or RT installed nodes lists. If it is, the node will
 * not be removed from the cluster configuration.
 *
 * If "force_flg" is not set, the node must be fully uncabled and
 * must not be referenced by any cluster services (e.g., rgm, dcs,
 * quorum), other than did (we will remove did references).
 *
 * If "force_flg" is set, it removes the reference to the node from
 * cluster confgiruation, except RG reference.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EINUSE		- node is cabled or otherwise referenced
 *	SCCONF_ENOEXIST		- the node does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_rm_node(char *nodename, uint_t force_flg, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	int did_cleared = 0;
	scconf_nodeid_t nodeid;
	clconf_obj_t *node;
	char buffer[SCCONF_MAXSTRINGLEN];

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodename == NULL)
		return (SCCONF_EINVAL);

	/*
	 * For force remove, remove the quorum reference to the nodes.
	 * Also must remove cable before removing the node.
	 */
	if (force_flg) {
		/* Remove reference to the node from quorums */
		rstatus = scconf_maintstate_quorum_node(nodename);
		if (rstatus != SCCONF_NOERR) {
			switch (rstatus) {
			case SCCONF_EBUSY:
				if (messages != (char **)0) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Node \"%s\" is still in "
					    "cluster mode.\n"), nodename);
					scconf_addmessage(buffer, messages);
				}
				break;
			case SCCONF_EQUORUM:
				if (messages != (char **)0) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Node \"%s\" is still in "
					    "cluster mode.\n"), nodename);
					scconf_addmessage(buffer, messages);
				}
				break;
			default:
				break;
			} /*lint !e788 */

			return (rstatus);
		}

		/* Remove path to the node from the quorums */
		rstatus = scconf_set_qd_common_properties(NULL,
		    nodename, 0);
		if (rstatus != SCCONF_NOERR) {
			return (rstatus);
		}

		/* Remove the cables */
		rstatus = scconf_rm_cables(nodename, messages);
		if (rstatus != SCCONF_NOERR) {
			return (rstatus);
		}
	}

	/*
	 * Loop until the commit succeeds, or until
	 * a fatal error is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			return (rstatus);

		/* Get the nodeid */
		rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Find the node */
		rstatus = conf_get_nodeobj(clconf, nodeid, &node);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/*
		 * Regardless of the force option, ensure the node is
		 * not in use by a resource group
		 */
		rstatus = conf_check_node_notinuse_byrg(nodename, nodeid,
		    messages);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/*
		 * Check whether is node is not present in Installed_nodes
		 * of all Resource Types
		 */
		rstatus = conf_check_node_notinuse_byrt(nodename, nodeid,
		    messages);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Make sure that the node is not in use */
		if (!force_flg) {
			rstatus = conf_check_node_notinuse(clconf, node,
			    messages);
		}
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Remove DG reference to this node in force mode. */
		if (force_flg) {
			/* Remove node from all device groups */
			rstatus = scconf_rm_node_ds(nodename,
			    NULL, messages);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}

		/* Clear all did devices for this node */
		if (!did_cleared) {
			if (did_delete_devices_by_node(nodename) < 0) {
				rstatus = SCCONF_EUNEXPECTED;

				/* Add message? */
				if (messages != (char **)0) {
					scconf_addmessage(
					    dgettext(TEXT_DOMAIN,
					    "Unable to delete DID "
					    "devices for this node.\n"),
					    messages);
				}
				goto cleanup;
			}
			++did_cleared;
		}

		/* Remove the node */
		clconf_err = clconf_cluster_remove_node(clconf,
		    (clconf_node_t *)node);
		if (clconf_err != CL_NOERROR) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, messages);

		/* If committed okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_rm_farmnode
 *
 * Remove a node from the farm configuration.
 *
 * The node must be fully uncabled and must not be referenced by
 * any cluster services (e.g., rgm).
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EINUSE		- node is used by RG
 *	SCCONF_ENOEXIST		- the node does not exist
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_rm_farmnode(char *nodename, char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scxcfg_error scxerr;
	int scxret;
	char errbuffer[SCCONF_MAXSTRINGLEN];
	scxcfg scxhdl;
	scconf_nodeid_t nodeid;
	uint_t isfarmmember;
	char nodestate[BUFSIZ];

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodename == NULL)
		return (SCCONF_EINVAL);


	/* Get the nodeid */
	rstatus = scconf_get_farmnodeid(nodename, &nodeid);
	switch (rstatus) {
	case SCCONF_NOERR:
		break;

	case SCCONF_ENOEXIST:
		(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
		    "%s does not exist in the farm configuration.\n"),
		    nodename);
		scconf_addmessage(errbuffer, messages);
		goto cleanup;

	default:
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Check if the node is active in the farm */
	rstatus = scconf_get_farmnodestate(nodeid, nodestate);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}
	if (strcmp(nodestate, "Online") == 0) {
		(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
		    "%s is actively used in the farm configuration.\n"),
		    nodename);
		scconf_addmessage(errbuffer, messages);
		rstatus = SCCONF_EINUSE;
		goto cleanup;
	}

	/*
	 * Make sure that the node is not used by a resource group.
	 */
	rstatus = conf_check_node_notinuse_byrg(nodename, nodeid, messages);
	if (rstatus != SCCONF_NOERR) {
		if (rstatus == SCCONF_EINUSE) {
			(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
			    "%s is still in use by a resource group.\n"),
			    nodename);
			scconf_addmessage(errbuffer, messages);
		}
		goto cleanup;
	}

	/* Remove a farm node */
	/* Allocate farm config */
	rstatus = scconf_scxcfg_open(&scxhdl, &scxerr);
	if (rstatus != SCCONF_NOERR) {
		(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
		    "Failed to initialize scxcfg library.\n"));
		scconf_addmessage(errbuffer, messages);
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}
	rstatus = scconf_scxcfg_del_node(&scxhdl, nodename, &scxerr);
	if (rstatus == SCCONF_EINVAL) {
		(void) sprintf(errbuffer, dgettext(TEXT_DOMAIN,
		    "%s does not exist in the farm configuration.\n"),
		    nodename);
		scconf_addmessage(errbuffer, messages);
	}

cleanup:
	/* Release the farm config */
	scconf_scxcfg_close(&scxhdl);

	return (rstatus);
}

/*
 * scconf_get_nodeid
 *
 * Get the ID of a node from its "nodename".
 *
 * If the nodename is NULL, get the id of the current node.
 *
 * If the "nodename" is numeric, it is assumed to actually be a nodeid.
 * And, if the ID is found in the configuration, it is returned in
 * "nodeidp" upon success.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOCLUSTER	- there is no cluster
 *	SCCONF_ENOEXIST		- the nodename (or, id) is not found
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_nodeid(char *nodename, scconf_nodeid_t *nodeidp)
{
	scconf_errno_t rstatus;
	clconf_cluster_t *clconf;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodeidp == NULL)
		return (SCCONF_EINVAL);

	/* If nodename is NULL, use _cladm() sys call to get our nodeid */
	if (nodename == NULL) {
		if (cladm(CL_CONFIG, CL_NODEID, nodeidp) != 0)
			return (SCCONF_ENOCLUSTER);
		return (SCCONF_NOERR);
	}

	/* Make sure libclconf is initialized */
	rstatus = scconf_cltr_openhandle(NULL);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);

	/* Get the clconf */
	clconf = clconf_cluster_get_current();
	if (clconf == NULL)
		return (SCCONF_ENOCLUSTER);

	/* Call our internal get_nodeid function */
	rstatus = conf_get_nodeid(clconf, nodename, nodeidp);

	/* Release the clconf */
	clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_get_farmnodeid
 *
 * Get the ID of a farm node from its "nodename".
 *
 * If the "nodename" is numeric, it is assumed to actually be a nodeid.
 * And, if the ID is found in the configuration, it is returned in
 * "nodeidp" upon success.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- the nodename (or, id) is not found
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_farmnodeid(char *nodename, scconf_nodeid_t *nodeidp)
{
	scconf_errno_t rstatus;
	scxcfg_error scxerr;
	int scxret;
	scxcfg scxhdl;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodeidp == NULL)
		return (SCCONF_EINVAL);

	/* Allocate farm config */
	rstatus = scconf_scxcfg_open(&scxhdl, &scxerr);
	if (rstatus != SCCONF_NOERR) {
		return (SCCONF_EUNEXPECTED);
	}

	/* Get the nodeid for the given farm node */
	rstatus = scconf_scxcfg_get_nodeid(&scxhdl, nodename,
	    nodeidp, &scxerr);

	/* Release the farm config */
	scconf_scxcfg_close(&scxhdl);

	return (rstatus);
}

/*
 * scconf_get_farmnodename
 *
 * Get the name of a farm node from its "ID". If ID is a valid one,
 * name will be returned in nodename.
 *
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- the nodename (or, id) is not found
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_farmnodename(scconf_nodeid_t nodeid, char **nodename)
{
	scconf_errno_t rstatus;
	scxcfg_error scxerr;
	int scxret;
	scxcfg scxhdl;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodeid < 1 || nodename == NULL)
		return (SCCONF_EINVAL);

	/* Allocate farm config */
	rstatus = scconf_scxcfg_open(&scxhdl, &scxerr);
	if (rstatus != SCCONF_NOERR) {
		return (SCCONF_EUNEXPECTED);
	}

	/* Get the nodename for the given farm node ID */
	*nodename = (char *)malloc(FCFG_MAX_LEN);
	if (*nodename == NULL) {
		rstatus = SCCONF_ENOMEM;
	} else {
		rstatus = scconf_scxcfg_get_nodename(&scxhdl, nodeid,
		    *nodename, &scxerr);
	}

	/* Release the farm config */
	scconf_scxcfg_close(&scxhdl);

	return (rstatus);
}

/*
 * scconf_get_farmnodestate
 *
 * Get the farm node state(status).
 *
 * If the "nodename" is numeric, it is assumed to actually be a nodeid.
 * And, if the ID is found in the configuration, it is returned in
 * "nodeidp" upon success.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_farmnodestate(scconf_nodeid_t nodeid, char *nodestate)
{
	scconf_errno_t scconf_error = SCCONF_NOERR;
	uint_t isfarmmember;

	/* Must be root */
	if (getuid() != 0) {
		return (SCCONF_EPERM);
	}

	/* Check arguments */
	if (nodeid < 1 || nodestate == NULL) {
		return (SCCONF_EINVAL);
	}

	/* Initialize fmm handle */
	scconf_error = scconf_fmm_initialize();
	if (scconf_error != SCCONF_NOERR) {
		return (scconf_error);
	}

	/* Get the membership status */
	scconf_error = scconf_isfarmmember(nodeid, &isfarmmember);
	if (scconf_error != SCCONF_NOERR) {
		goto cleanup;
	}

	if (isfarmmember) {
		scconf_get_default_status_string(
		    SCSTAT_ONLINE, nodestate);
	} else {
		scconf_get_default_status_string(
		    SCSTAT_OFFLINE, nodestate);
	}

cleanup:
	/* Finalize fmm handle */
	scconf_fmm_finalize();

	return (scconf_error);
}

/*
 * scconf_get_nodename
 *
 * Get the name of a node from its "nodeid".  Upon success,
 * a pointer to the nodename is left in "nodenamep".
 *
 * It is the caller's responsibility to free memory allocated
 * for the "nodename".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOCLUSTER	- there is no cluster
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_nodename(scconf_nodeid_t nodeid, char **nodenamep)
{
	scconf_errno_t rstatus;
	clconf_cluster_t *clconf;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (nodeid < 1 || nodenamep == NULL)
		return (SCCONF_EINVAL);

	/* Make sure libclconf is initialized */
	rstatus = scconf_cltr_openhandle(NULL);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);

	/* Get the clconf */
	clconf = clconf_cluster_get_current();
	if (clconf == NULL)
		return (SCCONF_ENOCLUSTER);

	/* Call our internal get_nodeid function */
	rstatus = conf_get_nodename(clconf, nodeid, nodenamep);

	/* Release the clconf */
	clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_get_cached_nodename
 *
 * Get the name of a node from its "nodeid" from the cached copy of the cluster
 * configuration. Upon success, a pointer to the nodename is left in
 * "nodenamep".
 *
 * It is the caller's responsibility to free memory allocated
 * for the "nodename".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_cached_nodename(const scconf_cfg_cluster_t *pcluster_config,
    const scconf_nodeid_t nodeid, char **nodenamep)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_cfg_node_t *plnodes = NULL;
	scconf_cfg_node_t *pnode = NULL;
	boolean_t found = B_FALSE;

	plnodes = pcluster_config->scconf_cluster_nodelist;
	if (plnodes == NULL) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	pnode = plnodes;
	while (pnode != NULL) {
		if (pnode->scconf_node_nodeid == nodeid) {
			if (pnode->scconf_node_nodename == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			*nodenamep = strdup(pnode->scconf_node_nodename);
			if (*nodenamep == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			found = B_TRUE;
			break;
		}

		pnode = pnode->scconf_node_next;
	}

	if (found == B_FALSE)  {
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

cleanup:
	return (rstatus);
}

/*
 * scconf_get_phost_nodeid
 *
 * Get the node ID of the node associated with the given "privatehostname".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOCLUSTER	- there is no cluster
 *	SCCONF_ENOEXIST		- not a known private hostname
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_phost_nodeid(char *privatehostname, scconf_nodeid_t *nodeidp)
{
	scconf_errno_t rstatus;
	scconf_cfg_cluster_t *clconfig = (scconf_cfg_cluster_t *)0;
	scconf_cfg_node_t *cluster_node;
	char *phost;

	/* Check arguments */
	if (privatehostname == NULL || nodeidp == NULL)
		return (SCCONF_EINVAL);

	/* Get a copy of the config */
	rstatus = scconf_get_clusterconfig(&clconfig);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);

	/* Search the nodelist for a matching private hostname */
	rstatus = SCCONF_ENOEXIST;
	for (cluster_node = clconfig->scconf_cluster_nodelist;  cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {
		phost = cluster_node->scconf_node_privatehostname;
		if (phost != NULL && strcmp(phost, privatehostname) == 0) {
			*nodeidp = cluster_node->scconf_node_nodeid;
			rstatus = SCCONF_NOERR;
		}
	}

	/* Free the config */
	if (clconfig != NULL)
		scconf_free_clusterconfig(clconfig);

	return (rstatus);
}

/*
 * scconf_get_node_privateipaddr
 *
 * Return the private IP address for the given node.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOCLUSTER	- node is not part of a cluster
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_EINVAL		- invalid arguments
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_node_privateipaddr(scconf_nodeid_t nodeid, struct in_addr *addr)
{
	scconf_errno_t rstatus;
	clconf_cluster_t *clconf;

	/* Check validity of arguments */
	if (addr == NULL) {
		return (SCCONF_EINVAL);
	}

	/* Make sure libclconf is initialized */
	rstatus = scconf_cltr_openhandle(NULL);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);

	/* Get the clconf */
	clconf = clconf_cluster_get_current();
	if (clconf == NULL)
		return (SCCONF_ENOCLUSTER);

	/* Call our internal get_node_inaddr function */
	rstatus = conf_get_node_inaddr(clconf, nodeid, addr);

	/* Release the clconf */
	clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_get_node_zones
 *
 * Return the zone names and their private hostnames.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOCLUSTER	- node is not part of a cluster
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_EINVAL		- invalid arguments
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_node_zones(scconf_nodeid_t nodeid, scconf_zonelist_t **zonelist)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scha_cluster_t hdl;
	scha_str_array_t *all_nonglobal_node_zones;
	scconf_zonelist_t *zoneptr, *listhead, *listtail;
	uint_t i;
	uint_t listcount;
#if SOL_VERSION >= __s10
	char *zonename = NULL;
#endif

	zoneptr = listhead = listtail = NULL;

	/* Check validity of arguments */
	if (zonelist == NULL)
		return (SCCONF_EINVAL);

	rstatus = scha_cluster_open(&hdl);
	if (rstatus != SCHA_ERR_NOERR)
		return (SCCONF_EUNEXPECTED);

	/* Get the names of all the zones in this node */
	rstatus = scha_cluster_get(hdl, SCHA_ALL_NONGLOBAL_ZONES_NODEID,
	    nodeid, &all_nonglobal_node_zones);
	if (rstatus != SCHA_ERR_NOERR) {
		(void) scha_cluster_close(hdl);
		return (SCCONF_EUNEXPECTED);
	}

	/* Populate the zonelist with zone names and their hostnames */
	listcount = all_nonglobal_node_zones->array_cnt;
	for (i = 0; i < (int)listcount; i++) {

		/* Allocate memory for scconf_zonelist_t */
		zoneptr = (scconf_zonelist_t *)malloc(
		    sizeof (scconf_zonelist_t));
		if (zoneptr == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Store the zonename */
		zoneptr->scconf_zone_name = strdup(
		    all_nonglobal_node_zones->str_array[i]);
		if (zoneptr->scconf_zone_name == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

#if SOL_VERSION >= __s10
		/*
		 * Since the zonename got from scha_cluster_get() is
		 * a logical node name (nodename or nodename:zonename),
		 * isolate the zonename from the nodename.
		 */
		zonename = strdup(zoneptr->scconf_zone_name);
		zonename = strchr(zonename, ':');
		if (zonename) {
			*zonename++ = '\0';

			/* Check for zonename */
			if (strlen(zonename) == 0)
				return (SCCONF_EUNEXPECTED);
		} else {
			/* Zonename must be a "node:zone" */
			return (SCCONF_EUNEXPECTED);
		}
		zoneptr->scconf_zone_privhostname = scprivip_get_hostname(
			zonename, nodeid);
#endif

		zoneptr->scconf_zonelist_next = NULL;

		/* Append the allocated struct to scconf_zonelist_t */
		if (listhead == NULL)
			listhead = zoneptr;
		if (listtail != NULL)
			listtail->scconf_zonelist_next = zoneptr;
		listtail = zoneptr;
	}

	*zonelist = listhead;

cleanup:
	if (rstatus != SCCONF_NOERR)
		scconf_free_zonelist(listhead);

	(void) scha_cluster_close(hdl);
	return (rstatus);
}

/*
 * scconf_free_zonelist
 *
 * Free all memory associated with "scconf_zonelist_t" structure.
 */
void
scconf_free_zonelist(scconf_zonelist_t *zonelist)
{
	scconf_zonelist_t *listp, *listp_next;

	/* free memory for each member of the list */
	for (listp = zonelist; listp; listp = listp_next) {
		listp_next = listp->scconf_zonelist_next;
		if (listp->scconf_zone_name)
			free(listp->scconf_zone_name);
		if (listp->scconf_zone_privhostname)
			free(listp->scconf_zone_privhostname);
		free(listp);
	}

	zonelist = NULL;
}

/*
 * scconf_get_farmnodes
 *
 * Return the farm nodenames, monitoring state, adapters.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- Not enough memory
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_EINVAL		- invalid arguments
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_farmnodes(scconf_farm_nodelist_t **farm_nodelist)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scxcfg_error scxerr;
	int scxret;
	scconf_farm_nodelist_t *farmptr, *listhead, *listtail;
	int i, num = 0;
	scxcfg scxhdl;
	f_property_t *farm_nodes = NULL;
	f_property_t *farmnodes;
	char property[FCFG_MAX_LEN];
	scconf_nodeid_t nodeid;
	char *nodename = NULL;
	scconf_nodestate_t monstate;
	char *adapters = NULL;
	char monitoring[SCCONF_MAXSTRINGLEN];
	char adaptype[SCCONF_MAXSTRINGLEN];
	char adapname1[SCCONF_MAXSTRINGLEN];
	char adapname2[SCCONF_MAXSTRINGLEN];

	farmptr = listhead = listtail = NULL;

	/* Check validity of arguments */
	if (farm_nodelist == NULL)
		return (SCCONF_EINVAL);

	/* Allocate farm config */
	rstatus = scconf_scxcfg_open(&scxhdl, &scxerr);
	if (rstatus != SCCONF_NOERR) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Get the list of N nodeids from FARM configuration */
	rstatus = scconf_scxcfg_getlistproperty_value(&scxhdl, NULL,
		    SCCONF_FARM_NODES, &farm_nodes, &num, &scxerr);
	if (rstatus != SCCONF_NOERR) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	if (num == 0 || farm_nodes == NULL) {
		goto cleanup;
	}

	/* Fill the farm_nodelist with node names and their monitoring state */
	for (farmnodes = farm_nodes; farmnodes; farmnodes = farmnodes->next) {

		/* Get the nodeid */
		nodeid = atoi(farmnodes->value);
		if (nodeid < 0)
			continue;

		/* Allocate memory for nodename */
		nodename = (char *)malloc(SCCONF_MAXSTRINGLEN);
		if (nodename == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Get the nodename */
		rstatus = scconf_scxcfg_get_nodename(&scxhdl, nodeid,
		    nodename, &scxerr);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Get the monitoring state */
		(void) sprintf(property, SCCONF_FARM_NODE_MONITORING_STATE,
		    nodeid);
		rstatus = scconf_scxcfg_getproperty_value(&scxhdl, NULL,
			    property, monitoring, &scxerr);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		if (strcmp(monitoring, STR_ENABLED) == 0) {
			monstate = SCCONF_STATE_ENABLED;
		} else {
			monstate = SCCONF_STATE_DISABLED;
		}

		/* Get the adapter type */
		(void) sprintf(property, SCCONF_FARM_NODE_NET_INTER_1_TYPE,
		    nodeid);
		rstatus = scconf_scxcfg_getproperty_value(&scxhdl, NULL,
			    property, adaptype, &scxerr);
		if (rstatus != SCCONF_NOERR) {
			goto cleanup;
		}

		/* Allocate memory for adapters */
		adapters = (char *)malloc(2 * SCCONF_MAXSTRINGLEN);
		if (adapters == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Get the adapter name(s) for respective adapter type. */
		if (strcmp(adaptype, STR_SIMPLE) == 0) {
			/* simple adapter type */
			(void) sprintf(property,
			    SCCONF_FARM_NODE_NET_INTER_1_NAME, nodeid);
			rstatus = scconf_scxcfg_getproperty_value(&scxhdl,
			    NULL, property, adapname1, &scxerr);
			if (rstatus != SCCONF_NOERR) {
				goto cleanup;
			}

			/* Save adapname1 into adapters */
			sprintf(adapters, "%s", adapname1);

		} else if (strcmp(adaptype, STR_REDUNDANT) == 0) {
			/* redundant adapter type */
			(void) sprintf(property,
			    SCCONF_FARM_NODE_NET_INTER_1_ADAP_1_NAME, nodeid);
			rstatus = scconf_scxcfg_getproperty_value(&scxhdl,
			    NULL, property, adapname1, &scxerr);
			if (rstatus != SCCONF_NOERR) {
				goto cleanup;
			}

			/* redudant type has adapter2 */
			(void) sprintf(property,
			    SCCONF_FARM_NODE_NET_INTER_1_ADAP_2_NAME, nodeid);
			rstatus = scconf_scxcfg_getproperty_value(&scxhdl,
			    NULL, property, adapname2, &scxerr);
			if (rstatus != SCCONF_NOERR) {
				goto cleanup;
			}

			/* Save adapname1, adapname2 into adapters */
			sprintf(adapters, "%s %s", adapname1, adapname2);
		} else {
			/* unknown adapter type */
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Allocate memory for scconf_farm_nodelist_t */
		farmptr = (scconf_farm_nodelist_t *)malloc(
		    sizeof (scconf_farm_nodelist_t));
		if (farmptr == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Store the nodename, nodeid, monitoring, adapters */
		farmptr->scconf_node_nodename = nodename;
		farmptr->scconf_node_nodeid = nodeid;
		farmptr->scconf_node_monstate = monstate;
		farmptr->scconf_node_adapters = adapters;

		farmptr->scconf_node_next = NULL;

		/* Append the allocated struct to scconf_farm_nodelist_t */
		if (listhead == NULL)
			listhead = farmptr;
		if (listtail != NULL)
			listtail->scconf_node_next = farmptr;
		listtail = farmptr;
	}

	*farm_nodelist = listhead;

cleanup:
	if (rstatus != SCCONF_NOERR) {
		/* Free nodename */
		if (nodename) {
			free(nodename);
		}

		/* Free adapters */
		if (adapters) {
			free(adapters);
		}

		scconf_free_farm_nodelist(listhead);
	}

	/* Free node ids list */
	if (farm_nodes) {
		scconf_scxcfg_freelist(farm_nodes);
	}

	/* Release the farm config */
	scconf_scxcfg_close(&scxhdl);

	return (rstatus);
}

/*
 * scconf_free_farm_nodelist
 *
 * Free all memory associated with "scconf_farm_nodelist_t" structure.
 */
void
scconf_free_farm_nodelist(scconf_farm_nodelist_t *farm_nodelist)
{
	scconf_farm_nodelist_t *listp, *listp_next;

	/* free memory for each member of the list */
	for (listp = farm_nodelist; listp; listp = listp_next) {
		listp_next = listp->scconf_node_next;
		if (listp->scconf_node_nodename)
			free(listp->scconf_node_nodename);
		if (listp->scconf_node_adapters)
			free(listp->scconf_node_adapters);
		free(listp);
	}

	farm_nodelist = NULL;
}

/*
 * conf_get_node_inaddr
 *
 * Return the private IP address for the given node.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_get_node_inaddr(clconf_cluster_t *clconf, scconf_nodeid_t nodeid,
    struct in_addr *addr)
{
	clconf_obj_t *node;
	scconf_errno_t scconferr;
	const char *prop;
	struct hostent *hp;
	char **p;
	char *buf;
	int buflen;
	int err;
	struct hostent result;

	scconferr = conf_get_nodeobj(clconf, nodeid, &node);
	if (scconferr != SCCONF_NOERR) {
		return (scconferr);
	}

	/* Get the private hostname */
	prop = clconf_obj_get_property(node, PROP_NODE_PRIVATE_HOSTNAME);
	if (prop == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	buflen = 4 * BUFSIZ;
	buf = (char *)malloc(sizeof (char) * (size_t)buflen);
	if (buf == NULL) {
		return (SCCONF_ENOMEM);
	}

	hp = gethostbyname_r(prop, &result, buf, buflen, &err);
	if (hp == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	p = hp->h_addr_list;
	if (*p == 0) {
		return (SCCONF_EUNEXPECTED);
	}

	(void) memcpy(&(addr->s_addr), *p, sizeof (addr->s_addr));
	return (SCCONF_NOERR);
}

/*
 * conf_get_nodeid
 *
 * Return the "nodeid" in "nodeidp" for the given "nodename"
 *
 * If the "nodename" is numeric, it is assumed to actually be a nodeid.
 * And, if the ID is found in the configuration, it is returned in
 * "nodeidp" upon success.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the nodename (or, id) is not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_nodeid(clconf_cluster_t *clconf, char *nodename,
    scconf_nodeid_t *nodeidp)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	scconf_nodeid_t nodeid = 0;
	scconf_nodeid_t id;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	const char *name;
	char *s;

	/* Check arguments */
	if (clconf == NULL || nodename == NULL || *nodename == '\0' ||
	    nodeidp == NULL)
		return (SCCONF_EINVAL);

	/* Check to see if nodename is a nodeid (number > 0) */
	for (s = nodename;  *s;  ++s)
		if (!isdigit(*s))
			break;
	if (*s == '\0') {
		nodeid = (scconf_nodeid_t)atoi(nodename);
		if (nodeid < 1)
			return (SCCONF_EINVAL);
	}

	/* Iterate through the list of configured nodes */
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {

		id = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (id == (scconf_nodeid_t)-1) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		name = clconf_obj_get_name(node);

		/* If nodeid is set, search for configured nodeid */
		if (nodeid > (uint_t)0) {
			if (nodeid == id) {
				*nodeidp = nodeid;
				rstatus = SCCONF_NOERR;
				goto cleanup;
			}

		/* Otherwise, search for a nodename match */
		} else if (name != NULL && strcmp(nodename, name) == 0) {
			*nodeidp = id;
			rstatus = SCCONF_NOERR;
			goto cleanup;
		}
		clconf_iter_advance(currnodesi);
	}

	/* The nodeid is not found */
	rstatus = SCCONF_ENOEXIST;

cleanup:
	/* Release iterator */
	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	return (rstatus);
}

/*
 * conf_get_nodename
 *
 * Return the "nodename" in "nodenamep" for the given "nodeid".
 *
 * If successful, the caller is responsible for freeing memory
 * returned in "nodenamep".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_nodename(clconf_cluster_t *clconf, scconf_nodeid_t nodeid,
    char **nodenamep)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	scconf_nodeid_t id;
	const char *name;
	char *nodename = NULL;

	/* Check arguments */
	if (clconf == NULL || nodeid < 1 || nodenamep == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of configured nodes */
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {

		id = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (id == (scconf_nodeid_t)-1) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		name = clconf_obj_get_name(node);

		/* Search for configured nodename */
		if (name != NULL && nodeid == id) {
			nodename = strdup(name);
			if (nodename == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
			*nodenamep = nodename;
			rstatus = SCCONF_NOERR;
			goto cleanup;
		}
		clconf_iter_advance(currnodesi);
	}

	/* The nodename is not found */
	rstatus = SCCONF_ENOEXIST;

cleanup:
	/* Release iterator */
	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	return (rstatus);
}

/*
 * conf_get_nodeobj
 *
 * Return the object pointer (clconf_obj_t *) for the given
 * "nodeid" in "nodep", using the given "clconf".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the nodeid is not found
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_nodeobj(clconf_cluster_t *clconf, scconf_nodeid_t nodeid,
    clconf_obj_t **nodep)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	scconf_nodeid_t id;

	/* Check arguments */
	if (clconf == NULL || nodeid < 1 || nodep == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of configured nodes */
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {

		id = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (id == (scconf_nodeid_t)-1) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		if (id == nodeid) {
			*nodep = node;
			rstatus = SCCONF_NOERR;
			goto cleanup;
		}
		clconf_iter_advance(currnodesi);
	}

	/* The nodeid is not found */
	rstatus = SCCONF_ENOEXIST;

cleanup:
	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	return (rstatus);
}

/*
 * conf_lookup_privatehostname
 *
 * Return the object pointer (clconf_obj_t *) for the given
 * "privatehostname" in "nodep", using the given "clconf".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- the privatehostname is not found
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_lookup_privatehostname(clconf_cluster_t *clconf, char *privatehostname,
    clconf_obj_t **nodep)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	clconf_obj_t *node;
	const char *prop;

	/* Check arguments */
	if (clconf == NULL || privatehostname == NULL || nodep == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of configured nodes */
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {

		prop = clconf_obj_get_property(node,
		    PROP_NODE_PRIVATE_HOSTNAME);
		if (prop != NULL &&
		    strcmp(prop, privatehostname) == 0) {
			*nodep = node;
			rstatus = SCCONF_NOERR;
			goto cleanup;
		}

		clconf_iter_advance(currnodesi);
	}

	/* The privatehostname is not found */
	rstatus = SCCONF_ENOEXIST;

cleanup:
	/* Release iterator */
	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	return (rstatus);
}

/*
 * conf_check_node_notinuse
 *
 * Return SCCONF_EINUSE if the given node is cabled or in use by any
 * cluster services (e.g., did, dcs, quorum).
 *
 * Does not check if node is in use by RGM. That should be checked by
 * caller.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINUSE		- node is cabled or otherwise referenced
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_check_node_notinuse(clconf_cluster_t *clconf, clconf_obj_t *node,
    char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_errno_t status;
	char *nodename = NULL;
	scconf_nodeid_t nodeid;
	char buffer[SCCONF_MAXSTRINGLEN];
	uint_t statechk = 0;

	/* Check arguments */
	if (clconf == NULL || node == NULL)
		return (SCCONF_EINVAL);

	/* I18N houskeeping */
	if (messages != (char **)0)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Get the nodeid */
	nodeid = (scconf_nodeid_t)clconf_obj_get_id(node);
	if (nodeid == (scconf_nodeid_t)-1) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Get the nodename */
	status = conf_get_nodename(clconf, nodeid, &nodename);
	if (status !=  SCCONF_NOERR || nodename == NULL) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/*
	 * Make sure that the node is not cabled.
	 */
	status = conf_check_notinuse_bycable(clconf, node, statechk);

	/* If in use, try to add message, then continue */
	if (status == SCCONF_EINUSE) {
		rstatus = SCCONF_EINUSE;

		/* Add message? */
		if (messages != (char **)0) {
			(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
			    "Node \"%s\" is still cabled.\n"), nodename);
			scconf_addmessage(buffer, messages);
		}

	/* Otherwise, if bad return status, return that status now */
	} else if (status != SCCONF_NOERR) {
		rstatus = status;
		goto cleanup;
	}

	/*
	 * Make sure that the node is not used by a device group
	 * including any autogenerated rawdisk device groups.
	 */
	status = conf_check_node_notinuse_bydg(nodename, nodeid, 1, messages);

	/* If in use, set rstatus, then continue */
	if (status == SCCONF_EINUSE) {
		rstatus = SCCONF_EINUSE;

	/* Otherwise, if bad return status, return that status now */
	} else if (status != SCCONF_NOERR) {
		rstatus = status;
		goto cleanup;
	}

	/* Make sure that the node is not in use for quorum */
	status = conf_check_node_notinuse_byquorum(clconf, nodename, messages);

	/* If in use, add possible message, set rstatus, then continue */
	if (status != SCCONF_NOERR) {
		switch (status) {
		case SCCONF_EQUORUM:
			/* Add message? */
			if (messages != (char **)0)
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				    "All two-node clusters must have at "
				    "least one shared quorum device.\n"),
				    messages);

			/* If rstatus is otherwise good, set it to EQUORUM */
			if (rstatus == SCCONF_NOERR)
				rstatus = status;

			break;

		case SCCONF_EINUSE:
		default:
			rstatus = status;
			break;
		} /*lint !e788 */

		goto cleanup;
	}

cleanup:
	/* Free nodename */
	if (nodename != NULL)
		free(nodename);

	return (rstatus);
}

/*
 * conf_check_node_notinuse_byrg
 *
 * Return SCCONF_EINUSE if the given node is in use by any Resource Group.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINUSE		- node is in use by a resource group
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_check_node_notinuse_byrg(char *nodename, scconf_nodeid_t nodeid,
    char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char buffer[SCCONF_MAXSTRINGLEN];
	char **managed_rgs = NULL;
	char **unmanaged_rgs = NULL;
	char **rgnames;
	int i;
	scha_errmsg_t rgm_status = {SCHA_ERR_NOERR, NULL};
	rgm_rg_t *rg = (rgm_rg_t *)0;
	nodeidlist_t *nl;
	scconf_nodeid_t nodenameid;
	boolean_t iseuropa = B_FALSE;

	/* Check arguments */
	if (nodename == NULL || nodeid < 1)
		return (SCCONF_EINVAL);

	/* Make sure that the node is not part of any RGM config */
	rgm_status = rgm_scrgadm_getrglist(&managed_rgs, &unmanaged_rgs,
	    B_FALSE, NULL);
	if (rgm_status.err_code != SCHA_ERR_NOERR &&
	    rgm_status.err_code != SCHA_ERR_RG) {
		switch (rgm_status.err_code) {
		case SCHA_ERR_NOMEM:
			rstatus = SCCONF_ENOMEM;
			break;

		default:
			rstatus = SCCONF_EUNEXPECTED;
			break;
		}
		goto cleanup;
	}

	/* Check if farm mgmt enabled */
	rstatus = scconf_iseuropa(&iseuropa);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Search the two RG types, managed and unmanaged */
	for (i = 0;  i < 2;  ++i) {

		/* rgnames list is either managed or unmanaged */
		rgnames = (i == 0) ? managed_rgs : unmanaged_rgs;

		/* If nothing, skip */
		if (rgnames == (char **)0)
			continue;

		/* Search each RG type */
		while (*rgnames) {

			/* Free last RG */
			if (rg != (rgm_rg_t *)0) {
				rgm_free_rg(rg);
				rg = (rgm_rg_t *)0;
			}

			/* Get RG for this RGname */
			rgm_status = rgm_scrgadm_getrgconf(*rgnames, &rg, NULL);
			if (rgm_status.err_code != SCHA_ERR_NOERR) {
				switch (rgm_status.err_code) {
				case SCHA_ERR_NOMEM:
					rstatus = SCCONF_ENOMEM;
					goto cleanup;

				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}
			}

			rstatus = scconf_get_nodeid(nodename, &nodenameid);
			if (iseuropa && rstatus == SCCONF_ENOEXIST) {
				rstatus = scconf_get_farmnodeid(
				    nodename, &nodenameid);
			}
			if (rstatus != SCCONF_NOERR) {
				goto cleanup;
			}

			for (nl = rg->rg_nodelist;  nl; nl = nl->nl_next) {
				if (nl->nl_nodeid == nodenameid) {
					if (rg->rg_name != NULL) {
						(void) sprintf(buffer,
						    dgettext(TEXT_DOMAIN,
						    "Node \"%s\" is still in "
						    "use by resource group "
						    "\"%.256s\".\n"),
						    nodename, rg->rg_name);
						scconf_addmessage(buffer,
						    messages);
					}
					rstatus = SCCONF_EINUSE;
					break;
				}
			}

			/* Next */
			++rgnames;
		}
	}

cleanup:
	/* Free RG names */
	if (unmanaged_rgs != NULL) {
		for (rgnames = unmanaged_rgs;  *rgnames;  ++rgnames)
			free(*rgnames);
		free(unmanaged_rgs);
	}
	if (managed_rgs != NULL) {
		for (rgnames = managed_rgs;  *rgnames;  ++rgnames)
			free(*rgnames);
		free(managed_rgs);
	}

	/* Free last RG */
	if (rg != (rgm_rg_t *)0)
		rgm_free_rg(rg);

	return (rstatus);
}

/*
 * conf_check_node_notinuse_byrt
 *
 * Return SCCONF_EINUSE if none of the Resource Types are installed on
 * the given node. Essentially this function checks the Installed_nodes
 * property of all the Resource Types.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 * If nodeid is greater than zero, then that is taken as id of the node,else
 * nodename is used to look-up the node id.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINUSE		- node is in use by a resource type
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_check_node_notinuse_byrt(char *nodename, scconf_nodeid_t node_id,
	char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_nodeid_t nodeid;
	scha_errmsg_t scha_status = {SCHA_ERR_NOERR, NULL};
	char **rt_names = NULL;
	rgm_rt_t *rt = (rgm_rt_t *)NULL;
	int count;
	nodeidlist_t *inst_nodeids = (nodeidlist_t *)NULL;
	char buffer[SCCONF_MAXSTRINGLEN];

	/* Verify that at least one among nodename and nodeid was specified */
	if (nodename == NULL && nodeid < 1) {
		return (SCCONF_EINVAL);
	}

	/* If nodeid was specified take it, otherwise do a look-up */
	if (node_id < 1) {
		rstatus = scconf_get_nodeid(nodename, &nodeid);
		/* Verify this is a valid node */
		if (rstatus != SCCONF_NOERR) {
			return (SCCONF_EINVAL);
		}
	} else {
		nodeid = node_id;
	}


	/* Get all the Resource Types */
	scha_status = rgm_scrgadm_getrtlist(&rt_names, NULL);
	if (scha_status.err_code != SCHA_ERR_NOERR) {
		return (SCCONF_EUNEXPECTED);
	}

	/* If there are no RTs, return */
	if (rt_names == NULL) {
		return (SCCONF_NOERR);
	}

	for (count = 0; rt_names[count] != NULL; count++) {
		if (rt != (rgm_rt_t *)0) {
			rgm_free_rt(rt);
			rt = (rgm_rt_t *)0;
		}

		scha_status = rgm_scrgadm_getrtconf(rt_names[count], &rt,
							NULL);

		/* Error getting the RT config. continue */
		if (scha_status.err_code != SCHA_ERR_NOERR) {
			continue;
		}

		/* If Installed_nodes is <All>, then we can skip. */
		if (rt->rt_instl_nodes.is_ALL_value) {
			continue;
		}

		inst_nodeids = rt->rt_instl_nodes.nodeids;

		for (; inst_nodeids; inst_nodeids = inst_nodeids->nl_next) {

			if (inst_nodeids->nl_nodeid != nodeid) {
				continue;
			}

			/* If we are here then the nodename is present */
			rstatus = SCCONF_EINUSE;
			(void) sprintf(buffer,
				    dgettext(TEXT_DOMAIN,
				    "Node \"%s\" still has "
				    "registered resource type "
				    "\"%.256s\".\n"),
				    nodename, rt->rt_name);
			scconf_addmessage(buffer,
				    messages);

		}
	}

	/* Free memory */
	if (rt != (rgm_rt_t *)0) {
		rgm_free_rt(rt);
		rt = (rgm_rt_t *)0;
	}

	rgm_free_strarray(rt_names);
	return (rstatus);
}


/*
 * conf_check_node_notinuse_bydg
 *
 * Return SCCONF_EINUSE if the given node is in use by any Device Group.
 * If skip is set to true (1), then all rawdisk device groups will be
 * ignored and won't generate an error message.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINUSE		- node is in use by a resource group
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_check_node_notinuse_bydg(char *nodename, scconf_nodeid_t nodeid, int skip,
    char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char buffer[SCCONF_MAXSTRINGLEN];
	scconf_cfg_ds_t *dgconfig = (scconf_cfg_ds_t *)0;
	scconf_cfg_ds_t *dg;
	scconf_nodeid_t *nodeidp;

	/* Check arguments */
	if (nodename == NULL || nodeid < 1)
		return (SCCONF_EINVAL);

	/* Get the device group config */
	rstatus = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG,
	    &dgconfig);
	if (rstatus != SCCONF_NOERR) {
		if (rstatus == SCCONF_ENOCLUSTER)
			rstatus = SCCONF_NOERR;
		goto cleanup;
	}

	/* Search each dg */
	for (dg = dgconfig;  dg;  dg = dg->scconf_ds_next) {

		/* skip anything without a nodelist */
		if (dg->scconf_ds_nodelist == NULL)
			continue;

		/* If selected, skip rawdisk device services */
		if (skip == 1 && (strcmp(dg->scconf_ds_type, "Disk") == 0 ||
				strcmp(dg->scconf_ds_type, "Local_Disk") == 0))
			continue;

		/* nodelist is an array of pointers to nodeids, ending in 0 */
		for (nodeidp = (scconf_nodeid_t *)dg->scconf_ds_nodelist;
		    *nodeidp;
		    ++nodeidp) {

			/* Found one */
			if (nodeid == *nodeidp) {
				if (dg->scconf_ds_name != NULL) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Node \"%s\" is still in "
					    "use by device group "
					    "\"%.256s\".\n"),
					    nodename, dg->scconf_ds_name);
					scconf_addmessage(buffer,
					    messages);
				}
				rstatus = SCCONF_EINUSE;
				break;
			}
		}
	}

cleanup:
	if (dgconfig != NULL)
		scconf_free_ds_config(dgconfig);

	return (rstatus);
}

/*
 * conf_check_node_notinuse_byquorum
 *
 * Return SCCONF_EINUSE if the given node is in use as a path
 * to a quorum device.  Return SCCONF_EQUORUM, if this reduces the
 * cluster to a two-node cluster without a quorum device.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINUSE		- node is in use by quorum device
 *	SCCONF_EQUORUM		- quorum would be compromised.
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_check_node_notinuse_byquorum(clconf_cluster_t *clconf, char *nodename,
    char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char buffer[SCCONF_MAXSTRINGLEN];
	scconf_cfg_cluster_t *clconfig = (scconf_cfg_cluster_t *)0;
	scconf_cfg_qdev_t *qdev;
	scconf_cfg_qdevport_t *port;
	const char *propval;
	int installmode;
#ifdef WEAK_MEMBERSHIP
	boolean_t multiple_partitions = B_FALSE;
#endif

	/* Check arguments */
	if (clconf == NULL || nodename == NULL)
		return (SCCONF_EINVAL);

	/* Get the cluster config */
	rstatus = scconf_handle_get_clusterconfig((scconf_cltr_handle_t)clconf,
	    &clconfig);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Get the installmode flag */
	propval = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_INSTALLMODE);
	if (propval && strcmp(propval, "enabled") == 0)
		installmode = 1;
	else
		installmode = 0;

#ifdef WEAK_MEMBERSHIP
	scconf_get_multiple_partitions(clconf, &multiple_partitions);
#endif

	/* Search each quorum device */
	for (qdev = clconfig->scconf_cluster_qdevlist;  qdev;
	    qdev = qdev->scconf_qdev_next) {

		for (port = qdev->scconf_qdev_ports;  port;
		    port = port->scconf_qdevport_next) {

			/* skip anything without a nodename */
			if (port->scconf_qdevport_nodename == NULL)
				continue;

			/* Found one */
			if (strcmp(
			    nodename, port->scconf_qdevport_nodename) == 0) {
				if (qdev->scconf_qdev_name != NULL) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Node \"%s\" is still in "
					    "use by quorum device "
					    "\"%.256s\".\n"),
					    nodename, qdev->scconf_qdev_name);
					scconf_addmessage(buffer,
					    messages);
				}
				rstatus = SCCONF_EINUSE;
				break;
			}
		}
	}

	/*
	 * If there no quorum devices, we do not allow the user to go
	 * from three nodes to two nodes under any circumstances.  However,
	 * we do allow going from two to one as long as installmode is
	 * enabled or if weak membership is enabled.
	 */
	if (clconfig->scconf_cluster_qdevlist == (scconf_cfg_qdev_t *)0) {

		switch (conf_nodecount(clconf)) {
		case -1:
		case 0:
		case 1:
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;

		case 2:
#ifdef WEAK_MEMBERSHIP
			if (!installmode && multiple_partitions == B_FALSE) {
#else
			if (!installmode) {
#endif
				rstatus = SCCONF_EQUORUM;
				goto cleanup;
			}
			break;

		case 3:
			rstatus = SCCONF_EQUORUM;
			goto cleanup;

		default:
			break;
		}
	}

cleanup:
	if (clconfig != NULL)
		scconf_free_clusterconfig(clconfig);

	return (rstatus);
}

/*
 * conf_remove_cables
 *
 * Remove the cables that have an endpoint to the selected node
 *
 * Possible return values:
 *
 * SCCONF_NOERR			- success
 * SCCONF_EINVAL		- invalid argument
 * SCCONF_ENOMEM		- not enough memory
 * SCCONF_EBUSY			- would result in node isolation
 * SCCONF_EUNEXPECTED		- internal or unexpected error
 * SCCONF_ESETUP		- setup attempt failed
 * SCCONF_ENOCLUSTER		- cluster does not exist
 * SCCONF_EQUORUM		- would result in loss of quorum
 * SCCONF_EOVERFLOW		- message buffer overflow
 */
scconf_errno_t
conf_remove_cables(clconf_cluster_t *clconf,
    clconf_obj_t *node, scconf_nodeid_t nodeid, char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_errnum_t clconf_err;
	clconf_obj_t *port_parent, *cable, *port, *node_obj;
	clconf_obj_t *ports[2], *port_parents[2];
	int port_id, port_parent_id;
	int e;
	scconf_nodeid_t node_ids[2], node_id = (scconf_nodeid_t)-1;
	clconf_objtype_t clobj_type, port_parent_type;
	clconf_objtype_t port_parent_types[2];
	clconf_iter_t *currcablesi, *cablesi = (clconf_iter_t *)0;
	clconf_cluster_t *existing_clconf = (clconf_cluster_t *)0;

	/* Check arguments */
	if (clconf == NULL || node == NULL)
		return (SCCONF_EINVAL);

	/* Make sure that the object is indeed a node */
	clobj_type = clconf_obj_get_objtype(node);
	if (clobj_type != CL_NODE)
		return (SCCONF_EINVAL);

	/* Make a copy of the original */
	existing_clconf = (clconf_cluster_t *)clconf_obj_deep_copy(
		(clconf_obj_t *)clconf);
	if (existing_clconf == NULL)
		return (SCCONF_ENOMEM);

	/* Iterate through the list of cables */
	cablesi = currcablesi = clconf_cluster_get_cables(clconf);
	while ((cable = clconf_iter_get_current(currcablesi)) != NULL) {
		/* Initialize the node_ids array */
		node_ids[0] = node_ids[1] = (scconf_nodeid_t)-1;

		/* Check both endpoints */
		for (e = 1; e <= 2; ++e) {
			/* Get port and port ID */
			port = (clconf_obj_t *)clconf_cable_get_endpoint(
				(clconf_cable_t *)cable, e);
			if (port == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			ports[e - 1] = port;

			port_id = clconf_obj_get_id(port);
			if (port_id == -1) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* Get parent of port and ID of parent of port */
			port_parent = clconf_obj_get_parent(port);
			if (port_parent == NULL) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			port_parents[e - 1] = port_parent;

			port_parent_id = clconf_obj_get_id(port_parent);
			if (port_parent_id == -1) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* What type of port is this? */
			port_parent_type = clconf_obj_get_objtype(
				port_parent);
			if (port_parent_type != CL_ADAPTER &&
			    port_parent_type != CL_BLACKBOX) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			port_parent_types[e - 1] = port_parent_type;

			/* If port type is adapter, get the node and node ID */
			if (port_parent_type == CL_ADAPTER) {
				node_obj = clconf_obj_get_parent(port_parent);
				if (node_obj == NULL) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}

				node_id = (scconf_nodeid_t)
					clconf_obj_get_id(node_obj);
				if (node_id == (scconf_nodeid_t)-1) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}
				node_ids[e - 1] = node_id;
			}
		} /* Finished gathering information about the endpoints */

		/* Is this cable attached to the node in question? */
		if (nodeid == node_ids[0] || nodeid == node_ids[1]) {
			/* Yes, so remove the cable */
			clconf_err = clconf_cluster_remove_cable(clconf,
				(clconf_cable_t *)cable);
			if (clconf_err != CL_NOERROR) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* And, remove the two ports as well */
			for (e = 1; e <= 2; ++e) {
				port = ports[e - 1];
				port_parent = port_parents[e - 1];
				port_parent_type = port_parent_types[e - 1];
				switch (port_parent_type) {
				case CL_ADAPTER:
					clconf_err = clconf_adapter_remove_port(
						(clconf_adapter_t *)port_parent,
						(clconf_port_t *)port);
					if (clconf_err != CL_NOERROR) {
						rstatus = SCCONF_EUNEXPECTED;
						goto cleanup;
					}
					break;
				case CL_BLACKBOX:
					clconf_err = clconf_bb_remove_port(
						(clconf_bb_t *)port_parent,
						(clconf_port_t *)port);
					if (clconf_err != CL_NOERROR) {
						rstatus = SCCONF_EUNEXPECTED;
						goto cleanup;
					}
					break;
				default:
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				} /*lint !e788 */
			}

			/* Check the transition */
			rstatus = conf_check_transition(existing_clconf, clconf,
				messages);
			if (rstatus != SCCONF_NOERR) {
				goto cleanup;
			}

			/*
			 * Refresh currcablesi to account for the removed
			 * cable.  This means restarting at the beginning
			 * of the updated cable list to ensure that we remove
			 * all the correct cables.
			 */
			currcablesi = clconf_cluster_get_cables(clconf);
		} else {
			/*
			 * Do not remove this cable.  It is not attached to the
			 * node to be removed.  Continue iteration by getting
			 * the next cable.
			 */
			clconf_iter_advance(currcablesi);
		}
	}
cleanup:
	/* Release iterator */
	if (cablesi != (clconf_iter_t *)0)
		clconf_iter_release(cablesi);

	/* Release copy of cluster config */
	if (existing_clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)existing_clconf);

	return (rstatus);
}

/*
 * conf_get_clusternodes
 *
 * Get the nodes list from the given "clconf".
 *
 * The caller is responsible for freeing memory
 * associated with "nodeslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_clusternodes(clconf_cluster_t *clconf,
    scconf_cfg_node_t **nodeslistp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	clconf_iter_t *currnodesi, *nodesi = (clconf_iter_t *)0;
	scconf_cfg_node_t *nodeslist = (scconf_cfg_node_t *)0;
	scconf_cfg_node_t **nextp;
	clconf_obj_t *node;
	const char *name;
	const char *prop;
	scconf_nodeid_t id;
	int state;

	/* Check arguments */
	if (clconf == NULL || nodeslistp == NULL)
		return (SCCONF_EINVAL);

	/* Iterate through the list of configured nodes */
	nextp = &nodeslist;
	nodesi = currnodesi = clconf_cluster_get_nodes(clconf);
	while ((node = clconf_iter_get_current(currnodesi)) != NULL) {

		/* Allocate struct for the next node */
		*nextp = (scconf_cfg_node_t *)calloc(1,
		    sizeof (scconf_cfg_node_t));
		if (*nextp == (scconf_cfg_node_t *)0) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Add the node name */
		name = clconf_obj_get_name(node);
		if (name != NULL) {
			(*nextp)->scconf_node_nodename = strdup(name);
			if ((*nextp)->scconf_node_nodename == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Add the node ID */
		id = (scconf_nodeid_t)clconf_obj_get_id(node);
		if (id == (scconf_nodeid_t)-1) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		(*nextp)->scconf_node_nodeid = id;

		/* Set the node state */
		state = (clconf_obj_enabled(node)) ?
		    SCCONF_STATE_ENABLED : SCCONF_STATE_DISABLED;
		(*nextp)->scconf_node_nodestate = state;

		/* Add the private hostname */
		prop = clconf_obj_get_property(node,
		    PROP_NODE_PRIVATE_HOSTNAME);
		if (prop != NULL) {
			(*nextp)->scconf_node_privatehostname =
			    strdup(prop);
			if ((*nextp)->scconf_node_privatehostname == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Add the quorum vote count for this node */
		prop = clconf_obj_get_property(node, PROP_NODE_QUORUM_VOTE);
		(*nextp)->scconf_node_qvotes = (prop == NULL) ? -1 :
		    atoi(prop);

		/* Add the default quorum vote count for this node */
		prop = clconf_obj_get_property(node,
		    PROP_NODE_QUORUM_DEFAULTVOTE);
		(*nextp)->scconf_node_qdefaultvotes = (prop == NULL) ? 1 :
		    atoi(prop);

		/* Add the quorum disk reservation key for this node */
		prop = clconf_obj_get_property(node, PROP_NODE_QUORUM_RESV_KEY);
		if (prop != NULL) {
			(*nextp)->scconf_node_qreskey = strdup(prop);
			if ((*nextp)->scconf_node_qreskey == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* SC SLM addon start */
		prop = clconf_obj_get_property(
		    node, PROP_NODE_SCSLM_GLOBAL_SHARES);
		(*nextp)->scslm_global_zone_shares =
		    (prop == NULL) ? 1 : atoi(prop);
		prop = clconf_obj_get_property(
		    node, PROP_NODE_SCSLM_DEFAULT_PSET);
		(*nextp)->scslm_default_pset_min =
		    (prop == NULL) ? 1 : atoi(prop);
		/* SC SLM addon end */

		/* Add the cluster transport adapter list for this node */
		rstatus = conf_get_cltr_adapters((clconf_node_t *)node,
		    &(*nextp)->scconf_node_adapterlist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* next */
		nextp = &(*nextp)->scconf_node_next;
		clconf_iter_advance(currnodesi);
	}

cleanup:
	if (nodesi != (clconf_iter_t *)0)
		clconf_iter_release(nodesi);

	if (rstatus == SCCONF_NOERR) {
		*nodeslistp = nodeslist;
	} else {
		conf_free_clusternodes(nodeslist);
	}

	return (rstatus);
}

/*
 * conf_free_clusternodes
 *
 * Free all memory associated with an "scconf_cfg_node" structure.
 */
void
conf_free_clusternodes(scconf_cfg_node_t *nodeslist)
{
	scconf_cfg_node_t *last, *next;

	/* Check argument */
	if (nodeslist == NULL)
		return;

	/* Free memory for each node in the list */
	last = (scconf_cfg_node_t *)0;
	next = nodeslist;
	while (next != NULL) {

		/* free the last node struct */
		if (last != NULL)
			free(last);

		/* node name */
		if (next->scconf_node_nodename)
			free(next->scconf_node_nodename);

		/* private hostname */
		if (next->scconf_node_privatehostname)
			free(next->scconf_node_privatehostname);

		/* quorum disk reservation key */
		if (next->scconf_node_qreskey)
			free(next->scconf_node_qreskey);

		/* cluster transport adapter list */
		if (next->scconf_node_adapterlist)
			conf_free_cltr_adapters(
			    next->scconf_node_adapterlist);

		last = next;
		next = next->scconf_node_next;
	}
	if (last != NULL)
		free(last);
}

/*
 * conf_form_reskey
 *
 * Upon success, a disk reservation key is formed from the given
 * cluster and node ID's.  The key is copied into the supplied "key"
 * buffer.
 *
 * The "key" buffer must be large enough to hold a disk reservation string,
 * (SCCONF_RESKEY_BUFSIZ).
 *
 * The reservation key string has a format of "0xFFFFFFFFFFFFFFFF".
 * The first 4 "bytes" are formed from the clusterid;
 * the  last 4 "bytes" are formed from the nodeid.
 *
 * Note that the string is introduced by "0x" to indicate to the
 * consumer that a conversion to binary data (8 bytes)
 * is required.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
conf_form_reskey(int clusterid, scconf_nodeid_t nodeid, char *key)
{
	/* Check arguments */
	if (key == NULL)
		return (SCCONF_EINVAL);

	/* Convert from long to ASCII */
	(void) sprintf(key, "0x%8.8X%8.8X", clusterid, nodeid);

	return (SCCONF_NOERR);
}

/*
 * conf_check_node_removal
 *
 * Return SCCONF_EINUSE if the node cannot be removed safely.
 * The node cannot be referenced by any quorum device, resource group, or
 * logical device group (logical device groups include SDS (or SVM)
 * metasets).
 *
 * Physical device groups (DID disks) and cluster transport objects such as
 * cables and interface endpoints will not be checked by this function.
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- node can be safely removed
 *	SCCONF_EINUSE		- node cannot be safely removed
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_check_node_removal(clconf_cluster_t *clconf, clconf_obj_t *node,
    char **messages)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_errno_t status;
	char *nodename = NULL;
	scconf_nodeid_t nodeid;

	/* Check arguments */
	if (clconf == NULL || node == NULL)
		return (SCCONF_EINVAL);

	/* I18N houskeeping */
	if (messages != (char **)0)
		(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Get the nodeid */
	nodeid = (scconf_nodeid_t)clconf_obj_get_id(node);
	if (nodeid == (scconf_nodeid_t)-1) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Get the nodename */
	status = conf_get_nodename(clconf, nodeid, &nodename);
	if (status !=  SCCONF_NOERR || nodename == NULL) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/*
	 * Make sure that the node is not used by a resource group.
	 */
	status = conf_check_node_notinuse_byrg(nodename, nodeid, messages);

	/* If in use, set rstatus, then continue */
	if (status == SCCONF_EINUSE) {
		rstatus = SCCONF_EINUSE;

	/* Otherwise, if bad return status, return that status now */
	} else if (status != SCCONF_NOERR) {
		rstatus = status;
		goto cleanup;
	}

	/*
	 * Make sure that the node is not used by a logical device group,
	 * ignoring all physical rawdisk device services.
	 */
	status = conf_check_node_notinuse_bydg(nodename, nodeid, 0, messages);

	/* If in use, set rstatus, then continue */
	if (status == SCCONF_EINUSE) {
		rstatus = SCCONF_EINUSE;

	/* Otherwise, if bad return status, return that status now */
	} else if (status != SCCONF_NOERR) {
		rstatus = status;
		goto cleanup;
	}

	/* Make sure that the node is not in use for quorum */
	status = conf_check_node_notinuse_byquorum(clconf, nodename, messages);

	/* If in use, add possible message, set rstatus, then continue */
	if (status != SCCONF_NOERR) {
		switch (status) {
		case SCCONF_EQUORUM:
			/* Add message? */
			if (messages != (char **)0)
				scconf_addmessage(dgettext(TEXT_DOMAIN,
				    "All two-node clusters must have at "
				    "least one shared quorum device.\n"),
				    messages);

			/* If rstatus is otherwise good, set it to EQUORUM */
			if (rstatus == SCCONF_NOERR)
				rstatus = status;

			break;

		case SCCONF_EINUSE:
		default:
			rstatus = status;
			break;
		} /*lint !e788 */

		goto cleanup;
	}

cleanup:
	/* Free nodename */
	if (nodename != NULL)
		free(nodename);

	return (rstatus);
}

/*
 * scconf_free_zones()
 *     Free a block of zone pointers.
 */
void
scconf_free_zones(char **zonelist)
{
	char **zl = zonelist;
	int i = 0;

	if (!zonelist)
		return;

	while (zl[i]) {
		free(zl[i]);
		i++;
	}
	free(zonelist);
}

/*
 * scconf_get_xip_zones
 *     Get the list of exclusive IP zones on a cluster node.
 */
scconf_errno_t
scconf_get_xip_zones(char *node, char ***zonelist)
{
	char *scr_cmd = (char *)NULL;
	char zbuf[BUFSIZ];
	int cmd_len = 0;
	FILE *file_ptr;
	char **zl = (char **)NULL;
	int i = 0;
	char *sp_ptr;

	cmd_len = strlen("/usr/cluster/lib/sc/scrconf -X -N %s");
	cmd_len += strlen(node);
	if ((scr_cmd = (char *)malloc(cmd_len)) == NULL) {
		return (SCCONF_ENOMEM);
	}
	(void) sprintf(scr_cmd, "/usr/cluster/lib/sc/scrconf -X -N %s", node);
	if ((file_ptr = popen(scr_cmd, "r")) != NULL) {
		(void) memset(zbuf, 0, BUFSIZ);
		while (fgets(zbuf, BUFSIZ, file_ptr) != NULL) {
			if (*zbuf != '\0') {
				zl = (char **)realloc(zl,
				    (i + 1) * sizeof (char *));
				if (!zl) {
					return (SCCONF_ENOMEM);
				}
				zl[i] = strdup(zbuf);
				if (zl[i] == NULL)
					return (SCCONF_ENOMEM);
				sp_ptr = strchr(zl[i], '\n');
				if (sp_ptr)
					*sp_ptr = '\0';
				i++;
				(void) memset(zbuf, 0, BUFSIZ);
			}
		}
		zl = (char **)realloc(zl, (i + 1) * sizeof (char *));
		if (!zl) {
			return (SCCONF_ENOMEM);
		}
		zl[i] = (char *)NULL;

		(void) pclose(file_ptr);
	} else {
		return (SCCONF_EUNEXPECTED);
	}

	*zonelist = zl;
	free(scr_cmd);
	return (SCCONF_NOERR);
}


/* SC SLM addon start */
/*
 * scconf_set_scslm
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_set_scslm(char *nodename, int prop,
    int ivalue, char *svalue, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_errnum_t clconf_err;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_nodeid_t nodeid;
	clconf_obj_t *node;
	char str[32]; /* result of atoi, big enough  */
	char *property;
	static const int max_shares = FSS_MAXSHARES;

	/* Must be root */
	if (getuid() != 0) {
		return (SCCONF_EPERM);
	}

	/* Check arguments */
	if (prop == SCCONF_NODE_SCSLM_GLOBAL_SHARES) {
		if (ivalue <= 0 || ivalue > max_shares) {
			return (SCCONF_EINVAL);
		}
		property = PROP_NODE_SCSLM_GLOBAL_SHARES;
	} else if (prop == SCCONF_NODE_SCSLM_DEFAULT_PSET) {
		if (ivalue <= 0) {
			return (SCCONF_EINVAL);
		}
		property = PROP_NODE_SCSLM_DEFAULT_PSET;
	} else {
		return (SCCONF_EINVAL);
	}

	/*
	 * Loop until the commit succeeds, or until
	 * a fatal error is encountered.
	 */
	for (;;) {

		/* Release copies of cluster config, if they exist */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			return (rstatus);

		/* Get the nodeid */
		rstatus = conf_get_nodeid(clconf, nodename, &nodeid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Find the node */
		rstatus = conf_get_nodeobj(clconf, nodeid, &node);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		(void) snprintf(str, 32, "%d", ivalue);

		clconf_err = clconf_obj_set_property(node,
		    property, str);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}


		/* Commit */
		rstatus = conf_cluster_commit(clconf, messages);

		/* If committed okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release the cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}
/* SC SLM addon end */
