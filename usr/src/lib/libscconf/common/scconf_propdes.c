/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)scconf_propdes.c	1.15	08/05/20 SMI"

/*
 * libscconf functions for transport property descriptions
 */

#include "scconf_private.h"

/*
 * conf_get_propdeslist
 *
 * Get the list of all properties for a given "clplobj_type" (e.g.,
 * CL_ADAPTER or CL_BLACKBOX) and "clplobj_name" (e.g., "hme" or "switch").
 *
 * Upon success, a pointer to the property list is returned in
 * "propdeslisp".  If there is no property description list, the value
 * pointed to by "propdeslistp" is set to NULL.
 *
 * The caller is responsible for freeing memory associated
 * with the "propdeslistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root or permission denied
 *	SCCONF_EINVAL		- invalid argument (or item(s) in clconfig)
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_propdeslist(clconf_objtype_t clplobj_type, const char *clplobj_name,
    scconf_cltr_propdes_t **propdeslistp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	DIR *dirp = (DIR *)0;
	struct dirent *direntp;
	struct stat sbuf;
	char clpldir[MAXPATHLEN + 1];
	char path[MAXPATHLEN + 1];
	char *basedir;
	char *pathptr, *ptr;
	clconf_iter_t *currpropsi, *propsi = (clconf_iter_t *)0;
	clconf_obj_t *clplobj = (clconf_obj_t *)0;
	clconf_obj_t *propobj;
	clconf_propdesc_t clpl_propdesc;
	scconf_cltr_propdes_t *propdes = (scconf_cltr_propdes_t *)0;
	scconf_cltr_propdes_t **propdesp;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (clplobj_type == CL_UNKNOWN_OBJ || clplobj_name == NULL)
		return (SCCONF_EINVAL);

	/* Initialize clpl */
	clpl_refresh();

	/* If we are not able to access our clpldir, check SCCONF_BASEDIR */
	(void) strcpy(clpldir, SCCONF_CLPL_DIR);
	if (access(clpldir, F_OK) < 0) {
		if (errno != ENOENT) /*lint !e746 */
			return (SCCONF_EUNEXPECTED);
		if ((basedir = getenv("SCCONF_BASEDIR")) == NULL)
			return (SCCONF_EUNEXPECTED);
		(void) sprintf(clpldir, "%s/%s", basedir, SCCONF_CLPL_DIR);
		if (access(clpldir, F_OK) < 0)
			return (SCCONF_EUNEXPECTED);
	}

	/* Initialize our buffer */
	(void) snprintf(path, sizeof (path), "%s/", clpldir);
	pathptr = &path[strlen(path)];

	/* Init pointer to props list */
	propdesp = &propdes;

	/* Read all clpl files from the clpl directory */
	if ((dirp = opendir(clpldir)) == NULL)
		return (SCCONF_EUNEXPECTED);
	while ((direntp = readdir(dirp)) != NULL) {

		/* If reclen not set, skip */
		if (!direntp->d_reclen)
			continue;

		/* Construct the full name of the file */
		(void) strncpy(pathptr, direntp->d_name, direntp->d_reclen);

		/* If it does not end in the expected suffix, skip it */
		ptr = strrchr(pathptr, '.');
		if ((ptr == NULL) || (strcmp(ptr, SCCONF_CLPL_DOTSUFFIX) != 0))
			continue;

		/* Make sure that it is a regular file and we can read it */
		if (stat(path, &sbuf) < 0 || !S_ISREG(sbuf.st_mode)) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		if (access(path, R_OK) < 0) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Read in the file */
		clpl_read(path);
	}

	/* Get the object */
	clplobj = clpl_get_object(clplobj_type, clplobj_name);
	if (clplobj == (clconf_obj_t *)0)
		goto cleanup;

	/* Get the properties, and copy them to scconf format */
	propsi = currpropsi = clpl_obj_get_properties(clplobj);
	while ((propobj = clconf_iter_get_current(currpropsi)) != NULL) {

		/* Get the property description */
		clpl_propdesc = clpl_prop_get_description(propobj);
		*propdesp = (scconf_cltr_propdes_t *)calloc(1,
		    sizeof (scconf_cltr_propdes_t));
		if (*propdesp == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Copy the property name */
		if (clpl_propdesc.name != NULL) {
			(*propdesp)->scconf_cltr_propdes_name =
			    strdup(clpl_propdesc.name);
			if ((*propdesp)->scconf_cltr_propdes_name == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Copy the property type */
		(*propdesp)->scconf_cltr_propdes_type = clpl_propdesc.type;

		/* Copy the description of the property */
		/* XXX - DOES NOT EXIST YET IN CLPL!! */

		/* Copy the min length for string */
		(*propdesp)->scconf_cltr_propdes_min = clpl_propdesc.min;

		/* Copy the max length for string */
		(*propdesp)->scconf_cltr_propdes_max = clpl_propdesc.max;

		/* Copy the default value */
		if (clpl_propdesc.defaultv != NULL) {
			(*propdesp)->scconf_cltr_propdes_default =
			    strdup(clpl_propdesc.defaultv);
			if ((*propdesp)->scconf_cltr_propdes_default == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Copy the required flag */
		(*propdesp)->scconf_cltr_propdes_required =
		    clpl_propdesc.required;

		/* Copy the enum list of possible values */
		if (clpl_propdesc.enumlist != NULL) {
			(*propdesp)->scconf_cltr_propdes_enumlist =
			    strdup(clpl_propdesc.enumlist);
			if ((*propdesp)->scconf_cltr_propdes_enumlist
			    == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* Next pointer */
		propdesp = &(*propdesp)->scconf_cltr_propdes_next;

		/* Advance the iterator */
		clconf_iter_advance(currpropsi);
	}

cleanup:
	/* Close directory */
	if (dirp != NULL)
		(void) closedir(dirp);

	/* Release iterator */
	if (propsi != (clconf_iter_t *)0)
		clconf_iter_release(propsi);

	/* If not successful, free the list */
	if (rstatus != SCCONF_NOERR) {
		if (propdes != NULL) {
			scconf_free_propdeslist(propdes);
			propdes = (scconf_cltr_propdes_t *)0;
		}
	}

	/* Return the list */
	*propdeslistp = propdes;

	return (rstatus);
}

/*
 * conf_check_property
 *
 * Check the given property "name" and "value" against
 * the given propery description list ("propdeslist").
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- bad propterty
 */
scconf_errno_t
conf_check_property(char *name, char *value,
    scconf_cltr_propdes_t *propdeslist)
{
	scconf_cltr_propdes_t *propdes;
	int numvalue = 0;
	int isnumvalue = 0;
	char *ptr, *laststrtok;
	size_t len;
	char *s;

	/* Check args */
	if (name == NULL || propdeslist == NULL)
		return (SCCONF_EINVAL);

	/* If the value is numeric, set isnumvalue */
	if (value != NULL) {
		isnumvalue = 1;
		for (s = value;  *s;  ++s) {
			if (!isdigit(*s)) {
				isnumvalue = 0;
				break;
			}
		}
	}

	/* If isnumvalue, set numvalue */
	if (isnumvalue) {
		errno = 0;
		numvalue = (value != NULL) ? atoi(value) : 0;

		/* numeric, but not integer? */
		if (errno != 0)
			isnumvalue = 0;
	}

	/* Search the propdes list for a matching property which passes */
	for (propdes = propdeslist;  propdes;
	    propdes = propdes->scconf_cltr_propdes_next) {

		/* Matching property name? */
		if (strcmp(propdes->scconf_cltr_propdes_name, name) != 0)
			continue;

		/* If required, is value also given? */
		if (propdes->scconf_cltr_propdes_required && value == NULL)
			continue;

		/* Check value based on expected type */
		switch (propdes->scconf_cltr_propdes_type) {
		case CL_STRING:
			/* Get the string length */
			len = (value != NULL) ? strlen(value) : 0;

			/* If there is a min, make sure value is within range */
			if (propdes->scconf_cltr_propdes_min != 0 &&
			    len < (size_t)propdes->scconf_cltr_propdes_min)
				continue;

			/* If there is a max, make sure value is within range */
			if (propdes->scconf_cltr_propdes_max != 0 &&
			    len > (size_t)propdes->scconf_cltr_propdes_max)
				continue;

			break;

		case CL_ENUM:
			/* If the enumlist it empty, accept anything */
			if (propdes->scconf_cltr_propdes_enumlist == NULL)
				break;

			/* Otherwise, search the list for a match */
			ptr = propdes->scconf_cltr_propdes_enumlist;
			ptr = strtok_r(ptr, ":", &laststrtok);
			while (ptr != NULL && value != NULL) {
				if (strcmp(ptr, value) == 0)
					break;
				ptr = strtok_r(NULL, ":", &laststrtok);
			}
			if (ptr == NULL)
				continue;

			break;

		case CL_INT:
			/* Make sure that the value is numeric */
			if (!isnumvalue)
				continue;

			/* And, that it is within range */
			if (numvalue < propdes->scconf_cltr_propdes_min ||
			    numvalue > propdes->scconf_cltr_propdes_max)
				continue;

			break;

		case CL_BOOLEAN:
			/* Make sure that the value is numveric */
			if (!isnumvalue)
				continue;

			/* And, that it is either 0 or 1 */
			if (numvalue != 0 && numvalue != 1)
				continue;

			break;

		default:
			/* Reject the desctiption, if unkown type */
			continue;
		}

		/* If we got this far, we found a good match */
		return (SCCONF_NOERR);
	}

	return (SCCONF_EINVAL);
}

/*
 * scconf_free_propdeslist
 *
 * Free all memory associated with the given
 * property description "propdeslist".
 */
void
scconf_free_propdeslist(scconf_cltr_propdes_t *propdeslist)
{
	register scconf_cltr_propdes_t **listp;
	register scconf_cltr_propdes_t *last;

	/* check arg */
	if (propdeslist == (scconf_cltr_propdes_t *)0)
		return;

	/* free memory for each member of the list */
	listp = &propdeslist;
	while (*listp) {
		if ((*listp)->scconf_cltr_propdes_name)
			free((*listp)->scconf_cltr_propdes_name);
		if ((*listp)->scconf_cltr_propdes_description)
			free((*listp)->scconf_cltr_propdes_description);
		if ((*listp)->scconf_cltr_propdes_default)
			free((*listp)->scconf_cltr_propdes_default);
		if ((*listp)->scconf_cltr_propdes_enumlist)
			free((*listp)->scconf_cltr_propdes_enumlist);

		last = *listp;
		*listp = (*listp)->scconf_cltr_propdes_next;
		free(last);
	}
}

/*
 * conf_propdes_to_prop()
 *
 * Given a property description "propdes", turns it into a property "prop"
 * by copying the default value of "propdes" into the value of "prop".
 *
 * The memory for "prop", and the strings that are referenced from there,
 * are malloc'ed, and needs to be freed by the caller.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid arguments
 *	SCCONF_ENOMEM		- not enough memory
 */
static scconf_errno_t
conf_propdes_to_prop(scconf_cfg_prop_t **prop,
    scconf_cltr_propdes_t *propdes)
{
	if (prop == NULL || propdes == NULL ||
	    propdes->scconf_cltr_propdes_name == NULL ||
	    propdes->scconf_cltr_propdes_default == NULL) {
		return (SCCONF_EINVAL);
	}

	/* alloc memory for structure */
	*prop = (scconf_cfg_prop_t *)calloc(1, sizeof (scconf_cfg_prop_t));
	if (*prop == NULL)
		return (SCCONF_ENOMEM);

	/* dup the key */
	(*prop)->scconf_prop_key = strdup(propdes->scconf_cltr_propdes_name);
	if ((*prop)->scconf_prop_key == NULL) {
		free (*prop);
		*prop = NULL;
		return (SCCONF_ENOMEM);
	}

	/* dup the default into the value */
	(*prop)->scconf_prop_value =
		strdup(propdes->scconf_cltr_propdes_default);
	if ((*prop)->scconf_prop_value == NULL) {
		free ((*prop)->scconf_prop_key);
		free (*prop);
		*prop = NULL;
		return (SCCONF_ENOMEM);
	}

	return (SCCONF_NOERR);
}

/*
 * conf_use_adapter_default_properties()
 *
 * Walks through the "propdeslist" list, and for every property that is
 * is not required, but for which a default is supplied, inserts the default
 * value into the property list "proplist".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid arguments
 */
scconf_errno_t
conf_use_adapter_default_properties(scconf_cltr_propdes_t *propdeslist,
    scconf_cfg_prop_t **proplist)
{
	scconf_cltr_propdes_t	*propdes;

	if (proplist == NULL) {
		return (SCCONF_EINVAL);
	}
	for (propdes = propdeslist;  propdes;
	    propdes = propdes->scconf_cltr_propdes_next) {
		scconf_errno_t		retval;
		scconf_cfg_prop_t	*prop;
		scconf_cfg_prop_t	**proplistp;

		/*
		 * Convert property description to property.
		 * Memory is only allocated if there is no error.
		 */
		if (propdes->scconf_cltr_propdes_required == 1 ||
		    propdes->scconf_cltr_propdes_name == NULL)
			continue;

		/*
		 * Don't pick properties with "_v1" which is for
		 * previous version. When the range for adapter
		 * heartbeat parameters are changed and add a new
		 * version, it needs to add checking the new
		 * version here as well.
		 */
		if (strstr(propdes->scconf_cltr_propdes_name, "_v1"))
			continue;

		/*
		 * Skip vlanid, the default for vlanid should not be used.
		 */
		if (strstr(propdes->scconf_cltr_propdes_name, "vlan"))
			continue;

		retval = conf_propdes_to_prop(&prop, propdes);
		if (retval != SCCONF_NOERR)
			return (retval);

		/* check if property is already in the list */
		for (proplistp = proplist; *proplistp;
		    proplistp = &(*proplistp)->scconf_prop_next) {
			if ((*proplistp)->scconf_prop_key == NULL)
				continue;
			if (strcmp((*proplistp)->scconf_prop_key,
			    prop->scconf_prop_key) == 0)
				break;
		}

		if (*proplistp == NULL) {
			/* not in property list, enter it at head */
			prop->scconf_prop_next = *proplist;
			*proplist = prop;
		} else {
			/* is already in list, free  */
			free(prop);
		}
	}
	return (SCCONF_NOERR);
}
