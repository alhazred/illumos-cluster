/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scconf_cluster.c	1.21	09/01/16 SMI"

/*
 * libscconf cluster config functions
 */

#include "scconf_private.h"
#include <limits.h>

#define	HEX_LEADER		"0x"		/* introduces cluster id */

static scconf_errno_t scconf_properties_to_udp(char **timeout,
    char *properties);
static int conf_check_udp_timeout(char *udp_timeout);

/*
 * scconf_change_clustername
 *
 * Change the "clustername".
 *
 * If "messages" is not NULL, it should either point to a NULL or
 * a NULL-terminated character string.  When "messages" is not NULL,
 * messages may be passed back to the caller by either setting "*messages"
 * to a new messages buffer or by enlarging and concatenating to the
 * given buffer.  Each message is terminated with a NEWLINE, and the
 * complete message buffer is terminated with a NULL.  It is the callers
 * responsibiltiy to free the message buffer memory.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EOVERFLOW	- message buffer overflow
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_change_clustername(char *clustername, char **messages)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (clustername == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Loop until the commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Change the cluster name */
		clconf_obj_set_name((clconf_obj_t *)clconf, clustername);

		/* attempt commit */
		rstatus = conf_cluster_commit(clconf, messages);

		/* if commit or check returned okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			goto cleanup;
		}
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * conf_get_clusterid
 *
 * Upon success, return the cluster ID in "clusteridp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- cluster ID does not exist
 *	SCCONF_EBADVALUE	- bad cluster ID
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
conf_get_clusterid(clconf_cluster_t *clconf, int *clusteridp)
{
	register char *s;
	const char *idstring;
	int id;

	/* Check arguments */
	if (clconf == NULL || clusteridp == NULL)
		return (SCCONF_EINVAL);

	/* Get the cluster ID string */
	idstring = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_ID);
	if (idstring == NULL)
		return (SCCONF_ENOEXIST);

	/* The idstring must be an 8 digit hex number (0x7FFFFFFF) */
	if (strlen(idstring) != 10 ||
	    strncmp(HEX_LEADER, idstring, (sizeof (HEX_LEADER) - 1)) != 0)
		return (SCCONF_EBADVALUE);

	/* make sure it is not negative */
	s = (char *)&idstring[sizeof (HEX_LEADER) - 1];
	if (isalpha(*s) || *s == '9' || *s == 8)
		return (SCCONF_EBADVALUE);

	/* make sure it is hex */
	while (*s)
		if (!isxdigit(*s++))
			return (SCCONF_EBADVALUE);

	/* Convert from ASCII to long */
	id = (int)strtol(idstring, (char **)0, 16);
	if (!id)
		return (SCCONF_EBADVALUE);

	/* Got a good cluster id */
	*clusteridp = id;

	return (SCCONF_NOERR);
}

/*
 * conf_set_clusterid
 *
 * Upon success, update the given "clconf" with a new cluster ID.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_set_clusterid(clconf_cluster_t *clconf)
{
	clconf_errnum_t clconf_err;
	char clusterid[SCCONF_CLUSTERID_BUFSIZ];

	/* Check arguments */
	if (clconf == NULL)
		return (SCCONF_EINVAL);

	/* Form cluster ID */
	if (scconf_form_clusterid(clusterid) != SCCONF_NOERR)
		return (SCCONF_EUNEXPECTED);

	/* Set the cluster ID property */
	clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_ID, clusterid);
	if (clconf_err != CL_NOERROR) {
		switch (clconf_err) {
		case CL_BADNAME:
		default:
			return (SCCONF_EUNEXPECTED);
		} /*lint !e788 */
	}

	return (SCCONF_NOERR);
}

/*
 * scconf_form_clusterid
 *
 * Upon success, a new cluster ID string placed in "clusterid".
 * The "clusterid" buffer must be large enough to hold a cluster ID string,
 * (SCCONF_CLUSTERID_BUFSIZ).
 *
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_form_clusterid(char *clusterid)
{
	int id;

	/* Check arguments */
	if (clusterid == NULL)
		return (SCCONF_EINVAL);

	/* The cluster id is set to the current time */
	id = (int)time((time_t *)0);

	/* Convert from long to ASCII */
	(void) sprintf(clusterid, "0x%8.8X", id);

	return (SCCONF_NOERR);
}

/*
 * scconf_get_clusterconfig
 *
 * Get a snapshot of the current cluster config.
 *
 * This configuration data, as found in "scconf_cfg_cluster_t", is limited
 * to selected data stored in the infrastructure CCR table.
 *
 * The caller is responsible for freeing memory associated
 * with the "clconfigp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR            - success
 *	SCCONF_EPERM            - not root
 *	SCCONF_ENOCLUSTER       - there is no cluster
 *	SCCONF_ENOMEM           - not enough memory
 *	SCCONF_EINVAL           - invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_clusterconfig(scconf_cfg_cluster_t **clconfigp)
{
	return (scconf_handle_get_virtualclusterconfig(NULL, NULL, clconfigp));
}

/*
 * scconf_get_virtualclusterconfig
 * Same as scconf_get_clusterconfig except that it takes a cluster name as
 * argument, and operates in that cluster's context
 */
scconf_errno_t
scconf_get_virtualclusterconfig(
    const char *clustername,  scconf_cfg_cluster_t **clconfigp)
{
	return (scconf_handle_get_virtualclusterconfig(clustername, NULL,
	    clconfigp));
}


/*
 * scconf_handle_get_clusterconfig
 *
 * Get a snapshot of the cluster config from the given "handle".  The
 * handle may be NULL.  The caller is responsible for releasing the handle.
 *
 * This configuration data, as found in "scconf_cfg_cluster_t", is limited
 * to selected data stored in the infrastructure CCR table.
 *
 * The caller is responsible for freeing memory associated
 * with the "clconfigp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR            - success
 *	SCCONF_EPERM            - not root
 *	SCCONF_ENOCLUSTER       - there is no cluster
 *	SCCONF_ENOMEM           - not enough memory
 *	SCCONF_EINVAL           - invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_handle_get_clusterconfig(scconf_cltr_handle_t handle,
    scconf_cfg_cluster_t **clconfigp)
{
	return (scconf_handle_get_virtualclusterconfig(NULL, handle,
	    clconfigp));
}

/*
 * scconf_handle_get_virtualclusterconfig
 * Same as scconf_handle_get_clusterconfig except that it takes a cluster
 * name as argument, and operates in that cluster's context
 */
scconf_errno_t
scconf_handle_get_virtualclusterconfig(const char *clustername,
    scconf_cltr_handle_t handle, scconf_cfg_cluster_t **clconfigp)
{
	scconf_errno_t rstatus;
	clconf_cluster_t *clconf = (clconf_cluster_t *)handle;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (clconfigp == NULL)
		return (SCCONF_EINVAL);

	/* If no handle given, get a copy of the current clconf */
	if (handle == NULL) {

		/* Make sure libclconf is initialized */
		rstatus = scconf_cltr_openhandle(NULL);
		if (rstatus != SCCONF_NOERR)
			return (rstatus);

		/* Get the clconf */
		if (clustername == NULL) {
			clconf = clconf_cluster_get_current();
		} else {
#if SOL_VERSION >= __s10
			clconf = clconf_cluster_get_vc_current(clustername);
#else
			clconf = clconf_cluster_get_current();
#endif	// we should check that call is made for global cluster itself
		}
		if (clconf == NULL)
			return (SCCONF_ENOCLUSTER);
	}

	/* Create and fill out clconfig */
	rstatus = conf_get_clusterconfig(clconf, clconfigp);

	/* If no handle given, release the clconf */
	if (handle == NULL)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * cconf_get_clusterconfig
 *
 * Get a snapshot of the current cluster config, using the given "clconf".
 *
 * The caller is responsible for freeing memory associated
 * with the "clconfigp".
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_ENOMEM           - not enough memory
 *      SCCONF_EINVAL           - invalid argument
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
conf_get_clusterconfig(clconf_cluster_t *clconf,
    scconf_cfg_cluster_t **clconfigp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_cfg_cluster_t *clconfig = (scconf_cfg_cluster_t *)0;
	const char *name;
	const char *prop;

	/* Check arguments */
	if (clconf == NULL || clconfigp == NULL)
		return (SCCONF_EINVAL);

	/* Create clconfig */
	clconfig = (scconf_cfg_cluster_t *)calloc(1,
	    sizeof (scconf_cfg_cluster_t));
	if (clconfig == (scconf_cfg_cluster_t *)0) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Add the cluster name */
	name = clconf_obj_get_name((clconf_obj_t *)clconf);
	if (name != NULL) {
		clconfig->scconf_cluster_clustername = strdup(name);
		if (clconfig->scconf_cluster_clustername == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the cluster id */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf, PROP_CLUSTER_ID);
	if (prop != NULL) {
		clconfig->scconf_cluster_clusterid = strdup(prop);
		if (clconfig->scconf_cluster_clusterid == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the installmode */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_INSTALLMODE);
	if (prop == NULL)
		prop = "disabled";
	clconfig->scconf_cluster_installmode = strdup(prop);
	if (clconfig->scconf_cluster_installmode == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Add the private netaddr to the cluster */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_NETADDR);
	if (prop != NULL) {
		clconfig->scconf_cluster_privnetaddr = strdup(prop);
		if (clconfig->scconf_cluster_privnetaddr == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the private netmask to the cluster */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_NETMASK);
	if (prop != NULL) {
		clconfig->scconf_cluster_privnetmask = strdup(prop);
		if (clconfig->scconf_cluster_privnetmask == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the private subnetmask to the cluster */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_SUBNETMASK);
	if (prop != NULL) {
		clconfig->scconf_cluster_privsubnetmask = strdup(prop);
		if (clconfig->scconf_cluster_privsubnetmask == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the private user net number to the cluster */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_USER_NET_NUM);
	if (prop != NULL) {
		clconfig->scconf_cluster_privusernetnum = strdup(prop);
		if (clconfig->scconf_cluster_privusernetnum == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the private user netmask  to the cluster  */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_USER_NETMASK);
	if (prop != NULL) {
		clconfig->scconf_cluster_privusernetmask = strdup(prop);
		if (clconfig->scconf_cluster_privusernetmask == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the maximum number of nodes to the cluster */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_MAXNODES);
	if (prop != NULL) {
		clconfig->scconf_cluster_maxnodes = strdup(prop);
		if (clconfig->scconf_cluster_maxnodes == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the maximum number of private networks to the cluster */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_MAXPRIVATENETS);
	if (prop != NULL) {
		clconfig->scconf_cluster_maxprivnets = strdup(prop);
		if (clconfig->scconf_cluster_maxprivnets == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the secure authtype */
	rstatus = conf_get_secure_authtype(clconf,
	    &clconfig->scconf_cluster_authtype);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Add the secure authlist */
	rstatus = conf_get_secure_joinlist((clconf_obj_t *)clconf,
	    &clconfig->scconf_cluster_authlist);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Add the default heart beat */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_HB_TIMEOUT);
	if (prop != NULL) {
		clconfig->scconf_cluster_hb_timeout = strdup(prop);
		if (clconfig->scconf_cluster_hb_timeout == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the default heart beat */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_HB_QUANTUM);
	if (prop != NULL) {
		clconfig->scconf_cluster_hb_quantum = strdup(prop);
		if (clconfig->scconf_cluster_hb_quantum == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the default UDP session timeout beat */

	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_UDP_TIMEOUT);
	if (prop != NULL) {
		clconfig->scconf_cluster_udp_timeout = strdup(prop);
		if (clconfig->scconf_cluster_udp_timeout == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

#ifdef WEAK_MEMBERSHIP
	/* Add the multiple partitions */
	prop = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_MULTI_PARTITIONS);
	if (prop != NULL) {
		clconfig->scconf_cluster_multi_partitions = strdup(prop);
		if (clconfig->scconf_cluster_multi_partitions == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	} else {
		clconfig->scconf_cluster_multi_partitions =
				strdup(SCCONF_MULTI_PARTITIONS_DFLT);
		if (clconfig->scconf_cluster_multi_partitions == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}

	/* Add the ping targets */
	rstatus = scconf_get_ping_targets(clconf,
	    &clconfig->scconf_cluster_ping_targets);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;
#endif // WEAK_MEMBERSHIP

	/* Add the nodes list */
	rstatus = conf_get_clusternodes(clconf,
	    &clconfig->scconf_cluster_nodelist);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Add the quorum devices */
	rstatus = conf_get_qdevs(clconf,
	    &clconfig->scconf_cluster_qdevlist);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Add the cluster transport cpoint list */
	rstatus = conf_get_cltr_cpoints(clconf,
	    &clconfig->scconf_cluster_cpointlist);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Add the cluster transport cables list */
	rstatus = conf_get_cltr_cables(clconf,
	    &clconfig->scconf_cluster_cablelist);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

cleanup:
	if (rstatus == SCCONF_NOERR) {
		*clconfigp = clconfig;
	} else {
		scconf_free_clusterconfig(clconfig);
	}

	return (rstatus);
}

/*
 * scconf_free_clusterconfig
 *
 * Free all memory associated with an "scconf_cfg_cluster" structure.
 */
void
scconf_free_clusterconfig(scconf_cfg_cluster_t *clconfig)
{
	/* Check argument */
	if (clconfig == NULL)
		return;

	/* cluster name */
	if (clconfig->scconf_cluster_clustername)
		free(clconfig->scconf_cluster_clustername);

	/* cluster id */
	if (clconfig->scconf_cluster_clusterid)
		free(clconfig->scconf_cluster_clusterid);

	/* installmode */
	if (clconfig->scconf_cluster_installmode)
		free(clconfig->scconf_cluster_installmode);

	/* private netaddr */
	if (clconfig->scconf_cluster_privnetaddr)
		free(clconfig->scconf_cluster_privnetaddr);

	/* private netmask */
	if (clconfig->scconf_cluster_privnetmask)
		free(clconfig->scconf_cluster_privnetmask);

	/* private subnetmask */
	if (clconfig->scconf_cluster_privsubnetmask)
		free(clconfig->scconf_cluster_privsubnetmask);

	/* private user net number */
	if (clconfig->scconf_cluster_privusernetnum)
		free(clconfig->scconf_cluster_privusernetnum);

	/* private user netmask */
	if (clconfig->scconf_cluster_privusernetmask)
		free(clconfig->scconf_cluster_privusernetmask);

	/* maximum nodes */
	if (clconfig->scconf_cluster_maxnodes)
		free(clconfig->scconf_cluster_maxnodes);

	/* maximum private nets */
	if (clconfig->scconf_cluster_maxprivnets)
		free(clconfig->scconf_cluster_maxprivnets);

	/* auth list */
	if (clconfig->scconf_cluster_authlist)
		scconf_free_namelist(clconfig->scconf_cluster_authlist);

	/* nodes list */
	if (clconfig->scconf_cluster_nodelist)
		conf_free_clusternodes(clconfig->scconf_cluster_nodelist);

	/* quorum devices */
	if (clconfig->scconf_cluster_qdevlist)
		conf_free_qdevs(clconfig->scconf_cluster_qdevlist);

	/* cluster transport cpoint list */
	if (clconfig->scconf_cluster_cpointlist)
		conf_free_cltr_cpoints(clconfig->scconf_cluster_cpointlist);

	/* cluster transport cables list */
	if (clconfig->scconf_cluster_cablelist)
		conf_free_cltr_cables(clconfig->scconf_cluster_cablelist);

	/* heart beat */
	if (clconfig->scconf_cluster_hb_timeout)
		free(clconfig->scconf_cluster_hb_timeout);
	if (clconfig->scconf_cluster_hb_quantum)
		free(clconfig->scconf_cluster_hb_quantum);

	/* UDP timeout */
	if (clconfig->scconf_cluster_udp_timeout)
		free(clconfig->scconf_cluster_udp_timeout);

	/* cluster netmask */
	if (clconfig->scconf_cluster_clusternetmask)
		free(clconfig->scconf_cluster_clusternetmask);

#ifdef WEAK_MEMBERSHIP
	/* multiple partitions */
	if (clconfig->scconf_cluster_multi_partitions)
		free(clconfig->scconf_cluster_multi_partitions);

	/* ping targets */
	if (clconfig->scconf_cluster_ping_targets)
		scconf_free_namelist(
			clconfig->scconf_cluster_ping_targets);
#endif // WEAK_MEMBERSHIP

	/* clconfig itself */
	free(clconfig);
}

/*
 * conf_get_maxprivnets
 *
 * Upon success, return the maxprivnets in "maxp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EBADVALUE	- bad maxprivnet value
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
conf_get_maxprivnets(clconf_cluster_t *clconf, int *maxp)
{
	const char *smaxprivnets;
	int maxprivnets;

	/* Check arguments */
	if (clconf == NULL || maxp == NULL)
		return (SCCONF_EINVAL);

	/* Get the cluster ID string */
	smaxprivnets = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_MAXPRIVATENETS);
	if (smaxprivnets == NULL) {
		*maxp = SCCONF_DEFAULT_MAXPRIVNETS;
		return (SCCONF_NOERR);
	}

	/* Convert from ASCII to long */
	maxprivnets = (int)strtol(smaxprivnets, (char **)0, 16);
	if (!maxprivnets)
		return (SCCONF_EBADVALUE);

	*maxp = maxprivnets;

	return (SCCONF_NOERR);
}

/*
 * conf_get_maxnodes
 *
 * Upon success, return the maxnodes in "maxp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EBADVALUE	- bad maxnodes value
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
conf_get_maxnodes(clconf_cluster_t *clconf, int *maxp)
{
	const char *smaxnodes;
	int maxnodes;

	/* Check arguments */
	if (clconf == NULL || maxp == NULL)
		return (SCCONF_EINVAL);

	/* Get the cluster ID string */
	smaxnodes = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_MAXNODES);
	if (smaxnodes == NULL) {
		*maxp = SCCONF_DEFAULT_MAXNODES;
		return (SCCONF_NOERR);
	}

	/* Convert from ASCII to long */
	maxnodes = (int)strtol(smaxnodes, (char **)0, 16);
	if (!maxnodes)
		return (SCCONF_EBADVALUE);

	*maxp = maxnodes;

	return (SCCONF_NOERR);
}

/*
 * scconf_change_udp_timeout
 *
 * Change or set the default UDP session timeout settings for
 * the cluster.
 *
 */
scconf_errno_t
scconf_change_udp_timeout(char *subopts, char **messages)
{
	scconf_errno_t scconferr = SCCONF_NOERR;
	clconf_errnum_t clconf_err = CL_NOERROR;
	char *udp_timeout = (char *)0;
	scconf_cltr_handle_t handle = (scconf_cltr_handle_t *)0;

	/* Check arguments */
	if (subopts == NULL) {
		return (SCCONF_EINVAL);
	}

	scconferr = scconf_properties_to_udp(&udp_timeout, subopts);
	if (scconferr != SCCONF_NOERR) {
		goto cleanup;
	}

	if (!udp_timeout) {
		return (SCCONF_TM_EBADOPTS);
	}

	scconferr = conf_check_udp_timeout(udp_timeout);
	if (scconferr != SCCONF_NOERR) {
		goto cleanup;
	}

	/*
	 * If we generate a handle here, loop until commit succeeds,
	 * or until a fatal error is encountered.
	 */
	for (;;) {
		/* Release old handle, if necessary */
		if (handle != (scconf_cltr_handle_t)0)
			scconf_cltr_releasehandle(handle);
		handle = (scconf_cltr_handle_t)0;

		/* Get handle */
		(void) scconf_cltr_openhandle(&handle);


		/* Change the default UDP session timeout setting */
		clconf_err = clconf_obj_set_property(handle,
		    PROP_CLUSTER_UDP_TIMEOUT, udp_timeout);
		if (clconf_err != CL_NOERROR) {
			goto cleanup;
		}

		if (handle != (scconf_cltr_handle_t)0) {
			/* update */
			scconferr = scconf_cltr_updatehandle(handle, messages);

			/* if update returned okay, we are done */
			if (scconferr == SCCONF_NOERR) {
				break;
				/* if outdated handle, continue to retry */
			} else if (scconferr != SCCONF_ESTALE) {
				goto cleanup;
			}
		} else {
			break;
		}
	}

cleanup:
	/* Release the handle */
	if (handle != (scconf_cltr_handle_t)0)
		scconf_cltr_releasehandle(handle);
	if (udp_timeout != (char *)0)
		free(udp_timeout);

	return (scconferr);
}

/*
 * conf_check_udp_timeout
 *
 * Verifies whether the user provided value is legal.
 *
 */

static int
conf_check_udp_timeout(char *udp_timeout)
{
	char *c;

	/* check if the value is numeric */

	for (c = udp_timeout; *c;  c++) {
		if (!isdigit(*c)) {
			return (SCCONF_EINVAL);
		}
	}

	/* Check if the value is within the range */

	if ((atoi(udp_timeout) < 50) || (atoi(udp_timeout) > INT_MAX)) {
		return (SCCONF_ERANGE);
	}

	return (SCCONF_NOERR);
}

/*
 * scconf_properties_to_udp
 *
 * Get the udp_session_timeout beat values from the "properties". Upon
 * finished, the timeout could have the following values and what it
 * means to the caller:
 *
 *      "default"       Needs to get the default udp_session_timeout
 *      '\0'            No input from the user.
 *      Some value specified by user
 *
 * Possible return values:
 *
 *      SCCONF_NOERR            - success
 *      SCCONF_ENOMEM           - not enough memory
 */

static scconf_errno_t
scconf_properties_to_udp(char **timeout, char *properties)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *prop, *props = NULL;
	char *p, *last;
	char udp_timeout[SCCONF_HB_BUFSIZ];
	char *value = NULL;

	/* Check arguments */

	if (properties == NULL || *properties == '\0') {
		*timeout = NULL;
		return (SCCONF_NOERR);
	}

	/* Create the property strings */

	(void) strcpy(udp_timeout, PROP_CLUSTER_UDP_TIMEOUT);

	/* Make a copy of the properties */

	if ((props = strdup(properties)) == NULL) {
		return (SCCONF_ENOMEM);
	}

	if ((prop = strtok_r(props, ",", &last)) == NULL) {
		rstatus = SCCONF_TM_EBADOPTS;
		goto cleanup;
	}

	/* Get the properties values */

	while (prop != NULL) {
		if ((p = strchr(prop, '=')) == NULL) {
			rstatus = SCCONF_TM_EBADOPTS;
			goto cleanup;
		}

		/* if there is a value, dup it */

		*p++ = '\0';
		value = (*p != '\0') ? strdup(p) : strdup("default");

		if (value == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Set the values */

		if (strcmp(udp_timeout, prop) == 0) {
			if ((*timeout = strdup(value)) == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* find the next property */

		prop = strtok_r(NULL, ",", &last);
	}
cleanup:
	/* Release props */

	if (props != NULL) {
		free(props);
	}

	if (value != NULL) {
		free(value);
	}

	return (rstatus);
}
