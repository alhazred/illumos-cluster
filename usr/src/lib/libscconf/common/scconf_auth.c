/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)scconf_auth.c	1.12	08/05/20 SMI"

/*
 * libscconf logical host config functions
 */

#include "scconf_private.h"

/*
 * scconf_set_secure_authtype
 *
 * Set the admin security authentication type.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- unexpected or internal error
 */
scconf_errno_t
scconf_set_secure_authtype(scconf_authtype_t authtype)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_errnum_t clconf_err;
	char *sauthtype;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check argument */
	switch (authtype) {
	case SCCONF_AUTH_SYS:
		sauthtype = "sys";
		break;

	case SCCONF_AUTH_DES:
		sauthtype = "des";
		break;

	default:
		return (SCCONF_EINVAL);
	}

	/*
	 * Loop until commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Set the property */
		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_AUTHTYPE, sauthtype);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returns okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_get_secure_authtype
 *
 * Lookup the type of admin security currently in use.  Upon
 * success, the authentication type is set in the location
 * pointed to by "authtypep".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- unexpected or internal error
 */
scconf_errno_t
scconf_get_secure_authtype(scconf_authtype_t *authtypep)
{
	scconf_errno_t rstatus;
	clconf_cluster_t *clconf;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check argument */
	if (authtypep == NULL)
		return (SCCONF_EINVAL);

	/* Get a copy of the clconf */
	rstatus = scconf_cltr_openhandle(
	    (scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);

	/* Get the authentication type */
	rstatus = conf_get_secure_authtype(clconf, authtypep);

	/* Release the clconf object */
	clconf_obj_release((clconf_obj_t *)clconf);

	/* Return */
	return (rstatus);
}

/*
 * conf_get_secure_authtype
 *
 * Lookup the type of admin security currently in use with the given
 * "clconf".  Upon success, the authentication type is set in the location
 * pointed to by "authtypep".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- unexpected or internal error
 */
scconf_errno_t
conf_get_secure_authtype(clconf_cluster_t *clconf,
    scconf_authtype_t *authtypep)
{
	const char *propval;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check argument */
	if (clconf == NULL || authtypep == NULL)
		return (SCCONF_EINVAL);

	/* Get the auth type */
	propval = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_AUTHTYPE);

	/* Set the authtypep from the property */
	if (propval == NULL)
		*authtypep = SCCONF_AUTH_SYS;
	else if ((strcasecmp(propval, "sys") == 0) ||
	    (strcasecmp(propval, "unix") == 0))
		*authtypep = SCCONF_AUTH_SYS;
	else if ((strcasecmp(propval, "des") == 0) ||
	    (strcasecmp(propval, "df") == 0))
		*authtypep = SCCONF_AUTH_DES;
	else {
		return (SCCONF_EUNEXPECTED);
	}

	return (SCCONF_NOERR);
}

/*
 * scconf_addto_secure_joinlist
 *
 * Add a "nodename" to the list of nodes which may add
 * themselves to the cluster.
 *
 * If the special "nodename" of "." is added, all other names are
 * cleared;  if a "nodename" is added which is NOT ".", then "." is removed.
 * If the list becomes completely empty, any node may add itself to the
 * cluster.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- already in the list
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- unexpected or internal error
 */
scconf_errno_t
scconf_addto_secure_joinlist(char *nodename)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_namelist_t *listptr, *joinlist = (scconf_namelist_t *)0;
	char *joinlistprop = (char *)0;
	clconf_errnum_t clconf_err;
	size_t len;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check argument */
	if (nodename == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Loop until commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Free old joinlist */
		if (joinlist != NULL) {
			scconf_free_namelist(joinlist);
			joinlist = (scconf_namelist_t *)0;
		}

		/* Free old joinlist property */
		if (joinlistprop != (char *)0) {
			free(joinlistprop);
			joinlistprop = (char *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* if "nodename" is ".", it replaces old list */
		if (strcmp(nodename, ".") == 0) {
			joinlistprop = strdup(".");
			if (joinlistprop == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

		/* otherwise, add the new node to the list */
		} else {
			/* Get the current join list */
			rstatus = conf_get_secure_joinlist(
			    (clconf_obj_t *)clconf, &joinlist);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

			/* Allocate the joinlist property buffer */
			len = 0;
			for (listptr = joinlist;  listptr;
			    listptr = listptr->scconf_namelist_next) {
				len += strlen(listptr->scconf_namelist_name);
				++len;
			}
			len += strlen(nodename);
			++len;

			joinlistprop = (char *)calloc(1, len);
			if (joinlistprop == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}

			/* Add nodename to list, removing any dots */
			for (listptr = joinlist;  listptr;
			    listptr = listptr->scconf_namelist_next) {
				if (listptr->scconf_namelist_name == NULL) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}
				if (strcmp(listptr->scconf_namelist_name,
				    nodename) == 0) {
					rstatus = SCCONF_EEXIST;
					goto cleanup;
				}
				if (strcmp(listptr->scconf_namelist_name,
				    ".") == 0)
					continue;
				if (*joinlistprop != '\0')
					(void) strcat(joinlistprop, ",");
				(void) strcat(joinlistprop,
				    listptr->scconf_namelist_name);
			}
			if (*joinlistprop != '\0')
				(void) strcat(joinlistprop, ",");
			(void) strcat(joinlistprop, nodename);
		}

		/* Set the property */
		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_AUTHJOINLIST, joinlistprop);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returns okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	/* Free old joinlist */
	if (joinlist != NULL)
		scconf_free_namelist(joinlist);

	/* Free old joinlist property */
	if (joinlistprop != (char *)0)
		free(joinlistprop);

	return (rstatus);
}

/*
 * scconf_rmfrom_secure_joinlist
 *
 * Remove a "nodename" from the list of nodes which may add
 * themselves to the cluster.
 *
 * If the list becomes completely empty, any node may add itself to the
 * cluster.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- not in the list
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- unexpected or internal error
 */
scconf_errno_t
scconf_rmfrom_secure_joinlist(char *nodename)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	scconf_namelist_t *listptr, *joinlist = (scconf_namelist_t *)0;
	char *joinlistprop = (char *)0;
	clconf_errnum_t clconf_err;
	size_t len;
	int removed;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check argument */
	if (nodename == NULL)
		return (SCCONF_EINVAL);

	/*
	 * Loop until commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Free old joinlist */
		if (joinlist != NULL) {
			scconf_free_namelist(joinlist);
			joinlist = (scconf_namelist_t *)0;
		}

		/* Free old joinlist property */
		if (joinlistprop != (char *)0) {
			free(joinlistprop);
			joinlistprop = (char *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Get the current join list */
		rstatus = conf_get_secure_joinlist((clconf_obj_t *)clconf,
		    &joinlist);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Allocate the joinlist property buffer */
		len = 0;
		for (listptr = joinlist;  listptr;
		    listptr = listptr->scconf_namelist_next) {
			len += strlen(listptr->scconf_namelist_name);
			++len;
		}
		if (len == 0) {
			rstatus = SCCONF_ENOEXIST;
			goto cleanup;
		}
		joinlistprop = (char *)calloc(1, len);
		if (joinlistprop == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/* Remove nodename from list */
		removed = 0;
		for (listptr = joinlist;  listptr;
		    listptr = listptr->scconf_namelist_next) {
			if (strcmp(listptr->scconf_namelist_name,
			    nodename) == 0) {
				++removed;
			} else {
				if (*joinlistprop != '\0')
					(void) strcat(joinlistprop, ",");
				(void) strcat(joinlistprop,
				    listptr->scconf_namelist_name);
			}
		}
		if (!removed) {
			rstatus = SCCONF_ENOEXIST;
			goto cleanup;
		}

		/* If the list is empty, set property to NULL */
		if (*joinlistprop == '\0') {
			free(joinlistprop);
			joinlistprop = NULL;
		}

		/* Set the property */
		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_AUTHJOINLIST, joinlistprop);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returns okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	/* Free old joinlist */
	if (joinlist != NULL)
		scconf_free_namelist(joinlist);

	/* Free old joinlist property */
	if (joinlistprop != (char *)0)
		free(joinlistprop);

	return (rstatus);
}

/*
 * scconf_clear_secure_joinlist
 *
 * Clear the list of nodes able to add themselves to the cluster.
 * Once the list has been cleared, any node may add itself to the cluster.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- unexpected or internal error
 */
scconf_errno_t
scconf_clear_secure_joinlist(void)
{
	scconf_errno_t rstatus = SCCONF_EUNEXPECTED;
	clconf_cluster_t *clconf = (clconf_cluster_t *)0;
	clconf_errnum_t clconf_err;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/*
	 * Loop until commit succeeds, or until a fatal error
	 * is encountered.
	 */
	for (;;) {

		/* Release cluster config */
		if (clconf != (clconf_cluster_t *)0) {
			clconf_obj_release((clconf_obj_t *)clconf);
			clconf = (clconf_cluster_t *)0;
		}

		/* Allocate the cluster config */
		rstatus = scconf_cltr_openhandle(
		    (scconf_cltr_handle_t *)&clconf);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Clear the property */
		clconf_err = clconf_obj_set_property((clconf_obj_t *)clconf,
		    PROP_CLUSTER_AUTHJOINLIST, NULL);
		if (clconf_err != CL_NOERROR) {
			switch (clconf_err) {
			case CL_BADNAME:
			default:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* Commit */
		rstatus = conf_cluster_commit(clconf, NULL);

		/* if commit returns okay, we are done */
		if (rstatus == SCCONF_NOERR) {
			break;

		/* else if outdated handle, continue to retry */
		} else if (rstatus != SCCONF_ESTALE) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

cleanup:
	/* Release cluster config */
	if (clconf != (clconf_cluster_t *)0)
		clconf_obj_release((clconf_obj_t *)clconf);

	return (rstatus);
}

/*
 * scconf_get_secure_joinlist
 *
 * Get the list of nodes which have permission to add themselves
 * to the cluster.   Upon success, a pointer to the list of names
 * is placed in the location pointed to by "joinlistp".   If the list
 * is empty, the pointer is set to NULL.
 *
 * The caller is responsible for freeing memory associated
 * with the "joinlistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
scconf_get_secure_joinlist(scconf_namelist_t **joinlistp)
{
	scconf_errno_t rstatus;
	clconf_cluster_t *clconf;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check argument */
	if (joinlistp == NULL)
		return (SCCONF_EINVAL);

	/* Get a copy of the clconf */
	rstatus = scconf_cltr_openhandle(
	    (scconf_cltr_handle_t *)&clconf);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);

	/* Get the secure join list */
	rstatus = conf_get_secure_joinlist((clconf_obj_t *)clconf, joinlistp);

	/* Release the clconf object */
	clconf_obj_release((clconf_obj_t *)clconf);

	/* Return */
	return (rstatus);
}

/*
 * conf_get_secure_joinlist
 *
 * From the given "clconf", get the list of nodes which have permission
 * to add themselves to the cluster.   Upon success, a pointer to the
 * list of names is placed in the location pointed to by "joinlistp".
 * If the list is empty, the pointer is set to NULL.
 *
 * The caller is responsible for freeing memory associated
 * with the "joinlistp".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EINVAL		- invalid argument
 */
scconf_errno_t
conf_get_secure_joinlist(clconf_obj_t *clconf, scconf_namelist_t **joinlistp)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *propval = (char *)0;
	const char *value;
	scconf_namelist_t *joinlist = (scconf_namelist_t *)0;
	scconf_namelist_t **namelistp;
	char *laststrtok;
	char *ptr;

	/* Check argument */
	if (clconf == NULL)
		return (SCCONF_EINVAL);

	/* Get the joinlist property */
	value = clconf_obj_get_property((clconf_obj_t *)clconf,
	    PROP_CLUSTER_AUTHJOINLIST);

	/* Empty list? */
	if (value == NULL) {
		*joinlistp = (scconf_namelist_t *)0;
		return (SCCONF_NOERR);
	}

	/* Dup the list */
	propval = strdup(value);
	if (propval == NULL)
		return (SCCONF_ENOMEM);

	/* Set the join list */
	namelistp = &joinlist;
	ptr = (char *)propval;
	ptr = strtok_r(ptr, ",", &laststrtok);
	while (ptr != NULL) {
		*namelistp = (scconf_namelist_t *)calloc(1,
		    sizeof (scconf_namelist_t));
		if (*namelistp == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
		(*namelistp)->scconf_namelist_name = strdup(ptr);
		if ((*namelistp)->scconf_namelist_name == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
		namelistp = &(*namelistp)->scconf_namelist_next;
		ptr = strtok_r(NULL, ",", &laststrtok);
	}

cleanup:
	if (propval)
		free(propval);

	if (rstatus == SCCONF_NOERR) {
		*joinlistp = joinlist;
	} else {
		if (joinlist)
			scconf_free_namelist(joinlist);
	}

	return (rstatus);
}
