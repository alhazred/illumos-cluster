/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)scconf_logicalhost.c	1.5	08/05/20 SMI"

/*
 * libscconf logical host config functions
 */

#include "scconf_private.h"

/*
 * scconf_add_lhost
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scconf_errno_t
scconf_add_lhost(scconf_cfg_lhost_t *lhost)
{
	return (SCCONF_NOERR);
}

/*
 * scconf_change_lhost
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scconf_errno_t
scconf_change_lhost(scconf_cfg_lhost_t *lhost)
{
	return (SCCONF_NOERR);
}

/*
 * scconf_rm_lhost
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scconf_errno_t
scconf_rm_lhost(scconf_cfg_lhost_t *lhost)
{
	return (SCCONF_NOERR);
}

/*
 * scconf_get_lhost_config
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scconf_errno_t
scconf_get_lhost_config(char *lhostname, scconf_cfg_lhost_t **lhostp)
{
	return (SCCONF_NOERR);
}

/*
 * scconf_free_lhost_config
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scconf_errno_t
scconf_free_lhost_config(scconf_cfg_lhost_t *lhost)
{
	return (SCCONF_NOERR);
}
