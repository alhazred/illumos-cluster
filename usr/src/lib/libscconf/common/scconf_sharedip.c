/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma	ident	"@(#)scconf_sharedip.c	1.5	08/05/20 SMI"

/*
 * libscconf logical host config functions
 */

#include "scconf_private.h"

/*
 * scconf_add_shost
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scconf_errno_t
scconf_add_shost(scconf_cfg_shost_t *shost)
{
	return (SCCONF_NOERR);
}

/*
 * scconf_change_shost
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scconf_errno_t
scconf_change_shost(scconf_cfg_shost_t *shost)
{
	return (SCCONF_NOERR);
}

/*
 * scconf_rm_shost
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scconf_errno_t
scconf_rm_shost(scconf_cfg_shost_t *shost)
{
	return (SCCONF_NOERR);
}

/*
 * scconf_get_shost_config
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scconf_errno_t
scconf_get_shost_config(char *shostname, scconf_cfg_shost_t **shostp)
{
	return (SCCONF_NOERR);
}

/*
 * scconf_free_shost_config
 */
/* XXX - incomplete function; remove ARGSUSED when complete */
/* ARGSUSED */
scconf_errno_t
scconf_free_shost_config(scconf_cfg_shost_t *shost)
{
	return (SCCONF_NOERR);
}
