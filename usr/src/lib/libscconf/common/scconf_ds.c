/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scconf_ds.c	1.57	08/05/20 SMI"

/*
 * libscconf cluster device services config functions
 */

#include <scconf_private.h>

#include <dlfcn.h>
#include <libdcs.h>
#include <regex.h>

/* Common shared library path for device services shared object */
#define	SCCONF_SHARED_LIB_DIR "/usr/cluster/lib/dcs"
#define	SCCONF_ADD_SERVICE "scds_add_ds"
#define	SCCONF_ADD_LOCAL_SERVICE "scds_add_ds_local"
#define	SCCONF_CHANGE_SERVICE "scds_change_ds"
#define	SCCONF_CHANGE_LOCAL_SERVICE "scds_change_ds_local"
#define	SCCONF_REMOVE_SERVICE "scds_remove_ds"
#define	SCCONF_GET_SERVICE_TYPE "scds_get_ds_type"
#define	SCCONF_GET_SERVICE_CONFIG "scds_get_ds_config"
#define	SCCONF_SDS "SUNWmd"
#define	SCCONF_DISK "DISK"
#define	SCCONF_LOCAL "SUNWlocal"

typedef scconf_errno_t (*conf_add_dgp_t)(char *, char **, scconf_state_t,
    scconf_gdev_range_t *, scconf_state_t, scconf_cfg_prop_t *, char *,
    unsigned int, char **);

typedef scconf_errno_t (*conf_change_dgp_t)(char *, char **, scconf_state_t,
    scconf_gdev_range_t *, scconf_state_t, scconf_cfg_prop_t *, char *,
    unsigned int, char **);

typedef scconf_errno_t (*conf_rm_dgp_t)(char *, char **,
    scconf_gdev_range_t *, scconf_cfg_prop_t *, char *, char **);

typedef scconf_errno_t (*conf_get_srvc_configp_t)(char *, const uint_t,
    scconf_cfg_ds_t **);


static void ds_free_namelist(scconf_namelist_t *namelist);
static void ds_free_proplist(scconf_cfg_prop_t *proplist);
static void *conf_openlib(char *dcs_type, char *scconf_type);

/*
 * scconf_add_ds
 * Call the internal libscconf form of this function with an added
 * numsecondaries and a NULL messages parameter and pass back any error.
 */
scconf_errno_t
scconf_add_ds(char *dstype, char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions)
{
	return (scconf_add_ds_vers2(dstype, dsname, dsnodes, dspreference,
	    dsdevices, dsfailback, dspropertylist, dsoptions,
	    SCCONF_NUMSECONDARIES_UNSET, NULL));
}

/*
 * scconf_add_ds_vers2
 * Add a new device group to the cluster configuration.
 *
 * This function dlopens /usr/cluster/lib/dcs/scconf_<dstype>.so.1
 * and calls that library's scconf_add_deviceservice() routine in
 * order to add a device service for the given type.
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 * If dsfailback is set to SCCONF_STATE_UNCHANGED, the default is
 * to disable failback.
 *
 * See "scds/<dstype>/scds_<dstype>.h" for device specific details.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- device group already exists
 *	SCCONF_ENOEXIST		- an object is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EUSAGE		- unsupported operation
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 *	SCCONF_DS_EINVAL        - device configuration inconsistencies detected
 */
scconf_errno_t
scconf_add_ds_vers2(char *dstype, char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, unsigned int dsnumsecondaries, char **messages)
{
	void *dl_handle = NULL;
	conf_add_dgp_t scconf_add_dgp;
	scconf_errno_t scconferr = SCCONF_NOERR;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	dl_handle = conf_openlib(NULL, dstype);
	if (dl_handle == (void *)0) {
		scconferr = SCCONF_EUNKNOWN;
		goto cleanup;
	}

	/* Get the address of the function */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if ((scconf_add_dgp = (conf_add_dgp_t)dlsym(dl_handle,
	    SCCONF_ADD_SERVICE)) !=  NULL) {

		scconferr = (*scconf_add_dgp)(dsname, dsnodes, dspreference,
		    dsdevices, dsfailback, dspropertylist, dsoptions,
		    dsnumsecondaries, messages);

	} else {
		scconferr = SCCONF_EUSAGE;
	}
	/*lint -restore */

cleanup:
	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}
	return (scconferr);
}

/*
 * scconf_add_ds_local
 * Add a new device group to the cluster configuration.
 *
 * This function dlopens /usr/cluster/lib/dcs/scconf_<dstype>.so.1
 * and calls that library's scconf_add_deviceservice() routine in
 * order to add a device service for the given type.
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 * If dsfailback is set to SCCONF_STATE_UNCHANGED, the default is
 * to disable failback.
 *
 * See "scds/<dstype>/scds_<dstype>.h" for device specific details.
 *
 * Unlike the similar function scds_add_ds, this function only acts on the
 * local node.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- device group already exists
 *	SCCONF_ENOEXIST		- an object is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EBUSY		- busy state, try again later
 *	SCCONF_EUSAGE		- unsupported operation
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 */
scconf_errno_t
scconf_add_ds_local(char *dstype, char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, unsigned int dsnumsecondaries, char **messages)
{
	void *dl_handle = NULL;
	conf_add_dgp_t scconf_add_dgp;
	scconf_errno_t scconferr = SCCONF_NOERR;

	dl_handle = conf_openlib(NULL, dstype);
	if (dl_handle == (void *)0) {
		scconferr = SCCONF_EUNKNOWN;
		goto cleanup;
	}

	/* Get the address of the function */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if ((scconf_add_dgp = (conf_add_dgp_t)
	    dlsym(dl_handle, SCCONF_ADD_LOCAL_SERVICE)) != NULL) {

		scconferr = (*scconf_add_dgp)(dsname, dsnodes, dspreference,
		    dsdevices, dsfailback, dspropertylist, dsoptions,
		    dsnumsecondaries, messages);

	} else {
		scconferr = SCCONF_EUSAGE;
	}
	/*lint -restore */

cleanup:
	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}
	return (scconferr);
}

/*
 * scconf_change_ds
 * Call the internal libscconf form of this function with an added
 * numsecondaries parameter and pass back any error.
 */
scconf_errno_t
scconf_change_ds(char *dsname, char *dsnodes[], scconf_state_t dspreference,
    scconf_gdev_range_t *dsdevices, scconf_state_t dsfailback,
    scconf_cfg_prop_t *dspropertylist, char *dsoptions)
{
	return (scconf_change_ds_vers2(dsname, dsnodes, dspreference,
	    dsdevices, dsfailback, dspropertylist, dsoptions,
	    SCCONF_NUMSECONDARIES_UNSET, NULL));
}

/*
 * scconf_change_ds_vers2
 *
 * This function gets the device service type from the "dsname". It then
 * dlopens /usr/cluster/lib/dcs/scconf_<dstype>.so.1
 * and calls that library's scconf_change_deviceservice() routine in
 * order to change options for the given device service.
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 * See "scds/<dstype>/scds_<dstype>.h" for device specific details.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group, device, or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EUSAGE		- unsupported operation
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 *	SCCONF_DS_EINVAL        - device configuration inconsistencies detected
 */
scconf_errno_t
scconf_change_ds_vers2(char *dsname, char *dsnodes[],
    scconf_state_t dspreference, scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, unsigned int dsnumsecondaries, char **messages)
{
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t scconferr = SCCONF_NOERR;
	char *service_class_name = NULL;
	void *dl_handle = NULL;
	conf_change_dgp_t scconf_change_dgp;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	dc_err = dcs_get_service_parameters(dsname, &service_class_name, NULL,
	    NULL, NULL, NULL, NULL);

	if (dc_err != DCS_SUCCESS) {
		scconferr = SCCONF_ENOEXIST;
		goto cleanup;
	}

	dl_handle = conf_openlib(service_class_name, NULL);
	if (dl_handle == (void *)0) {
		scconferr = SCCONF_EUNKNOWN;
		goto cleanup;
	}

	/* Get the address of the function */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if ((scconf_change_dgp = (conf_change_dgp_t)
	    dlsym(dl_handle, SCCONF_CHANGE_SERVICE)) != NULL) {

		scconferr = (*scconf_change_dgp)(dsname, dsnodes,
		    dspreference, dsdevices, dsfailback, dspropertylist,
		    dsoptions, dsnumsecondaries, messages);
	} else {
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}
	/*lint -restore */

cleanup:
	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}
	if (service_class_name != NULL)
		dcs_free_string(service_class_name);
	return (scconferr);
}

/*
 * scconf_change_ds_local
 *
 * This function gets the device service type from the "dsname". It then
 * dlopens /usr/cluster/lib/dcs/scconf_<dstype>.so.1
 * and calls that library's scconf_change_deviceservice() routine in
 * order to change options for the given device service.
 *
 * The "dsnodes" argument is a NULL terminated array of character
 * pointers each pointing to a nodename.  The order in which the nodes
 * are given establishes preference.  Some device types provide a method for
 * defaulting the nodes preference list, allowing "dsnodes" to be
 * NULL.
 *
 * See "scds/<dstype>/scds_<dstype>.h" for device specific details.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group, device, or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EUSAGE		- unsupported operation
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 */
scconf_errno_t
scconf_change_ds_local(char *dsname, char *dsnodes[],
    scconf_state_t dspreference,
    scconf_gdev_range_t *dsdevices,
    scconf_state_t dsfailback,
    scconf_cfg_prop_t *dspropertylist, char *dsoptions,
    unsigned int dsnumsecondaries, char **messages)
{
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t scconferr = SCCONF_NOERR;
	char *service_class_name = NULL;
	void *dl_handle = NULL;
	conf_change_dgp_t scconf_change_dgp;

	dc_err = dcs_get_service_parameters(dsname, &service_class_name, NULL,
	    NULL, NULL, NULL, NULL);

	if (dc_err != DCS_SUCCESS) {
		scconferr = SCCONF_ENOEXIST;
		goto cleanup;
	}

	dl_handle = conf_openlib(service_class_name, NULL);
	if (dl_handle == (void *)0) {
		scconferr = SCCONF_EUNKNOWN;
		goto cleanup;
	}

	/* Get the address of the function */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if ((scconf_change_dgp = (conf_change_dgp_t)
	    dlsym(dl_handle, SCCONF_CHANGE_LOCAL_SERVICE)) != NULL) {

		scconferr = (*scconf_change_dgp)(dsname, dsnodes,
		    dspreference, dsdevices, dsfailback, dspropertylist,
		    dsoptions, dsnumsecondaries, messages);
	} else {
		scconferr = SCCONF_EUSAGE;
		goto cleanup;
	}
	/*lint -restore */

cleanup:
	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}
	if (service_class_name != NULL)
		dcs_free_string(service_class_name);
	return (scconferr);
}

/*
 * scconf_rm_ds
 * Call the internal libscconf form of this function with an added
 * messages parameter and pass back any error.
 */
scconf_errno_t
scconf_rm_ds(char *dsname, char *dsnodes[], scconf_gdev_range_t *dsdevices,
    scconf_cfg_prop_t *dspropertylist, char *dsoptions)
{
	return (scconf_rm_ds_vers2(dsname, dsnodes, dsdevices, dspropertylist,
	    dsoptions, NULL));
}

/*
 * scconf_rm_ds_vers2
 *
 * Depending on the type of device group class, it dlopens appropriate
 * /usr/cluster/lib/dcs/scconf_<dstype>.so.1.
 * Upon success, the user specified device group is removed from the DCS
 * configuration. If nodelist and global device list is also specified
 * then they are removed from the disk group and the disk group is removed
 * if no nodes and devices exist in the disk group.
 *
 * See "scds/<dstype>/scds_<dstype>.h" for device specific details.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_ENOEXIST		- device group, device, or node is not found
 *	SCCONF_EINUSE		- an object is already in use
 *	SCCONF_EUSAGE		- unsupported operation
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_ESETUP		- handle data is invalid
 *	SCCONF_EUNKNOWN		- unknown type
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_ERANGE		- operation not allowed because of out of range
 *				  numsecondaries
 */
scconf_errno_t
scconf_rm_ds_vers2(char *dsname, char *dsnodes[],
    scconf_gdev_range_t *dsdevices, scconf_cfg_prop_t *dspropertylist,
    char *dsoptions, char **messages)
{
	dc_error_t dc_err = 0;
	scconf_errno_t scconferr = SCCONF_NOERR;
	char *service_class_name = NULL;
	void *dl_handle = NULL;
	conf_rm_dgp_t scconf_rm_dgp;

	/* I18N */
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);


	dc_err = dcs_get_service_parameters(dsname, &service_class_name, NULL,
	    NULL, NULL, NULL, NULL);

	if (dc_err != DCS_SUCCESS) {
		scconferr = SCCONF_ENOEXIST;
		goto cleanup;
	}

	dl_handle = conf_openlib(service_class_name, NULL);
	if (dl_handle == (void *)0) {
		scconferr = SCCONF_EUNKNOWN;
		goto cleanup;
	}

	/* Get the address of the function */
	/*lint -save -e611 suppress "Suspicious cast" Warning */
	if ((scconf_rm_dgp = (conf_rm_dgp_t)
	    dlsym(dl_handle, SCCONF_REMOVE_SERVICE)) != NULL) {
		scconferr = (*scconf_rm_dgp)(dsname, dsnodes, dsdevices,
		    dspropertylist, dsoptions, messages);
	} else {
		scconferr = SCCONF_EUSAGE;
	}
	/*lint -restore */

cleanup:
	if (dl_handle != (void *)0) {
		(void) dlclose(dl_handle);
	}
	if (service_class_name != NULL)
		dcs_free_string(service_class_name);
	return (scconferr);
}

/*
 * scconf_get_ds_config
 *
 * Get a copy of a device services configuration.
 *
 * If "dsname" is NULL, the entire device services configuration
 * is returned.
 *
 * Depending on the type of device group class, appropriate .so is opened.
 * Upon success , dsconfigp structure is populated with a list of device group
 * objects. Memory is allocated for dsconfigp structure even if there is some
 * error returned. This memory is freed up by the caller.
 * The variable options is used when the caller wants to get the list of all the
 * character or block devices for rawdisk or SDS. The value of options
 * could be assigned using the flags defined to get this info. The list of
 * block device can be retrieved by using the 'scconf_ds_bdevicenames' field
 * of the structure 'scconf_cfg_ds_t' and the list of character device can
 * be retrieved using the 'scconf_ds_cdevicenames' field of the structure
 * 'scconf_cfg_ds_t'.
 *
 * See "scds/<dstype>/scds_<dstype>.h" for device specific details.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_EEXIST		- the given "nodename" is already configured
 *	SCCONF_ENOEXIST		- an object does not exit
 *	SCCONF_EUSAGE		- unsupported operation
 *	SCCONF_EINVAL		- invalid argument
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_ds_config(char *name, const uint_t options,
    scconf_cfg_ds_t **dsconfigp)
{
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t scconferr = SCCONF_NOERR;
	uint_t i = 0;
	void *dl_handle = NULL;
	char *service_class_name = NULL;
	char *dsnames[1];
	dc_string_seq_t *service_names = NULL;
	conf_get_srvc_configp_t scconf_get_srvc_configp;
	scconf_cfg_ds_t *current_ptr = (scconf_cfg_ds_t *)0;
	scconf_cfg_ds_t *dsconfig = (scconf_cfg_ds_t *)0;
	const char *dsclass = NULL;
	const char *dsname = NULL;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (dsconfigp == NULL)
		return (SCCONF_EINVAL);

	if (options & SCCONF_BY_NAME_FLAG) {
		if (options & SCCONF_BY_TYPE_FLAG) {
			return (SCCONF_EINVAL);
		}
		dsname = name;
	} else {
		if (options & SCCONF_BY_TYPE_FLAG) {
			dsclass = name;
		} else {
			return (SCCONF_EINVAL);
		}
	}

	/* get names of all services */
	if (dsname == NULL) {
		if (dsclass == NULL) {
			dc_err = dcs_get_service_names(&service_names);
		} else {
			if (strcmp(dsclass, "sds") == 0) {
				dc_err = dcs_get_service_names_of_class(
				    SCCONF_SDS, &service_names);
			} else if (strcmp(dsclass, "multi-owner-svm") == 0) {
				dc_err = dcs_get_service_names_of_class(
				    SCCONF_LOCAL, &service_names);
			} else if (strcmp(dsclass, "rawdisk") == 0) {
				dc_err = dcs_get_service_names_of_class(
				    SCCONF_DISK, &service_names);
			} else {
				dc_err = dcs_get_service_names_of_class(dsclass,
				    &service_names);
			}
		}
		if (dc_err != DCS_SUCCESS) {
			scconferr = SCCONF_EUNEXPECTED;
			*dsconfigp = NULL;
			goto cleanup;
		}
	} else {
		if ((service_names = (dc_string_seq_t *)
		    calloc(1, sizeof (dc_string_seq_t))) == NULL) {
			scconferr = SCCONF_ENOMEM;
			goto cleanup;
		}

		bzero(dsnames, sizeof (dsnames));
		if ((dsnames[0] = strdup(dsname)) == NULL) {
			scconferr = SCCONF_ENOMEM;
			goto cleanup;
		}
		service_names->strings = dsnames;
		service_names->count = 1;
	}

	*dsconfigp = NULL;

	if (service_names->count == 0) {
		scconferr = SCCONF_NOERR;
		goto cleanup;
	}

	/* Get service class of every service */
	for (i = 0; i < service_names->count; i++) {

		dc_err = dcs_get_service_parameters(
		    (service_names->strings)[i],
		    &service_class_name, NULL, NULL, NULL,
		    NULL, NULL);

		/*
		 * If the name is invalid, and the name was not passed
		 * from the caller (dsname == NULL) assume the service_name
		 * was deleted between the time it was obtained above and now.
		 * In this case just go on to the next entry
		 */
		if ((dc_err == DCS_ERR_SERVICE_NAME) && (dsname == NULL)) {
			if (service_class_name != NULL) {
				dcs_free_string(service_class_name);
				service_class_name = NULL;
			}
			continue;
		}

		if (dc_err != DCS_SUCCESS) {
			scconferr = SCCONF_EUNKNOWN;
			goto cleanup;
		}

		/* allocate the new list structure */
		if ((dsconfig = (scconf_cfg_ds_t *)
		    calloc(1, sizeof (scconf_cfg_ds_t))) == NULL) {
			scconferr = SCCONF_ENOMEM;
			goto cleanup;
		}

		dl_handle = conf_openlib(service_class_name, NULL);
		if (dl_handle == (void *)0) {
			if (service_class_name != NULL) {
				dcs_free_string(service_class_name);
				service_class_name = NULL;
			}
			if (dsconfig != NULL) {
				scconf_free_ds_config(dsconfig);
				dsconfig = NULL;	/*lint !e423 */
			}
			continue;
		}

		/* Get the address of the function */
		/*lint -save -e611 suppress "Suspicious cast" Warning */
		if ((scconf_get_srvc_configp = (conf_get_srvc_configp_t)
		    dlsym(dl_handle, SCCONF_GET_SERVICE_CONFIG))
		    != NULL) {
			scconferr = (*scconf_get_srvc_configp)
			    ((service_names->strings) [i],
			    options, &dsconfig);

			if (scconferr == SCCONF_ENOEXIST) {
				if (service_class_name != NULL) {
					dcs_free_string(service_class_name);
					service_class_name = NULL;
				}
				if (dsconfig != NULL) {
					scconf_free_ds_config(dsconfig);
					dsconfig = NULL;
				}
				if (dl_handle != (void *)0) {
					(void) dlclose(dl_handle);
					dl_handle = NULL;
				}
				continue;
			}

			if (scconferr != SCCONF_NOERR) {
				goto cleanup;
			}
		} else {
			scconferr = SCCONF_EUSAGE;
			goto cleanup;
		}
		/*lint -restore */

		if (dl_handle != (void *)0) {
			(void) dlclose(dl_handle);
			dl_handle = NULL;
		}

		if (current_ptr != NULL) {
			dsconfig->scconf_ds_next = current_ptr;
		} else {
			dsconfig->scconf_ds_next = NULL;
		}
		current_ptr = dsconfig;
		dsconfig = NULL;

		if (service_class_name != NULL) {
			dcs_free_string(service_class_name);
			service_class_name = NULL;
		}
	}
	*dsconfigp = current_ptr;
	current_ptr = NULL;

cleanup:
	if (service_names != NULL) {
		if (dsname == NULL) {
			dcs_free_string_seq(service_names);
		} else {
			if (service_names->strings[0] != NULL)
				free(service_names->strings[0]);
			free(service_names);
		}
	}
	if (service_class_name != NULL) {
		dcs_free_string(service_class_name);
	}
	if (dsconfig != NULL)
		scconf_free_ds_config(dsconfig);
	if (current_ptr != NULL)
		scconf_free_ds_config(current_ptr);
	if (dl_handle != (void *)0)
		(void) dlclose(dl_handle);

	return (scconferr);
}

/*
 * scconf_rm_node_ds
 *
 * The given node is removed from all device groups that are attached
 * to it.
 *
 * The device groups can be of the specified "ds_type"; If ds_type
 * is NULL, the device groups are all types of devices in the
 * device service.
 *
 * If "messages" is not NULL, it should either point to NULL or a NULL-
 * terminated character string.  When "messages" is not NULL, messages may
 * be passed back to the caller by either setting "*messages" to a new
 * messages buffer or by enlarging and concatenating to the given buffer.
 * Each message is terminated with a NEWLINE, and the complete message buffer
 * is terminated with a NULL.  It is the caller's responsibility to free the
 * messages buffer memory.
 *
 * Possible return values:
 *
 * 	SCCONF_NOERR		- success
 * 	SCCONF_EPERM		- not root
 * 	SCCONF_ENOMEM		- not enough memory
 * 	SCCONF_EINVAL		- invalid argument
 * 	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_rm_node_ds(char *node, const char *ds_type,
    char **messages)
{
	scconf_errno_t		rstatus = SCCONF_EUNEXPECTED;
	scconf_errno_t		tmp_err = SCCONF_EUNEXPECTED;
	scconf_cfg_ds_t		*cp,
				*config = NULL;
	scconf_nodeid_t		*current_nodeid;
	char			**dsnodes = NULL;
	char			dsoption[BUFSIZ];
	char			buffer[BUFSIZ];
	int			is_attached = 0;
	scconf_nodeid_t		nodeid;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check the nodename */
	if (node == NULL)
		return (SCCONF_EINVAL);

	/* Get the nodeid */
	rstatus = scconf_get_nodeid(node, &nodeid);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Get all devices that are of type *ds_type */
	if (ds_type) {
		rstatus = scconf_get_ds_config((char *)ds_type,
		    SCCONF_BY_TYPE_FLAG, &config);
	} else {
		/* Get all types of devices */
		rstatus = scconf_get_ds_config(NULL, SCCONF_BY_NAME_FLAG,
		    &config);
	}
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Get node into proper format so we can call scconf_rm_ds() */
	dsnodes = (char **)calloc((size_t)2, sizeof (char *));
	if (dsnodes == (char **)0) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}
	dsnodes[0] = node;
	dsnodes[1] = NULL;

	dsoption[0] = '\0';

	/*
	 * Iterate through all the devices.
	 */
	for (cp = config; cp; cp = cp->scconf_ds_next) {
		if (!cp->scconf_ds_name || !cp->scconf_ds_type) {
			continue;
		}

		/* Skip device not attached to this node */
		is_attached = 0;
		current_nodeid = cp->scconf_ds_nodelist;
		while (current_nodeid != NULL &&
			*current_nodeid != (scconf_nodeid_t)0) {
			if (*current_nodeid == nodeid) {
				is_attached = 1;
				break;
			} else {
				current_nodeid++;
			}
		}
		if (is_attached == 0) {
			/* Node is not attached so skip this */
			continue;
		}

		/*
		 * For local rawdisk, set 'localonly' to false so we may then
		 * remove it successfully.
		 */
		if (cp->scconf_ds_type && strcmp(cp->scconf_ds_type,
			"Local_Disk") == 0) {
			if (scconf_change_ds(cp->scconf_ds_name, NULL, NULL,
			    NULL, NULL, NULL, "localonly=false") !=
			    SCCONF_NOERR) {
				if (messages != (char **)0) {
					(void) sprintf(buffer,
					    dgettext(TEXT_DOMAIN,
					    "Cannot change localonly property "
					    "for rawdisk device \"%s\".\n"),
					    cp->scconf_ds_name);
					scconf_addmessage(buffer, messages);
					if (rstatus == SCCONF_NOERR) {
						rstatus = SCCONF_EUNEXPECTED;
					}
				}
			}
		}

		/* Remove the node */
		tmp_err = scconf_rm_ds_vers2(cp->scconf_ds_name, dsnodes, NULL,
		    NULL, dsoption, messages);
		if (tmp_err != SCCONF_NOERR) {
			if (messages != (char **)0) {
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Cannot remove node \"%s\" from %s "
				    "device group \"%s\"."), node,
				    cp->scconf_ds_type, cp->scconf_ds_name);
				scconf_addmessage(buffer, messages);
			}
			if (rstatus == SCCONF_NOERR) {
				rstatus = tmp_err;
			}
		}
	}

cleanup:
	/* Free memory */
	if (config)
		scconf_free_ds_config(config);
	free(dsnodes);

	return (rstatus);
}

/*
 * Given a dev_t value, return the name of device service that contains this
 * device.
 *
 * The caller is responsible for freeing the memory returned in "name".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- the given device is not configured
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_ENOCLUSTER	- cluster config does not exist
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
scconf_errno_t
scconf_get_ds_by_devt(major_t maj, minor_t min, char **dsname)
{
	dc_error_t dc_err = DCS_SUCCESS;
	scconf_errno_t scconferr = SCCONF_NOERR;
	char *service_name;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	dc_err = dcs_get_service_name_by_dev_t(maj, min, &service_name);
	switch (dc_err) {
	case DCS_SUCCESS:
		break;
	case DCS_ERR_NOT_CLUSTER:
		scconferr = SCCONF_ENOCLUSTER;
		break;
	case DCS_ERR_DEVICE_INVAL:
		scconferr = SCCONF_ENOEXIST;
		break;
	default:
		scconferr = SCCONF_EUNEXPECTED;
		break;
	} /*lint !e788 */

	if (scconferr == SCCONF_NOERR) {
		/*
		 * We can't just return service_name because it has been
		 * allocated by libdcs, which is implemented in C++ and uses
		 * new and delete to handle memory allocation as opposed to
		 * this C library which uses malloc and free.
		 */
		*dsname = strdup(service_name);
		dcs_free_string(service_name);
		if (*dsname == NULL) {
			scconferr = SCCONF_ENOMEM;
		}
	}

	return (scconferr);
}

/*
 * scconf_free_ds_config
 *
 * Free memory for dsconfig structure.
 *
 * Possible return values:
 *
 *	NONE
 */
void
scconf_free_ds_config(scconf_cfg_ds_t *dsconfig)
{
	scconf_cfg_ds_t *last, *next;
	scconf_gdev_range_t *pdev;
	scconf_gdev_range_t *pdev_next;

	if (dsconfig == (scconf_cfg_ds_t *)0)
		return;

	/* free memory for each member of the list */
	last = (scconf_cfg_ds_t *)0;
	next = dsconfig;
	while (next != NULL) {
		if (next->scconf_ds_name != NULL)
			free(next->scconf_ds_name);
		if (next->scconf_ds_type != NULL)
			free(next->scconf_ds_type);
		if (next->scconf_ds_label != NULL)
			free(next->scconf_ds_label);
		if (next->scconf_ds_nodelist != NULL)
			free(next->scconf_ds_nodelist);
		pdev = next->scconf_ds_devlist;
		while (pdev != NULL) {
			pdev_next = pdev->scconf_gdev_next;
			free(pdev);
			pdev = pdev_next;
		}
		ds_free_namelist(next->scconf_ds_devvalues);
		ds_free_namelist(next->scconf_ds_cdevicenames);
		ds_free_namelist(next->scconf_ds_bdevicenames);
		ds_free_proplist(next->scconf_ds_propertylist);

		last = next;
		next = next->scconf_ds_next;
		free(last);
	}
}

/*
 * Get a copy of the did configuration for the given did device.
 *
 * The did name specified should be in the form "dx".
 *
 * The caller is responsible for freeing memory associated with 'didconfigp'.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root
 *	SCCONF_ENOEXIST		- the did name specified does not exist
 */
scconf_errno_t
scconf_get_did_config(char *didname, scconf_cfg_did_t **didconfig)
{
	did_device_list_t *didlist;
	did_subpath_t *pathlist;
	scconf_errno_t scconferr = SCCONF_NOERR;
	scconf_cfg_devices_t *slist = NULL;
	scconf_cfg_devices_t *slist_last = NULL;

	didlist = map_from_did_device(didname);
	if (didlist == NULL) {
		return (SCCONF_ENOEXIST);
	}

	*didconfig = (scconf_cfg_did_t *)calloc(1, sizeof (scconf_cfg_did_t));
	if (*didconfig == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}
	(*didconfig)->scconf_devlist = NULL;

	(*didconfig)->scconf_didname = strdup(didname);
	if ((*didconfig)->scconf_didname == NULL) {
		scconferr = SCCONF_ENOMEM;
		goto cleanup;
	}

	pathlist = didlist->subpath_listp;
	while (pathlist) {
		char *tmp;
		tmp = strchr(pathlist->device_path, ':');
		if (tmp == NULL) {
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		*tmp = 0;
		slist = (scconf_cfg_devices_t *)calloc(1,
		    sizeof (scconf_cfg_devices_t));
		slist->scconf_device_next = NULL;
		slist->devname = strdup(tmp+1);
		if (slist->devname == NULL) {
			scconferr = SCCONF_ENOMEM;
			goto cleanup;
		}
		scconferr = scconf_get_nodeid(pathlist->device_path,
		    &(slist->nodeid));
		if (scconferr != SCCONF_NOERR) {
			scconferr = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
		if ((*didconfig)->scconf_devlist == NULL) {
			(*didconfig)->scconf_devlist = slist;
		} else {
			slist_last->scconf_device_next = slist;
		}
		slist_last = slist;
		slist = NULL;
		pathlist = pathlist->next;
	}

cleanup:
	if ((scconferr != SCCONF_NOERR) && (*didconfig != NULL)) {
		scconf_free_did_config(*didconfig);
		*didconfig = NULL;
	}
	if (slist != NULL) {
		if (slist->devname != NULL)
			free(slist->devname);
		free(slist);
	}

	return (scconferr);
}

/*
 * Free all memory associated with an "scconf_cfg_ds" structure.
 */
void
scconf_free_did_config(scconf_cfg_did_t *didconfig)
{
	scconf_cfg_devices_t *slist;
	scconf_cfg_devices_t *slist_next;

	if (didconfig == NULL) {
		return;
	}

	if (didconfig->scconf_didname != NULL)
		free(didconfig->scconf_didname);
	slist = didconfig->scconf_devlist;
	while (slist) {
		slist_next = slist->scconf_device_next;
		if (slist->devname != NULL)
			free(slist->devname);
		free(slist);
		slist = slist_next;
	}
	free(didconfig);
}

/*
 * Helper function to free a 'scconf_namelist_t' structure.
 */
static void
ds_free_namelist(scconf_namelist_t *nmlist)
{
	scconf_namelist_t *last, *next;

	if (nmlist == (scconf_namelist_t *)0)
		return;

	/* free memory for each member of the list */
	last = (scconf_namelist_t *)0;
	next = nmlist;
	while (next != NULL) {
		if (next->scconf_namelist_name != NULL)
			free(next->scconf_namelist_name);

		last = next;
		next = next->scconf_namelist_next;
		free(last);
	}
}

/*
 * Helper function to free a 'scconf_cfg_prop_t' structure.
 */
static void
ds_free_proplist(scconf_cfg_prop_t *proplist)
{
	scconf_cfg_prop_t *last, *next;

	if (proplist == (scconf_cfg_prop_t *)0)
		return;

	/* free memory for each member of the list */
	last = (scconf_cfg_prop_t *)0;
	next = proplist;
	while (next != NULL) {
		if (next->scconf_prop_key != NULL)
			free(next->scconf_prop_key);
		if (next->scconf_prop_value != NULL)
			free(next->scconf_prop_value);

		last = next;
		next = next->scconf_prop_next;
		free(last);
	}
}

/*
 * conf_openlib
 *
 * This function opens the "/usr/cluster/lib/dcs" dir and based on the
 * device type return a handle of the right shared library.
 *
 * Possible return values:
 *	equal to zero		- error
 *	nonzero			- success. A library handle is returned.
 */
static void *
conf_openlib(char *dcs_type, char *scconf_type)
{
	boolean_t match_found = B_FALSE;
	char path_name[MAXPATHLEN];
	void *dl_handle = NULL;
	DIR *dirp = NULL;
	char dirbuf[BUFSIZ];
	struct dirent *dp = NULL;
	boolean_t (*scconf_get_service_typep)(char *, char *);
	char *pattern = "^scconf_.*.so.1$";
	regex_t re;

	/* Open library */
	if ((dirp = opendir(SCCONF_SHARED_LIB_DIR)) == NULL) {
		goto cleanup;
	}

	/* Compile the pattern */
	if (regcomp(&re, pattern, REG_EXTENDED) != 0) {
		goto cleanup;
	}

	while ((dp = readdir(dirp)) != NULL) {
		/* Check if the library opened is in the format required */
		if (regexec(&re, dp->d_name, (size_t)0, NULL, 0) != 0)
			continue;
		if (sscanf(dp->d_name, "scconf_%s", dirbuf) != 1)
			continue;

		(void) sprintf(path_name, "%s/%s", SCCONF_SHARED_LIB_DIR,
		    dp->d_name);

		/*
		 * Open the shared library with RTLD_NODELETE so it
		 * won't be loaded repeatedly in subsequent calls to
		 * this function.
		 */
		if ((dl_handle = dlopen(path_name,
		    RTLD_NOW|RTLD_NODELETE)) == (void *)0) {
			(void) closedir(dirp);
			return ((void *)0);
		}

		/* Get the address of the function */
		/*lint -save -e611 suppress "Suspicious cast" Warning */
		if ((scconf_get_service_typep = (boolean_t(*)(char *, char *))
		    dlsym(dl_handle, SCCONF_GET_SERVICE_TYPE)) != NULL) {

			/* call the function */
			match_found = (*scconf_get_service_typep)(scconf_type,
			    dcs_type);
			if (match_found) {
				goto cleanup;
			} else {
				if (dl_handle != (void *)0) {
					(void) dlclose(dl_handle);
					dl_handle = NULL;
				}
				continue;
			}
		} else {
			if (dl_handle != (void *)0)
				(void) dlclose(dl_handle);
			dl_handle = NULL;
			goto cleanup;
		}
		/*lint -restore */
	}
cleanup:
	regfree(&re);
	if (dirp != NULL)
		(void) closedir(dirp);
	return (dl_handle);
}

/*
 * scconf_is_local_device_service
 *
 * This function determines if the device service name corresponds to
 * a local device service (SUNWlocal).
 *
 * Possible return values:
 *	B_TRUE  - device service is of type of SCCONF_LOCAL
 *	B_FALSE - device service is of another type
 *
 */
boolean_t
scconf_is_local_device_service(char *dsname)
{
	char *service_class_name = NULL;

	if (dsname == NULL)
		return (B_FALSE);

	/*
	 * Find the service class name associated with this service and
	 * determine if it is of type SCCONF_LOCAL.
	 */
	if (dcs_get_service_parameters(dsname, &service_class_name,
	    NULL, NULL, NULL, NULL, NULL) == DCS_SUCCESS) {
		if (service_class_name != NULL &&
		    strcmp(service_class_name, SCCONF_LOCAL) == 0) {
			return (B_TRUE);
		}
	}
	dcs_free_string(service_class_name);
	return (B_FALSE);
}


/*
 * scconf_verify_replication
 *
 * Function of scconf that verifies replication configuration.
 * Verifies that all disks in the dg are of either replicated or
 * non-replicated type.  A mixture of replicated and non-replicated
 * devices is not allowed.  Another check it makes is that all disks
 * in devicegroup are replicated in same dev_grp (truecopy replication
 * unit) and the name of the diskgroup should be same as the name of the
 * dev_grp.  To do this, it uses check_for_repl_device function of libdid.
 * Based on the checks, "replicated" will be set to B_TRUE or B_FALSE.
 * Also if "replicated" is set to B_TRUE, "repl_type" will be set to type
 * of replication.
 *
 * Possible return values:
 *		SCCONF_NOERR		- success
 *		SCCONF_ENOMEM		- not enough memory
 *		SCCONF_EUNEXPECTED	- internal or unexpected error
 *		SCCONF_DS_EINVAL	- inconsistencies detected in device
 *					  configuration
 *
 */
scconf_errno_t
scconf_verify_replication(char **globaldev_list, char *dsname,
	boolean_t *replicated, char **repl_type, char **messages)
{
	char did_path[MAXPATHLEN];
	boolean_t got_replicated = B_FALSE;
	boolean_t got_not_replicated = B_FALSE;
	scconf_errno_t rstatus = SCCONF_NOERR;
	int i = 0;
	char buffer[SCCONF_MAXSTRINGLEN];
	char *dev_grp_name = NULL;
	char *dg_repl_type = NULL;
	did_repl_list_t repl_data;
	did_repl_t is_replicated;
	int libinit;

	/* dsname cannot be null */
	if (dsname == NULL) {
		return (SCCONF_EUNEXPECTED);
	}

	/* Libdid intialisation */
	libinit = did_initlibrary(1, DID_NONE, NULL);
	if (libinit != DID_INIT_SUCCESS) {
		rstatus = SCCONF_EUNEXPECTED;
		return (rstatus);
	}

	/*
	 * Verify if all disks in dg are either replicated
	 * or non-replicated.A mix is an error. Also verify
	 * that all disks in dg belong to the same replication
	 * type. A mix is an error.
	 */
	for (i = 0; globaldev_list[i] != NULL; i++) {

		if (scconf_didname(globaldev_list[i], did_path)) {
			rstatus = SCCONF_EUNEXPECTED;
			return (rstatus);
		}

		is_replicated = check_for_repl_device(did_path, &repl_data);
		switch (is_replicated) {
		case DID_REPL_TRUE:
			/* Replicated device */
			got_replicated = B_TRUE;
			dev_grp_name = repl_data.dev_group;

			if (dg_repl_type == NULL) {
				dg_repl_type = repl_data.repl_type;
			} else if (strcmp(dg_repl_type,
					repl_data.repl_type) != 0) {
				/*
				 * Replicated devices are not of same
				 * replicated type.
				 */
				rstatus = SCCONF_DS_EINVAL;
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Some disks in the devicegroup \"%s\" "
				    "belong to different replication type."
				    "Please verify your replication "
				    "configuration."), dsname);
				scconf_addmessage(buffer, messages);
				return (rstatus);
			}

			if (strcmp(dev_grp_name, dsname) != 0) {
				/* dev_grp_name is different from dg name */
				rstatus = SCCONF_DS_EINVAL;
				(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
				    "Some disks in the devicegroup \"%s\" "
				    "are replicated whose name does not "
				    "match the name of devicegroup.\nPlease "
				    "verify your replication configuration."),
				    dsname);
				scconf_addmessage(buffer, messages);
				return (rstatus);
			}
			break;

		case DID_REPL_FALSE:
			/* Non-replicated device */
			got_not_replicated = B_TRUE;
			break;

		case DID_REPL_BAD_DEV:
		case DID_REPL_ERROR:
		default:
			/* Internal error */
			rstatus = SCCONF_EUNEXPECTED;
			return (rstatus);
		}
	}

	if (got_replicated == B_TRUE &&
	    got_not_replicated == B_TRUE) {
		rstatus = SCCONF_DS_EINVAL;
		(void) sprintf(buffer, dgettext(TEXT_DOMAIN,
		    "The diskgroup \"%s\" consists of both replicated and "
		    "non replicated devices. Verify the replication setup.\n"),
		    dsname);

		scconf_addmessage(buffer, messages);
		return (rstatus);
	}

	if (got_not_replicated == B_TRUE) {
		/* It is non-replicated configuration */
			*replicated = B_FALSE;
	}

	if (got_replicated == B_TRUE) {
		/* It is replicated configuration */
		*replicated = B_TRUE;
		*repl_type = strdup(dg_repl_type);
	}

	return (rstatus);
}

/*
 * scconf_get_repl_property
 *
 *	Searches the property list and returns the value of
 *	property SCCONF_DS_PROP_REPLICATED_DEVICE. If such
 *	property does not exists, NULL is returned.
 */
char *
scconf_get_repl_property(scconf_cfg_prop_t *dg_property_list)
{
	scconf_cfg_prop_t *prop;
	char *repl_prop_val = NULL;

	/* Args check */
	if (dg_property_list == NULL)
		return (NULL);

	for (prop = dg_property_list; prop;
	    prop = prop->scconf_prop_next) {
		if (prop->scconf_prop_key &&
		    strcmp(SCCONF_DS_PROP_REPLICATED_DEVICE,
		    prop->scconf_prop_key) == 0) {
			repl_prop_val = prop->scconf_prop_value;
			break;
		}
	}

	return (repl_prop_val);
}
