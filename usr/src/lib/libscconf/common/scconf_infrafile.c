/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scconf_infrafile.c	1.27	09/01/16 SMI"

/*
 * libscconf functions for creating a new infrastructure CCR table as a file
 */

#include "scconf_private.h"

/* Cluster */
#define	CLUSTER_NAME		"cluster.name"
#define	CLUSTER_STATE		"cluster.state"
#define	CLUSTER_PROPS		"cluster.properties"

/* Node */
#define	CLUSTER_NODE_NAME	"cluster.nodes.%d.name"
#define	CLUSTER_NODE_STATE	"cluster.nodes.%d.state"
#define	CLUSTER_NODE_PROPS	"cluster.nodes.%d.properties"

/* Adapter */
#define	CLUSTER_NODE_ADAPTER_NAME "cluster.nodes.%d.adapters.%d.name"
#define	CLUSTER_NODE_ADAPTER_STATE "cluster.nodes.%d.adapters.%d.state"
#define	CLUSTER_NODE_ADAPTER_PROPS "cluster.nodes.%d.adapters.%d.properties"

/* Adapter Ports */
#define	ADAPTER_PORT_NAME "cluster.nodes.%d.adapters.%d.ports.%d.name"
#define	ADAPTER_PORT_STATE "cluster.nodes.%d.adapters.%d.ports.%d.state"

/* Cpoint */
#define	CLUSTER_CPOINT_NAME	"cluster.blackboxes.%d.name"
#define	CLUSTER_CPOINT_STATE	"cluster.blackboxes.%d.state"
#define	CLUSTER_CPOINT_PROPS	"cluster.blackboxes.%d.properties"

/* Cpoint Ports */
#define	CPOINT_PORT_NAME	"cluster.blackboxes.%d.ports.%d.name"
#define	CPOINT_PORT_STATE	"cluster.blackboxes.%d.ports.%d.state"

/* Cables */
#define	CLUSTER_CABLE_STATE	"cluster.cables.%d.state"
#define	CLUSTER_CABLE_EPOINT	"cluster.cables.%d.properties.end%d"

#define	PROP_CABLE_ADAP_EPOINT	"cluster.nodes.%d.adapters.%d.ports.%d"
#define	PROP_CABLE_CPOINT_EPOINT "cluster.blackboxes.%d.ports.%d"

static scconf_errno_t conf_clconfig_get_endpoints(scconf_cfg_cable_t *cl_cable,
    char *estr1, char *estr2, scconf_cfg_cluster_t *clconfig);
static scconf_errno_t conf_clconfig_add_adapterprops(
    scconf_cfg_cluster_t *clconfig, FILE *fp,
    char *nodename, scconf_nodeid_t nodeid, char *adaptername, int adapterid);
static scconf_errno_t conf_clconfig_add_adapterports(
    scconf_cfg_cluster_t *clconfig, FILE *fp,
    char *nodename, scconf_nodeid_t nodeid, char *adaptername,
    char *short_adpname, int adapterid);
static scconf_errno_t conf_clconfig_add_cpointprops(
    scconf_cfg_cluster_t *clconfig, FILE *fp, char *cpointname, int cpointid);
static scconf_errno_t conf_clconfig_add_cpointports(
    scconf_cfg_cluster_t *clconfig, FILE *fp, char *cpointname, int cpointid);
static char *conf_clconfig_generate_default_portname(
    scconf_cfg_cluster_t *clconfig, scconf_cltr_epoint_t *epoint1,
    scconf_cltr_epoint_t *epoint2);
static scconf_nodeid_t conf_clconfig_get_nodeid(scconf_cfg_cluster_t *clconfig,
    char *nodename);
static int conf_clconfig_get_adapterid(scconf_cfg_cluster_t *clconfig,
    char *nodename, char *adaptername);
static int conf_clconfig_get_cpointid(scconf_cfg_cluster_t *clconfig,
    char *cpointname);

/*
 * scconf_createfile_infrastructure
 *
 * Create a cluster infrastructure ccr file from the given clconfig
 * structure.
 *
 * Only the following fields are inspected:
 *
 *	scconf_cfg_cluster_t:
 *	    scconf_cluster_clustername
 *	    scconf_cluster_privatenetaddr
 *	    scconf_cluster_privatenetmask
 *	    scconf_cluster_authtype
 *	    scconf_cluster_authlist
 *	    scconf_cluster_nodelist
 *	    scconf_cluster_cpointlist
 *	    scconf_cluster_cablelist
 *	    scconf_cluster_hb_timeout
 *	    scconf_cluster_hb_quantum
 *	    scconf_cluster_udp_timeout
 *	    scconf_cluster_clusternetmask
 *
 *  START if Weak Membership Feature is enabled
 *          scconf_cluster_multi_partitions
 *	    scconf_cluster_ping_targets
 *  END if Weak Membership Feature is enabled
 *
 *	    scconf_cfg_node_t:
 *		scconf_node_nodename
 *		scconf_node_privatehostname
 *		scconf_node_adapterlist
 *		scconf_node_next
 *
 *		scconf_cfg_adap_t:
 *		    scconf_adap_adaptername
 *		    scconf_adap_cltrtype
 *		    scconf_adap_propertylist
 *		    scconf_adap_next
 *
 *		    scconf_cfg_prop_t:
 *			scconf_prop_key
 *			scconf_prop_value
 *			scconf_prop_next
 *
 *	    scconf_cfg_cpoint_t:
 *		scconf_cpoint_cpointname
 *		scconf_cpoint_type
 *		scconf_cpoint_propertylist
 *		scconf_cpoint_next
 *
 *		scconf_cfg_prop_t:
 *		    scconf_prop_key
 *		    scconf_prop_value
 *		    scconf_prop_next
 *
 *	    scconf_cfg_cable_t:
 *		scconf_cable_epoint1
 *		scconf_cable_epoint2
 *
 *		scconf_cltr_epoint_t:
 *		    scconf_cltr_epoint_type
 *		    scconf_cltr_epoint_nodename
 *		    scconf_cltr_epoint_devicename
 *		    scconf_cltr_epoint_portname
 *
 *	If a bad field is found, SCCONF_EINVAL is returned.
 *	scconf_cltr_epoint_portid fields may be set by this function.
 *	All other fields not in this list are ignored.
 *
 *	This function uses "scconf_clconfig_infrastructure()" to check
 *	the sanity of "clconfig".   But, typically, the caller will actually
 *	construct "clconfig" in an iterative fashion, calling
 *	"scconf_clconfig_infrastructure()" after each set of changes.
 *	Otherwise, it is difficult to determine the source of an
 *	SCCONF_EINVAL error.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EPERM		- not root or permission denied
 *	SCCONF_EINVAL		- invalid argument (or item(s) in clconfig)
 *	SCCONF_EUNKOWN		- unknown transport, adapter, or switch type
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EEXIST		- the file already exists
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 *	SCCONF_TM_EBADOPTS	- bad "properties"
 */
scconf_errno_t
scconf_createfile_infrastructure(char *filename, scconf_cfg_cluster_t *clconfig)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	int fd;
	FILE *fp = NULL;
	char *clustername;
	char clusterid[SCCONF_CLUSTERID_BUFSIZ];
	char privatehostname[SYS_NMLN];
	char reskey[SCCONF_RESKEY_BUFSIZ];
	char *ptr;
	scconf_namelist_t *inamelist;
	scconf_cltr_epoint_t *e, *e1, *e2;
	scconf_cfg_node_t *cluster_node;
	scconf_cfg_cltr_adap_t *cluster_adap;
	scconf_cfg_cpoint_t *cluster_cpoint;
	scconf_cfg_cable_t *cluster_cable;
	int adapterid, cpointid, cableid, eid;
	scconf_nodeid_t nodeid;
	uint_t portid;
	char buffer[BUFSIZ], buffer2[BUFSIZ];
	char device_name[BUFSIZ];
	char device_instance[BUFSIZ];
	char short_adpname[BUFSIZ];
	size_t len;
	int id;
	int i;
	div_t inst_vlan;
	int tmp_instance = 0;

	/* Must be root */
	if (getuid() != 0)
		return (SCCONF_EPERM);

	/* Check arguments */
	if (filename == NULL || clconfig == NULL)
		return (SCCONF_EINVAL);

	/* Check "clconfig" */
	rstatus = scconf_clconfig_infrastructure(clconfig);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);

	/* Create the file */
	if ((fd = open(filename, O_CREAT | O_EXCL | O_RDWR, 0600)) < 0) {
		switch (errno) { /*lint !e746 */
		case EEXIST:
			return (SCCONF_EEXIST);

		case EACCES:
			return (SCCONF_EPERM);

		default:
			return (SCCONF_EINVAL);
		}
	}

	/* Create the file pointer */
	if ((fp = fdopen(fd, "w+")) == NULL) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Reset all portids to zero */
	for (cluster_cable = clconfig->scconf_cluster_cablelist;
	    cluster_cable;  cluster_cable = cluster_cable->scconf_cable_next) {
		e1 = &cluster_cable->scconf_cable_epoint1;
		e2 = &cluster_cable->scconf_cable_epoint2;
		if (e1)
			e1->scconf_cltr_epoint_portid = 0;
		if (e2)
			e2->scconf_cltr_epoint_portid = 0;
	}

	/*
	 * Add the cluster name
	 */

	/* Default cluster name is the name of the first node in nodelist */
	if (clconfig->scconf_cluster_clustername) {
		clustername = clconfig->scconf_cluster_clustername;
	} else if (clconfig->scconf_cluster_nodelist &&
	    clconfig->scconf_cluster_nodelist->scconf_node_nodename) {
		clustername =
		    clconfig->scconf_cluster_nodelist->scconf_node_nodename;
	} else {
		clustername = NULL;
	}

	if (clustername != NULL) {
		if (fprintf(fp, "%s\t%s\n", CLUSTER_NAME,
		    clustername) == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/*
	 * Add the cluster state
	 */
	if (fprintf(fp, "%s\t%s\n", CLUSTER_STATE, "enabled") == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/*
	 * Add the cluster ID
	 */

	/* Form a new cluster ID */
	if (scconf_form_clusterid(clusterid) != SCCONF_NOERR) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Add the cluster ID to the file */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS, PROP_CLUSTER_ID,
	    clusterid) == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Set installmode to "enabled" */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS, PROP_CLUSTER_INSTALLMODE,
	    "enabled") == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/*
	 * Add the cluster netnumber
	 */

	/* If not set, use default */
	if (clconfig->scconf_cluster_privnetaddr) {
		ptr = clconfig->scconf_cluster_privnetaddr;
	} else {
		ptr = SCCONF_DEFAULT_NETNUMBER_STRING;
	}

	/* Add the cluster netnumber to the file */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
	    PROP_CLUSTER_NETADDR, ptr) == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/*
	 * Add the cluster netmask, this is the overall
	 * cluster netmask which includes the virtual
	 * cluster networks and physical cluster networks
	 */
	/* If not set, use default */
	if (clconfig->scconf_cluster_clusternetmask) {
		ptr = clconfig->scconf_cluster_clusternetmask;
	} else {
		ptr = SCCONF_DEFAULT_NETMASK_STRING;
	}

	/* Add the cluster netmask to the file */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
	    PROP_CLUSTER_CLUSTERNETMASK, ptr) == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Add the physical cluster netmask */
	/* If not set, use default */
	if (clconfig->scconf_cluster_privnetmask) {
		ptr = clconfig->scconf_cluster_privnetmask;
	} else {
		ptr = SCCONF_DEFAULT_NETMASK_STRING;
	}

	/* Add the cluster netmask to the file */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
	    PROP_CLUSTER_NETMASK, ptr) == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Add the cluster subnet netmask */

	/* If not set, use default */
	if (clconfig->scconf_cluster_privsubnetmask) {
		ptr = clconfig->scconf_cluster_privsubnetmask;
	} else {
		ptr = SCCONF_DEFAULT_SUBNETMASK_STRING;
	}

	/* Add the cluster subnet netmask to the file */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
	    PROP_CLUSTER_SUBNETMASK, ptr) == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Add the cluster pernode network number */

	/* If not set, use default */
	if (clconfig->scconf_cluster_privusernetnum) {
		ptr = clconfig->scconf_cluster_privusernetnum;
	} else {
		ptr = SCCONF_DEFAULT_USER_NET_NUM_STRING;
	}

	/* Add the pernode net number to the file */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
	    PROP_CLUSTER_USER_NET_NUM, ptr) == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Add the pernode netmask */

	/* If not set, use default */
	if (clconfig->scconf_cluster_privusernetmask) {
		ptr = clconfig->scconf_cluster_privusernetmask;
	} else {
		ptr = SCCONF_DEFAULT_USER_NETMASK_STRING;
	}

	/* Add the pernode netmask to the file */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
	    PROP_CLUSTER_USER_NETMASK, ptr) == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Add the maximum number of nodes */

	/* If not set, use default */
	if (clconfig->scconf_cluster_maxnodes) {
		ptr = clconfig->scconf_cluster_maxnodes;
	} else {
		ptr = SCCONF_DEFAULT_MAXNODES_STRING;
	}

	/* Add the max nodes to the file */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
	    PROP_CLUSTER_MAXNODES, ptr) == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Add the maximum number of private networks */

	/* If not set, use default */
	if (clconfig->scconf_cluster_maxprivnets) {
		ptr = clconfig->scconf_cluster_maxprivnets;
	} else {
		ptr = SCCONF_DEFAULT_MAXPRIVNETS_STRING;
	}

	/* Add the max nodes to the file */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
	    PROP_CLUSTER_MAXPRIVATENETS, ptr) == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Add the number of zone cluster value */

	/* If not set, use default */
	if (clconfig->scconf_cluster_zoneclusters) {
		ptr = clconfig->scconf_cluster_zoneclusters;
	} else {
		ptr = SCCONF_DEFAULT_ZONECLUSTERS_STRING;
	}

	/* Add the max nodes to the file */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
		PROP_CLUSTER_ZONECLUSTERS, ptr) == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/*
	 * Add the authentication type
	 */

	/* Convert the authentication type to a string */
	switch (clconfig->scconf_cluster_authtype) {
	case SCCONF_AUTH_DES:
		ptr = "des";
		break;

	case SCCONF_AUTH_SYS:
	default:
		ptr = "sys";
		break;
	}

	/* Add the authentication type to the file */
	if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
	    PROP_CLUSTER_AUTHTYPE, ptr) == EOF) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/*
	 * Add the authentication node list
	 */

	inamelist = clconfig->scconf_cluster_authlist;
	if (inamelist != NULL) {

		/* Print the key */
		if (fprintf(fp, "%s.%s\t", CLUSTER_PROPS,
		    PROP_CLUSTER_AUTHJOINLIST) == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Print each name in a comma seperated list */
		i = 0;
		while (inamelist && inamelist->scconf_namelist_name) {
			if (i) {
				if (putc(',', fp) == EOF) {
					rstatus = SCCONF_EUNEXPECTED;
					goto cleanup;
				}
			}
			++i;
			if (fprintf(fp, inamelist->scconf_namelist_name)
			    == EOF) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			inamelist = inamelist->scconf_namelist_next;
		}
		if (fprintf(fp, "\n") == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/*
	 * Add global heart beat
	 */
	if (clconfig->scconf_cluster_hb_timeout) {
		if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
		    PROP_CLUSTER_HB_TIMEOUT,
		    clconfig->scconf_cluster_hb_timeout) == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}
	if (clconfig->scconf_cluster_hb_quantum) {
		if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
		    PROP_CLUSTER_HB_QUANTUM,
		    clconfig->scconf_cluster_hb_quantum) == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	(void) sprintf(buffer, "%d", UDP_SESSION_TIMEOUT_DFLT);
	clconfig->scconf_cluster_udp_timeout = strdup(buffer);
	if (clconfig->scconf_cluster_udp_timeout) {
		if (fprintf(fp, "%s.%s\t%s\n", CLUSTER_PROPS,
		    PROP_CLUSTER_UDP_TIMEOUT,
		    clconfig->scconf_cluster_udp_timeout) == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}
	}

	/*
	 * Add nodes, and their adapters
	 */

	/* Nodes */
	nodeid = 1;
	for (cluster_node = clconfig->scconf_cluster_nodelist;  cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {

		/* Add nodename */
		if (cluster_node->scconf_node_nodename == NULL) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}
		(void) sprintf(buffer, CLUSTER_NODE_NAME, nodeid);
		if (fprintf(fp, "%s\t%s\n", buffer,
		    cluster_node->scconf_node_nodename) == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Add nodestate - only node 1 is enabled */
		(void) sprintf(buffer, CLUSTER_NODE_STATE, nodeid);
		if (fprintf(fp, "%s\t%s\n", buffer,
		    (nodeid == 1) ? "enabled" : "disabled") == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Add private hostname */
		if (cluster_node->scconf_node_privatehostname) {
			ptr = cluster_node->scconf_node_privatehostname;
		} else {
			rstatus = scconf_form_default_privatehostname(
			    nodeid, privatehostname);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			ptr = privatehostname;
		}
		(void) sprintf(buffer, CLUSTER_NODE_PROPS, nodeid);
		if (fprintf(fp, "%s.%s\t%s\n", buffer,
		    PROP_NODE_PRIVATE_HOSTNAME, ptr) == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Add quorum vote count - 1 for node 1, 0 for all others */
		(void) sprintf(buffer, CLUSTER_NODE_PROPS, nodeid);
		if (fprintf(fp, "%s.%s\t%s\n", buffer,
		    PROP_NODE_QUORUM_VOTE,
		    (nodeid == 1) ? "1" : "0") == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Form the disk reservation key, and add it to the file */
		id = (int)strtol(clusterid, (char **)0, 0);
		rstatus = conf_form_reskey(id, nodeid, reskey);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;
		(void) sprintf(buffer, CLUSTER_NODE_PROPS, nodeid);
		if (fprintf(fp, "%s.%s\t%s\n", buffer,
		    PROP_NODE_QUORUM_RESV_KEY, reskey) == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Adapters */
		adapterid = 1;
		for (cluster_adap = cluster_node->scconf_node_adapterlist;
		    cluster_adap;
		    cluster_adap = cluster_adap->scconf_adap_next) {

			/* Add adaptername */
			if (cluster_adap->scconf_adap_adaptername == NULL) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
			(void) sprintf(buffer, CLUSTER_NODE_ADAPTER_NAME,
			    nodeid, adapterid);
			if (fprintf(fp, "%s\t%s\n", buffer,
			    cluster_adap->scconf_adap_adaptername) == EOF) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* Add adapter state */
			(void) sprintf(buffer, CLUSTER_NODE_ADAPTER_STATE,
			    nodeid, adapterid);
			if (fprintf(fp, "%s\t%s\n", buffer,
			    "disabled") == EOF) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* Split the adaptername into its two components */
			len = strlen(cluster_adap->scconf_adap_adaptername) + 1;
			if (len > sizeof (device_name) ||
			    len > sizeof (device_instance)) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			rstatus = conf_split_adaptername(
			    cluster_adap->scconf_adap_adaptername,
			    device_name, device_instance);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

			tmp_instance = atoi(device_instance);
			if (tmp_instance >= VLAN_ZONE_START) {
				/* Implicit vlan device, deduce instance. */
				inst_vlan = div(tmp_instance, VLAN_MULTIPLIER);
				tmp_instance = inst_vlan.rem;
			}
			(void) sprintf(short_adpname, "%s%d", device_name,
			    tmp_instance);

			/* Add device name and instance */
			(void) sprintf(buffer, CLUSTER_NODE_ADAPTER_PROPS,
			    nodeid, adapterid);
			if (fprintf(fp, "%s.%s\t%s\n", buffer,
			    PROP_ADAPTER_DEVICE_NAME, device_name) == EOF) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
			if (fprintf(fp, "%s.%s\t%d\n", buffer,
			    PROP_ADAPTER_DEVICE_INSTANCE,
			    tmp_instance) == EOF) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* Add transport type */
			if (cluster_adap->scconf_adap_cltrtype == NULL) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
			(void) sprintf(buffer, CLUSTER_NODE_ADAPTER_PROPS,
			    nodeid, adapterid);
			if (fprintf(fp, "%s.%s\t%s\n", buffer,
			    PROP_ADAPTER_TRANSPORT_TYPE,
			    cluster_adap->scconf_adap_cltrtype) == EOF) {
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}

			/* Add adapter properties */
			rstatus = conf_clconfig_add_adapterprops(clconfig, fp,
			    cluster_node->scconf_node_nodename, nodeid,
			    cluster_adap->scconf_adap_adaptername, adapterid);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

			/* Add adapter ports */
			rstatus = conf_clconfig_add_adapterports(
			    clconfig, fp,
			    cluster_node->scconf_node_nodename, nodeid,
			    cluster_adap->scconf_adap_adaptername,
			    short_adpname, adapterid);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;

			/* Next adapter */
			++adapterid;
		}

		/* Next node */
		++nodeid;
	}

	/*
	 * Add off-node connection points
	 */

	/* Cpoints */
	cpointid = 1;
	for (cluster_cpoint = clconfig->scconf_cluster_cpointlist;
	    cluster_cpoint;
	    cluster_cpoint = cluster_cpoint->scconf_cpoint_next) {

		/* Add cpoint name */
		if (cluster_cpoint->scconf_cpoint_cpointname == NULL) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}
		(void) sprintf(buffer, CLUSTER_CPOINT_NAME, cpointid);
		if (fprintf(fp, "%s\t%s\n", buffer,
		    cluster_cpoint->scconf_cpoint_cpointname) == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Set state to disabled for all cpoints */
		(void) sprintf(buffer, CLUSTER_CPOINT_STATE, cpointid);
		if (fprintf(fp, "%s\t%s\n", buffer, "disabled") == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Set the type */
		if (cluster_cpoint->scconf_cpoint_type == NULL) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}
		(void) sprintf(buffer, CLUSTER_CPOINT_PROPS, cpointid);
		if (fprintf(fp, "%s.%s\t%s\n", buffer, PROP_CPOINT_TYPE,
		    cluster_cpoint->scconf_cpoint_type) == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Add cpoint properties */
		rstatus = conf_clconfig_add_cpointprops(clconfig, fp,
		    cluster_cpoint->scconf_cpoint_cpointname, cpointid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Add cpoint ports */
		rstatus = conf_clconfig_add_cpointports(clconfig, fp,
		    cluster_cpoint->scconf_cpoint_cpointname, cpointid);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Next cpoint */
		++cpointid;
	}

	/*
	 * Add cables
	 */
	cableid = 1;
	for (cluster_cable = clconfig->scconf_cluster_cablelist; cluster_cable;
	    cluster_cable = cluster_cable->scconf_cable_next) {
		e = &cluster_cable->scconf_cable_epoint1;
		for (eid = 1;  eid < 3;  ++eid) {
			if (e == NULL ||
			    e->scconf_cltr_epoint_devicename == NULL) {
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}

			switch (e->scconf_cltr_epoint_type) {
			case SCCONF_CLTR_EPOINT_TYPE_ADAPTER:
				if (e->scconf_cltr_epoint_nodename == NULL) {
					rstatus = SCCONF_EINVAL;
					goto cleanup;
				}
				nodeid = conf_clconfig_get_nodeid(clconfig,
				    e->scconf_cltr_epoint_nodename);
				adapterid = conf_clconfig_get_adapterid(
				    clconfig,
				    e->scconf_cltr_epoint_nodename,
				    e->scconf_cltr_epoint_devicename);
				portid = e->scconf_cltr_epoint_portid;
				if (nodeid < 1 || adapterid < 1 || portid < 1) {
					rstatus = SCCONF_EINVAL;
					goto cleanup;
				}

				(void) sprintf(buffer, CLUSTER_CABLE_EPOINT,
				    cableid, eid);
				(void) sprintf(buffer2, PROP_CABLE_ADAP_EPOINT,
				    nodeid, adapterid, portid);
				if (fprintf(fp, "%s\t%s\n", buffer, buffer2)
				    == EOF) {
					rstatus = SCCONF_EINVAL;
					goto cleanup;
				}
				break;

			case SCCONF_CLTR_EPOINT_TYPE_CPOINT:
				cpointid = conf_clconfig_get_cpointid(clconfig,
				    e->scconf_cltr_epoint_devicename);
				portid = e->scconf_cltr_epoint_portid;
				if (cpointid < 1 || portid < 1) {
					rstatus = SCCONF_EINVAL;
					goto cleanup;
				}

				(void) sprintf(buffer, CLUSTER_CABLE_EPOINT,
				    cableid, eid);
				(void) sprintf(buffer2,
				    PROP_CABLE_CPOINT_EPOINT, cpointid, portid);
				if (fprintf(fp, "%s\t%s\n", buffer, buffer2)
				    == EOF) {
					rstatus = SCCONF_EINVAL;
					goto cleanup;
				}
				break;

			default:
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}

			/* Other endpoint */
			e = &cluster_cable->scconf_cable_epoint2;
		}

		/* Set cable state to disabled */
		(void) sprintf(buffer, CLUSTER_CABLE_STATE, cableid);
		if (fprintf(fp, "%s\t%s\n", buffer, "disabled") == EOF) {
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		}

		/* Next cable */
		++cableid;
	}

cleanup:
	if (fp != NULL) {
		(void) fclose(fp);
		(void) close(fd);
	}

	if (filename && rstatus != SCCONF_NOERR && rstatus != SCCONF_EEXIST)
		(void) unlink(filename);

	return (rstatus);
}

/*
 * scconf_clconfig_infrastructure
 *
 * Check the sanity of the given "clconfig" for use with
 * scconf_createfile_infrastructure().
 *
 * See the comments for scconf_createfile_infrastructure for the list
 * of "clconfig" fields which are used.
 *
 * If a bad field is found, SCCONF_EINVAL is returned.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EEXIST		- item in clconfig already exists
 *	SCCONF_EINVAL		- invalid argument (or item(s) in clconfig)
 *	SCCONF_EUNKOWN		- unknown transport, adapter, or switch type
 *	SCCONF_EINUSE		- cable port already in use
 *	SCCONF_EUNEXPECTED	- inernal error
 *	SCCONF_TM_EBADOPTS	- bad "properties"
 */
scconf_errno_t
scconf_clconfig_infrastructure(scconf_cfg_cluster_t *clconfig)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	ulong_t netaddr, netmask, clusternetmask;
	scconf_cfg_node_t *cl_node1, *cl_node2;
	scconf_cfg_cltr_adap_t *cl_adap1, *cl_adap2;
	scconf_cfg_cpoint_t *cl_cpoint1, *cl_cpoint2;
	scconf_cfg_cable_t *cl_cable1, *cl_cable2;
	scconf_cltr_propdes_t *propdeslist;
	scconf_cltr_propdes_t *propdes;
	scconf_cfg_prop_t *prop;
	scconf_namelist_t *inamelist;
	char *ptr, *laststrtok;
	scconf_nodeid_t nodeid;
	char *phostname1, *phostname2;
	char phostbuff1[SYS_NMLN];
	char phostbuff2[SYS_NMLN];
	struct cable {
		char estr1[BUFSIZ];
		char estr2[BUFSIZ];
	};
	struct cable c1, c2;
	char *hb_timeout = (char *)0;
	char *hb_quantum = (char *)0;
	char p_timeout[SCCONF_HB_BUFSIZ];
	char p_quantum[SCCONF_HB_BUFSIZ];

	/* Get the private netaddr */
	if (clconfig->scconf_cluster_privnetaddr) {
		netaddr = inet_network(clconfig->scconf_cluster_privnetaddr);
		if (netaddr == (ulong_t)-1)
			return (SCCONF_EINVAL);
	} else {
		netaddr = 0;
	}

	/* Get the private netmask */
	if (clconfig->scconf_cluster_privnetmask) {
		netmask = inet_network(clconfig->scconf_cluster_privnetmask);
		if (netmask == (ulong_t)-1)
			return (SCCONF_EINVAL);
	} else {
		netmask = 0;
	}

	/* Get the cluster netmask */
	if (clconfig->scconf_cluster_clusternetmask) {
		clusternetmask =
			inet_network(clconfig->scconf_cluster_clusternetmask);
		if (clusternetmask == (ulong_t)-1)
			return (SCCONF_EINVAL);
	} else {
		clusternetmask = 0;
	}

	/* Check the netaddr and netmask */
	rstatus = scconf_check_netaddr(netaddr, clusternetmask);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);

	/* If authentication list is set, only allow one "." */
	inamelist = clconfig->scconf_cluster_authlist;
	if (inamelist != NULL) {
		if (inamelist->scconf_namelist_name == NULL)
			return (SCCONF_EINVAL);
		if (strcmp(inamelist->scconf_namelist_name, ".") == 0 &&
		    inamelist->scconf_namelist_next != NULL)
			return (SCCONF_EINVAL);
		for (inamelist = inamelist->scconf_namelist_next;  inamelist;
		    inamelist = inamelist->scconf_namelist_next) {
			if (inamelist->scconf_namelist_name == NULL ||
			    strcmp(inamelist->scconf_namelist_name, ".") == 0)
				return (SCCONF_EINVAL);
		}
	}

	/* Make sure names and private hostnames are unique */
	for (cl_node1 = clconfig->scconf_cluster_nodelist;  cl_node1;
	    cl_node1 = cl_node1->scconf_node_next) {

		/* Make sure there is a node name */
		if (cl_node1->scconf_node_nodename == NULL)
			return (SCCONF_EINVAL);

		/* Set private hostname1 */
		if (cl_node1->scconf_node_privatehostname) {
			phostname1 = cl_node1->scconf_node_privatehostname;
		} else {
			nodeid = conf_clconfig_get_nodeid(clconfig,
			    cl_node1->scconf_node_nodename);
			if (nodeid == 0)
				return (SCCONF_EINVAL);
			rstatus = scconf_form_default_privatehostname(
			    nodeid, phostbuff1);
			if (rstatus != SCCONF_NOERR)
				return (rstatus);
			phostname1 = phostbuff1;
		}

		/* Iterate through remaining list */
		for (cl_node2 = cl_node1->scconf_node_next;  cl_node2;
		    cl_node2 = cl_node2->scconf_node_next) {

			/* Make sure there is a node name */
			if (cl_node2->scconf_node_nodename == NULL)
				return (SCCONF_EINVAL);

			/* Compare node names */
			if (strcmp(cl_node1->scconf_node_nodename,
			    cl_node2->scconf_node_nodename) == 0)
				return (SCCONF_EEXIST);

			/* Set private hostname2 */
			if (cl_node2->scconf_node_privatehostname) {
				phostname2 =
				    cl_node2->scconf_node_privatehostname;
			} else {
				nodeid = conf_clconfig_get_nodeid(clconfig,
				    cl_node2->scconf_node_nodename);
				if (nodeid == 0)
					return (SCCONF_EINVAL);
				rstatus = scconf_form_default_privatehostname(
				    nodeid, phostbuff2);
				if (rstatus != SCCONF_NOERR)
					return (rstatus);
				phostname2 = phostbuff2;
			}

			/* Compare private hostnames */
			if (strcmp(phostname1, phostname2) == 0)
				return (SCCONF_EEXIST);
		}
	}

	/* For each adapter on each node, check adapter names and properties */
	for (cl_node1 = clconfig->scconf_cluster_nodelist;  cl_node1;
	    cl_node1 = cl_node1->scconf_node_next) {

		/* Adapters */
		for (cl_adap1 = cl_node1->scconf_node_adapterlist;  cl_adap1;
		    cl_adap1 = cl_adap1->scconf_adap_next) {

			/* Make sure there is an adapter name and trtype */
			if (cl_adap1->scconf_adap_adaptername == NULL ||
			    cl_adap1->scconf_adap_cltrtype == NULL)
				return (SCCONF_EINVAL);

			/* Get the properties description list */
			propdeslist = (scconf_cltr_propdes_t *)0;
			rstatus = scconf_get_cltr_adapter_propdeslist(
			    cl_adap1->scconf_adap_adaptername, &propdeslist);
			if (rstatus != SCCONF_NOERR) {
				scconf_free_propdeslist(propdeslist);
				return (rstatus);
			}

			/* Make sure adapter names are unique for each node */
			for (cl_adap2 = cl_adap1->scconf_adap_next;  cl_adap2;
			    cl_adap2 = cl_adap2->scconf_adap_next) {

				/* Make sure there is an adapter name */
				if (cl_adap2->scconf_adap_adaptername == NULL) {
					scconf_free_propdeslist(propdeslist);
					return (SCCONF_EINVAL);
				}

				/* Compare adapter names */
				if (strcmp(cl_adap1->scconf_adap_adaptername,
				    cl_adap2->scconf_adap_adaptername) == 0) {
					scconf_free_propdeslist(propdeslist);
					return (SCCONF_EEXIST);
				}
			}

			/* Make sure the transport type is valid */
			ptr = NULL;
			for (propdes = propdeslist;  propdes;
			    propdes = propdes->scconf_cltr_propdes_next) {
				if (propdes->scconf_cltr_propdes_name == NULL ||
				    propdes->scconf_cltr_propdes_enumlist ==
				    NULL)
					continue;
				if (strcmp(propdes->scconf_cltr_propdes_name,
				    PROP_ADAPTER_TRANSPORT_TYPE) != 0)
					continue;
				ptr = propdes->scconf_cltr_propdes_enumlist;
				ptr = strtok_r(ptr, ":", &laststrtok);
				while (ptr != NULL) {
					if (strcmp(ptr,
					    cl_adap1->scconf_adap_cltrtype)
					    == 0)
						break;
					ptr = strtok_r(NULL, ":", &laststrtok);
				}
			}
			if (ptr == NULL) {
				scconf_free_propdeslist(propdeslist);
				return (SCCONF_EUNKNOWN);
			}

			/* Get the default global heart beat settings */
			rstatus = scconf_get_legal_hb(
			    cl_adap1->scconf_adap_cltrtype, propdeslist,
			    &hb_timeout, B_TRUE, &hb_quantum, B_TRUE);
			if (rstatus != SCCONF_NOERR) {
				scconf_free_propdeslist(propdeslist);
				return (rstatus);
			}

			/* make sure all required properties are included */
			for (propdes = propdeslist;  propdes;
			    propdes = propdes->scconf_cltr_propdes_next) {
				if (propdes->scconf_cltr_propdes_name == NULL ||
				    propdes->scconf_cltr_propdes_required == 0)
					continue;
				if (strcmp(propdes->scconf_cltr_propdes_name,
				    PROP_ADAPTER_TRANSPORT_TYPE) == 0)
					continue;
				/* Needs cleanup up as per bug ID 4257425 */
				if (strcmp(propdes->scconf_cltr_propdes_name,
				    PROP_ADAPTER_DEVICE_NAME) == 0)
					continue;
				for (prop = cl_adap1->scconf_adap_propertylist;
				    prop;  prop = prop->scconf_prop_next) {
					if (prop->scconf_prop_key == NULL)
						continue;
					if (strcmp(prop->scconf_prop_key,
					    propdes->scconf_cltr_propdes_name)
					    == 0)
						break;
				}
				if (prop == (scconf_cfg_prop_t *)0) {
					scconf_free_propdeslist(propdeslist);
					return (SCCONF_TM_EBADOPTS);
				}
			}

			/*
			 * If property not required, and there is default,
			 * use it. The default values are entered into
			 * the adapter's property list.
			 */
			if ((rstatus = conf_use_adapter_default_properties(
			    propdeslist,
			    &cl_adap1->scconf_adap_propertylist))
			    != SCCONF_NOERR) {
				scconf_free_propdeslist(propdeslist);
				return (rstatus);
			}

			/* Free the prop description list for this adapter */
			scconf_free_propdeslist(propdeslist);
		}
	}

	/*
	 * For each adapter on each node, set the heart beat properties.
	 * Also check its properties
	 */
	for (cl_node1 = clconfig->scconf_cluster_nodelist;  cl_node1;
	    cl_node1 = cl_node1->scconf_node_next) {

		/* Adapters */
		for (cl_adap1 = cl_node1->scconf_node_adapterlist;  cl_adap1;
		    cl_adap1 = cl_adap1->scconf_adap_next) {

			/* Get the property strings */
			(void) sprintf(p_timeout, "%s_%s",
			    cl_adap1->scconf_adap_cltrtype,
			    PROP_ADAPTER_HEARTBEAT_TIMEOUT);

			(void) sprintf(p_quantum, "%s_%s",
			    cl_adap1->scconf_adap_cltrtype,
			    PROP_ADAPTER_HEARTBEAT_QUANTUM);

			/* Get the properties description list */
			propdeslist = (scconf_cltr_propdes_t *)0;
			rstatus = scconf_get_cltr_adapter_propdeslist(
			    cl_adap1->scconf_adap_adaptername, &propdeslist);
			if (rstatus != SCCONF_NOERR) {
				scconf_free_propdeslist(propdeslist);
				return (rstatus);
			}

			/* Check adapter properties */
			for (prop = cl_adap1->scconf_adap_propertylist;
			    prop; prop = prop->scconf_prop_next) {
				/* Set the heart beat properties */
				if ((strcmp(prop->scconf_prop_key,
				    p_timeout) == 0) &&
				    (hb_timeout != NULL)) {
					(void) strcpy(
					    prop->scconf_prop_value,
					    hb_timeout);
				}

				if ((strcmp(prop->scconf_prop_key,
				    p_quantum) == 0) &&
				    (hb_quantum != NULL)) {
					(void) strcpy(
					    prop->scconf_prop_value,
					    hb_quantum);
				}

				/* Check the properties */
				rstatus = scconf_check_cltr_property(
				    prop->scconf_prop_key,
				    prop->scconf_prop_value,
				    propdeslist);
				if (rstatus != SCCONF_NOERR) {
					scconf_free_propdeslist(propdeslist);
					return (rstatus);
				}
			}

			/* Free the prop description list for this adapter */
			scconf_free_propdeslist(propdeslist);
		}
	}

	/* Set the global heart beat properties */
	if ((hb_timeout != NULL) && (hb_quantum != NULL)) {
		clconfig->scconf_cluster_hb_timeout = hb_timeout;
		clconfig->scconf_cluster_hb_quantum = hb_quantum;
	}

	/* For each cpoint, check cpoint names and properties */
	for (cl_cpoint1 = clconfig->scconf_cluster_cpointlist;  cl_cpoint1;
	    cl_cpoint1 = cl_cpoint1->scconf_cpoint_next) {

		/* Make sure there is a cpoint name and type */
		if (cl_cpoint1->scconf_cpoint_cpointname == NULL ||
		    cl_cpoint1->scconf_cpoint_type == NULL)
			return (SCCONF_EINVAL);

		/* Get the properties description list */
		propdeslist = (scconf_cltr_propdes_t *)0;
		rstatus = scconf_get_cltr_cpoint_propdeslist(
		    cl_cpoint1->scconf_cpoint_type, &propdeslist);
		if (rstatus != SCCONF_NOERR) {
			scconf_free_propdeslist(propdeslist);
			return (rstatus);
		}

		/* Make sure cpoint names are unique */
		for (cl_cpoint2 = cl_cpoint1->scconf_cpoint_next;  cl_cpoint2;
		    cl_cpoint2 = cl_cpoint2->scconf_cpoint_next) {

			/* Make sure there is a cpoint name */
			if (cl_cpoint2->scconf_cpoint_cpointname == NULL)
				return (SCCONF_EINVAL);

			/* Compare cpoint names */
			if (strcmp(cl_cpoint1->scconf_cpoint_cpointname,
			    cl_cpoint2->scconf_cpoint_cpointname) == 0)
				return (SCCONF_EEXIST);
		}

		/* make sure all required properties are included */
		for (propdes = propdeslist;  propdes;
		    propdes = propdes->scconf_cltr_propdes_next) {
			if (propdes->scconf_cltr_propdes_name == NULL ||
			    propdes->scconf_cltr_propdes_required == 0)
				continue;
			if (strcmp(propdes->scconf_cltr_propdes_name,
			    PROP_CPOINT_TYPE) == 0)
				continue;
			for (prop = cl_cpoint1->scconf_cpoint_propertylist;
			    prop; prop = prop->scconf_prop_next) {
				if (prop->scconf_prop_key == NULL)
					continue;
				if (strcmp(prop->scconf_prop_key,
				    propdes->scconf_cltr_propdes_name) == 0)
						break;
			}
			if (prop == (scconf_cfg_prop_t *)0) {
				scconf_free_propdeslist(propdeslist);
				return (SCCONF_TM_EBADOPTS);
			}
		}

		/* Check switch properties */
		for (prop = cl_cpoint1->scconf_cpoint_propertylist;
		    prop; prop = prop->scconf_prop_next) {
			rstatus = scconf_check_cltr_property(
			    prop->scconf_prop_key,
			    prop->scconf_prop_value,
			    propdeslist);
			if (rstatus != SCCONF_NOERR) {
				scconf_free_propdeslist(propdeslist);
				return (rstatus);
			}
		}

		/* Free the prop description list for this adapter */
		scconf_free_propdeslist(propdeslist);
	}

	/* Check cables */
	for (cl_cable1 = clconfig->scconf_cluster_cablelist;  cl_cable1;
	    cl_cable1 = cl_cable1->scconf_cable_next) {

		/* Get endpoints */
		rstatus = conf_clconfig_get_endpoints(cl_cable1,
		    c1.estr1, c1.estr2, clconfig);
		if (rstatus != SCCONF_NOERR)
			return (rstatus);

		/* Make sure both endpoints are not the same */
		if (strcmp(c1.estr1, c1.estr2) == 0)
			return (SCCONF_EINVAL);

		/* Compare endpoints of all cables to avoid duplicates */
		for (cl_cable2 = cl_cable1->scconf_cable_next;  cl_cable2;
		    cl_cable2 = cl_cable2->scconf_cable_next) {

			/* Get endpoints */
			rstatus = conf_clconfig_get_endpoints(cl_cable2,
			    c2.estr1, c2.estr2, clconfig);
			if (rstatus != SCCONF_NOERR)
				return (rstatus);

			/* Make sure both endpoints are not the same */
			if (strcmp(c2.estr1, c2.estr2) == 0)
				return (SCCONF_EINVAL);

			/* Check for duplicate cable */
			if ((strcmp(c1.estr1, c2.estr1) == 0 &&
			    strcmp(c1.estr2, c2.estr2) == 0) ||
			    (strcmp(c1.estr1, c2.estr2) == 0 &&
			    strcmp(c1.estr2, c2.estr1) == 0))
				return (SCCONF_EEXIST);

			/* Check for port in use (other cable) */
			if (strcmp(c1.estr1, c2.estr1) == 0 ||
			    strcmp(c1.estr1, c2.estr2) == 0)
				return (SCCONF_EINUSE);
		}
	}

	return (SCCONF_NOERR);
}

/*
 * conf_clconfig_get_endpoints
 *
 * Copy an endpoint identifier for the given "cl_cable" in each of
 * the two "estr" buffers.   The endpoint identifier takes one of the following
 * two forms:
 *
 *	nodes.<NODE_ID>.adapters.<ADAP_ID>.portname.<PORTNAME>
 *	blackboxes.<BB_ID>.portname.<PORTNAME>
 *
 * If the blackbox "PORTNAME" is unspecified, the "PORTNAME" will be
 * set to "*".
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument (or item(s) in cable)
 *	SCCONF_EUNEXPECTED	- inernal error
 */
static scconf_errno_t
conf_clconfig_get_endpoints(scconf_cfg_cable_t *cl_cable, char *estr1,
    char *estr2, scconf_cfg_cluster_t *clconfig)
{
	scconf_cltr_epoint_t *e, *oe;
	char *estr;
	scconf_nodeid_t nid;
	int did;
	char *portname;

	e = &cl_cable->scconf_cable_epoint1;
	oe = &cl_cable->scconf_cable_epoint2;
	estr = estr1;
	for (;;) {
		/* Make sure devicename is set */
		if (e->scconf_cltr_epoint_devicename == NULL)
			return (SCCONF_EINVAL);

		/* Adapter or Cpoint? */
		switch (e->scconf_cltr_epoint_type) {
		case SCCONF_CLTR_EPOINT_TYPE_ADAPTER:

			/* Make sure nodename is set */
			if (e->scconf_cltr_epoint_nodename == NULL)
				return (SCCONF_EINVAL);

			/* Make sure this is a known node */
			nid = conf_clconfig_get_nodeid(clconfig,
			    e->scconf_cltr_epoint_nodename);
			if (nid == 0)
				return (SCCONF_EINVAL);

			/* Make sure this is a known adapter */
			did = conf_clconfig_get_adapterid(clconfig,
			    e->scconf_cltr_epoint_nodename,
			    e->scconf_cltr_epoint_devicename);
			if (did == 0)
				return (SCCONF_EINVAL);

			/* Set the portname */
			if (e->scconf_cltr_epoint_portname) {
				portname = strdup(
				    e->scconf_cltr_epoint_portname);
				if (portname == NULL)
					return (SCCONF_ENOMEM);
			} else {
				portname =
				    conf_clconfig_generate_default_portname(
				    clconfig, e, oe);
				if (portname == NULL)
					return (SCCONF_EINVAL);
			}

			/* Set identifier */
			(void) sprintf(estr, "nodes.%d.adapters.%d.portname.%s",
			    nid, did, portname);
			free(portname);

			break;

		case SCCONF_CLTR_EPOINT_TYPE_CPOINT:

			/* Make sure this is a known cpoint */
			did = conf_clconfig_get_cpointid(clconfig,
			    e->scconf_cltr_epoint_devicename);
			if (did == 0)
				return (SCCONF_EINVAL);

			/* Set the portname */
			if (e->scconf_cltr_epoint_portname) {
				portname = strdup(
				    e->scconf_cltr_epoint_portname);
				if (portname == NULL)
					return (SCCONF_ENOMEM);
			} else {
				portname =
				    conf_clconfig_generate_default_portname(
				    clconfig, e, oe);
				if (portname == NULL)
					return (SCCONF_EINVAL);
			}

			/* Set identifier.  */
			(void) sprintf(estr,
			    "blackboxes.%d.portname.%s", did, portname);
			free(portname);

			break;

		default:
			return (SCCONF_EINVAL);
		}

		if (e == &cl_cable->scconf_cable_epoint1) {
			e = &cl_cable->scconf_cable_epoint2;
			oe = &cl_cable->scconf_cable_epoint1;
			estr = estr2;
		} else {
			break;
		}
	}

	return (SCCONF_NOERR);
}

/*
 * conf_clconfig_add_adapterprops
 *
 * Add adapter properties found in "clconfig" to the open "fp" file
 * for the given adapter.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument (or item(s) in clconfig)
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_clconfig_add_adapterprops(scconf_cfg_cluster_t *clconfig,
    FILE *fp, char *nodename, scconf_nodeid_t nodeid, char *adaptername,
    int adapterid)
{
	scconf_cfg_node_t *cluster_node;
	scconf_cfg_cltr_adap_t *cluster_adap;
	scconf_cfg_prop_t *prop;
	char buffer[BUFSIZ];

	cluster_adap = (scconf_cfg_cltr_adap_t *)0;

	/* Check arguments */
	if (clconfig == NULL || fp == NULL ||
	    nodename == NULL || adaptername == NULL ||
	    nodeid < 1 || adapterid < 1)
		return (SCCONF_EINVAL);

	/* Search for node-adapter pair */
	for (cluster_node = clconfig->scconf_cluster_nodelist;  cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {

		if (strcmp(cluster_node->scconf_node_nodename, nodename) != 0)
			continue;

		for (cluster_adap = cluster_node->scconf_node_adapterlist;
		    cluster_adap;
		    cluster_adap = cluster_adap->scconf_adap_next) {
			if (strcmp(cluster_adap->scconf_adap_adaptername,
			    adaptername) == 0)
				break;
		}

		if (cluster_adap != NULL)
			break;
	}

	/* Found it? */
	if (cluster_node == NULL)
		return (SCCONF_EUNEXPECTED);

	/* Add properties */
	(void) sprintf(buffer, CLUSTER_NODE_ADAPTER_PROPS, nodeid, adapterid);
	for (prop = cluster_adap->scconf_adap_propertylist;  prop;
	    prop = prop->scconf_prop_next) {

		if (prop->scconf_prop_key == NULL ||
		    prop->scconf_prop_value == NULL)
			return (SCCONF_EINVAL);

		if (fprintf(fp, "%s.%s\t%s\n", buffer,
		    prop->scconf_prop_key, prop->scconf_prop_value) == EOF)
			return (SCCONF_EUNEXPECTED);
	}

	return (SCCONF_NOERR);
}

/*
 * conf_clconfig_add_adapterports
 *
 * Add adapter ports found in the list of cables in "clconfig" to the
 * open "fp" file for the given adapter.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument (or item(s) in clconfig)
 *	SCCONF_ENOMEM		- not enough memory
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_clconfig_add_adapterports(scconf_cfg_cluster_t *clconfig,
    FILE *fp, char *nodename, scconf_nodeid_t nodeid, char *adaptername,
    char *short_adpname, int adapterid)
{
	scconf_cfg_cable_t *cluster_cable;
	scconf_cltr_epoint_t *e, *oe;
	char buffer[BUFSIZ];
	char *nname, *aname;
	char *portname;
	uint_t portid;

	/* Check arguments */
	if (clconfig == NULL || fp == NULL ||
	    nodename == NULL || adaptername == NULL || short_adpname == NULL ||
	    nodeid < 1 || adapterid < 1)
		return (SCCONF_EINVAL);

	/* Walk through the list of cables */
	portid = 1;
	for (cluster_cable = clconfig->scconf_cluster_cablelist; cluster_cable;
	    cluster_cable = cluster_cable->scconf_cable_next) {
		e = &cluster_cable->scconf_cable_epoint1;
		oe = &cluster_cable->scconf_cable_epoint2;
		for (;;) {
			if (e && e->scconf_cltr_epoint_type ==
			    SCCONF_CLTR_EPOINT_TYPE_ADAPTER) {
				nname = e->scconf_cltr_epoint_nodename;
				aname = e->scconf_cltr_epoint_devicename;
				if (nname && aname &&
				    strcmp(nname, nodename) == 0 &&
				    (strcmp(aname, adaptername) == 0 ||
				    strcmp(aname, short_adpname) == 0))
					break;
			}

			if (e == &cluster_cable->scconf_cable_epoint1) {
				e = &cluster_cable->scconf_cable_epoint2;
				oe = &cluster_cable->scconf_cable_epoint1;
			} else {
				e = (scconf_cltr_epoint_t *)0;
				break;
			}
		}
		if (e != (scconf_cltr_epoint_t *)0) {

			/* Set the portname */
			if (e->scconf_cltr_epoint_portname) {
				portname = strdup(
				    e->scconf_cltr_epoint_portname);
				if (portname == NULL)
					return (SCCONF_ENOMEM);
			} else {
				portname =
				    conf_clconfig_generate_default_portname(
				    clconfig, e, oe);
				if (portname == NULL)
					return (SCCONF_EINVAL);
			}

			/* Add the port name */
			(void) sprintf(buffer, ADAPTER_PORT_NAME,
			    nodeid, adapterid, portid);
			if (fprintf(fp, "%s\t%s\n", buffer, portname) == EOF) {
				free(portname);
				return (SCCONF_EUNEXPECTED);
			}
			free(portname);

			/* Add the state */
			(void) sprintf(buffer, ADAPTER_PORT_STATE,
			    nodeid, adapterid, portid);
			if (fprintf(fp, "%s\t%s\n", buffer, "disabled") == EOF)
				return (SCCONF_EUNEXPECTED);

			/* Set the portid in clconfig */
			e->scconf_cltr_epoint_portid = portid;

			/* Next port ID */
			++portid;
		}
	}

	return (SCCONF_NOERR);
}

/*
 * conf_clconfig_add_cpointprops
 *
 * Add cpoint properties found in "clconfig" to the open "fp" file
 * for the given cpoint.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument (or item(s) in clconfig)
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_clconfig_add_cpointprops(scconf_cfg_cluster_t *clconfig,
    FILE *fp, char *cpointname, int cpointid)
{
	scconf_cfg_cpoint_t *cluster_cpoint;
	scconf_cfg_prop_t *prop;
	char buffer[BUFSIZ];

	/* Check arguments */
	if (clconfig == NULL || fp == NULL ||
	    cpointname == NULL || cpointid < 1)
		return (SCCONF_EINVAL);

	/* Search for cpoint */
	for (cluster_cpoint = clconfig->scconf_cluster_cpointlist;
	    cluster_cpoint;
	    cluster_cpoint = cluster_cpoint->scconf_cpoint_next) {

		/* Match? */
		if (strcmp(cluster_cpoint->scconf_cpoint_cpointname,
		    cpointname) == 0)
			break;
	}

	/* Found it? */
	if (cluster_cpoint == NULL)
		return (SCCONF_EUNEXPECTED);

	/* Add properties */
	(void) sprintf(buffer, CLUSTER_CPOINT_PROPS, cpointid);
	for (prop = cluster_cpoint->scconf_cpoint_propertylist;  prop;
	    prop = prop->scconf_prop_next) {

		if (prop->scconf_prop_key == NULL ||
		    prop->scconf_prop_value == NULL)
			return (SCCONF_EINVAL);

		if (fprintf(fp, "%s.%s\t%s\n", buffer,
		    prop->scconf_prop_key, prop->scconf_prop_value) == EOF)
			return (SCCONF_EUNEXPECTED);
	}

	return (SCCONF_NOERR);
}

/*
 * conf_clconfig_add_cpointports
 *
 * Add cpoint ports found in the list of cables in "clconfig" to the
 * open "fp" file for the given cpoint.
 *
 * Possible return values:
 *
 *	SCCONF_NOERR		- success
 *	SCCONF_EINVAL		- invalid argument (or item(s) in clconfig)
 *	SCCONF_EUNEXPECTED	- internal or unexpected error
 */
static scconf_errno_t
conf_clconfig_add_cpointports(scconf_cfg_cluster_t *clconfig,
    FILE *fp, char *cpointname, int cpointid)
{
	scconf_cfg_cable_t *cluster_cable;
	scconf_cltr_epoint_t *e, *oe;
	char buffer[BUFSIZ];
	char *cname;
	char *portname;
	uint_t portid;

	/* Check arguments */
	if (clconfig == NULL || fp == NULL ||
	    cpointname == NULL || cpointid < 1)
		return (SCCONF_EINVAL);

	/* Walk through the list of cables */
	portid = 1;
	for (cluster_cable = clconfig->scconf_cluster_cablelist; cluster_cable;
	    cluster_cable = cluster_cable->scconf_cable_next) {
		e = &cluster_cable->scconf_cable_epoint1;
		oe = &cluster_cable->scconf_cable_epoint2;
		for (;;) {
			if (e && e->scconf_cltr_epoint_type ==
			    SCCONF_CLTR_EPOINT_TYPE_CPOINT) {
				cname = e->scconf_cltr_epoint_devicename;
				if (cname && strcmp(cname, cpointname) == 0)
					break;
			}

			if (e == &cluster_cable->scconf_cable_epoint1) {
				e = &cluster_cable->scconf_cable_epoint2;
				oe = &cluster_cable->scconf_cable_epoint1;
			} else {
				e = (scconf_cltr_epoint_t *)0;
				break;
			}
		}
		if (e != (scconf_cltr_epoint_t *)0) {

			/* Set the portname */
			if (e->scconf_cltr_epoint_portname) {
				portname = strdup(
				    e->scconf_cltr_epoint_portname);
				if (portname == NULL)
					return (SCCONF_ENOMEM);
			} else {
				portname =
				    conf_clconfig_generate_default_portname(
				    clconfig, e, oe);
				if (portname == NULL)
					return (SCCONF_EINVAL);
			}

			/* Add the port name */
			(void) sprintf(buffer, CPOINT_PORT_NAME,
			    cpointid, portid);
			if (fprintf(fp, "%s\t%s\n", buffer, portname) == EOF) {
				free(portname);
				return (SCCONF_EUNEXPECTED);
			}
			free(portname);

			/* Add the state */
			(void) sprintf(buffer, CPOINT_PORT_STATE,
			    cpointid, portid);
			if (fprintf(fp, "%s\t%s\n", buffer, "disabled") == EOF)
				return (SCCONF_EUNEXPECTED);

			/* Set the portid in clconfig */
			e->scconf_cltr_epoint_portid = portid;

			/* Next port ID */
			++portid;
		}
	}

	return (SCCONF_NOERR);
}

/*
 * conf_clconfig_generate_default_portname
 *
 * Generate a default portname for use with an endpoint on a new cable.
 * Two endpoints must be given;  the returned portname is for the
 * first endpoint, "epoint1".
 *
 * When creating a new cable, the default portname for an adapter
 * endpoint is zero.   And, for a blackbox, it is the nodeid on
 * the other end of the cable.  There is no default for
 * blackbox-to-blackbox cables.
 *
 * This function will generate portnames which may already be in use by
 * this or another cable.
 *
 * If it is not possible to generate a default, the function returns NULL.
 *
 * It is the responsibility of the caller to free portname memory.
 */
static char *
conf_clconfig_generate_default_portname(scconf_cfg_cluster_t *clconfig,
    scconf_cltr_epoint_t *epoint1, scconf_cltr_epoint_t *epoint2)
{
	scconf_nodeid_t nodeid;
	char snodeid[10];

	/* Check arguments */
	if (clconfig == NULL || epoint1 == NULL || epoint2 == NULL ||
	    epoint1->scconf_cltr_epoint_devicename == NULL ||
	    epoint2->scconf_cltr_epoint_devicename == NULL ||
	    epoint1->scconf_cltr_epoint_portname != NULL)
		return (NULL);

	/* Adapter or cpoint? */
	switch (epoint1->scconf_cltr_epoint_type) {
	case SCCONF_CLTR_EPOINT_TYPE_ADAPTER:
		return (strdup("0"));

	case SCCONF_CLTR_EPOINT_TYPE_CPOINT:

		/* Make sure the other end is attached to a node */
		if (epoint2->scconf_cltr_epoint_type !=
		    SCCONF_CLTR_EPOINT_TYPE_ADAPTER ||
		    epoint2->scconf_cltr_epoint_nodename == NULL)
			return (NULL);

		/* Get the nodeid of the other end */
		nodeid = conf_clconfig_get_nodeid(clconfig,
		    epoint2->scconf_cltr_epoint_nodename);
		if (nodeid == 0)
			return (NULL);

		/* Return the default */
		(void) sprintf(snodeid, "%d", nodeid);
		return (strdup(snodeid));

	default:
		break;
	}

	return (NULL);
}

/*
 * conf_clconfig_get_nodeid
 *
 * Return the nodeid for the given nodename.
 *
 * Possible return values:
 *
 *	0	Cannot find nodeid
 *	>0	Node id
 */
static scconf_nodeid_t
conf_clconfig_get_nodeid(scconf_cfg_cluster_t *clconfig, char *nodename)
{
	scconf_cfg_node_t *cluster_node;
	scconf_nodeid_t nodeid = 1;

	if (clconfig == NULL || nodename == NULL)
		return ((scconf_nodeid_t)0);

	for (cluster_node = clconfig->scconf_cluster_nodelist;  cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {
		if (strcmp(cluster_node->scconf_node_nodename, nodename) == 0)
			return (nodeid);
		++nodeid;
	}

	return ((scconf_nodeid_t)0);
}

/*
 * conf_clconfig_get_adapterid
 *
 * Return the adapterid for the given node-adapter name pair.
 *
 * Possible return values:
 *
 *	0	Cannot find adapterid
 *	>0	Adapter id
 */
static int
conf_clconfig_get_adapterid(scconf_cfg_cluster_t *clconfig, char *nodename,
    char *adaptername)
{
	scconf_cfg_node_t *cluster_node;
	scconf_cfg_cltr_adap_t *cluster_adap;
	int adapterid = 1;
	char device_name[BUFSIZ];
	char device_instance[BUFSIZ];
	char phys_adp[BUFSIZ];
	div_t inst_vlan;
	int tmp_instance = 0;

	if (clconfig == NULL || nodename == NULL || adaptername == NULL)
		return (0);

	/* Nodes */
	for (cluster_node = clconfig->scconf_cluster_nodelist;  cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {
		if (strcmp(cluster_node->scconf_node_nodename, nodename) == 0)
			break;
	}

	/* Found node? */
	if (cluster_node == NULL)
		return (0);

	/* Adapters */
	for (cluster_adap = cluster_node->scconf_node_adapterlist;
	    cluster_adap;  cluster_adap = cluster_adap->scconf_adap_next) {
		if (strcmp(cluster_adap->scconf_adap_adaptername,
		    adaptername) == 0)
			return (adapterid);
		if (conf_split_adaptername(
		    cluster_adap->scconf_adap_adaptername,
		    device_name, device_instance) != SCCONF_NOERR)
			return (0);
		tmp_instance = atoi(device_instance);
		if (tmp_instance >= VLAN_ZONE_START) {
			inst_vlan = div(tmp_instance, VLAN_MULTIPLIER);
			tmp_instance = inst_vlan.rem;
			(void) sprintf(phys_adp, "%s%d", device_name,
			    tmp_instance);
			if (strcmp(adaptername, phys_adp) == 0)
				return (adapterid);
		}

		++adapterid;
	}

	return (0);
}

/*
 * conf_clconfig_get_cpointid
 *
 * Return the cpointid for the given cpointname.
 *
 * Possible return values:
 *
 *	0	Cannot find cpointid
 *	>0	Cpoint id
 */
static int
conf_clconfig_get_cpointid(scconf_cfg_cluster_t *clconfig, char *cpointname)
{
	scconf_cfg_cpoint_t *cluster_cpoint;
	int cpointid = 1;

	if (clconfig == NULL || cpointname == NULL)
		return (0);

	for (cluster_cpoint = clconfig->scconf_cluster_cpointlist;
	    cluster_cpoint;
	    cluster_cpoint = cluster_cpoint->scconf_cpoint_next) {
		if (strcmp(cluster_cpoint->scconf_cpoint_cpointname,
		    cpointname) == 0)
			return (cpointid);
		++cpointid;
	}

	return (0);
}
