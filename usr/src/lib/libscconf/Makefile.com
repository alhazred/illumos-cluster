#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.35	09/01/16 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libscconf/Makefile.com
#

LIBRARY= libscconf.a
VERS=.1

OBJECTS= \
	scconf_cluster.o \
	scconf_nodes.o \
	scconf_transport.o \
	scconf_quorum.o \
	scconf_ds.o \
	scconf_logicalhost.o \
	scconf_sharedip.o \
	scconf_auth.o \
	scconf_misc.o \
	scconf_infrafile.o \
	scconf_propdes.o

# include library definitions
include ../../Makefile.lib

MAPFILE=	../common/mapfile-vers
$(POST_S9_BUILD)MAPFILE = ../common/mapfile-vers.s10
$(WEAK_MEMBERSHIP)MAPFILE = ../common/mapfile-vers.s11

SRCS=		$(OBJECTS:%.o=../common/%.c)

LIBS =		$(DYNLIB) $(LIBRARY)

LINTFLAGS +=	-I..
LINTFILES +=	$(OBJECTS:%.o=%.ln)

MTFLAG	=	-mt
CPPFLAGS +=	-I$(SRC)/lib/libscxcfg/common
CPPFLAGS +=	-v -I$(SRC)/lib/libdid/include -I$(SRC)/common/cl/dc/libdcs -I..
CPPFLAGS +=	-I$(ROOTCLUSTINC)/fmm
$(POST_S9_BUILD)CPPFLAGS += 	-I$(SRC)/common/cl/privip

PMAP =	-M $(MAPFILE)

# Warning: new dependencies here should be marked in cmd/scrconf/Makefile also
# to build the static command
LDLIBS +=	-lrgm -lclconf -ldid -lsocket -lnsl -lc -ldl -lclos -lscha -lpnm
$(POST_S9_BUILD)LDLIBS +=	-lscprivip

.KEEP_STATE:

$(DYNLIB):	$(MAPFILE)

# include library targets
include ../../Makefile.targ

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<
