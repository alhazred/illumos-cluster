#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.3	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libfmm/Makefile.com
#
include $(SRC)/Makefile.master

LIBRARY= libfmm.a
VERS= .1

LIBFMM_OBJECTS += \
	fmm_notification_client.o \
	fmm_api.o 

OBJECTS += $(LIBFMM_OBJECTS)

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib
CC_PICFLAGS = -KPIC

SRCS =		$(OBJECTS:%.o=../common/%.c)
CHECKHDRS = fmm_notification_client.h

CHECK_FILES =	$(LIBFMM_OBJECTS:%.o=%.c_check) \
	$(CHECKHDRS:%.h=%.h_check)

MTFLAG = -mt

CPPFLAGS += -I$(ROOTCLUSTINC)/fmm

LIBS = $(DYNLIB)

LDLIBS += -lc -ldoor -lfmmutils -lpthread -lrt

CLEANFILES =
LINTFILES += $(LIBFMM_OBJECTS:%.o=%.ln)

PMAP=

OBJS_DIR = objs
OBJS_DIR += pics

.KEEP_STATE:

.NO_PARALLEL:

all: $(LIBS)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules

objs/%.o pics/%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

