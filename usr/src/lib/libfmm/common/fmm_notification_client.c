/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fmm_notification_client.c	1.3	08/05/20	SMI"


#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

#include <fmmutils.h>
#include <fmm_api_comm.h>

#include "fmm_notification_client.h"

void
fmm_fifo_disconnect(int *fifo)
{
	if (fifo == NULL)
		return;

	if (*fifo == -1)
		return;

	fmm_log(LOG_DEBUG, "fmm_fifo_disconnect %d", *fifo);
	(void) close(*fifo);
	*fifo = -1;
}


int
fmm_fifo_connect(const int uniqueId)
{
	char key[PATH_MAX];
	int fifo;

	fmm_log(LOG_DEBUG, "fmm_fifo_connect: id %d", uniqueId);

	(void) sprintf(key, "%s/%s%d", FMM_TMP_DIR, FMM_KEY_PREFIX, uniqueId);
	fmm_log(LOG_DEBUG, "fmm_fifo_connect: create fifo %s", key);

	fifo = open(key, O_RDONLY | O_NONBLOCK);

	if (fifo == -1) {
		fmm_log(LOG_ERR, "fmm_fifo_connect: open failed %s",
		    strerror(errno)); /*lint !e746 */
	} else
		fmm_log(LOG_DEBUG, "fmm_fifo_connect: open %s (id=%d) ok", key,
		    fifo);

	return (fifo);
}


/*
 * result = -1 if read error
 * result = 0 if nothing to read
 * result > 0 if new viewnumber
 */
int
fmm_fifo_read(const int fifo, SaUint64T *viewNumber)
{
	ssize_t nbytes = 0;
	boolean_t go_on = B_TRUE;
	fmm_api_notification_message_t message;
	int result = -1;
	boolean_t emptyfifo = B_TRUE;

	*viewNumber = 0;

	while (go_on) {
		nbytes = read(fifo, &message,
		    sizeof (fmm_api_notification_message_t));
		switch (nbytes) {
		case 0:
			fmm_log(LOG_ERR, "fmm_fifo_read: read fifo failed - "
			    "no writer process");
			go_on = B_FALSE;
			break;
		case -1:
			if (errno == EAGAIN) {
				fmm_log(LOG_DEBUG, "fmm_fifo_read: empty fifo");
				result = 0;
			} else {
				fmm_log(LOG_ERR,
				    "fmm_fifo_read: read failed %s",
				    strerror(errno));
			}
			go_on = B_FALSE;
			break;
		case (sizeof (fmm_api_notification_message_t)):
			fmm_log(LOG_DEBUG, "fmm_fifo_read: new cluster view %u",
			    message.viewnumber);
			result = 1;
			emptyfifo = B_FALSE;
			break;
		default:
			fmm_log(LOG_ERR, "fmm_fifo_read: invalid message size");
			break;
		}
	}

	if (result == 0 && !emptyfifo)
		result = 1;

	if (result == 1) {
		fmm_log(LOG_DEBUG, "fmm_fifo_read: new view %u",
		    message.viewnumber);
		*viewNumber = message.viewnumber;
	}

	return (result);
}
