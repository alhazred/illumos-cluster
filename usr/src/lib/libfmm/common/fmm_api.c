/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fmm_api.c 1.4	08/05/20 SMI"


#include <stdlib.h>
#include <door.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <strings.h>
#include <signal.h>
#include <sys/time.h>

#include <saf_clm.h>
#include <fmm_api_comm.h>
#include <fmmutils.h>

#include "fmm_notification_client.h"



/* Structure to store information for each API client */
typedef struct {
	boolean_t initialized;
	int fifo;
	SaClmCallbacksT callbacks;
	SaUint8T trackFlags;
	boolean_t callbackPending;
	SaClmClusterNotificationT *notificationBuffer;
	SaUint32T numberOfItems;
	SaClmClusterNotificationT *lastCluster;
	SaUint64T lastViewNumber;
} saclm_client_t;

#define	FMM_API_MAX_CLIENTS	1

typedef struct {
	int doorHandle;
	saclm_client_t client[FMM_API_MAX_CLIENTS];
} saclm_client_list_t;

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; /*lint !e708 */
static saclm_client_list_t fmm_clients;
static boolean_t initialized = B_FALSE;

/* Initialize a client info */
static void
client_init(saclm_client_t *client)
{
	fmm_log(LOG_DEBUG, "client_init");
	if (client == NULL)
		return;

	client->initialized = B_FALSE;
	client->fifo = -1;
	client->callbacks.saClmClusterNodeGetCallback = NULL;
	client->callbacks.saClmClusterTrackCallback = NULL;
	client->trackFlags = 0;
	client->callbackPending = B_FALSE;
	client->notificationBuffer = NULL;
	client->numberOfItems = 0;
	client->lastCluster = NULL;
	client->lastViewNumber = 0;
}

/* Reset a client info */
static void
client_reset(saclm_client_t *client)
{
	fmm_log(LOG_DEBUG, "client_reset");
	if (client == NULL)
		return;

	client->initialized = B_FALSE;
	fmm_fifo_disconnect(&client->fifo);
	client->callbacks.saClmClusterNodeGetCallback = NULL;
	client->callbacks.saClmClusterTrackCallback = NULL;
	client->trackFlags = 0;
	client->callbackPending = B_FALSE;
	client->notificationBuffer = NULL;
	client->numberOfItems = 0;
	if (client->lastCluster)
		free(client->lastCluster);
	client->lastCluster = NULL;
	client->lastViewNumber = 0;
}


/*
 * called before the fork operation.
 * taking locks to prevent deadlock.
 */
static void
_prepare_handler(void)
{
	(void) pthread_mutex_lock(&mutex);
}


/*
 * called after fork() in the parent process
 * release the locks taken before fork()
 */
static void
_parent_handler(void)
{
	(void) pthread_mutex_unlock(&mutex);
}


/*
 * called after fork() in the child process
 * release the locks taken before fork()
 */
static void
_child_handler(void)
{
	/* Clean the clients list */
	unsigned int i = 0;
	for (i = 0; i < FMM_API_MAX_CLIENTS; i++) {
		if (!fmm_clients.client[i].initialized)
			continue;
		client_reset(&(fmm_clients.client[i]));
	}
	initialized = B_FALSE;
	(void) pthread_mutex_unlock(&mutex);
}


void
register_atfork_handler(void)
{
	(void) pthread_atfork(_prepare_handler, _parent_handler,
	    _child_handler);
}


#pragma init(register_atfork_handler)

void
fmm_api_init(void)
{
	unsigned int i;

	fmm_log_set_level(LOG_ERR);
	fmm_log_set_tag("Cluster.FMM.api");

	for (i = 0; i < FMM_API_MAX_CLIENTS; i ++) {
		client_init(&(fmm_clients.client[i]));
	}

	fmm_clients.doorHandle = open(FMM_API_DOOR_NAME, O_RDWR);
	fmm_log(LOG_DEBUG, "fmm_api_init: open door returned %d errno %d",
	    fmm_clients.doorHandle, errno); /*lint !e746 */

	initialized = B_TRUE;
}


static SaErrorT
startApi(sigset_t *old_mask, int *old_cancelstate)
{
	sigset_t newmask;
	int err = 0;

	err = sigfillset(&newmask);
	if (err != 0)
		return (SA_ERR_LIBRARY);

	/* We need these 2 signals to be delivered */
	(void) sigdelset(&newmask, SIGHUP);
	(void) sigdelset(&newmask, SIGINT);

	/* test for cancellation */
	pthread_testcancel();

	/* block signals */
	err = pthread_sigmask(SIG_BLOCK, &newmask, old_mask);
	if (err != 0)
		return (SA_ERR_LIBRARY);

	/* block thread cancellation */
	err = pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, old_cancelstate);
	if (err != 0) {
		(void) pthread_sigmask(SIG_SETMASK, old_mask, NULL);
		return (SA_ERR_LIBRARY);
	}

	return (SA_OK);
}


static SaErrorT
endApi(sigset_t *old_mask, int old_cancelstate)
{
	int err = 0;

	err = pthread_setcancelstate(old_cancelstate, NULL);
	if (err != 0)
		return (SA_ERR_LIBRARY);

	err = pthread_sigmask(SIG_SETMASK, old_mask, NULL);
	if (err != 0)
		return (SA_ERR_LIBRARY);

	return (SA_OK);
}


#define	START_API() \
	sigset_t old_mask; \
	int old_cancelstate; \
	SaErrorT saApiErr; \
	saApiErr = startApi(&old_mask, &old_cancelstate); \
	if (saApiErr != SA_OK) { \
		return (saApiErr); \
	}


#define	END_API() \
	saApiErr = endApi(&old_mask, old_cancelstate); \
	if (saApiErr != SA_OK) { \
		return (saApiErr); \
	}


static SaErrorT
client_call(fmm_api_args_t *args, size_t size)
{
	door_arg_t	doorArgs;
	SaErrorT	result = SA_OK;

	if ((fmm_clients.doorHandle == -1) || (args == NULL))
		return (SA_ERR_LIBRARY);

	doorArgs.data_ptr = (char *)args;
	doorArgs.data_size = size;
	doorArgs.desc_ptr = NULL;
	doorArgs.desc_num = 0;
	doorArgs.rbuf = (char *)args;
	doorArgs.rsize = size;

	if (door_call(fmm_clients.doorHandle, &doorArgs) == -1) {
		fmm_log(LOG_ERR, "client_call door_call failed: %s",
		    strerror(errno)); /*lint !e746 */
		switch (errno) {
		case EBADF:
			result = SA_ERR_BAD_HANDLE;
			break;
		default:
			result = SA_ERR_LIBRARY;
			break;
		}
		goto end;
	}

	if (doorArgs.data_size != size) {
		fmm_log(LOG_ERR, "client_call: returned incorrect size");
		result = SA_ERR_LIBRARY;
	}

end:
	fmm_log(LOG_DEBUG, "client_call returns (%i, %i)", result,
	    args->result);

	return (result);
}


/*
 * saClmInitialize
 *
 * RETURN VALUE
 * SA_OK The function completed successfully
 * SA_ERR_LIBRARY An unexpected problem occurred in the library. The library
 * cannot be used anymore
 * SA_ERR_VERSION The version parameter is not compatible
 * SA_ERR_TIMEOUT Implementation-dependent timeout.
 * SA_ERR_INVALID_PARAM A parameter is not correctly set
 * SA_ERR_NO_MEMORY Out of memory
 * SA_ERR_SYSTEM A system error occurred.
 */
static SaErrorT
saClmInitialize_main(SaClmHandleT *clmHandle,
	const SaClmCallbacksT *clmCallbacks,
	const SaVersionT *version)
{
	SaErrorT result = SA_OK;
	unsigned int i;
	unsigned int client_index = FMM_API_MAX_CLIENTS;
	fmm_api_args_t args;
	int fifo;
	pid_t pid = getpid();

	if (!initialized)
		fmm_api_init();

	if ((clmHandle == NULL) || (clmCallbacks == NULL) ||
	    (clmCallbacks->saClmClusterNodeGetCallback == NULL) ||
	    (clmCallbacks->saClmClusterTrackCallback == NULL) ||
	    (version == NULL))
		return (SA_ERR_INVALID_PARAM);

	if (fmm_clients.doorHandle == -1)
		return (SA_ERR_SYSTEM);

	args.service = FMM_API_REGISTER;
	args.in.fmm_register.id = pid;
	args.in.fmm_register.version.releaseCode = version->releaseCode;
	args.in.fmm_register.version.major = version->major;
	args.in.fmm_register.version.minor = version->minor;

	result = client_call(&args, sizeof (fmm_api_args_t));
	if (result != SA_OK)
		fmm_log(LOG_ERR, "saClmInitialize: FMM_API_REGISTER failed");
	else
		result = args.result;

	if (result != SA_OK)
		goto end;

	fmm_log(LOG_DEBUG, "saClmInitialize: FMM_API_REGISTER ok");

	/*
	 * Connect to the created fifo as a reader (Needed for FMM to open as a
	 * writer)
	 */
	fifo = fmm_fifo_connect(pid);
	if (fifo == -1) {
		fmm_log(LOG_ERR, "saClmInitialize: fmm_fifo_connect failed");
		result = SA_ERR_LIBRARY;
		goto end;
	}
	fmm_log(LOG_DEBUG, "saClmInitialize: fmm_fifo_connect ok");

	/* Call the server (2/2) */
	args.service = FMM_API_CONNECT;
	args.in.fmm_connect.id = pid;

	result = client_call(&args, sizeof (fmm_api_args_t));
	if (result != SA_OK) {
		fmm_log(LOG_ERR, "saClmInitialize: FMM_API_CONNECT failed");
	} else
		result = args.result;

	if (result != SA_OK)
		goto end;

	fmm_log(LOG_DEBUG, "saClmInitialize: FMM_API_CONNECT ok");

	/* Look for 1st free client in the list */
	(void) pthread_mutex_lock(&mutex);
	for (i = 0; i < FMM_API_MAX_CLIENTS; i++) {
		if (fmm_clients.client[i].initialized == B_FALSE) {
			client_index = i;
			break;
		}
	}

	if (client_index == FMM_API_MAX_CLIENTS) {
		/* Did not find a free client */
		fmm_log(LOG_ERR, "saClmInitialize: too many clients");
		(void) pthread_mutex_unlock(&mutex);
		return (SA_ERR_NO_MEMORY);
	}

	fmm_clients.client[client_index].initialized = B_TRUE;
	fmm_clients.client[client_index].fifo = fifo;
	fmm_clients.client[client_index].callbacks = *clmCallbacks;
	*clmHandle = client_index;

	fmm_log(LOG_DEBUG, "saClmInitialize: new client %d", client_index);
	(void) pthread_mutex_unlock(&mutex);

end:
	return (result);
}


SaErrorT
saClmInitialize(SaClmHandleT *clmHandle,
	const SaClmCallbacksT *clmCallbacks,
	const SaVersionT *version)
{
	SaErrorT saErr = SA_OK;
	START_API();

	saErr = saClmInitialize_main(clmHandle, clmCallbacks, version);

	END_API();
	return (saErr);
}


/*
 * saClmSelectionObjectGet
 *
 * RETURN VALUE
 * SA_OK The function completed successfully
 * SA_ERR_LIBRARY Unexpected problem
 * SA_ERR_INIT saClmInitialize not called or saClmFinalize already called
 * SA_ERR_NO_MEMORY Out of memory
 * SA_ERR_BAD_HANDLE clmHandle is invalid
 * SA_ERR_NO_RESOURCES Not enough resources available
 */
static SaErrorT
saClmSelectionObjectGet_main(SaClmHandleT clmHandle,
	SaSelectionObjectT *selectionObject)
{
	if (selectionObject == NULL)
		return (SA_ERR_INVALID_PARAM);

	if (clmHandle >= FMM_API_MAX_CLIENTS)
		return (SA_ERR_BAD_HANDLE);

	(void) pthread_mutex_lock(&mutex);

	if ((fmm_clients.client[clmHandle].initialized == B_FALSE) ||
	    (fmm_clients.doorHandle == -1) ||
	    (fmm_clients.client[clmHandle].fifo < 0)) {
		(void) pthread_mutex_unlock(&mutex);
		return (SA_ERR_INIT);
	}

	*selectionObject = fmm_clients.client[clmHandle].fifo; /*lint !e732 */
	(void) pthread_mutex_unlock(&mutex);
	return (SA_OK);
}


SaErrorT
saClmSelectionObjectGet(SaClmHandleT clmHandle,
	SaSelectionObjectT *selectionObject)
{
	SaErrorT saErr = SA_OK;
	START_API();

	saErr = saClmSelectionObjectGet_main(clmHandle, selectionObject);

	END_API();

	return (saErr);
}


/*
 * saClmDispatch
 *
 * RETURN VALUE
 * SA_OK
 * SA_ERR_LIBRARY
 * SA_ERR_INIT
 * SA_ERR_TIMEOUT
 * SA_ERR_INVALID_PARAM
 * SA_ERR_BAD_HANDLE
 * SA_ERR_BAD_FLAGS
 */
static SaErrorT
saClmDispatch_main(SaClmHandleT clmHandle,
	SaDispatchFlagsT dispatchFlags)
{
	SaUint64T viewNumber = 0;
	fmm_api_args_t *args;
	SaUint32T numberOfItems = 0;
	SaUint32T numberOfMembers = 0;
	SaClmClusterNotificationT *newBuffer, *oldBuffer;
	unsigned int i = 0, j = 0;
	SaErrorT result = SA_OK;
	int fifo_res = -1;

	if (dispatchFlags != SA_DISPATCH_ALL)
		return (SA_ERR_BAD_FLAGS);

	if (clmHandle >= FMM_API_MAX_CLIENTS)
		return (SA_ERR_BAD_HANDLE);

	if ((fmm_clients.doorHandle == -1) ||
	    (fmm_clients.client[clmHandle].initialized == B_FALSE))
		return (SA_ERR_INIT);

	(void) pthread_mutex_lock(&mutex);

	if (fmm_clients.client[clmHandle].callbackPending == B_FALSE) {
		/* The client did not call TrackStart => no buffer allocated */
		/* call the callback with error set to SA_ERR_NO_SPACE */
		fmm_clients.client[clmHandle].callbacks
		    .saClmClusterTrackCallback(NULL, 0, 0, 0LL,
		    SA_ERR_NO_SPACE);
		(void) pthread_mutex_unlock(&mutex);
		return (SA_OK);
	}

	fifo_res = fmm_fifo_read(fmm_clients.client[clmHandle].fifo,
	    &viewNumber);
	if (fifo_res < 0) {
		(void) pthread_mutex_unlock(&mutex);
		return (SA_ERR_LIBRARY);
	} else if (fifo_res == 0) {
		/* Nothing to read in the fifo => no need for the callback */
		(void) pthread_mutex_unlock(&mutex);
		return (SA_OK);
	}

	fmm_log(LOG_DEBUG, "saClmDispatch: new view number %d", viewNumber);

	if (fmm_clients.client[clmHandle].lastViewNumber >= viewNumber) {
		/* No change */
		(void) pthread_mutex_unlock(&mutex);
		return (SA_OK);
	}

	numberOfItems = fmm_clients.client[clmHandle].numberOfItems;
	oldBuffer = fmm_clients.client[clmHandle].lastCluster;

	/* Need to retrieve the new membership and compare */

	args = malloc(sizeof (fmm_api_args_t)
	    + sizeof (SaClmClusterNotificationT) * (numberOfItems - 1));
	if (args == NULL) {
		(void) pthread_mutex_unlock(&mutex);
		return (SA_ERR_LIBRARY);
	}
	args->service = FMM_API_ALL_NODES_GET;
	args->in.fmm_all_nodes_get.numberOfItems = numberOfItems;

	result = client_call(args, sizeof (fmm_api_args_t)
	    + sizeof (SaClmClusterNotificationT) * (numberOfItems - 1));

	if (result != SA_OK) {
		(void) pthread_mutex_unlock(&mutex);
		return (SA_ERR_LIBRARY);
	}

	if (args->result != SA_OK) {
		fmm_log(LOG_ERR, "saClmDispatch: FMM_API_ALL_NODES_GET failed");
		fmm_clients.client[clmHandle].callbacks
		    .saClmClusterTrackCallback(NULL, 0, 0, 0LL, args->result);
		(void) pthread_mutex_unlock(&mutex);
		return (SA_OK);
	}

	fmm_log(LOG_DEBUG, "saClmDispatch: FMM_API_ALL_NODES_GET ok");

	newBuffer = fmm_clients.client[clmHandle].notificationBuffer;
	bzero(newBuffer, sizeof (SaClmClusterNotificationT) * numberOfItems);
	bcopy(args->out.fmm_all_nodes_get.notificationBuffer, newBuffer,
	    sizeof (SaClmClusterNotificationT) * numberOfItems);

	/* Compare new membership and old membership */
	for (i = 0; i < numberOfItems; i++) {
		boolean_t found = B_FALSE;

		if (newBuffer[i].clusterNode.nodeId == 0)
			continue;

		for (j = 0; j < numberOfItems; j++) {
			if (oldBuffer[j].clusterNode.nodeId
			    == newBuffer[i].clusterNode.nodeId) {
				found = B_TRUE;
				break;
			}
		}

		/* Found this node in old cluster */
		if (found) {
			if (oldBuffer[j].clusterNode.member
			    == newBuffer[i].clusterNode.member) {
				newBuffer[i].clusterChanges
				    = SA_CLM_NODE_NO_CHANGE;
			} else if (newBuffer[i].clusterNode.member) {
				newBuffer[i].clusterChanges
				    = SA_CLM_NODE_JOINED;
			} else {
				newBuffer[i].clusterChanges = SA_CLM_NODE_LEFT;
			}

		} else {
			/* Did not find this node in old cluster */
			if (newBuffer[i].clusterNode.member) {
				newBuffer[i].clusterChanges
				    = SA_CLM_NODE_JOINED;
			} else
				newBuffer[i].clusterChanges
				    = SA_CLM_NODE_NO_CHANGE;
		}
	}

	/* Compute the number of members */
	for (i = 0; i < numberOfItems; i++)
		if (newBuffer[i].clusterNode.member)
			numberOfMembers++;

	bcopy(newBuffer, fmm_clients.client[clmHandle].lastCluster,
	    numberOfItems * sizeof (SaClmClusterNotificationT));

	fmm_log(LOG_DEBUG, "saClmDispatch: calling saClmClusterTrackCallback");

	fmm_clients.client[clmHandle].callbacks.saClmClusterTrackCallback(
	    fmm_clients.client[clmHandle].notificationBuffer,
	    numberOfItems, numberOfMembers, viewNumber, SA_OK);
	(void) pthread_mutex_unlock(&mutex);
	return (SA_OK);
}


SaErrorT
saClmDispatch(SaClmHandleT clmHandle,
	SaDispatchFlagsT dispatchFlags)
{
	SaErrorT saErr = SA_OK;
	START_API();

	saErr = saClmDispatch_main(clmHandle, dispatchFlags);

	END_API();

	return (saErr);
}


/*
 * saClmFinalize
 *
 * RETURN VALUE
 * SA_OK
 * SA_ERR_LIBRARY
 * SA_ERR_INIT
 * SA_ERR_BAD_HANDLE
 */
static SaErrorT
saClmFinalize_main(SaClmHandleT clmHandle)
{
	fmm_api_args_t args;
	SaErrorT result = SA_OK;

	if (clmHandle >= FMM_API_MAX_CLIENTS)
		return (SA_ERR_BAD_HANDLE);

	(void) pthread_mutex_lock(&mutex);
	if ((fmm_clients.doorHandle == -1) ||
	    (fmm_clients.client[clmHandle].initialized == B_FALSE)) {
		(void) pthread_mutex_unlock(&mutex);
		return (SA_ERR_INIT);
	}

	/* Remove client from the list */
	client_reset(&(fmm_clients.client[clmHandle]));

	(void) pthread_mutex_unlock(&mutex);

	/* Unregister the client to the server */
	args.service = FMM_API_UNREGISTER;
	args.in.fmm_unregister.id = getpid();

	result = client_call(&args, sizeof (fmm_api_args_t));
	if (result == SA_OK)
		return (args.result);

	return (result);
}


SaErrorT
saClmFinalize(SaClmHandleT clmHandle)
{
	SaErrorT saErr = SA_OK;
	START_API();

	saErr = saClmFinalize_main(clmHandle);

	END_API();

	return (saErr);
}


/*
 * saClmClusterTrackStart
 *
 * RETURN VALUE
 * SA_OK
 * SA_ERR_LIBRARY
 * SA_ERR_INIT
 * SA_ERR_TIMEOUT
 * SA_ERR_INVALID_PARAM
 * SA_ERR_NO_MEMORY
 * SA_ERR_BAD_HANDLE
 * SA_ERR_EXIST
 * SA_ERR_BAD_FLAGS
 */
static SaErrorT
saClmClusterTrackStart_main(SaClmHandleT clmHandle, SaUint8T trackFlags,
	SaClmClusterNotificationT *notificationBuffer, SaUint32T numberOfItems)
{
	fmm_api_args_t *args;
	SaErrorT result;

	if ((trackFlags &
	    ~(SA_TRACK_CURRENT | SA_TRACK_CHANGES))
	    != 0)
		return (SA_ERR_BAD_FLAGS);

	if ((trackFlags & SA_TRACK_CHANGES) &&
	    (trackFlags & SA_TRACK_CHANGES_ONLY))
		return (SA_ERR_BAD_FLAGS);

	if (trackFlags == 0)
		return (SA_ERR_BAD_FLAGS);

	if ((notificationBuffer == NULL) || (numberOfItems == 0))
		return (SA_ERR_INVALID_PARAM);

	if (clmHandle >= FMM_API_MAX_CLIENTS)
		return (SA_ERR_BAD_HANDLE);

	(void) pthread_mutex_lock(&mutex);
	if ((fmm_clients.doorHandle == -1) ||
	    (fmm_clients.client[clmHandle].initialized == B_FALSE)) {
		(void) pthread_mutex_unlock(&mutex);
		return (SA_ERR_INIT);
	}

	if (fmm_clients.client[clmHandle].callbackPending == B_TRUE) {
		(void) pthread_mutex_unlock(&mutex);
		return (SA_ERR_EXIST);
	}

	(void) pthread_mutex_unlock(&mutex);


	/* Get the whole membership */

	args = malloc(sizeof (fmm_api_args_t)
	    + sizeof (SaClmClusterNotificationT) * (numberOfItems - 1));
	if (args == NULL)
		return (SA_ERR_NO_MEMORY);

	args->service = FMM_API_ALL_NODES_GET;
	args->in.fmm_all_nodes_get.numberOfItems = numberOfItems;

	result = client_call(args, sizeof (fmm_api_args_t)
	    + sizeof (SaClmClusterNotificationT) * (numberOfItems - 1));

	if (result == SA_OK)
		result = args->result;

	if (result != SA_OK) {
		fmm_log(LOG_ERR,
		    "saClmClusterTrackStart: FMM_API_ALL_NODES_GET failed");
		return (SA_ERR_LIBRARY);
	}

	fmm_log(LOG_DEBUG, "saClmClusterTrackStart: FMM_API_ALL_NODES_GET ok");
	if (trackFlags & SA_TRACK_CURRENT) {
		unsigned int numberOfMembers = 0, i = 0;

		bcopy(args->out.fmm_all_nodes_get.notificationBuffer,
		    notificationBuffer,
		    sizeof (SaClmClusterNotificationT)
		    * args->out.fmm_all_nodes_get.numberOfItems);

		/* Compute the number of members */
		for (i = 0; i < args->out.fmm_all_nodes_get.numberOfItems;
		    i++)
			if (args->out.fmm_all_nodes_get
			    .notificationBuffer[i].clusterNode.member)
				numberOfMembers++;

		/* call the callback */
		(void) pthread_mutex_lock(&mutex);
		fmm_log(LOG_DEBUG, "saClmClusterTrackStart: "
		    "calling saClmClusterTrackCallback");

		fmm_clients.client[clmHandle].callbacks
		    .saClmClusterTrackCallback(notificationBuffer,
		    args->out.fmm_all_nodes_get.numberOfItems,
		    numberOfMembers,
		    args->out.fmm_all_nodes_get.viewNumber, SA_OK);

		(void) pthread_mutex_unlock(&mutex);

		return (SA_OK);
	} else if (trackFlags & SA_TRACK_CHANGES) {
		(void) pthread_mutex_lock(&mutex);
		fmm_clients.client[clmHandle].callbackPending = B_TRUE;
		fmm_clients.client[clmHandle].notificationBuffer
		    = notificationBuffer;
		fmm_clients.client[clmHandle].numberOfItems = numberOfItems;

		/* Store the initial membership */
		fmm_clients.client[clmHandle].lastCluster = malloc(
		    sizeof (SaClmClusterNotificationT) * numberOfItems);

		if (fmm_clients.client[clmHandle].lastCluster == NULL) {
			(void) pthread_mutex_unlock(&mutex);
			return (SA_ERR_NO_MEMORY);
		}

		bcopy(args->out.fmm_all_nodes_get.notificationBuffer,
		    fmm_clients.client[clmHandle].lastCluster,
		    sizeof (SaClmClusterNotificationT)
		    * args->out.fmm_all_nodes_get.numberOfItems);

		fmm_clients.client[clmHandle].lastViewNumber =
		    args->out.fmm_all_nodes_get.viewNumber;

		(void) pthread_mutex_unlock(&mutex);
	}
	return (SA_OK);
}


SaErrorT
saClmClusterTrackStart(SaClmHandleT clmHandle, SaUint8T trackFlags,
    SaClmClusterNotificationT *notificationBuffer, SaUint32T numberOfItems)
{
	SaErrorT saErr = SA_OK;
	START_API();

	saErr = saClmClusterTrackStart_main(clmHandle, trackFlags,
	    notificationBuffer, numberOfItems);

	END_API();

	return (saErr);
}


/*
 * saClmClusterTrackStop
 *
 * RETURN VALUE
 * SA_OK
 * SA_ERR_LIBRARY
 * SA_ERR_INIT
 * SA_ERR_TIMEOUT
 * SA_ERR_INVALID_PARAM
 * SA_ERR_NO_MEMORY
 * SA_ERR_BAD_HANDLE
 * SA_ERR_NOT_EXIST
 */
static SaErrorT
saClmClusterTrackStop_main(SaClmHandleT clmHandle)
{
	if (clmHandle >= FMM_API_MAX_CLIENTS)
		return (SA_ERR_BAD_HANDLE);

	(void) pthread_mutex_lock(&mutex);
	if ((fmm_clients.doorHandle == -1) ||
	    (fmm_clients.client[clmHandle].initialized == B_FALSE)) {
		(void) pthread_mutex_unlock(&mutex);
		return (SA_ERR_INIT);
	}

	if (fmm_clients.client[clmHandle].callbackPending == B_FALSE) {
		(void) pthread_mutex_unlock(&mutex);
		return (SA_ERR_NOT_EXIST);
	}

	fmm_clients.client[clmHandle].callbackPending = B_FALSE;
	fmm_clients.client[clmHandle].notificationBuffer = NULL;
	fmm_clients.client[clmHandle].numberOfItems = 0;
	(void) pthread_mutex_unlock(&mutex);
	return (SA_OK);
}


SaErrorT
saClmClusterTrackStop(SaClmHandleT clmHandle)
{
	SaErrorT saErr = SA_OK;
	START_API();

	saErr = saClmClusterTrackStop_main(clmHandle);

	END_API();

	return (saErr);
}


/*
 * saClmClusterNodeGet
 *
 * RETURN VALUE
 * SA_OK
 * SA_ERR_LIBRARY
 * SA_ERR_INIT
 * SA_ERR_TIMEOUT
 * SA_ERR_INVALID_PARAM
 */
static SaErrorT
saClmClusterNodeGet_main(SaClmHandleT clmHandle, SaClmNodeIdT nodeId,
	SaTimeT timeout, SaClmClusterNodeT *clusterNode)
{
	fmm_api_args_t args;
	SaErrorT result;

	if (clusterNode == NULL)
		return (SA_ERR_INVALID_PARAM);

	if (clmHandle >= FMM_API_MAX_CLIENTS)
		return (SA_ERR_BAD_HANDLE);

	if ((fmm_clients.client[clmHandle].initialized == B_FALSE) ||
	    (fmm_clients.doorHandle == -1) ||
	    (fmm_clients.client[clmHandle].fifo < 0))
		return (SA_ERR_INIT);

	args.service = FMM_API_NODE_GET;
	args.in.fmm_node_get.nodeId = nodeId;
	args.in.fmm_node_get.timeout = timeout + gethrtime();

	result = client_call(&args, sizeof (fmm_api_args_t));
	if ((result == SA_OK) && (args.result == SA_OK)) {
		bcopy(&args.out.fmm_node_get.node, clusterNode,
		    sizeof (SaClmClusterNodeT));
		result = args.result;
	} else {
		fmm_log(LOG_ERR,
		    "saClmClusterNodeGet: FMM_API_NODE_GET failed");
	}

	return (args.result);
}


SaErrorT
saClmClusterNodeGet(SaClmHandleT clmHandle, SaClmNodeIdT nodeId,
	SaTimeT timeout, SaClmClusterNodeT *clusterNode)
{
	SaErrorT saErr = SA_OK;
	START_API();

	saErr = saClmClusterNodeGet_main(clmHandle, nodeId, timeout,
	    clusterNode);

	END_API();

	return (saErr);
}


/*
 * saClmClusterNodeGetAsync
 *
 * RETURN VALUE
 * SA_OK
 * SA_ERR_LIBRARY
 * SA_ERR_INIT
 * SA_ERR_TIMEOUT
 * SA_ERR_INVALID_PARAM
 * SA_BAD_HANDLE
 */
/*ARGSUSED*/
SaErrorT
saClmClusterNodeGetAsync_main(SaClmHandleT clmHandle, SaInvocationT invocation,
	SaClmNodeIdT nodeId, SaClmClusterNodeT *clusterNode)
{
	if (clusterNode == NULL)
		return (SA_ERR_INVALID_PARAM);

	if (clmHandle >= FMM_API_MAX_CLIENTS)
		return (SA_ERR_BAD_HANDLE);

	(void) pthread_mutex_lock(&mutex);
	if ((fmm_clients.doorHandle == -1) ||
	    (fmm_clients.client[clmHandle].initialized == B_FALSE)) {
		(void) pthread_mutex_unlock(&mutex);
		return (SA_ERR_INIT);
	}
	(void) pthread_mutex_unlock(&mutex);

	/* Not implemented */
	return (SA_ERR_LIBRARY);
}


SaErrorT
saClmClusterNodeGetAsync(SaClmHandleT clmHandle, SaInvocationT invocation,
	SaClmNodeIdT nodeId, SaClmClusterNodeT *clusterNode)
{
	SaErrorT saErr = SA_OK;
	START_API();

	saErr = saClmClusterNodeGetAsync_main(clmHandle, invocation, nodeId,
	    clusterNode);

	END_API();
	return (saErr);
}
