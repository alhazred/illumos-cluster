#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)Makefile.com	1.8	08/07/24 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# lib/libzccfg/Makefile.com
#

LIBRARY= libzccfg.a
VERS= .1

OBJECTS = libzccfg.o clzonecfg_net.o

# include library definitions, do not change order of include and DYNLIB
include ../../Makefile.lib

LINTFILES = $(OBJECTS:%.o=%.ln)
CHECK_FILES = $(OBJECTS:%.o=%.c_check)

MAPFILE=	../common/mapfile-vers
PMAP=		-M $(MAPFILE)
SRCS =		$(OBJECTS:%.o=../common/%.cc)

LIBS = $(DYNLIB) $(LIBRARY)

MTFLAG	= -mt
CPPFLAGS += $(CL_CPPFLAGS)
CPPFLAGS += -I. -I$(REF_PROTO)/usr/include/libxml2 -D_REENTRANT
CPPFLAGS += -I$(SRC)/common/cl/interfaces/$(CLASS) -I$(SRC)/common/cl
CPPFLAGS += -I$(SRC)/lib/libclcomm/common

LIBS =		$(DYNLIB)
LDLIBS += -L$(REF_PROTO)/usr/lib
LDLIBS += -lc $(REF_USRLIB:%=%/libCrun.so.1) -ldoor -lclcomm -lclos
LDLIBS += -lclconf -lnvpair -lnsl -lsocket
$(DYNLIB) :=	LDLIBS += -lxml2

AS_CPPFLAGS     += -D_ASM
# need to set this to NULL for lint
lint:= AS_CPPFLAGS     =
ASFLAGS         += -P

.KEEP_STATE:

.NO_PARALLEL:

all debug:  $(LIBS)

$(DYNLIB): 	$(MAPFILE)

# include library targets
include ../../Makefile.targ
include $(SRC)/common/cl/Makefile.rules

objs/%.o pics/%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

objs/%.o pics/%.o: %.s
	$(COMPILE.s) -o $@ $<

%.ln: ../common/%.cc
	$(LINT.cc) $< > $@
	$(CAT) $@
