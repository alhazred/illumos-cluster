//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http://www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)libzccfg.cc	1.35	09/03/31 SMI"

//
// libzccfg is an adaptation of Solaris libzonecfg for Sun Cluster. The
// exported library interfaces have similar (but not identical!) names and
// signatures to those in libzonecfg. The libzccfg functions that directly
// manipulate XML data have more or less the same algorithms as the
// corresponding functions in libzonecfg, and in many cases we re-use the
// Solaris code after appropriate symbol name and stylistic changes.
//
// In addition to functions that work with XML data, we also have functions
// that work with CCR data, such as conversion of XML to CCR and vice versa.
// These functions are completely new for libzccfg and have no analogues in
// the Solaris code.
//
// For each new zone resource or property that is introduced in Solaris, the
// corresponding functions must be ported here, as well as updating the CCR
// accessor functions to deal with the new resources/properties.
//

#include <stdlib.h>
#include <errno.h>
#include <strings.h>
#include <unistd.h>
#include <assert.h>
#include <libgen.h>
#include <libintl.h>
#include <alloca.h>
#include <ctype.h>
#include <sys/nvpair.h>
#include <sys/types.h>

#include <fcntl.h>
#include <sys/stat.h>

#include <arpa/inet.h>
#include <netdb.h>

#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#include <h/ccr.h>
#include <h/remote_exec.h>
#include <nslib/ns.h>
#include <nslib/naming_context_impl.h>
#include <sys/os.h>
#include <sys/rsrc_tag.h>
#include <sys/clconf_int.h>

#include <libzccfg/libzccfg.h>

#include <cmm/cmm_ns.h>
#include <h/membership.h>
#include <sys/clconf_int.h>
#include <sys/vc_int.h>

//
// Used by the zccfg_attr_exists_in_zones_dtd interface.
//
#define	BUFFERSIZE	1024
#define	DTD_PATH	"/usr/share/lib/xml/dtd/zonecfg.dtd.1"

//
// DTD element/attribute/entity definitions, similar to those in
// usr/src/lib/libzonecfg/common/libzonecfg.c in Solaris. This list must be
// updated if/when new resource types and/or properties are added/removed.
//
#define	DTD_ELEM_ATTR		(const xmlChar *) "attr"
#define	DTD_ELEM_COMMENT	(const xmlChar *) "comment"
#define	DTD_ELEM_DATASET	(const xmlChar *) "dataset"
#define	DTD_ELEM_DEVICE		(const xmlChar *) "device"
#define	DTD_ELEM_FS		(const xmlChar *) "filesystem"
#define	DTD_ELEM_FSOPTION	(const xmlChar *) "fsoption"
#define	DTD_ELEM_IPD		(const xmlChar *) "inherited-pkg-dir"
#define	DTD_ELEM_NET		(const xmlChar *) "network"
#define	DTD_ELEM_NODE		(const xmlChar *) "node"
#define	DTD_ELEM_RCTL		(const xmlChar *) "rctl"
#define	DTD_ELEM_RCTLVALUE	(const xmlChar *) "rctl-value"
#define	DTD_ELEM_SYSID		(const xmlChar *) "sysid"
#define	DTD_ELEM_ZONE		(const xmlChar *) "zone"
#define	DTD_ELEM_MCAP		(const xmlChar *) "mcap"
#define	DTD_ELEM_PSET		(const xmlChar *) "pset"
#define	DTD_ELEM_TMPPOOL	(const xmlChar *) "tmp_pool"

#define	DTD_ATTR_ACTION			(const xmlChar *) "action"
#define	DTD_ATTR_ADDRESS		(const xmlChar *) "address"
#define	DTD_ATTR_AUTOBOOT		(const xmlChar *) "autoboot"
#define	DTD_ATTR_DIR			(const xmlChar *) "directory"
#define	DTD_ATTR_BRAND			(const xmlChar *) "brand"
#define	DTD_ATTR_BOOTARGS		(const xmlChar *) "bootargs"
#define	DTD_ATTR_LIMIT			(const xmlChar *) "limit"
#define	DTD_ATTR_LIMITPRIV		(const xmlChar *) "limitpriv"
#define	DTD_ATTR_MATCH			(const xmlChar *) "match"
#define	DTD_ATTR_NAME			(const xmlChar *) "name"
#define	DTD_ATTR_HOSTNAME		(const xmlChar *) "hostname"
#define	DTD_ATTR_ROOTPASSWD		(const xmlChar *) "root_password"
#define	DTD_ATTR_NAMESERVICE		(const xmlChar *) "name_service"
#define	DTD_ATTR_NFS4DOMAIN		(const xmlChar *) "nfs4_domain"
#define	DTD_ATTR_SECPOLICY		(const xmlChar *) "security_policy"
#define	DTD_ATTR_SYSLOCALE		(const xmlChar *) "system_locale"
#define	DTD_ATTR_TERMINAL		(const xmlChar *) "terminal"
#define	DTD_ATTR_TIMEZONE		(const xmlChar *) "timezone"
#define	DTD_ATTR_PHYSICAL		(const xmlChar *) "physical"
#define	DTD_ATTR_POOL			(const xmlChar *) "pool"
#define	DTD_ATTR_PRIV			(const xmlChar *) "priv"
#define	DTD_ATTR_RAW			(const xmlChar *) "raw"
#define	DTD_ATTR_TYPE			(const xmlChar *) "type"
#define	DTD_ATTR_VALUE			(const xmlChar *) "value"
#define	DTD_ATTR_VERSION		(const xmlChar *) "version"
#define	DTD_ATTR_SPECIAL		(const xmlChar *) "special"
#define	DTD_ATTR_ZONEPATH		(const xmlChar *) "zonepath"
#define	DTD_ATTR_SCHED			(const xmlChar *) "scheduling-class"
#define	DTD_ATTR_CLPRIVNET		(const xmlChar *) "enable_priv_net"
#define	DTD_ATTR_NCPU_MIN		(const xmlChar *) "ncpu_min"
#define	DTD_ATTR_NCPU_MAX		(const xmlChar *) "ncpu_max"
#define	DTD_ATTR_IMPORTANCE		(const xmlChar *) "importance"
#define	DTD_ATTR_PHYSCAP		(const xmlChar *) "physcap"
#define	DTD_ATTR_IPTYPE			(const xmlChar *) "ip-type"
#define	DTD_ATTR_DEFROUTER		(const xmlChar *) "defrouter"
#define	DTD_ATTR_PHYSICALHOST		(const xmlChar *) "physical-host"

#define	DTD_ENTITY_BOOLEAN	"boolean"
#define	DTD_ENTITY_DEVPATH	"devpath"
#define	DTD_ENTITY_FALSE	"false"
#define	DTD_ENTITY_TRUE		"true"
#define	DTD_ENTITY_INT		"int"
#define	DTD_ENTITY_STRING	"string"
#define	DTD_ENTITY_UINT		"uint"

#define	DTD_ENTITY_BOOL_LEN	6	// false

// Defines for the zone cluster configuration CCR table. Expand as needed.
#define	ZC_ZONE_NAME			"zone.name"
#define	ZC_ZONE_PATH			"zone.zonepath"
#define	ZC_ZONE_AUTOBOOT		"zone.autoboot"
#define	ZC_ZONE_BRAND			"zone.brand"
#define	ZC_ZONE_LIMITPRIV		"zone.limitpriv"
#define	ZC_ZONE_IPTYPE			"zone.ip-type"
#define	ZC_ZONE_POOL			"zone.pool"
#define	ZC_ZONE_CLPRIVNET		"zone.clprivnet"
#define	ZC_ZONE_BOOTARGS		"zone.bootargs"
#define	ZC_ZONE_IPDS			"zone.inherited-pkg-dir.%d.directory"
#define	ZC_ZONE_DEVICE			"zone.device.%d.match"
#define	ZC_ZONE_DATASET			"zone.dataset.%d.name"
#define	ZC_ZONE_FS_DIR			"zone.filesystem.%d.directory"
#define	ZC_ZONE_FS_SPECIAL		"zone.filesystem.%d.special"
#define	ZC_ZONE_FS_RAW			"zone.filesystem.%d.raw"
#define	ZC_ZONE_FS_TYPE			"zone.filesystem.%d.type"
#define	ZC_ZONE_FS_OPT			"zone.filesystem.%d.option.%d.name"
#define	ZC_ZONE_NET_ADDRESS		"zone.network.%d.address"
#define	ZC_ZONE_NET_PHYSICAL		"zone.network.%d.physical"
#define	ZC_ZONE_RCTL_NAME		"zone.rctl.%d.name"
#define	ZC_ZONE_RCTLVAL_PRIV		"zone.rctl.%d.value.%d.priv"
#define	ZC_ZONE_RCTLVAL_LIMIT		"zone.rctl.%d.value.%d.limit"
#define	ZC_ZONE_RCTLVAL_ACTION		"zone.rctl.%d.value.%d.action"
#define	ZC_ZONE_MCAP_PHYSCAP		"zone.mcap.physcap"
#define	ZC_ZONE_PSET_NCPU_MIN		"zone.pset.ncpu_min"
#define	ZC_ZONE_PSET_NCPU_MAX		"zone.pset.ncpu_max"
#define	ZC_ZONE_PSET_IMPORTANCE		"zone.pset.importance"
#define	ZC_ZONE_SCHED_CLASS		"zone.scheduling-class"
#define	ZC_SYSID_ROOT_PASSWORD		"zone.sysid.root_password"
#define	ZC_SYSID_NAME_SERVICE		"zone.sysid.name_service"
#define	ZC_SYSID_NFS4_DOMAIN		"zone.sysid.nfs4_domain"
#define	ZC_SYSID_SEC_POLICY		"zone.sysid.security_policy"
#define	ZC_SYSID_SYS_LOCALE		"zone.sysid.system_locale"
#define	ZC_SYSID_TERMINAL		"zone.sysid.terminal"
#define	ZC_SYSID_TIMEZONE		"zone.sysid.timezone"
#define	ZC_NODE_NAME			"zone.node.%d.name"
#define	ZC_NODE_HOSTNAME		"zone.node.%d.hostname"
#define	ZC_NODE_NET_ADDRESS		"zone.node.%d.network.%d.address"
#define	ZC_NODE_NET_PHYSICAL		"zone.node.%d.network.%d.physical"
#define	ZC_NODE_NET_DEFROUTER		"zone.node.%d.network.%d.defrouter"

// Defines for the zone cluster infrastructure table. Expand as needed.
#define	ZC_CLUSTER_NAME			"cluster.name"
#define	ZC_CLUSTER_PROPS		"cluster.properties"
#define	ZC_CLUSTER_NODE_NAME		"cluster.nodes.%d.name"
#define	ZC_CLUSTER_NODE_PROPS		"cluster.nodes.%d.properties"

static const char *ZC_STATE_STR_CONFIGURED = "Configured";
static const char *ZC_STATE_STR_INCOMPLETE = "Incomplete";
static const char *ZC_STATE_STR_INSTALLED = "Installed";
static const char *ZC_STATE_STR_DOWN = "Down";
static const char *ZC_STATE_STR_MOUNTED = "Mounted";
static const char *ZC_STATE_STR_READY = "Ready";
static const char *ZC_STATE_STR_SHUTTING_DOWN = "Shutting_down";
static const char *ZC_STATE_STR_RUNNING = "Running";
static const char *ZC_STATE_STR_UNKNOWN = "Unknown";
static const char *ZC_STATE_STR_ONLINE = "Online";
static const char *ZC_STATE_STR_OFFLINE = "Offline";

//
// rctl alias definitions copied from usr/lib/libzonecfg/common/libzonecfg.c
// in Solaris. If the definition in Solaris changes, the same changes must be
// made here.
//
// This holds the alias, the full rctl name, the default priv value, action
// and lower limit.  The functions that handle rctl aliases step through
// this table, matching on the alias, and using the full values for setting
// the rctl entry as well the limit for validation.
//
static struct alias {
	const char *shortname;
	const char *realname;
	const char *priv;
	const char *action;
	uint64_t low_limit;
} aliases[] = {
	{ALIAS_MAXLWPS, "zone.max-lwps", "privileged", "deny", 100},
	{ALIAS_MAXSHMMEM, "zone.max-shm-memory", "privileged", "deny", 0},
	{ALIAS_MAXSHMIDS, "zone.max-shm-ids", "privileged", "deny", 0},
	{ALIAS_MAXMSGIDS, "zone.max-msg-ids", "privileged", "deny", 0},
	{ALIAS_MAXSEMIDS, "zone.max-sem-ids", "privileged", "deny", 0},
	{ALIAS_MAXLOCKEDMEM, "zone.max-locked-memory", "privileged", "deny",
	    0},
	{ALIAS_MAXSWAP, "zone.max-swap", "privileged", "deny", 0},
	{ALIAS_SHARES, "zone.cpu-shares", "privileged", "none", 0},
	{ALIAS_CPUCAP, "zone.cpu-cap", "privileged", "deny", 0},
	{NULL, NULL, NULL, NULL, 0}
};

//
// Number of attributes of the "node" XML element (name & hostname). Used in
// populate_node_ent(). Must be changed if the number of attributes of the
// node element changes.
//
static const int num_node_attrs = 2;

struct zc_dochandle {
	xmlDocPtr	zc_dh_doc;
	xmlNodePtr	zc_dh_cur;
	xmlNodePtr	zc_dh_top;
};

char *zonecfg_root = (char *)"";

static os::sc_syslog_msg logger(SC_SYSLOG_ZC_LIB_TAG, "", NULL);

static int zccfg_add_net_elem(zc_nodetab *,
    struct zc_nwiftab *);
static int zccfg_get_local_net(xmlNodePtr, struct zc_nodetab *);

/* ARGSUSED */
static void
zccfg_error_func(void *ctx, const char *msg, ...)
{
	//
	// This function is a no-op by design. Its purpose is to prevent
	// libxml from throwing unwanted messages to stdout/stderr.
	//
}

//
// This function validates the "cluster" brand definition file against the
// brand DTD provided by Solaris. This check is needed to ensure that the
// current version of oour brand definition is compatible with the version
// of Solaris we're running on.
//
// Returns:
//	ZC_OK on success
//	ZC_NOMEM if there is memory allocation failure
//	ZC_SYSTEM if the cluster brand definition file doesn't exist
//	ZC_INVALID_DOCUMENT otherwise
//
int
zccfg_validate_brand_definition(void)
{
	int		valid;
	struct stat	statbuf;
	xmlValidCtxtPtr cvp = { NULL };
	xmlDocPtr	doc;
	const char	*filenamep = "/usr/lib/brand/cluster/config.xml";

	if ((doc = xmlParseFile(filenamep)) == NULL) {
		if (stat(filenamep, &statbuf) == 0) {
			return (ZC_INVALID_DOCUMENT);
		}
		//
		// Very bad. The cluster brand definition file does not
		// exist. Return ZC_SYSTEM, but set errno to ENOENT.
		//
		errno = ENOENT;
		return (ZC_SYSTEM);
	}

	if ((cvp = xmlNewValidCtxt()) == NULL) {
		return (ZC_NOMEM);
	}
	cvp->error = zccfg_error_func;
	cvp->warning = zccfg_error_func;
	valid = xmlValidateDocument(cvp, doc);
	xmlFreeValidCtxt(cvp);

	if (valid == 0) {
		return (ZC_INVALID_DOCUMENT);
	}
	return (ZC_OK);
}

//
// Convenience function to return the XML file path name for a given zonename.
// The result is returned via the "answer" argument.
//
// Returns: True on success; false on failure.
//
static bool
config_file_path(const char *zonename, char *answer)
{
	return (snprintf(answer, MAXPATHLEN, "%s%s/%s.xml", zonecfg_root,
	    ZC_CONFIG_ROOT, zonename) < MAXPATHLEN);
}

//
// zccfg_init_handle
// Allocates memory for the pointer to the XML document handle and performs
// libxml initializations. This function must be called before any other
// operations are performed on a handle.
//
// Returns NULL and sets errno to ZC_NOMEM on error, else returns an initialized
// handle.
//
zc_dochandle_t
zccfg_init_handle(void)
{
	zc_dochandle_t handle = (zc_dochandle_t)calloc(1,
	    sizeof (struct zc_dochandle));
	if (handle == NULL) {
		errno = ZC_NOMEM;
		return (NULL);
	}

	// libxml initialization
	(void) xmlLineNumbersDefault(1);
	xmlLoadExtDtdDefaultValue |= XML_DETECT_IDS;
	xmlDoValidityCheckingDefaultValue = 1;
	(void) xmlKeepBlanksDefault(0);
	xmlGetWarningsDefaultValue = 0;
	xmlSetGenericErrorFunc(NULL, zccfg_error_func);

	return (handle);
}

//
// Checks whether a given XML handle is sane or not.
//
int
zccfg_check_handle(const zc_dochandle_t handle)
{
	if (handle == NULL || handle->zc_dh_doc == NULL) {
		return (ZC_BAD_HANDLE);
	}
	return (ZC_OK);
}

//
// Deallocates the handle initialized by zccfg_init_handle(). So any use
// of the XML handle goes as follows:
//
// handle = zccfg_init_handle();
// if (handle == NULL) {
//	handle error;
// }
// work with the handle;
//
// zccfg_fini_handle(handle);
//
void
zccfg_fini_handle(zc_dochandle_t handle)
{
	if (zccfg_check_handle(handle) == ZC_OK) {
		xmlFreeDoc(handle->zc_dh_doc);
	}
	if (handle != NULL) {
		free(handle);
	}
}

//
// Convenience function to get the root element of the XML tree.
//
static int
getroot(zc_dochandle_t handle, xmlNodePtr *root)
{
	if (zccfg_check_handle(handle) == ZC_BAD_HANDLE) {
		return (ZC_BAD_HANDLE);
	}

	*root = xmlDocGetRootElement(handle->zc_dh_doc);

	if (*root == NULL) {
		return (ZC_EMPTY_DOCUMENT);
	}

	if (xmlStrcmp((*root)->name, DTD_ELEM_ZONE)) {
		return (ZC_WRONG_DOC_TYPE);
	}

	return (ZC_OK);
}

//
// "Rewind" the document tree pointers to the root of the tree.
//
static int
operation_prep(zc_dochandle_t handle)
{
	xmlNodePtr root;
	int err;

	if ((err = getroot(handle, &root)) != ZC_OK) {
		return (err);
	}

	handle->zc_dh_cur = root;
	handle->zc_dh_top = root;
	return (ZC_OK);
}

//
// Convenience function to get a property specified by "propname", at the XML
// node pointed to by "cur". The result is stored in "dst". Storage for the
// result must have been allocated by the caller.
//
static int
fetchprop(xmlNodePtr cur, const xmlChar *propname, char *dst, size_t dstsize)
{
	xmlChar *property;
	size_t srcsize;

	if ((property = xmlGetProp(cur, propname)) == NULL) {
		return (ZC_BAD_PROPERTY);
	}
	srcsize = strlcpy(dst, (char *)property, dstsize);
	xmlFree(property);
	if (srcsize >= dstsize) {
		return (ZC_TOO_BIG);
	}
	return (ZC_OK);
}

//
// Like fetchprop above, but allocates memory for the property value. The
// caller is responsible for freeing the allocated memory.
//
static int
fetch_alloc_prop(xmlNodePtr cur, const xmlChar *propname, char **dst)
{
	xmlChar *property;

	if ((property = xmlGetProp(cur, propname)) == NULL) {
		return (ZC_BAD_PROPERTY);
	}
	if ((*dst = strdup((char *)property)) == NULL) {
		xmlFree(property);
		return (ZC_NOMEM);
	}
	xmlFree(property);
	return (ZC_OK);
}

//
// Convenience function to check whether or not the current XML node, pointed
// to by "cur" has a property of type "attr", of value stored in "user_prop".
// Returns true if it's a match, false otherwise.
//
static bool
match_prop(xmlNodePtr cur, const xmlChar *attr, const char *user_prop)
{
	xmlChar *gotten_prop;
	int prop_result;

	gotten_prop = xmlGetProp(cur, attr);
	if (gotten_prop == NULL) {
		return (false);
	}
	prop_result = xmlStrcmp(gotten_prop, (const xmlChar *) user_prop);
	xmlFree(gotten_prop);
	return ((prop_result == 0));
}

static int
getrootattr(zc_dochandle_t handle, const xmlChar *propname,
    char *propval, size_t propsize)
{
	xmlNodePtr root;
	int err;

	if ((err = getroot(handle, &root)) != ZC_OK) {
		return (err);
	}

	return (fetchprop(root, propname, propval, propsize));
}

static int
get_alloc_rootattr(zc_dochandle_t handle, const xmlChar *propname,
    char **propval)
{
	xmlNodePtr root;
	int err;

	if ((err = getroot(handle, &root)) != ZC_OK) {
		return (err);
	}

	return (fetch_alloc_prop(root, propname, propval));
}

static int
setrootattr(zc_dochandle_t handle, const xmlChar *propname,
    const char *propval)
{
	int err;
	xmlNodePtr root;

	if (propval == NULL) {
		return (ZC_INVAL);
	}

	if ((err = getroot(handle, &root)) != ZC_OK) {
		return (err);
	}

	if (xmlSetProp(root, propname, (const xmlChar *)propval) == NULL) {
		return (ZC_INVAL);
	}
	return (ZC_OK);
}

//
// Convenience function to get a "per-node" net entry from the CCR element
// sequence and populate the net structure. The "start" argument is the
// sequence index at which the "net" CCR entry begins.
//
static int
fetch_local_net(ccr::element_seq_var &seq, struct zc_nodetab *tabp,
    uint_t start)
{
	int err;
	struct zc_nwiftab net_elem;

	//
	// seq[start], seq[start+1], seq[start + 2] are address, physical and
	// Default router respectively
	//
	// TODO figure out how to handle exclusive IP type zones where there
	// is no 'address' entry.
	//
	(void) strcpy(net_elem.zc_nwif_address, (char *)seq[start].data);
	(void) strcpy(net_elem.zc_nwif_physical, (char *)seq[start+1].data);
	(void) strcpy(net_elem.zc_nwif_defrouter, (char *)seq[start+2].data);

	err = zccfg_add_net_elem(tabp, &net_elem);

	return (err);
}

//
// Function to populate a "node" structure from the CCR sequence, and insert
// the node into the XML tree "handle". "start" specifies the starting index in
// the CCR sequence of this particular "node" element.
//
static int
populate_node_ent(ccr::element_seq_var &seq, zc_dochandle_t handle,
    uint_t start, int nodeid)
{
	int			err;
	int			propid;
	uint_t			len;
	uint_t			idx;
	char			buff[MAXPATHLEN];
	char			addr_prop_str[MAXNAMELEN];
	struct zc_nodetab	nodetab;

	len = seq.length();

	//
	// seq[start] and seq[start+1] are node name and hostname respectively.
	//
	(void) strcpy(nodetab.zc_nodename, (char *)seq[start].data);
	(void) strcpy(nodetab.zc_hostname, (char *)seq[start+1].data);

	// for now
	nodetab.zc_nwif_listp = NULL;

	//
	// "start+num_node_attrs" is the sequence index from which the
	// "per-node" resources are stored. We look for each resource type in
	// turn; and whenever a match is found, we update "nodetab" with that
	// data.
	//
	propid = 1;
	for (idx = start + num_node_attrs; idx < len; idx++) {
		(void) sprintf(addr_prop_str, ZC_NODE_NET_ADDRESS,
		    nodeid, propid);
		(void) sprintf(buff, (char *)seq[idx].key);
		if (strncmp(addr_prop_str, buff,
		    sizeof (addr_prop_str)) == 0) {
			err = fetch_local_net(seq, &nodetab, idx);
			if (err != ZC_OK) {
				return (err);
			}
			propid++;
		}
	}

	return (zccfg_add_node(handle, &nodetab));
}

//
// Get per-node properties and resources from the CCR and update "handle".
//
static int
fetch_node_local_props(ccr::element_seq_var &seq, zc_dochandle_t handle)
{
	int		err;
	int		nodeid;
	uint_t		len;
	uint_t		idx;
	char		buff[MAXPATHLEN];
	char		nodename_str[SYS_NMLN];

	len = seq.length();

	//
	// Fetch node resources.
	//
	nodeid = 1;
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(nodename_str, sizeof (nodename_str),
		    ZC_NODE_NAME, nodeid);
		(void) snprintf(buff, sizeof (buff), (char *)seq[idx].key);
		if (strncmp(buff, nodename_str,
		    sizeof (nodename_str)) == 0) {
			err = populate_node_ent(seq, handle, idx, nodeid);
			if (err != ZC_OK) {
				return (err);
			}
			nodeid++;
		}
	}

	return (ZC_OK);
}

//
// Fetch the global scope properties of the zone. For each property type, we
// perform a simple linear search of the CCR element sequence. This is ok since
// the number of elements in the sequence is small (i.e., not tens of
// tousands). We examine each "key" of the sequence and compare it with a
// string defining the property type. If it's a match, we update "handle" with
// the data. Since there is only one instance of a property type, each time
// one is found we can break the search and move on to the next property type.
// This is the general algorithm for all the functions that convert CCR data
// into XML.
//
// Also see the block comment above init_handle_from_ccr().
//
static int
fetch_global_props(ccr::element_seq_var &seq, zc_dochandle_t handle)
{
	int	err;
	uint_t	len;
	uint_t	idx;
	char	buff[MAXPATHLEN]; // should be enough to hold the CCR key
	bool	true_false;

	len = seq.length();

	//
	// Fetch the zonename.
	//
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), "%s",
		    (char *)seq[idx].key);
		if (strncmp(buff, ZC_ZONE_NAME, sizeof (buff)) == 0) {
			err = zccfg_set_name(handle,
			    (char *)seq[idx].data);
			if (err == ZC_OK) {
				break;
			} else {
				return (err);
			}
		}
	}
	//
	// Fetch zonepath.
	//
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), "%s",
		    (char *)seq[idx].key);
		if (strncmp(buff, ZC_ZONE_PATH, sizeof (buff)) == 0) {
			err = zccfg_set_zonepath(handle,
			    (char *)seq[idx].data);
			if (err == ZC_OK) {
				break;
			} else {
				return (err);
			}
		}
	}
	//
	// Fetch autoboot.
	//
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), "%s",
		    (char *)seq[idx].key);
		if (strncmp(buff, ZC_ZONE_AUTOBOOT, sizeof (buff)) == 0) {
			(void) snprintf(buff, sizeof (buff), "%s",
			    (char *)seq[idx].data);
			if (strncmp(buff, "true", sizeof (buff)) == 0) {
				true_false = true;
			} else if (strncmp(buff, "false", sizeof (buff)) == 0) {
				true_false = false;
			} else {
				return (ZC_BAD_PROPERTY);
			}

			err = zccfg_set_autoboot(handle, true_false);
			if (err == ZC_OK) {
				break;
			} else {
				return (err);
			}
		}
	}
	// Fetch ip-type
	zc_iptype_t iptype;
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), "%s",
		    (char *)seq[idx].key);
		if (strncmp(buff, ZC_ZONE_IPTYPE, sizeof (buff)) == 0) {
			(void) snprintf(buff, sizeof (buff), "%s",
			    (char *)seq[idx].data);
			if (strncmp(buff, "shared", sizeof (buff)) == 0) {
				iptype = CZS_SHARED;
			} else if (strncmp(buff, "exclusive",
			    sizeof (buff)) == 0) {
				iptype = CZS_EXCLUSIVE;
			} else {
				return (ZC_BAD_PROPERTY);
			}
			err = zccfg_set_iptype(handle, iptype);
			if (err == ZC_OK) {
				break;
			} else {
				return (err);
			}
		}
	}
	// Fetch bootargs
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), "%s",
		    (char *)seq[idx].key);
		if (strncmp(buff, ZC_ZONE_BOOTARGS, sizeof (buff)) == 0) {
			err = zccfg_set_bootargs(handle,
			    (const char *)seq[idx].data);
			if (err == ZC_OK) {
				break;
			} else {
				return (err);
			}
		}
	}
	// Fetch limitpriv
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), "%s",
		    (char *)seq[idx].key);
		if (strncmp(buff, ZC_ZONE_LIMITPRIV, sizeof (buff)) == 0) {
			err = zccfg_set_limitpriv(handle,
			    (const char *)seq[idx].data);
			if (err == ZC_OK) {
				break;
			} else {
				return (err);
			}
		}
	}
	// Fetch scheduling-class
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), "%s",
		    (char *)seq[idx].key);
		if (strncmp(buff, ZC_ZONE_SCHED_CLASS,
		    sizeof (buff)) == 0) {
			err = zccfg_set_sched(handle,
			    (const char *)seq[idx].data);
			if (err == ZC_OK) {
				break;
			} else {
				return (err);
			}
		}
	}
	// Fetch pool
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), "%s",
		    (char *)seq[idx].key);
		if (strncmp(buff, ZC_ZONE_POOL,
		    sizeof (buff)) == 0) {
			err = zccfg_set_pool(handle,
			    (const char *)seq[idx].data);
			if (err == ZC_OK) {
				break;
			} else {
				return (err);
			}
		}
	}
	// Fetch clprivnet
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), "%s",
		    (char *)seq[idx].key);
		if (strncmp(buff, ZC_ZONE_CLPRIVNET, sizeof (buff)) == 0) {
			(void) snprintf(buff, sizeof (buff), "%s",
			    (char *)seq[idx].data);
			if (strncmp(buff, "true", sizeof (buff)) == 0) {
				true_false = true;
			} else if (strncmp(buff, "false", sizeof (buff)) == 0) {
				true_false = false;
			} else {
				return (ZC_BAD_PROPERTY);
			}

			err = zccfg_set_clprivnet(handle, true_false);
			if (err == ZC_OK) {
				break;
			} else {
				return (err);
			}
		}
	}

	/*
	 * Note that we don't have to fetch "brand" as this is always fixed for
	 * a ZC.
	 */

	return (ZC_OK);
}

//
// Get sysid resource from the CCR and insert it to "handle".
//
static int
fetch_sysid(ccr::element_seq_var &seq, uint_t start, zc_dochandle_t handle)
{
	struct zc_sysidtab sysidtab;

	//
	// seq[start] is zc_root_password, etc.
	//
	sysidtab.zc_root_password = strdup(seq[start].data);
	if (sysidtab.zc_root_password == NULL) {
		return (ZC_NOMEM);
	}
	(void) strcpy(sysidtab.zc_name_service, seq[start + 1].data);
	(void) strcpy(sysidtab.zc_nfs4_domain, seq[start + 2].data);
	(void) strcpy(sysidtab.zc_sec_policy, seq[start + 3].data);
	(void) strcpy(sysidtab.zc_sys_locale, seq[start + 4].data);
	(void) strcpy(sysidtab.zc_terminal, seq[start + 5].data);
	(void) strcpy(sysidtab.zc_timezone, seq[start + 6].data);

	return (zccfg_add_sysid(handle, &sysidtab));
}

//
// Fetch a global scope file system entry from the CCR element seq and insert
// it into the XML tree referred to by handle.
//
// "propid" is the file system resource's ID in the CCR.
// "start" is the index into seq_v at which this file system resource entry
// begins.
//
static int
fetch_global_scope_fs(ccr::element_seq_var &seq_v, zc_dochandle_t handle,
    uint_t propid, uint_t start)
{
	struct zc_fstab fstab = {"", "", "", NULL, ""};
	uint_t		idx;
	int		err;
	uint_t		opt_id;
	char		buff[MAXPATHLEN];

	//
	// seq[start] is zone.filesystem.propid.directory
	// seq[start+1] is zone.filesystem.propid.special
	// seq[start+2] is zone.filesystem.propid.raw
	// seq[start+3] is zone.filesystem.propid.type
	//
	(void) strcpy(fstab.zc_fs_dir, seq_v[start].data);
	(void) strcpy(fstab.zc_fs_special, seq_v[start + 1].data);

	// raw is optional.
	if (strlen((const char *)seq_v[start + 2].data) > 0) {
		(void) strcpy(fstab.zc_fs_raw, seq_v[start + 2].data);
	}
	(void) strcpy(fstab.zc_fs_type, seq_v[start + 3].data);

	//
	// Add mount options, if any, for this fs.
	//
	opt_id = 1;
	for (idx = start + 4; idx < seq_v.length(); idx++) {
		(void) snprintf(buff, sizeof (buff), ZC_ZONE_FS_OPT,
		    propid, opt_id);
		if (strncmp(buff, (char *)seq_v[idx].key, sizeof (buff)) == 0) {
			err = zccfg_add_fs_option(&fstab,
			    (const char *)seq_v[idx].data);
			if (err != ZC_OK) {
				return (err);
			}
			opt_id++;
		}
	}

	return (zccfg_add_filesystem(handle, &fstab));
}

//
// Fetch a global scope network entry from the CCR element seq and insert
// it into the XML tree referred to by handle.
//
// "start" is the index into seq_v at which this network resource entry
// begins.
//
static int
fetch_global_scope_net(ccr::element_seq_var &seq_v, zc_dochandle_t handle,
    uint_t start)
{
	struct zc_nwiftab nwiftab = {"", "", ""};

	//
	// seq[start] is zone.network.propid.address
	// seq[start + 1] is zone.network.propid.physical
	//
	(void) strcpy(nwiftab.zc_nwif_address, seq_v[start].data);
	(void) strcpy(nwiftab.zc_nwif_physical, seq_v[start + 1].data);

	// Add it to the handle
	return (zccfg_add_nwif(handle, &nwiftab));
}

//
// Fetch a global scope rctl entry from seq_v and add it to 'handle'.
//
// "propid" is the rctl resource's ID in the CCR.
// "start" is the index in seq_v at which this rctl entry begins.
//
static int
fetch_global_scope_rctl(ccr::element_seq_var &seq_v, zc_dochandle_t handle,
    uint_t propid, uint_t start)
{
	int 			err;
	uint_t			val_id;	// 'value id' of the rctl value
	struct zc_rctltab	rctltab = { "", NULL};
	struct zc_rctlvaltab	valtab = { "", "", "", NULL};
	char			buff[MAXPATHLEN];

	//
	// seq_v[start].key is zone.rctl.propid.name
	// seq_v[start + 1].key is zone.rctl.propid.value.val_id.priv
	// seq_v[start + 2].key is zone.rctl.propid.value.val_id.limit
	// seq_v[start + 3].key is zone.rctl.propid.value.val_id.action
	//
	// A given rctl entry may have more than one rctl values. We look for
	// all rctl values for this particular rctl entry, identified by
	// 'propid'.
	//
	(void) strcpy(rctltab.zc_rctl_name, seq_v[start].data);

	val_id = 1;
	for (uint_t i = start + 1; i < seq_v.length(); i++) {
		(void) snprintf(buff, sizeof (buff), ZC_ZONE_RCTLVAL_PRIV,
		    propid, val_id);
		if (strncmp(buff, (char *)seq_v[i].key, sizeof (buff)) == 0) {
			//
			// Grab seq_v[i], seq_v[i + 1], seq_v[i + 2].
			//
			(void) strcpy(valtab.zc_rctlval_priv,
			    (const char *)seq_v[i].data);
			(void) strcpy(valtab.zc_rctlval_limit,
			    (const char *)seq_v[i + 1].data);
			(void) strcpy(valtab.zc_rctlval_action,
			    (const char *)seq_v[i + 2].data);

			err = zccfg_add_rctl_value(&rctltab, &valtab);
			if (err != ZC_OK) {
				return (err);
			}

			i += 3; // skip ahead to the next rctlval entry
			val_id++;
			continue;
		}
	}

	return (zccfg_add_rctl(handle, &rctltab));
}

//
// Extract global scope zone resources (fs, IPD, net, device, dataset, rctl,
// sysid, attr) from the CCR seq and insert them into the XML tree referred to
// by handle.
//
// We do not expect to have thousands of elements in the sequence, so the
// searching method we use to find data is a simple linear search trough the
// sequence for each resource type.
//
static int
fetch_global_resources(ccr::element_seq_var &seq, zc_dochandle_t handle)
{
	int			err;
	uint_t			propid;
	uint_t			len;
	uint_t			idx;
	char			buff[MAXPATHLEN];
	char			ipd_str[MAXPATHLEN];
	struct zc_fstab		fstab;
	struct zc_devtab	devtab;
	struct zc_dstab		dstab;

	len = seq.length();

	//
	// Fetch sysid settings
	//
	for (idx = 0; idx < len; idx++) {
		if (strncmp(ZC_SYSID_ROOT_PASSWORD, (char *)seq[idx].key,
		    sizeof (ZC_SYSID_ROOT_PASSWORD)) == 0) {
			err = fetch_sysid(seq, idx, handle);
			if (err != ZC_OK) {
				return (err);
			}
			// There can only be one sysid resource.
			break;
		}
	}
	//
	// Fetch inherited-pkg-dir's.
	//
	propid = 1;
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(ipd_str, sizeof (ipd_str), ZC_ZONE_IPDS,
		    propid);
		(void) snprintf(buff, sizeof (buff), (char *)seq[idx].key);
		if (strncmp(buff, ipd_str, sizeof (ipd_str)) == 0) {
			(void) strlcpy(fstab.zc_fs_dir,
			    (const char *)seq[idx].data,
			    sizeof (fstab.zc_fs_dir));
			err = zccfg_add_ipd(handle, &fstab);
			if (err != ZC_OK) {
				return (err);
			}
			propid++;
		}
	}
	//
	// Fetch devices
	//
	propid = 1;
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(ipd_str, sizeof (ipd_str), ZC_ZONE_DEVICE,
		    propid);
		(void) snprintf(buff, sizeof (buff), (char *)seq[idx].key);
		if (strncmp(buff, ipd_str, sizeof (ipd_str)) == 0) {
			(void) strlcpy(devtab.zc_dev_match,
			    (const char *)seq[idx].data,
			    sizeof (devtab.zc_dev_match));
			err = zccfg_add_dev(handle, &devtab);
			if (err != ZC_OK) {
				return (err);
			}
			propid++;
		}
	}
	//
	// Fetch ZFS datasets
	//
	propid = 1;
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(ipd_str, sizeof (ipd_str), ZC_ZONE_DATASET,
		    propid);
		(void) snprintf(buff, sizeof (buff), (char *)seq[idx].key);
		if (strncmp(buff, ipd_str, sizeof (ipd_str)) == 0) {
			(void) strlcpy(dstab.zc_dataset_name,
			    (const char *)seq[idx].data,
			    sizeof (dstab.zc_dataset_name));
			err = zccfg_add_ds(handle, &dstab);
			if (err != ZC_OK) {
				return (err);
			}
			propid++;
		}
	}
	//
	// Fetch global scope filesystems.
	//
	propid = 1;
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), ZC_ZONE_FS_DIR,
		    propid);
		if (strncmp(buff, (char *)seq[idx].key, sizeof (buff)) == 0) {
			err = fetch_global_scope_fs(seq, handle, propid, idx);
			if (err != ZC_OK) {
				return (err);
			}
			propid++;
		}
	}
	//
	// Fetch global scope network resources.
	//
	propid = 1;
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), ZC_ZONE_NET_ADDRESS,
		    propid);
		if (strncmp(buff, (char *)seq[idx].key, sizeof (buff)) == 0) {
			err = fetch_global_scope_net(seq, handle, idx);
			if (err != ZC_OK) {
				return (err);
			}
			propid++;
		}
	}
	//
	// Fetch global scope rctl's.
	//
	propid = 1;
	for (idx = 0; idx < len; idx++) {
		(void) snprintf(buff, sizeof (buff), ZC_ZONE_RCTL_NAME,
		    propid);
		if (strncmp(buff, (char *)seq[idx].key, sizeof (buff)) == 0) {
			err = fetch_global_scope_rctl(seq, handle, propid,
			    idx);
			if (err != ZC_OK) {
				return (err);
			}
			propid++;
		}
	}
	//
	// Fetch capped memory, if any.
	//
	(void) snprintf(buff, sizeof (buff), ZC_ZONE_MCAP_PHYSCAP);
	for (idx = 0; idx < len; idx++) {
		if (strncmp(buff, (char *)seq[idx].key, sizeof (buff)) == 0) {
			struct zc_mcaptab mcaptab;

			(void) strcpy(mcaptab.zc_physmem_cap,
			    (char *)seq[idx].data);
			err = zccfg_modify_mcap(handle, &mcaptab);
			if (err != ZC_OK) {
				return (err);
			}
			// There can only be one capped memory resource.
			break;
		}
	}
	//
	// Fetch dedicated CPU, if any.
	//
	(void) snprintf(buff, sizeof (buff), ZC_ZONE_PSET_NCPU_MIN);
	for (idx = 0; idx < len; idx++) {
		if (strncmp(buff, (char *)seq[idx].key, sizeof (buff)) == 0) {
			struct zc_psettab psettab;

			// The next three elements are what we want.
			(void) strcpy(psettab.zc_ncpu_min,
			    (char *)seq[idx].data);
			(void) strcpy(psettab.zc_ncpu_max,
			    (char *)seq[idx + 1].data);
			(void) strcpy(psettab.zc_importance,
			    (char *)seq[idx + 2].data);

			err = zccfg_add_pset(handle, &psettab);
			if (err != ZC_OK) {
				return (err);
			}
			// There can only be one pset resource.
			break;
		}
	}

	return (ZC_OK);
}

//
// init_handle_from_ccr
//
// This function reads the zone cluster configuration stored in the CCR
// and populates the XML tree pointed to by 'handle' with that data.
// The CCR stores data in [key, value] pairs, and when the CCR table
// is read, the [key, value] pairs are stored in a linear sequence. This makes
// transforming the (unstructured) CCR data into an XML tree somewhat
// inelegant. Perhaps there are more elegant ways to do that than the method
// used here - just inspect each 'key' and look for 'interesting' patterns.
//
// For example, if we want to fetch device entries, we inspect each key, and
// see if it looks like "zone.device.n.match", where 'n' is a non-zero
// integer. If it's a match for some value of 'n', we read the corresponding
// 'value' and use that to initialize the 'device' node in our XML tree.
//
// For the sake of clarity, the CCR elements are parsed in three different
// helper functions:
//
// fetch_global_props() to fetch the global scope "properties".
// fetch_global_resources() to fetch the global scope "resources".
// fetch_node_local_props() to fetch properties and resources in the node scope
//
// Returns:
//	ZC_OK on success
//	ZC_* on failure
//
static int
init_handle_from_ccr(const char *zonename, zc_dochandle_t handle)
{
	ccr::readonly_table_ptr	rtable_p;
	ccr::directory_var	directory_v;
	ccr::element_seq_var	seq_v;
	int32_t			num_elem; // number of elements in CCR table
	uint_t			cluster_id;
	int			err;
	Environment		e;
	CORBA::Exception	*exp;

	//
	// Call get_cluster_id() here and return ZC_NO_ZONE if an invalid ID is
	// returned.
	//
	if ((err = clconf_get_cluster_id((char *)zonename,
	    &cluster_id)) != 0) {
		return (err == ENOENT ? ZC_NO_ZONE : err);
	}

	naming::naming_context_var ctx_v = ns::local_nameserver();
	assert(!CORBA::is_nil(ctx_v));
	if (CORBA::is_nil(ctx_v)) {
		return (ZC_NAMESERVER_ERR);
	}

	CORBA::Object_var obj_v = ctx_v->resolve("ccr_directory", e);
	if (e.exception()) {
		e.clear();
		return (ZC_NAMESERVER_ERR);
	}
	directory_v = ccr::directory::_narrow(obj_v);

	rtable_p = directory_v->lookup_zc(zonename, "clzone_config", e);
	if ((exp = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(exp)) {
			//
			// A case of partially configured zone cluster.
			// We have a valid zone cluster ID, but the
			// configuration table was not created. The
			// configuration utility probably crashed while
			// committing the configuration.
			//
			err = ZC_MISSING_CONFIG;
		} else {
			err = ZC_CCR_ERROR;
		}
		e.clear();
		return (err);
	}

	num_elem = rtable_p->get_num_elements(e);
	if (e.exception()) {
		e.clear();
		CORBA::release(rtable_p);
		return (ZC_CCR_ERROR);
	}

	rtable_p->atfirst(e);
	if (e.exception()) {
		e.clear();
		CORBA::release(rtable_p);
		return (ZC_CCR_ERROR);
	}

	rtable_p->next_n_elements((uint32_t)num_elem, seq_v, e);
	if (e.exception()) {
		e.clear();
		CORBA::release(rtable_p);
		return (ZC_CCR_ERROR);
	}
	CORBA::release(rtable_p);

	//
	// Initialize the handle to point to a "blank" configuration, as
	// defined in SUNWclblank.xml. Then populate the "blank" XML tree
	// with data read from the CCR.
	//
	char filename[MAXPATHLEN];
	(void) snprintf(filename, sizeof (filename), "%s/%s",
	    ZC_CONFIG_ROOT, "SUNWclblank.xml");
	handle->zc_dh_doc = xmlParseFile(filename);

	if ((err = fetch_global_props(seq_v, handle)) != ZC_OK) {
		return (err);
	}

	if ((err = fetch_global_resources(seq_v, handle)) != ZC_OK) {
		return (err);
	}

	if ((err = fetch_node_local_props(seq_v, handle)) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

//
// Main implementation function to get/populate handle for the specified
// zonename, using filename as a template if specified. Otherwise if
// filename is NULL, we open the "blank" template and populate the handle
// with data retrieved from the CCR.
//
static int
zccfg_get_handle_impl(const char *zonename, const char *filename,
    zc_dochandle_t handle)
{
	xmlValidCtxtPtr			cvp;
	struct stat			statbuf;
	int				valid;
	version_manager::vp_version_t 	ns_version;

	ns_version = ns::get_ns_version();
	if (ns_version.major_num  < 2) {
		return (ZC_VERSION_ERR);
	}

	if (zonename == NULL) {
		return (ZC_NO_ZONE);
	}
	if (filename == NULL) {
		return (init_handle_from_ccr(zonename, handle));
	}

	if ((handle->zc_dh_doc = xmlParseFile(filename)) == NULL) {
		// distinguish file not found vs. found but not parsed
		if (stat(filename, &statbuf) == 0) {
			//
			// SCMSGS
			// @explanation
			// libzccfg was unable to parse the specified zone
			// cluster template file. The template file may be
			// corrupt.
			// @user_action
			// Contact your authorized Sun service
			// provider to determine whether a workaround or patch
			// is available.
			//
			(void) logger.log(LOG_ERR, MESSAGE, "%s: Invalid zone "
			    "cluster template.\n", filename);
			return (ZC_INVALID_DOCUMENT);
		} else {
			//
			// It's possible that we're trying to use an existing
			// zone cluster as a template to configure a new one.
			// If this is the case, the existing zone cluster
			// exists in the CCR, but not in ZC_CONFIG_ROOT. Get
			// the info from the CCR.
			//
			return (init_handle_from_ccr(zonename, handle));
		}
	}
	if ((cvp = xmlNewValidCtxt()) == NULL) {
		return (ZC_NOMEM);
	}
	cvp->error = zccfg_error_func;
	cvp->warning = zccfg_error_func;
	valid = xmlValidateDocument(cvp, handle->zc_dh_doc);
	xmlFreeValidCtxt(cvp);
	if (valid == 0) {
		return (ZC_INVALID_DOCUMENT);
	}

	return (ZC_OK);
}

//
// Get handle for the specified zonename. Just turns around and calls
// zccfg_get_handle_impl with a NULL filename argument.
//
int
zccfg_get_handle(const char *zonename, zc_dochandle_t handle)
{
	return (zccfg_get_handle_impl(zonename, NULL, handle));
}

//
// Get the handle for the specified zonename, using templ as a template. This
// turns around and calls zccfg_get_handle_impl with a non-NULL filename
// argument.
//
int
zccfg_get_template_handle(const char *templ, const char *zonename,
    zc_dochandle_t handle)
{
	char path[MAXPATHLEN];
	int err;

	if (!config_file_path(templ, path)) {
		return (ZC_MISC_FS);
	}
	if ((err = zccfg_get_handle_impl(templ, path, handle)) != ZC_OK) {
		return (err);
	}
	return (setrootattr(handle, DTD_ATTR_NAME, zonename));
}

int
zccfg_get_name(zc_dochandle_t handle, char *name, size_t namesize)
{
	return (getrootattr(handle, DTD_ATTR_NAME, name, namesize));
}

//
// Validate that the zonename doesn't contain illegal characters.
//
int
zccfg_validate_zonename(const char *zonename)
{
	int i;

	if (!((zonename[0] >= 'a' && zonename[0] <= 'z') ||
	    (zonename[0] >= 'A' && zonename[0] <= 'Z') ||
	    (zonename[0] >= '0' && zonename[0] <= '9'))) {
		return (ZC_BOGUS_ZONE_NAME);
	}

	for (i = 1; zonename[i] != '\0'; i++) {
		if (!((zonename[i] >= 'a' && zonename[i] <= 'z') ||
		    (zonename[i] >= 'A' && zonename[i] <= 'Z') ||
		    (zonename[i] >= '0' && zonename[i] <= '9') ||
		    (zonename[i] == '-') || (zonename[i] == '_') ||
		    (zonename[i] == '.'))) {
			return (ZC_BOGUS_ZONE_NAME);
		}
	}

	return (ZC_OK);
}

//
// Changing the name of a zone cluster will not be allowed initially.
// may be implemented later.
//
int
zccfg_set_name(zc_dochandle_t handle, const char *name)
{
	return (setrootattr(handle, DTD_ATTR_NAME, name));
}

int
zccfg_get_zonepath(zc_dochandle_t handle, char *path, size_t pathsize)
{
	return (getrootattr(handle, DTD_ATTR_ZONEPATH, path, pathsize));
}

int
zccfg_set_zonepath(zc_dochandle_t handle, const char *zonepath)
{
	return (setrootattr(handle, DTD_ATTR_ZONEPATH, zonepath));
}

int
zccfg_get_autoboot(zc_dochandle_t handle, bool *autoboot)
{
	char autobootstr[DTD_ENTITY_BOOL_LEN];
	int ret = ZC_OK;

	if ((ret = getrootattr(handle, DTD_ATTR_AUTOBOOT, autobootstr,
	    sizeof (autobootstr))) != ZC_OK) {
		return (ret);
	}

	if (strcmp(autobootstr, DTD_ENTITY_FALSE) == 0) {
		*autoboot = B_FALSE;
	} else if (strcmp(autobootstr, DTD_ENTITY_TRUE) == 0) {
		*autoboot = B_TRUE;
	} else {
		ret = ZC_BAD_PROPERTY;
	}
	return (ret);
}

int
zccfg_set_autoboot(zc_dochandle_t handle, bool autoboot)
{
	return (setrootattr(handle, DTD_ATTR_AUTOBOOT,
	    autoboot ? DTD_ENTITY_TRUE: DTD_ENTITY_FALSE));
}

int
zccfg_get_clprivnet(zc_dochandle_t handle, bool *clprivnet)
{
	char clprivnetstr[DTD_ENTITY_BOOL_LEN];
	int ret = ZC_OK;

	if ((ret = getrootattr(handle, DTD_ATTR_CLPRIVNET, clprivnetstr,
	    sizeof (clprivnetstr))) != ZC_OK) {
		return (ret);
	}

	if (strcmp(clprivnetstr, DTD_ENTITY_FALSE) == 0) {
		*clprivnet = B_FALSE;
	} else if (strcmp(clprivnetstr, DTD_ENTITY_TRUE) == 0) {
		*clprivnet = B_TRUE;
	} else {
		ret = ZC_BAD_PROPERTY;
	}
	return (ret);
}

int
zccfg_set_clprivnet(zc_dochandle_t handle, bool clprivnet)
{
	return (setrootattr(handle, DTD_ATTR_CLPRIVNET,
	    clprivnet ? DTD_ENTITY_TRUE: DTD_ENTITY_FALSE));
}

int
zccfg_get_brand(zc_dochandle_t handle, char *brandp, size_t brandsize)
{
	return (getrootattr(handle, DTD_ATTR_BRAND, brandp, brandsize));
}

int
zccfg_set_brand(zc_dochandle_t handle, const char *brandp)
{
	return (setrootattr(handle, DTD_ATTR_BRAND, brandp));
}

int
zccfg_get_pool(zc_dochandle_t handle, char *poolnamep, size_t poolsize)
{
	return (getrootattr(handle, DTD_ATTR_POOL, poolnamep, poolsize));
}

int
zccfg_set_pool(zc_dochandle_t handle, const char *poolnamep)
{
	return (setrootattr(handle, DTD_ATTR_POOL, poolnamep));
}

int
zccfg_get_limitpriv(zc_dochandle_t handle, char **limitprivp)
{
	return (get_alloc_rootattr(handle, DTD_ATTR_LIMITPRIV, limitprivp));
}

int
zccfg_set_limitpriv(zc_dochandle_t handle, const char *limitprivp)
{
	return (setrootattr(handle, DTD_ATTR_LIMITPRIV, limitprivp));
}

int
zccfg_get_bootargs(zc_dochandle_t handle, char *bootargsp,
    size_t bargssize)
{
	return (getrootattr(handle, DTD_ATTR_BOOTARGS, bootargsp, bargssize));
}

int
zccfg_set_bootargs(zc_dochandle_t handle, const char *bootargsp)
{
	return (setrootattr(handle, DTD_ATTR_BOOTARGS, bootargsp));
}

int
zccfg_get_sched_class(zc_dochandle_t handle, char *schedp,
    size_t schedsize)
{
	return (getrootattr(handle, DTD_ATTR_SCHED, schedp, schedsize));
}

int
zccfg_set_sched(zc_dochandle_t handle, const char *schedp)
{
	return (setrootattr(handle, DTD_ATTR_SCHED, schedp));
}

int
zccfg_get_iptype(zc_dochandle_t handle, zc_iptype_t *iptypep)
{
	char property[10]; // 10 is big enough for "shared"/"exclusive"
	int err;

	err = getrootattr(handle, DTD_ATTR_IPTYPE, property, sizeof (property));
	if (err == ZC_BAD_PROPERTY) {
		// Return default value
		*iptypep = CZS_SHARED;
		return (ZC_OK);
	} else if (err != ZC_OK) {
		return (err);
	}

	if (strlen(property) == 0 ||
	    strcmp(property, "shared") == 0) {
		*iptypep = CZS_SHARED;
	} else if (strcmp(property, "exclusive") == 0) {
		*iptypep = CZS_EXCLUSIVE;
	} else {
		return (ZC_INVAL);
	}
	return (ZC_OK);
}

int
zccfg_set_iptype(zc_dochandle_t handle, zc_iptype_t iptype)
{
	xmlNodePtr cur;

	if (handle == NULL) {
		return (ZC_INVAL);
	}

	cur = xmlDocGetRootElement(handle->zc_dh_doc);
	if (cur == NULL) {
		return (ZC_EMPTY_DOCUMENT);
	}

	if (xmlStrcmp(cur->name, DTD_ELEM_ZONE) != 0) {
		return (ZC_WRONG_DOC_TYPE);
	}
	switch (iptype) {
	case CZS_SHARED:
		//
		// Since "shared" is the default, we don't write it to the
		// configuration file.
		// xmlUnsetProp only fails when the attribute doesn't exist,
		// which we don't care.
		//
		(void) xmlUnsetProp(cur, DTD_ATTR_IPTYPE);
		break;
	case CZS_EXCLUSIVE:
		if (xmlSetProp(cur, DTD_ATTR_IPTYPE,
		    (const xmlChar *) "exclusive") == NULL) {
			return (ZC_INVAL);
		}
		break;
	default:
		return (ZC_INVAL);
	}
	return (ZC_OK);
}

int
zccfg_add_fs_option(struct zc_fstab *tabp, const char *option)
{
	zc_fsopt_t *last;
	zc_fsopt_t *old;
	zc_fsopt_t *new_opt;

	last = tabp->zc_fs_options;
	for (old = last; old != NULL; old = old->zc_fsopt_next) {
		last = old;	// walk to the end of the list
	}
	new_opt = (zc_fsopt_t *)malloc(sizeof (zc_fsopt_t));
	if (new_opt == NULL) {
		return (ZC_NOMEM);
	}
	(void) strlcpy(new_opt->zc_fsopt_opt, option,
	    sizeof (new_opt->zc_fsopt_opt));
	new_opt->zc_fsopt_next = NULL;
	if (last == NULL) {
		tabp->zc_fs_options = new_opt;
	} else {
		last->zc_fsopt_next = new_opt;
	}
	return (ZC_OK);
}

int
zccfg_remove_fs_option(struct zc_fstab *tabp, const char *option)
{
	zc_fsopt_t *last;
	zc_fsopt_t *this_opt;
	zc_fsopt_t *next;

	last = tabp->zc_fs_options;
	for (this_opt = last; this_opt != NULL;
	    this_opt = this_opt->zc_fsopt_next) {
		if (strcmp(this_opt->zc_fsopt_opt, option) == 0) {
			next = this_opt->zc_fsopt_next;
			if (this_opt == tabp->zc_fs_options) {
				tabp->zc_fs_options = next;
			} else {
				last->zc_fsopt_next = next;
			}
			free(this_opt);
			return (ZC_OK);
		} else {
			last = this_opt;
		}
	}
	return (ZC_NO_PROPERTY_ID);
}

void
zccfg_free_fs_option_list(zc_fsopt_t *listp)
{
	zc_fsopt_t *this_opt;
	zc_fsopt_t *next;

	for (this_opt = listp; this_opt != NULL; this_opt = next) {
		next = this_opt->zc_fsopt_next;
		free(this_opt);
	}
}

void
zccfg_free_rctl_value_list(struct zc_rctlvaltab *tabp)
{
	if (tabp == NULL) {
		return;
	}
	zccfg_free_rctl_value_list(tabp->zc_rctlval_next);
	free(tabp);
}

void
zccfg_free_nwif_list(zc_nwifelem_t *listp)
{
	zc_nwifelem_t *elemp;
	zc_nwifelem_t *next;

	for (elemp = listp; elemp != NULL; elemp = next) {
		next = elemp->next;
		free(elemp);
	}
}

//
// Convenience function for zccfg_get_aliased_rctl below.
//
static int
get_aliased_rctlval(const xmlNodePtr cur, struct zc_rctlvaltab *tabp, int idx)
{
	bool found_val = false;
	xmlNodePtr val;

	for (val = cur->xmlChildrenNode; val != NULL; val = val->next) {

		//
		// If we already have one value, we can't have
		// an alias since we just found another.
		//
		if (found_val) {
			return (ZC_ALIAS_DISALLOW);
		}
		found_val = true;

		if ((fetchprop(val, DTD_ATTR_PRIV, tabp->zc_rctlval_priv,
		    sizeof (tabp->zc_rctlval_priv)) != ZC_OK)) {
			break;
		}
		if ((fetchprop(val, DTD_ATTR_LIMIT, tabp->zc_rctlval_limit,
		    sizeof (tabp->zc_rctlval_limit)) != ZC_OK)) {
			break;
		}
		if ((fetchprop(val, DTD_ATTR_ACTION,
		    tabp->zc_rctlval_action,
		    sizeof (tabp->zc_rctlval_action)) != ZC_OK)) {
			break;
		}
	} // end for

	// check priv and action match the expected values
	if (strcmp(tabp->zc_rctlval_priv, aliases[idx].priv) != 0 ||
	    strcmp(tabp->zc_rctlval_action, aliases[idx].action) != 0) {
		return (ZC_ALIAS_DISALLOW);
	}
	return (ZC_OK);
}

//
// zccfg_get_aliased_rctl
//
// Copied from libzonecfg.c with small modifications.
//
// Support for aliased rctls; that is, rctls that have simplified names in
// zonecfg.  For example, max-lwps is an alias for a well defined zone.max-lwps
// rctl.  If there are multiple existing values for one of these rctls or if
// there is a single value that does not match the well defined template (i.e.
// it has a different action) then we cannot treat the rctl as having an alias
// so we return ZC_ALIAS_DISALLOW.  That means that the rctl cannot be
// managed in zonecfg via an alias and that the standard rctl syntax must be
// used.
//
// The possible return values are:
// 	ZC_NO_PROPERTY_ID - invalid alias name
// 	ZC_ALIAS_DISALLOW - pre-existing, incompatible rctl definition
// 	ZC_NO_ENTRY - no rctl is configured for this alias
// 	ZC_OK - we got a valid rctl for the specified alias
//
int
zccfg_get_aliased_rctl(zc_dochandle_t handle, char *namep,
    uint64_t *rvalp)
{
	bool				found = false;
	xmlNodePtr			cur;
	char				savedname[MAXNAMELEN];
	struct zc_rctlvaltab		rctl = {"", NULL};
	int				err;
	int				i;

	for (i = 0; aliases[i].shortname != NULL; i++) {
		if (strcmp(namep, aliases[i].shortname) == 0) {
			break;
		}
	}
	if (aliases[i].shortname == NULL) {
		return (ZC_NO_PROPERTY_ID);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	cur = handle->zc_dh_cur;
	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_RCTL) != 0) {
			continue;
		}
		if ((fetchprop(cur, DTD_ATTR_NAME, savedname,
		    sizeof (savedname)) == ZC_OK) &&
		    (strcmp(savedname, aliases[i].realname) == 0)) {

			//
			// If we already saw one of these, we can't have an
			// alias since we just found another.
			//
			if (found) {
				return (ZC_ALIAS_DISALLOW);
			}
			found = true;

			if ((err = get_aliased_rctlval(cur, &rctl, i))
			    != ZC_OK) {
				return (err);
			}
		}
	}

	if (found) {
		*rvalp = strtoull(rctl.zc_rctlval_limit, NULL, 10);
		return (ZC_OK);
	}
	return (ZC_NO_ENTRY);
}

//
// Remove aliased rctl of the specified name. We find the specified rctl in
// the configuration, then call zccfg_delete_rctl to actually delete it
// from the configuration.
//
int
zccfg_rm_aliased_rctl(zc_dochandle_t handle, char *namep)
{
	int			i;
	uint64_t		val;
	struct zc_rctltab	rctltab;

	//
	// First check that we have a valid aliased rctl to remove.
	// This will catch an rctl entry with non-standard values or
	// multiple rctl values for this name.  We need to ignore those
	// rctl entries.
	//
	if (zccfg_get_aliased_rctl(handle, namep, &val) != ZC_OK) {
		return (ZC_OK);
	}

	for (i = 0; aliases[i].shortname != NULL; i++) {
		if (strcmp(namep, aliases[i].shortname) == 0) {
			break;
		}
	}

	if (aliases[i].shortname == NULL) {
		return (ZC_NO_RESOURCE_ID);
	}

	(void) strlcpy(rctltab.zc_rctl_name, aliases[i].realname,
	    sizeof (rctltab.zc_rctl_name));

	return (zccfg_delete_rctl(handle, &rctltab));
}

bool
zccfg_aliased_rctl_ok(zc_dochandle_t handle, char *name)
{
	uint64_t tmp_val;

	switch (zccfg_get_aliased_rctl(handle, name, &tmp_val)) {
	case ZC_OK:
		// FALLTHRU
	case ZC_NO_ENTRY:
		return (true);
	default:
		return (false);
	}
}

int
zccfg_set_aliased_rctl(zc_dochandle_t handle, char *name, uint64_t val)
{
	int			i;
	int			err;
	struct zc_rctltab	rctltab;
	struct zc_rctlvaltab	*valtabp;
	char			buff[128];

	if (!zccfg_aliased_rctl_ok(handle, name)) {
		return (ZC_ALIAS_DISALLOW);
	}

	for (i = 0; aliases[i].shortname != NULL; i++) {
		if (strcmp(name, aliases[i].shortname) == 0) {
			break;
		}
	}

	if (aliases[i].shortname == NULL) {
		return (ZC_NO_RESOURCE_ID);
	}

	// remove any pre-existing definition for this rctl
	(void) zccfg_rm_aliased_rctl(handle, name);

	(void) strlcpy(rctltab.zc_rctl_name, aliases[i].realname,
	    sizeof (rctltab.zc_rctl_name));

	rctltab.zc_rctl_valp = NULL;

	valtabp = (struct zc_rctlvaltab *)calloc(1,
	    sizeof (struct zc_rctlvaltab));
	if (valtabp == NULL) {
		return (ZC_NOMEM);
	}

	(void) snprintf(buff, sizeof (buff), "%llu", (long long)val);

	(void) strlcpy(valtabp->zc_rctlval_priv, aliases[i].priv,
	    sizeof (valtabp->zc_rctlval_priv));
	(void) strlcpy(valtabp->zc_rctlval_limit, buff,
	    sizeof (valtabp->zc_rctlval_limit));
	(void) strlcpy(valtabp->zc_rctlval_action, aliases[i].action,
	    sizeof (valtabp->zc_rctlval_action));

	valtabp->zc_rctlval_next = NULL;

	if ((err = zccfg_add_rctl_value(&rctltab, valtabp)) != ZC_OK) {
		return (err);
	}

	return (zccfg_add_rctl(handle, &rctltab));
}

//
// Create a zone cluster. This operation creates a CCR directory of the same
// name as the zone cluster's name (zonename) - /etc/cluster/ccr/<zonename>.
// It also generates the "cluster ID" for this zone cluster, and adds the
// [<zonename>, <cluster ID>] to the /etc/cluster/ccr/cluster_directory table.
// Then it will create the zc context in the global nameserver.
//
static int
zccfg_create_cluster(zc_dochandle_t handle,
    const ccr::directory_var &ccr_dir_v)
{
	char				zonename[ZONENAME_MAX];
	int				err;
	Environment			e;
	uint_t				cluster_id;
	char				cluster_id_str[30];
	CORBA::Object_ptr 		nsobj = nil;
	naming::naming_context_var	ctx_v;

	if ((err = zccfg_get_name(handle, zonename, sizeof (zonename)))
	    != ZC_OK) {
		return (err);
	}

	//
	// Get the cluster ID. If we get a valid ID, it means that the zone
	// cluster's configuration is being updated so we just return
	// success to the caller.
	//
	if (clconf_get_cluster_id((char *)zonename, &cluster_id) == 0) {
		return (cluster_id > 2 ? ZC_OK : ZC_BAD_CLUSTER_ID);
	}

	ccr_dir_v->zc_create(zonename, e);
	if (e.exception()) {
		//
		// SCMSGS
		// @explanation
		// libzccfg was not able to create the specified zone cluster
		// due to an internal error.
		// @user_action
		// Contact your authorized Sun service provider to
		// determine whether a workaround or patch is available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Could not create zone "
		    "cluster %s due to CCR exception %s\n", zonename,
		    e.exception()->_name());
		e.clear();
		return (ZC_CCR_ERROR);
	}

	// Retrieve the custer id
	err = clconf_get_cluster_id((char *)zonename, &cluster_id);
	if (err != 0) {
		return (ZC_BAD_CLUSTER_ID);
	}

	// Valid ZC ids start from 3
	ASSERT(cluster_id > 2);

	// Create the global nameserver context for the zc
	ctx_v = ns::root_nameserver_root();
	assert(!CORBA::is_nil(ctx_v));
	if (CORBA::is_nil(ctx_v)) {
		return (ZC_NAMESERVER_ERR);
	}

	(void) os::itoa(cluster_id, cluster_id_str);
	nsobj = ctx_v->bind_new_context_v1(cluster_id_str,
	    true, NS_ALLOW_ALL_OPP_PERM, cluster_id, e);
	if (e.exception() || CORBA::is_nil(nsobj)) {
		e.exception()->print_exception(
		    "libzccfg failed to create context for zone cluster\n");
		e.clear();
		CORBA::release(nsobj);
		return (ZC_NAMESERVER_ERR);
	}
	CORBA::release(nsobj);

	return (ZC_OK);
}

//
// Convenience function to append [key, value] to the CCR element sequence.
//
static void
ccr_pack_string(ccr::element_seq &seq, const char *key, const char *value)
{
	unsigned int i = seq.length();

	seq.length(i + 1);
	seq[i].key = key;
	if (value == NULL) {
		seq[i].data = strdup("");
	} else {
		seq[i].data = strdup(value);
	}
}

//
// Add inherited-pkg-dir resources to the CCR element sequence.
//
static int
ccr_put_ipds(ccr::element_seq &seq, zc_dochandle_t handle)
{
	int			idx;
	int			err;
	struct zc_fstab	ipdent;
	char			key[MAXNAMELEN];

	if ((err = zccfg_setipdent(handle)) != ZC_OK) {
		return (err);
	}

	idx = 1;
	while (zccfg_getipdent(handle, &ipdent) == ZC_OK) {
		(void) sprintf(key, ZC_ZONE_IPDS, idx);
		ccr_pack_string(seq, key, ipdent.zc_fs_dir);
		idx++;
	}

	(void) zccfg_endipdent(handle);
	return (ZC_OK);
}

//
// Add mount options for file system resource specified by "fsid", to the CCR
// element sequence.
//
static void
ccr_put_fs_options(ccr::element_seq &seq, zc_fsopt_t *optp, int fsid)
{
	int	opt_id;
	char	key[MAXPATHLEN];

	// Filesystem mount options are optional.
	if (optp == NULL) {
		return;
	}

	opt_id = 1;
	while (optp) {
		(void) sprintf(key, ZC_ZONE_FS_OPT, fsid, opt_id);
		ccr_pack_string(seq, key, optp->zc_fsopt_opt);
		opt_id++;
		optp = optp->zc_fsopt_next;
	}
}

//
// Extract global scope filesystem resource entries from "handle", and convert
// them into CCR format. The global scope filesystem entries in the CCR look
// like
//
// zone.filesystem.1.dir		/mnt
// zone.filesystem.1.special		/dev/did/dsk/d10s6
// zone.filesystem.1.raw		/dev/did/rdsk/d10s6
// zone.filesystem.1.type		ufs
// zone.filesystem.1.options.1.name	noatime
// zone.filesystem.1.options.2.name	nodevice
// zone.filesystem.2.dir		/foo
//
// and so on. The "raw" and "options" attributes are optional at the time of
// configuration (raw does not even make sense for a lookback filesystem). So
// the "raw" attribute will often be empty, and a filesystem entry may not have
// any associated mount options. These facts must be kept in mind when reading
// the CCR data back to construct the XML tree. See fetch_global_scope_fs().
//
static int
ccr_put_filesystems(ccr::element_seq &seq, zc_dochandle_t handle)
{
	int			idx;
	int			err;
	struct zc_fstab		fstab;
	char			key[MAXPATHLEN];

	if ((err = zccfg_setfsent(handle)) != ZC_OK) {
		return (err);
	}

	idx = 1;
	while (zccfg_getfsent(handle, &fstab) == ZC_OK) {
		(void) sprintf(key, ZC_ZONE_FS_DIR, idx);
		ccr_pack_string(seq, key, fstab.zc_fs_dir);
		(void) sprintf(key, ZC_ZONE_FS_SPECIAL, idx);
		ccr_pack_string(seq, key, fstab.zc_fs_special);
		(void) sprintf(key, ZC_ZONE_FS_RAW, idx);
		ccr_pack_string(seq, key, fstab.zc_fs_raw);
		(void) sprintf(key, ZC_ZONE_FS_TYPE, idx);
		ccr_pack_string(seq, key, fstab.zc_fs_type);

		ccr_put_fs_options(seq, fstab.zc_fs_options, idx);
		idx++;
	}
	(void) zccfg_endfsent(handle);
	return (ZC_OK);
}

//
// Extract global scope network resources from handle and add them to the
// CCR sequence. Global scope network resources have the following CCR
// format:
//
// (1) For shared IP type zones -
//
// zone.network.1.address		1.2.3.4
// zone.network.1.physical		bge0
// zone.network.2.address		my-zone-hostname
// zone.network.2.physical		bge1
//
// (2) For exclusive IP type zones -
//
// zone.network.1.physical		eri0
//
static int
ccr_put_networks(ccr::element_seq &seq, zc_dochandle_t handle)
{
	int			err;
	int			prop_id;
	char			key[MAXPATHLEN];
	struct zc_nwiftab	nwiftab;

	if ((err = zccfg_setnwifent(handle)) != ZC_OK) {
		return (err);
	}

	prop_id = 1;
	while (zccfg_getnwifent(handle, &nwiftab) == ZC_OK) {
		(void) sprintf(key, ZC_ZONE_NET_ADDRESS, prop_id);
		ccr_pack_string(seq, key, nwiftab.zc_nwif_address);
		(void) sprintf(key, ZC_ZONE_NET_PHYSICAL, prop_id);
		ccr_pack_string(seq, key, nwiftab.zc_nwif_physical);
		prop_id++;
	}
	(void) zccfg_endnwifent(handle);
	return (ZC_OK);
}

//
// Convenience function to write rctl values for a given rctl resource to the
// CCR sequence. 'prop_id' is the property id of the containing rctl resource.
// See block comment above ccr_put_rctls().
//
static void
ccr_put_rctl_val(ccr::element_seq &seq, const struct zc_rctlvaltab *valp,
    int prop_id)
{
	char	key[MAXPATHLEN];
	int	val_id; // 'value id' for the rctl value

	val_id = 1;
	while (valp) {
		(void) sprintf(key, ZC_ZONE_RCTLVAL_PRIV, prop_id, val_id);
		ccr_pack_string(seq, key, valp->zc_rctlval_priv);
		(void) sprintf(key, ZC_ZONE_RCTLVAL_LIMIT, prop_id, val_id);
		ccr_pack_string(seq, key, valp->zc_rctlval_limit);
		(void) sprintf(key, ZC_ZONE_RCTLVAL_ACTION, prop_id,
		    val_id);
		ccr_pack_string(seq, key, valp->zc_rctlval_action);

		val_id++;
		valp = valp->zc_rctlval_next;
	}
}

//
// Extract global scope rctl resources from handle and add them to the CCR
// sequence. The CCR entries for rctls look like
//
// zone.rctl.1.name		zone.cpu-shares
// zone.rctl.1.value.1.priv	privileged
// zone.rctl.1.value.1.limit	1
// zone.rctl.1.value.1.action	deny
// zone.rctl.1.value.2.priv	privileged
// zone.rctl.1.value.2.limit	10
// zone.rctl.1.value.2.action	deny
// zone.rctl.2.name		zone.max-lwps
// zone.rctl.2.value.1.priv	privileged
// zone.rctl.2.value.1.limit	8192
// zone.rctl.2.value.1.action	deny
// ...
//
// The key thing to keep in mind here is that a given rctl resource entry can
// have more than one rctl values (but this rarely seems to happen in
// practice).
//
static int
ccr_put_rctls(ccr::element_seq &seq, zc_dochandle_t handle)
{
	int			prop_id;
	int			err;
	struct zc_rctltab	rctltab;
	char			key[MAXPATHLEN];

	if ((err = zccfg_setrctlent(handle)) != ZC_OK) {
		return (err);
	}

	prop_id = 1;
	while (zccfg_getrctlent(handle, &rctltab) == ZC_OK) {
		(void) sprintf(key, ZC_ZONE_RCTL_NAME, prop_id);
		ccr_pack_string(seq, key, rctltab.zc_rctl_name);

		ccr_put_rctl_val(seq, rctltab.zc_rctl_valp, prop_id);
		prop_id++;
	}
	(void) zccfg_endrctlent(handle);
	return (ZC_OK);
}

static int
ccr_put_mcap(ccr::element_seq &seq, zc_dochandle_t handle)
{
	int			err;
	struct zc_mcaptab	mcaptab;
	char			key[MAXPATHLEN];

	err = zccfg_getmcapent(handle, &mcaptab);
	if (err != ZC_OK && err == ZC_NO_ENTRY) {
		return (ZC_OK);
	}

	(void) sprintf(key, ZC_ZONE_MCAP_PHYSCAP);
	ccr_pack_string(seq, key, mcaptab.zc_physmem_cap);
	return (ZC_OK);
}

static int
ccr_put_pset(ccr::element_seq &seq, zc_dochandle_t handle)
{
	int			err;
	struct zc_psettab	psettab;
	char			key[MAXPATHLEN];

	err = zccfg_getpsetent(handle, &psettab);
	if (err != ZC_OK && err == ZC_NO_ENTRY) {
		return (ZC_OK);
	}

	(void) sprintf(key, ZC_ZONE_PSET_NCPU_MIN);
	ccr_pack_string(seq, key, psettab.zc_ncpu_min);
	(void) sprintf(key, ZC_ZONE_PSET_NCPU_MAX);
	ccr_pack_string(seq, key, psettab.zc_ncpu_max);

	(void) sprintf(key, ZC_ZONE_PSET_IMPORTANCE);
	// This one is optional; if unspecified we just stuff an empty string.
	if (psettab.zc_importance[0] == '\0') {
		ccr_pack_string(seq, key, "");
	} else {
		ccr_pack_string(seq, key, psettab.zc_importance);
	}

	return (ZC_OK);
}

//
// Extract sysid resource from handle and add it to the CCR sequence. A zone
// cluster can have one, and only one, sysid resource. So the CCR entry looks
// like
//
// zone.sysid.root_password	XXXXXXXX
// zone.sysid.name_service	NIS{domain_name=something.com name_server=...}
// zone.sysid.nfs4_domain	dynamic
// zone.sysid.security_policy	NONE
// zone.sysid.system_locale	C
// zone.sysid.terminal		xterms
// zone.sysid.timezone		Asia/Calcutta
//
static int
ccr_put_sysid(ccr::element_seq &seq, zc_dochandle_t handle)
{
	int			err;
	struct zc_sysidtab	sysidtab;
	char			key[MAXPATHLEN];

	if ((err = zccfg_setsysident(handle)) != ZC_OK) {
		return (err);
	}

	err = zccfg_getsysident(handle, &sysidtab);
	if (err == ZC_NO_ENTRY) {
		return (ZC_OK);
	}

	(void) sprintf(key, ZC_SYSID_ROOT_PASSWORD);
	ccr_pack_string(seq, key, sysidtab.zc_root_password);
	(void) sprintf(key, ZC_SYSID_NAME_SERVICE);
	ccr_pack_string(seq, key, sysidtab.zc_name_service);
	(void) sprintf(key, ZC_SYSID_NFS4_DOMAIN);
	ccr_pack_string(seq, key, sysidtab.zc_nfs4_domain);
	(void) sprintf(key, ZC_SYSID_SEC_POLICY);
	ccr_pack_string(seq, key, sysidtab.zc_sec_policy);
	(void) sprintf(key, ZC_SYSID_SYS_LOCALE);
	ccr_pack_string(seq, key, sysidtab.zc_sys_locale);
	(void) sprintf(key, ZC_SYSID_TERMINAL);
	ccr_pack_string(seq, key, sysidtab.zc_terminal);
	(void) sprintf(key, ZC_SYSID_TIMEZONE);
	ccr_pack_string(seq, key, sysidtab.zc_timezone);

	(void) zccfg_endsysident(handle);
	return (ZC_OK);
}

//
// Extract global scope device resources from handle and add them to the CCR
// element sequence. Global scope device entries look like
//
// CSTYLED
// zone.device.1.match		/dev/*dsk/c0t0d2s7
// CSTYLED
// zone.device.2.match		/dev/did/*dsk/d10
//
static int
ccr_put_device(ccr::element_seq &seq, zc_dochandle_t handle)
{
	int			err;
	int			prop_id;
	struct zc_devtab	devtab;
	char			key[MAXPATHLEN];

	if ((err = zccfg_setdevent(handle)) != ZC_OK) {
		return (err);
	}

	prop_id = 1;
	while (zccfg_getdevent(handle, &devtab) == ZC_OK) {
		(void) sprintf(key, ZC_ZONE_DEVICE, prop_id);
		ccr_pack_string(seq, key, devtab.zc_dev_match);
		prop_id++;
	}
	(void) zccfg_enddevent(handle);
	return (ZC_OK);
}

//
// Extract global scope dataset resources from handle and add them to the CCR
// element sequence. Global scope dataset entries look like
//
// CSTYLED
// zone.dataset.1.name		tank/home
// CSTYLED
// zone.dataset.2.name		/tank/export
//
static int
ccr_put_dataset(ccr::element_seq &seq, zc_dochandle_t handle)
{
	int			err;
	int			prop_id;
	struct zc_dstab		dstab;
	char			key[MAXPATHLEN];

	if ((err = zccfg_setdsent(handle)) != ZC_OK) {
		return (err);
	}

	prop_id = 1;
	while (zccfg_getdsent(handle, &dstab) == ZC_OK) {
		(void) sprintf(key, ZC_ZONE_DATASET, prop_id);
		ccr_pack_string(seq, key, dstab.zc_dataset_name);
		prop_id++;
	}
	(void) zccfg_enddsent(handle);
	return (ZC_OK);
}

//
// Extract those properties and resources of the zone that have cluster-wide
// scope and pack them into the CCR element sequence. These are:
//
// zonepath, autoboot, ip-type, pool, limitpriv, bootargs, brand,
// scheduling-class, enable_priv_net, sysid, inherited-pkg-dir, filesystem,
// network, device, dataset, attr, pset, mcap, cpu-shares, and rctl.
//
static int
ccr_put_global_props(ccr::element_seq &seq, zc_dochandle_t handle)
{
	char	buff[MAXPATHLEN];
	char 	*priv_str = NULL;
	bool 	true_false;
	int	err;

	if ((err = zccfg_get_zonepath(handle, buff,
	    sizeof (buff))) != ZC_OK) {
		return (err);
	}
	ccr_pack_string(seq, ZC_ZONE_PATH, buff);

	if ((err = zccfg_get_autoboot(handle, &true_false)) != ZC_OK) {
		return (err);
	}
	ccr_pack_string(seq, ZC_ZONE_AUTOBOOT,
	    true_false ? "true" : "false");

	/* ip-type goes next */

	// resource pool
	if ((err = zccfg_get_pool(handle, buff, sizeof (buff))) != ZC_OK) {
		return (err);
	}
	ccr_pack_string(seq, ZC_ZONE_POOL, buff);

	// limitpriv
	if ((err = zccfg_get_limitpriv(handle, &priv_str)) != ZC_OK) {
		return (err);
	}
	// (void) sprintf(buff, "\"%s\"", priv_str);
	ccr_pack_string(seq, ZC_ZONE_LIMITPRIV, priv_str);

	// bootargs
	if ((err = zccfg_get_bootargs(handle, buff,
	    sizeof (buff))) != ZC_OK) {
		return (err);
	}
	ccr_pack_string(seq, ZC_ZONE_BOOTARGS, buff);

	// brand - always "cluster", but don't hard-code
	if ((err = zccfg_get_brand(handle, buff, sizeof (buff))) != ZC_OK) {
		return (err);
	}
	ccr_pack_string(seq, ZC_ZONE_BRAND, buff);

	// scheduling-class
	if ((err = zccfg_get_sched_class(handle, buff, sizeof (buff)))
	    != ZC_OK) {
		return (err);
	}
	ccr_pack_string(seq, ZC_ZONE_SCHED_CLASS, buff);

	// clprivnet (enable_priv_net)
	if ((err = zccfg_get_clprivnet(handle, &true_false)) != ZC_OK) {
		return (err);
	}
	ccr_pack_string(seq, ZC_ZONE_CLPRIVNET,
	    true_false ? "true" : "false");

	if ((err = ccr_put_sysid(seq, handle)) != ZC_OK) {
		return (err);
	}

	if ((err = ccr_put_ipds(seq, handle)) != ZC_OK) {
		return (err);
	}

	if ((err = ccr_put_filesystems(seq, handle)) != ZC_OK) {
		return (err);
	}

	if ((err = ccr_put_networks(seq, handle)) != ZC_OK) {
		return (err);
	}

	if ((err = ccr_put_rctls(seq, handle)) != ZC_OK) {
		return (err);
	}

	if ((err = ccr_put_mcap(seq, handle)) != ZC_OK) {
		return (err);
	}

	if ((err = ccr_put_pset(seq, handle)) != ZC_OK) {
		return (err);
	}

	if ((err = ccr_put_device(seq, handle)) != ZC_OK) {
		return (err);
	}

	if ((err = ccr_put_dataset(seq, handle)) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

//
// Helper function to add per-node net resources to the CCR element sequence.
//
static int
ccr_put_local_nets(ccr::element_seq &seq, zc_nwifelem_t *listp,
    int nodeid)
{
	int	propid;
	char	key[MAXNAMELEN];

	//
	// TODO handle exclusive ip type zones.
	//
	propid = 1;
	while (listp != NULL) {
		(void) sprintf(key, ZC_NODE_NET_ADDRESS, nodeid, propid);
		ccr_pack_string(seq, key, listp->elem.zc_nwif_address);
		(void) sprintf(key, ZC_NODE_NET_PHYSICAL, nodeid, propid);
		ccr_pack_string(seq, key, listp->elem.zc_nwif_physical);
		(void) sprintf(key, ZC_NODE_NET_DEFROUTER, nodeid, propid);
		ccr_pack_string(seq, key, listp->elem.zc_nwif_defrouter);
		propid++;
		listp = listp->next;
	}

	return (ZC_OK);
}

//
// Extract node resource entries from handle and add them to the CCR element
// sequence.
//
static int
ccr_put_node_entries(ccr::element_seq &seq, zc_dochandle_t handle)
{
	int			err;
	int			nodeid;
	struct zc_nodetab	nodetab;
	char			key[MAXNAMELEN];

	if ((err = zccfg_setnodeent(handle)) != ZC_OK) {
		return (err);
	}

	//
	// IMPORTANT: Strictly follow this ordering when writing node scope
	// resource specification to the CCR:
	//
	// nodetab.zc_nodename, nodetab.zc_hostname, node-specific net
	// resources, node-specific fs resources, node-specific devices,
	// node-specific dataset resources, node-specific rctl resources.
	//
	// Any new node-specific zone resource must be appended to the above
	// list.
	//
	nodeid = 1;
	while (zccfg_getnodeent(handle, &nodetab) == ZC_OK) {
		(void) sprintf(key, ZC_NODE_NAME, nodeid);
		ccr_pack_string(seq, key, nodetab.zc_nodename);
		(void) sprintf(key, ZC_NODE_HOSTNAME, nodeid);
		ccr_pack_string(seq, key, nodetab.zc_hostname);
		(void) ccr_put_local_nets(seq, nodetab.zc_nwif_listp,
		    nodeid);
		nodeid++;
	}
	(void) zccfg_endnodeent(handle);

	return (ZC_OK);
}

//
// Create, or update the zone cluster's CCR configuration table.
//
static int
zccfg_create_zc_table(zc_dochandle_t handle,
    const ccr::directory_var ccr_dir_v)
{
	int				err;
	char				zonename[ZONENAME_MAX];
	Environment			e;
	CORBA::Exception		*exp;
	ccr::updatable_table_var	utable_v;
	ccr::readonly_table_ptr		rtable_p;
	ccr::element_seq		seq;

	if ((err = zccfg_get_name(handle, zonename, sizeof (zonename)))
	    != ZC_OK) {
		return (err);
	}

	//
	// Lookup the "clzone_config" table. If the table exists, it means that
	// we are modifying the configuration. If it does not exist, we create
	// it.
	//
	// If the table exists, we "empty" it by removing all the elements.
	// "handle" has all the information about the new configuration, so
	// it's faster (and easier) to just write the new configuration instead
	// of examining each element to see whether or not it has changed. This
	// approach is OK since modification of the configuration rarely occurs.
	//
	rtable_p = ccr_dir_v->lookup_zc(zonename, "clzone_config", e);
	//
	// Release rtable_p as we have no further use for it. This is to ensure
	// that the ref counts are sane.
	//
	CORBA::release(rtable_p);
	if ((exp = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(exp) != NULL) {
			e.clear();
			ccr_dir_v->create_table_zc(zonename, "clzone_config",
			    e);
			if (e.exception()) {
				//
				// SCMSGS
				// @explanation
				// The CCR raised an exception while creating
				// the clzone_config table.
				// @user_action
				// Contact your authorized Sun service provider
				// to determine whether a workaround or patch
				// is available.
				//
				(void) logger.log(LOG_ERR, MESSAGE,
				    "CCR create table failed for zone cluster"
				    " %s\n", zonename);
				e.clear();
				return (ZC_CCR_ERROR);
			}
		} else {
			//
			// SCMSGS
			// @explanation
			// The CCR raised an exception while looking up the
			// clzone_config table.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "CCR lookup failed for zone cluster %s\n",
			    zonename);
			e.clear();
			return (ZC_CCR_ERROR);
		}
	}

	utable_v = ccr_dir_v->begin_transaction_zc(zonename, "clzone_config",
	    e);
	if (e.exception()) {
		//
		// SCMSGS
		// @explanation
		// The CCR raised an exception while starting a transaction on
		// the zone cluster configuration table.
		// @user_action
		// Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "CCR begin transaction "
		    "failed for zone cluster %s\n", zonename);
		e.clear();
		return (ZC_CCR_ERROR);
	}

	// "Empty" the table.
	utable_v->remove_all_elements(e);
	if (e.exception()) {
		e.clear();
		utable_v->abort_transaction(e);
		return (ZC_CCR_ERROR);
	}

	unsigned int i = seq.length();
	seq.length(i + 1);
	seq[i].key = strdup(ZC_ZONE_NAME);
	seq[i].data = strdup(zonename);

	if ((err = ccr_put_global_props(seq, handle)) != ZC_OK) {
		utable_v->abort_transaction(e);
		return (err);
	}
	if ((err = ccr_put_node_entries(seq, handle)) != ZC_OK) {
		utable_v->abort_transaction(e);
		return (err);
	}

	utable_v->add_elements(seq, e);
	if (e.exception()) {
		utable_v->abort_transaction(e);
		e.clear();
		return (ZC_CCR_ERROR);
	}

	utable_v->commit_transaction(e);
	if (e.exception()) {
		//
		// SCMSGS
		// @explanation
		// The CCR raised an exception while committing a transaction on
		// the zone cluster configuration table.
		// @user_action
		// Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "CCR commit transaction "
		    "failed for zone cluster %s\n", zonename);
		e.clear();
		return (ZC_CCR_ERROR);
	}

	return (ZC_OK);
}

//
// Create the CCR infrastructure table for the CZ. Though the infrastructure
// table of the CZ looks like the physical cluster's, it contains far less
// information than its physical cluster counterpart. This is because the CZ
// doesn't have a separate interconnect, switches, cables, quorum devices, etc.
//
// If the tabel already exists, it means that we're updating it.
//
static int
zccfg_create_infra_table(zc_dochandle_t handle,
    const ccr::directory_var ccr_dir_v)
{
	int				err;
	char				zonename[ZONENAME_MAX];
	char				buffer[MAXPATHLEN];
	Environment			e;
	CORBA::Exception		*exp;
	ccr::updatable_table_var	utable_v;
	ccr::readonly_table_ptr		rtable_p;
	struct zc_nodetab		nodetab;
	nodeid_t			nodeid;
	char				props_str[MAXPATHLEN];
	char 				privnodename[SYS_NMLN];

	if ((err = zccfg_get_name(handle, zonename, sizeof (zonename)))
	    != ZC_OK) {
		return (err);
	}

	//
	// Lookup the infrastructure table for this zone cluster. If the table
	// exists, it means that we're modifying the configuration. If table
	// does not exist we create it.
	//
	// If the table exists, we "empty" it by removing all the elements.
	// "handle" has all the information about the new configuration, so
	// it's faster (and easier) to just write the new configuration instead
	// of examining each element to see whether or not it has changed. The
	// performance impact is acceptable since modification of the
	// configuration rarely accurs.
	//
	rtable_p = ccr_dir_v->lookup_zc(zonename, "infrastructure", e);
	//
	// Release rtable_p as we have no further use for it. This is to ensure
	// that the ref counts are sane.
	//
	CORBA::release(rtable_p);
	if ((exp = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(exp) != NULL) {
			e.clear();
			ccr_dir_v->create_table_zc(zonename, "infrastructure",
			    e);
			if (e.exception()) {
				(void) logger.log(LOG_ERR, MESSAGE,
				    "CCR create table failed for zone cluster"
				    " %s\n", zonename);
				e.clear();
				return (ZC_CCR_ERROR);
			}
		} else {
			(void) logger.log(LOG_ERR, MESSAGE,
			    "CCR lookup failed for zone cluster %s\n",
			    zonename);
			e.clear();
			return (ZC_CCR_ERROR);
		}
	}

	utable_v = ccr_dir_v->begin_transaction_zc(zonename, "infrastructure",
	    e);
	if (e.exception()) {
		(void) logger.log(LOG_ERR, MESSAGE, "CCR begin transaction "
		    "failed for zone cluster %s\n", zonename);
		e.clear();
		return (ZC_CCR_ERROR);
	}

	// "Empty" the table.
	utable_v->remove_all_elements(e);
	if (e.exception()) {
		e.clear();
		utable_v->abort_transaction(e);
		return (ZC_CCR_ERROR);
	}

	utable_v->add_element(ZC_CLUSTER_NAME, zonename, e);
	if (e.exception()) {
		e.clear();
		utable_v->abort_transaction(e);
		return (ZC_CCR_ERROR);
	}

	//
	// Add the 'zone hostnames' to the infrastructure. The zone hostname
	// becomes the 'node name' for the ZC instance. The entry is of the
	// form
	//
	// cluster.nodes.x.name		<zone hostname>
	//
	// where 'x' is the nodeid of the underlying physical node. This means
	// that the 'x'-es will not necessarily be in numerical order.
	//
	if ((err = zccfg_setnodeent(handle)) != ZC_OK) {
		utable_v->abort_transaction(e);
		return (err);
	}
	while (zccfg_getnodeent(handle, &nodetab) == ZC_OK) {
		nodeid = clconf_cluster_get_nodeid_by_nodename(
		    nodetab.zc_nodename);
		assert(nodeid != NODEID_UNKNOWN);
		(void) snprintf(buffer, sizeof (buffer),
		    ZC_CLUSTER_NODE_NAME, nodeid);
		utable_v->add_element(buffer, nodetab.zc_hostname, e);
		if (e.exception()) {
			e.clear();
			utable_v->abort_transaction(e);
			return (ZC_CCR_ERROR);
		}

		//
		// Set the private hostname to default form clusternodei-priv
		// where i is nodeid.
		(void) sprintf(privnodename, "clusternode%d-priv", nodeid);
		(void) snprintf(props_str, sizeof (props_str),
		    ZC_CLUSTER_NODE_PROPS, nodeid);
		(void) sprintf(buffer, "%s.private_hostname", props_str);
		utable_v->add_element(buffer, privnodename, e);
		if (e.exception()) {
			e.clear();
			utable_v->abort_transaction(e);
			return (ZC_CCR_ERROR);
		}
	}
	(void) zccfg_endnodeent(handle);

	utable_v->commit_transaction(e);
	if (e.exception()) {
		(void) logger.log(LOG_ERR, MESSAGE, "CCR commit transaction "
		    "failed for zone cluster %s\n", zonename);
		e.clear();
		return (ZC_CCR_ERROR);
	}

	return (ZC_OK);
}

//
// Convenience function to obtain a CORBA reference to the cl_exec daemon on
// the cluster node specified by nodeid. If successful, the reference is
// returned via the second argument, and ZC_OK is returned by the function.
// On failure, a ZC_* error code is returned and the caller must not use
// cl_exec_v in that case.
//
static int
get_clexecd_ref(const nodeid_t nodeid, remote_exec::cl_exec_var &cl_exec_v)
{
	Environment			env;
	CORBA::Exception		*exp;
	CORBA::Object_var		obj_v;
	char				clexec_name[12]; // Up to clexec.64
	naming::naming_context_var	ctx_v = ns::root_nameserver();

	assert(!CORBA::is_nil(ctx_v));
	if (CORBA::is_nil(ctx_v)) {
		return (ZC_NAMESERVER_ERR);
	}

	(void) sprintf(clexec_name, "cl_exec.%d", nodeid);
	obj_v = ctx_v->resolve(clexec_name, env);
	if ((exp = env.exception()) != NULL) {
		if (naming::not_found::_exnarrow(exp) != NULL) {
			return (ZC_NODE_DOWN);
		}
	} else {
		//
		// Test to see that the object we got from the
		// name server is active.
		//
		cl_exec_v = remote_exec::cl_exec::_narrow(obj_v);
		ASSERT(!CORBA::is_nil(cl_exec_v));
		(void) cl_exec_v->is_alive(env);
		if (env.exception() != NULL) {
			env.clear();
			return (ZC_REMOTE_EXEC_ERR);
		}
	}

	return (ZC_OK);
}

//
// Execute the command pointed to by cmd on the cluster node specified by
// nodeid.
//
// Return value:
//	ZC_* from libzccfg.h
//		OR
//	The exit status of the task
//
int
zccfg_execute_cmd(nodeid_t nodeid, const char *cmdp)
{

	remote_exec::cl_exec_var	cl_exec_v;
	CORBA::Environment		env;
	CORBA::Exception		*exp;
	int				err;

	ASSERT(cmdp != NULL);

	if ((err = get_clexecd_ref(nodeid, cl_exec_v)) != ZC_OK) {
		return (err);
	}

	int	ret_val = ZC_OK;		// Exit status of task cmd.
	char	*stdout_str = NULL;
	char	*stderr_str = NULL;
	int	optval;

	optval = cl_exec_v->exec_program_get_output(ret_val, stdout_str,
	    stderr_str, cmdp, remote_exec::DEF, 0, env);

	if ((exp = env.exception()) != NULL) {
		if (CORBA::COMM_FAILURE::_exnarrow(exp)) {
			err = ZC_NODE_DOWN;
		} else {
			err = ZC_REMOTE_EXEC_ERR;
		}
		env.clear();
		return (err);
	}

	//
	// The possible return values from exec_program_get_output() are
	// really those from door_call(2), i.e., set in errno. Rather than
	// try to map errno to one of our ZC_* error codes, we'll just return
	// ZC_SYSTEM and ask the user to consult errno instead.
	//
	if (optval != 0) {
		char *nodename = NULL;
		clconf_get_nodename(nodeid, nodename);
		assert(nodename);
		//
		// SCMSGS
		// @explanation
		// Attempt to execute a command on a remote cluster node
		// failed.
		// @user_action
		// Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "Failed to execute %s on %s. optval = %d", cmdp, nodename,
		    optval);
		delete [] nodename;
		return (ZC_SYSTEM);
	}

	if ((ret_val != ZC_OK) && (*stdout_str != '\0')) {
		//
		// SCMSGS
		// @explanation
		// A remote command failed and produced information in its
		// standard output.
		// @user_action
		// Check the message to determine what might be wrong.
		// Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s on node %d, stdout: %s\n", cmdp, nodeid, stdout_str);
	}
	if ((ret_val != ZC_OK) && (*stderr_str != '\0')) {
		//
		// SCMSGS
		// @explanation
		// A remote command failed and produced information in its
		// standard error.
		// @user_action
		// Check the message to determine what might be wrong.
		// Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE,
		    "%s on node %d, stderr: %s\n", cmdp, nodeid, stderr_str);
	}

	if (stdout_str != NULL) {
		delete [] stdout_str;
	}
	if (stderr_str != NULL) {
		delete [] stderr_str;
	}

	return (ret_val);
}

//
// Get the state of the zone member of zone cluster specified by first arg on
// the physical node specified by the second arg. The state value is returned
// via the third arg.
//
int
get_zone_state_on_node(const char *zonename, nodeid_t nodeid,
    zone_cluster_state_t *statep)
{
	remote_exec::cl_exec_var	cl_exec_v;
	CORBA::Environment		env;
	CORBA::Exception		*exp;
	int				err;
	char				cmdbuf[MAXPATHLEN];
	// Exit status of task cmd.
	int				ret_val = ZC_OK;
	char				*stdout_str = NULL;
	char				*stderr_str = NULL;
	int				optval;

	assert(statep != NULL);

	(void) sprintf(cmdbuf, "%s %s status", ZC_HELPER, zonename);

	// Get cl_execd reference on that node.
	if ((err = get_clexecd_ref(nodeid, cl_exec_v)) != ZC_OK) {
		*statep = ZC_STATE_UNKNOWN;
		return (err);
	}

	optval = cl_exec_v->exec_program_get_output(ret_val,
	    stdout_str, stderr_str, cmdbuf, remote_exec::DEF, 0, env);

	if ((exp = env.exception()) != NULL) {
		//
		// SCMSGS
		// @explanation
		// An exception occured while a remote command is being
		// executed.
		// @user_action
		// Check the message to determine what might be wrong.
		// Contact your authorized Sun service
		// provider to determine whether a workaround or patch is
		// available.
		//
		(void) logger.log(LOG_ERR, MESSAGE, "%s exception occured "
		    "during execution of %s on node %d\n", exp->_name(),
		    cmdbuf, nodeid);
		*statep = ZC_STATE_UNKNOWN;
		env.clear();
		return (ZC_REMOTE_EXEC_ERR);
	}

	if (optval != 0) {
		*statep = ZC_STATE_UNKNOWN;
	} else {
		*statep = (zone_cluster_state_t)ret_val;
	}
	return (ZC_OK);
}

//
// Get the state of the zone cluster itself. The zone cluster state is defined
// as the highest state of its constituent zones. This is a dynamic information
// and is not stored in the CCR. We need to go to each physical node to get the
// state of the zone on that node.
//
int
get_zone_cluster_state(const char *zonename, zone_cluster_state_t *statep)
{
	int			err;
	zc_dochandle_t		handle;
	struct zc_nodetab	nodetab;
	nodeid_t		nodeid;
	zone_cluster_state_t	state = ZC_STATE_UNKNOWN;

	if ((handle = zccfg_init_handle()) == NULL) {
		*statep = ZC_STATE_UNKNOWN;
		return (ZC_NOMEM);
	}

	if ((err = zccfg_get_handle(zonename, handle)) != ZC_OK) {
		*statep = ZC_STATE_UNKNOWN;
		zccfg_fini_handle(handle);
		return (err);
	}

	if ((err = zccfg_setnodeent(handle)) != ZC_OK) {
		*statep = ZC_STATE_UNKNOWN;
		zccfg_fini_handle(handle);
		return (err);
	}

	*statep = ZC_STATE_UNKNOWN;
	while (zccfg_getnodeent(handle, &nodetab) == ZC_OK) {
		nodeid = clconf_cluster_get_nodeid_by_nodename(
		    nodetab.zc_nodename);
		(void) get_zone_state_on_node(zonename, nodeid, &state);
		if (state > *statep) {
			*statep = state;
		}
	}
	(void) zccfg_endnodeent(handle);
	zccfg_fini_handle(handle);
	return (ZC_OK);
}

int
get_zone_cluster_state_on_node(const char *zcname, nodeid_t nodeid,
	char **status_str) {
	Environment		e;
	CORBA::Exception	*exp;
	cmm::membership_t	my_membership;
	cmm::seqnum_t		seqnum;
	clconf_cluster_t	*clp;
	int			flag = 0;

	membership::api_var membership_api_v = cmm_ns::get_membership_api_ref();
	if (CORBA::is_nil(membership_api_v)) {
		*status_str = strdup((char *)gettext(ZC_STATE_STR_UNKNOWN));
		return (ZC_MEMBERSHIP_ERROR);
	}

	membership_api_v->get_cluster_membership(my_membership, seqnum,
	    zcname, e);
	exp = e.exception();
	if (exp != NULL) {
		*status_str = strdup((char *)gettext(ZC_STATE_STR_UNKNOWN));
		return (ZC_MEMBERSHIP_ERROR);
	}

	// query static membership
	clp = clconf_cluster_get_vc_current(zcname);
	if (clp == NULL) {
		*status_str = strdup((char *)gettext(ZC_STATE_STR_UNKNOWN));
		return (ZC_MEMBERSHIP_ERROR);
	}

	for (nodeid_t nid = 1; nid <= NODEID_MAX; nid++) {
		// check if it is configured as part of zc
		if (nid == nodeid) {
			if (clconf_cluster_get_nodename_by_nodeid(clp, nid)
				== NULL) {
				// not configured
				flag = 0;
				continue;
			}

			if (my_membership.members[nid] != INCN_UNKNOWN) {
				// member is up
				flag = 1;
			} else {
				// member is down (zone could be in
				// configured/installed state, but will
				// exist for sure)
				flag = 2;
			}
		}
	}

	switch (flag) {
		case 0:
			*status_str =
				strdup(ZC_STATE_STR_UNKNOWN);
			break;

		case 1:
			*status_str = strdup(ZC_STATE_STR_ONLINE);
			break;

		case 2:
			*status_str = strdup(ZC_STATE_STR_OFFLINE);
			break;
	}

	return (ZC_OK);
}

const char *
zone_cluster_state_str(zone_cluster_state_t state)
{
	switch (state) {
	case ZC_STATE_CONFIGURED:
		return (ZC_STATE_STR_CONFIGURED);
	case ZC_STATE_INCOMPLETE:
		return (ZC_STATE_STR_INCOMPLETE);
	case ZC_STATE_INSTALLED:
		return (ZC_STATE_STR_INSTALLED);
	case ZC_STATE_DOWN:
		return (ZC_STATE_STR_DOWN);
	case ZC_STATE_MOUNTED:
		return (ZC_STATE_STR_MOUNTED);
	case ZC_STATE_READY:
		return (ZC_STATE_STR_READY);
	case ZC_STATE_SHUTTING_DOWN:
		return (ZC_STATE_STR_SHUTTING_DOWN);
	case ZC_STATE_RUNNING:
		return (ZC_STATE_STR_RUNNING);
	case ZC_STATE_UNKNOWN:
	default:
		return (ZC_STATE_STR_UNKNOWN);
	}
}

//
// Create the command log table for a zone cluster. The command log table
// resides in /etc/cluster/ccr/global with the table name <zc_name>.log.
//
// Command log entries are of the form
//
// physical-node-name	command
//
// For example:
//
// phys-cluster-1	create
// phys-cluster-2	create
// phys-cluster-3	delete
//
// The log entries indicate which command needs to be replayed for the given
// zone cluster on the physical cluster node. The CCR key is unique, which
// guarantees that there can be at most one outstanding command on a physical
// node for any zone cluster.
//
static int
zccfg_create_cmd_log(zc_dochandle_t handle,
    const ccr::directory_var ccr_dir_v)
{
	int				err;
	char				zonename[ZONENAME_MAX];
	char				table_name[MAXPATHLEN];
	Environment			e;
	CORBA::Exception		*exp;
	ccr::readonly_table_ptr		rtable_p;

	if ((err = zccfg_get_name(handle, zonename, sizeof (zonename)))
	    != ZC_OK) {
		return (err);
	}

	(void) snprintf(table_name, sizeof (table_name), "%s.log",
	    zonename);

	//
	// Lookup the command log table. If the table exists, it means that
	// we are modifying the configuration. If it does not exist, we create
	// it.
	//
	rtable_p = ccr_dir_v->lookup_zc("global", table_name, e);
	//
	// Release rtable_p as we have no further use for it. This is to ensure
	// that the ref counts are sane.
	//
	CORBA::release(rtable_p);
	if ((exp = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(exp) != NULL) {
			e.clear();
			ccr_dir_v->create_table_zc("global", table_name,
			    e);
			if (e.exception()) {
				//
				// SCMSGS
				// @explanation
				// The CCR raised an exception while creating
				// the command log table.
				// @user_action
				// Contact your authorized Sun service provider
				// to determine whether a workaround or patch
				// is available.
				//
				(void) logger.log(LOG_ERR, MESSAGE,
				    "Could not create command log for zone "
				    "cluster %s\n", zonename);
				e.clear();
				return (ZC_CCR_ERROR);
			}
		} else {
			//
			// SCMSGS
			// @explanation
			// The CCR raised an exception while looking up the
			// command log table.
			// @user_action
			// Contact your authorized Sun service provider to
			// determine whether a workaround or patch is available.
			//
			(void) logger.log(LOG_ERR, MESSAGE,
			    "Failed to lookup command log for zone cluster "
			    "%s\n", zonename);
			e.clear();
			return (ZC_CCR_ERROR);
		}
	} else {
		//
		// Command log already exists. Nothing to do.
		//
		e.clear();
	}
	return (ZC_OK);
}

//
// Implementation function for zccfg_save below.
//
static int
zccfg_save_impl(zc_dochandle_t handle)
{
	int			valid;
	int			err;
	xmlValidCtxt		cvp = { NULL };
	Environment		e;
	ccr::directory_var	ccr_dir_v;

	// look up ccr directory in name server
	naming::naming_context_var ctx_v = ns::local_nameserver();
	assert(!CORBA::is_nil(ctx_v));
	if (CORBA::is_nil(ctx_v)) {
		return (ZC_NAMESERVER_ERR);
	}

	CORBA::Object_var obj_v = ctx_v->resolve("ccr_directory", e);
	if (e.exception()) {
		e.clear();
		return (ZC_NAMESERVER_ERR);
	}
	ccr_dir_v = ccr::directory::_narrow(obj_v);

	cvp.error = zccfg_error_func;
	cvp.warning = zccfg_error_func;

	//
	// Uncomment the following if you run into XML errors and you want
	// to see the raw XML output.
	//
	// (void) xmlDocDump(stderr, handle->zc_dh_doc);
	valid = xmlValidateDocument(&cvp, handle->zc_dh_doc);
	if (valid == 0) {
		(void) fprintf(stderr, "save_impl: xmlValidateDocument"
		    " failed\n");
		return (ZC_SAVING_FILE);
	}

	if ((err = zccfg_create_cluster(handle, ccr_dir_v)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_create_infra_table(handle, ccr_dir_v)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_create_zc_table(handle, ccr_dir_v)) != ZC_OK) {
		return (err);
	}

	//
	// Successfully created the zone cluster configuration. Now create
	// the command log CCR table for this zone cluster.
	//
	if ((err = zccfg_create_cmd_log(handle, ccr_dir_v)) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

//
// Save the CZ configuration to the CCR.
//
int
zccfg_save(zc_dochandle_t handle)
{
	if (zccfg_check_handle(handle) != ZC_OK) {
		return (ZC_BAD_HANDLE);
	}

	return (zccfg_save_impl(handle));
}

//
// Add command log entry
//
int
zccfg_add_cmd_log_entry(const char *zonenamep, const char *nodenamep,
    const char *cmdp)
{
	ccr::directory_var		ccr_dir_v;
	Environment			e;
	CORBA::Exception		*exp;
	char				table_name[MAXPATHLEN];
	ccr::updatable_table_var	utable_v;

	naming::naming_context_var ctx_v = ns::local_nameserver();
	assert(!CORBA::is_nil(ctx_v));
	if (CORBA::is_nil(ctx_v)) {
		return (ZC_NAMESERVER_ERR);
	}

	CORBA::Object_var obj_v = ctx_v->resolve("ccr_directory", e);
	if (e.exception()) {
		e.clear();
		return (ZC_NAMESERVER_ERR);
	}
	ccr_dir_v = ccr::directory::_narrow(obj_v);

	(void) snprintf(table_name, sizeof (table_name), "%s.log",
	    zonenamep);
	utable_v = ccr_dir_v->begin_transaction_zc("global", table_name, e);

	if (e.exception()) {
		// handle exception
		e.clear();
		return (ZC_CCR_ERROR);
	}

	//
	// There can be at most one command log entry per node. Try to add the
	// entry. If that fails because an entry for the node already exists,
	// update the entry. This ensures that the log always has the latest
	// executed command to be replayed.
	//
	utable_v->add_element(nodenamep, cmdp, e);
	if ((exp = e.exception()) != NULL) {
		if (ccr::key_exists::_exnarrow(exp) != NULL) {
			//
			// Key already exists. Update the value instead.
			//
			e.clear();
			utable_v->update_element(nodenamep, cmdp, e);
			if (e.exception()) {
				// Syslog message
				e.clear();
				utable_v->abort_transaction(e);
				return (ZC_CCR_ERROR);
			}
		} else {
			e.clear();
			utable_v->abort_transaction(e);
			return (ZC_CCR_ERROR);
		}
	}

	utable_v->commit_transaction(e);
	if (e.exception()) {
		e.clear();
		return (ZC_CCR_ERROR);
	}
	return (ZC_OK);
}

//
// Delete a command log entry for the specified zone cluster. Since there is
// at most one command log entry per node, we just delete the element whose
// key matches the specified nodename.
//
int
zccfg_delete_cmd_log_entry(const char *zonenamep, const char *nodenamep)
{
	ccr::directory_var		ccr_dir_v;
	Environment			e;
	char				table_name[MAXPATHLEN];
	ccr::updatable_table_var	utable_v;

	naming::naming_context_var ctx_v = ns::local_nameserver();
	assert(!CORBA::is_nil(ctx_v));
	if (CORBA::is_nil(ctx_v)) {
		return (ZC_NAMESERVER_ERR);
	}

	CORBA::Object_var obj_v = ctx_v->resolve("ccr_directory", e);
	if (e.exception()) {
		e.clear();
		return (ZC_NAMESERVER_ERR);
	}
	ccr_dir_v = ccr::directory::_narrow(obj_v);

	(void) snprintf(table_name, sizeof (table_name), "%s.log",
	    zonenamep);
	utable_v = ccr_dir_v->begin_transaction_zc("global", table_name, e);

	if (e.exception()) {
		// handle exception
		e.clear();
		return (ZC_CCR_ERROR);
	}

	utable_v->remove_element(nodenamep, e);
	if (e.exception()) {
		e.clear();
		utable_v->abort_transaction(e);
		return (ZC_CCR_ERROR);
	}

	utable_v->commit_transaction(e);
	if (e.exception()) {
		e.clear();
		return (ZC_CCR_ERROR);
	}
	return (ZC_OK);
}

//
// Get command log entry to be processed for the specified node. The nodename
// argument serves as the CCR key.
//
// Returns NULL if the key does not exist, i.e., if there's no command to be
// replayed for the specified node.
//
char *
zccfg_get_cmd_log_entry(const char *zonenamep, const char *nodenamep)
{
	ccr::readonly_table_ptr	rtable_p;
	ccr::directory_var	directory_v;
	Environment		e;
	char			table_name[MAXPATHLEN];
	char			*datap = NULL;

	naming::naming_context_var ctx_v = ns::local_nameserver();
	assert(!CORBA::is_nil(ctx_v));
	if (CORBA::is_nil(ctx_v)) {
		return (NULL);
	}

	CORBA::Object_var obj_v = ctx_v->resolve("ccr_directory", e);
	if (e.exception()) {
		e.clear();
		return (NULL);
	}
	directory_v = ccr::directory::_narrow(obj_v);

	(void) snprintf(table_name, sizeof (table_name), "%s.log",
	    zonenamep);
	rtable_p = directory_v->lookup_zc("global", table_name, e);
	if (e.exception()) {
		e.clear();
		CORBA::release(rtable_p);
		return (NULL);
	}

	datap = rtable_p->query_element(nodenamep, e);
	if (e.exception()) {
		e.clear();
	}
	CORBA::release(rtable_p);
	return (datap);
}

//
// Remove zone cluster specified by first argument from the system. Also
// removes the command log table for the zone cluster.
//
int
zccfg_destroy(const char *zonename, bool force)
{
	int			err;
	ccr::directory_var	ccr_dir_v;
	Environment		e;
	CORBA::Exception	*exp;
	char			table_name[MAXPATHLEN];
	uint_t			cluster_id;
	char			cluster_id_str[30];
	bool			status;

	naming::naming_context_var	root_ctx_v;

	//
	// We will discard the xml file removal soon.
	//
	char xmlfile[MAXPATHLEN];

	(void) sprintf(xmlfile, "%s/%s.xml", ZC_CONFIG_ROOT, zonename);
	(void) unlink(xmlfile);

	// Retrieve the custer id
	err = clconf_get_cluster_id((char *)zonename, &cluster_id);
	if (err != 0) {
		return (ZC_BAD_CLUSTER_ID);
	}

	// Valid ZC ids start from 3
	ASSERT(cluster_id > 2);

	(void) os::itoa(cluster_id, cluster_id_str);

	// Retrieve the root of the global nameserver
	root_ctx_v = ns::root_nameserver_root();
	assert(!CORBA::is_nil(root_ctx_v));
	if (CORBA::is_nil(root_ctx_v)) {
		return (ZC_NAMESERVER_ERR);
	}

	status = root_ctx_v->unbind_existing_context_v1(cluster_id_str, e);
	if ((!status) || (e.exception())) {
		e.exception()->print_exception(
		    "libzccfg failed to delete context for zone cluster\n");
		e.clear();
		return (ZC_NAMESERVER_ERR);
	}

	// look up ccr directory in name server
	naming::naming_context_var ctx_v = ns::local_nameserver();
	assert(!CORBA::is_nil(ctx_v));
	if (CORBA::is_nil(ctx_v)) {
		return (ZC_NAMESERVER_ERR);
	}

	CORBA::Object_var obj_v = ctx_v->resolve("ccr_directory", e);
	if (e.exception()) {
		e.clear();
		return (ZC_NAMESERVER_ERR);
	}
	ccr_dir_v = ccr::directory::_narrow(obj_v);

	//
	// Remove command log table for this zone cluster.
	//
	(void) snprintf(table_name, sizeof (table_name), "%s.log",
	    zonename);
	ccr_dir_v->remove_table_zc("global", table_name, e);
	if ((exp = e.exception()) != NULL) {
		if (ccr::no_such_table::_exnarrow(exp) &&
		    cluster_id >= MIN_CLUSTER_ID) {
			//
			// We're deleting a partially configured zone
			// cluster. Ignore the exception in this case.
			//
			e.clear();
		} else {
			e.clear();
			return (ZC_CCR_ERROR);
		}
	}

	ccr_dir_v->zc_remove(zonename, e);
	if (e.exception()) {
		e.clear();
		return (ZC_CCR_ERROR);
	}

	return (ZC_OK);
}

char *
zccfg_strerror(int errnum)
{
	switch (errnum) {
	case ZC_OK:
		return (dgettext(TEXT_DOMAIN, "OK"));
	case ZC_EMPTY_DOCUMENT:
		return (dgettext(TEXT_DOMAIN, "Empty document"));
	case ZC_WRONG_DOC_TYPE:
		return (dgettext(TEXT_DOMAIN, "Wrong document type"));
	case ZC_BAD_PROPERTY:
		return (dgettext(TEXT_DOMAIN, "Bad document property"));
	case ZC_TEMP_FILE:
		return (dgettext(TEXT_DOMAIN,
		    "Problem creating temporary file"));
	case ZC_SAVING_FILE:
		return (dgettext(TEXT_DOMAIN, "Problem saving file"));
	case ZC_NO_ENTRY:
		return (dgettext(TEXT_DOMAIN, "No such entry"));
	case ZC_BOGUS_ZONE_NAME:
		return (dgettext(TEXT_DOMAIN, "Invalid zone cluster name"));
	case ZC_REQD_RESOURCE_MISSING:
		return (dgettext(TEXT_DOMAIN, "Required resource missing"));
	case ZC_REQD_PROPERTY_MISSING:
		return (dgettext(TEXT_DOMAIN, "Required property missing"));
	case ZC_BAD_HANDLE:
		return (dgettext(TEXT_DOMAIN, "Bad handle"));
	case ZC_NOMEM:
		return (dgettext(TEXT_DOMAIN, "Out of memory"));
	case ZC_INVAL:
		return (dgettext(TEXT_DOMAIN, "Invalid argument"));
	case ZC_ACCES:
		return (dgettext(TEXT_DOMAIN, "Permission denied"));
	case ZC_TOO_BIG:
		return (dgettext(TEXT_DOMAIN, "Argument list too long"));
	case ZC_MISC_FS:
		return (dgettext(TEXT_DOMAIN,
		    "Miscellaneous file system error"));
	case ZC_NO_ZONE:
		return (dgettext(TEXT_DOMAIN,
		    "No such zone cluster configured"));
	case ZC_NO_RESOURCE_TYPE:
		return (dgettext(TEXT_DOMAIN, "No such resource type"));
	case ZC_NO_RESOURCE_ID:
		return (dgettext(TEXT_DOMAIN, "No such resource with that id"));
	case ZC_NO_PROPERTY_TYPE:
		return (dgettext(TEXT_DOMAIN, "No such property type"));
	case ZC_NO_PROPERTY_ID:
		return (dgettext(TEXT_DOMAIN, "No such property with that id"));
	case ZC_BAD_ZONE_STATE:
		return (dgettext(TEXT_DOMAIN,
		    "Zone state is invalid for the requested operation"));
	case ZC_INVALID_DOCUMENT:
		return (dgettext(TEXT_DOMAIN, "Invalid document"));
	case ZC_NAME_IN_USE:
		return (dgettext(TEXT_DOMAIN,
		    "Zone cluster name already in use"));
	case ZC_NO_SUCH_ID:
		return (dgettext(TEXT_DOMAIN, "No such zone ID"));
	case ZC_UPDATING_INDEX:
		return (dgettext(TEXT_DOMAIN, "Problem updating index file"));
	case ZC_LOCKING_FILE:
		return (dgettext(TEXT_DOMAIN, "Locking index file"));
	case ZC_UNLOCKING_FILE:
		return (dgettext(TEXT_DOMAIN, "Unlocking index file"));
	case ZC_INSUFFICIENT_SPEC:
		return (dgettext(TEXT_DOMAIN, "Insufficient specification"));
	case ZC_RESOLVED_PATH:
		return (dgettext(TEXT_DOMAIN, "Resolved path mismatch"));
	case ZC_IPV6_ADDR_PREFIX_LEN:
		return (dgettext(TEXT_DOMAIN,
		    "IPv6 address missing required prefix length"));
	case ZC_BOGUS_ADDRESS:
		return (dgettext(TEXT_DOMAIN,
		    "Neither an IPv4 nor an IPv6 address nor a host name"));
	case ZC_PRIV_PROHIBITED:
		return (dgettext(TEXT_DOMAIN,
		    "Specified privilege is prohibited"));
	case ZC_PRIV_REQUIRED:
		return (dgettext(TEXT_DOMAIN,
		    "Required privilege is missing"));
	case ZC_PRIV_UNKNOWN:
		return (dgettext(TEXT_DOMAIN,
		    "Specified privilege is unknown"));
	case ZC_BRAND_ERROR:
		return (dgettext(TEXT_DOMAIN,
		    "Brand-specific error"));
	case ZC_NO_CLUSTER:
		return (dgettext(TEXT_DOMAIN,
		    "Not a cluster node"));
	case ZC_SYSTEM:
		return (dgettext(TEXT_DOMAIN,
		    "System error"));
	case ZC_REMOTE_EXEC_ERR:
		return (dgettext(TEXT_DOMAIN,
		    "Remote command execution failure"));
	case ZC_NODE_DOWN:
		return (dgettext(TEXT_DOMAIN,
		    "Node down or node not in cluster"));
	case ZC_CCR_ERROR:
		return (dgettext(TEXT_DOMAIN,
		    "CCR transaction error"));
	case ZC_NAMESERVER_ERR:
		return (dgettext(TEXT_DOMAIN,
		    "Cluster nameserver error"));
	case ZC_VERSION_ERR:
		return (dgettext(TEXT_DOMAIN,
		    "Cluster version manager error"));
	case ZC_NO_SUBNET:
		return (dgettext(TEXT_DOMAIN,
		    "No subnet available"));
	case ZC_MISSING_CONFIG:
		return (dgettext(TEXT_DOMAIN,
		    "Missing zone cluster configuration"));
	default:
		return (dgettext(TEXT_DOMAIN, "Unknown error"));
	}
}

static int
newprop(xmlNodePtr node, const xmlChar *attrname, const char *src)
{
	xmlAttrPtr newattr;

	newattr = xmlNewProp(node, attrname, (xmlChar *)src);
	if (newattr == NULL) {
		xmlUnlinkNode(node);
		xmlFreeNode(node);
		return (ZC_BAD_PROPERTY);
	}
	return (ZC_OK);
}

static int
zccfg_add_filesystem_core(zc_dochandle_t handle,
    struct zc_fstab *tabp)
{
	xmlNodePtr	newnode;
	xmlNodePtr	cur = handle->zc_dh_cur;
	xmlNodePtr	options_node;
	zc_fsopt_t	*ptr;
	int		err;

	newnode = xmlNewTextChild(cur, NULL, DTD_ELEM_FS, NULL);
	if ((err = newprop(newnode, DTD_ATTR_SPECIAL,
	    tabp->zc_fs_special)) != ZC_OK) {
		return (err);
	}
	// Raw is optional, for example, for loopback file systems.
	if (tabp->zc_fs_raw[0] != '\0' &&
	    (err = newprop(newnode, DTD_ATTR_RAW,
	    tabp->zc_fs_raw)) != ZC_OK) {
		return (err);
	}
	if ((err = newprop(newnode, DTD_ATTR_DIR,
	    tabp->zc_fs_dir)) != ZC_OK) {
		return (err);
	}
	if ((err = newprop(newnode, DTD_ATTR_TYPE,
	    tabp->zc_fs_type)) != ZC_OK) {
		return (err);
	}
	if (tabp->zc_fs_options != NULL) {
		for (ptr = tabp->zc_fs_options; ptr != NULL;
		    ptr = ptr->zc_fsopt_next) {
			options_node = xmlNewTextChild(newnode, NULL,
			    DTD_ELEM_FSOPTION, NULL);
			if ((err = newprop(options_node, DTD_ATTR_NAME,
			    ptr->zc_fsopt_opt)) != ZC_OK) {
				return (err);
			}
		}
	}
	return (ZC_OK);
}

int
zccfg_add_filesystem(zc_dochandle_t handle, struct zc_fstab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	return (zccfg_add_filesystem_core(handle, tabp));
}

//
// Delete the matching fs resource. An fs resource is uniquely identified by
// 4-tuple [zc_fs_dir, zc_fs_special, zc_fs_raw, zc_fs_type].
// We must therefore ensure that all these properties match before deleting
// an entry.
//
static int
zccfg_delete_filesystem_core(zc_dochandle_t handle,
    struct zc_fstab *tabp)
{
	xmlNodePtr cur = handle->zc_dh_cur;
	bool dir_match, spec_match, raw_match, type_match;

	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_FS)) {
			continue;
		}

		dir_match = match_prop(cur, DTD_ATTR_DIR, tabp->zc_fs_dir);
		spec_match = match_prop(cur, DTD_ATTR_SPECIAL,
		    tabp->zc_fs_special);
		raw_match = match_prop(cur, DTD_ATTR_RAW, tabp->zc_fs_raw);
		type_match = match_prop(cur, DTD_ATTR_TYPE,
		    tabp->zc_fs_type);

		// Delete only if everything matches.
		if (dir_match && spec_match && raw_match && type_match) {
			xmlUnlinkNode(cur);
			xmlFreeNode(cur);
			return (ZC_OK);
		}
	}
	return (ZC_NO_RESOURCE_ID);
}

int
zccfg_delete_filesystem(zc_dochandle_t handle,
    struct zc_fstab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	return (zccfg_delete_filesystem_core(handle, tabp));
}

int
zccfg_modify_filesystem(zc_dochandle_t handle,
    struct zc_fstab *oldtabp, struct zc_fstab *newtabp)
{
	int err;

	if (oldtabp == NULL || newtabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_delete_filesystem_core(handle, oldtabp))
	    != ZC_OK) {
		return (err);
	}

	return (zccfg_add_filesystem_core(handle, newtabp));
}

//
// Lookup and retrieve file system specified by the second argument. If a
// matching fs resource is found, it is returned via the tabp pointer. If more
// than one fs resources satisfy the specified criteria, ZC_INSUFFICIENT_SPEC
// is returned.
//
int
zccfg_lookup_filesystem(
	zc_dochandle_t handle,
	struct zc_fstab *tabp)
{
	xmlNodePtr	cur;
	xmlNodePtr	options;
	xmlNodePtr	firstmatch;
	int		err;
	char		dir_name[MAXPATHLEN];
	char		special[MAXPATHLEN];
	char		raw[MAXPATHLEN];
	char		type[FSTYPSZ];
	char		options_str[MAX_MNTOPT_STR];

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	//
	// Walk the list of children looking for matches on any properties
	// specified in the fstab parameter.  If more than one resource
	// matches, we return ZC_INSUFFICIENT_SPEC; if none match, we return
	// ZC_NO_RESOURCE_ID.
	//
	cur = handle->zc_dh_cur;
	firstmatch = NULL;
	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_FS)) {
			continue;
		}
		if (strlen(tabp->zc_fs_dir) > 0) {
			if ((fetchprop(cur, DTD_ATTR_DIR, dir_name,
			    sizeof (dir_name)) == ZC_OK) &&
			    (strcmp(tabp->zc_fs_dir, dir_name) == 0)) {
				if (firstmatch == NULL) {
					firstmatch = cur;
				} else {
					return (ZC_INSUFFICIENT_SPEC);
				}
			}
		}
		if (strlen(tabp->zc_fs_special) > 0) {
			if ((fetchprop(cur, DTD_ATTR_SPECIAL, special,
			    sizeof (special)) == ZC_OK)) {
				if (strcmp(tabp->zc_fs_special,
				    special) == 0) {
					if (firstmatch == NULL) {
						firstmatch = cur;
					} else if (firstmatch != cur) {
						return (ZC_INSUFFICIENT_SPEC);
					}
				} else {
					//
					// If another property matched but this
					// one doesn't then reset firstmatch.
					//
					if (firstmatch == cur) {
						firstmatch = NULL;
					}
				}
			}
		}
		if (strlen(tabp->zc_fs_raw) > 0) {
			if ((fetchprop(cur, DTD_ATTR_RAW, raw,
			    sizeof (raw)) == ZC_OK)) {
				if (strcmp(tabp->zc_fs_raw, raw) == 0) {
					if (firstmatch == NULL) {
						firstmatch = cur;
					} else if (firstmatch != cur) {
						return (ZC_INSUFFICIENT_SPEC);
					}
				} else {
					//
					// If another property matched but this
					// one doesn't then reset firstmatch.
					//
					if (firstmatch == cur) {
						firstmatch = NULL;
					}
				}
			}
		}
		if (strlen(tabp->zc_fs_type) > 0) {
			if ((fetchprop(cur, DTD_ATTR_TYPE, type,
			    sizeof (type)) == ZC_OK)) {
				if (strcmp(tabp->zc_fs_type, type) == 0) {
					if (firstmatch == NULL) {
						firstmatch = cur;
					} else if (firstmatch != cur) {
						return (ZC_INSUFFICIENT_SPEC);
					}
				} else {
					//
					// If another property matched but this
					// one doesn't then reset firstmatch.
					//
					if (firstmatch == cur) {
						firstmatch = NULL;
					}
				}
			}
		}
	}

	if (firstmatch == NULL) {
		return (ZC_NO_RESOURCE_ID);
	}

	cur = firstmatch;

	if ((err = fetchprop(cur, DTD_ATTR_DIR, tabp->zc_fs_dir,
	    sizeof (tabp->zc_fs_dir))) != ZC_OK) {
		return (err);
	}

	if ((err = fetchprop(cur, DTD_ATTR_SPECIAL, tabp->zc_fs_special,
	    sizeof (tabp->zc_fs_special))) != ZC_OK) {
		return (err);
	}

	if ((err = fetchprop(cur, DTD_ATTR_RAW, tabp->zc_fs_raw,
	    sizeof (tabp->zc_fs_raw))) != ZC_OK) {
		return (err);
	}

	if ((err = fetchprop(cur, DTD_ATTR_TYPE, tabp->zc_fs_type,
	    sizeof (tabp->zc_fs_type))) != ZC_OK) {
		return (err);
	}

	// options are optional
	tabp->zc_fs_options = NULL;
	for (options = cur->xmlChildrenNode; options != NULL;
	    options = options->next) {
		if ((fetchprop(options, DTD_ATTR_NAME, options_str,
		    sizeof (options_str)) != ZC_OK)) {
			break;
		}
		if (zccfg_add_fs_option(tabp, options_str) != ZC_OK) {
			break;
		}
	}
	return (ZC_OK);
}

static int
zccfg_add_ipd_core(zc_dochandle_t handle, struct zc_fstab *tabp)
{
	xmlNodePtr newnode, cur = handle->zc_dh_cur;
	int err;

	newnode = xmlNewTextChild(cur, NULL, DTD_ELEM_IPD, NULL);
	if ((err = newprop(newnode, DTD_ATTR_DIR, tabp->zc_fs_dir))
	    != ZC_OK) {
		return (err);
	}
	return (ZC_OK);
}

int
zccfg_add_ipd(zc_dochandle_t handle, struct zc_fstab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_add_ipd_core(handle, tabp)) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

//
// Delete matching iherit-pkg-dir resource.
//
static int
zccfg_delete_ipd_core(zc_dochandle_t handle,
    struct zc_fstab *tabp)
{
	xmlNodePtr cur = handle->zc_dh_cur;

	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_IPD)) {
			continue;
		}

		if (match_prop(cur, DTD_ATTR_DIR, tabp->zc_fs_dir)) {
			xmlUnlinkNode(cur);
			xmlFreeNode(cur);
			return (ZC_OK);
		}
	}
	return (ZC_NO_RESOURCE_ID);
}

int
zccfg_delete_ipd(zc_dochandle_t handle, struct zc_fstab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	return (zccfg_delete_ipd_core(handle, tabp));
}

int
zccfg_modify_ipd(zc_dochandle_t handle,
    struct zc_fstab *oldtabp, struct zc_fstab *newtabp)
{
	int err;

	if (oldtabp == NULL || newtabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_delete_ipd_core(handle, oldtabp)) != ZC_OK) {
		return (err);
	}

	return (zccfg_add_ipd_core(handle, newtabp));
}

int
zccfg_lookup_ipd(zc_dochandle_t handle, struct zc_fstab *tabp)
{
	xmlNodePtr cur, match;
	int err;
	char dir_name[MAXPATHLEN];

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}
	//
	// General algorithm:
	// Walk the list of children looking for matches on any properties
	// specified in the fstab parameter.  If more than one resource
	// matches, we return ZC_INSUFFICIENT_SPEC; if none match, we return
	// ZC_NO_RESOURCE_ID.
	//
	cur = handle->zc_dh_cur;
	match = NULL;
	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_IPD)) {
			continue;
		}
		if (strlen(tabp->zc_fs_dir) > 0) {
			if ((fetchprop(cur, DTD_ATTR_DIR, dir_name,
			    sizeof (dir_name)) == ZC_OK) &&
			    (strcmp(tabp->zc_fs_dir, dir_name) == 0)) {
				if (match == NULL) {
					match = cur;
				} else {
					return (ZC_INSUFFICIENT_SPEC);
				}
			}
		}
	}

	if (match == NULL) {
		return (ZC_NO_RESOURCE_ID);
	}
	cur = match;

	if ((err = fetchprop(cur, DTD_ATTR_DIR, tabp->zc_fs_dir,
	    sizeof (tabp->zc_fs_dir))) != ZC_OK) {
		return (err);
	}
	return (ZC_OK);
}

//
// Compare two IP addresses in string form.  Allow for the possibility that
// one might have "/<prefix-length>" at the end: allow a match on just the
// IP address (or host name) part.
//
bool
zccfg_same_net_address(char *a1, char *a2)
{
	char *slashp, *slashp1, *slashp2;
	int result;

	if (strcmp(a1, a2) == 0) {
		return (true);
	}

	//
	// If neither has a slash or both do, they need to match to be
	// considered the same, but they did not match above, so fail.
	//
	slashp1 = strchr(a1, '/');
	slashp2 = strchr(a2, '/');
	if ((slashp1 == NULL && slashp2 == NULL) ||
	    (slashp1 != NULL && slashp2 != NULL)) {
		return (false);
	}

	//
	// Only one had a slash: pick that one, zero out the slash, compare
	// the "address only" strings, restore the slash, and return the
	// result of the comparison.
	//
	slashp = (slashp1 == NULL) ? slashp2 : slashp1;
	*slashp = '\0';
	result = strcmp(a1, a2);
	*slashp = '/';
	return ((result == 0));
}

int
zccfg_lookup_nwif(zc_dochandle_t handle, struct zc_nwiftab *tabp)
{
	xmlNodePtr	cur;
	xmlNodePtr	firstmatch;
	int		err;
	char		address[INET6_ADDRSTRLEN];
	char		physical[LIFNAMSIZ];

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	cur = handle->zc_dh_cur;
	firstmatch = NULL;
	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_NET)) {
			continue;
		}
		if (strlen(tabp->zc_nwif_physical) > 0) {
			if ((fetchprop(cur, DTD_ATTR_PHYSICAL, physical,
			    sizeof (physical)) == ZC_OK) &&
			    (strcmp(tabp->zc_nwif_physical,
			    physical) == 0)) {
				if (firstmatch == NULL) {
					firstmatch = cur;
				} else {
					return (ZC_INSUFFICIENT_SPEC);
				}
			}
		}
		if (strlen(tabp->zc_nwif_address) > 0) {
			if ((fetchprop(cur, DTD_ATTR_ADDRESS, address,
			    sizeof (address)) == ZC_OK)) {
				if (zccfg_same_net_address(
				    tabp->zc_nwif_address, address)) {
					if (firstmatch == NULL) {
						firstmatch = cur;
					} else if (firstmatch != cur) {
						return (ZC_INSUFFICIENT_SPEC);
					}
				} else {
					//
					// If another property matched but this
					// one doesn't then reset firstmatch.
					//
					if (firstmatch == cur) {
						firstmatch = NULL;
					}
				}
			}
		}
	}
	if (firstmatch == NULL) {
		return (ZC_NO_RESOURCE_ID);
	}
	cur = firstmatch;

	if ((err = fetchprop(cur, DTD_ATTR_PHYSICAL, tabp->zc_nwif_physical,
	    sizeof (tabp->zc_nwif_physical))) != ZC_OK) {
		return (err);
	}

	if ((err = fetchprop(cur, DTD_ATTR_ADDRESS, tabp->zc_nwif_address,
	    sizeof (tabp->zc_nwif_address))) != ZC_OK) {
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_DEFROUTER, tabp->zc_nwif_defrouter,
	    sizeof (tabp->zc_nwif_defrouter)) != ZC_OK)) {
		return (err);
	}

	return (ZC_OK);
}

static int
zccfg_add_nwif_core(zc_dochandle_t handle, struct zc_nwiftab *tabp)
{
	xmlNodePtr newnode, cur = handle->zc_dh_cur;
	int err;

	newnode = xmlNewTextChild(cur, NULL, DTD_ELEM_NET, NULL);
	if ((err = newprop(newnode, DTD_ATTR_ADDRESS,
	    tabp->zc_nwif_address)) != ZC_OK) {
		return (err);
	}
	if ((err = newprop(newnode, DTD_ATTR_PHYSICAL,
	    tabp->zc_nwif_physical)) != ZC_OK) {
		return (err);
	}
	//
	// Do not add this property when it is not set, for backwards
	// compatibility and because it is optional.
	//
	if ((strlen(tabp->zc_nwif_defrouter) > 0) &&
	    ((err = newprop(newnode, DTD_ATTR_DEFROUTER,
	    tabp->zc_nwif_defrouter)) != ZC_OK)) {
		return (err);
	}
	return (ZC_OK);
}

int
zccfg_add_nwif(zc_dochandle_t handle, struct zc_nwiftab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}
	return (zccfg_add_nwif_core(handle, tabp));
}

//
// Delete matching net resource. Here we must match both the address and
// physical properties because a given physical interface can host more than
// one IP addresses.
//
static int
zccfg_delete_nwif_core(zc_dochandle_t handle,
    struct zc_nwiftab *tabp)
{
	xmlNodePtr cur = handle->zc_dh_cur;
	bool addr_match;
	bool phys_match;

	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_NET)) {
			continue;
		}

		addr_match = match_prop(cur, DTD_ATTR_ADDRESS,
		    tabp->zc_nwif_address);
		phys_match = match_prop(cur, DTD_ATTR_PHYSICAL,
		    tabp->zc_nwif_physical);
		if (addr_match && phys_match) {
			xmlUnlinkNode(cur);
			xmlFreeNode(cur);
			return (ZC_OK);
		}
	}
	return (ZC_NO_RESOURCE_ID);
}

int
zccfg_delete_nwif(zc_dochandle_t handle, struct zc_nwiftab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	return (zccfg_delete_nwif_core(handle, tabp));
}

int
zccfg_modify_nwif(zc_dochandle_t handle,
    struct zc_nwiftab *oldtabp, struct zc_nwiftab *newtabp)
{
	int err;

	if (oldtabp == NULL || newtabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_delete_nwif_core(handle, oldtabp)) != ZC_OK) {
		return (err);
	}

	return (zccfg_add_nwif_core(handle, newtabp));
}

int
zccfg_lookup_node(zc_dochandle_t handle, struct zc_nodetab *tabp)
{
	xmlNodePtr cur, firstmatch;
	int err;
	char nodename[SYS_NMLN], hostname[SYS_NMLN];

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	cur = handle->zc_dh_cur;
	firstmatch = NULL;
	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_NODE)) {
			continue;
		}
		if (strlen(tabp->zc_nodename) > 0) {
			if ((fetchprop(cur, DTD_ATTR_PHYSICALHOST, nodename,
			    sizeof (nodename)) == ZC_OK) &&
			    (strcmp(tabp->zc_nodename,
			    nodename) == 0)) {
				if (firstmatch == NULL) {
					firstmatch = cur;
					break;
				} else {
					return (ZC_INSUFFICIENT_SPEC);
				}
			}
		}
		if (strlen(tabp->zc_hostname) > 0) {
			if ((fetchprop(cur, DTD_ATTR_HOSTNAME, hostname,
			    sizeof (hostname)) == ZC_OK)) {
				if (strcmp(tabp->zc_hostname,
				    hostname) == 0) {
					if (firstmatch == NULL) {
						firstmatch = cur;
					} else if (firstmatch != cur) {
						return (ZC_INSUFFICIENT_SPEC);
					}
				} else {
					//
					// If another property matched but this
					// one doesn't then reset firstmatch.
					//
					if (firstmatch == cur) {
						firstmatch = NULL;
					}
				}
			}
		}
	}
	if (firstmatch == NULL) {
		return (ZC_NO_RESOURCE_ID);
	}

	cur = firstmatch;

	if ((err = fetchprop(cur, DTD_ATTR_PHYSICALHOST, tabp->zc_nodename,
	    sizeof (tabp->zc_nodename))) != ZC_OK) {
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_HOSTNAME, tabp->zc_hostname,
	    sizeof (tabp->zc_hostname))) != ZC_OK) {
		return (err);
	}

	(void) zccfg_get_local_net(cur->children, tabp);

	//
	// TODO
	// We will have to retrieve all of the node-specific info here.
	// To be implemented later.
	//
	return (ZC_OK);
}

static int
zccfg_add_nwif_to_node(xmlNodePtr parent, zc_nwifelem_t *listp)
{
	int err;
	xmlNodePtr newnode;

	while (listp != NULL) {
		newnode = xmlNewTextChild(parent, NULL, DTD_ELEM_NET, NULL);
		if ((err = newprop(newnode, DTD_ATTR_ADDRESS,
		    listp->elem.zc_nwif_address)) != ZC_OK) {
			return (err);
		}
		if ((err = newprop(newnode, DTD_ATTR_PHYSICAL,
		    listp->elem.zc_nwif_physical)) != ZC_OK) {
			return (err);
		}
		if ((err = newprop(newnode, DTD_ATTR_DEFROUTER,
		    listp->elem.zc_nwif_defrouter)) != ZC_OK) {
			return (err);
		}
		listp = listp->next;
	}
	return (ZC_OK);
}

static int
zccfg_add_node_core(zc_dochandle_t handle, struct zc_nodetab *tabp)
{
	xmlNodePtr newnode, cur = handle->zc_dh_cur;
	zc_nwifelem_t *nwif_listp = tabp->zc_nwif_listp;
	int err;

	newnode = xmlNewTextChild(cur, NULL, DTD_ELEM_NODE, NULL);
	if ((err = newprop(newnode, DTD_ATTR_PHYSICALHOST,
	    tabp->zc_nodename)) != ZC_OK) {
		return (err);
	}
	if ((err = newprop(newnode, DTD_ATTR_HOSTNAME,
	    tabp->zc_hostname)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_add_nwif_to_node(newnode, nwif_listp)) != ZC_OK) {
		return (err);
	}

	// TODO insert the rest of the node scope stuff here

	return (ZC_OK);
}

int
zccfg_add_node(zc_dochandle_t handle, struct zc_nodetab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}
	return (zccfg_add_node_core(handle, tabp));
}

//
// Delete node resource whose nodename matches the one specified in the second
// argument. We do not have to check whether or not the hostnames match,
// because there can only be one node resource with a given nodename.
//
static int
zccfg_delete_node_core(zc_dochandle_t handle,
    struct zc_nodetab *tabp)
{
	xmlNodePtr cur = handle->zc_dh_cur;

	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_NODE)) {
			continue;
		}

		if (match_prop(cur, DTD_ATTR_PHYSICALHOST,
		    tabp->zc_nodename)) {
			xmlUnlinkNode(cur);
			xmlFreeNode(cur);
			return (ZC_OK);
		}
	}
	return (ZC_NO_RESOURCE_ID);
}

int
zccfg_delete_node(zc_dochandle_t handle, struct zc_nodetab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	return (zccfg_delete_node_core(handle, tabp));
}

int
zccfg_modify_node(zc_dochandle_t handle,
    struct zc_nodetab *oldtabp, struct zc_nodetab *newtabp)
{
	int err;

	if (oldtabp == NULL || newtabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_delete_node_core(handle, oldtabp)) != ZC_OK) {
		return (err);
	}

	return (zccfg_add_node_core(handle, newtabp));
}

int
zccfg_lookup_sysid(zc_dochandle_t handle, struct zc_sysidtab *tabp)
{
	xmlNodePtr cur;
	int err;
	bool found = false;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	cur = handle->zc_dh_cur;
	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_SYSID) == 0) {
			found = true;
			break;
		}
	}

	if (!found) {
		return (ZC_NO_ENTRY);
	}

	if ((err = fetch_alloc_prop(cur, DTD_ATTR_ROOTPASSWD,
	    &(tabp->zc_root_password))) != ZC_OK) {
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_NAMESERVICE,
	    tabp->zc_name_service,
	    sizeof (tabp->zc_name_service))) != ZC_OK) {
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_NFS4DOMAIN,
	    tabp->zc_nfs4_domain,
	    sizeof (tabp->zc_nfs4_domain))) != ZC_OK) {
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_SECPOLICY,
	    tabp->zc_sec_policy,
	    sizeof (tabp->zc_sec_policy))) != ZC_OK) {
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_SYSLOCALE,
	    tabp->zc_sys_locale,
	    sizeof (tabp->zc_sys_locale))) != ZC_OK) {
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_TERMINAL, tabp->zc_terminal,
	    sizeof (tabp->zc_terminal))) != ZC_OK) {
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_TIMEZONE, tabp->zc_timezone,
	    sizeof (tabp->zc_timezone))) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

static int
zccfg_add_sysid_core(zc_dochandle_t handle,
    struct zc_sysidtab *tabp)
{
	xmlNodePtr newnode, cur = handle->zc_dh_cur;
	int err;

	newnode = xmlNewTextChild(cur, NULL, DTD_ELEM_SYSID, NULL);
	if ((err = newprop(newnode, DTD_ATTR_ROOTPASSWD,
	    tabp->zc_root_password)) != ZC_OK) {
		return (err);
	}
	if ((err = newprop(newnode, DTD_ATTR_NAMESERVICE,
	    tabp->zc_name_service)) != ZC_OK) {
		return (err);
	}
	if ((err = newprop(newnode, DTD_ATTR_NFS4DOMAIN,
	    tabp->zc_nfs4_domain)) != ZC_OK) {
		return (err);
	}
	if ((err = newprop(newnode, DTD_ATTR_SECPOLICY,
	    tabp->zc_sec_policy)) != ZC_OK) {
		return (err);
	}
	if ((err = newprop(newnode, DTD_ATTR_SYSLOCALE,
	    tabp->zc_sys_locale)) != ZC_OK) {
		return (err);
	}
	if ((err = newprop(newnode, DTD_ATTR_TERMINAL,
	    tabp->zc_terminal)) != ZC_OK) {
		return (err);
	}
	if ((err = newprop(newnode, DTD_ATTR_TIMEZONE,
	    tabp->zc_timezone)) != ZC_OK) {
		return (err);
	}

	// (void) xmlDocDump(stderr, handle->zc_dh_doc);
	/* insert the rest of the sysid scope stuff here */

	return (ZC_OK);
}

int
zccfg_add_sysid(zc_dochandle_t handle, struct zc_sysidtab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}
	return (zccfg_add_sysid_core(handle, tabp));
}

//
// Delete the sysid resource. Note that no complicated matching logic is
// required since there can only be one sysid resource per zone cluster.
//
static int
zccfg_delete_sysid_core(zc_dochandle_t handle,
    struct zc_sysidtab *tabp)
{
	xmlNodePtr cur = handle->zc_dh_cur;

	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_SYSID)) {
			continue;
		}

		xmlUnlinkNode(cur);
		xmlFreeNode(cur);
		return (ZC_OK);
	}
	return (ZC_NO_RESOURCE_ID);
}

int
zccfg_delete_sysid(zc_dochandle_t handle, struct zc_sysidtab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	return (zccfg_delete_sysid_core(handle, tabp));
}

int
zccfg_modify_sysid(zc_dochandle_t handle,
    struct zc_sysidtab *oldtabp, struct zc_sysidtab *newtabp)
{
	int err;

	if (oldtabp == NULL || newtabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_delete_sysid_core(handle, oldtabp)) != ZC_OK) {
		return (err);
	}

	return (zccfg_add_sysid_core(handle, newtabp));
}

int
zccfg_lookup_ds(zc_dochandle_t handle, struct zc_dstab *tabp)
{
	xmlNodePtr cur, firstmatch;
	int err;
	char dataset[MAXNAMELEN];

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	cur = handle->zc_dh_cur;
	firstmatch = NULL;
	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_DATASET)) {
			continue;
		}
		if (strlen(tabp->zc_dataset_name) > 0) {
			if ((fetchprop(cur, DTD_ATTR_NAME, dataset,
			    sizeof (dataset)) == ZC_OK) &&
			    (strcmp(tabp->zc_dataset_name,
			    dataset) == 0)) {
				if (firstmatch == NULL) {
					firstmatch = cur;
				} else {
					return (ZC_INSUFFICIENT_SPEC);
				}
			}
		}
	}
	if (firstmatch == NULL) {
		return (ZC_NO_RESOURCE_ID);
	}
	cur = firstmatch;

	if ((err = fetchprop(cur, DTD_ATTR_NAME, tabp->zc_dataset_name,
	    sizeof (tabp->zc_dataset_name))) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

static int
zccfg_add_ds_core(zc_dochandle_t handle, struct zc_dstab *tabp)
{
	xmlNodePtr newnode, cur = handle->zc_dh_cur;
	int err;

	newnode = xmlNewTextChild(cur, NULL, DTD_ELEM_DATASET, NULL);
	if ((err = newprop(newnode, DTD_ATTR_NAME,
	    tabp->zc_dataset_name)) != ZC_OK) {
		return (err);
	}
	return (ZC_OK);
}

int
zccfg_add_ds(zc_dochandle_t handle, struct zc_dstab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_add_ds_core(handle, tabp)) != ZC_OK) {
		return (err);
	}
	return (ZC_OK);
}

//
// Delete matching dataset resource.
//
static int
zccfg_delete_ds_core(zc_dochandle_t handle,
    struct zc_dstab *tabp)
{
	xmlNodePtr cur = handle->zc_dh_cur;

	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_DATASET)) {
			continue;
		}

		if (match_prop(cur, DTD_ATTR_NAME,
		    tabp->zc_dataset_name)) {
			xmlUnlinkNode(cur);
			xmlFreeNode(cur);
			return (ZC_OK);
		}
	}

	return (ZC_NO_RESOURCE_ID);
}

int
zccfg_delete_ds(zc_dochandle_t handle, struct zc_dstab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	return (zccfg_delete_ds_core(handle, tabp));
}

int
zccfg_modify_ds(zc_dochandle_t handle,
    struct zc_dstab *oldtabp, struct zc_dstab *newtabp)
{
	int err;

	if (oldtabp == NULL || newtabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_delete_ds_core(handle, oldtabp)) != ZC_OK) {
		return (err);
	}

	return (zccfg_add_ds_core(handle, newtabp));
}

//
// Lookup the device resource pointed to by the second argument in the XML
// document.
int
zccfg_lookup_dev(zc_dochandle_t handle, struct zc_devtab *tabp)
{
	xmlNodePtr cur, firstmatch;
	int err;
	char match[MAXPATHLEN];

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	cur = handle->zc_dh_cur;
	firstmatch = NULL;
	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_DEVICE)) {
			continue;
		}
		if (strlen(tabp->zc_dev_match) == 0) {
			continue;
		}

		if ((fetchprop(cur, DTD_ATTR_MATCH, match,
		    sizeof (match)) == ZC_OK)) {
			if (strcmp(tabp->zc_dev_match,
			    match) == 0) {
				if (firstmatch == NULL) {
					firstmatch = cur;
				} else if (firstmatch != cur) {
					return (ZC_INSUFFICIENT_SPEC);
				}
			} else {
				/*
				 * If another property matched but this
				 * one doesn't then reset firstmatch.
				 */
				if (firstmatch == cur) {
					firstmatch = NULL;
				}
			}
		}
	}
	if (firstmatch == NULL) {
		return (ZC_NO_RESOURCE_ID);
	}

	cur = firstmatch;
	if ((err = fetchprop(cur, DTD_ATTR_MATCH, tabp->zc_dev_match,
	    sizeof (tabp->zc_dev_match))) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

static int
zccfg_add_dev_core(zc_dochandle_t handle, struct zc_devtab *tabp)
{
	xmlNodePtr newnode, cur = handle->zc_dh_cur;
	int err;

	newnode = xmlNewTextChild(cur, NULL, DTD_ELEM_DEVICE, NULL);

	if ((err = newprop(newnode, DTD_ATTR_MATCH,
	    tabp->zc_dev_match)) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

//
// Add device resource pointed to by the second argument to handle.
//
int
zccfg_add_dev(zc_dochandle_t handle, struct zc_devtab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_add_dev_core(handle, tabp)) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

//
// Delete matching device resource.
//
static int
zccfg_delete_dev_core(zc_dochandle_t handle,
    struct zc_devtab *tabp)
{
	xmlNodePtr cur = handle->zc_dh_cur;

	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_DEVICE)) {
			continue;
		}

		if (match_prop(cur, DTD_ATTR_MATCH,
		    tabp->zc_dev_match)) {
			xmlUnlinkNode(cur);
			xmlFreeNode(cur);
			return (ZC_OK);
		}
	}

	return (ZC_NO_RESOURCE_ID);
}

int
zccfg_delete_dev(zc_dochandle_t handle, struct zc_devtab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	return (zccfg_delete_dev_core(handle, tabp));
}

//
// Modify a device resource entry. We just delete the old entry from handle,
// and add the new one in its place.
//
int
zccfg_modify_dev(zc_dochandle_t handle,
    struct zc_devtab *oldtabp, struct zc_devtab *newtabp)
{
	int err;

	if (oldtabp == NULL || strlen(oldtabp->zc_dev_match) == 0 ||
	    newtabp == NULL || strlen(newtabp->zc_dev_match) == 0) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_delete_dev_core(handle, oldtabp)) != ZC_OK) {
		return (err);
	}

	return (zccfg_add_dev_core(handle, newtabp));
}

//
// Set the XML document's 'cur' pointer to the 'children node' pointer of the
// root node. zccfg_setXent() functions are just wrappers on this function.
//
static int
zccfg_setent(zc_dochandle_t handle)
{
	xmlNodePtr cur;
	int err;

	if (handle == NULL) {
		return (ZC_INVAL);
	}
	if ((err = operation_prep(handle)) != ZC_OK) {
		handle->zc_dh_cur = NULL;
		return (err);
	}
	cur = handle->zc_dh_cur;
	cur = cur->xmlChildrenNode;
	handle->zc_dh_cur = cur;
	return (ZC_OK);
}

//
// "Rewind" the 'cur' pointer to the root node. zccfg_endXent() functions
// are just wrappers on this function.
//
static int
zccfg_endent(zc_dochandle_t handle)
{
	if (handle == NULL) {
		return (ZC_INVAL);
	}
	handle->zc_dh_cur = handle->zc_dh_top;
	return (ZC_OK);
}

int
zccfg_setfsent(zc_dochandle_t handle)
{
	return (zccfg_setent(handle));
}

//
// Get a fs resource from handle, and return it in the second argument.
//
int
zccfg_getfsent(zc_dochandle_t handle, struct zc_fstab *tabp)
{
	xmlNodePtr cur, options;
	char options_str[MAX_MNTOPT_STR];
	int err;

	if (handle == NULL) {
		return (ZC_INVAL);
	}

	if ((cur = handle->zc_dh_cur) == NULL) {
		return (ZC_NO_ENTRY);
	}

	for (; cur != NULL; cur = cur->next) {
		if (!xmlStrcmp(cur->name, DTD_ELEM_FS)) {
			break;
		}
	}
	if (cur == NULL) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (ZC_NO_ENTRY);
	}

	if ((err = fetchprop(cur, DTD_ATTR_SPECIAL, tabp->zc_fs_special,
	    sizeof (tabp->zc_fs_special))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	if ((err = fetchprop(cur, DTD_ATTR_RAW, tabp->zc_fs_raw,
	    sizeof (tabp->zc_fs_raw))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	if ((err = fetchprop(cur, DTD_ATTR_DIR, tabp->zc_fs_dir,
	    sizeof (tabp->zc_fs_dir))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	if ((err = fetchprop(cur, DTD_ATTR_TYPE, tabp->zc_fs_type,
	    sizeof (tabp->zc_fs_type))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	// OK for options to be NULL
	tabp->zc_fs_options = NULL;
	for (options = cur->xmlChildrenNode; options != NULL;
	    options = options->next) {
		if (fetchprop(options, DTD_ATTR_NAME, options_str,
		    sizeof (options_str)) != ZC_OK) {
			break;
		}
		if (zccfg_add_fs_option(tabp, options_str) != ZC_OK) {
			break;
		}
	}

	handle->zc_dh_cur = cur->next;
	return (ZC_OK);
}

int
zccfg_endfsent(zc_dochandle_t handle)
{
	return (zccfg_endent(handle));
}

int
zccfg_setipdent(zc_dochandle_t handle)
{
	return (zccfg_setent(handle));
}

//
// Get an iherit-pkg-dir resource from handle, and return it in the second
// argument.
//
int
zccfg_getipdent(zc_dochandle_t handle, struct zc_fstab *tabp)
{
	xmlNodePtr cur;
	int err;

	if (handle == NULL) {
		return (ZC_INVAL);
	}

	if ((cur = handle->zc_dh_cur) == NULL) {
		return (ZC_NO_ENTRY);
	}

	for (; cur != NULL; cur = cur->next) {
		if (!xmlStrcmp(cur->name, DTD_ELEM_IPD)) {
			break;
		}
	}
	if (cur == NULL) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (ZC_NO_ENTRY);
	}

	if ((err = fetchprop(cur, DTD_ATTR_DIR, tabp->zc_fs_dir,
	    sizeof (tabp->zc_fs_dir))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	handle->zc_dh_cur = cur->next;
	return (ZC_OK);
}

int
zccfg_endipdent(zc_dochandle_t handle)
{
	return (zccfg_endent(handle));
}

int
zccfg_setnwifent(zc_dochandle_t handle)
{
	return (zccfg_setent(handle));
}

//
// Get a network resource from handle, and return it in the second argument.
//
int
zccfg_getnwifent(zc_dochandle_t handle, struct zc_nwiftab *tabp)
{
	xmlNodePtr cur;
	int err;

	if (handle == NULL) {
		return (ZC_INVAL);
	}
	if ((cur = handle->zc_dh_cur) == NULL) {
		return (ZC_NO_ENTRY);
	}
	for (; cur != NULL; cur = cur->next) {
		if (!xmlStrcmp(cur->name, DTD_ELEM_NET)) {
			break;
		}
	}
	if (cur == NULL) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (ZC_NO_ENTRY);
	}

	if ((err = fetchprop(cur, DTD_ATTR_ADDRESS, tabp->zc_nwif_address,
	    sizeof (tabp->zc_nwif_address))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	if ((err = fetchprop(cur, DTD_ATTR_PHYSICAL, tabp->zc_nwif_physical,
	    sizeof (tabp->zc_nwif_physical))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_DEFROUTER,
	    tabp->zc_nwif_defrouter,
	    sizeof (tabp->zc_nwif_defrouter))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	handle->zc_dh_cur = cur->next;
	return (ZC_OK);
}

int
zccfg_endnwifent(zc_dochandle_t handle)
{
	return (zccfg_endent(handle));
}

int
zccfg_setdevent(zc_dochandle_t handle)
{
	return (zccfg_setent(handle));
}

int
zccfg_getdevent(zc_dochandle_t handle, struct zc_devtab *tabp)
{
	xmlNodePtr cur;
	int err;

	if (handle == NULL) {
		return (ZC_INVAL);
	}
	if ((cur = handle->zc_dh_cur) == NULL) {
		return (ZC_NO_ENTRY);
	}

	for (; cur != NULL; cur = cur->next) {
		if (!xmlStrcmp(cur->name, DTD_ELEM_DEVICE)) {
			break;
		}
	}
	if (cur == NULL) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (ZC_NO_ENTRY);
	}

	if ((err = fetchprop(cur, DTD_ATTR_MATCH, tabp->zc_dev_match,
	    sizeof (tabp->zc_dev_match))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	handle->zc_dh_cur = cur->next;
	return (ZC_OK);
}

int
zccfg_enddevent(zc_dochandle_t handle)
{
	return (zccfg_endent(handle));
}

int
zccfg_setrctlent(zc_dochandle_t handle)
{
	return (zccfg_setent(handle));
}

int
zccfg_getrctlent(zc_dochandle_t handle, struct zc_rctltab *tabp)
{
	xmlNodePtr cur, val;
	struct zc_rctlvaltab *valp;
	int err;

	if (handle == NULL) {
		return (ZC_INVAL);
	}
	if ((cur = handle->zc_dh_cur) == NULL) {
		return (ZC_NO_ENTRY);
	}

	for (; cur != NULL; cur = cur->next) {
		if (!xmlStrcmp(cur->name, DTD_ELEM_RCTL)) {
			break;
		}
	}
	if (cur == NULL) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (ZC_NO_ENTRY);
	}

	if ((err = fetchprop(cur, DTD_ATTR_NAME, tabp->zc_rctl_name,
	    sizeof (tabp->zc_rctl_name))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	tabp->zc_rctl_valp = NULL;
	for (val = cur->xmlChildrenNode; val != NULL; val = val->next) {
		valp = (struct zc_rctlvaltab *)malloc(
		    sizeof (struct zc_rctlvaltab));
		if (valp == NULL) {
			return (ZC_NOMEM);
		}
		if (fetchprop(val, DTD_ATTR_PRIV, valp->zc_rctlval_priv,
		    sizeof (valp->zc_rctlval_priv)) != ZC_OK) {
			break;
		}
		if (fetchprop(val, DTD_ATTR_LIMIT, valp->zc_rctlval_limit,
		    sizeof (valp->zc_rctlval_limit)) != ZC_OK) {
			break;
		}
		if (fetchprop(val, DTD_ATTR_ACTION,
		    valp->zc_rctlval_action,
		    sizeof (valp->zc_rctlval_action)) != ZC_OK) {
			break;
		}

		if (zccfg_add_rctl_value(tabp, valp) != ZC_OK) {
			break;
		}
	}

	handle->zc_dh_cur = cur->next;
	return (ZC_OK);
}

int
zccfg_endrctlent(zc_dochandle_t handle)
{
	return (zccfg_endent(handle));
}

int
zccfg_setattrent(zc_dochandle_t handle)
{
	return (zccfg_setent(handle));
}

int
zccfg_getattrent(zc_dochandle_t handle, struct zc_attrtab *tabp)
{
	xmlNodePtr cur;
	int err;

	if (handle == NULL) {
		return (ZC_INVAL);
	}
	if ((cur = handle->zc_dh_cur) == NULL) {
		return (ZC_NO_ENTRY);
	}

	for (; cur != NULL; cur = cur->next) {
		if (!xmlStrcmp(cur->name, DTD_ELEM_ATTR)) {
			break;
		}
	}
	if (cur == NULL) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (ZC_NO_ENTRY);
	}

	if ((err = fetchprop(cur, DTD_ATTR_NAME, tabp->zc_attr_name,
	    sizeof (tabp->zc_attr_name))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	if ((err = fetchprop(cur, DTD_ATTR_TYPE, tabp->zc_attr_type,
	    sizeof (tabp->zc_attr_type))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	if ((err = fetchprop(cur, DTD_ATTR_VALUE, tabp->zc_attr_value,
	    sizeof (tabp->zc_attr_value))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	handle->zc_dh_cur = cur->next;
	return (ZC_OK);
}

int
zccfg_endattrent(zc_dochandle_t handle)
{
	return (zccfg_endent(handle));
}

int
zccfg_setdsent(zc_dochandle_t handle)
{
	return (zccfg_setent(handle));
}

int
zccfg_getdsent(zc_dochandle_t handle, struct zc_dstab *tabp)
{
	xmlNodePtr cur;
	int err;

	if (handle == NULL) {
		return (ZC_INVAL);
	}
	if ((cur = handle->zc_dh_cur) == NULL) {
		return (ZC_NO_ENTRY);
	}

	for (; cur != NULL; cur = cur->next) {
		if (!xmlStrcmp(cur->name, DTD_ELEM_DATASET)) {
			break;
		}
	}
	if (cur == NULL) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (ZC_NO_ENTRY);
	}

	if ((err = fetchprop(cur, DTD_ATTR_NAME, tabp->zc_dataset_name,
	    sizeof (tabp->zc_dataset_name))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	handle->zc_dh_cur = cur->next;
	return (ZC_OK);
}

int
zccfg_enddsent(zc_dochandle_t handle)
{
	return (zccfg_endent(handle));
}

int
zccfg_setsysident(zc_dochandle_t handle)
{
	return (zccfg_setent(handle));
}

int
zccfg_getsysident(zc_dochandle_t handle, struct zc_sysidtab *tabp)
{
	xmlNodePtr cur;
	int err;

	if (handle == NULL) {
		return (ZC_INVAL);
	}
	if ((cur = handle->zc_dh_cur) == NULL) {
		return (ZC_NO_ENTRY);
	}

	for (; cur != NULL; cur = cur->next) {
		if (!xmlStrcmp(cur->name, DTD_ELEM_SYSID)) {
			break;
		}
	}
	if (cur == NULL) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (ZC_NO_ENTRY);
	}

	if ((err = fetch_alloc_prop(cur, DTD_ATTR_ROOTPASSWD,
	    &(tabp->zc_root_password))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	if ((err = fetchprop(cur, DTD_ATTR_NAMESERVICE,
	    tabp->zc_name_service,
	    sizeof (tabp->zc_name_service))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_NFS4DOMAIN, tabp->zc_nfs4_domain,
	    sizeof (tabp->zc_nfs4_domain))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_SECPOLICY, tabp->zc_sec_policy,
	    sizeof (tabp->zc_sec_policy))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_SYSLOCALE, tabp->zc_sys_locale,
	    sizeof (tabp->zc_sys_locale))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_TERMINAL, tabp->zc_terminal,
	    sizeof (tabp->zc_terminal))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_TIMEZONE, tabp->zc_timezone,
	    sizeof (tabp->zc_timezone))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	handle->zc_dh_cur = cur->next;
	return (ZC_OK);
}

int
zccfg_endsysident(zc_dochandle_t handle)
{
	return (zccfg_endent(handle));
}

int
zccfg_setnodeent(zc_dochandle_t handle)
{
	return (zccfg_setent(handle));
}

static const xmlChar *
nm_to_dtd(const char *nm)
{
	if (strcmp(nm, "attr") == 0) {
		return (DTD_ELEM_ATTR);
	} else if (strcmp(nm, "dataset") == 0) {
		return (DTD_ELEM_DATASET);
	} else if (strcmp(nm, "device") == 0) {
		return (DTD_ELEM_DEVICE);
	} else if (strcmp(nm, "fs") == 0) {
		return (DTD_ELEM_FS);
	} else if (strcmp(nm, "inherit-pkg-dir") == 0) {
		return (DTD_ELEM_IPD);
	} else if (strcmp(nm, "net") == 0) {
		return (DTD_ELEM_NET);
	} else if (strcmp(nm, "node") == 0) {
		return (DTD_ELEM_NODE);
	} else if (strcmp(nm, "rctl") == 0) {
		return (DTD_ELEM_RCTL);
	} else if (strcmp(nm, "sysid") == 0) {
		return (DTD_ELEM_SYSID);
	} else {
		return (NULL);
	}
}

//
// Returns the number of instances of a given resource type. Note that this
// only works for "top level" resources, i.e., resources specified in the
// global scope. Resources specified in the node scope (e.g., a node-specific
// net resource) do not contribute to the count.
//
int
zccfg_num_resources(zc_dochandle_t handle, const char *rsrc)
{
	int num = 0;
	const xmlChar *dtd;
	xmlNodePtr cur;

	if ((dtd = nm_to_dtd(rsrc)) == NULL) {
		return (num);
	}

	if (zccfg_setent(handle) != ZC_OK) {
		return (num);
	}

	for (cur = handle->zc_dh_cur; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, dtd) == 0) {
			num++;
		}
	}

	(void) zccfg_endent(handle);
	return (num);
}

//
// Deletes all resources matching the one specified by the second argument.
// Walks the XML tree searching for a match. Whenever a matching node is
// found, that node is unlinked from the tree and freed.
//
int
zccfg_del_all_resources(zc_dochandle_t handle, const char *rsrc)
{
	int err;
	const xmlChar *dtd;
	xmlNodePtr cur;

	if ((dtd = nm_to_dtd(rsrc)) == NULL) {
		return (ZC_NO_RESOURCE_TYPE);
	}

	if ((err = zccfg_setent(handle)) != ZC_OK) {
		return (err);
	}

	cur = handle->zc_dh_cur;
	while (cur != NULL) {
		xmlNodePtr tmp;

		if (xmlStrcmp(dtd, cur->name)) {
			cur = cur->next;
			continue;
		}
		tmp = cur->next;
		xmlUnlinkNode(cur);
		xmlFreeNode(cur);
		cur = tmp;
	}

	(void) zccfg_endent(handle);
	return (ZC_OK);
}

//
// Helper function to check whether the string pointed to by srcp is a valid
// unsigned integer. Returns true if it is valid, and the corresponding number
// is returned via the nump argument.
//
static bool
valid_uint(char *srcp, uint64_t *nump)
{
	char *endp;

	// strtoull accepts '-'?! so we want to flag that as an error
	if (strchr(srcp, '-') != NULL) {
		return (false);
	}

	errno = 0;
	*nump = strtoull(srcp, &endp, 10);

	if (errno != 0 || *endp != '\0') {
		return (false);
	}
	return (true);
}

//
// Convert a string representing a number (possibly a fraction) into an integer.
// The string can have a modifier (K, M, G or T).   The modifiers are treated
// as powers of two (not 10). Returns 0 on success, -1 on error.
//
int
zccfg_str_to_bytes(char *str, uint64_t *bytes)
{
	long double val;
	char *unitp;
	uint64_t scale;

	if ((val = strtold(str, &unitp)) < 0) {
		return (-1);
	}

	// remove any leading white space from units string
	while (std::isspace(*unitp) != 0) { //lint !e1055
		unitp++;
	}

	// if no units explicitly set, error
	if (unitp == NULL || *unitp == '\0') {
		scale = 1;
	} else {
		int i;
		char *units[] = {"K", "M", "G", "T", NULL};

		scale = 1024;

		// update scale based on units
		for (i = 0; units[i] != NULL; i++) {
			if (strcasecmp(unitp, units[i]) == 0) {
				break;
			}
			scale <<= 10;
		}

		if (units[i] == NULL) {
			return (-1);
		}
	}

	*bytes = (uint64_t)(val * scale);
	return (0);
}

bool
zccfg_valid_ncpus(char *lowstr, char *highstr)
{
	uint64_t low, high;

	if (!valid_uint(lowstr, &low) || !valid_uint(highstr, &high) ||
	    low < 1 || low > high) {
		return (false);
	}

	return (true);
}

bool
zccfg_valid_importance(char *impstr)
{
	uint64_t num;

	if (!valid_uint(impstr, &num)) {
		return (false);
	}

	return (true);
}

bool
zccfg_valid_alias_limit(char *name, char *limitstr, uint64_t *limit)
{
	int i;

	for (i = 0; aliases[i].shortname != NULL; i++) {
		if (strcmp(name, aliases[i].shortname) == 0) {
			break;
		}
	}

	if (aliases[i].shortname == NULL) {
		return (false);
	}

	if (!valid_uint(limitstr, limit) || *limit < aliases[i].low_limit) {
		return (false);
	}

	return (true);
}

bool
zccfg_valid_memlimit(char *memstr, uint64_t *mem_val)
{
	if (zccfg_str_to_bytes(memstr, mem_val) != 0) {
		return (false);
	}
	return (true);
}


int
zccfg_add_net_elem(struct zc_nodetab *nodep,
    struct zc_nwiftab *nwifp)
{
	zc_nwifelem_t *oldp, *lastp, *newp;

	lastp = nodep->zc_nwif_listp;

	// Go to the end of the list
	for (oldp = lastp; oldp != NULL; oldp = oldp->next) {
		lastp = oldp;
	}

	newp = (zc_nwifelem_t *)malloc(sizeof (zc_nwifelem_t));
	if (newp == NULL) {
		return (ZC_NOMEM);
	}
	(void) strlcpy(newp->elem.zc_nwif_address,
	    nwifp->zc_nwif_address,
	    sizeof (newp->elem.zc_nwif_address));
	(void) strlcpy(newp->elem.zc_nwif_physical,
	    nwifp->zc_nwif_physical,
	    sizeof (newp->elem.zc_nwif_physical));
	newp->elem.zc_nwif_defrouter[0] = '\0';
	(void) strlcpy(newp->elem.zc_nwif_defrouter,
	    nwifp->zc_nwif_defrouter,
	    sizeof (newp->elem.zc_nwif_defrouter));
	newp->next = NULL;

	if (lastp == NULL) {
		nodep->zc_nwif_listp = newp;
	} else {
		lastp->next = newp;
	}

	return (ZC_OK);
}

int
zccfg_get_local_net(xmlNodePtr cur, struct zc_nodetab *tabp)
{
	int err;
	struct zc_nwiftab nwiftab;
	tabp->zc_nwif_listp = NULL;

	if (cur == NULL) {
		return (ZC_NO_ENTRY);
	}

	// TODO: Handle exclusive IP zone
	for (; cur != NULL; cur = cur->next) {
		if (!xmlStrcmp(cur->name, DTD_ELEM_NET)) {
			(void) fetchprop(cur, DTD_ATTR_ADDRESS,
			    nwiftab.zc_nwif_address,
			    sizeof (nwiftab.zc_nwif_address));
			(void) fetchprop(cur, DTD_ATTR_PHYSICAL,
			    nwiftab.zc_nwif_physical,
			    sizeof (nwiftab.zc_nwif_physical));
			(void) fetchprop(cur, DTD_ATTR_DEFROUTER,
			    nwiftab.zc_nwif_defrouter,
			    sizeof (nwiftab.zc_nwif_defrouter));
			err = zccfg_add_net_elem(tabp, &nwiftab);
			if (err != ZC_OK) {
				return (err);
			}
		}
	}

	return (ZC_OK);
}

int
zccfg_getnodeent(zc_dochandle_t handle, struct zc_nodetab *tabp)
{
	xmlNodePtr cur;
	int err;

	if (handle == NULL) {
		return (ZC_INVAL);
	}
	if ((cur = handle->zc_dh_cur) == NULL) {
		return (ZC_NO_ENTRY);
	}

	for (; cur != NULL; cur = cur->next) {
		if (!xmlStrcmp(cur->name, DTD_ELEM_NODE)) {
			break;
		}
	}
	if (cur == NULL) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (ZC_NO_ENTRY);
	}

	if ((err = fetchprop(cur, DTD_ATTR_PHYSICALHOST, tabp->zc_nodename,
	    sizeof (tabp->zc_nodename))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}
	if ((err = fetchprop(cur, DTD_ATTR_HOSTNAME, tabp->zc_hostname,
	    sizeof (tabp->zc_hostname))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	if ((err = zccfg_get_local_net(cur->children, tabp)) != ZC_OK) {
		return (err);
	}

	// TODO get other node scope resources here
	// Set the list pointers to NULL for now
	tabp->zc_fs_listp = NULL;
	tabp->zc_dev_listp = NULL;
	tabp->zc_ds_listp = NULL;
	tabp->zc_rctl_listp = NULL;

	handle->zc_dh_cur = cur->next;
	return (ZC_OK);
}

int
zccfg_endnodeent(zc_dochandle_t handle)
{
	return (zccfg_endent(handle));
}

//
// rctl convenience functions. Adapted from the corresponding functions in
// libzonecfg.c
//

int
zccfg_construct_rctlblk(const struct zc_rctlvaltab *rctlvalp,
    rctlblk_t *rctlblkp)
{
	unsigned long long ull;
	char *endp;
	rctl_priv_t priv;
	rctl_qty_t limit;
	uint_t action;

	// Get the privilege
	if (strcmp(rctlvalp->zc_rctlval_priv, "basic") == 0) {
		priv = RCPRIV_BASIC;
	} else if (strcmp(rctlvalp->zc_rctlval_priv, "privileged") == 0) {
		priv = RCPRIV_PRIVILEGED;
	} else {
		// Ivalid privilege
		return (ZC_INVAL);
	}

	// Deal with negative input; strtoull(3c) doesn't do what we want
	if (rctlvalp->zc_rctlval_limit[0] == '-') {
		return (ZC_INVAL);
	}

	// Get the limit
	errno = 0;
	ull = strtoull(rctlvalp->zc_rctlval_limit, &endp, 0);
	if (errno != 0 || *endp != '\0') {
		// Parse failed
		return (ZC_INVAL);
	}
	limit = (rctl_qty_t)ull;

	// Get the action
	if (strcmp(rctlvalp->zc_rctlval_action, "none") == 0) {
		action = RCTL_LOCAL_NOACTION;
	} else if (strcmp(rctlvalp->zc_rctlval_action, "signal") == 0) {
		action = RCTL_LOCAL_SIGNAL;
	} else if (strcmp(rctlvalp->zc_rctlval_action, "deny") == 0) {
		action = RCTL_LOCAL_DENY;
	} else {
		// Invalid action
		return (ZC_INVAL);
	}
	rctlblk_set_local_action(rctlblkp, action, 0);
	rctlblk_set_privilege(rctlblkp, priv);
	rctlblk_set_value(rctlblkp, limit);
	return (ZC_OK);
}

//
// Check to see if the given property name exists in the DTD.
// Return value:
// true if the given attribute is defined in the DTD.
// false otherwise.
//
bool
zccfg_attr_exists_in_zones_dtd(const char *attr_namep)
{
	char		buf[BUFFERSIZE];
	FILE		*fdp = fopen(DTD_PATH, "r");

	ASSERT(fdp != NULL);
	ASSERT(strlen(attr_namep) > 0);

	if ((fdp == NULL) || (strlen(attr_namep) == 0)) {
		return (false);
	}
	while (fgets(buf, BUFFERSIZE, fdp) != NULL) {
		// Make sure buffer isn't too small.
		ASSERT(!((strlen(buf) == BUFFERSIZE - 1) &&
		    (buf[BUFFERSIZE - 2] != '\n')));
		if (strstr(buf, attr_namep) != NULL) {
			// Match.
			fclose(fdp);
			return (true);
		}
	}
	fclose(fdp);
	return (false);
}

static int
rctl_check(const char *rctlnamep, void *argp)
{
	const char *attrname = (const char *)argp;

	//
	// Returning 1 here is our signal to zccfg_is_rctl() that it is
	// indeed an rctl name recognized by the system.
	//
	return (strcmp(rctlnamep, attrname) == 0 ? 1 : 0);
}

//
// Returns true if namep is a valid rctl name.
//
bool
zccfg_is_rctl(const char *namep)
{
	return (rctl_walk(rctl_check, (void *)namep) == 1);
}

//
// Check if namep is a valid zone rctl name. All zone level rctl names begin
// with the "zone." prefix. Valid names contain alphanumeric characters, with
// the only non-alphabetic character allowed being '-'.
//
bool
zccfg_valid_rctlname(const char *namep)
{
	const char *temp;

	if (strncmp(namep, "zone.", sizeof ("zone.") - 1) != 0) {
		return (false);
	}
	if (strlen(namep) == sizeof ("zone.") - 1) {
		return (false);
	}
	for (temp = namep + sizeof ("zone.") - 1; *temp != '\0'; temp++) {
		if (!std::isalpha(*temp) && *temp != '-') { //lint !e1055
			return (false);
		}
	}
	return (true);
}

//
// Similar to the previous function, but this time for rctlblk's.
//
bool
zccfg_valid_rctlblk(const rctlblk_t *rctlblkp)
{
	rctl_priv_t priv = rctlblk_get_privilege((rctlblk_t *)rctlblkp);
	uint_t action = rctlblk_get_local_action((rctlblk_t *)rctlblkp, NULL);

	if (priv != RCPRIV_PRIVILEGED) {
		return (false);
	}
	if (action != RCTL_LOCAL_NOACTION && action != RCTL_LOCAL_DENY) {
		return (false);
	}
	return (true);
}

//
// Validate the given rctl name with the specified rctlblk value.
//
bool
zccfg_valid_rctl(const char *namep, const rctlblk_t *rctlblkp)
{
	rctlblk_t *curp;
	rctlblk_t *next;
	rctl_qty_t limit = rctlblk_get_value((rctlblk_t *)rctlblkp);
	uint_t action = rctlblk_get_local_action((rctlblk_t *)rctlblkp, NULL);
	uint_t global_flags;

	if (!zccfg_valid_rctlblk(rctlblkp)) {
		return (false);
	}
	if (!zccfg_valid_rctlname(namep)) {
		return (false);
	}

	curp = (rctlblk_t *)alloca(rctlblk_size());
	assert(curp != NULL);
	if (getrctl(namep, NULL, curp, RCTL_FIRST) != 0) {
		// not an rctl on this system
		return (true);
	}

	//
	// Make sure the proposed value isn't greater than the current system
	// value.
	//
	next = (rctlblk_t *)alloca(rctlblk_size());
	assert(next != NULL);
	while (rctlblk_get_privilege(curp) != RCPRIV_SYSTEM) {
		rctlblk_t *tmp;

		if (getrctl(namep, curp, next, RCTL_NEXT) != 0) {
			// Shouldn't happen
			return (false);
		}
		tmp = curp;
		curp = next;
		next = tmp;
	}
	if (limit > rctlblk_get_value(curp)) {
		return (false);
	}

	//
	// Make sure the proposed action is allowed.
	//
	global_flags = rctlblk_get_global_flags(curp);
	if ((global_flags & RCTL_GLOBAL_DENY_NEVER) &&
	    action == RCTL_LOCAL_DENY) {
		return (false);
	}
	if ((global_flags & RCTL_GLOBAL_DENY_ALWAYS) &&
	    action == RCTL_LOCAL_NOACTION) {
		return (false);
	}

	return (true);
}

//
// Helper function for zccfg_lookup_rctl below. This function retrieves
// the rctl values and puts them into the struct pointed to by tabp.
//
static int
lookup_rctlval(xmlNodePtr cur, struct zc_rctltab *tabp)
{
	xmlNodePtr val;
	struct zc_rctlvaltab *valp;

	for (val = cur->xmlChildrenNode; val != NULL; val = val->next) {
		valp = (struct zc_rctlvaltab *)malloc(
		    sizeof (struct zc_rctlvaltab));
		if (valp == NULL) {
			return (ZC_NOMEM);
		}

		if ((fetchprop(val, DTD_ATTR_PRIV, valp->zc_rctlval_priv,
		    sizeof (valp->zc_rctlval_priv)) != ZC_OK)) {
			return (ZC_NO_RESOURCE_ID);
		}
		if ((fetchprop(val, DTD_ATTR_ACTION,
		    valp->zc_rctlval_action,
		    sizeof (valp->zc_rctlval_action)) != ZC_OK)) {
			return (ZC_NO_RESOURCE_ID);
		}
		if ((fetchprop(val, DTD_ATTR_LIMIT, valp->zc_rctlval_limit,
		    sizeof (valp->zc_rctlval_limit)) != ZC_OK)) {
			return (ZC_NO_RESOURCE_ID);
		}
		if (zccfg_add_rctl_value(tabp, valp) != ZC_OK) {
			return (ZC_NO_RESOURCE_ID);
		}
	}
	return (ZC_OK);
}

//
// Looks up the specified rctl resource from the configuration. The caller
// must have specified the name of the rctl resource in question for this
// to succeed. The struct pointed to by tabp is filled in with the retrieved
// data. ZC_NO_RESOURCE_ID is returned if no matching rctl resource instance
// could be found.
//
// This is an adaptation of the corresponding function in Solaris libzonecfg.c.
// We have simlified the structure of the function by using the lookup_rctl
// helper function.
//
int
zccfg_lookup_rctl(zc_dochandle_t handle, struct zc_rctltab *tabp)
{
	xmlNodePtr cur;
	char savedname[MAXNAMELEN];
	int err;

	if (tabp == NULL || strlen(tabp->zc_rctl_name) == 0) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	cur = handle->zc_dh_cur;
	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_RCTL)) {
			continue;
		}
		if ((fetchprop(cur, DTD_ATTR_NAME, savedname,
		    sizeof (savedname)) == ZC_OK) &&
		    (strcmp(savedname, tabp->zc_rctl_name) == 0)) {
			tabp->zc_rctl_valp = NULL;

			err = lookup_rctlval(cur, tabp);
			if (err != ZC_OK) {
				return (err);
			}
		}
	}
	return (ZC_NO_RESOURCE_ID);
}

static int
zccfg_add_rctl_core(zc_dochandle_t handle, struct zc_rctltab *tabp)
{
	xmlNodePtr newnode;
	xmlNodePtr cur = handle->zc_dh_cur;
	xmlNodePtr valnode;
	struct zc_rctlvaltab *valp;
	int err;

	newnode = xmlNewTextChild(cur, NULL, DTD_ELEM_RCTL, NULL);
	err = newprop(newnode, DTD_ATTR_NAME, tabp->zc_rctl_name);
	if (err != ZC_OK) {
		return (err);
	}

	for (valp = tabp->zc_rctl_valp; valp != NULL;
	    valp = valp->zc_rctlval_next) {
		valnode = xmlNewTextChild(newnode, NULL, DTD_ELEM_RCTLVALUE,
		    NULL);
		err = newprop(valnode, DTD_ATTR_PRIV,
		    valp->zc_rctlval_priv);
		if (err != ZC_OK) {
			return (err);
		}
		err = newprop(valnode, DTD_ATTR_LIMIT,
		    valp->zc_rctlval_limit);
		if (err != ZC_OK) {
			return (err);
		}
		err = newprop(valnode, DTD_ATTR_ACTION,
		    valp->zc_rctlval_action);
		if (err != ZC_OK) {
			return (err);
		}
	}
	return (ZC_OK);
}

int
zccfg_add_rctl(zc_dochandle_t handle, struct zc_rctltab *tabp)
{
	int err;

	if (tabp == NULL || strlen(tabp->zc_rctl_name) == 0) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	return (zccfg_add_rctl_core(handle, tabp));
}

static int
zccfg_delete_rctl_core(zc_dochandle_t handle,
    struct zc_rctltab *tabp)
{
	xmlNodePtr cur = handle->zc_dh_cur;
	xmlChar *savedname;
	int name_result;

	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_RCTL)) {
			continue;
		}

		savedname = xmlGetProp(cur, DTD_ATTR_NAME);
		if (savedname == NULL) {  // shouldn't happen
			continue;
		}
		name_result = xmlStrcmp(savedname,
		    (const xmlChar *) tabp->zc_rctl_name);
		xmlFree(savedname);
		if (name_result == 0) {
			xmlUnlinkNode(cur);
			xmlFreeNode(cur);
			return (ZC_OK);
		}
	}
	return (ZC_NO_RESOURCE_ID);
}

int
zccfg_delete_rctl(zc_dochandle_t handle, struct zc_rctltab *tabp)
{
	int err;

	if (tabp == NULL || strlen(tabp->zc_rctl_name) == 0) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	return (zccfg_delete_rctl_core(handle, tabp));
}

//
// Modify an rctl resource; it just deletes the old value from handle and
// then adds the new one.
//
int
zccfg_modify_rctl(zc_dochandle_t handle,
    struct zc_rctltab *oldtabp, struct zc_rctltab *newtabp)
{
	int err;

	if (oldtabp == NULL || strlen(oldtabp->zc_rctl_name) == 0 ||
	    newtabp == NULL || strlen(newtabp->zc_rctl_name) == 0) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_delete_rctl_core(handle, oldtabp)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_add_rctl_core(handle, newtabp)) != ZC_OK) {
		return (err);
	}
	return (ZC_OK);
}

//
// Add specified rctl value to the rctl resource pointed to by tabp. Basically
// appends the rctlval struct to the rctlval list.
//
int
zccfg_add_rctl_value(struct zc_rctltab *tabp,
    struct zc_rctlvaltab *valtabp)
{
	struct zc_rctlvaltab *lastp;
	struct zc_rctlvaltab *oldp;
	struct zc_rctlvaltab *newp;
	rctlblk_t *rctlblkp = (rctlblk_t *)alloca(rctlblk_size());

	lastp = tabp->zc_rctl_valp;

	// Walk to the end of the list
	for (oldp = lastp; oldp != NULL; oldp = oldp->zc_rctlval_next) {
		lastp = oldp;
	}

	newp = valtabp; // allocated by the caller
	if (newp == NULL) {
		return (ZC_INVAL);
	}
	newp->zc_rctlval_next = NULL;
	if (zccfg_construct_rctlblk(valtabp, rctlblkp) != ZC_OK) {
		return (ZC_INVAL);
	}
	if (!zccfg_valid_rctlblk(rctlblkp)) {
		return (ZC_INVAL);
	}

	//
	// If this is the first value, create a new node; else add it at
	// the end.
	//
	if (lastp == NULL) {
		tabp->zc_rctl_valp = newp;
	} else {
		lastp->zc_rctlval_next = newp;
	}

	return (ZC_OK);
}

//
// Remove the specified rctl value from the given rctl resource. If we find the
// specified rctl value in the list, we delete it and return ZC_OK; else we
// return ZC_NO_PROPERTY_ID.
//
int
zccfg_remove_rctl_value(struct zc_rctltab *tabp,
    struct zc_rctlvaltab *valtabp)
{
	struct zc_rctlvaltab *lastp;
	struct zc_rctlvaltab *curp;
	struct zc_rctlvaltab *nextp;

	lastp = tabp->zc_rctl_valp;
	// Walk the list to find a matching entry.
	for (curp = lastp; curp != NULL; curp = curp->zc_rctlval_next) {
		if (strcmp(curp->zc_rctlval_priv,
		    valtabp->zc_rctlval_priv) == 0 &&
		    strcmp(curp->zc_rctlval_limit,
		    valtabp->zc_rctlval_limit) == 0 &&
		    strcmp(curp->zc_rctlval_action,
		    valtabp->zc_rctlval_action) == 0) {
			nextp = curp->zc_rctlval_next;

			if (curp == tabp->zc_rctl_valp) {
				tabp->zc_rctl_valp = nextp;
			} else {
				lastp->zc_rctlval_next = nextp;
			}
			free(curp);
		} else {
			lastp = curp;
		}
	}
	return (ZC_NO_PROPERTY_ID);
}

static int
delete_tmp_pool(zc_dochandle_t handle)
{
	int err;
	xmlNodePtr cur = handle->zc_dh_cur;

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_TMPPOOL) == 0) {
			xmlUnlinkNode(cur);
			xmlFreeNode(cur);
			return (ZC_OK);
		}
	}
	return (ZC_NO_RESOURCE_ID);
}

static int
modify_tmp_pool(zc_dochandle_t handle, char *pool_importance)
{
	int err;
	xmlNodePtr newnode;
	xmlNodePtr cur = handle->zc_dh_cur;

	err = delete_tmp_pool(handle);
	if (err != ZC_OK && err != ZC_NO_RESOURCE_ID) {
		return (err);
	}

	if (*pool_importance != '\0') {
		if ((err = operation_prep(handle)) != ZC_OK) {
			return (err);
		}

		newnode = xmlNewTextChild(cur, NULL, DTD_ELEM_TMPPOOL, NULL);
		if ((err = newprop(newnode, DTD_ATTR_IMPORTANCE,
		    pool_importance)) != ZC_OK) {
			return (err);
		}
	}
	return (ZC_OK);
}

static int
zccfg_add_pset_core(zc_dochandle_t handle, struct zc_psettab *tabp)
{
	xmlNodePtr newnode;
	xmlNodePtr cur = handle->zc_dh_cur;
	int err;

	newnode = xmlNewTextChild(cur, NULL, DTD_ELEM_PSET, NULL);
	if ((err = newprop(newnode, DTD_ATTR_NCPU_MIN,
	    tabp->zc_ncpu_min)) != ZC_OK) {
		return (err);
	}
	if ((err = newprop(newnode, DTD_ATTR_NCPU_MAX,
	    tabp->zc_ncpu_max)) != ZC_OK) {
		return (err);
	}

	if ((err = modify_tmp_pool(handle, tabp->zc_importance)) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

int
zccfg_add_pset(zc_dochandle_t handle, struct zc_psettab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	return (zccfg_add_pset_core(handle, tabp));
}

int
zccfg_delete_pset(zc_dochandle_t handle)
{
	int err;
	int res = ZC_NO_RESOURCE_ID;
	xmlNodePtr cur = handle->zc_dh_cur;

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_PSET) == 0) {
			xmlUnlinkNode(cur);
			xmlFreeNode(cur);
			res = ZC_OK;
			break;
		}
	}

	//
	// Once we have msets, we should check that a mset
	// does not exist before we delete the tmp_pool data.
	//
	err = delete_tmp_pool(handle);
	if (err != ZC_OK && err != ZC_NO_RESOURCE_ID) {
		return (err);
	}

	return (res);
}

int
zccfg_modify_pset(zc_dochandle_t handle, struct zc_psettab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = zccfg_delete_pset(handle)) != ZC_OK) {
		return (err);
	}

	if ((err = zccfg_add_pset_core(handle, tabp)) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

int
zccfg_lookup_pset(zc_dochandle_t handle, struct zc_psettab *tabp)
{
	xmlNodePtr cur;
	int err;
	int res = ZC_NO_ENTRY;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	// this is an optional component
	tabp->zc_importance[0] = '\0';

	cur = handle->zc_dh_cur;
	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_PSET) == 0) {
			if ((err = fetchprop(cur, DTD_ATTR_NCPU_MIN,
			    tabp->zc_ncpu_min,
			    sizeof (tabp->zc_ncpu_min))) != ZC_OK) {
				handle->zc_dh_cur = handle->zc_dh_top;
				return (err);
			}

			if ((err = fetchprop(cur, DTD_ATTR_NCPU_MAX,
			    tabp->zc_ncpu_max,
			    sizeof (tabp->zc_ncpu_max))) != ZC_OK) {
				handle->zc_dh_cur = handle->zc_dh_top;
				return (err);
			}

			res = ZC_OK;
		} else if (xmlStrcmp(cur->name, DTD_ELEM_TMPPOOL) == 0) {
			if ((err = fetchprop(cur, DTD_ATTR_IMPORTANCE,
			    tabp->zc_importance,
			    sizeof (tabp->zc_importance))) != ZC_OK) {
				handle->zc_dh_cur = handle->zc_dh_top;
				return (err);
			}
		}
	}

	return (res);
}

int
zccfg_getpsetent(zc_dochandle_t handle, struct zc_psettab *tabp)
{
	int err;

	if ((err = zccfg_setent(handle)) != ZC_OK) {
		return (err);
	}

	err = zccfg_lookup_pset(handle, tabp);

	(void) zccfg_endent(handle);
	return (err);
}

static int
add_mcap(zc_dochandle_t handle, struct zc_mcaptab *tabp)
{
	xmlNodePtr newnode;
	xmlNodePtr cur = handle->zc_dh_cur;
	int err;

	newnode = xmlNewTextChild(cur, NULL, DTD_ELEM_MCAP, NULL);
	if ((err = newprop(newnode, DTD_ATTR_PHYSCAP,
	    tabp->zc_physmem_cap)) != ZC_OK) {
		return (err);
	}

	return (ZC_OK);
}

int
zccfg_delete_mcap(zc_dochandle_t handle)
{
	int err;
	xmlNodePtr cur = handle->zc_dh_cur;

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_MCAP) != 0) {
			continue;
		}
		xmlUnlinkNode(cur);
		xmlFreeNode(cur);
		return (ZC_OK);
	}

	return (ZC_NO_RESOURCE_ID);
}

int
zccfg_modify_mcap(zc_dochandle_t handle, struct zc_mcaptab *tabp)
{
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	err = zccfg_delete_mcap(handle);
	// it is ok if there is no mcap entry
	if (err != ZC_OK && err != ZC_NO_RESOURCE_ID) {
		return (err);
	}

	if ((err = add_mcap(handle, tabp)) != ZC_OK) {
		return (err);
	}
	return (ZC_OK);
}

int
zccfg_lookup_mcap(zc_dochandle_t handle, struct zc_mcaptab *tabp)
{
	xmlNodePtr cur;
	int err;

	if (tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((err = operation_prep(handle)) != ZC_OK) {
		return (err);
	}

	cur = handle->zc_dh_cur;
	for (cur = cur->xmlChildrenNode; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_MCAP) != 0) {
			continue;
		}

		if ((err = fetchprop(cur, DTD_ATTR_PHYSCAP,
		    tabp->zc_physmem_cap,
		    sizeof (tabp->zc_physmem_cap))) != ZC_OK) {
			handle->zc_dh_cur = handle->zc_dh_top;
			return (err);
		}
		return (ZC_OK);
	}
	return (ZC_NO_ENTRY);
}

static int
getmcapent_core(zc_dochandle_t handle, struct zc_mcaptab *tabp)
{
	xmlNodePtr cur;
	int err;

	if (handle == NULL || tabp == NULL) {
		return (ZC_INVAL);
	}

	if ((cur = handle->zc_dh_cur) == NULL) {
		return (ZC_NO_ENTRY);
	}

	for (; cur != NULL; cur = cur->next) {
		if (xmlStrcmp(cur->name, DTD_ELEM_MCAP) == 0) {
			break;
		}
	}

	if (cur == NULL) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (ZC_NO_ENTRY);
	}

	if ((err = fetchprop(cur, DTD_ATTR_PHYSCAP, tabp->zc_physmem_cap,
	    sizeof (tabp->zc_physmem_cap))) != ZC_OK) {
		handle->zc_dh_cur = handle->zc_dh_top;
		return (err);
	}

	handle->zc_dh_cur = cur->next;
	return (ZC_OK);
}

int
zccfg_getmcapent(zc_dochandle_t handle, struct zc_mcaptab *tabp)
{
	int err;

	if ((err = zccfg_setent(handle)) != ZC_OK) {
		return (err);
	}

	err = getmcapent_core(handle, tabp);
	(void) zccfg_endent(handle);
	return (err);
}
