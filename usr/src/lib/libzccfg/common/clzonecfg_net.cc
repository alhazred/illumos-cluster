//
// CDDL HEADER START
//
// The contents of this file are subject to the terms of the
// Common Development and Distribution License (the License).
// You may not use this file except in compliance with the License.
//
// You can obtain a copy of the license at usr/src/CDDL.txt
// or http:// www.opensolaris.org/os/licensing.
// See the License for the specific language governing permissions
// and limitations under the License.
//
// When distributing Covered Code, include this CDDL HEADER in each
// file and include the License file at usr/src/CDDL.txt.
// If applicable, add the following below this CDDL HEADER, with the
// fields enclosed by brackets [] replaced with your own identifying
// information: Portions Copyright [yyyy] [name of copyright owner]
//
// CDDL HEADER END
//

//
// Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
// Use is subject to license terms.
//

#pragma ident	"@(#)clzonecfg_net.cc	1.15	08/10/14 SMI"

#include <sys/os.h>
#include <sys/clconf_int.h>
#include <sys/rsrc_tag.h>
#include <clconf/clnode.h>
#include <clconf/clconf_ccr.h>
#include <clconf/clconf_file_io.h>
#include <cmm/cmm_ns.h>
#include <tm/user_tm.h>
#include <sys/rsrc_tag.h>
#include <h/component_state.h>	/* Query interface */
#include <h/repl_pxfs.h>
#include <sys/cl_assert.h>
#include <vm/versioned_protocol.h>
#include <sys/vm_util.h>
#include <sys/quorum_int.h>
#include <sys/cl_comm_support.h>

#include <libzccfg/libzccfg.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>


#ifdef _KERNEL
#include <netinet/in.h>
#include <orb/ip/ipconf.h>
#include <sys/cladm_debug.h>
#endif

#ifdef _KERNEL_ORB
#include <orb/transport/path_manager.h>
#else
#include <orb/infrastructure/orb.h>
#endif

typedef struct cz_elem {
	char *cz_name;
	char *net_number;
} cz_elem_t;

#define	MAX_NAME_LEN	255
#define	MAX_ERRBUF	256
#define	CCR_INFRA_TABLE "infrastructure"

static os::sc_syslog_msg logger(SC_SYSLOG_ZC_LIB_TAG, "", NULL);


/*
 * process_clprivnet: This will be called during the clusterized_zone
 * creation if the clprivnet property is enabled. This function
 * calculates a new subnet to be used for the clusterized zone
 * and writes this to the CCR files
 */

int
get_free_subnet(uint_t max_entries,
	uint_t max_number,
	uint_t *entry_number,
	uint_t *subnet_array) {

	uint_t  bitmask;
	uint_t  i, j, entry;
	int found = 0;

	bitmask = 1;

	for (i = 0, entry = 0; !found && i < max_entries; i++) {
		for (j = 0; j < UINT_SIZE; j++) {
			if (!(subnet_array[i] & (bitmask << j))) {
				found = 1;
				break;
			}
			entry++;
		}
	}

	if (found && entry_number && entry < max_number) {
		*entry_number = entry;
		return (0);
	} else {
		return (1);
	}
}

int
set_subnet_in_use(uint_t entry_number, uint_t max_number, uint_t *bitarray) {
	uint_t  bitmask, words, bits;

	if (entry_number > max_number)
		return (0);

	bitmask = 1;
	bits = entry_number % UINT_SIZE;
	words = entry_number / UINT_SIZE;

	if (bitarray[words] & (bitmask << bits))
		return (0);

	bitarray[words] |= (bitmask << bits);
	return (1);
}

int
read_ccr(cz_elem_t **elem, uint_t *num_elem) {
	int new_version = -1, retry = 1;
	Environment e;
	ccr::readonly_table_var tabptr;
	ccr::element_seq_var elems;
	int num_elems = 0;
	uint_t i;
	in_addr_t ipaddr;
	char *saddr, *snode;
	char *hostname, *zname;
	nodeid_t node;
	char *dval, *lasts = NULL;
	CORBA::Exception *ex;
	cz_elem_t *cz_elem = NULL;

#ifndef _KERNEL
	// Check if ORB is initialized
	if (!(ORB::is_initialized())) {
		return (-1);
	}
#endif

	ccr::directory_var ccr_ptr = ccr::directory::_nil();

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		e.clear();
		return (-1);
	}
	else
		ccr_ptr = ccr::directory::_narrow(obj);

	if (CORBA::is_nil(ccr_ptr))  {
		return (-1);
	}

	while (retry) {

		/* The table is created by the daemon if not already present */
		tabptr = ccr_ptr->lookup(CZNET_CCR_TABLE, e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::no_such_table::_exnarrow(ex)) {
				// The table does not exist. So need
				// to update any mappings.
				e.clear();
				*num_elem = num_elems;
				*elem = NULL;
				return (0);
			} else {
				return (-1);
			}
		}

		new_version = tabptr->get_gennum(e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::table_modified::_exnarrow(ex)) {
				// The table was modified since we
				// determined the number of elements
				// Restart the process.
				e.clear();
				continue;
			} else {
				e.clear();
				return (-1);
			}
		}

		/*
		 * Read the CCR table
		 */
		num_elems = tabptr->get_num_elements(e);
		if ((ex = e.exception()) != NULL) {
			if (ccr::table_modified::_exnarrow(ex)) {
			// The table was modified since we
			// determined the number of elements
			// Restart the process.
				e.clear();
				continue;
			} else {
				return (-1);
			}
		}

		if (num_elems > 0) {
			tabptr->atfirst(e);
			if ((ex = e.exception()) != NULL) {
				if (ccr::table_modified::_exnarrow(ex)) {
					// The table was modified since we
					// determined the number of elements
					// Restart the process.
					e.clear();
					continue;
				} else {
					return (-1);
				}
			}

			tabptr->next_n_elements((uint32_t)num_elems, elems, e);
			if ((ex = e.exception()) != NULL) {
				if (ccr::table_modified:: _exnarrow(ex)) {
					// The table was modified since we
					// determined the number of elements
					// and version of the table.
					// Restart the process.
					e.clear();
					continue;
				} else {
					return (-1);
				}
			}
			cz_elem = (cz_elem_t *)malloc(num_elems *
			    sizeof (cz_elem_t));
			for (i = 0; i < elems->length(); i++) {
				cz_elem[i].cz_name = strdup(elems[i].key);
				cz_elem[i].net_number = strdup(elems[i].data);
			}
		}
		retry = 0;
	}
	*elem = cz_elem;
	*num_elem = num_elems;
	return (0);
}

int
is_subnet_in_use(const char *subnet) {

	cz_elem_t *cz_elem;
	uint_t num_elem;
	int ret = 0;
	int i = 0;

	/*
	 * The vc_privip is created by scprivip deamon. Just read
	 * the ccr mappings and see whether this subnet is already
	 * available.
	 */
	ret = read_ccr(&cz_elem, &num_elem);
	for (i = 0; i < num_elem; i++) {
		if (strcmp(subnet, cz_elem[i].net_number) == 0) {
			/* The subnet is already in use */
			return (1);
		}
	}
	return (0);
}


int
get_new_subnet(char **cz_subnet) {

	uint_t total_subnets, total_addrs, subnet_addrs;
	uint_t num_hosts, subnet_bits, total_subnet_entries;
	uint_t physical_addrs, physical_subnets;
	int i = 0, found = 0, ret;
	struct in_addr in;
	const struct in_addr *mynet = NULL;
	const char *c_netmask = NULL, *priv_snet_mask, *ip_addr;
	in_addr_t cluster_netmask, private_subnet_netmask, subnet_num, net_num;
	uint_t maxprivnets, hostbits, free_entry;
	uint_t *subnet_entries;

	subnet_num = (in_addr_t)0;

	/* Initialize ORB */
	if (clconf_lib_init() != 0) {
		return (ZC_CCR_ERROR);
	}

	clconf_cluster_t *cl = clconf_cluster_get_current();

	/* Get the clusternetmask */
	c_netmask =  clconf_obj_get_property((clconf_obj_t *)cl,
	    "cluster_netmask");

	/*
	 * If the cluster_netmask is not available, that means
	 * this must be an upgraded cluster and the entry is not available.
	 * User should run the cluster command to specify the number of zone
	 * clusters and that will recalculate the netmask and add it
	 * to the infrastructure file.
	 */
	if (c_netmask == NULL) {
		//
		// SCMSGS
		// @explanation
		// clzonecluster was not able to create the specified
		// zone cluster due to insufficient private network addresses."
		// @user_action
		// Run the cluster command to specify the number of zone
		// clusters that you expect to create in the base cluster.
		// Then rerun the clzonecluster command to create the zone
		// cluster
		//
		(void) logger.log(LOG_ERR, MESSAGE, "Could not create zone "
		    "cluster due to lack of network addresses. Run the "
		    "cluster(1CL) set-netprops command to specify the "
		    "number of zone clusters and rerun the command.\n");
		return (ZC_CCR_ERROR);
	}
	cluster_netmask = inet_addr(c_netmask);
	/* host order used henceforth */
	cluster_netmask = ntohl(cluster_netmask);

	/* Get the subnet_netmask */
	priv_snet_mask = clconf_obj_get_property((clconf_obj_t *)cl,
	    "private_subnet_netmask");
	private_subnet_netmask = inet_addr(priv_snet_mask);
	/* host order used henceforth */
	private_subnet_netmask = ntohl(private_subnet_netmask);

	/*
	 * Using the cluster_netmask and subnet_netmask, calculate
	 * number of total_subnets needed
	 */
	total_addrs = (~cluster_netmask + 1);
	subnet_addrs = (~private_subnet_netmask + 1);
	total_subnets = total_addrs / subnet_addrs;

	/* Array Size */
	total_subnet_entries = (total_subnets / UINT_SIZE) + 1;

	/*
	 * Create the array subnet_entries which keeps tracks the usage
	 * of these subnets.
	 */
	subnet_entries = new uint_t[total_subnet_entries];
	bzero(subnet_entries, sizeof (subnet_entries));

	/* Initialize the subnet_entries array */
	for (i = 0; i < total_subnet_entries; i++) {
		subnet_entries[i] = 0;
	}

	/*
	 * Reserve the subnets used for physical cluster.
	 */
	maxprivnets = atoi(clconf_obj_get_property((clconf_obj_t *)cl,
	    "private_maxprivnets"));

	/* Calculate the subnets needed for physical cluster */
	physical_addrs = (4 * (maxprivnets) / 3) *
	    (~private_subnet_netmask + 1);
	physical_subnets = physical_addrs / subnet_addrs;

	if (physical_subnets > total_subnets)
		/* ?? Return internal error */
		return (-1);

	/* Reserve the subnets */
	for (i = 0; i < physical_subnets; i++) {
		set_subnet_in_use(i, total_subnets, subnet_entries);
	}

	/* Get the next available subnet */
	do {
		/* Get the free subnet entry */
		ret = get_free_subnet(total_subnet_entries,
		    total_subnets, &free_entry, subnet_entries);
		if (ret == 1) {
			/* No free subnets */
			break;
		}

		/* Get the Base IP number (network number) */
		ip_addr = clconf_obj_get_property((clconf_obj_t *)cl,
		    "private_net_number");
		net_num = inet_addr(ip_addr);
		/* host order used henceforth */
		net_num = ntohl(net_num);

		/*
		 * Get the hostbits: This defines the number
		 * of nodes
		 */
		num_hosts = subnet_addrs;
		hostbits = 0;
		do {
			num_hosts = num_hosts >> 1;
			hostbits++;
		} while (num_hosts > 0);
		hostbits = hostbits - 1;

		/* Now, get the subnet number (host order) */
		subnet_num = (net_num & cluster_netmask) |
		    (free_entry << hostbits);
		/* convert to network order */
		subnet_num = htonl(subnet_num);

		/* Check whether this is in use already */
		ret = is_subnet_in_use(inet_ntoa
		    (*(struct in_addr *)&subnet_num));
		if (ret == 1) {
			/* This is in use, so mark it as used */
			set_subnet_in_use(free_entry, total_subnets,
			    subnet_entries);
		} else {
			/* Found free Subnet */
			found = 1;
			break;
		}
	} while (found == 1 || ret == 1);

	if (ret == 1) {
		return (ZC_NO_SUBNET);
	} else {
		*cz_subnet =
		    (inet_ntoa(*(struct in_addr *)&subnet_num));
		return (ZC_OK);
	}
}

int
zccfg_write_subnet_to_netccr(char *vc_name, char *subnet_num) {
	ccr::updatable_table_var transp = NULL;
	ccr::readonly_table_var tabptr;
	char tablename[MAX_NAME_LEN];
	char keyval[MAX_NAME_LEN], dataval[MAX_NAME_LEN];
	Environment e;
	CORBA::Exception *ex;

	// Check if ORB is initialized
	if (!(ORB::is_initialized())) {
		return (-1);
	}

	ccr::directory_var ccr_ptr = ccr::directory::_nil();

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception())
		e.clear();
	else
		ccr_ptr = ccr::directory::_narrow(obj);

	if (CORBA::is_nil(ccr_ptr))
		return (-1);

	os::sprintf(tablename, "%s", CZNET_CCR_TABLE);
	transp = ccr_ptr->begin_transaction(tablename, e);
	if (e.exception()) {
		e.clear();
		return (-1);
	}

	(void) strcpy(keyval, vc_name);
	(void) strcpy(dataval, subnet_num);

	transp->add_element(keyval, dataval, e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::key_exists::_exnarrow(ex)) {
			// Entry is already there, no changes required
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (0);
		}
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();

		//
		// SCMSGS
		// @explanation
		// An unknown error has occurred while updating the CCR
		// with the curret zone  cluster name.
		// @user_action
		// Examine other syslog messages occurring at about the
		// same time to see if the problem can be identified. Save a
		// copy of the /var/adm/messages files on all nodes and
		// contact your authorized Sun service provider for
		// assistance in diagnosing and correcting the problem.
		//
		(void) logger.log(LOG_DEBUG, MESSAGE, "Failed to add "
		    "%s entry to the CCR table %s.\n", keyval,
		    tablename);
		return (-1);
	}
	transp->commit_transaction(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex)) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		} else {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
				return (-1);
			}
		}
		return (0);
	}

int
zccfg_process_clprivnet(char *cz_name, char **subnet) {
	int ret = 0;
	char *subnet_num;

	ret = get_new_subnet(&subnet_num);
	if (ret != 0) {
		*subnet = NULL;
		return (ret);
	}
	*subnet = strdup(subnet_num);
	return (ZC_OK);
}	

int
zccfg_remove_netccr_entry(char *czname)
{
	ccr::readonly_table_var tabptr;
	ccr::updatable_table_var transp = NULL;
	char tablename[MAX_NAME_LEN];
	Environment e;
	CORBA::Exception *ex;
	int new_version = -1;

	// Check if ORB is initialized
	if (!(ORB::is_initialized())) {
		return (-1);
	}

	ccr::directory_var ccr_ptr = ccr::directory::_nil();

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception())
		e.clear();
	else
		ccr_ptr = ccr::directory::_narrow(obj);

	if (CORBA::is_nil(ccr_ptr))
		return (-1);

	os::sprintf(tablename, "%s", CZNET_CCR_TABLE);
	transp = ccr_ptr->begin_transaction(tablename, e);
	if (e.exception()) {
		e.clear();
		return (-1);
	}

	tabptr = ccr_ptr->lookup(CZNET_CCR_TABLE, e);
	if ((ex = e.exception()) != NULL) {
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
		return (-1);
	}

	new_version = tabptr->get_gennum(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex)) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		} else {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		}
	}

	// Remove the entry identified by the cz given as input
	// to this method.
	transp->remove_element(czname, e);
	if (e.exception()) {
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
		return (-1);
	}

	transp->commit_transaction(e);
	if ((ex = e.exception()) != NULL) {
		if (ccr::table_modified::_exnarrow(ex)) {
			e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
		return (1);
	} else {
		e.clear();
		transp->abort_transaction(e);
		if (e.exception())
			e.clear();
			return (-1);
		}
	}
	return (0);
}

int
zccfg_modify_subnet()
{
	cz_elem_t *cz_elem;
	uint_t num_elem;
	const char *c_netmask = NULL, *ip_addr;
	in_addr_t cluster_netmask, net_num;
	char *subnet_num;
	Environment e;
	ccr::element_seq *ccr_elems;
	ccr::updatable_table_var transp = NULL;
	int i = 0;
	int subnet_modified = 0;
	ccr::readonly_table_var tabptr;
	char tablename[MAX_NAME_LEN];
	CORBA::Exception *ex;

	/* Initialize ORB */
	if (clconf_lib_init() != 0) {
		return (-1);
	}

	clconf_cluster_t *cl = clconf_cluster_get_current();

	/* Get the clusternetmask */
	c_netmask =  clconf_obj_get_property((clconf_obj_t *)cl,
	    "cluster_netmask");
	if (c_netmask == NULL) {
		/*
		 * cluster_netmask property is not available
		 * That means no zone clusters are created.
		 * return 0
		 */
		return (0);
	}
	cluster_netmask = inet_addr(c_netmask);
	cluster_netmask = ntohl(cluster_netmask);

	/* Get the Base IP number (network number) */
	ip_addr = clconf_obj_get_property((clconf_obj_t *)cl,
	    "private_net_number");
	net_num = inet_addr(ip_addr);
	net_num = ntohl(net_num);

	/*
	 * Get the current entries for the zone clusters from CCR
	 */
	if (read_ccr(&cz_elem, &num_elem) != 0)
		return (-1);

	ccr_elems = new ccr::element_seq(num_elem, num_elem);

	for (i = 0; i < num_elem; i++) {
		/*
		 * For each element in the CCR table, check whether
		 * the current subnet is within the base IP range
		 * assigned to the cluster. If not, recalculate
		 * the subnets assigned to the zone clusters using
		 * the new IP range and netmask values.
		 */
		if (((ntohl(inet_addr(cz_elem[i].net_number))) &
		    cluster_netmask) != net_num) {
			/* subnet needs change */
			if (get_new_subnet(&subnet_num) != 0) {
				return (-1);
			}
			(*ccr_elems)[i].key =
			    os::strdup(cz_elem[i].cz_name);
			(*ccr_elems)[i].data = os::strdup(subnet_num);
			subnet_modified = 1;
		}
		if (cz_elem[i].cz_name) {
			free(cz_elem[i].cz_name);
			free(cz_elem[i].net_number);
		}
	}

	/*
	 * If subnet is modified, repopulate the CCR table
	 */
	if (subnet_modified) {
		ccr::directory_var ccr_ptr = ccr::directory::_nil();

		naming::naming_context_var ctxp = ns::local_nameserver();
		CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
		if (e.exception())
			e.clear();
		else
			ccr_ptr = ccr::directory::_narrow(obj);

		if (CORBA::is_nil(ccr_ptr))
			return (-1);

		os::sprintf(tablename, "%s", CZNET_CCR_TABLE);
		transp = ccr_ptr->begin_transaction(tablename, e);
		if (e.exception()) {
			e.clear();
			return (-1);
		}

		// Remove all elements from privip_ccr
		transp->remove_all_elements(e);
		if ((ex = e.exception()) != NULL) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		}

		// Add all new elements from the sequence above
		transp->add_elements(*ccr_elems, e);
		if ((ex = e.exception()) != NULL) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		}

		// Commit the changes to the CCR table.
		transp->commit_transaction(e);
		if (e.exception()) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (-1);
		}
	}

	free(cz_elem);
	delete ccr_elems;

	return (0);
}

int
zccfg_set_clusternetmask(char *num_zclusters)
{
	struct in_addr in;
	in_addr_t net_num, private_subnet_netmask, cluster_netmask;
	uint_t maxprivnets, total_addrs, num;
	const char *priv_snet_mask, *ip_addr;
	char  cluster_netmask_string[16];
	int err;
	char keyval[MAX_NAME_LEN], dataval[MAX_NAME_LEN];
	ccr::updatable_table_var transp = NULL;
	Environment e;
	char tablename[MAX_NAME_LEN];
	CORBA::Exception *ex;

	clconf_cluster_t *cl = clconf_cluster_get_current();
	if (cl == NULL) {
		return (ZC_CCR_ERROR);
	}

	// Get the Base IP number (network number)
	ip_addr = clconf_obj_get_property((clconf_obj_t *)cl,
	    "private_net_number");
	net_num = inet_addr(ip_addr);
	net_num = ntohl(net_num);


	// Get the maxprivnets
	maxprivnets = atoi(clconf_obj_get_property((clconf_obj_t *)cl,
	    "private_maxprivnets"));

	// Get the subnet_netmask
	priv_snet_mask = clconf_obj_get_property((clconf_obj_t *)cl,
	    "private_subnet_netmask");
	private_subnet_netmask = inet_addr(priv_snet_mask);
	private_subnet_netmask = ntohl(private_subnet_netmask);

	// Calculate the cluster netmask
	total_addrs = (((4 * (maxprivnets) / 3) +
	    atoi(num_zclusters)) * (~private_subnet_netmask + 1));
	num = 1;
	cluster_netmask = (in_addr_t)0xFFFFFFFF;
	while (num < total_addrs) {
		num = num << 1;
		cluster_netmask = cluster_netmask << 1;
	}

	in = inet_makeaddr(cluster_netmask, 0);
	(void) strcpy(cluster_netmask_string, inet_ntoa(in));

	// Check whether the netnumber is big enough to have
	// this netmask
	if ((net_num & (~cluster_netmask)) != 0) {
		//
		// SCMSGS
		// @explanation
		// The current network address cannot accommodate the current
		// cluster configuration.
		// @user_action
		// Either reduce the number of zone clusters to configure
		// or change the network address so that it can accommodate
		// the expected number of zone clusters, base cluster nodes,
		// and private networks."
		//
		(void) logger.log(LOG_ERR,
		    MESSAGE, "%s is an invalid value for the current network "
		    "address.\n", num_zclusters);
		return (ZC_INVAL);
	}


	// Write these values to the ccr table
	// Check if ORB is initialized
	if (!(ORB::is_initialized())) {
		return (ZC_CCR_ERROR);
	}

	ccr::directory_var ccr_v = ccr::directory::_nil();

	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception())
		e.clear();
	else
		ccr_v = ccr::directory::_narrow(obj);

	if (CORBA::is_nil(ccr_v))
		return (ZC_NAMESERVER_ERR);

	os::sprintf(tablename, "%s", CCR_INFRA_TABLE);
	transp = ccr_v->begin_transaction(tablename, e);
	if (e.exception()) {
		e.clear();
		return (ZC_NAMESERVER_ERR);
	}

	(void) sprintf(keyval, "cluster.properties.zoneclusters");
	(void) strcpy(dataval, num_zclusters);

	transp->add_element(keyval, dataval, e);
	if ((ex = e.exception()) != NULL) {
		e.clear();
		if (ccr::key_exists::_exnarrow(ex)) {
			// Entry is already there, modify it
			transp->update_element(keyval, dataval, e);
			if ((ex = e.exception()) != NULL) {
				(void) logger.log(LOG_DEBUG, MESSAGE,
				    "Failed to add "
				    "%s entry to the CCR table %s.\n",
				    keyval, tablename);

				e.clear();
				transp->abort_transaction(e);
				if (e.exception())
					e.clear();
				return (ZC_CCR_ERROR);
			}
		} else {
			e.clear();
			(void) logger.log(LOG_DEBUG, MESSAGE,
			    "Failed to add "
			    "%s entry to the CCR table %s.\n",
			    keyval, tablename);
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (ZC_CCR_ERROR);
		}
	}

	(void) sprintf(keyval, "cluster.properties.cluster_netmask");
	(void) strcpy(dataval, cluster_netmask_string);

	transp->add_element(keyval, dataval, e);
	if ((ex = e.exception()) != NULL) {
		e.clear();
		if (ccr::key_exists::_exnarrow(ex)) {
			// Entry is already there, modify it
			transp->update_element(keyval, dataval, e);
			if ((ex = e.exception()) != NULL) {
				//
				// SCMSGS
				// @explanation
				// An unknown error has occurred while
				// updating the CCR with the curret zone
				// cluster name.
				// @user_action
				// Examine other syslog messages occurring
				// at about the same time to see if the
				// problem can be identified. Save a copy of
				// the  /var/adm/messages files on all nodes
				// and contact your authorized Sun
				// service provider for assistance in
				// diagnosing and correcting the
				// problem.
				//

				(void) logger.log(LOG_DEBUG,
				    MESSAGE, "Failed to add "
				    "the %s to the CCR table %s.\n",
				    keyval, tablename);

				e.clear();
				transp->abort_transaction(e);
				if (e.exception())
					e.clear();
				return (ZC_CCR_ERROR);
			}
		} else {
			e.clear();

			(void) logger.log(LOG_DEBUG,
			    MESSAGE, "Failed to add "
			    "the %s to the CCR table %s.\n",
			    keyval, tablename);
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (ZC_CCR_ERROR);
		}
	}

	transp->commit_transaction(e);
	if ((ex = e.exception()) != NULL) {
		//
		// SCMSGS
		// @explanation
		// An unknown error has occurred while updating the CCR
		// with the current zone
		// cluster name.
		// @user_action
		// Examine other syslog messages occurring at about the
		// same time to see if the problem can be identified.
		// Save a copy of the /var/adm/messages files on all nodes
		// and contact your authorized Sun service provider for
		// assistance in diagnosing and correcting the problem.
		//
		(void) logger.log(LOG_DEBUG, MESSAGE, "Failed to commit "
		    "the %s to the CCR table %s.\n", keyval, tablename);
		if (ccr::table_modified::_exnarrow(ex)) {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (ZC_CCR_ERROR);
		} else {
			e.clear();
			transp->abort_transaction(e);
			if (e.exception())
				e.clear();
			return (ZC_CCR_ERROR);
		}
	}
	return (ZC_OK);
}
