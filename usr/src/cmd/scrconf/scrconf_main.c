/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)scrconf_main.c	1.48	09/04/16 SMI"

/*
 * scrconf_main.c
 *
 *      main for scrconf(1M)
 */

/*lint -e793 */
#include "rpc_scadmd.h"

#include <scadmin/scconf.h>
#include <scadmin/scadmin.h>
#include <sys/cladm_int.h>
#include <rgm/sczones.h>
#include <scha.h>
#include <libdid.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <netdb.h>
#include <libintl.h>
#include <locale.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <rpc/rpc.h>
#include <rpc/rpcent.h>
#include <netinet/in.h>
#include <arpa/inet.h>

/* new-cli header files */
#include <cl_errno.h>

#define	SCCONF_CLNT_CREATE_TO	(1 * 60)	/* Clnt create timeout */
#define	SCCONF_CLNT_CALL_TO	(5 * 60)	/* Clnt call timeout */

static char *progname;
static rpcprog_t scadmprog = 0;

static void usage(void);

static scconf_errno_t scrconfcmd_create(int argc, char **argv, uint_t Uflag);
static scconf_errno_t scrconfcmd_add(int argc, char **argv, uint_t Uflag);
static scconf_errno_t scrconfcmd_set(int argc, char **argv, uint_t Uflag);
static scconf_errno_t scrconfcmd_getfile(int argc, char **argv, uint_t Uflag);
static scconf_errno_t scrconfcmd_printnodeid(int argc, char **argv,
    uint_t Uflag);
static scconf_errno_t scrconfcmd_printprivhostname(int argc, char **argv,
    uint_t Uflag);
static scconf_errno_t scrconfcmd_wait(int argc, char **argv, uint_t Uflag);
static scconf_errno_t scrconfcmd_boottoken(int argc, char **argv, uint_t Uflag);
static scconf_errno_t scrconfcmd_net(int argc, char **argv, uint_t Uflag);
static scconf_errno_t scrconfcmd_rmfile(int argc, char **argv, uint_t Uflag);
static scconf_errno_t scrconfcmd_rmnode(int argc, char **argv, uint_t Uflag);

static scconf_errno_t scrconfcmd_sys(int argc, char **argv, uint_t Uflag);

static scconf_errno_t scrconfcmd_major_number(int argc, char **argv,
		uint_t Uflag);

static scconf_errno_t scrconfcmd_xipzones(int argc, char **argv, uint_t Uflag);

static scconf_errno_t scrconfcmd_cluster(char *clustername, CLIENT *clnt);

static scconf_errno_t scrconfcmd_node(char *subopts, CLIENT *clnt,
    scconf_cfg_cluster_t *clconfig, uint_t Uflag);
static scconf_errno_t scrconfcmd_node_local(char *nodename,
    scconf_cfg_cluster_t *clconfig);
static scconf_errno_t scrconfcmd_node_remote(char *nodename, CLIENT *clnt,
    char **busynode);

static scconf_errno_t scrconfcmd_cltr_adapter(char *subopts, CLIENT *clnt,
    scconf_cfg_cluster_t *clconfig, uint_t Uflag);
static scconf_errno_t scrconfcmd_cltr_adapter_local(char *nodename,
    char *transport_type, char *adaptername, char *properties,
    scconf_cfg_cluster_t *clconfig);
static scconf_errno_t scrconfcmd_cltr_adapter_remote(char *nodename,
    char *transport_type, char *adaptername, int vlanid, char *properties,
    CLIENT *clnt);

static scconf_errno_t scrconfcmd_cltr_cpoint(char *subopts, CLIENT *clnt,
    scconf_cfg_cluster_t *clconfig, uint_t Uflag);
static scconf_errno_t scrconfcmd_cltr_cpoint_local(char *cpoint_type,
    char *cpointname, char *properties, scconf_cfg_cluster_t *clconfig);
static scconf_errno_t scrconfcmd_cltr_cpoint_remote(char *cpoint_type,
    char *cpointname, char *properties, CLIENT *clnt);

static scconf_errno_t scrconfcmd_cltr_cable(char *subopts, CLIENT *clnt,
    scconf_cfg_cluster_t *clconfig, uint_t Uflag);
static scconf_errno_t scrconfcmd_cltr_cable_local(
    scconf_cltr_epoint_t *epoint1, scconf_cltr_epoint_t *epoint2,
    scconf_cfg_cluster_t *clconfig);
static scconf_errno_t scrconfcmd_cltr_cable_remote(
    scconf_cltr_epoint_t *epoint1, scconf_cltr_epoint_t *epoint2,
    CLIENT *clnt);

static scconf_errno_t scrconfcmd_set_netaddr(char *subopts,
    scconf_cfg_cluster_t *clconfig, uint_t Uflag);
static scconf_errno_t scrconfcmd_set_netaddr_local(char *snetaddr,
    char *snetmask, char *maxnodes, char *maxprivatenets,
    char *virtualclusters, scconf_cfg_cluster_t *clconfig);

static scconf_errno_t scrconfcmd_set_authentication(char *subopts,
    scconf_cfg_cluster_t *clconfig, uint_t Uflag);
static scconf_errno_t scrconfcmd_addto_secure_joinlist_local(char *addnode,
    scconf_cfg_cluster_t *clconfig);
static scconf_errno_t scrconfcmd_set_secure_authtype_local(
    scconf_authtype_t authtype, scconf_cfg_cluster_t *clconfig);

static scconf_errno_t scrconfcmd_get_network_adapters(scadmin_namelist_t **nlp,
    uint_t flag, char *adaptypes, CLIENT *clnt);
static scconf_errno_t scrconfcmd_get_transport_config(scadmin_namelist_t **nlp,
    CLIENT *clnt);
static scconf_errno_t scrconfcmd_broadcast_ping(CLIENT *clnt);
static scconf_errno_t scrconfcmd_snoop(char *adapters,
    scadmin_namelist_t **nlp, uint_t discover_options, uint_t recvto,
    scadmin_namelist_t **errsp, CLIENT *clnt);
static scconf_errno_t scrconfcmd_autodiscover(char *adapters, char *vlans,
    char *filename, scadmin_namelist_t **nlp, uint_t discover_options,
    uint_t sendcount, uint_t recvto, uint_t waitcount, char *token,
    scadmin_namelist_t **errsp, CLIENT *clnt);

static void free_proplist(scconf_cfg_prop_t *list);

static int is_numeric(char *string);

static int initialize_endpoint(scconf_cltr_epoint_t *epoint, char *endpoint);

static scconf_errno_t scrconf_clnt_create(char *servername, CLIENT **clntp,
    uint_t silent);

static scconf_errno_t scrconf_clnt_addcred(CLIENT *clnt, char *servername);

static scadmin_namelist_t *scrconf_array_to_scadm_namelist(char **array,
    uint_t count);
static void scrconf_free_array(char **array, uint_t count);

static scconf_errno_t get_privname(char *node, char **servername);

/*
 * main
 */
int
main(int argc, char **argv)
{
	int i, c;
	int fflg, aflg, gflg, pflg, xflg, bflg, nflg, rflg, dflg, sflg, Pflg;
	int Xflg, cflg;
	int retry;
	uint_t Uflag;
	scconf_errno_t (*scrconfcmd)(int, char **, uint_t) = NULL;
	scconf_errno_t exitstatus;

	/* Set the program name */
	if ((progname = strrchr(argv[0], '/')) == NULL) {
		progname = argv[0];
	} else {
		++progname;
	}

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		return (1);

	/* check for basic options */
	fflg = aflg = gflg = pflg = xflg = Uflag = bflg = nflg = rflg = 0;
	dflg = sflg = Pflg = cflg = Xflg = 0;

	while ((c = getopt(argc, argv,
	    "f:agpPFxXbn:s:UN:C:h:A:B:m:w:T:rd:ocG:"))
	    != EOF) {
		switch (c) {
		case 'f':	/* create config CCR file or remove file */
			fflg = 1;
			break;

		case 'a':		/* add to transport config */
			aflg = 1;
			scrconfcmd = scrconfcmd_add;
			break;

		case 'g':		/* get file */
			gflg = 1;
			scrconfcmd = scrconfcmd_getfile;
			break;

		case 'p':		/* print node id */
			pflg = 1;
			scrconfcmd = scrconfcmd_printnodeid;
			break;

		case 'P':		/* print private hostname */
			Pflg = 1;
			scrconfcmd = scrconfcmd_printprivhostname;
			break;

		case 'x':		/* wait */
			xflg = 1;
			scrconfcmd = scrconfcmd_wait;
			break;

		case 'b':		/* get permission-to-boot token */
			bflg = 1;
			scrconfcmd = scrconfcmd_boottoken;
			break;

		case 'c':		/* set global property */
			cflg = 1;
			scrconfcmd = scrconfcmd_set;
			break;

		case 'n':		/* net command */
			nflg = 1;
			scrconfcmd = scrconfcmd_net;
			break;

		case 'U':		/* just check usage */
			Uflag = 1;
			break;

		case 'r':		/* remove file or remove node */
			rflg = 1;
			break;

		case 'd':		/* get major number for a driver */
			dflg = 1;
			scrconfcmd = scrconfcmd_major_number;
			break;

		case 'X':		/* get list of xclusive IP zones */
			Xflg = 1;
			scrconfcmd = scrconfcmd_xipzones;
			break;

		case 's':		/*  Update hosts and ntp.conf.cluster */
			sflg = 1;
			scrconfcmd = scrconfcmd_sys;
			break;

		case '?':
			usage();
			exit((int)SCCONF_EUSAGE);

		default:
			break;
		}
	}

	/* reset optind */
	optind = 1;

	/*
	 * Further parsing is required.
	 * Determine between scrconfcmd_create(), scrconfcmd_rmfile(), and
	 * scrconfcmd_rmnode().
	 */
	if ((fflg == 1) || (rflg == 1)) {
		if ((fflg == 1) && (rflg == 0)) {
			scrconfcmd = scrconfcmd_create;
		} else if ((fflg == 1) && (rflg == 1)) {
			scrconfcmd = scrconfcmd_rmfile;
			fflg = 0;
		} else {
		/* (fflg == 0) && (rflg == 1)) */
			scrconfcmd = scrconfcmd_rmnode;
		}
	}

	/* sum of basic options */
	i = fflg + aflg + gflg + pflg + xflg + bflg + nflg
	    + rflg + dflg + sflg + Pflg + Xflg + cflg;

	/* only one form of the command allowed */
	if (i != 1) {
		usage();
		exit((int)SCCONF_EUSAGE);
	}

	/* must be root to proceed */
	if (getuid() != 0) {
		(void) fprintf(stderr, gettext(
		    "%s:  You must be root.\n"), progname);
		return (SCCONF_EPERM);
	}

	/*
	 * Run the command.
	 *
	 * In case the daemon dies, re-try up to three times.
	 * SCCONF_ESTALE is only ever returned if we get RPC_CANTRECV
	 * back from our clnt_call().
	 */
	for (retry = 0; retry < 3; retry++) {
		if (retry > 0) {
			(void) printf(gettext(
			    "%s:  Stale RPC client handle, "
			    "retrying in 10 seconds.\n"),
			    progname);
			(void) sleep(10);
		}
		exitstatus = scrconfcmd(argc, argv, Uflag);
		if (exitstatus != SCCONF_ESTALE)
			break;
	}

	/* done */
	return ((int)exitstatus);
}

/*
 * usage
 *
 *	Print usage message.
 */
static void
usage()
{
	/* Create infrastructure file */
	(void) fprintf(stderr,
	    "usage:  %s [-U] -f <filename> <create_options>\n", progname);

	/* Add options to config using sponsor node */
	(void) fprintf(stderr, "\t%s [-U] -a -N <node> <add_options>\n",
	    progname);

	/* Set global property */
	(void) fprintf(stderr, "\t%s [-U] -c -G <global_fencing>\n",
	    progname);

	/* Copy "src_file" from "node" to local "dst_file" */
	(void) fprintf(stderr, "\t%s -g -N <node> <src_file> <dst_file>\n",
	    progname);

	/* Read stdin, and print node ID for given "nodename" on stdout */
	(void) fprintf(stderr, "\t%s -p -h <node=nodename>\n",
	    progname);

	/* Read stdin, and print priv hostname for given "nodename" on stdout */
	(void) fprintf(stderr, "\t%s -P -h <node=nodename>\n",
	    progname);

	/* Wait for "timeout" seconds for remote "node" to join cluster */
	(void) fprintf(stderr, "\t%s -x <timeout> -N <node>\n",
	    progname);

	/* Get permission to boot */
	(void) fprintf(stderr, "\t%s -b\n", progname);

	/* Remove configuration file on given "node" */
	(void) fprintf(stderr, "\t%s -r -f <filename> -N <node>\n",
	    progname);

	/* Remove selected node via sponsor node */
	(void) fprintf(stderr, "\t%s -r -h <node_to_be_removed> "
	    "-N <sponsor_node> [-F]\n", progname);

	/* Obtain major number information for the specified driver */
	(void) fprintf(stderr, "\t%s -d <driver> -N <node> [-o]\n", progname);

	/* Obtain list of exclusive IP zones */
	(void) fprintf(stderr, "\t%s -X -N <node> [-o]\n", progname);

	/* Network commands */
	(void) fprintf(stderr, "\t%s [-N <node>] "
	    "-n cmd=print_adapters,<adap_options>\n", progname);
	(void) fprintf(stderr, "\t%s [-N <node>] "
	    "-n cmd=print_adapters_available,<adap_options>\n", progname);
	(void) fprintf(stderr, "\t%s [-N <node>] "
	    "-n cmd=print_adapters_active\n", progname);
	(void) fprintf(stderr, "\t%s [-N <node>] "
	    "-n cmd=print_adapters_active_lif\n", progname);
	(void) fprintf(stderr, "\t%s -N <node> "
	    "-n cmd=print_transport_config\n", progname);
	(void) fprintf(stderr, "\t%s [-N <node>] "
	    "-n cmd=broadcast_ping\n", progname);
	(void) fprintf(stderr, "\t%s [-N <node>] "
	    "-n cmd=discover,<discover_options>\n", progname);
	(void) fprintf(stderr, "\t%s [-N <node>] "
	    "-n cmd=discover_send,<discover_options>\n", progname);
	(void) fprintf(stderr, "\t%s [-N <node>] "
	    "-n cmd=discover_receive,<discover_options>\n", progname);
	(void) fprintf(stderr, "\t%s [-N <node>] "
	    "-n cmd=discover_snoop,<discover_options>\n", progname);

	(void) putc('\n', stderr);

	/* System commands */
	(void) fprintf(stderr, "\t%s -N <node> "
	    "-s cmd=update_hosts\n", progname);
	(void) fprintf(stderr, "\t%s -N <node> "
	    "-s cmd=update_ntp\n", progname);
	(void) putc('\n', stderr);

	(void) fprintf(stderr, "Print adapter options:\n");
	(void) fprintf(stderr, "\tadaptypes=<adptype:...>\n");

	(void) putc('\n', stderr);

	(void) fprintf(stderr, "Discover options:\n");
	(void) fprintf(stderr, "\tadapters=<adp:...>\n");
	(void) fprintf(stderr, "\t[filename=<logfile>]\n");
	(void) fprintf(stderr, "\t[doping]\n");
	(void) fprintf(stderr, "\t[nowait]\n");
	(void) fprintf(stderr, "\t[token=<idstring>]\n");
	(void) fprintf(stderr, "\t[sendcount=<count>]\n");
	(void) fprintf(stderr, "\t[recvto=<timeout_seconds>]\n");
	(void) fprintf(stderr, "\t[waitcount=<count>]\n");

	(void) putc('\n', stderr);

	(void) fprintf(stderr, "Create options:\n");
	(void) fprintf(stderr, "\t-C clustername\n");
	(void) fprintf(stderr, "\t-h node=nodename\n");
	(void) fprintf(stderr, "\t-A trtype=type,node=node,name=name"
	    "[,vlanid=<vlanid>][,otheroptions]\n");
	(void) fprintf(stderr, "\t-B type=type,name=name[,otheroptions]\n");
	(void) fprintf(stderr, "\t-m endpoint=[node:]name[@port]"
	    ",endpoint=[node:]name[@port]\n");
	(void) fprintf(stderr, "\t-w netaddr=netaddr[,netmask=netmask]\n");
	(void) fprintf(stderr, "\t-T node=nodename[,...]"
	    "[,authtype=authtype]\n");
	(void) putc('\n', stderr);

	(void) fprintf(stderr, "Add options:\n");
	(void) fprintf(stderr, "\t-C clustername\n");
	(void) fprintf(stderr, "\t-h node=nodename\n");
	(void) fprintf(stderr, "\t-A trtype=type,node=node,name=name"
	    "[,vlanid=<vlanid>][,otheroptions]\n");
	(void) fprintf(stderr, "\t-B type=type,name=name[,otheroptions]\n");
	(void) fprintf(stderr, "\t-m endpoint=[node:]name[@port]"
	    ",endpoint=[node:]name[@port]\n");
	(void) putc('\n', stderr);
}

/*
 * scrconfcmd_create
 *
 *	Create a new cluster configuration file.
 */
static scconf_errno_t
scrconfcmd_create(int argc, char **argv, uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *filename = (char *)0;
	char errbuff[BUFSIZ];
	scconf_cfg_cluster_t *clconfig = (scconf_cfg_cluster_t *)0;
	int c;

	/* Allocate clconfig */
	if (!Uflag) {
		clconfig = (scconf_cfg_cluster_t *)calloc(1,
		    sizeof (scconf_cfg_cluster_t));
		if (clconfig == (scconf_cfg_cluster_t *)0)
			return (SCCONF_ENOMEM);
	}

	optind = 1;
	while ((c = getopt(argc, argv, "f:UC:h:A:B:m:w:T:")) != EOF) {
		switch (c) {
		case 'f':
			if (filename != NULL) {
				usage();
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
			filename = optarg;
			break;

		case 'C':
			if (Uflag)
				break;
			if (clconfig->scconf_cluster_clustername != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			clconfig->scconf_cluster_clustername = strdup(optarg);
			if (clconfig->scconf_cluster_clustername == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
			break;

		case 'h':
			rstatus = scrconfcmd_node(optarg, NULL,
			    clconfig, Uflag);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'A':
			rstatus = scrconfcmd_cltr_adapter(optarg, NULL,
			    clconfig, Uflag);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'B':
			rstatus = scrconfcmd_cltr_cpoint(optarg, NULL,
			    clconfig, Uflag);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'm':
			rstatus = scrconfcmd_cltr_cable(optarg, NULL,
			    clconfig, Uflag);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'w':
			rstatus = scrconfcmd_set_netaddr(optarg,
			    clconfig, Uflag);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'T':
			rstatus = scrconfcmd_set_authentication(optarg,
			    clconfig, Uflag);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			break;

		case '?':
			usage();
			rstatus = SCCONF_EUSAGE;
			goto cleanup;

		default:
			break;
		}
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Create the file */
	if (!Uflag) {
		rstatus = scconf_createfile_infrastructure(filename, clconfig);
		if (rstatus != SCCONF_NOERR) {
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to create configuration file - %s.\n"),
			    progname, errbuff);
			goto cleanup;
		}
	}

cleanup:
	if (clconfig != (scconf_cfg_cluster_t *)0)
		scconf_free_clusterconfig(clconfig);

	return (rstatus);
}

/*
 * scrconfcmd_add
 *
 *	Add node and transport config to existing cluster
 */
static scconf_errno_t
scrconfcmd_add(int argc, char **argv, uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *sponsor_node = (char *)0;
	char *clustername = (char *)0;
	CLIENT *clnt = (CLIENT *)0;
	int c;

	/* Get the sponsor node and cluster name */
	optind = 1;
	while ((c = getopt(argc, argv, "aN:UC:h:A:B:m:")) != EOF) {
		switch (c) {
		case 'N':
			if (sponsor_node != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			sponsor_node = optarg;
			break;

		case 'C':
			if (clustername != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			clustername = optarg;
			break;

		case '?':
			usage();
			rstatus = SCCONF_EUSAGE;
			goto cleanup;

		default:
			break;
		}
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Make sure we have a server */
	if (sponsor_node == NULL) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}

	/*
	 * if not just a usage check, get RPC handle,
	 * check clustername, and add credentials.
	 */
	if (!Uflag) {

		/* Get the CLIENT handle */
		rstatus = scrconf_clnt_create(sponsor_node, &clnt, 0);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* Add credentials */
		rstatus = scrconf_clnt_addcred(clnt, sponsor_node);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		/* If there is a clustername, check it */
		if (clustername) {
			rstatus = scrconfcmd_cluster(clustername, clnt);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
		}
	}

	/* Proccess remaining options */
	optind = 1;
	while ((c = getopt(argc, argv, "aN:UC:h:A:B:m:")) != EOF) {
		switch (c) {
		case 'a':
		case 'N':
		case 'U':
		case 'C':
			break;

		case 'h':
			rstatus = scrconfcmd_node(optarg, clnt, NULL, Uflag);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'A':
			rstatus = scrconfcmd_cltr_adapter(optarg, clnt,
			    NULL, Uflag);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'B':
			rstatus = scrconfcmd_cltr_cpoint(optarg, clnt,
			    NULL, Uflag);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			break;

		case 'm':
			rstatus = scrconfcmd_cltr_cable(optarg, clnt,
			    NULL, Uflag);
			if (rstatus != SCCONF_NOERR)
				goto cleanup;
			break;

		default:
			usage();
			return (SCCONF_EUSAGE);
		}
	}

cleanup:
	if (clnt != (CLIENT *)0) {
		if (clnt->cl_auth)
			auth_destroy(clnt->cl_auth);
		clnt_destroy(clnt);
	}

	return (rstatus);
}

/*
 * scrconfcmd_getfile
 *
 *	Get a copy of a file.
 */
static scconf_errno_t
scrconfcmd_getfile(int argc, char **argv, uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *sponsor_node = (char *)0;
	char *srcfile;
	char *dstfile = NULL;
	FILE *dst_fp = NULL;
	int dst_fd = -1;
	CLIENT *clnt = (CLIENT *)0;
	enum clnt_stat clnt_st;
	sc_result_file clnt_res;
	int c;

	/* Usage check is not performed for getfile */
	if (Uflag != 0)
		return (SCCONF_EINVAL);

	/* initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Get the sponsor node */
	optind = 1;
	while ((c = getopt(argc, argv, "gN:")) != EOF) {
		switch (c) {
		case 'g':
			break;

		case 'N':
			if (sponsor_node != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			sponsor_node = optarg;
			break;

		default:
			usage();
			rstatus = SCCONF_EUSAGE;
			goto cleanup;

		}
	}

	/* Make sure that there are two command args remaining */
	if ((optind + 2) != argc) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}
	srcfile = argv[optind++];
	dstfile = argv[optind++];

	/* Make sure we have our client and files */
	if (sponsor_node == NULL ||
	    srcfile == NULL ||
	    dstfile == NULL) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Attempt to create destination file */
	if ((dst_fd = open(dstfile, O_CREAT | O_EXCL | O_RDWR, 0600)) < 0) {
		switch (errno) {	/*lint !e746 */
		case EEXIST:
			rstatus = SCCONF_EEXIST;
			goto cleanup;

		case EACCES:
			rstatus = SCCONF_EPERM;
			goto cleanup;

		default:
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}
	}

	/* Create the file pointer */
	if ((dst_fp = fdopen(dst_fd, "w+")) == NULL) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Get the CLIENT handle */
	rstatus = scrconf_clnt_create(sponsor_node, &clnt, 0);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Add credentials */
	rstatus = scrconf_clnt_addcred(clnt, sponsor_node);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Set the file pointer in clnt_res */
	clnt_res.sc_res_fp = dst_fp;

	/* Call the RPC server */
	clnt_st = scadmproc_copy_from_1(srcfile, &clnt_res, clnt);

	/* Get the SCCONF_* return status */
	rstatus = (scconf_errno_t)clnt_res.sc_res_errno;

	if (rstatus != SCCONF_ENOEXIST) {
		if (clnt_st != RPC_SUCCESS)
			clnt_perror(clnt, progname);
	}

	/* If err-ed, return error */
	if (clnt_st != RPC_SUCCESS) {
		switch (clnt_st) {
		case RPC_TIMEDOUT:
			rstatus = SCCONF_ETIMEDOUT;
			goto cleanup;

		case RPC_AUTHERROR:
			rstatus = SCCONF_EAUTH;
			goto cleanup;

		case RPC_CANTRECV:
			rstatus = SCCONF_ESTALE;
			goto cleanup;

		default:
			if (rstatus != SCCONF_ENOEXIST)
				rstatus = SCCONF_EUNEXPECTED;

			goto cleanup;
		} /*lint !e788 */
	}

cleanup:
	/* close file pointer and descriptor */
	if (dst_fp != NULL) {
		if (fclose(dst_fp))
			rstatus = SCCONF_EUNEXPECTED;
	}
	if (dst_fd != -1)
		(void) close(dst_fd);

	if (rstatus != SCCONF_NOERR && dstfile != NULL)
		(void) unlink(dstfile);

	/* release RPC client handle */
	if (clnt != (CLIENT *)0) {
		if (clnt->cl_auth)
			auth_destroy(clnt->cl_auth);
		clnt_destroy(clnt);
	}

	return (rstatus);
}

/*
 * scrconfcmd_printnodeid
 *
 *	Print nodeid.
 */
static scconf_errno_t
scrconfcmd_printnodeid(int argc, char **argv, uint_t Uflag)
{
	char *nodename = (char *)0;
	char buffer[BUFSIZ];
	char nodenamebuff[BUFSIZ];
	int nodeid;
	int c;

	/* Usage check is not performed for printnodeid */
	if (Uflag != 0)
		return (SCCONF_EINVAL);

	/* Get the nodename */
	optind = 1;
	while ((c = getopt(argc, argv, "ph:")) != EOF) {
		switch (c) {
		case 'p':
			break;

		case 'h':
			if (nodename != NULL) {
				usage();
				return (SCCONF_EUSAGE);
			}
			/* either "nodename" or "node=<nodename>" are okay */
			if (strncmp("node=", optarg, strlen("node=")) == 0) {
				nodename = strchr(optarg, '=') + 1;
			} else {
				nodename = optarg;
			}
			break;

		default:
			usage();
			return (SCCONF_EUSAGE);
		}
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* Make sure that the nodename was set */
	if (nodename == NULL) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* Read stdin until a match is found */
	while (fgets(buffer, sizeof (buffer), stdin) != NULL) {
		if (buffer[strlen(buffer) - 1] != '\n')
			continue;
		if (sscanf(buffer, "cluster.nodes.%d.name %s\n", &nodeid,
		    nodenamebuff) == 2 &&
		    strcmp(nodenamebuff, nodename) == 0) {
			if (printf("%d\n", nodeid) == EOF)
				return (SCCONF_EUNEXPECTED);
			return (SCCONF_NOERR);
		}
	}

	return (SCCONF_ENOEXIST);
}

/*
 * scrconfcmd_printprivhostname
 *
 *	Print a nodes private hostname.
 */
static scconf_errno_t
scrconfcmd_printprivhostname(int argc, char **argv /*lint -e818 */,
    uint_t Uflag)
{
	char *nodename = (char *)0;
	char buffer[BUFSIZ];
	char nodenamebuff[BUFSIZ];
	int nodeid;
	int c;

	/* Usage check is not performed for printnodeid */
	if (Uflag != 0) {
		return (SCCONF_EINVAL);
	}

	/* Get the nodename */
	optind = 1;
	while ((c = getopt(argc, argv, "Ph:")) != EOF) {
		switch (c) {
		case 'P':
			break;

		case 'h':
			if (nodename != NULL) {
				usage();
				return (SCCONF_EUSAGE);
			}
			/* either "nodename" or "node=<nodename>" are okay */
			if (strncmp("node=", optarg, strlen("node=")) == 0) {
				nodename = strchr(optarg, '=') + 1;
			} else {
				nodename = optarg;
			}
			break;

		default:
			usage();
			return (SCCONF_EUSAGE);
		}
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* Make sure that the nodename was set */
	if (nodename == NULL) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* Read stdin until the node ID is found */
	while (fgets(buffer, sizeof (buffer), stdin) != NULL) {
		if (buffer[strlen(buffer) - 1] != '\n')
			continue;
		if ((sscanf(buffer, "cluster.nodes.%d.name %s\n", &nodeid,
		    nodenamebuff) == 2) && (
		    strcmp(nodenamebuff, nodename) == 0)) {

			/* Found the node ID */
			break;
		}
	}

	/* Read stdin until the privatehostname if found */
	while (fgets(buffer, sizeof (buffer), stdin) != NULL) {
		if (buffer[strlen(buffer) - 1] != '\n')
			continue;
		if (sscanf(buffer, "cluster.nodes.%d.properties."
		    "private_hostname %s\n", &nodeid, nodenamebuff) == 2) {

			/* Found the private hostname */
			if (printf("%s\n", nodenamebuff) == EOF)
				return (SCCONF_EUNEXPECTED);
			return (SCCONF_NOERR);
		}
	}

	return (SCCONF_ENOEXIST);
}

/*
 * scrconfcmd_rmfile
 *
 *	remove file.
 */
static scconf_errno_t
scrconfcmd_rmfile(int argc, char **argv, uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *sponsor_node = (char *)0;
	char *filename = (char *)0;
	CLIENT *clnt = (CLIENT *)0;
	enum clnt_stat clnt_st;
	sc_result clnt_res;
	int c;


	/* Usage check is not performed for printnodeid */
	if (Uflag != 0)
		return (SCCONF_EINVAL);

	/* Get the sponsor_node */
	optind = 1;
	while ((c = getopt(argc, argv, "rf:N:")) != EOF) {
		switch (c) {
		case 'r':
			break;
		case 'f':
			if (filename != NULL) {
				usage();
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
			filename = optarg;
			break;

		case 'N':
			if (sponsor_node != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			sponsor_node = optarg;
			break;

		default:
			usage();
			return (SCCONF_EUSAGE);
		}
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* check for required suboptions */
	if (sponsor_node == NULL) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* Get the CLIENT handle */
	rstatus = scrconf_clnt_create(sponsor_node, &clnt, 0);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Call the RPC server */
	clnt_st = scadmproc_remove_file_1(filename, sponsor_node, &clnt_res,
	    clnt);

	if (clnt_st != RPC_SUCCESS)
		clnt_perror(clnt, progname);

	/* If err-ed, return error */
	if (clnt_st != RPC_SUCCESS) {
		switch (clnt_st) {
			case RPC_TIMEDOUT:
				return (SCCONF_ETIMEDOUT);

			default:
				return (SCCONF_EUNEXPECTED);
		} /*lint !e788 */
	}

	/* Return result of remove file operation */
	rstatus = (scconf_errno_t)clnt_res.sc_res_errno;

cleanup:
	/* release RPC client handle */
	if (clnt != (CLIENT *)0) {
		if (clnt->cl_auth)
			auth_destroy(clnt->cl_auth);
		clnt_destroy(clnt);
	}

	return (rstatus);

}

/*
 * scrconfcmd_rmnode
 *
 *	remove node from cluster.
 *		In order to successfully remove a node from the cluster,
 *		the private interconnect cables and adapters and all
 *		autogenerated rawdisk device services associated with this
 *		node are removed.
 */
static scconf_errno_t
scrconfcmd_rmnode(int argc, char **argv, uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *sponsor_node = (char *)0;
	char *node_to_be_removed = (char *)0;
	scadmin_namelist_t *errs = (scadmin_namelist_t *)0;
	CLIENT *clnt = (CLIENT *)0;
	enum clnt_stat clnt_st;
	sc_result_removenode clnt_res;
	int c;
	char **array;
	uint_t count;
	int force_flag = 0;

	/* Usage check is not performed for rmnode */
	if (Uflag != 0)
		return (SCCONF_EINVAL);

	/* Get the sponsor_node */
	optind = 1;
	while ((c = getopt(argc, argv, "rh:N:F")) != EOF) {
		switch (c) {
		case 'r':
			break;
		case 'h':
			if (node_to_be_removed != NULL) {
				usage();
				rstatus = SCCONF_EINVAL;
				goto cleanup;
			}
			node_to_be_removed = optarg;
			break;

		case 'N':
			if (sponsor_node != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			sponsor_node = optarg;
			break;

		case 'F':
			force_flag = 1;
			break;

		default:
			usage();
			return (SCCONF_EUSAGE);
		}
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* check for required suboptions */
	if (sponsor_node == NULL) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Get the CLIENT handle */
	rstatus = scrconf_clnt_create(sponsor_node, &clnt, 0);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Add credentials */
	rstatus = scrconf_clnt_addcred(clnt, sponsor_node);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Call the RPC server */
	clnt_st = scadmproc_remove_node_1(node_to_be_removed, force_flag,
					&clnt_res, clnt);

	if (clnt_st != RPC_SUCCESS) {
	/* If err-ed, return error */
		clnt_perror(clnt, progname);
		switch (clnt_st) {
			case RPC_TIMEDOUT:
				rstatus = SCCONF_ETIMEDOUT;
				break;
			default:
				rstatus = SCCONF_EAUTH;
				break;
		} /*lint !e788 */
	} else {
	/* Return result of remove node operation */
		rstatus = (scconf_errno_t)clnt_res.sc_res_errno;
	}

	/* Print out the status */
	switch (rstatus) {
	case CL_NOERR:
		break;
	case CL_ENOMEM:
		(void) fprintf(stderr, gettext("%s: Failed to remove node "
			"(%s) - not enough memory.\n"), progname,
			node_to_be_removed);
		break;
	case CL_EOP:
		(void) fprintf(stderr, gettext("%s: Failed to remove node "
			"(%s) - node is in use.\n"),
			progname, node_to_be_removed);
		break;
	case CL_ENOENT:
		(void) fprintf(stderr, gettext("%s:  Failed to remove node "
			"(%s) - node is unknown.\n"),
			progname, node_to_be_removed);
		break;
	case CL_EINTERNAL:
		(void) fprintf(stderr, gettext("%s: Failed to remove node "
			"(%s) - call timed out.\n"), progname,
			node_to_be_removed);
		break;
	default:
		(void) fprintf(stderr, gettext("%s: Failed to remove node "
			"(%s).\n"), progname, node_to_be_removed);
		break;
	} /*lint !e788 */

	/* Get list of errors, if any */
	array = clnt_res.sc_res_errlist.sc_namelist_t_val;
	count = clnt_res.sc_res_errlist.sc_namelist_t_len;
	if (array) {
		/* Convert array into a namelist */
		errs = scrconf_array_to_scadm_namelist(array, count);
		scrconf_free_array(array, count);
		/* Print and then free the memory */
		scadmin_namelist_print(progname, errs, stderr);
		scadmin_free_namelist(errs);
	}

cleanup:
	/* release RPC client handle */
	if (clnt != (CLIENT *)0) {
		if (clnt->cl_auth)
			auth_destroy(clnt->cl_auth);
		clnt_destroy(clnt);
	}

	return (rstatus);

}

/*
 * scrconfcmd_major_number
 *
 *	Obtains specific information about major number entries in
 *	/etc/name_to_major.
 */
static scconf_errno_t
scrconfcmd_major_number(int argc, char **argv, uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *node = (char *)0;
	char *driver = (char *)0;
	scadmin_namelist_t *major_pair = (scadmin_namelist_t *)0;
	CLIENT *clnt = (CLIENT *)0;
	enum clnt_stat clnt_st;
	sc_result_major_number clnt_res;
	int c;
	char **array;
	uint_t count;
	int auth_override = 0;

	/* Usage check is not performed for major_number */
	if (Uflag != 0)
		return (SCCONF_EINVAL);

	/* Get the node */
	optind = 1;
	while ((c = getopt(argc, argv, "d:N:o")) != EOF) {
		switch (c) {
		case 'd':
			if (driver != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			driver = optarg;
			break;
		case 'N':
			if (node != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			node = optarg;
			break;

		case 'o':
			auth_override = 1;
			break;

		default:
			usage();
			return (SCCONF_EUSAGE);
		}
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* check for required suboptions */
	if (node == NULL) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Get the CLIENT handle */
	rstatus = scrconf_clnt_create(node, &clnt, 0);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Add credentials unless authentication is not needed by caller */
	if (!auth_override) {
		rstatus = scrconf_clnt_addcred(clnt, node);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;
	}

	/* Call the RPC server */
	clnt_st = scadmproc_get_major_number_1(driver, &clnt_res, clnt);

	if (clnt_st != RPC_SUCCESS) {
	/* If err-ed, return error */
		clnt_perror(clnt, progname);
		switch (clnt_st) {
			case RPC_TIMEDOUT:
				rstatus = SCCONF_ETIMEDOUT;
				break;
			default:
				rstatus = SCCONF_EUNEXPECTED;
				break;
		} /*lint !e788 */
	} else {
	/* Return result of remove node operation */
		rstatus = (scconf_errno_t)clnt_res.sc_res_errno;
	}

	/* Print out the status */
	switch (rstatus) {
	case SCCONF_NOERR:
		/* Print out the driver and maximum major number pair */
		break;
	case SCCONF_ENOEXIST:
		(void) fprintf(stderr, gettext("%s:  Failed to get major "
			"number for %s - node %s is unknown.\n"),
			progname, driver, node);
		break;
	case SCCONF_EUNEXPECTED:
	default:
		(void) fprintf(stderr, gettext("%s:  Failed to get major "
			"number for %s - unexpected error.\n"),
			progname, driver);
		break;
	} /*lint !e788 */

	/* Get the major number pair */
	array = clnt_res.sc_res_major_numbers.sc_namelist_t_val;
	count = clnt_res.sc_res_major_numbers.sc_namelist_t_len;
	if (array) {
		/* Convert array into a namelist */
		major_pair = scrconf_array_to_scadm_namelist(array, count);
		scrconf_free_array(array, count);
		/* Print and then free the memory */
		scadmin_namelist_print(NULL, major_pair, stderr);
		scadmin_free_namelist(major_pair);
	}

cleanup:
	/* release RPC client handle */
	if (clnt != (CLIENT *)0) {
		if (clnt->cl_auth)
			auth_destroy(clnt->cl_auth);
		clnt_destroy(clnt);
	}

	return (rstatus);

}

/*
 * scrconfcmd_xipzones
 *
 *	Obtains list of exclusive IP zones
 */
static scconf_errno_t
scrconfcmd_xipzones(int argc, char **argv, uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *node = (char *)0;
	scadmin_namelist_t *zonelist_val = (scadmin_namelist_t *)0;
	CLIENT *clnt = (CLIENT *)0;
	enum clnt_stat clnt_st;
	sc_result_zonelist clnt_res;
	int c;
	char **array;
	uint_t count;
	int auth_override = 0;
	char *servername = (char *)NULL;

	/* Usage check is not performed for zoneinfo */
	if (Uflag != 0)
		return (SCCONF_EINVAL);

	/* Get the node and zone name */
	optind = 1;
	while ((c = getopt(argc, argv, "XN:o")) != EOF) {
		switch (c) {
		case 'N':
			if (node != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			node = optarg;
			break;

		case 'o':
			auth_override = 1;
			break;

		case 'X':
			break;
		default:
			usage();
			return (SCCONF_EUSAGE);
		}
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* check for required suboptions */
	if (node == NULL) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* convert Node to internal cluster servername */
	rstatus = get_privname(node, &servername);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Get the CLIENT handle */
	rstatus = scrconf_clnt_create(servername, &clnt, 0);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Add credentials unless authentication is not needed by caller */
	if (!auth_override) {
		rstatus = scrconf_clnt_addcred(clnt, servername);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;
	}

	/* Call the RPC server */
	clnt_st = scadmproc_get_zonelist_1(&clnt_res, clnt);

	if (clnt_st != RPC_SUCCESS) {
	/* If err-ed, return error */
		clnt_perror(clnt, progname);
		switch (clnt_st) {
			case RPC_TIMEDOUT:
				rstatus = SCCONF_ETIMEDOUT;
				break;
			default:
				rstatus = SCCONF_EUNEXPECTED;
				break;
		} /*lint !e788 */
	} else {
		/* Return result of zoneinfo operation */
		rstatus = (scconf_errno_t)clnt_res.sc_res_errno;
	}

	/* Print out the status */
	switch (rstatus) {
	case SCCONF_NOERR:
		/* Print out the zoneinfo values */
		break;
	case SCCONF_EUNEXPECTED:
	default:
		(void) fprintf(stderr, gettext("%s:  Failed to get zone "
		    "information for %s - unexpected error.\n"),
		    progname, node);
		break;
	} /*lint !e788 */

	array = clnt_res.sc_res_zonelist.sc_namelist_t_val;
	count = clnt_res.sc_res_zonelist.sc_namelist_t_len;
	if (array) {
		/* Convert array into a namelist */
		zonelist_val = scrconf_array_to_scadm_namelist(array, count);
		scrconf_free_array(array, count);
		/* Print and then free the memory */
		scadmin_namelist_print(NULL, zonelist_val, stdout);
		scadmin_free_namelist(zonelist_val);
	}

cleanup:
	/* release RPC client handle */
	if (clnt != (CLIENT *)0) {
		if (clnt->cl_auth)
			auth_destroy(clnt->cl_auth);
		clnt_destroy(clnt);
	}

	return (rstatus);
}

/*
 * get_privname
 *
 *	Obtains cluster private name for the provided node name
 */
static scconf_errno_t
get_privname(char *node, char **servername)
{
	scha_err_t scha_res;
	scha_cluster_t scha_handle = (scha_cluster_t)0;
	scha_node_state_t scha_node_state;

	scha_res = scha_cluster_open(&scha_handle);
	if (scha_res != SCHA_ERR_NOERR) {
		(void) fprintf(stderr, gettext("%s: Failed to get cluster "
			    "information.\n"), progname);
		(void) fprintf(stderr, gettext("%s: Make sure this node is "
			    "an active cluster member.\n"), progname);
		return (SCCONF_EINVAL);
	}

	/* Make sure the other node is in the cluster */
	scha_res = scha_cluster_get(scha_handle, SCHA_NODESTATE_NODE,
	    node, &scha_node_state);
	if ((scha_res != SCHA_ERR_NOERR) ||
	    ((scha_res == SCHA_ERR_NOERR) &&
	    (scha_node_state != SCHA_NODE_UP))) {
		(void) fprintf(stderr, gettext("%s: Failed to get node "
		    "information.\n"), progname);
		(void) fprintf(stderr, gettext("%s: Make sure \"%s\" is "
		    "an active cluster member nodename.\n"), progname,
		    node);
		return (SCCONF_EINVAL);
	}

	*servername = (char *)0;
	scha_res = scha_cluster_get(scha_handle, SCHA_PRIVATELINK_HOSTNAME_NODE,
	    node, servername);
	if ((scha_res != SCHA_ERR_NOERR) || (*servername == (char *)0)) {
		(void) fprintf(stderr, gettext("%s: Failed to get cluster "
			    "information.\n"), progname);
		return (SCCONF_EINVAL);
	}

	return (SCCONF_NOERR);
}

/*
 * scrconfcmd_wait
 *
 *	Wait until indicated node joins the cluster (or, until timeout).
 */
static scconf_errno_t
scrconfcmd_wait(int argc, char **argv, uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	CLIENT *clnt = (CLIENT *)0;
	enum clnt_stat clnt_st;
	char *sponsor_node = (char *)0;
	char *stimeout = (char *)0;
	int timeout, start_time, current_time;
	sc_result_ismember clnt_res;
	int c;

	/* Usage check is not performed for wait */
	if (Uflag != 0)
		return (SCCONF_EINVAL);

	/* Get the sponsor node */
	optind = 1;
	while ((c = getopt(argc, argv, "x:N:")) != EOF) {
		switch (c) {
		case 'x':
			if (stimeout != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			stimeout = optarg;
			break;

		case 'N':
			if (sponsor_node != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			sponsor_node = optarg;
			break;

		default:
			usage();
			rstatus = SCCONF_EUSAGE;
			goto cleanup;
		}
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Make sure that the timeout was set correctly */
	if (stimeout == NULL || !isdigit(*stimeout)) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Get the timeout */
	errno = 0;
	timeout = atoi(stimeout);
	if (timeout < 0 || errno != 0) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Make sure we have our client */
	if (sponsor_node == NULL) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Retry every 20 seconds until timeout is passed or node is member */
	clnt = (CLIENT *)0;
	current_time = start_time = (int)time((time_t *)0);
	while ((current_time - start_time) <= timeout) {

		/* If there is an old handle, destroy it, and start over */
		if (clnt) {
			clnt_destroy(clnt);
			clnt = (CLIENT *)0;
		}

		/* Get the cleint handle */
		rstatus = scrconf_clnt_create(sponsor_node, &clnt, 1);
		if (rstatus != SCCONF_NOERR || clnt == (CLIENT *)0) {
			clnt = (CLIENT *)0;
			switch (rpc_createerr.cf_stat) {	/*lint !e746 */

			/* Errors to retry */
			case RPC_TIMEDOUT:
			case RPC_RPCBFAILURE:
			case RPC_PROGNOTREGISTERED:
			case RPC_CANTRECV:
				rstatus = SCCONF_ETIMEDOUT;
				break;

			/* Internal error? */
			case RPC_SUCCESS:
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;

			/* Errors to give up on */
			default:
				clnt_pcreateerror(progname);
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		/* If we have a handle, see if we are a member */
		if (rstatus == SCCONF_NOERR) {
			bzero(&clnt_res, sizeof (clnt_res));
			clnt_st = scadmproc_get_ismember_1(&clnt_res, clnt);
			switch (clnt_st) {

			/* Got it */
			case RPC_SUCCESS:
				rstatus = (scconf_errno_t)clnt_res.sc_res_errno;
				if (rstatus != SCCONF_NOERR)
					goto cleanup;

				/* If we are a mbmeber, we are done */
				if (clnt_res.sc_res_ismember)
					goto cleanup;

				/* Otherwise, retry */
				break;

			/* Errors to Retry */
			case RPC_TIMEDOUT:
			case RPC_CANTRECV:
				break;

			/* Errors to give up on */
			default:
				clnt_perror(clnt, progname);
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			} /*lint !e788 */
		}

		(void) sleep(20);
		current_time = (int)time((time_t *)0);
	}
	rstatus = SCCONF_ETIMEDOUT;

cleanup:
	if (clnt != (CLIENT *)0)
		clnt_destroy(clnt);

	return (rstatus);
}

/*
 * scrconfcmd_boottoken
 *
 *	Make sure that all configured nodes are currently cluster
 *	members.   Then, get permission to boot from the lowest
 *	configured node.   The re-boot token has a two-minute timeout.
 *	So, the caller is expected to leave the cluster by halting
 *	or re-booting within that timeout period.   halt(1M), reboot(1M),
 *	or uadmin(1M) should be used;   it is not safe to use
 *	init(1M) or shutdown(1M), since shutdown by these means could
 *	exceed the timeout period.
 */
/* ARGSUSED */
static scconf_errno_t
scrconfcmd_boottoken(int argc, char **argv, uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	CLIENT *clnt = (CLIENT *)0;
	scconf_cfg_cluster_t *clconfig = NULL;
	scconf_cfg_node_t *cluster_node;
	enum clnt_stat clnt_stat;
	uint_t isfullmembership;
	char *sponsor_node;
	scconf_nodeid_t nodeid, mynodeid;
	sc_result clnt_res;

	/* Usage check is not performed for wait */
	if (Uflag != 0)
		return (SCCONF_EINVAL);

	/* Make sure that there are no additional arguments */
	if (argc != 2) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* Make sure there is full cluster membership */
	isfullmembership = 0;
	rstatus = scconf_isfullmembership(0, &isfullmembership);
	if (rstatus != SCCONF_NOERR)
		return (rstatus);
	if (!isfullmembership)
		return (SCCONF_EBUSY);

	/* Get the private host name of the lowest numbered node */
	rstatus = scconf_get_clusterconfig(&clconfig);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;
	if (clconfig == NULL) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}
	nodeid = SCCONF_NODEID_MAX + 1;
	sponsor_node = NULL;
	for (cluster_node = clconfig->scconf_cluster_nodelist;  cluster_node;
	    cluster_node = cluster_node->scconf_node_next) {
		if (cluster_node->scconf_node_nodeid < nodeid) {
			nodeid = cluster_node->scconf_node_nodeid;
			sponsor_node =
			    cluster_node->scconf_node_privatehostname;
		}
	}
	if (sponsor_node == NULL) {
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

	/* Get my nodeid */
	if (cladm(CL_CONFIG, CL_NODEID, &mynodeid) != 0) {
		rstatus = SCCONF_ENOCLUSTER;
		goto cleanup;
	}

	/* Get the CLIENT handle */
	rstatus = scrconf_clnt_create(sponsor_node, &clnt, 1);
	if (rstatus != SCCONF_NOERR || clnt == (CLIENT *)0) {
		switch (rpc_createerr.cf_stat) {

		/* Errors the caller to scrconf(1M) will want to retry */
		case RPC_TIMEDOUT:
		case RPC_RPCBFAILURE:
		case RPC_PROGNOTREGISTERED:
		case RPC_CANTRECV:
			rstatus = SCCONF_EBUSY;
			goto cleanup;

		/* Internal error? */
		case RPC_SUCCESS:
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;

		/* Errors to give up on */
		default:
			clnt_pcreateerror(progname);
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		} /*lint !e788 */
	}

	/* initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Call the RPC server */
	clnt_stat = scadmproc_get_boottoken_1(mynodeid, &clnt_res, clnt);

	/* If err-ed, print and return error */
	if (clnt_stat != RPC_SUCCESS) {
		switch (clnt_stat) {

		/* Errors the caller to scrconf(1M) will want to retry */
		case RPC_TIMEDOUT:
		case RPC_CANTRECV:
			rstatus = SCCONF_EBUSY;
			goto cleanup;

		/* Authentication error */
		case RPC_AUTHERROR:
			clnt_perror(clnt, progname);
			rstatus = SCCONF_EAUTH;
			goto cleanup;

		default:
			clnt_perror(clnt, progname);
			rstatus = SCCONF_EUNEXPECTED;
			goto cleanup;
		} /*lint !e788 */
	}

	/* Get the SCCONF_* return status */
	rstatus = (scconf_errno_t)clnt_res.sc_res_errno;

cleanup:
	if (clnt != (CLIENT *)0) {
		if (clnt->cl_auth)
			auth_destroy(clnt->cl_auth);
		clnt_destroy(clnt);
	}

	if (clconfig != NULL)
		scconf_free_clusterconfig(clconfig);


	return (rstatus);
}

/*
 * scrconfcmd_net
 *
 *	Run one of the following "net" commands:
 *
 *		cmd=print_adapters		Print all local adapters
 *		cmd=print_adapters_available	Print local available adapters
 *		cmd=print_adapters_active	Print local active adapters
 *		cmd=print_adapters_active_lif	Print local active interfaces
 *		cmd=print_transport_config	Print transport config
 *		cmd=broadcast_ping		Send broadcast ping
 *		cmd=discover			Discover send & receive
 *		cmd=discover_send		Discover send only
 *		cmd=discover_receive		Discover receive only
 *		cmd=discover_snoop		Snoop only
 */
/* ARGSUSED */
static scconf_errno_t
scrconfcmd_net(int argc, char **argv, uint_t Uflag)
{
	scadmin_namelist_t *namelist, *np;
	scadmin_namelist_t *errs;
	char errbuff[BUFSIZ];
	int c;
	char *ptr;
	uint_t flag;
	uint_t discover_options = 0;
	int count;
	char *value;
	char *current, *dupopts = (char *)0;
	char *subopts = NULL;
	char *sponsor_node = NULL;
	CLIENT *clnt = (CLIENT *)0;
	scconf_errno_t rstatus;

	char *knownopts[] = {
		"cmd",				/* 0 - command */
		"adapters",			/* 1 - list of adapters */
		"filename",			/* 2 - filename */
		"doping",			/* 3 - do a ping when done */
		"nowait",			/* 4 - don't wait */
		"token",			/* 5 - token */
		"sendcount",			/* 6 - send count */
		"recvto",			/* 7 - receive timeout */
		"waitcount",			/* 8 - wait count */
		"adaptypes",			/* 9 - known adapter types */
		"vlans",			/* 10 - vlanids for adapters */
		(char *)0
	};

	char *cmd = NULL;
	char *adapters = NULL;
	char *filename = NULL;
	int doping = 0;
	int nowait = 0;
	char *token = NULL;
	char *ssendcount = NULL;
	char *srecvto = NULL;
	char *swaitcount = NULL;
	uint_t sendcount = 0;
	uint_t recvto = 0;
	uint_t waitcount = 0;
	char *adaptypes = NULL;
	char *vlans = NULL;

	/* Usage check is not performed */
	if (Uflag != 0)
		return (SCCONF_EINVAL);

	/* Get the sponsor node, if needed for remote operation */
	optind = 1;
	while ((c = getopt(argc, argv, "n:N:")) != EOF) {
		switch (c) {
		case 'n':
			if (subopts != NULL) {
				usage();
				return (SCCONF_EUSAGE);
			}
			subopts = optarg;
			break;

		case 'N':
			if (sponsor_node != NULL) {
				usage();
				return (SCCONF_EUSAGE);
			}
			sponsor_node = optarg;
			break;

		default:
			usage();
			return (SCCONF_EUSAGE);
		}
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* Make sure that there subopts were given */
	if (subopts == NULL) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconf_strerr(errbuff, SCCONF_ENOMEM);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to complete net command - %s.\n"),
		    progname, errbuff);
		return (SCCONF_ENOMEM);
	}

	/* just "cmd" by itself is okay */
	if (strchr(dupopts, ',') == NULL && strchr(dupopts, '=') == NULL) {
		cmd = dupopts;
	} else {
		current = dupopts;
		while (*current) {
			switch (getsubopt(&current, knownopts, &value)) {

			/* 0 - command */
			case 0:
				if (value == NULL || cmd != NULL) {
					usage();
					free(dupopts);
					return (SCCONF_EUSAGE);
				}
				cmd = value;
				break;

			/* 1 - colon seperated list of adapters to search */
			case 1:
				if (value == NULL || adapters != NULL) {
					usage();
					free(dupopts);
					return (SCCONF_EUSAGE);
				}
				adapters = value;
				break;

			/* 2 - filename */
			case 2:
				if (value == NULL || filename != NULL) {
					usage();
					free(dupopts);
					return (SCCONF_EUSAGE);
				}
				filename = value;
				break;

			/* 3 - do a ping when done */
			case 3:
				++doping;
				break;

			/* 4 - don't wait for full receive timeout */
			case 4:
				++nowait;
				break;

			/* 5 - token id string */
			case 5:
				if (value == NULL || token != NULL) {
					usage();
					free(dupopts);
					return (SCCONF_EUSAGE);
				}
				token = value;
				break;

			/* 6 - send count */
			case 6:
				if (value == NULL || !is_numeric(value) ||
				    ssendcount != NULL) {
					usage();
					free(dupopts);
					return (SCCONF_EUSAGE);
				}
				ssendcount = value;
				sendcount = (uint_t)atoi(ssendcount);
				break;

			/* 7 - recevie timeout in seconds */
			case 7:
				if (value == NULL || !is_numeric(value) ||
				    srecvto != NULL) {
					usage();
					free(dupopts);
					return (SCCONF_EUSAGE);
				}
				srecvto = value;
				recvto = (uint_t)atoi(srecvto);
				break;

			/* 8 - wait count */
			case 8:
				if (value == NULL || !is_numeric(value) ||
				    swaitcount != NULL) {
					usage();
					free(dupopts);
					return (SCCONF_EUSAGE);
				}
				swaitcount = value;
				waitcount = (uint_t)atoi(swaitcount);
				break;

			/* 9 - colon seperated list of adapter types */
			case 9:
				if (value == NULL || adaptypes != NULL) {
					usage();
					free(dupopts);
					return (SCCONF_EUSAGE);
				}
				adaptypes = value;
				break;

			/* 10 - colon seperated list of vlanids for adapters */
			case 10:
				if (value == NULL || *value == NULL ||
				    vlans != NULL) {
					usage();
					free(dupopts);
					return (SCCONF_EUSAGE);
				}
				vlans = value;
				break;

			/* unknown suboptions */
			case -1:
				usage();
				free(dupopts);
				return (SCCONF_EUSAGE);

			/* internal error */
			default:
				scconf_strerr(errbuff, SCCONF_EUNEXPECTED);
				(void) fprintf(stderr, gettext(
				    "%s:  Failed to complete "
				    "net command - %s.\n"),
				    progname, errbuff);
				free(dupopts);
				return (SCCONF_EUNEXPECTED);
			}
		}
	}

	/* check for required suboptions */
	if (cmd == NULL) {
		usage();
		free(dupopts);
		return (SCCONF_EUSAGE);
	}

	/* Find command */
	flag = 0;
	if (strcmp(cmd, "print_adapters") == 0) {
		flag = SCADMIN_ADAPTERS_ALL;
	} else if (strcmp(cmd, "print_adapters_available") == 0) {
		flag = SCADMIN_ADAPTERS_AVAILABLE;
	} else if (strcmp(cmd, "print_adapters_active") == 0) {
		flag = SCADMIN_ADAPTERS_ACTIVE;
	} else if (strcmp(cmd, "print_adapters_active_lif") == 0) {
		flag = SCADMIN_ADAPTERS_ACTIVE_LIF;
	} else if (strcmp(cmd, "print_transport_config") == 0) {
		flag = SCADMIN_TRANSPORT_CONFIG;
	} else if (strcmp(cmd, "broadcast_ping") == 0) {
		flag = SCADMIN_ADAPTERS_PING;
	} else if (strcmp(cmd, "discover") == 0) {
		flag = SCADMIN_ADAPTERS_DLPI_DISCOVER;
		discover_options = (SCADMIN_AUTODISCOVER_SEND |
		    SCADMIN_AUTODISCOVER_RECV);
	} else if (strcmp(cmd, "discover_send") == 0) {
		flag = SCADMIN_ADAPTERS_DLPI_DISCOVER;
		discover_options = SCADMIN_AUTODISCOVER_SEND;
	} else if (strcmp(cmd, "discover_receive") == 0) {
		flag = SCADMIN_ADAPTERS_DLPI_DISCOVER;
		discover_options = SCADMIN_AUTODISCOVER_RECV;
	} else if (strcmp(cmd, "discover_snoop") == 0) {
		flag = SCADMIN_ADAPTERS_DLPI_DISCOVER;
		discover_options = SCADMIN_AUTODISCOVER_SNOOP;
	}

	/* Make sure command flag was set */
	if (!flag) {
		usage();
		free(dupopts);
		return (SCCONF_EUSAGE);
	}

	/* more usage checks */
	switch (flag) {
	case SCADMIN_ADAPTERS_DLPI_DISCOVER:
		if (adapters == NULL || adaptypes != NULL) {
			usage();
			free(dupopts);
			return (SCCONF_EUSAGE);
		}
		break;

	case SCADMIN_TRANSPORT_CONFIG:
		if (!sponsor_node) {
			usage();
			free(dupopts);
			return (SCCONF_EUSAGE);
		}
		if (adapters || filename || doping || nowait || token ||
		    ssendcount || srecvto || swaitcount || adaptypes) {
			usage();
			free(dupopts);
			return (SCCONF_EUSAGE);
		}
		break;

	case SCADMIN_ADAPTERS_ALL:
	case SCADMIN_ADAPTERS_AVAILABLE:
		/* The only legal subupotion is adaptypes */
		if (adapters || filename || doping || nowait || token ||
		    ssendcount || srecvto || swaitcount) {
			usage();
			free(dupopts);
			return (SCCONF_EUSAGE);
		}
		break;

	case SCADMIN_ADAPTERS_ACTIVE:
	case SCADMIN_ADAPTERS_ACTIVE_LIF:
	default:
		/* No other commands take additional suboptions */
		if (adapters || filename || doping || nowait || token ||
		    ssendcount || srecvto || swaitcount || adaptypes) {
			usage();
			free(dupopts);
			return (SCCONF_EUSAGE);
		}
		break;
	}

	/* If there is a sponsor node, get the handle */
	if (sponsor_node) {

		/* Get the CLIENT handle */
		rstatus = scrconf_clnt_create(sponsor_node, &clnt, 0);
		if (rstatus != SCCONF_NOERR) {
			free(dupopts);
			return (rstatus);
		}

		/* Add credentials */
		rstatus = scrconf_clnt_addcred(clnt, sponsor_node);
		if (rstatus != SCCONF_NOERR) {
			free(dupopts);
			return (rstatus);
		}
	}

	/* Run the command */
	switch (flag) {

	/* adapter print flags */
	case SCADMIN_ADAPTERS_ALL:
	case SCADMIN_ADAPTERS_AVAILABLE:
	case SCADMIN_ADAPTERS_ACTIVE:
	case SCADMIN_ADAPTERS_ACTIVE_LIF:

		/* convert the adapter types list from ':' seperated to ',' */
		if (adaptypes) {
			for (ptr = adaptypes;  *ptr;  ptr++) {
				if (*ptr == ':')
					*ptr = ',';
			}
		}

		/* Get the list of network adapters */
		namelist = (scadmin_namelist_t *)0;
		rstatus = scrconfcmd_get_network_adapters(&namelist, flag,
		    adaptypes, clnt);
		if (clnt != (CLIENT *)0) {
			if (clnt->cl_auth)
				auth_destroy(clnt->cl_auth);
			clnt_destroy(clnt);
		}
		if (rstatus != SCCONF_NOERR) {
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
			    "%s:  Net adapters command failed - %s.\n"),
			    progname, errbuff);
			free(dupopts);
			return (rstatus);
		}

		/* Print */
		count = 0;
		for (np = namelist;  np;  np = np->scadmin_nlist_next) {
			if (!np->scadmin_nlist_name)
				continue;
			if (count++)
				(void) putchar(' ');
			(void) fputs(np->scadmin_nlist_name, stdout);
		}
		if (count)
			(void) putchar('\n');

		/* Free the list */
		scadmin_free_namelist(namelist);

		/* Done */
		break;

	/* print transport config */
	case SCADMIN_TRANSPORT_CONFIG:

		/* Get the transport config */
		namelist = (scadmin_namelist_t *)0;
		rstatus = scrconfcmd_get_transport_config(&namelist, clnt);
		if (clnt != (CLIENT *)0) {
			if (clnt->cl_auth)
				auth_destroy(clnt->cl_auth);
			clnt_destroy(clnt);
		}
		if (rstatus != SCCONF_NOERR) {
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to print transport config - %s.\n"),
			    progname, errbuff);
			free(dupopts);
			return (rstatus);
		}

		/* Print */
		scadmin_namelist_print(NULL, namelist, stdout);

		/* Free the list */
		scadmin_free_namelist(namelist);

		/* Done */
		break;

	/* send "broadcast ping" from "clnt" */
	case SCADMIN_ADAPTERS_PING:

		/* Ping */
		rstatus = scrconfcmd_broadcast_ping(clnt);
		if (clnt != (CLIENT *)0) {
			if (clnt->cl_auth)
				auth_destroy(clnt->cl_auth);
			clnt_destroy(clnt);
		}
		if (rstatus != SCCONF_NOERR) {
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
			    "%s:  Ping failed - %s.\n"), progname, errbuff);
			free(dupopts);
			return (rstatus);
		}

		/* Done */
		break;

	/* discover options */
	case SCADMIN_ADAPTERS_DLPI_DISCOVER:

		/* convert the adapters list from ':' seperated to ',' */
		for (ptr = adapters;  *ptr;  ptr++) {
			if (*ptr == ':')
				*ptr = ',';
		}

		/* convert the vlan list from ':' seperated to ',' */
		if (vlans) {
			for (ptr = vlans;  *ptr;  ptr++) {
				if (*ptr == ':')
					*ptr = ',';
			}
		}

		/* add options */
		if (nowait)
			discover_options |= SCADMIN_AUTODISCOVER_NOWAIT;
		if (doping)
			discover_options |= SCADMIN_AUTODISCOVER_PING;

		/* check snoop */
		if (discover_options & SCADMIN_AUTODISCOVER_SNOOP) {
			if ((discover_options &
			    (SCADMIN_AUTODISCOVER_PING |
			    SCADMIN_AUTODISCOVER_SEND)) ||
			    filename) {
				free(dupopts);
				return (SCCONF_EUSAGE);
			}
		}

		errs = (scadmin_namelist_t *)0;
		namelist = (scadmin_namelist_t *)0;

		if (discover_options == SCADMIN_AUTODISCOVER_SNOOP) {

			/* Snoop */
			rstatus = scrconfcmd_snoop(adapters, &namelist,
			    discover_options, recvto, &errs, clnt);
		} else  {

			/* Autodiscover */
			rstatus = scrconfcmd_autodiscover(adapters, vlans,
			    filename, &namelist, discover_options, sendcount,
			    recvto, waitcount, token, &errs, clnt);
		}
		if (clnt != (CLIENT *)0) {
			if (clnt->cl_auth)
				auth_destroy(clnt->cl_auth);
			clnt_destroy(clnt);
		}
		if (rstatus != SCCONF_NOERR) {
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
			    "%s:  Failed net discovery command - %s.\n"),
			    progname, errbuff);
			free(dupopts);
			return (rstatus);
		}

		/* Print */
		scadmin_namelist_print(NULL, namelist, stdout);
		scadmin_namelist_print(progname, errs, stderr);

		/* Free the lists */
		scadmin_free_namelist(namelist);
		scadmin_free_namelist(errs);

		/* Done */
		break;

	/* unknown */
	default:
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to complete net command - unknown option.\n"),
		    progname);
		free(dupopts);
		return (SCCONF_EUNEXPECTED);
	}

	/* cleanup */
	free(dupopts);

	/* Done */
	return (SCCONF_NOERR);
}

/*
 * scrconfcmd_cluster
 *
 *	Verify that the "clnt" is part of the given "clustername".
 */
static scconf_errno_t
scrconfcmd_cluster(char *clustername, CLIENT *clnt)
{
	enum clnt_stat clnt_st;
	sc_result_cname clnt_res;
	scconf_errno_t rstatus;

	/* initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Check arguments */
	if (clustername == NULL || clnt == NULL)
		return (SCCONF_EINVAL);

	/* Call the RPC server */
	clnt_st = scadmproc_get_clustername_1(&clnt_res, clnt);
	if (clnt_st != RPC_SUCCESS)
		clnt_perror(clnt, progname);

	/* If err-ed, return error */
	if (clnt_st != RPC_SUCCESS) {
		switch (clnt_st) {
		case RPC_TIMEDOUT:
			return (SCCONF_ETIMEDOUT);

		case RPC_AUTHERROR:
			return (SCCONF_EAUTH);

		case RPC_CANTRECV:
			return (SCCONF_ESTALE);

		default:
			return (SCCONF_EUNEXPECTED);
		} /*lint !e788 */
	}

	if (clnt_res.sc_res_cname == NULL || *clnt_res.sc_res_cname == NULL) {
		rstatus = SCCONF_ENOEXIST;
	} else if (strcmp(clustername, clnt_res.sc_res_cname) != 0) {
		rstatus = SCCONF_ENOCLUSTER;
	} else {
		rstatus = SCCONF_NOERR;
	}

	if (clnt_res.sc_res_cname != NULL)
		free(clnt_res.sc_res_cname);

	return (rstatus);
}

/*
 * scrconfcmd_node
 *
 *	Add the given "nodename" to the cluster configuration.
 *
 *	If an RPC "clnt" handle is given, the node is added to
 *	the remote configuration;   otherwise, if "clconfig" is given,
 *	the node is added to the given "clconfig" structure.
 *
 *	Or, if "Uflag" is set, only check for usage.
 */
static scconf_errno_t
scrconfcmd_node(char *subopts, CLIENT *clnt, scconf_cfg_cluster_t *clconfig,
    uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *busynode = NULL;
	char *value;
	char *current, *dupopts = (char *)0;

	char *knownopts[] = {
		"node",				/* 0 - node name */
		(char *)0
	};
	char *nodename = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconf_strerr(errbuff, SCCONF_ENOMEM);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add node - %s.\n"),
		    progname, errbuff);
		return (SCCONF_ENOMEM);
	}

	/* just "nodename" by itself is okay */
	if (strchr(dupopts, ',') == NULL && strchr(dupopts, '=') == NULL) {
		nodename = dupopts;
	} else {
		current = dupopts;
		while (*current) {
			switch (getsubopt(&current, knownopts, &value)) {

			/* 0 - node name */
			case 0:
				if (value == NULL || nodename != NULL) {
					usage();
					free(dupopts);
					return (SCCONF_EUSAGE);
				}
				nodename = value;
				break;

			/* unknown suboptions */
			case -1:
				usage();
				free(dupopts);
				return (SCCONF_EUSAGE);

			/* internal error */
			default:
				rstatus = SCCONF_EUNEXPECTED;
				scconf_strerr(errbuff, rstatus);
				(void) fprintf(stderr, gettext(
				    "%s:  Failed to add node - %s.\n"),
				    progname, errbuff);
				free(dupopts);
				return (rstatus);
			}
		}
	}

	/* check for required suboptions */
	if (nodename == NULL) {
		usage();
		free(dupopts);
		return (SCCONF_EUSAGE);
	}

	/* make sure that there is no ':' in the name */
	if (strchr(nodename, ':')) {
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add node - bad name.\n"),
		    progname);
		free(dupopts);
		return (SCCONF_EINVAL);
	}

	/* if just checking for usage, we are done */
	if (Uflag) {
		free(dupopts);
		return (SCCONF_NOERR);
	}

	/* add the nodename */
	if (clnt != NULL) {
		rstatus = scrconfcmd_node_remote(nodename, clnt, &busynode);
	} else if (clconfig != NULL) {
		rstatus = scrconfcmd_node_local(nodename, clconfig);
	} else {
		rstatus = SCCONF_EUNEXPECTED;
	}

	if (rstatus != SCCONF_NOERR) {
		switch (rstatus) {
		case SCCONF_EEXIST:
			(void) sprintf(errbuff, gettext(
			    "nodename \"%s\" already exists"),
			    nodename);
			break;

		/*
		 * If SCCONF_EBUSY is returned, it means that "installmode"
		 * is enabled, but all configured nodes are not all yet cluster
		 * members.  When SCCONF_EBUSY is returned, we print the
		 * name of the node we are waiting for on stdout.
		 */
		case SCCONF_EBUSY:
			if (busynode) {
				/* busy node name to stdout */
				(void) puts(busynode);

				(void) sprintf(errbuff, gettext(
				    "waiting for \"%s\" to join the cluster"),
				    busynode);

				/* Free busy node name */
				free(busynode);
			} else {
				(void) sprintf(errbuff, gettext(
				    "waiting for all configured nodes to "
				    "join the cluster"));
			}
			break;

		default:
			scconf_strerr(errbuff, rstatus);
			break;
		} /*lint !e788 */

		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add node (%s) - %s.\n"),
		    progname, nodename, errbuff);
		free(dupopts);
		return (rstatus);
	}

	free(dupopts);

	return (SCCONF_NOERR);
}

/*
 * scrconfcmd_node_local
 *
 *	Add the given "nodename" to the given "clconfig" structure.
 */
static scconf_errno_t
scrconfcmd_node_local(char *nodename, scconf_cfg_cluster_t *clconfig)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_cfg_node_t *node = (scconf_cfg_node_t *)0;
	scconf_cfg_node_t **nextp;

	/* Check arguments */
	if (nodename == NULL || clconfig == NULL ||
	    scconf_clconfig_infrastructure(clconfig) != SCCONF_NOERR)
		return (SCCONF_EINVAL);

	/* Allocate struct for the next node */
	node = (scconf_cfg_node_t *)calloc(1, sizeof (scconf_cfg_node_t));
	if (node == (scconf_cfg_node_t *)0) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Add the nodename */
	if ((node->scconf_node_nodename = strdup(nodename)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Find location of the last pointer, and set it */
	for (nextp = &clconfig->scconf_cluster_nodelist;  *nextp;
	    nextp = &(*nextp)->scconf_node_next)
		;
	*nextp = node;

	/* And, check our configuration again */
	rstatus = scconf_clconfig_infrastructure(clconfig);
	if (rstatus != SCCONF_NOERR) {
		*nextp = (scconf_cfg_node_t *)0;
		goto cleanup;
	}

cleanup:
	if (rstatus != SCCONF_NOERR && node) {
		if (node->scconf_node_nodename)
			free(node->scconf_node_nodename);
		free(node);
	}

	return (rstatus);
}

/*
 * scrconfcmd_node_remote
 *
 *	Add the given "nodename" to the configuration on the given "clnt".
 */
static scconf_errno_t
scrconfcmd_node_remote(char *nodename, CLIENT *clnt, char **busynode)
{
	scconf_errno_t rstatus;
	enum clnt_stat clnt_st;
	sc_result_addnode clnt_res;

	/* initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Check arguments */
	if (nodename == NULL || clnt == NULL)
		return (SCCONF_EINVAL);

	/* Call the RPC server */
	clnt_st = scadmproc_add_node_1(nodename, &clnt_res, clnt);
	if (clnt_st != RPC_SUCCESS)
		clnt_perror(clnt, progname);

	/* If err-ed, return error */
	if (clnt_st != RPC_SUCCESS) {
		switch (clnt_st) {
		case RPC_TIMEDOUT:
			return (SCCONF_ETIMEDOUT);

		case RPC_AUTHERROR:
			return (SCCONF_EAUTH);

		case RPC_CANTRECV:
			return (SCCONF_ESTALE);

		default:
			return (SCCONF_EUNEXPECTED);
		} /*lint !e788 */
	}

	/* Return result of add_node operation */
	rstatus = (scconf_errno_t)clnt_res.sc_res_errno;

	/*
	 * If SCCONF_EBUSY is returned, it means that "installmode"
	 * is enabled, but all configured nodes are not all yet cluster
	 * members.   Return to caller both SCCONF_EBUSY and the name of a
	 * node for which we are waiting to join the cluster.
	 */
	if (rstatus == SCCONF_EBUSY) {
		if (clnt_res.sc_res_busynode && *clnt_res.sc_res_busynode) {
			*busynode = strdup(clnt_res.sc_res_busynode);
			if (*busynode == NULL)
				rstatus = SCCONF_ENOMEM;
		}
	}

	/* If busy node name was set, free the memory from clnt_res */
	if (clnt_res.sc_res_busynode != NULL)
		free(clnt_res.sc_res_busynode);

	return (rstatus);
}

/*
 * scrconfcmd_cltr_adapter
 *
 *	Add an adapter to the cluster configuration.
 *
 *	If an RPC "clnt" handle is given, the adapter is added to
 *	the remote configuration;   otherwise, if "clconfig" is given,
 *	the adapter is added to the given "clconfig" structure.
 */
static scconf_errno_t
scrconfcmd_cltr_adapter(char *subopts, CLIENT *clnt,
    scconf_cfg_cluster_t *clconfig, uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *last, *dupopts = (char *)0;

	char *knownopts[] = {
		"trtype",			/* 0 - transport type */
		"name",				/* 1 - adapter name */
		"node",				/* 2 - node name or ID */
		"transport_type",		/* 3 - NOT LEGAL property */
		"vlanid",			/* 4 - vlanid */
		"vlan_id",			/* 5 - INVALID - vlan_id */
		(char *)0
	};
	char *transport_type = NULL;
	char *adaptername = NULL;
	char *nodename = NULL;
	char *properties = NULL;
	char *svlanid = NULL;
	int vlanid = 0;
	long tmp_vlanid = 0;
	char *endp;
	char *vlan_prop = NULL;
	char *expanded_properties = NULL;
	char *device_name = NULL;
	char *device_instance = NULL;
	div_t inst_vlan;
	long tmp_instance = 0;
	char *expanded_adaptername = NULL;
	size_t len;
	int expanded_instance_num = 0;
	size_t prop_len;

	/* duplicate subopts and allocate space for properties */
	if ((dupopts = strdup(subopts)) == NULL ||
	    (properties = strdup(subopts)) == NULL) {
		if (dupopts)
			free(dupopts);
		if (properties)
			free(properties);
		rstatus = SCCONF_ENOMEM;
		scconf_strerr(errbuff, rstatus);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add cluster transport adapter - %s.\n"),
		    progname, errbuff);
		return (rstatus);
	}
	bzero(properties, strlen(properties));

	vlan_prop = (char *)calloc(1, strlen("vlan_id=") + 20);
	if (vlan_prop == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	current = dupopts;
	while (*current) {
		last = current;
		switch (getsubopt(&current, knownopts, &value)) {

		/* 0 - transport type */
		case 0:
			if (value == NULL || transport_type != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			transport_type = value;
			break;

		/* 1 - adapter name */
		case 1:
			if (value == NULL || adaptername != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			adaptername = value;
			break;

		/* 2 - node name or ID */
		case 2:
			if (value == NULL || nodename != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			nodename = value;
			break;

		/* 3 - NOT LEGAL property */
		case 3:
			usage();
			rstatus = SCCONF_EUSAGE;
			goto cleanup;

		/* 4 - vlanid */
		case 4:
			if (value == NULL || svlanid != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			svlanid = value;
			tmp_vlanid = strtol(value, &endp, 10);
			if (*endp != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;
			}
			if ((tmp_vlanid < VLAN_ID_MIN) ||
			    (tmp_vlanid > VLAN_ID_MAX)) {
				rstatus = SCCONF_ERANGE;
				scconf_strerr(errbuff, rstatus);
				(void) fprintf(stderr, gettext(
				    "%s:  Failed to add cluster "
				    "transport adapter - %s.\n"),
				    progname, errbuff);
				goto cleanup;
			}
			vlanid = (int)tmp_vlanid;

			break;

		/* 5 - vlanid */
		case 5:
			usage();
			rstatus = SCCONF_EUSAGE;
			goto cleanup;


		/* properties */
		case -1:
			if (*properties != '\0')
				(void) strcat(properties, ",");
			(void) strcat(properties, last);
			break;

		/* internal error */
		default:
			rstatus = SCCONF_EUNEXPECTED;
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to add cluster "
			    "transport adapter - %s.\n"),
			    progname, errbuff);
			goto cleanup;
		}
	}

	/* check for required suboptions */
	if (transport_type == NULL ||
	    adaptername == NULL ||
	    nodename == NULL) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* make sure that there is no ':' in the name */
	if (strchr(adaptername, ':')) {
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add cluster transport adapter - "
		    "bad name.\n"), progname);
		rstatus = SCCONF_EINVAL;
		goto cleanup;
	}

	/* if just checking for usage, we are done */
	if (Uflag) {
		rstatus = SCCONF_NOERR;
		goto cleanup;
	}

	/* add the adapter */
	if (clnt != NULL) {
		rstatus = scrconfcmd_cltr_adapter_remote(nodename,
		    transport_type, adaptername, vlanid, properties, clnt);
	} else if (clconfig != NULL) {
		/* Split the adaptername into its two components */
		len = strlen(adaptername) + 1;
		device_name = (char *)calloc(1, len);
		device_instance = (char *)calloc(1, len);
		if (device_name == NULL || device_instance == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
		rstatus = conf_split_adaptername(adaptername, device_name,
		    device_instance);
		if (rstatus != SCCONF_NOERR)
			goto cleanup;

		tmp_instance = strtol(device_instance, &endp, 10);
		if (*endp != NULL) {
			(void) fprintf(stderr, gettext("%s: Invalid value"
			    " \"%.256s\" given for device instance.\n"),
			    progname, device_instance);
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		if (tmp_instance >
		    (VLAN_ID_MAX * VLAN_MULTIPLIER + VLAN_ZONE_START - 1)) {
			(void) fprintf(stderr, gettext("%s: Device instance "
			    "\"%.256s\" is out of range.\n"), progname,
			    device_instance);
			rstatus = SCCONF_ERANGE;
			goto cleanup;
		}

		if ((tmp_instance >= VLAN_ZONE_START) && (vlanid != 0)) {
			(void) fprintf(stderr, gettext("%s: The device "
			    "instance number specifies a VLAN device, you "
			    "cannot specify a separate VLAN ID.\n"),
			    progname);
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		if (vlanid != 0) {

			/* Expand the adaptername */
			expanded_adaptername = (char *)calloc(1, len + 20);
			if (expanded_adaptername == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
			(void) strncpy(expanded_adaptername, device_name,
			    strlen(device_name));
			expanded_instance_num = vlanid * VLAN_MULTIPLIER +
			    (int)tmp_instance;
			(void) sprintf(expanded_adaptername +
			    strlen(device_name), "%d", expanded_instance_num);
			(void) sprintf(vlan_prop, "vlan_id=%d", vlanid);

		} else if (tmp_instance >= VLAN_ZONE_START) {

			/* Implicit vlan device, deduce vlanid */
			inst_vlan = div((int)tmp_instance, VLAN_MULTIPLIER);
			vlanid = inst_vlan.quot;
			(void) sprintf(vlan_prop, "vlan_id=%d", vlanid);
			expanded_adaptername = adaptername;
		}

		/* Add the vlanid back to the properties */
		if (vlanid != 0) {
			prop_len = strlen(properties);
			expanded_properties = calloc(1, prop_len +
			    strlen(vlan_prop) + 20);
			if (expanded_properties == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
			(void) sprintf(expanded_properties, properties);
			if (*expanded_properties != '\0')
				(void) strcat(expanded_properties, ",");
			(void) strcat(expanded_properties, vlan_prop);
			free(vlan_prop);
			vlan_prop = NULL;
			rstatus = scrconfcmd_cltr_adapter_local(nodename,
			    transport_type, expanded_adaptername,
			    expanded_properties, clconfig);
			free(expanded_properties);
			expanded_properties = NULL;
		} else {
			rstatus = scrconfcmd_cltr_adapter_local(nodename,
			    transport_type, adaptername, properties, clconfig);
		}
	} else {
		rstatus = SCCONF_EUNEXPECTED;
	}
	if (rstatus != SCCONF_NOERR) {
		switch (rstatus) {
		case SCCONF_EUNKNOWN:
			(void) sprintf(errbuff, gettext(
			    "unknown adapter or transport type"));
			break;

		case SCCONF_TM_EBADOPTS:
			(void) sprintf(errbuff, gettext(
			    "bad additional properties given"));
			break;

		default:
			scconf_strerr(errbuff, rstatus);
			break;
		} /*lint !e788 */
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add cluster transport adapter - %s.\n"),
		    progname, errbuff);
		goto cleanup;
	}

cleanup:
	free(dupopts);
	free(properties);
	if (vlan_prop)
		free(vlan_prop);
	if (device_name)
		free(device_name);
	if (device_instance)
		free(device_instance);
	if (expanded_adaptername)
		free(expanded_adaptername);
	if (expanded_properties)
		free(expanded_properties);
	return (rstatus);
}

/*
 * scrconfcmd_cltr_adapter_local
 *
 *	Add an adapter to the given "clconfig" structure.
 */
static scconf_errno_t
scrconfcmd_cltr_adapter_local(char *nodename,
    char *transport_type, char *adaptername, char *properties,
    scconf_cfg_cluster_t *clconfig)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_cfg_node_t *node;
	scconf_cfg_cltr_adap_t *adapter = (scconf_cfg_cltr_adap_t *)0;
	scconf_cfg_cltr_adap_t **nextp;

	/* Check arguments */
	if (nodename == NULL ||
	    transport_type == NULL ||
	    adaptername == NULL ||
	    clconfig == NULL ||
	    scconf_clconfig_infrastructure(clconfig) != SCCONF_NOERR)
		return (SCCONF_EINVAL);

	/* Allocate struct for the next adapter */
	adapter = (scconf_cfg_cltr_adap_t *)calloc(1,
	    sizeof (scconf_cfg_cltr_adap_t));
	if (adapter == (scconf_cfg_cltr_adap_t *)0) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Add the adaptername */
	if ((adapter->scconf_adap_adaptername = strdup(adaptername)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Add the transport type */
	if ((adapter->scconf_adap_cltrtype = strdup(transport_type)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Add the properties */
	rstatus = scconf_propstr_to_proplist(properties,
	    &adapter->scconf_adap_propertylist);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Find the node */
	for (node = clconfig->scconf_cluster_nodelist;  node;
	    node = node->scconf_node_next) {
		if (node->scconf_node_nodename &&
		    strcmp(node->scconf_node_nodename, nodename) == 0)
			break;
	}
	if (node == NULL) {
		rstatus = SCCONF_ENOEXIST;
		goto cleanup;
	}

	/* Find location of the last pointer, and set it */
	for (nextp = &node->scconf_node_adapterlist;  *nextp;
	    nextp = &(*nextp)->scconf_adap_next)
		;
	*nextp = adapter;

	/* And, check our configuration again */
	rstatus = scconf_clconfig_infrastructure(clconfig);
	if (rstatus != SCCONF_NOERR) {
		*nextp = (scconf_cfg_cltr_adap_t *)0;
		goto cleanup;
	}

cleanup:
	if (rstatus != SCCONF_NOERR && adapter) {
		if (adapter->scconf_adap_propertylist)
			free_proplist(adapter->scconf_adap_propertylist);
		if (adapter->scconf_adap_adaptername)
			free(adapter->scconf_adap_adaptername);
		free(adapter);
	}

	return (rstatus);
}

/*
 * scrconfcmd_cltr_adapter_remote
 *
 *	Add an adapter to the configuration on the given "clnt".
 */
static scconf_errno_t
scrconfcmd_cltr_adapter_remote(char *nodename,
    char *transport_type, char *adaptername, int vlanid, char *properties,
    CLIENT *clnt)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	enum clnt_stat clnt_st;
	sc_result clnt_res;

	/* initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Check arguments */
	if (nodename == NULL ||
	    transport_type == NULL ||
	    adaptername == NULL ||
	    clnt == NULL)
		return (SCCONF_EINVAL);

	/* Call the RPC server */
	clnt_st = scadmproc_add_cltr_adapter_1(nodename,
	    transport_type, adaptername, vlanid, properties, &clnt_res, clnt);
	if (clnt_st != RPC_SUCCESS)
		clnt_perror(clnt, progname);

	/* If err-ed, return error */
	if (clnt_st != RPC_SUCCESS) {
		switch (clnt_st) {
		case RPC_TIMEDOUT:
			return (SCCONF_ETIMEDOUT);

		case RPC_AUTHERROR:
			return (SCCONF_EAUTH);

		case RPC_CANTRECV:
			return (SCCONF_ESTALE);

		default:
			return (SCCONF_EUNEXPECTED);
		} /*lint !e788 */
	}

	/* Return result of add_cltr_adapter operation */
	rstatus = (scconf_errno_t)clnt_res.sc_res_errno;

	return (rstatus);
}

/*
 * scrconfcmd_cltr_cpoint
 *
 *	Add an "off-node" cluster transport connection point definition
 *	to the cluster configuration.
 *
 *	If an RPC "clnt" handle is given, the cpoint is added to
 *	the remote configuration;  otherwise, if "clconfig" is given,
 *	the cpoint is added to the given "clconfig" structure.
 */
static scconf_errno_t
scrconfcmd_cltr_cpoint(char *subopts, CLIENT *clnt,
    scconf_cfg_cluster_t *clconfig, uint_t Uflag)
{
	scconf_errno_t rstatus;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *last, *dupopts = (char *)0;

	char *knownopts[] = {
		"type",				/* 0 - cpoint type */
		"name",				/* 1 - cpoint name */
		(char *)0
	};
	char *cpoint_type = NULL;
	char *cpointname = NULL;
	char *properties = NULL;

	/* duplicate subopts and allocate space for properties */
	if ((dupopts = strdup(subopts)) == NULL ||
	    (properties = strdup(subopts)) == NULL) {
		if (dupopts)
			free(dupopts);
		if (properties)
			free(properties);
		rstatus = SCCONF_ENOMEM;
		scconf_strerr(errbuff, rstatus);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add connection point - %s.\n"),
		    progname, errbuff);
		return (rstatus);
	}
	bzero(properties, strlen(properties));

	current = dupopts;
	while (*current) {
		last = current;
		switch (getsubopt(&current, knownopts, &value)) {

		/* 0 - cpoint type */
		case 0:
			if (value == NULL || cpoint_type != NULL) {
				usage();
				free(dupopts);
				free(properties);
				return (SCCONF_EUSAGE);
			}
			cpoint_type = value;
			break;

		/* 1 - cpoint name */
		case 1:
			if (value == NULL || cpointname != NULL) {
				usage();
				free(dupopts);
				free(properties);
				return (SCCONF_EUSAGE);
			}
			cpointname = value;
			break;

		/* properties */
		case -1:
			if (*properties != '\0')
				(void) strcat(properties, ",");
			(void) strcat(properties, last);
			break;

		/* internal error */
		default:
			rstatus = SCCONF_EUNEXPECTED;
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to add connection point - %s.\n"),
			    progname, errbuff);
			free(dupopts);
			free(properties);
			return (rstatus);
		}
	}

	/* check for required suboptions */
	if (cpoint_type == NULL ||
	    cpointname == NULL) {
		usage();
		free(dupopts);
		free(properties);
		return (SCCONF_EUSAGE);
	}

	/* make sure that there is no ':' in the name */
	if (strchr(cpointname, ':')) {
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add connection point - bad name.\n"),
		    progname);
		free(dupopts);
		free(properties);
		return (SCCONF_EINVAL);
	}

	/* if just checking for usage, we are done */
	if (Uflag) {
		free(dupopts);
		free(properties);
		return (SCCONF_NOERR);
	}

	/* add the connection point */
	if (clnt != NULL) {
		rstatus = scrconfcmd_cltr_cpoint_remote(cpoint_type,
		    cpointname, properties, clnt);
	} else if (clconfig != NULL) {
		rstatus = scrconfcmd_cltr_cpoint_local(cpoint_type,
		    cpointname, properties, clconfig);
	} else {
		rstatus = SCCONF_EUNEXPECTED;
	}
	if (rstatus != SCCONF_NOERR) {
		switch (rstatus) {
		case SCCONF_EUNKNOWN:
			(void) sprintf(errbuff, gettext(
			    "unknown junction type"));
			break;

		case SCCONF_TM_EBADOPTS:
			(void) sprintf(errbuff, gettext(
			    "bad additional properties given"));
			break;

		default:
			scconf_strerr(errbuff, rstatus);
			break;
		} /*lint !e788 */
		scconf_strerr(errbuff, rstatus);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add connection point - %s.\n"),
		    progname, errbuff);
		free(dupopts);
		free(properties);
		return (rstatus);
	}

	free(dupopts);
	free(properties);

	return (SCCONF_NOERR);
}

/*
 * scrconfcmd_cpoint_local
 *
 *	Add a cpoint to the given "clconfig" structure.
 */
static scconf_errno_t
scrconfcmd_cltr_cpoint_local(char *cpoint_type, char *cpointname,
    char *properties, scconf_cfg_cluster_t *clconfig)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_cfg_cpoint_t *cpoint = (scconf_cfg_cpoint_t *)0;
	scconf_cfg_cpoint_t **nextp;

	/* Check arguments */
	if (cpoint_type == NULL ||
	    cpointname == NULL ||
	    clconfig == NULL ||
	    scconf_clconfig_infrastructure(clconfig) != SCCONF_NOERR)
		return (SCCONF_EINVAL);

	/* Allocate struct for the next cpoint */
	cpoint = (scconf_cfg_cpoint_t *)calloc(1,
	    sizeof (scconf_cfg_cpoint_t));
	if (cpoint == (scconf_cfg_cpoint_t *)0) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Add the cpointname */
	if ((cpoint->scconf_cpoint_cpointname = strdup(cpointname)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Add the cpoint type */
	if ((cpoint->scconf_cpoint_type = strdup(cpoint_type)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Add the properties */
	rstatus = scconf_propstr_to_proplist(properties,
	    &cpoint->scconf_cpoint_propertylist);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

	/* Find location of the last pointer, and set it */
	for (nextp = &clconfig->scconf_cluster_cpointlist;  *nextp;
	    nextp = &(*nextp)->scconf_cpoint_next)
		;
	*nextp = cpoint;

	/* And, check our configuration again */
	rstatus = scconf_clconfig_infrastructure(clconfig);
	if (rstatus != SCCONF_NOERR) {
		*nextp = (scconf_cfg_cpoint_t *)0;
		goto cleanup;
	}

cleanup:
	if (rstatus != SCCONF_NOERR && cpoint) {
		if (cpoint->scconf_cpoint_propertylist)
			free_proplist(cpoint->scconf_cpoint_propertylist);
		if (cpoint->scconf_cpoint_cpointname)
			free(cpoint->scconf_cpoint_cpointname);
		free(cpoint);
	}

	return (rstatus);
}

/*
 * scrconfcmd_cpoint_remote
 *
 *	Add a cpoint to the configuration on the given "clnt".
 */
static scconf_errno_t
scrconfcmd_cltr_cpoint_remote(char *cpoint_type, char *cpointname,
    char *properties, CLIENT *clnt)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	enum clnt_stat clnt_st;
	sc_result clnt_res;

	/* initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Check arguments */
	if (cpoint_type == NULL ||
	    cpointname == NULL ||
	    clnt == NULL)
		return (SCCONF_EINVAL);

	/* Call the RPC server */
	clnt_st = scadmproc_add_cltr_cpoint_1(cpoint_type,
	    cpointname, properties, &clnt_res, clnt);
	if (clnt_st != RPC_SUCCESS)
		clnt_perror(clnt, progname);

	/* If err-ed, return error */
	if (clnt_st != RPC_SUCCESS) {
		switch (clnt_st) {
		case RPC_TIMEDOUT:
			return (SCCONF_ETIMEDOUT);

		case RPC_AUTHERROR:
			return (SCCONF_EAUTH);

		case RPC_CANTRECV:
			return (SCCONF_ESTALE);

		default:
			return (SCCONF_EUNEXPECTED);
		} /*lint !e788 */
	}

	/* Return result of add_cltr_adapter operation */
	rstatus = (scconf_errno_t)clnt_res.sc_res_errno;

	return (rstatus);
}

/*
 * scrconfcmd_cltr_cable
 *
 *	Add a cluster transport cable to the cluster configuration.
 *
 *	If an RPC "clnt" handle is given, the cable is added to
 *	the remote configuration;  otherwise, if "clconfig" is given,
 *	the cable is added to the given "clconfig" structure.
 */
static scconf_errno_t
scrconfcmd_cltr_cable(char *subopts, CLIENT *clnt,
    scconf_cfg_cluster_t *clconfig, uint_t Uflag)
{
	scconf_errno_t rstatus;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	scconf_cltr_epoint_t epoint1, epoint2;

	char *knownopts[] = {
		"endpoint",			/* 0 - cable endpoint */
		(char *)0
	};
	char *endpoint1 = NULL;
	char *endpoint2 = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		scconf_strerr(errbuff, rstatus);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add cluster transport cable - %s.\n"),
		    progname, errbuff);
		return (rstatus);
	}

	current = dupopts;
	while (*current) {
		switch (getsubopt(&current, knownopts, &value)) {

		/* 0 - cable endpoint */
		case 0:
			if (value == NULL ||
			    (endpoint1 != NULL && endpoint2 != NULL)) {
				usage();
				free(dupopts);
				return (SCCONF_EUSAGE);
			}
			if (endpoint1 == NULL)
				endpoint1 = value;
			else
				endpoint2 = value;
			break;

		/* unknown suboption */
		case -1:
			usage();
			free(dupopts);
			return (SCCONF_EUSAGE);

		/* internal error */
		default:
			rstatus = SCCONF_EUNEXPECTED;
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
			    "%:  Failed to add cluster transport "
			    "cable - %s.\n"), progname, errbuff);
			free(dupopts);
			return (rstatus);
		}
	}

	/* check for required suboptions and initialize endpoints */
	if (endpoint1 == NULL || endpoint2 == NULL ||
	    initialize_endpoint(&epoint1, endpoint1) ||
	    initialize_endpoint(&epoint2, endpoint2)) {
		usage();
		free(dupopts);
		return (SCCONF_EUSAGE);
	}

	/* if just checking for usage, we are done */
	if (Uflag) {
		free(dupopts);
		return (SCCONF_NOERR);
	}

	/* add the cable */
	if (clnt != NULL) {
		rstatus = scrconfcmd_cltr_cable_remote(&epoint1, &epoint2,
		    clnt);
	} else if (clconfig != NULL) {
		rstatus = scrconfcmd_cltr_cable_local(&epoint1, &epoint2,
		    clconfig);
	} else {
		rstatus = SCCONF_EUNEXPECTED;
	}
	if (rstatus != SCCONF_NOERR) {
		switch (rstatus) {
		case SCCONF_EINUSE:
			(void) sprintf(errbuff, gettext(
			    "one of the ports is already in use"));
			break;

		default:
			scconf_strerr(errbuff, rstatus);
			break;
		} /*lint !e788 */
		scconf_strerr(errbuff, rstatus);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to add cluster transport cable - %s.\n"),
		    progname, errbuff);
		free(dupopts);
		return (rstatus);
	}

	free(dupopts);

	return (SCCONF_NOERR);
}

/*
 * scrconfcmd_cable_local
 *
 *	Add a cable to the given "clconfig" structure.
 */
static scconf_errno_t
scrconfcmd_cltr_cable_local(scconf_cltr_epoint_t *epoint1,
    scconf_cltr_epoint_t *epoint2, scconf_cfg_cluster_t *clconfig)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_cfg_cable_t *cable = (scconf_cfg_cable_t *)0;
	scconf_cfg_cable_t **nextp;
	scconf_cltr_epoint_t *epoint;
	scconf_cltr_epoint_t *e;

	/* Check arguments */
	if (epoint1 == NULL ||
	    epoint2 == NULL ||
	    clconfig == NULL ||
	    scconf_clconfig_infrastructure(clconfig) != SCCONF_NOERR)
		return (SCCONF_EINVAL);

	/* Allocate struct for the next cable */
	cable = (scconf_cfg_cable_t *)calloc(1,
	    sizeof (scconf_cfg_cable_t));
	if (cable == (scconf_cfg_cable_t *)0) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* Dup endpoint data */
	e = &cable->scconf_cable_epoint1;
	epoint = epoint1;
	for (;;) {
		/* type */
		e->scconf_cltr_epoint_type = epoint->scconf_cltr_epoint_type;

		/* nodename */
		if (epoint->scconf_cltr_epoint_nodename) {
			e->scconf_cltr_epoint_nodename = strdup(
			    epoint->scconf_cltr_epoint_nodename);
			if (e->scconf_cltr_epoint_nodename == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* devicename */
		if (epoint->scconf_cltr_epoint_devicename) {
			e->scconf_cltr_epoint_devicename = strdup(
			    epoint->scconf_cltr_epoint_devicename);
			if (e->scconf_cltr_epoint_devicename == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* portname */
		if (epoint->scconf_cltr_epoint_portname) {
			e->scconf_cltr_epoint_portname = strdup(
			    epoint->scconf_cltr_epoint_portname);
			if (e->scconf_cltr_epoint_portname == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* portid and state */
		e->scconf_cltr_epoint_portid =
		    epoint->scconf_cltr_epoint_portid;
		e->scconf_cltr_epoint_portstate =
		    epoint->scconf_cltr_epoint_portstate;

		/* next */
		if (epoint == epoint1) {
			e = &cable->scconf_cable_epoint2;
			epoint = epoint2;
		} else {
			break;
		}
	}

	/* Find location of the last pointer, and set it */
	for (nextp = &clconfig->scconf_cluster_cablelist;  *nextp;
	    nextp = &(*nextp)->scconf_cable_next)
		;
	*nextp = cable;

	/* And, check our configuration again */
	rstatus = scconf_clconfig_infrastructure(clconfig);
	if (rstatus != SCCONF_NOERR) {
		*nextp = (scconf_cfg_cable_t *)0;
		goto cleanup;
	}

cleanup:
	if (rstatus != SCCONF_NOERR && cable) {
		e = &cable->scconf_cable_epoint1;
		epoint = epoint1;
		for (;;) {
			if (e->scconf_cltr_epoint_nodename)
				free(e->scconf_cltr_epoint_nodename);
			if (e->scconf_cltr_epoint_devicename)
				free(e->scconf_cltr_epoint_devicename);
			if (e->scconf_cltr_epoint_portname)
				free(e->scconf_cltr_epoint_portname);

			/* next */
			if (epoint == epoint1) {
				e = &cable->scconf_cable_epoint2;
				epoint = epoint2;
			} else {
				break;
			}
		}
		free(cable);
	}

	return (rstatus);
}

/*
 * scrconfcmd_cable_remote
	 *
	 *	Add a cable to the configuration on the given "clnt".
 */
static scconf_errno_t
scrconfcmd_cltr_cable_remote(scconf_cltr_epoint_t *epoint1,
    scconf_cltr_epoint_t *epoint2, CLIENT *clnt)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	enum clnt_stat clnt_st;
	sc_result clnt_res;

	/* initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Check arguments */
	if (epoint1 == NULL ||
	    epoint2 == NULL ||
	    clnt == NULL)
		return (SCCONF_EINVAL);

	/* Call the RPC server */
	clnt_st = scadmproc_add_cltr_cable_1(
	    (sc_cltr_epoint_t *)epoint1,
	    (sc_cltr_epoint_t *)epoint2, &clnt_res, clnt);
	if (clnt_st != RPC_SUCCESS)
		clnt_perror(clnt, progname);

	/* If err-ed, return error */
	if (clnt_st != RPC_SUCCESS) {
		switch (clnt_st) {
		case RPC_TIMEDOUT:
			return (SCCONF_ETIMEDOUT);

		case RPC_AUTHERROR:
			return (SCCONF_EAUTH);

		case RPC_CANTRECV:
			return (SCCONF_ESTALE);

		default:
			return (SCCONF_EUNEXPECTED);
		} /*lint !e788 */
	}

	/* Return result of add_cltr_adapter operation */
	rstatus = (scconf_errno_t)clnt_res.sc_res_errno;

	return (rstatus);
}

/*
 * initialize_endpoint
 *
 *	Initialize endpoint structure.  The "endpoint" string is modified.
 *
 *	Return zero upon successful.
 */
static int
initialize_endpoint(scconf_cltr_epoint_t *epoint, char *endpoint)
{
	char *node, *name, *port;

	node = NULL;
	if (epoint == NULL)
		return (1);

	bzero(epoint, sizeof (scconf_cltr_epoint_t));

	/* isolate the node name from the device name */
	name = strchr(endpoint, ':');
	if (name != NULL) {
		node = endpoint;
		*name++ = '\0';
		if (*name == '\0' || strchr(name, ':'))
			return (1);
	}
	if (name == NULL) {
		node = NULL;
		name = endpoint;
	}

	/* isolate the device name from the port */
	port = strchr(name, '@');
	if (port) {
		*port++ = '\0';
		if (*port == '\0' || strchr(port, '@'))
			return (1);
	}

	epoint->scconf_cltr_epoint_type = (node == NULL) ?
	    SCCONF_CLTR_EPOINT_TYPE_CPOINT :
	    SCCONF_CLTR_EPOINT_TYPE_ADAPTER;
	epoint->scconf_cltr_epoint_nodename = node;
	epoint->scconf_cltr_epoint_devicename = name;
	epoint->scconf_cltr_epoint_portname = port;

	return (0);
}

/*
 * scrconfcmd_set_netaddr
 *
 *	Set the cluster transport network number.
 */
static scconf_errno_t
scrconfcmd_set_netaddr(char *subopts, scconf_cfg_cluster_t *clconfig,
    uint_t Uflag)
{
	scconf_errno_t rstatus;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;
	struct netent net_ent;
	struct netent *net_entp;
	char buffer[BUFSIZ];
	struct in_addr in;

	char *knownopts[] = {
		"netaddr",			/* 0 - net address */
		"netmask",			/* 1 - netmask */
		"maxnodes",
		"maxprivatenets",
		"numvirtualclusters",
		(char *)0
	};
	char *snetaddr = NULL;
	ulong_t netaddr = 0;
	char *snetmask = NULL;
	char *maxnodes = NULL, *maxprivatenets = NULL;
	char *numvirtualclusters = NULL;

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		scconf_strerr(errbuff, rstatus);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to set netaddr for cluster transport - %s.\n"),
		    progname, errbuff);
		return (rstatus);
	}

	current = dupopts;
	while (*current) {
		switch (getsubopt(&current, knownopts, &value)) {

		/* 0 - net address */
		case 0:
			if (value == NULL || snetaddr != NULL) {
				usage();
				free(dupopts);
				return (SCCONF_EUSAGE);
			}
			snetaddr = value;
			break;

		/* 1 - netmask */
		case 1:
			if (value == NULL || snetmask != NULL) {
				usage();
				free(dupopts);
				return (SCCONF_EUSAGE);
			}
			snetmask = value;
			break;

		/* 2 - maxnodes */
		case 2:
			if (value == NULL || maxnodes != NULL) {
				usage();
				free(dupopts);
				return (SCCONF_EUSAGE);
			}
			maxnodes = value;
			break;

		/* 3 - maxprivatenets */
		case 3:
			if (value == NULL || maxprivatenets != NULL) {
				usage();
				free(dupopts);
				return (SCCONF_EUSAGE);
			}
			maxprivatenets = value;
			break;

		/* 4 - virtualclusters */
		case 4:
			if (value == NULL || numvirtualclusters != NULL) {
				usage();
				free(dupopts);
				return (SCCONF_EUSAGE);
			}
			numvirtualclusters = value;
			break;

		/* unknown suboption */
		case -1:
			usage();
			free(dupopts);
			return (SCCONF_EUSAGE);

		/* internal error */
		default:
			rstatus = SCCONF_EUNEXPECTED;
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to set netaddr for cluster "
			    "transport - %s.\n"),
			    progname, errbuff);
			free(dupopts);
			return (rstatus);
		}
	}

	/* check for required suboptions */
	if (snetaddr == NULL) {
		usage();
		free(dupopts);
		return (SCCONF_EUSAGE);
	}

	/* convert the netaddr string to a long;  a network name is okay */
	if ((netaddr = inet_network(snetaddr)) == (in_addr_t)-1) {
		net_entp = getnetbyname_r(snetaddr, &net_ent, buffer,
		    sizeof (buffer));
		if (net_entp == (struct netent *)0) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to set cluster transport "
			    "network number - unknown network name.\n"),
			    progname);
			free(dupopts);
			return (SCCONF_EINVAL);
		}
		netaddr = net_entp->n_net;
	}

	/* if netmask was given */
	if (snetmask != NULL) {

		/* convert the netmask string to a long */
		if (inet_network(snetmask) == (in_addr_t)-1) {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to set cluster transport "
			    "netmask - dot notation must be used.\n"),
			    progname);
			free(dupopts);
			return (SCCONF_EINVAL);
		}
	}

	/* if just checking for usage, we are done */
	if (Uflag) {
		free(dupopts);
		return (SCCONF_NOERR);
	}

	/* set the network number */
	in.s_addr = htonl(netaddr);
	rstatus = scrconfcmd_set_netaddr_local(inet_ntoa(in), snetmask,
		maxnodes, maxprivatenets, numvirtualclusters, clconfig);
	if (rstatus != SCCONF_NOERR) {
		scconf_strerr(errbuff, rstatus);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to set cluster transport "
		    "network number - %s.\n"), progname, errbuff);
		free(dupopts);
		return (rstatus);
	}

	free(dupopts);

	return (SCCONF_NOERR);
}

/*
 * scrconfcmd_set_netaddr_local
 *
 *	Set the cluster transport network number in the "clconfig"
 */
static scconf_errno_t
scrconfcmd_set_netaddr_local(char *snetaddr, char *snetmask, char *maxnodes,
	char *maxprivatenets, char *vclusters, scconf_cfg_cluster_t *clconfig)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char buf[BUFSIZ];
	uint_t nodes, privnets, given_privnets, virtualclusters;
	uint_t actualnodes, actualprivnets, total_subnets;
	uint_t subnet_addrs, pernode_addrs, total_addrs, physical_cluster_addrs;
	in_addr_t netnum, netmask, dflt_netmask;
	in_addr_t subnet_netmask, pernode_netmask, cluster_netmask;
	in_addr_t pernode_netnum;
	int pernode_host_bits;
	uint_t num;
	struct in_addr in;

	char subnetmask[16], usernetaddr[16], usernetmask[16];
	char netmaskstring[16], cluster_netmask_string[16];

	/* Check arguments */
	if (snetaddr == NULL ||
	    clconfig == NULL ||
	    clconfig->scconf_cluster_privnetaddr != NULL ||
	    clconfig->scconf_cluster_privnetmask != NULL ||
	    clconfig->scconf_cluster_clusternetmask != NULL ||
	    scconf_clconfig_infrastructure(clconfig) != SCCONF_NOERR)
		return (SCCONF_EINVAL);

	/* Net address */
	clconfig->scconf_cluster_privnetaddr = strdup(snetaddr);
	if (clconfig->scconf_cluster_privnetaddr == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	netnum = inet_addr(snetaddr);
	netnum = ntohl(netnum);

	/* Zone Clusters */
	clconfig->scconf_cluster_zoneclusters = strdup(vclusters);
	if (clconfig->scconf_cluster_zoneclusters == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/*
	 * First check if the maxnodes and maxprivatenets arguments
	 * are given. If they are, calculate the cluster netmask
	 * for the given values + 2 to deal with the all 0's and
	 * all 1's cases. Then if a netmask has also been given as
	 * an argument, check if that netmask is big enough for
	 * the given number of maxnodes and maxprivatenets. If not,
	 * return appropriate error.
	 *
	 * It is possible that the maxnodes and maxprivatenets are not
	 * given. In this case, check for the netmask. Since maxnodes
	 * and maxprivatenets are not given, the netmask must be the
	 * default or bigger than the default. If the netmask is also
	 * not given, assume the default netmask. For this case, assume
	 * the default maxnodes and maxprivatenets values.
	 *
	 */
	if ((maxnodes != NULL) && (maxprivatenets != NULL)) {
		nodes = (uint_t)atoi(maxnodes);
		given_privnets = privnets = (uint_t)atoi(maxprivatenets);
		virtualclusters = (uint_t)atoi(vclusters);

		if (nodes < SCCONF_MIN_MAXNODES ||
			nodes > SCCONF_MAX_MAXNODES) {
			(void) fprintf(stderr, gettext("%s:  Number of nodes"
			    " must be between %d and %d.\n"),
			    progname, SCCONF_MIN_MAXNODES, SCCONF_MAX_MAXNODES);
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		if (privnets < SCCONF_MIN_MAXPRIVNETS ||
			privnets > SCCONF_MAX_MAXPRIVNETS) {
			(void) fprintf(stderr, gettext("%s:  Number of private"
			    " networks must be between %d and %d.\n"),
			    progname,
			    SCCONF_MIN_MAXPRIVNETS, SCCONF_MAX_MAXPRIVNETS);
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/* Add 2 to both to handle the all 0's and all 1's case */
		nodes += 2;
		privnets += 2;
		/* Determine subnet netmask based on maxnodes */
		subnet_addrs = nodes;
		num = 1;
		subnet_netmask = (in_addr_t)0xFFFFFFFF;
		while (num < subnet_addrs) {
			num = num << 1;
			subnet_netmask = subnet_netmask << 1;
		}
		in = inet_makeaddr(subnet_netmask, 0);
		(void) strcpy(subnetmask, inet_ntoa(in));
		if (subnetmask != NULL) {
			clconfig->scconf_cluster_privsubnetmask =
				strdup(subnetmask);
			if (clconfig->scconf_cluster_privsubnetmask == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/*
		 * Actual nodes supported by subnet_netmask is the
		 * total number of subnet addresses minus 2 to exclude
		 * the first and last addresses
		 */
		actualnodes = (~subnet_netmask + 1) - 2;

		/*
		 * If the cluster netmask is not set, calculate it here.
		 * If its set, ensure that its enough to accommodate the
		 * given number of nodes and adapters.
		 */
		if (snetmask == NULL) {
			/*
			 * virtualclusters will accomodate subnets
			 * needed for virtual clusters. The default
			 * value is 12
			 */
			total_addrs = ((4 * (privnets) / 3) +
			    virtualclusters) * (~subnet_netmask + 1);
			num = 1;
			cluster_netmask = (in_addr_t)0xFFFFFFFF;
			while (num < total_addrs) {
				num = num << 1;
				cluster_netmask = cluster_netmask << 1;
			}
			in = inet_makeaddr(cluster_netmask, 0);
			(void) strcpy(cluster_netmask_string, inet_ntoa(in));

			subnet_addrs = (~subnet_netmask + 1);

			if (virtualclusters == 0) {
				/* No virtual clusters */
				(void) strcpy(netmaskstring,
				    cluster_netmask_string);
			} else {
				/* Calculate the physical cluster netmask */
				physical_cluster_addrs = (4 * (privnets) / 3) *
				    (~subnet_netmask + 1);

				num = 1;
				netmask = (in_addr_t)0xFFFFFFFF;
				while (num < physical_cluster_addrs) {
					num = num << 1;
					netmask = netmask << 1;
				}
				in = inet_makeaddr(netmask, 0);
				(void) strcpy(netmaskstring, inet_ntoa(in));

				/*
				 * Actual privnets is the 3/4 of total subnets
				 * excluding the first and last subnet
				 */
				physical_cluster_addrs = (~netmask + 1);
				total_subnets =
					physical_cluster_addrs / subnet_addrs;
				actualprivnets = ((3 * total_subnets) / 4) - 2;
			}
		} else {
			/*
			 * Find number of private networks in this
			 * netmask and compare against given maxprivnets
			 * If given maxprivnets is greater, return with
			 * error since given netmask will not accommodate
			 * the require privnets.
			 */
			cluster_netmask = inet_addr(snetmask);
			cluster_netmask = ntohl(cluster_netmask);
			(void) strcpy(cluster_netmask_string, snetmask);

			/*
			 * Actual privnets is the 3/4 of total subnets
			 * excluding the first and last subnet
			 */
			total_addrs = (~cluster_netmask + 1);
			subnet_addrs = (~subnet_netmask + 1);
			total_subnets = total_addrs / subnet_addrs;

			/*
			 * If the virtualclusters is set, then this
			 * should include these subnets too. Check
			 * whether this accomodate the given_privnets
			 * and virtual clusters.
			 */
			if (virtualclusters != 0) {
				if ((given_privnets + virtualclusters) >
				    total_subnets) {
					(void) fprintf(stderr, gettext
					    ("%s:  Given"
					    " netmask cannot accommodate given "
					    "number of nodes and private "
					    "networks "
					    "and virtual clusters.\n"),
					    progname);
					rstatus = SCCONF_EINVAL;
					goto cleanup;
				}
				/* Calculate the physical cluster netmask */
				physical_cluster_addrs = (4 * (privnets) / 3) *
					(~subnet_netmask + 1);

				num = 1;
				netmask = (in_addr_t)0xFFFFFFFF;
				while (num < physical_cluster_addrs) {
					num = num << 1;
					netmask = netmask << 1;
				}
				in = inet_makeaddr(netmask, 0);
				(void) strcpy(netmaskstring, inet_ntoa(in));

				/*
				 * Actual privnets is the 3/4 of total subnets
				 * excluding the first and last subnet
				 */
				physical_cluster_addrs = (~netmask + 1);
				total_subnets =
					physical_cluster_addrs / subnet_addrs;
				actualprivnets = ((3 * total_subnets) / 4) - 2;
			} else {
				/* No virtual clusters */
				(void) strcpy(netmaskstring,
				    cluster_netmask_string);
				actualprivnets = ((3 * total_subnets) / 4) - 2;

				if (actualprivnets < given_privnets) {
					(void) fprintf(stderr,
					    gettext("%s:  Given"
					    " netmask cannot accommodate given "
					    " number of nodes and private "
					    " networks.\n"),
					    progname);
					rstatus = SCCONF_EINVAL;
					goto cleanup;
				}
			}
		}

		/*
		 * If virtualcluster is set, then write the clusternetmask
		 * to ccr. This clusternetmask accomodates the subnets
		 * required the regular physical cluster needs as well
		 * as given virtual cluster needs.
		 */
		if (virtualclusters != 0) {
			clconfig->scconf_cluster_clusternetmask =
			    strdup(cluster_netmask_string);
			if (clconfig->scconf_cluster_clusternetmask == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		clconfig->scconf_cluster_privnetmask = strdup(netmaskstring);
		if (clconfig->scconf_cluster_privnetmask == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/*
		 * Write the actual values for maxnodes and maxprivnet,
		 * not the ones given as input.
		 */

		if (actualnodes > SCCONF_MAX_MAXNODES)
			actualnodes = SCCONF_MAX_MAXNODES;
		(void) sprintf(buf, "%d", actualnodes);
		clconfig->scconf_cluster_maxnodes = strdup(buf);
		if (clconfig->scconf_cluster_maxnodes == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		if (actualprivnets > SCCONF_MAX_MAXPRIVNETS)
			actualprivnets = SCCONF_MAX_MAXPRIVNETS;
		(void) sprintf(buf, "%d", actualprivnets);
		clconfig->scconf_cluster_maxprivnets = strdup(buf);
		if (clconfig->scconf_cluster_maxprivnets == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		/*
		 * Determine the the pernode netmask and the pernode
		 * netaddr
		 */
		total_addrs = (~netmask + 1);
		subnet_addrs = (~subnet_netmask + 1);
		total_subnets = total_addrs / subnet_addrs;

		pernode_addrs = (total_subnets/4) * subnet_addrs;
		num = 1;
		pernode_netmask = (in_addr_t)0xFFFFFFFF;
		pernode_host_bits = 0;
		while (num < pernode_addrs) {
			num = num << 1;
			pernode_netmask = pernode_netmask << 1;
			pernode_host_bits++;
		}
		in = inet_makeaddr(pernode_netmask, 0);
		(void) strcpy(usernetmask, inet_ntoa(in));

		pernode_netnum = (netnum | (2 << pernode_host_bits));
		in = inet_makeaddr(pernode_netnum, 0);
		(void) strcpy(usernetaddr, inet_ntoa(in));

		/* User Net address */
		if (usernetaddr != NULL) {
			clconfig->scconf_cluster_privusernetnum =
				strdup(usernetaddr);
			if (clconfig->scconf_cluster_privusernetnum == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}

		/* User Net mask */
		if (usernetmask != NULL) {
			clconfig->scconf_cluster_privusernetmask =
				strdup(usernetmask);
			if (clconfig->scconf_cluster_privusernetmask == NULL) {
				rstatus = SCCONF_ENOMEM;
				goto cleanup;
			}
		}
	} else {
		/*
		 * Check if either of maxnodes or maxprivnets is
		 * given. If so, return error.
		 */
		if ((maxnodes != NULL) || (maxprivatenets != NULL)) {
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		/*
		 * Use default values of maxnodes and maxprivnets.
		 * Note that if the maxnodes and maxprivnets are not
		 * given, and if netmask is not given, assume default
		 * netmask.
		 */
		if (snetmask == NULL)
			netmask = inet_addr(SCCONF_DEFAULT_NETMASK_STRING);
		else
			netmask = inet_addr(snetmask);

		/*
		 * Compare the input netmask with the default. If the
		 * integer value of the input netmask is bigger than
		 * the default netmask, the input netmask is smaller
		 * that the default, therefore return error.
		 */
		dflt_netmask = inet_addr(SCCONF_DEFAULT_NETMASK_STRING);
		if (netmask > dflt_netmask) {
			(void) fprintf(stderr, gettext("%s:  Netmask"
			    " has to be default or bigger since the maxnodes"
			    " and maxprivnets arguments are not given.\n"),
			    progname);
			rstatus = SCCONF_EINVAL;
			goto cleanup;
		}

		if (snetmask == NULL)
			clconfig->scconf_cluster_privnetmask =
				strdup(SCCONF_DEFAULT_NETMASK_STRING);
		else
			clconfig->scconf_cluster_privnetmask =
				strdup(snetmask);
		if (clconfig->scconf_cluster_privnetmask == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		clconfig->scconf_cluster_privsubnetmask =
			strdup(SCCONF_DEFAULT_SUBNETMASK_STRING);
		if (clconfig->scconf_cluster_privsubnetmask == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		clconfig->scconf_cluster_maxnodes =
			strdup(SCCONF_DEFAULT_MAXNODES_STRING);
		if (clconfig->scconf_cluster_maxnodes == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		clconfig->scconf_cluster_maxprivnets =
			strdup(SCCONF_DEFAULT_MAXPRIVNETS_STRING);
		if (clconfig->scconf_cluster_maxprivnets == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		clconfig->scconf_cluster_privusernetnum =
			strdup(SCCONF_DEFAULT_USER_NET_NUM_STRING);
		if (clconfig->scconf_cluster_privusernetnum == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}

		clconfig->scconf_cluster_privusernetmask =
			strdup(SCCONF_DEFAULT_USER_NETMASK_STRING);
		if (clconfig->scconf_cluster_privusernetmask == NULL) {
			rstatus = SCCONF_ENOMEM;
			goto cleanup;
		}
	}


	/* And, check our configuration again */
	rstatus = scconf_clconfig_infrastructure(clconfig);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

cleanup:
	if (rstatus != SCCONF_NOERR) {
		if (clconfig->scconf_cluster_privnetaddr) {
			free(clconfig->scconf_cluster_privnetaddr);
			clconfig->scconf_cluster_privnetaddr = NULL;
		}
		if (clconfig->scconf_cluster_privnetmask) {
			free(clconfig->scconf_cluster_privnetmask);
			clconfig->scconf_cluster_privnetmask = NULL;
		}
		if (clconfig->scconf_cluster_privsubnetmask) {
			free(clconfig->scconf_cluster_privsubnetmask);
			clconfig->scconf_cluster_privsubnetmask = NULL;
		}
		if (clconfig->scconf_cluster_privusernetnum) {
			free(clconfig->scconf_cluster_privusernetnum);
			clconfig->scconf_cluster_privusernetnum = NULL;
		}
		if (clconfig->scconf_cluster_privusernetmask) {
			free(clconfig->scconf_cluster_privusernetmask);
			clconfig->scconf_cluster_privusernetmask = NULL;
		}
		if (clconfig->scconf_cluster_maxnodes) {
			free(clconfig->scconf_cluster_maxnodes);
			clconfig->scconf_cluster_maxnodes = NULL;
		}
		if (clconfig->scconf_cluster_maxprivnets) {
			free(clconfig->scconf_cluster_maxprivnets);
			clconfig->scconf_cluster_maxprivnets = NULL;
		}
		if (clconfig->scconf_cluster_clusternetmask) {
			free(clconfig->scconf_cluster_clusternetmask);
			clconfig->scconf_cluster_clusternetmask = NULL;
		}
		if (clconfig->scconf_cluster_zoneclusters) {
			free(clconfig->scconf_cluster_zoneclusters);
			clconfig->scconf_cluster_zoneclusters = NULL;
		}
	}

	return (rstatus);
}

/*
 * scconfcmd_set_authentication
 *
 *	Set authentication options
 */
static scconf_errno_t
scrconfcmd_set_authentication(char *subopts, scconf_cfg_cluster_t *clconfig,
    uint_t Uflag)
{
	scconf_errno_t rstatus;
	char errbuff[BUFSIZ];
	char *value;
	char *current, *dupopts = (char *)0;

	char *knownopts[] = {
		"authtype",			/* 0 - authentication type */
		"node",				/* 1 - node name to add */
		(char *)0
	};
	char *sauthtype = NULL;
	scconf_authtype_t authtype;
	char *addnode = NULL;

	/* If we are not just checking usage, make sure clconfig is set */
	if (!Uflag && clconfig == NULL)
		return (SCCONF_EINVAL);

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		rstatus = SCCONF_ENOMEM;
		scconf_strerr(errbuff, rstatus);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to update authentication - %s.\n"),
		    progname, errbuff);
		return (rstatus);
	}

	current = dupopts;
	while (*current) {
		switch (getsubopt(&current, knownopts, &value)) {

		/* 0 - authentication */
		case 0:
			if (value == NULL || sauthtype != NULL) {
				usage();
				free(dupopts);
				return (SCCONF_EUSAGE);
			}
			sauthtype = value;
			break;

		/* 1 - node name to add */
		case 1:
			if (value == NULL) {
				usage();
				free(dupopts);
				return (SCCONF_EUSAGE);
			}

			/* add the node to the list */
			addnode = value;
			if (Uflag)
				break;
			rstatus = scrconfcmd_addto_secure_joinlist_local(
			    addnode, clconfig);
			if (rstatus != SCCONF_NOERR) {
				scconf_strerr(errbuff, rstatus);
				(void) fprintf(stderr, gettext(
				    "%s:  Failed to update "
				    "authentication - %s.\n"),
				    progname, errbuff);
				free(dupopts);
				return (rstatus);
			}

			break;

		/* unknown suboption */
		case -1:
			usage();
			free(dupopts);
			return (SCCONF_EUSAGE);

		/* internal error */
		default:
			rstatus = SCCONF_EUNEXPECTED;
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to update authentication - %s.\n"),
			    progname, errbuff);
			free(dupopts);
			return (rstatus);
		}
	}

	/* check for required suboptions */
	if (addnode == NULL && sauthtype == NULL) {
		usage();
		free(dupopts);
		return (SCCONF_EUSAGE);
	}

	/* if just checking for usage, we are done */
	if (Uflag) {
		free(dupopts);
		return (SCCONF_NOERR);
	}

	/* set authentication type, if requested */
	if (sauthtype != NULL) {
		if (strcmp(sauthtype, "sys") == 0)
			authtype = SCCONF_AUTH_SYS;
		else if (strcmp(sauthtype, "unix") == 0)
			authtype = SCCONF_AUTH_SYS;
		else if (strcmp(sauthtype, "des") == 0)
			authtype = SCCONF_AUTH_DH;
		else {
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to update authentication"
			    " - unknown authtype\n"),
			    progname);
			free(dupopts);
			return (SCCONF_EINVAL);
		}

		rstatus = scrconfcmd_set_secure_authtype_local(authtype,
		    clconfig);
		if (rstatus != SCCONF_NOERR) {
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
			    "%s:  Failed to update authentication - %s.\n"),
			    progname, errbuff);
			free(dupopts);
			return (rstatus);
		}
	}

	free(dupopts);

	return (SCCONF_NOERR);
}

/*
 * scrconfcmd_addto_secure_joinlist_local
 *
 *	Set the cluster authenticaion type in "clconfig"
 */
static scconf_errno_t
scrconfcmd_addto_secure_joinlist_local(char *addnode,
    scconf_cfg_cluster_t *clconfig)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	scconf_namelist_t **namelistp = (scconf_namelist_t **)0;
	char errbuff[BUFSIZ];

	/* Check arguments */
	if (addnode == NULL ||
	    clconfig == NULL ||
	    scconf_clconfig_infrastructure(clconfig) != SCCONF_NOERR)
		return (SCCONF_EINVAL);

	/* Get the authentication list */
	namelistp = &clconfig->scconf_cluster_authlist;

	/* Make sure that there is only one "." in the list */
	if (*namelistp != NULL &&
	    ((*namelistp)->scconf_namelist_name != NULL &&
	    (strcmp((*namelistp)->scconf_namelist_name, ".") == 0) ||
	    strcmp(addnode, ".") == 0)) {
		rstatus = SCCONF_EUSAGE;
		scconf_strerr(errbuff, rstatus);
		(void) fprintf(stderr, gettext(
		    "%s:  Failed to update authentication - %s.\n"),
		    progname, errbuff);
		(void) fprintf(stderr, gettext(
		    "%s:  No additional nodes allowed with \"%s\".\n"),
		    progname, ".");
		return (rstatus);
	}

	/* Find the end of the list */
	while (*namelistp)
		namelistp = &(*namelistp)->scconf_namelist_next;

	/* Add the node to the list */
	*namelistp = (scconf_namelist_t *)calloc(1, sizeof (scconf_namelist_t));
	if (*namelistp == NULL)
		return (SCCONF_ENOMEM);
	(*namelistp)->scconf_namelist_name = strdup(addnode);
	if ((*namelistp)->scconf_namelist_name == NULL) {
		rstatus = SCCONF_ENOMEM;
		goto cleanup;
	}

	/* And, check our configuration again */
	rstatus = scconf_clconfig_infrastructure(clconfig);
	if (rstatus != SCCONF_NOERR)
		goto cleanup;

cleanup:
	if (rstatus != SCCONF_NOERR) {
		if (*namelistp) {
			scconf_free_namelist(*namelistp);
			*namelistp = NULL;
		}
	}

	return (rstatus);
}

/*
 * scrconfcmd_set_secure_authtype_local
 *
 *	Set the cluster authenticaion type in "clconfig"
 */
static scconf_errno_t
scrconfcmd_set_secure_authtype_local(scconf_authtype_t authtype,
    scconf_cfg_cluster_t *clconfig)
{
	/* Check arguments */
	if (clconfig == NULL ||
	    scconf_clconfig_infrastructure(clconfig) != SCCONF_NOERR)
		return (SCCONF_EINVAL);

	/* Set the auth type */
	clconfig->scconf_cluster_authtype = authtype;

	/* And, check our configuration again */
	return (scconf_clconfig_infrastructure(clconfig));
}

/*
 * scrconfcmd_get_network_adapters
 *
 *	Return the list of network adapters in the given "namelist".
 */
static scconf_errno_t
scrconfcmd_get_network_adapters(scadmin_namelist_t **nlp, uint_t flag,
    char *adaptypes, CLIENT *clnt)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	sc_result_adapters clnt_res;
	char **array;
	uint_t count;
	enum clnt_stat clnt_st;

	if (clnt) {
		/* initialize clnt_res */
		bzero(&clnt_res, sizeof (clnt_res));

		/* Call the RPC server */
		clnt_st = scadmproc_get_network_adapters_1(flag, adaptypes,
		    &clnt_res, clnt);

		/* If err-ed, return error */
		if (clnt_st != RPC_SUCCESS) {
			switch (clnt_st) {
			case RPC_TIMEDOUT:
				return (SCCONF_ETIMEDOUT);

			case RPC_AUTHERROR:
				return (SCCONF_EAUTH);

			case RPC_CANTRECV:
				return (SCCONF_ESTALE);

			default:
				return (SCCONF_EUNEXPECTED);
			} /*lint !e788 */
		}

		/* Get the name list */
		array = clnt_res.sc_res_adapters.sc_namelist_t_val;
		count = clnt_res.sc_res_adapters.sc_namelist_t_len;
		if (array) {
			*nlp = scrconf_array_to_scadm_namelist(array,
			    count);
			scrconf_free_array(array, count);
		}
	} else {
		*nlp = scadmin_get_network_adapters(flag, adaptypes);
	}

	return (rstatus);
}

/*
 * scrconfcmd_get_transport_config
 *
 *	Return the transport config in the given "namelist".
 */
static scconf_errno_t
scrconfcmd_get_transport_config(scadmin_namelist_t **nlp, CLIENT *clnt)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	sc_result_trconfig clnt_res;
	char **array;
	uint_t count;
	enum clnt_stat clnt_st;

	if (clnt) {
		/* initialize clnt_res */
		bzero(&clnt_res, sizeof (clnt_res));

		/* Call the RPC server */
		clnt_st = scadmproc_transport_config_1(&clnt_res, clnt);

		/* If err-ed, return error */
		if (clnt_st != RPC_SUCCESS) {
			switch (clnt_st) {
			case RPC_TIMEDOUT:
				return (SCCONF_ETIMEDOUT);

			case RPC_AUTHERROR:
				return (SCCONF_EAUTH);

			case RPC_CANTRECV:
				return (SCCONF_ESTALE);

			default:
				return (SCCONF_EUNEXPECTED);
			} /*lint !e788 */
		}

		/* Return result of libscconf lookup */
		rstatus = (scconf_errno_t)clnt_res.sc_res_errno;

		/* Get the name list */
		array = clnt_res.sc_res_trconfig.sc_namelist_t_val;
		count = clnt_res.sc_res_trconfig.sc_namelist_t_len;
		if (array) {
			if (rstatus == SCCONF_NOERR) {
				*nlp = scrconf_array_to_scadm_namelist(array,
				    count);
			}
			scrconf_free_array(array, count);
		}
	} else {
		rstatus = SCCONF_EUSAGE;
	}

	return (rstatus);
}

/*
 * scrconfcmd_broadcast_ping
 *
 *	Send a broadcast ping.
 */
static scconf_errno_t
scrconfcmd_broadcast_ping(CLIENT *clnt)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	enum clnt_stat clnt_st;

	if (clnt) {
		/* Call the RPC server */
		clnt_st = scadmproc_broadcast_ping_1(NULL, clnt);

		/* If err-ed, return error */
		if (clnt_st != RPC_SUCCESS) {
			switch (clnt_st) {
			case RPC_TIMEDOUT:
				return (SCCONF_ETIMEDOUT);

			case RPC_AUTHERROR:
				return (SCCONF_EAUTH);

			case RPC_CANTRECV:
				return (SCCONF_ESTALE);

			default:
				return (SCCONF_EUNEXPECTED);
			} /*lint !e788 */
		}
	} else {
		scadmin_broadcast_ping();
	}

	return (rstatus);
}

/*
 * scrconfcmd_snoop
 *
 *	Return a count of packets found on each adapter in the adapter
 *	list over the "recvto" period.
 */
static scconf_errno_t
scrconfcmd_snoop(char *adapters, scadmin_namelist_t **nlp,
    uint_t discover_options, uint_t recvto, scadmin_namelist_t **errsp,
    CLIENT *clnt)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	int result;
	sc_result_snoop clnt_res;
	char **array;
	uint_t count;
	scadmin_namelist_t *namelist;
	enum clnt_stat clnt_st;

	if (clnt) {
		/* initialize clnt_res */
		bzero(&clnt_res, sizeof (clnt_res));

		/* Call the RPC server */
		clnt_st = scadmproc_snoop_1(adapters, discover_options,
		    recvto, &clnt_res, clnt);

		/* If err-ed, return error */
		if (clnt_st != RPC_SUCCESS) {
			switch (clnt_st) {
			case RPC_TIMEDOUT:
				return (SCCONF_ETIMEDOUT);

			case RPC_AUTHERROR:
				return (SCCONF_EAUTH);

			case RPC_CANTRECV:
				return (SCCONF_ESTALE);

			default:
				return (SCCONF_EUNEXPECTED);
			} /*lint !e788 */
		}
		/* If not zero, do not return name list */
		result = (scconf_errno_t)clnt_res.sc_res_result;

		/* Get the name list */
		array = clnt_res.sc_res_discovered.sc_namelist_t_val;
		count = clnt_res.sc_res_discovered.sc_namelist_t_len;
		if (array) {
			if (!result) {
				*nlp = scrconf_array_to_scadm_namelist(array,
				    count);
			}
			scrconf_free_array(array, count);
		}

		/* Get the list of errors */
		array = clnt_res.sc_res_errlist.sc_namelist_t_val;
		count = clnt_res.sc_res_errlist.sc_namelist_t_len;
		if (array) {
			*errsp = scrconf_array_to_scadm_namelist(array, count);
			scrconf_free_array(array, count);
		}
	} else {
		namelist = (scadmin_namelist_t *)0;
		result = (int)scadmin_snoop(adapters, &namelist,
		    discover_options, recvto, errsp);
		if (result)
			scadmin_free_namelist(namelist);
		else
			*nlp = namelist;
	}

	return (rstatus);
}

/*
 * scrconfcmd_autodiscover
 *
 *	Autodiscover using the given list of adapters.
 */
static scconf_errno_t
scrconfcmd_autodiscover(char *adapters, char *vlans, char *filename,
    scadmin_namelist_t **nlp, uint_t discover_options, uint_t sendcount,
    uint_t recvto, uint_t waitcount, char *token, scadmin_namelist_t **errsp,
    CLIENT *clnt)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	int result;
	sc_result_autodiscover clnt_res;
	char **array;
	uint_t count;
	scadmin_namelist_t *namelist;
	enum clnt_stat clnt_st;

	if (clnt) {
		/* initialize clnt_res */
		bzero(&clnt_res, sizeof (clnt_res));

		/* Call the RPC server */
		clnt_st = scadmproc_autodiscover_1(adapters, vlans, filename,
		    discover_options, sendcount, recvto, waitcount, token,
		    &clnt_res, clnt);

		/* If err-ed, return error */
		if (clnt_st != RPC_SUCCESS) {
			switch (clnt_st) {
			case RPC_TIMEDOUT:
				return (SCCONF_ETIMEDOUT);

			case RPC_AUTHERROR:
				return (SCCONF_EAUTH);

			case RPC_CANTRECV:
				return (SCCONF_ESTALE);

			default:
				return (SCCONF_EUNEXPECTED);
			} /*lint !e788 */
		}
		/* If not zero, do not return name list */
		result = (scconf_errno_t)clnt_res.sc_res_result;

		/* Get the name list */
		array = clnt_res.sc_res_discovered.sc_namelist_t_val;
		count = clnt_res.sc_res_discovered.sc_namelist_t_len;
		if (array) {
			if (!result) {
				*nlp = scrconf_array_to_scadm_namelist(array,
				    count);
			}
			scrconf_free_array(array, count);
		}

		/* Get the list of errors */
		array = clnt_res.sc_res_errlist.sc_namelist_t_val;
		count = clnt_res.sc_res_errlist.sc_namelist_t_len;
		if (array) {
			*errsp = scrconf_array_to_scadm_namelist(array, count);
			scrconf_free_array(array, count);
		}
	} else {
		namelist = (scadmin_namelist_t *)0;
		result = (int)scadmin_autodiscover(adapters, vlans, filename,
		    &namelist, discover_options, sendcount, recvto,
		    waitcount, token, errsp);
		if (result)
			scadmin_free_namelist(namelist);
		else
			*nlp = namelist;
	}

	return (rstatus);
}

/*
 * scrconf_clnt_create
 *
 *	Create RPC client handle.
 */
static scconf_errno_t
scrconf_clnt_create(char *servername, CLIENT **clntp, uint_t silent)
{
	CLIENT *clnt;
	struct rpcent rpc_ent;
	struct rpcent *rpc_entp;
	char buffer[BUFSIZ];
	struct timeval clnt_create_timeout = { SCCONF_CLNT_CREATE_TO, 0 };
	struct timeval clnt_call_timeout   = { SCCONF_CLNT_CALL_TO, 0 };

	/* Check arguments */
	if (servername == NULL || clntp == NULL)
		return (SCCONF_EINVAL);

	/* Initialize the RPC program number, if necessary */
	if (scadmprog == 0) {
		bzero(&rpc_ent, sizeof (rpc_ent));
		if ((rpc_entp = getrpcbyname_r(SCADMPROGNAME, &rpc_ent,
		    buffer, sizeof (buffer))) == NULL)
			scadmprog = SCADMPROG;
		else
			scadmprog = (rpcprog_t)rpc_entp->r_number;
	}

	/* Create the client handle */
	clnt = clnt_create_timed(servername, scadmprog, SCADMVERS,
	    "circuit_v", &clnt_create_timeout);
	if (clnt == (CLIENT *) NULL) {
		if (!silent)
			clnt_pcreateerror(progname);
		return (SCCONF_EUNEXPECTED);
	}

	/* Set the timeout value for clnt_call */
	(void) clnt_control(clnt, CLSET_TIMEOUT, (char *)&clnt_call_timeout);

	*clntp = clnt;

	return (SCCONF_NOERR);
}

/*
 * scrconf_clnt_addcred
 *
 *	Set credentials based on authentication type.
 */
static scconf_errno_t
scrconf_clnt_addcred(CLIENT *clnt, char *servername)
{
	enum clnt_stat clnt_st;
	sc_result_authtype clnt_res;
	char netname[MAXNETNAMELEN + 1];
	char myhostname[MAXHOSTNAMELEN + 1];
	char saved_hostname[MAXHOSTNAMELEN + 1];
	int len;
	uid_t uid = geteuid();
	gid_t gid = getegid();
	gid_t gids[NGRPS];
	AUTH *auth;

	/* initialize clnt_res */
	bzero(&clnt_res, sizeof (clnt_res));

	/* Check arguments */
	if (clnt == NULL)
		return (SCCONF_EINVAL);

	/* Call the RPC server */
	clnt_st = scadmproc_get_authtype_1(&clnt_res, clnt);
	if (clnt_st != RPC_SUCCESS)
		clnt_perror(clnt, progname);

	/* If err-ed, return error */
	if (clnt_st != RPC_SUCCESS) {
		switch (clnt_st) {
		case RPC_TIMEDOUT:
			return (SCCONF_ETIMEDOUT);

		case RPC_AUTHERROR:
			return (SCCONF_EAUTH);

		case RPC_CANTRECV:
			return (SCCONF_ESTALE);

		default:
			return (SCCONF_EUNEXPECTED);
		} /*lint !e788 */
	}

	/* initialize buffers */
	bzero(netname, sizeof (netname));
	bzero(myhostname, sizeof (myhostname));

	switch (clnt_res.sc_res_authtype) {
	case SCCONF_AUTH_SYS:
		if ((len = getgroups(NGRPS, gids)) < 0)
			len = 0;

		/* get my hostname */
		if (gethostname(myhostname, sizeof (myhostname) - 1) == -1) {
			return (SCCONF_EAUTH);
		}

		/* Get the fully qualified hostname if possible */
		bcopy(myhostname, saved_hostname, sizeof (myhostname));
		if (host2netname(netname, myhostname, NULL) == 1) {
			if (netname2host(netname, myhostname,
			    sizeof (myhostname)) != 1) {
				bcopy(saved_hostname, myhostname,
				    sizeof (saved_hostname));
			}
		}

		auth = authsys_create(myhostname, uid, gid, len, gids);

		if (auth == NULL)
			return (SCCONF_EAUTH);

		if (clnt->cl_auth)
			auth_destroy(clnt->cl_auth);
		clnt->cl_auth = auth;

		break;

	case SCCONF_AUTH_DES:
		/* Get the netname from the servername */
		if (host2netname(netname, servername, NULL) != 1) {
			return (SCCONF_EAUTH);
		}

		auth = authdes_seccreate(netname, 60, NULL, NULL);
		if (auth == NULL)
			return (SCCONF_EAUTH);

		if (clnt->cl_auth)
			auth_destroy(clnt->cl_auth);
		clnt->cl_auth = auth;

		break;

	default:
		return (SCCONF_EUNEXPECTED);
	}

	return (SCCONF_NOERR);
}

/*
 * free_proplist
 *
 * Free all memory associated with the given property "list".
 */
static void
free_proplist(scconf_cfg_prop_t *list)
{
	register scconf_cfg_prop_t **listp;
	register scconf_cfg_prop_t *last;

	/* check arg */
	if (list == (scconf_cfg_prop_t *)0)
		return;

	/* free memory for each member of the list */
	listp = &list;
	while (*listp) {
		if ((*listp)->scconf_prop_key)
			free((*listp)->scconf_prop_key);
		if ((*listp)->scconf_prop_value)
			free((*listp)->scconf_prop_value);
		last = *listp;
		*listp = (*listp)->scconf_prop_next;
		free(last);
	}
}

/*
 * is_numeric
 *
 * Verify that the given string is numeric.
 *
 * Possible return values:
 *
 *	0		- the string includes a non-numeric character
 *	1		- the string is numeric
 */
static int
is_numeric(char *string)
{
	char *s;

	for (s = string;  *s;  s++)
		if (!isdigit(*s))
			return (0);

	return (1);
}

/*
 * scrconf_array_to_scadm_namelist
 *
 * This function converts an array of names to an scadmin_namelist.
 */
static scadmin_namelist_t *
scrconf_array_to_scadm_namelist(char **array, uint_t count)
{
	uint_t i;
	scadmin_namelist_t *namelist = (scadmin_namelist_t *)0;

	/* Check args */
	if (array == NULL || count == 0)
		return ((scadmin_namelist_t *)0);

	/* Change from array to namelist */
	for (i = 0; i < count; ++i) {
		if (scadmin_append_namelist(&namelist, array[i])) {
			printf(array[i]);
			scadmin_free_namelist(namelist);
			return ((scadmin_namelist_t *)0);
		}
	}

	return (namelist);
}

/*
 * scrconf_free_array
 *
 * This the given array of "names".
 */
static void
scrconf_free_array(char **array, uint_t count)
{
	uint_t i;

	/* Check args */
	if (array == NULL)
		return;

	/* Free memory */
	for (i = 0; i < count; ++i) {
		if (array[i])
			free(array[i]);
	}
	free(array);
}

/*
 * scrconfcmd_sys
 *
 *      Run one of the following "sys" commands:
 *
 *      cmd=update_hosts                Update /etc/inet/hosts
 *      cmd=update_ntp_conf_cluster     Update /etc/inet/ntp.conf.cluster
 */
/* ARGSUSED */
static scconf_errno_t
scrconfcmd_sys(int argc, char **argv, uint_t Uflag)
{
	char errbuff[BUFSIZ];
	int c;
	uint_t flag;
	char *value;
	char *current, *dupopts = (char *)0;
	char *subopts = NULL;
	char *sponsor_node = NULL;
	CLIENT *clnt = (CLIENT *)0;
	scconf_errno_t rstatus;
	sc_result clnt_res;
	enum clnt_stat clnt_st;

	char *knownopts[] = {
		"cmd", /* 0 - command */
		(char *)0
	};

	char *cmd = NULL;

	/* Usage check is not performed */
	if (Uflag != 0) {
		return (SCCONF_EINVAL);
	}
	/* Get the sponsor node, if needed for remote operation */
	optind = 1;
	while ((c = getopt(argc, argv, "s:N:")) != EOF) {
		switch (c) {
		case 's':
			if (subopts != NULL) {
				usage();
				return (SCCONF_EUSAGE);
			}
			subopts = optarg;
			break;

		case 'N':
			if (sponsor_node != NULL) {
				usage();
				return (SCCONF_EUSAGE);
			}
			sponsor_node = optarg;
			break;

		default:
			usage();
			return (SCCONF_EUSAGE);
			}
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* check for required suboptions */
	if (sponsor_node == NULL) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* Make sure that there subopts were given */
	if (subopts == NULL) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* duplicate subopts */
	if ((dupopts = strdup(subopts)) == NULL) {
		scconf_strerr(errbuff, SCCONF_ENOMEM);
		(void) fprintf(stderr, gettext(
			"%s:  Failed to complete sys command - %s.\n"),
			progname, errbuff);
		return (SCCONF_ENOMEM);
	}

	/* just "cmd" by itself is okay */
	if (strchr(dupopts, ',') == NULL && strchr(dupopts, '=') == NULL) {
		cmd = dupopts;
	} else {
		current = dupopts;
		while (*current) {
			switch (getsubopt(&current, knownopts, &value)) {

			/* 0 - command */
			case 0:
				if (value == NULL || cmd != NULL) {
					usage();
					rstatus = SCCONF_EUSAGE;
					goto cleanup;
				}
				cmd = value;
				break;


			/* unknown suboptions */
			case -1:
				usage();
				rstatus = SCCONF_EUSAGE;
				goto cleanup;

			/* internal error */
			default:
				scconf_strerr(errbuff, SCCONF_EUNEXPECTED);
				(void) fprintf(stderr, gettext(
					"%s:  Failed to complete "
					"sys command - %s.\n"),
					progname, errbuff);
				rstatus = SCCONF_EUNEXPECTED;
				goto cleanup;
			}
		}
	}

	/* check for required suboptions */
	if (cmd == NULL) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* Find command */
	flag = 0;
	if (strcmp(cmd, "update_hosts") == 0) {
		flag = SCADMIN_UPDATE_HOSTS;
	} else if (strcmp(cmd, "update_ntp") == 0) {
		flag = SCADMIN_UPDATE_NTP;
	}
	/* Make sure command flag was set */
	if (!flag) {
		usage();
		rstatus = SCCONF_EUSAGE;
		goto cleanup;
	}

	/* If there is a sponsor node, get the handle */

	/* Get the CLIENT handle */
	rstatus = scrconf_clnt_create(sponsor_node, &clnt, 0);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Add credentials */
	rstatus = scrconf_clnt_addcred(clnt, sponsor_node);
	if (rstatus != SCCONF_NOERR) {
		goto cleanup;
	}

	/* Run the command */
	switch (flag) {

	/* update hosts */
	case SCADMIN_UPDATE_HOSTS:

		/* Update hosts */
		/* Call the RPC server */
		clnt_st = scadmproc_update_hosts_1(&clnt_res, clnt);

		if (clnt_st != RPC_SUCCESS) {
			clnt_perror(clnt, progname);
		}

		/* If err-ed, return error */
		if (clnt_st != RPC_SUCCESS) {
			switch (clnt_st) {
				case RPC_TIMEDOUT:
					return (SCCONF_ETIMEDOUT);

				default:
					return (SCCONF_EUNEXPECTED);
			} /*lint !e788 */
		}

		/* Return result of update_hosts operation */
		rstatus = (scconf_errno_t)clnt_res.sc_res_errno;

		if (rstatus != SCCONF_NOERR) {
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
				"%s:  Failed to update hosts.\n"),
				progname);
			goto cleanup;
		}


		/* Done */
		break;
	/* update ntp.conf.cluster */
	case SCADMIN_UPDATE_NTP:

		/* Update ntp.conf.cluster */
		/* Call the RPC Server */
		clnt_st = scadmproc_update_ntp_1(&clnt_res, clnt);

		if (clnt_st != RPC_SUCCESS)
			clnt_perror(clnt, progname);

		/* If err-ed, return error */
		if (clnt_st != RPC_SUCCESS) {
			switch (clnt_st) {
				case RPC_TIMEDOUT:
					return (SCCONF_ETIMEDOUT);

				default:
					return (SCCONF_EUNEXPECTED);
			} /*lint !e788 */
		}

		/* Return result of update_system_file operation */
		rstatus = (scconf_errno_t)clnt_res.sc_res_errno;

		if (rstatus != SCCONF_NOERR) {
			scconf_strerr(errbuff, rstatus);
			(void) fprintf(stderr, gettext(
				"%s:  Failed to update ntp.\n"),
				progname);
			goto cleanup;
		}

		/* Done */
		break;

	/* unknown */
	default:
		(void) fprintf(stderr, gettext(
			"%s:  Failed to complete sys command - \
			unknown option.\n"), progname);
		rstatus = SCCONF_EUNEXPECTED;
		goto cleanup;
	}

cleanup:
	/* release RPC client handle */
	if (clnt != (CLIENT *)0) {
		if (clnt->cl_auth)
			auth_destroy(clnt->cl_auth);
		clnt_destroy(clnt);
	}

	/* cleanup */
	free(dupopts);

	return (rstatus);
}

/*
 * scrconfcmd_set
 *
 *	Set global property. Currently only accepts global_fencing
 */
static scconf_errno_t
scrconfcmd_set(int argc, char **argv, uint_t Uflag)
{
	scconf_errno_t rstatus = SCCONF_NOERR;
	char *global_fencing = (char *)0;
	unsigned short globalfencing_status = 0;
	int c;

	/* Only property global_fencing is supported now. */
	optind = 1;
	while ((c = getopt(argc, argv, "cG:U")) != EOF) {
		switch (c) {
		case 'G':
			if (global_fencing != NULL) {
				usage();
				rstatus = SCCONF_EUSAGE;
				break;
			}
			global_fencing = optarg;
			break;
		case '?':
			usage();
			rstatus = SCCONF_EUSAGE;
			break;
		default:
			break;
		}
	}

	if (rstatus != SCCONF_NOERR) {
		return (rstatus);
	}

	/* Make sure that there are no additional arguments */
	if (optind != argc) {
		usage();
		return (SCCONF_EUSAGE);
	}

	/* Make sure the property is specified */
	if (!global_fencing) {
		usage();
		return (SCCONF_EUSAGE);
	} else {
		/* Validate the fencing input */
		if (strcasecmp(global_fencing, "pathcount") == 0) {
			globalfencing_status = GLOBAL_FENCING_PATHCOUNT;
		} else if (strcasecmp(global_fencing, "prefer3") == 0) {
			globalfencing_status = GLOBAL_FENCING_PREFER3;
		} else if (strcasecmp(global_fencing, "nofencing") == 0 ||
		    strcasecmp(global_fencing, "nofencing-noscrub") == 0) {
			globalfencing_status = GLOBAL_NO_FENCING;
		} else {
			(void) fprintf(stderr, gettext(
			    "%s: Invalid value \"%s\" for global_fencing "
			    "property.\n"), progname, global_fencing);
			return (SCCONF_EINVAL);
		}
		if (!Uflag) {
			/* Initialize did library */
			if (did_initlibrary(1, DID_CONF_FILE, NULL) !=
			    DID_INIT_SUCCESS) {
				(void) fprintf(stderr, gettext(
				    "Failed to initialize did library.\n"));
				return (SCCONF_EUNEXPECTED);
			}
			if (did_setglobalfencingstatus(globalfencing_status)
			    != 0) {
				(void) fprintf(stderr, gettext(
				    "%s: Failed to set the global_fencing "
				    "property to \"%s\".\n"),
				    progname, global_fencing);
				return (SCCONF_EUNEXPECTED);
			}
		}
	}

	return (rstatus);
}
