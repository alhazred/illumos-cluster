#!/bin/ksh
#
# Copyright (c) 2008, Sun Microsystems, Inc.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of Sun Microsystems, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
# Gate Specific Code -- BEGIN
# ident	"@(#)dns_svc_stop.ksh	1.18	08/07/14 SMI"
# Gate Specific Code -- END
# Sun Cluster Data Services Builder template version 1.0
#
# Stop method for DNS
#
###############################################################################
alias scds_syslog="/usr/cluster/lib/sc/scds_syslog"
SYSLOG_TAG="[SUNW.dns]"
###############################################################################
# Parse program arguments.
#
function parse_args # [args ...]
{
	typeset opt

	while getopts 'R:G:T:' opt
	do
		case "$opt" in
		R)
		# Name of the DNS resource.
		RESOURCE_NAME=$OPTARG
		;;

		G)
		# Name of the resource group in which the resource is
		# configured.
		RESOURCEGROUP_NAME=$OPTARG
		;;

		T)
		# Name of the resource type.
		RESOURCETYPE_NAME=$OPTARG
		;;

		*)
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"ERROR: Option %s unknown" \
		$OPTARG
		exit 1
		;;

		esac
	done
	SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"
}

##############################################################################
# MAIN
##############################################################################

export PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/proc/bin:$PATH

# Obtain the syslog facility to use. This will be used to log the messages.
SYSLOG_FACILITY=`scha_cluster_get -O SYSLOG_FACILITY`

# Parse the arguments that have been passed to this method.
parse_args "$@"

# get the Timeout value allowed for stop method from the RTR file
STOP_TIMEOUT=`scha_resource_get -O STOP_TIMEOUT -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

# We will try to wait for 80% of the stop_method_timeout value using the
# stop command provided by the user or we send a SIGTERM through PMF to
# the Data service. This is to make sure that the application stops in a
# decent manner. If the application does not respond favourably to this
# then we use SIGKILL to stop the data service and this will be done for
# a 15% of the Stop_method_timeout value. However, if the data service
# has not stopped by now, we conclude that there was a Failure in the
# stop method and exit non-zero. The remaining 5% of the stop method
# timeout is for other needs.

((SMOOTH_TIMEOUT=$STOP_TIMEOUT * 80/100))

((HARD_TIMEOUT=$STOP_TIMEOUT * 15/100))

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.svc
SYSLOG_TAG=$RESOURCETYPE_NAME,$RESOURCEGROUP_NAME,$RESOURCE_NAME

# We need to know the full path for the gethostnames utility which resides
# in the directory <RT_BASEDIR>. Get this from the RT_BASEDIR property of the
# resource type.
RT_BASEDIR=`scha_resource_get -O RT_BASEDIR -R $RESOURCE_NAME \
        -G $RESOURCEGROUP_NAME`

hostnames=`$RT_BASEDIR/gethostnames -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
        -T $RESOURCETYPE_NAME`

stop_cmd_args=

stop_cmd_prog=`echo $stop_cmd_args | nawk '{print $1}'`

typeset -i SEND_KILL=0

# User added code -- BEGIN vvvvvvvvvvvvvvv
# User added code -- END   ^^^^^^^^^^^^^^^

pmfadm -q $PMF_TAG
if [[ $? -eq 0 ]]; then
	if [[ -f $stop_cmd_prog && -x $stop_cmd_prog ]]; then
		# User added code -- BEGIN vvvvvvvvvvvvvvv
		# User added code -- END   ^^^^^^^^^^^^^^^

		pmfadm -s $PMF_TAG
		if [[ $? -ne 0 && $? -ne 1 ]]; then
			# SCMSGS
			# @explanation
			# Sun Cluster failed to stop restarting the application.
			# @user_action
			# Contact your authorized Sun service provider to determine
			# whether a workaround or patch is available.
			scds_syslog -p info -t "${SYSLOG_TAG}" -m \
				"Failed to take DNS out of PMF \
				control; trying to send SIGKILL now"
			SEND_KILL=1
		else
			# User added code -- BEGIN vvvvvvvvvvvvvvv
			# User added code -- END   ^^^^^^^^^^^^^^^

			# execute the user specified stop_cmd using hatimerun
			hatimerun -k KILL -t $SMOOTH_TIMEOUT $stop_cmd_args
			if [[ $? -ne 0 ]]; then
				# SCMSGS
				# @explanation
				# The stop command failed to stop the application.
				# @user_action
				# Refer to your data services documentation for
				# configuring the stop command. If problem persists,
				# contact your Sun service provider to determine
				# whether a workaround or patch is available.
				scds_syslog -p error -t "${SYSLOG_TAG}" -m \
					"Failed to stop DNS using \
					the custom stop command; trying \
					SIGKILL now."
			fi

			# Regardless of whether the command succeeded or not we
			# send KILL signal to the pmf tag. This will ensure
			# that the process tree goes away if it still exists.
			# If it doesn't exist by then, we return NOERR.
			SEND_KILL=1
		fi
	else
		# User added code -- BEGIN vvvvvvvvvvvvvvv
		# User added code -- END   ^^^^^^^^^^^^^^^

		# Send a SIGTERM signal to the Data service and wait for
		# 80% of the total timeout value.
		pmfadm -s $PMF_TAG -w $SMOOTH_TIMEOUT TERM
		# We compare the exit status of pmfadm to be greater than 2
		# because "exit status = 1" means nametag doesn't exist, which
		# is a OK, for the stop method has to be idempotent.
		if [[ $? -ge 2 ]]; then 
			# SCMSGS
			# @explanation
			# The data service failed to stop the application by
			# sending it a SIGTERM in the allotted time. Will retry
			# with SIGKILL.
			# @user_action
			# This problem may occur when the cluster is under load and 
			# Sun Cluster cannot stop the application within the timeout 
			# period specified. You may consider increasing the 
			# Stop_timeout property.
			scds_syslog -p error -t "${SYSLOG_TAG}" -m \
				"Failed to stop DNS with SIGTERM; \
				retry with SIGKILL"
			SEND_KILL=1;
		fi
	fi

	pmfadm -q $PMF_TAG
	if [[ $SEND_KILL -eq 1 && $? -eq 0 ]]; then 
		# User added code -- BEGIN vvvvvvvvvvvvvvv
		# User added code -- END   ^^^^^^^^^^^^^^^

		# Since the Data service did not stop with a SIGTERM we will
		# use a SIGKILL now and wait for another 15% of total timeout.
		pmfadm -s $PMF_TAG -w $HARD_TIMEOUT KILL
		# We compare the exit status of pmfadm to be greater than 2
		# because "exit status = 1" means nametag doesn't exist, which
		# is a OK, for the stop method has to be idempotent.
		if [[ $? -ge 2 ]]; then
			# SCMSGS
			# @explanation
			# The STOP method was unable to stop the application by
			# sending it a SIGKILL.
			# @user_action
			# Examine the /var/adm/messages to determine why the
			# resource failed to stop. If problem persists, contact
			# your Sun service representative.
			scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Failed to stop DNS; exiting UNSUCCESFUL"
			exit 1
		fi
	fi
else 
	# The Data service is not running as of now. Log a message and 
	# exit suceess.

	# SCMSGS
	# @explanation
	# The DNS service is not running.
	# @user_action
	# No action needed.
	scds_syslog -p info -t "${SYSLOG_TAG}" -m \
		"DNS not already running"

	# Even if the DNS is not running, we exit success, for idempotency of
	# the STOP method.
	exit 0
fi

# User added code -- BEGIN vvvvvvvvvvvvvvv
# User added code -- END   ^^^^^^^^^^^^^^^

# could successfully stop DNS. Log a message and exit success.

# SCMSGS
# @explanation
# The STOP method for DNS for was successful.
# @user_action
# No action needed.
scds_syslog -p info -t "${SYSLOG_TAG}" -m \
"DNS successfully stopped"
exit 0
