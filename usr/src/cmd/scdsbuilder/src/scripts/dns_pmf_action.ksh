#!/bin/ksh
#
# Copyright (c) 2008, Sun Microsystems, Inc.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of Sun Microsystems, Inc. nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.
#
# Gate Specific Code -- BEGIN
# ident	"@(#)dns_pmf_action.ksh	1.4	08/02/23 SMI"
# Gate Specific Code -- END
# Sun Cluster Data Services Builder template version 1.0
#
# PMF action script for DNS.
#
# This action script is invoked by PMF when the data service dies. The job
# of this script is to 
# 1) initiate a local restart if the number of resource
# restarts is less than the retry count in the retry time interval window.
# This would call scha_control with RESOURCE_RESTART tag to request
# the rgmd to execute STOP followed by START method.
# 2) if The number of restarts of services under the resource has reached
# the max. the resource is failover over by calling scha_control with
# SCHA_GIVEOVER
#
###############################################################################
alias scds_syslog="/usr/cluster/lib/sc/scds_syslog"
alias scha_resourcegroup_get="/usr/cluster/bin/scha_resourcegroup_get"
alias scha_resource_get="/usr/cluster/bin/scha_resource_get"
alias scha_control="/usr/cluster/bin/scha_control"
alias pmfadm="/usr/cluster/bin/pmfadm"
SYSLOG_TAG="[SUNW.dns]"
###############################################################################
# Parse program arguments.
#
function parse_args # [args ...]
{
	typeset opt

	while getopts 'R:G:T:' opt
	do
		case "$opt" in

		R)
		# Name of the DNS resource.
		RESOURCE_NAME=$OPTARG
		;;

		G)
		# Name of the resource group in which the resource is
		# configured.
		RESOURCEGROUP_NAME=$OPTARG
		;;

		T)
		# Name of the resource type.
		RESOURCETYPE_NAME=$OPTARG
		;;

		*)
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"ERROR: Option %s unknown" \
		$OPTARG
		exit 1
		;;

		esac
	done
	SYSLOG_TAG="[${RESOURCETYPE_NAME},${RESOURCE_NAME},${RESOURCEGROUP_NAME}]"
}

###############################################################################
# MAIN
##############################################################################

rc=1
parse_args "$@"

SCHA_PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.scha

RT_BASEDIR=`scha_resource_get -O RT_BASEDIR -R $RESOURCE_NAME \
        -G $RESOURCEGROUP_NAME`
if [[ $? -ne 0 ]]; then
	# SCMSGS
	# @explanation
	# The Resource is left in a faulted status and is not 
	# restarted.
	# @user_action
	# Examine the /var/adm/messages to determine why the
	# resource is failing, and restart it after taking
	# corrective action. The resource can be restarted by
	# the sequence "clresource disable <resource>; clresource
	# enable <resource>". Or, the whole resource group can be
	# restarted using "clresourcegroup restart <group>". If
	# problem persists, contact your Sun service representative.
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"scha_resource_get operation %s failed for Resource %s" \
		"RT_BASEDIR" "${RESOURCE_NAME}"
	# set the status to faulted state.
	scha_resource_setstatus -G $RESOURCEGROUP_NAME \
		-R $RESOURCE_NAME \
		-s FAULTED \
		-m "scha_resource_get operation failed."
	exit 1
fi

# If scha_control is executed under pmfadm, the exit status cannot
# be captured after the scha_control process terminates. This
# requires a wrapper script and the wrapper executes scha_control,
# checks the exit code, and writes the necessary error message to
# syslog before exiting.

scha_ctrl_wrapper=$RT_BASEDIR/dns_scha_ctrl_wrapper.ksh

# Get the resource number of restarts
num_resource_restart=`scha_resource_get -O NUM_RESOURCE_RESTARTS \
	-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`
if [[ $? -ne 0 ]]; then
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"scha_resource_get operation %s failed for Resource %s" \
		"NUM_RESOURCE_RESTARTS" "${RESOURCE_NAME}"

	# set the status to faulted state.
	scha_resource_setstatus -G $RESOURCEGROUP_NAME \
		-R $RESOURCE_NAME \
		-s FAULTED \
		-m "scha_resource_get operation failed."
	exit 1
fi
# Get the Retry count value from the system defined property Retry_count
retry_count=`scha_resource_get -O RETRY_COUNT \
		-G $RESOURCEGROUP_NAME \
		-R $RESOURCE_NAME`
if [[ $? -ne 0 ]]; then
	scds_syslog -p error -t "${SYSLOG_TAG}" -m \
		"scha_resource_get operation %s failed for Resource %s" \
		"RETRY_COUNT" "${RESOURCE_NAME}"
	# set the status to faulted state.
	scha_resource_setstatus -G $RESOURCEGROUP_NAME \
		-R $RESOURCE_NAME \
		-s FAULTED \
		-m "scha_resource_get operation failed."
	exit 1
fi

# If the application deamon dies immediately. The action script is executed
# before even the previous scha_control has completed. Hence the pmfadm
# would fail. Wait until the previous scha_control is completed.
pmfadm -s $SCHA_PMF_TAG -w -1

# Initiate a local restart if the number of resource restarts
# is less than the retry count in the retry time interval window.
if [[ $num_resource_restart -lt $retry_count ]]; then

	pmfadm -c $SCHA_PMF_TAG $scha_ctrl_wrapper -O RESOURCE_RESTART \
		-G $RESOURCEGROUP_NAME \
		-R $RESOURCE_NAME -T $RESOURCETYPE_NAME
	rc=$?
	if [[ $rc -ne 0 ]]; then
			# set the status to faulted state.
			scha_resource_setstatus -G $RESOURCEGROUP_NAME \
				-R $RESOURCE_NAME \
				-s FAULTED \
				-m "Restart failed."
	else
			scds_syslog -p info -t "${SYSLOG_TAG}" -m \
				"Successfully restarted service."
	fi
else
	# the fact that the number of resource restarts have exceeded
	# implies that the dataservice has already passed the max no
	# of retries allowed. Hence there is no point in trying to restart
	# the dataservice again. We might as well try to failover
	# to another node in the cluster.

	pmfadm -c $SCHA_PMF_TAG $scha_ctrl_wrapper -O GIVEOVER \
		-G $RESOURCEGROUP_NAME \
		-R $RESOURCE_NAME -T $RESOURCETYPE_NAME
	rc=$?
	if [ $rc -ne 0 ]; then
		scha_resource_setstatus -G $RESOURCEGROUP_NAME \
			-R $RESOURCE_NAME \
			-s FAULTED \
			-m "Unable to Failover to other node."
		scds_syslog -p error -t "${SYSLOG_TAG}" -m \
			"Error in failing over the resource group:%s" \
			"${RESOURCEGROUP_NAME}"
	fi
fi
exit 1
