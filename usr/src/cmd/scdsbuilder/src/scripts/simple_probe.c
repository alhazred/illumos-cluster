/*
 * Copyright (c) 2008, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Sun Microsystems, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * simple_probe.c - utility routine for DNS scripts.
 *
 * This is a utility program used by the probe method of DNS data service.
 * This utility probes the dataservice that calls this binary. The probing done
 * is a simple connect and disconnect to the IP address and port on which the
 * dataservice has been configured. The usage for this binary is
 *
 * simple_probe -R <resource_name>
 * 		-G <resourcegroup_name>
 * 		-T <resourcetype_name>
 *
 */


#pragma ident	"@(#)simple_probe.c	1.8	08/05/20 SMI"

#include <stdio.h>
#include <sys/types.h>
#include <time.h>
#include <rgm/libdsdev.h>

int
main(int argc, char *argv[])
{
	scds_netaddr_list_t 	*netaddr;
	int 			err;
	scds_handle_t 		scds_handle;
	char 		*hostname;
	int		port, ip, timeout;


	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	if (scds_get_netaddr_list(scds_handle, &netaddr)) {
		scds_syslog(LOG_ERR,
		    "No Network resource in resource group.");
		scds_close(&scds_handle);
		return (1);
	}

	timeout = scds_get_ext_probe_timeout(scds_handle);

	for (ip = 0; ip < netaddr->num_netaddrs; ip++) {
		/* Get the host IP address and the port number */
		hostname = netaddr->netaddrs[ip].hostname;
		port = netaddr->netaddrs[ip].port_proto.port;

		err = scds_simple_probe(scds_handle, hostname, port, timeout);
		if (err != 0) {
			return (err);
		}
	}

	scds_free_netaddr_list(netaddr);
	scds_close(&scds_handle);
	return (0);
}
