/*
 * Copyright (c) 2008, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Sun Microsystems, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * gethostnames.c - utility routine for DNS scripts.
 *
 * This is a utility program used by the probe method of DNS data service.
 * This utility pritns the list of all the network address resources that are
 * being used by the dataservice. The usage for this binary is
 *
 * gethostnames -R <resource_name>
 * 		-G <resourcegroup_name>
 * 		-T <resourcetype_name>
 *
 */

#pragma ident "@(#)gethostnames.c 1.7 08/05/20 SMI"

#include <stdio.h>
#include <sys/types.h>
#include <time.h>
#include <rgm/libdsdev.h>

int
main(int argc, char *argv[])
{
	scds_net_resource_list_t 	*netres;
	int 		rs, ip;
	scds_handle_t 	scds_handle;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	if (scds_get_rs_hostnames(scds_handle, &netres)) {
		/*
		 * SCMSGS
		 * @explanation
		 * This message indicates that there is no network resource
		 * configured for a "network-aware" service. A network aware
		 * service can not function without a network address.
		 * @user_action
		 * Configure a network resource and retry the command.
		 */
		scds_syslog(LOG_ERR,
		    "No Network resource in resource group.");
		scds_free_net_list(netres);
		scds_close(&scds_handle);
		return (1);
	}

	if ((netres->num_netresources == 1) &&
	    (netres->netresources[0].num_hostnames == 1)) {
		(void) printf("%s", netres->netresources[0].hostnames[0]);
	} else {
		for (rs = 0; rs < netres->num_netresources; rs++) {
			for (ip = 0;
			    ip < netres->netresources[rs].num_hostnames;
			    ip++) {
				(void) printf("%s",
				    netres->netresources[rs].hostnames[ip]);
				(void) printf("%s", ",");
			}
		}
	}

	scds_free_net_list(netres);

	scds_close(&scds_handle);

	return (0);
}
