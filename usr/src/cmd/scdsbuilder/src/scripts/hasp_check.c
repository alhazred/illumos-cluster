/*
 * Copyright (c) 2008, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Sun Microsystems, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * hasp_check.c - utility routine for checking the status of the HAStorage
 *		  Plus resources.
 *
 * Since this utility is typically called from the VALIDATE method, the
 * resource for which this is being called hasn't yet been created.
 * Therefore, this utility must be called with all the arguments as they were
 * passed to the VALIDATE method. Failing to do this would result in the
 * failure of scds_initialize and, consequently, of hasp_check itself.
 *
 */


#pragma ident	"@(#)hasp_check.c	1.7	08/05/20 SMI"

#include <stdio.h>
#include <rgm/libdsdev.h>

int
main(int argc, char *argv[])
{
	int 		rc;
	scds_handle_t 	scds_handle;
	scds_hasp_status_t hasp_status;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Check availability of SUNW.HAStoragePlus resources */
	if (scds_hasp_check(scds_handle, &hasp_status) != SCHA_ERR_NOERR) {
		/* Call failed, status is unknown */
		rc = 1;
		goto finished; /* validation has failed for this resource */
	}

	switch (hasp_status) {

	case SCDS_HASP_NO_RESOURCE:
		/*
		 * We do not depend on any SUNW.HAStoragePlus resources
		 */
		rc = 2;
		break;

	case SCDS_HASP_ERR_CONFIG:
		/*
		 * Configuration error, SUNW.HAStoragePlus resource is
		 * in a different RG.
		 */
		rc = 3;
		break;

	case SCDS_HASP_NOT_ONLINE:
		/*
		 * There is at least one SUNW.HAStoragePlus resource not
		 * online anywhere.
		 */
		rc = 4;
		break;

	case SCDS_HASP_ONLINE_NOT_LOCAL:
		/*
		 * Not all SUNW.HAStoragePlus we need, are online locally.
		 */
		rc = 5;
		break;

	case SCDS_HASP_ONLINE_LOCAL:
		/*
		 * All SUNW.HAStoragePlus resources we need are available on
		 * this node.
		 */
		rc = 6;
		break;

	default:
		/* Unknown status code */
		scds_syslog(LOG_ERR, "Unknown status code %d.", hasp_status);
		rc = 1;
		break;
	}

finished:
	scds_close(&scds_handle);
	return (rc);
}
