/*
 * Copyright (c) 2009, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Sun Microsystems, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _GDS_EVENTLOG_H_
#define	_GDS_EVENTLOG_H_

#pragma ident	"@(#)gds_eventlog.h	1.6	09/01/09 SMI"

/*
 * This file is the interface for GDS to generate an event which will be
 * logged through the event logging framework
 */

#include <rgm/libdsdev.h>
#include <sys/syslog.h>

/*
 * Type definitions
 */

typedef enum {
	GDS_START,
	GDS_STOP,
	GDS_PROBE,
	GDS_OTHER
} gds_callback_type_t;

/*
 * If the levels below are changed, change the log_level_strings array in
 * gds_eventlog.c accordingly.
 */

typedef enum {
	GDS_LOG_NONE = 0,
	GDS_LOG_ERR  = LOG_ERR,
	GDS_LOG_INFO = LOG_INFO
} gds_log_level_t;

/*
 * Init and destroy routines
 */

int gds_publish_init();
void gds_publish_destroy();

/*
 * publish routines
 */

int gds_publish_eventlog(gds_callback_type_t type, gds_log_level_t log_level,
    scds_handle_t scds_handle, char *string, ...);

#endif /* _GDS_EVENTLOG_H_ */
