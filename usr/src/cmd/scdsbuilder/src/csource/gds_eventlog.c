/*
 * Copyright (c) 2009, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Sun Microsystems, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Gate Specific Code -- BEGIN */
#pragma ident	"@(#)gds_eventlog.c	1.8	09/02/19 SMI"
/* Gate Specific Code -- END */

#include <sys/cl_events.h>
#include <stdarg.h>
#include <stdio.h>
#include <sys/types.h>
#include <time.h>
#include <scha.h>
#include <string.h>
#include <malloc.h>
#include <sys/sol_version.h>

#include "gds_eventlog.h"

#define	BUFFER_SIZE 2048

/*
 * Each time a trace has to be sent in an event, GDS won't check if the
 * Log_level property has changed (the SCHA call costs too much)
 *
 * The LOG_LEVEL_TIMEOUT constant specify the periodicity of the check. A
 * value of -1 means that the property will never be checked
 *
 * Its value is in seconds
 */

#define	LOG_LEVEL_TIMEOUT -1

/*
 *
 * Private type definitions
 *
 */

typedef struct {
	time_t last_check;
	gds_log_level_t log_level;
} global_datas_t;

/*
 *
 * Global datas
 *
 */

static global_datas_t *global_datas = NULL;

static const char *log_level_strings[] = {
	"NONE",
	"ERR",
	"INFO"
};

/*
 *
 * Private routines
 *
 */

/*
 * Don't call this routine directly !
 *
 * If 'clustername' is non-NULL, the event is logged for a zone cluster.
 */
static int
gds_publish_eventlog_internal(gds_callback_type_t type,
    gds_log_level_t log_level, const char *rname, const char *rgname,
    const char *clustername, char *string, va_list ap)
{
	char *ev_subclass = NULL;
	char comment_buffer[BUFFER_SIZE];
	int err;
	cl_event_severity_t severity;

	switch (type) {
	case GDS_START:
		ev_subclass = ESC_CLUSTER_GDS_START;
		break;

	case GDS_STOP:
		ev_subclass = ESC_CLUSTER_GDS_STOP;
		break;

	case GDS_PROBE:
		ev_subclass = ESC_CLUSTER_GDS_PROBE;
		break;

	case GDS_OTHER:
		ev_subclass = ESC_CLUSTER_GDS_OTHER;
		break;

	default:
		return (1);
	}

	switch (log_level) {
	case LOG_INFO:
		severity = CL_EVENT_SEV_INFO;
		break;

	case LOG_ERR:
		severity = CL_EVENT_SEV_ERROR;
		break;

	default:
		severity = CL_EVENT_SEV_INFO;
	}

	vsnprintf(comment_buffer, BUFFER_SIZE, string, ap);

#if SOL_VERSION >= __s10
	if (clustername != NULL) {
		err = sc_publish_zc_event((char *)clustername, ev_subclass,
		    CL_EVENT_PUB_GDS, severity,
		    CL_EVENT_INIT_AGENT, 0, 0, 0, CL_R_NAME,
		    SE_DATA_TYPE_STRING, rname,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rgname, CL_STATUS_MSG,
		    SE_DATA_TYPE_STRING, comment_buffer, NULL);
	} else
#endif
	{
		err = sc_publish_event(ev_subclass, CL_EVENT_PUB_GDS, severity,
		    CL_EVENT_INIT_AGENT, 0, 0, 0, CL_R_NAME,
		    SE_DATA_TYPE_STRING, rname,
		    CL_RG_NAME, SE_DATA_TYPE_STRING, rgname, CL_STATUS_MSG,
		    SE_DATA_TYPE_STRING, comment_buffer, NULL);
	}
	return (err);
}

static int
gds_publish_eventlog_without_check(gds_callback_type_t type, const char *rname,
    const char *rgname, const char *clustername, char *string, ...)
{
	va_list ap;
	int err;

	va_start(ap, string);
	err = gds_publish_eventlog_internal(type, LOG_INFO, rname, rgname,
	    clustername, string, ap);
	va_end(ap);

	return (err);
}

/*
 * Return 1 if the trace should be printed, 0 otherwise
 *
 * This implementation relies on a cache. We don't want to go and check the
 * actual value of the Log_level property each time an event is logged.
 *
 * The first part of the implementation check if the cache has to be
 * updated (timeout or invalid value) and the second one goes and retrieves
 * the current value
 */

static int
gds_publish_check_level(const char *zonename, boolean_t is_zc,
    gds_callback_type_t type, gds_log_level_t log_level,
    const char *rname, const char *rgname)
{
	int res = 1;
	int valid_cache = 1;
	time_t now;
	const char *clustername = (zonename && is_zc) ? zonename : NULL;

	if (global_datas->last_check == -1) {
		valid_cache = 0;
	}

	if ((valid_cache) && (LOG_LEVEL_TIMEOUT != -1)) {
		now = time(NULL);
		if (now - global_datas->last_check > LOG_LEVEL_TIMEOUT) {
			valid_cache = 0;
		}
	}

	if (!valid_cache) {
		/* We have to retrieve the Log_level property value */
		scha_resource_t handle;
		scha_err_t err;
		scha_extprop_value_t *extprop = NULL;

		err = scha_resource_open_zone(zonename, rname, rgname, &handle);
		if (err != SCHA_ERR_NOERR) {
			/* Failed. Keep the old value */
			(int)gds_publish_eventlog_without_check(type, rname,
			    rgname, clustername,
	"scha_resource_open failed [%d]. Keeping the old Log_level value", err);
		}

		if (err == SCHA_ERR_NOERR) {
			err = scha_resource_get_zone(zonename, handle,
			    SCHA_EXTENSION, "Log_level", &extprop);
			if (err != SCHA_ERR_NOERR) {
				(int)gds_publish_eventlog_without_check(type,
				    rname, rgname, clustername,
	"scha_resource_get failed [%d]. Keeping the old Log_level value", err);
				(scha_err_t)scha_resource_close(handle);
			}
		}

		if (err == SCHA_ERR_NOERR) {
			if (strcmp(extprop->val.val_enum, "NONE") == 0) {
				global_datas->log_level = GDS_LOG_NONE;
			} else if (strcmp(extprop->val.val_enum, "ERR") == 0) {
				global_datas->log_level = GDS_LOG_ERR;
			} else if (strcmp(extprop->val.val_enum, "INFO") == 0) {
				global_datas->log_level = GDS_LOG_INFO;
			} else {
				(int)gds_publish_eventlog_without_check(type,
				    rname, rgname, clustername,
	"The value found in the Log_level property is incorrect [%s]",
				    extprop->val.val_str);
			}
		}

		if (err == SCHA_ERR_NOERR) {
			(scha_err_t)scha_resource_close(handle);
		}

		/*
		 * Even if we failed to retrieve the value, we won't check
		 * until the next timeout ...
		 */
		global_datas->last_check = time(NULL);
	}

	return (log_level<=global_datas->log_level?1:0);
}

static int
gds_publish_eventlog_common(const char *zonename, boolean_t is_zc,
    gds_callback_type_t type, gds_log_level_t log_level, const char *rname,
    const char *rgname, char *string, va_list ap)
{
	int check;
	const char *clustername = (zonename && is_zc) ? zonename : NULL;

	check = gds_publish_check_level(zonename, is_zc, type, log_level, rname,
	    rgname);
	if (!check) {
		return (1);
	}

	return (gds_publish_eventlog_internal(type, log_level, rname, rgname,
	    clustername, string, ap));
}

/*
 *
 * Implementation of the API
 *
 */

int
gds_publish_init()
{
	if (global_datas) {
		return (0);
	}

	global_datas = (global_datas_t*)malloc(sizeof (global_datas_t));
	if (!global_datas) {
		return (1);
	}

	global_datas->last_check = -1;
	global_datas->log_level = GDS_LOG_INFO;

	return (0);
}

void
gds_publish_destroy()
{
	if (!global_datas) {
		return;
	}

	free(global_datas);
	global_datas = NULL;
}

int
gds_publish_eventlog(gds_callback_type_t type, gds_log_level_t log_level,
    scds_handle_t scds_handle, char *string, ...)
{
	const char *rname = scds_get_resource_name(scds_handle);
	const char *rgname = scds_get_resource_group_name(scds_handle);
	const char *zonename = scds_get_zone_name(scds_handle);
	boolean_t is_zc = scds_is_zone_cluster(scds_handle);
	va_list ap;
	int err;

	if (!global_datas) {
		(int)gds_publish_init();
	}

	va_start(ap, string);
	err = gds_publish_eventlog_common(zonename, is_zc, type, log_level,
	    rname, rgname, string, ap);
	va_end(ap);

	return (err);
}
