/*
 * Copyright (c) 2008, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Sun Microsystems, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _CMDLINE_SUBST_H_
#define	_CMDLINE_SUBST_H_

#pragma ident	"@(#)xfs_cmdline_subst.h	1.4	08/02/26 SMI"

#include <rgm/libdsdev.h>

typedef void *cmdline_handle_t;

/*
 * This file is the interface with the code that does the command line
 * substitution.
 *
 * See each function for parameter descriptions.
 */

/*
 * cmdline_open allocates the resources needed to parse the command line.
 *
 * - scds_handle is an input parameter giving access to the SCDS library ;
 * - handle is an output parameter that has to be given back to the other
 *   calls in this inteface
 *
 * The call returns 0 in case of success, any other value otherwise.
 */

int cmdline_open(scds_handle_t scds_handle, cmdline_handle_t *handle);

/*
 * cmdline_close frees the resources allocated by cmdline_open.
 *
 * - handle is the parameter returned by cmdline_open
 *
 * The call returns 0 in case of success, any other value otherwise.
 */

int cmdline_close(cmdline_handle_t handle);


/*
 * cmdline_substitute_parameters does the substitution in the input string.
 *
 * - input is the string containing the command line to be substituted.
 *
 * It returns the substituted command, or NULL in case of failure.
 *
 * The memory needed to allocate the result string is allocated by the
 * cmdline code and will be freed when cmdline_close will be
 * called.
 * DON'T FREE IT YOURSELF !!
 */

char *cmdline_substitute_parameters(cmdline_handle_t handle, char *input);

#endif /* _CMDLINE_SUBST_H_ */
