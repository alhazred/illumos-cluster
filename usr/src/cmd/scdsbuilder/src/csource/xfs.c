/*
 * Copyright (c) 2009, Sun Microsystems, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Sun Microsystems, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Gate Specific Code -- BEGIN */
#pragma ident	"@(#)xfs.c	1.60	09/01/09 SMI"
/* Gate Specific Code -- END */
/* Sun Cluster Data Services Builder template version 1.0 */
/*
 * xfs.c - Common utilities for XFS
 *
 * This utility has the methods for performing the validation, starting and
 * stopping the data service and the fault monitor. It also contains the method
 * to probe the health of the data service.  The probe just returns either
 * success or failure. Action is taken based on this returned value in the
 * method found in the file xfs_probe.c
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include <errno.h>
#include <dlfcn.h>
#include "xfs.h"
#include "xfs_cmdline_subst.h"

/* GDS Specific Code -- BEGIN */
#include "gds_eventlog.h"
/* GDS Specific Code -- END */

/* User added code -- BEGIN vvvvvvvvvvvvvvv */
/* User added code -- END   ^^^^^^^^^^^^^^^ */

/*
 * The initial timeout allowed  for the XFS dataservice to
 * be fully up and running. We will wait for for 3 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		3

/*
 * We need to wait for SVC_WAIT_TIME ( 2 secs) for pmf
 * to send the failure message before probing the service
 */

#define	SVC_WAIT_TIME		2
/* Network aware applications code -- BEGIN */
/*
 * We need to use 95% of probe_timeout to connect to the port and the
 * remaining time is used to disconnect from port in the svc_probe function.
 */
#define	SVC_CONNECT_TIMEOUT_PCT		95

/*
 * This value will be used as disconnect timeout, if there is no
 * time left from the probe_timeout.
 */

#define	SVC_DISCONNECT_TIMEOUT_SECONDS		2

boolean_t	run_probe;
boolean_t	userprobe_already_run;
/* Network aware applications code -- END */

#define	SVC_SMOOTH_PCT		80

#define	SVC_HARD_PCT		15
/* GDS Specific Code -- BEGIN */
#define	GDS
/* GDS Specific Code -- END */

/* This is the same as a pointer to the scds_hasp_check api */
typedef scha_err_t (*scds_hasp_check_t)(scds_handle_t, scds_hasp_status_t *);

/*
 * run_user_supplied_validate:
 * This routine runs the user-supplied validate command, if one is defined,
 * and returns its exit status.
 */

int
run_user_supplied_validate(scds_handle_t scds_handle,
			char *service_validate_cmd)
{
	int cmd_exit_status = 0;
	int validate_status = 0;
	char msg_str[SCDS_CMD_SIZE];

	/*
	 * validate command is specified; so use that to validate the
	 * application.
	 */

	cmd_exit_status = system(service_validate_cmd);
	if (cmd_exit_status == -1) {
		scds_syslog(LOG_ERR, "Cannot execute %s: %s.",
			service_validate_cmd, strerror(errno));
/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_OTHER, GDS_LOG_ERR,
		    scds_handle, "Validate couldn't be executed %s [%s]",
				service_validate_cmd, strerror(errno));
/* GDS Specific Code -- END */

		/*
		 * Validate command execute error
		 */
		validate_status = -1;
		goto finished;
	}

	if (WIFSIGNALED((uint_t)cmd_exit_status)) {
		(void) snprintf(msg_str, sizeof (msg_str),
			"%s terminated with signal %d", service_validate_cmd,
			WTERMSIG((uint_t)cmd_exit_status));
	} else if (WIFSTOPPED((uint_t)cmd_exit_status)) {
		(void) snprintf(msg_str, sizeof (msg_str),
			"%s stopped with signal %d", service_validate_cmd,
			WSTOPSIG((uint_t)cmd_exit_status));
	} else if (WIFEXITED((uint_t)cmd_exit_status)) {
		cmd_exit_status = WEXITSTATUS((uint_t)cmd_exit_status);
		(void) snprintf(msg_str, sizeof (msg_str),
			"%s exited with status %d", service_validate_cmd,
			cmd_exit_status);
	} else {
		cmd_exit_status = -1;
		(void) snprintf(msg_str, sizeof (msg_str),
			"%s returned with status %d",
			service_validate_cmd, cmd_exit_status);
	}

	if (cmd_exit_status != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failure in executing the command.
		 * @user_action
		 * Check the syslog message for the command
		 * description. Check whether the system is low in
		 * memory or the process table is full and take
		 * appropriate action. Make sure that the executable
		 * exists.
		 */
		scds_syslog(LOG_ERR, "Command %s failed with exit status <%d>.",
			service_validate_cmd, cmd_exit_status);

/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_OTHER, GDS_LOG_ERR,
		    scds_handle, "Validate couldn't be executed [%s]",
		    msg_str);
/* GDS Specific Code -- END */

		validate_status = -1;
		goto finished;
	}

	/*
	 * return the exit status of the command.
	 */
	validate_status = cmd_exit_status;
	scds_syslog_debug(DBG_LEVEL_HIGH,
			"Validate has been executed %s", msg_str);
	/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_OTHER, GDS_LOG_INFO, scds_handle,
			"Validate has been executed [%s]", msg_str);
	/* GDS Specific Code -- END */
finished:
	return (validate_status);
}

/*
 * svc_validate():
 *
 * Do XFS specific validation of the resource configuration.
 *
 */

int
svc_validate(scds_handle_t scds_handle)
{
	struct stat statbuf;
	char *start_cmd_prog[] = {
	""
	};
	int no_start_cmd = 0;
	int i, err, num_ports, protocol;
	char stop_cmd_prog[SCDS_CMD_SIZE] = "";
	char probe_cmd_prog[SCDS_CMD_SIZE] = "";
	char validate_cmd_prog[SCDS_CMD_SIZE] = "";
	int rc = 0, do_cmd_checks = 1;
	char cmd[SCDS_CMD_SIZE];
	scds_hasp_status_t hasp_status;
	scds_hasp_check_t scds_hasp_check_p;
	/* Network aware applications code -- BEGIN */
	scds_net_resource_list_t *snrlp = NULL;
	scds_port_list_t *portlist = NULL;
	/* Network aware applications code -- END */
	/* GDS Specific Code -- BEGIN */
	scha_extprop_value_t *start_commands = NULL;
	scha_extprop_value_t *stop_command = NULL;
	scha_extprop_value_t *probe_command = NULL;
	scha_extprop_value_t *validate_command = NULL;
	scha_extprop_value_t *network_aware_prop = NULL;
	char **gds_start_cmds = NULL;
	boolean_t 	network_aware = B_TRUE;
	boolean_t validate_flag = B_TRUE;

	/* GDS Specific Code -- END */

	/*
	 * Are we running with a libdsdev that does not have scds_hasp_check?
	 */
	scds_hasp_check_p = (scds_hasp_check_t)dlsym(RTLD_DEFAULT,
	    "scds_hasp_check");
	if (scds_hasp_check_p == NULL) {
		/* fake a call to scds_hasp_check() */
		err = SCHA_ERR_NOERR;
		hasp_status = SCDS_HASP_NO_RESOURCE;
	} else {
		/* actually check for HAStoragePlus resources */
		err = (*scds_hasp_check_p)(scds_handle, &hasp_status);
	}

	if (err != SCHA_ERR_NOERR) {
		/*
		 * scha_hasp_check() logs a message to syslog when it fails
		 */
		rc = 1;
		/*
		 * validation has failed for this resource
		 */
		goto finished;
	}

	switch (hasp_status) {

	case SCDS_HASP_NO_RESOURCE:
		/*
		 * We do not depend on any SUNW.HAStoragePlus resources
		 */
		scds_syslog(LOG_INFO,
		    "This resource does not depend on any SUNW.HAStoragePlus "
		    "resources. Proceeding with normal checks.");
		do_cmd_checks = 1;
		break;

	case SCDS_HASP_ERR_CONFIG:
		/*
		 * Configuration error, SUNW.HAStoragePlus resource is
		 * in a different RG. Fail the validation.
		 */
		scds_syslog(LOG_ERR,
		    "One or more of the SUNW.HAStoragePlus resources that "
		    "this resource depends on is in a different resource "
		    "group. Failing validate method configuration checks.");
		rc = 1;
		goto finished;

	case SCDS_HASP_NOT_ONLINE:
		/*
		 * There is at least one SUNW.HAStoragePlus resource not
		 * online anywhere.
		 */
		scds_syslog(LOG_ERR,
		    "One or more of the SUNW.HAStoragePlus resources that "
		    "this resource depends on is not online anywhere. "
		    "Failing validate method.");
		rc = 1;
		goto finished;

	case SCDS_HASP_ONLINE_NOT_LOCAL:
		/*
		 * Not all SUNW.HAStoragePlus we need, are online locally.
		 */
		scds_syslog(LOG_INFO,
		    "All the SUNW.HAStoragePlus resources that this resource "
		    "depends on are not online on the local node. "
		    "Skipping the checks for the existence and permissions "
		    "of the start/stop/probe commands.");
		do_cmd_checks = 0;
		break;

	case SCDS_HASP_ONLINE_LOCAL:
		/*
		 * All SUNW.HAStoragePlus resources we need are available on
		 * this node.
		 */
		scds_syslog(LOG_INFO,
		    "All the SUNW.HAStoragePlus resources that this resource "
		    "depends on are online on the local node. "
		    "Proceeding with the checks for the existence and "
		    "permissions of the start/stop/probe commands.");
		do_cmd_checks = 1;
		break;

	default:
		/* Unknown status code */
		scds_syslog(LOG_ERR, "Unknown status code %d.", hasp_status);
		rc = 1;
		break;
	}

	/*
	 * If we got here and do_cmd_checks is not set, it means that
	 * the SUNW.HAStorage resource is online but not on this node.
	 * Therefore, skip the checks for the existence and permissions
	 * of the start/stop/probe commands and jump (goto) straight to
	 * "global_checks".
	 */

	if (!do_cmd_checks) {
		goto global_checks;
	}

	/*
	 * If do_cmd_checks is set, it means that the SUNW.HAStorage
	 * resource is online on the local node. Therefore, proceed with
	 * the checks for the existence and permissions of the
	 * start/stop/probe commands.
	 */

	/* GDS Specific Code -- BEGIN */
	rc = scds_get_ext_property(scds_handle, "Validate_command",
	    SCHA_PTYPE_STRING, &validate_command);
	if (rc == SCHA_ERR_PROP) {
		scds_syslog_debug(DBG_LEVEL_HIGH, "resource property %s "
				"is not defined.", "VALIDATE");
		validate_flag = B_FALSE;
	} else if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Can not get extension property.
		 * @user_action
		 * Look for other syslog error messages on the same node. Save
		 * a copy of the /var/adm/messages files on all nodes, and
		 * report the problem to your authorized Sun service provider.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to retrieve resource <%s> extension property <%s>",
		    scds_get_resource_name(scds_handle), "Validate_command");
		goto finished;
	}

	if ((rc = scds_get_ext_property(scds_handle, "Start_command",
	    SCHA_PTYPE_STRINGARRAY, &start_commands)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve resource <%s> extension property <%s>",
		    scds_get_resource_name(scds_handle), "Start_command");
		goto finished;
	}

	no_start_cmd = start_commands->val.val_strarray->array_cnt;

	gds_start_cmds = (char **)malloc(no_start_cmd * sizeof (char *));
	if (gds_start_cmds == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		rc = SCHA_ERR_NOMEM;
		goto finished;
	}

	for (i = 0; i < no_start_cmd; i++) {
		gds_start_cmds[i] =
		    start_commands->val.val_strarray->str_array[i];
	}

	if ((rc = scds_get_ext_property(scds_handle, "Stop_command",
	    SCHA_PTYPE_STRING, &stop_command)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve resource <%s> extension property <%s>",
		    scds_get_resource_name(scds_handle), "Stop_command");
		goto finished;
	}

	if ((stop_command->val.val_str != NULL) &&
	    (strcmp(stop_command->val.val_str, "") != 0)) {
		(void) strcpy(stop_cmd_prog, stop_command->val.val_str);
		(void) strtok(stop_cmd_prog, " ");
	}

	if ((rc = scds_get_ext_property(scds_handle, "Probe_command",
	    SCHA_PTYPE_STRING, &probe_command)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve resource <%s> extension property <%s>",
		    scds_get_resource_name(scds_handle), "Probe_command");
		goto finished;
	}

	if ((probe_command->val.val_str != NULL) &&
	    (strcmp(probe_command->val.val_str, "") != 0)) {
		(void) strcpy(probe_cmd_prog, probe_command->val.val_str);
		(void) strtok(probe_cmd_prog, " ");
	}
	/* GDS Specific Code -- END */

	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

	/*
	 * Make sure that if xfs validate command is specified, then it exists
	 * and that the permissions are correct.
	 */
	/* GDS Specific Code -- BEGIN */
	if (validate_flag == B_TRUE) {
		if ((validate_command->val.val_str != NULL) &&
			(strcmp(validate_command->val.val_str, "") != 0)) {
			(void) strcpy(validate_cmd_prog,
				validate_command->val.val_str);
	/* GDS Specific Code -- END */
			(void) strcpy(cmd, validate_cmd_prog);
			(void) strtok(cmd, " ");
	/* GDS Specific Code -- BEGIN */
		}
	}
	/* GDS Specific Code -- END */
		if (strcmp(validate_cmd_prog, "") != 0) {
			if (stat(cmd, &statbuf) != 0) {
			scds_syslog(LOG_ERR, "Cannot access the %s command "
					"<%s> : <%s>", "validate",
					cmd, strerror(errno));
/* GDS Specific Code -- BEGIN */
				(int)gds_publish_eventlog(GDS_START,
					GDS_LOG_ERR, scds_handle,
					"Cannot access the %s command "
					"<%s> : <%s>", "validate",
					cmd, strerror(errno));
/* GDS Specific Code -- END */
				rc = 1;
				goto finished;
			}

			if (!(statbuf.st_mode & S_IXUSR)) {
				scds_syslog(LOG_ERR,
					"The %s command does not have execute "
					"permissions: <%s>", "validate",
					cmd);
/* GDS Specific Code -- BEGIN */
				(int)gds_publish_eventlog(GDS_START,
					GDS_LOG_ERR, scds_handle,
					"%s command does not have execute "
					"permissions: <%s>", "validate",
					cmd);
/* GDS Specific Code -- END */
				rc = 1;
				goto finished;
			}
		}
	for (i = 0; i < no_start_cmd; i++) {
		/*
		 * Make sure that xfs start command exists and that the
		 * permissions are correct.
		 */
		/* GDS Specific Code -- BEGIN */
#ifdef GDS
		(void) strcpy(cmd, gds_start_cmds[i]);
		(void) strtok(cmd, " ");
#else
		/* GDS Specific Code -- END */
		(void) strcpy(cmd, start_cmd_prog[i]);
		/* GDS Specific Code -- BEGIN */
#endif
		/* GDS Specific Code -- END */

		if (stat(cmd, &statbuf) != 0) {
			scds_syslog(LOG_ERR,
			    "Cannot access the %s command <%s> : <%s>",
			    "start", cmd, strerror(errno));
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_START, GDS_LOG_ERR,
			    scds_handle,
			    "Cannot access the start command <%s> : <%s>",
			    cmd, strerror(errno));
/* GDS Specific Code -- END */
			rc = 1;
			goto finished;
		}

		if (!(statbuf.st_mode & S_IXUSR)) {
			scds_syslog(LOG_ERR,
			    "The %s command does not have execute "
			    "permissions: <%s>", "start", cmd);
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_START, GDS_LOG_ERR,
			    scds_handle,
		    "Start command does not have execute permissions: <%s>",
			    cmd);
/* GDS Specific Code -- END */
			rc = 1;
			goto finished;
		}
	}

	/*
	 * Make sure that if xfs stop command is specified, then it exists
	 * and that the permissions are correct.
	 */
	if (strcmp(stop_cmd_prog, "") != 0) {
		if (stat(stop_cmd_prog, &statbuf) != 0) {
			scds_syslog(LOG_ERR,
			    "Cannot access the %s command <%s> : <%s>",
			    "stop", stop_cmd_prog, strerror(errno));
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_START, GDS_LOG_ERR,
			    scds_handle,
			    "Cannot access the stop command <%s> : <%s>",
			    stop_cmd_prog, strerror(errno));
/* GDS Specific Code -- END */
			rc = 1;
			goto finished;
		}

		if (!(statbuf.st_mode & S_IXUSR)) {
			scds_syslog(LOG_ERR,
			    "The %s command does not have execute "
			    "permissions: <%s>", "stop", stop_cmd_prog);
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_START, GDS_LOG_ERR,
			    scds_handle,
		    "Stop command does not have execute permissions: <%s>",
			    stop_cmd_prog);
/* GDS Specific Code -- END */

			rc = 1;
			goto finished;
		}
	}

	/*
	 * Make sure that if xfs probe command is specified, then it exists
	 * and that the permissions are correct.
	 */
	if (strcmp(probe_cmd_prog, "") != 0) {
		if (stat(probe_cmd_prog, &statbuf) != 0) {
			scds_syslog(LOG_ERR,
			    "Cannot access the %s command <%s> : <%s>",
			    "probe", probe_cmd_prog, strerror(errno));
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_START, GDS_LOG_ERR,
			    scds_handle,
			    "Cannot access the probe command <%s> : <%s>",
			    probe_cmd_prog, strerror(errno));
/* GDS Specific Code -- END */
			rc = 1;
			goto finished;
		}

		if (!(statbuf.st_mode & S_IXUSR)) {
			scds_syslog(LOG_ERR,
			    "The %s command does not have execute "
			    "permissions: <%s>", "probe", probe_cmd_prog);
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_START, GDS_LOG_ERR,
			    scds_handle,
		    "Probe command does not have execute permissions: <%s>",
			    probe_cmd_prog);
/* GDS Specific Code -- END */
			rc = 1;
			goto finished;
		}
	}

/* GDS Specific Code -- BEGIN */
#ifdef GDS
	if (validate_flag == B_TRUE) {
		(void) strcpy(validate_cmd_prog,
				validate_command->val.val_str);
#endif
/* GDS Specific Code -- END */
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Running User defined Validation %s",
			validate_cmd_prog);
		if (strcmp(validate_cmd_prog, "") != 0) {
			if (run_user_supplied_validate(scds_handle,
						validate_cmd_prog)) {
				/* User defined Validation Failure */
				rc = 1;
				goto finished;
			}
		}
/* GDS Specific Code -- BEGIN */
#ifdef GDS
	}
#endif
/* GDS Specific Code -- END */
global_checks:
	/* GDS Specific Code -- BEGIN */
	rc = scds_get_ext_property(scds_handle, "network_aware",
	    SCHA_PTYPE_BOOLEAN, &network_aware_prop);

	if (rc == SCHA_ERR_NOERR) {
		network_aware = network_aware_prop->val.val_boolean;
		/*
		 * SCMSGS
		 * @explanation
		 * Resource property Network_aware is set to the given value.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_INFO,
		    "Extension property <network_aware> has a value "
		    "of <%d>", network_aware);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Property Network_aware might not be defined in RTR file.
		 * Use the default value of true.
		 * @user_action
		 * This is an informational message; no user action is needed.
		 */
		scds_syslog(LOG_INFO,
		    "Either extension property <network_aware> is not "
		    "defined, or an error occured while retrieving this "
		    "property; using the default value of TRUE.");
	}

	if (!network_aware) {
		/*
		 * Since this is a non-network-aware application, there is
		 * no need to do the network related checks. Since all other
		 * checks have passed, we jump to "finished" here.
		 */
		goto finished;
	}
	/* GDS Specific Code -- END */

	/* Network aware applications code -- BEGIN */
	/*
	 * Network aware service should have at least one port specified
	 */

	rc = scds_get_port_list(scds_handle, &portlist);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_PORT_LIST, scds_error_string(rc));
		goto finished;
	}

	if (portlist == NULL || portlist->num_ports < 1) {
		scds_syslog(LOG_ERR,
		    "Property %s is not set.", SCHA_PORT_LIST);
		rc = 1;
		goto finished;
	} else {
		/*
		 * log debug messages that simple probe will be ignored for
		 * non-tcp protocols
		 */
		for (num_ports = 0; num_ports < portlist->num_ports;
				num_ports++) {
			protocol = portlist->ports[num_ports].proto;
			if (protocol != SCDS_IPPROTO_TCP ||
				protocol != SCDS_IPPROTO_TCP6) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"Simple probe is ignored for"
					" port %d configured on a non-tcp "
					"protocol.",
					portlist->ports[num_ports].port);
			}
		}
	}
	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Error in trying to access the configured network "
		    "resources : %s.", scds_error_string(rc));
		goto finished;
	}

	/* Return an error if there are no network address resources */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		rc = 1;
		goto finished;
	}

	/* Check to make sure other important extension props are set */
	if (scds_get_ext_monitor_retry_count(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
		"Monitor_retry_count or Monitor_retry_interval is not set.");
		rc = 1; /* Validation Failure */
		goto finished;
	}
	if (scds_get_ext_monitor_retry_interval(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
		"Monitor_retry_count or Monitor_retry_interval is not set.");
		rc = 1; /* Validation Failure */
		goto finished;
	}
	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

	/* All validation checks were successful */

	/* Network aware applications code -- END */

finished:
	/* GDS Specific Code -- BEGIN */
	if (validate_command)
		scds_free_ext_property(validate_command);
	if (start_commands)
		scds_free_ext_property(start_commands);
	if (stop_command)
		scds_free_ext_property(stop_command);
	if (probe_command)
		scds_free_ext_property(probe_command);
	if (network_aware_prop)
		scds_free_ext_property(network_aware_prop);

	free(gds_start_cmds);
	/* GDS Specific Code -- END */

	/* Network aware applications code -- BEGIN */
	scds_free_net_list(snrlp);
	scds_free_port_list(portlist);
	/* Network aware applications code -- END */

	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

	return (rc); /* return result of validation */
}

/*
 * svc_start():
 *
 */

int
svc_start(scds_handle_t scds_handle)
{
	int rc = 0;
	char *service_start_cmd[] = {
	""
	};
	int no_start_cmd = 0;
	int i;
	char cmd[SCDS_CMD_SIZE];
	scha_extprop_value_t *child_mon_level_prop = NULL;
	int child_mon_level;

	/* GDS Specific Code -- BEGIN */
	scha_extprop_value_t *start_commands = NULL;
	char **gds_start_cmds = NULL;
	/* GDS Specific Code -- END */

	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

	/* GDS Specific Code -- BEGIN */
	if ((rc = scds_get_ext_property(scds_handle, "Start_command",
	    SCHA_PTYPE_STRINGARRAY, &start_commands)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve resource <%s> extension property <%s>",
		    scds_get_resource_name(scds_handle), "Start_command");
		goto finished;
	}

	no_start_cmd = start_commands->val.val_strarray->array_cnt;

	gds_start_cmds = (char **)malloc(no_start_cmd * sizeof (char *));
	if (gds_start_cmds == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		rc = SCHA_ERR_NOMEM;
		goto finished;
	}

	for (i = 0; i < no_start_cmd; i++) {
		gds_start_cmds[i] =
		    start_commands->val.val_strarray->str_array[i];
	}
	/* GDS Specific Code -- END */

	rc = scds_get_ext_property(scds_handle, "Child_mon_level",
	    SCHA_PTYPE_INT, &child_mon_level_prop);

	if (rc == SCHA_ERR_NOERR) {
		child_mon_level = child_mon_level_prop->val.val_int;
		scds_syslog(LOG_INFO,
		    "Extension property <Child_mon_level> has a value of <%d>",
		    child_mon_level);
	} else {
		scds_syslog(LOG_INFO,
		    "Either extension property <Child_mon_level> is not "
		    "defined, or an error occurred while retrieving this "
		    "property; using the default value of -1.");
		child_mon_level = -1;
	}

	for (i = 0; i < no_start_cmd; i++) {
		/*
		 * Start XFS under PMF.
		 */

		/* GDS Specific Code -- BEGIN */
#ifdef GDS
		(void) strcpy(cmd, gds_start_cmds[i]);
#else
		/* GDS Specific Code -- END */
		(void) strcpy(cmd, service_start_cmd[i]);
		/* GDS Specific Code -- BEGIN */
#endif
		/* GDS Specific Code -- END */

		if ((rc = preprocess_cmd(scds_handle, cmd, SCDS_CMD_SIZE))
		    != 0) {
			scds_syslog(LOG_ERR,
			    "Failed to form the %s command.", "start");
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_START, GDS_LOG_ERR,
			    scds_handle,
			    "Failed to form the start command");
/* GDS Specific Code -- END */
			goto finished;
		}

		/* User added code -- BEGIN vvvvvvvvvvvvvvv */
		/* User added code -- END   ^^^^^^^^^^^^^^^ */

		rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC, i, cmd,
		    child_mon_level);

		if (rc == SCHA_ERR_NOERR) {
			scds_syslog(LOG_INFO,
				    "Start of %s completed successfully.", cmd);
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_START, GDS_LOG_INFO,
			    scds_handle, "Start succeeded. [%s]", cmd);
/* GDS Specific Code -- END */
		} else {
			scds_syslog(LOG_ERR, "Failed to start %s.", cmd);
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_START, GDS_LOG_ERR,
			    scds_handle, "Start failed. [%s]", cmd);
/* GDS Specific Code -- END */
			goto finished;
		}

		/* User added code -- BEGIN vvvvvvvvvvvvvvv */
		/* User added code -- END   ^^^^^^^^^^^^^^^ */
	}

finished:
	/* GDS Specific Code -- BEGIN */
	scds_free_ext_property(start_commands);
	scds_free_ext_property(child_mon_level_prop);
	free(gds_start_cmds);
	/* GDS Specific Code -- END */

	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */
	return (rc); /* return Success/failure status */
}

/*
 * svc_stop():
 *
 * Stop the XFS server
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle)
{
	int no_start_cmd = 0;
	int i;
	int rc = 0, cmd_exit_code = 0;
	char service_stop_cmd[SCDS_CMD_SIZE] = "";
	int stop_smooth_timeout =
	    (scds_get_rs_stop_timeout(scds_handle) * SVC_SMOOTH_PCT) / 100;
	int stop_hard_timeout =
	    (scds_get_rs_stop_timeout(scds_handle) * SVC_HARD_PCT) / 100;
	char cmd[SCDS_CMD_SIZE];
	scha_extprop_value_t *stop_signal_prop = NULL;
	int stop_signal = SIGTERM;

	/* GDS Specific Code -- BEGIN */
	scha_extprop_value_t *start_commands = NULL;
	scha_extprop_value_t *stop_command = NULL;

	if ((rc = scds_get_ext_property(scds_handle, "Start_command",
	    SCHA_PTYPE_STRINGARRAY, &start_commands)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve resource <%s> extension property <%s>",
		    scds_get_resource_name(scds_handle), "Start_command");
		/*
		 * In this case, we bail out right away. This may seem somewhat
		 * harsh. However, considering that in this case we don't even
		 * know how many start commands there are. So we won't know
		 * how many times the loop at "send_kill" should iterate and
		 * might end up leaving some pmf tags hanging behind.
		 */
		goto finished;
	}

	no_start_cmd = start_commands->val.val_strarray->array_cnt;

	if ((rc = scds_get_ext_property(scds_handle, "Stop_command",
	    SCHA_PTYPE_STRING, &stop_command)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve resource <%s> extension property <%s>",
		    scds_get_resource_name(scds_handle), "Stop_command");
		(int)gds_publish_eventlog(GDS_STOP, GDS_LOG_ERR, scds_handle,
		    "Failed to retrieve resource <%s> extension property <%s>",
		    scds_get_resource_name(scds_handle), "Stop_command");
		/*
		 * This error is less severe than the previous one. We do know
		 * how many start commands there were, but are not able to
		 * retrieve the stop command. Therefore, we goto send_kill.
		 * Also, notice that "rc" will get overwritten in the loop at
		 * send_kill.
		 */
		goto send_kill;
	}

	if ((stop_command->val.val_str != NULL) &&
	    (strcmp(stop_command->val.val_str, "") != 0)) {
		(void) strcpy(service_stop_cmd, stop_command->val.val_str);
	}
	/* GDS Specific Code -- END */

	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

	rc = scds_get_ext_property(scds_handle, "Stop_signal", SCHA_PTYPE_INT,
	    &stop_signal_prop);

	if (rc == SCHA_ERR_NOERR) {
		stop_signal = stop_signal_prop->val.val_int;
		/*
		 * SCMSGS
		 * @explanation
		 * Resource property stop_signal is set to a value or has a
		 * default value.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_INFO,
		    "Extension property <stop_signal> has a value of <%d>",
		    stop_signal);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Property stop_signal might not be defined in RTR file. The
		 * process continues with the default value of SIGTERM.
		 * @user_action
		 * This is an informational message; no user action is needed.
		 */
		scds_syslog(LOG_INFO,
		    "Either extension property <stop_signal> is not defined, "
		    "or an error occurred while retrieving this property; "
		    "using the default value of SIGTERM.");
/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_STOP, GDS_LOG_INFO, scds_handle,
		    "Either extension property <stop_signal> is not defined, "
		    "or an error occurred while retrieving this property "
		    "using the default value of SIGTERM.");
/* GDS Specific Code -- END */
	}

	if (strcmp(service_stop_cmd, "") != 0) {
		for (i = 0; i < no_start_cmd; i++) {

			/* User added code -- BEGIN vvvvvvvvvvvvvvv */
			/* User added code -- END   ^^^^^^^^^^^^^^^ */

			/*
			 * First take the command out of PMF monitoring,
			 * so that it doesn't keep restarting it.
			 */
			rc = scds_pmf_stop_monitoring(scds_handle,
			    SCDS_PMF_TYPE_SVC, i);
			if (rc != SCHA_ERR_NOERR) {
				/*
				 * SCMSGS
				 * @explanation
				 * Process monitor facility failed to stop
				 * monitoring the applicatin. The stop method
				 * will send SIGKILL to stop the application.
				 * @user_action
				 * No action required.
				 */
				scds_syslog(LOG_ERR,
				    "Failed to take the resource out of PMF "
				    "control. Sending SIGKILL now.");

/* GDS Specific Code -- BEGIN */
				(int)gds_publish_eventlog(GDS_STOP, GDS_LOG_ERR,
				    scds_handle,
				    "Failed to take the resource out of PMF "
				    "control. Sending SIGKILL now.");
/* GDS Specific Code -- END */
				goto send_kill;
			}
		}

		/* User added code -- BEGIN vvvvvvvvvvvvvvv */
		/* User added code -- END   ^^^^^^^^^^^^^^^ */

		/*
		 * First try to stop the application using the stop command
		 * provided.
		 */
		(void) strcpy(cmd, service_stop_cmd);
		if ((rc = preprocess_cmd(scds_handle, cmd, SCDS_CMD_SIZE))
		    != 0) {
			scds_syslog(LOG_ERR,
			    "Failed to form the %s command.", "stop");
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_STOP, GDS_LOG_ERR,
			    scds_handle,
			    "Failed to form the %s command.", "stop");
/* GDS Specific Code -- END */

			/*
			 * We failed to preprocess the stop command so can't
			 * use that. We still proceed to send KILL signal and
			 * try to stop the application anyway.
			 */
			goto send_kill;
		}

		rc = scds_timerun(scds_handle, cmd, stop_smooth_timeout,
		    SIGKILL, &cmd_exit_code);

		if (rc != 0 || cmd_exit_code != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * The user provided stop command cannot stop the
			 * application. Will re-attempt to stop the
			 * application by sending SIGKILL to the pmf tag.
			 * @user_action
			 * No action required.
			 */
			scds_syslog(LOG_ERR,
			    "The stop command <%s> failed to stop the "
			    "application. Will now use SIGKILL to stop the "
			    "application.",  cmd);
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_STOP, GDS_LOG_ERR,
			    scds_handle,
			    "Stop failed [%s]. Will use SIGKILL.", cmd);
		} else {
			(int)gds_publish_eventlog(GDS_STOP, GDS_LOG_INFO,
			    scds_handle, "Stop succeeded [%s].", cmd);
/* GDS Specific Code -- END */
		}

		/*
		 * Regardless of whether the command succeeded or not we
		 * send KILL signal to the pmf tag. This will ensure that
		 * the process tree goes away if it still exists. If it
		 * doesn't exist by then, we return NOERR.
		 */
		goto send_kill;
	} else {
		for (i = 0; i < no_start_cmd; i++) {
			/* User added code -- BEGIN vvvvvvvvvvvvvvv */
			/* User added code -- END   ^^^^^^^^^^^^^^^ */

			/*
			 * If no stop command is specified, we use
			 * scds_pmf_stop to stop the application.
			 */
			if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC,
			    i, stop_signal,
			    scds_get_rs_stop_timeout(scds_handle))) !=
			    SCHA_ERR_NOERR) {
				scds_syslog(LOG_ERR,
				    "Failed to stop %s.", "the application");

/* GDS Specific Code -- BEGIN */
				(int)gds_publish_eventlog(GDS_STOP, GDS_LOG_ERR,
				    scds_handle,
				    "Failed to stop %s.", "the application");
/* GDS Specific Code -- END */

				/*
				 * Since the Data service did not stop with a
				 * scds_pmf_stop, we return non-zero.
				 */
				goto finished;
			}
		}
		goto finished;
	}

send_kill:
	for (i = 0; i < no_start_cmd; i++) {
		/* User added code -- BEGIN vvvvvvvvvvvvvvv */
		/* User added code -- END   ^^^^^^^^^^^^^^^ */

		/*
		 * Since all else failed, send SIGKILL to stop the application.
		 * Notice that this call will return with success, even if
		 * the tag does not exist by now.
		 */
		if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC, i,
		    SIGKILL, stop_hard_timeout)) != SCHA_ERR_NOERR) {
			/*
			 * Failed to stop the application even with SIGKILL,
			 * bail out now.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * The stop method failed to stop the application with
			 * SIGKILL.
			 * @user_action
			 * Use pmfadm(1M) with the -L option to retrieve all
			 * the tags that are running on the server. Identify
			 * the tag name for the application in this resource.
			 * This can be easily identified as the tag ends in
			 * the string ".svc" and contains the resource group
			 * name and the resource name. Then use pmfadm(1M)
			 * with the -s option to stop the application. If the
			 * error still persists, then reboot the node.
			 */
			scds_syslog(LOG_ERR,
			    "Failed to stop the application with SIGKILL. "
			    "Returning with failure from stop method.");

/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_STOP, GDS_LOG_ERR,
			    scds_handle,
			    "Failed to stop the application with SIGKILL. "
			    "Returning with failure from stop method.");
/* GDS Specific Code -- END */

			goto finished;
		}
	}

	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

finished:
	/* GDS Specific Code -- BEGIN */

	scds_free_ext_property(start_commands);
	scds_free_ext_property(stop_command);

	/* GDS Specific Code -- END */

	scds_free_ext_property(stop_signal_prop);

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog(LOG_INFO, "Successfully stopped the application");

/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_STOP, GDS_LOG_INFO, scds_handle,
		    "Successfully stopped the application");
/* GDS Specific Code -- END */
	}

	return (rc); /* Successfully stopped */
}

/* Network aware applications code -- BEGIN */
/*
 * svc_wait_network_aware():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait_network_aware(scds_handle_t scds_handle)
{
	int	rc, svc_start_timeout, probe_timeout;
	int	ip, port, proto;
	char	*hostname;
	scds_pmf_status_t	status;
	scha_err_t	err;
	scds_netaddr_list_t	*netaddr;

	/* obtain the network resource to use for probing */
	if (scds_get_netaddr_list(scds_handle, &netaddr)) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		return (1);
	}

	/* Return an error if there are no network resources */
	if (netaddr == NULL || netaddr->num_netaddrs == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		return (1);
	}

	/*
	 * Get the Start method timeout and the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up in order to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
	    != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	do {
		/* set run_probe to true */
		run_probe = B_TRUE;
		userprobe_already_run = B_FALSE;
		/*
		 * probe the data service on all the IP address
		 * of the network resource and the portname
		 */
		for (ip = 0; ip < netaddr->num_netaddrs; ip++) {
			/* Retrieve hostname, port and protocol */
			hostname = netaddr->netaddrs[ip].hostname;
			port = netaddr->netaddrs[ip].port_proto.port;
			proto = netaddr->netaddrs[ip].port_proto.proto;

			rc = svc_probe_network_aware(scds_handle, hostname,
					port, proto, probe_timeout);
			if (rc != SCHA_ERR_NOERR) {
				break;
			}
		}
		/*
		 * If all the probes were successful, return success
		 * or else continue.
		 */
		if (rc == SCHA_ERR_NOERR || rc == NON_TCP_PORT) {
			/* Success. Free up resources and return */
			scds_free_netaddr_list(netaddr);
			return (SCHA_ERR_NOERR);
		}

		err = scds_pmf_get_status(scds_handle,
		    SCDS_PMF_TYPE_SVC, SCDS_PMF_SINGLE_INSTANCE, &status);
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
			    "Failed to retrieve process monitor facility tag.");
			return (1);
		}

		/* Check if the dataservice is still up and running */
		if (status != SCDS_PMF_MONITORED) {
			/*
			 * SCMSGS
			 * @explanation
			 * The application being started under pmf has exited.
			 * Either the user has decided to stop monitoring this
			 * process, or the process exceeded the number of
			 * retries. An error message is output to syslog.
			 * @user_action
			 * Check syslog messages and correct the problems
			 * specified in prior syslog messages. If the error
			 * still persists, report this problem.
			 */
			scds_syslog(LOG_ERR, "Application failed to stay up. "
			    "Start method Failure.");
			return (1);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		(void) scds_svc_wait(scds_handle, SVC_WAIT_TIME);

	/* We rely on RGM to timeout and terminate the program */
	} while (1);

}
/* Network aware applications code -- END */
/* Stand alone applications code -- BEGIN */
/*
 * svc_wait_non_network_aware():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait_non_network_aware(scds_handle_t scds_handle)
{
	int rc, svc_start_timeout, probe_timeout;
	scds_pmf_status_t	status;
	scha_err_t err;

	/*
	 * Get the Start method timeout and the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up in order to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
	    != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	do {
		rc = svc_probe_non_network_aware(scds_handle, probe_timeout);
		if (rc == SCHA_ERR_NOERR) {
			return (SCHA_ERR_NOERR);
		}

		err = scds_pmf_get_status(scds_handle,
		    SCDS_PMF_TYPE_SVC, SCDS_PMF_SINGLE_INSTANCE, &status);
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
			    "Failed to retrieve process monitor facility tag.");
			return (1);
		}

		/*
		 * Check if the dataservice is still up and running
		 */
		if (status != SCDS_PMF_MONITORED) {
			scds_syslog(LOG_ERR, "Application failed to stay up. "
			    "Start method Failure.");
			return (1);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		(void) scds_svc_wait(scds_handle, SVC_WAIT_TIME);

	/* We rely on RGM to timeout and terminate the program */
	} while (1);

}
/* Stand alone applications code -- END */

/*
 * This function starts the fault monitor for a XFS resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;
	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe xfs_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */

	/* GDS Specific Code -- BEGIN */
#ifdef GDS
	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, "gds_probe", 0);
#else
	/* GDS Specific Code -- END */
	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, "xfs_probe", 0);
	/* GDS Specific Code -- BEGIN */
#endif
	/* GDS Specific Code -- END */

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a XFS resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;
	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
	    scds_get_rs_monitor_stop_timeout(scds_handle));

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}

/*
 * run_user_supplied_probe:
 * This routine runs the user-supplied probe command, if one is defined,
 * and returns its exit status.
 * If no user-supplied probe command is defined, or an error occurs while
 * retrieving it, this routine returns -1. The calling routine may choose
 * to do "simple probing" (applicable only to network aware applications,
 * or do nothing (applicable to non-network-aware applications.
 */

int
run_user_supplied_probe(scds_handle_t scds_handle, int timeout)
{
	int rc = 0;
	int probe_status = 0;
	int cmd_exit_status = 0;
	static char service_probe_cmd[SCDS_CMD_SIZE] = "";
	char cmd[SCDS_CMD_SIZE];
	/* GDS Specific Code -- BEGIN */
	scha_extprop_value_t *probe_command = NULL;

	if (strcmp(service_probe_cmd, "") == 0) {
		if ((rc = scds_get_ext_property(scds_handle, "Probe_command",
		    SCHA_PTYPE_STRING, &probe_command)) != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * The fault monitor failed to retrieve the probe
			 * command from the cluster configuration. It will
			 * continue using the simple probe to monitor the
			 * application.
			 * @user_action
			 * No action required.
			 */
			scds_syslog(LOG_ERR,
			    "Failed to retrieve the probe command with "
			    "error <%d>. Will continue to do the simple probe.",
			    rc);
			/*
			 * If we fail to retrieve the user specified probe
			 * command, we log an error message and proceed to do
			 * the simple probe, i.e. the tcp connect/disconnect
			 * probing (only for network aware applications).
			 */
			return (-1);
		}

		if ((probe_command->val.val_str != NULL) &&
		    (strcmp(probe_command->val.val_str, "") != 0)) {
			(void) strcpy(service_probe_cmd,
			    probe_command->val.val_str);
		}
	}
	/* GDS Specific Code -- END */

	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

	if (strcmp(service_probe_cmd, "") != 0) {

		/*
		 * probe command is specified; so use that to probe the
		 * application.
		 */
		(void) strcpy(cmd, service_probe_cmd);
		if ((rc = preprocess_cmd(scds_handle, cmd, SCDS_CMD_SIZE))
		    != 0) {
			scds_syslog(LOG_ERR,
			    "Failed to form the %s command.", "probe");

			/*
			 * If we fail to preprocess the user specified probe
			 * command we log an error message and proceed to do
			 * the simple probe, i.e. the tcp connect/disconnect
			 * probing (only for network aware applications).
			 */
			return (-1);
		}

		rc = scds_timerun(scds_handle, cmd, timeout, SIGKILL,
		    &cmd_exit_status);
		if (rc == SCHA_ERR_TIMEOUT) {
			scds_syslog(LOG_ERR,
			    "The probe command <%s> timed out", cmd);
/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_PROBE, GDS_LOG_ERR,
			    scds_handle, "Probe timed out [%s]", cmd);
/* GDS Specific Code -- END */

			/*
			 * We return half of the SCDS_PROBE_COMPLETE_FAILURE,
			 * so that two failures in a row will be required
			 * for any action to be taken.
			 */
			probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
			goto finished;
		}
		if (rc == SCHA_ERR_INTERNAL) {
			char internal_err_str[SCDS_CMD_SIZE];

			(void) snprintf(internal_err_str, SCDS_CMD_SIZE,
			    "error occurred while launching "
			    "the probe command <%s>", cmd);

			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			    internal_err_str);

/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_PROBE, GDS_LOG_ERR,
			    scds_handle,
			    "Probe launch failed. Error is %s [%s]",
			    internal_err_str, cmd);
/* GDS Specific Code -- END */

			/*
			 * An internal error is likely to be caused by some
			 * system level problem (out of swap space etc.).
			 * Since most of these problems are transient, we
			 * return half of the SCDS_PROBE_COMPLETE_FAILURE,
			 * so that two failures in a row will be required
			 * for any action to be taken.
			 */
			probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
			goto finished;
		}
		if (rc == SCHA_ERR_INVAL) {
			/*
			 * SCMSGS
			 * @explanation
			 * Failure in executing the command.
			 * @user_action
			 * Check the syslog message for the command
			 * description. Check whether the system is low in
			 * memory or the process table is full and take
			 * appropriate action. Make sure that the executable
			 * exists.
			 */
			scds_syslog(LOG_ERR,
			    "Cannot execute %s: %d.", cmd, cmd_exit_status);
			probe_status = SCDS_PROBE_COMPLETE_FAILURE;

/* GDS Specific Code -- BEGIN */
			(int)gds_publish_eventlog(GDS_PROBE, GDS_LOG_ERR,
			    scds_handle, "Probe couldn't be executed [%s]",
			    cmd);
/* GDS Specific Code -- END */

			goto finished;
		}

		/*
		 * return the exit status of the command.
		 */
		probe_status = cmd_exit_status;

/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_PROBE, GDS_LOG_INFO, scds_handle,
		    "Probe has been executed with exit code %d [%s]",
		    cmd_exit_status, cmd);
/* GDS Specific Code -- END */

		goto finished;
	} else {
		/*
		 * No probe command is specified, set return value to -1.
		 */
		probe_status = -1;
		goto finished;
	}

finished:
	/* GDS Specific Code -- BEGIN */
	scds_free_ext_property(probe_command);
	/* GDS Specific Code -- END */
	return (probe_status);
}

/* Stand alone applications code -- BEGIN */
/*
 * probe routine for non-network-aware agents.
 */

int
svc_probe_non_network_aware(scds_handle_t scds_handle, int timeout)
{
	int rc = 0;

	rc = run_user_supplied_probe(scds_handle, timeout);

	if (rc == -1) {
		/*
		 * No user supplied probe defined.
		 * Since this is a non-network-aware application, there is
		 * nothing more to do. Consider the probe succeeded and return
		 * 0 (success);
		 */
		return (0);
	} else {
		/*
		 * The user has defined a probe command for this non-network
		 * aware application. Simply return the exit status of the
		 * the user-supplied probe.
		 */
		return (rc);
	}
}
/* Stand alone applications code -- END */

/* Network aware applications code -- BEGIN */
/*
 * do_simple_probe:
 * If the probe command is not specified, the following routine is used
 * to do simple probing, which simply connects to the port and disconnects.
 */
int
do_simple_probe(scds_handle_t scds_handle, char *hostname, int port,
    int timeout)
{
	int  rc = 0;
	int probe_status = 0;
	ulong_t t1, t2;
	int sock;
	int time_used, time_remaining;
	ulong_t	connect_timeout;


	/*
	 * probe the dataservice by doing a socket connection to the port
	 * specified in the port_list property to the host that is
	 * serving the XFS dataservice. If the XFS service which is configured
	 * to listen on the specified port, replies to the connection, then
	 * the probe is successful. Else we will wait for a time period set
	 * in probe_timeout property before concluding that the probe failed.
	 */

	/*
	 * Use the SVC_CONNECT_TIMEOUT_PCT percentage of timeout
	 * to connect to the port
	 */
	connect_timeout = (SVC_CONNECT_TIMEOUT_PCT * timeout)/100;
	t1 = gethrtime()/1E9;

	/*
	 * the probe makes a connection to the specified hostname and port.
	 * The connection is timed for 95% of the actual probe_timeout.
	 */
	rc = scds_fm_tcp_connect(scds_handle, &sock, hostname, port,
	    connect_timeout);
	if (rc) {
		scds_syslog(LOG_ERR,
		    "Failed to connect to the host <%s> and port <%d>.",
		    hostname, port);

/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_PROBE, GDS_LOG_ERR, scds_handle,
		    "Failed to connect to the host <%s> and port <%d>.",
		    hostname, port);
/* GDS Specific Code -- END */

		/* this is a complete failure */
		probe_status = SCDS_PROBE_COMPLETE_FAILURE;
		goto finished;
	}

/* GDS Specific Code -- BEGIN */
	(int)gds_publish_eventlog(GDS_PROBE, GDS_LOG_INFO, scds_handle,
	    "Succeeded to connect to the host <%s> and port <%d>.",
	    hostname, port);
/* GDS Specific Code -- END */

	t2 = gethrtime()/1E9;

	/*
	 * Compute the actual time it took to connect. This should be less than
	 * or equal to connect_timeout, the time allocated to connect.
	 * If the connect uses all the time that is allocated for it,
	 * then the remaining value from the probe_timeout that is passed to
	 * this function will be used as disconnect timeout. Otherwise, the
	 * the remaining time from the connect call will also be added to
	 * the disconnect timeout.
	 *
	 */

	time_used = t2 -t1;

	/*
	 * Use the remaining time(timeout - time_took_to_connect) to disconnect
	 */

	time_remaining = timeout - (int)time_used;

	/*
	 * If all the time is used up, use a small hardcoded timeout
	 * to still try to disconnect. This will avoid the fd leak.
	 */
	if (time_remaining <= 0) {
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "svc_probe used entire timeout of "
		    "%d seconds during connect operation and exceeded the "
		    "timeout by %d seconds. Attempting disconnect with timeout"
		    " %d ",
		    connect_timeout,
		    abs(time_used),
		    SVC_DISCONNECT_TIMEOUT_SECONDS);

/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_PROBE, GDS_LOG_INFO, scds_handle,
		    "svc_probe used entire timeout of "
		    "%d seconds during connect operation and exceeded the "
		    "timeout by %d seconds. Attempting disconnect with timeout"
		    " %d ",
		    connect_timeout,
		    abs(time_used),
		    SVC_DISCONNECT_TIMEOUT_SECONDS);
/* GDS Specific Code -- END */

		time_remaining = SVC_DISCONNECT_TIMEOUT_SECONDS;
	}

	/*
	 * Return partial failure in case of disconnection failure.
	 * Reason: The connect call is successful, which means
	 * the application is alive. A disconnection failure
	 * could happen due to a hung application or heavy load.
	 * If it is the later case, don't declare the application
	 * as dead by returning complete failure. Instead, declare
	 * it as partial failure. If this situation persists, the
	 * disconnect call will fail again and the application will be
	 * restarted.
	 */
	rc = scds_fm_tcp_disconnect(scds_handle, sock, time_remaining);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to disconnect from port %d of resource %s.",
		    port, scds_get_resource_name(scds_handle));

/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_PROBE, GDS_LOG_ERR, scds_handle,
		    "Failed to disconnect from port %d of resource %s.",
		    port, scds_get_resource_name(scds_handle));
/* GDS Specific Code -- END */

		/* this is a partial failure */
		probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}

	t2 = gethrtime()/1E9;
	time_used = t2 -t1;
	time_remaining = timeout - time_used;

	/*
	 * If there is no time left, don't do the full test with
	 * fsinfo. Return SCDS_PROBE_COMPLETE_FAILURE/2
	 * instead. This will make sure that if this timeout
	 * persists, server will be restarted.
	 */
	if (time_remaining <= 0) {
		scds_syslog(LOG_ERR, "Probe timed out.");

/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_PROBE, GDS_LOG_ERR, scds_handle,
		    "Probe timed out.");
/* GDS Specific Code -- END */

		probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}

	/* User added code -- BEGIN vvvvvvvvvvvvvvv */
	/* User added code -- END   ^^^^^^^^^^^^^^^ */

finished:
	return (probe_status);
}


/*
 * set_status_msg(): Preserve the status and set only the status
 * message. We don't error out in case there is an error.
 */
void
set_status_msg(scds_handle_t scds_handle, char *msg)
{
	int	rc, status;
	char	*rsname = NULL, *rgname = NULL;
	scha_resource_t		local_handle;
	const char *zname = scds_get_zone_name(scds_handle);

	rc = scha_resource_open_zone(zname, scds_get_resource_name(scds_handle),
	    scds_get_resource_group_name(scds_handle), &local_handle);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_WARNING,
			"Failed to retrieve the resource handle: %s.",
			scds_error_string(rc));
		return;
	}

	if (zname == NULL) {
		rc = scha_resource_get(local_handle, SCHA_STATUS, &status);
	} else {
		char *fullname = NULL;

		rc = scds_get_fullname(zname, &fullname,
		    scds_is_zone_cluster(scds_handle));
		if (rc == SCHA_ERR_NOERR) {
			rc = scha_resource_get_zone(zname, local_handle,
			    SCHA_STATUS_NODE, fullname, &status);
			free(fullname);
		}
	}
	(void) scha_resource_close(local_handle);

	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to retrieve Resource STATUS.
		 * @user_action
		 * Contact your authorized Sun service provider to
		 * determine whether a workaround or patch is available.
		 */
		scds_syslog(LOG_WARNING, "INTERNAL ERROR: "
			"Failed to retrieve %s.", "STATUS");
		return;
	}

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	(void) scha_resource_setstatus_zone(zname, rsname, rgname,
	    SCHA_RSSTATUS_OK, msg);

	return;

}

/*
 * svc_probe_network_aware(): Do data service specific probing.
 * Return a integer value between 0 (success) and 100(complete failure).
 *
 * The probe does a simple socket connection to the XFS server on the specified
 * port which is configured as the resource extension property (Port_list) and
 * pings the dataservice. If the probe fails to connect to the port, we return
 * a value of 100 indicating that there is a total failure. If the connection
 * goes through and the disconnect to the port fails, then a value of 50 is
 * returned indicating a partial failure.
 *
 */
int
svc_probe_network_aware(scds_handle_t scds_handle, char *hostname,
    int port, int proto, int timeout)
{
	int rc = 0;

	/* run user defined probe only once */
	if (run_probe == B_TRUE && userprobe_already_run == B_FALSE) {
		rc = run_user_supplied_probe(scds_handle, timeout);
		userprobe_already_run = B_TRUE;

		if (rc != -1) {
			/*
			 * The user has defined a probe command, return
			 * the exit code and set run_probe to false.
			 */
			run_probe = B_FALSE;
			return (rc);
		}
	} else {
		/* force simple probe to run */
		rc = -1;
	}

	if (rc == -1 && run_probe == B_TRUE) {
		/*
		 * No user supplied probe defined or user supplied probe is not
		 * run. Since this is a network-aware application, run the
		 * simple probe (using the routine do_simple_probe) and
		 * simply bubble up its return code. The simple probe
		 * supports only tcp/tcp6 protocol.
		 */
		if (proto == SCDS_IPPROTO_TCP || proto == SCDS_IPPROTO_TCP6) {
			rc = do_simple_probe(scds_handle, hostname,
			    port, timeout);
			return (rc);
		}
		/* Return non tcp port error */
		return (NON_TCP_PORT);
	} else {
		/*
		 * The user has defined a probe command for this network
		 * aware application, which has already run.
		 * Simply return 0.
		 */
		return (0);
	}
}

/* Network aware applications code -- END */

/* The following are utility routines */

/*
 * This utility routine searches a given string, typically the commands
 * input to the Builder, for the occurrence of specific Builder defined
 * variables, e.g. $hostnames, and replace them with their appropriate value.
 *
 * The caller gives the output buf (and its size) where the new command has
 * to be put.
 */

int
preprocess_cmd(scds_handle_t scds_handle, char *cmd, int cmd_size)
{
	int rc = 0;
	cmdline_handle_t cmdline_handle = NULL;
	char *new_cmd = NULL;

	/* Substitute command line variables */

	rc = cmdline_open(scds_handle, &cmdline_handle);
	if (rc) {
/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_OTHER, GDS_LOG_ERR, scds_handle,
		    "gds_cmdline_open failed [%d]", rc);
/* GDS Specific Code -- END */
		return (rc);
	}

	new_cmd = cmdline_substitute_parameters(cmdline_handle, cmd);
	if (!new_cmd) {
/* GDS Specific Code -- BEGIN */
		(int)gds_publish_eventlog(GDS_OTHER, GDS_LOG_ERR, scds_handle,
		    "gds_substitute_parameters failed");
/* GDS Specific Code -- END */
		(int)cmdline_close(cmdline_handle);
		return (1);
	}

	strncpy(cmd, new_cmd, cmd_size);

	(int)cmdline_close(cmdline_handle);

	return (rc);
}
