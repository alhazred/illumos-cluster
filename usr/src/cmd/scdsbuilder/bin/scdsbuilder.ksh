#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident "@(#)scdsbuilder.ksh 1.15     08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# Define msg. file name and location
# NOTE: TEXTDOMAIN must be kept in sync with the TEXT_DOMAIN var in the
# makefiles.
typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

# check for java in original path in case it has JDK1.3 in a non-standard place
java -version > /dev/null 2>&1
if [[ $? -ne 0 ]]; then
			lmsg=`/usr/bin/gettext '\n\
Java not found.\n\
Exiting ...'`
			/usr/bin/printf "${lmsg}\n\n"
	exit 1
fi
jver=`java -version 2>&1 | /usr/bin/head -1 | /usr/bin/sed -e "s/[^0-9]*\([0-9]\)\.\([0-9]\)\.\([0-9]\).*/\1\2\3/"`
old_ver=`expr $jver \< 131`
if [[ $old_ver -ne 0 ]]; then
			lmsg=`/usr/bin/gettext '\n\
Required version (JDK1.3.1 or later) of Java not found.\n\
Exiting ...'`
			/usr/bin/printf "${lmsg}\n\n"
	exit 1
fi

# first find out if the c compiler -- cc -- exists in user's path
whence cc 1>/dev/null
if [[ $? != 0 ]]; then

	# cc does not exist
	if [[ -f rtconfig ]]; then
		# rtconfig file exists in the current dir, i.e. there already
		# exists a RT in the current dir

		# retrieve the RT_NAME and the SOURCE_TYPE
		RT_NAME=`/usr/bin/nawk -F= '/^RT_NAME/ {print $2}' rtconfig`
		SOURCE_TYPE=`/usr/bin/nawk -F= '/^SOURCE_TYPE/ {print $2}' rtconfig`

		# if it is C then exit immediately, because can't load
		# the RT in the current directory
		if [[ $SOURCE_TYPE == C ]]; then
			lmsg=`/usr/bin/gettext '\n\
Resource Type <%s> found in the current directory requires C compiler,\n\
which was not found in your path.\n\
Please make sure cc is in your path.\n\
Or run the Builder from a different directory.\n\
Exiting ...'`
			/usr/bin/printf "${lmsg}\n\n" "$RT_NAME"
			exit 1
		fi
	fi

	# Either no RT exists in the current directory, or
	# it's not a C RT
	# So launch the Wizard with the -k option, indicating that cc was not
	# found. we only set the KSH variable here, which will be used while
	# launching the Wizard later.
	KSH=-k
fi

java -classpath /usr/cluster/lib/scdsbuilder/classes \
	com.sun.scdsbuilder.SCDSBuilder $KSH
