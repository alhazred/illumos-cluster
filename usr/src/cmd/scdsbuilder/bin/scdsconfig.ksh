#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)scdsconfig.ksh 1.40	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# Define msg. file name and location
# NOTE: TEXTDOMAIN must be kept in sync with the TEXT_DOMAIN var in the
# makefiles.
typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

set +o noclobber

PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/ccs/bin:$PATH

VERBOSE=false

trap 'print "\n$0 interrupted - removing temp files and exiting ..." ;\
	rm *tmp; exit 1' 1 2 3 15

function run_cmd
{
	if [[ $VERBOSE == true ]]; then
		printf "%s\n" "$@"
	fi

	eval "$@"
	if [[ $? != 0 ]]; then
		lmsg=`gettext 'FAILED: %s'`
		printf "${lmsg}\n" "$@"
		exit 1
	fi
}

function usage
{
	lmsg=`gettext \
'Usage: %s -s <start command> [-u <start method timeout>]\n\
	[-e <validate command>] [-y <validate method timeout>]\n\
	[-t <stop command>] [-v <stop method timeout>]\n\
	[-m <probe command>] [-n <probe timeout>]\n\
	[-d <Working Directory>]'`
	printf "${lmsg}\n" `basename $0`
}

function build_pkg
{
	run_cmd "cd $RT_NEW_DIR/$RT_FULLNAME/etc"
	# Update the REV field in the pkginfo file
	run_cmd 'sed "s/^REV=.*$/REV=`date '+%Y.%m.%d.%H.%M'`/" \
		pkginfo > pkginfo.tmp'
	run_cmd "mv pkginfo.tmp pkginfo"

	lmsg=`gettext '\nCreating the package for the resource type %s ...'`
	printf "${lmsg}\n" $RT_NAME

	run_cmd "cd $RT_NEW_DIR/$RT_FULLNAME/src"
	run_cmd "make pkg"


	lmsg=`gettext '\n***\nThe package for the %1$s service has been created in:\n\
%2$s\n***'`
	printf "${lmsg}\n" $RT_NAME ${RT_NEW_DIR}/${RT_FULLNAME}/pkg

	printf "\n\n"
}

###############################################################################
# Parse program arguments.
#
function parse_args # [args ...]
{
	typeset opt

	while getopts 's:t:u:v:m:n:d:e:y:' opt
	do
		case "$opt" in
		s)
			# Start command for the resource type.
			START_COMMAND=$OPTARG
			;;
		t)
			# Stop command for the resource type.
			STOP_COMMAND=$OPTARG
			;;
		e)
			# Validate command for the resource type.
			VALIDATE_COMMAND=$OPTARG
			;;
		u)
			# Timeout for the start method.
			START_TIMEOUT=$OPTARG
			;;
		v)
			# Timeout for the stop method.
			STOP_TIMEOUT=$OPTARG
			;;
		y)
			# Timeout for Validate method.
			VALIDATE_TIMEOUT=$OPTARG
			;;
		m)
			# Probe command for the resource type.
			PROBE_COMMAND=$OPTARG
			;;
		n)
			# Probe Timeout for the start method.
			PROBE_TIMEOUT=$OPTARG
			;;
		d)
			# Name of the Working Directory.
			RT_NEW_DIR=$OPTARG
			echo $RT_NEW_DIR | egrep -e '^/' > /dev/null 2>&1
			if [[ $? != 0 ]]; then
				RT_NEW_DIR=`pwd`/$RT_NEW_DIR
			fi
			;;
		*)
			usage
		 	exit 1
			;;
		esac
	done

	shift $((OPTIND - 1))

	# Make sure there are no remaining args
	if [[ $# -ne 0 ]]; then
		usage
		exit 1
	fi
}

#########################
#			#
#	Main		#
#			#
#########################

parse_args "$@"

if [[ -z "$START_COMMAND" ]]; then
	lmsg=`gettext 'Start command for the resource type must be \
specified.'`
	printf "${lmsg}\n"
	usage
	exit 1
fi

echo $START_COMMAND | grep '&' > /dev/null 2>&1
if [[ $? == 0 ]]; then
	# the command contains '&', hence disallow it.
	lmsg=`gettext 'Start command cannot have the "&" character in it.'`
	printf "${lmsg}\n"
	exit 1
fi

echo $VALIDATE_COMMAND | grep '&' > /dev/null 2>&1
if [[ $? == 0 ]]; then
	# the command contains '&', hence disallow it.
	lmsg=`gettext 'Validate command cannot have the "&" character in it.'`
	printf "${lmsg}\n"
	exit 1
fi
echo $STOP_COMMAND | grep '&' > /dev/null 2>&1
if [[ $? == 0 ]]; then
	# the command contains '&', hence disallow it.
	lmsg=`gettext 'Stop command cannot have the "&" character in it.'`
	printf "${lmsg}\n"
	exit 1
fi

echo $PROBE_COMMAND | grep '&' > /dev/null 2>&1
if [[ $? == 0 ]]; then
	# the command contains '&', hence disallow it.
	lmsg=`gettext 'Probe command cannot have the "&" character in it.'`
	printf "${lmsg}\n"
	exit 1
fi

if [[ -z "$STOP_COMMAND" ]]; then
	lmsg=`gettext 'Stop command not specified ... will use signals to \
stop the resource type.'`
	printf "${lmsg}\n"
	STOP_COMMAND=""
fi

if [[ -z "$START_TIMEOUT" ]]; then
	lmsg=`gettext 'Start method timeout not specified ... using the \
default value of 300 secs.'`
	printf "${lmsg}\n"
	START_TIMEOUT=300
fi

if [[ $(expr "${START_TIMEOUT}" : '[0-9]*') -ne ${#START_TIMEOUT} ]]; then
	lmsg=`gettext 'Start timeout is not numeric.'`
	printf "${lmsg}\n"
	exit 1
fi

if [[ $(expr "${STOP_TIMEOUT}" : '[0-9]*') -ne ${#STOP_TIMEOUT} ]]; then
	lmsg=`gettext 'Stop timeout is not numeric.'`
	printf "${lmsg}\n"
	exit 1
fi

if [[ -z "$VALIDATE_TIMEOUT" ]]; then
	lmsg=`gettext 'Validate method timeout not specified ... using the \
default value of 300 secs.'`
	printf "${lmsg}\n"
	VALIDATE_TIMEOUT=300
fi

if [[ $(expr "${VALIDATE_TIMEOUT}" : '[0-9]*') -ne ${#VALIDATE_TIMEOUT} ]]; then
	lmsg=`gettext 'Validate timeout is not numeric.'`
	printf "${lmsg}\n"
	exit 1
fi

if [[ $(expr "${PROBE_TIMEOUT}" : '[0-9]*') -ne ${#PROBE_TIMEOUT} ]]; then
	lmsg=`gettext 'Probe timeout is not numeric.'`
	printf "${lmsg}\n"
	exit 1
fi


if [[ -z "$STOP_TIMEOUT" ]]; then
	lmsg=`gettext 'Stop method timeout not specified ... using the \
default value of 300 secs.'`
	printf "${lmsg}\n"
	STOP_TIMEOUT=300
fi

if [[ -z "$RT_NEW_DIR" ]]; then
	lmsg=`gettext 'Working directory not specified ... using the current \
directory.'`
	printf "${lmsg}\n"
	RT_NEW_DIR=`pwd`
fi

run_cmd "cd $RT_NEW_DIR"

if [[ ! -f rtconfig ]]; then
	lmsg=`gettext 'rtconfig does not exist in the current directory.\n\
Run the \"Configure\" operation from the same directory where \"Create\" was \
run.'`
	printf "${lmsg}\n"
	exit 1
fi

# Read the configuration information from the rtconfig file, that should
# have been supplied in Step 1.
RT_NAME=`nawk -F= '/^RT_NAME/ {print $2}' rtconfig`
RT_VENDOR=`nawk -F= '/^RT_VENDOR/ {print $2}' rtconfig`
RT_FULLNAME=`nawk -F= '/^RT_FULLNAME/ {print $2}' rtconfig`
SOURCE_TYPE=`nawk -F= '/^SOURCE_TYPE/ {print $2}' rtconfig`
STAND_ALONE=`nawk -F= '/^STAND_ALONE/ {print $2}' rtconfig`
RT_VERSION=`nawk -F= '/^RT_Version/ {print $2}' rtconfig`

if [[ -z "$PROBE_COMMAND" ]]; then
	lmsg=`gettext 'Probe command not specified ... will use simple \
probing.'`
	printf "${lmsg}\n"
	PROBE_COMMAND=""
fi

if [[ -z "$PROBE_TIMEOUT" ]]; then
	lmsg=`gettext 'Probe method timeout not specified ... using the \
default value of 30 secs.'`
	printf "${lmsg}\n"
	PROBE_TIMEOUT=30
fi

RTR_FILE=${RT_VENDOR}.${RT_NAME}
SRC_UTIL_FILE=${RT_NAME}.c
SRC_PROBE_FILE=${RT_NAME}_probe.c

SRC_MAKEFILE=Makefile

run_cmd 'sed -e "/START_COMMAND/,/PROBE_TIMEOUT/d" rtconfig > rtconfig.tmp'
run_cmd "mv rtconfig.tmp rtconfig"

printf "START_COMMAND=%s\n" "$START_COMMAND" >> rtconfig
printf "START_TIMEOUT=%s\n" $START_TIMEOUT >> rtconfig
printf "VALIDATE_COMMAND=%s\n" "$VALIDATE_COMMAND" >> rtconfig
printf "VALIDATE_TIMEOUT=%s\n" $VALIDATE_TIMEOUT >> rtconfig
printf "STOP_COMMAND=%s\n" "$STOP_COMMAND" >> rtconfig
printf "STOP_TIMEOUT=%s\n" $STOP_TIMEOUT >> rtconfig
printf "PROBE_COMMAND=%s\n" "$PROBE_COMMAND" >> rtconfig
printf "PROBE_TIMEOUT=%s\n" $PROBE_TIMEOUT >> rtconfig

run_cmd "cd $RT_NEW_DIR/$RT_FULLNAME/src"

# Modify the Makefile.$RT_NAME by adding the WORKING_DIR variable
run_cmd 'sed "/^WORKING_DIR=/ {
	i\\
WORKING_DIR=$RT_NEW_DIR
	d
	}" $SRC_MAKEFILE > ${SRC_MAKEFILE}.tmp'
run_cmd "mv ${SRC_MAKEFILE}.tmp $SRC_MAKEFILE"

# unlike stop_cmd_prog and probe_cmd_prog, start_cmd_prog is only used to
# see if the file exists and is executable or not.
start_cmd_prog=`echo $START_COMMAND | nawk '{print $1}'`

stop_cmd_prog=`echo $STOP_COMMAND | nawk '{print $1}'`
probe_cmd_prog=`echo $PROBE_COMMAND | nawk '{print $1}'`
validate_cmd_prog=`echo $VALIDATE_COMMAND | nawk '{print $1}'`

if [[ $SOURCE_TYPE == GDS ]]; then
	lmsg=`gettext '\nConfiguring the GDS driving scripts with user supplied data ...'`
	printf "${lmsg}"

	if [[ ! -f $start_cmd_prog || -x $start_cmd_prog ]]; then
		# if the file doesn't exist (which means it has to exist
		# on the cluster), or if the file is executable then we
		# use the input START_COMMAND as as the single command to
		# start the application.
		start_cmd_array=$START_COMMAND
	else
		# if the file is not executable, then we have to read the
		# file to to retrieve all the different commands in the
		# file, and process them individually.

		# Construct the list of commands to be used as the value
		# of START_CMD variable in the start utility script. This
		# list is of the form "cmd1 args ...,cmd2 args ..."
		# Notice that form of this list is different from the list
		# of start commands generated for C later. Also, notice that
		# for GDS, we only need list of full commands (with
		# arguments); we don't need the list of "binaries" or the
		# "no." of start commands.
		start_cmd_array=`nawk '{
			if ( NR == 1 )
				printf "%s", $0
			else
				printf ",%s",  $0
			}' $START_COMMAND`
	fi

	run_cmd "cd $RT_NEW_DIR/$RT_FULLNAME/util"
	run_cmd 'sed -e "s#^START_CMD=.*#START_CMD=\"$start_cmd_array\"#" \
	    -e "s#^VALIDATE_CMD=.*#VALIDATE_CMD=\"$VALIDATE_COMMAND\"#" \
	    -e "s#^STOP_CMD=.*#STOP_CMD=\"$STOP_COMMAND\"#" \
	    -e "s#^PROBE_CMD=.*#PROBE_CMD=\"$PROBE_COMMAND\"#" \
	    -e "s#^START_TIMEOUT=.*#START_TIMEOUT=\"$START_TIMEOUT\"#" \
	    -e "s#^VALIDATE_TIMEOUT=.*#VALIDATE_TIMEOUT=\"$VALIDATE_TIMEOUT\"#" \
	    -e "s#^STOP_TIMEOUT=.*#STOP_TIMEOUT=\"$STOP_TIMEOUT\"#" \
	    -e "s#^PROBE_TIMEOUT=.*#PROBE_TIMEOUT=\"$PROBE_TIMEOUT\"#" \
	    start${RT_NAME} > start${RT_NAME}.tmp'
	run_cmd "mv start${RT_NAME}.tmp start${RT_NAME}"

	lmsg=`gettext 'done.'`
	printf "${lmsg}\n"

	build_pkg
	exit 0
fi

lmsg=`gettext '\nConfiguring the resource type with user supplied data ...'`
printf "${lmsg}"

if [[ ! -f $start_cmd_prog || -x $start_cmd_prog ]]; then
	# if the file doesn't exist (which means it has to exist on the
	# cluster), or
	# if the file is executable then we use the input START_COMMAND as
	# as the single command to start the application.
	start_cmd_array=\"$START_COMMAND\"
	no_start_cmd=1;
	start_prog_array=\"`echo $START_COMMAND | nawk '{print $1}'`\"
else
	# if the file is not executable, then we have to read the file to
	# to retrieve all the different commands in the file, and process
	# them individually.

	# First construct the list of commands to be used in the svc_start
	# method. These are of the form "cmd1 args ...", "cmd2 args ..."
	start_cmd_array=`nawk '{
		if ( NR == 1 )
			printf "\"%s\" ", $0
		else
			printf ", \"%s\" ",  $0
	}' $START_COMMAND`

	# Next construct the list of commands to be used in the svc_validate
	# method. These are of the form "cmd1", "cmd2"
	start_prog_array=`nawk '{
		if ( NR == 1 )
			printf "\"%s\" ", $1
		else
			printf ", \"%s\" ",  $1
	}' $START_COMMAND`
	
	# Next set the no. of command for the application. These occurs at
	# 3 places in the code -- svc_validate, svc_start and svc_stop.
	no_start_cmd=`wc -l $START_COMMAND | nawk '{print $1}' | tr -d " "`
fi

if [[ $SOURCE_TYPE == "KSH" ]]; then
	KSH_START=${RT_NAME}_svc_start.ksh
	KSH_STOP=${RT_NAME}_svc_stop.ksh
	KSH_PROBE=${RT_NAME}_probe.ksh
	KSH_VALIDATE=${RT_NAME}_validate.ksh

	for ksh_file in $KSH_START $KSH_STOP $KSH_PROBE $KSH_VALIDATE
	do
	run_cmd 'sed \
		-e "s;^start_cmd_args=.*;start_cmd_args=\"$START_COMMAND\";" \
		-e "s;^stop_cmd_args=.*;stop_cmd_args=\"$STOP_COMMAND\";" \
		-e "s;^probe_cmd_args=.*;probe_cmd_args=\"$PROBE_COMMAND\";" \
		-e "s;^validate_cmd_args=.*;validate_cmd_args=\"$VALIDATE_COMMAND\";" \
		$ksh_file > ${ksh_file}.tmp'
	run_cmd "mv ${ksh_file}.tmp $ksh_file"
	done
fi

if [[ $SOURCE_TYPE == "C" ]]; then
# Modify the start_cmd_array, start_prog_array, stop_command,
# stop_cmd_prog, probe_cmd_prog
# Note the use of ";" in the first two sed commands because of
# "/" being there in the filenames.
run_cmd 'sed -e "/char \*service_start_cmd/,/\}/ {
	s;\".*\";$start_cmd_array;
}" \
	-e "/char \*start_cmd_prog/,/\}/ {
		s;\".*\";$start_prog_array;
}" \
	-e "s/int no_start_cmd = .*/int no_start_cmd = $no_start_cmd\;/" \
	-e "/char service_stop_cmd\[SCDS_CMD_SIZE\] =/ {
	i\\
	char service_stop_cmd[SCDS_CMD_SIZE] = \"$STOP_COMMAND\";
	d
}" \
	-e "/char \*start_cmd_prog =/ {
	i\\
	char *start_cmd_prog = \"$start_cmd_prog\";
	d
}" \
	-e "/char stop_cmd_prog\[SCDS_CMD_SIZE\] =/ {
	i\\
	char stop_cmd_prog[SCDS_CMD_SIZE] = \"$stop_cmd_prog\";
	d
}" \
	-e "/char probe_cmd_prog\[SCDS_CMD_SIZE\] =/ {
	i\\
	char probe_cmd_prog[SCDS_CMD_SIZE] = \"$probe_cmd_prog\";
	d
}" \
	-e "/char service_probe_cmd\[SCDS_CMD_SIZE\] =/ {
	i\\
	char service_probe_cmd[SCDS_CMD_SIZE] = \"$PROBE_COMMAND\";
	d
}" \
	-e "/char validate_cmd_prog\[SCDS_CMD_SIZE\] =/ {
	i\\
	char validate_cmd_prog[SCDS_CMD_SIZE] = \"$VALIDATE_COMMAND\";
	d
}" $SRC_UTIL_FILE > ${SRC_UTIL_FILE}.tmp'
run_cmd "mv ${SRC_UTIL_FILE}.tmp $SRC_UTIL_FILE"

fi # if [[ $SOURCE_TYPE == "C"]]

# Modify the RTR file to modify the values of Stop_timeout,
# Start_timeout, Port_list and RT_DESCRIPTION
run_cmd "cd $RT_NEW_DIR/$RT_FULLNAME/etc"
run_cmd 'sed -e "/Stop_timeout/,/^\}/ {
	/DEFAULT/ i\\
	DEFAULT = $STOP_TIMEOUT;
	/DEFAULT/d
}" \
	-e "/Start_timeout/,/^\}/ {
	/DEFAULT/ i\\
	DEFAULT = $START_TIMEOUT;
	/DEFAULT/d
}" \
	-e "/Port_list/,/^\}/ {
	/DEFAULT/ i\\
	DEFAULT = "";
	/DEFAULT/d
}" \
	-e "/Probe_timeout/,/^\}/ {
	/DEFAULT/ i\\
	DEFAULT = $PROBE_TIMEOUT;
	/DEFAULT/d
}" \
	-e "/Validate_timeout/,/^\}/ {
	/DEFAULT/ i\\
	DEFAULT = $VALIDATE_TIMEOUT;
	/DEFAULT/d
}" \
	-e "/^RT_DESCRIPTION/s/X Font/$RT_NAME/" \
$RTR_FILE > ${RTR_FILE}.tmp'
run_cmd "mv ${RTR_FILE}.tmp $RTR_FILE"

run_cmd "cd $RT_NEW_DIR/$RT_FULLNAME/src"

if [[ $SOURCE_TYPE == "C" ]]; then
	lmsg=`gettext '\nCompiling the resource type source code ...'`
	printf "${lmsg}\n"
else
	lmsg=`gettext '\nCopying the source files to bin directory ...'`
	printf "${lmsg}\n"
fi

run_cmd "make"

lmsg=`gettext 'done.'`
printf "${lmsg}\n"

build_pkg

exit 0
