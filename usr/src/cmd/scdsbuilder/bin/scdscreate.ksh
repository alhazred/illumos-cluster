#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)scdscreate.ksh	1.44	08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# Define msg. file name and location
# NOTE: TEXTDOMAIN must be kept in sync with the TEXT_DOMAIN var in the
# makefiles.
typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale

set +o noclobber

export PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/proc/bin:$PATH

# initialize
STAND_ALONE=false
SCALABLE=false
SOURCE_TYPE=C
VERBOSE=false

trap 'print "\n$0 interrupted - removing temp files and exiting ..." ;\
	rm *tmp; exit 1' 1 2 3 15

function run_cmd
{
	if [[ $VERBOSE == true ]]; then
		printf "%s\n" "$@"
	fi

	eval "$@"
	if [[ $? != 0 ]]; then
		lmsg=`gettext 'FAILED: %s'`
		printf "${lmsg}\n" "$@"
		exit 1
	fi
}

function usage
{
	lmsg=`gettext \
'Usage: %s -V <Vendor_ID>\n\
	-T <application name>\n\
	[-n <RT_Version>] \n\
	[-g|-k] [-a] [-s] [-d <Working Directory>]'`
	printf "${lmsg}\n" `basename $0`
}

###############################################################################
# Parse program arguments.
#
function parse_args # [args ...]
{
	typeset opt

	while getopts 'gaskV:T:n:d:' opt
	do
		case "$opt" in
		g)
			# Generic Data Service model
			if [[ $SOURCE_TYPE == KSH ]]; then
				lmsg=`gettext 'Please choose either -k for \
Korn Shell source code or -g for GDS.'`
				printf "${lmsg}\n"
				usage
				exit 1
			fi

			SOURCE_TYPE=GDS
			;;
		a)
			# Whether the resource type is stand-alone
			# (non-network aware)
			STAND_ALONE=true
			;;
		s)
			# Whether the resource type is scalable
			SCALABLE=true
			;;
		k)
			# Whether to generate the source code in ksh
			if [[ $SOURCE_TYPE == GDS ]]; then
				lmsg=`gettext 'Please choose either -k for \
Korn Shell source code or -g for GDS.'`
				printf "${lmsg}\n"
				usage
				exit 1
			fi

			SOURCE_TYPE=KSH
			;;
		V)
			# Name of the vendor.
			RT_VENDOR=$OPTARG
			;;
		n)
			# RT_Version
			RT_VERSION=$OPTARG
			;;
		T)
			# Name of the resource type.
			RT_NAME=$OPTARG
			;;
		d)
			# Name of the Working Directory.
			RT_NEW_DIR=$OPTARG
			echo $RT_NEW_DIR | egrep -e '^/' > /dev/null 2>&1
			if [[ $? != 0 ]]; then
				RT_NEW_DIR=`pwd`/$RT_NEW_DIR
			fi
			;;
		*)
			usage
		 	exit 1
			;;
		esac
	done

	shift $((OPTIND - 1))

	# Make sure there are no remaining args
	if [[ $# -ne 0 ]]; then
		usage
		exit 1
	fi
}

#################
#	Main	#
#################

parse_args "$@"

if [[ -z "$RT_VENDOR" ]]; then
	lmsg=`gettext 'Vendor name must be specified.'`
	printf "${lmsg}\n"
	usage
	exit 1
fi

if [[ -z "$RT_NAME" ]]; then
	lmsg=`gettext 'Resource type name must be specified.'`
	printf "${lmsg}\n"
	usage
	exit 1
fi

if [[ -z "$RT_VERSION" ]]; then
	if [[ $SOURCE_TYPE == GDS ]]; then
		RT_VERSION="6"
	elif [[ $SOURCE_TYPE == C ]]; then
		RT_VERSION="4"
	elif [[ $SOURCE_TYPE == KSH ]]; then
		RT_VERSION="3.2"
	fi
fi

echo $RT_NAME | grep '-' > /dev/null 2>&1
if [[ $? == 0 ]]; then
	# The RT name cotains a '-' in it. This causes problems in the
	# generated code. Hence disallow it.
	lmsg=`gettext 'The RT name cannot have a "-" in it.'`
	printf "${lmsg}\n"
	exit 1
fi

tmp_file_name=`echo $RT_NAME | \
	sed -e "s/dns//" \
	-e "s/xfs//"`

if [[ $RT_NAME != $tmp_file_name ]]; then
	lmsg=`gettext 'You cannot use substrings %1$s or %2$s in your \
resource type name.'`
	printf "${lmsg}\n" "dns" "xfs"
	lmsg=`gettext 'Please choose a different resource type name.'`
	printf "${lmsg}\n"
	lmsg=`gettext 'This restriction will be removed in future versions of \
the Builder.'`
	printf "${lmsg}\n"
	exit 1
fi

if [[ -z "$RT_NEW_DIR" ]]; then
	lmsg=`gettext 'Working directory not specified ... \
using the current directory.'`
	printf "${lmsg}\n"
	RT_NEW_DIR=`pwd`
fi

if [[ $SCALABLE == false ]]; then
	lmsg=`gettext 'Creating a failover resource type.'`
	printf "${lmsg}\n"
else
	lmsg=`gettext 'Creating a scalable resource type.'`
	printf "${lmsg}\n"
fi

RT_FULLNAME=$RT_VENDOR$RT_NAME

if [[ ! -d $RT_NEW_DIR ]]; then
	lmsg=`gettext '%s does not exist.  Creating %s ...'`
	printf "${lmsg}" "$RT_NEW_DIR" "$RT_NEW_DIR"
	run_cmd "mkdir -p $RT_NEW_DIR"
	lmsg=`gettext 'done.'`
	printf "${lmsg}\n"
fi

if [[ -f $RT_NEW_DIR/rtconfig ]]; then
	lmsg=`gettext 'rtconfig already exists.\n\
Please run \"Create\" operation from a different directory.'`
	printf "${lmsg}\n"
	exit 1
fi

run_cmd "cd $RT_NEW_DIR"

lmsg=`gettext 'Creating the rtconfig file ...'`
printf "${lmsg}"

printf "RT_NAME=%s\n" $RT_NAME >> rtconfig
printf "RT_VENDOR=%s\n" $RT_VENDOR >> rtconfig
printf "RT_FULLNAME=%s\n" $RT_FULLNAME >> rtconfig
printf "SCALABLE=%s\n" $SCALABLE >> rtconfig
printf "SOURCE_TYPE=%s\n" $SOURCE_TYPE >> rtconfig
printf "STAND_ALONE=$STAND_ALONE\n" >> rtconfig
printf "RT_VERSION=$RT_VERSION\n" >> rtconfig

lmsg=`gettext 'done.'`
printf "${lmsg}\n"

NA_APP_BEGIN="Network aware applications code -- BEGIN"
NA_APP_END="Network aware applications code -- END"

NNA_APP_BEGIN="Stand alone applications code -- BEGIN"
NNA_APP_END="Stand alone applications code -- END"

GATE_BEGIN="Gate Specific Code -- BEGIN"
GATE_END="Gate Specific Code -- END"

DSB_BEGIN="DS Builder Specific Code -- BEGIN"
DSB_END="DS Builder Specific Code -- END"

GDS_BEGIN="GDS Specific Code -- BEGIN"
GDS_END="GDS Specific Code -- END"

run_cmd "mkdir $RT_FULLNAME"
run_cmd "mkdir $RT_FULLNAME/src"
run_cmd "mkdir $RT_FULLNAME/util"
run_cmd "mkdir -p $RT_FULLNAME/man/man1m"
run_cmd "mkdir $RT_FULLNAME/etc"
run_cmd "mkdir $RT_FULLNAME/pkg"

if [[ $SOURCE_TYPE != GDS ]]; then
	run_cmd "mkdir $RT_FULLNAME/bin"
fi

DSB_SRC_DIR=/usr/cluster/lib/scdsbuilder/src

if [[ $SOURCE_TYPE == GDS ]]; then
	SRC_FILES="$DSB_SRC_DIR/gds/* $DSB_SRC_DIR/common/*"
else
	if [[ $SOURCE_TYPE == C ]]; then
		SRC_FILES="$DSB_SRC_DIR/csource/* $DSB_SRC_DIR/common/*"
	else
		SRC_FILES="$DSB_SRC_DIR/scripts/* $DSB_SRC_DIR/common/*"
	fi
fi

run_cmd "cd $RT_NEW_DIR/$RT_FULLNAME/src"

# First copy and modify all the XFS files in the "src" directory
for full_file_name in $SRC_FILES
do
	# for all files in the SRC_FILES
	file=`basename $full_file_name`

	new_file_name=`echo $file | \
		sed -e "s/xfs/$RT_NAME/" \
		-e "s/SUNW/$RT_VENDOR/" \
		-e "s/dns/$RT_NAME/"`

	lmsg=`gettext 'Cloning and modifying %s ...'`
	printf "${lmsg}" $new_file_name

	# copy the XFS/DNS file to its new name derived from the RT_NAME
	run_cmd "cp $full_file_name $new_file_name"

	# The depend file is an exception in that it should not be modified
	# at all, except for removing the gate specific stuff
	if [[ $file == depend ]]; then
		run_cmd 'sed -e "/$GATE_BEGIN/,/$GATE_END/d" depend > \
			depend.tmp'
		run_cmd "mv depend.tmp depend"
		lmsg=`gettext 'done.'`
		printf "${lmsg}\n"
		continue
	fi

	# The following files are exceptions because they're pre-compiled
	if [[ $file == gethostnames || $file == simple_probe || \
			$file == hasp_check ]]; then
		lmsg=`gettext 'done.'`
		printf "${lmsg}\n"
		continue
	fi

	# Modify the source files according to the RT_NAME provided
	run_cmd 'sed -e "/$GATE_BEGIN/,/$GATE_END/d" \
		-e "s/xfs/$RT_NAME/g" \
		-e "s/XFS/$RT_NAME/g" \
		-e "s/dns/$RT_NAME/g" \
		-e "s/DNS/$RT_NAME/g" \
		-e "s/SUNW/$RT_VENDOR/g" \
		-e "s/GDS_RT/SUNW.gds/g" \
		$new_file_name > ${new_file_name}.tmp'
	
	if [[ $SOURCE_TYPE == GDS ]]; then
		run_cmd 'sed -e "/$GDS_BEGIN/d" -e "/$GDS_END/d" \
		    -e "/$DSB_BEGIN/,/$DSB_END/d" \
		    ${new_file_name}.tmp > $new_file_name'
		rm ${new_file_name}.tmp
	else
		run_cmd 'sed -e "/$GDS_BEGIN/,/$GDS_END/d" \
		    -e "/$DSB_BEGIN/d" -e "/$DSB_END/d" \
		    ${new_file_name}.tmp > $new_file_name'
		rm ${new_file_name}.tmp
	fi

	if [[ $STAND_ALONE == true ]]; then
		# if it's a non-network aware RT then take out all the
		# network-aware code and the non-network-aware directives.
		run_cmd 'sed -e "/$NA_APP_BEGIN/,/$NA_APP_END/d" \
			-e "/$NNA_APP_BEGIN/d" -e "/$NNA_APP_END/d" \
			$new_file_name > ${new_file_name}.tmp'
		run_cmd "mv ${new_file_name}.tmp $new_file_name"
	else
		# if it's a network aware RT then take out all the
		# non-network-aware code and the network-aware directives.
		run_cmd 'sed -e "/$NNA_APP_BEGIN/,/$NNA_APP_END/d" \
			-e "/$NA_APP_BEGIN/d" -e "/$NA_APP_END/d" \
			$new_file_name > ${new_file_name}.tmp'
		run_cmd "mv ${new_file_name}.tmp $new_file_name"
	fi
	lmsg=`gettext 'done.'`
	printf "${lmsg}\n"
done

printf "\n"

# Create the utility scripts: startxfs, removexfs and stopxfs to the util
# directory.
# Set the values of RT_NAME, RT_VENDOR, SCALABLE and STAND_ALONE.
# For GDS model, RT_FULLNAME is "SUNW.gds:{RT_VERSION}"
# For non-GDS model, RT_FULLNAME is ${RT_VENDOR}.${RT_NAME}:{RT_VERSION}
for bin_file in start$RT_NAME remove$RT_NAME stop$RT_NAME
do
	if [[ $SOURCE_TYPE == GDS ]]; then
		lmsg=`gettext 'Creating the driving script %s ...'`
		run_cmd 'sed -e "s/^RT_FULLNAME=/RT_FULLNAME=SUNW.gds:${RT_VERSION}/" \
			$bin_file > ${bin_file}.tmp'
		lmsg=`gettext 'Creating the utility script %s ...'`
	else
		run_cmd 'sed -e \
			"s/^RT_FULLNAME=/RT_FULLNAME=${RT_VENDOR}.${RT_NAME}:${RT_VERSION}/" \
			$bin_file > ${bin_file}.tmp'
	fi

	run_cmd "mv ${bin_file}.tmp $bin_file"
	printf "${lmsg}" $bin_file

	run_cmd 'sed -e "s/^RT_NAME=/RT_NAME=$RT_NAME/" \
		-e "s/^RT_VENDOR=/RT_VENDOR=$RT_VENDOR/" \
		-e "s/^SCALABLE=/SCALABLE=$SCALABLE/" \
		-e "s/^STAND_ALONE=/STAND_ALONE=$STAND_ALONE/" \
		$bin_file > ${bin_file}.tmp'
	run_cmd "mv ${bin_file}.tmp ../util/${bin_file}"
	rm $bin_file
	lmsg=`gettext 'done.'`
	printf "${lmsg}\n"
done

# Create <app>_config file from the template xfs_config
config_file=${RT_NAME}_config
lmsg=`gettext 'Creating the config file %s ...'`
printf "${lmsg}" $config_file

if [[ $SCALABLE == true ]]; then
	if [[ $STAND_ALONE == true ]]; then
		run_cmd 'sed -e "s/^RS_NAME=.*$/RS_NAME=$RT_NAME-sars/" \
		    -e "s/^RG_NAME=.*$/RG_NAME=$RT_NAME-sarg/" \
		    -e "/SA_RG_NAME/d" \
		    $config_file > ../util/${config_file}'
	else
		run_cmd 'sed -e "s/^RS_NAME=.*$/RS_NAME=$RT_NAME-sars/" \
		    -e "s/^RG_NAME=.*$/RG_NAME=$RT_NAME-sarg/" \
		    $config_file > ../util/${config_file}'
	fi
else
	run_cmd 'sed -e "s/^RS_NAME=.*$/RS_NAME=$RT_NAME-hars/" \
	    -e "s/^RG_NAME=.*$/RG_NAME=$RT_NAME-harg/" \
	    -e "/SA_RG_NAME/d" \
	    -e "s/^MAXIMUM_PRIMARIES=.*$/MAXIMUM_PRIMARIES=1/" \
	    -e "s/^DESIRED_PRIMARIES=.*$/DESIRED_PRIMARIES=1/" \
	    $config_file > ../util/${config_file}'
fi

rm $config_file
lmsg=`gettext 'done.'`
printf "${lmsg}\n"

for man_file in start$RT_NAME.1m remove$RT_NAME.1m stop$RT_NAME.1m \
	${RT_NAME}_config.1m
do
	lmsg=`gettext 'Creating the man page %s ...'`
	printf "${lmsg}" $man_file
	run_cmd "mv ${man_file} ../man/man1m/${man_file}"
	lmsg=`gettext 'done.'`
	printf "${lmsg}\n"
done

# move the RTR, pkginfo, prototype and depend files to the etc directory
run_cmd "mv pkginfo ../etc"
run_cmd "mv prototype ../etc"
run_cmd "mv depend ../etc"

# Add the RT_NAME and RT_VENDOR and copy the preremove to the etc directory
for pkg_file in preremove
do
	lmsg=`gettext 'Creating the package file %s ...'` 
	printf "${lmsg}" $pkg_file
	run_cmd 'sed -e "s/^RT_NAME=/RT_NAME=$RT_NAME/" \
		-e "s/^RT_VENDOR=/RT_VENDOR=$RT_VENDOR/" \
		$pkg_file > ${pkg_file}.tmp'
	run_cmd "mv ${pkg_file}.tmp ../etc/${pkg_file}"
	rm $pkg_file
	lmsg=`gettext 'done.'`
	printf "${lmsg}\n"
done

run_cmd "mv README.$RT_NAME .."

if [[ $SOURCE_TYPE == GDS ]]; then
	# For GDS model, we only create the driving scripts and it doesn't
	# have an RTR file, so exit now
	exit 0
fi

# Modify the RTR file to modify the value of Scalable
run_cmd "mv $RT_VENDOR.$RT_NAME ../etc"
run_cmd "cd $RT_NEW_DIR/$RT_FULLNAME/etc"
RTR_FILE=${RT_VENDOR}.${RT_NAME}
lmsg=`gettext 'Creating the RTR file %s ...'`
printf "${lmsg}" $RTR_FILE
run_cmd 'sed -e "/Scalable/,/^\}/ {
	/DEFAULT/ i\\
	DEFAULT=$SCALABLE;
	/DEFAULT/d
}" \
-e "s/^RT_VERSION.*/RT_VERSION=${RT_VERSION};/" \
$RTR_FILE > ${RTR_FILE}.tmp'
run_cmd "mv ${RTR_FILE}.tmp $RTR_FILE"

if [[ $SOURCE_TYPE != GDS  && $RT_VERSION != 1.0 ]]; then
    sed -e "s/^##$upgrade/#$upgrade/" \
$RTR_FILE > ${RTR_FILE}.tmp
run_cmd "mv ${RTR_FILE}.tmp $RTR_FILE"
fi

lmsg=`gettext 'done.'`
printf "${lmsg}\n"

printf "\n\n"
