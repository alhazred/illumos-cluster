#!	/usr/bin/perl
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#


#
# ident "@(#)vfstab-global-mount-points.pl 1.5     08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#


#######################################
#
# Data gathering utility, supplemental to explorer(1m)
#
#
# This utility is intended solely to be handed to explorer
# via its "-c <command>" interface in order to add data to
# the explorer results.
#
# This additional data supports eRAS check #3065: "Global
# filesystem /etc/vfstab entries are not consistent across
# all Sun Cluster 3.x nodes."
#
# There is no data gathered by explorer which this check can use to
# tell if a mount point actually exists. It would be akin to
# `ls -laR /` and therefore enormous in both space and time.
#
# This script looks in /etc/vfstab for mount points then looks in the
# filesystem to see if they exist. If they do simply list the
# filesystem entry. This produces a very short listing which the check
# can refer to.
#

#######################################
#
# for each line in /etc/vfstab:
#	if not a comment line             AND
#	if line has 7 words               AND
#	if first word starts with "/dev"  AND
#	if second word starts with "/dev" AND
#	if seventh word contains "global" AND
#	if mount-point is a directory
#		then print 'ls -lad <mount-point>'
# endfor
#

use strict;

my $INFILE = "/etc/vfstab";

open (IN, "<$INFILE");
while (my $in = <IN>) {
  
  #print $in;
  chomp $in;	
  
  if ($in =~ /^\#/) { # exclude comment lines
    next;
  }
  
  my @line = split ' ',$in;
  
  if (@line ne 7) { # line must have 7 words
    next;
  }
  
  if (! ($line[0] =~ /^\/dev/)) { # dev to mount starts with "/dev"
    next;
  }
  
  if (! ($line[1] =~ /^\/dev/)) { # dev to fsck starts with "/dev"
    next;
  }
  
  if (! ($line[6] =~ /global/)) { # options contains "global"
    next;
  }
  
  if (-d $line[2]) {		# mount point is dir?
    my $ls = `/usr/bin/ls -lad $line[2] 2>/dev/null`;
    print "$ls";
  }				# is directory
  
}				# while read

close (IN);
