#! /usr/bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)sccheckd.ksh	1.19	08/12/03 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#
#
#	Startup wrapper for sccheckd
#

typeset -r PROG=${0##*/} # equiv to `basename $0`
typeset -x TEXTDOMAIN=SUNW_SC_CMD
typeset -x TEXTDOMAINDIR=/usr/cluster/lib/locale
umask 0022


# messages from server go to syslog
# error messages also appear on console
# must agree with Globals.java
typeset -r SYSLOG=/usr/cluster/lib/sc/scds_syslog
typeset -r SYSLOG_ERROR="-p daemon.error"
typeset -r SYSLOG_NOTICE="-p daemon.notice"
typeset -r SYSLOG_TAG="-t sccheckd"

typeset -r SCCHECK_EXPS_UPKDIR="/var/tmp/"

#
# load common definitions and functions
#	exit immediately if load fails

integer status=0

typeset -r SC_CHECK_COMMON=/usr/cluster/lib/sccheck/sccheck_common
if [[ -e ${SC_CHECK_COMMON} ]]; then
    . ${SC_CHECK_COMMON} 2> /dev/null
    status=$?
else
    status=1
fi

if [[ $status -ne 0 ]]; then
    printf "$(gettext '%s: unable to load %s')\n" ${PROG} "${SC_CHECK_COMMON}" >&2
    exit 200
fi

# server-side ke log file name
KE_JDEFINES="${KE_JDEFINES} -Dke.server.log=${SCCHECK_LOGDIR}/${KESERVERLOG}"
KE_JDEFINES="${KE_JDEFINES} -Dke.server.xsldir=${SERVER_XSLDIR}"

CLASSPATH=${CLASSPATH}:${SERVER_XSLDIR}


#
# Test for java server already running on this node.
#
# A running server has created a file containing the pid of
# sccheckd.ksh. If there is a server pidfile present this means a
# server is running.
#
# If a server is not running we continue and become a running
# server. If a server is already running we simply exit silently; the
# client that caused us to be invoked will now attach to the already
# running server.
# 
# Make sure any found server pidfile is legit.
#

typeset -r SCCHECKD_PIDFILE=${SCCHECK_PIDFILES_BASE}/__sccheckd.pid
common_clean_pidfiles ${SCCHECKD_PIDFILE}


if [[ -e $SCCHECKD_PIDFILE ]]; then
    ${SYSLOG} ${SYSLOG_NOTICE} ${SYSLOG_TAG} -m \
	"Using existing server."
    exit 0
else
    # create the server pidfile
    echo $$ > ${SCCHECKD_PIDFILE}
    ${SYSLOG} ${SYSLOG_NOTICE} ${SYSLOG_TAG} -m \
	"Starting..."
fi


#
# definitions used by server only
#

# java class for server-side
typeset -r JAVA_SERVER=com.sun.cluster.sccheck.SccheckServer

#
# Lists of "defines" handed to java at atartup.
#
# Parameters are passed in to JVM via -D defines rather
# than on the command line.
#
# Values appended as appropriate during execution of this script.
#
typeset CLUSTER_JDEFINES=	# cluster-related data

# process ID of the JVM once it's invoked from this ksh
typeset JVM_PID=-1


#
#	Function definitions
#

##############################
#
#  load_cluster_data
#
#	input:		nothing
#	output:		nothing
#	returns:	nothing
#
#	action:	
#	    fetch cluster data:
#		clustername
#		clustermode
#		private net info
#		setup java defines for passing data to jvm
#
#	no I18N: use C locale
#
##############################

load_cluster_data()
{
    typeset lc_save=${LC_ALL}
    export LC_ALL=C

    typeset clustermode=$(common_get_clustermode)
    typeset clustername=$(/usr/bin/uname -n)
    typeset private_net=not_a_cluster		# no I18n
    typeset private_netmask=not_a_cluster	# no I18n

    if [[ ${clustermode} == ${TRUE} ]]; then
	clustername=$(${SCHA_CLGET} CLUSTERNAME)
	private_net="$(${SCCONF} | /usr/bin/grep 'Cluster private net:' | /usr/bin/awk '{print $4}')"
	private_netmask="$(${SCCONF} | /usr/bin/grep 'Cluster private netmask:' | /usr/bin/awk '{print $4}')"
    fi

    CLUSTER_JDEFINES="${CLUSTER_JDEFINES} -Dsccheck.clustername=${clustername}"
    CLUSTER_JDEFINES="${CLUSTER_JDEFINES} -Dsccheck.clustermode=${clustermode}"
    CLUSTER_JDEFINES="${CLUSTER_JDEFINES} -Dsccheck.privatenet=${private_net}"
    CLUSTER_JDEFINES="${CLUSTER_JDEFINES} -Dsccheck.privatenetmask=${private_netmask}"

    export LC_ALL=${lc_save}
} # load_cluster_data


#####################################################
#
# exit_err
#
#	input:		nothing
#	output:		nothing
#	returns:	exits the shell
#
#	action: 
#
#	Exit the shell with a default status code
#	after calling cleanup.
#
#	Any routine that calls exit_err() should have
#	sent its own message to syslog.
#
#	Exit code is actually meaningless since we were
#	invoked by inetd.
#
#####################################################

exit_err()
{
	cleanup
	exit 201
}

#####################################################
#
# cleanup
#
#	File cleanups performed before exiting:
#		pidfile for this server
#
#	input:		nothing
#	output:		nothing
#	returns:	nothing
#
#	action: 
#
#	File cleanups performed before exiting:
#		pidfile for this client
#
#####################################################

cleanup()
{
    /bin/rm -f ${SCCHECKD_PIDFILE}
}

#####################################################
#
# age_server_logs
#
#	input:		nothing
#	output:		nothing
#	returns:	nothing
#
#	action: 
#
#	Perform aging of log files on server machine.
#	One set of files was written by sccheckd, the
#	other by the Embedded Knowledge Engine.
#
#	Logs progress through four stages then disappear.
#	
#####################################################

age_server_logs()
{

    #
    # age the server-side logs
    #

    # server.log
    # .older -> .oldest
    LOG_TARGET=${SCCHECK_LOGDIR}/server.log.OLDER
    LOG_AGED=${SCCHECK_LOGDIR}/server.log.OLDEST

    if [[ -a ${LOG_TARGET} ]]; then
	/usr/bin/mv ${LOG_TARGET} ${LOG_AGED}
    fi

    # .old -> .older
    LOG_TARGET=${SCCHECK_LOGDIR}/server.log.OLD
    LOG_AGED=${SCCHECK_LOGDIR}/server.log.OLDER
    if [[ -a ${LOG_TARGET} ]]; then
	/usr/bin/mv ${LOG_TARGET} ${LOG_AGED}
    fi

    # log -> .old
    LOG_TARGET=${SCCHECK_LOGDIR}/server.log
    LOG_AGED=${SCCHECK_LOGDIR}/server.log.OLD
    if [[ -a ${LOG_TARGET} ]]; then
	/usr/bin/mv ${LOG_TARGET} ${LOG_AGED}
    fi

    # ke server-side log
    # .old -> .older
    LOG_TARGET=${SCCHECK_LOGDIR}/${KESERVERLOG}.OLDER
    LOG_AGED=${SCCHECK_LOGDIR}/${KESERVERLOG}.OLDEST

    if [[ -a ${LOG_TARGET} ]]; then
	/usr/bin/mv ${LOG_TARGET} ${LOG_AGED}
    fi

    # .old -> .older
    LOG_TARGET=${SCCHECK_LOGDIR}/${KESERVERLOG}.OLD
    LOG_AGED=${SCCHECK_LOGDIR}/${KESERVERLOG}.OLDER
    if [[ -a ${LOG_TARGET} ]]; then
	/usr/bin/mv ${LOG_TARGET} ${LOG_AGED}
    fi

    # log -> .old
    LOG_TARGET=${SCCHECK_LOGDIR}/${KESERVERLOG}
    LOG_AGED=${SCCHECK_LOGDIR}/${KESERVERLOG}.OLD
    if [[ -a ${LOG_TARGET} ]]; then
	/usr/bin/mv ${LOG_TARGET} ${LOG_AGED}
    fi
} # age_server_logs

#######################################
#
#  trap_exit
#
#	Signal handler.
#
#	Exits the shell.
#
#	input:		nothing
#	output:		nothing
#	returns:	exits the shell.
#
#	action: 
#		Attempt to kill the JVM spawned by this
#	server. Exit the shell with ${ERR_TRAP} exit code.
#	(Exit code not useful since we're invoked by inetd.)
#
#######################################

trap_exit()
{
    ${SYSLOG} ${SYSLOG_ERROR} ${SYSLOG_TAG} -m \
	"Interrupted."

    if [[ ${JVM_PID} > 1 ]]; then

	# make sure that ${JVM_PID} points to real process
	# and that it's sccheck or sccheckd
	/usr/bin/pgrep -f "sccheck" | /usr/bin/grep ${JVM_PID} 2> /dev/null
	if [[ $? -eq 0 ]]; then
	    kill -15 ${JVM_PID}
	fi
    fi

    cleanup
    exit ${ERR_TRAP}
} # trap_exit


###################################################
#
#	end function definitions
#
###################################################


##############################################################
#
# "main"
#
#
#	Exits the shell either: indirectly via exit_err
#	function or by a subroutine calling exit_err; or by
#	the explicit exit at the end of this section.
#
#	Exit code is irrelevant since we're launched by inetd.
#
###############################################################

trap 'trap_exit' HUP INT QUIT TERM

typeset errmsg=

# validation: is root user?
# error is fatal; message sent to syslog
errmsg=$(common_verify_isroot)
status=$?
if [[ $status -ne 0 ]]; then
   ${SYSLOG} ${SYSLOG_ERROR} ${SYSLOG_TAG} -m \
	"${errmsg}"
   exit_err
fi


# required utilites available?
# errors are fatal; messages are sent to syslog
errmsg=$(common_check_prereqs)
status=$?
if [[ $status -ne 0 ]]; then
   ${SYSLOG} ${SYSLOG_ERROR} ${SYSLOG_TAG} -m \
	"${errmsg}"
   exit_err
fi


# minimal version of java available?
# errors are fatal; messages are sent to syslog
#
# tested here rather than in java code because downrev
# version might not support logging which the 
# java code does right on startup
#
# min values defined in /etc/default/sccheck
#
typeset SC_LIB_SC=/usr/cluster/lib/scadmin/lib/sc_common
. ${SC_LIB_SC}
if [[ $? -ne 0 ]]; then
   errmsg=`printf "$(gettext 'Unable to load %s')" "${SC_LIB_SC}"`
   ${SYSLOG} ${SYSLOG_ERROR} ${SYSLOG_TAG} -m "${errmsg}"
   exit_err
fi

errmsg=$(lib_test_java_version)
status=$?
if [[ $status -ne 0 ]]; then
   ${SYSLOG} ${SYSLOG_ERROR} ${SYSLOG_TAG} -m \
	"${errmsg}"
   exit_err
fi

# misc version strings are sent to server-side log files
typeset java_ver=$(common_get_java_version)

# get name, mode, network info, etc
load_cluster_data

# Server will test explorer_ver so it can pass error msg to client.
# ${explorer_ver} must not contain any spaces or the java command
# line chokes. Quoting doesn't help.
typeset explorer_ver=$(common_get_explorer_version)
CLUSTER_JDEFINES="${CLUSTER_JDEFINES} -Dsccheck.explorerVer=${explorer_ver}"

#user may specified a zipped explorer archive
CLUSTER_JDEFINES="${CLUSTER_JDEFINES} -Dsccheck.gunzip=${GUNZIP}"
CLUSTER_JDEFINES="${CLUSTER_JDEFINES} -Dsccheck.tar=${TAR}"
CLUSTER_JDEFINES="${CLUSTER_JDEFINES} -Dsccheck.explorersUnpackDir=${SCCHECK_EXPS_UPKDIR}"



# able to make required directories?
# errors are fatal; messages are sent to syslog
errmsg=$(common_mkdirs)
status=$?
if [[ $status -ne 0 ]]; then
   ${SYSLOG} ${SYSLOG_ERROR} ${SYSLOG_TAG} -m \
	"${errmsg}"
   exit_err
fi

# validation: sufficient diskspace?
# error is fatal; message sent to syslog
errmsg=$(common_test_diskspace)
status=$?
if [[ $status -ne 0 ]]; then
   ${SYSLOG} ${SYSLOG_ERROR} ${SYSLOG_TAG} -m \
	"${errmsg}"
   exit_err
fi

# validation: appropriate EKE?
# error is fatal; message sent to syslog
errmsg=$(common_test_eke_version)
status=$?
if [[ $status -ne 0 ]]; then
   ${SYSLOG} ${SYSLOG_ERROR} ${SYSLOG_TAG} -m \
	"${errmsg}"
   exit_err
fi

# log maintainence
age_server_logs

# launch jvm in background so we can save pid of jvm:
# enables ^C kill of both ksh & java

$JAVA -classpath $CLASSPATH ${KE_JDEFINES} ${CLUSTER_JDEFINES} ${JAVA_SERVER} ${MYNAME} "${java_ver}" &

JVM_PID=$!
wait $JVM_PID

cleanup

${SYSLOG} ${SYSLOG_NOTICE} ${SYSLOG_TAG} -m \
    "Finished."


exit 0
