#! /bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident "@(#)explorer_wrapper.ksh 1.11     08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#


##############################################
#
#
#	Service wrapper for explorer(1M)
#
#
#
# This wrapper provides three explorer related functions:
#
#	run:
#		Invoke explorer, adding any additional arguments
#		from optional file ${ARGFILE}.${clmode} according
#		to whether or not the node is booted in cluster mode.
#
#		Additional arguments are for adding commands such as
#		'vfstab-global-mount-points' to be run by explorer.
#		Users may wish to add '-keep' or '-w <module-name>'
#		to this file as well.
#		The contents of the file must be a single line,
#		appropriate for consumption by explorer.
#
#	gzipname:
#		Return filename of most recent compressed explorer
#		results.
#
#	dirname:
#		Return directoryname of most recent uncompressed
#		explorer results.
#
# This wrapper is intended for use only by sccheck(1M) java
# code. It is *not* intended for user-command-line use
#
# Special workaround code for
#	6198608 "Cluster systems panic when sccheck, explorer or diskinfo -g are called"
# to prevent the disks module from being called if explorer version is 4.3(.x)
#
# and for
#	6263176 sccheck: explorer 5.0 fails to run
# by dynamically removing modules renamed in 4.3.1
# but retained for use by older explorers
#


# allow environmental overrides via config file
typeset SCCHECK_CONF=/etc/default/sccheck

if [[ -e ${SCCHECK_CONF} ]]; then
    . ${SCCHECK_CONF}
fi


typeset EXPLORER_HOME=${EXPLORER_HOME:-/opt/SUNWexplo}
typeset -r HOSTID=$(hostid)
typeset -r UNAME=$(/usr/bin/uname -n)
typeset -r EXPL_RESULTS_BASE=output/explorer.${HOSTID}.${UNAME}

typeset -r ARGFILE_BASE=/usr/cluster/lib/sccheck/explorer_args
typeset clmode=


if [[ ! -x ${EXPLORER_HOME}/bin/explorer ]]; then
   	printf "$(gettext '%s: %s not found.')\n" ${0} ${EXPLORER_HOME}/bin/explorer >&2
	exit 1
fi

#
# fetch explorer version, mapped into buckets that interest sccheck
#
#	version < 4.3 -> 4
#	version == 4.3.x -> 4.3
#	version == 5.x -> 5
#
get_expl_ver() {
  expl_ver=`${EXPLORER_HOME}/bin/explorer -V 2>&1 | /usr/bin/grep -i "explorer version"`

  # pick out the numeric value (as a string) with blanks removed
  expl_ver=`echo ${expl_ver} | /usr/bin/awk -F: '{print $2}' | /usr/bin/tr -d "[:blank:]"`

  x=${expl_ver##4.3}
  if [ "${x}" != "${expl_ver}" ]; then
    echo "4.3"
    return
  fi

  x=${expl_ver##4.}
  if [ "${x}" != "${expl_ver}" ]; then
    echo "4" 
    return
  fi

  x=${expl_ver##5.}
  if [ "${x}" != "${expl_ver}" ]; then
    echo "5"
    return
  fi

  echo ${expl_ver}
} # get_expl_ver

if [[ -x /usr/sbin/clinfo ]]; then
    /usr/sbin/clinfo > /dev/null
    if [[ $? -eq 0 ]]; then
	clmode="cluster"
    else
	clmode="non-cluster"
    fi
else
    clmode="non-cluster"
fi

typeset	EXPL_VER=`get_expl_ver`
typeset ARGFILE=${ARGFILE_BASE}.${clmode}
typeset MOREARGS=""

# validate args

if [[ $# -ne 1 ]]
then
	printf "$(gettext 'usage: %s run | gzipname | dirname')\n" ${0} >&2
	exit 1
fi

# execute explorer

if [[ "$1" = "run" ]]; then
	NODE=$(uname -n)

	if [[ -f $ARGFILE ]]; then
	    
	    # read the first content line that doesn't contain a '#'
	    while read MOREARGS
	    do
		echo ${MOREARGS} | /usr/bin/grep "#"
		if [[ $? -eq 1 && -n ${MOREARGS} ]]; then 
		    break
		fi
	    done < ${ARGFILE}

	    #
	    # test explorer version
	    # if 4.3(.x) then insert "!disks" to prevent disks module from being run
	    # if both "!disks" and "disks" are present "!disks" takes precedence
	    #
	    # if 5.x do not call for sds or vxfsextended modules since these modules
	    # were renamed post 4.1; under 5.x missing modules generate a fatal error
	    #

	    if [ "${EXPL_VER}" = "4.3" ]; then
		MOREARGS=`echo ${MOREARGS} | /usr/bin/sed -e "s/!default,/!default,!disks,/"`
	    elif [ "${EXPL_VER}" = "5" ]; then
		MOREARGS=`echo ${MOREARGS} | /usr/bin/sed -e "s/,sds//"`
		MOREARGS=`echo ${MOREARGS} | /usr/bin/sed -e "s/,vxfsextended//"`
	    fi

	    # echo to stdout
	    printf "$(gettext 'Additional explorer arguments: %s')\n" "${MOREARGS}"
	fi

	# launch explorer
	eval ${EXPLORER_HOME}/bin/explorer ${MOREARGS}
	exit $?
fi


# get (newest) results file name

if [[ "$1" = "gzipname" ]]; then
	/usr/bin/ls -d ${EXPLORER_HOME}/${EXPL_RESULTS_BASE}*gz > /dev/null 2>&1 
	if [[ $? -ne 0 ]]; then
	    exit 1
	else
	    /usr/bin/ls -d ${EXPLORER_HOME}/${EXPL_RESULTS_BASE}*gz | /usr/bin/tail -1
	    exit 0
	fi
fi


# get (newest) results dir name

if [[ "$1" = "dirname" ]]; then
	/usr/bin/ls -d ${EXPLORER_HOME}/${EXPL_RESULTS_BASE}* | /usr/bin/grep -v tar.gz > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then
	    exit 1
	else
	    /usr/bin/ls -d ${EXPLORER_HOME}/${EXPL_RESULTS_BASE}* | /usr/bin/grep -v tar.gz | /usr/bin/tail -1
	    exit 0
	fi
fi

# else

printf "$(gettext '%s: unknown option: %s')\n" ${0} ${1} >&2

exit 1

