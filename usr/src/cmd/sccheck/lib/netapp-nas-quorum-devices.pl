#!	/usr/bin/perl
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#


#
# ident "@(#)netapp-nas-quorum-devices.pl 1.5     08/05/20 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#


#######################################
#
# Data gathering utility, supplemental to explorer(1m)
#
#
# This utility is intended solely to be handed to explorer
# via its "-c <command>" interface in order to add data to
# the explorer results.
#
# This script looks through the ccr infrastructure file using the "scconf -pvv"
# output. If there are any quorum devices configured on NetApp filers, then
# the command issues a rsh to the filer to determine whether the iSCSI
# license is correctly installed.
#

#######################################
#
# for each line in output of scconf -pvv | grep "Quorum device filer"
# 	rsh filer_name license | grep iscsi
# endfor
#

open(SCCONF, "/usr/cluster/bin/scconf -pvv | grep 'Quorum device filer' |");
while(<SCCONF>) {
	print $_;
	my @line = split;
	$filername = @line[4];

	$rsh_command = "rsh ".$filername." license";

	open(RSH, $rsh_command."|");
	while (<RSH>) {
		my @license_line = split;
		if ($license_line[0] eq "iscsi") {
			print $_;
		}
	}
}

