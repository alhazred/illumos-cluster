/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * ident        "@(#)Utils.java 1.9     08/05/20 SMI"
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 *
 */


package com.sun.cluster.sccheck;

import java.io.*;

import java.net.*;

import java.util.*;


/*
 * Misc utility functions:
 *
 *      network address conversions and manipulations (static)
 *      disk file and directory manipulations (static & non-static)
 *      string array display (static)
 *
 * Note to future developer:
 *      If methods are called by server-side objects then
 *      System.out.println() won't be helpful when server is invoked under
 *      inetd. Use Logger.getLogger().trace('message') instead.
 *
 *
 * Utils for executing external commands:
 *
 * runScript()
 *      Accepts a set of commands, creates a shell script,
 *      executes the script: no stdout returned.
 *
 * runCmdNoStdout()
 *      Accepts a single command with args and executes the command,
 *      ignoring stdout from the command. If the command
 *      returns non-0 then output from the command's stderr
 *      is returned in a WrapperException.
 *
 * runCmdOneString()
 *      Accepts a single command with args and executes the command,
 *      expecting exactly one string on stdout. If the command
 *      returns non-0 then output from the command's stderr
 *
 * Misc directory-related functions.
 */

public class Utils implements Globals {


    // -- network address manipulations  --

    public static int inetAddressToInt(InetAddress ia) {
        byte ba[] = ia.getAddress();
        int i = Utils.byteArrayToInt(ba);

        return i;
    } // inetAddressToInt

    public static int dottedQuadToInt(String dq) {
        int val = Utils.byteArrayToInt(dottedQuadToByteArray(dq));

        return val;
    } // dottedQuadToInt

    // dottedQuadToByteArray(): reports NumberFormatException to Logger
    public static byte[] dottedQuadToByteArray(String dq) {
        StringTokenizer st = new StringTokenizer(dq, ".");
        int ct = st.countTokens();

        byte ba[] = new byte[ct];
        byte b;
        int i = 0;
        int index = 0;

        while (st.hasMoreTokens()) {
            String quad = st.nextToken();

            // force the quad to a signed int so it'll fit in a byte
            try {
                i = Integer.parseInt(quad);

                if (i > 127) {
                    i = i - 256;
                }

                b = Byte.parseByte(Integer.toString(i));
            } catch (NumberFormatException nfe) {
                Logger.getLogger().info("Utils.dottedQuadToByteArray(): " +
                    "in NumberFormatException on: " + dq + ": " +
                    nfe.getMessage());
                b = 0;
            }

            ba[index++] = b;
        }

        return ba;
    } // dottedQuadToByteArray

    public static int byteArrayToInt(byte ba[]) {

        /*
         * input: byte array representing the dotted quads of an
         * IPv4 address for each quad: strip off sign bits,
         * leftshift, accumulate
         */
        int x = 0;
        int y = 0;

        for (int i = 0; i < ba.length; i++) {
            Byte b = new Byte(ba[i]);
            int ib = ba[i];

            if (ib < 0) {
                ib = ib & 0xff;
            }

            x = x << 8;
            x = x + ib;
        }

        return x;
    } // byteArrayToInt


    // -- execute arbitrary external command(s)  --

    /*
     * runScript(String cmds)
     *
     * Run a series of /bin/sh commands as a shell script
     * cmds: A string containing  /bin/sh  command lines.
     *
     * Throws WrapperException with stderr if commands exits non-0.
     */
    static public void runScript(String cmds) throws WrapperException {
        Utils.runScript(cmds, null);
    } // runScript


    /*
     * runScript(String cmds, String[] env)
     *
     * Run a series of /bin/sh commands as a shell script
     * cmds: string containing /bin/sh command lines.
     * env:  environment vars
     *
     * Throws WrapperException with stderr if generated script
     * (i.e. last command executed in generated script) exits non-0.
     */
    static public void runScript(String cmds, String env[])
        throws WrapperException {

        Logger logger = Logger.getLogger();

        int returnCode = 0;
        String line = null;
        String errLine = "";

        Runtime rt = Runtime.getRuntime();
        Process rtcmd = null;
        File cmdFile = null;

        try {

            // Create a temp "shell script" file
            cmdFile = File.createTempFile("sccheck_", ".sh");
            cmdFile.deleteOnExit();

            PrintWriter cmdFileWriter = new PrintWriter(new FileOutputStream(
                        cmdFile));
            cmdFileWriter.println(cmds);
            cmdFileWriter.close();

            /*
             * Use java exec to run the shell script file
             * under /bin/sh
             */
            String cmd = "/bin/sh " + cmdFile.getPath();
            logger.info("Utils.runScript(): " + cmd);
            logger.info("Utils.runScript(): " + cmds);

            if (env == null) {
                rtcmd = rt.exec(cmd);
            } else {
                rtcmd = rt.exec(cmd, env);
            }

            returnCode = rtcmd.waitFor();
            logger.info("Utils.runScript() " + "got return code " + returnCode);

            if (returnCode != 0) {
                InputStream errIn = rtcmd.getErrorStream();
                InputStreamReader errInR = new InputStreamReader(errIn);
                BufferedReader errBR = new BufferedReader(errInR);

                while ((line = errBR.readLine()) != null) {
                    logger.info("Utils.runScript() " + "stderr: " + line);
                    errLine = errLine + line;
                } // errBR

                logger.error("Utils.runScript() " +
                    "throwing WrapperException");
                throw new WrapperException(errLine);
            } // returnCode != 0

        } catch (IOException ioex) { // getRuntime(); readLine()
            logger.error("Utils.runScript(): " + "IOException ioex: " +
                ioex.getMessage());
            throw new WrapperException(ioex.getMessage());
        } catch (InterruptedException iex) { // child.waitFor()

            // will be thrown if this thread is interrupted
            // by another
            logger.error("Utils.runScript(): " + "InterruptedException iex: " +
                iex.getMessage());
            throw new WrapperException(iex.getMessage());
        }

        logger.trace("Utils.runScript() -- EXIT-- ");
    } // runScript

    /*
     * runCmdNoStdout()
     *
     * Invocation of cmdArray[0] with args cmdArray[1],...
     *
     *      create/launch the Process
     *
     *      check for any trailing stderr
     *      obtain exit code; throw exception if code != 0
     *
     * No return values.
     */
    static public void runCmdNoStdout(String cmdArray[])
        throws WrapperException {

        Logger logger = Logger.getLogger();
        logger.info("Utils.runCmdNoStdout() " + "cmdArray: " +
            Utils.dumpStringArray(cmdArray));

        int returnCode = 0;
        String line = null;
        String errLine = "";

        try {
            Process child = Runtime.getRuntime().exec(cmdArray);

            returnCode = child.waitFor();
            logger.info("Utils.runCmdNoStdout() " + "got return code " +
                returnCode);

            if (returnCode != 0) {

                InputStream errIn = child.getErrorStream();
                InputStreamReader errInR = new InputStreamReader(errIn);
                BufferedReader errBR = new BufferedReader(errInR);

                while ((line = errBR.readLine()) != null) {
                    logger.info("Utils.runCmdNoStdout() " + "stderr: " + line);
                    errLine = errLine + line;
                } // errBR

                logger.trace("Utils.runCmdNoStdout() " + "end read stderr");

                logger.error("Utils.runCmdNoStdout() " +
                    "throwing WrapperException: " + returnCode);
                throw new WrapperException(errLine);
            } // returnCode != 0

        } catch (IOException ioex) { // getRuntime(); readLine()
            logger.error("Utils.runCmdNoStdout(): " + "IOException ioex: " +
                ioex.getMessage());
            throw new WrapperException(ioex.getMessage());
        } catch (InterruptedException iex) { // child.waitFor()

            // will be thrown if this thread is interrupted
            // by another
            logger.error("Utils.runCmdNoStdout(): " +
                "InterruptedException iex: " + iex.getMessage());
            throw new WrapperException(iex.getMessage());
        }

        logger.trace("Utils.runCmdNoStdout() -- EXIT-- ");
    } // runCmdNoStdout


    /*
     * runCmdOneString()
     *
     * Execute an external command that returns a single string
     * result.
     *
     * If external command does not return 0 throw exception with
     * strings from stderr.
     */
    static public String runCmdOneString(String cmdArray[])
        throws WrapperException {
        Logger logger = Logger.getLogger();

        String result = null;

        int retCode = 0;
        String line = null;
        String errLine = "";

        logger.info("Utils.runCmdOneString() cmdArray: " +
            Utils.dumpStringArray(cmdArray));

        try {
            Process child = Runtime.getRuntime().exec(cmdArray);

            InputStream stdIn = child.getInputStream();
            InputStreamReader inR = new InputStreamReader(stdIn);
            BufferedReader inBR = new BufferedReader(inR);

            InputStream errIn = child.getErrorStream();
            InputStreamReader errInR = new InputStreamReader(errIn);
            BufferedReader errBR = new BufferedReader(errInR);

            result = inBR.readLine();
            logger.info("Utils.runCmdOneString() result: " + result);

            retCode = child.waitFor();
            logger.info("Utils.runCmdOneString() " + "returnCode: " + retCode);

            if (retCode != 0) {

                while ((line = errBR.readLine()) != null) {
                    logger.info("err: " + line);
                    errLine = errLine + line;
                }

                logger.info("Utils.runCmdOneString() " + "error return: " +
                    retCode);
                throw new WrapperException(errLine); // no I18n
            }
        } catch (IOException iox) {
            logger.info("Utils.runCmdOneString(): " + "IOException: " +
                iox.getMessage());
            throw new WrapperException(iox.getMessage());
        } catch (InterruptedException iex) {
            logger.info("Utils.runCmdOneString(): " + "InterruptedException: " +
                iex.getMessage());
            throw new WrapperException(iex.getMessage());
        }

        logger.trace("Utils.runCmdOneString() -- EXIT-- ");

        return result;
    } // runCmdOneString


    // -- file & directory manipulations  --

    // mkdir -p
    static public boolean mkdir(String dirname) {
        File dir = new File(dirname);
        boolean success = dir.mkdirs();

        return success;
    } // mkdir

    // rmrfDir(): delete a subtree: rm -rf dirname
    public boolean rmrfDir(String dirname) {
        boolean succeeded = true;

        File dir = new File(dirname);
        String path = dir.getPath();

        String entries[] = dir.list();

        if (!dir.exists()) {
            succeeded = false;

            return succeeded;
        }

        if (!dir.isDirectory()) {
            succeeded = false;

            return succeeded;
        }

        if (entries == null) {
            succeeded = false;

            return succeeded;
        }

        for (int i = 0; i < entries.length; i++) {
            String fname = path + "/" + entries[i];
            File nextFile = new File(fname);

            if (nextFile.isDirectory()) {
                Utils u = new Utils();
                u.rmrfDir(fname);
                nextFile.delete();
            } else {
                nextFile.delete();
            }
        }

        dir.delete();

        return succeeded;
    } // rmrfDir

    public static String dumpStringArray(String sarray[]) {

        if (sarray == null) {
            return "null";
        }

        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < sarray.length; i++) {

            if (i == 0)
                sb.append("[");

            if (i > 0)
                sb.append(",");

            sb.append(sarray[i]);
        }

        sb.append("]");

        return sb.toString();
    } // dumpStringArray

    public boolean isDir(String path) {

        if (path == null) {
            return false;
        }

        File tmp = new File(path);

        return (tmp.isDirectory());
    } // isDir


    /*
     * stringIsUnique (Vector vec, String stingToFind)
     * test if the string stingToFind is NOT already
     * inside vec vector
     * return false otherwise
     */

    public static boolean stringIsUnique(Vector vec, String stringToFind) {

        for (int i = 0; i < vec.size(); i++) {

            if (stringToFind.equals(vec.get(i))) {
                return false;
            }
        }

        return true;
    }


    // test access point
    public static void main(String args[]) {

        if (args.length < 1) {
            System.out.println("Utils.main() " + "requires filename argument ");
            System.exit(0);
        }

        Utils u = new Utils();
        u.rmrfDir(args[0]);
    } // main

} // Utils
