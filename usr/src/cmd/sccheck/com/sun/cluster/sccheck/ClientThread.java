/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * ident        "@(#)ClientThread.java 1.11     08/05/20 SMI"
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.cluster.sccheck;

import java.io.*;

import java.util.*;


/*
 * This Thread class handles the client-server session for an individual
 * server.
 *
 * An instance is instantiated for each server by SccheckClient.
 *
 * ClientThread is given the server's public and private names and some
 * parameters about the report run to be launched. ClientThread
 * instantiates the ClientProtocol and Session objects.
 */
public class ClientThread extends Thread implements Globals {

    private ProgressListener progresslistener;
    private Client parent;
    private String publicName;
    private String privateName;
    private String resultsFilename;
    private String reportFilename;
    private boolean brief = false;
    private int minSeverity = 0;
    private String checkListFilePath = null;
    private String exploArchivePath = null;
    private Logger logger = Logger.getLogger();
    private SCProperties props = SCProperties.getSCProperties();

    /*
     * Constructor:
     *
     * Accept from the client values necessary for setting up a session
     * with a particular server.
     */
    public ClientThread(ProgressListener pl, Client c, String publicN,
        String privateN, String results, String report, boolean brief,
        int minSeverity, String ruleFilePath, String explorerArchivePath) {
        logger.info("ClientThread() -- ENTER -- " + publicN);
        logger.info("ClientThread() resultsFilename: " + results);
        logger.info("ClientThread() reportFilename: " + report);
        logger.info("ClientThread() brief: " + brief);
        logger.info("ClientThread() minSeverity: " + minSeverity);
        logger.info("ClientThread() check rule file: " + ruleFilePath);
        logger.info("ClientThread() explorer archive: " + explorerArchivePath);

        progresslistener = pl;
        parent = c;
        publicName = publicN;
        privateName = privateN;
        resultsFilename = results;
        reportFilename = report;
        checkListFilePath = ruleFilePath;
        exploArchivePath = explorerArchivePath;
        this.brief = brief;
        this.minSeverity = minSeverity;
        logger.trace("ClientThread() -- EXIT -- " + publicN);
    } // ()


    /*
     * run()
     *
     * implements Thread.run()
     * launch session with the server specified in the constructor
     */
    public void run() {

        try {
            remoteOperations();
        } catch (SCException scex) {
            logger.error("ClientThread.run() SCException on: " + publicName +
                ": " + scex.getMessage());

            Object i18nArgs[] = { publicName, scex.getMessage() };
            String i18nMsg = I18n.getLocalized("clientThreadRunError",
                    i18nArgs);
            progresslistener.postErrMsg(i18nMsg);
            parent.postFailure(publicName);
        }
    } // run


    /*
     * remoteOperations()
     *
     * Launch a session with the server:
     *          create a ClientProtocol object to handle the low-level
     *          communication;
     *          negotiate a Session version & create a Session object;
     *          call Session.initClient() then session.runClient() with args
     *            appropriate to the Session version.
     *
     * When new versions of Session are implemented they must be added to
     * the two switches below. Old versions must remain supported.
     *
     * Note: checkRuleFilePath and exploArchivePath argument
     * are only supported with version 2 or older
     * in case of version 1, we send null for both argument
     */
    private void remoteOperations() throws SCException {
        String errorMsg = null;
        ClientProtocol prot = null;

        Session session = null;


        try {
            prot = new ClientProtocol(progresslistener, parent, publicName,
                    privateName, parent.getPrivateClientHostname());

            int version = prot.getCommonVersion(VERSION); // PEx

            session = SessionFactory.getSession(version); // PEx

            logger.trace("ClientThread.remoteOperations() " +
                "initializing SessionV" + version);

            // future Session version must add a new case
            switch (version) {

            case 1:

                if (exploArchivePath != null) {
                    logger.error("- ClientThread.remoteOperations() -\n" +
                        "ERROR:  " + " the server does not support " +
                        " SessionV2 or greater" +
                        " which is required for custom\n" +
                        "-----------------------------------");

                    Object i18nArgs[] = { "" + version };
                    String i18nMsg = I18n.getLocalized(
                            "unsupportedSessionVersion", i18nArgs);
                    throw new SCException(i18nMsg);
                }

                // call original interface for V1
                session.initClient(parent, prot, publicName, privateName,
                    resultsFilename, reportFilename, brief, minSeverity);
                            // throws PEx

                break;

            case 2:

                Object additionalData[] = {
                        checkListFilePath, exploArchivePath
                    };

                // call extended interface
                session.initClient(parent, prot, publicName, privateName,
                    resultsFilename, reportFilename, brief, minSeverity,
                    additionalData);

                // throws PEx
                break;

            default:
                logger.error("ClientThread.remoteOperations(): " + "init: " +
                    "unknown version: " + version);

                Object i18nArgs[] = { "" + version };
                String i18nMsg = I18n.getLocalized("unsupportedSessionVersion",
                        i18nArgs);
                throw new SCException(i18nMsg);
            } // switch

            logger.info("ClientThread.remoteOperations() " +
                "running SessionV" + version);

            // future Session version must add a new case
            switch (version) {

            case 1:
            case 2:
                session.runClient(progresslistener); // PEx

                break;

            default:
                logger.error("ClientThread.remoteOperations(): " + "run: " +
                    "unknown version: " + version);

                Object i18nArgs[] = { "" + version };
                String i18nMsg = I18n.getLocalized("unsupportedSessionVersion",
                        i18nArgs);
                throw new SCException(i18nMsg);
            } // switch

        } catch (ProtocolException pe1) {
            errorMsg = pe1.getMessage();

            String i18nMsg = errorMsg;

            if (errorMsg == null) {
                errorMsg = "Unexpected early return " + "from server.";
                i18nMsg = I18n.getLocalized("unexpectedEarlyReturnFromServer");
            }

            logger.error("ClientThread.remoteOperations() " +
                "in ProtocolException: " + errorMsg);
            throw new SCException(i18nMsg);
        }

    } // remoteOperations


    /*
     * die()
     *
     * Called by SccheckClient to tell this thread to exit prematurely.
     */
    public void die() {
        logger.trace("ClientThread.die(): killing thread for: " + publicName);
        this.destroy();
    }


} // ClientThread
