/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)MessagesBundle.java 1.17   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.cluster.sccheck;

import java.util.ListResourceBundle;


public class MessagesBundle extends java.util.ListResourceBundle {

    static Object contents[][] = {

            /*
             * Keys are derived from message contents. Dot is used
             * sometimes to aid readibility
             */


            /*
             * messages appearing in more than one class
             */

            { "requiredDefinesMissing", "Required defines missing: {0}" },

            {
                "unsupportedSessionVersion",
                "Unsupported Session version: " + "{0}"
            },


            /*
             * SccheckClient
             */
            { "scheck", "sccheck" }, // do not localize this name

            { "earlyExitFormat", "{0}: {1} {2}" },
            // {sccheck}: {message:} {dynamic-msg}

            { "permissionDenied", "Permission denied:" },
            { "componentError", "Component error:" },
            { "internalInvocationError", "Internal invocation error:" },
            { "unableToLoadClusterData", "Unable to load cluster data:" },

            { "noClusterNodeNameMatching", "No cluster node name matching:" },

            {
                "hostlistNotAllowedInNonClusterMode",
                "Hostlist not allowed in non-cluster mode:"
            },

            { "unableToRunChecksOn", "Unable to run checks on:" },
            { "internalError", "Internal error:" },
            { "unknownError", "Unknown error:" },


            { "postProgressFormat", "{0}: {1}" },
            // {sccheck}: {dynamic-msg}

            { "postErrMsgFormat", "{0}: {1}" },
            // {sccheck}: {dynamic-msg}


            { "warnGenDates", "Warning: different checklist versions." },
            {
                "rerunWithVVForMoreDetails",
                "Rerun with -v[v] for more " + "details."
            },
            // do not translate '-v[v]'

            { "showGenDate", "{0} {1}" },
            // {dynamic-name:} {dynamic-msg}


            {
                "interruptionBeforeRequestsCompleted",
                "Interruption before requests completed."
            },

            {
                "requestingExplorerAndNodeReportFrom",
                "Requesting explorer data and node report from {0}."
            },


            { "startingMultiNodeChecks", "Starting multi-node checks." },
            { "multiNodeChecksFinished", "Multi-node checks finished." },


            /*
             * ClientThread
             */
            {
                "explorerFileNotFound",
                "The specified explorer file {0} is not found"
            },

            { "clientThreadRunError", "{0} error: {1}" },

            {
                "unexpectedEarlyReturnFromServer",
                "Unexpected early return " + "from server."
            },

            /*
             * SessionFactory
             */
            { "unableToLoad", "Unable to load {0}.class" },
            { "unableToLoadException", "Unable to load {0}.class: {1}" },

            /*
             * ClientProtocol
             */
            { "unknownHost", "Unknown host: {0} " },
            {
                "waitingForServerStart",
                "Waiting for {0}  to start " + "server..."
            },

            { "unableToContactServer", "Unable to contact server on {0}" },

            {
                "UnexpectedEOF.OnFileRx",
                "ClientProtocol.receiveFile() unexpected EOF."
            },

            /*
             * SessionV1
             */
            { "sessionPermissionDenied", "Session permission denied: {0}" },
            { "explorerFinished", "{0}: Explorer finished." },

            {
                "startingSingleNodeChecks",
                "{0}: Starting single-node " + "checks."
            },

            {
                "singleNodeChecksFinished",
                "{0}: Single-node checks " + "finished."
            },

            { "joiningExistingSession", "Joining existing session." },
            { "explorerRunFailed", "Explorer run failed: " },
            {
                "KE.RunFailedProtocolException",
                "KE run failed: ProtocolException: {0}"
            },

            {
                "KE.RunFailedWrapperException",
                "KE run failed: WrapperException: {0}"
            },

            {
                "noResultsFile",
                "SessionV1.getRemoteFile() no results file \"{0}\" from {1}"
            },

            /*
             * ServerProtocol
             */
            {
                "UnexpectedEOF.OnFileRead",
                "ServerProtocol.sendFile() unexpected EOF on file read."
            },

            /*
             * ExplorerWrapper
             */
            {
                "usingExistingExplorerResults",
                "{0} Using existing explorer " + "results."
            },
            { "execExplorer.progress", " {0}{1}" }, // no <space>
            { "stderr", "stderr: " },
            { "execExplorer.progress.err", "{0}{1} {2}" },
            // no <space> between {0}{1}

            { "explorerExitCode", "Explorer exit code: {0}" },

            {
                "explorerResultsDirectoryNameNotFound",
                "Explorer results directory name not " + "found: {0}"
            },

            {
                "explorerResultsFileNameNotFound",
                "Explorer results file name not found: {0}"
            },

            /*
             * SccheckServer
             */
            { "authenticationFailure", "Authentication failure." },

            {
                "mustConnectViaPrivateNetworkOrLocalhost",
                "Must connect via private network or localhost."
            },

            {
                "mustConnectViaPrivilegedPort",
                "must connect via privileged port"
            },

            {
                "explorerConfigurationFailure",
                "Could not get explorer version;" +
                "Ensure that 'explorer -g' has been executed."
            },

            {
                "explorerVersionFailure",
                "Explorer version \"{0}\" too old; " +
                "\"{1}\" minimum required."
            },

            /*
             * KEWrapper
             */
            { "noUnzippedExplorerResults", "No unzipped explorer results." },

            {
                "explorerResultsEmptyOrNonExistent",
                "Explorer results empty or non-existent: {0}"
            },

            {
                "unpackedExplorerResultsNotFound",
                "Unpacked explorer results not found."
            },

            {
                "unableToUnpackEplorerResults",
                "Unable to unpack eplorer results: {0}"
            },

            { "unpackingChecks", "Unpacking checks..." },
            { "loadingChecks", "Loading checks..." },
            { "runningChecks", "Running checks..." },
            { "checkRunning", "Check {0} is running; {1} percent complete" },

            { "noExplorerResultsToRead", "No explorer results to read." },

            {
                "ResultsWriterEngineClientExitError",
                "ResultsWriterEngineClient exited with error condition"
            },

            {
                "ResultsWriterEngineClientExitTimeout",
                "ResultsWriterEngineClient exited due to comm timeout"
            },

            {
                "ResultsWriterEngineClientExitErrUnknown",
                "ResultsWriterEngineClient exited with unknown error"
            },


            /*
             *
             */

            { "", "" }
        };

    public Object[][] getContents() {
        return contents;
    }
}
