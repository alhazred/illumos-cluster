/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)SessionFactory.java 1.8   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.cluster.sccheck;

/*
 * SessionFactory attempts to instantiate and return a Session object of
 * the requested version, 'N'.
 *
 * The class is expected to be named SessionV'N' in the SESSIONCLASS
 * specified in Globals.
 */

public class SessionFactory implements Globals {

    public static Session getSession(int version) throws SCException {

        Logger logger = Logger.getLogger();
        logger.trace("SessionFactory() -- ENTER -- " + version);

        Session session = null;
        String classname = SESSIONCLASS + version;
        logger.info("SessionFactory() classname: " + classname);

        Object i18nArgs[] = null;
        String i18nMsg = "";

        try {
            Class c = Class.forName(classname);
            Object obj = c.newInstance();
            session = (Session) obj;
        } catch (ClassNotFoundException cnfex) {
            logger.error("Session.getSession() " + "ClassNotFoundException: " +
                cnfex.getMessage());
            i18nArgs = new Object[] { classname };
            i18nMsg = I18n.getLocalized("unableToLoad", i18nArgs);

            throw new SCException(i18nMsg);
        } catch (InstantiationException ie) {
            logger.error("Session.getSession() " + "InstantiationException: " +
                ie.getMessage());
            i18nArgs = new Object[] { classname, ie.getMessage() };
            i18nMsg = I18n.getLocalized("unableToLoadException", i18nArgs);

            throw new SCException(i18nMsg);
        } catch (IllegalAccessException iae) {
            logger.error("Session.getSession() " + "IllegalAccessException: " +
                iae.getMessage());
            i18nArgs = new Object[] { classname, iae.getMessage() };
            i18nMsg = I18n.getLocalized("unableToLoad.exception", i18nArgs);
            throw new SCException(i18nMsg);
        }

        logger.trace("SessionFactory() --EXIT-- " + session);

        return session;
    } // getSession
} // SessionFactory
