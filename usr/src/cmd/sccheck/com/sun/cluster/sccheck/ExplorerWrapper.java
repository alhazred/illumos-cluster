/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)ExplorerWrapper.java 1.9   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.cluster.sccheck;

import java.io.*;

import java.util.*;


/*
 * This class wraps execution of the Explorer external component.
 *
 * Progress strings and error messages are returned via the
 * ProgressListener
 *
 *      runExplorer()
 *              invokes Explorer and returns the compressed results
 *              filename
 *      getExplorerDir()
 *              returns the directory holding uncompressed results
 */

public class ExplorerWrapper implements Globals {

    private ProgressListener progressListener = null;
    private String originID = null;
    private Logger logger = Logger.getLogger();
    private SCProperties props = SCProperties.getSCProperties();

    /*
     * Constructor: accept ProgressListener & optional ID string
     */
    public ExplorerWrapper(ProgressListener pl, String id) {
        progressListener = pl;
        originID = id;

        if (originID == null) {
            originID = "";
        } else {
            originID = originID + ": ";
        }

    } // ()


    /*
     * runExplorer()
     *
     * Check 'explorer.skip' property:
     *      if true:
     *          attempt to find most recent gzipped explorer results
     *          attempt to find most recent uncompressed explorer results dir
     *          validate dirname against gzip name
     *          if gzip or dir not found or validation fails ignore the
     *            property and run explorer anyway
     *      if false:
     *          run explorer
     *
     * User is _always_ notified if existing results are being used.
     *
     * Return compressed results filename.
     */
    public String runExplorer() throws WrapperException {
        logger.trace("ExplWrapper.runExplorer() -- ENTER-- ");

        String fname = null;

        boolean skipexpl = SCProperties.booleanFromProps(PROP_SC_EXPLSKIP,
                false, props);
        logger.info("ExplWrapper.runExplorer() explorer.skip: " + skipexpl);

        if (!skipexpl) {
            execExplorer();
            fname = getExplorerGzipName();
        } else { // attempt skip

            try {
                logger.trace("ExplWrapper.runExplorer() " +
                    "skip requesting fnname");
                fname = getExplorerGzipName();
                logger.info("ExplWrapper.runExplorer() " + "fname for skip: " +
                    fname);

                // not returning dname now but if it
                // doesn't exist we can't skip
                logger.trace("ExplWrapper.runExplorer() " +
                    "skip requesting dirname");

                String dname = getExplorerDirname();
                logger.info("ExplWrapper.runExplorer() " + "dname for skip: " +
                    dname);

                // validate dname relative to fname
                if (!fname.startsWith(dname)) {
                    logger.trace("ExplWrapper.runExplorer() " +
                        "dname/fname mismatch; " + "forcing explorer run");
                    throw new WrapperException();
                }

                Object i18nArgs[] = { originID };
                String i18nMsg = I18n.getLocalized(
                        "usingExistingExplorerResults", i18nArgs);

                // QUIET messages cannot be suppressed by user
                progressListener.postProgress(QUIET, i18nMsg);

            } catch (WrapperException wex) {

                // no valid existing fname and/or dname so run
                // explorer anyway
                logger.info("ExplWrapper.runExplorer() " +
                    "valid explorer results (both packed " +
                    "& unpacked) not found: forcing explorer " + "run");
                execExplorer();
                fname = getExplorerGzipName();
            }
        } // attempt skip

        logger.trace("ExplWrapper.runExplorer() -- EXIT--: " + fname);

        return fname;
    } // runExplorer


    /*
     * getExplorerDir()
     *
     * Return directory name of uncompressed explorer results
     *
     * Public signature calls private implementation.
     */
    public String getExplorerDir() throws WrapperException {
        logger.trace("ExplWrapper.getExplorerDir() -- ENTER--");

        String dname = getExplorerDirname();
        logger.info("ExplWrapper.getExplorerDir() -- EXIT--: " + dname);

        return dname;
    } // getExplorerDir


    /*
     * execExplorer()
     *
     * Actual invocation of Explorer (represented by the
     * explorerWrapper.ksh script, defined in Globals):
     *
     *          create/launch the Process
     *          read explorer's stdout and stderr (if any) from process;
     *            pass same back to ProgressListener
     *
     *            Note that stderr from Explorer itself is sent as
     *            progress, not as error; failure to launch
     *            Explorer itself or if Explorer itself exits non-0
     *            results in exceptions which eventually
     *            get back to client as errors.
     *
     *          check for any trailing stderr
     *          obtain exit code; throw exception if code != 0
     *
     * No return values.
     */
    private void execExplorer() throws WrapperException {
        logger.trace("ExplWrapper.execExplorer() -- ENTER-- ");

        int returnCode = 0;
        String line = null;

        try {
            String cmdArray[] = { EXPLORER, "run" };
            logger.info("ExplWrapper.execExplorer() cmdArray: " +
                Utils.dumpStringArray(cmdArray));

            Process child = Runtime.getRuntime().exec(cmdArray);

            InputStream stdIn = child.getInputStream();
            InputStreamReader inR = new InputStreamReader(stdIn);
            BufferedReader inBR = new BufferedReader(inR);

            InputStream errIn = child.getErrorStream();
            InputStreamReader errInR = new InputStreamReader(errIn);
            BufferedReader errBR = new BufferedReader(errInR);

            Object i18nArgs[] = null;
            String i18nMsg = "";

            logger.trace("ExplWrapper.execExplorer() " +
                "starting read stdout/stderr");

            while ((line = inBR.readLine()) != null) {
                logger.info("ExplWrapper.execExplorer() " + "stdout: " + line);
                i18nArgs = new Object[] { originID, line };
                i18nMsg = I18n.getLocalized("execExplorer.progress", i18nArgs);

                progressListener.postProgress(VVERBOSE, i18nMsg);

                if (errBR.ready()) {
                    line = errBR.readLine();

                    if (line != null) {
                        logger.info("ExplWrapper.execExplorer()" +
                            " mid stderr: " + line);
                        i18nMsg = I18n.getLocalized("stderr");
                        i18nArgs = new Object[] { originID, i18nMsg, line };
                        i18nMsg = I18n.getLocalized("execExplorer.progress.err",
                                i18nArgs);
                        progressListener.postProgress(VVERBOSE, i18nMsg);

                    } else {
                        logger.info("ExplWrapper.execExplorer()" +
                            " mid stderr: got null");
                    }
                }
            } // inBR

            logger.trace("ExplWrapper.execExplorer() " +
                "end reading stdout/stderr");

            logger.trace("ExplWrapper.execExplorer() " +
                "starting final read stderr");

            while ((line = errBR.readLine()) != null) {
                logger.info("ExplWrapper.execExplorer() " + "final stderr: " +
                    line);
                i18nMsg = I18n.getLocalized("stderr");
                i18nArgs = new Object[] { originID, i18nMsg, line };
                i18nMsg = I18n.getLocalized("execExplorer.progress.err",
                        i18nArgs);
                progressListener.postProgress(VVERBOSE, i18nMsg);
            } // errBR

            logger.trace("ExplWrapper.execExplorer() " +
                "end final read stderr");

            returnCode = child.waitFor();
            logger.info("ExplWrapper.execExplorer() " + "got return code " +
                returnCode);

            if (returnCode != 0) {
                logger.error("ExplWrapper.execExplorer() " +
                    "throwing WrapperException: " + returnCode);
                i18nArgs = new Object[] { new String("" + returnCode) };
                i18nMsg = I18n.getLocalized("explorerExitCode", i18nArgs);

                throw new WrapperException(i18nMsg);
            } // returnCode != 0

        } catch (IOException ioex) { // getRuntime(); readLine()
            logger.error("ExplWrapper.execExplorer(): " + "IOException ioex: " +
                ioex.getMessage());
            throw new WrapperException(ioex.getMessage());
        } catch (InterruptedException iex) { // child.waitFor()

            // will be thrown if this thread is interrupted
            // by another
            logger.error("ExplWrapper.execExplorer(): " +
                "InterruptedException iex: " + iex.getMessage());
            throw new WrapperException(iex.getMessage());
        }

        logger.trace("ExplWrapper.execExplorer() -- EXIT-- ");
    } // execExplorer


    /*
     * getExplorerGzipName()
     *
     * Fetch and return filename of most recent compressed
     * Explorer results.
     *
     * Exec explorerWrapper.ksh to obtain the name.
     */
    private String getExplorerGzipName() throws WrapperException {

        String fname = null;
        String cmdArray[] = { EXPLORER, "gzipname" };

        try {
            fname = Utils.runCmdOneString(cmdArray);
        } catch (WrapperException wex) {
            Object i18nArgs[] = { wex.getMessage() };
            String i18nMsg = I18n.getLocalized(
                    "explorerResultsDirectoryNameNotFound", i18nArgs);

            throw new WrapperException(i18nMsg);
        }

        return fname;
    } // getExplorerGzipName


    /*
     * getExplorerDirname()
     *
     * Fetch and return directory name of most recent uncompressed
     * Explorer results.
     *
     * Exec explorerWrapper.ksh to obtain the name.
     */
    private String getExplorerDirname() throws WrapperException {

        String dname = null;
        String cmdArray[] = { EXPLORER, "dirname" };

        try {
            dname = Utils.runCmdOneString(cmdArray);
        } catch (WrapperException wex) {
            Object i18nArgs[] = { wex.getMessage() };
            String i18nMsg = I18n.getLocalized(
                    "explorerResultsDirectoryNameNotFound", i18nArgs);

            throw new WrapperException(i18nMsg);
        }

        return dname;
    } // getExplorerDirname


} // ExplorerWrapper
