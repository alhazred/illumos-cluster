/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * ident        "@(#)Globals.java 1.20     08/05/20 SMI"
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.cluster.sccheck;

/*
 * Constants used by the sccheck package. No I18N.
 */


public interface Globals {

    // The property to pass the caller's program name
    String PROGNAME = "sccheck.progname";

    String LIBDIR = "/usr/cluster/lib/sccheck/";
    String VARDIR = "/var/cluster/sccheck/";
    String VARTMPDIR = VARDIR + "tmp";
    String CLIENT_VARTMPDIR = VARTMPDIR + "/client";
    // will have client ID appended

    String SERVER_VARTMPDIR = VARTMPDIR + "/server";
    String ETCDIR = "/etc/default/";
    String KAEHOME = LIBDIR + "kae";
    String FACTS_ZIP = KAEHOME + "/storage/facts.zip";

    // template file read by SCXmlToText.java
    // results in generated xsl file
    String XSL_TMPL_NAME = LIBDIR + "SCXmlToText.xsl.tmpl";

    // xsl filename *requires* leading slash but no dirname!!
    // CLASSPATH must include dir holding xsl file
    String XSL_OUTF_NAME = "/SCXmlToText.xsl";
    String XSL_DELIM = "<!-- scheck section marker";

    // Strings for text report: must agree with sccheck.ksh
    String SEVERITY_STRINGS[] = {
            "NONE", "LOW", "MODERATE", "HIGH", "CRITICAL"
        };


    // session versioning
    String SESSIONCLASS = "com.sun.cluster.sccheck.SessionV";

    /*
     * highest supported version in this release
     * VERSION is concatted onto SESSIONCLASS
     * by SessionFactory
     */
    int VERSION = 2;


    // dir & filename definitions: must be in synch w/sccheck.ksh
    String PROPSFILE = ETCDIR + "sccheck";

    String EXPLORER = LIBDIR + "explorer_wrapper"; // ksh
    String EXPLORER_VERSION_UNDEF = "undetermined"; // tested by SccheckServer
    String EXPLORER_MIN_VER = "3.5.1"; // tested by SccheckServer

    String CHECKLIST_CLUSTER_MULTI = LIBDIR + "checklist.cluster.multinode.xml";
    String CHECKLIST_CLUSTER_SINGLE = LIBDIR +
        "checklist.cluster.singlenode.xml";
    String CHECKLIST_NONCLUSTER = LIBDIR + "checklist.noncluster.xml";

    String SERVER_REPORTSDIR = SERVER_VARTMPDIR;
    String SERVER_REPORTNAME = "results-tmp";

    String CLIENT_REPORTNAME = "sccheck-results";

    // sccheck logs
    String CLIENTLOG = "/var/cluster/logs/sccheck/client.log";
    String SERVERLOG = "/var/cluster/logs/sccheck/server.log";

    // must agree with sccheckd.ksh
    String SYSLOG_CMD = "/usr/cluster/lib/sc/scds_syslog ";
    String SYSLOG_PRI_FLAG = " -p ";
    String SYSLOG_PRI_NOTICE = "daemon.notice";
    String SYSLOG_PRI_ERROR = "daemon.error";
    String SYSLOG_TAG_FLAG = " -t ";
    String SYSLOG_TAG = "sccheckd";
    String SYSLOG_MSG_FLAG = " -m ";


    // socket ports; may be overridden via /etc/default/sccheck
    int INETPORT = 7123;
    int SERVERPORT = 7124;

    /*
     * the "root-privileged" ports are 1 - 1023
     * sccheck will use the upper part of this range
     * for outgoing connection to server
     */
    int MAXOUTPORT = 1023;
    int MINOUTPORT = 513;


    // protocol "progress" codes
    int PROTProgressStr = 1;
    int PROTExecExitOK = 2;
    int PROTExecErrRun = 3;
    int PROTExecErrExit = 4;

    // error codes
    int EXITPermERR = 101; // session denied (permission)
    int EXITComponentERR = 102; // explorer or ke exit err
    int EXITUsageERR = 103; // usage error: possibly invalid args
    int EXITConfigERR = 104; // failed to load cluster
                             // data via defines
    int EXITBadNameERR = 105; // invalid hostnames
    int EXITNodeERR = 107; // one or more nodes exited in error
    int EXITFrameERR = 108; // internal error

    // verbosity codes
    int QUIET = 0;
    int VERBOSE = 1;
    int VVERBOSE = 2;


    // property names/values
    String DEVNULL = "/dev/null";
    String SFX_TXT = ".txt";
    String SFX_XML = ".xml";

    // sccheck
    String PROP_SCK_CLMODE = "sccheck.clustermode";
    String PROP_SCK_CLNAME = "sccheck.clustername";
    String PROP_SCK_PRIVNET = "sccheck.privatenet";
    String PROP_SCK_PRIVNETMASK = "sccheck.privatenetmask";
    String PROP_SCK_LOCALNAME = "sccheck.localname";
    String PROP_SCK_PRIVLOCALNAME = "sccheck.privatelocalname";
    String PROP_SCK_PUBLICNAMES = "sccheck.publicnodenames";
    String PROP_SCK_PRIVATENAMES = "sccheck.privatenodenames";
    String PROP_SCK_OUTPUTDIR = "sccheck.outputDir";
    String PROP_SCK_EXPLVER = "sccheck.explorerVer";
    String PROP_SCK_EXPLGZDIR = "sccheck.explorersgzDir";
    String PROP_SCK_EXPLUNPACKDIR = "sccheck.explorersUnpackDir";
    String PROP_SCK_GUNZIP = "sccheck.gunzip";
    String PROP_SCK_TAR = "sccheck.tar";
    String PROP_SCK_CLIENTNUM = "sccheck.clientnumber";
    String PROP_SCK_BRIEF = "sccheck.brief";
    String PROP_SCK_VERBOSE = "sccheck.verbose";
    String PROP_SCK_VVERBOSE = "sccheck.vverbose";
    String PROP_SCK_HOSTLIST = "sccheck.hostlist";
    String PROP_SCK_SEVERITY = "sccheck.severity";
    String PROP_SCK_EXPLFILES = "sccheck.explorerFilesPath";
    String PROP_SCK_SINGLE_RULEFILE = "sccheck.singleNodecheckFilePath";
    String PROP_SCK_MULTI_RULEFILE = "sccheck.multiNodeModecheckFilePath";

    String PROP_KE_CLIENTLOG = "ke.client.log";
    String PROP_KE_SERVERLOG = "ke.server.log";
    String PROP_KE_CLIENT_XSLDIR = "ke.client.xsldir";
    String PROP_KE_SERVER_XSLDIR = "ke.server.xsldir";

    // SCProperties defined in /etc/default/sccheck (sccheck.dfl)
    String PROP_SC_LOGGING = "LOGGING";
    String PROP_SC_INETPORT = "INET_PORT";
    String PROP_SC_SERVERPORT = "SERVER_PORT";
    String PROP_SC_CLIENTWAITCOUNT = "CLIENT_WAIT_COUNT";
    String PROP_SC_CLIENTWAITINTERVAL = "CLIENT_WAIT_INTERVAL";
    String PROP_SC_EXPLSKIP = "EXPLORER_SKIP";


} // Globals
