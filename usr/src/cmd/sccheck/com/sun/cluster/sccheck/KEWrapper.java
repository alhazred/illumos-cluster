/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * ident        "@(#)KEWrapper.java 1.14     08/05/20 SMI"
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.cluster.sccheck;

import com.sun.eras.common.checks.*;
import com.sun.eras.common.checkstorage.*;
import com.sun.eras.common.exception.NestedException;
import com.sun.eras.common.kaeresult.*;
import com.sun.eras.common.logging4.*;
import com.sun.eras.common.translator.*;
import com.sun.eras.common.translator.result.*;
import com.sun.eras.kae.engine.*;
import com.sun.eras.kae.io.input.explorerDir.ExplorerDataCollectorDesignator;

import java.io.*;

import java.rmi.*; // for checkListDesignator.getCheckList()

import java.util.*;


/*
 *  This class wraps sccheck's use of the Knowledge Engine and
 *  implements EngineListener
 *
 *      constructors can accept either a Vector of filenames holding
 *        compressed Explorers or a String[] of directory names holding
 *        uncompressed explorer results
 *
 *      runKE()
 *
 *              accept checklist filename
 *              unpack explorers if necessary
 *              execute KE & create reports
 *                remember maxSeverity of all failed checks encountered
 *                reports are named based on 'reportNameBase'
 *      getMaxSeverity()
 *              return maxSeverity of all failed checks encountered
 *      getGenDate()
 *              return generation date of checklist used in run
 *      removeUnpackedExplorers()
 *              'rm -rf' of results directory
 */

public class KEWrapper implements Globals, EngineListener {

    // results tallies for Engine interface methods
    private static int checksComplete = 0;
    private static int totalChecks = 0;
    private static int currcheck = 0;

    // to check for engine errors
    private static boolean engineErrorFlag = false;
    private static String engineErrorMessage = null;

    private String reportNameBase;
    private com.sun.cluster.sccheck.Logger logger =
        com.sun.cluster.sccheck.Logger.getLogger();
    private com.sun.eras.common.logging4.Logger kaelogger = null;

    private String gunzipCmd = null;
    private String tarCmd = null;

    private String explorersUnpackDir;
    private String unpackedExplorers[];
    private ProgressListener progresslistener;
    private String genXslDir; // generate xsl file in this dir
    private Vector zippedExplorers;
    private int minSeverity = 0; // for filtering checks by severity
    private int maxSeverity = 0; // updated by checkFinished()
    private int numFilteredChecks = 0;
    private String genDate = null;
    private boolean brief = false;


    /*
     * Constructor
     *
     * accept Vector of filenames of compressed explorer results
     * accept a dirname in which to unpack them
     * accept parameters for report preparation
     * accept reportNameBase upon which to base report names
     * accept name of logfile for kelogger to use
     * accept xslTmpDir in which xsl file is created from xsl template
     */
    public KEWrapper(ProgressListener pl, String reportNameBase,
        Vector zippedExplorers, String expupkdir, boolean brief,
        int minSeverity, String kelogfile, String xslTmpDir)
        throws WrapperException {
        this(pl, reportNameBase, zippedExplorers, expupkdir, null, brief,
            minSeverity, kelogfile, xslTmpDir);
    } // ()

    /*
     * Constructor
     *
     * accept String[] of directory names holding uncompressed
     *          explorer results
     * accept parameters for report preparation
     * accept reportNameBase upon which to base report names
     * accept name of logfile for kelogger to use
     * accept xslTmpDir in which xsl file is created from xsl template
     */
    public KEWrapper(ProgressListener pl, String reportNameBase,
        String unpackedExplorers[], boolean brief, int minSeverity,
        String kelogfile, String xslTmpDir) throws WrapperException {

        this(pl, reportNameBase, null, null, unpackedExplorers, brief,
            minSeverity, kelogfile, xslTmpDir);
    } // ()


    /*
     * Constructor
     *
     * union of two user constructors.
     *
     */
    public KEWrapper(ProgressListener pl, String reportNameBase,
        Vector zippedExplorers, String expupkdir, String unpackedExplorers[],
        boolean brief, int minSeverity, String kelogfile, String xslTmpDir)
        throws WrapperException {
        logger.info("KEWrapper()  -- START --");

        this.minSeverity = minSeverity;
        this.brief = brief;
        this.genXslDir = xslTmpDir;

        // setup the ke logger and its logfile
        kaelogger = com.sun.eras.common.logging4.Logger.getLogger(
                KEWrapper.class.getName());

        if (kelogfile == null) {
            kelogfile = "/var/cluster/logs/sccheck/ke-default.log";
        }

        try {
            com.sun.eras.common.logging4.FileHandler fHandler =
                new com.sun.eras.common.logging4.FileHandler(kelogfile, false);
            kaelogger.addHandler(fHandler);
        } catch (IOException ioex) {
            logger.info("KEWrapper(): ioex: " + ioex.getMessage());
        }

        gunzipCmd = System.getProperty(PROP_SCK_GUNZIP);
        tarCmd = System.getProperty(PROP_SCK_TAR);

        logger.info("KEWrapper() ke log: " + kelogfile);
        logger.info("KEWrapper() genXslDir: " + genXslDir);

        logger.info("KEWrapper() gunzipCmd: " + gunzipCmd);
        logger.info("KEWrapper() tarCmd: " + tarCmd);

        logger.info("KEWrapper() reportNameBase: " + reportNameBase);
        logger.info("KEWrapper() zippedExplorers: " + zippedExplorers);

        if (expupkdir == null) {
            explorersUnpackDir = System.getProperty(PROP_SCK_EXPLUNPACKDIR);
        } else {
            explorersUnpackDir = expupkdir;
        }

        logger.info("KEWrapper() explorersUnpackDir: " + explorersUnpackDir);
        logger.info("KEWrapper() unpackedExplorers[]: " +
            Utils.dumpStringArray(unpackedExplorers));
        logger.info("KEWrapper() minSeverity: " + minSeverity);
        logger.info("KEWrapper() brief: " + brief);

        this.reportNameBase = reportNameBase;

        progresslistener = pl;
        this.zippedExplorers = zippedExplorers;
        this.unpackedExplorers = unpackedExplorers;
        this.minSeverity = minSeverity;
    } // ()


    /*
     * public queries
     */
    public int getMaxSeverity() {
        return maxSeverity;
    } // getMaxSeverity

    public String getGenDate() {
        return genDate;
    } // getGenDate


    /*
     * removeUnpackedExplorers()
     *
     * forcibly delete expanded explorers
     */
    public void removeUnpackedExplorers() {

        // forcibly delete expanded explorers
        Utils u = new Utils();
        u.rmrfDir(explorersUnpackDir);
        u.mkdir(explorersUnpackDir);
    } // removeUnpackedExplorers


    /*
     * runKE()
     *
     * accept checklist filename
     * unpack explorers if necessary
     * call for KE run which also produces reports
     * reports are named based on 'reportNameBase'
     */
    public void runKE(String checklist) throws WrapperException {

        logger.trace("KEWrapper.runKE() -- ENTER: " + checklist);

        // explorer input to KE is String[] of unpacked dirs
        String unzippedExplorers[] = null;

        if (zippedExplorers != null) {
            unzippedExplorers = unpackExplorers(zippedExplorers);
        } else {
            unzippedExplorers = unpackedExplorers;
        }

        logger.info("KEWrapper.runKE(): " +
            Utils.dumpStringArray(unzippedExplorers));

        if (unzippedExplorers == null) {
            logger.error("KEWrapper.runKE(): " + "no unzipped explorers");

            String i18nMsg = I18n.getLocalized("noUnzippedExplorerResults");
            throw new WrapperException(i18nMsg);
        }


        // invoke the KE component
        // reports will appear on disk
        runEngine(checklist, unzippedExplorers); // throws WEx

        logger.trace("KEWrapper.runKE() -- EXIT --");
    } // runKE


    /*
     * unpackExplorers()
     *
     * Accept a Vector of packed (compressed tar) Explorer results
     * file names and unpacks them.
     *
     * Return an array of Strings where each entry names the top
     * of an unpacked explorer hierarchy.
     */
    private String[] unpackExplorers(Vector tgzV) throws WrapperException {
        logger.trace("KEWrapper.unpackExplorers() --ENTER--");

        Utils u = new Utils();

        // return unpacked explorer directory names
        String expdirs[] = new String[tgzV.size()];
        int index = 0; // points into expdirs

        String i18nMsg = I18n.getLocalized("unpackingChecks");
        progresslistener.postProgress(VVERBOSE, i18nMsg);

        File f = null;
        // incoming filenames must be fully qualified

        // for each filename in Vector
        for (Enumeration e = tgzV.elements(); e.hasMoreElements();) {
            String tgzf = (String) e.nextElement();

            if (u.isDir(tgzf)) {

                // user may specified explorer archive
                // from command line
                // he may supply unpacked explorer archive
                // (directory path).
                logger.info("KEWrapper.unpackExplorers() " +
                    "skip unpacking of " + tgzf);
                expdirs[index++] = tgzf;

                continue;
            }

            logger.info("KEWrapper.unpackExplorers() " + "will unpack " + tgzf);

            // check for empty file
            f = new File(tgzf);

            if (f.length() == 0) {
                logger.error("KEWrapper.unpackExplorers() " +
                    "file empty or non-existent: " + tgzf);

                Object i18nArgs[] = { tgzf };
                i18nMsg = I18n.getLocalized("explorerResultsEmptyOrNonExistent",
                        i18nArgs);
                throw new WrapperException(i18nMsg);
            }

            try {
                String expldir = unpackSingleExplorer(tgzf, explorersUnpackDir);
                expdirs[index++] = expldir;

            } catch (WrapperException wex) {
                logger.error("KEWrapper.unpackExplorers() " + "in wex: " +
                    wex.getMessage());
                throw new WrapperException(wex.getMessage());
            }
        } // for tgzV.elements

        logger.trace("KEWrapper.unpackExplorers() --EXIT--");

        return expdirs;
    } // unpackExplorers


    /*
     * unpackSingleExplorer()
     *
     * Accept a fully qualified compressed tar file name and the
     * directory to unpack it into. Test that it's a valid gzip
     * then unpack it.
     * Return a fully qualified pathname to the top of the unpacked
     * explorer hierarchy.
     */
    private String unpackSingleExplorer(String tgzf, String unpackDir)
        throws WrapperException {

        logger.trace("KEWrapper.unpackSingleExplorer() -- ENTER -- " + tgzf);

        String cmds = null;
        String unpackedPath = null;
        String i18nMsg = null;

        try {

            // create a uniquely named work dir under the unpackDir
            String uniquifier = Integer.toHexString(
                    (new Long(System.currentTimeMillis())).intValue());
            String tmpDirPath = unpackDir + File.separator + "sccheck_" +
                uniquifier;

            Utils.mkdir(tmpDirPath);

            File tmpDirFile = new File(tmpDirPath);

            // test the zipfile
            cmds = gunzipCmd + " -t " + tgzf; // noI18n
            logger.trace("KEWrapper.unpackSingleExplorer(): " +
                "testing zipfile");
            Utils.runScript(cmds); // WEx

            // unpack: pipe gunzip into tar
            logger.trace("KEWrapper.unpackSingleExplorer(): " + "unpacking");

            /*
             * Provide -d to gunzip just in case the actual
             * command isn't named gunzip; gzip will work, too,
             * given this 'decompress' flag.
             */
            cmds = "cd " + tmpDirPath + "; " + gunzipCmd + " -dc " + tgzf +
                " | " + tarCmd + " xif - "; // noI18n
            Utils.runScript(cmds); // WEx

            // figure out what the unpacked hierarchy calls itself
            logger.trace("KEWrapper.unpackSingleExplorer(): " +
                "getting result name");

            String files[] = tmpDirFile.list();
            String resultDirName = null;

            /*
             * this dir should have only '.', '..',
             * and our results in it
             */
            for (int i = 0; i < files.length; i++) {

                if (!(files[i].startsWith("."))) {
                    resultDirName = files[i];

                    break;
                }
            }

            if (resultDirName == null) {
                logger.info("KEWrapper.unpackSingleExplorer(): " +
                    "couldn't get resultDirName");
                i18nMsg = I18n.getLocalized("unpackedExplorerResultsNotFound");
                throw new WrapperException(i18nMsg);
            }

            logger.info("KEWrapper.unpackSingleExplorer(): " +
                "resultDirName: " + resultDirName);

            // fully qualified result will be...
            unpackedPath = unpackDir + File.separator + resultDirName;

            // shift results from tmpDir up to unpackDir
            logger.trace("KEWrapper.unpackSingleExplorer(): " +
                "pushing tmpDir to unpackDir");

            // make sure no name collisions in destination
            String destPath = unpackDir + File.separator + resultDirName;
            new Utils().rmrfDir(destPath);

            // do the move
            cmds = "cd " + tmpDirPath + "; " + "/usr/bin/mv " + resultDirName +
                " " + unpackDir;
            Utils.runScript(cmds); // WEx

            // and get rid of the now-empty tmp dir
            new Utils().rmrfDir(tmpDirPath);

        } catch (WrapperException wex) {
            logger.error("KEWrapper.unpackSingleExplorer() in wex: " +
                wex.getMessage());

            Object i18nArgs[] = { tgzf };
            i18nMsg = I18n.getLocalized("unableToUnpackEplorerResults",
                    i18nArgs);
            throw new WrapperException(i18nMsg + wex.getMessage());
        }

        logger.trace("KEWrapper.unpackSingleExplorer() -- EXIT -- " +
            unpackedPath);

        return unpackedPath;
    } // unpackSingleExplorer


    /*
     * readGenDate()
     *
     * Parse and return the 'generation date' field near the end of
     * the XML checklist file: read last 500 bytes of checklist file
     * into byte[] then search for the marker and pick out the
     * sequence of interest, for example:
     *          <generation_date GMT='2003.01.22 at 21:30:26 GMT'>
     */
    private String readGenDate(String checklist) {
        logger.trace("KEWrapper.readGenDate() -- ENTER -- " + checklist);

        byte checks[] = null;

        try {
            RandomAccessFile raf = new RandomAccessFile(checklist, "r");
                    // read-only

            long len = raf.length();
            raf.seek(len - 500);
            checks = new byte[500];
            raf.readFully(checks);

        } catch (FileNotFoundException fnfex) {
            logger.info("genDate() fnfex: " + fnfex.getMessage());
        } catch (EOFException eofex) {
            logger.info("genDate() eofex: " + eofex.getMessage());
        } catch (IOException ioex) {
            logger.info("genDate() ioex: " + ioex.getMessage());
        }

        String checksStr = new String(checks);

        String gd = null;
        String startMark = "<generation_date GMT='";

        try {
            int gdPos = checksStr.indexOf(startMark);

            if (gdPos > -1) {
                int startPos;
                int endPos;
                startPos = gdPos + startMark.length();
                endPos = checksStr.indexOf("'>");

                if ((endPos > -1) && (startPos > -1) && (endPos > startPos)) {
                    gd = checksStr.substring(startPos, endPos);
                }
            }
        } catch (Exception e) { // insurance
            gd = "Unable to read generation date" + e.getMessage();
        }

        logger.trace("KEWrapper.readGenDate() -- EXIT: " + gd);

        return gd;
    } // readGenDate


    /*
     * runEngine()
     *
     * Accept name of checklist to use.
     * Accept String[] of unpacked explorer results dirnames.
     * Read generation date (see getGenDate().)
     * Create the engine and set severity filtering.
     * Launch the engine (starts its own thread.)
     * Engine thread will be updating the count of checks complete.
     * Loop, calculating percent complete until engine thread expires,
     *          sending status to ProgressListener.
     * Query the severity filter for filtered count; call for
     *          report generation.
     */
    private void runEngine(String checklist, String expdirs[])
        throws WrapperException {

        logger.trace("KEWrapper.runEngine():-- ENTER --");

        String i18nMsg = null;
        i18nMsg = I18n.getLocalized("loadingChecks");
        progresslistener.postProgress(VVERBOSE, i18nMsg);

        if ((expdirs == null) || (expdirs.length == 0)) {
            logger.error("KEWrapper.runEngine() " +
                "null or 0 explorers -- EXIT--");
            i18nMsg = I18n.getLocalized("noExplorerResultsToRead");
            throw new WrapperException(i18nMsg);
        }

        /*
         * Fetch the checklist generation date.
         * Will send it back to Client along with the
         * reports later.
         */
        genDate = readGenDate(checklist);

        // create & launch the engine

        SCSeverityCheckFilter severityFilter = new SCSeverityCheckFilter(
                minSeverity);
        logger.info("KEWrapper.runEngine() expdirs[0]: " + expdirs[0]);

        EngineControllerAPI engine = null;

        try {
            engine = getEngine(checklist, expdirs);
            logger.info("KEWrapper.runEngine() made engine");
            engine.setCheckFilter(severityFilter);
            engine.addListener(this);

            i18nMsg = I18n.getLocalized("runningChecks");
            progresslistener.postProgress(VVERBOSE, i18nMsg);

            /*
             * Execute engine in a separate thread
             * and wait for completion, peeking from time to time
             * for number of checks run so far.
             */
            Thread t = engine.executeAsynchronously("ekeThread");

            while (t.isAlive()) {
                t.join(5000);

                int percComp = ((checksComplete * 100) / totalChecks);

                if (t.isAlive()) {
                    Object i18nArgs[] = { "" + currcheck, "" + percComp };

                    // (force int to String)
                    i18nMsg = I18n.getLocalized("checkRunning", i18nArgs);
                    progresslistener.postProgress(VVERBOSE, i18nMsg);
                }
            }

        } catch (RemoteException rex) {
            logger.error("KEWrapper.runEngine(): " + "RemoteException in " +
                "runEngine(): " + rex.getMessage());
            throw new WrapperException(rex.getMessage());
        } catch (Exception ex) { // insurance
            logger.error("KEWrapper.runEngine(): " +
                "Exception in runEngine(): " + ex.getMessage());
            throw new WrapperException(ex.getMessage());
        }

        if (engineErrorFlag) {
            String errMsg = new String(engineErrorMessage);
            engineErrorFlag = false;
            engineErrorMessage = null;
            throw new WrapperException(errMsg);
        }

        // now fetch the number of checks filtered out
        numFilteredChecks = severityFilter.getNumFilteredChecks();
        logger.info("KEWrapper.runEngine(): numFilteredChecks: " +
            numFilteredChecks);
        logger.trace("KEWrapper.runEngine(): " +
            "engine.executeAsynchronously() finished ***");

        // And produce our product...
        if (engine != null) {
            writeReports(engine); // throws WEx
        }

        logger.trace("KEWrapper.runEngine() -- EXIT -- ");
    } // runEngine


    /*
     *      writeReports()
     *
     * Fetch check-run results from engine in the form of XML string.
     * Call for results to be written to reports in XML and text formats.
     */
    private void writeReports(EngineControllerAPI engine)
        throws WrapperException {
        logger.trace("KEWrapper.writeReports() -- ENTER -- ");

        StackTraceElement ste[] = null;
        String err = "";

        try {
            String xml = getXmlResults(engine);

            // reportNameBase set by constructor
            writeXmlFile(xml, reportNameBase + SFX_XML);
            writeTextFile(xml, reportNameBase + SFX_TXT);

        } catch (EngineException e0) {
            logger.info("KEWrapper.writeReports() EngineException: " +
                e0.getMessage());
            ste = e0.getStackTrace();
            err = e0.getMessage();
        } catch (UnsupportedEncodingException e1) {
            logger.info("KEWrapper.writeReports() " +
                "UnsupportedEncodingException: " + e1.getMessage());
            ste = e1.getStackTrace();
            err = e1.getMessage();
        } catch (FileNotFoundException e2) {
            logger.info("KEWrapper.writeReports() " +
                "FileNotFoundException: " + e2.getMessage());
            ste = e2.getStackTrace();
            err = e2.getMessage();
        } catch (IOException e3) {
            logger.info("KEWrapper.writeReports() IOException: " +
                e3.getMessage());
            ste = e3.getStackTrace();
            err = e3.getMessage();
        } catch (ResultTranslationException e4) {
            logger.info("KEWrapper.writeReports()" +
                " ResultTranslationException: " + e4.getMessage());
            ste = e4.getStackTrace();
            err = e4.getMessage();
        } catch (TranslationException e5) {
            logger.info("KEWrapper.writeReports() " + "TranslationException: " +
                e5.getMessage());
            ste = e5.getStackTrace();
            err = e5.getMessage();
        } catch (KAEResultConversionException e6) {
            logger.info("KEWrapper.writeReports() " +
                "KAEResultConversionException: " + e6.getMessage());
            ste = e6.getStackTrace();
            err = e6.getMessage();
        } catch (SCException e7) {
            logger.info("KEWrapper.writeReports() SCException: " +
                e7.getMessage());
            ste = e7.getStackTrace();
            err = e7.getMessage();
        }

        if (ste != null) {

            for (int i = 0; i < ste.length; i++) {
                logger.info(ste[i].toString());
            }

            throw new WrapperException(err);

        }

        logger.trace("KEWrapper.writeReports() -- EXIT -- ");
    } // writeReports

    /*
     * getXmlResults()
     *
     * Get check-run results from engine in XML
     */
    private String getXmlResults(EngineControllerAPI engine)
        throws EngineException, KAEResultConversionException {
        logger.trace("KEWrapper.getXmlResults() -- ENTER -- ");

        EngineViewAPI view = (EngineViewAPI) engine;
        RunInfoBean runInfo = view.getRunInfo();
        RunResultBean runResult = view.newInstanceOfRunResult();
        runResult.addRunInfo(runInfo);

        KAEResultBean kaeResult = view.newInstanceOfKAEResult();
        kaeResult.addRunResult(runResult);

        // get complete XML output.
        String xml = kaeResult.toXML();

        // logger.trace("KEWrapper.getXmlResults() xml: " + xml);
        logger.trace("KEWrapper.getXmlResults() -- EXIT -- ");

        return xml;
    } // getXmlResults


    /*
     * Write the XML report: no particular formatting
     */
    private void writeXmlFile(String xml, String filename)
        throws UnsupportedEncodingException, FileNotFoundException {
        logger.info("KEWrapper.writeXmlFile() -- ENTER: " + filename);

        PrintWriter xmlout = new PrintWriter(new OutputStreamWriter(
                    new FileOutputStream(filename), "UTF-8"));
        xmlout.print(xml);
        xmlout.close();
        logger.info("KEWrapper.writeXmlFile() -- EXIT-- ");
    } // writeXmlFile


    /*
     * Write the text report: formatting according to custom XSL file
     */
    private void writeTextFile(String xml, String filename) throws IOException,
        FileNotFoundException, TranslationException, ResultTranslationException,
        SCException {
        logger.info("KEWrapper.writeTextFile() -- ENTER: " + filename);

        // minSeverity & brief are globals set from constructor params
        SCXmlToText xsl = new SCXmlToText(genXslDir, numFilteredChecks,
                minSeverity, brief);
        xsl.generateXSLFile(); // throws SCEx

        XmlToTextTranslator xttt = new XmlToTextTranslator(XSL_OUTF_NAME);

        ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes(
                    "UTF-8"));
        BufferedOutputStream textout = new BufferedOutputStream(
                new FileOutputStream(filename));
        xttt.translate(bais, textout);
        textout.close();
        logger.trace("KEWrapper.writeTextFile() -- EXIT-- ");
    } // writeTextFile


    /*
     * getEngine()
     *
     * Instantiate an engine instance based on the passed in
     * configuration of checklist and explorers.
     */
    private EngineControllerAPI getEngine(String checklistFile,
        String explorerPaths[]) throws CheckStorageException, EngineException,
        RemoteException {

        XmlFilenameCheckListDesignator checkListDesignator =
            new XmlFilenameCheckListDesignator(checklistFile);
        checkListDesignator.setValidating(false);

        CheckList cl = checkListDesignator.getCheckList(); // throws REx
        totalChecks = cl.size();
        logger.info("KEWrapper.getEngine(): totalChecks: " + totalChecks);

        logger.trace("KEWrapper.getEngine(): path to facts.zip: " + FACTS_ZIP);

        FactStoreDesignator factStoreDesignator =
            new FilenameFactStoreDesignator(FACTS_ZIP, null);
        // FilenameFactStoreDesignator factStoreDesignator =
        // new FilenameFactStoreDesignator(FACTS_ZIP, null);

        ExplorerDataCollectorDesignator dataCollectorDesignator =
            new ExplorerDataCollectorDesignator();

        for (int i = 0; i < explorerPaths.length; i++) {
            logger.trace("KEWrapper.getEngine(): adding to " +
                "dataCollector: " + explorerPaths[i]);
            dataCollectorDesignator.addToExplorerDirs(explorerPaths[i]);
        }

        EngineFactory factory = EngineFactory.getInstance();
        EngineControllerAPI engine = factory.getEngine(checkListDesignator,
                factStoreDesignator, dataCollectorDesignator, false);

        EngineViewAPI view = (EngineViewAPI) engine;
        String ver = view.getKAEVersion();
        logger.info("KEWrapper.getEngine() KE version: " + ver);

        return engine;
    } // getEngine


    // ===== ===== ===== start EngineListener Interface ===== ===== =====

    /*
     * EngineListener Interface
     *
     * These methods are called by the running engine from its
     * private execution thread.
     * The most interesting is checkFinished() which logs the
     * 'state' of the just completed check.
     * Messages are logged variously to the sccheck logger, the
     * ke logger, or both.
     */

    public void checkListStarted(CheckListStartedEvent ev) {
        kaelogger.log(Level.INFO, "Got CheckListStartedEvent");
        logger.info("KEWrapper.checkListStarted()");
    } // checkListStarted

    public void checkListFinished(CheckListFinishedEvent ev) {
        kaelogger.log(Level.INFO, "Got CheckListFinishedEvent");
        logger.info("KEWrapper.checkListFinished()");
    } // checkListFinished

    public void checkStarted(CheckStartedEvent ev) {
        String id = ev.getCheckId();
        logger.info("KEWrapper.checkStarted(): " + id);

        try {
            currcheck = Integer.parseInt(id);
        } catch (Exception ex) {
            kaelogger.log(Level.WARNING,
                "Error parsing check id in CheckStarted()", ex);
        }

        kaelogger.log(Level.INFO,
            "Got CheckStartedEvent " + "for check: " + id);
    } // checkStarted


    public void checkFinished(CheckFinishedEvent ev) {
        String id = "xx";

        try {
            checksComplete++;
            id = ev.getCheckId();

            EngineViewAPI source = ev.getSourceEngine();

            CheckResults cr = source.getCheckResults(id);
            StringBuffer status = new StringBuffer();
            NestedException nestedEx = null;

            if (cr.isErrored()) {
                status.append("E");
                logger.info("KEWrapper.checkFinished(): " + id + " Errored");
                nestedEx = (NestedException) cr.getException();
                logger.info("KEWrapper.checkFinished() error: " + nestedEx);
                logger.info("KEWrapper.checkFinished() " + "nestedException: " +
                    nestedEx.getAbbreviatedTrace());
            } else if (!cr.isApplicable()) {
                status.append("NA");
                logger.info("KEWrapper.checkFinished(): " + id + " N/A");
            } else if (cr.isConditionPasses()) {
                status.append("P");
                logger.info("KEWrapper.checkFinished(): " + id + " Passed");
            } else {
                status.append("F ");

                int severity = cr.getSeverity().intValue();
                status.append(severity);

                // track maximum severity encountered
                if (severity > maxSeverity) {
                    maxSeverity = severity;
                }

                status.append("\nAnalysis: " +
                    cr.getAnalysisCml().getAsString() + "\n");
                status.append("\nReccommend: " +
                    cr.getRecommendationsCml().getAsString() + "\n");
                logger.info("KEWrapper.checkFinished(): " + id + " Failed");
            }

            kaelogger.log(Level.INFO,
                "Finished Check: " + ev.getCheckId() + " " + status);

            if (cr.isErrored()) {
                kaelogger.log(Level.INFO, "     Check Error: " + nestedEx);
                kaelogger.log(Level.INFO,
                    "     Exception: " + nestedEx.getAbbreviatedTrace());
            }
        } catch (Exception ex) {
            kaelogger.log(Level.INFO, "Error in checkFinished(): ", ex);
            logger.info("KEWrapper.checkFinished() " + "exception on check: " +
                id);
            logger.info(ex.getMessage());
        }
    } // checkFinished

    public void engineError(EngineErrorEvent ev) {
        kaelogger.log(Level.SEVERE, "Got fatal engineError: " + ev);
        logger.info("KEWrapper.engineError(): " +
            ev.getException().getMessage());

        engineErrorFlag = true;
        engineErrorMessage = new String(ev.getException().getMessage());

    } // engineError

    // ===== ===== ===== end EngineListener Interface ===== ===== =====


} // KEWrapper
