/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)SCProperties.java 1.8   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.cluster.sccheck;

import java.io.*;

import java.util.Properties;


/*
 * This Properties class enables a JVM-single Properties object
 * loaded from a named properties file.
 */
public class SCProperties extends Properties {

    private static SCProperties props = null;
    private static String fileName = null;


    // Constructor: create properties from a String filename
    public SCProperties(String s) throws IOException {
        super();

        File source = new File(s);
        InputStream in = new BufferedInputStream(new FileInputStream(
                    source.getPath()));
        load(in);
        in.close();
    } // ()


    // -- create and fetch the Properties object

    public static SCProperties getSCProperties() {
        return props;
    } // getSCProperties()

    public static SCProperties getSCProperties(String s) throws IOException {

        if (props == null) {
            props = new SCProperties(s);
        }

        return props;
    } // getSCProperties()


    // -- fetch typed values from Properties

    public static int intFromProps(String key, int defaultIntVal,
        SCProperties pr) {

        if (pr == null) {
            return defaultIntVal;
        }

        Integer x = new Integer(defaultIntVal);

        try {
            x = Integer.decode(pr.getProperty(key, new Integer(defaultIntVal)
                        .toString()));
        } catch (NumberFormatException e) {
            return defaultIntVal;
        }

        return x.intValue();
    } // intFromProps


    public static long longFromProps(String key, long defaultLongVal,
        SCProperties pr) {

        if (pr == null) {
            return defaultLongVal;
        }

        Long x = new Long(defaultLongVal);

        try {
            x = Long.valueOf(pr.getProperty(key, new Long(defaultLongVal)
                        .toString()));
        } catch (NumberFormatException e) {
            return defaultLongVal;

        }

        return x.longValue();
    } // longFromProps


    public static String stringFromProps(String key, String defaultStringVal,
        SCProperties pr) {

        if (pr == null) {
            return defaultStringVal;
        }

        String s = String.valueOf(pr.getProperty(key,
                    new String(defaultStringVal)));

        return s;
    } // stringFromProps


    public static boolean booleanFromProps(String key, boolean defaultBoolVal,
        SCProperties pr) {

        if (pr == null) {
            return defaultBoolVal;
        }

        Boolean bool = Boolean.valueOf(pr.getProperty(key,
                    new Boolean(defaultBoolVal).toString()));

        return bool.booleanValue();
    } // booleanFromProps

} // SCProperties
