/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)Logger.java 1.10   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.cluster.sccheck;

import java.io.*;

import java.util.*;


/*
 * Utility class for sccheck-execution log files. Creates a single
 * Logger object for the JVM.
 *
 * Logging may be (de)activated globally via the constructor. It is
 * either on or off for the life of the JVM.
 *
 * trace()      - prepends 'trace' to log entries
 * info()       - prepends 'info' to log entries
 * error()      - prepends 'error' to log entries
 * toConsole()  - send entry to console
 * toSyslog()   - send entry to syslog
 *
 */

public class Logger implements Globals {

    private static Logger logger = null; // only one per JVM

    private PrintWriter pw = null;
    private boolean doLogging = false;

    public Logger(String logfilename, boolean doLogging) {

        if (logger == null) {

            try {

                if (doLogging) {
                    FileWriter fw = new FileWriter(logfilename, true); // append
                    BufferedWriter bw = new BufferedWriter(fw);
                    pw = new PrintWriter(bw);

                    Date d = new Date();
                    this.info(d.toString());
                }

                this.logger = this;
                this.doLogging = doLogging;
            } catch (IOException ioex) {
                // silently ignore
            }
        }
    } // ()

    public static synchronized Logger getLogger() {
        return logger;
    } // getLogger

    public synchronized void close() {

        if (pw != null) {
            pw.close();
        }
    } // close

    public synchronized void trace(String msg) {

        if (doLogging) {
            pw.println("trace: " + Thread.currentThread().getName() + ": " +
                msg);
            pw.flush();
        }
    } // trace

    public synchronized void info(String msg) {

        if (doLogging) {
            pw.println("info : " + Thread.currentThread().getName() + ": " +
                msg);
            pw.flush();
        }
    } // info

    public synchronized void error(String msg) {

        if (doLogging) {
            pw.println("error: " + Thread.currentThread().getName() + ": " +
                msg);
            pw.flush();
        }
    } // error

    public synchronized void toConsole(String msg) {

        try {
            FileWriter fw = new FileWriter("/dev/console", true); // append
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            pw.println(msg);
            pw.close();
        } catch (IOException ioex) {
            // silently ignore
        }

        if (doLogging) {
            this.trace(msg);
        }
    } // toConsole

    public synchronized void toSyslog(String priority, String tag, String msg) {

        try {
            Utils.runScript(SYSLOG_CMD + SYSLOG_PRI_FLAG + priority +
                SYSLOG_TAG_FLAG + tag + SYSLOG_MSG_FLAG + "\"" + msg + "\"");

        } catch (WrapperException wex) {

            // attempt to use our own facility
            Logger.getLogger().error("Logger.toSyslog(): " +
                "unable to syslog message: " + msg + ". WrapperException: " +
                wex.getMessage());
        }

    } // toSyslog

} // Logger
