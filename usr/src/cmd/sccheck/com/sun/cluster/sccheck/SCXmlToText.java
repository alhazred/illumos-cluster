/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)SCXmlToText.java 1.8   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.cluster.sccheck;

/*
 * This class creates the XSL file for converting the check run
 * results XML string to text format.
 *
 * In order to support entry of severity filtering information in the
 * output the XSL file is dynamically created based on a static template
 * file.
 *
 * See generateXSLFile().
 *
 * The file will be created in 'outdir.' Template and output filenames
 * are specified in class Globals.
 *
 *                      NO I18N OF ANY STRINGS IN THIS FILE!
 *
 * These strings and data appear in the output reports which are exempt
 * from I18N requirements!
 *
 */

import java.io.*;


public class SCXmlToText implements Globals {

    private com.sun.cluster.sccheck.Logger logger =
        com.sun.cluster.sccheck.Logger.getLogger();

    private String outDir = null;
    private int numFilteredChecks = 0;
    private int minSeverity = 0;
    private boolean brief;

    // constructor

    public SCXmlToText(String outDir, int numFilteredChecks, int minSeverity,
        boolean brief) {
        this.outDir = outDir;
        this.minSeverity = minSeverity;
        this.brief = brief;
        this.numFilteredChecks = numFilteredChecks;
        logger.info("SCXmlToText() outDir: " + outDir);
        logger.info("SCXmlToText() minSeverity: " + minSeverity + "; brief: " +
            brief);
        logger.info("SCXmlToText() numFilteredChecks: " + numFilteredChecks);
    } // ()


    /*
     * generateXSLFile()
     *
     * In order to support entry of severity filtering information in the
     * output the XSL file is dynamically created based on a static template
     * file.
     *
     * The template file has entries like
     *      <!-- scheck section marker 0 -->
     *
     * The template file is reproduced as is up to but not including a
     * section marker. The section marker is replaced by static strings and
     * dynamic data.
     */

    public void generateXSLFile() throws SCException {
        logger.trace("SCXmlToText.generateXSLFile() -- ENTER --");

        StringBuffer sections[] = new StringBuffer[5];
        // fits sections 0 through 4

        StackTraceElement ste[] = null;

        /*
         * read each section of the template into an array
         */
        try {
            FileReader fr = new FileReader(XSL_TMPL_NAME);
            logger.trace("SCXmlToText.generateXSLFile() reading: " +
                XSL_TMPL_NAME);

            BufferedReader br = new BufferedReader(fr);
            String s = "";

            for (int i = 0; i < sections.length; i++) {
                sections[i] = new StringBuffer("");
            }

            int index = 0;

            do {
                s = br.readLine();

                if (s != null) {
                    sections[index].append(s + "\n");

                    if (s.startsWith(XSL_DELIM)) {
                        index++;
                    }
                }
            } while (s != null);

        } catch (FileNotFoundException fnfex) {
            logger.info("SCXmlToText: fnfex: " + fnfex.getMessage());
            throw new SCException(fnfex.getMessage());
        } catch (IOException ioex) {
            logger.info("SCXmlToText: ioex: " + ioex.getMessage());
            throw new SCException(ioex.getMessage());
        }

        /*
         * print each section from the array to ouput file, inserting
         * appropriate dynamic info between sections
         */
        try {
            logger.trace("SCXmlToText.generateXSLFile() writing: " + outDir +
                XSL_OUTF_NAME);

            FileWriter fw = new FileWriter(outDir + XSL_OUTF_NAME); // overwrite
            PrintWriter pw = new PrintWriter(fw, true); // autoflush

            // note: each sections[] has an ending \n already
            pw.print(sections[0]);

            if (minSeverity > 0) {
                pw.println("Severity Filter Allowing      : " +
                    SEVERITY_STRINGS[minSeverity] + " and greater.");
            }

            pw.print(sections[1]);

            if (minSeverity > 0) {
                pw.println("Filtered : " + numFilteredChecks);
            }

            pw.print(sections[2]);

            if (!brief) {
                pw.print(sections[3]);
            }

            pw.print(sections[4]);

            pw.close();
        } catch (IOException ioex2) {
            logger.info("SCXmlToText: ioex2: " + ioex2.getMessage());

            /*
             * ste = ioex2.getStackTrace();
             * if (ste != null) {
             *      for (int i = 0; i < ste.length; i++) {
             *              logger.info(ste[i].toString());
             *      }
             * }
             */
            throw new SCException(ioex2.getMessage());
        }

        logger.trace("SCXmlToText.generateXSLFile() " + "-- EXIT --");

    } // generateXSLFile

} // SCXmlToText
