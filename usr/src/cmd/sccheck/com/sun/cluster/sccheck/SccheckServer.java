/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * ident        "@(#)SccheckServer.java 1.16     08/05/20 SMI"
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.cluster.sccheck;

import java.io.*;

import java.net.*;

import java.util.*;


/*
 * This class is the main java server:
 *
 * SccheckServer is initially invoked by sccheckd.ksh via inetd.
 * It performs various initializations then sits waiting for a socket
 * connection.
 *
 * The client then makes a socket connection directly to this server.
 *
 * See remarks above mainServer() for instituting the server side of a
 * session.
 *
 * Exit codes and messages are not useful since SccheckServer is launched
 * under inetd. Error messages are logged.
 */

public class SccheckServer implements Server, Globals {

    private static boolean busy = false;
    private String localeLang = null;
    private String localeCountry = null;
    private String localeVariant = null;
    private String auth = null;
    private String explorerVer = null;
    private ServerProtocol sprot = null;
    private String privateSubnet = ""; // via defines
    private String privateNetMask = ""; // via defines
    private String clustername = ""; // via defines
    private boolean inClusterMode = false; // via defines
    private Session sessionInProgress = null;
    private ServerThread serverThread = null;

    private Logger logger = null;
    private SCProperties props = null;

    /*
     * Constructor
     *
     * Most values passed from ksh via java defines.
     * A few additional values passed via command-line args: sent
     * only for logging
     *
     * Perform some initializations then call mainServer().
     */

    public SccheckServer(String args[]) {
        String errorMsg = null;

        /*
         * load properties file
         * begin logging
         */
        try {
            props = SCProperties.getSCProperties(PROPSFILE);
        } catch (IOException e) {
            /*
             * not fatal; queries to SCProperties will return
             * default if props == null
             */
        }

        boolean doLogging = SCProperties.booleanFromProps(PROP_SC_LOGGING, true,
                props);
        logger = new Logger(SERVERLOG, doLogging);
        logger.trace("SccheckServer() -- ENTER -- ");

        if (!doLogging) {

            /*
             * override Knowlede Engine property for "kae" logging
             * kae: Knowledge Automation Engine
             */
            System.setProperty(PROP_KE_SERVERLOG, DEVNULL);
        }

        for (int i = 0; i < args.length; i++) {
            logger.info("SccheckServer(): " + args[i]);
        }

        /*
         * validate defines:
         *
         * if any required defines are missing exit
         */
        try {
            checkRequiredDefines();
        } catch (SCException scex) {
            logger.toSyslog(SYSLOG_PRI_ERROR, SYSLOG_TAG, scex.getMessage());
            serverExit();
        }

        // time to start acting like a server
        mainServer();
    } // ()

    /*
     * mainServer()
     *
     * Create the ServerSocket and start the wait for connection loop.
     *
     * Upon connection:
     *      create a ServerProtocol object to handle communication with
     *              the Client
     *      negotiate for a Session version
     *      perform Session initializations
     *      perform client validations: network, root user
     *      if a ServerThread already active then
     *              pass new ServerProtocol to it
     *      else
     *              create and launch a ServerThread
     *      return to waiting for Client connection
     */
    private void mainServer() {

        /*
         * Start the ServerSocket and wait for a Client connection
         */
        int serverPort = SCProperties.intFromProps(PROP_SC_SERVERPORT,
                SERVERPORT, props);
        ServerSocket ssock = null;
        sprot = null;

        try {
            logger.info("**  SccheckServer.mainServer() " +
                "making ServerSocket on port " + serverPort);
            ssock = new ServerSocket(serverPort);
        } catch (IOException ioex1) {
            logger.error("SccheckServer.mainServer() " +
                "in IOException ioex1: " + ioex1.getMessage());
            logger.toSyslog(SYSLOG_PRI_ERROR, SYSLOG_TAG,
                "sccheckd: Unable to create " + " ServerSocket; exiting.");
            serverExit();
            // typically because trying to run a second instance
            // port in use
        }

        /*
         * Start the infinite wait-for-connection loop.
         *
         * this loop will be broken when ServerThread calls serverExit()
         * at its own completion.
         */
        for (;;) {
            Socket socket = null;
            logger.trace("--------------------------------");
            logger.trace(" ***  SccheckServer.mainServer() " + "waiting...");

            try {
                socket = ssock.accept();

                // now have a client on my socket
                logger.trace(" **   SccheckServer.mainServer() " +
                    "have a client...");
                logger.trace("");
                logger.trace("SccheckServer.mainServer() " +
                    "about to make SProt");

                /*
                 * Create a ServerProtocol so we can chat with
                 * Client, negotiate a Session version & create
                 * a Session object call Session.initServer()
                 * to get startup information
                 */
                sprot = new ServerProtocol(this, socket);

                int version = sprot.getCommonVersion(VERSION);
                logger.info("SccheckServer.mainServer() " + "common version: " +
                    version);

                Session session = SessionFactory.getSession(version); // throws
                                                                      // SCEx
                logger.info("SccheckServer.mainServer() " + "made session: " +
                    SESSIONCLASS + version);

                logger.trace("SccheckServer.mainServer() " +
                    "initializing SessionV" + version);

                // future Session version must add a new case
                switch (version) {

                case 1:
                case 2:
                    session.initServer(this, sprot, inClusterMode);

                    break;

                default:

                    String errorMsg = "sccheckd: " + "Unsupported Session " +
                        "version: " + version;
                    logger.toSyslog(SYSLOG_PRI_ERROR, SYSLOG_TAG, errorMsg);
                    conditionalServerExit(); // fatal

                    break;
                } // switch

                logger.trace("SccheckServer.mainServer() " +
                    "did session.initServer() ");


                /*
                 * Calculate some client network information.
                 * Set locale to client locale.
                 */
                InetAddress localAddr = getLocalAddr(socket);
                int port = getRemotePort(socket);

                // permission to proceed?
                logger.trace("SccheckServer.mainServer() " +
                    "validating request for session");

                Locale userLocale = new Locale(localeLang, localeCountry,
                        localeVariant);
                Locale.setDefault(userLocale);
                logger.info("SccheckServer.mainServer() " + "set userLocale: " +
                    userLocale.getDisplayName());

                /*
                 * Decide if client is authorized to continue.
                 */
                try {
                    testPermission(auth, privateSubnet, privateNetMask,
                        localAddr, port);
                    testExplorerVer();
                } catch (SCException pex1) {

                    // no I18N
                    logger.error("SccheckServer.mainServer() " +
                        "permission red light: " + "ProtocolException pex1: " +
                        pex1.getMessage());
                    sprot.sendErrPerm(pex1.getMessage());
                    conditionalServerExit(); // fatal
                }

                logger.trace("SccheckServer.mainServer() " +
                    "permission green light; sending permOK");
                sprot.sendPermOK(); // throws IOEx


                /*
                 * Prepare to run a server session with
                 * this client:
                 *
                 * if a ServerThread already exists then
                 *      hand it this ServerProtocol
                 *      object to add to its list of clients
                 *      to talk to; ask it to "catch-up"
                 * else
                 *      start up a ServerThread and start
                 *      some work
                 */
                if (isBusy()) {
                    // join sessionInProgress & serverThread
                    // send ready files in this main thread

                    // no I18N
                    logger.trace("SccheckServer.mainServer() " +
                        "joining session in progress");
                    sessionInProgress.joinSession(sprot);

                    // send progress and future files
                    // in ServerThread
                    serverThread.addServerProtocol(sprot);

                } else {
                    setBusy(true);

                    // will be unset by ServerThread on exit
                    logger.trace("SccheckServer.mainServer() " +
                        "starting new session");
                    sessionInProgress = session;
                    startService(session, version, sprot);
                    // spawns worker thread; no exceptions
                }
            } catch (IOException ioex2) {
                logger.error("SccheckServer.mainServer() " +
                    "in IOException ioex2: " + ioex2.getMessage());
                logger.toSyslog(SYSLOG_PRI_ERROR, SYSLOG_TAG,
                    "sccheckd: Unable to accept socket " +
                    "connection: exiting.");
                conditionalServerExit(); // fatal
            } catch (ProtocolException pex) {
                logger.error("SccheckServer.mainServer() " +
                    "in ProtocolException pex: " + pex.getMessage());
                logger.toSyslog(SYSLOG_PRI_ERROR, SYSLOG_TAG,
                    "sccheckd: Session init failed (pex).");
                conditionalServerExit(); // fatal
            } catch (SCException scex) {
                logger.error("SccheckServer.mainServer() " +
                    "in SCException scex: " + scex.getMessage());
                logger.toSyslog(SYSLOG_PRI_ERROR, SYSLOG_TAG,
                    "sccheckd: Session init failed (scex).");
                conditionalServerExit(); // fatal
            }
        } // for (;;)
    } // mainServer


    /*
     * startService()
     *
     * Create and start a ServerThread with the just created
     * ServerProtocol and Session objects.
     *
     * This method returns immediately after calling serverThread.start().
     */
    private void startService(Session session, int version,
        ServerProtocol sprot) {
        logger.trace("SccheckServer.startService() -- ENTER-- ");

        serverThread = new ServerThread(this, session, version, sprot);
        serverThread.start();

        logger.trace("SccheckServer.startService() -- EXIT-- ");
    } // startService


    /*
     * checkRequiredDefines()
     *
     * Make sure that various defines required by sccheck and KE
     *      have been provided by scccheck.ksh.
     * Missing properties are fatal: throw exception.
     *
     * Load sccheck required defines into class members.
     *
     */
    private void checkRequiredDefines() throws SCException {
        String status = null;
        Vector reqProps = new Vector();

        // required by sccheckd
        reqProps.addElement(PROP_SCK_CLMODE);
        reqProps.addElement(PROP_SCK_CLNAME);
        reqProps.addElement(PROP_SCK_PRIVNET);
        reqProps.addElement(PROP_SCK_PRIVNETMASK);
        reqProps.addElement(PROP_SCK_EXPLVER);

        reqProps.addElement(PROP_KE_SERVERLOG);
        reqProps.addElement(PROP_KE_SERVER_XSLDIR);

        // walk the list and verify that each named define has a value
        String requiredProp;
        String systemProp;
        Enumeration e = reqProps.elements();

        while (e.hasMoreElements()) {

            requiredProp = (String) e.nextElement();
            systemProp = System.getProperty(requiredProp);

            if ((systemProp == null) || (systemProp.length() == 0)) {

                if (status == null) {
                    status = requiredProp;
                } else {
                    status = status + " " + requiredProp; // no I18n
                }
            }
        } // while

        if (status != null) {
            Object i18nArgs[] = { status };
            String i18nMsg = I18n.getLocalized("requiredDefinesMissing",
                    i18nArgs);
            throw new SCException(i18nMsg);
        }

        clustername = System.getProperty(PROP_SCK_CLNAME);
        privateSubnet = System.getProperty(PROP_SCK_PRIVNET);
        privateNetMask = System.getProperty(PROP_SCK_PRIVNETMASK);

        inClusterMode = Boolean.getBoolean(PROP_SCK_CLMODE);
        logger.info("SccheckServer.checkRequiredDefines() " +
            "inClusterMode: " + inClusterMode);

        explorerVer = System.getProperty(PROP_SCK_EXPLVER);
        logger.info("SccheckServer.checkRequiredDefines() " + "explorerVer: " +
            explorerVer);
    } // checkRequiredDefines


    // -----   start interface Server  -----

    /*
     * These set methods are called by ServerProtocol
     */
    public void setLocaleLang(String s) {
        localeLang = s;
    } // setLocale

    public void setLocaleCountry(String s) {
        localeCountry = s;
    } // setLocale

    public void setLocaleVariant(String s) {
        localeVariant = s;
    } // setLocale

    public void setAuth(String s) {
        auth = s;
    } // setAuth


    // -----   end interface Server    -----


    // -----  helper methods -----

    /*
     * setBusy()
     *
     * Set the flag as indicated.
     *
     * Marks that a ServerThread has been instantiated and is running.
     */
    public void setBusy(boolean b) {
        logger.info(" ***  SccheckServer.setBusy(): " + b);
        busy = b;
    }


    /*
     * conditionalServerExit()
     *
     * Exit only if there's no active ServerThread.
     *
     * If called by a second-in client there will be no exit.
     */
    private void conditionalServerExit() {

        if (!isBusy()) {
            serverExit();
        }
    } // conditionalServerExit


    /*
     * serverExit()
     *
     * The end: exit the JVM.
     */
    public void serverExit() {
        logger.trace(" ***  SccheckServer.serverExit()");
        logger.close();
        System.exit(0);
    } // serverExit


    /*
     * testPermission()
     *
     * Accept an authorization string and network information
     * and evaluate them for acceptability.
     *
     * Throws SCException if unacceptable.
     */
    private void testPermission(String auth, String privateSubnet,
        String privateNetMask, InetAddress localAddr, int remotePort)
        throws SCException {
        logger.trace("SccheckServer.testPermission() -- ENTER -- ");

        String i18nMsg = "";

        // test authStr
        if (!processAuthStr(auth)) {
            logger.error("SccheckServer.testPermission() " +
                "authentication failure");
            i18nMsg = I18n.getLocalized("authenticationFailure");
            throw new SCException(i18nMsg);
        } else {
            logger.info("SccheckServer.testPermission() " +
                "authentication succeeded");
        }

        // test originating port & net address
        if (!isRemotePortOK(remotePort)) {
            logger.error("SccheckServer.testPermission() " +
                "not 'root port' from client");
            i18nMsg = I18n.getLocalized("mustConnectViaPrivilegedPort");
            throw new SCException(i18nMsg);
        } else {
            logger.info("SccheckServer.testPermission() " +
                "remote port is privileged");
        }

        if (!isNetAddressOK(privateSubnet, privateNetMask, localAddr)) {
            logger.error("SccheckServer.testPermission() " +
                "not private net and not localhost");
            i18nMsg = I18n.getLocalized(
                    "mustConnectViaPrivateNetworkOrLocalhost");
            throw new SCException(i18nMsg);
        } else {
            logger.info("SccheckServer.testPermission() " +
                "network connection granted");
        }

        logger.trace("SccheckServer.testPermission() -- EXIT -- ");
    } // testPermission


    /*
     * isNetAddressOK()
     *
     * Evaluate the address the client used to connect to the
     * server:
     *
     * If clustermode then
     *      address must be on private subnet.
     * else
     *      address must be 'localhost.'
     */
    private boolean isNetAddressOK(String privateSubnet, String privateNetMask,
        InetAddress localAddr) {
        logger.trace("SccheckServer.isNetAddressOK() -- ENTER-- ");

        boolean isOK = false;

        if (inClusterMode) {
            isOK = isPrivateNet(privateSubnet, privateNetMask, localAddr);
        } else {
            isOK = isLocalhost(localAddr);
        }

        logger.trace("SccheckServer.isNetAddressOK() -- EXIT-- " + isOK);

        return isOK;
    } // isNetAddressOK


    /*
     * isRemotePortOK()
     *
     * Evaluate client's outgoing port: if it's in the privileged
     * root range it's OK.
     */
    private boolean isRemotePortOK(int remPort) {
        logger.trace("SccheckServer.isRemotePortOK() -- ENTER: " + remPort);

        boolean isOK = false;

        if ((remPort <= MAXOUTPORT) && (remPort >= MINOUTPORT)) {
            isOK = true;
        } else {
            isOK = false;
        }

        logger.trace("SccheckServer.isRemotePortOK() -- EXIT--: " + isOK);

        return isOK;
    } // isRemotePortOK


    /*
     * isPrivateNet()
     *
     * Returns true if the localAddress is on the private subnet.
     */
    private boolean isPrivateNet(String privateSubnet, String privateNetMask,
        InetAddress localAddr) {
        logger.trace("SccheckServer.isPrivateNet() -- ENTER --");

        boolean isPrivate = false;

        logger.info("SccheckServer.isPrivateNet() privateSubnet: " +
            privateSubnet);
        logger.info("SccheckServer.isPrivateNet() privateNetMask: " +
            privateNetMask);
        logger.info("SccheckServer.isPrivateNet() localAddr: " +
            localAddr.toString());

        /*
         * convert String representations of dotted quad IPv4
         * addresses to integer
         */

        // convert host InetAddress to integer
        int hostVal = Utils.inetAddressToInt(localAddr);
        logger.info("SccheckServer.isPrivateNet() hostVal: " + hostVal);

        int maskVal = Utils.dottedQuadToInt(privateNetMask);
        logger.info("SccheckServer.isPrivateNet() maskVal: " + maskVal);

        int privateSubnetVal = Utils.dottedQuadToInt(privateSubnet);
        logger.info("SccheckServer.isPrivateNet() privateSubnetVal: " +
            privateSubnetVal);

        // apply the subnetmask to the host address
        int hostSubnet = maskVal & hostVal;
        logger.info("SccheckServer.isPrivateNet() hostSubnet: " + hostSubnet);

        // same as the subnet?
        if (hostSubnet == privateSubnetVal) {
            isPrivate = true;
        }

        logger.trace("SccheckServer.isPrivateNet() -- EXIT --: " + isPrivate);

        return isPrivate;
    } // isPrivateNet


    /*
     * isLocalhost()
     *
     * Returns true if the address is that of 'localhost'
     */
    private boolean isLocalhost(InetAddress localAddr) {
        boolean isLocal = false;
        logger.trace("SccheckServer.isLocalhost() localAddr: " +
            localAddr.toString());

        // jdk 1.1.1 returns "localhost/127.0.0.1"
        // jdk 1.4.x returns "/127.0.0.1"
        if (localAddr.toString().endsWith("/127.0.0.1")) { // no I18n
            isLocal = true;
        }

        logger.trace("SccheckServer.isLocalhost(): " + isLocal);

        return isLocal;
    } // isLocalhost


    /*
     * getLocalAddr()
     *
     * Return server's InetAddress on the socket.
     *
     * This will indicate the subnet the client is connecting
     * to server on.
     */
    private InetAddress getLocalAddr(Socket sock) {
        InetAddress ia = sock.getLocalAddress();
        logger.info("SccheckServer.getLocalAddr(): " + ia.toString());

        return ia;
    } // getLocalAddr


    /*
     * getRemotePort()
     *
     * Return the outgoing port number used by the client at its
     * end.
     *
     * (Port numbers less than 1024 can only be used by root user.)
     *
     * This is not the port number the client connected to here on
     * the server.
     */
    private int getRemotePort(Socket sock) {
        int port = sock.getPort();
        logger.info("SccheckServer.getRemotePort(): " + port);

        return port;
    } // getRemotePort


    /*
     * processAuthStr()
     *
     * Placeholder method for additional security authorization.
     *
     * Given an acceptable token this server might be allowed to
     * accept connections on other than the private subnet and the
     * heuristic of "is private net, therefore is OK" can be strengthened.
     *
     */
    private boolean processAuthStr(String s) {
        boolean authorized;

        if (s.equals("")) { // no I18n
            authorized = true;
        } else {
            authorized = true;
        }

        return authorized;
    } // processAuthStr


    /*
     * testExplorerVer()
     *
     * Check whether explorer version passed in from the ksh wrapper
     * is undetermined.
     *
     * Throw an exception if the explorer config file is not present.
     *
     * Compare the explorer version passed in from the ksh wrapper
     * against the defined minimum supported version.
     *
     * Throw an exception if the passed in version is less than the
     * supported version.
     */
    private void testExplorerVer() throws SCException {
        logger.trace("SccheckServer.testExplorerVer() -- ENTER -- " +
            explorerVer + " vs. " + EXPLORER_MIN_VER);

        if (explorerVer.equals(EXPLORER_VERSION_UNDEF)) {
            String i18nMsg = "";
            logger.error("SccheckServer.testExplorerVer()" +
                " explorer configuration file not present");
            i18nMsg = I18n.getLocalized("explorerConfigurationFailure");
            throw new SCException(i18nMsg);
        }

        StringTokenizer st = new StringTokenizer(explorerVer, ",");
        String testVal = st.nextToken();

        int testValFields[] = { 0, 0, 0 };

        st = new StringTokenizer(explorerVer, ".");

        for (int i = 0; i < 3; i++) {

            try {
                testValFields[i] = fieldToInt(st.nextToken());
            } catch (NoSuchElementException e) {
                // use the 0
            }
        }

        int minValFields[] = { 0, 0, 0 };

        st = new StringTokenizer(EXPLORER_MIN_VER, ".");

        for (int i = 0; i < 3; i++) {

            try {
                minValFields[i] = fieldToInt(st.nextToken());
            } catch (NoSuchElementException e) {
                // just keep the 0
            }
        }

        if (compareIntArrays(testValFields, minValFields) < 0) {
            String i18nMsg = "";
            Object i18nArgs[] = { explorerVer, EXPLORER_MIN_VER };
            logger.error("SccheckServer.testExplorerVer() " +
                "explorer version too old.");
            i18nMsg = I18n.getLocalized("explorerVersionFailure", i18nArgs);
            throw new SCException(i18nMsg);
        }

        logger.trace("SccheckServer.testExplorerVer() -- EXIT -- ");
    } // testExplorerVer


    /*
     * compareIntArrays()
     *
     * Specialized compare method that accepts two
     * 3-element int arrays and compares them field by field.
     * If either value at any index is different return a difference
     * indicator.
     *
     * returns -1 if a1 is "less than" a2
     * returns 0  if each element of a1 equals the corresponding elt in a2
     * returns 1 if a1 is "greater than" a2
     *
     * example: {1,99,99}, {2,0,0} returns -1
     * example: {2,2,2},   {2,2,2} returns 0
     * example: {3,0,1},   {3,0,0} returns 1
     */
    private int compareIntArrays(int a1[], int a2[]) {
        int retval = 0;

        for (int i = 0; i < 3; i++) {

            if (a1[i] < a2[i]) {
                retval = -1;

                break;
            } else if (a1[i] > a2[i]) {
                retval = 1;

                break;
            }
        }

        return retval;
    } // compareIntArrays


    /*
     * fieldToInt()
     *
     * Specialized string to int converter that will
     * return 0 rather than an exception if unable to
     * convert the string.
     */
    private int fieldToInt(String field) {
        int i = 0;

        try {
            i = Integer.parseInt(field);
        } catch (NumberFormatException nfe) {
            // don't bomb just for this
            // return 0
        }

        return i;
    } // fieldToInt


    public boolean isBusy() {
        return busy;
    } // isBusy


    // -----  end helper methods -----


    // #####################   MAIN   #############################

    // invoked by sccheckd.ksh

    public static void main(String args[]) {
        SccheckServer sccheckserver = new SccheckServer(args);
    } // main


} // SccheckServer
