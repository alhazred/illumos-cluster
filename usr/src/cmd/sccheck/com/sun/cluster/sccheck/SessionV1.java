/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * ident        "@(#)SessionV1.java 1.17     08/05/20 SMI"
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.cluster.sccheck;

import java.io.*;

import java.util.*;


/*
 * SessionV1 implements the Session interface.
 *
 * This class implements version 1 of the four main methods for conducting
 * the client-server session:
 *
 *      initClient()
 *      runClient()
 *      initServer()
 *      runServer()
 *
 * and joinSession() which may be called by the server.
 *
 */
public class SessionV1 implements Session, Globals {

    private ClientProtocol clientProt = null;
    private String publicName = null;
    private String privateName = null;
    private String resultsFilenameDest = null;
    private String reportsFilenameBaseDest = null;
    private Client clientParent = null;
    private ProgressListener progressListener = null;

    private Vector serverProts = null;

    private Server serverParent = null;

    private String explorergzip = null;
    private String explorerDirA[] = null;
    private String report1 = null;
    private String report2 = null;

    private boolean clusterMode = false;
    private boolean brief = false;
    private int minSeverity = 0;

    private Logger logger = Logger.getLogger();


    /*
     * Constructor
     */
    public SessionV1() {
        serverProts = new Vector();
    } // ()


    // -----   start Session interface -----


    /*
     * initClient()
     *
     * Called by ClientThread to perform initial communications
     * and prepare for a session.
     * Corresponds to initServer():
     *
     *      pass the publicName of the server
     *      send locale info
     *      send authorization string
     *      send run params:
     *              brief report format desired?
     *              filter reporta on some minimum severity?
     *      no additionalData in V1
     *      read permission to continue from server
     *
     */


    // Original interface
    public void initClient(Client c, ClientProtocol cp, String pbn, String pvn,
        String results, String reports, boolean brief, int minSeverity)
        throws ProtocolException, SCException {
        this.initClient(c, cp, pbn, pvn, results, reports, brief, minSeverity,
            null);
    } // initClient

    // Expanded interface
    public void initClient(Client c, ClientProtocol cp, String pbn, String pvn,
        String results, String reports, boolean brief, int minSeverity,
        Object additionalData[]) throws ProtocolException, SCException {
        clientParent = c;
        clientProt = cp;
        publicName = pbn;
        privateName = pvn;
        resultsFilenameDest = results;
        reportsFilenameBaseDest = reports;
        this.brief = brief;
        this.minSeverity = minSeverity;

        try {
            cp.sendString(publicName);
            cp.sendLocale();
            cp.sendAuth();
            cp.sendBoolean(brief);
            cp.sendInt(minSeverity);


            if (cp.getPermission() != true) {
                Object i18nArgs[] = { cp.readMessage() };
                String i18nMsg = I18n.getLocalized("sessionPermissionDenied",
                        i18nArgs);
                throw new SCException(i18nMsg);
            }


        } catch (IOException ioex) {
            logger.error("SessionV1.initClient() IOException: " +
                ioex.getMessage());
            throw new ProtocolException(ioex.getMessage());
        }
    } // initClient


    /*
     * initServer()
     *
     * Called by SccheckServer to perform initial communications
     * and prepare for a session.
     * Corresponds to initClient():
     *
     *      add the new ServerProtocol object to the list
     *      read my publicName
     *      read locale and authorization strings
     *      read user-specified params:
     *              brief report format?
     *              filter report on minimum severity?
     *
     */
    public void initServer(Server s, ServerProtocol sp, boolean clmode)
        throws ProtocolException {
        logger.trace("SessionV1.initServer() -- ENTER -- ");
        serverParent = s;
        clusterMode = clmode;
        logger.info("SessionV1.initServer() clusterMode: " + clusterMode);

        serverProts.addElement(new ServerProtocolState(sp));

        try {
            publicName = sp.readString();
            sp.readLocale();
            sp.readAuthStr();
            brief = sp.readBoolean();
            minSeverity = sp.readInt();

        } catch (IOException ioex) {
            throw new ProtocolException(ioex.getMessage());
        }

        logger.trace("SessionV1.initServer() -- EXIT -- ");
    } // initServer

    /*
     * runClient()
     *
     * Called by ClientThread to run the client-side of the session.
     * Corresponds to runServer():
     *      if the user did not specify a explorer archive
     *      Pass the results file name to the Client.
     *      Wait for then receive two reports files, accepting progress
     *        strings and error messages before the file receive starts
     *      Read the greatest severity encountered by the server and pass
     *        it to the Client.
     *      Read the generation date of the checklist used by the server
     *        and pass it to the Client.
     */
    public void runClient(ProgressListener pl) throws ProtocolException,
        SCException {

        String i18nMsg;

        logger.trace("SessionV1.runClient() -- ENTER -- ");
        progressListener = pl;

        logger.trace("SessionV1.runClient() sending run vars");


        Object i18nArgs[] = { publicName };

        // get explorer results from remote
        logger.trace("SessionV1.runClient() " +
            "starting Explorer wait loop for " + publicName);
        getRemoteFile(publicName, clientProt, resultsFilenameDest); // throws
                                                                    // SCEx
        clientParent.addZippedExplorer(resultsFilenameDest);


        i18nMsg = I18n.getLocalized("explorerFinished", i18nArgs);
        progressListener.postProgress(VERBOSE, i18nMsg);


        i18nMsg = I18n.getLocalized("startingSingleNodeChecks", i18nArgs);
        progressListener.postProgress(VERBOSE, i18nMsg);

        // get KE report from remote
        logger.trace("SessionV1.runClient() " + "starting KE wait loop for " +
            publicName);
        getRemoteFile(publicName, clientProt,
            reportsFilenameBaseDest + SFX_XML); // throws SCEx
        getRemoteFile(publicName, clientProt,
            reportsFilenameBaseDest + SFX_TXT); // throws SCEx

        try {
            int maxSeverity = clientProt.readInt();
            logger.info("SessionV1.runClient() maxSeverity: " + maxSeverity);
            clientParent.addServerExitCode(publicName, maxSeverity);

            String genDate = clientProt.readString();
            logger.info("SessionV1.runClient() genDate: " + genDate);
            clientParent.addGenDate(publicName, genDate);
        } catch (IOException ioex) {
            logger.error("SessionV1.runClient() IOException: " +
                ioex.getMessage());
            throw new ProtocolException(ioex.getMessage());
        } // try

        i18nMsg = I18n.getLocalized("singleNodeChecksFinished", i18nArgs);
        progressListener.postProgress(VERBOSE, i18nMsg);

        logger.trace("SessionV1.runClient() -- EXIT -- ");

    } // runClient

    /*
     * runServer()
     *
     * Called by ServerThread to run the server-side of the session.
     * Corresponds to runClient():
     *      execute Explorer:
     *              invoke Explorer
     *              send its progress strings to clients
     *              send the compressed results file to clients
     *      execute the KnowledgeEngine:
     *              send its progress strings to clients
     *              send the two produced reports to the clients
     */
    public void runServer(ProgressListener pl) throws ProtocolException,
        WrapperException {

        logger.trace("SessionV1.runServer() -- ENTER -- ");
        progressListener = pl;

        // run Explorer; deliver copy of gzip to client

        boolean okToContinue = doExplorer();

        // run KE; deliver copies of txt & xml reports to client
        if (okToContinue) {
            doKE(brief, minSeverity);
        }

        logger.trace("SessionV1.serverRun() -- EXIT -- ");
    } // runServer


    /*
     * joinSession()
     *
     * Called by SccheckServer to add an additional ServerProtocol object
     * (representing a new client) to the session in progress.
     * This method runs on the main thread, not the ServerThread.
     *
     *      Wrap the ServerProtocol in a ServerProtocolState (inner class)
     *        object.
     *      Play catch-up with new client: send any files that have already
     *        been sent to other clients this session
     */
    public void joinSession(ServerProtocol sp) throws ProtocolException {
        logger.trace("SessionV1.joinSession() -- ENTER -- sprot" + sp.getID());

        // use sp directly in order to send only to new client
        // progressListener.postProgress() [ServerThread]
        // sends to all active clients
        try {
            String i18nMsg = I18n.getLocalized("joiningExistingSession");
            sp.sendProgress(i18nMsg);
        } catch (IOException ioex) {
            throw new ProtocolException(ioex.getMessage());
        }

        // add to vector of SPState objects
        // catchup with files ready to send

        serverProts.addElement(new ServerProtocolState(sp));

        try {
            serverSendFiles();
        } catch (ProtocolException pex) {

            /*
             * All clients, including this new one have gone away.
             * Must let the server know that nobody's left
             * so the conditionalServerExit() will actually exit
             * when we toss pex on up.
             */
            serverParent.setBusy(false);
            throw pex;
        } // try

        logger.trace("SessionV1.joinSession() -- EXIT -- sprot" + sp.getID());
    } // joinSession

    // -----   end Session interface   -----


    /*
     * doExplorer() [Server method]
     *
     * Wrap the Explorer execution and compressed results file transfer to
     * client.
     * Obtain and save the name of the unpacked results from the run.
     *
     * Return true if completed OK.
     * Return false if sent exit error to all clients but didn't
     * throw exceptions.
     */
    private boolean doExplorer() throws ProtocolException, WrapperException {

        logger.trace("SessionV1.doExplorer() -- ENTER -- ");

        boolean okToContinue = true;
        ExplorerWrapper ew = new ExplorerWrapper(progressListener, null);

        // invoke Explorer and deliver gzip to client
        try {

            // run explorer & get results gzip name
            explorergzip = ew.runExplorer();

            // results left unpacked by explorer run
            explorerDirA = new String[1];
            explorerDirA[0] = ew.getExplorerDir();
            logger.info("SessionV1.doExplorer() explorerDirA: " +
                Utils.dumpStringArray(explorerDirA));

            if ((explorergzip != null) && (explorerDirA[0] != null)) {

                /*
                 * Exceptions from serverSendFiles()
                 * and sendExecErrExit() only if all
                 * clients are gone.
                 * Results in serverExit()
                 * from ServerThread.run().
                 */
                serverSendFiles(); // throws PEx
            } else {
                sendExecErrExit(); // throws IOEx
                okToContinue = false;
            }
        } catch (IOException ioex) {
            logger.error("ServerThread.doExplorer() IOException: " +
                ioex.getMessage());
            throw new ProtocolException(ioex.getMessage());
        } catch (WrapperException kewex) {
            Object i18nArgs[] = { kewex.getMessage() };
            String i18nMsg = I18n.getLocalized("explorerRunFailed", i18nArgs);
            throw new WrapperException(i18nMsg);
        }

        logger.trace("SessionV1.doExplorer() -- EXIT -- " + okToContinue);

        return okToContinue;
    } // doExplorer


    /*
     * doKE() [Server method]
     *
     * Wrap the KnowledgEngine execution and reports file transfers to
     * client:
     *
     * Call runKE() to create a KEWrapper and execute the KE through it.
     * Accept the KEWrapper object itself back to enable fetching auxillary
     * data from the run.
     *
     * Allow sending first report to all clients before sending
     * second report.
     *
     * Send to each client maximum severity encountered among all
     * failed checks.
     * Send generation date from checklist used.
     */
    private void doKE(boolean brief, int minSeverity) throws ProtocolException,
        WrapperException {

        logger.trace("SessionV1.doKE() -- ENTER -- ");

        String reportNameBase = SERVER_REPORTSDIR + "/" + SERVER_REPORTNAME;

        try {
            String reportNames[] = null;

            try {
                KEWrapper kew = runKE(reportNameBase, brief, minSeverity);

                // throws PEx, WEx

                // probably should query KEW for
                // reports rather than also constructing
                // report name XXXX
                // ditto SccheckClient.runKE()
                reportNames = new String[] {
                        reportNameBase + SFX_XML, reportNameBase + SFX_TXT
                    };

                // expect back an xml file & a txt file

                /*
                 * Exceptions from serverSendFiles()
                 * and sendExecErrExit() only if all
                 * clients are gone.
                 * Results in serverExit()
                 * from ServerThread.run().
                 */
                if (reportNames[0] != null) {
                    report1 = reportNames[0];
                    serverSendFiles(); // throws PEx
                } else {
                    sendExecErrExit(); // throws IOEx
                }

                if (reportNames[1] != null) {
                    report2 = reportNames[1];
                    serverSendFiles(); // throws PEx
                } else {
                    sendExecErrExit(); // throws IOEx
                }

                int maxSeverity = kew.getMaxSeverity();
                serverSendData(maxSeverity); // throws IOEx

                String genDate = kew.getGenDate();
                serverSendData(genDate); // throws IOEx

            } catch (ProtocolException pe) {

                // trapped here so we can log the PEx and add
                // our message to it before passing it along
                logger.error("SessionV1.doKE() " + "ProtocolException: " +
                    pe.getMessage());

                Object i18nArgs[] = { pe.getMessage() };
                String i18nMsg = I18n.getLocalized(
                        "KE.RunFailedProtocolException", i18nArgs);
                throw new ProtocolException(i18nMsg);
            } catch (WrapperException kewex) {
                logger.error("SessionV1.doKE() " + "WrapperException: " +
                    kewex.getMessage());

                Object i18nArgs[] = { kewex.getMessage() };
                String i18nMsg = I18n.getLocalized(
                        "KE.RunFailedWrapperException", i18nArgs);
                throw new WrapperException(i18nMsg);
            }
            // done
        } catch (IOException ioex) {
            logger.error("ServerThread.doKE() IOException: " +
                ioex.getMessage());
            throw new ProtocolException(ioex.getMessage());
        }

        logger.trace("SessionV1.doKE() -- EXIT -- ");
    } // doKE


    /*
     * runKE() [Server method]
     *
     * Wrap the execution of the KnowledgeEngine:
     *      Assign checklist based on clustermode.
     *      Create a KEWrapper sending the unpacked results
     *      directory name,
     *        then invoke runKE() on it.
     *      Return the KEWrapper itself.
     */
    private KEWrapper runKE(String reportNameBase, boolean brief,
        int minSeverity) throws ProtocolException, WrapperException {
        Vector vexploArchToUse = new Vector();

        logger.trace("SessionV1.runKE() --ENTER--");

        String checklist = null;

        if (clusterMode) {
            checklist = CHECKLIST_CLUSTER_SINGLE;
        } else {
            checklist = CHECKLIST_NONCLUSTER;
        }

        logger.info("SessionV1.runKE() checklist: " + checklist);

        // fetch path to ke-server log & xsl file dir from property
        // guaranteed to be non-null by SccheckServer
        String kelogfile = System.getProperty(PROP_KE_SERVERLOG);
        logger.info("SessionV1.runKE() ke-server.log: " + kelogfile);

        String xslfiledir = System.getProperty(PROP_KE_SERVER_XSLDIR);
        logger.info("SessionV1.runKE() ke-xslfiledir: " + xslfiledir);

        KEWrapper keWrapper = new KEWrapper(progressListener, reportNameBase,
                explorerDirA, brief, minSeverity, kelogfile, xslfiledir);
                // throws WEx

        keWrapper.runKE(checklist); // throws WEx

        logger.trace("SessionV1.runKE() --EXIT-- ");

        return keWrapper;
    } // runKE


    /*
     * getRemoteFile() [Client method]
     *
     * Wait for and receive the named file from the server.
     * (Accept progress strings and error messages while waiting for file
     * transfer to begin.)
     */
    private void getRemoteFile(String publicName, ClientProtocol prot,
        String outfile) throws ProtocolException, SCException {

        if (!prot.getRemoteFile(outfile)) { // throws ProtEx

            Object i18nArgs[] = { outfile, publicName };
            String i18nMsg = I18n.getLocalized("noResultsFile", i18nArgs);
            throw new SCException(i18nMsg);
        }
    } // getRemoteFile


    /*
     * serverSendFiles() [Server method]
     *
     * Utility for sending prepared files to all clients in the list.
     *
     * The ServerProtocolState encapsulation of each ServerProtocol object
     * tracks which files have been transmitted to each client.
     *
     * For each client: send all files which are ready to send but have not
     * yet been sent.
     *
     * Synchronization needed because this method may be called from the
     * main thread (SessionV1.joinSession() as well as ServerThread.
     *
     * All other methods that iterate on serverProts and may call
     * purgeClientList() all run in the ServerThread, never simult.
     */
    private synchronized void serverSendFiles() throws ProtocolException {
        logger.trace("SessionV1.serverSendFiles()  -- ENTER -- ");

        /*
         * for each elt serverProts
         * this code sends as many files as possible to each sprot
         * a more equitable arrangement, though slightly
         * less efficient,
         * would be to work one file through each sprot
         *
         * If only one sprot in the list and sprot.sendFoo() throws an
         * exception then pass the exception on out. If more than one
         * sprot in the list then remove that sprot and retry entire
         * operation. This method is idempotent.
         * Passing the exception out will kill the session and server;
         * don't want to do this if multiple clients.
         */
        Iterator it = serverProts.iterator();
        ServerProtocolState sprotstate = null;

        while (it.hasNext()) {
            sprotstate = (ServerProtocolState) it.next();

            try {

                if ((explorergzip != null) && !sprotstate.sentExplFile) {
                    logger.info("SessionV1." + "serverSendFiles() " +
                        "sending explorergzip via sprot" +
                        sprotstate.sprot.getID());
                    sprotstate.sprot.sendFile(explorergzip);
                    sprotstate.sentExplFile = true;
                }

                if ((report1 != null) && !sprotstate.sentReport1) {
                    logger.info("SessionV1." + "serverSendFiles() " +
                        "sending report1 via sprot" + sprotstate.sprot.getID());
                    sprotstate.sprot.sendFile(report1);
                    sprotstate.sentReport1 = true;
                }

                if ((report2 != null) && !sprotstate.sentReport2) {
                    logger.info("SessionV1." + "serverSendFiles() " +
                        "sending report2 via sprot" + sprotstate.sprot.getID());
                    sprotstate.sprot.sendFile(report2);
                    sprotstate.sentReport2 = true;
                }
            } catch (ProtocolException pex) {
                logger.trace("SessionV1.serverSendFiles() " + "in pex: " +
                    pex.getMessage());

                if (purgeClientList(it, sprotstate) == false) {
                    logger.trace("SessionV1." + "serverSendFiles() " +
                        "pex on last sprot: sprot" + sprotstate.sprot.getID() +
                        " Forwarding pex.");
                    throw pex;
                } // if
            } // catch
        } // while

        logger.trace("SessionV1.serverSendFiles()  -- EXIT -- ");
    } // serverSendFiles


    /*
     * serverSendData() [Server methods]
     * sendExecErrExit()
     *
     * Utilities for sending data to each client in the list.
     * A dead sprot will be removed from the list of client
     * and the exception thrown only if a single client
     * remains. Throwing the exception will kill session
     * and server.
     */
    private void serverSendData(int data) throws IOException {
        logger.info("SessionV1.serverSendData(int)  " + "-- ENTER -- " + data);

        // for each elt serverProts
        // use sp in sprotstate to send data
        Iterator it = serverProts.iterator();
        ServerProtocolState sprotstate = null;

        while (it.hasNext()) {
            sprotstate = (ServerProtocolState) it.next();

            try {
                sprotstate.sprot.sendInt(data);
            } catch (IOException ioex) {
                logger.trace("SessionV1.serverSendData() " + "in ioex: " +
                    ioex.getMessage());

                if (purgeClientList(it, sprotstate) == false) {
                    logger.trace("SessionV1." + "serverSendData() " +
                        "ioex on last sprot: sprot" + sprotstate.sprot.getID() +
                        " Forwarding ioex.");
                    throw ioex;
                } // if
            } // catch
        } // while

        logger.trace("SessionV1.serverSendData(int)  -- EXIT -- ");
    } // serverSendData

    private void serverSendData(String data) throws IOException {
        logger.info("SessionV1.serverSendData(String)  " + "-- ENTER -- " +
            data);

        // for each elt serverProts
        // use sp in sprotstate to send data
        Iterator it = serverProts.iterator();
        ServerProtocolState sprotstate = null;

        while (it.hasNext()) {
            sprotstate = (ServerProtocolState) it.next();

            try {
                sprotstate.sprot.sendString(data);
            } catch (IOException ioex) {
                logger.trace("SessionV1.serverSendData() " + "in ioex: " +
                    ioex.getMessage());

                if (purgeClientList(it, sprotstate) == false) {
                    logger.trace("SessionV1." + "serverSendData() " +
                        "ioex on last sprot: sprot" + sprotstate.sprot.getID() +
                        " Forwarding ioex.");
                    throw ioex;
                } // if
            } // catch
        } // while

        logger.trace("SessionV1.serverSendData(String)  -- EXIT -- ");
    } // serverSendData

    private void sendExecErrExit() throws IOException {
        Iterator it = serverProts.iterator();
        ServerProtocolState sprotstate = null;

        while (it.hasNext()) {
            sprotstate = (ServerProtocolState) it.next();

            try {
                sprotstate.sprot.sendExecErrExit();
            } catch (IOException ioex) {
                logger.trace("SessionV1.sendExecErrExit() " + "in ioex: " +
                    ioex.getMessage());

                if (purgeClientList(it, sprotstate) == false) {
                    logger.trace("SessionV1." + "sendExecErrExit() " +
                        "ioex on last sprot: sprot" + sprotstate.sprot.getID() +
                        " Forwarding ioex.");
                    throw ioex;
                } // if
            } // catch
        }
    } // sendExecErrExit


    /*
     * purgeClientList()
     *
     * Utility to remove sprots from the client list if
     * the sender encountered an exception trying
     * to talk to a client via the sprot.
     *
     * Returns true if there're any sprots left on the list
     * after the removal.
     * Returns false if not.
     */
    private boolean purgeClientList(Iterator it,
        ServerProtocolState sprotstate) {
        boolean stillMoreClients = true;

        if (serverProts.size() == 1) {
            logger.trace("SessionV1." + "purgeClientList() " +
                "exception on last sprot: sprot" + sprotstate.sprot.getID());
            stillMoreClients = false;
        } else {
            logger.trace("SessionV1." + "purgeClientList() removing sprot" +
                sprotstate.sprot.getID() + " from serverProts list.");
            it.remove(); // last item returned
                         // by it.next()
        } // if-else


        return stillMoreClients;
    } // purgeClientList

    // --------------------------------------------------------------------

    /*
     * ServerProtocolState [Server inner class]
     *
     * Facilitates tracking which files have been sent out via which
     * ServerProtocol objects
     */
    class ServerProtocolState {
        ServerProtocol sprot = null;
        boolean sentExplFile = false;
        boolean sentReport1 = false;
        boolean sentReport2 = false;

        ServerProtocolState(ServerProtocol sp) {
            sprot = sp;
        } // ()

    } // ServerProtocolState

    // --------------------------------------------------------------------

} // SessionV1
