/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)Protocol.java 1.11   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.cluster.sccheck;

import java.io.*;

import java.net.*;

import java.util.*;
import java.util.zip.*;


/*
 * This class implement functions common to both client and server side
 * protocol classes and underlying socket communications.
 *
 * Convenience methods are implemented to protect other classes from
 * having to know about the sockets as well as details of data In/Out
 * streams.
 *
 * This class is not intended to be instantiated directly.
 */

public abstract class Protocol implements Globals {
    private static int ct = 1;

    protected int BUFFSIZE = 1024 * 4;
    protected byte buffer[] = new byte[BUFFSIZE];

    protected Socket socket = null;

    protected BufferedInputStream bIn = null;
    protected DataInputStream dIn = null;

    protected BufferedOutputStream bOut = null;
    protected DataOutputStream dOut = null;

    protected String id = null;

    // -----------------------------------------------------------

    public Protocol() {
        // A Protocol object's ID is simply a 'Stringified
        // integer' unique among all Protocol objects in the same JVM.

        id = "" + ct++;
    } // ()

    public String getID() {
        return id;
    }

    protected void sendString(String s) throws IOException {

        if (s == null) {
            s = "";
        }

        dOut.writeUTF(s);
        dOut.flush();
    } // sendString

    protected String readString() throws IOException {
        return dIn.readUTF();
    } // readString

    public String readMessage() throws IOException {
        return readString();
    } // getMessage

    protected void sendCode(int i) throws IOException {
        dOut.writeInt(i);
        dOut.flush();
    } // sendCode

    protected int readCode() throws IOException {
        return dIn.readInt();
    } // readCode

    protected void sendInt(int i) throws IOException {
        dOut.writeInt(i);
        dOut.flush();
    } // sendInt

    protected int readInt() throws IOException {
        return dIn.readInt();
    } // readInt

    protected void sendLong(long l) throws IOException {
        dOut.writeLong(l);
        dOut.flush();
    } // sendLong

    protected long readLong() throws IOException {
        return dIn.readLong();
    } // readLong

    protected void sendBoolean(boolean b) throws IOException {
        dOut.writeBoolean(b);
        dOut.flush();
    } // sendBoolean

    protected boolean readBoolean() throws IOException {
        return dIn.readBoolean();
    } // readBoolean


    protected void setSocketStreams(Socket s) throws ProtocolException {

        try {
            bIn = new BufferedInputStream(socket.getInputStream());
            dIn = new DataInputStream(bIn);

            dOut = new DataOutputStream(socket.getOutputStream());

        } catch (IOException ioex) {
            throw new ProtocolException(ioex.getMessage());
        }
    } // setSocketStreams

} // Protocol
