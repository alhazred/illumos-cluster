/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * ident        "@(#)ServerProtocol.java 1.12     08/05/20 SMI"
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */


package com.sun.cluster.sccheck;

import java.io.*;

import java.net.*;

import java.util.zip.*;


/*
 * This class implements server side protocol and
 * underlying socket communications.
 */

public class ServerProtocol extends Protocol {

    private Server parent;
    private Logger logger = Logger.getLogger();

    /*
     * Constructor:
     * Accept the passed-in, already connected socket.
     * Attach to the socket streams.
     */
    public ServerProtocol(Server server, Socket s) throws ProtocolException {
        socket = s;
        setSocketStreams(s);
        parent = server;
        logger.trace("ServerProtocol() made sprot" + getID());
    } // ()


    /*
     * getCommonVersion()
     *
     * Server side of the session version negotiation.
     * Corresponds with ServerProtocol.getCommonVersion().
     * Read the highest version supported by the client.
     * Send the highest version implemented; defined in Globals.
     * Select the smaller of the two as the version to use.
     * This version number will be passed to the SessionFactory
     * to obtain a Session object.
     */
    public int getCommonVersion(int myVersion) throws ProtocolException {
        int clientVersion;
        int version;

        try {
            clientVersion = readInt();
            sendInt(myVersion);
        } catch (IOException ioex) {
            logger.error("ServerProtocol(): " + "getCommonVersion IOException" +
                ioex.getMessage());
            throw new ProtocolException(ioex.getMessage());
        }

        if (myVersion < clientVersion) {
            version = myVersion;
        } else {
            version = clientVersion;
        }

        logger.trace("ServerProtocol.getCommonVersion(): " + version);

        return version;

    } // getCommonVersion


    /*
     * sendFile()
     *
     * The actual file send:
     *      send code telling client that file is coming
     *      send filesize in bytes
     *      send blocks of bytes
     *
     */
    public void sendFile(String fname) throws ProtocolException {
        logger.trace("ServerProtocol.sendFile() -- ENTER -- sprot" +
            this.getID() + "  " + fname);

        try {

            // tell client that we're going to send
            sendExecExitOK();

            /*
             * get file size & send it
             * send file in chunks (inherit 'buffer' from Protocol)
             */

            // input stream from local file
            FileInputStream fIn = new FileInputStream(fname);
            BufferedInputStream bfIn = new BufferedInputStream(fIn);

            File f = new File(fname);
            long fsize = f.length();

            long toSend = fsize;
            long byteTally = 0;
            int bytesRead = 0;

            // tell client number of bytes to expect for file
            sendLong(fsize);
            logger.info("ServerProtocol.sendFile() fsize: " + fsize);

            // write the file
            while (byteTally < fsize) {
                bytesRead = bfIn.read(buffer);

                if (bytesRead == -1) {
                    String i18nMsg = I18n.getLocalized(
                            "UnexpectedEOF.OnFileRead");
                    logger.error("ServerProtocol.sendFile() " +
                        "unexpected EOF on file read.");
                    throw new ProtocolException(i18nMsg);
                }

                dOut.write(buffer, 0, bytesRead);
                dOut.flush();
                byteTally += bytesRead;
                toSend -= bytesRead;
            }

            dOut.flush();
            bfIn.close();

        } catch (FileNotFoundException fnfe) {
            logger.error("ServerProtocol.sendFile() " +
                "FileNotFoundException: " + fnfe.getMessage());
            throw new ProtocolException(fnfe.getMessage());
        } catch (IOException ioe1) {

            /*
             * this IOException catches ioex from all of:
             *      sendExplExitOK()
             *      dOut writes
             *      fIn read
             *      bfIn close
             */
            logger.error("ServerProtocol.sendFile() " + "in IOException: " +
                ioe1.getMessage());
            throw new ProtocolException(ioe1.getMessage());
        }

        logger.trace("ServerProtocol.sendFile() --EXIT-- ");
    } // sendFile

    /*
     * sendProgress()
     *
     * Send progress string with verbosity level to client
     */
    public void sendProgress(int verboseLevel, String s) throws IOException {
        sendCode(PROTProgressStr);
        sendCode(verboseLevel);
        sendString(s);
        logger.info("ServerProtocol.sendProgress(v level: " + verboseLevel +
            ") msg: " + s);
    } // sendProgress

    public void sendProgress(String s) throws IOException {

        // if not otherwise specified messages go VVERBOSE
        sendProgress(VVERBOSE, s);
    } // sendProgress

    /*
     * sendPermOK()
     * sendErrBusy()
     * sendErrPerm()
     * sendExecExitOK()
     * sendExecErrRun()
     * sendExecErrExit()
     *
     * Send appropriate codes and or messages to client.
     *
     */
    public void sendPermOK() throws IOException {
        logger.info("ServerProtocol.sendPermOK()");
        sendBoolean(true);
    } // sendPermOK

    public void sendErrBusy() throws IOException {
        sendBoolean(false);
    } // sendErrBusy

    public void sendErrPerm(String msg) throws IOException {
        sendBoolean(false);
        sendString(msg);
    } // sendErrPerm

    public void sendExecExitOK() throws IOException {
        sendInt(PROTExecExitOK);
    } // sendExecExitOK

    public void sendExecErrRun(String msg) throws IOException {
        sendInt(PROTExecErrRun);
        sendString(msg);
    } // sendExecErrRun

    public void sendExecErrExit() throws IOException {
        sendInt(PROTExecErrExit);
    } // sendExecErrExit


    /*
     * readAuthStr()
     *
     * Read authorization string from client.
     * Pass on to server.
     */
    public void readAuthStr() throws IOException {
        String s = readString();
        logger.info("ServerProtocol.readAuthStr(): " + s);
        parent.setAuth(s);
    } // readAuthStr


    /*
     * readLocale()
     *
     * Read locale strings from client.
     * Pass on to server.
     */
    public void readLocale() throws IOException {
        String s = readString();
        logger.info("ServerProtocol.readLocale() lang >" + s + "<");
        parent.setLocaleLang(s);

        s = readString();
        logger.info("ServerProtocol.readLocale() country >" + s + "<");
        parent.setLocaleCountry(s);

        s = readString();
        logger.info("ServerProtocol.readLocale() variant >" + s + "<");
        parent.setLocaleVariant(s);
    } // readLocale

} // ServerProtocol
