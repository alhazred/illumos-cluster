/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)SCSeverityCheckFilter.java 1.8   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */
/*
 * This class is a CheckFilter based on severity. The engine (v 3.x+) uses
 * this filter to determine which checks to run or ignore.
 *
 * We're using this class instead of
 *      com.sun.eras.common.checks.filters.SeverityCheckFilter
 * because we need a count of checks filtered out (numFilteredChecks).
 * We also get to use the sccheck logger.
 *
 * Not extending
 *      com.sun.eras.common.checks.filters.IntCheckFilter
 * because this class is less cluttered.
 *
 * Checks with severity strictly less than the severity sent in to the
 * constructor will not be run. See match().
 *
 * Checks with severity equal to or greater than the severity sent in to
 * the constructor will be run. See match().
 *
 */


package com.sun.cluster.sccheck;

import com.sun.eras.common.checks.*;
import com.sun.eras.common.checks.filters.*;


public class SCSeverityCheckFilter extends CheckFilter implements Globals {

    private com.sun.cluster.sccheck.Logger logger =
        com.sun.cluster.sccheck.Logger.getLogger();

    private int severity = 0;
    private int numFilteredChecks = 0;

    public SCSeverityCheckFilter() {
    } // ()

    public SCSeverityCheckFilter(int severity) {
        this.severity = severity;
        logger.info("SCSeverityCheckFilter() filtering at severity " +
            severity);
    } // ()

    public boolean match(Check check) {

        if (check.getSeverity().intValue() < severity) {
            logger.info("SCSeverityCheckFilter.match() excluding " +
                check.getId());
            numFilteredChecks++;

            return false;
        } else {
            return true;
        }
    } // match

    public int getNumFilteredChecks() {
        return numFilteredChecks;
    } // getNumFilteredChecks


} // SCSeverityCheckFilter
