/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)I18n.java 1.8   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.cluster.sccheck;

import java.io.*;

import java.lang.*;

import java.text.MessageFormat;

import java.util.*;


public class I18n {

    /*
     * This class provides access to message strings from ResourceBundles.
     *
     * getLocalized(String key)
     *          Get the string for key.
     *
     * getLocalized(String key, Object[] i18nArgs)
     *          Get the string key with elements from i18nArgs substituted in.
     *
     * getBundle() -internal
     *          Figure out current locale, obtain ResourceBundle and
     *          remember it in a static. This assumes that once locale has been
     *          set in the JVM that it won't be changed.
     *
     * readEnv(String envVar) -internal
     *          Read environment variables.
     *
     */


    private static ResourceBundle messages = null;

    /*
     * getLocalized(String key)
     *
     * Wraps ResourceBundle.getString(String key)
     */
    public static synchronized String getLocalized(String key) {

        if (messages == null) {
            messages = I18n.getBundle();

            if (messages == null) {
                return key;
            }
        }

        String str = key;

        try {
            str = messages.getString(key);
        } catch (java.util.MissingResourceException mrex) {
            // ignore and just return the key
        }

        return str;
    } // getLocalized


    /*
     * getLocalized(String key, Object[] i18nArgs)
     *
     * Wraps MessageFormat.format(String pattern, Object[] args);
     */
    public static synchronized String getLocalized(String key,
        Object i18nArgs[]) {

        if (messages == null) {
            messages = I18n.getBundle();

            if (messages == null) {
                return key;
            }
        }

        String str = "";

        try {
            String pattern = messages.getString(key);
            str = MessageFormat.format(pattern, i18nArgs);
        } catch (java.util.MissingResourceException mrex) {

            // ignore and just return the key
            str = key;
        }

        return str;
    } // getLocalized


    /*
     * getBundle()
     *
     * Attempt to find a locale-specific ResourceBundle, locale
     * based on environment settings.
     * If locale is not set default to en_US.
     *
     * ResourceBundle.getBundle(bun,locale) behavior is:
     *          attempt to find 'bun_locale'.
     *          if that fails look for 'bun_<default_locale>'.
     *          if that fails look for 'bun'.
     *          if that fails throw java.util.MissingResourceException.
     *
     * Note that even if a locale-specific bundle is provided that
     * a 'base' bundle should be provided as well.
     */
    private static ResourceBundle getBundle() {

        Logger logger = Logger.getLogger();
        String localeString;
        String language = "";
        String country = "";

        /*
         * Read the language environment variable from the UNIX shell.
         * If LC_MESSAGES environment varible is present, it overrides
         * the value of LANG. So, first look for LC_MESSAGES environment
         * variable. If it's not set, then get the value of LANG
         * variable. If this is also not set, then use en_US as default
         * locale.
         */

        localeString = readEnv("LC_MESSAGES");

        if ((localeString == null) || localeString.equals("")) {
            localeString = readEnv("LANG");

            if ((localeString == null) || localeString.equals("") ||
                    (localeString.equals("C"))) {
                localeString = "en_US";
            }
        }

        /*
         * The format of the localeString can be in
         * any of the following forms:
         * <LANGUAGE>
         * <LANGUAGE>_<COUNTRY>
         */
        StringTokenizer strTok = new StringTokenizer(localeString, "_");
        language = strTok.nextToken();

        if (strTok.hasMoreTokens()) {
            country = strTok.nextToken();
        }

        logger.info("I18n.getBundle(): " + language + "_" + country);

        ResourceBundle rb = null;

        try {
            rb = ResourceBundle.getBundle(
                    "com.sun.cluster.sccheck.MessagesBundle",
                    new Locale(language, country));
        } catch (MissingResourceException mrex) {

            // ignore and return null
            logger.error("I18n.getBundle(): bundle not found");
        }

        return rb;
    } // getBundle


    /*
     * readEnv(String envVar)
     *
     * Read the specified environment variable from the UNIX shell.
     * If the exit code is non-zero or the value is empty then return
     * null.
     */
    private static String readEnv(String envVar) {

        String envVarValue = null;

        String cmdArray[] = { "/usr/ucb/printenv", envVar };

        try {
            envVarValue = Utils.runCmdOneString(cmdArray);

            if (envVarValue.equals("")) {
                envVarValue = null;
            }
        } catch (WrapperException wex) {
            // ignore; return null
        }

        return envVarValue;
    } // readEnv
} // I18n
