/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * ident        "@(#)Session.java 1.10     08/05/20 SMI"
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.cluster.sccheck;

/*
 * This interface defines the five main methods for conducting
 * the client-server session.
 *
 */

public interface Session {

    // Original interface
    public void initClient(Client c, ClientProtocol cp, String pbn, String pvn,
        String results, String reports, boolean brief, int minSeverity)
        throws ProtocolException, SCException;

    // Expanded interface
    public void initClient(Client c, ClientProtocol cp, String pbn, String pvn,
        String results, String reports, boolean brief, int minSeverity,
        Object additionalData[]) throws ProtocolException, SCException;

    public void runClient(ProgressListener pl) throws ProtocolException,
        SCException;

    public void initServer(Server s, ServerProtocol sp, boolean clmode)
        throws ProtocolException;

    public void runServer(ProgressListener pl) throws ProtocolException,
        WrapperException;

    public void joinSession(ServerProtocol sp) throws ProtocolException;

} // Session
