/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * ident        "@(#)ServerThread.java 1.14     08/05/20 SMI"
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.cluster.sccheck;

import java.io.*;

import java.util.*;


/*
 * This Thread class handles the server's side of the client-server
 * session after authentication and initialization has been done by
 * SccheckServer which returns to listening for additional client
 * connections after launching this Thread.
 *
 * ServerThread receives the ServerProtocol and Session objects from
 * SccheckServer.
 *
 * If an additional client contacts SccheckServer while this Thread is
 * active SccheckServer will hand the new ServerProtocol object to this
 * ServerThread.
 *
 * ServerThread implements ProgressListener for server-side message
 * generators, passing the messages to the client(s) via the ServerProtocol
 * object(s).
 *
 * Upon completion, the run() method calls server.serverExit() which
 * terminates the server-side JVM.
 */

public class ServerThread extends Thread implements ProgressListener, Globals {

    private Server server = null;
    private Vector serverProts = null;
    private Session session = null;
    private int version = 0;
    private Logger logger = Logger.getLogger();

    /*
     * Constructor:
     *
     * Accept from the server values necessary for continuing a
     * session with an originating client.
     */
    public ServerThread(Server sv, Session s, int version, ServerProtocol p) {
        logger.trace("ServerThread() -- ENTER -- ");

        server = sv;
        this.version = version;
        session = s;
        serverProts = new Vector();
        addServerProtocol(p);

        logger.trace("ServerThread() -- EXIT -- ");
    } // ()


    /*
     * addServerProtocol()
     *
     * Called by SccheckServer to add an additional client to a
     * running session.
     */
    public void addServerProtocol(ServerProtocol p) {
        logger.info("ServerThread.addServerProtocol() adding sprot" +
            p.getID());
        serverProts.addElement(p);
    } // addServerProtocol


    /*
     * run()
     *
     * Implements Thread.run()
     * Call runServer() on existing Session object.
     * Return any exception messages via ProgressListener.postErrMsg().
     *
     * Session is finished: tell the server to exit the JVM.
     */
    public void run() {
        logger.trace("ServerThread.run() -- ENTER -- ");

        try {

            // future Session version must add a new case
            logger.info("ServerThread.run() running SessionV" + version);

            switch (version) {

            case 1:
            case 2:
                session.runServer(this);

                break;

            default:

                String errorMsg = "Unsupported Session version: " + version;
                logger.error("ServerThread.run(): run: " + errorMsg);

                Object i18nArgs[] = { new String("" + version) };
                String i18nMsg = I18n.getLocalized("unsupportedSessionVersion",
                        i18nArgs);
                throw new SCException(i18nMsg);
            } // switch
        } catch (ProtocolException pex) {
            logger.error("ServerThread.run() ProtocolException: " +
                pex.getMessage());
            this.postErrMsg(pex.getMessage());
        } catch (WrapperException wex) {
            logger.error("ServerThread.run() WrapperException: " +
                wex.getMessage());
            this.postErrMsg(wex.getMessage());
        } catch (SCException scex) {
            logger.error("ServerThread.run() SCException: " +
                scex.getMessage());
            this.postErrMsg(scex.getMessage());
        }

        logger.trace("ServerThread.run() calling server.serverExit()");
        server.serverExit();
    } // run


    // -----  start interface ProgressListener -----

    public void postProgress(int verboseLevel, String s) {
        logger.trace("SccheckServerThread.postProgress(): " + s);
        sendProgress(verboseLevel, s);
    } // postProgress

    public void postErrMsg(String s) {
        logger.trace("SccheckServerThread.postErrMsg(): " + s);
        sendError(s);
    } // postErrMsg

    // -----  end interface ProgressListener -----


    /*
     * sendProgress()
     * sendError()
     *
     * Helper methods for implementing ProgressListener interface.
     * Call the appropriate ServerProtocol method on each of the
     * ServerProtocol objects in the list.
     *
     * If the client to which a ServerProtocol object was attached
     * disconnects, typically closing its socket when user presses ^C,
     * the sprot is removed.
     */
    private void sendProgress(int verboseLevel, String msg) {

        logger.info("ServerThread.sendProgress(v level: " + verboseLevel +
            ") " + "sending msg: " + msg);

        Iterator it = serverProts.iterator();
        ServerProtocol sprot = null;

        while (it.hasNext()) {
            sprot = (ServerProtocol) it.next();
            logger.info("ServerThread.sendProgress() " +
                "passing msg to sprot" + sprot.getID());

            try {
                sprot.sendProgress(verboseLevel, msg);
            } catch (IOException ioex) {
                logger.error("SccheckServerThread." + "sendProgress() ioex: " +
                    ioex.getMessage());
                logger.trace("SccheckServerThread." +
                    "sendProgress() removing sprot" + sprot.getID());
                it.remove(); // last item returned by it.next()
            }
        }
    } // sendProgress

    private void sendError(String msg) {
        logger.info("ServerThread.sendError() sending msg: " + msg);

        Iterator it = serverProts.iterator();
        ServerProtocol sprot = null;

        while (it.hasNext()) {
            sprot = (ServerProtocol) it.next();
            logger.info("ServerThread.sendError() " + "passing msg to sprot" +
                sprot.getID());

            try {
                sprot.sendExecErrRun(msg);
            } catch (IOException ioex) {
                logger.error("SccheckServerThread.sendError() " + "ioex: " +
                    ioex.getMessage());
                logger.trace("SccheckServerThread.sendError() " +
                    "removing sprot" + sprot.getID());
                it.remove(); // last item returned by it.next()
            }
        }
    } // sendError

} // ServerThread
