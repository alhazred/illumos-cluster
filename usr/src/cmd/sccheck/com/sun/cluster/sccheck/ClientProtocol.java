/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * @(#)ClientProtocol.java 1.8   08/05/20 SMI
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.cluster.sccheck;

import java.io.*;

import java.net.*;

import java.util.zip.*;


/*
 * This class implements client side protocol and
 * underlying socket communications.
 */

public class ClientProtocol extends Protocol implements Globals {

    private ProgressListener progresslistener = null;
    private Client parent = null;

    private String resultsFile = null;
    private String serverPublicName = null;
    private Logger logger = Logger.getLogger();
    private SCProperties props = SCProperties.getSCProperties();

    /*
     * Constructor:
     *
     * Make a fresh socket and immediately attempt to connect to specified
     * server via 'inet.port' to cause inetd to fire up the server.
     *
     * The server cannot use only the streams provided by the inetd
     * connection. The server needs a Socket object in order to perform
     * network-related validations.
     *
     * Discard the socket and attempt to connect directly to the server via
     * 'serverPort' using an outgoing port in a subrange of the
     * root-priviledged ports: start at MAXOUTPORT and decrement to
     * MINOUTPORT.
     * Retry the connection for each possible port within an overall retry
     * loop.
     * Loop parameters are controlled by 'client.wait.*' properties.
     *
     * Attach to the socket streams.
     *
     * Now ready to communicate.
     *
     */

    public ClientProtocol(ProgressListener pl, Client c,
        String serverPublicName, String serverPrivateName, String myName)
        throws ProtocolException {

        progresslistener = pl;
        parent = c;

        String server = serverPrivateName;
        this.serverPublicName = serverPublicName;

        Object i18nArgs[] = null;
        String i18nMsg = null;

        int inetPort = SCProperties.intFromProps(PROP_SC_INETPORT, INETPORT,
                props);
        logger.info("ClientProtocol() inetPort: " + inetPort);

        int serverPort = SCProperties.intFromProps(PROP_SC_SERVERPORT,
                SERVERPORT, props);
        logger.info("ClientProtocol() serverPort: " + serverPort);

        try {
            logger.trace("ClientProtocol() " + "contacting " + server +
                " on inetd port " + inetPort);

            // cause inetd to launch the server
            socket = new Socket(server, inetPort);

            // now let another thread run while the server starts up
            Thread.yield();
        } catch (UnknownHostException uhex) {
            logger.error("ClientProtocol(): UnknownHostException " +
                serverPublicName + ": " + uhex.getMessage());
            i18nArgs = new Object[] { uhex.getMessage() };
            i18nMsg = I18n.getLocalized("unknownHost", i18nArgs);
            throw new ProtocolException(i18nMsg);
        } catch (IOException ioex) {
            logger.error("ClientProtocol(): IOException " + serverPublicName +
                ": " + ioex.getMessage());
            throw new ProtocolException(ioex.getMessage());
        }

        boolean connected = false;
        int retryCt = SCProperties.intFromProps(PROP_SC_CLIENTWAITCOUNT, 15,
                props);
        int sleepInterval = SCProperties.intFromProps(
                PROP_SC_CLIENTWAITINTERVAL, 1000, props);
        int ct = 0;

        while (!connected && (ct++ < retryCt)) {
            socket = null;

            try {
                logger.trace("ClientProtocol() " + "contacting " + server +
                    " on server port " + serverPort);

                // attempt to connect directly to server
                // on root-privileged port
                boolean madeSocket = false;

                // decrement as needed
                int outgoingPort = MAXOUTPORT;

                // subrange of reserved ports:
                // MINOUTPORT through MAXOUTPORT
                InetAddress myIA = InetAddress.getByName(myName);
                logger.info("ClientProtocol() myIA: " + myIA);

                while (!madeSocket) {

                    try {
                        logger.trace("ClientProtocol() " +
                            "attempting outgoing " + "port: " + outgoingPort);
                        socket = new Socket(server, serverPort, myIA,
                                outgoingPort);
                        madeSocket = true;
                    } catch (UnknownHostException uhex2) {
                        logger.error("ClientProtocol():" +
                            " UnknownHostException2 " + serverPublicName +
                            ": " + uhex2.getMessage());
                        throw new ProtocolException(uhex2.getMessage());
                        // fatal
                    } catch (IOException ioex1) {

                        /*
                         * Assume exception is due to
                         * unavailable outgoing port or
                         * server not yet responding.
                         * Decrement port num up down to
                         * MINOUTPORT then give up.
                         */
                        logger.error("ClientProtocol() " +
                            "in IOException ioex1: " + ioex1.getMessage());
                        outgoingPort--;

                        if (outgoingPort < MINOUTPORT) {
                            logger.error("ClientProtocol() " + "reserved " +
                                "outgoingPort not" + " found");
                            throw new IOException(ioex1.getMessage());
                        }
                    }
                } // while !madeSocket: outgoing port loop

                connected = true;
                logger.trace("ClientProtocol() " +
                    "connected on outgoing port: " + outgoingPort +
                    " in retry loop " + ct);
                setSocketStreams(socket); // throws PEx
            } catch (IOException ioex2) {

                // assume exception is due to server not running
                // and retry up to retryCt times
                logger.error("ClientProtocol() " + "in IOException ioex2: " +
                    ioex2.getMessage());
                logger.error("ClientProtocol() " +
                    "incrementing wait-for-server loop");
                i18nArgs = new Object[] { serverPublicName };
                i18nMsg = I18n.getLocalized("waitingForServerStart", i18nArgs);

                try {
                    Thread.sleep(sleepInterval);
                } catch (InterruptedException iex) {
                    // ignore
                }

                postProgress(VVERBOSE, i18nMsg, false);
            }
        } // while !connected: waiting-for-server loop

        if (!connected) {

            /*
             * exception message will be
             *          "Connection refused" (server not accepting
             *          requests on server.port)
             * or
             *          "Address already in use" (requested outgoing
             *          port is in use; not able to even construct
             *          the socket)
             * neither of which are useful to the user
             * so we invent our own slightly more general message
             */
            logger.trace("ClientProtocol(): " + "giving up on " +
                serverPublicName);
            i18nArgs = new Object[] { serverPublicName };
            i18nMsg = I18n.getLocalized("unableToContactServer", i18nArgs);
            throw new ProtocolException(i18nMsg);
            // this exception message is displayed to the user
        }

    } // ()


    /*
     * getCommonVersion()
     *
     * Client side of the session version negotiation.
     * Corresponds with ServerProtocol.getCommonVersion().
     * Send the highest version implemented; defined in Globals.
     * Read the highest version supported by the server.
     * Select the smaller of the two as the version to use.
     * This version number will be passed to the SessionFactory
     * to obtain a Session object.
     */
    public int getCommonVersion(int myVersion) throws ProtocolException {
        int serverVersion;

        try {
            sendInt(myVersion);
            serverVersion = readInt();

        } catch (IOException ioex) {
            logger.error("ClientProtocol(): " + "getCommonVersion IOException" +
                ioex.getMessage());
            throw new ProtocolException(ioex.getMessage());
        }

        if (myVersion < serverVersion) {
            return myVersion;
        } else {
            return serverVersion;
        }
    } // getCommonVersion


    /*
     * getRemoteFile()
     *
     * Wait for and receive file from server.
     * Accept progress strings or error messages while waiting for file
     * transfer to begin.
     * Upon receipt of code indicating that the server has "exited OK" start
     * the file receive.
     *
     */
    public boolean getRemoteFile(String resultsFile) throws ProtocolException {

        int code;
        int verboseLevel;
        boolean success = true;
        boolean done = false;
        String s = null;

        try {

            while (!done) {
                code = readCode();

                switch (code) {

                case PROTProgressStr:
                    verboseLevel = readCode();
                    s = readString();
                    postProgress(verboseLevel, s);

                    break;

                case PROTExecErrRun:
                    s = readString(); // err msg from Expl
                    postErrMsg(s);

                    break;

                case PROTExecErrExit:
                    success = false;
                    done = true;

                    break;

                case PROTExecExitOK:
                    receiveFile(resultsFile); // ProtEx
                    success = true;
                    done = true;

                    break;

                default:
                    success = false;
                    done = true;

                    break;
                } // switch
            } // while
        } catch (IOException ioex) {
            throw new ProtocolException(ioex.getMessage());
        }

        return success;
    } // getRemoteFile


    /*
     * sendLocale()
     *
     * Send locale information: lang, country, variant
     */
    public void sendLocale() throws IOException {

        String s = parent.getLocaleLang();

        if (s == null) {
            s = "";
        }

        sendString(s);

        s = parent.getLocaleCountry();

        if (s == null) {
            s = "";
        }

        sendString(s);

        s = parent.getLocaleVariant();

        if (s == null) {
            s = "";
        }

        sendString(s);
    } // sendLocale

    /*
     * sendAuth()
     *
     * Send additional authorization string
     */
    public void sendAuth() throws IOException {
        String s = parent.getAuth();

        if (s == null) {
            s = "";
        }

        sendString(s);
    } // sendAuth

    /*
     * getPermission()
     *
     * Read boolean from server: indicates permission to continue.
     */
    public boolean getPermission() throws IOException {
        boolean b = readBoolean();

        return b;
    } // getPermission


    /*
     * postProgress()
     *
     * Pass a progress message and its verbosity level along to my
     * ProgressListener.
     * If appropriate, prefix the message with the name of the node the
     * message came from.
     */
    private void postProgress(int verboseLevel, String s,
        boolean useServerName) {

        if (useServerName) {

            // message is "signed" by server
            s = serverPublicName + ": " + s;
        }

        progresslistener.postProgress(verboseLevel, s);
    } // postProgress

    private void postProgress(int verboseLevel, String s) {
        postProgress(verboseLevel, s, true);
    } // postProgress


    /*
     * postErrMsg()
     *
     * Given an error message from the server I'm connected to,
     * prepend the server name and pass it along to my
     * ProgressListener.
     */
    private void postErrMsg(String s) {
        Object i18nArgs[] = { serverPublicName, s };
        String i18nMsg = I18n.getLocalized("postErrMsgFormat", i18nArgs);
        progresslistener.postErrMsg(i18nMsg);
    } // postErrMsg


    /*
     * receiveFile()
     *
     * The actual file receive:
     *          read filesize
     *          read blocks of bytes in loop until read this many bytes
     * save to specified name
     *
     */
    private void receiveFile(String fname) throws ProtocolException {

        /*
         * read filesize
         * read blocks of bytes in loop until read this many bytes
         */

        int bytesRead = 0;
        long bytesToRead = 0;
        long nextBlockSize = 0;

        try {

            // output stream to file being written locally
            FileOutputStream fOut = new FileOutputStream(fname);
            BufferedOutputStream bOut = new BufferedOutputStream(fOut);

            // read filesize
            bytesToRead = dIn.readLong();

            while (bytesToRead > 0) {
                nextBlockSize = bytesToRead;

                if (nextBlockSize > BUFFSIZE) {
                    nextBlockSize = BUFFSIZE;
                }

                // at this point nextBlockSize is guaranteed
                // to be an int; OK to cast
                bytesRead = bIn.read(buffer, 0, (int) nextBlockSize);

                // if (bytesRead != nextBlockSize) {
                if (bytesRead <= 0) {
                    logger.error("ClientProtocol.receiveFile() " + "expected " +
                        nextBlockSize + "; got " + bytesRead +
                        " bytes: breaking");

                    String i18nMsg = I18n.getLocalized(
                            "UnexpectedEOF.OnFileRx");
                    throw new ProtocolException(i18nMsg);
                }

                bytesToRead -= (long) bytesRead;
                bOut.write(buffer, 0, bytesRead);
            } // while

            bOut.close();

            // capture UnknownHostException first
        } catch (UnknownHostException unhex) {
            logger.error("ClientProtocol.receiveFile() " +
                "in UnknownHostException: " + unhex.getMessage());
            throw new ProtocolException(unhex.getMessage());

        } catch (IOException ioe1) {
            logger.error("ClientProtocol.receiveFile() " + "in IOException: " +
                ioe1.getMessage());
            throw new ProtocolException(ioe1.getMessage());

        }
    } // receiveFile

} // ClientProtocol
