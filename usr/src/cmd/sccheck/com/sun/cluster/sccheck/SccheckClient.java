/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * ident        "@(#)SccheckClient.java 1.17     08/05/20 SMI"
 *
 * Copyright 2006-2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

package com.sun.cluster.sccheck;

import java.awt.*;

import java.io.*;

import java.text.MessageFormat;

import java.util.*;


/*
 * This class is the main java client:
 *
 * collect user and other values from the ksh wrapper
 * perform additional validations
 * launch one ClientThread per participating node (server)
 * accept from ClientThreads:
 *      progress and error messages
 *      generation date for checklist used
 *      maxSeverity of all failed rules encountered by server
 *      name of compressed explorer results file returned from server
 * when all threads have returned, if no execution failures:
 *      run the Knowledge Engine against all the explorer results
 *      and the multi-node checklist.
 * exit with either an execution error code or code reflecting
 *      max maxSeverity all single-nod and multi-node reports.
 */

public class SccheckClient implements Client, ProgressListener, Globals {

    // may be overridden by user optional properties
    // see checkOptionalDefines() for initializations and assignments
    private boolean brief = false;
    private boolean verbose = false;
    private boolean vverbose = false;
    private int minSeverity = 0;
    private String hostlist = null;
    private String reportsDir = null;
    private String singleRuleFilePath = CHECKLIST_CLUSTER_SINGLE;
    private String multiRuleFilePath = CHECKLIST_CLUSTER_MULTI;
    private String explorerFilesPath = null;

    /*
     * if the user specify explorer archive as input
     * we don't need to collect them from remote nodes
     */
    private boolean explorerArchiveSpecified = false;

    // not overridden by user prefs
    private String explorersgzDir = null;
    private String explorersUnpackDir = null;
    private Vector zippedExplorers = new Vector();
    private String clientNumber = null;
    private String clientName = null;
    private String privateClientName = null;
    private String clustername = null;
    private String clientVarTmpDir = null;
    private String xslDir = null;
    private String keLogName = null;

    private boolean clientThreadFailed = false;
    private String failedNodes = null;
    private int numHosts = 0;

    private String genDates[] = null;

    // current cluster members; passed in via defines
    private String clusterPublicNames[] = null;
    private String clusterPrivateNames[] = null;

    // the list of names selected for this session
    // created either by validating hostlist or by defaulting
    private String sessionPublicNames[] = null;
    private String sessionPrivateNames[] = null;

    private boolean inClusterMode = true;
    private int maxServerExitCode = 0;
    private int multiReportExitCode = 0;

    private Logger logger = null;
    private SCProperties props = null;


    /*
     * Constructor
     *
     * All values passed from ksh via java defines.
     */
    public SccheckClient() {

        boolean doOnlyMultiCheck = false;

        /*
         * get clientnumber in order to start log file
         * load properties file
         * begin logging
         */
        clientNumber = System.getProperty(PROP_SCK_CLIENTNUM);

        if (clientNumber == null) {
            clientNumber = "0";
            clientVarTmpDir = CLIENT_VARTMPDIR + "." + clientNumber;

        }

        try {
            props = SCProperties.getSCProperties(PROPSFILE);
        } catch (IOException e) {
            /*
             * not fatal; queries to SCProperties will return
             * default if props == null
             */
        }

        boolean doLogging = SCProperties.booleanFromProps(PROP_SC_LOGGING,
                false, props);
        logger = new Logger(CLIENTLOG + "." + clientNumber, doLogging);
        logger.info("SccheckClient() -- ENTER; clientNumber: " + clientNumber);
        logger.info("SccheckClient() java version: " +
            System.getProperty("java.version"));

        if (!doLogging) {

            // override kae property for kae logging
            System.setProperty(PROP_KE_CLIENTLOG, DEVNULL);
        }

        /*
         * validate defines:
         *
         * if any required defines are missing exit error
         */
        try {
            checkRequiredDefines();
        } catch (SCException scex) {
            String err = scex.getMessage();
            logger.error("SccheckClient() usage error: " + err);
            earlyExit(EXITUsageERR, err);
        }

        try {
            loadOptionalDefines();
        } catch (SCException scex) {
            String err = scex.getMessage();
            logger.error("SccheckClient() error: " + err);
            earlyExit(EXITComponentERR, err);
        }


        /*
         * create and/or validate the hostlist:
         * if any invalid hosts exit error
         */

        if (explorerFilesPath != null) {

            // user specified explorer archive => special treatment
            expandExplorerList();
            explorerArchiveSpecified = true;

        }

        logger.info("explorer archive specified : " + explorerArchiveSpecified);

        /*
         * if more than one explorer file is specified
         * call only multi nodes check
         * User can specify one or more explorer result
         * if only one explorer archive :
         *  perform single-nodecheck on it
         *  otherwise perform multinodecheck on the all set
         *  of archive
         */

        if (zippedExplorers.size() > 1) {
            logger.trace("More than one explorer archives specified" +
                "=> jump to  multi nodes checks");
            doOnlyMultiCheck = true;
        } else {

            // will perform single-nodecheck on one explorer file
            sessionPublicNames = new String[1];
            sessionPrivateNames = new String[1];
            sessionPublicNames[0] = clientName;
            sessionPrivateNames[0] = privateClientName;
        }

        logger.info("do only multi node check : " + doOnlyMultiCheck);

        if (!explorerArchiveSpecified) {

            try {
                expandHostlist();
            } catch (SCException scex) {
                String err = scex.getMessage();
                logger.error("SccheckClient() bad hostname: " + err);
                earlyExit(EXITBadNameERR, err);
            }
        }

        if (!doOnlyMultiCheck) {


            /*
             * run the remote operations:
             *
             * scatter/gather:
             *  run a ClientThread for each host in hostlist
             *  collect back the explorer results and KE reports
             *
             * if execution failure with any host (server) exit error
             */
            try {

                if (!explorerArchiveSpecified) {
                    remoteOperations(sessionPublicNames, sessionPrivateNames,
                        null);
                } else {
                    remoteOperations(sessionPublicNames, sessionPrivateNames,
                        zippedExplorers.get(0).toString());
                }
            } catch (SCException scex) {
                String err = scex.getMessage();
                logger.error("SccheckClient() remote ops exception: " + err);

                if (clientThreadFailed) {
                    earlyExit(EXITNodeERR, err);
                } else {
                    earlyExit(EXITFrameERR, err);
                }
            } // try

        } // if (!doOnlyMultiCheck)

        /*
         * run the cluster report:
         *
         * if more than one host involved run multi-node
         *      checks on the client machine
         *
         * if unable to run exit error
         */
        if (zippedExplorers.size() > 1) {
            logger.trace("SccheckClient(): " + clustername +
                ": running multi-node checks");

            try {

                // multi-node checks run on local machine
                multiReportExitCode = runKE(zippedExplorers, multiRuleFilePath);
            } catch (WrapperException wex2) {
                logger.error("SccheckClient() " + "in WrapperException wex2: " +
                    wex2.getMessage());
                earlyExit(EXITFrameERR, wex2.getMessage());
            }
        } else {
            logger.trace(clustername + ": skipping multi-node checks");
        }


        /*
         * If any single-node checklist used by
         * any server has different
         *      generation date than the others, issue a warning.
         * Note that this is not an error because the checklists
         *      are all valid; they're just not the same versions.
         */
        if (!doOnlyMultiCheck) {
            compareGenDates();
        }

        /*
         * ordinary exit:
         * main() will retrieve the appropriate exit code and
         * exit with it
         */

        logger.trace("SccheckClient() --EXIT-- " + maxServerExitCode);
        logger.close();
    } // ()


    /*
     * checkRequiredDefines()
     *
     * Make sure that various defines required by sccheck and KE
     *      have been provided by scccheck.ksh.
     * Missing properties are fatal: throw exception.
     *
     * Load sccheck required defines into class members.
     */

    private void checkRequiredDefines() throws SCException {
        String status = null;
        Vector reqProps = new Vector();

        // required by sccheck
        reqProps.addElement(PROP_SCK_CLMODE);
        reqProps.addElement(PROP_SCK_CLNAME);
        reqProps.addElement(PROP_SCK_LOCALNAME);
        reqProps.addElement(PROP_SCK_PRIVLOCALNAME);
        reqProps.addElement(PROP_SCK_PUBLICNAMES);
        reqProps.addElement(PROP_SCK_PRIVATENAMES);
        reqProps.addElement(PROP_SCK_OUTPUTDIR);
        reqProps.addElement(PROP_SCK_EXPLGZDIR);
        reqProps.addElement(PROP_SCK_EXPLUNPACKDIR);
        reqProps.addElement(PROP_SCK_GUNZIP);
        reqProps.addElement(PROP_SCK_TAR);

        reqProps.addElement(PROP_KE_CLIENTLOG);
        reqProps.addElement(PROP_KE_CLIENT_XSLDIR);

        // walk the list and verify that each named define has a value
        String requiredProp;
        String systemProp;
        Enumeration e = reqProps.elements();

        while (e.hasMoreElements()) {
            requiredProp = (String) e.nextElement();
            systemProp = System.getProperty(requiredProp);

            if (systemProp == null) {

                // assemble a complete list before throwing SCEx
                if (status == null) {
                    status = requiredProp;
                } else {
                    status = status + ", " + requiredProp;
                }
            }
        } // while

        if (status != null) {
            Object i18nArgs[] = { status };
            String i18nMsg = I18n.getLocalized("requiredDefinesMissing",
                    i18nArgs);
            throw new SCException(i18nMsg);
        }

        clustername = System.getProperty(PROP_SCK_CLNAME);
        logger.info("SccheckClient.checkRequiredDefines() " + "clustername: " +
            clustername);
        clientName = System.getProperty(PROP_SCK_LOCALNAME);
        logger.info("SccheckClient.checkRequiredDefines() clientName: " +
            clientName);
        privateClientName = System.getProperty(PROP_SCK_PRIVLOCALNAME);
        logger.info(
            "SccheckClient.checkRequiredDefines() private clientName: " +
            privateClientName);

        // convert String: 'name,name,...' to String[]
        String tmp;
        tmp = System.getProperty(PROP_SCK_PUBLICNAMES);
        clusterPublicNames = preProcessNodeNames(tmp);
        tmp = System.getProperty(PROP_SCK_PRIVATENAMES);
        clusterPrivateNames = preProcessNodeNames(tmp);

        reportsDir = System.getProperty(PROP_SCK_OUTPUTDIR);
        explorersgzDir = System.getProperty(PROP_SCK_EXPLGZDIR);
        explorersUnpackDir = System.getProperty(PROP_SCK_EXPLUNPACKDIR);

        inClusterMode = Boolean.getBoolean(PROP_SCK_CLMODE);
        logger.info("SccheckClient.checkRequiredDefines() " +
            "inClusterMode: " + inClusterMode);

        logger.info("SccheckClient.checkRequiredDefines() " + "gunzip: " +
            System.getProperty(PROP_SCK_GUNZIP));

        keLogName = System.getProperty(PROP_KE_CLIENTLOG);
        logger.info("SccheckClient.checkRequiredDefines() " + "keLogName: " +
            keLogName);

        xslDir = System.getProperty(PROP_KE_CLIENT_XSLDIR);
        logger.info("SccheckClient.checkRequiredDefines() " + "xslDir: " +
            xslDir);

    } // checkRequiredDefines


    /*
     * loadOptionalDefines()
     *
     * Collect values that may have been delivered by sccheck.ksh
     *      and assign to class members.
     */
    private void loadOptionalDefines() throws SCException {
        String s;
        Integer intVal;
        Boolean boolVal;


        brief = Boolean.getBoolean(PROP_SCK_BRIEF);
        logger.info("SccheckClient.loadOptionalDefines() brief: " + brief);

        verbose = Boolean.getBoolean(PROP_SCK_VERBOSE);
        logger.info("SccheckClient.loadOptionalDefines() verbose: " + verbose);

        vverbose = Boolean.getBoolean(PROP_SCK_VVERBOSE);
        logger.info("SccheckClient.loadOptionalDefines() vverbose: " +
            vverbose);

        hostlist = System.getProperty(PROP_SCK_HOSTLIST);
        logger.info("SccheckClient.loadOptionalDefines() hostlist: " +
            hostlist);

        minSeverity = 0;

        if ((intVal = Integer.getInteger(PROP_SCK_SEVERITY)) != null) {
            minSeverity = intVal.intValue();
        }

        logger.info("SccheckClient.loadOptionalDefines() " + "minSeverity: " +
            minSeverity);

        s = System.getProperty(PROP_SCK_SINGLE_RULEFILE);

        if (s != null) {
            singleRuleFilePath = s;
            logger.info("SccheckClient.loadOptionalDefines() " +
                "single mode rule file path: " + singleRuleFilePath);
        }

        s = System.getProperty(PROP_SCK_MULTI_RULEFILE);

        if (s != null) {
            multiRuleFilePath = s;
            logger.info("SccheckClient.loadOptionalDefines() " +
                "multi mode rule file path: " + multiRuleFilePath);

        }

        explorerFilesPath = System.getProperty(PROP_SCK_EXPLFILES);
        logger.info("SccheckClient.loadOptionalDefines() " +
            "explorer input files path: " + explorerFilesPath);

        if (explorerFilesPath != null) {

            // Check if the explorer file exists.
            File explorerfile = new File(explorerFilesPath);

            if (explorerfile.exists() == false) {
                Object i18nArgs[] = { explorerFilesPath };
                String i18nMsg = I18n.getLocalized("explorerFileNotFound",
                        i18nArgs);
                throw new SCException(i18nMsg);
            }
        }
    } // loadOptionalDefines


    /*
     * expandHostList()
     *
     * Create sessionPublic and sessionPrivate name arrays
     * as appropriate for cluster or non-cluster mode.
     */
    private void expandHostlist() throws SCException {

        if (inClusterMode) {
            expandHostlistClusterMode();
        } else {
            expandHostlistNonClusterMode();
        }

        logger.info("SccheckClient.expandHostlist() " +
            "-- sessionPublicNames: " +
            Utils.dumpStringArray(sessionPublicNames));
        logger.info("SccheckClient.expandHostlist() -- " +
            "sessionPrivateNames: " +
            Utils.dumpStringArray(sessionPrivateNames));

    } // expandHostlist


    /*
     * remoteOperations()
     *
     * Scatter-gather:
     *
     *      launch a ClientThread per host (server).
     *      wait for threads to finish.
     *
     * Conditions when all threads have returned successfully:
     *      zippedExplorers holds names of all explorer results
     *      all reports in reportsDir
     *      genDates[] filled
     *      maxServerExitCode holds maxSeverity of all single-node runs
     *
     * If any thread reports failure:
     *      clientThreadFailed set to true
     *      failedNodes holds list of failed nodenames
     *
     */
    private void remoteOperations(String publicNames[], String privateNames[],
        String explorerArchivePath) throws SCException {

        logger.info("SccheckClient.remOps(): " +
            Utils.dumpStringArray(publicNames) + " / " +
            Utils.dumpStringArray(publicNames));

        if (explorerArchivePath != null) {
            this.postProgress(VERBOSE,
                "preparing to run checks" +
                "against specified explorer archive" + explorerArchivePath);
        }

        numHosts = publicNames.length;
        genDates = new String[numHosts];

        ClientThread threads[] = new ClientThread[numHosts];

        for (int i = 0; i < numHosts; i++) {
            String publicName = publicNames[i];
            String privateName = privateNames[i];
            String resultsFilename = explorersgzDir + "/" + publicName +
                ".expl.gzip";
            String reportFilename = reportsDir + "/" + CLIENT_REPORTNAME + "." +
                publicName;
            logger.info("SccheckClient.remOps() reportsDir: " + reportsDir);
            logger.info("SccheckClient.remOps() " + "CLIENT_REPORTNAME: " +
                CLIENT_REPORTNAME);
            logger.info("SccheckClient.remOps() reportFilename: " +
                reportFilename);
            logger.info("SccheckClient.remOps() new thread for: " + publicName +
                "/" + privateName);

            if (!explorerArchiveSpecified) {
                Object i18nArgs[] = { publicName };
                String progStr = I18n.getLocalized(
                        "requestingExplorerAndNodeReportFrom", i18nArgs);
                this.postProgress(VERBOSE, progStr);
            }

            threads[i] = new ClientThread(this, this, publicName, privateName,
                    resultsFilename, reportFilename, brief, minSeverity,
                    singleRuleFilePath, explorerArchivePath);
            threads[i].start();
        } // for hosts

        try {
            waitForClientThreads(threads);
        } catch (InterruptedException iex) {
            logger.error("SccheckClient.remOps() " + "InterruptedException");

            String i18nMsg = I18n.getLocalized(
                    "interruptionBeforeRequestsCompleted");
            throw new SCException(i18nMsg);
        }

        if (clientThreadFailed) {
            logger.error("SccheckClient.remOps() failedNodes: " + failedNodes);
            throw new SCException(failedNodes); // noI18n
        }

        logger.trace("SccheckClient.remOps() -- EXIT -- ");
    } // remoteOperations


    /*
     * runKE()
     *
     * Create and run KEWrapper with multi-node checks against explorer
     * results from all nodes.
     *
     * Remove the unpacked explorer results.
     *
     * At method conclusion reports are in reportsDir.
     */
    private int runKE(Vector zippedExplorers, String checkListPath)
        throws WrapperException {
        logger.trace("SccheckClient.runKE() --ENTER--");

        String reportNameBase = reportsDir + "/" + CLIENT_REPORTNAME + "." +
            clustername;
        int maxSeverity = 0;
        String progStr = I18n.getLocalized("startingMultiNodeChecks");
        this.postProgress(VERBOSE, progStr);
        logger.trace("SccheckClient.runKE() about to make KEWrapper");

        logger.info("SccheckClient.runKE() explorersUnpackDir: " +
            explorersUnpackDir);

        KEWrapper keWrapper = null;

        try {
            keWrapper = new KEWrapper(this, reportNameBase, zippedExplorers,
                    explorersUnpackDir, brief, minSeverity, keLogName, xslDir);
            keWrapper.runKE(checkListPath);
            progStr = I18n.getLocalized("multiNodeChecksFinished");
            this.postProgress(VERBOSE, progStr);
            keWrapper.removeUnpackedExplorers();
        } catch (WrapperException wex) {
            logger.error("SccheckClient.runKE() " +
                "in WrapperException wex: " + wex.getMessage());
            throw new WrapperException(wex.getMessage());
        }

        maxSeverity = keWrapper.getMaxSeverity();
        logger.trace("SccheckClient.runKE() --EXIT: " + maxSeverity);

        return maxSeverity;
    } // runKE


    /*
     * compareGenDates
     *
     * Compare the generation dates from checklists used for single-node
     * reports on servers.
     * If any are different issue a warning.
     */
    private void compareGenDates() {
        logger.trace("SccheckClient.compareGenDates() -- ENTER -- ");

        String initial = genDates[0];

        // true, there's no need to include index 0 in the loop
        // but including it accomodates a 'single
        // host' session without
        // special handling elsewhere
        for (int i = 0; i < genDates.length; i++) {

            if (!initial.equals(genDates[i])) {
                warnGenDates();

                break;
            }
        }

        logger.trace("SccheckClient.compareGenDates() -- EXIT -- ");
    } // compareGenDates


    /*
     * getExitCode()
     *
     * Called by main() to get return code for ordinary exit.
     *
     * Return the highest of all returned server codes and the
     * multi-report code.
     * Codes are the highest severity seen among all failed checks.
     */
    public int getExitCode() {

        if (getMaxServerExitCode() >= getMultiReportExitCode()) {
            return getMaxServerExitCode();
        } else {
            return getMultiReportExitCode();
        }
    } // getExitCode


    /*
     * earlyExit()
     *
     * Print the message to stderr and exit the JVM with the specified exit
     * code.
     */
    private void earlyExit(int exitCode) {
        earlyExit(exitCode, null);
    }

    private void earlyExit(int exitCode, String msg) {
        // msg already I18n if appropriate

        String errKey = "";

        switch (exitCode) {

        case EXITPermERR:
            errKey = "permissionDenied";

            break;

        case EXITComponentERR:
            errKey = "componentError";

            break;

        case EXITUsageERR: // should be caught by ksh
            errKey = "internalInvocationError";

            break;

        case EXITConfigERR:
            errKey = "unableToLoadClusterData";

            break;

        case EXITBadNameERR:

            if (inClusterMode) {
                errKey = "noClusterNodeNameMatching";
            } else {
                errKey = "hostlistNotAllowedInNonClusterMode";
            }

            break;

        case EXITNodeERR:
            errKey = "unableToRunChecksOn";

            break;

        case EXITFrameERR:
            errKey = "internalError";

            break;

        default:
            errKey = "unknownError";

            break;
        }

        // show msg to user
        String prog_name = System.getProperty(PROGNAME);

        if (prog_name == null) {
            prog_name = "sccheck";
        }

        Object i18nArgs[] = { prog_name, I18n.getLocalized(errKey), msg };
        String i18nMsg = I18n.getLocalized("earlyExitFormat", i18nArgs);
        System.err.println(i18nMsg);

        logger.info("SccheckClient.earlyExit() called with code " + exitCode +
            " (" + msg + ")");
        logger.close();

        System.exit(exitCode);
    } // earlyExit


    // -----  start interface Client -----

    public synchronized void addZippedExplorer(String s) {
        zippedExplorers.addElement(s);
    } // addExplorerResults

    public synchronized void postFailure(String nodename) {
        clientThreadFailed = true;

        if (failedNodes == null) {
            failedNodes = nodename;
        } else {
            failedNodes = failedNodes + "," + nodename;
        }
    } // postFailure

    public synchronized void addServerExitCode(String nodename, int i) {
        logger.info("SccheckClient.addServerExitCode(): " + i + " from " +
            nodename);

        if (i > maxServerExitCode) {
            maxServerExitCode = i;
        }

        logger.info("SccheckClient.addServerExitCode() " +
            "maxServerExitCode: " + maxServerExitCode);
    } // addServerExitCode

    public synchronized void addGenDate(String publicName, String gd) {
        logger.info("SccheckClient.addGenDate(): " + gd + " from " +
            publicName);

        // find publicName in sessionPublicNames
        // set same index in genDates[] to gd
        boolean match = false;

        for (int i = 0; i < sessionPublicNames.length; i++) {

            if (sessionPublicNames[i].equals(publicName)) {
                genDates[i] = gd;
                match = true;
                logger.info("SccheckClient.addGenDate() " + "match: " +
                    publicName);

                break;
            }
        }

        if (match == false) {
            logger.error("SccheckClient.addGenDate() " + "couldn't match " +
                publicName + " in sessionPublicNames[]");
        }

        logger.trace("SccheckClient.addGenDate() -- EXIT --");
    } // addGenDate


    public String getLocaleLang() {
        return Locale.getDefault().getLanguage();
    } // getLocaleLang

    public String getLocaleCountry() {
        return Locale.getDefault().getCountry();
    } // getLocaleCountry

    public String getLocaleVariant() {
        return Locale.getDefault().getVariant();
    } // getLocaleVariant

    public String getAuth() {
        return null;
    } // getAuth

    public String getPrivateClientHostname() {
        return privateClientName;
    } // getPrivateClientHostname

    public String getClusterName() {
        return clustername;
    } // getClusterName

    // -----  end interface Client -----

    // -----  start interface ProgressListener -----

    public synchronized void postProgress(int verboseLevel, String str) {
        // user can block these messages
        // str already I18n if appropriate

        logger.info("postProgress: (verbose: " + verboseLevel + ") " + str);

        if ((verboseLevel == QUIET) || (vverbose == true) ||
                ((verboseLevel == VERBOSE) && (verbose == true))) {
            String prog_name = System.getProperty(PROGNAME);

            if (prog_name == null) {
                prog_name = "sccheck";
            }

            Object i18nArgs[] = { prog_name, str };
            String msg = I18n.getLocalized("postProgressFormat", i18nArgs);
            System.out.println(msg);

        }
    } // postProgress

    public synchronized void postErrMsg(String str) {
        // user cannot block these messages
        // str already I18n if appropriate

        String prog_name = System.getProperty(PROGNAME);

        if (prog_name == null) {
            prog_name = "sccheck";
        }

        logger.info("postErrMsg: " + str);

        Object i18nArgs[] = { prog_name, str };
        String i18nMsg = I18n.getLocalized("postErrMsgFormat", i18nArgs);
        System.err.println(i18nMsg);

    } // postErrMsg

    // -----  end interface ProgressListener -----


    // -----  helper methods -----


    /*
     * preProcessNodeNames()
     *
     * Given a string of the form name[,name...] split on commas
     *      and return names in a String array.
     */

    private String[] preProcessNodeNames(String namelist) {
        StringTokenizer st = new StringTokenizer(namelist, ",");
        int ct = st.countTokens();
        int index = 0;

        String dest[] = new String[ct];

        while (st.hasMoreTokens()) {
            dest[index++] = st.nextToken();
        }

        return dest;
    } // preProcessNodeNames

    /*
     * expandExplorerList()
     *
     * expand a explorer input file list
     * given by the user in a form name[,name,...]
     * and fill zippedExplorers vector witj list element
     */

    private void expandExplorerList() {
        StringTokenizer st;
        String str;

        if (!zippedExplorers.isEmpty()) {
            zippedExplorers.clear();
        }

        logger.trace("SccheckClient() > " + "expanding explorer archives list");

        st = new StringTokenizer(explorerFilesPath, ",");

        while (st.hasMoreTokens()) {
            str = st.nextToken();

            if (Utils.stringIsUnique(zippedExplorers, str)) {
                logger.trace("SccheckClient() > adding " + str);
                zippedExplorers.add(str);
            }
        }

    }


    /*
     * expandHostlistClusterMode()
     *
     * Create and fill class members sessionPublicNames &
     * sessionPrivateNames:
     *
     * if no user-specified hostlist
     *      default to all current cluster members
     * else
     *      validate hostlist:
     *              strip out any duplicate names
     *              put hostlist names into sessionPublic array
     *              throw exception if a hostname is not a
     *                      current cluster member
     *              match public to private name and enter in
     *                      sessionPrivate array
     */
    private void expandHostlistClusterMode() throws SCException {
        int ct = 0;

        if (hostlist == null) {

            // default to all current cluster members
            ct = clusterPublicNames.length;
            sessionPublicNames = new String[ct];
            sessionPrivateNames = new String[ct];

            for (int i = 0; i < ct; i++) {
                sessionPublicNames[i] = clusterPublicNames[i];
                sessionPrivateNames[i] = clusterPrivateNames[i];
            } // for
        } else {

            // validate hostlist
            StringTokenizer st;
            String str;

            // first strip out dups:
            // use vector method for convenience
            StringBuffer strippedList = null;
            st = new StringTokenizer(hostlist, ",");

            Vector v = new Vector();

            while (st.hasMoreTokens()) {
                str = st.nextToken();

                if (!v.contains(str)) {
                    v.add(str);

                    if (strippedList == null) {
                        strippedList = new StringBuffer();
                    } else {
                        strippedList.append(",");
                    }

                    strippedList.append(str);
                }
            }

            /*
             * now operate on stripped list:
             * put user-specified hostnames into array
             */
            st = new StringTokenizer(strippedList.toString(), ",");
            ct = st.countTokens();

            String userPublicNames[] = new String[ct];
            sessionPublicNames = new String[ct];
            sessionPrivateNames = new String[ct];

            for (int i = 0; st.hasMoreTokens(); i++) {
                String nm = st.nextToken();
                userPublicNames[i] = nm;
            }

            /*
             * test for hostlist names in clusterPublicNames
             * if not found then not cluster member: exception
             * if found then find corresponding private name
             */
            for (int i = 0; i < sessionPublicNames.length; i++) {
                boolean matched = false;

                for (int j = 0; j < clusterPublicNames.length; j++) {

                    if (userPublicNames[i].equals(clusterPublicNames[j])) {
                        sessionPublicNames[i] = userPublicNames[i];
                        sessionPrivateNames[i] = clusterPrivateNames[j];
                        matched = true;

                        break;
                    }
                }

                if (!matched) {
                    logger.error("SccheckClient." +
                        "expandHostlistClusterMode() " +
                        "hostname not a cluster member: " + userPublicNames[i]);
                    throw new SCException(userPublicNames[i]); // noI18n
                }
            } // for userPublicNames
        } // validate hostlist
    } // expandHostlistClusterMode


    /*
     * expandHostlistNonClusterMode()
     *
     * Create and fill class members sessionPublicNames &
     * sessionPrivateNames:
     *
     * User specified hostlist not allowed in non-cluster
     * mode: exception.
     *
     * May establish session only to server on same machine:
     *      assign clientName as public name.
     *      assign "localhost" as private name.
     */
    private void expandHostlistNonClusterMode() throws SCException {

        if (hostlist != null) {
            throw new SCException(hostlist); // no I18N
        } else {
            sessionPublicNames = new String[] { clientName };
            sessionPrivateNames = new String[] { "localhost" }; // no I18N
        }
    } // expandHostlistNonClusterMode


    /*
     * waitForClientThreads()
     *
     * Wait for all ClientThreads to return.
     */
    private void waitForClientThreads(ClientThread threads[])
        throws InterruptedException {
        logger.trace("SccheckClient.waitForClientThreads() " + "-- ENTER -- ");

        int numthreads = threads.length;

        try {

            for (int i = 0; i < numthreads; i++) {
                threads[i].join();
            }
        } catch (InterruptedException iex) {
            logger.error("SccheckClient.waitForClientThreads() " +
                "InterruptedException");

            for (int i = 0; i < numthreads; i++) {
                threads[i].die();
            }

            throw iex;
        }

        logger.trace("SccheckClient.waitForClientThreads() " + "-- EXIT -- ");
    } // waitForClientThreads


    /*
     * warnGenDates()
     *
     * Display an 'unmaskable' message to user.
     */
    private void warnGenDates() {
        logger.trace("SccheckClient.warnGenDates() -- ENTER -- ");

        postProgress(QUIET, I18n.getLocalized("warnGenDates"));

        // make sure user knows...
        if (!verbose && !vverbose) {
            postProgress(QUIET, I18n.getLocalized("rerunWithVVForMoreDetails"));
        }

        // show all gen dates to user
        for (int i = 0; i < genDates.length; i++) {
            Object i18nArgs[] = { sessionPublicNames[i] + ":", genDates[i] };
            String i18nMsg = I18n.getLocalized("showGenDate", i18nArgs);
            postProgress(VERBOSE, i18nMsg);
        }

        logger.trace("SccheckClient.warnGenDates() -- EXIT -- ");
    } // warnGenDates


    /*
     * getMultiReportExitCode()
     * getMaxServerExitCode()
     *
     * Convenience methods
     */
    private int getMultiReportExitCode() {
        return multiReportExitCode;
    } // getMultiReportExitCode

    private int getMaxServerExitCode() {
        return maxServerExitCode;
    } // getMaxServerExitCode


    // -----  end helper methods -----


    // #####################   MAIN   #############################

    public static void main(String args[]) {
        SccheckClient c = new SccheckClient();
        int exitVal = c.getExitCode();
        System.exit(exitVal);
    } // main

} // SccheckClient
