/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ssm_nafo.c - makes calls to pnm to get information about IPMP groups
 */

#pragma ident	"@(#)ssm_ipmp.c	1.16	08/07/31 SMI"

#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <syslog.h>
#include <scha.h>
#include <rgm/pnm.h>
#include "ssm_util.h"

extern ssm_prop_err_t get_ipmp_groups_from_rlist(
    ssm_net_resource_list_t *snrl, pnm_group_list_t *ipmp_groups, char *zname);

/*
 * Return B_TRUE if there are one or more IPMP groups on this node.
 * Otherwise, return B_FALSE.
 */
boolean_t
local_node_has_nafo_group(void)
{
	int			rc;
	pnm_group_list_t	all_nafo_groups = NULL;
	boolean_t		bool_rc = B_FALSE;

	/* Get all IPMP groups */
	rc = pnm_group_list(&all_nafo_groups);
	if (rc != 0 || all_nafo_groups == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * An unexpected error occurred while trying to communicate
		 * with the network monitoring daemon (cl_pnmd).
		 * @user_action
		 * Make sure the network monitoring daemon (cl_pnmd) is running.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to get all IPMP groups (request failed with %d).",
		    rc);
		/* report that there are no IPMP groups */
		return (B_FALSE);
	}

	/*
	 * pnm passes back the empty string if there are no IPMP groups,
	 * so if the string is something other than the empty string, we
	 * assume that there are valid IPMP groups on this node.
	 */
	if (strcmp(all_nafo_groups, "") != 0) {
		bool_rc = B_TRUE;
	}

	free(all_nafo_groups);
	return (bool_rc);
}

/*
 * Register a callback for ALL IPMP groups on this node.
 *
 * If any IPMP group transitions to the DOWN state, we will
 * receive a callback and assume that the scalable service
 * cannot respond to client requests.
 *
 * This function returns void because even when we encounter failure,
 * we treat it as success. We do this work at MONITOR_START time and want
 * to start the real RT monitor even if we cannot register the callbacks
 * correctly.  Therefore, regardless of any failure we encounter below, it
 * will be considered as success.
 */
void
ssm_register_nafo_callback(char *rs, char *rg, char *rt,
    ssm_properties_t *ssm_props, char *zname)
{
	int			rc;
	pnm_group_list_t	all_nafo_groups = NULL;
	pnm_group_list_t	group_ptr = NULL;
	pnm_group_t		nafo_group = NULL;
	char			tag[SSM_NAFO_TAGSIZE];
	char			cmd[SSM_NAFO_CMDSIZE];

	if (pnm_param_isset(PNM_PARAM_MONALL)) {
		/* Get all IPMP groups */
		rc = pnm_group_list(&all_nafo_groups);
	} else {
		/* Get only IPMP groups the resource depends on */
		rc = get_ipmp_groups_from_rlist(
			ssm_props->ssm_net_res_data, &all_nafo_groups, zname);
	}
	/* all_nafo_groups is a list of group names. Check it's nonempty */
	if (rc != 0 || all_nafo_groups == NULL ||
	    strlen(all_nafo_groups) == 0) {
		/* We don't propogate the failure to the caller */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to get all IPMP groups (request failed with %d).",
		    rc);
		return;
	}

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "The query for all IPMP groups returned %s.",
	    all_nafo_groups);

	/*
	 * XXX Bug 4324175: need larger buffer for registering callbacks in pnm
	 * will cause callback to be registered incorrectly if the
	 * RGM object names are too long.
	 */
	if (zname == NULL) {
		(void) snprintf(cmd, sizeof (cmd), "%s/%s %s %s %s",
		    SSM_CALLBACK_DIR, SSM_CALLBACK_NAME, rg, rs, rt);
	} else {
		(void) snprintf(cmd, sizeof (cmd), "%s/%s %s %s %s %s",
		    SSM_CALLBACK_DIR, SSM_CALLBACK_NAME, zname, rg, rs, rt);
	}

	/*
	 * Parse out the names of each IPMP group. The list is ':' delimited.
	 * For each IPMP group, register a callback for this resource.
	 */
	group_ptr = all_nafo_groups;
	while ((nafo_group = strtok(group_ptr, SSM_NAFO_DELIMITER)) != NULL) {
		/* For repeated calls to strtok */
		group_ptr = NULL;

		(void) snprintf(tag, sizeof (tag), "%s.%s.%s",
		    SSM_CALLBACK_NAME, rs, nafo_group);

		rc = pnm_callback_reg(nafo_group, tag, cmd);
		if (rc != 0) {
			/* We don't propogate the failure to the caller */
			/*
			 * SCMSGS
			 * @explanation
			 * An unexpected error occurred while trying to
			 * communicate with the network monitoring daemon
			 * (cl_pnmd).
			 * @user_action
			 * Make sure the network monitoring daemon (cl_pnmd) is
			 * running.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Failed to register callback for IPMP group %s "
			    "with tag %s and callback command %s "
			    "(request failed with %d).",
			    nafo_group,
			    tag,
			    cmd,
			    rc);
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Registered callback for IPMP group %s with "
			    "tag %s and callback command %s.",
			    nafo_group,
			    tag,
			    cmd);
		}
	}
}

/*
 * Un-register all callbacks for this resource.
 *
 * This function returns void because even when we encounter failure,
 * we treat it as success. We do this work at MONITOR_STOP time and want
 * to stop the real RT monitor even if we cannot un-register the callbacks
 * correctly.  Therefore, regardless of any failure we encounter below, it
 * will be considered as success. Also, we want to make sure we avoid the
 * STOP_FAILED state for this resource.
 */
void
ssm_unregister_nafo_callback(char *rs)
{
	int			rc;
	pnm_group_list_t	all_nafo_groups = NULL;
	pnm_group_list_t	group_ptr = NULL;
	pnm_group_t		nafo_group = NULL;
	char			tag[SSM_NAFO_TAGSIZE];

	/* Get all IPMP groups */
	rc = pnm_group_list(&all_nafo_groups);
	if (rc != 0 || all_nafo_groups == NULL) {
		/* We don't propogate the failure to the caller */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to get all IPMP groups (request failed with %d).",
		    rc);
		return;
	}

	syslog(LOG_DEBUG,
	    "The query for all IPMP groups returned %s.",
	    all_nafo_groups);

	/*
	 * Parse out the names of each IPMP group. The list is ':' delimited.
	 * For each IPMP group, unregister the callback for this resource.
	 */
	group_ptr = all_nafo_groups;
	while ((nafo_group = strtok(group_ptr, SSM_NAFO_DELIMITER)) != NULL) {
		/* For repeated calls to strtok */
		group_ptr = NULL;

		(void) snprintf(tag, sizeof (tag), "%s.%s.%s",
		    SSM_CALLBACK_NAME, rs, nafo_group);

		rc = pnm_callback_unreg(nafo_group, tag);
		if (rc != 0) {
			/* We don't propogate the failure to the caller */
			/*
			 * SCMSGS
			 * @explanation
			 * An unexpected error occurred while trying to
			 * communicate with the network monitoring daemon
			 * (cl_pnmd).
			 * @user_action
			 * Make sure the network monitoring daemon (cl_pnmd) is
			 * running.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Failed to unregister callback for IPMP group %s "
			    "with tag %s (request failed with %d).",
			    nafo_group,
			    tag,
			    rc);
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Unregistered callback for IPMP group %s with "
			    "tag %s.",
			    nafo_group,
			    tag);
		}
	}
}

/*
 * Check the status for all IPMP groups. If any IPMP group is in
 * the DOWN state, return non-zero.
 */
int
ssm_check_all_nafogroups(void)
{
	int			rc;
	pnm_group_list_t	all_nafo_groups = NULL;
	pnm_group_list_t	group_ptr = NULL;
	pnm_group_t		nafo_group = NULL;
	pnm_status_t		group_status;
	hrtime_t		curr_time;
	hrtime_t		loop_timeout;
	boolean_t		all_groups_ok = B_TRUE;

	/*
	 * We will spend a maximum of SSM_NAFO_STATUS_TIMEOUT seconds
	 * in this loop.
	 *
	 * The times below are in nanoseconds. The SSM_NAFO_STATUS_TIMEOUT
	 * is in seconds, so we must convert it.
	 */
	curr_time = gethrtime();
	loop_timeout = SSM_NAFO_STATUS_TIMEOUT;   /* time in seconds to wait */
	loop_timeout = loop_timeout * 1000000000; /* convert to nanoseconds */
	loop_timeout = loop_timeout + curr_time;  /* add to get final timeout */

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "Checking IPMP status. Loop will timeout in %d seconds.",
	    SSM_NAFO_STATUS_TIMEOUT);

	for (; curr_time < loop_timeout; curr_time = gethrtime()) {
		/*
		 * Get all IPMP groups. We do this in the loop because
		 * we may spend a couple of minutes trying to check the
		 * status of all IPMP groups. If the set changes over the
		 * course of those minutes, we want to avoid assuming
		 * failure because we have out of date information.  Of
		 * course, there is still a window where this info can be
		 * wrong, but it's much smaller if we re-get the information
		 * each time just before we use it.
		 */
		free(all_nafo_groups);
		rc = pnm_group_list(&all_nafo_groups);
		if (rc != 0 || all_nafo_groups == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Failed to get all IPMP groups "
			    "(request failed with %d).",
			    rc);
			return (1);
		}

		syslog(LOG_DEBUG,
		    "The query for all IPMP groups returned %s.",
		    all_nafo_groups);

		group_ptr = all_nafo_groups;
		all_groups_ok = B_TRUE;

		/*
		 * Parse out the names of each IPMP group.
		 * The list is ':' delimited.
		 * For each IPMP group, get the status of the IPMP group.
		 */
		while ((nafo_group = strtok(group_ptr, SSM_NAFO_DELIMITER)) !=
		    NULL) {
			/* For repeated calls to strtok */
			group_ptr = NULL;

			rc = pnm_group_status(nafo_group, &group_status);
			if (rc != 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * A query to get the state of a IPMP group
				 * failed. This may cause a method failure to
				 * occur.
				 * @user_action
				 * Make sure the network monitoring daemon
				 * (cl_pnmd) is running. Save a copy of the
				 * /var/adm/messages files on all nodes.
				 * Contact your authorized Sun service
				 * provider for assistance in diagnosing the
				 * problem.
				 */
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_ERROR, MESSAGE,
				    "Failed to get IPMP status for group %s "
				    "(request failed with %d).",
				    nafo_group,
				    rc);
				free(all_nafo_groups);
				return (1);
			}

			/* See pnm.h for all possible status values */
			switch (group_status.status) {
				case PNM_STAT_OK:
					syslog(LOG_DEBUG,
					    "IPMP group %s has status %s.",
					    nafo_group,
					    pnm_status_str(
						group_status.status));
					break;

				case PNM_STAT_DOWN:
					/* Node cannot respond to clients */
					/*
					 * SCMSGS
					 * @explanation
					 * The state of the IPMP group named
					 * is degraded.
					 * @user_action
					 * Make sure all adapters and cables
					 * are working. Look in the
					 * /var/adm/messages file for message
					 * from the network monitoring daemon
					 * (cl_pnmd).
					 */
					(void) sc_syslog_msg_log(log_handle,
					    SC_SYSLOG_ERROR, MESSAGE,
					    "IPMP group %s has status %s. "
					    "Assuming this node cannot "
					    "respond to client requests.",
					    nafo_group,
					    pnm_status_str(
						group_status.status));
					free(all_nafo_groups);
					pnm_status_free(&group_status);
					return (1);
				default:
					/* Don't treat this as fatal error */
					/*
					 * SCMSGS
					 * @explanation
					 * The status of the IPMP group is not
					 * among the set of statuses that is
					 * known.
					 * @user_action
					 * Save a copy of the
					 * /var/adm/messages files on all
					 * nodes. Contact your authorized Sun
					 * service provider for assistance in
					 * diagnosing the problem.
					 */
					(void) sc_syslog_msg_log(log_handle,
					    SC_SYSLOG_ERROR, MESSAGE,
					    "IPMP group %s has unknown status "
					    "%d. Skipping this IPMP group.",
					    nafo_group,
					    group_status.status);
					break;
			}

			pnm_status_free(&group_status);
		} /* while there are more IPMP groups in the list */

		if (all_groups_ok == B_TRUE) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "All IPMP groups are in a healthy state.");
			free(all_nafo_groups);
			return (0);
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "All IPMP groups are not in a healthy state. "
			    "Will check status again.");
		}

	} /* for curr_time until loop_timeout */

	/* We timed out in the for loop */
	/*
	 * SCMSGS
	 * @explanation
	 * The state of the IPMP groups on the node could not be determined.
	 * @user_action
	 * Make sure all adapters and cables are working. Look in the
	 * /var/adm/messages file for message from the network monitoring
	 * daemon (cl_pnmd).
	 */
	(void) sc_syslog_msg_log(log_handle,
	    SC_SYSLOG_ERROR, MESSAGE,
	    "Failed to verify that all IPMP groups are in a stable state. "
	    "Assuming this node cannot respond to client requests.");
	free(all_nafo_groups);
	return (1);
}

ssm_prop_err_t
get_ipmp_groups_from_rlist(
    ssm_net_resource_list_t *snrl, pnm_group_list_t *ipmp_groups, char *zname)
{
	uint_t			i, k;
	char			*rname;
	uint_t			this_node;
	char			groupbuf[200], *nodeidp;
	scha_err_t		e;
	scha_resource_t		rs_handle;
	scha_extprop_value_t	*extprop = NULL;
	scha_str_array_t	*strarray;

	this_node = get_local_nodeid();
	groupbuf[0] = '\0';

	for (i = 0; i < snrl->snrl_num_snr; i++) {
		rname = snrl->snrl_snr_list[i].snr_name;
retry_rs:
		e = scha_resource_open_zone(zname, rname, NULL, &rs_handle);
		if (e != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Failed to retrieve the resource handle for %s "
			    "while querying for property %s: %s.",
			    rname,
			    SHARED_ADDRESS_NETIFLIST,
			    get_scha_error_string(e));
			exit(1);
		}

		e = scha_resource_get_zone(zname, rs_handle, SCHA_EXTENSION,
			SHARED_ADDRESS_NETIFLIST, &extprop);
		if (e == SCHA_ERR_SEQID) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_NOTICE, MESSAGE,
			    "Retrying to retrieve the resource information.");
			(void) scha_resource_close(rs_handle);
			rs_handle = NULL;
			(void) sleep(1);
			goto retry_rs;
		} else if (e != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Failed to retrieve the resource property %s "
			    "for %s: %s.",
			    SHARED_ADDRESS_NETIFLIST,
			    rname,
			    get_scha_error_string(e));
			exit(1);
		}

		strarray = extprop->val.val_strarray;
		for (k = 0; k < strarray->array_cnt; k++) {
			if (strarray->str_array[k] != NULL && (nodeidp =
			    strchr(strarray->str_array[k], '@')) != NULL &&
			    atoi(nodeidp + 1) == this_node) {
				if (groupbuf[0] != '\0')
					(void) strcat(groupbuf, ":");
				*nodeidp = '\0';
				(void) strcat(groupbuf, strarray->str_array[k]);
			}
		}

		(void) scha_resource_close(rs_handle);
	}

	*ipmp_groups = strdup(groupbuf);
	return (0);
}
