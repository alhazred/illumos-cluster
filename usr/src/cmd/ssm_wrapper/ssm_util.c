/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ssm_util.c - code for collecting properties need to make the ssm call
 */

#pragma ident	"@(#)ssm_util.c	1.44	09/04/15 SMI"

#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>
#include <locale.h>
#include <scha.h>
#include "ssm_util.h"

/*
 * Supress lint info about limit of 511 'external identifiers' exceed
 */
/*lint -e793 */

static ssm_prop_err_t get_string_property_from_args(char *propname, int argc,
    char *argv[], char **propval);
static ssm_prop_err_t get_string_property(char *rs, char *rg, char *zname,
    char *propname, validate_mode_t vmode, int argc, char *argv[],
    char **propval);
static ssm_prop_err_t get_int_property(char *rs, char *rg, char *zname,
    char *propname, validate_mode_t vmode, int argc, char *argv[],
    int *propval);
static ssm_prop_err_t get_bool_property(char *rs, char *rg, char *zname,
    char *propname, validate_mode_t vmode, int argc, char *argv[],
    boolean_t *propval);

static ssm_prop_err_t get_stringarray_property_from_args(char *propname,
    int argc, char *argv[], scha_str_array_t **propval);
static ssm_prop_err_t get_stringarray_property(char *rs, char *rg, char *zname,
    char *propname, validate_mode_t vmode, int argc, char *argv[],
    scha_str_array_t **propval);
static void print_stringarray_property(char *propname, scha_str_array_t *prop);
static ssm_prop_err_t get_stringarray_property_from_rg(char *rg,
    char *zname, char *propname,
    validate_mode_t vmode, int argc, char *argv[], scha_str_array_t **propval);

static ssm_prop_err_t get_lb_policy_from_string(char *policy_string,
    scha_ssm_policy_t *policy_code);

static char internal_err_str[INT_ERR_MSG_SIZE];

#ifdef SCTP_RGM_SUPPORT
static ssm_prop_err_t
process_net_resources(struct ssm_net_resource_list *net_resource);
#endif

static ssm_prop_err_t process_weight_info(scha_str_array_t *weights_array,
    ssm_nodeweight_t **weights, uint_t *numweights, boolean_t is_rr_enabled,
    char *zname);
static void print_weights(ssm_nodeweight_t *weights, uint_t numweights);

static ssm_prop_err_t process_port_info(scha_str_array_t *ports_array,
    ssm_port_and_proto_t **ports, uint_t *numports);
static void print_ports(ssm_port_and_proto_t *ports, uint_t numports);

static void print_nodelist(ssm_node_list_t *nodelist);

static int str_kv(char *s, char **key, char **val);
static int str_to_strarray(char *val, scha_str_array_t **sa);
static scha_str_array_t *strarraydup(scha_str_array_t *sa);
static void free_strarray(scha_str_array_t *sa);

char *normalize_nodename(const char *nodename);

/*
 * This is the set of properties that the user cannot update after resource
 * creation.  These values are used in the function veto_ssm_property_updates().
 */
static char *non_updatable_properties[] = {
	SSM_PROP_SCALABLE,
	SSM_PROP_NET_RES,
	SSM_PROP_LB_POLICY,
	NULL
};

/*
 * These are the method names that are currently defined.
 * The RGM shouldn't call us for methods below the comment.
 */
code_table_t method_names[] = {
	SCHA_VALIDATE,		METH_CODE_VALIDATE,
	SCHA_BOOT,		METH_CODE_BOOT,
	SCHA_INIT,		METH_CODE_INIT,
	SCHA_START,		METH_CODE_START,
	SCHA_UPDATE,		METH_CODE_UPDATE,
	SCHA_MONITOR_CHECK,	METH_CODE_MONCHK,
	SCHA_FINI,		METH_CODE_FINI,
	/* The following methods do not require any SSM action. */
	SCHA_STOP,		METH_CODE_STOP,
	SCHA_MONITOR_START,	METH_CODE_MONSTART,
	SCHA_MONITOR_STOP,	METH_CODE_MONSTOP,
	SCHA_PRENET_START,	METH_CODE_PRENETSTART,
	SCHA_POSTNET_STOP,	METH_CODE_POSTNETSTOP,
	NULL,			CTAB_CODE_INVALID
};

/* These are the valid policies for the property SSM_PROP_LB_POLICY */
static code_table_t lb_policy_names[] = {
	LB_STRING_WEIGHTED,	scha_ssm_lb_weighted,
	LB_STRING_STICKY,	scha_ssm_lb_st,
	LB_STRING_STICKY_WILD,	scha_ssm_lb_sw,
	NULL,			CTAB_CODE_INVALID
};

/*
 * These are the valid protocols to be used in the property SSM_PROP_PORT_LIST.
 * See netinet/in.h
 */
code_table_t proto_names[] = {
	PROTO_STRING_TCP,	IPPROTO_TCP,
	PROTO_STRING_UDP,	IPPROTO_UDP,
#ifdef SCTP_RGM_SUPPORT
	PROTO_STRING_SCTP,	IPPROTO_SCTP,
#endif
	NULL,			CTAB_CODE_INVALID
};

/* Translate a code into its string representation. This will never be NULL */
char *
code_to_string(int code, code_table_t *ctab)
{
	int	i;

	for (i = 0; ctab[i].c_string != NULL; i++) {
		if (ctab[i].c_code == code)
			return (ctab[i].c_string);
	}

	return (CTAB_STRING_INVALID);
}

/*
 * Translate a string into its code representation.
 * This will be CTAB_CODE_INVALID if the string is not matched.
 */
int
string_to_code(char *string, code_table_t *ctab)
{
	int	i;

	for (i = 0; ctab[i].c_string != NULL; i++) {
		if (strcasecmp(string, ctab[i].c_string) == 0)
		    return (ctab[i].c_code);
	}

	return (CTAB_CODE_INVALID);
}

/*
 * Converts an IP address into its string representation. If address is
 * V4-mapped or V4-compat, the dotted decimal is returned. Otherwise,
 * inet_ntop() is used to convert the V6 address. If that fails, a string
 * with the 4 32-bit components of the address in hex separated by ":" is
 * returned.
 *
 * This routine will never return NULL.  Caller must not free the
 * memory returned by the call.  A static buffer (either in the inet_ntoa()
 * call or the static local variable buf) is used, so each
 * new call will overwrite the previous call.
 */
char *
ip_to_str(in6_addr_t ip)
{
	static char 	buf[INET6_ADDRSTRLEN];
	uint32_t	*uip;
	int		af = AF_INET6;

	if (IN6_IS_ADDR_V4MAPPED(&ip) || IN6_IS_ADDR_V4COMPAT(&ip))
		af = AF_INET;

	if (inet_ntop(af, &ip, buf, sizeof (buf)) == NULL) {
		/*
		 * Conversion failed, for strange reason! Just dump out
		 * the address in hex.
		 */
		uip = (uint32_t *)&ip;
		(void) snprintf(buf, sizeof (buf),
		    "%x:%x:%x:%x",
		    uip[0], uip[1], uip[2], uip[3]);
	}

	return (buf);
}

/*
 * Given a scha_err_t, get a string corresponding to that error.
 * This function will never return a NULL value.
 * Caller should not the free memory for the returned string.
 */
char *
get_scha_error_string(scha_err_t e)
{
	return (scha_strerror(e));
}

/* Get each of the SSM related properties and put into the struct */
ssm_prop_err_t
get_ssm_properties(char *rs, char *rg, validate_mode_t vmode,
    char *zname, int argc, char *argv[], ssm_properties_t *ssm_props)
{
	ssm_prop_err_t		prop_rc = SSM_PROP_ERR_NOERR;
	scha_str_array_t	*net_resource_array = NULL;
	scha_str_array_t	*port_list_array = NULL;
	scha_str_array_t	*weights_array = NULL;
	scha_str_array_t	*nodes_array = NULL;
	char			*lb_policy_string = NULL;
	int			last_char = 0;
	int			sticky_timeout = 0;
	boolean_t		sticky_udp = B_FALSE;
	boolean_t		sticky_weak = B_FALSE;
	boolean_t		sticky_generic = B_FALSE;
	boolean_t		rr_enabled = B_FALSE;
	int			conn_threshold = 0;

	/* Initialize the structure. ssm_props can't be NULL. */
	ssm_props->ssm_group = NULL;
	ssm_props->ssm_net_res_data = NULL;
	ssm_props->ssm_ports = NULL;
	ssm_props->ssm_numports = 0;
	ssm_props->ssm_lb_policy_code = CTAB_CODE_INVALID;
	ssm_props->ssm_weights = NULL;
	ssm_props->ssm_numweights = 0;
	ssm_props->ssm_rg_nodelist = NULL;
	ssm_props->ssm_rg_dependencies = NULL;
	ssm_props->ssm_sticky_timeout = 0;
	ssm_props->ssm_sticky_udp = B_FALSE;
	ssm_props->ssm_sticky_weak = B_FALSE;
	ssm_props->ssm_rr_enabled = B_FALSE;
	ssm_props->ssm_conn_threshold = 0;
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "Getting properties for resource %s for %s.",
	    rs,
	    (vmode != VALIDATE_NONE ?
	    "validate method" :
	    "non-validate method"));

	/*
	 * Set the SSM group name.
	 * For now, we use the resource name for the SSM group
	 */
	ssm_props->ssm_group = (ssm_group_t *)malloc(1 * sizeof (ssm_group_t));
	if (ssm_props->ssm_group == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}
	(void) strncpy(ssm_props->ssm_group->resource, rs,
	    sizeof (ssm_props->ssm_group->resource));
	last_char = sizeof (ssm_props->ssm_group->resource) - 1;
	ssm_props->ssm_group->resource[last_char] = '\0';

	/*
	 * Process the Port_list property.
	 */
	prop_rc = get_stringarray_property(rs, rg, zname, SSM_PROP_PORT_LIST,
	    vmode, argc, argv, &port_list_array);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}

	/* Fill in the port/protocol array */
	prop_rc = process_port_info(port_list_array,
	    &(ssm_props->ssm_ports),
	    &(ssm_props->ssm_numports));
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}
	free_strarray(port_list_array);
	/*
	 * Process the Network_resources_used property.
	 */
	prop_rc = get_stringarray_property(rs, rg, zname, SSM_PROP_NET_RES,
	    vmode, argc, argv, &net_resource_array);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}

	/* Get the IP addresses from the given net resources */
	prop_rc = get_ipaddrs_from_rlist(net_resource_array,
	    &(ssm_props->ssm_net_res_data),
	    ssm_props->ssm_ports,
	    ssm_props->ssm_numports, zname);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}
	free_strarray(net_resource_array);

#ifdef SCTP_RGM_SUPPORT
	if (is_sctp_protocol(ssm_props->ssm_numports, ssm_props->ssm_ports)) {
		prop_rc = process_net_resources(ssm_props->ssm_net_res_data);
		if (prop_rc != SSM_PROP_ERR_NOERR) {
			return (prop_rc);
		}
	}
#endif

	/*
	 * Get the Load_balancing_policy property.
	 */
	prop_rc = get_string_property(rs, rg, zname, SSM_PROP_LB_POLICY,
	    vmode, argc, argv, &lb_policy_string);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}

	/* Translate the Load_balancing_policy string to the SSM code */
	prop_rc = get_lb_policy_from_string(lb_policy_string,
	    &(ssm_props->ssm_lb_policy_code));
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}
	free(lb_policy_string);

	/*
	 * Load the Load_balancing_weights .
	 */
	prop_rc = get_stringarray_property(rs, rg, zname, SSM_PROP_LB_WEIGHTS,
	    vmode, argc, argv, &weights_array);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}

	/*
	 * Process the RG property Nodelist.
	 */
	prop_rc = get_stringarray_property_from_rg(rg, zname, SCHA_NODELIST,
	    vmode, argc, argv, &nodes_array);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}
	prop_rc = process_nodelist(nodes_array, &(ssm_props->ssm_rg_nodelist),
	    zname);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}
	free_strarray(nodes_array);


	/*
	 * Process the RG dependency list
	 */
	prop_rc = get_stringarray_property_from_rg(rg, zname,
	    SCHA_RG_DEPENDENCIES,
	    vmode, argc, argv, &(ssm_props->ssm_rg_dependencies));
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}

	prop_rc = get_int_property(rs, rg, zname, SCHA_AFFINITY_TIMEOUT,
	    vmode, argc, argv, &sticky_timeout);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}
	ssm_props->ssm_sticky_timeout = sticky_timeout;

	prop_rc = get_bool_property(rs, rg, zname, SCHA_UDP_AFFINITY,
	    vmode, argc, argv, &sticky_udp);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}
	ssm_props->ssm_sticky_udp = sticky_udp;

	prop_rc = get_bool_property(rs, rg, zname, SCHA_WEAK_AFFINITY,
	    vmode, argc, argv, &sticky_weak);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}
	ssm_props->ssm_sticky_weak = sticky_weak;

	prop_rc = get_bool_property(rs, rg, zname, SCHA_GENERIC_AFFINITY,
	    vmode, argc, argv, &sticky_generic);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}
	ssm_props->ssm_sticky_generic = sticky_generic;

	if (sticky_generic && ((sticky_timeout != -1) &&
	    sticky_timeout < MIN_AFFINITY_TIMEOUT)) {
		/*
		 * SCMSGS
		 * @explanation
		 * When you set the Generic_Affinity resource property,
		 * the Affinity_timeout resource property
		 * must be set to a value
		 * that is at least 50 Seconds.
		 * @user_action
		 * Set the value of the Affinity_Timeout resource
		 * property to at least 50 seconds.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Generic_Affinity resource property "
		    "cannot be set when "
		    "Affinity_timeout resource property "
		    "is less than 50 Seconds.");
		exit(1);
	}

	if (sticky_generic && sticky_udp) {
		/*
		 * SCMSGS
		 * @explanation
		 * Generic_Affinity and UDP_Affinity are mutually exclusive
		 * features.
		 * @user_action
		 * Enable either Generic_Affinity or UDP_Affinity for the
		 * resource.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Generic_Affinity and UDP_Affinity are mutually"
		    " exclusive features.");
		exit(1);
	}

	prop_rc = get_bool_property(rs, rg, zname, SCHA_ROUND_ROBIN,
	    vmode, argc, argv, &rr_enabled);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}

	ssm_props->ssm_rr_enabled = rr_enabled;

	prop_rc = get_int_property(rs, rg, zname, SCHA_CONN_THRESHOLD,
	    vmode, argc, argv, &conn_threshold);
	if (prop_rc != SSM_PROP_ERR_NOERR) {
		return (prop_rc);
	}

	ssm_props->ssm_conn_threshold = conn_threshold;

	/*
	 * Process the load balancing weights
	 */
	prop_rc = process_weight_info(weights_array,
	    &(ssm_props->ssm_weights),
	    &(ssm_props->ssm_numweights), rr_enabled, zname);

	free_strarray(weights_array);

	return (prop_rc);
}

void
free_ssm_properties(ssm_properties_t *ssm_props)
{
	if (ssm_props == NULL)
		return;

	free(ssm_props->ssm_group);
	free_snrl(ssm_props->ssm_net_res_data);
	free_ports(ssm_props->ssm_ports, ssm_props->ssm_numports);
	free(ssm_props->ssm_weights);
	free_nodelist(ssm_props->ssm_rg_nodelist);
	free_strarray(ssm_props->ssm_rg_dependencies);
}

void
print_ssm_properties(char *rs, ssm_properties_t *ssm_props)
{
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "The following are the scalable service related properties "
	    "for resource %s.",
	    rs);

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "%s = %s.",
	    SSM_PROP_GROUP,
	    (ssm_props->ssm_group->resource != NULL ?
	    ssm_props->ssm_group->resource : "NULL"));

	print_snrl(ssm_props->ssm_net_res_data);

	print_ports(ssm_props->ssm_ports, ssm_props->ssm_numports);

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "%s = %s which is %d numerically.",
	    SSM_PROP_LB_POLICY,
	    code_to_string(ssm_props->ssm_lb_policy_code, lb_policy_names),
	    ssm_props->ssm_lb_policy_code);

	print_weights(ssm_props->ssm_weights, ssm_props->ssm_numweights);

	print_nodelist(ssm_props->ssm_rg_nodelist);

	print_stringarray_property(SCHA_RG_DEPENDENCIES,
	    ssm_props->ssm_rg_dependencies);
}

/*
 * Performs additional checks for the SSM properties that have not
 * yet been checked during the filling in of the properties structure.
 */
scha_ssm_exceptions
validate_ssm_properties(char *rs, ssm_properties_t *ssm_props)
{
#define	NULLADDR(x) IN6_IS_ADDR_UNSPECIFIED(x)

	uint_t				i, j, k, l;
	ssm_net_resource_list_t		*net_res_data = NULL;
	boolean_t			bool_rc;
	int				found_weight;
	uint_t				this_node = 0;

	/*
	 * convenience variables used in checking for duplicate Port_list
	 * entries.
	 */
	in_port_t			port_i;
	in_port_t			port_j;
	uint8_t				proto_i;
	uint8_t				proto6_i;
	uint8_t				proto_j;
	char				*ip_name_i;
	char				*ip_name_j;
	in6_addr_t			ip_addr_i[2];
	in6_addr_t			ip_addr_j[2];

	/*
	 * Verify that no Port_list entry is duplicated.  Comparison
	 * between two entries is done on port number (both port and proto) and
	 * IP address (hostname).  Entries are judged to be duplicates if:
	 *   - port numbers (both port and proto) are the same and
	 *   - ((either ip name is NULL) or
	 *	((ip names are both not NULL) and
	 *	 (ip names are the same)))
	 *
	 * Also verify that any IP address that is explicitly given is an IP
	 * that belongs to a resource from Network_resources_used.
	 *
	 * NOTE: The ssm_wrapper does not currently make use of the IP address
	 * given in the Port_list property when creating the scalable services
	 * (ie, calling ssm_add_scal_service()).  Below, we prevent
	 * duplicates by allowing the same port/proto pair to exist so long
	 * as the IP address given is different.  However, since we currently
	 * don't use the IP address given when calling ssm_add_scal_service()
	 * (we create a scalable service for each port/proto pair and IP
	 * address combination in the Network_resources_used property), we
	 * can end up making redundant calls to 'add' the same scalable
	 * services in the case where two different IP addresses are given
	 * for the same port/proto pair (ie, ip1/80/tcp and ip2/80/tcp).
	 * This is OK at the networking layer because the call to
	 * ssm_add_scal_service() is idempotent.
	 */
	for (i = 0; i < ssm_props->ssm_numports; i++) {

		/* Assign and initialize convenience variables */
		port_i = ssm_props->ssm_ports[i].port;
		proto_i = ssm_props->ssm_ports[i].proto;
		proto6_i = ssm_props->ssm_ports[i].proto6_supported;
		ip_name_i = ssm_props->ssm_ports[i].ip_name;
		bzero(ip_addr_i, sizeof (ip_addr_i));

		/*
		 * If the user has given an explicit IP address to use for
		 * this Port_list entry, verify that the IP address exists
		 * in one of the resources in Network_resources_used.
		 *
		 * If IPv6 not supported, then the explicit IP must
		 * have a V4 address mapping.
		 */
		if (ip_name_i != NULL) {
			if (hostip_string_to_binary(ip_name_i,
			    ip_addr_i) != SSM_OK || (!proto6_i &&
			    !IN6_IS_ADDR_V4MAPPED(&ip_addr_i[0]) &&
			    !IN6_IS_ADDR_V4MAPPED(&ip_addr_i[1]))) {
				(void) fprintf(stderr, gettext(
				    "IP address (hostname) string %s "
				    "in property %s, entry %d "
				    "could not be resolved "
				    "to an IP address.\n"),
				    ip_name_i,
				    SSM_PROP_PORT_LIST,
				    i + 1);
				exit(1);
			}

			if ((!NULLADDR(&ip_addr_i[0]) &&
			    !is_ip_in_nru(ip_addr_i[0],
			    ssm_props->ssm_net_res_data)) ||
			    (!NULLADDR(&ip_addr_i[1]) &&
			    !is_ip_in_nru(ip_addr_i[1],
			    ssm_props->ssm_net_res_data))) {

				(void) fprintf(stderr, gettext(
				    "IP address (hostname) string %s "
				    "in property %s, entry %d does not "
				    "resolve to an IP address that belongs "
				    "to any of the resources named in "
				    "resource dependency properties of this "
				    "resource.\n"),
				    ip_name_i,
				    SSM_PROP_PORT_LIST,
				    i + 1);
				exit(1);
			}
		}

		for (j = i + 1; j < ssm_props->ssm_numports; j++) {

			/* Assign and initialize convenience variables */
			port_j = ssm_props->ssm_ports[j].port;
			proto_j = ssm_props->ssm_ports[j].proto;
			ip_name_j = ssm_props->ssm_ports[j].ip_name;
			bzero((char *)ip_addr_j, sizeof (ip_addr_j));

			/*
			 * If the i'th and j'th entry port/proto numbers are
			 * identical, check if the entries are in fact
			 * duplicates of each other.
			 */
			if (port_i == port_j && proto_i == proto_j) {
				/*
				 * Port_list entries i and j are assumed to
				 * be duplicates of each other if along w/
				 * having identical port/proto numbers, any
				 * of the following is true:
				 *   - either ip name is NULL.  In this case
				 *	one or both Port_list entries has
				 *	wildcarded the IP.  If a port number
				 *	is given w/ a wildcarded IP, no other
				 *	Port_list entry should contain that
				 *	port number.
				 *   - ((ip names are both not NULL) and
				 *	(underlying ip addrs are the same)).
				 *	This is where two explicitly given
				 *	IP/port entries duplicate the IP
				 *	address and the port number.
				 */

				/*
				 * Check if either IP name is NULL.
				 */
				if ((ip_name_i == NULL) ||
					(ip_name_j == NULL)) {
					(void) fprintf(stderr, gettext(
					    "Port %d%c%s is listed twice "
					    "in property %s, at entries %d "
					    "and %d.\n"),
					    port_i,
					    PORT_PROTO_DELIMITER,
					    code_to_string(
						proto_i,
						proto_names),
					    SSM_PROP_PORT_LIST,
					    i + 1, j + 1);
					exit(1);
				}

				/*
				 * ip_name_i has already been translated to
				 * its integer representation (ip_addr_i) above
				 * in the case where ip_name_i is not NULL.
				 * If ip_name_i is NULL, then we can't get
				 * to this point in the code due to the check
				 * above.
				 */

				/*
				 * Convert the hostname or binary form
				 * from the j'th Port_list entry to
				 * its underlying IP address.
				 */
				if (hostip_string_to_binary(ip_name_j,
				    ip_addr_j) != SCHA_SSM_SUCCESS) {
					(void) fprintf(stderr, gettext(
					    "IP address (hostname) string %s "
					    "in property %s, entry %d "
					    "could not be resolved "
					    "to an IP address.\n"),
					    ip_name_j,
					    SSM_PROP_PORT_LIST,
					    j + 1);
					exit(1);
				}

				/*
				 * Now compare the i'th and j'th Port_list
				 * entries on underlying IP address (port
				 * numbers are already known to be equal).
				 */
				if ((!NULLADDR(&ip_addr_i[0]) &&
				    (IN6_ARE_ADDR_EQUAL(&ip_addr_i[0],
				    &ip_addr_j[0]) ||
				    IN6_ARE_ADDR_EQUAL(&ip_addr_i[0],
				    &ip_addr_j[1]))) ||
				    (!NULLADDR(&ip_addr_i[1]) &&
				    (IN6_ARE_ADDR_EQUAL(&ip_addr_i[1],
				    &ip_addr_j[0]) ||
				    IN6_ARE_ADDR_EQUAL(&ip_addr_i[1],
				    &ip_addr_j[1])))) {

					(void) fprintf(stderr, gettext(
					    "IP address (hostname) and Port "
					    "pairs %s%c%d%c%s and %s%c%d%c%s "
					    "in property %s, at entries %d "
					    "and %d, effectively "
					    "duplicate each other.  The port "
					    "numbers are the same and the "
					    "resolved IP addresses are the "
					    "same.\n"),

					    /* entry i */
					    ip_name_i,
					    IP_PORT_DELIMITER,
					    port_i,
					    PORT_PROTO_DELIMITER,
					    code_to_string(
						proto_i,
						proto_names),

					    /* entry j */
					    ip_name_j,
					    IP_PORT_DELIMITER,
					    port_j,
					    PORT_PROTO_DELIMITER,
					    code_to_string(
						proto_j,
						proto_names),

					    SSM_PROP_PORT_LIST,
					    i + 1, j + 1);
					exit(1);
				}
			}
		}
	}

	/*
	 * Verify that no node id is listed twice in the weights string and
	 * that each node in the weights string is also in the nodelist
	 */
	for (i = 0; i < ssm_props->ssm_numweights; i++) {
		/* It's an error to list a node twice in the weights list */
		for (j = i + 1; j < ssm_props->ssm_numweights; j++) {
			if (ssm_props->ssm_weights[i].node ==
			    ssm_props->ssm_weights[j].node) {
				(void) fprintf(stderr, gettext(
				    "Node %d is listed twice "
				    "in property %s.\n"),
				    ssm_props->ssm_weights[i].node,
				    SSM_PROP_LB_WEIGHTS);
				exit(1);
			}
		}

		/* Warn if a node has a weight but is not in the nodelist */
		if (is_node_in_nodelist(ssm_props->ssm_weights[i].node,
		    ssm_props->ssm_rg_nodelist) != B_TRUE) {
			/* Warn only if weight is non-zero */
			if (ssm_props->ssm_weights[i].weight != 0) {
				(void) fprintf(stderr, gettext(
				    "Warning: node %d has a weight assigned "
				    "to it for property %s, but node %d is "
				    "not in the %s for resource %s.\n"),
				    ssm_props->ssm_weights[i].node,
				    SSM_PROP_LB_WEIGHTS,
				    ssm_props->ssm_weights[i].node,
				    SCHA_NODELIST,
				    rs);
			}
		}
	}

	/*
	 * Warn if there are weights specified for some, but not all of
	 * the nodes in the nodelist, or if weights are 0.
	 */
	if (ssm_props->ssm_numweights > 0) {
		/*
		 * Go through all nodes in the nodelist and see if
		 * there is a weight corresponding to each node.
		 */
		for (i = 0; i < ssm_props->ssm_rg_nodelist->numnodes; i++) {
			found_weight = SSM_INVALID_WEIGHT;
			for (j = 0; j < ssm_props->ssm_numweights; j++) {
				if (ssm_props->ssm_rg_nodelist->nodeids[i] ==
				    ssm_props->ssm_weights[j].node) {
					found_weight = (int)ssm_props->
					    ssm_weights[j].weight;
					break;
				}
			}

			if (found_weight == SSM_INVALID_WEIGHT) {
				/* Warning only */
				(void) fprintf(stderr, gettext(
				    "Warning: node %d does not have a "
				    "weight assigned to it for property %s, "
				    "but node %d is in the %s "
				    "for resource %s. A weight of %d will "
				    "be used for node %d.\n"),
				    ssm_props->ssm_rg_nodelist->nodeids[i],
				    SSM_PROP_LB_WEIGHTS,
				    ssm_props->ssm_rg_nodelist->nodeids[i],
				    SCHA_NODELIST,
				    rs,
				    SSM_DEFAULT_WEIGHT,
				    ssm_props->ssm_rg_nodelist->nodeids[i]);
			} else if (found_weight == 0) {
				/* Warning only */
				(void) fprintf(stderr, gettext(
				    "Warning: node %d has a weight of 0 "
				    "assigned to it for property %s.\n"),
				    ssm_props->ssm_rg_nodelist->nodeids[i],
				    SSM_PROP_LB_WEIGHTS);
			}
		}
	}
	/*
	 * Verify that
	 * - resource dependencies does not contain duplicates.
	 * - no IP address is duplicated in any of the resources listed
	 *   in  resource dependencies list.
	 */
	net_res_data = ssm_props->ssm_net_res_data;
	for (i = 0; i < net_res_data->snrl_num_snr; i++) {
		for (k = i; k < net_res_data->snrl_num_snr; k++) {
			/* Check for same resource name listed twice */
			if (i != k && strcmp(
			    net_res_data->snrl_snr_list[i].snr_name,
			    net_res_data->snrl_snr_list[k].snr_name) == 0) {
				(void) fprintf(stderr, gettext(
				    "Resource dependencies contain a duplicate "
				    "resource name %s.\n"),
				    net_res_data->snrl_snr_list[i].snr_name);
				exit(1);
			}

			/* Check that there is not an IP addr used by 2 R's */
			for (j = 0;
			    j < net_res_data->snrl_snr_list[i].snr_numip; j++) {
				/* start at 0 -- lists could be different */
				for (l = 0; l <
				    net_res_data->snrl_snr_list[k].snr_numip;
				    l++) {
					if (i == k && j == l) {
						continue;
					}

					if (IN6_ARE_ADDR_EQUAL(
					    &net_res_data->snrl_snr_list[i].
					    snr_iplist[j],
					    &net_res_data->snrl_snr_list[k].
					    snr_iplist[l])) {
						/* The IPs are the same */
						(void) fprintf(stderr, gettext(
						    "IP address %s is "
						    "an IP address in "
						    "resource %s and in "
						    "resource %s.\n"),
						    ip_to_str(net_res_data->
						    snrl_snr_list[i].
						    snr_iplist[j]),
						    net_res_data->
						    snrl_snr_list[i].snr_name,
						    net_res_data->
						    snrl_snr_list[k].snr_name);
						exit(1);
					}
				} /* all ips in one net ressource */
			} /* all ips in other net ressource */
		} /* all net ressources */
	} /* all net ressources */

	/*
	 * Verify that the masters of this resource (nodes in the RG nodelist
	 * of the containing RG) appear in the RG nodeslist or the AuxNodeList
	 * for all dependee SharedAddress resources/RGs.
	 */
	net_res_data = ssm_props->ssm_net_res_data;
	for (i = 0; i < ssm_props->ssm_rg_nodelist->numnodes; i++) {
		for (j = 0; j < net_res_data->snrl_num_snr; j++) {
			bool_rc = is_nodename_in_nodelist(
			    ssm_props->ssm_rg_nodelist->nodenames[i],
			    net_res_data->snrl_snr_list[j].snr_allnodeids);
			if (bool_rc != B_TRUE) {
				(void) fprintf(stderr, gettext(
				    "Node %d is in the %s for resource %s, "
				    "but the dependee resource %s "
				    "cannot host an address on node/zone"
				    " %s.\n"),
				    ssm_props->ssm_rg_nodelist->nodeids[i],
				    SCHA_NODELIST,
				    rs,
				    net_res_data->snrl_snr_list[j].snr_name,
				    ssm_props->ssm_rg_nodelist->nodenames[i]);
				exit(1);
			}
		}
	}

	/*
	 * If this node is in the rg_nodelist, verify that there is at least
	 * one IPMP group on this node.
	 */
	this_node = get_local_nodeid();
	if (is_node_in_nodelist(this_node, ssm_props->ssm_rg_nodelist) ==
	    B_TRUE) {
		if (local_node_has_nafo_group() != B_TRUE) {
			(void) fprintf(stderr, gettext(
			    "This node can be a primary for scalable resource "
			    "%s, but there is no IPMP group defined on this "
			    "node. A IPMP group must be created "
			    "on this node.\n"),
			    rs);
			exit(1);
		}
	}

	return (SCHA_SSM_SUCCESS);
}

/*
 * This function is called during the validate method to prevent certain
 * properties from being updated after resource cretation.
 */
void
veto_ssm_property_updates(char *rs, int argc, char *argv[]) {
	int	c;
	int	i;
	char	*key, *val;

	optind = 1;
	while ((c = getopt(argc, argv, "m:p:R:T:G:r:z:Z:x:g:cu")) != EOF) {
		switch (c) {
			/* ignore all options except the R properties */
			case 'm':
			case 'p':
			case 'R':
			case 'T':
			case 'G':
			case 'c':
			case 'u':
			case 'x':
			case 'z':
			case 'Z':
			case 'g':
				break;
			case 'r':
				if (str_kv(optarg, &key, &val) != 0) {
					(void) fprintf(stderr, gettext(
					    "Failed to parse key/value pair "
					    "from command line for %s.\n"),
					    optarg);
					exit(1);
				}

				for (i = 0; non_updatable_properties[i] != NULL;
				    i++) {
					if (strcasecmp(key,
					    non_updatable_properties[i]) == 0) {
						(void) fprintf(stderr, gettext(
						    "The property %s cannot be "
						    "updated because it "
						    "affects the scalable "
						    "resource %s.\n"),
						    key,
						    rs);
						exit(1);
					}
				}
				free(key);
				free(val);
				break;
			default:
				break;
		}
	}
	/* Success */
}

ssm_prop_err_t
get_string_property_from_args(char *propname, int argc, char *argv[],
    char **propval)
{
	int	c;
	char	*key, *val;

	optind = 1;
	while ((c = getopt(argc, argv, "m:p:R:T:G:Z:z:r:x:g:cu")) != EOF) {
		switch (c) {
			/* ignore all options except the resource properties */
			case 'm':
			case 'p':
			case 'R':
			case 'T':
			case 'G':
			case 'c':
			case 'u':
			case 'g':
			case 'x':
			case 'Z':
			case 'z':
				break;
			case 'r':
				if (str_kv(optarg, &key, &val) != 0) {
					/*
					 * SCMSGS
					 * @explanation
					 * The validate method for the
					 * scalable resource network
					 * configuration code was unable to
					 * convert the property information
					 * given to a usable format.
					 * @user_action
					 * Verify the property information was
					 * properly set when configuring the
					 * resource.
					 */
					(void) sc_syslog_msg_log(log_handle,
					    SC_SYSLOG_ERROR, MESSAGE,
					    "Failed to parse key/value pair "
					    "from command line for %s.",
					    optarg);
					exit(1);
				}

				if (strcasecmp(key, propname) == 0) {
					/*
					 * SCMSGS
					 * @explanation
					 * Need explanation of this message!
					 * @user_action
					 * Need a user action for this
					 * message.
					 */
					syslog(LOG_DEBUG,
					    "Found string on command "
					    "line: %s.",
					    optarg);

					*propval = val;
					free(key);
					return (SSM_PROP_ERR_NOERR);
				} else {
					free(key);
					free(val);
				}
				break;
			default:
				break;
		}
	}

	/* Returning failed here means that it is not on the command line */
	return (SSM_PROP_ERR_FAILED);
}

/*
 * Retrieve a string property from the API.
 * Memory must be freed by the caller.
 */
ssm_prop_err_t
get_string_property(char *rs, char *rg, char *zname, char *propname,
    validate_mode_t vmode, int argc, char *argv[], char **propval)
{
	scha_err_t		scha_rc;
	scha_resource_t		rs_handle;
	ssm_prop_err_t		prop_rc = SSM_PROP_ERR_NOERR;
	char			*string_prop = NULL;

	/* propval cannot be a NULL pointer */
	*propval = NULL;

	if (vmode != VALIDATE_NONE) {
		/* check args first */
		prop_rc = get_string_property_from_args(propname, argc, argv,
		    propval);

		/* If we fail to find it in the args, we will keep looking */
		if (prop_rc != SSM_PROP_ERR_FAILED) {
			goto finished;
		}
	}

retry_rs:
	scha_rc = scha_resource_open_zone(zname, rs, rg, &rs_handle);
	if (scha_rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The query for a property failed. The reason for
		 * failure is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages file on all nodes.
		 * Contact your authorized Sun service provider for
		 * assistance in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "%z Failed to retrieve the resource handle for %s while "
		    "querying for property %s: %s.",
		    zname, rs,
		    propname,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	scha_rc = scha_resource_get_zone(zname, rs_handle, propname,
	    &string_prop);
	if (scha_rc == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource information.");
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
		goto retry_rs;
	} else if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource property %s for %s: %s.",
		    propname,
		    rs,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	if (string_prop == NULL) {
		*propval = NULL;
	} else {
		*propval = strdup(string_prop);
		if (*propval == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Out of memory.");
			exit(1);
		}
	}

	(void) scha_resource_close(rs_handle);
	prop_rc = SSM_PROP_ERR_NOERR;

finished:
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "String property %s has value %s.",
	    propname,
	    ((*propval != NULL) ? *propval : "NULL"));

	return (prop_rc);
}

/*
 * Retrieve an integer property from the API.
 * Memory must be freed by the caller.
 */
ssm_prop_err_t
get_int_property(char *rs, char *rg, char *zname, char *propname,
    validate_mode_t vmode, int argc, char *argv[], int *propval)
{
	scha_err_t		scha_rc;
	scha_resource_t		rs_handle;
	ssm_prop_err_t		prop_rc = SSM_PROP_ERR_NOERR;
	char			*int_prop_str = NULL, *endptr = NULL;
	int			int_prop;

	if (vmode != VALIDATE_NONE) {
		/* check args first */
		prop_rc = get_string_property_from_args(propname, argc, argv,
		    &int_prop_str);

		/* If we fail to find it in the args, we will keep looking */
		if (prop_rc != SSM_PROP_ERR_FAILED && int_prop_str) {
			*propval = strtol(int_prop_str, &endptr, 10);
			if (endptr == NULL || *endptr != '\0') {
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_ERROR, MESSAGE,
				    "Failed to parse key/value pair "
				    "from command line for %s.",
				    propname);
				exit(1);
			}
			goto finished;
		}
	}

retry_rs:
	scha_rc = scha_resource_open_zone(zname, rs, rg, &rs_handle);
	if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource handle for %s while "
		    "querying for property %s: %s.",
		    rs,
		    propname,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	scha_rc = scha_resource_get_zone(zname, rs_handle, propname, &int_prop);
	if (scha_rc == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource information.");
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
		goto retry_rs;
	} else if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource property %s for %s: %s.",
		    propname,
		    rs,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	*propval = int_prop;

	(void) scha_resource_close(rs_handle);
	prop_rc = SSM_PROP_ERR_NOERR;

finished:
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "Integer property %s has value %d.",
	    propname,
	    *propval);

	return (prop_rc);
}

/*
 * Retrieve a boolean property from the API.
 * Memory must be freed by the caller.
 */
ssm_prop_err_t
get_bool_property(char *rs, char *rg, char *zname, char *propname,
    validate_mode_t vmode, int argc, char *argv[], boolean_t *propval)
{
	scha_err_t		scha_rc;
	scha_resource_t		rs_handle;
	ssm_prop_err_t		prop_rc = SSM_PROP_ERR_NOERR;
	char			*bool_prop_str = NULL;
	boolean_t		bool_prop;

	if (vmode != VALIDATE_NONE) {
		/* check args first */
		prop_rc = get_string_property_from_args(propname, argc, argv,
		    &bool_prop_str);

		/* If we fail to find it in the args, we will keep looking */
		if (prop_rc != SSM_PROP_ERR_FAILED && bool_prop_str) {
			if (strcasecmp(bool_prop_str, "TRUE") == 0)
				*propval = B_TRUE;
			else if (strcasecmp(bool_prop_str, "FALSE") == 0)
				*propval = B_FALSE;
			else {
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_ERROR, MESSAGE,
				    "Failed to parse key/value pair "
				    "from command line for %s.",
				    propname);
				exit(1);
			}
			goto finished;
		}
	}

retry_rs:
	scha_rc = scha_resource_open_zone(zname, rs, rg, &rs_handle);
	if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource handle for %s while "
		    "querying for property %s: %s.",
		    rs,
		    propname,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	scha_rc = scha_resource_get_zone(zname, rs_handle, propname,
	    &bool_prop);
	if (scha_rc == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource information.");
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
		goto retry_rs;
	} else if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource property %s for %s: %s.",
		    propname,
		    rs,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	*propval = bool_prop;

	(void) scha_resource_close(rs_handle);
	prop_rc = SSM_PROP_ERR_NOERR;

finished:
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "Boolean property %s has value %s.",
	    propname,
	    (*propval == B_TRUE? "TRUE": "FALSE"));

	return (prop_rc);
}


ssm_prop_err_t
get_stringarray_property_from_args(char *propname, int argc, char *argv[],
    scha_str_array_t **propval)
{
	int	c;
	int	rc;
	char	*key, *val;

	optind = 1;
	while ((c = getopt(argc, argv, "m:p:R:T:G:r:x:g:z:Z:cu")) != EOF) {
		switch (c) {
			/* ignore all options except the R and RG properties */
			case 'm':
			case 'p':
			case 'R':
			case 'T':
			case 'G':
			case 'c':
			case 'u':
			case 'x':
			case 'z':
			case 'Z':
				break;
			case 'r':
			case 'g': /* the nodelist & deps are group propertes */

				if (str_kv(optarg, &key, &val) != 0) {
					(void) sc_syslog_msg_log(log_handle,
					    SC_SYSLOG_ERROR, MESSAGE,
					    "Failed to parse key/value pair "
					    "from command line for %s.",
					    optarg);
					exit(1);
				}

				if (strcasecmp(key, propname) == 0) {
					rc = str_to_strarray(val, propval);
					if (rc != 0) {
						/*
						 * SCMSGS
						 * @explanation
						 * The validate method for the
						 * scalable resource network
						 * configuration code was
						 * unable to convert the
						 * property information given
						 * to a usable format.
						 * @user_action
						 * Verify the property
						 * information was properly
						 * set when configuring the
						 * resource.
						 */
						(void) sc_syslog_msg_log(
						    log_handle,
						    SC_SYSLOG_ERROR, MESSAGE,
						    "Failed to format "
						    "stringarray for property "
						    "%s from value %s.",
						    propname, val);
						exit(1);
					} else {
						/*
						 * SCMSGS
						 * @explanation
						 * Need explanation of this
						 * message!
						 * @user_action
						 * Need a user action for this
						 * message.
						 */
						syslog(LOG_DEBUG,
						    "Found stringarray on "
						    "command line: %s.",
						    optarg);
						free(key);
						free(val);
						return (SSM_PROP_ERR_NOERR);
					}
				} else {
					free(key);
					free(val);
				}
				break;
			default:
				break;
		}
	}

	/* Returning failed here means that it is not on the command line */
	return (SSM_PROP_ERR_FAILED);
}

/*
 * Retrieve a stringarray property from the API or from the args.
 * Memory must be freed by the caller.
 */
ssm_prop_err_t
get_stringarray_property(char *rs, char *rg, char *zname, char *propname,
    validate_mode_t vmode, int argc, char *argv[], scha_str_array_t **propval)
{
	scha_err_t		scha_rc;
	scha_resource_t		rs_handle;
	ssm_prop_err_t		prop_rc = SSM_PROP_ERR_NOERR;
	scha_str_array_t	*strarray_prop = NULL;

	/* propval cannot be a NULL pointer */
	*propval = NULL;

	if (vmode != VALIDATE_NONE) {
		prop_rc = get_stringarray_property_from_args(propname, argc,
		    argv, propval);

		/* If we fail to find it in the args, we will keep looking */
		if (prop_rc != SSM_PROP_ERR_FAILED) {
			goto finished;
		}
	}

retry_rs:
	scha_rc = scha_resource_open_zone(zname, rs, rg, &rs_handle);
	if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource handle for %s while "
		    "querying for property %s: %s.",
		    rs,
		    propname,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	scha_rc = scha_resource_get_zone(zname, rs_handle, propname,
	    &strarray_prop);
	if (scha_rc == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource information.");
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
		goto retry_rs;
	} else if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource property %s for %s: %s.",
		    propname,
		    rs,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	if (strarray_prop == NULL) {
		*propval = NULL;
	} else {
		*propval = strarraydup(strarray_prop);
		if (*propval == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Out of memory.");
			exit(1);
		}
	}

	(void) scha_resource_close(rs_handle);
	prop_rc = SSM_PROP_ERR_NOERR;

finished:
	print_stringarray_property(propname, *propval);

	return (prop_rc);
}

/*
 * Retrieve a stringarray property from the API or from the args.
 * Memory must be freed by the caller.
 */
ssm_prop_err_t
get_stringarray_property_from_rg(char *rg, char *zname, char *propname,
    validate_mode_t vmode, int argc, char *argv[], scha_str_array_t **propval)
{
	scha_err_t		scha_rc;
	scha_resource_t		rg_handle;
	ssm_prop_err_t		prop_rc = SSM_PROP_ERR_NOERR;
	scha_str_array_t	*strarray_prop = NULL;

	/* propval cannot be a NULL pointer */
	*propval = NULL;

	if (vmode != VALIDATE_NONE) {
		prop_rc = get_stringarray_property_from_args(propname, argc,
		    argv, propval);

		/* If we fail to find it in the args, we will keep looking */
		if (prop_rc != SSM_PROP_ERR_FAILED) {
			goto finished;
		}
	}

retry_rg:
	scha_rc = scha_resourcegroup_open_zone(zname, rg, &rg_handle);
	if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource group handle for %s while "
		    "querying for property %s: %s.",
		    rg,
		    propname,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	scha_rc = scha_resourcegroup_get_zone(zname, rg_handle, propname,
	    &strarray_prop);
	if (scha_rc == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource group information.");
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
		goto retry_rg;
	} else if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource group property %s for %s: "
		    "%s.",
		    propname,
		    rg,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	if (strarray_prop == NULL) {
		*propval = NULL;
	} else {
		*propval = strarraydup(strarray_prop);
		if (*propval == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Out of memory.");
			exit(1);
		}
	}

	(void) scha_resourcegroup_close(rg_handle);
	prop_rc = SSM_PROP_ERR_NOERR;

finished:
	print_stringarray_property(propname, *propval);

	return (prop_rc);
}

void
print_stringarray_property(char *propname, scha_str_array_t *prop)
{
	uint_t	i;

	if (prop == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "Stringarray property %s is NULL.",
		    propname);
		return;
	}

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "Stringarray property %s has %d element(s).",
	    propname,
	    prop->array_cnt);

	for (i = 0; i < prop->array_cnt; i++) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "  %s[%d] = %s",
		    propname,
		    i,
		    (prop->str_array[i] != NULL ? prop->str_array[i] : "NULL"));
	}
}

ssm_prop_err_t
get_lb_policy_from_string(char *policy_string, scha_ssm_policy_t *policy_code)
{
	/* policy_code can't be NULL */

	if (policy_string == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The property named does not have a legal value.
		 * @user_action
		 * Assign the property a legal value.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "The property %s does not have a legal value.",
		    SSM_PROP_LB_POLICY);
		exit(1);
	}

	*policy_code = string_to_code(policy_string, lb_policy_names);
	if (*policy_code == CTAB_CODE_INVALID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "The property %s does not have a legal value.",
		    SSM_PROP_LB_POLICY);
		exit(1);
	}

	return (SSM_PROP_ERR_NOERR);
}

ssm_prop_err_t
process_weight_info(scha_str_array_t *weights_array,
    ssm_nodeweight_t **weights, uint_t *numweights, boolean_t is_rr_enabled,
    char *zname)
{
	uint_t	i;
	char	*endp = NULL;
	char	*node_string;
	int	node_int;
	char	*weight_string;
	int	weight_int;
	int	total_weight = 0;

	/* weights and numweights cannot be NULL */

	if (weights_array == NULL || weights_array->array_cnt == 0) {
		*numweights = 0;
		*weights = NULL;
		return (SSM_PROP_ERR_NOERR);
	}

	*numweights = weights_array->array_cnt;
	*weights = (ssm_nodeweight_t *)calloc(*numweights,
	    sizeof (ssm_nodeweight_t));

	if (*weights == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	for (i = 0; i < *numweights; i++) {
		if (weights_array->str_array[i] == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Entry at position %d in property %s was invalid.",
			    i,
			    SSM_PROP_LB_WEIGHTS);
			exit(1);
		}

		weight_string = strdup(weights_array->str_array[i]);
		if (weight_string == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Out of memory.");
			exit(1);
		}

		node_string = strchr(weight_string, WEIGHT_NODE_DELIMITER);
		if (node_string == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * The property was specified incorrectly.
			 * @user_action
			 * Set the property using the correct syntax.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "No node was specified as part of property %s "
			    "for element %s. "
			    "The property must be specified as "
			    "%s=Weight%cNode,Weight%cNode,...",
			    SSM_PROP_LB_WEIGHTS,
			    weight_string,
			    SSM_PROP_LB_WEIGHTS,
			    WEIGHT_NODE_DELIMITER,
			    WEIGHT_NODE_DELIMITER);
			exit(1);
		} else {
			/*
			 * The user specfied a node following the weight.
			 * NULL terminate weight_string and advance the ptr
			 */
			*node_string = NULL;
			node_string = node_string + 1;
		}

		/*
		 * errno.h is missing void arg, so suppress lint error.
		 * [should be errno(void) rather than errno()]
		 */
		errno = 0;			/*lint !e746 */
		/* Get the weight as an integer */
		weight_int = strtol(weight_string, &endp, 10);
		if (errno != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * The weight noted does not have a valid value. The
			 * position index, which starts at 0 for the first
			 * element in the list, indicates which element in the
			 * property list was invalid.
			 * @user_action
			 * Give the weight a valid value.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "The weight portion "
			    "of %s at position %d in property %s "
			    "is not a valid weight. "
			    "The weight should be an integer between %d "
			    "and %d.",
			    weights_array->str_array[i],
			    i,
			    SSM_PROP_LB_WEIGHTS,
			    SSM_MIN_WEIGHT,
			    SSM_MAX_WEIGHT);
			exit(1);
		} else if (*endp != '\0') {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "The weight portion "
			    "of %s at position %d in property %s "
			    "is not a valid weight. "
			    "The weight should be an integer between %d "
			    "and %d.",
			    weights_array->str_array[i],
			    i,
			    SSM_PROP_LB_WEIGHTS,
			    SSM_MIN_WEIGHT,
			    SSM_MAX_WEIGHT);
			exit(1);
		}

		if (weight_int < SSM_MIN_WEIGHT ||
		    weight_int > SSM_MAX_WEIGHT) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "The weight portion "
			    "of %s at position %d in property %s "
			    "is not a valid weight. "
			    "The weight should be an integer between %d "
			    "and %d.",
			    weights_array->str_array[i],
			    i,
			    SSM_PROP_LB_WEIGHTS,
			    SSM_MIN_WEIGHT,
			    SSM_MAX_WEIGHT);
			exit(1);
		}

		/* Check if the node is a valid nodename */
		if ((node_int = ssm_get_nodeid(node_string, zname)) ==
		    INVALID_NODEID) {
			/*
			 * SCMSGS
			 * @explanation
			 * An invalid node was specified for the named
			 * property. The position index, which starts at 0 for
			 * the first element in the list, indicates which
			 * element in the property list was invalid.
			 * @user_action
			 * Specify a valid node instead.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "The node portion "
			    "of %s at position %d in property %s "
			    "is not a valid node identifier "
			    "or node name.",
			    weights_array->str_array[i],
			    i,
			    SSM_PROP_LB_WEIGHTS);
			exit(1);
		}

		(*weights)[i].weight = (uint_t)weight_int;
		(*weights)[i].node = (uint_t)node_int;
		total_weight += weight_int;
		free(weight_string);
	}

	/* If RoundRobin enabled check the total weight */

	if (is_rr_enabled) {
		if (total_weight > SSM_TOTAL_WEIGHT) {
			/*
			 * SCMSGS
			 * @explanation
			 * The sum of the weights assigned to the nodes is not
			 * a legal value.
			 * @user_action
			 * Give the nodes a valid value
			 * so that the sum of the weights is a legal value.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "The sum of the weights assigned to the nodes "
			    "in scalable service exceeds %d", SSM_TOTAL_WEIGHT);
			    exit(1);
		}
	}

	return (SSM_PROP_ERR_NOERR);
}

void
print_weights(ssm_nodeweight_t *weights, uint_t numweights)
{
	uint_t	i;

	if (weights == NULL || numweights == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "%s is empty.",
		    SSM_PROP_LB_WEIGHTS);
	} else {
		syslog(LOG_DEBUG,
		    "%s has %d element(s).",
		    SSM_PROP_LB_WEIGHTS,
		    numweights);
	}

	for (i = 0; i < numweights; i++) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "  %s[%d]  Node %d  Weight %d",
		    SSM_PROP_LB_WEIGHTS,
		    i,
		    weights[i].node,
		    weights[i].weight);
	}
}

ssm_prop_err_t
process_port_info(scha_str_array_t *ports_array, ssm_port_and_proto_t **ports,
    uint_t *numports)
{
	uint_t	i;
	char	*endp = NULL;
	int	port_int;
	char	*port_string;
	char	*port_string_head;
	int	proto_int;
	char	*proto_string;
	char	*ip_name;
	char	*ip;
	char	*cptr;
	uchar_t	proto6_supp;

	/* ports and numports cannot be NULL */

	if (ports_array == NULL || ports_array->array_cnt == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The named property does not have a legal value.
		 * @user_action
		 * Assign the property a value.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "The property %s must contain at least one value.",
		    SSM_PROP_PORT_LIST);
		exit(1);
	}

	*numports = ports_array->array_cnt;
	*ports = (ssm_port_and_proto_t *)calloc(*numports,
	    sizeof (ssm_port_and_proto_t));

	if (*ports == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	/*
	 * Process each port in Port_list.
	 *
	 * The elements of a port entry in Port_list are:
	 *   - optional: [IP address (hostname) followed by a delimiter]
	 *   - Port number
	 *   - delimiter
	 *   - Port protocol
	 *
	 * Like this:
	 *
	 * [<IP>IP_PORT_DELIMITER]<Port num>PORT_PROTO_DELIMITER<Port protocol>
	 *
	 * When unspecified, the optional IP address is considered to be
	 * a "wildcard".
	 *
	 * SSM_wrapper uses the Port number and protocol from Port_list
	 * to drive scalable services setup.  SSM_wrapper only tolerates
	 * the IP address in Port_list, i.e. IP address is essentially
	 * ignored except in Port_list validation.  IP address is parsed off
	 * and kept, but only for the Port_list validation.
	 */
	for (i = 0; i < *numports; i++) {
		if (ports_array->str_array[i] == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Entry at position %d in property %s was invalid.",
			    i,
			    SSM_PROP_PORT_LIST);
			exit(1);
		}

		port_string = strdup(ports_array->str_array[i]);
		if (port_string == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Out of memory.");
			exit(1);
		}
		port_string_head = port_string;

		proto_string = strrchr(port_string, PORT_PROTO_DELIMITER);
		if (proto_string == NULL) {
			/*
			 * The protocol was not specified.
			 * We could use a default of TCP, but we currently
			 * don't allow it. Replacing the error message below
			 * with the following line will set the default
			 * of TCP.
			 *
			 *	proto_int = IPPROTO_TCP;
			 *
			 * Note that the error msg below does not
			 * account for the Apache-specific Port_list
			 * optional format of
			 *    [IP name/]PortNumber/Protocol
			 * This is on purpose to avoid confusing readers
			 * of the msg not using Apache.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * The IPMP group named is in a transition state. The
			 * status will be checked again.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "No protocol was given as part of property %s "
			    "for element %s. "
			    "The property must be specified as "
			    "%s=PortNumber%cProtocol,"
			    "PortNumber%cProtocol,...",
			    SSM_PROP_PORT_LIST,
			    port_string,
			    SSM_PROP_PORT_LIST,
			    PORT_PROTO_DELIMITER,
			    PORT_PROTO_DELIMITER);
			exit(1);
		} else {
			/* The user specfied a protocol following the port */

			/* NULL terminate the port_string and advance the ptr */
			*proto_string = NULL;
			proto_string = proto_string + 1;

			/*
			 * Support for "tcp6" and "udp6", which indicate
			 * IPv6 support in the data service.
			 */
			if ((cptr = strchr(proto_string, '6')) != NULL &&
			    cptr[1] == '\0') {
				*cptr = '\0';
				proto6_supp = 1;
			} else
				proto6_supp = 0;

			proto_int = string_to_code(proto_string, proto_names);

			if (proto_int == CTAB_CODE_INVALID ||
			    (cptr != NULL && *++cptr != '\0')) {
				/*
				 * SCMSGS
				 * @explanation
				 * The property named does not have a legal
				 * value.
				 * @user_action
				 * Assign the property a legal value.
				 */
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_ERROR, MESSAGE,
				    "Invalid protocol %s given as part of "
				    "property %s.",
				    proto_string,
				    SSM_PROP_PORT_LIST);
				exit(1);
			}
		}

		/*
		 * parse the port_string out from the remaining
		 * <IP>IP_PORT_DELIMITER<port string>
		 * (<IP>IP_PORT_DELIMITER is optional).
		 */
		ip = port_string;
		port_string = strchr(ip, IP_PORT_DELIMITER);
		if (port_string != NULL) {
			*port_string = '\0'; /* separate port string from IP */
			port_string++;	/* advance it from the separator */
		} else {
			port_string = ip;
			ip = NULL;
		}

		/*
		 * If the IP was specified, copy it for later validation use.
		 */
		if (ip != NULL) {
			ip_name = strdup(ip);
			if (ip_name == NULL) {
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_ERROR, MESSAGE,
				    "Out of memory.");
				exit(1);
			}

		/*
		 * Otherwise IP has not been specified, meaning it is
		 * "wildcarded".
		 *
		 * The name is marked NULL.
		 */
		} else {
			ip_name = NULL;
		}

		errno = 0;
		port_int = strtol(port_string, &endp, 10);
		if (errno != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * The property named does not have a legal value. The
			 * position index, which starts at 0 for the first
			 * element in the list, indicates which element in the
			 * property list was invalid.
			 * @user_action
			 * Assign the property a legal value.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "The port portion "
			    "of %s at position %d in property %s "
			    "is not a valid port.",
			    ports_array->str_array[i],
			    i,
			    SSM_PROP_PORT_LIST);
			exit(1);
		} else if (*endp != '\0') {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "The port portion "
			    "of %s at position %d in property %s "
			    "is not a valid port.",
			    ports_array->str_array[i],
			    i,
			    SSM_PROP_PORT_LIST);
			exit(1);
		}

		if (port_int < 0) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "The port portion "
			    "of %s at position %d in property %s "
			    "is not a valid port.",
			    ports_array->str_array[i],
			    i,
			    SSM_PROP_PORT_LIST);
			exit(1);
		}

		(*ports)[i].port = (in_port_t)port_int;
		(*ports)[i].proto = (uint8_t)proto_int;
		(*ports)[i].proto6_supported = proto6_supp;
		(*ports)[i].ip_name = ip_name;

		free(port_string_head);
	}

	return (SSM_PROP_ERR_NOERR);
}

void
print_ports(ssm_port_and_proto_t *ports, uint_t numports)
{
	uint_t	i;

	if (ports == NULL || numports == 0) {
		syslog(LOG_DEBUG,
		    "%s is empty.",
		    SSM_PROP_PORT_LIST);
	} else {
		syslog(LOG_DEBUG,
		    "%s has %d element(s).",
		    SSM_PROP_PORT_LIST,
		    numports);
	}

	for (i = 0; i < numports; i++) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "  %s[%d]  Port %d%c%s  Hostname/IP-address %s",
		    SSM_PROP_PORT_LIST,
		    i,
		    ports[i].port,
		    PORT_PROTO_DELIMITER,
		    code_to_string(ports[i].proto, proto_names),
		    IP_NAME_STRING(ports[i].ip_name));
	}
}

/*
* Convert a string(representing a valid nodename or nodeid) to a nodename.
* Returns a newly-allocated string with a canonical nodename.
*/
char *
normalize_nodename(const char *nodename)
{
	scha_err_t e;
	scha_cluster_t clust_handle;
	char *endp = NULL;
	char *node_name = NULL;
	int node_int = INVALID_NODEID;

	if (nodename == NULL) {
		(void) sprintf(internal_err_str,
		    "Can't normalize nodename; "
		    "Nodename is null");
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

	e = scha_cluster_open(&clust_handle);
	if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the cluster handle "
		    "while querying for property %s: %s.",
		    SCHA_NODENAME_NODEID,
		    get_scha_error_string(e));
		exit(1);
	}

	node_int = strtol(nodename, &endp, 10);

	if (node_int > 0 && node_int < 65 && *endp == '\0') {
		char *tmp1 = NULL;
		e = scha_cluster_get(clust_handle, SCHA_NODENAME_NODEID,
			node_int, &tmp1);
		if (e != SCHA_ERR_NOERR) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Failed to retrieve the cluster property %s"
			    " for %s: %s.",
			    SCHA_NODENAME_NODEID,
			    node_int,
			    get_scha_error_string(e));
			exit(1);
		}
		node_name = strdup(tmp1);
	} else {
		node_name = strdup(nodename);
	}

	(void) scha_cluster_close(clust_handle);
	return (node_name);
}

ssm_prop_err_t
process_nodelist(scha_str_array_t *nodes_array, ssm_node_list_t **nodelist,
    char *zname)
{
	uint_t	i;
	int	newnodeid;

	/* nodelist cannot be NULL */

	if (nodes_array == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "The property %s does not have a legal value.",
		    SCHA_NODELIST);
		exit(1);
	}

	*nodelist = (ssm_node_list_t *)malloc(sizeof (ssm_node_list_t));
	if (*nodelist == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	(*nodelist)->numnodes = 0;
	(*nodelist)->nodeids = NULL;
	(*nodelist)->nodenames = NULL;

	for (i = 0; i < nodes_array->array_cnt; i++) {
		if (nodes_array->str_array[i] == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Entry at position %d in property %s was invalid.",
			    i,
			    SCHA_NODELIST);
			exit(1);
		}

		newnodeid = ssm_get_nodeid(nodes_array->str_array[i], zname);
		if (newnodeid == INVALID_NODEID) {
			/*
			 * SCMSGS
			 * @explanation
			 * The value given for the named property has an
			 * invalid node specified for it. The position index,
			 * which starts at 0 for the first element in the
			 * list, indicates which element in the property list
			 * was invalid.
			 * @user_action
			 * Specify a valid node for the property.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Entry at position %d in property %s with value "
			    "%s is not a valid node identifier or node name.",
			    i,
			    SCHA_NODELIST,
			    nodes_array->str_array[i]);
			exit(1);
		}

		/* add the node id to the nodelist */
		(*nodelist)->numnodes++;
		(*nodelist)->nodeids = (uint_t *)realloc((*nodelist)->nodeids,
		    (*nodelist)->numnodes * sizeof (uint_t));
		if ((*nodelist)->nodeids == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Out of memory.");
			exit(1);
		}

		(*nodelist)->nodenames = (char **)realloc(
		    (*nodelist)->nodenames,
		    (*nodelist)->numnodes * sizeof (char*));
		if ((*nodelist)->nodenames == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Out of memory.");
			exit(1);
		}

		(*nodelist)->nodeids[(*nodelist)->numnodes - 1] =
		    (uint_t)newnodeid;

		(*nodelist)->nodenames[(*nodelist)->numnodes - 1] =
		normalize_nodename(nodes_array->str_array[i]);

	}

	return (SSM_PROP_ERR_NOERR);
}

/*
 * This function adds the nodeids from nodelist2 to nodelist1,
 * similar to strcat().
 * Also adds nodenames from nodelist2 to nodelist1.
 * nodelist2 is not modified.
 * nodelist1 is extended by the number of nodes in nodelist2.
 * Duplicate nodes are not removed.
 */
void
nodelistcat(ssm_node_list_t *nodelist1, const ssm_node_list_t *nodelist2)
{
	uint_t	i;

	if (nodelist1 == NULL || nodelist2 == NULL) {
		return;
	}

	nodelist1->nodeids = (uint_t *)realloc(nodelist1->nodeids,
	    (nodelist1->numnodes + nodelist2->numnodes) * sizeof (uint_t));
	if (nodelist1->nodeids == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	nodelist1->nodenames = (char **)realloc(nodelist1->nodenames,
		(nodelist1->numnodes + nodelist2->numnodes) * sizeof (char *));
	if (nodelist1->nodenames == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	/* copy nodeids from nodelist2 to the end of nodelist1 */

	(void) memcpy(nodelist1->nodeids + nodelist1->numnodes,
		nodelist2->nodeids,
		nodelist2->numnodes * sizeof (uint_t));

	/* copy nodenames from nodelist2 to the end of nodelist1 */

	for (i = 0; i < nodelist2->numnodes; i++) {
		nodelist1->nodenames[i + nodelist1->numnodes] =
			strdup(nodelist2->nodenames[i]);
	}
	nodelist1->numnodes = nodelist1->numnodes + nodelist2->numnodes;
}

void
free_ports(ssm_port_and_proto_t *ports, uint_t numports) {
	uint_t i;

	if ((ports == NULL) || (numports == 0)) {
		return;
	}

	for (i = 0; i < numports; i++) {
		free(ports[i].ip_name);
	}
	free(ports);
}

void
free_nodelist(ssm_node_list_t *nodelist) {
	uint_t i;

	if (nodelist == NULL) {
		return;
	}

	for (i = 0; i < nodelist->numnodes; ++i) {
		free(nodelist->nodenames[i]);
	}
	free(nodelist->nodenames);
	free(nodelist->nodeids);
	free(nodelist);
}

void
print_nodelist(ssm_node_list_t *nodelist)
{
	uint_t	i;

	if (nodelist == NULL || nodelist->numnodes == 0)
		syslog(LOG_DEBUG,
		    "%s is empty.",
		    SCHA_NODELIST);
	else
		syslog(LOG_DEBUG,
		    "%s has %d element(s).",
		    SCHA_NODELIST,
		    nodelist->numnodes);

	for (i = 0; i < nodelist->numnodes; i++) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "  %s[%d]  NodeID %d",
		    SCHA_NODELIST,
		    i,
		    nodelist->nodeids[i]);
	}
}

/*
 * Returns B_TRUE if the switch with the given name is enabled
 * Returns B_FALSE if the switch with the given name is disabled
 */
static boolean_t
is_resource_switch_set(const char *cluster, char *rs, char *rg, const char *tag)
{
	scha_err_t		scha_rc;
	scha_resource_t		rs_handle;
	scha_switch_t		switch_prop = SCHA_SWITCH_DISABLED;

retry_rs:
	scha_rc = scha_resource_open_zone(cluster, rs, rg, &rs_handle);
	if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource handle for %s while "
		    "querying for property %s: %s.",
		    rs,
		    SCHA_MONITORED_SWITCH,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	scha_rc = scha_resource_get_zone(cluster, rs_handle, tag, &switch_prop);
	if (scha_rc == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource information.");
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
		goto retry_rs;
	} else if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource property %s for %s: %s.",
		    tag, rs, get_scha_error_string(scha_rc));
		exit(1);
	}

	/*
	 * Using the property value after the close() is ok because we've
	 * allocated the space for it.
	 */
	(void) scha_resource_close(rs_handle);
	return (switch_prop == SCHA_SWITCH_ENABLED ? B_TRUE : B_FALSE);
}

/* returns B_TRUE if the resource monitor is enabled, B_FALSE otherwise */
boolean_t
is_resource_monitored(const char *cluster, char *rs, char *rg)
{
	return (is_resource_switch_set(cluster, rs, rg, SCHA_MONITORED_SWITCH));
}

/* returns B_TRUE if the resource is enabled, B_FALSE otherwise */
boolean_t
is_resource_on(const char *cluster, char *rs, char *rg)
{
	return (is_resource_switch_set(cluster, rs, rg, SCHA_ON_OFF_SWITCH));
}

/*
 * Returns the nodeid of the local node where this function is running.
 */
uint_t
get_local_nodeid(void)
{
	scha_err_t		scha_rc;
	scha_cluster_t		cluster_handle;
	uint_t			nodeid = 0;

	scha_rc = scha_cluster_open(&cluster_handle);
	if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the cluster handle while "
		    "querying for property %s: %s.",
		    SCHA_NODEID_LOCAL,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	scha_rc = scha_cluster_get(cluster_handle, SCHA_NODEID_LOCAL,
	    &nodeid);
	if (scha_rc == SCHA_ERR_SEQID) {
		/* Just ignore the SCHA_ERR_SEQID error. */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Ignoring the SCHA_ERR_SEQID while retrieving %s",
		    SCHA_NODEID_LOCAL);
	} else if (scha_rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the cluster property %s: %s.",
		    SCHA_NODEID_LOCAL,
		    get_scha_error_string(scha_rc));
		exit(1);
	}

	/*
	 * Using the property value after the close() is ok because we've
	 * allocated the space for it.
	 */
	(void) scha_cluster_close(cluster_handle);
	return (nodeid);
}

/*
 * Break a string of format "keyword=values" into two substrings, pointed
 * by 'key' and 'val'.  The delimiter is '='.
 *
 * Caller frees memory of key and val after the return of the function.
 */
int
str_kv(char *s, char **key, char **val)
{
	char *tmp;

	if (s == NULL)
		return (1);

	tmp = strdup(s);
	if (tmp == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	*key = strchr(tmp, '=');

	if (*key == NULL) {
		return (1);
	} else {
		/* move past '=' character */
		*val = strdup(*key + 1);
		**key = '\0';
		*key = strdup(tmp);
		free(tmp);

		if (*key == NULL || *val == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Out of memory.");
			exit(1);
		}
	}

	return (0);
}


/*
 * Converts the input string of format "s1,s2,..." to scha_str_array_t
 * Returns NULL when the input string is NULL.
 *
 * Caller should call free_strarray() to release memory allocated by this call.
 */
int
str_to_strarray(char *val, scha_str_array_t **sa)
{
	char *temp, **ptr, *s, *valcopy;

	if (val == NULL) {
		*sa = NULL;
		return (0);
	}

	*sa = (scha_str_array_t *)calloc(1, sizeof (scha_str_array_t));
	if (*sa == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	if (strlen(val) == 0) {
		/* an empty string array will be returned */
		return (0);
	}

	valcopy = strdup(val);
	if (valcopy == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	s = valcopy;
	do {
		temp = strchr(s, ',');
		ptr = (char **)realloc((*sa)->str_array,
		    ((*sa)->array_cnt + 1) * (sizeof (char *)));
		if (ptr == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Out of memory.");
			exit(1);
		}

		if (temp != NULL)
			*temp = '\0';

		ptr[(*sa)->array_cnt] = strdup(s);
		if (ptr[(*sa)->array_cnt] == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Out of memory.");
			exit(1);
		}

		/* move past NULL character */
		s = temp + 1;
		(*sa)->array_cnt++;
		(*sa)->str_array = ptr;
	} while (temp != NULL);

	free(valcopy);
	return (0);
}

/*
 * Copy a stringarray (similar to strdup).
 * The strarray to copy must be valid and cannot be a NULL pointer.
 *
 * This routine will return NULL if there is no memory available.
 *
 * Caller should call free_strarray() to release memory allocated by this call.
 */
scha_str_array_t *
strarraydup(scha_str_array_t *sa)
{
	scha_str_array_t	*new_sa;
	uint_t			i;

	new_sa = (scha_str_array_t *)calloc(1, sizeof (scha_str_array_t));
	if (new_sa == NULL) {
		return (NULL);
	}

	new_sa->array_cnt = sa->array_cnt;
	new_sa->is_ALL_value = sa->is_ALL_value;

	if (sa->array_cnt == 0 || sa->str_array == NULL) {
		new_sa->array_cnt = 0;
		new_sa->str_array = NULL;
	} else {
		new_sa->str_array = (char **)calloc(new_sa->array_cnt,
		    sizeof (char *));

		for (i = 0; i < new_sa->array_cnt; i++) {
			if (sa->str_array[i] == NULL) {
				new_sa->str_array[i] = NULL;
			} else {
				new_sa->str_array[i] = strdup(sa->str_array[i]);
				if (new_sa->str_array[i] == NULL) {
					return (NULL);
				}
			}
		}
	}

	return (new_sa);
}

/*
 * Free the memory created by calls to str_to_strarray() or strarraydup()
 */
void
free_strarray(scha_str_array_t *sa)
{
	uint_t	i;

	if (sa == NULL)
		return;

	for (i = 0; i < sa->array_cnt; i++) {
		free(sa->str_array[i]);
	}

	free(sa->str_array);
	free(sa);
}

#ifdef SCTP_RGM_SUPPORT
static ssm_prop_err_t
process_net_resources(struct ssm_net_resource_list *net_resource)
{
	ssm_prop_err_t	rcode = SSM_PROP_ERR_NOERR;
	uint_t		i;

	for (i = 1; i < net_resource->snrl_num_snr; i++) {
		if (strcmp(net_resource->snrl_snr_list[0].snr_rgname,
		    net_resource->snrl_snr_list[i].snr_rgname)) {
			/*
			 * SCMSGS
			 * @explanation
			 * The shared addresses used by the SCTP service
			 * belong to different resource groups.
			 * @user_action
			 * Make sure the shared addresses belong to the same
			 * resource group.
			 */
			(void) sc_syslog_msg_log(log_handle,
			SC_SYSLOG_ERROR, MESSAGE,
			"SCTP services should depend on SharedAddress "
			"resources belonging to the same resource group");
			exit(1);
		}
	}
	return (rcode);
}
#endif
