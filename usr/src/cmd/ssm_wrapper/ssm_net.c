/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ssm_net.c - code for collecting network data need to make the ssm call
 */

#pragma ident	"@(#)ssm_net.c	1.31	08/06/18 SMI"

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>
#include <netdb.h>
#include <scha.h>
#include "ssm_util.h"

static int get_ipaddrs_from_r(char *rname, uint_t, ssm_net_resource_t *snr,
    char *zname);
static boolean_t is_shared_address(char *rname, char *zname);
static boolean_t is_nodeid_valid(int nodeid, char *zname);
static int get_rg_primary_node(char *rgname, char *zname);
static void get_nodes_where_ip_is_available(char *rname, char *rgname,
    ssm_node_list_t **allnodeids, char *zname);

static char internal_err_str[INT_ERR_MSG_SIZE];

/*
 * Get the IP addresses from multiple resources.
 */
ssm_prop_err_t
get_ipaddrs_from_rlist(const scha_str_array_t *r_array,
    struct ssm_net_resource_list **snrl,
    const ssm_port_and_proto_t *ports,
    uint_t numports, char *zname)
{
	uint_t				proto6_ok = 0;
	uint_t 				i;
	int				rc;
	boolean_t			bool_rc;
	ssm_net_resource_list_t 	*snrlp;

	/* snrl cannot be NULL */
	*snrl = NULL;

	if (r_array == NULL || r_array->array_cnt == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The resource dependencies property (any of strong/weak/
		 * restart/offline-restart) must contain
		 * at least one SharedAddress network resource.
		 * @user_action
		 * Specify a SharedAddress network resource in at least
		 * one of the resource dependencies properties of this
		 * resource.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "No resource dependencies properties of this resource"
		    " contain a SharedAddress network resource.");
		exit(1);
	}

	snrlp = (ssm_net_resource_list_t *)calloc(1,
	    sizeof (ssm_net_resource_list_t));

	if (snrlp == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	/* Determine if we care about V6 resolutions */
	for (i = 0; i < numports; i++) {
		if (ports[i].proto6_supported) {
			proto6_ok = 1;
			break;
		}
	}

	/* Initialize the values in the snrl */
	snrlp->snrl_num_snr = 0;
	snrlp->snrl_snr_list = NULL;

	/* Cycle through the resources and store the SharedAddress info */
	for (i = 0; i < r_array->array_cnt; i++) {

		if (r_array->str_array[i] == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * An invalid entry was found in the named property.
			 * The position index, which starts at 0 for the first
			 * element in the list, indicates which element in the
			 * property list was invalid.
			 * @user_action
			 * Make sure the property has a valid value.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Entry at position %d in property %s was invalid.",
			    i,
			    SSM_PROP_NET_RES);
			exit(1);
		}

		/* Make sure this looks like a right resource type */
		bool_rc = is_shared_address(r_array->str_array[i], zname);
		if (bool_rc != B_TRUE) {
			/*
			 * SCMSGS
			 * @explanation
			 * The dependee resource is not a SharedAddress
			 * resource.
			 * @user_action
			 * Specify at least one SharedAddresses resource
			 * as a dependee.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Resource %s named in one of the resource "
			    "dependencies properties is not a "
			    "SharedAddress resource.",
			    r_array->str_array[i]);
			exit(1);
		}

		/*
		 * The current resource is a SharedAddress resource, so
		 * allocate space for it and increment the counter
		 * of network resources
		 */
		snrlp->snrl_num_snr++;
		snrlp->snrl_snr_list =
		    (ssm_net_resource_t *)realloc((void *)snrlp->snrl_snr_list,
		    snrlp->snrl_num_snr * sizeof (ssm_net_resource_t));

		if (snrlp->snrl_snr_list == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Out of memory.");
			exit(1);
		}

		/* Add the ip addresses from this resource to our list */
		rc = get_ipaddrs_from_r(r_array->str_array[i], proto6_ok,
		    &(snrlp->snrl_snr_list[snrlp->snrl_num_snr - 1]), zname);

		if (rc != SSM_OK) {
			/*
			 * SCMSGS
			 * @explanation
			 * The networking information for the resource could
			 * not be retrieved.
			 * @user_action
			 * Save a copy of the /var/adm/messages files on all
			 * nodes. Contact your authorized Sun service provider
			 * for assistance in diagnosing the problem.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Failed to get host names from resource %s.",
			    r_array->str_array[i]);
			exit(1);
		}

	} /* for all resources */

	*snrl = snrlp;
	return (SSM_PROP_ERR_NOERR);
}

/*
 * Get the IP addresses from one network resource.
 * Assume the memory pointed by snr is pre-allocated
 */
int
get_ipaddrs_from_r(char *rname, uint_t proto6, ssm_net_resource_t *snr,
    char *zname)
{
	scha_err_t		e;
	scha_extprop_value_t	*extprop = NULL;
	scha_str_array_t	*strarray;
	in6_addr_t		ip[2];		/* IPV6 */
	int			rc;
	uint_t			j, k;
	scha_resource_t		rs_handle;
	char			*rgname;

	/* Initialize the network resource data. snr cannot be NULL. */
	snr->snr_name = NULL;
	snr->snr_rgname = NULL;
	snr->snr_currnodeid = INVALID_NODEID;
	snr->snr_allnodeids = NULL;
	snr->snr_numip = 0;
	snr->snr_iplist = NULL;

	/* NULL RG name means the API will fill it in - 4214120 */
retry_rs:
	e = scha_resource_open_zone(zname, rname, NULL, &rs_handle);
	if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Access to the object named failed. The reason for the
		 * failure is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource handle for %s "
		    "while querying for property %s: %s.",
		    rname,
		    SHARED_ADDRESS_HOSTNAMELIST,
		    get_scha_error_string(e));
		exit(1);
	}

	e = scha_resource_get_zone(zname, rs_handle, SCHA_EXTENSION,
		SHARED_ADDRESS_HOSTNAMELIST, &extprop);
	if (e == SCHA_ERR_SEQID) {
		/*
		 * SCMSGS
		 * @explanation
		 * An update to cluster configuration occured while resource
		 * properties were being retrieved
		 * @user_action
		 * Ignore the message.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource information.");
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
		goto retry_rs;
	} else if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The query for a property failed. The reason for the failure
		 * is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource property %s for %s: %s.",
		    SHARED_ADDRESS_HOSTNAMELIST,
		    rname,
		    get_scha_error_string(e));
		exit(1);
	}

	/* Get the rg name for this resource */
	e = scha_resource_get_zone(zname, rs_handle, SCHA_GROUP, &rgname);
	if (e == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource information.");
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
		goto retry_rs;
	} else if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource property %s for %s: %s.",
		    SCHA_GROUP,
		    rname,
		    get_scha_error_string(e));
		exit(1);
	}

	if (rgname == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * An unexpected value was returned for the named property.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "The value returned for property %s for resource %s "
		    "was invalid.",
		    SCHA_GROUP,
		    rname);
		exit(1);
	}

	/* Set the RG name */
	if (extprop == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "The value returned for property %s for resource %s "
		    "was invalid.",
		    SHARED_ADDRESS_HOSTNAMELIST,
		    rname);
		exit(1);
	}

	strarray = extprop->val.val_strarray;
	/* presumably, the SharedAddress implementation wouldn't allow this */
	if (strarray->array_cnt < 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * The named property does not have any hostnames set for it.
		 * @user_action
		 * Re-create the named resource with one or more hostnames.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Property %s has no hostnames for resource %s.",
		    SHARED_ADDRESS_HOSTNAMELIST,
		    rname);
		exit(1);
	}

	/*
	 * Allocate storage for list of IP addresses. Since each hostname
	 * can have both a V4 and a V6 mapping, allocate for 2 times the
	 * number of hostnames.
	 */
	snr->snr_iplist = (in6_addr_t *)calloc(strarray->array_cnt * 2,
	    sizeof (in6_addr_t));

	if (snr->snr_iplist == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	k = 0;
	for (j = 0; j < strarray->array_cnt; j++) {
		if (strarray->str_array[j] == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Entry at position %d in property %s was invalid.",
			    j,
			    SHARED_ADDRESS_HOSTNAMELIST);
			exit(1);
		}

		bzero((char *)ip, sizeof (ip));
		rc = hostip_string_to_binary(strarray->str_array[j], ip);

		/*
		 * hostname resolution failed if there is no mappings, or
		 * if all mappings are V6 but V6 is not wanted.
		 */
		if (rc != SSM_OK || (!proto6 &&
		    !IN6_IS_ADDR_V4MAPPED(&ip[0]) &&
		    !IN6_IS_ADDR_V4MAPPED(&ip[1]))) {
			/*
			 * SCMSGS
			 * @explanation
			 * The hostname or IP address given could not be
			 * converted to an integer.
			 * @user_action
			 * Add the hostname to the /etc/inet/hosts file.
			 * Verify the settings in the /etc/nsswitch.conf file
			 * include "files" for host lookup.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Conversion of hostnames failed for %s.",
			    strarray->str_array[j]);
			/* XXX - Should this be a fatal error? */
			exit(1);
		}

		/*
		 * We could get both a V4 and V6 addresses for this
		 * hostname. Return both of them.
		 */
		if (!IN6_IS_ADDR_UNSPECIFIED(&ip[0]) &&
		    (proto6 || IN6_IS_ADDR_V4MAPPED(&ip[0]))) {
			snr->snr_numip++;
			snr->snr_iplist[k++] = ip[0];
		}
		if (!IN6_IS_ADDR_UNSPECIFIED(&ip[1]) &&
		    (proto6 || IN6_IS_ADDR_V4MAPPED(&ip[1]))) {
			snr->snr_numip++;
			snr->snr_iplist[k++] = ip[1];
		}

	} /* for all ip addresses for the resource */

	/* Set the RG name */
	snr->snr_rgname = strdup(rgname);
	if (snr->snr_rgname == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	/* Set the R name */
	snr->snr_name = strdup(rname);
	if (snr->snr_name == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	(void) scha_resource_close(rs_handle);

	/* Get the current primary of this RS(needed for set_primary_gifnode) */
	snr->snr_currnodeid = get_rg_primary_node(snr->snr_rgname, zname);

	/* Get all potential primaries and Auxilary Nodes */
	get_nodes_where_ip_is_available(snr->snr_name, snr->snr_rgname,
	    &snr->snr_allnodeids, zname);

	return (SSM_OK);
}


void
free_snrl(ssm_net_resource_list_t *snrl)
{
	uint_t	i;

	if (snrl == NULL)
		return;

	if (snrl->snrl_snr_list == NULL)
		return;

	for (i = 0; i < snrl->snrl_num_snr; i++) {
		free(snrl->snrl_snr_list[i].snr_name);
		free(snrl->snrl_snr_list[i].snr_rgname);
		free_nodelist(snrl->snrl_snr_list[i].snr_allnodeids);
		free(snrl->snrl_snr_list[i].snr_iplist);
	}

	free(snrl->snrl_snr_list);
	free(snrl);
}

void
print_snrl(const ssm_net_resource_list_t *snrl) {
	uint_t	i, j;

	if (snrl == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "%s was NULL.",
		    SSM_PROP_NET_RES);
		return;
	}

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "%s has %d element(s).",
	    SSM_PROP_NET_RES,
	    snrl->snrl_num_snr);

	if (snrl->snrl_snr_list == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "  %s is NULL",
		    SSM_PROP_NET_RES);

		if (snrl->snrl_num_snr != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "  ERROR: The number of %s is %d but list is NULL.",
			    SSM_PROP_NET_RES,
			    snrl->snrl_num_snr);
			return;
		}
	}

	for (i = 0; i < snrl->snrl_num_snr; i++) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "  %s[%d]  Name %s  RG %s  CurrNodeId %d  NumIP %d:",
		    SSM_PROP_NET_RES,
		    i,
		    (snrl->snrl_snr_list[i].snr_name != NULL ?
		    snrl->snrl_snr_list[i].snr_name : "NULL"),
		    (snrl->snrl_snr_list[i].snr_rgname != NULL ?
		    snrl->snrl_snr_list[i].snr_rgname : "NULL"),
		    snrl->snrl_snr_list[i].snr_currnodeid,
		    snrl->snrl_snr_list[i].snr_numip);

		/* we don't call print_nodelist for formatting reasons */
		if (snrl->snrl_snr_list[i].snr_allnodeids == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "    %s is NULL.",
			    SCHA_NODELIST);
			continue;
		}

		for (j = 0; j < snrl->snrl_snr_list[i].snr_allnodeids->numnodes;
		    j++) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "    %s[%d]  %d",
			    SCHA_NODELIST,
			    j,
			    snrl->snrl_snr_list[i].snr_allnodeids->nodeids[j]);
		}

		if (snrl->snrl_snr_list[i].snr_iplist == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "    IP list is NULL.");
			continue;
		}

		for (j = 0; j < snrl->snrl_snr_list[i].snr_numip; j++) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "    %s[%d]  %s (dotted decimal)",
			    SHARED_ADDRESS_HOSTNAMELIST,
			    j,
			    ip_to_str(snrl->snrl_snr_list[i].snr_iplist[j]));
		}
	}
}


boolean_t
is_shared_address(char *rname, char *zname)
{
	scha_err_t		e;
	scha_resourcetype_t	rt_handle;
	scha_resource_t		rs_handle;
	char			*rtname = NULL;
	boolean_t		bool_val = B_FALSE;

	if (rname == NULL) {
		(void) sprintf(internal_err_str,
		    "Failed to check the resource type name: "
		    "resource name is null");
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

retry_rs:
	e = scha_resource_open_zone(zname, rname, NULL, &rs_handle);
	if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource handle for %s "
		    "while querying for property %s: %s.",
		    rname,
		    SCHA_TYPE,
		    get_scha_error_string(e));
		if (e == SCHA_ERR_RSRC) {
			/*
			 * SCMSGS
			 * @explanation
			 * The resource dependencies property (one of
			 * strong/weak/restart/offline-restart) contains
			 * an invalid resource. This is an internal error
			 * and should not occur.
			 * @user_action
			 * Save a copy of the /var/adm/messages files on all
			 * nodes. Contact your authorized Sun service provider
			 * for assistance in diagnosing the problem.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Resource %s named in one of the resource "
			    "dependencies properties does not exist.",
			    rname);
		}
		exit(1);
	}

	e = scha_resource_get_zone(zname, rs_handle, SCHA_TYPE, &rtname);
	if (e == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource information.");
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
		goto retry_rs;
	} else if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource property %s for %s: %s.",
		    SCHA_TYPE,
		    rname,
		    get_scha_error_string(e));
		exit(1);
	}

	if (rtname == NULL) {
		(void) sprintf(internal_err_str,
		    "Failed to retrieve the resource type name for %s: "
		    "Resource type name is null", rname);
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

retry_rt:
	e = scha_resourcetype_open_zone(zname, rtname, &rt_handle);
	if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Access to the object named failed. The reason for the
		 * failure is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource type handle for %s "
		    "while querying for property %s: %s.",
		    rtname,
		    SCHA_IS_SHARED_ADDRESS,
		    get_scha_error_string(e));
		exit(1);
	}

	e = scha_resourcetype_get_zone(zname, rt_handle, SCHA_IS_SHARED_ADDRESS,
	    &bool_val);
	if (e == SCHA_ERR_SEQID) {
		/*
		 * SCMSGS
		 * @explanation
		 * An update to cluster configuration occured while resource
		 * type properties were being retrieved
		 * @user_action
		 * Ignore the message.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource type information.");
		(void) scha_resourcetype_close(rt_handle);
		rt_handle = NULL;
		goto retry_rt;
	} else if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The query for a property failed. The reason for the failure
		 * is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource type property %s for %s:"
		    " %s.",
		    SCHA_IS_SHARED_ADDRESS,
		    rtname,
		    get_scha_error_string(e));
		exit(1);
	}


	(void) scha_resource_close(rs_handle);
	(void) scha_resourcetype_close(rt_handle);

	return (bool_val);
}


/*
 * is_ip_in_nru()
 *
 * Given an IP address, this function iterates over the known IPs in
 * the network resources used by this resource and returns B_TRUE if the
 * IP is found in one of the network resources.  B_FALSE is returned if the
 * IP address is not found in any of the network resources.
 */
boolean_t
is_ip_in_nru(in6_addr_t ip, const ssm_net_resource_list_t *snrl) {
	uint_t	i, j;

	/* If there are no network resources, the IP can't be found */
	if (snrl == NULL) {
		return (B_FALSE);
	}

	/* For each Net Res, cycle through all of the IPs and compare */
	for (i = 0; i < snrl->snrl_num_snr; i++) {
		for (j = 0; j < snrl->snrl_snr_list[i].snr_numip; j++) {
			if (IN6_ARE_ADDR_EQUAL(&ip,
			    &snrl->snrl_snr_list[i].snr_iplist[j])) {
				return (B_TRUE);
			}
		}
	}

	return (B_FALSE);
}

/*
 * hostip_string_to_binary()
 * Convert a hostname or a string respresentation of an IP address into
 * binary form (in6_addr_t).
 * If the hostname maps to an IPv6 address, the binary form of the
 * address is returned in 'ip[0]'. 'ip[1]' will be zero'ed out.
 * If the hostname maps to an IPv4 address, the V4-mapped version of the
 * address is returned in 'ip[0]'. 'ip[1]' will be zero'ed out. The
 * v4-mapped address is expected by ssm.h for handling of V4 addresses.
 * If the hostname maps to both V4 and V6, then both are returned (V4 as
 * V4-mapped). The order in 'ip' is not definite.
 *
 * 'ip' must point to an array of at least 2 in6_addr_t's.
 * Note: An in6_addr_t of all zero's is a valid IPv6 address -- the V6
 * unspecified address. Thus, a hostname that maps to that
 * address won't be supported properly. (V4 INADDR_ANY will be fine
 * though, since it'll be V4-mapped, and so it won't be all zero's.
 */
int
hostip_string_to_binary(char *hostorip, in6_addr_t *ip)
{
	struct hostent	*hp = NULL;
	int		i, rc;
	in6_addr_t	cur;

	if (hostorip == NULL || ip == NULL) {
		(void) sprintf(internal_err_str,
		    "Conversion of host names failed;"
		    "NULL value passed for hostname or IP address");
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.",
		    internal_err_str);
		exit(1);
	}

	/*
	 * Convert address to binary form. This call will succeed even if
	 * hostorip is an IP address in string form instead of hostname.
	 * getipnodebyname() is used to support both V4 and V6 addresses.
	 */
	hp = getipnodebyname(hostorip, AF_INET6, AI_DEFAULT|AI_ALL, &rc);
	if (hp == NULL || hp->h_addr_list[0] == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The data service failed to retrieve the host information.
		 * @user_action
		 * If the logical hostname and shared address entries are
		 * specified in the /etc/inet/hosts file, check that the
		 * entries are correct. Verify the settings in the
		 * /etc/nsswitch.conf file include "files" for host lookup. If
		 * these are correct, check the health of the name server. For
		 * more error information, check the syslog messages.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
			"Failed to retrieve the host "
			"information for %s: %s.",
			hostorip, hstrerror(h_errno));
		if (hp) {
			freehostent(hp);
		}
		return (SSM_ERROR);
	}

	(void) memcpy(&ip[0], hp->h_addr_list[0], sizeof (in6_addr_t));
	(void) memset(&ip[1], 0, sizeof (in6_addr_t));

	for (i = 1; hp->h_addr_list[i] != NULL; i++) {
		/*
		 * Need to memcpy the address into a in6_addr_t structure,
		 * since h_addr_list[] aren't always aligned properly.
		 */
		(void) memcpy(&cur, hp->h_addr_list[i], sizeof (in6_addr_t));

		if (IN6_IS_ADDR_V4MAPPED(ip)) {
			/* Looking for the first V6 address */
			if (!IN6_IS_ADDR_V4MAPPED(&cur)) {
				ip[1] = cur;
				break;
			}
		} else {
			/* Looking for the first V4 address */
			if (IN6_IS_ADDR_V4MAPPED(&cur)) {
				ip[1] = cur;
				break;
			}
		}
	}

	freehostent(hp);
	return (SSM_OK);
}


/*
 * Convert a string (representing the nodename or nodeid) to a nodeid.
 * If the string is not a valid nodeid, then INVALID_NODEID is returned.
 */
int
ssm_get_nodeid(char *node_string, char *zname)
{
	scha_err_t		e;
	scha_cluster_t		clust_handle;
	int			node_int = INVALID_NODEID;
	char			*endp = NULL;

	if (node_string == NULL) {
		(void) sprintf(internal_err_str,
		    "Can't convert nodename to nodeid; "
		    "Nodename is null");
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

	e = scha_cluster_open_zone(zname, &clust_handle);
	if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the cluster handle "
		    "while querying for property %s: %s.",
		    SCHA_NODEID_NODENAME,
		    get_scha_error_string(e));
		exit(1);
	}

	e = scha_cluster_get_zone(zname, clust_handle, SCHA_NODEID_NODENAME,
		node_string, &node_int);
	if (e == SCHA_ERR_SEQID) {
		/* Just ignore the SCHA_ERR_SEQID error. */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Ignoring the SCHA_ERR_SEQID while retrieving %s",
		    SCHA_NODEID_NODENAME);
	} else if (e != SCHA_ERR_NOERR) {
		if (e != SCHA_ERR_NODE) {
			/*
			 * SCMSGS
			 * @explanation
			 * The query for a property failed. The reason for the
			 * failure is given in the message.
			 * @user_action
			 * Save a copy of the /var/adm/messages files on all
			 * nodes. Contact your authorized Sun service provider
			 * for assistance in diagnosing the problem.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Failed to retrieve the cluster property %s"
			    " for %s: %s.",
			    SCHA_NODEID_NODENAME,
			    node_string,
			    get_scha_error_string(e));
			exit(1);
		}
	}

	/*
	 * XXX - The api currently will return success if the nodename was
	 * a string with an integer nodeid. The following code is in
	 * place in case that behavior goes away.
	 *
	 * The above query failed because the nodename wasn't valid.
	 * Maybe what we have is actually a string with an integer nodeid.
	 */
	if (e == SCHA_ERR_NODE) {
		/*
		 * errno.h is missing void arg, so suppress lint error.
		 * [should be errno(void) rather than errno()]
		 */
		errno = 0;			/*lint !e746 */
		/* Try to convert the string to an integer */
		node_int = strtol(node_string, &endp, 10);
		if (errno != 0 || *endp != '\0') {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "%s is not a valid nodename or nodeid.",
			    node_string);
			return (INVALID_NODEID);
		}

		/* We have a potentially valid node id. Verify it */
		if (is_nodeid_valid(node_int, zname) == B_FALSE) {
			syslog(LOG_DEBUG,
			    "%s is not a valid nodename or nodeid.",
			    node_string);
			return (INVALID_NODEID);
		}
	}

	(void) scha_cluster_close(clust_handle);
	return (node_int);
}

/*
 * is_nodeid_valid() verifies that the nodeid passed to it is a valid nodeid.
 * It is used to verify user input for properties like Load_balancing_weights,
 * where the user specifies nodeids in the property value.  This function
 * takes the nodeid as a signed integer so that the case where a user
 * specifies a negative value can be detected.
 */
boolean_t
is_nodeid_valid(int nodeid, char *zname)
{
	boolean_t		valid = B_FALSE;
	uint_t			i;
	scha_err_t		e;
	scha_uint_array_t	*all_nodeids = NULL;
	scha_cluster_t		clust_handle;

	if (nodeid == INVALID_NODEID) {
		return (B_FALSE);
	}

	e = scha_cluster_open_zone(zname, &clust_handle);
	if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the cluster handle "
		    "while querying for property %s: %s.",
		    SCHA_ALL_NODEIDS,
		    get_scha_error_string(e));
		exit(1);
	}

	e = scha_cluster_get_zone(zname, clust_handle, SCHA_ALL_NODEIDS,
		&all_nodeids);
	if (e == SCHA_ERR_SEQID) {
		/* Just ignore the SCHA_ERR_SEQID error. */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Ignoring the SCHA_ERR_SEQID while retrieving %s",
		    SCHA_ALL_NODEIDS);
	} else if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the cluster property %s: %s.",
		    SCHA_ALL_NODEIDS,
		    get_scha_error_string(e));
		exit(1);
	}

	if (all_nodeids == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.",
		    "The array of nodeids returned from the API was NULL");
		exit(1);
	}

	for (i = 0; i < all_nodeids->array_cnt; i++) {
		if ((uint_t)nodeid == all_nodeids->uint_array[i]) {
			valid = B_TRUE;
			break;
		}
	}

	(void) scha_cluster_close(clust_handle);
	return (valid);
}

/*
 * Looks through the given nodelist and returns B_TRUE if the given nodename
 * is contained in the nodelist, B_FALSE otherwise
 */
boolean_t
is_nodename_in_nodelist(const char *nodename, const ssm_node_list_t *nodelist)
{
	boolean_t	found = B_FALSE;
	uint_t		i;

	if (nodename == NULL || nodelist == NULL) {
		return (B_FALSE);
	}

	for (i = 0; i < nodelist->numnodes; i++) {
		if (strcmp(nodename, nodelist->nodenames[i]) == 0) {
			found = B_TRUE;
			break;
		}
	}

	return (found);
}

boolean_t
is_node_in_nodelist(uint_t nodeid, const ssm_node_list_t *nodelist)
{
	boolean_t	found = B_FALSE;
	uint_t		i;

	if (nodelist == NULL) {
		return (B_FALSE);
	}

	for (i = 0; i < nodelist->numnodes; i++) {
		if (nodeid == nodelist->nodeids[i]) {
			found = B_TRUE;
			break;
		}
	}

	return (found);
}

/*
 * Given a Failover RG name, returns the
 * current primary node of this RG. exit()s with 1 if there is a
 * serious problem (API call failures), returns -1 if
 * the RG currently has no primaries
 */
int
get_rg_primary_node(char *rgname, char *zname)
{
	scha_err_t		e;
	scha_resourcegroup_t	rg_handle;
	scha_str_array_t	*nodelist;
	int			nid = INVALID_NODEID;
	uint_t			i;
	int			n;
	scha_rgstate_t		state;

	if (rgname == NULL) {
		(void) sprintf(internal_err_str,
		    "Can't get primary node; Resource group name is NULL");
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.", internal_err_str);
		exit(1);
	}

retry_rg:
	e = scha_resourcegroup_open_zone(zname, rgname, &rg_handle);
	if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Access to the object named failed. The reason for the
		 * failure is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource group handle for %s "
		    "while querying for property %s: %s.",
		    rgname,
		    SCHA_MAXIMUM_PRIMARIES,
		    get_scha_error_string(e));
		exit(1);
	}

	/* Maximum primaries and desired primaries must be 1 */
	e = scha_resourcegroup_get_zone(zname, rg_handle,
	    SCHA_MAXIMUM_PRIMARIES, &n);
	if (e == SCHA_ERR_SEQID) {
		/*
		 * SCMSGS
		 * @explanation
		 * An update to cluster configuration occured while resource
		 * group properties were being retrieved
		 * @user_action
		 * Ignore the message.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource group information.");
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
		goto retry_rg;
	} else if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The query for a property failed. The reason for the failure
		 * is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource group property %s for %s: "
		    "%s.",
		    SCHA_MAXIMUM_PRIMARIES,
		    rgname,
		    get_scha_error_string(e));
		exit(1);
	}
	if (n > 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * The named property has an unexpected value.
		 * @user_action
		 * Change the value of the property to be 1.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "%s is %d for %s. It should be 1.",
		    SCHA_MAXIMUM_PRIMARIES,
		    n,
		    rgname);
		exit(1);
	}

	e = scha_resourcegroup_get_zone(zname, rg_handle,
	    SCHA_DESIRED_PRIMARIES, &n);
	if (e == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource group information.");
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
		goto retry_rg;
	} else if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource group property %s for %s: "
		    "%s.",
		    SCHA_DESIRED_PRIMARIES,
		    rgname,
		    get_scha_error_string(e));
		exit(1);
	}
	if (n > 1) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "%s is %d for %s. It should be 1.",
		    SCHA_DESIRED_PRIMARIES,
		    n,
		    rgname);
		exit(1);
	}

	e = scha_resourcegroup_get_zone(zname, rg_handle, SCHA_NODELIST,
		&nodelist);
	if (e == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource group information.");
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
		goto retry_rg;
	} else if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource group property %s for %s: "
		    "%s.",
		    SCHA_NODELIST,
		    rgname,
		    get_scha_error_string(e));
		exit(1);
	}

	if (nodelist == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The value returned for the named property was not valid.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Invalid value was returned for resource group property "
		    "%s for %s.",
		    SCHA_NODELIST,
		    rgname);
		exit(1);
	}

	nid = INVALID_NODEID;
	for (i = 0; i < nodelist->array_cnt; i++) {
		if (nodelist->str_array[i] == NULL) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Entry at position %d in property %s was invalid.",
			    i,
			    SCHA_NODELIST);
			exit(1);
		}

		e = scha_resourcegroup_get_zone(zname, rg_handle,
		    SCHA_RG_STATE_NODE,  nodelist->str_array[i], &state);
		if (e == SCHA_ERR_SEQID) {
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_NOTICE, MESSAGE,
			    "Retrying to retrieve the resource group "
			    "information.");
			(void) scha_resourcegroup_close(rg_handle);
			rg_handle = NULL;
			goto retry_rg;
		}
		if (e != SCHA_ERR_NOERR) {
			/*
			 * BugID 4236927: It is ok if the query fails
			 * because the node is not in the cluster. In
			 * this case, we will continue to look for the
			 * master on a different node.
			 */
			if (e == SCHA_ERR_NODE) {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_DEBUG,
				    "Node %s is not in the cluster.",
				    nodelist->str_array[i]);
				continue;
			}
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Failed to retrieve the "
			    "resource group property %s for %s: %s.",
			    SCHA_RG_STATE_NODE,
			    rgname,
			    get_scha_error_string(e));
			exit(1);
		}

		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG, "rg_state_node for rg %s, node %s: %d",
		    rgname, nodelist->str_array[i], state);
		if ((state == SCHA_RGSTATE_ONLINE) ||
		    (state == SCHA_RGSTATE_PENDING_ONLINE)) {
			if (nid != INVALID_NODEID) {
				/* We are online on two different nodes! */
				/*
				 * SCMSGS
				 * @explanation
				 * The named resource group should be online
				 * on only one node, but it is actually online
				 * on more than one node.
				 * @user_action
				 * Save a copy of the /var/adm/messages files
				 * on all nodes. Contact your authorized Sun
				 * service provider for assistance in
				 * diagnosing the problem.
				 */
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_ERROR, MESSAGE,
				    "Resource group %s is online on more "
				    "than one node.",
				    rgname);
			}

			nid = ssm_get_nodeid(nodelist->str_array[i], zname);
			if (nid == INVALID_NODEID) {
				/*
				 * SCMSGS
				 * @explanation
				 * The nodeid for the given name could not be
				 * determined.
				 * @user_action
				 * Make sure that the name given is a valid
				 * node identifier or node name.
				 */
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_ERROR, MESSAGE,
				    "Failed to retrieve nodeid for %s.",
				    nodelist->str_array[i]);
				exit(1);
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_DEBUG,
				    "Resource group %s is online on nodeid %d.",
				    rgname,
				    nid);
			}
		}
	} /* for all nodes in the nodelist */

	/* It is OK for the RG to not be online. */
	if (nid == INVALID_NODEID) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "Resource group %s is not online on any of its primaries.",
		    rgname);
	}

	(void) scha_resourcegroup_close(rg_handle);
	return (nid);
}

void
get_nodes_where_ip_is_available(char *rname, char *rgname,
    ssm_node_list_t **allnodeids, char *zname)
{
	scha_err_t		e;
	scha_resource_t		rs_handle;
	scha_extprop_value_t	*extprop = NULL;
	scha_resourcegroup_t	rg_handle;
	scha_str_array_t	*rgnodes_array = NULL;
	ssm_node_list_t 	*rg_nodelist = NULL;
	ssm_node_list_t 	*aux_nodelist = NULL;

	/* allnodeids cannot be NULL */
	*allnodeids = NULL;

	/* Get the RG node list */
retry_rg:
	e = scha_resourcegroup_open_zone(zname, rgname, &rg_handle);
	if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource group handle for %s "
		    "while querying for property %s: %s.",
		    rgname,
		    SCHA_NODELIST,
		    get_scha_error_string(e));
		exit(1);
	}

	e = scha_resourcegroup_get_zone(zname, rg_handle, SCHA_NODELIST,
		&rgnodes_array);
	if (e == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource group information.");
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
		goto retry_rg;
	} else if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource group property %s for %s: "
		    "%s.",
		    SCHA_NODELIST,
		    rgname,
		    get_scha_error_string(e));
		exit(1);
	}

	if (rgnodes_array == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Invalid value was returned for resource group property "
		    "%s for %s.",
		    SCHA_NODELIST,
		    rgname);
		exit(1);
	}

	/* Get the Aux Node List */
retry_rs:
	e = scha_resource_open_zone(zname, rname, rgname, &rs_handle);
	if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource handle for %s "
		    "while querying for property %s: %s.",
		    rname,
		    SHARED_ADDRESS_AUXNODELIST,
		    get_scha_error_string(e));
		exit(1);
	}

	e = scha_resource_get_zone(zname, rs_handle, SCHA_EXTENSION,
	    SHARED_ADDRESS_AUXNODELIST, &extprop);
	if (e == SCHA_ERR_SEQID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Retrying to retrieve the resource information.");
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
		goto retry_rs;
	} else if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the resource property %s for %s: %s.",
		    SHARED_ADDRESS_AUXNODELIST,
		    rname,
		    get_scha_error_string(e));
		exit(1);
	}

	if (extprop == NULL || extprop->val.val_strarray == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The value returned for the named property was not valid.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Invalid value was returned for resource property "
		    "%s for %s.",
		    SHARED_ADDRESS_AUXNODELIST,
		    rname);
		exit(1);
	}

	/* convert the node arrays to node_list_t's */
	(void) process_nodelist(rgnodes_array, &rg_nodelist, zname);
	(void) process_nodelist(extprop->val.val_strarray, &aux_nodelist,
	    zname);

	(void) scha_resourcegroup_close(rg_handle);
	(void) scha_resource_close(rs_handle);

	/* Combine the two lists. There should be no dup nodes (scrgadm(1M)) */
	nodelistcat(rg_nodelist, aux_nodelist);
	*allnodeids = rg_nodelist;
	free_nodelist(aux_nodelist);
}

#ifdef SCTP_RGM_SUPPORT
boolean_t
is_sctp_protocol(uint_t num_ports, const ssm_port_and_proto_t *port_list) {
	uint_t	i;

	for (i = 0; i < num_ports; i++) {
		if (port_list[i].proto == IPPROTO_SCTP) {
			return (B_TRUE);
		}
	}

	return (B_FALSE);
}
#endif
