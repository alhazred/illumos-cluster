/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ssm_common.c - code shared between the wrapper and the callback.
 *		This file may expand in the future.
 */

#pragma ident	"@(#)ssm_common.c	1.13	09/01/14 SMI"

#include <stdlib.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/sc_syslog_msg.h>
#include "ssm_util.h"

/*
 * Initialize the syslog with tag and facility.
 */

/* global handle used for all messages. */
sc_syslog_msg_handle_t log_handle;
static char ssm_syslog_tag[2048];

static int ssm_stop_retry(char *tag);

void
ssm_syslog_init(char *rname, char *rgname, char *rtname, char *curr_method)
{
	if (rname == NULL)
		rname = "invalid_resource";

	if (curr_method == NULL)
		curr_method = "invalid_method";

	if (rgname == NULL)
		rgname = "invalid_resource_group";

	if (rtname == NULL)
		rtname = "invalid_resource_type";

	(void) snprintf(ssm_syslog_tag, sizeof (ssm_syslog_tag),
	    "SC[%s,%s,%s,%s%s]", rtname, rgname,
		rname, "SSM_", curr_method);

	(void) sc_syslog_msg_initialize(&log_handle, "", "");
	(void) sc_syslog_msg_set_syslog_tag(ssm_syslog_tag);
}

/*
 * Launches the retry utility. The utility can be used to rebalance an
 * rg or to getoff this node. The pmf tag used is as follows:
 *
 * ssm_retry.rg.rebalance
 * --or--
 * ssm_retry.rg.rs.getoff
 *
 * Rebalance is run on a per resource group basis, but getoff runs on a
 * per resource basis. If ssm_start_retry detects that the tag is already
 * up and running, it exits silently.
 */
int
ssm_start_retry(char *rs, char *rg, char *rt, char *zone,
    ssm_retry_mode_t m, ulong_t d)
{
	char tag[SSM_NAFO_TAGSIZE];
	char cmd[SSM_NAFO_CMDSIZE];
	char pmf_cmd[SSM_NAFO_CMDSIZE];
	uint_t rc;

	/* build the tag and the command line depending on the mode */
	switch (m) {
	case MODE_REBALANCE:
		if (zone == NULL) {
			/*
			* For global zone, -Z option is not used
			*/
			(void) snprintf(tag, sizeof (tag), "%s.%s.rebalance",
			    SSM_RETRY_NAME, rg);
			(void) snprintf(cmd, sizeof (cmd),
			    "%s/%s -R %s -T %s -G %s -r",
			    SSM_CALLBACK_DIR, SSM_RETRY_NAME, rs, rt, rg);
		} else {
			(void) snprintf(tag, sizeof (tag), "%s.%s.rebalance",
			    SSM_RETRY_NAME, rg);
			(void) snprintf(cmd, sizeof (cmd),
			    "%s/%s -R %s -T %s -G %s -Z %s -r",
			    SSM_CALLBACK_DIR, SSM_RETRY_NAME, rs, rt, rg, zone);
		}
		break;
	case MODE_GETOFF:
		if (zone == NULL) {
			/*
			* For global zone, -Z option is not used
			*/
			(void) snprintf(tag, sizeof (tag), "%s.%s.%s.getoff",
			    SSM_RETRY_NAME, rg, rs);
			(void) snprintf(cmd, sizeof (cmd),
			    "%s/%s -R %s -T %s -G %s -g "
			    "-d %d", SSM_CALLBACK_DIR, SSM_RETRY_NAME,
			    rs, rt, rg, d);
		} else {
			(void) snprintf(tag, sizeof (tag), "%s.%s.%s.getoff",
			    SSM_RETRY_NAME, rg, rs);
			(void) snprintf(cmd, sizeof (cmd),
			    "%s/%s -R %s -T %s -G %s -Z %s -g "
			    "-d %d", SSM_CALLBACK_DIR, SSM_RETRY_NAME,
			    rs, rt, rg, zone, d);
		}
		break;
	default:
		return (1);
	}

	/* Rest of the code based on haip_start_retry(). */
	(void) snprintf(pmf_cmd, sizeof (pmf_cmd),
	    "/usr/cluster/bin/pmfadm -q %s", tag);
	rc = (uint_t)system(pmf_cmd);
	if (WIFEXITED(rc)) {
		if ((rc = WEXITSTATUS(rc)) == 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG, "ssm_retry utility with tag %s "
			    "running already", tag);
			return (0);		/* Already running */
		}
	} else {
		return (1);	/* Other problems */
	}

	/* launch the specified command under PMF */
	(void) snprintf(pmf_cmd, sizeof (pmf_cmd),
	    "/usr/cluster/bin/pmfadm -c %s %s", tag, cmd);
	rc = (uint_t)system(pmf_cmd);
	if (WIFEXITED(rc)) {
		if ((rc = WEXITSTATUS(rc)) == 0) {
			return (0);		/* Success */
		}
	}

	/* just use pmf_cmd as the err msg buffer */
	(void) snprintf(pmf_cmd, sizeof (pmf_cmd), "Failed to launch retry "
	    "utility (%s). Error code: %d", cmd, rc);
	(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.", pmf_cmd);
	return (1);
}

/*
 * Wrapper around ssm_stop_retry. Creates tag used by rebalance and
 * calls stop_retry
 */
int
ssm_stop_rebalance(char *rg)
{
	char tag[SSM_NAFO_TAGSIZE];

	(void) snprintf(tag, sizeof (tag), "%s.%s.rebalance", SSM_RETRY_NAME,
	    rg);

	return (ssm_stop_retry(tag));
}

/*
 * Wrapper around ssm_stop_retry. Creates tag used by getoff and
 * calls stop_retry
 */
int
ssm_stop_getoff(char *rg, char *rs)
{
	char tag[SSM_NAFO_TAGSIZE];

	(void) snprintf(tag, sizeof (tag), "%s.%s.%s.getoff", SSM_RETRY_NAME,
	    rg, rs);

	return (ssm_stop_retry(tag));
}

/*
 * Stops the retry utility running with the given tag
 */
int
ssm_stop_retry(char *tag)
{
	char pmf_cmd[SSM_NAFO_CMDSIZE];
	uint_t rc;

	(void) snprintf(pmf_cmd, sizeof (pmf_cmd),
	    "/usr/cluster/bin/pmfadm -q %s", tag);
	rc = (uint_t)system(pmf_cmd);
	if (WIFEXITED(rc)) {
		if ((rc = WEXITSTATUS(rc)) != 0) {
			return (0);		/* Already Stopped */
		}
	} else {
		return (1);	/* Other problems */
	}

	/*
	 * Run pmfadm -s to stop monitoring it
	 * also kill with SIGKILL
	 */
	(void) sprintf(pmf_cmd, "/usr/cluster/bin/pmfadm -s %s %d", tag,
	    SIGKILL);
	rc = (uint_t)system(pmf_cmd);
	if (WIFEXITED(rc)) {
		if ((rc = WEXITSTATUS(rc)) == 0) {
			return (0);		/* Success */
		}
	}

	/* just use pmf_cmd as the err msg buffer */
	(void) snprintf(pmf_cmd, sizeof (pmf_cmd),
	    "Unable to stop PMF tag: %s.", tag);
	(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.", pmf_cmd);
	return (1);
}
