/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SSM_UTIL_H
#define	_SSM_UTIL_H

#pragma ident	"@(#)ssm_util.h	1.33	08/07/25 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <stdarg.h>
#include <strings.h>
#include <sys/ssm.h>
#include <scha.h>
#include <scha_ssm.h>
#include <sys/sc_syslog_msg.h>
#include <rgm/sczones.h>

#ifdef	__cplusplus
extern "C" {
#endif

#if SOL_VERSION >= __s10
#define	SCTP_RGM_SUPPORT
#endif

#define	SSM_PROG_NAME		"ssm_wrapper"

#define	SSM_CALLBACK_DIR	"/usr/cluster/lib/sc"
#define	SSM_CALLBACK_NAME	"ssm_ipmp_callback"
#define	SSM_CALLBACK_METHOD	"IPMP_CALLBACK"
#define	SSM_RETRY_NAME		"ssm_retry"
#define	SSM_RETRY_METHOD	"RETRY"
#define	SSM_NAFO_TAGSIZE	1024
#define	SSM_NAFO_CMDSIZE	2048
#define	SSM_NAFO_DELIMITER	":"
#define	SSM_NAFO_STATUS_TIMEOUT	180	/* in seconds */
#define	SSM_NAFO_SLEEPTIME	5	/* in seconds */
#define	SSM_RETRY_TIME		180	/* in seconds */

/* buffer for internal errors */
#define	INT_ERR_MSG_SIZE 1024

#define	CTAB_CODE_INVALID	-1
#define	CTAB_STRING_INVALID	"INVALID"

#define	MIN_AFFINITY_TIMEOUT	50

typedef struct code_table {
	char	*c_string;
	int	c_code;
} code_table_t;

char *code_to_string(int code, code_table_t *ctab);
int string_to_code(char *string, code_table_t *ctab);

typedef enum method_codes {
	METH_CODE_VALIDATE,
	METH_CODE_BOOT,
	METH_CODE_INIT,
	METH_CODE_START,
	METH_CODE_UPDATE,
	METH_CODE_MONSTART,
	METH_CODE_MONCHK,
	METH_CODE_MONSTOP,
	METH_CODE_FINI,
	/* The following methods do not require any SSM action. */
	METH_CODE_STOP,
	METH_CODE_PRENETSTART,
	METH_CODE_POSTNETSTOP
} method_codes_t;

extern code_table_t method_names[];
void method_names_print(code_table_t *ctab);

extern code_table_t proto_names[];

typedef enum validate_mode {
	VALIDATE_NONE,
	VALIDATE_CREATE,
	VALIDATE_UPDATE
} validate_mode_t;

typedef enum {
	MODE_INVALID = -1,
	MODE_REBALANCE,
	MODE_GETOFF
} ssm_retry_mode_t;

#define	LB_STRING_WEIGHTED	"LB_WEIGHTED"
#define	LB_STRING_STICKY	"LB_STICKY"
#define	LB_STRING_STICKY_WILD	"LB_STICKY_WILD"

#define	PROTO_STRING_TCP	"TCP"
#define	PROTO_STRING_UDP	"UDP"

#ifdef SCTP_RGM_SUPPORT
#define	PROTO_STRING_SCTP	"SCTP"
#endif

/* generic error codes for some of the subroutines */
#define	SSM_OK		0
#define	SSM_ERROR	1

#define	SSM_PROP_SCALABLE	SCHA_SCALABLE
#define	SSM_PROP_NET_RES	SCHA_NETWORK_RESOURCES_USED
#define	SSM_PROP_PORT_LIST	SCHA_PORT_LIST
#define	SSM_PROP_LB_POLICY	SCHA_LOAD_BALANCING_POLICY
#define	SSM_PROP_LB_WEIGHTS	SCHA_LOAD_BALANCING_WEIGHTS

/* This is not a real RGM property. It is only used for printing debug info */
#define	SSM_PROP_GROUP		"Scalable_service_group"

/*
 * From cl_net/ma_service.cc: ASSERT(load_dist.weights[i] < 1000000);
 * It was decided that 10,000 was a reasonable maximum value to enforce.
 *
 * If negative weights are ever allowed, the definition of the nodeweight
 * struct will have to change.
 */
#define	SSM_MIN_WEIGHT		0
#define	SSM_MAX_WEIGHT		10000
#define	SSM_TOTAL_WEIGHT	100
#define	SSM_INVALID_WEIGHT	-1
#define	SSM_DEFAULT_WEIGHT	1

#define	INVALID_NODEID			-1

#define	SHARED_ADDRESS_HOSTNAMELIST	"HostnameList"
#define	SHARED_ADDRESS_AUXNODELIST	"AuxNodeList"
#define	SHARED_ADDRESS_NETIFLIST	"NetIfList"

/* character delimiter used for complex property types */
#define	PORT_PROTO_DELIMITER	'/'
#define	IP_PORT_DELIMITER	'/'
#define	WEIGHT_NODE_DELIMITER	'@'

/*
 * String indicator to indicate that an IP specification in Port_list
 * has been silently wildcarded, i.e. the port serves all IP addresses
 * contained within all the network resources in Network_resources_used.
 */
#define	ALL_NETWORK_RESOURCES_USED_IPS	"<All IPs in Network_resources_used>"

/*
 * Given a (char *)ip_name, returns a printable string for the name,
 * either the name itself if defined, or a predefined indicator string.
 */
#define	IP_NAME_STRING(ip_name)	\
	    ((ip_name != NULL) ? (ip_name) : (ALL_NETWORK_RESOURCES_USED_IPS))

/* The types for port and proto match those for ssm_service_t in ssm.h */
typedef struct ssm_port_and_proto {
	in_port_t	port;
	uint8_t		proto;
	/*
	 * Indicate if the protocol is allowed to work with IPv6.
	 */
	uint8_t		proto6_supported;

	/*
	 * Either NULL, which indicates that a IP address (hostname)
	 * was not specified (meaning "silent wildcard" IP addr), or
	 * a single IP address or hostname.
	 *
	 * Only used for validation purposes.
	 */
	char		*ip_name;
} ssm_port_and_proto_t;

typedef struct ssm_nodeweight {
	uint_t	weight;
	uint_t	node;
} ssm_nodeweight_t;

typedef struct ssm_node_list {
	uint_t	numnodes;
	uint_t	*nodeids;
	char	**nodenames;
} ssm_node_list_t;

/* Structure to hold a network resource */
typedef struct ssm_net_resource {
	char    *snr_name;	/* Name of the resource */
	char    *snr_rgname;	/* Name of the resource group */
	int	snr_currnodeid;	/* Nodeid where the resource is currently */
	ssm_node_list_t	*snr_allnodeids; /* Nodes in Nodelist or AuxNodeList */
	uint_t	snr_numip;	/* Number of hostnames */
	in6_addr_t *snr_iplist;	/* Array of ipaddresses */
} ssm_net_resource_t;

/* A structure to hold all network resources used by the scalable resource */
typedef struct ssm_net_resource_list {
	ssm_net_resource_t	*snrl_snr_list;	/* array of snr */
	uint_t			snrl_num_snr;	/* num of resources */
} ssm_net_resource_list_t;

/* All scalable service relevant properties */
typedef struct ssm_properties
{
	ssm_group_t		*ssm_group; /* the resource name is used */
	ssm_net_resource_list_t	*ssm_net_res_data; /* data for each net res */
	ssm_port_and_proto_t	*ssm_ports; /* structure for ports/protos */
	uint_t			ssm_numports;
	scha_ssm_policy_t	ssm_lb_policy_code;
	ssm_nodeweight_t	*ssm_weights; /* structure for nodes/weights */
	uint_t			ssm_numweights;
	ssm_node_list_t		*ssm_rg_nodelist; /* Nodes where RG can live */
	scha_str_array_t	*ssm_rg_dependencies;
	int			ssm_sticky_timeout;
	boolean_t		ssm_sticky_udp;
	boolean_t		ssm_sticky_weak;
	boolean_t		ssm_sticky_generic;
	boolean_t		ssm_rr_enabled;
	int			ssm_conn_threshold;
} ssm_properties_t;

/* Future: Might support SSM_PROP_ERR_AGAIN to support SEQID error from API */
typedef enum ssm_prop_err {
	SSM_PROP_ERR_NOERR = 0,
	SSM_PROP_ERR_FAILED
} ssm_prop_err_t;

uint_t get_local_nodeid(void);

scha_ssm_exceptions ssm_call(char *rs, char *rg, char *rt,
    method_codes_t method_opcode, validate_mode_t vmode, char *zname,
    int argc, char *argv[]);

ssm_prop_err_t get_ssm_properties(char *rs, char *rg, validate_mode_t vmode,
    char *zname, int argc, char *argv[], ssm_properties_t *ssm_props);
void free_ssm_properties(ssm_properties_t *ssm_props);
void print_ssm_properties(char *rs, ssm_properties_t *ssm_props);

ssm_prop_err_t get_ipaddrs_from_rlist(const scha_str_array_t *r_array,
    struct ssm_net_resource_list **snrl,
    const ssm_port_and_proto_t *ports, uint_t numports, char *zname);

void free_snrl(ssm_net_resource_list_t *snrl);
void print_snrl(const ssm_net_resource_list_t *snrl);

ssm_prop_err_t process_nodelist(scha_str_array_t *nodes_array,
    ssm_node_list_t **nodelist, char *zname);
void free_nodelist(ssm_node_list_t *nodelist);
void nodelistcat(ssm_node_list_t *nodelist1, const ssm_node_list_t *nodelist2);

void free_ports(ssm_port_and_proto_t *ports, uint_t numports);

int ssm_get_nodeid(char *nodename, char *zname);
boolean_t is_nodename_in_nodelist(const char *nodename,
    const ssm_node_list_t *nodelist);
boolean_t is_node_in_nodelist(uint_t nodeid, const ssm_node_list_t *nodelist);

boolean_t is_resource_monitored(const char *cluster, char *rs, char *rg);
boolean_t is_resource_on(const char *cluster, char *rs, char *rg);

scha_ssm_exceptions validate_ssm_properties(char *rs,
    ssm_properties_t *ssm_props);
void veto_ssm_property_updates(char *rs, int argc, char *argv[]);

char *ip_to_str(in6_addr_t ip_uint);
char *get_scha_error_string(scha_err_t e);

int hostip_string_to_binary(char *hostorip, in6_addr_t *ip);
boolean_t is_ip_in_nru(in6_addr_t ip, const ssm_net_resource_list_t *snrl);

/* Nafo utilites */
boolean_t local_node_has_nafo_group(void);
void ssm_register_nafo_callback(char *rs, char *rg, char *rt,
    ssm_properties_t *ssm_props, char *zname);
void ssm_unregister_nafo_callback(char *rs);
int ssm_check_all_nafogroups(void);

/* Syslog utilities */
extern sc_syslog_msg_handle_t log_handle; /* global syslog handle */
void ssm_syslog_init(char *rname, char *rgname, char *rtname,
    char *curr_method);

void usage(void);

int ssm_start_retry(char *rs, char *rg, char *rt, char *zone,
    ssm_retry_mode_t m, ulong_t d);
int ssm_stop_rebalance(char *rgname);
int ssm_stop_getoff(char *rgname, char *rsname);

#ifdef SCTP_RGM_SUPPORT
boolean_t is_sctp_protocol(uint_t num_ports,
    const ssm_port_and_proto_t *port_list);
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* _SSM_UTIL_H */
