/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ssm_call.c - makes the underlying ssm calls for a given resource
 */

#pragma ident	"@(#)ssm_call.c	1.37	09/04/15 SMI"

#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <syslog.h>
#include <scha.h>
#include <scha_lb.h>
#include <sys/clconf_int.h>
#include <locale.h>
#include "ssm_util.h"

static void ssm_verify_no_group(ssm_group_t *ssm_grp);
static scha_ssm_exceptions ssm_init(ssm_properties_t *ssm_props, char *zname);
static scha_ssm_exceptions ssm_fini(ssm_properties_t *ssm_props, char *zname);
static scha_ssm_exceptions ssm_start(char *rs, char *rg,
    ssm_properties_t *ssm_props, char *zname);
static scha_ssm_exceptions ssm_update(ssm_properties_t *ssm_props, char *zname);
static scha_ssm_exceptions ssm_update_nodeids(ssm_properties_t *ssm_props,
    char *zname);
static scha_ssm_exceptions ssm_update_portlist(ssm_properties_t *ssm_props);
static scha_ssm_exceptions ssm_call_set_primary_gifnode(
    ssm_properties_t *ssm_props);
static scha_ssm_exceptions ssm_call_set_lb_weights(
    ssm_properties_t *ssm_props);
static scha_ssm_exceptions ssm_call_config_sticky_srv_grp(
    ssm_properties_t *ssm_props);
static scha_ssm_exceptions ssm_call_config_rr_grp(
    ssm_properties_t *ssm_props);

/*
 * This function is called by ssm_wrapper to make the scalable service
 * related calls for a resource.
 *
 * This function returns a value that will be zero for success or non-zero
 * otherwise.
 *
 * If the scalable related calls below succeed, then we will call the
 * real RT method (if it exists) or simply return the successful status
 * to the RGM.
 *
 * If the scalable related calls below fail, then the real RT method
 * will not be called and the failure status for this method will be
 * returned to the RGM.
 */
scha_ssm_exceptions
ssm_call(char *rs, char *rg, char *rt, method_codes_t method_opcode,
    validate_mode_t vmode, char *zname, int argc, char *argv[])
{
	scha_ssm_exceptions	ssm_rc = SCHA_SSM_SUCCESS;
	ssm_properties_t	ssm_props;
	ssm_prop_err_t		prop_rc;
	boolean_t		got_ssm_props = B_FALSE;

	/* Only get the properties for methods which make ssm calls. */
	switch (method_opcode) {
		case METH_CODE_VALIDATE:
		case METH_CODE_BOOT:
		case METH_CODE_INIT:
		case METH_CODE_START:
		case METH_CODE_MONSTART:
		case METH_CODE_UPDATE:
		case METH_CODE_FINI:
			prop_rc = get_ssm_properties(rs, rg, vmode, zname,
			    argc, argv, &ssm_props);
			if (prop_rc != SSM_PROP_ERR_NOERR) {
				/*
				 * SCMSGS
				 * @explanation
				 * An unexpected error occurred while trying
				 * to collect the properties related to
				 * scalable networking for the named resource.
				 * @user_action
				 * Save a copy of the /var/adm/messages files
				 * on all nodes. Contact your authorized Sun
				 * service provider for assistance in
				 * diagnosing the problem.
				 */
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_ERROR, MESSAGE,
				    "Failed to get the scalable service "
				    "related properties for resource %s.",
				    rs);
				exit(1);
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_DEBUG,
				    "Successfully got the scalable service "
				    "related properties for resource %s.",
				    rs);
				print_ssm_properties(rs, &ssm_props);
			}

			got_ssm_props = B_TRUE;
			break;
		case METH_CODE_MONCHK:
		case METH_CODE_MONSTOP:
		case METH_CODE_STOP:
		case METH_CODE_PRENETSTART:
		case METH_CODE_POSTNETSTOP:
		default:
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Skipping the calls to get the scalable service "
			    "related properties for resource %s.",
			    rs);
			(void) memset((void *)&ssm_props, 0,
			    sizeof (ssm_properties_t));
			break;
	}

	switch (method_opcode) {
		case METH_CODE_VALIDATE:
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Calling scalable service validate.");
			if (vmode == VALIDATE_CREATE) {
				/*
				 * Check if the SSM group exists.
				 * Currently, this only produces a warning.
				 */
				ssm_verify_no_group(ssm_props.ssm_group);
			} else if (vmode == VALIDATE_UPDATE) {
				/*
				 * Will exit if there are illegal
				 * property updates
				 */
				veto_ssm_property_updates(rs, argc, argv);
			} else {
				/* The -c/-u was not specified for VALIDATE */
				/*
				 * SCMSGS
				 * @explanation
				 * The arguments passed to the function
				 * unexpected omitted the given flags.
				 * @user_action
				 * Save a copy of the /var/adm/messages files
				 * on all nodes. Contact your authorized Sun
				 * service provider for assistance in
				 * diagnosing the problem.
				 */
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_ERROR, MESSAGE,
				    "The -c or -u flag must be specified for "
				    "the %s method.",
				    code_to_string(method_opcode,
				    method_names));
				usage();
			}

			/* Do validation and return. There are no ssm calls. */
			ssm_rc = validate_ssm_properties(rs, &ssm_props);
			break;
		case METH_CODE_BOOT:
		case METH_CODE_INIT:
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Calling scalable service init for method %s.",
			    code_to_string(method_opcode, method_names));
			ssm_rc = ssm_init(&ssm_props, zname);
			ssm_register_nafo_callback(rs, rg, rt, &ssm_props,
			    zname);
			break;
		case METH_CODE_START:
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Calling scalable service start.");
			ssm_rc = ssm_start(rs, rg, &ssm_props, zname);
			break;
		case METH_CODE_UPDATE:
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Calling scalable service update.");
			ssm_rc = ssm_update(&ssm_props, zname);
			break;
		case METH_CODE_MONSTART:
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Calling scalable service monitor start.");
			/*
			 * The callbacks should be already registered. We
			 * do this just in case the registration failed
			 * during init earlier. Registration is idempotent
			 * and this call is harmless.
			 */
			ssm_register_nafo_callback(rs, rg, rt, &ssm_props,
			    zname);
			ssm_rc = SCHA_SSM_SUCCESS;
			break;
		case METH_CODE_MONCHK:
			/* SSM properties structure is not filled in. */
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Calling scalable service monitor check.");
			ssm_rc = ssm_check_all_nafogroups();
			break;
		case METH_CODE_MONSTOP:
			/* SSM properties structure is not filled in. */
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Calling scalable service monitor stop.");

			/* stop all retry utilities */
			(void) ssm_stop_rebalance(rg);
			(void) ssm_stop_getoff(rg, rs);

			/* Always succeed - avoid STOP_FAILED state */
			ssm_rc = SCHA_SSM_SUCCESS;
			break;
		case METH_CODE_FINI:
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Calling scalable service fini.");
			ssm_rc = ssm_fini(&ssm_props, zname);
			ssm_unregister_nafo_callback(rs);
			break;
		case METH_CODE_STOP:
		case METH_CODE_PRENETSTART:
		case METH_CODE_POSTNETSTOP:
			/*
			 * SSM properties structure is not filled in.
			 *
			 * The RGM won't call us for these methods,
			 * but just in case, we return success and do
			 * nothing.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "There are no scalable service calls to make "
			    "for method %s.",
			    code_to_string(method_opcode, method_names));
			ssm_rc = SCHA_SSM_SUCCESS;
			break;
		default:
			/* SSM properties structure is not filled in. */
			/*
			 * SCMSGS
			 * @explanation
			 * The method code given is not a method code that was
			 * expected.
			 * @user_action
			 * Save a copy of the /var/adm/messages files on all
			 * nodes. Contact your authorized Sun service provider
			 * for assistance in diagnosing the problem.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Unknown scalable service method code: %d.",
			    method_opcode);
			usage();
			break;
	}

	if (got_ssm_props == B_TRUE) {
		free_ssm_properties(&ssm_props);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "Returning from scalable service call with %d.",
	    ssm_rc);
	return (ssm_rc);
}

/*
 * This is called during validation before resource creation.
 * Warns the user if the group already exisits.
 *
 * The return code SCHA_SSM_MOD_NOTLOADED will be returned if the cl_net
 * module hasn't been loaded.  In this case, the group can't exist on this
 * node, so we treat this case as successfully verifying that the group
 * did not exist.
 */
void
ssm_verify_no_group(ssm_group_t *ssm_grp)
{
	scha_ssm_exceptions	ssm_rc;
	int			group_exists = -1;

	if (ssm_grp == NULL || ssm_grp->resource == NULL) {
		(void) fprintf(stderr, gettext(
		    "INTERNAL ERROR: %s.\n"),
		    "The scalable service group name was invalid while "
		    "trying to verify that the group did not exist");
		exit(1);
	}

	ssm_rc = ssm_is_scalable_service_group(ssm_grp, &group_exists);
	if (ssm_rc != SCHA_SSM_SUCCESS) {
		if (ssm_rc == SCHA_SSM_MOD_NOTLOADED) {
			/* It's OK to fail because cl_net is not loaded. */
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "The network module is not yet loaded, so the "
			    "scalable service group %s cannot exist. Success.",
			    ssm_grp->resource);
		} else {
			(void) fprintf(stderr, gettext(
			    "Warning: Failed to check if scalable service "
			    "group %s exists: %s.\n"),
			    ssm_grp->resource,
			    ssm_err_code_to_mesg(ssm_rc));
		}

		/* just log the error, but don't exit(1) */
		return;
	}

	/* Regardless of the result, we currently just warn the user */
	if (group_exists == 1) {
		/* group exists before resource creation! */
		(void) fprintf(stderr, gettext(
		    "Warning: Scalable service group for resource %s has "
		    "already been created.\n"),
		    ssm_grp->resource);
	} else if (group_exists == 0) {
		/* group doesn't exist */
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "Scalable service group for resource %s does not exist yet "
		    "as expected.",
		    ssm_grp->resource);
	} else {
		/* unexpected return code--should only be 0 or 1. */
		(void) fprintf(stderr, gettext(
		    "Warning: Unexpected result returned while checking "
		    "for the existence of scalable service group %s: %d.\n"),
		    ssm_grp->resource,
		    group_exists);
	}
}

/*
 * Create the scalable service group and add all of the services to it.
 * Also, add the nodeids to the group, and call set_primary_gifnode() for
 * all online network resources.
 */
scha_ssm_exceptions
ssm_init(ssm_properties_t *ssm_props, char *zname)
{
	uint_t				i, j, k;
	ssm_net_resource_list_t		*net_res_data;
	scha_ssm_exceptions		ssm_rc = SCHA_SSM_SUCCESS;
	ssm_service_t			sap;
	in6_addr_t			*ip;

	ssm_rc = ssm_create_scal_srv_grp(ssm_props->ssm_group,
	    ssm_props->ssm_lb_policy_code);
	if (ssm_rc != SCHA_SSM_SUCCESS) {
		if (ssm_rc == SCHA_SSM_GROUP_EXISTS) {
			/*
			 * Only one of the nodes will succed in adding
			 * the SSM group. Success.
			 *
			 * Previously, the code would exit at this point
			 * and defer service creation to the node that
			 * added the group. Instead, we will try to add
			 * all of the services just to verify that they
			 * exist after the init method is run on this node.
			 *
			 * XXX - We have no way to know if the SSM group
			 * existed from a previous instantiation of the
			 * resource.  We currently log a message in validate if
			 * the SSM group exists before creation time.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Scalable service group %s has already been "
			    "created. Success.",
			    ssm_props->ssm_group->resource);
			ssm_rc = SCHA_SSM_SUCCESS;
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * A call to the underlying scalable networking code
			 * failed.
			 * @user_action
			 * Save a copy of the /var/adm/messages files on all
			 * nodes. Contact your authorized Sun service provider
			 * for assistance in diagnosing the problem.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Failed to create scalable service group %s: %s.",
			    ssm_props->ssm_group->resource,
			    ssm_err_code_to_mesg(ssm_rc));
			exit(1);
		}
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "Successfully added scalable service group %s.",
		    ssm_props->ssm_group->resource);
	}

	/*
	 * The group has been successfully added.
	 * For all network resources used by this resource,
	 * add the sap (ip + port + proto) to the group.
	 * Since there could be > 1 port associated with the
	 * resource, loop over ports as well.
	 */
	net_res_data = ssm_props->ssm_net_res_data;
	for (i = 0; i < net_res_data->snrl_num_snr; i++) {
		for (j = 0; j < net_res_data->snrl_snr_list[i].snr_numip; j++) {
			for (k = 0; k < ssm_props->ssm_numports; k++) {
				ip = &net_res_data->snrl_snr_list[i].
				    snr_iplist[j];

				if (!IN6_IS_ADDR_V4MAPPED(ip) &&
				    !ssm_props->ssm_ports[k].proto6_supported)
					continue;

				sap.ipaddr = *ip;
				sap.port = ssm_props->ssm_ports[k].port;
				sap.protocol = ssm_props->ssm_ports[k].proto;

				/*
				 * The ssm_add_scal_service() call
				 * is now idempotent.
				 * It will return SUCCESS if the
				 * code successfully adds the sap
				 * or if the sap has already been
				 * added.
				 */
				ssm_rc = ssm_add_scal_service(
				    ssm_props->ssm_group,
				    &sap);
				if (ssm_rc != SCHA_SSM_SUCCESS) {
					/*
					 * SCMSGS
					 * @explanation
					 * A call to the underlying scalable
					 * networking code failed. This call
					 * may fail because the IP, Port, and
					 * Protocol combination listed in the
					 * message conflicts with the
					 * configuration of an existing
					 * scalable resource. A conflict can
					 * occur if the same combination
					 * exists in a scalable resource that
					 * is already configured on the
					 * cluster. A combination may also
					 * conflict if there is a resource
					 * that uses Load_balancing_policy
					 * LB_STICKY_WILD with the same IP
					 * address as a different resource
					 * that also uses LB_STICKY_WILD.
					 * @user_action
					 * Try using a different IP, Port, and
					 * Protocol combination. Otherwise,
					 * save a copy of the
					 * /var/adm/messages files on all
					 * nodes. Contact your authorized Sun
					 * service provider for assistance in
					 * diagnosing the problem.
					 */
					(void) sc_syslog_msg_log(
					    log_handle,
					    SC_SYSLOG_ERROR, MESSAGE,
					    "Failed to create scalable "
					    "service in group %s "
					    "for IP %s Port %d%c%s: "
					    "%s.",
					    ssm_props->ssm_group->
						resource,
					    ip_to_str(sap.ipaddr),
					    sap.port,
					    PORT_PROTO_DELIMITER,
					    code_to_string(
						sap.protocol,
						proto_names),
					    ssm_err_code_to_mesg(
						ssm_rc));
					exit(1);
				} else {
					/*
					 * SCMSGS
					 * @explanation
					 * Need explanation of this message!
					 * @user_action
					 * Need a user action for this
					 * message.
					 */
					syslog(LOG_DEBUG,
					    "Scalable service in group %s "
					    "has been successfully created "
					    "for IP %s Port %d%c%s.",
					    ssm_props->ssm_group->resource,
					    ip_to_str(sap.ipaddr),
					    sap.port,
					    PORT_PROTO_DELIMITER,
					    code_to_string(sap.protocol,
						proto_names));
				}
			} /* All ports */
		} /* All addresses for one net resource */
	} /* All net resources used by this resource */

	/* Add all nodes in the RG nodelist to this group */
	ssm_rc = ssm_update_nodeids(ssm_props, zname);

	/* call set_primary_gifnode() for all online global interfaces */
	ssm_rc = ssm_call_set_primary_gifnode(ssm_props);

	return (ssm_rc);
}

scha_ssm_exceptions
ssm_start(char *rs, char *rg, ssm_properties_t *ssm_props, char *zname)
{
	scha_ssm_exceptions	ssm_rc;
	int			rc;
	boolean_t		result = B_FALSE;

	/*
	 * If monitoring is enabled, then we verify that all IPMP
	 * groups are OK before allowing the resource to start up
	 * on this node.
	 */
	if (is_resource_monitored(zname, rs, rg) == B_TRUE) {
		rc = ssm_check_all_nafogroups();
		if (rc != 0) {
			/* message explaining error already logged */
			return (1);
		}
	}

	/* call set_primary_gifnode() for all online global interfaces */
	ssm_rc = ssm_call_set_primary_gifnode(ssm_props);

	/* make sure the config list and the RG nodelist are still in sync */
	ssm_rc = ssm_update_nodeids(ssm_props, zname);

	/* Configure load balancing weights for this resource */

	/*
	 * Get the current distribution and check whether the
	 * weights are set already. If yes, don't call set_lb_weights,
	 * this way the DLMI weights will be preserved.
	 */
	rc = ssm_are_weights_set(ssm_props->ssm_group, &result);
	if (rc !=  SCHA_SSM_SUCCESS) {
		/*
		 * SCMSGS
		 * @explanation
		 * The query for checking, whether the weights
		 * are set or not, has failed.
		 * @user_action
		 * Save a copy of the /var/adm/messages files
		 * on all nodes. Contact your authorized Sun
		 * service provider for assistance in
		 * diagnosing the problem.
		 */
		syslog(LOG_ERR,
		    "Failed to check whether the weights are set with "
		    "error: %s", ssm_err_code_to_mesg(rc));
		return (rc);
	}
	if (result != B_TRUE) {
		/* the weight_array is not being initialized  */
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG, "The weights are not set, so setting the "
		    "weights.");
		ssm_rc = ssm_call_set_lb_weights(ssm_props);
	}

	/* If sticky service, configure it here. */
	ssm_rc = ssm_call_config_sticky_srv_grp(ssm_props);

	/* If round robin, configure it here */
	ssm_rc = ssm_call_config_rr_grp(ssm_props);

	/* XXX - should always succeed? should check errors above? */
	ssm_rc = SCHA_SSM_SUCCESS;
	return (ssm_rc);
}

/*
 * Call set_primary_gifnode() for all IPs that are currently online.
 * This must be called for each IP after the scalable service group and
 * scalable services for the resource have been created.  It is not
 * sufficient to rely solely on the hascip resource to make the
 * set_primary_gifnode() call because the scalable service group for
 * the resources that use the IP may not have been created when the IP address
 * is switched online.  Therefore, we currently call set_primary_gifnode()
 * during INIT and START.
 */
scha_ssm_exceptions
ssm_call_set_primary_gifnode(ssm_properties_t *ssm_props)
{
	uint_t				i, j;
	ssm_net_resource_list_t		*net_res_data = NULL;
	scha_ssm_exceptions		ssm_rc = SCHA_SSM_SUCCESS;
	ssm_service_t			sap;

	/* call set_primary_gifnode() for all online global interfaces */
	net_res_data = ssm_props->ssm_net_res_data;
	for (i = 0; i < net_res_data->snrl_num_snr; i++) {
		/* Check if the RG for the global interface is online yet */
		if (net_res_data->snrl_snr_list[i].snr_currnodeid ==
		    INVALID_NODEID) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Resource %s is not online yet, so "
			    "not setting the global interface node for its "
			    "IP addresses.",
			    net_res_data->snrl_snr_list[i].snr_name);
			continue;
		}

		/* The RG is online, so for all IPs, call set_primary_gifnode */
		for (j = 0; j < net_res_data->snrl_snr_list[i].snr_numip; j++) {
			sap.port = 0;		/* the port is ignored */
			sap.protocol = 0;	/* the protocol is ignored */
			sap.ipaddr = net_res_data->
			    snrl_snr_list[i].snr_iplist[j];

			ssm_rc = ssm_set_primary_gifnode(
			    &sap,
			    (nodeid_t)net_res_data->
			    snrl_snr_list[i].snr_currnodeid);
			if (ssm_rc != SCHA_SSM_SUCCESS) {
				/*
				 * SCMSGS
				 * @explanation
				 * A call to the underlying scalable
				 * networking code failed.
				 * @user_action
				 * Save a copy of the /var/adm/messages files
				 * on all nodes. Contact your authorized Sun
				 * service provider for assistance in
				 * diagnosing the problem.
				 */
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_ERROR, MESSAGE,
				    "Failed to set the global "
				    "interface node to %d for IP %s: %s.",
				    net_res_data->snrl_snr_list[i].
					snr_currnodeid,
				    ip_to_str(sap.ipaddr),
				    ssm_err_code_to_mesg(ssm_rc));

				/* XXX ignore failure? */
				ssm_rc = SCHA_SSM_SUCCESS;
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_DEBUG,
				    "Set the global interface node to %d "
				    "for IP %s.",
				    net_res_data->snrl_snr_list[i].
					snr_currnodeid,
				    ip_to_str(sap.ipaddr));
			}
		} /* All addresses for one net resource */
	} /* All net resources used by this resource */

	return (ssm_rc);
}

/*
 * Update the load balancer with the current weights for the resource.
 * This is done during start and update.
 *
 * This routine calls into libscha which contacts the rgm, which then
 * acts directly on the load balancing objects.  There is no direct
 * networking interface for setting the load balancing weights
 * like those in ssm.h.
 *
 * The load balancer code is written so that an update with the same
 * weights does not change the distribution table.  So, making repeated
 * calls with the same weights will not affect the distribution (or reset
 * "stickiness").
 *
 * The convention is to call set_distribution with NODEID_MAX + 1 weights.
 * The network code will ignore any entries for nodes that aren't in the
 * configuration list (RG_nodelist) of the SSM group.
 */
scha_ssm_exceptions
ssm_call_set_lb_weights(ssm_properties_t *ssm_props)
{
	uint_t			max_node = NODEID_MAX;
	uint_t			max_node_in_nodelist = 0;
	uint_t			wi_array_elements;
	uint_t			i;
	struct weight_info	winfo;
	int			rc = 0;

	/*
	 * Allocate space for an array of "max_node" weights.
	 * Since the element 0 is not used, allocate space for max_node + 1
	 */
	wi_array_elements = max_node + 1;
	winfo.num_nodes = (int)wi_array_elements;
	winfo.weights = (int *)calloc(wi_array_elements, sizeof (int));
	if (winfo.weights == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Out of memory.");
		exit(1);
	}

	/*
	 * Initialize the weight for each node in the nodelist to
	 * SSM_DEFAULT_WEIGHT.  Weights for nodes that aren't in the nodelist
	 * have already been initialized to 0 by the calloc() call above.
	 * We will overwrite the default weight if the user has specified
	 * an explict weight later on.
	 */
	for (i = 0; i < ssm_props->ssm_rg_nodelist->numnodes; i++) {
		/* sanity check that our weight info won't overrun the array */
		if (ssm_props->ssm_rg_nodelist->nodeids[i] > max_node) {
			/* Internal error */
			/*
			 * SCMSGS
			 * @explanation
			 * In one of the scalable networking properties, a
			 * node id was encountered that was higher than
			 * expected.
			 * @user_action
			 * Verify that the nodes listed in the scalable
			 * networking properties are still valid cluster
			 * members.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Node id %d is higher than the maximum "
			    "node id of %d in the cluster.",
			    ssm_props->ssm_rg_nodelist->nodeids[i],
			    max_node);
			exit(1);
		}

		/* Keep track of the highest nodeid in the nodeslist */
		if (ssm_props->ssm_rg_nodelist->nodeids[i] >
		    max_node_in_nodelist) {
			max_node_in_nodelist =
			    ssm_props->ssm_rg_nodelist->nodeids[i];
		}

		/* Set a default weight for any node in the nodelist */
		winfo.weights[ssm_props->ssm_rg_nodelist->nodeids[i]] =
		    SSM_DEFAULT_WEIGHT;
	}

	/* Overwrite the default with any weights the user specified. */
	for (i = 0; i < ssm_props->ssm_numweights; i++) {
		/* sanity check that our weight info won't overrun the array */
		if (ssm_props->ssm_weights[i].node > max_node) {
			/* Internal error */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Node id %d is higher than the maximum "
			    "node id of %d in the cluster.",
			    ssm_props->ssm_weights[i].node,
			    max_node);
			exit(1);
		}

		/* Only fill in the weight if the node is in the RG Nodelist */
		if (is_node_in_nodelist(ssm_props->ssm_weights[i].node,
		    ssm_props->ssm_rg_nodelist) == B_TRUE) {
			winfo.weights[ssm_props->ssm_weights[i].node] =
			    (int)ssm_props->ssm_weights[i].weight;
		}
	}

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "Calling the load balancer with %d weights.  Only the weights "
	    "up to node %d, the highest node in the %s, will be displayed:",
	    winfo.num_nodes,
	    max_node_in_nodelist,
	    SCHA_NODELIST);

	/*
	 * It is true that (max_node_in_nodelist < winfo.num_nodes) because
	 * the nodeid we set max_node_in_nodelist to is less than or equal to
	 * max_node, and max_node < winfo.num_nodes.
	 */
	for (i = 0; i <= max_node_in_nodelist; i++) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "weight_info[%d] = %d",
		    i,
		    winfo.weights[i]);
	}

	/* Update the load balancer with the new weights */
	rc = scha_ssm_lb_set_distribution(ssm_props->ssm_group->resource,
	    &winfo);

	/* XXX - the load balancer commonly returns 82 (EREMCHG) */
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "The load balancer returned status %d (non-zero is ok).",
	    rc);

	free(winfo.weights);

	/* XXX how to handle errors? */
	return (SCHA_SSM_SUCCESS);
}

scha_ssm_exceptions
ssm_call_config_sticky_srv_grp(ssm_properties_t *ssm_props)
{
	scha_ssm_exceptions ssm_rc = SCHA_SSM_SUCCESS;

	if (ssm_props->ssm_lb_policy_code != scha_ssm_lb_st &&
	    ssm_props->ssm_lb_policy_code != scha_ssm_lb_sw)
		return (SCHA_SSM_SUCCESS);

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG, "ssm_call_config_sticky_srv_grp on %s - %d, %s, %s",
	    ssm_props->ssm_group,
	    ssm_props->ssm_sticky_timeout,
	    (ssm_props->ssm_sticky_udp? "UDP": "No UDP"),
	    (ssm_props->ssm_sticky_weak? "Weak": "Strong"));

	ssm_rc = ssm_config_sticky_srv_grp(ssm_props->ssm_group,
	    ssm_props->ssm_sticky_timeout,
	    ssm_props->ssm_sticky_udp,
	    ssm_props->ssm_sticky_weak,
	    ssm_props->ssm_sticky_generic);

	if (ssm_rc != SCHA_SSM_SUCCESS) {
		/*
		 * SCMSGS
		 * @explanation
		 * A call to the underlying scalable networking code failed.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Warning: Failed to configure client affinity "
		    "for group %s: %s",
		    ssm_props->ssm_group,
		    ssm_err_code_to_mesg(ssm_rc));
		exit(1);
	}

	return (SCHA_SSM_SUCCESS);
}

scha_ssm_exceptions
ssm_call_config_rr_grp(ssm_properties_t *ssm_props)
{
	scha_ssm_exceptions ssm_rc = SCHA_SSM_SUCCESS;

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG, "ssm_call_config_rr_grp on %s - %s, %d",
	    ssm_props->ssm_group,
	    ssm_props->ssm_rr_enabled ?
	    "Round robin enabled" : "Round Robin disabled",
	    (ssm_props->ssm_conn_threshold));

	ssm_rc = ssm_config_rr_srv_grp(ssm_props->ssm_group,
	    ssm_props->ssm_rr_enabled, ssm_props->ssm_conn_threshold);
	if (ssm_rc != SCHA_SSM_SUCCESS) {
		/*
		 * SCMSGS
		 * @explanation
		 * A call to the underlying scalable networking code failed.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Warning: Failed to configure Round robin load balancing"
		    " property for group %s: %s", ssm_props->ssm_group,
		    ssm_err_code_to_mesg(ssm_rc));

		exit(1);
	}

	return (SCHA_SSM_SUCCESS);
}

scha_ssm_exceptions
ssm_update(ssm_properties_t *ssm_props, char *zname)
{
	scha_ssm_exceptions	ssm_rc;

	/* perhaps the RG nodelist has changed */
	ssm_rc = ssm_update_nodeids(ssm_props, zname);

	/*
	 * Current implementation of RGM does not give the
	 * property name or the value that's being updated
	 * when ssm_update is called. Due to this behavior,
	 * we go ahead and call the ssm_call_set_lb_weights()
	 * irrespective of the need. This is a problem
	 * for DLMI weights as they are getting overwritten
	 * everytime the ssm_update is called for any RG
	 * or resource property update. This behavior need
	 * to be changed and the ssm_call_set_lb_weights()
	 * need to be called only on need basis. This
	 * functionality need to be implemented when the
	 * fix for RGM RFE 4261740 is available.
	 */

	/* Configure load balancing weights for this resource */
	ssm_rc = ssm_call_set_lb_weights(ssm_props);

	/* Handle any changes to the port list */
	ssm_rc = ssm_update_portlist(ssm_props);

	/* If sticky service, configure it here. */
	ssm_rc = ssm_call_config_sticky_srv_grp(ssm_props);

	/* should always succeed? */
	return (ssm_rc);
}

/*
 * This function updates the SSM Config List with the current set of
 * nodes in the nodelist.  This is done by explicitly adding all the nodes that
 * are in the current nodelist (even if they are already in the Config List)
 * and by explicitly removing all nodes that are not in the nodelist.  There
 * is currently no clean way to get a "diff" list of the "old" and "new" values
 * of the nodelist, so we rely on this method to update the SSM Config List.
 */
scha_ssm_exceptions
ssm_update_nodeids(ssm_properties_t *ssm_props, char *zname)
{
	scha_ssm_exceptions	ssm_rc;
	uint_t			i;
	scha_err_t		e;
	scha_uint_array_t	*all_nodeids = NULL;
	scha_cluster_t		clust_handle;

	/* Get the valid nodeids in the cluster */
	e = scha_cluster_open_zone(zname, &clust_handle);
	if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Access to the object named failed. The reason for the
		 * failure is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the cluster handle "
		    "while querying for property %s: %s.",
		    SCHA_ALL_NODEIDS,
		    get_scha_error_string(e));
		exit(1);
	}

	e = scha_cluster_get_zone(zname, clust_handle, SCHA_ALL_NODEIDS,
		&all_nodeids);
	if (e == SCHA_ERR_SEQID) {
		/* Just ignore the SCHA_ERR_SEQID error. */
		/*
		 * SCMSGS
		 * @explanation
		 * An update to the cluster configuration tables occured while
		 * trying to retrieve certain cluster related information.
		 * However, the update does not affect the property that is
		 * being retrieved.
		 * @user_action
		 * Ignore the message
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "Ignoring the SCHA_ERR_SEQID while retrieving %s",
		    SCHA_ALL_NODEIDS);
	} else if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The query for a property failed. The reason for the failure
		 * is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the cluster property %s: %s.",
		    SCHA_ALL_NODEIDS,
		    get_scha_error_string(e));
		exit(1);
	}

	if (all_nodeids == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.",
		    "The array of nodeids returned from the API was NULL");
		exit(1);
	}

	/* For all nodes in the cluster, check if the node is in the Nodelist */
	for (i = 0; i < all_nodeids->array_cnt; i++) {
		if (is_node_in_nodelist(all_nodeids->uint_array[i],
		    ssm_props->ssm_rg_nodelist) == B_TRUE) {
			/* add the node if the nodeid is in the nodelist */
			ssm_rc = ssm_add_nodeid(ssm_props->ssm_group,
			    all_nodeids->uint_array[i]);
			if (ssm_rc != SCHA_SSM_SUCCESS) {
				if (ssm_rc == SCHA_SSM_NODE_EXISTS) {
					/* Success */
					/*
					 * SCMSGS
					 * @explanation
					 * Need explanation of this message!
					 * @user_action
					 * Need a user action for this
					 * message.
					 */
					syslog(LOG_DEBUG,
					    "Node %d is already in "
					    "scalable service group %s. "
					    "Success.",
					    all_nodeids->uint_array[i],
					    ssm_props->ssm_group->resource);
				} else {
					/*
					 * SCMSGS
					 * @explanation
					 * A call to the underlying scalable
					 * networking code failed.
					 * @user_action
					 * Save a copy of the
					 * /var/adm/messages files on all
					 * nodes. Contact your authorized Sun
					 * service provider for assistance in
					 * diagnosing the problem.
					 */
					(void) sc_syslog_msg_log(log_handle,
					    SC_SYSLOG_ERROR, MESSAGE,
					    "Failed to add node %d to scalable "
					    "service group %s: %s.",
					    all_nodeids->uint_array[i],
					    ssm_props->ssm_group->resource,
					    ssm_err_code_to_mesg(ssm_rc));
					exit(1);
				}
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_DEBUG,
				    "Node %d was successfully added to "
				    "scalable service group %s.",
				    all_nodeids->uint_array[i],
				    ssm_props->ssm_group->resource);
			}
		} else {
			/* remove the node */
			ssm_rc = ssm_remove_nodeid(ssm_props->ssm_group,
			    all_nodeids->uint_array[i]);
			if (ssm_rc != SCHA_SSM_SUCCESS) {
				if (ssm_rc == SCHA_SSM_NO_NODEID) {
					/* Success */
					/*
					 * SCMSGS
					 * @explanation
					 * Need explanation of this message!
					 * @user_action
					 * Need a user action for this
					 * message.
					 */
					syslog(LOG_DEBUG,
					    "Node %d is already not in "
					    "scalable service group %s. "
					    "Success.",
					    all_nodeids->uint_array[i],
					    ssm_props->ssm_group->resource);
				} else {
					/*
					 * SCMSGS
					 * @explanation
					 * A call to the underlying scalable
					 * networking code failed.
					 * @user_action
					 * Save a copy of the
					 * /var/adm/messages files on all
					 * nodes. Contact your authorized Sun
					 * service provider for assistance in
					 * diagnosing the problem.
					 */
					(void) sc_syslog_msg_log(log_handle,
					    SC_SYSLOG_ERROR, MESSAGE,
					    "Failed to remove node %d from "
					    "scalable service group %s: %s.",
					    all_nodeids->uint_array[i],
					    ssm_props->ssm_group->resource,
					    ssm_err_code_to_mesg(ssm_rc));
					exit(1);
				}
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				syslog(LOG_DEBUG,
				    "Node %d was successfully removed from "
				    "scalable service group %s.",
				    all_nodeids->uint_array[i],
				    ssm_props->ssm_group->resource);
			}
		}
	}

	(void) scha_cluster_close(clust_handle);
	return (SCHA_SSM_SUCCESS);
}

/* delete all services and the containing group */
scha_ssm_exceptions
ssm_fini(ssm_properties_t *ssm_props, char *zname)
{
	uint_t				i, j, k;
	ssm_net_resource_list_t		*net_res_data = NULL;
	scha_ssm_exceptions		ssm_rc = SCHA_SSM_SUCCESS;
	ssm_service_t			sap;
	in6_addr_t			*ip;
	scha_err_t			e;
	scha_cluster_t			clust_handle;
	uint_t				local_nodeid;
	boolean_t			islocal_inlist = B_FALSE;

	/* Get the local nodeid in the cluster */
	e = scha_cluster_open_zone(zname, &clust_handle);
	if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the cluster handle "
		    "while querying for property %s: %s.",
		    SCHA_NODEID_LOCAL,
		    get_scha_error_string(e));
		exit(1);
	}

	e = scha_cluster_get_zone(zname, clust_handle, SCHA_NODEID_LOCAL,
		&local_nodeid);
	if (e != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve the cluster property %s: %s.",
		    SCHA_NODEID_LOCAL,
		    get_scha_error_string(e));
		exit(1);
	}

	/*
	 * Check if the local node is in the RG Nodelist
	 * If it is present do a global cleanup, if not
	 * just return
	 */
	for (i = 0; i < ssm_props->ssm_rg_nodelist->numnodes; i++) {
		if (local_nodeid == ssm_props->ssm_rg_nodelist->nodeids[i]) {
			islocal_inlist = B_TRUE;
			break;
		}
	}

	if (islocal_inlist == B_FALSE)
		return (ssm_rc);

	/*
	 * For all network resources used by this resource,
	 * delete the sap (ip + port + proto) from the group.
	 * Since there could be > 1 port associated with the
	 * resource, loop over ports as well.
	 */
	net_res_data = ssm_props->ssm_net_res_data;
	for (i = 0; i < net_res_data->snrl_num_snr; i++) {
		for (j = 0; j < net_res_data->snrl_snr_list[i].snr_numip; j++) {
			for (k = 0; k < ssm_props->ssm_numports; k++) {
				ip = &net_res_data->snrl_snr_list[i].
				    snr_iplist[j];

				if (!IN6_IS_ADDR_V4MAPPED(ip) &&
				    !ssm_props->ssm_ports[k].proto6_supported)
					continue;

				sap.ipaddr = *ip;
				sap.port = ssm_props->ssm_ports[k].port;
				sap.protocol = ssm_props->ssm_ports[k].proto;

				ssm_rc = ssm_rem_scal_service(
				    ssm_props->ssm_group,
				    &sap);
				if (ssm_rc != SCHA_SSM_SUCCESS) {
					if (ssm_rc ==
					    SCHA_SSM_INVALID_SERVICE_GROUP ||
					    ssm_rc ==
					    SCHA_SSM_NOSUCH_SERVICE) {
						/* Success */
						/*
						 * SCMSGS
						 * @explanation
						 * Need explanation of this
						 * message!
						 * @user_action
						 * Need a user action for this
						 * message.
						 */
						syslog(LOG_DEBUG,
						    "Scalable service in group "
						    "%s has already been "
						    "deleted for IP %s Port "
						    "%d%c%s. Success.",
						    ssm_props->ssm_group->
							resource,
						    ip_to_str(sap.ipaddr),
						    sap.port,
						    PORT_PROTO_DELIMITER,
						    code_to_string(sap.protocol,
							proto_names));
						/*
						 * Previously, the code would
						 * exit at this point and defer
						 * service deletion to the node
						 * that removed the first
						 * service. Instead, we will
						 * try to remove all of the
						 * services just to verify that
						 * they do not exist after the
						 * fini method is run on this
						 * node.
						 */
						ssm_rc = SCHA_SSM_SUCCESS;
					} else if (ssm_rc ==
					    SCHA_SSM_MOD_NOTLOADED) {
						/* Nothing to remove */
						/*
						 * SCMSGS
						 * @explanation
						 * Need explanation of this
						 * message!
						 * @user_action
						 * Need a user action for this
						 * message.
						 */
						syslog(LOG_DEBUG,
						    "Scalable service in group "
						    "%s is not configured "
						    "for IP %s Port "
						    "%d%c%s. Success: %s",
						    ssm_props->ssm_group->
							resource,
						    ip_to_str(sap.ipaddr),
						    sap.port,
						    PORT_PROTO_DELIMITER,
						    code_to_string(sap.protocol,
							proto_names),
						    ssm_err_code_to_mesg(
							ssm_rc));
						ssm_rc = SCHA_SSM_SUCCESS;
						/* No point in continuing */
						return (ssm_rc);
					} else {
						/*
						 * SCMSGS
						 * @explanation
						 * A call to the underlying
						 * scalable networking code
						 * failed.
						 * @user_action
						 * Save a copy of the
						 * /var/adm/messages files on
						 * all nodes. Contact your
						 * authorized Sun service
						 * provider for assistance in
						 * diagnosing the problem.
						 */
						(void) sc_syslog_msg_log(
						    log_handle,
						    SC_SYSLOG_ERROR, MESSAGE,
						    "Failed to delete scalable "
						    "service in group %s "
						    "for IP %s Port %d%c%s: "
						    "%s.",
						    ssm_props->ssm_group->
							resource,
						    ip_to_str(sap.ipaddr),
						    sap.port,
						    PORT_PROTO_DELIMITER,
						    code_to_string(sap.protocol,
							proto_names),
						    ssm_err_code_to_mesg(
							ssm_rc));

						exit(1);
					}
				} else {
					/*
					 * SCMSGS
					 * @explanation
					 * Need explanation of this message!
					 * @user_action
					 * Need a user action for this
					 * message.
					 */
					syslog(LOG_DEBUG,
					    "Scalable service in group %s "
					    "has been successfully deleted "
					    "for IP %s Port %d%c%s.",
					    ssm_props->ssm_group->resource,
					    ip_to_str(sap.ipaddr),
					    sap.port,
					    PORT_PROTO_DELIMITER,
					    code_to_string(sap.protocol,
						proto_names));
				}
			} /* All ports */
		} /* All addresses for one net resource */
	} /* All net resources used by this resource */

	ssm_rc = ssm_del_scal_srv_grp(ssm_props->ssm_group);
	if (ssm_rc != SCHA_SSM_SUCCESS) {
		if (ssm_rc == SCHA_SSM_INVALID_SERVICE_GROUP) {
			/* Success */
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			syslog(LOG_DEBUG,
			    "Scalable service group %s has already been "
			    "deleted. Success.",
			    ssm_props->ssm_group->resource);
			ssm_rc = SCHA_SSM_SUCCESS;
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * A call to the underlying scalable networking code
			 * failed.
			 * @user_action
			 * Save a copy of the /var/adm/messages files on all
			 * nodes. Contact your authorized Sun service provider
			 * for assistance in diagnosing the problem.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Failed to delete scalable service group %s: %s.",
			    ssm_props->ssm_group->resource,
			    ssm_err_code_to_mesg(ssm_rc));
			exit(1);
		}
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "Successfully deleted scalable service group %s.",
		    ssm_props->ssm_group->resource);
	}

	return (ssm_rc);
}

/*
 * This function updates the Port_list
 */
scha_ssm_exceptions
ssm_update_portlist(ssm_properties_t *ssm_props)
{
	uint_t i, j, k, deleted = 0, total = 0;
	int h, n = 0;
	ssm_net_resource_list_t *net_res_data = ssm_props->ssm_net_res_data;
	scha_ssm_exceptions ssm_rc = SCHA_SSM_SUCCESS;
	ssm_service_t sap, *current_list, cl;
	in6_addr_t *ip;

	/* get a list of all SAPs currently in this service group */
	ssm_rc = ssm_get_scal_services(ssm_props->ssm_group, &current_list,
	    &n);
	if (ssm_rc != SCHA_SSM_SUCCESS) {
		/*
		 * SCMSGS
		 * @explanation
		 * An unexpected error occurred while trying
		 * to get the list of scalable services
		 * in this service group.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to retrieve list of scalable services in group %s"
		    ":%s", ssm_props->ssm_group, ssm_err_code_to_mesg(ssm_rc));
		free(current_list);
		exit(1);
	}
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG, "%d SAPs in the group", n);

	/* Delete all SAPs that are to be removed from service */
	for (h = 0; h < n; h++) {
		cl = current_list[h];

		/* is this sap in the new list? */
		for (i = 0; i < net_res_data->snrl_num_snr; i++)
		for (j = 0; j < net_res_data->snrl_snr_list[i].snr_numip; j++)
		for (k = 0; k < ssm_props->ssm_numports; k++) {
			ip = &net_res_data->snrl_snr_list[i].snr_iplist[j];

			if (!IN6_IS_ADDR_V4MAPPED(ip) &&
			    !ssm_props->ssm_ports[k].proto6_supported)
				continue;

			sap.ipaddr = *ip;
			sap.port = ssm_props->ssm_ports[k].port;
			sap.protocol = ssm_props->ssm_ports[k].proto;

			/* do the SAPs match? */
			if (cl.port == sap.port &&
			    cl.protocol == sap.protocol &&
			    IN6_ARE_ADDR_EQUAL(&cl.ipaddr, ip)) {
				/* break out of all 3 loops */
				goto sap_found;
			}
		}

		/* sap was not found in the new list, delete it */
		ssm_rc = ssm_rem_scal_service(ssm_props->ssm_group, &cl);
		deleted++; /* purely for debugging purposes */
		if (ssm_rc != SCHA_SSM_SUCCESS &&
		    ssm_rc != SCHA_SSM_NOSUCH_SERVICE) {
			(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_ERROR,
			    MESSAGE, "Failed to delete scalable "
			    "service in group %s for IP %s Port %d%c%s: %s.",
			    ssm_props->ssm_group->resource,
			    ip_to_str(cl.ipaddr), cl.port,
			    PORT_PROTO_DELIMITER,
			    code_to_string(cl.protocol, proto_names),
			    ssm_err_code_to_mesg(ssm_rc));

			/*
			 * Should we be more forgiving? After all, all the
			 * nodes are going to process the new port_list. If
			 * even one of them is successful, the PDT should
			 * be allright.
			 */
			free(current_list);
			exit(1);
		}
sap_found:
		/* nop */;
	}

	/* done with current_list */
	if (n)
		free(current_list);

	/*
	 * Add all SAPs that are to be added
	 *
	 * Iterate over the list of SAPs that should be in the service group.
	 * We (re)add each SAP to the service group. This takes care of adding
	 * the new ones and is harmless for the SAPs already in the group.
	 */
	for (i = 0; i < net_res_data->snrl_num_snr; i++)
	for (j = 0; j < net_res_data->snrl_snr_list[i].snr_numip; j++)
	for (k = 0; k < ssm_props->ssm_numports; k++) {
		ip = &net_res_data->snrl_snr_list[i].snr_iplist[j];

		if (!IN6_IS_ADDR_V4MAPPED(ip) &&
		    !ssm_props->ssm_ports[k].proto6_supported)
			continue;

		sap.ipaddr = *ip;
		sap.port = ssm_props->ssm_ports[k].port;
		sap.protocol = ssm_props->ssm_ports[k].proto;
		ssm_rc = ssm_add_scal_service(ssm_props->ssm_group, &sap);
		total++; /* for debugging purposes */
		if (ssm_rc != SCHA_SSM_SUCCESS) {
			(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_ERROR,
			    MESSAGE, "Failed to create scalable "
			    "service in group %s for IP %s Port %d%c%s: %s.",
			    ssm_props->ssm_group->resource,
			    ip_to_str(sap.ipaddr), sap.port,
			    PORT_PROTO_DELIMITER,
			    code_to_string(sap.protocol, proto_names),
			    ssm_err_code_to_mesg(ssm_rc));

			/*
			 * Again, should we be more forgiving?
			 * See comment after ssm_rem_scal_service() above
			 */
			exit(1);
		}
	}

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG, "Service group %s now has %d SAPs, after "
	    "%d deletions", ssm_props->ssm_group->resource, total, deleted);

	return (0);
}
