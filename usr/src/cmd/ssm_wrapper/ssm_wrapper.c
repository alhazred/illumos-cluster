/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ssm_wrapper.c -	SSM wrapper program to be invoked by the RGM before
 *			invoking the real RT method for scalable resources.
 */

#pragma ident	"@(#)ssm_wrapper.c	1.25	08/06/19 SMI"

#include <fcntl.h>

#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <locale.h>

#include <scha.h>
#include "ssm_util.h"

#include <sys/sol_version.h>
#if SOL_VERSION >= __s10
#include <sys/contract/process.h>
#include <sys/ctfs.h>
#include <zone.h>
#include <libcontract.h>
#include <sys/task.h>
#include <project.h>
#include <pwd.h>
#include <scadmin/scconf.h>
scconf_errno_t scconf_err = SCCONF_NOERR;
uint_t zc_id;
#define	PWD_BUF_SIZE	1024
#endif


/*
 * usage: ssm_wrapper [ -c | -u ] -R resource -T resource_type -G resource_group
 *	[-Z zone_name] [ -r ... ] [ -x ... ] [ -g ... ] [ -P project_name ]
 *	-m method_context
 *	[ -p path_to_real_method ] [-z zone_to_launch_method_into]
 *
 *			where method_context is one of:
 *
 *				VALIDATE
 *				BOOT
 *				INIT
 *				START
 *				UPDATE
 *				MONITOR_CHECK
 *				FINI
 *				STOP
 *				MONITOR_START
 *				MONITOR_STOP
 *				PRENET_START
 *				POSTNET_STOP
 *
 *			and the -m and -p and -z (if specified) must come
 *			at the end of the argument list
 */
void
usage(void)
{
	int i;

	(void) fprintf(stderr,
	    gettext("usage: %s [ -c | -u ] -R resource -T resource_type "
	    "-G resource_group\n"
#if SOL_VERSION >= __s10
	    "\t[-Z zonename] [ -r ... ] [ -x ... ] [ -g ... ]\n"
	    "\t[-P projectname] \n"
	    "\t-m method_context [ -p path_to_real_method ] [-z zonename]\n"),
#else
	    "\t[ -r ... ] [ -x ... ] [ -g ... ]\n"
	    "\t-m method_context [ -p path_to_real_method ]\n"),
#endif
	    SSM_PROG_NAME);

	(void) fprintf(stderr,
	    gettext("\n\t\twhere method_context is one of:\n\n"));

	/* Print all method names that are currently defined */
	for (i = 0; method_names[i].c_string != NULL; i++) {
		(void) fprintf(stderr,
		    gettext("\t\t\t%s\n"),
		    method_names[i].c_string);
	}

	(void) fprintf(stderr,
	    gettext("\n\t\tand the -m and -p & -z (if specified) must come\n"
	    "\t\tat the end of the argument list\n"));
	exit(1);
}

#if SOL_VERSION >= __s10
static int
init_template(void)
{
	int fd;
	int err = 0;

	fd = open64(CTFS_ROOT "/process/template", O_RDWR);
	if (fd == -1)
		return (-1);

	/*
	 * zlogin doesn't do anything with the contract.
	 * Deliver no events, don't inherit, and allow it to be orphaned.
	 */
	err |= ct_tmpl_set_critical(fd, 0);
	err |= ct_tmpl_set_informative(fd, 0);
	err |= ct_pr_tmpl_set_fatal(fd, CT_PR_EV_HWERR);
	err |= ct_pr_tmpl_set_param(fd, CT_PR_PGRPONLY | CT_PR_REGENT);
	if (err || ct_tmpl_activate(fd)) {
		(void) close(fd);
		return (-1);
	}

	return (fd);
}
#endif


/*
 * This program is called by the RGM to make the necessary scalable
 * services calls before invoking the real RT method.
 */
int
main(int argc, char *argv[])
{
	int			c;
	int			rc = 0;
	int			i;
	char			*rs = NULL;
	char			*rg = NULL;
	char			*rt = NULL;
	char			*rt_method = NULL;
	char			*zonename = NULL;
#if SOL_VERSION >= __s10
	char			*projectname = NULL;
	char			*username = NULL;
	struct passwd		pwd;
	char			pwd_buff[PWD_BUF_SIZE];
#endif
	method_codes_t		method_opcode = CTAB_CODE_INVALID;
	validate_mode_t		vmode = VALIDATE_NONE;
	pid_t			ssm_child_pid;
	pid_t			wait_pid;
	int			ssm_child_stat;
	int			ssm_wstat;
	int			ssm_rc = 0;
#if SOL_VERSION >= __s10
	zoneid_t		zoneid = GLOBAL_ZONEID;
#endif
	boolean_t		enter_local_zone = B_FALSE;
	int			tmpl_fd;
	extern int optind;
	/* used to store the index of -P option in the optarg list */
	int indexofP = 0;

	/* I18N housekeeping */
	(void) setlocale(LC_ALL, "");
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);
	(void) textdomain(TEXT_DOMAIN);

	if (sc_zonescheck() != 0)
		return (1);

	/*
	 * z flag tells the zone in which to launch the method.
	 * Z flag tells us on behalf of which zone is the call
	 * being made and is only valid if z flag is NULL.
	 * It is callers responsibility to make sense out of Z flag
	 * P flag tells us about the project under which the service
	 * is registered and U flag tells us about the owner/user of
	 * the service.
	 * Some code below assumes that the -m and -p and [-z] come
	 * at the end
	 */
#if SOL_VERSION >= __s10
	while ((c = getopt(argc, argv, "z:m:p:R:T:G:Z:P:r:x:g:cu")) != EOF) {
#else
	while ((c = getopt(argc, argv, "m:p:R:T:G:r:x:g:cu")) != EOF) {
#endif
		switch (c) {
			case 'm':
				method_opcode = string_to_code(optarg,
				    method_names);
				break;
			case 'p':
				rt_method = optarg;
				break;
#if SOL_VERSION >= __s10
			case 'z':
				zonename = optarg;
				break;
			case 'P':
				projectname = optarg;
				indexofP = optind - 2;
				break;
#endif
			case 'R':
				rs = optarg;
				break;
			case 'T':
				rt = optarg;
				break;
			case 'G':
				rg = optarg;
				break;
#if SOL_VERSION >= __s10
			case 'Z':
				break;
#endif
			case 'c':
				vmode = VALIDATE_CREATE;
				break;
			case 'u':
				vmode = VALIDATE_UPDATE;
				break;
			case 'r':
			case 'x':
			case 'g':
				break;
			default:
				usage();
				break;
		}
	}

	ssm_syslog_init(rs, rg, rt,
	    code_to_string(method_opcode, method_names));


	/* Validate that the required options were given */
	if (rs == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The usage of the program was incorrect for the reason
		 * given.
		 * @user_action
		 * Use the correct syntax for the program.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Incorrect usage: %s.",
		    "Resource name was not specified.");
		usage();
	}
	if (rg == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Incorrect usage: %s.",
		    "Resource group name was not specified.");
		usage();
	}
	if (rt == NULL) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Incorrect usage: %s.",
		    "Resource type name was not specified.");
		usage();
	}
#if SOL_VERSION >= __s10

	/* The -P option must not be passed down to the methods. */
	if (indexofP) {
		for (i = indexofP; argv[i + 2] != NULL; i++) {
			argv[i] = argv[i+2];
		}
		argv[i] = NULL;
		argc = argc - 2;
	}

	if (zonename != NULL && (zoneid = getzoneidbyname(zonename)) == -1) {
		/*
		 * SCMSGS
		 * @explanation
		 * A scalable service resource attempting to execute in a
		 * non-global zone was unable to find the zone name in
		 * Solaris. This causes the currently executing method to
		 * fail. The RGM takes the usual recovery actions for a method
		 * failure. Other related syslog messages might be found near
		 * this one.
		 * @user_action
		 * If the cause of the failure cannot be determined from
		 * syslog messages, contact your authorized Sun service
		 * provider to determine whether a workaround or patch is
		 * available.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "couldn't query zonename");
		exit(1);
	}
	enter_local_zone = (zoneid != GLOBAL_ZONEID) ? B_TRUE : B_FALSE;
#endif
	if (method_opcode == CTAB_CODE_INVALID) {
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Incorrect usage: %s.",
		    "Method context was not valid.");
		usage();
	}

	/*
	 * Create two proccesses:
	 *
	 * Parent:
	 *	- fork child to do the real SSM work
	 *	- wait for child to complete
	 *	- if child exits successfully, invoke the real
	 *	  RT method if one exists
	 *	- if child exits unsuccessfully, the bail out
	 *	  and exit unsuccessfully without calling the real
	 *	  RT method.
	 *
	 * Child:
	 *	- Gather all necessary information (from command
	 *	  line args or via the scha_api) to make the appropriate
	 *	  SSM method callback directly.
	 *	- Exit unsuccessfully if fatal errors were encountered
	 *	  that should prevent the real RT method from being called.
	 */

#if SOL_VERSION >= __s10
	if (enter_local_zone) {
		if ((tmpl_fd = init_template()) == -1) {
			/*
			 * SCMSGS
			 * @explanation
			 * A scalable service resource attempting to execute
			 * in a non-global zone was unable to initialize a
			 * Solaris contract. This causes the currently
			 * executing method to fail. The RGM takes the usual
			 * recovery actions for a method failure. Other
			 * related syslog messages might be found near this
			 * one.
			 * @user_action
			 * If the cause of the failure cannot be determined
			 * from syslog messages, contact your authorized Sun
			 * service provider to determine whether a workaround
			 * or patch is available.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "init template failed");
			exit(1);
		}

	}
#endif
#if SOL_VERSION >= __s10
	/*
	 * if zonename given is a ZC then zonename should be passed else
	 * pass NULL
	 */
	scconf_err = scconf_get_zone_cluster_id(zonename, &zc_id);
	if (scconf_err != SCCONF_NOERR)
		ssm_rc = ssm_call(rs, rg, rt, method_opcode, vmode,
		    NULL, argc, argv);
	else
		ssm_rc = ssm_call(rs, rg, rt, method_opcode, vmode,
		    zonename, argc, argv);
#else
	ssm_rc = ssm_call(rs, rg, rt, method_opcode, vmode,
	    NULL, argc, argv);

#endif
	if (ssm_rc != SCHA_SSM_SUCCESS) {
		/* SSM call failed.  Abort this method. */
		/*
		 * SCMSGS
		 * @explanation
		 * The proccessing that is required for scalable services did
		 * not complete successfully.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "Failed to configure the networking components "
		    "for scalable resource %s for method %s.",
		    rs, code_to_string(method_opcode, method_names));
		exit(1);
	} else {
		/* Successful execution of the SSM method */
		/*
		 * SCMSGS
		 * @explanation
		 * The calls to the underlying scalable networking code
		 * succeeded.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_NOTICE, MESSAGE,
		    "The networking components for scalable "
		    "resource %s have been configured successfully "
		    "for method %s.",
		    rs,
		    code_to_string(method_opcode, method_names));
		}

	if (rt_method == NULL) {
		return (ssm_rc);
	}

	/* call the real RT method */

	ssm_child_pid = fork();
#if SOL_VERSION >= __s10
	if (enter_local_zone)
		/*
		 * we know tmpl_fd has been initialized in this code
		 * patch, so suppress lint error
		 */
		(void) ct_tmpl_clear(tmpl_fd); /*lint !e644 */
#endif
	if (ssm_child_pid == (pid_t)-1) {
		/* fork failed */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "fork() failed: %m.");
		exit(1);
	}


	if (ssm_child_pid == 0) {
		/* child process */
		argv[0] = rt_method;
		for (i = 0; argv[i] != NULL; i++) {
			if (strcmp("-m", argv[i]) == 0) {
				argv[i] = NULL;
				break;
			}
		}

#if SOL_VERSION >= __s10
		if (enter_local_zone && zone_enter(zoneid) == -1) {
			/*
			 * SCMSGS
			 * @explanation
			 * A scalable service resource attempting to execute
			 * in a non-global zone was unable to enter the zone.
			 * This might indicate that the zone has died. This
			 * causes the currently executing method to fail. The
			 * RGM takes the usual recovery actions for a method
			 * failure. Other related syslog messages might be
			 * found near this one.
			 * @user_action
			 * If the cause of the failure cannot be determined
			 * from syslog messages, contact your authorized Sun
			 * service provider to determine whether a workaround
			 * or patch is available.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "zone enter failed: %m. %d", zoneid);
			exit(1);
		}

		/* User name */
		if (getpwuid_r(getuid(), &pwd, pwd_buff, PWD_BUF_SIZE)
		    == NULL) {
			(void) sc_syslog_msg_initialize(&log_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(
			    log_handle, LOG_ERR, MESSAGE,
			    "<%s> getpwuid_r uid %d error %s",
			    rs, getuid(), strerror(errno)); /*lint !e746 */
			sc_syslog_msg_done(&log_handle);
			(void) sc_syslog_msg_initialize(&log_handle,
			    SC_SYSLOG_RGM_FED_TAG, "");
			(void) sc_syslog_msg_log(
			    log_handle, LOG_WARNING, MESSAGE,
			    "<%s> using fake user name for setproject",
			    rs);
			sc_syslog_msg_done(&log_handle);
			username = "root";
		} else {
			username = pwd.pw_name;
		}
		/* Set the project name */
		if (enter_local_zone && setproject(projectname,
		    username, TASK_NORMAL) != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * setproject call failed. Should never occur.
			 * @user_action
			 * Verify project database.
			 * Contact your authorized Sun service
			 * provider to determine whether
			 * a workaround or patch is available.
			 */

			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "setproject call failed : %s",
			    strerror(errno)); /*lint !e746 */
			exit(1);
		}
#endif

		rc = execv(rt_method, argv);
		if (rc == -1) {
			/*
			 * SCMSGS
			 * @explanation
			 * The exec() system call failed for the given reason.
			 * @user_action
			 * Verify that the pathname given is valid.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "exec() of %s failed: %m.",
			    rt_method);
			exit(1);
		}

	}
	/* parent */
	while (1) {
		wait_pid = waitpid(ssm_child_pid, &ssm_child_stat, 0);
		if (wait_pid == -1 && errno == EINTR) /*lint !e746 */
			continue;
		if (wait_pid == -1) {
			/* unexpected failure. bail out */
			/*
			 * SCMSGS
			 * @explanation
			 * The waitpid() system call failed for the given
			 * reason.
			 * @user_action
			 * Save a copy of the /var/adm/messages files on all
			 * nodes. Contact your authorized Sun service provider
			 * for assistance in diagnosing the problem.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "waitpid() failed: %m.");
			exit(1);
		}
		break;
	}
	/* lint doesn't understand WEXITSTATUS */
	ssm_wstat = WEXITSTATUS(ssm_child_stat); /*lint !e702 */
	return (ssm_wstat);
}
