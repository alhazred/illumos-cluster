/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ssm_ipmp_callback.c - Callback utility registered with IPMP to
 * call us back when the IPMP group goes bad.
 *
 * This version was created by modifying
 *	cluster/src/cmd/ha-services/hascip/hascip_ipmp_callback.c
 */

#pragma ident "@(#)ssm_ipmp_callback.c 1.42 08/07/31 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <rgm/pnm.h>
#include <rgm/rgm_common.h>
#include <rgm/rgm_scswitch.h>
#include <scha.h>
#include "ssm_util.h"

static ulong_t ssm_ipmp_delay(void);

/*
 * This binary is registered with pnmd by the MONITOR_START method and
 * is unregistered by the MONITOR_STOP method. pnmd calls it for 3 situations:
 * a) An IPMP group has failed
 * b) An IPMP group has been updated
 * c) An IPMP group has repaired
 *
 * The command line of this program looks like this:
 * progname rgname rsname rtname failed/updated/repaired groupname delay/nodelay
 *
 * "updated" event is ignored, SSM has no interest in it.
 *
 * For a "failed" event:
 * a) If there is a utility trying to rebalance the RG, it is stopped.
 * b) Launches a retry utility in order to get the resource off this node.
 *
 * For a "repair" event:
 * a) If there is a utility trying to get the RG off this node, it is stopped.
 * b) Launches a retry utility that tries to rebalance the RG.
 *
 */

int
main(int argc, char *argv[])
{
	char *rgname = NULL, *rsname = NULL, *rtname = NULL;
	char *nafoname = NULL, *event = NULL, *action = NULL;
	pid_t		child_pid;
	ulong_t delay;
	char *cluster;

	if (sc_zonescheck() != 0)
		return (1);
	/*
	 * The callback string is
	 * "progname rgname rsname rtname failed/updated/repaired ipmpname
	 * delay/nodelay"
	 */
	if (argc < 7) {
		/*
		 * The args are used to initialize the syslog below,
		 * but if the args aren't set correctly, then we
		 * cannot use them to initialize the syslog.
		 * So we just pass in NULL, which will cause a generic
		 * string to be used for each component of the tag.
		 */
		ssm_syslog_init(NULL, NULL, NULL, SSM_CALLBACK_METHOD);
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.",
		    "Expected command usage is: " SSM_CALLBACK_NAME
		    " RG_name R_name RT_name event ipmpgroup delay/nodelay");
		exit(1);
	}

	if (argc == 8) {
		cluster = (char *)argv[1];
		rgname = argv[2];
		rsname = argv[3];
		rtname = argv[4];
		event = argv[5];
		nafoname = argv[6];
		action = argv[7];
	} else {
		cluster = NULL;
		rgname = argv[1];
		rsname = argv[2];
		rtname = argv[3];
		event = argv[4];
		nafoname = argv[5];
		action = argv[6];
	}

	ssm_syslog_init(rsname, rgname, rtname, SSM_CALLBACK_METHOD);

	if (rgname == NULL || rsname == NULL || rtname == NULL ||
	    event == NULL || nafoname == NULL || action == NULL) {
		/* unexpected failure. bail out */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.",
		    "Invalid values were passed to " SSM_CALLBACK_NAME);
		exit(1);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG,
	    "Program %s was called for method %s for resource %s "
	    "in group %s of type %s for event %s for IPMP group %s with %s.",
	    SSM_CALLBACK_NAME, SSM_CALLBACK_METHOD, rsname,
	    rgname, rtname, event, nafoname, action);

	/* is_resource_on will log error and exit(1) if it fails */
	if (!is_resource_on(cluster, rsname, rgname)) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG, "Nothing to do, resource %s is not "
		    "switched on", rsname);
		exit(0);
	}

	/* is_resource_monitored will log error and exit(1) if it fails */
	if (!is_resource_monitored(cluster, rsname, rgname)) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG, "Nothing to do, resource %s is not "
		    "monitored", rsname);
		exit(0);
	}

	/*
	 * At this point the IPMP daemon is executing us via
	 * a call to system(). If we make RPC calls to it,
	 * there is a chance of deadlock. Thus we spawn
	 * a child and make the parent exit. The deadlock
	 * prevention and locking in the IPMP daemon is
	 * on an "best effort basis only", particularly
	 * when it comes to callbacks. Better play it safe.
	 */
	child_pid = fork();
	if (child_pid == (pid_t)-1) {
		/*
		 * If can't even fork(). There is something very
		 * wrong with the system. Let us not make it worse
		 * by doing other stuff.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * The fork() system call failed for the given reason.
		 * @user_action
		 * If system resources are not available, consider rebooting
		 * the node.
		 */
		(void) sc_syslog_msg_log(log_handle,
		    SC_SYSLOG_ERROR, MESSAGE,
		    "fork() failed: %m.");
		exit(1);
	}

	if (child_pid != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG,
		    "Parent process exiting. Child process %d will continue.",
		    child_pid);
		return (0);
	}
	/* Child continues here */

	/* if the event is "repaired", rebalance this rg */
	if (strcmp(event, PNM_EVENT_REPAIRED) == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The named IPMP group is now functional.
		 * @user_action
		 * No user action is required. This is an informational
		 * message.
		 */
		(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_NOTICE,
		    MESSAGE, "Received notice that IPMP group %s has repaired.",
		    nafoname);

		/* stop any attempts to get the rg off this node */
		(void) ssm_stop_getoff(rgname, rsname);

		/* launch utility that will rebalance the rg */
		return (ssm_start_retry(rsname, rgname, rtname, cluster,
		    MODE_REBALANCE, 0));
	}

	/* if the event is "failed", get the rg off this node */
	if (strcmp(event, PNM_EVENT_FAILED) == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The status of the named IPMP group has become degraded. If
		 * possible, the scalable resources currently running on this
		 * node with monitoring enabled will be relocated off of this
		 * node, if the IPMP group stays in a degraded state.
		 * @user_action
		 * Check the status of the IPMP group on the node. Try to fix
		 * the adapters in the IPMP group.
		 */
		(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_NOTICE,
		    MESSAGE, "Received notice that IPMP group %s has failed.",
		    nafoname);

		/* stop any attempts to rebalance the RG */
		(void) ssm_stop_rebalance(rgname);

		/*
		 * Note that we dont actually sleep here but launch the
		 * retry utility and ask it to sleep instead. This is
		 * to make sure that ssm_retry is launched immediately
		 * in response to a callback. If we didnt do this, we'd
		 * be susceptible to this race:
		 * 1. ipmp group fails, getoff callback is invoked
		 * 2. getoff callback sleeps
		 * 3. ipmp group repairs _before_ the getoff callback has
		 *    launched the retry utility, rebalance callback is invoked
		 * 4. rebalance retry is launched and tries to kill any
		 *    getoff retry utility (but none is found)
		 * 5. the original getoff callback wakes up and launches
		 *    the getoff retry. Now both getoff and rebalance
		 *    retry utilities are running!
		 */
		if (strcmp(action, "delay") == 0) {
			delay = ssm_ipmp_delay();
		} else {
			/*
			 * wait for a very short (100ms) duration, just in case
			 * this is a cluster wide network outage. Ideally, all
			 * the nodes will know about the outage at the same
			 * instant. This is just an extra precaution.
			 */
			delay = 100000;

			/*
			 * pnmd has requested an immediate failover. No need to
			 * wait for the other nodes to detect this failure
			 * (will be the case if the whole subnet failed)
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * All network interfaces in the IPMP group have
			 * failed. All of these failures were detected by the
			 * hardware drivers for the network interfaces, and
			 * not by in.mpathd probes. In such a situation cl_pnmd
			 * requests all scalable resources to fail over to
			 * another node without any delay.
			 * @user_action
			 * No user action is needed. This is an informational
			 * message that indicates that the scalable resources
			 * will be failed over to another node immediately.
			 */
			(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_NOTICE,
			    MESSAGE, "cl_pnmd has requested an immediate "
			    "failover of all scalable resources dependent on "
			    "IPMP group %s", nafoname);
		}
		/*
		 * SCMSGS
		 * @explanation
		 * The named IPMP group has failed, so the node may not be
		 * able to respond to client requests. It would be desirable
		 * to move the resource to another node that has functioning
		 * IPMP groups. A request will be issued on behalf of this
		 * resource to relocate the resource to another node.
		 * @user_action
		 * Check the status of the IPMP group on the node. Try to fix
		 * the adapters in the IPMP group.
		 */
		(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_ERROR, MESSAGE,
		    "IPMP group %s has failed, so scalable resource %s in "
		    "resource group %s may not be able to respond to client "
		    "requests. A request will be issued to relocate resource "
		    "%s off of this node.", nafoname, rsname, rgname, rsname);
		return (
		    ssm_start_retry(
		    rsname, rgname, rtname, cluster, MODE_GETOFF, delay));
	}

	/* ignore any other event */
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG, "Ignoring callback for event %s for group %s.",
	    event, nafoname);
	return (0);
}

/*
 * XXX - This code is duplicated here from libhaip to avoid having
 * to link with the data services' library (libdsutil).
 * The code was taken from "@(#)haip_ipmp.c 1.9 00/03/14 SMI".
 *
 * As described below, the code from the haip library was taken from
 * the nafod code.  This code should be kept consistent in all three
 * places.  Ideally, pnm would provide an interface to query for these
 * values.
 */

/*
 * ssm_ipmp_delay()
 * Get the value of IPMP FAILURE_DETECTION_TIME. Called by
 * IPMP callback utility. Makes sure that in case of
 * subnet failures, other nodes would have detected
 * the problem as well.
 *
 * The code has been now changed to read /etc/default/mpathd
 * file for FAILURE_DETECTION_TIME for equivalent
 * functionality for IPMP based implementation of
 * PNM (Public Network Monitoring).
 */
/* Default value of the failure detection time */
#define	MPATHD_DETECTION_TIME		10000
#define	MPATHD_DEFAULT_FILE		"/etc/default/mpathd"
#define	MPATHD_DETECTION_PARAM	"FAILURE_DETECTION_TIME"
#define	MPATHD_LINELEN		1024

static ulong_t
ssm_ipmp_delay(void)
{
	ulong_t		sleep_time;
	FILE	*fp = NULL;
	char	line[MPATHD_LINELEN];
	char	*p = NULL, *val = NULL, *lasts = NULL;
	ulong_t		v;

	sleep_time = MPATHD_DETECTION_TIME;

	fp = fopen(MPATHD_DEFAULT_FILE, "r");
	if (fp == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		syslog(LOG_DEBUG, "Unable to open <%s>: %s.",
			MPATHD_DEFAULT_FILE, strerror(errno));	/*lint !e746 */
	} else {
		while (fgets(line, MPATHD_LINELEN, fp) != NULL) {
			/* Skip white spaces */
			for (p = line; isspace(*p); p++)
				;
			/* Skip empty lines */
			if (*p == 0)
				continue;
			/* Skip comment lines */
			if (*p == '#')
				continue;
			if ((p = (char *)strtok_r(p, "=", &lasts)) == NULL)
				continue;
			if ((val = (char *)strtok_r(NULL, "\n", &lasts))
				== NULL)
				continue;
			if (strcasecmp(p, MPATHD_DETECTION_PARAM) == 0) {
				v = (ulong_t)atol(val);
				if (v > 0) {
					sleep_time = v;
				}
			}
		}
	}
	/* Convert milliseconds to microseconds */
	sleep_time *= 1000;
	/*
	 * SCMSGS
	 * @explanation
	 * Need explanation of this message!
	 * @user_action
	 * Need a user action for this message.
	 */
	syslog(LOG_DEBUG, "Sleep time is %ld.", sleep_time);
	return (sleep_time);
}
