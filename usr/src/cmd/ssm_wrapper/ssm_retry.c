/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident "@(#)ssm_retry.c 1.7 08/07/25 SMI"

#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <rgm/rgm_common.h>
#include <rgm/rgm_scswitch.h>
#include "ssm_util.h"
/*
 * ssm_retry.c - utility launched by ssm_ipmp_callback in response to
 * failure or recovery events.
 *
 * This utility is started under pmf and can be used for two purposes:
 * a) getting the specified resource group off this node
 * b) rebalancing the specified resource group.
 *
 * For both of these, this utility is willing to wait for some time and
 * retry the operation if it fails. eg if rebalance doesnt work the first
 * time, the utility will sleep a while and then retry rebalancing the
 * resource group. This will continue until either the operation succeeds,
 * or the utility is stopped.
 *
 * This utility takes the following parameters:
 * -R <resource name>: resource name
 * -T <resource type>: resource type
 * -G <groupname> : resource group on which to perform the operation
 * -r : Rebalance the specified RG
 * -g : Get the specified RG off this node
 * -d <initial delay> : Delay for these many microseconds before starting
 *
 * One and only one of -r or -g must be specified.
 *
 * ssm_retry is a private, undocumented interface. The flags that it
 * understands, and how it interprets these flags is specific to ssm_retry.
 * Other sun cluster commands might interpret the same flag(s) in a different
 * way.
 *
 */

static boolean_t rg_offline_here(char *zone, char *rgname);

int
main(int argc, char *argv[])
{
	int c;
	long delay = -1;
	ssm_retry_mode_t mode = MODE_INVALID;
	char *rgname = NULL, *rsname = NULL, *rtname = NULL;
	char *zonename = NULL, *msg;
	const char *rg[] = {NULL, NULL};
	scha_errmsg_t e;
	char err[INT_ERR_MSG_SIZE];
	boolean_t parsed;

	if (sc_zonescheck() != 0)
		return (1);

	srandom((uint_t)getpid());
	/* process cmdline args */
	parsed = B_TRUE;
	while (parsed && ((c = getopt(argc, argv, ":R:T:G:Z:rgd:")) != -1)) {
		switch (c) {
		case 'R':
			rsname = optarg;
			break;
		case 'T':
			rtname = optarg;
			break;
		case 'G':
			rgname = optarg;
			break;
		case 'Z':
			zonename = optarg;
			break;
		/*
		 * if both -r and -g are specified, the one that comes
		 * last takes precedence
		 */
		case 'r':
			mode = MODE_REBALANCE;
			break;
		case 'g':
			mode = MODE_GETOFF;
			break;

		case 'd':
			errno = 0; /*lint !e746 */
			delay = atol(optarg);
			if (errno) {
				(void) snprintf(err, sizeof (err),
				    "%s is not a valid value for delay",
				    optarg);
				parsed = B_FALSE;
			}
			break;
		case ':':
			/* missing operand for one of the options */
			(void) snprintf(err, sizeof (err),
			    "option %s needs an operand", optopt);
			parsed = B_FALSE;
			break;
		case '?':
			/* unknown option */
			(void) snprintf(err, sizeof (err),
			    "%c is not a valid option", optopt);
			parsed = B_FALSE;
			break;
		default:
			/* unknown error */
			(void) snprintf(err, sizeof (err),
			    "unknown error parsing command line");
			parsed = B_FALSE;
			break;
		}
	}

	/* initialize the syslog settings */
	ssm_syslog_init(rsname, rgname, rtname, SSM_RETRY_METHOD);

	/* todo: fail if some parameter is invalid */
	if (mode == MODE_INVALID) {
		(void) snprintf(err, sizeof (err), "no operation specified");
		parsed = B_FALSE;
	}

	/* both operations need an rgname */
	if (rgname == NULL) {
		(void) snprintf(err, sizeof (err), "no resource group "
		    "specified");
		parsed = B_FALSE;
	}

	/* getoff needs an rsname also */
	if ((mode == MODE_GETOFF) && (rsname == NULL)) {
		(void) snprintf(err, sizeof (err), "no resource name "
		    "specified");
		parsed = B_FALSE;
	}

	if (!parsed) {
		(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_ERROR, MESSAGE,
		    "INTERNAL ERROR: %s.", err);
		exit(1); /* let pmf complain that the tag failed to stay up */
	}

	/*
	 * Were we asked to sleep a bit before starting? ssm_ipmp_callback
	 * usually sets this to mpathd's FDT. The only case where its 0 for
	 * a failure is for a linkdown notification, which means failover right
	 * away.
	 */
	if (delay > 0)
		(void) usleep((uint_t)delay);

	rg[0] = rgname;

	while (1) {
		if (mode == MODE_REBALANCE) {
			e.err_code = 0;
			e.err_msg = NULL;

			/*
			 * If the RG is _anything_ other than OFFLINE,
			 * go away without attempting a rebalance.
			 * The idea is to attempt a rebalance iff the RG
			 * is in a well known state.
			 */
			if (!rg_offline_here(zonename, rgname)) {
				(void) ssm_stop_rebalance(rgname);
				break;
			}

			/*
			 * rebalance the rg
			 * rgm_scswitch_switch_rg is changed to take an
			 * extra parameter.
			 * set the verbose flag param to B_FALSE.
			 */
			e = rgm_scswitch_switch_rg(NULL, rg, RGACTION_REBALANCE,
			    0, B_FALSE, zonename);
			if (e.err_code == SCHA_ERR_NOERR) {
				/*
				 * SCMSGS
				 * @explanation
				 * The named resource group was successfully
				 * brought online on the cluster node.
				 * @user_action
				 * This is an informational message, no user
				 * action is needed.
				 */
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_NOTICE, MESSAGE,
				    "Resource group %s rebalanced successfully",
				    rgname);
				/*
				 * die in such a way that pmf doesnt say
				 * tag failed to stay up
				 */
				(void) ssm_stop_rebalance(rgname);

				/* shouldnt come here */
				break;
			}

			/*
			 * rebalance did not work, but is it because we are
			 * undergoing a reconfiguration of some sort? Maybe
			 * the other node(s) got the rebalance request in
			 * before this node...
			 *
			 * In that case, sleep for a short semi-random time
			 * and retry again. The interval is varied to ensure
			 * that there is a better chance of one request not
			 * causing a reconf error for others.
			 */
			if (e.err_code == SCHA_ERR_RECONF ||
			    e.err_code == SCHA_ERR_RGRECONF ||
			    e.err_code == SCHA_ERR_CLRECONF) {
				uint_t rnd = 30 + ((uint_t)random() % 30);

				/*
				 * SCMSGS
				 * @explanation
				 * The named resource group, or some other
				 * part of the cluster is undergoing
				 * reconfiguration. Due to this, a request to
				 * migrate this resource group or to rebalance
				 * it did not work. The utility will recheck
				 * the state of the resource group after the
				 * time duration specified in the message.
				 * @user_action
				 * This is an informational message, no user
				 * action is needed.
				 */
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_NOTICE, MESSAGE,
				    "Resource group %s is undergoing "
				    "reconfiguration, will check again in %ds",
				    rgname, rnd);

				if (e.err_msg)
					free(e.err_msg);

				/* sleep for between 30-60s and try again */
				(void) sleep(rnd);

				continue;
			}

			/*
			 * we get here if we failed with something other than
			 * a reconf and if the rg is not online here.
			 */
			msg = e.err_msg ? e.err_msg : rgm_error_msg(e.err_code);
			/*
			 * SCMSGS
			 * @explanation
			 * An attempt was made to bring the named resource
			 * group online on the cluster node but it failed. The
			 * reason why it failed is also provided in the
			 * message.
			 * @user_action
			 * Wait to see if a subsequent message indicates that
			 * more attempts will be made. If no such message
			 * shows up, save a copy of the syslog on all nodes
			 * and contact your authorized Sun service provider
			 * for assistance.
			 */
			(void) sc_syslog_msg_log(log_handle,
			    SC_SYSLOG_ERROR, MESSAGE,
			    "Attempt to rebalance resource group %s failed: %s",
			    rgname, msg);
			if (e.err_msg)
				free(e.err_msg);
		} else if (mode == MODE_GETOFF) {
			int rc;

			/* get off this node */
			rc = scha_control_zone(
			    "GETOFF", rgname, rsname, zonename);

			if (rc == SCHA_ERR_NOERR) {
				/*
				 * die in such a way that pmf doesnt say
				 * tag failed to stay up
				 */
				(void) ssm_stop_getoff(rgname, rsname);

				/* shouldnt come here */
				break;
			} else if (rc == SCHA_ERR_RECONF) {
				uint_t rnd = 30 + ((uint_t)random() % 30);

				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_NOTICE, MESSAGE,
				    "Resource group %s is undergoing "
				    "reconfiguration, will check again in %ds",
				    rgname, rnd);
				(void) sleep(rnd);
				continue;
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * The failover attempt of the resource is
				 * rejected or encountered an error.
				 * @user_action
				 * For more detailed error message, check the
				 * syslog messages. Check whether the
				 * Pingpong_interval has appropriate value. If
				 * not, adjust it by using clresourcegroup.
				 * Otherwise, use clresourcegroup switch to
				 * switch the resource group to a healthy
				 * node.
				 */
				(void) sc_syslog_msg_log(log_handle,
				    SC_SYSLOG_ERROR, MESSAGE,
				    "Failover attempt failed: %s.",
				    scha_strerror(rc));
			}
		} else {
			/* how did we get here?? mode was validated earlier! */
			(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_NOTICE,
			    MESSAGE, "INTERNAL ERROR: %s.", "Unexpected mode");
			break;
		}

		/* didnt work, sleep for some time and then try again */
		(void) sleep(SSM_RETRY_TIME);
	}

	return (1);
}

/*
 * returns true if the named resource group is offline on this node.
 * In case there is an error, a message is logged and false is returned.
 *
 * Other than logging messages differently, code comes straight from
 * haip_rg_local()
 */
boolean_t
rg_offline_here(char *zone_name, char *rgname)
{
	scha_err_t rc;
	scha_resourcegroup_t rg_handle;
	scha_rgstate_t rg_state;

retry_rg:
	rc = scha_resourcegroup_open_zone(zone_name, rgname, &rg_handle);
	if (rc != SCHA_ERR_NOERR) {
		return (B_FALSE);
	}

	rc = scha_resourcegroup_get_zone(zone_name, rg_handle,
	    SCHA_RG_STATE, &rg_state);
	if (rc == SCHA_ERR_SEQID) {
		(void) sleep(1);
		/* close the resource group handle and try again */
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
		goto retry_rg;
	}

	if (rc != SCHA_ERR_NOERR) {
		(void) sc_syslog_msg_log(log_handle, SC_SYSLOG_ERROR,
		    MESSAGE, "Failed to retrieve the resource group property"
		    " %s: %s.", SCHA_RG_STATE, scha_strerror(rc));
		(void) scha_resourcegroup_close(rg_handle);
		return (B_FALSE);
	}
	(void) scha_resourcegroup_close(rg_handle);

	if (rg_state == SCHA_RGSTATE_OFFLINE ||
	    rg_state == SCHA_RGSTATE_PENDING_OFFLINE)
		return (B_TRUE);
	else
		return (B_FALSE);
}
