/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fmm_cfg.cc	1.8	08/07/31 SMI"

#include <h/ccr.h>
#include <h/naming.h>
#include <nslib/ns.h>
#include <orb/object/adapter.h>
#include <scxcfg/scxcfg.h>

#include <pthread.h>

#include <fmm/fmmutils.h>

#include "fmm_internal_types.h"
#include "fmm_cfg.h"

#define	FMM_CFG_TABLE "farm"

#define	FMM_CFG_HASH_TABLE_SIZE	256

typedef struct fmm_cfg_node_info_t {
	char name[FMM_MAX_NAME_SIZE];
	char ipAddress[FMM_MAX_ADDR_SIZE];
	boolean_t monitoringState;
} fmm_cfg_node_info_t;

typedef struct fmm_cfg_node_t {
	nodeid_t nodeId;
	fmm_cfg_node_info_t *info;
	fmm_cfg_node_info_t *old;
	fmm_cfg_node_t *next;
	fmm_cfg_node_t *prev;
} fmm_cfg_node_t;

typedef fmm_cfg_node_t *fmm_cfg_list_t;

typedef struct fmm_cfg_cluster_t {
	char *name;
	pthread_mutex_t mutex;
	boolean_t initialized;
	fmm_cfg_callback_t callback;
	fmm_cfg_list_t head[FMM_CFG_HASH_TABLE_SIZE];
} fmm_cfg_cluster_t;

static fmm_cfg_cluster_t fmm_cfg_cluster =
	{NULL, PTHREAD_MUTEX_INITIALIZER, B_FALSE, NULL, NULL}; /*lint !e708 */


static unsigned int
fmm_cfg_hash(nodeid_t nodeid)
{
	unsigned int h = 0;
	h = nodeid % FMM_CFG_HASH_TABLE_SIZE;
	return (h);
}


class fmm_cfg_callback_impl : public McServerof<ccr::callback> {
public:
	void did_update(const char *tname, ccr::ccr_update_type,
	    Environment &);
	void did_update_zc(const char *cluster, const char *tname,
	    ccr::ccr_update_type, Environment &);
	void did_recovery_zc(const char *cluster, const char *tname,
	    ccr::ccr_update_type, Environment &) {
		fmm_log(LOG_DEBUG, "did_recovery called for %s:%s\n", cluster,
		    tname);
	}
	void did_recovery(const char *tname, ccr::ccr_update_type,
	    Environment &) {
		fmm_log(LOG_DEBUG, "did_recovery called for %s:%s\n", "global",
		    tname);
	}
	/* CSTYLED */
	void _unreferenced(unref_t){}
};


fmm_cfg_node_info_t *
fmm_cfg_node_info_get(nodeid_t nodeId, scxcfg_t cfg)
{
	fmm_cfg_node_info_t *info;
	char name[FCFG_MAX_LEN];
	char prop[FCFG_MAX_LEN];
	char property[64];
	scxcfg_error error;

	/* Fill in the new info */
	info = (fmm_cfg_node_info_t *)malloc(sizeof (fmm_cfg_node_info_t));
	if (info == NULL) {
		fmm_log(LOG_ERR, "fmm_cfg_node_info_get: no mem - info");
		return (NULL);
	}

	/* Add the node name */
	sprintf(property, FARM_NODE_NAME, nodeId);
	if (scxcfg_getproperty_value(cfg, NULL, property, name, &error) != 0) {
		info->name[0] = '\0';
	} else {
		(void) strcpy(info->name, name);
	}

	/* Add the monitoring state */
	sprintf(property, FARM_NODE_MONITORING_STATE, nodeId);
	if (scxcfg_getproperty_value(cfg, NULL, property, prop, &error) != 0) {
		info->monitoringState = B_TRUE;
	} else {
		if (strcmp(prop, "enabled") == 0) {
			info->monitoringState = B_TRUE;
		} else {
			info->monitoringState = B_FALSE;
		}
	}

	/* Add the ip address */
	sprintf(property, FARM_NODE_IPADDR, nodeId);
	if (scxcfg_getproperty_value(cfg, NULL, property, prop, &error) != 0) {
		info->ipAddress[0] = '\0';
	} else {
		(void) strcpy(info->ipAddress, prop);
	}

	return (info);
}


static fmm_cfg_node_t *
fmm_cfg_node_get(nodeid_t nodeid)
{
	fmm_cfg_node_t *oneNode;
	unsigned int h = fmm_cfg_hash(nodeid);

	(void) pthread_mutex_lock(&fmm_cfg_cluster.mutex);
	for (oneNode = fmm_cfg_cluster.head[h]; oneNode != NULL;
	    oneNode = oneNode->next) {
		if (oneNode->nodeId == nodeid) {
			(void) pthread_mutex_unlock(&fmm_cfg_cluster.mutex);
			return (oneNode);
		}
	}
	(void) pthread_mutex_unlock(&fmm_cfg_cluster.mutex);
	return (NULL);
}


static void
fmm_cfg_node_update(fmm_cfg_node_t *fmm_cfg_node, scxcfg_t cfg)
{
	/* Free prev info */
	if (fmm_cfg_node->info)
		free(fmm_cfg_node->info);

	/* Fill in the new info */
	fmm_cfg_node->info = fmm_cfg_node_info_get(fmm_cfg_node->nodeId, cfg);
}


static fmm_cfg_node_t *
fmm_cfg_node_create(nodeid_t nodeid, scxcfg_t cfg)
{
	fmm_cfg_node_t *fmm_cfg_node;
	fmm_cfg_node_info_t *fmm_cfg_node_info;

	/* Allocate space for next node and set nodeId */
	fmm_cfg_node = (fmm_cfg_node_t *)malloc(sizeof (fmm_cfg_node_t));
	if (fmm_cfg_node == NULL) {
		fmm_log(LOG_ERR, "fmm_cfg_node_create: no mem - node");
		return (NULL);
	}

	/* retrieve info */
	fmm_cfg_node_info = fmm_cfg_node_info_get(nodeid, cfg);

	fmm_cfg_node->next = NULL;
	fmm_cfg_node->prev = NULL;
	fmm_cfg_node->nodeId = nodeid;
	fmm_cfg_node->info = fmm_cfg_node_info;
	fmm_cfg_node->old = NULL;

	return (fmm_cfg_node);
}


void
fmm_cfg_cluster_save_old(void)
{
	fmm_cfg_node_t *oneNode;
	unsigned int h = 0;

	(void) pthread_mutex_lock(&fmm_cfg_cluster.mutex);
	for (h = 0; h < FMM_CFG_HASH_TABLE_SIZE; h++) {
		for (oneNode = fmm_cfg_cluster.head[h]; oneNode != NULL;
		    oneNode = oneNode->next) {
			if (oneNode->old != NULL)
				free(oneNode->old);
			oneNode->old = oneNode->info;
			oneNode->info = NULL;
		}
	}
	(void) pthread_mutex_unlock(&fmm_cfg_cluster.mutex);
}


static int
fmm_cfg_cluster_read(void)
{
	scxcfg cfg;
	f_property_t *list_value;
	f_property_t *elem;
	char property[64];
	scxcfg_error error;
	char value[FCFG_MAX_LEN];
	int num = 0;

	fmm_cfg_node_t *fmm_cfg_node = NULL;
	int nodeid;
	int rstatus = 0;
	unsigned int h = 0;

	if (scxcfg_open(&cfg, &error) != 0) {
		fmm_log(LOG_ERR, "fmm_cfg_cluster_read: unable to open cfg");
		return (-1);
	}

	if (scxcfg_getlistproperty_value(&cfg, NULL, FARM_NODES, &list_value,
	    &num, &error) != 0) {
		if (error.scxcfg_errno != FCFG_ENOTFOUND) {
			fmm_log(LOG_ERR,
			    "fmm_cfg_cluster_read: get list error");
			return (-1);
		}
	}
	if (num == 0) {
		/* no farm node in the config */
		goto cleanup;
	}
	/* set the nodelist */
	for (elem = list_value; elem != NULL; elem = elem->next) {
		nodeid = atoi(elem->value);
		if (nodeid < 0) {
			fmm_log(LOG_ERR, "fmm_cfg_cluster_read: invalid id");
			rstatus = -1;
			goto cleanup;
		}

		/* Look for the node in the nodes list */
		fmm_cfg_node = fmm_cfg_node_get(nodeid);

		if (fmm_cfg_node == NULL) {
			/* If not found, create a new one */
			fmm_cfg_node = fmm_cfg_node_create(nodeid, &cfg);

			/* Add the node in the cluster node list */
			(void) pthread_mutex_lock(&fmm_cfg_cluster.mutex);
			h = fmm_cfg_hash(nodeid);
			fmm_cfg_node->next = fmm_cfg_cluster.head[h];
			if (fmm_cfg_cluster.head[h] != NULL)
				fmm_cfg_cluster.head[h]->prev = fmm_cfg_node;
			fmm_cfg_cluster.head[h] = fmm_cfg_node;
			(void) pthread_mutex_unlock(&fmm_cfg_cluster.mutex);
		} else {
			/* If found, update the old one */
			(void) pthread_mutex_lock(&fmm_cfg_cluster.mutex);
			fmm_cfg_node_update(fmm_cfg_node, &cfg);
			(void) pthread_mutex_unlock(&fmm_cfg_cluster.mutex);
		}

	}

cleanup:
	if (list_value != NULL)
		scxcfg_freelist(list_value);

	scxcfg_close(&cfg);
	return (rstatus);
}


void
fmm_cfg_cluster_notify(void)
{
	fmm_cfg_node_t *oneNode;
	unsigned int h = 0;

	(void) pthread_mutex_lock(&fmm_cfg_cluster.mutex);

	for (h = 0; h < FMM_CFG_HASH_TABLE_SIZE; h++) {
		for (oneNode = fmm_cfg_cluster.head[h]; oneNode != NULL;
		    oneNode = oneNode->next) {

			/* If node info old == NULL, new node */
			if (oneNode->old == NULL) {
				fmm_cfg_cluster.callback(FMM_CFG_NODE_ADDED,
				    oneNode->nodeId);
				continue;
			}

			/* If node info == NULL, removed node */
			if (oneNode->info == NULL) {
				fmm_cfg_cluster.callback(FMM_CFG_NODE_REMOVED,
				    oneNode->nodeId);

				/* remove the node */
				if (oneNode == fmm_cfg_cluster.head[h]) {
					/* head  list */
					if (oneNode->next != NULL)
						oneNode->next->prev = NULL;
					fmm_cfg_cluster.head[h] = oneNode->next;
				} else {
					if (oneNode->next != NULL)
						oneNode->next->prev
						    = oneNode->prev;
					oneNode->prev->next = oneNode->next;
				}
				free(oneNode->old);
				continue;
			}

			if ((strcmp(oneNode->info->name, oneNode->old->name)
			    != 0) || (strcmp(oneNode->info->ipAddress,
			    oneNode->old->ipAddress) != 0) ||
			    (oneNode->info->monitoringState
			    != oneNode->old->monitoringState)) {
				fmm_cfg_cluster.callback(FMM_CFG_NODE_CHANGED,
				    oneNode->nodeId);
				continue;
			}
		}
	}

	(void) pthread_mutex_unlock(&fmm_cfg_cluster.mutex);
}

static int
fmm_cfg_get_cluster_name(void)
{
	char name[FCFG_MAX_LEN];
	int rstatus = 0;
	scxcfg cfg;
	scxcfg_error error;

	if (scxcfg_open(&cfg, &error) != 0) {
		rstatus = -1;
		return (rstatus);
	}

	(void) scxcfg_getproperty_value(&cfg, NULL, FARM_NAME, name,
	    &error);
	if (name != NULL) {
		(void) pthread_mutex_lock(&fmm_cfg_cluster.mutex);

		fmm_cfg_cluster.name = strdup(name);
		if (fmm_cfg_cluster.name == NULL) {
			fmm_log(LOG_ERR,
			    "fmm_cfg_get_cluster_name: no mem - cluster name");
			rstatus = -1;
		}

		(void) pthread_mutex_unlock(&fmm_cfg_cluster.mutex);
	} else
		rstatus = -1;

	scxcfg_close(&cfg);
	return (rstatus);
}


static int
fmm_cfg_cluster_init(void)
{
	if (fmm_cfg_get_cluster_name() != 0)
		return (-1);

	if (fmm_cfg_cluster_read() != 0)
		return (-1);

	fmm_cfg_cluster_notify();
	return (0);
}

//
// CCR callback method, this is invoked by the CCR.
//
void fmm_cfg_callback_impl::did_update_zc(const char *cluster,
    const char *tname, ccr::ccr_update_type ccr_op, Environment &)
{
	if (tname == NULL) {
		fmm_log(LOG_DEBUG, "did_update called - no table name");
		return;
	}
	if (cluster == NULL) {
		fmm_log(LOG_DEBUG, "did_update called - no cluster name");
		return;
	}

	fmm_log(LOG_DEBUG, "did_update called for table %s:%s, op %d",
	    cluster, tname, ccr_op);
	fmm_cfg_cluster_save_old();
	if (fmm_cfg_cluster_read() != 0) {
		fmm_log(LOG_WARNING, "did_update: error reading cluster info");
		return;
	}
	fmm_cfg_cluster_notify();
}

void fmm_cfg_callback_impl::did_update(const char *tname,
    ccr::ccr_update_type ccr_op, Environment &)
{
	if (tname == NULL) {
		fmm_log(LOG_DEBUG, "did_update called - no table name");
		return;
	}

	fmm_log(LOG_DEBUG, "did_update called for table %s, op %d",
	    tname, ccr_op);
	fmm_cfg_cluster_save_old();
	if (fmm_cfg_cluster_read() != 0) {
		fmm_log(LOG_WARNING, "did_update: error reading cluster info");
		return;
	}
	fmm_cfg_cluster_notify();
}

static int
fmm_cfg_cb_init(void)
{
	ccr::directory_var ccr_dir;
	ccr::readonly_table_ptr tabptr;
	Environment e;

	fmm_cfg_callback_impl *cb;
	ccr::callback_var cbptr;

	// look up ccr directory in name server
	naming::naming_context_var ctxp = ns::local_nameserver();
	CORBA::Object_var obj = ctxp->resolve("ccr_directory", e);
	if (e.exception()) {
		fmm_log(LOG_ERR,
		    "fmm_cfg_cb_init: ccr_directory not found in name server");
		return (-1);
	}
	ccr_dir = ccr::directory::_narrow(obj);

	// get reference to callback object
	cb = new fmm_cfg_callback_impl;
	cbptr = cb->get_objref();

	// lookup table "infrastructure"
	tabptr = ccr_dir->lookup(FMM_CFG_TABLE, e);
	if (e.exception()) {
		fmm_log(LOG_ERR,
		    "fmm_cfg_cb_init: table infrastructure not found");
		return (-1);
	}

	// register callback with infrastructure
	tabptr->register_callbacks(cbptr, e);
	if (e.exception()) {
		fmm_log(LOG_ERR, "fmm_cfg_cb_init: register_callbacks failed");
		return (-1);
	}

	CORBA::release(tabptr);
	return (0);
}


int
fmm_cfg_register(fmm_cfg_callback_t callback)
{
	int result = 0;
	unsigned int h = 0;

	if ((result = ORB::initialize()) != 0) {
		fmm_log(LOG_ERR, "fmm_cfg_register: unable to init ORB");
		return (result);
	}

	fmm_log(LOG_DEBUG, "fmm_cfg_register");
	(void) pthread_mutex_lock(&fmm_cfg_cluster.mutex);
	if (fmm_cfg_cluster.initialized) {
		fmm_log(LOG_ERR, "fmm_cfg_register: already initialized");
		(void) pthread_mutex_unlock(&fmm_cfg_cluster.mutex);
		return (-1);
	}

	for (h = 0; h < FMM_CFG_HASH_TABLE_SIZE; h++) {
		fmm_cfg_cluster.head[h] = NULL;
	}

	fmm_cfg_cluster.callback = callback;
	fmm_cfg_cluster.initialized = B_TRUE;
	(void) pthread_mutex_unlock(&fmm_cfg_cluster.mutex);

	result = fmm_cfg_cluster_init();
	if (result != 0) {
		fmm_log(LOG_ERR, "fmm_cfg_register: unable to init cluster");
		return (result);
	}
	result = fmm_cfg_cb_init();
	if (result != 0)
		fmm_log(LOG_ERR, "fmm_cfg_register: unable to initialize cb");

	return (result);
}
