/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FMM_CONFIGURATION_H
#define	_FMM_CONFIGURATION_H

#pragma ident	"@(#)fmm_cfg.h	1.3	08/05/20 SMI"

#include <scadmin/scconf.h>

/*
 * fmm_configuration.h
 */


#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
	FMM_CFG_NODE_ADDED = 1,
	FMM_CFG_NODE_REMOVED = 2,
	FMM_CFG_NODE_CHANGED = 3
} fmm_cfg_event_t;


typedef void (*fmm_cfg_callback_t)(fmm_cfg_event_t event, nodeid_t nodeid);

int fmm_cfg_register(fmm_cfg_callback_t callback);

#ifdef __cplusplus
}
#endif

#endif /* _FMM_CONFIGURATION_H */
