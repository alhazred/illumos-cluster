/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fmm_api_server.c	1.2	08/05/20 SMI"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <door.h>
#include <pthread.h>

#include <fmm/fmmutils.h>
#include <fmm/fmm_api_comm.h>

#include "fmm_api_server.h"
#include "fmm_api_jobs.h"
#include "fmm_timeout.h"

static int   doorHandler = -1;
static char *doorName = FMM_API_DOOR_NAME;

#define	DOOR_MODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH)


/*ARGSUSED*/
static void
fmm_api_door_dispatcher(void *cookie, char *dataptr, size_t datasize,
	door_desc_t *descptr, uint_t ndesc)
{
	fmm_api_args_t	*args;
	fmm_api_args_t	errArgs;
	SaErrorT	result;

	/*
	 * The server-side API functions are NOT cancellation points
	 * to avoid strange behaviors when a thread is cancelled and
	 * the locks are not released.
	 * Cancellations handlers could have been used, but this solution
	 * is simpler and the secondary impact is not important.
	 */
	(void) pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

	if (dataptr == NULL) {
		fmm_log(LOG_ERR, "fmm_api_door_dispatcher: invalid argument");
		errArgs.result = SA_ERR_LIBRARY;
		(void) door_return((char *)&errArgs,
		    sizeof (fmm_api_args_t), NULL, 0);
		return;
	}

	/* check argument size */
	if (datasize < sizeof (fmm_api_args_t)) {
		fmm_log(LOG_ERR,
		    "fmm_api_door_dispatcher: invalid argument size");
		errArgs.result = SA_ERR_LIBRARY;
		(void) door_return((char *)&errArgs,
		    sizeof (fmm_api_args_t), NULL, 0);
		return;
	}

	args = (fmm_api_args_t *)dataptr;

	switch (args->service) {
	case FMM_API_REGISTER:
		fmm_log(LOG_DEBUG, "fmm_api_door_dispatcher: FMM_API_REGISTER");
		result = fmm_api_register(args->in.fmm_register);
		break;
	case FMM_API_CONNECT:
		fmm_log(LOG_DEBUG, "fmm_api_door_dispatcher: FMM_API_CONNECT");
		result = fmm_api_connect(args->in.fmm_connect.id);
		break;
	case FMM_API_UNREGISTER:
		fmm_log(LOG_DEBUG,
		    "fmm_api_door_dispatcher: FMM_API_UNREGISTER");
		result = fmm_api_unregister(args->in.fmm_unregister.id);
		break;
	case FMM_API_NODE_GET:
		fmm_log(LOG_DEBUG, "fmm_api_door_dispatcher: FMM_API_NODE_GET");
		result = fmm_api_node_get(args->in.fmm_node_get,
		    &args->out.fmm_node_get.node);
		break;
	case FMM_API_ALL_NODES_GET:
		fmm_log(LOG_DEBUG,
		    "fmm_api_door_dispatcher: FMM_API_ALL_NODES_GET");
		result = fmm_api_all_nodes_get(args->in.fmm_all_nodes_get,
		    &args->out.fmm_all_nodes_get);
		break;
	default:
		fmm_log(LOG_ERR,
		    "fmm_api_door_dispatcher: invalid service request: %i",
		    args->service);
		result = SA_ERR_LIBRARY;
	}

	args->result = result;
	(void) door_return((char *)args, datasize, NULL, 0);
}

static fmm_error_t
fmm_api_door_server_start(void)
{
	int fd;

	if (doorHandler != -1)
		return (FMM_EINVAL);

	/* Create the entry point */
	(void) unlink(doorName);
	fd = open(doorName, O_CREAT | O_RDWR, DOOR_MODE);
	if (fd == -1) {
		fmm_log(LOG_ERR,
		    "fmm_api_door_server_start: failed to open door: %s",
		    strerror(errno)); /*lint !e746 */
		return (FMM_ENOTSUP);
	}
	(void) close(fd);

	/* Create the door */
	doorHandler = door_create(fmm_api_door_dispatcher, NULL, 0);
	if (doorHandler == -1) {
		fmm_log(LOG_ERR,
		    "fmm_api_door_server_start: failed to create door: %s",
		    strerror(errno));
		return (FMM_ENOTSUP);
	}
	if (fattach(doorHandler, doorName) != 0) {
		fmm_log(LOG_ERR,
		    "fmm_api_door_server_start: failed to attach door: %s",
		    strerror(errno));
		return (FMM_ENOTSUP);
	}

	fmm_log(LOG_DEBUG, "fmm_api_door_server_start: success");
	return (FMM_OK);
}


fmm_error_t
fmm_api_init(void)
{
	fmm_log(LOG_DEBUG, "fmm_api_init: start");

	/* Init and start API server */
	if (fmm_api_door_server_start() != FMM_OK) {
		fmm_log(LOG_ERR, "fmm_api_init: failed to start door server");
		return (FMM_ENOTSUP);
	}

	fmm_log(LOG_DEBUG, "fmm_api_init: success");
	return (FMM_OK);
}
