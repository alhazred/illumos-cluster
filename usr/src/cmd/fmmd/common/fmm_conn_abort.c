/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fmm_conn_abort.c	1.3	08/05/20 SMI"

#include <inet/tcp.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stropts.h>
#include <strings.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#include <fmm/fmmutils.h>

#include "fmm_conn_abort.h"

void
fmm_conn_abort(fmm_memberaddr_t addr)
{
	int s4 = -1, s6 = -1, s;
	struct strioctl iocb;
	tcp_ioc_abort_conn_t tcpac;
	struct sockaddr_in *sinp4 = (struct sockaddr_in *)&tcpac.ac_remote;
	struct sockaddr_in6 *sinp6 = (struct sockaddr_in6 *)&tcpac.ac_remote;

	/*
	 * Note that zeroing out the whole tcpac structure here means
	 * we dont have to worry about filling inaddrany for remote v4
	 * or v6 address because in solaris, both are a string of zeroes
	 */
	bzero(&tcpac, sizeof (tcpac));
	bzero(&iocb, sizeof (iocb));

	/* need to deal differently with v4 and v6 addresses */
	if (inet_pton(AF_INET, addr, &(sinp4->sin_addr)) == 1) {
		s4 = socket(AF_INET, SOCK_STREAM, 0);
		if (s4 < 0) {
			fmm_log(LOG_ERR,
			    "fmm_conn_abort: failed to create IPv4 socket: %s.",
			    strerror(errno)); /*lint !e746 */
			goto end;
		}

		/*
		 * Setup the TCP_IOC_ABORT_CONN ioctl connection filters
		 * First the local connection filter.
		 * If the local endpoint has the Logical IP,
		 * we need to terminate it, irrespective of whether
		 * it is an outbound or inbound connection.
		 */
		tcpac.ac_local.ss_family  = AF_INET;

		/* The remote connection filter. */
		tcpac.ac_remote.ss_family = AF_INET;

		/* ioctl should operate on s4 */
		s = s4;
	} else if (inet_pton(AF_INET6, addr, &(sinp6->sin6_addr))
	    == 1) {
		s6 = socket(AF_INET6, SOCK_STREAM, 0);
		if (s6 < 0) {
			fmm_log(LOG_ERR,
			    "fmm_conn_abort: failed to create IPv6 socket: %s.",
			    strerror(errno)); /*lint !e746 */
			goto end;
		}

		/*
		 * Setup the TCP_IOC_ABORT_CONN ioctl connection filters
		 * First the local connection filter.
		 * If the local endpoint has the Logical IP,
		 * we need to terminate it, irrespective of whether
		 * it is an outbound or inbound connection.
		 */
		tcpac.ac_local.ss_family = AF_INET6;

		/* The remote connection filter. */
		tcpac.ac_remote.ss_family = AF_INET6;

		/* ioctl should operate on s6 */
		s = s6;
	} else {
		fmm_log(LOG_ERR, "fmm_conn_abort: not a valid IP address %s",
		    addr);
		goto end;
	}

	/* TCP connection state filter. */
	tcpac.ac_start	= TCPS_SYN_SENT;
	tcpac.ac_end	= TCPS_TIME_WAIT;

	/* Prepare the ioctl. */
	iocb.ic_cmd	= TCP_IOC_ABORT_CONN;
	iocb.ic_timout	= 0;
	iocb.ic_len	= (int)sizeof (tcp_ioc_abort_conn_t);
	iocb.ic_dp	= (char *)&tcpac;

	/* Send the ioctl. */
	if (ioctl(s, I_STR, &iocb) != 0) {
		/*
		 * TCP_IOC_ABORT_CONN fails with ENOENT if there are
		 * no connections to be reset. Hence making this a
		 * debug message.
		 */
		if (errno == ENOENT)
			fmm_log(LOG_DEBUG, "fmm_conn_abort: "
			    "TCP_IOC_ABORT_CONN operation failed : %s",
			    strerror(errno));	/*lint !e746 */
		else
			fmm_log(LOG_ERR, "fmm_conn_abort: "
			    "TCP_IOC_ABORT_CONN operation failed : %s",
			    strerror(errno));
	}

end:
	/* close the sockets */
	if (s4 >= 0)
		(void) close(s4);
	if (s6 >= 0)
		(void) close(s6);
}
