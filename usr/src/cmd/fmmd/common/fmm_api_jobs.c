/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fmm_api_jobs.c 1.3	08/05/20 SMI"

#include <string.h>
#include <strings.h>

#include <fmm/saf_clm.h>
#include <fmm/fmm_api_comm.h>
#include <fmm/fmmutils.h>

#include "fmm_notification_server.h"
#include "fmm_cluster.h"
#include "fmm_timeout.h"

#define	FMM_API_RELEASE_CODE 'A'
#define	FMM_API_MAJOR 0x01
/* #define	FMM_API_MINOR 0x02 */



SaErrorT
fmm_api_register(fmm_api_register_args_t args)
{
	if ((args.version.releaseCode != FMM_API_RELEASE_CODE) ||
	    (args.version.major != FMM_API_MAJOR)) {
		fmm_log(LOG_ERR, "fmm_api_register: version mismatch");
		return (SA_ERR_VERSION);
	}

	if (fmm_fifo_create(args.id) != FMM_OK) {
		fmm_log(LOG_ERR,
		    "fmm_api_register: failed to create the fifo %d", args.id);
		return (SA_ERR_LIBRARY);
	}

	fmm_log(LOG_DEBUG, "fmm_api_register: success");
	return (SA_OK);
}

SaErrorT
fmm_api_connect(int pid)
{
	if (fmm_fifo_open_for_write(pid) == -1) {
		fmm_log(LOG_ERR, "fmm_api_connect: failed to open for write %d",
		    pid);
		return (SA_ERR_LIBRARY);
	}

	fmm_log(LOG_DEBUG, "fmm_api_connect: success");
	return (SA_OK);
}

SaErrorT
fmm_api_unregister(int pid)
{
	if (fmm_fifo_destroy(pid) != B_TRUE) {
		fmm_log(LOG_ERR, "fmm_api_unregister: failed to destroy %d",
		    pid);
		return (SA_ERR_LIBRARY);
	}

	fmm_log(LOG_DEBUG, "fmm_api_unregister: success");
	return (SA_OK);
}

static void
fmm2api_node(fmm_node_config_t cluster_node, SaClmClusterNodeT *api_node)
{
	if (api_node == NULL)
		return;

	api_node->nodeId = cluster_node.nodeid;
	api_node->nodeAddress.length =
	    strlen(cluster_node.addr) + 1; /*lint !e734 */
	(void) memcpy(api_node->nodeAddress.value, cluster_node.addr,
	    api_node->nodeAddress.length);
	api_node->nodeName.length =
	    strlen(cluster_node.name) + 1; /*lint !e734 */
	(void) memcpy(api_node->nodeName.value, cluster_node.name,
	    api_node->nodeName.length);
	api_node->clusterName.length =
	    strlen(cluster_node.clusterName) + 1; /*lint !e734 */
	(void) memcpy(api_node->clusterName.value, cluster_node.clusterName,
	    api_node->clusterName.length);
	api_node->member = (cluster_node.role == MEMBERSHIP_IN_CLUSTER_ROLE) ?
	    SA_TRUE : SA_FALSE;
	api_node->bootTimestamp = cluster_node.incarnation_number;
	api_node->initialViewNumber = cluster_node.initialviewnumber;
}

SaErrorT
fmm_api_node_get(fmm_api_node_get_args_t args, SaClmClusterNodeT *node)
{
	fmm_node_config_t *cluster_node;

	if (node == NULL)
		return (SA_ERR_LIBRARY);

	if (fmm_cluster_lock(args.timeout) != FMM_OK) {
		fmm_log(LOG_ERR,
		    "fmm_api_node_get: timeout trying to grab the lock");
		fmm_cluster_unlock();
		return (SA_ERR_TIMEOUT);
	}

	cluster_node = fmm_node_get(args.nodeId);
	fmm_cluster_unlock();

	if (cluster_node == NULL) {
		fmm_log(LOG_ERR, "fmm_api_node_get: node not found");
		return (SA_ERR_INVALID_PARAM);
	}

	fmm2api_node(*cluster_node, node);

	if (fmm_check_timeout(args.timeout) == FMM_ETIMEDOUT) {
		fmm_log(LOG_ERR, "fmm_api_node_get: timeout");
		return (SA_ERR_TIMEOUT);
	}

	fmm_log(LOG_DEBUG, "fmm_api_node_get: success");
	return (SA_OK);
}


SaErrorT
fmm_api_all_nodes_get(fmm_api_all_nodes_get_args_t args,
	fmm_api_all_nodes_get_res_t *res)
{
	fmm_node_config_t *node;
	unsigned int ii = 0;

	if (res == NULL) {
		fmm_log(LOG_ERR, "fmm_api_all_nodes_get: invalid param");
		return (SA_ERR_LIBRARY);
	}

	if (args.numberOfItems < fmm_cluster_nodes_number()) {
		res->numberOfItems = 0;
		fmm_log(LOG_ERR, "fmm_api_all_nodes_get: table too small");
		return (SA_ERR_NO_SPACE);
	}

	bzero(res->notificationBuffer,
	    sizeof (SaClmClusterNotificationT) * args.numberOfItems);

	(void) fmm_cluster_lock(FMM_NO_TIMEOUT);
	for (node = fmm_first_node_get(); node != NULL;
	    node = fmm_next_node_get(node)) {
		fmm2api_node(*node,
		    &(res->notificationBuffer[ii].clusterNode));
		res->notificationBuffer[ii].clusterChanges
		    = SA_CLM_NODE_NO_CHANGE;
		ii ++;
	}
	res->numberOfItems = ii;
	res->viewNumber = fmm_cluster_viewnumber_get();
	fmm_cluster_unlock();
	fmm_log(LOG_DEBUG, "fmm_api_all_nodes_get: success");
	return (SA_OK);
}
