/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fmm_notification_server.c 1.2	08/05/20 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <limits.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <assert.h>

#include <fmm/fmmutils.h>
#include <fmm/fmm_api_comm.h>
#include "fmm_notification_server.h"

/*
 * We must handle a list of queues because, when a notification occurs, we have
 * to send a message to all of them. This structure stores for each element:
 * o the key given by the client to create the queue (to avoid double
 *   registration)
 * o the id of the created queue at registration
 *
 * This structure provides a way to check double registration, bad
 * unregistration ....
 */

typedef struct fmm_msq_list_t {
	struct fmm_msq_list_t	*next;
	struct fmm_msq_list_t	*previous;
	int			fifoid;
	int			uniqueid;
	boolean_t		erroneous; /* to close and forget */
	char			key[PATH_MAX];
} fmm_msq_list_t;


static struct {
	/* Mutex associated with the previous structure */
	pthread_mutex_t mutex;
	/* Head of the list of registered queue id */
	fmm_msq_list_t	*head;

	/*
	 * This boolean indicates if a call-back has been registrated to be
	 * called at server exit. This call-back is used to close all message
	 * queues in order to warn every client of the safclmapi that the server
	 * is down
	 */
	boolean_t	atexit_cb_registrated;
	void		(*old_sig_handler)(int);
} fmm_clients = {
	PTHREAD_MUTEX_INITIALIZER, /*lint !e708 */
	NULL,
	B_FALSE,
	NULL};

/*
 * The maximum number of notification clients.
 * This number is calculated to leave at least 30 file descriptors
 * free to be used by fmmd for admin purposes. This number has been
 * choosen after some tests.
 */
#define	FMM_MAX_NOTIF_CLIENTS ((unsigned long) (sysconf(_SC_OPEN_MAX) - 30))

/* Notif client counter to limit the number of clients */
static unsigned long   fmm_numberOfClients = 0;
static pthread_mutex_t fmm_numberOfClientsMutex
	= PTHREAD_MUTEX_INITIALIZER; /*lint !e708 */



/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * [INTERNAL] At exit function
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * This function is called at exit to make cleaning work: Queue cleaning,
 * queue closing (note that Closing these Message queues is trapped by
 * clients)
 *
 * Param:
 *   N/A
 *
 * result:
 *   N/A
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static void
fmm_close_all_atexit()
{
	fmm_msq_list_t *oneQueue;
	fmm_msq_list_t *nextQueue;
	/* NO MUTEX_LOCK - LOCK IS IN CALLED FUNCTION */

	oneQueue = fmm_clients.head;

	while (oneQueue != NULL) {
		nextQueue = oneQueue->next;
		(void) fmm_fifo_destroy(oneQueue->uniqueid);
		oneQueue = nextQueue;
	}
	fmm_log(LOG_DEBUG, "fmm_close_all_atexit: success");
}


/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * [INTERNAL] close erroneous FIFO
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * This function is called to close erroneous FIFO. This avoid wasting time
 * with them
 *
 * Param:
 *   N/A
 *
 * result:
 *   N/A
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static void
fmm_fifo_close_erroneous()
{
	fmm_msq_list_t	*oneQueue;
	boolean_t	found;

	found = B_TRUE;
	while (found == B_TRUE) {
		found = B_FALSE;
		(void) pthread_mutex_lock(&fmm_clients.mutex);
		oneQueue = fmm_clients.head;
		while (oneQueue != NULL) {
			if (oneQueue->erroneous == B_TRUE) {
			    found = B_TRUE;
			    break;
			}
			oneQueue = oneQueue->next;
		}
		(void) pthread_mutex_unlock(&fmm_clients.mutex);
		if (found == B_TRUE)
			(void) fmm_fifo_destroy(oneQueue->uniqueid);
	}
	fmm_log(LOG_DEBUG, "fmm_fifo_close_erroneous: success");
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*
 * [INTERNAL] thread in charge of closing erroneous pipes
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * The thread is created in the first fmm_fifo_create(...)
 * Every two minutes it will check for the existance of an erroneous pipe
 * due to an abnormal process termination with open pipe.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static pthread_t threadCloseErroneousId = (pthread_t)-1;

static void
*threadCloseErroneous(void *param)
{
	boolean_t atLeastOne;
	fmm_msq_list_t *queue;
	const struct timespec rqtp = {60, 0};

	param = (void *)1;
	while (param) {
		atLeastOne = B_FALSE;
		(void) nanosleep(&rqtp, NULL);
		(void) pthread_mutex_lock(&fmm_clients.mutex);
		for (queue = fmm_clients.head; queue; queue = queue->next) {
			if (queue->erroneous == B_TRUE) {
				atLeastOne = B_TRUE;
				continue;
			}
			if (kill(queue->uniqueid, 0) == -1)
				if (errno == ESRCH) { /*lint !e746 */
					atLeastOne = B_TRUE;
					queue->erroneous = B_TRUE;
					fmm_log(LOG_DEBUG,
					    "threadCloseErroneous: No process "
					    "is using pipe %d",
					    queue->uniqueid);
				}
		}
		(void) pthread_mutex_unlock(&fmm_clients.mutex);
		if (atLeastOne == B_TRUE)
			fmm_fifo_close_erroneous();
	}
	return (NULL);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * [INTERNAL] signal handling function
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * This function handles message interuption. it calls the cleaning function
 * before exiting
 *
 * Param:
 *   o [IN] received signal
 *
 * result:
 *   N/A
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static void
sig_func(int sig)
{
	fmm_log(LOG_DEBUG, "sig_func");
	fmm_close_all_atexit();
	if ((fmm_clients.atexit_cb_registrated == B_TRUE) &&
	    (fmm_clients.old_sig_handler != NULL))
		fmm_clients.old_sig_handler(sig);
	else
		exit(EXIT_FAILURE);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * [INTERNAL] register function for clean exit
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * This function register at_exit and signal handling function
 *
 * Param:
 *   o N/A
 *
 * result:
 *   B_TRUE on success, B_FALSE otherwise
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static boolean_t
register_atexit()
{
	int result = B_TRUE;

	if (fmm_clients.atexit_cb_registrated == B_FALSE) {
		(void) pthread_mutex_lock(&fmm_clients.mutex);
		fmm_log(LOG_DEBUG, "register_atexit");
		if (atexit(fmm_close_all_atexit) != 0) {
			fmm_log(LOG_ERR,
			    "register_atexit: registration failed");
			result = B_FALSE;
		} else {
			/* Check If I may trap this signal */
			fmm_clients.old_sig_handler = signal(SIGINT, sig_func);
			fmm_clients.atexit_cb_registrated = B_TRUE;
		}
		(void) pthread_mutex_unlock(&fmm_clients.mutex);
	}
	fmm_log(LOG_DEBUG, "register_atexit: %s", (result)?"success":"failure");
	return (result);
}


/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * [INTERNAL] Make the general initialization
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static fmm_error_t
fmm_fifo_generalInit()
{

	if (register_atexit() == B_FALSE)
		return (FMM_ENOTSUP);

	return (FMM_OK);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * [INTERNAL] Check if a queue is already registrated (according to its key)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Check if a queue already exists. (according to a key) . If yes
 * return the message id
 *
 * Param:
 *   o [IN] key to check (given by client as a unique id)
 *
 * result:
 *   a message queue or FMM_INVALID_FIFOID if this key is not registered
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static int
fmm_fifo_id_get(char *key)
{
	int result = -1; /* suppose failure */
	fmm_msq_list_t *one_queue;

	/* check if the fifoid is not already stored as a notification list */
	(void) pthread_mutex_lock(&fmm_clients.mutex);
	for (one_queue = fmm_clients.head; one_queue != NULL;
	    one_queue = one_queue->next) {
		if (strcmp(one_queue->key, key) == 0) {
			result = one_queue->fifoid;
			break;
		}
	}
	(void) pthread_mutex_unlock(&fmm_clients.mutex);
	return (result);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * [INTERNAL]
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * Param:
 *   o [IN] key to check (given by client as a unique id)
 *
 * result:
 *   queue information
 * WARNING :
 *   MUST BE CALLED UNDER LOCK
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static fmm_msq_list_t *
fmm_fifo_get(char *key)
{
	fmm_msq_list_t *one_queue;

	if (key == NULL)
		return (NULL);

	/* check if the fifoid is not already stored as a notification list */

	for (one_queue = fmm_clients.head; one_queue != NULL;
	    one_queue = one_queue->next) {
		fmm_log(LOG_DEBUG,
		    "fmm_fifo_get: found fifo id: %d , key : %s, erroneous: %d",
		    one_queue->fifoid, one_queue->key, one_queue->erroneous);
		if (strcmp(one_queue->key, key) == 0)
			break;
	}

	return (one_queue);
}


/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*
 * [INTERNAL] remember a new queue
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Insert a new queue in the list of queues to notify.
 *
 * Param:
 *   o [IN] key of the queue to store (given by client at registration)
 *   o [IN] id of the queue to store (returned to client at registration)
 *
 * result:
 *   B_TRUE on success, B_FALSE otherwise
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
static boolean_t
fmm_fifo_remember(char *key,
    int uniqueId,
    int const fifoid)
{
	boolean_t queue_locked = B_FALSE;
	boolean_t result = B_FALSE; /* suppose failure */
	fmm_msq_list_t *new_elem;
	fmm_msq_list_t *one_queue;

	if (key == NULL)
		goto end;
	if (fifoid == -1)
		goto end;
	if (fmm_fifo_generalInit() != FMM_OK)
		goto end;

	(void) pthread_mutex_lock(&fmm_clients.mutex);
	queue_locked = B_TRUE;

	/* check if the fifoid is not already stored as a notification list */
	for (one_queue = fmm_clients.head; one_queue != NULL;
	    one_queue = one_queue->next) {
		if (strcmp(one_queue->key, key) == 0) {
			result = B_TRUE;
			fmm_log(LOG_WARNING,
			    "fmm_fifo_remember: %s already stored => ignored",
			    key);
			goto end;
		}
	}

	new_elem = (fmm_msq_list_t *)malloc(sizeof (fmm_msq_list_t));
	if (new_elem == NULL)
		goto end;

	new_elem->fifoid = fifoid;
	new_elem->uniqueid = uniqueId;

	/* problem when reuse a connection that was erroneous */
	new_elem->erroneous = B_FALSE;
	(void) strcpy(new_elem->key, key);

	new_elem->previous = NULL;
	new_elem->next = fmm_clients.head;
	if (fmm_clients.head != NULL)
		fmm_clients.head->previous = new_elem;
	fmm_clients.head = new_elem;

	(void) pthread_mutex_unlock(&fmm_clients.mutex);
	queue_locked = B_FALSE;

	result = B_TRUE;
end:
	if (queue_locked == B_TRUE)
		(void) pthread_mutex_unlock(&fmm_clients.mutex);

	fmm_log(LOG_DEBUG, "fmm_fifo_remember: %d (key=%s) => %s",
	    fifoid, key, (result == B_TRUE)?"OK":"KO");
	return (result);
}

/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*
 * [EXPORTED] Destroy all information relative to a client
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * This function ends the relationship between a client and the CMM.
 *
 * Param:
 *   o [IN] id of the queue that will transmit the message
 *   o [IN] is a notification necessary or the FIFO is already DOWN
 *
 * result:
 *   B_TRUE on success, B_FALSE otherwise (i.e. queue was not registered)
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
boolean_t
fmm_fifo_destroy(int uniqueId)
{
	char key[PATH_MAX];
	fmm_msq_list_t *queue;

	/* CREATE A CORRECT TEMP NAME FOR FIFO */
	(void) sprintf(key, "%s/%s%d", FMM_TMP_DIR, FMM_KEY_PREFIX,
	    uniqueId);

	fmm_log(LOG_DEBUG, "fmm_fifo_destroy: destruction of queue %s", key);

	(void) pthread_mutex_lock(&fmm_clients.mutex);

	queue = fmm_fifo_get(key);
	if (queue == NULL) {
		fmm_log(LOG_DEBUG,
		    "fmm_fifo_destroy: unknown fifo %s -> abort destroy", key);
		(void) pthread_mutex_unlock(&fmm_clients.mutex);
		return (B_FALSE);
	}
	fmm_log(LOG_DEBUG,
	    "fmm_fifo_destroy: destruction of queue %d", queue->fifoid);

	(void) close(queue->fifoid);
	(void) unlink(key);

	/*
	 * remove the element from the list
	 */
	/* has it next nodes */
	if (queue->next != NULL)
		queue->next->previous = queue->previous;

	/* has it previous nodes */
	if (queue->previous != NULL)
		queue->previous->next = queue->next;
	else
		/*
		 * If first elem, fmm_clients.head point on it...refresh the
		 * fmm_clients.head
		 */
		fmm_clients.head = queue->next;

	free(queue);

	(void) pthread_mutex_unlock(&fmm_clients.mutex);

	/* Decrement the notif clients counter */
	if (pthread_mutex_lock(&fmm_numberOfClientsMutex) == FMM_OK) {
		assert(fmm_numberOfClients > 0);
		fmm_numberOfClients--;
		(void) pthread_mutex_unlock(&fmm_numberOfClientsMutex);
	}

	fmm_log(LOG_DEBUG, "fmm_fifo_destroy: success");

	return (B_TRUE);
}


/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*
 * [EXPORTED] Create a queue for a client
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * This function create a queue that will be used to notify one client. The
 * client must give an id that uniquely identifies it on the node (process id
 * is a good candidate for this information). The created queue is associated
 * with an id that identifies it on the server side. This id is returned to
 * the client, because it must use it for deconnection.
 *
 * Param:
 *   o [IN] an id that uniquely identifies the client on the node
 *
 * result:
 *   the id of the created queue on success,
 *   FMM_INVALID_fifoid otherwise (i.e. queue was not registered)
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

fmm_error_t
fmm_fifo_create(int uniqueId)
{
	char		key[PATH_MAX];
	fmm_error_t	result;
	mode_t		oldMask;
	boolean_t	locked = B_FALSE;
	pthread_attr_t	attr;

	(void) pthread_attr_init(&attr);
	(void) pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);
	(void) pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);

	if (threadCloseErroneousId == (pthread_t)-1)
		if (pthread_create(&threadCloseErroneousId, &attr,
		    threadCloseErroneous, NULL) != 0)
			threadCloseErroneousId = (pthread_t)-1;
	(void) pthread_attr_destroy(&attr);

	/* CREATE A CORRECT TEMP NAME FOR FIFO */
	(void) sprintf(key, "%s/%s%d", FMM_TMP_DIR, FMM_KEY_PREFIX, uniqueId);
	fmm_log(LOG_DEBUG, "fmm_fifo_create: creating fifo %s", key);

	oldMask = umask(0);

	result = pthread_mutex_lock(&fmm_numberOfClientsMutex);
	if (result != 0)
		goto end;
	locked = B_TRUE;

retry:
	if (fmm_numberOfClients >= FMM_MAX_NOTIF_CLIENTS) {
		fmm_log(LOG_DEBUG,
		    "fmm_fifo_create: too many notification clients (%lu/%lu)",
		    fmm_numberOfClients, FMM_MAX_NOTIF_CLIENTS);
		result = FMM_ENOENT;
		goto end;
	}

	if (mkfifo(key, (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1) {
		if (errno == EAGAIN) {
			fmm_log(LOG_DEBUG,
			    "fmm_fifo_create: mkfifo interrupted -> retry");
				goto retry;
		}

		if (errno != EEXIST) {
			fmm_log(LOG_ERR,
			    "fmm_fifo_create: mkfifo failed (error=%s)",
			    strerror(errno));
			result = FMM_ENOTSUP;
			goto end;
		}
	}
	fmm_numberOfClients++;

	(void) pthread_mutex_unlock(&fmm_numberOfClientsMutex);
	locked = B_FALSE;

	fmm_log(LOG_DEBUG, "fmm_fifo_create: mkfifo done");
	/* Let's check what we really know about it */
	if (fmm_fifo_id_get(key) != -1) {
		fmm_log(LOG_WARNING,
		    "fmm_fifo_create: %s queue already exists -> use it",
		    key);
	}
	result = FMM_OK;

end:
	if (locked)
		(void) pthread_mutex_unlock(&fmm_numberOfClientsMutex);

	(void) umask(oldMask);
	fmm_log(LOG_DEBUG,
	    "fmm_fifo_create: creation of queue %s ..FINISHED", key);

	return (result);
}

int
fmm_fifo_open_for_write(int const uniqueId)
{
	char	key[PATH_MAX];
	int	result;
	int	subcallResult;
	mode_t	oldMask;

	/* CREATE A CORRECT TEMP NAME FOR FIFO */
	(void) sprintf(key, "%s/%s%d", FMM_TMP_DIR, FMM_KEY_PREFIX,
	    uniqueId);

	oldMask = umask(0);
	subcallResult = open(key, O_WRONLY | O_NONBLOCK);
	(void) umask(oldMask);

	/* see ENXIO */
	if (subcallResult == -1) {
		fmm_log(LOG_ERR,
		    "fmm_fifo_open_for_write: open failed (error=%s)",
		    strerror(errno));
		result = -1;
		(void) remove(key);
		goto end;
	}

	result = subcallResult;

	/* Operation succeeds, let's remember the queue info */
	if (fmm_fifo_remember(key, uniqueId, result) == B_FALSE) {
		fmm_log(LOG_ERR,
		    "fmm_fifo_open_for_write: fmm_fifo_remember failed");
		result = -1;
	}

end:
	fmm_log(LOG_DEBUG, "fmm_fifo_open_for_write: success");
	return (result);
}


void
fmm_fifo_notify_change(fmm_msq_list_t *queue, unsigned long viewnumber)
{
	fmm_api_notification_message_t message;
	message.viewnumber = viewnumber;

	if (queue == NULL)
		return;

	if (queue->erroneous) {
		fmm_log(LOG_DEBUG, "fmm_fifo_notify_change: skip %s",
		    queue->key);
		return;
	}

	if (write(queue->fifoid, &message,
	    sizeof (fmm_api_notification_message_t))
	    != (ssize_t)sizeof (fmm_api_notification_message_t)) {
		if (errno == EPIPE)
			queue->erroneous = B_TRUE;
		fmm_log(LOG_ERR, "fmm_fifo_notify_change: write %s failed",
		    queue->key);
	} else
		fmm_log(LOG_DEBUG,
		    "fmm_fifo_notify_change: write %s ok", queue->key);
}


void
fmm_notify_api_clients(unsigned long viewnumber)
{
	fmm_msq_list_t	*one_queue;

	fmm_log(LOG_DEBUG, "fmm_notify_api_clients");
	(void) pthread_mutex_lock(&fmm_clients.mutex);
	for (one_queue = fmm_clients.head; one_queue != NULL;
	    one_queue = one_queue->next)
		fmm_fifo_notify_change(one_queue, viewnumber);

	fmm_log(LOG_DEBUG, "fmm_notify_api_clients done");
	(void) pthread_mutex_unlock(&fmm_clients.mutex);
	fmm_fifo_close_erroneous();
}
