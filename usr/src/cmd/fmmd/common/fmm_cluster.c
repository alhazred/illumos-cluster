/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fmm_cluster.c	1.3	08/05/20 SMI"

#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <errno.h>
#include <fmm/fmmutils.h>
#include <fmm/hbr.h>

#include "fmm_cluster.h"
#include "fmm_events.h"
#include "fmm_conn_abort.h"
#include "fmm_notification_server.h"


#define	HASH_TABLE_SIZE 256

/* List element */
typedef struct fmm_member_node {
	fmm_node_config_t	*info;
	struct fmm_member_node	*previous;
	struct fmm_member_node	*next;
	boolean_t		use_old;
	fmm_node_config_t	*old;
} fmm_member_node_t;

typedef fmm_member_node_t *fmm_hash_list_t;

/* List head */
typedef struct {
	unsigned int		len;
	fmm_hash_list_t		list[HASH_TABLE_SIZE];
	unsigned long		viewnumber;
} fmm_cluster_description_t;

/*
 * Cluster nodes list
 */
static fmm_cluster_description_t nodes_list;

#define	NO_OWNER ((pthread_t)-1)

/* Shared data mutex */
static pthread_mutex_t cluster_mutex
	= PTHREAD_MUTEX_INITIALIZER; /*lint !e708 */
static pthread_t	cluster_mutex_owner = NO_OWNER;



static unsigned int
hash(fmm_nodeid_t P_nodeid)
{
	unsigned int hash_index;
	hash_index = P_nodeid % HASH_TABLE_SIZE;
	return (hash_index);
}


static void
fmm_lock_check(char *caller)
{
	pthread_t current_thread;

	current_thread = pthread_self();
	if (cluster_mutex_owner == NO_OWNER) {
		fmm_log(LOG_CRIT, "fmm_lock_check: thread [%d] calls '%s' "
		    "without locking configuration", current_thread, caller);
		exit(EXIT_FAILURE);
	}
}

void
fmm_cluster_init(void)
{
	unsigned int i;
	for (i = 0; i < HASH_TABLE_SIZE; i++)
		nodes_list.list[i] = NULL;

	nodes_list.len = 0;
	nodes_list.viewnumber = 0;
}


fmm_error_t
fmm_cluster_lock(fmm_timeout_t timeout)
{
	int		lock_result;
	fmm_error_t	result = FMM_OK;

	lock_result = pthread_mutex_trylock(&cluster_mutex);

	if (lock_result != 0) {
		if (lock_result == EBUSY)
			if (pthread_self() == cluster_mutex_owner) {
				fmm_log(LOG_CRIT,
				    "fmm_cluster_lock: deadlock detected");
				assert(0 == -1);
			}
		fmm_log(LOG_DEBUG, "fmm_cluster_lock: lock wanted by %d",
		    pthread_self());
		result = fmm_pthread_mutex_timedlock(&cluster_mutex, timeout);
	}

	switch (result) {
	case FMM_OK:
		cluster_mutex_owner = pthread_self();
		fmm_log(LOG_DEBUG, "fmm_cluster_lock: lock owner is %d",
		    cluster_mutex_owner);
		break;
	case FMM_ETIMEDOUT:
		fmm_log(LOG_DEBUG, "fmm_cluster_lock: lock timed-out for %d",
		    cluster_mutex_owner);
		break;
	default: /* paranoia: this should never happen */
		fmm_log(LOG_DEBUG, "fmm_cluster_lock: lock failed for %d",
		    cluster_mutex_owner);
		break;
	} /*lint !e788 */


	return (result);
}


void
fmm_cluster_unlock(void)
{
	if (cluster_mutex_owner != pthread_self()) {
		fmm_log(LOG_CRIT, "fmm_cluster_unlock: thread %d tries to "
		    "unlock whereas %d is owner",
		    pthread_self(), cluster_mutex_owner);
		assert(0 == -1);
	}

	fmm_log(LOG_DEBUG, "fmm_cluster_unlock: %d free lock",
	    cluster_mutex_owner);
	cluster_mutex_owner = NO_OWNER;
	(void) pthread_mutex_unlock(&cluster_mutex);
	/* no retry needed theorically */
}


/*
 * Cluster configuration list processing
 */

static fmm_member_node_t
*list_element_get(fmm_nodeid_t const nodeId)
{
	fmm_member_node_t *list = nodes_list.list[hash(nodeId)];
	boolean_t found = B_FALSE;

	while ((found == B_FALSE) && (list)) {
		if (list->info->nodeid == nodeId) {
			found = B_TRUE;
			break;
		}
		list = list->next;
	}

	return ((found)?list:NULL);
}

static void
list_element_insert(fmm_cluster_description_t *list,
    fmm_member_node_t *node, fmm_nodeid_t nodeid)
{
	unsigned int index = hash(nodeid);

	if ((list == NULL) || (node == NULL))
		return;

	/* Check valid node */
	if ((node->info == NULL) || (node->old == NULL) ||
	    (node->info->nodeid != node->old->nodeid)) {
		fmm_log(LOG_ERR, "list_element_insert: invalid node");
		return;
	}
	node->previous = NULL;
	node->next = list->list[index];
	if (list->list[index])
		list->list[index]->previous = node;
	list->list[index] = node;
	list->len++;
}

/*
 * Remove a list element without memory deallocation (to transfer it in another
 * list, for instance)
 */
static void
list_element_remove(fmm_cluster_description_t *list,
    fmm_member_node_t *node)
{
	unsigned int index;

	if ((node == NULL) || (list == NULL))
		return;

	index = hash(node->info->nodeid);
	if (node == list->list[index]) { /*  head of list */
		if (node->next != NULL) {  /* not end of list */
			node->next->previous = NULL;
		}
		list->list[index] = node->next;
	} else { /* node head of list */
		if (node->next != NULL)
			node->next->previous = node->previous;
		/* I know it's not the head, thus, there is a previous node */
		node->previous->next = node->next;
	}
	node->next = node->previous = NULL;
	list->len--;
	fmm_log(LOG_DEBUG, "list_element_remove: node removed");
}

static fmm_member_node_t
*list_element_create(void)
{
	return (malloc(sizeof (fmm_member_node_t)));
}


static void
fmm_viewnumber_increment(void)
{
	nodes_list.viewnumber++;
}


static fmm_node_config_t
*info_element_create(void)
{
	return (malloc(sizeof (fmm_node_config_t)));
}

unsigned long
fmm_cluster_nodes_number(void)
{
	return (nodes_list.len);
}


unsigned long
fmm_cluster_viewnumber_get(void)
{
	return (nodes_list.viewnumber);
}


fmm_node_config_t
*fmm_node_get(fmm_nodeid_t const nodeId)
{
	fmm_member_node_t *list_element = NULL;
	fmm_node_config_t *node = NULL;

	fmm_lock_check("fmm_node_get");

	if ((list_element = list_element_get(nodeId)) != NULL)
		node = list_element->info;

	return (node);
}


fmm_node_config_t
*fmm_first_node_get(void)
{
	fmm_member_node_t *list = NULL;
	fmm_node_config_t *node = NULL;
	unsigned int index = 0;

	fmm_lock_check("fmm_first_node_get");

	for (index = 0; index < HASH_TABLE_SIZE; index++) {
		list = nodes_list.list[index];
		if (list != NULL)
			break;
	}

	if (list != NULL)
		node = list->info;

	return (node);
}

fmm_node_config_t
*fmm_next_node_get(fmm_node_config_t const *node)
{
	fmm_member_node_t *list = NULL;
	fmm_node_config_t *nextNode = NULL;

	if (node == NULL)
		return (NULL);

	fmm_lock_check("fmm_next_node_get");

	if ((list = list_element_get(node->nodeid)) != NULL) {
		list = list->next;
		if (list)
			nextNode = list->info;
		else {
			unsigned int index = hash(node->nodeid);
			unsigned int i;

			for (i = index + 1; i < HASH_TABLE_SIZE; i++) {
				if (nodes_list.list[i] != NULL) {
					nextNode = nodes_list.list[i]->info;
					break;
				}
			}
		}
	}

	return (nextNode);
}

fmm_node_config_t
*fmm_node_create(fmm_nodeid_t const P_nodeid)
{
	fmm_node_config_t *node = NULL, *old = NULL;
	fmm_member_node_t *list_element = NULL;

	fmm_lock_check("fmm_node_create");

	/*
	 * No presence checking is done, TBD by the caller
	 */
	list_element = list_element_create();

	if (list_element != NULL) {
		node = info_element_create();
		old = info_element_create();

		if ((node == NULL) || (old == NULL)) {
			free(list_element);
			if (node != NULL) {
				free(node);
				node = NULL;
			}
			if (old != NULL)
				free(old);
		} else {
			list_element->info = node;
			list_element->old = old;

			/* Set list management flags */
			list_element->use_old = B_FALSE;

			/* Initialize node with default value */
			/* Reset contents of node description */
			(void) memset(node, 0, sizeof (fmm_node_config_t));
			node->role = MEMBERSHIP_OUT_OF_CLUSTER_ROLE;
			node->responding = B_FALSE;
			node->monitoringState = B_TRUE;
			node->nodeid = P_nodeid;

			(void) memset(old, 0, sizeof (fmm_node_config_t));
			old->role = MEMBERSHIP_OUT_OF_CLUSTER_ROLE;
			old->responding = B_FALSE;
			old->monitoringState = B_TRUE;
			old->nodeid = P_nodeid;

			list_element_insert(&nodes_list, list_element,
			    P_nodeid);
		}
	}

	return (node);
}

void
fmm_node_remove(fmm_nodeid_t const nodeId)
{
	fmm_member_node_t *list_element = NULL;

	fmm_lock_check("fmm_node_remove");

	if ((list_element = list_element_get(nodeId)) != NULL) {
		if (list_element->info->role == MEMBERSHIP_IN_CLUSTER_ROLE)
			fmm_log(LOG_WARNING, "fmm_node_remove: "
			    "%d removed while still in the cluster", nodeId);

		list_element_remove(&nodes_list, list_element);

		/* Also remove the node from the HB driver */
		(void) hbr_delete_node(nodeId);

		fmm_log(LOG_DEBUG,
		    "fmm_node_remove: ****** Node [%d] ('%s') removed from "
		    "cluster configuration",
		    list_element->info->nodeid,
		    list_element->info->name);
		free(list_element->info);
		free(list_element->old);
		free(list_element);
	}
}

fmm_error_t
fmm_node_duplicate(fmm_nodeid_t const nodeId, fmm_node_config_t *newNode)
{
	fmm_node_config_t *node;
	fmm_error_t ret = FMM_EINVAL;

	if (newNode == NULL)
		return (FMM_EINVAL);

	fmm_lock_check("fmm_node_duplicate");

	if ((node = fmm_node_get(nodeId)) != NULL) {
		(void) memcpy(newNode, node, sizeof (fmm_node_config_t));
		ret = FMM_OK;
	}
	return (ret);
}

void
fmm_node_update(fmm_node_config_t *newNode)
{
	fmm_member_node_t *list_element = NULL;
	fmm_node_config_t *node;

	fmm_lock_check("fmm_node_update");

	if (newNode == NULL) {
		assert(newNode != NULL); /* Exit Only in debug mode */
		return; /* return in all cases */
	}

	/* Log a message if responding = true but monitoring disabled */
	if (!(newNode->monitoringState) && (newNode->responding)) {
		fmm_log(LOG_NOTICE,
		    "fmm_node_update: Node %s is UP but monitoring is DISABLED",
		    newNode->name);
	}
	/* Compute monitoring State and responding to evaluate role */
	if (newNode->monitoringState && newNode->responding)
		newNode->role = MEMBERSHIP_IN_CLUSTER_ROLE;
	else
		newNode->role = MEMBERSHIP_OUT_OF_CLUSTER_ROLE;

	/* Retrieve corresponding node */
	if ((list_element = list_element_get(newNode->nodeid)) == NULL) {
		assert(list_element != NULL);
		return;
	}

	node = list_element->info;
	if (node == NULL) {
		assert(node != NULL);
		return;
	}

	/* Save old values */
	if (list_element->use_old == B_FALSE) {
		(void) memcpy(list_element->old, node,
		    sizeof (fmm_node_config_t));
		list_element->use_old = B_TRUE;
	}

	if (node->monitoringState != newNode->monitoringState) {
		if (newNode->monitoringState) {
			(void) fmm_sc_publish_event(newNode->name,
			    CL_REASON_FARM_NODE_MONITORED);
			fmm_log(LOG_DEBUG,
			    "fmm_node_update: sc_publish_event NODE_MONITORED "
			    "%d",
			    newNode->nodeid);
		} else {
			(void) fmm_sc_publish_event(newNode->name,
			    CL_REASON_FARM_NODE_UNMONITORED);
			fmm_log(LOG_DEBUG,
			    "fmm_node_update: sc_publish_event NODE_UNMONITORED"
			    " %d",
			    newNode->nodeid);
		}
	}

	if (node->role != newNode->role) {
		fmm_viewnumber_increment();
		if (newNode->role == MEMBERSHIP_IN_CLUSTER_ROLE) {
			newNode->initialviewnumber
			    = fmm_cluster_viewnumber_get();
			(void) fmm_sc_publish_event(newNode->name,
			    CL_REASON_FARM_NODE_JOINED);
			fmm_log(LOG_DEBUG,
			    "fmm_node_update: sc_publish_event NODE_JOINED %d",
			    newNode->nodeid);
		} else {
			newNode->initialviewnumber = 0;
			(void) fmm_sc_publish_event(newNode->name,
			    CL_REASON_FARM_NODE_LEFT);
			fmm_log(LOG_DEBUG,
			    "fmm_node_update: sc_publish_event NODE_LEFT %d",
			    newNode->nodeid);
			fmm_conn_abort(newNode->addr);
			fmm_log(LOG_DEBUG,
			    "fmm_node_udpate: TCP_disconnect node %d",
			    newNode->nodeid);
		}
		fmm_notify_api_clients(fmm_cluster_viewnumber_get());
	}

	/* Update node contents */
	(void) memcpy(node, newNode, sizeof (fmm_node_config_t));
}
