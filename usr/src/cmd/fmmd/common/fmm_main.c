/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fmm_main.c	1.3	08/05/20 SMI"

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#ifdef linux
#include <sys/procfs.h>
#else
#include <procfs.h>
#include <strings.h>
#endif

#include <sys/st_failfast.h>
#include <fmm/hbr.h>
#include <fmm/fmmutils.h>
#include <scxcfg/scxcfg.h>

#include "fmm_internal_types.h"
#include "fmm_api_server.h"
#include "fmm_cluster.h"
#include "fmm_cfg.h"

#include <rgm/sczones.h>

#define	SYSLOG_FMM_FMMD_TAG "Cluster.FMM.fmmd"
int log_level = LOG_NOTICE;


/*
 * Return the number of fmmd daemons already running
 * Only one allowed at a time
 */
static int
nb_running_fmm(char *name)
{
	int result = 0;
	DIR *directory;
	struct dirent *oneDirectoryEntry;
	char strFileName[255];
#ifdef linux
	prpsinfo_t oneProcessInfo;
#else
	psinfo_t oneProcessInfo;
#endif
	int oneProcessFile;
	char *execName;	/* name without path */

	if (name == NULL)
		return (0);

	directory = opendir("/proc");

	if (directory == NULL)
		return (-1);

	/* Extract command file name (without full path) */
	execName = rindex(name, '/');
	if (execName == NULL)
		execName = name;
	else
		execName += 1;	/* skip "/" */

	while ((oneDirectoryEntry = readdir(directory)) != NULL) {
		if (oneDirectoryEntry->d_name[0] == '.')
			continue;

		(void) sprintf(strFileName, "/proc/%s/psinfo",
		    oneDirectoryEntry->d_name);
		oneProcessFile = open(strFileName, O_RDONLY);

		if (oneProcessFile == -1)
			continue;

		if (read(oneProcessFile, (void *) &oneProcessInfo,
#ifdef linux
		    sizeof (prpsinfo_t)) != (ssize_t)sizeof (prpsinfo_t)) {
#else
		    sizeof (psinfo_t)) != (ssize_t)sizeof (psinfo_t)) {
#endif
			(void) close(oneProcessFile);
				fmm_log(LOG_ERR, "nb_running_fmm: Failed to "
				    "read /psinfo file");
				(void) closedir(directory);
				return (-1);
		}

		if (strcmp(oneProcessInfo.pr_fname, execName) == 0)
			result++;

		(void) close(oneProcessFile);
	}
	(void) closedir(directory);
	return (result);
}

/*
 * daemonize the current process
 */
static int
daemonize(void)
{
	pid_t pid;

	fmm_log(LOG_DEBUG, "daemonize");

	/*
	 * Fork a first time
	 */

	if ((pid = fork()) > 0) {
		fmm_log(LOG_DEBUG, "daemonize: fmmd now runs as a daemon");
		exit(EXIT_SUCCESS);
	} else if (pid < 0) {
		fmm_log(LOG_CRIT,
		    "daemonize: failed to daemonize fmmd [%d/2] => abort", 1);
		exit(EXIT_FAILURE);
	}

	fmm_log(LOG_DEBUG, "daemonize: first child created");

	/* Become session leader */
	if (setsid() == (pid_t)-1) {
		fmm_log(LOG_CRIT, "daemonize: Error at setsid(): %s",
		    strerror(errno)); /*lint !e746 */
		exit(EXIT_FAILURE);
	}

	/* Ignore SIGHUP signal */
	if (sigignore(SIGHUP) == -1) {
		fmm_log(LOG_CRIT, "daemonize: Error at sigignore(): %s",
		    strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* Ignore SIGPIPE signal */
	if (sigignore(SIGPIPE) == -1) {
		fmm_log(LOG_CRIT, "daemonize: Error at sigignore(): %s",
		    strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* Fork a second time */
	if ((pid = fork()) > 0) {
		fmm_log(LOG_DEBUG, "daemonize: fmmd now runs as a daemon");
		exit(EXIT_SUCCESS);
	} else if (pid < 0) {
		fmm_log(LOG_CRIT,
		    "daemonize: failed to daemonize fmmd [%d/2] => abort", 2);
		exit(EXIT_FAILURE);
	}

	fmm_log(LOG_DEBUG, "daemonize: Second child created");

	/* Change current directory */
	if (chdir("/") == -1) {
		fmm_log(LOG_CRIT, "daemonize: Error at chdir() : %s",
		    strerror(errno));
		return (0);
	}

	/* Reinitialize umask */
	(void) umask((mode_t)0);

	return (0);
}

static void
fmm_hb_event(hb_nodeid_t nodeid, hb_evt_type_t event, uint32_t incn)
{
	fmm_node_config_t updated_node;

	fmm_log(LOG_DEBUG, "fmm_hb_event: node %d is %s\n", nodeid,
	    (event == F_NODE_UP)?"UP":"DOWN");

	(void) fmm_cluster_lock(FMM_NO_TIMEOUT);
	if (event == F_NODE_UP) {
		/* If node already exists, update properties */
		if (fmm_node_get(nodeid) != NULL) {
			if (fmm_node_duplicate(nodeid, &updated_node)
			    != FMM_OK) {
				fmm_log(LOG_ERR,
				    "fmm_hb_event: Failed to retrieve node %d",
				    nodeid);
				goto end;
			}
			updated_node.responding = B_TRUE;
			updated_node.incarnation_number = incn;
			fmm_log(LOG_NOTICE,
			    "fmm_hb_event: Node %s (id %d) is UP",
			    updated_node.name, nodeid);
		} else {
			/* Node unknown */
			fmm_log(LOG_WARNING,
			    "fmm_hb_event: Skipping node ID %d (unknown)",
			    nodeid);
			goto end;
		}
	} else if (event == F_NODE_DOWN) {
		/* If node already exists, update properties */
		if (fmm_node_get(nodeid) != NULL) {
			if (fmm_node_duplicate(nodeid, &updated_node)
			    != FMM_OK) {
				fmm_log(LOG_DEBUG,
				    "fmm_hb_event: Failed to set Node %d %s "
				    "for membership",
				    nodeid, (event == F_NODE_UP)?"UP":"DOWN");
				goto end;
			}
			updated_node.responding = B_FALSE;
			updated_node.incarnation_number = 0;
			fmm_log(LOG_NOTICE,
			    "fmm_hb_event: Node %s (id %d) is DOWN",
			    updated_node.name, nodeid);
		} else {
			/* Node unknown */
			fmm_log(LOG_WARNING,
			    "fmm_hb_event: Skipping node ID %d (unknown)",
			    nodeid);
			goto end;
		}
	} else {
		fmm_log(LOG_DEBUG, "fmm_hb_event: unexpected event");
		goto end;
	}

	fmm_node_update(&updated_node);
	fmm_log(LOG_DEBUG, "fmm_hb_event: node update ok");
end:
	fmm_cluster_unlock();
}

static void
fmm_cfg_event(fmm_cfg_event_t event, nodeid_t nodeid)
{
	fmm_node_config_t updated_node;
	scxcfg cluster_cfg;
	scxcfg_error error;
	char property[64];
	char value[FCFG_MAX_LEN];

	if (scxcfg_open(&cluster_cfg, &error) != 0) {
		fmm_log(LOG_ERR,
		    "fmm_cfg_event: unable to read cluster config");
		return;
	}

	(void) fmm_cluster_lock(FMM_NO_TIMEOUT);
	switch (event) {
	case FMM_CFG_NODE_ADDED:
		fmm_log(LOG_DEBUG, "fmm_cfg_event: FMM_CFG_NODE_ADDED %d",
		    nodeid);
		if (fmm_node_create(nodeid) == NULL) {
			fmm_log(LOG_ERR,
			    "fmm_cfg_event: Not enough memory to create node");
			goto end;
		}
		if (fmm_node_duplicate(nodeid, &updated_node) != FMM_OK) {
			fmm_log(LOG_DEBUG,
			    "fmm_cfg_event: Failed to retrieve node %d",
			    nodeid);
			goto end;
		}

		/* fill info from cluster config */
		(void) sprintf(property, FARM_NODE_IPADDR, nodeid);
		(void) scxcfg_getproperty_value(&cluster_cfg, NULL, property,
		    updated_node.addr, &error);

		(void) scxcfg_getproperty_value(&cluster_cfg, NULL,
		    FARM_NAME, updated_node.clusterName, &error);

		(void) sprintf(property, FARM_NODE_MONITORING_STATE, nodeid);
		if (scxcfg_getproperty_value(&cluster_cfg, NULL, property,
		    value, &error) == 0)
			if (strcmp(value, "enabled") == 0)
				updated_node.monitoringState = B_TRUE;
			else
				updated_node.monitoringState = B_FALSE;

		(void) sprintf(property, FARM_NODE_NAME, nodeid);
		(void) scxcfg_getproperty_value(&cluster_cfg, NULL, property,
		    updated_node.name, &error);

		updated_node.incarnation_number = 0;
		updated_node.initialviewnumber = 0;

		fmm_node_update(&updated_node);
		break;
	case FMM_CFG_NODE_REMOVED:
		fmm_log(LOG_DEBUG, "fmm_cfg_event: FMM_CFG_NODE_REMOVED %d",
		    nodeid);
		fmm_node_remove(nodeid);
		/*
		 * No need to notify clients as node removal is allowed only
		 * when the node is DOWN
		 */
		break;
	case FMM_CFG_NODE_CHANGED:
		fmm_log(LOG_DEBUG, "fmm_cfg_event: FMM_CFG_NODE_CHANGED %d",
		    nodeid);

		if (fmm_node_duplicate(nodeid, &updated_node) != FMM_OK) {
			fmm_log(LOG_DEBUG,
			    "fmm_cfg_event: Failed to retrieve node %d",
			    nodeid);
			goto end;
		}

		(void) sprintf(property, FARM_NODE_IPADDR, nodeid);
		(void) scxcfg_getproperty_value(&cluster_cfg, NULL, property,
		    updated_node.addr, &error);

		(void) scxcfg_getproperty_value(&cluster_cfg, NULL, FARM_NAME,
		    updated_node.clusterName, &error);

		(void) sprintf(property, FARM_NODE_MONITORING_STATE, nodeid);
		if (scxcfg_getproperty_value(&cluster_cfg, NULL, property,
		    value, &error) == 0)
			if (strcmp(value, "enabled") == 0)
				updated_node.monitoringState = B_TRUE;
			else
				updated_node.monitoringState = B_FALSE;

		(void) sprintf(property, FARM_NODE_NAME, nodeid);
		(void) scxcfg_getproperty_value(&cluster_cfg, NULL, property,
		    updated_node.name, &error);

		fmm_node_update(&updated_node);

		break;
	default:
		fmm_log(LOG_ERR, "fmm_cfg_event: unexpected event");
		break;
	}

	fmm_log(LOG_DEBUG, "fmm_cfg_event: node update ok");
end:
	fmm_cluster_unlock();
	(void) scxcfg_close(&cluster_cfg);
}


/*ARGSUSED*/
int
main(int argc, char *argv[])
{
	fmm_log_set_tag(SYSLOG_FMM_FMMD_TAG);
	fmm_log_set_level(log_level);

	if (sc_zonescheck() != 0)
		exit(EXIT_FAILURE);

	/* Check only one daemon is running */
	switch (nb_running_fmm(argv[0])) {
	case 1:
		/* Everything is fine */
		break;
	case 0:
	case -1:
		fmm_log(LOG_WARNING, "main: unable to detect running fmmd");
		break;
	default:
		fmm_log(LOG_CRIT, "main: another fmmd is running => exit");
		exit(EXIT_FAILURE);
	}

	(void) daemonize();

	/*
	 * Initialize cluster node list
	 */
	fmm_cluster_init();

	/*
	 * Initialize the failfast driver
	 */

	if (st_ff_arm("fmmd") != 0) {
		fmm_log(LOG_CRIT, "main: unable to arm failfast => abort");
		exit(EXIT_FAILURE);
	}

	fmm_log(LOG_DEBUG, "main: before fmm_api_init");

	if (fmm_api_init() != FMM_OK) {
		fmm_log(LOG_CRIT,
		    "main: unable to initialize the API => abort");
		exit(EXIT_FAILURE);
	}

	if (fmm_cfg_register(fmm_cfg_event) != 0) {
		fmm_log(LOG_CRIT, "main: failed to initialize config => abort");
		exit(EXIT_FAILURE);
	}

	if (hbr_init(fmm_hb_event) != 0) {
		fmm_log(LOG_CRIT,
		    "main: failed to initialize probe connection => abort");
		exit(EXIT_FAILURE);
	}

	if (hbr_reset() != HB_OK) {
		fmm_log(LOG_WARNING,
		    "main: failed to reset probe connexion => continue");
	}

	if (hbr_start() != HB_OK) {
		fmm_log(LOG_CRIT,
		    "main: failed to start probe connexion => abort");
		exit(EXIT_FAILURE);
	}

	while (1) {
		(void) sleep(1000);
	}

}
