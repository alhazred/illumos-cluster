/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fmm_events.c 1.3	08/05/20 SMI"

#include "fmm_events.h"

cl_event_error_t
fmm_sc_publish_event(char *nodename, uint32_t reason)
{
	return (sc_publish_event(ESC_CLUSTER_FARM_NODE_STATE_CHANGE,
	    CL_EVENT_PUB_FMM, CL_EVENT_SEV_INFO, CL_EVENT_INIT_SYSTEM, 0, 0, 0,
	    CL_NODE_NAME, SE_DATA_TYPE_STRING, nodename, CL_REASON_CODE,
	    SE_DATA_TYPE_UINT32, reason, NULL));
}
