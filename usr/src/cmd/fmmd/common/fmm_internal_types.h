/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FMM_INTERNAL_TYPES_H
#define	_FMM_INTERNAL_TYPES_H

#pragma ident	"@(#)fmm_internal_types.h	1.3	08/05/20 SMI"

/*
 * fmm_internal_types.h
 */

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

#define	FARM_NAME "farm.name"
#define	FARM_NODE_NAME "farm.nodes.%d.name"
#define	FARM_NODES "farm.nodes"
#define	FARM_NODE_MONITORING_STATE "farm.nodes.%d.monitoring_state"
#define	FARM_NODE_IPADDR "farm.nodes.%d.network.interface.1.address"


#define	FMM_MAX_ADDR_SIZE 64
#define	FMM_MAX_NAME_SIZE 256

typedef enum {
	FMM_OK = 0,
	FMM_EBADF = -100,
	FMM_EBUSY = -100 + 1,
	FMM_ECANCELED = -100 + 2,
	FMM_EEXIST = -100 + 3,
	FMM_EINVAL = -100 + 4,
	FMM_ENOENT = -100 + 5,
	FMM_ENOMSG = -100 + 6,
	FMM_ENOTSUP = -100 + 7,
	FMM_EPERM = -100 + 8,
	FMM_ERANGE = -100 + 9,
	FMM_ESRCH = -100 + 10,
	FMM_ENOCLUSTER = -100 + 11,
	FMM_ECONN = -100 + 12,
	FMM_ETIMEDOUT = -100 + 13,
	FMM_EAGAIN = -100 + 14
} fmm_error_t;

typedef uint32_t fmm_nodeid_t;
typedef char fmm_memberaddr_t[FMM_MAX_ADDR_SIZE];
typedef boolean_t fmm_responsiveness_t;

typedef enum fmm_membership_role_t {
	MEMBERSHIP_IN_CLUSTER_ROLE,
	MEMBERSHIP_OUT_OF_CLUSTER_ROLE
} fmm_membership_role_t;

typedef char fmm_membername_t[FMM_MAX_NAME_SIZE];
typedef long long fmm_incarn_t;

typedef struct fmm_node_config_t {
	fmm_nodeid_t nodeid;
	fmm_memberaddr_t addr;
	fmm_membership_role_t role;
	fmm_responsiveness_t responding;
	fmm_responsiveness_t monitoringState;
	fmm_membername_t name;
	fmm_membername_t clusterName;
	fmm_incarn_t incarnation_number;
	uint64_t initialviewnumber;
} fmm_node_config_t;

#ifdef __cplusplus
}
#endif

#endif /* _FMM_INTERNAL_TYPES_H */
