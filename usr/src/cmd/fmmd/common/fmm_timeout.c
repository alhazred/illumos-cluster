/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)fmm_timeout.c 1.4	08/05/20 SMI"

#include <time.h>
#include <errno.h>

#include "fmm_timeout.h"

/* Time between tries when trying to get a lock (100 us) */
#define	POLL_S	0
#define	POLL_NS 100000LL


fmm_error_t
fmm_check_timeout(fmm_timeout_t timeout)
{

#ifdef linux
	return (FMM_OK);
#endif

	if (timeout == FMM_NO_TIMEOUT)
		return (FMM_OK);

	return (gethrtime() > timeout ? FMM_ETIMEDOUT : FMM_OK);
}

fmm_error_t
fmm_pthread_mutex_timedlock(pthread_mutex_t *mutex, fmm_timeout_t timeout)
{
	int		err;
	fmm_error_t	result;
	struct timespec req = { POLL_S, POLL_NS };

	if (timeout == FMM_NO_TIMEOUT) {
		err = pthread_mutex_lock(mutex);
	} else {
		do {
			err = pthread_mutex_trylock(mutex);
			if (err == EBUSY &&
			    fmm_check_timeout(timeout) == FMM_OK)
				(void) nanosleep(&req, NULL);
		} while (err == EBUSY &&
		    fmm_check_timeout(timeout) == FMM_OK);
	}

	switch (err) {
	case 0:		/* OK */
		result = FMM_OK;
		break;
	case EBUSY:	/* Timed-out */
		result = FMM_ETIMEDOUT;
		break;
	default:	/* Error */
		result = FMM_EINVAL;
		break;
	}

	return (result);
}
