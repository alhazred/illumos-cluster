/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FMM_API_JOBS_H
#define	_FMM_API_JOBS_H

#pragma ident	"@(#)fmm_api_jobs.h	1.3	08/05/20 SMI"

#ifdef __cplusplus
extern "C" {
#endif

SaErrorT fmm_api_register(fmm_api_register_args_t args);
SaErrorT fmm_api_connect(int P_id);
SaErrorT fmm_api_unregister(int P_id);
SaErrorT fmm_api_node_get(fmm_api_node_get_args_t args,
    SaClmClusterNodeT *clusterNode);
SaErrorT fmm_api_all_nodes_get(fmm_api_all_nodes_get_args_t args,
    fmm_api_all_nodes_get_res_t *res);

#ifdef __cplusplus
}
#endif

#endif /* _FMM_API_JOBS_H */
