/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FMM_TIMEOUT_H
#define	_FMM_TIMEOUT_H

#pragma ident	"@(#)fmm_timeout.h	1.3	08/05/20 SMI"

/*
 * fmm_timeout.h
 */

#include <pthread.h>
#include "fmm_internal_types.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef hrtime_t fmm_timeout_t;

#define	FMM_NO_TIMEOUT ((fmm_timeout_t) -1)


fmm_error_t fmm_check_timeout(fmm_timeout_t P_Timeout);
fmm_error_t fmm_pthread_mutex_timedlock(pthread_mutex_t *mutex,
    fmm_timeout_t P_Timeout);

#ifdef __cplusplus
}
#endif

#endif /* _FMM_TIMEOUT_H */
