/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FMM_CLUSTER_H
#define	_FMM_CLUSTER_H

#pragma ident	"@(#)fmm_cluster.h	1.3	08/05/20 SMI"

#include "fmm_internal_types.h"
#include "fmm_timeout.h"

#ifdef __cplusplus
extern "C" {
#endif

extern void fmm_cluster_init(void);
extern fmm_error_t fmm_cluster_lock(fmm_timeout_t P_Timeout);
extern void fmm_cluster_unlock(void);
extern unsigned long fmm_cluster_nodes_number(void);
extern unsigned long fmm_cluster_viewnumber_get(void);

extern fmm_node_config_t *fmm_node_get(fmm_nodeid_t const P_nodeid);
extern fmm_node_config_t *fmm_first_node_get(void);
extern fmm_node_config_t *fmm_next_node_get(fmm_node_config_t const * P_node);
extern fmm_node_config_t *fmm_node_create(fmm_nodeid_t const P_nodeid);
extern fmm_error_t fmm_node_duplicate(fmm_nodeid_t const P_nodeid,
    fmm_node_config_t *P_node);
extern void fmm_node_update(fmm_node_config_t *P_node);
extern void fmm_node_remove(fmm_nodeid_t const nodeId);


#ifdef __cplusplus
}
#endif

#endif /* _FMM_CLUSTER_H */
