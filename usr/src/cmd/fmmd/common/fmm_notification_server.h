/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _FMM_NOTIFICATION_SERVER_H
#define	_FMM_NOTIFICATION_SERVER_H

#pragma ident	"@(#)fmm_notification_server.h	1.3	08/05/20 SMI"

/*
 * fmm_notification_server.h
 */

#include <sys/types.h>
#include "fmm_internal_types.h"

#ifdef __cplusplus
extern "C" {
#endif

extern boolean_t fmm_fifo_destroy(int P_UniqueId);
extern fmm_error_t fmm_fifo_create(int P_UniqueId);
extern int fmm_fifo_open_for_write(int const P_UniqueId);

extern void fmm_notify_api_clients(unsigned long viewnumber);

#ifdef __cplusplus
}
#endif

#endif /* _FMM_NOTIFICATION_SERVER_H */
