#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.5	08/05/20 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/fmmd/Makefile.com
#

DAEMON	= fmmd

include ../../Makefile.cmd

OBJECTS = \
	fmm_main.o \
	fmm_api_server.o \
	fmm_notification_server.o \
	fmm_cluster.o \
	fmm_timeout.o \
	fmm_events.o \
	fmm_conn_abort.o \
	fmm_cfg.o \
	fmm_api_jobs.o

SRCS	= $(OBJECTS:%.o=../common/%.c)
LINTFILES = $(OBJECTS:%.o=%.ln)

CHECKHDRS = fmm_api_jobs.h fmm_api_server.h fmm_cluster.h fmm_conn_abort.h \
	fmm_events.h fmm_internal_types.h fmm_notification_server.h \
	fmm_timeout.h fmm_cfg.h

CHECKFILES = $(OBJECTS:%.o=%.c_check) \
	$(CHECKHDRS:%.h=%.h_check)

CLOBBERFILES += $(DAEMON) $(OBJECTS)

.KEEP_STATE:

MTFLAG = -mt

CPPFLAGS += -D_REENTRANT -DEUROPA $(MTFLAG)
CPPFLAGS +=   $(CL_CPPFLAGS)
CPPFLAGS += -I$(SRC)/common/cl -I$(SRC)/common/cl/interfaces/$(CLASS)

LDLIBS += -lpthread -lrt -ldoor -lsocket -lnsl
LDLIBS += -lclevent -lclos -lclcomm -lclst $(REF_USRLIB:%=%/libCrun.so.1)
LDLIBS += -lscxcfg -lfmmutils -lhbr -lsczones

all: $(DAEMON)

$(DAEMON): $(OBJECTS)
	$(LINK.cc) $(OBJECTS) -o $@ $(LDLIBS)
	$(POST_PROCESS)

%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

%.o: ../common/%.cc
	$(COMPILE.cc) -o $@ $<

clean:
	$(RM) $(OBJECTS)

include ../../Makefile.targ
