#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.6	08/06/13 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# cmd/pnm/Makefile.com
#

PROG	= cl_pnmd

include $(SRC)/cmd/Makefile.cmd

SRCS		= $(OBJS:%.o=../common/%.c)
LINTFILES	= $(SRCS:%.c=%.ln)
PIFILES		= $(SRCS:%.c=%.pi)
POFILE		= pnmstuff.po

COMMON_OBJS	= pnmd_main.o pnmd_util.o pnmd_log.o pnmd_group.o pnmd_server.o
SPEC_OBJS = pnmd_probe_solaris_ipmp.o pnmd_network_solaris_ipmp.o

OBJS		= $(COMMON_OBJS) $(SPEC_OBJS)

CLOBBERFILES += $(PIFILES) $(OBJS) $(LINTFILES)

#
# Compiler flags
#
CPPFLAGS += $(MTFLAG) 
CPPFLAGS += -I$(REF_PROTO)/usr/src/head
CPPFLAGS += -I$(REF_PROTO)/usr/src/lib/libbrand/common
CPPFLAGS += -I$(REF_PROTO)/usr/src/lib/libuutil/common
CPPFLAGS += -I$(SRC)/head -DSYSV -DSTRNET -DBSD_COMP

LDLIBS += -lclos -lposix4 -lnsl -lkstat -lsocket -lscha -ldl \
		-lelf -lclevent -ldsdev -lsczones
AS_CPPFLAGS	+= -D_ASM
ASFLAGS		+= -P

LINTFLAGS	+=  -I$(SRC)/head

#
# Targets
#

all: $(PROG)

clean :
	$(RM) *.o $(CLOBBERFILES)

$(PROG): $(OBJS)
	$(LINK.cc) -o $@ $(OBJS) $(LDLIBS)
	$(POST_PROCESS)

%.o: ../common/%.c
	$(COMPILE.c) -o $@ $<

%.ll:	../common/%.c
	$(WLCC) $(CFLAGS) $(CPPFLAGS) -o $@ $<

include ../../Makefile.targ
